#include <fstream>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctime>

#include "DGCutils"
#include "roadFinding.hh"

// Function declaration


void printHelp();
int loadDefault();
int loadConfig(char* fromFile);
void outputConfig();
void refreshTrackbarPos();
void refreshSteps(int pos);

// edgeFinding
int line_link_pos;
int main_line_pos;

// lineMarker
int acc_pos;
int min_line_length_pos;
int max_gap_pos;
int distance_res_pos;
int angle_res_pos;
int size_tol_pos;
int angle_tol_pos;
int distance_tol_pos;
	
// processing time
long int startTime;
double timeTaken;
int start=0;

// state stuff
bool waitForState = false;

roadFinding process(14785236);

int loadDefault() {
	// edgeFinding
	line_link_pos = 60;
	main_line_pos = 300;

	// hough transform
	acc_pos = 80;
	min_line_length_pos = 20;
	max_gap_pos = 10;
	distance_res_pos = 1;
	angle_res_pos = 1;
	size_tol_pos = 15; //x10
	angle_tol_pos = 40; //x10
	distance_tol_pos = 150; //x10
	
	refreshTrackbarPos();
	
	cout << "Calibration loaded from default values\n" ;
	return 1;
}

int loadConfig(char* fromFile) {
	char *next;
	string line;
	
	ifstream calibration(fromFile);
	
	while (!calibration.eof()) {
		getline(calibration, line);
		if (line == "# Image: width; height") { 
			next = "width"; continue;
		} 
		if (next == "width") {
			// imageWidth = atoi(line.c_str()); we dont care about the dimensions here
			next = "height"; continue;
		} 
		if (next == "height") {
			// imageHeight = atoi(line.c_str());
			next = "null"; continue;
		} 
		if (line == "# EdgeFinding thresholds: Line Linking; Main Line") { 
			next = "EFlineLink"; continue;
		}
		if (next == "EFlineLink") {
			line_link_pos = atoi(line.c_str());
			next = "EFmainLine"; continue;
		}
		if (next == "EFmainLine") {
			min_line_length_pos = atoi(line.c_str());
			next = "null"; continue;
		}
		if (line == "# LaneMarker: Accumulator; Min Line Length; Max Gap; Distance Resolution; Angle Resolution; Size Tolerance; Angle Tol.; Distance Tol. ") { 
			next = "LMacc"; continue;
		}
		if (next == "LMacc") {
			acc_pos = atoi(line.c_str());
			next = "LMminLineLength"; continue;
		}
		if (next == "LMminLineLength") {
			min_line_length_pos = atoi(line.c_str());
			next = "LMmaxGap"; continue;
		}
		if (next == "LMmaxGap") {
			max_gap_pos = atoi(line.c_str());
			next = "LMdistanceResolution"; continue;
		}
		if (next == "LMdistanceResolution") {
			distance_res_pos = atoi(line.c_str());
			next = "LMangleResolution"; continue;
		}
		if (next == "LMangleResolution") {
			angle_res_pos = atoi(line.c_str());
			next = "LMsizeTol"; continue;
		}
		if (next == "LMsizeTol") {
			size_tol_pos = (int) round(10*atof(line.c_str()));
			next = "LMangleTol"; continue;
		}
		if (next == "LMangleTol") {
			angle_tol_pos = (int) round(10*atof(line.c_str()));
			next = "LMdistanceTol"; continue;
		}
		if (next == "LMdistanceTol") {
			distance_tol_pos = (int) round(10*atof(line.c_str()));
			next = "null"; continue;
		}
	}
	
	cout << "Calibration loaded from file\n" ;

	refreshTrackbarPos();
	
	calibration.close();
	return 1;
	
}
	
void outputConfig() {
	ofstream calibration("roadFinding.cal",ofstream::binary);
	
	calibration << "## Calibration file" << endl;
	calibration << "## !Add comments here!" << endl;
	calibration << "## Dont change number of lines" << endl;
	calibration << "# Image: width; height" << endl;
	calibration << process.cvCalLeft->width << endl << process.cvCalLeft->height << endl;
	calibration << "# EdgeFinding thresholds: Line Linking; Main Line" << endl;
	calibration << line_link_pos << endl << main_line_pos << endl;
	calibration << "# LaneMarker: Accumulator; Min Line Length; Max Gap; Distance Resolution; Angle Resolution; Size Tolerance; Angle Tol.; Distance Tol. " << endl;
	calibration << acc_pos << endl << min_line_length_pos << endl << max_gap_pos << endl << distance_res_pos << endl << angle_res_pos << endl << size_tol_pos << endl << angle_tol_pos << endl << distance_tol_pos << endl;
	calibration << "# Time processing: " << timeTaken << " sec for a full iteration" << endl;
	calibration << "# End" << endl;
	
	calibration.close();
	
	cout << "Calibration File Written" << endl;
	return;
}

void refreshTrackbarPos() {
	cvSetTrackbarPos("Line Link Threshold","EdgeFinding",line_link_pos);
	cvSetTrackbarPos("Main Line Threshold","EdgeFinding",main_line_pos);

	cvSetTrackbarPos("Acc Threshold","LaneMarker",acc_pos);
	cvSetTrackbarPos("Min Line Trld","LaneMarker",min_line_length_pos);
	cvSetTrackbarPos("Max Gap Thrld","LaneMarker",max_gap_pos);
	cvSetTrackbarPos("Distan Res *Pixels","LaneMarker",distance_res_pos);	
	cvSetTrackbarPos("Angle Res *degrees","LaneMarker",angle_res_pos);
	cvSetTrackbarPos("Size  Tol x10","LaneMarker",size_tol_pos);
	cvSetTrackbarPos("Angle Tol x10","LaneMarker",angle_tol_pos);
	cvSetTrackbarPos("Dista Tol x10","LaneMarker",distance_tol_pos);
	
}

void refreshSteps(int pos) {

	// check bad values
	if (acc_pos==0) { 
		cvSetTrackbarPos("Acc Threshold","LaneMarker",1);
		return;
	}  
	if (distance_res_pos==0) { 
		cvSetTrackbarPos("Dist Res *Pixels","LaneMarker",1);
		return;
	}
	if (angle_res_pos==0) {
		cvSetTrackbarPos("Angle Res *degrees","LaneMarker",1);
		return;
	}
	
	process.refreshCalibrationParameters(line_link_pos,main_line_pos,acc_pos,min_line_length_pos,max_gap_pos,distance_res_pos,angle_res_pos,size_tol_pos,angle_tol_pos,distance_tol_pos);
	
	if (start) {
		int oneStep=process.iFrame+1;
		startTime = clock();
		process.loopStereo(oneStep,100);
  		timeTaken = (double)(clock() - startTime)/((double)CLOCKS_PER_SEC);
  	}
  	
   cvShowImage("EdgeFinding",process.cvCalEdges);
   cvShowImage("LaneMarker",process.cvCalLanes);
}

void printMenu() {

   printf( "\nHot keys: \n"
       "\t Q,q - quit the program\n"
       "\t P,p - re-print this menu\n"
       "\t D,d - load 'default' values\n"
       "\t F,f - load 'calibration file' values\n"
       "\t O,o - output config file\n"
       "\t T,t - output time of last iteration\n"
       "\t R,r - refresh frame (live) or get next frame (files)\n"
       "\t S,s - save iteration's frame\n");       
}

int main(int argc, char *argv[]) {

	printMenu();
		
	if (argc == 1)	{
		cout << "Wrong number of arguments. Type './roadFindingCalibration -help' for help\n" << endl;
		exit(0);
	}
	
	if (argc > 1 ) {
	  string option=argv[1];
		if (option=="-files") {
			process._source=USE_FILES;
			cout << "Working from files\n" << endl;
		} 
		if (option=="-cameras") {
			process._source=USE_CAMERAS;
			cout << "Working from cameras\n" << endl;
		}
		if (option=="-help") {
			printHelp();
			exit(0);
		}
	}
	
	
	process._autoSaveImages=0;
	process._calibration=1;
	process.initStereo();

	process.refreshCalFrame=1;
	
	//create windows for output images
   cvNamedWindow("EdgeFinding",1);
   cvNamedWindow("LaneMarker",1);

   cvCreateTrackbar("Line Link Threshold", "EdgeFinding",&line_link_pos,200,refreshSteps);
   cvCreateTrackbar("Main Line Threshold", "EdgeFinding",&main_line_pos,500,refreshSteps);   
   cvCreateTrackbar("Acc Threshold", "LaneMarker",&acc_pos,500,refreshSteps);
   cvCreateTrackbar("Min Line Trld", "LaneMarker",&min_line_length_pos,300,refreshSteps);
   cvCreateTrackbar("Max Gap Thrld", "LaneMarker",&max_gap_pos,200,refreshSteps);
   cvCreateTrackbar("Distan Res *Pixels", "LaneMarker",&distance_res_pos,10,refreshSteps);
   cvCreateTrackbar("Angle Res *degrees", "LaneMarker",&angle_res_pos,10,refreshSteps);
   cvCreateTrackbar("Size  Tol x10", "LaneMarker",&size_tol_pos,100,refreshSteps);
   cvCreateTrackbar("Angle Tol x10", "LaneMarker",&angle_tol_pos,100,refreshSteps);
   cvCreateTrackbar("Dista Tol x10", "LaneMarker",&distance_tol_pos,500,refreshSteps);
   
	loadDefault();
	
	// prevent refresgSteps to be called on trackbar creation
	start = 1;
	
	for(;;) {
      int c;
      
      refreshSteps(0);
		c = cvWaitKey(0);

		if ( (char) c == 'Q' || (char) c == 'q') break;
		if ( (char) c == 'P' || (char) c == 'p') printMenu();
		if ( (char) c == 'D' || (char) c == 'd') loadDefault();
		if ( (char) c == 'F' || (char) c == 'f') loadConfig("roadFinding.cal");
		if ( (char) c == 'O' || (char) c == 'o') outputConfig();
		if ( (char) c == 'T' || (char) c == 't') cout << "Last iteration took " << timeTaken << " sec" << endl;
		if ( (char) c == 'R' || (char) c == 'r') process.refreshCalFrame=1;
		if ( (char) c == 'S' || (char) c == 's') process.saveCalIteration=1;
		 
   }
	
   //destroy windows 
	cvDestroyAllWindows();

	return 0;
}

void printHelp() {
   printf( "\n Usage: ./roadFinding -source\n"
       "\t -source:\n"
       "\t\t '-files' for images in 'images' folder\n" 
       "\t\t '-cameras' for online images from short pair cameras\n" );       
}
