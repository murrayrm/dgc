ROADFINDING_PATH = $(DGC)/projects/humberto/

ROADFINDING_DEPEND_LIBS = \
									$(DGCUTILS) $(SKYNETLIB) \
									$(STEREOSOURCELIB) $(FRAMESLIB) \
	 								$(STEREOPROCESSLIB) $(MATRIXLIB) \

ROADFINDING_DEPEND_SOURCE = \
                           $(ROADFINDING_PATH)/roadFinding.hh \
                           $(ROADFINDING_PATH)/roadFinding.cc \
                           $(ROADFINDING_PATH)/roadFindingMain.cc \
									$(ROADFINDING_PATH)/Makefile


ROADFINDING_DEPEND = $(ROADFINDING_DEPEND_LIBS) \
                      $(ROADFINDING_DEPEND_SOURCE)
