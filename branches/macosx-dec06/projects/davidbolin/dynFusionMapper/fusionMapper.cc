//#include "fusionMapperTabSpecs.hh"
#include "fusionMapper.hh"

using namespace sc_interface;

extern double sparrowTimeLockFeeder[MAX_INPUT_ELEV_LAYERS];
extern double sparrowTimeLockFusionMapper;

FusionMapper::FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg, int waitForState)
  : CSkynetContainer(SNfusionmapper, skynetKey, &(this->_status)),
    //CModuleTabClient(&m_input, &m_output),
    CStateClient(waitForState),
    CSuperConClient("FMP")
{
  _status = 10;
  printf("Welcome to fusionmapper init0\n");
  _QUIT = 0;
  _PAUSE = 1;
  
  mapperOpts = mapperOptsArg;
  _PAINTWHILEPAUSED = 0;
  mapperOpts.optWingWidth = DEFAULT_WING_WIDTH;

  // static allocated for memory efficiency, if larger than 100 pts, change this
  m_pRoadReceive = new double[MAX_ROAD_SIZE * 3 + 1];
  m_pRoadX = new double[MAX_ROAD_SIZE];
  m_pRoadY = new double[MAX_ROAD_SIZE];
  m_pRoadW = new double[MAX_ROAD_SIZE];
  m_pRoadPoints = 0;

  fusionHerman = new herman("fusionMapper");

  //initialize maps
  fusionMap.initMap(CONFIG_FILE_DEFAULT_MAP);
  roadMap.initMap(CONFIG_FILE_DEFAULT_MAP);
  swapMapA.initMap(CONFIG_FILE_DEFAULT_MAP);
  swapMapB.initMap(CONFIG_FILE_DEFAULT_MAP);
  
  //create all layers
  layerID_costFinal   = fusionMap.addLayer<double>(CONFIG_FILE_COST, true);
  layerID_corridor    = fusionMap.addLayer<double>(CONFIG_FILE_RDDF, true);  
  layerID_road = roadMap.addLayer<double>(CONFIG_FILE_ROAD);
  layerID_roadCounter = roadMap.addLayer<double>(CONFIG_FILE_ROAD_COUNTER);
  layerID_superCon = fusionMap.addLayer<double>(CONFIG_FILE_SUPERCON, false);
  layerID_fusedKF = fusionMap.addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV, false);
  layerID_fusedElev = fusionMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV, true);
  layerID_fusedVar = fusionMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV,              true);
  layerID_movingObst = fusionMap.addLayer<bool>(false, false, false);

  //Allocate memory for the list containing the moving obstacle information.
  obstacleList = new movingObstacle[MAX_NBR_MOVING];
  
  //Create the DynamicFuser
  costFuser = new CDynCostFuser(&fusionMap, layerID_movingObst, &fusionMap, 
			       layerID_costFinal, &fusionMap, layerID_corridor, 
			       &swapMapA, &swapMapB,
			       &costFuserOpts, &_corridorOptsFinalCost);

  costFuser->addLayerRoad(&roadMap, layerID_road);
  costFuser->addLayerSuperCon(&fusionMap, layerID_superCon);

  
  //Now initialize the FusionMapperElevationLayers in layerArray: 
  //each position in this vector corresponds to a ladar.

  layerArray[0].msgTypeElev = SNladarRoofDeltaMap;
  layerArray[0].msgTypeCost = SNladarRoofDeltaMapCost;
  layerArray[0].relWeight = 1.0;
  sprintf(layerArray[0].name, "LADAR (Roof)");
  sprintf(layerArray[0].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.roof");
  
  layerArray[1].msgTypeElev = SNladarSmallDeltaMap;
  layerArray[1].msgTypeCost = SNladarSmallDeltaMapCost;
  layerArray[1].relWeight = 0.1;
  sprintf(layerArray[1].name, "LADAR (Small)");
  sprintf(layerArray[1].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.small");
  
  layerArray[2].msgTypeElev = SNladarRieglDeltaMap;
  layerArray[2].msgTypeCost = SNladarRieglDeltaMapCost;
  layerArray[2].relWeight = 0.01;
  sprintf(layerArray[2].name, "LADAR (Riegl)");
  sprintf(layerArray[2].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.riegl");
  
  layerArray[3].msgTypeElev = SNladarBumperDeltaMap;
  layerArray[3].msgTypeCost = SNladarBumperDeltaMapCost;
  layerArray[3].relWeight = 0.0;
  sprintf(layerArray[3].name, "LADAR (Bumper)");
  sprintf(layerArray[3].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.bumper");	
  
  layerArray[4].msgTypeElev = SNladarFrontDeltaMap;
  layerArray[4].msgTypeCost = SNladarFrontDeltaMapCost;
  layerArray[4].relWeight = 0.5;
  sprintf(layerArray[4].name, "LADAR (Front)");
  sprintf(layerArray[4].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.front");	
  
  
  layerArray[5].msgTypeElev = SNstereoShortDeltaMap;
  layerArray[5].msgTypeCost = SNstereoShortDeltaMapCost;
  layerArray[5].relWeight = 1.0;
  sprintf(layerArray[5].name, "Stereo (Short)");
  sprintf(layerArray[5].optionsFilename, "config/fusionMapper/opts.fusionMapper.stereo.short");
  
  
  layerArray[6].msgTypeElev = SNstereoLongDeltaMap;
  layerArray[6].msgTypeCost = SNstereoLongDeltaMapCost;
  layerArray[6].relWeight = 1.0;
  sprintf(layerArray[6].name, "Stereo (Long)");
  sprintf(layerArray[6].optionsFilename, "config/fusionMapper/opts.fusionMapper.stereo.long");
  
  
  
  for(int i=0; i < NUM_ELEV_LAYERS; i++) {
    layerArray[i].sendCost = true;
    //enable all maps but the bumper ladar:
    if(i!=3) {
      layerArray[i].enabled = true;
    } else {
      layerArray[i].enabled = false;
    }
    //create the maps and initialize them:
    layerArray[i].map = new CMapPlus();
    if(i != 2){
      layerArray[i].map->initMap(CONFIG_FILE_DEFAULT_MAP);
    }
    else{
      layerArray[i].map->initMap(CONFIG_FILE_LOWRES_MAP);
    }

    DGCcreateMutex(&layerArray[i].shiftMutex);
    layerArray[i].elevLayerNum = (layerArray[i].map)->addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV);
    layerArray[i].terrainPainter = new CTerrainCostPainter( layerArray[i].map , costFuser);
    
    layerArray[i].terrainPainterIndex = (layerArray[i].terrainPainter)->addElevLayer(layerArray[i].elevLayerNum, 
										     &layerArray[i].options, layerArray[i].sendCost);
    layerArray[i].costLayerNum = (layerArray[i].terrainPainter)->_costLayerNums[layerArray[i].terrainPainterIndex];
    layerArray[i].costPainterIndex = costFuser->addLayerElevCost(layerArray[i].map, layerArray[i].costLayerNum,
								 layerArray[i].relWeight,
								 layerArray[i].map, layerArray[i].elevLayerNum);
    
    layerArray[i].corridorPainter = new CCorridorPainter(layerArray[i].map, &(fusionRDDF), &widerRDDF, fusionHerman);
    layerArray[i].corridorPainter->addLayer(layerArray[i].corridorLayerNum, &_corridorOptsReference, &(fusionRDDF), 1);
  }
  
  fusionCorridorPainter = new CCorridorPainter(&(fusionMap), &(fusionRDDF), &widerRDDF, fusionHerman);
  fusionCorridorPainter->addLayer(layerID_costFinal, &_corridorOptsWings, &(widerRDDF), 1);
  fusionCorridorPainter->addLayer(layerID_costFinal, &_corridorOptsFinalCost, &(fusionRDDF), 1);
  fusionCorridorPainter->addLayer(layerID_corridor, &_corridorOptsWings, &(widerRDDF), 1);
  fusionCorridorPainter->addLayer(layerID_corridor, &_corridorOptsReference, &(fusionRDDF), 1);
  
  //Set the CDynCostFuser options.
  costFuserOpts.maxSpeed = 15.0; //defaultTerrainPainterOpts.maxSpeed = 10.0;
  costFuserOpts.maxCellsToInterpolate = 1;
  costFuserOpts.minNumSupportingCellsForInterpolation=5;
  costFuserOpts.paintRoadWithoutElevation = 1;
  costFuserOpts.optDA = mapperOpts.optDA;

  _corridorOptsFinalCost.optRDDFscaling = 1.0;
  _corridorOptsFinalCost.optMaxSpeed = -2.5;
  _corridorOptsFinalCost.optOverwriteNegative = false;
  
  _corridorOptsWings.optRDDFscaling = 1.0;
  _corridorOptsWings.optMaxSpeed = 1.75;
  _corridorOptsWings.optOverwriteNegative = false;
  
  _corridorOptsReference.optRDDFscaling = 1.0;
  _corridorOptsReference.optMaxSpeed = 15.0;
  _corridorOptsReference.optOverwriteNegative = true;
  DGCcreateMutex(&roadMutex);
  
  DGCcreateMutex(&kfMutex);
  
  DGCcreateRWLock(&allMapsRWLock);
  
  usleep(500);
  
  ReadConfigFile();
  WriteConfigFile();
  
  widerRDDF.setExtraWidth(mapperOpts.optWingWidth);
}


FusionMapper::~FusionMapper() {
  //FIX ME
  printf("Thank you for using the fusionmapper destructor\n");
  delete fusionHerman;
}


void FusionMapper::ActiveLoop() {
  //TODO - Give this a sparrow/sngui display
  printf("Entering FusionMapper active-loop\n");
  printf("Press Ctrl+C to quit\n");
  //-------------------
  //char dataFilename[20];
  //sprintf(dataFilename, "%s.dat", "processTimes");
  //-------------------
  int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
  int socketMapDeltaCorridor = m_skynet.get_send_sock(SNdeltaMapCorridor);
  int mapDeltaSocketCost[6];

  //get the sockets on which the deltas for the different maps should be sent:
  for(int i=0; i < NUM_ELEV_LAYERS; i++)
    mapDeltaSocketCost[i] = m_skynet.get_send_sock(layerArray[i].msgTypeCost);

  NEcoord exposedRow[4], exposedCol[4];
  NEcoord frontLeftAliceCorner, frontRightAliceCorner, rearLeftAliceCorner, rearRightAliceCorner;
  
  bool firstPainting = true;
  
  _status = 0;
  while(!_QUIT) {
    int deltaSize = 0;
    
    WaitForNewState();
    _infoFusionMapper.startProcessTimer();
    
    if(!_PAUSE) { 
      DGCreadlockRWLock(&allMapsRWLock);
      fusionMap.updateVehicleLoc(m_state.Northing, m_state.Easting);
      
      if(fusionMap.getExposedRowBoxUTM(exposedRow) ||
	 fusionMap.getExposedColBoxUTM(exposedCol)) {
	fusionMap.getExposedColBoxUTM(exposedCol);
	
	DGClockMutex(&roadMutex);
	roadMap.updateVehicleLoc(m_state.Northing, m_state.Easting);

	//recenter all maps:
	for(int i=0; i < NUM_ELEV_LAYERS; i++) {
	  DGClockMutex(&layerArray[i].shiftMutex);
	  (layerArray[i].map)->updateVehicleLoc(m_state.Northing, m_state.Easting);
	  DGCunlockMutex(&layerArray[i].shiftMutex);
	};
	_infoFusionMapper.endLockTimer();
	
	fusionCorridorPainter->paintChanges(exposedRow, exposedCol);
      }
      _infoFusionMapper.endStage1Timer();
      //Fuse new cells:
      sparrowTimeLockFusionMapper = (costFuser->fuseChangesCost(NEcoord(m_state.Northing, m_state.Easting)))/1000.0;
      _infoFusionMapper.endStage2Timer();
      
      unsigned long long timestamp;
      DGCgettime(timestamp);
      
      
      CDeltaList* deltaList = NULL;
      
      //send all newly fused cells:
      if(mapperOpts.optKF) {
	deltaList = fusionMap.serializeDelta<double>(layerID_fusedElev, timestamp);	
	if(!deltaList->isShiftOnly()) {
	  SendMapdelta(socket_num, deltaList);
	  deltaSize = 0;
	  for(int i=0; i<deltaList->numDeltas; i++) {
	    deltaSize += deltaList->deltaSizes[i];
	  }
	  _infoFusionMapper.endProcessTimer(deltaSize);	
	  fusionMap.resetDelta<double>(layerID_fusedElev);
	}
	
	deltaList = fusionMap.serializeDelta<double>(layerID_fusedVar, timestamp);	
	if(!deltaList->isShiftOnly() && mapperOpts.sendRDDF == 1) {
	  SendMapdelta(socketMapDeltaCorridor, deltaList);
	  fusionMap.resetDelta<double>(layerID_fusedVar);
	}
	
      } else {
	deltaList = fusionMap.serializeDelta<double>(layerID_costFinal, timestamp);	
	if(!deltaList->isShiftOnly()) {
	  SendMapdelta(socket_num, deltaList);
	  deltaSize = 0;
	  for(int i=0; i<deltaList->numDeltas; i++) {
	    deltaSize += deltaList->deltaSizes[i];
	  }
	  _infoFusionMapper.endProcessTimer(deltaSize);


	  //------------------------	
	  //ofstream dataFile(dataFilename, ios::app);
	  //dataFile << _infoFusionMapper.processTime << ' ' << _infoFusionMapper.stage1Time << ' ' 
	  //	   << _infoFusionMapper.stage2Time << endl;
	  //dataFile.close();
	  //------------------------

	  fusionMap.resetDelta<double>(layerID_costFinal);
	}
	
	deltaList = fusionMap.serializeDelta<double>(layerID_corridor, timestamp);	
	if(!deltaList->isShiftOnly() && mapperOpts.sendRDDF == 1) {
	  SendMapdelta(socketMapDeltaCorridor, deltaList);
	  fusionMap.resetDelta<double>(layerID_corridor);
	}
      }
      
      DGCunlockMutex(&roadMutex);
      DGCunlockRWLock(&allMapsRWLock);
    } else {
      DGCusleep(50000);
    }
  }
  
  printf("%s [%d]: Active loop quitting\n", __FILE__, __LINE__);
}


void FusionMapper::ReceiveDataThread_ElevFeeder(void* pArg) {

  int index = (int)pArg;
  int mapDeltaSocketElev = m_skynet.listen(layerArray[index].msgTypeElev, ALLMODULES);
  int mapDeltaSocketCost = m_skynet.get_send_sock(layerArray[index].msgTypeCost);
  double UTMNorthing;
  double UTMEasting;
  
  CElevationFuser::STATUS overwrittenStatus = CElevationFuser::OK;
  
  unsigned long long timeOptsLastRead = 0;
  unsigned long long tempTimestamp;
  unsigned long long diffTimestamp;
  
  char* pMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      int numreceived = m_skynet.get_msg(mapDeltaSocketElev, pMapDelta, MAX_DELTA_SIZE, 0);
      layerArray[index].statusInfo.startProcessTimer();
      if(layerArray[index].enabled) {
	if(numreceived>0) {
	  DGCreadlockRWLock(&allMapsRWLock);
	  DGClockMutex(&layerArray[index].shiftMutex);
	  layerArray[index].statusInfo.endLockTimer();
	  
	  int numCells = layerArray[index].map->getDeltaSize(pMapDelta);
	  for(int i=0; i<numCells; i++) {
	    CElevationFuser receivedData = layerArray[index].map->getDeltaVal<CElevationFuser>(i, layerArray[index].elevLayerNum, 
											       pMapDelta, &UTMNorthing, &UTMEasting);
	    if(mapperOpts.optKF) {
	      DGClockMutex(&kfMutex);
	      CElevationFuser previousData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedKF, UTMNorthing, UTMEasting);
	      previousData.fuse_KFElevation(receivedData);
	      fusionMap.setDataUTM<CElevationFuser>(layerID_fusedKF, UTMNorthing, UTMEasting, previousData);
	      fusionMap.setDataUTM_Delta<double>(layerID_fusedElev, UTMNorthing, UTMEasting, previousData.getMeanElevation());
	      fusionMap.setDataUTM_Delta<double>(layerID_fusedVar, UTMNorthing, UTMEasting, 1000*previousData.getHeightVar());
	      DGCunlockMutex(&kfMutex);
	    } else {

	      //If we aren't using data association, ignore all incoming sensor data which belong to moving obstacles:
	      if(!mapperOpts.optDA){
		bool movingObst = fusionMap.getDataUTM<bool>(layerID_movingObst, UTMNorthing, UTMEasting);;
		if(!movingObst){
		  CElevationFuser previousData = layerArray[index].map->getDataUTM<CElevationFuser>(layerArray[index].elevLayerNum,
												    UTMNorthing, UTMEasting);
		  overwrittenStatus = previousData.fuse_MeanElevation(receivedData);
		  layerArray[index].map->setDataUTM<CElevationFuser>(layerArray[index].elevLayerNum, UTMNorthing, 
								     UTMEasting, receivedData);
		  layerArray[index].terrainPainter->markChanges(layerArray[index].terrainPainterIndex, UTMNorthing, 
								UTMEasting, overwrittenStatus);
		}
	      }else{
		CElevationFuser previousData = layerArray[index].map->getDataUTM<CElevationFuser>(layerArray[index].elevLayerNum,
												  UTMNorthing, UTMEasting);
		overwrittenStatus = previousData.fuse_MeanElevation(receivedData);
		layerArray[index].map->setDataUTM<CElevationFuser>(layerArray[index].elevLayerNum, UTMNorthing, 
								   UTMEasting, receivedData);
		layerArray[index].terrainPainter->markChanges(layerArray[index].terrainPainterIndex, UTMNorthing, 
							      UTMEasting, overwrittenStatus);
	      }
	    }
	  }
	  layerArray[index].statusInfo.endStage1Timer();
	  
	  sparrowTimeLockFeeder[index] = layerArray[index].terrainPainter->paintChanges(layerArray[index].terrainPainterIndex, layerArray[index].costPainterIndex);
	  DGCgettime(tempTimestamp);
	  diffTimestamp = tempTimestamp - timeOptsLastRead;
	  if(DGCtimetosec(diffTimestamp, true) > 1.0) {
	    timeOptsLastRead = tempTimestamp;
	    layerArray[index].terrainPainter->readOptionsFromFile(layerArray[index].terrainPainterIndex, layerArray[index].optionsFilename);
	    costFuser->_relWeights[layerArray[index].costPainterIndex] = layerArray[index].terrainPainter->_options[layerArray[index].terrainPainterIndex]->relativeWeight;
	  }

	  layerArray[index].statusInfo.endStage2Timer();
	  if(layerArray[index].sendCost) {
	    CDeltaList* deltaList = (layerArray[index].map)->serializeDelta<double>(layerArray[index].costLayerNum, 0);
	    if(!deltaList->isShiftOnly()) {
	      SendMapdelta(mapDeltaSocketCost, deltaList);
	      (layerArray[index].map)->resetDelta<double>(layerArray[index].costLayerNum);
	    }
	  }
	  layerArray[index].statusInfo.endProcessTimer(numreceived);
	  DGCunlockMutex(&layerArray[index].shiftMutex);
	  DGCunlockRWLock(&allMapsRWLock);
	}
      }
    }	 else {
      DGCusleep(50000);
    }
  }
  delete pMapDelta; 
}

void FusionMapper::ReceiveDataThread_Road()
{
  int road_socket = m_skynet.listen(SNroad2map, SNroadfinding);

  bool first_road=true;
  int cur_road=0;
  Road road[2];
  CRoadPainter painter;

  printf("RoadThread started\n");

  int bytes=0;
  int cnt=0;

  while(!_QUIT) {
    //Read road data over skynet, make sure we always get the last transfer
    //so that we don't build up the buffert
    int received;
    do {
      received = m_skynet.get_msg(road_socket, &road[cur_road], sizeof(Road), 0);
      if(received < road[cur_road].data_size()) {
				printf("Error receiving road data\n");
				continue;
      }
      
      bytes +=received;
      ++cnt;
      //printf("Received %i messages, %i bytes, %i road points\n", cnt, bytes, road[cur_road].length);
    } while(m_skynet.is_msg(road_socket));
    
    if(received < road[cur_road].data_size())   //Invalid road data, get next package
      continue;
    
    _infoRoad.startProcessTimer();
    if(mapperOpts.optRoad) {      
      //Paint the trajectory into the map
      DGCreadlockRWLock(&allMapsRWLock);
      DGClockMutex(&roadMutex);
      //myCMap.clearLayer(layerNum[SNGUI_LAYER_ROAD]);
      _infoRoad.endLockTimer();
      _infoRoad.endStage1Timer();
      
      DGCreadlockRWLock(&costFuser->_swapLock);
      if(!first_road) {
	painter.paint(road[cur_road], road[1-cur_road], &roadMap, layerID_road, layerID_roadCounter, costFuser);
      } else {
	painter.paint(road[cur_road], &roadMap, layerID_road, layerID_roadCounter, costFuser);
      }
      DGCunlockRWLock(&costFuser->_swapLock);
      
      first_road=false;
      cur_road = 1-cur_road;
      
      DGCunlockMutex(&roadMutex);
      DGCunlockRWLock(&allMapsRWLock);
      _infoRoad.endStage2Timer();
      _infoRoad.endProcessTimer(received);
    }
  }
}

void FusionMapper::ReceiveDataThread_Supercon() {
  int mapDeltaSocket = m_skynet.listen(SNsuperConMapDelta, MODsupercon);
  double UTMNorthing;
  double UTMEasting;
  NEcoord point;
  double cellval;
  
  m_pSuperconMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      _infoSupercon.startProcessTimer();
      int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pSuperconMapDelta, MAX_DELTA_SIZE, 0);
      if(mapperOpts.optSupercon) {
	if(numreceived>0) {
	  // Begin cutting here?
	  int cellsize = fusionMap.getDeltaSize(m_pSuperconMapDelta);
	  DGCreadlockRWLock(&allMapsRWLock);
	  DGCreadlockRWLock(&costFuser->_swapLock);					
	  _infoSupercon.endLockTimer();
	  _infoSupercon.endStage1Timer();
	  for(int i=0; i<cellsize; i++) {
	    cellval = fusionMap.getDeltaVal<double>(i, layerID_superCon, m_pSuperconMapDelta, &UTMNorthing, &UTMEasting);
	    fusionMap.setDataUTM<double>(layerID_superCon, UTMNorthing, UTMEasting, cellval);
	    point.N = UTMNorthing;
	    point.E = UTMEasting;
	    costFuser->markChangesCost(point);
	    //fusionCostPainter.fuseChanges(UTMNorthing, UTMEasting  );
	  }
	  DGCunlockRWLock(&costFuser->_swapLock);
	  DGCunlockRWLock(&allMapsRWLock);
	  _infoSupercon.endStage2Timer();
	  _infoSupercon.endProcessTimer(numreceived);
	}
      }
    }
    usleep(100);
  }
  delete m_pSuperconMapDelta; 
}


void FusionMapper::ReceiveDataThread_SuperconActions() {
  int mapActionSocket = m_skynet.listen(SNsuperConMapAction, MODsupercon);
  int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
  CDeltaList* deltaList;
  superCon_map_action scCommand;
  unsigned long long timestamp;
  while(!_QUIT) {
    int numBytesRcvd = m_skynet.get_msg(mapActionSocket, &scCommand, sizeof(scCommand), 0);
    if(mapperOpts.optSupercon) {
      if(numBytesRcvd > 0) {
	switch(scCommand.mapActionCmd) {
	case sc_interface::broaden_rddf_one_map:
	  cout << "Received a broaden RDDF request (from superCon) to broaden by " 
	       << scCommand.dblArg << endl;
	  DGCwritelockRWLock(&allMapsRWLock);
	  fusionRDDF.setExtraWidth(scCommand.dblArg);					
	  widerRDDF.setExtraWidth(scCommand.dblArg+mapperOpts.optWingWidth);
	  fusionCorridorPainter->repaintAll();
	  for(int i=0; i < NUM_ELEV_LAYERS; i++) {
	    layerArray[i].map->clearLayer(layerArray[i].costLayerNum);
	    layerArray[i].terrainPainter->readOptionsFromFile(layerArray[i].terrainPainterIndex, layerArray[i].optionsFilename);
	    layerArray[i].terrainPainter->repaintAll(layerArray[i].terrainPainterIndex,
						     layerArray[i].costPainterIndex);
	  }
	  costFuser->markChangesCostAll();
	  costFuser->fuseChangesCost(NEcoord(m_state.Northing, m_state.Easting));										
	  fusionRDDF.setExtraWidth(0.0);
	  
	  widerRDDF.setExtraWidth(mapperOpts.optWingWidth);
	  
	  DGCgettime(timestamp);	
	  
	  deltaList = NULL;
	  
	  deltaList = fusionMap.serializeFullMapDelta<double>(layerID_costFinal, timestamp);	
	  if(!deltaList->isShiftOnly()) {
	    SendMapdelta(socket_num, deltaList);
	    fusionMap.resetDelta<double>(layerID_costFinal);
	  }
	  
	  DGCunlockRWLock(&allMapsRWLock);					
	  scMessage( int(sc_interface::broaden_rddf_complete) );  //generic RDDF broaden confirmation
	  cout << "RDDF BROADENED! (at superCon's request)" << endl;
	  break;
	case sc_interface::clear_all_maps_everywhere:
	  DGCwritelockRWLock(&allMapsRWLock);
	  costFuser->clearBuffers();
	  fusionMap.clearMap();
	  roadMap.clearMap();
	  for(int i=0; i < NUM_ELEV_LAYERS; i++) {
	    layerArray[i].map->clearMap();
	  }
	  costFuser->clearBuffers();
	  fusionMap.clearMap();
	  roadMap.clearMap();
	  for(int i=0; i < NUM_ELEV_LAYERS; i++) {
	    layerArray[i].map->clearMap();
	  }
	  fusionCorridorPainter->repaintAll();
	 
	  DGCgettime(timestamp);	
	  
	  deltaList = NULL;
	  
	  deltaList = fusionMap.serializeFullMapDelta<double>(layerID_costFinal, timestamp);	
	  if(!deltaList->isShiftOnly()) {
	    SendMapdelta(socket_num, deltaList);
	    fusionMap.resetDelta<double>(layerID_costFinal);
	  }
	  
	  DGCunlockRWLock(&allMapsRWLock);					
	  scMessage( int(sc_interface::clear_maps_complete) ); //generic clear-maps confirmation
	  cout << "MAPS CLEARED! - (at superCon's request)" << endl;
	  break;
	default:
	  break;
	}
      }
    }
  }
}


void FusionMapper::PlannerListenerThread() { 
  int mapDeltaSocket = m_skynet.listen(SNfullmaprequest, SNplanner);
  int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
  bool bRequestMap;
  while(!_QUIT)
    {
      int numreceived =  m_skynet.get_msg(mapDeltaSocket, &bRequestMap, MAX_DELTA_SIZE, 0);
      if(numreceived>0) {
	if(bRequestMap == true) {
	  unsigned long long timestamp;
	  DGCgettime(timestamp);
	  DGCwritelockRWLock(&allMapsRWLock);
	  CDeltaList* deltaList = fusionMap.serializeFullMapDelta<double>(layerID_costFinal, timestamp);
	  if(!SendMapdelta(socket_num, deltaList))
	    cerr << "FusionMapper::PlannerListenerThread(): couldn't send delta" << endl;
	  else
	    fusionMap.resetDelta<double>(layerID_costFinal);
	  
	  DGCunlockRWLock(&allMapsRWLock);
	} 
      }
    }
}

void FusionMapper::ReceiveDataThread_MovingObstacles(){
  cerr << "Entering the Moving Obstacle Thread!"<<endl;
  DGCcreateMutex(&movingMutex);
  int mapMovingSocket = m_skynet.listen(SNmovingObstacle, MODtrackMO);
  movingObstacle newObstacle;
  while(!_QUIT){
    int numreceived = m_skynet.get_msg(mapMovingSocket, &newObstacle, sizeof(movingObstacle), 0);
    if(numreceived>0){
      //is it a new obstacle we have received? If so, add it to the moving obstacle map.
      if(newObstacle.status == 1){

	DGClockMutex(&movingMutex);
	costFuser->updateNewMoving(newObstacle, true);
	DGCunlockMutex(&movingMutex);

	//is it an active obstacle we have received?
      }else if(newObstacle.status ==2){

	//is this the second time we receive information about this obstacle?
	//if so, remove all old positions from the map and add the new.
	if(obstacleList[newObstacle.ID].status == 1){
	  DGClockMutex(&movingMutex);	  
	  if(mapperOpts.optDA){
	    costFuser->subtractMovingRec(obstacleList[newObstacle.ID].cornerPos[0]);
	  }else{
	    costFuser->updateNewMoving(obstacleList[newObstacle.ID], false);
	  }
	  
	  costFuser->updateActiveMoving(newObstacle, true);
	  DGCunlockMutex(&movingMutex);

	  //was the obstacle active the last time we received information about it?
	  //if so, update its location.
	}else if(obstacleList[newObstacle.ID].status == 2){
 
	  DGClockMutex(&movingMutex);
	  if(mapperOpts.optDA){
	    costFuser->subtractMovingRec(obstacleList[newObstacle.ID].cornerPos[0]);
	  }else{
	    costFuser->updateActiveMoving(obstacleList[newObstacle.ID], false);
	  }
	  costFuser->updateActiveMoving(newObstacle, true);
	  DGCunlockMutex(&movingMutex);

	  //is the obstacle status active even though it's the first time we receive information 
	  //about it? If so, add all old positions to the map.
	}else{ 
	  DGClockMutex(&movingMutex);
	  costFuser->updateNewMoving(newObstacle, true);
	  DGCunlockMutex(&movingMutex);
	}

	//Has the obstacle left the map?
      }else{
 
	DGClockMutex(&movingMutex);
	if(mapperOpts.optDA){
	  costFuser->subtractMovingRec(obstacleList[newObstacle.ID].cornerPos[0]);
	}else{
	  costFuser->updateActiveMoving(obstacleList[newObstacle.ID],false);
	}
	DGCunlockMutex(&movingMutex);
      }

      //update the moving obstacle list.
      memcpy(&obstacleList[newObstacle.ID], &newObstacle, sizeof(movingObstacle));

      //If we aren't using data association, print some debugging info for Laura:
      if(!mapperOpts.optDA){
	cerr << "----------------------" << endl;
	cerr << "Corner position: (" << newObstacle.cornerPos[0].E << "," 
	     << newObstacle.cornerPos[0].N << ")" << endl; 
	cerr << " ObstacleID: " << newObstacle.ID << " TimeStamp: " << newObstacle.timeStamp << endl;
	cerr << " Obstacle sides: (" << newObstacle.side1.N << "," << newObstacle.side1.E 
	     << "), ("  << newObstacle.side2.N << "," << newObstacle.side2.E << ")" << endl;
      }
    }
  }
}


void FusionMapper::ReceiveDataThread_EStop() {
  //int oldStatus = -1;
  _PAUSE=true;
  while(!_QUIT) {
    WaitForNewActuatorState();
		
    if(!_PAINTWHILEPAUSED){
      
      if(m_actuatorState.m_about_to_unpause == true || 
	 (m_actuatorState.m_estoppos == 2 && m_state.Speed2() > 0.1)){		
	_PAUSE = false;
      }
      else{
	_PAUSE = true;
      }
    }
    else{
      _PAUSE = false;
    }
  }
}


void FusionMapper::ReadConfigFile() {
  FILE* configFile = NULL;
  char fieldBuffer[512];
  char fieldName[256];
  char fieldValue[256];

  configFile = fopen(mapperOpts.configFilename, "r");
  if(configFile != NULL) {
    while(!feof(configFile)) {
      fgets(fieldBuffer, 512, configFile);
      if(strncmp(fieldBuffer, "%", 1)!=0) {
	sscanf(fieldBuffer, "%s %s\n", fieldName, fieldValue);
	if(strcmp(fieldName, "Max_Speed:")==0) {
	  costFuserOpts.maxSpeed = atof(fieldValue);
	  _corridorOptsReference.optMaxSpeed = atof(fieldValue);
	} else if(strcmp(fieldName, "RDDF_Speed_Scaling:")==0) {
	  _corridorOptsReference.optRDDFscaling = atof(fieldValue);
	  _corridorOptsFinalCost.optRDDFscaling = 1.0;
	} else if(strcmp(fieldName, "Inside_RDDF_No_Data_Speed:")==0) {
	  _corridorOptsFinalCost.optMaxSpeed = atof(fieldValue);
	} else if(strcmp(fieldName, "Max_Cells_to_Interp:")==0) {
	  costFuserOpts.maxCellsToInterpolate = atoi(fieldValue);
	} else if(strcmp(fieldName, "Min_Cells_for_Interp:")==0) {
	  costFuserOpts.minNumSupportingCellsForInterpolation = atoi(fieldValue);
	} else if(strcmp(fieldName, "Paint_Road_Without_Elev:")==0) {
	  costFuserOpts.paintRoadWithoutElevation = atoi(fieldValue);
	} else if(strcmp(fieldName, "Paint_While_Paused:")==0) {
	  _PAINTWHILEPAUSED = atoi(fieldValue);
	} else if(strcmp(fieldName, "Wing_Width:")==0) {
	  mapperOpts.optWingWidth = atof(fieldValue);
	} else {
	  printf("%s [%d]: Unknown field '%s' with value '%s' - pausing\n", __FILE__, __LINE__, fieldName, fieldValue);
	}
      }
    }
    fclose(configFile);
  }

}


void FusionMapper::WriteConfigFile() {
  FILE* configFile = NULL;

  configFile = fopen(mapperOpts.configFilename, "w");
  if(configFile != NULL) {
    fprintf(configFile, "%% FusionMapper Aggressiveness Tuning Configuration File\n");
    fprintf(configFile, "RDDF_Speed_Scaling: %lf\n", _corridorOptsReference.optRDDFscaling);
    fprintf(configFile, "Inside_RDDF_No_Data_Speed: %lf\n", _corridorOptsFinalCost.optMaxSpeed);
    fprintf(configFile, "Max_Speed: %lf\n", costFuserOpts.maxSpeed);
    fprintf(configFile, "Max_Cells_to_Interp: %d\n", costFuserOpts.maxCellsToInterpolate);
    fprintf(configFile, "Min_Cells_for_Interp: %d\n", costFuserOpts.minNumSupportingCellsForInterpolation);
    fprintf(configFile, "Paint_Road_Without_Elev: %d\n", costFuserOpts.paintRoadWithoutElevation);
    fprintf(configFile, "Paint_While_Paused: %d\n", _PAINTWHILEPAUSED);
    fprintf(configFile, "Wing_Width: %lf\n", mapperOpts.optWingWidth);
    fclose(configFile);
  }
}
