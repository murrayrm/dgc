#ifndef MOVINGOBSTACLE_HH
#define MOVINGOBSTACLE_HH

#include "frames/coords.hh"
#define dot(a,b) ((a)[0]*(b)[0] + (a)[1]*(b)[1])
#define NBR_OF_SAMPLES 10       //The length of the vectors in the movingObstacle struct
#define MAX_NBR_MOVING 100     //The max number of moving obstacle in the map

/**
The movingObstacle struct is the format used to send the moving obstacle information across skynet.
cornerPos contains the position of one corner of the car, side1 and side2 contains two sides of the car as vectors.
obstacleStatus contains the status of the moving obstacle: 
1 = 'new obstacle'
2 = 'active
3 = 'has left the map'

The elements in cornerPos, side1 and side2 contains the 10 last positions of the moving obstacle, the first element 
contains the current position. timeStamp is the time when the information in the first position of the vectors were valid.
We will later add the prediction information to this struct as well.
*/
struct movingObstacle{
  NEcoord cornerPos[NBR_OF_SAMPLES]; 
  NEcoord side1, side2;
  int status, ID; 
  unsigned long long timeStamp;
  NEcoord vel;
};

enum MO_STATUS{INACTIVE, NEW, ACTIVE, OUTSIDE_MAP};

#endif


