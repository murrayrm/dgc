SUPERCON_PATH = $(DGC)/projects/joshuaf_ft

SUPERCON_DEPEND_MODULES = \
        $(TIMBERLIB) \
        $(DGCUTILS) \
        $(ADRIVELIB) \
        $(MODULEHELPERSLIB) \
        $(CMAPLIB) \
	$(SPARROWHAWKLIB) \
	$(SPARROWLIB) \
        $(SKYNETLIB) \
        $(RDDFLIB) \
        $(GGISLIB)

SUPERCON_DEPEND_SOURCES = \
        $(SUPERCON_PATH)/Diagnostic.cc \
        $(SUPERCON_PATH)/Diagnostic.hh \
        $(SUPERCON_PATH)/StateServer.cc \
        $(SUPERCON_PATH)/StateServer.hh \
        $(SUPERCON_PATH)/Strategy.cc \
        $(SUPERCON_PATH)/Strategy.hh \
        $(SUPERCON_PATH)/StrategyGPSreAcq.cc \
        $(SUPERCON_PATH)/StrategyGPSreAcq.hh \
        $(SUPERCON_PATH)/StrategyHelpers.hh \
        $(SUPERCON_PATH)/StrategyInterface.cc \
        $(SUPERCON_PATH)/StrategyInterface.hh \
        $(SUPERCON_PATH)/StrategyLoneRanger.cc \
        $(SUPERCON_PATH)/StrategyLoneRanger.hh \
        $(SUPERCON_PATH)/StrategyLturnReverse.cc \
        $(SUPERCON_PATH)/StrategyLturnReverse.hh \
        $(SUPERCON_PATH)/StrategyNominal.cc \
        $(SUPERCON_PATH)/StrategyNominal.hh \
        $(SUPERCON_PATH)/StrategySlowAdvance.cc \
        $(SUPERCON_PATH)/StrategySlowAdvance.hh \
        $(SUPERCON_PATH)/StrategyUnseenObstacle.cc \
        $(SUPERCON_PATH)/StrategyUnseenObstacle.hh \
        $(SUPERCON_PATH)/StrategyEstopPausedNOTsuperCon.cc \
        $(SUPERCON_PATH)/StrategyEstopPausedNOTsuperCon.hh \
        $(SUPERCON_PATH)/StrategyPlnFailedNoTraj.cc \
        $(SUPERCON_PATH)/StrategyPlnFailedNoTraj.hh \
        $(SUPERCON_PATH)/StrategyEndOfRDDF.cc \
        $(SUPERCON_PATH)/StrategyEndOfRDDF.hh \
        $(SUPERCON_PATH)/StrategyOutsideRDDF.cc \
        $(SUPERCON_PATH)/StrategyOutsideRDDF.hh \
        $(SUPERCON_PATH)/SuperCon.hh \
        $(SUPERCON_PATH)/SuperConClient.cc \
        $(SUPERCON_PATH)/SuperConClient.hh \
        $(SUPERCON_PATH)/SuperConHelpers.hh \
        $(SUPERCON_PATH)/SuperConInterface.cc \
        $(SUPERCON_PATH)/SuperConInterface.hh \
        $(SUPERCON_PATH)/SuperConMain.cc \
        $(SUPERCON_PATH)/bool_counter.hh \
        $(SUPERCON_PATH)/bool_latched.hh \
        $(SUPERCON_PATH)/cbuffer.hh \
        $(SUPERCON_PATH)/interface_superCon_adrive.hh \
        $(SUPERCON_PATH)/interface_superCon_astate.hh \
        $(SUPERCON_PATH)/interface_superCon_pln.hh \
        $(SUPERCON_PATH)/interface_superCon_superCon.hh \
        $(SUPERCON_PATH)/interface_superCon_trajF.hh \
        $(SUPERCON_PATH)/sc_specs.h \
        $(SUPERCON_PATH)/SuperConLog.cc \
        $(SUPERCON_PATH)/SuperConLog.hh \
        $(SUPERCON_PATH)/stage_iterator.hh \
        $(TRAJFOLLOWER_PATH)/trajF_speedCap_cmd.hh \
        $(TRAJFOLLOWER_PATH)/trajF_status_struct.hh


SUPERCON_DEPEND_SCENARIOS = \
        $(SUPERCON_PATH)/test_scenarios/rosebowl_blockedRDDF/map_rosebowlBlockedRDDF.pgm \
        $(SUPERCON_PATH)/test_scenarios/rosebowl_blockedRDDF/map_rosebowlBlockedRDDF.hdr \
        $(SUPERCON_PATH)/test_scenarios/rosebowl_blockedRDDF/st_rosebowlBlockedRDDF \
        $(SUPERCON_PATH)/test_scenarios/stluke_dogleg/map_stlukeDoglegBlockedRDDF.pgm \
        $(SUPERCON_PATH)/test_scenarios/stluke_dogleg/map_stlukeDoglegBlockedRDDF.hdr \
        $(SUPERCON_PATH)/test_scenarios/stluke_dogleg/st_stlukeDoglegBlockedRDDF \
        $(SUPERCON_PATH)/test_scenarios/stluke_straight/map_stlukeStraightBlockedRDDF.pgm \
        $(SUPERCON_PATH)/test_scenarios/stluke_straight/map_stlukeStraightBlockedRDDF.hdr \
        $(SUPERCON_PATH)/test_scenarios/stluke_straight/st_stlukeStraightBlockedRDDF


SUPERCON_DEPEND = \
        $(SUPERCON_DEPEND_MODULES) \
        $(SUPERCON_DEPEND_SOURCES) \
        $(SUPERCON_DEPEND_SCENARIOS)
