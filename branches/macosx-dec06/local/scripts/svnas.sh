NEW_USERNAME=$1
SVN_PREFIX=svn+ssh://

echo "WARNING: Beta copy: does not support quoted arguments"

URL_CHECK=`svn info | grep URL: | grep @`

if svn info | grep URL: | grep @ > /dev/null; then
    echo "found @";
    SVN_USERNAME=`svn info | grep URL: | sed "s/URL: svn+ssh:\\/\\/\(.*\)@\(.*\)/\1/"`
    SVN_URL=`svn info | grep URL: | sed "s/URL: svn+ssh:\\/\\/\(.*\)@\(.*\)/\2/"`
    SVN_FULLURL=$SVN_PREFIX$SVN_USERNAME@$SVN_URL
else
    echo "no @"
    SVN_USERNAME=
    SVN_URL=`svn info | grep URL: | sed "s/URL: svn+ssh:\\/\\/\(.*\)/\1/"`
    SVN_FULLURL=$SVN_PREFIX$SVN_URL
fi

NEW_FULLURL=$SVN_PREFIX$NEW_USERNAME@$SVN_URL


#echo "SVN_USERNAME= $SVN_USERNAME"
#echo "SVN_URL= $SVN_URL"

#echo $SVN_FULLURL
#echo $NEW_FULLURL

#echo $#
#echo "*=$*"
#echo "@=$@"
#echo "$@"
#echo ${@:2:$#}

#echo $*

export CMD=
for i; do export CMD=`echo -n "$CMD\"$i\" "`; done
#echo
#for i in ${@:2:$#}; do echo -n "\"$i\" "; done
#for i in ${@:2:$#}; do export CMD=`echo -n "$CMD\"$i\" "`; done
#echo
export CMD=`echo "$CMD" | sed "s/\"$1\" //"`

echo "*** Using user $NEW_USERNAME ***"
svn switch --relocate $SVN_FULLURL $NEW_FULLURL
#echo "Running svn $CMD"
#export CMD=\"info\"
svn ${@:2:$#}
echo "*** Returing to user $SVN_USERNAME (Do NOT CTRL-C) ***"
svn switch --relocate $NEW_FULLURL $SVN_FULLURL
#echo "Done."
