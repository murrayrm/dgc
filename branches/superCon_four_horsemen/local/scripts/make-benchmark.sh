#!/bin/bash

# This script downloads/updates the current 
# repository and runs the necessary coompilations 
# to run the benchmarks
#
# Created: Nov 14, 2004
# Author: Henry Barnor
#TODO
# will need some awk to replace yacc with bison for gentoo systems

# first go to user home directory
cd ~/
#check if dgc directory exits and update else ceckout
if [ -f ~/dgc ]; then 
    cd ~/dgc
    svn up 
else
    svn co svn+ssh://team@gc/dgc/subversion/dgc/trunk dgc
    cd dgc
fi

#run make in dgc
make clean 
make
make install
#now do same in bob 
cd ~/dgc/bob
make clean 
make
make install
# same in planner CCorridorPainter
cd ~/dgc/projects/CCorridorPainter
make clean 
make 
make install
# same in traj
cd ~/dgc/projects/traj
make clean 
make 
make install
#final make
cd ~/dgc/projects/planner
make clean 
make 
make benchmark

echo "System Ready for Benchmarking"