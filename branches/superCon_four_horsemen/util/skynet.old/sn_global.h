/* this is a first draft of the shared global variables api */

/** \function int sn_register
 *  sn_register must be called before any of these functions can be used.
 *  it is defined in sn_msg.h
 */

/** \function int sn_unregister
 *  sn_unregister tells skynetd that a particular module is quitting
 *  it is not strictly neccesary to call this, but doing so will prevent
 *  memory leaks in skynetd. return 0 on success, -1 on error.
*/

/** sn_global_map maps a region of shared memory created by skynetd */
void* sn_global_map(key);

/** sn_add_global tells skynetd to allocate some space for name, without
  * initializing it.  return value is 0 on success, -1 on error.
*/
int sn_add_global(char* name, size_t varsize);

/** sn_write_global writes var and skynetd then pushes it out to the other
  * skynetd instances.  returns 0 on success, -1 on error. */
int sn_write_global(void* var, char* name, size_t size);

/** sn_read_global returns a pointer to a global variable within the shared
  * memory region.  sn_global_map must have already been called.  returns null
  * on error
*/
void* sn_read_global(char* name);

/** sn_listen_global tells skynetd to begin updating the variable name.
    it is neccesary to call this before using sn_read_global .  returns 0 on
    success, -1 on error.
*/
int sn_listen_global(char* name);

/** sn_unlisten_global tells skynetd to stop updating variable name.  It is not
    strictly neccesary to ever use this call, but doing so may improve system
    performance.  returns 0 on success, -1 on error.
*/
int sn_unlisten_global(char* name);

/** sn_remove_global tells skynetd to deallocate variable name and discontinue
    updating it.  It is not strictly neccesary to use this call, but doing so
    may improve system performance.
*/
int sn_remove_global(char* name);

/** tells skynetd to reallocate space for variable name.  returns 0 on success,
    -1 on error.
*/
int sn_change_global(char* name, size_t size);
