# This is a typical Unix makefile for maintaining variants of SNOPT.
# It should be called "makefile".
#
# 04 March 1993: Original version written by David Gay and Philip Gill
#                for a DECstation 3100 with the DEC RISC F77 compiler.
#                The FFLAGS compiler options will be machine-dependent.
#
# This makefile is for SGI f77 Optimized

SHELL   = /bin/sh
#FFLAGS  = -g
FFLAGS = -O3  -msse2 -mfpmath=sse -maccumulate-outgoing-args -fforce-mem -fforce-addr	\
-ffast-math -fstrength-reduce -frerun-cse-after-loop -fexpensive-optimizations -fschedule-insns -fschedule-insns2 \
-fcaller-saves -funroll-loops

SRC     = 
F77     = g77
.SUFFIXES: .c .f .o
.f.o:
	$(F77) -c $(FFLAGS) $*.f

SNOPT  = double.f   sn09time.f sn10unix.f sn11user.f\
         sn12snzz.f sn17util.f sn20amat.f\
         sn25bfac.f sn27LU.f   sn30spec.f sn35mps.f  sn40bfil.f\
         sn50lp.f   sn55qp.f   sn60srch.f sn65rmod.f sn70nobj.f\
         sn80ncon.f sn85Hess.f sn87sopt.f sn90lmqn.f sn95fmqn.f

SNOPTM = double.f   sn09time.f sn10unix.f sn11user.f\
         sn12snmz.f  sn17util.f sn20amat.f\
         sn25bfac.f sn27LU.f   sn30spec.f sn35mps.f  sn40bfil.f\
         sn50lp.f   sn55qp.f   sn60srch.f sn65rmod.f sn70nobj.f\
         sn80ncon.f sn85Hess.f sn87sopt.f sn90lmqn.f sn95fmqn.f

CSNOPT = double.f   sn09time.f sn10unix.f sn11user.f\
         sn12snzz.f  sn17util.f sn20amat.f\
         sn25bfac.f sn27LU.f   sn30spec.f            sn40bfil.f\
         sn50lp.f   sn55qp.f   sn60srch.f sn65rmod.f sn70nobj.f\
         sn80ncon.f sn85Hess.f sn87sopt.f sn90lmqn.f sn95fmqn.f

ASNOPT = double.f   sn09time.f sn10ampl.f sn11user.f\
         sn12snzz.f  sn17util.f sn20amat.f\
         sn25bfac.f sn27LU.f   sn30spec.f sn35mps.f  sn40bfil.f\
         sn50lp.f   sn55qp.f   sn60srch.f sn65rmod.f sn70nobj.f\
         sn80ncon.f sn85Hess.f sn87sopt.f sn90lmqn.f sn95fmqn.f

NPOPT  = double.f   sn09time.f sn10unix.f sn11user.f\
         sn12npzz.f  sn17util.f sn20amat.f\
         sn25bfac.f sn27LU.f   sn30spec.f            sn40bfil.f\
         sn50lp.f   sn55qp.f   sn60srch.f sn65rmod.f sn70nobj.f\
         sn80ncon.f sn85Hess.f sn87sopt.f sn90lmqn.f sn95fmqn.f

CNPOPT = double.f   sn09time.f sn10unix.f sn11user.f\
         sn12npzz.f  sn17util.f sn20amat.f\
         sn25bfac.f sn27LU.f   sn30spec.f            sn40bfil.f\
         sn50lp.f   sn55qp.f   sn60srch.f sn65rmod.f sn70nobj.f\
         sn80ncon.f sn85Hess.f sn87sopt.f sn90lmqn.f sn95fmqn.f

SQOPT  = double.f   sn09time.f sn10unix.f\
         sn12sqzz.f  sn17util.f sn20amat.f\
         sn25bfac.f sn27LU.f   sn30spec.f sn35mps.f  sn40bfil.f\
         sn50lp.f   sn55qp.f   sn57qopt.f sn65rmod.f

libsnopt.a: $(SNOPT)
	for i in $?; do  $(F77) -c $(FFLAGS) $$i; done
	ar ruv libsnopt.a `echo $? | sed 's/\.[cf]/.o/g'`
	rm `echo $? | sed 's/\.[cf]/.o/g'`
	ranlib libsnopt.a || true

libsnoptm.a: $(SNOPTM)
	for i in $?; do  $(F77) -c $(FFLAGS) $$i; done
	ar ruv libsnoptm.a `echo $? | sed 's/\.[cf]/.o/g'`
	rm `echo $? | sed 's/\.[cf]/.o/g'`
	ranlib libsnoptm.a || true

libamplsnopt.a: $(ASNOPT)
	for i in $?; do  $(F77) -c $(FFLAGS) $$i; done
	ar ruv libamplsnopt.a `echo $? | sed 's/\.[cf]/.o/g'`
	rm `echo $? | sed 's/\.[cf]/.o/g'`
	ranlib libamplsnopt.a || true

libcutesnopt.a: $(CSNOPT)
	for i in $?; do  $(F77) -c $(FFLAGS) $$i; done
	ar ruv libcutesnopt.a `echo $? | sed 's/\.[cf]/.o/g'`
	rm `echo $? | sed 's/\.[cf]/.o/g'`
	ranlib libcutesnopt.a || true

libnpopt.a: $(NPOPT)
	for i in $?; do  $(F77) -c $(FFLAGS) $$i; done
	ar ruv libnpopt.a `echo $? | sed 's/\.[cf]/.o/g'`
	rm `echo $? | sed 's/\.[cf]/.o/g'`
	ranlib libnpopt.a || true

libcutenpopt.a: $(CNPOPT)
	for i in $?; do  $(F77) -c $(FFLAGS) $$i; done
	ar ruv libcutenpopt.a `echo $? | sed 's/\.[cf]/.o/g'`
	rm `echo $? | sed 's/\.[cf]/.o/g'`
	ranlib libcutenpopt.a || true

libsqopt.a: $(SQOPT)
	for i in $?; do  $(F77) -c $(FFLAGS) $$i; done
	ar ruv libsqopt.a `echo $? | sed 's/\.[cf]/.o/g'`
	rm `echo $? | sed 's/\.[cf]/.o/g'`
	ranlib libsqopt.a || true

clean:
	rm -f *.o
