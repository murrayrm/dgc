# c is 4xN, d is NxN. we want to compute c*d. this is a 4xN matrix.
# however, i want to represent this as M*dr
# where dr is d represented as a N^2 long vector. M is thus a 4NxN^2 matrix.

# we also want a similar matrix for d*c', where THIS d is 4xN

M_cd  = [];
M_dcp = [];

for i=1:N
	M_cd  = [M_cd ; zeros(4,N*(i-1)) c zeros(4,N*(N-i))];
end

for i=1:N

	col = [];

	for j=1:4
		cell = c(j,i)*eye(4);
		col = [col; cell];
	end

	M_dcp = [M_dcp col];
end
