#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <iostream.h>
#include "rddf.hh"
#include <CMap/CMapPlus.hh>
#include "../../MTA/Kernel/Misc/Time/Timeval.hh"
#include "frames/frames.hh"

int main (int argc,char* argv[]){
  //system("clear");
  int ind, total,ret, waypointNumber;
  RDDF rddf("rddf.dat");
  RDDFVector targetPoints;
  char * bobfile;
  double Northing,Easting;
  CMapPlus myMap;
  int numRows=250;
  int numCols = 250;
  double resRows =2;
  double resCols =2;

  //myMap.initMap(10000.753, 499015.283, numRows, numCols, resRows, resCols, 0);
  myMap.initMap(3846508.753+375, 499015.283-250, numRows, numCols, resRows, resCols, 0);
  //myMap.initMap(3846508.753, 499015.28, numRows, numCols, resRows, resCols, 0);

  printf("Game\n");

  int rddfLayerID = myMap.addLayer<double>(0, -2);

  //myMap.initLayerDisplay(rddfLayerID);
  //myMap.startLayerDisplay(rddfLayerID);


  /*
  for(int row=0; row<numRows; row++) {
    for(int col=0; col<numCols; col++) {
      double tempVal = myMap.getDataWin<double>(rddfLayerID, row, col);
      myMap.Win2UTM(row, col, &Northing, &Easting);
      myMap.setDataWin<double>(rddfLayerID, row, col, rddf.isPointInCorridor(Northing, Easting));
      //tempVal = rddf.isPointInCorridor(Northing, Easting);
    }
  }
  */
  double blN, blE, trN, trE;
  double wblN, wblE, wtrN, wtrE;
  int blR, blC, trR, trC;
  int r, c;
  double tempa, tempb;
  int retval, fout, bout;
  Timeval before, after;

  wblN = myMap.getWindowBottomLeftUTMNorthing();
  wblE = myMap.getWindowBottomLeftUTMEasting();
  wtrN = myMap.getWindowTopRightUTMNorthing() - myMap.getResRows()/2;
  wtrE = myMap.getWindowTopRightUTMEasting() - myMap.getResCols()/2;

  before = TVNow();

  //fout = rddf.getWaypointNumAheadOut(20, wblN, wblE, wtrN, wtrE);
  //bout = rddf.getWaypointNumBehindOut(20, wblN, wblE, wtrN, wtrE);
  //printf("%d is next, %d is fout, %d is bout\n", retval, fout, bout);


  for(int i=0; i<=rddf.getNumTargetPoints(); i++) {
    retval = rddf.getCorridorSegmentBoundingBoxUTM(i, wblN, wblE, wtrN, wtrE,
					  blN, blE, trN, trE);
    myMap.UTM2Win(blN, blE, &blR, &blC);
    myMap.UTM2Win(trN, trE, &trR, &trC);
    if(retval >= 0) {
      //printf("%d (is a %d) goes from (%d, %d) to (%d, %d)\n",
      //i, retval, blR, blC, trR, trC);
      for(int row=blR; row <= trR; row++) {
	for(int col=blC; col <= trC; col++) {
	  myMap.Win2UTM(row, col, &Northing, &Easting);
	  if(myMap.getDataWin<double>(rddfLayerID, row, col) != -1) {
	    if(rddf.isPointInCorridor(i, Northing, Easting)) {
	      myMap.setDataWin<double>(rddfLayerID, row, col,
				       rddf.isPointInCorridor(i, Northing, Easting));
	    }
	  }
	}
      }
      //tempa = rddf.getWaypointNorthing(i);
      //tempb = rddf.getWaypointEasting(i);
      //myMap.setDataUTM<double>(rddfLayerID, tempa, tempb, 1.0);
    }
    //printf("%d: (%lf, %lf) to (%lf, %lf)\n", i, blN, blE, trN, trE);
    //myMap.setDataUTM<double>(rddfLayerID, blN, blE, -1.0);
    //myMap.setDataUTM<double>(rddfLayerID, trN, trE, -1.0);
  }
  after = TVNow();
  printf("Took %lf\n", (after-before).dbl());

  before = TVNow();
  rddf.getCurrentWaypointNumber(0,0);
  after = TVNow();
  printf("get cur way num Took %lf\n", (after-before).dbl());

  printf("initial view\n");
  //myMap.refreshLayerDisplay<double>(rddfLayerID);
  //sleep(15);

  myMap.updateVehicleLoc(3846508.753-10, 499015.283+10);

  printf("shifted view\n");
  //myMap.refreshLayerDisplay<double>(rddfLayerID);
  //sleep(15);

  /*
  long int origBLRow, origBLCol, newBLRow, newBLCol;
  origBLRow = myMap.getWindowBottomLeftUTMNorthingRowResMultiple();
  origBLCol = myMap.getWindowBottomLeftUTMEastingColResMultiple();

  myMap.updateVehicleLoc(3846508.753+375, 499015.283-250);


  newBLRow = myMap.getWindowBottomLeftUTMNorthingRowResMultiple();
  newBLCol = myMap.getWindowBottomLeftUTMEastingColResMultiple();

  int deltaRow, deltaCol;
  deltaRow = newBLRow - origBLRow;
  deltaCol = newBLCol - origBLCol;
  printf("Delta %d, %d\n", deltaRow, deltaCol);

  int rbBLr, rbBLc, rbTRr, rbTRc;
  int cbBLr, cbBLc, cbTRr, cbTRc;
  int bbBLr, bbBLc, bbTRr, bbTRc;

  if(deltaRow>=0) {
    //we moved north, empty area is at top
    bbBLr = myMap.getNumRows()-deltaRow;
    bbTRr = myMap.getNumRows() - 1;

    rbBLr = myMap.getNumRows() - deltaRow;
    rbTRr = myMap.getNumRows() - 1;
    cbBLr = 0;
    cbTRr = myMap.getNumRows() - deltaRow;
  } else {
    //moved south, empty at bottom
    bbBLr = 0;
    bbBLr = -1*deltaRow;
    rbBLr = 0;
    rbTRr = -1*deltaRow;
    cbBLr = -1*deltaRow;
    cbTRr = myMap.getNumRows() -1;
  }

  if(deltaCol>=0) {
    //we moved east, empty area is at right
    bbBLc = myMap.getNumCols() - deltaCol;
    bbTRc = myMap.getNumCols() -1;

    cbBLc = myMap.getNumCols() - deltaCol;
    cbTRc = myMap.getNumCols() - 1;
    rbBLc = 0;
    rbTRc = myMap.getNumCols() - deltaCol;
  } else {
    //moved west, empty at left
    bbBLc = 0;
    bbTRc = -1*deltaCol;

    cbBLc = 0;
    cbTRc = -1*deltaCol;
    rbBLc = -1*deltaCol;
    rbTRc = myMap.getNumCols()-1;
  }

  printf("(%d, %d) to (%d, %d)\n", rbBLr, rbBLc, rbTRr, rbTRc);

  //for(int r=rbBLr; r <= rbTRr; r++) {
  //for(int c = rbBLc; c<=rbTRc; c++) {
      //myMap.setDataWin<double>(rddfLayerID, r, c, -0.25);

      //wblN = myMap.getWindowBottomLeftUTMNorthing();
      //wblE = myMap.getWindowBottomLeftUTMEasting();
      //wtrN = myMap.getWindowTopRightUTMNorthing() - myMap.getResRows()/2;
      //wtrE = myMap.getWindowTopRightUTMEasting() - myMap.getResCols()/2;

      myMap.Win2UTM(rbBLr, rbBLc, &wblN, &wblE);
      myMap.Win2UTM(rbTRr, rbTRc, &wtrN, &wtrE);
      
      fout = rddf.getWaypointNumAheadOut(20, wblN, wblE, wtrN, wtrE);
      bout = rddf.getWaypointNumBehindOut(20, wblN, wblE, wtrN, wtrE);
      printf("%d is next, %d is fout, %d is bout\n", retval, fout, bout);
      
      
      for(int i=bout; i<=fout; i++) {
	retval = rddf.getCorridorSegmentBoundingBoxUTM(i, wblN, wblE, wtrN, wtrE,
						       blN, blE, trN, trE);
	myMap.UTM2Win(blN, blE, &blR, &blC);
	myMap.UTM2Win(trN, trE, &trR, &trC);
	if(retval >= 0) {
	  printf("%d (is a %d) goes from (%d, %d) to (%d, %d)\n",
		 i, retval, blR, blC, trR, trC);
	  for(int row=blR; row <= trR; row++) {
	    for(int col=blC; col <= trC; col++) {
	      myMap.Win2UTM(row, col, &Northing, &Easting);
	      if(myMap.getDataWin<double>(rddfLayerID, row, col) != 1) {
		//if(rddf.isPointInCorridor(i, Northing, Easting)) {
		myMap.setDataWin<double>(rddfLayerID, row, col,
					 rddf.isPointInCorridor(i, Northing, Easting));
		//}
	      }
	    }
	  }
	  //tempa = rddf.getWaypointNorthing(i);
	  //tempb = rddf.getWaypointEasting(i);
	  //myMap.setDataUTM<double>(rddfLayerID, tempa, tempb, 1.0);
	}
	//printf("%d: (%lf, %lf) to (%lf, %lf)\n", i, blN, blE, trN, trE);
	//myMap.setDataUTM<double>(rddfLayerID, blN, blE, -1.0);
	//myMap.setDataUTM<double>(rddfLayerID, trN, trE, -1.0);
      }






      
      //}
      //}


  for(int r=cbBLr; r <= cbTRr; r++) {
    for(int c = cbBLc; c<=cbTRc; c++) {
      myMap.setDataWin<double>(rddfLayerID, r, c, -0.5);
    }
  }

  for(int r=bbBLr; r <= bbTRr; r++) {
    for(int c = bbBLc; c<=bbTRc; c++) {
      myMap.setDataWin<double>(rddfLayerID, r, c, -0.75);
    }
  }

  double fooa, foob;
  myMap.Win2UTM(0, 0, &fooa, &foob);
  printf("BL at %lf %lf\n", fooa, foob);

  printf("Set\n");
  myMap.refreshLayerDisplay<double>(rddfLayerID);
  sleep(30);

    int imageRow=0;
    int imageCol=0;
    int winRow, winCol;
    double minDouble, maxDouble, valueDouble;
    char valueChar;
    minDouble = 0;
    maxDouble = 0;
    double dptr;

    int _numRows = myMap.getNumRows();
    int _numCols = myMap.getNumCols();

    char *imageBuffer;

    imageBuffer = (char*)malloc(myMap.getNumRows()*myMap.getNumCols());

    for(winRow = 0; winRow < _numRows; winRow++) {
      for(winCol = 0; winCol < _numCols; winCol++) {
	dptr = myMap.getDataWin<double>(rddfLayerID, winRow, winCol);
	if(dptr != -2) {
	  valueDouble = dptr;
	  if(valueDouble < minDouble) minDouble = valueDouble;
	  if(valueDouble > maxDouble) maxDouble = valueDouble;
	}
      }
    }

    printf("MaxDouble: %d, MinDouble: %d\n", maxDouble, minDouble);
    double newValueDouble;
    imageRow = myMap.getNumRows()-1;
    for(winRow = 0; winRow < _numRows; winRow++) {
      imageCol = 0;
      for(winCol = 0; winCol < _numCols; winCol++) {
	valueDouble = 0;
	dptr = myMap.getDataWin<double>(rddfLayerID, winRow, winCol);
	if(dptr != -2) valueDouble = dptr;
	newValueDouble = (((valueDouble-minDouble)*255/(maxDouble-minDouble)));
	valueChar = (char)(int)newValueDouble;
	imageBuffer[imageRow*_numCols + imageCol] = valueChar;
	imageCol++;
      }
      imageRow--;
    }


    FILE* imagefile;
    imagefile=fopen("map.pgm", "w");
    if( imagefile == NULL) {
      fprintf( stderr, "Can't create '%s'", "map.pgm");
    } else {
      fprintf(imagefile,"P5\n%u %u 255\n", _numRows, _numCols);
      fwrite((const char *)imageBuffer, 1, _numRows*_numCols, imagefile);
      fclose(imagefile);
    }


  */
 
  /*************************************************************************/
  /********** DECLARING VARIABLES ******************************************/
  /*************************************************************************/
  // Variales used to get parameters from command line
  int opt, outOfMemory;
  char *argMethod;
  int method,param;

  /*************************************************************************
   ********** GETTING THE PARAMETERS FROM COMMAND LINE			
   **********  CAUTION:						  
   **********    - optind,opterr:  extern variables, do not declare a
   **********	         local variable with any of these names	     
   **********	 - optind: indice of the argv where to find the argument 
   **********		  desired					    
   *************************************************************************/

  // Defining the characters used as delimiter of a command line argument  
  param = 0;
  outOfMemory = 0;
  while ((opt = getopt (argc, argv, "m:")) != -1){
    switch ((unsigned char)opt){
    case 'm':
      param = 1;
      if(optarg != NULL){
	argMethod = (char*) malloc(strlen(optarg)+1);
	if(argMethod == NULL)
	  outOfMemory = 1;
	else
	  strcpy(argMethod,optarg);
      }
      break;
    case '?':
    default:
      system("clear");
      cout << "usage: [-m method]" << endl << endl
           << "1 : load RDDF file test" << endl
           << "2 : create BOB file" << endl
           << "3 : getNextWaypoint" << endl << endl;
      return(-1);
    }    
  }
  
  // If any allocation failed
  if(outOfMemory){
    if(argMethod != NULL) free(argMethod);
    system("clear");
    cout << "Problems with memory management" << endl;
    return(-1);
  }
  
  // Showing the choosen test
  if(param)
    method = atoi(argMethod);
  else
    method = 4;

  switch(method){
  case 1:

    //**********************************************//
    // Testing constructor
    //**********************************************//

    targetPoints = rddf.getTargetPoints();
    total = rddf.getNumTargetPoints();
    for(ind=0; ind<total; ind++){
      targetPoints[ind].display();
    }
    for(double val=0.0; val < 100.0; val+=10.0)
    {
      XYcoord tem;
      XYcoord too(10.0, -10.0);
      XYcoord tee;
      tee = targetPoints[0].xy_coord()+too;
      tem = rddf.getPointAlongTrackLine( tee, val);
      //printf("val: %f, tem.X: %f, tem.Y: %f\n", val, tem.X, tem.Y);
      printf("val: %f, [tem.X tem.Y] = %f  %f\n", val, tem.X, tem.Y);
    } 
    for(double val=0.0; val < 100.0; val+=10.0)
    {
      double temN, temE;
      rddf.getPointOnTrackLineDist(
                      targetPoints[0].Northing+10.0,
                      targetPoints[0].Easting-10.0,val,
                      temN, temE);
      printf("val: %f, [temN  temE ] = %f  %f\n", val, temN, temE);
    }
    break;

  case 2:

    //**********************************************//
    // create BOB file
    //**********************************************//

    targetPoints = rddf.getTargetPoints();
    total = rddf.getNumTargetPoints();
    ret = rddf.createBobFile("rddf.bob");

    break;

  case 3:

    //**********************************************//
    // getNextWaypoint
    //**********************************************//
    waypointNumber = 0;
    cout << "NEXT WAYPOINT = " << rddf.getNextWaypointNumber(Northing,Easting,waypointNumber) << endl;
    break;

  default:
    cout << "Wrong choice !!" << endl;
  }
  
  return(0);
}
