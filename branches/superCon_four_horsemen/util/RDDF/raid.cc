#include "raid.hh"

using namespace std;


raid::raid()
{
  m_raid_state = new double[rmsg::LAST];
  /** set all debug values to their defaults */
  RAID_INFO(SET_RAID_DEFAULTS)
}



/* raid destructor */
raid::~raid()
{
  delete [] m_raid_state;
}



void raid::printDebug(string message, int rmsg_enum_val, double required_level, string file, int line)
{
  //  cout << "raid::printDebug start, rmsg_enum_val = " << rmsg_enum_val << ", required_level = "
  //     << required_level << ", and m_raid_state[rmsg_enum_val] = " << m_raid_state[rmsg_enum_val] << endl;
  if (m_raid_state[rmsg_enum_val] >= required_level)
    cout << "[" << file << ":" << line << "] " << message << endl;
}
