#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define ERROR 1
#define MAX_MSG 200

int main (int argc, char *argv[]) {

  int Socket, Server, i, j; //Socket is connection to server
  int fd_max;               //max file descriptors
  int bytes;
  int SERVER_PORT;
  struct sockaddr_in localAddr, servAddr;
  struct hostent *h;
  char line[MAX_MSG];       //stores message here
  fd_set fileDesc;          //file descriptors
  fd_set copy_fileDesc;

  FD_ZERO(&fileDesc);       //clear file descriptors
  FD_ZERO(&copy_fileDesc);

  FD_SET(0, &fileDesc);     //add 0 (Standard In)
  fd_max = 0;

  if(argc < 3)
    {
      printf("usage: %s <server hostname> <server port>\n",argv[0]);
      return -1;
    }

  //get server port number from input
  SERVER_PORT = atoi(argv[2]);

  //get hostname from input
  h = gethostbyname(argv[1]);
  if(h==NULL) {
    printf("%s: unknown host '%s'\n",argv[0],argv[1]);
    return -1;
  }

  servAddr.sin_family = h->h_addrtype;
  memcpy((char *) &servAddr.sin_addr.s_addr, h->h_addr_list[0], h->h_length);
  servAddr.sin_port = htons(SERVER_PORT);

  /* create socket */
  Socket = socket(AF_INET, SOCK_STREAM, 0);
  if(Socket < 0) {
    perror("cannot open socket ");
    return -1;
  }

  /* bind any port number */
  localAddr.sin_family = AF_INET;
  localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  localAddr.sin_port = htons(0);
  
  Server = bind(Socket, (struct sockaddr *) &localAddr, sizeof(localAddr));
  if(Server<0) {
    printf("%s: cannot bind port TCP %u\n",argv[0],SERVER_PORT);
    perror("error ");
    return -1;
  }
  
  /* connect to server */
  Server = connect(Socket, (struct sockaddr *) &servAddr, sizeof(servAddr));
  if(Server<0) {
    perror("cannot connect ");
    return -1;
  }
  else
    printf("%s: Connected to Server at Socket %d: Port %d\n", argv[0], Server, SERVER_PORT);

  //add Socket to file descriptor to be able to receive messages
  FD_SET(Socket, &fileDesc);
  if (Socket > fd_max)
    fd_max = Socket;

  while(1)
    {
      // copy file descriptor to avoid changes on select call
      copy_fileDesc = fileDesc;
      if(select(fd_max + 1, &copy_fileDesc, NULL, NULL, NULL) == -1)
	{
	  perror("Socket");
	  return ERROR;
	}
      for (i = 0; i <= fd_max; i++)
	if (FD_ISSET(i, &copy_fileDesc))
	  {
	    if (i == 0) //terminal entry
	      {
		//read data into line
		if ((bytes = read(i, line, sizeof(line))) <= 0)
		  {
		    if (bytes == 0)
		      {
			printf("%s: Disconnected From Server!\n", argv[0]);
			return -1;
		      }
		  }
		//if bytes != 0, ie no error, send line to server Socket
		else
		  {
		    line[bytes - 1] = '\0';
		    if (send(Socket, line, bytes - 1, 0) == -1)
		      {
			perror("Cannot send to Server");
			return ERROR;
		      }
		    else
		      {
			printf("%s: Message Sent to Server: %s\n", argv[0], line);
		      }
		  }
	      }
	    //if i != 0, getting message from server
	    else
	      {
		//read message into line
		if ((bytes = recv(i, line, sizeof(line), 0)) <= 0)
		  {
		    if (bytes == 0)
		      {
			printf("%s: Disconnected From Server!\n", argv[0]);
			return -1;
		      }
		    else
		      perror("Connection error from server");
		    close(i); //on error, close out server Socket
		    if (i == fd_max)
		      fd_max -= 1;
		    FD_CLR(i, &fileDesc);
		  }
		//without error, print out line
		else
		  {
		    line[bytes] = '\0';
		    printf("%s: %s\n", argv[0], line);
		  }
	      }
	  }
    }
  
  return 0;
  
}
