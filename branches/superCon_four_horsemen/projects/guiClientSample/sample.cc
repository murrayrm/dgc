#include <iostream>
#include <fstream>
using namespace std;

#include "DGCutils"
#include "sample.h"

#define SAMPLE_UPDATE_PERIOD 2000000

void CSampleClient::getDataThread(void)
{
  while(true)
    {
      cout << "Enter an unsigned and a double to display" << endl;
      cin >> m_input.SAMPLE_INPUT_UNSIGNED >> m_input.SAMPLE_INPUT_DOUBLE;
    }
}

CSampleClient::CSampleClient(int skynet_key)
  : CSkynetContainer(SNplanner, skynet_key),
    CModuleTabClient(&m_input, &m_output)
{
  DGCstartMemberFunctionThread(this, &CSampleClient::getDataThread);
  DGCstartMemberFunctionThread(this, &CSampleClient::updatePlot);
}

void CSampleClient::activeLoop(void)
{
  while(true)
    {
      cout << "Current data: " << m_output.SAMPLE_OUTPUT_INT << endl;
      DGCusleep(SAMPLE_UPDATE_PERIOD);
    }
}

void CSampleClient::updatePlot(void)
{
  point myData;
  //gdouble x = 0;
  //gdouble y; 
  myData.x = 0.0;
  int plotSocket = m_skynet.get_send_sock(SNModulePlotInput);
  while(true)
    {      
      myData.y = rand()%10/10.;
      myData.x += 1.0;
      std::cout << " Calling add "<< myData.x << "," << myData.y << " " << std::endl;
      m_skynet.send_msg(plotSocket, &myData, sizeof(myData), 0);
      DGCusleep(SAMPLE_UPDATE_PERIOD);
    }

}
