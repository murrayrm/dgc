#include "bPlanner.hh"

using namespace std;

/* bPlanner constructor */
bPlanner::bPlanner(int des_skynet_key)
  : CSkynetContainer(MODbPlanner, des_skynet_key)
{
  pd("bPlanner::bPlanner start", rmsg::fncall, 1);
  pd("bPlanner::bPlanner finish", rmsg::fncall, 1);
}



/* bPlanner destructor */
bPlanner::~bPlanner()
{
}
