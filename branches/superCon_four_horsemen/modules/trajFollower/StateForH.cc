#include "StateForH.hh"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fstream>
#include <pthread.h>



char *testlog  = new char[100];
char *pathlog  = new char[100];

using namespace std;


StateForH::StateForH(int skynet_key) 
	: CStateClient(SNastate, skynet_key) {
  cout<<"before DGCgettime"<<endl;	
	DGCgettime(starttime);
	cout<<"after DGCgettime"<<endl;
		
		char astateFileName[256];
		time_t t = time(NULL);
		tm *local;
		local = localtime(&t);

  sprintf(astateFileName, "logs/astate_%04d%02d%02d_%02d%02d%02d.dat", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

  m_outputAstate.open(astateFileName);
		
 	m_outputAstate << setprecision(20);

	m_outputAstate<<"testing"<<endl;


		m_outputAstate << "Time\tnorth\teast\talt\tVel_N\tVel_E\tVel_U\tacc_n\tacc_e\tacc_u\tRoll\tPitch\tYaw\trollrate\tpitchrate\tyawrate\trollacc\tpitchacc\tyawacc\n\r";
				


}

void StateForH::Active() {
	while( true ) 
	{
	  //sleep(1);
	  usleep(25);
		cout << "Trying to update state: " << endl;
		UpdateState();
		cout << "Updated state." << endl;

    		timecalc = (m_state.Timestamp - starttime) / 1e6;
		
    		m_outputAstate <<  timecalc << '\t';
    		m_outputAstate <<  m_state.Northing << '\t' << m_state.Easting << '\t' << m_state.Altitude << "\t";
    		m_outputAstate <<  m_state.Vel_N << '\t' << m_state.Vel_E << '\t' << m_state.Vel_D << "\t";
    		m_outputAstate <<  m_state.Acc_N << '\t' << m_state.Acc_E << '\t' << m_state.Acc_D << "\t";
    		m_outputAstate <<  m_state.Roll << '\t' << m_state.Pitch << '\t' << m_state.Yaw << "\t";
    		m_outputAstate <<  m_state.RollRate << '\t' << m_state.PitchRate << '\t' << m_state.YawRate << "\t";
    		m_outputAstate <<  m_state.RollAcc << '\t' << m_state.PitchAcc << '\t' << m_state.YawAcc << "\r\n";
		m_outputAstate.close();

    		}
}


int main(int argc, char **argv) {

	int sn_key = 0;

	cout<<"yet another debugging fxn"<<endl;
	char* pSkynetkey = getenv("SKYNET_KEY");
	cout<<"got skynet key"<<endl;
	if ( pSkynetkey == NULL)
	{
		cerr << "SKYNET_KEY environment variable isn't set." << endl;
	} else
	{
		sn_key = atoi(pSkynetkey);
	}
	cerr << "Constructing skynet with KEY = " << sn_key << endl;
	StateForH ast(sn_key);
	cout<<"about to call active"<<endl;
	ast.Active();
  	return 0;
}


