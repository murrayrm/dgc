/*!* This file holds standardized function calls from adrive
 * which in turn call the individual driver for actuators. */

#include "actuators.h"


// Look for the vehicle structure. 
extern struct vehicle_t my_vehicle;
extern int simulation_flag;
extern char simulator_IP;

/******************Brake Function Calls *****************
 * are below */
/*
 * functionalitiy with multithreaded adrive.  Basically standardized function
 * calls that call functions already defined in the existing code.  
 *  Tully Foote
 * 
 ************************************************************************/
/*! The standadized function call to execute brake command*/
int execute_brake_command( double command ) 
{
  double brake_command;
  brake_command = command;
  //printf("Executing Brake Command %g\n", command);
  if (brake_command >= 0 && brake_command <= 1)
    {
      if (brake_setposition(brake_command) == FALSE)
	{
	  // serial write failed
	  my_vehicle.actuator[ACTUATOR_BRAKE].error = TRUE;
	  return FALSE;
	}
      else
	return TRUE;
    }
  else
    {
      event << "BRAKE COMMAND REJECTED OUT OF RANGE";
      //      cerr << command << " is out of the Brake Rance" ;
      // command out of range
      return FALSE;
    }
}

/*! The standadized function call to execute brake status*/
int execute_brake_status()
{
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator[ACTUATOR_BRAKE].update_time);

  double temp_val = 0;
  //printf("Executing Brake Status \n");
  if ( (temp_val = brake_getposition()) == -1)
    {
      my_vehicle.actuator[ACTUATOR_BRAKE].error = TRUE;
      return ERROR;
    }
  else 
    // cout << "!!!!!!!!!!!!!!!!!!!!!!!!temp_vall is " << temp_val << " for the prake position" <<endl;
    my_vehicle.actuator[ACTUATOR_BRAKE].position = temp_val;
  if ( (temp_val = brake_getpressure()) == -1)
    {
      my_vehicle.actuator[ACTUATOR_BRAKE].error = TRUE;
      return ERROR;
    }
  else 
    my_vehicle.actuator[ACTUATOR_BRAKE].pressure = temp_val;
  
  return TRUE;
}

/*! The standadized function call to execute brake initialization*/
void execute_brake_init()
{
  printf("Executing Brake Init \n");
  if (simulation_flag)
    {
      //      printf("Sim");
      simulator_brake_open(my_vehicle.actuator[ACTUATOR_BRAKE].port);
    }
  else if(brake_open(my_vehicle.actuator[ACTUATOR_BRAKE].port) != TRUE)
    {
      printf("Brake open failed\n");
      my_vehicle.actuator[ACTUATOR_BRAKE].status = 0;

    }
  else
    {
      //printf("Brake OK\n");
      my_vehicle.actuator[ACTUATOR_BRAKE].status = 1;
    }
}

void execute_brake_close()
{
  brake_close();
}

/******************Trans Function Calls *****************
 * are below */
/**********************These are placeholder functions */

/*! The standadized function call to execute transmission command*/
int execute_trans_command( double command ) 
{
  //printf("Executing Trans Command \n");
  // make sure we actually have to send a command
  if (my_vehicle.actuator_trans.command != my_vehicle.actuator_trans.position)
    {
      if (my_vehicle.actuator_estop.position == EPAUSE )  //Make sure we're in pause
	{
	  if (my_vehicle.actuator_obdii.status == 1)  // Make sure OBDII is running
	    {
	      if (fabs(my_vehicle.actuator_obdii.VehicleWheelSpeed) < SPEED_STOPPED_THRESHOLD)  
		// Make sure we're stopped.  
		{
		  // If all those are met shift.
		  trans_setposition( my_vehicle.actuator_trans.command);
		}
	      else
		{
		  event << "TRANS COMMAND REJECTED  ALice not stopped";
		  // Not at zero speed
		  //	  cout << "NOT STOPPED NO SHIFTING!" << endl;
		}
	    }
	  else // IN pause but no oobdii
	    {
	      // Sleep until we are sure that pausing has stopped us.  
	      sleep (BLIND_SHIFTING_WAIT_TIME);
	      //cout << "OBDII invalid no shifting" << endl;
	      // If OBDII has failed put execution of tran status here.  
	      trans_setposition( my_vehicle.actuator_trans.command);
	    }
	}
      else 	  // We're not in pause
	{
	  event << "TRANS COMMAND REJECTED NOT IN PAUSE";
	  //cerr << "NO SHIFTING OUT OF PAUSE!!!" << endl;
	}
    }
  // Check whether the ignition is enabled.  
  if (my_vehicle.enable_ignition)
    {
      // If the ignition is not in the right position
      if (my_vehicle.actuator_trans.ignition_command != my_vehicle.actuator_trans.ignition_position)
	{
	  
	  if (my_vehicle.actuator_trans.ignition_command == I_START)
	    {
	      return  execute_engine_startup();
	    }
	  else if (my_vehicle.actuator_trans.ignition_command == I_RUN || my_vehicle.actuator_trans.ignition_command == I_OFF)
	    {
	      return ign_setposition((int)my_vehicle.actuator_trans.ignition_command);
	    }
	}
    }
  
  if (my_vehicle.actuator_trans.led1_command != my_vehicle.actuator_trans.led1_position)
    {
      return led_setposition(1, my_vehicle.actuator_trans.led1_command);
    }
  if (my_vehicle.actuator_trans.led2_command != my_vehicle.actuator_trans.led2_position)
    {
      return led_setposition(1, my_vehicle.actuator_trans.led2_command);
    }
  if (my_vehicle.actuator_trans.led3_command != my_vehicle.actuator_trans.led3_position)
    {
      return led_setposition(1, my_vehicle.actuator_trans.led3_command);
    }
  if (my_vehicle.actuator_trans.led4_command != my_vehicle.actuator_trans.led4_position)
    {
      return led_setposition(1, my_vehicle.actuator_trans.led4_command);
    }
  // Everything was in the right position return 1
  return 1;
  
}

/*! The standadized function call to execute transmission status*/
int execute_trans_status()
{
  int error_count = 0;
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator_trans.update_time);

  //printf("Executing Trans Status \n");
  if (my_vehicle.actuator_obdii.status != 1)
    {
      if ((my_vehicle.actuator_trans.position = trans_getposition()) == TRANS_ERROR)
	{
	  error_count++;
	}
    }
  my_vehicle.actuator_trans.ignition_position = ign_getposition();
  //  cout << ign_getposition() << " is the position of the ignition " << my_vehicle.actuator_trans.ignition_position << "again" <<endl;
  if ((my_vehicle.actuator_trans.led1_position = led_getposition(1)) == ERROR)
    error_count++;
  if ((my_vehicle.actuator_trans.led2_position = led_getposition(2)) == ERROR)
    error_count++;
  if ((my_vehicle.actuator_trans.led3_position = led_getposition(3)) == ERROR)
    error_count++;
  if ((my_vehicle.actuator_trans.led4_position = led_getposition(4)) == ERROR)	
    error_count++;
  if (error_count != 0)
    return -1;
  return 1;
}

/*! The standadized function call to execute transmission initialization*/
void execute_trans_init()
{
  printf("Executing Trans Init \n");
  my_vehicle.actuator_trans.status = igntrans_open(my_vehicle.actuator_trans.port);
}
void execute_trans_close()
{
  igntrans_close();
}


/******************Estop Function Calls *****************
 * are below */
/**********************These are placeholder functions */
/*! The standadized function call to execute estop command*/
int execute_estop_command( double command ) 
{
  printf("Executing Estop Command %g, you should not be doing this!\n", command);
  return 1;
}

/*! The standadized function call to execute estop status*/
int execute_estop_status()
{
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator_estop.update_time);
  unsigned long long the_current_time;
  static unsigned long long end_sleep_time;
  static int timeout_enabled = false;


  //  printf("about to read status");
  int  current_estop_status =  estop_status();
  //printf("i have read status %d, replacing %d\n", current_estop_status, my_vehicle.actuator_estop.dstop);
  
  //printf("Executing Estop Status \n");

  if ( current_estop_status == -1)
    {
      my_vehicle.actuator_estop.status = 0;
      return -1;
    }


  /* This is a level of estop that adrive will react to a disable without commands
   * coming down the chain.  
   *
   * In case of disable adrive will:
   * Put the brakes on full
   * Set the throttle to zero
   * Center the steering wheel
   *
   */
  if ( current_estop_status == DISABLE)
    {
      if (my_vehicle.automatic_timber_logging == true)
	if (my_vehicle.actuator_estop.dstop == RUN)
	  {
	    // Stop logging here
	    my_vehicle.pAtimberBox->timberStop();
	  } 
      
      // Make sure that we're not timing for a reenable
      timeout_enabled = false;
      
      // Record that we're in darpa diaable
      my_vehicle.actuator_estop.dstop = DISABLE; 
      
      //Apply Full Brakes
      my_vehicle.actuator[ACTUATOR_BRAKE].command = 1; 
      pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
      
      // Center the steering
      my_vehicle.actuator[ACTUATOR_STEER].command = 0; 
      pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_STEER].cond_flag );

      // Zero the gas
      my_vehicle.actuator[ACTUATOR_GAS].command = 0;
      pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );

    }

  /* This is the case when we are in pause.
   * if we are not using mode man this will be enabled by the --nomm flag.  
   * gas will be set to zero and brake to half whenever in pause.
   */

  else if ( current_estop_status == EPAUSE)
    {
      if (my_vehicle.automatic_timber_logging == true)
	if (my_vehicle.actuator_estop.dstop == RUN)
	  {
	    // Stop logging here
	    my_vehicle.pAtimberBox->timberStop();
	  }
      
      // Make sure that we're not timing for a reenable
      timeout_enabled = false;
      
      // Record that we're in darpa pause
      my_vehicle.actuator_estop.dstop = EPAUSE; 
      updateEstopPosition(&my_vehicle);

      if (my_vehicle.actuator_estop.dstop == DISABLE)
	{
	  //cout << "overiding steering disable in pause loop" << endl;
	  pthread_mutex_lock(&(my_vehicle.actuator[ACTUATOR_STEER].mutex));
	  steer_enable_overide();
	  pthread_mutex_unlock(&(my_vehicle.actuator[ACTUATOR_STEER].mutex));
	}
    }
  
  
  // Deal with the 5 second delay required coming out of pause into run.  
  //   Including the possibility of jumping straight out of disable.  

  else if (my_vehicle.actuator_estop.dstop != RUN &&  current_estop_status == RUN)
    {
      //      cout << "trying to go to run" << endl;
      if (!timeout_enabled)
	{
	  //cout << "starting timer" ;
	  // Fill in the struct that we're trying to go to run
	  my_vehicle.actuator_estop.about_to_unpause = true;
	  timeout_enabled = true;
	  DGCgettime(end_sleep_time);
	  end_sleep_time += DARPA_PAUSE_TIMEOUT;
	  //cout << end_sleep_time << " is what the end time is set to" << endl;
	  return 1;
	}
      else
	{
	  DGCgettime(the_current_time);
	  //  cout << the_current_time << " is the current time" << endl;
	  // printf("about to sleep for 5");
	  //cout << end_sleep_time - the_current_time << " is the current  time diff" << endl;
	  
	  
	  if (the_current_time > end_sleep_time)
	    {
	      // Reset the timout
	      timeout_enabled = false;
	      my_vehicle.actuator_estop.about_to_unpause = false;
	      //	      cout << "resetting the timeout" << endl;
	      my_vehicle.actuator_estop.dstop = RUN;
	      if (my_vehicle.automatic_timber_logging == true)
		{
		  // Start logging here
		  my_vehicle.pAtimberBox->timberStart();
		}
	      return 1; 
	    }
	  else
	    {
	      //	      printf("i have not yet slept for 5");
	      return 1;
	    }
	  
	  	
	    
	    //THey didn't match so don't act.
	  return 1;
	}
    }
  // Finally return run since all other cases should be exhausted.  
  my_vehicle.actuator_estop.dstop =  current_estop_status;
  //  printf("ending estops_execute_status()\n");
  return 1;
}

/*! The standadized function call to execute estop initialization*/
void execute_estop_init()
{
  printf("Executing Estop Init \n");
  if(estop_open(my_vehicle.actuator_estop.port) != TRUE)
    {
      // If it fails set the status to 0.  
      printf("EStop failed to initialize\n");
      my_vehicle.actuator_estop.status = 0;
    }
  else
    {
      my_vehicle.actuator_estop.status = 1;
    }
}

void execute_estop_close()
{
  estop_close();
}


/******************Steer Function Calls *****************
 * are below */

/*! The standadized function call to execute steer command*/
int execute_steer_command( double command ) 
{
  double steer_command = command;
  //printf("Executing Steer Command %d\n", steer_command);
  if (my_vehicle.protective_interlocks)
    {
      // Check that the OBDII is working and if so we are moving
      if (my_vehicle.actuator_obdii.status == 1)
	{
	  // Don't turn the wheel when we're stopping for a pause
	  // less than 1 mps
	  if (my_vehicle.actuator_estop.position == EPAUSE && 
	      my_vehicle.actuator_obdii.VehicleWheelSpeed < 1)
	    {
	      if (my_vehicle.actuator_estop.position == EPAUSE && 
		  my_vehicle.actuator_obdii.VehicleWheelSpeed <= SPEED_STOPPED_THRESHOLD)
		{
		  //  printf("We're slow and paused no steer.");
		  event << "STEER COMMAND  IN PAUSE and below SPEED_STOPPED_THRESHOLD  disabling steering";
		  steer_disable();
		}
	      
		return 0;
	    }
	  // Don't turn the wheel when we are stopped
	  if (fabs(my_vehicle.actuator_obdii.VehicleWheelSpeed) <= SPEED_STOPPED_THRESHOLD)
	    {
	      event << "STEER COMMAND REJECTED Vehicle speed too low ";
	      //	  printf("We're effectively stopped no steer");
	      return 0;
	    }
	  
	}
    }

  if (steer_command <= 1 && steer_command >= -1)
    {
      steer_heading(steer_command);
      return 1;
    }
  else 
    {
      event << "STEER COMMAND REJECTED OUT OF RANCE";
      return 0;
    }
}

/*! The standadized function call to execute steering velocity */
int execute_steer_velocity( double command ) 
{
  double steer_command = command;
  //printf("Executing Steer Command %d\n", steer_command);
  if (steer_command <= 1 && steer_command >= 0)
    {
      // Check whether the actuator is enabled first
      if (my_vehicle.actuator[ACTUATOR_STEER].command_enabled == 1)
	steer_setvel(steer_command);
      return 1;
    }
  else 
    return 0;
}

/*! The standadized function call to execute steering acceleration */
int execute_steer_acceleration( double command ) 
{
  double steer_command = command;
  //printf("Executing Steer Command %d\n", steer_command);
  if (steer_command <= 1 && steer_command >= 0)
    {
      // Check whether the actuator is enabled first
      if (my_vehicle.actuator[ACTUATOR_STEER].command_enabled == 1)
	steer_setaccel(steer_command);
      return 1;
    }
  else 
    return 0;
}




/*! The standadized function call to execute steer status*/
int execute_steer_status()
{
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator[ACTUATOR_STEER].update_time);

  double retval;
  //  printf("Executing Steer Status \n");
  if ((retval =  (double) steer_getheading()) == -2)
    return ERROR;
  my_vehicle.actuator[ACTUATOR_STEER].position = retval;
  return true;
}

/*! The standadized function call to execute steer initialization*/
void execute_steer_init()
{
  printf("Executing Steer Init \n");

  if(simulation_flag)
    {
      simulator_steer_open(my_vehicle.actuator[ACTUATOR_STEER].port);
      pthread_cond_broadcast(&steer_calibrated_flag);
    }
  
  else if(steer_open(my_vehicle.actuator[ACTUATOR_STEER].port) != TRUE)
    {
      my_vehicle.actuator[ACTUATOR_STEER].status = 0;
      //abort();
      // This will start up things that are waiting on the steering.  
      printf("Steer init failed, continuing anyway.\n");
      pthread_cond_broadcast(&steer_calibrated_flag);
      
    }
  else
	{
	  //printf("Steer_opened properly\n");
	  my_vehicle.actuator[ACTUATOR_STEER].status = 1;
	  pthread_cond_broadcast(&steer_calibrated_flag);
	  //printf("I ihave sent the wakeup\n");
	}
}

void execute_steer_close()
{
  steer_close();
}





/******************Gas Function Calls *****************
 * are below */

/*! The standadized function call to execute gas initialization*/
int execute_gas_command( double command ) 
{
  double throttle_command;
  throttle_command = command;
  //  printf("Executing Gas Command %f\n", throttle_command);
  if (my_vehicle.protective_interlocks)
    if (my_vehicle.actuator_trans.position == T_PARK )
      {
	event << "THROTTLE COMMAND REJECTED due to being in park";
	return FALSE;
      }
  // Make sure we're in range
  if (throttle_command <= 1 && throttle_command >=0)
    {
      if ( throttle_setposition(throttle_command) == FALSE )
	{
	  my_vehicle.actuator[ACTUATOR_GAS].error = 1;
	  // serial write failed
	  return FALSE;
	}
      return TRUE;
    }
  else
    {
      event << "THROTTLE COMMAND REJECTED OUT OF RANGE";
      // command out of range
    }
  return FALSE;
}


/*! The standadized function call to execute gas status*/
int execute_gas_status()
{
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator[ACTUATOR_GAS].update_time);

  double retval;
  //printf("Executing Gas Status \n");
  if (  (retval = throttle_getposition()) == -1) //AKA an error
  {
    my_vehicle.actuator[ACTUATOR_GAS].error = 1;
    return ERROR;
  }
  else 
    my_vehicle.actuator[ACTUATOR_GAS].position = retval;
  //printf("Throttle get_position() returns %d\n", throttle_getposition());
  return TRUE;
}

/*! The standadized function call to execute gas initialization*/
void execute_gas_init()
{
  printf("Executing Gas Init \n");
  if(simulation_flag)
    {
      simulator_throttle_open(my_vehicle.actuator[ACTUATOR_GAS].port);
      printf("Throttle open failed!!!!!!\n");
      my_vehicle.actuator[ACTUATOR_GAS].status = 0;
      //      exit(1);
    }
  else if(throttle_open(my_vehicle.actuator[ACTUATOR_GAS].port) != TRUE)
    {
      printf("THROTTLE: open failed!!!!!!\n");
      printf("THROTTLE: Is the serial cable plugged into the controller box and /dev/ttyS%d?\n", my_vehicle.actuator[ACTUATOR_GAS].port);
      printf("THROTTLE: Is the throttle power switch on the dashboard turned on?\n");
      printf("THROTTLE: Is the light switch on the estop box turned on?\n");
      printf("THROTTLE: Is the estop in run or pause mode?\n");
      printf("THROTTLE: Are the amber strobe lights on the roof flashing?\n");

      my_vehicle.actuator[ACTUATOR_GAS].status = 0;
      //      exit(1);
    }
  else
    {
      my_vehicle.actuator[ACTUATOR_GAS].status = 1;
      //printf("Throttle OK\n");
    }
}

void execute_gas_close()
{
  throttle_close();
}


/*** THIS IS THE CODE FOR THE OBDII INTERFACE ***/
/*! The standadized function call to execute brake status*/
int execute_obdii_status()
{
  //  double dummy_engine_torque;
  double dummy_vehicle_speed;
  char dummy_trans_position;
  static bool priority_distribution_toggle;
  
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator_obdii.update_time);
  
  //printf("Executing OBDII Status \n");
  int retval = 1;
  // Get Vehicle speed
  if (my_vehicle.actuator_obdii.my_obdii_driver.getVehicleSpeed(dummy_vehicle_speed) == -1)
    return -1;
  // Multiply by transmissino position to make it a  velocity.
  my_vehicle.actuator_obdii.VehicleWheelSpeed = my_vehicle.actuator_trans.position * dummy_vehicle_speed;
  

  // Check whether we have polled enough speed we still need to poll other values
  if (my_vehicle.actuator_obdii.poll_type_counter > my_vehicle.actuator_obdii.speed_priority)
    {
      my_vehicle.actuator_obdii.poll_type_counter = 0; // reset the counter
      if (priority_distribution_toggle)
	{
	  //	  cout << "toggle true";
	  // GET RPM
	  if (my_vehicle.actuator_obdii.my_obdii_driver.getRPM(  my_vehicle.actuator_obdii.engineRPM ) == -1)
	    return -1;
	  priority_distribution_toggle = false;
	}
      else
	{
	  //	  cout << "toggle false" << endl;
	  // GET ENGINE TEMP
	  //      my_vehicle.actuator_obdii.my_obdii_driver.getEngineCoolantTemp(my_vehicle.actuator_obdii.EngineCoolantTemp);
	  // GET TRANSMISSION POSITION
	  if (my_vehicle.actuator_obdii.my_obdii_driver.getTransmissionPosition(dummy_trans_position) == -1)
	    return -1;;
	  switch (dummy_trans_position)
	    {
	    case 'X':
	      my_vehicle.actuator_trans.position = TRANS_ERROR;
	      // This is usually the case that the engine is off.  And is what the OBDII returns when i cannot read.  
	      my_vehicle.actuator_obdii.status = 0;
	      retval = OFF;
	      break;
	    case 'P':
	      my_vehicle.actuator_trans.position = T_PARK;
	      // The OBDII is working properly
	      my_vehicle.actuator_obdii.status = 1;
	      break;
	    case 'R':
	      my_vehicle.actuator_trans.position = T_REVERSE;
	      // The OBDII is working properly
	      my_vehicle.actuator_obdii.status = 1;
	      break;
	    case 'D':
	      my_vehicle.actuator_trans.position = T_DRIVE;  
	      // The OBDII is working properly
	      my_vehicle.actuator_obdii.status = 1;
	      break;
	    case 'N':
	      my_vehicle.actuator_trans.position = T_NEUTRAL;
	      // The OBDII is working properly
	      my_vehicle.actuator_obdii.status = 1;
	      break;
	    }
	  priority_distribution_toggle = true;
	}

      // GET CURRENT GEAR RATIO
      //my_vehicle.actuator_obdii.my_obdii_driver.getCurrentGearRatio(my_vehicle.actuator_obdii.CurrentGearRatio);
      // GET BATTERY VOLTAGE
      //  my_vehicle.actuator_obdii.my_obdii_driver.getBatteryVoltage(my_vehicle.actuator_obdii.BatteryVoltage);  
      // GET TORQUE
      //      my_vehicle.actuator_obdii.my_obdii_driver.getTorque(dummy_engine_torque );
      // Set the wheel force based on torque
      //      my_vehicle.actuator_obdii.WheelForce = dummy_engine_torque  * TORQUE_CONVERTER_EFFICIENCY * DIFFERENTIAL_RATIO / VEHICLE_TIRE_RADIUS * US_FOOT_POUNDS_TO_NEWTON_METERS; //* my_vehicle.actuator_obdii.CurrentGearRatio
    }
  else
    {
      my_vehicle.actuator_obdii.poll_type_counter++;
    }
  
  if (retval == 0)
    // Increment that we're in another status loop.  
    my_vehicle.actuator_obdii.poll_type_counter++;

  return retval;
}
  
/*! The standadized function call to execute obdii initialization*/
void execute_obdii_init()
{
  printf("Executing obdii Init\n");
  //  ostringstream devicestring("");
  //devicestring << "/dev/ttyS" << ;
  //cout << devicestring.str() <<endl;
  my_vehicle.actuator_obdii.status = my_vehicle.actuator_obdii.my_obdii_driver.connect((int)my_vehicle.actuator_obdii.port);
  //  cout << "OBDII INIT returned " <<   my_vehicle.actuator_obdii.status << endl;
}


int execute_obdii_command(double arg)
{
  event << "OBDII COMMAND RECIEVED You shouldn't be commanding the OBDII!";
  return FALSE;
}

int obdii_is_valid()
{
  if ( my_vehicle.actuator_obdii.status_enabled == 1)
    {
      return my_vehicle.actuator_obdii.status;
    }
  else 
    return false;
}

void execute_obdii_close()
{
  my_vehicle.actuator_obdii.my_obdii_driver.disconnect();
}
 
int engine_condition()
{
 
  if (my_vehicle.actuator_trans.ignition_position == I_OFF)
    return ENGINE_OFF;
  else if (my_vehicle.actuator_obdii.status == 0)
    return ENGINE_UNKNOWN;
  else if (my_vehicle.actuator_obdii.engineRPM  < ENGINE_ON_RPM)
    return ENGINE_STOPPED;
  return ENGINE_RUNNING;
}
 
 
 
/* The code to execute a command to start the engine safely.  
 * This will have a standard OBDII method and a failover in case
 * OBDII is invalid.  
 */
int execute_engine_startup()
{
  cout << "Exeuting execute_engine_startup" << endl;
  // If not in EPAUSE we will set pause and have to come back.
  if ( my_vehicle.actuator_estop.position == DISABLE)
    {
      execute_adrive_pause();
      return 0;
    }
  // If not in park we will set park and have to come back.  
  if ( my_vehicle.actuator_trans.position != T_PARK)
    {
      // Set the transmission to park 
      my_vehicle.actuator_trans.command = T_PARK;
      pthread_cond_broadcast( & my_vehicle.actuator_trans.cond_flag );
      cout << "moving to park" << endl;
      // And fail we will have to try again
      return 0;
    }

  // The optimal configuration
  if ( engine_condition() == ENGINE_STOPPED )  // OBDII is functional
    {
      cout << "the engine is stopped" << endl;
      // This assumes that we are in run since the OBDII is working
      // First lock the OBDII mutex so we can guarentee fast rpm polling
      pthread_mutex_lock(&my_vehicle.actuator_obdii.mutex);
      // Find out what temp the engine is.  
      my_vehicle.actuator_obdii.my_obdii_driver.getEngineCoolantTemp(my_vehicle.actuator_obdii.EngineCoolantTemp);
      // If it's cold wait
      if (my_vehicle.actuator_obdii.EngineCoolantTemp < COLD_START_TEMP)
	sleep(COLD_START_WAIT_TIME);
      
      ign_setposition(I_START);
      
      while(engine_condition() == ENGINE_STOPPED )
	{
	  // Wait until RPMs come up
	  // Refill the RPM before looping
	  my_vehicle.actuator_obdii.my_obdii_driver.getRPM(  my_vehicle.actuator_obdii.engineRPM );
	  my_vehicle.actuator_obdii.status_loop_counter++;  // Increment the OBDII counter so that supervisory doesn't flag the OBDII as not responding
	}

      // The engine is no longer in a STOPPED state so stop trying to start it.  
      // Return to run
      ign_setposition(I_RUN);
      //Return the command and position to run
      my_vehicle.actuator_trans.ignition_command = I_RUN;
      my_vehicle.actuator_trans.ignition_position = I_RUN;
      
      
      //Release the mutex
      pthread_mutex_unlock(&my_vehicle.actuator_obdii.mutex);
      return 1;
    }
  else if (engine_condition() == ENGINE_UNKNOWN) // OBDII is not valid
    {
      cout << "executing blind start" << endl;
      // Assume the engine is cold
      sleep(COLD_START_WAIT_TIME);
      // Start the started motor
      ign_setposition(I_START);
      sleep(BLIND_STARTER_MOTOR_TIME);
      ign_setposition(I_RUN);
      //Return the command and position to run
      my_vehicle.actuator_trans.ignition_command = I_RUN;
      //      my_vehicle.actuator_trans.ignition_position = I_RUN;
      return 1;
    }
  // We shouldn't get here so it must be an error.
  return 0;  
}
