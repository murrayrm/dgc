//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Aerial_RoadTracer.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void UD_Aerial_RoadTracer::trace(double east, double north, double head)
{
  int i, j;

  easting = east;
  northing = north;
  heading = head;

  reset();
  
  // should be multiple iterations to go down road ahead

  for (i = 0; i < path_length; i++) {

    iterate();

    path_x_state[i] = state->x[0];
    path_y_state[i] = state->x[1];

    for (j = 0; j < num_samples; j++) {
      path_x_samp[i][j] = sample[j]->x[0];
      path_y_samp[i][j] = sample[j]->x[1];
    }

//     glutSetWindow(UD_Aerial::aerial->window_id);
//     glutPostRedisplay();

    // put all particles and states in buffer (copy from state history in aerial)
    // redisplay aerial
  }
}

//----------------------------------------------------------------------------

/// constructor for ladar gap tracker class derived from particle filter class

//UD_Aerial_RoadTracer::UD_Aerial_RoadTracer(UD_Aerial *aer, int num_particles, UD_Vector *meanstate, UD_Vector *covdiag) : UD_ParticleFilter(num_particles, meanstate, covdiag)
UD_Aerial_RoadTracer::UD_Aerial_RoadTracer(int num_particles, UD_Vector *meanstate, UD_Vector *covdiag) : UD_ParticleFilter(num_particles, meanstate, covdiag)
{
  int i;

  // default values

  path_length = 50;
  path_x_samp = (double **) calloc(path_length, sizeof(double *));
  path_y_samp = (double **) calloc(path_length, sizeof(double *));
  for (i = 0; i < path_length; i++) {
    path_x_samp[i] = (double *) calloc(num_particles, sizeof(double));
    path_y_samp[i] = (double *) calloc(num_particles, sizeof(double));
  }
  path_x_state = (double *) calloc(path_length, sizeof(double));
  path_y_state = (double *) calloc(path_length, sizeof(double));

  init_position_variance = 1 * covdiag->x[0];
  init_heading_variance = 1 * covdiag->x[2];
  speed = 2; 

  show_particles = TRUE;
  initialize_prior();
}

//----------------------------------------------------------------------------

void UD_Aerial_RoadTracer::iterate()
{
  update();
}

//----------------------------------------------------------------------------

/// put particles in their a priori states, uniformly distributed over					
/// ladar angular range

void UD_Aerial_RoadTracer::pf_samp_prior(UD_Vector *samp)
{
  set_easting(normal_sample_UD_Random(easting, init_position_variance), samp);
  set_northing(normal_sample_UD_Random(northing, init_position_variance), samp);
  set_heading(normal_sample_UD_Random(heading, init_heading_variance), samp);
}

//----------------------------------------------------------------------------

/// apply deterministic motion model to particles

void UD_Aerial_RoadTracer::pf_dyn_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  new_samp->x[0] = old_samp->x[0] + speed * sin(old_samp->x[2]);
  new_samp->x[1] = old_samp->x[1] + speed * cos(old_samp->x[2]);
  new_samp->x[2] = old_samp->x[2];
}

//----------------------------------------------------------------------------

/// random diffusion: add Gaussian noise to old state sample 

void UD_Aerial_RoadTracer::pf_samp_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  sample_gaussian_diagonal_covariance(new_samp, old_samp, covdiagsqrt);
}

//----------------------------------------------------------------------------

/// evaluate likelihood of one particle given current image

double UD_Aerial_RoadTracer::pf_condprob_zx(UD_Vector *samp)
{
  int tilenum, x, y, intensity;
  double returnval;

  /*
  tilenum = UD_Aerial::aerial->utm2tile(samp->x[0], samp->x[1]);
  x = UD_Aerial::aerial->utm2tilepix_x(samp->x[0]);
  y = 200-UD_Aerial::aerial->utm2tilepix_y(samp->x[1]);

  if (!UD_Aerial::aerial->tileim[tilenum])
    return 0.0;
  else {
    intensity = (int) UCHAR_R_IMXY(UD_Aerial::aerial->tileim[tilenum], x, y);
    returnval = exp(-0.05 * (double) (255 - intensity));
    return returnval;
  }
  */
  return 1.0;

  // look up dominant orientation DO at samp's position; exponential of fabs(DO - samp heading)
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point POSITION (in image coordinates)
/// to file (terminal string is assumed to be newline)

void UD_Aerial_RoadTracer::write(FILE *fp)
{
  //  write(fp, "\n");
}

//----------------------------------------------------------------------------

/// write current estimate of vanishing point POSITION (in image coordinates)
/// to file with an argument specifying terminal string (i.e., newline, 
/// comma-space, etc.)

void UD_Aerial_RoadTracer::write(FILE *fp, char *terminal_string)
{
//   fprintf(fp, "%.2f, %.2f%s", vp_x(), vp_y(), terminal_string);
//   fflush(fp);
}

//----------------------------------------------------------------------------

/// draw road curvature tracker parameters, namely the POSITION of the
/// current road vanishing point.  assuming window dimensions are same as
/// candidate region

void UD_Aerial_RoadTracer::draw()
{
  int i;

  // particles are yellow points

  /*
  if (show_particles) {

    glColor3ub(255, 255, 0);
    glPointSize(1);

    glBegin(GL_POINTS);

    for (i = 0; i < num_samples; i++) 
      glVertex2f(sample[i], win_h - sample[i]);

    glEnd();
  }
  */

  // state is thick blue line

  /*
  glColor3ub(0, 0, 255);
  glPointSize(5);

  glBegin(GL_POINTS);

  glVertex2f(candidate_vp_x(), win_h - candidate_vp_y());

  glEnd();
  */
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
