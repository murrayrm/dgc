#include "AState_test.hh"

char *testlog  = new char[100];

AState_test::AState_test(int skynet_key) 
	: CSkynetContainer(SNastate_test, skynet_key) {
		DGCgettime(starttime);
}

void AState_test::Active() {
  //double outtime;
    unsigned long long nowtime;
	while( true ) 
	{
        DGCgettime(nowtime);

		UpdateState();

	  	testlogfile.open(testlog, fstream::out|fstream::app);
    
		testlogfile.precision(10);
    		testlogfile <<  m_state.Timestamp << '\t';
    		testlogfile <<  m_state.Northing << '\t' << m_state.Easting << '\t' << m_state.Altitude << "\t";
    		testlogfile <<  m_state.Vel_N << '\t' << m_state.Vel_E << '\t' << m_state.Vel_D << "\t";
    		testlogfile <<  m_state.Acc_N << '\t' << m_state.Acc_E << '\t' << m_state.Acc_D << "\t";
    		testlogfile <<  m_state.Roll << '\t' << m_state.Pitch << '\t' << m_state.Yaw << "\t";
    		testlogfile <<  m_state.RollRate << '\t' << m_state.PitchRate << '\t' << m_state.YawRate << "\t";
    		testlogfile <<  m_state.RollAcc << '\t' << m_state.PitchAcc << '\t' << m_state.YawAcc << "\r\n";

		testlogfile.close();

    		usleep(50000);
	}
}


int main(int argc, char **argv) {

	sprintf(testlog,"%s_full.dat",argv[1]);

	int sn_key = 0;
	char* pSkynetkey = getenv("SKYNET_KEY");

	if ( pSkynetkey == NULL)
	{
		cerr << "SKYNET_KEY environment variable isn't set." << endl;
	} else
	{
		sn_key = atoi(pSkynetkey);
	}

	cerr << "Constructing skynet with KEY = " << sn_key << endl;

	AState_test ast(sn_key);

	ast.Active();
  	return 0;
}
