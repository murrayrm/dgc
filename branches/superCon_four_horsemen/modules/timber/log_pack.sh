#!/bin/sh

# LBC, 5 Apr 05
#
# This script reflects some of the standard log file handling operations 
# that we want to be able to automate.  I've added "echo " to the beginning
# of each of the commands below just to show the commands that /would/ be
# run.

# If you actually want to use it, remove the "echo"s and run this cheesy script
# from dgc/modules.

# Be sure that NAME is what you want the test_logs directory to be called
NAME='20050405_racerddfsim2'

# Be sure that SUFFIX is what you want the individual module log directories to
# include
SUFFIX='logs_20050405_094000'

# These aren't used right now.
MODULES='skynetgui simulator trajFollower plannerModule fusionMapper'
PREFIXES='gui sim follower planner mapper'

# 1) Make a logs directory
echo
echo "This is just for show..."
echo
echo mkdir -p $NAME
echo

# 2) Move all of the logs to this directory, renamed
echo mv skynetGui/logs $NAME/gui$SUFFIX
echo mv simulator/logs $NAME/sim$SUFFIX
echo mv trajFollower/logs $NAME/follower$SUFFIX
echo mv plannerModule/logs $NAME/planner$SUFFIX
echo mv fusionMapper/logs $NAME/mapper$SUFFIX
echo

# 3) Put a README in there with all of the relevant information (manually for now)
#    - e.g. what modules were run; on what computer, in what directory, when started?
#    - The backslashes are artifacts of the echo command
echo echo \"lots of salient information!\" \> README
echo

# 4) Compress it all into one file (manually for now)
echo tar czvf $NAME.tgz $NAME
echo

# 5) scp it to grandchallenge and untar it from there (manually for now)
#    - The backslashes are artifacts of the echo command
echo scp $NAME.tgz grandchallenge:/dgc/test_logs
echo ssh grandchallenge \"cd /dgc/test_logs\; tar xzvf $NAME\; rm $NAME.tgz\"
echo

