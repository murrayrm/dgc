#ifndef SMART_COUNTER_HH
#define SMART_COUNTER_HH

#include"DGCutils"

//Smart counter, counting loops and frequency
template<class T>
class smart_counter
{
 public:
  smart_counter() : cnt(0), freq(0.0), start_time(0), start_cnt(0) {}
  ~smart_counter() {}

  smart_counter &operator++() {   //Pre cond
    return operator+=(1);
  }
  smart_counter &operator++(int) {  //Post cond
    return operator+=(1);
  }

  smart_counter &operator+=(T tt) {   //Pre cond
    cnt+=tt;
    start_cnt+=tt;
    unsigned long long t;
    DGCgettime(t);
    if(t-start_time > 1000000) {   //More than one second
      freq = start_cnt / (double)(t-start_time) * 1000000;
      start_cnt=0;
      start_time=t;
    }
    return *this;
  }

  void clear() {
    cnt = 0;
    freq = 0.0;
    start_time = 0;
    start_cnt = 0;
  }
 public:   //Data
  T cnt;
  double freq;

 private:
  unsigned long long start_time;   //Start time for counter below
  T start_cnt;
};


#endif //SMART_COUNTER_HH
