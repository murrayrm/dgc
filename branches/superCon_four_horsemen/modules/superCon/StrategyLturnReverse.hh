#ifndef STRATEGY_LTURNREVERSE_HH
#define STRATEGY_LTURNREVERSE_HH

//SUPERCON STRATEGY TITLE: L-turn Reverse - SOMETIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: 
//It has been determined that Alice needs to reverse, she wil reverse for a distance
//UP TO her 

#include "Strategy.hh"
#include "sc_specs.h"

namespace s_lturnreverse {

#define LTURNREVERSE_STAGE_LIST(_) \
  _(estop_pause, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(gear_reverse, ) \
  _(trajF_reverse, ) \
  _(estop_run, ) \
  _(reversing_wait, )
DEFINE_ENUM(lturnreverse_stage_names, LTURNREVERSE_STAGE_LIST)

}

using namespace std;
using namespace s_lturnreverse;

class CStrategyLturnReverse : public CStrategy
{

public:

  /** CONSTRUCTORS **/
  CStrategyLturnReverse() : CStrategy() {}  

  /** DESTRUCTOR **/
  ~CStrategyLturnReverse() {}


  /** MUTATORS **/

  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};

#endif
