#include "AState.hh"

//Struct containing all possible pieces of data being outputted

#include "asdisp.h"		// main display table
#include "asimu.h"		// main display table
#include "novdisp.h"		// Novatel GPS page

//Populate displayVars struct

void AState::sparrowThread() 
{
  CSparrowHawk &sh = SparrowHawk();

  sh.add_page(asdisp, "Main");

  sh.rebind("snkey", &snKey);
  
  sh.rebind("gpsState", &_metaState.gpsActive);
  sh.rebind("imuState", &_metaState.imuActive);
  sh.rebind("obdState", &_metaState.obdActive);
  sh.rebind("novState", &_metaState.novActive);
  sh.rebind("kfState", &_metaState.kfEnabled);
  sh.rebind("scState", &_metaState.scEnabled);

  sh.rebind("gpsCount", &gpsCount);
  sh.rebind("imuCount", &imuCount);
  sh.rebind("obdCount", &obdCount);
  sh.rebind("novCount", &novCount);

  sh.rebind("gpsValid", &gpsValid);

  sh.rebind("timber", &timberEnabled);
  sh.rebind("stateMode", &stateMode);

  sh.rebind("gpsNorth", &gpsNorth);
  sh.rebind("gpsEast", &gpsEast);
  sh.rebind("gpsHeight", &gpsHeight);
  sh.rebind("gpsFOM", &gpsFOM);
  sh.rebind("gpsDOP", &gpsDOP);
  sh.rebind("satellites", &gpsSats);

  sh.rebind("novNorth", &novNorth);
  sh.rebind("novEast", &novEast);
  sh.rebind("novHeight", &novHeight);
  sh.rebind("novNstd", &novNorthStd);
  sh.rebind("novEstd", &novEastStd);
  sh.rebind("novDstd", &novHgtStd);
  sh.rebind("novStdFactor", &novStdFactor);

  sh.rebind("obdVel", &obdVel);
  sh.rebind("obdSteer", &obdSteer);
  sh.rebind("obdBrake", &obdBrake);

  sh.rebind("trackErr", &trackErr);
  sh.rebind("stateJump", &stateJump);

  sh.rebind("vsNorthing", &_vehicleState.Northing);
  sh.rebind("vsEasting", &_vehicleState.Easting);
  sh.rebind("vsAltitude", &_vehicleState.Altitude);
  sh.rebind("vsVel_N",  &_vehicleState.Vel_N);
  sh.rebind("vsVel_E",  &_vehicleState.Vel_E);
  sh.rebind("vsVel_D",  &_vehicleState.Vel_D);
  sh.rebind("vsAcc_N",  &_vehicleState.Acc_N);
  sh.rebind("vsAcc_E",  &_vehicleState.Acc_E);
  sh.rebind("vsAcc_D",  &_vehicleState.Acc_D);
  sh.rebind("vsRoll", &_vehicleState.Roll);
  sh.rebind("vsPitch", &_vehicleState.Pitch);
  sh.rebind("vsYaw", &_vehicleState.Yaw);
  sh.rebind("vsRollRate", &_vehicleState.RollRate);
  sh.rebind("vsPitchRate", &_vehicleState.PitchRate);
  sh.rebind("vsYawRate", &_vehicleState.YawRate);
  sh.rebind("vsRollAcc", &_vehicleState.RollAcc);
  sh.rebind("vsPitchAcc", &_vehicleState.PitchAcc);
  sh.rebind("vsYawAcc", &_vehicleState.YawAcc);
  sh.rebind("vsNorthConf", &_vehicleState.NorthConf);
  sh.rebind("vsEastConf", &_vehicleState.EastConf);
  sh.rebind("vsHeightConf", &_vehicleState.HeightConf);
  sh.rebind("vsRollConf", &_vehicleState.RollConf);
  sh.rebind("vsPitchConf", &_vehicleState.PitchConf);
  sh.rebind("vsYawConf", &_vehicleState.YawConf);
  sh.rebind("headErr", &headErr);
  sh.rebind("alpha", &alpha);

  sh.add_page(asimu, "IMU");

  sh.rebind("gyroError", &gyroError);
  sh.rebind("accelError", &accelError);
  sh.rebind("gyroTmp", &gyroTmp);
  sh.rebind("accelTmp", &accelTmp);
  sh.rebind("powerError", &powerError);
  
  sh.rebind("xGyroTmp", &xGyroTmp);
  sh.rebind("yGyroTmp", &yGyroTmp);
  sh.rebind("zGyroTmp", &zGyroTmp);
  sh.rebind("xAccelTmp", &xAccelTmp);
  sh.rebind("yAccelTmp", &yAccelTmp);
  sh.rebind("zAccelTmp", &zAccelTmp);
  sh.rebind("diodeTmp", &diodeTmp);
  sh.rebind("receivTmp", &receivTmp);
  sh.rebind("bfsTmp", &bfsTmp);
  sh.rebind("minus5mon", &minus5mon);
  sh.rebind("plus5mon", &plus5mon);
  sh.rebind("minus15mon", &minus15mon);
  sh.rebind("plus15mon", &plus15mon);
  sh.rebind("analogGnd", &analogGnd);

  sh.rebind("imu_dvx", &imu_dvx);
  sh.rebind("imu_dvy", &imu_dvy);
  sh.rebind("imu_dvz", &imu_dvz);

  sh.rebind("imu_dtx", &imu_dtx);
  sh.rebind("imu_dty", &imu_dty);
  sh.rebind("imu_dtz", &imu_dtz);

  sh.rebind("imu_avx", &imu_avx);
  sh.rebind("imu_avy", &imu_avy);
  sh.rebind("imu_avz", &imu_avz);

  sh.rebind("imu_atx", &imu_atx);
  sh.rebind("imu_aty", &imu_aty);
  sh.rebind("imu_atz", &imu_atz);

  sh.rebind("imu_gain", &imu_gain);

  sh.rebind("imu_bvx", &imu_bvx);
  sh.rebind("imu_bvy", &imu_bvy);
  sh.rebind("imu_bvz", &imu_bvz);

  sh.rebind("imu_btx", &imu_btx);
  sh.rebind("imu_bty", &imu_bty);
  sh.rebind("imu_btz", &imu_btz);

  sh.set_notify("NOGPS", this, &AState::spToggleGps);
  sh.set_notify("NOIMU", this, &AState::spToggleImu);
  sh.set_notify("JUMP", this, &AState::allowJump);
  sh.set_notify("RESTART", this, &AState::restart);

  sh.set_string("gpsMode", "N/A");
  //Sparrow bind goes here

  //  sh.add_page(astune, "Tune");
  //More sparrow bind goes here

  // Set up Novatel page
  sh.add_page(novatel_tbl, "Novatel");
  sh.rebind("novNorth", &novNorth);
  sh.rebind("novEast", &novEast);

  // Run sparrow
  sh.run();

  while(sh.running()) {
    

    
    usleep(100000);
  }

  quitPressed = 1;

}

void AState::spToggleGps() {
  if (spGPSDisable == 0) {
    spGPSDisable = 1;
  } else {
    spGPSDisable = 0;
  }
}

void AState::spToggleImu() {
  if (spIMUDisable == 0) {
    spIMUDisable = 1;
  } else {
    spIMUDisable = 0;
  }
}
