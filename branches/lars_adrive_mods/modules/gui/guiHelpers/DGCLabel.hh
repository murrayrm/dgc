/**
 * DGCLabel.hh
 * Revision History:
 * 07/08/2005  hbarnor  Created
 * $Id: DGCLabel.hh 8530 2005-07-11 06:26:58Z hbarnor $
 */

#ifndef DGCLABEL_HH
#define DGCLABEL_HH

#include <gtkmm/label.h>
#include "DGCWidget.hh"

using namespace std;
/**
 * Customized Gtk::Label for our the Tab api. 
 * Subclasses Gtk::Label and adds a type-field. The type field is used to
 * determine the type of widget.. This is used mostly for error
 * correction. 
 */

class DGCLabel : public DGCWidget, public Gtk::Label
{

public:
  /**
   * Constructor for the label.
   * Contructor for a label with a default text value.
   * @param label - the default text to be displayed
   */
  DGCLabel(const Glib::ustring& label);

  /**
   * To DO 
   */
  virtual string getText();
  /**
   * @see DGCWidget
   */
  virtual void displayNewValue();
};

#endif
