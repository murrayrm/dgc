#include "CTimberBox.hh"

CTimberBox::CTimberBox(int skynetKey)
  : CSkynetContainer(SNGui, skynetKey) {
  
  m_label_timber_playback = new Gtk::Label("Timber Playback: ");
  m_label_timber_logging = new Gtk::Label("Timber Logging: ");

  m_hbox_timber_playback = new Gtk::HBox;
  m_hbox_timber_logging = new Gtk::HBox;

  m_button_timber_resume = new Gtk::Button("Resume");
  m_button_timber_pause = new Gtk::Button("Pause");
  m_button_timber_replay = new Gtk::Button("Replay");
  m_button_timber_start = new Gtk::Button("Start");
  m_button_timber_stop = new Gtk::Button("Stop");
  m_button_timber_restart = new Gtk::Button("Restart");

  this->pack_start(*m_hbox_timber_playback, Gtk::PACK_EXPAND_WIDGET, 10);
  this->pack_start(*m_hbox_timber_logging, Gtk::PACK_EXPAND_WIDGET, 10);

  m_hbox_timber_playback->pack_start(*m_label_timber_playback, Gtk::PACK_SHRINK, 1);
  m_hbox_timber_playback->pack_start(*m_button_timber_resume, Gtk::PACK_EXPAND_WIDGET, 1);
  m_button_timber_resume->signal_clicked().connect(mem_fun(this, &CTimberBox::timberResume));
  m_hbox_timber_playback->pack_start(*m_button_timber_pause, Gtk::PACK_EXPAND_WIDGET, 1);
  m_button_timber_pause->signal_clicked().connect(mem_fun(this, &CTimberBox::timberPause));
  m_hbox_timber_playback->pack_start(*m_button_timber_replay, Gtk::PACK_EXPAND_WIDGET, 1);
  m_button_timber_replay->signal_clicked().connect(mem_fun(this, &CTimberBox::timberReplay));

  m_hbox_timber_logging->pack_start(*m_label_timber_logging, Gtk::PACK_SHRINK, 1);
  m_hbox_timber_logging->pack_start(*m_button_timber_start, Gtk::PACK_EXPAND_WIDGET, 1);
  m_button_timber_start->signal_clicked().connect(mem_fun(this, &CTimberBox::timberStart));
  m_hbox_timber_logging->pack_start(*m_button_timber_stop, Gtk::PACK_EXPAND_WIDGET, 1);
  m_button_timber_stop->signal_clicked().connect(mem_fun(this, &CTimberBox::timberStop));
  m_hbox_timber_logging->pack_start(*m_button_timber_restart, Gtk::PACK_EXPAND_WIDGET, 1);
  m_button_timber_restart->signal_clicked().connect(mem_fun(this, &CTimberBox::timberRestart));

  timberMsgSocket = m_skynet.get_send_sock(SNguiToTimberMsg);
}


CTimberBox::~CTimberBox() {

}


void CTimberBox::timberResume() {
  CTimber::GUI_MSG_TYPES msg = CTimber::RESUME;
  m_skynet.send_msg(timberMsgSocket, &msg, sizeof(CTimber::GUI_MSG_TYPES));
}


void CTimberBox::timberPause() {
  CTimber::GUI_MSG_TYPES msg = CTimber::PAUSE;
  m_skynet.send_msg(timberMsgSocket, &msg, sizeof(CTimber::GUI_MSG_TYPES));
}


void CTimberBox::timberReplay() {
  CTimber::GUI_MSG_TYPES msg = CTimber::REPLAY;
  m_skynet.send_msg(timberMsgSocket, &msg, sizeof(CTimber::GUI_MSG_TYPES));
}


void CTimberBox::timberStart() {
  CTimber::GUI_MSG_TYPES msg = CTimber::START;
  m_skynet.send_msg(timberMsgSocket, &msg, sizeof(CTimber::GUI_MSG_TYPES));
}


void CTimberBox::timberStop() {
  CTimber::GUI_MSG_TYPES msg = CTimber::STOP;
  m_skynet.send_msg(timberMsgSocket, &msg, sizeof(CTimber::GUI_MSG_TYPES));
}


void CTimberBox::timberRestart() {
  CTimber::GUI_MSG_TYPES msg = CTimber::RESTART;
  m_skynet.send_msg(timberMsgSocket, &msg, sizeof(CTimber::GUI_MSG_TYPES));
}
