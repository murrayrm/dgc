/* This is my adaptation for the gimbal
 * Tully Foote
 * 2005/08/12
 */

/*----------------------------------------------------------------------------
--                                                                          --
--                        U N C L A S S I F I E D                           --
--                                                                          --
--                  L I T T O N   P R O P R I E T A R Y                     --
--                                                                          --
-- Copyright (c) 2004, Litton Systems, Inc.                                 --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
--                                                                          --
--                       RESTRICTED RIGHTS LEGEND                           --
--                                                                          --
-- Use, duplication or disclosure is subject to restrictions stated in      --
-- Proprietary Information Agreement between Litton Systems, Inc.,          --
-- Guidance and Controll Systems Division and California Institute of       --
-- Technology.                                                              --
--                                                                          --
----------------------------------------------------------------------------*/

/*
example_application.c

Read data from the LN-200 via the FastComm ESCC/P card device driver, at 400 Hz.
Calculate and display scaled 1 Hz sums of the delta velocities and delta angles.

Notes:

The EXCC/P device driver can buffer up to 3 seconds worth of LN-200 data. This allows an
application program to not read data from the device driver for up to 3 seconds, as long
as the application program keeps up with the LN-200 output over the long term (read data
faster than 400 Hz once the program has gotten behind).

This example application program checks the integrity of the gather data in 4 ways, each
time data is read from the LN-200. The 4 ways are:

1) The device driver is statused to see if an internal buffer overrun has occurred.

2) The data returned from the device driver is checked for the proper byte count.

3) The SDLC (LN-200 data bus) CRC byte included at the end of the data read from the
device driver is verified.

4) The incrementing mux counter (the 14th byte in the data read from the device driver)
is tracked to verify that it increments by one each time data is read from the device
driver.

If any one of these checks fails, the application program has lost data.
*/

/*
Operating mode switches:

The following two defines enable code that shows how to status the ESCC/P card to see if data
is available for reading. With both defines set to 0, the application program call to read
data from the device driver will block until there is data available. That block can be as
long as 2.5 milli-seconds (400 Hz), the amount of time between data frames transmitted by
the LN-200.  
*/

#define READ_NONBLOCK			0	// Read, not blocking for next available data.
#define CHECK_RX_READY			0	// Check for data available before reading.

/* Program defines. */

#define MSG_RATE				400	// Rate in Hz the LN-200 outputs a message.
#define FRAME_SIZE_REQUEST		64	// Mingimbaln number of bytes to request from a DD read call. 
#define FRAME_SIZE_GOOD_RETURN	27	// Number of bytes that should be returned from a DD read call.
#define MUX_MAX_VALUE			33	// MUX counter maxgimbalm value before rolling back over to 0.

/* Program includes. */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "esccpdrv.h"
#include <sys/ioctl.h>
#include <errno.h>
#include <math.h>
#include <sys/poll.h>
#include "gimbal_fastcom.h"


	/* ESCC/P Interface vars. */
	
	int escc = -1 ;				// ESCC/P Device Driver Handle. Init to -1 for device open failure check.
	char ddname[ 80 ] ;			// ESCC/P Device Driver Name.
	unsigned bytes_read ;		// Number of bytes actually returned from a DD read.
	char rx_data[4096] ;		// Receive data buffer (for DD read calls).
	unsigned long ddstatus ;	// Device Driver return status.

	/* Read data integrity check vars. */

	unsigned long dd_rx_buf_orun_cnt= 0 ;	// Count of times the DD internal receive buffer have been overrun (data lost).
	unsigned long read_cnt_bad		= 0 ;	// Count of times DD read call did not return 27 bytes (data lost).
	unsigned long read_crc_fail_cnt	= 0	;	// Count of read data crc failures (data invalid, therefore lost).
	unsigned long read_cnt			= 0 ;	// Count of times a valid read has occured.
	unsigned char mux_last_value	= 100 ;	// Used to track the mux counter value (rotates from 0 to 33). Init to 100 for tracking initialization.
	unsigned long mux_skip_cnt		= 0 ;	// Count of times the mux counter value skipped (data lost).

	/* 1 Hz data summing and conversion vars. */

	short dvx;
	short dvy;
	short dvz;
	short dtx;
	short dty;
	short dtz;

///////////////////////////////////////////////////////////////////////////////

	/* Create the DD name that will access Port 0 of the ESCC/P. */

int GIMBAL_open()
{

	sprintf( ddname, "/dev/escc0" ) ;

	/* Open the DD for reading, obtaining a handle to the DD. */

#if READ_NONBLOCK
	escc = open( ddname, O_RDWR | O_NONBLOCK ) ;
#else
	escc = open( ddname, O_RDWR ) ;
#endif

	if( escc == -1 ) {

		printf( "Cannot open %s.\n", ddname ) ;

		perror( NULL ) ;

		return -1;
	}

	/* Read the DD status and discard to clear any previous conditions. */

	if ( ioctl( escc, ESCC_IMMEDIATE_STATUS, &ddstatus ) == -1 ) {

		printf( "Failed initial ioctl ESCC_IMMEDIATE_STATUS call.\n" ) ;

		perror( NULL ) ;

		close( escc ) ;

		return -1;
	}

	return 1;
}

int GIMBAL_read(GIMBAL_DATA *state_dat) {

		/* Check for the DD for receive data overrun condition. */

		if ( ioctl( escc, ESCC_IMMEDIATE_STATUS, &ddstatus ) == -1 ) {

			printf( "Failed ioctl ESCC_IMMEDIATE_STATUS call.\n" ) ;

			perror( NULL ) ;

			close( escc ) ;

			return -1;
		}
		else {

			/* The DD has lost data !!!! */

			if ( ddstatus & ST_OVF ) dd_rx_buf_orun_cnt++ ;
		}

#if CHECK_RX_READY
		/* Check if the DD has any data. */
		
		if ( ioctl( escc, ESCC_RX_READY, &ddstatus ) == -1 ) {

			printf( "Failed ioctl ESCC_RX_READY call.\n" ) ;

			perror( NULL ) ;

			close( escc ) ;

			return -1;
		}
		else {

			if ( ddstatus == 0 ) {

				/* No data ready for reading. */

				/************************************************************************/
				/*																		*/
				/* WHERE TO PUT LOGIC THAT DOES WHATEVER TILL THERE IS DATA TO READ.	*/
				/*																		*/
				/************************************************************************/

			}
			else {
#endif
				/* Read a frame of data. */

				if ( ( bytes_read = read( escc, rx_data, FRAME_SIZE_REQUEST ) ) == -1 ) {
#if READ_NONBLOCK
					/* Ignore errno 11, "Resource temporarily unavailable",
					   the error returned when in non-blocking mode and there is no data. */

					if ( errno != 11 ) {
#endif
						printf( "Read failed, errno = %d.\n", errno ) ;

						perror( NULL ) ;

						close( escc ) ;

						return -1;
#if READ_NONBLOCK
					}
					else {

						/* Non-blocked call to read() has returned with no data read. */

						/************************************************************************/
						/*																		*/
						/* WHERE TO PUT LOGIC THAT DOES WHATEVER TILL THERE IS DATA TO READ.	*/
						/*																		*/
						/************************************************************************/
					}
#endif
				}
				else {

					/* Data has been read. */

					/* Check that the correct number of bytes were read. */

					if ( bytes_read != FRAME_SIZE_GOOD_RETURN ) {

						read_cnt_bad++ ;
					}
					else {

						/* Process the data only when a full frame has been read. */

						/* Check the data frame CRC. */

						if ( ( rx_data[ bytes_read - 1 ] & 0x20 ) == 0x00 ) {

							read_crc_fail_cnt++ ;
						}
						else {

							/* Process the data only when a valid CRC has been read. */

							/* Track the rotating mux counter (data lost check). */

							if ( mux_last_value == 100 ) {

								mux_last_value = rx_data[ 14 ] ;
							}
							else {

								if ( rx_data[ 14 ] == 0 ) {
						
									if ( mux_last_value != MUX_MAX_VALUE ) {

										mux_skip_cnt++ ;
									}

								}
								else {

									if ( ( rx_data[ 14 ] - mux_last_value ) != 1 ) {

										mux_skip_cnt++ ;
									}
								}

								mux_last_value = rx_data[14] ;
							}

							/* Add the delta vel and delta angle readings to their respective sums. */

							dvx = *(short *)&rx_data[ 0];
							dvy = *(short *)&rx_data[ 2];
							dvz = *(short *)&rx_data[ 4];
							dtx = *(short *)&rx_data[ 6];
							dty = *(short *)&rx_data[ 8];
							dtz = *(short *)&rx_data[10];

							//Conversion factors are fun...

							state_dat->dvx = dvx / pow(2,14);
							state_dat->dvy = dvy / pow(2,14);
							state_dat->dvz = dvz / pow(2,14);
							state_dat->dtx = dtx / pow(2,19);
							state_dat->dty = dty / pow(2,19);
							state_dat->dtz = dtz / pow(2,19);

						}
					}
				}
#if CHECK_RX_READY
			}
		}
#endif
		return 1;
}

int GIMBAL_close()
{

  /* Close the DD and exit the program. */
  /* This was closing escc but that is just an status int so I changed it to the name of the device.  */
  close( ddname ) ;

	return 1;
}
