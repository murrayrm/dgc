
#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#include <iostream>

#include <opencv/cv.h>
#include <opencv/cvaux.h>
#include <opencv/highgui.h>

#include "stereoSource.hh"

using namespace std;

int main(int argc, char *argv[]){
  stereoSource brightSource;
  brightSource.init(0,"config/stereovision/CamID.ini.short", "temp", "bmp");
  
  /*int index = brightSource.pairIndex();
  brightSource.grab("temp", "bmp", 0);*/

  CvRect partOfImage = cvRect(0,210,640,200);   //regionOfInterest
  double t0 = 1.0;    //period av sjalvsvaningar
  double k0 = 0.35;   // K da systemet sjalvsvanger
  //  for(double i=0.0; i<0.5; i+=.2){     //increasing the proportionalGain
  double Kp[] = {k0*0.5, k0*0.45, k0*0.6};
  double Kd[] = {0.0, 0.0, (k0*0.6*t0)/8.0};
  double Ki[] = {0.0, (k0*0.45*1.2)/t0, (k0*0.6*2.0)/t0};
  /*double Kp[] = {0.1, 0.1, 0.1};
  double Kd[] = {0.0, 0.0, 0.2};
  double Ki[] = {0.0, 0.5, 0.5};*/

  for(int i=0; i<3; ++i) {
    brightSource.grab();                 //AvgBrightness with AutoExposure
    double b = brightSource.getAvgBrightness(0,0,640,210,410);   //regionOfInterest
    //cout << "USING AUTOEXPOSURE. AvgBrightness = " << b;
    cout<< " ErrorWithAutoExposure = " << abs(b-128) << endl;

    brightSource.startExposureControl(partOfImage);   //using exposureControl
    cout << "ExposureControlGain == " << Kp[i] << ", " << Kd[i] << ", " << Ki[i]<< endl;
    brightSource.setExposureControl(Kp[i], Kd[i], Ki[i], 1.0);
    for( int k=0; k<=60 ; ++k){          // 40 "grabbings"
      brightSource.grab();
      b = brightSource.getAvgROIBrightness();
      //cout << " AvgBrightness = " << b;
      if(k%1 == 0){                      // printing out every error
	cout<< "  " << b-128 << " "; //printing out the error
      }
    } 
    brightSource.stopExposureControl();
    cout << " " << endl;
    cout << " " << endl;

    cout<< " Resetting Exposure"<<endl;
    for(int k=0; k<10; ++k) {
      brightSource.grab();
      double b = brightSource.getAvgBrightness(0,0,640,210,410);   //regionOfInterest
      //cout << "USING AUTOEXPOSURE. AvgBrightness = " << b;
      //cout<< " " << abs(b-128);
      cout.flush();
      usleep(300000);
    }
    //cout<<endl;
  }

  brightSource.stop();

  return 0;
}


