#include "trajSelector.h"
#include <iostream>

#include "specs.h"
#include <getopt.h>

using namespace std;

int main(int argc, char** argv)
{

  trajSelector* selector;

  int opt_silent = 0;

  static struct option long_options[] = {
    //Options that don't require arguments
    {"silent", no_argument, &opt_silent, 1}, {0,0,0,0}};

  readspecs();

  char* keyPtr = getenv("SKYNET_KEY");

  int key;

  if(keyPtr == NULL) //No SKYNET_KEY environment variable has been set.
    {
      cout<<"No Skynet environemnt variable has been set.  Input the Skynet key to use: "<<endl;
      cin>>key;
    }
  else
    {
      key = atoi(keyPtr);
    }

  int option_index = 0;

  int c = getopt_long_only(argc, argv, "", long_options, &option_index);

  if(c == '?')
    {
      cout<<"Unrecognized option; use --silent to run trajSelector in silent mode."<<endl;
      exit(-1);
    }
  else if(opt_silent == 1)
    {
      selector = new trajSelector(5, key, true);
    }
  else
    {
      selector = new trajSelector(5, key, false);
    }

  selector->select();
}
