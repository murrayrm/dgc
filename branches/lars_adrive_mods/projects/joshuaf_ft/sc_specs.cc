#include "sc_specs.h"

DEFINESPECS;

#include <fstream>
#include <sstream>
using namespace std;

#define PARSESPEC(type, name, val) \
	if(fieldname == #name) \
    linestream >> name; \
  else

#define PARSESPECS SPECLIST(PARSESPEC)

#define DEFAULTSPEC(type, name, val) \
	name = val;

#define DEFAULTSPECS SPECLIST(DEFAULTSPEC)

void readspecs(void)
{
	DEFAULTSPECS

	ifstream specsfile("sc_specsfile");
	string line;
	string fieldname;

	// do nothing if the specs file wasn't opened properly
	if(specsfile)
	{
		while(specsfile)
		{		
			getline(specsfile,line);
			istringstream linestream(line);
			linestream >> fieldname;

			PARSESPECS;
		}
	}
}
