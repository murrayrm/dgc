#include "Waypoint.hh"
using namespace std;

Waypoint::Waypoint(int m, int n, int p, double lat, double lon) : GPSPoint(m, n, p, lat, lon)
{
  this->checkpoint = false;
  this->stopSign = false;
}

Waypoint::~Waypoint()
{
}

int Waypoint::getCheckpointID()
{
  if(isCheckpoint())
    return this->checkpointID;
  else
    return 0;
}

void Waypoint::setCheckpointID(int checkpointID)
{
  this->checkpoint = true;
  this->checkpointID = checkpointID;
}

void Waypoint::setStopSign()
{
  this->stopSign = true;
}

bool Waypoint::isCheckpoint()
{
  return this->checkpoint;
}

bool Waypoint::isStopSign()
{
  return this->stopSign;
}
