#include "GPSPoint.hh"
using namespace std;

GPSPoint::GPSPoint(int m, int n, int p, double latitude, double longitude)
{
  segmentID = m;
  laneID = n;
  waypointID = p;
  this->latitude = latitude;
  this->longitude = longitude;
  entry = exit = false;
}

GPSPoint::~GPSPoint()
{
}

int GPSPoint::getSegmentID()
{
  return this->segmentID;
}

int GPSPoint::getLaneID()
{
  return this->laneID;
}

int GPSPoint::getWaypointID()
{
  return this->waypointID;
}

double GPSPoint::getLatitude()
{
  return this->latitude;
}

double GPSPoint::getLongitude()
{
  return this->longitude;
}

vector<GPSPoint*> GPSPoint::getEntryPoints()
{
  return entryPoints;
}

void GPSPoint::setEntry()
{
  this->entry = true;
}

void GPSPoint::setExit(GPSPoint* entryGPSPoint)
{
  this->exit = true;
  this->entryPoints.push_back(entryGPSPoint);
}

bool GPSPoint::isEntry()
{
  return this->entry;
}

bool GPSPoint::isExit()
{
  return this->exit;
}
