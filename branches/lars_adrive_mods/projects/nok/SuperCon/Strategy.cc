#include"Strategy.hh"
#include"Diagnostic.hh"

#include<iostream>
using namespace std;

///////////////////////////////////////////////////////////////////////////////
//Class used to map state transfers at compile time
CStrategyMapper *CStrategyMapper::pInstance;

CStrategyMapper::CStrategyMapper()
{
}

CStrategyMapper::CStrategyMapper(SCStrategy from, SCStrategy to, const char *reason)
{
  if(!pInstance)
    pInstance = new CStrategyMapper();

  Transition trans;
  trans.to = to;
  trans.from = from;
  trans.reason = reason;

  pInstance->m_transition.push_back(trans);
}


//Diagnostic used for name lookup
void CStrategyMapper::print(const CDiagnostic &diag)
{
  //Print all transitions
  if(!pInstance) {
    cout<<"No singleton instance of CStrategyMapper, are any transitions defined?"<<endl;
    return;
  }

  cout<<"All registered transitions"<<endl;

  for(vector<Transition>::iterator itr = pInstance->m_transition.begin(); itr!= pInstance->m_transition.end(); ++itr) {
    cout<<diag.strategyName(itr->from)<<"("<<itr->from<<") -> "<<diag.strategyName(itr->to)<<"("<<itr->to<<") - "<<itr->reason<<endl;
  }
}

//////////////////////////////////////////////////////////////////////////////
//Class declares virtual base class for strategies

//Default constructor - NOTE requires a pointer to the CStrategyInterface
//(sole) instance instantiated by the (sole) instance of CDiagnostic.
CStrategy::CStrategy()
  : m_ID(StrategyNone), m_pDiag(0)
{
}

//Destructor
CStrategy::~CStrategy()
{
  m_pDiag = 0;
}


/** COMMON STRATEGY LOOPING PERSISTENCE CHECKS **/
bool CStrategy::checkForPersistLoop( CStrategy* pCaller, const persistCheckAction checkAction, int currentCount ) {

  //This switch statement is not very elegant, however given that every
  //enum in the associated list needs a threshold, a more elegant, and more
  //efficient (code-readability) form doesn't seem obvious
  
  bool checkBool;
  checkBool = false;

  switch( checkAction ) {
    
  case persist_loop_estop:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_ESTOP_THRESHOLD );
    }
    break;
    
  case persist_loop_astateResponse:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_ASTATE_RESPONSE_THRESHOLD );
    }
    break;

  case persist_loop_gearChange:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_GEARCHANGE );
    }
    break;

  case persist_loop_trajfModeChange:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_TRAJFMODECHANGE );      
    }
    break;
    
  case persist_loop_trajfReverseFinished:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_TRAJF_REVERSE_FINISHED );            
    }
    break;

  case persist_loop_slowAdvanceApproach:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_SLOWADVANCE_APPROACH );
    }
    break;

  case persist_loop_UnseenObstWaitForStationary:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_UNSEENOBST_WAIT_FOR_STATIONARY );      
    }
    break;

  case persist_loop_waitstationary:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_WAIT_FOR_ALICE_STATIONARY );
    }
    break;

  case persist_loop_mapdeltasapplied:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_WAIT_FOR_MAPDELTASAPPLIED );
    }
    break;

  case persist_loop_clearmap:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_WAIT_FOR_MAPCLEAR );
    }
    break;

  case persist_loop_waitforDARPA:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_WAIT_FOR_DARPA_PAUSE_ENDOFRDDF );
    }
    break;

  case persist_loop_broadenRDDF:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_WAIT_FOR_BROADEN_RDDF );
    }
    break;

  case persist_loop_placeBlock:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_WAIT_FOR_PLACE_BLOCK );
    }
    break;
    
  case persist_loop_stepRoute:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_WAIT_FOR_STEP_ROUTE );
    }
  
  case persist_loop_alterWaypoint:
    {
      checkBool = ( currentCount > TIMEOUT_LOOP_WAIT_FOR_WAYPOINT );
    }

    
  default:
    {
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategy - invalid persist_loop_action!" );
      SuperConLog().log( STR, ERROR_MSG, "ERROR:CStrategy invalid persist_loop_action:%s", persistCheckAction_asString( checkAction ) );
    }
  }
  
  if( checkBool == true ) {
    //persist action has 'timed-out' - hence transition to NOMINAL
    (*pCaller).transitionStrategy( StrategyNominal, "ERROR: persist action timed-out --> NOMINAL" );
    SuperConLog().log( STR, ERROR_MSG, "ERROR CStrategy: persist action %s timed-out --> NOMINAL", persistCheckAction_asString( checkAction ) );
  }
  return checkBool;
}


/** COMMON STRATEGY TERMINATION METHODS **/
//Termination methods evaluate whether to LEAVE the current strategy, and
//return to the NOMINAL strategy, and are CONDITIONAL (i.e. IF a test
//evaluates to true, transition to nominal, otherewise proceed

//terminate current strategy IF planner is producing a trajectory that does
//NOT pass through either a TERRAIN OR SUPERCON ZERO-SPEED (each use
//different zeros) area
bool CStrategy::term_PlannerNotNFP( const SCdiagnostic &diag, const char* StrategySpecificStr ) {
  
  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TERMINATION METHOD (i.e. what caused the termination
  //ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];

  //the other part (the beginning section of the action string) is passed in the
  //argument to this method

  bool trueIFterm; //TRUE iff termination conditions met

  if ( diag.termPlnNotNFP ) {
    
    /* need to terminate current strategy and return to nominal strategy */
    sprintf(buf, "%s : PLN -> !NFP -> transition to Nominal", StrategySpecificStr);
    transitionStrategy( StrategyNominal, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );
    
    trueIFterm = true;
  } else {
    trueIFterm = false;
  }
  return trueIFterm;
}


// ? --> UNSEEN-OBSTACLE strategy conditional transition method
// NOTE: THIS IS CURRENTLY SPECIFIC TO DRIVING FORWARDS (see diagnostic conditional element)
bool CStrategy::trans_UnseenObstacle( CStrategyInterface &m_StrategyInterface, const SCdiagnostic &diag, const char* StrategySpecificStr ) {

  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method

  bool trueIFtrans;

  if ( diag.transUnseenObstacle ) {
    //Unseen obstacle detected - stop immediately
    m_StrategyInterface.stage_superConEstop( estp_pause );
    
    /* need to transition to UnseenObstacle strategy */
    sprintf(buf, "%s : Detected Unseen Obstacle -> transition to UnseenObstacle", StrategySpecificStr);
    transitionStrategy( StrategyUnseenObstacle, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );

    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;
}
 

/** COMMON STRATEGY TRANSITION METHODS (move from current strategy to another one) **/
//Conditional methods that return a boolean, =TRUE iff we should transition to
//the strategy for which this is the transition method, otherwise they return FALSE

// ? --> GPS-REACQ (GPS-reacquisition) strategy conditional transition method
bool CStrategy::trans_GPSreAcq( const SCdiagnostic &diag, const char* StrategySpecificStr ) {
  
  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method

  bool trueIFtrans;

  if ( diag.transGPSreAcq ) {
    
    /* need to transition to GPSreAcq strategy */
    sprintf(buf, "%s : AState signalled GPS re-acq. -> transition to GPS-reacquisition", StrategySpecificStr);
    transitionStrategy( StrategyGPSreAcq, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );

    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;
}


// ? --> NOT SUPERCON ESTOP PAUSE strategy conditional transition method
bool CStrategy::trans_EstopPausedNOTsuperCon( const SCdiagnostic &diag, const char* StrategySpecificStr ) {
  
  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method

  bool trueIFtrans;
  
  if ( diag.transEstopPausedNOTsuperCon ) {

    /* need to transition to EstopPausedNOTsuperCon strategy */
    sprintf(buf, "%s : Detected adrive/DARPA estop pause -> NOTsuperConEstopPause", StrategySpecificStr);
    transitionStrategy( StrategyEstopPausedNOTsuperCon, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );

    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;
}


// ? --> LTURN REVERSE strategy conditional transition method
bool CStrategy::trans_LturnReverse( const SCdiagnostic &diag, const char* StrategySpecificStr ) {
  
  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method

  bool trueIFtrans;
  
  if ( diag.transLturnReverse ) {

    /* need to transition to LturnReverse strategy */
    sprintf(buf, "%s : Reversing action required -> LturnReverse", StrategySpecificStr);
    transitionStrategy( StrategyLturnReverse, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );

    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;  
}

// ? --> PLANNER FAILED NO TRAJ strategy conditional transition method
bool CStrategy::trans_PlnFailedNoTraj( const SCdiagnostic &diag, const char* StrategySpecificStr ) {

  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method - the string should detail the STRATEGY & STAGE at
  //which the method is being called in the following format:
  //"STRATEGYNAME - StageSTAGENAME" e.g. "SlowAdvance - estop_pause"

  bool trueIFtrans;
  
  if ( diag.transPlnFailedNoTraj ) {

    /* need to transition to PlnFailedNoTraj strategy */
    sprintf(buf, "%s : Detected Planner failed to output traj -> PlnFailedNoTraj", StrategySpecificStr);
    transitionStrategy( StrategyPlnFailedNoTraj, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );
    
    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;
}


// ? --> END OF RDDF strategy conditional transition method
bool CStrategy::trans_EndOfRDDF( const SCdiagnostic &diag, const char* StrategySpecificStr ) {

  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method - the string should detail the STRATEGY & STAGE at
  //which the method is being called in the following format:
  //"STRATEGYNAME - StageSTAGENAME" e.g. "SlowAdvance - estop_pause"

  bool trueIFtrans;
  
  if ( diag.transEndOfRDDF ) {

    /* need to transition to EndOfRDDF strategy */
    sprintf(buf, "%s : Detected closest WP = last WP in RDDF -> EndOfRDDF", StrategySpecificStr);
    transitionStrategy( StrategyEndOfRDDF, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );

    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;
}


// ? --> SLOW ADVANCE strategy conditional transition method
bool CStrategy::trans_SlowAdvance( const SCdiagnostic &diag, const char* StrategySpecificStr ) {

  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method

  bool trueIFtrans;
  
  if ( diag.transSlowAdvance ) {

    /* need to transition to SlowAdvance strategy */
    sprintf(buf, "%s : Detected Planner -> NFP from Nominal -> SlowAdvance", StrategySpecificStr);
    transitionStrategy( StrategySlowAdvance, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );

    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;
}


// ? --> OUTSIDE RDDF strategy conditional transition method
bool CStrategy::trans_OutsideRDDF( const SCdiagnostic &diag, const char* StrategySpecificStr ) {
  
  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method
  
  bool trueIFtrans;
  
  if( diag.transOutsideRDDF ) {
    
    /* need to transition to OutsideRDDF strategy */
    sprintf(buf, "%s : Alice is outside the RDDF! -> OutsideRDDF", StrategySpecificStr);
    transitionStrategy( StrategyOutsideRDDF, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );
    
    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  
  return trueIFtrans;
}

// ? --> ROAD BLOCK strategy conditional transition method
bool CStrategy::trans_RoadBlock( const SCdiagnostic &diag, const char* StrategySpecificStr ) {
  
  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method
  
  bool trueIFtrans;
  
  if( diag.transRoadBlock ) {
    
    /* need to transition to OutsideRDDF strategy */
    sprintf(buf, "%s : Alice has met a Road Block -> RoadBlock", StrategySpecificStr);
    transitionStrategy( StrategyRoadBlock, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );
    
    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  
  return trueIFtrans;
}

// ? --> U-TURN strategy conditional transition method
bool CStrategy::trans_Uturn( const SCdiagnostic &diag, const char* StrategySpecificStr ) {
  
  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method
  
  bool trueIFtrans;
  
  if( diag.transUturn ) {
    
    /* need to transition to OutsideRDDF strategy */
    sprintf(buf, "%s : Alice needs to make a U-turn -> Uturn", StrategySpecificStr);
    transitionStrategy( StrategyUturn, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );
    
    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  
  return trueIFtrans;
}

// ? --> INTERSECTION strategy conditional transition method
bool CStrategy::trans_Intersection( const SCdiagnostic &diag, const char* StrategySpecificStr ) {
  
  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char buf[LOGGING_BUFFER_SIZE];
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method
  
  bool trueIFtrans;
  
  if( diag.transIntersection ) {
    
    /* need to transition to OutsideRDDF strategy */
    sprintf(buf, "%s : Alice is at an intersection -> Intersection", StrategySpecificStr);
    transitionStrategy( StrategyIntersection, buf );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, buf );
    
    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  
  return trueIFtrans;
}
  
//sometime strategy termination/transition condition evaluation method
bool CStrategy::checkAnytimeEntryConds( CStrategyInterface *pStrategyInterface, const SCdiagnostic *pdiag, const char* StrategySpecificStr ) {
  
  bool checkBool;
  checkBool = false;

  //NOTE: The ORDER of the transition/termination methods is IMPORTANT! - this is
  //because if any transition/termination method performs its transition/termination
  //then it changes the record of the 'current strategy' being executed, and also
  //leaves() the current strategy, and enters() the new strategy.
  //
  //THE CURRENT-STRATEGY SHOULD BE CHANGED AT MOST *ONCE* BY THIS METHOD
  //(or any method within ONE stepForward() execution) - hence the structure
  //of the code below, *and* the methods are in DECREASING PRIORITY ORDER.

  if( checkBool == false ) {
    checkBool = trans_OutsideRDDF( (*pdiag), StrategySpecificStr );
  }
  if( checkBool == false ) {
    checkBool = trans_EstopPausedNOTsuperCon( (*pdiag), StrategySpecificStr );
  }
  if( checkBool == false ) {
    checkBool = term_PlannerNotNFP( (*pdiag), StrategySpecificStr );
  }
  if( checkBool == false ) {
    checkBool = trans_UnseenObstacle( (*pStrategyInterface), (*pdiag), StrategySpecificStr );
  }
  if( checkBool == false ) {
    checkBool = trans_GPSreAcq( (*pdiag), StrategySpecificStr );
  }

  return checkBool;
}


/** HELPER FUNCTIONS FOR INHERITED CLASSES  **/

//This should really be changed so that it isn't a form of wrapper for transitionStrategy in the
//CDiagnostic class...
void CStrategy::transitionStrategy(SCStrategy to, const char *reason)
{
  if(m_pDiag) {
    m_pDiag->transitionStrategy(to, reason);
  } else {
    cerr<< "ERROR: CStrategy::transitionStrategy - Pointer to diagnostics class not instansiated"<<endl;
    SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategy::transitionStrategy - Pointer to diagnostics class not instansiated" );
  }
}

void CStrategy::doSetDoubleState(double SCstate::*pState, double val)
{
  if(m_pDiag) {
    m_pDiag->setDoubleState(pState, val);
  } else {
    cerr<< "ERROR: CStrategy::doSetDoubleState - Pointer to diagnostics class not instansiated" << endl;
    SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategy::doSetDoubleState - Pointer to diagnostics class not instansiated" );
  }
}

void CStrategy::doSetIntState(int SCstate::*pState, int val)
{
  if(m_pDiag) {
    m_pDiag->setIntState(pState, val);
  } else {
    cerr<<"ERROR: CStrategy::doSetIntState - Pointer to diagnostics class not instansiated"<<endl;
    SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategy::doSetIntState - Pointer to diagnostics class not instansiated" );
  }
}

char* CStrategy::strprintf( const char *str, ... ) {
  
  //has to be static so that a char* can be returned (as the char[]
  //will continue to exist after leaving the scope of this method)
  //this in turn is necessary because the non-const char* arg taken
  //by vsnprintf()
  static char buffer[LOGGING_BUFFER_SIZE];

  //Construct the string passed into the method in printf() form into a string
  //in the log buffer, NOTE - vsnprintf() like the printf() family will only
  //write 'size' characters into the buffer, and will return the number of
  //characters that were not written in the case when the buffer was too small
  va_list lst;  
  va_start(lst, str);
  vsnprintf( buffer, sizeof(buffer)-1, str, lst);
  va_end(lst);

  return buffer;
}
