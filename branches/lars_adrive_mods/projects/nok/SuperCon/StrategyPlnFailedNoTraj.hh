#ifndef STRATEGY_PLN_FAILED_NO_TRAJ_HH
#define STRATEGY_PLN_FAILED_NO_TRAJ_HH

//SUPERCON STRATEGY TITLE: plannerModule fails to output a valid (meets PLN's constraints) trajectory - ANYTIME STRATEGY (blocking for all strategies EXCEPT for EstopPausedNOTsuperCon
//HEADER FILE (.hh)
//DESCRIPTION: Strategy to deal with the situation in which the planner fails
//to find a trajectory which satisfies it's constraints, under these conditions
//NO trajectory is sent.
//NOTE - the orginal purpose of this strategy was to handle situations in
//which the deceleration constraint was not satisfied (i.e. there was an
//obstacle that we could not slow down fast enough to avoid)

#include "Strategy.hh"
#include "sc_specs.h"
#include "stage_iterator.hh"

namespace s_plnfailed {

#define PLNFAILED_STAGE_LIST(_) \
  _(set_tf_speedcaps, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(clear_maps, ) \
  _(wait_new_traj, )
DEFINE_ENUM(plnfailed_stage_names, PLNFAILED_STAGE_LIST)

}

using namespace std;
using namespace s_plnfailed;

class CStrategyPlnFailedNoTraj : public CStrategy
{

/** METHODS **/
public:

  CStrategyPlnFailedNoTraj() : CStrategy() {}  
  ~CStrategyPlnFailedNoTraj() {}


  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

  
  /** HELPER METHODS **/
  
  //Check if the planner has output a new traj (trajs which are output must
  //meet the planners feasibility criteria), if a new traj is output then
  //transition to nominal
  bool checkForPlnOutputNewTraj( bool checkBool, const SCdiagnostic *m_pdiag );

};

#endif
