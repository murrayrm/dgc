#ifndef _CSpatialPlanner_H_
#define _CSpatialPlanner_H_
#include "traj.h"
#include "frames/coords.hh"
#include "CVectorMap.h"
#include "CCElementaryPath.h"
#include "CCLandMark.h"
#include "VehicleState.hh"
#include <rddf.hh>
#include <math.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <vector>

#define 	CSpatialPlanner_INTEGRATION_NODES				10 
#define 	CSpatialPlanner_CONST_FILE							"CSpatialPlannerconsts.txt"
#define		CSPATIALPLANNER_MAX_LANDMARKS 					1000
#define		CSPATIALPLANNER_SEGMENT_LENGTH 				10.0
#define		CSPATIALPLANNER_SEGMENT_POINTS 				100
#define		CSPATIALPLANNER_NUM_BRANCHES						3
#define		CSPATIALPLANNER_MAX_DEPTH    						3
#define		CSPATIALPLANNER_EPSILON 								0.2
#define		CSPATIALPLANNER_MAX_ITERATIONS  				5
#define		CSPATIALPLANNER_MAX_TIME  							0.1
#define		CSPATIALPLANNER_MIN_LENGTH							4.0
#define		CSPATIALPLANNER_LENGTH_INTERVAL					4.0
#define 	CSPATIALPLANNER_ABS_MAX_BRANCHES 				11
#define 	CSPATIALPLANNER_CLOSEST_NODE 						10.0
#define 	CSPATIALPLANNER_MIN_SEPARATION 					6.0
#define 	CSPATIALPLANNER_SEPARATION_COEFF 				0.4
#define 	CSPATIALPLANNER_SPEED_COEFF_A 					0.3 
#define 	CSPATIALPLANNER_SPEED_COEFF_B 					0.0 
#define 	CSPATIALPLANNER_MIN_DIST_SQD						25.0
#define 	CSPATIALPLANNER_MAX_F										0.0
#define 	CSPATIALPLANNER_MIN_MIN_DIST_SQD				2.0

using namespace std;

/** The main class for the spatial planner. Includes the search and explore algorithm, and the overall
Ariadne's Clew/A* algorithm implementation. */
class CSpatialPlanner
{
public:	

	/*------------------------------- Methods ----------------------------*/
	
	/** pre-compute clothoids with different sharpnesses and lengths */
	void generatePaths();

	/** database of precomputed clothoids */
	CCElementaryPath m_pathDB[CSPATIALPLANNER_SIGMAS][CSPATIALPLANNER_LENGTHS];

	/** Initialisation function: start again from scratch. Clears the tree and positions the start node. */ 
	void initialise(VehicleState *pState);
	
	/** generates lambda by optimization: the landmark furtherest from any other */
	void explore(CCLandMark *m);

	/** tries to find a bielementary path from lamba to finish */
	bool search(CCLandMark *p1);

	/** main loop */
	bool run(VehicleState *pState, CCLandMark target, double time=0.1,bool r=false);
	
	/** Saves all the nodes in the tree to file. */
	void saveTree(ostream *f); 

	/** Return the generated trajectory. */
	CTraj* getTraj(); 
	
	/** output the trajectory */
	bool save(NEcoord *pos);

	/** constructor */
	CSpatialPlanner(CVectorMap *map);
	
	/*----------------------- Variables --------------------*/
	
	/** final path */
	CTraj m_traj;
	
	/** object-orientated vectorised map representing the area we are allowed to drive in */
	CVectorMap *m_pVectorMap;

	/** start and finish vehicle states */
	CCLandMark *m_start, m_finish, m_oldFinish;
	
	/** whether reversing is currently allowed, e.g. for u-turns/parking */
	bool m_reverseAllowed;
	
	/** The minimum squared distance from all other nodes that a new node is allowed to be placed. 
	This is reduced as the algorithm proceeds to increase the resolution of the map sampling. */
	double m_minDistSqd;

	/** Have we initialised yet? */ 
	bool m_initialised;
	
	/** number of landmarks that we have created */
	int m_numLandmarks;
};
#endif
