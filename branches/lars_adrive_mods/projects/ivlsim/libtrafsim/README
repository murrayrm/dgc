
libtrafsim is...

libtrafsim is a traffic simulation tool. It consists of C++ classes,
which are meant to be used through a Python wrapper. There is doxygen
documentation for all of its classes, as well as a (woefully) limited
number of doctests and some epydoc documentation. I make no guarantees
about bugs, or lack thereof, but I'll be available for technical
support throughout the year. To get started figuring out what's what,
build and run as follows:

libtrafsim/Debug $ make all
libtrafsim $ python -i GenericApp.py

NOTE: If you get strange errors involving invalid pointers, try
"export MALLOC_CHECK_=1" in the shell before running GenericApp.py

Tutorial

GenericApp is a barebones program that makes some of libtrafsim's
tools available in a marginally intuitive way. The source is very
short and simple, so you should have no problem making your own
programs using it as a template. Once you've gotten it running, you
should see a black window captioned "Generic libtrafsim App", as well
as the normal Python prompt.

Splines

The first thing you might like to do is to draw some
splines. libtrafsim uses Catmull-Rom splines, which consist of four or
more control points, and a squiggle that cleverly manages to pass
through each of them. To start a new spline, hold down CTRL and click
anywhere in the window. That should leave a blue dot. Then, hold down
SHIFT, and keep clicking. Eventually, a red line spline should show
up. You can move the control points around by clicking and dragging,
you can add more points by holding down SHIFT, and you can start in on
an entirely new spline by CTRL-clicking again. To delete a control
point, hold the 'd' key and SHIFT-click on it. To delete an entire
spline, hold the 'd' key and CTRL-click on any of its control points.

Navigation

You can click anywhere on the screen to see the coordinates of that
point. To move the "camera" around, you can hold down the 'z'
key. Then, click with the left mouse button and drag the mouse to pan,
and click with the right mouse button and drag the mouse to zoom in
and out.

Roads

Unfortunately, I was far too lazy to create a clever GUI-based method
to do anything more than construct splines. However, it's still pretty
easy to do everything else. First of all, notice that one spline is
always red, and all the others are white. The red one is the currently
selected spline, and it's accessible in the variable
'app.editor.selectedSpline'.

	A Confusing Alternative: If you want to grab all the splines
you've created so far and stick them into a list, you can try
'app.environment.getObjectsByClassID('Spline').values()'. However,
this will actually only return a list of Objects, which you must then
explicitly downcast to splines, like so: 'splines =
[Spline.downcast(obj) for obj in objects]'. Please pardon Python's
stupidity on this matter; you have to do this for any object you fetch
by its class ID or name.

So anyway, let's say you want to make a road. Grab a spline you made
(let's say you put it into a variable called 'spline'). What you can
do now is something like this:

road = Road()
app.environment.addObject(road, 'MyRoad')
road.initialize(app.environment, # the environment to create the road in
		spline, 	 # the spline to use for geometry data
		1, 1, 		 # resp. number of forward and reverse lanes
		5.0)		 # the width of each lane, in meters

Most objects in libtrafsim can be created via an argumentless
constructor in which nothing much is done. Once we have an
uninitialized road object, we add it to the environment and give it
the name 'MyRoad'. Then, we set it up with the initialize
function. Once you've done all this you should see something that
looks like a road in the viewport. You can fiddle with the parameters,
or try calling some of the Road's functions (which are documented
fully in the doxygen docs). Python is great for learning by
experimentation.

Intersections

Try making four roads, all of which converge at one spot and are
'facing' in that direction. The simplest way to do this is just to
make the splines, and then do something like this:

splines = [Spline.downcast(s) for s in app.environment.getObjectsByClassID('Spline').values()]
roads = [Road() for s in splines]
for road in roads: app.environment.addObject(road)

for road, spline in zip(roads, splines):
    road.initialize(app.environment, spline, 1, 1, 5.0)

This script just takes all the splines in your environment and makes a
two-lane road out of each of them. Once you have your roads (and make
sure that they're oriented facing each other!), you can make an
intersection.

xi = XIntersection()
app.environment.addObject(xi, 'MyIntersection')
for road in roads: xi.hookStart(road)

xi.computeGeometry(app.environment)

The call to computeGeometry() may take a second or two, since it does
a lot of preprocessing.

So that's the basic idea behind making roads and intersections. There
is a number of other things you can do with intersections, such as
changing their geometry parameters. You can also try
YIntersections. Just remember that computeGeometry() finalizes the
intersection's state, so you should make that call last of all. After
you've calculated the geometry, all you can really change is the
positioning of stop signs (via the setStop() functions).

Saving and Loading

All libtrafsim objects know how to serialize themselves, and an
Environment keeps track of pointers and resolves all dependencies
between independently loaded objects. So the bottom line is that all
you have to do to get a complete textual description of any static
Environment is just to use Python's str() function. To get a
functioning environment back from such a description, use the static
method Environment.parse(). To make things a bit easier, GenericApp
just lets you do this:

app.save('env.txt')
... and then, in another session ...
app.load('env.txt')

The Point of All This (Cars)

So once you've created a static environment to your satisfaction, you
can save it and then try adding some cars. The absolute simplest way
to add a car is as follows:

c = Car()
c.switchLane( road,    # some road in your environment
	      0,       # a lane on that road, indexed from left to right
	      0.0 )    # the point on the lane to start at, from 0.0 to 1.0

The car will appear where you told it and set off driving straight as
far as it can. If it can't go straight anymore, it will make an
arbitrary turn. If it reaches a dead end, it gets removed from the
simulation and deleted. Note that GenericApp runs at 10x real-time
speed by default. You can change this value at 'app.timingFactor'.

You can try out changing the car's properties by instantiating a
CarProperties object and using the setProperties() method. You can
also try changing the car's default path like this:

c.setPath( CarPath( [CROSS_STRAIGHT, CROSS_LEFT, CROSS_RIGHT] ) )

If you do the above, the car will alternate going straight, then left,
then right, then straight again, etc.

Car Factories

A single car isn't all that exciting, of course. If you want to create
actual streams of traffic, try making a CarFactory.

cf = CarFactory()
cf.setPosition(road, 0, 0.0)   
cf.setCreationRate(1.0)
cf.setRemainingCars(10)
app.environment.addObject(cf)

The arguments to setPosition() are analogous to those of a car's
switchLane() method. setCreationRate() specifies that one car should
be created each second (if possible), and setRemainingCars() tells the
factory to make a total of ten cars. If you set the remaining number
of cars to -1, the factory will keep making cars forever. Once you add
the factory to the environment, it will go ahead and start creating
cars.

Of course, the default CarFactory creates a stream of cars with the
exact same parameters. To make more interesting randomized streams of
traffic, use the setCarProperties() method. A single CarFactory can
make cars follow only a single path, but you can create more than one
CarFactory and assign each a different path via the setPath()
method. Using several car factories on even a small road network, you
can create all sorts of interesting and unpredictable traffic
patterns. Try removing the stop signs from some of your intersections
to create major and minor traffic streams. Depending on the
CarProperties you set, you can make some fairly pathological drivers,
and can sometimes cause crashes and huge pileups at intersections.

External Cars

As an example of how you might plug Alice into the simulation, I've
written a small script in CarDriver.py. This script lets you move an
ExternalCar with a simplistic keyboard control scheme.

First of all, before using the ExternalCar class, you must create a
LaneGrid. Load up an environment with some roads and such, then do
this:

lg = LaneGrid()
lg.partition(app.environment, 	# the environment to partition
	     5.0,		# the unit size, in meters, of the grid
	     Vector(-100,-100), # the bottom left corner of your environment's world (fill in your own value here)
	     Vector(100, 100))  # the top right corner of your environment's world (fill in your own value here)
app.environment.addObject(lg)

Now, create an ExternalCar, and switch the editor from Spline editing
mode to CarDriver mode:

ec = ExternalCar()
ec.initialize(app.environment)
app.editor = CarDriver(ec)

You might want to set app.timingFactor to 1.0 for this. You should see
a red car appear at the world origin. You can drive it around with the
arrow keys (yeah, it's not even vaguely realistic, so sue me). The
other cars are aware of the ExternalCar, and will behave appropriately
to avoid crashes and such. When you go to implement an actual
controller for this using Alice's software, be sure to read the
documentation for the Intersection class, as it requires a bit of
special handling to work correctly.

To switch back to spline editing mode, you can do this:

app.editor = SplineEditor(app.environment)

ROAMS Integration -- Exporting Static Scenes

Adding roads from a libtrafsim environment to a ROAMS Dem is pretty
straightforward. Import DemExporter.py, and try this on an environment
with roads and intersections:

DemExporter.ExportRoadData(app.environment,  	# the environment to export
			   (-100, -100),	# the bottom left corner of your environment's world (fill in your own value here)
			   (512, 512), 		# the size in pixels of the output Dem (fill in your own value here)
			   1.0,			# the scaling factor (in Dem pixels per meter)
			   'test.png')		# a filename

This should write a black-and-white image of your geometry to the
given filename. Then, to apply it to an actual Dem, you can:

DemExporter.MergeImageIntoDem(myDem, 		# a Dem
			      'test.png', 	# the filename
			      0.4)		# the distance to indent the roads (in meters)

This is entirely un-fancy, so if someone wants to do something more
clever on this front, that should pose no problem. DemExporter should
serve as a good sample of how one might go about extracting geometric
data from a libtrafsim environment.

ROAMS Integration -- Runtime Simulation

I haven't yet gotten around to porting the ROAMS glue from the
original Python version of libtrafsim to the new C++ version. It's
only 50 or so lines of code, but I need a working ROAMS sandbox to do
the job. When you guys need this to work, drop me a line and I'll come
and make the conversion.
