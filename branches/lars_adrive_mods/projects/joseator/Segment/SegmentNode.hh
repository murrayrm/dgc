/*!****************************************************************
 *  Project    : Learning and adaptation                          *
 *  File       : Segment.hh                                       *
 *  Description:        *
 *  Author     : Jose Torres                                      *
 *  Modified   : 7-27-06                                          *
 *****************************************************************/ 

#ifndef SEGMENTNODE_HH
#define SEGMENTNODE_HH

struct SegmentNode
{
  double roadEdge_A;
  double roadEdge_B;
  double directionDivider;
  double A_laneDividerOne;
  double A_laneDividerTwo;
  double A_laneDividerThree;

  double B_laneDividerOne;
  double B_laneDividerTwo;
  double B_laneDividerThree;

  double stopSign_A;
  double stopSign_B;
  double featureOne;
  double featureTwo;

  SegmentNode* next;
  SegmentNode* previous;
};

#endif
