#include "Test.hh"

Test::Test(int skynet_key) : CSkynetContainer(SNastate, skynet_key)
{
   
}



Test::~Test()
{

}



void Test::createPseudoCheckpoints()
{
  //Set up socket for skynet broadcasting:
  int sendSocket  = m_skynet.get_send_sock(SNrndfCheckpoint); 
  //sendSocket = 114;
  cout<< sendSocket <<endl;
  if (sendSocket < 0)
    {
      cerr << "TraversedPath::TraversedPath(): skynet listen returned error" << endl;
    }
  
  Checkpoint  my_command;
  
  
  while(1)
    {
      UpdateState();
      my_command.startX = m_state.Easting;
      my_command.startY = m_state.Northing;
      cout<< "sending checkpoint" <<endl;
      
      //my_command.x = my_command.x + 1;
      //my_command.y = my_command.y + 1;       
      DGCusleep(10000000);

      UpdateState();
      my_command.endX = m_state.Easting;
      my_command.endY = m_state.Northing;
      m_skynet.send_msg(sendSocket, &my_command, sizeof(my_command), 0);
      DGCusleep(10000000);
    }
}



int main() 
{
  
  int sn_key = 0;
  
  cout<<"Run this when driving desert roads..."<<endl;
  char* pSkynetkey = getenv("SKYNET_KEY");
  cout<<"got skynet key"<<endl;
  if ( pSkynetkey == NULL)
    {
      cerr << "SKYNET_KEY environment variable isn't set." << endl;
    } else
      {
	sn_key = atoi(pSkynetkey);
      }
  
  Test t(sn_key);
  t.createPseudoCheckpoints();
  cout<<"after creating object" <<endl;  
  
  return 0;
}    
