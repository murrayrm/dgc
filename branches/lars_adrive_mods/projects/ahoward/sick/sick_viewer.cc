/*
 Desc: Viewer for the SICK laser
 Author: Andrew Howard
 Date: 9 Oct 2006
 CVS: $Id: sicklms200.cc,v 1.60 2006/10/05 20:05:03 gerkey Exp $
*/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <float.h>
#include <pthread.h>
#include <FL/Fl.H> 
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>

#include <jplv/Fl_Glv_Window.H>

#include "sick_driver.h"


// Application data for calibration utility
typedef struct
{
  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;

  // Window for GL
  Fl_Glv_Window *world_win;

  // Menu options
  Fl_Menu_Item *action_pause;

  // Should we quit?
  bool quit;

  // Display needs redrawing
  bool dirty;

  // Laser driver
  sick_driver_t *driver;

  // Laser angular constants
  float ang_res, ang_off;

  // Laser data buffer
  int num_ranges;
  float ranges[181];
  int retros[181];

  // FOO
  int num_export;

} app_t;



// Local functions
void app_on_exit(Fl_Widget *w, int option);
void app_on_action(Fl_Widget *w, int option);
void app_on_world_draw(Fl_Glv_Window *win, app_t *self);


// Commands
enum
{
  APP_ACTION_PAUSE = 0x1000,
  APP_ACTION_EXPORT,
};

// Set up the menu
Fl_Menu_Item menuitems[] =
{
  {"&File", 0, 0, 0, FL_SUBMENU},    
  {"E&xit", FL_CTRL + 'q', (Fl_Callback*) app_on_exit},
  {"Pause", ' ', NULL, (void*) APP_ACTION_PAUSE, FL_MENU_TOGGLE},
  {"Export", 'e', (Fl_Callback*) app_on_action, (void*) APP_ACTION_EXPORT},
  {0},
  {0},
};


// Initialize GUI
int app_init(app_t *self)
{
  int i;
  int w, h;

  w = 640;
  h = 480;
  
  self->mainwin = new Fl_Window(w, h + 30, "SICK viewer");
  self->mainwin->user_data(self);

  // Set up the main window
  self->menubar = new Fl_Menu_Bar(0, 0, 3 * w + 3 * h, 30);
  self->menubar->user_data(self);
  self->menubar->menu(menuitems);
  self->world_win = new Fl_Glv_Window(0, 30, w, h, self, (Fl_Callback*) app_on_world_draw);
  self->world_win->user_data(self);
  self->mainwin->end();

  // Make world window resizable 
  self->mainwin->resizable(self->world_win);

    // Hook up menu items
  for (i = 0; i < (int) (sizeof(menuitems) / sizeof(menuitems[0])); i++)
  {
    if (menuitems[i].user_data_ == (void*) APP_ACTION_PAUSE)
      self->action_pause = menuitems + i;
  }

  return 0;
}


// Handle menu callbacks
void app_on_exit(Fl_Widget *w, int option)
{
  app_t *self;

  self = (app_t*) w->user_data();
  self->quit = true;

  return;
}


// Handle menu callbacks
void app_on_action(Fl_Widget *w, int option)
{
  app_t *self;

  self = (app_t*) w->user_data();

  switch (option)
  {
    case APP_ACTION_EXPORT:
    {
      int i;
      FILE *file;
      char filename[1024];

      snprintf(filename, sizeof(filename), "scan-%02d.txt", self->num_export);
      fprintf(stderr, "writing %s", filename);

      file = fopen(filename, "w");
      assert(file);

      printf("scan %d ", self->num_export);
      fprintf(file, "scan %d ", self->num_export);      
      for (i = 0; i < self->num_ranges; i++)
      {
        if (self->retros[i])
        {
          printf("%d %f %d ", i, self->ranges[i], self->retros[i]);
          fprintf(file, "%d %f %d ", i, self->ranges[i], self->retros[i]);
        }
      }
      printf("\n");
      fprintf(file, "\n");
      self->num_export++;
      
      fclose(file);
      break;
    }
    default:
      break;
  }

  return;
}


// Re-draw the world
void app_on_world_draw(Fl_Glv_Window *win, app_t *self)
{
  int i;
  float size;
  float px, py, pz;
  
  self->dirty = false;

  size = 0.50;
  glLineWidth(3);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();
  glLineWidth(1);

  
  glColor3f(1, 0, 1);
  glLineWidth(3);
  glBegin(GL_LINE_STRIP);
  for (i = 0; i < self->num_ranges; i++)
  {
    px = self->ranges[i] * cos(i * self->ang_res + self->ang_off);
    py = self->ranges[i] * sin(i * self->ang_res + self->ang_off);
    pz = 0;
    glVertex3f(px, py, pz);
  }
  glEnd();

  for (i = 0; i < self->num_ranges; i++)
  {
    GLUquadric *quad;
    quad = gluNewQuadric();
    if (self->retros[i])
    {
      px = self->ranges[i] * cos(i * self->ang_res + self->ang_off);
      py = self->ranges[i] * sin(i * self->ang_res + self->ang_off);
      pz = 0;
      glPushMatrix();
      glTranslatef(px, py, pz);
      gluSphere(quad, 0.20, 16, 16);
      glPopMatrix();
    }
    gluDeleteQuadric(quad);
  }
  
  return;
}



// Handle idle callbacks
void app_on_idle(app_t *self)
{
  if (self->dirty)
    self->world_win->redraw();
  else
    usleep(0);
  
  return;
}


// Thread control data
static pthread_t thread;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


// Laser reading thread
void *read_laser_loop(app_t *app)
{
  int num_ranges;
  float ranges[181];
  int retros[181];

  while (!app->quit)
  {
    // Read laser data
    if (sick_driver_read(app->driver, sizeof(ranges), &num_ranges, ranges, retros) != 0)
      continue;

    // TESTING
    if (false)
    {      
      int i, d;
      usleep(100000);
      d = rand() % 8;
      for (i = 0; i < 181; i++)
      {
        ranges[i] = fabs(sin(i * M_PI/180)) * d;
        retros[i] = 0;
      }
      num_ranges = 181;
    }

    // Copy to main thread
    pthread_mutex_lock(&mutex);
    if (!app->action_pause->value())
    {
      app->num_ranges = num_ranges;
      memcpy(app->ranges, ranges, sizeof(app->ranges));
      memcpy(app->retros, retros, sizeof(app->retros));
      app->dirty = true;
    }
    pthread_mutex_unlock(&mutex);
  }

  return NULL;
}


int main(int argc, char *argv[])
{
  app_t *app;
  char *port;

  if (argc < 2)
  {
    fprintf(stderr, "usage: sick-viewer <PORT> (e.g., /dev/ttyS1)\n");
    return -1;
  }

  app = (app_t*) calloc(1, sizeof(app_t));

  // Parse options
  port = argv[1];
  
  // Open driver 
  app->driver = sick_driver_alloc();
  assert(app->driver);
  if (sick_driver_open(app->driver, port, 38400, 38400, 100, 10) != 0)
    return -1;
  app->ang_res = +M_PI/180;
  app->ang_off = -M_PI/2;
  
  // Initialize gui
  if (app_init(app) != 0)
    return -1;
  app->mainwin->show();

  // Kick off thread for reading laser
  if (pthread_create(&thread, NULL, (void* (*) (void*)) read_laser_loop, app) != 0)
    return -1;
  
  // Set idle callback
  Fl::add_idle((void (*) (void*)) app_on_idle, app);

  // Run
  while (!app->quit)
    Fl::wait();

  // Shut down laser thread
  app->quit = true;
  if (pthread_join(thread, NULL))
    fprintf(stderr, "unable to join thread: %s\n", strerror(errno));

  // Clean up
  sick_driver_close(app->driver);
  sick_driver_free(app->driver); 
  free(app);
  
  return 0;
}
