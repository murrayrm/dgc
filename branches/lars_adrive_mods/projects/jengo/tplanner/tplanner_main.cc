#include "TPlanner.hh"

/*! The default RNDF file */
//const char RNDF_FILENAME[32] = "../rndf_files/co_union_RNDF.txt";
/*! The default astateSim file */
//const char SIM_FILENAME[32] = "../logs/gps_20060824_122454.dat";
/*! The default lane boundaries file. */
//const char LANES_FILENAME[33] = "../rndf_files/co_union_LANES.txt";


/*! \file
 *
 * \brief An example of a way to run TPlanner
 *
 * This program creates one instance of TPlanner, initializes it, and lets
 * it run through a set of goals until the user quits the program.
 */

/*! Prints a help message to stdout, detailing the possible command line
 * arguments. */
void printHelp()
{
  cout<<"Usage: tplanner [options]"<<endl;
  cout<<"  --help, -h   Display this message"<<endl;
  cout<<"  --rndf, -r [RNDF_filename]   Give the rndf file to use."<<endl;
  cout<<"  --interactive, -i   Run in interactive mode."<<endl;
  cout<<"  --sim, -s [sim_filename] [lanes_filename]  Run in simulation mode"<<endl;
  cout<<endl;
  cout<<"Note: Giving the rndf file is not optional."<<endl<<endl;
}

int main(int argc, char *argv[]) {

  char input;

  TPlanner tplanner;

  ////////////////////////////
  // Load command line args //
  ////////////////////////////
  
  bool rndf_given = false;

  while ((optind < argc) && (argv[optind][0]=='-')) {
    string temp = argv[optind];
    if (temp=="-h" || temp=="--help") {
      printHelp();
      exit(0);
    }
    else if (temp=="-i" || temp=="--interactive") {
      tplanner.interactive = true;
    }
    else if (temp=="-r" || temp=="--rndf") {
      rndf_given = true;
      optind++;
      tplanner.rndf_filename = argv[optind];
    }
    else if (temp=="-s" || temp=="--sim") {
      tplanner.simulation = true;
      optind++;
      tplanner.sim_filename = argv[optind];
      optind++;
      tplanner.lanes_filename = argv[optind];
    }
    else {
      printHelp();
      exit(0);
    }
    optind++;
  }
  
  if (!rndf_given) {
    printHelp();
    exit(0);
  }

  //////////////////////
  // Store skynet key //
  //////////////////////

  cout<<endl;  
  tplanner.skynet_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if ( pSkynetkey == NULL)
    {
      cerr << "SKYNET_KEY environment variable isn't set." << endl;
    } else
      {
	tplanner.skynet_key = atoi(pSkynetkey);
      }
  cerr << "Constructing skynet with KEY = " << tplanner.skynet_key << endl;

  /////////////////////
  // Set up tplanner //
  /////////////////////

  tplanner.setup();

  /////////////////////////
  // Run some iterations //
  /////////////////////////

  cout<<"Continue? (y/n): ";
  cin>>input;
  if (input == 'y') {
    tplanner.printRoad();
    tplanner.runIteration();
  }

  return 0;
}
