
%use this one for plotCH
function [cars, posObjects, sizeObjects] = segmentImage(ranges)

%use this one for trackCars
%function [cars, posObjects] = segmentImage(ranges)

% this expects as input an array of distances, in cm 
% (the raw format that the ladars spit out)
% and outputs the points that could potentially correspond to cars

% Currently, this code uses the idea that separate objects can be defined
% by being bordered by discontinuities

% NOTE: see p. 10 in notebook for notes about this
% many of the assumptions made in this code will not be valid when dealing
% with multiple cars and/or occlusion. eg:
%% min and max length for cars
%% unbroken scans
%% cars defined by midpoint of line btwn start/end ranges

len = length(ranges);
curRange = ranges(1); 
sf = 4;         % scaling factor for threshold computation 

%NOTE: these values are being changed to play with tracking of ALL objects.
%      may wish to change back if only going to track cars
%      for plotCH
% sizeThresh =0;% 5;   % minimum # of points for object to be considered a possible car
% minLength = 0;%50;   % minimum dimension (in cm) for us to consider an object a car
% maxLength = 10000;%700;  % maximium dimension (in cm) for us to consider an object a car
% for trackCars
sizeThresh = 3;   % minimum # of points for object to be considered a possible car
minLength = 50;   % minimum dimension (in cm) for us to consider an object a car
maxLength = 700;  % maximium dimension (in cm) for us to consider an object a car

plotHere = true;  % whether we want to plot from this fxn

%for now, we're using a 180 degree field of view, with 1 deg spacing
%hopefully, we can get .5 deg spacing...
%
% for now, using 1m as max diff btwn scans to show up as diff object

numObjects = 1; % this keeps track of number of diff objects identified
currSize = 1;   % this keeps track of the 
posObjects = [1,1];   % 1st col is the index of 1st range in object
                      % 2nd col is index of last range in object
                      % the indices match those in the ranges array
diffs = abs(diff(ranges)); %this returns an array of size len-1, containing
                           % the differrences between adjacent elements
       
%FIXME: add convex hull requirements to range discontinuities!
for i=1:1:(len-1)
    distThresh = max(10, ranges(i)*.0175*sf);  % scales threshold by distance from ladar (cm)
    if diffs(i) > distThresh   % if the distance is too large to be same object
        [i,diffs(i),distThresh];
        currSize=1;                          % reset size of new object
        posObjects(numObjects,2) = i;        % set end point of prev object
        
        numObjects = numObjects + 1;         % increment total number of objects
        sizeObjects(numObjects) = 1;         % initialize the size of the new object
        posObjects(numObjects,1) = i+1;      % set beginning point of new object
    else                      % if the distance is small enough to be same object
        currSize = currSize + 1;             % increment size of current object
        sizeObjects(numObjects)=currSize;    % set size of current object
    end
end
posObjects(numObjects,2) = len;  


temp = [1:len];
angles = 2*pi*temp/360;
x = cos(angles).*ranges;
y = sin(angles).*ranges;

% plot the ranges in cartesian form 
if plotHere == true
    hold off;
    plot(x,y,'+');
    hold on;
end

% now, we apply the assumptions about what a "car" is, and create output
numCars = 0;
for i=1:1:numObjects   
    
    % an object must have a minimum number of points to be considered
    if sizeObjects(i) >= sizeThresh
        % gets the start and ending coordinates of the line segment
        % defining this object
        xy = [x(posObjects(i,1)), y(posObjects(i,1)); x(posObjects(i,2)),y(posObjects(i,2))];
        len = sum((xy(1,:)-xy(2,:)).^2).^0.5; 
        lenObjects(i) = len;
        
        % making assumptions about the possible dimensions of a car
        if len >= minLength  && len <= maxLength
            numCars = numCars + 1; % we've found a "car"
            %FIXME: should be defined by more: the end points of 2 line
            %    segments. however, this needs a more sophisticated filter than
            %    range discontinuity and size of object
            cars(numCars,:) = [xy(1,:), xy(2,:)]; %car defined by endpoints of line
            %cars(numCars,:) = [mean(xy)];  % car will be defined by line's midpoint
            
            if plotHere == true
                plot(xy(:,1),xy(:,2),'LineWidth',1,'Color','green');  % for diff colored lines, use 
                                                                      % [rand(1), rand(1), rand(1)]
                                                                      
                % plot beginnings and ends of lines
                plot(xy(1,1),xy(1,2),'o','LineWidth',1,'Color','black');
                plot(xy(2,1),xy(2,2),'o','LineWidth',1,'Color','red');
            end
        end
    end
end






