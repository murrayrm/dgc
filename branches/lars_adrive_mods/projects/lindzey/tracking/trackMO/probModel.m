function [prob]=probModel(angles,ranges,model)

s = 1; %side length, in cm
buffer = 5; %how many pts on each side to add

size(ranges);
size(angles);

xx = ranges.*cos(angles);
yy = ranges.*sin(angles);

plot(xx,yy,'go');

size(xx);
size(yy);

[segs,is,js,foo] = size(model);

for k=1:1:length(angles)
    modelEval = zeros(is,js); %each point gets clean model
    if ranges(k)>1
        dk = ranges(k);
        tk = angles(k);
        sd = 5; %variance of range measures is ~5cm
        st = .5*pi/180; %want variance of angles to be .5 degree

        P = ((dk^2)*(st^2)/2) * [2*sin(tk)*sin(tk), -sin(2*tk); -sin(2*tk), 2*cos(tk)*cos(tk)] + ((sd^2)/2) * [2*cos(tk)*cos(tk), sin(2*tk);sin(2*tk), 2*sin(tk)*sin(tk)];
        mu = [xx(1,k);yy(1,k)];
        for l=1:1:segs
            for i=1:1:is
                for j=1:1:js
                    x = [model(l,i,j,1);model(l,i,j,2)];
                    modelEval(i,j) = modelEval(i,j)+(det(2*pi*P)^-.5) * exp(-.5*(x-mu)' * inv(P) * (x-mu));

                end
            end
        end
    end
%figure();

    modelEval = modelEval*s*s;
    t1 = sum(modelEval);
    t2 = sum(t1);
    prob(k) = t2;
end
