#ifndef GIMBALCONTROL_HH
#define GIMBALCONTROL_HH

#include <iostream>
#include <fstream>
#include <sstream>
#include <deque>
#include <iterator>
#include <math.h>
#include <stdlib.h>
#include <string>

#include "frames/coords.hh"
#include "frames/frames.hh"
#include "DGCutils"
#include "SkynetContainer.h"
#include "StateClient.h"
#include "VehicleState.hh"
#include "CMap.hh"
#include "CMapPlus.hh"
#include "TrajTalker.h"
#include "AliceConstants.h"
#include "MapConstants.h"

//#include "../../jengo/astateSim/astateSim.hh"

#define PI 3.141592653
#define DEG2RAD (PI/180)

#define TIMEOUT 50000
#define NUM_COMMANDS_DEQUEUE 10

#define TRUE 1
#define FALSE 0

const char* LOGFILENAME = "log";
const char* STATETRAJFILENAME = "statestraj";
const char* STATEINTFILENAME = "statesint";

using namespace std;

typedef enum {RECEIVED,
	      VALID,
	      INVALID,
	      SENT,
	      DISCARDED} commandState;

typedef enum {OVERRIDE,
	      TRACKING,
	      INTERSECTION,
	      UTURN,
	      NONE} commandMode;

typedef enum {SIMULATION,
	      MATLAB,
	      PERFECT_HORIZON} outputOption;

typedef enum {} gimbalMode;//check yhis

typedef struct{

  NEDcoord UTM;
  XYZcoord XYZ_Vehicle;
  
  RPYangle RPYAngle_Gimbal;
  RPYangle RPYSpeed_Gimbal;
  // RPYangle RPYAccel_Gimbal;
  
  unsigned long long timestampIssued;
  unsigned long long timestampReceived;
  unsigned long long timestampSent;
  
  unsigned int sendingAttemptsLeft;
  
  commandMode mode;
  commandState state;

  void init()
  {
    UTM.N = UTM.E = UTM.D = XYZ_Vehicle.X = XYZ_Vehicle.Y = XYZ_Vehicle.Z = 0;
    RPYAngle_Gimbal.R = RPYAngle_Gimbal.P = RPYAngle_Gimbal.Y = RPYSpeed_Gimbal.R = RPYSpeed_Gimbal.P = RPYSpeed_Gimbal.Y = 0;
    timestampIssued = timestampReceived = 0;
    mode = NONE; 
  }

} gimbalCommand;

class gimbal: public CStateClient	     
{

public:
  
  deque<gimbalCommand> commandsDequeue;
  int _QUIT;

  /*init*/

  //just initializes the gimbal frame and the skynetcontainer  
  gimbal(int SkynetKey);
  
  //basic destructor
  ~gimbal();

  //defines the max angles, rates, accels that the gimbal can take (perhaps create config file? location stored in opts struct?)
  void define_maxParameters();

  /*queue work*/

  //processes command
  void processCommand(gimbalCommand & commandToProcess);
  
  //processes dequeue
  void processCommandsDequeueThread();
  
  //optimizes dequeue before sending
  void optimizeCommandsDequeue();
  
  //RPYangle NED2RPY_Gimbal(NEDcoord UTM);
  //XYZcoord NED2XYZ_Vehicle(NEDcoord UTM);
  
  // take a xyz vehicle relative coord and calculate gimbal angles for pointing at that location
  RPYangle XYZ_Vehicle2RPY_Gimbal(XYZcoord desiredXYZ);
  
  //checks if desired command is ok with the gimbal params
  bool check_valid(gimbalCommand & commandToCheck);
  
  /*comms*/
  
  //receives commands from anywhere
  void receiveDataThread_Commands();
  
  //receives gimbal status
  void receiveDataThread_GimbalState();

  //sends command to gimbal
  void sendDataThread_Commands();

  unsigned long long send2Simulation(gimbalCommand & commandToSend);
  unsigned long long send2Perfect_Horizon(gimbalCommand & commandToSend);

  void sendDataThread_GimbalState();

  /*logs*/
  void logOrDisplay(gimbalCommand & commandToLogOrDisplay);
  void writeCommand(gimbalCommand & commandToWrite, ostream & outstream);
  void writeGimbalState(ostream & outstream);

  void ActiveLoop();

private:

  pthread_mutex_t processCommandsMutex, sendCommandsMutex, receiveCommandsMutex;

  // Angles in degrees
  RPYangle maxRPYAngle, minRPYAngle;
  // Speed in degrees/second
  RPYangle maxRPYSpeed;
  // Accel in degrees/(second^2)
  // RPYangle maxRPYAccel(0.0,0.0,0.0);

  RPYangle currentRPYAngle, currentRPYSpeed;
  //RPYangle currentRPYAccel(0,0,0);
  RPYangle RPYinit;
  XYZcoord XYZinit;

  //rot matrix in radians
  frames frame;
  
  gimbalMode mode;
  
  /*options*/

  outputOption optOutput;
  
  //int optTimber;  -> TBD
  int optDisplay, optSparrow, optSkynet, optLog;
  
  //if override is on,a command can override all other commands waiting to be sent
  int optOverride;
};














/* need to think about the structure used for queuing commands
   probably should use interpolation(maybe mpm does it?)
   and weights
*/  

/* should log with timestamps:
   
commands received --> calculate frequency? max ~= 10Hz
commands sent (command id, numcommands? to check how many aren't valid against the sent ones)
current orientation, speed, accel of gimbal
operating mode
gimbal destination
queue? stack? ...
max parameters (once, don't think they'll change - maybe only with operating mode)
*/

/* should display: ~= everything logged (last commands sent/received only and part of queue only)    */

/* still need to define skynet messages (commands!):
   
input:  xyz or ned(vectors?)
RPYSpeed
RPYAccel?
priority system?

output: valid RPYAngle
valid RPYSpeed
valid RPYAccel?
priority system?
operating mode?            
*/

#endif
