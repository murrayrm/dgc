#ifndef EDGE_HH_
#define EDGE_HH_
using namespace std;

class Vertex;

/*! Edge class. Represents an edge of a graph. An edge contains an edge weight
 *  and a vertex pointer to the vertex it is connected to.
 * \brief The Edge class used in the Graph class.
 */
class Edge
{
public:
/*! All edges have a vertex pointer to the vertex it is connected to. */
	Edge(Vertex* next);
	virtual ~Edge();
  
/*! Returns the edge weight of THIS. */
  double getWeight();
  
/*! Returns a vertex pointer to the vertex THIS is connected to. */
  Vertex* getNext();
  
/*! Sets the edge weight of THIS. */
  void setWeight(double weight);
  
/*! Sets the vertex pointer to the vertex THIS is connected to. */
  void setNext(Vertex* next);
  
private:
  double weight;
  Vertex* next;
};

#endif /*EDGE_HH_*/
