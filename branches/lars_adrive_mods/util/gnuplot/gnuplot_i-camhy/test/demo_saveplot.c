#include <math.h>
#include <stdio.h>
#include "gnuplot_i.c"
/*#include "gnuplot_i.h"*/
#include "nrutil.c"

int main(int argc, char *argv[]) {
  gnuplot_ctrl    *h1,*h2,*h3 ; /*Gnuplot-Controls*/
  double *        a;
  double *        d;
  int             i ;

  d=dvector(1,50);
  a=dvector(1,50);
  h1 = gnuplot_init() ;
  h2 = gnuplot_init();
  h3 =gnuplot_init();

  gnuplot_set_title(h1,"Display on screen");
  gnuplot_setstyle(h1,"lines"); 
  gnuplot_set_title(h2,"Saved to ps - first plot");
  gnuplot_setstyle(h2,"lines"); 
  gnuplot_set_title(h3,"Saved to ps - second plot");
  gnuplot_setstyle(h3,"lines"); 

  for (i=1 ; i<=50 ; i++) {
    a[i] = (double) i;
    d[i] = (double)(i*i) ;
  }
 
  //The following plots will be displayed on screen

  gnuplot_plot_xy(h1,a,d,50,"parabola");
  gnuplot_plotrange_xy(h1, a,d, 1, 50, "First plot") ;

  //The next plot will be saved to the file test.ps   
  gnuplot_savetops(h2,"test.ps"); //Before plot-statement
  gnuplot_plotrange_xy(h2,a,d,1,50,"parabola");

  //The next plot will be saved to another file
  gnuplot_savetops(h3,"test2.ps");
  gnuplot_plotrange_xy(h3, a,d, 1, 50, "Second plot") ;
  

  printf("press ENTER to continue\n");
  while (getchar()!='\n') {} 

  gnuplot_close(h1) ;
  gnuplot_close(h2) ;
  gnuplot_close(h3) ;
 
  free_dvector(d,1,50);
  return 1;
}
