#ifndef TAHOE_HH
#define TAHOE_HH

#include <gazebo.h> // gz_position_t
#include <gazebo/Body.hh>
#include <gazebo/Model.hh>
#include <gazebo/HingeJoint.hh>
#include <gazebo/Geom.hh>

/**
 * Gazebo model for simulating the Chevy Tahoe we used in the 2004 race.
 * The model is created out of the ClodBuster model that ships with Gazebo.
 * It has 4WD and Ackerman steering on the front pair. The old position 
 * estimation based on odometry is stripped away and the exact real state
 * (position and rotation i read directly from Gazebo and is available through
 * the position3d device). For commanding the speed and steering, use a position
 * device.
 *
 * Installation
 * -# Run 'make' to compile
 * -# See /dgc/gazebo/worlds/tahoe_demo.world for usage of the SickLMS221 model.
 * @author Henrik Kjellander
 *
 * $Id$
 */
class Tahoe : public Model
{
public:
  /** Constructor
   * @param world the world the object shall be created in
   */
  Tahoe( World *world );
  
  /** Destructor */
  virtual ~Tahoe();

  /** Load the model */
  virtual int Load( WorldFile *file, WorldFileNode *node );

  /** Initialize the model */
  virtual int Init( WorldFile *file, WorldFileNode *node );

  /** Finalize the model */
  virtual int Fini();

  /** Update the model state */
  virtual void Update( double step );

private:
  /** Update the odometry */
  void UpdateOdometry( double step );

  /** Load ODE stuff */
  int OdeLoad( WorldFile *file, WorldFileNode *node );

  /** Initialize ODE */
  int OdeInit( WorldFile *file, WorldFileNode *node );

  /** Finalize ODE */
  int OdeFini();

  /** Initialize the external interface */
  int IfaceInit();

  /** Finalize the external interfaces */
  int IfaceFini();

  /** Get commands from the external interface */
  void IfaceGetCmd();
  
  /** Update the data in the external interface */
  void IfacePutData();
  
  /** ODE component */
  Body *body;
  /** ODE component */
  Geom *bodyGeoms[4];
  /** ODE component */
  Body *wheels[4];
  /** ODE component */
  Body *stubs[4];
  /** ODE component */
  HingeJoint *wjoints[4];
  /** ODE component */
  HingeJoint *sjoints[4]; 
  
  // External interfaces
  /** Device used for setting the speed and steering and for getting state data*/
  gz_position_t *iface;

  // Tahoe attributes:
  /** Robot size attributes */
  double wheelSep, wheelDiam, mainLength;
  /** Wheel speeds (left and right wheels are paired) */
  double wheelSpeed[2];
  /** Odometric pose estimate */
  double odomPose[3];
  /** Encoder counts */
  double encoders[2];
  /** Raw encoder counts */
  bool raw_encoder_position;
};

#endif
