#ifndef VIDEORECORDER_HH
#define VIDEORECORDER_HH

#include "CTimberClient.hh"
#include "DGCutils"
#include "smart_counter.hh"
#include <cv.h>

#include "pid.hh"

//Multithreaded videorecorder class, running against a firewire camera,
// grabbing images and storing them with timber
//Also allowes outside access to latest image (which is updated at 30hz,
// or highest allowed by cpu)

class SDL_Surface;      //Forward declaration
class CFirewireCamera;  //Forward declaration

class CVideoRecorder : public CTimberClient
{
 public:
  CVideoRecorder(int skynet_key, bool sparrow);
  ~CVideoRecorder();
  
 public:
  /*
  **
  ** Camera handling
  **
  */
  void setCamera(int num);
  void setCamera(const char *uid);
  void setCamera(CFirewireCamera *pCamera);
  CFirewireCamera *getCamera();
  void closeCamera();

 public:
  /*
  **
  ** Accessors
  **
  */
  int width() const;
  int height() const;
  void imageCopy(IplImage *image);   //Get a copy of the image
  void imageCopyBW(IplImage *image); //Get a copy of the b&w image

  bool terminated() const;

  double logFramerate() const;
  double cameraFramerate() const;
  double displayFramerate() const;

  long long bytesLoged() const;
  double bytesLogRate() const;
  bool display() const;

 public:
  /*
  **
  ** Setters
  **
  */
  enum Resolution {
    R640x480 =0,
    R320x240
  };

  void setResolution(Resolution res);
  void setLogFramerate(double framerate);
  void setDisplayFramerate(double framerate);
  void setDisplay(bool display);

 protected:
  /*
  **
  ** Helper functions
  **
  */
  //Thread reading from camera, exposure controlling and saving to file
  void recorderThread();
  bool getImage();
  void displayImage();
  void logImage();

  void calcBWHist(int *hist);   //Calculate the histogram over the ROI in the BW image

  void setDisplayNoLock(bool display);   //Used internally when already locked
 protected:
  /*
  **
  ** Data members
  **
  */

  //General data
  pthread_mutex_t m_mutex;         //General mutex for lock on images
  pthread_mutex_t m_write_mutex;   //Lock on writing to image
  pthread_mutex_t m_first_image_mutex;
  pthread_mutex_t m_camera_mutex;

  bool m_terminate;

  CFirewireCamera *m_pCamera;
  CFirewireCamera *m_pInternalCamera;   //Internally created camera instance

  bool m_first_image;             //bool signaling this is first image
  IplImage *m_image;
  IplImage *m_imageBW;
  unsigned char *m_image_buf;     //Image buffer for bayer converted image

  double m_image_avg;
  double m_image_ROI_avg;
  CvRect m_ROI;

  bool m_auto_exposure;
  double m_exposure_level;
  Cpid m_exposure_pid;

  double m_whitebalance_bu;
  double m_whitebalance_rv;

  //Log data
  smart_counter<long long> m_bytes_loged;
  FILE *    m_log_index_file;

  //Display data
  bool m_display_inited;
  SDL_Surface *m_screen;
  SDL_Surface *m_surf;

  bool m_show_ROI;

  //Histogram data
  bool m_show_hist;
  bool m_hist_eq;
  int m_hist[256];

  //Framerate & framerate counters
  double m_log_interval;
  double m_display_interval;

  smart_counter<int> m_display_cnt;
  smart_counter<int> m_log_cnt;
  smart_counter<int> m_camera_cnt;

 protected:
  /*
  **
  ** Sparrow interface functions
  **
  */
  void set_display_strings();
  void set_loged_bytes();
  void set_log_rate();
  double getter_desired_framerate(int type);
  void setter_desired_framerate(double *p, double c, int type);

  void setter_exposure(double *p, double v);

  void setter_whitebalance_bu(double *p, double v);
  void setter_whitebalance_rv(double *p, double v);

  void setter_show_hist(bool *p, bool v);

  void display_mode_notification(int mode);
};



#endif
