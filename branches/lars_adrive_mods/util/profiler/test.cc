
#include"profiler.hh"
#include<math.h>

#include<pthread.h>

#include<iostream>
using namespace std;

int buf[10000];


void *calc_thread(void *)
{
  for(int i=0; i<500; ++i) {
    CProfile p("calcs2");
    
    double d= 345524+i*414321;
    for(int j=0; j<100000; ++j) {
      d = sqrt(d);
      d *= j;
      while(d>2)
	d = sqrt(d);
    }
    
    if((i%100)==0)
      printf("2: %i/500\n", i);
  }

  return 0;
}

int main()
{
  pthread_t th;

  pthread_create(&th, NULL, &calc_thread, 0);

  double d;
  for(int i=0; i<500; ++i) {
    {
      CProfile p("calcs");
      
      d= 345524+i*414321;
      for(int k=0; k<10; ++k) {
	CProfile p("internal");
	for(int j=1000*k; j<1000*(k+1); ++j) {
	  d = sqrt(d);
	  d *= exp((double)j+1);
	}
      }
    }
    
    {
      CProfile p("mem");
      for(int j=0; j<100000; ++j) {
	buf[j%10000] = j*buf[(j+5000)%10000];
      }
    }
    if((i%100)==0)
      printf("1: %i/500\n", i);
  }

  CProfile::print();
}

