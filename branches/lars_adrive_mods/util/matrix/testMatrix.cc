#include "Matrix.hh"
#include <iostream.h>
#include <stdlib.h>

int main() {

  //Example of things you can do with Matrices:

    Matrix foo1(3,3);

    double foo1elems[] = {1, 3, 1,
                         7, 2, -4,
			 2.1, 8, 3};

    foo1.setelems(foo1elems);

    Matrix foo2(3,3);
    foo2.setelem(0,0,4);
    foo2.setelem(0,1,2);
    foo2.setelem(0,2,-5);
    foo2.setelem(1,0,1.3);
    foo2.setelem(1,1,7);
    foo2.setelem(1,2,3);
    foo2.setelem(2,0,9.2);
    foo2.setelem(2,1,0);
    foo2.setelem(2,2,-2);

    Matrix bar1(3);
    
    double bar1elems[] = {1, 2, 3};

    bar1.setelems(bar1elems);

    Matrix bar2(3);
    bar2.setelem(0, 4);
    bar2.setelem(1, 3);
    bar2.setelem(2, -2.5);

    cout << "Foo1 = " << endl;
    foo1.print();
    cout << "Foo2 = " << endl;
    foo2.print();
    cout << "Bar1 = " << endl;
    bar1.print();
    cout << "Bar2 = " << endl;
    bar2.print();

    Matrix tmp1;

    tmp1 = foo1 * bar2;

    cout << "Foo1 * Bar2 = " << endl;
    tmp1.print();

    tmp1 = bar2.transpose() * foo1;

    cout << "Bar2.transpose * Foo1 = " << endl;
    tmp1.print();


    tmp1 = foo2.inverse() * bar1;

    cout << "Foo2.inverse * Bar1 = " << endl;
    tmp1.print();


    tmp1 = (foo2 / 2.0 ) * (foo1.inverse() * bar1) * 3.0;

    cout << "(Foo2 / 2) * (Foo1.inverse * bar1) * 3.0  = " << endl;   
    tmp1.print();

  //Example of doing a transform:

  /* A brief explanation of points and transforms:
     
     A transformation made up of a rotation and a translation
     can be represented as a single 4x4 matrix.  However, in
     order for this transform to work, your point must be
     represented as a length 4 column vector.

     The 4th element of this vector is 1 if you want to transform
     a point, while the 4th element of this vector is 0 if you
     want to transform a vector.

     The tranform matrix takes the form:

     | R  t |
     |      |
     | 0  1 |

     Where R is actually a 3x3 rotation matrix, t is a 3x1
     translation and the 0 is actually 3 0's necessary to pad 
     the matrix.
     
     Multiplying the transform by an appropiately formed point p
     will give: R p + t, thus rotating the point and adding
     the translation.

     If this doesn't make sense to you, find a book on transforms

     Transform is used by passing it a matrix of size 4x4
     
  */

  // Create a point: {1.0, 0.0, 0.0}
  double xval = 1.0;
  double yval = 2.0;
  double zval = 3.0;

  Matrix pnt(4);
  pnt.setelem(0, xval);
  pnt.setelem(1, yval);
  pnt.setelem(2, zval);
  pnt.setelem(3, 1.0); //The added 1 that pads the vector as a point

  cout << "Pnt = " << endl;
  pnt.print(); // Print for debugging purposes

  Matrix transform1(4,4);
  makeTransform(transform1, 0, 3.14159265/2, 0, 0, 0.5, 0); // Rotate about x by pi/2 and shift along y axis by 0.5

  Matrix transform2(4,4);
  makeTransform(transform2, 0, 0, 3.14159265/4, 0, 0, -2); // Rotate about x by pi/2 and shift along z axis by 0.5

  Matrix transform3 = transform2 * transform1;

  Matrix pnt2a = transform1 * pnt;

  cout << "Pnt2a  = " << endl;
  pnt2a.print();

  Matrix pnt2 = transform2 * pnt2a;

  Matrix pnt3 = transform3 * pnt;

  cout << "Pnt2  = " << endl;
  pnt2.print(); // Print the matrix for debugging purposes.
  cout << "Pnt3  = " << endl;
  pnt3.print();

  double xvalnew = pnt2.getelem(0);
  double yvalnew = pnt2.getelem(1);
  double zvalnew = pnt2.getelem(2);
  

  return 0;
}
