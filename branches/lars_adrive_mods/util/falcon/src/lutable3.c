#include <stdio.h>
#include "lutable.h"

#ifdef MSDOS
#include <alloc.h>
#else
#include <malloc.h>
#endif



#define LU_DEBUG

/*
 * Accessor functions for the lookup table.
 */

int lu_dimension(LU_DATA *l){
  if (l == (LU_DATA *) 0) return -1;
  return l->ndim;
}

int lu_outputs(LU_DATA *l){
  if (l == (LU_DATA *) 0) return -1;
  return l->nout;
}

int lu_interp(LU_DATA *l){
  if (l == (LU_DATA *) 0) return -1;
  return l->ninterp;
}

int lu_elements(LU_DATA *l){
  if (l == (LU_DATA *) 0) return -1;
  return l->elements;
}

int lu_coordinate(LU_DATA *l, int row, double *coord){
  int i;

  if (l == (LU_DATA *) 0) return -1;

  if (row >= l->elements) return -2;  /* Out of range */

  for (i=0; i < lu_dimension(l); i++){
    *(coord + i) = *( *(l->coords + row) + i);
  }

  return 0;
}  
 
