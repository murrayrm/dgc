@node reference,,lpv,Top
@chapter Reference

This section contains a listing of all of the functions defined in the
@code{Falcon} library (@emph{incomplete}).

@c
@c Each entry in this section is formatted as follows
@c
@c 	Node description
@c	@bullet@ @code{Command name}
@c	Command description (@quotation)
@c
@c The three sections should be separated by spaces to get things to
@c look right in the printed manual.
@c
@c An empty tempate for a typical entry is contained in 'template.ref'
@c

@menu
* servo_cleanup::       permanantly disable the servo routine
* servo_disable::       disable the servo routine
* servo_enable::        enable the servo routine
* servo_setup::         set up a servo to run at a given rate
* servo_start::         obsolete: use servo_setup
* servo_stop::          obsolete: use servo_cleanup
@end menu


@c -------- servo_disable
@node servo_disable,servo_enable,,reference
@cindex servo_disable

@bullet{} @code{servo_disable}

@example
int servo_disable()
@end example

@quotation
@code{servo_disable} temporarily disables the servo routine.  It can be
restarted by a call to  
@code{servo_enable}.  Returns @code{-1} if a servo loop has not been
setup by a valid call to @code{servo_setup}.
@end quotation

@c -------- servo_enable
@node servo_enable,,servo_disable,reference
@cindex servo_enable

@bullet{} @code{servo_enable}

@example
int servo_enable()
@end example

@quotation
@code{servo_enable} enables the timer interrupts and begins calling the
routine specified in @code{servo_setup} at the chosen rate.
Returns @code{-1} if a servo loop has not been
setup by a valid call to @code{servo_setup}.
@end quotation

@c -------- servo_setup
@node servo_setup,,,reference
@cindex servo_setup

@bullet{} @code{servo_setup}
@example
int servo_setup(loop, rate, flags)
int (*loop)();
int rate, flags;
@end example

@quotation
@code{servo_setup} sets up an interrupt routine to call the user
function @code{loop()} at the given rate.  It returns the actual rate of
the servo loop, which is guaranteed to be at least as fast as the
desired rate.  The timers on the Motorola 133XT boards limit the rate to
be in the range 25 Hz to 300 kHz.  If the specified rate is outside of
this range, @code{servo_setup} returns @code{-1}.  The servo routine is
not actually started until @code{servo_enable} is called.

The @code{flags} argument is used to select different options for the
servo module.  It is currently not used.
@end quotation

@c -------- servo_start
@node servo_start,,,reference
@cindex servo_start

@bullet{} @code{servo_start}
@example
int servo_start(loop, rate)
int (*loop)();
int rate;
@end example

@quotation
This function is now obsolete.  Use @code{servo_setup} instead.
@end quotation

@c -------- servo_stop
@node servo_stop,,,reference
@cindex servo_stop

@bullet{} @code{servo_stop}

@example
int servo_stop()
@end example

@quotation
The @code{servo_stop} is obsolete.  Use the @code{servo_cleanup}
function instead.
@end quotation

@c -------- servo_cleanup
@node servo_cleanup,,,reference
@cindex servo_cleanup

@bullet{} @code{servo_cleanup}

@example
int servo_cleanup()
@end example

@quotation
@code{servo_cleanup} removes the interrupt handler and permanently
disables the servo loop.  Returns @code{-1} if
a servo loop has not been setup by a valid call to @code{servo_start}.
@end quotation
