#ifndef _MAP_ACCESS_H_
#define _MAP_ACCESS_H_

#include "CMap.hh"

double getTimeMapValueGrownNoDiff(CMap* pMap, int mapLayer, bool bPassableNodata,
																	double initSpeed,
																	double Northing, double Easting,
																	double sinyaw, double cosyaw,
																	double len);
void getContinuousMapValueDiffGrown(int kernelWidth,
																		CMap* pMap, int mapLayer, bool bPassableNodata,
																		double initSpeed,
																		double Northing, double Easting, double yaw,
																		double *fval, double *dfdN, double *dfdE, double *dfdyaw);
void getContinuousMapValueDiff(CMap* pMap, int mapLayer,
															 double UTMNorthing, double UTMEasting,
															 double *f, double *dfdN, double *dfdE);
double approxMin(double* f, double* dmin_df);

#endif

