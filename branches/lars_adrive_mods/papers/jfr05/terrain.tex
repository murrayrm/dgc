% Master File: jfr06.tex
\section{Sensing and Fusion}
\label{sec:terrain}

Alice uses a sensor suite of an IMU, GPS, range sensors and monocular vision to 
perceive its own state and that of the environment around 
it (see Table \ref{tab-sensors} for the full list).
\begin{table}
  \caption{Sensors used on Alice.} \vspace*{1ex}
  \label{tab-sensors}\centering
  \begin{tabular}{|p{5cm}|c|p{6cm}|}
    \hline
    Sensor Type & Mounting Location & Specifications \\ 
    \hline
    LADAR (SICK LMS 221-30206) & Roof & 
      180$^\circ$ FOV, 1$^\circ$ resolution, 75 Hz, 80 m max range, 
      pointed 20 m away \\ 
    \hline
    LADAR (SICK LMS 291-S14) & Roof & 
      90$^\circ$ FOV, 0.5$^\circ$ resolution, 75 Hz, 80 m max range, 
      pointed 35 m away\\ 
    \hline
    LADAR (Riegl LMS Q120i) & Roof &  
      80$^\circ$ FOV, 0.4$^\circ$ resolution, 50 Hz, 120 m max range, 
      pointed 50 m away \\ 
    \hline
    LADAR (SICK LMS 291-S05) & Bumper & 
      180$^\circ$ FOV, 1$^\circ$ resolution, 80 m max range, 
      pointed 3m away \\ 
    \hline
    LADAR (SICK LMS 221-30206) & Bumper & 
      180$^\circ$ FOV, 1$^\circ$ resolution, 80 m max range, 
      pointed horizontally \\ 
    \hline
    Stereovision Pair (Point Grey Dragonfly) & Roof & 1 m baseline, 
      640x480 resolution, 2.8 mm focal length, 128 disparities \\ 
    \hline
    Stereovision Pair (Point Grey Dragonfly) & Roof & 1.5 m baseline, 
      640x480 resolution, 8 mm focal length, 128 disparities \\ 
    \hline
    Road-Finding Camera (Point Grey Dragonfly) & Roof & 640x480 resolution, 
      2.8mm focal length \\ 
    \hline
    IMU (Northrop Grumman LN-200) & Roof & 1--10$^\circ$ gyro bias,
      0.3--3 mg acceleration bias, 400 Hz update rate \\
    \hline
    GPS (Navcom SF-2050) & Roof & 0.5 m CEP, 2 Hz update rate \\
    \hline
    GPS (NovAtel DL-4plus) & Roof & 0.4 m CEP, 10 Hz update rate \\
    \hline
  \end{tabular}
\end{table}
Obstacle detection is performed using a combination of several SICK
and Riegl LADAR units, as well as two pairs of stereovision cameras. 
Figure~\ref{fig:coverage} shows Alice's sensor coverage.
\begin{figure}
  \centerline{
    \includegraphics[width=0.75\textwidth]{sensor_coverage.eps}
  }
  \caption{Alice's sensor coverage.  Black dotted lines indicate the
  intersection of LADAR scan planes with the ground, the shorter,
  wider cone indicates ground covered by the short-range stereovision
  pair, and the longer, narrower cone indicates coverage by the
  long-range stereovision pair.  The box to the left is Alice, for
  scale.}  \label{fig:coverage}
\end{figure}
This combination of disparate sensors was chosen to allow Alice to be
robust to different environments as well as to multiple sensor
failures---a critical requirement in our specification for autonomous desert 
driving.

To accurately navigate through its environment, it is necessary to
provide Alice with a method of fusing the sensor data from its range
sensors into a single representation of its environment that can
capture information concerning both where it would be safest and
fastest for it to drive.  The decision of what sort of representation
to use was driven primarily by the requirements of the path-planning
software: it expected as its input a speed limit map, where each cell
in the map contained a maximum speed intended to limit the speed at
which the vehicle could drive through that cell.  The problem of
sensor fusion is thus naturally broken down into two sub-problems:
how the incoming sensor data should be fused, and how the speed limit data
should be generated from the raw sensor data.
\begin{figure}
  \centerline{
    \includegraphics[width=1.0\textwidth]{sensorfusion_costavg_grayscale.eps}
  }
  \caption{The sensor fusion framework.}
  \label{fig:sensorfusion}
\end{figure}
The resulting algorithm involves three
basic steps (Figure~\ref{fig:sensorfusion}):
\begin{enumerate}
\item For every range sensor, incoming data is transformed into
  global coordinates (UTM for $x$ and $y$, and altitude in meters for $z$)
  using the 
  vehicle's state estimate, and then averaged into the map along with
  any existing data from that sensor.  This creates several individual
  elevation maps (one per sensor).

\item For every elevation map, a conversion is performed to transform
  the elevation data into speed limit data, based on a set of
  heuristic measures of goodness.

\item Matching cells from all of the speed limit maps are averaged 
  together to produce a single speed limit map, which is passed to
  the path-planning software.
\end{enumerate}
A more detailed description of each stage of the process, as well as
an analysis of some of the strengths and weaknesses of our approach,
is given below.

\subsection{Map Data Structure}

The map data structure we use is a simple 2.5D grid with fixed cell
length and width (40 cm per side for short-range sensors, 80cm for
long-range sensors), as well as fixed overall map length and width
(200 m per side for all sensors).  The map is referenced using global
coordinates (UTM coordinates for $x$ and $y$, altitude for $z$), and is
scrolled along with the vehicle's movement such that the vehicle
always remained at the center of the map.  The data structure of the
map itself was made flexible, so that cells could contain whatever
information was deemed necessary.

The first step of the sensor fusion algorithm is to use the incoming
range data, as well as the vehicle's state estimate, to create an
individual digital elevation map (DEM) for each sensor.  After
performing the appropriate 
coordinate transform on the range data, a simple averaging approach
is used to integrate new measurements with old ones, using the
equation:
\begin{equation}
  \aligned 
    z_{ij} &\gets (z_{ij}+z_m)/(n_{ij}+1) \\
    n_{ij} &\gets n_{ij}+1,
  \endaligned
\end{equation}
where $z_{ij}$ corresponds to the estimate of the height of a given
cell in the grid, $n_{ij}$ is a count of the number of measurements
that fall into the cell $i,j$, and $z_m$ is a new height
measurement for that cell.  Due to time constraints, we were not able
to take into account error models of either the range sensors or the
state estimates when creating our elevation maps.  This is one of the
reasons we chose to perform sensor fusion in the speed limit domain:
it dramatically reduced the sensitivity of the system to
cross-calibration errors in the range sensors.  Measurements are
fused by the system as they come in, and after each scan (or frame)
the resulting changes are sent on to the next stage of the algorithm.

\subsubsection{Elevation to Speed Limit Conversion}

In the next stage of the algorithm, the system needs to convert
elevation data into speed limit data. We use two independent measures
for this task.  The first measure is the variance in elevation of the
cell: the larger the variance, the larger the spread in elevation
measurements that fall into that cell, the more likely that cell
contains some sort of vertical obstacle, and thus the more dangerous
the cell is to drive through.  If the variance is greater than a
certain threshold, the cell is identified as an obstacle, and the
speed limit $s_1$ of that cell is set to zero.\footnote{In fact, a
small non-zero value is used to avoid numerical difficulties in the
path planner.}  
Otherwise, the speed
limit is set to some maximum value.  Thus the variance acts as a
very rough binary obstacle detector, able to detect obstacles that
are completely contained within a single cell (e.g., a fence post,
which is thinner than a cell width or length, but certainly tall
enough to pose a danger to the vehicle).

The second measure we use for determining the speed limit of a cell
is a discrete low-pass filter over a window surrounding that cell.
The precise equation for the low-pass filter for a cell at row $r$ and
column $c$  is
\begin{equation}
  l(r,c) = \frac{1}{n} 
    \sum_{\substack{i=-K \\ i \neq 0}}^K \,
    \sum_{\substack{j=-K \\ j \neq 0}}^K
    \left|(z_{r+i,c+j}-z_{r,c})*G_{i,j}(\sigma)\right|
\end{equation} 
where $K$ is the half-width of the window (e.g., the window has $2K+1$
cells per side), $z_{r,c}$ is the elevation of the cell at row $r$
and column $c$, $n$ is the number of cells used within the window (at
most $(2K+1)^2$), and $G_{i,j}(\sigma)$ corresponds to the value at cell
$i,j$ of a discrete Gaussian centered at the origin with standard
deviation $\sigma$.  In essence then, the filter acts as a Gaussian
weighted average of the difference in height between the central cell
and its neighbors within the window described by $K$.  Of course, not
all of the neighboring cells necessarily have an elevation estimate.
For example, a large obstacle may cast a range shadow, blocking the
sensor from making measurements behind the obstacle.  When such a cell
is encountered in the sum, it is discarded and $n$ is
reduced by one.  Thus we have $n=(2K+1)^2-1-m$, where $m$ is the
number of cells without elevation data inside the window.
Additionally, if $n$ is smaller than some threshold, then the output
of the filter is not computed, based on the principle that the system
did not have enough information about the neighborhood surrounding the
center cell to make a reasonable estimate of the area's roughness.

Assuming the output is computed, however, we still need to
transform it into a speed limit.  To accomplish this transformation,
we pass the output through a sigmoid of the form
\begin{equation}
  s_{2}=h*\tanh(w*(l(r,c)-x))+y,
\end{equation} 
where $l(r,c)$ is the response of the low-pass filter given above,
and the parameters $h$, $w$, $x$, and $y$ are tuned heuristically by
comparing sample filter responses to the real-life situations to which
they corresponded.  Then, to compute the final speed $s_f$, we simply
take the minimum of $s_1$, the speed generated using the variance, and
$s_2$, the speed generated using this roughness filter.

Of course, the computations described here are not computationally
trivial; to do them every time a single new elevation data point is
generated would be infeasible given that some of the sensors are
capable of generating hundreds of thousands of measurements per
second.  Additionally, over a small time interval (e.g., less than one
second) measurements from a sensor will frequently fall into a small
portion of the entire map.  To take advantage of this redundancy, and
to increase the computational speed of the algorithm, speed limits
are generated at a fixed frequency instead of on a per-measurement
basis.  At some predefined interval the algorithm sweeps through
the map, regenerating the speed limit of any cell whose elevation
value has changed since the last sweep.  Additionally, any cells
whose neighbors have changed elevation values are also re-evaluated,
as the response of the roughness filter could be different.
To speed this process up, the algorithm uses a dynamic list to keep
track of which specific cells have changed.  Any time a cell receives a
new measurement, the algorithm adds that cell to the list of cells to
update during the next sweep (if the cell has not been added already).
The result is an algorithm that takes advantage of the redundancy of
subsequent measurements from a single sensor to transform a digital
elevation map into an accurate speed limit map quickly and
efficiently.

\subsection{Speed Limit-Based Fusion}

After computing a separate speed limit map for each sensor, all that
remains is to fuse these different maps together into a single final
speed limit map.  We chose to use a simple weighted average, using
heuristics to determine the weights for each sensor.  Thus the
equation for determining the final fused speed limit $s(r,c)$ of a
cell at row $r$ and column $c$ is
\begin{equation}
  s(r,c)=\frac{\sum\limits_{i}s_{i}(r,c)*w_i}{\sum\limits_{i}w_i},
\end{equation}
where $s_i(r,c)$ is the speed limit of the corresponding cell in the
speed limit map of sensor $i$, and $w_i$ is the weight associated
with the sensor $i$.  However, this algorithm has a serious
flaw: obstacles occasionally disappear and then reappear as they
entered the range of each sensor (see Figure~\ref{fig:disappearing}).
\begin{figure}
  \centerline{\includegraphics[width=0.5\textwidth]{disappearing.eps}}
  \caption{The disappearing obstacle problem.}
  \label{fig:disappearing}
\end{figure}
This phenomenon is due to the weights used
for fusing the sensors: sensors that are pointed closer to the
vehicle have higher weights than sensors that are pointed further
away, on the principle that the closer a sensor is pointed, the less
susceptible its output is to synchronization errors between
range measurements and the vehicle's state estimate.  As a result, a
chain of events can occur in which:
\begin{enumerate}
\item Sensor A, a long-range sensor, picks up measurements from an
  obstacle and assigns the cells that contain it a low speed.
\item The very bottom of the obstacle comes into the view of sensor B,
  a short-range sensor.
\item The cell containing the leading edge of the obstacle is set to a
  higher speed than was there before because: sensor B sees only the
  part of the obstacle near to the ground; seeing only the part near
  the ground, sensor B perceives only a very small obstacle (if it
  detects one at all); and finally, sensor B has a higher weight than
  sensor A.
\item Sensor B sees the entirety of the obstacle, and the cell
  containing the obstacle is once again set to a low speed.  At this
  point, it is usually too late for the vehicle to react, and a
  collision occurs.
\end{enumerate}

To solve this problem, we introduce an additional term into the
weighted average that served to favor cells that have more measurements
(and thus were more complete).  The equation becomes
\begin{equation}
  s(r,c) = \frac{\sum_{i}s_{i}(r,c)*w_i*n_i(r,c)}{\sum_{i}w_i*n_i(r,c)},
\end{equation}
where $n_i(r,c)$ is the number of measurements by sensor $i$ that
were contained in the cell at $(r, c)$.  As with the cost-generation
algorithm, cost-fusion is done at a fixed frequency instead of on a
per-cell basis, and only cells whose speed limits have changed since
the last sweep are updated.

\begin{figure}
  \centerline{\includegraphics[width=1.0\textwidth]{costmaps.eps}}
  \caption{Example input speed limit maps and a final output map. In
  each case, the small rectangle to the left is the vehicle (which is
  traveling from right to left) and the line extending behind it is
  its previous path.  (For scale, the grid lines are 40 m apart.)  From
  bottom-left, moving clockwise: speed limit map generated by a
  midrange LADAR; speed limit map generated by a short-range LADAR;
  speed limit map generated by a second midrange LADAR; the combined
  speed limit map. The combined speed limit map has also taken into
  account information about the race corridor.}  \label{fig:costmaps}
\end{figure}

\subsection{Additional Information: Roads, RDDF Corridors, and No Data}

Although the sensor fusion problem makes up the bulk of the work,
additional information is available for integration into the final
speed limit map.  

\newcommand{\accumBuffer}{\mathbf{I}_{\mathit{VP}}}    
\newcommand{\Ray}{\mathbf{r}}  
\newcommand{\DO}{\theta}    % dominant orientation angle 
\newcommand{\voter}{\mathbf{p}}  

\paragraph{Road Following}
Alice's road following algorithm~\citep{Ras06-icra} uses a single
calibrated grayscale camera and a bumper-mounted SICK LADAR.  The
best-fit vanishing point of the road ahead is repeatedly extracted
from the pattern of parallel ruts, tracks, lane lines, or road edges
present in the camera image, yielding a target direction in the ground
plane.  This visual information is augmented with estimates of the
road width and Alice's lateral displacement from its centerline,
derived from the LADAR returns, to populate a ``road layer'' in the
combined cost map used by the planner.  Additionally, a failure
detection process classifies images as road or non-road to
appropriately turn off the road layer and monitors for sun glare or
excessive shadow conditions that can confuse the vanishing point
finder.  Figure~\ref{fig:onboard} illustrates the operation of the algorithm.
\begin{figure}
  \begin{center} 
    \includegraphics[height=1.25in]{camera_095.eps}
    \includegraphics[height=1.25in]{DO_095.eps}
    \includegraphics[height=1.25in]{VP_095.eps}
    \includegraphics[height=1.25in]{pipeline_095.eps} \\
  \end{center}
  \hspace{0.9in}(a)\hspace{1.55in}(b)\hspace{1.55in}(c)\hspace{1.15in} (d) 
  \caption{Sample road follower output on test run: (a) computed road
    direction and Alice direction (image projections of diagonal,
    vertical lines, respectively, in (d)); (b) dominant orientations
    ($[0, \pi]$ angle proportional 
    to intensity); (c) vanishing point votes $\accumBuffer$; (d)
    overhead view of estimated road region (green/lightly shaded area): Alice is
    the rectangle, bumper LADAR returns are the scattered points,
    LADAR-derived obstacle function $h(x)$ is shown in at
    bottom.}
  \label{fig:onboard}
\end{figure}

To obtain the road vanishing point, the dominant texture orientation
at each pixel of a downsampled $80 \times 60$ image is estimated by
convolution with a bank of $12 \times 12$ complex Gabor wavelet
filters over 36 orientations in the range $[0, \pi]$.  The filter
orientation $\DO(\voter)$ at each pixel $\voter$ which elicits the
maximum response implies that the vanishing point lies along the ray
defined by $\Ray_{\voter}=(\voter,\DO(\voter))$.  The instantaneous
road vanishing point for all pixels is the maximum vote-getter in a
Hough-like voting procedure in which each $\Ray_{\voter}$ is
rasterized in a roughly image-sized accumulation buffer
$\accumBuffer$.  Using $\accumBuffer$ as a likelihood function, we
track the road vanishing point over time with a particle filter.

The road region is computed by projecting LADAR hit points along the
vision-derived road direction onto a line defined by Alice's front
axle, deweighting more distant points, to make a 1-D ``obstacle
function'' $h(x)$.  The middle of the gap in $h(x)$ closest to the
vehicle's current position marks Alice's lateral offset from the road
midpoint; it is tracked with a separate particle filter.  If
sufficient cues are available, this road-finding system is able to 
identify a rectangular section of the area in front of Alice as a
road.  Using the assumption that roads are generally safer to drive on
(and thus the vehicle should drive faster on them), this data is
integrated into the existing speed limit map using a heuristic
algorithm as a speed bonus.  As a result, roads appear as faster
sections in the map, enabling the path-planning algorithm to keep
Alice on roads whenever possible.

\paragraph{RDDF Corridor}
The final (and perhaps most important) piece of information that is
incorporated is the race corridor.  Incorporating this information
into the map is trivial: if a given cell falls inside the race
corridor, then the value of that cell is set to be the minimum of the
corridor speed limit and the terrain and road-based speed limit.  If
a cell falls outside the corridor, its speed is set to zero.  In
effect, the race corridor acts simply as a mask on the underlying
terrain data: no inherent assumptions are made in the mapping
software that rely on the corridor information.  In fact, testing
was frequently performed in which the corridor speed limit was
essentially set to infinity, and the corridor's width was set to be
dozens of meters wide.  Even with these loose bounds, the mapping
software was easily able to identify drivable areas and generate
accurate speed limit maps.

\paragraph{No-Data Cells}
One final topic that we have alluded to, but not covered in detail, is
what we call the no-data problem: in many cases, not
all of the cells in a given area of the map will contain elevation
measurements.  For example, no range measurements will be returned
when a sensor is pointed off the side of a cliff, or from behind an
obstacle that is obstructing the sensor's field of view.  This
problem can be partially alleviated by making some assumptions about
the continuity of the terrain in the environment, and interpolating
between cells accordingly.  Our software performs some
interpolation (by filling in small numbers of empty cells based on the
average elevation of surrounding cells that did have data).  Despite
this, there are still situations where large patches of the map
simply have no data, and interpolating across them could lead to
incorrect estimates of the nature of the surrounding terrain.  The
question, then, is how the mapping software should treat those areas:
should it be cautious and set them to have a low speed limit?  Or
should it make assumptions about the continuity of the surrounding
terrain and set them to have a high speed limit?  We found that for
robust performance, a compromise between these two extremes was
necessary.

Clearly, setting no-data cells to a high speed limit would be inviting
trouble.  For example, the vehicle may decide it prefers to drive off
a cliff because the open area beyond the cliff is a high-speed no-data
region.  However, setting no-data cells to a speed limit of zero can
also result in undesirable behavior.  Consider a set of steep rolling
hills with a dirt road down the center, and bushes or boulders lining
the sides.  
\begin{figure}
  \begin{tabular}{ccc}
  \includegraphics[width=0.45\textwidth]{hill.eps} &&
  \includegraphics[width=0.5\textwidth]{nodata.eps} \\ (a) &\qquad&
  (b) \end{tabular} \caption{Example no-data problem.  (a) As the
  vehicle crests the hill, its sensors will detect the bushes lining
  the road before they detect the road itself.  (b) A corresponding
  sample no-data map (with the vehicle traveling from left to right).
  If area I is no-data, and is a lower speed than area II
  (corresponding to obstacles) and area III (corresponding to flat
  terrain), then the vehicle may prefer to drive in no-data areas (I)
  instead, even if they correspond to dangerous obstacles.}
  \label{fig:nodata}
\end{figure}
As the vehicle crests a hill, the first thing its sensors
will detect are the bushes lining the side of the road
(Figure~\ref{fig:nodata}(a)).  In particular, these bushes may be
detected and 
placed into the map prior to the vehicle detecting the dirt road
below, resulting in a speed limit map that looks something like Figure
\ref{fig:nodata}(b).  In this situation, if the no-data speed is set too low
(that is, lower than the speed assigned to the cells containing the
bushes) the vehicle may prefer to drive through the bushes instead of
stay on the road.  Clearly this is undesirable---as a result, a
compromise must be made where the no-data speed is low (so as to avoid
the vehicle barreling across uncharted terrain at high speed) but not
lower than the speed associated with most obstacles.  The
interpretation of no-data cells is performed by the path planning
module, described in the next section.


