#!/bin/csh
#
# jfrmap - script to generate the files for JFR submission
# RMM, 3 Jun 06
#

# Information about the journal
set pre = ROB-05-0046;

# Make the figure directory and initialize counter
mkdir -p submit
set fig = 1;

# Submit need to be tiff or eps, named accordingly

# Section 1 (introduction)
echo Fig ${fig}: alice-gce
convert alice-gce.jpg alice-gce.png
convert alice-gce.png submit/${pre}fig${fig}.tiff;
rm alice-gce.png
@ fig++; 

# Section 2 (architecture)
echo Fig ${fig}ab: alice-{frontdiff,backwork}
convert alice-frontdiff-hires.jpg alice-frontdiff.png
convert alice-backwork-hires.jpg alice-backwork.png
convert alice-frontdiff.png submit/${pre}fig${fig}a.tiff;
convert alice-backwork.png submit/${pre}fig${fig}b.tiff;
rm alice-{backwork,frontdiff}.png
@ fig++;

echo Fig ${fig}: systemarch
cp systemarch.eps submit/${pre}fig${fig}.eps;
@ fig++;

# Section 3 (vehicle)
echo Fig ${fig}ab: switches, braking
convert switches.jpg switches.png; convert braking.jpg braking.png
convert switches.png submit/${pre}fig${fig}a.tiff;
convert braking.png submit/${pre}fig${fig}b.tiff;
rm {braking,switches}.png
@ fig++;

echo Fig ${fig}ab: statesmooth, astate-jump
cp statesmooth.eps submit/${pre}fig${fig}a.eps
cp astate-jump.eps submit/${pre}fig${fig}b.eps
@ fig++;

echo Fig ${fig}ab: tflongitudinal, tflateral
cp tflongitudinal.eps submit/${pre}fig${fig}a.eps
cp tflateral.eps submit/${pre}fig${fig}b.eps
@ fig++;

# Section 4 (terrain)
echo Fig ${fig}: sensor coverage
convert sensor_coverage.png submit/${pre}fig${fig}.tiff;
@ fig++;

echo Fig ${fig}: sensor fusion framework
convert sensorfusion_costavg_grayscale.png submit/${pre}fig${fig}.tiff;
@ fig++;

echo Fig ${fig}: dissappearing
convert disappearing.png submit/${pre}fig${fig}.tiff;
@ fig++;

echo Fig ${fig}: cost maps
convert costmaps.png submit/${pre}fig${fig}.tiff;
@ fig++;

echo Fig ${fig}abcd: road following
convert camera_095.png submit/${pre}fig${fig}a.tiff
convert DO_095.png submit/${pre}fig${fig}b.tiff
convert VP_095.png submit/${pre}fig${fig}c.tiff
convert pipeline_095.png submit/${pre}fig${fig}d.tiff
@ fig++;

echo Fig ${fig}ab: no data
convert hill.png submit/${pre}fig${fig}a.tiff;
convert nodata.jpg nodata.png
convert nodata.png submit/${pre}fig${fig}b.tiff;
rm nodata.png
@ fig++;

# Section 5 (planning)
echo Fig ${fig}: receding horizon
fig2dev -Leps recedinghorizon.fig submit/${pre}fig${fig}.eps;
@ fig++;

echo Fig ${fig}abcd: planner iterations
fig2dev -Leps plots86.fig submit/${pre}fig${fig}a.eps;
fig2dev -Leps plots90.fig submit/${pre}fig${fig}b.eps;
fig2dev -Leps plots92.fig submit/${pre}fig${fig}c.eps;
fig2dev -Leps plots96.fig submit/${pre}fig${fig}d.eps;
@ fig++;

# Section 6 (supercon)
echo Fig ${fig}: NFP scenarios
cp five-scenario-types.eps submit/${pre}fig${fig}.eps;
@ fig++;

echo Fig ${fig}: SC state machine
cp sc-state-machine.eps submit/${pre}fig${fig}.eps;
@ fig++;

echo Fig ${fig}: Dead end getout
cp dead-end-getout.eps submit/${pre}fig${fig}.eps;
@ fig++;

echo Fig ${fig}: SC NQE run1
cp sc-nqe-run1.eps submit/${pre}fig${fig}.eps;
@ fig++;

# Section 7 (results)
echo Fig ${fig}: RDDFs
convert desert_rddfs.png submit/${pre}fig${fig}.tiff
@ fig++;

echo Fig ${fig}: Daggett
convert dagget_ridge_costmap.png submit/${pre}fig${fig}.tiff
@ fig++;

echo Fig ${fig}: Alice at NQE
convert alice-nqe.jpg alice-nqe.png
convert alice-nqe.png submit/${pre}fig${fig}.tiff
rm alice-nqe.png
@ fig++;

echo Fig ${fig}: NQE cost map
convert nqe_costmap.png submit/${pre}fig${fig}.tiff
@ fig++;

echo Fig ${fig}ab: yawcrash, supercon
cp yawcrash.eps submit/${pre}fig${fig}a.eps
cp sc-gce-crash.eps submit/${pre}fig${fig}b.eps
@ fig++;

echo Fig ${fig}: crash gui
convert alicecrashgui.png submit/${pre}fig${fig}.tiff
@ fig++;

echo Fig ${fig}: alice crash
convert alice-crash.eps submit/${pre}fig${fig}.tiff
@ fig++;
