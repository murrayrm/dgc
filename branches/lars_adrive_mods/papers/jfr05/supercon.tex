% Master File: jfr05.tex
\section{Contingency Management}
\label{sec:supercon}

Experience has shown that a major challenge in the successful
deployment of autonomous systems is the development of mechanisms to
allow these systems to continue to operate in unanticipated
(non-nominal) conditions.  An important subset of these non-nominal
conditions is system-level faults.  These are situations in which all
the individual components of the system are operating correctly, that
is to say as detailed in their specifications, yet the system as a
whole is unable to achieve its stated goals. 

In small-scale, comparatively simple systems, a state machine is often
used as the system level controller.  The principal advantages of this
approach are the ability of the designer to specify through a
state-table exactly how the system can evolve so that it responds
effectively to the specific faults they envisage.  As the state-table
defines exactly how the system can evolve, only the stability of
the system in the scenarios described by the state table need be
considered, reducing the scale of the robustness problem and typically
leading to a comparatively short development time.  However the
state machine approach does not scale well with system complexity or
size, and as it cannot execute multiple states simultaneously is not
suited to situations in which multiple connected faults may have
occurred.  

A recent approach combining state-feedback with multiple rule-based
filtering stages is demonstrated in JPL's Mission Data System
(MDS)~\citep{Ras01-ieeeac}.
MDS seeks to provide a very flexible and expandable architecture for
large and complex systems, such as unmanned space missions, that
is capable of managing multiple faults simultaneously.
However, the disadvantages of this 
approach are the absence of a known method to verify system stability 
and robustness, hence significant time is required to develop or enhance 
the system while maintaining previous empirically proven system robustness.

\subsection{Problem Specification}

The primary objective of the supervisory controller, SuperCon, is
to ensure that where physically possible Alice continues to make
forward progress along the course as defined by the RDDF.  In
situations where forward progress is not immediately possible
(e.g., intraversible obstacle in front of Alice's current position), SuperCon
should take all necessary action to make it possible.  This objective
encompasses the detection, and unified response to, all system-level
faults from which it is possible for the system to recover given its
designed functionality, performance, constraints and assumptions.  SuperCon
is not required to consider specific responses to hardware failure
since these are handled at a lower level.  It
should also be noted that Alice does not carry any backups for its
actuators.  It is however a requirement of the mapping software that
it be resistant to up to two sensor failures.  In addition, Alice's
computer cluster is configured such that in the event that any machine
powers down it will restart (if physically possible), and if any
module crashes it will be immediately restarted.

The secondary objective of SuperCon is to modify the overall system
configuration/status if required by a module under certain conditions
and manage the effects of these changes upon the different
components of the system.  SuperCon should also verify its own actions, and
make decisions based on the accuracy of the information available to
it.  SuperCon must also be stable, robust, easy to expand to respond to new
issues as they are discovered 
through testing, fast to test and verify and quick to initially
assemble.

In essence the presence of SuperCon should only increase the performance,
functionality, stability and robustness of the complete system and so
increase Alice's chances of winning, it should never lead to a
decrease in system performance by impeding correct operation.

\subsection{Hybrid Architecture}

The hybrid architecture developed has a state table
and a series of 
rule-based filtering layers that are re-evaluated each cycle and
stored in a diagnostic table.  The resulting framework shares the 
general characteristics with MDS, described above.  The state machine
is composed of ten strategies, each of which consists of a sequence of
stages to configure the system to 
achieve a specified goal (e.g., reversing requires changing gear in
addition to the reconfiguration of several affected software modules).  Each stage is composed of a series of conditional tests
that must be passed in order for the (single) SuperCon action (e.g., change gear) to be performed, and
hence the stage to be marked as complete.  If a stage is not
completed, and the current strategy is not updated, the next time the
state machine steps forward it will re-enter the (same) stage.  This
allows the state machine to loop until conditions are met while
always using the current version of the diagnostic table.  If the test
was a transition condition (which determines whether SuperCon should move to
another strategy) and evaluates to true, the corresponding strategy change
is made, for execution in the next cycle of the state machine.

Half of the strategies are stand-alone strategies that are
responsible for managing Alice's response to special case scenarios
(e.g., the safe reincorporation of GPS after a sustained signal
outage).  The other half form an interconnected ring that can be used
to identify and then resolve any identified scenarios in which Alice
would be unable to make forward progress along the course.  The hybrid
architecture effectively expands the capabilities of the state machine
approach through rule-based filtering, and a strategy-driven approach
making it easier to design for large, complex systems while retaining
its desirable robustness and ease of expansion properties.  

The SuperCon hybrid structure
consists of two permanent threads, the state update thread maintains
the live version of the state table that holds the current and
selected history data for all SuperCon state elements and is purely
event-driven (by the arrival of new messages).  The deliberation
thread by contrast is prevented from executing above a defined
frequency (10Hz) to prevent unnecessary processor loading.


% Alex - I thought this paragraph was not necessary.  Feel free to put it
% back if it adds something that I missed. 
%
% I think this is necessary as it is important to state that strategies
% are not completed each SuperCon cycle, rather that stage are, as without
% this safe-looping can't occur, and the danger of system lock-up
% increases enormously.  Also as the timing diagram and architecture
% structure diagram can't fit in, this is the only para that really
% covers the flow of the SuperCon cycle  - hence putting this back in

The SuperCon state machine only attempts to complete one stage each time it
``steps forwards''.  Once it has finished processing a stage (regardless
of whether it was successfully completed) the state machine suspends
execution and requests a new copy of the state table.  Once the new copy
of the state table has been received, and the diagnostic rules table has
been re-evaluated, the state machine takes another step forward. 

A significant advantage of the hybrid approach detailed above is that
as it is modular, it is comparatively easy to test as the components
can be verified independently in sequence.

In order to make SuperCon robust, every time the SuperCon state
machine takes an action (e.g., sends a request to change gear), before
any additional action is taken SuperCon verifies that any action
associated with the previous stage has been successfully completed.
Each of these verification stages has an associated time-out.  If the
time-out is exceeded then SuperCon assumes a fault has occurred, and
reverts to the Nominal strategy, from which SuperCon is reset and can
re-evaluate which strategy should be active and then transition to it.

\subsection{Strategies}

The role of the strategies is to create a logical structure that can
fulfill the primary and secondary objectives for SuperCon.  
Corresponding to the primary objective, to ensure that Alice always
continues to make forward progress along the RDDF where possible, a
list of possible scenarios (from experience and analysis) that could
prevent such progress was produced.  By definition, this list was coined as No 
Forward Progress (NFP) scenarios.  Analysis of the list produced 
five distinct families of NFP scenarios, prototypical examples for each of
which are shown in Figure~\ref{fig:five-scenario-types}. 

\begin{figure}
  \centerline{\includegraphics[width=16.5cm]{five-scenario-types.eps}}
  \caption{Diagram showing prototypical examples of the five general
  NFP (No Forward Progress) scenarios identified.} 
  \label{fig:five-scenario-types}
\end{figure}

%In addition to these five general families, there was a group of
%special case scenarios that represented specific faults/conditions
%that have particular solutions.  These special case scenarios included
%responses to service particular modules (e.g., preparing the system so
%that it is safe for Astate to incorporate a new GPS signal into the
%state estimate), corresponding to the secondary objective of SuperCon.
%As these special cases have unique solutions, and hence cannot be
%described as belonging to a larger group, they were each given their
%own strategy.

Due to the limited perceptive and cognitive abilities of our system
(as compared to a human) 
it is often not initially possible to distinguish between
the five groups shown in Figure~\ref{fig:five-scenario-types}.  Hence 
in order for SuperCon to be robust
an integrated array of strategies that can identify and resolve any
arbitrary scenario from any of the groups is required.

After extensive analysis of the responses that a human would take to
attempt to resolve the prototypical examples (assumed to be at least
on average the best response) and the information they used to do so,
the NFP Response Cycle shown in Figure~\ref{fig:sc-state-machine} was
produced.  The cycle consists of five strategies including the nominal
strategy, which the system should remain in or revert to if no
fault conditions are detected.  The following strategies are used in 
the NFP Response Cycle:

\begin{figure}
  \centerline{\includegraphics[width=14.5cm]{sc-state-machine.eps}}
  \caption{No-Forward-Progress (NFP) Response Cycle, showing the
  constituent strategies and purposes of each of the interconnections
  between them to produce the cycle response effect.  Note that the
  general conditions under which a transition between strategies
  occurs (colored arrows) is described by the text of the same color
  as the arrow. Specific details of individual transitions are given in
  black text associated with the relevant arrow.} 
  \label{fig:sc-state-machine}
\end{figure}

\paragraph{Nominal} No system faults/requests
detected and the current plan does not pass through any obstacles.  Verify
that the system is correctly configured for nominal operation (gear = drive
etc) and correct any exceptions found.

\paragraph{Slow Advance} The planner's current plan now passes through an
obstacle (terrain or SuperCon, including outside the RDDF corridor) when it did
not pass through any obstacles previously (nominal strategy).  Hence limit
the max speed to ensure the sensors can accurately scan the terrain ahead,
but allow Alice to continue along the current plan, whose speed profile will
bring Alice to a stop in front of the obstacle if it is not cleared from the
map by new sensor data.

\paragraph{Lone Ranger} The planner's current plan passes through a
terrain obstacle, which has not been cleared by subsequent sensor data, even
after Alice has driven up to it.  As Alice would always avoid any terrain
obstacles if there was a way to do so (that did not pass through SuperCon
obstacles), verify that the terrain obstacle really exists by attempting to
push through it.

\paragraph{Unseen Obstacle} Alice is stationary when it does not intend
to be (TrajFollower's reference speed less than minimum maintainable speed), its engine
is on and it is actively attempting to move forwards (positive accelerator command and zero brake command) yet an external (assumed) object in front of
it is preventing it from doing so.  Hence mark the terrain directly in
front of its current position as a SuperCon obstacle, as it has been investigated
and found to be intraversible.

\paragraph{L-turn Reverse} The planner's current plan passes through a SuperCon
obstacle, these are avoided in preference of all other terrain where
possible and indicate areas that have been verified as intraversible (or are
outside the RDDF).  Hence reverse along the previous path (trail of state
data points) until the current plan passes through no obstacles, up to a
maximum of 15 m (the distance required for Alice to perform a 90-degree
left/right turn plus an error margin) per call of the L-turn reverse
strategy.  If the current plan passes through terrain obstacles after
reversing for 15 m then go forwards and investigate them to verify their
existence.  If it still passes through SuperCon obstacles then recall
L-turn Reverse and hence initiate another reversing action. 

\medskip\noindent
A significant benefit of the NFP response cycle design is that it only
requires a very small amount of additional information to be computed on top
of what is already available in the system (prior to SuperCon).  The main
piece of information used is the value of the speed limit in the lowest
speed cell (evaluated over Alice's footprint) through which the current plan
evaluated through the most recent version of the cost map speed-layer
passes, referred to as the minimum speed cell (MSC).  The current plan is
defined as the plan currently being followed by the TrajFollower.  The
planner that Alice uses (as detailed in Section~\ref{sec:planning}) is a
non-linear optimizer. As a result, it is not necessary for SuperCon to know
the location at which the minimum speed cell occurs, as the plan is optimal
for the cost function it represents and the best option available in terms
of its obstacle avoidance.  There are three ranges (of values) in the speed
layer of the cost map: no-obstacle, terrain obstacle and SuperCon obstacle
\and outside the RDDF.  The range that the current minimum speed cell value
belongs to is used by SuperCon when determining what action (if any) to
take.  There is a fundamental difference between what is known about terrain
and SuperCon obstacles.  Terrain obstacles are identified by the sensor
suite as having an elevation, and/or gradient outside of Alice's safe
limits.  SuperCon obstacles represent areas where Alice attempted to drive
through (irrespective of whether they were identified as terrain obstacles)
and was unable to do so.  As such SuperCon obstacles are verified terrain
obstacles.  Note that all ``obstacles'' are theoretically intraversible by
definition.

% TODO: This paragraph could probably be cut.
The process is analogous to a typical feedback controller, where the planner
is the process whose output is the minimum speed cell value, SuperCon is the
controller and the reference signal is the range of obstacle-free terrain
values.  The error signal then becomes non-zero when the current plan passes
through an obstacle, which prompts SuperCon to take corrective action
through the use of the NFP response cycle.

Figure~\ref{fig:dead-end-getout} shows an example of the NFP response
cycle enabling Alice to successfully navigate its way out of a dead
end 
scenario,  
listing the actions taken by SuperCon at each stage, and indicating when each strategy is active.
\begin{figure}
  \centerline{\includegraphics[width=16.5cm]{dead-end-getout.eps}}
  \caption{Example of Alice resolving a dead-end scenario fault,
    showing the events and strategy transitions at each point.}
  \label{fig:dead-end-getout}
\end{figure}
Stages 2--4 in the response are repeated until SuperCon obstacles have been
placed as shown in stage 5 (assuming the terrain dead-end obstacle
really exists).  Note that the RDDF border on the right hand-side of
Alice does not need to be verified, as outside the RDDF the terrain is
equivalent to an SuperCon obstacle by default.  A miniaturized version of
the NFP response cycle is also shown for each stage, with the current
strategy highlighted in red (darker shading), and the next in green (light
shading).  In case 5, blue (upper center state) is used to 
denote the most probable current strategy at the start of the stage).

Figure \ref{fig:supercon_nqe} depicts the evolution of SuperCon strategies that
enabled the forward progress made during Alice's first NQE run.  
\begin{figure}
  \centerline{
    \includegraphics[width=0.75\hsize]{sc-nqe-run1.eps}
  }
  \caption{Strategies used during NQE Run \#1.  The GPS re-acquisition
    intervals are execution of the design requirement to safely stop
    the vehicle in the face of large corrections of state estimate errors.}
  \label{fig:supercon_nqe}
\end{figure}

