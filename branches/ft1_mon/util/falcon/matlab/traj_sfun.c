/*
 * traj_sfun.c - S-Function for reading trajectory files
 *
 * RMM 4/30/96
 *
 * Inputs:	none
 * Outputs:	from trajectory file
 * Inputargs:   filename (string)
 *
 * This S-funtion allows you to read trajectory files from within
 * Simulink files.  It is modeled after lut_sfun (by Mike Kantner).
 */


#define S_FUNCTION_NAME traj_sfun

#include "simstruc.h"
#include "mex.h"
#include "falcon/traj.h"		/* trajectory data types */
#include <math.h>

/* #define TRJ_VERBOSE */

/* Function to load a trajectory file into memory */
static TRAJ_DATA *loadtable(SimStruct *S)
{
    Matrix *fn;
    char trajfile[1024];
    TRAJ_DATA *trjp;

    /* Grab the input argument and see if it is a string */
    fn = ssGetArg(S, 0);
    if (mxIsString(fn) == 0 || mxGetString(fn, trajfile, 1023)) {
	mexPrintf("traj_sfun: filename parameter is not a valid string\n");
	return NULL;
    }

    /* Initialize the trajectory file information */
    if (trjp == NULL) { traj_free(trjp); trjp = NULL; }
    if (trjp = traj_load(trajfile)) == NULL) {
	mexPrintf("traj_sfun: can't open %s\n", trajfile);
	return NULL;
    }
#   ifdef TRJ_VERBOSE
    fprintf(stderr, "traj_sfun: loaded %s\n", filename);
#   endif
    
    traj_reset(trjp);
#   ifdef TRJ_VERBOSE
    fprintf(stderr, "traj_sfun: reset trjp\n");
#   endif

    return trjp;
}

/* mdlInitializeSizes - initialize the sizes array */
static void mdlInitializeSizes(S)
SimStruct *S;
{
    TRAJ_DATA *trjp;
    int nout;

    /* Load the trajectory so we can figure out the number of outputs */
    if ((trjp = loadtable(S)) == NULL) return;
    nout = trjp->ncols;
    traj_free(trjp);

    ssSetNumContStates(    S, 0);	/* number of continuous states */
    ssSetNumDiscStates(    S, 0);	/* number of discrete states */
    ssSetNumInputs(        S, 0);	/* number of inputs */
    ssSetNumOutputs(       S, nout);	/* number of outputs */
    ssSetDirectFeedThrough(S, 1);	/* direct feedthrough flag */
    ssSetNumSampleTimes(   S, 1);	/* number of sample times */
    ssSetNumInputArgs(     S, 1);	/* number of input arguments (parms) */
    ssSetNumRWork(         S, 0);	/* number real work vector elements */
    ssSetNumIWork(         S, 0);	/* number int work vector elements */
    ssSetNumPWork(         S, 1);	/* number ptr work vector elements */
}

/* mdlInitializeSampleTimes - initialize the sample times array */
static void mdlInitializeSampleTimes(S)
SimStruct *S;
{
    /* Set things up to run as continuous time block */
    ssSetSampleTimeEvent(S, 0, 0.0);
    ssSetOffsetTimeEvent(S, 0, 0.0);
}

/* mdlInitializeConditions - initialize the states */
static void mdlInitializeConditions(x0, S)
double *x0;
SimStruct *S;
{
    /* Save the pointer to the trajectory table as working pointer */
    ssSetPWorkValue(S, 0, loadtable(S));
}

/* mdlOutputs - compute the outputs */
static void mdlOutputs(y, x, u, S, tid)
double *y, *x, *u;
SimStruct *S;
int tid;
{
    double time = ssGetT(S);
    TRAJ_DATA *trjp = (TRAJ_DATA *) ssGetPWorkValue(S, 0);

    if (trjp == NULL) {
	mexPrintf("traj_sfun: mdlOutputs called with trjp == NULL\n");
	return;
    }

    /* Look up the outputs from the table at the current time */
#   ifdef TRJ_VERBOSE
    fprintf(stderr, "traj_sfun: calling traj_read\n");
#   endif
    traj_read(trjp, y, time);
#   ifdef TRJ_VERBOSE
    fprintf(stderr, "traj_sfun: done reading data for time %g\n", time);
#   endif
}

/* mdlUpdate - perform action at major integration time step (not used) */
static void mdlUpdate(x, u, S, tid)
double *x, *u;
SimStruct *S;
int tid;
{
    /* Not used for this model */
}

/* mdlDerivatives - compute the derivatives */
static void mdlDerivatives(dx, x, u, S, tid)
double *dx, *x, *u;
SimStruct *S;
int tid;
{
}

/* mdlTerminate - called when the simulation is terminated */
static void mdlTerminate(S)
    SimStruct *S;
{
    TRAJ_DATA *trjp = (TRAJ_DATA *) ssGetPWorkValue(S, 0);

    /* Free up the trajectory file */
    if (trjp != NULL) traj_free(trjp);
    trjp = NULL;
}

/* Trailer information to set everything up for simulink usage */
#ifdef MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#endif
