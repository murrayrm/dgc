#ifndef LINESEGMENT_HH
#define LINESEGMENT_HH

#include "frames/coords.hh"
#include <math.h>
#include <iostream>

using namespace std;

class CLineSegment {
public:
  enum {
    INTERSECTION_NO=0,
    INTERSECTION_YES=1,
    INTERSECTION_COLLINEAR=2
  };

  NEcoord startPoint, endPoint;
  double slope;
  double y_intercept;
  int vertical;
  double x_intercept;

  CLineSegment(NEcoord start, NEcoord end);
  ~CLineSegment();

  int checkIntersection(const CLineSegment &other,
			 NEcoord &interectionPoint);

  bool checkBetweenN(double value) const;
  bool checkBetweenE(double value) const;
};

#endif //LINESEGMENT_HH
