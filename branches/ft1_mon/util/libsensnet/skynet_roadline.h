/* 
 * Desc: Skynet road lines blob.
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef SENSNET_ROADLINES_H
#define SENSNET_ROADLINES_H

#include <stdint.h>
#include <string.h>

#include "sensnet_types.h"


/// @brief Skynet message with road line data.
///
typedef struct
{
  /// Skynet message type (must be SNroadLine)
  int msg_type;

  /// Image frame id (from the sensor).
  int frameid;

  /// Image timestamp (from the sensor).
  double timestamp;

  /// Vehicle state data
  sensnet_state_t state;

  /// List of detected lines, represented by their end-points
  /// in the vehicle frame.
  int num_lines;
  struct {float a[3], b[3];} lines[16];
  
} skynet_roadline_msg_t  __attribute__((packed));



#endif
