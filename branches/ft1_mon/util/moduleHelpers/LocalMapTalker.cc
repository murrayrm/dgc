/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include "LocalMapTalker.hh"

using namespace std;
using namespace Mapper;

CLocalMapTalker::CLocalMapTalker() {
  m_pLocalMapDataBuffer = new char[LOCALMAP_MAX_BUFFER_SIZE];
  DGCcreateMutex(&m_localMapDataBufferMutex);
}

CLocalMapTalker::~CLocalMapTalker() {
  delete [] m_pLocalMapDataBuffer;  
  DGCdeleteMutex(&m_localMapDataBufferMutex);
}

bool CLocalMapTalker::RecvLocalMap(int localMapSocket, Map* localMap, int* pSize) {
  int bytesToReceive = LOCALMAP_MAX_BUFFER_SIZE;
  char* pBuffer = m_pLocalMapDataBuffer;

  vector<Segment> segments;

  // Build the mutex list. We want to protect the data buffer.
  int numMutices = 1;
  pthread_mutex_t* ppMutices[numMutices];
  ppMutices[0] = &m_localMapDataBufferMutex;

  // Get the localMap data from skynet. We want to receive the whole Map object, locking
  // the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  *pSize = m_skynet.get_msg(localMapSocket, m_pLocalMapDataBuffer, bytesToReceive, 0, ppMutices, false, numMutices);
  if(*pSize <= 0)
  {
	cerr << "CLocalMapTalker::RecvLocalMap(): skynet error" << endl;
	DGCunlockMutex(&m_localMapDataBufferMutex);
	return false;
  }
  
  // Decode the message
  int numOfSegments;
  memcpy(&numOfSegments, pBuffer, sizeof(int));
  pBuffer += sizeof(int);
  
  for (int i=0; i<numOfSegments; i++)
  {
  	int segmentID, numOfLanes;
	memcpy(&segmentID, pBuffer, sizeof(int));
	pBuffer += sizeof(int);
	memcpy(&numOfLanes, pBuffer, sizeof(int));
	pBuffer += sizeof(int);
	
  	vector<Lane> lanes;
	
	// Lane
	for (int j=0; j<numOfLanes; j++)
	{
		int laneID, laneWidth;
		
		memcpy(&laneID, pBuffer, sizeof(int));
		pBuffer += sizeof(int);
		memcpy(&laneWidth, pBuffer, sizeof(double));
		pBuffer += sizeof(double);
		
		// Get other info about this lane
		LaneBoundary::Divider lbType, rbType;
		vector<Location> lbLoc, rbLoc;
		int numLocLB, numLocRB, numStopLines, numCheckpoints;
		vector<StopLine> stopLines;
		vector<Checkpoint> checkpoints;
		
		// Left Boundary
		memcpy(&lbType, pBuffer, sizeof(LaneBoundary::Divider));
		pBuffer += sizeof(LaneBoundary::Divider);
		memcpy(&numLocLB, pBuffer, sizeof(int));
		pBuffer += sizeof(int);
		for (int k=0; k<numLocLB; k++)
		{
			Location loc;
			memcpy(&loc, pBuffer, sizeof(Location));
			pBuffer += sizeof(Location);
			lbLoc.push_back(loc);			
		}
		LaneBoundary leftBoundary = LaneBoundary("", lbLoc, lbType);
		
		// Right Boundary		
		memcpy(&rbType, pBuffer, sizeof(LaneBoundary::Divider));
		pBuffer += sizeof(LaneBoundary::Divider);
		memcpy(&numLocRB, pBuffer, sizeof(int));
		pBuffer += sizeof(int);
		cout << numLocRB << endl;
		for (int k=0; k<numLocRB; k++)
		{
			Location loc;
			memcpy(&loc, pBuffer, sizeof(Location));
			pBuffer += sizeof(Location);	
			rbLoc.push_back(loc);			
		}
		LaneBoundary rightBoundary = LaneBoundary("", rbLoc, rbType);
		
		// Stop lines
		memcpy(&numStopLines, pBuffer, sizeof(int));
		pBuffer += sizeof(int);
		for (int k=0; k<numStopLines; k++)
		{
			StopLine stopLine = StopLine("", 0, 0);
			memcpy(&stopLine, pBuffer, sizeof(StopLine));
			pBuffer += sizeof(StopLine);	
			stopLines.push_back(stopLine);		
		}
		
		// Checkpoints
		memcpy(&numCheckpoints, pBuffer, sizeof(int));
		pBuffer += sizeof(int);
		for (int k=0; k<numCheckpoints; k++)
		{
			Checkpoint checkpoint = Checkpoint("", 0, 0);
			memcpy(&checkpoint, pBuffer, sizeof(Checkpoint));
			pBuffer += sizeof(Checkpoint);
			checkpoints.push_back(checkpoint);		
		}
		
		// Construct the corresponding lane object
		lanes.push_back(Lane(laneID, leftBoundary, rightBoundary, stopLines, checkpoints));
	}
	segments.push_back(Segment(segmentID, lanes)); 
	segments[i].print();
  }

  DGCunlockMutex(&m_localMapDataBufferMutex); 
  *localMap = Map(segments);
  return true;
}


bool CLocalMapTalker::SendLocalMap(int localMapSocket, Map* localMap) {
  int bytesToSend = 0;
  int bytesSent;
  char* pBuffer = m_pLocalMapDataBuffer;
  
  vector<Segment> segments = localMap->getAllSegs();
  int numOfSegments = (int)(segments.size());
  
  DGClockMutex(&m_localMapDataBufferMutex); 
  memcpy(pBuffer, &numOfSegments, sizeof(int));
  pBuffer += sizeof(int);
  bytesToSend += sizeof(int);
  
  for (int i=0; i<numOfSegments; i++)
  {
  	int segmentID = segments[i].getID();
  	vector<Lane> lanes = segments[i].getAllLanes();
  	int numOfLanes = lanes.size();
	memcpy(pBuffer, &segmentID, sizeof(int));
	pBuffer += sizeof(int);
	bytesToSend += sizeof(int);  
	memcpy(pBuffer, &numOfLanes, sizeof(int));
	pBuffer += sizeof(int);
	bytesToSend += sizeof(int);
	
	// Lane
	for (int j=0; j<numOfLanes; j++)
	{
		Lane currentLane = lanes[j];
		int laneID = currentLane.getID();
		double laneWidth = currentLane.getWidth();
		memcpy(pBuffer, &laneID, sizeof(int));
		pBuffer += sizeof(int);
		bytesToSend += sizeof(int);
		memcpy(pBuffer, &laneWidth, sizeof(double));
		pBuffer += sizeof(double);
		bytesToSend += sizeof(double);
		
		// Get other info about this lane
		LaneBoundary::Divider lbType = currentLane.getLBType();
		LaneBoundary::Divider rbType = currentLane.getRBType();
		vector<Location> lbLoc = currentLane.getLB();
		vector<Location> rbLoc = currentLane.getRB();
		int numLocLB = (int)(lbLoc.size());
		int numLocRB = (int)(rbLoc.size());
		vector<StopLine> stopLines = currentLane.getStopLines();
		int numStopLines = (int)(stopLines.size());
		vector<Checkpoint> checkpoints = currentLane.getCheckpoints();
		int numCheckpoints = (int)(checkpoints.size());
		
		// Left Boundary		
		memcpy(pBuffer, &lbType, sizeof(LaneBoundary::Divider));
		pBuffer += sizeof(LaneBoundary::Divider);
		bytesToSend += sizeof(LaneBoundary::Divider);
		memcpy(pBuffer, &numLocLB, sizeof(int));
		pBuffer += sizeof(int);
		bytesToSend += sizeof(int);
		for (int k=0; k<numLocLB; k++)
		{
			memcpy(pBuffer, (char*)(&lbLoc[k]), sizeof(Location));
			pBuffer += sizeof(Location);
			bytesToSend += sizeof(Location);			
		}
		
		// Right Boundary		
		memcpy(pBuffer, &rbType, sizeof(LaneBoundary::Divider));
		pBuffer += sizeof(LaneBoundary::Divider);
		bytesToSend += sizeof(LaneBoundary::Divider);
		memcpy(pBuffer, &numLocRB, sizeof(int));
		pBuffer += sizeof(int);
		bytesToSend += sizeof(int);
		for (int k=0; k<numLocRB; k++)
		{
			memcpy(pBuffer, (char*)(&rbLoc[k]), sizeof(Location));
			pBuffer += sizeof(Location);
			bytesToSend += sizeof(Location);			
		}
		
		// Stop lines
		memcpy(pBuffer, &numStopLines, sizeof(int));
		pBuffer += sizeof(int);
		bytesToSend += sizeof(int);
		for (int k=0; k<numStopLines; k++)
		{
			memcpy(pBuffer, (char*)(&stopLines[k]), sizeof(StopLine));
			pBuffer += sizeof(StopLine);
			bytesToSend += sizeof(StopLine);			
		}
		
		// Checkpoints
		memcpy(pBuffer, &numCheckpoints, sizeof(int));
		pBuffer += sizeof(int);
		bytesToSend += sizeof(int);
		for (int k=0; k<numCheckpoints; k++)
		{
			memcpy(pBuffer, (char*)(&checkpoints[k]), sizeof(Checkpoint));
			pBuffer += sizeof(Checkpoint);
			bytesToSend += sizeof(Checkpoint);			
		}
	}
  }

  bytesSent = m_skynet.send_msg(localMapSocket, m_pLocalMapDataBuffer, bytesToSend, 0);
  
  DGCunlockMutex(&m_localMapDataBufferMutex);  
  
  if(bytesSent != bytesToSend)
  {
    cerr << "CLocalMapTalker::SendLocalMap(): sent " << bytesSent << " bytes while expected to send " << bytesToSend << " bytes" << endl;
    return false;
  }

  return true;
}
