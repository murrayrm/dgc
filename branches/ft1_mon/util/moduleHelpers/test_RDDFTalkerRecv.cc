/*!**
 * Lars Cremean and Nok Wongpiromsarn
 * December 8, 2006
 */

#include "RDDFTalker.hh"

class CRDDFTalkerTestRecv : public CRDDFTalker
{
public:
	CRDDFTalkerTestRecv(int sn_key)
		: CSkynetContainer(MODrddftalkertestrecv, sn_key), 
		  CRDDFTalker(true)
	{
		RDDF rddf(NULL, true);

		cout << "about to listen...";
		// int rddfSocket = m_skynet.listen(SNrddf, MODrddftalkertestsend);
		cout << " listening!" << endl;
		while(true)
		{
			//RecvRDDF(rddfSocket, &rddf);
      		if (NewRDDF())
      		{
      			printf("[%s:%d] \n", __FILE__, __LINE__);
				cout << "about to receive an rddf...\n";
				WaitForRDDF();
      			//UpdateRDDF();
      			m_newRddf.print();
				cout << " received an rddf!" << endl;
      		}
		}
	}
};

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;
	CRDDFTalkerTestRecv test(key);
}
