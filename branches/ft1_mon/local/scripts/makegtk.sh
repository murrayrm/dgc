#!/bin/bash

# this builds whatever's necessary to run skynetgui
#
# actually run this and then go to gtk and make there.. for some reason
#
# jason

DGC='../..'

MAPDISPLAY=$DGC/projects/MapDisplay
CCORRIDORPAINTER=$DGC/projects/CCorridorPainter
CCOSTPAINTER=$DGC/projects/CCostPainter
SN_FUSIONMAPPER=$DGC/projects/sn_FusionMapper
GTK=$DGC/projects/SkyNetGUI/gtk


# check if clean argument is given
if [ $1 ]; then
  if [ $1=="clean" ]; then
      echo "-clean switch found!"
  else
      echo "Unrecognized switch!"
  fi
  echo "You must be in the /dgc/local/scripts directory when running this script!"
  echo "If this is the case, press enter to continue, otherwise, quit with Ctrl-C."
  read foo

  cd $MAPDISPLAY; make clean; make; make install; cd -;
  cd $CCORRIDORPAINTER; make clean; make; make install; cd -;
  cd $CCOSTPAINTER; make clean; make; make install; cd -;
  cd $SN_FUSIONMAPPER; make clean; make; cd -;
  cd $GTK; make clean; make; cd -;

else
  echo "You must be in the dgc/local/scripts directory when running this script!"
  echo "If this is the case, press enter to continue, otherwise, quit with Ctrl-C."
  read foo

  cd $MAPDISPLAY; make; make install; cd -;
  cd $CCORRIDORPAINTER; make; make install; cd -;
  cd $CCOSTPAINTER; make; make install; cd -;
  cd $SN_FUSIONMAPPER; make; cd -;
  cd $GTK; make; cd -;

fi

echo " "
echo " "
echo "script finished."


exit 0
