function [model]=buildModel(pt)

pts = [pt(1,:)-39900700;pt(2,:)-378128100];

[foo,num] = size(pts);

s = 1; %side length, in cm
buffer = 5; %how many pts on each side to add

for i=1:1:num-1 % for each segment, building the 'model'
    x1 = pts(1,i);
    y1 = pts(2,i);
    x2 = pts(1,i+1);
    y2 = pts(2,i+1);
    
    m = (y2-y1)/(x2-x1);
    mp = -1/m;
    
    r = sqrt((x2-x1)^2 + (y2-y1)^2);
    
    xa = sqrt(1/(1+m*m));
    if (x2-x1)<0
        xa = -1*xa;
    end
    
    ya = m*xa;

    xb = sqrt(1/(1+mp*mp));
    if (x2-x1)<0
        xb = -1*xb;
    end
    yb = mp*xb;
    endpts = [x1,x2;y1,y2]/1000000000;
    slopes = [xa,xb;ya,yb];
    
    if i==1
        start = 1;
    else
        start = 2*buffer+1;
    end
    for j=start:1:round((r/s)+2*buffer+1)
       for k=1:1:2*buffer+1
       
           model(i,j,k,1) = x1 + (j-buffer-1)*xa*s + (k-buffer-1)*xb*s;
           model(i,j,k,2) = y1 + (j-buffer-1)*ya*s + (k-buffer-1)*yb*s;
           plot(model(i,j,k,1),model(i,j,k,2),'k.');
           hold on;
           
       end
    end
end
plot(pts(1,:),pts(2,:),'ro');

