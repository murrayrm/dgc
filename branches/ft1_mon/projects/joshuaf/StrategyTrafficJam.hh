#ifndef STRATEGY_TRAFFICJAM_HH
#define STRATEGY_TRAFFICJAM_HH

//SUPERCON STRATEGY TITLE: Traffic Jam - SOMETIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: 
//Alice has reached an intersection and has waited for 10 seconds without 
//anything happening. This is a traffic jam, by the DARPA definition. SuperCon
//will now step in and try to resolve this problem. We move ourselves up in the
//queue every two seconds until we are at the front of the line. If another
//vehicle progresses, we default back to the normal intersection
//strategy.

#include "Strategy.hh"
#include "sc_specs.h"

namespace s_trafficjam {

#define TRAFFICJAM_STAGE_LIST(_) \
  _(estop_pause, = 1 )/*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(cut_in_line, ) \
  _(check_for_Traj_1, ) \
  _(broaden_corridor, ) \
  _(check_for_Traj_2, ) \
  _(place_block, ) \
  _(wait_for_route, )
DEFINE_ENUM(trafficjam_stage_names, TRAFFICJAM_STAGE_LIST)

}

using namespace std;
using namespace s_trafficjam;

class CStrategyTrafficJam : public CStrategy
{

public:

  /** CONSTRUCTORS **/
  CStrategyTrafficJam() : CStrategy() {}  

  /** DESTRUCTOR **/
  ~CStrategyTrafficJam() {}


  /** MUTATORS **/

  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};

#endif
