#ifndef SUPERCONLOG_HH
#define SUPERCONLOG_HH

//TITLE: SuperCon SINGLETON File Logging Class HEADER FILE
//DESCRIPTION: Singleton class (just like in Highlander, 'there can only be one!')
//that contains all logging methods (for BOTH SPARROWHAWK & file logging using
//TIMBER).  Note that the methods contained wtihin this class are the ONLY
//way through which any part of superCon can log (to either the SparrowHawk
//status tab, OR a file (using Timber)

//WARNING: This class ASSUMES that DIFFERENT threads will **NEVER** want 
//to log to the SAME FILE

#include "SkynetContainer.h"
#include "sparrowhawk.hh"
#include "CTimberClient.hh"
#include "sc_specs.h"
#include "sn_types.h"

#include <fstream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <vector>

#define SPARROWHAWK_DISP 0
#define COUT_DISP 1
#define NO_DISP 2

#define LOGGING_BUFFER_SIZE 511

#define LOGFILE_STATESERVER    "StateServer.dat"
#define LOGFILE_MSG            "Messages.dat"
#define LOGFILE_STATE          "State.dat"

//SparrowHawk Logging tab & Timber file logging levels (shared)
#define ERROR_MSG                           0
#define WARNING_MSG                         1 
#define STRATEGY_TRANSITION_MSG             2
#define STRATEGY_STAGE_MSG                  3
#define SC_STATE_DIAGNOSTIC_TABLE_UPDATES   4
#define OUTPUT_SN_MSGS                      5
#define DEBUG_MSGS                          6
//insert new logging levels here - must also increment LAST_LOG_LEVEL_VALUE (used
//for error checking on command line)
#define LAST_LOG_LEVEL_VALUE                7

using namespace std;

/* SRV = SuperCon state server
 * DGN = Diagnostics
 * SIF = Strategy Interface
 * STR = Strategies
 */
enum superConComponents { SRV, DGN, SIF, STR };


class CSuperConLog; //forward declaration of class
class SCstate;

//Accessor method to the (sole) instance of the singleton class CSuperConLog
//the accessor is outside the main class so that it can be called without
//requiring a pointer to the class (as this is what it returns)
//
//Example of usage: SuperConLog().CSUPERCONLOG_METHOD_CALL
CSuperConLog &SuperConLog();

//This overload of the method would be used in the Main file (as it requires
//knowledge of the Timber logging level which is a parameter set on
//startup (--option), and the skynet key to be used
CSuperConLog &SuperConLog( int skynet_key, int usrSpefTimberLogLevel, int dispOpt );


//Class Declaration
class CSuperConLog : virtual public CSkynetContainer, public CTimberClient
{
/*** METHODS ***/
public:

  //Get a pointer to THE (singleton) instance of CSuperConLog - if
  //no instance currently exists, then an instance will be created
  static CSuperConLog* Instance();
  
  //This overload Intended to be used to set the Timber 
  //logging level entered by the user on the command line
  //and the skynet key to be used
  static CSuperConLog* Instance( int sn_key, int TimberLogLevel, int dispOpt );
  
protected:
  //As this class is a singleton, the constructor &
  //overloaded assignment operator must all be protected
  
  /** CONSTRUCTOR **/
  CSuperConLog( int skynet_key );

public:
  
  /** DESTRUCTOR **/
  ~CSuperConLog();

  
  /** LOGGING METHODS**/

  /*UN-CONDITIONAL - combined SparrowHawk/Timber logging method
   * ARGS: comp = Author of message (must be a registered superCon component)
   *       logLevel = combined logging level (SparrowHawk & Timber) of message
   *                  passed as an argument
   *       str/list = message to log -> uses the same format as printf()
   */
  void log( superConComponents comp, int logLevel, const char *str, ... );
  
  /*CONDITIONAL - combined SparrowHawk/Timber logging method
   * ARGS: comp = Author of message (must be a registered superCon component)
   *       test = boolean expression that must evaluate to TRUE in order for the
   *              message to be logged (if false nothing is logged)
   *       logLevel = combined logging level (SparrowHawk & Timber) of message
   *                  passed as an argument
   *       str/list = message to log -> uses the same format as printf()
   */
  void log( superConComponents comp, bool test, int logLevel, const char *str, ... );

  
  /* HELPER METHODS */
  //performs set-up required for timber (file) logging
  bool initTimber( int TimberLogLevel );
  
  //writes the string (buffer) authored by [comp] to the output files
  //configured in the switch() statement internal to the method.  ALL
  //entries are pre-fixed with a timestamp (HHMMSS), then <tab>, and then
  //the buffer passed in the argument, and EOL is written *after* each
  //buffer
  void writeTimber( superConComponents comp, char *buffer );

  void writeState(SCstate *pState);
  bool readState(SCstate *pState);

 private:
  /*** Helper functions ***/
  bool verifyLogging();    //open or reopen files

/*** VARIABLES ***/
private:  
  /* SHARED LOGGING  */
  
  //pointer to the SOLE CSuperConLog instance
  static CSuperConLog* pinstance;
  
  static bool passMsgsToSparrowHawk;

  //  static dispOptions dispOptSelected;
  static int dispOptSelected;

  /* TIMBER LOGGING */
  
  //TRUE if timber logging is turned on (centrally) - i.e. timber is running
  //FALSE otherwise
  static bool timberLoggingEnabled;
  
  //location to log files to
  string timberLog_location;

  //Set by user on start-up of superCon - after being set initially, this is 
  //only referenced by the class
  static int timberLoggingLevel;

  //flag used to determine if the level has already been set (true if SET)
  static bool timberLoggingLevelSet;

  //Timestamp related vars
  tm *local;
  char timeStamp[16];
  char shortTimeStamp[7];

  //UNIQUE names of output files (change between consecutive executions
  //of superCon)
  string specific_scStateLog_name;
  string specific_scMsgs_name;

  //Timber logging log file fstream objects
  fstream scStateServer_file;
  fstream scMsgs_file;
  fstream scState_file;
};

#endif
