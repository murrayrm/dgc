//
// opencv0.c - creating and displaying an image using Intel OpenCV
//
#include "cv.h" // includes OpenCV definitions
#include "highgui.h" // includes highGUI definitions
#include <stdio.h>// includes C standard input/output definitions

int main ()

{
IplImage *cvImg; // image used for visualisation
CvSize imgSize; // size of visualisation image
int i = 0, j = 0;
imgSize.width = 640; // visualisation image is
imgSize.height = 480; // 640x480 pixels
// creation of a 8 bits depth gray image
cvImg = cvCreateImage( imgSize, 8, 1 );
// image is filled with gray values
// corresponding to the 256 modulus
// of their position in the image
for ( i = 0; i < imgSize.width; i++ )
for ( j = 0; j < imgSize.height; j++ )
((uchar*)(cvImg->imageData + cvImg->widthStep*j))[i] =
( char ) ( ( i * j ) % 256 );
cvNamedWindow( "Testing OpenCV...", 1 ); // creation of a visualisation window
cvShowImage( "Testing OpenCV...", cvImg ); // image visualisation
cvWaitKey( 0 ); // wait for key
cvDestroyWindow( "image" ); // close window
cvReleaseImage( &cvImg ); // memory release before exiting the application
return( 0 ); // stopping our first program

}
