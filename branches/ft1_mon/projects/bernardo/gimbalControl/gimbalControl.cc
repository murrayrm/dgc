#include "gimbalControl.hh"

using namespace std;

//init
gimbal::gimbal(int skynetKey):  CSkynetContainer (MODgimbalControl, skynetKey)
{
  _QUIT = 0;
  
  commandsDequeue.resize(NUM_COMMANDS_DEQUEUE);
  commandsDequeue.clear();
  
  //this stuff has to be changed so it can come from the command line
  char ans='n';
  cout<<endl<<"Log?(y/n)"<<endl;
  cin>>ans;
  if(ans!='n' && ans!='y') ans = 'n';
  if(ans == 'y')
    {
      optLog=1;
      /*unsigned long long startLog;
      DGCgettime(startLog);
      ofstream logFile(LOGFILENAME);
      logFile <<"#--------------------------------------------------------"<<endl
	      <<"#Logging started @ " << startLog << endl
	      <<"#--------------------------------------------------------"<<endl;
	      logFile.close();*/
      cout<<"Log option selected."<<endl;
    }
  
  cout<<"Display?(y/n)"<<endl;
  cin>>ans;
  if(ans!='n' && ans!='y') ans = 'n';
  if(ans == 'y')
    {
      optDisplay = 1;
      cout<<"Display option selected."<<endl<<endl;
    }

  if(skynetKey)
    {
      optSkynet=1;
    }
  
  optSparrow=0;
  optOverride=1;
  optOutput=MATLAB;
  
  //loadSimFile(LOGFILENAME); //astatesim

  RPYinit.R=0.0;
  //RPYinit.P=78.69;
  RPYinit.P=0.0;
  RPYinit.Y=0.0; //pointed @ 10m in front

  XYZinit.X=2.0;
  XYZinit.Y=0.0;
  XYZinit.Z=-1.5;

  frame.initFrames(XYZinit, DEG2RAD*RPYinit.R, DEG2RAD*RPYinit.P, DEG2RAD*RPYinit.Y);
  define_maxParameters();
}

gimbal::~gimbal()
{
  cout << "gimbal destructor has been called." << endl;
  DGCdeleteMutex(&processCommandsMutex);
  DGCdeleteMutex(&sendCommandsMutex); 
  DGCdeleteMutex(&receiveCommandsMutex);
  delete &commandsDequeue;
}

void gimbal::define_maxParameters()
{
  switch(optOutput)
    {
    case MATLAB:
    case SIMULATION:
       // Roll is not pointed
      //maybe put all these values in RPYangle span(30,90,0)
      maxRPYAngle.P = RPYinit.P + 78.69 + 20.0;//78.69 has to be in a define
      minRPYAngle.P = RPYinit.P + 78.69 - 20.0;
      maxRPYSpeed.P = 400.0;
      // maxRPYAccel.P = ;
      maxRPYAngle.Y = RPYinit.Y + 90.0;
      minRPYAngle.Y = RPYinit.Y - 90.0;
      maxRPYSpeed.Y = 400.0;
      // maxRPYAccel.Y = ;
      maxRPYAngle.R = RPYinit.R + 0.0;
      maxRPYAngle.R = RPYinit.R - 0.0;
      maxRPYSpeed.R = 0.0;
      // maxRPYAccel.R = ;
      break;
    case PERFECT_HORIZON:
      break;
    default:
      cerr << "Unknown output option selected.Quitting..." << endl;
      _QUIT = 1;
      break;
    }
}

//queue work
RPYangle gimbal::XYZ_Vehicle2RPY_Gimbal(XYZcoord desiredXYZ)
{
  RPYangle desiredRPYAngle(0,0,0);
  double flatYaw = 0.0, flatPitch = 0.0, flatRoll = 0.0;
  double correctionYaw = 0.0, correctionPitch = 0.0, correctionRoll = 0.0;
  //desiredXYZ.display();//
  desiredXYZ = frame.transformB2S(desiredXYZ);
  //desiredXYZ.display();//
	
  desiredRPYAngle.R = flatRoll + correctionRoll;
  
  flatYaw = (atan2(desiredXYZ.Y, desiredXYZ.X)) / DEG2RAD;
  desiredRPYAngle.Y = flatYaw + correctionYaw;
  
  flatPitch = (atan2( hypot(desiredXYZ.X,desiredXYZ.Y), desiredXYZ.Z)) / DEG2RAD;
  //  how can I be sure that minObstacleHeight = desired_NED.D works?
  // check [[LADAR Mounting Calibration]] 
  // define minObstacleHeight?
  /*----------------------------------------------------------------
        calculate correction pitch:

        estimate where the most recent LADAR scan intersects the ground:

	      select set of points within w meters of the last ladar scan;
	      calculate mean and standard deviation of straight line distance from the points to the gimbal;
	      discard points more than 1 std. deviation and update set;
	      recalculate set mean;
	      compute running(moving) average; --> ?

	e = d - est; //	compare with distance to desired point;
	correction pitch += K*e; //update correction_pitch; need to define K
  ----------------------------------------------------------------*/
  desiredRPYAngle.P = flatPitch + correctionPitch;

  return desiredRPYAngle;	
}

bool gimbal::check_valid(gimbalCommand & commandToCheck)
{
  bool result_rangeR, result_rangeP, result_rangeY, result_speedR, result_speedP, result_speedY;
  /* range check */
  result_rangeR = ((commandToCheck.RPYAngle_Gimbal.R<=maxRPYAngle.R) && (commandToCheck.RPYAngle_Gimbal.R>=minRPYAngle.R));
  if(!result_rangeR)
    {
      cout<<"Command invalid: roll out of range!"<<endl;
    }		
  result_rangeP = ((commandToCheck.RPYAngle_Gimbal.P<=maxRPYAngle.P) && (commandToCheck.RPYAngle_Gimbal.P>=minRPYAngle.P));
  if(!result_rangeP)
    {
      cout<<"Command invalid: pitch out of range!"<<endl;
    }
  result_rangeY = ((commandToCheck.RPYAngle_Gimbal.Y<=maxRPYAngle.Y) && (commandToCheck.RPYAngle_Gimbal.Y>=minRPYAngle.Y));
  if(!result_rangeY)
    {
      cout<<"Command invalid: yaw out of range!"<<endl;
    }
    
    /* speed check */
  result_speedR = (commandToCheck.RPYSpeed_Gimbal.R<=maxRPYSpeed.R);
  if(!result_speedR)
    {
      cout<<"Command invalid: excessive roll speed!"<<endl;
    }
  result_speedP = (commandToCheck.RPYSpeed_Gimbal.P<=maxRPYSpeed.P);
  if(!result_speedP)
    {
      cout<<"Command invalid: excessive pitch speed!"<<endl;
    }
  result_speedY = (commandToCheck.RPYSpeed_Gimbal.Y<=maxRPYSpeed.Y);
  if(!result_speedY)
    {
      cout<<"Command invalid: excessive yaw speed!"<<endl;
    }
    
  return (result_rangeR && result_rangeP && result_rangeY && result_speedR && result_speedP && result_speedY);
}

void gimbal::processCommand(gimbalCommand & commandToProcess)
{
  if(!(commandToProcess.UTM.N==0 &&
       commandToProcess.UTM.E==0 &&
       commandToProcess.UTM.D==0))
    {

      bool frameUpdated = FALSE;
      NEDcoord pos;

      switch(optOutput)
	{
	case SIMULATION:
	  /*I hate inheritances

	  UpdateStateSim();
	  NEDcoord pos(m_state_sim.ne_coord());
	  frame.updateState(pos, 0, 0, atan2(m_state_sim.ne_coord().N, m_state_sim.ne_coord().E));

	  */

	  //read state from astate
	  if(UpdateState(commandToProcess.timestampIssued))
	    {
	      pos.N = m_state.Northing;
	      pos.E = m_state.Easting;
	      pos.D = m_state.Altitude;
	      frame.updateState(pos, m_state.Pitch,m_state.Roll, m_state.Yaw);
	      frameUpdated = TRUE;
	    }
	  break;
	case MATLAB: 
	  {
#warning "read states from file; watch out for timestamps"	   
	    ifstream statesFile(STATETRAJFILENAME);
	    if(commandToProcess.mode == INTERSECTION)
	      {
		statesFile.close();
		ifstream statesFile(STATEINTFILENAME);
		//cout << "heheh" << endl;
	      }
	    if(statesFile)
	      {
		string stemp;
		unsigned long long ttemp;
		do
		  {
		    getline(statesFile, stemp);
		    
		    istringstream istemp(stemp);
		    istemp >> ttemp >> pos.N >> pos.E >> pos.D;
		    
		    cout << endl << "--------------------------------------------------------------" << endl
			 << "State read from file: " << ttemp << " " << pos.N << " " << pos.E << " " << pos.D
			 << endl << "--------------------------------------------------------------" << endl;    
		  }
		while(ttemp!=commandToProcess.timestampIssued || !(statesFile.eof()));
		if(statesFile.eof())
		  {
		    frameUpdated = FALSE;		 
		  }
		else
		  {
		    frame.updateState(pos, 0, 0, atan2(pos.N, pos.E));
		    frameUpdated = TRUE;
		  }
	      }
	    else
	      {
		cerr << "Error opening states file." << endl;
		frameUpdated = FALSE;
	      }
	    statesFile.close();
	    break;
	  }
	case PERFECT_HORIZON:
	  //yet to be defined
	default:
	  break;
	}

      if(frameUpdated) 
	{
	  commandToProcess.XYZ_Vehicle = frame.transformN2B(commandToProcess.UTM);

	  //log the state after each frame update if needed
	  /*ofstream logFile(LOGFILENAME, ios::app);
	  logFile.precision(15);
	  logFile << endl << "-1 " << commandToProcess.timestampIssued << " " << pos.N << " " << pos.E << " " << pos.D << endl << endl;*/
	}
      else
	{
	  cerr<<"State unreachable @ time of command issuing ("<<commandToProcess.timestampIssued <<") - invalid command."<< endl;
	  commandToProcess.state = INVALID;
	  return;
	}
    }
  
  if(!(commandToProcess.XYZ_Vehicle.X==0 &&
       commandToProcess.XYZ_Vehicle.Y==0 &&
       commandToProcess.XYZ_Vehicle.Z==0))
    {
      commandToProcess.RPYAngle_Gimbal = XYZ_Vehicle2RPY_Gimbal(commandToProcess.XYZ_Vehicle);
    }
  
  if(!(commandToProcess.RPYAngle_Gimbal.R==0 &&
       commandToProcess.RPYAngle_Gimbal.P==0 &&
       commandToProcess.RPYAngle_Gimbal.Y==0))
    {
      if(check_valid(commandToProcess))
	{
	  commandToProcess.state = VALID;
	  commandsDequeue.push_front(commandToProcess);
	}
      else
	{
	  commandToProcess.state = INVALID;//check how to deal with invalid commands
	}
      return;
    }
  
  cerr<<"No UTM, XYZ or RPY specified - invalid command."<<endl;
  commandToProcess.state = INVALID;
}

void gimbal::processCommandsDequeueThread()
{
  /*cerr << "Entering the Process Dequeue Commands Thread!"<<endl;
  DGCcreateMutex(&processCommandsMutex);
  while(!_QUIT)
    {
      //cerr << "Entering the loop..."<<endl;
      
      gimbalCommand commandToProcess;
      deque<gimbalCommand>::iterator it;
      
      for(it=commandsDequeue.end(); it!=commandsDequeue.begin(); it--)
	{
	if(it->state == SENT || it->state == DISCARDED)
	  {
	  DGClockMutex(&processCommandsMutex);
	  commandToProcess = *it;
	
	  processCommand(commandToProcess);
	  if(check_valid(commandToProcess))
	    {
	      it->state = commandToProcess.state = VALID;
	      it->RPYAngle_Gimbal = commandToProcess.RPYAngle_Gimbal;
	    }
	  else
	    {
	      commandToProcess.state = INVALID; //check how to deal with invalid commands
	      commandsDequeue.erase(it);
	    }	
      
	  logOrDisplay(commandToProcess);
	  
	  DGCunlockMutex(&processCommandsMutex);
	  }
	}
    }*/
}


void gimbal::optimizeCommandsDequeue(){}

//comms
void gimbal::receiveDataThread_Commands()
{
  cerr << "Entering the Receive Commands Thread!"<<endl;
  DGCcreateMutex(&receiveCommandsMutex);
  int gimbalCommandSocket = m_skynet.listen(SNgimbalCommand, ALLMODULES);

  gimbalCommand newCommand;
  
  while(!_QUIT)
    {
      //cerr << "Entering the loop..."<<endl;
      int numreceived = m_skynet.get_msg(gimbalCommandSocket, &newCommand, sizeof(gimbalCommand), 0);
      if(numreceived>0)
	{
	  DGClockMutex(&receiveCommandsMutex);
	  
	  if(newCommand.mode==OVERRIDE && optOverride>0) commandsDequeue.clear();     
	  if(commandsDequeue.size()==NUM_COMMANDS_DEQUEUE) commandsDequeue.resize(2*NUM_COMMANDS_DEQUEUE);//receivedCommandsfull = 1?
#warning "//unnecessary if command is initialized before sending"
	  newCommand.timestampSent = 0;
	  DGCgettime(newCommand.timestampReceived);
	  newCommand.state = RECEIVED;
	  
	  logOrDisplay(newCommand); //received     
	  processCommand(newCommand);
	  logOrDisplay(newCommand); //processed
	  
	  DGCunlockMutex(&receiveCommandsMutex);
	  
	}
    }
  //delete newCommand;
}

void gimbal::receiveDataThread_GimbalState()
{
}

void gimbal::sendDataThread_Commands()
{
  cerr << "Entering the Send Commands Thread!"<<endl;
  DGCcreateMutex(&sendCommandsMutex);
  
  while(!_QUIT)
    {
      //cerr << "Entering the loop..."<<endl;
      if(!(commandsDequeue.empty())){
	//DGClockMutex(&sendCommandsMutex);
	
	gimbalCommand commandToSend = commandsDequeue.back();
	if(commandToSend.timestampSent==0 && commandToSend.state==VALID)
	  {
	    DGClockMutex(&sendCommandsMutex);

	    unsigned long long timestampTimeout;
	    DGCgettime(timestampTimeout);
	    timestampTimeout+=TIMEOUT;
	    
	    switch(optOutput)
	      {
	      case MATLAB:
	      case SIMULATION:
		//DGCusleep(100000); -> to test timeout
		commandToSend.timestampSent = send2Simulation(commandToSend);
		break;
	      case PERFECT_HORIZON:
		commandToSend.timestampSent = send2Perfect_Horizon(commandToSend);
		break;
	      default:
		cerr << "Unknown output option selected.Quitting..." << endl;
		_QUIT = 1;
	      }
	    
	    if(commandToSend.timestampSent > 0 && commandToSend.timestampSent<timestampTimeout)
	      {
		commandToSend.state = SENT; 
		logOrDisplay(commandToSend);	    	    
		commandsDequeue.pop_back();
	      }
	    else
	      {		    
		commandToSend.state = DISCARDED;	  	    
		logOrDisplay(commandToSend);
		commandsDequeue.pop_back(); //too many attempts, discarding -> ok? timeout stuff
	      }
	    
	    DGCunlockMutex(&sendCommandsMutex);
	  }
	//else cerr << "timestampSent != 0 or other than valid, shouldn't be here!" << endl;
	
	//DGCunlockMutex(&sendCommandsMutex);
      }  
      else
	{
	  //DGCusleep(50000); //check this
	}
    }
}

unsigned long long gimbal::send2Simulation(gimbalCommand & commandToSend)
{
  unsigned long long timestampSent;
  
  //send

  DGCgettime(timestampSent);
  return timestampSent;
}

unsigned long long gimbal::send2Perfect_Horizon(gimbalCommand & commandToSend)
{
  return 0;
}

//logs
void gimbal::logOrDisplay(gimbalCommand & commandToLogOrDisplay)
{
  
  if(optLog)
    {
      ofstream logFile(LOGFILENAME, ios::app);
      writeCommand(commandToLogOrDisplay, logFile);
      logFile.close();
    }
  
  if(optDisplay)
    {
      writeCommand(commandToLogOrDisplay, cout);
    }
}

void gimbal::writeCommand(gimbalCommand & commandToWrite, ostream & outstream)
{
  string state, mode;
  switch(commandToWrite.state)
    {
    case RECEIVED:
      state = "RECEIVED";
      break;
    case VALID:
      state = "VALID";
      break;
    case INVALID:
      state = "INVALID";
      break;
    case DISCARDED:
      state = "DISCARDED";
      break;
    case SENT:
      state = "SENT";
      break;
    default:
      state="UNKNOWN";
    }
  
  switch(commandToWrite.mode)
    {
    case OVERRIDE:
      mode = "OVERRIDE";
      break;
    case TRACKING:
      mode = "TRACKING";
      break;
    case INTERSECTION:
      mode = "INTERSECTION";
      break;
    case UTURN:
      mode = "UTURN";
      break;
    case NONE:
      mode = "NONE";
      break;
    default:
      mode = "UNKNOWN";
    }
  
  outstream.precision(15);
  
  switch(optOutput)
    {
    case SIMULATION:
      outstream<<state<<" "
	       <<commandToWrite.timestampIssued<<" "
	       <<commandToWrite.timestampReceived<<" "
	       <<commandToWrite.timestampSent<<" "
	       <<mode<<" ("
	       <<commandToWrite.UTM.N<<","
	       <<commandToWrite.UTM.E<<","
	       <<commandToWrite.UTM.D<<") ("
	       <<commandToWrite.XYZ_Vehicle.X<<","
	       <<commandToWrite.XYZ_Vehicle.Y<<","
	       <<commandToWrite.XYZ_Vehicle.Z<<") ("
	       <<commandToWrite.RPYAngle_Gimbal.R<<","
	       <<commandToWrite.RPYAngle_Gimbal.P<<","
	       <<commandToWrite.RPYAngle_Gimbal.Y<<") ("
	       <<commandToWrite.RPYSpeed_Gimbal.R<<","
	       <<commandToWrite.RPYSpeed_Gimbal.P<<","
	       <<commandToWrite.RPYSpeed_Gimbal.Y<<")"<<endl;
      break;
    case MATLAB:        
      if(state=="VALID" || state == "INVALID")
	{
	  if(state=="VALID")
	    {
	      outstream<<"1"<<" ";
	    }	     
	  if(state=="INVALID")
	    {
	      outstream<<"2"<<" ";
	    }
	  outstream<<commandToWrite.timestampIssued<<" "
		   <<commandToWrite.UTM.N<<" "
		   <<commandToWrite.UTM.E<<" "
		   <<commandToWrite.UTM.D<<" "
		   <<commandToWrite.XYZ_Vehicle.X<<" "
		   <<commandToWrite.XYZ_Vehicle.Y<<" "
		   <<commandToWrite.XYZ_Vehicle.Z<<" "
		   <<commandToWrite.RPYAngle_Gimbal.R<<" "
		   <<commandToWrite.RPYAngle_Gimbal.P<<" "
		   <<commandToWrite.RPYAngle_Gimbal.Y<<endl;
	}
      break;
    case PERFECT_HORIZON:
      //yet to be defined;
    default:
      break;
    }
}

void gimbal::writeGimbalState(ostream & outstream)
{
  string output="";
  switch(optOutput)
    {
    case SIMULATION:
      output="SIMULATION";
      break;
    case PERFECT_HORIZON:
      output="PERFECT_HORIZON";
      break;
    default:
      output="UNKNOWN";
    }

  //switch(mode){}
  outstream<<output<<" ";
  switch(optOutput)
    {
    case SIMULATION:
      outstream<<mode<<" ("
	       <<currentRPYAngle.R<<","
	       <<currentRPYAngle.P<<","
	       <<currentRPYAngle.Y<<") ("
	       <<currentRPYSpeed.R<<","
	       <<currentRPYSpeed.P<<","
	       <<currentRPYSpeed.Y<<")"<<endl;
      break;
    case PERFECT_HORIZON:
      break;
    default:
      break;
    }
}

void gimbal::ActiveLoop()
{
  /* process gimbalControlOpts(iopts)
     {
       command line?
       if logging, open file(s) and put headers;
       check timber.cc unixTime2string for:
         (today's date)_command.log
	 (today's date)_command.log
	 timber?
	 ofstream logFile(gimbalControlOpts.logFileFilename, ios::app);
	 logFile.close();
     }
     start threads
     {
       receive/send and log commands
       receive and log gimbal state
       display stuff
     }
     loop
     {
       process dequeue
     }
   */
}

int main(int argc, const char* argv[])
{
  /* set up skynet
     set up gimbal
     set up logging
     set up display
     run activeloop     
   */
  
  int sn_key = -1;
  char* pSkynetkey = getenv("SKYNET_KEY");

  if( pSkynetkey == NULL )
    {
      cerr << "SKYNET_KEY environment variable isn't set" << endl;
    }
  else
  sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  gimbal* test;
  test = new gimbal(sn_key);

  DGCstartMemberFunctionThread(test, &gimbal::sendDataThread_Commands);
  DGCstartMemberFunctionThread(test, &gimbal::receiveDataThread_Commands);
  //DGCstartMemberFunctionThread(test, &gimbal::processCommandsDequeueThread);
  
  while(!(test->_QUIT))
    {
    }

  delete test;
  return 0;
}
