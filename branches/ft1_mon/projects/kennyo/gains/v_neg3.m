% controller for V_0 = 10m/s

close all;
clear all;


% define the matricies for the system, so we can plot its response
V_0 = -3;   % speed of the vehicle- meters/sec
L = 3;      % wheel base of the vehicle in meters
v = V_0;    % different varialble names used for the simulink model
l = L;

A_ = [ 0 V_0; 0 0 ];
B_ = [ 0; V_0/L ];        % I want to plot the frequency response of just the
% process, so there isn't going to be any control yet
C_ = [ 1 0 ];        % Y is the output
D_ = 0;

proc = ss(A_, B_, C_, D_);
process = tf(proc);

% controller

s = tf( 's' );

% loop gain
K = .02;
% frequency at which the derivative term rolls off
T_r = 15;
% derivative gain
T_d = 5;
% integral gain
T_i = 40;

%controller = K * (1 + T_d * s / ((s / T_r) + 1));
controller = K * (1 + T_d * s / ((s / T_r) + 1) + 1/(T_i * s));
L = process * controller;
closed_loop = L / (1 + L);

%figure;
%margin(L);
%figure;
%rlocus(closed_loop);
%figure;
%nyquist(L);
%figure;
%step(closed_loop);

[cont_num, cont_den] = tfdata(controller, 'v');

[Ac, Bc, Cc, Dc] = tf2ss(cont_num, cont_den)

Bc2 = zeros(2,18);
Bc2(1, 2) = Bc(1);
Bc2(1, 11) = -1 * Bc(1);
Dc2 = zeros(1,18);
Dc2(1, 2) = Dc(1);
Dc2(1, 11) = Dc(1);

% convert the continuous time system to a discrete time one
Ts = 0.1;       % follow runs at 10hz..
A = -1 * eye( size(Ac) ) + Ts * Ac;
B = -1 * Ts * Bc;
C = -1 * Cc;
D = -1 * Dc;

% do simulink stuff

cd ../model
sim alice
cd ../v_10

figure;
plot(tout, yout, tout, rout.signals.values);



% transfer functions and ss systems will cause the mat lab file loader in
% follow to segfault
clear s proc process controller L closed_loop;
