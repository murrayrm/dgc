#ifndef _CVECTORMAP_H_
#define _CVECTORMAP_H_
#define CVECTORMAP_MAX_VERTICES 50
#define CVECTORMAP_MAX_STATICOBSTACLES 20
#define CVECTORMAP_MAX_LANEBOUNDARY 20
#define CVECTORMAP_MAX_SAFETYZONES 20
#include "frames/coords.hh"
#include <fstream>
#include <iostream>
#include <rddf.hh>

using namespace std; 

/* A vectorised map is a good idea because: 
1. Binary nature of urban environment (can or can't drive here). 
2. Reduced message sizes to pass around
3. Faster computation (hopefully)

There must be some good 2D geometry packages out there that we could use for primitive collision
detection, but I don't have time to look now. 
*/ 

/** 
	Base object struct for vector map.
*/ 
struct CMapObject
{
	/** Unique ID. */
	int m_ID;
	
	/** Whether the object has changed since the previous planning cycle so that legal routes will have to be rechecked. */
	bool m_updated; 
	
	/** Number of vertices in array. */
  int m_numVertices; 
  
  /** Actual vertices. */
	NEcoord m_vertices[CVECTORMAP_MAX_VERTICES]; 
	
	/** Load vertices from file. */
	void load(ifstream *f); 
	
	/** Print vertices to a file. */
	void print(ofstream *f); 
	
	/** Interpolate. */
	void interpolate(double res);
};

/**
	Struct for a single lane boundary, including array of points and boundary type. 
*/ 
struct CLaneBoundary : CMapObject
{
	/**
		Find the nearest point to coord p and return the index and squared distance.
	*/
	void getNearestPoint(NEcoord p, double *distSqd, int *index);
	
	/** 
		Find yaw at particular index.
	*/
	double getYaw(int index); 
}	;

/** 
	Polygon struct which CLegalRegion and CStaticObstacle inherent from.
*/ 
struct CPolygonObject : CMapObject
{
	/** @brief Returns whether a point is inside the region. 
	@param p The point. 
	Used to check whether a particular trajectory is legal. Uses a ray technique - if the horizontal half line from the point crosses the 
	polygon an odd number of times it is inside the polygon, an even number means it is outside.   */
	int pointInRegion(NEcoord p);
	
	/** @brief Returns whether a line is completely inside the region.
	@param Start of line
	@param End of line
	Could be used to check whether width of Alice crosses into illegal region. 
	!!! Currently just checks the two endpoints! 
	*/
	bool lineInRegion(NEcoord p1, NEcoord p2);
};

/** 
	Struct for region we are allowed to drive in, represented as a closed polygon - the final vertex
		must be at the same location as the first. 
*/ 
struct CLegalRegion : CPolygonObject
{
	// Umm... I'm sure more stuff will have to go in here but can't quite think what... 
	// maybe:
	float m_weight; 
};

/** 
	Struct for safety zone: maybe a parking zone or a road junction, represented as a closed polygon - the final vertex
		must be at the same location as the first. Costs associated with lanes are not calculated in these areas. 
*/ 
struct CSafetyZone : CPolygonObject
{
	// Umm... I'm sure more stuff will have to go in here but can't quite think what... 
	int type; // parking zone or safety zone... (not used)
};

/** Struct for static obstacles. */ 
struct CStaticObstacle : CPolygonObject
{
	// Umm... I'm sure more stuff will have to go in here but can't quite think what... 
	// Maybe it also makes sense to have a weight here - but I'm not sure any obstacle is better than another 
	// I guess a pot hole whould have a lower cost than a tree though! 
};

/** 

Class describing the vectorised map representing region in which we are allowed to drive. 
Includes functionality to emulate a legal driving region using an RDDF for backwards compatibility testing. 
It can check whether a point lies within the region or whether a line intersects it at all.

 */
struct CVectorMap 
{
	/** Pointer to an RDDF that can be used to represent a legal driving region (for testing). */ 
	RDDF *m_pRDDF;
	
	/** Legal driving region object. Assume only one for now. I envisage that if we do use multiple weighted
	legal regions then we will run the planner in each separately so the planner need only be aware of the one
	it is currently trying anyway. */ 
	CLegalRegion m_LegalRegion; 
	
	/** Number of safety zones. */
	int m_numSafetyZones; 
	
	/** Array of pointers to safety zones. */ 
	CSafetyZone *m_pSafetyZone[CVECTORMAP_MAX_SAFETYZONES];
	
	/** Number of static obstacles. */
	int m_numStaticObstacles; 
	
	/** Array of pointers to static obstacles. */ 
	CStaticObstacle *m_pStaticObstacle[CVECTORMAP_MAX_STATICOBSTACLES]; 
	
	/** Number of lane boundaries. */
	int m_numLaneBoundaries; 
	
	/** Array of pointers to lane boundaries. */
	CLaneBoundary *m_pLaneBoundary[CVECTORMAP_MAX_LANEBOUNDARY]; 
		
	/** Returns whether it is legal for Alice to be at a particular point p. */
	bool pointIsLegal(NEcoord p); 
	
	/** Returns whether a particular point p is in a safety zone. */
	bool pointInSafetyZone(NEcoord p); 
	
	/** Find the distance to and yaw of the nearest point on any of the lane boundaries. */
	bool getDistanceAndYawOfNearestLaneBoundary(NEcoord p, double* distance, double* yaw);
	
	/** Save the polygon to file. */
	void print(ofstream *f);
	
	/** Just use the RDDF class functionality to check if points are inside the legal region.  */
	void fakeUsingRDDF(RDDF *pRDDF);
	
	/** Try to generate the polygon from the RDDF - this works for simple RDDFs but does not handle acute angles well. */ 
	void generateFromRDDF(RDDF *pRDDF);
	
	/** Constructor. */
	CVectorMap();
	
	/** THIS WILL NEED AN INTERFACE WITH SENSING LIKE CMap TO BUILD ITS VECTORIZED WORLD REPRESENTATION. */ 
};
#endif
