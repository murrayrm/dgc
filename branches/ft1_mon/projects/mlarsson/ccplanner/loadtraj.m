function traj = loadtraj(fname)

inid = fopen(fname,'r');
outid = fopen('trajtemp','w');

while feof(inid) == 0
    tline = fgetl(inid);
    if (tline(1) == '%' || tline(1) == '#')
        continue
    end
    fprintf(outid,'%s\n',tline);
end
fclose(inid);
fclose(outid);

load trajtemp;
traj = trajtemp;
clear trajtemp;
