#ifndef _CVELOCITY_PLANNER_H_
#define _CVELOCITY_PLANNER_H_
#include "traj.h"
#include "frames/coords.hh"
#include "VehicleState.hh"
#include "CDynMap.hh"
#include <rddf.hh>
#include <math.h>
#include <fstream>
#include <iostream>

/** 

This is a prototype of a velocity planner operating on the arc-length vs. time plane. It varies only one parameter -
k, the spatial variation in velocity. It chooses the highest possible currently value of k for the next 3 seconds, under 
constraints of: maximum acceleration, maximum velocity, moving obstacle avoidance and optionally stopping at the end of the 
trajectory (if m_stopAtEnd is set to true). 

*/ 
class CVelocityPlanner
{
	public:
		
		/**  @brief Find a velocity profile along the spatial path traj. 
		@param Current vehicle state
		@param Current time
		@param Pointer to the spatial path, which must be order 3 so that the velocity can be stored in it
		We find a velocity profile along traj that obeys the constraints of constraints of: maximum acceleration, 
		maximum velocity, moving obstacle avoidance and optionally stopping at the end of the trajectory. This is
		done by iterating through different possible spatial derivatives of speed over the next 3 seconds (i.e. dv/ds). 
		The highest value satisfying these constraints is used. 
		*/ 
		int run(VehicleState *state, double initialTime, CTraj *traj);
		
		/** @brief Check whether a certain value of k = dv/ds is valid over the next 3 seconds. 
		@param k = dv/ds, the spatial derivative of time
		@param startIndex is the current index of the spatial path we are at, i.e. the nearest to Alice's current state. 
		@param current time
		@param Alice's current velocity
		*/
		bool tryK(double k,int startIndex, double initialTime, double initialVelocity);
		
		/** Should we stop at the end of the trajectory? */ 
		bool m_stopAtEnd; 
		
		/** Pointer to the trajectory. */ 
		CTraj *m_pTraj; 
		
		/** Dynamic map containing all detected moving obstacles. */ 
		CDynMap *m_pDynMap; 
		
		/** Useless constructor. */ 
		CVelocityPlanner() {};
		
		/** Useful constructor. */
		CVelocityPlanner(CDynMap *pDynMap); 
};

#endif


