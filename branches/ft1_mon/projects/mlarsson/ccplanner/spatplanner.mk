SPATPLANNER_PATH = $(DGC)/projects/mlarsson/CSpatialPlanner

SPATPLANNER_DEPEND_LIBS = \
	$(TRAJLIB)

SPATPLANNER_DEPEND_SOURCES = \
	CSpatialPlanner.cc \
	CSpatialPlanner.h \
	DGLegalRegion.cc \
	DGLegalRegion.h \
	CCLandMark.cc \
	CCLandMark.h \
	CCElementaryPath.cc \
	CCElementaryPath.h \
	CVelocityPlanner.cc \
	CVelocityPlanner.h

SPATPLANNER_DEPEND = $(SPATPLANNER_DEPEND_LIBS) #$(SPATPLANNER_DEPEND_SOURCES)
