#ifndef _CCELEMENTARYPATH_CC_
#define _CCELEMENTARYPATH_CC_
#include "CCElementaryPath.h"
#include "CCLandMark.h"
#include "fresnel.h"
#include <iostream>
#include <fstream>
ofstream trajFile("matlab_data/cctraj.spat");

inline int sign(double a) { return (a == 0.0) ? 0 : (a<0.0 ? -1 : 1); }; 

double integralD1(double alpha)
{
	float sqrtAlpha = sqrt(alpha/PIBY2); 
	float fresnelCos, fresnelSin;
	fresnel(sqrtAlpha, &fresnelSin, &fresnelCos); 
	return cos(alpha)*fresnelCos + sin(alpha)*fresnelSin; 
}

CCElementaryPath::CCElementaryPath()
{
	m_reverse = false;
}

/** Generate sharpness, sigma and length, l of an elementary clothoid curve linking symmetric postures
p1 and p2. Formula used are from Scheuer 98. Returns whether the values are feasible.  */
bool CCElementaryPath::findSigmaL(CCLandMark *p1, CCLandMark *p2)
{
	NEcoord d = p1->m_pos - p2->m_pos; 
	double separationSquared = d.N*d.N + d.E*d.E;
	double alpha = (p2->m_theta - p1->m_theta)*0.5;
	// from Scheuer 98
	double D1 = integralD1(fabs(alpha));
	m_sigma = 4*PI*sign(alpha)*(D1*D1)/separationSquared; 
	m_length = 2*sqrt(2*alpha/m_sigma);
	double max_curvature = m_sigma*m_length*0.5;
	bool result = ((fabs(max_curvature) < CSPATIALPLANNER_MAX_CURVATURE)&&(fabs(m_sigma)<CSPATIALPLANNER_SHARPNESS)&&(m_length<CSPATIALPLANNER_MAX_SEARCH_LENGTH));
  return result;
}

void CCElementaryPath::getTraj(CTraj *t, int *i)
{
	vector<CPoint>::iterator point=begin(); 
	while (point != end())
	{
		double sinTheta = sin(point->m_theta);
		double cosTheta = cos(point->m_theta);
		t->setPoint((*i)++,point->N,sinTheta,point->m_curvature*cosTheta,point->E,cosTheta,-point->m_curvature*sinTheta);
		point++;
	}
}

double CCElementaryPath::cost()
{
	double cost;
	cost = CSPATIALPLANNER_SEGMENT_COST; // per segment cost
	cost += 0.25*fabs(m_sigma)*m_length*m_length*CSPATIALPLANNER_CURVATURE_COST; // total curvature cost !!! WRONG
	cost += fabs(m_sigma)*m_length*CSPATIALPLANNER_MAX_CURVATURE_COST; // max curvature cost
	cost += m_length*CSPATIALPLANNER_LENGTH_COST; // length cost
	cost += m_length*CSPATIALPLANNER_REVERSE_LENGTH_COST*m_reverse; // length cost
	cost += fabs(m_sigma)*CSPATIALPLANNER_SHARPNESS_COST; // steering effort cost
	return cost;
}

bool CCElementaryPath::finalPoint(CCLandMark *start, CVectorMap *map, NEcoord *f)
{
	// eventually we won't store each segment explicitly, but easier for now (big memory usage though!)
	double cosTheta = cos(start->m_theta);
	double sinTheta = sin(start->m_theta);
	NEcoord dummy;
	// Rotate by 180 degrees for reversing. 
	if (m_reverse) 
	{
		cosTheta *= -1.0;
		sinTheta *= -1.0;
	}
	// Find last coordinate. 
	*f = (NEcoord)back();
	
	dummy = *f;
	// rotate
	f->N = dummy.E*sinTheta + dummy.N*cosTheta; 
	f->E = dummy.E*cosTheta - dummy.N*sinTheta; 
  // translate
	*f += start->m_pos; 

	return map->pointIsLegal(*f);
}

bool CCElementaryPath::startFrom(CCLandMark *start, CVectorMap *map)
{
	// Eventually should not store each segment explicitly, but easier for now (big memory usage though!)
	double cosTheta = cos(start->m_theta);
	double sinTheta = sin(start->m_theta);
	NEcoord dummy,back;
	
	// Rotate by 180 degrees for reversing. 
	if (m_reverse) 
	{
		cosTheta *= -1.0;
		sinTheta *= -1.0;
	}
	
	// Check the end point will be in the legal region before checking along the path.
	if (!finalPoint(start,map,&back))
		return false; 
		
	trajFile.precision(2); 
	trajFile.setf(ios::fixed);
	
	for (vector<CPoint>::iterator point=begin();  point != end(); point++)
	{
		dummy = *point;
		// rotate
		point->N = dummy.E*sinTheta + dummy.N*cosTheta; 
		point->E = dummy.E*cosTheta - dummy.N*sinTheta; 
	  // translate
		*point += start->m_pos;
		
		// update theta
		point->m_theta += start->m_theta;
		if (point->m_theta > PI) point->m_theta -= 2*PI;
		if (point->m_theta < -PI) point->m_theta += 2*PI;
		
		trajFile << point->N << " " << point->E << endl;
		
		/* Test if left and right wheels are in legal zone
		Too processor intensive atm - maybe could precompute these as well? Or go perpendicular to tangent rather than theta? 
		Or could grow static obstacles/shrink legal driving region. 
		NEcoord left;
		double cosThetaHere = cos(*theta);
		double sinThetaHere = sin(*theta);
		left = *coord; 
		left.N += 0.5 * CSPATIALPLANNER_ALICE_WIDTH * cosThetaHere;
		left.E -= 0.5 * CSPATIALPLANNER_ALICE_WIDTH * sinThetaHere;
		NEcoord right;
		right = *coord; 
		right.N -= 0.5 * CSPATIALPLANNER_ALICE_WIDTH * cosThetaHere;
		right.E += 0.5 * CSPATIALPLANNER_ALICE_WIDTH * sinThetaHere;
		
		trajFile << left.N << " " << left.E << endl;
		trajFile << right.N << " " << right.E << endl;  */
		
		if (map != NULL)
			if (!map->pointIsLegal(*point))
				return false;
	}
	return true;
}

/** Used to check whether a path is still legal after an update to the map.  */
bool CCElementaryPath::isLegal(CVectorMap *map)
{	
	for (vector<CPoint>::iterator point=begin();  point != end(); point++)
	{
		if (map != NULL)
			if (!map->pointIsLegal(*point))
				return false;
	}
	return true;
}

void CCElementaryPath::calculate()
{
	CPoint point;
	double s,cosTheta,sinTheta; 
	double prevCosTheta = cos(point.m_theta);
	double prevSinTheta = sin(point.m_theta); 
	double prevk = 0.0; 
	 
	int segmentPoints = (int)(m_length/CCELEMENTARYPATH_SECTION_LENGTH);
	clear();
	// iterate along the trajectory calculating curvature, theta, N and E
	for (int j=1; j<segmentPoints; j++)
	{
		s = j * CCELEMENTARYPATH_SECTION_LENGTH; 
		
		// symmetric curvature
		if (j < segmentPoints/2) 
			point.m_curvature = m_sigma * s;
		else
			point.m_curvature = m_sigma * (m_length-s);
			
		// obey the minimum turning radius constraint
		if (point.m_curvature > CSPATIALPLANNER_MAX_CURVATURE) point.m_curvature = CSPATIALPLANNER_MAX_CURVATURE; 
		if (point.m_curvature < -CSPATIALPLANNER_MAX_CURVATURE) point.m_curvature = -CSPATIALPLANNER_MAX_CURVATURE; 

		// Use trapezoidal numerical integration
		point.m_theta += (prevk + point.m_curvature) * CCELEMENTARYPATH_SECTION_LENGTH * 0.5; // theta(s) = /int^s_0 k(t) dt
 
		// to do this once the algorithm is in place
		cosTheta = cos(point.m_theta);
		sinTheta = sin(point.m_theta);
		point.E +=  (cosTheta + prevCosTheta) * CCELEMENTARYPATH_SECTION_LENGTH * 0.5;  // x(s) = /int^s_0 cos /theta (t) dt
	  point.N +=  (sinTheta + prevSinTheta) * CCELEMENTARYPATH_SECTION_LENGTH * 0.5;  // y(s) = /int^s_0 sin /theta (t) dt

	  prevk = point.m_curvature;
	  prevCosTheta = cosTheta;
	  prevSinTheta = sinTheta; 
	  push_back(point);
	}
}

#endif

