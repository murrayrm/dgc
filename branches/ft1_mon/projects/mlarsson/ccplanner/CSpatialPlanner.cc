#ifndef _CSPATIALPLANNER_CC_
#define _CSPATIALPLANNER_CC_
#include <ctime>
#include "CSpatialPlanner.h"
#include "CVectorMap.h"
#ifndef PI
#define PI 3.1415927 
#endif
inline int sign(double a) { return (a == 0.0) ? 0 : (a<0.0 ? -1 : 1); }; 
inline double atan2(NEcoord d) {return atan2(d.E,d.N); };

ofstream logfile("matlab_data/log.txt");
ofstream rddfpolygon("matlab_data/RDDFpolygon.spat");
ofstream targetsFile("matlab_data/targets.spat");

CSpatialPlanner::CSpatialPlanner(CVectorMap *map)
{
	m_pVectorMap = map;
	generatePaths();
	m_initialised = false; 
	m_start = NULL;
}

/** Saves all the nodes in the tree to file. */
void CSpatialPlanner::saveTree(ostream *f)
{
	if (m_start != NULL)
	{
		m_start->printAll(f); 
	}
}

/** Precompute elementary paths. */
void CSpatialPlanner::generatePaths()
{
	cout << "Planner is precomputing clothoid paths..." << endl;
	for (int i=0; i<CSPATIALPLANNER_SIGMAS; i++)
		for (int j=0; j<CSPATIALPLANNER_LENGTHS; j++)
		{
			// Ok, I know this looks like a mess, but all it actually does is iterate linearly across the range
			// +/- CSPATIALPLANNER_CUBEROOT_SHARPNESS, but starting at the center and working out. 
			double sigma = CSPATIALPLANNER_CUBEROOT_SHARPNESS * ((double)((CSPATIALPLANNER_SIGMAS-1)/2+(int)(i/2)*((i%2==0)?1:-1)) /((CSPATIALPLANNER_SIGMAS-1)/2)-1);
			// We cube things to give more curves with low sharpness, as these are more likely to be useful 
			sigma = sigma*sigma*sigma; 
			// Just a hack to stop them clustering too close together around sharpness = 0 
			double as = fabs(sigma);
			if ((0.00001 < as)&&(as < 0.0015)) as = 0.0015;
			sigma = sign(sigma)*as; 
			// For now just use linear intervals of length
			// We start with the longest ones so that these are considered first and not blocked by shorter ones
			double length = CSPATIALPLANNER_MIN_LENGTH+CSPATIALPLANNER_LENGTH_INTERVAL*(CSPATIALPLANNER_LENGTHS-j-1);
			logfile << "Precomputing path with sigma = " << sigma << " " << "length = " << length << endl; 
			m_pathDB[i][j].m_sigma = sigma;
			m_pathDB[i][j].m_length = length;
			// Actually generate the path 
			m_pathDB[i][j].calculate(); 
		}
	cout << "Precomputing complete. " << endl;
}

/** Explore expands the tree of landmarks from the landmark m */
void CSpatialPlanner::explore(CCLandMark *landmarkToExplore)
{
	logfile << "CSpatialPlanner::explore node at "; landmarkToExplore->log(&logfile); 
	CCElementaryPath path;
	NEcoord endpoint;
	// Remember that we have explored this landmark at the current resolution - it will only be explored
	// again if the resolution is increased. 
	landmarkToExplore->m_exploredDistSqd = m_minDistSqd;
	
	// Iterate through all the precomputed paths
	for (int i=0; i<CSPATIALPLANNER_SIGMAS; i++)
	{
		for (int j=0; j<CSPATIALPLANNER_LENGTHS; j++)
		{
			for (int reverse = 0; reverse <= m_reverseAllowed; reverse++)
			{
		 		path = m_pathDB[i][j];
		 		path.m_reverse = reverse;
		 		// Check the endpoint will be in the legal driving zone
		 		if (!path.finalPoint(landmarkToExplore,m_pVectorMap,&endpoint))
		 			continue; 
		 		// Check the endpoint is not so close to another node as to make it pointless
		 		// !!! Could also check whether orientation is different.
			  if (m_start->nodeWithinTolerance(endpoint,m_minDistSqd))
			  	continue;
			  // Generate the path. Check it does not leave the legal driving region. 
				if (!path.startFrom(landmarkToExplore,m_pVectorMap))
					continue; 
				// Create a new landmark at the end. 
				CCLandMark *c = new CCLandMark();
				c->setPathParentCost(&path,landmarkToExplore,m_pVectorMap); 
				c->calculateF(&m_finish);
				// c->log(&landmarkFile);
				m_numLandmarks++;
				logfile << endl << "Added child at "; c->log(&logfile); 
				// search is not really part of explore, but anyway... 
				bool success = search(landmarkToExplore->m_children.back());
				logfile << "Search success: " << success << endl; 
			}
		}
	}
}

// cos(x^2), used as callback function for numerical integration
double cosXSquared(double x) {return cos(x*x);}

// sin(x^2), used as callback function for numerical integration
double sinXSquared(double x) {return sin(x*x);}

/** Wrapper for the actual search function to deal with possibility of reversing */
bool CSpatialPlanner::search(CCLandMark *p1) 
{
	logfile << "CSpatialPlanner::search node: "; p1->log(&logfile); 
	if (p1->search(&m_finish,false,m_pVectorMap)) 
		return true; 
	if (m_reverseAllowed) 
		if (p1->search(&m_finish,true,m_pVectorMap)) 
			return true; 
	return false; 
}

bool CSpatialPlanner::save(NEcoord *pos) 
{
	int numPoints;
	// Find the optimal trajectory... 
	CCLandMark *bestEndpoint = m_start->findBestEndpoint(); 
	
	// ...and save it into m_traj
	if (bestEndpoint == NULL)
	{
		cout << "Failed to find trajectory from start to finish" << endl; 
		return false;
	}
	else
	{
		// This "pruning" really needs to be dependent on the current vehicle position. 
		m_start = bestEndpoint->choose(bestEndpoint,pos);
		
		m_traj.startDataInput();
		numPoints = bestEndpoint->getNumPoints();
		cout << "Saving trajectory with " << numPoints << " points."<< endl; 
		m_traj.setNumPoints(numPoints); 
		
		bestEndpoint->getTraj(&m_traj);
		
		ofstream f("matlab_data/ftraj");
		m_traj.print(f); 
		
		return true; 
	}
}

CTraj* CSpatialPlanner::getTraj() 
{
	return &m_traj;
}

/** Delete existing graph (if it exists) and setup the start node. */
void CSpatialPlanner::initialise(VehicleState *pState)
{
	cout << "CSpatialPlanner::initialise" << endl;
	m_initialised = true; 
	// Delete existing graph
	if (m_start != NULL)
	{
		cout << "Deleting existing graph..." << endl;
		m_start->deleteDescendants(); 
		delete m_start; 
	}
	// Create and setup start node
	// TODO: We should plan from WHERE WE WILL BE IN 0.4S (processing time)
	m_start = new CCLandMark();
	m_numLandmarks = 0;
	m_start->m_pos.N = pState->Northing_rear();
	m_start->m_pos.E = pState->Easting_rear();
	m_start->m_theta =  PI/2 -  pState->Yaw;
	m_start->calculateF(&m_finish);
}

bool CSpatialPlanner::run(VehicleState *pState, CCLandMark target, double timeAllowed, bool reverseAllowed)
{
	// Setup ----------------------------------------------------------------------------------------
	NEcoord pos = pState->ne_coord();
	
	// Do we need to (re)initialise? 
	if (!m_initialised)
	{
		initialise(pState);
	}
	else
	{
		// Reinitialise if the error from the planner route is too large. 
		NEcoord error = m_traj.getNEcoord(m_traj.getClosestPoint(pos.N,pos.E)) - pos; 
		if (error.norm() > CSPATIALPLANNER_MAX_DIST_ERROR)
		{
			cout << "Error between state and traj too great, scratching old graph." << endl;
			initialise(pState);
		}
	}
	cout << "Planning from : "; m_start->log(&cout); 
	// Check all paths are still legal and update lane costs (I think it is fair to _assume_ the vector 
	//	map will be updated at least once per planning cycle). 
	if (m_start != NULL) // (it shouldn't be!)
	{
		m_start->pruneTree(m_pVectorMap); 
		m_start->updateLaneCost(m_pVectorMap); 
	}
	
	// Load the target point
	m_finish = target;
	m_finish.log(&targetsFile);

	// If this position is significantly different to our previous result, make the necessary calculations. 
	// Also run on first time - runs search on start and initialises m_oldFinish
	NEcoord d = m_finish.m_pos - m_oldFinish.m_pos; 
	if (d.norm() > CSPATIALPLANNER_EPSILON) 
  {
  	cout << "Target waypoint is now at " << m_finish.m_pos.N << " " << m_finish.m_pos.E << ", adjusting graph. " << endl; 
    m_start->finishHasChanged(&m_finish,m_reverseAllowed,m_pVectorMap);
    m_oldFinish = m_finish; 
  }
	
	// Check that the start and finish positions are legal. 
	if (!m_pVectorMap->pointIsLegal(m_start->m_pos))
	{
		cout << "Start location is outside legal region, exiting." << endl;
		return false;
	}
	if (!m_pVectorMap->pointIsLegal(m_finish.m_pos))
	{
		cout << "Finish location is outside legal region, exiting." << endl;
		return false;
	}
	
	// Main planning function ------------------------------------------------------------------------------------
	
	long int startTime = clock(); // record the start time
	cout << "Spatial planner::run() - time allowed: " << timeAllowed << ", reverse allowed: " << reverseAllowed << endl;
	cout << "Planning from : "; m_start->log(&cout); 
	cout << "To : ";  m_finish.log(&cout); 
	int i=0;
	double timeTaken;
	
	CCLandMark *lowestF;
	m_minDistSqd = 4.0; // A lower limit on the squared distance of a new node from all existing ones. 
	m_reverseAllowed = reverseAllowed;

	cout << "Exploration loop..." << endl;
	explore(m_start);
	// Exploration loop 
	do 
	{ 
		logfile << "Iteration " << i++ << " " << "Time taken: " << timeTaken << "s. Searching for lowest f..." << endl;
		// Find the node with the lowest F value (ala A*) 
		lowestF = m_start->findLowestF(m_minDistSqd);
		logfile << "lowestF = " << lowestF << endl;
		// Did we find a node? 
		if (lowestF == NULL) 
		{
			// If all possible nodes have been explored at this resolution, increase the resolution by root 2
			m_minDistSqd *= 0.5;
		}
		else
		{
			// Explore (i.e. grow new nodes) from the node in the tree with the lowest F value 
			explore(lowestF);
		}
		
		// If the resolution has become extremely high this means we have filled the configuration space
		// and there is no point continuing to waste CPU time. 
		if (m_minDistSqd < CSPATIALPLANNER_MIN_MIN_DIST_SQD)
			break; 
		
		timeTaken = (double)(clock() - startTime) / (double)CLOCKS_PER_SEC; 
	} while (timeTaken<timeAllowed);
	cout << m_numLandmarks << " landmarks created in " << timeTaken << " seconds." << endl;
	return save(&pos); 
}

#endif
