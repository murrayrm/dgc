#include "movingObstacle.hh"
//#include "CMapDelta.hh"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "SpoofCom.hh"

#include <getopt.h> 
#include "DGCutils"
#include "sn_msg.hh"
#include "CDynMap.hh"
#define max(a,b) (((a)>(b))?(a):(b))
#define min(a,b) (((a)<(b))?(a):(b))



int main() {
  int numRows, numCols;
  double resRows, resCols;
  double Northing, Easting;


  CDynMap testMap;
  numRows = 200;
  numCols = 200;

  resRows=1.0;
  resCols=1.0;

  Northing = 0.0;
  Easting = 0.0;
  testMap.initMap(Northing,Easting, numRows,numCols, resRows,resCols, 0);

  movingObstacle obstacle;
  obstacle.obstacleID = 0;
  obstacle.obstacleStatus=NEW;

  for(int i = 0; i<10; i++){
    obstacle.cornerPos[i].N = 50;
    obstacle.cornerPos[i].E = 50.0 - (double) i/5;
    obstacle.side1[i][0] = 0;
    obstacle.side1[i][1] = 20;
    obstacle.side2[i][0] = 10;
    obstacle.side2[i][1] = 0;
  }
  DGCgettime(obstacle.timeStamp);

  cout << "Testing CDynMap functionality...\n";
  cout << "Testing getDataRectangle" << endl;
  double theta = 0.1;
  double cos_theta = cos(theta);
  double sin_theta = sin(theta); 
  double time = (double)(obstacle.timeStamp)/1e6;
  double radlat = 50;
  double radlong = 50;
  bool  pF[200*200];
  double pX[200*200];
  double pY[200*200];
  int numCells;
  testMap.getDataRectangle(50, 50, time, cos_theta, sin_theta, radlat, radlong, pF, pX, pY, &numCells);

  cout << "Saving before addition" << endl;

  char dataFilename[10];// = "before";

  sprintf(dataFilename, "%s.dat", "beforeData");
  ofstream dataFile1(dataFilename);
  sprintf(dataFilename, "%s.dat", "beforeX");
  ofstream dataFile2(dataFilename);
  sprintf(dataFilename, "%s.dat", "beforeY");
  ofstream dataFile3(dataFilename);
  
 
  for(int i = 0; i < numCells; i++){
    dataFile1 << pF[i] << ' ';
    dataFile2 << pX[i] << ' ';
    dataFile3 << pY[i] << ' ';
  }
 
  dataFile1.close();
  dataFile2.close();
  dataFile3.close();


  cout << "Adding moving obstacle." << endl;
  testMap.update(&obstacle);

  testMap.getDataRectangle(50, 50, time, cos_theta, sin_theta, radlat, radlong, pF, pX, pY, &numCells);
  cout << "Saving after addition" << endl;


  sprintf(dataFilename, "%s.dat", "afterData");
  ofstream dataFile4(dataFilename);
  sprintf(dataFilename, "%s.dat", "afterX");
  ofstream dataFile5(dataFilename);
  sprintf(dataFilename, "%s.dat", "afterY");
  ofstream dataFile6(dataFilename);
 
  for(int i = 0; i < numCells; i++){
    dataFile4 << pF[i] << ' ';
    dataFile5 << pX[i] << ' ';
    dataFile6 << pY[i] << ' ';
  }
  
 dataFile4.close();
 dataFile5.close();
 dataFile6.close();



 //--------------------------
  testMap.getDataRectangle(50, 50, time+1, cos_theta, sin_theta, radlat, radlong, pF, pX, pY, &numCells);
  cout << "Saving after addition" << endl;


  sprintf(dataFilename, "%s.dat", "after1Data");
  ofstream dataFile7(dataFilename);
  sprintf(dataFilename, "%s.dat", "after1X");
  ofstream dataFile8(dataFilename);
  sprintf(dataFilename, "%s.dat", "after1Y");
  ofstream dataFile9(dataFilename);
 
  for(int i = 0; i < numCells; i++){
    dataFile7 << pF[i] << ' ';
    dataFile8 << pX[i] << ' ';
    dataFile9 << pY[i] << ' ';
  }
  
 dataFile7.close();
 dataFile8.close();
 dataFile9.close();
 //-------------------------
  testMap.getDataRectangle(50, 50, time+2, cos_theta, sin_theta, radlat, radlong, pF, pX, pY, &numCells);
  cout << "Saving after addition" << endl;


  sprintf(dataFilename, "%s.dat", "after2Data");
  ofstream dataFile10(dataFilename);
  sprintf(dataFilename, "%s.dat", "after2X");
  ofstream dataFile11(dataFilename);
  sprintf(dataFilename, "%s.dat", "after2Y");
  ofstream dataFile12(dataFilename);
 
  for(int i = 0; i < numCells; i++){
    dataFile10 << pF[i] << ' ';
    dataFile11 << pX[i] << ' ';
    dataFile12 << pY[i] << ' ';
  }
  
 dataFile10.close();
 dataFile11.close();
 dataFile12.close();
 //---------------------------------

 
 /*
 cout << "Testing getTimeData" << endl;

 bool tData[100];
 double timeVals[100];

 testMap.getTimeData(55, 55, time + 1, 1,tData, timeVals, &numCells);


  sprintf(dataFilename, "%s.dat", "timeData");
  ofstream dataFile13(dataFilename);
  sprintf(dataFilename, "%s.dat", "timeT");
  ofstream dataFile14(dataFilename);
  dataFile13.precision(15); 
  dataFile14.precision(15);
  for(int i = 0; i < numCells; i++){
    dataFile13 << tData[i] << ' ';
    dataFile14 << timeVals[i] << ' ';
  }
  
 dataFile13.close();
 dataFile14.close();
 */



 return 0;
}
