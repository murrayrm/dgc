//SUPERCON STRATEGY TITLE: U-turn - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyLturnReverse.hh"
#include "StrategyHelpers.hh"

void CStrategyLturnReverse::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {
  skipStageOps.reset(); //resets to FALSE
  currentStageName = uturn_stage_names_asString( uturn_stage_names(stgItr.nextStage()) );
  
  switch( stgItr.nextStage() ) {

  case s_uturn::estop_pause_1: 
    //superCon e-stop pause Alice prior to shifting into reverse
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->validAliceStop == false ) {
	SuperConLog().log( STR, WARNING_MSG, "WARNING: Uturn called when not in a valid stop" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  //Places an exit waypoint at the last point of the turn  
  case s_uturn::place_waypoint_exit:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be while restructuring the corridor
	//and waypoints (creating local RNDF, basically)
	SuperConLog().log( STR, WARNING_MSG, "Uturn: e-stop pause not yet in place, cannot broaden corridor -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	ExitWpt->m_pos.N = m_pdiag->northing;
        ExitWpt->m_pos.E = m_pdiag->easting;
	if ( m_pdiag->yaw <= 0 )
	  ExitWpt->m_yaw = m_pdiag->yaw + 3.14159;
	else
	  ExitWpt->m_yaw = m_pdiag->yaw + 3.14159;
	NextWpt = ExitWpt;
	m_pStrategyInterface->stage_placeWaypoint( NextWpt );
	//stage_placeWaypoint may need some work
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  //Checks if we can reach the desired exit waypoint simply by turning toward
  //it.
  case s_uturn::check_for_uturn:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->placewaypoint_complete == false ) {
	//DEFINED placeWaypoint_complete!!!
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Waypoint-exit not yet placed" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_alterWaypoint, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
        SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	//NEED TO DEFINE how and where to place waypoints      
	m_pStrategyInterface->stage_findWaypoint ( ExitWpt );
	stgItr.incrStage();
      }
    }
    break;

  //If findWaypoint ends up returning a non-zero waypoint, this places a 
  //waypoint at the
  //first point of the 3-point turn. If it does not return a waypoint, that
  //means that Alice can successfully find her way out of the u-turn on a
  //simple trajectory, so step ahead to estop_run_exit
  case s_uturn::place_waypoint_1:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->find_waypoint_complete == false) {
        skipStageOps = true;
      }
      else { 
	NextWpt->m_pos.N = m_pdiag->ErrNorthing;
	NextWpt->m_pos.E = m_pdiag->ErrEasting;
	NextWpt->m_yaw = m_pdiag->ErrYaw;
      }

      if(( skipStageOps == false ) && (NextWpt->m_pos.N == 0 )) {
	  stgItr.setStage( s_uturn::estop_run_exit);
	  skipStageOps = true;
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_placeWaypoint( NextWpt );
	//stage_placeWaypoint defined!!!
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  //superCon e-stop run Alice now that she is ready to go to waypoint 1  
  case s_uturn::estop_run_1:
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "LturnReverse: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }      

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->placewaypoint_complete == false ) {
	//DEFINED exitRoute_complete!!!
        //Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Waypoint-1 not yet placed" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_alterWaypoint, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_run);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::estop_pause_2: //pauses Alice again when she gets to waypoint 1
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "UturnReverse: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->atWaypoint == false ) {
         //NOT yet at Waypoint 1 
        //DEFINED atWaypoint!!!
        //Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
	//Used reached_end_of_traj (from trajF interfaces struct) to define this
        SuperConLog().log( STR, WARNING_MSG, "Uturn: Not yet at Waypoint 1 -> will poll" );
        skipStageOps = true;
	/* Should there be a timeout option here? If so, how long should it be?
	checkForPersistLoop( this, persist_loop_?, stgItr.currentStageCount() );*/	
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::find_waypoint_2: //finds out where to put next waypoint
    {
      /* conditions generic to *all strategies* and all stages  should be evaluated *below* this point */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* conditions generic to *all* stages in *this* strategy should be evaluated *below* this point */
      
      /* conditions specific to this stage of the strategy should be evaluated *below* this point */
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be before things get under way
	SuperConLog().log( STR, WARNING_MSG, "Uturn: e-stop pause not yet in place, cannot set up waypt 2 -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }

      if ( m_pdiag->validAliceStop == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Alice not yet stationary, cannot cannot set up waypt 2 -> will poll" );
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      } 
      
      /* all conditional checks should be completed above this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* execute operations for this stage *below* this point */
      if( skipStageOps == false ) {
	//need to define how and where to delete waypoints      
	m_pStrategyInterface->stage_findWaypoint ( ExitWpt );
	//stage_deletewaypoint as yet undefined!!!
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "uturn - stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  //places a waypoint at point 2 of the 3-point turn
  case s_uturn::place_waypoint_2:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
           if( m_pdiag->find_waypoint_complete == false) {
        skipStageOps = true;
      }
      else { 
	NextWpt->m_pos.N = m_pdiag->ErrNorthing;
	NextWpt->m_pos.E = m_pdiag->ErrEasting;
	NextWpt->m_yaw = m_pdiag->ErrYaw;
      }

      //checks for the rare (should never happen) condition that we overshot the
      //desired waypt and now need to back up to it.
      if(( skipStageOps == false ) && (NextWpt->m_pos.N == 0 )) {
	  stgItr.setStage( s_uturn::estop_run_exit);
	  skipStageOps = true;
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE how and where to place waypoints      
	m_pStrategyInterface->stage_placeWaypoint ( NextWpt );
	//stage_placeWaypoint as yet undefined!!!
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;
    
  case s_uturn::gear_reverse: //send-gear change command to adrive ?-->reverse
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if ( m_pdiag->placewaypoint_complete ) {
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Waypoint not yet in place -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_alterWaypoint, stgItr.currentStageCount() );
      }
      
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be to change gear, don't send gear change
	//command until Alice is paused
	SuperConLog().log( STR, WARNING_MSG, "Uturn: superCon e-stop pause not yet in place -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }
      
      if( m_pdiag->safeGearChangeConds == false ) {
	//conditions NOT yet safe for Alice to change gear -> don't send gear change command yet

	skipStageOps = true;	
	SuperConLog().log( STR, WARNING_MSG, "Uturn: alice not yet stationary (safeGearChangeConds == false), can't change gear -> will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_changeGear(sc_interface::reverse_gear);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::trajF_reverse: //send change-mode command to TrajFollower ?-->reverse-MODE
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->adriveGearReverse == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "Uturn - Alice not yet in reverse gear -> will poll" );
	checkForPersistLoop( this, persist_loop_gearChange, stgItr.currentStageCount() );	
      }

      if( m_pdiag->superConPaused == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, ERROR_MSG, "ERROR: Uturn - not yet paused (will NOT change TFmode yet) - will poll" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_changeTFmode( tf_reverse, 50 );
        //Need to define (overload) stage_changeTFmode (...) for one arg!!!!
	//Temporary (?) fix - TrajF will allow Allice to freely reverse over
	//Trajs it recieves from planning if the reverse distance is >= 50 
	//Or need to send to Planner instead?
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;
    
  case s_uturn::estop_run_2: //superCon e-stop run Alice now that she is ready to REVERSE
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }      

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->trajFReverse == false ) {
	SuperConLog().log( STR, WARNING_MSG, "Uturn: TrajFollower not yet in reverse mode" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_trajfModeChange, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_run);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::estop_pause_3: //pauses Alice again when she gets to waypoint 2
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "UturnReverse: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->atWaypoint == false ) {
         //NOT yet at Waypoint 2 
        //DEFINE atWaypoint!!!
        //Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
	//Maybe do this check internally with State data & Waypoint data? Would
	//probably be easier for traffic planner to check. Ask Jessica.
        SuperConLog().log( STR, WARNING_MSG, "Uturn: Not yet at Waypoint 2 -> will poll" );
        skipStageOps = true;
	/* Should there be a timeout option here? If so, how long should it be?
	checkForPersistLoop( this, persist_loop_?, stgItr.currentStageCount() );*/	
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::gear_drive: //send-gear change command to adrive ?-->drive
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
     if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be to change gear, don't send gear change
	//command until Alice is paused
	SuperConLog().log( STR, WARNING_MSG, "Uturn: superCon e-stop pause not yet in place -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }
      
      if( m_pdiag->safeGearChangeConds == false ) {
	//conditions NOT yet safe for Alice to change gear -> don't send gear change command yet

	skipStageOps = true;	
	SuperConLog().log( STR, WARNING_MSG, "Uturn: alice not yet stationary (safeGearChangeConds == false), can't change gear -> will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_changeGear(sc_interface::drive_gear);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::trajF_forwards: //send change-mode command to TrajFollower ?-->reverse-MODE
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->adriveGearReverse == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "Uturn - Alice not yet in drive gear -> will poll" );
	checkForPersistLoop( this, persist_loop_gearChange, stgItr.currentStageCount() );	
      }

      if( m_pdiag->superConPaused == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, ERROR_MSG, "ERROR: Uturn - not yet paused (will NOT change TFmode yet) - will poll" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	transitionStrategy( StrategyNominal, "Uturn - Strategy complete --> nominal" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::estop_run_exit:
    //We are currently lined up to leave the uturn maneuver by reaching the
    //exit waypoint defined in place_exit_waypoint stage. We should be in
    //trajF_forwards mode before proceding.
    {
/* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->trajFReverse == false ) {
	SuperConLog().log( STR, WARNING_MSG, "Uturn: TrajFollower not yet in reverse mode" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_trajfModeChange, stgItr.currentStageCount() );
      }

      if( m_pdiag->superConPaused == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, ERROR_MSG, "ERROR: Uturn - not yet paused (will NOT change TFmode yet) - will poll" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_run);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }


    /** END OF STRATEGY **/
    
  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyUturn::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyUturn::stepForward - default case in nextStep switch() reached" );
      transitionStrategy(StrategyNominal, "ERROR: Uturn - default stage reached!" );
    }
  }


}


void CStrategyLturnReverse::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Uturn - Exiting" );
}

void CStrategyLturnReverse::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  skipStageOps.reset(); //resets to FALSE 
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Uturn - Entering" );
}


