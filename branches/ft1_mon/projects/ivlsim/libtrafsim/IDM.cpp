#include "IDM.h"
#include "CarProperties.h"
#include <cmath>

namespace TrafSim
{
	
float evalIDMAccel(float myVel, float otherVel, float gap, float targetVel, 
				   CarProperties const& myProps, CarProperties const& otherProps)
{		 
	float accelDelta = myProps.accelDelta;
	//	(targetVel < 1.0f && myVel < 4.0f) ?
	//	myProps.accelDelta / 4.0f :
	//	myProps.accelDelta;
	
	// a bit of special handling if there's no car up ahead	
	if (gap == -1.0f)
		return myProps.comfyAccel * (1.0f - pow(myVel / targetVel, accelDelta));
	
	// get the actual gap between the cars' bumpers
	float properGap = gap - myProps.size.y * 0.5f - otherProps.size.y * 0.5f;	
	if (properGap < myProps.minDistance)
		return -myProps.maxAccel * 1.1f;
	
	// evaluate the IDM formula
	float desiredGap = myProps.minDistance + (myVel * myProps.safetyTime) 
					 + myVel * (myVel - otherVel) / (2.0f * myProps.comfyAccel);
	
	return myProps.comfyAccel * (1.0f - pow(myVel / targetVel, accelDelta)
									  - pow(desiredGap / properGap, 2.0f) );
}
	
}
