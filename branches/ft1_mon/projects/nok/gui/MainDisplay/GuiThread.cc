/**
 * GuiThread.cc
 * Revision History:
 * 01/21/2005  hbarnor  Created
 */

#include "GuiThread.h"

MapConfig * GuiThread::get_map_config()
{
  return m_map_config;
}

GuiThread::GuiThread(MapConfig * mc, int sn_key)
  : m_map_config(mc)
{      
  kit = new Gtk::Main(0, NULL);
#if USE_GL
  Gtk::GL::init(0, NULL);
#endif  
window = new Gui(m_map_config, sn_key);
  //Gui::Gui window(m_map_config);
  _sn_key = sn_key;
}

void GuiThread::main()
{
  cout << "Starting Gui Thread" << endl;  

  window->startThreads();
  kit->run(*window); //Shows the window and returns when it is closed.    
  // we want to exit when the window is closed  
  exit(0);
}

void GuiThread::guithread_start_thread() 
{
  cout << "Initializing glib threads" << endl;  
  Glib::thread_init();  
  cout << "Spawning Gui Thread" << endl;  
  m_thread = Glib::Thread::create( sigc::mem_fun(*this, & GuiThread::main), false);
}

GuiThread::~GuiThread()
{
  Gtk::Main::quit();
  // cannot delete a glib thread 
  ///delete m_thread;
  delete window;
  delete kit;  
}

Gui * GuiThread::getGui()
{
  return window;
}
