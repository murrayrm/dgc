
/**
 * MapDisplaySN.hh
 * This class updates the mapconfig datastructure, 
 * allowing for display of RDDF, paths and cost maps on the 
 * skynetdisplay. Messaging is done using the skynet API.
 * Revision History:
 * 02/05/2005  hbarnor  Created
 */

#ifndef MAPDISPLAYSN_HH
#define MAPDISPLAYSN_HH

#include <string>

#include "sn_msg.hh"
#include "DGCutils"
#include "VehicleState.hh"
#include "StateClient.h"
#include "MapdeltaTalker.h"
#include "TrajTalker.h"
#include "RDDFTalker.hh"

#include "MapDisplay/MapDisplay.hh" // for MapConfig
#include "CMapPlus.hh"
#include "MapConstants.h"

#define RTP_VPROFILE_DISPLAY
#ifdef RTP_VPROFILE_DISPLAY
#include "pstream.h"
#endif


#define PATHIDX_HISTORY					0
#define PATHIDX_PLANNERSEED			1
#define PATHIDX_PLANNERINTERM		2
#define PATHIDX_PLANNED					3
#define PATHIDX_GPS             4
#define PATHIDX_NUMPATHS				5

#define NUM_MESSAGES_TO_LOG     10000

using namespace std;

//Values for finding the layer ID of a given layer in the map
enum {
    SNGUI_LAYER_FUSIONMAP_FINAL_COST,

	SNGUI_LAYER_FUSIONMAP_CORRIDOR,

    SNGUI_LAYER_LADAR_ROOFRIGHT_ELEV,
	SNGUI_LAYER_LADAR_ROOFRIGHT_COST,
	SNGUI_LAYER_LADAR_ROOFRIGHT_STDDEV,
 	SNGUI_LAYER_LADAR_ROOFRIGHT_NUM,
// 	SNGUI_LAYER_LADAR_ROOFRIGHT_PITCH,

    SNGUI_LAYER_LADAR_ROOFLEFT_ELEV,
	SNGUI_LAYER_LADAR_ROOFLEFT_COST,
	SNGUI_LAYER_LADAR_ROOFLEFT_STDDEV, 
 	SNGUI_LAYER_LADAR_ROOFLEFT_NUM,
// 	SNGUI_LAYER_LADAR_ROOFLEFT_PITCH,

	SNGUI_LAYER_LADAR_BUMPRIGHT_ELEV,
	SNGUI_LAYER_LADAR_BUMPRIGHT_COST,
	SNGUI_LAYER_LADAR_BUMPRIGHT_STDDEV,
 	SNGUI_LAYER_LADAR_BUMPRIGHT_NUM,
// 	SNGUI_LAYER_LADAR_BUMPRIGHT_PITCH,

	SNGUI_LAYER_LADAR_BUMPLEFT_ELEV,
	SNGUI_LAYER_LADAR_BUMPLEFT_COST,
	SNGUI_LAYER_LADAR_BUMPLEFT_STDDEV,
 	SNGUI_LAYER_LADAR_BUMPLEFT_NUM,
// 	SNGUI_LAYER_LADAR_BUMPLEFT_PITCH,

	SNGUI_LAYER_LADAR_BUMPER_ELEV,
	SNGUI_LAYER_LADAR_BUMPER_COST,
	SNGUI_LAYER_LADAR_BUMPER_STDDEV,
 	SNGUI_LAYER_LADAR_BUMPER_NUM,
// 	SNGUI_LAYER_LADAR_BUMPER_PITCH,

	SNGUI_LAYER_LADAR_REAR_ELEV,
	SNGUI_LAYER_LADAR_REAR_COST,
	SNGUI_LAYER_LADAR_REAR_STDDEV,
 	SNGUI_LAYER_LADAR_REAR_NUM,
// 	SNGUI_LAYER_LADAR_REAR_PITCH,

	SNGUI_LAYER_STEREO_SHORT_ELEV,
	SNGUI_LAYER_STEREO_SHORT_COST,

	SNGUI_LAYER_STEREO_LONG_ELEV,
	SNGUI_LAYER_STEREO_LONG_COST,

  SNGUI_LAYER_STATIC_001,
  SNGUI_LAYER_ROAD,
  SNGUI_LAYER_ROAD_CNT,

  SNGUI_LAYER_TOTAL
};


enum {
	SNGUI_LAYER_LADAR_RIEGL_ELEV,
	SNGUI_LAYER_LADAR_RIEGL_COST,
	SNGUI_LAYER_LADAR_RIEGL_STDDEV,
	SNGUI_LAYER_LADAR_RIEGL_NUM,
	SNGUI_LAYER_LADAR_RIEGL_PITCH,

	SNGUI_LOWRES_LAYER_TOTAL
};

class MapDisplaySN : public CStateClient, public CTrajTalker, public CMapdeltaTalker, public CRDDFTalker
{
  // Declare the Map class.
  CMapPlus myCMap; // the Map
  int* layerNum;//[SNGUI_LAYER_TOTAL];
  CTraj *curPath; // the path received from PathFollower
  sigc::signal1<void, ostream*> * m_writeLogSig; //the signal that connects button in gui to the writeLog function
  MapConfig * m_mapConfig;

	bool bShiftAllMaps;

	sn_msg m_pathTypes[PATHIDX_NUMPATHS];
	modulename m_pathSources[PATHIDX_NUMPATHS];
	RGBA_COLOR m_pathColors[PATHIDX_NUMPATHS];

  sn_msg* m_mapTypes;//[SNGUI_LAYER_TOTAL];
  modulename* m_mapSources;//[SNGUI_LAYER_TOTAL];

	// planned_path, path_history
  PATH paths[PATHIDX_NUMPATHS];
  char* m_pMapDelta[SNGUI_LAYER_TOTAL];
	pthread_mutex_t m_mapdeltaMutex;

	// The actual log data. This data is stored as a circular buffer with
	// m_logMessageIndex indicating the position in this buffer
	string **m_ppLogs;
	bool m_bLogBufferFull;
	int m_logMessageIndex;

	pthread_mutex_t m_loggingMutex;

#ifdef RTP_VPROFILE_DISPLAY
	redi::pstream     m_rtpPipe;
#endif


	int incrementLogMessageIndex(void)
	{
		int oldval = m_logMessageIndex;
		if(++m_logMessageIndex == NUM_MESSAGES_TO_LOG)
		{
			m_logMessageIndex = 0;
			m_bLogBufferFull = true;
		}
		return oldval;
	}
	void writelogs(ostream* pStream = NULL);

public:
  /**
   * Constructor
   * Sets the statestruct of the mapconfig,
   * sets the CMap to the first waypoint
   */
  MapDisplaySN(MapConfig * mapConfig, int sn_key, bool bWaitForStateFill, bool useHighResMaps = true,
							 bool shiftAllMaps = false);
  /**
   * Destructor to handle dynamic
   * memory - currently empty
   */
  ~MapDisplaySN();
  /** 
   * Init - updates the state data and ensures 
   * the state is valid(northing != 0). 
   */
  void init();
  /**
   * Active - contains the loop for listening to
   * incoming skynet messages.
   */
  void active();

  /** These functions read in a new traj/mapdelta/state and update the map **/
  void getTrajThread(void* pArg);
  void getMapDeltaThread(void* pArg);
  void getRoadThread();           //Run from getMapDeltaThread to get the road data (which isn't sent as mapdelta
  void writeLogThread(void);
  void getStateThread(void);
  void getRDDFThread();

	int numLayers;
};

#endif
