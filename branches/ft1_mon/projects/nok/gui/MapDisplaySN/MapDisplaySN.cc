/**
 * MapDisplaySN.cc
 * Revision History:
 * 02/05/2005  hbarnor  Created
 */


#include "MapDisplaySN.hh"
#include "RoadPainter.hh"
#include "Road.hh"

#include <unistd.h>
#include <iostream>
#include <string>
#include <sstream>

#define MAX_DELTA_SIZE  10000000
#define THREAD_INTERVAL 50000

MapDisplaySN::MapDisplaySN(MapConfig * mapConfig, int sn_key, bool bWaitForStateFill, bool useHighResMaps, bool shiftAllMaps)
  : CSkynetContainer(SNMapDisplay,sn_key), 
		CStateClient(bWaitForStateFill), CRDDFTalker(true),
		m_mapConfig(mapConfig)
{
#ifdef RTP_VPROFILE_DISPLAY
	m_rtpPipe.open("./rtp");
#endif

	bShiftAllMaps = shiftAllMaps;

  m_mapConfig->set_state_struct(&m_state);
  m_mapConfig->set_actuator_state_struct(&m_actuatorState);

  //RDDFData& x = mapConfig->get_rddf_data(0);

if(useHighResMaps) {
		numLayers = SNGUI_LAYER_TOTAL;
		layerNum = new int[SNGUI_LAYER_TOTAL];
		m_mapTypes = new sn_msg[SNGUI_LAYER_TOTAL];
		m_mapSources = new modulename[SNGUI_LAYER_TOTAL];

		myCMap.initMap(CONFIG_FILE_DEFAULT_MAP);

//============ Layer 1 ================//
		layerNum[SNGUI_LAYER_FUSIONMAP_FINAL_COST] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_FUSIONMAP_FINAL_COST], "Final Cost");
		m_mapTypes[SNGUI_LAYER_FUSIONMAP_FINAL_COST] = SNfusiondeltamap;
		m_mapSources[SNGUI_LAYER_FUSIONMAP_FINAL_COST] = SNfusionmapper;

//============ Layer 2 ================//
		layerNum[SNGUI_LAYER_FUSIONMAP_CORRIDOR] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_FUSIONMAP_CORRIDOR], "RDDF Corridor");
		m_mapTypes[SNGUI_LAYER_FUSIONMAP_CORRIDOR] = SNdeltaMapCorridor;
		m_mapSources[SNGUI_LAYER_FUSIONMAP_CORRIDOR] = SNfusionmapper;
				
//============ Layer 3 ================//
		layerNum[SNGUI_LAYER_LADAR_ROOFRIGHT_ELEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_ROOFRIGHT_ELEV], "LADAR RoofRight Mean Elev");
		m_mapTypes[SNGUI_LAYER_LADAR_ROOFRIGHT_ELEV] = SNladarRoofRightDeltaMapElev;
		m_mapSources[SNGUI_LAYER_LADAR_ROOFRIGHT_ELEV] = MODladarFeeder;      
		
//============ Layer 4 ================//		
		layerNum[SNGUI_LAYER_LADAR_ROOFRIGHT_COST] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_ROOFRIGHT_COST], "LADAR RoofRight Cost");
		m_mapTypes[SNGUI_LAYER_LADAR_ROOFRIGHT_COST] = SNladarRoofRightDeltaMapCost;
		m_mapSources[SNGUI_LAYER_LADAR_ROOFRIGHT_COST] = MODladarFeeder;

//============ Layer 5 ================//		
		layerNum[SNGUI_LAYER_LADAR_ROOFRIGHT_STDDEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_ROOFRIGHT_STDDEV], "LADAR RoofRight StdDev");
		m_mapTypes[SNGUI_LAYER_LADAR_ROOFRIGHT_STDDEV] = SNladarRoofRightDeltaMapStdDev;
		m_mapSources[SNGUI_LAYER_LADAR_ROOFRIGHT_STDDEV] = MODladarFeeder;

//============ Layer 6 ================//
		layerNum[SNGUI_LAYER_LADAR_ROOFRIGHT_NUM] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_NUM);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_ROOFRIGHT_NUM], "LADAR RoofRight Num");
		m_mapTypes[SNGUI_LAYER_LADAR_ROOFRIGHT_NUM] = SNladarRoofRightDeltaMapNum;
		m_mapSources[SNGUI_LAYER_LADAR_ROOFRIGHT_NUM] = MODladarFeeder;

// 		layerNum[SNGUI_LAYER_LADAR_ROOFRIGHT_PITCH] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_PITCH);
// 		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_ROOFRIGHT_PITCH], "LADAR RoofRight Pitch");
// 		m_mapTypes[SNGUI_LAYER_LADAR_ROOFRIGHT_PITCH] = SNladarRoofRightDeltaMapPitch;
// 		m_mapSources[SNGUI_LAYER_LADAR_ROOFRIGHT_PITCH] = MODladarFeeder;

//============ Layer 7 ================//
  	 	layerNum[SNGUI_LAYER_LADAR_ROOFLEFT_ELEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_ROOFLEFT_ELEV], "LADAR RoofLeft Mean Elev");
		m_mapTypes[SNGUI_LAYER_LADAR_ROOFLEFT_ELEV] = SNladarRoofLeftDeltaMapElev;
		m_mapSources[SNGUI_LAYER_LADAR_ROOFLEFT_ELEV] = MODladarFeeder;      

//============ Layer 8 ================//		
		layerNum[SNGUI_LAYER_LADAR_ROOFLEFT_COST] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_ROOFLEFT_COST], "LADAR RoofLeft Cost");
		m_mapTypes[SNGUI_LAYER_LADAR_ROOFLEFT_COST] = SNladarRoofLeftDeltaMapCost;
		m_mapSources[SNGUI_LAYER_LADAR_ROOFLEFT_COST] = MODladarFeeder;
		
//============ Layer 9 ================//		
		layerNum[SNGUI_LAYER_LADAR_ROOFLEFT_STDDEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_ROOFLEFT_STDDEV], "LADAR RoofLeft StdDev");
		m_mapTypes[SNGUI_LAYER_LADAR_ROOFLEFT_STDDEV] = SNladarRoofLeftDeltaMapStdDev;
		m_mapSources[SNGUI_LAYER_LADAR_ROOFLEFT_STDDEV] = MODladarFeeder;

//============ Layer 10 ================//
		layerNum[SNGUI_LAYER_LADAR_ROOFLEFT_NUM] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_NUM);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_ROOFLEFT_NUM], "LADAR RoofLeft Num");
		m_mapTypes[SNGUI_LAYER_LADAR_ROOFLEFT_NUM] = SNladarRoofLeftDeltaMapNum;
		m_mapSources[SNGUI_LAYER_LADAR_ROOFLEFT_NUM] = MODladarFeeder;

// 		layerNum[SNGUI_LAYER_LADAR_ROOFLEFT_PITCH] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_PITCH);
// 		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_ROOFLEFT_PITCH], "LADAR RoofLeft Pitch");
// 		m_mapTypes[SNGUI_LAYER_LADAR_ROOFLEFT_PITCH] = SNladarRoofLeftDeltaMapPitch;
// 		m_mapSources[SNGUI_LAYER_LADAR_ROOFLEFT_PITCH] = MODladarFeeder;

//============ Layer 11 ================//
		layerNum[SNGUI_LAYER_LADAR_BUMPRIGHT_ELEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPRIGHT_ELEV], "LADAR BumpRight Mean Elev");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPRIGHT_ELEV] = SNladarBumpRightDeltaMapElev;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPRIGHT_ELEV] = MODladarFeeder;      

//============ Layer 12 ================//		
		layerNum[SNGUI_LAYER_LADAR_BUMPRIGHT_COST] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPRIGHT_COST], "LADAR BumpRight Cost");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPRIGHT_COST] = SNladarBumpRightDeltaMapCost;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPRIGHT_COST] = MODladarFeeder;

//============ Layer 13 ================//	
		layerNum[SNGUI_LAYER_LADAR_BUMPRIGHT_STDDEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPRIGHT_STDDEV], "LADAR BumpRight StdDev");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPRIGHT_STDDEV] = SNladarBumpRightDeltaMapStdDev;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPRIGHT_STDDEV] = MODladarFeeder;

//============ Layer 14 ================//
		layerNum[SNGUI_LAYER_LADAR_BUMPRIGHT_NUM] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_NUM);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPRIGHT_NUM], "LADAR BumpRight Num");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPRIGHT_NUM] = SNladarBumpRightDeltaMapNum;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPRIGHT_NUM] = MODladarFeeder;

// 		layerNum[SNGUI_LAYER_LADAR_BUMPRIGHT_PITCH] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_PITCH);
// 		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPRIGHT_PITCH], "LADAR BumpRight Pitch");
// 		m_mapTypes[SNGUI_LAYER_LADAR_BUMPRIGHT_PITCH] = SNladarBumpRightDeltaMapPitch;
// 		m_mapSources[SNGUI_LAYER_LADAR_BUMPRIGHT_PITCH] = MODladarFeeder;

//============ Layer 15 ================//
		layerNum[SNGUI_LAYER_LADAR_BUMPLEFT_ELEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPLEFT_ELEV], "LADAR BumpLeft Mean Elev");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPLEFT_ELEV] = SNladarBumpLeftDeltaMapElev;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPLEFT_ELEV] = MODladarFeeder;      

//============ Layer 16 ================//		
		layerNum[SNGUI_LAYER_LADAR_BUMPLEFT_COST] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPLEFT_COST], "LADAR BumpLeft Cost");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPLEFT_COST] = SNladarBumpLeftDeltaMapCost;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPLEFT_COST] = MODladarFeeder;

//============ Layer 17 ================//	
		layerNum[SNGUI_LAYER_LADAR_BUMPLEFT_STDDEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPLEFT_STDDEV], "LADAR BumpLeft StdDev");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPLEFT_STDDEV] = SNladarBumpLeftDeltaMapStdDev;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPLEFT_STDDEV] = MODladarFeeder;

//============ Layer 18 ================//
		layerNum[SNGUI_LAYER_LADAR_BUMPLEFT_NUM] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_NUM);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPLEFT_NUM], "LADAR BumpLeft Num");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPLEFT_NUM] = SNladarBumpLeftDeltaMapNum;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPLEFT_NUM] = MODladarFeeder;

// 		layerNum[SNGUI_LAYER_LADAR_BUMPLEFT_PITCH] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_PITCH);
// 		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPLEFT_PITCH], "LADAR BumpLeft Pitch");
// 		m_mapTypes[SNGUI_LAYER_LADAR_BUMPLEFT_PITCH] = SNladarBumpLeftDeltaMapPitch;
// 		m_mapSources[SNGUI_LAYER_LADAR_BUMPLEFT_PITCH] = MODladarFeeder;


//============ Layer 19 ================//
		layerNum[SNGUI_LAYER_LADAR_REAR_ELEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_REAR_ELEV], "LADAR Rear Mean Elev");
		m_mapTypes[SNGUI_LAYER_LADAR_REAR_ELEV] = SNladarRearDeltaMapElev;
		m_mapSources[SNGUI_LAYER_LADAR_REAR_ELEV] = MODladarFeeder;      

//============ Layer 20 ================//		
		layerNum[SNGUI_LAYER_LADAR_REAR_COST] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_REAR_COST], "LADAR Rear Cost");
		m_mapTypes[SNGUI_LAYER_LADAR_REAR_COST] = SNladarRearDeltaMapCost;
		m_mapSources[SNGUI_LAYER_LADAR_REAR_COST] = MODladarFeeder;

//============ Layer 21 ================//		
		layerNum[SNGUI_LAYER_LADAR_REAR_STDDEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_REAR_STDDEV], "LADAR Rear StdDev");
		m_mapTypes[SNGUI_LAYER_LADAR_REAR_STDDEV] = SNladarRearDeltaMapStdDev;
		m_mapSources[SNGUI_LAYER_LADAR_REAR_STDDEV] = MODladarFeeder;

//============ Layer 22 ================//
		layerNum[SNGUI_LAYER_LADAR_REAR_NUM] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_NUM);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_REAR_NUM], "LADAR Rear Num");
		m_mapTypes[SNGUI_LAYER_LADAR_REAR_NUM] = SNladarRearDeltaMapNum;
		m_mapSources[SNGUI_LAYER_LADAR_REAR_NUM] = MODladarFeeder;

// 		layerNum[SNGUI_LAYER_LADAR_REAR_PITCH] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_PITCH);
// 		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_REAR_PITCH], "LADAR Rear Pitch");
// 		m_mapTypes[SNGUI_LAYER_LADAR_REAR_PITCH] = SNladarRearDeltaMapPitch;
// 		m_mapSources[SNGUI_LAYER_LADAR_REAR_PITCH] = MODladarFeeder;

//============ Layer 23 ================//
		layerNum[SNGUI_LAYER_LADAR_BUMPER_ELEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPER_ELEV], "LADAR Bumper Mean Elev");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPER_ELEV] = SNladarBumperDeltaMapElev;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPER_ELEV] = MODladarFeeder;      

//============ Layer 24 ================//		
		layerNum[SNGUI_LAYER_LADAR_BUMPER_COST] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPER_COST], "LADAR Bumper Cost");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPER_COST] = SNladarBumperDeltaMapCost;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPER_COST] = MODladarFeeder;

//============ Layer 25 ================//		
		layerNum[SNGUI_LAYER_LADAR_BUMPER_STDDEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPER_STDDEV], "LADAR Bumper StdDev");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPER_STDDEV] = SNladarBumperDeltaMapStdDev;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPER_STDDEV] = MODladarFeeder;

//============ Layer 26 ================//
		layerNum[SNGUI_LAYER_LADAR_BUMPER_NUM] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_NUM);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPER_NUM], "LADAR Bumper Num");
		m_mapTypes[SNGUI_LAYER_LADAR_BUMPER_NUM] = SNladarBumperDeltaMapNum;
		m_mapSources[SNGUI_LAYER_LADAR_BUMPER_NUM] = MODladarFeeder;

// 		layerNum[SNGUI_LAYER_LADAR_BUMPER_PITCH] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_PITCH);
// 		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_BUMPER_PITCH], "LADAR Bumper Pitch");
// 		m_mapTypes[SNGUI_LAYER_LADAR_BUMPER_PITCH] = SNladarBumperDeltaMapPitch;
// 		m_mapSources[SNGUI_LAYER_LADAR_BUMPER_PITCH] = MODladarFeeder;

//============ Layer 27 ================//
		layerNum[SNGUI_LAYER_STEREO_SHORT_ELEV] = myCMap.addLayer<double>(CONFIG_FILE_STEREO_MEAN_ELEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_STEREO_SHORT_ELEV], "Stereo Short Mean Elev");
		m_mapTypes[SNGUI_LAYER_STEREO_SHORT_ELEV] = SNstereoShortDeltaMapElev;
		m_mapSources[SNGUI_LAYER_STEREO_SHORT_ELEV] = MODstereofeeder;

//============ Layer 28 ================//		
		layerNum[SNGUI_LAYER_STEREO_SHORT_COST] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_STEREO_SHORT_COST], "Stereo Short Cost");
		m_mapTypes[SNGUI_LAYER_STEREO_SHORT_COST] = SNstereoShortDeltaMapCost;
		m_mapSources[SNGUI_LAYER_STEREO_SHORT_COST] = MODstereofeeder;

//============ Layer 29 ================//
		layerNum[SNGUI_LAYER_STEREO_LONG_ELEV] = myCMap.addLayer<double>(CONFIG_FILE_STEREO_MEAN_ELEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_STEREO_LONG_ELEV], "Stereo Long Mean Elev");
		m_mapTypes[SNGUI_LAYER_STEREO_LONG_ELEV] = SNstereoLongDeltaMapElev;
		m_mapSources[SNGUI_LAYER_STEREO_LONG_ELEV] = MODstereofeeder;

//============ Layer 30 ================//
		layerNum[SNGUI_LAYER_STEREO_LONG_COST] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_STEREO_LONG_COST], "Stereo Long Cost");
		m_mapTypes[SNGUI_LAYER_STEREO_LONG_COST] = SNstereoLongDeltaMapCost;
		m_mapSources[SNGUI_LAYER_STEREO_LONG_COST] = MODstereofeeder;

//============ Layer 31 ================//
		layerNum[SNGUI_LAYER_STATIC_001] = myCMap.addLayer<double>(CONFIG_FILE_STATIC);
		m_mapTypes[SNGUI_LAYER_STATIC_001] = SNstaticdeltamap;
		m_mapSources[SNGUI_LAYER_STATIC_001] = MODstaticpainter;

//============ Layer 32 ================//		
		layerNum[SNGUI_LAYER_ROAD] = myCMap.addLayer<double>(CONFIG_FILE_ROAD);
		m_mapTypes[SNGUI_LAYER_ROAD] = SNroad2map;
		m_mapSources[SNGUI_LAYER_ROAD] = SNroadfinding;

//============ Layer 33 ================//
		layerNum[SNGUI_LAYER_ROAD_CNT] = myCMap.addLayer<double>(CONFIG_FILE_ROAD_COUNTER);
		m_mapTypes[SNGUI_LAYER_ROAD_CNT] = SNroad2map;
		m_mapSources[SNGUI_LAYER_ROAD_CNT] = SNroadfinding;
	} else {
		numLayers = SNGUI_LOWRES_LAYER_TOTAL;
		layerNum = new int[SNGUI_LOWRES_LAYER_TOTAL];
		m_mapTypes = new sn_msg[SNGUI_LOWRES_LAYER_TOTAL];
		m_mapSources = new modulename[SNGUI_LOWRES_LAYER_TOTAL];

		myCMap.initMap(CONFIG_FILE_LOWRES_MAP);

		layerNum[SNGUI_LAYER_LADAR_RIEGL_ELEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_RIEGL_ELEV], "LADAR Riegl Mean Elev");
		m_mapTypes[SNGUI_LAYER_LADAR_RIEGL_ELEV] = SNladarRieglDeltaMapElev;
		m_mapSources[SNGUI_LAYER_LADAR_RIEGL_ELEV] = MODladarFeeder;      
		
		layerNum[SNGUI_LAYER_LADAR_RIEGL_COST] = myCMap.addLayer<double>(CONFIG_FILE_COST);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_RIEGL_COST], "LADAR Riegl Cost");
		m_mapTypes[SNGUI_LAYER_LADAR_RIEGL_COST] = SNladarRieglDeltaMapCost;
		m_mapSources[SNGUI_LAYER_LADAR_RIEGL_COST] = MODladarFeeder;
		
		layerNum[SNGUI_LAYER_LADAR_RIEGL_STDDEV] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_RIEGL_STDDEV], "LADAR Riegl StdDev");
		m_mapTypes[SNGUI_LAYER_LADAR_RIEGL_STDDEV] = SNladarRieglDeltaMapStdDev;
		m_mapSources[SNGUI_LAYER_LADAR_RIEGL_STDDEV] = MODladarFeeder;

		layerNum[SNGUI_LAYER_LADAR_RIEGL_NUM] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_NUM);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_RIEGL_NUM], "LADAR Riegl Num");
		m_mapTypes[SNGUI_LAYER_LADAR_RIEGL_NUM] = SNladarRieglDeltaMapNum;
		m_mapSources[SNGUI_LAYER_LADAR_RIEGL_NUM] = MODladarFeeder;

		layerNum[SNGUI_LAYER_LADAR_RIEGL_PITCH] = myCMap.addLayer<double>(CONFIG_FILE_LADAR_PITCH);
		myCMap.setLayerLabel(layerNum[SNGUI_LAYER_LADAR_RIEGL_PITCH], "LADAR Riegl Pitch");
		m_mapTypes[SNGUI_LAYER_LADAR_RIEGL_PITCH] = SNladarRieglDeltaMapPitch;
		m_mapSources[SNGUI_LAYER_LADAR_RIEGL_PITCH] = MODladarFeeder;
	}

  m_mapConfig->set_cmap(&myCMap, &m_mapdeltaMutex);



  static const sn_msg pathTypes[] =
    {SNtraj, SNtrajPlannerSeed, SNtrajPlannerInterm, SNtraj, SNtrajReverse};
  static const modulename pathSources[] =
    {ALLMODULES,SNplanner,SNplanner,SNplanner, SNtrajfollower};
  static const RGBA_COLOR pathColors[] = 
    {RGBA_COLOR(1.0, 0.0, 0.0),
     RGBA_COLOR(0.0, 0.5, 0.0),
     RGBA_COLOR(0.0, 1.0, 1.0),
     RGBA_COLOR(1.0, 0.0, 1.0),
     RGBA_COLOR(.1, 0.1, .1),
    };

  memcpy(m_pathTypes, pathTypes, sizeof(m_pathTypes));
  memcpy(m_pathSources, pathSources, sizeof(m_pathSources));
  memcpy(m_pathColors, pathColors, sizeof(m_pathColors));

  int i;
  // initialize paths
  // Set the color for our path history to re
  for(i=0; i<PATHIDX_NUMPATHS; i++)
	{
    paths[i].color = m_pathColors[i];
		DGCcreateMutex(&paths[i].pathMutex);
	}

  // memory for the map deltas
  for(i=0; i<numLayers; i++)
    m_pMapDelta[i] = new char[MAX_DELTA_SIZE];

  // set the paths
  m_mapConfig->set_paths( paths, PATHIDX_NUMPATHS );

	DGCcreateMutex(&m_mapdeltaMutex);
	DGCcreateMutex(&m_loggingMutex);

	m_ppLogs = new string*[NUM_MESSAGES_TO_LOG];
	for(i=0; i<NUM_MESSAGES_TO_LOG; i++)
		m_ppLogs[i] = NULL;

	m_logMessageIndex = 0;
	m_bLogBufferFull = false;
}

MapDisplaySN::~MapDisplaySN()
{
  int i;
  for(i=0; i<numLayers; i++)
    delete m_pMapDelta[i];

  for(i=0; i<PATHIDX_NUMPATHS; i++)
		DGCdeleteMutex(&paths[i].pathMutex);

	DGCdeleteMutex(&m_loggingMutex);
	DGCdeleteMutex(&m_mapdeltaMutex);

	for(i=0; i<NUM_MESSAGES_TO_LOG; i++)
		if(m_ppLogs[i] != NULL)
			delete m_ppLogs[i];
		
	delete[] m_ppLogs;
	
	delete m_writeLogSig;
}

void MapDisplaySN::init()
{
	printf("[%s:%d] Calling UpdateState() in init()... ", __FILE__, __LINE__);
	UpdateState(); // update the state
	printf(" ...done.");
	m_writeLogSig = new sigc::signal<void, ostream*>();
	m_writeLogSig->connect(sigc::mem_fun(this,&MapDisplaySN::writelogs));
	m_mapConfig->set_writeLogSig(m_writeLogSig);
	// center map display on vehicle
	myCMap.updateVehicleLoc(m_state.Northing,m_state.Easting);
	cout << "Skynet Gui: MapDisplay Module Init Finished" << endl;
}

// This thread reads in the map deltas as they come in. The display is only
// notified that something's changed in the getStateThread function
void MapDisplaySN::getMapDeltaThread(void* pArg)
{
  int layerIndex = (int)pArg;
  cerr << "MapDisplaySN::getMapDeltasThread(): running with index " << layerIndex << endl;

  if(layerIndex==SNGUI_LAYER_ROAD_CNT)
    return;

  if(layerIndex==SNGUI_LAYER_ROAD) {
    getRoadThread();
    return;
  }

  sn_msg mapType = m_mapTypes[layerIndex];
  modulename mapSource = m_mapSources[layerIndex];

  int shift = 0;


  if(layerIndex == SNGUI_LAYER_FUSIONMAP_FINAL_COST || bShiftAllMaps) {
    shift=1;
  }

  int mapDeltaSocket = m_skynet.listen(mapType, mapSource);
  
  if(mapDeltaSocket < 0)
    cerr << "MapDisplaySN::getMapDeltasThread(): skynet listen returned error" << endl;
  
  while(true)
  {
		int deltasize;
		if(RecvMapdelta(mapDeltaSocket, m_pMapDelta[layerIndex], &deltasize))
    {
			unsigned long long timestamp;
			DGCgettime(timestamp);
			DGClockMutex(&m_loggingMutex);

			string **ppThismsg = &m_ppLogs[incrementLogMessageIndex()];
			if(*ppThismsg != NULL)
			{
				delete *ppThismsg;
			}
			*ppThismsg = new string;
			(*ppThismsg)->append((char*)&timestamp, sizeof(timestamp));
			(*ppThismsg)->append((char*)&mapType, sizeof(mapType));
			(*ppThismsg)->append((char*)&deltasize, sizeof(deltasize));
			(*ppThismsg)->append((char*)m_pMapDelta[layerIndex], deltasize);
			DGCunlockMutex(&m_loggingMutex);
      
      DGClockMutex(&m_mapdeltaMutex);
      myCMap.applyDelta<double>(layerNum[layerIndex], m_pMapDelta[layerIndex], deltasize, shift);
      DGCunlockMutex(&m_mapdeltaMutex);
    }
  }   
}

//Run from getMapDeltaThread to get the road data (which isn't sent as mapdelta
void MapDisplaySN::getRoadThread()
{
  int road_socket = m_skynet.listen(SNroad2map, SNroadfinding);

  bool first_road=true;
  int cur_road=0;
  Road road[2];
  CRoadPainter painter;

  printf("getRoadThread started\n");
  
  static int bytes=0;
  static int cnt=0;

  while(true) {
    //Read road data over skynet, make sure we always get the last transfer
    //so that we don't build up the buffert
    int received;
    do {
      received = m_skynet.get_msg(road_socket, &road[cur_road], sizeof(Road), 0);
      if(received < road[cur_road].data_size()) {
	printf("Error receiving road data\n");
	continue;
      }

      bytes +=received;
      ++cnt;
      //printf("Received %i messages, %i bytes, %i road points\n", cnt, bytes, road[cur_road].length);
    } while(m_skynet.is_msg(road_socket));

    if(received < road[cur_road].data_size())   //Invalid road data, get next package
      continue;

    //Paint the trajectory into the map
    DGClockMutex(&m_mapdeltaMutex);
    //myCMap.clearLayer(layerNum[SNGUI_LAYER_ROAD]);

    if(!first_road) {
      painter.paint(road[cur_road], road[1-cur_road], &myCMap, layerNum[SNGUI_LAYER_ROAD],layerNum[SNGUI_LAYER_ROAD_CNT]);
    } else {
      painter.paint(road[cur_road], &myCMap, layerNum[SNGUI_LAYER_ROAD], layerNum[SNGUI_LAYER_ROAD_CNT]);
    }
    
    first_road=false;
    cur_road = 1-cur_road;

    DGCunlockMutex(&m_mapdeltaMutex);
  }
}

// This thread reads in paths as they come in. The display is only notified that
// something's changed in the getStateThread function
void MapDisplaySN::getTrajThread(void* pArg)
{
  int pathIndex = (int)pArg;
  sn_msg pathType = m_pathTypes[pathIndex];
  modulename pathSource = m_pathSources[pathIndex];

  int trajSocket = m_skynet.listen(pathType, pathSource);

  while(true)
  {
		WaitForTrajData(trajSocket);

		unsigned long long timestamp;
		DGCgettime(timestamp);

		DGClockMutex(&m_loggingMutex);

		string **ppThismsg = &m_ppLogs[incrementLogMessageIndex()];
		if(*ppThismsg != NULL)
		{
			delete *ppThismsg;
		}
		*ppThismsg = new string;
		(*ppThismsg)->append((char*)&timestamp, sizeof(timestamp));
		(*ppThismsg)->append((char*)&pathType, sizeof(pathType));

		RecvTraj(trajSocket, &paths[pathIndex].points, &paths[pathIndex].pathMutex, *ppThismsg);

		DGCunlockMutex(&m_loggingMutex);

#ifdef RTP_VPROFILE_DISPLAY
		if(pathIndex == PATHIDX_PLANNED)
		{
			if(m_rtpPipe.good())
			{
				m_rtpPipe << "0" << endl;
				paths[pathIndex].points.printSpeedProfile(m_rtpPipe);
			}
		}
#endif


  }
}

void MapDisplaySN::getStateThread(void)
{
  while(true)
  {
    /* This sets m_state and m_actuatorState */
    UpdateState();
		UpdateActuatorState();

		unsigned long long timestamp;
		sn_msg skynettype;
		int msgsize;
		DGCgettime(timestamp);

		DGClockMutex(&m_loggingMutex);

		string **ppThismsg = &m_ppLogs[incrementLogMessageIndex()];
		if(*ppThismsg != NULL)
		{
			delete *ppThismsg;
		}
		*ppThismsg = new string;

		skynettype = SNstate;
		msgsize = sizeof(m_state);
		(*ppThismsg)->append((char*)&timestamp, sizeof(timestamp));
		(*ppThismsg)->append((char*)&skynettype, sizeof(skynettype));
		(*ppThismsg)->append((char*)&msgsize, sizeof(msgsize));
		(*ppThismsg)->append((char*)&m_state, msgsize);

		skynettype = SNactuatorstate;
		msgsize = sizeof(m_actuatorState);
		(*ppThismsg)->append((char*)&timestamp, sizeof(timestamp));
		(*ppThismsg)->append((char*)&skynettype, sizeof(skynettype));
		(*ppThismsg)->append((char*)&msgsize, sizeof(msgsize));
		(*ppThismsg)->append((char*)&m_actuatorState, msgsize);

		DGCunlockMutex(&m_loggingMutex);

    // Add our current position to the path history   
    // If our history has too many points in it, we need to shift them and add
    int num_points;
    if( (num_points = paths[PATHIDX_HISTORY].points.getNumPoints()) > TRAJ_MAX_LEN-2 )
    {
      paths[PATHIDX_HISTORY].points.shiftNoDiffs(1);
    }
    paths[PATHIDX_HISTORY].points.inputNoDiffs(m_state.Northing_rear(), m_state.Easting_rear());

		// Add our current raw GPS to the path history
		// If our history has too many points in it, we need to shift them and add
    if( (num_points = paths[PATHIDX_GPS].points.getNumPoints()) > TRAJ_MAX_LEN-2 )
    {
      paths[PATHIDX_GPS].points.shiftNoDiffs(1);
    }
    paths[PATHIDX_GPS].points.inputNoDiffs(m_state.GPS_Northing_rear(), m_state.GPS_Easting_rear());

    m_mapConfig->notify_update();      
    usleep(THREAD_INTERVAL);
  }
}

void MapDisplaySN::writelogs(ostream* pStream)
{
  printf("[%s:%d] writelogs has been called.\n", __FILE__, __LINE__);

  // if we've no data to write, return
  if(m_logMessageIndex == 0 && !m_bLogBufferFull)
  {
    printf("[%s:%d] No data to write.\n", __FILE__, __LINE__);
    return;
  }
  
  DGClockMutex(&m_loggingMutex);

	int oldMessageIndex = m_logMessageIndex;

	m_logMessageIndex = (m_bLogBufferFull ? m_logMessageIndex : 0);

	static int logidx = 0;

	ofstream outfile;
	if(pStream == NULL)
	{
    printf("[%s:%d] Opening output file.\n", __FILE__, __LINE__);

		ostringstream filenamstream;
		filenamstream << "/tmp/guilog" << logidx++;

		outfile.open(filenamstream.str().c_str());
    if(!outfile) 
    {
      printf("[%s:%d] Couldn't open outfile.\n", __FILE__, __LINE__);
    }
		pStream = &outfile;
	}
		
	do
	{
		string *pThismsg = m_ppLogs[incrementLogMessageIndex()];

		if(pThismsg != NULL)
    {
			pStream->write(pThismsg->c_str(), pThismsg->length());
    }
    else
    {
      printf("[%s:%d] pThismsg == NULL (not writing)\n", __FILE__, __LINE__);
    }
	} while(m_logMessageIndex != oldMessageIndex);

	m_logMessageIndex = oldMessageIndex;
  printf("[%s:%d] Message index = %d\n", __FILE__, __LINE__, oldMessageIndex);

	DGCunlockMutex(&m_loggingMutex);
}

// This thread reads in paths as they come in. The display is only notified that
// something's changed in the getStateThread function
void MapDisplaySN::getRDDFThread()
{
  while(true)
  {
		WaitForRDDF();
		RDDFVector rddf_vector = m_newRddf.getTargetPoints();
		//m_newRddf.print();
		m_mapConfig->set_rddf(rddf_vector);
		m_mapConfig->notify_update();
  }
}
