//SUPERCON STRATEGY TITLE: End of RDDF - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyEndOfMission.hh"
#include "StrategyHelpers.hh"

void CStrategyEndOfMission::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE (this MUST remain here)
  currentStageName = endofmission_stage_names_asString( endofmission_stage_names(stgItr.nextStage()) );

  switch( stgItr.nextStage() ) {
    
  case s_endofmission::estop_pause: 
    //end of RDDF has been reached -> estop pause
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "EndOfMission - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_endofmission::wait_for_new_mission: 
    //wait in this stage until either the rule saying that we are
    //at the end of the mission is NOT reached (rule changes) OR we
    //time-out waiting for DARPA to pause us, note that this is a
    //VERY long timeout
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */      
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      skipStageOps = trans_EstopPausedNOTsuperCon( (*m_pdiag), strprintf( "EndOfMission: %s", currentStageName.c_str() ) );
 /* 2007 - at this point EstopPausedNOTsuperCon should perhaps incorporate
  * a 'Between Missions' contingency that puts us into an Estop condition */


      if( !(m_pdiag->NewMission) ) {
	//need to define NewMission
        skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "StrategyEndOfMission: No new Mission yet -> will poll" );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	transitionStrategy( StrategyNominal, "StrategyEndOfMission: Starting up new Mission -> NOMINAL" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "EndOfMission - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;    

    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyEndOfMission::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyEndOfMission::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: EndOfMission - default stage reached!");
    }
  }
}


void CStrategyEndOfMission::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "EndOfMission - Exiting");
}

void CStrategyEndOfMission::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset(); //reset the stage iterator
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "EndOfMission - Entering");
}


