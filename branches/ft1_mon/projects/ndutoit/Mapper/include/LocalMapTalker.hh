#ifndef LOCALMAPTALKER_HH_
#define LOCALMAPTALKER_HH_

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "SkynetContainer.h"
#include "DGCutils"
#include "Map.hh"
#include "RoadObject.hh"
#include "LogicalRoadObject.hh"

#define LOCALMAP_MAX_BUFFER_SIZE 50000

class CLocalMapTalker : virtual public CSkynetContainer {
  	//TODO What do I do with these?  Private/public?
	pthread_mutex_t m_localMapDataBufferMutex;
	char* m_pLocalMapDataBuffer;

public:
	//TODO needs: MODmapping, int sn_key
	CLocalMapTalker(int);
	// Call before sending a message
	void getAndSetSocket();
	~CLocalMapTalker();

	bool SendLocalMap(Mapper::Map* localMap);
	bool RecvLocalMap(int localMapSocket, Mapper::Map* localMap, int* pSize);
private:
	int localMapSocket;  
};

#endif //  LOCALMAPTALKER_HH
