#ifndef ENVIRONMENT_HH
#define ENVIRONMENT_HH

#include <vector>
#include "State.hh"
#include "../astateSim/astateSim.hh"
using namespace std;

// TODO: Make Environment a virtual class, with subclasses like 
// SimulationEnvironment, InteractiveEnvironment, etc, instead of just 
// overriding certain functions (this creates ambiguity)

/*! \brief Stores information about the local area around our vehicle. 
 * 
 * This area consists of the road between two intersections
 * (if driving on a road), or a zone such as a parking lot. Most of the 
 * information here comes from sensing, so the environment won't be very
 * detailed outside of the range of our sensors (~100 meters?), and info
 * outside of this range is limited to what comes from the RNDF (stop signs,
 * etc).
 * 
 * The information stored in the Environment includes:
 * - Our vehicle's State
 * - Other vehicles in the local area (their States)
 * - A list of waypoints (ie future goals) given by the global/strategic
 *   planner. The RNDF is used to determine characteristics of these
 *   waypoints.
 * - Characteristics of the currents segment (segment ID, number of lanes,
 *   how many "right" lanes, etc)
 */
class Environment {

  ///////////
  // Setup //
  ///////////

public:

  /*! Constructor. Creates an empty environment. Information
   * is filled in by the user using setup(). 
   * \param rndf_filename The name of the rndf file to use.
   */
  Environment(char* rndf_filename);

  /*! Destructor. */
  virtual ~Environment();

  /*! Sets up the environment. Each class derived from Environment
   * has its own particular setup.
   */
  virtual void setup() = 0;
  
protected:

  /*! The RNDF file */
  char* rndf_filename;

  /*! Loads an RNDF file and uses it to initialize the appropriate variables
   * in the Environment. 
   *
   * Note: This needs to be run AFTER we initialize a state for our vehicle.
   */
  virtual void loadRNDF() = 0;

  /*! Gets the lane boundaries and stores them. */
  virtual void setLaneBoundaries() = 0;

  /*! Indicates the orientation of the lane numbering. If it is true,
   * then lanes to the left of Alice are numbered lower than lanes to
   * the right. If it is false, then lanes to the left are numbered
   * higher than lanes to the right.
   *
   * See also firstRightLane and DARPA's RNDF definition.
   */
  bool leftToRight;
  /*! Determines which lanes we are allowed to drive in on a segment */
  void calcRightLanes();

private:
  

  //////////////
  // Vehicles //
  //////////////

protected:

  /*! Our state */
  State* ourState;

  /*! The other vehicles in the environment. They are each identified
   * by a vehicle ID (stored in their State). */
  vector<State> vehicles;
 

  //////////////////////
  // Road Information //
  //////////////////////

public:
  
  /*! Prints the current status to stdout. This very simple graphical
   * representation of the Environment can be used for testing purposes
   * until it is replaced by a working simulator that can output something
   * much more detailed.
   */
  void printRoad();

protected:

  /*! The RNDF we store. I think eventually there will be an RNDF stored
   * somewhere higher up, and it is only created once and accessed by
   * whoever needs it... but for now there's an RNDF for each environment.
   */
  RNDF rndf;

  /*! Segment ID for the current road */
  Segment* currSegment;

  /*! An array of vectors. Each vector holds NEcoords that represent the
   * boundaries of a lane. */
  vector<NEcoord>* laneBoundaries;

  /*! \brief Names the first lane that we're allowed to drive in. 
   *
   * If leftToRight is true, then all lanes >= to this are "right" lanes, 
   * and all < are "left" lanes. 
   * 
   * If leftToRight is false, then all lanes <= this are "right lanes,
   * and all > this are "left" lanes.
   */
  Lane* firstRightLane;
  /*! \return The Lane immediately to the left of the current lane. If no
   * such lane exists, returns NULL. */
  Lane* getLeftLane();
  /*! \return The Lane immediately to the right of the current lane. If no
   * such lane exists, returns NULL */
  Lane* getRightLane();

  /*! The next checkpoint, stop line, or exit waypoint we need to reach. 
   * This is given by the global planner, and the RNDF can be used to
   * determine characteristics about the waypoint.
   */
  Waypoint* currGoal;

  /*! Determine if a Segment is one-way or two-way. */
  bool isOneWay(Segment* segment);
  /*! Determines if a Waypoint is the first or last waypoint in a lane */
  bool isTerminalWaypoint(Waypoint* waypoint);
  /*! Determines if a given lane is a "right" (ie, legal) lane. This will,
   * of course, depend on our vehicle's lane.
   * \param lane The lane to be checked
   * \return 
   * - true if lane is a "right" lane
   * - false if lane is a "left" lane
   */
  bool isLegalLane(Lane* lane);
  /*! Determines if a given lane is to the left or right of the current lane.
   * \param lane The lane to be checked.
   * \return
   * - true if given lane is EQUAL TO or TO THE RIGHT of the current lane
   * - false if given lane is TO THE LEFT of the current lane. 
   */
  bool toTheRight(Lane* lane);
  /*! Determines if a given lane is to the left or right of the current lane.
   * \param laneID The lane ID of the lane to be checked.
   * \return
   * - true if given lane is EQUAL TO or TO THE RIGHT of the current lane
   * - false if given lane is TO THE LEFT of the current lane. 
   */
  bool toTheRight(int laneID);    


  //////////////
  // Updating //
  //////////////

public:

  /*! Updates the Environment. */
  virtual void update() = 0;

protected:

  /*! Updates our vehicle's state. */
  virtual void updateState() = 0;

  /*! Updates the state of other vehicles */
  virtual void updateVehicles() = 0;

  /*! Adds a vehicle to the environment. */
  virtual void addVehicle() = 0;

  /*! Removes a vehicle from the Environment. 
   * \param ID The ID of the vehicle to be removed. */
  void removeVehicle(int ID);

  /*! Uses a vehicle's State and the lane boundaries in the current segment
   * to determine which lane the vehicle is probably in. 
   *
   * Stores this lane in the State.
   * \return 1 If it was determined that the vehicle is on a road
   * \return 0 If no lane was found for the vehicle and it was deleted.
   */
  int calcLane(State& state);
  
  /*! Gets the goal from the route planner and stores it. */
  virtual void setGoal() = 0;

  ////////////////
  // Evaluating //
  ////////////////

public:

  /*! Determines if we've reached the current goal. If we have, then it
   * gets the next goal.
   */
  bool atGoal();

  /*! \return An array of size NUM_STATES that has a double for each state:
   *   - Zero if state is impossible, 
   *   - 1 if highly desirable, 
   *   - or somewhere in between according to how desirable the action.
   *
   * See globalConstants.hh for information on the possible states.
   */ 
  double* getPossibleStates();

protected:

  /*! Decreases a number by one-third of its original value (Helper fn) */
  double decOneThird(double num) { return (num - 0.333 * num); }
  /*! Decreases a number by one-half of its original value (Helper fn) */
  double decOneHalf(double num) { return (num - 0.5 * num); }

  /*! The possible states, determined by evaluate() */
  double* states;

  /*! \brief Determines the possible future states. 
   * Use getPossibleStates() to retrieve these states after evaluation.
   */
  void evaluate();

};


///////////////////////
// ALICE ENVIRONMENT //
///////////////////////

class AliceEnvironment : virtual public Environment, public CStateClient {

  ///////////
  // Setup //
  ///////////

public:

  AliceEnvironment(char* rndf_filename, int skynetKey) :
    Environment(rndf_filename), CSkynetContainer(SNastate, skynetKey) { }

  ~AliceEnvironment() { }

  /*! Sets up the environment with input from other modules. */
  void setup();

protected:

  void loadRNDF();

  void setLaneBoundaries();

  //////////////
  // Updating //
  //////////////

public:

  /*! Uses input from other modules to update the Environment */
  void update();

protected:

  /*! Uses input from other modules to update our vehicle's state */
  void updateState();

  void updateVehicles();

  void addVehicle();

  void setGoal();

};

/////////////////////////////
// INTERACTIVE ENVIRONMENT //
/////////////////////////////

class InteractiveEnvironment : virtual public Environment
{

  ///////////
  // Setup //
  ///////////

public:

  /*! Default constructor. Creates an empty environment. Information
   * is filled in by the user using setup(). 
   * \param rndf_filename The name of the RNDF file to use.
   */
  InteractiveEnvironment(char* rndf_filename) : 
    Environment(rndf_filename) { }

  /*! Destructor. */
  ~InteractiveEnvironment() { }

  /*! Sets up the environment with input from user. For testing purposes,
   * and also until the other necessary modules are completed and interfaced
   * to tplanner. 
   */
  void setup();

protected:

  /*! Loads an RNDF file and uses it, in addition to user input, to 
   * initialize the appropriate variables in the Environment.
   *
   * Note: This needs to be run AFTER we initialize a state for our vehicle.
   */
  void loadRNDF();

  void setLaneBoundaries();

  //////////////
  // Updating //
  //////////////

public:

  /*! Uses input from user to update the Environment */
  void update();

protected:

  /*! Uses input from user to update our vehicle's state */
  void updateState();

  void updateVehicles();

  void addVehicle();

  void setGoal();

};


////////////////////////////
// SIMULATION ENVIRONMENT //
////////////////////////////

class SimulationEnvironment : virtual public Environment, public CStateSimClient
{

  ///////////
  // Setup //
  ///////////

public:

  /*! Default constructor. Creates an empty environment. Information
   * is filled in by the user using setup(). 
   * \param skynetKey Uses this key to set up the skynet interface. 
   * \param rndf_filename The name of the RNDF file to use
   * \param sim_filename The name of the astateSim file to use
   * \param lanes_filename The name of the lane boundaries filename to use
   */
  SimulationEnvironment(char* rndf_filename, 
			int skynetKey,
			char* sim_filename, 
			char* lanes_filename);

  /*! Destructor. */
  ~SimulationEnvironment();

  /*! Sets up the environment in preparation for simulation testing. */
  void setup();

protected:

  void loadRNDF();

  void setLaneBoundaries();

private:

  /*! The simulation filename */
  char* sim_filename;
  /*! The lanes filename */
  char* lanes_filename;


  //////////////
  // Updating //
  //////////////

public:

  /*! Uses simulation files to update our vehicle's state. */
  void update();

protected:

  /*! Updates our vehicle's state with input from astateSim. */
  void updateState();

  void updateVehicles();

  void addVehicle();

  void setGoal();

};

#endif
