#include "LocalMapTalker.hh"

using namespace std;
using namespace Mapper;

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;

	CLocalMapTalker mapTalker = CLocalMapTalker(key);
	
	vector<Segment> segments;
	Map map(segments);
	cout << "about to listen...";
	mapTalker.getAndSetSocket();
	cout << " listening!" << endl;

	int size;
	cout << "about to receive a map...";
	mapTalker.RecvLocalMap(&map, &size);
	cout << " received a map!" << endl;
	
}
