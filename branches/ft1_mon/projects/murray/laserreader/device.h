/*********************************************************************************/
/* SICK LASER DRIVER V0.9                                                        */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs (See README for details) */
/*********************************************************************************/
#ifndef DEVICE
#define DEVICE
#include<pthread.h>

class CDevice {
private:
  pthread_mutex_t dataAccessMutex;
  pthread_mutex_t setupMutex;

  bool firstdata;

public:
  CDevice();
  virtual ~CDevice();

  void LockNGetData( unsigned char * );
  void LockNGetData();
  void LockNPutData( unsigned char * );
  int LockNShutdown();
  virtual int Setup() = 0;

  /* these should really be private but the run thread needs access to them */
  virtual int Shutdown() = 0;
  virtual void GetData() = 0;
  virtual void GetData( unsigned char * ) = 0;
  virtual void PutData( unsigned char * ) = 0;
};

#endif
