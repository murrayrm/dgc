 
/* 
 * Desc: Sink for ladar blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include "sensnet.h"
#include "sensnet_ladar.h"

#if USE_LADAR_LOG
#include "ladar_log.h"
#endif


// Display data for ladar blobs
class LadarSink
{
  public:

  // Constructor
  LadarSink();

  public:

  // Update with current sensnet data
  int sensnetUpdate(sensnet_t *sensnet);

  public:

#if USE_LADAR_LOG
  // Open log file
  int logOpen(const char *filename);

  // Close log file
  int logClose();
  
  // Update with next frame from log
  int logUpdate();

  public:
  
  // Log file
  ladar_log_t *log;
#endif
  
  public:

  // Generate range point cloud (local frame)
  void predrawPointCloud();

  // Predraw camera footprint (sensor frame)
  void predrawFootprint();
    
  public:

  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId;

  // Blob data buffer
  sensnet_ladar_blob_t blob;
  
  // Are we enabled?
  bool enable;

  // Set flag if the data needs predrawing
  bool dirty;

  // Rainbow color scale
  uint8_t rainbow[0x10000][3];
  
  // How many clouds to we render?
  int maxClouds;
  
  // GL Drawing lists
  int cloudList, footList;
};
