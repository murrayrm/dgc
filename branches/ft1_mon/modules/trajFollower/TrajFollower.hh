#ifndef TRAJFOLLOWER_HH
#define TRAJFOLLOWER_HH

#include <pthread.h>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <iomanip>
#include <math.h>

#include "trajFollowerTabSpecs.hh"
#include "sn_msg.hh"
#include "adrive_skynet_interface_types.h"
#include "DGCutils"
#include "StateClient.h"
#include "TrajTalker.h"
#include "CTimberClient.hh"
#include "trajFollowerTabSpecs.hh"
#include "TabClient.h"
#include "rddf.hh"
#include "pathLib.hh"
#include "CPath.hh"
#include "TrajTalker.h"
#include "AliceConstants.h"
#include "trajF_speedCap_cmd.hh"
#include "trajF_status_struct.hh"
#include "interface_superCon_trajF.hh"
#include "SuperConClient.hh"
#include "traj.h"
#include "PID_Controller.hh"


/***** DEFINE STATEMENTS TO CHANGE FUNCTION OF CONTROLLER ******/

// Whether to limit the steering angle based on maximum lateral acceleration
#define USE_DFE

//whether to use linear scheduling. default is inversely proportional to speed.
//#define USE_LINEAR_SCHEDULING



/***** DEFINE STATEMENTS TO CHANGE WHICH MODULE TO LISTEN TO *****/
//#define USE_SNRDDFTRAJ
//#define USE_SNREACTIVETRAJ
//#define USE_SNROADFINDINGTRAJ
//#define USE_SNSTATICTRAJ


/************ Defines relative to the reversing functionality **********/
#define MAXHISTORYSIZE 100 // max number of points in the history buffer
#define HISTORYSPACING 2 //spacing between two points in the reverse history. in meters.
#define RSPEED 1.5
#define RWIDTH 5.0

//estop defines required (used to check whether Alice is paused, which
//is a requirement in order to change trajFollower's mode (Forwards/Reverse)
#define ESTP_PAUSE 1
#define ESTP_RUN 2

using namespace std;
using namespace sc_interface;

class TrajFollower : public CStateClient, public CTrajTalker, public CTimberClient, public CModuleTabClient, public CSuperConClient
{
  /*! The steer command to send to adrive (in SI units). */
  double m_steer_cmd;

  /*! The accel command to send to adrive (in SI units). */
  double m_accel_cmd;

  /* the curr yerr from pid_controller */
  double m_out_yerr;

  unsigned long long grr_timestamp;

  /*! The trajectory controller. */
  CPID_Controller* m_pPIDcontroller;
  CPID_Controller* reversecontroller;

  /*! how many times the while loop in the Active function has run.  */
  int m_nActiveCount;

  /*! The number of trajectories we have received. */
  int m_trajCount;

  /*! The current trajectory to follow. */
  CTraj* m_pTraj;

  /** status flag to be sent to gui */
  int status_flag;

  /*! Time that module started. */
  unsigned long long m_timeBegin; 

  /*! min and max speed caps */
  double m_speedCapMin, m_speedCapMax;

  /*! Speed - 2D [m/s], (for sparrowHawk display)  */
  double m_Speed2;

  bool at_end;
  //this will be set to what UpdateState returns, allowing me to check whether to send commands
  bool valid_state, old_valid_state;
  
  /*! Angular state variables in DEGREES (converted from radians) */
  double m_PitchRateDeg, m_RollRateDeg, m_YawRateDeg, m_PitchDeg, m_RollDeg, m_YawDeg;

  /*! vector of position history. */
  RDDFData history[MAXHISTORYSIZE];
  int sizeIndex; //this tells how large the active buffer is
  int pointIndex; //this tells what index in the buffer is the most recent

  CTraj reversetraj;
  double distancelefttoreverse;

  double reverseDist; //how far we're supposed to reverse
  bool reverse; // true if we are reversing
  bool free_reverse; //true if we are reversing along a dynamic traj
  bool no_history; //true if we are supposed to be reversing and have no history left!

  double historyN[MAXHISTORYSIZE];
  double historyE[MAXHISTORYSIZE];
  double historyDiff[MAXHISTORYSIZE];

  double historyDist; //total length of history in our buffer
  double distSinceLastAdd;
  double lastN, lastE;

  // vars related to timber
  bool logs_enabled;
  string logs_location;
  string lindzey_logs_location;
  bool logs_newDir;

  /*! Logs to output the plans and the state */
  ofstream m_outputPlans, m_outputTest;

	/*! The skynet socket for the communication with drive */
	int m_drivesocket;

	/*! The mutex to protect the traj we're following, and the speedCap calculations */
	pthread_mutex_t	m_trajMutex, m_speedCapMutex, m_reverseMutex, m_historyMutex;

	/*! The skynet socket for receiving trajectories */
	int m_trajSocket;
 
  /*!Used for guiTab.*/
  StrajfollowerTabInput  m_input;
  StrajfollowerTabOutput m_output;

public:
  TrajFollower(int sn_key, char* pTrajfile, char* pLindzey);
  ~TrajFollower();

  void Active();

  void Comm();

  void superconComm();

  void speedCapComm();

  void statusComm();

  void setupLogFiles();

  void Init_SparrowDisplayTable();
  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

  double DFE(double proposed_cmd, double speed);

  double access_SmallestMAXspeedC() { 
    double out;
      DGClockMutex(&m_speedCapMutex);
      out = m_speedCapMax;
      DGCunlockMutex(&m_speedCapMutex);
    return out; 
  }

  double access_LargestMINspeedC() {
    double out;
      DGClockMutex(&m_speedCapMutex);
      out = m_speedCapMin;
      DGCunlockMutex(&m_speedCapMutex);
    return out; 
  }

  double access_PitchRateDeg() { return m_PitchRateDeg; }
  double access_RollRateDeg() { return m_RollRateDeg; }
  double access_YawRateDeg() { return m_YawRateDeg; }
  double access_PitchDeg() { return m_PitchDeg; }
  double access_RollDeg() { return m_RollDeg; }
  double access_YawDeg() { return m_YawDeg; }


private:

  CTraj ReverseTrajGen(double distance);
  void UpdateReverse();
  void Resume();
  
};

#endif
