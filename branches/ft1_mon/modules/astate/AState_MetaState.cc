#include "AState.hh"

//MetaState is solely responsible for Keeping track of which sensors are enabled / active and restarting them
//If necessary.  This should happen on a level almost entirely outside of AState_Update.cc

using namespace sc_interface;

void AState::metaStateThread() {

  _heartbeat.gps = false;
  _heartbeat.gpsValid = false;
  _heartbeat.nov = false;
  _heartbeat.novValid = false;
  _heartbeat.imu = false;
  _heartbeat.obd = false;

  int goodNowTimer = 0;

  double northDiff = 0.0;
  double eastDiff = 0.0;
  double altDiff = 0.0;
  double lastNorth = 0.0;
  double lastEast = 0.0;
  double lastAlt = 0.0;
  double magVel = 0.0;
  double magDiff = 0.0;

  timedOutCount = 0;

  if (_astateOpts.useSC == 1) {
	  printf("[%s:%d] Using superCon\n", __FILE__, __LINE__);
  }
  else { 
	  printf("[%s:%d] Not using superCon\n", __FILE__, __LINE__);
  }

  // give time for the heartbeats to start
  sleep(1);

  while(!quitPressed) {

    DGClockMutex(&m_MetaStateMutex);        
    DGClockMutex(&m_HeartbeatMutex);
    if(_metaState.gpsEnabled) {

      if (_heartbeat.gps) {
	_metaState.gpsActive = 1;
      } else {
	_metaState.gpsActive = 0;
      }

      if(_heartbeat.gpsValid) {
	_metaState.gpsPvtValid = 1;
      } else {
	_metaState.gpsPvtValid = 0;
      }

    }

    if(_metaState.novEnabled) {

      if (_heartbeat.nov) {
	_metaState.novActive = 1;
      } else {
	_metaState.novActive = 0;
      }

      if(_heartbeat.novValid) {
	_metaState.novPvtValid = 1;
      } else {
	_metaState.novPvtValid = 0;
      }

    }

    if(_metaState.imuEnabled) {

      if(_heartbeat.imu) {
	_metaState.imuActive = 1;
      } else {
	_metaState.imuActive = 0;
      }

    }

    if(_metaState.obdEnabled) {

      if(_heartbeat.obd) {
	_metaState.obdActive = 1;
      } else {
	_metaState.obdActive = 0;
      }

    }

    _heartbeat.gps = false;
    _heartbeat.gpsValid = false;
    _heartbeat.nov = false;
    _heartbeat.novValid = false;
    _heartbeat.imu = false;
    _heartbeat.obd = false;

    DGCunlockMutex(&m_HeartbeatMutex);
    DGCunlockMutex(&m_MetaStateMutex);
    

    
    // Mode setting.
    if(stateMode != PREINIT && stateMode != INIT) {

      if (_metaState.imuActive == 0 && imuDead == 1) {
	IMU_close();
	imuInit();
	if (_metaState.imuEnabled == 1) {
	  DGCstartMemberFunctionThread(this, &AState::imuThread);
	}
      }

      if (_metaState.imuActive == 1) {
	stateMode = NAV;
      } else {
	stateMode = FALLBACK;
      }
      
      if (_metaState.gpsActive == 0 && gpsDead == 1) {
	gps_close();
	gpsInit();
	if (_metaState.gpsEnabled == 1) {
	  DGCstartMemberFunctionThread(this, &AState::gpsThread);
	}
      }

      if (_metaState.novActive == 0 && novDead == 1) {
	nov_close();
	novInit();
	if (_metaState.novEnabled == 1) {
	  DGCstartMemberFunctionThread(this, &AState::novThread);
	}
      }


      // Code for handling state jumps

      DGClockMutex(&m_jumpMutex);
      if (stateJump == JUMP_REQUESTED && timedOutCount > 5) {
	if (_astateOpts.useReplay == 0) {
	  scMessage((int)need_to_stop_GPS_reacquired);	
	} else {
	  stateJump = JUMP_OK;
	}
      }

      timedOutCount++;

      //Don't defer jump just b/c we lost GPS
      /*
      if (_metaState.gpsPvtValid == 0 && _metaState.novPvtValid == 0 && 
	  (stateJump == JUMP_OK || stateJump == JUMP_REQUESTED)) {
	stateJump = JUMP_NONE;
	scMessage((int)move_without_clear);
      }
      */

      if (stateJump == JUMP_DONE && goodNowTimer > 2) {

	stateJump = JUMP_NONE;
	goodNowTimer = 0;

	northDiff = _vehicleState.Northing - jumpNorth;
	eastDiff = _vehicleState.Easting - jumpEast;
	altDiff = _vehicleState.Altitude - jumpAlt;

	magDiff = sqrt(pow(northDiff, 2.0) + pow(eastDiff, 2.0) + pow(altDiff, 2.0));

	if (magDiff < .2) {
	  scMessage((int)move_without_clear, magDiff);
	} else {
	  scMessage((int)move_with_clear, magDiff);
	}
      } else if (stateJump == JUMP_DONE) {
	goodNowTimer++;
      }


      if (stateJump == JUMP_OK) {
	northDiff = _vehicleState.Northing - lastNorth;
	eastDiff = _vehicleState.Easting - lastEast;
	altDiff = _vehicleState.Altitude - lastAlt;

	magDiff = sqrt(pow(northDiff, 2.0) + pow(eastDiff, 2.0) + pow(altDiff, 2.0));

	magVel = sqrt(pow(_vehicleState.Vel_N, 2.0) + pow(_vehicleState.Vel_E, 2.0));
	
	if (magDiff < .05 && magVel < .05) {
	  stateJump = JUMP_DONE;
	}
      }

      lastNorth = _vehicleState.Northing;
      lastEast = _vehicleState.Easting;
      lastAlt = _vehicleState.Altitude;

      DGCunlockMutex(&m_jumpMutex);
      
    }
    sleep(1);
  }
}

void AState::allowJump() {
    DGClockMutex(&m_jumpMutex);    
    stateJump = JUMP_OK;

    jumpNorth = _vehicleState.Northing;
    jumpEast = _vehicleState.Easting;
    jumpAlt = _vehicleState.Altitude;
    DGCunlockMutex(&m_jumpMutex);
}
void AState::getJumpMessage() {
  
  superconAstateCmd jumpCommand;

  while (!quitPressed) {

    if (m_skynet.get_msg(superconSock, &jumpCommand, sizeof(superconAstateCmd), 0) != sizeof(superconAstateCmd)) {
      cerr << "Didn't receive right size superconAStateCmd" << endl;
    }
    DGClockMutex(&m_jumpMutex);    
    if (jumpCommand.commandType == ok_to_add_gps && stateJump == JUMP_REQUESTED) {

      stateJump = JUMP_OK;

      jumpNorth = _vehicleState.Northing;
      jumpEast = _vehicleState.Easting;
      jumpAlt = _vehicleState.Altitude;

    } else if (jumpCommand.commandType == timed_out_waiting_for_astate && stateJump == JUMP_OK) {
      timedOutCount = 0;
      stateJump = JUMP_NONE;
    }
    DGCunlockMutex(&m_jumpMutex);
  }
}
