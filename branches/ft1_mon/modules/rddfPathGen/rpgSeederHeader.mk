RPGSEEDERHDR_PATH = $(DGC)/modules/rddfPathGen

RPGSEEDERHDR_DEPEND = \
        $(RPGSEEDERHDR_PATH)/RddfPathGen.hh \
        $(RPGSEEDERHDR_PATH)/pathLib.hh \
        $(RPGSEEDERHDR_PATH)/vectorOps.hh \
        $(RPGSEEDERHDR_PATH)/trajDFE.hh \
        $(RPGSEEDERHDR_PATH)/CPath.hh
