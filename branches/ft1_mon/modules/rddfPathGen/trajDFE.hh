#ifndef TRAJDFE_HH
#define TRAJDFE_HH
/**
 * @brief
 * trajDFE
 * Created 02-28-2005 Ben Pickett
 *
 * trajDFE is Dynamic Feasibility code that modifies accelerations and velocities in traj files
 * to make them easier to follow.  The primary function TrajDFE operates on a path struct passed
 * from RddfPathGen or other code using the pathLib definition of the pathstruct data structure.
 * trajDFE will smooth the velocities and apply necessary accelerations (limited by the constants
 * defined in AliceConstants.h) so the velocity varies continuously and within the abilities of
 * the vehicle.
 * 
 * trajDFE depends on AliceConstants.h for vehicle constants that define the performance
 * capabilities.
 *
 * trajDFE depends on pathLib.hh for class definitions.
 *
 * Usage:
 * @param  dense_path  Smooth dense path (pathstruct type defined in pathLib.hh)
 *
 * @code
 * void TrajDFE(pathstruct& dense_path); 
 * @endcode
 *
 * 
*/

using namespace std;
//#include "RddfPathGen.hh"
#include "pathLib.hh"
//#include "vectorOps.hh"

/* RDDF_MAXACCEL, RDDF_MAXDECEL, RDDF_MAXLATERAL, and 
 * RDDF_VEHWIDTH_EFF defined in: */
#include "AliceConstants.h"

/** @brief Ensures velocity varies continuously over dense traj
 * and assigns corresponding longitudinal accelerations */
void TrajDFE(pathstruct& dense_path); 


/** @brief the following are helper functions for TrajDFE */

//! lower velocity to make dynamically feasible (lateral_accel < max_lateral_accel)
void lower_velocity(pathstruct& path, unsigned int current_point);

//! smooth velocities over path and assign corresponding tangential accelerations
void recurse_accel(pathstruct& path, unsigned int current_point);
void recurse_decel(pathstruct& path, unsigned int current_point);

/** @brief some functions that make my life easier up above */
//! set lateral accel at one point in the path and adjust velocity accordingly
void set_lat_accel(pathstruct& path, unsigned int current_point, double desired_radial_accel);
//! project vector arr2 onto vector arr1 and storing result in arr1 - note: both must be of the same dimension (length)
void vec_project(double arr1[], double arr2[], unsigned int length);
//! copy vector arr2 to vector arr2
void vec_copy(double arr1[], double arr2[], unsigned int length);
//! vector inner product (dot product) 
double vec_dot(double arr1[], double arr2[], unsigned int length);
//! magnitude of vector arr1 
double vec_mag(double arr1[], unsigned int length);

#endif
