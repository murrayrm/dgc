//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_Random
//----------------------------------------------------------------------------

#ifndef UD_RANDOM_DECS

//----------------------------------------------------------------------------

#define UD_RANDOM_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "UD_Linux_Time.hh"
#include "UD_Error.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE          1
#endif

#ifndef FALSE
#define FALSE         0
#endif

#define SQUARE(x)     ((x) * (x))

//-------------------------------------------------
// functions
//-------------------------------------------------

double gaussian_UD_Random(double, double, double);
double UD_round(double);
void initialize_UD_Random();
double normal_sample_UD_Random(double, double);
double uniform_UD_Random();
double normal_UD_Random();
double ranged_uniform_UD_Random(double, double);
int ranged_uniform_int_UD_Random(int, int);
int probability_UD_Random(double);
int coin_flip_UD_Random();

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif    
