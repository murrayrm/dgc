//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_VP_Tracker
//----------------------------------------------------------------------------

#ifndef UD_VP_TRACKER_DECS

//----------------------------------------------------------------------------

#define UD_VP_TRACKER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#include "opencv_utils.hh"

// my stuff 

#include "UD_VanishingPoint.hh"
#include "UD_ParticleFilter.hh"
#include "UD_DominantOrientations.hh"
#include "UD_ImageSource.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

// camera calibration data; in degrees

#define DEFAULT_HFOV        45.0
#define PIX2RAD(x, w, hfov) ((hfov)*(x)/(w))     // can also use y, h, vfov

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_VP_Tracker : public UD_ParticleFilter
{
  
 public:

  // variables

  int w, h;                                            ///< dimensions of source image

  UD_VanishingPoint *vp;                               ///< vanishing point object associated with road tracker

  int show_particles;                                  ///< draw particle filter representations?
  UD_Vector *initstate, *dstate;                       ///< bookkeeping for regular initial grid of particle states

  int num_iteration;                                   ///< how many tracker iterations we've run on current image
  int end_iteration;                                   ///< maximum number of iterations to run - 1
  int do_reset;                                        ///< reset tracker parameters before next image is loaded?

  // functions

  UD_VP_Tracker(UD_VanishingPoint *, int, UD_Vector *, UD_Vector *, UD_Vector *);

  void run();
  int iterate();
  void pf_reset();
  double myround(double);

  virtual void draw(int, int, int) = 0;
  virtual void write(FILE *) = 0;

  virtual float candidate_vp_x(UD_Vector *) = 0;
  virtual float candidate_vp_y(UD_Vector *) = 0; 
  virtual float vp_x(UD_Vector *) = 0;
  virtual float vp_y(UD_Vector *) = 0;

  virtual float candidate_vp_x() = 0;
  virtual float candidate_vp_y() = 0;
  virtual float vp_x() = 0;
  virtual float vp_y() = 0;

  void process_command_line_flags(int, char **);

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
