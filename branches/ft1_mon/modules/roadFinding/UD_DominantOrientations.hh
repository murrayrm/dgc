//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_DominantOrientations
//----------------------------------------------------------------------------

#ifndef UD_DOMINANT_DECS

//----------------------------------------------------------------------------

#define UD_DOMINANT_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "fftw3.h"

#include "opencv_utils.hh"

// my stuff 

#include "UD_Error.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef MAXSTRLEN
#define MAXSTRLEN  256
#endif

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#ifndef PI
#define PI      3.14159265358979323846
#endif

#ifndef PI_2
#define PI_2    1.57079632679489661923
#endif

// row major
#define MATRC(mat, r, c)     (mat->x[(c) + mat->cols * (r)])

//-------------------------------------------------
// struct/class
//-------------------------------------------------

// 2-D of doubles

typedef struct 
{
  int rows, cols;            // currently used rows, cols
  double *x;
  double buffsize;           // rows * cols * sizeof(double)
  char *name;

} UD_Matrix;

//-------------------------------------------------

class UD_DominantOrientations
{
  
 public:

  // variables

  int show;                                            ///< draw output of dominant orientation calculations?

  int w, h;                                            ///< dimensions of image source

  IplImage        *gabor_max_response_index;           ///< indices of orientation winners
  IplImage        *gabor_max_response_intensity;       ///< scaled indices for grayscale display
  IplImage        *gabor_max_response_value;           ///< strengths of winners
  
  int              gabor_num_orientations;             ///< how many discrete orientations to search over for dominant one ("n" hereafter)
  float            gabor_start_orientation;            ///< leftmost discrete orientation to start searching at (in degrees)
  float            gabor_end_orientation;              ///< rightmost discrete orientation to stop searching at (in degrees)
  float            gabor_delta_orientation;            ///< angle between successive discrete orientations to search over (in degrees).  
                                                       ///< equal to (end - start) / n  

  float            gabor_wavelength;                   ///< wavelength of gabor kernel to be convolved with image.
                                                       ///< value of 4 => 12 x 12 kernel, value of 8 => 25 x 25 kernel, 16 => 50 x 50, etc.
  int              gabor_size;                         ///< sidelength of standard-size kernel
  
  IplImage        *image_gray_char;                    ///< source image in IPL_DEPTH_8U, 1 format (intermediate step to float conversion)
  IplImage        *image_gray_float;                   ///< source image in IPL_DEPTH_32F, 1 format (what FFT takes as input)
  
  float         ***gabor_bank;                         ///< n x 2 (for even, odd phases) array of Gabor kernels in standard sizes 
                                                       ///< (see note above for gabor_wavelength)
  float         ***gabor_bank_fft_float;               ///< n x 2 array of Gabor kernels shifted and padded to source image size
  IplImage      ***conv_image;                         ///< n x 2 array of images resulting from source image 
                                                       ///< being convolved with Gabor kernels (in spatial domain)
  IplImage       **gabor_response;                     ///< n x 1 array of images after combining columns of conv_image
                                                       ///< via conv_image_odd^2 + conv_image_even^2  
  IplImage        *gabor_new_max_mask;                 ///< scratch image used to compute index of maximum response kernel at each pixel

  // single-precision FFT variables 
  
  fftwf_complex   *image_fft_float_out;                ///< source image in frequency domain, after forward FFT
  float           *image_fft_float_out_sum;            ///< sum of real and imaginary components of float_out (same dimensions as complex 
                                                       ///< image); used to compute complex product with each Gabor kernel's FFT
  fftwf_plan       image_fft_float_plan;               ///< "plan" (see http://www.fftw.org) for transforming source image -> fft_float_out
  
  fftwf_complex ***gabor_fft_float_out;                ///< n x 2 array of Gabor kernels after forward FFT 
                                                       ///< (each the same dimensions as source image in frequency domain)
  float         ***gabor_fft_float_out_sum;            ///< n x 2 array of sums of real and imaginary components of Gabor kernels after FFT
  
  fftwf_complex ***product_inv_fft_float_in;           ///< n x 2 array of products of source image & kernels in FFT domain
  fftwf_plan     **product_inv_fft_float_plan;         ///< array of plans for taking convolved images back to spatial domain
  
  // functions

  UD_DominantOrientations(int, int, int, float, float, float);

  void compute(IplImage *);

  // fft_float could be derived from this class (leading way for hypothetical fft_double or GPU versions)

  void make_gabor_bank_fft_float();
  float *make_gabor_kernel_fft_float(float, float, float **, int *);
  IplImage *make_fft_float_IplImage(int, int);
  void initialize_gabor_fft_float();
  void run_gabor_fft_float(IplImage *);

  void find_gabor_max_responses();
  void filter_out_weak_responses();

  void process_command_line_flags(int, char **);

};

//-------------------------------------------------
// functions
//-------------------------------------------------

UD_Matrix *make_UD_Matrix(char *, int, int);
UD_Matrix *make_all_UD_Matrix(char *, int, int, double);
void set_all_UD_Matrix(double, UD_Matrix *);
void free_UD_Matrix(UD_Matrix *);
UD_Matrix *make_gabor_kernel_UD_Matrix(double, double, double, int);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
