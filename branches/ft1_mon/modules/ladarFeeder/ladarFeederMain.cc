#include <stdlib.h>
#include <stdio.h>

#include "ladarFeeder.hh"
#include <DGCutils>

#ifdef MACOSX
/* Install libgnugetopt for getopt_long_only support in Mac OS X */
#include <gnugetopt/getopt.h>
#else
#include <getopt.h>
#endif


enum {
  OPT_NONE, 
  OPT_LOG,
  OPT_NAME,
  OPT_MAXSCANS,
  OPT_VERBOSE,
  OPT_HELP,
  OPT_LADAR,
  OPT_FILE,
  OPT_SNKEY,
  OPT_COM,
  OPT_NOCOM,
  OPT_NOSTATE,
  OPT_DELAY,
  OPT_RATE,
  OPT_THRESH,
  OPT_TIMBER,
  OPT_DELTARATE,
  OPT_CALIBRATE,
  OPT_ESTOP,
  OPT_QUIET,
  OPT_SAVEMAPS,
 
  NUM_OPTS
};


using namespace std;

void printLadarHelp();  

int main(int argc, char** argv) {
  //Initialize options to be passed to the LadarFeeder object
  LadarFeederOptions ladarOpts;
  sprintf(ladarOpts.sourceFilenamePrefix, "None");
  sprintf(ladarOpts.logFilenamePrefix, "");
  ladarOpts.optCom = 1;
  ladarOpts.optState = 1;
  ladarOpts.optLadar = LadarFeeder::LADAR_UNKNOWN;
  ladarOpts.loggingOpts.logRaw = 1;
  ladarOpts.loggingOpts.logRanges = 0;
  ladarOpts.loggingOpts.logState = 1;
  ladarOpts.loggingOpts.logRelative = 0;
  ladarOpts.loggingOpts.logUTM = 0;
  ladarOpts.logTime = 0;
  ladarOpts.logMaps = 0;
  ladarOpts.optMaxScans = -1;
  ladarOpts.optSource = ladarSource::SOURCE_LADAR;
  ladarOpts.optLog = 0;
  ladarOpts.optVerbose = 0;
  ladarOpts.optDelay = -1;
  ladarOpts.optRate = -1;
  ladarOpts.optSNKey = -1;
  ladarOpts.optPause = 0;
  ladarOpts.optThresh = 0.5;
  ladarOpts.optMapDisp = 0;
  ladarOpts.optTimber = 0;
  ladarOpts.optDeltaRate = 20.0;
  ladarOpts.optSaveMapsRate = 0.0;
  ladarOpts.optZeroAltitude = 0;
  ladarOpts.optReset = 0;
  ladarOpts.optSendElev = 1;
  ladarOpts.optSendNum = 1;
  ladarOpts.optSendStdDev = 1;
  ladarOpts.optSendPitch = 1;
  ladarOpts.optKF = 0;

  int optEStop = 1;

  ladarOpts.optCalibrate = 0;

  //Initialize local options
  int optSparrow = 1;
  int optWait = -1;

  int c;
  //int digit_optind = 0;


  /* The following structure is defined for the use of the getlong_only function
   * which is a GNU library function that parses arguments added at the command 
   * line when running ladarFeeder; the below written struct is an 'array' of 
   * structures actually, with each struct having 4 fields; the first field is 
   * the name of the potential argument passed; the second field can either be:
   * -- no_argument
   * -- required_argument
   * -- optional_arguments 
   * the third field is a flag and the fourth field is usually an integer value;  
   * when getlong_only is called, it will parse the command line arguments; for arguments  
   * that match against any of the first fields of the static struct array defined below,
   * one of two things will happen:
   * 
   * --if the third field of the found match is a 0, then the return value from 
   *   the getlong_only function will be the fourth field
   * 
   * --if the third field of the found match is not 0, then the value of the 
   *   variable pointed to by the third field will be set to the value of the
   *   integer in the fourth field and the return value will be 0
   * 
   * Ex. ./ladarFeeder --ladar ro 
   * in this case, ladar is the long argument and ro is the short argument 
   * required; since the third field of the associated struct is 0, then 
   * the return value from getlong_only will be the fourth field, namely OPT_LADAR
   * 
   * also, long arguments are preceded by "--"; arguments to long arguments can be 
   * represented as follows: --ladar=roof or --ladar roof    --JM
   */

  static struct option long_options[] = {
    //Options that don't require arguments:
    {"sparrow", no_argument, &optSparrow, 1},
    {"nosparrow", no_argument, &optSparrow, 0},
    {"disp", no_argument, &ladarOpts.optMapDisp, 1},
    {"help",      no_argument, 0, OPT_HELP},
    {"wait", no_argument, &optWait, 1},
    {"nowait", no_argument, &optWait, 0},
    {"communicate", no_argument, &ladarOpts.optCom, 1},
    {"nocommunicate", no_argument, &ladarOpts.optCom, 0},
    {"nostate", no_argument, &ladarOpts.optState, 0},
    {"pause", no_argument, &ladarOpts.optPause, 1},
    {"timber", no_argument, 0, OPT_TIMBER},
    {"zero", no_argument, &ladarOpts.optZeroAltitude, 1},
    {"noestop", no_argument, &optEStop, 0},
    {"reset", no_argument, &ladarOpts.optReset, 1},
    {"quiet", no_argument, 0, OPT_QUIET},
    {"kf", no_argument, &ladarOpts.optKF, 1},
    //Options that require arguments
    {"thresh", required_argument, 0, OPT_THRESH},
    {"ladar", required_argument, 0, OPT_LADAR},
    {"maxscans", required_argument, 0, OPT_MAXSCANS},
    {"snkey", required_argument, 0, OPT_SNKEY},
    {"skynetkey", required_argument, 0, OPT_SNKEY},
    {"log",    required_argument, 0, OPT_LOG},
    {"name", required_argument, 0, OPT_NAME}, 
    {"delay", required_argument, 0, OPT_DELAY},
    {"rate", required_argument, 0, OPT_RATE},
    {"file", required_argument, 0, OPT_FILE},
    {"deltarate", required_argument, 0, OPT_DELTARATE},
    {"calibrate", required_argument, 0, OPT_CALIBRATE},
    {"savemaps", required_argument, 0, OPT_SAVEMAPS},
    //Options that have optional arguments
    {"verboselevel", optional_argument, 0, OPT_VERBOSE},
    {"estop", optional_argument, 0, OPT_ESTOP},
    {0,0,0,0}
  };
  
  
  while (1) {
    //int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    
    //this getopt_long_only function makes use of the static strcut defined above --JM
    c = getopt_long_only(argc, argv, "", long_options, &option_index); 
    if(c == -1) break;
    switch(c) {
    case OPT_THRESH:
      	ladarOpts.optThresh = atof(optarg); //optarg defined in getopt.h; refers to the next argument parsed --JM
      	break;
    case OPT_QUIET:
   	ladarOpts.optSendNum = 0;
	ladarOpts.optSendStdDev = 0;
	ladarOpts.optSendElev = 0;
	ladarOpts.optSendPitch = 0;
	break;
    case OPT_VERBOSE:
      	if(optarg != NULL) {
		ladarOpts.optVerbose=atoi(optarg);
	} else {
		ladarOpts.optVerbose=1;
      	}
      	break;
    case OPT_ESTOP:
	if(optarg != NULL) {
		optEStop = atoi(optarg);
	} else {
		optEStop = 1;
	}
	break;
    case '?':
    case OPT_HELP:
      	printLadarHelp();
      	exit(1);
      break;
    case OPT_LADAR:
      if(strcmp(optarg, "bumper")==0 || strcmp(optarg, "b")==0) {
				ladarOpts.optLadar = LadarFeeder::LADAR_BUMPER;
				sprintf(ladarOpts.ladarString, "%s", "Bumper_SICK");
      } else if(strcmp(optarg, "bumpleft")==0 || strcmp(optarg, "blt")==0) {
				ladarOpts.optLadar = LadarFeeder::LADAR_BUMPLEFT;
				sprintf(ladarOpts.ladarString, "%s", "Bumpleft_SICK");
      } else if(strcmp(optarg, "bumpright")==0 || strcmp(optarg, "brt")==0) {
				ladarOpts.optLadar = LadarFeeder::LADAR_BUMPRIGHT;
				sprintf(ladarOpts.ladarString, "%s", "Bumpright_SICK");
      } else if(strcmp(optarg, "roofleft")==0 || strcmp(optarg, "rolt")==0) {
				ladarOpts.optLadar = LadarFeeder::LADAR_ROOFLEFT;
				sprintf(ladarOpts.ladarString, "%s", "Roofleft_SICK");
      } else if(strcmp(optarg, "roofright")==0 || strcmp(optarg, "rort")==0) {
				ladarOpts.optLadar = LadarFeeder::LADAR_ROOFRIGHT;
				sprintf(ladarOpts.ladarString, "%s", "Roofright_SICK");								
      } else if(strcmp(optarg, "Rear")==0 || strcmp(optarg, "rear")==0) {
				ladarOpts.optLadar = LadarFeeder::LADAR_REAR;
				sprintf(ladarOpts.ladarString, "%s", "Rear_SICK");				
      } else if(strcmp(optarg, "roof")==0 || strcmp(optarg, "ro")==0) {
				ladarOpts.optLadar = LadarFeeder::LADAR_ROOF;
				sprintf(ladarOpts.ladarString, "%s", "Roof_SICK");
      } else if(strcmp(optarg, "small")==0 || strcmp(optarg, "s")==0) {
				ladarOpts.optLadar = LadarFeeder::LADAR_SMALL;
				sprintf(ladarOpts.ladarString, "%s", "Small_SICK");
      } else if(strcmp(optarg, "riegl")==0 || strcmp(optarg, "ri")==0) {
				ladarOpts.optLadar = LadarFeeder::LADAR_RIEGL;
				sprintf(ladarOpts.ladarString, "%s", "Riegl");
      } else if(strcmp(optarg, "front")==0 || strcmp(optarg, "f")==0) {
				ladarOpts.optLadar = LadarFeeder::LADAR_FRONT;
				sprintf(ladarOpts.ladarString, "%s", "Front_SICK");
      } else {
				printf("No such LADAR as '%s' is known - please use bumper, bumpleft, bumpright, roof, roofleft, roofright, rear, small, front, or riegl\n", optarg);
				printLadarHelp();
				exit(1);
      }
      break;
    case OPT_MAXSCANS:
      ladarOpts.optMaxScans = atoi(optarg);
      break;
    case OPT_LOG:
      ladarOpts.optLog = 1;
      if(optarg) {
	if(strstr(optarg, "all")) {	
	  ladarOpts.loggingOpts.logRaw = 1;
	  ladarOpts.loggingOpts.logState = 1;
	  ladarOpts.loggingOpts.logRanges = 1;
	  ladarOpts.loggingOpts.logRelative = 1;
	  ladarOpts.loggingOpts.logUTM = 1;
	  ladarOpts.logTime = 1;
	  ladarOpts.logMaps = 1;
	  break;
  	} else if(strstr(optarg, "base")) {
	  ladarOpts.loggingOpts.logRaw = 1;
	  ladarOpts.loggingOpts.logState = 1;
	  ladarOpts.loggingOpts.logRanges = 0;
	  ladarOpts.loggingOpts.logRelative = 0;
	  ladarOpts.loggingOpts.logUTM = 0;
	  ladarOpts.logTime = 0;
	  ladarOpts.logMaps = 0;
	  break;
	} else if(strstr(optarg, "quickdebug") || strstr(optarg, "qd") || strstr(optarg, "debug")) {
	  ladarOpts.loggingOpts.logRaw = 0;
	  ladarOpts.loggingOpts.logState = 0;
	  ladarOpts.loggingOpts.logRanges = 1;
	  ladarOpts.loggingOpts.logRelative = 1;
	  ladarOpts.loggingOpts.logUTM = 0;
	  ladarOpts.logTime = 0;
	  ladarOpts.logMaps = 0;
	  break;
	} else if(strstr(optarg, "alldebug") || strstr(optarg, "ad")) {
	  ladarOpts.loggingOpts.logRaw = 0;
	  ladarOpts.loggingOpts.logState = 0;
	  ladarOpts.loggingOpts.logRanges = 1;
	  ladarOpts.loggingOpts.logRelative = 1;
	  ladarOpts.loggingOpts.logUTM = 1;
	  ladarOpts.logTime = 1;
	  ladarOpts.logMaps = 1;
	  break;
	} else {
	  if(strchr(optarg, 'w')) {
	    ladarOpts.loggingOpts.logRaw = 1;
	  } else {
	    ladarOpts.loggingOpts.logRaw = 0;
	  }
	  if(strchr(optarg, 's')) {
	    ladarOpts.loggingOpts.logState = 1;
	  } else {
	    ladarOpts.loggingOpts.logState = 0;
	  }
	  if(strchr(optarg, 'g')) {
	    ladarOpts.loggingOpts.logRanges = 1;
	  } else {
	    ladarOpts.loggingOpts.logRanges = 0;
	  }
	  if(strchr(optarg, 'r')) {
	    ladarOpts.loggingOpts.logRelative = 1;
	  } else {
	    ladarOpts.loggingOpts.logRelative = 0;
	  }
	  if(strchr(optarg, 'u')) {
	    ladarOpts.loggingOpts.logUTM = 1;
	  } else {
	    ladarOpts.loggingOpts.logUTM = 0;
	  }
	  if(strchr(optarg, 'm')) {
	    ladarOpts.logMaps = 1;
	  } else {
	    ladarOpts.logMaps = 0;
	  }
	  if(strchr(optarg, 't')) {
	    ladarOpts.logTime = 1;
	  } else {
	    ladarOpts.logTime = 0;
	  }
	}
      }
      break;
    case OPT_NAME:
      sprintf(ladarOpts.logFilenamePrefix, "%s", optarg);
      break;
    case OPT_DELAY:
      ladarOpts.optDelay = atof(optarg);
      break;
    case OPT_RATE:
      ladarOpts.optRate = atof(optarg);
      break;
    case OPT_TIMBER:
      ladarOpts.optSource = ladarSource::SOURCE_FILES;
      ladarOpts.optTimber = 1;
    case OPT_FILE:
      ladarOpts.optSource = ladarSource::SOURCE_FILES;
      sprintf(ladarOpts.logFilenamePrefix, "%s", optarg);
      break;
    case OPT_SNKEY:
      ladarOpts.optSNKey = atoi(optarg);
      break;
    case OPT_DELTARATE:
      ladarOpts.optDeltaRate = atof(optarg);
      break;
    case OPT_SAVEMAPS:
      ladarOpts.optSaveMapsRate = atof(optarg);
    case OPT_CALIBRATE:
      ladarOpts.optCalibrate = atoi(optarg);
      break;
    default:
      if(c!=0) {
	printf("Unknown option %d!\n", c);
	printLadarHelp();
	exit(1);
      }
    }
  }
  
  if(ladarOpts.optLadar==LadarFeeder::LADAR_UNKNOWN && ladarOpts.optSource==ladarSource::SOURCE_LADAR) {
    printf("No LADAR unit specified - please use the --ladar {roofleft, roofright, bumper, bumpleft, bumpright, rear} argument to choose a LADAR\n");
    exit(1);
  }


  char* ptrSkynetKey = getenv("SKYNET_KEY");

  if(ptrSkynetKey == NULL && ladarOpts.optSNKey==-1) {
    printf("You have not specified a skynet key as a command-line argument (using --snkey)\n");
    printf("or as an environment variable.  To set a skynet ket, try doing:\n");
    printf("$> export SKYNET_KEY=X\n");
    printf("where X is some number.  To check your skynet key, do:\n");
    printf("$> echo $SKYNET_KEY\n");
    return 0;
  } else {
    printf("Welcome to LadarFeeder!\n");
    if(ladarOpts.optSNKey==-1) {
      ladarOpts.optSNKey = atoi(ptrSkynetKey);
    }

		if(ladarOpts.optTimber)
			optEStop = 0;

    printf("Option Summary - General:\n");
    printf("SkynetKey      - %d\n", ladarOpts.optSNKey);
    printf("Sparrow        - %d\n", optSparrow);
    printf("Display Map    - %d\n", ladarOpts.optMapDisp);
    printf("Verbose Level  - %d\n", ladarOpts.optVerbose);
    printf("Communicate    - %d\n", ladarOpts.optCom);
		printf("EStop          - %d\n", optEStop);
		printf("Using KF       - %d\n", ladarOpts.optKF);
		if(ladarOpts.optSendNum == 0 && ladarOpts.optSendStdDev == 0 && ladarOpts.optSendElev == 0) {
			printf("Being Quiet    - 1\n");
		}
    if(ladarOpts.optDeltaRate == 0.0) {
      printf("DeltaRate      - Instant\n");
    } else {
      printf("DeltaRate      - %lf\n", ladarOpts.optDeltaRate);
    }
    if(ladarOpts.optSaveMapsRate == 0.0) {
      printf("SaveMapsRate   - Don't save\n");
    } else {
      printf("SaveMapsRate   - %lf\n", ladarOpts.optSaveMapsRate);
    }
    if(ladarOpts.optSource!=ladarSource::SOURCE_FILES) {
      printf("Using State    - %d\n", ladarOpts.optState);
      printf("Using Ladar    - %s\n", ladarOpts.ladarString);
    }
    if(ladarOpts.optDelay!=-1) {
      printf("Delay          - %lfsec\n", ladarOpts.optDelay);
    }
    if(ladarOpts.optRate!=-1) {
      printf("Rate           - %lfHz\n", ladarOpts.optRate);
    }
    if(ladarOpts.optZeroAltitude) {
      printf("Using 0 Alt.   - True\n");
    }
    if(ladarOpts.optCalibrate) {
      printf("Calibrating    - %d\n", ladarOpts.optCalibrate);
    }
    if(ladarOpts.optSource!=ladarSource::SOURCE_LADAR) {
      if(ladarOpts.optSource==ladarSource::SOURCE_FILES) printf("Using Files    - %s\n", ladarOpts.logFilenamePrefix);
    }
    if(ladarOpts.optMaxScans != -1) {
      printf("Maximum Scans  - %d\n", ladarOpts.optMaxScans);
    }
    if(ladarOpts.optLog) {
      printf("Option Summary - Logging:\n");
      if(strcmp(ladarOpts.logFilenamePrefix, "")==0) {
	printf("Log File Name  - Will Be Based On Timestamp\n");
      } else {
	printf("Log File Name  - %s\n", ladarOpts.logFilenamePrefix);
      }
      printf("Raw:    %d | Ranges:   %d\n", ladarOpts.loggingOpts.logRaw, ladarOpts.loggingOpts.logRanges);
      printf("State:  %d | Relative: %d\n", ladarOpts.loggingOpts.logState, ladarOpts.loggingOpts.logRelative);
      printf("Time:   %d | UTM:      %d\n", ladarOpts.logTime, ladarOpts.loggingOpts.logUTM);
      printf("Maps:   %d |             \n", ladarOpts.logMaps);
    }
    if(ladarOpts.logMaps) printf("WARNING!  Logging maps significantly slows down the program\n");
    if(ladarOpts.loggingOpts.logRaw && !ladarOpts.loggingOpts.logState)
      printf("WARNING!  Logging scans but not state means you might not be able to playback these logs!\n");
    if(ladarOpts.optVerbose>1 && optSparrow) printf("WARNING!  Using a verbose level greater than 1 may wreak havoc on the Sparrow display!\n");

    if(ladarOpts.optMapDisp) {
      printf("WARNING! Closing the mapDisplay may cause ladarFeeder to segfault!\n");
    }

    printf(" == HIT CTRL+C TO CANCEL ==\n");
    char temp;
    const int waitTime = 7;
    if(optWait==1) {
      for(int i=0; i<waitTime; i++) {
	printf("Program will start in %d seconds...\r", waitTime-i);
	fflush(stdout);
	sleep(1);
      }
    } else if(optWait==-1) {
      printf("Please review the options you have set.  Do you want to continue (y/n)? ");      
      temp = getchar();
      if(temp!='y' && temp!='Y') {
	printf("You have entered something other than 'y' - quitting...\n");
	return 0;
      }
    }

    LadarFeeder* ladarFeederObj;
    ladarFeederObj = new LadarFeeder(ladarOpts.optSNKey, ladarOpts);
    
    if(optSparrow) {
      DGCstartMemberFunctionThread(ladarFeederObj, &LadarFeeder::SparrowDisplayLoop);
      DGCstartMemberFunctionThread(ladarFeederObj, &LadarFeeder::UpdateSparrowVariablesLoop);
    }

    ladarFeederObj->_careAboutEstop = (bool)optEStop;
    DGCstartMemberFunctionThread(ladarFeederObj, &LadarFeeder::ReceiveDataThread_EStop);

    if(ladarOpts.optDeltaRate != 0.0) {
      DGCstartMemberFunctionThread(ladarFeederObj, &LadarFeeder::SendDataThread);
    }

    if(ladarOpts.optSaveMapsRate != 0.0) {
      DGCstartMemberFunctionThread(ladarFeederObj, &LadarFeeder::SaveMapsThread);
    }

    ladarFeederObj->ActiveLoop();
    
    printf("\n%s [%d]: Ladar feeder shutting down.  LADAR status message is '%s'\n",
	   __FILE__, __LINE__, ladarFeederObj->ladarObject.getErrorMessage());
  }  

  return 0;
}


void printLadarHelp() {
  printf("Usage: ladarFeeder [OPTION]\n");
  printf("Runs the DGC ladarFeeder software\n\n");

  printf("  --sparrow           Shows the sparrow display\n");
  printf("  --nosparrow         Does not show the sparrow display\n");
  printf("  --disp              Displays a local mapDisplay window showing the ladar data\n");
  printf("  --help              Prints this help message\n");
  printf("  --wait              Enables a wait before the program begins\n");
  printf("  --nowait            Disables a wait before the program begins\n");
  printf("  --communicate       Enables using Skynet to communicate (sends mapDeltas)\n");
  printf("  --nocommunicate     Disables using Skynet to communicate (does not send mapDeltas)\n");
  printf("  --nostate           Disables using state (forces state to be all zeros)\n"); 
  printf("  --ladar LADAR       Use the ladar denoted by LADAR (described below)\n");
  printf("  --maxscans NUM      Runs the code for NUM scans\n");
  printf("  --log LOG           Enables logging of varous data denoted by LOG (described below), must use --name\n");
  printf("  --name NAME         Specifies the base filename NAME to be used when logging data (default is temp)\n");
  printf("  --file FILE         Run off of logged images (using every frame) with the base filename FILE\n");
  printf("  --timber            Play logged data using timber\n");
  printf("  --verboselevel NUM  Not yet supported\n");
  printf("  --pause             Starts the ladarFeeder in paused mode\n");
  printf("  --zero              Ignore altitude from state - assume altitude is always zero\n");
	printf("  --quiet             Don't send any map deltas other than directly to fusionMapper\n");
	printf("  --savemaps NUM      Saves maps at the given rate (in Hz)\n");

  printf("\nIn --log LOG, LOG must be some combination of the following:\n");
  printf("w=raw, m=maps, s=state, t=timing data, g=range, r=relative, u=UTM\n");
  printf("Additionally the shortcuts all=wmstgru, base=ws, quickdebug=gr, alldebug=rgumt each work individually.\n");

  printf("\nIn --ladar LADAR, LADAR must be one of the following:\n");
  printf("\tBumper: bumper, b;\n\tBumperLeft: bumpleft, blt;\n\tBumperRight: bumpright, brt\n");
	printf("\tRoof: roof, ro;\n\tRoofLeft: roofleft, rolt;\n\tRoofRight: roofright, rort;\n");
	printf("\tFront: front, f;\n\tRiegl: riegl, ri;\n\tRear: rear\n");
}
