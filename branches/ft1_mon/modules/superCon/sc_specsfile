SUPERCON_FREQ 10.0
SPARROW_LOG_LEVEL 3

/*** DEFINES USED BY DIAGNOSTIC RULES ***/
WHEEL_SPIN_DETECTION_THRESHOLD_SPEED_MS 5
WHEEL_SPIN_WHEELS_FASTER_FACTOR 1.25

//Factor used to control the threshold used to determine whether sufficient force 
//has been applied to move Alice up whatever incline she is currently on, the 
//threshold is set to the force required to move Alice up a hill = (her current 
//incline * this factor), NOTE: this should be >1 (% factor) 
ENGINE_FORCE_GREATER_FACTOR 1.25
ASTATE_CONFIDENCE_POOR_THRESHOLD 0.4

//This is the slowest speed in M/S that the TrajFollower can maintain, speeds 
//below this are assumed to be zero in some form  */ 
SLOWEST_MAINTAINABLE_ALICE_SPEED 0.5
SLOWEST_MAINTAINABLE_WHEEL_SPEED 0.5

//In the case when OBD2 FAILS, this is accelCmd (hence -ve for brake, +ve for throttle (-1 -> +1)
//threshold that must be exceeded (in a +VE direction) before it is assumed that we have applied
//enough force to the wheels to move Alice's mass up a slope with angle = her current pitch
THROTTLE_THRESHOLD_FOR_ENGINE_FORCE_INCLINE_IF_OBD2_DEAD 0.5


/*** DEFINES USED IN STRATEGY CLASSES  ***/ 

/** Generic Strategy class **/
//Persist loop values are measured in SUPERCON EXECUTION CYCLES && are used 
//during action 'time-out' checking 
PERSIST_LOOP_ESTOP_THRESHOLD 100
PERSIST_LOOP_ASTATE_GPS_REINC_THRESHOLD 100
PERSIST_LOOP_GEARCHANGE 100
PERSIST_LOOP_TRAJFMODECHANGE 100
PERSIST_LOOP_TRAJF_REVERSE_FINISHED 100
PERSIST_LOOP_SLOWADVANCE_APPROACH 100
PERSIST_LOOP_UNSEENOBST_WAIT_FOR_STATIONARY 100

/** GPSreAcq Strategy **/ 
WHEELSPIN_PERSIST_TRUE_COUNT 10


/** LoneRanger Strategy **/ 
//Speed at which to attempt to drive through obstacles [METERS/SECOND]
LONE_RANGER_MIN_SPEED_MS 2.0


/** LturnReverse Strategy **/ 
LTURN_REVERSE_PERSIST_TRUE_COUNT 10

//Distance to reverse (along the path we came in on) for an L-turn
LTURN_REVERSE_DISTANCE_METERS 15


/** Nominal Strategy **/


/** SlowAdvance Strategy **/ 
//Speed [meters/second] below which the maps can reasonably be considered 
//to be 'high-quality' as sensor data gathered at slower speeds will be more 
//heavily weighted than sensor data gathered at higher speeds, and reduced 
//high frequency oscillations etc
HIGH_ACCURACY_MAPS_SPEED_MS 7.0


/** UnseenObstacle Strategy **/ 
UNSEENOBSTACLE_PERSIST_TRUE_COUNT 10

//This define should be ZERO if the center of the REAR-AXLE is being used, 
//and if this is NOT the case then this define should be the offset in METERS  
//(TOWARDS THE FRONT AXLE = +VE) from the center of the rear axle (assumes that 
//vehicle origin is along the center-line (line joining the centers of both axles)  
//of the vehicle) of the actual vehicle origin
OFFSET_OF_VEHICLE_ORIGIN_FROM_REAR_AXLE 0.0

//Number of points along the bumper for which map-delta updates are sent when an 
//unseen-obstacle has been detected (should be even)
NUM_POINTS_TO_EVALUATE_ALONG_BUMPER 42

//cost (speed) value assigned to a cell in the superCon map layer to denote a superCon 
//obstacle - in meters/second
SUPERCON_OBSTACLE_SPEED_MS 0.01


/** WheelSpin Strategy **/
WHEELSPIN_PERSIST_TRUE_COUNT 10

/** NOTsuperConEstopPause Strategy **/
ESTOP_PAUSED_NOT_SUPERCON_PERSIST_TRUE_COUNT 10

/** Multiple Strategies **/ 
PLN_NOT_NFP_PERSIST_TRUE_COUNT 10
