#include "TrajTalker.h"

class CTrajTalkerTestSend : public CTrajTalker
{
public:
	CTrajTalkerTestSend(int sn_key)
		: CSkynetContainer(SNtrajtalkertestsend, sn_key)
	{
		int i;
		CTraj traj;
		double NEout[6];
		traj.startDataInput();
		for(i=0; i<1000; i++)
		{
			NEout[0] = (double)i;
			NEout[1] = (double)i+1.0;
			NEout[2] = (double)i+2.0;
			NEout[3] = (double)i+3.0;
			NEout[4] = (double)i+4.0;
			NEout[5] = (double)i+5.0;
			traj.inputWithDiffs(NEout+0, NEout+3);
		}

		cout << "about to get_send_sock...";
		int trajSocket = m_skynet.get_send_sock(SNtraj);
		cout << " get_send_sock returned" << endl;
		while(true)
		{
			cout << "about to send a traj...";
			SendTraj(trajSocket, &traj);
			cout << " sent a traj!" << endl;
// 			usleep(10000);
		}
	}
};

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;

	CTrajTalkerTestSend test(key);
}
