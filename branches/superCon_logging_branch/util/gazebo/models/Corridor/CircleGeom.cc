/**
 * Author: Henrik Kjellander
 * $Id$
 */
#include "CircleGeom.hh"

//////////////////////////////////////////////////////////////////////////////
// Constructor
CircleGeom::CircleGeom( Body *body, const dSpaceID space, const double posE, const double posN, const double rad )
  : Geom( space )
{ 
  // must set something in order to have it render. Just a 0 sized box.
  this->SetGeom( body, dCreateBox( space, 0, 0, 0 ), NULL, false);
  
  // Save the attributes
  this->posE = posE;
  this->posN = posN;
  this->radius = rad;

  this->SetColor( GzColor(0, 0, 0, 1.0));
  return;
}

//////////////////////////////////////////////////////////////////////////////
// Destructor
CircleGeom::~CircleGeom()
{
  return;
}

//////////////////////////////////////////////////////////////////////////////
// Render the geom (GL)
void CircleGeom::Render(RenderOptions *opt)
{
  bool dirty;
  GLuint listId;
  RenderOptions listOpt;

  // Recover stored display list for this camera
  this->GetList(opt->cameraIndex, &listId, &listOpt);

  // See if the current display list is dirty
  dirty = this->dirty;
  dirty |= (listId == 0);
  dirty |= (opt->displayMaterials != listOpt.displayMaterials);
  dirty |= (opt->displayRays != listOpt.displayRays);

  this->dirty = false;

  // Generate the display list
  if (dirty)
  {
    if (listId == 0)
      listId = glGenLists(1);
    glNewList(listId, GL_COMPILE);

    // Set material properties
    if (opt->displayMaterials)
    {
      glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, this->colorAmbient);
      glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, this->colorDiffuse);
      glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, this->colorSpecular);
      glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, this->colorEmission);
    }

    draw();

    // Unset material properties (so other geoms dont get emissive)
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, GzColor(0, 0, 0, 1));

    // Store list options
    this->SetList(opt->cameraIndex, listId, *opt);
    glEndList();
  }

  // Call the display list
  if (listId)
    glCallList(listId);

  return;
}

void CircleGeom::draw()
{
  // Draw a circle with 32 steps
  int steps = 32;
  float x,y,z;
  z = 0.01; // let's draw 1 cm above the ground for better visibility

  glBegin(GL_LINE_STRIP); // Draw connected lines between every two vertices.
  for(int i=0; i < steps ;i++)
  {
    x = posE + radius * cos(2*M_PI*i/(double)steps);
    y = posN + radius * sin(2*M_PI*i/(double)steps);

    //printf("x,y = %f, %f\n", x, y);
    glVertex3f(x, y, z );

  }
  // Draw the last vertex to close the circle
  glVertex3f(posE + radius, posN, z);
  glEnd();
}
