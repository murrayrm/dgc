using namespace std;
#include <iostream>
#include <fstream>
#include "math.h"
#include "rddf.hh"


int main(void)
{
  ofstream outfile("worlds/worldgen.world");
  ifstream simfile("simInitState.dat");
  ifstream roofladarfile("LadarCal.ini.roof");
  ifstream bumperladarfile("LadarCal.ini.bumper");

  string simtype;
  int startwp;
  float n, e, x, y, yaw, temp;
  float rlx, rly, rlz, rlroll, rlpitch, rlyaw;
  float blx, bly, blz, blroll, blpitch, blyaw;
  string tmp;

  RDDF rddfWhole("rddf.dat");
  RDDFVector rddf;
  rddf = rddfWhole.getTargetPoints();
  RDDFData& firstwp = rddf.at(0);
  simfile >> simtype;

  if(simtype == "relative2")
    {
      simfile >> startwp >> x >> y >> yaw;
      RDDFData& w0 = rddf.at(startwp);
      RDDFData& w1 = rddf.at(startwp+1);
      
      // calculate the angle to the next waypoint
      yaw = 180.0/3.14159*(atan2( w1.Northing-w0.Northing,w1.Easting -w0.Easting)+ yaw);
      //cout << yaw<< endl;
      n = w0.Northing - firstwp.Northing - sqrt(x*x + y*y)*sin(atan2(w1.Northing-w0.Northing,w1.Easting -w0.Easting)+ atan2(y,x));
      e = w0.Easting - firstwp.Easting   + sqrt(x*x + y*y)*cos(atan2( w1.Northing-w0.Northing,w1.Easting -w0.Easting)+ atan2(y,x));
    }
  else if(simtype == "relative")
    {
      simfile >> startwp >> temp >> temp >> temp >> temp >> temp >> temp >> yaw;

      yaw= yaw*180.0/3.14159;

      RDDFData& w0 = rddf.at(startwp);
      n = w0.Northing - firstwp.Northing +n;
      e = w0.Easting - firstwp.Easting + e;
    }
  else if(simtype == "absolute")
    {
      simfile >> n >> temp >> temp >> e >> temp >> temp >> yaw;
      yaw= yaw*180.0/3.14159;
      n = n - firstwp.Northing;
      e = e - firstwp.Easting;
    }
  else 
    {
      cout << "Cannot parse simInitState.dat"<< endl;
      return 0;
    }

  roofladarfile >> tmp >> rlx >> tmp >> rly >>  tmp >> rlz >> tmp >> rlroll >> tmp >> rlpitch >> tmp >> rlyaw;
  bumperladarfile >> tmp >> blx >> tmp >> bly >>  tmp >> blz >> tmp >> blroll >> tmp >> blpitch >> tmp >> blyaw;

  rlz= -1.0*rlz;
  rlroll = 180/3.14159*rlroll;
  rlpitch = -180/3.14159*rlpitch;
  rlyaw = 180/3.14159*rlyaw;

  blz= -1*blz;
  blroll = 180/3.14159*blroll;
  blpitch = -180/3.14159*blpitch;
  blyaw = 180/3.14159*blyaw;

  outfile << "<!--This world file was generated automatically by worldgen -->"<< endl << endl
	  << "<gz:world "<< endl
	  << "  xmlns:gz='http://playerstage.sourceforge.net/gazebo/xmlschema/#gz'"<< endl
	  << "  xmlns:model='http://playerstage.sourceforge.net/gazebo/xmlschema/#model'"<< endl
	  << "  xmlns:sensor='http://playerstage.sourceforge.net/gazebo/xmlschema/#sensor'"<< endl
	  << "  xmlns:window='http://playerstage.sourceforge.net/gazebo/xmlschema/#window'"<< endl
  	  << "  xmlns:param='http://playerstage.sourceforge.net/gazebo/xmlschema/#params'"<< endl
  	  << "  xmlns:ui='http://playerstage.sourceforge.net/gazebo/xmlschema/#params'>"<< endl
	  << endl
	  << "  <param:Global>"<< endl
	  << "    <gravity>0.0 0.0 -9.8</gravity>"<< endl
	  << "    <stepTime>0.010</stepTime>"<< endl
	  << "    <speed>1.0</speed>"<< endl
	  << "  </param:Global>"<< endl
	  << endl 
	  << "  <model:LightSource>"<< endl
	  << "    <id>light1</id>"<< endl
	  << "    <xyz>0.0 10.0 100.0</xyz>"<< endl
	  << "  </model:LightSource>"<< endl
	  << endl
	  << "  <model:GroundPlane>"<< endl
	  << "    <id>ground1</id>"<< endl
	  << "    <color>1 1 1</color>"<< endl
	  << "    <textureSize> 20.0 20.0 </textureSize>"<< endl
	  << "    <textureFile>screenshot0002.jpg</textureFile>"<< endl
	  << "    <!-- Create waypoints -->"<< endl
	  << "    <model:Corridor>"<< endl
	  << "      <plugin>../models/Corridor/Corridor.so</plugin>"<< endl
	  << "    </model:Corridor>"<< endl
	  << "  </model:GroundPlane>"<< endl
	  << endl 
	  << "    <model:Alice>"<< endl
	  << "      <plugin>../models/Alice/Alice.so</plugin>"<< endl
	  << "      <id>robot1</id>"<< endl
	  << "      <xyz>" << e << " " << n << " .1</xyz>"<< endl
	  << "      <rpy>0 0 "<< yaw<<"</rpy>"<< endl
	  << "      <model:ObserverCam>"<< endl
	  << "        <id>userCam0</id>"<< endl
	  << "        <xyz>-10 -2 4</xyz>"<< endl
	  << "        <rpy>0 10 5</rpy>"<< endl
	  << "        <!-- <displayRays>true</displayRays> -->"<< endl
	  << "        <renderMethod>GLXP</renderMethod>"<< endl
	  << "        <imageSize>800 600</imageSize>"<< endl
	  << "      </model:ObserverCam>"<< endl
	  << endl
	  << "      <model:LightSource>"<< endl
	  << "        <id>light1</id>"<< endl
	  << "        <xyz>0.0 10.0 100.0</xyz>"<< endl
	  << "      </model:LightSource>"<< endl
	  << endl
	  << "      <model:SickLMS200>"<< endl
	  << "        <id>roofLadar</id>"<< endl
	  << "        <xyz>" <<rlx << " " <<rly << " " <<rlz << "</xyz>"<< endl
	  << "        <rpy>" <<rlroll << " " <<rlpitch << " " <<rlyaw <<"</rpy> <!-- 10 is a good pitch value (pos is down) -->"<< endl
	  << "        <rangeMin>0.20</rangeMin>"<< endl
	  << "        <rangeMax>81.91</rangeMax>"<< endl
	  << "        <rayCount>181</rayCount>"<< endl
	  << "        <rangeCount>181</rangeCount>"<< endl
	  << "      </model:SickLMS200>"<< endl
	  << endl
	  << "     <model:SickLMS200>"<< endl
	  << "        <id>bumperLadar</id>"<< endl
	  << "        <xyz>" <<blx << " " <<bly << " " <<blz << "</xyz>"<< endl
	  << "        <rpy>" <<blroll << " " <<blpitch << " " <<blyaw <<"</rpy> <!-- 10 is a good pitch value (pos is down) -->"<< endl
	  << "        <rangeMin>0.20</rangeMin>"<< endl
	  << "        <rangeMax>81.91</rangeMax>"<< endl
	  << "        <rayCount>181</rayCount>"<< endl
	  << "        <rangeCount>181</rangeCount>"<< endl
	  << "      </model:SickLMS200>"<< endl
	  << endl
	  << "     <model:SonyVID30>"<< endl
	  << "        <id>StereoLeft</id>"<< endl
	  << "        <xyz>0 -1 3.0</xyz>"<< endl
	  << "        <rpy>0 0 0</rpy>"<< endl
	  << "        <updateRate>1</updateRate> <!-- running at 1 Hz -->"<< endl
	  << "        <imageSize>640 480</imageSize>"<< endl
	  << "        <renderMethod>GLXP</renderMethod>"<< endl
	  << "      </model:SonyVID30>"<< endl
	  << endl
	  << "      <model:SonyVID30>"<< endl
	  << "        <id>StereoRight</id>"<< endl
	  << "        <xyz>0 1 3.0</xyz>"<< endl
	  << "        <rpy>0 0 0</rpy>"<< endl
	  << "        <updateRate>1</updateRate>"<< endl
	  << "        <imageSize>640 480</imageSize>"<< endl
	  << "        <renderMethod>GLXP</renderMethod>"<< endl
	  << "      </model:SonyVID30>"<< endl
	  << endl
	  << "      <model:GarminGPS>"<< endl
	  << "        <id>GPSunit</id>"<< endl
	  << "        <xyz>-1 0 3.0</xyz>"<< endl
	  << "        <rpy>0 0 0</rpy>"<< endl
	  << "        <origin>0 0 0</origin>"<< endl
	  << "      </model:GarminGPS>"<< endl
	  << "    </model:Alice>"<< endl
	  << endl  
	  << "    <model:SimpleSolid>"<< endl
	  << "      <xyz>15 0 1</xyz>"<< endl
	  << "      <rpy>0 0 0</rpy>"<< endl
	  << "      <shape>cylinder</shape>"<< endl
	  << "      <size>4 6</size>"<< endl
	  << "      <color>1 0 0</color>"<< endl
	  << "    </model:SimpleSolid>"<< endl
	  << endl
	  << "    <model:SimpleSolid>"<< endl
	  << "      <xyz>-20 4 1</xyz>"<< endl
	  << "      <rpy>0 0 0</rpy>"<< endl
	  << "      <shape>cylinder</shape>"<< endl
	  << "      <size>4 6</size>"<< endl
	  << "      <color>1 0 0</color>"<< endl
	  << "    </model:SimpleSolid>"<< endl
	  << endl
	  << "    <model:SimpleSolid>"<< endl
	  << "      <xyz>-20 -12 1</xyz>"<< endl
	  << "      <rpy>0 0 0</rpy>"<< endl
	  << "      <shape>cylinder</shape>"<< endl
	  << "      <size>4 6</size>"<< endl
	  << "      <color>1 0 0</color>"<< endl
	  << "    </model:SimpleSolid>"<< endl
	  << endl
	  << "    <model:SimpleSolid>"<< endl
	  << "      <xyz>-40 -7 1</xyz>"<< endl
	  << "      <rpy>0 0 0</rpy>"<< endl
	  << "      <shape>cylinder</shape>"<< endl
	  << "      <size>4 6</size>"<< endl
	  << "      <color>1 0 0</color>"<< endl
	  << "    </model:SimpleSolid>"<< endl
	  << endl
	  << "    <model:SimpleSolid>"<< endl
	  << "      <xyz>-60 -13.5 1</xyz>"<< endl
	  << "      <rpy>0 0 0</rpy>"<< endl
	  << "      <shape>cylinder</shape>"<< endl
	  << "      <size>4 6</size>"<< endl
	  << "      <color>1 0 0</color>"<< endl
	  << "    </model:SimpleSolid>"<< endl
	  << endl
	  << "    <model:SimpleSolid>"<< endl
	  << "      <xyz>-80 -18 1</xyz>"<< endl
	  << "      <rpy>0 0 0</rpy>"<< endl
	  << "      <shape>cylinder</shape>"<< endl
	  << "      <size>4 6</size>"<< endl
	  << "      <color>1 0 0</color>"<< endl
	  << "    </model:SimpleSolid>"<< endl
	  << endl
	  << "    <model:SimpleSolid>"<< endl
	  << "      <xyz>-120 -27 1</xyz>"<< endl
	  << "      <rpy>0 0 0</rpy>"<< endl
	  << "      <shape>cylinder</shape>"<< endl
	  << "      <size>4 6</size>"<< endl
	  << "      <color>1 0 0</color>"<< endl
	  << "    </model:SimpleSolid>"<< endl
	  << endl
	  << "  </gz:world>";
  outfile.close();
  simfile.close();
  roofladarfile.close();
  bumperladarfile.close();
  cout <<"Thank you for using worldgen"<< endl;
  system("wxgazebo worlds/worldgen.world");
  return 0;
}
