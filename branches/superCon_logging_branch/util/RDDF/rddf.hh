/**********************************************************************
 **********************************************************************
 * +FILE:        rddf.hh 
 *    
 * +DESCRIPTION: class to read the rddf file and return the waypoints
 *
 * +AUTHOR     : Thyago Consort 
 **********************************************************************
 *********************************************************************/

#ifndef __RDDF_HH__
#define __RDDF_HH__

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include "ggis.h"
#include "GlobalConstants.h" // for RDDF_FILE and unit conversions
#include "frames/coords.hh" // for NEcoord

#define RDDF_DELIMITER ','
#define BAD_BOB_FILE -1

// If the last waypoint in the rddf has zero speed (as all darpa rddfs do),
// move the last waypoint 20ft back and tack an additional waypoint onto the end with
// zero speed. This assures that the planner's use of pathgen doesn't get confused
#define TACK_ONE_ON   true
#define LAST_WAYPOINT_DIST  200.0 /**< used for tack_one_on */
#define LAST_WAYPOINT_SPEED 0.09

#define MOD_RDDF_NUM -1234  /**< This is the waypoint number which is
			     * added to the beginning of the RDDF by
			     * CStartup 
			     * THIS NUMBER MUST BE A NEGATIVE NUMBER!!!! */

#define UTM_ZONE    11   /**< same hardcoded hacks as in bob2rddf */
#define UTM_LETTER 'S'   /**< same hardcoded hacks as in bob2rddf */


using namespace std;


/** Waypoints are defined by the RDDF, and translated to a vector of RDDFData 
  structs when RDDF::loadFile() is called.  Even though the RDDF is defined in 
  terms of latitude, longitude, mph, and feed, SI units are used from this 
  point on in all of our code. */
typedef struct RDDFData
{
  /** number of the waypoint.  The corridor segment defined by waypoint i goes 
    from waypoint i to waypoint i+1.  DARPA RDDFs are indexed from waypoint 1, 
    which corresponds to index 0 in our code. */
  int number;

  /** distance along the trackline from the first waypoint.  Thus for waypoint
   * 0, this would be 0; for waypoint 1, this would be the length of the 0th
   * corridor; for waypoint 2, this would be the length of the 0th and 1st
   * corridor segments.  Added by jason 24 July 2005.*/
  double distFromStart;

  /** Northing (in meters, UTM zone 11 S) of waypoint <number>. */
  double Northing;
  /** Easting (in meters, UTM zone 11 S) of waypoint <number>. */
  double Easting;

  /** speed limit (in m/s) for the given corridor segment. */
  double maxSpeed;  
  /** boundary offset (in meters) of the corridor to the next waypoint. */  
  double offset;    
  /** radius (in meters) is actually identical to offset, and defines the 
    boundaries of a given corridor segment.  Each race vehicle must stay within 
    the union of all of the corridor segments. */
  double radius;

  /** Display function. */
  void display()
  {
    printf("N=%d D=%.3f North=%10.3f East=%10.3f VEL=%.3f Off= %.2f R=%.2f\n",
		    number,distFromStart,Northing,Easting,maxSpeed,offset,radius);
  }
 
  /** Accessor function to get an NEcoord format of a given waypoint. */
  NEcoord ne_coord() const
  {
    NEcoord ret;
    ret.N = Northing;
    ret.E = Easting;
    return ret;
  }
};

/** RDDFVector is a standard template vector of waypoint data defined by 
  RDDFData. */
typedef std::vector<RDDFData> RDDFVector;

class RDDF 
{
	bool m_bParsed;

protected:

  int numTargetPoints;
  RDDFVector targetPoints;

  double _additionalWidth;

  bool is_startup_prepended_rddf;

public:

  // Constructors
  RDDF(bool tack_one_on = TACK_ONE_ON);
  RDDF(char*pFileName, bool tack_one_on = TACK_ONE_ON);
  RDDF(const RDDF &other) 
  {
    numTargetPoints = other.getNumTargetPoints();
    targetPoints = other.getTargetPoints();
  }

  // Destructor
  ~RDDF();

	bool isParsed() { return m_bParsed; }

  RDDF &operator=(const RDDF &other);

  // Accessors

  /** Accessor function to get the number of waypoints. */
  int getNumTargetPoints() const { return numTargetPoints; }

  /** Accessor function to get a vector of all the waypoints.  RDDFVector is a 
   * standard template vector of RDDFData structs, which contain all essential 
   * data for a waypoint (number, Northing, Easting, maxSpeed, offset). */
  RDDFVector getTargetPoints() const { return targetPoints; }

  /** Return waypoint as NEcoord. */
  NEcoord getWaypoint(int waypointNum)
  {
    return targetPoints[waypointNum].ne_coord();
  };    

  /** Get the Northing value of a given waypoint. */
  double getWaypointNorthing(int waypointNum) 
  { 
    return targetPoints[waypointNum].Northing; 
  };

  /** Get the Easting value of a given waypoint. */ 
  double getWaypointEasting(int waypointNum) 
  { 
    return targetPoints[waypointNum].Easting;
  };

  double getWaypointSpeed(int waypointNum)
  {
    return targetPoints[waypointNum].maxSpeed;
  }

  double getWaypointDistFromStart(int waypointNum)
  {
    return targetPoints[waypointNum].distFromStart;
  }

  /** Load data from file (RDDF format file). */
  int loadFile(char* fileName, bool tack_one_on = TACK_ONE_ON);

  /** Add a new point to the end of the RDDF */
  void addDataPoint(RDDFData &data_point);

  /** prints rddf to screen or file */
  void print(ostream & out = cout);

  /** Get the waypoint number for the given location, i.e. the index of the
    corridor segment that contains the point.  Note that corridor segment i is
    the segment between waypoints i and i+1.  If the point is not within the
    corridor (the sum of corridor segments), then this function returns the
    index of the closest waypoint. The ..First variant returns the first valid
    segment and the ..Last variant returns the last valid segment */
  int getCurrentWaypointNumberFirst( NEcoord& pos, double bonus_ratio = 1, double bonus_const = 0 );
  int getCurrentWaypointNumberFirst( double Northing, double Easting, double bonus_ratio = 1, double bonus_const = 0 );
  int getCurrentWaypointNumberLast( NEcoord& pos, double bonus_ratio = 1, double bonus_const = 0 );
  int getCurrentWaypointNumberLast( double Northing, double Easting, double bonus_ratio = 1, double bonus_const = 0 );

  /** Get the angle (in radians) from the specified waypoint to the next, 
   * defined as positive clockwise from north.  Waypoints are indexed from 0.  
   * */
  double getTrackLineYaw( int i );
  
  /** Get the distance (in meters) from the specified waypoint to the next. */
  double getTrackLineDist( int i );

  /** Boolean function to return whether a given point is inside a specified 
   * corridor segment (the maximal rectangle or the circles of waypoint i's 
   * radius at either end).  Used by getCurrentWaypointNumber and other fine 
   * functions. */
  int isPointInCorridor(int waypointNum, double Northing, double Easting, double bonus_ratio = 1, double bonus_const = 0);

  //! Tells you if the point is inside the RDDF at all
  bool isPointInCorridor(double Northing, double Easting, double bonus_ratio = 1, double bonus_const = 0);
  bool isPointInCorridor(NEcoord pos, double bonus_ratio = 1, double bonus_const = 0);

  /** Gets the location along the trackline a given distance away from the 
   * specified point.  This function projects the position to the current 
   * waypoint's trackline, and then traces forward along the trackline from 
   * there by the specified distance.  It returns this coordinate.
   * Also, if thetaAtTarget isn't NULL, it'll get filled in with the
   * orientation the vehicle would have if it was at the returned XYcoord,
   * facing down the trackline. */
  NEcoord getPointAlongTrackLine( NEcoord& pos, double distance,
    double* thetaAtTarget = NULL, double* corRad = NULL );
  NEcoord getPointAlongTrackLine( double n, double e, double distance,
    double* thetaAtTarget = NULL, double* corRad = NULL );

  /** Save the file as the bob format (returns 0 if OK) */
  int createBobFile(char* fileName);

  /** Save the file as the rddf format (returns 0 if OK) */
  int createRDDFFile(char* fileName);
  
  /** Returns the lateral distance from the given (Alice coord) point
   * to the trackline segment that starts with the waypoint number
   * specified in the argument */
#warning getDistToTrackline(..) ASSUMES that the current WP# that is passed as an argument is BEHIND Alice along the trackline of the RDDF (herman does NOT guarantee this to be the case)!!  
  double getDistToTrackline( NEcoord pointCoords, int wayptNum );

  /** Same functionality as getDistToTrackline, except method takes
   * as an argument separate northing & easting values for the current
   * location of Alice */
  double getDistToTrackline( double northing, double easting, int wayptNum );
  
  /** Returns the lateral distance from the given point to the nearest 
   * trackline segment (using the getCurrentWaypointNumberFirst(..) - 
   * NOT herman */
  double getDistToTrackline(NEcoord pointCoords);

  /** Returns the ratio of (distance from given point to the nearest trackline
   * segment) over (lateral boundary offset, i.e. the width of the corridor
   * in that segment of the RDDF) */
  double getRatioToTrackline(NEcoord pointCoords);
  
  /** Ask Jeremy Gillula what the heck this function does (and put that 
    information here). */
  int getCorridorSegmentBoundingBoxUTM(int waypointNum, 
				       double winBottomLeftUTMNorthing, double winBottomLeftUTMEasting,
				       double winTopRightUTMNorthing, double winTopRightUTMEasting,
				       double& BottomLeftUTMNorthing, double& BottomLeftUTMEasting, 
				       double& TopRightUTMNorthing, double& TopRightUTMEasting);

  /** Ask Jeremy Gillula what the heck this function does (and put that 
    information here). */
  int getWaypointNumAheadDist(double UTMNorthing, double UTMEasting, 
                              double distance);

  /** Ask Jeremy Gillula what the heck this function does (and put that 
    information here). */
  int getWaypointNumBehindDist(double UTMNorthing, double UTMEasting, 
                               double distance);

  /** Given the line defined by a corridor segment (the one from wpt_num
   * to wpt_num + 1) and a point (position), finds the closest point
   * on that line and returns the distance to it from position.  Note that
   * this point may be in the middle of the segment, in which case the
   * distance will be the length of a perpendicular line from position
   * to the corridor segment, or the nearest point may be either end, in
   * which case the distance will be not be measured perpendicular to the
   * trackline.  If the last point in the corridor is given, the distance
   * will never be measured perpendicularly. 
   * 
   * @param wpt_num       the waypoint number at the beginning of the
   *                      segment in question
   * @param position      the point to measure from
   * @param distFromStart double in which the distance from the start
   *                      of the RDDF along the RDDF centerline to the
   *                      nearest point on the centerline will be stored
   * @param ratio         the distance from the beginning of the segment
   *                      to the closest point on the segment to `position'
   *                      divided by the length of the segment.
   *                      ratio will be -1 if the corridor length is 0! */
  double getDistToCorridorSegment(int wpt_num, const NEcoord & position,
				  double & distFromStart, double & ratio);

  double exhaustiveNearestSearch(int & wpt_num, const NEcoord & position,
				 double & distFromStart, double & ratio);

  //Hey Lars - ready for another really confusing set of functions? - JHG
  enum CORRIDOR_SIDE {
    SIDE_LEFT,
    SIDE_RIGHT
  };
  enum CORRIDOR_END {
    END_FRONT,
    END_BACK
  };
  NEcoord getCorridorPoint(int wayptNum, CORRIDOR_SIDE side, CORRIDOR_END end);

  //It's about time somebody made this thing
  double getOffset(int waypointNum) {return targetPoints[waypointNum].offset + _additionalWidth;};

	double getRadius(int waypointNum) {return targetPoints[waypointNum].radius + _additionalWidth;};

	//Function to widen our RDDF - adds additionalWidth to every LBO
	// - thus each RDDF corridor is 2*additionalWidth wider
	void setExtraWidth(double additionalWidth);

	double getExtraWidth() {return _additionalWidth;};

	//! Returns the waypoint with the largest width which contains the point pos
	int findWaypointWithLargestWidth(NEcoord pos);
	
};

#endif //__RDDF_HH__
