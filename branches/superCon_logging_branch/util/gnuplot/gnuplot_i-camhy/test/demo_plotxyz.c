#include <math.h>
#include <stdio.h>
#include "gnuplot_i.c"
/*#include "gnuplot_i.h"*/
#include "nrutil.c"

int main(int argc, char *argv[]) {
  gnuplot_ctrl    *h ; /*Gnuplot-Control*/
  double         a[100];
  double          b[100]; //static memory allocation
  double          c[100];
  double *        d;
  double *         e; //dynamic memory allocation
  double *         f;
  int             i ;

  d=dvector(1,100); /* Dynamic memory allocation*/
  e=dvector(1,100); /* Dynamic memory allocation*/
  f=dvector(1,100); /* Dynamic memory allocation*/

  h = gnuplot_init() ; /*Init the Gnuplot-Window*/
  gnuplot_set_xlabel(h,"x"); /*Set label of x-Axes*/
  gnuplot_set_ylabel(h,"y"); /*Set label of y-Axes*/
  gnuplot_set_zlabel(h,"z"); /*Set label of y-Axes*/
  gnuplot_set_title(h,"gnuplot_plot_xyz - Demonstration"); /*Set Title*/
  gnuplot_set_grid(h); /* Turns grid on*/
  gnuplot_setstyle(h,"lines"); 
  /* Set style
     possible values:  
       - lines
       - points
       - linespoints
       - impulses
       - dots
       - steps
       - errorbars
       - boxes
       - boxeserrorbars 
  */

  /*Range:
  gnuplot_set_xrange(h,1.0,10.0); 
  gnuplot_set_yrange(h,1.0,10.0);
  gnuplot_set_zrange(h,1.0,10.0);
  */

  for (i=1 ; i<=100 ; i++) {
    d[i] = (double) cos(i);
    e[i] = (double)(i*i) ;
    f[i]= (double) sin(i);
  }
  for (i=0 ; i<100; i++) {
    a[i] = (double) sin(i);
    b[i] = (double)(i*i*0.5);
    c[i]= (double) sin(i);
  }

  /* Plotting of the first array: Start index 0, end index 49*/
  gnuplot_plot_xyz(h,a,b,c,100,"3D - Plot");
  /* Plot the dynamic array: Start index 1, end index 50*/
  gnuplot_plotrange_xyz(h, d,e,f, 1, 100, "3D - Plot nrutil") ;

  printf("press ENTER to continue\n");
  while (getchar()!='\n') {} /* Wait for keypress */

  gnuplot_close(h) ; /*Close the gnuplot-window*/
  free_dvector(d,1,50);
  return 1;
}
