#!/bin/zsh

computers=( 192.168.0.51 192.168.0.52 192.168.0.53 192.168.0.54 192.168.0.55 192.168.0.56 192.168.0.57 )

count="0"
        
while true; do 

  count=`expr $count + 1`

  for i in ${computers}; do
    date >> logs/${i}.ntp
    ntpq -p $i >> logs/${i}.ntp
  done

  sleep 3
  echo $count

done
