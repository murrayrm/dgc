/**
 * networking.hh
 * Include files defines for writing the networking code for
 * processMonitor. 
 * Revision History:
 * 21/08/2005  hbarnor  Created
 * $Id$
 */

#ifndef HB_NETWORKING_H
#define HB_NETWORKING_H

#include <sys/socket.h>
#include <sys/utsname.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>

#include "pm_mesg.hh"

#define VOTE_PORT 4811
#define WHOIS_PORT 4812
#define HEARTBEAT_PORT 4813


#endif //HB_NETWORKING_H
