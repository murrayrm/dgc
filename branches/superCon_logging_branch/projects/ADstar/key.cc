#include "key.h"

key::key(double first, double second)
{
  k1 = first;
  k2 = second;
}

double key::getK1()
{
  return k1;
}

double key::getK2()
{
  return k2;
}

double key::setK1(double newK1)
{
  k1 = newK1;
}

double key::setK2(double newK2)
{
  k2 = newK2;
}

key::~key()
{
  //All data fields in this class are primitive, so nothing to do here
}

