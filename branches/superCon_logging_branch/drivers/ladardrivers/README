SICK LASER DRIVER

The original sick laser driver was written by someone at USC.  We have
made a few modifications to it.  The original README is preserved
below - klk


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SICK LASER DRIVER V0.9
**********************

Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

See COPYRIGHT.TXT for more details.

Introduction
------------

This is *NOT* a complete driver this is some basic code that sets the
laser up to run at 38K and request the laser to send its raw laser scans
continuously. It was developed and tested on Linux using the SICK LMS 400.

Use this code as starting point to make the laser do what you want it 
to do.

This driver is very loosely based on a previous version made by 
Richard T Vaughan.

Enjoy,

Kasper Stoy
kaspers@robotics.usc.edu
USC Robotics Labs
http::/www-robotics.usc.edu
Summer 2000


Quick start
-----------

Make the project by typing "make". This will compile the program laserreader
which setups the laser, reads ten laser scans and shuts down the laser. 

NOTE: Make sure that you change the argument to the constructor of the
laserdevice class to point to the device your laser is connected to.

CLaserDevice laserDevice("/dev/ttyS0"); /* change this */




Files
-----
laserdevice.h	This is where all the laser specific stuff is
laserdevice.cc

device.h	Class that takes care of mutual exclusion of the laser
device.cc	driver thread and the user thread

main.cc		An examples program

Makefile	Makefile to compile the example program: laserreader

README		This file




Interface
---------

If you don't want to change any of the driver code you only need to
know the basic interface consisting of four methods.


CLaserDevice(char *port)
************************

You supply the port the laser is connected to. This function sets up local
variables and locks the data buffer so no thread can access the data yet
because it is of course not valid.

Example:

CLaserDevice laserDevice("/dev/ttyS0");


int Setup()
***********

Configures the laser to start sending scans at 38k. Spawns of a thread to
continuously read from the laser and copy to a private data buffer. The first
time data is copied into this buffer it is unlocked and can be accessed 
through the LockNGetData method. If it returns 0 it was successful otherwise
not.

Example:

result = laserDevice.Setup();


void LockNGetData( unsigned char * )
************************************

This function will lock until the laser has been setup and returned the first 
laser scan. After that it will copy the most recent laser scan into the 
supplied array. The format is of the data is:


|           l               | 
| scan[0] low order byte    | 
| scan[0] high order byte   | 
.... 
| scan[180] low order byte  |
| scan[180] high order byte |

The scan is distributed equally in the interval from 90 degrees to the right
of the laser to 90 degrees to the left. The laser is communicated at 38k meaning
that a new scan is available at approximately 5hz. Therefore it doesn't make
sense to call LockNGetData more than five times a second.

Example:
unsigned char data[1024];

laserDevice.LockNGetData( data );


int LockNShutdown()
**************

Resets local variables and locks the access to the data buffer so nobody can
get to it.

Example:
laserDevice.LockNShutdown();

