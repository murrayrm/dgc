#include <stdlib.h>
#include <iostream.h>
#include <stdio.h>
#include <unistd.h>
#include <fstream.h>
#include "novatel.hh"
#include "ggis.h"
#include <math.h>

struct rawNov
{
      unsigned long long time;
      novData data;
};

int main(int argc, char **argv)
{
  GisCoordLatLon latsForGPS;
  GisCoordUTM utmForGPS;

  if (argc < 3) {
    cout << "Syntax: decompress_gps infile outfile";
    return 0;
  }
  char infile[100];
  char outfile[100];
  ifstream input_stream;
  ofstream output_stream;
  unsigned int nav_mode;

  rawNov nov_in;
  novDataWrapper my_novdata;

  unsigned long long novtime;

  sprintf(infile, "%s", argv[1]);
  sprintf(outfile, "%s", argv[2]);

  input_stream.open(infile);
  output_stream.open(outfile);

  output_stream.precision(40);

  while (input_stream) {

    input_stream.read((char*)&nov_in, sizeof(rawNov));

    memcpy(&my_novdata.data, &(nov_in.data), sizeof(novData));

    novtime = nov_in.time;

    if (nov_in.data.msg_type == BESTPOS) {

      latsForGPS.latitude = nov_in.data.lat*(180/M_PI);
      latsForGPS.longitude = nov_in.data.lon*(180/M_PI);
      gis_coord_latlon_to_utm(&latsForGPS, &utmForGPS, GIS_GEODETIC_MODEL_WGS_84);
    
      output_stream << novtime << "\t";
      output_stream << nov_in.data.lat << "\t";
      output_stream << nov_in.data.lon << "\t";
      output_stream << nov_in.data.pos_type << "\t";
      output_stream << endl;

    }
    /*    output_stream << gps_in.data.gdop << "\t";
    output_stream << gps_in.data.pdop << "\t";
    output_stream << gps_in.data.hdop << "\t";
    output_stream << gps_in.data.vdop << "\t";
    output_stream << gps_in.data.tdop << "\t";
    output_stream << gps_in.data.tfom << "\t";
    output_stream << gps_in.data.sats_used << endl;*/
  }
}

