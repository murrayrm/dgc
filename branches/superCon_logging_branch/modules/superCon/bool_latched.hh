#ifndef BOOL_LATCHED_HH
#define BOOL_LATCHED_HH

//Defines a class which behaves like a latching boolean, i.e. when a new
//(bool) value is assinged to an instance, it is checked against the currently
//stored value, and the new stored value = currently stored value LOGICAL OR (||)
//new value, i.e. the bool_latched class instance latches on TRUE.

class bool_latched
{

public:
  
  /* CONSTRUCTORS  */
  bool_latched() : stored(false) {}
  bool_latched(bool other) : stored(other) {}

  bool_latched & operator=(const bool other) { 
    if( stored == true ) {
      //don't make any updates - value already latched to TRUE
    } else {
      stored = other;
    }
    return *this;
  }

  void reset() { stored = false; }
  bool getValue() { return stored; }

  //This makes an instance of this class return the private member 'stored'
  //when the instance is used in a situation in which a boolean is required
  //e.g. if( *condition* ) --> *condition* is required to equate to a boolean
  operator bool() { return stored; }

private:
  bool stored;

};

#endif //BOOL_LATCHED_HH
