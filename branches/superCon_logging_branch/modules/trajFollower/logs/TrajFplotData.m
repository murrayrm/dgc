%TRAJFPLOTDATA.M  TrajFplotData.m
%
%Plots the data read in by the TrajFreadinData(date_time_tag) function, the
%following plots are produced:
%
%{planned paths, actual path}
%{y-error, index}
%{h-error, index}
%{c-error, index}
%{RMS y-error, index}
%{RMS theta-error, index}
%{planned speed, actual speed}
%{speed-error, index}
%{RMS speed-error, index}


%%%%% -- PLOTS -- %%%%%

%% {planned paths; actual path} %%
figure(1);

%Get the screensize of the machine being used in pixels
desired_position = get(0, 'Screensize');
    
%Change the size of the figure and position such that it fills the
%screen of the machine being used
figure('Position', desired_position);

%x-axis = NORTHING
%y-axis = EASTING 
%Plot the INTENDED PATH
plot(defaultTraj_raw(:,1), defaultTraj_raw(:,4)); hold all;
%Plot the ACTUAL PATH
plot(state2_raw(:,1), state2_raw(:,2)); hold off;

%Set-up the plot, labels etc
xlabel('Northing UTM (m)');
ylabel('Easting UTM (m)');
title('Plot of the intended & actual paths taken during 10x20');
legend('Intended', 'Actual', 'Location', 'Best');

%Change the figures PaperPositionMode to AUTO - this means that the plots
%are 'printed' as JPEGs with the SAME DIMENSIONS as they have on the screen
set(gcf,'PaperPositionMode','auto')

%Save the plot as a JPEG
print('-djpeg', 'routes taken');




%% {y-error, index} %%
figure(2);

%Get the screensize of the machine being used in pixels
desired_position = get(0, 'Screensize');
    
%Change the size of the figure and position such that it fills the
%screen of the machine being used
figure('Position', desired_position);

%x-axis = Point Index
%y-axis = y-error (m)
plot(error_raw(:, 10), error_raw(:,1));

%Set-up the plot, labels etc
xlabel('Point Index');
ylabel('y-error (m)');
title('Plot of y-error against point index during the 10x20');

%Change the figures PaperPositionMode to AUTO - this means that the plots
%are 'printed' as JPEGs with the SAME DIMENSIONS as they have on the screen
set(gcf,'PaperPositionMode','auto')

%Save the plot as a JPEG
print('-djpeg', 'y-error index');





%% {theta-error, index} %%
figure(3)

%Get the screensize of the machine being used in pixels
desired_position = get(0, 'Screensize');
    
%Change the size of the figure and position such that it fills the
%screen of the machine being used
figure('Position', desired_position);

%x-axis = Point Index
%y-axis = theta-error (radians)
plot(error_raw(:,10), error_raw(:, 2));

%Set-up the plot, labels etc
xlabel('Point Index');
ylabel('Theta-error (rad)');
title('Plot of theta-error against point index during the 10x20');

%Change the figures PaperPositionMode to AUTO - this means that the plots
%are 'printed' as JPEGs with the SAME DIMENSIONS as they have on the screen
set(gcf,'PaperPositionMode','auto')

%Save the plot as a JPEG
print('-djpeg', 'theta-error index');





%% {combined-error, index} %%

%Get the screensize of the machine being used in pixels
desired_position = get(0, 'Screensize');
    
%Change the size of the figure and position such that it fills the
%screen of the machine being used
figure('Position', desired_position);

figure(4)
%x-axis = Point Index
%y-axis = combined-error (quasi unitless)
plot(error_raw(:,10), error_raw(:,3));

%Set-up the plot, labels etc
xlabel('Point Index');
ylabel('Combined-error (quasi-unitless)');
title('Plot of the combined-error against point index during the 10x20');

%Change the figures PaperPositionMode to AUTO - this means that the plots
%are 'printed' as JPEGs with the SAME DIMENSIONS as they have on the screen
set(gcf,'PaperPositionMode','auto')

%Save the plot as a JPEG
print('-djpeg', 'combined-error index');





%% {RMS y-error, index} %%
figure(5)

%Get the screensize of the machine being used in pixels
desired_position = get(0, 'Screensize');
    
%Change the size of the figure and position such that it fills the
%screen of the machine being used
figure('Position', desired_position);

%x-axis = point index
%y-axis = RMS y-error
plot(error_raw(:,10), error_raw(:,6));

%Set-up the plot, labels etc
xlabel('Point Index');
ylabel('RMS y-error (m)');
title('Plot of RMS y-error against point index during the 10x20');

%Change the figures PaperPositionMode to AUTO - this means that the plots
%are 'printed' as JPEGs with the SAME DIMENSIONS as they have on the screen
set(gcf,'PaperPositionMode','auto')

%Save the plot as a JPEG
print('-djpeg', 'RMS y-error index');





%% {RMS theta-error, index} %%
figure(6)

%Get the screensize of the machine being used in pixels
desired_position = get(0, 'Screensize');
    
%Change the size of the figure and position such that it fills the
%screen of the machine being used
figure('Position', desired_position);

%x-axis = point index
%y-axis = RMS theta-error
plot(error_raw(:,10), error_raw(:, 9));

%Set-up the plot, labels etc
xlabel('Point Index');
ylabel('RMS theta-error (rad)');
title('Plot of RMS theta-error against point index during the 10x20');

%Change the figures PaperPositionMode to AUTO - this means that the plots
%are 'printed' as JPEGs with the SAME DIMENSIONS as they have on the screen
set(gcf,'PaperPositionMode','auto')

%Save the plot as a JPEG
print('-djpeg', 'RMS theta-error index');





%% {planned speed, actual speed} %%
figure(7)

%Get the screensize of the machine being used in pixels
desired_position = get(0, 'Screensize');
    
%Change the size of the figure and position such that it fills the
%screen of the machine being used
figure('Position', desired_position);

%x-axis = point index
%y-axis = speed (m/s)
%Plot the intended speed
plot(velocity_raw(:,4), velocity_raw(:,1)); hold all;
%Plot the actual speed
plot(velocity_raw(:,4), velocity_raw(:,2)); hold off;

%Set-up the plot, labels etc
xlabel('Point Index');
ylabel('Speed (m/s)');
title('Plot of the actual & intended speeds during the 10x20');
legend('Intended', 'Actual', 'Location', 'Best');

%Change the figures PaperPositionMode to AUTO - this means that the plots
%are 'printed' as JPEGs with the SAME DIMENSIONS as they have on the screen
set(gcf,'PaperPositionMode','auto')

%Save the plot as a JPEG
print('-djpeg', 'intended actual speed index');






%% {speed-error, index} %%
figure(8)

%Get the screensize of the machine being used in pixels
desired_position = get(0, 'Screensize');
    
%Change the size of the figure and position such that it fills the
%screen of the machine being used
figure('Position', desired_position);

%x-axis = point index
%y-axis = speed-error (m/s)
plot(velocity_raw(:,4), velocity_raw(:,3))

%Set-up the plot, labels etc
xlabel('Point Index');
ylabel('Speed-error (m/s)');
title('Plot of the speed-error during the 10x20');

%Change the figures PaperPositionMode to AUTO - this means that the plots
%are 'printed' as JPEGs with the SAME DIMENSIONS as they have on the screen
set(gcf,'PaperPositionMode','auto')

%Save the plot as a JPEG
print('-djpeg', 'speed error index');





%% {RMS speed-error, index} %%
figure(9)

%Get the screensize of the machine being used in pixels
desired_position = get(0, 'Screensize');
    
%Change the size of the figure and position such that it fills the
%screen of the machine being used
figure('Position', desired_position);

%x-axis = point index
%y-axis = RMS speed-error
plot(velocity_raw(:,4), velocity_raw(:,7));

%Set-up the plot, labels etc
xlabel('Point Index');
ylabel('RMS speed-error (m/s)');
title('Plot of the RMS speed-error during the 10x20');

%Change the figures PaperPositionMode to AUTO - this means that the plots
%are 'printed' as JPEGs with the SAME DIMENSIONS as they have on the screen
set(gcf,'PaperPositionMode','auto')

%Save the plot as a JPEG
print('-djpeg', 'RMS speed-error index');

