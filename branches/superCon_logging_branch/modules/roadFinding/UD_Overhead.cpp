//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Overhead.hh"
	
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// horrible kludge to get GLUT callback functions into class

UD_Overhead *UD_Overhead::overhead = new UD_Overhead(40, 60, 4.0, NULL);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for overhead viewing class.  units are meters

UD_Overhead::UD_Overhead(float mapw, float lookahead_distance, float pixels_per_meter, UD_RoadFollower *roadfollower)
{
  // local names

  rf = roadfollower;

  pix_per_meter = pixels_per_meter;
  map_width = mapw;
  map_depth = lookahead_distance;

  // default values

  vehicle_offset = 5;

  vehicle_width                  = 2.0;
  vehicle_height                 = 2.0;
  vehicle_front_axle_to_bumper   = 0.5;
  vehicle_rear_axle_to_bumper    = 0.5;
  vehicle_wheelbase              = 4.0;
}

//----------------------------------------------------------------------------

/// initialize overhead display functions, open window, etc.

void UD_Overhead::initialize_display(int x_position, int y_position)
{
  width = (int) ceil(map_width * pix_per_meter);
  height = (int) ceil((vehicle_offset + map_depth) * pix_per_meter);

  glutInitWindowSize(width, height);
  glutInitWindowPosition(x_position, y_position);
  window_id = glutCreateWindow("overhead");

  glutDisplayFunc(sdisplay); 
  glutMouseFunc(smouse); 
  glutPassiveMotionFunc(spassivemotion); 
  glutKeyboardFunc(skeyboard);

  glPixelZoom(1, -1);
  glRasterPos2i(-1, 1);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, width-1, 0, height-1);
}

//----------------------------------------------------------------------------

/// in vehicle coordinates (meters)

void UD_Overhead::draw_circle(float x, float z, float radius, int r, int g, int b)
{
  float theta, delta_theta;

  delta_theta = 0.1;

  // center cross

  glColor3ub(255-r, 255-g, 255-b);

  glBegin(GL_LINES);

  glVertex2f(v2o_x(x - 0.5), v2o_z(z));
  glVertex2f(v2o_x(x + 0.5), v2o_z(z));

  glVertex2f(v2o_x(x), v2o_z(z - 0.5));
  glVertex2f(v2o_x(x), v2o_z(z + 0.5));

  glEnd();

  // circle perimeter

  glColor3ub(r, g, b);

  glBegin(GL_LINE_LOOP);

  for (theta = 0; theta < 2*PI; theta += delta_theta) 
    glVertex2f(v2o_x(x + radius * cos(theta)), v2o_z(z+ radius * sin(theta)));

  glEnd();
}

//----------------------------------------------------------------------------

void UD_Overhead::draw_gap()
{
  int i, j;
  float r, g, d;
  float dx, dy, x0, y0, x, z, val, maxval, maxx;

  //  if (rf->is_synced()) {

    // draw road "shape" determined by direction, offset, widths

  //  printf("drawing \n");
  for (i = 0; i < rf->num_lookahead_pts; i++) {
    //    printf("%lf %lf %lf\n", rf->lookahead_x[i], rf->lookahead_z[i], rf->lookahead_radius[i]);
    draw_circle(rf->lookahead_x[i], rf->lookahead_z[i], rf->lookahead_radius[i], 0, 255, 0);
  }

    /*
    for (i = 0; i < rf->ladarray->ladsrc[0]->num_rays; i++) {
      if (rf->ladarray->ladsrc[0]->range[i] != NO_RETURN) {

	x = rf->gap_tracker->baseline_project(rf->ladarray->ladsrc[0]->vehicle_x[i], rf->ladarray->ladsrc[0]->vehicle_z[i]);
	
	val = fabs(rf->ladarray->ladsrc[0]->vehicle_y[i]);
	r = 5.0*val;
	g = 5.0*(1 - val);
	glColor3f(MIN2(r,1), MAX2(g,0),0);
	glVertex2f(v2o_x(x), y0);
      }
    }
    
    glEnd();
    */
    //  }

  // draw gap tracker particles as yellow line segments

  glColor3ub(255, 255, 0);
  glLineWidth(1);
  
  glBegin(GL_LINES);
  
  for (i = 0; i < rf->gap_tracker->num_samples; i++) {
    glVertex2f(v2o_x(rf->gap_tracker->sample[i]->x[0] - 0.5 * rf->gap_tracker->gapwidth), v2o_z(-2.0));
    glVertex2f(v2o_x(rf->gap_tracker->sample[i]->x[0] + 0.5 * rf->gap_tracker->gapwidth), v2o_z(-2.0));
  }
  
  glEnd();

  // draw gap tracker state as thick blue line segment

  glColor3ub(0, 0, 255);
  glLineWidth(2);
  
  glBegin(GL_LINES);
  
  for (i = 0; i < rf->gap_tracker->num_samples; i++) {
    glVertex2f(v2o_x(rf->gap_tracker->state->x[0] - 0.5 * rf->gap_tracker->gapwidth), v2o_z(-2.0));
    glVertex2f(v2o_x(rf->gap_tracker->state->x[0] + 0.5 * rf->gap_tracker->gapwidth), v2o_z(-2.0));
  }
  
  glEnd();

  glLineWidth(1);
    
}

//----------------------------------------------------------------------------

/// draw ladar hit points in vehicle coordinates, with color indicating 
/// height

void UD_Overhead::draw_ladars()
{ 
  int i, j;

  //  printf("%p %p\n", rf, ladarray);

  glPointSize(2);
        
  glBegin(GL_POINTS);
    
  for (j = 0; j < ladarray->num_ladars; j++) {
    //  for (j = 0; j < rf->ladarray->num_ladars; j++) {
    //if (ladarray->ladsrc[j]->statesrc->is_synced()) {
      if (j == 0)
	glColor3f(1.0, 1.0, 0.0);
      else if (j == 1)
	glColor3f(0.0, 0.0, 1.0);
      else if (j == 2)
	glColor3f(1.0, 0.0, 0.0);

      for (i = 0; i < ladarray->ladsrc[j]->num_rays; i++) {
	if (ladarray->ladsrc[j]->dangerous[i])
	  glVertex2f(v2o_x(ladarray->ladsrc[j]->vehicle_x[i]), 
		     v2o_z(ladarray->ladsrc[j]->vehicle_z[i]));
      }
      //}
  }

  glPointSize(1);

  // hit points

  /*
    //  if (lad->diff < lad->max_diff) {

    //    printf("2\n");fflush(stdout);

    glPointSize(2);
    glBegin(GL_POINTS);

    for (i = 0; i < lad->num_rays; i++) {
      if (lad->range[i] != NO_RETURN) {
	r = 5.0*fabs(lad->vehicle_y[i]);
	g = 5.0*(1 - fabs(lad->vehicle_y[i]));
	glColor3f(MIN2(r,1), MAX2(g,0),0);
	
	glVertex2f(v2o_x(lad->vehicle_x[i]), v2o_z(lad->vehicle_z[i]));
      }
    }
    glEnd();
  }
  */

  //  printf("3\n");fflush(stdout);

  //  rf->gap_tracker->draw();
}

//----------------------------------------------------------------------------

/// draw schematic overhead view of estimates curvature, boundaries of
/// road ahead

void UD_Overhead::draw_road()
{
  float dx, dy, x0, y0, x, road_half_width, cos_theta;

  dy = (float) height;
  dx = tan(rf->direction_error) * dy;
  x0 = v2o_x(0.0);
  y0 = v2o_z(0.0);

  dx = tan(rf->direction_error) * map_depth;
  //  cos_theta = cos(rf->direction_error);
  cos_theta = 1 + tan(rf->direction_error);
  road_half_width = 2.0;

  /*
  glColor3ub(100, 100, 100);

  glBegin(GL_LINES);

  if (rf->gap_tracker) {
    for (x = -0.5 * rf->gap_tracker->ladar_hist_range; x < 0.5 * rf->gap_tracker->ladar_hist_range; x += rf->gap_tracker->ladar_hist_bin_width) {   
      glVertex2f(v2o_x(x), y0);
      glVertex2f(v2o_x(x + dx), y0 + dy);
    }
  }
  else {
    for (x = -width/2; x < 3*width/2; x += 10) {   
      glVertex2f(x, y0);
      glVertex2f(x + dx, y0 + dy);
    }
  }
  */

  /*
  glColor3ub(255, 0, 255);

  glVertex2f(v2o_x(rf->lateral_offset), v2o_z(0.0));
  glVertex2f(v2o_x(rf->lateral_offset + dx), v2o_z(map_depth));

  glColor3ub(255, 255, 0);

  glVertex2f(v2o_x(rf->lateral_offset - cos_theta * road_half_width), v2o_z(0.0));
  glVertex2f(v2o_x(rf->lateral_offset + dx - cos_theta * road_half_width), v2o_z(map_depth));

  glVertex2f(v2o_x(rf->lateral_offset + cos_theta * road_half_width), v2o_z(0.0));
  glVertex2f(v2o_x(rf->lateral_offset + dx + cos_theta * road_half_width), v2o_z(map_depth));
  */

  glEnd();

}

//----------------------------------------------------------------------------

/// giving sun position and vehicle state, predict vehicle shadow in 
/// vehicle coordinates assuming 0 pitch and planar ground.  

void UD_Overhead::draw_vehicle_shadow(float sunalt, float sunazi)
{
  double r, dx, dz, x[4], z[4];

  x[0] = -0.5 * vehicle_width;
  x[1] = 0.5 * vehicle_width;
  x[2] = 0.5 * vehicle_width;
  x[3] = -0.5 * vehicle_width;

  z[0] = vehicle_front_axle_to_bumper;
  z[1] = vehicle_front_axle_to_bumper;
  z[2] = -(vehicle_wheelbase + vehicle_rear_axle_to_bumper);
  z[3] = -(vehicle_wheelbase + vehicle_rear_axle_to_bumper);

  //  printf("sunalt = %f degs\n", RAD2DEG(sunalt));

  // make sure sunalt > 0

  if (sunalt < 0)
    return;
   
  // given corner of roof at (x, y, z) and direction to sun (alt, azi)
  // -- where (0, 0) is on horizon and straight ahead -- project it onto
  // ground (y = 0) at point (x', y', z')

  r = vehicle_height / tan(sunalt);
  dx = r * sin(sunazi);
  dz = r * cos(sunazi);

  // quadrangle representing outline of vehicle shadow

  glLineWidth(1);
  glColor3ub(128, 128, 128);

  glBegin(GL_LINE_LOOP);
  
  for (int i = 0; i < 4; i++)
    glVertex2f(v2o_x(x[i] + dx), v2o_z(z[i] + dz));

  glEnd();
  
}

//----------------------------------------------------------------------------

/// draw schematic overhead view of vehicle assuming midpoint of front axle
/// is origin of vehicle coordinate system

void UD_Overhead::draw_vehicle()
{
  glLineWidth(1);

  // line representing front of vehicle (even with front axle)

  /*
  glColor3ub(0, 0, 255);

  glBegin(GL_LINES);

  glVertex2f(0, v2o_z(0.0));
  glVertex2f(width-1, v2o_z(0.0));

  glEnd();
  */

  // rectangle representing outline of vehicle

  glColor3ub(255, 0, 255);

  glBegin(GL_LINE_LOOP);
  
  glVertex2f(v2o_x(-0.5 * vehicle_width), v2o_z(vehicle_front_axle_to_bumper));
  glVertex2f(v2o_x(0.5 * vehicle_width), v2o_z(vehicle_front_axle_to_bumper));
  glVertex2f(v2o_x(0.5 * vehicle_width), v2o_z(-(vehicle_wheelbase + vehicle_rear_axle_to_bumper)));
  glVertex2f(v2o_x(-0.5 * vehicle_width), v2o_z(-(vehicle_wheelbase + vehicle_rear_axle_to_bumper)));

  glEnd();

}

//----------------------------------------------------------------------------

void UD_Overhead::display()
{
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  if (ladarray->num_ladars > 0) {
    if (rf)
      draw_gap();
    draw_ladars();
  }
  else
    draw_road();
  draw_vehicle();
  //  draw_vehicle_shadow(rf->ladarray->ladsrc[0]->statesrc->sunalt, rf->ladarray->ladsrc[0]->statesrc->sunazi - rf->ladarray->ladsrc[0]->statesrc->heading + PI);

  glutSwapBuffers();

}

//----------------------------------------------------------------------------

void UD_Overhead::keyboard(unsigned char key, int x, int y)
{
  printf("overhead key\n");
}

//----------------------------------------------------------------------------

int ystart;
int mouse_set = 0;

void UD_Overhead::mouse(int button, int state, int x, int y)
{
  //  if (GLUT_MOUSE_DOWN) {
  //  ystart = y;
  //  printf("overhead mouse down\n");
  // }
  // else {
  printf("overhead mouse %i\n", y);
  ystart = y;
  mouse_set = !mouse_set;
    //  }
}

//----------------------------------------------------------------------------

void UD_Overhead::passivemotion(int x, int y)
{
  /*
  ladarray->ladsrc[1]->cal->pitch_angle = -0.005 * (float) (y);
  ladarray->ladsrc[1]->cal->sin_pitch_angle = sin(ladarray->ladsrc[1]->cal->pitch_angle);
  ladarray->ladsrc[1]->cal->cos_pitch_angle = cos(ladarray->ladsrc[1]->cal->pitch_angle);
 
  //  if (mouse_set)
  printf("%f\n", ladarray->ladsrc[1]->cal->pitch_angle);
  */
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
