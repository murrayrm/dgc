//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_ParticleFilter.hh"

//----------------------------------------------------------------------------
// minimal subset of UD_Linux_Matrix that UD_ParticleFilter needs
//----------------------------------------------------------------------------

/// make a vector (a 1-D array of doubles)

UD_Vector *make_UD_Vector(char *name, int rows)
{
  UD_Vector *V;

  V = (UD_Vector *) malloc(sizeof(UD_Vector));

  V->name = (char *) calloc(MAXSTRLEN, sizeof(char));
  strncpy(V->name, name, MAXSTRLEN);

  V->rows = rows;

  V->x = (double *) calloc(rows, sizeof(double));

  V->buffsize = rows * sizeof(double);

  return V;
}

//----------------------------------------------------------------------------

/// create new vector that is exact copy of old one

UD_Vector *copy_UD_Vector(UD_Vector *V)
{
  UD_Vector *V_copy = copy_header_UD_Vector(V);

  memcpy(V_copy->x, V->x, V->buffsize);

  return V_copy;
}

//--------------------------------------

/// copy vector V1's data to V2, overwriting V2's old data

void copy_UD_Vector(UD_Vector *V1, UD_Vector *V2)
{
  memcpy(V2->x, V1->x, V1->buffsize);
}

//----------------------------------------------------------------------------

/// make "empty" vector of same size as template without duplicating contents

UD_Vector *copy_header_UD_Vector(UD_Vector *V)
{
  UD_Vector *V_copy;

  V_copy = make_UD_Vector("V_copy", V->rows);

  return V_copy;
}

//----------------------------------------------------------------------------

/// scale every element of vector in place

void scale_UD_Vector(double a, UD_Vector *V)
{
  int i;

  for (i = 0; i < V->rows; i++)
    V->x[i] *= a;
}

//----------------------------------------------------------------------------

/// sum individual elements of array of same-sized vectors into
/// single "sum" vector

void sum_UD_Vector_array(UD_Vector **V_array, int n, UD_Vector *Sum)
{
  int i, j;

  for (j = 0; j < Sum->rows; j++) {
    Sum->x[j] = 0;
    for (i = 0; i < n; i++)
      Sum->x[j] += V_array[i]->x[j];
  }
}

//----------------------------------------------------------------------------

/// print a vector

void print_UD_Vector(UD_Vector *V)
{
  int r;

  printf("%s = ", V->name);
  printf("[ ");
  for (r = 0; r < V->rows - 1; r++)
    printf("%6.3lf, ", V->x[r]);
  printf("%6.3lf ];\n", V->x[r]);
}

//----------------------------------------------------------------------------

/// generate N-dimensional sample from unrotated Gaussian distribution
/// (i.e., off-diagonal elements of covariance are 0) centered on VMean

void sample_gaussian_diagonal_covariance(UD_Vector *V, UD_Vector *VMean, UD_Vector *CovDiagSqrt)
{
  int i;

  for (i = 0; i < V->rows; i++) 
    V->x[i] = VMean->x[i] + CovDiagSqrt->x[i] * normal_UD_Random();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for particle filter base class.  samp_mean is vector indicating 
/// dimensionality of state, and samp_Cov is covariance of Gaussian process/
/// dynamics noise in state update step.  the terms "particles" and "samples"
/// are used interchangeably here

UD_ParticleFilter::UD_ParticleFilter(int n, UD_Vector *samp_mean, UD_Vector *covdiag)
{
  int i;

  updates = 0;

  num_samples = n;

  // happens when this is a multi-tracker

  if (!n || !samp_mean || !covdiag)
    return;

  sample = (UD_Vector **) calloc(num_samples, sizeof(UD_Vector *));
  new_sample = (UD_Vector **) calloc(num_samples, sizeof(UD_Vector *));
  
  weight = make_UD_Vector("weight", num_samples);
  cumulative_weight = make_UD_Vector("cumulative_weight", num_samples);
  
  weighted_sample = (UD_Vector **) calloc(num_samples, sizeof(UD_Vector *)); 

  moved_sample = copy_UD_Vector(samp_mean);  
  state = copy_UD_Vector(samp_mean);

  print_UD_Vector(covdiag);
  covdiagsqrt = copy_header_UD_Vector(covdiag);
  for (i = 0; i < covdiagsqrt->rows; i++)
    covdiagsqrt->x[i] = sqrt(covdiag->x[i]);
}

//----------------------------------------------------------------------------

/// allocate arrays for samples and generate initial distribution of sample
/// states from prior p(state).  this function should be called immediately after class constructor

void UD_ParticleFilter::initialize_prior()
{
  int i;

  for (i = 0; i < num_samples; i++) {
    sample[i] = copy_UD_Vector(state);
    weighted_sample[i] = copy_UD_Vector(state);
    pf_samp_prior(sample[i]);
    new_sample[i] = copy_UD_Vector(sample[i]);
  }  
}

//----------------------------------------------------------------------------

/// return all particles back to their initial, prior distribution

void UD_ParticleFilter::reset()
{
  int i;

  updates = 0;

  //----------------------------------------------------
  // generate samples from prior p(state)
  //----------------------------------------------------

  for (i = 0; i < num_samples; i++) 
    pf_samp_prior(sample[i]);
}

//----------------------------------------------------------------------------

/// execute one iteration of particle filter update.  Z is optional array of measurements

void UD_ParticleFilter::update()
{
  int i, samp_chosen;
  double r;

  updates++;

  //----------------------------------------------------
  // compute conditional probabilities p(z|state) 
  // and keep track of their partial sums
  //----------------------------------------------------

  for (i = 0, weight_sum = 0; i < num_samples; i++) {
    weight->x[i] = pf_condprob_zx(sample[i]);
    weight_sum += weight->x[i]; 
  }

  if(weight_sum==0) {
    printf("Incorrect weight sum!!!!");
    return;
  }
  
  for (i = 0; i < num_samples; i++) {
    weight->x[i] /= weight_sum;
    if (i == 0)
      cumulative_weight->x[i] = weight->x[i];
    else
      cumulative_weight->x[i] = cumulative_weight->x[i - 1] + weight->x[i];
  }

  //----------------------------------------------------
  // pick num_samples samples (with replacement) from 
  // sample set, choosing sample s_i with probability 
  // weight_i, to obtain new_sample
  //----------------------------------------------------
  
  for (i = 0; i < num_samples; i++) {
    r = uniform_UD_Random();
    samp_chosen = interval_membership(0, num_samples - 1, r);
    copy_UD_Vector(sample[samp_chosen], new_sample[i]);
  }

  //----------------------------------------------------
  // predict by applying deterministic and stochastic
  // transformations to elements of new_sample,
  // which become s for the next round.
  // also, keep track of weighted samples in 
  // weighted_sample and estimate state
  //----------------------------------------------------

  for (i = 0; i < num_samples; i++) {
     pf_dyn_state(new_sample[i], moved_sample);
     pf_samp_state(moved_sample, sample[i]);
  }

  //----------------------------------------------------
  // keep track of weighted samples and their sum,
  // which is the estimated state.
  // a mode finder makes more sense
  //----------------------------------------------------
  
  for (i = 0; i < num_samples; i++) { 
    copy_UD_Vector(sample[i], weighted_sample[i]);
    scale_UD_Vector(weight->x[i], weighted_sample[i]);
  }

  sum_UD_Vector_array(weighted_sample, num_samples, state);
}

//----------------------------------------------------------------------------

/// use random number r in [0, 1] to pick index of num_samples particles according to
/// cumulative particle probabilities, where c_i = sum i=0:i of p_i,
/// the probability of each particle (i.e., sum i=0:n-1 p_i = 1)

int UD_ParticleFilter::interval_membership(int l, int u, double r)
{
  int m;

  if (l == u)
    return l;
  else {
    m = (l + u) / 2;
    if (cumulative_weight->x[m] >= r)
      return interval_membership(l, m, r);
    else {
      if (m == u - 1)
	return u;
      else
	return interval_membership(m, u, r);
    }
  }
}

//----------------------------------------------------------------------------

/// print current particle filter parameters

void UD_ParticleFilter::print()
{
  int i;

  printf("\n");

  printf("%i ------------------------------\n\n",  updates);

  for (i = 0; i < num_samples; i++) {
    printf("%i:\n", i);
    print_UD_Vector(sample[i]);
  }

  printf("\n");

  for (i = 0; i < num_samples; i++)
    printf("%i: pi = %f\n", i, weight->x[i]);

  printf("\n");

  for (i = 0; i < num_samples; i++)
    printf("%i: c = %f\n", i, cumulative_weight->x[i]);

  printf("\n");
  printf("***********************\n");
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
