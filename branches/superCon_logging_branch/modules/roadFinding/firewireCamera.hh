//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// CR_CaptureFireWire
//----------------------------------------------------------------------------

#ifndef CR_CAPTURE_DECS

//----------------------------------------------------------------------------

#define CR_CAPTURE_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Firewire-related headers

#include <sys/ioctl.h>
#include <fcntl.h> 

#include <libraw1394/raw1394.h>
#include <libraw1394/ieee1394.h>
#include <libdc1394/dc1394_control.h>
#include <ieee1394-ioctl.h>

#include "unistd.h"

#include "conversions.h"   // just for YUV2RGB

#include "cv.h"
#include "highgui.h"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif


#define MAX_PORTS   4
#define MAX_CAMERAS 8
#define NUM_BUFFERS 32
#define DROP_FRAMES 1

#define  CV_CAST_8U(t)  (uchar)(!((t) & ~255) ? (t) : (t) > 0 ? 255 : 0)

//-------------------------------------------------
// structure
//-------------------------------------------------


//-------------------------------------------------
// functions
//-------------------------------------------------

int initialize_capture_firewire(int *, int *, int);
int capture_raw_firewire_image(char *, int, int);
int capture_firewire_image(IplImage *, int);
int log_firewire_image(FILE *, int);
int capture_and_log_firewire_image(IplImage *, int, FILE *);
void cleanup_capture_firewire();

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
