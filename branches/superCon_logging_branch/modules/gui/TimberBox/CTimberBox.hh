#ifndef __CTIMBERBOX_HH__
#define __CTIMBERBOX_HH__

#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>

#include "SkynetContainer.h"
#include "CTimber.hh"

class CTimberBox : public Gtk::HBox, virtual public CSkynetContainer {
public:
  CTimberBox(int skynetKey);
  ~CTimberBox();

  Gtk::Button* m_button_timber_resume;
  Gtk::Button* m_button_timber_pause;
  Gtk::Button* m_button_timber_replay;

  Gtk::Button* m_button_timber_start;
  Gtk::Button* m_button_timber_stop;
  Gtk::Button* m_button_timber_restart;

private:
  Gtk::Label*  m_label_timber_playback;
  Gtk::Label*  m_label_timber_logging;
  Gtk::HBox*   m_hbox_timber_playback;
  Gtk::HBox*   m_hbox_timber_logging;

  int timberMsgSocket;

  void timberPause();
  void timberResume();
  void timberReplay();

  void timberStart();
  void timberStop();
  void timberRestart();
};


#endif //__CTIMBERBOX_HH__
