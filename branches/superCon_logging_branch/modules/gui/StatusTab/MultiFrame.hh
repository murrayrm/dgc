/**
 * MultiFrame.hh
 * Revision History:
 * 05/21/2005  hbarnor  Created
 */

#ifndef MULTIFRAME_HH
#define MULTIFRAME_HH

#include <gtkmm/frame.h>
#include <gtkmm/table.h>

#include <string>
#include <iostream>
#define maxCol 10
#define maxRow 3

using namespace std;
/**
 * MultiFrame - an encapsulation of a frame and a table. 
 * Allows addition of multiple widges into a 4x5 array
 */
class MultiFrame : public Gtk::Frame
{
public:
  /**
   * Constructor
   */

  MultiFrame(const Glib::ustring &label);
  
  /**
   * override the add method of the frame
   * such that it delegates to the table 
   */
  void add(Widget& widget);
  
private:
/**
   * init - initialize the table 
   * and other aesthetics
   */
  void init();  
  /**
   * The table instance
   * used by the fame
   */
  Gtk::Table myTable;
  /**
   * used to determine current position 
   * in table
   */
  int curX;
  int curY;
};

#endif
