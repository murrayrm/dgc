/**
 * DGCEntry.cc
 * Revision History:
 * 07/08/2005  hbarnor  Created
 * $Id: DGCEntry.cc 8530 2005-07-11 06:26:58Z hbarnor $
 */

#include "DGCEntry.hh"

#warning "Please delete"
#include <iostream>

DGCEntry::DGCEntry(const Glib::ustring& label)
  : DGCWidget(DGCENTRY)
{
  set_text(label);
}


string DGCEntry::getText()
{
  //cout << " Entry, get text" << endl;
  return get_text();
}

void DGCEntry::displayNewValue()
{
  //cout << "Entry update display" << endl;
  set_text(m_displayValue);
}
