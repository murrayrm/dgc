/**
 * MainTab.h
 * Revision History:
 * 01/07/2005  hbarnor  Created
 */

#ifndef MAINTAB_H
#define MAINTAB_H

#include <gtkmm/box.h>
#include <gtkmm/frame.h>

using namespace std;

#define DEBUG false

/**
 * MainTab class. MainTab provides an overview and summary.
 * Displays the status of all modules. Controls for the 
 * for the various modules can also be found here. In 
 * addition displays a scrollable log of all past actions.
 * $Id$ 
 */


class MainTab : public Gtk::VBox
{

 public:

  /**
   * Default constructor.
   * Contains a status table, command buttons
   * and a log.
   */
  MainTab();
  /**
   * MainTab Destructor - for taking care of dynamically 
   * allocated memory. 
   */
  ~MainTab();
 private:
  /**
   * Module Status
   * Container for displaying the status of the 
   * various modules.
   */
  Gtk::Frame statusFrm;
  /**
   * Module Commands
   * Container for the control buttons.
   */
  Gtk::Frame commandFrm;
  /**
   * MainTab logs 
   * Container for log of all commands 
   */
  Gtk::Frame logFrm;
};

#endif
