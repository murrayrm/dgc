#include "sn_msg.h"
#include "stdio.h"
#define BUFSIZE 1024

int main(){
  const int sn_key = 12345;
  modulename myself = fusionmapper;
  sn_register(myself, sn_key);
  sn_msg myinput = pointcloud;
  
  int sock1 = sn_listen(myinput, myself);
  char* buffer[BUFSIZE];
  sn_get_msg(sock1, buffer, BUFSIZE, 0);
  printf("%s\n", (char*)buffer);
  return 0;
  }
