#ifndef SN_TYPES_H
#define SN_TYPES_H

#include "enumstring.h"

/* IMPORTANT: Do NOT insert new message types in the middle of this list!  It 
 * will screw up logging and playback capabilities if you do.  Instead, append 
 * to this list ONLY at the bottom (just before the last_type line). */

#define SN_MSG_LIST(_) \
  _(SNpointcloud, = 0) /** Created for demonstration purposes. Unsure if */\
                  /* it's used by anything important */ \
  _(SNdeltamap, )   /** as above */\
  _(SNdrivecmd, )  /** This is the message type to send commands to adrive. */\
                  /* The structs is in adrive_skynet_interface_types.h */\
  _(SNdrivecom, )  /**This is the message type for returns from adrive to modules.*/\
                 /* The struct is in adrive_skynet_interface_types.h */\
	_(SNactuatorstate, )/* the state of all of the actuators*/\
  _(SNmodlist, )    /** chirps which modules are registered */\
  _(skynetcom, )   /** Skynet internal use only */\
  _(rmulti, )      /** Skynet internal use only */\
  _(SNtraj, )\
  _(SNstate, )\
  _(SNgetmetastate, )\
  _(SNmetastate, )\
  _(SNGuiMsg, ) /** Feedback to the gui containing 256 char mesg**/\
  _(SNmodcom, )  /** comma separated stringg containing commands to be started*/ \
                 /*and options*/\
  _(SNRDDFtraj, )  /** a hack until skynet get_msg knows which module to get*/\
		   /*message from: traj's sent by RddfPathGen **/\
  _(SNroadtraj, )  /*message from: traj's sent by Road Finding **/\
  _(SNplannertraj, ) /** a hack until skynet get_msg knows which module to get*/\
                      /*message from: traj's sent by PlannerModule **/\
  _(SNmodemantraj, )/** a hack until skynet get_msg knows which module to get*/\
                      /*message from: traj's sent by ModeManModule **/\
	_(SNtrajPlannerSeed, )/** a debugging traj sent by the planner.*/\
												 /*this is the solver seed */\
	_(SNtrajPlannerInterm, )/** a debugging traj sent by the planner.*/\
												 /*this is the result of the spatial stage */\
  _(SNreactiveTraj, )\
  _(SNfusiondeltamap, )\
  _(SNladardeltamap,  )\
  _(SNstereodeltamap, )\
  _(SNstaticdeltamap, )\
  _(SNroad2map, )    /** Send a road to fusion mapper */\
  _(SNtimberstring, ) /** a char (probably of length 256) for use in sending logger info */\
  _(SNmark1, )      /** used only for benchmarking*/\
  _(SNmark2, )      /** used only for benchmarking*/\
  _(SNladardeltamap_roof, )\
  _(SNladardeltamap_bumper, )\
  _(SNfullmaprequest,)    /**used for asking fusionmapper for a full map**/ \
  _(SNladarmeas_roof, )\
  _(SNladarmeas_bumper, )\
  _(SNplannerTabInput, )\
  _(SNplannerTabOutput, )\
	_(SNSampleTabOutput, )	\
	_(SNSampleTabInput, )	\
  _(SNSamplePlotInput, ) \
	_(SNreactiveTabOutput, ) \
	_(SNreactiveTabInput, )	\
	_(SNasimTabOutput, ) \
	_(SNasimTabInput, )	\
	_(SNtrajfollowerTabOutput, ) \
	_(SNtrajfollowerTabInput, )	\
	_(SNastateTabOutput, ) \
	_(SNastateTabInput, )	\
	_(SNfusionmapperTabOutput, ) \
	_(SNfusionmapperTabInput, )	\
	_(SNladarfeederTabOutput, ) \
	_(SNladarfeederTabInput, )	\
  _(SNladarmeas, )\
  _(SNsuperconMessage, ) \
  _(SNsuperconResume, ) \
  _(SNsuperconReverse, ) \
  _(SNfusionElevDeltaMap, )\
  _(SNladarDeltaMapFused, ) \
  _(SNladarDeltaMapStdDev, ) \
  _(SNtrajReverse, ) \
  _(SNladarDeltaMapNum, ) \
  _(SNfusionDeltaMapStdDev, ) \
  _(SNguiToTimberMsg, ) \
  _(SNselectorTraj, ) \
  _(SNcheckerTraj, ) \
  _(SNstereoDeltaMapMean, ) \
  _(SNstereoDeltaMapFused, ) \
  _(SNsuperconTrajfCmd, ) \
  _(SNtrajFstatus, ) \
  _(SNtrajFspeedCapCmd, ) \
  _(SNlowResSpeedDeltaMap, ) \
  _(SNsuperConMapAction, ) \
  _(SNsuperconPlnCmd, ) \
  _(SNsuperconAstateCmd, ) \
  _(last_type, ) /**< do not ever use last_type, it is just a hack to figure out how 
                 many types of msg there are. 
* This enum defines all the message types.  Message types should
                * begin with "SN" */
DEFINE_ENUM(sn_msg, SN_MSG_LIST) 

#define MODULENAME_LIST(_) \
  _(SNMapDisplay, = 0) \
  _(SNmodstart, ) \
  _(SNstereo, ) \
  _(SNladar, ) \
  _(SNfusionmapper, ) \
  _(SNplanner, ) \
  _(SNtrajSelector, ) \
  _(SNtrajChecker, ) \
  _(SNadrive, ) \
  _(SNadrive_commander, ) \
  _(SNadrivelogger, ) \
  _(SNGui, ) \
  _(SNGuimodstart, ) \
  _(SNProcMon, ) \
  _(SNasim, ) \
  _(SNtrajfollower, ) \
  _(SNastate, ) \
  _(SNastate_test, ) \
  _(SNRddfPathGen, ) \
  _(SNModeMan, ) \
  _(SNtrajtalkertestsend, ) \
  _(SNtrajtalkertestrecv, ) \
  _(SNladarfeeder, ) \
  _(SNroadfinding,  ) \
  _(MODmodlist, ) \
  _(MODmark, ) \
  _(MODtimber, ) \
  _(MODtimberClient, ) \
  _(MODstereofeeder, ) \
  _(MODstaticpainter, ) \
  _(MODreactive,  ) \
  _(MODladarfeeder_roof, ) \
  _(MODladarfeeder_bumper, ) \
  _(SNguilogplayer,  ) \
  _(MODbPlanner,  ) \
  _(MODladarFeeder, ) \
  _(MODsupercon, ) \
  _(MODDBS, ) \
  _(ALLMODULES, )

DEFINE_ENUM(modulename, MODULENAME_LIST)   
/** This enum defines module names.  To distinguish the constants from
                * message types, module names should start with "MOD" */


#endif

