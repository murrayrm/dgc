/** This is the trajectory container class. The trajectory stored is a list of
	Northing values, their derivatives, Easting values and their derivatives. The
	number of derivatives stored is given by the m_order member (order =
	maxderivativenumber+1). The number of points stored is hard-limited by
	TRAJ_MAX_LEN. Setting this high will only affect local memory allocation and
	will not slow down anything that sends trajectories over the network, since
	only m_numPoints points would be sent. The actual data is stored as two
	one-dimensional arrays in m_pN, m_pE. Those arrays are indexes by the INDEX
	macro.
*/

#include <ostream>
#include <iostream>
#include "frames/frames.hh"
using namespace std;

#ifndef _TRAJ_H_
#define _TRAJ_H_

#define TRAJ_MAX_LEN  50000

// How close to the path we have to be to be considered "in the path"
#define TRAJ_IN_PATH_RES  2.0

#define INDEX(i, d) ((i) + (d)*TRAJ_MAX_LEN)

struct TrajPoint {
  double n,e,nd,ed,ndd,edd;
  TrajPoint (double,double,double,double,double,double);
};


class CTraj
{
 protected:
  int  		m_numPoints;		// number of points in this trajectory
  double	m_confidence;		// 
													// confidence in the path (-100 to 100). NOT CURRENTLY USED
  int     m_order;        // max derivatives of each point+1 (x,xdot -> 2)
  
  // actual trajectory: northing, easting and their derivatives
  // each array holds at most TRAJ_MAX_LEN*m_order points
  double  *m_pN;
  double  *m_pE;
  
 public:
  /** The constructors allocate memory for the data */
  CTraj(int order=3);
  
  /** If an input stream is specified, the trajectory is filled in from that
    stream. The trajectory file is a white-space-delimited text array of {n,
    ndot, nddot, ..., e, edot, eddot, ...} points (assumed to be SI units).
    Comments can appear in a line as long as the first character is '%' or '#'
  */
  CTraj(int order, istream& inputstream);

  /** If an filename is specified, the trajectory is filled in from that
    file. The trajectory file is a white-space-delimited text array of {n,
    ndot, nddot, ..., e, edot, eddot, ...} points (assumed to be SI units).
    Comments can appear in a line as long as the first character is '%' or '#'
  */
  CTraj(int order, char* pFilename);

  /** This function loads the current traj from a stream */
	void load(istream& inputstream);

  /** Destructor deallocates memory */
  ~CTraj();
  
  /* Assignment operator overload */
  CTraj &operator=(const CTraj &other);

	/* Append operator */
  CTraj &operator+=(const CTraj &other);
  
  CTraj(const CTraj & other);
  
  /** Returns the last Northing value */
  double  lastN();

  /** Returns the last Easting value */
  double  lastE();

  /** Returns the number of points in the structure */
  int     getNumPoints() { return m_numPoints; }

  /** Returns the order of the structure (max derivative number + 1) */
  int     getOrder() { return m_order; }
  
  /** Returns a pointer to the Northing data of specified order */
  double* getNdiffarray(int order) { return m_pN+INDEX(0, order); }

  /** Returns a pointer to the Easting data of specified order */
  double* getEdiffarray(int order) { return m_pE+INDEX(0, order); }
  
  /** Returns the number of bytes stored in the header of the class. The header
    is m_numPoints,m_confidence,m_order (stored at the top of the class). This
    is used when sending the trajectory across a network. The receiving CTraj
    instance copies the header and then copies the actual data. */
  static unsigned int getHeaderSize() { return sizeof(int) + sizeof(double) + sizeof(int); }
  
  /** Returns the index-th Northing value */
  double  getNorthing(int index) { return m_pN[INDEX(index,0)]; }

  /** Returns the index-th Easting value */
  double  getEasting(int index) { return m_pE[INDEX(index,0)]; }

  /** Sets the index-th Northing value */
  void  setNorthing(int index, double val) { m_pN[INDEX(index,0)] = val; }

  /** Sets the index-th Easting value */
  void  setEasting (int index, double val) { m_pE[INDEX(index,0)] = val; }

  /** Sets the index-th Northing,Easting value */
  void  setNorthingEasting (int index, double valN, double valE) { m_pN[INDEX(index,0)] = valN; m_pE[INDEX(index,0)] = valE; }

	/** Set the number of points */
	void  setNumPoints(int numPoints) { m_numPoints = numPoints; }

  /** Returns the diff-th value derivative of the index-th Northing value  */
  double  getNorthingDiff(int index, int diff) { return m_pN[INDEX(index,diff)]; }
  
  /** Returns the diff-th value derivative of the index-th Easting value  */
  double  getEastingDiff(int index, int diff) { return m_pE[INDEX(index,diff)]; }
  
  /** Computes whether the given (N,E) pair is within TRAJ_IN_PATH_RES of the
    path */
  bool    inTraj(double northing, double easting);
  
  /** This function is called before starting to fill in the trajectory */
  void    startDataInput(void);
  
  /** Input the given (N,E) pair into the trajectory */
  void    inputNoDiffs(double northing, double easting);
  
  /** Input the given N, N derivatives, E, E derivatives data into the
    trajectory */
  void    inputWithDiffs(double* northing, double* easting);

  /** Shifts the spatial data ptsToToss points back to the beginning */
	void    shiftNoDiffs(int ptsToToss);
  
  /** Cuts down the current trajectory to be of length desiredDist (if the traj
    is longer), or lengthens it to be desiredDist long */
  void    truncate(double desiredDist);

	/** Prepends the pFrom traj into the current traj */
	void    prepend(CTraj* pFrom);

	/** Appends from point start to point end of pFrom onto the current traj */
	void    append (CTraj* pFrom, int start, int end);

  /** Fills in thetavector with theta(s) where s is the length along the traj
		that goes from 0 to dist with num total points. bias is subtracted from
		every element */
  void    getThetaVector(double dist, int num, double* thetavector, double bias = 0.0);

  /** Fills in speedvector with speed(s) where s is the length along the traj
		that goes from 0 to dist with num total points. bias is subtracted from
		every element */
  void    getSpeedVector(double dist, int num, double* speedvector, double bias = 0.0);

  /** Print out the current trajectory to the given ostream (cout if not
    specified). If less than every point should be printed, that can be
    specified as well.  Column format is [N, dN, ddN, E, dE, ddE], (SI). */
  void    print(ostream& outputstream = cout, int numPoints = -1);

  /** Prints out the speed profile into the ostream specified */
  void    printSpeedProfile(ostream& outputstream = cout);


  /** This funstion applies a speed limit to the path.*/
  void    speed_adjust(double speedlimit,NEcoord* pState,double aspeed);

  /** Returns the index of the closest point in this traj to the provided (N,E) 
   * */
  int getClosestPoint(double n, double e);

  /** Returns the first index of the trajectory that is further forward on the 
   * trajectory by a minimum distance, assuming the trajectory to be piecewise 
   * linear.  If this minimum distance is greater than the distance to the end 
   * of the trajectory, then the index of the last point on the trajectory is 
   * returned. */
  int getPointAhead( int curpoint, double dist );
  
  /** Returns the length of the whole traj if it's assumed to be piecewise 
   * linear. */
  double getLength(void);

	/** Returns the distance between two points */
	double dist(int from, int to);

  /**Enhance the CTraj class with trajectory interpolation*/
  TrajPoint interpolate (NEcoord);
	
	
	
};



#endif // _TRAJ_H_
