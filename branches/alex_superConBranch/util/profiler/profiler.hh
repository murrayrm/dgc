#ifndef PROFILE_HH
#define PROFILE_HH

#ifdef DO_PROFILE

//General class for profiling
//name is always the name of the profile region to use
//multiple profiles can be used in the same code (even overlapping)
//however they all must have different names

class CProfile
{
 public:
//profiles the containing code segment
  CProfile(const char *name) : m_name(name) { start(name); }
  ~CProfile() { stop(); }

  void pause() { pause(m_name); }
  void resume() { resume(m_name); }
  void stop() { stop(m_name); }

 public:
//Control functions
  static void start(const char *name);
  static void resume(const char *name);
  static void pause(const char *name);
  static void stop(const char *name);

 public:
//Accessor functions
  static double get_hz(const char *name); //Get the currently running freq
  static double get_run_avg_hz(const char *name);
  static double get_avg_hz(const char *name);

  static double get_last_time(const char *name);   //Returns last time in s
  static double get_run_avg_time(const char *name);
  static double get_avg_time(const char *name);   //Returns s

  static double get_total_time(const char *name);
  static int get_count(const char *name);
  static double get_process_time();

 public:
//Output functions
  static void print(const char *name);
  static void print();
  static void fprint(const char *filename);

  static void auto_file_save(int interval=1);   //auto-save profile data interval in seconds, 0 to deactivate

 private:
  const char *m_name;
};

#else  // DO_PROFILE

//Dummy class for empty profiling

class CProfile
{
 public:
//profiles the containing code segment
  CProfile(const char *name) { }
  ~CProfile() { }

  void pause() { }
  void resume() { }
  void stop() { }

 public:
//Control functions
  static void start(const char *name) { return; }
  static void resume(const char *name) { return; }
  static void pause(const char *name) { return; }
  static void stop(const char *name) { return; }

 public:
//Accessor functions
  static double get_hz(const char *name) { return 0.0; }
  static double get_run_avg_hz(const char *name) { return 0.0; }
  static double get_avg_hz(const char *name) { return 0.0; }

  static double get_last_time(const char *name) { return 0.0; }
  static double get_run_avg_time(const char *name) { return 0.0; }
  static double get_avg_time(const char *name) { return 0.0; }

  static double get_total_time(const char *name) { return 0.0; }
  static int get_count(const char *name) { return 0; }
  static double get_process_time() { return 0.0; }

 public:
//Output functions
  static void print(const char *name) {}
  static void print() {}
  static void fprint(const char *filename) {}

  static void auto_file_save(int interval=1) {}
};

#endif  // DO_PROFILE

#endif
