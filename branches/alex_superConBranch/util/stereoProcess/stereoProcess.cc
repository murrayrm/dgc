#include "stereoProcess.hh"
#include <fstream>

stereoProcess::stereoProcess() {
  _pairIndex = -1;
  memset(_currentFilename, 0, 256);
  memset(_currentFileType, 0, 10);

  showingDisp = FALSE;
  showingRectLeft = FALSE;
  showingRectRight = FALSE;

  DGCcreateMutex(&_paramsMutex);
}


stereoProcess::~stereoProcess() {

}


int stereoProcess::init(int verboseLevel, char* SVSCalFilename, char* SVSParamsFilename, char* CamParamsFilename, 
			char *baseFilename, char *baseFileType, int num) {
  DGClockMutex(&_paramsMutex);
  _verboseLevel = verboseLevel;
  memcpy(_currentFilename, baseFilename, strlen(baseFilename));
  memcpy(_currentFileType, baseFileType, strlen(baseFileType));
  _pairIndex = num;

  //Initialize the SVS variables
  videoObject = new svsStoredImages();
  processObject = new svsMultiProcess();

  //Load the internal calibration data
  FILE *svsCalFile = NULL;
  svsCalFile = fopen(SVSCalFilename, "r");
  if(svsCalFile != NULL) {
    fclose(svsCalFile);
    videoObject->ReadParams(SVSCalFilename);
    videoObject->Start();
  } else {
    return stereoProcess_NO_FILE;
  }

  //Load the SVS parameter data
  //Variables for getting the SVS parameters
  int corrsize, thresh, ndisp, offx, svsUnique, multi;
 
  //Variables for resizing the image
  int width, height;
  //Variables for using a sub-window of the resized image
  int sub_window_x_offset, sub_window_y_offset, sub_window_width, sub_window_height, use_sub_window_temp;
  bool use_sub_window;
  FILE *svsParamsFile = NULL;

  svsParamsFile = fopen(SVSParamsFilename, "r");
  
  if(svsParamsFile != NULL) {
    fscanf(svsParamsFile, "corrsize: %d\nthresh: %d\nndisp: %d\noffx: %d\nunique: %d\nwidth: %d\nheight: %d\nminpoints: %d\nuse_sub_window: %d\nsub_window_x_offset: %d\nsub_window_y_offset: %d\nsub_window_width: %d\nsub_window_height: %d\nmulti: %d\nblobActive: %d\nblobTresh: %d\nblobDispTol: %d", &corrsize, &thresh, &ndisp, &offx, &svsUnique, &width, &height, &numMinPoints, &use_sub_window_temp, &sub_window_x_offset, &sub_window_y_offset, &sub_window_width, &sub_window_height, &multi, &m_blobActive, &m_blobTresh, &m_blobDispTol);
    
    if(use_sub_window_temp == 1) {
      use_sub_window = TRUE;
    } else {
      use_sub_window = FALSE;
    }

    videoObject->Load(0, 0, NULL, NULL, NULL, NULL, FALSE, FALSE);
    imageObject = videoObject->GetImage(1);
    
    imageObject->dp.corrsize = corrsize;
    imageObject->dp.thresh = thresh;
    imageObject->dp.ndisp = (int)ndisp;
    imageObject->dp.offx = offx;
    imageObject->dp.unique = svsUnique;

    processObject->doIt = multi;

    imageSize = cvSize(width, height);

    subWindow = cvRect(sub_window_x_offset, sub_window_y_offset, sub_window_width, sub_window_height);

    fclose(svsParamsFile);
  } else {
    return stereoSource_NO_FILE;
  }

  //Initialize the frame variable
  double cameraRoll, cameraPitch, cameraYaw;
  XYZcoord cameraOffset(0, 0, 0);
  FILE *camParamsFile = NULL;

  camParamsFile = fopen(CamParamsFilename, "r");

  if(camParamsFile != NULL) {
    fscanf(camParamsFile, "x: %lf\ny: %lf\nz: %lf\npitch: %lf\nroll: %lf\nyaw: %lf",
	   &cameraOffset.X, &cameraOffset.Y, &cameraOffset.Z,
	   &cameraPitch, &cameraRoll, &cameraYaw);
    
    cameraFrame.initFrames(cameraOffset, cameraPitch, cameraRoll, cameraYaw);
    
    fclose(camParamsFile);
  } else {
    return stereoSource_NO_FILE;
  }


  for(int i=0; i < MAX_CAMERAS; i++) {
    tempImages[i] = cvCreateImage(imageSize, IPL_DEPTH_8U, 1);
    stereoImages[i] = cvCreateImage(imageSize, IPL_DEPTH_8U, 1);
  }

  DGCunlockMutex(&_paramsMutex);

  return stereoSource_OK;
}


int stereoProcess::loadPair(stereoPair pair, VehicleState state) {
  return loadPair(pair.left, pair.right, state);
}

int stereoProcess::loadPair(unsigned char *left, unsigned char *right, VehicleState state) {
  cvSetData(tempImages[LEFT_CAM], (char *)left, imageSize.width);
  cvSetData(tempImages[RIGHT_CAM], (char *)right, imageSize.width);

  for(int i=0; i<MAX_CAMERAS; i++) {
    cvCopy(tempImages[i], stereoImages[i]);
  }

  videoObject->Load(stereoImages[LEFT_CAM]->width, stereoImages[LEFT_CAM]->height,
		    (unsigned char*) stereoImages[LEFT_CAM]->imageData,
		    (unsigned char*) stereoImages[RIGHT_CAM]->imageData,
		    NULL, NULL,
		    FALSE, FALSE);

  _currentState = state;
  NEDcoord camPos(_currentState.Northing, _currentState.Easting, _currentState.Altitude);
  cameraFrame.updateState(camPos, _currentState.Pitch, _currentState.Roll, _currentState.Yaw);

  _pairIndex++;

  return stereoProcess_OK;
}


int stereoProcess::calcRect() {
  
  imageObject = videoObject->GetImage(1); //this line of code is beautiful
  
  /*
    videoObject->Load(imageObject->sp.linelen, subWindow.height,
    imageObject->Left() + imageObject->sp.linelen*subWindow.y,
    imageObject->Right() + imageObject->sp.linelen*subWindow.y,
    NULL, NULL,
    FALSE, FALSE);


    imageObject = videoObject->GetImage(1);

    svsSP *mySP;

    mySP = imageObject->GetSP();
    mySP->subwarp = TRUE;
    mySP->subwindow = TRUE;
    mySP->left_ix = subWindow.x;
    mySP->left_iy = subWindow.y;
    mySP->left_width = subWindow.width;
    mySP->left_height = subWindow.height;
    mySP->right_ix = subWindow.x;
    mySP->right_iy = subWindow.y;
    mySP->right_width = subWindow.width;
    mySP->right_height = subWindow.height;
    mySP->linelen = imageSize.width;
    mySP->lines = imageSize.height;

    imageObject->SetSP(mySP);
  */
  if(showingRectLeft) {
    fltk_check();
    rectLeftWindow->DrawImage(imageObject, svsLEFT);
    if(!rectLeftWindow->visible_r()) showingRectLeft = FALSE;
  }
  if(showingRectRight) {
    fltk_check();
    rectRightWindow->DrawImage(imageObject, svsRIGHT);
    if(!rectRightWindow->visible_r()) showingRectRight = FALSE;
  }

  return stereoProcess_OK;
}


int stereoProcess::calcDisp() {
  DGClockMutex(&_paramsMutex);
  
  /*  //ugly hack to swap left and right images
  if (m_blobActive == 2) {
    unsigned char *templeft = imageObject->left;
    imageObject->left = imageObject->right;
    imageObject->right = templeft;
    } */

  processObject->CalcStereo(imageObject);
  DGCunlockMutex(&_paramsMutex);
  
  // blob filter starts here (to be put in a function)

  if (m_blobActive == 1) {
    
    // image width and heigth should not be hardcoded!
    
    dispIm = imageObject->Disparity();
    
    // set the first row and first column to no disparity (temporary solution?)
    for (int j = 0; j < 640; j++) {
      dispIm[j] = -1;
    }
    for (int i = 1; i < 480; i++) {
      dispIm[640*i] = -1;
    }
  
    int k = 0; // current color
    int c[480*640]; // color matrix
    vector< vector<int> > colorTable;

    // begin on second row, second column, since first are cleared
    for (int i = 1; i < 480; i++) {
      for (int j = 1; j < 640; j++) {
	short m = dispIm[640*i+j];
	if (m == -1) {
	  c[640*i+j] = -1;
	  // do nothing
	}
	else {
	  // indexes
	  int ui = 640*(i-1)+j; //up
	  int li = 640*i+j-1; //left
	  int mi = 640*i+j; //current pixel
	
	  // pre-cooked evaluations
	  bool up = false;
	  if ((dispIm[ui] != -1) && (abs(dispIm[ui]-m) < m_blobDispTol)) {
	    up = true; }
	  bool left = false;	
	  if ((dispIm[li] != -1) && (abs(dispIm[li]-m) < m_blobDispTol)) {
	    left = true; }
	
	  if (up && !left) {
	    c[mi] = c[ui];
	    colorTable.at(c[ui]).push_back(mi);
	  }
	  else if (!up && left) {
	    c[mi] = c[li]; 
	    colorTable.at(c[li]).push_back(mi);
	  }
	  else if (up && left) {
	    c[mi] = c[li]; 
	    colorTable.at(c[li]).push_back(mi);
	    if (c[ui] != c[li]) {
	      // now all elements in c == c[ui] should be set to c[li]
	      int oldColor = c[ui];
	      while (!colorTable.at(oldColor).empty()) {
		int change = colorTable.at(oldColor).back();
		colorTable.at(oldColor).pop_back();
		c[change] = c[li];
		colorTable.at(c[li]).push_back(change); 
	      }
	      c[ui] = c[li];
	    }
	  }
	  else {
	    c[mi] = k;
	    vector<int> v;
	    colorTable.push_back(v);
	    colorTable.at(k).push_back(mi);
	    k++;
	  }
	}
      }
    }
    
    // change this stupid blob-removing algorithm to something that goes through the vectors with sizes under threshold!
    
    // count blob sizes
    int count[k+1]; 
    
    // use memset instead!
    for (int i = 1; i <= k; i++) {
      count[i] = 0; }
    for (int i = 1; i < 480; i++) {
      for (int j = 1; j < 640; j++) {
	count[c[640*i+j]] = count[c[640*i+j]] + 1;
      }
    }
  
    // remove small blobs
    for (int i = 1; i < 480; i++) {
      for (int j = 1; j < 640; j++) {
	if (count[c[640*i+j]] < m_blobTresh) {
	  dispIm[640*i+j] = -1;
	}
      }
    }
  
  
    /* // the following code outputs the disparity image to a file which can be imported by MATLAB.	
      cout << "disparity width: " << imageObject->dp.dwidth << ", height: " << imageObject->dp.dheight << endl;
	ofstream tempFile("/tmp/blob.txt", ios::app);
	tempFile << "[";
	for (int i=0; i<480; i++) { //imageObject->dp.dheight; i++) {
	for (int j=0; j<640; j++) { //imageObject->dp.dwidth; j++) {
	//tempFile << "x: " << x << ", y: " << y << endl;  
	if (j != 0) {
	tempFile << " ";
	} 
	tempFile << dispIm[640*i+j];//imageObject->dp.dwidth*i+j];  
	}
	if (i+1 < 480) //imageObject->dp.dheight)
	tempFile << ";" << endl;
	}
	tempFile << "];" << endl;
	cout << "done!" << endl;
	tempFile.close();
	usleep(100000000);
    */
  
  
    // blob filter ends here
  
  }

  if(showingDisp) {
    fltk_check();
    dispWindow->DrawImage(imageObject, svsDISPARITY);
    if(!dispWindow->visible_r()) showingDisp = FALSE;
  }
  
  return stereoProcess_OK;
}

  
int stereoProcess::save() {
  int saveRectStatus = saveRect();
  int saveDispStatus = saveDisp();
  if(saveRectStatus!=stereoProcess_OK) {
    return saveRectStatus;
  }
  if(saveDispStatus!=stereoProcess_OK) {
    return saveDispStatus;
  }

  return stereoProcess_OK;
}


int stereoProcess::save(char *baseFilename, char *baseFileType, int num) {
  int saveRectStatus = saveRect(baseFilename, baseFileType, num);
  int saveDispStatus = saveDisp(baseFilename, baseFileType, num);
  if(saveRectStatus!=stereoProcess_OK) {
    return saveRectStatus;
  }
  if(saveDispStatus!=stereoProcess_OK) {
    return saveDispStatus;
  }

  return stereoProcess_OK;
}


int stereoProcess::saveDisp() {
  return saveDisp(_currentFilename, _currentFileType, _pairIndex);
}


int stereoProcess::saveDisp(char *baseFilename, char *baseFileType, int num) {
  char fullFilename[256];

  sprintf(fullFilename, "%s%.4dDisp.%s", baseFilename, num, baseFileType);

  imageObject->SaveDisparity(fullFilename, TRUE);

  return stereoProcess_OK;
}


int stereoProcess::saveRect() {
  return saveRect(_currentFilename, _currentFileType, _pairIndex);
}


int stereoProcess::saveRect(char *baseFilename, char *baseFileType, int num) {
  char fullFilename[256];

  sprintf(fullFilename, "%s%.4dRect", baseFilename, num);

  imageObject->SaveToFile(fullFilename);
    
  return stereoProcess_OK;
}



int stereoProcess::show(int width, int height) {
  showRect(width, height);
  showDisp(width, height);
  return stereoProcess_OK;
}


int stereoProcess::showDisp(int width, int height) {
  if(width == 0) width = imageSize.width;
  if(height == 0) height = subWindow.height;

  if(!showingDisp) {
    dispWindow = new svsWindow(100,100,width,height);
    showingDisp = TRUE;
  }
  dispWindow->show();

  return stereoProcess_OK;
}


int stereoProcess::showRect(int width, int height) {
  int showRectLeftStatus = showRectLeft();
  int showRectRightStatus = showRectRight();
  if(showRectLeftStatus!=stereoProcess_OK) {
    return showRectLeftStatus;
  }
  if(showRectRightStatus!=stereoProcess_OK) {
    return showRectRightStatus;
  }

  return stereoProcess_OK;
}


int stereoProcess::showRectLeft(int width, int height) {
  if(!showingRectLeft) {
    rectLeftWindow = new svsWindow(0,0, imageSize.width, subWindow.height);
    showingRectLeft = TRUE;
  }
  rectLeftWindow->show();

  return stereoProcess_OK;
}


int stereoProcess::showRectRight(int width, int height) {
  if(!showingRectRight) {
    rectRightWindow = new svsWindow(50, 50, imageSize.width, subWindow.height);
    showingRectRight = TRUE;
  }
  rectRightWindow->show();

  return stereoProcess_OK;
}


unsigned char* stereoProcess::disp() {
  fprintf(stderr, "stereoProcess::disp() not yet implemented\n");
  return NULL;
}


unsigned char* stereoProcess::rectLeft() {
  fprintf(stderr, "stereoProcess::rectLeft() not yet implemented\n");
  return NULL;
}


unsigned char* stereoProcess::rectRight() {
  fprintf(stderr, "stereoProcess::rectRight() not yet implemented\n");
  return NULL;
}


stereoPair stereoProcess::rectPair() {
  stereoPair temp;
  temp.left = rectLeft();
  temp.right = rectRight();
  return temp;
}


int stereoProcess::numPoints() {
  return imageObject->numPoints;
}


bool stereoProcess::validPoint(int i) {
  if(i < numPoints() && i >= 0) {
    //return imageObject->V[i];
    return (imageObject->pts3D[i].A > 0.0); // svsupgrade
  } else {
    return FALSE;
  }
}


NEDcoord stereoProcess::UTMPoint(int i) {
  NEDcoord retVal(0, 0, 0);
  XYZcoord cameraPoint;

  if(validPoint(i)) {
    //According to the SVS documentation:
    //The camera's Z-axis is forward - so our X-axis
    //    cameraPoint.X = imageObject->Z[i]; // /1000;
    cameraPoint.X = imageObject->pts3D[i].Z; // svsupgrade
    //The camera's X axis is right - so our Y-axis
    //    cameraPoint.Y = imageObject->X[i]; // /1000;
    cameraPoint.Y = imageObject->pts3D[i].X; // svsupgrade    
    //The camera's Y-axis is down - so our Z-axis
    //    cameraPoint.Z = imageObject->Y[i]; // /1000;
    cameraPoint.Z = imageObject->pts3D[i].Y; // svsupgrade  
    retVal = cameraFrame.transformS2N(cameraPoint);
  }

  return retVal;
}


XYZcoord stereoProcess::Point(int i) {
  XYZcoord retVal(0, 0, 0);

  if(validPoint(i)) {
    /* retVal.X = imageObject->X[i];
       retVal.Y = imageObject->Y[i];
       retVal.Z = imageObject->Z[i]; */
    retVal.X = imageObject->pts3D[i].X; //svsupgrade
    retVal.Y = imageObject->pts3D[i].Y;
    retVal.Z = imageObject->pts3D[i].Z;

  }

  return retVal;
}


bool stereoProcess::SinglePoint(int x, int y, NEDcoord* resultPoint) {
  XYZcoord tempPoint(0, 0, 0);
  NEDcoord transformedPoint(0, 0, 0);

  if(x > 0 && x < stereoImages[0]->width &&
     y > 0 && y < stereoImages[0]->height) {
    /*
      ofstream tempFile("/tmp/singlePoint.txt", ios::app);
      tempFile << "x is " << x << " and y is " << y << endl;
      tempFile.close();
    */
    if(processObject->CalcPoint3D(x, y, imageObject, &tempPoint.Y, &tempPoint.Z, &tempPoint.X)) {
      tempPoint.X = tempPoint.X; // /1000;
      tempPoint.Y = tempPoint.Y; // /1000;
      tempPoint.Z = tempPoint.Z; // /1000;
      
      transformedPoint = cameraFrame.transformS2N(tempPoint);

      
      (resultPoint->N) = transformedPoint.N;
      (resultPoint->E) = transformedPoint.E;
      (resultPoint->D) = transformedPoint.D;
      
      return true;
    }
    /*
      } else {
      cout << "ohbou x is " << x << " y is " << y 
      << " and width is " << stereoImages[0]->width
      << " and height is " << stereoImages[0]->height << endl;
    */
  }

  return false;
}


bool stereoProcess::calc3D() {
  //return processObject->Calc3D(imageObject, subWindow.x, subWindow.y, subWindow.width, subWindow.height);
  return processObject->Calc3D(imageObject);
}


bool stereoProcess::SingleRelativePoint(int x, int y, XYZcoord* resultPoint) {
  XYZcoord tempPoint(0, 0, 0);
  XYZcoord transformedPoint(0, 0, 0);

  if(x > 0 && x < stereoImages[0]->width &&
     y > 0 && y < stereoImages[0]->height) {
    /*
      ofstream tempFile("/tmp/singleRelativePoint.txt", ios::app);
      tempFile << "x is " << x << " and y is " << y << endl;
      tempFile.close();
    */
    if(processObject->CalcPoint3D(x, y, imageObject, &tempPoint.Y, &tempPoint.Z, &tempPoint.X)) {
      tempPoint.X = tempPoint.X; // /1000;
      tempPoint.Y = tempPoint.Y; // /1000;
      tempPoint.Z = tempPoint.Z; // /1000;
      
      transformedPoint = cameraFrame.transformS2B(tempPoint);
      
      (resultPoint->X) = transformedPoint.X;
      (resultPoint->Y) = transformedPoint.Y;
      (resultPoint->Z) = transformedPoint.Z;
      
      return true;
    } 
  }

  return false;
}

VehicleState stereoProcess::currentState() {
  return _currentState;
}


int stereoProcess::pairIndex() {
  return _pairIndex;
}


int stereoProcess::resetPairIndex() {
  _pairIndex = 0;

  return stereoProcess_OK;
}


int stereoProcess::corrsize() {
  return imageObject->dp.corrsize;
}


int stereoProcess::thresh() {
  return imageObject->dp.thresh;
}


int stereoProcess::ndisp() {
  return imageObject->dp.ndisp;
}


int stereoProcess::offx() {
  return imageObject->dp.offx;
}


int stereoProcess::svsUnique() {
  return imageObject->dp.unique;
}


int stereoProcess::multi() {
  return processObject->doIt;
}


int stereoProcess::setSVSParams(int corrsize, int thresh, int ndisp, int offx, int svsUnique, int multi, int subwindow_x_off, int subwindow_y_off, int subwindow_width, int subwindow_height) {
  DGClockMutex(&_paramsMutex);
  if(corrsize == -1) corrsize = imageObject->dp.corrsize;
  if(thresh == -1) thresh = imageObject->dp.thresh;
  if(ndisp == -1) ndisp = imageObject->dp.ndisp;
  if(offx == -1) offx = imageObject->dp.offx;
  if(svsUnique == -1) svsUnique = imageObject->dp.unique;
  if(multi == -1) multi = processObject->doIt;

  if(subwindow_x_off == -1) subwindow_x_off = subWindow.x;
  if(subwindow_y_off == -1) subwindow_y_off = subWindow.y;
  if(subwindow_width == -1) subwindow_width = subWindow.width;
  if(subwindow_height == -1) subwindow_height = subWindow.height;

  imageObject->dp.corrsize = corrsize;
  imageObject->dp.thresh = thresh;
  imageObject->dp.ndisp = ndisp;
  imageObject->dp.offx = offx;
  imageObject->dp.unique = svsUnique;
  processObject->doIt = multi;
  subWindow.x = subwindow_x_off;
  subWindow.y = subwindow_y_off;
  subWindow.width = subwindow_width;
  subWindow.height = subwindow_height;

  DGCunlockMutex(&_paramsMutex);
  return stereoProcess_OK;
}

int stereoProcess::blobActive() {
  return m_blobActive;
}

int stereoProcess::blobTresh() {
  return m_blobTresh;
}

int stereoProcess::blobDispTol() {
  return m_blobDispTol;
}

int stereoProcess::setBlobParams(int blobActive, int blobTresh, int blobDispTol) {
  m_blobActive = blobActive;
  m_blobTresh = blobTresh;
  m_blobDispTol = blobDispTol;
}
