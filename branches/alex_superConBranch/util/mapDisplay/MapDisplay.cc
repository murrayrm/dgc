#include "MapDisplay.hh"
// vehicle-independent state struct location.
#include "VehicleState.hh"
#include "DGCutils"

#define DEBUG_LEVEL  0

MapWidget::MapWidget(MapConfig * mc) : m_config(mc),
    m_cmap_draw_area( sigc::mem_fun(*this, &MapWidget::_cmap_init), 
                      sigc::mem_fun(*this, &MapWidget::_cmap_reshape), 
                      sigc::mem_fun(*this, &MapWidget::_cmap_draw),
                      sigc::mem_fun(*this, &MapWidget::_cmap_button) ),
    m_label_cmap_info("[Click on a cell to see its UTM coordinates and value]"),
    m_colormap_draw_area( sigc::mem_fun(*this, &MapWidget::_colormap_init), 
                          sigc::mem_fun(*this, &MapWidget::_colormap_reshape), 
                          sigc::mem_fun(*this, &MapWidget::_colormap_draw),
                          sigc::mem_fun(*this, &MapWidget::_colormap_button) ),
    // For displaying the Colormap Legend
    m_label_colormap_maxvalue("Max"),
    m_label_colormap_midvalue("Mid"),
    m_label_colormap_minvalue("Min"),
    // Color Chart Options 
    m_frame_colormap_options("Colormap Options"),
    m_label_colormap_center("Center:  "),
    m_label_colormap_range("Range: "),
    m_check_colormap_log_scale("Use Log Scale"),
    m_label_colormap_min_range("Min Range: "),
    m_spin_colormap_min_range(0.001, 3),
    m_spin_colormap_range(0.001, 3),
    m_spin_colormap_center(0.001, 3),
    m_check_colormap_auto_range("Auto Range"),
    m_check_colormap_auto_center("Auto Center"),
    // CMap Options
    m_frame_cmap_options("Map Options"),
    m_frame_cmap("CMap Options"),
    m_label_cmap_layer("Layer: "),
    m_check_draw_cmap_border( "Draw CMap Border" ),
    m_label_zoom_scale("Zoom: "),
    m_label_gridlines_spacing("Gridlines every "),
    m_check_draw_cmap( "Draw CMap" ),
    m_button_clear_cmap("Clear Current Layer"),
    m_button_clear_all_layers("Clear All Layers"),
    m_check_draw_gridlines("Grid"),
    m_check_draw_sensors("Sensors"),
    m_check_draw_paths ("Paths"),
    m_check_draw_rddf   ("RDDF"),
    m_check_draw_waypoints( "Waypts" ),
    m_check_draw_info   ("Info"),
    m_spin_zoom_scale(0.1, 1),
    m_spin_cmap_layer(0, 0),

    // Path Options    
    m_frame_paths("Path Options"),
    m_toggle_draw_current_path( "Toggle Current Path" ),
    m_label_path("Paths: "),
    m_spin_path(0,0),    

    //Logging Options
    m_button_saveLog("Write Log"),
    m_button_findCMap("Find CMap"),
    map_utm_to_screen( RECTANGLE<double> (-1, 1, -1, 1), 
                       RECTANGLE<double> (-1, 1, -1, 1) )
{
  //m_button_zoom_in.signal_clicked().connect( sigc::mem_fun(*this, &MapWidget::zoomin) ); //zoom in '+' button 
  //m_button_zoom_out.signal_clicked().connect( sigc::mem_fun(*this, &MapWidget::zoomout) ); //zoom out '-' button 

  this->pack_start(m_overall_gui_box, Gtk::PACK_EXPAND_WIDGET, 0);

  m_overall_gui_box.pack_start(m_cmap_box, Gtk::PACK_EXPAND_WIDGET, 0);
  m_overall_gui_box.pack_start(m_colormap_box, Gtk::PACK_SHRINK, 0);
  m_overall_gui_box.pack_start(m_options_box, Gtk::PACK_SHRINK, 0);

  m_cmap_box.pack_start(m_cmap_draw_area, Gtk::PACK_EXPAND_WIDGET, 0);
  m_cmap_box.pack_start(m_label_cmap_info, Gtk::PACK_SHRINK, 0);

  m_colormap_box.pack_start(m_label_colormap_maxvalue, Gtk::PACK_SHRINK, 0);
  m_colormap_box.pack_start(m_colormap_draw_area, Gtk::PACK_EXPAND_WIDGET, 0);
  m_colormap_box.pack_start(m_label_colormap_minvalue, Gtk::PACK_SHRINK, 0);
  
  m_frame_colormap_options.set_size_request(-1, -1);
  
  m_options_box.pack_start(m_frame_colormap_options, Gtk::PACK_SHRINK, 5);
  m_options_box.pack_start(m_frame_cmap_options, Gtk::PACK_SHRINK, 5);

  m_frame_colormap_options.add(m_vbox_colormap_options);
  m_vbox_colormap_options.pack_start(m_label_colormap_center, Gtk::PACK_SHRINK, 0);
  m_vbox_colormap_options.pack_start(m_spin_colormap_center, Gtk::PACK_SHRINK, 0);

  m_vbox_colormap_options.pack_start(m_label_colormap_range, Gtk::PACK_SHRINK, 0);
  m_vbox_colormap_options.pack_start(m_spin_colormap_range, Gtk::PACK_SHRINK, 0);

  m_vbox_colormap_options.pack_start(m_label_colormap_min_range, Gtk::PACK_SHRINK, 0);
  m_vbox_colormap_options.pack_start(m_spin_colormap_min_range, Gtk::PACK_SHRINK, 0);
  
  m_vbox_colormap_options.pack_start(m_check_colormap_auto_range, Gtk::PACK_SHRINK, 0);
  m_vbox_colormap_options.pack_start(m_check_colormap_auto_center, Gtk::PACK_SHRINK, 0);
  //m_vbox_colormap_options.pack_start(m_check_colormap_log_scale, Gtk::PACK_SHRINK, 0);

  m_frame_cmap_options.add(m_vbox_cmap_options);
  m_vbox_cmap_options.pack_start(m_frame_cmap, Gtk::PACK_SHRINK, 5);
  m_frame_cmap.add(m_vbox_cmap);
  //m_vbox_cmap.pack_start(m_hbox_draw_cmap_checks, Gtk::PACK_SHRINK, 0);
  m_vbox_cmap.pack_start(m_check_draw_cmap, Gtk::PACK_EXPAND_WIDGET, 0);
  m_vbox_cmap.pack_start(m_check_draw_cmap_border, Gtk::PACK_EXPAND_WIDGET, 0);

  m_vbox_cmap.pack_start(m_hbox_cmap_layer, Gtk::PACK_SHRINK, 0);
  m_hbox_cmap_layer.pack_start(m_label_cmap_layer, Gtk::PACK_SHRINK, 0);
  m_hbox_cmap_layer.pack_start(m_spin_cmap_layer, Gtk::PACK_EXPAND_WIDGET, 0);
  m_spin_cmap_layer.signal_value_changed().connect(sigc::mem_fun(this, &MapWidget::_print_layer_name));

  m_vbox_cmap.pack_start(m_button_clear_cmap, Gtk::PACK_SHRINK, 0);
  m_button_clear_cmap.signal_clicked().connect(sigc::mem_fun(this,&MapWidget::_clear_current_layer));
  m_vbox_cmap.pack_start(m_button_clear_all_layers, Gtk::PACK_SHRINK, 0);
  m_button_clear_all_layers.signal_clicked().connect(sigc::mem_fun(this,&MapWidget::_clear_all_layers));
  
  //initialize path options
  m_vbox_cmap_options.pack_start(m_frame_paths,Gtk::PACK_SHRINK, 5);
  m_frame_paths.add(m_vbox_path);
  m_vbox_path.pack_start(m_hbox_path, Gtk::PACK_SHRINK, 0);
  m_hbox_path.pack_start(m_label_path, Gtk::PACK_SHRINK, 0);
  m_hbox_path.pack_start(m_spin_path, Gtk::PACK_EXPAND_WIDGET, 0);
  m_vbox_path.pack_start(m_toggle_draw_current_path, Gtk::PACK_SHRINK, 0);
  m_toggle_draw_current_path.signal_clicked().connect(sigc::mem_fun(this,&MapWidget::togglePath));

  m_vbox_cmap_options.pack_start(m_hbox_cmap_checks, Gtk::PACK_SHRINK, 0);
  m_hbox_cmap_checks.pack_start(m_vbox_cmap_checks_col1, Gtk::PACK_EXPAND_WIDGET, 0);
  m_hbox_cmap_checks.pack_start(m_vbox_cmap_checks_col2, Gtk::PACK_EXPAND_WIDGET, 0);

  m_vbox_cmap_checks_col1.pack_start(m_check_draw_gridlines, Gtk::PACK_SHRINK, 0);
  m_vbox_cmap_checks_col1.pack_start(m_check_draw_sensors, Gtk::PACK_SHRINK, 0);
  m_vbox_cmap_checks_col1.pack_start(m_check_draw_waypoints, Gtk::PACK_SHRINK, 0);
  m_vbox_cmap_checks_col2.pack_start(m_check_draw_rddf, Gtk::PACK_SHRINK, 0);
  m_vbox_cmap_checks_col2.pack_start(m_check_draw_paths, Gtk::PACK_SHRINK, 0);
  m_vbox_cmap_checks_col2.pack_start(m_check_draw_info, Gtk::PACK_SHRINK, 0);
  m_vbox_cmap_options.pack_start(m_hbox_cmap_zoom, Gtk::PACK_SHRINK, 0);
  m_hbox_cmap_zoom.pack_start(m_label_zoom_scale, Gtk::PACK_SHRINK, 0);
  m_hbox_cmap_zoom.pack_start(m_spin_zoom_scale, Gtk::PACK_EXPAND_WIDGET, 0);
  //m_vbox_cmap_options.pack_start(m_button_zoom_in, Gtk::PACK_SHRINK, 0);
  //m_vbox_cmap_options.pack_start(m_button_zoom_out, Gtk::PACK_SHRINK, 0);
  m_vbox_cmap_options.pack_start(m_label_gridlines_spacing, Gtk::PACK_SHRINK, 0);

  m_vbox_cmap_options.pack_start(m_button_saveLog, Gtk::PACK_SHRINK, 0);
  m_button_saveLog.signal_clicked().connect(sigc::mem_fun(m_config,&MapConfig::activate_writeLogSig));
  m_vbox_cmap_options.pack_start(m_button_findCMap, Gtk::PACK_SHRINK, 0);
  m_button_findCMap.signal_clicked().connect(sigc::mem_fun(this,&MapWidget::findCMap));
  
  // Initialization of Colormap Widgets
  m_spin_colormap_center.set_range(-1000000, 1000000);
  m_spin_colormap_center.set_value(0);
  m_spin_colormap_center.set_increments(0.001, 1);
  
  m_spin_colormap_range.set_range(0.1, 1000000);
  m_spin_colormap_range.set_value(1);
  m_spin_colormap_range.set_increments(0.001, 1);
  
  m_spin_colormap_min_range.set_range(0.1, 1000000);
  m_spin_colormap_min_range.set_value(0.1);
  m_spin_colormap_min_range.set_increments(0.001, 1);
  
  // Initialization of CMap widgets
  m_spin_cmap_layer.set_range( 0, 0 );
  m_spin_cmap_layer.set_increments(1, 1);
  m_spin_path.set_range( 0, 0 );
  m_spin_path.set_increments(1, 1);
  m_spin_zoom_scale.set_range( -100, 100 );
  m_spin_zoom_scale.set_increments( 0.1, 0.5 );
 
  // Initialization of check boxes (comment out if you want them off)
  m_check_draw_cmap.set_active();
  m_check_draw_cmap_border.set_active();
  m_check_draw_gridlines.set_active();
  m_check_draw_sensors.set_active();
  m_check_draw_paths.set_active();
  m_check_draw_rddf.set_active();
//  m_check_draw_waypoints.set_active();
//  m_check_draw_info.set_active();
  m_check_colormap_auto_range.set_active();
  m_check_colormap_auto_center.set_active();
  
  // Connect a notifier for when the cmap gets changed
  m_config->add_cmap_notifier( sigc::mem_fun(*this, &MapWidget::notify_cmap_pointer_change) );
  // notify now
  notify_cmap_pointer_change();
  // Connect a notifier for when stuff gets updated
  m_config->add_update_notify( sigc::mem_fun(*this, &MapWidget::notify_update) );
  // Add a functor for converting cmap to pixbuf
  m_cmap_to_pixbuf.connect( sigc::ptr_fun(&CMapToPixbuf) );

  // Pack the widget
  CMap* m_cmap = m_config->get_cmap();
  if( m_cmap ) {
    m_cmap_draw_area.set_size_request( m_cmap_pixbuf->get_width(), 
                                       m_cmap_pixbuf->get_height() );    
    m_colormap_draw_area.set_size_request( 50, m_cmap_pixbuf->get_height() );

    m_layerOptions = new ColormapOptions[m_cmap->getNumLayers()];
    for(int i=0; i < m_cmap->getNumLayers(); i++) {
      m_layerOptions[i].center = get_colormap_center();
      m_layerOptions[i].range = get_colormap_range();
      m_layerOptions[i].autoRange = get_colormap_use_auto_range();
      m_layerOptions[i].autoCenter = get_colormap_use_auto_center();
    }
    m_lastLayer = 0;
  } else {
    int width  = 300;
    int height = 300;
    m_cmap_draw_area.set_size_request( width, height );    
    m_colormap_draw_area.set_size_request( 50, height );
  }  
}


// Functions for redrawing the CMap
void MapWidget::_cmap_init() 
{
  // Initialized fonts for rendering
  {
    m_Font12ListBase = glGenLists (128);
    Pango::FontDescription font12_desc("courier 12");
    Glib::RefPtr<Pango::Font> font12 =
      Gdk::GL::Font::use_pango_font(font12_desc, 0, 128, m_Font12ListBase);
    if (!font12)
    {
      std::cerr << "*** Can't load font courier 12" << std::endl;
      Gtk::Main::quit();
    }
    Pango::FontMetrics font12_metrics = font12->get_metrics();
    m_Font12Height = font12_metrics.get_ascent() + font12_metrics.get_descent();
    m_Font12Height = PANGO_PIXELS(m_Font12Height);
    m_Font12HalfHeight = (double) m_Font12Height / 2.0;

    // AND 36 HEIGHT FONT
    m_Font36ListBase = glGenLists (128);
    Pango::FontDescription font36_desc("courier 36");
    Glib::RefPtr<Pango::Font> font36 =
      Gdk::GL::Font::use_pango_font(font36_desc, 0, 128, m_Font36ListBase);
    if (!font36)
      {
  std::cerr << "*** Can't load font courier 36" << std::endl;
  Gtk::Main::quit();
      }
    Pango::FontMetrics font36_metrics = font36->get_metrics();
    m_Font36Height = font36_metrics.get_ascent() + font36_metrics.get_descent();
    m_Font36Height = PANGO_PIXELS(m_Font36Height);
    m_Font36HalfHeight = (double) m_Font36Height / 2.0;
  }
}

void MapWidget::_cmap_reshape(int w, int h) 
{    
  glViewport( 0, 0, (GLsizei) w, (GLsizei) h );
  
  glMatrixMode ( GL_PROJECTION );
  glLoadIdentity();
    
  gluOrtho2D( 0.0, (GLdouble) w, 0.0, (GLdouble) h );
  //  glMatrixMode( GL_MODELVIEW );
  //  glLoadIdentity();    
}

void MapWidget::_cmap_draw(int width, int height) 
{
  // Lock the configuration struct
  Glib::RecMutex::Lock lock( m_config->m_mutex );

  CMap * m_cmap = m_config->get_cmap();
	pthread_mutex_t* cmap_mutex = m_config->get_cmap_mutex();
  
  // Clear entire window first
  RGBA_COLOR tmp_color = m_config->m_clear_color;
  glClearColor( tmp_color.r, tmp_color.g, tmp_color.b, tmp_color.a );
  glClear(GL_COLOR_BUFFER_BIT);
  glLineWidth( m_config->m_line_width );  
  
  RECTANGLE<int> widget_dimensions(0, width, 0, height);
  
  // // cout << "Widget Dimensions " << widget_dimensions << endl;
  
  whitespace = RECTANGLE<double>(0);
  
  // if need whitespace  
  if( m_check_draw_info.get_active() )       
    whitespace = RECTANGLE<int> (0,70, 30, 0);
  
  RECTANGLE<int> screen_dimensions(0, widget_dimensions.width()  - whitespace.left - whitespace.right,
           0, widget_dimensions.height() - whitespace.top  - whitespace.bottom);
  
  // // cout << "Screen Dimensions " << screen_dimensions << endl;
  
  RECTANGLE<int> cmap_dimensions;
  if( m_cmap) cmap_dimensions = RECTANGLE<int>( 0, m_cmap->getNumRows(), 0, m_cmap->getNumCols() ); 
  
  // // cout << "CMap Dimensions " << cmap_dimensions << endl;
  
  RECTANGLE<double> scaled_pixmap_dimensions = cmap_dimensions;
  RECTANGLE<double> cmap_utm_coordinates;
  

	DGClockMutex(cmap_mutex);

  if( m_cmap ) 
    {
      cmap_utm_coordinates = RECTANGLE<double> ( m_cmap->getWindowBottomLeftUTMEasting(),
             m_cmap->getWindowTopRightUTMEasting(),
             m_cmap->getWindowBottomLeftUTMNorthing(),
             m_cmap->getWindowTopRightUTMNorthing() );
    }
  else 
    {
      cmap_utm_coordinates = RECTANGLE<double> ( -1, 1, -1, 1 );  
    }
  
  
  // Determine the "standard" number of pixels per meter
  double std_pixels_per_meter;
  // CMap sets scale a bit differently if we are using it
  // This option is currently disabled by the "&& 0" at the end.
  if ( ! (m_cmap && m_check_draw_cmap.get_active() && 0) )
  {
    std_pixels_per_meter = 5.0;
  } 
  else
  {
    scaled_pixmap_dimensions 
    = screen_dimensions.fit_aspect_ratio_inside( cmap_dimensions.aspect_ratio() );
      
    // cout << "Scaled Pixmap Dimensions " << scaled_pixmap_dimensions << endl;
      
    std_pixels_per_meter 
            = (double) (scaled_pixmap_dimensions.width()) / 
              (double) (m_cmap->getWindowTopRightUTMEasting() -
                        m_cmap->getWindowBottomLeftUTMEasting() );
  }
  
  // Multiply by the zoom factor
  double pixels_per_meter =  std_pixels_per_meter * pow(10.0, m_spin_zoom_scale.get_value());
  
  // // cout << "Pixels per meter = " << pixels_per_meter << endl;
  
  // Find the center of the map
  POINT2D<double> utm_screen_center;
  if( m_config->m_drop_anchor ) 
  {
    utm_screen_center = POINT2D<double> (m_config->m_anchor_pos.E, m_config->m_anchor_pos.N);
  }
  else 
  {
    VehicleState * ss = m_config->get_state_struct();
    if( ss ) 
    {
      utm_screen_center = POINT2D<double>(ss->Easting_rear(), ss->Northing_rear());
    }
    else 
    {
      // // cout << "TODO: GRAB STATE FROM STATE ESTIMATOR" << endl;
      utm_screen_center = POINT2D<double>(0,0);
    }
  }
  
  // Find the UTM coordinates of the corners of the screen
  RECTANGLE<double> utm_screen_coordinates 
    = RECTANGLE<double>::centered_at( utm_screen_center,
              (double) screen_dimensions.width()  / pixels_per_meter,
              (double) screen_dimensions.height() / pixels_per_meter );

  // cout << "UTM Screen Coordinates " << utm_screen_coordinates << endl;



  map_utm_to_screen = RECTANGLE_MAP<double, double> (utm_screen_coordinates, screen_dimensions);
  

  // ***************************************************
  // BEGIN -- DRAW CMAP
  // ***************************************************
  if ( m_cmap && m_check_draw_cmap.get_active() ) 
  {
    // Map the CMap onto the screen
    
    if(DEBUG_LEVEL > 0)
    {
      printf("CMap UTM Coordinates      = % 12.3f % 12.3f % 12.3f % 12.3f\n",
             cmap_utm_coordinates.left, cmap_utm_coordinates.right,
             cmap_utm_coordinates.bottom, cmap_utm_coordinates.top);  
      // cout << "CMap UTM Coordinates " << cmap_utm_coordinates << endl;
    } 
    RECTANGLE<double> cmap_screen_coordinates 
      = map_utm_to_screen.map_rectangle( cmap_utm_coordinates );
    
    if(DEBUG_LEVEL > 0)
    {
      printf("CMap Screen Coordinates   = % 12.3f % 12.3f % 12.3f % 12.3f\n",
             cmap_screen_coordinates.left, cmap_screen_coordinates.right,
             cmap_screen_coordinates.bottom, cmap_screen_coordinates.top);  
      // cout << "CMap Screen Coordinates " << cmap_screen_coordinates << endl;
    }
    
    // Crop the CMap to the visible section only
    RECTANGLE<double> cropped_cmap_screen_coordinates
      = cmap_screen_coordinates.intersection( screen_dimensions );
    
    if(DEBUG_LEVEL > 0)
    {
      printf("Cropped CMap Screen Coord = % 12.3f % 12.3f % 12.3f % 12.3f\n",
             cropped_cmap_screen_coordinates.left, cropped_cmap_screen_coordinates.right,
             cropped_cmap_screen_coordinates.bottom, cropped_cmap_screen_coordinates.top);  
      // cout << "Cropped CMap Screen Coordinates " << 
      // cropped_cmap_screen_coordinates << endl;
    }

    RECTANGLE_MAP<double, double> map_cmap_image( cmap_screen_coordinates, 
                                                  cmap_dimensions );

    RECTANGLE<double> cmap_image_coordinates_double 
      = map_cmap_image.map_rectangle(cropped_cmap_screen_coordinates);
    
    RECTANGLE<int> cmap_image_coordinates = cmap_image_coordinates_double;

    cmap_image_coordinates.left = lround(ceil(cmap_image_coordinates_double.left));
    cmap_image_coordinates.right = lround(floor(cmap_image_coordinates_double.right));
    cmap_image_coordinates.bottom = lround(ceil(cmap_image_coordinates_double.bottom));
    cmap_image_coordinates.top = lround(floor(cmap_image_coordinates_double.top));

    if(DEBUG_LEVEL > 0)
    {
      printf("CMap Image Coordinates    = % 12d % 12d % 12d % 12d\n",
             cmap_image_coordinates.left, cmap_image_coordinates.right,
             cmap_image_coordinates.bottom, cmap_image_coordinates.top);  

      printf("CMap Image Double         = % 12.3f % 12.3f % 12.3f % 12.3f\n\n",
             cmap_image_coordinates_double.left, cmap_image_coordinates_double.right,
             cmap_image_coordinates_double.bottom, cmap_image_coordinates_double.top);  
      // cout << "CMap ICD " << cmap_image_coordinates_double << endl;
    }
    
    // Create the new cropped image
    m_cmap_to_pixbuf(this); // Convert CMap to Pixbuf
    if(m_cmap_pixbuf == 0) 
    {
      printf("[%s:%d] m_cmap_to_pixbuf returned 0. skipping CMap display.", __FILE__,__LINE__);
    }
    else
    {
      if ( cmap_image_coordinates.width() > 0 && cmap_image_coordinates.height() > 0 )
      {
        int * full_cmap_image = (int*) m_cmap_pixbuf->get_pixels();
        int * zoomed_image    = new int[cmap_image_coordinates.area()];    
        int i, j, k=0;  
    
        for(i=cmap_image_coordinates.bottom; i<cmap_image_coordinates.top; i++)
        {
          for(j=cmap_image_coordinates.left; j<cmap_image_coordinates.right; j++, k++)
          {
            zoomed_image[k] = full_cmap_image[i*m_cmap->getNumCols()+j];
          }
        }
 
        // Determine offsets needed for CMap to scroll smoothly
        int pixels_per_cell = (int)(pixels_per_meter * m_cmap->getResRows());
        
        int left_pixels = 0;
        if( cmap_screen_coordinates.left < 0 )
        {
          left_pixels = (int)(pixels_per_cell * 
                  ((double)cmap_image_coordinates.left - 
                   cmap_image_coordinates_double.left));
        }
       
        int bottom_pixels = 0;
        if( cmap_screen_coordinates.bottom < 0 )
        {
          bottom_pixels = (int)(pixels_per_cell *
                  ((double)cmap_image_coordinates.bottom - 
                   cmap_image_coordinates_double.bottom));
        }

        // Draw the CMap
        int ras_x = whitespace.left   + left_pixels 
                + (int) cropped_cmap_screen_coordinates.left;
        int ras_y = whitespace.bottom + bottom_pixels 
                + (int) cropped_cmap_screen_coordinates.bottom;
  
        //    glClear(GL_COLOR_BUFFER_BIT);
        glRasterPos2i( ras_x , ras_y );
        double zoom_width  = (double)cropped_cmap_screen_coordinates.width() / 
                             (double)cmap_image_coordinates_double.width();
        double zoom_height = (double)cropped_cmap_screen_coordinates.height() / 
                             (double)cmap_image_coordinates_double.height();
        glPixelZoom(zoom_width, zoom_height); 

        glDrawPixels( cmap_image_coordinates.width(), cmap_image_coordinates.height(), 
                      GL_RGBA, GL_UNSIGNED_BYTE, zoomed_image );
  
        glFlush();      
        delete [] zoomed_image;
      }
    }
  }
	DGCunlockMutex(cmap_mutex);

  // ***************************************************
  // END -- DRAW CMAP
  // ***************************************************      




  // ***************************************************
  // BEGIN -- DRAW CMAP BORDER
  // ***************************************************
  if( m_check_draw_cmap_border.get_active() )
    {
      RECTANGLE<double> c_c 
  = map_utm_to_screen.map_rectangle( cmap_utm_coordinates );

      tmp_color = m_config->m_cmap_border_color;
      glColor4f( tmp_color.r, tmp_color.g, tmp_color.b, tmp_color.a );
      
      glBegin(GL_LINES);

      // Draw the 4 lines around the outside of the cmap.
      glVertex2f( whitespace.left + c_c.left,  whitespace.bottom + c_c.top );
      glVertex2f( whitespace.left + c_c.right, whitespace.bottom + c_c.top );

      glVertex2f( whitespace.left + c_c.right, whitespace.bottom + c_c.top );
      glVertex2f( whitespace.left + c_c.right, whitespace.bottom + c_c.bottom );

      glVertex2f( whitespace.left + c_c.right, whitespace.bottom + c_c.bottom );
      glVertex2f( whitespace.left + c_c.left,  whitespace.bottom + c_c.bottom );

      glVertex2f( whitespace.left + c_c.left,  whitespace.bottom + c_c.bottom );
      glVertex2f( whitespace.left + c_c.left,  whitespace.bottom + c_c.top );

      glEnd();
    }
  // ***************************************************
  // END -- DRAW CMAP BORDER
  // ***************************************************      




  // ***************************************************
  // BEGIN -- DRAW DARPA AREA CLOSURE
  // ***************************************************
  if( 1 )
    {
      // southwest corner, in Barstow
      POINT2D<double> pt_sw = 
        map_utm_to_screen.map_point( POINT2D<double> (497258.792, 3860845.004) );

      // southeast corner, in California
      POINT2D<double> pt_se = 
        map_utm_to_screen.map_point( POINT2D<double> (692796.511, 3775215.799) );
    
      // northeast corner, in Nevada
      POINT2D<double> pt_ne = 
        map_utm_to_screen.map_point( POINT2D<double> (702598.228, 3995165.088) );
    
      // northwest corner, in Nevada
      POINT2D<double> pt_nw = 
        map_utm_to_screen.map_point( POINT2D<double> (584420.616, 4015415.073) );
    
      glBegin(GL_LINES);

      glVertex2f( pt_sw.x, pt_sw.y );
      glVertex2f( pt_se.x, pt_se.y );
 
      glVertex2f( pt_se.x, pt_se.y );
      glVertex2f( pt_ne.x, pt_ne.y );
 
      glVertex2f( pt_ne.x, pt_ne.y );
      glVertex2f( pt_nw.x, pt_nw.y );
 
      glVertex2f( pt_nw.x, pt_nw.y );
      glVertex2f( pt_sw.x, pt_sw.y );
 
      glEnd();
    }
  // ***************************************************
  // END -- DRAW DARPA AREA CLOSURE
  // ***************************************************      




  // ***************************************************
  // BEGIN -- DRAW COLORMAP LEGEND
  // ***************************************************      

  // Update the widget.
  char maxvalue_text_buffer[100];
  double maxvalue = m_spin_colormap_center.get_value() +
                    m_spin_colormap_range.get_value();
  snprintf( maxvalue_text_buffer, 100, "% .2f", maxvalue );
  m_label_colormap_maxvalue.set_text(maxvalue_text_buffer);

  char minvalue_text_buffer[100];
  double minvalue = m_spin_colormap_center.get_value() -
                    m_spin_colormap_range.get_value();
  snprintf( minvalue_text_buffer, 100, "% .2f", minvalue );
  m_label_colormap_minvalue.set_text(minvalue_text_buffer);


  // ***************************************************
  // END -- DRAW COLORMAP LEGEND
  // ***************************************************      




  // ***************************************************
  // BEGIN -- DRAW RDDF
  // ***************************************************      
  RDDFVector rddf_waypoint_number_draw_list;

  if( m_check_draw_rddf.get_active() && m_config->m_rddf.size() >= 2 )
  {
    RDDFVector::iterator  prev_point = m_config->m_rddf.begin();
    RDDFVector::iterator  cur_point;
    RDDFVector::iterator  points_stop = m_config->m_rddf.end();     
    for( cur_point = prev_point++; cur_point != points_stop; prev_point=cur_point, cur_point++ )
    {
      double dist_y = cur_point->Northing - prev_point->Northing;
      double dist_x = cur_point->Easting - prev_point->Easting;
      if( dist_x != 0.0 || dist_y != 0.0 )
      {
        // First draw the boundary lines
        double theta = atan2( dist_y, dist_x );
        double sin_theta = sin(theta);
        double cos_theta = cos(theta);

        
        tmp_color = m_config->m_rddf_color;
        glColor4f( tmp_color.r, tmp_color.g, tmp_color.b, tmp_color.a );
      
        glBegin(GL_LINES);
      
        POINT2D<double> offset = POINT2D<double> ( - sin_theta, cos_theta ) * prev_point->radius;       
        POINT2D<double> pt_a = POINT2D<double> (whitespace.left, whitespace.bottom) 
          + map_utm_to_screen.map_point( offset + POINT2D<double> (prev_point->Easting, prev_point->Northing) );
        POINT2D<double> pt_b = POINT2D<double> (whitespace.left, whitespace.bottom) 
          + map_utm_to_screen.map_point( offset + POINT2D<double> (cur_point->Easting, cur_point->Northing) );
      
        glVertex2f( pt_a.x, pt_a.y );
        glVertex2f( pt_b.x, pt_b.y );
      
        offset = POINT2D<double> ( sin_theta, - cos_theta ) * prev_point->radius;
        pt_a = POINT2D<double> (whitespace.left, whitespace.bottom) 
          + map_utm_to_screen.map_point( offset + POINT2D<double> (prev_point->Easting, prev_point->Northing) );
        pt_b = POINT2D<double> (whitespace.left, whitespace.bottom) 
          + map_utm_to_screen.map_point( offset + POINT2D<double> (cur_point->Easting, cur_point->Northing) );
      
        glVertex2f( pt_a.x, pt_a.y );
        glVertex2f( pt_b.x, pt_b.y );
      
        glEnd();

        // now draw the circles around each waypoint
        // For speed reasons, only draw circles on or near the display
        double max_dist
          = 1.5 / pixels_per_meter * (double) max( screen_dimensions.width(), screen_dimensions.height());
        double max_dist_squared = pow( max_dist, 2.0 );
        
        POINT2D<double> whitespace_left_bottom(whitespace.left, whitespace.bottom);
        
        double p_dx = utm_screen_center.x - prev_point->Easting;
        double p_dy = utm_screen_center.y - prev_point->Northing;
        
        double c_dx = utm_screen_center.x - cur_point->Easting;
        double c_dy = utm_screen_center.y - cur_point->Northing;
        
        if( p_dx*p_dx + p_dy*p_dy < max_dist_squared || 
            c_dx*c_dx + c_dy*c_dy < max_dist_squared )
        { 
          // Add the waypoints to the list that need to be drawn
          // so that they can be numbered
          rddf_waypoint_number_draw_list.push_back( *prev_point );
          rddf_waypoint_number_draw_list.push_back( *cur_point );
        
          glBegin(GL_LINES);
          
          double step_size = 2.0*M_PI/ 10.0;
          
          for( theta = 0.0; theta < 2.0 * M_PI; theta+= step_size )
          {
            POINT2D<double> offset1 = POINT2D<double> ( cos(theta), sin(theta) ) * prev_point->radius;        
            POINT2D<double> offset2 = POINT2D<double> ( cos(theta+step_size), sin(theta+step_size) ) * prev_point->radius;
        
            POINT2D<double> pt_a = whitespace_left_bottom
              + map_utm_to_screen.map_point( offset1 + POINT2D<double> (prev_point->Easting, prev_point->Northing) );
        
            POINT2D<double> pt_b = whitespace_left_bottom 
              + map_utm_to_screen.map_point( offset2 + POINT2D<double> (prev_point->Easting, prev_point->Northing) );
        
            glVertex2f( pt_a.x, pt_a.y );
            glVertex2f( pt_b.x, pt_b.y );
        
        
            pt_a = whitespace_left_bottom 
              + map_utm_to_screen.map_point( offset1 + POINT2D<double> (cur_point->Easting, cur_point->Northing) );
        
            pt_b = whitespace_left_bottom 
              + map_utm_to_screen.map_point( offset2 + POINT2D<double> (cur_point->Easting, cur_point->Northing) );
        
            glVertex2f( pt_a.x, pt_a.y );
            glVertex2f( pt_b.x, pt_b.y );
          }     
          glEnd();
        }           
      }       
    }
  }
  // ***************************************************
  // END -- DRAW RDDF
  // ***************************************************      


  // ***************************************************
  // BEGIN -- DRAW PATHS
  // ***************************************************      
  
  if( m_check_draw_paths.get_active() )
  {
    PATH* paths;
    int numPaths;
    m_config->get_paths(&paths, &numPaths);
    // set the limits of the spin
    m_spin_path.set_range(0,numPaths-1);
  
    for( int i=0; i<numPaths; i++)
    {
      if(paths[i].draw)
			{
				tmp_color = paths[i].color;
				glColor4f( tmp_color.r, tmp_color.g, tmp_color.b, tmp_color.a );

				glBegin(GL_LINE_STRIP);
				DGClockMutex(&paths[i].pathMutex);
				{
					int j,  num_points  = paths[i].points.getNumPoints();
					for( j=0; j<num_points; j++ )
					{
						POINT2D<double> pt = POINT2D<double> (whitespace.left, whitespace.bottom) 
							+ map_utm_to_screen.map_point( POINT2D<double> (paths[i].points.getEasting(j), paths[i].points.getNorthing(j)));
						glVertex2f( pt.x, pt.y );        
					}
				} 
				DGCunlockMutex(&paths[i].pathMutex);
				glEnd();
			}
    }
  }  
  // ***************************************************
  // END -- DRAW PATHS
  // ***************************************************      



  // ***************************************************
  // BEGIN -- DRAW THE VEHICLE
  // ***************************************************      
  
  VehicleState * ss             = m_config->get_state_struct();
  if( m_check_draw_sensors.get_active() && ss )
  {
    ActuatorState* pActuatorState = m_config->get_actuator_state_struct();

    // first, draw the vehicle itself
    POINT2D<double> mid_front_axle, front_right_wheel, back_right_wheel, back_left_wheel, front_left_wheel;
    double screenyaw = M_PI/2.0 - ss->Yaw;

    mid_front_axle    = POINT2D<double> (ss->Easting, ss->Northing);
    front_right_wheel = mid_front_axle + POINT2D<double>(sin(screenyaw), -cos(screenyaw)) * ( VEHICLE_AVERAGE_TRACK/2.0);
    front_left_wheel  = mid_front_axle + POINT2D<double>(sin(screenyaw), -cos(screenyaw)) * (-VEHICLE_AVERAGE_TRACK/2.0);

    back_right_wheel  = front_right_wheel + POINT2D<double>(cos(screenyaw), sin(screenyaw)) * (-VEHICLE_WHEELBASE);
    back_left_wheel   = front_left_wheel  + POINT2D<double>(cos(screenyaw), sin(screenyaw)) * (-VEHICLE_WHEELBASE);

    mid_front_axle    = POINT2D<double> (whitespace.left, whitespace.bottom) + map_utm_to_screen.map_point(mid_front_axle);
    front_right_wheel = POINT2D<double> (whitespace.left, whitespace.bottom) + map_utm_to_screen.map_point(front_right_wheel);
    back_right_wheel  = POINT2D<double> (whitespace.left, whitespace.bottom) + map_utm_to_screen.map_point(back_right_wheel);
    front_left_wheel  = POINT2D<double> (whitespace.left, whitespace.bottom) + map_utm_to_screen.map_point(front_left_wheel);
    back_left_wheel   = POINT2D<double> (whitespace.left, whitespace.bottom) + map_utm_to_screen.map_point(back_left_wheel);

    tmp_color = m_config->m_sensor_color;
    glColor4f( tmp_color.r, tmp_color.g, tmp_color.b, tmp_color.a );
    glBegin(GL_LINE_STRIP);

    glVertex2f( mid_front_axle.x   , mid_front_axle.y);
    glVertex2f( front_right_wheel.x, front_right_wheel.y);
    glVertex2f( back_right_wheel.x , back_right_wheel.y);
    glVertex2f( back_left_wheel.x  , back_left_wheel.y);
    glVertex2f( front_left_wheel.x , front_left_wheel.y);
    glVertex2f( mid_front_axle.x   , mid_front_axle.y);

    // now draw the steering arc for the front of the vehicle
    double phi;
    if( pActuatorState == NULL )
    {
      phi = 0.0;
    }
    else
    {
      phi = pActuatorState->m_steerpos * VEHICLE_MAX_AVG_STEER;
    }
    if(fabs(phi) < 0.0001)
    {
      phi = 0.0001;
    }
    double turningRadius = VEHICLE_WHEELBASE / sin(phi);
    double cn, ce, angle, dangle;
    double lengthArc = VEHICLE_WHEELBASE * 0.5;

    // turningRadius has a sign and will make sure the following are correct
    cn = ss->Northing - turningRadius*sin(ss->Yaw + phi);
    ce = ss->Easting  + turningRadius*cos(ss->Yaw + phi);
    dangle = 2.0 * M_PI * lengthArc / turningRadius / 20.0;
    angle = atan2(ss->Easting - ce, ss->Northing - cn);

    int i;
    for(i=0; i<20; i++)
    {
      angle += dangle;

      POINT2D<double> point(ce + fabs(turningRadius)*sin(angle), cn + fabs(turningRadius)*cos(angle));
      point = POINT2D<double> (whitespace.left, whitespace.bottom) + map_utm_to_screen.map_point(point);
      glVertex2f(point.x, point.y);
    }
    glEnd();

    // now draw the steering arc for the back of the vehicle
    glBegin(GL_LINE_STRIP);
    turningRadius = VEHICLE_WHEELBASE / tan(phi);

    // turningRadius has a sign and will make sure the following are correct
    cn = ss->Northing_rear() - turningRadius*sin(ss->Yaw);
    ce = ss->Easting_rear()  + turningRadius*cos(ss->Yaw);
    dangle = 2.0 * M_PI * lengthArc / turningRadius / 20.0;
    angle = atan2(ss->Easting_rear() - ce, ss->Northing_rear() - cn);

    glVertex2f( (back_left_wheel.x+back_right_wheel.x)/2.0, 
                (back_left_wheel.y+back_right_wheel.y)/2.0 );
    for(i=0; i<20; i++)
    {
      angle += dangle;

      POINT2D<double> point(ce + fabs(turningRadius)*sin(angle), cn + fabs(turningRadius)*cos(angle));
      point = POINT2D<double> (whitespace.left, whitespace.bottom) + map_utm_to_screen.map_point(point);
      glVertex2f(point.x, point.y);
    }
    glEnd();

    // Now draw the commanded steering arc
    glBegin(GL_LINE_STRIP);
    glColor4f( m_config->m_steercmd_color.r,
               m_config->m_steercmd_color.g,
               m_config->m_steercmd_color.b,
               m_config->m_steercmd_color.a );

    if( pActuatorState == NULL )
    {
      phi = 0.0;
    }
    else
    {
      phi = pActuatorState->m_steercmd * VEHICLE_MAX_AVG_STEER;
    }
    if(fabs(phi) < 0.0001)
    {
      phi = 0.0001;
    }
    turningRadius = VEHICLE_WHEELBASE / sin(phi);

    // turningRadius has a sign and will make sure the following are correct
    cn = ss->Northing - turningRadius*sin(ss->Yaw + phi);
    ce = ss->Easting  + turningRadius*cos(ss->Yaw + phi);
    dangle = 2.0 * M_PI * lengthArc / turningRadius / 20.0;
    angle = atan2(ss->Easting - ce, ss->Northing - cn);

    glVertex2f( mid_front_axle.x   , mid_front_axle.y);
    for(i=0; i<20; i++)
    {
      angle += dangle;

      POINT2D<double> point(ce + fabs(turningRadius)*sin(angle), cn + fabs(turningRadius)*cos(angle));
      point = POINT2D<double> (whitespace.left, whitespace.bottom) + map_utm_to_screen.map_point(point);
      glVertex2f(point.x, point.y);
    }
    glEnd();

  }
  // ***************************************************
  // END -- DRAW THE VEHICLE
  // ***************************************************      





  // ***************************************************
  // BEGIN -- DRAW GRIDLINES
  // ***************************************************      

  // Calculate gridline spacing regardless of actually using it.
  double msd = (double) screen_dimensions.width() / pixels_per_meter;
  double msdexp, msdmantissa;

  // represent msd in base-10 scientific notation:
  msdexp = pow(10.0, floor(log(msd) / log(10.0)));

  msdmantissa = msd / msdexp;

  double grid_spacing = (msdmantissa < 4.0 ) ? msdexp / 2.5 : msdexp / 1.0;   
  double starting_grid_left_coordinate   
    = floor( (utm_screen_coordinates.left+grid_spacing) / grid_spacing ) * grid_spacing;
  double starting_grid_bottom_coordinate 
    = floor( (utm_screen_coordinates.bottom+grid_spacing) / grid_spacing ) * grid_spacing;
  
  // Update the widget.
  char grid_spacing_text_buffer[100];
  snprintf( grid_spacing_text_buffer, 100, "Gridlines every %6.6gm", grid_spacing );
  m_label_gridlines_spacing.set_text(grid_spacing_text_buffer);
 
  // Now draw the gridlines
  if( m_check_draw_gridlines.get_active() ) 
    { 
      // Issue OpenGL commands to draw the gridlines
      // maximally thin grid lines      
      glLineWidth( 1 );
      glBegin(GL_LINES);
      tmp_color = m_config->m_grid_color;
      glColor4f( tmp_color.r, tmp_color.g, tmp_color.b, tmp_color.a );


      double utm_left_coordinate   = starting_grid_left_coordinate;
      double utm_bottom_coordinate = starting_grid_bottom_coordinate;

      while ( utm_left_coordinate < utm_screen_coordinates.right ) 
  {
    double left_coordinate = (map_utm_to_screen.map_point( POINT2D<double> (utm_left_coordinate , 0 ))).x;
    glVertex2f( (double) whitespace.left+left_coordinate, whitespace.bottom + screen_dimensions.height());
    glVertex2f( (double) whitespace.left+left_coordinate, whitespace.bottom );    

    utm_left_coordinate += grid_spacing;
  }
      while ( utm_bottom_coordinate < utm_screen_coordinates.top ) 
  {
    double bottom_coordinate = (map_utm_to_screen.map_point( POINT2D<double> (0, utm_bottom_coordinate ))).y;
    glVertex2f( (double) whitespace.left,  (double) whitespace.bottom + bottom_coordinate );
    glVertex2f( (double) whitespace.left + screen_dimensions.width(), (double) whitespace.bottom + bottom_coordinate );   
    utm_bottom_coordinate += grid_spacing;
  }
      glEnd();
    }
  // ***************************************************
  // END -- DRAW GRIDLINES
  // ***************************************************      


  // ***************************************************
  // BEGIN -- DRAW RDDF NUMBERS
  // ***************************************************      
  if( m_check_draw_waypoints.get_active() )
    {
      POINT2D<double> whitespace_left_bottom(whitespace.left, whitespace.bottom);

      tmp_color = m_config->m_rddf_number_color;      
      glColor4f( tmp_color.r, tmp_color.g, tmp_color.b, tmp_color.a );
      char waypoint_char_buffer[10];

      RDDFVector::iterator waypoint;
      RDDFVector::iterator stop = rddf_waypoint_number_draw_list.end();
      for( waypoint = rddf_waypoint_number_draw_list.begin(); waypoint != stop; waypoint++)
  {
    double pixels = waypoint->radius * pixels_per_meter;
    if( pixels > 4 )
      {
        POINT2D<double> text_position = whitespace_left_bottom 
    + map_utm_to_screen.map_point( POINT2D<double> (waypoint->Easting, waypoint->Northing) );
      
        snprintf( waypoint_char_buffer, 10, "%d", waypoint->number );
        int len = strlen(waypoint_char_buffer);
        glRasterPos2f( text_position.x, text_position.y );
        glListBase(m_Font36ListBase);
        glCallLists(len, GL_UNSIGNED_BYTE, waypoint_char_buffer);         
      }
  }
    }
  // ***************************************************
  // END -- DRAW RDDF NUMBERS
  // ***************************************************      
  



  // ***************************************************
  // BEGIN -- DRAW INFO
  // ***************************************************      
  if( m_check_draw_info.get_active() ) 
    {
      // First draw the border on which to draw the info
      {
  tmp_color = m_config->m_border_color;
  glColor4f( tmp_color.r, tmp_color.g, tmp_color.b, tmp_color.a );
  
  glBegin( GL_POLYGON );
  {
    glVertex2i( 0, 0 );
    glVertex2i( 0, whitespace.bottom-1 );
    glVertex2i( widget_dimensions.width(), whitespace.bottom-1 );
    glVertex2i( widget_dimensions.width(), 0 );   
  }
  glEnd();
  glBegin( GL_POLYGON );
  {
    glVertex2i( screen_dimensions.width(), 0 );
    glVertex2i( screen_dimensions.width(), widget_dimensions.height() );
    glVertex2i( widget_dimensions.width(), widget_dimensions.height() );
    glVertex2i( widget_dimensions.width(), 0 );   
  }
  glEnd();


      }
  
      // Now draw the text
      {
  tmp_color = m_config->m_info_text_color;
  glColor4f( tmp_color.r, tmp_color.g, tmp_color.b, tmp_color.a );
  double utm_left_coordinate   = starting_grid_left_coordinate;
  double utm_bottom_coordinate = starting_grid_bottom_coordinate;
  char write_buffer[100];
  
  while ( utm_left_coordinate < utm_screen_coordinates.right ) 
    {
      double left_coordinate = (map_utm_to_screen.map_point( POINT2D<double> (utm_left_coordinate , 0 ))).x;
      snprintf( write_buffer, 100, "%.8g", utm_left_coordinate );
      //    // cout << "Writing coordinate " << write_buffer << endl;
      int len = strlen( write_buffer );
      glRasterPos2f( (double) whitespace.left+left_coordinate - (double) len * 3.0, whitespace.bottom - 3 - m_Font12Height );
      glListBase(m_Font12ListBase);
      glCallLists(len, GL_UNSIGNED_BYTE, write_buffer);
      
      utm_left_coordinate += grid_spacing;
    }
  
  while ( utm_bottom_coordinate < utm_screen_coordinates.top ) 
    {
      double bottom_coordinate = (map_utm_to_screen.map_point( POINT2D<double> (0, utm_bottom_coordinate ))).y;
      snprintf( write_buffer, 100, "%.8g", utm_bottom_coordinate );
      //    // cout << "Writing coordinate " << write_buffer << endl;
      int len = strlen( write_buffer );
      glRasterPos2f( (double) whitespace.left + (double) screen_dimensions.width() + 3, 
         (double) (whitespace.bottom + bottom_coordinate) - 3 );
      glListBase(m_Font12ListBase);
      glCallLists(len, GL_UNSIGNED_BYTE, write_buffer);
      
      utm_bottom_coordinate += grid_spacing;
    }       
      }
    }
  // ***************************************************
  // END -- DRAW INFO
  // ***************************************************      
  
  glFlush();      
}

void MapWidget::_colormap_init() 
{
  m_colormap_pixbuf = Gdk::Pixbuf::create( Gdk::COLORSPACE_RGB, true, 8, 256, 1 );    
  guint8 * pix_ptr = m_colormap_pixbuf->get_pixels();
  
  InterpolationMap<double, RGBA_COLOR> m_colormap = m_config->m_colormap;

  int i;
  for( i=0; i<256; i++, pix_ptr+=4 ) {
    
    RGBA_COLOR rgbap = m_colormap.get_value( (double) i );
    
    pix_ptr[0] = (guint8) ( 255.0 * rgbap.r );
    pix_ptr[1] = (guint8) ( 255.0 * rgbap.g );
    pix_ptr[2] = (guint8) ( 255.0 * rgbap.b );
    pix_ptr[3] = (guint8) ( 255.0 * rgbap.a );
  }
}
void MapWidget::_colormap_reshape(int w, int h) 
{    
  glViewport( 0, 0, (GLsizei) w, (GLsizei) h );
  
  glMatrixMode ( GL_PROJECTION );
  glLoadIdentity();
  
  gluOrtho2D( 0.0, (GLdouble) w, 0.0, (GLdouble) h );
  glMatrixMode( GL_MODELVIEW );
  glLoadIdentity();    
}

void MapWidget::_colormap_draw(int width, int height) 
{
  int colormap_num_pixels = m_colormap_pixbuf->get_height() * m_colormap_pixbuf->get_width();
  
  double zoom_width  = ((double) width);
  double zoom_height = ((double) height) / (double) colormap_num_pixels;
  
  glRasterPos2i( 0, 0 );
  glPixelZoom(zoom_width, zoom_height);
  
  glDrawPixels( 1 , colormap_num_pixels, GL_RGBA, GL_UNSIGNED_BYTE, m_colormap_pixbuf->get_pixels() );
  
  glFlush();
}


void MapWidget::_print_layer_name() {
  char infoBuffer[256];
  sprintf(infoBuffer, "Switched to layer '%s'", get_cmap()->getLayerLabel(get_layer_num()));

  m_layerOptions[m_lastLayer].center = get_colormap_center();
  m_layerOptions[m_lastLayer].range = get_colormap_range();
  m_layerOptions[m_lastLayer].autoRange = get_colormap_use_auto_range();
  m_layerOptions[m_lastLayer].autoCenter = get_colormap_use_auto_center();

  m_lastLayer = get_layer_num();
    
  set_colormap_center(m_layerOptions[m_lastLayer].center);
  set_colormap_range(m_layerOptions[m_lastLayer].range);
  set_colormap_use_auto_range(m_layerOptions[m_lastLayer].autoRange);
  set_colormap_use_auto_center(m_layerOptions[m_lastLayer].autoCenter);

  m_label_cmap_info.set_text(infoBuffer);
}

void MapWidget::_clear_current_layer() 
{
  get_cmap()->clearLayer(get_layer_num());

  // Clear the first path (the history)
  PATH* paths;
  int numPaths;
  m_config->get_paths(&paths, &numPaths);
  paths[0].points.startDataInput();
}


void MapWidget::_clear_all_layers() 
{
  for(int i=0; i<get_cmap()->getNumLayers(); i++) {
    get_cmap()->clearLayer(i);
  }

  // Clear the first path (the history)
  PATH* paths;
  int numPaths;
  m_config->get_paths(&paths, &numPaths);
  paths[0].points.startDataInput();
}


void MapWidget::notify_cmap_pointer_change()
{
  // Lock the configuration struct
  Glib::RecMutex::Lock lock( m_config->m_mutex );

  CMap * m_cmap = m_config->get_cmap();
  if( m_cmap )
    {
      int width  = m_cmap->getNumCols();
      int height = m_cmap->getNumRows();      
      m_cmap_pixbuf = Gdk::Pixbuf::create( Gdk::COLORSPACE_RGB, true, 8, width, height );      
      m_spin_cmap_layer.set_range( 0, m_cmap->getNumLayers()-1 );
      if(m_cmap->getNumLayers()>=1) {
  m_spin_cmap_layer.set_value( 0 );
      } else {
  m_spin_cmap_layer.set_value( m_cmap->getNumLayers()-1 );
      }
    }
  else 
    {
      m_cmap_pixbuf.clear();
      m_spin_cmap_layer.set_range(0,0);
    }
}

void MapWidget::togglePath()
{
  PATH* paths;
  int numPaths;
  m_config->get_paths(&paths, &numPaths);
  int pindex = (int)m_spin_path.get_value();
  paths[pindex].draw = ! paths[pindex].draw;
}


void MapWidget::findCMap() {
  char infoBuffer[256];
  snprintf(infoBuffer, 256, "Map at (N,E) = (%.10g, %.10g)", get_cmap()->getVehLocUTMNorthing(),
	   get_cmap()->getVehLocUTMEasting());
  m_label_cmap_info.set_text(infoBuffer);

  m_config->m_anchor_pos = UTM_POINT(get_cmap()->getVehLocUTMEasting(),
				     get_cmap()->getVehLocUTMNorthing());
  m_config->m_drop_anchor = true;

}


void MapWidget::notify_update() 
{
  m_cmap_draw_area.redraw_immediately();
}


// Helper function to convert cmap to pixbuf

void CMapToPixbuf(MapWidget * map_widget) {
  Glib::RefPtr<Gdk::Pixbuf>  tmpbuf = map_widget->get_pixbuf();
  if (tmpbuf==0) {
    printf("returning due to map_widget->get_pixbuf() returning 0\n");
    return;
  }
  int width  = map_widget->get_pixbuf()->get_width();
  int height = map_widget->get_pixbuf()->get_height();
  int layer_num = map_widget->get_layer_num();

  CMap  * _cmap_ptr = map_widget->get_cmap();
  if( ! _cmap_ptr ) return;

  // Grab the min and max we are going to use and update widgets
  static double prev_min=DBL_MAX, prev_max=-DBL_MAX;
  double prev_center = (prev_max + prev_min) / 2.0;

  double this_center = prev_center;

  // get/set the center value first.
  if( map_widget->get_colormap_use_auto_center() )
    {
      // set the center
      this_center = prev_center;
      map_widget->set_colormap_center(this_center);
    }
  else
    {
      // get the center
      this_center = map_widget->get_colormap_center();
    }

  // Get/Set the range value
  double this_range;
  if( map_widget->get_colormap_use_auto_range() ) 
    {
      // set the range
      this_range = max(  fabs(prev_max - this_center), fabs( this_center - prev_min ) );
      double min_range;
      if( this_range < (min_range = map_widget->get_colormap_min_range()) ) 
	{
	  this_range = min_range;   
	}
      map_widget->set_colormap_range(this_range);
    }
  else 
    {
      // get the range
      this_range = map_widget->get_colormap_range();
    }

  unsigned int * _pixbuf_ptr   = (unsigned int*) map_widget->get_pixbuf()->get_pixels(); 
  
  unsigned int * _colormap_ptr = (unsigned int*) map_widget->get_colormap()->get_pixels();
  
  double this_min = DBL_MAX;
  double this_max = -DBL_MAX;

  double colormap_min = this_center - this_range;
  double colormap_scale = 255.0 / (2.0 * this_range);

  double no_data_val = map_widget->get_no_data_val();

  int no_data_color = map_widget->get_map_config()->m_clear_color.get_RGBA_int();

  int i, j, k=0;
  int non_zero = 0;
  for( i=0; i<width; i++ ) {
    for( j=0; j<height; j++, k++ ) {
      // Jeremy's code from FusionMapper
      double val = _cmap_ptr->getDataWin<double>(layer_num, i, j);
      if( val == no_data_val ) {
	_pixbuf_ptr[k] = no_data_color;
      } else {
	non_zero ++;

	if ( val < this_min) {
	  this_min = val;
	}
	if ( val > this_max) {
	  this_max = val;
	}
	
	int c = (int)( (val-colormap_min) * colormap_scale );
	if( c < 0 ) c = 0;
	if( c > 255 ) c = 255;
	
	_pixbuf_ptr[k] = _colormap_ptr[c];
      }      
    }
  }

  //  cout << non_zero << " pixels are filled." << endl;
  prev_min = this_min;
  prev_max = this_max;
}

RGBA_COLOR Interpolate( const RGBA_COLOR & lhs, const RGBA_COLOR & rhs, double lhs_c )
{
  double rhs_c = 1.0 - lhs_c;
  return RGBA_COLOR( lhs.r * lhs_c + rhs.r * rhs_c, 
         lhs.g * lhs_c + rhs.g * rhs_c,
         lhs.b * lhs_c + rhs.b * rhs_c,
         lhs.a * lhs_c + rhs.a * rhs_c );
}

void MapWidget::zoomin()
{
  m_spin_zoom_scale.spin(Gtk::SPIN_STEP_FORWARD, .1);
}

void MapWidget::zoomout()
{
  m_spin_zoom_scale.spin(Gtk::SPIN_STEP_BACKWARD, .1);
}
