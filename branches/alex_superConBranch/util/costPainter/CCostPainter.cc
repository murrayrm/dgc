#include "CCostPainter.hh"

CCostPainter::CCostPainter ()
{
 _inputMap = NULL;
 _speedMap = NULL;

 //logKernel = NULL;

  _costLayerNum = 0;
  _elevLayerNum = 0;
  _corridorLayerNum = 0;
}


CCostPainter::~CCostPainter ()
{

}


int
CCostPainter::initPainter (CMap * inputMap, int costLayerNum,
                           int elevLayerNum, int corridorLayerNum,
                           int staticLayerNum, int roadLayerNum, int fusedLayerNum ,  int nonBinary,
                           int growLayerNum, double radius,
                           int conservativeGrowing,
													 CMap* speedMap, int sampledCostLayerNum, int sampledCorridorLayerNum,
													 int fusedYetLayerNum) {
  _inputMap = inputMap;
	_speedMap = speedMap;
  _nonBinary = nonBinary;
  _costLayerNum = costLayerNum;
  _elevLayerNum = elevLayerNum;
  _corridorLayerNum = corridorLayerNum;
  _growLayerNum = growLayerNum;
  _staticLayerNum = staticLayerNum;
  _roadLayerNum = roadLayerNum;
  _fusedLayerNum = fusedLayerNum;
	_sampledCostLayerNum = sampledCostLayerNum;
	_sampledCorridorLayerNum = sampledCorridorLayerNum;
	_fusedYetLayerNum = fusedYetLayerNum;

  _numRows = _inputMap->getNumRows ();
  _numCols = _inputMap->getNumCols ();

  _radius = radius;

  _conservativeGrowing = conservativeGrowing;

  _numMaskPts = 0;

  double rowRes = _inputMap->getResRows ();
  double colRes = _inputMap->getResCols ();

  int minRow, minCol, maxRow, maxCol;

  minRow = (int) (-radius / rowRes);
  minCol = (int) (-radius / colRes);
  maxRow = (int) (radius / rowRes);
  maxCol = (int) (radius / colRes);

  /*
     for(int row=minRow; row<maxRow; row++) {
     for(int col=minCol; col<maxCol; col++) {
     if(conservativeGrowing) {

     } else {
     if(
     }
     _numMaskPts++;
     }
     }
   */

  return 0;
}

int CCostPainter::paintChanges (double UTMNorthing, double UTMEasting,
                             bool paintSurrounding )
{
  if (_inputMap->checkBoundsUTM (UTMNorthing, UTMEasting) == CMap::CM_OK)
  {
    double maxVelo, currentVelo;
    double maxGradient;
    double corridorReference, staticReference, roadReference;
    int maxLocalRow, maxLocalCol;
    int minLocalRow, minLocalCol;
    int cellRow, cellCol;
    double distance;

    //First, set us some limits
    _inputMap->UTM2Win (UTMNorthing, UTMEasting, &cellRow, &cellCol);
    double resRows, resCols;
    resRows = _inputMap->getResRows();
    resCols = _inputMap->getResCols();

    if (cellRow < _numRows - 1) {
      maxLocalRow = cellRow + 1;
      if(paintSurrounding == 1){
	paintChanges(UTMNorthing+resRows, UTMEasting, 0);
      }
    } else {
      maxLocalRow = cellRow;
    }

    if (cellCol < _numCols - 1) {
      maxLocalCol = cellCol + 1;
      if(paintSurrounding ==1){
	paintChanges(UTMNorthing, UTMEasting + resCols, 0);
      }
    } else {
      maxLocalCol = cellCol;
    }

    if (cellRow > 0) {
      minLocalRow = cellRow - 1;
      if(paintSurrounding ==1){
	paintChanges(UTMNorthing-resRows, UTMEasting,  0);
      }
    } else {
      minLocalRow = cellRow;
    }

    if (cellCol > 0) {
      minLocalCol = cellCol - 1;
      if(paintSurrounding ==1){
	paintChanges(UTMNorthing, UTMEasting -resCols, 0);
      }
    } else {
      minLocalCol = cellCol;
    }
 
    
    int numAdjObs = 0;
    double currentElev, northElev, southElev, eastElev, westElev;
    double northGrad, southGrad, eastGrad, westGrad;
    currentElev =
      _inputMap->getDataWin < double >(_elevLayerNum, cellRow, cellCol);
    northElev =
      _inputMap->getDataWin < double >(_elevLayerNum, maxLocalRow, cellCol);
    southElev =
      _inputMap->getDataWin < double >(_elevLayerNum, minLocalRow, cellCol);
    eastElev =
      _inputMap->getDataWin < double >(_elevLayerNum, cellRow, maxLocalCol);
    westElev =
      _inputMap->getDataWin < double >(_elevLayerNum, cellRow, minLocalCol);
    double noData = _inputMap->getLayerNoDataVal < double >(_elevLayerNum);
    int gradTemp;
    double gradientNorthing, gradientEasting;
    CElevationFuser& mapData  = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, cellRow, cellCol);
    double stdDev = mapData.getStdDev();
    double stdDevVelo;
    currentVelo = _inputMap->getDataWin < double >(_costLayerNum, cellRow, cellCol);
    corridorReference =
      _inputMap->getDataWin < double >(_corridorLayerNum, cellRow, cellCol);
    staticReference =
      _inputMap->getDataWin < double >(_staticLayerNum, cellRow, cellCol);
    roadReference = 
      _inputMap->getDataWin < double >(_roadLayerNum, cellRow ,cellCol);
    bool northObs = false, southObs = false, eastObs=false, westObs = false;
    double stdDevArea = 0;
    
    double stdTemp;
    int stdCount = 0 ;
    //area stdDev here
    int i, j;
    //bound checking
    for(i=-2; i<3 ;i++){
      for(j=-2;j<3;j++){
        mapData  = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, cellRow, cellCol);
	stdTemp = mapData.getStdDev();
	if (stdTemp >= .01){
	  stdCount++;
	  stdDevArea += stdTemp/pow((double)(pow(i,2.0)+pow(j,2.0)),.5)  ; 
	}
      }
    }
    stdDevArea = stdDevArea / stdCount;

    if(currentElev != noData){
      gradTemp = calcAreaGradient(cellRow, cellCol, gradientNorthing, gradientEasting);
      if ( _mapperOpts.optUseAreaGradient == 0){
	gradTemp = 0;
      }
      if (northElev != noData) {
	  if (gradTemp==1){
	    northGrad = fabs( gradientCorrection( (northElev - currentElev) / resRows,gradientNorthing,_mapperOpts.optAreaGradScaling));
	  } else{
	    northGrad = fabs ( northElev - currentElev)/resRows;
	  }
	} else {
	  northGrad = 0.0;
	}
      if (southElev != noData) {
	  if (gradTemp==1){
	    southGrad = fabs (gradientCorrection( (southElev - currentElev) / resRows,-gradientNorthing,_mapperOpts.optAreaGradScaling));
	  } else{
	    southGrad = fabs (southElev - currentElev) / resRows;
	  }
	} else {
	  southGrad = 0.0;
	}
      if (eastElev != noData) {
	  if (gradTemp==1){
	    eastGrad = fabs (gradientCorrection( (eastElev - currentElev) / resCols, gradientEasting, _mapperOpts.optAreaGradScaling));
	  } else{
	    eastGrad = fabs (eastElev - currentElev) / resCols;
	  }
	} else {
	  eastGrad = 0.0;
	}
      if (westElev != noData) {
	if (gradTemp==1){
	  westGrad = fabs (gradientCorrection( (westElev - currentElev) / resCols,-gradientEasting, _mapperOpts.optAreaGradScaling));
	} else{ 
	  westGrad = fabs (westElev - currentElev) / resCols;
	}
      } else {
	westGrad = 0.0;
      }
    
    // Calculate the maximum actual gradient between the current cell and the 
    // 4-neighboring cells with real elevation values
      if(northGrad >= _mapperOpts.optUnpassableObsHeight){
	numAdjObs++;
	northObs = true;
      }
      if(southGrad >= _mapperOpts.optUnpassableObsHeight){
	numAdjObs++;
	southObs = true;
      }
      if(eastGrad >= _mapperOpts.optUnpassableObsHeight){
	numAdjObs++;
	eastObs = true;
      }
      if(westGrad >= _mapperOpts.optUnpassableObsHeight){
	numAdjObs++;
	westObs = true;
      }

      double temp1, temp2;
      double unpassObs = _mapperOpts.optUnpassableObsHeight;
      double noDataVal = _inputMap->getLayerNoDataVal<double>(_elevLayerNum);


      if(numAdjObs == 1){
	if(northObs == true){
	  temp1 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow +1, cellCol+1 );
	  temp2 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow +1, cellCol-1 );
	  if(cellRow +1 < _numRows -1 && cellCol+1 < _numCols -1 && cellCol-1 >= 0 )
	  if(temp1 != noDataVal && temp2 != noDataVal){
	    
	    if(fabs(currentElev - temp2 ) < unpassObs && fabs(currentElev -temp2 ) < unpassObs){
	      northGrad = 0;
	    }
	  }
	}
	
     
     	if(southObs == true){
	  temp1 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow -1, cellCol+1 );
	  temp2 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow -1, cellCol-1 );
	  if(cellRow -1 >= 0 && cellCol+1 < _numCols -1 && cellCol-1 >= 0 )
	  if(temp1 != noDataVal && temp2 != noDataVal){
	    
	    if(fabs(currentElev - temp2 ) < unpassObs && fabs(currentElev -temp2 ) < unpassObs){
	      southGrad = 0;
	    }
	  }
	}
	

	if(eastObs == true){
	  temp1 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow +1, cellCol+1 );
	  temp2 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow -1, cellCol+1 );
	  if(cellCol +1 < _numRows -1 && cellRow+1 < _numCols -1 && cellRow-1 >= 0 )
	  if(temp1 != noDataVal && temp2 != noDataVal){
	    
	    if(fabs(currentElev - temp2 ) < unpassObs && fabs(currentElev -temp2 ) < unpassObs){
	      eastGrad = 0;
	    }
	  }
	}

	if(westObs == true){
	  temp1 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow +1, cellCol-1 );
	  temp2 = _inputMap->getDataWin<double>(_elevLayerNum,cellRow -1, cellCol-1 );
	  if(cellCol -1 >= 0 && cellRow+1 < _numCols -1 && cellRow-1 >= 0 )
	  if(temp1 != noDataVal && temp2 != noDataVal){
	    
	    if(fabs(currentElev - temp2 ) < unpassObs && fabs(currentElev -temp2 ) < unpassObs){
	      westGrad = 0;
	    }
	  }
	}

    }
  
  

    maxGradient = fmax(northGrad, fmax(southGrad, fmax(eastGrad, westGrad)));
    if(_mapperOpts.optZeroGradSpeedAsRDDFPercentage == 0){
      if(_mapperOpts.optVeloGenerationAlg ==2){
	maxVelo = generateVeloFromGrad2(maxGradient, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight,_mapperOpts.optVeloTweak1,_mapperOpts.optVeloTweak2);
	stdDevVelo = generateVeloFromGrad2(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight, _mapperOpts.optVeloTweak1, _mapperOpts.optVeloTweak2);
      }
      else if(_mapperOpts.optVeloGenerationAlg ==3){
	maxVelo = generateVeloFromGrad3(maxGradient, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight,_mapperOpts.optVeloTweak1,_mapperOpts.optVeloTweak2);
	stdDevVelo = generateVeloFromGrad3(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight, _mapperOpts.optVeloTweak1, _mapperOpts.optVeloTweak2);
 }
      else{
	maxVelo = generateVeloFromGrad(maxGradient, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight);
	stdDevVelo = generateVeloFromGrad(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed, _mapperOpts.optUnpassableObsHeight);
      }
    } else{

      if(_mapperOpts.optVeloGenerationAlg ==2){
	maxVelo = generateVeloFromGrad2(maxGradient, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight,_mapperOpts.optVeloTweak1,_mapperOpts.optVeloTweak2);
	stdDevVelo = generateVeloFromGrad2(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight, _mapperOpts.optVeloTweak1, _mapperOpts.optVeloTweak2);
      }
      else if(_mapperOpts.optVeloGenerationAlg ==3){
       	maxVelo = generateVeloFromGrad3(maxGradient, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight,_mapperOpts.optVeloTweak1, _mapperOpts.optVeloTweak2);
     stdDevVelo = generateVeloFromGrad3(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight, _mapperOpts.optVeloTweak1, _mapperOpts.optVeloTweak2);
      }
      else{
	maxVelo = generateVeloFromGrad(maxGradient, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight);
	stdDevVelo = generateVeloFromGrad(stdDev*_mapperOpts.optStdDevScaling, _mapperOpts.optZeroGradSpeed*corridorReference, _mapperOpts.optUnpassableObsHeight);
      }
    }


        if (_nonBinary == 1)
        {
     
	  //    printf("%s [%d]: nonbinary, maxvelo is %lf\n", __FILE__, __LINE__, maxVelo);
	  /*	  if(staticReference != _inputMap->getLayerNoDataVal<double>(_staticLayerNum)){
	    if(maxVelo <= staticReference){
	      
	      //add linear element
	      maxVelo = maxVelo + (staticReference-(pow(maxVelo-staticReference/2,2) ) *(4/staticReference) )*_mapperOpts.optStaticScaling;
	      
	      } 
	    else {
	      
	      maxVelo = (maxVelo + staticReference)/2;
	    }
	    }*/
	  if(roadReference != _inputMap->getLayerNoDataVal<double>(_roadLayerNum)){
	    if(maxVelo <= roadReference){
	      maxVelo = maxVelo + (roadReference-(pow(maxVelo-roadReference/2,2))*(4/roadReference))*_mapperOpts.optRoadScaling;
	    }
	    else{
	      //prolly don't need to do anything
	    }
	  }
	}
        else
	  {
	    if (maxGradient > 0.3) {
	      maxVelo = _inputMap->getLayerNoDataVal<double>(_costLayerNum);
	    } else {
	      maxVelo = corridorReference;
	    }
	  }

	if(stdDevVelo != 0){//make sure we have data
	  if(maxVelo > .01 && maxVelo != noData) {  //make sure it's not an obstacle
	  maxVelo = maxVelo*(1-_mapperOpts.optStdDevWeight) +  stdDevVelo * _mapperOpts.optStdDevWeight;
	  }
	}
#warning "add a better fusion algorithm here";
	//   if(corridorReference != _inputMap->getLayerNoDataVal<double>(_corridorLayerNum) || corridorReference != _inputMap->getLayerOutsideMapVal<double>(_corridorLayerNum)){

    }
   else
     {
	 maxVelo = _mapperOpts.corridorOpts.optMaxSpeed;
     }
    


        if (currentVelo != maxVelo && maxVelo != _inputMap->getLayerNoDataVal < double >(_costLayerNum) &&
	    corridorReference != _inputMap->getLayerNoDataVal<double>(_corridorLayerNum))

	  {
	    distance = pow((pow((cellRow-_numRows/2)*resRows ,2.0  )+pow((cellCol-_numCols/2)*resCols,2.0)),.5);
	    if(distance > _mapperOpts.optObstacleDistanceLimit ){
	      maxVelo = fmax( _mapperOpts.optDistantObstacleSpeed, maxVelo);
	    }

         _inputMap->setDataWin_Delta < double >(_costLayerNum, cellRow, cellCol,
						fmax(fmin(fmin (maxVelo, corridorReference),_mapperOpts.optMaxSpeed),  _inputMap->getLayerNoDataVal<double>(_costLayerNum)));
       }
    
  }
  return 0;
}


double
CCostPainter::generateVeloFromGrad (double gradient, double zeroGradSpeed,
                                    double unpassableObsHeight)
{
  /*                    ^
   *    zeroGradSpeed ->+
   *                    |\
   *                    | \
   *                    |  \
   *             0.0001 +---+->
   *                        ^
   *                        |
   *                        unpassableGradient
   */
double temp = fabs(gradient);
  if (temp < unpassableObsHeight)
  {
    return (unpassableObsHeight - temp) * zeroGradSpeed / unpassableObsHeight;
  }
  else
  {
   return _inputMap->getLayerNoDataVal<double>(_costLayerNum);
  }
}

double CCostPainter::generateVeloFromGrad2(double gradient, double zeroGradSpeed, double unpassableObsHeight, double tweak1, double tweak2){
  double temp = fabs(gradient);
  
  if(temp < unpassableObsHeight)
    { //tweak1 like obsHeight
      if(temp <= tweak1){
	return ((tweak1 - temp)*zeroGradSpeed + temp*tweak2)/tweak1 ;
      }
      else{
	return (unpassableObsHeight - (temp-tweak1)*unpassableObsHeight/(unpassableObsHeight - tweak1))*tweak2 / unpassableObsHeight  ;
      }
    }
  else
    {
      return 0.0001;
    }
  
  
}
 
double CCostPainter::generateVeloFromGrad3(double gradient, double zeroGradSpeed, double unpassableObsHeight, double tweak1, double tweak2){
   double temp = fabs (gradient);
  if (temp < unpassableObsHeight){
   temp =  pow((unpassableObsHeight - pow(temp,tweak2)/pow(unpassableObsHeight,tweak2 - 1)) * zeroGradSpeed / unpassableObsHeight,tweak1)*pow(zeroGradSpeed,1.0-tweak1);
 
 
   // cout << tweak1 <<  " t1 t2 " << tweak1 << endl;
  //  cout << gradient << " gr  velo  " << temp  << endl;
  return temp;
  }
 else{
    return _inputMap->getLayerNoDataVal<double>(_costLayerNum);
  }
}
 

int
CCostPainter::growChanges (double UTMNorthing, double UTMEasting)
{
  double rowRes = _inputMap->getResRows ();
  double colRes = _inputMap->getResCols ();

  int minRow, minCol, maxRow, maxCol;

  minRow = (int) (-1.0 * _radius / rowRes) - 1;
  minCol = (int) (-1.0 * _radius / colRes) - 1;
  maxRow = (int) (_radius / rowRes) + 1;
  maxCol = (int) (_radius / colRes) + 1;

  int origRow, origCol;

  int localRow, localCol;

  _inputMap->UTM2Win (UTMNorthing, UTMEasting, &origRow, &origCol);

  double blNorthing, blEasting, tlNorthing, tlEasting;
  double trNorthing, trEasting, brNorthing, brEasting;

  double blDist, tlDist, trDist, brDist;

  int *rowMask;
  int *colMask;

  rowMask = new int[(maxRow - minRow) * (maxCol - minCol)];
  colMask = new int[(maxRow - minRow) * (maxCol - minCol)];

  int numPoints = 0;

  double minCost = 999.0;

  //minCost = ((double)(rand()%100))/-100.0*6.0;

  double localCost;

  double noData = _inputMap->getLayerNoDataVal < double >(_costLayerNum);

  for (int row = minRow; row <= maxRow; row++)
  {
    for (int col = minCol; col <= maxCol; col++)
    {
      localRow = row + origRow;
      localCol = col + origCol;

      localCost =
        _inputMap->getDataWin < double >(_costLayerNum, localRow, localCol);

      if (localCost != _inputMap->getLayerOutsideMapVal <
          double >(_growLayerNum))
      {
        _inputMap->Win2UTM (localRow, localCol, &blNorthing, &blEasting);
        tlNorthing = blNorthing + rowRes;
        trNorthing = blNorthing + rowRes;
        brNorthing = blNorthing;

        brEasting = blEasting + colRes;
        trEasting = blEasting + colRes;
        tlEasting = blEasting;

        blDist =
          sqrt (pow (blNorthing - UTMNorthing, 2) +
                pow (blEasting - UTMEasting, 2));
        brDist =
          sqrt (pow (brNorthing - UTMNorthing, 2) +
                pow (brEasting - UTMEasting, 2));
        tlDist =
          sqrt (pow (tlNorthing - UTMNorthing, 2) +
                pow (tlEasting - UTMEasting, 2));
        trDist =
          sqrt (pow (trNorthing - UTMNorthing, 2) +
                pow (trEasting - UTMEasting, 2));

        if (_conservativeGrowing)
        {
          if (blDist < _radius || brDist < _radius ||
              tlDist < _radius || trDist < _radius)
          {
            rowMask[numPoints] = localRow;
            colMask[numPoints] = localCol;
            if (localCost < minCost && localCost != noData)
              minCost = localCost;
            numPoints++;
          }
        }
        else
        {
          if (blDist < _radius && brDist < _radius &&
              tlDist < _radius && trDist < _radius)
          {
            rowMask[numPoints] = localRow;
            colMask[numPoints] = localCol;
            if (localCost < minCost && localCost != noData)
              minCost = localCost;
            numPoints++;
          }

        }

      }
    }
  }

  for (int i = 0; i < numPoints; i++)
  {
    if (_inputMap->getDataWin <
        double >(_growLayerNum, rowMask[i], colMask[i]) > minCost)
    {
      _inputMap->setDataWin_Delta < double >(_growLayerNum, rowMask[i],
                                             colMask[i], minCost);
    }
  }
  return 1;
}


int
CCostPainter::growChanges (NEcoord exposedArea[])
{
  int minRow, maxRow, minCol, maxCol;

  _inputMap->UTM2Win (exposedArea[0].N, exposedArea[0].E, &minRow, &minCol);
  _inputMap->UTM2Win (exposedArea[2].N, exposedArea[2].E, &maxRow, &maxCol);

  // force the values into range
  minRow = max (minRow - 2, 0);
  minCol = max (minCol - 2, 0);
  maxRow = min (maxRow + 2, _inputMap->getNumRows () - 1);
  maxCol = min (maxCol + 2, _inputMap->getNumCols () - 1);

  double tempN, tempE;

  for (int row = minRow; row <= maxRow; row++)
  {
    for (int col = minCol; col <= maxCol; col++)
    {
      _inputMap->Win2UTM (row, col, &tempN, &tempE);
      growChanges (tempN, tempE);
    }
  }

  return 0;
}

int CCostPainter:: calcAreaGradient( double UTMNorthing, double UTMEasting, double & gradientNorthing, double & gradientEasting){

int areaDimension = 2;
int nCount, eCount;
int i;

double n1, n2;
double e1, e2;

 double resRows = _inputMap->getResRows();
 double resCols = _inputMap->getResCols();

 i = 1; 
 n1 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing + areaDimension , UTMEasting);
 while ( n1 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum ) && i < areaDimension   ){
   n1 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing + areaDimension - i , UTMEasting);
   i++;
 }
 if(n1 ==  _inputMap->getLayerNoDataVal<double>(_elevLayerNum) ){
   return 0;  }
 
 nCount = areaDimension + 2 - i;
 
 i = 1; 
 n2 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing - areaDimension , UTMEasting);
 while ( n1 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum ) && i < areaDimension   ){
   
   n2 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing + areaDimension + i , UTMEasting);
   i++;
 }
 nCount += areaDimension + 1 - i;
 if(2 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum)){
   return 0;  }
 gradientNorthing = (n1-n2)/nCount/resRows;
 
 //take care of instance where a cell is surronded by no data
 
 i = 1; 
 e1 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing , UTMEasting + areaDimension);
 while ( e1 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum ) && i < areaDimension   ){
   e1 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing + areaDimension - i , UTMEasting);
   i++;
 }
 
 if(e1 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum)){
   return 0;  }
 eCount = areaDimension + 2 - i;
 
 i = 1; 
 e2 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing , UTMEasting + areaDimension);
 while ( e2 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum ) && i < areaDimension   ){
   e2 =  _inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing + areaDimension + i , UTMEasting);
   i++;
 }
 
 if(e2 == _inputMap->getLayerNoDataVal<double>(_elevLayerNum)){
   return 0;  }
 eCount += areaDimension + 1 - i;
 gradientEasting = (e1-e2)/eCount/resCols;
 
return 1;

}



double CCostPainter::gradientCorrection(double gradient, double areaGradient, double gradScaling){
  gradient =   fabs(gradient - areaGradient*gradScaling); 
#warning "this needs uberfication";
  return gradient;
}

void CCostPainter::setMapperOpts(FusionMapperOptions *  mapperOpts)
{
  _mapperOpts =* mapperOpts;

	//cout << "Setting kernel!" << endl;

	//if(logKernel!=NULL)
	//delete logKernel;
  int kernelRowSize = _mapperOpts.kernelSize;
  int kernelColSize = kernelRowSize;
	double sigma = _mapperOpts.sigma;

	//logKernel = new double[(kernelRowSize*2+1)*(kernelColSize*2+1)];
	double temp = 0.0;



	for(int r=0; r<kernelRowSize*2+1; r++) {
		for(int c=0; c<kernelColSize*2+1; c++) {
			logKernel[c+ r*(kernelRowSize*2+1)] = exp(-(pow((double)(r-kernelRowSize),2.0)+pow((double)(c-kernelColSize),2.0))/(2.0*pow(sigma,2.0)));
			temp+=logKernel[c+ r*(kernelRowSize*2+1)];
			//cout << r-kernelRowSize << "," << c-kernelColSize << " ";
		}
		//cout << endl;
	}
	//cout << endl;

	double secondtemp = 0.0;

	for(int r=0; r<kernelRowSize*2+1; r++) {
		for(int c=0; c<kernelColSize*2+1; c++) {
			logKernel[c+ r*(kernelRowSize*2+1)] /= temp;
			logKernel[c+ r*(kernelRowSize*2+1)] *= (pow((r-kernelRowSize), 2.0) + pow((c-kernelColSize), 2.0) - 2*pow(sigma,2.0))/(pow(sigma,4.0));
			secondtemp += logKernel[c+ r*(kernelRowSize*2+1)];
			//cout << r-kernelRowSize << "," << c-kernelColSize << " ";
		}
		//cout << endl;
	}
	//cout << endl;

	for(int r=0; r<kernelRowSize*2+1; r++) {
		for(int c=0; c<kernelColSize*2+1; c++) {
			logKernel[c+ r*(kernelRowSize*2+1)] -= secondtemp/((kernelRowSize*2+1)*(kernelColSize*2+1));
		}
	}

	//cout << "Done setting kernel" << endl;
}


int CCostPainter::repaintAll(bool paintSurrounding) {
  double UTMNorthing, UTMEasting;

  for(int row=0; row < _inputMap->getNumRows(); row++) {
    for(int col=0; col < _inputMap->getNumCols(); col++) {
      _inputMap->Win2UTM(row, col, &UTMNorthing, &UTMEasting);
      paintChanges(UTMNorthing, UTMEasting, paintSurrounding);
    }
  }

  return 1;
}


int CCostPainter::uberRepaintAll(bool paintSurrounding) {
  double UTMNorthing, UTMEasting;

  for(int row=0; row < _inputMap->getNumRows(); row++) {
    for(int col=0; col < _inputMap->getNumCols(); col++) {
      _inputMap->Win2UTM(row, col, &UTMNorthing, &UTMEasting);
      paintChangesUber(UTMNorthing, UTMEasting, paintSurrounding);
    }
  }

  return 1;
}


int CCostPainter::paintChangesUber(double UTMNorthing, double UTMEasting, bool paintSurrounding) {
  //how this will work:
  //
  //we min the final speed with the rddf speed for safe measure


  if (_inputMap->checkBoundsUTM (UTMNorthing, UTMEasting) != CMap::CM_OK)
    return 0;
  /*
  if(_inputMap->getDataUTM<double>(_corridorLayerNum, UTMNorthing, UTMEasting) ==
     _inputMap->getLayerNoDataVal<double>(_corridorLayerNum))
    return 0;
  */
	if(_inputMap->getDataUTM<double>(_elevLayerNum, UTMNorthing, UTMEasting) ==
		 _inputMap->getLayerNoDataVal<double>(_elevLayerNum))
		return 0;

  double finalSpeed = MAX_SPEED;
  double obstacleSpeed = MAX_SPEED;;
  //double bumpinessSpeed = 0.0;

  //double corridorReferenceSpeed = _inputMap->getDataUTM<double>(_corridorLayerNum, UTMNorthing, UTMEasting);
  CElevationFuser fusedElevData = _inputMap->getDataUTM<CElevationFuser>(_fusedLayerNum, UTMNorthing, UTMEasting);
  if(fusedElevData.getStdDev() > _mapperOpts.stdDevThreshold) {
    obstacleSpeed = 0.10001;
  } else {
    obstacleSpeed = MAX_SPEED;
  }

  int currentRow, currentCol;
  int kernelRowSize, kernelColSize;
  _inputMap->UTM2Win(UTMNorthing, UTMEasting, &currentRow, &currentCol);
   kernelRowSize = _mapperOpts.kernelSize;
   kernelColSize = kernelRowSize;
// 	double sigma = _mapperOpts.sigma;
  //double Gx_kernel[3][3] = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
  //double Gy_kernel[3][3] = {{1, 2, 1}, {0, 0, 0}, {-1, -2 ,1}};

// 	double* logKernel = new double[(kernelRowSize*2+1)*(kernelColSize*2+1)];
// 	double temp = 0.0;

// 	for(int r=0; r<kernelRowSize*2+1; r++) {
// 		for(int c=0; c<kernelColSize*2+1; c++) {
// 			logKernel[c+ r*(kernelRowSize*2+1)] = exp(-(pow((double)(r-kernelRowSize),2.0)+pow((double)(c-kernelColSize),2.0))/(2.0*pow(sigma,2.0)));
// 			temp+=logKernel[c+ r*(kernelRowSize*2+1)];
// 			//cout << r-kernelRowSize << "," << c-kernelColSize << " ";
// 		}
// 		//cout << endl;
// 	}
// 	//cout << endl;

// 	double secondtemp = 0.0;

// 	for(int r=0; r<kernelRowSize*2+1; r++) {
// 		for(int c=0; c<kernelColSize*2+1; c++) {
// 			logKernel[c+ r*(kernelRowSize*2+1)] /= temp;
// 			logKernel[c+ r*(kernelRowSize*2+1)] *= (pow((r-kernelRowSize), 2.0) + pow((c-kernelColSize), 2.0) - 2*pow(sigma,2.0))/(pow(sigma,4.0));
// 			secondtemp += logKernel[c+ r*(kernelRowSize*2+1)];
// 			//cout << r-kernelRowSize << "," << c-kernelColSize << " ";
// 		}
// 		//cout << endl;
// 	}
// 	//cout << endl;

// 	for(int r=0; r<kernelRowSize*2+1; r++) {
// 		for(int c=0; c<kernelColSize*2+1; c++) {
// 			logKernel[c+ r*(kernelRowSize*2+1)] -= secondtemp/49.0;
// 		}
// 	}


  int minRow, maxRow, minCol, maxCol;
  constrainRows(currentRow, kernelRowSize, minRow, maxRow);
  constrainCols(currentCol, kernelColSize, minCol, maxCol);

  //double totalAbsHeightDiff = 0.0;
  double cellElev;
  CElevationFuser cellFused;
  int numCellsUsed = 0;
  //double currentElev = _inputMap->getDataWin<double>(_elevLayerNum, currentRow, currentCol);
  double elevNoDataVal = _inputMap->getLayerNoDataVal<double>(_elevLayerNum);
  double speedNoDataVal = _inputMap->getLayerNoDataVal<double>(_costLayerNum);
  //double maxAbsHeightDiff = 0.0;
  //double Gx = 0.0;
  //double Gy = 0.0;
  //double totalStdDev = 0.0;
  //double avgStdDev = 0.0;
  //int numPointsUsed = 0;

  //double* weightVector = new double[kernelRowSize*kernelColSize];
  //double fortranPointArray = new double[kernelRowSize*kernelColSize*3];
  //double* pointArray = new double[kernelRowSize*kernelColSize*3];
  //double weightVector[121];
  //double pointArray[363];
  //double coeffs[3];

  double totalHeight = 0;
	double convolution = 0.0;
	//int numNoData = 0;

	//cout << "got " << 3/2 << endl;

	double otherN, otherE;

  for(int r = minRow; r<=maxRow; r++) {
    for(int c = minCol; c<=maxCol; c++) {
      cellElev = _inputMap->getDataWin<double>(_elevLayerNum, r, c);
      //cellFused = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, r, c);
      if(cellElev != elevNoDataVal) {
				totalHeight+=cellElev;
				/*
				if(!(r==currentRow && c==currentCol)) {
					totalHeight += cellElev;//*((double)cellFused.getNumPoints());
					numPointsUsed += 1;//cellFused.getNumPoints();
				}
				*/
				numCellsUsed++;
				if(paintSurrounding) {
					_inputMap->Win2UTM(r,c,&otherN, &otherE);
					paintChangesUber(otherN, otherE, false);
				}
			}
		}
	}
  double avgHeight = totalHeight/((double)numCellsUsed);
	double avgConvolution = 0.0;
	double sumConvolution = 0.0;

  for(int r = minRow; r<=maxRow; r++) {
    for(int c = minCol, i=0; c<=maxCol; c++, i++) {
      cellElev = _inputMap->getDataWin<double>(_elevLayerNum, r, c);
      //cellFused = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, r, c);
			//printf("% .4lf ", logKernel[c-currentCol + kernelColSize + (kernelRowSize*2+1)*(r-currentRow + kernelRowSize)]);
			avgConvolution+=(avgHeight*logKernel[c-currentCol + kernelColSize + (kernelRowSize*2+1)*(r-currentRow + kernelRowSize)]);
			sumConvolution+=fabs(logKernel[c-currentCol + kernelColSize + (kernelRowSize*2+1)*(r-currentRow + kernelRowSize)]);
      if(cellElev != elevNoDataVal) {
				//totalStdDev += cellFused.getStdDev()*cellFused.getNumPoints();
				//totalAbsHeightDiff += fabs(currentElev - cellElev);
				/*
					if(fabs(currentElev-cellElev) > maxAbsHeightDiff) {
					maxAbsHeightDiff = fabs(currentElev - cellElev);
					}
					Gx+=Gx_kernel[r-currentRow+1][c-currentCol+1]*cellElev;
					Gy+=Gy_kernel[r-currentRow-1][c-currentCol+1]*cellElev;
					weightVector[numCellsUsed] = (double)cellFused.getNumPoints();
					pointArray[numCellsUsed*3 + 0] = weightVector[numCellsUsed] * cellFused.getMeanN();
					pointArray[numCellsUsed*3 + 1] = weightVector[numCellsUsed] * cellFused.getMeanE();
					pointArray[numCellsUsed*3 + 2] = weightVector[numCellsUsed] * cellFused.getMeanElevation();
				*/
				//cout << c-currentCol + kernelColSize/2 + kernelRowSize*(r-currentRow + kernelRowSize/2) << " ";
				convolution += cellElev*fabs(logKernel[c-currentCol + kernelColSize + (kernelRowSize*2+1)*(r-currentRow + kernelRowSize)]);
				//totalHeight+=cellElev;
				/*
				if(!(r==currentRow && c==currentCol)) {
					totalHeight += cellElev;//*((double)cellFused.getNumPoints());
					numPointsUsed += 1;//cellFused.getNumPoints();
				}
				*/
				//numCellsUsed++;
      } else {
				convolution += avgHeight*fabs(logKernel[c-currentCol + kernelColSize + (kernelRowSize*2+1)*(r-currentRow + kernelRowSize)]);
				//totalStdDev += cellFused.getStdDev();
				//numCellsUsed++;
				//Gx+=Gx_kernel[r-currentRow+1][c-currentCol+1]*currentElev;
				//Gy+=Gy_kernel[r-currentRow+1][c-currentCol+1]*currentElev;
      }
    }
		//printf("\n");
  }
	//printf("\n");
  //double G = fabs(Gx) + fabs(Gy);
	//delete logKernel;

  if(numCellsUsed<3)
    return 0;

  double avgAbsHeightDiff;// = totalAbsHeightDiff / ((double)numCellsUsed);
  //avgStdDev = totalStdDev / ((double)numPointsUsed);
  //double avgHeight = totalHeight/((double)numCellsUsed);

  
  //double residual = 0;

  /*
  if(numCellsUsed > 3) {
    residual = calculateWeightedPlaneFitAndResidual(pointArray,
						    weightVector,
						    numCellsUsed,
						    coeffs);
  } else {
    //cout << "used " << numCellsUsed << " points" << endl;
  }
  */
  //delete pointArray;
  //delete weightVector;
  //finalSpeed = G;
  //finalSpeed = maxAbsHeightDiff;

  /*
  bumpinessSpeed = inverse(avgAbsHeightDiff, 1.70929,
		       0.02408, -8.77551, 0.44704);
  */

  //finalSpeed = avgAbsHeightDiff;
	  
  //finalSpeed = linear(fusedElevData.getStdDev(), -300.0, 30.0, 0.44704);
	  
  //finalSpeed = linear(fusedElevData.getStdDev(), -55.0, 30.0, 0.44704);

  //finalSpeed = linear(avgAbsHeightDiff, -55, 30.0, 0.44704);
	  
  //finalSpeed = linear(maxAbsHeightDiff, -140, 30.0, 0.44704);
	  
  //finalSpeed = avgStdDev;
	  
  //finalSpeed = fusedElevData.getStdDev()-avgStdDev;

  //finalSpeed = residual*1.0e5;

  finalSpeed = fabs(fusedElevData.getMeanElevation() - avgHeight);
  /*
  if(finalSpeed < 0)
    finalSpeed = 0;
  */
  //finalSpeed = inverse(fabs(fusedElevData.getMeanElevation() - avgHeight),
  //		       3.6584, 0.0755862, -8.27586);

  avgAbsHeightDiff = fabs(fusedElevData.getMeanElevation() - avgHeight);

  //finalSpeed = avgAbsHeightDiff;

  //finalSpeed = 0.44704*(-50/M_PI*tanh(50.0*(avgAbsHeightDiff-0.05))+25.0) - 4.0;
  finalSpeed = linear(avgAbsHeightDiff, -200.0, 30.0, 0.44704);
	finalSpeed = linear(fabs(convolution-sumConvolution*avgHeight), -10000, 30.0, 0.44704);
	finalSpeed = 0.44704*(-15*2/M_PI*tanh(1000.0*(fabs(convolution-sumConvolution*avgHeight) - 0.003))+15.0);

  //if(obstacleSpeed == 0.1) finalSpeed = 0.11;

  finalSpeed = fmin(finalSpeed, obstacleSpeed);
  //finalSpeed = fmin(finalSpeed, _inputMap->getDataUTM<double>(_corridorLayerNum, UTMNorthing, UTMEasting));
  //finalSpeed = fmax(finalSpeed, _inputMap->getLayerNoDataVal<double>(_costLayerNum));
  finalSpeed = fmax(finalSpeed, 0.1);


	//finalSpeed = fmax(finalSpeed,
	//cout << "got " << sumConvolution*avgHeight << endl;
	//finalSpeed = fabs((convolution - sumConvolution*avgHeight)*100.0);//((convolution-avgConvolution)*100.0);

  _inputMap->setDataUTM_Delta<double>(_costLayerNum, UTMNorthing, UTMEasting, finalSpeed);
	//cout << __FILE__ << "[" << __LINE__ << "]: " << endl;
	//return 0;

  constrainRows(currentRow, 1, minRow, maxRow);
  constrainCols(currentCol, 1, minCol, maxCol);

  int minLocalRow, maxLocalRow, minLocalCol, maxLocalCol;
  double localCellCost, localCellElev;
  double localNorthing, localEasting;
	double otherCost;

  for(int r=minRow; r<=maxRow; r++) {
    for(int c=minCol; c<=maxCol; c++) {
      cellElev = _inputMap->getDataWin<double>(_elevLayerNum, r, c);
			otherCost = _inputMap->getDataWin<double>(_costLayerNum, r, c);
      if(cellElev == elevNoDataVal &&
				 otherCost == speedNoDataVal) {
	constrainRows(r, 1, minLocalRow, maxLocalRow);
	constrainCols(c, 1, minLocalCol, maxLocalCol);
	double localCost=0.0;
	int numSupporters=0;
	_inputMap->Win2UTM(r, c, &localNorthing, &localEasting);
	//cout << "New cell! (" << setprecision(10) << localNorthing << ", " << localEasting << ") ";
	for(int localR=minLocalRow; localR<=maxLocalRow; localR++) {
	  for(int localC=minLocalCol; localC<=maxLocalCol; localC++) {
	    localCellCost = _inputMap->getDataWin<double>(_costLayerNum, localR, localC);
	    localCellElev = _inputMap->getDataWin<double>(_elevLayerNum, localR, localC);
	    if(localCellCost != 1.0 &&
	       localCellCost != 0.1 && 
	       localCellElev != elevNoDataVal) {
	      //cout << _inputMap->getDataWin<double>(_costLayerNum, localR, localC) << ", ";
	      localCost+=_inputMap->getDataWin<double>(_costLayerNum, localR, localC);
	      numSupporters++;
	    }
	  }
	}
	if(numSupporters >= 3) {
	  //cout << "Setting with a val of " << localCost/((double)numSupporters);
	  _inputMap->setDataWin_Delta<double>(_costLayerNum, r, c, fmax(0.1, localCost/((double)numSupporters)));
	}
	//cout << endl;
      }
    }
  }


}


int CCostPainter::paintChangesNotify(double UTMNorthing, double UTMEasting) {
	int currentRow, currentCol, minRow, minCol, maxRow, maxCol;
  int kernelRowSize = _mapperOpts.kernelSize;
  int kernelColSize = kernelRowSize;
	_inputMap->UTM2Win(UTMNorthing, UTMEasting, &currentRow, &currentCol);

  constrainRows(currentRow, kernelRowSize, minRow, maxRow);
  constrainCols(currentCol, kernelColSize, minCol, maxCol);
	//cout << "rows from " << minRow << " to " << maxRow << ", cols from " << minCol << " to " << maxCol << endl;
	for(int r=minRow; r	<= maxRow; r++) {
		for(int c=minCol; c<=maxCol; c++) {
			_inputMap->setDataWin_Delta<int>(_fusedYetLayerNum, r, c, 1);
		}
	}

	return 1;
}

int CCostPainter::paintNotifications() {
	//CDeltaList* deltaList = _inputMap->serializeDelta<int>(_fusedYetLayerNum, 0);

	double UTMNorthing=0.0, UTMEasting=0.0;

	int cellsize = _inputMap->getCurrentDeltaSize(_fusedYetLayerNum);
	//cout << "got " << cellsize << " different cells " << endl;
	for(int i=0; i<cellsize; i++) {
		int resultCell = _inputMap->getCurrentDeltaVal<int>(i, _fusedYetLayerNum, &UTMNorthing, &UTMEasting);				
		//cout << "Cell at " << UTMNorthing << ", " << UTMEasting << " = " << resultCell << endl;
		paintChangesUber(UTMNorthing, UTMEasting, false);
	}

	_inputMap->resetDelta<int>(_fusedYetLayerNum);

	return 1;
}


void CCostPainter::constrainRows(int currentRow, int numRows, int& minRow, int& maxRow) {
  constrainRange(currentRow, numRows, 0, _inputMap->getNumRows()-1, minRow, maxRow);
}

void CCostPainter::constrainCols(int currentCol, int numCols, int& minCol, int& maxCol) {
  constrainRange(currentCol, numCols, 0, _inputMap->getNumCols()-1, minCol, maxCol);
}

void CCostPainter::constrainRange(int start, int numDiff, int min, int max, int& resultMin, int& resultMax) {
  if(start - numDiff < min) {
    resultMin = min;
  } else {
    resultMin = start - numDiff;
  }

  if(start + numDiff > max) {
    resultMax = max;
  } else {
    resultMax = start + numDiff;
  }
}

  //Returns multipler*(a/(gradient+b)+c)
double CCostPainter::inverse(double gradient, double a, double b, double c, double multiplier) {
  return multiplier*(a/(gradient+b)+c);
}


//Returns multiplier*(m*gradient+b)
double CCostPainter::linear(double gradient, double m, double b, double multiplier) {
  return multiplier*(m*gradient+b);
}


