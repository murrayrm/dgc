/**
 * Author: Henrik Kjellander
 * $Id$
 */

#include <gazebo/ModelFactory.hh>

#include "Corridor.hh"
#include "CircleGeom.hh"
#include "LineGeom.hh"

//#include "RayGeom.hh"

#include <fstream> // ifstream
#include <cstdio> // getenv
#include <iostream>
#include <string>

#define DEBUG false // set this to true for more debug printinga

/////////////////////////////////////////////////////////////////////////////
//// Register the model
GZ_REGISTER_PLUGIN("Corridor", Corridor)

  //////////////////////////////////////////////////////////////////////////////
  // Constructor
  Corridor::Corridor( World *world )
    : Model( world ), ppCircles(NULL), ppLines(NULL)
{
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Destructor
Corridor::~Corridor()
{
  unsigned int i;

  if(ppCircles != NULL)
  {
    for(i=0; i < rddf.size()-1 ;i++)
    {
      delete ppCircles[i];
      delete ppLines[i];
    }
    delete ppCircles;
    delete ppLines;
  }
}


//////////////////////////////////////////////////////////////////////////////
// Load the sensor
int Corridor::Load( WorldFile *file, WorldFileNode *node )
{  
  // Create the canonical body
  this->body = new Body( this->world );
  this->AddBody( this->body, true );


  //
  // Open up the RDDF file
  // 
  printf("Reading RDDF waypoints from rddf.dat\n");
  RDDF rddfWhole("rddf.dat");
  rddf = rddfWhole.getTargetPoints();

  ppCircles = new CircleGeom*[2*rddf.size()];
  ppLines   = new LineGeom  *[2*rddf.size()];

  translateWaypoints();
  createCorridor();

  //Some code from an attempt to use the RayGeom instead
  /*GzVector a = GzVectorSet(0, 0, 1);
    GzVector b = GzVectorSet(40, 40, 1);
  
    dGeomSetCategoryBits((dGeomID) this->modelSpaceId, GZ_LASER_COLLIDE);
    dGeomSetCollideBits((dGeomID) this->modelSpaceId, ~GZ_LASER_COLLIDE);

    // A ray
    RayGeom *ray = new RayGeom(this->body, this->modelSpaceId);
    ray->SetColor( GzColor(0, 0, 1) );
    ray->SetCategoryBits( GZ_LASER_COLLIDE );
    ray->SetCollideBits( ~GZ_LASER_COLLIDE );
    ray->SetLength(GzVectorMag(GzVectorSub(b, a)));
    ray->Set(a, b);
  */
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize sensors
int Corridor::Init( WorldFile *file, WorldFileNode *node )
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize sensors
int Corridor::Fini()
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Update sensor information
void Corridor::Update( double step )
{
  // Get commands from the external interface
  //this->IfaceGetCmd();

  // Command any actuators
  
  // Update any sensors

  // Update the external interface with the new data
  //IfacePutData();
  return;
}

void Corridor::translateWaypoints()
{
  double n0 = rddf[0].Northing;
  double e0 = rddf[0].Easting;

  for(RDDFVector::iterator i = rddf.begin(); i != rddf.end(); i++)
  {
    (*i).Northing -= n0;
    (*i).Easting  -= e0;
    if(DEBUG) printf("x,y = %lf,%lf\n", (*i).Northing, (*i).Easting);
  }
}

void Corridor::createCorridor()
{
  // cycle throught each CORRIDOR SEGMENT
  for(unsigned int i=0; i < fmin(rddf.size()-1,100) ;i++)
  {
    RDDFData& w0 = rddf.at(i);
    RDDFData& w1 = rddf.at(i+1);

    ppCircles[2*i + 0] = new CircleGeom( body, modelSpaceId, w0.Easting, w0.Northing, w0.radius);
    ppCircles[2*i + 1] = new CircleGeom( body, modelSpaceId, w1.Easting, w1.Northing, w0.radius);

    // calculate the angle to the next waypoint
    double bearing = atan2( w1.Northing-w0.Northing,
			    w1.Easting -w0.Easting);

    // draw the left lateral offset line
    double fromE, fromN, toE, toN;
    //      coord        radius    perpedicular angle
    fromE = w0.Easting  + w0.radius * cos(bearing + 0.5*M_PI);
    fromN = w0.Northing + w0.radius * sin(bearing + 0.5*M_PI);
    toE = w1.Easting    + w0.radius * cos(bearing + 0.5*M_PI);
    toN = w1.Northing   + w0.radius * sin(bearing + 0.5*M_PI);
    ppLines[2*i + 0] = new LineGeom(body, modelSpaceId, fromE, fromN, toE, toN);
    if(DEBUG) printf("drawing left line\n");
      
    // draw the right lateral offset line
    //      coord        radius    perpedicular angle
    fromE = w0.Easting  + w0.radius * cos(bearing - 0.5*M_PI);
    fromN = w0.Northing + w0.radius * sin(bearing - 0.5*M_PI);
    toE = w1.Easting    + w0.radius * cos(bearing - 0.5*M_PI);
    toN = w1.Northing   + w0.radius * sin(bearing - 0.5*M_PI);
    ppLines[2*i + 1] = new LineGeom(body, modelSpaceId, fromE, fromN, toE, toN);      
    if(DEBUG) printf("drawing right line\n");
      
  }
}
