#include"sparrowhawk.hh"
#include"vddtable.h"
#include"vddtable2.h"
#include"vddtable3.h"

#include<time.h>
#include<iostream>
using namespace std;

class Test
{
 public:
  Test() : test_int(1), test_double(1.0), set1(0) {}

  void test_int_change() {
    test_double = (double)test_int;
    SparrowHawk().log("Integer value changed to %i", test_int);
  }
  
  void test_double_change() {
    test_int = (int)test_double;
    SparrowHawk().log("Double value changed to %d", test_double);
  }

  void set_int(int *ptr, int val) {
    if(val>10)
      val=10;
    if(val<0)
      val=0;
    *ptr = val;
  }
  
  void reset() {
    test_int=1;
    test_double=1.0;
  }

  //Accessor functions to protected data member
  int get_1() { return set1; }
  void set_1(int *p, int v, int i) { set1=v+i; }

 public:
  int test_int;
  double test_double;

 protected:
  int set1;
};

//Count number of updates
int counter() {
  static int i;
  return i++;
}

//global variable and accessor functions
int set2=0;
int get_2() { return set2; }
void set_2(int *p, int v) { set2=v; }

int main()
{
  CSparrowHawk &sh = SparrowHawk();

  Test t;

  sh.add_page(vddtable, "Main");
  sh.add_page(vddtable2, "Page2");
  sh.add_page(vddtable3, "Page3");

  sh.set_notify("CLICK ME", &t, &Test::reset);

  bool test_bool = true;

  //Set up new bindings
  sh.rebind("TestInt", &t.test_int);
  sh.rebind("TestDouble", &t.test_double);
  sh.rebind("TestBool", &test_bool);

  //Set notifications
  sh.set_notify("TestInt", &t, &Test::test_int_change);
  sh.set_notify("TestDouble", &t, &Test::test_double_change);

  //Set accessors
  sh.set_getter<int>("UpdateCounter", &counter);
  sh.set_setter("TestInt", &t, &Test::set_int);

  sh.set_getter("Set1", &t, &Test::get_1);
  sh.set_setter<int>("Set1", set_2);

  sh.set_getter<int>("Set2", get_2);
  sh.set_setter("Set2", &t, &Test::set_1, 10);

  cout<<"enter key to start"<<endl;
  char ch;
  //cin>>ch;

  sh.run();

  int cnt=0;
  do {
    usleep(1000000);
    sh.log("Total thread time %f", clock()/(double)CLOCKS_PER_SEC);
    ++cnt;

    char buf[100];
    sprintf(buf, "playing %i times", cnt);
    sh.set_string("MyString", buf);
    sh.set_fg_color("MyString", cnt % 8);
  } while(sh.running());

  return 0;
}
