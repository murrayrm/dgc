#ifndef PAINTERFUNCTORS_HH
#define PAINTERFUNCTORS_HH

#include"CCostPainter.hh"
#include"CMap.hh"

#include"Distances.hh"

#include<iostream>
#include<iomanip>
using namespace std;

//Defines the different ways we can paint road into map

//The one we normaly use is the first one, and in the definition
//of the operator function the transfer function from distance
//to speed is done

/*
**
** Paints colored on distance/width
**
*/
class PainterFunctorRelDistance
{
 public:
  PainterFunctorRelDistance(CMap *pMap, int layer, CCostPainter *pPainter, double speed)
    : m_pmap(pMap), m_layer(layer), m_ppainter(pPainter), dist(0,0,0,0,0,0), m_speed(speed)
  {
  }

  void set_center_line(double N1, double E1, double w1, double N2, double E2, double w2) {
    dist.set(N1,E1,w1, N2, E2,w2);
  }
  
  void set_speed(double s) { m_speed=s; }

  //Let the painted speed depend on the distance as:
  //                    f1
  //          ______   /
  //    /----/      \----\   <-s0
  //    |                |
  //   /                  \    <--- f2
  //   |                  |
  //                     ^
  //                    d0

  //Where f1 is first degree and f2 is first degree  (in d^2 so effectivly a 2nd degree)
  //Let the cutoffs be d0 and s0
  //
  // f1(d) = (s0-1)/d0 * d2 + 1
  //
  // f2(d) = s0/(d0-1) * d2 - s0/(d0-1)

  void operator()(double N, double E) {
    //paint
    double d2 = dist(N,E);

    const double s0 = .8;
    const double d0 = .8;

    if(d2<d0) {
      d2 = (s0-1)/d0 *d2 + 1;
    } else {
      d2 = s0/(d0-1) *d2 - s0/(d0-1);
    }

    m_pmap->setDataUTM(m_layer, N, E, d2 * m_speed);

    if(m_ppainter) {
      m_ppainter->paintChanges(N,E, 0);
    }
  }

 protected:
  CMap *m_pmap;
  int m_layer;
  CCostPainter *m_ppainter;
  RoadDistanceRel2 dist;
  double m_speed;
};


/*
**
** Paints distance scaled color
**
*/
class PainterFunctorDistance
{
 public:
  PainterFunctorDistance(CMap *pMap, int layer, CCostPainter *pPainter, double speed)
    : m_pmap(pMap), m_layer(layer), m_ppainter(pPainter), m_speed(speed), dist(0,0,0,0)
  {
  }

  void set_center_line(double N1, double E1, double N2, double E2) {
    dist.set(N1,E1, N2, E2);
  }

  void operator()(double N, double E) {
    //paint

    m_pmap->setDataUTM(m_layer, N, E, dist(N,E));
    //m_pmap->setDataUTM(m_layer, N, E, (N-m_N1)*(N-m_N1) + (E-m_E1)*(E-m_E1) );

    if(m_ppainter) {
      m_ppainter->paintChanges(N,E, 0);
    }
  }

 protected:
  CMap *m_pmap;
  int m_layer;
  CCostPainter *m_ppainter;
  double m_speed;
  RoadDistance2 dist;

  double m_N1, m_E1;
};

/*
**
** Paints a uniform color
**
*/
class PainterFunctorUniform
{
 public:
  PainterFunctorUniform(CMap *pMap, int layer, CCostPainter *pPainter, double val)
    : m_pmap(pMap), m_layer(layer), m_ppainter(pPainter), m_val(val)
  {
  }

  void operator()(double N, double E) {
    m_pmap->setDataUTM(m_layer, N, E, m_val);

    //cout<<"Painting " <<setprecision(10)<<N<<" " <<E<<endl;
    if(m_ppainter) {
      m_ppainter->paintChanges(N,E, 0);
    }
  }

 protected:
  CMap *m_pmap;
  int m_layer;
  CCostPainter *m_ppainter;
  double m_val;
};

#endif
