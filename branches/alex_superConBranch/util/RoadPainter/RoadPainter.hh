#ifndef ROADPAINTER_HH
#define ROADPAINTER_HH


#include"Road.hh"

#include<vector>
using namespace std;

//Forward declarations
class CMap;
class CCostPainter;
class CRasterization;

//Declares class RoadPainter to paint a road into the map
class CRoadPainter
{
 public:
  CRoadPainter();
  ~CRoadPainter();

  void paint(const Road &road, CMap *pMap, int layer, CCostPainter *pPainter=NULL);
  void paint(const Road &road, const Road &old_road, CMap *pMap, int layer, CCostPainter *pPainter=NULL);

 protected:   //Helper functions to paint the road
  double direction(double x, double y) const;    //direction in rads of vector

 protected:   //Data members
  CRasterization  *m_praster;
};

#endif
