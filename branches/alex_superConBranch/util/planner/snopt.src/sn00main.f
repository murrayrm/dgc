************************************************************************
*                                                                      *
*     File  sn00main.f                                                 *
*                                                                      *
*     Generic main program for stand-alone SNOPT.                      *
*                                                                      *
************************************************************************
*                                                                      *
*                               S N O P T                              *
*                                                                      *
*    Sparse Nonlinear Optimization by Sequential Quadratic Programming * 
*                                                                      *
*                      Version 5.3-4                  Oct 31, 1998     *
*                                                                      *
*    Philip E. Gill    Walter Murray           Michael A. Saunders     *
*    UC San Diego      Stanford University     Stanford University     *
*                                                                      *
*----------------------------------------------------------------------*
*     (C) 1992--1998  Regents of the University of California          *
*                     and the Trustees of Stanford University          *
*                                                                      *
*     This software is NOT in the public domain. Its use is governed   *
*     by a license agreement with either Stanford University or the    *
*     University of California.  It is a breach of copyright to make   *
*     copies except as authorized by the license agreement.            *
*                                                                      *
*     This material is based upon work partially supported by the      *
*     National Science Foundation under Grants DMI-9204208 and         *
*     DMI-9204547; and the Office of Naval Research Grant              *
*     N00014-90-J-1242.                                                *
************************************************************************
*
*  SNOPT Fortran source files:
*
*  1. sn00main   Main program for stand-alone SNOPT
*  2. sn01main   Main program for stand-alone SQOPT (LP)
*  3. sn09time   Machine-dependent timing routine
*  4. sn10unix   Machine-dependent routines
*  5. sn11user   Template for user-defined monitor routine
*  6. sn12npzz   Top-level NPOPT routines and auxiliaries
*  7. sn12sqzz   Top-level SQOPT routines and auxiliaries
*  8. sn12snzz   Top-level SNOPT routines and auxiliaries
*  9. sn15blas   Level-1 Basic Linear Algebra Subprograms (a subset)
* 10. sn17util   linear algebra subprograms
* 11. sn20amat   Routines for the manipulation of ( A  -I )
* 12. sn25bfac   Basis factorization routines
* 13. sn27LU     LU factorization routines
* 14. sn30spec   SPECS file routines
* 15. sn35mps    MPS file routines
* 16. sn40bfil   Basis file and solution output routines
* 17. sn50lp     Routines for the primal simplex method
* 18. sn55qp     Routines for quadratic programming
* 19. sn57qopt   QP and Memory allocation routines called by SQOPT 
* 20. sn60srch   Routines for the linesearch and subproblem obj
* 21. sn65rmod   For maintaining R, the approximate reduced Hessian
* 22. sn70nobj   Gradient checking routines
* 23. sn80ncon   Routines for handling nonlinear constraints
* 24. sn85Hess   Routines for handling the Hessian of the Lagrangian
* 25. sn87sopt   Sparse NP and Memory allocation routines.
* 26. sn90lmqn   Routines for updating a limited-memory BFGS Hessian
* 27. sn95fmqn   Routines for updating a full-memory    BFGS Hessian
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      program            snmps
      implicit           double precision (a-h,o-z)

*     ------------------------------------------------------------------
*     This is the default main program for SNOPT
*     It provides all of the necessary workspace.
*     ------------------------------------------------------------------
      parameter         (lencw = 150000, leniw = 300000, lenrw = 100000)
      character*8        cw(lencw)
      integer            iw(leniw)
      double precision   rw(lenrw)

      call snmps1( cw, lencw, iw, leniw, rw, lenrw )

*     end of main program for stand-alone SNOPT.
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snmps1( cw, lencw, iw, leniw, rw, lenrw )

      implicit           double precision (a-h,o-z)
      character*8        cw(lencw)
      integer            iw(leniw)
      double precision   rw(lenrw)

*     ==================================================================
*     snmps1 is used for the stand-alone version of the optimizer.
*     It is called by the main program (or equivalent driver).
*     It repeatedly looks for a new problem in the SPECS file
*     the and asks for it to be solved, until s3file returns inform gt 1,
*     which means an ENDRUN card was found in the SPECS file,
*     or end-of-file was encountered.
*
*     15 Nov 1991: First version.
*     14 Oct 1998: Current version of snmps1.
*     ==================================================================
      integer            Htype
      character*30       title
      integer            plInfy
      external           snwrap, funcon, funobj
      external           s3opt

      parameter         (plInfy    =  70)

      parameter         (maxru     =   2)
      parameter         (maxiu     =   4)
      parameter         (maxcu     =   6)

      parameter         (maxrw     =   3)
      parameter         (maxiw     =   5)
      parameter         (maxcw     =   7)

      parameter         (iSpecs    =  11)
      parameter         (iPrint    =  12)
      parameter         (iSumm     =  13)

      parameter         (nnCon     =  21)
      parameter         (nnJac     =  22)
      parameter         (nnObj     =  23)
      parameter         (nnL       =  24)
      parameter         (ngObj     =  26)
      parameter         (lvlTim    =  77)
      parameter         (lDenJ     = 105)
      parameter         (maxm      = 133)
      parameter         (maxn      = 134)
      parameter         (maxne     = 135)
*     -------------------------------------------------------------------

      if (lencw .lt. 500 .or. leniw .lt. 500 .or. lenrw .lt. 500) then 
*        ---------------------------------------------------------------
*        Not enough workspace to do ANYTHING!
*        ---------------------------------------------------------------
         inform = 41
         write(*, 9000)
         go to 999
      end if

*     ------------------------------------------------------------------
*     Define global files (reader, printer, etc.)
*     ------------------------------------------------------------------
      iw(iSpecs) = 4
      iw(iPrint) = 15
      iw(iSumm ) = 6
      call s1file( 'Default files to be opened', iw, leniw )

*     ==================================================================
*     Loop through each problem in the SPECS file.
*     ==================================================================
      do 100, loop = 1, 100000
         ncalls    = loop

         call s3undf( cw, lencw, iw, leniw, rw, lenrw )

*        ---------------------------------------------------------------
*        Initialize some global values.
*        ---------------------------------------------------------------
         rw(plInfy) = 1.0d+20
         iw(lvlTim) = 3

         iw(maxru) = 500        ! rw(1:500) contains snopt variables 
         iw(maxrw) = lenrw
         iw(maxiu) = 500        ! iw(1:500) contains snopt variables   
         iw(maxiw) = leniw
         iw(maxcu) = 500        ! cw(1:500) contains snopt variables   
         iw(maxcw) = lencw

*        Initialize timers.
      
         iw(lvlTim) = 1
         call s1time( 0, 0, iw, leniw, rw, lenrw )
      
*        Initialize dimensions that can be set in the specs file.

         iw(nnCon )  =   0
         iw(nnJac )  =   0
         iw(nnObj )  =   0
         iw(ngObj )  =   0
         iw(nnL   )  =   0
         iw(lDenJ )  =   1
      
         iw(maxm  )  =   0
         iw(maxn  )  =   0
         iw(maxne )  =   0

*        ---------------------------------------------------------------
*        Define the SNOPT title and read the Specs file.
*        Input values for  maxm,  maxn  and  maxne.
*        ---------------------------------------------------------------
         call sntitl( title )
         call s1init( title, iw, leniw, rw, lenrw )
         call s3fils( ncalls, iw(iSpecs), s3opt,
     $                title, iw(iPrint), iw(iSumm), inform,
     $                cw, lencw, iw, leniw, rw, lenrw )
         if (inform .ge. 2) then
            inform = 100 + inform 
            go to 999
         end if
      
         call s1file( 'Open files defined in specs file', iw, leniw  )

*        Set undefined MPS options to their default values.

         call s3dflt( cw, lencw, iw, leniw, rw, lenrw )

*        ---------------------------------------------------------------
*        Get values of the problem dimensions:
*           m    ,  n   , ne
*           nnObj
*           neJac, nnCon, nnJac
*        Initialize  a, ha, ka, nnJac, nnCon, neJac, 
*                 bl, bu, iObj, and  ObjAdd. 
*        Compute the array pointers accordingly.
*        ---------------------------------------------------------------
         call s1time( 1, 0, iw, leniw, rw, lenrw )
         call s3inpt( ierror,
     $                iw(maxm), iw(maxn), iw(maxne),
     $                iw(nnCon), iw(nnJac), iw(nnObj), 
     $                m, n, ne,
     $                iObj, ObjAdd,
     $                mincw, miniw, minrw,
     $                cw, lencw, iw, leniw, rw, lenrw )
         call s1time(-1, 0, iw, leniw, rw, lenrw )
         if (ierror .ne. 0) return

*        Record n, m, ne and iObj for s8dflt.

         iw( 15)    = m
         iw( 16)    = n
         iw( 17)    = ne
         iw(218)    = iObj

*        ---------------------------------------------------------------
*        Check options.
*        Open any files needed for this problem.
*        ---------------------------------------------------------------
         call s8dflt( 'Check parameters', 
     $                cw, lencw, iw, leniw, rw, lenrw )
         call s1file( 'Open files defined in specs file', iw, leniw  )
      
*        ---------------------------------------------------------------
*        Print the options if iPrint > 0, Print level > 0, lvlPrm > 0.
*        ---------------------------------------------------------------
         call s8dflt( 'Print parameters', 
     $                cw, lencw, iw, leniw, rw, lenrw )

*        ---------------------------------------------------------------
*        Compute the storage requirements for SNOPT  from the following
*        variables:
*           m    ,  n   , ne
*           maxS ,  maxR 
*           nnObj
*           neJac, nnCon, nnJac
*        All are now known.
*        ---------------------------------------------------------------
         neJac = iw( 20)
         maxR  = iw( 56)
         maxS  = iw( 57)

         call s8Mem ( ierror, iw(iPrint), iw(iSumm),
     $                m, n, ne, neJac,
     $                iw(nnCon), iw(nnJac), iw(nnObj),
     $                maxR, maxS, 
     $                iw(maxcw), iw(maxiw), iw(maxrw),
     $                lencw, leniw, lenrw,
     $                mincw, miniw, minrw, iw )
         if (ierror .gt. 0) go to 100

*        Fetch the addresses of the problem arrays (set in s3inpt).

         la      = iw(251)
         lha     = iw(252)
         lka     = iw(253)
         lbl     = iw(254)
         lbu     = iw(255)
         lxs     = iw(256)
         lpi     = iw(257)
         lhs     = iw(259)
         lNames  = iw(261)

         nb      = n   + m
         nka     = n   + 1 
         nName   = nb
         lrc     = lpi + m

*        ------------------------------------------------------------------
*        Solve the problem.
*        Tell s8solv that we don't have an initial Hessian.
*        ------------------------------------------------------------------
         Htype   = - 1
         call s8solv( 'Cold', Htype,
     $                m, n, nb, ne, nka, nname,
     $                iObj, ObjAdd, fObj, Objtru,
     $                nInf, sInf,
     $                snwrap, funCon, funObj,
     $                rw(la), iw(lha), iw(lka), rw(lbl), rw(lbu),
     $                cw(lNames),
     $                iw(lhs), rw(lxs), rw(lpi), rw(lrc),
     $                inform, nMajor, nS, 
     $                cw, lencw, iw, leniw, rw, lenrw,
     $                cw, lencw, iw, leniw, rw, lenrw  )

*        Print times for all clocks (if lvlTim > 0).

         call s1time( 0, 2, iw, leniw, rw, lenrw )

  100 continue
*     ==================================================================
*     End of loop through SPECS file.
*     ==================================================================

  999 return

 9000 format(  ' EXIT -- SNOPT character, integer and real work arrays',
     $         ' must each have at least 500 elements')

*     end of snmps1
      end

