#ifndef _SPECS_H_
#define _SPECS_H_

#define SPECLIST(_) \
  _(unsigned,MIN_PLANNING_CYCLETIME_US,       0  )	\
  _(double  ,PLANNING_LOOKAHEAD,              0.4) /* seconds ahead */ \
  _(double  ,LOOKAHEAD_DIST_PROXIMITY,        1.5) \
  _(double  ,LOOKAHEAD_SPEED_PROXIMITY,       3.0) \
  _(bool    ,NO_LOOKAHEAD,                    true) \
  _(int     ,MAX_POINTS_TO_PREPEND,           50) \
  _(double  ,OBSTACLE_AVOIDANCE_DELTA,        0.5) \
  _(int     ,OBSTACLE_AVOIDANCE_RAD,          0) \
  _(double  ,MIN_V,                           0.05) \
  _(double  ,MAXSPEED,                        5.0) \
  _(bool    ,USE_ONLY_THE_SEED_REFINEMENT,    false) \
  _(double  ,PATHGENSEED_MERGEDIST,           30.0) \
  _(double  ,ADJUSTABLE_HORIZON_MIN_SPEED,    7.0) \
  _(double  ,ADJUSTABLE_HORIZON_MAX_SPEED,    10.0) \
  _(double  ,ADJUSTABLE_HORIZON_MIN_TARGET,   27.0) \
  _(double  ,ADJUSTABLE_HORIZON_MAX_TARGET,   60.0) \
  _(bool    ,SEND_SEED_TRAJ,                  true) \
  _(bool    ,SEND_INTERM_TRAJ,                true) \
  _(bool    ,SEND_FINAL_TRAJ,                 true) \
  _(bool    ,SEND_FAILURES,                   false) \
  _(bool    ,LOG_SEED_TRAJ,                   true) \
  _(bool    ,LOG_INTERM_TRAJ,                 true) \
  _(bool    ,LOG_FINAL_TRAJ,                  true) \
  _(bool    ,DOPLOT,                          false) \
\
/* for the reactive stage */ \
  _(double  ,REACTIVE_DENSE_LENGTH,           30.0) \
  _(double  ,REACTIVE_LENGTH,		              70.0)  /* The total reactive length. This consists of a fine section REACTIVE_DENSE_LENGTH long and a sparser remaining section */ \
  _(double  ,SPACING_ACROSS_LAYER1,            2.5) /* point density and layer density close to the vehicle... */ \
  _(double  ,LAYER_SPACING1,                   5.0) \
  _(double  ,SPACING_ACROSS_LAYER2,            1.5) /* ... and further away from it */ \
  _(double  ,LAYER_SPACING2,                   20.0) \
  _(int     ,REACTIVE_OUTPUT_POINTS,          100) \
  _(double  ,NODE_CUTOFF_THRESHOLD,           1.0) \
  _(int     ,MAX_POINTS_PER_LAYER,            10) \
	_(double  ,FIRSTSTAGE_MAX_REUSE_SEPARATION, 10.0) \
\
	_(bool    ,SPEED_OPTIMIZE_FAILURES,         false) \
  _(double  ,ROLLOVER_TWEAK,                  0.1) \
  _(double  ,SPEED_SEED_SCALE_FACTOR,         1.0) \
  _(double  ,SPEED_SEED_SCALE_BIAS,           0.3) \
  _(double  ,NUM_IGNORED_CONSTRAINTS,         1) /* how many constraints are ignored at the start of the trajectory. Ignore 1 since the first point is uncontrollable anyway */ 	\
	_(bool    ,SEED_MAXDECEL,                   1) \
	_(bool    ,SEED_MAXACCEL,                   0) \
	_(bool    ,SEED_NOACCEL,                    0) \
	_(bool    ,SEED_SPEED_FROM_MAP,             0) \
  _(double  ,EXTRAFACTOR_DIST,              1.0) \
  _(double  ,EXTRAFACTOR_THETA,             1.0) \
  _(double  ,EXTRAFACTOR_ACCEL,             1.0) \
  _(double  ,EXTRAFACTOR_YAWDOT,            1.0) \
	_(double  ,EXTRAFACTOR_SPEED,             1.0) \
	_(double  ,EXTRAFACTOR_TANPHI,            1.0) \
	_(double  ,EXTRAFACTOR_PHIDOT,            1.0) \
	_(double  ,EXTRAFACTOR_ROLLOVER,          1.0) \
  _(double  ,EXTRAFACTOR_TIME,              5.0) \
  _(double  ,EXTRAFACTOR_STEER_EFFORT,      1.0) \
  _(double  ,HALF_TARGET_ERROR_LONG,        0.2) \
	_(double  ,HALF_TARGET_ERROR_LAT,         0.5) \
  _(double  ,MAPACCESS_CENTER_FROM_REAR,    0.0) \
  _(double  ,MAPSAMPLE_WIDTH,               3.5) \
  _(double  ,MAPSAMPLE_WIDTH_TRANSITION,    1.7) \
  _(double  ,MAPSAMPLE_LENGTH,              2.0) \
  _(double  ,MAPSAMPLE_LENGTH_TRANSITION,   1.0) \
  _(double  ,MAPSAMPLE_WINDOW_MIN,          0.00001) \
  _(double  ,MAPSAMPLE_BIAS,                0.0 ) \
  _(double  ,MAPSAMPLE_KERNLONG_T0,          1.0) \
  _(double  ,MAPSAMPLE_KERNLONG_T2,           -2.00111) \
  _(double  ,MAPSAMPLE_KERNLONG_T4,            1.00200) \
  _(double  ,MAPSAMPLE_KERNLONG_B0,            0.96931) \
  _(double  ,MAPSAMPLE_KERNLONG_B2,           -0.08646) \
  _(double  ,MAPSAMPLE_KERNLONG_B4,          -24.66897) \
  _(double  ,MAPSAMPLE_KERNLONG_B6,          112.63841) \
  _(double  ,MAPSAMPLE_KERNLAT_T0,           1.0 )\
  _(double  ,MAPSAMPLE_KERNLAT_T2,          -0.65336 )\
  _(double  ,MAPSAMPLE_KERNLAT_T4,     			 0.10680 )\
  _(double  ,MAPSAMPLE_KERNLAT_B0,     			 0.96559 )\
  _(double  ,MAPSAMPLE_KERNLAT_B2,     			-0.03893 )\
  _(double  ,MAPSAMPLE_KERNLAT_B4,     			-2.37508 )\
  _(double  ,MAPSAMPLE_KERNLAT_B6,     			 3.28061 )\
  _(bool    ,USE_INITIAL_YAWRATE,     			 0 )\
  _(bool    ,USE_INITIAL_ACCEL,     			   0 )\
/* This variable specifies the margin of error allowed for the nonlinear \
equality constraints. Specified as a fraction of the maximum value. So 0.1 \
here will translate into a 10% of maximum acceleration allowed error on the \
initial acceleration term, for example */ \
	_(double  ,NONLINEAR_MARGIN,               0.1)




#define EXTERNDEFINESPEC(type, name, val) \
extern type name;

#define DEFINESPEC(type, name, val) \
type name = val;

#define EXTERNDEFINESPECS namespace specs { SPECLIST(EXTERNDEFINESPEC) }; using namespace specs
#define DEFINESPECS namespace specs { SPECLIST(DEFINESPEC)	}; using namespace specs


EXTERNDEFINESPECS;


extern void readspecs(void);

#endif // _SPECS_H_
