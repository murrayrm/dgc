#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include "AliceConstants.h"
#include "CMapPlus.hh"
#include "RefinementStage.h"
#include "DGCutils"
#include "MapAccess.h"


extern "C" int snoptm_(char *start, int *m, int *n, int *ne,
											 int *nname, int *nncon, int *nnobj, int *nnjac, 
											 int *iobj, double *objadd, char *prob,
											 int (*)(int *, int *, int *, int *, int *, double *, double *, double *, double *, double *, int *, char *, int *, int *, int *, double *, int *, int), 
											 double *a, int *ha, int *ka, double *bl, double *
											 bu, char *names, int *hs, double *xs, double *pi, 
											 double *rc, int *inform__, int *mincw, int *miniw, 
											 int *minrw, int *ns, int *ninf, double *sinf, 
											 double *obj, char *cu, int *lencu, int *iu, int *
											 leniu, double *ru, int *lenru, char *cw, int *lencw, 
											 int *iw, int *leniw, double *rw, int *lenrw, int 
											 start_len, int prob_len, int names_len, int cu_len, int 
											 cw_len);
extern "C" int snopt_(char *, int *, int *, int *,
											int *, int *, int *, int *, int *, 
											double *, char *,
											int (*)(int *, int *, int *, int *, double *x, double *, double *, int *, char *, int *, int *, int *, double *, int *, int),
											int (*)(int *, int *, double *, double *, double *, int *, char *, int *, int *, int *, double *, int *, int),
											double *, int *, 
											int *, double *, double *, char *, int *, 
											double *, double *, double *, int *, int *, 
											int *, int *, int *, int *, double *, 
											double *, char *, int *, int *, int *, double *,
											int *, char *, int *, int *, int *, double *,
											int *, int, int, int, int, int);
extern "C" int snseti_(char *,    int *, int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);
extern "C" int snsetr_(char *, double *, int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);
extern "C" int sninit_(int *, int *, char *, int *, int *, int *, double *, int *, int);
extern "C" int snset_ (char *,    			 int *, int *, int *, char *, int *, int *, int *, double *, int *, int, int);

#ifdef DO_BENCHMARK
unsigned long long costtime = 0;
unsigned long long integrateNEtime = 0;
unsigned long long timetotal = 0;
#endif

#define min(a,b) ((a)<(b) ? (a) : (b))

int SNOPTfuncConstrCost(int *mode, int *nnobj, int *nncon, int *nnjac, 
												int *nejac, double *pSolverState, double *fObj, double *gObj,
												double *fCon, double *gCon, int *nstate,
												char *cu, int *lencu, int *iu, int *leniu, double *ru, int *lenru, int cu_len)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

	CRefinementStage* pRefinementStage = (CRefinementStage*)cu;
	if(memcmp(pRefinementStage->m_pPrevSolverState, pSolverState, (*nnobj)*sizeof(pSolverState[0])) != 0)
	{
		memcpy(pRefinementStage->m_pPrevSolverState, pSolverState, (*nnobj)*sizeof(pSolverState[0]));
		pRefinementStage->makeCollocationData(pSolverState);
		pRefinementStage->funcConstr(pSolverState);
		pRefinementStage->funcCost(pSolverState);
	}

	if(*mode == 2 || *mode == 0)
	{
		*fObj = pRefinementStage->m_obj;
		memcpy(fCon, pRefinementStage->m_pConstrData, (*nncon)*sizeof(fCon[0]));
	}
	if(*mode == 2 || *mode == 1)
	{
		memcpy(gObj, pRefinementStage->m_pCostGradData, (*nnobj)*sizeof(gObj[0]));
		memcpy(gCon, pRefinementStage->m_pConstrGradData, (*nejac)*sizeof(gCon[0]));
	}

#ifdef DO_BENCHMARK
	DGCgettime(t2);
	costtime += t2-t1;
#endif

	return 0;
}

int SNOPTfuncConstr(int *mode, int *nncon, int *nnjac, 
										int *nejac, double *pSolverState, double *fCon, double *gCon, int *nstate,
										char *cu, int *lencu, int *iu, int *leniu, double *ru, int *lenru, int cu_len)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

	CRefinementStage* pRefinementStage = (CRefinementStage*)cu;
	if(memcmp(pRefinementStage->m_pPrevSolverState, pSolverState, (*nnjac)*sizeof(pSolverState[0])) != 0)
	{
		memcpy(pRefinementStage->m_pPrevSolverState, pSolverState, (*nnjac)*sizeof(pSolverState[0]));
		pRefinementStage->makeCollocationData(pSolverState);
		pRefinementStage->funcConstr(pSolverState);
		pRefinementStage->funcCost(pSolverState);
	}

	if(*mode == 2 || *mode == 0)
		memcpy(fCon, pRefinementStage->m_pConstrData, (*nncon)*sizeof(fCon[0]));
	if(*mode == 2 || *mode == 1)
		memcpy(gCon, pRefinementStage->m_pConstrGradData, (*nejac)*sizeof(gCon[0]));

#ifdef DO_BENCHMARK
	DGCgettime(t2);
	costtime += t2-t1;
#endif
	return 0;
}
int SNOPTfuncCost(int *mode, int *nnobj,
									double *pSolverState, double *fObj, double *gObj, int *nstate,
									char *cu, int *lencu, int *iu, int *leniu, double *ru, int *lenru, int cu_len)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
#endif

	CRefinementStage* pRefinementStage = (CRefinementStage*)cu;
	if(memcmp(pRefinementStage->m_pPrevSolverState, pSolverState, (*nnobj)*sizeof(pSolverState[0])) != 0)
	{
		memcpy(pRefinementStage->m_pPrevSolverState, pSolverState, (*nnobj)*sizeof(pSolverState[0]));
		pRefinementStage->makeCollocationData(pSolverState);
		pRefinementStage->funcConstr(pSolverState);
		pRefinementStage->funcCost(pSolverState);
	}

	if(*mode == 2 || *mode == 0)
		*fObj = pRefinementStage->m_obj;
	if(*mode == 2 || *mode == 1)
		memcpy(gObj, pRefinementStage->m_pCostGradData, (*nnobj)*sizeof(gObj[0]));

#ifdef DO_BENCHMARK
	DGCgettime(t2);
	costtime += t2-t1;
#endif

	return 0;
}

CRefinementStage::~CRefinementStage()
{
	int i;

	delete m_pTraj;

	// free memory
	delete[] m_SNOPTmatrix;
	delete[] ha;
	delete[] ka;
	delete[] hs;
	delete[] pi;
	delete[] rc;

	delete[] m_pSolverState;
	delete[] m_pPrevSolverState;
	delete[] m_solverLowerBounds;
	delete[] m_solverUpperBounds;

	delete[] m_splinecoeffsTheta;
	delete[] m_PlannerLowerBounds;
	delete[] m_PlannerUpperBounds;

	delete[] m_valcoeffs;
	delete[] m_d1coeffs;
	delete[] m_d2coeffs;
	delete[] m_valcoeffsfine;
	delete[] m_d1coeffsfine;
	delete[] m_d2coeffsfine;
	delete[] m_valcoeffs_speed;
	delete[] m_d1coeffs_speed;
	delete[] m_valcoeffsfine_speed;
	delete[] m_d1coeffsfine_speed;

	delete[] m_stateestimatematrix;
	delete[] m_speedestimatematrix;
	delete[] m_speedestimatematrixfine;

	delete[] m_pColl_theta;
	delete[] m_pColl_dtheta;
	delete[] m_pColl_ddtheta;
	delete[] m_pColl_speed;
	delete[] m_pColl_dspeed;
	delete[] m_pCollN;
	delete[] m_pCollE;
	delete[] m_pCollGradN;
	delete[] m_pCollGradE;
	delete[] m_pCollVlimit;
	delete[] m_pCollDVlimitDN;
	delete[] m_pCollDVlimitDE;
	delete[] m_pCollDVlimitDTheta;

	delete[] m_pCollGradVlimit;

	delete[] m_pConstrData;
	delete[] m_pConstrGradData;
	delete[] m_pCostGradData;

	for(i=0;
			i<NUM_NLIN_INIT_CONSTR +
				NUM_NLIN_TRAJ_CONSTR +
				NUM_NLIN_FINL_CONSTR;
			i++)
	{
		delete[] m_grad_solver[i];
	}
	delete[] m_grad_solver;
	delete[] m_dercoeffsmatrix;

	delete[] m_pOutputTheta;
	delete[] m_pOutputDTheta;
	delete[] m_pOutputSpeed;
	delete[] m_pOutputDSpeed;

	delete[] m_pThetaVector;
	delete m_pSeedTraj;

	delete[] cw;
	delete[] iw;
	delete[] rw;
}

void CRefinementStage::outputTraj(CTraj* pTraj)
{
	int i;
	double NEout[6];
	double n,e;


	char notrans = 'N';
	int rows = NUM_COLLOCATION_POINTS_FINE;
	int cols = NUM_POINTS;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffsfine, &rows,
				 m_pSolverState, &incr, &beta, m_pOutputTheta, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffsfine, &rows,
				 m_pSolverState, &incr, &beta, m_pOutputDTheta, &incr);

	cols = NUM_SPEED_SEGMENTS;
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffsfine_speed, &rows,
				 m_pSolverState+NUM_POINTS, &incr, &beta, m_pOutputSpeed, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffsfine_speed, &rows,
				 m_pSolverState+NUM_POINTS, &incr, &beta, m_pOutputDSpeed, &incr);


	n = e = 0.0;

	getSplineCoeffsTheta(m_pSolverState);

	pTraj->startDataInput();
	for(i=0; i<NUM_COLLOCATION_POINTS_FINE-1; i++)
	{
		m_pOutputTheta[i] += m_initTheta;
		m_pOutputSpeed[i] += m_initSpeed;

		NEout[0] = m_pSolverState[NPnumVariables-1]*n + m_initN;
		NEout[3] = m_pSolverState[NPnumVariables-1]*e + m_initE;
		NEout[1] = m_pOutputSpeed[i]*cos(m_pOutputTheta[i]);
		NEout[4] = m_pOutputSpeed[i]*sin(m_pOutputTheta[i]);
		NEout[2] = m_pOutputSpeed[i] / m_pSolverState[NPnumVariables-1] *
			( m_pOutputDSpeed[i] * cos(m_pOutputTheta[i]) - m_pOutputSpeed[i]*m_pOutputDTheta[i]*sin(m_pOutputTheta[i]) );
		NEout[5] = m_pOutputSpeed[i] / m_pSolverState[NPnumVariables-1] *
			( m_pOutputDSpeed[i] * sin(m_pOutputTheta[i]) + m_pOutputSpeed[i]*m_pOutputDTheta[i]*cos(m_pOutputTheta[i]) );
		pTraj->inputWithDiffs(NEout+0, NEout+3);

		integrateNEfine(i, n, e);
	}
}

void CRefinementStage::MakeTrajC2(CTraj* pTraj)
{
	m_initTheta = atan2(pTraj->getEastingDiff(0, 1), pTraj->getNorthingDiff(0, 1));
	m_initSpeed = hypot(pTraj->getEastingDiff(0, 1), pTraj->getNorthingDiff(0, 1));
	m_initN = pTraj->getNorthingDiff(0, 0);
	m_initE = pTraj->getEastingDiff (0, 0);

	double *pThetaVector = new double[NUM_COLLOCATION_POINTS_FINE];
	double *pSpeedVector = new double[NUM_COLLOCATION_POINTS_FINE];
	double dist = pTraj->getLength();

	pTraj->getThetaVector(dist, NUM_COLLOCATION_POINTS_FINE, pThetaVector, m_initTheta);
	pTraj->getSpeedVector(dist, NUM_COLLOCATION_POINTS_FINE, pSpeedVector, m_initSpeed);

	char notrans = 'N';
	int rows = NUM_POINTS;
	int cols = NUM_COLLOCATION_POINTS_FINE;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_stateestimatematrix, &rows,
				 pThetaVector, &incr, &beta, m_pSolverState, &incr);

	rows = NUM_SPEED_SEGMENTS;
	dgemv_(&notrans, &rows, &cols, &alpha, m_speedestimatematrixfine, &rows,
				 pSpeedVector, &incr, &beta, m_pSolverState+NUM_POINTS, &incr);
	m_pSolverState[NPnumVariables-1] = dist;


	delete[] pThetaVector;
	delete[] pSpeedVector;

	outputTraj(pTraj);
}

CTraj* CRefinementStage::getSeedTraj(void)
{
	return m_pSeedTraj;
}

#define n								m_pCollN[collIndex]
#define e								m_pCollE[collIndex]
// this function evaluates the spatial part of the current solver state
// through the map to seed the temporal part with a least squares fit of the
// map-based speeds. This function is similar to CRefinementStage::SeedFromTraj
void CRefinementStage::SeedSpeedFromState(void)
{
	// first, compute theta(s) on a solver (not fine) collocation spacing.
	int collIndex;
	char notrans = 'N';
	int rows = NUM_COLLOCATION_POINTS;
	int cols = NUM_POINTS;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	// Store this theta(s) vector into m_pOutputTheta
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffs, &rows,
				 m_pSolverState, &incr, &beta, m_pOutputTheta, &incr);

	// Now integrate the trajectory to find the map speeds at each location
	collIndex = 0;
	n = 0.0;
	e = 0.0;
	for(collIndex=0; collIndex<NUM_COLLOCATION_POINTS; collIndex++)
	{
		double dummy;
		getContinuousMapValueDiffGrown(m_pMap, m_mapLayer,
																	 n*m_pSolverState[NPnumVariables-1] + m_initN,
																	 e*m_pSolverState[NPnumVariables-1] + m_initE,
																	 m_pOutputTheta[collIndex] + m_initTheta,
																	 &m_pSpeedVector[collIndex], &dummy, &dummy, &dummy);

		m_pSpeedVector[collIndex] *= SPEED_SEED_SCALE_FACTOR;
		m_pSpeedVector[collIndex] -= (m_initSpeed + SPEED_SEED_SCALE_BIAS);

		if(collIndex != NUM_COLLOCATION_POINTS-1)
			integrateNE(collIndex);
	}

	// Now compute a least squares fit into the speed data
  notrans = 'N';
	rows = NUM_SPEED_SEGMENTS;
	cols = NUM_COLLOCATION_POINTS;
	alpha = 1.0;
	beta  = 0.0;
	incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_speedestimatematrix, &rows,
				 m_pSpeedVector, &incr, &beta, m_pSolverState+NUM_POINTS, &incr);
}
#undef n
#undef e

bool CRefinementStage::SeedFromTraj(CTraj* pTraj, double dist)
{
	double trajlen = pTraj->getLength();

	// if the distance isn't specified, use the whole length of the traj
	// if the traj isn't long enough to accomodate the seed, return an error
	if(dist == 0.0)
		dist = trajlen;
	else if(trajlen < dist)
		return false;

	pTraj->getThetaVector(dist, NUM_COLLOCATION_POINTS_FINE, m_pThetaVector, m_initTheta);

	char notrans = 'N';
	int rows = NUM_POINTS;
	int cols = NUM_COLLOCATION_POINTS_FINE;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	dgemv_(&notrans, &rows, &cols, &alpha, m_stateestimatematrix, &rows,
				 m_pThetaVector, &incr, &beta, m_pSolverState, &incr);
	m_pSolverState[NPnumVariables-1] = dist;

	getSplineCoeffsTheta(m_pSolverState);

	setTargetToDistAlongSpline(1.0);
	return true;
}

void CRefinementStage::setTargetToDistAlongSpline(double distRatio)
{
	int i;
	int finalPoint;

	finalPoint = min(lround(distRatio * (double)NUM_COLLOCATION_POINTS_FINE), NUM_COLLOCATION_POINTS_FINE-1);

	m_targetN = m_targetE = 0.0;
	for(i=0;
			i < finalPoint;
			i++)
	{
		integrateNEfine(i, m_targetN, m_targetE);
	}
	m_targetN *= m_pSolverState[NPnumVariables-1];
	m_targetE *= m_pSolverState[NPnumVariables-1];
	m_targetTheta =
		m_splinecoeffsTheta[3*(NUM_POINTS-2) + 0] + 
		m_splinecoeffsTheta[3*(NUM_POINTS-2) + 1] + 
		m_splinecoeffsTheta[3*(NUM_POINTS-2) + 2];
}



// pVehState is still the state from the astate
int CRefinementStage::run(VehicleState* pVehState, CTraj* pSeedTraj, bool bIsStateFromAstate)
{
#ifdef DO_BENCHMARK
	unsigned long long t1, t2;
	DGCgettime(t1);
	costtime = 0;
	integrateNEtime = 0;
	timetotal = 0;
#endif

	double speedAtRearAxle;
	if(bIsStateFromAstate)
	{
		speedAtRearAxle = hypot(pVehState->Vel_N + VEHICLE_WHEELBASE*pVehState->YawRate*sin(pVehState->Yaw), 
														pVehState->Vel_E - VEHICLE_WHEELBASE*pVehState->YawRate*cos(pVehState->Yaw));

		m_initSpeed		= fmax(MIN_V, speedAtRearAxle);
		m_initTheta		= pVehState->Yaw;
		m_initYawRate = pVehState->YawRate;
		m_initN				= pVehState->Northing - VEHICLE_WHEELBASE*cos(pVehState->Yaw);
		m_initE				= pVehState->Easting  - VEHICLE_WHEELBASE*sin(pVehState->Yaw);

		// note: this is wrong. fucking front state
		m_initAcc     = (pVehState->Vel_N*pVehState->Acc_N + pVehState->Vel_E*pVehState->Acc_E) / speedAtRearAxle;
	}
	else
	{
		speedAtRearAxle = hypot(pVehState->Vel_N, pVehState->Vel_E);

		m_initSpeed		= fmax(MIN_V, speedAtRearAxle);
		m_initTheta		= pVehState->Yaw;
		m_initYawRate = pVehState->YawRate;
		m_initN				= pVehState->Northing;
		m_initE				= pVehState->Easting;
		m_initAcc     = (pVehState->Vel_N*pVehState->Acc_N + pVehState->Vel_E*pVehState->Acc_E) / speedAtRearAxle;
	}

	if(m_initSpeed < ADJUSTABLE_HORIZON_MIN_SPEED)
		PLANNING_TARGET_DIST = ADJUSTABLE_HORIZON_MIN_TARGET;
	else if(m_initSpeed > ADJUSTABLE_HORIZON_MAX_SPEED)
		PLANNING_TARGET_DIST = ADJUSTABLE_HORIZON_MAX_TARGET;
	else
		PLANNING_TARGET_DIST = (m_initSpeed - ADJUSTABLE_HORIZON_MIN_SPEED)/(ADJUSTABLE_HORIZON_MAX_SPEED - ADJUSTABLE_HORIZON_MIN_SPEED)*
			(ADJUSTABLE_HORIZON_MAX_TARGET - ADJUSTABLE_HORIZON_MIN_TARGET) + ADJUSTABLE_HORIZON_MIN_TARGET;



// // 	double a,b,c,d;
// // 	m_initN = m_initE = 0.0;
// // 	getContinuousMapValueDiffGrown(3834372.27, 442597.5, 135.0*M_PI/180.0, &a, &b, &c, &d);
// // 	return 0;

// 	cout << " starting to output sp3" << endl;
// 	m_initN = m_initE = 0.0;
// 	double nmid = 3777387.75;
// 	double emid = 403477.25;

// 	double nlow = nmid - 2.0;
// 	double elow = emid - 2.0;
// 	double nhigh= nmid + 2.0;
// 	double ehigh= emid + 2.0;
// 	double resl = 0.1;
// 	double n,e,v,dvn,dve,dvdyaw;

// 	double yaw=0.0;

// 	getContinuousMapValueDiffGrown(m_pMap, m_mapLayer,
// 																 nmid, emid ,yaw,&v,&dvn,&dve,&dvdyaw);
// 	cout << v << endl;
// 	ofstream sp3("sp3.dat");
// 	sp3 << setprecision(10);

// 	for(n=nlow; n<=nhigh; n+=resl)
// 	{
// 		for(e=elow;e<=ehigh;e+=resl)
// 		{
// 			getContinuousMapValueDiffGrown(m_pMap, m_mapLayer, n,e,yaw,&v,&dvn,&dve,&dvdyaw);
// 			sp3 << v << ' ';
// 		}
// 		sp3 << endl;
// 	}
// 	exit(1);





	// if we couldn't set up the problem (probably because we were too close to
	// the end, return. Note that we're passing the astate state into this
	// function
	if(!SetUpPlannerBoundsAndSeedCoefficients(pSeedTraj))
		return -2;

	// make the bounds arrays for SNOPT
	makeSolverBoundsArrays();

	// zero out the previous solver state, since there is none
	memset(m_pPrevSolverState, 0, (NPnumVariables)*sizeof(m_pPrevSolverState[0]));

// 	double asdf[11] = {-3.1526015312158786,-2.8088311304172127,0.093214528023877349,3.0229032627714303,1.5115091178747668,-2.3217007625102601,-1.5600698414849232,0.82443778173860915,1.431700685086517,-0.013708444128843304,66.767714035427019};
// 	memcpy(m_pSolverState, asdf, sizeof(asdf));
// 	outputTraj(m_pTraj);
// 	ofstream seedfil("seedfil");
// 	m_pTraj->print(seedfil);
// 	exit(1);

	if(!m_bOnlySeed)
		CallSolver();
	else
		inform = 0;

	// 	if(inform == 0)
	{
		outputTraj(m_pTraj);
	}

#ifdef DO_BENCHMARK
	DGCgettime(t2);
	timetotal += t2-t1;

 	cout << setprecision(3);
	if(!m_bOnlySeed)
	{
		cout << "Time integrateNE: " << DGCtimetosec(integrateNEtime) << endl;
		cout << "Time cost: " << DGCtimetosec(costtime) << endl;
	}
	cout << "Time total: " << DGCtimetosec(timetotal) << endl;
#endif

	return inform;
}

void CRefinementStage::checkDerivatives(void)
{
// 	ofstream derdat("derdat");
// 	derdat << setprecision(20);

// 	int i = 1;
// 	double del = 0.001;
// 	double swing = 1.0;

// 	m_pSolverState[i] -= swing;
// 	for(double j=-swing;j<swing;j+=del)
// 	{
// 		m_pSolverState[i] += del;

// 		makeCollocationData(m_pSolverState);
// 		funcConstr();
// 		funcCost();

// 		derdat << m_pSolverState[i] << ' ' << m_obj << ' ' << m_pCostGradData[i] << endl;
// 	}
// 	exit(1);







// 	int i;
// 	double obj;
// 	double* pCostGradData = new double[NPnumVariables];
// 	double* pCostGradComputed = new double[NPnumVariables];

// 	makeCollocationData(m_pSolverState);
// 	funcConstr();
// 	funcCost();

// 	obj = m_obj;
// 	memcpy(pCostGradData, m_pCostGradData, NPnumVariables*sizeof(pCostGradData[0]));

// 	cout << "Objective gradient check: given, computed, %err" << endl;
// 	cout.setf(ios::showpoint);
// 	cout.precision(4);
// 	for(i=0; i<NPnumVariables; i++)
// 	{
// 		cout.width(10);
// 		cout << pCostGradData[i];
// 	}
// 	cout << endl;

// 	for(i=0; i<NPnumVariables; i++)
// 	{
// 		m_pSolverState[i] += CHECKDERIVATIVES_DELTA;

// 		makeCollocationData(m_pSolverState);
// 		funcConstr();
// 		funcCost();

// 		pCostGradComputed[i] = (m_obj - obj) / CHECKDERIVATIVES_DELTA;
// 		cout.width(10);
// 		cout << pCostGradComputed[i];

// 		m_pSolverState[i] -= CHECKDERIVATIVES_DELTA;
// 	}
// 	cout << endl;

// 	for(i=0; i<NPnumVariables; i++)
// 	{
// 		cout.width(10);
// 		cout << fabs((pCostGradComputed[i] - pCostGradData[i]) / pCostGradData[i]);
// 	}
// 	cout << endl;

// 	delete[] pCostGradData;
// 	delete[] pCostGradComputed;
}

void CRefinementStage::CallSolver(void)
{
	int isumm = 0;
	sninit_(&PRINT_LEVEL, &isumm,
					cw, &lencw, iw, &leniw, rw, &lenrw, 8);

	// parse the specs file
	ifstream specsfile("specsfile");
	string line;

	// do nothing if the specs file wasn't opened properly
	if(specsfile)
	{
		// search until the solver specs tag is found
		do
			getline(specsfile,line);
		while(specsfile && (line != m_specsName));

		getline(specsfile,line);
		while(specsfile && (line != ("END " + m_specsName)))
		{		
			if(line == "print6")
				PRINT_LEVEL = 6;
			else if(line == "print0")
				PRINT_LEVEL = 0;
			else
			{
				snset_((char*)line.c_str(), &PRINT_LEVEL, &isumm, &inform,
							 cw, &lencw, iw, &leniw, rw, &lenrw, line.length(), 8);
			}
			getline(specsfile,line);
		}
	}

	// zero out the top half (slacks) of solverstate. This might not be necessary
	memset(m_pSolverState + NPnumVariables, 0, (NPnumNonLinearConstr + NPnumLinearConstr)*sizeof(m_pSolverState[0]));
	memset(hs, 0, (SNOPTn+SNOPTm)*sizeof(hs[0]));
	memset(pi, 0, SNOPTm*sizeof(pi[0]));

	snopt_(start, &SNOPTm, &SNOPTn, &ne, &nName, &nncon, &nnobj, &nnjac, &iobj, &objadd,
				 prob, SNOPTfuncConstr, SNOPTfuncCost,
				 m_SNOPTmatrix, ha, ka, m_solverLowerBounds, m_solverUpperBounds, NULL, hs, m_pSolverState, pi, rc,
				 &inform, &mincw, &miniw, &minrw, &ns, &ninf, &sinf, &obj,
				 (char*)this, &lencw, NULL, NULL, rw, &lenrw,
				 cw, &lencw, iw, &leniw, rw, &lenrw,
				 8, 8, 8, 8, 8);
// 	snoptm_(start, &SNOPTm, &SNOPTn, &ne, &nName, &nncon, &nnobj, &nnjac, &iobj, &objadd,
// 				 prob, SNOPTfuncConstrCost,
// 				 m_SNOPTmatrix, ha, ka, m_solverLowerBounds, m_solverUpperBounds, NULL, hs, m_pSolverState, pi, rc,
// 				 &inform, &mincw, &miniw, &minrw, &ns, &ninf, &sinf, &obj,
// 				 (char*)this, &lencw, NULL, NULL, rw, &lenrw,
// 				 cw, &lencw, iw, &leniw, rw, &lenrw,
// 				 8, 8, 8, 8, 8);


	// the following tests whether the gradients are proper
	// 	int iState = 20;
	// 	int iCon;
	// 	int mode = 2;
	// 	double fCon[NPnumNonLinearConstr];
	// 	double gCon[NPnumNonLinearConstr*NPnumVariables];
	// 	ofstream dataout("test.dat");
	//   m_pSolverState[0]=   -0.70204;
	//   m_pSolverState[1]=    0.64412;
	//   m_pSolverState[2]=   -6.06464;
	//   m_pSolverState[3]=    3.17813;
	//   m_pSolverState[4]=   -0.02959;
	//   m_pSolverState[5]=   -0.95691;
	//   m_pSolverState[6]=    1.80859;
	//   m_pSolverState[7]=    0.84552;
	//   m_pSolverState[8]=   -4.46146;
	//   m_pSolverState[9]=    5.67879;
	//   m_pSolverState[10]=    0.0;
	//   m_pSolverState[11]= -16.73304;
	//   m_pSolverState[12]=  -2.15412;
	//   m_pSolverState[13]=  11.57775;
	//   m_pSolverState[14]=   8.61134;
	//   m_pSolverState[15]=   6.07860;
	//   m_pSolverState[16]=   5.00339;
	//   m_pSolverState[17]=   1.33800;
	//   m_pSolverState[18]=  -2.05089;
	//   m_pSolverState[19]=   4.32064;
	//   m_pSolverState[20]=  77.68337;
	// 	for(int i=0; i<10; i++)
	// 		m_pSolverState[i]=0.0;

	// 	for(m_pSolverState[iState] = -0.5;
	// 			m_pSolverState[iState] < 0.5;
	// 			m_pSolverState[iState]+=0.05)
	// 	{
	// 		int nu = NPnumNonLinearConstr;
	// 		SNOPTfuncConstr(&mode, NULL, NULL,  NULL,
	// 										m_pSolverState, fCon, gCon,
	// 										NULL, NULL, NULL, NULL, &COLLOCATION_FACTOR, NULL, NULL, 0);

	// 		dataout << m_pSolverState[iState] << ' ';
	// 		for(iCon = 0; iCon<NPnumNonLinearConstr; iCon++)
	// 		{
	// 			dataout << fCon[iCon] << ' ' << gCon[MATRIX_INDEX(iCon, iState)] << ' ';
	// 		}
	// 		dataout << endl;
	// 	}
}
