#include "StaticPainter.hh"

enum {
  OPT_HELP,
  OPT_SNKEY,
  OPT_RATE,
  OPT_BLURWIDTH,
  OPT_VERBOSE,
  //  OPT_SPARROW,
  OPT_TRAJ,
  OPT_C2,
  OPT_LOG,
  OPT_LOGRATE,
  
  NUM_OPTS
};

void printStaticHelp() {
  cout << "Help for StaticPainter?  Look no further." << endl;
  cout << "Implemented command-line options: " << endl;
  cout << " --help      : Calls help.  Gets you here." << endl;
  cout << " --traj      : Outputs trajectories to trajFollower directly," << endl; 
  cout << "                instead of sending them to the map." << endl;
  cout << " --c2        : All trajectories used within StaticPainter will" << endl;
  cout << "                be continuous in second derivative. (i.e. C2)." << endl;
  cout << " --snkey     : Takes the desired skynet key as the argument" << endl;
  cout << "                that follows. (e.g. --snkey 14921066). " << endl;
  cout << " --rate      : Sets rate that StaticPainter updates.  Default" << endl;
  cout << "                is 50 Hz." << endl;
  cout << " --blurwidth : Sets the width that the path spreads when painted." << endl;
  cout << "                Default is 6m on each side." << endl;
  cout << " --log       : Logs trajectories being output with timber." << endl;
  cout << " --lograte   : Rate at which trajectories are logged.  Default" << endl;
  cout << "                is 2 Hz." << endl;
}

//#warning Add sparrow loop

int main(int argc, char **argv) {
  // Set defaults.
  int sn_key = 0;
  int quit = 0;
  //int optSparrow = 0; // Default is "sparrow off" until I make a display.
  double updateRate = 50.0; //Hz; it's just default.
  double logRate = 2.0; //Hz; it's just default.
  double width = -1.0; // Negative => don't load one
  int optTraj = 0;
  int optC2 = 0;
  int optLog = 0;
  int c; 

  static struct option long_options[] = {
    //Options that don't require arguments:
    //    {"sparrow", no_argument, 0, OPT_SPARROW},
    {"help",      no_argument, 0, OPT_HELP},
    {"traj",      no_argument, 0, OPT_TRAJ},
    {"c2",        no_argument, 0, OPT_C2},
    {"log",       no_argument, 0, OPT_LOG},
    //Options that require arguments
    {"snkey", required_argument, 0, OPT_SNKEY},
    {"rate", required_argument, 0, OPT_RATE},
    {"blurwidth", required_argument, 0, OPT_BLURWIDTH},
    {"lograte", required_argument, 0, OPT_LOGRATE},
    {0,0,0,0}
  };
  
  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    
    c = getopt_long_only(argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
    case OPT_HELP:
      printStaticHelp();
      exit(1);
      break;
    case OPT_SNKEY:
      sn_key = atoi(optarg);
      break;
    case OPT_RATE:
      updateRate = atof(optarg);
      break;
    case OPT_BLURWIDTH:
      width = atof(optarg);
      break;
      //    case OPT_SPARROW:
      //optSparrow = 1;
      //break;
    case OPT_TRAJ:
      optTraj = 1;
      break;
    case OPT_C2:
      optC2 = 1;
      break;
    case OPT_LOG:
      optLog = 1;
      break;
    case OPT_LOGRATE:
      logRate = atof(optarg);
      break;
    default:
      if(c!=0) {
	printf("Unknown option %d!\n", c);
	printStaticHelp();
	exit(1);
      }
    }
  }
  
  // Get the skynet key and make a staticPainter object with it.  
  cout << "Welcome to StaticPainter!" << endl;
  char* pSkynetkey = getenv("SKYNET_KEY");
  cout<<"got skynet key"<<endl;
  if ( pSkynetkey == NULL) {
    cerr << "SKYNET_KEY environment variable isn't set." << endl;
  } else {
    sn_key = atoi(pSkynetkey);
  }
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  printf("Option Summary - General:\n");
  printf("SkynetKey      - %d\n", sn_key);
  printf("Rate           - %lfHz\n", updateRate);
  printf("Blur Width     - %lfm\n", width);
  //  printf("Use Sparrow?   - %d\n", optSparrow);
  printf("Use traj?      - %d\n", optTraj);
  printf("Use C2?        - %d\n", optC2);
  printf("Use logging?   - %d\n", optLog);

  StaticPainter sp(sn_key);

  // Create a map and initialize it.
  sp.Initialize();
  sp.InitializeRDDF();
  
  // Start logging rate-limiting.
  unsigned long long logTime, nowTime;
  DGCgettime(logTime);

  // Begin active loop.
  if(optTraj) {
    /* Case 1: Using trajs */
    sp.makeStaticTrajSocket();
    while(!quit) {
      if(sp.LoadRDDFtoTraj(1, optC2)) {
	sp.SendStaticTraj();
	if(optLog) {
	  DGCgettime(nowTime);
	  if((nowTime - logTime) > (1/logRate*1e6)) {
	    DGCgettime(logTime);
	    sp.LogTrajs();
	  }
	}
      } else {
	cerr << "Can't load RDDF to traj--failing." << endl;
	return 1;
      }
      usleep((1/updateRate)*1e6);
    }    
  } else {
    /* Case 2: Using maps */
    while(!quit) {
      sp.Recenter();
      if(sp.LoadRDDFtoTraj(1, optC2)) {
	sp.PaintDelta();
	sp.SendStaticDelta();
	if(optLog) {
	  DGCgettime(nowTime);
	  if((nowTime - logTime) > (1/logRate*1e6)) {
	    DGCgettime(logTime);
	    sp.LogTrajs();
	  }
	}	
      } else {
	cerr << "Can't load RDDF to traj--failing." << endl;
	return 1;
      }
      usleep((1/updateRate)*1e6);
    }
  }

  // End it all!
  return 0;
}

