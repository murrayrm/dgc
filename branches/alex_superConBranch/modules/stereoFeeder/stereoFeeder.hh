#ifndef STEREOFEEDER_HH
#define STEREOFEEDER_HH

#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include "StateClient.h"
#include "MapdeltaTalker.h"
#include "CMapPlus.hh"
#include "frames/frames.hh"
#include "stereovision/stereoSource.hh"
#include "stereovision/stereoProcess.hh"
#include "DGCutils"
#include "CTimberClient.hh"
#include "GlobalConstants.h"
#include "CElevationFuser.hh"
#include "MapConstants.h"

using namespace std;

//Sparrow Display Functions
int user_quit(long arg);
int user_pause(long arg);
int user_step(long arg);
int user_reset(long arg);

int display_all(long arg);
int display_rect(long arg);
int display_disp(long arg);

int log_all(long arg);
int log_base(long arg);
int log_quickdebug(long arg);
int log_alldebug(long arg);
int log_none(long arg);

enum {
  NO_CAMERAS,
  SHORT_RANGE,
  LONG_RANGE,
  SHORT_COLOR,
  HOMER,

  NUM_CAMERA_TYPES
};


typedef struct{
  char SVSCalFilename[256];
  char SVSParamFilename[256];
  char CamCalFilename[256];
  char CamIDFilename[256];
  char GenMapFilename[256];
} calibFileStruct;

class StereoAvgCell {
public:
  int numPoints;
  double cellVal;

  StereoAvgCell() {
    numPoints = 0;
    cellVal = 0.0;
  };
  ~StereoAvgCell() {};

  bool operator!=(const StereoAvgCell &rhs) {
    if(this->numPoints==rhs.numPoints) {
      return true;
    }
    return false;
  };
};


class StereoFeeder : public CStateClient, public CMapdeltaTalker, public CTimberClient {
public:
  StereoFeeder(int skynetKey);
  ~StereoFeeder();
  
  void ActiveLoop();
  
  void SparrowDisplayLoop();

  void UpdateSparrowVariablesLoop();

  void ImageCaptureThread();

private:
  CMapPlus stereoMap;

  bool *stopCapture;
  stereoSource sourceObject;
  stereoProcess processObject;

  int layerID_stereoAvgDbl;
  int layerID_stereoAvgInt;
  StereoAvgCell noDataVal_stereoAvg;
  StereoAvgCell outsideMapVal_stereoAvg;
  
  int layerID_stereoElev;
  int layerID_stereoCount;
  double noDataVal_stereoElev;
  double outsideMapVal_stereoElev;

  int layerID_fusedElev;
  CElevationFuser noDataVal_fusedElev;
  CElevationFuser outsideMapVal_fusedElev;

  VehicleState tempState;

  calibFileStruct calibFileObject[NUM_CAMERA_TYPES];

  pthread_mutex_t cameraMutex;
};

int change(long arg);

#endif
