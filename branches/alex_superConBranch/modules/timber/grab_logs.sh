#!/bin/bash

# make sure they entered one argument
if (( $# != 2 )) ; then
    echo "Description:"
    echo "  This script uses the todo_timber file (output by the timber module) to"
    echo "  grab all the logs that have been generated on the various computers."
    echo "  Please do not put the logs in the same spot in which they were originally"
    echo "  created - this will cause problems.  Right now, all logs are created in"
    echo "   /tmp/logs/, so don't put the logs there when we collect them."
    echo 
    echo "Usage:"
    echo "  $0 computer-to-ssh-to where-to-put-logs"
    echo 
    echo "Example:"
    echo "  If we were running the timber module on skynet 7, and wanted"
    echo "  to put the logs from the race machines and development cluster"
    echo "  onto skynet2, in the /tmp/test_logs folder, we would run"
    echo
    echo "  user@skynet7$ $0 skynet2 /tmp/test_logs/"
    echo
    echo "PLEASE MAKE SURE TO PUT THE \"/\" ON THE END OF THE DIRECTORY!"
    exit 1
fi


if [ 0 == 0 ]; then
    export DEST_DIR=$2
#    export ME=`whoami`
    DEST_COMP=$1
# ok, finally found which grep will remove all teh commented parts of a file!!
# you have NO IDEA how long i've wanted to find this... but it always eluded me
# but now, i seem to haev gotten luck, cuz i still don't *really* see why it
# prints out the first half of the line instead of the second....but ...cool
#################################
#      egrep -o [^\#]\*         #
#################################
    
# make a temp file
#    tempfile=`mktemp /tmp/timber-todo.XXXXXX`

    echo "------------------------------------------------"
    echo "destination directory is (better end with a / !!!):"
    echo $DEST_DIR
    echo 
    
    echo "destination computer is:"
    echo $DEST_COMP
    echo "------------------------------------------------"

    if [ "$DEST_DIR" == "/tmp/logs/" ]; then
	echo "siigh...don't be silly!"
	echo "you can't use /tmp/logs/ as a location."
	echo "Run $0 with no args for more info"
	exit 1
    fi
    
    echo "running commands in interactive mode, type \'y\' ENTER to actually run the command"
    echo
    cat todo_timber \
	| egrep -o [^\#]\* \
	| gawk "{print \"\\\"\" \"mkdir -p $DEST_DIR\" \$2 \" && cd $DEST_DIR\" \$2 \" && cd .. && scp -r \" \$1\$2 \" .\\\"\"}" \
	| xargs -i ssh $DEST_COMP '{}'
fi





############################
# IGNORE ALL THIS CRAP
############################

#scp jason@192.168.0.34:/home/jason/timber/logs/2005_06_03/22_54_26/rddfPathGen/ jason@192.168.0.52:/donkey/logs/2005_06_03/22_54_26/rddfPathGen/

if [ 1 == 0 ] ; then
    ssh skynet2 "mkdir -p /tmp/logs/2005_06_03/22_54_26/rddfPathGen/ && \
cd /tmp/logs/2005_06_03/22_54_26/rddfPathGen/ && \
cd .. && \
scp -r jason@192.168.0.34:/home/jason/timber/logs/2005_06_03/22_54_26/rddfPathGen/ ."
fi

if [ 1 == 0 ]; then
    ssh jason@192.168.0.34 tar -cf - /home/jason/timber/logs/2005_06_03/22_54_26/rddfPathGen/ | tar -xf - -C /tmp/logs/2005_06_03/22_54_26/rddfPathGen/
fi


if [ 1 == 0 ]; then
    for line in $(cat todo)  # Concatenate the converted files.
                         # Uses wild card in variable list.
      do
      echo $line;
    done
fi


#cat todo_timber | egrep -o .*[\#]
#gives before comemnts, with # at end

# works (for soommmem reason....?)
# basically, this gets rid of comments
# cat todo_timber | egrep -o [^\#]\*
