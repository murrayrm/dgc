#include "AState.hh"

//Struct containing all possible pieces of data being outputted

#include "asdisp.h"		// main display table

//Populate displayVars struct

void AState::sparrowThread() 
{
  CSparrowHawk &sh = SparrowHawk();

  sh.add_page(asdisp, "Main");

  sh.rebind("snkey", &snKey);
  
  sh.rebind("gpsState", &_metaState.gpsEnabled);
  sh.rebind("imuState", &_metaState.imuEnabled);
  sh.rebind("obdState", &_metaState.obdEnabled);
  sh.rebind("kfState", &_metaState.kfEnabled);

  sh.rebind("gpsCount", &gpsCount);
  sh.rebind("imuCount", &imuCount);
  sh.rebind("obdCount", &obdCount);

  sh.rebind("gpsValid", &gpsValid);

  sh.rebind("timber", &timberEnabled);

  sh.rebind("gpsNorth", &gpsNorth);
  sh.rebind("gpsEast", &gpsEast);
  sh.rebind("gpsHeight", &gpsHeight);
  sh.rebind("gpsFOM", &gpsFOM);
  sh.rebind("gpsDOP", &gpsDOP);
  sh.rebind("satellites", &gpsSats);

  sh.rebind("obdVel", &obdVel);
  sh.rebind("obdSteer", &obdSteer);
  sh.rebind("obdBrake", &obdBrake);

  sh.rebind("trackErr", &trackErr);
  sh.rebind("yawErr", &yawErr);

  sh.rebind("vsNorthing", &_vehicleState.Northing);
  sh.rebind("vsEasting", &_vehicleState.Easting);
  sh.rebind("vsAltitude", &_vehicleState.Altitude);
  sh.rebind("vsVel_N",  &_vehicleState.Vel_N);
  sh.rebind("vsVel_E",  &_vehicleState.Vel_E);
  sh.rebind("vsVel_D",  &_vehicleState.Vel_D);
  sh.rebind("vsAcc_N",  &_vehicleState.Acc_N);
  sh.rebind("vsAcc_E",  &_vehicleState.Acc_E);
  sh.rebind("vsAcc_D",  &_vehicleState.Acc_D);
  sh.rebind("vsRoll", &_vehicleState.Roll);
  sh.rebind("vsPitch", &_vehicleState.Pitch);
  sh.rebind("vsYaw", &_vehicleState.Yaw);
  sh.rebind("vsRollRate", &_vehicleState.RollRate);
  sh.rebind("vsPitchRate", &_vehicleState.PitchRate);
  sh.rebind("vsYawRate", &_vehicleState.YawRate);
  sh.rebind("vsRollAcc", &_vehicleState.RollAcc);
  sh.rebind("vsPitchAcc", &_vehicleState.PitchAcc);
  sh.rebind("vsYawAcc", &_vehicleState.YawAcc);
  sh.rebind("vsNorthConf", &_vehicleState.NorthConf);
  sh.rebind("vsEastConf", &_vehicleState.EastConf);
  sh.rebind("vsHeightConf", &_vehicleState.HeightConf);
  sh.rebind("vsRollConf", &_vehicleState.RollConf);
  sh.rebind("vsPitchConf", &_vehicleState.PitchConf);
  sh.rebind("vsYawConf", &_vehicleState.YawConf);

  sh.set_notify("NOGPS", this, &AState::spToggleGps);

  sh.set_string("gpsMode", "N/A");
  //Sparrow bind goes here

  //  sh.add_page(astune, "Tune");

  //More sparrow bind goes here

  sh.run();

  while(sh.running()) {
    

    
    usleep(100000);
  }

  quitPressed = 1;

}

void AState::spToggleGps() {
  if (spGPSDisable == 0) {
    spGPSDisable = 1;
  } else {
    spGPSDisable = 0;
  }
}
