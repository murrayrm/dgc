#include "AState.hh"

void AState::metaStateThread() {

  _heartbeat.gps = false;
  _heartbeat.gpsValid = false;
  _heartbeat.imu = false;
  _heartbeat.obd = false;

  // give time for the heartbeats to start
  sleep(1);

  while(!quitPressed) {

    DGClockMutex(&m_MetaStateMutex);        
    DGClockMutex(&m_HeartbeatMutex);
    if(_metaState.gpsEnabled) {
      if (_heartbeat.gps) {
	_metaState.gpsActive = 1;
	_metaState.extJumpFlag = 0;
      } else {
	// GPS is hanging...we're going to jump!
	_metaState.gpsActive = 0;
	_metaState.extJumpFlag = 1;
      }

      // Invalid data gets discarded and will also cause jumps.
      if(_heartbeat.gpsValid) {
	_metaState.gpsPvtValid = 0;
      } else {
	_metaState.gpsPvtValid = 1;
      }
    }

    if(_metaState.imuEnabled) {
      if(_heartbeat.imu) {
	_metaState.imuActive = 1;
      } else {
	_metaState.imuActive = 0;
      }
    }

    if(_metaState.obdActive) {
      if(_heartbeat.obd) {
	_metaState.obdActive = 1;
      } else {
	_metaState.obdActive = 0;
      }
    }
    
    // Mode setting.
    if(stateMode != PREINIT && stateMode != INIT) {
      if(_heartbeat.gps && _heartbeat.imu && _heartbeat.obd) {
	stateMode = IMU_GPS_OBD;
      } else if(_heartbeat.gps && _heartbeat.imu && !_heartbeat.obd) {
	stateMode = IMU_GPS;
      } else if(!_heartbeat.gps && _heartbeat.imu && _heartbeat.obd) {
	stateMode = IMU_OBD; 
      } else if(!_heartbeat.gps && _heartbeat.imu && !_heartbeat.obd) {
	stateMode = IMU;
      } else if(_heartbeat.gps && !_heartbeat.imu && _heartbeat.obd) {
	stateMode = GPS_OBD;
      } else if(_heartbeat.gps && !_heartbeat.imu && !_heartbeat.obd) {
	stateMode = GPS;
      } else if(!_heartbeat.gps && !_heartbeat.imu && _heartbeat.obd) {
	stateMode = OBD;
      } else {
	stateMode = OH_SHIT;
      }
    }

    _heartbeat.gps = false;
    _heartbeat.gpsValid = false;
    _heartbeat.imu = false;
    _heartbeat.obd = false;

    DGCunlockMutex(&m_HeartbeatMutex);
    DGCunlockMutex(&m_MetaStateMutex);
    
    // Wait a little while before we check again.
    sleep(1);
  }
}


// heartbeat
// 
// client thread -- calls it with argument for every runthrough
// 
// server thread -- listens for calls (how?) and if nothing set in X seconds, does a contingency.
/*
  int metaStateSock = m_skynet.get_send_sock(SNmetastate);
  int wantMetaStateSock = m_skynet.listen(SNgetmetastate, MODsupercon);
  int numReceived = 0;
  bool wantMeta = false;

    numReceived = m_skynet.get_msg(wantMetaStateSock, &wantMeta, sizeof(bool), 0);
    if(numReceived > 0 && wantMeta) {
      pthread_mutex_t* pMutex = &m_MetaStateMutex;
      if(m_skynet.send_msg(metaStateSock, &_metaState, sizeof(MetaState),
			   0, &pMutex) != sizeof(MetaState)) {
	cerr << "AState::metaStateThread() failed to send right size message" << endl;
      }
    }
*/
