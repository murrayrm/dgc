/* AState.hh
 *
 * JML 09 Jun 05
 *
 */

#ifndef ASTATE_HH
#define ASTATE_HH

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "VehicleState.hh"

#include "ggis.h"
#include "frames/coords.hh"
#include "gps.hh"
#include "imu_fastcom.h"
#include "sn_msg.hh"
#include "sn_types.h"
#include "Matrix.hh"

#include "DGCutils"

#include "SensorConstants.h"
#include "SkynetContainer.h"
#include "CTimberClient.hh"

#include "ActuatorState.hh"

#include "sparrowhawk.hh"

#ifndef NOSDS
#define GPSPORT 4
#else
#define GPSPORT 0
#endif

#define IMU_BUFFER_SIZE 10
#define GPS_BUFFER_SIZE 10
#define OBD_BUFFER_SIZE 10

/*!  MetaState struct.  This is the struct that stores higher level information 
 *   about the status of various components of the stateserver.
 *
 *   At the moment, this struct is neither used nor broadcast.  Its contents are 
 *   still subject to change depending on what we decide fault management might 
 *   need to know.
 */

void makeBody2Nav(Matrix &Cnb, double roll, double pitch, double yaw);
void makeEarth2Nav(Matrix &Cne, double lat, double lon, double alpha);
void makeNav2Geo(Matrix &Cgn, double alpha);

struct MetaState
{
  unsigned long long Timestamp;    

  /*  
   *  1 = enabled.  0 = disabled. -1 = Couldn't Start. 
   */
  int gpsEnabled;
  int imuEnabled;
  int kfEnabled;
  int obdEnabled;

  int gpsActive;
  int imuActive;
  int obdActive;
    
  int gpsPvtValid;
  int extJumpFlag;  
};

struct astateOpts {
  int useIMU;
  int useGPS;
  int useKF;
  int useOBD;
  int useVerbose;
  int logRaw;
  int useReplay;
  char replayFile[100];
  int timberPlayback;
  int debug;
  int useSparrow;
  int fastMode;
};

struct rawIMU {
  unsigned long long time;
  IMU_DATA data;
};

struct rawGPS {
  unsigned long long time;
  gpsData data;
};

struct rawObd {
  unsigned long long time;
  ActuatorState data;
};

struct heartbeat {
  bool gps;
  bool gpsValid;
  bool imu;
  bool obd;
};

enum {
  PREINIT,
  INIT,
  IMU_GPS_OBD,
  IMU_GPS,
  IMU_OBD,
  IMU,
  GPS_OBD,
  GPS,
  OBD,
  OH_SHIT,
  STATIONARY,
  NUM_MODES
};
  
  

class AState : virtual public CSkynetContainer, public CTimberClient //Timber not in stable branch yet
{

  pthread_mutex_t m_VehicleStateMutex;
  // Lock these for the m_VehicleStateMutex

  struct VehicleState _vehicleState;

  // end lock list 



  pthread_mutex_t m_MetaStateMutex;

  // Lock these for the m_MetaStateMutex

  struct MetaState	_metaState;

  // end lock list 





  pthread_mutex_t m_IMUDataMutex;

  // Lock these for the m_IMUDataMutex

  DGCcondition imuBufferFree;

  rawIMU imuBuffer[IMU_BUFFER_SIZE];

  int imuBufferReadInd;

  int imuBufferLastInd;

  unsigned long long imuLogStart;

  // end lock list





  pthread_mutex_t m_GPSDataMutex;

  // Lock these for the m_GPSDataMutex

  DGCcondition gpsBufferFree;

  rawGPS gpsBuffer[GPS_BUFFER_SIZE];
 
  int gpsBufferReadInd;

  int gpsBufferLastInd;

  unsigned long long gpsLogStart;

  // end lock list 


 


  pthread_mutex_t m_OBDDataMutex;
  
  // Lock these for the m_OdoomDataMutex
  DGCcondition obdBufferFree;

  rawObd obdBuffer[OBD_BUFFER_SIZE];

  int obdBufferReadInd;
  
  int obdBufferLastInd;

  unsigned long long obdLogStart;
  // end lock list





  //The Follow sets of variables are PROBABLY not needed outside of AState_Update
  
  // Variables Related to the Kalman Filter:

  Matrix kfX;

  Matrix kfP;

  Matrix kfdXdt;

  Matrix kfA;

  Matrix kfH2d;
  Matrix kfH3d;
  Matrix kfHnoVel;
  Matrix kfHobd;

  Matrix kfZ2d;
  Matrix kfZ3d;
  Matrix kfZnoVel;
  Matrix kfZobd;

  Matrix kfR2d;
  Matrix kfR3d;
  Matrix kfRnoVel;
  Matrix kfRobd;

  Matrix kfK;
  Matrix kfQ;




  int stateMode;


  //These variables are ONLY here to give sparrow access to them...

  //Outputs:
  int imuCount;
  int gpsCount;
  int obdCount;

  int gpsValid; 

  int timberEnabled;

  double gpsNorth;
  double gpsEast;
  double gpsHeight;
  double gpsFOM;
  double gpsDOP;
  int    gpsSats;

  double obdVel;
  double obdSteer;
  double obdBrake;

  double trackErr;
  double yawErr;

  //Inputs:
  int spGPSDisable;






  DGCcondition newData;


  double obdVelScaleFactor;
  double obdSteerScaleFactor;


  double pError;


  pthread_mutex_t m_HeartbeatMutex;

  // Lock these for the m_HeartbeatMutex

  struct heartbeat _heartbeat;

  // end lock list


  // Variables to remain unlocked
  int quitPressed;

  int snKey;  // Should be set in AState_Main and never changed.

  struct astateOpts  _astateOpts;  // Should be set in constructor and never changed.

  int broadcastStateSock;  // Should be set in constructor and never changed.

  unsigned long long startTime; // Should be set in constructor and never changed.

  fstream imuLogStream; // These four are opened & closed in constructor and destructor
  fstream imuReplayStream; // and only accessed as a write in AState_IMU.  
  char imuLogFile[100];
  char imuReplayFile[100];

  fstream gpsLogStream; // These four are opened & closed in constructor and destructor
  fstream gpsReplayStream; // and only accessed as a write in AState_GPS.
  char gpsLogFile[100];
  char gpsReplayFile[100];

  fstream obdLogStream; // These four are opened & closed in constructor and destructor
  fstream obdReplayStream; // and only accessed as a write in AState_OBDII.  
  char obdLogFile[100];
  char obdReplayFile[100];

  fstream timberLogStream; // Only called by AState_Update.
  char timberLogFile[100]; // See above
  ifstream timberPlaybackStream; // Only called by AState_Update.
  char timberPlaybackFile[100];  // See above

  fstream debugStream;  // Not called anywhere yet.
  char debugFile[100];  // See above


  /*****************************************************************
   *  The following are the private methods of the AState class.
   *****************************************************************/

  /*! Function that initializes the IMU */
  void imuInit();

  /*! Function that initializes the GPS */
  void gpsInit();

  /*! Function that initializes the odometry */
  void obdInit();

  /*! Function that updates the vehicle_state struct */
  void updateStateThread();

  /*! Function to broadcast state data */
  void broadcast();

  /*! Thread to read from the IMU.  Note: this is where UpdateState normally gets called since it's the fastest thread.*/
  void imuThread(); 

  /*! Thread to read from the GPS.*/
  void gpsThread();

  /*! Thread to read from the odometry.*/
  void obdThread();

  /*! Thread to update sparrow variables*/
  void sparrowThread();

  void spToggleGps();

  /*! Thread to output metastate to supercon. */
  void metaStateThread();

  void playback();

  void kfInit();

  void readCovariances();

public:
  /*! AState Constructor */
  AState(int skynetKey, astateOpts inputAstateOpts);

  /*! Function to restart AState without having to stop/start the
   *  whole system
   */
  void restart();
  void active();


  ~AState();
};

#endif
