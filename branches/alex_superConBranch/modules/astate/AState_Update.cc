/* AState.cc - new vehicle state estimator
 *
 * JML 09 Jun 05
 *
 * */

#warning Earth Radius is based on Latitude -- we should really use a dynamically changing variable for most of the places we refer to it.

#include "AState.hh"
#include "Matrix.hh"

#define square(x) ((x)*(x))

void AState::updateStateThread()
{

  /*The following definitions were moved to AState.hh for Sparrow access
    
  //GPS RELATED:
  double gpsHeight;
  double gpsFOM;
  double gpsDOP;
  
  //OBD RELATED:
  double obdVel;
  double obdSteer;
  double obdBrake;

  */
  unsigned long long stateTime;


  //IMU data read in
  unsigned long long imuTime;
  Matrix fbSum(3);
  Matrix wb_ibSum(3);
  double dtSum = 0.0;
  double dtFbSum = 0.0;
  double dtWbSum = 0.0;
  int newIMU = 0;


  //GPS data read in
  double gpsLat;
  double gpsLon;
  unsigned long long gpsTime;
  Matrix gpsVg(3);
  Matrix gpsVn(3);
  int gps3D;
  int newGPS = 0;
  GisCoordLatLon latsForGPS;
  GisCoordUTM utmForGPS;


  //OBD data read in
  unsigned long long obdTime;
  int newObd = 0;


  //Internal state estimate
  int stateMode;  
  double lat = 0.0;
  double lon = 0.0;
  double height = 0.0;
  Matrix vn(3);
  double roll = 0.0;
  double pitch = 0.0;
  double yaw = 0.0;
  double alpha = 0.0;
  Matrix Cne(3,3);
  Matrix Cnb(3,3);
  Matrix Cgn(3,3);
  Matrix Cbi(3,3);
  makeNav2Geo(Cgn,alpha);
  makeBody2Nav(Cbi, (ROLL_IMU), (PITCH_IMU), (YAW_IMU));

  //Variables necessary for initialization
  double gpsLatSum = 0.0;
  double gpsLonSum = 0.0;
  double gpsHeightSum = 0.0;
  int goodGpsCnt = 0;
  double gNorm;



  // These are temporary variables used for calculating dXdt
  double vnx;
  double vny;
  double Rmeridian;
  double ca;
  double sa;
  double ca2;
  double sa2;
  double Rnormal;
  double rhox;
  double rhoy;
  double vx;
  double vy;
  double vz;
  double px;
  double py;
  double pz;
  double wx;
  double wy;
  double wz;
  double ox;
  double oy;
  double oz;
  double fx;
  double fy;
  double fz;
  double magg;
  double g0;

  //Defined Earth Rate.
  Matrix we_ie(3);
  we_ie.setelem(2, EARTH_RATE);

  //Variables used for finding nav solutions
  Matrix gn(3);
  Matrix fn(3);
  Matrix wn_ib(3);
  Matrix wn_ie(3);
  Matrix wn_in(3);
  Matrix wn_bn(3);
  Matrix wn_en(3);

  //Varibales used for applying corrections
  Matrix CneMeas(3,3);
  Matrix dTheta(3);
  Matrix dPhi(3);
  Matrix vdot(3);
  double dH;
  Matrix dVn(3);
  Matrix oldWn_bn(3);
  Matrix oldWb_ibSum(3);



  //Definition of gpsOffset in body frame
  Matrix gpsOffset(3);
#warning double check these offset calculations
  gpsOffset.setelem(0, (X_IMU)-(X_GPS));
  gpsOffset.setelem(1, (Y_IMU)-(Y_GPS));
  gpsOffset.setelem(2, (Z_IMU)-(Z_GPS));

  //gpsOffset transformed into Nav Frame
  Matrix gpsNavOffset(3);


  //Offsets from IMU to Vehicle
  Matrix vehicleOffsets(3);
#warning double check these offset calculations
  vehicleOffsets.setelem(0, -X_IMU);
  vehicleOffsets.setelem(1, -Y_IMU);
  vehicleOffsets.setelem(2, -Z_IMU);


  //Variables used for converting to UTM / Latlon
  GisCoordLatLon latsForVehState;
  GisCoordUTM utmForVehState;


  //Counter that increments every loop (used to limit broadcast rate)
  int count = 0;

  while (!quitPressed) {

    DGClockMutex(&m_IMUDataMutex);
    if (imuBufferReadInd > imuBufferLastInd) {
      DGCSetConditionTrue(imuBufferFree);
      imuBufferLastInd++;
      fbSum.setelem(0, fbSum.getelem(0) - imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dvx);
      fbSum.setelem(1, fbSum.getelem(1) + imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dvy);
      fbSum.setelem(2, fbSum.getelem(2) - imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dvz);
      wb_ibSum.setelem(0, wb_ibSum.getelem(0) - imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dtx);
      wb_ibSum.setelem(1, wb_ibSum.getelem(1) + imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dty);
      wb_ibSum.setelem(2, wb_ibSum.getelem(2) - imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dtz);
      dtSum += 0.0025; // 1/400 --> IMU works at 400 Hz
      dtFbSum += 0.0025; // 1/400 --> IMU works at 400 Hz
      dtWbSum += 0.0025; // 1/400 --> IMU works at 400 Hz

      imuTime = imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].time;

      newIMU = 1;

      imuCount++;
    }
    DGCunlockMutex(&m_IMUDataMutex);

    DGClockMutex(&m_GPSDataMutex);
    if (gpsBufferReadInd > gpsBufferLastInd) {
      if (gpsBuffer[(gpsBufferLastInd + 1) % GPS_BUFFER_SIZE].time < imuTime) {
	DGCSetConditionTrue(gpsBufferFree);
	gpsBufferLastInd++;
	gps3D =  (gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.nav_mode & NAV_3D) && 1;
      
	if (stateMode != INIT) {
	  gpsNavOffset = Cgn * Cnb * gpsOffset;
	  gpsLat = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.lat*M_PI/180 + gpsNavOffset.getelem(0)/EARTH_RADIUS;
	  gpsLon = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.lng*M_PI/180 + gpsNavOffset.getelem(1)/EARTH_RADIUS;
	} else {
	  gpsLat = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.lat*M_PI/180;
	  gpsLon = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.lng*M_PI/180;
	}
	gpsHeight = - gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.ellipsoidal_height;
	gpsVg.setelem(0,gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.vel_n);
	gpsVg.setelem(1,gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.vel_e);
	gpsVg.setelem(2,-gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.vel_u);
	gpsVn = Cgn.transpose() * gpsVg;
	gpsFOM = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.position_fom;
	gpsDOP = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.gdop;
	gpsSats = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.sats_used;

	gpsTime = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].time;

	
	latsForGPS.latitude = gpsLat*(180/M_PI);
	latsForGPS.longitude = gpsLon*(180/M_PI);
	gis_coord_latlon_to_utm(&latsForGPS, &utmForGPS, GIS_GEODETIC_MODEL_WGS_84);

	gpsNorth = utmForGPS.n;
	gpsEast = utmForGPS.e;

	newGPS = 1;
	
	gpsCount++;
      }
    }
    DGCunlockMutex(&m_GPSDataMutex);

    DGClockMutex(&m_OBDDataMutex);
    if (obdBufferReadInd > obdBufferLastInd) {
      if(obdBuffer[(obdBufferLastInd + 1) % OBD_BUFFER_SIZE].time < imuTime) {
	DGCSetConditionTrue(obdBufferFree);
	obdBufferLastInd++;
	obdTime = obdBuffer[obdBufferLastInd % OBD_BUFFER_SIZE].time;
	obdSteer = obdBuffer[obdBufferLastInd % OBD_BUFFER_SIZE].data.m_steerpos * obdSteerScaleFactor;
	obdBrake = obdBuffer[obdBufferLastInd % OBD_BUFFER_SIZE].data.m_brakepos;
	obdVel = obdBuffer[obdBufferLastInd % OBD_BUFFER_SIZE].data.m_VehicleWheelSpeed * obdVelScaleFactor;
	newObd = 1;
	obdCount++;
      }
    }
    DGCunlockMutex(&m_OBDDataMutex);

    if (newGPS == 0 && newIMU == 0 && newObd == 0) {
      newData.bCond = false;
      DGCWaitForConditionTrue(newData);
    }


    //LOCK MUTEXES
    if (stateMode == PREINIT) { 
      //Essentially just make sure everything is cleared
      fbSum.clear();
      wb_ibSum.clear();
      dtSum = 0;
      dtWbSum = 0;
      dtFbSum = 0;
      
      
      goodGpsCnt = 0;
      gpsLatSum = 0;
      gpsLonSum = 0;
      gpsHeightSum = 0;
  
      stateMode = INIT;
    } else if(stateMode == INIT) {
      if (dtSum > 5.0 && goodGpsCnt > 20) { //average both imu info and gps info

	lat = gpsLatSum / goodGpsCnt;
	lon = gpsLonSum / goodGpsCnt;
	height = gpsHeightSum / goodGpsCnt;

	//Find mag of gravity...	
	gNorm = fbSum.norm();

	//Work backwards to find roll, pitch, yaw from these.
	pitch = asin(fbSum.getelem(0) / gNorm);
	roll = asin(-fbSum.getelem(1) / (gNorm*cos(pitch)));

	//Wtmp = Earth rotation rate translated into current nav frame.
	Matrix ctmp(3,3);
	makeBody2Nav(ctmp,roll,pitch,0);
	Matrix wtmp = ctmp * wb_ibSum;
	
	yaw = -alpha + -atan2(wtmp.getelem(1), wtmp.getelem(0));

	makeBody2Nav(Cnb, roll, pitch, yaw); // Really is Cni, so...
	Cnb = Cnb*Cbi.transpose(); // make Cnb out of it.

	gpsNavOffset = Cnb * Cbi * gpsOffset;
	lat += gpsNavOffset.getelem(0)/EARTH_RADIUS;
	lon += gpsNavOffset.getelem(1)/EARTH_RADIUS;

	makeEarth2Nav(Cne, lat, lon, alpha);
	makeNav2Geo(Cgn, alpha);

	gpsLatSum = 0;
	gpsLonSum = 0;
	gpsHeightSum = 0;
	fbSum.clear();
	wb_ibSum.clear();
	dtSum = 0.0;
	dtFbSum = 0.0;
	dtWbSum = 0.0;
	
	stateMode = IMU_GPS_OBD;

	kfInit();

	lat = asin( -Cne.getelem(2,2) );
	lon =  atan2( -Cne.getelem(2,1), -Cne.getelem(2,0) );
	alpha = atan2( -Cne.getelem(1,2), Cne.getelem(0,2) );
	pitch = asin( - Cnb.getelem(2,0));
	yaw = atan2( Cnb.getelem(1,0), Cnb.getelem(0,0) );
	roll = atan2( Cnb.getelem(2,1), Cnb.getelem(2,2) );

	newGPS = 0;
	newIMU = 0;

      } else if (newGPS) {
 	gpsLatSum += gpsLat;
	gpsLonSum += gpsLon;
	gpsHeightSum += gpsHeight;
	goodGpsCnt++;
	newGPS = 0;
      } else {
	newIMU = 0;
      }


    } else if (stateMode == IMU_GPS_OBD || stateMode == IMU_GPS || stateMode == IMU_OBD || stateMode == IMU) {
      if(newIMU == 1) {
	//Do actually nav calculation...

	//Rotate by half the roation...

	wn_ie = Cne * we_ie;

	wn_ib = Cnb * Cbi *(wb_ibSum / dtSum);

	wn_ie = Cne * we_ie;

	wn_in = wn_ie + wn_en;

	wn_bn = wn_in - wn_ib;

	Cnb = matrixDiffEqStep(wn_bn * dtSum / 2) * Cnb;



	//Move according to accelerations...

	g0 = GWGS0*(1+GWGS1*pow(sin(lat),2))/sqrt(1 - EPSILON2*pow(sin(lat),2));
	gn.setelem(2,g0*(1+2*height/EARTH_RADIUS));

	fn = Cnb * Cbi * (fbSum / dtSum);

	vdot = fn - matrixCross(wn_en + wn_ie * 2)*vn + gn;

	vnx = vn.getelem(0) + vdot.getelem(0) * dtSum / 2;
	vny = vn.getelem(1) + vdot.getelem(1) * dtSum / 2;
	Rmeridian = EARTH_RADIUS*(1-EPSILON2)/pow((1-EPSILON2*pow(sin(lat),2)),1.5);
	ca = cos(alpha);
	sa = sin(alpha);
	ca2 = pow(ca,2);
	sa2 = pow(sa,2);
	Rnormal = EARTH_RADIUS/sqrt(1-EPSILON2*pow(sin(lat),2));
	rhox = vny*(ca2/(Rnormal - height) + sa2/(Rmeridian - height)) - vnx*sa*ca*(1/(Rmeridian - height) - 1/(Rnormal - height));
	rhoy = -vnx*(sa2/(Rnormal - height) + ca2/(Rmeridian - height)) + vny*sa*ca*(1/(Rmeridian - height) - 1/(Rnormal - height));
	wn_en.setelem(0,rhox);
	wn_en.setelem(1,rhoy);

	vn = vn + vdot * dtSum;

	height = height + dtSum * vn.getelem(2);


#warning Does matrixDiffEqStep gurantee than rotation matrices remain normalized, or will rounding errors build up over a long enough period of time?  Maybe we should re-initialize rotation matrices every once in a while.  
	Cne = matrixDiffEqStep(wn_en * dtSum) * Cne;

	//Finish Rotation...

	wn_ie = Cne * we_ie;

	wn_ib = Cnb * Cbi * (wb_ibSum / dtSum);

	wn_ie = Cne * we_ie;

	wn_in = wn_ie + wn_en;

	wn_bn = wn_in - wn_ib;

	Cnb = matrixDiffEqStep(wn_bn * dtSum / 2) * Cnb;


	//Correctly set all internal state values

	lat = asin( -Cne.getelem(2,2) );
	lon =  atan2( -Cne.getelem(2,1), -Cne.getelem(2,0) );
	alpha = atan2( -Cne.getelem(1,2), Cne.getelem(0,2) );
	pitch = asin( - Cnb.getelem(2,0));
	yaw = atan2( Cnb.getelem(1,0), Cnb.getelem(0,0) );
	roll = atan2( Cnb.getelem(2,1), Cnb.getelem(2,2) );
	makeNav2Geo(Cgn, alpha);


	//Propagate errors according to Nav movement

	//Setup for calculation of kfdXdt

	vx = vn.getelem(0);
	vy = vn.getelem(1);
	vz = vn.getelem(2);
	px = wn_en.getelem(0);
	py = wn_en.getelem(1);
	pz = wn_en.getelem(2);
	wx = wn_in.getelem(0);
	wy = wn_in.getelem(1);
	wz = wn_in.getelem(2);
	ox = wn_ie.getelem(0);
	oy = wn_ie.getelem(1);
	oz = wn_ie.getelem(2);
	fx = fn.getelem(0);
	fy = fn.getelem(1);
	fz = fn.getelem(2);
	magg = gn.norm();

	double tempprop[100] = {0, 0, -vy/square(EARTH_RADIUS), 0, 1/EARTH_RADIUS, 0, 0, 0, 0, -py,
				0, 0, vx/square(EARTH_RADIUS), -1/EARTH_RADIUS, 0, 0, 0, 0, 0, px,
				0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				-2*(vy*oy + vz*oz), 2*vy*ox, -vz*vx/square(EARTH_RADIUS), vz/EARTH_RADIUS, 2*oz, -(py + 2*oy), 0, -fz, fy, 2*vz*ox,
				2*vx*oy, -2*(vx*ox + vz*oz), -vy*vz/square(EARTH_RADIUS), -2*oz, vz/EARTH_RADIUS, (px + 2*ox), fz, 0, -fx, 2*vz*oy,
				2*vx*oz, 2*vy*oz, (vx*vx + vy*vy)/square(EARTH_RADIUS)+2*magg/EARTH_RADIUS, 2*(py + oy), -2*(px + ox), 0, -fy, fx, 0, -2*(vx*ox + vy*oy),
				0, -oz, -vy/square(EARTH_RADIUS), 0, 1/EARTH_RADIUS, 0, 0, wz, -wy, oy,
				oz, 0, vx/square(EARTH_RADIUS), -1/EARTH_RADIUS, 0, 0, -wz, 0, wx, -ox,
				-oy, ox, 0, 0, 0, 0, wy, -wx, 0, 0,
				py, -px, 0, 0, 0, 0, 0, 0, 0, 0};


	kfdXdt.setelems(tempprop);

	kfA = kfA.eye() + kfdXdt*dtSum;

	kfX = kfA*kfX;

	kfP = (kfA*kfP*kfA.transpose() + kfQ);

	fbSum.clear();
      }

      if (newGPS == 1 && (stateMode == IMU_GPS || stateMode == IMU_GPS_OBD)) {

	//Create measurement according to gps latitude and longitude
	makeEarth2Nav(CneMeas, gpsLat, gpsLon, alpha);
	dTheta = unmatrixCross(Matrix(3,3).eye() - (Cne * CneMeas.transpose()));
        dVn = vn - gpsVn;
	dH = height - gpsHeight;
	  
	Matrix tmp;
	
	if(gps3D) {

	  
	  kfZ3d.setelem(0,dTheta.getelem(0));
	  kfZ3d.setelem(1,dTheta.getelem(1));
	  kfZ3d.setelem(2,dH);
	  kfZ3d.setelem(3,dVn.getelem(0));
	  kfZ3d.setelem(4,dVn.getelem(1));
	  kfZ3d.setelem(5,dVn.getelem(2));
	  
#warning R values for kfR3d are bullshit
	  kfR3d.setelem(0, 0, pow(gpsFOM/100,2)/2/(EARTH_RADIUS * EARTH_RADIUS));
	  kfR3d.setelem(1, 1, pow(gpsFOM/100,2)/2/(EARTH_RADIUS * EARTH_RADIUS));
	  kfR3d.setelem(2, 2, 2);
	  kfR3d.setelem(3, 3, pow(1.,2.));
	  kfR3d.setelem(4, 4, pow(1.,2.));
	  kfR3d.setelem(5, 5, pow(2,2.));
	  
	  tmp = (kfH3d*kfP*kfH3d.transpose() + kfR3d);
	  
	  kfK = kfP*kfH3d.transpose()*tmp.inverse();
	  
	  kfX = kfX + kfK*(kfZ3d - kfH3d*kfX);
	  
	  kfP = (kfP.eye() - kfK*kfH3d)*kfP;
	  
	} else {
	  kfZ2d.setelem(0,dTheta.getelem(0));
	  kfZ2d.setelem(1,dTheta.getelem(1));
	  kfZ2d.setelem(2,dVn.getelem(0));
	  kfZ2d.setelem(3,dVn.getelem(1));
	  
#warning R values for kfR2d are bullshit
	  kfR2d.setelem(0, 0, pow(gpsFOM/100,2)/2/(EARTH_RADIUS * EARTH_RADIUS));
	  kfR2d.setelem(1, 1, pow(gpsFOM/100,2)/2/(EARTH_RADIUS * EARTH_RADIUS));
	  kfR2d.setelem(2, 2, pow(2.,2.));
	  kfR2d.setelem(3, 3, pow(2.,2.));
	  
	  tmp = (kfH2d*kfP*kfH2d.transpose() + kfR2d);
	  
	  kfK = kfP*kfH2d.transpose()*tmp.inverse();
	  
	  kfX = kfX + kfK*(kfZ2d - kfH2d*kfX);
	  
	  kfP = (kfP.eye() - kfK*kfH2d)*kfP;
	  
	}
	
	newGPS = 0;
      }
      
      if (newObd == 1 && (stateMode == IMU_GPS_OBD || stateMode == IMU_OBD)) {
	// KF on the obdii speed
	Matrix tempV(3);
#warning OBD body-frame velocity calculations should be done using bicycle model
	tempV.setelem(0, obdVel);
	kfZobd = Cnb*tempV;
	

#warning Need to switch over to dynamic speed / throttle based variance on measurements
	kfRobd.setelem(0, 0, pow(2., 2.));
	kfRobd.setelem(1, 1, pow(2., 2.));
	kfRobd.setelem(2, 2, pow(2., 2.));
	Matrix tmp;

	tmp = (kfHobd*kfP*kfHobd.transpose() + kfRobd);

	kfK = kfP*kfHobd.transpose()*tmp.inverse();

	kfX = kfX + kfK*(kfZobd - kfHobd*kfX);

	kfP = (kfP.eye() - kfK*kfHobd)*kfP;

	newObd = 0;
      }

      if (newIMU) {
    
	dTheta.setelem(0,kfX.getelem(0));
	dTheta.setelem(1,kfX.getelem(1));
	dTheta.setelem(2,kfX.getelem(9));
	dPhi.setelem(0,kfX.getelem(6));
	dPhi.setelem(1,kfX.getelem(7));
	dPhi.setelem(2,kfX.getelem(8));
	dH = kfX.getelem(2);
	dVn.setelem(0,kfX.getelem(3));
	dVn.setelem(1,kfX.getelem(4));
	dVn.setelem(2,kfX.getelem(5));
	
	Cne = matrixDiffEqStep(dTheta * -pError)*Cne;
	
	Cnb = matrixDiffEqStep(dPhi * -1.0)*Cnb;
	
	height = height - pError*dH;
	
	vn = vn - dVn;
	  
	
	lat = asin( -Cne.getelem(2,2) );
	lon =  atan2( -Cne.getelem(2,1), -Cne.getelem(2,0) );
	alpha = atan2( -Cne.getelem(1,2), Cne.getelem(0,2) );
	pitch = asin( - Cnb.getelem(2,0));
	yaw = atan2( Cnb.getelem(1,0), Cnb.getelem(0,0) );
	roll = atan2( Cnb.getelem(2,1), Cnb.getelem(2,2) );
	makeNav2Geo(Cgn, alpha);
	  
	// Reset error vector with leftovers from smoothing, if any.
	kfX.clear();
	kfX.setelem(0, (1-pError)*dTheta.getelem(0));
	kfX.setelem(1, (1-pError)*dTheta.getelem(1));
	kfX.setelem(2, (1-pError)*dH);
	kfX.setelem(9, (1-pError)*dTheta.getelem(2));

	trackErr = sqrt( pow(dTheta.getelem(0)*EARTH_RADIUS, 2.0) + pow(dTheta.getelem(1)*EARTH_RADIUS, 2.0) );

	newIMU = 0;

	// Update vehicle state struct

	DGClockMutex(&m_VehicleStateMutex);
	
	latsForVehState.latitude = lat*(180/M_PI);
	latsForVehState.longitude = lon*(180/M_PI);	
	
	gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84);
	
	_vehicleState.Timestamp = imuTime;

#warning Either IMU needs to be mounted level or we need to incorporate a roll/pitch/yaw (in actuality only a pitch) offset.  This could actually be surprisingly easy if we just apply a rotation to the measurements coming out of the IMU.
	
	Matrix rtmp = Cgn*Cnb*vehicleOffsets;
	_vehicleState.Northing = utmForVehState.n + rtmp.getelem(0); 
	_vehicleState.Easting = utmForVehState.e + rtmp.getelem(1);
	_vehicleState.Altitude = height + rtmp.getelem(2);

#warning Check that we should be subtracting rrtmp and not adding.
	
	Matrix rrtmp = matrixCross(wn_bn)*rtmp;
	_vehicleState.Vel_N = vn.getelem(0) - rrtmp.getelem(0);
	_vehicleState.Vel_E = vn.getelem(1) - rrtmp.getelem(1);
	_vehicleState.Vel_D = vn.getelem(2) - rrtmp.getelem(2);
	
	
	Matrix rrrtmp = matrixCross((wn_bn - oldWn_bn)/dtSum)*rtmp - matrixCross(wn_bn)*rrtmp;
	_vehicleState.Acc_N = vdot.getelem(0) - rrrtmp.getelem(0);
	_vehicleState.Acc_E = vdot.getelem(1) - rrrtmp.getelem(1);
	_vehicleState.Acc_D = vdot.getelem(2) - rrrtmp.getelem(2);
	
	_vehicleState.Roll = roll;
	_vehicleState.Pitch = pitch;
	_vehicleState.Yaw = yaw + alpha;	
	_vehicleState.RollAcc = (wb_ibSum.getelem(0) - oldWb_ibSum.getelem(0))/dtSum;
	_vehicleState.PitchAcc = (wb_ibSum.getelem(1) - oldWb_ibSum.getelem(1))/dtSum;
	_vehicleState.YawAcc = (wb_ibSum.getelem(2) - oldWb_ibSum.getelem(2))/dtSum;
	_vehicleState.RollRate = wb_ibSum.getelem(0)/dtSum;
	_vehicleState.PitchRate = wb_ibSum.getelem(1)/dtSum;
	_vehicleState.YawRate = wb_ibSum.getelem(2)/dtSum;

	//Confidences are calculated as standard deviations...

	_vehicleState.NorthConf = sqrt(kfP.getelem(0,0))*EARTH_RADIUS;
	_vehicleState.EastConf = sqrt(kfP.getelem(1,1))*EARTH_RADIUS;
	_vehicleState.HeightConf = sqrt(kfP.getelem(2,2));
	_vehicleState.RollConf = sqrt(kfP.getelem(6,6));
	_vehicleState.PitchConf = sqrt(kfP.getelem(7,7));
	_vehicleState.YawConf = sqrt(kfP.getelem(8,8));
	
	oldWn_bn = wn_bn;
	oldWb_ibSum = wb_ibSum;
	DGCunlockMutex(&m_VehicleStateMutex);
	
	wb_ibSum.clear();
	dtSum = 0.0;
	
	if (count++ % 8 == 0) {
	  if (_astateOpts.debug == 1) {
	    cout.precision(20);
	    cout << lat << '\t' << lon << '\t' << gpsLat << '\t' << gpsLon << '\t' << roll << '\t' << pitch << '\t' << yaw + alpha << '\t' << gpsFOM << '\t';
	    cout << height << '\t' << gpsHeight << endl;
	  }
	  //All Your Network Traffic Are Belong To Us
	  if (_astateOpts.fastMode == 0) {
	    broadcast(); 
	  }
	}
      }
    } else if (stateMode == GPS_OBD || stateMode == GPS) {	
      // Stuff goes here.
    } else if (stateMode == OBD) {
      // Stuff goes here.
    } else if (stateMode == OH_SHIT) {
      cerr << "All heartbeats failed.  Shutting down state." << endl;
      exit(1);
    } else if (stateMode == STATIONARY) {
      // Stuff goes here.
    } else {
      stateMode = PREINIT; 
    }
  }
}

