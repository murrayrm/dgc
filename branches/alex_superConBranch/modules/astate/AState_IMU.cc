/* AState_IMU.cc - new vehicle state estimator
 *
 * JML 09 Jun 05
 *
 * */

#include "AState.hh"

void AState::imuInit()
{
  if (_astateOpts.useReplay == 1) {
    rawIMU imuIn;

    imuReplayStream.read((char*)&imuIn, sizeof(rawIMU));
    DGClockMutex(&m_IMUDataMutex);
    imuLogStart = imuIn.time;
    DGCunlockMutex(&m_IMUDataMutex);

    DGClockMutex(&m_MetaStateMutex);
    _metaState.imuEnabled = 1;
    DGCunlockMutex(&m_MetaStateMutex);
  } else {
    DGClockMutex(&m_MetaStateMutex);
    _metaState.imuEnabled = IMU_open();
    if(_metaState.imuEnabled == -1) {
      cerr << "AState::imuInit(): failed to open IMU" << endl;
    }
    DGCunlockMutex(&m_MetaStateMutex);
  }
}

void AState::imuThread()
{
  unsigned long long nowTime;
  unsigned long long rawImuTime;

  rawIMU imuIn;
  rawIMU imuOut;
  IMU_DATA imuData;

  while (!quitPressed) {
    if (_astateOpts.useReplay == 1) {
      if (!imuReplayStream) {
	quitPressed = 1;
	return;
      }
      imuReplayStream.read((char*)&imuIn, sizeof(rawIMU));
      memcpy(&imuData, &(imuIn.data), sizeof(IMU_DATA));
      rawImuTime = imuIn.time - imuLogStart + startTime;
      //Wait until time has caught up with raw input
      if (_astateOpts.fastMode == 0) {
	DGCgettime(nowTime);
	while( rawImuTime > nowTime ) {
	  usleep(1);
	  DGCgettime(nowTime);
	}
      }
    } else {
      IMU_read(&imuData); // Need to add new code for IMU_read!
      DGCgettime(rawImuTime); // time stamp as soon as data read.
    }
    if (_astateOpts.logRaw == 1) {
      imuOut.time = rawImuTime;
      
      memcpy(&(imuOut.data), &imuData, sizeof(IMU_DATA));
      
      imuLogStream.write((char*)&imuOut, sizeof(rawIMU));
    }

    DGClockMutex(&m_HeartbeatMutex);
    _heartbeat.imu = true;
    DGCunlockMutex(&m_HeartbeatMutex);

    DGClockMutex(&m_IMUDataMutex);


    if (((imuBufferReadInd + 1) % IMU_BUFFER_SIZE) == (imuBufferLastInd % IMU_BUFFER_SIZE)) {
      imuBufferFree.bCond = false;

      DGCunlockMutex(&m_IMUDataMutex);

      DGCWaitForConditionTrue(imuBufferFree);


      DGClockMutex(&m_IMUDataMutex);
    }

    ++imuBufferReadInd;

    memcpy(&(imuIn.data), &imuData, sizeof(IMU_DATA));
    imuIn.time = rawImuTime;

    memcpy(&imuBuffer[imuBufferReadInd % IMU_BUFFER_SIZE], &imuIn, sizeof(rawIMU));

    DGCunlockMutex(&m_IMUDataMutex);

    DGCSetConditionTrue(newData);
  }
}
