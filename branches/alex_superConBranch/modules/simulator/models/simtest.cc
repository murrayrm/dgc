// Simulator tests loop

#include "SimModel.hh"
#include<sys/time.h>
#include<time.h>
#include<unistd.h>
#include<fstream>

using namespace std;

int main(){
  timeval thetime;
  double t_start;
  double t_now;
  SimModel theModel;
  double steer = -0.1;
  double pedal = -1;
  double trans = 1;
  double speed, time_el;
  StateReport report;
  ofstream outfile("data.dat");
    
  cout<<"Initializing simulation... ";
  gettimeofday(&thetime, NULL);
  t_start = (double) thetime.tv_sec + (((double) thetime.tv_usec) / 1.0e6);
  report.n = 0;
  report.e = 0;
  report.nd = 10;
  report.ed = 10;
  report.phi = 0;
  report.yaw = 0;
  
  theModel.Init(report, t_start, 0.0, 0.699);
  cout<<"done"<<endl;

  while(1){
    gettimeofday(&thetime, NULL);
    t_now = (double) thetime.tv_sec + (((double) thetime.tv_usec) / 1.0e6);
    time_el = t_now - t_start;
    //steer = .1* sin(.1 * time_el);
    steer = 0;
    theModel.SetCommands(steer, pedal, trans);
    theModel.RunSim(t_now);
    report = theModel.GetFrontState();
    cout<<"n: "<<report.n<<"e: "<<report.e<<"speed: "<<speed<<
      "yaw: "<<report.yaw<<"phi: "<<report.phi<<endl;
    speed = sqrt(powf(report.nd, 2) + powf(report.ed, 2 ));
    outfile<<'\t'<<time_el<<'\t'<<report.n<<'\t'<<report.e<<'\t'<<speed<<
      '\t'<<report.yaw<<'\t'<<report.phi<<endl;
    usleep(10000);
  }

  return 0;
}

    
  


    
