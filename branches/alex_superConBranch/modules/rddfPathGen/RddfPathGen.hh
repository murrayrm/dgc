#ifndef RDDFPATHGEN_HH
#define RDDFPATHGEN_HH



using namespace std;

/********************************************
 * Options available to the user
 */

/** Whether or not to actually send traj's over skynet */
#define SEND_TRAJ 1

/** meters between consecutive points in a dense path */
#define DENSITY .5

/** If a turn has a radius greater than this, it will be
 * be reduced to this value.  This is so on extremely long
 * corridor segments we don't end up really far away from
 * the centerline of the corridor in exchange for a
 * marginal increase in maximum dynamically feasible
 * speed */
#define MAXRADIUS 200.0

/** When we chop the path, this is how far back along the path
 * from our current location we go */
#define CHOPBEHIND 5.0 

/** when we chop the path, this is how far forward along the
 * path we go from our current location */
#define CHOPAHEAD 100.0 

/** The file containing the rddf.  Most likely this will
 * be a symlink to dgc/rddf/rddf.dat */
#define RDDF_FILE "rddf.dat"

/** The file that the complete dense traj is output to. */
#define TRAJOUTPUT "rddf.traj"

/** The file that the complete sparse traj is output to. */
#define SPARSE_PATH_FILE "sparse_path.traj"

/** RddfPathGen will continuously output the most recent traj
 * to a file starting with this base name */
#define CHOPPED_OUTPUT_BASE "chopped_traj" 

/** frequency at which chopped dense traj's are sent over skynet*/
#define FREQUENCY 1

/** A slightly advanced option.  To deal with
 * overlapping paths, RddfPathGen keeps track of where
 * we are on the complete sparse path.  In theory,
 * there is a pathological example where our algorithm
 * breaks down (starting pathgen in the middle of a
 * long corridor with loops) and becomes stuck, 
 * thinking we are closest to a point quite far from
 * the actual vehicle location.  However, if at any
 * point we're farther than MAXDISTANCEFROMPATH from
 * the point on the path we think is closest, RPG will
 * do an exhaustive search of all points to guarantee
 * our choice of the closest point is the correct one.
 **/
#define MAXDISTANCEFROMPATH 30 

#include "CTimberClient.hh"
#include "StateClient.h"
#include "TrajTalker.h"
#include <pthread.h>
#include <unistd.h>
#include <sstream>
#include "pathLib.hh"
#include "CPath.hh"
#include "rddf.hh"

/** Several constants that RddfPathGen needs are
 * defined in AliceConstants.h.  These include:
 * -#RDDF_MAXACCEL - maximum forward acceleration
 * -#RDDF_MAXDECEL - maximum braking acceleration
 * -#RDDF_MAXLATERAL - maximum sideways acceleration
 * -#RDDF_VEHWIDTH_EFF - width subtracted from the
 *    corridor before any paths are computed.
 *    The minimum corridor width must be greater than
 *    this value.
 */
#include "AliceConstants.h" 
#include <iomanip>
#include <DGCutils>


/**
 * RddfPathGen class.
 * input: none
 * output: none
 */
class RddfPathGen : public CStateClient, public CTrajTalker, public CTimberClient
{
public:
  /** Contstructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  RddfPathGen(int skynetKey);

  /** Standard destructor */
  ~RddfPathGen();

  /** The main loop where execution is trapped */
  void ActiveLoop();

  /** not currently implemented (we don't need to receive any messages) */
  void ReceiveDataThread();

private:

  /** called when we need to know where we are (So we know about which point to
   * crop the traj */
  void GetLocation(vector<double> & location);

};

#endif  // RDDFPATHGEN_HH
