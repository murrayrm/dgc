function getAvg()

     files = ['sr010.calc'; 'sl010.calc'; 'sr020.calc'; 'sl020.calc'; 'sr030.calc'; 'sl030.calc'; 'sr040.calc'; 'sl040.calc'; 'sr045.calc'; 'sl045.calc'; 'sr050.calc'; 'sl050.calc'; 'sr055.calc'; 'sl055.calc'; 'sr060.calc'; 'sl060.calc'; 'sr070.calc'; 'sl070.calc'; 'sr080.calc'; 'sl080.calc'; 'sr090.calc'; 'sl090.calc'; 'sr100.calc'; 'sl100.calc'];
#     files = ['sr010.calc']

asdf = size(files);
num = asdf(1);

avgAct = [0,0];
avgCmd = [0,0];
AvsC = [0,0];
ratio = [];

for i=1:1:num 

curr = load(files(i,:));
len = size(curr(:,1));
tempAct = sum(curr(:,2));
tempCmd = sum(curr(:,3));
tempPhi = sum(curr(:,6));
Cmd = tempCmd / len(1);
Phi = tempPhi / len(1);
Act = tempAct / len(1);
avgAct = [avgAct; Act, Phi];
avgCmd = [avgCmd; Cmd, Phi];
AvsC = [AvsC; Act, Cmd];
ratio = [ratio; Cmd, Phi./Cmd];
endfor

save -ascii avgAct avgAct
save -ascii avgCmd avgCmd
save -ascii AvsC AvsC
save -ascii ratio ratio
