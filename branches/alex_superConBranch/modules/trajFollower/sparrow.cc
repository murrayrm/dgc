#include "trajFollowerTabSpecs.hh"
#include "TrajFollower.hh"
#include "fwd_trajF_sparrow.h"
#include "rev_trajF_sparrow.h"
#include <unistd.h>
#include <string.h>


void TrajFollower::startSparrowHawkDisplays() {
  
  CSparrowHawk &sh = SparrowHawk();

  sh.add_page(fwd_trajF_sparrow, "Forward PID Controller");
  sh.add_page(rev_trajF_sparrow, "Reverse PID Controller");

  /* FORWARDS CONTROLLER */
  sh.set_getter<double>( "fwd_yError", m_pPIDcontroller, &CPID_Controller::access_YError );
  sh.set_getter<double>( "fwd_aError", m_pPIDcontroller, &CPID_Controller::access_AError );
  sh.set_getter<double>( "fwd_cError", m_pPIDcontroller, &CPID_Controller::access_CError );
  sh.set_getter<double>( "fwd_yErrRef", m_pPIDcontroller, &CPID_Controller::access_YErrorRefPos );
  sh.set_getter<double>( "fwd_aErrRef", m_pPIDcontroller, &CPID_Controller::access_AErrorRefPos );
  sh.set_getter<double>( "fwd_aFFref", m_pPIDcontroller, &CPID_Controller::access_AFFRefPos );
  sh.set_getter<double>( "fwd_vRef", m_pPIDcontroller, &CPID_Controller::access_VRef );
  sh.set_getter<double>( "fwd_vTraj", m_pPIDcontroller, &CPID_Controller::access_VTraj );
  sh.set_getter<double>( "fwd_SmallMAXspeedC", this, &TrajFollower::access_SmallestMAXspeedC );
  sh.set_getter<double>( "fwd_LargeMINspeedC", this, &TrajFollower::access_LargestMINspeedC );
  sh.set_getter<double>( "fwd_accelFF", m_pPIDcontroller, &CPID_Controller::access_AccelFF );
  sh.set_getter<double>( "fwd_accelFB", m_pPIDcontroller, &CPID_Controller::access_AccelFB );
  sh.set_getter<double>( "fwd_accelCmd", m_pPIDcontroller, &CPID_Controller::access_AccelCmd );
  sh.set_getter<double>( "fwd_steerFF", m_pPIDcontroller, &CPID_Controller::access_SteerFF );
  sh.set_getter<double>( "fwd_steerFB", m_pPIDcontroller, &CPID_Controller::access_SteerFB );
  sh.set_getter<double>( "fwd_steerCmd", m_pPIDcontroller, &CPID_Controller::access_SteerCmd );
  sh.set_getter<double>( "fwd_LatGainp", m_pPIDcontroller, &CPID_Controller::access_LatGainp );
  sh.set_getter<double>( "fwd_LatGaini", m_pPIDcontroller, &CPID_Controller::access_LatGaini );
  sh.set_getter<double>( "fwd_LatGaind", m_pPIDcontroller, &CPID_Controller::access_LatGaind );
  sh.set_getter<double>( "fwd_LongGainp", m_pPIDcontroller, &CPID_Controller::access_LongGainp );
  sh.set_getter<double>( "fwd_LongGaini", m_pPIDcontroller, &CPID_Controller::access_LongGaini );
  sh.set_getter<double>( "fwd_LongGaind", m_pPIDcontroller, &CPID_Controller::access_LongGaind );
  sh.set_getter<double>( "fwd_LatErrp", m_pPIDcontroller, &CPID_Controller::access_LatErrp );
  sh.set_getter<double>( "fwd_LatErri", m_pPIDcontroller, &CPID_Controller::access_LatErri );
  sh.set_getter<double>( "fwd_LatErrd", m_pPIDcontroller, &CPID_Controller::access_LatErrd );
  sh.set_getter<double>( "fwd_LongErrp", m_pPIDcontroller, &CPID_Controller::access_LongErrp );
  sh.set_getter<double>( "fwd_LongErri", m_pPIDcontroller, &CPID_Controller::access_LongErri );
  sh.set_getter<double>( "fwd_LongErrd", m_pPIDcontroller, &CPID_Controller::access_LongErrd );
  
  sh.set_getter<double>( "fwd_PitchRateDeg", this, &TrajFollower::access_PitchRateDeg );
  sh.set_getter<double>( "fwd_RollRateDeg", this, &TrajFollower::access_RollRateDeg );
  sh.set_getter<double>( "fwd_YawRateDeg", this, &TrajFollower::access_YawRateDeg );
  sh.set_getter<double>( "fwd_PitchDeg", this, &TrajFollower::access_PitchDeg );
  sh.set_getter<double>( "fwd_RollDeg", this, &TrajFollower::access_RollDeg );
  sh.set_getter<double>( "fwd_YawDeg", this, &TrajFollower::access_YawDeg );

  sh.set_getter<double>( "fwd_d_timestamp", m_pPIDcontroller, &CPID_Controller::access_Timestamp );

  sh.rebind( "fwd_d_speed", &m_Speed2 );
  sh.rebind( "fwd_Northing", &m_state.Northing );
  sh.rebind( "fwd_Easting", &m_state.Easting );
  sh.rebind( "fwd_Altitude", &m_state.Altitude );
  sh.rebind( "fwd_Vel_N", &m_state.Vel_N );
  sh.rebind( "fwd_Vel_E", &m_state.Vel_E );
  sh.rebind( "fwd_Vel_D", &m_state.Vel_D );
  sh.rebind( "fwd_Pitch", &m_state.Pitch );
  sh.rebind( "fwd_Roll", &m_state.Roll );
  sh.rebind( "fwd_Yaw", &m_state.Yaw );
  sh.rebind( "fwd_PitchRate", &m_state.PitchRate );
  sh.rebind( "fwd_RollRate", &m_state.RollRate );
  sh.rebind( "fwd_YawRate", &m_state.YawRate );
  sh.rebind( "fwd_RecvTrajCount", &m_trajCount);
  sh.rebind( "fwd_steerCommandRad", &m_steer_cmd );
  sh.rebind( "fwd_accelCommand", &m_accel_cmd );
  

  
  /* REVERSE CONTROLLER */
  sh.set_getter<double>( "rev_yError", reversecontroller, &CPID_Controller::access_YError );
  sh.set_getter<double>( "rev_aError", reversecontroller, &CPID_Controller::access_AError );
  sh.set_getter<double>( "rev_cError", reversecontroller, &CPID_Controller::access_CError );
  sh.set_getter<double>( "rev_yErrRef", reversecontroller, &CPID_Controller::access_YErrorRefPos );
  sh.set_getter<double>( "rev_aErrRef", reversecontroller, &CPID_Controller::access_AErrorRefPos );
  sh.set_getter<double>( "rev_aFFref", reversecontroller, &CPID_Controller::access_AFFRefPos );
  sh.set_getter<double>( "rev_vRef", reversecontroller, &CPID_Controller::access_VRef );
  sh.set_getter<double>( "rev_vTraj", reversecontroller, &CPID_Controller::access_VTraj );
  sh.set_getter<double>( "rev_accelFF", reversecontroller, &CPID_Controller::access_AccelFF );
  sh.set_getter<double>( "rev_accelFB", reversecontroller, &CPID_Controller::access_AccelFB );
  sh.set_getter<double>( "rev_accelCmd", reversecontroller, &CPID_Controller::access_AccelCmd );
  sh.set_getter<double>( "rev_steerFF", reversecontroller, &CPID_Controller::access_SteerFF );
  sh.set_getter<double>( "rev_steerFB", reversecontroller, &CPID_Controller::access_SteerFB );
  sh.set_getter<double>( "rev_steerCmd", reversecontroller, &CPID_Controller::access_SteerCmd );
  sh.set_getter<double>( "rev_LatGainp", reversecontroller, &CPID_Controller::access_LatGainp );
  sh.set_getter<double>( "rev_LatGaini", reversecontroller, &CPID_Controller::access_LatGaini );
  sh.set_getter<double>( "rev_LatGaind", reversecontroller, &CPID_Controller::access_LatGaind );
  sh.set_getter<double>( "rev_LongGainp", reversecontroller, &CPID_Controller::access_LongGainp );
  sh.set_getter<double>( "rev_LongGaini", reversecontroller, &CPID_Controller::access_LongGaini );
  sh.set_getter<double>( "rev_LongGaind", reversecontroller, &CPID_Controller::access_LongGaind );
  sh.set_getter<double>( "rev_LatErrp", reversecontroller, &CPID_Controller::access_LatErrp );
  sh.set_getter<double>( "rev_LatErri", reversecontroller, &CPID_Controller::access_LatErri );
  sh.set_getter<double>( "rev_LatErrd", reversecontroller, &CPID_Controller::access_LatErrd );
  sh.set_getter<double>( "rev_LongErrp", reversecontroller, &CPID_Controller::access_LongErrp );
  sh.set_getter<double>( "rev_LongErri", reversecontroller, &CPID_Controller::access_LongErri );
  sh.set_getter<double>( "rev_LongErrd", reversecontroller, &CPID_Controller::access_LongErrd );
  
  sh.set_getter<double>( "rev_PitchRateDeg", this, &TrajFollower::access_PitchRateDeg );
  sh.set_getter<double>( "rev_RollRateDeg", this, &TrajFollower::access_RollRateDeg );
  sh.set_getter<double>( "rev_YawRateDeg", this, &TrajFollower::access_YawRateDeg );
  sh.set_getter<double>( "rev_PitchDeg", this, &TrajFollower::access_PitchDeg );
  sh.set_getter<double>( "rev_RollDeg", this, &TrajFollower::access_RollDeg );
  sh.set_getter<double>( "rev_YawDeg", this, &TrajFollower::access_YawDeg );

  sh.set_getter<double>( "rev_d_timestamp", m_pPIDcontroller, &CPID_Controller::access_Timestamp );

  sh.rebind( "rev_d_speed", &m_Speed2 );
  sh.rebind( "rev_Northing", &m_state.Northing );
  sh.rebind( "rev_Easting", &m_state.Easting );
  sh.rebind( "rev_Altitude", &m_state.Altitude );
  sh.rebind( "rev_Vel_N", &m_state.Vel_N );
  sh.rebind( "rev_Vel_E", &m_state.Vel_E );
  sh.rebind( "rev_Vel_D", &m_state.Vel_D );
  sh.rebind( "rev_Pitch", &m_state.Pitch );
  sh.rebind( "rev_Roll", &m_state.Roll );
  sh.rebind( "rev_Yaw", &m_state.Yaw );
  sh.rebind( "rev_PitchRate", &m_state.PitchRate );
  sh.rebind( "rev_RollRate", &m_state.RollRate );
  sh.rebind( "rev_YawRate", &m_state.YawRate );
  sh.rebind( "rev_RecvTrajCount", &m_trajCount);
  sh.rebind( "rev_steerCommandRad", &m_steer_cmd );
  sh.rebind( "rev_accelCommand", &m_accel_cmd );
  sh.rebind( "rev_distleft", &distancelefttoreverse );

  //Start the SparrowHawk displays
  sh.run();  
}
  

void TrajFollower::startGUItab() {
#warning "this sleep is a hack"
  sleep(1);
  while(true) 
  {
    if(reverse == 0)
     {
       /* FORWARDS CONTROLLER */
       
       m_input.Northing = m_state.Northing;
       m_input.Easting = m_state.Easting;
       m_input.Altitude = m_state.Altitude;
       m_input.Vel_N = m_state.Vel_N;
       m_input.Vel_E = m_state.Vel_E;
       m_input.Vel_U = m_state.Vel_D;
       m_input.PitchRad = m_state.Pitch;
       m_input.RollRad = m_state.Roll;
       m_input.YawRad = m_state.Yaw;
       m_input.PitchRateRad = m_state.PitchRate;
       m_input.RollRateRad = m_state.RollRate;
       m_input.YawRateRad = m_state.YawRate;
       
       m_input.SteerCommandRad = m_steer_cmd;
       m_input.AccelCommand = m_accel_cmd;
       
       m_input.yError = m_pPIDcontroller->access_YError();
       m_input.aError = m_pPIDcontroller->access_AError();
       m_input.cError = m_pPIDcontroller->access_CError();
       
       m_input.yErrorRef = m_pPIDcontroller->access_YErrorRefPos();
       m_input.aErrorRef = m_pPIDcontroller->access_AErrorRefPos();
       m_input.SteerFFRef = m_pPIDcontroller->access_AFFRefPos();
       
       m_input.VRef = m_pPIDcontroller->access_VRef();
       m_input.VTraj = m_pPIDcontroller->access_VTraj();
    
       m_input.AccelFF = m_pPIDcontroller->access_AccelFF();
       m_input.AccelFB = m_pPIDcontroller->access_AccelFB();
       m_input.AccelCmd = m_pPIDcontroller->access_AccelCmd();
       
       m_input.SteerFF = m_pPIDcontroller->access_SteerFF();
       m_input.SteerFB = m_pPIDcontroller->access_SteerFB();
       m_input.SteerCmd = m_pPIDcontroller->access_SteerCmd();
       
       m_input.LatGainP = m_pPIDcontroller->access_LatGainp() ;
       m_input.LatGainI = m_pPIDcontroller->access_LatGaini() ;
       m_input.LatGainD = m_pPIDcontroller->access_LatGaind() ;
       m_input.LongGainP = m_pPIDcontroller->access_LongGainp() ;
       m_input.LongGainI = m_pPIDcontroller->access_LongGaini() ;
       m_input.LongGainD = m_pPIDcontroller->access_LongGaind() ;
       
       m_input.Time = m_pPIDcontroller->access_Timestamp();
       m_input.Speed = m_state.Speed2();
       
       m_input.PitchDeg = access_PitchDeg();
       m_input.RollDeg = access_RollDeg();
       m_input.YawDeg = access_YawDeg();
       m_input.PitchRateDeg = access_PitchRateDeg();
       m_input.RollRateDeg = access_RollRateDeg();
       m_input.YawRateDeg = access_YawRateDeg();       
     }
   else
     {
       /* REVERSE CONTROLLER */

       m_input.SteerCommandRad = m_steer_cmd;
       m_input.AccelCommand = m_accel_cmd;
       
       m_input.yError = reversecontroller->access_YError();
       m_input.aError = reversecontroller->access_AError();
       m_input.cError = reversecontroller->access_CError();
       
       m_input.yErrorRef = reversecontroller->access_YErrorRefPos();
       m_input.aErrorRef = reversecontroller->access_AErrorRefPos();
       m_input.SteerFFRef = reversecontroller->access_AFFRefPos();
       
       m_input.VRef = reversecontroller->access_VRef();
       m_input.VTraj = reversecontroller->access_VTraj();
       
       m_input.AccelFF = reversecontroller->access_AccelFF();
       m_input.AccelFB = reversecontroller->access_AccelFB();
       m_input.AccelCmd = reversecontroller->access_AccelCmd();
       
       m_input.SteerFF = reversecontroller->access_SteerFF();
       m_input.SteerFB = reversecontroller->access_SteerFB();
       m_input.SteerCmd = reversecontroller->access_SteerCmd();
       
       m_input.LatGainP = reversecontroller->access_LatGainp();
       m_input.LatGainI = reversecontroller->access_LatGaini();
       m_input.LatGainD = reversecontroller->access_LatGaind();
       m_input.LongGainP = reversecontroller->access_LongGainp();
       m_input.LongGainI = reversecontroller->access_LongGaini();
       m_input.LongGainD = reversecontroller->access_LongGaind();

       m_input.PitchDeg = access_PitchDeg();
       m_input.RollDeg = access_RollDeg();
       m_input.YawDeg = access_YawDeg();
       m_input.PitchRateDeg = access_PitchRateDeg();
       m_input.RollRateDeg = access_RollRateDeg();
       m_input.YawRateDeg = access_YawRateDeg();       


     }
    
    m_input.TrajCount = m_trajCount;
    
    // Wait a bit
    usleep(500000); 
  }
}
