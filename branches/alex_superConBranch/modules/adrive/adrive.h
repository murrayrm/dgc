/*!**
 *  Adrive mutex version header.  
 *  This defines the structures for adrive 
 *  Including vehicle, ports, actuators, and errors.  
 *  ALso all constants are in here.  
 *  TUlly Foote
 *  12/28/04
 */



#ifndef _ADRIVE_MUTEX
#define _ADRIVE_MUTEX
#include <iostream>
#include <fstream>
#include <pthread.h>

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>
#include <sys/time.h>
#include <time.h>
#include <errno.h>

#include "DGCutils"
#include "parker_steer.h"
#include "sn_msg.hh"
#include "actuators.h"
#include "brake.h"
#include "throttle.h"

#include "OBDIIDriver.hpp"



/******  COnstants for adrive componants *******/

/* Define status values. */
#define ON 1
#define OFF 0
#define ERROR -1
#define ENABLED 1
#define DISABLED 0
#define ERROR_HAPPENED 5
#define NO_ERROR true
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

//Values use usleep
#define LOGGING_DELAY 10000  //100Hz
#define ACTUATOR_COMMAND_SLEEP_TIME 100000  //10Hz
#define MAX_COMMAND_WAIT 1  // One second
#define SUPERVISORY_SLEEP_LENGTH 1000000 //Needs to be longer than STATUS_SLEEP_LENGTH 
#define STATUS_SLEEP_LENGTH 500000 /* 2Hz 
				    * We've had problems upping this, I thimnk 
				    * that we might have satureated the serial 
				    * port.  
				    *
				    * Previous changes
				    * Set at 1 Hz right now.  
				    * 10 Hz made things terrible at S.A.
				    * I've tested it at 20Hz in the shop
				    * However I do not have any way to test for 
				    * message/packet loss.  So I'm going to back  
				    * off from that tested value of 50000us of sleep. */
#define ACTUATOR_STATE_BROADCAST_INTERVAL 30000 // send the actuator state at ~30Hz

#define BRAKE_PAUSE_POSITION .7  // How strongly the brake will turn on when put into pause.  


typedef int error_t;
typedef int (*actuator_command_function)(double position);
typedef int (*actuator_status_function)();
typedef void (*actuator_init_function)();
typedef int (*actuator_obdii_command_function)(double command);
typedef int (*actuator_obdii_status_function)();
typedef void (*actuator_obdii_init_function)();
typedef int (*actuator_estop_command_function)(double command);
typedef int (*actuator_estop_status_function)();
typedef void (*actuator_estop_init_function)();



/*! An enumeration of all the actuators associated with adrive. */
enum ACTUATOR_TYPE {
  ACTUATOR_BRAKE, ACTUATOR_GAS, ACTUATOR_TRANS, ACTUATOR_STEER, NUM_ACTUATORS
};

/*! An enumeration of estop states. */
enum ESTOP_TYPES {
  ESTOP_RUN, ESTOP_PAUSE, ESTOP_DISABLE
};



/*! The generic actuator type.  
 *  Valid for throttle, brake, steering, and transmission 3/10/05. 
 * It has a little extra unused data, but optimization has not been an issue. */
struct actuator_t
{
  pthread_mutex_t mutex;
  
  pthread_once_t start_bit;
  pthread_cond_t cond_flag;
  
  /*! The port number of the actuator. */
  int port;
  /*! A boolean of whether the actuator status thread is enabled. */
  int status_enabled;
  /*! A boolean of whether the actuator status thread is enabled. */
  int command_enabled;
  /*! An unused counter */
  int status_loop_counter;
  /*! An unused counter */
  int command_loop_counter;
  /*! A boolean current status of the actuator */
  int status;
  /*! The current commanded position.  Values dependant on actuator. */
  double command;
  /*! The current actuator position.  Values dependant on actuator. */
  double position;
  /* The actuator velocity */
  double velocity;
  /* The actuator acceleration */
  double acceleration;
  /*! The current actuator independant feedback.  Values dependant on actuator. */
  double pressure;
  /*! The actuator error struct */
  error_t error;
  /*! The name of the actutor, unused*/
  char name[100];
  /*! The address of the initialization function */
  actuator_init_function execute_init;
  /*! The address of the command function */
  actuator_command_function execute_command;
  /*! The address of the status update function */
  actuator_status_function execute_status;
  /*! The pthread handle for the command thread. */
  pthread_t pthread_command_handle;
  /*! The pthread handle for the status thread. */
  pthread_t pthread_status_handle;
}; 

struct estop_t
{
  pthread_mutex_t mutex;
  
  pthread_once_t start_bit;
  pthread_cond_t cond_flag;
  
  /*! The port number of the actuator. */
  int port;
  /*! A boolean of whether the actuator status thread is enabled. */
  int status_enabled;
  /*! A boolean of whether the actuator status thread is enabled. */
  int command_enabled;
  /*! An unused counter */
  int status_loop_counter;
  /*! An unused counter */
  int command_loop_counter;
  /*! A boolean current status of the actuator */
  int status;
  /*! The current commanded position.  Values dependant on actuator. */
  int command;
  /*! The current actuator position.  Values dependant on actuator. */
  int position;
  /* The actuator disable positon */
  int astop;
  /* the darpa command */
  int dstop;

  /*! The actuator error struct */
  error_t error;
  /*! The name of the actutor, unused*/
  char name[100];
  /*! The address of the initialization function */
  actuator_init_function execute_init;
  /*! The address of the command function */
  actuator_command_function execute_command;
  /*! The address of the status update function */
  actuator_status_function execute_status;
  /*! The pthread handle for the command thread. */
  pthread_t pthread_command_handle;
  /*! The pthread handle for the status thread. */
  pthread_t pthread_status_handle;
}; 

struct obdii_t 
{
  pthread_mutex_t mutex;
  
  pthread_once_t start_bit;
  pthread_cond_t cond_flag;

  /*! The port number of the actuator. */
  int port;
  /*! A boolean of whether the actuator status thread is enabled. */
  int status_enabled;
  /*! A boolean of whether the actuator status thread is enabled. */
  int command_enabled;
  /*! An unused counter */
  int status_loop_counter;
  /*! An unused counter */
  int command_loop_counter;
  /*! A boolean current status of the actuator */
  int status;
  /* The actuator's name */
  char name[100];


  /*! The class that contains all the function calls. */
  OBDIIDriver my_obdii_driver;
  /*! The RPM of the engine */
  int engineRPM;
  /*! The time since the engine started in seconds */
  int TimeSinceEngineStart;
  /*! Vehicle speed m/s */
  int VehicleWheelSpeed;
  /*! The temperature of the engine coolant. */
  float EngineCoolantTemp;
  /*! The engine torque in foot pounds */
  double WheelForce;
  /*! The time the glow plug has been on??? in seconds */
  int GlowPlugLampTime;
  /*! The position of the accelerator petal UNITS?? */
  int ThrottlePosition;
  /*! The current gear ratio */
  float CurrentGearRatio;
  /*! Battery Voltate */
  float BatteryVoltage;
  /*! The address of the initialization function */
  actuator_obdii_init_function  execute_init;
  /*! The address of the command function */
  actuator_obdii_command_function execute_command;
  /*! The address of the status update function */
  actuator_obdii_status_function execute_status;
  /*! The pthread handle for the status thread. */
  pthread_t pthread_status_handle;
};




/** The vehicle struct.  This contains all the adrive data. */
struct vehicle_t {
  struct actuator_t actuator[NUM_ACTUATORS];
  struct obdii_t actuator_obdii;
  struct estop_t actuator_estop;
  int skynet_key;
  int shutdown;
};

/* A function to initialize the vehicle struct */
// moved to askynet_monitor.hh
//void init_vehicle_struct(vehicle_t & new_vehicle);

/* the global vehicle*/
extern struct vehicle_t my_vehicle;

// Global mutexes and conds
/* The mutex conditional that allows the simple steering calibration hack */
extern pthread_cond_t steer_calibrated_flag;


/*! A function to update the overall estop position based on 
 * the current inputs */
int updateEstopPosition(vehicle_t *pVehicle);

void* logging_main (void* arg);
void read_config_file(vehicle_t & my_vehicle);
void* sparrow_main (void* arg);
void* actuator_status_thread_function(void* arg);
void* actuator_command_thread_function(void* arg);
void* actuator_obdii_status_thread_function(void * arg);
void* actuator_estop_status_thread_function(void * arg);
void* supervisory_thread (void* arg);

int execute_adrive_pause();

#endif // __ADRIVE_H__
	  
