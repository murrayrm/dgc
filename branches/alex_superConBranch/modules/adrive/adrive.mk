ADRIVE_PATH = $(DGC)/modules/adrive
#Libraries
ADRIVE_LIBS = $(SDSLIB) $(SKYNETLIB) $(SPARROWLIB) $(CDD) $(ALICELIB) $(MODULEHELPERSLIB) $(TIMBERLIB)

#Source
ADRIVE_VEHLIB_CFILES = 	$(VEHLIB_PATH)/throttle.c \
			$(VEHLIB_PATH)/brake.c \
			$(VEHLIB_PATH)/parker_steer.cc 

ADRIVE_VEHLIB_HEADERS = $(VEHLIB_PATH)/throttle.h \
			$(VEHLIB_PATH)/brake.h \
			$(VEHLIB_PATH)/parker_steer.h 

ADRIVE_VEHLIB_DEPEND = $(ADRIVE_VEHLIB_CFILES) $(ADRIVE_VEHLIB_HEADERS)


ADRIVE_ADRIVE_CFILES = 	$(ADRIVE_PATH)/actuators.c \
			$(ADRIVE_PATH)/adrive.c \
			$(ADRIVE_PATH)/askynet_monitor.cc \
			$(ADRIVE_PATH)/sn_send_commandline.cc \
			$(ADRIVE_PATH)/AdriveTimberClient.cc \
			$(ADRIVE_PATH)/adriveMain.c

ADRIVE_ADRIVE_HEADERS = $(ADRIVE_PATH)/actuators.h \
			$(ADRIVE_PATH)/adrive.h \
			$(ADRIVE_PATH)/askynet_monitor.hh \
			$(ADRIVE_PATH)/addisp.dd \
			$(ADRIVE_PATH)/adrive_skynet_interface_types.h \
			$(ADRIVE_PATH)/AdriveTimberClient.hh \

ADRIVE_ADRIVE_DEPEND =  $(ADRIVE_ADRIVE_CFILES) $(ADRIVE_ADRIVE_HEADERS) 


ADRIVE_DEPEND = $(ADRIVE_ADRIVE_DEPEND) $(ADRIVE_LIBS)
#$(ADRIVE_VEHLIB_DEPEND) 
