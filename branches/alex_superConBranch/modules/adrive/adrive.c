/* This Is A Initial Try At Building A Threaded Adrive.  
 * It Lays Down The Functions Needed For Each Actuator.  
 * And Provides All The Structure.  
 * Tully Foote
 * 12/29/04
 */

/* This now holds most of the funcitons called by adrive. The main function
 * is in adriveMain.c 
 * Tully Foote
 * 8/09/05
 */



#include "adrive.h"
#include "askynet_monitor.hh"

#include "sparrow/display.h"     /* sparrow display header */
#include "sparrow/debug.h"       /* sparrow debugging routines */
#include "addisp.h"			/* adrive display table */

using namespace std;


// Set initial values of flags.  
int shutdown_flag = false;
int run_sparrow_thread = false;
int run_skynet_thread = false;
int run_supervisory_thread = false;
int run_logging_thread = false;
int run_obdii_thread = false;
pthread_cond_t steer_calibrated_flag;
int simulation_flag = false;

// A place to store the IP of the simulator
char simulator_IP[15];


struct vehicle_t my_vehicle;		/* The vehicle data structure */

int flag_gas_condition(long x);
int flag_brake_condition(long x);
int flag_steer_condition(long x);
int flag_steer_enable(long x);


/******************** READ CONFIG FILE ******************/
/* This function opens adrive.config and fills in the vehicle struct.
 * The most important parts are reading the ports for the actuators,
 * as well as finding out which threads are disabled.  By default the vehicle
 * will be in expected race ready condition. */

void read_config_file(vehicle_t & my_vehicle) 
{
  ifstream infile("adrive.config");

  std::string cmd;

  while ( ! infile.eof() )
    {
      infile >> cmd;
      /* TO BE SAFE ALL OF THESE WRITINGS WOULD NEED TO BE MUTEX LOCKED
       * BUT SINCE THIS IS A ONETIME START UP BEFORE THE OTHER THREADS START 
       * IT IS OK.  IF THAT CHANGES LOCKING MUST BE ADDED.  */
      // Serial Ports
      if( cmd == "gas_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_GAS].port;
	  //printf("using gas_port &d\n", my_vehicle.actuator[ACTUATOR_GAS].port);
	}
      else if ( cmd == "brake_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_BRAKE].port;
	  //printf("using brake_port &d\n", my_vehicle.actuator[ACTUATOR_BRAKE].port);
	}
      else if ( cmd == "trans_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_TRANS].port;
	  //printf("using trans_port &d\n", my_vehicle.actuator[ACTUATOR_TRANS].port);
	}
      else if ( cmd == "estop_port" )
	{
	  infile >> my_vehicle.actuator_estop.port;
	  //printf("using estop_port &d\n", my_vehicle.actuator_estop.port);
	}
      else if ( cmd == "steer_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_STEER].port;
	  //printf("using steer_port &d\n", my_vehicle.actuator[ACTUATOR_STEER].port);
	}
      else if ( cmd == "obdii_port" )
	{
	  infile >> my_vehicle.actuator_obdii.port;
	  //printf("using steer_port &d\n", my_vehicle.actuator[ACTUATOR_STEER].port);
	}


      /***********************************
       * Check for disabled threads.
       ***********************************/
      // Status Threads
      else if ( cmd == "enable_steer_status" )
	{
	  my_vehicle.actuator[ACTUATOR_STEER].status_enabled = 1;
	}
      else if ( cmd == "enable_brake_status" )
	{
	  my_vehicle.actuator[ACTUATOR_BRAKE].status_enabled = 1;
	}
      else if ( cmd == "enable_gas_status" )
	{
	  my_vehicle.actuator[ACTUATOR_GAS].status_enabled = 1;
	}
      else if ( cmd == "enable_trans_status" )
	{
	  my_vehicle.actuator[ACTUATOR_TRANS].status_enabled = 1;
	}
      else if ( cmd == "enable_estop_status" )
	{
	  my_vehicle.actuator_estop.status_enabled = 1;
	}
      else if ( cmd == "enable_obdii_status" )
	{
	  my_vehicle.actuator_obdii.status_enabled = 1;
	}

      // Command Threads
      else if ( cmd == "enable_steer_command" )
	{
	  my_vehicle.actuator[ACTUATOR_STEER].command_enabled = 1;
	}
      else if ( cmd == "enable_brake_command" )
	{
	  my_vehicle.actuator[ACTUATOR_BRAKE].command_enabled = 1;
	}
      else if ( cmd == "enable_gas_command" )
	{
	  my_vehicle.actuator[ACTUATOR_GAS].command_enabled = 1;
	}
      else if ( cmd == "enable_trans_command" )
	{
	  my_vehicle.actuator[ACTUATOR_TRANS].command_enabled = 1;
	}
      else if ( cmd == "enable_estop_command" )
      	{
	  my_vehicle.actuator_estop.command_enabled = 1;
	}


      // Check for Sparrow and Skynet interface enabled
      else if ( cmd == "enable_sparrow" )
	{
	  //The default is true
	  run_sparrow_thread = true;
	}	  
      else if ( cmd == "enable_skynet" )
	{
	  //The default is true
	  run_skynet_thread = true;
	}	  
      else if ( cmd == "enable_logging" )
	{
	  //The default is true
	  run_logging_thread = true;
	}	  
      else if ( cmd == "enable_supervisory_thread" )
	{
	  //The default is true
	  run_supervisory_thread = true;
	}	  
      else if ( cmd == "enable_simulation" )
	{
	  //The default is true
	  simulation_flag = true;
	  infile >> simulator_IP;
	}	  
    }

}


/******************* SPARROW KEY BINDING FUNCTIONS ****************/

/* Sparrow requires the bound key to return int and take a long
 * these are wrapper functions to allow the pthread_cond_broadcast to
 * activate the command threads.  */

// Callback functions from sparrow to start command threads.  
/*! The gas function call for sparrow bind key to execute a gas command. */
int flag_gas_condition(long x)
{
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );
  return 0;
}
/*! The brake function call for sparrow bind key to execute a brake command. */
int flag_brake_condition(long x)
{
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
  return 0;
}
/*! The steer function call for sparrow bind key to execute a steer command. */
int flag_steer_condition(long x)
{
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_STEER].cond_flag );
  return 0;
}
/*! The steer function call for sparrow bind key to set steer velocity. */
int flag_steer_velocity(long x)
{
  execute_steer_velocity(my_vehicle.actuator[ACTUATOR_STEER].velocity);
  return 0;
}
/*! The steer function call for sparrow bind key to set steer acceleration. */
int flag_steer_acceleration(long x)
{
  execute_steer_acceleration(my_vehicle.actuator[ACTUATOR_STEER].acceleration);
  return 0;
}


/*! This function is for the sparrow binding to reenable the steering motor
 * after being manually diabled. */
int flag_steer_enable(long x)
{
  steer_enable_overide();
  return 0;
}

/******************** updateEstopPosition ************************/
/*
 * A function to update the overall position of the estop based 
 * on each individual input.  */


int updateEstopPosition(vehicle_t * pVehicle)
{
  if (pVehicle->actuator_estop.astop == DISABLE ||
      pVehicle->actuator_estop.command == DISABLE ||
      pVehicle->actuator_estop.dstop == DISABLE)
    {
      pVehicle->actuator_estop.position = DISABLE;
    }

  else if
    (pVehicle->actuator_estop.astop == PAUSE ||
     pVehicle->actuator_estop.command == PAUSE ||
     pVehicle->actuator_estop.dstop == PAUSE)
    {
      pVehicle->actuator_estop.position = PAUSE;
    }
  
  else 
    pVehicle->actuator_estop.position = RUN;
  
  
  return 1;
}



/******************** LOGGING THREAD *******************************/

/*! The function that is called as the logging thread of adrive if enabled.  
 * This function periodically records the actuators commanded and 
 * actual positions to file. */

void* logging_main (void* arg){
  char testlog[255];
  char datestamp[255];
  unsigned long long current_time;
  fstream testlogfile;
  time_t t = time(NULL);
  tm *local;
  

  local = localtime(&t);
  
  //sprintf(testlog,"adrive_log.dat");
  sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
	  local->tm_mon+1, local->tm_mday, local->tm_year+1900,
	  local->tm_hour, local->tm_min, local->tm_sec);
 
  sprintf(testlog, "logs/adrive_%s.log", datestamp);
    
  while (run_logging_thread)
    {
      //      printf("Running Logging thread");
      // Fed up with DGCutils
      //	timeval tv;
      //	gettimeofday(&tv, NULL);
      //	current_time = (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;      
      DGCgettime(current_time);
      testlogfile.open(testlog, fstream::out|fstream::app);
      testlogfile.precision(10);
      testlogfile <<  current_time << '\t';
      testlogfile <<  my_vehicle.actuator[ACTUATOR_STEER].command << '\t' << my_vehicle.actuator[ACTUATOR_STEER].position << '\t';
      testlogfile <<  my_vehicle.actuator[ACTUATOR_BRAKE].command << '\t' << my_vehicle.actuator[ACTUATOR_BRAKE].position << '\t' << my_vehicle.actuator[ACTUATOR_BRAKE].pressure << "\t";
      testlogfile <<  my_vehicle.actuator[ACTUATOR_GAS].command   << '\t' << my_vehicle.actuator[ACTUATOR_GAS].position   << "\r\n" ;
      testlogfile.close();
      //DGCgettime(current_time);
      //cout << current_time << "Before the logging sleep\n";
      usleep(LOGGING_DELAY);
      //DGCgettime(current_time);
      //cout << current_time << "Before the logging sleep\n";
    }
  return 0;
}



/************************* SPARROW THREAD *************************/

/*! This function runs as the sparrow thread of adrive. When sparrow is 
 * enabled this thread waits for the steering to calibrate then will take over
 * the screen with the UI.  
 *
 **** Caution should be exercized when using this interface there are
 * no protections against bad values and all fields are editable.  
 * */
void* sparrow_main (void* arg)
{
  //printf("Sparrow main is running\n");

  pthread_mutex_lock(& my_vehicle.actuator[ACTUATOR_STEER].mutex);
  
  //printf("command %d, status %d\n", my_vehicle.actuator[ACTUATOR_STEER].command_enabled, my_vehicle.actuator[ACTUATOR_STEER].status_enabled);
  if( my_vehicle.actuator[ACTUATOR_STEER].command_enabled || my_vehicle.actuator[ACTUATOR_STEER].status_enabled)
    {
      //    printf("the steering is enabled I will wait\n");
      pthread_cond_wait(&steer_calibrated_flag, &my_vehicle.actuator[ACTUATOR_STEER].mutex);
      // printf("sparrow has been awakened\n");
    }
  pthread_mutex_unlock(& my_vehicle.actuator[ACTUATOR_STEER].mutex);

  /* Initialize the screen and run the main display loop */
  if (dd_open() < 0) { dbg_error("can't open display"); exit(-1); }
  if (dd_usetbl(addisp) < 0) { dbg_error("can't open display table"); exit(-1); }
  //*! Pressing g executes the gas command */
  dd_bindkey( 'g' , flag_gas_condition   ); 
  //*! Pressing b executes the brake command */
  dd_bindkey( 'b' , flag_brake_condition );
  //*! Pressing s executes the steer command */
  dd_bindkey( 's' , flag_steer_condition );
  //*! Pressing e reenables steering */
  dd_bindkey( 'e' , flag_steer_enable );
  //*! Pressing c sets steering acceleration */
  dd_bindkey( 'c' , flag_steer_acceleration );
  //*! Pressing v sets steering velocity*/
  dd_bindkey( 'v' , flag_steer_velocity    );
  dd_loop();
  dd_close();

  sleep(3);
  exit(0);
}



/********************** STATUS THREAD *************************/

/*! This is the generic thread function for all actuator status loops.  
 * Each different actuator status thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_status_thread_function(void* arg)
{
  actuator_t *act = (actuator_t*) arg;

  printf("Starting Thread: %s Status\n", act->name);

  pthread_once(&(act->start_bit), act->execute_init);

  while( !shutdown_flag && act->status_enabled) {  
    /* This if statement is a hack for currently the estop status call will
     * block until a change in state. Adrive relies on quick resonses even if they
     * do nothing or are redundant.  */
	// Lock the mutex
	pthread_mutex_lock(&act->mutex);
    // Execute the function
    if ( act->execute_status() == ERROR_HAPPENED) 
      {
	//printf("%s Status: SOMETHING BAD HAPPENED\n", act->name);
      }
    else {
      act->status_loop_counter++;
      //printf("%s Status: Que Bueno! %d\n", act->name, act->status_loop_counter);
    }
    /* This is the 2nd half of the hack to take care of blocking estop status */

	// Unlock the mutex
	pthread_mutex_unlock(&act->mutex);

    // Sleep before repeating
    //DGCgettime(current_time);
    //cout << current_time << "Before the status sleep\n";
    usleep(STATUS_SLEEP_LENGTH);
    //DGCgettime(current_time);
    //cout << current_time << "After the status sleep\n";
    
  }
  
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
  return NULL;
}



/*********************** COMMAND THREAD ************************/

/*! This is the generic thread function for all actuator command loops.  
 * Each different actuator command thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_command_thread_function(void* arg)
{
  actuator_t *act = (actuator_t*) arg;
  struct timespec ts;
  struct timeval tp;

  //printf("Starting Thread: %s Command\n", act->name);

  pthread_once(&(act->start_bit), act->execute_init);

  while( !shutdown_flag && act->command_enabled) {  

    // Lock the mutex
    pthread_mutex_lock(&act->mutex);
    gettimeofday(&tp, NULL);
    ts.tv_sec = tp.tv_sec;
    ts.tv_nsec = tp.tv_usec * 1000;
    ts.tv_sec += MAX_COMMAND_WAIT;
    
    if (ETIMEDOUT == pthread_cond_timedwait(&act->cond_flag, &act->mutex, &ts))
      {
	//	cout << act->name << " command loop timed out going to pause. " << endl;
	execute_adrive_pause();		    
      }
    //    cout << "Executed timed wait" << endl;


    if ( act->execute_command(act->command) == ERROR_HAPPENED) 
      {
	//printf("%s Command: SOMETHING BAD HAPPENED\n", act->name);
      }
    else {
      //      act->command_loop_counter++;
      //printf("%s Command: Que Bueno! %d\n", act->name, act->command_loop_counter);
    }
    // Unlock the mutex
    pthread_mutex_unlock(&act->mutex);   
    /* Sleep to prevent overloading the serial interface */
    //DGCgettime(current_time);
    //cout << current_time << "Before the actuator command sleep\n";
    usleep(ACTUATOR_COMMAND_SLEEP_TIME);
    //DGCgettime(current_time);
    //cout << current_time << "After the actuator command sleep\n";

  }
  
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
  return NULL;
}



/***************************** OBDII THREAD ********************************/

/*! This is the OBDII copy of the generic thread function for all actuator status loops.  
 * Each different actuator status thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_obdii_status_thread_function(void* arg)
{
  obdii_t *act = (obdii_t*) arg;

  printf("Starting Thread:OBDII Status\n");

  pthread_once(&(act->start_bit), act->execute_init);

  while( !shutdown_flag && act->status_enabled) {  
    // Lock the mutex
    pthread_mutex_lock(&act->mutex);

    // Execute the function
    if ( act->execute_status() == ERROR_HAPPENED) 
      {
      //printf("%s Status: SOMETHING BAD HAPPENED\n", act->name);
      }
    else {
      act->status_loop_counter++;
      //printf("%s Status: Que Bueno! %d\n", act->name, act->status_loop_counter);
    }
    // Unlock the mutex
    pthread_mutex_unlock(&act->mutex);
    // Sleep before repeating
    //DGCgettime(current_time);
    //cout << current_time << "Before the status sleep\n";
    usleep(STATUS_SLEEP_LENGTH);
    //DGCgettime(current_time);
    //cout << current_time << "After the status sleep\n";
    
  }
  
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
  return NULL;
}

/***************************** ESTOP THREAD ********************************/

/*! This is the ESTOP copy of the generic thread function for all actuator status loops.  
 * Each different actuator status thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_estop_status_thread_function(void* arg)
{
  estop_t *act = (estop_t*) arg;

  printf("Starting Thread:ESTOP Status\n");

  pthread_once(&(act->start_bit), act->execute_init);

  while( !shutdown_flag && act->status_enabled) {  
    // Lock the mutex
    pthread_mutex_lock(&act->mutex);

    // Execute the function
    if ( act->execute_status() == ERROR_HAPPENED) 
      {
      //printf("%s Status: SOMETHING BAD HAPPENED\n", act->name);
      }
    else {
      act->status_loop_counter++;
      //printf("%s Status: Que Bueno! %d\n", act->name, act->status_loop_counter);
    }
    // Unlock the mutex
    pthread_mutex_unlock(&act->mutex);

    //Update the estop position given the new position
    updateEstopPosition(&my_vehicle);


    // Sleep before repeating
    //DGCgettime(current_time);
    //cout << current_time << "Before the status sleep\n";
#ifdef ESTOP_SDS
    /* This sleep is only used for the ESTOP_SDS because the serial port blocks 
    * whereas the SDS will return the latest message.  */

    //    cout << "Sleeping for SDS" << endl;
    usleep(STATUS_SLEEP_LENGTH);
#endif //ESTOP_SDS
    
//DGCgettime(current_time);
    //cout << current_time << "After the status sleep\n";
    
  }
  
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
  return NULL;
}



/**************************SUPERVISORY_THREAD *************************/



void * supervisory_thread (void * arg)
{
  while (!shutdown_flag)
    {
      //  vehicle_t *my_vehicle = (vehicle_t*) arg;
      int last_estop_count = 0;
      
      
      /* These are just to take care of the critical bugs.  There should be 
       * much more through error checking with restart capabilities */
      
      // Check that estop is still recieving commands
      if (my_vehicle.actuator_estop.status_enabled)
	{
	  if (last_estop_count == my_vehicle.actuator_estop.status_loop_counter)
	    {
	      execute_adrive_pause();
	      my_vehicle.actuator_estop.status = 0;
	      //	  cout << "going to pause" << endl;
	    }
	  else
	    {
	      last_estop_count = my_vehicle.actuator_estop.status_loop_counter;
	      //cout << "passed ESTOP counter test" << endl;
	    }
	}
      if ( my_vehicle.actuator_obdii.status_enabled == 1)
	{
	  // Check for obdii validity
	  if ( my_vehicle.actuator_obdii.EngineCoolantTemp == -40)
	    my_vehicle.actuator_obdii.status = 0;
	  else 
	    my_vehicle.actuator_obdii.status = 1;
	}
      /* Critical things to do immediately
       * 
       ** Check for estop not responding
       ** Check for obdii validity
       */
      
      /* Future imlimentations 
       * Check all actuators for tracking errors
       * Check all actuators for responsiveness/ count incrementation
       * In case of failure pause and attempt restart
       * 
       * Make sure engine is on
       */
      
      
      
      usleep(SUPERVISORY_SLEEP_LENGTH);
    }
  return NULL;
}



int execute_adrive_pause()
{
  my_vehicle.actuator_estop.astop = PAUSE;
  my_vehicle.actuator[ACTUATOR_GAS].command = 0;
  my_vehicle.actuator[ACTUATOR_BRAKE].command = .7;
  //  cout << "execute_adrive_pause done" <<endl;
  return 1;  
}
