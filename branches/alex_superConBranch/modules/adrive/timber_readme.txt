The timber logs are tab delimited files the fields are as follows these are basically taked from ActuatorState.hh



  Timestamp  microseconds from epoc

  // These are all in the [0,1] range
  //Steer
  int m_steerstatus; // 0 = off, 1 = on 
  double m_steerpos; // -1 to 1, left to right
  double m_steercmd;  //dito

  //Gas
  int m_gasstatus; // 0 = off, 1 = on 
  double m_gaspos;
  double m_gascmd;  

  //Brake
  int m_brakestatus; // 0 = off, 1 = on 
  double m_brakepos; // 0 to 1
  double m_brakecmd;  // 0 to 1
  double m_brakepressure; // 0 to 1

  //Estop
  int m_estopstatus; // 0 = off, 1 = on 
  double m_estoppos; // 0 to 1
  
  //Transmission
  int m_transstatus; // 0 = off, 1 = on 
  double m_transcmd; // 0 to 1
  double m_transpos; // 0 to 1

  //OBDII
  int m_obdiistatus; // 0 = off, 1 = on 
  /*! The RPM of the engine */
  int m_engineRPM;
  /*! The time since the engine started in seconds */
  int m_TimeSinceEngineStart;
  /*! Vehicle speed m/s */
  int m_VehicleWheelSpeed;
  /*! The temperature of the engine coolant. */
  int m_EngineCoolantTemp;
  /*! The engine torque in foot pounds */
  int m_EngineTorque;
  /*! The time the glow plug has been on??? in seconds */
  int m_GlowPlugLampTime;
  /*! The position of the accelerator petal UNITS?? */
  int m_ThrottlePosition;
  /*! The current gear ratio */
  float m_CurrentGearRatio;

