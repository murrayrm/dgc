#ifdef MACOSX
/* Install libgnugetopt for getopt_long_only support in Mac OS X */
#include <gnugetopt/getopt.h>
#else
#include <getopt.h>
#endif

#include "parker_steer.h"
#include "adrive.h"
#include "actuators.h"
#include "brake.h"
#include "throttle.h"
#include "askynet_monitor.hh"


void printAdriveHelp(void);
enum {
  OPT_NAME,
  OPT_HELP,
  OPT_VERBOSE,  
  NUM_OPTS
};



extern int shutdown_flag;
extern int run_sparrow_thread;
extern int run_skynet_thread;
extern int run_logging_thread;
extern pthread_cond_t steer_calibrated_flag;
extern int run_obdii_thread;
extern int run_supervisory_thread;
extern int simulation_flag;
extern char simulator_IP[15];

/*! Adrive is a multi threaded program.  For each actuator there will be 
 * 2 threads running one to periodically poll for status, and a second 
 * interupt based thread executing commands. In addition sparrow is run 
 * in its own thread.  Also logging is another thread.  And the skynet 
 * interface is another thread.   
 *
 * The process is first to initialize the vehicle struct and mutexes.
 * Second if sparrow is enabled it must be started first for it waits for 
 * the steering to initialize, if steering is enabled, so that the calibration
 * routine can happen while the user can still see the screen.  
 * After that each enabled actuator thread is created. 
 * Adrive is then done, it waits for all the threads to end. ANd has the 
 * to add additional shutdown functionality.  But nothing is implemented. */
int main(int argc, char** argv)
{
  int c;

  /* Look for the commandline arguments
   *
   * The only one at this point is the help/usage
   */


  static struct option long_options[] = {
    //Options that don't require arguments:
    {"help",      no_argument, 0, OPT_HELP},
    //Options that require arguments
    //    {"name", required_argument, 0, OPT_NAME}, 
    //Options that have optional arguments
    //{"verboselevel", optional_argument, 0, OPT_VERBOSE},
    {0,0,0,0}
  };
  
  while (1) {
    int option_index = 0;
    
    c = getopt_long_only(argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
      /*    case OPT_VERBOSE:
      if(optarg != NULL) {
	optVerbose=atoi(optarg);
      } else {
	optVerbose=1;
      }
      break; */
    case '?':
    case OPT_HELP:
      printAdriveHelp();
      exit(1);
      break;
      /*    case OPT_NAME:
      optName = 1;
      sprintf(logFilenamePrefix, "%s", optarg);
      break;*/
    }
  }
  


  //Start running adrive.  
  int i;
  /* Run statup functions.
   * This initializes the vehicle struct 
   */
  init_vehicle_struct( my_vehicle );
  /* Read in the settings from adrive.config */
  read_config_file( my_vehicle );

  /*Start up the sparrow thread first so that it can wait for 
   * steering if necessary. 
   */
  pthread_t sparrow_handle;
  if( run_sparrow_thread )  
    pthread_create( &sparrow_handle, NULL, &sparrow_main, &my_vehicle );
  else 
    cout << "NO SPARROW DISPLAY" << endl;


  /* Start threads for actuators if we are using them
   * 
   * This iterates through each of the actuators, checking whether their
   * threads are enabled and starts the threads if they are enabled.
   */ 
  for(i=0; i<NUM_ACTUATORS; i++) {
    actuator_t * act = &(my_vehicle.actuator[i]);
    // Check and start the actuator status threads
    if( act->status_enabled ) 
      pthread_create(& act->pthread_status_handle, NULL, &actuator_status_thread_function, act );
    // Check and start the actuator command threads
    if( act->command_enabled ) 
      pthread_create(& act->pthread_command_handle, NULL, &actuator_command_thread_function, act );
  }

  /* Start the skynet thread 
  * 
  * This thread handles all of the skynet communications.
  * It listens for incoming commands as well as creating an additional 
  * thread to broadcast the actuator_state data to fill the CStateClient 
  * struct for other modules.  
  */
  pthread_t skynet_handle;
	struct skynet_main_args args;
  if( run_skynet_thread )
	{
		args.pVehicle = &my_vehicle;
		args.pShutdown_flag = &shutdown_flag;
		args.pRun_skynet_thread = &run_skynet_thread;

    pthread_create( &skynet_handle, NULL, &skynet_main, &args );
	}





  /* Start the OBDII thread
   * 
   * This thread moniters the vehicle through the
   * OBDII interface board. */

  if (my_vehicle.actuator_obdii.status_enabled)
    {
      pthread_create(& my_vehicle.actuator_obdii.pthread_status_handle, NULL, &actuator_obdii_status_thread_function, &my_vehicle.actuator_obdii );
    }


  /* Start the ESTOP thread
   * 
   * This thread moniters the vehicle through the
   * ESTOP interface board. */

  if (my_vehicle.actuator_estop.status_enabled)
    {
      pthread_create(& my_vehicle.actuator_estop.pthread_status_handle, NULL, &actuator_estop_status_thread_function, &my_vehicle.actuator_estop );
    }





  /* Start the logging thread
   * 
   * This thread simply logs data to file
   */


  pthread_t logging_handle;  
  if (run_logging_thread)
    pthread_create( &logging_handle, NULL, &logging_main, 0);

  /* Start the supervisory thread
   * 
   * This thread makes sure all the actuators are working properly
   */


  pthread_t supervisory_handle;  
  if (run_supervisory_thread)
    pthread_create( &supervisory_handle, NULL, &supervisory_thread, 0);


  /* Everything is done by the threads.  
   * At this point wait for all the threads to terminate.  
   * This will never happen currently unless the shutdown flag is set.
   * Currently however there are no provisions to do so.  
   */

  // NOW JOIN THREADS IN ETERNAL HAPPINESS  
  for(i=0; i<NUM_ACTUATORS; i++) {
    actuator_t * act = &(my_vehicle.actuator[i]);
    
    if( act->status_enabled ) 
      pthread_join( act->pthread_status_handle, NULL);
    if( act->command_enabled ) 
      pthread_join( act->pthread_command_handle, NULL);
  }
  if( run_sparrow_thread ) {
    pthread_join( sparrow_handle, NULL);
  }
  if( run_skynet_thread )  
    pthread_join( skynet_handle, NULL);

  if (my_vehicle.actuator_estop.status_enabled)
    {  
      estop_t * act = &(my_vehicle.actuator_estop);
      
      pthread_join( act->pthread_status_handle, NULL);
    }


  if (my_vehicle.actuator_obdii.status_enabled)
    {  
      obdii_t * act = &(my_vehicle.actuator_obdii);
      
      pthread_join( act->pthread_status_handle, NULL);
    }

  if (run_supervisory_thread)
    pthread_join(supervisory_handle, NULL);
  
  if (run_logging_thread)
    pthread_join(logging_handle, NULL);


  /* If code is necessary to shutdown adrive it would go here */
  // TIME TO SHUT DOWN 

  // .. stuff goes here maybe
  
  return 0;
} /* End of main */


/* A simple function to print out the help message */ 

void printAdriveHelp() {
  printf("Usage: adrive\n");
  printf("Runs the DGC adrive software\n\n");

  printf("This is the driving software developed to drive alice\n");
  printf("  --help              Prints this help message\n\n");
  printf("Adrive is controlled by the file adrive.config found\n");
  printf("locally installed with adrive.  In $(DGC)/modules/adrive\n");
  printf("can be found ADRIVE_MAN_PAGE which documents how to run adrive\n");
  printf("as well as README.TXT which explains adrive.config \n");
  printf("NOTE: adrive.config is designed with double negatives to allow \n");
  printf("it to run without the config file.\n");

}
