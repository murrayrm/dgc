/*  This is the header file for actuators.c.  
 * actuators.c is the threading interface for the steering code.  
 */

#ifndef _ACTUATOR_H
#define _ACTUATOR_H

#include <pthread.h>
#include <stdio.h>
#include <sstream>
#include <iostream>

#include "adrive.h"
#include "brake.h"
#include "parker_steer.h"
#include "throttle.h"
#include "estop.h"
#include "OBDIIDriver.hpp"
#include "AliceConstants.h"
#include "GlobalConstants.h"

#define DARPA_PAUSE_TIMEOUT 5000000


/*! The standadized function call to execute trans command*/
int  execute_trans_command( double command );
/*! The standadized function call to execute trans status*/ 
int  execute_trans_status();
/*! The standadized function call to execute trans initialization*/
void execute_trans_init();

/*! The standadized function call to execute estop command*/
int execute_estop_command( double command ); 
/*! The standadized function call to execute estop status*/
int execute_estop_status();
/*! The standadized function call to execute estop initialization*/
void execute_estop_init();

/*! The standadized function call to execute gas command*/
int  execute_gas_command( double command );
/*! The standadized function call to execute gas status*/ 
int  execute_gas_status();
/*! The standadized function call to execute gas initialization*/
void execute_gas_init();

/*! The standadized function call to execute steer command*/
int  execute_steer_command( double command );
/*! The standadized function call to execute steer command*/
int  execute_steer_velocity( double command );
/*! The standadized function call to execute steer command*/
int  execute_steer_acceleration( double command );
/*! The standadized function call to execute steer status*/
int  execute_steer_status();
/*! The standadized function call to execute steer initialization*/
void execute_steer_init();

/*! The standadized function call to execute brake command*/
int  execute_brake_command( double command );
/*! The standadized function call to execute brake status*/
int  execute_brake_status();
/*! The standadized function call to execute brake initialization*/
void execute_brake_init();

/*! The standadized function call to execute obdii command*/
int  execute_obdii_command(double);
/*! The standadized function call to execute obdii status*/
int  execute_obdii_status();
/*! The standadized function call to execute obdii initialization*/
void execute_obdii_init();


#endif //_ACTUATOR_H
