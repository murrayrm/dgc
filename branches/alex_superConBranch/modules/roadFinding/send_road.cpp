#include<stdlib.h>
#include"RFCommunications.hh"

#include<math.h>
#include<iostream>
using namespace std;

int main()
{
  int sn_key = 0;
  char *RFSkyNetKey = getenv("SKYNET_KEY");
  if (RFSkyNetKey == NULL) {
    cout<<"SKYNET_KEY environment variable is not set!!"<<endl;
    exit(1);
  }
  else
    sn_key = atoi(RFSkyNetKey);
  cout<<"sn key = "<<sn_key<<endl;

  RFCommunications RFComm(sn_key, false, true);
  cout<<"done constructing RFComm"<<endl;

  double x[3], y[3], w[3];

  x[0] = 0;  y[0] = 0; w[0]=15;

  for(int i=0; i<10000; ++i) {
  //for(int i=0; i<1; ++i) {
    for(double d =0.0; d<=2*3.1415926535; d+=.05) {
      //for(double d =4.2; d<=4.3; d+=.05) {
      double l1 = 30 + 10 * sin(sqrt(d)/5 * (i+1));
      double l2 = 30 + 10 * sin(sqrt(d) * (i+1));
      x[1] = l1*cos(d);  y[1] = l1*sin(d); w[1]=10+5*sin(d*d+i);
      
      x[2] = x[1] + l2*cos( d + .2*sin(d*d) );
      y[2] = y[1] + l2*sin( d + .2*sin(d*d) );
      w[2]=10+5*cos(d*d*d+i*d);

      //cerr<<"Road: "<<x[0]<<", "<<y[0]<<"("<<w[0]<<") - "<<x[1]<<", "<<y[1]<<"("<<w[1]<<") - "<<x[2]<<", "<<y[2]<<"("<<w[2]<<")"<<endl;

      RFComm.RFSendRoad(x,y,w, 3); //sizeof(x)/sizeof(double));
      usleep(100000);
    }
  }
  
  return 0;
}
