#ifndef SMART_COUNTER_HH
#define SMART_COUNTER_HH

#include"DGCutils"

//Smart counter, counting loops and frequency
class smart_counter
{
 public:
  smart_counter() : cnt(0), freq(0.0), start_time(0), start_cnt(0) {}
  ~smart_counter() {}

  smart_counter &operator++() {   //Pre cond
    ++cnt;
    ++start_cnt;
    unsigned long long t;
    DGCgettime(t);
    if(t-start_time > 1000000) {   //More than one second
      freq = start_cnt / (double)(t-start_time) * 1000000;
      start_cnt=0;
      start_time=t;
    }
    return *this;
  }
  smart_counter &operator++(int) {  //Post cond
    return operator++();
  }

 public:   //Data
  int cnt;
  double freq;

 private:
  unsigned long long start_time;   //Start time for counter below
  int start_cnt;
};


#endif //SMART_COUNTER_HH
