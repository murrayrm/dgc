//----------------------------------------------------------------------------
//  Road follower module
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//----------------------------------------------------------------------------

#include "decs.h"

//----------------------------------------------------------------------------

UD_CameraCalibration *camcal          = NULL;
UD_ImageSource *imsrc                 = NULL;
UD_LadarCalibration *ladcal           = NULL;
UD_LadarSourceArray *ladarray         = NULL;
UD_DominantOrientations *dom          = NULL;
UD_VanishingPoint *vp                 = NULL;
UD_RoadFollower *rf                   = NULL;
UD_OnOff *oo                          = NULL;

UD_Reporter *reporter                 = NULL;
UD_Logger *live_logger                = NULL;

#ifdef SKYNET

#include "RFCommunications.hh"
#include "sparrowhawk.hh"
#include "vddroad.h"

#include <iostream>
#include <iomanip>

using namespace std;

RFCommunications *RFComm;

#endif

//----------------------------------------------------------------------------

int do_computations                   = TRUE;    // run algorithm? (vs. just displaying input)
int do_aerial                         = FALSE;   // show aerial display?
int do_alice                          = FALSE;   // are we connected to the vehicle?
int input_paused                      = FALSE;   // are we "frozen" on the current image?
int main_window_id;

//----------------------------------------------------------------------------

void display()
{
  // raw source

  if (!do_computations)
    glDrawPixels(imsrc->im->width, imsrc->im->height, GL_RGB, GL_UNSIGNED_BYTE, imsrc->im->imageData);

  // current image, tracker state, etc.

  else 
    rf->draw(imsrc);  

  // ladar projection
  
//   if (ladsrc) 
//     ladsrc->draw(camcal);

  glutSwapBuffers();
}

//----------------------------------------------------------------------------

void keyboard(unsigned char key, int x, int y)
{
  // pause/unpause current image frame (just a freeze frame if live input--does not buffer)

  if (key == 'p')
    input_paused = !input_paused;
}

//----------------------------------------------------------------------------

void mouse(int button, int state, int x, int y)
{
  if (do_computations) {

    // cycle between raw image view, dominant orientation view, and vote function view
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
      rf->cycle_draw_mode_image();
    
    // cycle between overlaying nothing, tracker particles, or support rays
    
    else if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP) 
      rf->cycle_draw_mode_overlay();
  }

  // save whatever the display is currently showing to file

  if (button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) {
    glReadPixels(0, 0, imsrc->im->width, imsrc->im->height, GL_RGB, GL_UNSIGNED_BYTE, imsrc->im->imageData);
    sprintf(imsrc->frame_filename, "../road_data/saved_%05i.png", imsrc->relative_frame);
    cvConvertImage(imsrc->im, imsrc->im, CV_CVTIMG_SWAP_RB | CV_CVTIMG_FLIP);  // RGB->BGR	
    cvSaveImage(imsrc->frame_filename, imsrc->im);
  }
}

//----------------------------------------------------------------------------

void process_image()
{
  // calculate dominant orientations

  dom->compute(imsrc->im);
  
  // compute vote function 

  vp->compute(dom->gabor_max_response_index);
  
  // track vanishing point, centerline, make on/off road decision, etc.

  rf->run();
  
  // talk to vehicle controller
  
  if (do_alice) {
    //    rf->send_output_to_controller(camcal);
#ifdef SKYNET
    // printf("%f degs., %f m\n", RAD2DEG(rf->direction_error), rf->laser_offset);
    RFComm->RFSendRoad(rf->lookahead_z, rf->lookahead_x, rf->lookahead_radius, rf->num_lookahead_pts);
 #endif
  }
}

//----------------------------------------------------------------------------

void compute_display_loop()
{
  if (imsrc) {

    glutSetWindow(main_window_id);
    
    // start timing (of _iteration_, of which there may be multiple per image frame)

    reporter->start_timer(imsrc);

    // perform image processing for current image (frame 0 already "captured" by constructor)
    
    if (do_computations)
      process_image();

    // draw current image and algorithm output in camera window

    glutPostRedisplay();
  }

  // draw in overhead and aerial view windows 

  if (!ladarray->no_ladar()) {
    glutSetWindow(UD_Overhead::overhead->window_id);
    glutPostRedisplay();
  }

  if (do_aerial) {
    glutSetWindow(UD_Aerial::aerial->window_id);
    glutPostRedisplay();
  }

  // ready for new sensor data?

  if (!input_paused && (!do_computations || (rf && rf->iterate()))) {

    // handle logging

    if (live_logger)
      live_logger->write(imsrc, rf);                                      

    // capture 

    if (imsrc)
      imsrc->capture();    
//       else
//         UD_sleep(0.1);  // slow things down

    if (!ladarray->no_ladar()) 
      ladarray->capture(imsrc);     // syncs to image (ok with no imsrc)

    // something different should be done ifdef SKYNET

    if (do_aerial) {
#ifdef SKYNET
      UD_Aerial::aerial->statesrc->read();
#else
      if (imsrc && ladarray->no_ladar())
	UD_Aerial::aerial->statesrc->sync(imsrc->timestamp);  // sync to image if no ladar did it already
      else if (!imsrc)  
	UD_Aerial::aerial->statesrc->read();   // aerial must read state source itself
#endif
    }
  }

  // end timing

  if (imsrc)
    reporter->end_timer(imsrc);
}

//----------------------------------------------------------------------------

/// register callbacks and create one display window
/// imwidth = 0 or imheight = 0 indicates that there is no image source

void initialize_display(int imwidth, int imheight, int argc, char **argv)
{
  int x, y;
  
  x = 100;
  y = 100;

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  if (!ladarray->no_ladar()) {
    UD_Overhead::overhead->ladarray = ladarray;
    UD_Overhead::overhead->initialize_display(x + imwidth + 10, y);
  }
  if (do_aerial) 
    UD_Aerial::aerial->initialize_display(x + imwidth + UD_Overhead::overhead->width + 20, y);

  glutIdleFunc(compute_display_loop);

  if (!imwidth || !imheight)
    return;

  glutInitWindowSize(imwidth, imheight); 
  glutInitWindowPosition(x, y);
  main_window_id = glutCreateWindow("road");

  //  glutIdleFunc(compute_display_loop);
  
  glutDisplayFunc(display); 
  glutMouseFunc(mouse); 
  glutKeyboardFunc(keyboard);

  if (!do_computations) {
    glPixelZoom(1, -1);
    glRasterPos2i(-1, 1);
  }

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, imwidth-1, 0, imheight-1);
}


//----------------------------------------------------------------------------

void init_per_run(int argc, char **argv)
{
  if(argc>=2 && (strcasecmp(argv[1], "--help")==0 || strcasecmp(argv[1], "-help")==0)) {
    //Print help
    printf("USAGE: %s [options]\n", argv[0]);
    printf("General flags\n");
    printf("  -sn_key <skynet_key>                      Sets the skynet key\n");
    printf("  -image_list <file> <start> <end> <width>  Load list of images from file\n");
    printf("                                            starting with image start and\n");
    printf("                                            running till image end.\n");
    printf("  -image_avi <file> <start> <end> <width>   Load video from file starting with\n");
    printf("                                            frame start and running till frame\n");
    printf("                                            <end>.\n");
    printf("  -image_live <cam_nr> <mode> <nframes> <w> Run from camera feed where mode is\n");
    printf("                                            one of:\n");
    printf("                                              MODE_640x480_MONO\n");
    printf("                                              MODE_640x480_YUV422\n");
    printf("                                              MODE_320x240_YUV422\n");
    printf("  -ladar_scans <file> <stamp> <diff>        Loads logged ladar scans from <file>\n");
    printf("                                            and loads a time stamp <stamp> file\n");
    printf("                                            for the images, finally sets maximal\n");
    printf("                                            allowed time discrepancy <diff> in\n");
    printf("                                            seconds between ladar and image\n");
    printf("  -ladar_skynet <diff>                      Runs ladars from alice, sets the\n");
    printf("                                            maximal allowed time discrepancy\n");
    printf("                                            <diff> in seconds between ladar\n");
    printf("                                            and image\n");
    printf("  -nocompute                                \n");
    printf("  -log <file> <interval>                    Start image loggin to <file>,\n");
    printf("                                            logging every log_interval image\n");
    printf("  -alice                                    Run with communication with alice\n");
    printf("  -sw_cabladar                              Use calibration for cab mounted\n");
    printf("                                            ladar from Stoddard Wells\n");
    printf("  -sw_bumperladar                           Use calibration for bumper mounted\n");
    printf("                                            ladar from Stoddard Wells\n");
    printf("  -sw_rightcam                              Use calibration for right-hand\n");
    printf("                                            stereo camera from Stoddard Wells\n");
    printf("  -sw_leftcam                               Use calibration for left-hand\n");
    printf("                                            stereo camera from Stoddard Wells\n");
    /*
      printf("Printing and logging flags\n");
      printf("  -time <interval>                          Do timing\n");
      printf("  -printnum                                 Print image number\n");
      printf("Vanishing point flags\n");
      printf("  -sky <fraction>                           Set the fraction of the image that\n");
      printf("                                            should be considered sky\n");
      printf("  -computemax                               \n");
      printf("OnOff flags (?)\n");
      printf("  -klthresh <threshold>                     \n");
      printf("  -klthreshold <threshold>                  Same as above\n");
      printf("  -klmajority                               \n");
      printf("  -printonoff                               \n");
      printf("  -noonoff                                  \n");
      printf("RoadFollowing flags\n");
      printf("  -centerline                               Show centerline\n");
      printf("  -showsupp                                Show support\n");
      printf("  -thresh <threshold>                       Support angle difference threshold\n");
      printf("  -alpha <value>                            Support alpha\n");
      printf("Tracker flags\n");
      printf("  -deltax <val>                             \n");
      printf("  -deltay <val>                             \n");
      printf("  -doreset                                  \n");
      printf("  -iterations <val>                         Set number of iterations per image\n");
    */

    return;
  }

  //-------------------------------------------------
  // reporting (command-line flags can modify)
  //-------------------------------------------------

  reporter = new UD_Reporter();
  reporter->process_command_line_flags(argc, argv);   

  //-------------------------------------------------
  // ladar array stub
  //-------------------------------------------------

  ladarray = new UD_LadarSourceArray();

  //-------------------------------------------------
  // command-line flags (imsrc initialized after this)
  //-------------------------------------------------

  process_general_command_line_flags(argc, argv);

  //-------------------------------------------------
  // alice
  //-------------------------------------------------

#ifdef SKYNET
  printf("skynet!!\n");
  if (do_alice) {
    int sn_key = 0;
    char *RFSkyNetKey = getenv("SKYNET_KEY");
    if (RFSkyNetKey == NULL) {
      printf("SKYNET_KEY environment variable is not set!!\n");
      exit(1);
    }
    else
      sn_key = atoi(RFSkyNetKey);
    printf("sn key = %i\n", sn_key);
    RFComm = new RFCommunications(sn_key);
    printf("done constructing RFComm\n");
  }
#endif

  //-------------------------------------------------
  // random number generation
  //-------------------------------------------------

  initialize_UD_Random();

#ifdef SKYNET
  SparrowHawk().rebind("YawRad", &camcal->yaw_angle);
  static double dummy1;
  SparrowHawk().rebind("YawDeg", &dummy1);
  SparrowHawk().set_setter("YawDeg", camcal, &UD_CameraCalibration::set_yaw_deg);
  SparrowHawk().set_getter("YawDeg", camcal, &UD_CameraCalibration::get_yaw_deg);
#endif

  //-------------------------------------------------
  // short circuit rest of init sequence?
  //-------------------------------------------------

  if (!imsrc || !do_computations) {
    if (!imsrc)
      initialize_display(0, 0, argc, argv);
    else
      initialize_display(imsrc->im->width, imsrc->im->height, argc, argv);

#ifdef SKYNET
    SparrowHawk().run();
#endif

    glutMainLoop();
    return;
  }

  //-------------------------------------------------
  // dominant orientation estimation
  //-------------------------------------------------

  dom = new UD_DominantOrientations(imsrc->im->width, imsrc->im->height, 36, 0.0, 180.0, 4.0);
  dom->process_command_line_flags(argc, argv);

  //-------------------------------------------------
  // vanishing point finding
  //-------------------------------------------------

  vp = new UD_VanishingPoint(dom, 0.2, 0.0);
  vp->process_command_line_flags(argc, argv);
  if (!ladarray->no_ladar()) 
    ladarray->set_win_deltas((float) -vp->candidate_lateral_margin, (float) -vp->candidate_top_margin); 
 
  //-------------------------------------------------
  // on vs. off road confidence
  //-------------------------------------------------

  if (imsrc->im->height == 60)
    //    oo = new UD_OnOff(imsrc, -0.05, 200, 0.5);
    oo = new UD_OnOff(imsrc, -0.1, 100, 1.0, 0.5);
  else
    oo = new UD_OnOff(imsrc, 0.1, 300, 1.0, 0.667);
  oo->process_command_line_flags(argc, argv);

  //-------------------------------------------------
  // tracking vanishing point
  //-------------------------------------------------

  rf = new UD_RoadFollower(vp, oo, ladarray);  
  rf->process_command_line_flags(argc, argv);
  rf->cal = camcal;

  //-------------------------------------------------
  // overhead and aerial views (constructed in 
  // respective source files)
  //-------------------------------------------------

  UD_Overhead::overhead->rf = rf;

  //-------------------------------------------------
  // start OpenGL GLUT
  //-------------------------------------------------

  initialize_display(vp->cw, vp->ch, argc, argv);

#ifdef SKYNET
    SparrowHawk().run();
#endif

  glutMainLoop();
}

//----------------------------------------------------------------------------

void process_general_command_line_flags(int argc, char **argv)
{
  int i, mode;
  UD_Aerial *tempaerial;

  if (argc == 1) {
    printf("running with race parameters\n");
#ifndef SKYNET
    printf("skynet must be defined\n");
    exit(1);
#endif
    do_alice = TRUE;
    //left camera
    //    camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, -0.248, -2.089, -0.464, -0.1, 0.0);
    
    //center camera
    camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, 0.0, -2.089, -0.464, 0.0, DEG2RAD(-2));
    
    imsrc = new UD_ImageSource_Live(camcal, 0, MODE_640x480_MONO, -1, 80);
    ladcal = new UD_LadarCalibration(500, 50.0, -0.5, 0.0, -0.648, 0.965, 0.0);
    ladarray->add_ladar(new UD_LadarSource_Skynet(ladcal, 0.5));
    //    ladcal = new UD_LadarCalibration(500, 50.0, -0.5, 0.0, -2.413, -0.477, -0.09032);
    //    ladcal = new UD_LadarCalibration(500, 50.0, -0.5, 0.0, -2.413, -0.477, -0.09032);
    // ladarray->add_ladar(new UD_LadarSource_Skynet(ladcal, 0.5));
    live_logger = new UD_Logger("./", 1, ladarray);

  }

  for (i = 1; i < argc; i++) {

    // path to image file listing, start frame (by place in file list), end frame, target image width (180 recommended)

    if (!strcmp(argv[i],                             "-image_list")) 
      imsrc = new UD_ImageSource_Image(camcal, argv[i + 1], atoi(argv[i + 2]), atoi(argv[i + 3]), atoi(argv[i + 4]));

    // path to AVI file, start frame, end frame (-1 = last frame of movie), target image width (180 recommended)

    else if (!strcmp(argv[i],                        "-image_avi"))
      imsrc = new UD_ImageSource_AVI(camcal, argv[i + 1], atoi(argv[i + 2]), atoi(argv[i + 3]), atoi(argv[i + 4]));

      // camera number, mode, end frame (-1 = NONE), target image width (180 recommended)

    else if (!strcmp(argv[i],                        "-image_live")) {
      if (!strcmp(argv[i + 2],      "MODE_640x480_MONO"))
	mode = MODE_640x480_MONO;
      else if (!strcmp(argv[i + 2], "MODE_640x480_YUV422"))
	mode = MODE_640x480_YUV422;
      else if (!strcmp(argv[i + 2], "MODE_320x240_YUV422"))
	mode = MODE_320x240_YUV422;
      else {
	printf("unsupported firewire camera mode\n");
	exit(1);
      }

      imsrc = new UD_ImageSource_Live(camcal, atoi(argv[i + 1]), mode, atoi(argv[i + 3]), atoi(argv[i + 4]));
    }

    // path to scans, path to image timestamp logs, maximum timestamp difference

    else if (!strcmp(argv[i],                        "-ladar_scans")) {  // use calibration for roof ladar at Stoddard Wells

      UD_LadarSource_Scans *ladsrc = new UD_LadarSource_Scans(argv[i + 1], ladcal, atof(argv[i + 2]));
      ladarray->add_ladar(ladsrc);
    }

    else if (!strcmp(argv[i],                        "-image_timestamps")) {
      if (imsrc)
	imsrc->setup_timestamp(argv[i + 1]);
    }

#ifdef SKYNET

    // bumper or roof needs to be specified here

    else if (!strcmp(argv[i],                        "-ladar_skynet")) 
      ladarray->add_ladar(new UD_LadarSource_Skynet(ladcal, atof(argv[i + 1])));

#endif
    else if (!strcmp(argv[i],                        "-nocompute")) 
      do_computations = FALSE;

    // path, save image interval

    else if (!strcmp(argv[i],                        "-log")) 
      live_logger = new UD_Logger(argv[i + 1], atoi(argv[i + 2]), ladarray);

    // ladar's mount point, FOV

    else if (!strcmp(argv[i],                        "-sw_roofladar"))   // use calibration for cab-mounted ladar at Stoddard Wells 
      //      ladcal = new UD_LadarCalibration(201, 50.0, -0.5, 0.0, -2.387, -0.477, -0.2);
      ladcal = new UD_LadarCalibration(181, 90.0, -1.0, 0.0, -2.387, -0.477, -0.2);

    else if (!strcmp(argv[i],                        "-swjune_roofladar"))   // use calibration for roof-mounted ladar at Stoddard Wells 
      ladcal = new UD_LadarCalibration(201, 50.0, -0.5, 0.0, -2.413, -0.477, -0.09032);

    else if (!strcmp(argv[i],                        "-sw_bumperladar"))   // use calibration for bumper-mounted ladar at Stoddard Wells 
      ladcal = new UD_LadarCalibration(201, 50.0, -0.5, 0.0, -0.648, 0.965, 0.0);

    else if (!strcmp(argv[i],                        "-swaug_bumperladar"))   // use calibration for bumper-mounted ladar at Stoddard Wells 
      ladcal = new UD_LadarCalibration(1024, 90.0, -1.0, 0.0, -0.648, 0.965, 0.0);

    else if (!strcmp(argv[i],                        "-swaug21_bumperladar"))   // use calibration for bumper-mounted ladar at Stoddard Wells 
      ladcal = new UD_LadarCalibration(181, 90.0, -1.0, 0.0, -0.648, 0.965, 0.0);

    // see here for information (e.g., FOV) on Caltech's stereo cameras: http://team.caltech.edu/members/

    else if (!strcmp(argv[i],                        "-sw_rightcam"))   // use calibration for right-hand stereo camera at Stoddard Wells 
      camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, 0.226, -2.088, -0.468, -0.1, 0.0);  // k1 = -0.266, k2 = 0.0877
    else if (!strcmp(argv[i],                        "-sw_leftcam"))   // use calibration for left-hand stereo camera at Stoddard Wells 
      camcal = new UD_CameraCalibration(640, 480, DEG2RAD(94.3), 380.94, 379.83, 344.58, 243.72, -0.248, -2.089, -0.464, -0.1, DEG2RAD(-2));

    // path to state source (RDDF or log), tile directory, and run mode ("fetch", "nofetch")
    
    else if (!strcmp(argv[i],                        "-alice")) 
      do_alice = TRUE;

    else if (!strcmp(argv[i],                        "-aerial")) {
      do_aerial = TRUE;
      UD_StateSource *ss = NULL;
      if (!ladarray->no_ladar()) 
	ss = ladarray->ladsrc[0]->statesrc;
      tempaerial = new UD_Aerial(argv[i+1], argv[i+2], argv[i+3], ss);
      UD_Aerial::aerial->finish_construction(argv[i+1], argv[i+2], argv[i+3], ss);
    }
  }

  if (!imsrc) 
    do_computations = FALSE;
}

//----------------------------------------------------------------------------

int main(int argc, char **argv)
{
#ifdef SKYNET
  //Add all the sparrow display pages
  SparrowHawk().add_page(vddroad, "Main");
#endif

  init_per_run(argc, argv);

  return 1;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
