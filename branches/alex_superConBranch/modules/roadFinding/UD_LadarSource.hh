//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_LadarSource
//----------------------------------------------------------------------------

#ifndef UD_LADARSOURCE_DECS

//----------------------------------------------------------------------------

#define UD_LADARSOURCE_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <GL/glut.h>

#include "UD_ImageSource.hh"
#include "UD_StateSource.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef NONE
#define NONE    -1         // convention when end frame number or iteration must be supplied
#endif


#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#ifndef PI
#define PI    3.14159
#endif

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

#define NO_RETURN -1.0

#define MAXSTRLEN  256

#define SCANS_LADAR_LOG   1
#define RAW_LADAR_LOG     2

#define DANGER_HEIGHT 0.5   // in meters -- goes for negative as well as positive obstacles

#define MAX_RAYS          500

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_LadarCalibration
{
  
 public:

  // variables

  int num_rays;                        ///< number of ladar "rays" sent out in each scan
  float first_ray_theta;               ///< angle of first ray (0 = straight ahead, + = to the right) (in degrees)
  float last_ray_theta;                ///< angle of last ray 
  float ray_delta_theta;               ///< angle between adjacent rays (in degrees)

  float *sin_pan_angle;                ///< precomputed from the absolute angle for each ray
  float *cos_pan_angle;                ///< precomputed from the absolute angle for each ray

  float dx;                            ///< + = right displacement of ladar from vehicle origin (in meters)
  float dy;                            ///< + = down displacement
  float dz;                            ///< + = forward displacement

  float max_range;                     ///< highest range value that can be returned (in meters)

  float pitch_angle;                   ///< radians off horizontal (- = below horizontal)
  float sin_pitch_angle;
  float cos_pitch_angle;

  // functions

  UD_LadarCalibration(int, float, float, float, float, float, float);

};

//-------------------------------------------------

class UD_LadarSource
{
  
 public:

  // variables

  int index;
  UD_StateSource *statesrc;
  UD_LadarCalibration *cal;            ///< intrinsic and extrinsic calibration of ladar sensor
  int num_rays;                        ///< duplicate of what's in cal
  float win_dx, win_dy;                ///< translation from image coordinates to window coordinates
  int ladar_type;

  float *range;                        ///< distance to ray hit point (in meters) 
  float *angle;                        ///< angle in radians
  float *vehicle_x;                    ///< vehicle x coordinate of ray hit points
  float *vehicle_y;                    ///< vehicle y coordinate of ray hit points
  float *vehicle_z;                    ///< vehicle z coordinate of ray hit points
  float *image_x, *image_y;            ///< image coordinates (in specified calibrated camera) of ray hit points
  float *baseline_x;                   ///< vehicle x coordinate of ray hit points PROJECTED ON FRONT AXLE LINE
                                       ///< ALONG VANISHING POINT DIRECTION
  int *dangerous;                      ///< is hit point high enough or low enough to worry us?

  // functions

  UD_LadarSource(UD_LadarCalibration *);

  virtual void init_log2file(char *) {}
  virtual void log2file(int) {}

  virtual void capture() = 0;
  virtual void capture(double) = 0;
  virtual void capture(UD_ImageSource *) = 0;
  void draw(UD_CameraCalibration *);

  void ladar2image(int, float, float *, float *, float *, UD_CameraCalibration *);
  void ladar2leftimage(int, float, float *, float *, float *);
  void compute_image_positions(UD_CameraCalibration *);
  double myround(double);
  void baseline_project(float);
  int is_dangerous(int);
  float density_in_gap(float, float, float);
};

//-------------------------------------------------

//#define SKYNET

#ifdef SKYNET

#include "RFCommunications.hh"
extern RFCommunications *RFComm;

#endif

class UD_LadarSource_Skynet : public UD_LadarSource
{
  
 public:

  // variables

  //  UD_StateSource_Skynet *statesrc;
  double *skynet_angle, *skynet_range;
  FILE *ladar_logfp;

  // functions

  UD_LadarSource_Skynet(UD_LadarCalibration *, float);

  void init_log2file(char *);
  void log2file(int);

  void capture();
  void capture(double);
  void capture(UD_ImageSource *);
  //  void get_state(double *, double *, double *);
  void ladar2vehicle(int, float, float *, float *, float *);
};


//-------------------------------------------------

class UD_LadarSource_Scans : public UD_LadarSource
{
  
 public:

  // variables

  FILE *scans_fp;
  //  UD_StateSource_Scans *statesrc;

  int scan_type;                       ///< format indicator for log file (it changed between March & June Stoddard Wells tests)

  // functions

  UD_LadarSource_Scans(char *, UD_LadarCalibration *, float);

  void capture();
  void capture(double);
  void capture(UD_ImageSource *);

  void read_header();

  void process_command_line_flags(int, char **);
  void eat_comments(FILE *);
  void ladar2vehicle(int, float, float *, float *, float *);
};

//-------------------------------------------------

#define MAX_LADARS        16

class UD_LadarSourceArray
{
  
 public:

  // variables

  int num_ladars;
  UD_LadarSource *ladsrc[MAX_LADARS];
  float *centerline_dist;              ///< distance of hitpoint to putative road centerline in vehicle coords.

  // functions

  UD_LadarSourceArray() { num_ladars = 0; centerline_dist = (float *) calloc(MAX_LADARS * MAX_RAYS, sizeof(float)); }
  
  int no_ladar() { return num_ladars == 0; }
  void add_ladar(UD_LadarSource *);
  // { ladsrc[num_ladars] = new_ladsrc; ladsrc[num_ladars]->index = num_ladars++; }
  void set_win_deltas(float, float);
  void capture(UD_ImageSource *);
  void baseline_project(float);
  float density_in_gap(float, float, float);
  double n_hit_radius(float, float, float, int);
};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
