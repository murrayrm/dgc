//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_Logger
//----------------------------------------------------------------------------

#ifndef UD_LOGGER_DECS

//----------------------------------------------------------------------------

#define UD_LOGGER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "UD_ImageSource.hh"
#include "UD_RoadFollower.hh"
#include "UD_Linux_Time.hh"
#include "opencv_utils.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef NONE
#define NONE    -1         // convention when end frame number or iteration must be supplied
#endif

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

//-------------------------------------------------
// class
//-------------------------------------------------

// for reporting frame-rate, current frame, etc. to stdout 

class UD_Reporter
{
  
 public:

  // variables

  //  static double start, finish, difference;
  double start_time, finish_time;
  struct timeval tv;

  int interval;
  int do_timing;
  int print_image_number;

  // functions

  UD_Reporter();

  void start_timer(UD_ImageSource *);
  void end_timer(UD_ImageSource *);

  void process_command_line_flags(int, char **); 

};

//-------------------------------------------------

// for saving images, timestamps, state, etc. to file

class UD_Logger
{
  bool m_log_images;
 public:

  // variables

  FILE *fp;        // timestamps, anything else
  char *command;
  char *path;
  char *timestamp;
  char *image_filename;
  char *filename;
  char *ladar_filename;
  int save_image_interval;

  // functions

  UD_Logger(char *, int, UD_LadarSourceArray *ldarray);

  void write(UD_ImageSource *, UD_RoadFollower *);

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

