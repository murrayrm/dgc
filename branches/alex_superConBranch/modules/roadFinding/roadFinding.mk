UDROAD_PATH = $(DGC)/modules/roadFinding

UDROAD_DEPEND_LIBS = $(LADARSOURCELIB) \
                     $(MODULEHELPERSLIB) \
                     $(TRAJLIB) \
                     $(SKYNETLIB) \
                     $(GGISLIB) \
                     $(FRAMESLIB) \
                     $(SPARROWLIB) \
                     $(SPARROWHAWKLIB)

UDROAD_DEPEND_SOURCES = \
    $(UDROAD_PATH)/Makefile \
    $(UDROAD_PATH)/UD_DominantOrientations.cpp \
    $(UDROAD_PATH)/UD_DominantOrientations.hh \
    $(UDROAD_PATH)/UD_Error.cpp \
    $(UDROAD_PATH)/UD_Error.hh \
    $(UDROAD_PATH)/UD_FirewireCamera.cpp \
    $(UDROAD_PATH)/UD_FirewireCamera.hh \
    $(UDROAD_PATH)/UD_ImageSource.cpp \
    $(UDROAD_PATH)/UD_ImageSource.hh \
    $(UDROAD_PATH)/UD_LadarSource.cpp \
    $(UDROAD_PATH)/UD_LadarSource.hh \
    $(UDROAD_PATH)/UD_Ladar_GapTracker.cpp \
    $(UDROAD_PATH)/UD_Ladar_GapTracker.hh \
    $(UDROAD_PATH)/UD_Linux_Movie_AVI.cpp \
    $(UDROAD_PATH)/UD_Linux_Movie_AVI.hh \
    $(UDROAD_PATH)/UD_Linux_Time.cpp \
    $(UDROAD_PATH)/UD_Linux_Time.hh \
    $(UDROAD_PATH)/UD_Logger.cpp \
    $(UDROAD_PATH)/UD_Logger.hh \
    $(UDROAD_PATH)/UD_OnOff.cpp \
    $(UDROAD_PATH)/UD_OnOff.hh \
    $(UDROAD_PATH)/UD_Overhead.cpp \
    $(UDROAD_PATH)/UD_Overhead.hh \
    $(UDROAD_PATH)/UD_ParticleFilter.cpp \
    $(UDROAD_PATH)/UD_ParticleFilter.hh \
    $(UDROAD_PATH)/UD_Random.cpp \
    $(UDROAD_PATH)/UD_Random.hh \
    $(UDROAD_PATH)/UD_RoadFollower.cpp \
    $(UDROAD_PATH)/UD_RoadFollower.hh \
    $(UDROAD_PATH)/UD_VP_MultiTracker.cpp \
    $(UDROAD_PATH)/UD_VP_MultiTracker.hh \
    $(UDROAD_PATH)/UD_VP_PositionTracker.cpp \
    $(UDROAD_PATH)/UD_VP_PositionTracker.hh \
    $(UDROAD_PATH)/UD_VP_Tracker.cpp \
    $(UDROAD_PATH)/UD_VP_Tracker.hh \
    $(UDROAD_PATH)/UD_VP_VelocityTracker.cpp \
    $(UDROAD_PATH)/UD_VP_VelocityTracker.hh \
    $(UDROAD_PATH)/UD_VanishingPoint.cpp \
    $(UDROAD_PATH)/UD_VanishingPoint.hh \
    $(UDROAD_PATH)/conversions.cpp \
    $(UDROAD_PATH)/conversions.h \
    $(UDROAD_PATH)/decs.h \
    $(UDROAD_PATH)/main.cpp \
    $(UDROAD_PATH)/opencv_utils.hh \

UDROAD_DEPEND = \
	$(UDROAD_DEPEND_LIBS) \
	$(UDROAD_DEPEND_SOURCES)
