//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_ImageSource
//----------------------------------------------------------------------------

#ifndef UD_IMAGESOURCE_DECS

//----------------------------------------------------------------------------

#define UD_IMAGESOURCE_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "opencv_utils.hh"

#include "UD_Linux_Time.hh"
#include "UD_Linux_Movie_AVI.hh"
#include "UD_FirewireCamera.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef NONE
#define NONE    -1         // convention when end frame number or iteration must be supplied
#endif

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#ifndef MAXSTRLEN
#define MAXSTRLEN  256
#endif

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))
#define MAX3(A, B, C)         (((A) > (B)) ? MAX2((A), (C)) : MAX2((B), (C)))
#define MIN3(A, B, C)         (((A) < (B)) ? MIN2((A), (C)) : MIN2((B), (C)))

//-------------------------------------------------
// class
//-------------------------------------------------

// intrinsic and extrinsic variables -- assuming no radial distortion, zero pan

class UD_CameraCalibration
{
  
 public:

  // intrinsic variables

  int w, h;                             ///< image dimensions
  float fx, fy;                         ///< focal length (in pixels)
  float px, py;                         ///< principal point (in pixels)
  float hfov, vfov;                     ///< horizontal, vertical fields of view (in radians)
  float scale_factor;                   ///< above variables describe raw source; need to divide by this for image size we're working at

  // extrinsic variables

  float dx;                             ///< + = right displacement of camera from vehicle origin (in meters)
  float dy;                             ///< + = down displacement
  float dz;                             ///< + = forward displacement
    
  float pitch_angle;                    ///< radians off horizontal (- = below horizontal)
  float sin_pitch_angle, cos_pitch_angle;

  double yaw_angle;                      ///< radians off yaw compared to ahead (+ = pointing to the right)

  // functions

  UD_CameraCalibration(int, int, float, float, float, float, float, float, float, float, float, float);

  void vehicle2camera(float, float, float, float *, float *, float *);
  void camera2image(float, float, float, float *, float *);

  //Sparrow display getter/setter functions
  double get_yaw_deg();
  void set_yaw_deg(double *pData, double val);
};

//-------------------------------------------------

class UD_ImageSource
{
  
 public:

  // variables

  UD_CameraCalibration *cal;            ///< intrinsic and extrinsic camera parameters of raw image source if known

  IplImage **source_im;                 ///< image pyramid with raw captured source at source_im[0], half-size version at source_im[1], etc.
  IplImage **gray_source_im;            ///< grayscale copy of source_im (i.e., IPL_DEPTH_8U, 1, whereas source_im may have 3 channels)
  IplImage *im;                         ///< pointer to smallest image in source_im pyramid.  this is what we image process
  IplImage *gray_im;                    ///< pointer to bottom of gray_source_im
  
  int is_grayscale;                     ///< flag to indicate that raw source type is already IPL_DEPTH_8U, 1 (so no conversion necessary)

  int num_pyramid_levels;               ///< height of image pyramid

  int start_frame, end_frame;           ///< source frames to start and end processing on (defaults = 0 and never/EOF, respectively)

  int absolute_frame;                   ///< the current frame of the image source in an absolute sense 
  int relative_frame;                   ///< how many frames we've worked on so far (i.e., absolute_frame - start_frame)

  char *frame_filename;                 ///< scratch space for string to name current frame when saving to file

  double timestamp;                     ///< precise time in seconds that current frame was captured
  long int timestamp_secs;              ///< left side of decimal in timestamp
  long int timestamp_usecs;             ///< right side of decimal in timestamp
  char *timestamp_filename;             ///< name of file where timestamps are being logged (if applicable)
  FILE *timestamp_fp;                   ///< pointer to file where timestamps are being logged (if applicable)

  // functions

  UD_ImageSource();

  virtual int capture() = 0;
  virtual void setup_timestamp(char *) = 0;
  virtual void get_timestamp() = 0;
  void calculate_num_pyramid_levels(int, int);
  void initialize_pyramid();
  void pyramidize();
  double myround(double);
  int count_commas(char *);
};

//-------------------------------------------------

class UD_ImageSource_Live : public UD_ImageSource
{
  
 public:

  // variables

  int cam_num;                          ///< which camera we are using if several are attached
  UD_FirewireCamera *cam;               ///< variables necessary to capture from firewire cameras

  // functions

  UD_ImageSource_Live(UD_CameraCalibration *, int, int, int, int);

  int capture();
  void setup_timestamp(char *);
  void get_timestamp();

};

//-------------------------------------------------

class UD_ImageSource_AVI : public UD_ImageSource
{
  
 public:

  // variables

  UD_Movie *movie_source;               ///< variables necessary to capture from AVI files

  // functions

  UD_ImageSource_AVI(UD_CameraCalibration *, char *, int, int, int);

  int capture();
  void setup_timestamp(char *);
  void get_timestamp();

};

//-------------------------------------------------

class UD_ImageSource_Image : public UD_ImageSource
{
  
 public:

  // variables

  char *filename;                       ///< name of file listing sequence of images that serves as image source
  FILE *filelist_fp;                    ///< pointer to image sequence file listing

  // functions

  UD_ImageSource_Image(UD_CameraCalibration *, char *, int, int, int);

  int capture();
  void setup_timestamp(char *);
  void get_timestamp();
  int count_lines(FILE *);

  int goto_image(int);
  int read_next_image(IplImage **);

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif
