#include "fusionMapper.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#include <iostream>


using namespace std;

int QUIT=0;
int PAUSE=0;
int RESET=0;
int CHANGE=0;
int REPAINT=0;
int REEVALUATE=0;
int SAVE=0;

int sparrowSNKey;

double sparrowAggrTuningRDDFScaling;
double sparrowAggrTuningMaxSpeed;
int    sparrowAggrTuningZeroGradSpeedAsRDDFPercentage;
double sparrowAggrTuningZeroGradSpeed;
double sparrowAggrTuningUnpassableObsHeight;
int    sparrowAggrTuningUseAreaGradient;

double sparrowAggrTuningAreaGradientScaling;
int    sparrowAggrTuningVeloAlg;
double sparrowAggrTuningVeloTweak1;
double sparrowAggrTuningVeloTweak2;

int sparrowCenterPaintTrackline;
int sparrowCenterAdjustWhich;
double sparrowCenterAdjustmentVal;

int sparrowStatusLadarFeeder;
int sparrowNumMsgsLadarFeeder;
int sparrowMsgSizeLadarFeeder;
double sparrowMsgSizeAvgLadarFeeder;
double sparrowTimeLadarFeeder;
double sparrowTimeAvgLadarFeeder;

/*
int sparrowStatusLadarFeederBumper;
int sparrowNumMsgsLadarFeederBumper;
int sparrowMsgSizeLadarFeederBumper;
double sparrowMsgSizeAvgLadarFeederBumper;
double sparrowTimeLadarFeederBumper;
double sparrowTimeAvgLadarFeederBumper;
*/

double sparrowStdDevWeight;
double sparrowStdDevScaling;

int sparrowStatusStereoFeeder;
int sparrowNumMsgsStereoFeeder;
int sparrowMsgSizeStereoFeeder;
double sparrowMsgSizeAvgStereoFeeder;
double sparrowTimeStereoFeeder;
double sparrowTimeAvgStereoFeeder;

int sparrowStatusGPSFinder;
int sparrowNumMsgsGPSFinder;
int sparrowMsgSizeGPSFinder;
double sparrowMsgSizeAvgGPSFinder;
double sparrowTimeGPSFinder;
double sparrowTimeAvgGPSFinder;

int sparrowStatusRoadFinder;
int sparrowNumMsgsRoadFinder;
int sparrowMsgSizeRoadFinder;
double sparrowMsgSizeAvgRoadFinder;
double sparrowTimeRoadFinder;
double sparrowTimeAvgRoadFinder;

int sparrowStatusFusionMapper;
int sparrowNumMsgsFusionMapper;
int sparrowMsgSizeFusionMapper;
double sparrowMsgSizeAvgFusionMapper;
double sparrowTimeFusionMapper;
double sparrowTimeAvgFusionMapper;

double sparrowMaxObstacleDistance;
double sparrowDistantObstacleSpeed;

double sparrowRDDFNoDataSpeed;

double sparrowStaticScaling;
double sparrowRoadScaling;


double stdDevThresh;
int kernelSize;
double sigma;

#include "costtable.h"
#include "vddtable.h"


void FusionMapper::UpdateSparrowVariablesLoop() {
  m_input.SkynetKey = sparrowSNKey = mapperOpts.optSNKey;

  sparrowAggrTuningRDDFScaling = mapperOpts.corridorOpts.optRDDFscaling;
  sparrowAggrTuningMaxSpeed = mapperOpts.optMaxSpeed;
  sparrowAggrTuningZeroGradSpeedAsRDDFPercentage = mapperOpts.optZeroGradSpeedAsRDDFPercentage;
  sparrowAggrTuningZeroGradSpeed = mapperOpts.optZeroGradSpeed;
  sparrowAggrTuningUnpassableObsHeight = mapperOpts.optUnpassableObsHeight;
  sparrowAggrTuningUseAreaGradient = mapperOpts.optUseAreaGradient;

  sparrowAggrTuningVeloAlg = mapperOpts.optVeloGenerationAlg;
  sparrowAggrTuningVeloTweak1 = mapperOpts.optVeloTweak1;
  sparrowAggrTuningVeloTweak2 = mapperOpts.optVeloTweak2;
  
  sparrowStdDevWeight = mapperOpts.optStdDevWeight;
  sparrowStdDevScaling = mapperOpts.optStdDevScaling;

  sparrowAggrTuningAreaGradientScaling = mapperOpts.optAreaGradScaling;



  sparrowMaxObstacleDistance = mapperOpts.optObstacleDistanceLimit;
  sparrowDistantObstacleSpeed = mapperOpts.optDistantObstacleSpeed;

  sparrowRDDFNoDataSpeed = mapperOpts.corridorOpts.optMaxSpeed;

  sparrowStaticScaling = mapperOpts.optStaticScaling;
  sparrowRoadScaling = mapperOpts.optRoadScaling;

  sparrowCenterPaintTrackline = (int)mapperOpts.corridorOpts.paintCenterTrackline;
  sparrowCenterAdjustWhich = (int)mapperOpts.corridorOpts.whatToAdjust;
  sparrowCenterAdjustmentVal = mapperOpts.corridorOpts.adjustmentVal;

  stdDevThresh = mapperOpts.stdDevThreshold;
  kernelSize = mapperOpts.kernelSize;
	sigma = mapperOpts.sigma;

  int savedMapCount = 0;

    while(!_QUIT) {
      //printf("Got here\n");
    //Update variables that changed in the display
    if(CHANGE) {
      CHANGE = 0;

      m_input.oLR = mapperOpts.optLadar = sparrowStatusLadarFeeder;
      m_input.oSF = mapperOpts.optStereo = sparrowStatusStereoFeeder;
      m_input.oGF = mapperOpts.optStatic = sparrowStatusGPSFinder;
      m_input.oFM = mapperOpts.optRoad   = sparrowStatusRoadFinder;


      mapperOpts.corridorOpts.optRDDFscaling = sparrowAggrTuningRDDFScaling;
      mapperOpts.optMaxSpeed = sparrowAggrTuningMaxSpeed;
      mapperOpts.corridorOpts.optMaxSpeed = sparrowRDDFNoDataSpeed;
      mapperOpts.optZeroGradSpeedAsRDDFPercentage = sparrowAggrTuningZeroGradSpeedAsRDDFPercentage;
      mapperOpts.optZeroGradSpeed = sparrowAggrTuningZeroGradSpeed;
      mapperOpts.optUnpassableObsHeight = sparrowAggrTuningUnpassableObsHeight;
      mapperOpts.optUseAreaGradient = sparrowAggrTuningUseAreaGradient;

      mapperOpts.optAreaGradScaling = sparrowAggrTuningAreaGradientScaling;
      mapperOpts.optVeloGenerationAlg = sparrowAggrTuningVeloAlg;
      mapperOpts.optVeloTweak1 = sparrowAggrTuningVeloTweak1;
      mapperOpts.optVeloTweak2 = sparrowAggrTuningVeloTweak2;

      mapperOpts.optStdDevWeight = sparrowStdDevWeight;
      mapperOpts.optStdDevScaling = sparrowStdDevScaling;
      

      mapperOpts.optObstacleDistanceLimit = sparrowMaxObstacleDistance;
      mapperOpts.optDistantObstacleSpeed = sparrowDistantObstacleSpeed;

      mapperOpts.optStaticScaling = fmin(sparrowStaticScaling, .5);
      mapperOpts.optRoadScaling = fmin(sparrowRoadScaling,.5);

      mapperOpts.corridorOpts.paintCenterTrackline = (CCorridorPainterOptions::PAINT_CENTER_TRACKLINE_TYPE)sparrowCenterPaintTrackline;
      mapperOpts.corridorOpts.whatToAdjust = (CCorridorPainterOptions::WHAT_TO_ADJUST_TYPE)sparrowCenterAdjustWhich;
      mapperOpts.corridorOpts.adjustmentVal = sparrowCenterAdjustmentVal;

      mapperOpts.stdDevThreshold = stdDevThresh;
      mapperOpts.kernelSize = kernelSize;
			mapperOpts.sigma = sigma;

      fusionCostPainter.setMapperOpts(& mapperOpts );
      fusionCorridorPainter.setPainterOptions(&(mapperOpts.corridorOpts));
			fusionSampledCorridorPainter.setPainterOptions(&(mapperOpts.corridorOpts));

      WriteConfigFile();

      double temp = mapperOpts.corridorOpts.optMaxSpeed;

      mapperOpts.corridorOpts.optMaxSpeed = mapperOpts.optMaxSpeed;
      fusionCorridorReference.setPainterOptions(&(mapperOpts.corridorOpts));
      fusionSampledCorridorReference.setPainterOptions(&(mapperOpts.corridorOpts));
      mapperOpts.corridorOpts.optMaxSpeed = temp;
    }
    WriteConfigFile();

    //Update read-only variables
    m_input.nmLR = sparrowNumMsgsLadarFeeder = _infoLadarFeeder.numMsgs;
    m_input.msLR = sparrowMsgSizeLadarFeeder = _infoLadarFeeder.msgSize;
    m_input.amsLR = sparrowMsgSizeAvgLadarFeeder = _infoLadarFeeder.msgSizeAvg;
    m_input.tLR = sparrowTimeLadarFeeder = _infoLadarFeeder.processTime;
    m_input.atLR = sparrowTimeAvgLadarFeeder = _infoLadarFeeder.processTimeAvg;

    m_input.oLR = sparrowStatusLadarFeeder = mapperOpts.optLadar;
    //sparrowStatusLadarFeederBumper = mapperOpts.optLadar;
    m_input.oSF = sparrowStatusStereoFeeder = mapperOpts.optStereo;
    m_input.oGF = sparrowStatusGPSFinder = mapperOpts.optStatic;
    m_input.oRF = sparrowStatusRoadFinder = mapperOpts.optRoad;
    m_input.oFM = sparrowStatusFusionMapper = 1;
  
    /*
    sparrowNumMsgsLadarFeederBumper = _infoLadarFeederBumper.numMsgs;
    sparrowMsgSizeLadarFeederBumper = _infoLadarFeederBumper.msgSize;
    sparrowMsgSizeAvgLadarFeederBumper = _infoLadarFeederBumper.msgSizeAvg;
    sparrowTimeLadarFeederBumper = _infoLadarFeederBumper.processTime;
    sparrowTimeAvgLadarFeederBumper = _infoLadarFeederBumper.processTimeAvg;
    */

    m_input.nmSF = sparrowNumMsgsStereoFeeder = _infoStereoFeeder.numMsgs;
    m_input.msSF = sparrowMsgSizeStereoFeeder = _infoStereoFeeder.msgSize;
    m_input.amsSF = sparrowMsgSizeAvgStereoFeeder = _infoStereoFeeder.msgSizeAvg;
    m_input.tSF = sparrowTimeStereoFeeder = _infoStereoFeeder.processTime;
    m_input.atSF = sparrowTimeAvgStereoFeeder = _infoStereoFeeder.processTimeAvg;

    m_input.nmGF = sparrowNumMsgsGPSFinder = _infoStaticPainter.numMsgs;
    m_input.msGF = sparrowMsgSizeGPSFinder = _infoStaticPainter.msgSize;
    m_input.amsGF = sparrowMsgSizeAvgGPSFinder = _infoStaticPainter.msgSizeAvg;
    m_input.tGF = sparrowTimeGPSFinder = _infoStaticPainter.processTime;
    m_input.atGF = sparrowTimeAvgGPSFinder = _infoStaticPainter.processTimeAvg;

    m_input.nmGF = sparrowNumMsgsRoadFinder = _infoRoad.numMsgs;
    m_input.msGF = sparrowMsgSizeRoadFinder = _infoRoad.msgSize;
    m_input.amsGF = sparrowMsgSizeAvgFusionMapper = _infoRoad.msgSizeAvg;
    m_input.tGF = sparrowTimeRoadFinder = _infoRoad.processTime;
    m_input.atGF = sparrowTimeAvgRoadFinder = _infoRoad.processTimeAvg;

    m_input.nmFM = sparrowNumMsgsFusionMapper = _infoFusionMapper.numMsgs;
    m_input.msFM = sparrowMsgSizeFusionMapper = _infoFusionMapper.msgSize;
    m_input.amsFM = sparrowMsgSizeAvgFusionMapper = _infoFusionMapper.msgSizeAvg;
    m_input.tFM = sparrowTimeFusionMapper = _infoFusionMapper.processTime;
    m_input.atFM = sparrowTimeAvgFusionMapper = _infoFusionMapper.processTimeAvg;

    _PAUSE = PAUSE;
    _QUIT = QUIT;
    if(RESET) {
      _infoLadarFeeder.reset();
      //_infoLadarFeederBumper.reset();
      _infoFusionMapper.reset();
      _infoStaticPainter.reset();
      RESET = 0;
    }

    if(REPAINT) {
      REPAINT = 0;
      int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
      DGClockMutex(&m_mapMutex);
      CDeltaList* deltaList = fusionMap.serializeFullMapDelta<double>(layerID_cost);
      SendMapdelta(socket_num, deltaList);
      fusionMap.resetDelta<double>(layerID_cost);
      DGCunlockMutex(&m_mapMutex);
    }

    if(REEVALUATE) {
      REEVALUATE = 0;
      DGClockMutex(&m_mapMutex);
			fusionCorridorPainter.repaintAll();
			fusionSampledCorridorPainter.repaintAll();
			if(!mapperOpts.optJeremy) {
				//mapperOpts.optRDDFScaling,mapperOpts.optMaxSpeed);
				fusionCostPainter.repaintAll(1);
			} else {
				fusionCostPainter.uberRepaintAll(1);
			}
      DGCunlockMutex(&m_mapMutex);
    }

    if(SAVE) {
      SAVE = 0;
      DGClockMutex(&m_mapMutex);
      char mapName[128];
      for(int i=0; i<fusionMap.getNumLayers(); i++) {
				sprintf(mapName, "fusionMapperMap-%03d-Layer-%02d-(%s)", savedMapCount, i,
								fusionMap.getLayerLabel(i));
				fusionMap.saveLayer<double>(i, mapName, false);
				fusionMap.saveLayer<double>(i, mapName, true);
      }
      savedMapCount++;
      DGCunlockMutex(&m_mapMutex);
    }

    usleep(10000);
  }

}

void FusionMapper::SparrowDisplayLoop() 
{

  dbg_all = 0;

  if (dd_open() < 0) exit(1);

  dd_bindkey('Q', user_quit);
  dd_bindkey('q', user_quit);
  dd_bindkey('P', user_pause);
  dd_bindkey('p', user_pause);
  dd_bindkey('R', user_reset);
  dd_bindkey('r', user_reset);
  dd_bindkey('S', user_save);
  dd_bindkey('s', user_save);
  dd_bindkey('A', user_repaint);
  dd_bindkey('a', user_repaint);
  dd_bindkey('E', user_reevaluate);
  dd_bindkey('e', user_reevaluate);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();	
  dd_close();
  _QUIT = 1;
  //sleep(1);
}

int user_quit(long arg)
{
  QUIT = 1;
  return DD_EXIT_LOOP;
}


int user_reset(long arg)
{
  RESET = 1;
  return 0;
}

int user_pause(long arg)
{
  if(PAUSE) {
    PAUSE=0;
  } else {
    PAUSE=1;
  }
  return 0;
}

int user_change(long arg) {
  CHANGE=1;
  return 0;
}

int user_repaint(long arg) {
  REPAINT=1;
  return 0;
}


int user_save(long arg) {
  SAVE = 1;
  return 0;
}


int user_reevaluate(long arg) {
  REEVALUATE=1;
  return 0;
}
