#ifndef FUSIONMAPPER_HH
#define FUSIONMAPPER_HH

#include <unistd.h>
#include <pthread.h>

#include "StateClient.h"
#include "MapdeltaTalker.h"
#include "DGCutils"
#include "fusionMapperTabSpecs.hh"
#include "TabClient.h"

#include "CMapPlus.hh"
#include "rddf.hh"
#include "CCorridorPainter.hh"
#include "CCostPainter.hh"
#include "StaticPainter.hh"
#include "MapConstants.h"
#include "RoadPainter.hh"
#include "Road.hh"

#define MAX_DELTA_SIZE 999999
#define MAX_ROAD_SIZE 50

using namespace std;

//Sparrow Display Functions
int user_quit(long arg);
int user_pause(long arg);
int user_reset(long arg);
int user_change(long arg);
int user_repaint(long arg);
int user_reevaluate(long arg);
int user_save(long arg);



class FusionMapperStatusInfo {
private:
  unsigned long long startProcessTime;
  unsigned long long endProcessTime;

public:
  int numMsgs;
  int msgSize;
  double msgSizeAvg;
  double processTime;
  double processTimeAvg;

  FusionMapperStatusInfo() {reset();};
  ~FusionMapperStatusInfo() {};

  void startProcessTimer() {
    DGCgettime(startProcessTime);
  };

  void endProcessTimer(int newMsgSize) {
    DGCgettime(endProcessTime);
    msgSize = newMsgSize;
    msgSizeAvg = (msgSize + numMsgs*msgSizeAvg)/(numMsgs+1);
    processTime = ((double)(endProcessTime-startProcessTime))/(1000.0);
    processTimeAvg = (processTime + processTimeAvg*numMsgs)/(numMsgs+1);
    numMsgs++;
  };

  void reset() {
    numMsgs=0;
    msgSize=0;
    msgSizeAvg=0.0;
    processTime=0.0;
    processTimeAvg=0.0;
  };
};
  

class FusionMapper : public CStateClient, public CMapdeltaTalker,
                      public CTrajTalker, public CModuleTabClient {
public:
  FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg, int waitForState);
  ~FusionMapper();
  
  void ActiveLoop();
  
  void ReceiveDataThread_Ladar();

  //void ReceiveDataThread_LadarBumper();

  void ReceiveDataThread_Static();

  void ReceiveDataThread_Stereo();

  void ReceiveDataThread_Road();

  void UpdateSparrowVariablesLoop();

  void PlannerListenerThread();
  
  void SparrowDisplayLoop();

  void ReadConfigFile();

  void WriteConfigFile();

  //void SendDataThread();

private:

  FusionMapperOptions mapperOpts;

  //Info Structs for each module we receive from
  FusionMapperStatusInfo _infoLadarFeeder;
  //FusionMapperStatusInfo _infoLadarFeederBumper;
  FusionMapperStatusInfo _infoStaticPainter;  
  FusionMapperStatusInfo _infoStereoFeeder;
  FusionMapperStatusInfo _infoRoad;
  //And one for output info
  FusionMapperStatusInfo _infoFusionMapper;

  char* m_pLadarMapDelta;
  //char* m_pLadarBumperMapDelta;
  char* m_pStaticMapDelta;
  char* m_pStereoMapDelta;
  double* m_pRoadInfo;


  //double* m_pRoadInfo;

  double* m_pRoadReceive;

  double* m_pRoadX;
  double* m_pRoadY;
  double* m_pRoadW;
  int m_pRoadPoints;

  pthread_mutex_t m_mapMutex;

  CMapPlus fusionMap;
	//Since we may have maps of different resolutions, we will have two different maps
	CMapPlus speedMap;

  
  RDDF fusionRDDF;
  
  CCorridorPainter fusionCorridorPainter;
  CCostPainter fusionCostPainter;
  CCorridorPainter fusionCorridorReference;

	CCorridorPainter fusionSampledCorridorPainter;
	CCorridorPainter fusionSampledCorridorReference;

  CCorridorPainter fusionGrowPainter;
	

  int layerID_stereoAvgDbl;

  int layerID_static;

  int layerID_grown;
  int layerID_road;
  int layerID_ladarElev;
  //double noDataVal_ladarElev;
  //double outsideMapVal_ladarElev;
  
  int layerID_cost;
  //double noDataVal_cost;
  //double outsideMapVal_cost;

	int layerID_fusedYet;

  int layerID_number;
  //long long noDataVal_number;
  //long long outsideMapVal_number;
 
  int layerID_corridor;

  int layerID_fusedElev;
  //CElevationFuser noDataVal_fusedElev;
  //CElevationFuser outsideMapVal_fusedElev;
  
  int layerID_elev;
  //double noDataVal_elev;
  //double outsideMapVal_elev;

  int layerID_fusedElevStdDev;

  int layerID_sampledCost;
	int layerID_sampledCorridor;

  double rddf_insideVal;
  double rddf_outsideVal;
  double rddf_tracklineVal;
  double rddf_edgeVal;

  // Temporary way to debug
//   ofstream delta_log;

  int _QUIT;
  int _PAUSE;
 
  /*!Used for guiTab.*/
  SfusionmapperTabInput  m_input;
  SfusionmapperTabOutput m_output;
};

#endif
