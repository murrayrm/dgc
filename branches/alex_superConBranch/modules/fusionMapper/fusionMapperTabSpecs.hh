#define TABNAME fusionmapper

// fields: type, name, default value, widgetType, xLabel, yLabel, xVal, yVal
// everything is TAB-RELATIVE. Thus input is FROM the client INTO the tab
#define TABINPUTS(_) \
  _(int, SkynetKey, 0, Label, 4, 0, 5, 0) \
  _(char, a, 'Z', SLabel, 0, 0, 0, 0) \
  _(char, b, 'Z', SLabel, 1, 0, 1, 0) \
  _(char, c, 'Z', SLabel, 0, 1, 0, 1) \
  _(char, d, 'Z', SLabel, 0, 2, 0, 2) \
  _(char, e, 'Z', SLabel, 0, 3, 0, 3) \
  _(char, f, 'Z', SLabel, 0, 4, 0, 4) \
  _(char, g, 'Z', SLabel, 0, 5, 0, 5) \
  _(int, oLR, 0, SLabel, 1, 1, 1, 1) \
  _(int, oSF, 0, SLabel, 1, 2, 1, 2) \
  _(int, oGF, 0, SLabel, 1, 3, 1, 3) \
  _(int, oRF, 0, SLabel, 1, 4, 1, 4) \
  _(int, oFM, 0, SLabel, 1, 5, 1, 5) \
  _(char, h, 'Z', SLabel, 0, 6, 0, 6) \
  _(char, i, 'Z', SLabel, 1, 6, 1, 6) \
  _(char, j, 'Z', SLabel, 2, 6, 2, 6) \
  _(char, k, 'Z', SLabel, 3, 6, 3, 6) \
  _(char, l, 'Z', SLabel, 4, 6, 4, 6) \
  _(char, m, 'Z', SLabel, 5, 6, 5, 6) \
  _(char, n, 'Z', SLabel, 0, 7, 0, 7) \
  _(char, o, 'Z', SLabel, 0, 8, 0, 8) \
  _(char, p, 'Z', SLabel, 0, 9, 0, 9) \
  _(char, q, 'Z', SLabel, 0, 10, 0, 10) \
  _(char, r, 'Z', SLabel, 0, 11, 0, 11) \
  _(int, nmLR, 0, SLabel, 1, 7, 1, 7) \
  _(int, nmSF, 0, SLabel, 1, 8, 1, 8) \
  _(int, nmGF, 0, SLabel, 1, 9, 1, 9) \
  _(int, nmRF, 0, SLabel, 1, 10, 1, 10) \
  _(int, nmFM, 0, SLabel, 1, 11, 1, 11) \
  _(int, msLR, 0, SLabel, 2, 7, 2, 7) \
  _(int, msSF, 0, SLabel, 2, 8, 2, 8) \
  _(int, msGF, 0, SLabel, 2, 9, 2, 9) \
  _(int, msRF, 0, SLabel, 2, 10, 2, 10) \
  _(int, msFM, 0, SLabel, 2, 11, 2, 11) \
  _(double, amsLR, 0., SLabel, 3, 7, 3, 7) \
  _(double, amsSF, 0., SLabel, 3, 8, 3, 8) \
  _(double, amsGF, 0., SLabel, 3, 9, 3, 9) \
  _(double, amsRF, 0., SLabel, 3, 10, 3, 10) \
  _(double, amsFM, 0., SLabel, 3, 11, 3, 11) \
  _(double, tLR, 0., SLabel, 4, 7, 4, 7) \
  _(double, tSF, 0., SLabel, 4, 8, 4, 8) \
  _(double, tGF, 0., SLabel, 4, 9, 4, 9) \
  _(double, tRF, 0., SLabel, 4, 10, 4, 10) \
  _(double, tFM, 0., SLabel, 4, 11, 4, 11) \
  _(double, atLR, 0., SLabel, 5, 7, 5, 7) \
  _(double, atSF, 0., SLabel, 5, 8, 5, 8) \
  _(double, atGF, 0., SLabel, 5, 9, 5, 9) \
  _(double, atRF, 0., SLabel, 5, 10, 5, 10) \
  _(double, atFM, 0., SLabel, 5, 11, 5, 11)

#define TABOUTPUTS(_)

//#undef TABNAME
