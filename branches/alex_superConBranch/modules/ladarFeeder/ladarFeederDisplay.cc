#include "ladarFeeder.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#include <iostream>


using namespace std;

int QUIT;
int PAUSE;
int STEP;
int RESET;
int CHANGE;
int RESEND;

int sdSNKey;
int sdCom;
int sdState;
char sdLadarString[256];
int sdFiles;
int sdSim;

int sdScanIndex;
int sdNumDeltas;

char sdLogFilename[256];

char sdErrorMessage[1024];

double sdPosX;
double sdPosY;
double sdPosZ;
double sdAngP;
double sdAngR;
double sdAngY;

double angleSample[25];
double rangeSample[25];

double rate;
double time_ms;

#include "vddtable.h"

void LadarFeeder::UpdateSparrowVariablesLoop() {
  int sampleStep;
  XYZcoord newPositionOffset;
  RPYangle newAngleOffset;

  sdSNKey = ladarOpts.optSNKey;
  sdCom = ladarOpts.optCom;
  sdState = ladarOpts.optState;
  strcpy(sdLadarString, ladarOpts.ladarString);
  
  strcpy(sdLogFilename, ladarObject.getLogFilename());

  sdPosX = ladarPositionOffset.X;
  sdPosY = ladarPositionOffset.Y;
  sdPosZ = ladarPositionOffset.Z;
  sdAngP = ladarAngleOffset.P;
  sdAngR = ladarAngleOffset.R;
  sdAngY = ladarAngleOffset.Y;
  
  while(!_QUIT && !_EOL) {
    //Update read-only variables
    sdScanIndex = ladarObject.scanIndex();
    sdNumDeltas = _numDeltas;
    strcpy(sdErrorMessage, ladarObject.getErrorMessage());
    if(ladarObject.currentSourceType()==ladarSource::SOURCE_FILES) {
      sdFiles=1;
    } else {
      sdFiles = 0;
    }
    if(ladarObject.currentSourceType()==ladarSource::SOURCE_SIM) {
      sdSim=1;
    } else {
      sdSim = 0;
    }

    rate = 1e6/((double) processingTime);
    time_ms = ((double)processingTime)/1e3;

    sampleStep = ladarObject.numPoints()/24;
    if(sampleStep>0) {
      for(int i=0; i<25; i++) {
	angleSample[i] = ladarObject.angle((24-i)*sampleStep)*180.0/M_PI;
	rangeSample[i] = ladarObject.range((24-i)*sampleStep);
      }
    }
    

    //Update variables that changed in the display
    if(CHANGE) {
      CHANGE = 0;

      ladarOpts.optCom = sdCom;
      ladarOpts.optState = sdState;

      newPositionOffset = XYZcoord(sdPosX, sdPosY, sdPosZ);
      newAngleOffset = RPYangle(sdAngR, sdAngP, sdAngY);
      if(newPositionOffset!=ladarPositionOffset || newAngleOffset!=ladarAngleOffset) {
				ladarPositionOffset = newPositionOffset;
				ladarAngleOffset = newAngleOffset;
				ladarObject.setLadarFrame(ladarPositionOffset, ladarAngleOffset);
				DGClockMutex(&ladarMapMutex);
				ladarMap.clearLayer(layerID_ladarElev);
				ladarMap.clearLayer(layerID_ladarElevFused);
				DGCunlockMutex(&ladarMapMutex);    
	
      }
    }

    _PAUSE = PAUSE;
    _QUIT = QUIT;
    if(STEP) {
      _STEP = 1;
      STEP = 0;
    }
    if(RESET) {
      ladarObject.resetScanIndex();
      _numDeltas = 0;
      RESET = 0;
    }
    if(RESEND) {
      CDeltaList* deltaPtr = NULL;
      int socket_num = 0;
      int socket_num_fused = 0;
      int socket_num_stddev = 0;
      int socket_num_num = 0;
      unsigned long long scan_timestamp;
      unsigned long long start, end;
      
      unsigned long long totalCycleTime = (unsigned long long)(1.0e6/ladarOpts.optDeltaRate);
      
      socket_num = m_skynet.get_send_sock(SNladardeltamap);
      socket_num_fused = m_skynet.get_send_sock(SNladarDeltaMapFused);
      socket_num_stddev = m_skynet.get_send_sock(SNladarDeltaMapStdDev);
      socket_num_num = m_skynet.get_send_sock(SNladarDeltaMapNum);

      DGClockMutex(&ladarMapMutex);
      
      DGCgettime(scan_timestamp);
      
      deltaPtr = ladarMap.serializeFullMapDelta<double>(layerID_ladarElev);
      if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1) {
	SendMapdelta(socket_num, deltaPtr);
	_numDeltas++;
      }
      ladarMap.resetDelta<double>(layerID_ladarElev);    
      
      deltaPtr = ladarMap.serializeFullMapDelta<CElevationFuser>(layerID_ladarElevFused);
      if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1) {
	SendMapdelta(socket_num_fused, deltaPtr);
	_numDeltas++;
      }
      ladarMap.resetDelta<CElevationFuser>(layerID_ladarElevFused);    
      
      deltaPtr = ladarMap.serializeFullMapDelta<double>(layerID_ladarElevStdDev);
      if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1) {
	SendMapdelta(socket_num_stddev, deltaPtr);
	_numDeltas++;
      }
      ladarMap.resetDelta<double>(layerID_ladarElevStdDev);    

      deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElevNum);
      if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1) {
	SendMapdelta(socket_num_num, deltaPtr);
	_numDeltas++;
      }
      ladarMap.resetDelta<double>(layerID_ladarElevNum);    

      DGCunlockMutex(&ladarMapMutex);    
      RESEND = 0;
    }

    usleep(10000);
  }

  dd_close();
}

void LadarFeeder::SparrowDisplayLoop() 
{
  dbg_all = 0;

  PAUSE = _PAUSE;

  if (dd_open() < 0) exit(1);

  dd_bindkey('Q', user_quit);
  dd_bindkey('q', user_quit);
  dd_bindkey('P', user_pause);
  dd_bindkey('p', user_pause);
  dd_bindkey('R', user_reset);
  dd_bindkey('r', user_reset);
  dd_bindkey('S', user_step);
  dd_bindkey('s', user_step);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();	
  dd_close();
  _QUIT = 1;
}

int user_quit(long arg)
{
  PAUSE = 0;
  QUIT = 1;
  return DD_EXIT_LOOP;
}


int user_reset(long arg)
{
  RESET = 1;
  return 0;
}


int user_step(long arg)
{
  STEP = 1;
  return 0;
}


int user_pause(long arg)
{
  if(PAUSE) {
    PAUSE=0;
  } else {
    PAUSE=1;
  }
  return 0;
}


int user_change(long arg) {
  CHANGE=1;
  return 0;
}


int user_resend(long arg) {
  RESEND = 1;
  return 0;
}
