/**
 * gui.h
 * Revision History:
 * 01/07/2005  hbarnor  Created
 */

#ifndef GUI_H
#define GUI_H

#include <gtkmm/window.h>
#include <gtkmm/notebook.h>

#include "MainTab.h"
#include "MapDisplay/MapDisplay.hh"
#include "../StatusTab/StartTab.hh"
#include "rddf.hh"
#include "../MapDisplaySN/MapDisplaySN.hh"
#include "../TimberBox/CTimberBox.hh"


using namespace std;

#define DEBUG false

/**
 * GUI class. This class is a representation of 
 * the main window in the skynet gui.
 * $Id$
 */
class Gui : public Gtk::Window
{

 public:
  /**
   * Gui default constructor.
   * Initializes a 400x200 window with the various module tabs
   */
  Gui();
  /**
   * Gui constructor with a mapconfig passed to it.
   * Initializes a 400x200 window with the various module tabs
   */
  Gui(MapConfig * mapConfig);
  /**
   * Gui Destructor - for taking care of dynamically 
   * allocated memory. 
   */
  ~Gui();
  /** 
   * Initialize the GTK gui. Set the window tite, add the tabs 
   * and basically set-up the display.
   */
  void initGui();

  void startThreads();
  /**
   * TODO
   */
  void buildStartTab(string configFile);
 private:
  /**
   * SkynetKey used by all modules
   */
  int sn_key;
  /**
   * Instance of skynet used by all other sub modules.
   */
  //skynet* m_skynet;

  /**
   * Initialize the MapDisplay. This involves setting 
   * RDDF data of the mapDisplay
   */
  void initMapConfig();

  /**
   * Dynamically adds a tab for a recently started module.
   * Requests a pointer from StatustTab of the tab to be added. Adds
   * the tabto the gui.
   */
  void dynAddTab();

  /**
   * update the skynet key
   * from the users environment
   */
  void updateSNKey();
  /**
   * Widget to create the tabs
   */
  Gtk::Notebook notebook;
  /**
   * The following are instantiations of the various
   * sub-widgets in the skynet gui.
   */ 
  MainTab mainTab;
  CTimberBox *timberBox;
  StartTab *startTab;
  MapWidget *m_mapWidget;
  MapConfig * map_config;
  DGCTab *plannerTab;

  /*
   * Thread to update the status messages
   */
  Glib::Thread *  status_thread; 
  /**
   * Signal that adds new tabs when modules become active.
   */
  sigc::signal<void> * addTabSignal;
};

#endif
