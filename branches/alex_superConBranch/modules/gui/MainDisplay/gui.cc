/**
 * gui.cc
 * Revision History:
 * 01/07/2005  hbarnor  Created
 */

#include <iostream>
#include "gui.h"

Gui::Gui()
{
  map_config = new MapConfig();
  m_mapWidget= new MapWidget(map_config);
  add_mnemonic('l', m_mapWidget->m_spin_cmap_layer);
  add_mnemonic('m', m_mapWidget->m_spin_zoom_scale);
  add_mnemonic('e', m_mapWidget->m_check_colormap_auto_center);
  add_mnemonic('r', m_mapWidget->m_check_colormap_auto_range);
  add_mnemonic('k', m_mapWidget->m_button_clear_cmap);
  add_mnemonic('j', m_mapWidget->m_button_clear_all_layers);
  add_mnemonic('q', m_mapWidget->m_spin_colormap_center);
  add_mnemonic('w', m_mapWidget->m_spin_colormap_range);
  add_mnemonic('z', m_mapWidget->m_check_draw_gridlines);
  add_mnemonic('x', m_mapWidget->m_check_draw_sensors);
  add_mnemonic('c', m_mapWidget->m_check_draw_waypoints);
  add_mnemonic('v', m_mapWidget->m_check_draw_rddf);
  add_mnemonic('b', m_mapWidget->m_check_draw_paths);
  add_mnemonic('n', m_mapWidget->m_check_draw_info);
  set_mnemonic_modifier(Gdk::CONTROL_MASK);
  addTabSignal = new sigc::signal<void>();
  addTabSignal->connect(mem_fun(this, &Gui::dynAddTab));
  updateSNKey();
  startTab = new StartTab(sn_key, addTabSignal);
  initGui();  
  show_all_children();
}
 
Gui::Gui(MapConfig * mapConfig)
  : map_config(mapConfig)
{
  m_mapWidget= new MapWidget(map_config);
  add_mnemonic('q', m_mapWidget->m_spin_colormap_center);
  add_mnemonic('w', m_mapWidget->m_spin_colormap_range);
  add_mnemonic('e', m_mapWidget->m_check_colormap_auto_range);
  add_mnemonic('r', m_mapWidget->m_check_colormap_auto_center);
  add_mnemonic('l', m_mapWidget->m_spin_cmap_layer);
  add_mnemonic('m', m_mapWidget->m_spin_zoom_scale);
  add_mnemonic('k', m_mapWidget->m_button_clear_cmap);
  add_mnemonic('j', m_mapWidget->m_button_clear_all_layers);
  add_mnemonic('i', m_mapWidget->m_check_draw_cmap);
  add_mnemonic('z', m_mapWidget->m_check_draw_gridlines);
  add_mnemonic('x', m_mapWidget->m_check_draw_sensors);
  add_mnemonic('c', m_mapWidget->m_check_draw_waypoints);
  add_mnemonic('v', m_mapWidget->m_check_draw_rddf);
  add_mnemonic('b', m_mapWidget->m_check_draw_paths);
  add_mnemonic('n', m_mapWidget->m_check_draw_info);
  add_mnemonic('o', m_mapWidget->m_button_saveLog);
  add_mnemonic('p', m_mapWidget->m_button_findCMap);

  updateSNKey();

  timberBox = new CTimberBox(sn_key);

  m_mapWidget->pack_start(*timberBox, Gtk::PACK_SHRINK, 0); 

  add_mnemonic('a', *(timberBox->m_button_timber_resume));
  add_mnemonic('s', *(timberBox->m_button_timber_pause));
  add_mnemonic('d', *(timberBox->m_button_timber_replay));
  add_mnemonic('f', *(timberBox->m_button_timber_start));
  add_mnemonic('g', *(timberBox->m_button_timber_stop));
  add_mnemonic('h', *(timberBox->m_button_timber_restart));

  set_mnemonic_modifier(Gdk::CONTROL_MASK);

  addTabSignal = new sigc::signal<void>();
  addTabSignal->connect(mem_fun(this, &Gui::dynAddTab));
  //m_skynet = new skynet(SNGui, sn_key);
  startTab = new StartTab(sn_key, addTabSignal);
  //statusTab = new StatusTab(sn_key, addTabSignal);
  //plannerTab = new ModuleTab(m_skynet);
}


Gui::~Gui()
{

  // do some memory clean up
  delete startTab;
  delete timberBox;
  delete m_mapWidget;
  delete addTabSignal;
}

void Gui::initGui()
{
  set_title("BESTEST GUI EVER - New 2D Plot Features"); // set the title of the window
  set_border_width(5); // set the border size 
#warning "Hard-coded window size not good"
  set_default_size(600,400); // default size of the 
  notebook.set_border_width(5); // set the border width of the tabbed panes
  //initMapConfig();
  notebook.append_page(*m_mapWidget,"MapDisplay"); // add the mapdisplay tab  
  notebook.append_page(*startTab,"Module Starter"); // add the adrive tab 
  add(notebook);
  show_all_children();
}

void Gui::initMapConfig()
{

}

void Gui::updateSNKey()
{
  /** 
   * Read skynet key from environment
   */
  sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
    {
      cerr << "SKYNET_KEY environment variable isn't set" << endl;
    }
  else
    {
      sn_key = atoi(pSkynetkey);
    }
  cout << "Constructing skynet with KEY = " << sn_key << endl;
}

void Gui::startThreads()
{
  //cout << "Spawning Gui sub-Thread" << endl;
  startTab->startSubThreads();
  status_thread = Glib::Thread::create( sigc::mem_fun(startTab, &StartTab::statusDisplay), false);
  //status_thread = Glib::Thread::create( sigc::mem_fun(statusTab, &StatusTab::listen), false);
  ///DGCstartMemberFunctionThread(startTab,&StartTab::statusDisplay);
}

void Gui::buildStartTab(string configFile)
{
  startTab->readFile(configFile);
  startTab->show_all_children();
}

void Gui::dynAddTab()
{
  string newTabName = startTab->getTabName();
  Widget* newTab = startTab->getTab();
  notebook.append_page(*newTab, newTabName);  
  show_all_children();
}
