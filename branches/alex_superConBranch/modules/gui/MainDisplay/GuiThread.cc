/**
 * GuiThread.cc
 * Revision History:
 * 01/21/2005  hbarnor  Created
 */

#include "GuiThread.h"

MapConfig * GuiThread::get_map_config()
{
  return m_map_config;
}

GuiThread::GuiThread(MapConfig * mc)
  : m_map_config(mc)
{      
  kit = new Gtk::Main(0, NULL);
  Gtk::GL::init(0, NULL);
  window = new Gui(m_map_config);
  //Gui::Gui window(m_map_config);
  
}

void GuiThread::main()
{
  cout << "Starting Gui Thread" << endl;  

  window->startThreads();
  kit->run(*window); //Shows the window and returns when it is closed.    
}

void GuiThread::guithread_start_thread() 
{
  cout << "Initializing glib threads" << endl;  
  Glib::thread_init();  
  cout << "Spawning Gui Thread" << endl;  
  m_thread = Glib::Thread::create( sigc::mem_fun(*this, & GuiThread::main), false);
}

GuiThread::~GuiThread()
{
  // cannot delete a glib thread 
  ///delete m_thread;
  delete window;
  delete kit;
}

Gui * GuiThread::getGui()
{
  return window;
}
