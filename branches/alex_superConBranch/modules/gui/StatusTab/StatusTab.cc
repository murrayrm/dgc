/**
 * StatusTab.cc
 * Revision History:
 * 01/07/2005  hbarnor  Created
 */

#include <iostream>

#include "StatusTab.hh"

StatusTab::StatusTab(int sn_key, sigc::signal<void>* addTabSig)
  : statusFrm("Status Table"),
    m_skynet(SNGuimodstart, sn_key),
    labelCount(0),
    timeOutValue(1500) //start counting labels from zero
{  
  myInput = SNmodlist;
  sender = ALLMODULES;
  m_addTab.connect(*addTabSig);
  show_all_children();
}

StatusTab::~StatusTab()
{
  delete newTabMutex;
}


void StatusTab::listen()
{
#ifndef OLD_SKYNET
  newTabMutex = new Glib::Mutex();
  int listenSocket = m_skynet.listen(myInput,sender);
	SSkynetID id;
  while(true)
  {
    int size;
    size = m_skynet.get_msg(listenSocket, &id, sizeof(id), 0);

    if(size != sizeof(id))
      {
	cerr << "StatusTab::listen(); got " << size << " bytes. expected " << sizeof(id) << endl;
      }
    else
      {
	intelliAdd(id);
      }
  }
#endif
}

void StatusTab::intelliAdd(SSkynetID& id)
{
  //find map for this location
  ComputerMap::iterator myIter = myComps.find(id.host);  
  StatusMap::iterator module;
  if(myIter == myComps.end())
    {
      cerr << "Did not find computer" << endl;
      //create map and insert status if 
      // it does not already exist
      Status *newModule = manage(new Status(Glib::ustring(modulename_asString(id.name)),id.host));
      StatusMap *newSMap = new StatusMap();
      newSMap->insert(make_pair(id.name,newModule));
      myComps.insert(make_pair(id.host, newSMap));
      addLabel(id.host, newModule);
      generateTab(id.name);
    }
  else
    {
      // map for location exists
      // get map of id's and locate the id
      module = myIter->second->find(id.name);
      if(module == myIter->second->end())
	{
	  // insert module if it does not aleady 
	  // exist
	  Status *newModule = manage(new Status(modulename_asString(id.name),id.host));
	  myIter->second->insert(make_pair(id.name,newModule));
	  addLabel(id.host,newModule);
	  generateTab(id.name);
	}
      else
	{
	  module->second->setActive(true);
	  //cout << "setting as active" << endl;
	}
    }
}

void StatusTab::addLabel(char *pHostname, Status *label)
{
  //create a slot for the label in the main display thread 
  sigc::slot<bool> my_slot = sigc::bind(sigc::mem_fun(label,&Status::on_timeout),timeOutValue);
  //connect slot to Glib::signal_imeout()
  sigc::connection conn = Glib::signal_timeout().connect(my_slot,timeOutValue);

  std::map< string, Gtk::Frame* >::iterator frameIter = myFrames.find(pHostname);
  if(frameIter == myFrames.end())
    {
      frameIter= myFrames.insert(make_pair(pHostname,manage(new MultiFrame(Glib::ustring(pHostname))))).first;      
      add(*(frameIter->second));
    } 
  frameIter->second->add(*label);
  show_all_children();
}

void StatusTab::generateTab(modulename newModule)
{
  cout << " calling generate tab " << newModule << endl;
#include "tabSelection.h"

}

