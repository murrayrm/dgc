/**
 * StartTab.cc
 * Revision History:
 * 02/19/2005  hbarnor  Created
 */

#include "StartTab.hh"
//#include "ModuleStarter.hh"


StartTab::StartTab(int sn_key,  sigc::signal<void>* addTabSig)
  : m_sn_key(sn_key),
    modulesFrm("Module Management"),
    modulesTbl(1,6,false),
    modulesBtnBox(Gtk::BUTTONBOX_SPREAD),
    modulesSSelBtn("Start Selected"),
    //modulesKSelBtn("Exit Selected"),
    statusDisplayFrm("Status Messages"),

    m_statusTab(sn_key, addTabSig),  
    m_skynet(SNGuimodstart, sn_key) 
{
  sender = SNmodstart;  
  myInput = SNGuiMsg;
  initSN();

  add(m_VBox);

  //Module list
  modulesFrm.set_shadow_type(Gtk::SHADOW_ETCHED_IN);  
  modulesFrm.add(modulesVBox);
  modulesVBox.pack_start(modulesTbl);
  modulesVBox.pack_start(modulesBtnBox);
  modulesBtnBox.add(modulesSSelBtn);
  //modulesBtnBox.add(modulesKSelBtn);

  modulesSSelBtn.signal_clicked().connect(sigc::mem_fun(this,&StartTab::startModules));
  //modulesKSelBtn.signal_clicked().connect(sigc::mem_fun(this,&StartTab::killModules));

  m_VBox.pack_start(modulesFrm);

  //Status window
  updateDisplay.connect(sigc::mem_fun(this,&StartTab::addMessage));

  statusDisplayFrm.set_shadow_type(Gtk::SHADOW_ETCHED_IN);  
  statusDisplayFrm.add(m_statusTab);
  m_VBox.pack_start(statusDisplayFrm);
  //root directory 
  Gtk::Label *lbl = manage(new Gtk::Label("", Gtk::ALIGN_LEFT));
  lbl->set_markup("<b>Root Directory</b>");
  modulesTbl.attach(*lbl, 1, 2, 0, 1, Gtk::FILL,Gtk::FILL, 5,5);
  // allocate space to get current path
  char  pwd[pathmax+1]; 
  int size = pathmax+1;
  
  if(getcwd(pwd,size) == NULL)
    {
      cerr << "Get cwd failed." << endl;
    }
  rootDir = manage(new Gtk::Entry());
  rootDir->set_text(Glib::ustring(pwd));
  modulesTbl.attach(*rootDir,2, 4, 0, 1, Gtk::FILL,Gtk::FILL, 5,5);
  //Add headers for the modules list
  lbl = manage(new Gtk::Label("", Gtk::ALIGN_LEFT));
  lbl->set_markup("<b>Module</b>");
  modulesTbl.attach(*lbl, 1, 2, 1, 2, Gtk::FILL,Gtk::FILL, 5,5);

  lbl = manage(new Gtk::Label("", Gtk::ALIGN_LEFT));
  lbl->set_markup("<b>Command line</b>");
  modulesTbl.attach(*lbl, 2, 3, 1, 2, Gtk::FILL | Gtk::EXPAND,Gtk::FILL, 5,5);

  lbl = manage(new Gtk::Label("", Gtk::ALIGN_LEFT));
  lbl->set_markup("<b>Cpu#</b>");
  modulesTbl.attach(*lbl, 3, 4, 1, 2, Gtk::FILL,Gtk::FILL, 5,5);

  show_all_children();
}

StartTab::~StartTab()
{
  for(list<ModuleRow*>::iterator itr = moduleRows.begin(); itr != moduleRows.end(); ++itr)
    delete *itr;
  moduleRows.clear();
}

void StartTab::initSN()
{
  cmdSendSocket = m_skynet.get_send_sock(SNmodcom);
  if(cmdSendSocket< 0)
    {
      cerr << "StartTab::initSN: skynet get send returned error" << endl;
    }  
}


void StartTab::startModules()
{
  for(list<ModuleRow*>::iterator itr = moduleRows.begin(); itr != moduleRows.end(); ++itr) {
    if((*itr)->chkBtn->get_active())
      startModule(*itr);
  }
  cout << "I got clicked" << endl;
}

void StartTab::startModule(ModuleRow *row)
{
  executeSkynet('s', row);
}

void StartTab::executeSkynet(char action, ModuleRow *row)
{
  cerr<<"Executing on module:"<<row->cmd.type<<endl;
  /**
   * Gtk::Table_Helpers::TableList& tl = chkBxTbl.children();
   *for (Gtk::Table_Helpers::TableList::iterator cur = tl.begin(); cur != tl.end(); cur++)
   * {
   * }
   */
  row->cmd.action = action;  // set the action to perform 
  //(*iter).cmd.compID = compArray[currComp];	
  cout << "Sending Command: " <<  row->cmd.type << " Action: " <<  row->cmd.action << " Computer: " <<  row->cmd.compID << endl;
  if(action == 's')
    {
      // putting all the following on one line generates errors
      stringstream command;
      command <<"ssh -X " << row->cpuSpn->get_text() <<  " \"/usr/local/bin/startModule.sh " <<  rootDir->get_text() << " " << row->cmd.type <<  " " << m_sn_key  << " \\\"" << row->cmdEnt->get_text() << "\\\" \" &";
      cout << command.str() << endl;
      system(command.str().c_str());
    }
}


void StartTab::killModules()
{
  for(list<ModuleRow*>::iterator itr = moduleRows.begin(); itr != moduleRows.end(); ++itr) {
    if((*itr)->chkBtn->get_active())
      killModule(*itr);
  }
}

void StartTab::killModule(ModuleRow *row)
{
  executeSkynet('k', row);
}

bool cpuSpnBtnOutput(Gtk::SpinButton *pSpnBtn)
{
  if(!pSpnBtn)
    return false;
  char buf[20];
  if((int)pSpnBtn->get_value() < 8)
    {
      sprintf(buf, "skynet%i", (int)pSpnBtn->get_value());
      pSpnBtn->set_text(buf);
      return true;
    }
  else
    {
      // this is really a quick hack - there should be another way to do this
      switch((int)pSpnBtn->get_value())
	{
	case 8:
	  sprintf(buf, "moscow");
	  break;
	case 9:
	  sprintf(buf, "nickel");
	  break;
	case 10:
	  sprintf(buf, "cobalt");
	  break;
	case 11:
	  sprintf(buf, "gcdev");
	  break;
	case 12:
	  sprintf(buf, "tantalum");
	  break;
	case 13:
	  sprintf(buf, "magnesium");
	  break;
	case 14:
	  sprintf(buf, "iridium");
	  break;
	default:
	  sprintf(buf, "gcdev");
	}
      pSpnBtn->set_text(buf);
      return true;
    }
}

int cpuSpnBtnInput(double *val, Gtk::SpinButton *pSpnBtn)
{
  if(!pSpnBtn)
    return false;

  string str = pSpnBtn->get_text();
  int v;
  if(sscanf(str.c_str(), "skynet%i", &v)==0)
    {
      if(strcmp(str.c_str(),"gcdev") == 0)
	{
	  *val = 11;
	  return true;
	}
      if(strcmp(str.c_str(),"iridium") == 0)
	{
	  *val = 14;
	  return true;
	}
      if(strcmp(str.c_str(),"magnesium") == 0)
	{
	  *val = 13;
	  return true;
	}
      if(strcmp(str.c_str(),"tantalum") == 0)
	{
	  *val = 12;
	  return true;
	}
      if(strcmp(str.c_str(),"cobalt") == 0)
	{
	  *val = 10;
	  return true;
	}
      if(strcmp(str.c_str(),"nickel") == 0)
	{
	  *val = 9;
	  return true;
	}
      if(strcmp(str.c_str(),"moscow") == 0)
	{
	  *val = 8;
	  return true;
	}
      return false;
    }
  else
    {
      *val = v;
      return true;
    }
}

void StartTab::addModule(string snModName, string cmdline, int destComp, bool start)
{
  cout << "AddModule " << snModName << " CmdLine: " << cmdline << " Computer: " << destComp << " Checked is " << start << endl;
  ModuleRow *mr = new ModuleRow;

  //Create all controls for the module row
  mr->chkBtn = manage(new Gtk::CheckButton());
  mr->chkBtn->set_active(start);

  mr->moduleLbl = manage(new Gtk::Label(snModName, Gtk::ALIGN_LEFT));

  mr->cmdEnt = manage(new Gtk::Entry());
  mr->cmdEnt->set_text(cmdline);

  mr->cpuSpn = manage(new Gtk::SpinButton(1, 0));
  mr->cpuSpn->set_range(1,14);
  mr->cpuSpn->set_increments(1, 3);
  mr->cpuSpn->set_value(destComp);
  mr->cpuSpn->set_width_chars(10);
  mr->cpuSpn->signal_output().connect( sigc::bind(sigc::ptr_fun(cpuSpnBtnOutput), mr->cpuSpn) );
  mr->cpuSpn->signal_input().connect( sigc::bind(sigc::ptr_fun(cpuSpnBtnInput), mr->cpuSpn) );

  mr->startBtn = manage(new Gtk::Button("Start"));
  //mr->killBtn = manage(new Gtk::Button("Exit"));

  mr->startBtn->signal_clicked().connect(sigc::bind(sigc::mem_fun(this,&StartTab::startModule), mr));
  //mr->killBtn->signal_clicked().connect(sigc::bind(sigc::mem_fun(this,&StartTab::killModule), mr));

  //Add module row to table
  int row = modulesTbl.property_n_rows();
  modulesTbl.resize(row+1, modulesTbl.property_n_columns());
  modulesTbl.attach(*mr->chkBtn, 0, 1, row, row+1, Gtk::FILL,Gtk::FILL, 5,5);
  modulesTbl.attach(*mr->moduleLbl, 1, 2, row, row+1, Gtk::FILL,Gtk::FILL, 5,5);
  modulesTbl.attach(*mr->cmdEnt, 2, 3, row, row+1, Gtk::FILL | Gtk::EXPAND,Gtk::FILL, 5,0);
  modulesTbl.attach(*mr->cpuSpn, 3, 4, row, row+1, Gtk::FILL,Gtk::FILL, 5,5);
  modulesTbl.attach(*mr->startBtn, 4, 5, row, row+1, Gtk::FILL,Gtk::FILL, 5,5);
  //modulesTbl.attach(*mr->killBtn, 5, 6, row, row+1, Gtk::FILL,Gtk::FILL, 5,5);

  strcpy(mr->cmd.type, snModName.c_str());
  mr->cmd.action='u'; //mark as unused
  mr->cmd.compID = destComp; // give it a computer to run on  
  moduleRows.push_back(mr);
  modulesTbl.show_all_children();
}

void StartTab::readFile(string fileName)
{
  // read in config file passed in as param
  // format of config file will be 
  // first line of config file is a number 
  // specifying the number of modules 
  // read that and call the function  
  // setMaxModule(int n)
  // Moule:snModName 0/1 
  // snModName is the synet module name
  // 0/1 - means either a 0 or a 1 
  // where 1 means start it up automatically and
  // 0 means show it with a non-checked checkbox 
  // but don't start it up 
  // for each line in the config file call the function
  // addModule(String snModName, bool start)

  //char buffer[256];
  //int numLines = 0;
  string snModName, line;
  unsigned int pos, i;
  bool start;
  unsigned int destComp;
  string cmdline;
  ifstream configfile(fileName.c_str() );
  if (! configfile.is_open())
    { cout << "Error opening file: " << fileName << "Run gui from bin directory "; exit (1); }
  while (! configfile.eof() )
    {
      //cout << "\nreading line..." << endl;
      getline(configfile, line);
      if(! line.empty() )
	if(line[0] != '#') // # at beginning of line denotes comment
	  {	    
	    //parse string
	    if( (pos = line.find(" ")) != string::npos)
	      {
		if(line.substr(0,pos) == _module)
		  {
		    //module listing
		    line.erase(0,pos);
		    for(i = 0;line[i] == ' ';++i);//elim whitepsace
		    line.erase(0,i);
		    //cout << "module listing length: " << pos << endl;
		    //cout << "line is now: " << line << endl;
		    if( (pos = line.find(" ")) != string::npos)
		      {
			snModName = line.substr(0,pos); //module name
			line.erase(0,pos);
			
			
			for(i = 0;line[i] == ' ';++i);//elim whitepsace
			line.erase(0,i);
			
			
			// get computer number
			if( (pos = line.find(" ")) != string::npos)
			  {
			    destComp = atoi((line.substr(0,pos)).c_str()); // set the computer this module should run on. 
			    line.erase(0,pos);
			  }
			
			
			for(i = 0;line[i] == ' ';++i);//elim whitepsace
			line.erase(0,i);			
			
			
			//cout << "module name length: " << pos << endl;
			//cout << "line is now: " << line << endl;
			switch (line[0])
			  {
			  case '0':
			    start = 0;
			    break;
			  case '1':
			    start = 1;
			    break;
			  default:
			    cout << "invalid boolean specifier: " << line[0] <<  endl;
			    //cout << "in line: " << line << endl;
			    exit (1);
			  }
			//Parse past pause column and then parse cmdline
			cmdline="";
			//cout<<"Remains of parse line: "<<line<<endl;
			if(line.find_first_not_of(" ", 1)!=string::npos)
			  {
			    line.erase(0,line.find_first_not_of(" ", 1));
			    if(line.find(" ")!=string::npos)
			      {
				line.erase(0, line.find(" ")); //Skip pause column
				if(line.find_first_not_of(" ")!=string::npos)
				  {
				    line.erase(0, line.find_first_not_of(" "));
				    cmdline = line;
				  }
			      }
			  }
			addModule(snModName, cmdline, destComp, start); //add module to skynetGUI
		      }
		  }
	      }
	  }
    }
  cout << "done" << endl; 
  configfile.close();
}

void StartTab::updateConfig(const string fileName, const string snModName, const bool start)
{
  // updates the config file start-up value
  string s;  //input string read from file
  char start_char = (start ? 1 : 0) + '0';
  fstream configfile(fileName.c_str(), fstream::in | fstream::out);

  if (! configfile.is_open() )
    { cout << "Error opening file: " << fileName << "Run gui from bin directory "; exit (1); }
  while (! configfile.eof() )
    {
      configfile >> s;
      if(s[0] != '#')
	{
	  if(s == _module) // module listing
	    {
	      configfile >> s;
	      if (s == snModName)
		{
		  cout << "module name: " << s << endl;
		  cout << "snModName: " << snModName << endl;
		  configfile.get();
		  configfile.put(start_char);
		}
	      else
		{
		  getline(configfile,s);
		  cout << "Instead of module name we have: " << s << endl;
		  cout << "string in: " << s << endl;
		}
	    }
	  else
	    {
	      cout << "Instead of Module: we have: " << s << endl;
	      //get rid of the rest of the line
	      getline(configfile, s);
	      cout << "string out: " << s << endl;
	    }
	}
      else
	{
	  getline(configfile, s); //clear commented line
	  cout << "commented line... " << s << endl;
	}
    }
  configfile.close();  
}



void StartTab::statusDisplay()
{  
  int statusSocket = m_skynet.listen(myInput, sender);
  int MSGSIZE = sizeof(mesg);
  while(true)
    {
      m_skynet.get_msg(statusSocket, &mesg, MSGSIZE, 0);
      updateDisplay();
      //statText.show();
    }
}

void StartTab::addMessage()
{
  statPos = buffer->insert(statPos,Glib::ustring(mesg));
}

void StartTab::startSubThreads()
{
  statusTab_thread = Glib::Thread::create( sigc::mem_fun(m_statusTab, &StatusTab::listen), false);
}
