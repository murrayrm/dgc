# DGC Globaly Included Makefile
# Include this at the top of every makefile after setting DGC
.PHONY: redirect folders all
redirect: folders all

# Tool definitions
CC = gcc
CPP= g++
INCLUDE=-I$(INCLUDEDIR)
CFLAGS += -g -Wall $(INCLUDE)
CPPFLAGS += -pthread $(CFLAGS)
LDFLAGS += -g -pthread -Wall -L$(LIBDIR) -ltspread -lserial
INSTALL = install
INSTALL_DATA = ln -f
INSTALL_PROGRAM = ln -f 
#INSTALL_DATA = $(INSTALL) -m 644
#INSTALL_PROGRAM = $(INSTALL) -m 754
MAKEDEPEND = makedepend -Y
CDD = $(BINDIR)/cdd

# Rule to make the target folders and copy constants before any rules execute
ifneq (1,${MAKINGFIRST})
MAKEFIRST := $(shell export MAKINGFIRST=1 && $(MAKE) first)
endif

# Directory constants
BINDIR = $(DGC)/bin
LIBDIR = $(DGC)/lib
INCLUDEDIR = $(DGC)/include

# headers library locations
#  fake libraries, don't link with these :)
RPGSEEDERHDR        = $(LIBDIR)/rpgSeederHeaderLib.a

# header include makefiles - must have HEADERNAME_PATH and HEADERNAME_DEPEND 
# defined within
include $(DGC)/modules/rddfPathGen/rpgSeederHeader.mk

# drivers library locations
ALICELIB            = $(LIBDIR)/alice.a
IMU_FASTCOMLIB      = $(LIBDIR)/imu_fastcom.a
SDSLIB              = $(LIBDIR)/SDS.a
LADARDRIVERSLIB     = $(LIBDIR)/ladardrivers.a
STEREOSOURCELIB     = $(LIBDIR)/stereoSource.a
LADARSOURCELIB      = $(LIBDIR)/ladarSource.a
GPSLIB              = $(LIBDIR)/gps.a

# driver include makefiles - must have DRIVERNAME_PATH and DRIVERNAME_DEPEND 
# defined within
include $(DGC)/drivers/alice/alice.mk
include $(DGC)/drivers/gpsSDS/gps.mk
include $(DGC)/drivers/gps/gps.mk
include $(DGC)/drivers/imu_fastcom/imu_fastcom.mk
include $(DGC)/drivers/SDS/SDS.mk
include $(DGC)/drivers/ladardrivers/ladardrivers.mk
include $(DGC)/drivers/stereovision/stereoSource.mk
include $(DGC)/drivers/ladar/ladarSource.mk

# Util library locations
CMAPLIB             = $(LIBDIR)/CMap.a
CMAPPLIB            = $(LIBDIR)/CMapPlus.a
CCPAINTLIB          = $(LIBDIR)/CCorridorPainter.a
CCOSTPAINTLIB       = $(LIBDIR)/CCostPainter.a
CPIDLIB             = $(LIBDIR)/libpid.a
DGCUTILS            = $(LIBDIR)/DGCutils.o
FINDFORCELIB        = $(LIBDIR)/findforce.a
FRAMESLIB           = $(LIBDIR)/frames.a
GGISLIB             = $(LIBDIR)/ggis.a
MAPDISPLAYLIB       = $(LIBDIR)/MapDisplay.a
MODULEHELPERSLIB    = $(LIBDIR)/libmodulehelpers.a 
PIDCONTROLLERLIB    = $(LIBDIR)/PID_Controller.a
PLANNERLIB          = $(LIBDIR)/planner.a
RDDFLIB             = $(LIBDIR)/rddf.a
SKYNETLIB           = $(LIBDIR)/libskynet.a
SPARROWLIB          = $(LIBDIR)/sparrow.a
STEREOPROCESSLIB    = $(LIBDIR)/stereoProcess.a
TRAJLIB             = $(LIBDIR)/traj.a
LADARCLIENTLIB	    = $(LIBDIR)/LadarClient.o
RFCOMMUNICATIONSLIB = $(LIBDIR)/RFCommunicationslib.a
GEOMETRYLIB         = $(LIBDIR)/geometry.a
CTRAJPAINTERLIB     = $(LIBDIR)/CTrajPainter.a
CELEVFUSERLIB       = $(LIBDIR)/CElevationFuser.a
PROFILERLIB         = $(LIBDIR)/profiler.a
ROADPAINTERLIB      = $(LIBDIR)/RoadPainter.a
SPARROWHAWKLIB      = $(LIBDIR)/SparrowHawk.a

# util include makefiles - must have UTILNAME_PATH and UTILNAME_DEPEND defined 
# within
include $(DGC)/util/cMap/CMap.mk
include $(DGC)/util/corridorPainter/CCorridorPainter.mk
include $(DGC)/util/costPainter/CCostPainter.mk
include $(DGC)/util/pid/Cpid.mk
include $(DGC)/util/moduleHelpers/DGCutils.mk
include $(DGC)/util/controllerUtils/force/find_force.mk
include $(DGC)/util/frames/frames.mk
include $(DGC)/util/latlong/latlong.mk
include $(DGC)/util/mapDisplay/MapDisplay.mk
include $(DGC)/util/moduleHelpers/ModuleHelpers.mk
include $(DGC)/util/pidController/PID_Controller.mk
include $(DGC)/util/planner/planner.mk
include $(DGC)/util/RDDF/Rddf.mk
include $(DGC)/util/skynet/skynet.mk
include $(DGC)/util/sparrow/sparrow.mk
include $(DGC)/util/stereoProcess/stereoProcess.mk
include $(DGC)/util/traj/traj.mk
include $(DGC)/util/geometry/geometry.mk
include $(DGC)/util/CTrajPainter/CTrajPainter.mk
include $(DGC)/util/RoadPainter/RoadPainter.mk
include $(DGC)/util/profiler/profiler.mk
include $(DGC)/util/SparrowHawk/SparrowHawk.mk

# Module binary/library locations
ADRIVELIB           = $(LIBDIR)/adrive.a
ASTATELIB           = $(LIBDIR)/astate.a
FUSIONMAPPERLIB     = $(LIBDIR)/fusionMapper.a
GUILIB              = $(LIBDIR)/gui.a
LADARFEEDERBIN      = $(BINDIR)/ladarFeeder
MODULESTARTERLIB    = $(LIBDIR)/moduleStarter.a
PLANNERMODULELIB    = $(LIBDIR)/plannerModule.a
RDDFPATHGENLIB      = $(LIBDIR)/rddfPathGen.a
SIMULATORLIB        = $(LIBDIR)/simulator.a
SIMULATORBIN        = $(BINDIR)/simulator
RDDFPATHGENBIN      = $(BINDIR)/rddfPathGen
PLANNERMODULEBIN    = $(BINDIR)/plannerModule
STEREOFEEDERBIN     = $(BINDIR)/stereoFeeder
TRAJFOLLOWERLIB     = $(LIBDIR)/trajFollower.a
ROADFINDINGBIN      = $(BINDIR)/road
LOGPLAYERBIN        = $(BINDIR)/logplayer
LOADSPEWBIN         = $(BINDIR)/loadspewbin
NLPSOLVERLIB        = $(LIBDIR)/libnlpsolver.a
STATICPAINTERLIB    = $(LIBDIR)/StaticPainter.a
SUPERCONBIN         = $(BINDIR)/superCon
SUPERCONLIB         = $(LIBDIR)/superCon.a

# module include makefiles - must have MODULENAME_PATH and MODULENAME_DEPEND 
# defined within
include $(DGC)/modules/adrive/adrive.mk
include $(DGC)/modules/astate/astate.mk
include $(DGC)/modules/fusionMapper/fusionMapper.mk
include $(DGC)/modules/gui/gui.mk
include $(DGC)/modules/ladarFeeder/ladarFeeder.mk
include $(DGC)/modules/moduleStarter/moduleStarter.mk
include $(DGC)/modules/plannerModule/plannerModule.mk #Must come after planner
include $(DGC)/modules/rddfPathGen/rddfPathGen.mk
include $(DGC)/modules/simulator/simulator.mk # This needs to come after adrive.
include $(DGC)/modules/stereoFeeder/stereoFeeder.mk
include $(DGC)/modules/trajFollower/trajFollower.mk
include $(DGC)/util/gazebo/clients/LadarClient/ladarClient.mk
include $(DGC)/util/gazebo/clients/FakeState/FakeState.mk
include $(DGC)/modules/roadFinding/roadFinding.mk
include $(DGC)/util/logplayer/logplayer.mk
include $(DGC)/modules/StaticPainter/StaticPainter.mk
include $(DGC)/util/elevationFuser/elevationFuser.mk
include $(DGC)/projects/superCon/superCon.mk

# other library locations
TIMBERLIB           = $(LIBDIR)/timber.a
REACTIVEPLANNERLIB  = $(LIBDIR)/reactivePlanner.a

# other include makefiles
include $(DGC)/modules/timber/timber.mk
include $(DGC)/projects/reactivePlanner/reactivePlanner.mk

# DGC Global Makefile Rules
first: folders

folders: $(INCLUDEDIR) $(LIBDIR) $(BINDIR) $(LOGSDIR)

$(LADARFEEDERBIN): $(LADARFEEDER_DEPEND)
	$(MAKE) install -C $(LADARFEEDER_PATH)

$(LADARDRIVERSLIB): $(LADARDRIVERS_DEPEND)
	$(MAKE) install -C $(LADARDRIVERS_PATH)

$(STEREOPROCESSLIB): $(STEREOPROCESS_DEPEND)
	$(MAKE) install -C $(STEREOPROCESS_PATH)

$(STEREOSOURCELIB): $(STEREOSOURCE_DEPEND)
	$(MAKE) install -C $(STEREOSOURCE_PATH)

$(STEREOFEEDERBIN): $(STEREOFEEDER_DEPEND)
	$(MAKE) install -C $(STEREOFEEDER_PATH)

$(GGISLIB): $(LATLONG_DEPEND)
	$(MAKE) install -C $(LATLONG_PATH)

$(ADRIVEBIN): $(ADRIVELIB) 
	$(MAKE) install -C  $(ADRIVE_PATH)

$(ADRIVELIB): $(ADRIVE_DEPEND)
	$(MAKE) install -C $(ADRIVE_PATH)

$(ASTATELIB): $(ASTATE_DEPEND)
	$(MAKE) install -C  $(ASTATE_PATH)

$(IMU_FASTCOMLIB): $(IMU_FASTCOM_DEPEND)
	$(MAKE) install -C $(IMU_FASTCOM_PATH)

$(PLANNERMODULELIB): $(PLANNERMODULE_DEPEND)
	$(MAKE) install -C $(PLANNERMODULE_PATH)

$(PLANNERLIB): $(PLANNER_DEPEND)
	$(MAKE) install -C  $(PLANNER_PATH)

$(SIMULATORLIB): $(SIMULATOR_DEPEND)
	$(MAKE) install -C $(SIMULATOR_PATH)

$(FRAMESLIB): $(FRAMES_DEPEND)
	$(MAKE) install -C  $(FRAMES_PATH)

$(GUILIB): $(GUI_DEPEND)
	$(MAKE) install -C $(GUI_PATH)

$(MODULESTARTERLIB): $(MODULESTARTER_DEPEND)
	$(MAKE) install -C $(MODULESTARTER_PATH)

$(MAPDISPLAYLIB): $(MAPDISPLAY_DEPEND)
	$(MAKE) install -C $(MAPDISPLAY_PATH)

$(FUSIONMAPPERLIB): $(FUSIONMAPPER_DEPEND)
	$(MAKE) install -C $(FUSIONMAPPER_PATH)

$(TRAJFOLLOWERLIB): $(TRAJFOLLOWER_DEPEND)
	$(MAKE) install -C $(TRAJFOLLOWER_PATH)

$(CPIDLIB): $(CPID_DEPEND)
	$(MAKE) install -C $(CPID_PATH)

$(PIDCONTROLLERLIB): $(PIDCONTROLLER_DEPEND)
	$(MAKE) install -C $(PIDCONTROLLER_PATH)

$(FINDFORCELIB): $(FINDFORCE_DEPEND)
	$(MAKE) install -C $(FINDFORCE_PATH)

$(CMAPLIB): $(CMAP_DEPEND)
	$(MAKE) install -C $(CMAP_PATH)

$(CMAPPLIB): $(CMAP_DEPEND)
	$(MAKE) install -C $(CMAP_PATH)

$(RDDFLIB): $(RDDF_DEPEND)
	$(MAKE) install -C  $(RDDF_PATH)

$(CCPAINTLIB): $(CCORRIDORPAINTER_DEPEND)
	$(MAKE) install -C $(CCORRIDORPAINTER_PATH)

$(CCOSTPAINTLIB): $(CCOSTPAINTER_DEPEND)
	$(MAKE) install -C $(CCOSTPAINTER_PATH)

$(SDSLIB): $(SDS_DEPEND)
	$(MAKE) install -C $(SDS_PATH)

$(MODULEHELPERSLIB): $(MODULEHELPERS_DEPEND)
	$(MAKE) install -C $(MODULEHELPERS_PATH)

$(DGCUTILS): $(DGCUTILS_DEPEND)
	$(MAKE) install -C $(DGCUTILS_PATH)

$(SKYNETLIB): $(SKYNET_DEPEND)
	$(MAKE) install -C $(SKYNET_PATH)

$(TRAJLIB): $(TRAJ_DEPEND)
	$(MAKE) install -C $(TRAJ_PATH)

$(ROADFINDINGBIN): $(UDROAD_DEPEND)
	$(MAKE) install -C $(UDROAD_PATH)

$(RDDFPATHGENLIB): $(RDDFPATHGEN_DEPEND)
	cd $(RDDFPATHGEN_PATH) && $(MAKE)

$(RPGSEEDERHDR): $(RPGSEEDERHDR_DEPEND)
	cd $(RPGSEEDERHDR_PATH) && $(MAKE) headers

$(SPARROWLIB): $(SPARROW_DEPEND)
	$(MAKE) install -C $(SPARROW_PATH)

$(TIMBERLIB): $(TIMBER_DEPEND)
	cd $(TIMBER_PATH) && $(MAKE) 
# installs by default

$(CDD): $(SPARROWLIB)

$(INCLUDEDIR):
	cd $(DGC) && mkdir include

$(LIBDIR):
	cd $(DGC) && mkdir lib

$(BINDIR):
	cd $(DGC) && mkdir bin

$(ALICELIB): $(ALICE_DEPEND)
	$(MAKE) install -C $(ALICE_PATH)

$(REACTIVEPLANNERLIB): $(REACTIVEPLANNER_DEPEND)
	$(MAKE) install -C $(REACTIVEPLANNER_PATH)

$(LADARSOURCELIB): $(LADARSOURCE_DEPEND)
	$(MAKE) install -C $(LADARSOURCE_PATH)

$(STATICPAINTERLIB): $(STATICPAINTER_DEPEND)
	$(MAKE) install -C $(STATICPAINTER_PATH)

$(LADARCLIENTLIB): $(LADARCLIENT_DEPEND)
	$(MAKE) install -C $(LADARCLIENT_PATH)

$(LOGPLAYERBIN): $(LOGPLAYER_DEPEND)
	$(MAKE) install -C $(LOGPLAYER_PATH)

$(LOADSPEWBIN): $(LOGPLAYER_DEPEND)
	$(MAKE) install -C $(LOGPLAYER_PATH)

$(NLPSOLVERLIB): /usr/lib/libsnopt.a
	$(INSTALL_DATA)s $< $(LIBDIR)/libnlpsolver.a

$(GPSLIB): $(GPS_DEPEND)
	$(MAKE) install -C  $(GPS_PATH)

$(CTRAJPAINTERLIB): $(CTRAJPAINTER_DEPEND)
	$(MAKE) install -C $(CTRAJPAINTER_PATH)

$(GEOMETRYLIB): $(GEOMETRY_DEPEND)
	$(MAKE) install -C  $(GEOMETRY_PATH)

$(CELEVFUSERLIB): $(CELEVATIONFUSER_DEPEND)
	$(MAKE) install -C $(CELEVATIONFUSER_PATH)

$(PROFILERLIB): $(PROFILER_DEPEND)
	$(MAKE) install -C  $(PROFILER_PATH)

$(ROADPAINTERLIB): $(ROADPAINTER_DEPEND)
	$(MAKE) install -C $(ROADPAINTER_PATH)

$(SPARROWHAWKLIB): $(SPARROWHAWK_DEPEND)
	$(MAKE) install -C $(SPARROWHAWK_PATH)

$(SUPERCONBIN): $(SUPERCON_DEPEND)
	cd $(SUPERCON_PATH) && $(MAKE) install

$(SUPERCONLIB): $(SUPERCON_DEPEND)
	$(MAKE) install_lib -C $(SUPERCON_PATH)

$(SIMULATORBIN): $(SIMULATOR_DEPEND)
	$(MAKE) install -C $(SIMULATOR_PATH)

$(PLANNERMODULEBIN): $(PLANNERMODULE_DEPEND)
	$(MAKE) install -C $(PLANNERMODULE_PATH)

$(RDDFPATHGENBIN): $(RDDFPATHGEN_DEPEND)
	cd $(RDDFPATHGEN_PATH) && $(MAKE)

