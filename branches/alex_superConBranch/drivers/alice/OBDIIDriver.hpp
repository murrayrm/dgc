///////////////////////////////////////////////////////////////
//  OBDIIDriver.h
//   
//////////////////////////////////////////////////////////////

#ifndef OBDIIDRIVER_HPP
#define OBDIIDRIVER_HPP

#include "SerialDevice.hpp"

#include <math.h>
#include <unistd.h>
#include <string>
#include <cassert>
#include <fstream>

#define DEBUG_DRIVER false

#define PI 3.14159265
#define LEFT_MOTOR  0x00
#define RIGHT_MOTOR  0x01
#define WHEELBASE 380.0
#define TICK_PER_MM 173.4
#define MAXVEL 173400
#define TENTHS_PER_SEC 10

/////////////////////////////////////////////////////////////
// OBDIIDriver CLASS DEFINITION
/////////////////////////////////////////////////////////////

class OBDIIDriver {
public:
  // Variables 
 
  //Functions
  OBDIIDriver() ;
  ~OBDIIDriver()  {}
  int connect(const string& device) ;
  void disconnect() ;
  int init();
  int getRPM();
  int getTimeSinceEngineStart();//seconds
  int getVehicleSpeed();//mps
  float getEngineCoolantTemp();//degrees F
  int getTorque();//footpounds
  int getGlowPlugLampTime();//seconds
  int getThrottlePosition();//%
  float getCurrentGearRatio();
  float getBatteryVoltage();//Volts
  bool GlowPlugLightOn();//UNTESTED
  int getCurrentGear();//1=first, 2=second, etc.  This is only useful when getTransmissionPosition() returns 'D'
  char getTransmissionPosition();//P=park, R=reverse, N=neutral, D=drive
  float getTorqueConvClutchDutyCycle();//% INOP
  int getTorqueConvControlState();//0=TCC not controlled by OSC, 1=TCC unlocked, 2=TCC locked UNTESTED
  void clearDTCs();//INOP
  void requestDTCs();//INOP


  string readport() {string out = _serialport.readport();return out;};

  string sendcommand(const string& commandstring) ;
  void printstring(const string& s);
  unsigned char checksum(const string & s); 
  int parseresponse(const string & response, int expectedlength, bool twobyte) ;
  char parseSignedResponse(const string & response);
  string parsebitmappedresponse(const string & response, int expectedlength);
  int parsetwobytesignedresponse(const string & response);
  string buildcommand(char command) ;
  string buildtwobytecommand(char commandMSB, char commandLSB);
  string buildDTCcommand(char mode);

private:
  // Variables
	SerialDevice _serialport;
	//Functions
	int getAPP_D();
	int getAPP_E();
	int getAPP_F();
};

#endif
