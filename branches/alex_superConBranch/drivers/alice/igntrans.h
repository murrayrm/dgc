// this file contains the function defintions for trans.c
#ifndef IGNTRANS_H
#define IGNTRANS_H

#define IGNTRANS_SERIAL

#ifndef IGNTRANS_SERIAL
#define IGNTRANS_SDS
#endif

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>




#define TRUE 1
#define FALSE 0
#define ERROR -1

//transmission position states
#define PARK 0
#define REVERSE 1
#define NEUTRAL 2
#define DRIVE 3

//ignition position states
#define OFF 0
#define RUN 1
#define START 2

//led position states
#define ON 1

#define LED1 1
#define LED2 2
#define LED3 3
#define LED4 4

//transmission position outputs
#define TRANS_PARK 'P'
#define TRANS_REVERSE 'R'
#define TRANS_NEUTRAL 'N'
#define TRANS_DRIVE 'D'

//ignition position outputs
#define IGN_OFF 'O'
#define IGN_RUN 'R'
#define IGN_START 'S'

// led position outputs
#define LED_ON 'N'
#define LED_OFF 'O'

// communication directions
#define SEND 'S'
#define RECEIVE 'R'

//device types
#define IGN 'I'
#define TRANS 'T'
#define LED_I '1'
#define LED_II '2'
#define LED_III '3'
#define LED_IV '4'
#define NONE 'N'

#define DEVICE_TYPE 0
#define DEVICE_DIRECTION 1
#define DEVICE_DATA 2
#define RECEIVED_POSITION 1
#define RECEIVE_EOL_POS 2
#define SEND_EOL_POS 3

#define EOL '\n'

#define SET_POS_LEN 4
#define GET_POS_LEN 3


int igntrans_open(int port);
void simulator_igntrans_open(int port);
int igntrans_close(void);
int ign_setposition(int position);
int ign_getposition(void);
int trans_setposition(int position);
int trans_getposition(void);
int led_setposition(int device, int port);
int led_getposition(int device);



#endif
