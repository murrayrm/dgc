// header file for eatop input

#define ESTOP_INPUT_LENGTH 1
#define ESTOP_BIG_INPUT_LENGTH 300

#define RUN_CHAR 'R'
#define PAUSE_CHAR 'P'
#define DISABLE_CHAR 'D'

#define RUN 2
#define PAUSE 1
#define DISABLE 0



int estop_open(int);
void simulator_estop_open(int);
int estop_close(void);
int estop_status(void);


//Chose Serial or SDSD
//#define ESTOP_SERIAL

#ifndef ESTOP_SERIAL
#define ESTOP_SDS
#endif //not SERIAL
