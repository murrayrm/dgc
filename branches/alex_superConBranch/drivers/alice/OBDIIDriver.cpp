#include "OBDIIDriver.hpp"

///////////////////////////////////////////////////////////////////////////
//  Function: OBDIIdriver (Constructor) 
//  Inputs: None 
//  Outputs: None
//  Error Checking: None 
//  Summary: Opens the serial port and initializes it
///////////////////////////////////////////////////////////////////////////

OBDIIDriver::OBDIIDriver() 
{
     
  
  assert(sizeof(int) >=4);
  assert (sizeof(short) >= 2);

}

int OBDIIDriver::connect(const string& device) 
{
	//currently SerialDevice is hard coded to 19200 so the baud value passed has no effect
	int val = _serialport.initport(device,19200);
	
	return val;
}

void OBDIIDriver::disconnect() 
{
 
	_serialport.closeport();
 
}

unsigned char OBDIIDriver::checksum(const string & s)
{
	int length = 	s.length();
	int sum = 0;
	unsigned char twoscomp;
  unsigned char checksum = 0x00;

	for (int i = 1; i < (length-1); ++i){
		checksum = checksum + s[i];
		sum = sum + (int)s[i];
}
	checksum = (checksum)&0xff;
	return (checksum);
	
}


int OBDIIDriver::parseresponse(const string & response, int expectedlength, bool twobyte) 
{
	// INIT RESPONSES from CAN bus 
	
	// 40 | 80 | 57 | 16 | 7f | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 6c |
	
	// 40 | 80 | 57 | 16 | 7f | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 6c |

	// these all come at once as a long 42 byte string
	// 40 | 88 | 06 | 41 | 00 | 98 | 3b | 00 | 17 | 00 | 00 | 00 | 00 | b9 | 
	// 40 | 88 | 06 | 41 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | cf | 
	// 40 | 88 | 06 | 41 | 00 | 98 | 18 | 00 | 13 | 00 | 00 | 00 | 00 | 92 |

	// these are received together as a 28 byte string
	// 40 | 88 | 06 | 41 | 01 | 00 | 04 | 00 | 00 | 00 | 00 | 00 | 00 | d4 |
	// 40 | 88 | 06 | 41 | 01 | 81 | 05 | 80 | 00 | 00 | 00 | 00 | 00 | d6 |

	// these are received together as a 28 byte string
	// 40 | 88 | 07 | 42 | 00 | 00 | d8 | 18 | 00 | 03 | 00 | 00 | 00 | c4 | 
	// 40 | 88 | 07 | 42 | 00 | 00 | d8 | 3b | 00 | 07 | 00 | 00 | 00 | eb |

	// SAMPLE QUERY COMMAND RESPONSES
	// 40 | 88 | 07 | 42 | 00 | 00 | d8 | 3b | 00 | 07 | 00 | 00 | 00 | eb | 
	// 40 | 88 | 07 | 42 | 00 | 00 | d8 | 18 | 00 | 03 | 00 | 00 | 00 | c4 |
	// 40 | 88 | 07 | 42 | 01 | 00 | 00 | 05 | 80 | 00 | 00 | 00 | 00 | 57 |
	// 40 | 88 | 05 | 42 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | d1 | 
	// 40 | 88 | 05 | 42 | 02 | 00 | 01 | 55 | 00 | 00 | 00 | 00 | 00 | 27 |
	// 40 | 88 | 05 | 42 | 02 | 00 | 01 | 55 | 00 | 00 | 00 | 00 | 00 | 27 | 
	// 40 | 88 | 05 | 42 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | d1 |

	// Sam's parsing guess :
	// byte 0 : packet header 1 (always 0x40)
	// byte 1 : packet header 2 (0x88 for all commands, 0x80 for some init commands)
	// byte 2 : packet header 3 (?)
	// byte 3 : packet header 4 (0x42 for all commands, 0x16 0x41 for some init commands)
	// byte 4 : packet header 5 COMMAND TYPE?
	// byte 5 : packet header 6 (0x00 for all commands, 0x7f 0x01 for some init commands)
	// byte 6-12 : data bytes (possibly parsed as multiple numbers or as a single sum)
	// byte 13 : checksum (sum of bytes 2-13)
  
	int length = response.length();
	int i = 0;
 	int startbyte;
	long dataval = 0;
	long deltaval = 0;
	string correctedresponse;
	//if (DEBUG_DRIVER) cout << "response length = " << length << endl;
	if (length >=14){
		
		
		unsigned char headerbyte_a = response[0];
		unsigned char headerbyte_b = response[1];
		unsigned char headerbyte_c = response[2];
		unsigned char headerbyte_d = response[3];
		unsigned char commandbyte = response[4];

		unsigned char checkbyte = response[13];
		unsigned char checksum = '\0';
		

		// sum bytes for checksum
		for (i = 1 ; i < 13 ; ++i)
    {
			checksum = checksum+response[i];
		}
		checksum = checksum & '\xff';
		if (checkbyte != checksum)
    {
		  //cout << "in OBDIIDriver.cpp : bad checksum" << endl;	
    }
    
 		if(twobyte == true)
    {
			startbyte = 6;
    }
	 	else
    {
			startbyte = 5;
    }
		
		if(response[3] == 0x7f && response.length() > 14)
    {
			correctedresponse = response.c_str()+14;
    }
		else
    {
			correctedresponse = response;
    }
    
		// sum all data bytes (may need changing for two number responses)
		int bytenum = 0;
		for (i = startbyte+expectedlength-1 ; i >= startbyte; --i)
    {
			deltaval =  ((long)(correctedresponse[i])&0xff)<<(8*(bytenum));
			dataval = dataval +deltaval;

			bytenum = bytenum+1;
			//cout << "dval = " << dataval << "  delta = "  << deltaval <<"   bytenum = " << bytenum << endl;
		}
		


		// Insert more checking/parsing here on header bytes
		// maybe parse those extra long init strings as well
  }
	else {
	  //cout << "in OBDIIDriver.cpp : bad response size = " << length << endl; 
	}
  return dataval;
}

char OBDIIDriver::parseSignedResponse(const string & response)
{
	return response[5];
}

string OBDIIDriver::parsebitmappedresponse(const string & response, int expectedlength)
{
	string requestedresponse(1,response[6]);
	return requestedresponse;
}

int OBDIIDriver::parsetwobytesignedresponse(const string & response)
{
  unsigned char msb = response[6];
  unsigned char lsb = response[7];
  signed short res = (signed short)((((unsigned short)msb) << 8) | ((unsigned short)lsb));

  return res;
}


string OBDIIDriver::buildtwobytecommand(char commandMSB, char commandLSB)
{
  // INIT 	
	// 70 | a2 | fb | e0 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 7d |
	// 70 | a6 | fd | 00 | fd | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | a0 |
	// 70 | a0 | 08 | 02 | 01 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ab |
	// 70 | a0 | 08 | 02 | 01 | 01 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ac |
	// 70 | a0 | 08 | 03 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ad |
	// SOME COMMANDS	
	// 70 | a0 | 08 | 03 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ad |
	// 70 | a0 | 08 | 03 | 02 | 01 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ae |
	// 70 | a0 | 08 | 03 | 02 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | af |
	// 70 | a0 | 08 | 03 | 02 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | af |
	// 70 | a0 | 08 | 03 | 02 | 04 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | b1 |
	// 70 | a0 | 08 | 03 | 02 | 04 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | b1 |
	// 70 | a0 | 08 | 03 | 02 | 05 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | b2 |
	// 70 | a0 | 08 | 03 | 02 | 05 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | b2 |

  string outstring(15,0x00);

  outstring[0] = 0x70;
	outstring[1] = 0xa0;
	outstring[2] = 0x08;
	outstring[3] = 0x03;
	//outstring[4] = 0x01;
	outstring[4] = 0x22;
	outstring[5] = commandMSB;
	outstring[6] = commandLSB;


	unsigned char csum = checksum(outstring);
	outstring[14] = csum;
  
  return outstring;
} 

string OBDIIDriver::buildDTCcommand(char mode)
{
	string outstring(15,0x00);

	outstring[0] = 0x70;
	outstring[1] = 0xa0;
	outstring[2] = 0x08;
	outstring[3] = 0x01;
	outstring[4] = mode;

	unsigned char csum = checksum(outstring);
	outstring[14] = csum;

	return outstring;
}

string OBDIIDriver::buildcommand(char command) //identical to buildtwobytecommand except for contents of outstring[4]
{
	string outstring(15,0x00);

	outstring[0] = 0x70;
	outstring[1] = 0xa0;
	outstring[2] = 0x08;
	outstring[3] = 0x02;
	outstring[4] = 0x01;
	//outstring[4] = 0x22;
	outstring[5] = command;

	unsigned char csum = checksum(outstring);
	outstring[14] = csum;

	return outstring;
}


void OBDIIDriver::printstring(const string& s) {
	int thischar, j;
	int thissize = s.size();
	for (j = 0; j < (thissize);j++){
		thischar = (int)s[j];
		thischar = thischar&0xff;
		if (thischar <= 0x0f)
			cout << hex << "0" << (int)thischar <<" | "; 
		else
			cout << hex << (int) thischar <<" | "; 
		
		
	}
	if (thissize) cout << dec<< endl;
}


string OBDIIDriver::sendcommand(const string& commandstring) 
{
  string out;
  _serialport.writeport(commandstring);
	
	out = _serialport.readport();

	if (DEBUG_DRIVER) {
	  //		cout << "out = " ;
	  //printstring(commandstring);
	  //cout << "in = " ;
	  //printstring(out);
	  //cout << endl;
	}
	return out;    
}

int OBDIIDriver::init()
{
		string input;		
		string s1(15,0x00);
		string s2(15,0x00);
		string s3(15,0x00);
		string s4(15,0x00);
		string s5(15,0x00);
	//70 | a2 | fb | e0 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 7d |
		s1[0] = 0x70;		
		s1[1] = 0xa2;
		s1[2] = 0xfb;
		s1[3] = 0xe0;
		s1[14] = 0x7d;

		//70 | a6 | fd | 00 | fd | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | a0 |
		s2[0] = 0x70;		
		s2[1] = 0xa6;
		s2[2] = 0xfd;
		s2[3] = 0x00;
		s2[4] = 0xfd;
		s2[14] = 0xa0;

	//70 | a0 | 08 | 02 | 01 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ab |
		s3[0] = 0x70;		
		s3[1] = 0xa0;
		s3[2] = 0x08;
		s3[3] = 0x02;
		s3[4] = 0x01;
		s3[14] = 0xab;

		//70 | a0 | 08 | 02 | 01 | 01 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ac |
		s4[0] = 0x70;		
		s4[1] = 0xa0;
		s4[2] = 0x08;
		s4[3] = 0x02;
		s4[4] = 0x01;
		s4[5] = 0x01;
		s4[14] = 0xac;

		//70 | a0 | 08 | 03 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | ad |
		s5[0] = 0x70;		
		s5[1] = 0xa0;
		s5[2] = 0x08;
		s5[3] = 0x03;
		s5[4] = 0x02;
		s5[14] = 0xad;
		
		input = sendcommand(s1);
		input = sendcommand(s2);
		input = sendcommand(s3);
		input = sendcommand(s4);
		input = sendcommand(s5);
		
		return input.size();
		// needs some better error checking by comparing with the expected init response strings
		// now just returning last response size which should be 28
}


// Sample function (edit parse response if the val isn't assembled correctly);
int OBDIIDriver::getRPM() 
{
	string command = buildcommand(0x0c);
 	int val = parseresponse(sendcommand(command), 2, false)/4;
	return val;
}

int OBDIIDriver::getTimeSinceEngineStart()
{
	string command = buildcommand(0x1f);
	int val = parseresponse(sendcommand(command), 2, false);
	return val;
}

int OBDIIDriver::getVehicleSpeed()
{
	string command = buildcommand(0x0d);
	int val = (parseresponse(sendcommand(command), 1, false)*1000)/3600;
	return val;
}

float OBDIIDriver::getEngineCoolantTemp()
{
	string command = buildcommand(0x05);
	int val = parseresponse(sendcommand(command), 1, false)-40;
	float Fahrenheitval = 1.8*(float)val + 32.0;
	return Fahrenheitval;
}

int OBDIIDriver::getThrottlePosition()
{
	string command = buildtwobytecommand(0x09,0xd4);
	int val = parseresponse(sendcommand(command), 1, true) / 2;
	return val;
}

int OBDIIDriver::getTorque()
{
	string command = buildtwobytecommand(0x09,0xcb);
	int val = parsetwobytesignedresponse(sendcommand(command));
	return val;
}

int OBDIIDriver::getGlowPlugLampTime()
{
	string command = buildtwobytecommand(0x14,0x51);
	int val = parseresponse(sendcommand(command), 2, true);
	return val;
}

float OBDIIDriver::getCurrentGearRatio()
{
	string command = buildtwobytecommand(0x11,0xb7);
	float val = parseresponse(sendcommand(command), 2, true)/16384.0;
	return val;
}

float OBDIIDriver::getBatteryVoltage()
{
  string command = buildtwobytecommand(0x11,0x72);
  float val = parseresponse(sendcommand(command), 1, true)/16.0;
  return val;
}

void OBDIIDriver::clearDTCs()
{
	string command = buildDTCcommand(0x14);
	sendcommand(command);

	return;
}

void OBDIIDriver::requestDTCs()
{
	string command = buildDTCcommand(0x13);
	sendcommand(command);

	return;
}

bool OBDIIDriver::GlowPlugLightOn()
{
	string command = buildtwobytecommand(0x16,0x67);
	string response = parsebitmappedresponse(sendcommand(command), 1);
	bool val = response[1] & 0x20;
	return val;
}

int OBDIIDriver::getCurrentGear()
{
  string command = buildtwobytecommand(0x11, 0xb3);
  int val = parseresponse(sendcommand(command), 1, true) / 2;
  return val;
}

char OBDIIDriver::getTransmissionPosition()
{
  string command = buildtwobytecommand(0x11, 0xb6);
  char val;
  int tempval = parseresponse(sendcommand(command), 1, true);
  if(tempval / 2 == 70) val = 'P';
  else if(tempval / 2 == 60) val = 'R';
  else if(tempval / 2  == 50) val = 'N';
  else val = 'D';

  return val;
}

float OBDIIDriver::getTorqueConvClutchDutyCycle()
{
  string command = buildtwobytecommand(0x11, 0xb0);
  float val = parseresponse(sendcommand(command), 2, true);//need to do some math on result to get meaningful data
  return val;
}

int OBDIIDriver::getTorqueConvControlState()
{
  string command = buildtwobytecommand(0x09, 0x71);
  int val = parseresponse(sendcommand(command), 1, true);
  return val;
}
