/******************************************************************
 **
 **  test.cpp
 **
 **    Time-stamp: <2003-06-13 14:12:11 evo>
 **
 **    Author: Sam Pfister
 **    Created: Thu May 22 21:53:37 2003
 **
 **
 ******************************************************************
 **
 **
 **
 ******************************************************************/
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <math.h>
#include "OBDIIDriver.hpp"


using namespace std;
int main(int argc, char* argv[])
{
 

	OBDIIDriver obd;
		

	static char port_A[] = "/dev/ttyS0";
	
 	cout << "(from test.cpp) connecting " << endl;	

	if (obd.connect(port_A) < 1){
		cout << "(from test.cpp) Unable to open serial port "<< port_A << endl;
	}
	else{
		cout << "(from test.cpp) Serial port " << port_A << " opened"<< endl;
	



		int val = 0;
		long retval = 0;
		cout << "init : " << endl;
		val = obd.init();
		cout << "return val = " << val << endl;
		
		/*	cout << "getRPM" << endl;
		retval = obd.getRPM();
		cout << "return val = " << retval << endl;
		
		cout << "**********************************" << endl;
		
		cout << "getTimeSinceEngineStart" << endl;
		retval = obd.getTimeSinceEngineStart();
		cout << "return val = " << retval << endl;

		cout << "**********************************" << endl;
		
		cout << "getVehicleSpeed" << endl;
		retval = obd.getVehicleSpeed();
		cout << "return val = " << retval << endl;
		
		cout << "**********************************" << endl;
		
		cout << "getEngineCoolantTemp" << endl;
		float thisval = obd.getEngineCoolantTemp();
		cout << "return val = " << thisval << endl;
		
		cout << "**********************************" << endl;

		cout << "getThrottlePosition" << endl;
	  retval = obd.getThrottlePosition();
		cout << "return val = " << retval << endl;
		
		cout << "**********************************" << endl;

		cout << "getTorque" << endl;
		retval = obd.getTorque();
		cout << "return val = " << retval << endl;

		cout << "**********************************" << endl;

		cout << "getGlowPlugLampTime" << endl;
		retval = obd.getGlowPlugLampTime();
		cout << "return val = " << retval << endl;

		cout << "**********************************" << endl;
		
		cout << "getCurrentGearRatio" << endl;
		retval = obd.getCurrentGearRatio();
		cout << "return val = " << retval << endl;

		cout << "**********************************" << endl;

		cout << "GlowPlugLightOn?" << endl;
		retval = obd.GlowPlugLightOn();
		cout << "return val = " << retval << endl;

		cout << "**********************************" << endl;

		cout << "Request DTCs" << endl;
		obd.requestDTCs();

		cout << "clear DTCs" << endl;
		obd.clearDTCs();
		
		cout << "**********************************" << endl;
		
		cout << "getCurrentGear" << endl;
		retval = obd.getCurrentGear();
		cout << "return val = " << retval << endl;

		cout << "**********************************" << endl;
		
		cout << "getTransmissionPosition" << endl;
		char result = obd.getTransmissionPosition();
		cout << "return val = " << result << endl;
		
		cout << "**********************************" << endl;
		*/
		cout << "getTorqueConvClutchDutyCycle" << endl;
		float thisval = obd.getTorqueConvClutchDutyCycle();
		cout << "return val = " << thisval << endl;

		cout << "**********************************" << endl;

		cout << "getTorqueConvControlState" << endl;
		retval = obd.getTorqueConvControlState();
		cout << "return val = " << retval << endl;

cout << "**********************************" << endl;

		cout << "getBatteryVoltage" << endl;
		thisval = obd.getBatteryVoltage();
		cout << "return val = " << thisval << endl;

	}
	return(1);
}
