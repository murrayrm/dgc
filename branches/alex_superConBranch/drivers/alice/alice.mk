ALICE_PATH = $(DGC)/drivers/alice

ALICE_DEPEND_SOURCES = \
	$(ALICE_PATH)/adrive_test.c \
	$(ALICE_PATH)/brake.c \
	$(ALICE_PATH)/brake.h \
	$(ALICE_PATH)/estop.c \
	$(ALICE_PATH)/estop.h \
	$(ALICE_PATH)/parker_steer.cc \
	$(ALICE_PATH)/parker_steer.h \
	$(ALICE_PATH)/throttle.c \
	$(ALICE_PATH)/throttle.h \
	$(ALICE_PATH)/OBDIIDriver.hpp \
	$(ALICE_PATH)/OBDIIDriver.cpp \
	$(ALICE_PATH)/SerialDevice.hpp \
	$(ALICE_PATH)/SerialDevice.cpp

ALICE_DEPEND_LIBS = 

ALICE_DEPEND = $(ALICE_DEPEND_SOURCES) $(ALICE_DEPEND_LIBS)
