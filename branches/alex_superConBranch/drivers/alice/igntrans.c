// this files contains the drivers for Alice's ignition, transmission, and
// rear status LEDs
#include "igntrans.h"

#include <iostream>
#include <sys/time.h>
#include "vehports.h"
#include "GlobalConstants.h"
#include "throttle.h"
#include "SDSPort.h"
#include "sparrow/dbglib.h"

#ifdef IGNTRANS_SERIAL
#include <sstream>
#include <SerialStream.h>
using namespace std;
using namespace LibSerial;
static SerialStream serial_port;
#endif

#ifdef IGNTRANS_SDS
SDSPort* igntransport;
int igntransPortOpen = 0;
extern char simulator_IP[];
int igntrans_serial_port = -1;  //not a valid serial port number, must assign
#endif //IGNTRANS_SDS



/*************************************************************************
 * int igntrans_open(int port)
 *************************************************************************/
int igntrans_open(int port) {
    int errval = 0;

#ifdef IGNTRANS_SERIAL
  ostringstream portname;
  portname << "/dev/ttyS" << port;
  
  serial_port.Open(portname.str());
  if ( ! serial_port )
    {
      cerr << "igntrans_open() error: Could not open " << portname.str() << endl ;
      return FALSE;
    }
  
  //make sure that the error bit is clear before attempting initialization.
  if (! serial_port.good() ) {
    serial_port.clear();
  }
  
  serial_port.SetBaudRate( SerialStreamBuf::BAUD_9600 ) ;
  if ( ! serial_port.good() )
    {
      cerr << "igntrans_open() error: Could not set the baud rate." << endl ;
      errval++;
      //return FALSE;
    }
  serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
  if ( ! serial_port.good() )
    {
      cerr << "igntrans_open error: Could not disable the parity." << endl ;
      errval++;
      //return FALSE;
    }
  serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
  if ( ! serial_port.good() )
    {
      cerr << "igntrans_open error: Could not turn off hardware flow control." << endl;
      errval++;
      //return FALSE;
    }
  serial_port.SetNumOfStopBits( 1 ) ;
  if ( ! serial_port.good() )
    {
      cerr << "igntrans_open error: Could not set the number of stop bits."  << endl;
      errval++;
      //return FALSE;
    }
  serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8) ;
  if ( ! serial_port.good() )
    {
      cerr << "igntrans_open() error: Could not set the char size." << endl ;
      errval++;
      //return FALSE;
    }
  
  if ( errval > 0 ) return FALSE;
#endif //IGNTRANS_SERIAL
  
#ifdef IGNTRANS_SDS 
  if(igntransPortOpen == 0)
    {
      igntransport = new SDSPort("192.168.0.60", port);
      igntransPortOpen = 1;
      igntrans_serial_port = port;
    }
  
  igntransport->openPort();
  
  if(igntransport->isOpen() == 1)
    {
      igntransPortOpen = 1;
      igntrans_serial_port = port;
      //return TRUE;
    }
  else
    {
      return FALSE;
    }
#endif //IGNTRANS_SDS
  
  // perform a read to check that we are actually receiving good data
  if (ign_getposition() == -1) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}





/**************************************************************************
 * void simulator_igntrans_open(int)
 * Author: David Rosen
 * Functionality: Opens the igntrans port for the simulator
 * Returns: None
 **************************************************************************/
void simulator_igntrans_open(int port)
{
#ifdef IGNTRANS_SDS
  igntransport = new SDSPort(simulator_IP, port);
  igntransPortOpen = 1;
  igntrans_serial_port = port;
#endif //IGNTRANS_SDS
}



/**************************************************************************
 * int igntrans_close(void)
 * Author: Jeff Lamb
 * Functionality: Closes the igntrans port
 * Returns: TRUE  (1) on success
 *          FALSE (0) on failure
 **************************************************************************/
int igntrans_close(){
#ifdef IGNTRANS_SERIAL
  serial_port.Close();
  return TRUE;
#endif //IGNTRANS_serial
  
#ifdef IGNTRANS_SDS
  if(igntransport->closePort() == 0)
    {
      igntransPortOpen = 0;
      return TRUE;
    }
  else
    {
      return FALSE;
    }
#endif //IGNTRANS_SDS
}




/**************************************************************************
 * int trans_setposition(int position)
 **************************************************************************/
int trans_setposition(int position) {
  char *positionString;
  positionString[DEVICE_TYPE] = TRANS;
  positionString[DEVICE_DIRECTION] = SEND;
  positionString[SEND_EOL_POS] = EOL;
  if (position == PARK) positionString[DEVICE_DATA] = TRANS_PARK;
  else if (position == REVERSE) positionString[DEVICE_DATA] = TRANS_REVERSE;
  else if (position == NEUTRAL) positionString[DEVICE_DATA] = TRANS_NEUTRAL;
  else if (position == DRIVE) positionString[DEVICE_DATA] = TRANS_DRIVE;
  else return ERROR;
  if (!serial_port.write(positionString, SET_POS_LEN)) return ERROR;
  else return TRUE;
}


/**************************************************************************
 * int trans_getposition(void)
 **************************************************************************/
int trans_getposition(void) {
  char *positionString;
  char transPosition;
  positionString[DEVICE_TYPE] = TRANS;
  positionString[DEVICE_DIRECTION] = RECEIVE;
  positionString[RECEIVE_EOL_POS] = EOL;
  if (!serial_port.write(positionString, GET_POS_LEN)) return ERROR;

  if (!serial_port.getline(positionString, GET_POS_LEN, EOL)) return ERROR;
  if (positionString[DEVICE_TYPE] != TRANS) return ERROR;
  if (positionString[RECEIVED_POSITION] == TRANS_PARK) transPosition = PARK;
  else if (positionString[RECEIVED_POSITION] == TRANS_REVERSE) transPosition = REVERSE;
  else if (positionString[RECEIVED_POSITION] == TRANS_NEUTRAL) transPosition = NEUTRAL;
  else if (positionString[RECEIVED_POSITION] == TRANS_DRIVE) transPosition = DRIVE;
  else return ERROR;
  return transPosition;
}


/*****************************************************************************88
 * int ign_setposition(int position)
 ********************************************************************************/
int ign_setposition(int position) {
  char *positionString;
  positionString[DEVICE_TYPE] = IGN;
  positionString[DEVICE_DIRECTION] = SEND;
  positionString[SEND_EOL_POS] = EOL;
  if (position == OFF) positionString[DEVICE_DATA] = IGN_OFF;
  else if (position == RUN) positionString[DEVICE_DATA] = IGN_RUN;
  else if (position == START) positionString[DEVICE_DATA] = IGN_START;
  else return ERROR;
  if (!serial_port.write(positionString, SET_POS_LEN)) return ERROR;
  else return TRUE;
}


/**************************************************************************
 * int ign_getposition(void)
 **************************************************************************/
int ign_getposition(void) {
  char *positionString;
  char ignPosition;
  positionString[DEVICE_TYPE] = IGN;
  positionString[DEVICE_DIRECTION] = RECEIVE;
  positionString[RECEIVE_EOL_POS] = EOL;
  if (!serial_port.write(positionString, GET_POS_LEN)) return ERROR;

  if (!serial_port.getline(positionString, GET_POS_LEN, EOL)) return ERROR;
  if (positionString[DEVICE_TYPE] != IGN) return ERROR;
  if (positionString[RECEIVED_POSITION] == IGN_OFF) ignPosition = OFF;
  else if (positionString[RECEIVED_POSITION] == IGN_RUN) ignPosition = RUN;
  else if (positionString[RECEIVED_POSITION] == IGN_START) ignPosition = START;
  else return ERROR;
  return ignPosition;
}

  
/*****************************************************************************88
 * int led_setposition(itn device, int position)
 ********************************************************************************/
int led_setposition(int device, int position) {
  char *positionString;
  if (device == LED1) positionString[DEVICE_TYPE] = LED_I;
  else if (device == LED2) positionString[DEVICE_TYPE] = LED_II;
  else if (device == LED3) positionString[DEVICE_TYPE] = LED_III;
  else if (device == LED4) positionString[DEVICE_TYPE] = LED_IV;
  else return ERROR;

  positionString[DEVICE_DIRECTION] = SEND;
  
  positionString[SEND_EOL_POS] = EOL;
  
  if (position == OFF) positionString[DEVICE_DATA] = LED_OFF;
  else if (position == ON) positionString[DEVICE_DATA] = LED_ON;
  else return ERROR;
  if (!serial_port.write(positionString, SET_POS_LEN)) return ERROR;
  else return TRUE;
}


/**************************************************************************
 * int led_getposition(void)
 **************************************************************************/
int led_getposition(int device) {
  char *positionString;
  char ledPosition;
  if (device == LED1) positionString[DEVICE_TYPE] = LED_I;
  else if (device == LED2) positionString[DEVICE_TYPE] = LED_II;
  else if (device == LED3) positionString[DEVICE_TYPE] = LED_III;
  else if (device == LED4) positionString[DEVICE_TYPE] = LED_IV;
  else return ERROR;

  positionString[DEVICE_DIRECTION] = RECEIVE;
  positionString[RECEIVE_EOL_POS] = EOL;
  if (!serial_port.write(positionString, GET_POS_LEN)) return ERROR;

  if (!serial_port.getline(positionString, GET_POS_LEN, EOL)) return ERROR;

  if (positionString[RECEIVED_POSITION] == LED_OFF) ledPosition = OFF;
  else if (positionString[RECEIVED_POSITION] == LED_ON) ledPosition = ON;
  else return ERROR;

  return ledPosition;
}
  
