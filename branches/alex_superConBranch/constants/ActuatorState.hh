/* ActuatorState.hh - platform-independent actuator state struct
 *
 * Dima
 */

#ifndef ACTUATORSTATE_HH
#define ACTUATORSTATE_HH

#include <string.h>

struct ActuatorState
{
  /** Default constructor */
  ActuatorState()
	{
	  // initialize the state to zeros to appease the memory profilers
	  memset(this, 0, sizeof(*this));
	}
  
  // These are all in the [0,1] range
  //Steer
  int m_steerstatus; // 0 = off, 1 = on 
  double m_steerpos; // -1 to 1, left to right
  double m_steercmd;  //dito

  //Gas
  int m_gasstatus; // 0 = off, 1 = on 
  double m_gaspos;
  double m_gascmd;  

  //Brake
  int m_brakestatus; // 0 = off, 1 = on 
  double m_brakepos; // 0 to 1
  double m_brakecmd;  // 0 to 1
  double m_brakepressure; // 0 to 1

  //Estop
  int m_estopstatus; // 0 = off, 1 = on 
  // Something is pausing us.  
  // Poll this if you don't care why we're stopped
  int m_estoppos; // 0 or 1
  // Darpa Pause
  int m_dstoppos; // 0 or 1
  // Adrive error pause
  int m_astoppos; // 0 or 1
  // Controled Pause from software
  int m_cstoppos; // 0 or 1

  
  //Transmission
  int m_transstatus; // 0 = off, 1 = on 
  int m_transcmd; // 0 to 1
  int m_transpos; // 0 to 1

  //OBDII
  int m_obdiistatus; // 0 = off, 1 = on 
  /*! The RPM of the engine */
  int m_engineRPM;
  /*! The time since the engine started in seconds */
  int m_TimeSinceEngineStart;
  /*! Vehicle speed m/s */
  int m_VehicleWheelSpeed;
  /*! The temperature of the engine coolant. */
  float m_EngineCoolantTemp;
  /*! The wheel force in Newtons */
  double m_WheelForce;
  /*! The time the glow plug has been on??? in seconds */
  int m_GlowPlugLampTime;
  /*! The position of the accelerator petal UNITS?? */
  int m_ThrottlePosition;
  /*! The current gear ratio */
  float m_CurrentGearRatio;


};

#endif
