#!/bin/bash

# This script  runs the benchmark program 
# on the local system.
#
# Created: Nov 14, 2004
# Author: Henry Barnor

#now do a couple of runs
cd ~/dgc/projects/planner

resultDir=~/bench_results
if [ ! -e $resultDir ]; then 
    mkdir $resultDir
fi

/usr/bin/time -p -o $resultDir/test1.txt ./benchmark 1 &> /dev/null
/usr/bin/time -p -o $resultDir/test2.txt ./benchmark 5 &> /dev/null
/usr/bin/time -p -o $resultDir/test3.txt ./benchmark 25 &> /dev/null 
/usr/bin/time -p -o $resultDir/test4.txt ./benchmark 100 &> /dev/null
/usr/bin/time -p -o $resultDir/test5.txt ./benchmark 500 &> /dev/null

echo "Done benchmarking - hope you are pleased with the results :-D"