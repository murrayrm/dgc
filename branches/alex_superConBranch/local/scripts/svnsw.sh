# Script to svn switch
# Usage svnsw newPath
# where newPath is the path within the repo from the root
# Example1: svnsw trunk
# Example2: svnsw branches/yourBranch

NEW_PATH=$1
SVN_PREFIX=svn+ssh://

echo "WARNING: Beta copy"

SVN_PATH=dgc\\/subversion\\/dgc

#echo "$SVN_PATH"

SVN_URL=`svn info | grep URL: | sed "s/URL: svn+ssh:\\/\\/\(.*\)$SVN_PATH\(.*\)/\1$SVN_PATH/"`

#echo "SVN_URL= $SVN_URL"


NEW_URL="$SVN_PREFIX$SVN_URL/$NEW_PATH"

echo "Switching to $NEW_URL"
svn switch "$NEW_URL"
