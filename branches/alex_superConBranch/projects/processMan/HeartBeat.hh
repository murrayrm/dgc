/**
 * HeartBeat.hh
 * Revision History:
 * 20/08/2005  hbarnor  Created
 * $Id$
 */

#ifndef HEARTBEAT_HH
#define HEARTBEAT_HH

#include <pthread.h>
#include <string>


#include "networking.h"

#include "DGCutils"
//define DEBUG 1


#define LISTENQ 5 
#define HEART_PERIOD 1000000
// no file descriptor
#define NO_FILE_DESC -5


using namespace std;

/**
 * HeartBeat - tcp server that sends a heartbeat containing
 * a heart beat message type. Any attempt to connect to the tcp-port
 * will send a reset message to the connected monitor and disconnect
 * from it.
 */
class HeartBeat
{
public:
  /**
   * Default Constructor.
   * Spawns a heartbeat thread. Then calls listen to handle
   * connections.  
   */
  HeartBeat(string localIP, struct pmRmMesg * heartBeat);
  
  /**
   * Destructor - if it was not obvious.
   */
  ~HeartBeat();

private:

  /**
   * server - Sets up the necessary server params. Starts a listen
   * server that listens for incoming connections, sets a new
   * connection flag and also sets the newConnectionFd value.
   */
  void server();
  /**
   * heartBeat - sends a heartbeat message every couple of seconds to
   * the currently connected socket. 
   */
  void heartBeat();
  /**
   * writeData - wrapper for write. Sends the data to the globalConnfd
   * file descriptor.
   * @param filedes - the file descriptor to write data to. 
   * @param data - the data to be written out. 
   * @param nbytes - number of bytes of data.
   * @return number of bytes written or -1 if there is an error
   */
  ssize_t writeData(int filedes, const void * data, size_t nbytes);
private:
  /**
   * The IP on which this server is runing.
   */
  string m_localIP;
  /**
   * this true when a new connection is received. The heartbeat thread
   * looks at this to determine when to reset and connect to a new
   * host.
   */
  bool newConnection;
  /**
   * NULL when there is no connection, else the fd for the newest
   * connection. 
   */  
  int globalConnfd;
  /**
   * Mutex to prevent race conditions between heartbeat thread and
   * listen thread. 
   */
  pthread_mutex_t newConnMutex;
  /**
   * Pointer to the heartbeat that we send. 
   */
  struct pmRmMesg * p_myHeartBeat;
};



#endif 
