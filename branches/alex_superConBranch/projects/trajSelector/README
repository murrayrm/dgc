TRAJSELECTOR README

As currently implemented, trajSelector reads in the config file trajSelector.config to determine which trajs to listen for.  A "1" next to the name of a planner means that we will listen for it, and a "0" means that we will not.

Currently, trajSelector will output the trajectory of the input traj with the greatest average speed, provided that the traj, at the time of selection, was not received more than a set number of seconds ago.  This threshold value is the #define constant  TRAJ_TIMEOUT value found in trajSelector.h.  In the event that a traj has timed out, trajFollower will assume that it is due to a planner crash or failure, and will ignore the input from any planner that is older than TRAJ_TIMEOUT seconds.  Additionally, any traj whose speed exceeds the speed limit in the combined speed map at any point will be flagged as having excesive speeds, and will not be considered a viable candidate for selection.  The only exception to this rule is the case in which EVERY available plan that has not timed out has excessive speeds, in which case the plan with the LEAST average speed will be selected.

trajSelector also performs hysteresis to prevent chaotic traj switching.  In its current implementation, it does this by increasing the average speed of the traj that we're currently following by some percentage.  This percentage is the #define constant PERCENT_BONUS in trajSelector.h

Finally, trajSelector has the ability to create logs of the input data and meta-data that it uses to perform its traj evaluations.  It has 4 loggin levels, which may be set in the timber executable at run-time.  Logging level 0 corresponds to no logging, and 3 corresponds to the most verbose available logging; 2 is the default value.

Level 0: No data recorded

Level 1: The traj that is being followed before selection, the average speeds of all trajs that we are listening for, the elapsed times between planner trajectory reception and the beginning of the selection cycle, and the traj that is being followed after selection.

Level 2:  Same as Level 1, but also prints the meta-data arrays excessiveSpeeds and badTrajs

Level 3:  Same as above, but also prints the trajs that each planner produces.

The format for the log files is as follows:

(number of traj that is being followed)
(Planner number) (traj average speed) (traj elapsed time)
(The number of points in the traj for this planner)
(traj data)
(Planner number) (traj average speed) (traj elapsed time)
(The number of points in the traj for this planner)
(traj data)
	.
	.
	.
	.

(Planner number) (traj average speed) (traj elapsed time)
(The number of points in the traj for this planner)
(traj data)
(excessiveSpeeds array - consists of a series of 1 and 0 to represent the boolean values "true" and "false", respectively
(badTrajs array - consists of a series of 1 and 0 to represent the boolean values "true" and "false", respectively