#include "trajSelector.h"
#include <iostream>
#include <math.h>
//#include "DGCutils"
#include "TrajTalker.h"
#include "MapAccess.h"
#include <fstream>
#include "GlobalConstants.h"
#include "MapConstants.h"
#include "CTimberClient.hh"

using namespace std;


int deltasize;

int previousTrajNum;  //Number of the planner whose traj we were previously following; used for logging.


trajSelector::trajSelector(int num, double bonus, int sn_key)
  :CSkynetContainer(SNtrajSelector, sn_key), CStateClient(true), CTimberClient(timber_types::trajSelector), CSuperConClient("SLT")
{
  cout<<"Entering constructor."<<endl;

  cout<<"Constructing trajSelector with Skynet key = "<<sn_key<<endl;

  cout<<"Beginning initialization."<<endl;
  //Initialize the number of planners the vehicle has, and the bonus to give the currently followed traj.
  numTrajs = num;

  PERCENT_BONUS = bonus;

  //Initialize the arrays to hold the trajs, the traj updates, the average speed of each traj, and the boolean array to determine which planners to listern for.

  trajs = new CTraj[numTrajs];
  trajUpdates = new CTraj[numTrajs];

  avgSpeeds = new double[numTrajs];

  listenForTrajs = new bool[numTrajs];

  minSpeed = new double [numTrajs];

  excessiveSpeeds = new bool[numTrajs];

  badTrajs = new bool[numTrajs];

  trajTimes = new unsigned long long[numTrajs];

  trajElapsedTimes = new double[numTrajs];

  times = new pthread_mutex_t[numTrajs];

  trajSocket = m_skynet.get_send_sock(SNselectorTraj);

  cout<<"Reading in config file."<<endl;
  //Read in the config file to determine which planners to listen for.
  char buffer[30];
  ifstream infile("trajSelector.config");
  
  //Parse the lines in the file to determine how
  bool setFlag;
  
  for(int i = 0; i < numTrajs; i++)
    {
      infile.getline(buffer, 30);
      setFlag = false;
      int pos = 0;
      while( (pos < 30) && (!setFlag))
	{

	  if(buffer[pos] == '0')
	    {
	      listenForTrajs[i] = false;
	      setFlag = true;
	    }
	  else if(buffer[pos] == '1')
	    {
	      listenForTrajs[i] = true;
	      setFlag = true;
	    }
	  
	  pos++;
	}
    }
  
  cout<<"Creating mutexes."<<endl;
  //Need to create mutexes and blank trajs for planners that we're listening for.


  if(listenForTrajs[0])
    {
      cout<<"Creating deliberativeTraj mutex"<<endl;
      DGCcreateMutex(&deliberativeTraj);
    }

  if(listenForTrajs[1])
    {
      cout<<"Creating reactiveTraj mutex"<<endl;
      DGCcreateMutex(&reactiveTraj);
    }

  if(listenForTrajs[2])
    {
      cout<<"Creating blankTraj Mutex"<<endl;
      DGCcreateMutex(&blankTraj);
    }

  if(listenForTrajs[3])
    {
      cout<<"Creating roadTraj mutex"<<endl;
      DGCcreateMutex(&roadTraj);

    }

  if(listenForTrajs[4])
    {
      cout<<"Creating rddfTraj Mutex."<<endl;
      DGCcreateMutex(&rddfTraj);
    }

  //Create mutexes to keep track of accss to the array of times.
  for(int i = 0; i < numTrajs; i++)
    {
      if(listenForTrajs[i])
	{
	  DGCcreateMutex(&times[i]);
	}
    }

  //Create mutex to control access to previous traj info
  DGCcreateMutex(&previousTrajMutex);

  //Set up the map stuff
  DGCcreateMutex(&mapMutex);
  
  mapFullRequestSocket = m_skynet.get_send_sock(SNfullmaprequest);
  mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
  mapDelta = new char[MAX_DELTA_SIZE];



  //Initialize the map
  m_map.initMap(CONFIG_FILE_DEFAULT_MAP);

  //Insert the speed layer
  speedLayer = m_map.addLayer<double>(CONFIG_FILE_COST);

  //Get the vehicle's current location.
  cout<<"Updating state."<<endl;
  CStateClient::UpdateState();

  vehicleUTMNorthing = m_state.Northing_rear();
  vehicleUTMEasting = m_state.Easting_rear();

  m_map.updateVehicleLoc(vehicleUTMNorthing, vehicleUTMEasting);

  cout<<"Initializing map."<<endl;
 
  bool bRequestMap = true;
  m_skynet.send_msg(mapFullRequestSocket, &bRequestMap, sizeof(bool), 0);  //Request and then receive initial map info.
  mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
  if(mapDeltaSocket < 0)
    {
      cerr << "trajSelector constructor returned  skynet listen error." << endl;
    }
  
  //Receive map info. 
  RecvMapdelta(mapDeltaSocket, mapDelta, &deltasize);


  cout<<"Received initial map info"<<endl;
  //Apply initial map info.
  m_map.applyDelta<double>(speedLayer, mapDelta, deltasize);
  



  //Wait here to receive the first set of trajs.

  cout<<"Waiting for first set of trajs to get here."<<endl;

  if(listenForTrajs[0])
    {
      cout<<"Waiting for deliberative traj."<<endl;
      deliberativeSocket = m_skynet.listen(SNtraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.

      RecvTraj(deliberativeSocket, &trajUpdates[0]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[0]);

      cout<<"Received deliberative traj."<<endl;
    }

 if(listenForTrajs[1])
    {
      cout<<"Waiting for reactive traj."<<endl;
      reactiveSocket = m_skynet.listen(SNreactiveTraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.

      RecvTraj(reactiveSocket, &trajUpdates[1]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[1]);

      cout<<"Received reactive traj."<<endl;
    }


 if(listenForTrajs[2])
    {
      cout<<"Waiting for blank traj."<<endl;
      #warning:"Need a new skynet type for blank planner trajs here."
      blankSocket = m_skynet.listen(SNplannertraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.

     RecvTraj(blankSocket, &trajUpdates[2]);  //Receive the traj Update the buffer.

     DGCgettime(trajTimes[2]);

      cout<<"Received reactive traj."<<endl;
    }

 if(listenForTrajs[3])
    {
      cout<<"Waiting for road following traj."<<endl;
      #warning:"Need to add a new traj for plans coming from roadFinding."
      roadSocket = m_skynet.listen(SNplannertraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.
      RecvTraj(roadSocket, &trajUpdates[3]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[3]);

      cout<<"Received road following traj."<<endl;

    }

 if(listenForTrajs[4])
    {
      cout<<"Waiting for rddf traj."<<endl;
      rddfSocket = m_skynet.listen(SNRDDFtraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.

      RecvTraj(rddfSocket, &trajUpdates[4]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[4]);

      cout<<"Received rddf traj."<<endl;
    }


 //Initialize the traj we are following.  Note that the lowest-numbered active traj in the array trajs has priority by default.
  for(int i = numTrajs - 1; i >= 0; i--)
    {
      if(listenForTrajs[i])
	{
	  trajWeAreFollowing = i;
	}
    }

  cout<<"Constructor finished."<<endl;

}


void trajSelector::select()
{

  cout<<"Starting main program loop."<<endl;

  //Start up the various function threads.

  cout<<"Starting map listener thread."<<endl;
  DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::mapListener);

  if(listenForTrajs[0])
    {
      cout<<"Starting deliberative listener thread."<<endl;
      DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::deliberativeListener);
    }

  if(listenForTrajs[1])
    {
      cout<<"Starting reactive listener thread."<<endl;
      DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::reactiveListener);
  
    }

  if(listenForTrajs[2])
    {
      cout<<"Starting blank listener thread."<<endl;
      DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::blankListener);
    }

  if(listenForTrajs[3])
    {
      cout<<"Starting road following listener thread."<<endl;
      DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::roadListener);
    }

  if(listenForTrajs[4])
    {
      cout<<"Starting rddfPathGen listener thread."<<endl;
      DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::rddfListener);
    }


  //Logging shiznit

  string log_directory;
 
  string LOG_FILE_name;
  string HEADER_FILE_name;

  bool loggingEnabled;
  bool newSession; //Records whether or not this is the first time we're entering data into a log file.

  ofstream outfile;
  ofstream header_file;

  int logging_level;


  while(true)
    {

      previousTrajNum = trajWeAreFollowing;
      cout<<"Entering method select()"<<endl;
      //Update the list of trajs to the most current available set of trajs.
      
      cout<<"Updating trajs."<<endl;
      
      if(listenForTrajs[0])
	{
	  //Get the deliberative planner's traj.
	  DGClockMutex(&deliberativeTraj);
	  trajs[0] = trajUpdates[0];
	  DGCunlockMutex(&deliberativeTraj);
	}
      
      if(listenForTrajs[1])
	{
	  //Get the reactive planner's traj
	  DGClockMutex(&reactiveTraj);
	  trajs[1] = trajUpdates[1];
	  DGCunlockMutex(&reactiveTraj);
	}
      
      if(listenForTrajs[2])
	{
	  //Get the blank planner's traj.
	  DGClockMutex(&blankTraj);
	  trajs[2] = trajUpdates[2];
	  DGCunlockMutex(&blankTraj);
	}
      
      if(listenForTrajs[3])
	{
	  //Get the roadfinding traj.
	  DGClockMutex(&roadTraj);
	  trajs[3] = trajUpdates[3];
	  DGCunlockMutex(&roadTraj);
	}
      
      if(listenForTrajs[4])
	{
	  //Get the rddfPathGen traj.
	  DGClockMutex(&rddfTraj);
	  trajs[4] = trajUpdates[4];
	  DGCunlockMutex(&rddfTraj);
	}

      //Check to see if loggin is enabled with each loop execution.
      //loggingEnabled = getLoggingEnabled();

      if(loggingEnabled)
	{
	  logging_level = getMyLoggingLevel();
	  //If logging is enabled and this is a new session, we need to output a new header file.
	  newSession = checkNewDirAndReset();
	  if(newSession)
	    {
	      log_directory = getLogDir();
		HEADER_FILE_name = log_directory + HEADER_FILE;
	      LOG_FILE_name = log_directory + LOG_FILE;

	      cout<<"Creating new header file: "<<HEADER_FILE_name<<endl;
	      header_file.open(HEADER_FILE_name.c_str(), ios::out | ios::trunc);
	      for(int i = 0; i < numTrajs;i ++)
		{
		  header_file<<(listenForTrajs[i] ? 1 : 0)<<endl;
		}

	      header_file<<PERCENT_BONUS;

	      header_file.close();

	      if(outfile.is_open())
		{
		  outfile.close();
		}

	      outfile.open(LOG_FILE_name.c_str(), ios::out | ios::trunc);
	    }
	}
      
      
      cout<<"Beginning selection."<<endl;
      
      //Reset the array to keep track of excessive speeds.
      for(int i = 0; i < numTrajs; i++)
	{
	  excessiveSpeeds[i] = false;
	}

      //Reset the array to keep track of bad trajs.

      for(int i = 0; i < numTrajs; i++)
	{
	  badTrajs[i] = false;
	}


            
      //Determines the traversal time for each of the various trajs.
      double time, speed, distance, sectionDistance, trajAvgSpeed;
      
      double* northing; 
      double* easting;
      double* n1;
      double* e1;
      double mapSpeed;
      double yaw;
      double junk1, junk2, junk3;  //Needed for accessing Dima's function, as it takes several arguments that it uses to return data that we don't care to know about.

      unsigned long long timeDifference;  //Keeps track of current/elapsed time
      unsigned long long currentTime;


            
      cout<<"Calculating average speeds for all active trajs."<<endl;
      
      //Calculate average speeds for all trajs
      for(int currentTraj = 0; currentTraj <= numTrajs - 1; currentTraj++)
	{
	  if(listenForTrajs[currentTraj])
	    {
	      DGCgettime(currentTime);  //Record the current time.
	      DGClockMutex(&times[currentTraj]);
	     
	      timeDifference = currentTime -  trajTimes[currentTraj];

	      trajElapsedTimes[currentTraj] = DGCtimetosec(timeDifference);  //Calculate the elapsed time from when this traj was received until right now.
	      DGCunlockMutex(&times[currentTraj]);

	      cout<<"Elapsed time for planner "<<currentTraj<<" is "<<trajElapsedTimes[currentTraj]<<" seconds."<<endl;

	      //Check to see if the traj is too old.  If it is, we know we're not going to use it, so skip all the processing below and just print an error message.

	      if(trajElapsedTimes[currentTraj] <= TRAJ_TIMEOUT)  //This traj is ok.
		{	      
		  time = 0; //Set the traversal time for the current traj to zero.

		  distance = 0;  //Set the total distance for the current traj to zero.

		  //Determine the number of points in the current traj
		  int numPoints = trajs[currentTraj].getNumPoints(); 
		  
		  //Get the data for the current traj
		  northing = trajs[currentTraj].getNdiffarray(0);
		  easting = trajs[currentTraj].getEdiffarray(0);
		  n1 = trajs[currentTraj].getNdiffarray(1);
		  e1 = trajs[currentTraj].getNdiffarray(1);

		  minSpeed[currentTraj] = 100000;
		  
		  for(int point = 0; point <= numPoints - 2; point++)
		    {
		      sectionDistance = sqrt( pow( (northing[point+1] - northing[point]), 2) + pow( (easting[point+1] - easting[point]), 2) ); //Compute the straight-line distance from the curent point to the next point on the traj
		      speed = sqrt( pow( n1[point], 2) + pow( e1[point], 2 ) ); //Compute the speed at which the vehicle will be traversing this section
		      time += sectionDistance / speed;  //Increment the total time appropriately
		      distance += sectionDistance; //Increment the total distance appropriately.
		      
#warning:"May also want to include something about lookahead distance of the object we're looking at as well (i.e., only consider trajs to have excessive speeds for nearby obstacles, as if an just popped up on our sensors, the planners probably just haven't had time to take account of it yet, whereas if it's close to us, that could indicate planner failure.  May also want to take into consideration the speed at which we're currently traveling when taking into consideration the lookahead distance."
		      //Use Dima's function to check if we're going too fast for the map value at this point.
		      
		      
		      yaw = atan(e1[point]/n1[point]);  // = tangent of yaw.
		      DGClockMutex(&mapMutex);
		      getContinuousMapValueDiffGrown(&m_map, speedLayer, northing[point], easting[point], yaw, &mapSpeed, &junk1, &junk2, &junk3);
		      DGCunlockMutex(&mapMutex);

		      if(mapSpeed < minSpeed[currentTraj])
			{
			  minSpeed[currentTraj] = mapSpeed;  //Keep track of the min speed that the vehicle's traj passes through in the map for SuperCon
			}

		      if(speed > mapSpeed)  //This traj has excessive speed.
			{
			  excessiveSpeeds[currentTraj] = true;
			}
		      
		    }
		  
		  if(excessiveSpeeds[currentTraj])
		    {
		      cout<<"Planner "<<currentTraj<<" has generated a plan with excessive speed."<<endl;
		    }
	      
		  trajAvgSpeed = distance / time;  //Find the average traj speed by dividing total distance by total time.
		  
		  avgSpeeds[currentTraj] = trajAvgSpeed; //Store the time value for this traj
		  cout<<"Calculated average speed for traj "<<currentTraj<<" is "<<avgSpeeds[currentTraj]<<" m/s"<<endl;
		}

	      else  //This traj has not been updated in too long.  We can assume that the planner that generated this traj has crapped out, so we should ignore its input.
		{
		  badTrajs[currentTraj] = true;
		  cout<<"Planner "<<currentTraj<<" has timed out.  Ignoring input."<<endl;
		}
	    }
	}
      
      //First, determine if any planner's plan is out of date (i.e., if there is a possibility that it has completely crapped out on us.



      //Then determine if all of the planners have failed to generate a safe path.
      
      bool allExcessiveSpeeds = true;
      for(int i = 0; i < numTrajs; i++)
	{
	  if(!excessiveSpeeds[i])
	    {
	      allExcessiveSpeeds = false;
	    }
	}
      
      
      if(!allExcessiveSpeeds)  //At least one planner has generated a plan that looks ok.
	{
	  //Give the average speed of the trajectory that we're currently following a "bonus" or "weight" as a form of hysteresis to prevent chaotic traj switching.

	  //Find the max average speed of all the trajs.
	  double maxSpeed = 0;
	  
	  for(int currentTraj = 0; currentTraj <= numTrajs - 1; currentTraj++)   //For each time in the array of traversal times
	    {
	      if(listenForTrajs[currentTraj])
		{

		  if(currentTraj == trajWeAreFollowing)  //This traj should get a speed bonus of PERCENT_BONUS in the calculation
		    {

		      if((avgSpeeds[currentTraj] * (1 + PERCENT_BONUS) > maxSpeed)  && !excessiveSpeeds[currentTraj] && !badTrajs[currentTraj]) 
			{
			  maxSpeed = avgSpeeds[currentTraj]*(1 + PERCENT_BONUS);  //If the max speed found up until this point is less than the speed we're looking at right now, set the current speed to be the new max.
			  trajWeAreFollowing = currentTraj;  //Set the traj associated with this time to be the traj that we're currently following.
			}
		    }


		  else  //We're not currently following this traj, so it shouldn't get a speed bonus.
		    {
		      if((avgSpeeds[currentTraj] > maxSpeed) && !excessiveSpeeds[currentTraj] && !badTrajs[currentTraj])
			{
			maxSpeed = avgSpeeds[currentTraj];
			trajWeAreFollowing = currentTraj;
			}
		    }
			 
		}
	    }      
	}
      
      else   //If we EVER find ourselves here, it means that we're in SERIOUS TROUBLE.  Oh well.  DAMN THE TORPEDOS!!!  Actually, if all of the planners have exessive speeds, we really should pick the slowest plan
	{
	  //Find the min average speed of all the trajs.
	  double minSpeed = avgSpeeds[0];
	  
	  for(int currentTraj = 1; currentTraj <= numTrajs - 1; currentTraj++)   //For each time in the array of traversal times
	    {
	      if(listenForTrajs[currentTraj])
		{
		  if( (avgSpeeds[currentTraj] < minSpeed) && !badTrajs[currentTraj])
		    {
		      minSpeed = avgSpeeds[currentTraj];  //If the min speed found up until this point is greater than the speed we're looking at right now, set the current speed to be the new minimum.
		      trajWeAreFollowing = currentTraj;  //Set the traj associated with this time to be the traj that we're currently following.
		    }
		}
	    }
	}
      
      cout<<"Finished selection.  Sending traj "<<trajWeAreFollowing<<" with average speed "<<avgSpeeds[trajWeAreFollowing]<<" m/s"<<endl;
      //Publish the traj that we want to follow (stored in the Ctraj array in slot trajs[trajWeAreFollowing])
      SendTraj(trajSocket, &trajs[trajWeAreFollowing]);

      scMessage( int( min_speed_in_current_traj ), minSpeed[trajWeAreFollowing]);

      DGClockMutex(&previousTrajMutex);
      previousTraj = trajs[trajWeAreFollowing];
      DGCunlockMutex(&previousTrajMutex);
      
      cout<<"Method select() finished."<<endl<<endl;

      //Now we need to write all this awesome data that we've just received/calculated to our super-cool log files.

      if(loggingEnabled && (logging_level != 0))
	{

	  //Print out the number of the planner whose traj we were previously following

	  outfile<<previousTrajNum<<endl;

	  for(int i = 0; i < numTrajs; i++)
	    {
	      if(listenForTrajs[i])
		{
		  //Print out a planner id number, avg speed, and elapsed time since reception on one line
		  outfile<<i<<" "<<avgSpeeds[i]<<" "<<trajElapsedTimes[i]<<endl;
		  //Print out the actual traj itself that this corresponds to ONLY IF logging_level == 3, as this leads to GIGANTIC log files.
		  if(logging_level ==3)
		    {
		      outfile<<trajs[i].getNumPoints()<<endl;
		      trajs[i].print(outfile);
		      outfile<<endl;
		    }
		}
	    }

	  //
	  if(logging_level >= 2)
	    {
	      //Now print out the values stored in the excessiveSpeeds array on one line; true = 1, false = 0.
	      for(int i = 0; i < numTrajs; i++)
		{
		  outfile<<(excessiveSpeeds[i] ? 1 : 0)<<" ";
		}
	      
	      outfile<<endl;

	      //Print out the values of the badTrajs array in the same fashion.
	      for(int i = 0; i < numTrajs; i++)
		{
		  outfile<<(badTrajs[i] ? 1 : 0)<<" ";
		}
	      
	      outfile<<endl;
	    }

	  //Finally, print out the number of the traj that we're currently following, followed by TWO carriage returns (to create a blank line to signal the end of a selection cycle.
	  outfile<<trajWeAreFollowing<<endl<<endl;
	}

	      

      usleep(DELAY_TIME*(double) 1E6);
    } 
}


void trajSelector::deliberativeListener()
{

  //  cout<<"Entering method deliberativeListener()."<<endl;

  while(true)
    {
      WaitForTrajData(deliberativeSocket);  //Wait for data to become available.
      DGClockMutex(&deliberativeTraj);
      RecvTraj(deliberativeSocket, &trajUpdates[0]);  //Receive the traj Update the buffer.
      DGCunlockMutex(&deliberativeTraj);      //Lock the mutex associated with the buffer for this traj.

      DGClockMutex(&times[0]);
      DGCgettime(trajTimes[0]);
      DGCunlockMutex(&times[0]);

      //cout<<"Received deliberative traj."<<endl;
    }
  //cout<<"Exiting method deliberativeListener()."<<endl;
}



void trajSelector::reactiveListener()
{

  //cout<<"Entering method reactiveListener()."<<endl;

  while(true)
    {
      WaitForTrajData(reactiveSocket);  //Wait for data to become available.
      DGClockMutex(&reactiveTraj);
      RecvTraj(reactiveSocket, &trajUpdates[1]);  //Recv the traj.
      DGCunlockMutex(&reactiveTraj);      //Lock the mutex associated with the buffer for this traj.

      DGClockMutex(&times[1]);
      DGCgettime(trajTimes[1]);
      DGCunlockMutex(&times[1]);

      //cout<<"Received reactive traj."<<endl;


    }

  //cout<<"Exiting method reactiveListener()"<<endl;
}

void trajSelector::blankListener()
{
  //cout<<"Entering method blankListener()"<<endl;

  while(true)
    {
      WaitForTrajData(blankSocket);  //Wait for data to become available.
      DGClockMutex(&blankTraj);
      RecvTraj(blankSocket, &trajUpdates[2]);  //Receive the traj.
      DGCunlockMutex(&blankTraj);      //Lock the mutex associated with the buffer for this traj.

      DGClockMutex(&times[2]);
      DGCgettime(trajTimes[2]);
      DGCunlockMutex(&times[2]);
      
      //cout<<"Received blank traj."<<endl;


     
    }

  //cout<<"Exiting method blankListener()"<<endl;
}


void trajSelector::roadListener()
{
  //cout<<"Entering method roadListener()"<<endl;

  while(true)
    {
      WaitForTrajData(roadSocket);  //Wait for data to become available.
      DGClockMutex(&roadTraj);
      RecvTraj(roadSocket, &trajUpdates[3]);  //Receive  the traj.
      DGCunlockMutex(&roadTraj);      //Lock the mutex associated with the buffer for this traj.

      DGClockMutex(&times[3]);
      DGCgettime(trajTimes[3]);
      DGCunlockMutex(&times[3]);

      //cout<<"Received road traj."<<endl;


    }

  //cout<<"Exiting method roadListener()"<<endl;
}

void trajSelector::rddfListener()
{

  //cout<<"Entering method rddfListener()"<<endl;

  while(true)
    {
      WaitForTrajData(rddfSocket);  //Wait for data to become available.
      DGClockMutex(&rddfTraj);
      RecvTraj(rddfSocket, &trajUpdates[4]);  //Receive the traj.
      DGCunlockMutex(&rddfTraj);      //Lock the mutex associated with the buffer for this traj.

      DGClockMutex(&times[4]);
      DGCgettime(trajTimes[4]);
      DGCunlockMutex(&times[4]);

      //cout<<"Received rddf traj."<<endl;


    }

  //cout<<"Exiting method rddfListener()"<<endl;
}



void trajSelector::mapListener()
{
  //cout<<"Entering method mapListener()"<<endl;
  while(true)
    {
      CStateClient::UpdateState();  //Get vehicle state update.
      vehicleUTMNorthing = m_state.Northing_rear();
      vehicleUTMEasting = m_state.Easting_rear();
      //cout<<"Updated vehicle state."<<endl;

      DGClockMutex(&mapMutex);
      //Center the map on the vehicle's new location.
      m_map.updateVehicleLoc(vehicleUTMNorthing, vehicleUTMEasting);
      DGCunlockMutex(&mapMutex);


      //Get map deltas.
      RecvMapdelta(mapDeltaSocket, mapDelta, &deltasize);

      //Apply the map deltas.
      DGClockMutex(&mapMutex);
      m_map.applyDelta<double>(speedLayer, mapDelta, deltasize);
      //cout<<"Updated map."<<endl;
      DGCunlockMutex(&mapMutex);
    }
  //cout<<"Exiting method mapListener()"<<endl;
}


#warning:"This code may be on acid, as I wasn't thinking clearly when I wrote it."
void trajSelector::modifyTraj(CTraj& trajToModify)
{
  DGClockMutex(&previousTrajMutex);

  int branchPoint = previousTraj.getPointAhead(0, LOOKAHEAD_DIST);  //Get the index of the first point on the traj that is at least LOOKAHEAD_DIST meters from the beginning.

  double* pNarray = previousTraj.getNdiffarray(0);
  double* pEarray = previousTraj.getEdiffarray(0);
  double Northing = pNarray[branchPoint];
  double Easting = pEarray[branchPoint];
  double NDiff = (previousTraj.getNdiffarray(1))[branchPoint];
  double EDiff = (previousTraj.getEdiffarray(1))[branchPoint];

  DGCunlockMutex(&previousTrajMutex);

  int length = trajToModify.getNumPoints();
  double* Narray = trajToModify.getNdiffarray(0);
  double* Earray = trajToModify.getEdiffarray(0);

  double x,y;
  double angle;
  int joinPoint = 0;  //Index of spot to connect previous traj to on the traj that we are modifying.

  //Find the first point on the traj we're receiving that falls within a cone of angle CONE ANGLE.  If no such point exists, choose the last point on the end of the traj.

  do
    {
      x = Earray[joinPoint] - Easting;
      y = Narray[joinPoint] - Northing;
      angle = acos((x*EDiff + y*NDiff) / ( sqrt(pow(x, 2) + pow(y, 2)) * sqrt( pow(EDiff, 2) + pow(NDiff, 2)) ));
      joinPoint++;
    } while((joinPoint < length) && (angle > (CONE_ANGLE/2)));

  joinPoint--;


  double Ndelta = Narray[joinPoint] - Northing;
  double Edelta = Earray[joinPoint] - Easting;
  double dist = sqrt( pow(Edelta, 2) + pow(Ndelta, 2) );

  double sine = Ndelta/dist;
  double cosine = Edelta/dist;

  int extraPoints = (int) (dist / .2);

  //Create the new traj here.

  int newTrajLength = (branchPoint + 1) + extraPoints + (length - joinPoint);

  CTraj modifiedTraj;
  modifiedTraj.setNumPoints(newTrajLength);
  modifiedTraj.startDataInput();

  //Fill in the points on the old traj up to and including the branch point.
  for(int i = 0; i <= branchPoint; i++)
    {
      modifiedTraj.inputNoDiffs(pNarray[i], pEarray[i]);
    }

  //Fill in the points on our joining section.
  for(int i = 1; i <= extraPoints; i++)
    {
      modifiedTraj.inputNoDiffs(Northing + (dist/extraPoints)*i*sine, Easting + (dist/extraPoints)*i*cosine);
    }


  //Fill in the remaining points on the traj that we're modifying, including the point at which our joining section connects to the traj.
  for(int i = joinPoint; i < length; i++)
    {
      modifiedTraj.inputNoDiffs(pNarray[i], pEarray[i]);
    }

#warning:"Now that we've got a new spacial traj here, need to use Dima's black magic to C2-ify and assign a speed profile."

#warning:"Need to check for dynamic feasibility here."
}



trajSelector::~trajSelector()
{
  delete [] trajs;
  delete [] avgSpeeds;
  delete [] trajUpdates;
  delete [] mapDelta;
  delete [] excessiveSpeeds;
  delete [] badTrajs;
  delete [] trajTimes;
  delete [] trajElapsedTimes;
  delete [] minSpeed;

  for(int i = 0; i < numTrajs; i++)
    {
      if(listenForTrajs[i])
	{
	  DGCdeleteMutex(&times[i]);
	}
    }

  if(listenForTrajs[0])
    {
      DGCdeleteMutex(&deliberativeTraj);
    }

 if(listenForTrajs[1])
    {
      DGCdeleteMutex(&reactiveTraj);
    }

 if(listenForTrajs[2])
    {
      DGCdeleteMutex(&blankTraj);
    }

 if(listenForTrajs[3])
    {
      DGCdeleteMutex(&roadTraj);
    }

 if(listenForTrajs[4])
    {
      DGCdeleteMutex(&rddfTraj);
    }

 DGCdeleteMutex(&mapMutex);

  delete [] listenForTrajs;

}
