#ifndef DBS_HH
#define DBS_HH

#include "StateClient.h"
#include <pthread.h>
#include <unistd.h>
#include <sstream>
#include "AliceConstants.h" 
#include <iomanip>
#include <string>
#include "DGCutils"
#include <math.h>
#include <fftw3.h>
#include "gnuplot_i.h"

using namespace std;


#define DBS_NUM_VALUES 40
#define AVESIZE 10

/**
 * DBS class.
 * input: none
 * output: none
 */
class DBS : public CStateClient
{
public:
  /** Contstructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  DBS(int skynetKey);

  /** Standard destructor */
  ~DBS();


  /** not currently implementedb (we don't need to receive any messages) */
  void ActiveLoop();

  //Return all mean values
  void stats(VehicleState &means, VehicleState &vars);

  void fftcalculatorLastN();
  int GetRoadType();
private:

  double pitcharea, pitcharea3, rollarea5to8, Zdotarea, pitchhighestValue, rollhighestValue,rollarea, highestValuepitch, meanspeed, pitch10;

  double avpitcharea, avpitch3, avroll5, avpitch10;
  double hpitcha[AVESIZE];
  double hpitch3a[AVESIZE];
  double hroll5a[AVESIZE];
  double hpitch10a[AVESIZE];

  int roadtype;//-1 = unsure 0 paved, 1 dirt, 2 rocky, 3 bumpy

  void PrintValue(fftw_complex* fft);
  void PrintDebug(fftw_complex* in, fftw_complex* out);
  void PrintValueSum(fftw_complex* fftA);
  int m_next_pos;
  VehicleState m_states[DBS_NUM_VALUES];
};


#endif  // DBS_HH
