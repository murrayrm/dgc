Help (see the following by typing ./rddfPathGen help)

| Command line options:
| (you must type command line options just as they appear in this help
| screen; do not preceed them with some arbitrary number of -'s.
===============================================================
  h,-h,help,--help    Prints this.
  k, noskynet         Doesn't send any skynet messages.  We still need skynet
                         to run, because we need state.
  d, writedense       Writes the dense traj to a file.
  s, nosparse         Skips writing the sparse traj to a file.
  e, everytraj        Writes every traj sent over skynet to a file (named sequentially).
  w, nowrite          Skips writing the dense and sparse traj to a file.
  nomm                Don't use ModeManModule.
  c, c2               Make all traj's c2 by running them through an approximation
                         function in the planner.
  n, noc2             Do not perform the c2 approximation.

| Additional info:
==============================================================

  Users must explicitly specify either c2 (c) or noc2 (n) on the command line.  This is
  required so that there isn't any confusion as to what kind of traj's we're sending.
  C2 traj's will be smoother (continuous second derivative), but the actual path may
  jump around.  Non-c2 traj's will not move around, but they have a second derivative
  which is only piecewise continuous.

  Also, the options writedense and c2 are mutually exclusive, because the approximation
  function currently used would never work for the whole corridor.

  Nothing will be written to any log file unless logging has been enabled via timber!







==============================================================================
Documentation from the WIKI:
==============================================================================

The Big Picture:
----------------
RddfPathGen is a module (currently an skynet module) that generates paths using 
an RDDF file. An RDDF file defines a corridor which our vehicle must stay in; 
where exactly within the corridor the vehicle goes is up to us. The format of
 the RDDF file is specified by DARPA.

How it Works:
-------------
When RddfPathGen starts up, it reads in the complete RDDF file, and fits a 
drivable curve to the entire corridor. It stores this drivable curve, called a
'path' in its own proprietary format so as to not take up too much memory. 
Then it begins sending paths to any modules which need the paths in the common 
CTraj format. The paths sent over skynet are dense paths, and contain x number 
of points per meter, where x is usually greater than one. Because the paths are 
dense, they consume more memory than the native format used internally by 
RddfPathGen, so in general the entire path will not be sent at once.


"How to run RddfPathGen for dummies"
------------------------------------
Compile the dependencies:
make; make install in dgc/
make; make install in dgc/projects/skynet
make; make install in dgc/projects/ModuleHelpers/
make; make install in dgc/projects/traj

Set up the RDDF:
Edit dgc/RDDF/Makefile line 85 or so to make it point to the correct RDDF
make in dgc/RDDF

Compile RddfPathGen:
make in dgc/projects/RddfPathGen

Run it:
Skynetd must be running (go to dgc/projects/skynet and type "./skynetd &"
head over to dgc/projects/RddfPathGen and type ./RddfPathGen

It should start up, tell you some info about the skynet key you're using, 
the current RDDF file, how many points were ready in, etc... RddfPathGen 
will print out a line everytime it sends a traj; if you're not seeing a 
line every second or so, it's because RddfPathGen can't find the state. 
Run astate or asim to remedy this problem, or if you just want a demo of 
RddfPathGen printing stuff out, you can run "./RddfPathGen nostate". But 
that's getting into our next section...

Command line options:
---------------------
h,-h,help,--help Prints the command line option help (this) and status.
a, nostate Sets location to (0,0) every time GetLocation is called. Use 
   if RddfPathGen hangs because it can't find astate.
k, noskynet Runs without skynet (doesn't send any messages). Acutally, 
   we still need skynet to run, because we derive from ModuleHelpers :(
d, nodense Skips writing the dense traj to a file.

How to 1337 H4xoR RddfPathGen:
------------------------------
Many options can be changed by editing RddfPathGen.hh and AliceConstants.h. 
These changes require a make to take effect. Seeing as you're an elite 
haxor, I'll assume you don't mind looking at actual excerpts from the code.

============================================================================
End Documentation From the Wiki
============================================================================


Files
--------------------------
These functions comprise the skynet module which generates paths from an RDDF
and sends them on to certain other modules.

RddfPathGen also outputs the complete dense path to a file (currently set to be
./rddf.traj)

RddfPathGen.hh
RddfPathGen.cc
- main module files

pathLib.cc
- library containing basically all the functions required for RddfPathGen

jytest.cc
- useful for debugging RddfPathGen and pathhelper.cc

bptest.cc
- uses rddf file specified in the RDDF makefile (creates link).
- used for debugging path creation / trajDFE functionality.
- outputs sparse path and dense paths (one smoothed, one not).

plot_results_ben.m
- used to plot a traj and corridor for easy analysis of outputs.
- currently set to default output from RddfPathGen (unless someone changed it).

vectorOps.hh
vectorOps.cc
- a nice vector operations library I wrote (defines + operator for
  example...see vectorOps.hh for more info
=======




Notes:
============
To see Dima's changes:
  svn diff -r5754:5755


To back out Ben's DFE changes:
  svn merge -r5830:5829 .
or better yet...just comment out line 516 of pathLib.cc:
   -TrajDFE(path_dense);
   +//TrajDFE(path_dense);


Skynet's not working... it works now at rev 7031, I'm committing 7032 
so I can do a fresh checkout and see if we still have these problems
