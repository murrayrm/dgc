#include "DBS.hh"

using namespace std;


extern int NOSKYNET;                /**< for command line argument */

DBS::DBS(int skynetKey) : CSkynetContainer(MODDBS, skynetKey)
{

  //read in all values.
  //DGCstartMemberFunctionThread(this, &DBS::StateReader);
}

DBS::~DBS()
{
  // Do we need to destruct anything??
  cout << "DBS destructor has been called." << endl;
}

void DBS::fftcalculatorLastN()
{
  // make a new chunk of memory to use as a buffer for the last
  // N values
  VehicleState* buffer = new VehicleState[DBS_NUM_VALUES];  

  // get last DBS_NUM_VALUES state values and store them in our buffer,
  // using GetLastNValues(DBS_NUM_VALUES,destination
  GetLastNValues(DBS_NUM_VALUES,buffer);

  // do this:
  fftw_complex *in;
  fftw_complex *out;
  fftw_plan p;
  in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  
  // copy the pitch from our buffer to "in"
  int n;
  for(n=0;n<DBS_NUM_VALUES;n++){
    in[n][0] = buffer[n].Pitch;
  }
  
  // do the fft
  p = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);

  // do something with the fft results
  PrintDebug(in, out);
  PrintValueSum(out);

  // at end, free the memory
  fftw_destroy_plan(p);
  fftw_free(in); fftw_free(out);
  delete [] buffer; 
}


void DBS::stats(VehicleState &means, VehicleState &vars) {
  double vR, sumR, mVR,r,sumVR,varR,vPR,sumPR,mVPR,pR,sumVPR,varPR,vRR,sumRR,mVRR,rR,sumVRR,varRR,sumP,mVP,p,sumVP,varP,vfDD,sumfDD,mVfDD,fDD,sumVfDD,varfDD,vsDD,sumsDD,mVsDD,sDD,sumVsDD,varsDD,maxAP,maxAR;
  int n;  
  
  sumPR=0;
  sumVPR=0; 
  sumR=0;
  sumVR=0;
  sumRR=0;
  sumVRR=0;
  sumP=0;
  sumVP=0;
  
  sumfDD=0;
  sumVfDD=0;
  sumsDD=0;
  sumVsDD=0;
  n=0;
 
   
  //MaxAP = 10.87;
  // MaxAR = 7.96;

   
     //if(fftAP<MaxAP && fftAR<MaxAR){
       //Hur sager jag till systemet att sakte ner
       //else
   //Hur sager jag till systemet att oka hastigheten
   
   //terrainclassification
   //while(n!= DBS_NUM_VALUES ){
     //if fftAP<0.6
	  //pavedroad
//if fftAP<0.6 && fftAP<5
//  cout<<dirtroad<<;
//  else
//    cout<<offroad<<;
/*
 while(n!=DBS_NUM_VALUES){
   r = m_states[n].Roll;
   sumR = sumR + r ;
   n++;
 }
 means.Roll = sumR/DBS_NUM_VALUES; 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   r = m_states[n].Roll;
   sumVR  =sumVR + pow(r-means.Roll,2);
   n++;
 }
 vars.Roll =sumVR/DBS_NUM_VALUES;

//"MeanValue and Variance rollrate"
 n=0;
 while(n!=DBS_NUM_VALUES){
   rR = m_states[n].RollRate;
   sumRR = sumRR   + rR ;
   n++;
 }
 means.RollRate = sumRR  /DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   rR = m_states[n].RollRate;
   sumVRR  =sumVRR + pow(r-mVRR,2);
   n++;
 }
 vars.RollRate = sumVRR/DBS_NUM_VALUES;

// pitchrate MeanValue and Variance pitchrate"
 n=0;
 while(n!=DBS_NUM_VALUES){
   pR = m_states[n].PitchRate;
   sumPR = sumPR + pR ;
   n++;
 }
 means.PitchRate = sumPR  /DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   pR = m_states[n].PitchRate;
   sumVPR = sumVPR + pow(pR-means.PitchRate,2);
   n++;
 }
 vars.PitchRate = sumPR/DBS_NUM_VALUES;

 //MeanValue and Variance pitch
 n=0;
 while(n!=DBS_NUM_VALUES){
   p = m_states[n].Pitch;
   sumP = sumP + p ;
   n++;
 }
 means.Pitch = sumP/DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   p = m_states[n].Pitch;
   sumVP  =sumVP + pow(p-means.Pitch,2);
   n++;
 }
 vars.Pitch = sumVP/DBS_NUM_VALUES;

// first derivates down, mean value and varianceMeanValue and Variance
 n=0;
 while(n!=DBS_NUM_VALUES){
   fDD = m_states[n].Vel_D;
   sumfDD = sumfDD + fDD;
   n++;
 }
 means.Vel_D = sumfDD  /DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   fDD = m_states[n].Vel_D;
   sumVfDD  =sumVfDD+ pow(fDD-means.Vel_D,2);
   n++;
 }
 vars.Vel_D = sumVfDD/DBS_NUM_VALUES;

// second derivates down, meanvalue and variance 
 n=0;
 while(n!=DBS_NUM_VALUES){
   sDD = m_states[n].Acc_D;
   sumsDD = sumsDD + sDD;
   n++;
 }
 means.Acc_D = sumsDD/DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   sDD = m_states[n].Acc_D;
   sumVsDD=sumVsDD+ pow(sDD-means.Acc_D,2);
   n++;
 }
 vars.Acc_D = sumVsDD/DBS_NUM_VALUES;
*/

}



void DBS::ActiveLoop()
{
  // make a new chunk of memory to use as a buffer for the last
  // N values

  double pitchav[DBS_NUM_VALUES];
  double pitch3av[DBS_NUM_VALUES];
  double roll5av[DBS_NUM_VALUES]; 
  int numpitch=0;
  
  VehicleState* buffer = new VehicleState[DBS_NUM_VALUES];
  // memory for gnuplot
  double* gnuplot_y = new double [DBS_NUM_VALUES];
  double* gnuplot_yav = new double [DBS_NUM_VALUES];

  double* gnuplot_pitch = new double [DBS_NUM_VALUES];
  double* gnuplot_roll = new double [DBS_NUM_VALUES];
  double* gnuplot_zdot = new double [DBS_NUM_VALUES];

  int historysize = 200;
  double* gnuplot_pitcharea3 = new double[historysize];
  double* gnuplot_pitcharea = new double[historysize];
  double* gnuplot_meanspeed = new double[historysize];
  double* gnuplot_zdotarea = new double[historysize];
  double* gnuplot_rollarea = new double[historysize];
  double* gnuplot_rollarea5to8 = new double[historysize];
  double* gnuplot_avpitch = new double[historysize];
  double* gnuplot_avpitch3 = new double[historysize];
  double* gnuplot_avroll5 = new double[historysize];
  double* gnuplot_pitchsp = new double[historysize];
  double* gnuplot_avpitch10 = new double[historysize];

  for(int i=0;i<historysize;i++)
    {
      gnuplot_pitcharea[i]=0;
      gnuplot_pitcharea3[i]=0;
      gnuplot_meanspeed[i]=0;
      gnuplot_zdotarea[i]=0;
      gnuplot_rollarea[i]=0;
      gnuplot_rollarea5to8[i]=0;
      gnuplot_avpitch[i]=0;
      gnuplot_avpitch3[i]=0;
      gnuplot_pitchsp[i]=0;
      gnuplot_avroll5[i]=0;
      gnuplot_avpitch10[i]=0;
    }

  for(int i=0;i<DBS_NUM_VALUES;i++)
    {
      pitchav[i]=0;
      pitch3av[i]=0;
      roll5av[i]=0;
    }
 
  for(int i = 0;i<AVESIZE;i++)
    {
      hpitcha[i]=0;
      hpitch3a[i]=0;
      hroll5a[i]=0;
      hpitch10a[i]=0;
    } 
  // initialize FFT plannner
  fftw_complex *in;
  fftw_complex *out;
  fftw_plan p;
  in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  p = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);  
  // initialize gnuplot handle
  gnuplot_ctrl* handle;
  handle = gnuplot_init();
  gnuplot_resetplot(handle);
  gnuplot_setstyle(handle, "lines");
  gnuplot_ctrl* handle2;
  handle2 = gnuplot_init();
  gnuplot_resetplot(handle2);
  gnuplot_setstyle(handle2, "lines");

  // main loop
  while (true)
    {
      // get last DBS_NUM_VALUES state values and store them in our buffer,
      // using GetLastNValues(DBS_NUM_VALUES,destination
      GetLastNValues(DBS_NUM_VALUES,buffer);
      
      // copy the pitch from our buffer to "in"
      for(int i = 0; i < DBS_NUM_VALUES; i++)
	{
	  in[i][0] = buffer[i].Pitch;
	  gnuplot_pitch[i] = buffer[i].Pitch;
	  gnuplot_roll[i] = buffer[i].Roll+.1;
	  gnuplot_zdot[i] = buffer[i].Vel_D;
	}
 
      // do the FFT
      fftw_execute(p);      
      // plot it :)
      PrintDebug(in ,out);
      cout << "attempting plot... " << endl;
      for (int i = 0; i < DBS_NUM_VALUES; i++)
	{
	  gnuplot_y[i] = sqrt(out[i][0]*out[i][0]+out[i][1]*out[i][1]);
	}
      gnuplot_y[0]=0;//hack

      cout << "road type: " << GetRoadType()<< endl;

      if(meanspeed>3)
	{
	  numpitch++;
	  for(int i =0;i<DBS_NUM_VALUES;i++)
	    {
	      pitchav[i] += sqrt(out[i][0]*out[i][0]+out[i][1]*out[i][1]);
	      gnuplot_yav[i]= pitchav[i]/numpitch;
	    }
	}
      gnuplot_yav[0]=0;
	      
      for(int i = historysize-1; i>0;i--)
	{
	  gnuplot_pitcharea[i] = gnuplot_pitcharea[i-1];
	  gnuplot_meanspeed[i] = gnuplot_meanspeed[i-1];
	  gnuplot_pitcharea3[i] = gnuplot_pitcharea3[i-1];	  
	  gnuplot_zdotarea[i] = gnuplot_zdotarea[i-1];
	  gnuplot_rollarea[i] = gnuplot_rollarea[i-1];
	  gnuplot_rollarea5to8[i] = gnuplot_rollarea5to8[i-1];
	  gnuplot_avpitch[i] = gnuplot_avpitch[i-1];
	  gnuplot_avpitch3[i] = gnuplot_avpitch3[i-1];
	  gnuplot_avroll5[i] = gnuplot_avroll5[i-1];
	  gnuplot_pitchsp[i] = gnuplot_pitchsp[i-1];
	  gnuplot_avpitch10[i] = gnuplot_avpitch10[i-1]; 
	}

      avpitcharea = 0;
      avpitch3=0;
      avroll5=0;
      for(int i = AVESIZE-1; i>0;i--) // move histories for averages
	{
	  avpitcharea += hpitcha[i];
	  hpitcha[i]= hpitcha[i-1];
	  
	  avpitch3 += hpitch3a[i];
	  hpitch3a[i]= hpitch3a[i-1];

	  avroll5 += hroll5a[i];
	  hroll5a[i]= hroll5a[i-1];
	  
	  avpitch10 += hpitch10a[i];
	  hpitch10a[i] = hpitch10a[i-1];
	}
      hpitcha[0] = pitcharea;              // update current  value for history array
      hpitch3a[0] = pitcharea3/pitcharea;
      hroll5a[0] = rollarea5to8/rollarea;

      avpitcharea += pitcharea;
      avpitcharea = avpitcharea/AVESIZE;
      
      avpitch3 += pitcharea3/pitcharea;
      avpitch3 = avpitch3/AVESIZE;
      
      avroll5 += rollarea5to8/rollarea;
      avroll5 = avroll5/AVESIZE;

      avpitch10 += pitch10;
      avpitch10 = avpitch10/AVESIZE;
      
	
      gnuplot_avpitch[0] = avpitcharea;
      gnuplot_avpitch3[0] = avpitch3;
      gnuplot_avroll5[0] = 10*avroll5;
      gnuplot_avpitch10[0] = 10*avpitch10;
      

      gnuplot_pitcharea[0] = pitcharea;
      gnuplot_meanspeed[0] = .1*meanspeed;
      gnuplot_pitcharea3[0]= pitcharea3/pitcharea;
      gnuplot_zdotarea[0] = .1*Zdotarea;
      gnuplot_rollarea[0] = rollarea;
      gnuplot_rollarea5to8[0] = 10*rollarea5to8/rollarea;
      gnuplot_pitchsp[0] = 10*pitcharea/meanspeed;

      //gnuplot_plot_x(handle, gnuplot_pitcharea, historysize, "Pitcharea");
      gnuplot_plot_x(handle, gnuplot_meanspeed, historysize, "Meanspeed");
      //gnuplot_plot_x(handle, gnuplot_pitcharea3, historysize, "Pitcharea3");
      //gnuplot_plot_x(handle, gnuplot_zdotarea, historysize, "PitchA3normed");
      //gnuplot_plot_x(handle, gnuplot_pitchsp, historysize, "Pitch/Speed");
      //gnuplot_plot_x(handle, gnuplot_zdotarea, historysize, "Zdotarea");
      //gnuplot_plot_x(handle, gnuplot_rollarea, historysize, "Rollarea");
      //gnuplot_plot_x(handle, gnuplot_rollarea5to8, historysize, "Rollarea5to8");
      
      gnuplot_plot_x(handle, gnuplot_avpitch, historysize, "avPitch");
      gnuplot_plot_x(handle, gnuplot_avpitch3, historysize, "avPitch3");
      gnuplot_plot_x(handle, gnuplot_avroll5, historysize, "avRoll5");
      gnuplot_plot_x(handle, gnuplot_avpitch10, historysize, "avPitch10");      

      //gnuplot_plot_x(handle2, gnuplot_y, DBS_NUM_VALUES/2, "Pitch");
      //gnuplot_plot_x(handle2, gnuplot_yav, DBS_NUM_VALUES/2, "AvPitch");
      //gnuplot_plot_x(handle2, gnuplot_pitch, DBS_NUM_VALUES, "Pitch");
      //gnuplot_plot_x(handle2, gnuplot_roll, DBS_NUM_VALUES, "Roll");
      //gnuplot_plot_x(handle2, gnuplot_zdot, DBS_NUM_VALUES, "Zdot");      
      sleep(1);
      gnuplot_resetplot(handle);
      gnuplot_resetplot(handle2);
    }

  // at end, free the memory
  fftw_destroy_plan(p);
  fftw_free(in); fftw_free(out);
  delete [] buffer;
  delete [] gnuplot_y;
  delete [] gnuplot_pitcharea;
  delete [] gnuplot_meanspeed;
  delete [] gnuplot_pitcharea3;
  delete [] gnuplot_zdotarea;
  delete [] gnuplot_rollarea;
  delete [] gnuplot_rollarea5to8;

  gnuplot_close(handle);  // deletes files in /tmp or /usr/tmp or something
}



void DBS::PrintValue(fftw_complex* fft) 
{ int n;  
 for(n=0;n<DBS_NUM_VALUES;n++){
      cout << "fft[n][0]* = " << fft[n][0] << endl;
    }    
}



void DBS::PrintDebug(fftw_complex* in, fftw_complex* out)
{
  cout << setw(4) << "number" 
       << setw(13) << "in(real)" 
       << setw(13) << "in(imag)" 
       << setw(13) << "out(real)"
       << setw(13) << "out(imag)"
       << endl;
  for (int i = 0; i < DBS_NUM_VALUES; i++)
    {
      cout << setw(4) << i
	   << setw(13) << in[i][0]
	   << setw(13) << in[i][1]
	   << setw(13) << out[i][0]
	   << setw(13) << out[i][1]
	   << endl;
    }
}



void DBS::PrintValueSum(fftw_complex* fftA)  
{
int n  ;
 double sumfft = 0;
 for(n=0;n<DBS_NUM_VALUES;n++)
   {
     
     double fft =fftA[n][0];
     sumfft =sumfft +fft;
     
     
   }
 cout<<"sumfft"<<sumfft<<endl; 
}

int DBS::GetRoadType()
{

  double pitch[DBS_NUM_VALUES];
  double roll[DBS_NUM_VALUES];
  double zdot[DBS_NUM_VALUES];
  roadtype=-1;
  pitcharea=0;
  pitchhighestValue=0;
  // make a new chunk of memory to use as a buffer for the last
  // N values
  VehicleState* buffer = new VehicleState[DBS_NUM_VALUES];  

  // get last DBS_NUM_VALUES state values and store them in our buffer,
  // using GetLastNValues(DBS_NUM_VALUES,destination
  GetLastNValues(DBS_NUM_VALUES,buffer);

  // do this:
  fftw_complex *in;
  fftw_complex *out;
  fftw_plan fft;
  in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  
  // copy the pitch from our buffer to "in"
  int n;
  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      in[n][0] = buffer[n].Pitch;
      in[n][1] = 0;
    }
  
  // do the fft
  fft = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(fft);

  // do something with the fft results
  PrintDebug(in, out);
  PrintValueSum(out);
  
  for( int n=1;n<DBS_NUM_VALUES;n++)
    { 
      pitch[n] = sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]);
    }
  pitch[0]=0;
  for( int n=0;n<DBS_NUM_VALUES;n++)
    {
      pitcharea += pitch[n];
      //cout << "n: " << n << " abs: " << sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]) << " real: " << out[n][0] << "complex: " << out[n][1] << endl;
      if (pitch[n] > pitchhighestValue)
	{
	  pitchhighestValue = pitch[n];
	}
    }
  pitcharea3= 0;
  for( int n=0;n<3;n++)
    {
      pitcharea3 += pitch[n]; 
    }
  pitch10=0;
  for(int n=10;n<DBS_NUM_VALUES;n++)
    {
      pitch10 += pitch[n];
    }
  
for(n=0;n<DBS_NUM_VALUES;n++)
    {
      in[n][0] = buffer[n].Roll;
    }
  
  // do the fft
  fft = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(fft);

  for( int n=0;n<DBS_NUM_VALUES;n++)
    { 
      roll[n] = sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]);
    }

  // do something with the fft results
  //PrintDebug(in, out);
  //PrintValueSum(out);
  rollarea=0;
  for( int n=0;n<DBS_NUM_VALUES;n++)
    {
      rollarea += roll[n];
      if (roll[n] > rollhighestValue)
	{
	  rollhighestValue = roll[n];
	}
    }
  rollarea5to8= 0;
  for( int n=4;n<7;n++)
    {
      rollarea5to8 += roll[n]; 
    }

  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      in[n][0] = buffer[n].Vel_D;
    }
  
  // do the fft
  fft = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(fft);

  for( int n=0;n<DBS_NUM_VALUES;n++)
    { 
      zdot[n] = sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]);
    }

  Zdotarea=0;

  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      Zdotarea += zdot[n];
    }

  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      meanspeed += sqrt(buffer[n].Vel_N*buffer[n].Vel_N + buffer[n].Vel_E*buffer[n].Vel_E);
    }
  meanspeed = meanspeed/DBS_NUM_VALUES;
  cout << "pitcharea is: " << pitcharea << " pitchhigestValue is: " << pitchhighestValue << endl;
  if(pitcharea<.5) //fix me
    {
      roadtype = 0;
    }
else  if(pitchhighestValue >1)
    {
      roadtype=3;
    }
  else 
    {
      roadtype = -1;
  }

// at end, free the memory
  fftw_destroy_plan(fft);
  fftw_free(in); fftw_free(out);
  delete [] buffer;
  return roadtype; 
}  
