#ifndef SC_SPECS_H
#define SC_SPECS_H

#include <math.h>

/** this should contain "_(type, name, init value)\" **/
#define SPECLIST(_) \
  /*** TOP-LEVEL SUPERCON DEFINES ***/ \
  _( double, SUPERCON_FREQ, 5.0 ) \
  /* (fewest log messages) --> LOG_ERROR = 0, LOG_WARNING = 1, LOG_INFORM = 2, LOG_ANNOY = 3 --> (most log messages) */ \
  _( int, SPARROW_LOG_LEVEL, 5 ) \
  /* Units = Meters/Second */ \
  _( double, TERRAIN_MAP_ZERO_SPEED, 0.4 ) \
  \
  \
  \
  /*** DEFINES USED BY DIAGNOSTIC RULES ***/ \
  _( double, WHEEL_SPIN_DETECTION_THRESHOLD_SPEED_MS, 5 ) \
  _( double, WHEEL_SPIN_WHEELS_FASTER_FACTOR, 1.25 ) \
  /* Factor used to control the threshold used to determine whether sufficient force \
   * has been applied to move Alice up whatever incline she is currently on, the \
   * threshold is set to the force required to move Alice up a hill = (her current \
   * incline * this factor), NOTE: this should be >1 (% factor) \
   */ \
  _( double, ENGINE_FORCE_GREATER_FACTOR, 1.25 ) \
  /* THIS IS A HACK VALUE!! NEED A PROPER ONE! */ \
  _( double, ASTATE_CONFIDENCE_POOR_THRESHOLD, 0.4 ) \
  /* This is the slowest speed in M/S that the TrajFollower can maintain, speeds \
   * below this are assumed to be zero in some form  */ \
  /* THIS IS A HACK VALUE!! NEED A PROPER ONE! */ \
  _( double, SLOWEST_MAINTAINABLE_ALICE_SPEED, 0.5 ) \
  _( double, SLOWEST_MAINTAINABLE_WHEEL_SPEED, 0.5 ) \
  \
  \
  \
  \
  /*** DEFINES USED IN STRATEGY CLASSES  ***/ \
  \
  /** GPSreAcq Strategy **/ \
  /* THIS IS A HACK VALUE!! NEED A PROPER ONE */ \
  _( int, GPSREACQ_PERSIST_TRUE_COUNT, 10 ) \
  \
  /** LoneRanger Strategy **/ \
  /* Speed at which to attempt to drive through obstacles [METERS/SECOND] */ \
  _( double, LONE_RANGER_MIN_SPEED_MS, 2.0 ) \
  \
  /** LturnReverse Strategy **/ \
  /* Distance to reverse (along the path we came in on) for an L-turn */ \
  _( double, LTURN_REVERSE_DISTANCE_METERS, 15.0 ) \
  \
  /** Nominal Strategy **/ \
  \
  /** SlowAdvance Strategy **/ \
  /* Speed [meters/second] below which the maps can reasonably be considered \
   * to be 'high-quality' as sensor data gathered at slower speeds will be more \
   * heavily weighted than sensor data gathered at higher speeds, and reduced \
   * high frequency oscillations etc */ \
  _(double, HIGH_ACCURACY_MAPS_SPEED_MS, 7.0 ) \
  \
  /** UnseenObstacle Strategy **/ \
  /* THIS IS A HACK VALUE!! NEED A PROPER ONE */ \
  _( int, UNSEENOBSTACLE_PERSIST_TRUE_COUNT, 10 ) \
  /* This define should be ZERO if the center of the REAR-AXLE is being used, \
   * and if this is NOT the case then this define should be the offset in METERS \
   * (TOWARDS THE FRONT AXLE = +VE) from the center of the rear axle (assumes that \
   * vehicle origin is along the center-line (line joining the centers of both axles) \
   * of the vehicle) of the actual vehicle origin. */ \
  _( double, OFFSET_OF_VEHICLE_ORIGIN_FROM_REAR_AXLE, 0.0 ) \
  /* Number of points along the bumper for which map-delta updates are sent when an \
   * unseen-obstacle has been detected (should be even) */ \
  _( int, NUM_POINTS_TO_EVALUATE_ALONG_BUMPER, 42 ) \
  /* cost-value assigned to a cell in the superCon map layer to denote a superCon \
   * obstacle */ \
  _( double, SUPERCON_OBSTACLE_SPEED_MS, 0.001 )  \
  \
  /** WheelSpin Strategy **/ \
  /* THIS IS A HACK VALUE!! NEED A PROPER ONE */ \
  _( int, WHEELSPIN_PERSIST_TRUE_COUNT, 10 ) \
  \
  /** Multiple Strategies **/ \
  /* THIS IS A HACK VALUE!! NEED A PROPER ONE */ \
  _( int, PLN_NOT_NFP_PERSIST_TRUE_COUNT, 10 )

#define EXTERNDEFINESPEC(type, name, val) \
extern type name;

#define DEFINESPEC(type, name, val) \
type name = val;

#define EXTERNDEFINESPECS namespace specs { SPECLIST(EXTERNDEFINESPEC) }; using namespace specs
#define DEFINESPECS namespace specs { SPECLIST(DEFINESPEC)	}; using namespace specs


EXTERNDEFINESPECS;


extern void readspecs(void);

#endif //SC_SPECS_H
