#ifndef INTERFACE_SUPERCON_ASTATE_HH
#define INTERFACE_SUPERCON_ASTATE_HH

//FILE DESCRIPTION: Header file that contains the definitions of data structures that are
//used in direct communication between SuperCon and astate

namespace superCon
{

/* Use the scMessage method */
//Used for astate --> superCon (conditional) messages *using the scMessage method/message
//ONLY* - these enums should be used in the msgID field of the scMessage, ALSO
//the msgID field only takes an integer - HENCE you should RECAST one of the
//enums (type: scMsgID_AST) - to an integer (you should recast whichever member
//of the enum details what you want to do - superCon will then perform the inverse
//recast statement so that the code on either side is cleaner - there are reasons
//why this has to be done)
#define ASTATE_SC_MSG_IDS_LIST(_) \ 
  _( need_to_stop_gps_stabilisation, = 0 ) \
  _( ready_to_include_gps, = 1 ) \
  _( astate_happy_panda, = 2 )
DEFINE_ENUM(scMsgID_AST, ASTATE_SC_MSG_IDS_LIST)

/* Use the standard skynet message type: SNsuperconAstateCmd */
//Used for superCon --> astate messages
#define SC_ASTATE_CMDS_LIST(_) \
  _( ok_to_add_gps, = 0 )
DEFINE_ENUM(SC_astateCmdType, SC_ASTATE_CMDS_LIST )

    //struct passed in the control message: superCon --> astate
    //in the SNsuperconAstateCmd message type
    struct superconAstateCmd {
      
      SC_astateCmdType commandType;
      //keep as a struct in case of expansion
    };
}

#endif
