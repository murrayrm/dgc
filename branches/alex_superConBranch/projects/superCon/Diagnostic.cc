#include "Diagnostic.hh"
#include "sparrowhawk.hh"

#include "StrategyGPSreAcq.hh"
#include "StrategyLoneRanger.hh"
#include "StrategyLturnReverse.hh"
#include "StrategyNominal.hh"
#include "StrategySlowAdvance.hh"
#include "StrategyUnseenObstacle.hh"
#include "StrategyWheelSpin.hh"

#include "sc_specs.h"

#include "smart_counter.hh"

#include "debug.h"   //Sparrow debug interface

#include <sys/time.h>
#include <iostream>
#include <stdarg.h>

using namespace std;

CDiagnostic::CDiagnostic( int skynet_key, bool usrSpef_silentOptionSelected, bool nodisp )
  : CSkynetContainer(MODsupercon, skynet_key), CSuperConClient("SC ")
{
  //(sole) Strategy Interface class instance
  m_pStrategyInterface = new CStrategyInterface( skynet_key, &m_SCstate, &m_diagnostics, this, usrSpef_silentOptionSelected );

  runSparrowHawkDisplay = !nodisp;

  m_playback=false;
  //Reset all strategy pointers
  for(int i=0; i<(int)StrategyLast; ++i) {
    m_strategies[i].name = "";
    m_strategies[i].pStrategy = 0;
  }

  //Reset log data
  m_logging_enabled=false;
  m_logfile_state=0;
  m_logfile_log=0;

  //Reset debug tab
  m_debug.is_step_forward=false;
  m_debug.do_steps=0;
  m_debug.do_steps_reset=1;
  m_debug.cycles=0;
  m_debug.start_time=0;
  m_debug.cur_time=0;
  m_debug.playback_hz=10;

  DGCcreateCondition(&m_SCstate_event);
  DGCcreateMutex(&m_SCstate_mutex);  
}

CDiagnostic::~CDiagnostic()
{
  //Delete all registered strategies
  for(int i=0; i<(int)StrategyLast; ++i) {
    if(m_strategies[i].pStrategy)
      delete m_strategies[i].pStrategy;
    m_strategies[i].pStrategy = 0;
  }

  DGCdeleteCondition(&m_SCstate_event);
  DGCdeleteMutex(&m_SCstate_mutex);
}

//////////////////////////////////////////////////////////////////////////////////
//Hands over execution to diagnostics class... assumes the state server is running
void CDiagnostic::run()
{
  init();
  
  //Enter nominal state
  m_current_strategy = StrategyNominal;
  if(m_strategies[m_current_strategy].pStrategy)
    m_strategies[m_current_strategy].pStrategy->enter(m_pStrategyInterface);

  SuperConLog().log( DGN, WARNING_MSG, "CDiagnostic::run - entering main message loop" );

  smart_counter<int> cnt;               //Freq. tracker

  //Enter message pump
  pthread_mutex_lock(&m_SCstate_mutex);
  while(1) {
    if(!m_playback)
      wait_for_state();    //Request new state copy

    do_loging();

    //Used to limit the frequency of supercon
    unsigned long long start;
    DGCgettime(start);
    
    SuperConLog().log( DGN, SC_STATE_DIAGNOSTIC_TABLE_UPDATES, "CDiagnostic::run - new state data received, calculate diagnostics");
    
    updateDiagnostics(m_SCstate, m_diagnostics);

    wait_loging();

    //Update the global position of the superCon map instance so that it is centered
    //on Alice's current position
    m_pStrategyInterface->updateVehPosInSuperconMap(m_SCstate);

    if(m_strategies[m_current_strategy].pStrategy) {
      m_strategies[m_current_strategy].pStrategy->stepForward(&m_diagnostics, 
							      m_pStrategyInterface);
    } else {
      SuperConLog().log( DGN, ERROR_MSG, "Diagnostics module currently in incorrect" \
			 "strategy: %i transition to nominal", m_current_strategy);
      transitionStrategy(StrategyNominal, "Transition from invalid strategy to nominal");
    }
    
    ++cnt;
    log(SC_STATE_DIAGNOSTIC_TABLE_UPDATES, "Supercon diagnostics running at: %f Hz", cnt.freq);
    
    //Pause execution
    unsigned long long end;
    DGCgettime(end);
    
    if(!m_playback) {
      if((end-start)< 1e6/SUPERCON_FREQ)
	usleep( static_cast<unsigned int>(1e6/SUPERCON_FREQ - (end-start)) );
    } else {
      if((end-start)< 1e6/m_debug.playback_hz)
	usleep( static_cast<unsigned int>(1e6/m_debug.playback_hz - (end-start)) );
    }
  }

  pthread_mutex_unlock(&m_SCstate_mutex);
}

//////////////////////////////////////////////////////////////////////////////////
//   Internal functions related to strategies and rules

void CDiagnostic::wait_for_state()
{
  m_SCstate_new=false;
      
  while(!m_SCstate_new) {
    //Send message demanding new data
    scMessage( int( copy_scstate_table ), &m_SCstate, &m_SCstate_new, &m_SCstate_mutex, &m_SCstate_event);
    
    log(SC_STATE_DIAGNOSTIC_TABLE_UPDATES, "CDiagnostic::run - sent request for new state data");
    
    //wait for state to be updated, then relock the mutex
    timespec ts;
    timeval tv;
    gettimeofday(&tv, NULL);
    ts.tv_sec = tv.tv_sec+1;   //Wait for state for max 1s
    ts.tv_nsec = tv.tv_usec*1000;
    
    pthread_cond_timedwait(&m_SCstate_event, &m_SCstate_mutex, &ts);
    
    if(!m_SCstate_new) {
      log(ERROR_MSG, "CDiagnostic::run - Error receiving new state data, re-requiring state from CStateServer");
    }
  }
}

void CDiagnostic::do_loging()
{
  if(!m_playback) {
    //Log the state and diagnostics data
    SuperConLog().writeState(&m_SCstate);
  } else {
    /*    //Playback data from timber
    
    string path = getPlaybackDir();
    if(checkNewPlaybackDirAndReset() || !m_logfile_state) {
      //Reopen the log files in correct directory
      if(m_logfile_state)
	fclose(m_logfile_state);
      
      string file = path + "state.log";
      m_logfile_state = fopen(file.c_str(), "r");
      if(!m_logfile_state) {
	SparrowHawk().log("Unable to open logfile '%s' for playback", file.c_str());
	cerr<<"Unable to open logfile '"<<file<<"' for playback"<<endl;
	return;   //Terminate session!
      }
      
      SparrowHawk().set_string("dbg_file", file.c_str());
      m_debug.start_time=0;
      m_debug.cycles=0;
    }
    
    //Read the next valid line
    static char buf[2048];
    do {
      fscanf(m_logfile_state, "%s", buf);
      if(buf[0]=='#') {  //Skip line
	fgets(buf, sizeof(buf), m_logfile_state);
	continue;
      } else
	break;

      if(feof(m_logfile_state)) {
	printf("Error in logfile, end of file found");
	return;
      }
    } while(1);

    sscanf(buf, "%lli", &m_debug.cur_time);
    m_SCstate.log_read(m_logfile_state);

    if(m_debug.start_time<0)
      m_debug.start_time = m_debug.cur_time;
    */  
  }
}

void CDiagnostic::wait_loging()
{/*
  if(m_playback) {   //Wait for timber to signal our playback, or the debug tab to step us forward
    if(m_debug.start_time<0)
      m_debug.start_time=m_debug.cur_time;
    
    if(m_debug.is_step_forward) {   //Wait for step forward signal
      while(m_debug.do_steps<=0)
	usleep(50000);

      --m_debug.do_steps;
    } else {
      blockUntilTime(m_debug.cur_time);
    }
    
    ++m_debug.cycles;
    }*/
}


//Helper functions to dynamically create specs page
#define SPEC_CNT_FNC(type, name, val)  +1
#define SPEC_CNT(list)   list(SPEC_CNT_FNC)

//Struct to handle logging of specs variable changes
template<class T>
struct spec_log_changed
{
  spec_log_changed() {}
  spec_log_changed(const char *n, T *p) : name(n), v(p) {}
  void operator()() { SuperConLog().log( DGN, WARNING_MSG, "SPECS CHANGED: %s", name); }

  const char *name;
  T *v;
};

template<>
struct spec_log_changed<int>
{
  spec_log_changed() {}
  spec_log_changed(const char *n, int *p) : name(n), v(p) {}
  void operator()() { SuperConLog().log( DGN, WARNING_MSG, "SPECS CHANGED: %s = %i", name, *v); }

  const char *name;
  int *v;
};

template<>
struct spec_log_changed<double>
{
  spec_log_changed() {}
  spec_log_changed(const char *n, double *p) : name(n), v(p) {}
  void operator()() { SuperConLog().log( DGN, WARNING_MSG, "SPECS CHANGED: %s = %f", name, *v); }

  const char *name;
  double *v;
};

template<>
struct spec_log_changed<bool>
{
  spec_log_changed() {}
  spec_log_changed(const char *n, bool *p) : name(n), v(p) {}
  void operator()() { SuperConLog().log( DGN, WARNING_MSG, "SPECS CHANGED: %s = %s", name, *v ? "true" : "false"); }

  const char *name;
  bool *v;
};

#define SPEC_SPARROW_FNC(type, name, val)				\
  SparrowHawk().make_label(de++, row, cols[0], "| " #name ":");		\
  SparrowHawk().make_edit(de, row, cols[1], &name);			\
  SparrowHawk().set_notify(de++, spec_log_changed<type>(#name, &name));	\
  SparrowHawk().make_label(de++, row, 80, "|");				\
  ++row;

#define SPEC_SPARROW(list)  list(SPEC_SPARROW_FNC)

int specs_sparrow_items() {
  int cnt = 0 + SPEC_CNT(SPECLIST);
  return 3*cnt+2;
}

void specs_sparrow_create(display_entry *de, int row)
{
  int cols[2] = {1, 50};

  SparrowHawk().make_label(de++, row, 1, "+-------------------------------+-------+-------------------------------+------+");
  ++row;

  SPEC_SPARROW(SPECLIST)

  SparrowHawk().make_label(de++, row, 1, "+-------------------------------+-------+-------------------------------+------+");
  ++row;
}

//Main init file
void CDiagnostic::init()
{
  //Initialize sparrow pages so stretegies are able to do binds
  //SparrowHawk().add_page();

  if(m_playback) {
    SparrowHawk().add_page(debug_tbl, "Debug");
    SparrowHawk().set_readonly("dbg_file");
    SparrowHawk().set_getter("dbg_time", this, &CDiagnostic::get_debug_elapsed);
    SparrowHawk().set_readonly("dbg_time");
    SparrowHawk().rebind("dbg_cycle", &m_debug.cycles);
    SparrowHawk().set_readonly("dbg_cycle");
    SparrowHawk().rebind("dbg_is_step", (bool*)&m_debug.is_step_forward);
    SparrowHawk().rebind("dbg_steps", (int*)&m_debug.do_steps);
    SparrowHawk().set_readonly("dbg_steps");
    SparrowHawk().rebind("dbg_steps_reset", &m_debug.do_steps_reset);
    SparrowHawk().rebind("dbg_playback_hz", &m_debug.playback_hz);
    SparrowHawk().set_notify("Step", this, &CDiagnostic::do_debug_step);

    SparrowHawk().set_keymap(KEY_A('p'), this, &CDiagnostic::do_debug_step);
    SparrowHawk().set_keymap(KEY_A('P'), this, &CDiagnostic::do_debug_step);
  }

  //Dynamicaly create pages
  display_entry *de = SparrowHawk().create_page("State Table", m_SCstate.sparrow_items());
  m_SCstate.set_sparrow(de, 1);

  de = SparrowHawk().create_page("Diagnostics Table", m_diagnostics.sparrow_items());
  m_diagnostics.set_sparrow(de, 1);

  de = SparrowHawk().create_page("Specs", specs_sparrow_items());
  specs_sparrow_create(de, 1);

  //Add keymaps
  SparrowHawk().set_keymap(KEY_A('d'), &SparrowHawk(), &CSparrowHawk::tab_select, "Diagnostics Table");
  SparrowHawk().set_keymap(KEY_A('D'), &SparrowHawk(), &CSparrowHawk::tab_select, "Diagnostics Table");
  SparrowHawk().set_keymap(KEY_A('s'), &SparrowHawk(), &CSparrowHawk::tab_select, "State Table");
  SparrowHawk().set_keymap(KEY_A('S'), &SparrowHawk(), &CSparrowHawk::tab_select, "State Table");

  /** INITIALISE ALL STRATEGIES **/
  registerStrategy(StrategyNominal, "Nominal", new CStrategyNominal());
  registerStrategy(StrategyLoneRanger, "LoneRanger", new CStrategyLoneRanger());
  registerStrategy(StrategyLturnReverse, "LturnReverse", new CStrategyLturnReverse());
  registerStrategy(StrategySlowAdvance, "SlowAdvance", new CStrategySlowAdvance());
  registerStrategy(StrategyUnseenObstacle, "UnseenObstacle", new CStrategyUnseenObstacle());
  registerStrategy(StrategyWheelSpin, "WheelSpin", new CStrategyWheelSpin());

  //return;
  //Start the sparrow display
  if( runSparrowHawkDisplay == true ) {
    SparrowHawk().run();
  }
}

/*** Update all diagnostic rules using the latest copy of the SuperCon state ***/
void CDiagnostic::updateDiagnostics(const SCstate &SCstate, SCdiagnostic &diag)
{
  /*** DIAGNOSTIC RULES DEFINITION LIST ***/
  //NOTE: This list contains the definitions of the LOGICAL TESTS used to
  //determine whether a rule evaluates to TRUE or FALSE, *and* variables to 
  //track the persistence (# of times the conditional has evaluated to TRUE
  //in SEQUENTIAL supercon evaluation cycles - these are RESET whenever
  //the conditional element for which they are the persistence figure evaluates
  //to FALSE) *only* boolean variables are allowed for CONDITIONAL ELEMENTS, 
  //and *only* integer vars are allowed for PERSISTENCE ELEMENTS.  

  //Additional Notes:
  //1. NO default values exist for diagnostic table elements
  //2. NO altered variables retain knowledge of their previous value
  //2b. The caveat to (2.) is the persistence elements of the diagnostic table
  //3. The table is defined in the SuperCon.hh header file

  
  /** 'STANDARD' DIAGNOSTIC TABLE CONDITIONAL ELEMENTS  **/

  //Engine started/off? -> engine on = TRUE                                                                                                            
  diag.engineStatus = ( SCstate.timeSinceEngineStart > 0 );        

  //Is the engine force > force required to move Alice UP the current incline + #defined % -> actual force > required = TRUE
  //NOTE: This is dependent upon the accuracy of the force from adrive, which is
  //likely to only be accurate at low speeds, as the torque converter % is not yet
  //available from OBD2, hence it's % is probably hard-coded in adrive
  diag.engineForceForIncline = ( SCstate.forceAppliedtoWheels > ( SCstate.componentOfWeightDownHill * ENGINE_FORCE_GREATER_FACTOR ) );

  //Is astate's !confidence > BAD threshold (defined in specsfile) ? -> astate confidence < BAD = TRUE
  //NOTE: [!confidence > threshold] form is used as the astate confidence is a variance of our estimate
  //(hence = 0 is the best, and > 0 --> worse)
  diag.astateConfidencePoor = ( SCstate.astateINVconfidence > ASTATE_CONFIDENCE_POOR_THRESHOLD );

  //Is Alice stationary && is the traj telling her to stop
  diag.validAliceStop = ( SCstate.vehicleSpeed <= SLOWEST_MAINTAINABLE_ALICE_SPEED && SCstate.wheelSpeed <= SLOWEST_MAINTAINABLE_WHEEL_SPEED && SCstate.currentSpeedRef <= SLOWEST_MAINTAINABLE_ALICE_SPEED && SCstate.currentAccelCmd <= 0 );

  //Is Alice currently stationary && is the accel command < ZERO? (braking) --> if all conditions true then -> TRUE
  diag.aliceStationary = ( SCstate.vehicleSpeed <= SLOWEST_MAINTAINABLE_ALICE_SPEED && SCstate.wheelSpeed <= SLOWEST_MAINTAINABLE_WHEEL_SPEED && SCstate.currentAccelCmd <= 0 );

  //Is superCon e-stop == pause? -> superCon e-stop = pauseD --> = TRUE
  diag.superConPaused = ( SCstate.estopSuperConPos == estp_pause );
  
  //Is Alice paused by DARPA/adrive e-stop NOT superCon
  diag.pausedNOTsuperCon = ( SCstate.estopSuperConPos != estp_pause && SCstate.estopDARPAPos == estp_pause || SCstate.estopAdrivePos == estp_pause );

  //Is the TrajFollower in FORWARDS mode? -> TRUE if in FORWARDS mode
  diag.trajFForwards = ( SCstate.currentTFmode == int(tf_forwards) );

  //Is the TrajFollower in reverse mode? -> TRUE if in reverse mode
  diag.trajFReverse = ( SCstate.currentTFmode == int(tf_reverse) );

  //Is the current speed-cap being used by the trajFollower a MIN speedcap && speed == LoneRanger strategy drive through obstacles speed? --> TRUE if BOTH conditions true
  /* this was also considered: 'SCstate.trajF_MAXspeedCap == NO_MAX_SPEEDCAP' and was removed - see Bug #2388, as the largest MIN speed-cap should always win over
   * a smaller MAX speed-cap, checking this should not be required */
  diag.trajFLoneRspeedCap = ( SCstate.trajFMINspeedCap == LONE_RANGER_MIN_SPEED_MS );

  //Is the current gear = reverse? -> TRUE iff gear = reverse
  diag.adriveGearReverse = ( SCstate.transmissionPos == int(reverse_gear) );

  //Is the current gear = drive? -> TRUE iff gear = drive
  diag.adriveGearDrive = ( SCstate.transmissionPos == int(drive_gear) );

  //Is the current gear = park? -> TRUE iff gear = park_gear
  diag.adriveGearPark = ( SCstate.transmissionPos == int(park_gear) );

  //Is the current traj output by the PLN valid (i.e. NOT NFP - *for any reason*)
  diag.plnValidTraj = ( SCstate.minCellSpeedOnCurrentTraj > TERRAIN_MAP_ZERO_SPEED );

  //Is the *current* NFP traj, NFP because it passes through a terrain obstacle (and NO SC obstacles)? -> pln NFP due to terrain (only) = TRUE
  diag.plnNFP_Terrain = ( ( SCstate.minCellSpeedOnCurrentTraj <= TERRAIN_MAP_ZERO_SPEED ) && ( SCstate.minCellSpeedOnCurrentTraj > SUPERCON_OBSTACLE_SPEED_MS ) );

  //Is the *current* NFP traj, NFP because it passes through a SuperC. obstacle? -> pln NFP due to SC = TRUE
  diag.plnNFP_SuperC = ( SCstate.minCellSpeedOnCurrentTraj == SUPERCON_OBSTACLE_SPEED_MS );


  /** DIAGNOSTIC RULES THAT CARRY FORWARD STATE TABLE VARIABLES **/
  //Note that as the SCstate table can only support ints, re-casting to bools is used
  
  //Signals TF has successfully reversed for the distance specified by SuperCon and has now stopped the vehicle and awaits further instructions
  diag.tfFinishedReversing = bool(SCstate.tfFinishedReversing);
  
  /* STATE TABLE ELEMENTS SET BY LATER SUPERCON COMPONENTS CARRIED FORWARD INTO DIAGNOSTIC TABLE  */


  //*NO* DIAGNOSTIC TABLE ELEMENTS OTHER THAN STRATEGY TRANSITION CONDITIONS SHOULD BE
  //ADDED *BELOW* THIS LINE



  /**** IMPORTANT NOTE: RULES THAT ARE BELOW THIS POINT ARE SPECIAL - THEY CAN INCLUDE
   **** DEPENDENCIES NOT ONLY ON SC-STATE ELEMENTS, BUT *ALSO* ON OTHER DIAGNOSTIC TABLE
   **** ELEMENTS (NOTE THAT THEY *CANNOT* HAVE DEPENDENCIES UPON OTHER RULES IN THIS
   **** SECTION ****/

  /** STRATEGY TRANSITION CONDITIONAL ELEMENTS **/

  //Strategy: WheelSpin
  diag.transWheelSpin = ( ( SCstate.vehicleSpeed < WHEEL_SPIN_DETECTION_THRESHOLD_SPEED_MS ) && ( SCstate.wheelSpeed > ( SCstate.vehicleSpeed * WHEEL_SPIN_WHEELS_FASTER_FACTOR ) ) );

  //Strategy: UnseenObstacle
  //NOTE: this is currently limited to FORWARDS TRAVEL
  diag.transUnseenObstacle = ( ( SCstate.vehicleSpeed < SLOWEST_MAINTAINABLE_ALICE_SPEED ) && ( SCstate.currentSpeedRef > SLOWEST_MAINTAINABLE_ALICE_SPEED ) && ( SCstate.gasCmd > 0 ) && ( SCstate.transmissionPos == int(drive_gear) ) && ( diag.engineStatus == true ) && ( diag.engineForceForIncline == true ) );

  //Strategy: GPSreAcq (astate GPS re-acquisition/integration into estimate)
  diag.transGPSreAcq = ( SCstate.astReadyToIncludeGPSafterReAcq == SUPERCON_INT_TRUE );

  //Strategy: SlowAdvance (this should ONLY be referenced in the Nominal strategy, as current strategy = nominal is also a condition)
  diag.transSlowAdvance = ( diag.plnNFP_Terrain && !diag.plnNFP_SuperC );

  


  /** ALL DIAGNOSTIC RULE LOGICAL DEFINITIONS SHOULD OCCUR *ABOVE* THIS POINT **/
}

//////////////////////////////////////////////////////////////////////////////////
//Interface functions (for CStrategy etc.)
void CDiagnostic::transitionStrategy(SCStrategy to, const char *reason)
{
  //Transfer to the new strategy

  //Signal leaving the old strategy
  if(m_strategies[m_current_strategy].pStrategy)
    m_strategies[m_current_strategy].pStrategy->leave(m_pStrategyInterface);

  m_current_strategy = to;

  //Signal enter new strategy
  if(m_strategies[m_current_strategy].pStrategy)
    m_strategies[m_current_strategy].pStrategy->enter(m_pStrategyInterface);
}

void CDiagnostic::setDoubleState(double SCstate::*pState, double val)
{
  //Send supercon message to set variable
  scMessage( int( update_double_scstate_value), pState, val);
}

void CDiagnostic::setIntState(int SCstate::*pState, int val)
{
  //Send supercon message to set variable
  scMessage( int( update_int_scstate_value ), pState, val);
}

string CDiagnostic::strategyName(SCStrategy id) const
{
  if((int)id<0 || (int)id>=StrategyLast)
    return string("Unknown strategy");

  if(!m_strategies[id].pStrategy)
    return m_strategies[id].name + "(not regitered!!!)";

  return m_strategies[id].name;
}

//////////////////////////////////////////////////////////////////////////////////
//   Internal helper functions
void CDiagnostic::registerStrategy(SCStrategy id, const char *name, CStrategy *pStrategy)
{
  //Initialize the strategy pointers
  pStrategy->m_ID = id;
  pStrategy->m_pDiag = this;

  //Add to list
  m_strategies[id].name = name;
  m_strategies[id].pStrategy = pStrategy;
}


double CDiagnostic::get_debug_elapsed()
{
  unsigned long long diff = m_debug.cur_time - m_debug.start_time;
  if(m_debug.start_time==0)
    return 0.0;
  return DGCtimetosec(diff);
}

void CDiagnostic::do_debug_step()
{
  if(m_debug.do_steps==0) {
    m_debug.do_steps = m_debug.do_steps_reset;
    SparrowHawk().set_string("Step", "Stop");
  } else {
    m_debug.do_steps = 0;
    SparrowHawk().set_string("Step", "Stop");
  }
}


