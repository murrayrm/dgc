#ifndef STRATEGY_HH
#define STRATEGY_HH

#include "SuperCon.hh"
#include "SuperConLog.hh"
#include "SuperConClient.hh"
#include "bool_counter.hh"
#include "bool_latched.hh"

#include "sc_specs.h"

#include "StrategyInterface.hh"

#include<string>
#include<vector>
using namespace std;

class CDiagnostic;    //Forward declaration
class CStrategyInterface; //Forward declaration

///////////////////////////////////////////////////////////////////////////////
//Class used to map state transfers at compile time
class CStrategyMapper
{
private:
  CStrategyMapper();   //Singleton class... use private default constructor
public:
  CStrategyMapper(SCStrategy from, SCStrategy to, const char *reason);

  static void print(const CDiagnostic &diag);   //Diagnostic used for name lookup

private:
  static CStrategyMapper *pInstance;

  struct Transition {
    SCStrategy to;
    SCStrategy from;
    string reason;
  };
  vector<Transition> m_transition;
};

///////////////////////////////////////////////////////////////////////////////
//Class declares virtual base class for strategies
class CStrategy
{

/** METHODS **/
public:
  
  /*** CONSTRUCTOR & DESTRUCTOR  ***/
  CStrategy();
  virtual ~CStrategy();


  /*** COMMON STRATEGY ACTION METHODS ***/

  //Step one step forward using the suppled diagnostic rules
  //Description: Step forward and execute the next stage of the current strategy:
  //Move to the current strategy and evaluate the termination (if TRUE move 
  //to nominal strategy) and transition (if TRUE move to another (not Nominal) strategy)
  //conditions for the next stage to be executed in the current strategy.
  //IF ALL of the termination & transition criteria evaluate to FALSE
  //then the method will enter and execute the next stage in the current
  //strategy.
  virtual void stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) =0;
 
  //We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
  //Description: Leave (& clean up) the current strategy for another strategy:
  //A termination or transition condition in the current strategy has 
  //evaluated to TRUE (this includes the condition that the end of the
  //current strategy has been reached).  Hence make the new strategy the
  //current strategy, and clean-up the old strategy (one being left) so
  //that all internal variables whose values are  NOT to be retained for the
  //next time that this strategy is used are RESET to their default (initial)
  //values.
  virtual void leave( CStrategyInterface *m_pStrategyInterface ) =0;

  //We are entering this state, do initialization (NO NEW STATE TRANSFERS)
  //Description: Enter (& intialise) the current strategy for the first time (i.e.
  //enter a new strategy):
  //A termination or transition condition in the previous strategy evaluated
  //to TRUE.  Intialise any required internal variables in the
  //current (new) strategy to their starting values
  virtual void enter( CStrategyInterface *m_pStrategyInterface ) =0;


  /** COMMON STRATEGY TERMINATION METHODS **/
  //Termination methods evaluate whether to LEAVE the current strategy, and
  //return to the NOMINAL strategy, and are CONDITIONAL (i.e. IF a test
  //evaluates to true, transition to nominal, otherewise proceed
  
  //terminate current strategy IF planner is producing a trajectory that does
  //NOT pass through either a TERRAIN OR SUPERCON ZERO-SPEED (each use
  //different zeros) area
  bool term_PlannerNotNFP( const SCdiagnostic &diag, const char* StrategySpecificStr );
  
  
  /** COMMON STRATEGY TRANSITION METHODS (move from current strategy to another one) **/
  //Conditional methods that return a boolean, =TRUE iff we should transition to
  //the strategy for which this is the transition method, otherwise they return FALSE
  
  /** --> ANYTIME STRATEGY METHODS **/

  // ? --> UNSEEN OBSTACLE strategy conditional transition method
  // NOTE: THIS IS CURRENTLY SPECIFIC TO DRIVING FORWARDS
  bool trans_UnseenObstacle( CStrategyInterface &m_StrategyInterface, const SCdiagnostic &diag, const char* StrategySpecificStr );
  
  // ? --> WHEEL-SPIN strategy conditional transition method
  bool trans_WheelSpin( CStrategyInterface &m_StrategyInterface, const SCdiagnostic &diag, const char* StrategySpecificStr );

  // ? --> GPS-REACQ (GPS-reacquisition) strategy conditional transition method
  bool trans_GPSreAcq( const SCdiagnostic &diag, const char* StrategySpecificStr );


  /** --> SOMETIME/NOMINAL STRATEGY METHODS **/

  // ? --> SLOW ADVANCE strategy conditional transition method
  bool trans_SlowAdvance( const SCdiagnostic &diag, const char* StrategySpecificStr );


  /* SOMETIME-STRATEGY TERMINATION/TRANSITION CONDITION EVALUATION METHOD  */
  //Method calls all termination & transition evaluation methods (which are common
  //to all *sometime* strategies by definition), and returns a boolean which is
  //TRUE if *ANY* of the termination or transition conditions occur, and FALSE
  //otherwise
  bool checkAll_TransTerms( CStrategyInterface *m_pStrategyInterface, const SCdiagnostic *diag, const char* StrategySpecificStr );


protected:  //Helper functions for inherited classes
  void transitionStrategy(SCStrategy to, const char *reason);
  void doSetDoubleState(double SCstate::*pState, double val);
  void doSetIntState(int SCstate::*pState, int val);
  SCStrategy getID() const { return m_ID; }


/** VARIABLES **/
private:    //Prevents inherited classes from access this member
  SCStrategy m_ID;
  CDiagnostic *m_pDiag;      //These variables are set from CDiagnostic (ie. friend) curring registration

  friend class CDiagnostic;
};


#endif
