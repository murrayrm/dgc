//SUPERCON STRATEGY TITLE: Wheel-Spin - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyWheelSpin.hh"
#include "StrategyHelpers.hh"

void CStrategyWheelSpin::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE

  switch( nextStage ) {

  case 1: //Wheel-spin detected - superCon e-stop pause the vehicle immediately to
          //stop Alice digging herself in.
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "WheelSpin - Stage 1 Completed");
	nextStage++;
      }
    }
    break;
    
  case 2: //wait until e-stop pause applied, and vehicle && wheels are stationary
          //and then transition to UnseenObstacle
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->superConPaused == false || m_pdiag->aliceStationary == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "WheelSpin - Alice not paused, OR not stationary - will poll");
      }
      

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	transitionStrategy( StrategyLoneRanger, "WheelSpin - detected wheel-sping, stopped Alice -> UnseenObstacle to mark the area" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "WheelSpin - Stage 2 Completed");
	nextStage++;
      }
    }
    break;

    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyWheelSpin::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyWheelSpin::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: WheelSpin - default stage reached!");
    }

  }
  
}


void CStrategyWheelSpin::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "WheelSpin - Exiting");
}

void CStrategyWheelSpin::enter( CStrategyInterface *m_pStrategyInterface )
{
  //reset the nextStage counter (starts at ONE, NOT zero) as the
  //strategy is being entered --> first stage to execute should be
  //stage #1
  nextStage = 1;
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "WheelSpin - Entering");
}
