#include"Strategy.hh"
#include"Diagnostic.hh"

#include<iostream>
using namespace std;

///////////////////////////////////////////////////////////////////////////////
//Class used to map state transfers at compile time
CStrategyMapper *CStrategyMapper::pInstance;

CStrategyMapper::CStrategyMapper()
{
}

CStrategyMapper::CStrategyMapper(SCStrategy from, SCStrategy to, const char *reason)
{
  if(!pInstance)
    pInstance = new CStrategyMapper();

  Transition trans;
  trans.to = to;
  trans.from = from;
  trans.reason = reason;

  pInstance->m_transition.push_back(trans);
}


//Diagnostic used for name lookup
void CStrategyMapper::print(const CDiagnostic &diag)
{
  //Print all transitions
  if(!pInstance) {
    cout<<"No singleton instance of CStrategyMapper, are any transitions defined?"<<endl;
    return;
  }

  cout<<"All registered transitions"<<endl;

  for(vector<Transition>::iterator itr = pInstance->m_transition.begin(); itr!= pInstance->m_transition.end(); ++itr) {
    cout<<diag.strategyName(itr->from)<<"("<<itr->from<<") -> "<<diag.strategyName(itr->to)<<"("<<itr->to<<") - "<<itr->reason<<endl;
  }
}

//////////////////////////////////////////////////////////////////////////////
//Class declares virtual base class for strategies

//Default constructor - NOTE requires a pointer to the CStrategyInterface
//(sole) instance instantiated by the (sole) instance of CDiagnostic.
CStrategy::CStrategy()
  : m_ID(StrategyNone), m_pDiag(0)
{
}

//Destructor
CStrategy::~CStrategy()
{
  m_pDiag = 0;
}


/** COMMON STRATEGY TERMINATION METHODS **/
//Termination methods evaluate whether to LEAVE the current strategy, and
//return to the NOMINAL strategy, and are CONDITIONAL (i.e. IF a test
//evaluates to true, transition to nominal, otherewise proceed

//terminate current strategy IF planner is producing a trajectory that does
//NOT pass through either a TERRAIN OR SUPERCON ZERO-SPEED (each use
//different zeros) area
bool CStrategy::term_PlannerNotNFP( const SCdiagnostic &diag, const char* StrategySpecificStr ) {
  
  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TERMINATION METHOD (i.e. what caused the termination
  //ALL of these strings should start with " : "
  static char termPlnNotNFPStr[LOGGING_BUFFER_SIZE] = " : PLN -> !NFP -> transition to Nominal";
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method - the string should detail the STRATEGY & STAGE at
  //which the method is being called in the following format:
  //"STRATEGYNAME - StageSTAGENUMBER" e.g. "SlowAdvance - Stage1"

  bool trueIFterm; //TRUE iff termination conditions met

  if ( diag.plnValidTraj == true && diag.plnValidTraj.count() >= PLN_NOT_NFP_PERSIST_TRUE_COUNT ) {
    
    /* need to terminate current strategy and return to nominal strategy */
    strcat(termPlnNotNFPStr, StrategySpecificStr);
    transitionStrategy( StrategyNominal, termPlnNotNFPStr );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, termPlnNotNFPStr );
    trueIFterm = true;
  } else {
    trueIFterm = false;
  }
  return trueIFterm;
}


// ? --> UNSEEN-OBSTACLE strategy conditional transition method
// NOTE: THIS IS CURRENTLY SPECIFIC TO DRIVING FORWARDS (see diagnostic conditional element)
bool CStrategy::trans_UnseenObstacle( CStrategyInterface &m_StrategyInterface, const SCdiagnostic &diag, const char* StrategySpecificStr ) {

  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char transUnseenObstacleStr[LOGGING_BUFFER_SIZE] = " : Detected Unseen Obstacle -> transition to UnseenObstacle";
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method - the string should detail the STRATEGY & STAGE at
  //which the method is being called in the following format:
  //"STRATEGYNAME - StageSTAGENUMBER" e.g. "SlowAdvance - Stage1"

  bool trueIFtrans;

  if ( diag.transUnseenObstacle == true && diag.transUnseenObstacle.count() >= UNSEENOBSTACLE_PERSIST_TRUE_COUNT ) {
    //Unseen obstacle detected - stop immediately
    m_StrategyInterface.stage_superConEstop( estp_pause );
    
    /* need to transition to UnseenObstacle strategy */
    strcat(transUnseenObstacleStr, StrategySpecificStr);
    transitionStrategy( StrategyUnseenObstacle, transUnseenObstacleStr );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, transUnseenObstacleStr );
    
    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;
}
 

/** COMMON STRATEGY TRANSITION METHODS (move from current strategy to another one) **/
//Conditional methods that return a boolean, =TRUE iff we should transition to
//the strategy for which this is the transition method, otherwise they return FALSE

// ? --> WHEEL-SPIN strategy conditional transition method
// NOTE: THIS IS CURRENTLY SPECIFIC TO DRIVING FORWARDS (see diagnostic conditional element)
bool CStrategy::trans_WheelSpin( CStrategyInterface &m_StrategyInterface, const SCdiagnostic &diag, const char* StrategySpecificStr ) {

  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char transWheelSpinStr[LOGGING_BUFFER_SIZE] = " : Detected WheelSpin -> transition to WheelSpin";
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method - the string should detail the STRATEGY & STAGE at
  //which the method is being called in the following format:
  //"STRATEGYNAME - StageSTAGENUMBER" e.g. "SlowAdvance - Stage1"

  bool trueIFtrans;

  if ( diag.transWheelSpin == true && diag.transWheelSpin.count() >= WHEELSPIN_PERSIST_TRUE_COUNT ) {
    //Wheel-spinning - stop immediately
    m_StrategyInterface.stage_superConEstop( estp_pause );
    
    /* need to transition to WheelSpin strategy */
    strcat(transWheelSpinStr, StrategySpecificStr);
    transitionStrategy( StrategyWheelSpin, transWheelSpinStr );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, transWheelSpinStr );

    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;
}


// ? --> GPS-REACQ (GPS-reacquisition) strategy conditional transition method
bool CStrategy::trans_GPSreAcq( const SCdiagnostic &diag, const char* StrategySpecificStr ) {
  
  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char transGPSreAcqStr[LOGGING_BUFFER_SIZE] = " : AState signalled GPS re-acq. -> transition to GPS-reacquisition";
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method - the string should detail the STRATEGY & STAGE at
  //which the method is being called in the following format:
  //"STRATEGYNAME - StageSTAGENUMBER" e.g. "SlowAdvance - Stage1"

  bool trueIFtrans;

  if ( diag.transGPSreAcq == true && diag.transGPSreAcq.count() >= GPSREACQ_PERSIST_TRUE_COUNT ) {
    
    /* need to transition to GPSreAcq strategy */
    strcat(transGPSreAcqStr, StrategySpecificStr);
    transitionStrategy( StrategyGPSreAcq, transGPSreAcqStr );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, transGPSreAcqStr );

    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;
}


// ? --> SLOW ADVANCE strategy conditional transition method
bool CStrategy::trans_SlowAdvance( const SCdiagnostic &diag, const char* StrategySpecificStr ) {

  //ending section of the action string sent to sparrow display - includes
  //the information specific to THIS TRANSITION METHOD (i.e. what type of transition
  //it is (& where it is going) - ALL of these strings should start with " : "
  static char transSlowAdvanceStr[LOGGING_BUFFER_SIZE] = " : Detected Planner -> NFP from Nominal -> SlowAdvance";
  //the other part (the beginning section of the action string) is passed in the
  //argument to this method - the string should detail the STRATEGY & STAGE at
  //which the method is being called in the following format:
  //"STRATEGYNAME - StageSTAGENUMBER" e.g. "SlowAdvance - Stage1"

  bool trueIFtrans;
  
  if ( diag.transSlowAdvance == true ) {

    /* need to transition to SlowAdvance strategy */
    strcat(transSlowAdvanceStr, StrategySpecificStr);
    transitionStrategy( StrategySlowAdvance, transSlowAdvanceStr );
    SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, transSlowAdvanceStr );

    trueIFtrans = true;
  } else {
    trueIFtrans = false;
  }
  return trueIFtrans;
}



//sometime strategy termination/transition condition evaluation method
bool CStrategy::checkAll_TransTerms( CStrategyInterface *m_pStrategyInterface, const SCdiagnostic *diag, const char* StrategySpecificStr ) {
  
  bool checkBool;
  checkBool = false;

  //NOTE: The ORDER of the transition/termination methods is IMPORTANT! - this is
  //because if any transition/termination method performs its transition/termination
  //then it changes the record of the 'current strategy' being executed, and also
  //leaves() the current strategy, and enters() the new strategy.
  //
  //THE CURRENT-STRATEGY SHOULD BE CHANGED AT MOST *ONCE* BY THIS METHOD
  //(or any method within ONE stepForward() execution) - hence the structure
  //of the code below, *and* the methods are in DECREASING PRIORITY ORDER.

  if( checkBool == false) {
    checkBool = term_PlannerNotNFP( (*diag), StrategySpecificStr );
  }
  if( checkBool == false ) {
    checkBool = trans_UnseenObstacle( (*m_pStrategyInterface), (*diag), StrategySpecificStr );
  }
  if( checkBool == false ) {
    checkBool = trans_WheelSpin( (*m_pStrategyInterface), (*diag), StrategySpecificStr );
  }
  if( checkBool == false) {
    checkBool = trans_GPSreAcq( (*diag), StrategySpecificStr );
  }

  return checkBool;
}


/** HELPER FUNCTIONS FOR INHERITED CLASSES  **/

//This should really be changed so that it isn't a form of wrapper for transitionStrategy in the
//CDiagnostic class...
void CStrategy::transitionStrategy(SCStrategy to, const char *reason)
{
  if(m_pDiag) {
    m_pDiag->transitionStrategy(to, reason);
  } else {
    cerr<< "ERROR: CStrategy::transitionStrategy - Pointer to diagnostics class not instansiated"<<endl;
    SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategy::transitionStrategy - Pointer to diagnostics class not instansiated" );
  }
}

void CStrategy::doSetDoubleState(double SCstate::*pState, double val)
{
  if(m_pDiag) {
    m_pDiag->setDoubleState(pState, val);
  } else {
    cerr<< "ERROR: CStrategy::doSetDoubleState - Pointer to diagnostics class not instansiated" << endl;
    SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategy::doSetDoubleState - Pointer to diagnostics class not instansiated" );
  }
}

void CStrategy::doSetIntState(int SCstate::*pState, int val)
{
  if(m_pDiag) {
    m_pDiag->setIntState(pState, val);
  } else {
    cerr<<"ERROR: CStrategy::doSetIntState - Pointer to diagnostics class not instansiated"<<endl;
    SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategy::doSetIntState - Pointer to diagnostics class not instansiated" );
  }
}
