#ifndef STRATEGYDEFINES_HH
#define STRATEGYDEFINES_HH

//FILE DESCRIPTION: Header file to contain all defines required for strategies
//and/or the transition/termination rules required for the strategies

//Speed at which to attempt to drive through obstacles [METERS/SECOND]
#define LONE_RANGER_MIN_SPEED_MS 2.0
//Distance to reverse (along the path we came in on) for an L-turn
#define LTURN_REVERSE_DISTANCE_METERS 15 //--> int 
//Speed [meters/second] below which the maps can reasonably be considered 
//to be 'high-quality' as sensor data gathered at slower speeds will be more
//heavily weighted than sensor data gathered at higher speeds, and reduced
//high frequency oscillations etc)
#define HIGH_ACCURACY_MAPS_SPEED_MS 7.0 //--> double
//This define should be ZERO if the center of the REAR-AXLE is being used, and if this is NOT the
//case then this define should be the offset in METERS (TOWARDS THE FRONT AXLE = +VE) from the center
//of the rear axle (assumes that vehicle origin is along the center-line (line joining the centers of
//both axles) of the vehicle) of the actual vehicle origin.
#define OFFSET_OF_VEHICLE_ORIGIN_FROM_REAR_AXLE 0.0


#endif
