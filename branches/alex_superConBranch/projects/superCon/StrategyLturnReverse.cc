//SUPERCON STRATEGY TITLE: L-turn Reverse - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyLturnReverse.hh"
#include "StrategyHelpers.hh"

void CStrategyLturnReverse::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE
  
  switch( nextStage ) {

  case 1: //superCon e-stop pause Alice prior to shifting into reverse
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "LturnReverse - Stage1" );

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      //NEED TO CHECK THE RESPONSE TO THIS - NOT HAPPY WITH THIS
      if( m_pdiag->validAliceStop == false ) {
	SuperConLog().log( STR, ERROR_MSG, "ERROR: LturnReverse called when not in a valid stop" );
	cerr << "ERROR: LturnReverse called when Alice not in a valid stop!!- will poll" << endl;
      }
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage 1 Completed" );
	nextStage++;
      }
    }
    break;

  case 2: //send-gear change command to adrive ?-->reverse
    {
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "LturnReverse - Stage2" );
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be to change gear, don't send gear change
	//command until Alice is paused

	/* SHOULD WE RE-PLAY STAGE ONE HERE? - OR JUST WAIT FOR Adrive, OR SHOULD WE
	 * REPLAY STAGE 1 (SEND pause) AFTER A TIMEOUT OF REACHING THIS POINT?
	 * nextStage = 1;*/
	SuperConLog().log( STR, WARNING_MSG, "LoneRanger: superCon e-stop pause not yet in place -> will poll" );
	skipStageOps = true;
      }
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_changeGear(reverse_gear);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage 2 Completed" );
	nextStage++;
      }
    }
    break;

  case 3: //send change-mode command to TrajFollower ?-->reverse-MODE
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "LturnReverse - Stage3" );

      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->adriveGearReverse == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "LturnReverse - Alice not yet in reverse gear - will poll" );
      }

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	m_pStrategyInterface->stage_changeTFmode( tf_reverse, LTURN_REVERSE_DISTANCE_METERS );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage 3 Completed" );
      }
    }
    break;
    
  case 4: //superCon e-stop run Alice now that she is ready to REVERSE
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "LturnReverse - Stage4" );      
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->trajFReverse == false ) {
	SuperConLog().log( STR, WARNING_MSG, "LturnReverse: TrajFollower not yet in reverse mode" );
	skipStageOps = true;
      }
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_run);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage 4 Completed" );
      }
    }
    break;

  case 5: //wait until Alice has reversed through LTURN_REVERSE_DISTANCE_METERS (assuming PLN !-> FP)
          //and then transition to LoneRanger IFF PLN -> NFP through TERRAIN obstacles, and reverse
          //further IFF PLN -> NFP through SUPERCON obstacles
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAll_TransTerms( m_pStrategyInterface, m_pdiag, "LturnReverse - Stage5" );      
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->tfFinishedReversing == false ) {
	//not finished reversing
	skipStageOps = true;
      }

      if( m_pdiag->tfFinishedReversing == true && m_pdiag->plnNFP_SuperC == true ) {
	/* REPEAT REVERSING ACTION */
	nextStage = 3;
	skipStageOps = true;
	transitionStrategy( StrategyLoneRanger, "reversing complete and PLN->NFP through SUPERCON obstacle -> RE-REVERSE" );
      }
      
      if( m_pdiag->tfFinishedReversing == true && m_pdiag->plnNFP_Terrain == false ) {
	//This is a check to ensure that we only transition to the lone ranger when
	//we have finished reversing && the planner is NFP through a TEERAIN obstacle ONLY
	skipStageOps = true;
      }

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {

	transitionStrategy( StrategyLoneRanger, "reversing complete and PLN->NFP through TERRAIN obstacles ONLY -> LoneRanger" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "reversing complete and PLN->NFP through TERRAIN obstacles ONLY" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage 5 Completed" );
      }      
    }
    break;

    /** END OF STRATEGY **/
    
  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyLturnReverse::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyLturnReverse::stepForward - default case in nextStep switch() reached" );
      transitionStrategy(StrategyNominal, "ERROR: LturnReverse - default stage reached!" );
    }
  }
}


void CStrategyLturnReverse::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LturnReverse - Exiting" );
}

void CStrategyLturnReverse::enter( CStrategyInterface *m_pStrategyInterface )
{
  //reset the nextStage counter (starts at ONE, NOT zero) as the
  //strategy is being entered --> first stage to execute should be
  //stage #1
  nextStage = 1; //reset to starting value
  skipStageOps.reset(); //resets to FALSE 
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "LturnReverse - Entering" );
}


