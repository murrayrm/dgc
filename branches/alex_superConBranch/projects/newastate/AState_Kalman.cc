/*
 * Kalman initialization
 * 8/14/05 EJC
 */

#include "AState.hh"

void AState::kfInit(int inputNumStates, int inputNumMeasurements){
  if (inputNumStates <= 0) {
    cerr << "Error: cannot make a kalman filter with 0 or less states." << endl;
    exit(1);
  }


  kfX = Matrix(inputNumStates);
  kfP = Matrix(inputNumStates,inputNumStates);
  
  kfdXdt = Matrix(inputNumStates,inputNumStates);
  kfA = Matrix(inputNumStates,inputNumStates);
  kfQ = Matrix(inputNumStates,inputNumStates);

  kfH3d = Matrix(inputNumMeasurements,inputNumStates);
  kfZ3d = Matrix(inputNumMeasurements);
  kfR3d = Matrix(inputNumMeasurements,inputNumMeasurements);

  kfH3d.setelem(0,0,1);
  kfH3d.setelem(1,1,1);
  kfH3d.setelem(2,2,1);
  kfH3d.setelem(3,3,1);
  kfH3d.setelem(4,4,1);
  kfH3d.setelem(5,5,1);

  kfH2d = Matrix(inputNumMeasurements - 2,inputNumStates);
  kfZ2d = Matrix(inputNumMeasurements - 2);
  kfR2d = Matrix(inputNumMeasurements - 2,inputNumMeasurements - 2);

  kfH2d.setelem(0,0,1);
  kfH2d.setelem(1,1,1);
  kfH2d.setelem(2,3,1);
  kfH2d.setelem(3,4,1);

  kfHnoVel = Matrix(inputNumMeasurements - 2,inputNumStates);
  kfZnoVel = Matrix(inputNumMeasurements - 2);
  kfRnoVel = Matrix(inputNumMeasurements - 2,inputNumMeasurements - 2);

  kfHnoVel.setelem(0,0,1);
  kfHnoVel.setelem(1,1,1);
  kfHnoVel.setelem(2,2,1);
  kfHnoVel.setelem(3,8,1);

  //3 => velccity components
  kfHodom = Matrix(3, inputNumStates);
  kfZodom = Matrix(3);
  kfRodom = Matrix(3, 3);

  // K is never preset--no initialization.

  numStates = inputNumStates;
  numMeasurements = inputNumMeasurements;

  readCovariances();

  kfR2d.setelem(0,0,kfR3d.getelem(0,0));
  kfR2d.setelem(1,1,kfR3d.getelem(1,1));
  kfR2d.setelem(2,2,kfR3d.getelem(3,3));
  kfR2d.setelem(3,3,kfR3d.getelem(4,4));

  DGClockMutex(&m_MetaStateMutex);
  _metaState.kfEnabled = 1;  
  DGCunlockMutex(&m_MetaStateMutex);
}

void AState::readCovariances() { 
  int i, k;

  // Everything set to zero in the Matrix(row, col) creation
  // A, K, X, Z, dXdt don't need to change
  // I needs to go to Identity
  // Set elsewhere for H, dXdt
  // Read in for P, Q, R  

  double pVals[numStates];
  double qVals[numStates];
  double rVals[numMeasurements];
  
  // If we get more states, we may have to change h, but
  // right now it's straightforward.

  FILE *cf = fopen("kalman.config", "r");
  char line[50];
  char errorCov[50];
  double tempCov;

  // Read the entire file.
  if(cf != NULL) {
    // Get one line at a time...
    while(fgets(line, sizeof(line), cf)) {
      // And scan it to see if it has a string identifying a matrix.
      if(sscanf(line, "%s\n", errorCov) == 1) {
	// For example, here if it matches "P:"...
	if(strcmp(errorCov, "P:") == 0) {
	  // We write in a line for each diagonal value of P
	  for(i = 0; i < numStates; i++) {
	    fgets(line, sizeof(line), cf);
	    // And check to make sure we read it ok.
	    if(sscanf(line, "%lf\n", &tempCov) == 1) {
	      pVals[i] = tempCov;
	    } else {
	      cerr << "Error reading pVals[" << i 
		   << "] in kalman.config." << endl;
	      pVals[i] = 0;
	    }
	  }
	} else if(strcmp(errorCov, "Q:") == 0) {
	  // We do the same thing for "Q:"
	  for(i = 0; i < numStates; i++) {
	    fgets(line, sizeof(line), cf);
	    if(sscanf(line, "%lf\n", &tempCov) == 1) {
	      qVals[i] = tempCov;
	    } else {
	      cerr << "Error reading qVals[" << i 
		   << "] in kalman.config." << endl;
	      qVals[i] = 0;
	    }	    
	  }
	} else if(strcmp(errorCov, "R:") == 0) {
	  // and "R:".  Note R is a different size.
	  for(i = 0; i < numMeasurements; i++) {
	    fgets(line, sizeof(line), cf);
	    if(sscanf(line, "%lf\n", &tempCov) == 1) {
	      rVals[i] = tempCov;
	    } else {
	      cerr << "Error reading rVals[" << i 
		   << "] in kalman.config." << endl;
	      rVals[i] = 0;
	    }	    
	  }
	} else {
	  // Nothing to do.
	}
      }
    }
  } else {
    cerr << "Error: Read failure in kalman.config--KF covariances "
	 << "not initialized!" << endl;
  }
  fclose(cf);

  for(i = 0; i < numStates; i++) { //cols
    kfP.setelem(i,i,pVals[i]); 
    kfQ.setelem(i,i,qVals[i]); 
  }
   
  for(k = 0; k < numMeasurements; k++) {
    kfR3d.setelem(k, k, rVals[k]);
  }
}
