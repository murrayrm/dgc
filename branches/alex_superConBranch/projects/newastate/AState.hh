/* AState.hh
 *
 * JML 09 Jun 05
 *
 */

#ifndef ASTATE_HH
#define ASTATE_HH

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "VehicleState.hh"

#include "ggis.h"
#include "frames/coords.hh"
#include "gps.hh"
#include "imu_fastcom.h"
#include "sn_msg.hh"
#include "sn_types.h"
#include "Matrix.hh"

#include "DGCutils"

#include "SensorConstants.h"
#include "SkynetContainer.h"
#include "CTimberClient.hh"

#include "ActuatorState.hh"

#ifndef NOSDS
#define GPSPORT 4
#else
#define GPSPORT 0
#endif

#define IMU_BUFFER_SIZE 10
#define GPS_BUFFER_SIZE 10
#define ODOM_BUFFER_SIZE 10

/*!  MetaState struct.  This is the struct that stores higher level information 
 *   about the status of various components of the stateserver.
 *
 *   At the moment, this struct is neither used nor broadcast.  Its contents are 
 *   still subject to change depending on what we decide fault management might 
 *   need to know.
 */

void makeBody2Nav(Matrix &Cnb, double roll, double pitch, double yaw);
void makeEarth2Nav(Matrix &Cne, double lat, double lon, double alpha);
void makeNav2Geo(Matrix &Cgn, double alpha);

struct MetaState
{
  unsigned long long Timestamp;    

  /*  
   *  1 = enabled.  0 = disabled. -1 = Couldn't Start. 
   */
  int gpsEnabled;
  int imuEnabled;
  int kfEnabled;
  int odomEnabled;

  int gpsActive;
  int imuActive;
  int odomActive;
    
  int gpsPvtValid;
  int extJumpFlag;  
};

struct astateOpts {
  int useIMU;
  int useGPS;
  int useKF;
  int useOdom;
  int useVerbose;
  int logRaw;
  int useReplay;
  char replayFile[100];
  int timberPlayback;
  int debug;
  int useSparrow;
  int fastMode;
};

struct rawIMU {
  unsigned long long time;
  IMU_DATA data;
};

struct rawGPS {
  unsigned long long time;
  gpsData data;
};

struct rawOdom {
  unsigned long long time;
  double speed;
};

struct heartbeat {
  bool gps;
  bool gpsValid;
  bool imu;
  bool odom;
};

enum {
  PREINIT,
  INIT,
  IMU_GPS_ODOM,
  IMU_GPS,
  IMU_ODOM,
  IMU,
  GPS_ODOM,
  GPS,
  ODOM,
  OH_SHIT,
  STATIONARY,
  NUM_MODES
};
  
  

/*! AState Class.  AState is the State Estimation class for Alice.
 *  GPS, and IMU information are combined through the use of a NGC Kalman filter.
 *
 *  The Kalman filter code is in the kfilter.a library.  It is northrop grumman
 *  proprietary code.  If you need access to it, contact Richard, Jeremy L., or
 *  Alex Fax. Hopefully this code will be replace in the near future.
 *
 *
 *  Main initializes a new instance of AState referred to as ss.
 *
 *  Within the constructor of AState:
 *     First the sensors are initialized through calling:
 *        GPS_init()
 *        IMU_init()
 *        Mag_init()
 *
 *     Then, the following threads are started
 *        GPS_thread() -- Does GPS communication @ ~5 Hz
 *        IMU_thread() -- Does IMU communication @ ~400 Hz
 *        Mag_thread() -- Does Mag communication @ ~0 Hz (No Mag yet)
 *        Sparrow_thread() -- Updates sparrow display variables
 *
 *     If the Kalman filter is enabled, IMU_thread calls UpdateState().  Otherwise
 *     the GPS_thread calls UpdateState().
 *
 *     If state is updated and vehiclestate is populated with new information,
 *     UpdateState() will call Broadcast() which sends out the information to
 *     CStateclient.  CStateClient locally buffers state data and returns it
 *     when requested.  It allows users to either request the most recent state
 *     via UpdateState() or a specific state (based on interpolation between
 *     points) via UpdateState(unsigned long long).
 *
 *     NOTE: This is a DIFFERENT UpdateState() than the one called internally
 *     to AState.
 *
 *     CStateClient is located in /utils/moduleHelpers/StateClient.cc.
 *
 *  The code is broken up as follows:
 *
 *  AState_Main.cc
 *     Does user input, initializes everything, and then waits for quit to
 *     be pressed, at which point it deletes ss and exits.
 *
 *  AState.cc
 *     Constructor
 *     Broadcast()
 *
 *  AState_GPS.cc
 *     GPS_Init()
 *     GPS_Thread()
 *
 *  AState_IMU.cc
 *     IMU_Init()
 *     IMU_Thread()
 *
 *  AState_Sparrow.cc
 *     All Sparrow Interface functionality
 *     Update_Sparrow_Thread()
 *     toggle_stationary and restart_astate are external to astate class
 *
 *  AState_Update.cc
 *     UpdateState() function which deals with Kalman Filter if the Kalman 
 *     Filter is enabled, and otherwise updates state to the best of its ability.
 *     This is where Broadcast() gets called.
 *
 *  \brief This is the class that implements the state server.
 */
class AState : virtual public CSkynetContainer, public CTimberClient //Timber not in stable branch yet
{
  // Header variables, sorted by the mutex that locks and unlocks around them.
  int fastmode;

  pthread_mutex_t m_VehicleStateMutex;

  // Lock these for the m_VehicleStateMutex
  struct VehicleState _vehicleState;
  // end lock list 






  pthread_mutex_t m_MetaStateMutex;

  // Lock these for the m_MetaStateMutex
  struct MetaState	_metaState;
  // end lock list 






  pthread_mutex_t m_IMUDataMutex;

  DGCcondition newData;
  DGCcondition imuBufferFree;
  DGCcondition gpsBufferFree;
  DGCcondition odomBufferFree;

  // Lock these for the m_IMUDataMutex
  rawIMU imuBuffer[IMU_BUFFER_SIZE];

  unsigned long long stateTime;

  Matrix fbSum;
  Matrix wb_ibSum;
  double dtSum;
  int imuBufferReadInd;
  int imuBufferLastInd;
  unsigned long long imuLogStart;
  // end lock list

  pthread_mutex_t m_GPSDataMutex;

  // Lock these for the m_GPSDataMutex
  rawGPS gpsBuffer[GPS_BUFFER_SIZE];
  int gpsBufferReadInd;
  int gpsBufferLastInd;
  double gpsLat;
  double gpsLon;
  double gpsHeight;
  Matrix gpsVn;
  double gpsfom;
  int newGPS;
  int gpsValid;
  int gps3D;
  unsigned long long gpsLogStart;
  // end lock list 


  pthread_mutex_t m_OdomDataMutex;
  
  rawOdom odomBuffer[ODOM_BUFFER_SIZE];
  int odomBufferReadInd;
  int odomBufferLastInd;
  unsigned long long odomLogStart;
  double obdiiScaleFactor;
  
  pthread_mutex_t m_KalmanMutex;

  // Lock these for the m_KalmanMutex 
  int numStates;
  int numMeasurements;
  Matrix kfX;
  Matrix kfP;
  Matrix kfdXdt;
  Matrix kfA;
  Matrix kfH2d;
  Matrix kfH3d;
  Matrix kfHnoVel;
  Matrix kfHodom;
  Matrix kfZ2d;
  Matrix kfZ3d;
  Matrix kfZnoVel;
  Matrix kfZodom;
  Matrix kfK;
  Matrix kfQ;
  Matrix kfR2d;
  Matrix kfR3d;
  Matrix kfRnoVel;
  Matrix kfRodom;
  // end lock list

  // Lock these for the m_StateMutex
  int stateMode;  
  int correctFreq;
  double lat;
  double lon;
  double height;
  double roll;
  double pitch;
  double yaw;
  double alpha;
  Matrix Cne;
  Matrix Cnb;
  Matrix Cgn;
  Matrix vn;
  double pError;
  double stateJump;
  // end lock list

  pthread_mutex_t m_HeartbeatMutex;

  struct heartbeat _heartbeat;

  // Variables to remain unlocked
  int snKey;  // Should be set in AState_Main and never changed.
  struct astateOpts  _astateOpts;  // Should be set in constructor and never changed.
  int broadcastStateSock;  // Should be set in constructor and never changed.
  unsigned long long startTime; // Should be set in constructor and never changed.
  fstream imuLogStream; // These four are opened & closed in constructor and destructor
  fstream imuReplayStream; // and only accessed as a write in AState_IMU.  
  char imuLogFile[100];
  char imuReplayFile[100];
  fstream gpsLogStream; // These four are opened & closed in constructor and destructor
  fstream gpsReplayStream; // and only accessed as a write in AState_GPS.
  char gpsLogFile[100];
  char gpsReplayFile[100];
  fstream odomLogStream; // These four are opened & closed in constructor and destructor
  fstream odomReplayStream; // and only accessed as a write in AState_OBDII.  
  char odomLogFile[100];
  char odomReplayFile[100];
  fstream timberLogStream; // Only called by AState_Update.
  char timberLogFile[100]; // See above
  ifstream timberPlaybackStream; // Only called by AState_Update.
  char timberPlaybackFile[100];  // See above
  fstream debugStream;  // Not called anywhere yet.
  char debugFile[100];  // See above
  // end unlocked list


  // Sparrow stuff---where the heck do these go?
  int gpsTimeOffset;
  double gpsPosErrConst;
  double gpsVelErrConst;
  int gpsPosErrMode;
  int gpsVelErrMode;
  int quitPressed;
  int stationary;



  /*****************************************************************
   *  The following are the private methods of the AState class.
   *****************************************************************/

  /*! Function that initializes the IMU */
  void imuInit();

  /*! Function that initializes the GPS */
  void gpsInit();

  /*! Function that initializes the odometry */
  void odomInit();

  /*! Function that updates the vehicle_state struct */
  void updateStateThread();

  /*! Function to broadcast state data */
  void broadcast();

  /*! Thread to read from the IMU.  Note: this is where UpdateState normally gets called since it's the fastest thread.*/
  void imuThread(); 

  /*! Thread to read from the GPS.*/
  void gpsThread();

  /*! Thread to read from the odometry.*/
  void odomThread();

  /*! Thread to update sparrow variables*/
  void updateSparrowThread();

  /*! Thread to actually update sparrow display*/
  void sparrowDisplayLoop();

  /*! Thread to output metastate to supercon. */
  void metaStateThread();

  void playback();

  void kfInit(int inputNumStates, int inputNumMeasurements);

  void readCovariances();

public:
  /*! AState Constructor */
  AState(int skynetKey, astateOpts inputAstateOpts);

  /*! Function to restart AState without having to stop/start the
   *  whole system
   */
  void restart();
  void active();
  int sparrowGPSDisable;

  ~AState();
};

#endif
