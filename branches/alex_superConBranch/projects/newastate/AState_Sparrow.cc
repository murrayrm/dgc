#include "AState.hh"
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

//Struct containing all possible pieces of data being outputted
struct astateToSparrowInfo
{
  char   gpsStat[12];
  double gpsLat;
  double gpsLon;
  double gpsHeight;
  double gpsN;
  double gpsE;

  char   imuStat[10];
  double kfLat;
  double kfLon;
  double kfHeight;
  double kfRoll;
  double kfPitch;
  double kfYaw;

  char   kfStat[10];
  int    imuCount;
  int    gpsCount;

  int    timestamp;

  VehicleState vstate;

  int    snkey;
  int    stat;

  double stateJump;

  int gpsTimeOffset;
  int gpsPosErrConst;
  int gpsVelErrConst;
  int gpsPosErrMode;
  int gpsVelErrMode;
};

struct sparrowToAstateInfo {
  int quitPressed;
  int gpsTimeOffset;
  int gpsPosErrConst;
  int gpsVelErrConst;
  int gpsPosErrMode;
  int gpsVelErrMode;
};

extern AState *ss;

astateToSparrowInfo displayVars;
sparrowToAstateInfo astateVars;

int toggleStationary(long);
int restartAstate(long);
int flipGPS(long);

#include "astune.h"		// table for tunning params
#include "asdisp.h"		// main display table

//Populate displayVars struct
void AState::updateSparrowThread() 
{
  while(!quitPressed) {
    // Update the ones that change

      if (_metaState.gpsEnabled == 1) {
	if (gpsValid == 1) {
	  strcpy(displayVars.gpsStat,"valid PVT");
	} else {
	  strcpy(displayVars.gpsStat,"invalid PVT");
	}
      } else {
	strcpy(displayVars.gpsStat,"GPS ERROR");
      }

      if (_metaState.imuEnabled == 1) {
	strcpy(displayVars.imuStat,"active");
      } else {
	strcpy(displayVars.imuStat,"inactive");
      }


      GisCoordLatLon latsForVehState;
      GisCoordUTM utmForVehState;

      latsForVehState.latitude = gpsLat*(180/M_PI);
      latsForVehState.longitude = gpsLon*(180/M_PI);
      gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84);

      displayVars.gpsLat = gpsLat;
      displayVars.gpsLon = gpsLon;
      displayVars.gpsHeight = gpsHeight;
      displayVars.gpsN = utmForVehState.n;
      displayVars.gpsE = utmForVehState.e;
    
      displayVars.kfLat = lat;
      displayVars.kfLon = lon;
      displayVars.kfHeight = height;
      displayVars.kfRoll = roll;
      displayVars.kfPitch = pitch;
      displayVars.kfYaw = yaw;

      if (_metaState.kfEnabled == 1) {
	strcpy(displayVars.kfStat,"active");
      } else {
	strcpy(displayVars.kfStat,"inactive");
      }

      displayVars.imuCount = imuBufferReadInd;
      displayVars.gpsCount = gpsBufferReadInd;

      displayVars.timestamp = _vehicleState.Timestamp - startTime;
      displayVars.vstate = _vehicleState;

      displayVars.stateJump = (stateJump*100.0); // m to cm

      displayVars.snkey = snKey;
      displayVars.stat = stationary;

    usleep(100000);
  }

}

//Sparrow display loop
void AState::sparrowDisplayLoop() {
  dbg_all = 0;

  usleep(1000000);

  if (dd_open() < 0) exit(1);

  dd_usetbl(asdisp);

  dd_loop();
  dd_close();
  quitPressed = 1;
}

//Toggle Stationary mode
int toggleStationary(long arg) {
#warning Wouldnt it be nice if toggleStationary did something?
  return 1;
}

int restartAstate(long arg) {
#warning Wouldnt it be nice if restartAstate did something as well?
  return 1;
}

int flipGPS(long arg) {
  (*ss).sparrowGPSDisable = !(*ss).sparrowGPSDisable;
  return 1;
}

