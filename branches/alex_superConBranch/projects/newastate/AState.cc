/* AState.cc - new vehicle state estimator
 *
 * JML 09 Jun 05
 *
 * */

#include "AState.hh"

AState::AState(int skynetKey, astateOpts inputAstateOpts)
  : CSkynetContainer(SNastate, skynetKey), CTimberClient(timber_types::astate)
{
  fbSum = Matrix(3);
  wb_ibSum = Matrix(3);
  dtSum = 0.0;
  gpsVn = Matrix(3);
  Cne = Matrix(3,3);
  Cnb = Matrix(3,3);
  Cgn = Matrix(3,3);
  vn = Matrix(3);
  newGPS = 0;
  gpsLat = 0.0;
  gpsLon = 0.0;
  gpsHeight = 0.0;
  alpha = 0;
  correctFreq = 40;
  pError = 0.001;
  stateJump = 0.0;
  stateMode = PREINIT;
  obdiiScaleFactor = 1.0;

  imuBufferLastInd = 0;
  gpsBufferLastInd = 0;
  odomBufferLastInd = 0;
  imuBufferReadInd = 0;
  gpsBufferReadInd = 0;
  odomBufferReadInd = 0;
  
  // Set internal options.
  _astateOpts = inputAstateOpts;
  snKey = skynetKey;

  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);

  // Set up files for raw logging.
  if (_astateOpts.logRaw || _astateOpts.debug) {
    mkdir("/tmp/logs/astate_raw/", S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH);
  }
   
  if (_astateOpts.logRaw == 1) {
    sprintf(imuLogFile, "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_IMU.raw",
	    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	    local->tm_hour, local->tm_min, local->tm_sec);
    sprintf(gpsLogFile, "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_GPS.raw",
	    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	    local->tm_hour, local->tm_min, local->tm_sec);
    imuLogStream.open(imuLogFile, fstream::out | fstream::binary);
    gpsLogStream.open(gpsLogFile, fstream::out | fstream::binary);
    odomLogStream.open(odomLogFile, fstream::out | fstream::binary);
  }

  if (_astateOpts.debug == 1) {
    sprintf(debugFile, "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_debug.raw",
	    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	    local->tm_hour, local->tm_min, local->tm_sec);
    debugStream.open(debugFile, fstream::out);
  }

  // Set up files for raw replay.
  if (_astateOpts.useReplay == 1) {
    sprintf(imuReplayFile, "%s_IMU.raw", _astateOpts.replayFile);
    sprintf(gpsReplayFile, "%s_GPS.raw", _astateOpts.replayFile);
    sprintf(odomReplayFile, "%s_ODOM.raw", _astateOpts.replayFile);
    imuReplayStream.open(imuReplayFile, fstream::in | fstream::binary);
    gpsReplayStream.open(gpsReplayFile, fstream::in | fstream::binary);
    odomReplayStream.open(odomReplayFile, fstream::in | fstream::binary);
  }

  //Find startting time;
  DGCgettime(startTime);

  //Initialize EVERYTHING to be zeroed for good measure.
  _vehicleState.Timestamp = startTime;
  _vehicleState.Northing = 0;
  _vehicleState.Easting = 0;
  _vehicleState.Altitude = 0;
  _vehicleState.Vel_N = 0;
  _vehicleState.Vel_E = 0;
  _vehicleState.Vel_D = 0;
  _vehicleState.Acc_N = 0;
  _vehicleState.Acc_E = 0;
  _vehicleState.Acc_D = 0;
  _vehicleState.Roll = 0;
  _vehicleState.Pitch = 0;
  _vehicleState.Yaw = 0;
  _vehicleState.RollAcc = 0;
  _vehicleState.PitchAcc = 0;
  _vehicleState.YawAcc = 0;

  gpsValid = 0;

  stationary = 0;

  gpsTimeOffset = 0;
  gpsPosErrConst = 2;
  gpsVelErrConst = 5.0;
  gpsPosErrMode = 1;
  gpsVelErrMode = 1;
  quitPressed = 0;

  //Initialize metastate struct
  _metaState.gpsEnabled = 0;
  _metaState.imuEnabled = 0;
  _metaState.odomEnabled = 0;
  _metaState.kfEnabled = 0;

  _metaState.gpsActive = 0;
  _metaState.imuActive = 0;
  _metaState.odomActive = 0;
  _metaState.gpsPvtValid = 0;
  _metaState.extJumpFlag = 0;

  //Create mutexes:
  DGCcreateMutex(&m_VehicleStateMutex);
  DGCcreateMutex(&m_MetaStateMutex);
  DGCcreateMutex(&m_GPSDataMutex);
  DGCcreateMutex(&m_IMUDataMutex);
  DGCcreateMutex(&m_OdomDataMutex);
  DGCcreateMutex(&m_KalmanMutex);
  DGCcreateMutex(&m_HeartbeatMutex);

  DGCcreateCondition(&newData);
  DGCcreateCondition(&gpsBufferFree);
  DGCcreateCondition(&imuBufferFree);
  DGCcreateCondition(&odomBufferFree);
  DGCSetConditionTrue(gpsBufferFree);
  DGCSetConditionTrue(imuBufferFree);
  DGCSetConditionTrue(odomBufferFree);

  //Set up socket for skynet broadcasting:
  broadcastStateSock = m_skynet.get_send_sock(SNstate);
  if(broadcastStateSock < 0) {
    cerr << "AState::AState(): skynet get_send_sock returned error" << endl;
  }
  if (_astateOpts.timberPlayback == 1) {
    cout << "Starting timber playback..." << endl;
    DGCstartMemberFunctionThread(this, &AState::playback);
  } else {


    //Initialize sensors:
    if(_astateOpts.useIMU == 0) {
      DGClockMutex(&m_MetaStateMutex);     
      _metaState.imuEnabled = 0;
      DGCunlockMutex(&m_MetaStateMutex);     
    } else {
      imuInit(); 	   // This will set imuEnabled
    }
    if (_metaState.imuEnabled == 1) {
            DGCstartMemberFunctionThread(this, &AState::imuThread);
    }

    if(_astateOpts.useGPS == 0) {
      DGClockMutex(&m_MetaStateMutex);     
      _metaState.gpsEnabled = 0;
      DGCunlockMutex(&m_MetaStateMutex);     
    } else {
      gpsInit();           // This will set gpsEnabled
    }
    if (_metaState.gpsEnabled == 1) {
      DGCstartMemberFunctionThread(this, &AState::gpsThread);
    }

    if(_astateOpts.useOdom == 0) {
      DGClockMutex(&m_MetaStateMutex);     
      _metaState.odomEnabled = 0;
      DGCunlockMutex(&m_MetaStateMutex);     
    } else {
      odomInit();           // This will set gpsEnabled
    }
    if (_metaState.odomEnabled == 1) {
      DGCstartMemberFunctionThread(this, &AState::odomThread);
    }

    //Initialize sensors:

    DGCstartMemberFunctionThread(this, &AState::updateStateThread);

    DGCstartMemberFunctionThread(this, &AState::metaStateThread);

    //Start sparrow threads
    if(_astateOpts.useSparrow) {
      DGCstartMemberFunctionThread(this, &AState::updateSparrowThread);
      DGCstartMemberFunctionThread(this, &AState::sparrowDisplayLoop);
    }
  }
}

void AState::restart() {
  // Lock all the mutexes--this will stop all the threads at their locks.
  DGClockMutex(&m_VehicleStateMutex);
  DGClockMutex(&m_MetaStateMutex);
  DGClockMutex(&m_GPSDataMutex);
  DGClockMutex(&m_IMUDataMutex);
  DGClockMutex(&m_OdomDataMutex);
  DGClockMutex(&m_KalmanMutex);
  DGClockMutex(&m_HeartbeatMutex);

  // Reinitialize all the values

  //Initialize EVERYTHING to be zeroed for good measure.
  _vehicleState.Timestamp = startTime;
  _vehicleState.Northing = 0;
  _vehicleState.Easting = 0;
  _vehicleState.Altitude = 0;
  _vehicleState.Vel_N = 0;
  _vehicleState.Vel_E = 0;
  _vehicleState.Vel_D = 0;
  _vehicleState.Acc_N = 0;
  _vehicleState.Acc_E = 0;
  _vehicleState.Acc_D = 0;
  _vehicleState.Roll = 0;
  _vehicleState.Pitch = 0;
  _vehicleState.Yaw = 0;
  _vehicleState.RollAcc = 0;
  _vehicleState.PitchAcc = 0;
  _vehicleState.YawAcc = 0;

  gpsValid = 0;

  stationary = 0;

  gpsTimeOffset = 0;
  gpsPosErrConst = 2;
  gpsVelErrConst = 5.0;
  gpsPosErrMode = 1;
  gpsVelErrMode = 1;
  quitPressed = 0;

  // Reinitialize the Kalman Filter covariances, if they've changed.
  readCovariances();

  // Unlock the mutexes and begin again.
  DGCunlockMutex(&m_VehicleStateMutex);
  DGCunlockMutex(&m_MetaStateMutex);
  DGCunlockMutex(&m_GPSDataMutex);
  DGCunlockMutex(&m_IMUDataMutex);
  DGCunlockMutex(&m_OdomDataMutex);
  DGCunlockMutex(&m_KalmanMutex);
  DGCunlockMutex(&m_HeartbeatMutex);
}

AState::~AState()
{
  if (_astateOpts.logRaw == 1) {
    imuLogStream.close();
    gpsLogStream.close();
    odomLogStream.close();
  }
  
  if (_astateOpts.useReplay == 1) {
    imuReplayStream.close();
    gpsReplayStream.close();
    odomReplayStream.close();
  }

  if (_astateOpts.debug == 1) {
    debugStream.close();
  }

}

// VehicleState messaging Method.
void AState::broadcast()
{
  pthread_mutex_t* pMutex = &m_VehicleStateMutex;
  if(m_skynet.send_msg(broadcastStateSock,
		       &_vehicleState, 
		       sizeof(VehicleState), 
		       0, 
		       &pMutex) 
     != sizeof(VehicleState))
    {
      cerr << "AState::Broadcast(): didn't send right size state message" << endl;
    }
  if(getLoggingEnabled()) {
    if(checkNewDirAndReset()) {
      cout << "Opening new file" << endl;
      if (timberLogStream.is_open()) {
	timberLogStream.close();
      }
      sprintf(timberLogFile, "%sstate.dat",getLogDir().c_str());
      timberLogStream.open(timberLogFile, fstream::out|fstream::app);
    }
    timberLogStream.precision(10);
    timberLogStream <<  _vehicleState.Timestamp << '\t';
    timberLogStream <<  _vehicleState.Northing << '\t' << _vehicleState.Easting << '\t' << _vehicleState.Altitude << "\t";
    timberLogStream <<  _vehicleState.Vel_N << '\t' << _vehicleState.Vel_E << '\t' << _vehicleState.Vel_D << "\t";
    timberLogStream <<  _vehicleState.Acc_N << '\t' << _vehicleState.Acc_E << '\t' << _vehicleState.Acc_D << "\t";
    timberLogStream <<  _vehicleState.Roll << '\t' << _vehicleState.Pitch << '\t' << _vehicleState.Yaw << "\t";
    timberLogStream <<  _vehicleState.RollRate << '\t' << _vehicleState.PitchRate << '\t' << _vehicleState.YawRate << "\t";
    timberLogStream <<  _vehicleState.RollAcc << '\t' << _vehicleState.PitchAcc << '\t' << _vehicleState.YawAcc << endl;
  }
}

void AState::playback()
{
  unsigned long long playbackTime;
  sprintf(timberPlaybackFile, "%sstate.dat",getPlaybackDir().c_str());
  cout << "Opening file... " << timberPlaybackFile << endl;
  timberPlaybackStream.open(timberPlaybackFile);
  _vehicleState.Timestamp = 0;
  while (1) {
    if(checkNewPlaybackDirAndReset()) {
      if (timberPlaybackStream.is_open()) {
	timberPlaybackStream.close();
      }
      sprintf(timberPlaybackFile, "%sstate.dat",getPlaybackDir().c_str());
      cout << "Re-Opening file... " << timberPlaybackFile << endl;
      timberPlaybackStream.open(timberPlaybackFile);
    } 
    if (!timberPlaybackStream) {
      cout << __FILE__ << " [" << __LINE__ 
	   << "]: Reached the end of the playback file, or couldn't open it to begin with" << endl;
      exit(0);
    } else {
      unsigned long long timberTime = getPlaybackTime();
      while(timberTime >= _vehicleState.Timestamp && timberPlaybackStream) {
	timberPlaybackStream >> _vehicleState.Timestamp;
	timberPlaybackStream >> _vehicleState.Northing;
	timberPlaybackStream >> _vehicleState.Easting;
	timberPlaybackStream >> _vehicleState.Altitude;
	timberPlaybackStream >> _vehicleState.Vel_N;
	timberPlaybackStream >> _vehicleState.Vel_E;
	timberPlaybackStream >> _vehicleState.Vel_D;
	timberPlaybackStream >> _vehicleState.Acc_N;
	timberPlaybackStream >> _vehicleState.Acc_E;
	timberPlaybackStream >> _vehicleState.Acc_D;
	timberPlaybackStream >> _vehicleState.Roll;
	timberPlaybackStream >> _vehicleState.Pitch;
	timberPlaybackStream >> _vehicleState.Yaw;
	timberPlaybackStream >> _vehicleState.RollRate;
	timberPlaybackStream >> _vehicleState.PitchRate;
	timberPlaybackStream >> _vehicleState.YawRate;
	timberPlaybackStream >> _vehicleState.RollAcc;
	timberPlaybackStream >> _vehicleState.PitchAcc;
	timberPlaybackStream >> _vehicleState.YawAcc;
      }
      playbackTime = getPlaybackTime();
      
      if (_vehicleState.Timestamp > playbackTime) {
	blockUntilTime(_vehicleState.Timestamp);
	broadcast();
      }
    }
  }
}

void AState::active() {
  while (!quitPressed) {
    sleep(1);
  }
}
  
