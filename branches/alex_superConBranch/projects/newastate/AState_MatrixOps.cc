#include "AState.hh"

void makeBody2Nav(Matrix &Cnb, double roll, double pitch, double yaw) {
  double cr = cos(roll);
  double cp = cos(pitch);
  double cy = cos(yaw);
  double sr = sin(roll);
  double sp = sin(pitch);
  double sy = sin(yaw);
  
  double newCnb[] = {
    cp*cy,  cy*sp*sr - cr*sy,  cr*cy*sp + sr*sy,
    cp*sy,  cr*cy + sp*sr*sy, -cy*sr + cr*sp*sy,
    -sp,    cp*sr,             cp*cr
  };  

  Cnb.setelems(newCnb);
}

void makeEarth2Nav(Matrix &Cne, double lat, double lon, double alpha) {
  double cla = cos(lat);
  double clo = cos(lon);
  double ca = cos(alpha);
  double sla = sin(lat);
  double slo = sin(lon);
  double sa = sin(alpha);
  
  double newCne[] = {
    -ca*clo*sla - sa*slo,  clo*sa - ca*sla*slo,  ca*cla,
    clo*sa*sla - ca*slo,   ca*clo + sa*sla*slo, -cla*sa,
    -cla*clo,             -cla*slo,              -sla
  };

  Cne.setelems(newCne);
}

void makeNav2Geo(Matrix &Cgn, double alpha){
  double ca = cos(alpha);
  double sa = sin(alpha);
  
  double newCgn[] = {
    ca, -sa, 0,
    sa,  ca, 0,
    0,   0,   1
  };
  Cgn.setelems(newCgn);
}
