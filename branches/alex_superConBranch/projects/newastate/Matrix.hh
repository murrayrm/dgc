#ifndef _BLASMATRIX_HH_
#define _BLASMATRIX_HH_

class Matrix {

private:
  int rows;
  int cols;
  int size;
  double* data;

public:
  // ructors
  /*! create empty matrix. */
  Matrix();  
  Matrix(int rows, int cols);
  Matrix(int elems);
  /*! copy ructor. */
  Matrix(const Matrix &matrix2);

  // Mutators
  /*! sets the (row, col)th element of the matrix to value. */
  void setelem(int row, int col, double value);
  void setelem(int elem, double value);
  void setelems(double* data);
  void clear();

  // Accessors
  /*! returns the number of rows. */
  int getrows() const  {return rows;}  
  /*! returns the number of columns. */
  int getcols() const  {return cols;}
  /*! returns the (row, col)th element of the matrix. */
  double getelem(int row, int col)  const;
  double getelem(int elem)  const;
  /*! returns the transpose of the matrix. */
  double* getdata() const;
 
  Matrix transpose() const; 

  Matrix inverse() const; 

  Matrix eye() const;

  double norm() const; 

  //  Matrix inverse();

  // Operators

  /*! add another r x c matrix. */
  Matrix operator+(const Matrix &matrix2) const;  
  
  /*! subtract another r x c matrix. */
  Matrix operator-(const Matrix &matrix) const;  

  /*! multiply by another matrix. */
  Matrix operator*(const Matrix &matrix2) const;  

  Matrix operator*(const double sf) const;  

  Matrix operator/(const double sf) const;  
  
  /*! multiply self set one matrix equal to another. */
  Matrix& operator=(const Matrix &matrix2);

  void print();

  // Destructors
  /*! Default destructor. */
  ~Matrix();  
};

#endif

Matrix matrixCross(Matrix vect);
Matrix unmatrixCross(Matrix mat);
Matrix matrixDiffEqStep(Matrix w);
