#include "AState.hh"

void AState::odomInit()
{
  if (_astateOpts.useReplay == 1) {
    rawOdom odomIn;

    odomReplayStream.read((char*)&odomIn, sizeof(rawOdom));
    DGClockMutex(&m_OdomDataMutex);
    odomLogStart = odomIn.time;
    DGCunlockMutex(&m_OdomDataMutex);

    DGClockMutex(&m_MetaStateMutex);
    _metaState.odomEnabled = 1;
    DGCunlockMutex(&m_MetaStateMutex);
  } else {
    DGClockMutex(&m_MetaStateMutex);
    _metaState.odomEnabled = 1;
    DGCunlockMutex(&m_MetaStateMutex);
  }
}

void AState::odomThread()
{
  unsigned long long nowTime;
  unsigned long long rawOdomTime;

  rawOdom odomIn;
  rawOdom odomOut;
  double obdiiVel;

  int actuatorStateSock = m_skynet.listen(SNactuatorstate, ALLMODULES);
  ActuatorState obdiiState;

  while (!quitPressed) {
    if (_astateOpts.useReplay == 1) {
      if (!odomReplayStream) {
	quitPressed = 1;
	return;
      }
      odomReplayStream.read((char*)&odomIn, sizeof(rawOdom));
      obdiiVel = odomIn.speed;
      rawOdomTime = odomIn.time - odomLogStart + startTime;
      //Wait until time has caught up with raw input
      if (_astateOpts.fastMode == 0) {
	DGCgettime(nowTime);
	while( rawOdomTime > nowTime ) {
	  usleep(1);
	  DGCgettime(nowTime);
	}
      }
    } else {
      if(m_skynet.get_msg(actuatorStateSock, &obdiiState, sizeof(obdiiState), 0, NULL) !=
	 sizeof(obdiiState))
	{
	  cerr << "Didn't receive the right number of bytes in the actuatorstate structure" << endl;
	  continue;
	}
      obdiiVel = obdiiState.m_VehicleWheelSpeed;
      DGCgettime(rawOdomTime); // time stamp as soon as data read.
    }
    if (_astateOpts.logRaw == 1) {
      odomOut.time = rawOdomTime;
      odomOut.speed = obdiiVel;      
      imuLogStream.write((char*)&odomOut, sizeof(rawOdom));
    }

    DGClockMutex(&m_HeartbeatMutex);
    _heartbeat.odom = true;
    DGCunlockMutex(&m_HeartbeatMutex);

    DGClockMutex(&m_OdomDataMutex);


    if (((odomBufferReadInd + 1) % ODOM_BUFFER_SIZE) == (odomBufferLastInd % ODOM_BUFFER_SIZE)) {
      odomBufferFree.bCond = false;

      DGCunlockMutex(&m_OdomDataMutex);

      DGCWaitForConditionTrue(odomBufferFree);


      DGClockMutex(&m_OdomDataMutex);
    }

    ++odomBufferReadInd;

    odomIn.speed = obdiiVel;
    odomIn.time = rawOdomTime;

    memcpy(&odomBuffer[odomBufferReadInd % ODOM_BUFFER_SIZE], &odomIn, sizeof(rawOdom));

    DGCunlockMutex(&m_OdomDataMutex);

    DGCSetConditionTrue(newData);
  }
}
