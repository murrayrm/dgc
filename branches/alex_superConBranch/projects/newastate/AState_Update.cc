/* AState.cc - new vehicle state estimator
 *
 * JML 09 Jun 05
 *
 * */

#include "AState.hh"
#include "Matrix.hh"

#define square(x) ((x)*(x))

void AState::updateStateThread()
{
  double gpsLatSum = 0.0;
  double gpsLonSum = 0.0;
  double gpsHeightSum = 0.0;
  int goodGpsCnt = 0;
  double gNorm;

  unsigned long long imuTime;
  unsigned long long gpsTime;
  unsigned long long odomTime;

  double vnx;
  double vny;
  double Rmeridian;
  double ca;
  double sa;
  double ca2;
  double sa2;
  double Rnormal;
  double rhox;
  double rhoy;
  double vx;
  double vy;
  double vz;
  double px;
  double py;
  double pz;
  double wx;
  double wy;
  double wz;
  double ox;
  double oy;
  double oz;
  double fx;
  double fy;
  double fz;
  double magg;
  double g0;

  Matrix we_ie(3);
  we_ie.setelem(2, EARTH_RATE);
  Matrix gn(3);
  Matrix fn(3);
  Matrix wn_ib(3);
  Matrix wn_ie(3);
  Matrix wn_in(3);
  Matrix wn_bn(3);
  Matrix wn_en(3);
  Matrix dTheta(3);
  Matrix dPhi(3);
  Matrix vdot(3);
  double dH;
  Matrix dVn(3);
  Matrix oldWn_bn(3);
  Matrix oldWb_ibSum(3);

  Matrix gpsVg(3);
  makeNav2Geo(Cgn,alpha);
  Matrix CneMeas(3,3);

  Matrix gpsOffset(3);
  gpsOffset.setelem(0, (X_IMU)-(X_GPS));
  gpsOffset.setelem(1, (Y_IMU)-(Y_GPS));
  gpsOffset.setelem(2, (Z_IMU)-(Z_GPS));
  Matrix gpsNavOffset(3);

  double dtFbSum = 0.0;
  double dtWbSum = 0.0;

  Matrix vehicleOffsets(3);
  vehicleOffsets.setelem(0, -X_IMU);
  vehicleOffsets.setelem(1, -Y_IMU);
  vehicleOffsets.setelem(2, -Z_IMU);

  GisCoordLatLon latsForVehState;
  GisCoordUTM utmForVehState;

  imuBufferLastInd = 0;
  gpsBufferLastInd = 0;
  odomBufferLastInd = 0;

  int newIMU = 0;
  int imuCount = 0;

  int count = 0;
  int gpsNoVel = 0;

  double obdiiVel = 0;
  int newOdom = 0;

  while (!quitPressed) {

    DGClockMutex(&m_IMUDataMutex);
    if (imuBufferReadInd > imuBufferLastInd) {
      DGCSetConditionTrue(imuBufferFree);
      imuBufferLastInd++;
      fbSum.setelem(0, fbSum.getelem(0) - imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dvx);
      fbSum.setelem(1, fbSum.getelem(1) + imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dvy);
      fbSum.setelem(2, fbSum.getelem(2) - imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dvz);
      wb_ibSum.setelem(0, wb_ibSum.getelem(0) - imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dtx);
      wb_ibSum.setelem(1, wb_ibSum.getelem(1) + imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dty);
      wb_ibSum.setelem(2, wb_ibSum.getelem(2) - imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].data.dtz);
      dtSum += 0.0025; // 1/400 --> IMU works at 400 Hz
      dtFbSum += 0.0025; // 1/400 --> IMU works at 400 Hz
      dtWbSum += 0.0025; // 1/400 --> IMU works at 400 Hz

      imuTime = imuBuffer[imuBufferLastInd % IMU_BUFFER_SIZE].time;

      newIMU = 1;
      imuCount += 1;      
    }
    DGCunlockMutex(&m_IMUDataMutex);

    DGClockMutex(&m_GPSDataMutex);
    if (gpsBufferReadInd > gpsBufferLastInd) {
      if (gpsBuffer[(gpsBufferLastInd + 1) % GPS_BUFFER_SIZE].time < imuTime) {
	DGCSetConditionTrue(gpsBufferFree);
	gpsBufferLastInd++;
	gps3D =  (gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.nav_mode & NAV_3D) && 1;
      
	if (stateMode != INIT) {
	  gpsNavOffset = Cgn * Cnb * gpsOffset;
	  gpsLat = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.lat*M_PI/180 + gpsNavOffset.getelem(0)/EARTH_RADIUS;
	  gpsLon = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.lng*M_PI/180 + gpsNavOffset.getelem(1)/EARTH_RADIUS;
	} else {
	  gpsLat = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.lat*M_PI/180;
	  gpsLon = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.lng*M_PI/180;
	}
	gpsHeight = - gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.ellipsoidal_height;
	gpsVg.setelem(0,gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.vel_n);
	gpsVg.setelem(1,gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.vel_e);
	gpsVg.setelem(2,-gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.vel_u);
	gpsVn = Cgn.transpose() * gpsVg;
	gpsfom = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].data.position_fom;

	gpsTime = gpsBuffer[gpsBufferLastInd % GPS_BUFFER_SIZE].time;
	newGPS = 1;
	
      }
    }
    DGCunlockMutex(&m_GPSDataMutex);

    DGClockMutex(&m_OdomDataMutex);
    if (odomBufferReadInd > odomBufferLastInd) {
      if(odomBuffer[(odomBufferLastInd + 1) % ODOM_BUFFER_SIZE].time < imuTime) {
	DGCSetConditionTrue(odomBufferFree);
	odomBufferLastInd++;
	obdiiVel = odomBuffer[odomBufferLastInd % ODOM_BUFFER_SIZE].speed;
	odomTime = odomBuffer[odomBufferLastInd % ODOM_BUFFER_SIZE].time;
	newOdom = 1;
      }
    }
    DGCunlockMutex(&m_OdomDataMutex);

    if (newGPS == 0 && newIMU == 0 && newOdom == 0) {
      newData.bCond = false;
      DGCWaitForConditionTrue(newData);
    }


    //LOCK MUTEXES
    if (stateMode == PREINIT) { 
      fbSum.clear();
      wb_ibSum.clear();
      dtSum = 0;
      dtWbSum = 0;
      dtFbSum = 0;
      
      
      goodGpsCnt = 0;
      gpsLatSum = 0;
      gpsLonSum = 0;
      gpsHeightSum = 0;
  
      stateMode = INIT;
    } else if(stateMode == INIT) {
      if (dtSum > 5.0 && goodGpsCnt > 20) { //average both imu info and gps info
	lat = gpsLatSum / goodGpsCnt;
	lon = gpsLonSum / goodGpsCnt;
	height = gpsHeightSum / goodGpsCnt;

	//Gtmp = G in body frame
	
	//Wtmp = Earth rotation rate in body frame
	gNorm = fbSum.norm();

	//Work backwards to find roll, pitch, yaw from these.
	pitch = asin(fbSum.getelem(0) / gNorm);
	roll = asin(-fbSum.getelem(1) / (gNorm*cos(pitch)));

	Matrix ctmp(3,3);
	makeBody2Nav(ctmp,roll,pitch,0);
	Matrix wtmp = ctmp * wb_ibSum;
	
	yaw = -alpha + -atan2(wtmp.getelem(1), wtmp.getelem(0));

	makeBody2Nav(Cnb, roll, pitch, yaw);

	gpsNavOffset = Cnb * gpsOffset;
	lat += gpsNavOffset.getelem(0)/EARTH_RADIUS;
	lon += gpsNavOffset.getelem(1)/EARTH_RADIUS;

	makeEarth2Nav(Cne, lat, lon, alpha);
	makeNav2Geo(Cgn, alpha);

	gpsLatSum = 0;
	gpsLonSum = 0;
	gpsHeightSum = 0;
	fbSum.clear();
	wb_ibSum.clear();
	dtSum = 0.0;
	dtFbSum = 0.0;
	dtWbSum = 0.0;
	
	stateMode = IMU_GPS_ODOM;
	kfInit(10, 6);

	lat = asin( -Cne.getelem(2,2) );
	lon =  atan2( -Cne.getelem(2,1), -Cne.getelem(2,0) );
	alpha = atan2( -Cne.getelem(1,2), Cne.getelem(0,2) );
	pitch = asin( - Cnb.getelem(2,0));
	yaw = atan2( Cnb.getelem(1,0), Cnb.getelem(0,0) );
	roll = atan2( Cnb.getelem(2,1), Cnb.getelem(2,2) );

	newGPS = 0;
	imuCount = 0;
	newIMU = 0;

      } else if (newGPS) {
 	gpsLatSum += gpsLat;
	gpsLonSum += gpsLon;
	gpsHeightSum += gpsHeight;
	goodGpsCnt++;
	newGPS = 0;
      } else {
	newIMU = 0;
      }


    } else if (stateMode == IMU_GPS_ODOM || stateMode == IMU_GPS || stateMode == IMU_ODOM || stateMode == IMU) {
      if(newIMU == 1) {
	// Alternate acceleration and rotation updates
	wn_ie = Cne * we_ie;

	wn_ib = Cnb * (wb_ibSum / dtSum);

	wn_ie = Cne * we_ie;

	wn_in = wn_ie + wn_en;

	wn_bn = wn_in - wn_ib;

	Cnb = matrixDiffEqStep(wn_bn * dtSum / 2) * Cnb;



	g0 = GWGS0*(1+GWGS1*pow(sin(lat),2))/sqrt(1 - EPSILON2*pow(sin(lat),2));
	gn.setelem(2,g0*(1+2*height/EARTH_RADIUS));

	fn = Cnb * (fbSum / dtSum);

	vdot = fn - matrixCross(wn_en + wn_ie * 2)*vn + gn;

	vnx = vn.getelem(0) + vdot.getelem(0) * dtSum / 2;
	vny = vn.getelem(1) + vdot.getelem(1) * dtSum / 2;
	Rmeridian = EARTH_RADIUS*(1-EPSILON2)/pow((1-EPSILON2*pow(sin(lat),2)),1.5);
	ca = cos(alpha);
	sa = sin(alpha);
	ca2 = pow(ca,2);
	sa2 = pow(sa,2);
	Rnormal = EARTH_RADIUS/sqrt(1-EPSILON2*pow(sin(lat),2));
	rhox = vny*(ca2/(Rnormal - height) + sa2/(Rmeridian - height)) - vnx*sa*ca*(1/(Rmeridian - height) - 1/(Rnormal - height));
	rhoy = -vnx*(sa2/(Rnormal - height) + ca2/(Rmeridian - height)) + vny*sa*ca*(1/(Rmeridian - height) - 1/(Rnormal - height));
	wn_en.setelem(0,rhox);
	wn_en.setelem(1,rhoy);

	vn = vn + vdot * dtSum;

	height = height + dtSum * vn.getelem(2);

	Cne = matrixDiffEqStep(wn_en * dtSum) * Cne;


	wn_ie = Cne * we_ie;

	wn_ib = Cnb * (wb_ibSum / dtSum);

	wn_ie = Cne * we_ie;

	wn_in = wn_ie + wn_en;

	wn_bn = wn_in - wn_ib;

	Cnb = matrixDiffEqStep(wn_bn * dtSum / 2) * Cnb;


	lat = asin( -Cne.getelem(2,2) );
	lon =  atan2( -Cne.getelem(2,1), -Cne.getelem(2,0) );
	alpha = atan2( -Cne.getelem(1,2), Cne.getelem(0,2) );
	pitch = asin( - Cnb.getelem(2,0));
	yaw = atan2( Cnb.getelem(1,0), Cnb.getelem(0,0) );
	roll = atan2( Cnb.getelem(2,1), Cnb.getelem(2,2) );
	makeNav2Geo(Cgn, alpha);

	vx = vn.getelem(0);
	vy = vn.getelem(1);
	vz = vn.getelem(2);
	px = wn_en.getelem(0);
	py = wn_en.getelem(1);
	pz = wn_en.getelem(2);
	wx = wn_in.getelem(0);
	wy = wn_in.getelem(1);
	wz = wn_in.getelem(2);
	ox = wn_ie.getelem(0);
	oy = wn_ie.getelem(1);
	oz = wn_ie.getelem(2);
	fx = fn.getelem(0);
	fy = fn.getelem(1);
	fz = fn.getelem(2);
	magg = gn.norm();

	double tempprop[100] = {0, 0, -vy/square(EARTH_RADIUS), 0, 1/EARTH_RADIUS, 0, 0, 0, 0, -py,
				0, 0, vx/square(EARTH_RADIUS), -1/EARTH_RADIUS, 0, 0, 0, 0, 0, px,
				0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				-2*(vy*oy + vz*oz), 2*vy*ox, -vz*vx/square(EARTH_RADIUS), vz/EARTH_RADIUS, 2*oz, -(py + 2*oy), 0, -fz, fy, 2*vz*ox,
				2*vx*oy, -2*(vx*ox + vz*oz), -vy*vz/square(EARTH_RADIUS), -2*oz, vz/EARTH_RADIUS, (px + 2*ox), fz, 0, -fx, 2*vz*oy,
				2*vx*oz, 2*vy*oz, (vx*vx + vy*vy)/square(EARTH_RADIUS)+2*magg/EARTH_RADIUS, 2*(py + oy), -2*(px + ox), 0, -fy, fx, 0, -2*(vx*ox + vy*oy),
				0, -oz, -vy/square(EARTH_RADIUS), 0, 1/EARTH_RADIUS, 0, 0, wz, -wy, oy,
				oz, 0, vx/square(EARTH_RADIUS), -1/EARTH_RADIUS, 0, 0, -wz, 0, wx, -ox,
				-oy, ox, 0, 0, 0, 0, wy, -wx, 0, 0,
				py, -px, 0, 0, 0, 0, 0, 0, 0, 0};


	kfdXdt.setelems(tempprop);

	kfA = kfA.eye() + kfdXdt*dtSum;

	kfX = kfA*kfX;

	kfP = (kfA*kfP*kfA.transpose() + kfQ);

	fbSum.clear();
      }

      if (newGPS == 1 && (stateMode == IMU_GPS || stateMode == IMU_GPS_ODOM)) {
	makeEarth2Nav(CneMeas, gpsLat, gpsLon, alpha);
	dTheta = unmatrixCross(Matrix(3,3).eye() - (Cne * CneMeas.transpose()));
        dVn = vn - gpsVn;
	  
	Matrix tmp;
	if(gpsNoVel) {
	  kfZnoVel.setelem(0,dTheta.getelem(0));
	  kfZnoVel.setelem(1,dTheta.getelem(1));
	  kfZnoVel.setelem(2,dH);
	  if (gpsVn.norm() > 1) {
	    kfZnoVel.setelem(3,yaw - atan2(vn.getelem(1),vn.getelem(0)));
	  } else {
	    kfZnoVel.setelem(3,0);
	  }
	    
	  //      mRad2 needs to be defined as (1/R)^2
	  kfRnoVel.setelem(0, 0, pow(gpsfom/100,2)/2/(EARTH_RADIUS * EARTH_RADIUS));
	  kfRnoVel.setelem(1, 1, pow(gpsfom/100,2)/2/(EARTH_RADIUS * EARTH_RADIUS));
	  kfRnoVel.setelem(2, 2, pow(gpsfom/100,2));
	  kfRnoVel.setelem(3, 3, .0000001);
	    
	  tmp = (kfHnoVel*kfP*kfHnoVel.transpose() + kfRnoVel);
	    
	  kfK = kfP*kfHnoVel.transpose()*tmp.inverse();
	    
	  kfX = kfX + kfK*(kfZnoVel - kfHnoVel*kfX);
	    
	  kfP = (kfP.eye() - kfK*kfHnoVel)*kfP;	    
	} else {
	  if(gps3D) {
	    dH = height - gpsHeight;
	    
	    kfZ3d.setelem(0,dTheta.getelem(0));
	    kfZ3d.setelem(1,dTheta.getelem(1));
	    kfZ3d.setelem(2,dH);
	    kfZ3d.setelem(3,dVn.getelem(0));
	    kfZ3d.setelem(4,dVn.getelem(1));
	    //	    kfZ3d.setelem(5,dVn.getelem(2));
	    
	    //      mRad2 needs to be defined as (1/R)^2
	    kfR3d.setelem(0, 0, pow(gpsfom/100,2)/2/(EARTH_RADIUS * EARTH_RADIUS));
	    kfR3d.setelem(1, 1, pow(gpsfom/100,2)/2/(EARTH_RADIUS * EARTH_RADIUS));
	    //kfR3d.setelem(2, 2, pow(gpsfom*3/100,2));
	    kfR3d.setelem(2, 2, 2);
	    kfR3d.setelem(3, 3, pow(1.,2.));
	    kfR3d.setelem(4, 4, pow(1.,2.));
	    kfR3d.setelem(5, 5, pow(2,2.));
	    
	    tmp = (kfH3d*kfP*kfH3d.transpose() + kfR3d);
	    
	    kfK = kfP*kfH3d.transpose()*tmp.inverse();
	    
	    kfX = kfX + kfK*(kfZ3d - kfH3d*kfX);
	    
	    kfP = (kfP.eye() - kfK*kfH3d)*kfP;
	    
	  } else {
	    kfZ2d.setelem(0,dTheta.getelem(0));
	    kfZ2d.setelem(1,dTheta.getelem(1));
	    kfZ2d.setelem(2,dVn.getelem(0));
	    kfZ2d.setelem(3,dVn.getelem(1));
	    
	    //      mRad2 needs to be defined as (1/R)^2
	    kfR2d.setelem(0, 0, pow(gpsfom/100,2)/2/(EARTH_RADIUS * EARTH_RADIUS));
	    kfR2d.setelem(1, 1, pow(gpsfom/100,2)/2/(EARTH_RADIUS * EARTH_RADIUS));
	    kfR2d.setelem(2, 2, pow(2.,2.));
	    kfR2d.setelem(3, 3, pow(2.,2.));

	    tmp = (kfH2d*kfP*kfH2d.transpose() + kfR2d);
	  
	    kfK = kfP*kfH2d.transpose()*tmp.inverse();
	  
	    kfX = kfX + kfK*(kfZ2d - kfH2d*kfX);
	  
	    kfP = (kfP.eye() - kfK*kfH2d)*kfP;
	    
	  }
	}
	newGPS = 0;
      }

      if (newOdom == 1 && (stateMode == IMU_GPS_ODOM || stateMode == IMU_ODOM)) {
	// KF on the obdii speed
	Matrix tempV(3);
	tempV.setelem(0, obdiiVel);
	kfZodom = Cnb*tempV;
	
	kfHodom.setelem(0, 3, obdiiScaleFactor);
	kfHodom.setelem(1, 4, obdiiScaleFactor);
	kfHodom.setelem(2, 5, obdiiScaleFactor);
	
	kfRodom.setelem(0, 0, pow(2., 2.));
	kfRodom.setelem(1, 1, pow(2., 2.));
	kfRodom.setelem(2, 2, pow(2., 2.));
	Matrix tmp;

	tmp = (kfHodom*kfP*kfHodom.transpose() + kfRodom);

	kfK = kfP*kfHodom.transpose()*tmp.inverse();

	kfX = kfX + kfK*(kfZodom - kfHodom*kfX);

	kfP = (kfP.eye() - kfK*kfHodom)*kfP;

	newOdom = 0;
      }

      if (newIMU) {
    

	//      if ((imuCount == 2 || imuCount == 4) && newIMU == 1) {
	dTheta.setelem(0,kfX.getelem(0));
	dTheta.setelem(1,kfX.getelem(1));
	dTheta.setelem(2,kfX.getelem(9));
	dPhi.setelem(0,kfX.getelem(6));
	dPhi.setelem(1,kfX.getelem(7));
	dPhi.setelem(2,kfX.getelem(8));
	dH = kfX.getelem(2);
	dVn.setelem(0,kfX.getelem(3));
	dVn.setelem(1,kfX.getelem(4));
	dVn.setelem(2,kfX.getelem(5));
	
	Cne = matrixDiffEqStep(dTheta * -pError)*Cne;
	
	Cnb = matrixDiffEqStep(dPhi * -1.0)*Cnb;
	
	height = height - pError*dH;
	
	vn = vn - dVn;
	  
	
	lat = asin( -Cne.getelem(2,2) );
	lon =  atan2( -Cne.getelem(2,1), -Cne.getelem(2,0) );
	alpha = atan2( -Cne.getelem(1,2), Cne.getelem(0,2) );
	pitch = asin( - Cnb.getelem(2,0));
	yaw = atan2( Cnb.getelem(1,0), Cnb.getelem(0,0) );
	roll = atan2( Cnb.getelem(2,1), Cnb.getelem(2,2) );
	makeNav2Geo(Cgn, alpha);
	  
	stateJump = sqrt(square(dTheta.getelem(0)*pError) +
			 square(dTheta.getelem(1)*pError))*EARTH_RADIUS;
	  
	// Reset error vector with leftovers from smoothing, if any.
	kfX.clear();
	kfX.setelem(0, (1-pError)*dTheta.getelem(0));
	kfX.setelem(1, (1-pError)*dTheta.getelem(1));
	kfX.setelem(2, (1-pError)*dH);
	kfX.setelem(9, (1-pError)*dTheta.getelem(2));

	newIMU = 0;

	// Update vehicle state struct

	DGClockMutex(&m_VehicleStateMutex);
	
	latsForVehState.latitude = lat*(180/M_PI);
	latsForVehState.longitude = lon*(180/M_PI);	
	
	gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84);
	
	_vehicleState.Timestamp = imuTime;
	
	Matrix rtmp = Cgn*Cnb*vehicleOffsets;
	_vehicleState.Northing = utmForVehState.n + rtmp.getelem(0); 
	_vehicleState.Easting = utmForVehState.e + rtmp.getelem(1);
	_vehicleState.Altitude = height + rtmp.getelem(2);
	
	Matrix rrtmp = matrixCross(wn_bn)*rtmp;
	_vehicleState.Vel_N = vn.getelem(0) - rrtmp.getelem(0);
	_vehicleState.Vel_E = vn.getelem(1) - rrtmp.getelem(1);
	_vehicleState.Vel_D = vn.getelem(2) - rrtmp.getelem(2);
	
	
	Matrix rrrtmp = matrixCross((wn_bn - oldWn_bn)/dtSum)*rtmp - matrixCross(wn_bn)*rrtmp;
	_vehicleState.Acc_N = vdot.getelem(0) - rrrtmp.getelem(0);
	_vehicleState.Acc_E = vdot.getelem(1) - rrrtmp.getelem(1);
	_vehicleState.Acc_D = vdot.getelem(2) - rrrtmp.getelem(2);
	
	_vehicleState.Roll = roll;
	_vehicleState.Pitch = pitch + .066;
	_vehicleState.Yaw = yaw + alpha;	
	_vehicleState.RollAcc = (wb_ibSum.getelem(0) - oldWb_ibSum.getelem(0))/dtSum;
	_vehicleState.PitchAcc = (wb_ibSum.getelem(1) - oldWb_ibSum.getelem(1))/dtSum;
	_vehicleState.YawAcc = (wb_ibSum.getelem(2) - oldWb_ibSum.getelem(2))/dtSum;
	_vehicleState.RollRate = wb_ibSum.getelem(0)/dtSum;
	_vehicleState.PitchRate = wb_ibSum.getelem(1)/dtSum;
	_vehicleState.YawRate = wb_ibSum.getelem(2)/dtSum;

	_vehicleState.NorthConf = 0;
	_vehicleState.EastConf = 0;
	_vehicleState.HeightConf = 0;
	_vehicleState.RollConf = 0;
	_vehicleState.PitchConf = 0;
	_vehicleState.YawConf = 0;
	
	oldWn_bn = wn_bn;
	oldWb_ibSum = wb_ibSum;
	DGCunlockMutex(&m_VehicleStateMutex);
	
	wb_ibSum.clear();
	dtSum = 0.0;
	
	if (count++ % 8 == 0) {
	  if (_astateOpts.debug == 1) {
	    cout.precision(20);
	    cout << lat << '\t' << lon << '\t' << gpsLat << '\t' << gpsLon << '\t' << roll << '\t' << pitch << '\t' << yaw + alpha << '\t' << gpsfom << '\t';
	    cout << height << '\t' << gpsHeight << endl;
	  }
	  //All Your Network Traffic Are Belong To Us
	  if (_astateOpts.fastMode == 0) {
	    broadcast(); 
	  }
	}
      }
    } else if (stateMode == GPS_ODOM || stateMode == GPS) {	
      // Stuff goes here.
    } else if (stateMode == ODOM) {
      // Stuff goes here.
    } else if (stateMode == OH_SHIT) {
      cerr << "All heartbeats failed.  Shutting down state." << endl;
      exit(1);
    } else if (stateMode == STATIONARY) {
      // Stuff goes here.
    } else {
      stateMode = PREINIT; 
    }
  }
}

