ASTATE_PATH = $(DGC)/projects/newastate

ASTATE_DEPEND_LIBS = $(SKYNETLIB) \
		$(FRAMESLIB)
		$(MODULEHELPERSLIB) \
		$(SERIALLIB) \
		$(SPARROWLIB) \
		$(GGISLIB) \
		$(GPSLIB) \
		$(GPSSDSLIB) \
		$(SERIALLIB) \
		$(SDSLIB) \
		$(IMULIB) \
		$(ALICELIB)

ASTATE_DEPEND_SOURCES = $(ASTATE_PATH)/AState.cc \
		$(ASTATE_PATH)/AState.hh \
		$(ASTATE_PATH)/AState_GPS.cc \
		$(ASTATE_PATH)/AState_IMU.cc \
		$(ASTATE_PATH)/AState_Main.cc \
		$(ASTATE_PATH)/AState_test.cc \
		$(ASTATE_PATH)/AState_test.hh \
		$(ASTATE_PATH)/AState_Sparrow.cc \
		$(ASTATE_PATH)/AState_Kalman.cc \
		$(ASTATE_PATH)/AState_Update.cc \
		$(ASTATE_PATH)/AState_MatrixOps.cc \
		$(ASTATE_PATH)/AState_MetaState.cc \
		$(ASTATE_PATH)/AState_OBDII.cc \
		$(ASTATE_PATH)/asdisp.dd \
		$(ASTATE_PATH)/astune.dd 

ASTATE_DEPEND = $(ASTATE_DEPEND_LIBS) $(ASTATE_DEPEND_SOURCES)
