/********************
Haomiao Huang
3 x 3 matrix class
Modify for use to generate rotation matrices
********************/

#include "Matrix.hh"
#include <iostream>
#include <cassert>
#include <math.h>

enum CBLAS_ORDER {CblasRowMajor=101, CblasColMajor=102 };
enum CBLAS_TRANSPOSE {CblasNoTrans=111, CblasTrans=112, CblasConjTrans=113,
		      AtlasConj=114};

extern "C" void cblas_daxpy(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY);

extern "C" void cblas_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc);

extern "C" void cblas_dscal(const int N, const double alpha, double *X, const int incX);

extern "C" int clapack_dgetri(const enum CBLAS_ORDER Order, const int N, double *A,
				   const int lda, const int *ipiv);
extern "C" int clapack_dgetrf(const enum CBLAS_ORDER Order, const int M, const int N,
                   double *A, const int lda, int *ipiv);

extern "C" double cblas_dnrm2(const int N, const double *X, const int incX);

using namespace std;

// ructors
Matrix::Matrix()
{
  rows = 0;
  cols = 0;
  size = 0;
  data = NULL;
}

Matrix::Matrix(int newrows, int newcols) // create empty matrix
{
  rows = newrows;
  cols = newcols;
  size = rows*cols;
  data = new double[size];
  clear();

}

Matrix::Matrix(int elems) // create empty matrix
{
  rows = elems;
  cols = 1;
  size = rows*cols;
  data = new double[size];
  clear();
}


Matrix::Matrix(const Matrix &matrix2)// copy ructor
{  
  rows = matrix2.getrows(); 
  cols = matrix2.getcols();
  size = rows*cols;
  data = new double[size];
  memcpy(data, matrix2.data, (size*sizeof(double))); 
}

void Matrix::clear()
{
  memset(data,0,size*sizeof(double));
}

// Mutators
// sets the rowth colth element the matrix to value
void Matrix::setelems(double* newdata)  
{
  memcpy(data, newdata, (size*sizeof(double)));
}

void Matrix::setelem(int row, int col, double value) 
{
  assert(row < rows && col < cols);
  data[row*cols + col] = value;
}

void Matrix::setelem(int elem, double value) 
{
  assert(elem < size);
  data[elem] = value;
}

double Matrix::getelem(int row, int col)  const 
{
  assert(row < rows && col < cols);
  return data[row*cols + col];
} 
double Matrix::getelem(int elem)  const 
{
  assert(elem < size);
  return data[elem];
} 
/*! returns the transpose of the matrix. */
double* Matrix::getdata() const 
{
  return data;
}


Matrix Matrix::transpose() const
{
  Matrix result(cols, rows);
  double transdata[size];
  for(int i = 0;i < rows;i++) {
    for(int j = 0;j < cols;j++) {
      transdata[j*rows + i] = data[i*cols + j];
    }
  }
  result.setelems(transdata);
  return result;
}

Matrix Matrix::eye() const
{
  Matrix result(cols, rows);
  for(int i = 0;i < rows;i++) {
    result.setelem(i,i,1.0);
  }
  return result;
}

Matrix Matrix::inverse() const
{
  Matrix result(*this);
  int invdata[rows];
  clapack_dgetrf(CblasRowMajor, rows, cols, result.getdata(), cols, invdata);
  clapack_dgetri(CblasRowMajor, rows, result.getdata(), cols, invdata);
  return result;
}

double Matrix::norm() const
{
  return cblas_dnrm2(size,data,1);
}


Matrix Matrix::operator+(const Matrix& matrix2) const 
{
  assert((matrix2.getrows() == rows)&&(matrix2.getcols() == cols));
  // only add if matrices are the same size
  Matrix result(*this); // create a copy of the current matrix
  cblas_daxpy(size,1.0,matrix2.getdata(),1,result.getdata(),1);
  return result;
}

Matrix Matrix::operator-(const Matrix &matrix2) const
{
  assert((matrix2.getrows() == rows)&&(matrix2.getcols() == cols));
  Matrix result(*this); // create a copy of the current matrix
  cblas_daxpy(size,-1.0,matrix2.getdata(),1,result.getdata(),1);
  return result;
}


Matrix Matrix::operator*(const Matrix &matrix2) const
{
  /* Matrix multiplication:
  multiply ith row by jth column and sum results
  n x m * i x n = n x i */

  assert(cols == matrix2.getrows());
  // only multiply when you can
  Matrix result(rows, matrix2.getcols());
  cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, rows, matrix2.getcols(), cols, 1.0, data, cols, matrix2.getdata(), matrix2.getcols(), 0.0, result.getdata(), matrix2.getcols());
  return result;
}

Matrix Matrix::operator*(const double sf) const
{
  Matrix result(*this); // create a copy of the current matrix
  cblas_dscal(size,sf,result.getdata(),1);
  return result;
}

Matrix Matrix::operator/(const double sf) const
{
  Matrix result(*this); // create a copy of the current matrix
  cblas_dscal(size,1/sf,result.getdata(),1);
  return result;
}


Matrix& Matrix::operator=(const Matrix &matrix2)
{
  if (data != NULL) {
    delete [] data;
  }
  rows = matrix2.getrows(); 
  cols = matrix2.getcols();
  size = rows*cols;

  data = new double[size];
  memcpy(data, matrix2.data, (size*sizeof(double)));
  return *this;
}

void Matrix::print() {
  for (int i=0; i<rows; ++i) {
    for (int j=0; j<cols; ++j)  printf ("%2.20f\t", data[i*cols+j]);
    printf("\n");
  }
  printf("\n");
}

// Destructors
Matrix::~Matrix()
{
  if (data != NULL) {
    delete [] data;
  }
}

// Note--cross product functions should only be used on 3x3 matrices
Matrix matrixCross(Matrix vect) {
  double tmp[] = {0, -vect.getelem(2), vect.getelem(1),
		  vect.getelem(2), 0, -vect.getelem(0),
		  -vect.getelem(1), vect.getelem(0), 0};
  Matrix newmat(3,3);
  newmat.setelems(tmp);
  return newmat;
}

Matrix unmatrixCross(Matrix mat) {
  double tmp[] = {-mat.getelem(1,2),mat.getelem(0,2),-mat.getelem(0,1)};
  Matrix newmat(3);
  newmat.setelems(tmp);
  return newmat;
}

Matrix matrixDiffEqStep(Matrix w) {
  Matrix rot;
  if (w.norm() > 0) {
    double nw = w.norm();
    w = w/nw;
    Matrix what = matrixCross(w);
    rot = Matrix(3,3).eye() + what * sin(-nw) + what * what * (1 - cos(-nw));
  } else {
    rot = Matrix(3,3).eye();
  }
  return rot;
}



