/**
 * MainTab.cc
 * Revision History:
 * 01/07/2005  hbarnor  Created
 */

#include <iostream>

#include "MainTab.h"

MainTab::MainTab()
  : statusFrm("Status Table"),
    commandFrm("Command"),
    logFrm("Command Log")
{
  // add the components
  add(statusFrm);
  add(commandFrm);
  add(logFrm);
  // set the style of the frames
  statusFrm.set_shadow_type(Gtk::SHADOW_ETCHED_OUT);
  commandFrm.set_shadow_type(Gtk::SHADOW_ETCHED_OUT);
  logFrm.set_shadow_type(Gtk::SHADOW_ETCHED_OUT);
  statusFrm.set_label_align(Gtk::ALIGN_RIGHT, Gtk::ALIGN_TOP);
  commandFrm.set_label_align(Gtk::ALIGN_RIGHT, Gtk::ALIGN_TOP);
  logFrm.set_label_align(Gtk::ALIGN_RIGHT, Gtk::ALIGN_TOP);
  show_all_children();
}

MainTab::~MainTab()
{
  // nothing here yet
}
