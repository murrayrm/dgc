#!/bin/bash

# whether or not we want to keep processing the file after we fail
# 0 = don't continue
# 1 = continue anyway
export continue_after_failure=1

export header="   "
export num_failures=0

# make sure they entered one argument
if (( $# != 2 )) ; then
    echo "Description:"
    echo "  This script uses the todo_timber file (output by the timber module) to"
    echo "  grab all the logs that have been generated on the various computers."
    echo "  Please do not put the logs in the same spot in which they were originally"
    echo "  created - this will cause problems.  Right now, all logs are created in"
    echo "   /tmp/logs/, so don't put the logs there when we collect them."
    echo "  For more info, see the comments at the top of the script."
    echo 
    echo "Usage:"
    echo "  $0 computer-to-ssh-to where-to-put-logs"
    echo 
    echo "Example:"
    echo "  If we were running the timber module on skynet 7, and wanted"
    echo "  to put the logs from the race machines and development cluster"
    echo "  onto skynet2, in the /tmp/test_logs folder, we would run"
    echo
    echo "  user@skynet7$ $0 skynet2 /tmp/test_logs/"
    exit 1
fi


# proccess the todo_timber file, appending `delete' at the head of every
# line whose data should be trashed
# using sed because some comps don't have tac...
#cat todo_timber | sed '1!G;h;$!d' | gawk 'BEGIN {dele=0; print "lkjsdf"} {if (dele == 0) {if ($1 == "#" && $2 == "!!!" && $3 == "delete") {dele=1; print}} else {if ($1 == "#") {dele=0; print} else {print "delete " $0}}}' | sed '1!G;h;$!d'

cat todo_timber | sed '1!G;h;$!d' | gawk 'BEGIN {dele=0} {if (substr($1, 1, 1) == "#") (dele=0); if (dele == 0) {if (substr($1, 1, 1) == "#" && $2 == "trash" && $3 == "!!!") {dele=1; print} else {print}} else {if (substr($1, 1, 1) == "#") {dele=0; print} else {if ($1 != "trash") {print "trash " $0} else print $0}}}' | sed '1!G;h;$!d' > grab_logs_temp_file
mv grab_logs_temp_file todo_timber

if [ 0 == 0 ]; then
    export DEST_DIR=$2
    # ensure that DEST_DIR ends with a /
    last_char=`echo $DEST_DIR | egrep -o ".$"`
    if [ "$last_char" == "/" ]; then
	# good
	true
    else
	export DEST_DIR="$DEST_DIR/"
    fi
	
#    export ME=`whoami`
    DEST_COMP=$1
# ok, finally found which grep will remove all teh commented parts of a file!!
# you have NO IDEA how long i've wanted to find this... but it always eluded me
# but now, i seem to haev gotten luck, cuz i still don't *really* see why it
# prints out the first half of the line instead of the second....but ...cool
#################################
#      egrep -o [^\#]\*         #
#################################
    
# make a temp file
#    tempfile=`mktemp /tmp/timber-todo.XXXXXX`

    echo "------------------------------------------------"
    echo "destination directory is:"
    echo $DEST_DIR
    echo 
    
    echo "destination computer is:"
    echo $DEST_COMP
    echo "------------------------------------------------"

    if [ "$DEST_DIR" == "/tmp/logs/" ]; then
	echo "siigh...don't be silly!"
	echo "you can't use /tmp/logs/ as a location."
	echo "Run $0 with no args for more info"
	exit 1
    fi
    
#    echo "running commands in interactive mode, type \'y\' ENTER to actually run the command"
    echo

    export N=`cat todo_timber | egrep -o [^\#]\* | sed -n 'G; s/\n/&&/; /^\([ -~]*\n\).*\n\1/d; s/\n//; h; P' | wc -l`
    echo -e "\033[01;32mRead $N unique lines from todo_timber.\033[00;00m"    
    for i in `seq 1 $N`; do
	export line=`cat todo_timber | egrep -o [^\#]\* | sed -n 'G; s/\n/&&/; /^\([ -~]*\n\).*\n\1/d; s/\n//; h; P' | sed -n "${i}p"`
	echo -e "\033[01;34mProcessing line $i\033[00;00m: \"$line\""
	if [ "$line" == "" ]; then
	    echo 
	    echo "Done processing todo_timber file."
	    break
	fi
	
	# check for requested deletion
	first=`echo $line | gawk '{print $1}'`
	export line_processed=0
	if test $first = "trash"; then
	    echo -n "${header}deleting remote log directory... "
	    export ssh_comp=`echo $line | gawk '{print $2}' | grep -o ^.*: | cut -d : -f 1`
	    export dir_to_remove=`echo $line | gawk '{print $2, $3}' | cut -d : -f 2 | gawk '{print $1 $2}'`
	    ssh $ssh_comp "rm -rf $dir_to_remove" && export line_processed=1
	    
	    if test $line_processed -eq 0; then
		export num_failures=$(($num_failures + 1))
		echo -e "\033[01;31mfailed!\033[00;00m"
	    else
		echo -e "\033[01;35mtrashed.\033[00;00m"
	    fi
	    continue
	else
	    export line_processed=1
	fi
	if test \( $line_processed -eq 0 \) -a \( $continue_after_failure -eq 0 \) ; then
	    break;
	fi

	# copy the file
	echo -n "${header}copying directory...             "
	export line_processed=0
	# remove the 'jason@' or whatever from the front; makes it ssh as the current user
	line_impersonal=`echo $line | egrep -o "@.*" | cut -b 2-`
	echo $line_impersonal \
	    | gawk "{print \"\\\"\" \"mkdir -p $DEST_DIR\" \$2 \" && cd $DEST_DIR\" \$2 \" && cd .. && scp -r \" \$1\$2 \" .\\\"\"}" \
	    | xargs -i ssh $DEST_COMP '{}' && export line_processed=1
	if test $line_processed -eq 0; then
	    echo -e "\033[01;31mfailed!\033[00;00m"
	    export num_failures=$(($num_failures + 1))
	else
	    echo -e "\033[01;32mdone.\033[00;00m"
	fi

	if test \( $line_processed -eq 0 \) -a \( $continue_after_failure -eq 0 \); then
	    break;
	fi

	# remove the file in the remote location
	echo -n "${header}removing remote log directory... "
	export line_processed_2=0
	if test $line_processed -eq 0; then
	    echo -e "\033[01;31mskipped.\033[00;00m"
	else
	    export ssh_comp=`echo $line_impersonal | gawk '{print $1}' | grep -o ^.*: | cut -d : -f 1`
	    export dir_to_remove=`echo $line | cut -d : -f 2 | gawk '{print $1 $2}'`
	    ssh $ssh_comp "rm -rf $dir_to_remove" && export line_processed_2=1
	    
	    if test $line_processed_2 -eq 0; then
		export num_failures=$(($num_failures + 1))
		echo -e "\033[01;31mfailed!\033[00;00m"
	    else
		echo -e "\033[01;32mdone.\033[00;00m"
	    fi
	fi
	
	if test \( $line_processed_2 -eq 0 \) -a \( $continue_after_failure -eq 0 \); then
	    break;
	fi
    done ## end of for loop

    if test $num_failures -gt 0; then
	echo -e "\033[01;31mThere were $num_failures failures!\033[00;00m"
	echo -e "\033[01;31mPlease comment out all the lines in todo_timber that succeeded before attempting to re-run this script.\033[00;00m"
	echo "If you don't, they'll just show up as failures because the files have already been moved."
    else
	echo -e "\033[01;32mScript finished.\033[00;00m"
    fi
fi





############################
# IGNORE ALL THIS CRAP
############################

#scp jason@192.168.0.34:/home/jason/timber/logs/2005_06_03/22_54_26/rddfPathGen/ jason@192.168.0.52:/donkey/logs/2005_06_03/22_54_26/rddfPathGen/

if [ 1 == 0 ] ; then
    ssh skynet2 "mkdir -p /tmp/logs/2005_06_03/22_54_26/rddfPathGen/ && \
cd /tmp/logs/2005_06_03/22_54_26/rddfPathGen/ && \
cd .. && \
scp -r jason@192.168.0.34:/home/jason/timber/logs/2005_06_03/22_54_26/rddfPathGen/ ."
fi

if [ 1 == 0 ]; then
    ssh jason@192.168.0.34 tar -cf - /home/jason/timber/logs/2005_06_03/22_54_26/rddfPathGen/ | tar -xf - -C /tmp/logs/2005_06_03/22_54_26/rddfPathGen/
fi


if [ 1 == 0 ]; then
    for line in $(cat todo)  # Concatenate the converted files.
                         # Uses wild card in variable list.
      do
      echo $line;
    done
fi


#cat todo_timber | egrep -o .*[\#]
#gives before comemnts, with # at end

# works (for soommmem reason....?)
# basically, this gets rid of comments
# cat todo_timber | egrep -o [^\#]\*
