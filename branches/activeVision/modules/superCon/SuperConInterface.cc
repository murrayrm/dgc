#include"SuperConInterface.hh"

////////////////////////////////////////////////////////////////////////////////////
//Serialization functions for supercon_msg_cmd
/*! Serialize n bytes from pBuf to the throw command */
void sc_serialize_raw(supercon_msg_cmd *pCmd, void *pBuf, int n)
{
  if(pCmd->user_bytes+n > SUPERCON_MAX_USER_BYTES) {
    pCmd->user_bytes += n;  //Increase, just to show that to many bytes where required
    return;
  }
  memcpy(pCmd->user_data + pCmd->user_bytes, pBuf, n);
  pCmd->user_bytes += n;
}

//template<typename T>
//void sc_serialize(supercon_msg_cmd *pCmd, T t)
//{
//  sc_serialize_raw(pCmd, (void*)&t, sizeof(T));
//}

template<>
void sc_serialize<const char *>(supercon_msg_cmd *pCmd, const char *t)
{
  sc_serialize_raw(pCmd, (void*)t, strlen(t)+1);
}

template<>
void sc_serialize<char *>(supercon_msg_cmd *pCmd, char *t)
{
  sc_serialize_raw(pCmd, (void*)t, strlen(t)+1);
}
template<>
void sc_serialize<const unsigned char *>(supercon_msg_cmd *pCmd, const unsigned char *t)
{
  sc_serialize_raw(pCmd, (void*)t, strlen((const char*)t)+1);
}
template<>
void sc_serialize<unsigned char *>(supercon_msg_cmd *pCmd, unsigned char *t)
{
  sc_serialize_raw(pCmd, (void*)t, strlen((const char*)t)+1);
}

template<>
void sc_serialize<const string>(supercon_msg_cmd *pCmd, const string t)
{
  sc_serialize(pCmd, t.c_str());
}
template<>
void sc_serialize<string>(supercon_msg_cmd *pCmd, string t)
{
  sc_serialize(pCmd, t.c_str());
}

////////////////////////////////////////////////////////////////////////////////////
//Deserialization functions for supercon_throw_cmd
/*! Deserialize n bytes from the throw command at position npos to pBuf */

void sc_deserialize_raw(supercon_msg_cmd *pCmd, int &npos, void *pBuf, int n)
{
  if(npos+n > pCmd->user_bytes || npos+n > SUPERCON_MAX_USER_BYTES) {
    npos+=n; //Increase, just to show that to many bytes where required
    return;
  }
  memcpy(pBuf, pCmd->user_data + npos, n);
  npos += n;
}

//Specializations for some common types
template<>
void sc_deserialize<string>(supercon_msg_cmd *pCmd, int &npos, string &t)
{
  //Read from the user_data up to a terminating 0 or end of data
  int end = npos;

  while(end<SUPERCON_MAX_USER_BYTES && end<pCmd->user_bytes && pCmd->user_data[end]!=0)
    ++end;

  if(end<SUPERCON_MAX_USER_BYTES && end<pCmd->user_bytes) {
    //Fully deserializable string, thus is zero terminated
    t = (const char*)(&pCmd->user_data[npos]); //Do a string assigment, the string assigment operator will copy data up to the zero terminator
  }

  npos = end+1;   //Skip over the terminating zero
}

/** This has never caused any issues, and will not be implemented for the race **/
//#warning Should SuperCon serialize int:s to network order?
