#include "rddf.hh"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fstream>

int main(int argc, char **argv) {
  double speed;
  string line;
  int currentWaypoint, lowerWaypoint, upperWaypoint;

  if (argc != 6) {
    cerr << "Syntax: speedCap infile outfile speedInMPH "
	 << "lowerWaypoint upperWaypoint" << endl;
    exit(1);
  }

  ifstream fileIn;
  fileIn.open(argv[1], ios::in);

  ofstream fileOut;
  fileOut.open(argv[2], ios::out);

  speed = atof(argv[3]);

  lowerWaypoint = atoi(argv[4]);
  upperWaypoint = atoi(argv[5]);

  int i = 0;
  currentWaypoint = 0;
  while(!fileIn.eof()) {
    // Read in value.
    if(i == 4) { // last element in RDDF line
      getline(fileIn,line,'\n');
    } else {
      getline(fileIn,line,RDDF_DELIMITER);
    }

    // Drop blank lines
    if (line.empty()) {
      continue;
    }

    // Update waypoint being looked at
    if (i == 0) {
      currentWaypoint = atoi(line.c_str());
    }

    // Cap speed if it falls in the appropriate range, 
    // otherwise output what was before
    if (i == 4 && currentWaypoint >= lowerWaypoint && currentWaypoint <= upperWaypoint) {
      fileOut << speed << endl;
    } else if (i == 4) {
      fileOut << line << endl;
    } else {
      fileOut << line << RDDF_DELIMITER;
    }

    // Iterate the item being looked at
    i = (i + 1)%5;
  }
}
