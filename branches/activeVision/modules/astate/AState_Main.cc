#include <getopt.h>
#include "AState.hh"
#include <stdlib.h>
#include "DGCutils"

void print_usage() {
  cout << "Usage: astate [options]" << endl;
  cout << '\t' << "--help, -h" << '\t' << "Display this message" << endl;
  cout << '\t' << "--noimu, -i" << '\t' << "Disable IMU" << endl;
  cout << '\t' << "--nogps, -g" << '\t' << "Disable Navcom GPS" << endl;
  cout << '\t' << "--obd, -o" << '\t' << "Enable obdii" << endl;
  cout << '\t' << "--nov, -n" << '\t' << "Disable Novatel GPS" << endl;
  cout << '\t' << "--now, -!" << '\t' << "Run without warning massage at start" << endl;
  cout << '\t' << "--autotune, -a" << '\t' << "Output appropriate parameters for autotuning" << endl;
  cout << '\t' << "--nosparrow, --nosp, -x" << '\t' << "Disable sparrow" << endl;
  cout << '\t' << "--nokf -k" << '\t' << "Disable the kalman filter" << endl;
  cout << '\t' << "--verbose, -v" << '\t' << "Enable verbose mode" << endl;
  cout << '\t' << "--snkey, -s <snkey>" << '\t' << "Use skynetkey <snkey> (defaults to $SKYNET_KEY)" << endl;
  cout << '\t' << "--log, -l " << '\t' << "Log raw data to /tmp/log/astate_raw/<timestamp>" << endl;
  cout << '\t' << "--replay, -r <logfile>" << '\t' << "Replay raw data from <logfile>" << endl;
  cout << '\t' << "--old, -w" << '\t' << "Log file is of old format" << endl;
  cout << '\t' << "-p" << '\t' <<  "Start astate in timber playback mode" << endl;
  cout << '\t' << "-d" << '\t' << "Log debug information" << endl;
  cout << '\t' << "-c" << '\t' << "Depend on supercon messages" << endl;
}

const char* const short_options = "higonakvs:lr:f:pdxwc!";

static struct option long_options[] =
  {
    //first: long option (--option) string
    //second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    //third: if pointer, set variable to value of fourth argument
    //	 if NULL, getopt_long returns fourth argument
    {"help",	0,	NULL,	'h'},
    {"noimu",	0,	NULL,	'i'},
    {"nogps",	0,	NULL,	'g'},
    {"obd",     0,      NULL,   'o'},
    {"o",       0,      NULL,   'o'},
    {"nov",       0,      NULL,   'n'},
    {"autotune",0,      NULL, 'a'},
    {"nokf",	0,	NULL,	'k'},
    {"verbose",	0,	NULL,	'v'},
    {"snkey",	1,	NULL,	's'},
    {"s",	1,	NULL,	's'},
    {"log",	0,	NULL,	'l'},
    {"l",	0,	NULL,	'l'},
    {"replay",	1,	NULL,	'r'},
    {"r",	1,	NULL,	'r'},
    {"fast",	1,	NULL,	'f'},
    {"f",	1,	NULL,	'f'},
    {"p",   0,	NULL,   'p'},
    {"d",   0,	NULL,   'd'},
    {"nosparrow", 0, NULL,  'x'},
    {"x",   0,      NULL,   'x'},
    {"nosp", 0,     NULL,   'x'},
    {"old", 0,     NULL,   'w'},
    {"c", 0,     NULL,   'c'},
    {"now", 0,      NULL,   '!'},
    {0,0,0,0}
  };

AState *ss;

astateOpts inputAstateOpts;

int main(int argc, char **argv)
{
  // Set this so we know the difference between a key set in the 
  // command line and an uninitialized key.
  int sn_key = -1;
  int now = 0;

  // Temporary variables for command-line parsing
  int options_index;
  int ch;

  // Set default options for the astateOpts struct.
  inputAstateOpts.useIMU = 1;
  inputAstateOpts.useGPS = 1;
  inputAstateOpts.useNov = 1;
  inputAstateOpts.useKF = 1;
  inputAstateOpts.useOBD = 1;
  inputAstateOpts.useVerbose = 0;
  inputAstateOpts.logRaw = 0;
  inputAstateOpts.useReplay = 0;
  inputAstateOpts.timberPlayback = 0;
  inputAstateOpts.debug = 0;
  inputAstateOpts.useSparrow = 1;
  inputAstateOpts.fastMode = 0;
  inputAstateOpts.oldLog = 0;
  inputAstateOpts.useAutotune = 0;
  inputAstateOpts.useSC = 0;

  // Parse command-line options
  while((ch = getopt_long(argc, argv, short_options, long_options, &options_index)) != -1)
    {
      switch(ch)
	{
	case 'h':
	  print_usage();
	  return 0;
	  break;
	case 'i':
	  inputAstateOpts.useIMU = 0;
	  break;
	case 'g':
	  inputAstateOpts.useGPS = 0;
	  break;
	case 'o':
	  inputAstateOpts.useOBD = 0;
	  break;
	case 'n':
	  inputAstateOpts.useNov = 0;
	  break;
	case 'k':
	  inputAstateOpts.useKF = 0;
	  break;
	case 'v':
	  inputAstateOpts.useVerbose = 1;
	  break;
	case 's':
	  sn_key = atoi(optarg);
	  break;
	case 'l':
	  inputAstateOpts.logRaw = 1;
	  break;
	case 'd':
	  inputAstateOpts.debug = 1;
	  break;
	case 'r':
	  inputAstateOpts.useReplay = 1;
	  strncpy(inputAstateOpts.replayFile, optarg, 99);
	  break;
	case 'f':
	  inputAstateOpts.useReplay = 1;
	  inputAstateOpts.fastMode = 1;
	  strncpy(inputAstateOpts.replayFile, optarg, 99);
	  break;
	case 'p':
	  inputAstateOpts.timberPlayback = 1;
	  break;
	case 'x':
	  inputAstateOpts.useSparrow = 0;
	  break;
	case 'w':
	  inputAstateOpts.oldLog = 1;
	  break;
	case 'c':
	  inputAstateOpts.useSC = 1;
	  break;
	case '!':
	  now = 1;
	  break;
	case 'a':
	  inputAstateOpts.useAutotune = 1;
	  break;
	default:
	  break;
	}
    }
	
  if (inputAstateOpts.useReplay == 1 && inputAstateOpts.logRaw == 1) {
    cerr << "Don't log a replay file." << endl;
    return 0;
  }

  if (!(inputAstateOpts.useReplay || inputAstateOpts.timberPlayback || now)) {
    cerr << "Warning: AState should be started from a dead stop for proper orientation." << endl;
    cerr << "Hit enter to continue once the vehicle is stopped." << endl;
    cerr << "It will take approximately 30 seconds for state to converge." << endl;
    cerr << "DO NOT MOVE DURING THAT TIME!" << endl;
    getchar();
  }

  char* pSkynetkey = getenv("SKYNET_KEY");
  if (sn_key == -1) {
    if ( pSkynetkey == NULL )
      {
	cerr << "SKYNET_KEY environment variable isn't set." << endl;
      } else {
	sn_key = atoi(pSkynetkey);
      }
  }
	
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  //Create AState server:
  ss = new AState(sn_key, inputAstateOpts);

  ss->active();

  delete ss;

  return 0;
}
