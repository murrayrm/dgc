#include <sn_msg.hh>
#include <sys/time.h>
#include <iostream>
#include <unistd.h>
#include <signal.h>

#define us_p_s  1000000 // 4294967296

using namespace std;
void finish(int status);

int send_chan; 
unsigned int n, msg_size;
double totaltime;
long long int newmytimeint, starttime;
skynet* skynetptr;
int main(int argc, char** argv)
{
  int key; 
  double msg_rate;  //Bytes per second
  double testlen;
  if (argc < 4)
  {
    cerr << "This program should only be called from snm-main.sh" << endl;
    cerr << "usage: single-throughput-master key msg_size rate time" << endl;
    cerr << "key is the skynet key, ";
    cerr << "msg_size is in bytes, ";
    cerr << "rate is bytes per second, time is test time in seconds" << endl;
      
    exit(2);
  }
  else
  {
    key = atoi(argv[1]);
    msg_size = atoi(argv[2]);
    msg_rate = (double)atoi(argv[3]);
    testlen = atof(argv[4]); 
    alarm((unsigned int)(testlen+1));
    signal(SIGALRM, finish);
    //cout << key << " " << msg_size << " " << msg_rate << " " << testlen << endl ;
  }
  //converg msg_rate into messages per second
  msg_rate = (double)msg_rate / (double)msg_size;
  unsigned int delay = (unsigned int) (1000000 / msg_rate); //microseconds / message
  struct timeval mytime;
  long long int mytimeint;
  gettimeofday(&mytime,0);
  skynet Skynet(MODmark, key);
  skynetptr = &Skynet;
  send_chan = Skynet.get_send_sock(SNmark1);
  char buffer[msg_size];
  buffer[0] = 0;                            //continue character
  mytimeint = mytime.tv_sec * us_p_s + mytime.tv_usec; 
  starttime = mytimeint;
  long long int dtime;
  n = 0;
  long long int timer = (long long int)1000000 * testlen;  //in microseconds
  while(timer > 0)
  {
    Skynet.send_msg(send_chan, buffer, msg_size, 0);
    gettimeofday(&mytime, 0);
    newmytimeint = mytime.tv_sec * us_p_s + mytime.tv_usec;
    dtime = newmytimeint - mytimeint;
    if (delay - dtime > 1) usleep(delay - dtime);
    mytimeint += delay;
    timer -= delay;
    n++;
  }
  //now kill the slave.  Send multiple kill signals
  finish(0);
  return 0;
}

void finish(int signum)
{
  char buffer[1];
  buffer[0] = 1;                            //kill character
  int m;
  totaltime = (double)(newmytimeint - starttime)/1000000;
  for(m=0; m<=25; m++)
  {
    usleep(1000);                             //wait a while
    skynetptr->send_msg(send_chan, buffer, 1, 0); //send 1 char
  }
  cerr << "messages= " << n << "\ttime= " << totaltime ;
  double real_rate = (double)(n * msg_size) / totaltime;
  cerr << "\trate= " << real_rate << endl;
  cout << n << " " << real_rate << " " << totaltime <<endl; 
  if (signum==0) exit(0); else exit(1);
}
