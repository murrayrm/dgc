/*
 * FollowClient.cc - skynet module for follow
 *
 * RMM, 10 Dec 05
 *
 */

#include "follow.h"

FollowClient::FollowClient(int sn_key) : CSkynetContainer(SNastate, sn_key)
{
  cerr << "FollowClient initalized on key " << sn_key << "\n";
}

/* 
 * Member function for reading state
 *
 */
void FollowClient::ReadState()
{
  cerr << "Calling ReadState() function\n";

  while (1) {
    // Update the current state (via broadcast message)
    UpdateState();

    // Put the data in a simple array for use by the controller
    inp[XPOS] = m_state.Northing - xorigin;
    inp[YPOS] = m_state.Easting - yorigin;
    inp[TPOS] = m_state.Yaw;

    inp[XVEL] = m_state.Vel_N;
    inp[YVEL] = m_state.Vel_E;
    inp[TVEL] = m_state.YawRate;
  }
}

/*
 * Member function for reading trajectories
 *
 */
void FollowClient::ReadTraj()
{
  cerr << "Calling ReadTraj() function\n";

  while (1) {
    sleep(1);
  }
}

/*
 * Member function for executing control loop
 *
 */
void FollowClient::ControlLoop()
{
  cerr << "Calling ControlLoop() function\n";

  while (1) {
    sleep(1);
  }
}
