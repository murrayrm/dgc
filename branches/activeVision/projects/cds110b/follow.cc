/*
 * follow.cc - simple trajectory tracking algorithm for CDS 110b
 *
 * This file contains a simple trajectory tracking framework for use
 * on Alice.  The controller is defined in a file that is loaded up at
 * run-time, so that students can design controllers and see how they
 * work. 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <getopt.h>
using namespace std;

#include "follow.h"		// definition of common variables

// Global variables
double inp[2*NUMINP];		// controller inputs (cur + ref)
double err[NUMINP];		// error between current and reference
double out[NUMOUT];		// controller outputs (acc, steer)
double xorigin = 3833759;	// Northing and Easting for Caltech
double yorigin = 487579;

FollowClient *client;		// skynet client for send/recv messages

// Sparrow displays - these should be included *after* global variables
#include "sparrow/display.h"
#include "maindisp.h"

// Command line options
char *short_options = "h";
static struct option long_options[] = {
  // first: long option (--option) string
  // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
  // third: if pointer, set variable to value of fourth argument
  //        if NULL, getopt_long returns fourth argument
  {"help",        0, NULL,               'h'},
};
char *usage_string = "\
Usage: follow [options]\n\
  -h, --help   print usage information\n\
";

int main(int argc, char **argv) {
  int ch, errflg = 0;

  // Print a welcome message to let everyone know we are alive
  cout << "Welcome to follow!" << endl;

  // Parse command line options
  while (!errflg && (ch = getopt_long(argc, argv, short_options, 
				      long_options, NULL)) != -1) {
    switch (ch) {
    case 'h':			// print options
      fprintf(stderr, usage_string);
      ++errflg;
      break;

    case '?':			// invalid option
      cerr << "Invalid option; use --help to list options\n";
      ++errflg;
    }
  }

  // Check to see if there were any errors
  if (errflg) { exit(1); }

  // Get the skynet key that we will be using
  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if(pSkynetkey == NULL) {
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  } else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  /*
   * Start up any threads that are required
   *
   * In this section, we start up the various threads that feed data 
   * to the follower.  These threads are each defined in separate
   * files that define the data structures that are used.
   *
   */

  // Set up skynet module
  client = new FollowClient(sn_key);

  // Threads for reading state and reference
  DGCstartMemberFunctionThread(client, &FollowClient::ReadState);
  DGCstartMemberFunctionThread(client, &FollowClient::ReadTraj);
  DGCstartMemberFunctionThread(client, &FollowClient::ControlLoop);

  // Startup the sparrow display
  if (dd_open() < 0) {
    cerr << "follow: can't initalize display\n";
    exit(1);
  }
  dd_usetbl(maindisp);		// start in the main display table
  dd_loop();			// start the display manager
  dd_close();			// close up the screen

  // Close off any threads that we have started

  return 0;
}
