/*
 * follow.h - header file for follow program
 *
 * RMM, 10 Dec 05
 *
 */

#include "SkynetContainer.h"	// skynet object
#include "StateClient.h"	// state client object
#include "TrajTalker.h"		// trajectory receiver

/* Origin for locating ourselves on the planet */
extern double xorigin, yorigin;

/* Declare the global input, output and reference variables */
#define NUMINP 9
#define NUMOUT 2
extern double inp[2*NUMINP], out[NUMOUT], err[NUMINP];
#define ref (inp+NUMINP)

/* Define offsets for states and reference values */
#define XPOS 0
#define YPOS 1
#define TPOS 2
#define XVEL 3
#define YVEL 4
#define TVEL 5

// Main skynet module
class FollowClient: public CStateClient, public CTrajTalker {
 public:
  FollowClient(int sn_key);
  void ReadState();
  void ReadTraj();
  void ControlLoop();
};
