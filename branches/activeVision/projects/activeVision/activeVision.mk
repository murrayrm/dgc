ACTIVEVISION_PATH = $(DGC)/projects/activeVision

ACTIVEVISION_DEPEND_LIBS =    $(SDSLIB) \
                        $(SKYNETLIB) \
                        $(SPARROWLIB) \
                        $(ALICELIB) \
                        $(MODULEHELPERSLIB) \
                        $(TIMBERLIB)

ACTIVEVISION_DEPEND_SOURCES = $(ACTIVEVISION_PATH)/activeVision.cc

ACTIVEVISION_DEPEND = $(ACTIVEVISION_DEPEND_LIBS) $(ACTIVEVISION_DEPEND_SOURCES)
