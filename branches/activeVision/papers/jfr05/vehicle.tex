% Master File: jfr05.tex
\section{Control System Design}
\label{sec:control}

The lowest level of the vehicle control system consists of the vehicle's
actuation system, its state sensing hardware and software, and the
trajectory tracking algorithms.  Like many teams in the Grand Challenge, these
systems were custom designed based on commercially available hardware.  This
section describes the design decisions and implementation of the control
system for Alice.

\subsection{Vehicle Actuation Systems}

There are five components critical to the control and operation of any
vehicle: steering, acceleration control (throttle), deceleration
control (braking), engine start and stop (ignition) and gear change
(transmission).  Two auxiliary components, emergency stop and on-board
diagnostics round out the control interfaces in use on Alice.  Team
Caltech developed actuators and interfaces to meet each of the
aforementioned needs.  Each actuator was designed with safety and
performance in mind.  To this end, in the case of electrical,
mechanical and or software failure, critical actuation subsystems
automatically bring the vehicle to a quick stop without needing
external commands.  To achieve the desired overall performance, each actuator 
is designed by specification to be able to react at least as fast as a human 
driver.

Each actuator uses its own RS-232 serial interface to 
communicate with the computing system.  The original specification required 
the ability to survive one or more computer failures which dictated 
that any actuator had to be available to at least two computers at 
any time.  This led to the use of an Ethernet enabled serial device server.  
However, through testing it was determined the mean time between
failures for the serial  
device server was shorter than other components in the actuator 
communication chain.  As a result the specification was dropped and 
direct serial connections were made to the computers.  

Each actuation subsystem is controlled through individual switches
accessible to a safety driver sitting in the driver's seat
(Figure~\ref{fig:actuation}(a)). 
\begin{figure}
  \centerline{\begin{tabular}{ccc}
    \includegraphics[width=0.4\hsize]{switches.eps} &&
    \includegraphics[width=0.45\hsize]{braking.eps} \\
    (a) &\qquad& (b)
  \end{tabular}}
  \caption{Vehicle actuation systems: 
    (a) dashboard controls and (b) pneumatic braking system.}
  \label{fig:actuation}
\end{figure}
This configuration allows a great deal of flexibility in testing,
including allowing the safety driver to control some functionality of
the system while the software controlled other functions.  The
dashboard controls are also used to allow the safety driver to take
over control of the vehicle during testing.

\paragraph{Steering Actuator}
The steering system used on Alice was originally developed for Bob and used
during the 2004 Grand Challenge.  It was adapted and improved for use in the
2005 competition and proved to be a highly reliable component.  The basis of
this system is a Parker-Hannefin 340 volt servo motor and a GV-6UE
controller interfaced to the computing cluster via RS-232.  This
servo-motor is capable of providing 8 Nm of torque while running and up to 14
Nm stall torque.  Attached to this motor is a 3:1 single stage planetary
gearbox which is attached to the steering column by means of steel chain
providing a further 1.8:1 reduction.  The result is a system capable of
providing a continuous torque of more than 40 Nm to the steering
column, which is sufficient to turn the wheels should the power
steering system of the 
vehicle fail.  The variable speed motor is tuned to allow the wheels to
traverse from one end of travel limit to the other in approximately 1.5
seconds, which is slightly faster than is possible for a human.  Higher speeds 
were tested, but despite the reduced delay, they were found to contribute to
an overly aggressive behavior in the vehicle control system, as well
as causing heavier 
wear on all components.

\paragraph{Brake Actuator}
Following Team Caltech's less than satisfactory experience with electrical
linear actuators for
braking in the 2004 Grand Challenge, a pneumatic actuation system was chosen
for use this year.  This choice provided a faster response while still 
providing enough force to bring the vehicle to a
complete stop.  The five piston mechanical portion of the actuator was
designed and implemented by Chris Pederson of competing team A.\ I.\
Motorvators.  As shown in Figure~\ref{fig:actuation}(b), the 
actuator consists of five pistons of incrementally increasing bore arrayed
in parallel with their piston rods acting in tension attached to a single
pivot point.  
A one-to-one ratio pivot positioned beneath the driver's seat
changes the retracting motion of the pistons to a compressive motion applied
to the brake pedal by means of a removable pushrod.  Four primary pistons
are used for articulate control of braking pressure during autonomous
operation.  Each piston may be applied and released independently by means
of 5 volt active solenoid control valves, each of which is in turn
controlled by solid-state relays linked to an Atmel AVR microprocessor,
interfaced to the control computer.  In case of electrical or air compressor
failure, the fifth cylinder is automatically deployed using a separate air
reserve.  The entire system runs on 70 psi compressed air provided by two
2.4 cfm air 12 volt air compressors.  Air is compressed, piped to a
reservoir, and then distributed to the brake and sensor cleaning systems.
A pressure switch located at the main manifold will close and deploy 
the reserve piston if the main air system pressure
falls below a safe level.

\paragraph{Throttle Actuation}
Caltech's 2005 E-350 van shipped with a 6.0L Powerstroke diesel
engine that is entirely electronically controlled, including control of
engine acceleration, referred to as ``throttle''.  The accelerator pedal in 
this vehicle is simply an electrical interface to the Powertrain Control Module 
(PCM), which is the Ford vehicle computer. Using specifications provided by
engineers from Ford, an interface was designed that closely
approximates the response of the accelerator pedal.  Using a microcontroller
and digital to analog converters, the accelerator pedal was replaced with an
effective interface that was controlled via RS-232.  The stock
pedal was left in the vehicle, and can be used by flipping a switch to
disable the aftermarket actuator.  Unfortunately, the throttle actuation
solution was not perfect.  The PCM requires that three sampled input lines
agree to within 2.1\% otherwise an error condition is declared.  In this case 
the PCM puts the vehicle into a mode called
``Limited Operating System'' which restricts the engine to idle.  The only 
method to
clear the LOS condition is to restart the engine, which requires ignition
actuation.  Despite these minor problems, the throttle actuation performed
admirably, exhibiting no measurable delay between command and actuation, as
well as a high degree of operational robustness.

\paragraph{Ignition and Transmission Actuation}
Ignition and transmission actuation, while two completely separate sub-systems
on the vehicle, were bundled together in one actuator controller out of
convenience, as neither required high communication bandwidth nor was
processing intensive for a microcontroller.  Ignition control was achieved
through the use of three 50 amp solid state relays to control three
circuits: powered in run, powered in start, or powered in run and start.  The 
ignition actuator as developed is tri-state: Off, Run or Start.
The vehicle protects the ignition system so that the starter motor cannot
operate if the engine is already running, and so that the engine cannot be
started unless the vehicle is in park or neutral. 

Transmission actuation was achieved
through the use of an electronic linear actuator connected to the
transmission by means of a push-pull cable.  A four position controller was
used to provide automated shifting into Park, Reverse, Neutral or
Drive.  The ignition and transmission can also be controlled by a human in
one of two ways.  Included in the actuator design is a control box that allows
the human driver to take control and operate the vehicle by means of a
push-button ignition system and a multi-position switch knob for
transmission control.  Alternatively each can be controlled
manually after disabling the actuators.

\paragraph {On Board Diagnostics}
To gather additional real-time data about the vehicle's performance,
Alice's CAN bus was accessed using a 
commercial OBD II reader.  Data values such as wheel speed, throttle
position, transmission position, and engine torque are gathered to provide
more information for contingency management and state estimation. However
the data had a large amount of latency due to poor interfaces, with overhead 
on the order of 10 times the data throughput.  To alleviate the
bandwidth limitations, 
asymmetric polling methods are implemented.  By prioritizing how often 
data fields are polled, performance was increased from 0.5 Hz updates to 
2 Hz for the time critical value of vehicle speed, which is still 
significantly less than the desired 10 Hz.  
Data rates for other fields have been decreased to as slow as once 
every 8 seconds for the lowest priority information.  

\paragraph {Emergency Stop}
Critical to the safety of operators and observers is a method to stop
the vehicle when it is autonomous.  DARPA provided the basis for this
remote safety system in the form of an ``E-Stop'' system consisting of
a transmitter-receiver pair
manufactured by Omnitech Robotics.  Using a 900 MHz transmitter, an
operator outside the vehicle and up to 11 miles (line of sight) away can
send one of three commands: RUN, PAUSE, or DISABLE.  RUN allows normal
operation of the vehicle, PAUSE brings the vehicle to a full and
complete stop with all components still functioning, and DISABLE
powers down the throttle, turns off the engine, and deploys full
braking pressure.  The receiver inside the vehicle is connected to a
microcontroller interfaced to the computing cluster.  This device also
takes input from several PAUSE and DISABLE switches located throughout
the vehicle, and then passes the most restrictive state to the vehicle
interface, called Adrive, which is described in the next section.
This system is built with many failsafes to ensure safe operation over
a variety of failure modes, including loss of power and communication.
The emergency stop system is designed so that even in the event of a
full computing system or failure, the vehicle can be remotely brought
to a safe stop.

\subsection{Vehicle Interface}

To bridge the transition from software to serial ports over Ethernet, a
layer of abstraction was added through a module called ``Adrive''.  The
primary role of this layer is to send the most recent command to each
actuator over RS-232 as well as read back the position
periodically.  Performance limitations to protect physical hardware are
also implemented in this module.  

The design specifications dictated less than 10 ms lag between Adrive
receiving any command and the associated actuator initiating response.
A secondary role was to provide actuator status and position feedback to 
the rest of the system.  
Later, system specifications were amended and the functional role of Adrive 
was expanded to include automatic execution of all actuator error
recovery procedures.  The E-Stop processing is also
handled within Adrive, such that a PAUSE command from any of the onboard
safety systems brings the vehicle to a rapid and safe stop.

All data related to the actuators is stored and managed in a single data 
structure within Adrive.  Adrive uses a multi-threaded design to allow multiple 
interrupt driven tasks to operate concurrently. This allows slow tasks, such as 
serial communications, to operate in parallel without blocking other tasks.  
Each actuator has two corresponding threads within Adrive, a status thread for
retrieving information from the actuator and a command thread for
executing commands.  There are two additional threads for
communications over the network with other modules.

As part of the specification to be fault-tolerant, Adrive runs a
supervisory thread that acts as a watchdog to ensure that every
actuator is operating properly.  If any actuator reports an error
condition, the supervisory thread will override the E-Stop status to
put the vehicle into a PAUSE state.  Once in PAUSE and the vehicle has
stopped, the actuator's supervisory thread will execute a sequence of
commands in an attempt to rectify the error condition.  An example of
this behavior is detecting an engine stall.  In this case the OBD II
interface will register an error that will generate a PAUSE.  The
supervisory thread of the OBD II driver will then detect that the OBD
II failed, and take the corrective action of trying to restart the
engine.  The OBD II status thread will keep trying to get status and
failing.  However once the engine is back on, OBD II will succeed and
set status to valid.  The next time the supervisory thread runs it
will detect that there are no longer any errors and return the vehicle
to the RUN condition.

The use of Adrive to abstract simultaneous communication between multiple 
software modules and multiple actuators worked well.  The computer resources
required are usually less than 1\% CPU usage and required less 4MB of
memory.  Response times range from 10 ms to 100 ms.  However the maximum 
bandwidth of approximately 10Hz is limited by
the serial communications.  The final structure proved to be a relatively
adaptable and scalable architecture to allow many asynchronous control
interfaces accessible to multiple processes across a network.

\subsection{State Sensing}
\label{sec:astate}

The estimate of vehicle state is made up of the vehicle's global
position (northing, easting, altitude), orientation (roll, pitch,
yaw), both the first and second derivatives of these values, as well
as the precise time-stamp for when this measurement was valid. The requirements 
and specifications of tolerances for planning, trajectory tracking and sensor 
fusion dictate how timely and accurate estimation of Alice's state
needs to be.  The main specification we believed necessary for ``safe
driving,'' was to be able to detect a 20 cm obstacle at 80 m.  Doing so
requires no more than 10 cm of relative error over 80 m of travel, and
orientation estimates accurate to within 0.001 radians (based on the
approximation $\arctan(0.1/80) \approx 0.001$).  

We chose to approach the state estimation problem with a combined
Inertial Measurement Unit (IMU) and Global Positioning System (GPS).
The output of the IMU (which returns changes in velocity and
orientation) was integrated according to the standard inertial
navigation equations.  Unfortunately, while the IMU provides smooth
relative position that is accurate over a short time-scale, errors in
the initial estimate and IMU biases and scale factors lead to a
solution that drifts from the true position.

Corrections to the errors in the inertial measurement are made with
measurements of the ``true'' position and velocity coming out of the
GPS unit and the velocity reported by the OBD II system.  A Kalman
filter is applied to an estimation of error values in position,
orientation and velocity.  These error values are calculated based
on the difference between the inertial solution and corresponding
measurements from the GPS unit.  In the case of OBD II velocity,
time-delays are such that only the stationary condition is used.
However, this does allow us to guarantee the vehicle is stationary
even in the absence of a GPS signal.  The error estimates are
propagated forward in time according to a linearization of the
equations of motion being integrated in the inertial solution.
Corrections are then applied by subtracting these errors out of the
inertial solution, effectively zeroing out the error values themselves.

An additional
constraint largely influencing the construction of the state estimator
was the fact that our maps are currently built in a global reference frame.  In
essence, the problem with this is that previous state errors are
cemented into the regions of the map where we are not getting new
updates and this must be taken into consideration when applying a
correction.  This has large ramifications on applying measurement
updates, since to update our state to the new correct location when we
get this information would create a jump discontinuity in the state
estimate, which in turn creates a sharp, spurious ridge in the map.
As such, there was a trade-off to be made between reporting the most
accurate state at a given time, and the most ``useful'' estimate of
state from the point of view of the other modules.

Application of zero-speed corrections, however, can lead to the previously
mentioned problem of jump-discontinuities.  We took two approaches to
solving this problem.  One solution involves a smoothing of the
applied corrections.  Essentially, rather than applying a correction
all at once, a fraction of the correction is applied at each time
step, leading to a smoothed out exponential decay in error rather than
a sharp jump.  For small jump-discontinuities this is sufficient to
avoid problems, as shown in Figure~\ref{fig:astate}(a).  
\begin{figure}
  \centerline{\begin{tabular}{ccc}
    \includegraphics[width=0.45\hsize]{statesmooth.eps} &&
    \includegraphics[width=0.4\hsize]{astate-jump.eps} \\
    (a) &\qquad& (b)
  \end{tabular}}
  \caption{Vehicle state estimate:
    (a) smoothing of small GPS signal and (b) sample GPS ``jump'': the
    trace shows the estimated vehicle state, with a correction while
    the vehicle is stopped (from second NQE run, just after the tunnel).}
  \label{fig:astate}
\end{figure}
However, occasionally, when new GPS satellites are
picked up or lost (such as when going through a tunnel, or under
power-lines), the GPS reading itself can jump by many meters.  

To approach this problem we pre-process the GPS data to determine
when it is ``safe'' to incorporate new measurements.  Essentially, as
long as the effective jump is below a threshold, GPS data can be
continuously applied without causing a problem.  If the GPS jumps by
a large enough amount (2 meters in the race configuration), we 
initially ignore it, assuming it is more likely a glitch in GPS.
However, if the confidence in the position falls below an empirically
determined threshold, we bring the vehicle to a pause and apply
the new corrections.  The vehicle is allowed to return to forward
motion when the state estimator indicates that the estimate has
converged, as determined by looking at the magnitude of the
differences between subsequent state estimates.  Because of the use of
a global representation for the cost map (and hence the implicit
location of obstacles), the elevation and cost maps for the system
are cleared at this point.  This ``jump'' functionality is implemented
through the use of a supervisory control strategy as described in
Section~\ref{sec:supercon}.  A sample state jump is shown in
Figure~\ref{fig:astate}(b). 

\subsection{Trajectory Tracking}

The design specification for the trajectory tracking algorithm is
to receive a trajectory from a planning module and output appropriate
actuator commands to keep Alice on this trajectory.  The inputs to the
algorithm are the current state of the vehicle (position and
orientation, along with first and second derivatives) and the desired
trajectory (specified in Northing and Easting coordinates, with their
first and second derivatives). From these inputs, the algorithm
outputs steering and brake/throttle commands to Adrive. Goals for
accuracy were +0/-10\% for velocity tracking, and $\pm$20 cm
perpendicular y-error at 5 m/s, with larger errors allowable at higher
speeds. These performance criteria needed to be met on any terrain
type found in the system specifications, at speeds up to 15 m/s.

\paragraph{System Characterization}
Before designing the controller, it was necessary to characterize the 
open-loop dynamics of the system. With this characterization a mapping
 from actuator positions to accelerations were obtained. They showed 
that Alice understeers, and allowed the determination of safe steering 
commands at various speeds, such that the vehicle would remain in the 
linear response region. In this region, the feedforward term will be
reasonable, and possibly dangerous roll angles/sliding are avoided.
Additionally, system delays were determined  by examination of the time 
between commands leaving this module and the resulting vehicular accelerations. 

\paragraph{Control Law Design}
Although not entirely independent, the lateral and longitudinal
controllers are treated separately in the system design. The lateral
control loop includes a feedforward term calculated from curvature of
the path along with a PID loop around a combined error
term. Longitudinal control is a PID loop around error in velocity
combined with a feedforward term based on current pitch.

The error for the lateral PID is a combination of heading and
perpendicular errors:  
\begin{displaymath}
  C_{\text{err}} = \alpha \tilde{Y}_{\text{err}} + 
    (1 - \alpha ) \beta \theta_{\text{err}},
\end{displaymath}
where $\tilde Y_{\text{err}}$ is the lateral position error (saturated at some 
maximum value), $\theta_{\text{err}}$ is the heading error and $\alpha$ is a 
scale factor.  This form was motivated by a desire to obtain stability at any 
distance from the path. Since this combined error term is being minimized there 
can be cases when the state heading variable is off (bad state) and there is a 
combined error minimum at some distance from the path, resulting in poor 
performance.

The lateral feedforward term is generated by determining the curvature required 
by the input trajectory, and applying the mapping for steering position to 
curvature, which yields
\begin{equation}
  \phi_{\text{FF}} = \arctan\left(L \frac{ \dot{N} \ddot{E} - \dot{E}
    \ddot{N}}{(\dot{N} + \dot{E})^\frac{3}{2}}\right),
\end{equation}
where $N$ is the northing position, $E$ is the easting position and
$L$ is the distance between front and rear wheels.  For this calculation, it is 
assumed that Alice behaves as described by the bicycle model~\cite{McR75-tsmc}.
% TODO: Add bicycle model equations.

For the longitudinal controller, we decided not to use a feedforward term 
associated with acceleration
based on the input trajectory. This was determined by experience, as there
were occasions where the feedforward term would overpower the feedback.
These issues were caused because this design was trying to control based on 
two different signals: velocity and acceleration. While they are related, 
and in theory tracking of one would result in perfect tracking of the other,
the uncertainties of the real system decreased the utility of feedforward 
accelerations, since the end goal was to maintain a specified velocity. 
Instead, the controller uses feedback around the velocity error combined
with a feedforward term  
estimating the additional throttle/brake needed based on current pitch.

To avoid oscillations, an integral reset was incorporated in both the
lateral and longitudinal controllers, when the relevant error was
below some acceptable threshold. In testing, the lateral integral term
rarely built up to any significant amount, since performance of the
system maintained average errors comparable to the threshold. For the
longitudinal controller, resetting helped to alleviate the overshoot
associated with transferring from hilly to flat ground.

To compensate for system delays, a lookahead term was added. This chose the 
point on the trajectory that lateral feedforward and longitudinal feedback would be 
computed from.

\paragraph{System Interfacing} Many of the difficulties in
implementing the tracking controller
were due not to the actual control algorithm but to interface
issues with the trajectory planner and the state estimation software.
The controller is relatively simple, but has to make certain
assumptions about the data and commands it is receiving from the other
modules. Working out these interfaces made the most difference in
performance. The trajectory tracking module (trajFollower) interfaced
with three other modules: the trajectory planner (plannerModule), the
state estimator (Astate) and the software that directly controlled the
actuators (Adrive).

Since the planner has no concept of the actual topography of the area,
a feasibility evaluator was implemented to check final commands
against vehicle state. This is broken down into two components: a
``bumpiness'' sensor (DBS) and a dynamic feasibility estimator (DFE).
The DBS algorithm analyzes the frequency components of the state
signal to determine how rough the road is, and adjusts the reference
velocity accordingly. In testing on dirt trails, this component choose speed
limits comparable to a human's judgment (within 1 m/s). The DFE
algorithm places hard limits on safe steering angles, dependent on our
speed. These limits were chosen based on where Alice began to
understeer.

In the final implementation, the quality of the state estimate provides the
most variability in trajectory following performance. Small jumps in state are
detrimental to stability, as they lead to an immediate jump in proportional
error, and thus steering command. More problematic are the intermittent 
difficulties with poor heading estimates, which lead to constant y-errors 
in tracking the trajectory.

The interface between the planner and trajectory following has to make
assumptions about how aggressive trajectories may be and what type of
continuity they possess.  Obviously, the sequential plans need to be
continuous for the controller to be stable. However, this leaves open
the question of what they are continuous with respect to. Initially
the plans originated from the current vehicle state, but this led to
oscillations due to the similarity of the rates between the two loops
(and the consequent phase delays in the loop transfer function).
Instead, the final race configuration used the planner to update plans
based on new terrain and information continuity was then established with
the initial portion of the previous trajectory, both in space and in
time.  One side effect of this approach is that it is possible for
situations to occur where the vehicle physically can not do what is
commanded. To handle these cases, it is necessary for the planner to
reset and replan from vehicle state whenever thresholds for position
or velocity errors are exceeded.

\paragraph{Tracking Performance} Figure~\ref{fig:tfresults} shows
representative results of the performance of the trajectory tracking
algorithms for longitudinal and lateral motion.
\begin{figure}
  \centerline{\begin{tabular}{ccc}
    \includegraphics[width=0.45\hsize]{tflongitudinal.eps} &&
    \includegraphics[width=0.45\hsize]{tflateral.eps} \\
    (a) &\qquad& (b)
  \end{tabular}}
  \caption{Trajectory tracking results: (a) longitudinal control and
  (b) lateral control. The resetting of the error at the end of the
    trajectory is due to a jump in the GPS position, as described in
    Section~\ref{sec:astate}.}
  \label{fig:tfresults}
\end{figure}

The algorithm was tested extensively prior to the
qualification event and demonstrated excellent performance in testing
and during the multiple qualifying event runs.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "jfr05"
%%% End: 
