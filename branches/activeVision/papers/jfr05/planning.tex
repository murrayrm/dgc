% Master File: jfr05.tex
\section{Navigation and Planning}
\label{sec:planning}

The navigation system for Alice was designed as two discrete
components: a planner that generates trajectories for the vehicle to
follow, and a controller that follows those trajectories. The
controller was described in Section~\ref{sec:control}; this section
will describe the design and implementation of the trajectory planner.

\subsection{Planner Approach}
The top level design goal of the planning system is to be able to traverse the
DARPA-specified course while satisfying DARPA's speed limits and avoiding
obstacles. An ideal planner will produce trajectories that are ``best'', in some
sense. Thus a numerical optimization method lies at the core of Alice's planning
system. Since the DGC race is over 100 miles long, it is both computationally
prohibitive and unnecessary to plan the vehicle's trajectory to the finish
line. An appropriate solution to deal with this problem is to run the planner in
a receding horizon framework. A receding horizon scenario is one where plans are
computed not to the final goal, but to a point a set horizon (spatial or
temporal) ahead on some rough estimate of the path towards the goal
(Figure~\ref{recedinghorizon}).

\begin{figure}
\begin{center}
\input{recedinghorizon.pstex_t}
\end{center}
\caption{An illustration of the receding horizon framework. A very inaccurate
(but easily computable) path is used as the global plan. Here this is a spline
based on the trackline. The solver that produces a higher-quality plan performs
its computation to reach a point on the lower quality path, some distance
ahead. In this figure, that distance (the horizon) is set to the range of the
sensors of the vehicle.}
\label{recedinghorizon}
\end{figure}

The vehicle model used by the planning system is a simple kinematic,
rear-centered bicycle model:
\begin{equation}
\label{modeleqn}
\begin{matrix}
\dot N &=& v \cos \theta \\
\dot E &=& v \sin \theta \\
\dot v &=& a \\
\dot \theta &=& \frac{v}{L} \tan \phi
\end{matrix}
\end{equation}
where $N$, $E$ are Cartesian spatial coordinates, $\theta$ is yaw (measured from
North to East), $v$ is the scalar speed, $a$ is the vehicle
acceleration, $L$ is the vehicle wheelbase and
$\phi$ is the steering angle.

To apply a numerical optimization scheme to the path planning problem, we have
to represent the space of all potential trajectories as a vector space. To cut
down on the dimensionality of this vector space, we represent the spatial part
of the trajectory as a quadratic spline of $\theta(s)$ where $s$ is length along
the trajectory normalized to the total length of the trajectory, $S_f$, and the
temporal part as a linear spline of $v(s)$.  With this representation, we can
represent all variables in the system model~(\ref{modeleqn}) as functions of
derivatives of $\theta(s)$, $v(s)$, $S_f$:
\begin{equation}
  \label{flat}
  \aligned 
    N(s) &= N_0 + S_f \int_0^s \cos(\theta(s)) ds \\
    E(s) &= E_0 + S_f \int_0^s \sin(\theta(s)) ds \\
    \dot N(s) &= S_f \cos(\theta(s)) v(s) \\
    \dot E(s) &= S_f \sin(\theta(s)) v(s) \\
  \endaligned \qquad\qquad
  \aligned 
    a &= \frac{v}{S_f} \frac{dv}{ds} \\
    \dot \theta &= \frac{v}{S_f} \frac{d \theta}{ds} \\
    \tan \phi &= \frac{L}{S_f} \frac{d \theta}{ds} \\
    \dot \phi &= \frac{L S_f \frac{d^2 \theta}{d s^2}}
      {S_f^2 + \left(L \frac{d\theta}{ds}\right)^2} \\
  \endaligned
\end{equation}

To pose the planning problem as an optimization problem, we need an objective to
optimize and constraints to satisfy, all functions of some state vector. Our
state is $S_f$ and the spline coefficients of $\theta(s)$ and $v(s)$. We try to
find trajectories that are fastest, while keeping the steering and acceleration
control effort low. Thus our objective function is
\begin{equation}
  J = S_f \int_0^1 \frac{1}{v(s)} ds
    + k_1\left\|\dot \phi(s)\right\|_2^2 + k_2\left\|a(s)\right\|_2^2
\end{equation}

Besides the initial and end condition constraints, we also satisfy dynamic
feasibility constraints:
\begin{equation}
  \label{dynamicfeasibilityconstraints}
  \begin{array}{lccccc}
    \mathrm{Speed\ limit} &&& v &<& v_{\mathrm{limit}} \\
    \mathrm{Acceleration\ limit} & a_{\mathrm{min}} 
      &<& a &<& a_{\mathrm{max}} \\
    \mathrm{Steering\ limit} & -\phi_{\mathrm{max}} 
      &<& \phi &<& \phi_{\mathrm{max}} \\
    \mathrm{Steering\ speed\ limit} & -\dot \phi_{\mathrm{max}} 
      &<& \dot \phi &<& \dot \phi_{\mathrm{max}} \\
    \mathrm{Rollover\ constraint} & -\frac{g W}{2 h_\mathrm{cg}} 
      &<& v^2 \frac{\tan \phi}{L}&<& \frac{g W}{2 h_\mathrm{cg}}
  \end{array}
\end{equation}
In the rollover constraint expression, $W$ is the track of the vehicle (distance
between left and right wheels), $h_\mathrm{cg}$ is the height of the center of
gravity of the vehicle above ground, and $g$ is the acceleration due to
gravity. This expression is derived from assuming flat ground and rollover due
purely to a centripetal force. In reality, on many surfaces, sideslip will occur
much before rollover so this constraint has an adjustment factor.

Obstacle avoidance enters into the problem as the speed limit constraint, with
the RDDF and terrain data processed to produce a combined, discrete map that
represents a spatially dependent speed limit. What this means is that the areas
outside of the RDDF and those that lie inside obstacles have a very low speed
limit, and thus any trajectory that goes through those areas is not explicitly
infeasible, but \emph{is} suboptimal. This representation allows the obstacle
avoidance and quick traversal conditions to be cleanly combined.

The processing of $v_{\mathrm{limit}}$ from the discrete map generated from the
sensors as described in Section \ref{sec:terrain}, to a $C_1$ surface was a
nontrivial matter. A lot of thought went into the design of this component, but
its details are beyond the scope of this paper.  More information is
available in~\cite{Kog05-plans}.

Since the numerical optimizer is only locally optimal, and the problem is only
locally convex, choosing a good seed for the solver is an important prerequisite
to a successful solution cycle. The seeding algorithm used for Alice's planner
consists of a coarse spatial path selector, planning several times beyond
Alice's stopping distance. This approach evaluates quickly, introduces a needed
element of long-term planning, and works well to select a good local basin of
attraction.

\subsection{System interfacing}
During the development of the planning system the challenges were not limited to
the planning system itself: a lot of work went towards interfacing the planner
with the other system components.

One issue that needed to be addressed was the interaction of the controller and
the planner, and the location of the feedback loop. If a feedback loop is
contained in both the controller (through tracking error feedback) and the
planner (by planning from the vehicle state), these two feedback loops interact
unfavorably and produce spurious oscillations in the closed loop system
performance. Since these two feedback loops work on the same time scale, these
interactions are natural, and one of the feedback loops has to be eliminated to
address the problem. The controller feedback can be eliminated by using only
feedforward control input. This creates a very clean system, but requires
relatively quick replanning, and a high fidelity model for the trajectory
generation. Since there are no theoretical guarantees for the numerical
optimizer convergence, this option was rejected in favor of moving all of the
feedback into the controller: controller feeds back on its tracking errors, and
the planner plans from a point on its previously computed plan. In this way, the
vehicle motion does not directly affect the planner's operation, and the
oscillations are eliminated. This can result in a potential issue of runaway
poor tracking. To address this concern, the planner {\em does} plan from the
vehicle's state if the tracking errors grow beyond some predefined threshold.

An issue observed during the testing of the planner was an indecisiveness about
which way to go to avoid a small obstacle. The local optimality of the planner
could repeatedly change the avoidance direction between planning cycles,
resulting in an ungraceful, late swerve to avoid an obstacle that has been
detected long before. Since the seeding algorithm chooses the basin of
attraction, the fix was placed there. By adding a slight element of hysteresis
to the seeding algorithm to lightly favor solutions close to the previous
solution, the balance between the two avoidance directions was broken, and a
consistent direction was favored.

A third interaction challenge that was discovered during the planner
development was the treatment of no-data cells in the map. This issue
was briefly mentioned in Section \ref{sec:terrain}, but the
planning-side issues are described here. While all map cells that the
planner gets from the mapping components represent a specific speed
limit that the planner should satisfy, this is not as clear for
no-data cells. To avoid being overly cautious, or overly brazen around
no-data, a dual interpretation of no-data was adopted. First, a
threshold distance is computed as the larger of twice the stopping
distance or 10m. Then, any no-data cell closer than the threshold
distance to the vehicle is treated as a very slow cell, while no-data
beyond the threshold distance is treated as twice-the-current-speed.
This representation attempts to avoid or slow down through no-data
cells where they could be dangerous (at the vehicle), but does not let
no-data negatively influence long-term navigation decisions.

\subsection{Planner results}
With these issues taken care of, the planning problem was solved by an
off-the-shelf numerical optimizer, SNOPT. This optimization-based
approach provides a very clean planning solution, and avoids most
heuristic aspects of the planners that were so ubiquitous in the
planning systems of most other teams. Optimizer convergence speed is a
potential issue, but on Alice's 2.2 GHz Opteron, an average rate of
4.28 plans/second was achieved during the race. Further, this system
produces very drivable and precise trajectories, which allows for very
tight obstacle avoidance in potentially difficult situations (more
than sufficient for the DGC). Some planner performance from the
National Qualifying Event is illustrated in Figure
\ref{plannerresults}.
\begin{figure}[p!]
  \begin{center}
    \begin{tabular}{cc}
    \input{plots86.pstex_t} & \input{plots90.pstex_t} \\
    \hline \\
    \input{plots92.pstex_t} & \input{plots96.pstex_t} 
    \end{tabular}
  \end{center}
  \caption{Non-consecutive planner iterations showing Alice avoid a
  car at the NQE. The vehicle is traveling East. The planner seed is
  green and the final solution blue. Grayscale colors are the speed
  limits in the map, with red representing nodata. Cars south and
  straight ahead of the vehicle are visible, along with the sensor
  shadow of nodata for the vehicle straight ahead. We can see distant
  no-data being treated favorably.  We can also see a change of plans
  to avoid the newly detected second car.}
  \label{plannerresults}
  \afterpage{\clearpage}
\end{figure}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "jfr05"
%%% End: 
