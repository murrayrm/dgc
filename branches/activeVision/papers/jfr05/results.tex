% Master File: jfr05.tex

\section{Experimental Results}
\label{sec:results}
In the previous six sections we have described in detail Team
Caltech's approach to designing its unmanned ground vehicle, Alice.
While Alice shares many subsystems in common with previous autonomous
vehicles as well as other Grand Challenge entrants, four unique
characteristics set Alice apart.

The first such characteristic is its sensor coverage: Alice relied
heavily on its disparate combination of sensors to generate
information about its environment, making use of all available data
% except the bumper LADAR :)
when determining the locations of obstacles.  In particular, Alice only
``forgot'' prior map data when sufficiently new map data arrived,
regardless of the accuracy of existing measurements, allowing it to
``drive blind'' for as long as necessary.

A second unique characteristic was the use of speed-limit maps to
encode information about both the traversability and difficulty of the
terrain.  This approach allowed Alice to very precisely navigate
through areas that were complex both in terms of how the vehicle
should navigate spatially (e.g. field of obstacles, through which a
precise course must be chosen to prevent collisions) as well as how
the vehicle should navigate temporally (e.g. rocky or bumpy areas,
through which precise speed-limits should be followed to prevent
damage to the vehicle or instability).

The third characteristic that sets Alice apart from most other Grand
Challenge entrants is the fact that its software architecture makes
absolutely no assumptions about the structure of the race course as
defined by the RDDF.  In particular, Alice makes no use of a priori
map data, thereby enhancing its flexibility; it would be just as able
to drive a desert road in a foreign country as it would in the Mojave
desert.  Additionally it does not assume that the RDDF will ``hold
its hand,'' so to speak, by constraining it to a road, or forcing it
to decelerate around turns or in rough terrain.  In fact, a great
deal of testing was done in which the corridor's width was set very
high (on the order of dozens of meters) and the speed limit was chosen
to be essentially infinity.  Despite the open-endedness of this
problem, Alice's architecture allowed it to consistently choose an
optimal course and speed.

The fourth and final feature that set Alice apart from many other GCE
entrants was its physical robustness.  While there were several other
entrants who would likely have been at least as physically capable as Alice in 
a true off-road environment, Alice's tough physical exterior enabled it to
take risks that may have damaged some vehicles catastrophically.  

These four characteristics have contributed to making Alice a
robust and extraordinarily capable autonomous vehicle.  In fact, autonomous 
operation of Alice has been demonstrated in a wide variety of situations,
including open desert, parking lots, rainy and wet conditions, dirt
roads of varying degrees of roughness, rolling hills, winding roads
and mountain passes.  The following sections describe the nature and
results of experiments on and testing of Alice leading up to the
National Qualifying Event, for the National Qualifying Event itself,
and in the Grand Challenge Event.

\subsection{Desert Testing}

Team Caltech documented over 300 miles of fully autonomous desert
driving with Alice from June 2005 to the National Qualifying Event in
Fontana in September, all in the Mojave Desert.

Approximately 33 of these miles were driven on the 2004 Grand
Challenge race route during the week of June 13th, 2005.  Alice
traversed these miles with a testing team of four people inside,
scrutinizing its performance and making software improvements and
corrections.  Over the course of three days, Alice suffered three flat
tires including a debilitating crash into a short rocky wall that blew
out the inside of its front left tire and split its rim into two
pieces.  This crash was determined to be caused primarily by a lack of
accurate altitude estimates when cresting large hills.  Along with
several related bug fixes, an improved capability to estimate
elevation was added to the state estimator.

The majority (approximately 169 miles) of autonomous operation for
Alice took place in the two weeks leading into the National Qualifying
Event.  This operation included a full traverse of Daggett Ridge at 4
m/s average speed, and significant operation in hilly and mountainous
terrain. The top speed attained over all autonomous operations was 35
mph.  The longest uninterrupted autonomous run was approximately 25
miles.

\subsection{National Qualifying Event}

As one of 43 teams at the California Speedway in Fontana, Alice
successfully completed three of its four qualifying runs.  Several of
its runs were characterized by frequent stopping as a result of the
performance of the state estimator under conditions of intermittent
GPS.  Specifically, under degraded GPS conditions its state estimate
would drift considerably, partially due to miscalibration of IMU
angular (especially yaw) biases and partially due to lack of odometry
inputs to the state estimator.  However, zero-speed corrections were
applied to the Kalman filter when Alice was stopped, which served to
correct errors in its state estimate due to drift quite well.

During Alice's first NQE run, zero-speed corrections were not applied
in the state estimator.  Accordingly, drift accumulating in the state
estimator was not corrected adequately when Alice stopped.  Travel
through the man-made tunnel produced drift substantial enough for
Alice's estimate to be outside the RDDF, which at the time resulted in
a reverse action.  Alice performed a series of reverse actions in this
state, going outside the actual corridor, and resulting in DARPA pause
and the end of its first run.

Zero-speed corrections were added to the state estimator after Alice's
first run, enabling it to successfully complete all subsequent runs,
clearing all obstacles and 137 out of a total 150 gates.  Completion
times were slow for runs 2, 3 and 4 partially due to frequent stopping as a
result of state estimator corrections.  Figure \ref{fig:alicefontana}
provides a summary of Alice's NQE run performances.
\begin{figure}
  \centerline{
    \includegraphics[width=0.287\textwidth]{alice-nqe.eps}
    \quad\small
    \begin{tabular}[b]{|c|c|c|c|p{4cm}|}
    \hline
    Run & Gates & Obst & Time & Issues \\
    \hline
    1 & 21/50 & 0/4 & DNF & State estimate problems after tunnel \\
    \hline
    2 & 44/50 & 4/4 & 12:45 & \\
    \hline
    3 & 49/50 & 5/5 & 16:21 & Multiple PAUSEs due to short in E-Stop
    wiring \\
    \hline
    4 & 44/50 & 5/5 & 16:59 & Frequent stops due to state drift \\
    \hline
    \end{tabular}
  }
  \caption{Alice on the National Qualifying Event course (left).  Table of results from Alice's four runs at the NQE (right).}
  \label{fig:alicefontana}
\end{figure}

Alice was one of only eleven teams to complete at least three of four
NQE runs.  As a result of Alice's performance at the NQE, it was
preselected as one of ten teams to qualify for a position on the
starting line in Primm, Nevada.

\subsection{Grand Challenge Event}

When Alice left the starting line on October 8th, 2005, all of its
systems were functioning properly.  However, a series of failures
caused it to drive off course, topple a concrete barrier and 
disqualify itself from the race as a result.  Although as mentioned above, the
system we have described performed well over the course of hundreds of
miles of testing in the desert prior to the Grand Challenge we believe
the pathological nature of this particular failure scenario
demonstrates a few of the more important weaknesses of the system and
exemplifies the need for further ongoing research.  We will begin by
providing a brief chronological timeline of the events of the race
leading up to Alice's failure, followed by an analysis of what
weaknesses contributed to the failure.

Alice's timeline of events in the Grand Challenge is as follows:
\begin{itemize}

\item Zero minutes into the race, Alice leaves the starting line with
all systems functioning normally

\item Approximately four minutes into the race, two of its midrange
LADARs enter an error mode from which they cannot recover, despited
repeated attempts by the software to reset.  Alice continues driving
using its long and short-range sensors.

\item Approximately 30 minutes into the race, Alice passes under a set
of high-voltage power lines.  Signals from the power lines interfere
with its ability to receive GPS signals, and its state estimate begins
to rely heavily on data from its Inertial Measurement Unit (IMU).

\item Approximately 31 minutes into the race, Alice approaches a
section of the course lined by concrete barriers.  Because new GPS
measurements are far from its current state estimate, the state
estimator requests and is granted a stop from supervisory control to
correct approximately 4 meters of state drift.  This is done and the 
map is cleared to prevent blurred obstacles from remaining.

\item GPS measurements report large signal errors and the state
estimator consequently converges very slowly, mistakenly determining
that the state has converged after a few seconds.  With the state
estimate in an unconverged state, Alice proceeds forward.  
\begin{figure}
  \centerline{
    \includegraphics[width=0.45\textwidth]{yawcrash.eps}
    \includegraphics[width=0.55\hsize]{sc-gce-crash.eps}
  }
  \caption{Alice's estimated heading and yaw, both measured clockwise
  from north, in its final moments of the GCE.  Yaw is from the state
  estimator and heading is computed directly as the arctangent of the
  easting and northing speeds of the rear axle.  The times at which
  the state estimator requests a vehicle pause (A), the map is cleared
  and pause is released (B), Alice speeds up after clearing a region
  of no data (C) and impact (D) are shown.  Between A and B the
  direction of motion is noisy as expected as Alice is stopped.
  Between B and D the state estimate is converged around 180 degree
  yaw, which we know to be about 8 degrees off leading into the
  crash.}  
  \label{fig:gce_yaw}
\end{figure}

\item A considerable Eastward drift of the state estimate results from a low 
confidence placed on the GPS measurements.  This causes our velocity vector and 
yawangle to converge to values that are a few degrees away from their true 
values.  Based on the placement of the North-South aligned row of K-rails in 
the map by the short-range LADAR (see Figure \ref{fig:gce_gui}(a)), Alice's 
actual average yaw for the 5 or so seconds leading into the crash -- between 
times C and D on Figure \ref{fig:gce_yaw} -- appears to be about 8 degrees west 
of south (-172 degrees).  For the same period, our average estimated yaw was 
about -174 degrees and our average heading (from ndot, edot) was about -178 
degrees.
Roughly, as Alice drives south-southwest, its state estimate says it is
driving due south, straight down the race corridor 
(Figure~\ref{fig:gce_gui}(a)).

\begin{figure}
  \centerline{\includegraphics[width=1.0\textwidth]{alicecrashgui.eps}}
  \caption{Alice's speed limit maps over the last few seconds of the
  race, with notation added.  For all three diagrams, the brown area
  is the road, the light blue vertically-oriented rectangular areas
  are the concrete barriers, the blue box is Alice, and the pink line
  is its perceived yaw (direction of travel). The color-bar on the
  right indicates the speed limit that was assigned to a given cell,
  where brown is highest and blue is lowest.  The leftmost diagram
  indicates Alice's expected yaw and Alice's actual yaw during the
  last few seconds of the race.  The center diagram is Alice's speed
  limit map less than one second before it crashed, indicating the
  differing placement of the obstacles by the short and long-range
  sensors -- indicated as two parallel lines of
  concrete barriers (blue rectangles). In fact, there was only a
  single line of them, and that line went directly north-south, not
  angled as Alice perceived.  The rightmost diagram is Alice's final
  estimate of its location: accurate, but thirty seconds too late.}
  \label{fig:gce_gui}
\end{figure}

\item Alice's long-range sensors detect the concrete barriers and
place them improperly in the map due to the error in state estimate.
Alice's mid-range sensors are still in an error mode.  Alice picks up
speed and is now driving at 10-15mph.

\item At 32 minutes, because it is not driving where it thinks it is,
Alice crashes into a concrete barrier.  Its short-range sensors detect
the barrier, but not until it is virtually on top of it
(Figure~\ref{fig:gce_gui}(b) and Figure~\ref{fig:crash}).
\begin{figure}
  \centerline{
    \includegraphics[width=0.8\textwidth]{alice-crash.eps}
  }
  \caption{Alice as it ``clears'' a concrete barrier during the GCE.}
  \label{fig:crash}
\end{figure}

\item Almost simultaneously, DARPA gives an E-Stop pause, the front
right wheel collides with the barrier, the power steering gearbox is
damaged, and the driving software detects this and executes its own
pause also, independent of that from DARPA.  The strong front bumper
prevents Alice from suffering any major damage as it drives over the
barrier.

\item Once Alice has come to a stop, the state-estimation software
once again attempts to re-converge to get a more accurate state
estimate---this time it corrects about 9.5 meters and converges close to the
correct location, outside the race corridor.  Alice is subsequently
issued an E-Stop DISABLE (Figure~\ref{fig:gce_gui}(c)).

\end{itemize}
Figure~\ref{fig:gce_yaw}(b) shows the superCon state during this final
segment, including a Slow Advance through some spurious obstacles, a brief
Lone Ranger push to clear them, the GPS reacquisition while stopped, and the
final Slow Advance only after Alice has picked up speed and is near to 
crashing.

It is clear that while Alice's ultimate demise was rooted in its
incorrect state estimates (due to poor GPS signals), other factors
also contributed to its failure, or could conceivably have prevented it.
These include the mid-range LADAR sensor failures, the lack of a system-level 
response to such failures, and the high speeds assigned to long range sensor 
data even in the face of state uncertainty.  Additionally, in race 
configuration the forward facing bumper LADAR sensor was only used to assist in 
detection of the boundaries of the roads for the road following module.  This 
could have helped in assigning appropriate speeds in the map for the row of K-rails.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "jfr05"
%%% End: 
