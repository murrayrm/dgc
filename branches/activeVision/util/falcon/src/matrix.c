/*
 * matrix.c - general purpose matrix routines
 *
 * version 0.50b, August 1995
 *
 * Copyright 1995, Michael Kantner, kantner@hot.caltech.edu
 *
 * Do not distribute without the consent of the author
 * No warranties are implied.  Use at your own risk.
 */

#include <stdio.h>
#include <string.h>

#ifdef MSDOS
#include <alloc.h>
#else
#include <malloc.h>
#endif

#include "matrix.h"

MATRIX  *mat_init(int nrows, int ncols) {
  MATRIX *handle;

  handle = mat_create();

  if (mat_resize(handle,nrows,ncols)<0){
#ifdef MAT_DEBUG
    printf("  Matrix initial sizing error!\n");
#endif
    mat_free(handle);
    return (MATRIX *)0;
  }

  mat_reset(handle);

  return handle;
}

MATRIX *mat_create(){
  MATRIX *handle;

  handle = (MATRIX *)malloc(sizeof(MATRIX));

  if (handle==(MATRIX *)0){
#ifdef MAT_DEBUG
    printf(" mat_create error!\n");
#endif
    return (MATRIX *)0;
  }

  handle->type = 0;
  handle->nrows = 0;
  handle->ncols = 0;
  handle->imagf = 0;
  
  handle->real = (double *)0;
  handle->imag = (double *)0;
  handle->prev = (MATRIX *)0;
  handle->next = (MATRIX *)0;

  handle->namelen = 7;
  strcpy(handle->name,"NONAME");

  return handle;
}


int mat_set_name(MATRIX *a, char *name){

  if (a==(MATRIX *)0) return -1;
  
  strcpy(a->name,name);
  a->namelen=strlen(name)+1;
  return 0;
}

char * mat_name(MATRIX *a){
  return a->name;
}

char * mat_get_name(MATRIX *a){
  return mat_name(a);
}

void mat_reset(MATRIX *a) {
  int i;

  /* Initialize to all elements to 0 */
  for (i=0; i < a->nrows*a->ncols; i++) *(a->real+i)=0.0;
}

int mat_resize(MATRIX *mx, int nrows, int ncols) {

  double *r;

  if (mx == (MATRIX *)0) return -1;

  if (mx->real != (double *)0)
    free(mx->real);
  r = (double *)malloc(nrows*ncols*sizeof(double));
  if (r == (double *)0) return -1;

  mx->real = r;
  mx->nrows = nrows;
  mx->ncols = ncols;
  return 0;
}
  

double mat_element_get(MATRIX *mx, int row, int col) {

  double entry = 0; /* default value */

  if (mx != (MATRIX *)0) 
    if ( (row < mx->nrows) && (col < mx->ncols))
      entry = mat_element_get_f(mx,row,col);

  return entry;
}

double mat_element_get_f(MATRIX *mx, int row, int col) {
  return *(mx->real + col*mx->nrows + row);
}

double *mat_get_real(MATRIX *mat) {

  if (mat==(MATRIX *)0) return (double *)0;
  return mat->real;
}

double *mat_get_imaginary(MATRIX *mat) {

  if (mat==(MATRIX *)0) return (double *)0;
  return mat->imag;
}

int mat_element_set(MATRIX *mx, int row, int col, double value) {

  if (mx == (MATRIX *)0) return -1;
  if ( (row >= mx->nrows) || (col >= mx->ncols) ) return -1;

  mat_element_set_f(mx,row,col,value);

  return 0;
}

void mat_element_set_f(MATRIX *mx, int row, int col, double value) {
  *(mx->real + col*mx->nrows + row) = value;
}


void mat_print(MATRIX *a) {
  if (a == (MATRIX *)0) 
    printf("Matrix is NULL\n");
  else {
    int i, j;
    
    printf("Matrix %s: %d rows, %d cols\n", a->name, a->nrows, a->ncols);
    for (i = 0; i < a->nrows; i++) {
      for (j = 0; j < (a->ncols); j++)
	printf("%8.3f ", *(a->real + i +j*a->nrows));
      printf("\n");
    }
  }
}


int mat_transpose(MATRIX *at, MATRIX *a) { /* at = transpose(a) */
  MATRIX *a1;
  int freea1=0;

  if ((a == (MATRIX *)0) || (at == (MATRIX *)0)) return -1;

  if (at == a){
    a1 = mat_create();
    mat_copy(a1,a);
    freea1=1;
  }
  else
    a1=a;
 
  mat_resize(at,a1->ncols,a1->nrows);

  mat_transpose_f(at,a1);

  if (freea1) mat_free(a1);

  return 0;
}

void mat_transpose_f(MATRIX *at, MATRIX *a) { /* at = transpose(a) */

  register int i,j;

  for (i = 0; i < a->nrows; i++)
    for (j = 0; j < a->ncols; j++)
      *(at->real + j + i*at->nrows) = *(a->real + i + j*a->nrows);
}

int mat_list_free(MATRIX *a){
  return mat_free_list(a);
}

int mat_free_list(MATRIX *a) {

  MATRIX *next;
  int i = 0;

  while(a != (MATRIX *)0){
    i++;
    next = a->next;
    mat_free(a);
    a = next;
  }

  return i;
}

void mat_free(MATRIX *a) {

  if (a==(MATRIX *)0) return;

  /* Clear a from any list */
  if (a->prev != (MATRIX *)0) a->prev->next = a->next;
  if (a->next != (MATRIX *)0) a->next->prev = a->prev;

  if (a->real != (double *)0)
    free(a->real);
  if (a->imag != (double *)0)
    free(a->imag);
  free(a);

}


int mat_copy(MATRIX *dst, MATRIX *src) { /* dst = src */
  if ((src == (MATRIX *)0) || (dst == (MATRIX *)0)) return -1;
  if (src==dst) return 1;

  mat_resize(dst, src->nrows, src->ncols);

  mat_copy_f(dst,src);

  return 0;
}

void mat_copy_f(MATRIX *dst, MATRIX *src){ /* dst = src */

  register int i;

  for (i = 0; i < src->nrows*src->ncols; i++)
      *(dst->real + i) = *(src->real + i);
}



int mat_dotmult(MATRIX *dst, MATRIX *a, MATRIX *b) { /* dst = a .* b */
  MATRIX *a1, *b1;
  int freea1=0;
  int freeb1=0;


  if ((dst == (MATRIX *)0) || (a == (MATRIX *)0) || (b == (MATRIX *)0)) return -1;
  if ((b->nrows != a->nrows) || (b->ncols != a->ncols)) return -1;

  if (dst == a){
    a1 = mat_create();
    mat_copy(a1,a);
    freea1 = 1;
  }
  else
    a1 = a;

  if (dst == b){
    b1 = mat_create();
    mat_copy(b1,b);
    freeb1 = 1;
  }
  else
    b1 = b;

  mat_resize(dst,a1->nrows,a1->ncols);

  mat_dotmult_f(dst,a1,b1);

  if (freea1) mat_free(a1);
  if (freeb1) mat_free(b1);
  
  return 0;
}

void mat_dotmult_f(MATRIX *dst, MATRIX *a, MATRIX *b){ /* dst = a .* b */
  
  register int i;

  for (i = 0; i < dst->nrows*dst->ncols; i++)
    *(dst->real + i) = *(a->real + i) * *(b->real + i);
}


int mat_scale(MATRIX *dst, MATRIX *a, double scale) { /* dst = scale*a */
  MATRIX *a1;
  int freea1=0;

  if ((dst == (MATRIX *)0) || (a == (MATRIX *)0)) return -1;

  if (dst == a){
    a1 = mat_create();
    mat_copy(a1,a);
    freea1 = 1;
  }
  else
    a1 = a;

  mat_resize(dst,a1->nrows,a1->ncols);

  mat_scale_f(dst,a1,scale);

  if (freea1) mat_free(a1);
  
  return 0;
}

void mat_scale_f(MATRIX *dst, MATRIX *a, double scale){ /* dst = scale*a*/
  
  register int i;

  for (i = 0; i < dst->nrows*dst->ncols; i++)
    *(dst->real + i) = *(a->real + i)*scale;
}

int mat_offset(MATRIX *dst, MATRIX *a, double offset) { /* dst = offset+a */
  MATRIX *a1;
  int freea1=0;

  if ((dst == (MATRIX *)0) || (a == (MATRIX *)0)) return -1;

  if (dst == a){
    a1 = mat_create();
    mat_copy(a1,a);
    freea1 = 1;
  }
  else
    a1 = a;

  mat_resize(dst,a1->nrows,a1->ncols);

  mat_offset_f(dst,a1,offset);

  if (freea1) mat_free(a1);
  
  return 0;
}

void mat_offset_f(MATRIX *dst, MATRIX *a, double offset){ /* dst = offset+a*/
  
  register int i;

  for (i = 0; i < dst->nrows*dst->ncols; i++)
    *(dst->real + i) = *(a->real + i)+offset;
}


int mat_rows(MATRIX *a){

  if (a==(MATRIX *)0) return -1;
  return a->nrows;
}

int mat_get_rows(MATRIX *a){
  return mat_rows(a);
}

int mat_columns(MATRIX *a){
  if (a==(MATRIX *)0) return -1;
  return a->ncols;
}

int mat_get_cols(MATRIX *a){
  return mat_columns(a);
}

/* Find a matrix and return a pointer to the matrix data structure */
MATRIX *mat_find(MATRIX *list, char *name)
{
    /* Do a bit of error checking */
    if (name == NULL) return (MATRIX *)0;

    while (list != (MATRIX *)0){
      if (list->name == NULL) break;
      if (strcmp(list->name, name) == 0) return list;
      list = list->next;
    }

    /* Couldn't find the matrix */
    return (MATRIX *)0;
}

static int mat_load_helper(FILE *fp, int *type, int *mrows, int *ncols, 
            int *imagf, char *pname, double **preal, double **pimag, int ftype)
{
  if (ftype == 0)
    return loadmat(fp, type, mrows, ncols, imagf, pname, preal, pimag);
  else
    return loadmtx(fp, type, mrows, ncols, imagf, pname, preal, pimag);

}


MATRIX *mat_load(char *file)
{
    FILE *fp;
#ifdef MAT_DEBUG
    int i=0;
#endif
    int ftype=0;
    MATRIX *first, *prev, *cur;

    /* Open the file */
    if (strstr(file,".mat")){
      if ((fp = fopen(file, "rb")) == NULL) return (MATRIX *)0;
      ftype = 0;
    }
    else {
      if ((fp = fopen(file, "r")) == NULL) return (MATRIX *)0;
      ftype = 1;
    }

    first = mat_create(); cur = first; prev = cur;
    /* For the first pass, prev=cur so that if the first loadmat fails,
     * the assignment prev->next = NULL will not seg fault.  If loadmat
     * is successful, things are fine. */

    /* Read in a matrix */
      
    while (mat_load_helper(fp, &cur->type, &cur->nrows, &cur->ncols,
			   &cur->imagf, cur->name, &cur->real, &cur->imag, 
			   ftype) == 0){

#ifdef MAT_DEBUG
      printf("Loaded matrix %i, name %s\n",++i,cur->name);
#endif
      prev = cur;
      cur = mat_create();
      prev->next = cur;
      cur->prev = prev;
      
    }

    /* In case the load_mat fails on the first one */
    prev->next = (MATRIX *)0;
    mat_free(cur);

    fclose(fp);

    return first;
}

void mat_list(MATRIX *list){

  if (list == (MATRIX *)0){
    printf("The list contains no elements (NULL list).\n");
    return;
  }
  
  printf("The following matrices are in the list:\n");

  while (list != (MATRIX *)0) {
     printf("  %s\n",list->name);
     list = list->next;
  }
}



