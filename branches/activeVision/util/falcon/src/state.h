#ifndef _have_state
#define _have_state

#include "matrix.h"

typedef struct state_space {
  int n;  /* The number of states  */
  int m;  /* The number of inputs  */
  int p;  /* The number of outputs */

  MATRIX *A, *B, *C, *D, *u;

  MATRIX *y, *ycx, *ydu;

  MATRIX *xcur, *xnext, *xdelax, *xdelbu;

  int namelen;    /* name of state space system */
  char name[20];  /* name length (including NULL) */

} STATE_SPACE;

STATE_SPACE *ss_init(int nstates, int ninputs, int noutputs);
STATE_SPACE *ss_create();

int ss_setup(STATE_SPACE *s, MATRIX *list);
int ss_resize(STATE_SPACE *s, int nstates, int ninputs, int noutputs);

int ss_resize_state(STATE_SPACE *s, int nstates);
int ss_resize_inputs(STATE_SPACE *s, int ninputs);
int ss_resize_outputs(STATE_SPACE *s, int noutputs);
char *ss_get_name(STATE_SPACE *s);
int ss_set_name(STATE_SPACE *s, char *name);

int ss_get_nstates(STATE_SPACE *s);
int ss_get_noutputs(STATE_SPACE *s);
int ss_get_ninputs(STATE_SPACE *s);

MATRIX *ss_el_get(STATE_SPACE *s, char which);
int ss_el_set(STATE_SPACE *s, char which, MATRIX *mat);
int ss_el_free(STATE_SPACE *s, char which);
int ss_verify(STATE_SPACE *s);

void ss_reset(STATE_SPACE *s);
void ss_free(STATE_SPACE *s);

int ss_copy(STATE_SPACE *dst, STATE_SPACE *src);

void ss_print(STATE_SPACE *s);

int ss_set_ic(STATE_SPACE *s, double *ic);
double *ss_compute(STATE_SPACE *s, double *input);

int ss_set_input(STATE_SPACE *s, double *input);
void ss_equation(STATE_SPACE *s);
double *ss_output(STATE_SPACE *s);

double *ss_state(STATE_SPACE *s);
double *ss_nextstate(STATE_SPACE *s);

STATE_SPACE *ss_load(char *fname);
int ss_verify(STATE_SPACE *s);

/* Obsolete Function Names */
STATE_SPACE *ss_matlab_load(char *fname);

#endif
