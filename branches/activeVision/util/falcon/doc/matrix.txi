@c -*- texinfo -*-

@node     matrix,  state, lookup,    Top
@comment  node-name,  next,  previous,  up
@chapter Matrix Calculations
@cindex Matrix Calculations

The matrix calculation package allows for easy manipulation of matrices.
Currently, only real matrices are well supported.

@menu
* introduction: matrix/intro.          introduction to matrix package
* initialization: matrix/init.         initializing the matrix
* file format: matrix/file.            matrix file forma
* on-line creation: matrix/manual.     manual creation of matrices
* element access functions: matrix/el. element access functions
* math functions: matrix/math.         Interpolation functions
* other functions: matrix/other.       Other matrix functions
* function prototypes: matrix/proto.   Function prototypes
@end menu

@node matrix/intro, matrix/init, matrix, matrix
@section Introduction

The matrix calculation package provides a foundation for more complex
packages.  The state space routines are based upon this package, for
example.  Most basic matrix manipulation functions are implemented.
While not currently available, efficient inverse and least squares routines 
will be included.

The routines use a special strucure, called @code{MATRIX}.  This
structure serves simultaneously as both a list of matrices and as an
individual matrix.  Every matrix belongs to some list, even if the list
only contains one matrix.  Throughout this
documentation, the distinction between a matrix and its list are only
mentioned when the meaning is not obvious.

Within the @code{MATRIX}
structure, the matrix elements are stored in column order.  The 
user should neither change any of the elements in the structure 
nor directly access them.  All necessary information can be 
obtained through function calls.

All functions begin with the prefix @code{mat_}.  The structure 
(and also function prototypes) are specified in @code{matrix.h}.
Many functions have both a fast and a slow implementation.  The
slow implementations provide some error checking.  The fast versions,
with the suffix @code{_f}, do not perform error checking.

Matrices may be generated in advance and loaded from both @code{MATLAB}
data files and a formatted ASCII files.  Matrices can also be created
and destroyed during program execution.



@node matrix/init, matrix/file, matrix/intro, matrix
@section Initialization

The first step is to load the matrices into memory.  This is accomplished
using @code{mat_load}, which returns a pointer to the first matrix loaded.
If the load is not successful, the program returns a @code{NULL} pointer.
A sample program is shown below.
@cindex mat_load

@example
#include "matrix.h"

int main(argc,argv)
@{
    MATRIX *matrices;

    matrices=mat_load(filename);

    /* other code goes here */

    mat_free_list(matrices);
@}
@end example

The @code{MATRIX} structure is actually a doubly linked list of 
matrices.  Each @code{MATRIX} can hold as many matrices as you desire,
within memory limitations.  Individual matrices can be found, by name,
in the list using @code{mat_find}.  
@cindex mat_find

Since a @code{MATRIX} can contain many matrices, and some may be unwanted,
two methods exist for removing matrices from memory.
Individual matrices can be removed from the list by @code{mat_free}.
Note that removing the top matrix can make the rest of the list 
unaccessible.
To remove a matrix and all matrices further down the list, 
@code{mat_free_list} is used.  Improper use of
the memory freeing commands will cause memory to be held.
@cindex mat_free
@cindex mat_free_list


@node matrix/file, matrix/manual, matrix/init, matrix
@section The File Format

The function @code{mat_load} reads a file that contains matrices
stored in either a binary format or an ASCII format.  The binary 
format is identical to the MATLAB level 1.0 @code{.mat} format.
The ASCII format is described below.  The type of file is determined
by the file extension.  Binary files must end in @code{.mat}, while
ASCII files can end in anything else, though @code{.mtx} is the most
common suffix.
@cindex mat_load

The ASCII file format is best described by an example.
@example
Matrix Name
Type   Number of rows   Number of Columns  Imaginary
real row 1
real row 2
...
real row n
imag row 1
imag row 2
...
imag row n
Matrix Name
Number of rows   Number of Columns
row 1
row 2
...
row n
and so on...
@end example
@noindent
The first line is the name of the matrix, which must be less than 255
characters.  Currently, all names are truncated to 19 characters.  The
second line contains information about the size of the matrix.  Numbers
must be separated by white space or tabs.  The first
number, @code{type}, denotes the storage format of the matrix.  Currently,
the only matrix type is 1, and this field is ignored.  However, future
additions, such as sparse matrix storage, may use different types.  To
ensure future compatibility, this field must be set to 1.

The next two numbers are the number of rows and columns of the matrix.
The fourth number indicates if the matrix has imaginary data.  If this
value is nonzero, imaginary values are expected.  The remaining rows are
the matrix data, row by row, with all of the real data preceding the 
imaginary data.

Additional matrices are appended after the first matrix.  Any number of
matrices, of varying sizes and types,  can be stored in one file.

Binary data can be stored in either big endian or little endian format,
and read by both big endian and little endian machines.  This allows 
@code{.mat} files created on Sun machines to be read by PC's, and vice
versa.  Other binary formats are not currently supported.

@node matrix/manual, matrix/el, matrix/file, matrix
@comment  node-name,  next,  previous,  up
@section On-line matrix creation

It is often necessary to create matrices within a program for use in
intermediate computations.  The first step is to create the matrix using
@code{mat_create}, which returns a pointer to @code{MATRIX}.  This
creates a list of matrices with one matrix of zero size named "NONAME".
This list of matrices may be added to other lists of matrices, if
desired.  If the list cannot be created, @code{NULL} is returned.  Once
the matrix is created, it must be set to the correct size by
@code{mat_resize}.  These two commands are combined into a shorter
notation, @code{mat_init}, which also sets each element of the matrix to
0.  The command@code{mat_set_name} changes the name of the matrix.  A
name can be up to 19 characters long and cannot contain spaces.  A sample
program is shown below.
@cindex mat_create
@cindex mat_resize
@cindex mat_init
@cindex mat_set_name

@example
#include "matrix.h"

int main(argc,argv)
@{
    MATRIX *data;

    data=mat_init(4,2); /* 4 rows, 2 columns */
    mat_set_name(data,"some new data");

    /* other code goes here */

    mat_free(data);
@}
@end example

A matrix can always be resized using @code{mat_resize}, though all 
previous values are lost.  Each use of @code{mat_resize} contains
calls to both @code{free} and @code{malloc}.  @code{mat_resize}
does not initialize the elements.  The command @code{mat_reset}
sets all elements of the matrix to 0.
@cindex mat_resize
@cindex mat_reset

@node matrix/el, matrix/math, matrix/manual, matrix
@section Matrix Element Manipulation

Several accessor functions return information about a matrix.
@code{mat_name} returns a pointer to the matrices name.  While this
gives direct access to the data structure, it should not be used to
changed the matrix's name.  @code{mat_set_name} exists for this
purpose.  @code{mat_rows} and
@code{mat_columns} return the number of rows and columns of the
matrix, respectively.  If the matrix has not been created yet, 
-1 is returned.
@cindex mat_name
@cindex mat_set_name
@cindex mat_rows
@cindex mat_columns

To find the value of an individual element of a matrix, 
@code{mat_element_get} and @code{mat_element_get_f} are used.
Both of these functions return
the value of the specified element.  The difference is that
@code{mat_element_get_f} does not ensure that the requested element
is valid.  The fast versions are useful for real time code,
all debugging is complete.
@cindex mat_element_get
@cindex mat_element_get_f

Similar functions, @code{mat_element_set} and @code{mat_element_set_f},
change the value of any element in the matrix.  @code{mat_element_set_f}
returns a value of 1 if successful and a value of 0 is not.  Possible
error conditions include trying to a assign an element that is larger
than the matrix.
@cindex mat_element_set
@cindex mat_element_set_f

Two functions also exist for copying matrices.  @code{mat_copy}
copies one matrix into another.  The destination matrix is
automatically resized if needed.  If either matrix has not been created,
a value of 0 is returned.  @code{mat_copy_f} assumes that
the destination matrix is the same size as the source matrix.  These 
functions do not change the name of either matrix (the name is not copied).  
@cindex mat_copy
@cindex mat_copy_f

It is often useful to treat the matrix as an array.  The function
@code{mat_get_real} returns a pointer to the first element in the
matrix.  The data are ordered by columns.  The first column is 
stored before the second column, etc.  If the matrix is not initialized,
@code{NULL} is returned.  To access the imaginary portion of the matrix,
@code{mat_get_imaginary} is used.
@cindex mat_get_real
@cindex mat_get_imaginary


@node matrix/math, matrix/other, matrix/el, matrix
@comment  node-name,  next,  previous,  up
@section Math Functions

Math functions only support real matrices currently.

Each of the math functions has a slow and a fast form.  The slow form
returns a value of 1 if successful and 0 if not.  For the slow forms, 
in-place operations (A=A*B, for example) are acceptable.  They are not 
always allowed for the fast forms.

@unnumberedsubsec Transpose

These functions form the transpose of the source matrix.  For the fast
form, in place operations are not allowed.

@example
int mat_transpose(MATRIX *dst, MATRIX *src);
void mat_transpose_f(MATRIX *dst, MATRIX *src);
@end example
@cindex mat_transpose
@cindex mat_transpose_f

@unnumberedsubsec Determinant

These functions find the determinant of a matrix, which is returned as a
double.  In place operations are not applicable.

@example
int mat_det(double *det, MATRIX *mat);
double mat_det_f(MATRIX *mat);
@end example
@cindex mat_det
@cindex mat_det_f

@unnumberedsubsec Multiplication

These functions multiply two matrices.  For the fast
form, in place operations are not allowed.

@example
int mat_mult(MATRIX *dst, MATRIX *a, MATRIX *b);
void mat_mult_f(MATRIX *dst, MATRIX *a, MATRIX *b);
@end example
@cindex mat_mult
@cindex mat_mult_f

@unnumberedsubsec Addition

These functions add two matrices.  For the fast
form, in place operations are allowed.

@example
int mat_add(MATRIX *dst, MATRIX *a, MATRIX *b);
void mat_add_f(MATRIX *dst, MATRIX *a, MATRIX *b);
@end example
@cindex mat_add
@cindex mat_add_f

@unnumberedsubsec Subtraction

These functions subtract two matrices (dst = a -- b).  For the fast
form, in place operations are allowed.

@example
int mat_subtract(MATRIX *dst, MATRIX *a, MATRIX *b);
void mat_subtract_f(MATRIX *dst, MATRIX *a, MATRIX *b);
@end example
@cindex mat_subtract
@cindex mat_subtract_f

@unnumberedsubsec Scalar Multiplication

These functions multiply each element of a matrix by a scalar.  For the fast
form, in place operations are allowed.

@example
int mat_scale(MATRIX *dst, MATRIX *a, double scale);
void mat_scale_f(MATRIX *dst, MATRIX *a, double scale);
@end example
@cindex mat_scale
@cindex mat_scale_f

@unnumberedsubsec Scalar Addition

These functions add a scalar to each element of a matrix.  For the fast
form, in place operations are allowed.

@example
int mat_offset(MATRIX *dst, MATRIX *a, double offset);
void mat_offset_f(MATRIX *dst, MATRIX *a, double offset);
@end example
@cindex mat_offset
@cindex mat_offset_f

@unnumberedsubsec Elemental Multiplication

These functions multiply two matrices element by element.  
The two matrices must be the same size.  For the fast
form, in place operations are allowed.

@example
int mat_dotmult(MATRIX *dst, MATRIX *a, MATRIX *b);
void mat_dotmult_f(MATRIX *dst, MATRIX *a, MATRIX *b);
@end example
@cindex mat_dotmult
@cindex mat_dotmult_f

@unnumberedsubsec Matrix Inverse

These functions find the inverse of a matrix in a slow manner.
Ill conditioned matrices are not handled well.  
For the fast form, in place operations are not allowed.  

@example
int mat_inverse(MATRIX *dst, MATRIX *a, double scale);
void mat_inverse_f(MATRIX *dst, MATRIX *a, double scale);
@end example
@cindex mat_inverse
@cindex mat_inverse_f


@node matrix/other, matrix/proto, matrix/math, matrix
@comment  node-name,  next,  previous,  up
@section Other Matrix Functions

The command @code{mat_print} displays the matrix in a pretty
format (at least for small matrices).  @code{mat_list} displays
a list of all matrices in a specified list on the screen.
@cindex mat_print
@cindex mat_list

@node matrix/proto,  , matrix/other, matrix
@comment  node-name,  next,  previous,  up
@section Function Prototypes

All function prototypes are included in @code{matrix.h}.

@example
MATRIX *mat_init(int nrows, int ncols); /* Initializes an nxm matrix */
int mat_set_name(MATRIX *a, char *name);
char *mat_get_name(MATRIX *a);
void mat_print(MATRIX *a);
void mat_free(MATRIX *a);
void mat_reset(MATRIX *a);
int mat_resize(MATRIX *a, int nrows, int ncols);
double mat_element_get(MATRIX *mx, int row, int col);
double mat_element_get_f(MATRIX *mx, int row, int col);
int mat_element_set(MATRIX *mx, int row, int col, double value);
void mat_element_set_f(MATRIX *mx, int row, int col, double value);
int mat_copy(MATRIX *dst, MATRIX *src);
void mat_copy_f(MATRIX *dst, MATRIX *src);
double *mat_get_real(MATRIX *mat);
int mat_get_rows(MATRIX *mat);
int mat_get_cols(MATRIX *mat);
int mat_transpose(MATRIX *dst, MATRIX *a);
void mat_transpose_f(MATRIX *dst, MATRIX *a);
int mat_mult(MATRIX *dst, MATRIX *a, MATRIX *b);
void mat_mult_f(MATRIX *dst, MATRIX *a, MATRIX *b);
int mat_add(MATRIX *dst, MATRIX *a, MATRIX *b);
void mat_add_f(MATRIX *dst, MATRIX *a, MATRIX *b);
int mat_subtract(MATRIX *dst, MATRIX *a, MATRIX *b);
void mat_subtract_f(MATRIX *dst, MATRIX *a, MATRIX *b);
int mat_scale(MATRIX *dst, MATRIX *a, double scale);
void mat_scale_f(MATRIX *dst, MATRIX *a, double scale);
int mat_offset(MATRIX *dst, MATRIX *a, double offset);
void mat_offset_f(MATRIX *dst, MATRIX *a, double offset);

int mat_inverse();
void mat_inverse_f();
int mat_lstsqr();
void mat_lstsqr_f();
MATRIX *mat_load();

int matlab_open(char *file);
MATRIX *matlab_find(char *name);
int matlab_close();

@end example
