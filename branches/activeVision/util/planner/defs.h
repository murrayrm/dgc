#ifndef _DEFS_H_
#define _DEFS_H_

#include "specs.h"

#define IDX_THETA   0
#define IDX_DTHETA  1
#define IDX_DDTHETA 2
#define IDX_V       3
#define IDX_DV      4
#define IDX_SF      5
#define IDX_N       6
#define IDX_E       7
#define SIZE_ZP     8

// NUM_POINTS is the number of points in the spline
// COLLOCATION_FACTOR is the number of collocation points in each spline segment
// NUM_COLLOCATION_POINTS is the resulting total number of collocation points.
#define	NUM_POINTS								 7
#define	COLLOCATION_FACTOR				 5
#define NUM_SPEED_SEGMENTS         6
#define COLLOCATION_FACTOR_SPEED   5

#define NUM_COLLOCATION_POINTS      ((NUM_POINTS-1) * COLLOCATION_FACTOR      + 1)
#define NUM_COLLOCATION_POINTS_FINE ((NUM_POINTS-1) * COLLOCATION_FACTOR_FINE + 1)
#define COLLOCATION_FACTOR_FINE     400





// the scaling factors
#define SCALEFACTOR_DIST      (EXTRAFACTOR_DIST     / 1.0)
#define SCALEFACTOR_ACCEL     (EXTRAFACTOR_ACCEL    / VEHICLE_MAX_ACCEL)
#define SCALEFACTOR_YAWDOT    (EXTRAFACTOR_YAWDOT   / (M_PI / 10.0))
#define SCALEFACTOR_SPEED     (EXTRAFACTOR_SPEED    / MAXSPEED)
#define SCALEFACTOR_TANPHI    (EXTRAFACTOR_TANPHI   / VEHICLE_MAX_TAN_AVG_STEER)
#define SCALEFACTOR_PHIDOT    (EXTRAFACTOR_PHIDOT   / (2.0 * MAX_PHI / MIN_WHEEL_RAIL_TO_RAIL_TIME))
#define SCALEFACTOR_ROLLOVER  (EXTRAFACTOR_ROLLOVER / (MAXSPEED_AT_FULL_STEERING*MAXSPEED_AT_FULL_STEERING* VEHICLE_MAX_TAN_AVG_STEER / VEHICLE_WHEELBASE))

#define NUM_LIN_CONSTR        (NUM_LIN_INIT_CONSTR  + NUM_LIN_TRAJ_CONSTR  + NUM_LIN_FINL_CONSTR)
#define NUM_NLIN_CONSTR       (NUM_NLIN_INIT_CONSTR + NUM_NLIN_TRAJ_CONSTR + NUM_NLIN_FINL_CONSTR)

#define NPnumLinearConstr       ((NUM_LIN_INIT_CONSTR)  + (NUM_LIN_FINL_CONSTR)  + NUM_COLLOCATION_POINTS * (NUM_LIN_TRAJ_CONSTR))
#define NPnumNonLinearConstr    ((NUM_NLIN_INIT_CONSTR) + (NUM_NLIN_FINL_CONSTR) + NUM_COLLOCATION_POINTS * (NUM_NLIN_TRAJ_CONSTR))

#define MATRIX_INDEX(constr, i) ((constr) + (i)*(NPnumNonLinearConstr))


/* #define INTERPOLATION_SMOOTH */
#define INTERPOLATION_GAUSSIAN
// INTERPOLATION_BILINEAR is the default



// this is for the INTERPOLATION_SMOOTH, INTERPOLATION_GAUSSIAN intepolation methods
#define MAP_INTERP_RAD        2
#define MAP_INTERP_DIAM       (MAP_INTERP_RAD + MAP_INTERP_RAD)
#define MAP_INTERP_EXPCOEFF   (log(1.2e-2)/(2.0 * (double)(MAP_INTERP_RAD*MAP_INTERP_RAD))*2.0)




// these are for the INTERPOLATION_BILINEAR interpolation method. bilinear
// interpolation implies a window 2 points across:
// how many CELLS wide the map interpolation window is. must be even
#define MAP_INTERPOLATION_DIAM  4

#define MIN_SF                (PLANNING_TARGET_DIST / 2.0)
#define MAX_SF                (PLANNING_TARGET_DIST * 2.0)

#define BIGNUMBER             1.0e20

// if the seed says to move straight ahead, add this to the
// heading. this improves numerics
#define HEADINGCORRECTION 0.001
// if the seed turning radius is larger than this, set it to
// this. this improves numerics
#define MAXSEEDRADIUS 1000.0

#define CHECKDERIVATIVES_DELTA  (1e-7)

#define OBSTACLE_AVOIDANCE_DIAM   (OBSTACLE_AVOIDANCE_RAD*2 + 1)

// No target point constraint, constant S_f
/* #define CONSTANT_SF */




#define USE_FINAL_ANGLE_CONSTRAINT false

//  Which constraints are here
// Present: 1
// Absent:  0
#define VCONSTR					1
#define ACONSTR					1
#define PHICONSTR				1
#define PHIDCONSTR			1
#define ROLLOVERCONSTR	1

#define VCONSTR_IDX					(VCONSTR - 1)
#define ACONSTR_IDX					(VCONSTR_IDX + ACONSTR)
#define PHICONSTR_IDX				(ACONSTR_IDX + PHICONSTR)
#define PHIDCONSTR_IDX			(PHICONSTR_IDX + PHIDCONSTR)
#define ROLLOVERCONSTR_IDX	(PHIDCONSTR_IDX + ROLLOVERCONSTR)

#define MAX_PHI				VEHICLE_MAX_AVG_STEER
#define MAX_TAN_PHI		VEHICLE_MAX_TAN_AVG_STEER

// min time in which wheel can be turned from rail to rail
#define MIN_WHEEL_RAIL_TO_RAIL_TIME		1.0

#define SCALEFACTOR_TIME ( EXTRAFACTOR_TIME / PLANNING_TARGET_DIST / (NUM_POINTS-1))

#endif // _DEFS_H_
