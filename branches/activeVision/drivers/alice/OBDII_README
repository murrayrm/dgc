Thanks for stopping by to read the OBDII README documentation.

OBDII is currently being served by adrive in the actuator state struct (or
whatever it's called...ask Tully).  Functionality is prototyped in
OBDIIDriver.hpp, and it is defined in OBDIIDriver.cpp.  The serial drivers that
the OBDII code uses were written by Sam Pfister while he was working at
Evolution Robotics.  I am using these instead of the standard DARPA serial
drivers for a couple reasons: the standard drivers were only recently fixed, and
I was developing OBDII code on Sam's laptop, which only could conveniently use
his own drivers.  It should be easy to port this to the SDS.

The current functionality that is present in OBDIIDriver.hpp is outlined below:

int connect(DEVICE_NAME)		//DEVICE_NAME needs to be an STL string specifying the path of the serial port, e.g. /dev/ttyS0

int init()

int getRPM()				//returns the crankshaft RPM

int getTimeSinceEngineStart()		//returns the time in seconds since the
last ignition event

int getVehicleSpeed()			//returns in meters/sec

int getEngineCoolantTemperature()	//returns in degrees F

int getTorque()				//returns in footpounds

int getGlowPlugLampTime()		//meaning unknown, UNTESTED

int getThrottlePosition()		//returns a percentage, with 0% being
idle

float getCurrentGearRatio()		//engineRPM:transmissionRPM

bool GlowPlugLightOn()			//true if the glow plug light is on,
false othewise, UNTESTED

int getCurrentGear()			//values from 1 to 5 (or 6), each
representing first gear, second gear, etc.  This is only useful when the vehicle
is in DRIVE, UNTESTED

char getTransmissionPosition()		//returns a character representing the
position of the shift lever, P = park, R = reverse, N = neutral, D = drive

float getTorqueConvClutchDutyCycle()	//INOP

int getTorqueConvControlState()		//returns 0 is the torque converter
clutch is not being commanded, 1 if it is commanded locked, and 2 if it is
commanded unlocked, UNTESTED

void clearDTCs()			//INOP

void requestDTCs()			//INOP



It may go without saying, but please don't use functions that are labeled as
INOP.  They are known not to work for various reasons, and will cause you to be
angry if you use them and think their outputs are correct.

The three functions labeled UNTESTED are indeed untested.  They have only been
run in conditions where they can have one possible state/value, and even though they
do report the expected result, that is no guarentee that they are correct for all states/values.



	// INIT RESPONSES from CAN bus 
	
	// 40 | 80 | 57 | 16 | 7f | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 6c |
	
	// 40 | 80 | 57 | 16 | 7f | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 6c |

	// these all come at once as a long 42 byte string
	// 40 | 88 | 06 | 41 | 00 | 98 | 3b | 00 | 17 | 00 | 00 | 00 | 00 | b9 | 
	// 40 | 88 | 06 | 41 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | cf | 
	// 40 | 88 | 06 | 41 | 00 | 98 | 18 | 00 | 13 | 00 | 00 | 00 | 00 | 92 |

	// these are received together as a 28 byte string
	// 40 | 88 | 06 | 41 | 01 | 00 | 04 | 00 | 00 | 00 | 00 | 00 | 00 | d4 |
	// 40 | 88 | 06 | 41 | 01 | 81 | 05 | 80 | 00 | 00 | 00 | 00 | 00 | d6 |

	// these are received together as a 28 byte string
	// 40 | 88 | 07 | 42 | 00 | 00 | d8 | 18 | 00 | 03 | 00 | 00 | 00 | c4 | 
	// 40 | 88 | 07 | 42 | 00 | 00 | d8 | 3b | 00 | 07 | 00 | 00 | 00 | eb |

	// SAMPLE QUERY COMMAND RESPONSES
	// 40 | 88 | 07 | 42 | 00 | 00 | d8 | 3b | 00 | 07 | 00 | 00 | 00 | eb | 
	// 40 | 88 | 07 | 42 | 00 | 00 | d8 | 18 | 00 | 03 | 00 | 00 | 00 | c4 |
	// 40 | 88 | 07 | 42 | 01 | 00 | 00 | 05 | 80 | 00 | 00 | 00 | 00 | 57 |
	// 40 | 88 | 05 | 42 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | d1 | 
	// 40 | 88 | 05 | 42 | 02 | 00 | 01 | 55 | 00 | 00 | 00 | 00 | 00 | 27 |
	// 40 | 88 | 05 | 42 | 02 | 00 | 01 | 55 | 00 | 00 | 00 | 00 | 00 | 27 | 
	// 40 | 88 | 05 | 42 | 02 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | d1 |
 