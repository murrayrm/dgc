#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <sstream>
#include <string>
#include <iostream>
#include <stdio.h>


#include <opencv/cv.h>
#include <opencv/cvaux.h>
#include <opencv/highgui.h>

#include "stereoSource.hh"

using namespace std;

/* goes through images from a video of sun dazzle, changed the grab() funktion so that it compares vertical subwindows */

int main(int argc, char *argv[]){
  if(argc != 3){
    printf("Have to add two arguments, auto(0)/control(1) SaveAs \n");
    return 1;
  }
 
  if( strcmp(argv[1],"0")==0){     //autoExposure
    printf("USING AUTOEXPOSURE \n");
    stereoSource s;
    s.init(0,"config/stereovision/CamID.ini.short", argv[2], "bmp");
    while (true){    //takes and saves 10 images
      s.grab();
      s.save();
    }
    return 0;
  }
  
  if (strcmp(argv[1],"1")==0){    //exposureControl
    printf("USING EXPOSURECONTROL \n");
    stereoSource ss;
    ss.init(0,"config/stereovision/CamID.ini.short", argv[2], "bmp");
    ss.startExposureControl(cvRect(0,210,640,200));
    while(true){      //takes and saves 10 images
      ss.grab();
      ss.save();
    }
    ss.stopExposureControl();
    return 0;
  }

  if ( strcmp(argv[1],"2")==0){   //test different controlers
    printf("TESTING DIFFERENT CONTROLERS \n");
    stereoSource st;
    st.init(0, "config/stereovision/CamID.ini.short", "temp", "bmp");
    CvRect partOfImage = cvRect(0,210,640,200);   //regionOfInterest
    st.startPidControl();
    FILE * pidFile = fopen(argv[2],"w");
    for( double p= 0.1; p< 0.25; p+= 0.05){
      for( double i= 0.0; i< 1.0; i+= 0.25){
	for( double d =0.0; d < 0.4; d+= 0.1){
	  printf("Testing with p, i, d = %lf %lf %lf \n", p, i, d);
	  st.grab();   //AutoExposure
	  double b = st.getAvgBrightness(0,0,640,210,410);//ROI
	  fprintf(pidFile, "%lf \n", b);
	  st.startExposureControl(partOfImage);
	  st.setExposureControl(p, d, i, 1.0);
	  for( int k=0; k<=60 ; ++k){          // 60 "grabbings"
	    st.grab();
	    b = st.getAvgROIBrightness();
	    fprintf(pidFile, "%lf  ", b);
	  } 
	  st.stopExposureControl();
	  fprintf(pidFile, "\n\n");
	  
	   for(int k=0; k<10; ++k) {
	     st.grab();
	     //double b = st.getAvgBrightness(0,0,640,210,410); //ROI
	     cout.flush();
	     usleep(300000);
	   }
	}
      }
    } 
    fclose(pidFile);
    st.stopExposureControl();
    st.stop();
  }
  
  /*stringstream ss;
  string fileName;
  for(int i=1; i<30; ++i){  //first 9 images in /pngs/
    stereoSource dazzleSource;
    ss << i;

    if( i<10 ) {
      fileName = "0000";
    }
    else if( i<100 ){
      fileName= "0000";
    }
    else if( i<1000 ){
      fileName= "00";
    }

    fileName= "pngs/"+fileName;

    dazzleSource.init(0,640,480, (char*)fileName.c_str(), "bmp", i);
    dazzleSource.grab();
    ss.str("");
    }*/

  //testar pa verkliga bilder fran kamerorna pa Chlorine, endast en bild tas!
  stereoSource dss;
  
  

  /*dss.init(0,"config/stereovision/CamID.ini.short", "temp", "bmp");
  cout << "Enter 'n' to get next grab, 'q' to quit" << endl;
  char e;
  cin >> e;
  do{
    dss.grab();
    double b = dss.getAvgROIBrightness();
    cout << "AvgBrightnessError== " << (128-b) << endl;
    cout << "Enter char again" << endl;
    cin >> e;
  }
  while (e!='q');
  cout << "Quit" << endl;*/
}
