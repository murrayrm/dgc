;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File name: ` ~/.emacs '
;;; ---------------------
;;;
;;; If you need your own personal ~/.emacs
;;; please make a copy of this file
;;; an placein your changes and/or extension.
;;;
;;; Copyright (c) 1997-2002 SuSE Gmbh Nuernberg, Germany.
;;;
;;; Author: Werner Fink, <feedback@suse.de> 1997,98,99,2002
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Test of Emacs derivates
;;; -----------------------

; make a simple insert date keybinding
(defun insert-standard-date ()
   "Inserts standard date time string." (interactive)
     (insert (format-time-string "%s")))
(global-set-key [(control ?c) (?d)] 'insert-standard-date)

; make a BETTER insert date keybinding
; the "* " at the front encourages people to nicely format their logs for the wiki
(defun insert-readable-date ()
   "Inserts human readable date time string." (interactive)
     (insert (format-time-string "* [%s %l:%M:%S %P] ")))
(global-set-key [(control ?c) (?c)] 'insert-readable-date)



(if (string-match "XEmacs\\|Lucid" emacs-version)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; XEmacs
  ;;; ------
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (progn
     (if (file-readable-p "~/.xemacs/init.el")
        (load "~/.xemacs/init.el" nil t))
  )
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; GNU-Emacs
  ;;; ---------
  ;;; load ~/.gnu-emacs or, if not exists /etc/skel/.gnu-emacs
  ;;; For a description and the settings see /etc/skel/.gnu-emacs
  ;;;   ... for your private ~/.gnu-emacs your are on your one.
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (if (file-readable-p "~/.gnu-emacs")
      (load "~/.gnu-emacs" nil t)
    (if (file-readable-p "/etc/skel/.gnu-emacs")
	(load "/etc/skel/.gnu-emacs" nil t)))

  ;; Custom Settings
  ;; ===============
  ;; To avoid any trouble with the customization system of GNU emacs
  ;; we set the default file ~/.gnu-emacs-custom
  (setq custom-file "~/.gnu-emacs-custom")
  (load "~/.gnu-emacs-custom" t t)
;;;
)
;;;


;; Set up the keyboard so the delete key on both the regular keyboard
;; and the keypad delete the character under the cursor and to the right
;; under X, instead of the default, backspace behavior.
(global-set-key [delete] 'delete-char)
(global-set-key [kp-delete] 'delete-char)

;; fix backspace
(global-set-key "\C-h" 'backward-delete-char)

;;disable autobackup
(setq make-backup-files nil) 


;; turn on font-lock mode                                                      
(global-font-lock-mode t)
;; enable visual feedback on selections                                       
(setq-default transient-mark-mode t)

;; always end a file with a newline                                            
(setq require-final-newline t)

;; stop at the end of the file, not just add lines                             
(setq next-line-add-newlines nil)

(when window-system
  ;; enable wheelmouse support by default                                      
  (mwheel-install)
  ;; use extended compound-text coding for X clipboard                         
  (set-selection-coding-system 'compound-text-with-extensions))

;; Set titles for frame and icon (%f == file name, %b == buffer name)          
(setq-default frame-title-format (list "Emacs: %f"))
(setq-default icon-title-format "Emacs - %b")


;; make meta-g do "goto-line"
(global-set-key "\M-g" 'goto-line)
