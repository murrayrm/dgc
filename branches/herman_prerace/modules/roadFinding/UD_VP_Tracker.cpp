//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_VP_Tracker.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// handles bookkeeping when we are doing multiple run()'s per image
/// versus just one.  does no actual computation; returns TRUE if ready for 
/// next image to be captured, FALSE if still working

int UD_VP_Tracker::iterate()
{
  // continuing to work on current image

  if (end_iteration == NONE || num_iteration < end_iteration) {
    num_iteration++;
    return FALSE;
  }

  // done with this one, ready for next image

  else { 
    num_iteration = 0;
    if (do_reset)
      pf_reset();      // vanishing point particle filter
    return TRUE;
  }
}

//----------------------------------------------------------------------------

/// constructor for vanishing point tracker class derived from particle filter class

UD_VP_Tracker::UD_VP_Tracker(UD_VanishingPoint *van, int num_particles, UD_Vector *meanstate, UD_Vector *covdiag, UD_Vector *delta) : UD_ParticleFilter(num_particles, meanstate, covdiag)
{
  // local names 
  
  vp = van;
  w = vp->w;
  h = vp->h;

  // default values

  show_particles = FALSE;
  do_reset = FALSE;    // don't reset tracker every image
  end_iteration = 0;   // 1 iteration per image 

  // body

  num_iteration = 0;

  // only happens when this is a multi-tracker

  if (!num_particles || !meanstate || !covdiag || !delta)
    return;

  initstate = copy_UD_Vector(meanstate);
  dstate = copy_UD_Vector(delta);
}

//----------------------------------------------------------------------------

/// reset particle filter and redistribute particles across 
/// candidate search region in regular grid pattern

void UD_VP_Tracker::pf_reset()
{
  copy_UD_Vector(initstate, state);  
  reset();  // particle filter
}

//----------------------------------------------------------------------------

/// utility rounding function

double UD_VP_Tracker::myround(double x)
{
  if (x - floor(x) >= 0.5)
    return ceil(x);
  else
    return floor(x);
}

//----------------------------------------------------------------------------

/// handle command-line parameters related to road tracker functions

void UD_VP_Tracker::process_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-deltax"))
      dstate->x[0] = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-deltay"))
      dstate->x[1] = atof(argv[i + 1]);
    else if (!strcmp(argv[i],                        "-doreset")) 
      do_reset = TRUE;
    else if (!strcmp(argv[i],                        "-iterations")) {
      end_iteration = atoi(argv[i + 1]);
      if (!end_iteration) {
	printf("0 iterations per image means no work to do!\n");
	exit(1);
      }
      else if (end_iteration > 0)
	end_iteration--;
    }
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
