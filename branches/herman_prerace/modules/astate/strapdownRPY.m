function [posbuffer, Xbuffer, Jbuffer] = strapdown(imuin,gpsin)


% 
% Differential eqns
% fn = Cnb * [dvx dvy dvz]'                   --  Transform dv's into nav frame
% gn = Cne * calc-grav(lat,long,height)       --  Find gravity vector in nav frame
% wne = [vy/Ry, -vx/Rx, 0]'                   --  Find transport rate vector
% wni = Cne * wie                             --  Find earth rate vector
% wnb = -Cbn*( wne + wni ) + wbi
%

MinR = 1.56786504e-07;

R = 6378137; %This should not be CONSTANT!!!!!

gpsXoffset = 0.0;
gpsYoffset = -0.317;
gpsZoffset = 0.0;

KF_X = zeros(10,1); %Initial erros -- best assumption is zero
KF_Xe = zeros(10,1); %Initial estimate -- similarly zero
KF_P = diag([MinR^2, MinR^2, 2^2, .05^2, .05^2, 2^2,.01^2,.01^2,.01^2,.05^2]); %Tune these later
KF_Pe = zeros(10,1); %Initial Covar Estimate (not used so initialize 0)
KF_K = zeros(10,4); %Initial Kalman Gains (not used so initialize 0)
KF_Q = diag([1e-10,1e-10,.001,.001,.001,.01,.00005,.00005,.0000005,.0000001,]); %Tune Process Noise Later
KF_A = zeros(10,10); %Calculated based on state

KF_R = diag([MinR^2, MinR^2, 1, MinR^2, MinR^2, MinR^2]);

KF_H = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0;
	    0, 1, 0, 0, 0, 0, 0, 0, 0, 0;
        0, 0, 1, 0, 0, 0, 0, 0, 0, 0;
        0, 0, 0, 1, 0, 0, 0, 0, 0, 0;
        0, 0, 0, 0, 1, 0, 0, 0, 0, 0;
  	    0, 0, 0, 0, 0, 1, 0, 0, 0, 0];

% Frequency at which IMU inputs are summed:
updatefreq = 5;
correctfreq = 400;

% Time-diff for summed IMU inputs:
dt = updatefreq / 400;

% Rotation rate of earth relative to inertial frame in earth frame:
we_ie = [0 0 (2*pi/(24*60*60))]';

% Initial velocities in nav frame:
vn = [0 0 0]';

alpha = 0;

lat = mean(gpsin(1:10,2)) * pi/180;
lon = mean(gpsin(1:10,3)) * pi/180;

% Average first 1000 imu values to find:

% Gtmp = G in body frame
gtmp = mean(imuin(1:1000,2:4))'
% Wtmp = Earth rotation rate in body frame
wtmp = mean(imuin(1:1000,5:7))'
        
% Work backwards to find roll, pitch, yaw from these.
pitch = asin(gtmp(1) / norm(gtmp))
roll = asin(-gtmp(2) / (norm(gtmp)*cos(pitch)))
ctmp = nav2body(roll,pitch,0);
wtmp2 = ctmp' * wtmp;
yaw = -alpha + -atan2(wtmp2(2), wtmp2(1))

% Construct nav 2 body and body 2 nav rotation matrices:
Cbn = nav2body(roll,pitch,yaw);
Cnb = Cbn';

% Average first 10 gps reads to get initial position
gpsloc = [gpsXoffset; gpsYoffset; gpsZoffset];
gpsloc = Cnb*gpsloc/R;
lat = lat - gpsloc(1);
lon = lon - gpsloc(2);
height = -mean(gpsin(1:10,5));

% Construct earth 2 nav and nav 2 earth rotation matrices:
Cne = earth2nav(lat,lon,alpha);
Cen = Cne';

% Extract lat,lon,alpha,roll,pitch,yaw from rotation matrices.
lat = asin( - Cne(3,3) );
lon =  atan2( -Cne(3,2), -Cne(3,1) );
alpha = atan2( -Cne(2,3), Cne(1,3) );
pitch = asin( - Cbn(1,3));
yaw = atan2( Cbn(1,2), Cbn(1,1) );
roll = atan2( Cbn(2,3), Cbn(3,3) );

gpsind = 1;
gpslastind = 0;
newGPS = 0;
correctErrors = 0;
propagateState = 0;

isGPSValid = 1;
isGPS3D = 1;

% ALlocate empty buffer.
posbuffer = [];
Xbuffer = [];
Jbuffer = [];

% Initialize imu sums to 0:
dvx = 0;
dvy = 0;
dvz = 0;
dtx = 0;
dty = 0;
dtz = 0;

pError = 1;

for i=45000:100000,
%for i=1:size(imuin,1),

	%DATA READS:

	%Sum up IMU values
	imutime = imuin(i,1);
	dvx = dvx + imuin(i,2);
	dvy = dvy + imuin(i,3);
	dvz = dvz + imuin(i,4);
	dtx = dtx + imuin(i,5);
	dty = dty + imuin(i,6);
	dtz = dtz + imuin(i,7);
	
	
	while ((gpsin(gpsind,1) < imutime) && gpsind < size(gpsin,1))
		gpsind = gpsind + 1;
	end;
    
	gpsind = gpsind - 1;
	
	if (gpsind <= 0)
		gpsind = 1;
	end;

	if (gpsind > gpslastind),
        
		gpsloc = [gpsXoffset; gpsYoffset; gpsZoffset];
        Calpha = [cos(alpha) -sin(alpha) 0;
                  sin(alpha)  cos(alpha) 0;
                  0           0          1;];
		gpsloc = Calpha*Cnb*gpsloc/R;
		
		gpstime = gpsin(gpsind,1);
		glat = gpsin(gpsind,2) * pi/180-gpsloc(1);
		glon = gpsin(gpsind,3) * pi/180-gpsloc(2);
		gheight = -gpsin(gpsind,5);
		gpstmp =  Calpha'*[gpsin(gpsind,6); gpsin(gpsind,7); gpsin(gpsind,8)];
		gvn = gpstmp(1);
		gve = gpstmp(2);
		gvd = gpstmp(3);
		ggdop = gpsin(gpsind,9);
        gpfom = gpsin(gpsind,10);
        gnavmode = gpsin(gpsind,11);
        
        %Check for valid signals
        if (bitget(gnavmode,8) == 0)
            isGPSValid = 0;
        else
            isGPSValid = 1;
        end
        
        if (bitget(gnavmode,7) == 0)
            isGPS3D = 0;
        else
            isGPS3D = 1;
        end
        
        newGPS = 1;
		gpslastind = gpsind;
	end;

	if (mod(i,updatefreq) == 0),
		propagateState = 1;
	end;

	if (mod(i,correctfreq) == 0),
		correctErrors = 1;
	end;
	
    	%Only update stuff on updatefreq sums
	if (propagateState == 1),
	
        % fn is effective accelerations in nav frame
		fn = Cnb * [dvx dvy dvz]' / dt;
		
        % Rotation of body frame relative to inertial frame in body frame
		wb_ib = [dtx dty dtz]' / dt;
		
        % Calculate gravity vector:
		gn = calcGrav(lat, height);

        % Calculate transport rate:
		wn_en = calcwne(lat,height,alpha,vn(1),vn(2));

        % Find earth rate in nav frame:
		wn_ie = Cne * we_ie;
		
        % Calculate rotations rates in different frames:
		wn_in = wn_ie + wn_en;
		
		wn_bn = wn_in - Cnb * wb_ib; 
		
		% Now, update vn, Cne, and Cnb according to:
        	%
		%vn_dot = fn - (W(wne) + 2 * W(wni) )*vn + gn;
		%Cne_dot = - W(wne) * Cne;
		%Cnb_dot = - W(wnb) * Cnb;
		
		vdot = fn - (W(wn_en) + 2 * W(wn_ie) )*vn + gn;
		
		vn  = vn + dt*( vdot );
		
        height = height + dt * vn(3);

		Cne = MatDiffEqStep(wn_en * dt) * Cne;
		Cen = Cne';
		
		Cnb = MatDiffEqStep(wn_bn * dt) * Cnb;
		Cbn = Cnb';

		%Calculate KF_A:
		vx = vn(1);
		vy = vn(2);
		vz = vn(3);
		px = wn_en(1);
		py = wn_en(2);
		pz = wn_en(3);
		wx = wn_in(1);
		wy = wn_in(2);
		wz = wn_in(3);
		ox = wn_ie(1);
		oy = wn_ie(2);
		oz = wn_ie(3);
		fx = fn(1);
		fy = fn(2);
		fz = fn(3);
		magg = norm(gn);
		

		dXdt = [
			0, 0, -vy/R^2, 0, 1/R, 0, 0, 0, 0, -py;
			0, 0, vx/R^2, -1/R, 0, 0, 0, 0, 0, px;
			0, 0, 0, 0, 0, 1, 0, 0, 0, 0;
			-2*(vy*oy + vz*oz), 2*vy*ox, -vz*vx/R^2, vz/R, 2*oz, -(py + 2*oy), 0, -fz, fy, 2*vz*ox;
			2*vx*oy, -2*(vx*ox + vz*oz), -vy*vz/R^2, -2*oz, vz/R, (px + 2*ox), fz, 0, -fx, 2*vz*oy;
			2*vx*oz, 2*vy*oz, (vx^2 + vy^2)/R^2+2*magg/R, 2*(py + oy), -2*(px + ox), 0, -fy, fx, 0, -2*(vx*ox + vy*oy);
			0, -oz, -vy/R^2, 0, 1/R, 0, 0, wz, -wy, oy;
			oz, 0, vx/R^2, -1/R, 0, 0, -wz, 0, wx, -ox;
			-oy, ox, 0, 0, 0, 0, wy, -wx, 0, 0;
			py, -px, 0, 0, 0, 0, 0, 0, 0, 0; ];
        

 		KF_A = dXdt * dt + eye(10);

		% Propagate KF Terms:
		KF_P = KF_A * KF_P * KF_A' + KF_Q;

		KF_X = KF_A * KF_X;
		
        
        % Extract information from rotation matrices
		lat = asin( - Cne(3,3) );
		lon =  atan2( -Cne(3,2), -Cne(3,1) );
		alpha = atan2( -Cne(2,3), Cne(1,3) );
		pitch = asin( - Cbn(1,3));
		yaw = atan2( Cbn(1,2), Cbn(1,1) );
		roll = atan2( Cbn(2,3), Cbn(3,3) );
        
        % Fill buffer:
		posbuffer = [posbuffer; [imutime,gpstime,glat+gpsloc(1),glon+gpsloc(2),gheight,lat,lon,alpha,height,roll,pitch,yaw,vn(1),vn(2),vn(3),gvd,gpfom/100,gnavmode]];
		Xbuffer = [Xbuffer; KF_X'];
          
        % Zero sums:
		dvx = 0;
		dvy = 0;
		dvz = 0;
		dtx = 0;
		dty = 0;
		dtz = 0;

		propagateState = 0;
    end;
    
	if (newGPS == 1 && isGPSValid == 1),

		%Position Errors:
        % Cne = [I - dTheta X]CneMeas
		CneMeas = earth2nav(glat,glon,alpha);
		CenMeas = CneMeas';
        tmp = eye(3) - (Cne * CenMeas);
        dTheta = UnW(tmp);
        
        dVn = [vn(1) - gvn; vn(2) - gve; vn(3) - gvd];
        
        if(isGPS3D)
            dH = height - gheight;
        else
            dH = KF_X(3);
        end
        
		KF_Z = [dTheta(1), dTheta(2),  dH, dVn(1), dVn(2), dVn(3)]';

		KF_R = diag([(gpfom/100)^2/2*(MinR)^2, (gpfom/100)^2/2*(MinR)^2, (gpfom/100)^2, 4^2, 4^2, 5^2]); %Read this out of GPS in future

        KF_K = KF_P * KF_H' * (KF_H * KF_P * KF_H' + KF_R)^-1;
        
		KF_X = KF_X + KF_K * (KF_Z - KF_H * KF_X);

		KF_P = (eye(10) - KF_K * KF_H)*KF_P;

		newGPS = 0;
	end;

	%correctErrors = 0;
    
	if (correctErrors == 1),
		dTheta = [KF_X(1); KF_X(2); KF_X(10)];
		dPhi   = [KF_X(7); KF_X(8); KF_X(9)];
		dH = KF_X(3);
		dVn = [KF_X(4); KF_X(5); KF_X(6)];
        
        Cne = MatDiffEqStep(-pError*dTheta)*Cne;
        Cen = Cne'; 
        
        Jbuffer = [Jbuffer; pError*dTheta(1)*R, pError*dTheta(2)*R];
        
        Cnb = MatDiffEqStep(-dPhi)*Cnb;
        Cbn = Cnb';

		height = height - pError*dH;

 		vn = vn - dVn;

		lat = asin( - Cne(3,3) );
		lon =  atan2( -Cne(3,2), -Cne(3,1) );
        alpha = atan2( -Cne(2,3), Cne(1,3) );
		pitch = asin( - Cbn(1,3));
		yaw = atan2( Cbn(1,2), Cbn(1,1) );
		roll = atan2( Cbn(2,3), Cbn(3,3) );
        
        KF_X = [(1-pError)*dTheta(1); (1-pError)*dTheta(2); (1-pError)*dH; 0; 0; 0; 0; 0; 0; (1-pError)*dTheta(3)]; 
        
		correctErrors = 0;
	end;

end;
KF_P

%-------------------------------------------------------------------------%
% FUNCTIONS
%-------------------------------------------------------------------------%

function output = W(x),
output = [0, -x(3, 1), x(2,1);
          x(3,1), 0, -x(1,1);
          -x(2,1), x(1,1), 0];

function output = UnW(x),
output = [-x(2,3),x(1,3),-x(1,2)]';

function m = earth2nav(lat,lon,alpha),

rotlon = [
     cos(lon)    sin(lon)       0
    -sin(lon)    cos(lon)       0
        0           0           1
    ];

rotlat = [
     cos(lat)       0        sin(lat)
        0           1           0
    -sin(lat)       0        cos(lat)
    ];

chngaxis = [
        0           0           1
        0           1           0
       -1           0           0
    ];

 rotalpha =  [cos(alpha)   sin(alpha)   0     
             -sin(alpha)  cos(alpha)   0
              0            0            1];
                   

m = rotalpha * chngaxis * rotlat * rotlon;


function m = nav2body(roll,pitch,yaw),

rotyaw = [
     cos(yaw)    sin(yaw)       0
    -sin(yaw)    cos(yaw)       0
        0           0           1
    ];

rotpitch = [
     cos(pitch)     0      -sin(pitch)
        0           1           0
     sin(pitch)     0       cos(pitch)
    ];

rotroll = [
        1           0           0
        0       cos(roll)   sin(roll)
        0      -sin(roll)   cos(roll)
    ];

m = rotroll * rotpitch * rotyaw;

function g = calcGrav(lat, height)
    epsilon = 0.0818191908426;
    gWGS0 = 9.7803267714;
    gWGS1 = 0.00193185138639;
    Re = 6378137.;
    g0 = gWGS0*(1+gWGS1*sin(lat)^2)/sqrt(1 - epsilon^2*sin(lat)^2);
    g = [0, 0, g0*(1+2*height/Re)]';
   
function wneout = calcwne(lat, height, alpha, vx, vy)
    height = -height;
    epsilon = 0.0818191908426;
    Re = 6378137.;
    Rmeridian = Re*(1-epsilon^2)/(1-epsilon^2*sin(lat)^2)^(3/2);
    Rnormal = Re/sqrt(1-epsilon^2*sin(lat)^2);
    rhox = vy*(cos(alpha)^2/(Rnormal + height) + sin(alpha)^2/(Rmeridian + height)) - vx*sin(alpha)*cos(alpha)*(1/(Rmeridian + height) - 1/(Rnormal + height));
    rhoy = -vx*(sin(alpha)^2/(Rnormal + height) + cos(alpha)^2/(Rmeridian + height)) + vy*sin(alpha)*cos(alpha)*(1/(Rmeridian + height) - 1/(Rnormal + height));
    wneout = [rhox, rhoy, 0]';
    %wneout = [vy/Re, -vx/Re, 0]';
 
function rot = MatDiffEqStep(w),
    if (norm(w) > 0),
        nw = norm(w);
        w = w / nw;
        what = [0, -w(3), w(2);
                w(3), 0, -w(1);
                -w(2), w(1), 0;];
        rot = eye(3) + what * sin(-nw) + what^2 * (1 - cos(-nw));
    else,
        rot = eye(3);
    end;

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% Only comments below this line!
%
% SCRATCH TEST CODE:    
% Cne = earth2nav(pi/4, pi/4, 0);
% Cbn = nav2body(pi/8, pi/8, pi/4)
% 
% figure(1);
% close;
% 
% %Plot Earth Axes:
% [x,y,z] = sphere(10);
% surfl(x*3,y*3,z*3)
% 
% hold on;
% plot3([-4,4],[0,0],[0,0],'r--');
% plot3([0,0],[-4,4],[0,0],'g--');
% plot3([0,0],[0,0],[-4,4],'b--');
% hold off;
% 
% 
% Pt0 = [0;0;-3.5];
% Pt1 = [1;0;0];
% Pt2 = [0;1;0];
% Pt3 = [0;0;1];
% 
% Pt0 = Cne'*Pt0
% Pt1 = Cne'*Pt1 + Pt0
% Pt2 = Cne'*Pt2 + Pt0
% Pt3 = Cne'*Pt3 + Pt0
% 
% hold on;
% plot3([Pt0(1),Pt1(1)],[Pt0(2),Pt1(2)],[Pt0(3),Pt1(3)],'r-');
% plot3([Pt0(1),Pt2(1)],[Pt0(2),Pt2(2)],[Pt0(3),Pt2(3)],'g-');
% plot3([Pt0(1),Pt3(1)],[Pt0(2),Pt3(2)],[Pt0(3),Pt3(3)],'b-');
% hold off;
% 
% axis equal;
