/**
 * DGCEntry.hh
 * Revision History:
 * 07/08/2005  hbarnor  Created
 * $Id: DGCEntry.hh 8530 2005-07-11 06:26:58Z hbarnor $
 */

#ifndef DGCENTRY_HH
#define DGCENTRY_HH

#include <gtkmm/entry.h>
#include "DGCWidget.hh"

using namespace std;
/**
 * Customized Gtk::Entry for our the Tab api. 
 * Subclasses Gtk::Entry and adds a type-field. The type field is used to
 * determine the type of widget.. This is used mostly for error
 * correction. 
 */

class DGCEntry : public DGCWidget, public Gtk::Entry
{

public:
  /**
   * Constructor for the entry.
   * Contructor for a entry with a default text value.
   * @param label - the default text to be displayed
   */
  DGCEntry(const Glib::ustring& label);

  /**
   * @see DGCWidget
   */
  virtual string getText();
  /**
   * @see DGCWidget
   */
  virtual void displayNewValue();
};

#endif
