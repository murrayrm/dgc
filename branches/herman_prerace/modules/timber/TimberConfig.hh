#ifndef TIMBER_CONFIG
#define TIMBER_CONFIG

#define DEFAULT_DEBUG      0        /**< the debug level that will be used for all
				     * debugPrint statements, unless it is changed using
				     * setDebugLevel() */
#define MAX_SERVER_DELAY   10.0     /**< seconds before we consider the server AWOL */
#define SERVER_SLEEP       0.2      /**< how long the server sleeps between messages (sec)*/
#define MAX_PLAYBACK_SLEEP 200000   /**< the longest we'll ever sleep without waking up
				     * to check to make sure the playback speed hasn't
				     * changed (in usec). */

#define TODOFILE "todo_timber"
#define TODOHEADER "#!/bin/sh"
#define TODOPERMISSIONS "chmod a+wrx"
#define DEFAULT_MODULENAME "NameGoesHere"
#define USE_UNIX_TIMESTAMP 0      /**< use nasty unix seconds for timestamp */
#define TODO_EXECUTABLE 0         /**< make a todo executable (rather than list of files) */
#define TODO_TIMESTAMP_NAME 0     /**< timestamp each todo file's name? */
#define TODO_TIMESTAMP_CMNT 1     /**< timestamp within each todo file? */
#define FAKE_TIMESTAMP_MODIFIER "_i"
#define RELATIVE_LOGGING 0        /**< if 1: LOG_DIRECTORY is relative to the dgc path
				   *   if 0: LOG_DIRECTORY is absolute (so rel to current
				   *         location if it's "logs" or abs if "/logs" */
#define LOG_DIRECTORY "/tmp/logs" /**< base path of where to log, from dgc.  no "/" on end */
#define SYMLINK_NAME  "recent"    /**< The name of the symlink that will be created to the
				   * most recent timestamp */
#define MIN_BLOCKS       10000    /**< min blocks left on disk before we stop logging */
#define DGCDIRFILE "dgcpath.txt"
#define LOG_DESTINATION ""        /**< can set this to be $1 for use in scripts */
#define SPACE_BEFORE_TIMEDIR 1
#define DONT_SCARE_PEOPLE    1    /**< 0 or 1; don't print out the make_timber_link thing
				   * failed, because it scares people for no reason. */

/** contains all the info we need to be able to define the enum, module name strings
 * and default debug levels used by timber.
 *
 * Fields:
 * name (used for directory name and enum name), short name (faster to type), default level */
#define TIMBER_LOG_INFO(_) \
_(astate, as, 2) \
_(adrive, ad, 2) \
_(rddfPathGen, rpg, 2) \
_(ladarFeeder, lf, 2) \
_(stereoFeeder, sf, 2) \
_(reactivePlanner, rp, 2) \
_(superCon, sc, 2) \
_(trajFollower, tf, 2) \
_(trajSelector, ts, 2) \
_(trajChecker, tc, 2) \
_(videoRecorder, vr, 2) \
_(roadFinding, rf, 2) \
_(DBS, db, 2)

/** create the enum */
#define TIMBER_LOG_ENUMS(name, shortname, default_level) name,
namespace timber_types
{
  enum types
    {
      TIMBER_LOG_INFO(TIMBER_LOG_ENUMS)
      LAST
    };
}

/** use to initialize all the module name strings */
#define MAPNAME module_string_map
#define TIMBER_LOG_STRINGMAP(name, shortname,  default_level) MAPNAME[timber_types::name] = #name;
#define TIMBER_MAP_STR2NUM_LONG_INIT(name, shortname, default_level) MAPNAME[#name] = timber_types::name;
#define TIMBER_MAP_STR2NUM_SHORT_INIT(name, shortname, default_level) MAPNAME[#shortname] = timber_types::name;

#define LEVEL_ARRAY_NAME debug_level_array
#define SET_DEFAULT_LOG_LEVELS(name, shortname, default_level) LEVEL_ARRAY_NAME[timber_types::name] = default_level;
#define INITIALIZE_LOG_LEVELS(name, shortname, default_level) LEVEL_ARRAY_NAME[timber_types::name] = 0;

#define PRINT_INDIVIDUAL_STATUS(name, shortname, default_level) << endl << OUTPUT \
<< setw(21) << #name << setw(9) << ((string)"(" + #shortname + (string)")   ") \
<< LEVEL_ARRAY_NAME[timber_types::name]


#define MAX_TIMESTAMP_LENGTH 32
#define MAX_MODULENAME_LENGTH 32
#define MAX_INPUT_LENGTH 256
#define MAX_PATH_LENGTH 128
#define MAX_COMMAND_LENGTH 256
#define MESSAGE_LENGTH 256       /**< make >= 2*MAX_PATH_LENGTH */

#define TAGLENGTH 6
#define TAG_LOGTIMEON  "Slto  "  /**< server logging time, logging is on,
				  * individual logging on/off data */
#define TAG_LOGOFF     "S  f  "  /**< server logging is off */
#define TAG_PLAYINFO   "Splinf"  /**< server playback info (unsigned long long, 
				  unsigned long long, double) */
#define TAG_ADDFILE    "Caf   "  /**< client add file */
#define TAG_ADDFOLDER  "Cad   "  /**< client add folder */
#define CMD_ADDFILE    "scp"
#define CMD_ADDFOLDER  "scp -r"
#define CMD_MKDIR      "mkdir -p -m g+rw"
#define CMD_MKLINK     "./make_timber_link.sh"
#define PROMPT         "> "
#define OUTPUT         "   * "


#endif //TIMBER_CONFIG
