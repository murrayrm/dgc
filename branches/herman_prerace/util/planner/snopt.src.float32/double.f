*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*     
*     File  double.f 
*
*     s1eps    s1flmx   s1flmn
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      real   function s1eps ( )
      implicit           real (a-h,o-z)
*
*     Compute the machine precision.
*     IEEE Floating point real. 
*

      nbase  = 2
      ndigit = 24
      base   = nbase
      u      = base**(- ndigit)
      s1eps  = 2.0d+0*u

*     end of s1flmx
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      real   function s1flmx( )
      implicit           real (a-h,o-z)
*
*     IEEE Floating point real. 
*
      s1flmx = 1.7977d+36

*     end of s1flmx
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      real   function s1flmn( )
      implicit           real (a-h,o-z)
*
*     IEEE Floating point real. 
*
      s1flmn = 2.2251d-36

*     end of s1flmx
      end

