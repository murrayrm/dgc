#include "CCostFuser.hh"

CCostFuser::CCostFuser(CMapPlus* mapOutput, int layerNumFinalCost,
											 CMapPlus* mapRDDF, int layerNumRDDF,
											 CMapPlus* mapChangesA, CMapPlus* mapChangesB,
											 CCostFuserOptions* options, CCorridorPainterOptions* corridorOptions) {
	_options = options;
	_corridorOptions = corridorOptions;

	_mapOutput = mapOutput;
	_layerNumOutputCost = layerNumFinalCost;

	_mapRDDF = mapRDDF;
	_layerNumRDDF = layerNumRDDF;

	_layerNumCombinedElevCost = _mapOutput->addLayer<double>(CONFIG_FILE_COST, false);

	_mapChanges[0] = mapChangesA;
	_mapChanges[1] = mapChangesB;
	for(int i=0; i < 2; i++) {
		_layerNumElevChanges[i] = _mapChanges[i]->addLayer<int>(CONFIG_FILE_CHANGES, true);
		_layerNumCostChanges[i] = _mapChanges[i]->addLayer<int>(CONFIG_FILE_CHANGES, true);
		_layerNumInterpolationChanges[i] = _mapChanges[i]->addLayer<int>(CONFIG_FILE_CHANGES, true);
	}
 
	_currentProcessingMapIndex = 0;
	_currentFillingMapIndex = 1;

	DGCcreateRWLock(&_swapLock);
	DGCcreateRWLock(&_writeDeltaLock);

	_numElevLayers = 0;
	_layerNumRoad = -1;
	_layerNumSuperCon = -1;

	_numCellsFused = 0;
}


CCostFuser::~CCostFuser() {
	DGCdeleteRWLock(&_swapLock);
	DGCdeleteRWLock(&_writeDeltaLock);
}


int CCostFuser::addLayerElevCost(CMapPlus* inputMap, int layerNumElevCost, double relWeight,
																 CMapPlus* elevMap, int layerNumElev) {
	if(_numElevLayers==MAX_INPUT_COST_LAYERS) {
		return ERROR;
	}

	_mapPtrsElevCost[_numElevLayers] = inputMap;
	_layerNumsElevCost[_numElevLayers] = layerNumElevCost;
	_relWeights[_numElevLayers] = relWeight;

	_mapPtrsElev[_numElevLayers] = elevMap;
	_layerNumsElev[_numElevLayers] = layerNumElev;

	_numElevLayers++;

	return _numElevLayers-1;
}


CCostFuser::STATUS CCostFuser::addLayerRoad(CMapPlus* inputMap, int layerNumRoad) {
	_mapRoad = inputMap;
	_layerNumRoad = layerNumRoad;

	return OK;
}


CCostFuser::STATUS CCostFuser::addLayerSuperCon(CMapPlus* inputMap, int layerNumSuperCon) {
	_mapSuperCon = inputMap;
	_layerNumSuperCon = layerNumSuperCon;

	return OK;
}


unsigned long long CCostFuser::markChangesCost(NEcoord point) {
// 	return markChangesCost(point, false);
// }

	unsigned long long waitTime;

// CCostFuser::STATUS CCostFuser::markChangesCost(NEcoord point, bool alreadyLocked) {
// 	cout << __FILE__ << "[" << __LINE__ << "]: "
// 			 << "we get ere" << endl;
// 	if(!alreadyLocked)
//	DGCreadlockRWLock(&_swapLock, &waitTime);
// 	cout << __FILE__ << "[" << __LINE__ << "]: "
// 			 << "we got here too" << endl;

	_mapChanges[_currentFillingMapIndex]->setDataUTM_Delta<int>(_layerNumCostChanges[_currentFillingMapIndex], point.N, point.E, 1, &_writeDeltaLock);			

// 	cout << __FILE__ << "[" << __LINE__ << "]: "
// 			 << "filling in to the cfmi=" << _currentFillingMapIndex << endl;

	int currentRow, currentCol, maxRow, minRow, maxCol, minCol;
	
	_mapChanges[_currentFillingMapIndex]->UTM2Win(point.N, point.E, &currentRow, &currentCol);
	
	//Now we do some interpolation
  _mapChanges[_currentFillingMapIndex]->constrainRows(currentRow, _options->maxCellsToInterpolate, minRow, maxRow);
  _mapChanges[_currentFillingMapIndex]->constrainCols(currentCol, _options->maxCellsToInterpolate, minCol, maxCol);

  for(int r=minRow; r<=maxRow; r++) {
    for(int c=minCol; c<=maxCol; c++) {
			_mapChanges[_currentFillingMapIndex]->setDataWin_Delta<int>(_layerNumInterpolationChanges[_currentFillingMapIndex], r, c, 1, &_writeDeltaLock);
    }
  }

// 	if(!alreadyLocked)
//	DGCunlockRWLock(&_swapLock);
	
	return waitTime;
}


unsigned long long CCostFuser::markChangesTerrain(int layerIndex, NEcoord point) {
// 	cout << __FILE__ << "[" << __LINE__ << "]: "
// 			 << "yo hre" << endl;
	unsigned long long waitTime;

	//DGCreadlockRWLock(&_swapLock, &waitTime);
// 	cout << __FILE__ << "[" << __LINE__ << "]: "
// 			 << "writing to " << _currentFillingMapIndex << endl;
	

	if(1 &&(_mapPtrsElevCost[layerIndex]->getResRows() > _mapOutput->getResRows() ||
					_mapPtrsElevCost[layerIndex]->getResCols() > _mapOutput->getResCols())) {
		int numRows = 2;
		int numCols = 2;
// // 		int numRows = (int)ceil(_mapPtrsElevCost[layerIndex]->getResRows()/_mapOutput->getResRows());
// // 		int numCols = (int)ceil(_mapPtrsElevCost[layerIndex]->getResCols()/_mapOutput->getResCols());
		int currentRow, currentCol;
		NEcoord tempPoint;
		_mapChanges[_currentFillingMapIndex]->UTM2Win(point.N, point.E, &currentRow, &currentCol);
		for(int r = currentRow; r < currentRow + numRows; r++) {
			for(int c = currentCol; c < currentCol + numCols; c++) {
				_mapChanges[_currentFillingMapIndex]->Win2UTM(r, c, &tempPoint.N, &tempPoint.E);
				_mapChanges[_currentFillingMapIndex]->setDataUTM_Delta<int>(_layerNumElevChanges[_currentFillingMapIndex], tempPoint.N, tempPoint.E, 1);
				//_mapChanges[_currentFillingMapIndex]->setDataUTM_Delta<int>(_layerNumElevChanges[_currentFillingMapIndex], point.N, point.E, 1);
				waitTime+=markChangesCost(tempPoint);
			}
		}
	} else {
		_mapChanges[_currentFillingMapIndex]->setDataUTM_Delta<int>(_layerNumElevChanges[_currentFillingMapIndex], point.N, point.E, 1);			
		waitTime+=markChangesCost(point);
	}

	//DGCunlockRWLock(&_swapLock);

	//return OK;
	return waitTime;
}


unsigned long long CCostFuser::fuseChangesCost(NEcoord newMapCenter) {
	unsigned long long waitTime;
	DGCwritelockRWLock(&_swapLock, &waitTime);
	//cout << "old  process=" << _currentProcessingMapIndex << ", filling=" << _currentFillingMapIndex << endl;
	_mapChanges[_currentProcessingMapIndex]->updateVehicleLoc(newMapCenter.N, newMapCenter.E);
	int temp = _currentFillingMapIndex;
	_currentFillingMapIndex = _currentProcessingMapIndex;
	_currentProcessingMapIndex = temp;
	//cout << "new process=" << _currentProcessingMapIndex << ", filling=" << _currentFillingMapIndex << endl << endl;
	DGCunlockRWLock(&_swapLock);

	//return OK;

	fuseChangesTerrain();

	NEcoord point;
	int numCells = _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaSize(_layerNumCostChanges[_currentProcessingMapIndex]);
	
// 	cout << __FILE__ << "[" << __LINE__ << "]: "
// 			 << "got cells=" << numCells << " from cpmi=" << _currentProcessingMapIndex << endl;


	for(int i=0; i < numCells; i++) {
		_mapChanges[_currentProcessingMapIndex]->getCurrentDeltaVal<int>(i, _layerNumCostChanges[_currentProcessingMapIndex], &point.N, &point.E);
		fuseCellCost(point);
	}

	_numCellsFused = numCells;

	_mapChanges[_currentProcessingMapIndex]->resetDelta<int>(_layerNumCostChanges[_currentProcessingMapIndex]);

	interpolateChanges();

	return waitTime;
	//return OK;
}


CCostFuser::STATUS CCostFuser::fuseChangesTerrain() {
	NEcoord point;
	int numCells = _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaSize(_layerNumElevChanges[_currentProcessingMapIndex]);

	for(int i=0; i < numCells; i++) {
		_mapChanges[_currentProcessingMapIndex]->getCurrentDeltaVal<int>(i, _layerNumElevChanges[_currentProcessingMapIndex], &point.N, &point.E);
		fuseCellTerrain(point);
	}

	_mapChanges[_currentProcessingMapIndex]->resetDelta<int>(_layerNumElevChanges[_currentProcessingMapIndex]);

	return OK;
}


CCostFuser::STATUS CCostFuser::interpolateChanges() {
	NEcoord point;
	int numCells = _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaSize(_layerNumInterpolationChanges[_currentProcessingMapIndex]);

	for(int i=0; i < numCells; i++) {
		_mapChanges[_currentProcessingMapIndex]->getCurrentDeltaVal<int>(i, _layerNumInterpolationChanges[_currentProcessingMapIndex], &point.N, &point.E);
		interpolateCell(point);
	}

	_mapChanges[_currentProcessingMapIndex]->resetDelta<int>(_layerNumInterpolationChanges[_currentProcessingMapIndex]);

	return OK;
}


CCostFuser::STATUS CCostFuser::fuseCellTerrain(NEcoord point) {
	double totalCost = 0.0;
	double layerCost;
	double numLayersUsed = 0.0;
	double obstacleWeight = 1.0;
	double numPoints;
	int setting=0;

	for(int i=0; i < _numElevLayers; i++) {
		layerCost = _mapPtrsElevCost[i]->getDataUTM<double>(_layerNumsElevCost[i], point.N, point.E);
		if(layerCost < MIN_SPEED && layerCost > _mapPtrsElevCost[i]->getLayerNoDataVal<double>(_layerNumsElevCost[i])) {
			totalCost = _mapPtrsElevCost[i]->getLayerNoDataVal<double>(_layerNumsElevCost[i]) + EPSILON_SPEED;
			numLayersUsed = 1;
			//cout << "hit it at cell " << point.N << ", " << point.E << endl;
			//setting = 1;
			break;
		} else if(layerCost != _mapPtrsElevCost[i]->getLayerNoDataVal<double>(_layerNumsElevCost[i])) {
			numPoints = _mapPtrsElev[i]->getDataUTM<CElevationFuser>(_layerNumsElev[i], point.N, point.E).getNumPoints();
			obstacleWeight = (layerCost < 1.0) ? 10.0 : 1.0;
			obstacleWeight = (layerCost < 1.0 && _relWeights[i] > 0.8) ? 1000.0 : 1.0;
			totalCost += layerCost*_relWeights[i]*obstacleWeight*numPoints;
			numLayersUsed+=_relWeights[i]*obstacleWeight*numPoints;
			//cout << "layer " << i << " has relweight " << _relWeights[i] << endl;
			//cout << "mins is " << MIN_SPEED << " and nod ios " << _mapPtrsElevCost[i]->getLayerNoDataVal<double>(_layerNumsElevCost[i]) << endl;
		}
	}
	if(numLayersUsed != 0){
	
	_mapOutput->setDataUTM<double>(_layerNumCombinedElevCost, point.N, point.E, totalCost/(numLayersUsed));
	}
	return OK;
}


CCostFuser::STATUS CCostFuser::fuseCellCost(NEcoord point) {
	double finalSpeed = 0.0;
	double speedNoDataVal = _mapRDDF->getLayerNoDataVal<double>(_layerNumRDDF);
	double rddfSpeed = _mapRDDF->getDataUTM<double>(_layerNumRDDF, point.N, point.E);
	double superConSpeed = _mapSuperCon->getDataUTM<double>(_layerNumSuperCon, point.N, point.E);
	double superConNoDataVal = _mapSuperCon->getLayerNoDataVal<double>(_layerNumSuperCon);

	if(rddfSpeed != speedNoDataVal) {
// 	   if(isnan(finalSpeed )){
// 	     // cout << "wtf1" << endl;
// 		    }
		   
		finalSpeed = _mapOutput->getDataUTM<double>(_layerNumCombinedElevCost, point.N, point.E);
		double roadSpeed = _mapRoad->getDataUTM<double>(_layerNumRoad, point.N, point.E);
		double roadNoDataSpeed = _mapRoad->getLayerNoDataVal<double> (_layerNumRoad);
		double terrainCostNoDataVal = _mapOutput->getLayerNoDataVal<double>(_layerNumCombinedElevCost);
		
		
		if(superConSpeed == superConNoDataVal){

		  if(finalSpeed != terrainCostNoDataVal) {
// 		    if(isnan(finalSpeed )){
// 		      // cout << "wtf2" << endl << endl;;
// 		    }
		   
		    if( finalSpeed > 1.0) {
		    paintRoadCell(&finalSpeed,&roadSpeed,NULL,&roadNoDataSpeed);
		    //		    cout << "doing awesome hack stuff" << endl;
				} else if(finalSpeed < MIN_SPEED && finalSpeed > speedNoDataVal) {
					finalSpeed = speedNoDataVal;
				}

				finalSpeed  = (rddfSpeed > 0) ?
					finalSpeed = fmin(finalSpeed, rddfSpeed) :
					finalSpeed = fmin(finalSpeed, -1.0*rddfSpeed);

				//And of course, we don't want a speed higher than our max speed
				finalSpeed = fmin(finalSpeed, _options->maxSpeed);
				//And we want the larger of speeds from above and the min speed of the map
				finalSpeed = fmax(finalSpeed, speedNoDataVal + EPSILON_SPEED);//MIN_SPEED + EPSILON_SPEED);
				
				_mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E,
																						 finalSpeed);					
		  } else if(_options->paintRoadWithoutElevation) {
				//if(roadSpeed != roadNoDataSpeed)
				
				//finalSpeed = roadSpeed;
				if(roadSpeed != roadNoDataSpeed){
				  finalSpeed = -3.0;
				
				finalSpeed  = (rddfSpeed > 0) ?
					finalSpeed = fmin(finalSpeed, rddfSpeed) :
					finalSpeed = fmin(finalSpeed, -1.0*rddfSpeed);				

				_mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E,
																						 finalSpeed);
				}
		  }
		}
		else{
		  _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, superConSpeed);
		}
	}

	return OK;
}


CCostFuser::STATUS CCostFuser::interpolateCell(NEcoord point) {
	//int currentRow, currentCol, maxRow, minRow, maxCol, minCol;
	int r, c;

	_mapOutput->UTM2Win(point.N, point.E, &r, &c);

	double currentCellCost = _mapOutput->getDataWin<double>(_layerNumCombinedElevCost, r, c);
	double speedNoDataVal = _mapOutput->getLayerNoDataVal<double>(_layerNumCombinedElevCost);
	double rddfSpeed = _mapRDDF->getDataUTM<double>(_layerNumRDDF, point.N, point.E);

	if(currentCellCost == speedNoDataVal &&
		 rddfSpeed != speedNoDataVal) {
		
		int minLocalRow, maxLocalRow, minLocalCol, maxLocalCol;
		double localCellCost;
		//CElevationFuser localCellFused;
		//double localNorthing, localEasting;

		_mapOutput->constrainRows(r, 1, minLocalRow, maxLocalRow);
		_mapOutput->constrainCols(c, 1, minLocalCol, maxLocalCol);
		double localCost=0.0;
		int numSupporters=0;
		//_mapOutput->Win2UTM(r, c, &localNorthing, &localEasting);
		for(int localR=minLocalRow; localR<=maxLocalRow; localR++) {
			for(int localC=minLocalCol; localC<=maxLocalCol; localC++) {
				localCellCost = _mapOutput->getDataWin<double>(_layerNumCombinedElevCost, localR, localC);
				//localCellFused = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, localR, localC);
				if(localCellCost != speedNoDataVal) {
					localCost+=localCellCost;
					numSupporters++;
				}
			}
		}

		if(numSupporters >= _options->minNumSupportingCellsForInterpolation) {
			double finalSpeed = localCost/((double)numSupporters);
		double roadSpeed = _mapRoad->getDataUTM<double>(_layerNumRoad, point.N, point.E);
		double roadNoDataSpeed = _mapRoad->getLayerNoDataVal<double> (_layerNumRoad);
			
			paintRoadCell(&finalSpeed,&roadSpeed,NULL,&roadNoDataSpeed);

			finalSpeed  = (rddfSpeed > 0) ?
				finalSpeed = fmin(finalSpeed, rddfSpeed) :
				finalSpeed = fmin(finalSpeed, -1.0*rddfSpeed);

			//And of course, we don't want a speed higher than our max speed
			finalSpeed = fmin(finalSpeed, _options->maxSpeed);
			//And we want the larger of speeds from above and the min speed of the map
			finalSpeed = fmax(finalSpeed, MIN_SPEED + EPSILON_SPEED);
			_mapOutput->setDataWin_Delta<double>(_layerNumOutputCost, r, c, finalSpeed);
		}
	}
  

	return OK;
}


void CCostFuser::paintRoadCell(double* terrainSpeed, double* roadSpeed, double* roadScaling, double* noData) {
  if(*roadSpeed != *noData){
    if(*terrainSpeed <= *roadSpeed){
      *terrainSpeed += (*roadSpeed-((*terrainSpeed-*roadSpeed/2)*(*terrainSpeed-*roadSpeed/2))*(4/ *roadSpeed))*.3;
    }
    else{
      //			cout << "got a road no data, painting " << *terrainSpeed << endl;
    }
  }
}


int CCostFuser::getNumCellsFused() {
	return _numCellsFused;
}


CCostFuser::STATUS CCostFuser::markChangesCostAll() {
	NEcoord point;
	for(int r=0; r < _mapOutput->getNumRows(); r++) {
		for(int c=0; c < _mapOutput->getNumCols(); c++) {
			_mapOutput->Win2UTM(r, c, &point.N, &point.E);
			markChangesCost(point);
			
		}
	}

	return OK;
}


CCostFuser::STATUS CCostFuser::clearBuffers() {
	DGCwritelockRWLock(&_swapLock);
	for(int i=0; i<2; i++)
		_mapChanges[i]->clearMap();
	DGCunlockRWLock(&_swapLock);

	return OK;
}
