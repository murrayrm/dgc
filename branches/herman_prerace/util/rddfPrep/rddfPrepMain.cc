#include <iostream>
#include <iomanip>
#include "CRddfPrep.hh"

#define INPUT_FILE  RDDF_FILE
#define OUTPUT_FILE RDDF_FILE
//#define OUTPUT_FILE "new_rddf.rddf"

using namespace std;


int main(int argc, char** argv)
{
  int skynet_key;
  char* p_skynet_key = getenv("SKYNET_KEY");
  if ( p_skynet_key == NULL )
    {
      cout << "You need to set a skynet key!" << endl;
      return 1;
    }
  else
    {
      skynet_key = atoi(p_skynet_key);
      cout << "Using skynet key: " << skynet_key << endl;
    }


  CRddfPrep obj(skynet_key, INPUT_FILE);

  cout << endl;
  if (obj.checkRDDFHasBeenPrepended())
    cout << "RDDF has already been prepended, old prepended point will be overwritten." << endl;
  else
    cout << "RDDF has not yet been modified, prepending our current location." << endl;

  int old_num_points = obj.getNumTargetPoints();
  double old_total_length = obj.getWaypointDistFromStart(old_num_points - 1);
  double old_first_cor_length = obj.getWaypointDistFromStart(1);

  obj.prependRDDFWithCurrentState();

  int new_num_points = obj.getNumTargetPoints();
  double new_total_length = obj.getWaypointDistFromStart(new_num_points - 1);
  double new_first_cor_length = obj.getWaypointDistFromStart(1);

  cout << "\nsanity check" << endl
       << endl
       << "original    points in RDDF:  " << old_num_points << endl
       << "              total length:  " << old_total_length << endl
       << "          first seg length:  " << old_first_cor_length << endl
       << endl
       << "new         points in RDDF:  " << new_num_points << endl
       << "              total length:  " << new_total_length << endl
       << "          first seg length:  " << new_first_cor_length << endl;


  cout << "Writing new RDDF file...";
  cout.flush();
  obj.createRDDFFile(OUTPUT_FILE);
  cout << endl;

  return 0;
}
