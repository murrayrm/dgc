#include "stereoProcess.hh"
#include <fstream>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cv.h>
#include <highgui.h>
#include "solpos00.h"
#include "ggis.h"

stereoProcess::stereoProcess() {
  y_off_add=0;
  exp_off_add=0;
  _pairIndex = -1;
  memset(_currentFilename, 0, 256);
  memset(_currentFileType, 0, 10);

  showingDisp = FALSE;
  showingRectLeft = FALSE;
  showingRectRight = FALSE;

  DGCcreateMutex(&_paramsMutex);  
  for(int i=0; i<5; ++i)
    pitch_oldies.push_back(0.0);
  top=0;

}


stereoProcess::~stereoProcess() {
  delete[] imageObject->disparity;
}


int stereoProcess::init(int verboseLevel, char* SVSCalFilename, char* SVSParamsFilename, char* CamParamsFilename, 
			char *baseFilename, char *baseFileType, int num, char* sunParamsFilename, char* cutOffsParamsFilename) {
  DGClockMutex(&_paramsMutex);
  _verboseLevel = verboseLevel;
  memcpy(_currentFilename, baseFilename, strlen(baseFilename));
  memcpy(_currentFileType, baseFileType, strlen(baseFileType));
  _pairIndex = num;

  //Initialize the SVS variables
  videoObject = new svsStoredImages();
  videoObject_subw = new svsStoredImages(); // for subwindowing

  processObject = new svsMultiProcess();

  //Load the internal calibration data
  FILE *svsCalFile = NULL;
  svsCalFile = fopen(SVSCalFilename, "r");
  if(svsCalFile != NULL) {
    fclose(svsCalFile);
    videoObject->ReadParams(SVSCalFilename);
    videoObject->Start();
    videoObject_subw->ReadParams(SVSCalFilename);
    //videoObject_subw->Start();
  } else {
    return stereoProcess_NO_FILE;
  }

  //Load the SVS parameter data
  //Variables for getting the SVS parameters
  int corrsize, thresh, ndisp, offx, svsUnique, multi;
 
  //Variables for resizing the image
  int width, height;
  //Variables for using a sub-window of the resized image
  int sub_window_x_offset, sub_window_y_offset, sub_window_width, sub_window_height, use_sub_window_temp;
  bool use_sub_window;
  FILE *svsParamsFile = NULL;

  svsParamsFile = fopen(SVSParamsFilename, "r");
  
  if(svsParamsFile != NULL) {
    fscanf(svsParamsFile, "corrsize: %d\nthresh: %d\nndisp: %d\noffx: %d\nunique: %d\nwidth: %d\nheight: %d\nuse_sub_window: %d\nsub_window_x_offset: %d\nsub_window_y_offset: %d\nsub_window_width: %d\nsub_window_height: %d\nmulti: %d\nblobActive: %d\nblobTresh: %d\nblobDispTol: %d", &corrsize, &thresh, &ndisp, &offx, &svsUnique, &width, &height, &use_sub_window_temp, &sub_window_x_offset, &sub_window_y_offset, &sub_window_width, &sub_window_height, &multi, &m_blobActive, &m_blobTresh, &m_blobDispTol);

    normal_y_off= sub_window_y_offset;
    FILE *sunParamsFile = NULL;
    sunParamsFile =fopen(sunParamsFilename, "r");
    if(sunParamsFile != NULL) {
      fscanf(sunParamsFile, "sunDetection: %d\naspectWidth: %d\ntiltWidth: %d\nfov_horizontal: %d\nfov_vertical: %d\npitchActive: %d",&m_sunDetection, &m_aspectWidth, &m_tiltWidth, &fov_h, &fov_v, &m_pitchActive );
      
      fov_v = fov_v /2; //most calculations below use half the field of view
      fov_h = fov_h /2;
    }
    else{
      printf("Couldn't find sunParamsFile \n");
      usleep(1000*1000*10);
    }
    
      
    if(use_sub_window_temp == 1) {
      use_sub_window = TRUE;
    } else {
      use_sub_window = FALSE;
    }
    
    videoObject->Load(0, 0, NULL, NULL, NULL, NULL, FALSE, FALSE);
    videoObject_subw->Load(0, 0, NULL, NULL, NULL, NULL, FALSE, FALSE);

    imageObject = videoObject->GetImage(1);
    
    imageObject->dp.corrsize = corrsize;
    imageObject->dp.thresh = thresh;
    imageObject->dp.ndisp = (int)ndisp;
    imageObject->dp.offx = offx;
    imageObject->dp.unique = svsUnique;

    disparityMemory = new short[width*height]; // hack in order to enable copying of disparity into this imageObject
        
    imageObject_subw = videoObject_subw->GetImage(1);

    imageObject_subw->dp.corrsize = corrsize;
    imageObject_subw->dp.thresh = thresh;
    imageObject_subw->dp.ndisp = (int)ndisp;
    imageObject_subw->dp.offx = offx;
    imageObject_subw->dp.unique = svsUnique;
    
    processObject->doIt = multi;
    
    imageSize = cvSize(width, height);

    subWindow = cvRect(sub_window_x_offset, sub_window_y_offset, sub_window_width, sub_window_height);

    fclose(svsParamsFile);
  }  
  else {
    return stereoSource_NO_FILE;
  }
  
  //Initialize the frame variable
  double cameraRoll, cameraPitch, cameraYaw;
  XYZcoord cameraOffset(0, 0, 0);
  FILE *camParamsFile = NULL;

  camParamsFile = fopen(CamParamsFilename, "r");

  if(camParamsFile != NULL) {
    fscanf(camParamsFile, "x: %lf\ny: %lf\nz: %lf\npitch: %lf\nroll: %lf\nyaw: %lf", &cameraOffset.X, &cameraOffset.Y, &cameraOffset.Z, &cameraPitch, &cameraRoll, &cameraYaw);
    
    //cameraFrame.initFrames(cameraOffset, cameraPitch, cameraRoll, cameraYaw);
    camera2Body = Matrix(4,4);
    body2Nav = Matrix(4,4);
    camera2Nav = Matrix(4,4);
    m_PointCloud = Matrix(4,sub_window_width*sub_window_height);
    m_UTMPointCloud = Matrix(4,sub_window_width*sub_window_height);
    
    makeTransform(camera2Body, cameraRoll, cameraPitch, cameraYaw, cameraOffset.X, cameraOffset.Y, cameraOffset.Z); 
    
    fclose(camParamsFile);
  } else {
    return stereoSource_NO_FILE;
  }
  m_nbrUTMPoints = 0;
  m_UTMPointCounter = 0; 

  FILE *cutOffsParamsFile = NULL;
  cutOffsParamsFile = fopen(cutOffsParamsFilename, "r");
  if(cutOffsParamsFile != NULL) {
    fscanf(cutOffsParamsFile, "MinRange: %lf\nMaxRange: %lf\nMaxRelativeAltitude: %lf\nMinRelativeAltitude: %lf", &m_PointMinRange, &m_PointMaxRange, &m_PointMaxRelativeAltitude, &m_PointMinRelativeAltitude);
    fclose(cutOffsParamsFile);
  }
  else {
    return stereoSource_NO_FILE;
  }

  for(int i=0; i < MAX_CAMERAS; i++) {
    tempImages[i] = cvCreateImage(imageSize, IPL_DEPTH_8U, 1);
    stereoImages[i] = cvCreateImage(imageSize, IPL_DEPTH_8U, 1);
  }
  
  DGCunlockMutex(&_paramsMutex);

  return stereoSource_OK;
}


int stereoProcess::loadPair(stereoPair pair, VehicleState state) {
  return loadPair(pair.left, pair.right, state);
}

int stereoProcess::loadPair(unsigned char *left, unsigned char *right, VehicleState state) {
  cvSetData(tempImages[LEFT_CAM], (char *)left, imageSize.width);
  cvSetData(tempImages[RIGHT_CAM], (char *)right, imageSize.width);

  for(int i=0; i<MAX_CAMERAS; i++) {
    cvCopy(tempImages[i], stereoImages[i]);
  }

  _currentState = state;


  /* detect and cover the sun if it might distroy the diparity image */
  if (m_sunDetection == 1){
    struct tm * thisTm;
    //time_t tt;
    //time(&tt);
    time_t tt = (_currentState.Timestamp)/1000000;
    thisTm = localtime(&tt);
    
    struct posdata pd, *pdat;
    long retval;
    pdat = &pd;
    S_init(pdat);
    
    GisCoordUTM utm;
    GisCoordLatLon latlon;
    utm.zone =11;
    utm.letter = 'S';
    utm.e = _currentState.Easting;
    utm.n = _currentState.Northing;
    gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
    
    pdat->longitude = latlon.longitude;
    pdat->latitude = latlon.latitude;
    pdat->timezone = -8.0;
    
    pdat->function&=~S_DOY;
    pdat->year      = 1900 + thisTm->tm_year;  
    pdat->month    =  1 + thisTm->tm_mon;  
    pdat->day      = thisTm->tm_mday;
    pdat->hour      =  thisTm->tm_hour;
    pdat->minute    = thisTm->tm_min;
    pdat->second    = thisTm->tm_sec;
    
    pdat->tilt = _currentState.Pitch*(180.0/M_PI); //degrees
    pdat->aspect = _currentState.Yaw*(180.0/M_PI);
    
    pdat->temp      =   27.0;    // ????
    pdat->press     = 1006.0;     //???
    
    
    int pixels_y = 0;
    int pixels_x = 0;          // nbr of pixels to set to black
    int x_start = -1;
    int y_start = -1;
    int x_end = 0;
    int y_end = 0;
    
    retval = S_solpos (pdat); 
    S_decode(retval, pdat);    
    
    float sun_aspect = pdat->azim - pdat->aspect;   //aspect relative to Alice
    float sun_tilt = pdat->elevref - pdat->tilt;    //tilt relative to Alice
    
    int pixel_width = 640;
    int pixel_height = 480;
    
    if(((abs((int)sun_tilt) - m_tiltWidth)<fov_v) && (pdat->elevref)>=0 ){ 
      /*if anything at all should be set to black
	sun is outside defined region or below the horizontal line*/
      
      if((abs((int)sun_tilt) + m_tiltWidth) >fov_v){  //reduced area to paint black
	if(abs((int)sun_tilt) >fov_v){       //sun not in image
	  pixels_y = (int) (m_tiltWidth - abs((int)sun_tilt) + fov_v)*(pixel_height/(2*fov_v));
	}
	else{
	  pixels_y = (int) ((2*m_tiltWidth - (abs((int)sun_tilt) + m_tiltWidth - fov_v ))*(pixel_height/(2*fov_v)));
	}
	if(sun_tilt<0.0){    //sun above the cameras "horizontal line"
	  y_start = pixel_height -1 -pixels_y;
	  y_end = pixel_height-1;
	}
	else{
	  y_start = 0;
	  y_end = pixels_y;
	}
      } 
      else{      //max region to paint black
	pixels_y = (int) (m_tiltWidth*(pixel_height/(2*fov_v)));
	y_start = (pixel_height/2)-1 - (int) (sun_tilt + m_tiltWidth)*(pixel_height/(2*fov_v));
	y_end = y_start + pixels_y;
      }
    }
    if(sun_aspect > 180.0){   //sun on left side of the cameras
      sun_aspect = sun_aspect - 360.0;
    }
    else if(sun_aspect < -180.0){
      sun_aspect = sun_aspect + 360.0;
    }
    if((abs((int) sun_aspect) - m_aspectWidth ) < fov_h){   //when to paint black at all
      if((abs( (int) sun_aspect) + m_aspectWidth ) > fov_h){  //reduced area to paint black
	if(abs( (int) sun_aspect ) > fov_h){                 //sun not in image
	  pixels_x = (int) (m_aspectWidth - abs((int)sun_aspect) + fov_h)*(pixel_width/(2*fov_h)); 
	}
	else{
	  pixels_x = (int) ((2*m_aspectWidth - (abs((int)sun_aspect) + m_aspectWidth - fov_h))*(pixel_width/(2*fov_h)));
	}
	if(sun_aspect > 0.0){    //sun on right side
	  x_start = pixel_width -1 - pixels_x;
	  x_end = pixel_width -1;
	}
	else {            //sun on left side
	  x_start = 0;
	  x_end = pixels_x;
	}
	
      }
      else{            //max region to paint black
	pixels_x = (int) (m_aspectWidth*(pixel_width/(2.0*fov_h)));
	x_start = (pixel_width/2)-1 + (int) (sun_aspect - m_aspectWidth)*(pixel_width/(2*fov_h));
	x_end = x_start + pixels_x;
      }
    }
    
    if((x_start !=-1) && (y_start!=-1)){    //cover the sun!!
      if( y_start <=0)
	y_start=1;
      if( y_end >(pixel_height-1))
	y_end =pixel_height -1;
      int x_value;
      for(int m= 0; m <pixels_x; ++m){
	x_value = x_start + m;
	if(x_value <=0)
	  x_value=1;
	else if(x_value >(pixel_width-1))
	  x_value=pixel_width -1;
	cvLine(stereoImages[LEFT_CAM], cvPoint(x_value, y_start), cvPoint(x_value, y_end), CV_RGB(255,0,0));
      }
    }
    
  }
  
  videoObject->Load(stereoImages[LEFT_CAM]->width, stereoImages[LEFT_CAM]->height,
		    (unsigned char*) stereoImages[LEFT_CAM]->imageData,
		    (unsigned char*) stereoImages[RIGHT_CAM]->imageData,
		    NULL, NULL,
		    FALSE, FALSE);
  makeTransform(body2Nav, _currentState.Roll, _currentState.Pitch, _currentState.Yaw, _currentState.Northing, _currentState.Easting, _currentState.Altitude);
  camera2Nav = body2Nav*camera2Body;

  //NEDcoord camPos(_currentState.Northing, _currentState.Easting, _currentState.Altitude);
 
  //cameraFrame.updateState(camPos, _currentState.Pitch, _currentState.Roll, _currentState.Yaw);
  
  _pairIndex++;

  return stereoProcess_OK;
}


int stereoProcess::calcRect() {
  
  imageObject = videoObject->GetImage(1); //this line of code is beautiful

  imageObject->haveDisparity = false;

  videoObject_subw->Load(imageObject->ip.linelen, subWindow.height,
			 imageObject->Left() + imageObject->ip.linelen*subWindow.y,
			 imageObject->Right() + imageObject->ip.linelen*subWindow.y,
			 NULL, NULL,
			 true, FALSE); // true so that rectification is only performed once
  
  imageObject_subw = videoObject_subw->GetImage(1);
  
  nbrPixels = imageObject_subw->ip.linelen*imageObject_subw->ip.lines; // 307200;
  lastPixelRow = nbrPixels - imageObject_subw->ip.linelen; // 306560
  

  /*

    ofstream tempFile("/tmp/subwindow_no.txt", ios::app);
    tempFile << "linelen: " << imageObject->ip.linelen << endl;
    tempFile << "lines: " << imageObject->ip.lines << endl;
    for (int i = 0; i < imageObject->ip.linelen*imageObject->ip.lines; i++) {
    tempFile << (imageObject->Left())[i] << ", ";
    if (i%imageObject->ip.linelen == 0) 
    tempFile << endl;
    }
    tempFile.close();
    
    ofstream tempFile2("/tmp/subwindow_yes.txt", ios::app);
    tempFile2 << "linelen: " << imageObject_subw->ip.linelen << endl;
    tempFile2 << "lines: " << imageObject_subw->ip.lines << endl;
    
    for (int i = 0; i < imageObject_subw->ip.linelen*imageObject_subw->ip.lines; i++) {
    tempFile2 << (imageObject_subw->Left())[i] << ", ";
    if (i%imageObject_subw->ip.linelen == 0) 
    tempFile2 << endl;
    }
    tempFile2.close();
  */  
  /* cout << "debug message 11, imageObject_subw->ip has" << endl;
  //cout << "linelen: " << imageObject_subw->ip.linelen << endl;
  //cout << "lines: " << imageObject_subw->ip.lines << endl;
  */

   /*   svsSP *mySP;

    mySP = imageObject->GetSP();
    mySP->subwarp = TRUE;
    mySP->subwindow = TRUE;
    mySP->left_ix = subWindow.x;
    mySP->left_iy = subWindow.y;
    mySP->left_width = subWindow.width;
    mySP->left_height = subWindow.height;
    mySP->right_ix = subWindow.x;
    mySP->right_iy = subWindow.y;
    mySP->right_width = subWindow.width;
    mySP->right_height = subWindow.height;
    mySP->linelen = imageSize.width;
    mySP->lines = imageSize.height;

    imageObject->SetSP(mySP);
  */
  if(showingRectLeft) {
    fltk_check();
    rectLeftWindow->DrawImage(imageObject_subw, svsLEFT);
    if(!rectLeftWindow->visible_r()) showingRectLeft = FALSE;
  }
  if(showingRectRight) {
    fltk_check();
    rectRightWindow->DrawImage(imageObject_subw, svsRIGHT);
    if(!rectRightWindow->visible_r()) showingRectRight = FALSE;
  }

  return stereoProcess_OK;
}


int stereoProcess::calcDisp() {
  DGClockMutex(&_paramsMutex);
  
  processObject->CalcStereo(imageObject_subw);
  DGCunlockMutex(&_paramsMutex);
  
  // blob filter starts here
  if (m_blobActive == 1) {
    
    dispIm = imageObject_subw->Disparity();

    memset(c,0,sizeof(c));

    int k = 1; //current color
    vector<int> count;
    count.push_back(m_blobTresh); // don't remove color 0 == no disparity
    //ofstream tempFile("/tmp/blobflood.txt", ios::app);      
    
    for (int pixel = 0; pixel < nbrPixels; pixel++) {
      //tempFile << "pixel: " << pixel << endl;
      if (dispIm[pixel] > -1) {
	if (c[pixel] == 0) {
	  //  tempFile << "flooding pixel " << pixel << endl;
	  flood(pixel,k,dispIm);
	  k++;
	  count.push_back(1);
	}
	else {
	  count.at(c[pixel])++;
	}
      }
    }
    
    for (int pixel = 0; pixel < nbrPixels; pixel++) {
      if (count.at(c[pixel]) < m_blobTresh) {
	dispIm[pixel] = -1;
      }
    }
    //tempFile.close();
  }    

  // blob filter ends here


  
  // put the disparity image in the original imageObject to make the 3D coordinates correct
  imageObject->disparity = disparityMemory;
  memset(imageObject->disparity, -1, imageObject->ip.linelen*imageObject->ip.lines*sizeof(imageObject->disparity[0])); 

  for (int i = 0; i < subWindow.height*imageObject->ip.linelen; i++) {
    imageObject->disparity[i+subWindow.y*imageObject->ip.linelen] = imageObject_subw->disparity[i];
  }

  imageObject->haveDisparity = true; 


  if(showingDisp) {
    fltk_check();
    dispWindow->DrawImage(imageObject_subw, svsDISPARITY);
    if(!dispWindow->visible_r()) showingDisp = FALSE;
  }

  return stereoProcess_OK;
}

  
int stereoProcess::save() {
  int saveRectStatus = saveRect();
  int saveDispStatus = saveDisp();
  if(saveRectStatus!=stereoProcess_OK) {
    return saveRectStatus;
  }
  if(saveDispStatus!=stereoProcess_OK) {
    return saveDispStatus;
  }

  return stereoProcess_OK;
}


int stereoProcess::save(char *baseFilename, char *baseFileType, int num) {
  int saveRectStatus = saveRect(baseFilename, baseFileType, num);
  int saveDispStatus = saveDisp(baseFilename, baseFileType, num);
  if(saveRectStatus!=stereoProcess_OK) {
    return saveRectStatus;
  }
  if(saveDispStatus!=stereoProcess_OK) {
    return saveDispStatus;
  }

  return stereoProcess_OK;
}


int stereoProcess::saveDisp() {
  return saveDisp(_currentFilename, _currentFileType, _pairIndex);
}


int stereoProcess::saveDisp(char *baseFilename, char *baseFileType, int num) {
  char fullFilename[256];

  sprintf(fullFilename, "%s%.4dDisp.%s", baseFilename, num, baseFileType);

  imageObject_subw->SaveDisparity(fullFilename, TRUE);

  return stereoProcess_OK;
}


int stereoProcess::saveRect() {
  return saveRect(_currentFilename, _currentFileType, _pairIndex);
}


int stereoProcess::saveRect(char *baseFilename, char *baseFileType, int num) {
  char fullFilename[256];

  sprintf(fullFilename, "%s%.4dRect", baseFilename, num);

  imageObject_subw->SaveToFile(fullFilename);
    
  return stereoProcess_OK;
}



int stereoProcess::show(int width, int height) {
  showRect(width, height);
  showDisp(width, height);
  return stereoProcess_OK;
}


int stereoProcess::showDisp(int width, int height) {
  if(width == 0) width = imageSize.width;
  if(height == 0) height = subWindow.height;

  if(!showingDisp) {
    dispWindow = new svsWindow(100,100,width,height);
    showingDisp = TRUE;
  }
  dispWindow->show();

  return stereoProcess_OK;
}


int stereoProcess::showRect(int width, int height) {
  int showRectLeftStatus = showRectLeft();
  int showRectRightStatus = showRectRight();
  if(showRectLeftStatus!=stereoProcess_OK) {
    return showRectLeftStatus;
  }
  if(showRectRightStatus!=stereoProcess_OK) {
    return showRectRightStatus;
  }
  return stereoProcess_OK;
}


int stereoProcess::showRectLeft(int width, int height) {
  if(!showingRectLeft) {
    rectLeftWindow = new svsWindow(0,0, imageSize.width, subWindow.height);
    showingRectLeft = TRUE;
  }
  rectLeftWindow->show();

  return stereoProcess_OK;
}


int stereoProcess::showRectRight(int width, int height) {
  if(!showingRectRight) {
    rectRightWindow = new svsWindow(50, 50, imageSize.width, subWindow.height);
    showingRectRight = TRUE;
  }
  rectRightWindow->show();

  return stereoProcess_OK;
}


unsigned char* stereoProcess::disp() {
  fprintf(stderr, "stereoProcess::disp() not yet implemented\n");
  return NULL;
}


unsigned char* stereoProcess::rectLeft() {
  fprintf(stderr, "stereoProcess::rectLeft() not yet implemented\n");
  return NULL;
}


unsigned char* stereoProcess::rectRight() {
  fprintf(stderr, "stereoProcess::rectRight() not yet implemented\n");
  return NULL;
}


stereoPair stereoProcess::rectPair() {
  stereoPair temp;
  temp.left = rectLeft();
  temp.right = rectRight();
  return temp;
}


int stereoProcess::numPoints() {
  return imageObject->numPoints;
}


bool stereoProcess::validPoint(int i) {
  if(i < numPoints() && i >= 0) {
    return (imageObject->pts3D[i].A > 0);
  } 
  else {
    return false;
  }
}


NEDcoord stereoProcess::UTMPoint(int i) {
#warning rewrite this function
  NEDcoord retVal(0, 0, 0);
  /* XYZcoord cameraPoint;

  if(validPoint(i)) {
    //According to the SVS documentation:
    //The camera's Z-axis is forward - so our X-axis
    cameraPoint.X = imageObject_subw->pts3D[i].Z;
    //The camera's X axis is right - so our Y-axis
    cameraPoint.Y = imageObject_subw->pts3D[i].X; 
    //The camera's Y-axis is down - so our Z-axis
    cameraPoint.Z = imageObject_subw->pts3D[i].Y;

    m_point.setelem(0,cameraPoint.X);
    m_point.setelem(1,cameraPoint.Y);
    m_point.setelem(2,cameraPoint.Z);
    m_point.setelem(3,1);
    
    m_resultPoint = camera2Nav*m_point;
    retVal.N = m_resultPoint.getelem(0);
    retVal.E = m_resultPoint.getelem(1);
    retVal.D = m_resultPoint.getelem(2);

    //retVal = cameraFrame.transformS2N(cameraPoint);
  }
  */
  return retVal;
}


XYZcoord stereoProcess::Point(int i) {
  XYZcoord retVal(0, 0, 0);

  if(validPoint(i)) {
    retVal.X = imageObject->pts3D[i].X;
    retVal.Y = imageObject->pts3D[i].Y;
    retVal.Z = imageObject->pts3D[i].Z;

  }

  return retVal;
}

bool stereoProcess::generateUTMPointCloud() {
  m_PointCloud.clear(); 
  m_UTMPointCloud.clear();
  m_nbrUTMPoints = 0;
  m_UTMPointCounter = 0; 
  /*
  // this doesn't seem to work. for some reason, numpoints is zero.
  processObject->Calc3D(imageObject, 0, 0 , 640, 480, imageObject->pts3D, NULL, 100000000); 
     
  if (imageObject->have3D) {
  ofstream tempFile("/tmp/generateUTMPointCloud.txt", ios::app);  
  tempFile << "numPoints: " << imageObject->numPoints << endl;
  for (int i=0; i<imageObject->numPoints; i++) {
    tempFile << "i: " << i << ", A: " << imageObject->pts3D[i].A << ", Z: " << imageObject->pts3D[i].Z << endl;
    if (imageObject->pts3D[i].A > 0) {
      m_PointCloud.setelem(0,m_nbrUTMPoints,imageObject->pts3D[i].Y);
      m_PointCloud.setelem(1,m_nbrUTMPoints,imageObject->pts3D[i].Z);
      m_PointCloud.setelem(2,m_nbrUTMPoints,imageObject->pts3D[i].X);
      m_PointCloud.setelem(3,m_nbrUTMPoints,1);
      m_nbrUTMPoints++;
    }
  }
  tempFile << "That was one frame! The number of UTMpoints is: " << m_nbrUTMPoints<< endl;
    tempFile.close();
    m_UTMPointCloud = camera2Nav*m_PointCloud;
  }
  else {
    return false;
  }
  return true; 
  */
  
  //temporary solution
  XYZcoord tempPoint(0, 0, 0);
  for(int y=subWindow.y; y<subWindow.y+subWindow.height; y++) {
    for(int x=subWindow.x; x<subWindow.x+subWindow.width; x++) {
      if(processObject->CalcPoint3D(x, y, imageObject, &tempPoint.Y, &tempPoint.Z, &tempPoint.X)) { // switched order because SVS uses a different coordinate system
	if (tempPoint.X < m_PointMaxRange && tempPoint.X > m_PointMinRange) { 
	m_PointCloud.setelem(m_nbrUTMPoints++,tempPoint.X);
	m_PointCloud.setelem(m_nbrUTMPoints++,tempPoint.Y);
	m_PointCloud.setelem(m_nbrUTMPoints++,tempPoint.Z);
	m_PointCloud.setelem(m_nbrUTMPoints++,1);
	}
      }
    }
  }
  //  m_nbrUTMPoints /= 4;
  m_PointCloud.setcols(m_nbrUTMPoints/4);  
  m_UTMPointCloud = camera2Nav*m_PointCloud;
  if (m_nbrUTMPoints > 0)
    return true;
  else 
    return false; 
}

bool stereoProcess::getNextUTMPoint(NEDcoord* UTMPoint) {
  bool pointOk = false;
  while (!pointOk) {
    if (m_UTMPointCounter < m_nbrUTMPoints) {
      
      UTMPoint->N = m_UTMPointCloud.getelem(m_UTMPointCounter++);
      UTMPoint->E = m_UTMPointCloud.getelem(m_UTMPointCounter++);
      UTMPoint->D = m_UTMPointCloud.getelem(m_UTMPointCounter++);    
      
      m_UTMPointCounter++; // since there are four rows
      double relativeAltitude = -UTMPoint->D + _currentState.Altitude;
      pointOk = ((relativeAltitude < m_PointMaxRelativeAltitude) && (relativeAltitude > m_PointMinRelativeAltitude));
      //cout << "relativeAltitude: " << relativeAltitude << ", pointOk: " << pointOk << endl;
    }
    else {
      return false;
    } 
  }
  return true; 
}

bool stereoProcess::SinglePoint(int x, int y, NEDcoord* resultPoint) {
#warning rewrite this function!
  
   /*    XYZcoord tempPoint(0, 0, 0);
  NEDcoord transformedPoint(0, 0, 0);
  
  if(x > 0 && x < stereoImages[0]->width &&
     y > 0 && y < stereoImages[0]->height) {
   
      //ofstream tempFile("/tmp/singlePoint.txt", ios::app);
     // tempFile << "x is " << x << " and y is " << y << endl;
    //  tempFile.close();
    
    if(processObject->CalcPoint3D(x, y, imageObject, &tempPoint.Y, &tempPoint.Z, &tempPoint.X)) {
      
      // the svs documentation seems to be incorrect! calcpoint3d returns true for invalid points, so we need to check ourselves.
      if (tempPoint.X <= 0) { //allow no points behind the cameras
	return false;
      }
      
      // this is so inefficient
      // transformedPoint = cameraFrame.transformS2N(tempPoint);
      
      m_point.setelem(0,tempPoint.X);
      m_point.setelem(1,tempPoint.Y);
      m_point.setelem(2,tempPoint.Z);
      m_point.setelem(3,1);
    
      m_resultPoint = camera2Nav*m_point;
      resultPoint->N = m_resultPoint.getelem(0);
      resultPoint->E = m_resultPoint.getelem(1);
      resultPoint->D = m_resultPoint.getelem(2);
      
      
      //(resultPoint->N) = transformedPoint.N;
      //(resultPoint->E) = transformedPoint.E;
      //(resultPoint->D) = transformedPoint.D;
      
      return true;
    }
}*/

  return false;
}


bool stereoProcess::calc3D() {
  //return processObject->Calc3D(imageObject, subWindow.x, subWindow.y, subWindow.width, subWindow.height);
  return processObject->Calc3D(imageObject);
}


bool stereoProcess::SingleRelativePoint(int x, int y, XYZcoord* resultPoint) {
#warning this function needs to be rewritten
  /*  XYZcoord tempPoint(0, 0, 0);
  XYZcoord transformedPoint(0, 0, 0);

  if(x > 0 && x < stereoImages[0]->width &&
     y > 0 && y < stereoImages[0]->height) {
    
      //ofstream tempFile("/tmp/singleRelativePoint.txt", ios::app);
     // tempFile << "x is " << x << " and y is " << y << endl;
    //  tempFile.close();
    
    if(processObject->CalcPoint3D(x, y, imageObject, &tempPoint.Y, &tempPoint.Z, &tempPoint.X)) {

      // the svs documentation seems to be incorrect! calcpoint3d returns true for invalid points, so we need to check ourselves.
      if (tempPoint.X <= 0) { //allow no points behind the cameras
	return false;
      }
      
      // this is so inefficient
      // transformedPoint = cameraFrame.transformS2B(tempPoint);
      
      m_point.setelem(0,tempPoint.X);
      m_point.setelem(1,tempPoint.Y);
      m_point.setelem(2,tempPoint.Z);
      m_point.setelem(3,1);
    
      m_resultPoint = camera2Body*m_point;
      resultPoint->X = m_resultPoint.getelem(0);
      resultPoint->Y = m_resultPoint.getelem(1);
      resultPoint->Z = m_resultPoint.getelem(2);
      
      
      //(resultPoint->N) = transformedPoint.N;
      //(resultPoint->E) = transformedPoint.E;
      //(resultPoint->D) = transformedPoint.D;
      
      
      return true;
    } 
  }*/

  return false;
}

VehicleState stereoProcess::currentState() {
  return _currentState;
}


int stereoProcess::pairIndex() {
  return _pairIndex;
}


int stereoProcess::resetPairIndex() {
  _pairIndex = 0;

  return stereoProcess_OK;
}


int stereoProcess::corrsize() {
  return imageObject_subw->dp.corrsize;
}


int stereoProcess::thresh() {
  return imageObject_subw->dp.thresh;
}


int stereoProcess::ndisp() {
  return imageObject_subw->dp.ndisp;
}


int stereoProcess::offx() {
  return imageObject_subw->dp.offx;
}


int stereoProcess::svsUnique() {
  return imageObject_subw->dp.unique;
}


int stereoProcess::multi() {
  return processObject->doIt;
}

int stereoProcess::setSVSParams(int corrsize, int thresh, int ndisp, int offx, int svsUnique, int multi, int subwindow_x_off, int subwindow_y_off, int subwindow_width, int subwindow_height) {
  DGClockMutex(&_paramsMutex);

  if (thresh < 0) { thresh = 0; }
  if (thresh > 40) { thresh = 40; }
  if (svsUnique < 0) { svsUnique = 0; }
  if (svsUnique > 40) { svsUnique = 40; }
  if (corrsize < 5) { corrsize = 5; }
  if (ndisp < 8) { ndisp = 8; }
  if (ndisp > 128) { ndisp = 128; }

  if(corrsize != -1) imageObject_subw->dp.corrsize = corrsize;
  if(thresh != -1) imageObject_subw->dp.thresh = thresh;
  if(ndisp != -1) imageObject_subw->dp.ndisp = ndisp;
  if(offx != -1) imageObject_subw->dp.offx = offx;
  if(svsUnique != -1) imageObject_subw->dp.unique = svsUnique;
  if(multi != -1) processObject->doIt = (multi!=0);

  if (subwindow_width < 0) { subwindow_width = 0; }
  if (subwindow_width > imageSize.width) { subwindow_width = imageSize.width; }
  if (subwindow_x_off < 0) { subwindow_x_off = 0; }
  if (subwindow_x_off+subwindow_width > imageSize.width) { subwindow_x_off = imageSize.width-subwindow_width; }
  if (subwindow_y_off < 0) { subwindow_y_off = 0; }
  if (subwindow_y_off+subwindow_height > imageSize.height) { subwindow_y_off = imageSize.height-subwindow_height; }

  if(subwindow_x_off != -1) subWindow.x = subwindow_x_off;
  if(subwindow_y_off != -1) subWindow.y = subwindow_y_off;
  if(subwindow_width != -1) subWindow.width = subwindow_width;
  // if(subwindow_height != -1) subWindow.height = subwindow_height; //at this point the subwindow height cannot be changed at runtime

  DGCunlockMutex(&_paramsMutex);
  return stereoProcess_OK;
}

int stereoProcess::blobActive() {
  return m_blobActive;
}

int stereoProcess::blobTresh() {
  return m_blobTresh;
}

int stereoProcess::blobDispTol() {
  return m_blobDispTol;
}

int stereoProcess::setBlobParams(int blobActive, int blobTresh, int blobDispTol) {
  m_blobActive = blobActive;
  m_blobTresh = blobTresh;
  m_blobDispTol = blobDispTol;
  return stereoProcess_OK;
}

int stereoProcess::setCutoffParams(double MinRange, double MaxRange, double MaxRelativeAltitude, double MinRelativeAltitude) {
  m_PointMinRange = MinRange;
  m_PointMaxRange = MaxRange;
  m_PointMaxRelativeAltitude = MaxRelativeAltitude;
  m_PointMinRelativeAltitude = MinRelativeAltitude;
  return stereoProcess_OK;
}

double stereoProcess::getMinRange() {
  return m_PointMinRange;
}

double stereoProcess::getMaxRange() {
  return m_PointMaxRange;
}

double stereoProcess::getMaxRelativeAltitude() {
  return m_PointMaxRelativeAltitude;
}

double stereoProcess::getMinRelativeAltitude() {
  return m_PointMinRelativeAltitude;
}

int stereoProcess::sunDetection(){
  return m_sunDetection;
}

int stereoProcess::aspectWidth(){
  return m_aspectWidth;
}

int stereoProcess::tiltWidth(){
  return m_tiltWidth;
}

int stereoProcess::setSunDetectionParams(int sunDetection, int aspectWidth, int tiltWidth){
  m_sunDetection = sunDetection;
  m_aspectWidth = aspectWidth;
  m_tiltWidth = tiltWidth;
  return stereoProcess_OK;
}

int stereoProcess::setPitchActive(int pitchActive){
  m_pitchActive = pitchActive;
  return stereoProcess_OK;
}

int stereoProcess::getPitchActive(){
  return m_pitchActive;
}

void stereoProcess::pitchControl(){
  double pitch = _currentState.Pitch*(180.0/M_PI); //degrees
  double pitchrate = _currentState.PitchRate*(180.0/M_PI);
  double old_pitch =0;
  list<double>::iterator it;
  for( it= pitch_oldies.begin(); it!=pitch_oldies.end(); it++){
    old_pitch +=  *it;
  }
  old_pitch = old_pitch / 5.0;     
  pitch_oldies.pop_front();
  pitch_oldies.push_back(pitch);
  
  /*if(old_pitch > old_pitch_max){
    old_pitch_max = old_pitch;
    }*/
  if(pitchrate >0){
    pitch= (pitchrate*old_pitch + pitch)/ (pitchrate+1);
  }
  else{
    pitch = (old_pitch - pitchrate*pitch)/(1-pitchrate);
  }
  
  if(top==1 && pitch< -1.0)
    top=0;

  if(abs((int)pitch) < 1 && top==1)
    pitch = (2 - abs((int) pitch));
  
  DGClockMutex(&_paramsMutex);
  // printf("  normal_y_off %d ", normal_y_off);
  int y_off = normal_y_off;
  int h = subWindow.height;
  
  if( m_pitchActive && pitch>1.0 ){    //change subwindow!!
    top=1;
    y_off = y_off +  (pitch*240.0)/(double)fov_v; 
    int _exposure_y_off = y_off;
    
    //printf( " wanted y_off= %d ", y_off);
    int max= 480;
    if(fov_v > 30) //short range
      max=460;

    if(y_off < 0){
      y_off = 0;
      _exposure_y_off = 0;
    }
    else if ( (y_off + h) >max ){
      if( y_off >(max-1))
	_exposure_y_off = max-1;
      else
	_exposure_y_off = y_off;
      
      y_off = max - h;  
    }
    y_off_add = y_off - normal_y_off;
    exp_off_add = _exposure_y_off - normal_y_off;
  }
  else{          // do not change subWindow.y or _exposure_y_off
    y_off_add =0;
    exp_off_add =0;
  }
  subWindow.y = normal_y_off + y_off_add;
  
  //printf("  subWindow.y= %d  y_off_add= %d  exp_off_add= %d\n", subWindow.y, y_off_add, exp_off_add);
  DGCunlockMutex(&_paramsMutex);
}

int stereoProcess::getSubwindowYOff(){
  return y_off_add;
}

int stereoProcess::getExposureYOff(){
  return exp_off_add;
}

int stereoProcess::getNormalYOff(){
  return normal_y_off;
}

int stereoProcess::setNormalYOff(int normal_off){
  normal_y_off = normal_off;
  return stereoProcess_OK;
}

void stereoProcess::flood(int pixel, int k, short* dispIm) {
  //ofstream tempFile("/tmp/blobflood.txt", ios::app);
  deque<int> q;
  c[pixel] = k;
  q.push_front(pixel);
  //tempFile << "push_front of pixel " << pixel << endl;
  
  while(!q.empty()) {
    pixel = q.front();
    q.pop_front();
    //tempFile << "pop front of pixel " << pixel << endl;
    int m = dispIm[pixel];
    int up = pixel - 640;
    int down = pixel + 640;
    int left = pixel - 1;
    int right = pixel + 1;
     
    //up
    if ((pixel>639) && (c[up]==0) && (dispIm[up] > -1) && (abs(dispIm[up]-m) < m_blobDispTol)) {     
      c[up] = k;
      q.push_front(up);
      //tempFile << "push_front of pixel " << pixel-640 << endl;
    }
    
    //left
    if (((pixel%640) > 0) && (c[left]==0) && (dispIm[left] > -1) && (abs(dispIm[left]-m) < m_blobDispTol)) {
      c[left] = k;
      q.push_front(left);
      //tempFile << "push_front of pixel " << pixel-1 << endl;

    }

    //down 
    if ((pixel<lastPixelRow) && (c[down]==0) && (dispIm[down] > -1) && (abs(dispIm[down]-m) < m_blobDispTol)) { 
      c[down] = k;
      q.push_front(down);
      //tempFile << "push_front of pixel " << pixel+640 << endl;
    }

    //right
    if (((pixel%640) < 639) && (c[right]==0) && (dispIm[right] > -1) && (abs(dispIm[right]-m) < m_blobDispTol)) { 
      c[right] = k;
      q.push_front(right);
      //tempFile << "push_front of pixel " << pixel+1 << endl;
    }
    
  }
  //tempFile << "Blob number " << k << " complete" << endl;
}
