#include "planner.h"
#include "DGCutils"
#include "MapAccess.h"
#include <iomanip>

CPlanner::CPlanner(CMap *pMap,  int mapLayerID, RDDF* pRDDF, bool USE_MAXSPEED, bool bGetVProfile)
	: m_refinementStage(pMap, mapLayerID, pRDDF, USE_MAXSPEED, bGetVProfile),
		m_reactiveStage  (pMap, mapLayerID, pRDDF, USE_MAXSPEED),
		m_interm("interm"),
		m_after("after"),
		m_pMap(pMap),
		m_layer(mapLayerID)
{
	m_interm << setprecision(20);
	m_after  << setprecision(20);
}

CPlanner::~CPlanner()
{
}

// pState is the state from astate
int CPlanner::plan(VehicleState *pState, bool bIsStateFromAstate,
									 CTraj* pPrevTraj)
{
	// copy the state so we can modify it if we need
	VehicleState stateCopy = *pState;

	cout << endl << "CPlanner::plan(VehicleState *pState)" << endl;
// 	cout << '{';
//  	cout << stateCopy.Northing << ", " << stateCopy.Vel_N << ", " << stateCopy.Acc_N << "," << endl;
//  	cout << stateCopy.Easting  << ", " << stateCopy.Vel_E << ", " << stateCopy.Acc_E << "," << endl;
//  	cout << stateCopy.Yaw << ", " << stateCopy.YawRate << "}" << endl 
// 			 << stateCopy.Northing_rear() << ' ' <<  stateCopy.Easting_rear() << endl;


	unsigned long long t1, t2;
	DGCgettime(t1);
	int res = m_reactiveStage.run(&stateCopy, bIsStateFromAstate,
																pPrevTraj);
	DGCgettime(t2);
	t2-=t1;
	cout << "reactiveStage: " << DGCtimetosec(t2) << " seconds" << endl;

	if(res == 0 || res == 4 || res == 9)
	{
		res = m_refinementStage.run(&stateCopy, m_reactiveStage.getTraj(), bIsStateFromAstate);
	}

	return res;
}

double CPlanner::getVProfile(CTraj* pTraj)
{
	m_refinementStage.m_bGetVProfile = true;

	*(m_refinementStage.getTraj()) = *pTraj;

	VehicleState vehstate(pTraj->getNorthingDiff(0, 0), pTraj->getNorthingDiff(0, 1), pTraj->getNorthingDiff(0, 2),
												pTraj->getEastingDiff (0, 0), pTraj->getEastingDiff (0, 1), pTraj->getEastingDiff (0, 2));
	m_refinementStage.run(&vehstate, pTraj, false);

	m_refinementStage.m_bGetVProfile = false;

	return m_refinementStage.getObjective();
}

CTraj* CPlanner::getTraj(void)
{
	return m_refinementStage.getTraj();
}

CTraj* CPlanner::getSeedTraj(void)
{
	return m_reactiveStage.getTraj();
}

CTraj* CPlanner::getIntermTraj(void)
{
	return m_refinementStage.getSeedTraj();
}

double CPlanner::getLength(void)
{
	return m_refinementStage.getLength();
}

double CPlanner::getMinSpeedOn(CTraj& traj)
{
	const double step = 1.0;
	int index = 0;
	int newindex = 0;
	double minspeed = 1.0e6;

	do
	{
		index = newindex;

		double speed;
		double dummy;
		getContinuousMapValueDiffGrown(1, m_pMap, m_layer, false, 0.0,
																	 traj.getNorthing(index),
																	 traj.getEasting(index),
																	 atan2(traj.getEastingDiff(index,1),
																				 traj.getNorthingDiff(index,1)),
																	 &speed, &dummy, &dummy, &dummy);

		minspeed = fmin(speed, minspeed);
		newindex = traj.getPointAhead( index, step );
	} while(newindex != index);

	return 0.0;
}

