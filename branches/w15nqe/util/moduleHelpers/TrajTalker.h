#ifndef _TRAJTALKER_H_
#define _TRAJTALKER_H_

#include "SkynetContainer.h"
#include "traj.h"
#include "pthread.h"
#include <unistd.h>

#include <string>
using namespace std;

class CTrajTalker : virtual public CSkynetContainer
{
	pthread_mutex_t m_dataBufferMutex;

	char* m_pDataBuffer;

public:
	CTrajTalker();
	~CTrajTalker();

	bool SendTraj(int trajSocket, CTraj* pTraj, pthread_mutex_t* pMutex = NULL);
	bool RecvTraj(int trajSocket, CTraj* pTraj, pthread_mutex_t* pMutex = NULL, string* pOutstring = NULL);
	void WaitForTrajData(int trajSocket);
};

#endif // _TRAJTALKER_H_
