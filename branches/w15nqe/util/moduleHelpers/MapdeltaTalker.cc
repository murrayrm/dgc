#include "MapdeltaTalker.h"

using namespace std;

CMapdeltaTalker::CMapdeltaTalker() {
}

CMapdeltaTalker::~CMapdeltaTalker() {
}

bool CMapdeltaTalker::RecvMapdelta(int mapdeltaSocket, char* pMapdelta, int* pSize) {
	// receive a larger-than-possible message
	*pSize = m_skynet.get_msg(mapdeltaSocket, pMapdelta, MAX_MSG_SIZE, 0);
	if(*pSize > 0) {
	  return true;
	}

	cerr << "CMapdeltaTalker::RecvMapdelta: Error!" << endl;
	return false;
}


bool CMapdeltaTalker::SendMapdelta(int mapdeltaSocket, CDeltaList* deltaList) {
  scatter tempScatter;

  tempScatter.num_elements = 2;
  tempScatter.elements[0].len = sizeof(deltaHeader);
    
  for(int i=0; i<deltaList->numDeltas; i++) {
    tempScatter.elements[0].buf = (char*)&(deltaList->deltaHeaders[i]);
    tempScatter.elements[1].buf = (char*)deltaList->deltaList[i];
    tempScatter.elements[1].len = deltaList->deltaSizes[i];

    m_skynet.send_msg(mapdeltaSocket, &tempScatter, 0);
  }
  
  return true;
}
