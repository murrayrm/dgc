#!/usr/bin/perl
#
# svn2-dist.pl - distribute e-mail to module owners
#
# Usage: svn-dist.pl repos rev 
#
# This perl script is used to distribute e-mail about svn commits to
# the owners of the various modules.  It works by reading data from a
# file that gives a regexp for the module and the list of people who
# should recieve e-mail when there are changes that match the
# expression.
#
# This script is basically a passthru to the commit-email script that
# comes with subversion.  The script assumes that commmit-email.pl is
# in the current directory (true for standard post-commmit hook).

$distfile = "/dgc/subversion/dgc/hooks/distfile";
$hostname = "grandchallenge.caltech.edu";
$prefix = "[dgc-svn]";

open(FP, $distfile) || die "can't open $distfile";
$line = 0;

while (<FP>) {
  ++$line;			# increment the line counter

    # Parse the contents of the line
    next if (/^\s*#/);		# skip comments
    next if (/^\s*$/);		# skip blank lines

    if (/^([^\s]*)\s(.*)$/) {
	# Find the module and the user list
	$module = $1;
	$users = $2;

	# Now call the commit-email script to do the work for us
	system "./commit-email.pl $ARGV[0] $ARGV[1] -h $hostname -s $prefix -m $module $users";
    } else {
	warn "can't parse line $line in file $distfile";
    }
}
