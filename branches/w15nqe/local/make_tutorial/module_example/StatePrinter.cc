#include "StatePrinter.hh"

using namespace std;



StatePrinter::StatePrinter(int skynetKey) : CSkynetContainer(SNRddfPathGen, skynetKey)
{
  // do something here if you want
}




StatePrinter::~StatePrinter()
{
  // Do something
}



void StatePrinter::ActiveLoop()
{
  // set up message counter
  int message_counter = 0;

  // echo SKYNET_KEY
  cout << "\n\nCurrent SKYNET_KEY: ";
  cout.flush();
  system("echo $SKYNET_KEY");

  
  // Loop is trapped here during execution
  while(true)
    {
      message_counter++;
      
      // grab where we are and store it as location, so we only have to access astate
      // once per time through the loop
      UpdateState();
      
      printf(" %4d   %8.1f   %8.1f", message_counter, 
	     m_state.Northing, m_state.Easting);
      cout << endl;
      
      usleep(500000);
    }

}



int main()
{
  //Setup skynet
  int intSkynetKey = 0;

  char* ptrSkynetKey = getenv("SKYNET_KEY");
  
  if(ptrSkynetKey == NULL)
    {
      cout << "Unable to get skynet key!" << endl;
      return 0;
    }
  else
    {
      //cout << "Got to StatePrinter int main" << endl;
      intSkynetKey = atoi(ptrSkynetKey);
      StatePrinter StatePrinterObj(intSkynetKey);
      
      //cout << "Entering ActiveLoop." << endl;
      StatePrinterObj.ActiveLoop();
    }
  
  return 0;
}
