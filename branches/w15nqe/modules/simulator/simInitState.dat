relative2
0
0.0
0.0
0.0
0.0 


Comments below this line:
-------------------------

The first line of simInitState.dat must be one of the
words {"absolute", "relative", "relative2"}.


The most desired setting ("relative2")
======================================

relative2 
startingWP
WPrelX 
WPrelY
theta
speed_forward 
        
WPrelX and WPrelY are relative to the coordinate system with the X-axis 
pointing along the trackline between startingWP and startingWP+1, and the 
Y-axis pointing to the right.

theta is the initial orientation of the vehicle with respect to the X-axis 
defined above, positive clockwise.

speed_forward is the speed in the direction the vehicle is facing.

Example simInitState.dat:

    relative 41
    0.5
    0.0 
    0.1
    0.25

This starts the vehicle at waypoint 41, 50 cm forward of the waypoint, directly 
on the trackline, with a yaw 0.1 radians to the right of the trackline yaw, and 
with a forward speed of 25 cm/s.


Other notes 
===========

If there is sufficient demand, some or all of the following could be 
implemented also for relative2:

speed_lateral(right)
accel_forward 
accel_lat(right) 
yaw_dot phi


The "absolute" setting
======================

  absolute  
  N     Nd   Ndd
  E     Ed   Edd
  yaw  yawd  phi
  
  An example simInitState.dat (for desert_loop2_wider):
    absolute
    3834841.834 0.0 0.0
    496059.329 0.0 0.0
    0.0 0.0 0.0

If the first line is "absolute", then DON'T INCLUDE a startingWP!


The "relative" setting
======================

If the first line of the file is "relative", then x and y will be interpreted 
as being relative to the startingWP'th point in the rddf (read from rddf.dat).  

(Hint: startingWP is the point on the RDDF that simulator will start relative 
 to)
startingWP is zero based.

  Example simInitState.dat:
    relative 0
    0.5 0.0 0.0
    0.5 0.0 0.0
    0.0 0.0 0.0


