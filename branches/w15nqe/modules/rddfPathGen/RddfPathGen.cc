#define TRYTIMBER

#include "RddfPathGen.hh"
#include <string>
using namespace std;

//Include the header file for the refinement stage of the planner, required in order to use
//Dima's make traj C2 function (used to convert the trajs output by RDDFPathGen to C2 just
//before they are output from RDDFPathGen
#include "RefinementStage.h"

//RDDF& rddf = *(new RDDF)


int NOSKYNET = 0;                /**< for command line argument */
int WRITE_COMPLETE_DENSE = 0;    /**< for command line argument */
int WRITE_COMPLETE_SPARSE = 1;   /**< for command line argument */
int WRITE_EVERY_TRAJ = 0;        /**< for command line argument */
int WAIT_STATE = true;           /**< for command line argument */
int NOMM = 0;                    /**< for command line argument; 
				    not using ModeManModule */
int C2 = 0;                      /**< whether or not to C2-ify the trajs
				  * 0 = none
				  * 1 = planner c2 function
				  * 2 = traj.feaziblize */
int C2SPECIFIED = 0;             /**< whether or not the C2 or noC2 option was specified */
/** for command line arguemnt */
int TIMBER_DEBUG_LEVEL = DEFAULT_DEBUG;


RddfPathGen::RddfPathGen(int skynetKey) : CSkynetContainer(SNRddfPathGen, skynetKey), CStateClient(WAIT_STATE), CTimberClient(timber_types::rddfPathGen)
{
  setDebugLevel(TIMBER_DEBUG_LEVEL);
  // do something here if you want
}



RddfPathGen::~RddfPathGen()
{
  // Do we need to destruct anything??
  cout << "RddfPathGen destructor has been called." << endl;
}

corridorstruct corridor_whole;

void RddfPathGen::ActiveLoop()
{
  /**
   * this is the main function which is run.  execution should be trapped
   * somewhere in this funciton during operation.
   */
  printf("[%s:%d] Entering RddfPathGen::ActiveLoop()\n", __FILE__, __LINE__);

  //###
  // trying to get ctimberclient to work
  //char* p_SkynetKey = getenv("SKYNET_KEY");
  //cout << "got key " << p_SkynetKey << endl;
  //int tempSkynetKey = atoi(p_SkynetKey);
  //CTimberClient lcObj();

  //  int thekey = lcObj.getSkynetKey();
  //cout << "The key is \"" << thekey << "\"" << endl;

  /*
  cout << "got here." << endl;
  char* junk = new char[32];
  storeMyIp(junk);
  cout << junk << endl;
  delete junk;
  */
  //###  

// #ifdef UNTESTED
//   /* Get rid of the ifdefs to test initial timber capability */
//   cout << "Getting log dir" << endl;
//   /* This function hangs in a loop by default if a Timber isn't running.  You 
//    * can tell it to do nothing if Timber is not running by giving it an 
//    * argument of "true".  TODO: Make this function return a default path even 
//    * if the Timber is not running. -LBC */
//   string logdir = getLogDir();
//   cout << "Log dir is \"" << logdir << "\"" << endl;
// #endif

  /*
  addFile("rddfdebug");
  addFile("rddfdebug");
  addFile("rddfdebug");
  addFile("rddfdebug2");
  addFile("rddfdebug3");
  addFile("rddfdebug4");
  addFile("rddfdebug");
  addFolder("folder1");
  addFolder("folder2");
  addFolder("folder1");
  addFolder("folder1/");
  addFolder("folder3/");
  addFolder("folder4/");
  */
  //  debug();

  // INITIALIZATION
  // set up message counter
  int message_counter = 0;

  // echo SKYNET_KEY
  cout << "\n\nCurrent SKYNET_KEY: ";
  cout.flush();
  system("echo $SKYNET_KEY");

  // let people know which file we're operating on
  cout << "Current RDDF is: ";
  cout.flush();
  system("readlink ../../util/RDDF/rddf.dat");

  // read the file into a corridor segment and get the waypoints in northing
  // and easging.  we'll use the nice functions in dgc/RDDF/rddf.hh
  cout << "Reading RDDF... "; cout.flush();
  herman& rddf = *(new herman);
  rddf.loadFile(RDDF_FILE);
  RDDFVector& waypoints = *(new RDDFVector);
  waypoints = rddf.getTargetPoints();
  
  // extract the info from waypoints and store in corridor
  int N = rddf.getNumTargetPoints();   // number of points
  // store data in corridor
  corridor_whole.numPoints = N;
  for(int i = 0; i < N; i++)
    {
      // shouldn't get any segfaults here, but who knows...
      corridor_whole.e.push_back(waypoints[i].Easting);
      corridor_whole.n.push_back(waypoints[i].Northing);
      //shrink the corridor width to account for vehicle width, tracking error
      corridor_whole.width.push_back(waypoints[i].radius-RDDF_VEHWIDTH_EFF);
      corridor_whole.speedLimit.push_back(waypoints[i].maxSpeed);
      
      if (waypoints[i].radius-RDDF_VEHWIDTH_EFF < .01)
	cerr << "CORRIDOR WIDTH IS SMALLER THAN RDDF_VEHWIDTH_EFF!" << endl;

      //###
      //printf("waypoints[ %d ].Easting is %f\n", i, waypoints[i].Easting);
      //printf("waypoints[ %d ].Northing is %f\n", i, waypoints[i].Northing);
      //###
    }
  cout << "done (read " << N << " points)." << endl;
  
  // generate and store whole path (sparse path, so max of 4 path points
  // per corridor point)
  cout << "Generating complete (sparse) path... "; cout.flush();
  //  pathstruct path_whole_sparse = (pathstruct)Path_From_Corridor(corridor_whole);
  pathstruct& path_whole_sparse = *(new pathstruct);
  path_whole_sparse = Path_From_Corridor(corridor_whole);
  path_whole_sparse.currentPoint = 0; // we haven't went anywhere yet, so we're still at the start of the path
  /** create an 'rddf' which contains the path, so that we can use herman to keep 
   * track of where we are along the path */
  herman& path_whole_sparse_rddf = *(new herman(NULL));
  RDDFData temp_point;
  cout << (int)path_whole_sparse.numPoints << endl;
  for (int i = 0; i < (int)path_whole_sparse.numPoints; i++)
    {
      temp_point.number = i;
      temp_point.distFromStart = (i==0) ? 0 :
	path_whole_sparse_rddf.getWaypointDistFromStart(i-1) +
	hypot(path_whole_sparse.n[i] - path_whole_sparse.n[i-1],
	      path_whole_sparse.e[i] - path_whole_sparse.e[i-1]);
      temp_point.Northing = path_whole_sparse.n[i];
      temp_point.Easting = path_whole_sparse.e[i];
      temp_point.maxSpeed = 1234.5;
      temp_point.offset = path_whole_sparse.corWidth[i];
      temp_point.radius = temp_point.offset;
      path_whole_sparse_rddf.addDataPoint(temp_point);
    }
  cout << "done (sparse path has " << path_whole_sparse.numPoints
       << " points, sparse path \'rddf\' has " << path_whole_sparse_rddf.getNumTargetPoints()
       << " points." << endl;


  int trajSocket;
  int skynetguiSocket;

  if(!NOSKYNET)
    {
      if (NOMM)
	trajSocket = m_skynet.get_send_sock(SNtraj);
      else 
	{
	  skynetguiSocket = m_skynet.get_send_sock(SNtraj);
	  trajSocket = m_skynet.get_send_sock(SNRDDFtraj);
	}
    }
  

  // generate blank trajectory
  CTraj* ptraj;
  ptraj = new CTraj(3);  // order 3 (by default)
  
  int usleep_time = 1000000/FREQUENCY; // set frequency in RddfPathGen.hh
  cout << endl << "   Num        N        E            First   Cur    Last" << endl;
  
  //###
  //this is for testing of CPath as a means of generating traj's for the planner
  //CTraj* seed_traj;
  //seed_traj = new CTraj(3);
  //CPath seed_path("rddf.dat");
  //###


  //Declare the refinement stage for the planner required when using Dimas make traj
  //C2 function
  CRefinementStage refstage;

  // some variables used for logging
  bool logging_enabled;
  string logging_location;
  int logging_level;
  
  while(true)
    {
      // Main execution loop.  This is where we're trapped during normal program execution
      message_counter++;
      
      // timber logging section (complete dense and sparse traj part)
      logging_enabled = getLoggingEnabled();
      if (logging_enabled)
	{
	  logging_location = getLogDir();
	  logging_level = getMyLoggingLevel();
 	  if (checkNewDirAndReset())
	    {
	      if (WRITE_COMPLETE_DENSE && logging_level > 0)
		{
		  string file_location = logging_location + (string)TRAJOUTPUT;
		  char* file = new char[256];
		  strcpy(file, file_location.c_str());
		  
		  cout << "Writing whole dense traj to file (may take a while; file"
		       << " is " << file_location << ")... ";
		  cout.flush();
		  
		  pathstruct path_whole_dense = DensifyPath(path_whole_sparse);
		  WritePathToFile(path_whole_dense, file);  // TRAJOUTPUT is defined in RddfPathGen.hh
		  delete [] file;
		  
		  cout << "done." << endl;
		}

	      if (WRITE_COMPLETE_SPARSE && logging_level > 0)
		{
		  string file_location = logging_location + (string)SPARSE_PATH_FILE;
		  char* file = new char[256];
		  strcpy(file, file_location.c_str());

		  cout << "Writing sparse path to file " << file_location << " ... "; cout.flush();
		  
		  WritePathToFile(path_whole_sparse, file);
		  cout << "done." << endl;
		}
	    }
	} // end of logging section
      
      // grab where we are and store it as location, so we only have to access astate
      // once per time through the loop
      vector<double> location(2);
      GetLocation(location);
      
      rddf.updateState(m_state); // m_state was updated in GetLocation
      path_whole_sparse_rddf.updateState(m_state); // m_state was updated in GetLocation
      // get the chopped dense traj (the one we send over skynet), in path format
      pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location,
						     CHOPBEHIND, CHOPAHEAD,
						     path_whole_sparse_rddf.getCurrentPoint());
      //pathstruct path_chopped_dense = path_whole_dense;  /**< send whole each time */
      int curpt = rddf.getCurrentPoint();
      cout << "   herman rddf: point " << curpt
       << " (" << location[0] - path_whole_sparse.n[curpt]
       << ", " << location[1] - path_whole_sparse.e[curpt] << ")" << endl;

      
      StorePath(*ptraj, path_chopped_dense);

      if(SEND_TRAJ && !NOSKYNET)
	{
	  if (C2 == 1)
	    {
	      //Use Dima's function to make the output traj C2
	      refstage.MakeTrajC2(ptraj);
	    }
	  else if (C2 == 2)
	    ptraj->feasiblize(RDDF_MAXLATERAL, RDDF_MAXACCEL, RDDF_MAXDECEL, CURVATURE_AVERAGING_DISTANCE);
	  
	  SendTraj(trajSocket, ptraj);
	  // Also send to skynetgui
	  if (!NOMM) SendTraj(skynetguiSocket, ptraj);
	}
      
      // timber logging section (chopped dense traj's)
      if (logging_enabled && logging_level >= 2)
	{
	  //### save the last sent chopped path
	  char* buffer= new char[256];
	  string file_location = logging_location + (string)CHOPPED_OUTPUT_BASE;
	  strcpy(buffer, file_location.c_str());
 
	  char* msg_num = new char[256];
	  sprintf(msg_num, "_%04d", message_counter);
	  strcat(buffer, msg_num);
 	  
	  //open file and write it
	  ofstream output_file;
	  output_file.open(buffer, ios::trunc | ios::out);
	  if(!output_file.is_open())
	    cout << "\n ! Unable to open file " << (string)buffer << " , no data will be written!" << endl;
	  else
	    ptraj->print(output_file);
	  output_file.close();

	  delete [] msg_num;
	  delete [] buffer;
	}

      //###
      // cout << "my logging level is " << getMyLoggingLevel() << endl;
      //###      
      printf(" %4d   %8.1f   %8.1f     %6d  %6d  %6d   (%d, %d, %d)", message_counter, 
	     location[0], location[1], 0, path_chopped_dense.currentPoint,
	     path_chopped_dense.numPoints-1, 0, path_whole_sparse.currentPoint,
	     path_whole_sparse.numPoints-1);
      cout << endl;
      
      usleep(usleep_time);
    }


  delete ptraj;
  delete &rddf;
  delete &waypoints;
  delete &path_whole_sparse;
  delete &path_whole_sparse_rddf;
}



void RddfPathGen::GetLocation(vector<double> & location)
{
  UpdateState(); 
  location[0] = m_state.Northing;
  location[1] = m_state.Easting;
}



void RddfPathGen::ReceiveDataThread()
{
  cout << "RddfPathGen is unable to receive data at this time." << endl;
}



int main(int argc, char** argv)
{
  // stick the whole thing in a try loop, so we can catch errors
  try {
    for (int i = 1; i < argc; ++i)
      {
	string arg(argv[i]);
      
	if (arg == "h" || arg == "-h" || arg == "--help" || arg == "help")
	  {
	    /* User has requested usage information. Print it to standard
	       output, and exit with exit code zero. */
	    cout << "| Command line options:\n"
		 << "| (you must type command line options just as they appear in this help\n"
		 << "| screen; do not preceed them with some arbitrary number of -'s.\n"
		 << "===============================================================\n"
		 << "  h,-h,help,--help    Prints this.\n"
		 << "  k, noskynet         Doesn't send any skynet messages.  We still need skynet\n"
		 << "                         to run, because we need state.\n"
		 << "  d, writedense       Writes the dense traj to a file.\n"
		 << "  s, nosparse         Skips writing the sparse traj to a file.\n"
		 << "  e, everytraj        Writes every traj sent over skynet to a file (named sequentially).\n"
		 << "  w, nowrite          Skips writing the dense and sparse traj to a file.\n"
		 << "  nw, nowait          Doesn't wait for the state buffer to fill (though it will still use\n"
		 << "                         state once it is filled).\n"
		 << "  nomm                Don't use ModeManModule.\n"
		 << "  c, c2               Make all traj's c2 by running them through an approximation\n"
		 << "                         function in the planner.\n"
		 << "  f, feasiblize       Run the trajs through the traj.feaziblize function to make\n"
		 << "                         the feedforward term smooth.\n"
		 << "  n, noc2             Do not perform any approximations.\n"
		 << "  d0                  Set debug level to 0 for timber operations.\n"
		 << "  d1                  Set debug level to 0 for timber operations.\n"
		 << "  d2                  Set debug level to 0 for timber operations.\n"
		 << "  \n"
		 << "| Additional info:\n"
		 << "==============================================================\n"
		 << "  \n"
		 << "  Users must explicitly specify either c2 (c) or noc2 (n) on the command line.  This is\n"
		 << "  required so that there isn't any confusion as to what kind of traj's we're sending.\n"
		 << "  C2 traj's will be smoother (continuous second derivative), but the actual path may\n"
		 << "  jump around.  Non-c2 traj's will not move around, but they have a second derivative\n"
		 << "  which is only piecewise continuous.\n"
		 << "  \n"
		 << "  Also, the options writedense and c2 are mutually exclusive, because the approximation\n"
		 << "  function currently used would never work for the whole corridor.\n"
		 << "  \n"
		 << "  Nothing will be written to any log file unless logging has been enabled via timber!\n"
		 << "  \n"
		 << "| Status:\n"
		 << "==============================================================\n"
		 << "  Current RDDF:  ";
	    cout.flush();
	    system("readlink ../../util/RDDF/rddf.dat");
	    cout << "Current SKYNET_KEY:  ";
	    cout.flush();
	    system("echo $SKYNET_KEY");
	    cout << endl;
	    return 0;
	  }
	else if (arg == "d" || arg == "writedense")
	  {
	    cout << "WRITING DENSE TRAJ TO FILE!" << endl;
	    WRITE_COMPLETE_DENSE = 1;
	  }
	else if (arg == "s" || arg == "nosparse")
	  {
	    cout << "NOT WRITING SPARSE TRAJ TO FILE!" << endl;
	    WRITE_COMPLETE_SPARSE = 0;
	  }
	else if (arg == "e" || arg == "everytraj")
	  {
	    cout << "WRITING EVERY TRAJ TO FILE!" << endl;
	    WRITE_EVERY_TRAJ = 1;
	  }
	else if (arg == "w" || arg == "nowrite")
	  {
	    cout << "NOT WRITING DENSE TRAJ TO FILE!" << endl;
	    cout << "NOT WRITING SPARSE TRAJ TO FILE!" << endl;
	    cout << "NOT WRITING EVERY TRAJ TO FILE!" << endl;
	    WRITE_COMPLETE_DENSE = 0;
	    WRITE_COMPLETE_SPARSE = 0;
	    WRITE_EVERY_TRAJ = 0;
	  }
	else if (arg == "nw" || arg == "nowait")
	  {
	    cout << "NOT WAITING FOR STATE TO FILL!" << endl;
	    WAIT_STATE = false;
	  }
	else if (arg == "k" || arg == "noskynet")
	  {
	    cout << "NOT SENDING SKYNET MESSAGES!" << endl;
	    NOSKYNET = 1;
	  }
	else if (arg == "nomm")
	  {
	    cout << "RUNNING WITHOUT MODEMANMODULE!" << endl;
	    NOMM = 1;
	  }
	else if (arg == "c" || arg == "c2")
	  {
	    cout << "TRAJ'S WILL BE C2 (THROUGH PLANNER FUNCTION)!" << endl;
	    C2 = 1;
	    C2SPECIFIED = 1;
	  }
	else if (arg == "f" || arg == "feasiblize")
	  {
	    cout << "TRAJ'S WILL BE C2 (THROUGH FEASIBLIZE)!" << endl;
	    C2 = 2;
	    C2SPECIFIED = 1;
	  }
	else if (arg == "n" || arg == "noc2")
	  {
	    cout << "TRAJ'S WILL NOT BE C2!" << endl;
	    C2 = 0;
	    C2SPECIFIED = 1;
	  }
	else if (arg == "d0")
	  {
	    cout << "TIMBER DEBUG LEVEL SET TO 0" << endl;
	    TIMBER_DEBUG_LEVEL = 0;
	  }
	else if (arg == "d1")
	  {
	    cout << "TIMBER DEBUG LEVEL SET TO 1" << endl;
	    TIMBER_DEBUG_LEVEL = 1;
	  }
	else if (arg == "d2")
	  {
	    cout << "TIMBER DEBUG LEVEL SET TO 2" << endl;
	    TIMBER_DEBUG_LEVEL = 2;
	  }
	else
	  cout << "UNKNOWN SWITCH: \"" << arg << "\"" << endl;
      }

    // Check the validity of the command line options.
    // two things must be checked... they must have specified either c2 or noc2, and
    // they can't specify c2 and writedense
    if (!C2SPECIFIED)
      {
	ostringstream oss;
	oss << __FILE__ << ", " <<__LINE__ << ": You must specify one of {c, c2, n, noc2, f, feasiblize} on"
	    << " the command line!";
	throw (oss.str());
      }
    if (C2 && WRITE_COMPLETE_DENSE)
      {
	ostringstream oss;
	oss << __FILE__ << ", " << __LINE__ << ": C2 and WRITE_COMPLETE_DENSE are incompatible options:"
	    << "  remove one of them!";
	throw (oss.str());
      }

    //Setup skynet
    int intSkynetKey = 0;

    char* ptrSkynetKey = getenv("SKYNET_KEY");
  
    if(ptrSkynetKey == NULL)
      {
	cout << "Unable to get skynet key!" << endl;
	return 1;
      }
    else
      {
	//cout << "Got to RddfPathGen int main" << endl;
	intSkynetKey = atoi(ptrSkynetKey);
	RddfPathGen RddfPathGenObj(intSkynetKey);

	//cout << "Entering ActiveLoop." << endl;
	RddfPathGenObj.ActiveLoop();
      }
    
  } // end of try {
  catch (std::string message)
    {
      cout << "Caught exception, error was \"" << message << "\"" << endl;
      return 1;
    }
  
  return 0;
}
