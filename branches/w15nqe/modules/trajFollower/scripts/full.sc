#!/bin/bash -x

for CMD in "run11" "run12" "run13" "run14" "run15" "run16" "run17" "run18" "run19" "run20" "run21" 

#for CMD in "run1" "run2" "run3" "run4" "run5" "run6" "run7" "run8" "run9" "run10" "run11" "run12" "run13" "run14" "run15" "run16" "run17" "run18" "run19" "run20" "run21" 

#for CMD in "run3" 

do

export FILE=$CMD
#mkdir $FILE

#plots and saves des vs. actual path, Yerr, FF*FB, 

./plot.sc

#runs analyzePerformance on the input file
export CMD=analyzePerformance\(\"$FILE\"\)
echo $CMD | octave > temp

 #mv FT_yerr.pdf $FILE\_FT_yerr.pdf
#mv FT_cmd.pdf $FILE\_FT_cmd.pdf

rm temp

done