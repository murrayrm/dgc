/* VehicleState.hh - platform-independent state struct
 *
 * Lars Cremean
 * 6 Jan 05
 * 
 * This file defines platform-independent state structs intended for use by 
 * various code that constructs, sends or receives state information.
 */

#ifndef VEHICLESTATE_HH
#define VEHICLESTATE_HH

#include <string.h>
#include <math.h>
#include <stdio.h>

#warning VehicleState.hh is deprecated; use interfaces/VehicleState.h instead

// Now needed for VEHICLE_WHEELBASE and the calculation of the partial state at 
// the front axle of the vehicle.
#include "AliceConstants.h"
#include "frames/coords.hh" // for NEcoord

/** New, clean struct for vehicle state representation.  Originally implemented 
 * as GetVehicleStateMsg in bob/vehlib/VState.hh, but moved here because it is 
 * platform-independent.  All values use the center of the rear axle as the origin
 * of the vehicle frame unless otherwise stated. */
struct VehicleState
{
	/** Default constructor */
	VehicleState()
	{
		// initialize the state to zeros to appease the memory profilers
		memset(this, 0, sizeof(*this));
	}

  /** This constructor initializes a VehicleState structure from derivative data
			that could come from a CTraj point. Note that no state transformation is
			performed */
	VehicleState(double n, double nd, double ndd,
							 double e, double ed, double edd)
	{
		utmNorthing = n;
		utmEasting = e;
		utmNorthVel = nd;
		utmEastVel = ed;

		utmAltitude = utmAltitudeVel = 0.0;
		utmPitch = utmRoll = utmRollRate = utmPitchRate = 0.0;

		utmYaw = atan2(ed, nd);
		utmYawRate = (nd*edd - ndd*ed) / (nd*nd + ed*ed);
	}

  /** Timestamp represents UNIX-time. This is the value of microseconds since
   *  the epoch (1/1/1970) */
  unsigned long long timestamp;

  /** Position of the vehicle in the UTM frame (meters).  To convert
      these to a global cartesian frame, use: (x = northing, y =
      easting, z = altitude).  Note that the altitude is positive
      /downward/ and zero at sea-level. */
  double utmNorthing, utmEasting, utmAltitude;

  /* The following variables are depricated */
  double GPS_Northing_deprecated;
  double GPS_Easting_deprecated;

  /** Linear velocity of the vehicle in the UTM frame (m/sec). */
  double utmNorthVel, utmEastVel, utmAltitudeVel;

  /** Component of linear acceleration in the Northing direction (m/s/s).  */
  double Acc_N_deprecated;

  /** Component of linear acceleration in the Easting direction (m/s/s).  */
  double Acc_E_deprecated;

  /** Component of linear acceleration in the downward direction (m/s/s).  */
  double Acc_D_deprecated;

  /** Rotation of the vehicle in the UTM frame (radians); these
      are Euler angles with yaw applied last.  Yaw can also be
      interpreted as a conventional compass heading, i.e., yaw = 0 if
      the vehicle is facing due north, yaw = pi/2 if the vehicle is
      facing due east, etc. */
  double utmRoll, utmPitch, utmYaw;

  /** Angular velocity of the vehicle in the UTM frame (radians/sec). */
  double utmRollRate, utmPitchRate, utmYawRate;
  
  // Old variables to preserve stucture
  double raw_YawRate_deprecated;
  double RollAcc_deprecated, PitchAcc_deprecated, YawAcc_deprecated;

  /** Confidence intervals on vehicle position in UTM frame.
      @todo What units are these measured in? */
  double utmNorthConfidence, utmEastConfidence, utmAltitudeConfidence;  

  // Old variables to preserve structure
  double rollConfidence;		//<! Confidence level in Roll
  double pitchConfidence;		//<! Confidence level in Pitch
  double yawConfidence;			//<! Confidence level in Yaw

  /** This is a unitless measure of the goodness of the GPS data.
      @todo What is the range of gamma? */
  double gpsGamma;

  /** Vehicle UTM zone (e.g., 11S for Southern California) */
  int utmZone, utmLetter;
    
  /** Linear velocities in vehicle frame (m/sec). */
  double vehXVel, vehYVel, vehZVel;

  /** Angular velocities in vehicle frame (rad/sec). */
  double vehRollRate, vehPitchRate, vehYawRate;

  /** Position in local frame (m); +z is down. */
  double localX, localY, localZ;

  /** Rotation in local frame (radians); these are Euler angles with
      yaw applied last. */
  double localRoll, localPitch, localYaw;

  /** Linear velocity of the vehicle in the local frame (m/sec). */
  double localXVel, localYVel, localZVel;

  /** Angular velocity of the vehicle in the local frame (radians/sec). */
  double localRollRate, localPitchRate, localYawRate;
  

  /** Accessor function to get an NEcoord format of the front of the Vehicle. */
  NEcoord ne_coord() const
  {
    NEcoord ret;
    ret.N = utmNorthing;
    ret.E = utmEasting;
    return ret;
  }

  /** The scalar component of the velocity vector in the ground plane (m/s). */
  double Speed2(void) 
  {
    return hypot(utmNorthVel, utmEastVel);
  }

  /** The scalar component of the velocity vector in 3D (m/s).  Measurement
   * of Vel_D may have low accuracy, so be careful. */
  double Speed3(void) 
  {
    return sqrt(utmNorthVel*utmNorthVel + utmEastVel*utmEastVel + utmAltitudeVel*utmAltitudeVel);
  }

  /** The scalar component of the acceleration vector in the ground plane 
   * (m/s/s).  Note that this is NOT the same as d(Speed)/dt, and is not the 
   * longitudinal component of the acceleration vector. */
  double Accel2(void) 
  {
    return hypot(Acc_N_deprecated, Acc_E_deprecated);
  }  
  
  /** The scalar component of the acceleration vector in 3D (m/s/s). Note that 
   * this is NOT the same as d(Speed)/dt, and is not the longitudinal component 
   * of the acceleration vector. Measurement of Acc_D may have low accuracy, so 
   * be careful. */
  double Accel3(void) 
  {
    return sqrt(Acc_N_deprecated*Acc_N_deprecated + Acc_E_deprecated*Acc_E_deprecated 
		+ Acc_D_deprecated*Acc_D_deprecated);
  }

  /** The Northing of the ground beneath the center of the rear axle [m].  */
  double Northing_rear(void)
  {
    return utmNorthing;
  }

  /** The Easting of the ground beneath the center of the rear axle [m]. */
  double Easting_rear(void)
  {
    return utmEasting;
  }
  
  /** The Northing of the ground beneath the center of the front axle [m].  The 
   * return value for this accessor function is calculated based on the nominal 
   * (rear-axle) coordinates.  This only implements the /planar/ coordinate 
   * transformation. */
  double Northing_front(void)
  {
    return (utmNorthing + VEHICLE_WHEELBASE*cos(utmYaw));
  }

  /** The Easting of the ground beneath the center of the front axle [m].  The 
   * return value for this accessor function is calculated based on the nominal 
   * (rear-axle) coordinates.  This only implements the /planar/ coordinate 
   * transformation. */
  double Easting_front(void)
  {
    return (utmEasting + VEHICLE_WHEELBASE*sin(utmYaw));
  }
  
  /** The GPS_Northing of the ground beneath the center of the rear axle [m].  The 
   * return value for this accessor function is calculated based on the nominal 
   * (front-axle) coordinates.  This only implements the /planar/ coordinate 
   * transformation. FOR NOVATEL */
  double GPS_Northing_rear(void)
  {
    return utmNorthing;
  }

  /** The GPS_Easting of the ground beneath the center of the rear axle [m].  The 
   * return value for this accessor function is calculated based on the nominal 
   * (front-axle) coordinates.  This only implements the /planar/ coordinate 
   * transformation. FOR NOVATEL */
  double GPS_Easting_rear(void)
  {
    return utmEasting;
  }

  /** The GPS_Northing of the ground beneath the center of the front axle [m].  The 
   * return value for this accessor function is calculated based on the nominal 
   * (read-axle) coordinates.  This only implements the /planar/ coordinate 
   * transformation. FOR NOVATEL */
  double GPS_Northing_front(void)
  {
    return (utmNorthing + VEHICLE_WHEELBASE*cos(utmYaw));
  }

  /** The GPS_Easting of the ground beneath the center of the front axle [m].  The 
   * return value for this accessor function is calculated based on the nominal 
   * (read-axle) coordinates.  This only implements the /planar/ coordinate 
   * transformation. FOR NOVATEL */
  double GPS_Easting_front(void)
  {
    return (utmEasting + VEHICLE_WHEELBASE*sin(utmYaw));
  }

  /** The first derivative of the Northing at the center of the rear axle of 
   * the vehicle [m/s].  */
  double Vel_N_rear(void)
  {
    return utmNorthVel;
  }

  /** The first derivative of the Easting at the center of the rear axle of the 
   * vehicle [m/s].  */
  double Vel_E_rear(void)
  {
    return utmEastVel;
  }
  
  /** The first derivative of the Northing at the center of the front axle of 
   * the vehicle [m/s].  The return value for this accessor function is 
   * calculated based on the nominal (rear-axle) coordinates.  This only 
   * implements the /planar/ coordinate transformation. */
  double Vel_N_front(void)
  {
    return (utmNorthVel - VEHICLE_WHEELBASE*utmYawRate*sin(utmYaw));
  }

  /** The first derivative of the Easting at the center of the front axle of the 
   * vehicle [m/s].  The return value for this accessor function is calculated 
   * based on the nominal (rear-axle) coordinates.  This only implements the 
   * /planar/ coordinate transformation. */
  double Vel_E_front(void)
  {
    return (utmEastVel + VEHICLE_WHEELBASE*utmYawRate*cos(utmYaw));
  }

	/** Estimate of YawRate based on the bicycle model and cartesian derivatives */
	double YawRateEstimate(void)
	{
		return (utmNorthVel*Acc_E_deprecated - Acc_N_deprecated*utmEastVel) / 
		  (utmNorthVel*utmEastVel + utmEastVel*utmEastVel);
	}


  /** Prints the VehicleState member variables */
  void print(void)
  {
    printf("Timestamp = %lld\n", timestamp);
    printf("Northing  = %f\n", utmNorthing);
    printf("Easting   = %f\n", utmEasting);
    printf("Altitude  = %f\n", utmAltitude);
    printf("Vel_N     = %f\n", utmNorthVel);
    printf("Vel_E     = %f\n", utmEastVel);
    printf("Vel_D     = %f\n", utmAltitudeVel);
    printf("Acc_N     = %f\n", Acc_N_deprecated);
    printf("Acc_E     = %f\n", Acc_E_deprecated);
    printf("Acc_D     = %f\n", Acc_D_deprecated);
    printf("Roll      = %f\n", utmRoll);
    printf("Pitch     = %f\n", utmPitch);
    printf("Yaw       = %f\n", utmYaw);
    printf("RollRate  = %f\n", utmRollRate);
    printf("PitchRate = %f\n", utmPitchRate);
    printf("YawRate   = %f\n", utmYawRate);
    printf("RollAcc   = %f\n", RollAcc_deprecated);
    printf("PitchAcc  = %f\n", PitchAcc_deprecated);
    printf("YawAcc    = %f\n\n", YawAcc_deprecated);
  }
  
}  __attribute__((packed)) ;;

#endif
