/* ActuatorState.hh - platform-independent actuator state struct
 *
 * Dima
 */

#ifndef ACTUATORSTATE_HH
#define ACTUATORSTATE_HH

#warning ActuatorState.hh is deprecated; use interfaces/ActuatorState.h

// Get the YaM actuator state struct
#include "interfaces/ActuatorState.h"

#endif
