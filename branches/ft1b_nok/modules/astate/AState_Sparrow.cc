/* \file Astate_Sparrow.cc
 * \brief Astate class functions to generate/update Astate Sparrow display
 * \author Stefano Di Cairano,
 * \date dec-06
 */

#include "AState.hh"

//Struct containing all possible pieces of data being outputted

#include "asdisp.h"		// display table

//Populate displayVars struct

void AState::sparrowThread() 
{
  CSparrowHawk &sh = SparrowHawk();

  sh.add_page(asdisp, "Main");

  sh.rebind("snkey", &snKey);
  
//  sh.rebind("apxActive", &apxActive);
  sh.rebind("apxStatus", &apxStatus);
  sh.rebind("apxEnabled", &apxEnabled);
  sh.rebind("apxCount", &apxCount);
  sh.rebind("stateMode", &stateMode);
  sh.rebind("timberEn", &timberEnabled);
  sh.rebind("apxActive", &apxActive);
  sh.rebind("logEn", &_options.logRaw);
  sh.rebind("repEn", &_options.stateReplay);
  
  sh.rebind("vsNorthing", &_vehicleState.Northing);
  sh.rebind("vsEasting", &_vehicleState.Easting);
  sh.rebind("vsAltitude", &_vehicleState.Altitude);
  sh.rebind("vsVel_N",  &_vehicleState.Vel_N);
  sh.rebind("vsVel_E",  &_vehicleState.Vel_E);
  sh.rebind("vsVel_D",  &_vehicleState.Vel_D);
  sh.rebind("vsAcc_N",  &_vehicleState.Acc_N);
  sh.rebind("vsAcc_E",  &_vehicleState.Acc_E);
  sh.rebind("vsAcc_D",  &_vehicleState.Acc_D);
  sh.rebind("vsRoll", &_vehicleState.Roll);
  sh.rebind("vsPitch", &_vehicleState.Pitch);
  sh.rebind("vsYaw", &_vehicleState.Yaw);
  sh.rebind("vsRollRate", &_vehicleState.RollRate);
  sh.rebind("vsPitchRate", &_vehicleState.PitchRate);
  sh.rebind("vsYawRate", &_vehicleState.YawRate);
  sh.rebind("vsRollAcc", &_vehicleState.RollAcc);
  sh.rebind("vsPitchAcc", &_vehicleState.PitchAcc);
  sh.rebind("vsYawAcc", &_vehicleState.YawAcc);
  sh.rebind("vsNorthConf", &_vehicleState.NorthConf);
  sh.rebind("vsEastConf", &_vehicleState.EastConf);
  sh.rebind("vsHeightConf", &_vehicleState.HeightConf);
  sh.rebind("vsRollConf", &_vehicleState.RollConf);
  sh.rebind("vsPitchConf", &_vehicleState.PitchConf);
  sh.rebind("vsYawConf", &_vehicleState.YawConf);
  sh.rebind("lpNorthing", &_vehicleState.localX);
  sh.rebind("lpEasting", &_vehicleState.localY);
  sh.rebind("lpAltitude", &_vehicleState.localZ);
  sh.rebind("lpVel_N",  &_vehicleState.vehicleVelX);
  sh.rebind("lpVel_E",  &_vehicleState.vehicleVelY);
  sh.rebind("lpVel_D",  &_vehicleState.vehicleVelZ);
  sh.rebind("lpAcc_N",  &_vehicleState.Acc_N);
  sh.rebind("lpAcc_E",  &_vehicleState.Acc_E);
  sh.rebind("lpAcc_D",  &_vehicleState.Acc_D);
  sh.rebind("lpRoll", &_vehicleState.localRoll);
  sh.rebind("lpPitch", &_vehicleState.localPitch);
  sh.rebind("lpYaw", &_vehicleState.localYaw);
  sh.rebind("lpRollRate", &_vehicleState.vehicleVelRoll);
  sh.rebind("lpPitchRate", &_vehicleState.vehicleVelPitch);
  sh.rebind("lpYawRate", &_vehicleState.vehicleVelYaw);
  sh.rebind("lpRollAcc", &_vehicleState.RollAcc);
  sh.rebind("lpPitchAcc", &_vehicleState.PitchAcc);
  sh.rebind("lpYawAcc", &_vehicleState.YawAcc);
  
//  sh.set_notify("RESTART", this, &AState::restart);
  
  //Sparrow bind goes here

  // Run sparrow
  sh.run();

  while(sh.running()) 
  { 
    usleep(100000);
  }

  quitPressed = 1;

}

