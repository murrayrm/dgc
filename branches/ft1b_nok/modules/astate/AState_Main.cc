/*! \file AState_Main.cc
 * \brief Main function of Astate
 * \author Stefano Di Cairano,
 * \date jan-07
 */


#include <getopt.h>
#include "AState.hh"
#include <stdlib.h>
#include "DGCutils"
#include "cmdline.h"
#include "skynet.hh"

AState *ss;


int main(int argc, char **argv)
{
  int sn_key  = skynet_findkey(argc, argv); //getting skynet key
  
  astateOpts options;
# warning Default options should be in the ggo file
  options.useSparrow=1;
  options.logRaw=0;
  options.stateReplay=0;
  
  //parsing user defined arguments
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0)
  { 
    exit(1);
  }

  /*
   * Note: you could probably just do this:
   *
   * options.logRaw = cmdline.enable_logging_given;
   * options.stateReplay = cmdline.enable_statereplay_given;
   */
  
  if (cmdline.enable_logging_given)
  {
  	options.logRaw = 1;
  }  
 
  if (cmdline.enable_statereplay_given)
  {
  	options.stateReplay = 1;
  }  
  
  if (cmdline.enable_datareplay_given)
  {
  	//options.dataReplay = 1; not implemented yet
  }  
  
  if (cmdline.disable_sparrow_given)
  {
  	options.useSparrow = 0;
  }
  
  
  // few checks to avoid stupid inconsistencies
#ifdef UNUSED  
  if ((options.dataReplay==1) && (options.stateReplay==1))
  {
  	cout << "You should decide: EITHER replay from state OR replay from DATA !!"<< endl;
  	exit(1);
  }
  
  if ( ((options.dataReplay==1) || (options.stateReplay==1)) && (options.logRaw==1) )
  {
  	cout << "Why do you want to log replay data? This makes no sense"<< endl;
  	exit(1);
  }
#endif  
 
 //Create AState server:
  ss = new AState(sn_key,options);

  ss->active();

  delete ss;

  return 0;
}
