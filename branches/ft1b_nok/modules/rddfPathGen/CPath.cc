
#include "CPath.hh"

using namespace std;


/**
 * constructor...reads in and creates whole sparse path
 */
CPath::CPath(char * rddf_file)
{
  // set default values of variables
  chop_ahead = CHOPAHEAD;    // both defined in RddfPathGen.hh
  chop_behind = CHOPBEHIND;


//   cout << "Reading RDDF... "; cout.flush();
  herman rddf("CPath_rddf");
  rddf.loadFile(rddf_file);
  RDDFVector waypoints;
  waypoints = rddf.getTargetPoints();

  // extract the info from waypoints and store in corridor
  int N = rddf.getNumTargetPoints();   // number of points
  // store data in corridor
  corridor_whole.numPoints = N;
  for(int i = 0; i < N; i++)
    {
      // shouldn't get any segfaults here, but who knows...
      corridor_whole.e.push_back(waypoints[i].Easting);
      corridor_whole.n.push_back(waypoints[i].Northing);
      //shrink the corridor width to account for vehicle width, tracking error
      corridor_whole.width.push_back(waypoints[i].radius-RDDF_VEHWIDTH_EFF);
      corridor_whole.speedLimit.push_back(waypoints[i].maxSpeed);
      
      if (waypoints[i].radius-RDDF_VEHWIDTH_EFF < .01)
	cerr << "CORRIDOR WIDTH IS SMALLER THAN RDDF_VEHWIDTH_EFF!" << endl;
      
      //###
      //printf("waypoints[ %d ].Easting is %f\n", i, waypoints[i].Easting);
      //printf("waypoints[ %d ].Northing is %f\n", i, waypoints[i].Northing);
      //###
    }
//   cout << "done (read " << N << " points)." << endl;
  
  // generate and store whole path (sparse path, so max of 4 path points
  // per corridor point)
//   cout << "Generating complete (sparse) path... "; cout.flush();
  //  pathstruct path_whole_sparse = (pathstruct)Path_From_Corridor(corridor_whole);
  path_whole_sparse = Path_From_Corridor(corridor_whole);
  path_whole_sparse.currentPoint = 0; // we haven't went anywhere yet, so we're still at the start of the path
//   cout << "done." << endl;


  /** create an 'rddf' which contains the path, so that we can use herman to keep
   * track of where we are along the path */
  path_whole_sparse_rddf = new herman("CPath_sparse", NULL);
  RDDFData temp_point;
  cout << (int)path_whole_sparse.numPoints << endl;
  for (int i = 0; i < (int)path_whole_sparse.numPoints; i++)
    {
      temp_point.number = i;
      temp_point.distFromStart = (i==0) ? 0 :
        path_whole_sparse_rddf->getWaypointDistFromStart(i-1) +
        hypot(path_whole_sparse.n[i] - path_whole_sparse.n[i-1],
              path_whole_sparse.e[i] - path_whole_sparse.e[i-1]);
      temp_point.Northing = path_whole_sparse.n[i];
      temp_point.Easting = path_whole_sparse.e[i];
      temp_point.maxSpeed = 1234.5;
      temp_point.offset = path_whole_sparse.corWidth[i];
      temp_point.radius = temp_point.offset;
      path_whole_sparse_rddf->addDataPoint(temp_point);
    }
  cout << "done.  sparse path has " << path_whole_sparse.numPoints
       << " points, sparse path \'rddf\' has " << path_whole_sparse_rddf->getNumTargetPoints()
       << " points." << endl;
}




CPath::~CPath()
{
  // Do we need to destruct anything??
  //   cout << "CPath destructor has been called." << endl;
  delete path_whole_sparse_rddf;
}



double CPath::GetChopBehind() {return chop_behind;}
double CPath::GetChopAhead() {return chop_ahead;}
void CPath::SetChopBehind(double value) {chop_behind = value;}
void CPath::SetChopAhead(double value) {chop_ahead = value;}



/**
 * Gets a seed for the planner starting at a certain location and pointing in a
 * certain direction.
 */
void CPath::SeedFromLocation(double n, double e, double yaw, CTraj * destination_traj, 
			     double merge_length)
{
  UpdateState(n, e);

  vector<double> location(2);
  location[0] = n;
  location[1] = e;
  
  // update cur point using herman, not the old way.
  path_whole_sparse_rddf->updateState(m_state); // m_state was updated in GetLocation
  path_whole_sparse.currentPoint = path_whole_sparse_rddf->getCurrentPoint();

//   printf("chop_behind=%f, chop_ahead=%f, merge_length=%f", chop_behind, chop_ahead, merge_length); cout << endl;
  pathstruct chopped_sparse_path = ChopPath(path_whole_sparse,
					    chop_behind,
					    chop_ahead + merge_length);
//   printf("location 1: path length is %d",chopped_sparse_path.numPoints); cout << endl;
  pathstruct prechopped_dense_path = DensifyPath(chopped_sparse_path);
//   printf("location 2: path length is %d",prechopped_dense_path.numPoints); cout << endl;
  UpdateCurrentPoint(prechopped_dense_path, location);
//   printf("location 3: path length is %d",prechopped_dense_path.numPoints); cout << endl;

//   printf("info before: (%d, %d, %d)", 0, prechopped_dense_path.currentPoint, 
// 	 prechopped_dense_path.numPoints-1); cout << endl;
  MoveCurrentPoint(prechopped_dense_path, merge_length);
//   printf("info after: (%d, %d, %d)", 0, prechopped_dense_path.currentPoint, 
// 	 prechopped_dense_path.numPoints-1); cout << endl;


//   printf("location 4: path length is %d",prechopped_dense_path.numPoints); cout << endl;
  pathstruct part2 = ChopPath(prechopped_dense_path, 0.0, chop_ahead);
//   printf("location 5: path length is %d",part2.numPoints); cout << endl;

  // to do the spline, we need to compute a speed to use for the boundary
  // conditions using non-dimensionalized time (goes from 0 to 1).
  // we'll do this by using the same speed at the intial and final point,
  // and making this be a speed of 1.2 times the straight line distance
  // (to account for some curviness).
  double speed = 1.2 * hypot(part2.n[0] - n, part2.e[0] - e);
  double vn_0_ratio = cos(yaw);
  double ve_0_ratio = sin(yaw);
  double vn_1_ratio = part2.vn[0] / hypot(part2.vn[0], part2.ve[0]);
  double ve_1_ratio = part2.ve[0] / hypot(part2.vn[0], part2.ve[0]);
  pathstruct part1 = SplineFromHereToThere(n, vn_0_ratio*speed,
					   e, ve_0_ratio*speed,
					   part2.n[0], vn_1_ratio*speed,
					   part2.e[0], ve_1_ratio*speed, DENSITY);
//   printf("location 6: path length is %d",part1.numPoints); cout << endl;

  MergePaths(part1, part2);
//   printf("location 7: path length is %d",part1.numPoints); cout << endl;

  //write to traj
  StorePath(*destination_traj, part1);
}



void CPath::UpdateState(double n, double e)
{
  vector<double> location(2);
  location[0] = n;
  location[1] = e;
  
  // no longer needed because of herman
  //  UpdateCurrentPoint(path_whole_sparse, location);

  m_state.utmNorthing = n;
  m_state.utmEasting = e;
}
