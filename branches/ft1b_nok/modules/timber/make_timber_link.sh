#!/bin/bash

# make sure we have the right number of arguments
if (( $# != 2 )); then
    echo "ERROR:  make_timber_link.sh:  needs 2 arguments"
    exit 71
fi

# else
# check some conditions...
if [ -f $2 ]; then
    echo "ERROR:  $2 already exists, and is a regular file (not symlink)"
    exit 72
fi
if [ -d $2 -a ! -h $2 ]; then
    echo "ERROR:  $2 already exists, and is a directory (not symlink)"
    exit 73
fi

# remove old symlink and create new link
echo "rm -f $2 && ln -fns $1 $2" | /bin/sh
