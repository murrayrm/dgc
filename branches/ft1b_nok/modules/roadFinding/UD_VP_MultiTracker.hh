//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_VP_MultiTracker
//----------------------------------------------------------------------------

#ifndef UD_VP_MULTITRACKER_DECS

//----------------------------------------------------------------------------

#define UD_VP_MULTITRACKER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include "UD_VP_Tracker.hh"
#include "UD_VP_PositionTracker.hh"
#include "UD_VP_VelocityTracker.hh"

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_VP_MultiTracker : public UD_VP_Tracker
{
  
 public:

  UD_VP_PositionTracker *pos;
  UD_VP_VelocityTracker *vel;

  UD_VP_MultiTracker(UD_VanishingPoint *);

  void update();
  void pf_reset();

  void draw(int, int, int);
  void write(FILE *);
  void write(FILE *, char *);

  void pf_samp_prior(UD_Vector *);
  void pf_samp_state(UD_Vector *, UD_Vector *);
  void pf_dyn_state(UD_Vector *, UD_Vector *);
  double pf_condprob_zx(UD_Vector *);

  void set_show_particles(int);

  float candidate_vp_x(UD_Vector *v) { return v->x[0]; }       ///< x coordinate of VP location in generic state vector
  float candidate_vp_y(UD_Vector *v) { return v->x[1]; }       ///< y coordinate of VP location in generic state vector 
  float candidate_vp_vx(UD_Vector *v) { return v->x[2]; }      ///< x component of VP velocity in generic state vector
  float candidate_vp_vy(UD_Vector *v) { return v->x[3]; }      ///< y component of VP velocity in generic state vector 
  float vp_x(UD_Vector *v) { return vp->can2im_x(v->x[0]); }   ///< x coordinate of VP location in generic state vector (image coords.)
  float vp_y(UD_Vector *v) { return vp->can2im_y(v->x[1]); }   ///< y coordinate of VP location in generic state vector (image coords.)
  float vp_vx(UD_Vector *v) { return v->x[2]; }                ///< x component of VP velocity in generic state vector (image coords.)
  float vp_vy(UD_Vector *v) { return v->x[3]; }                ///< y component of VP velocity in generic state vector (image coords.)

  float candidate_vp_x() { return candidate_vp_x(pos->state); }     ///< x coordinate of estimated VP location in candidate region
  float candidate_vp_y() { return candidate_vp_y(pos->state); }     ///< y coordinate of estimated VP location in candidate region
  float candidate_vp_vx() { return candidate_vp_vx(pos->state); }   ///< x component of estimated VP velocity in candidate region
  float candidate_vp_vy() { return candidate_vp_vy(pos->state); }   ///< y component of estimated VP velocity in candidate region
  float vp_x() { return vp_x(pos->state); }                         ///< x coordinate of estimated VP location (image coords.)
  float vp_y() { return vp_y(pos->state); }                         ///< y coordinate of estimated VP location (image coords.)
  float vp_vx() { return vp_vx(pos->state); }                       ///< x component of estimated VP velocity (image coords.)
  float vp_vy() { return vp_vy(pos->state); }                       ///< y component of estimated VP velocity (image coords.)
  
};

UD_VP_MultiTracker *initialize_UD_VP_MultiTracker(UD_VanishingPoint *);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
