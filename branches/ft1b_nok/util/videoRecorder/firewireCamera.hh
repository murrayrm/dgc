#ifndef FIREWIRECAMERA_HH
#define FIREWIRECAMERA_HH

#include <libraw1394/raw1394.h>
#include <libraw1394/ieee1394.h>
#include <libdc1394/dc1394_control.h>

#include "unistd.h"

class CFirewireCamera
{
 public:
  CFirewireCamera();
  ~CFirewireCamera();

  /*
  **
  ** camera initialization and closing
  **
  */
  bool open(int num);
  bool open_uid(unsigned long long uid);
  bool open(const char *uid);

  //Is there a camera open
  bool isOpen() const;

  void close();

  /*
  **
  ** Camera information
  **
  */
  void print_all_info();


  /*
  **
  ** Features
  **
  */

  int width() const { return m_camera.frame_width; }
  int height() const { return m_camera.frame_height; }

  //Exposure
  double exposure();
  bool autoExposure();

  void setExposure(double d);
  void setAutoExposure(bool b);
  
  //Whitebalance
  double whitebalanceBU();
  double whitebalanceRV();
  void setWhitebalanceBU(double d);
  void setWhitebalanceRV(double d);

  /*
  **
  ** Image capture
  **
  */

  unsigned char *capture();   //Capture next image buffer
  void release();    //Release capture buffer

 protected:
  /*
  **
  ** Helper functions
  **
  */
  
  bool setupCamera(nodeid_t cam);

  /*
  **
  ** Data mambers
  **
  */
  raw1394handle_t m_handle;
  dc1394_cameracapture m_camera;

  unsigned int m_exposure_min;
  unsigned int m_exposure_max;

  unsigned int m_whitebalance_min;
  unsigned int m_whitebalance_max;
};



#endif //FIREWIRECAMERA_HH
