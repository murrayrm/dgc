#ifndef __MAP_CONFIG_HH__
#define __MAP_CONFIG_HH__

#include "Rectangle.hh"
#include "InterpolationMap.hh"
#include "rddf.hh"
#include "traj.h"
#include <gtkmm.h>
#include <list>
#include <math.h>
using namespace std;

#include "VehicleState.hh"
#include "interfaces/ActuatorState.h"

// forward declarations

// Mac OS X Users: If using bash shell,
// $ export CPPFLAGS=-DMACOSX
#ifdef MACOSX
#warning "Map coloring configured for PowerPC."
#endif

class  CMapPlus; 
class  MapWidget;

struct RGBA_COLOR
{
  double r, g, b, a;

  RGBA_COLOR( double r, double g, double b, double a=0 )
    : r(r), g(g), b(b), a(a)    
  {
    // Saturate in the range [0,1]
    r = max ( 0.0 , min( 1.0, r ));
    g = max ( 0.0 , min( 1.0, g ));
    b = max ( 0.0 , min( 1.0, b ));
    a = max ( 0.0 , min( 1.0, a ));
  }
  RGBA_COLOR( int ir, int ig, int ib, int ia=0 )
  {
    // Convert to double and saturate in the range [0,1]
    // assuming values of [0,255] input.
    r = max ( 0.0 , min( 1.0, (double)ir/255.0 ));
    g = max ( 0.0 , min( 1.0, (double)ig/255.0 ));
    b = max ( 0.0 , min( 1.0, (double)ib/255.0 ));
    a = max ( 0.0 , min( 1.0, (double)ia/255.0 ));
  }

  RGBA_COLOR()
    : r(0), g(0), b(0), a(0)    
  {}

  int get_RGBA_int()
  {
#ifndef MACOSX
    return  ( ((int) (a * 255.0) << 24) |
	      ((int) (b * 255.0) << 16) |
	      ((int) (g * 255.0) <<  8) |
	      ((int) (r * 255.0) <<  0) );
#else
    return  ( ((int) (r * 255.0) << 24) |
	      ((int) (g * 255.0) << 16) |
	      ((int) (b * 255.0) <<  8) |
	      ((int) (a * 255.0) <<  0) );
#endif
  }

};

RGBA_COLOR Interpolate( const RGBA_COLOR & lhs, const RGBA_COLOR & rhs, double lhs_c );




struct UTM_POINT
{
  double E, N;

  UTM_POINT()
    : E(0), N(0)
  {}
  UTM_POINT(double e, double n)
    : E(e), N(n)
  {}
  
  template <class T>
  UTM_POINT( const POINT2D<T> & rhs)
    : E(rhs.x), N(rhs.y)
  {}
  UTM_POINT operator +( const UTM_POINT & rhs) const
  {
    return UTM_POINT( E + rhs.E, N + rhs.N );
  }
};



struct RDDF_WAYPOINT
{
  int             num;
  UTM_POINT       utm;
  double          width;

  RDDF_WAYPOINT() {}
  RDDF_WAYPOINT( int num, const UTM_POINT &utm, const double & width )
    : num(num), utm(utm), width(width)
  {}
};

struct PATH
{
	pthread_mutex_t        pathMutex;
  CTraj                  points;
  RGBA_COLOR             color;
  bool draw;
  PATH() : color(1.0,0.0,0.0,0.0), draw(true) {}
};



class MapConfig
{
  friend class MapWidget;
  
public: // Change these as needed

  // Where to center the map (if drop_anchor is true)
  // otherwise it will be centered based on the cmap
  // or the state estimator (using the sigc interface)
  bool                 m_drop_anchor;
  UTM_POINT            m_anchor_pos;

  // For drawing sensor swath...
  RGBA_COLOR           m_sensor_color;
  double               m_sensor_angle;
  int                  m_sensor_num_arcs;
  double               m_sensor_max_arc;

  // Miscellaneous drawing parameters
  RGBA_COLOR           m_clear_color;         // background color
  RGBA_COLOR           m_border_color;        // info border color
  RGBA_COLOR           m_rddf_color;          // rddf color
  RGBA_COLOR           m_rddf_number_color;   // waypoint numering
  RGBA_COLOR           m_cmap_border_color;   // border around cmap
  RGBA_COLOR           m_grid_color;          // color for gridlines
	RGBA_COLOR           m_steercmd_color;      // color for the steering command

  double               m_line_width;          // width of gridlines, rddf and paths
  RGBA_COLOR           m_info_text_color;     // color for grid text
  RGBA_COLOR           m_colormap_text_color; // colormap text color
  InterpolationMap<double, RGBA_COLOR> m_colormap;

protected:
  /**
   * Signal for connecting the save file button
   * to the method in MapDisplaySN
   *
   */
  sigc::signal1<void, ostream*> * m_writeLogSig;
  Glib::RecMutex       m_mutex;

  // CMAP stuff 
  CMapPlus  *              m_cmap; // pointer -- the cmap  
	pthread_mutex_t*     m_cmap_mutex;

  // Local Copy of RDDF waypoints
  RDDFVector  m_rddf;

  // Local Copy of Paths to draw
  PATH* m_paths;
	int   m_numPaths;

  // If no CMap and no anchor, need a way to get state of vehicle
  VehicleState*  m_ss;

	ActuatorState* m_pActuatorState;

public: // Accessors for the above
  CMapPlus* get_cmap() const 
  {
    return m_cmap;
  }
  pthread_mutex_t* get_cmap_mutex() const 
  {
    return m_cmap_mutex;
  }
  RDDFVector  get_rddf()
  {
    Glib::RecMutex::Lock lock(m_mutex);
    return m_rddf;
  }
	RDDFData&  get_rddf_data(int index)
	{
		Glib::RecMutex::Lock lock(m_mutex);
		return m_rddf[index];
	}

  void get_paths(PATH** paths, int* numPaths)
  {
    Glib::RecMutex::Lock lock(m_mutex);
    (*paths) = m_paths;
		(*numPaths) = m_numPaths;
  }
  VehicleState* get_state_struct()
  {
    return m_ss;
  }
  ActuatorState* get_actuator_state_struct()
  {
    return m_pActuatorState;
  }
  void activate_writeLogSig()
  {
    m_writeLogSig->emit(NULL);
  }

  
public: // Mutators for the above
  void set_cmap(CMapPlus* pc, pthread_mutex_t* pMapdeltaMutex)
  {
    Glib::RecMutex::Lock lock(m_mutex);
    m_cmap = pc;
		m_cmap_mutex = pMapdeltaMutex;
  }
  void set_rddf(const RDDFVector & rddf)
  {
    Glib::RecMutex::Lock lock(m_mutex);
    // save a copy of the passed rddf
    m_rddf = rddf;
  }
  void set_paths(PATH* paths, int numPaths)
  {
    Glib::RecMutex::Lock lock(m_mutex);
    m_paths = paths;
		m_numPaths = numPaths;
  }
  void set_state_struct(VehicleState * ss)
  {
    Glib::RecMutex::Lock lock(m_mutex);
    m_ss = ss;    
  }
  void set_actuator_state_struct(ActuatorState* pActuatorState)
  {
    Glib::RecMutex::Lock lock(m_mutex);
    m_pActuatorState = pActuatorState;
  }
  void set_writeLogSig(sigc::signal1<void, ostream*> * writeLogSig)
  {
    m_writeLogSig = writeLogSig;
  }

  // Constructor
  MapConfig()
    : m_drop_anchor(false),
      m_sensor_color( 0.0, 0.0, 1.0 ), // blue
      m_sensor_angle( M_PI / 3.0 ), // angle of 60deg
      m_sensor_num_arcs( 3 ),
      m_sensor_max_arc( 50 ), // 50m
      m_clear_color(1.0, 1.0, 1.0, 0.0),
      m_border_color( 0.0,  0.0,  0.0,  0.0),
      m_rddf_color( 0.0, 0.0, 1.0 ),           // blue
      m_rddf_number_color( 1.0, 0.0, 0.0),  // red
      m_cmap_border_color( 0.5, 0.5, 0.0, 0.0 ),
      m_grid_color( 0.0,  0.0,  0.0,  0.0),
			m_steercmd_color( 0.0, 1.0, 0.0),
      m_line_width(2.0),
      m_info_text_color( 1.0, 1.0, 1.0, 1.0 ),
      m_colormap_text_color( 1.0, 1.0, 1.0, 1.0 ),
      m_cmap(NULL),
      m_ss(NULL),
      m_pActuatorState(NULL)
  {
    
    // Build a default colormap mapping the range 
    // 0->255 to different colors
    {
      m_colormap.add_entry( 0,   RGBA_COLOR( 0x00, 0x00, 0x33 )); // BLUE
      m_colormap.add_entry( 23,  RGBA_COLOR( 0x00, 0x33, 0xFF )); // 
      m_colormap.add_entry( 47,  RGBA_COLOR( 0x00, 0x66, 0xFF )); // 
      m_colormap.add_entry( 71,  RGBA_COLOR( 0x00, 0xCC, 0xFF )); //  
      m_colormap.add_entry( 95,  RGBA_COLOR( 0x00, 0x99, 0x66 )); //  
      
      m_colormap.add_entry( 127, RGBA_COLOR( 0x00, 0x66, 0x00 )); // GREEN
      
      m_colormap.add_entry( 151, RGBA_COLOR( 0x66, 0x99, 0x00 )); // 
      m_colormap.add_entry( 175, RGBA_COLOR( 0xFF, 0xCC, 0x00 )); //  
      m_colormap.add_entry( 199, RGBA_COLOR( 0xFF, 0x66, 0x00 )); //  
      m_colormap.add_entry( 223, RGBA_COLOR( 0xFF, 0x33, 0x00 )); // 
      m_colormap.add_entry( 255, RGBA_COLOR( 0x33, 0x00, 0x00 )); // RED
    }
    
  } // end constructor
  
protected:
  // More advanced stuff -- do not use.
  sigc::signal0<void> m_cmap_notify;
  sigc::signal0<void> m_map_display_update_notify;
  
public:
  void add_cmap_notifier( sigc::slot0<void> cmap_notify_functor )
  {
    m_cmap_notify.connect( cmap_notify_functor );
  }
  
  void notify_update() 
  {
    m_map_display_update_notify();
  }

  void add_update_notify( sigc::slot0<void> map_update_notify_functor )
  {
    m_map_display_update_notify.connect(map_update_notify_functor);
  }


  ~MapConfig()
  {

  }
};



#endif
