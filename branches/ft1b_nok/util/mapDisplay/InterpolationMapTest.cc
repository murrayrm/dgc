#include "InterpolationMap.hh"

#include <iostream>
using namespace std;


double Interpolate( double lh, double rh, double lh_c )
{
  return lh * lh_c + rh * (1.0-lh_c);
}

int main () {

  InterpolationMap< double, double > my_map;

  my_map.add_entry( 0, 5 );
  my_map.add_entry( 5, 0 );
  my_map.add_entry( 3, 8 );

  double i;
  for( i=-4.0; i< 10.0; i+= 0.5 ) {
    cout << "table(" << i << ") = " << my_map.get_value( i ) << endl;
  }
}
