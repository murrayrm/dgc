package edu.caltech.me75;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * a Segment class
 * @author Xiang Jerry He
 *
 */

public class Segment {
	      /** the id of this segment */
	      protected int id;
	      /** number of lanes */
		  protected int num_lanes;
		  /** name of this segment */
		  protected String segment_name;
		  /** list of all lanes in this segment */
		  protected Lane[] lanes; 
		  
			/**
			 * Read in and parse a Segment
			 * @author Xiang Jerry He
			 * @param in the buffered reader for the RNDF
			 * @throws IOException if we fail to read in the file
			 * @throws MapFormatException if the file is incorrectly formatted
			 */
		  void readSegment(BufferedReader in) throws IOException, MapFormatException {
			  System.out.println("Entering segment");
			  String line = in.readLine();
			  System.out.println(line);
			  String[] tokens = line.split("\\s");
			  while(!line.startsWith("lane")) {
			       System.out.println(line);
			       //if it is not an empty line, then process it
			       if(line.matches("\\w.*")) {
				       tokens = line.split("\\s");
				       if(line.startsWith("num_lanes")) {
				    	   System.out.println("numlanetoken: " + tokens[1]);
				    	   num_lanes = Integer.parseInt(tokens[1]);
				       }
				       else if(line.startsWith("segment_name")) {
				    	   segment_name = tokens[1];
				       }
			       }
			       line = in.readLine();
			  }
			  System.out.println("number of lanes is " + num_lanes);
			  System.out.println("segment name is "+ segment_name);
			  System.out.println(line);
			  lanes = new Lane[num_lanes];
			  for(int i=0; i < num_lanes; i++) {
				  lanes[i] = new Lane();
				  lanes[i].readLane(in);
			  }
			  assert(in.readLine().equals("end_segment"));
		  }
	  
	}
	