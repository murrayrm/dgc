package edu.caltech.me75;
import java.io.*;
import java.awt.BorderLayout;
import java.awt.event.*;

import javax.swing.*;

/**
 * A class to represent the controls of the Maze
 * @author Xiang Jerry He
 */
public class MapViewer {

	/** default size of the MapViewer */
	public static final int DEFAULTSIZE = 80;
	/** the Frame of our display */
	private JFrame myWindow;
	/** the top-most Panel used to put buttons */
	protected JPanel buttonPanel;
	/** x-coordinate of the current mouse position measured either in LatLon or UTM*/
	protected final JTextField xcoord = new JTextField("0.0", 8);
	/** y coordinate of the current mouse position measured either in LatLon or UTM*/
	protected final JTextField ycoord= new JTextField("0.0", 8);
	/** The map viewing component */
	private MapView MapPanel;
	/** the map object to be drawn */
	private Map m;
	
	private Action fileOpenAction = new AbstractAction("Open RNDF...") {
		public void actionPerformed(ActionEvent e) {
			doOpen();
		}
	};
	private Action fileExitAction = new AbstractAction("Exit") {
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	};
	
	private Action modeUTMAction = new AbstractAction("UTM") {
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(myWindow, "not yet implemented");
		}
	};
	
	private Action modeLatLonAction = new AbstractAction("LatLon") {
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(myWindow, "not yet implemented " +
					"but LatLon is the default behavior");
		}
	};
	
	private Action stopSignAction = new AbstractAction("StopSigns") {
		public void actionPerformed(ActionEvent e) {
			MapPanel.toggleStopSignVisibility();
			MapPanel.repaint();
		}
	};
	
	/** sets up the menus */
	private void createMenus(JFrame myWindow) {
		
		JMenuBar mb = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		fileMenu.add(fileOpenAction);
		fileMenu.add(fileExitAction);
		mb.add(fileMenu);

		JMenu modeMenu = new JMenu("Mode");
		modeMenu.add(modeUTMAction);
		modeMenu.add(modeLatLonAction);
		mb.add(modeMenu);
		
		JMenu viewMenu = new JMenu("View");
		viewMenu.add(stopSignAction);
		mb.add(viewMenu);
		
		myWindow.setJMenuBar(mb);		
	}
        /** Construct an instance of the Map application by creating and
	      *  displaying its main window.
         */
	public MapViewer() {
		m = new Map();
	    myWindow = new JFrame("Map Viewer");
	    buttonPanel = new JPanel();
	    JPanel mainPanel = new JPanel();
	    MapPanel = new MapView(this);	
	    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
             mainPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        mainPanel.add(buttonPanel);
	    mainPanel.add(MapPanel);
	    buttonPanel.add(new JLabel("LatLon coordinate"));
	    buttonPanel.add(xcoord);
	    buttonPanel.add(ycoord);
	    MapPanel.setBorder(BorderFactory.createCompoundBorder(
		BorderFactory.createTitledBorder(""), 
		BorderFactory.createEmptyBorder(300,400,300,400)));		
	    mainPanel.add(MapPanel, BorderLayout.SOUTH);
	    createMenus(myWindow);
	    myWindow.setContentPane(mainPanel); 
	    myWindow.pack();
	    myWindow.setVisible(true);
	    myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      }
	/** Actions to perform when user clicks File->Open */
	void doOpen() {
		JFileChooser file = new JFileChooser("testRNDF");
        int result = file.showOpenDialog(null);
        if(result == JFileChooser.APPROVE_OPTION) {
        File f = file.getSelectedFile();
	    try {
		    Reader r = new FileReader(f);
		    BufferedReader br = new BufferedReader(r);
		    try { m.readMap(br); 
			      MapPanel.setMap(m);
			      myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			      myWindow.setTitle(m.info.get("RNDF_name"));
			  }
		    catch(IOException error) {
			     JOptionPane.showMessageDialog(null, error.toString());
		    }
		    catch(MapFormatException error) {
		    	JOptionPane.showMessageDialog(null, f.getName() +
		    			" is malformed!");
		    }
	     } catch (FileNotFoundException error2) {
		 JOptionPane.showMessageDialog(null, "File not found: " + f.getName());
	         }
	     } else {
	     JOptionPane.showMessageDialog(null, "No file selected");
		}
	}
  
 /**
	 * @param args The command-line arguments; if there is at least one and 
	 * the first is "-sysLAF", the application will use the system-specific 
	 * look and feel.
	 */
	public static void main(String[] args) {

		
		if (args.length > 0 && args[0].equals("-sysLAF")) {
			try {
				// Try to use a system-specific look and feel...
				javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
			} catch ( Exception e ) {
				// It didn't work.  What a shame.  We can still run.
				System.err.println(
						"-sysLAF: Unable to set platform look and feel.  Continuing with \n" +
						"default look and feel.");
			}
		}

		
		new MapViewer();
		
	}
 }
