package edu.caltech.me75;
import java.io.IOException;
import java.util.Vector;
import java.util.regex.Pattern;
import java.util.StringTokenizer;
import java.io.BufferedReader;

import javax.swing.JOptionPane;

/**
 * A class to represent a Lane
 * @author Xiang Jerry He
 */

public class Lane {
	    /** 
	     * an array of ids of the waypoints in this lane 
	     */
		int[] id;
		/** 
		 * number of waypoints on this lane 
		 */
		int num_waypoints;
		/** the width of this lane */
		int lane_width;
		
		Vector<Integer> stopSignIndex;
		/** a vector of waypoints that correspond to stop signs */
		Vector<WayPoint> stopSign;
		/** the left boundary type */
		String left_boundary;
		/** the right boundary type */
		String right_boundary;
		/** collection of all waypoints in this lane */
		Vector<WayPoint> waypoints;
        
		/** A list of boundaries */
		//TODO change the string based boundary system to enum based
		private static enum Boundary {
			double_yellow, broken_white;
		}
		
		/**
		 * Read in and parse a lane, gathers all lane-specific information  
		 * @author Xiang Jerry He
		 * @param in the buffered reader for the RNDF
		 * @throws IOException if we fail to read in the file
		 * @throws MapFormatException if the file is incorrectly formatted
		 */
		protected void readLane(BufferedReader in) throws IOException, MapFormatException {
			System.out.println("\tEntering lane");
			waypoints = new Vector<WayPoint>();
			Pattern p = Pattern.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}.*");
			String line = in.readLine();
			System.out.println(line);
			while(!line.startsWith("end_lane")) {
				StringTokenizer tok = new StringTokenizer(line);
				if(tok.hasMoreTokens()) {  // if not then we have an empty line so do nothing
					String firstToken = tok.nextToken();
					if(firstToken.equals("num_waypoints")) {
						num_waypoints = Integer.parseInt(tok.nextToken()); 
						stopSignIndex = new Vector<Integer>(num_waypoints);
					}
					else if(firstToken.equals("lane_width")) {
						lane_width = Integer.parseInt(tok.nextToken());
					}
					else if(firstToken.equals("stop")) {
						String identifier = tok.nextToken(); 
						String[] iden = identifier.split(".");
						System.out.println("identifier is " + identifier);
						id = new int[3];
						for(int i=0; i < iden.length; i++) {
							id[i] = Integer.parseInt(iden[i]);
						}
						if(id.length > 0) {
						  stopSignIndex.add(id[id.length-1]);
						}
					}
					else if(p.matcher(firstToken).matches()) {
						String[] identifier = firstToken.split(".");
						id = new int[identifier.length];
						for(int i=0; i < identifier.length; i++) {
							id[i] = Integer.parseInt(identifier[i]);
						}
						double x = Double.parseDouble(tok.nextToken());
						double y = Double.parseDouble(tok.nextToken());
						waypoints.add(new WayPoint(id, x, y));
						System.out.println(x + " " + y);
					}
				}
				line = in.readLine();
			}
			stopSign = new Vector<WayPoint>(stopSignIndex.size());
			for(int i: stopSignIndex) {
				stopSign.add(waypoints.get(i));
			}
		}
	}