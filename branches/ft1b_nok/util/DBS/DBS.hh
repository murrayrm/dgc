#ifndef DBS_HH
#define DBS_HH

#include "StateClient.h"
#include <pthread.h>
#include <unistd.h>
#include <sstream>
#include "AliceConstants.h"
#include "sn_msg.hh"
#include "sn_types.h"
#include <iomanip>
#include <string>
#include "DGCutils"
#include <math.h>
#include <fftw3.h>
#include <map>
#include "gnuplot_i.h"
#include "trajF_speedCap_cmd.hh"
#include <fstream>
#include "CTimberClient.hh"

using namespace std;


#define DBS_NUM_VALUES 40
#define MAXRANGE 2
#define FREQUENCY 2
//#define AVESIZE 10
#define MIN_ALLOWED_SPEED 2.0       /**< the slowest we'll ever report for our
				     * max speed */
#define HISTORY_AVE_LENGTH    50    /**< must be <= HISTORY_LENGTH */
#define HISTORY_INST_LENGTH    5
#define USE_SPEED_WEIGHTING    1    /**< 1 or 0 */
#define OVERALL_BUMP_SCALE   1.3
#define IGNORE_PAUSED_PERIODS  1
#define PAUSE_SPEED           .5

#define ASTATE_FIELDS 19
#define ASTATE_FIELD_FFT_START 2    // don't make this < 2
#define ASTATE_FIELD_FFT_END   19

/** plotting parameters */
#define DO_CURRENT_INFO_PLOT  0
#define CURRENT_INFO_RANGE    1     /**< from -value to value */

#define DO_HISTORY_PLOT       0
#define HISTORY_LENGTH        150
#define HISTORY_RANGE_MIN     0
#define HISTORY_RANGE_MAX     30


/** name, multiplier*/
#define HIST_INFO(_) \
_(pitch_1_20,         20) \
_(zdot_1_20,           1) \
_(zdot_10_20,         18) \
_(zdd_1_20,            1) \
_(pitchdd_1_20,       10) \
_(speed,               1) \
_(zdd_bump,           30) \
_(pitchdd_bump,       30) \
_(bumpiness,          30) \
_(worst_bumpiness,    30) \
_(decaying_bumpiness, 30) \
_(fit_bumpiness,      30) \
_(fit_inst_bumpiness, 30) \
_(speed_cap,           1) \
_(worst_speed_cap,     1) \
_(decaying_speed_cap,  1) \
_(fit_speed_cap,       1) \
_(coeff_zdd_1,         1) \
_(coeff_pitchdd_1,    30) \
_(coeff_zdd_4,         1) \
_(coeff_pitchdd_4,    30)


/** name, enum val, state mulitiplier (for plot), fft multiplier (for plot) */
#define STATE_INFO(_) \
_(Timestamp,=1, 1  , 1  ) \
_(Northing,   , 1  , 1  ) \
_(Easting,    , 1  , 1  ) \
_(Altitude,   , 1  , 1  ) \
_(Vel_N,      , 1  , 1  ) \
_(Vel_E,      , 1  , 1  ) \
_(Vel_D,      , 1  , 1  ) \
_(Acc_N,      , 1  , 1  ) \
_(Acc_E,      , 1  , 1  ) \
_(Acc_D,      , 1  , 1  ) \
_(Roll,       , 1  , 1  ) \
_(Pitch,      , 1  , 1  ) \
_(Yaw,        , 1  , 1  ) \
_(RollRate,   , 1  , 1  ) \
_(PitchRate,  , 1  , 1  ) \
_(YawRate,    , 1  , 1  ) \
_(RollAcc,    , 1  , 1  ) \
_(PitchAcc,   , 1  , 1  ) \
_(YawAcc,     , 1  , 1  )

#define HIST_INDEX(hist_enum, i) hist_enum * HISTORY_LENGTH + i

#define STATE_ENUM(name, val, s_mult, f_mult) name val,
#define HIST_ENUMS(name, mult) name,

#define STATE_FIELD_MAP(name, val, s_mult, f_mult) state_field_map[asf::name] = \
(string)#name + " (x " + #s_mult + ")";
#define STATE_FIELD_FFT_MAP(name, val, s_mult, f_mult) state_field_fft_map[asf::name] = \
(string)#name + " fft (x " + #f_mult + ")";
#define HIST_FIELD_MAP(name, mult) hist_field_map[hist::name] = (string)#name + " (x " + #mult + ")";
#define HIST_MULT_MAP(name, mult) hist_multiplier_map[hist::name] = mult;
#define STATE_MULT_MAP(name, val, s_mult, f_mult) state_multiplier_map[asf::name] = s_mult;
#define STATE_FFT_MULT_MAP(name, val, s_mult, f_mult) state_fft_multiplier_map[asf::name] = f_mult;

namespace asf // vehicle state fields
{
  enum the_enum
    {
      STATE_INFO(STATE_ENUM)
      LAST
    };
}

namespace areas
{
  enum areas_enum
    {
      pitch_1_20 = 0,
      zdot_1_20,
      zdot_10_20,
      zdd_1_20,
      pitchdd_1_20,
      LAST
    };
}

namespace hist
{
  enum hist_enum
    {
      HIST_INFO(HIST_ENUMS)
      LAST
    };
}

#define LOG_FILE "dbs.dat"


/**
 * DBS class.
 * input: state
 * output: bumpiness, speed cap
 */
class DBS : public CStateClient, public CTimberClient
{
public:
  /** Contstructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  DBS(int skynetKey, bool run_active_in_background = true, bool show_info_plots = DO_CURRENT_INFO_PLOT, bool show_hist_plots = DO_HISTORY_PLOT);

  /** Standard destructor */
  ~DBS();

  /**************************************
   * Accessors                          *
   **************************************/
  
  /** returns the instantaneous bumpiness */
  double GetInstBumpiness();

  /** returns the time-smoothified bumpiness */
  double GetDecayingBumpiness();

  /** returns the speedcap (based on the time-decaying bumpiness) */
  double GetSpeedCap();

  /** returns whether or not the current info plot is enabled */
  bool GetDoCurrentInfoPlot();

  /** returns whether or not the history plot is enabled */
  bool GetDoHistoryPlot();


  /**************************************
   * Mutators                           *
   **************************************/

  /** turn on or off the current info plot */
  void SetDoCurrentInfoPlot(bool new_val);

  /** turn on or off the history plot */
  void SetDoHistoryPlot(bool new_val);

  /** Main program loop... execution is trapped here */
  void DBSActiveLoop();

  //Return all mean values
  void stats(VehicleState &means, VehicleState &vars);
  
  void fftcalculatorLastN();
  //int GetRoadType();


private:
  /** Array that stores the offsets within a VehicleState struct for all
   * the different elements */
  double VehicleState::*vs_table[ASTATE_FIELDS+1];

  void PrintValue(fftw_complex* fft);
  void PrintDebug(fftw_complex* in, fftw_complex* out);
  void PrintValueSum(fftw_complex* fftA);

  bool first_ignore;



  /********************************************************
   *
   * DBS State stuff
   *
   ********************************************************/

  /** retrieves the most recent state values and recalculates all FFT's
   * and area stuff */
  void UpdateDBSState();

  struct DBS_state
  {
    /** State buffer (last DBS_NUM_VALUES) */
    VehicleState astate_buffer[DBS_NUM_VALUES];

    /** Most recent FFT's 
     * (ASTATE_FIELDS + 1 because field 19 requires
     * 20 spaces) */
    double astate_fft[ASTATE_FIELDS + 1][DBS_NUM_VALUES];

    /** Area under most recent FFT's */
    struct FFTArea
    {
      double area;
      int field;
      int start;
      int end;
    };

    FFTArea* fft_area;

    double recent_speed;
    bool ignore_this_iteration;

    /** DBS_state constructor */
    DBS_state()
    {
      for (int field = 0; field < ASTATE_FIELDS; field++)
	for (int i = 0; i < DBS_NUM_VALUES; i++)
	  astate_fft[field][i] = 0;
      //  cout << "DBS_state constructor, creating new fft_area with length "
      //   << areas::LAST << "." << endl;
      fft_area = new FFTArea[areas::LAST];
      for (int i = 0; i < areas::LAST; i++)
	{
	  this->fft_area[i].area = 0;
	}
      
      fft_area[areas::pitch_1_20].field = asf::Pitch;
      fft_area[areas::pitch_1_20].start = 1;
      fft_area[areas::pitch_1_20].end = 20;
      fft_area[areas::pitchdd_1_20].field = asf::PitchAcc;
      fft_area[areas::pitchdd_1_20].start = 1;
      fft_area[areas::pitchdd_1_20].end = 20;
      fft_area[areas::zdot_1_20].field = asf::Vel_D;
      fft_area[areas::zdot_1_20].start = 1;
      fft_area[areas::zdot_1_20].end = 20;
      fft_area[areas::zdot_10_20].field = asf::Vel_D;
      fft_area[areas::zdot_10_20].start = 10;
      fft_area[areas::zdot_10_20].end = 20;
      fft_area[areas::zdd_1_20].field = asf::Acc_D;
      fft_area[areas::zdd_1_20].start = 1;
      fft_area[areas::zdd_1_20].end = 20;

      ignore_this_iteration = false;
      recent_speed = 0;
    }
    ~DBS_state() { delete [] fft_area; }
  };

  /* keeps track of anything we need from one iteration of DBS to
   * the next */
  DBS_state * m_dbs_state;



  /********************************************************
   *
   * FFT stuff
   *
   ********************************************************/

  /** handle to the FFT plan, which is generated once and run many
   * times*/
  fftw_plan plan;

  /** buffers used for the FFT function */
  fftw_complex *fft_in;
  fftw_complex *fft_out;



  /********************************************************
   *
   * Plotting stuff
   *
   ********************************************************/

  /** updates the plotting state based on the new m_dbs_state */
  void UpdatePlotStateOne();

  /** updates the plotting state based on the new conclusins */
  void UpdatePlotStateTwo();

  /** Plots the current ffts and states */
  void PlotCurrentInfo();
  gnuplot_ctrl* current_info_plot;

  void PlotAstateField(int field);
  void PlotFFT(int field);

  /** Plots the history of variables */
  void PlotHistory();
  gnuplot_ctrl* history_plot;

  void PlotThisHist(int hist_field);

  /** use the push functions to keep track of stuff over time */
  void PushArea(int hist_field, int area);
  void PushSpeed(int hist_field);
  void PushDouble(int hist_field, double value);
  
  struct Plot_state
  {
    double* current_info_ranges;
    double* history_ranges;

    double* history_data;
  };

  /** keeps track of everthing needed for plotting */
  Plot_state * m_plot_state;

  bool do_current_info_plot;
  bool do_history_plot;
  pthread_mutex_t do_plot_mux;

  /** map from ints to names of fields (for labeling
   * the graphs) */
  map<int, string> state_field_map;
  map<int, string> state_field_fft_map;
  map<int, double> state_multiplier_map;
  map<int, double> state_fft_multiplier_map;
  map<int, string> hist_field_map;
  map<int, double> hist_multiplier_map;



  /********************************************************
   *
   * Conclusions
   *
   ********************************************************/
  void UpdateConclusions();


  struct DBS_Conclusions
  {
    double bumpiness;
    double worst_bumpiness;
    double decaying_bumpiness;
    double fit_bumpiness;
    double fit_inst_bumpiness;
    double speed_cap;
    double worst_speed_cap;
    double decaying_speed_cap;
    double fit_speed_cap;
    double pitchdd_bump;
    double zdd_bump;
  };

  DBS_Conclusions m_conc;
  DBS_Conclusions m_conc_copy; /**< the copy is the one that is used for
				* the accessor functions... this makes 
				* mutexing easier */
  pthread_mutex_t m_conc_copy_mux;
  bool m_conc_copy_initialized;

  double Speedfn(double speed);
  double TimeWeightingFunction(double time_ago);
  double SetMaxSpeed();
  double BumpinesstoSpeed(double bp);
  double CalculateDecayingBumpiness();
  double CalculateFitBumpiness(int history_ave_length, bool push_hists = true);


  /********************************************************
   *
   * Timber Logging
   *
   ********************************************************/
  bool logging_enabled;
  int logging_level;
  string logging_location;
  ofstream logging_file;

  void Log();
};


#endif  // DBS_HH
