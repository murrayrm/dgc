#ifndef _SKYNETCONTAINER_H_
#define _SKYNETCONTAINER_H_

#include "sn_msg.hh"
#include "sn_types.h"
#include <iostream>
using namespace std;

// This is a dummy class that just contains a skynet object. All module helpers
// virtually derive from this to get the skynet object. This class exists to
// avoid deriving directly from skynet, which would be unclean
class CSkynetContainer
{
protected:
	skynet m_skynet;

public:
	CSkynetContainer()
		: m_skynet(-1, -1)
	{
		cerr << "Called CSkynetContainer(). THIS IS AN ERROR. WE SHOULD NOT BE HERE." << endl;
	}
	CSkynetContainer(int snname, int snkey, int* status=NULL)
		: m_skynet(snname, snkey, status)
	{}
};

#endif // _SKYNETCONTAINTER_H_
