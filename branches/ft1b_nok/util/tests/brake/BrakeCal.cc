#include <iostream>
#include <iomanip>
#include <fstream>

#include "SDSPort.h"
#include "brake.h"

using namespace std;
char simulator_IP[100];
int main(void)
{
  brake_open(2);
  ofstream test_output;
  char testFileName[256];
  sprintf(testFileName, "test.dat");
test_output.open(testFileName);
test_output << setprecision(20);


  for(int i = 0; i <= 15; i++)
  {
    brake_setposition((i/15.0));
    usleep(1E6);
    int answer = brake_getpressure();
    cout<<"brake_getpressure returned :"<<answer<<endl;
    test_output<<i<<' '<<answer<<endl;
  }
  brake_setposition(0);
}
