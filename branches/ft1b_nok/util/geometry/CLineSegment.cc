#include "CLineSegment.hh"

CLineSegment::CLineSegment(NEcoord start, NEcoord end) {
  NEcoord tempPoint;
  if(start.N > end.N) {
    tempPoint = start;
    start = end;
    end = tempPoint;
  }
  
  startPoint = start;
  endPoint = end;

  vertical = 0;

  if(start.E==end.E) {
    vertical = 1;
    x_intercept = start.N;
  } else {
    slope = (start.E-end.E)/(start.N-end.N);
    y_intercept = start.E-slope*start.N;
  }
}


CLineSegment::~CLineSegment() {
  
}

int CLineSegment::checkIntersection(const CLineSegment& other,
				     NEcoord& intersectionPoint) {
  if(this->vertical && other.vertical) {
    if(this->x_intercept == other.x_intercept &&
       (this->checkBetweenE(other.startPoint.E) ||
	this->checkBetweenE(other.endPoint.E))) {
      return INTERSECTION_COLLINEAR;
    } else {
      return INTERSECTION_NO;
    }
  } else if(this->vertical) {
    if(other.checkBetweenN(this->startPoint.N) && 
       this->checkBetweenE(other.slope*this->startPoint.N+other.y_intercept)) {
      intersectionPoint = NEcoord(this->startPoint.N,
				  other.slope*this->startPoint.N+other.y_intercept);
      return INTERSECTION_YES;
    } else {
      return INTERSECTION_NO;
    }
  } else if(other.vertical) {
    if(this->checkBetweenN(other.startPoint.N) &&
       other.checkBetweenE(this->slope*other.startPoint.N+this->y_intercept)) {
      intersectionPoint = NEcoord(other.startPoint.N,
				  this->slope*other.startPoint.N+this->y_intercept);
      return INTERSECTION_YES;
    } else {
      return INTERSECTION_NO;
    }
  } else {
    double x_val = (other.y_intercept-this->y_intercept)/(this->slope-other.slope);
    if(this->checkBetweenN(x_val) &&
       other.checkBetweenN(x_val)) {
      intersectionPoint = NEcoord(x_val,
				  this->slope*x_val+this->y_intercept);
      return INTERSECTION_YES;
    } else {
      return INTERSECTION_NO;
    }
  }
}


bool CLineSegment::checkBetweenN(double value) const {
  return ((this->startPoint.N <= value) && (value <= this->endPoint.N));
}


bool CLineSegment::checkBetweenE(double value) const {
  if(this->startPoint.E < this->endPoint.E) {
    return ((this->startPoint.E <= value) && (value <= this->endPoint.E));
  } else {
    return ((this->endPoint.E <= value) && (value <= this->startPoint.E));
  }
}
