/* 
 * Desc: SensNet stereo blob
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef SENSNET_STEREO_H
#define SENSNET_STEREO_H

#include <stdint.h>
#include "sensnet_types.h"

/** @file

@brief Stereo blob and some useful accessors.

*/

/// @brief Maximum image dimensions
#define SENSNET_STEREO_BLOB_MAX_COLS 640

/// @brief Maximum image dimensions
#define SENSNET_STEREO_BLOB_MAX_ROWS 480
  

/// @brief Stereo image blob data.
///
/// The blob contains both color and disparity images for the left camera.
typedef struct
{  
  /// Blob type (must be SENSNET_STEREO_BLOB)
  int blob_type;

  /// Sensor ID for originating sensor
  int sensor_id;

  /// Frame id
  int frameid;

  /// Image timestamp
  double timestamp;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  sensnet_state_t state;

  /// Camera calibration data: (cx, cy) is the image center in pixels,
  /// (sx, sy) is the focal length in pixels.
  float cx, cy, sx, sy;
  
  // Stereo baseline (m).
  float baseline;

  /// Camera-to-vehicle transformation (homogeneous matrix)
  float sens2veh[4][4];

  /// Vehicle-to-local transformation (homogeneous matrix)
  float veh2loc[4][4];

  /// Disparity scaling factor
  float disp_scale;
  
  /// Image dimensions 
  int cols, rows;

  /// Number of color channels (1 for mono, 3 for RGB)
  int color_channels;

  /// Size of color image data (8-bits/pixel)
  uint32_t color_size;
  
  /// Color/grayscale image data
  uint8_t color_data[SENSNET_STEREO_BLOB_MAX_COLS * SENSNET_STEREO_BLOB_MAX_ROWS * 3];

  /// Size of disparity image (16-bits/pixel)
  uint32_t disp_size;

  /// Disparity data
  uint16_t disp_data[SENSNET_STEREO_BLOB_MAX_COLS * SENSNET_STEREO_BLOB_MAX_ROWS * 1];
  
} sensnet_stereo_blob_t __attribute__((packed));


/// @brief Get the disparity value at the given image location
static __inline__ 
uint16_t sensnet_stereo_get_disp(sensnet_stereo_blob_t *self,
                                 int c, int r)
{
  return self->disp_data[r * self->cols + c];
}


/// @brief Convert from image coords (col,row,disparity) to sensor coords (x,y,z)
static  __inline__ 
void sensnet_stereo_crd_to_xyz(sensnet_stereo_blob_t *self,
                               int c, int r, uint16_t d,
                               float *x, float *y, float *z)
{
  *z = self->sx / ((float) (int) d) * self->disp_scale * self->baseline;
  *x = (c - self->cx) / self->sx * *z;
  *y = (r - self->cy) / self->sy * *z;
  return;
}


/// @brief Convert from sensor to vehicle frame
static  __inline__ void sensnet_stereo_sensor_to_vehicle(sensnet_stereo_blob_t *self,
                                                         float px, float py, float pz,
                                                         float *qx, float *qy, float *qz)
{
  *qx = self->sens2veh[0][0]*px + self->sens2veh[0][1]*py + self->sens2veh[0][2]*pz + self->sens2veh[0][3];
  *qy = self->sens2veh[1][0]*px + self->sens2veh[1][1]*py + self->sens2veh[1][2]*pz + self->sens2veh[1][3];
  *qz = self->sens2veh[2][0]*px + self->sens2veh[2][1]*py + self->sens2veh[2][2]*pz + self->sens2veh[2][3];
  
  return;
}


/// @brief Convert from vehicle to local frame
static  __inline__ void sensnet_stereo_vehicle_to_local(sensnet_stereo_blob_t *self,
                                                        float px, float py, float pz,
                                                        float *qx, float *qy, float *qz)
{
  *qx = self->veh2loc[0][0]*px + self->veh2loc[0][1]*py + self->veh2loc[0][2]*pz + self->veh2loc[0][3];
  *qy = self->veh2loc[1][0]*px + self->veh2loc[1][1]*py + self->veh2loc[1][2]*pz + self->veh2loc[1][3];
  *qz = self->veh2loc[2][0]*px + self->veh2loc[2][1]*py + self->veh2loc[2][2]*pz + self->veh2loc[2][3];
  
  return;
}

#endif
