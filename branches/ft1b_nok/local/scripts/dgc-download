#!/bin/bash
#
# /usr/local/dgc-download - copy databases from gc to field server
# RMM, 24 Jul 04
#
# Usage: dgc-download [[-flags] user@remote]
#
# This script makes copies files from grand challenge to the field server
# using rsync.  It should be run after dgc-offline has been run on
# grand challenge.
#
# $Id$

DGC=/dgc
REMOTE=team@gc.caltech.edu
RSFLGS=-rav
MYPWD = rmmis1337

# See if we are doing a full download or quick download
if ( [ "$1" == "--full" ] ) then
  echo Performing full download
  FULL=1
  shift;
else
  echo Performing quick download
  FULL=0
fi

# See if we were passed a remote path as an argument
# Use $* to allow optional flags as well
if ( [ "$1" ] ) then
  REMOTE=$*
else
  echo "Usage: dgc-download [flags] user@machine"
  exit 1;
fi
echo Using $REMOTE as remote source for download

#
# Now use sync to copy over everything that we need, except for mysql
# database, which we transfer using backups to allow for different versions
#
# Wiki: mysql database ('wikidb') + uploaded files (in images)
# Bugzilla: mysql database ('bugs')
# Subversion: DGC source code
# 

echo -e "\n== Downloading Subversion =="
rsync $RSFLGS ${REMOTE}:$DGC/subversion $DGC

if [ $FULL ] then
echo -e "\n== Downloading Wiki == "
rsync $RSFLGS ${REMOTE}:$DGC/wiki $DGC
fi

if [ $FULL ] then
echo -e "\n== Downloading Bugzilla =="
rsync $RSFLGS ${REMOTE}:$DGC/bugzilla $DGC
fi

echo -e "\n== Downloading MySQL =="
# rsync $RSFLGS ${REMOTE}:$DGC/mysql $DGC

# Shut down the MySQL server so that we can make copies of the databases
# Location varies between field server and GC
echo -e "\n  * Shutting down MySQL server"
if ( [ -e /etc/rc.d/rc.mysqld ] ) then
  /etc/rc.d/rc.mysqld stop
fi
if ( [ -e /etc/init.d/mysql ] ) then
  /etc/init.d/mysql stop
fi

echo -en "\n  * Dumping databases: "
echo -en "wikidb "
ssh ${REMOTE} "mysqldump --opt -p$MYPWD wikidb > /dgc/tmp/wikidb.sql"

echo -en "bugs "
ssh ${REMOTE} "mysqldump --opt -p$MYPWD bugs > /dgc/tmp/bugs.sql"

echo -en "\n  * Copying databases"
rsync $RSFLGS ${REMOTE}:$DGC/tmp/\*.sql $DGC/tmp

# Restart the MySQL server
echo -en "\n  * Restarting MySQL server"
if ( [ -e /etc/rc.d/rc.mysqld ] ) then
  /etc/rc.d/rc.mysqld start
fi
if ( [ -e /etc/init.d/mysql ] ) then
  /etc/init.d/mysql start
fi

echo -en "\n  * Recreating databases: "
if ( [ -e /dgc/tmp/wikidb.sql ] ) then
  echo -en "wikidb "
  mysql -p$MYPWD wikidb < /dgc/tmp/wikidb.sql;
fi

if ( [ -e /dgc/tmp/bugs.sql ] ) then
  echo -en "bugs "
  mysql -p$MYPWD bugs < /dgc/tmp/bugs.sql;
fi

if [ $FULL ] then
echo -e "\n== Downlaoding documentation =="
rsync $RSFLGS ${REMOTE}:$DGC/www/doc $DGC/www
rsync $RSFLGS ${REMOTE}:$DGC/www/GrandChallenge $DGC/www
fi

sleep 1
