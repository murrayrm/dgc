#!/bin/sh

# timber_info.sh is intended to tell the duration of a given timber log.  It does 
# this by examining the time stamps in the state logs.  The master copy of this
# script is stored in dgc/local/scripts/.

TIMBER_DIR=2005_08_14/01_33_26
if [[ $# == 1 ]]; then
  TIMBER_DIR=$1
fi

tstart=`head -n 1 $TIMBER_DIR/astate/state.dat | gawk '{print $1}'`
tend=`tail -n 1 $TIMBER_DIR/astate/state.dat | gawk '{print $1}'`
tdiff=`echo "($tend - $tstart)/1000000" | bc -l`

echo "The following modules were logged:"
ls $TIMBER_DIR/* -1d

echo "The duration of the logs is $tdiff seconds."
