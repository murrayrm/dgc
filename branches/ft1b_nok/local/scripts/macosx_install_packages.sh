#!/bin/sh

# This script is intended to contain the commands necessary to get a blank
# installation up to snuff as far as development

# Nice stuff (textutils for octave-forge)
UTILS='xpdf textutils emacs21 xemacs svn-client vim'

# Other stuff required for DGC tree to compile
LIBGNUGETOPT='libgnugetopt libgnugetopt-shlibs'
DEVELOPMENT='fileutils gawk'
MPLAYER_DEPS='mpg123 mplayer-skin-default'

# GTK Map Display dependencies
GTKMM2='gtkmm2 gtkmm2-shlibs gtkmm2-dev'
GTKGLEXT1='gtkglext1 gtkglext1-shlibs'
#echo "You might have to fink install gtkmm2.4 (and -dev and -shlibs)"
#GTKMM24='gtkmm2.4 gtkmm2.4-dev gtkmm2.4-shlibs' # Fink only as of 01/05
#MAPDISPLAY="$GTKMM2 $GTKGLEXT1 $GTKMM24"

# For Octave
FLEX='flex'
BISON='bison'
GPERF='gperf'
GRACE='grace'
OCTAVE='octave'
OCTAVE_DEPS="$FLEX $BISON $GPERF $GRACE $OCTAVE"

# For Player
#CANVAS="libgnomecanvas2 libgnomecanvas2-dev"

# avidemux dependencies
#AVIDEMUX_DEPS='xvidcore xvidcore-shlibs a52dec-dev'

# Update
echo Doing apt-get update...
sudo apt-get update

# Install
echo Doing apt-get install...
sudo apt-get install $UTILS $LIBGNUGETOPT $DEVELOPMENT $MAPDISPLAY $OCTAVE_DEPS $CANVAS 

OCTFORGE='octave-forge'
AVIDEMUX='avidemux'
MPLAYER='mplayer'
#fink install $OCTFORGE $AVIDEMUX $MPLAYER
