#ifndef STATEPRINTER_HH
#define STATEPRINTER_HH

using namespace std;

#include "StateClient.h"

class StatePrinter : public CStateClient
{
public:
  /** Contstructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  StatePrinter(int skynetKey);

  /** Standard destructor */
  ~StatePrinter();

  /** The main loop where execution is trapped */
  void ActiveLoop();

private:

};

#endif  // STATEPRINTER_HH
