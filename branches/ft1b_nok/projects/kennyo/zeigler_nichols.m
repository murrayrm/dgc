% Ziegler-Nichols design for cds 110 project

close all;
clear all;

% define the matricies for the system, so we can plot its response
V_0 = 10;   % speed of the vehicle- meters/sec
L = 3;      % wheel base of the vehicle in meters

A = [ 0 V_0; 0 0 ];
B = [ 0; V_0/L ];        % I want to plot the frequency response of just the
% process, so there isn't going to be any control yet
C = [ 1 0 ];        % Y is the output
D = 0;

Proc = ss(A, B, C, D);

Tmax = 5;
Tsteps = 200;
dt = Tmax / Tsteps;
t = 0:dt:(Tmax - 1);

sys = Proc;
[y] = step(sys, t);

% take the derivative of the system
dy = diff(y) / dt;
dy( length(t) ) = 0;

% find the point of max slope and the time that occurs
max_slope = max(dy)
t_max_slope = find( dy == max_slope, 1) * dt;

% find the intercept of the line tangent to it with that slope
y_int = y(t_max_slope / dt) - (max_slope * t_max_slope)
x_int = -y_int / max_slope

y_line = ( max_slope * t) + y_int;

% plot the step response of the system
figure;
plot( t , y );
hold;
plot( t , y_line);
ylim([-.1, .3]);
title('Step response of Process in prob 1');

% the Z-N gains
K = - 1.2 / y_int
T_I = 2 * x_int
T_D = x_int / 2

s = tf('s');
Cont = K * ( 1 + (1/(T_I * s)) + (T_D * s));
closed_sys = ss(Proc * Cont / ( 1 + Proc * Cont));
open_sys = ss(Proc * Cont);

figure;
step(closed_sys);
title('Step response of system with Ziegler-Nichols control law');
figure;
margin(open_sys);
%title('Frequency response of system with Ziegler-Nichols control law');

