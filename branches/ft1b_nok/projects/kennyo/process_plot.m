% plotting stuff for the cds 110 project. 
close all;
clear all;


% define the matricies for the system, so we can plot its response

for V_0 = -10:5:15

    % V_0 = -5;   % speed of the vehicle- meters/sec
    L = 3;      % wheel base of the vehicle in meters

    A = [ 0 V_0; 0 0 ];
    B = [ 0; V_0/L ];        % I want to plot the frequency response of just the
    % process, so there isn't going to be any control yet
    C = [ 1 0 ];        % Y is the output
    D = 0;

    process = ss(A, B, C, D);

    % The controller

    tf(process)

    figure;
    margin(process);

end