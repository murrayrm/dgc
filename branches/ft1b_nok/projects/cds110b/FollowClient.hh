/*
 * follow.h - header file for follow program
 *
 * RMM, 10 Dec 05
 *
 */

#include "SkynetContainer.h"	// skynet object
#include "StateClient.h"	    // state client object
#include "TrajTalker.h"		    // trajectory receiver
#include "PID_Controller.hh"  // for speed PID control
#include "DGCutils"           // I'm sure it will be useful for something
#include "adrive_skynet_interface_types.h" //For talking to adrive
// #include "traj.h"

#include "CObserver.hh"

#include "falcon/state.h"
#include "falcon/traj.h"

/* Origin for locating ourselves on the planet */
extern double xorigin, yorigin;

/* Declare the global input, output and reference variables */
#define NUMINP 9
#define NUMOUT 2
#define NUMMEAS 3

extern double inp[2*NUMINP], out[NUMOUT], err[NUMINP], meas[NUMMEAS+NUMOUT];
extern double covar[NUMINP], correct[NUMINP];
extern double outGain[NUMOUT], outCtrl[NUMOUT], outFF[NUMOUT], outCmd[NUMOUT], outRef[NUMOUT], outState[NUMOUT];
extern int outOverride[NUMOUT];
#define ref (inp+NUMINP)

extern int NO_COMMANDS;

extern int sn_key;
extern double controlRate;
extern double currentTime;
extern double actualRate;

extern char ctrlFilename[256];
extern char gainFilename[256];
extern char obsvFilename[256];
extern char trajFilename[256];

extern char statusMessage[80];

extern int disableControl(long arg);

/* Define offsets for states and reference values */
#define XPOS 0
#define YPOS 1
#define TPOS 2
#define XVEL 3
#define YVEL 4
#define TVEL 5
#define XACC 6
#define YACC 7
#define TACC 8

#define XMEASPOS 0
#define YMEASPOS 1
#define TVMEASPOS 2

#define PHI 0
#define V   1

// Main skynet module
class FollowClient: public CStateClient, public CTrajTalker {
 public:
  FollowClient(int sn_key);

	void setupFiles();

  //void ReadState();
//   void ReadTraj();
  void ControlLoop();

	void writeLog();

	bool toggleLogging();

	bool useAutoLogNaming;

	char logFilename[256];
	bool loggingEnabled;

	enum ctrlStatus {
		enabled,
		paused,
		disabled
	};

	ctrlStatus controlEnabled;

	ctrlStatus obsvEnabled;

	bool setControlStatus(FollowClient::ctrlStatus status);

	bool toggleObsvStatus();

 private:
	int closestTrajPoint();
	int project_error(TRAJ_DATA *, double *, double *);

	CPID_Controller* m_speedController;

	STATE_SPACE* m_lateralController;

  int m_adriveMsgSocket;

	ofstream logFile;

	unsigned long long timePause;

	unsigned long long timeStart;

	TRAJ_DATA* m_traj_falcon;

// 	int m_currentTrajPoint;

// 	CTraj* m_traj_DGC;

	CObserver* m_observer;

	double gamma;
};
