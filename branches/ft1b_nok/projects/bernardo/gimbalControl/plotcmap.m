%plot c map with 'o'

data = load('tintersect_layer.dat');
[si, sj] = size(data);
resRows = 0.8;
resCols = resRows;
topRight = [3778569.6 398312.8];
bottomLeft = [3778329.6 398152.8];
pos = [3778449.6434082 398232.476163717];

figure;

for i = 1:si;
    for j = 1:sj;
        switch data(i,j);
            %case -1
                %plot(i,j,'ko');                
            %case 0
                %
            case 1
                %plot(i,j, 'r+');
                plot(bottomLeft(1) + (i-1)*resRows,bottomLeft(2) + (j-1)*resCols,'r+');
            case 2
                %plot(i,j, 'go');
                plot(bottomLeft(1) + (i-1)*resRows,bottomLeft(2) + (j-1)*resCols,'go'); 
        end
        
        hold on;
    end
end

plot(pos(1), pos(2), 'bx');