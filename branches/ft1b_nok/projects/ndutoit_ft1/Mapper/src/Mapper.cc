#include <iostream>
#include <vector>
#include "LocalMapTalker.hh"

using namespace std;
using namespace Mapper;

int main(int argc, char *argv[])
{
	// Get the skynet key
	int key = (argc >1) ? atoi(argv[1]) : 0;

	// Create the talker and the map
	CLocalMapTalker mapTalker = CLocalMapTalker(key);
	Map locMap = Map();
	
	// 
	cout << "about to get_send_sock...";
	mapTalker.getAndSetSocket();
	cout << " get_send_sock returned" << endl;

	cout << "about to send a map...";
	mapTalker.SendLocalMap(&locMap);
	cout << " sent a map!" << endl;
}
