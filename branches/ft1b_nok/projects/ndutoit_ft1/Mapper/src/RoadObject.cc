#include "RoadObject.hh"
#include <vector>
using std::vector;
#include <iterator> // ostream_iterator iterator
#include <algorithm> // Copy algorithm

namespace Mapper
{

/***********************************/
/* Member functions for RoadObject */
/***********************************/
/* Basic constructor */
RoadObject::RoadObject(const string &objName, 
					   double xloc, double yloc)
{
	name = objName;
	
}
/* Default constructor */
RoadObject::RoadObject()
{
}

void RoadObject::setName(const string &objName){
	name = objName;	
} 
string RoadObject::getName() const
{
	return name;
}
// Location accessor functions.
double RoadObject::getXLoc() const
{
	return loc.x;
}
double RoadObject::getYLoc() const
{
	return loc.y;
}
// Destructor
RoadObject::~RoadObject()
{
}
/***************************************/
/* End Member functions for RoadObject */
/***************************************/


/*********************************/
/* Member functions for StopLine */
/*********************************/
// Derived class stopline constructor
StopLine::StopLine( 
	const string &objName, double xloc, double yloc ) 
	: RoadObject(objName, xloc, yloc)
{
	// Nothing else to do
}
void StopLine::print() const
{
	std::cout << getName() << "\n";
	std::cout << "(" << getXLoc() << "," << getYLoc() << ")\n\n";
}
StopLine::~StopLine()
{
}
/*************************************/
/* End Member functions for StopLine */
/*************************************/


/***********************************/
/* Member functions for Checkpoint */
/***********************************/
Checkpoint::Checkpoint(
	const string &objName, double xloc, double yloc )
	: RoadObject(objName, xloc, yloc) // Explicitly call base object's constructor
{
}
void Checkpoint::print() const
{
	std::cout << getName() << "\n";
	std::cout << "(" << getXLoc() << "," << getYLoc() << ")\n\n";
}
Checkpoint::~Checkpoint()
{
}
/***************************************/
/* End Member functions for Checkpoint */
/***************************************/


/*************************************/
/* Member functions for LaneBoundary */
/*************************************/
/* Basic LaneBoundary Constructor */
LaneBoundary::LaneBoundary(const string &objName,
						   double firstXPoint,
						   double firstYPoint,	
						   Divider lineType)
	: RoadObject(objName, firstXPoint, firstYPoint)
{
	boundary = lineType;
}
/* More useful LaneBoundary Constructor */
LaneBoundary::LaneBoundary(const string &objName,
						   vector<Location> line,
						   Divider lineType)
	: RoadObject(objName, line[0].x, line[0].y)
{
	boundary = lineType;
	coords = line;
}
/* Default (in the sense of C++) constructor */
LaneBoundary::LaneBoundary()
{
	
}

/* Accessor functions. */
void LaneBoundary::setBoundary( Divider lineType ) {
	boundary = lineType;
}
LaneBoundary::Divider LaneBoundary::getBoundary() const
{
	return boundary;
}
// Add a point to the location description.
/* This is the style of function I always screw up. */
/* 1) Create a new location.
 * 2) Add it to the coords vector.
 * 3) Make sure it isn't screwed all to hell outside
 * of this function.
 */
//TODO I'm not sure this will work.  
void LaneBoundary::addLocPoint(double x, double y) {
	Location *loc_p = new Location;
	loc_p->x = x;
	loc_p->y = y;
	
	coords.push_back(*loc_p);
}
// Retrieve the entire location description.
// TODO Convert this to return two vectors of doubles, x-locs and 
// y-locs?
vector<Location> LaneBoundary::getCoords() const {
	return coords;	
}
void LaneBoundary::print() const
{
	std::cout << getName() << "\n";
	unsigned int i;
	for (i = 0; i<coords.size(); i++)
	{
	  printf("( %3.6f, %3.6f)\n" , coords[i].x, coords[i].y);
	  //std::cout << "(" << coords[i].x << "," << coords[i].y << ")\n";
	}
}
// LaneBoundary Destructor
LaneBoundary::~LaneBoundary()
{
}
/*****************************************/
/* End Member functions for LaneBoundary */
/*****************************************/
}
