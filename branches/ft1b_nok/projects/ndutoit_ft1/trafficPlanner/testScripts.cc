/*		TEST: extract rules	*/

		SegGoals segmentGoal;	
		segmentGoal = spoofSegmentGoal(); // works	
	    int segmentID = segmentGoal.entrySegmentID;
	    vector<TrafficRules> trafficRules;
	    trafficRules = extractRules(segmentGoal.segment_type);	// works

/*		TEST END: extract rules	*/

/*		TEST: generate spoofed local map data	*/	

	    int numLanes = 4;
	    int numTravelLanes = 2;
	    int senseMin = -10;
	    int senseMax = 100;
	    double laneWidth = 5;
	    int xSpacing = 10;
		LocalMapObject localMapObject = spoofLocalMapData(segmentID, numLanes, numTravelLanes, laneWidth, senseMin, senseMax, xSpacing); //works
/*		TEST END: generate spoofed local map data	*/

/*      TEST: lineFit */
/*
		double x_toFit[] = {2, 4, 6};
		double y_toFit[] = {1, 1, 1};
		int sizeOfXToFit = 3;
		double slope, intersection; 
		lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection); // works
		cout << endl;
*/			
/*      TEST END: lineFit */

/*      TEST: closestBoundaryPt */
/*
		int ind_CP = 0;
		int ind_Insert = 0;
		double x_Pt, y_Pt, x, y;
		double xB_temp[] = {0, 2, 4, 6, 8, 10};
		double yB_temp[] = {0, 1, 2, 3, 4, 5};
		vector<double> xB, yB;
		// spoof some data to check the algorithm
		x = 2;
		y = 1;
		for (int ii=0; ii<6; ii++)
		{
			xB.push_back(xB_temp[ii]);
			yB.push_back(yB_temp[ii]);
		}
		
		double distance = closestBoundaryPt(xB, yB, x, y, ind_CP, ind_Insert, x_Pt, y_Pt); // works
		cout << "distance = " << distance << endl;
		cout << "index of closest existing point = " << ind_CP << endl;
		cout << "index where should be inserted = " << ind_Insert << endl;			
		cout << "closest pt x = " << xB[ind_CP] << endl;
		cout << "length of xB = " << xB.size() << endl;
		cout << "x_Pt = " << x_Pt << endl;
		cout << "y_Pt = " << y_Pt << endl;
		cout << endl;
*/
/*      TEST END: closestBoundaryPt */
		
/*      TEST: insertPointOnBoundary */
/*
		cout << "insertPointOnBoundary function" << endl;
		cout << "BEFORE insertion: " << endl;
		for (int ii=0;ii<6;ii++)
		{
			cout << " xB[" << ii << "] = " << xB[ii] << "and yB[" << ii << "] = " << yB[ii]<< endl;			
		}
		insertPointOnBoundary(xB,yB,x_Pt, y_Pt,ind_Insert);	// works
		cout << "AFTER insertion: " << endl;
		for (int ii=0;ii<7;ii++)
		{
			cout << " xB[" << ii << "] = " << xB[ii] << " and yB[" << ii << "] = " << yB[ii]<< endl;
		}
*/			
/*      TEST END: insertPointOnBoundary */			

/*      TEST: distanceAlongBoundary */
/*		
		cout << "distanceAlongBoundary function" << endl;
		distance = 3;
		int index = 1;
		distanceAlongBoundary(xB, yB, index, distance);	// works
		cout << "AFTER distance calculation and point insertion: " << endl;
		for (int ii=0;ii<7;ii++)
		{
			cout << " xB[" << ii << "] = " << xB[ii] << " and yB[" << ii << "] = " << yB[ii]<< endl;
		}
*/
/*      TEST END: distanceAlongBoundary */		
			
/*      TEST: boundaryConstraints */
/*
		cout << "boundaryConstraints function" << endl;
		bool insert_flag = true;
		double distance = 5;
		double x = 5.5;
		double y = 0;
		double xB_temp[] = {0, 2, 4, 6, 8, 10};
		double yB_temp[] = {1, 1, 1, 1, 1, 1};
		
		vector<double> xB1, yB1, xB2, yB2;
		// spoof some data to check the algorithm
		for (int ii=0; ii<6; ii++)
		{
			xB1.push_back(xB_temp[ii]);
			yB1.push_back(2.5*yB_temp[ii]);
			xB2.push_back(xB_temp[ii]);
			yB2.push_back(-2.5*yB_temp[ii]);
		}
		int ind_CP, ind_Insert;
		double x_Pt, y_Pt;		
		double dist = closestBoundaryPt(xB1, yB1, x, y, ind_CP, ind_Insert, x_Pt, y_Pt);
		insertPointOnBoundary(xB1, yB1, x_Pt, y_Pt,ind_Insert);		
		distanceAlongBoundary(xB1, yB1, ind_Insert, distance);
		vector<Constraint> constraints;
		constraints = boundaryConstraints(xB1, yB1, xB2, yB2, x, y, distance, insert_flag); //works
		for (int ii=0;ii<(int)xB1.size();ii++)
		{
			cout << " xB1[" << ii << "] = " << xB1[ii] << " and yB1[" << ii << "] = " << yB1[ii]<< endl;			
		}
		
		for (int ii=0;ii<(int)constraints.size();ii++)
		{
			cout << "Constraint["<< ii << "].A1 = " << constraints[ii].A1 << endl;
		    cout << "Constraint["<< ii << "].A2 = " << constraints[ii].A2 << endl;	     
		    cout << "Constraint["<< ii << "].B = " << constraints[ii].B << endl;
		    cout << "Constraint["<< ii << "].xrange = [" << constraints[ii].xrange1 << " , " << constraints[ii].xrange2 << "]" << endl;
		    cout << "Constraint["<< ii << "].yrange = [" << constraints[ii].yrange1 << " , " << constraints[ii].yrange2 << "]" << endl;	    
		}
*/		
/*      TEST END: boundaryConstraints */		
			
/*      TEST: laneFollowing */		
/*
		int currentLane = 4;
		vector<Location> currentLaneLBCoord, currentLaneRBCoord;
		Divider currentLaneLBType, currentLaneRBType;
		currentLaneLBType =  localMapObject.segment.lane[currentLane-1].laneLBType;
		currentLaneRBType =  localMapObject.segment.lane[currentLane-1].laneRBType;
		for (unsigned ii=0;ii<localMapObject.segment.lane[currentLane-1].laneLBCoord.size();ii++)
		{
			currentLaneLBCoord.push_back(localMapObject.segment.lane[currentLane-1].laneLBCoord[ii]);
		} 
		for (unsigned ii=0;ii<localMapObject.segment.lane[currentLane-1].laneRBCoord.size();ii++)
		{
			currentLaneRBCoord.push_back(localMapObject.segment.lane[currentLane-1].laneRBCoord[ii]);
		} 
		double dLimit = 50;
		double dist2Exit = 100;
		double x_A ,y_A;
		cout << "Enter Alice position: x = ";
		cin >> x_A;
		cout << "Enter Alice position: y = ";
		cin >> y_A;
			
		ConstraintSet constraintSet = laneFollowing(segmentID, currentLane, currentLaneLBCoord, currentLaneRBCoord, currentLaneLBType, currentLaneRBType, trafficRules, dLimit, dist2Exit, x_A, y_A);//works

		for (int ii=0;ii<(int)constraintSet.leftBoundaryConstraint.size();ii++)
		{
		    cout << "LBConstraint["<< ii << "].xrange = [" << constraintSet.leftBoundaryConstraint[ii].xrange1 << " , " << constraintSet.leftBoundaryConstraint[ii].xrange2 << "]" << endl;
		    cout << "LBConstraint["<< ii << "].yrange = [" << constraintSet.leftBoundaryConstraint[ii].yrange1 << " , " << constraintSet.leftBoundaryConstraint[ii].yrange2 << "]" << endl;	    
		}
		for (int ii=0;ii<(int)constraintSet.rightBoundaryConstraint.size();ii++)
		{
		    cout << "RBConstraint["<< ii << "].xrange = [" << constraintSet.rightBoundaryConstraint[ii].xrange1 << " , " << constraintSet.rightBoundaryConstraint[ii].xrange2 << "]" << endl;
		    cout << "RBConstraint["<< ii << "].yrange = [" << constraintSet.rightBoundaryConstraint[ii].yrange1 << " , " << constraintSet.rightBoundaryConstraint[ii].yrange2 << "]" << endl;	    
		}
		
*/		
/*      TEST END: laneFollowing */		
