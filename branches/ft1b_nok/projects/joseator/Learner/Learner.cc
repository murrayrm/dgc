 /*****************************************************************
 *  Project    : Learning and adaptation, SURF 2006               *
 *  File       : Learner.cc                                       *
 *  Description: Program stores all the paths that ALICE has      *
 *               traversed. The program stores the paths using    *
 *               two methods. One method is writting the path     *
 *               directly to a file "savedPaths". The second      *
 *               method is using a double list. For every position* 
 *               ALICE has been at, a new node is added to a list *
 *               Each node also has a list of it's own,           *
 *               corresponding to the position ALICE went         *
 *               immediatly after the node in the main list.      *
 *  Author     : Jose Torres                                      *
 *  Modified   : 6-23-06                                          *
 *****************************************************************/      

#include "Learner.hh"
#include <gdbm.h>
#include <fcntl.h>

// Constructor
Learner::Learner(double northing, double easting, int skynet_key) :
  CSkynetContainer(SNastate, skynet_key)
{
  // DBM* myDBM = new DBM();
  //myDBM = dbm_open("path",O_RDWR,S_ISUID);
  //int x = dbm_error(myDBM);
  //if(myDBM == NULL)
  // {
  //    cout<<"no database created <------------" <<endl;
  //  }
  Node* newNode = new Node;
  newNode->northing = northing;
  newNode->easting = easting;
  start = newNode;
  end = newNode;
  newNode->location = NULL; 
  newNode->path = NULL;
  newNode->pathEnd = newNode;
  //cout<< "Current Position: (" <<northing <<", " << easting <<") " <<endl;
  printPath(6, 6); 
  locationListLength = 1;
  newNode->pathListLength = 1;
  numSeg = 1;
}



//destructor
Learner::~Learner()  
{
  destroyLocations();
}



void Learner::addNode(double lastNorthing, double lastEasting, double northing, double easting)
{
  //this checks to make sure ALICE has moved so an identical node is not added to the list
  if((abs(lastNorthing - northing)< 0.00001) && (abs(lastEasting - easting) < 0.00001))
    {
    }
  else
    {
      cout<<fixed <<showpoint <<setprecision(6);
      cout<< "Current Position: (" <<northing <<", " << easting <<")"<<endl;
      printPath(northing, easting);   /*! prints previously paths taken from current position*/
      Node* newNode = new Node;
      newNode->northing = northing;
      newNode->easting = easting;
      newNode->path = NULL;
      newNode->location = NULL;
      
      Node* visitedLocation = search(lastNorthing, lastEasting);
      visitedLocation->pathEnd->path = newNode;
      visitedLocation->pathEnd = newNode;
      visitedLocation->pathListLength++;
      
      Node* visitedNode = search(northing, easting);
      if(visitedNode == NULL)		/*! adds node if not in "Location" list already*/
	{
	  Node* newLocation = new Node;
	  newLocation->northing = northing;
	  newLocation->easting = easting;
	  newLocation->pathEnd = newLocation;
	  newLocation->path = NULL;
	  newLocation->location = NULL;
	  
	  end->location = newLocation;
	  end = end->location;
	  
	  newLocation->pathListLength = 1;
	  locationListLength++;
	}    
    }      
} //end addNode



void Learner::printPath()  
{
  Node* current;   /*! pointer to traverse list*/
  current = start; /*! set pointer to point to first node*/
  
  while(current != NULL)
    {
      cout<< current->northing <<" " << current->easting <<endl;	
      current = current->location;
    }
} //end printPath

  

void Learner::printPath(double x, double y)
{
  Node* current;       
  current = search(x,y);
  if(current != NULL)
    {
      current = current->path;
    }
  
  while(current != NULL)
    {
      cout<<fixed <<showpoint <<setprecision(6);
      cout<< "(" <<x <<", " <<y <<")" <<endl;
      cout<< "(" <<current->northing <<", " << current->easting <<")" <<endl;
      print(current->northing, current->easting);
      cout<<endl;
      current = current->path;
      
    }
  
} //end printPath*



void Learner::print(double x, double y)
{
  Node* current;       
  current = search(x,y);
  
  for(int i=0; i<4; i++)
    {
      current = search(x,y);
      if(current == NULL)
	{
	  break;
	}
      
      if(current->path != NULL)
	{
	  cout<<fixed <<showpoint <<setprecision(6);
	  cout<< "(" <<current->path->northing <<", " << current->path->easting <<")" <<endl;
	  x = current->path->northing;
	  y = current->path->easting;
	}
      
    } //end for loop
  
}



Node* Learner::search(double x, double y)
{
  Node* current;
  current = start;
  double distance;
  if(start != NULL)
    {	
      while(current != NULL)
	{ 
	  distance = sqrt(pow(x - current->northing,2) + pow(y - current->easting,2));
	  if(distance <= .002)
	    {
	      return current;
	    }
	  else
	    {
	      current = current->location;
	    }
	  
	} //end while
    } //end if
  
  return NULL; 
  
} //end search



Node* Learner::searchPath(double x, double y)
{
  Node* current;
  current = start;
  
  if(start != NULL)
    {	
      while(current != NULL)
	{ 
	  if((abs(current->northing - x)< 0.00000001) && (abs(current->easting - y) < 0.00000001)) 
	    {
	      return current;
	    }
	  else
	    {
	      current = current->path;
	    }
	  
	} //end while
    } //end if
  
  return NULL;
  
  
} //end searchPath



bool Learner::searchFile(double startX, double startY, double destinationX, double destinationY)
{
  double fileX, fileY, dummyVariable;
  ifstream inData;
  inData.open("savedPaths");
  cout<<fixed <<showpoint <<setprecision(6) <<"in searchfile" <<endl;
  while(!inData.eof())
    {
      inData >>fileX;
      inData >>fileY;
      inData>> dummyVariable;
      
      if((abs(fileX - startX)< 0.00000001) && (abs(fileY - startY) < 0.00000001)) 
	{
	  cout<<"found first checkpoint" <<endl;
	  while(!inData.eof())
	    {
	      inData >>fileX;
	      inData >>fileY;
	      if((abs(fileX - destinationX)< 0.00000001) && (abs(fileY - destinationY) < 0.00000001)) 
		{
		  cout<<"found path to checkpoint" <<endl;
		  pathToCheckPoint(startX, startY, destinationX, destinationY);
		  inData.close();
		  return true;
		}
	    }
	}
	
    }
  inData.close();
  return false;
  
} //end searchFile



void Learner::pathToCheckPoint(double startX, double startY, double destinationX, double destinationY)
{
  double fileX, fileY;
  double previousX = startX;
  double previousY = startY;
  double startTimeStamp, endTimeStamp;
  double distance = 0;
  ifstream inData;
  inData.open("savedPaths");
  while(!inData.eof())
    {
      inData >>fileX;
      inData >>fileY;
      inData >>startTimeStamp;
      if((abs(fileX - startX)< 0.00000001) && (abs(fileY - startY) < 0.00000001)) 
	{
	  cout<< fileX <<" " <<fileY <<endl;
	  while(!inData.eof())
	    {
	      inData >>fileX;
	      inData >>fileY;
	      inData >>endTimeStamp;
	      cout<< fileX <<" " <<fileY <<endl;
	      distance = distance + (sqrt(pow(previousX - fileX,2) + pow(previousY - fileY,2)));
	      if((abs(fileX - destinationX)< 0.00000001) && (abs(fileY - destinationY) < 0.00000001)) 
		{
		  cout<< endl <<"Distance = " << distance <<" m" <<endl;
		  cout<< "Time = " <<  (endTimeStamp - startTimeStamp)/1000000 <<" sec" <<endl;
		  inData.close();
		  return;
		  
		}
	      previousX = fileX;
	      previousY = fileY;
	    } //end inner while
	}
    } //end outer while
} //end pathToCheckPoint



void Learner::destroyLocations()
{
  Node* tempStart;
  tempStart = start->location;
  while(start != NULL)
    {
      destroyPaths(); 
      start = tempStart;
      if(tempStart != NULL) tempStart = tempStart->location;
    }
  delete tempStart;
  
} //end destroyLocations


  
void Learner::destroyPaths()
{
  Node* temp;
  while(start != NULL)
    {
      temp = start;
      start = start->path;
      delete temp;
    }
} //end destroyPaths



double Learner::abs(double x)
{
  if(x<0) return x * -1;
  else return x;
}
//end abs


//function constantly modified to test code
void Learner::testFunction()
{
  Node *temp;
  temp = search(0.0,0.0);
  //cout<<"location list length: " <<locationListLength <<endl;
  //cout<<"path list length: " <<temp->pathListLength <<endl;
  while(temp != NULL)
    {
      cout<< temp->northing <<" " <<temp->easting <<endl;
      temp = temp->path;
    }
} //end testFunction



void Learner::sendCheckpoint(double x, double y, double endX, double endY, bool segmentTransition)
{
  
  //Set up socket for skynet broadcasting:
  int sendSocket  = m_skynet.get_send_sock(SNrndfCheckpoint);
  //sendSocket = 114;
  //cout<< sendSocket <<endl;  

  //Set up socket for skynet broadcasting:
  int transition  = m_skynet.get_send_sock(SNsegmentTransition);
  
  if (sendSocket < 0)
    {
      cerr << "TraversedPath::TraversedPath(): skynet listen returned error" << endl;
    }
  
  Checkpoint  my_command;
  my_command.startX = x;
  my_command.startY = y;
  my_command.endX = endX;
  my_command.endY = endY;
  
  cout<< "sending checkpoint" <<endl;
  m_skynet.send_msg(sendSocket, &my_command, sizeof(my_command), 0);

  Transition newSegment;
  if(segmentTransition)
    {
      numSeg++;
      newSegment.segmentTransition = true;
      newSegment.segment = numSeg;
      cout<<"on a new road:  <--------------" <<numSeg <<"_segment"  <<endl;
      m_skynet.send_msg(transition, &newSegment, sizeof(newSegment), 0);
    }
  else
    {
      newSegment.segment = newSegment.segment;
      newSegment.segmentTransition = false;
      m_skynet.send_msg(transition, &newSegment, sizeof(newSegment), 0);
    }
   
  
  
}



void Learner::createDatabase()
{
  GDBM_FILE dbf;
  datum key, content;
  key.timestamp = 342564;
  //content.startX = 23;
  //content.startY = 34;
  //content.endX = 61;
  //content.endY = 1;
  int ret;
  //char *argv[];
  //argv = "database";
  //int argc = 4;
  //main (int argc, char *argv[])
  //{
  //if (argc < 3)
  //  {
  //    fprintf (stderr, "Usage: dbmexample  \n\n");
  //    exit (1);
  //  }
  
  dbf = gdbm_open ("myDatabase", 0, GDBM_WRCREAT, 0666, NULL);
  if (!dbf)
    {
      fprintf (stderr, "File %s either doesn't exist or is not a gdbm file.\n", "myDatabase");
      exit (2);
    }
  if(dbf)
    {
      cout<<"created database" <<endl;
    }


  ret = gdbm_store(dbf, key, content, GDBM_REPLACE);

  /* key.dsize = strlen (argv[2]) + 1;
  
  data = gdbm_fetch (dbf, key);
  
  if (data.dsize > 0) {
    printf ("%s\n", data.dptr);
    free (data.dptr);
  } else {
    printf ("Key %s not found.\n", argv[2]);
  }
  */ 
  gdbm_close(dbf);
 
  //}
  
} //end createDatabase



void Learner::active() 
{
  double lastX = 0.0;
  double lastY = 0.0;
  int counter = 0;
  double startX, startY, endX, endY;
  ofstream outData;
  ofstream trajFile;
  outData.open("pathFiles/path1", ios::app);
  outData<<fixed <<showpoint <<setprecision(6);
  outData<<endl;

  //creates a file that can be used by trajFollower to rerun a previously traversed path
  trajFile.open("trajFiles/traj1", ios::app);
  trajFile<<fixed <<showpoint <<setprecision(6); 
  
  
  while(1) 
    {
      counter++;
      UpdateState();
      if(counter % 16 == 0)
	{
	  startX = lastX;
	  startY = lastY;
	  
	}
      if(counter % 3 == 0)
	{
	  endX  = m_state.Easting;
	  endY = m_state.Northing;
	}
      if(counter % 10 == 0)
        {
	  sendCheckpoint(startX, startY, endX, endY, true);
	}
      else
	{
	  sendCheckpoint(startX, startY, endX, endY, false);
	}
      outData<<m_state.Easting<<"  " <<m_state.Northing <<" " << m_state.Timestamp<<endl;
      trajFile<<m_state.Northing<<"  " <<m_state.Vel_N<<" " <<m_state.Acc_N<<"  "<<m_state.Easting<<"  " <<m_state.Vel_E<<"  " <<m_state.Acc_E  <<endl;
      //time_t rawtime = m_state.Timestamp/1000000 ;
      //tm* timeinfo;
      //timeinfo = localtime (&rawtime);
      //outData<< timeinfo->tm_hour <<" " <<timeinfo->tm_min <<" " <<timeinfo->tm_sec <<endl;
      
      addNode(lastX, lastY, m_state.Easting, m_state.Northing);
      lastX = m_state.Easting;
      lastY = m_state.Northing; 
      
      DGCusleep(1000000); 
    }
  
  outData.close();
} //end active



int main() 
{
  
  int sn_key = 0;
  
  cout<<"Run this when driving desert roads..."<<endl;
  char* pSkynetkey = getenv("SKYNET_KEY");
  cout<<"got skynet key"<<endl;
  if ( pSkynetkey == NULL)
    {
      cerr << "SKYNET_KEY environment variable isn't set." << endl;
    } else
      {
	sn_key = atoi(pSkynetkey);
      }
  //cerr << "Constructing skynet with KEY = " << sn_key << endl;
  Learner ast(0.0, 0.0, sn_key);
  cout<<"about to call active"<<endl; 
  //ast.searchFile(646634.3042, 3942663.9096, 646634.3609, 3942663.9663);
  //ast.active();
  ast.createDatabase();
  return 0;
} //end main    




