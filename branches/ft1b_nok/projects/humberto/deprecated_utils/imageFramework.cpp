#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <Magick++.h>
#include <ctime>

/*! edge detection algorithm with smoothing */
/*! right now it only enhances image and gets edges */

//to compile run > gcc imageFramework.cpp -o imageFramework `Magick++-config --cppflags --cxxflags --ldflags --libs` -lm
//an example run is ./imageFramework images/stereo00001-R.bmp -l1 -s2 -k -sobelh -s1 -l2 -k -sobelv -c -w

/*
The image-processing framework is being built over the ImageMagick++ API for easy manipulation of images. It was made by initiating 3 Image objects (from the ImageMagick++ classes), where ''image1'' and ''image2'' are static and ''workingImage'' is for manipulation. You can then call the ImageManipulation function with a string of the order of the filters and processes you want to run on the images. The extensible command set now includes:
*-l1: load image1 to workingImage
*-l2: load image2 to workingImage
*-s1: save workingImage to image1
*-s2: save workingImage to image2
*-bw: change workingImage to B&W
*-c: compose (merge) workingImage with image1
*-d: despeckle workingImage
*-sb: simple Blur workingImage
*-gb: gaussian Blur workingImage
*-ab: adaptative Blur workingImage
*-w: write workingImage
*-ded: default edge detection on workingImage
*-sobelh: apply horizontal sobel kernel to workingImage
*-sobelv: apply vertical sobel kernel to workingImage

For example, the execution line ./edge road3/road.jpg -l1 -bw -s2 -sobelh -s1 -l2 -sobelv -c -w does the edge detection algorithm on the image provided .

*/

// SURF 06
// Humberto Pereira: tallberto @ gmail.com

using namespace std;
using namespace Magick;
 
int main(int argc,char **argv) {
	// Construct the image object. Seperating image construction from the
	// the read operation ensures that a failure to read the image file
	// doesn't render the image object useless.
	Image image;	//image object container
	Image workingImage;
	Image image2;	//other image object container
	
	int steps=0, i;
	string buffer;
	char *charArray="something";
	int debug=1;
		
	string originalName;
	string step_action;
	
	const double simpleBlur[9]={0.111,0.111,0.111,0.111,0.111,0.111,0.111,0.111,0.111}; //all pixels have equal weight
	const double simpleBlur2[9]={0.1,0.1,0.1,0.1,0.2,0.1,0.1,0.1,0.1};				// central pixels have bigger weight
	
	const double sobelH[9]={0.25,0.5,0.25,0,0,0,-0.25,-0.5,-0.25};						// these work fine
	const double sobelV[9]={-.25,0,0.25,-.5,0,0.5,-0.25,0,0.25};

	const double freiChen1[9]={0.3536,0.5,0.3536,0,0,0,-0.3536,-0.5,-0.3536};		// these are experimental
	const double freiChen2[9]={0.3536,0,-0.3536,0.5,0,-0.5,0.3536,0,-0.3536};
	const double freiChen3[9]={0,-0.3536,0.5,0.3536,0,-0.3536,-0.5,0.3535,0};
	const double freiChen4[9]={0.5,-0.3536,0,-0.3536,0,0.3536,0,0.3536,-0.5};
	const double freiChen5[9]={0,0.5,0,-0.5,0,-0.5,0,0.5,0};
	const double freiChen6[9]={-0.5,0,0.5,0,0,0,0.5,0,-0.5};
	const double freiChen7[9]={0.1667,-0.3333,0.1667,-0.3333,0.6667,-0.3333,0.1667,-0.3333,0.1667};
	const double freiChen8[9]={-0.3333,0.1667,-0.3333,0.1667,0.6667,0.1667,-0.3333,0.1667,-0.3333};
	const double freiChen9[9]={0.3333,0.3333,0.3333,0.3333,0.3333,0.3333,0.3333,0.3333,0.3333};
	
	if(argc < 2) {
		printf("Usage: %s image [-actions] \n type %s image [-help] for more info\n\n", argv[0]);
		exit(0);
	}
	
	originalName=argv[1];
	
	if(originalName=="-help") {
		printf("Usage: %s imageFile [-step actions...] , where `-step actions` are one or many of the following in order of execution. default step loads image to image1.\n");
		printf(" -l1: load image1 to workingImage\n");
		printf(" -l2: load image2 to workingImage\n");
		printf(" -s1: save workingImage to image1\n");
		printf(" -s2: save workingImage to image2\n");				
		printf(" -bw: change workingImage to B&W\n");
		printf(" -c: compose (merge) workingImage with image1\n");
		printf(" -d: despeckle workingImage\n");
		printf(" -sb: simple Blur workingImage\n");
		printf(" -gb: gaussian Blur workingImage\n");
		printf(" -ab: adaptative Blur workingImage\n");
		printf(" -w: write workingImage\n");
		printf(" -ded: default edge detection on workingImage\n");
		printf(" -sobelh: apply horizontal sobel kernel to workingImage\n");
		printf(" -sobelv: apply vertical sobel kernel to workingImage\n");
		printf("\n");
		exit(0);
	}
		
		
   steps=argc-2;
   
   if (debug) cout << "> Doing " << steps << " operations on the image" << endl;
   long int startTime = clock();

	try {					//framework for multiple image operations
  		
      image.read(originalName); // Read a file into image object

   	for (i=0;i<steps;i++) {
     		step_action = argv[i+2];
  		   if (debug) cout << "> " << i+1 << "::" << step_action << ": ";
    		if (step_action == "-l1") {						// load image 1 to workingImage
     			workingImage = image;
 			   if (debug) cout << "Loaded from image1" << endl;
   		} else if (step_action == "-l2") {				// load image 1 to workingImage
     			workingImage = image2;
  			   if (debug) cout << "Loaded from image2" << endl;
   		} else if (step_action == "-s1") {				// save workingImage to image 1
     			image = workingImage;
  			   if (debug) cout << "Saved to image1" << endl;
   		} else if (step_action == "-s2") {				// save workingImage to image 2
     			image2 = workingImage;
			   if (debug) cout << "Saved to image2" << endl;
   		} else if (step_action == "-d") {
   			workingImage.despeckle();						// despeckle
			   if (debug) cout << "Image despeckled" << endl;
   		} else if (step_action == "-en") {
   			workingImage.enhance();							// enhance
			   if (debug) cout << "Image enhanced" << endl;
   		} else if (step_action == "-eq") {
   			workingImage.equalize();							// enhance
			   if (debug) cout << "Image equalized" << endl;
   		} else if (step_action == "-sb") {
				workingImage.convolve(3,simpleBlur);		// simple blur
			   if (debug) cout << "Image blured" << endl;
   		} else if (step_action == "-gb") {
				workingImage.blur( 1, 0.5 );					// gaussian blur
	 	      if (debug) cout << "Image blured" << endl;
   		} else if (step_action == "-ct") {
				workingImage.contrast(1);						// contrast
	 	      if (debug) cout << "Contrast tuned" << endl;
   		} else if (step_action == "-ab") {
				//applies less blur to farther distances 	// adaptative blur
				// renovate code
				if (debug) cout << "Image blured" << endl;
   		} else if (step_action == "-w") {				// writes image
   			sprintf(charArray, "%d_step.bmp",i);
   			buffer = charArray; 
				workingImage.write(buffer);						
  			   if (debug) cout << "Image written" << endl;
   		} else if (step_action == "-ded") {				//default edge detection
   			workingImage.edge(2);
				if (debug) cout << "Image edges detected" << endl;
   		} else if (step_action == "-k" && i+1<steps) { 	//kernel
	   		i++;
	   		step_action = argv[i+2];
	   		if (step_action=="-sobelh") workingImage.convolve(3,sobelH);
	   		if (step_action=="-sobelv") workingImage.convolve(3,sobelV);
	   		if (step_action=="-frei1") workingImage.convolve(3,freiChen1);
	   		if (step_action=="-frei2") workingImage.convolve(3,freiChen2);
	   		if (step_action=="-frei3") workingImage.convolve(3,freiChen3);
	   		if (step_action=="-frei4") workingImage.convolve(3,freiChen4);
	   		if (step_action=="-frei5") workingImage.convolve(3,freiChen5);
	   		if (step_action=="-frei6") workingImage.convolve(3,freiChen6);
	   		if (step_action=="-frei7") workingImage.convolve(3,freiChen7);
	   		if (step_action=="-frei8") workingImage.convolve(3,freiChen8);
	   		if (step_action=="-frei9") workingImage.convolve(3,freiChen9);
	   		
  			   if (debug) cout << "Kernel " << step_action << " applied" << endl;
   		} else if (step_action == "-c") {				//composite image and working image
	 			workingImage.composite(image,0,0,PlusCompositeOp);
	     	   if (debug) cout << "Images merged" << endl;
	 			// join with sum operator... more operators at http://www.imagemagick.org/Magick++/Enumerations.html#CompositeOperator
   		} else if (step_action == "-bw") {
    			workingImage.type(GrayscaleType);
			   if (debug) cout << "Image type changed to greyscale" << endl;
   		} else if (step_action == "-tc") {
    			workingImage.type(TrueColorType);
			   if (debug) cout << "Image type changed to TrueColor" << endl;
			} else if (step_action == "-at") {
    			workingImage.adaptiveThreshold(10,10,0);
			   if (debug) cout << "Adaptative Threshold applied" << endl;
			}


		}
	}

   
   catch( Exception &error_ ) {
		cout << "Caught exception: " << error_.what() << endl;
   	return 1;
   }

	double timeTaken = (double)(clock() - startTime)/(double)CLOCKS_PER_SEC;
	cout << timeTaken << endl; 
	   
	return 0;
 	
}

