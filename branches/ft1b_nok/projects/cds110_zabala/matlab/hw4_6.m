% HW 4 problem 6

V = 10;
l = 3;

% "linearization" of A, with the control input k
A_lin = [0 V 0; 0 0 V/l; -1*k1 -1*k2 -1*k3];

% this is stable because the real parts of the eigenvalues are all less
% than zero
eigenvalues = eig(A_lin);
display(eigenvalues);

% inside simulink, set the initial condition for the y integrator to 1

if(real(eigenvalues(1))<0 && real(eigenvalues(3))<0 && real(eigenvalues(3))<0)

    sim hw4_6_c;

    figure;
    plot(z.time,z.signals.values(:,1),z.time,z.signals.values(:,2),z.time, ...
        z.signals.values(:,3));
    legend('show')
    legend('y','theta','phi')
    title(['eig1=',num2str(eig1),' | eig2=',num2str(eig2),' | eig3=',num2str(eig3)]);

    load ../follow/lateral_03;
    D(2) = -1*k1; 
    D(3) = -1*k2; 
    D(6) = -1*k3;
    D(11) = k1;
    D(12) = k2;
    D(15) = k3;
    save('../follow/lateral_04','A','B','C','D');

end