%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%     CALIFORNIA INSTITUTE OF TECHNOLOGY     %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%       Control and Dynamical Systems        %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%           CDS 110a (Fall 2006)             %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%                Midterm                     %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------     Program by: Francisco A. Zabala,       ---------------%
%--------------   Jessica Gonzalez, and Russell Newman.    ---------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear all
close all
% Setting the parameters
l   =  3;
V_0 = 10;

% Our linearized system around the equilibrium point
A = [0   V_0    0     0 ;
     0    0  -V_0/l   0 ;
     0    0     0     0 ;
     0    0     0     0];

B = [0 0;
     0 0;
     1 0;
     0 1];

C = eye(4);
% Using the place command to find the corresponding gains
%p  = random('Normal',-5,2,1,4)
p = [-2 -3.5 -2.5 -5]; %line


K = place(A,B,p);
%Kr = -1./(C*inv(A-B*K)*B);

%k_11 = K(1,1);k_12 = K(1,2);k_13 = K(1,3);k_14 = K(1,4);
%k_21 = K(2,1);k_22 = K(2,2);k_23 = K(2,3);k_24 = K(2,4);
k_11 = 0;k_12 = 0;k_13 = 200;k_14 = 0;
k_21 = 0;k_22 = 0;k_23 = 0;k_24 = 0.5;

%kr4 = Kr(:,2); kr1 = 2; kr2 = 4;
Co1 = ctrb(A,B(:,1));
Co2 = ctrb(A,B(:,2));
sim Nefragon_Midterm_sim;


y     = yout(:,1);
theta = yout(:,2);
phi   = yout(:,3);
v     = yout(:,4);

plot(tout,y)
hold on
plot(tout,theta,'y--')
plot(tout,phi,'g:')
plot(tout,v,'c-')
legend('y','\theta','\phi','v')