/* AState.cc (repAstate)
 * 
 * Astate class definition
 * 
 * Stefano Di Cairano, DEC-06
 *
 *
 */
#include "AState.hh"




AState::AState(int skynetKey, astateOpts inputAstateOpts):
CSkynetContainer(SNastate, skynetKey),
CTimberClient(timber_types::astate)
{
  // Set internal options.
  _astateOpts = inputAstateOpts;

  snKey = skynetKey;

  quitPressed = 0;

  //Values relevent to sparrow:
  apxCount = 0;
  apxDead = 1;
  apxStatus = NO_SOLUTION; // check the table
  timberEnabled = 0;
  
 	// state estimate as obtained from APX
  apxHeight = -1;
  apxNorth = -1;
  apxEast = -1;
  apxVelN = -1;
  apxVelE = -1;
  apxVelD = -1;
  apxAccN = -1;
  apxAccE = -1;
  apxAccD = -1;
  apxRoll = -1;
  apxPitch = -1;
  apxHeading = -1;
  apxRollRate = -1;
  apxPitchRate = -1;
  apxHeadingRate = -1;
// estimate accuracy
  apxHeight_accuracy = 1;
  apxNorth_accuracy = -1;
  apxEast_accuracy = -1;
  apxVelN_accuracy = -1;
  apxVelE_accuracy = -1;
  apxVelD_accuracy = -1;
  apxAccN_accuracy = -1;
  apxAccE_accuracy = -1;
  apxAccD_accuracy = -1;
  apxRoll_accuracy = -1;
  apxPitch_accuracy = -1;
  apxHeading_accuracy = -1;
  apxRollRate_accuracy = -1;
  apxPitchRate_accuracy = -1;
  apxHeadingRate_accuracy = -1;

  
  //end
  
  
  //Initialize the sockets
  
  apxControlSocket=socket(PF_INET, SOCK_STREAM, 0); // create the control socket
  if(apxControlSocket<0)
  {
    cout << "ERROR while creating control socket" << endl;
    sleep(1);
  }
  else
  {
    apxDataSocket=socket(PF_INET, SOCK_STREAM, 0); 	// create the data socket
  	if(apxDataSocket<0)
  	{
  	  cout << "ERROR while creating data socket" << endl;
	  sleep(1);
  	}
  }
  
  struct sockaddr_in apxControl_addr, apxData_addr;					// these structs contains the addresses 
 
  apxControl_addr.sin_family = AF_INET;								// IPv4 addressing
  apxControl_addr.sin_port = htons(APX_CONTROL_PORT);                   
  apxControl_addr.sin_addr.s_addr = inet_addr(APX_IP_ADDR);
  //inet_aton(APX_IP_ADDR, &apxControl_addr.sin_addr);   	   
  
  memset(&(apxControl_addr.sin_zero), '\0', 8); 				    //required by the structure	
  apxData_addr.sin_addr.s_addr = inet_addr(APX_IP_ADDR);  
  apxData_addr.sin_family = AF_INET;
  apxData_addr.sin_port = htons(APX_DATA_PORT);  					
 
 // inet_aton(APX_IP_ADDR, &apxControl_addr.sin_addr); 	
  memset(&(apxControl_addr.sin_zero), '\0', 8);
  
  if (connect(apxControlSocket,(struct sockaddr *)&apxControl_addr,sizeof(apxControl_addr)) < 0)
  { 
    cout << "ERROR connecting to Applanix Control Port"<<endl;
    cout<< "Error: "<<strerror(errno) << endl;
    sleep(1);
  } 
  else
  { cout<< "Connected to Control Port" << endl; 
    if (connect(apxDataSocket,(struct sockaddr *)&apxData_addr,sizeof(apxData_addr)) < 0)
    { 
      cout << ("ERROR connecting to Applanix Data Port") << endl;
      cout<< "Error: "<<strerror(errno) << endl;
      sleep(1);
    }
    else 
    { 
      cout<< "Connected to Data Port" << endl; 
    }
  }
  
 //THIS IS A BUNCH OF CRAP THAT SHOULD PROBABLY GO SOMEWHERE ELSE

  stateMode = PREINIT;

  time_t t = time(NULL);
  tm *local;					//THIS SET THE TIME
  local = localtime(&t);

  // end of the bunch of crap
  
  // Set up files for raw logging.
  if (_astateOpts.logRaw == 1)
  {
    mkdir("/tmp/logs/astate_raw/",
          S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH
          | S_IXOTH);
  }

  if (_astateOpts.logRaw == 1)
  { 
   	
    sprintf(apxLogFile,
            "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_APX.raw",
            local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
            local->tm_hour, local->tm_min, local->tm_sec);

    apxLogStream.open(apxLogFile, fstream::out | fstream::binary);
  }



  // Set up files for raw replay.

  if (_astateOpts.useReplay == 1)
  { 
    
    sprintf(apxReplayFile, "%s_APX.raw", _astateOpts.replayFile);
    apxReplayStream.open(apxReplayFile, fstream::in | fstream::binary);

    if (!apxReplayStream)
    {
      cerr << "Problems opening file. "  <<        endl;
      quitPressed = 1;
      return;
    }

  }

  //Find startting time;
  DGCgettime(startTime);

  //Initialize EVERYTHING to be zeroed for good measure.
  _vehicleState.Timestamp = startTime;
  _vehicleState.Northing = 0;
  _vehicleState.Easting = 0;
  _vehicleState.Altitude = 0;
  _vehicleState.Vel_N = 0;
  _vehicleState.Vel_E = 0;
  _vehicleState.Vel_D = 0;
  _vehicleState.Acc_N = 0;
  _vehicleState.Acc_E = 0;
  _vehicleState.Acc_D = 0;
  _vehicleState.Roll = 0;
  _vehicleState.Pitch = 0;
  _vehicleState.Yaw = 0;
  _vehicleState.RollAcc = 0;
  _vehicleState.PitchAcc = 0;
  _vehicleState.YawAcc = 0;

  //Initialize metastate struct
  _metaState.apxEnabled = 0;
  _metaState.apxActive = 0;
  _metaState.apxStatus = NO_SOLUTION;
  _metaState.Timestamp = startTime;
 

  //Create mutexes:
  DGCcreateMutex(&m_VehicleStateMutex);
  DGCcreateMutex(&m_MetaStateMutex);
  DGCcreateMutex(&m_ApxDataMutex);
  DGCcreateMutex(&m_HeartbeatMutex);
  
  DGCcreateCondition(&newData);
  DGCcreateCondition(&apxBufferFree);

  DGCSetConditionTrue(apxBufferFree);
  
  //Set up socket for skynet broadcasting:
  broadcastStateSock = m_skynet.get_send_sock(SNstate);
  if (broadcastStateSock < 0)
  {
    cerr << "AState::AState(): skynet get_send_sock returned error" << endl;
  }

  if (_astateOpts.timberPlayback == 1)
  {
    cout << "Starting timber playback..." << endl;
    DGCstartMemberFunctionThread(this, &AState::playback);
  }
  else
  {
    //Initialize sensors:

    apxBufferLastInd = 0;
    apxBufferReadInd = 0;

    if (_astateOpts.useApplanix == 0)
    {
      DGClockMutex(&m_MetaStateMutex);
      _metaState.apxEnabled = 0;
      DGCunlockMutex(&m_MetaStateMutex);
    }
    else
    {
      apxInit();                // This will set apxEnabled
    }

    if (_metaState.apxEnabled == 1)
    {
      DGCstartMemberFunctionThread(this, &AState::apxControlThread); //this will be the thread to control apx-> maybe this shall be moved to apxInit();
      DGCstartMemberFunctionThread(this, &AState::apxReadThread); //this will be the Thread to read from apx
    }
    
    DGCstartMemberFunctionThread(this, &AState::updateStateThread); //this will be the Thread to read from apx
    DGCstartMemberFunctionThread(this, &AState::metaStateThread); //this will be the Thread to read from apx

    //Start sparrow threads
    if (_astateOpts.useSparrow)
    {
      DGCstartMemberFunctionThread(this, &AState::sparrowThread);
    }
  }
} // end AState::AState() constructor

void AState::restart()
{
  // Lock all the mutexes--this will stop all the threads at their locks.
  DGClockMutex(&m_VehicleStateMutex);
  DGClockMutex(&m_MetaStateMutex);
  DGClockMutex(&m_ApxDataMutex);
  DGClockMutex(&m_HeartbeatMutex);

  // Reinitialize all the values

  //Initialize EVERYTHING to be zeroed for good measure.
  _vehicleState.Timestamp = startTime;
  _vehicleState.Northing = 0;
  _vehicleState.Easting = 0;
  _vehicleState.Altitude = 0;
  _vehicleState.Vel_N = 0;
  _vehicleState.Vel_E = 0;
  _vehicleState.Vel_D = 0;
  _vehicleState.Acc_N = 0;
  _vehicleState.Acc_E = 0;
  _vehicleState.Acc_D = 0;
  _vehicleState.Roll = 0;
  _vehicleState.Pitch = 0;
  _vehicleState.Yaw = 0;
  _vehicleState.RollAcc = 0;
  _vehicleState.PitchAcc = 0;
  _vehicleState.YawAcc = 0;

  //Values relevent to sparrow:
  apxCount = 0;
  apxDead = 1;
  
  apxHeight = -1;
  apxNorth = -1;
  apxEast = -1;
  apxVelN = -1;
  apxVelE = -1;
  apxVelD = -1;
  apxAccN = -1;
  apxAccE = -1;
  apxAccD = -1;
  apxRoll = -1;
  apxPitch = -1;
  apxHeading = -1;
  apxRollRate = -1;
  apxPitchRate = -1;
  apxHeadingRate = -1;
// estimate accuracy
  apxHeight_accuracy = 1;
  apxNorth_accuracy = -1;
  apxEast_accuracy = -1;
  apxVelN_accuracy = -1;
  apxVelE_accuracy = -1;
  apxVelD_accuracy = -1;
  apxAccN_accuracy = -1;
  apxAccE_accuracy = -1;
  apxAccD_accuracy = -1;
  apxRoll_accuracy = -1;
  apxPitch_accuracy = -1;
  apxHeading_accuracy = -1;
  apxRollRate_accuracy = -1;
  apxPitchRate_accuracy = -1;
  apxHeadingRate_accuracy = -1;
 
  stateMode = PREINIT;

  // Unlock the mutexes and begin again.
  DGCunlockMutex(&m_VehicleStateMutex);
  DGCunlockMutex(&m_MetaStateMutex);
  DGCunlockMutex(&m_ApxDataMutex);
  DGCunlockMutex(&m_HeartbeatMutex);
} // end AState::restart()

AState::~AState()
{
  if (_astateOpts.logRaw == 1)
  {
    apxLogStream.close();
  }

  if (_astateOpts.useReplay == 1)
  {
    apxReplayStream.close();
  }
    
  close(apxDataSocket);
  close(apxControlSocket); 

} // end AState::~AState() destructor

// VehicleState messaging Method.
void AState::broadcast()
{
  pthread_mutex_t *pMutex = &m_VehicleStateMutex;
  if (m_skynet.send_msg(broadcastStateSock,
                        &_vehicleState,
                        sizeof(VehicleState),
                        0, &pMutex) != sizeof(VehicleState))
  {
    cerr << "AState::Broadcast(): didn't send right size state message" <<
      endl;
  }
  if (getLoggingEnabled())
  {
    if (checkNewDirAndReset())
    {
      cout << "Opening new file" << endl;
      if (timberLogStream.is_open())
      {
        timberLogStream.close();
      }
      sprintf(timberLogFile, "%sstate.dat", getLogDir().c_str());
      timberLogStream.open(timberLogFile, fstream::out | fstream::app);
    }
    timberLogStream.precision(10);
    timberLogStream << _vehicleState.Timestamp << '\t';
    timberLogStream << _vehicleState.Northing << '\t' << _vehicleState.
      Easting << '\t' << _vehicleState.Altitude << "\t";
    timberLogStream << _vehicleState.Vel_N << '\t' << _vehicleState.
      Vel_E << '\t' << _vehicleState.Vel_D << "\t";
    timberLogStream << _vehicleState.Acc_N << '\t' << _vehicleState.
      Acc_E << '\t' << _vehicleState.Acc_D << "\t";
    timberLogStream << _vehicleState.Roll << '\t' << _vehicleState.
      Pitch << '\t' << _vehicleState.Yaw << "\t";
    timberLogStream << _vehicleState.RollRate << '\t' << _vehicleState.
      PitchRate << '\t' << _vehicleState.YawRate << "\t";
    timberLogStream << _vehicleState.RollAcc << '\t' << _vehicleState.
      PitchAcc << '\t' << _vehicleState.YawAcc << '\t';
    timberLogStream << _vehicleState.NorthConf << '\t' << _vehicleState.
      EastConf << '\t' << _vehicleState.HeightConf << '\t';
    timberLogStream << _vehicleState.RollConf << '\t' << _vehicleState.
      PitchConf << '\t' << _vehicleState.YawConf << endl;
  }
} // end AState::broadcast()

				// I did not change these two functions, since they are based only on _VehicleState value
void AState::playback()
{
  unsigned long long longTmp;
  double doubleTmp;
  unsigned long long playbackTime;
  sprintf(timberPlaybackFile, "%sstate.dat", getPlaybackDir().c_str());
  cout << "Opening file... " << timberPlaybackFile << endl;
  timberPlaybackStream.open(timberPlaybackFile);
  _vehicleState.Timestamp = 0;
  while (1)
  {
    if (checkNewPlaybackDirAndReset())
    {
      if (timberPlaybackStream.is_open())
      {
        timberPlaybackStream.close();
      }
      sprintf(timberPlaybackFile, "%sstate.dat", getPlaybackDir().c_str());
      cout << "Re-Opening file... " << timberPlaybackFile << endl;
      timberPlaybackStream.open(timberPlaybackFile);
    }
    if (!timberPlaybackStream)
    {
      cout << __FILE__ << " [" << __LINE__ << "]: " <<
      "Reached the end of the playback file, or couldn't open it to begin with"
        << endl;
      exit(0);
    }
    else
    {
      unsigned long long timberTime = getPlaybackTime();
      while (timberTime >= _vehicleState.Timestamp && timberPlaybackStream)
      {
        timberPlaybackStream >> longTmp;
        _vehicleState.Timestamp = longTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Northing = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Easting = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Altitude = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Vel_N = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Vel_E = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Vel_D = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Acc_N = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Acc_E = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Acc_D = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Roll = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Pitch = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.Yaw = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.RollRate = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.PitchRate = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.YawRate = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.RollAcc = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.PitchAcc = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.YawAcc = doubleTmp;

          //We're not playing back an old log
        timberPlaybackStream >> doubleTmp;
        _vehicleState.NorthConf = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.EastConf = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.HeightConf = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.RollConf = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.PitchConf = doubleTmp;

        timberPlaybackStream >> doubleTmp;
        _vehicleState.YawConf = doubleTmp;

        
 
      }
      playbackTime = getPlaybackTime();
      if (DEBUG_LEVEL > 1) 
      {
        printf("[%s:%d] playbackTime = %llu\n", __FILE__, __LINE__, 
                        playbackTime);
        printf("[%s:%d] (fileTime) - playbackTime = %llu\n", __FILE__, 
                        __LINE__, _vehicleState.Timestamp - playbackTime );
      }

      if (_vehicleState.Timestamp > playbackTime)
      {
        if (DEBUG_LEVEL > 1) 
        {
          printf("[%s:%d] Blocking until %llu (%.3f sec).\n", __FILE__, 
                          __LINE__, _vehicleState.Timestamp, 
                          (double)(_vehicleState.Timestamp - 
                                   playbackTime)/1.0e6);
        }
        blockUntilTime(_vehicleState.Timestamp);
        broadcast();
      }
    }
  }
} // end AState::playback()



void AState::active()
{
  while (!quitPressed)
  {
    sleep(1);
  }
}
