/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include <getopt.h>
#include <iostream>
#include "GloNavMapLib.hh"
#include "DGCutils"
#include "skynet.hh"
#include "cmdline_gloNavMapLib.h"

using namespace std;

int main(int argc, char **argv) 
{
  gengetopt_args_info cmdline;
  int sn_key;
  
  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
  
  /* Figure out what skynet key to use */
  sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;
  cout << "RNDF file: " << cmdline.rndf_arg << endl;

  CGloNavMapLib* pGloNavMapLib = new CGloNavMapLib(sn_key, cmdline.rndf_arg);
  
  DGCstartMemberFunctionThread(pGloNavMapLib, &CGloNavMapLib::sendGlobalGloNavMapThread);
  DGCstartMemberFunctionThread(pGloNavMapLib, &CGloNavMapLib::sendLocalGloNavMapThread);

  pGloNavMapLib->getGlobalGloNavMapThread();

  return 0;
}
