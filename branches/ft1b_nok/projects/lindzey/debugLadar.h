#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <termios.h>
#include <unistd.h>
#include <netinet/in.h>  /* for struct sockaddr_in, htons(3) */
#include <sys/ioctl.h>
#include <math.h>
#include <sstream>
#include <fstream>
#include <linux/serial.h>

#define LOBYTE(w) ((uint8_t) (w & 0xFF))
#define HIBYTE(w) ((uint8_t) ((w >> 8) & 0xFF))
#define MAKEUINT16(a,b) ((unsigned short)(a)|((unsigned short)(b)<<8))

#define RETURN_ERROR(erc, m) {return erc;}

#define STX '\x02'
#define ACK     0xA0
#define NACK    0x92
#define CRC16_GEN_POL 0x8005

using namespace std;

int main();

//private:
int Setup();

ssize_t WriteToLaser(uint8_t *data, ssize_t len);
ssize_t ReadFromLaser();

int RequestTest();
int RequestStatus();
int ChangeRate ();
int RequestScan();
int ChangeComputerPortRate ();

unsigned short CreateCRC(uint8_t* data, ssize_t len);
int PrintTelegram(uint8_t *data, ssize_t len);
void DGCgettime(unsigned long long& DGCtime);

int laser_term;
uint8_t grrr[2048];
