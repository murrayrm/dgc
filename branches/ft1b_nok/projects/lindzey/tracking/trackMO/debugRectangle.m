function debugRectangle()

points = load('points.dat');
x_pts = points(1,:);
y_pts = points(2,:);

figure();
  plot(x_pts,y_pts,'.','LineWidth',1);
hold on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% this code is trying to plot the cars from the C++ code
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cars = load('carsP.dat');
[row,col] = size(cars);

theta1 = [];
rho1   = [];
a1     = [];
b1     = [];
theta2 = [];
rho2   = [];
a2     = [];
b2     = [];
carx  = [];
cary  = [];

%for i=1:col/10
for i=1:row
%for i=argh:argh
    if (cars(i,1)~=NaN) && (abs(cars(i,2))~=Inf) && (cars(i,2) ~= 0 || cars(i,6) ~= 0)
       theta1 = [theta1, cars(i,1)];
       rho1   = [rho1,   cars(i,2)];
       a1     = [a1,     cars(i,3)];
       b1     = [b1,     cars(i,4)];
       theta2 = [theta2, cars(i,5)];
       rho2   = [rho2,   cars(i,6)];
       a2     = [a2,     cars(i,7)];
       b2     = [b2,     cars(i,8)];   
       carx   = [carx,   cars(i,9)];          
       cary   = [cary,   cars(i,10)];          
    end
end
% 
% theta1
% rho1
% a1
% b1
% theta2
% rho2
% a2
% b2

x_a1 = rho1.*cos(theta1) + a1.*cos(theta1+pi/2);
y_a1 = rho1.*sin(theta1) + a1.*sin(theta1+pi/2);

x_b1 = rho1.*cos(theta1) + b1.*cos(theta1+pi/2);
y_b1 = rho1.*sin(theta1) + b1.*sin(theta1+pi/2);

x_a2 = rho2.*cos(theta2) + a2.*cos(theta2+pi/2);
y_a2 = rho2.*sin(theta2) + a2.*sin(theta2+pi/2);

x_b2 = rho2.*cos(theta2) + b2.*cos(theta2+pi/2);
y_b2 = rho2.*sin(theta2) + b2.*sin(theta2+pi/2);

% figure(3);
% plot(x_pts,y_pts,'k.');
% hold on;
% i
% [theta1,theta2]
% [rho1,a1,b1,rho2,a2,b2]
 [x_a1,x_a2,x_b1,x_b2;y_a1,y_a2,x_b1,x_b2];
%plot([x_a1,x_b1],[y_a1,y_b1],'k');


plot(x_a1,y_a1,'ro');
%plot(x_a2,y_a2,'bo');
plot(x_b1,y_b1,'co');
%plot(x_b2,y_b2,'go');
%figure()



len = length(x_a1);
for i=1:len
%   plot([x_a1(i),x_b1(i)],[y_a1(i),y_b1(i)],'k'); 
end


figure(); hold on;
plot(x_pts,y_pts,'k.');
plot(x_a1,y_a1,'ro');
plot(x_a2,y_a2,'bo');
plot(x_b1,y_b1,'co');
plot(x_b2,y_b2,'go');
plot(carx, cary, 'kp');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% this code is trying to create cars in matlab
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%segments = load('segments.dat');
%[row,col] = size(segments);


% figure();
% plot(x_pts,y_pts,'k.');
% hold on;

%for i=1:col/2
%  if segments(2*i) > 0 &&segments(2*i-1) > 0 && (segments(2*i)-segments(2*i-1)>2)
%      objx = x_pts(segments(2*i-1)+1:segments(2*i)+1);
%      objy = y_pts(segments(2*i-1)+1:segments(2*i)+1);
      
%      [foo,num] = size(objx);
%      ssxx = sum(objx.*objx) - num*mean(objx)*mean(objx);
%      ssyy = sum(objy.*objy) - num*mean(objy)*mean(objy);
%      ssxy = sum(objx.*objy) - num*mean(objx)*mean(objy);

%      b = ssxy/ssxx;
%      a = mean(objy) - b*mean(objx);

      %now, trying to get in alpha, rho, r1, r2 format
%      if a < 0
%          theta = atan(b)+3*pi/2;
%      else
%          theta = atan(b)+pi/2;
%      end
            
%      xi = a/(tan(theta)-b);
%      yi = a+b*a/(tan(theta)-b);
%      rho = sqrt(xi*xi+yi*yi);
  
%      r1abs = sqrt((xi-objx(1))^2 + (yi-objy(1))^2);
%      r2abs = sqrt((xi-objx(num))^2 + (yi-objy(num))^2);
      
%      if objx(1) > 0
%          theta1 = atan(objy(1)/objx(1));
%      else
%          theta1 = atan(objy(1)/objx(1)) + pi;
%      end

%      if sin(theta1-theta)>0
%          r1 = r1abs;
%      else
%          r1 = -r1abs;
%      end
         
%      if objx(num) > 0
%          theta2 = atan(objy(num)/objx(num));
%      else
%          theta2 = atan(objy(num)/objx(num)) + pi;
%      end

%      if sin(theta2-theta) > 0
%          r2 = r2abs;
%      else
%          r2 = -r2abs;
%      end

%      x1 = rho*cos(theta) + r1*cos(theta+pi/2);
%      y1 = rho*sin(theta) + r1*sin(theta+pi/2);
%      x2 = rho*cos(theta) + r2*cos(theta+pi/2);
%      y2 = rho*sin(theta) + r2*sin(theta+pi/2);

%          figure(2);
%           plot(objx,objy,'.', 'MarkerEdgeColor',rand(1,3));
%           plot([x1,x2],[y1,y2],'k');      

        %plotting to test how well conversions work
%          grr = [-5000000:1000:5000000];
%          figure();
%          hold on;
%          plot(x_pts,y_pts,'k.');
%          plot(grr, a+b*grr,'b');
%          plot(grr, grr*tan(theta),'k');
%          plot(xi,yi,'ro');
%          plot([x1,x2],[y1,y2],'rx');      
         
%          theta
%          rho
%          [r1, r2]
%   end
%end
