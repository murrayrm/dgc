TRACKMO_PATH = $(DGC)/projects/lindzey/tracking/trackMO

TRACKMO_DEPEND_LIBS = $(SPARROWLIB) \
                           $(SKYNETLIB) \
                           $(MODULEHELPERSLIB) \
                           $(MATRIXLIB) \

TRACKMO_DEPEND_SOURCE = \
                           $(TRACKMO_PATH)/trackMO.hh \
                           $(TRACKMO_PATH)/trackMOMain.cc 


TRACKMO_DEPEND = $(TRACKMO_DEPEND_LIBS) \
                      $(TRACKMO_DEPEND_SOURCE)
