/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include "LocalMapTalker.hh"

using namespace std;
using namespace Mapper;

class CLocalMapTalkerTestRecv : public CLocalMapTalker
{
public:
	CLocalMapTalkerTestRecv(int sn_key)
		: CSkynetContainer(MODtrafficplanner, sn_key)
	{
		vector<Mapper::Segment> segments;
		Mapper::Map map(segments);

		  cout << "about to listen...";
		  int mapSocket = m_skynet.listen(SNtrafficLocalMap, MODmapping);
		  cout << " listening!" << endl;
      int i = 1;
  		while(true)
  		{
        int size;
        cout << "about to receive a map...";
        RecvLocalMap(mapSocket, &map, &size);
        cout << " received a map!" << endl;
        map.print();
        cout << "num received = " << i << endl;
        i++;
		}
	}
};

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;

	CLocalMapTalkerTestRecv test(key);
}
