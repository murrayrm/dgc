/* 
	DynamicPlanner_Utilities.hh
	DynamicPlanner
	
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	Created on 12-12-2006.
*/ 

#ifndef DYNAMICPLANNER_UTILITIES_HH
#define DYNAMICPLANNER_UTILITIES_HH

#include "AlgGeom/Polytope.hh"
#include "NURBSBasedOTG/OTG_Interfaces.hh" 
#include "NURBSBasedOTG/OTG_NURBSBuilder.hh"

#include "rddf.hh"

CPolytope* ConstructPolytope(int start_waypoint, int stop_waypoint, RDDFVector rddfvector);
void ConstructInitialGuess(int NofFlatOutputs, NURBS* Nurbs, TimeInterval* timeInterval, int start_waypoint, int stop_waypoint, RDDFVector rddfvector, double** Weights, double*** ControlPoints);

#endif /*DYNAMICPLANNER_UTILITIES_HH*/
