/*
 *  DynamicPlanner_Main.cpp
 *  DynamicPlanner
 *
 *  Created by Melvin E. Flores on 11/21/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

//#include <conio.h>
#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "NURBSBasedOTG/OTG_OCPComponents.hh"	

#include "DynamicPlanner.hh"

using namespace std;

int main(int argc, char* const argv[])
{ 
	cout << "Dynamic Planner is starting..." << endl;
	int SkynetKey   = 0;
	char* pSkynetKey = getenv("SKYNET_KEY");
	if(pSkynetKey != NULL)
	{
	  SkynetKey = atoi(pSkynetKey);
	}
	
	bool WAIT_STATE =  true;

	COCPComponents* OCPComponents = new COCPComponents();	

	CDynamicPlanner* DynamicPlanner = new CDynamicPlanner(SkynetKey, WAIT_STATE, OCPComponents);
	DynamicPlanner->ActiveLoop();

	cout << "Dynamic Planner is ending." << endl;
	//while(!_kbhit());
	
	delete DynamicPlanner;
	delete OCPComponents;
	
	return (int) 0;
} 
