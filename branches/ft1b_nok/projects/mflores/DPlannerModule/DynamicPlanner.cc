/*
 *  DynamicPlanner.cc
 *  DynamicPlanner
 *
 *  Created by Melvin E. Flores on 11/26/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "DynamicPlanner.hh"

CDynamicPlanner:: CDynamicPlanner(int SkynetKey, bool WAIT_STATE, COCPComponents* OCPComponents):CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE), CRDDFTalker(true)
{
	m_i   = 0;
	m_j   = 0;
	m_k   = 0;
	m_ell = 0;
	
	m_Error = 0;
	m_Duration = 0.0;
	
	/* Create an RDDF object */
	m_RDDF = NULL; 				//new RDDF("rddf.dat");
	if(m_RDDF != NULL)
	{
	   cout << "RDDF object was created successfully" << endl;		
	}
		
	/* Create a Traj object (y,yd,ydd,x,xd,xdd) */
	m_Traj = new CTraj(3);
	if(m_Traj != NULL)
	{
	   cout << "Traj object was created successfully" << endl;		
	}
		
	m_CostMapPlus = new CMapPlus();
	if(m_CostMapPlus != NULL)
	{
	   cout << "Cost Map Plus object was created successfully" << endl;		
	}

	m_OCPComponents = OCPComponents;

	/* 
	 * Create Alice problems: Currently only one is present - Alice_Nominal
	 * In the future there will be a set of Alice problems being created here. 
	 */ 
	m_apType = apt_Full;
  
	m_Alice_Full = new CAlice_Full(m_OCPComponents);
	if(m_Alice_Full != NULL)
	{
	   cout << "Alice OC problem definition was created successfully" << endl;	
	}

	m_OCProblem     = NULL;  //new COCProblem(m_Alice_Full);
	if(m_OCProblem != NULL)
	{
	   cout << "OC Problem was created successfully" << endl;
	}		

	m_OCPSolver     = NULL;  //new COCPSolver(m_OCProblem);
	if(m_OCPSolver != NULL)
	{
	   cout << "OCP Solver was created successfully" << endl;
	}
	
	m_AlgGeom = new CAlgebraicGeometry();
	
	/* m_RDDFSocket = m_skynet.listen(SNrddf,MODtrafficplanner); */
	//m_RDDFSocket = m_skynet.listen(SNrddf, MODrddftalkertestsend);
	m_SendSocket = m_skynet.get_send_sock(SNtraj);
		
	m_NofWaypoints = 0;
	m_start_waypoint = 0;
	m_stop_waypoint  = 0;
	
	m_Polytope = NULL;
	
	m_NofDimensions = 0;
	m_NofPoints     = 0;
	m_Points        = NULL; 
	
	m_NofVertices = 0;
	m_Vertices    = NULL;
		
	m_NofRowsOfHform = 0;
	m_NofColsOfHform = 0;
	m_A = NULL;
	m_b = NULL;
	
	m_x_start = 0;
	m_y_start = 0;
	m_v_start = 0;
		
	m_x_stop = 0;
	m_y_stop = 0;
	m_v_stop = 0;
	
	m_theta_start = 0;
	
	m_NofLIC = m_Alice_Full->getNofLIC();
	m_LIC    = new double[m_NofLIC];

	m_NofLFC = m_Alice_Full->getNofLFC();
	m_LFC    = new double[m_NofLFC];
		
	m_NofFlatOutputs             = m_Alice_Full->getNofFlatOutputs();
	m_NofWeights                 = m_Alice_Full->getNofWeights();
	m_NofDimensionsOfFlatOutputs = m_Alice_Full->getNofDimensionsOfFlatOutputs();
	m_NofControlPoints           = m_Alice_Full->getNofControlPoints();

	m_Weights       = new double*[m_NofFlatOutputs];
	m_ControlPoints = new double**[m_NofFlatOutputs];
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		m_Weights[m_i] = new double[m_NofWeights[m_i]];
		m_ControlPoints[m_i] = new double*[m_NofDimensionsOfFlatOutputs[m_i]];
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
			m_ControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
		}
	}
	
	m_NofConstraintCollocPoints = m_Alice_Full->getNofConstraintCollocPoints();
	m_NofStates                 = m_Alice_Full->getNofStates();
	m_NofInputs                 = m_Alice_Full->getNofInputs();
	m_NofParameters             = m_Alice_Full->getNofParameters();
	
	m_NofDerivativesOfStates    = new int[m_NofStates];
	m_NofDerivativesOfInputs    = new int[m_NofInputs];
	
	m_Alice_Full->getNofDerivativesOfStates(m_NofDerivativesOfStates);
	m_Alice_Full->getNofDerivativesOfInputs(m_NofDerivativesOfInputs);
	
	m_Time   = new double[m_NofConstraintCollocPoints];
	m_States = new double**[m_NofConstraintCollocPoints];
	m_Inputs = new double**[m_NofConstraintCollocPoints];
	for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
	{
		m_States[m_i] = new double*[m_NofStates];
		for(m_j=0; m_j<m_NofStates; m_j++)
		{
			m_States[m_i][m_j] = new double[m_NofDerivativesOfStates[m_j]+1];
		}
		
		m_Inputs[m_i] = new double*[m_NofInputs];
		for(m_j=0; m_j<m_NofInputs; m_j++)
		{
			m_Inputs[m_i][m_j] = new double[m_NofDerivativesOfInputs[m_j]+1];
		}
		
	}	
	m_Parameters = new double[m_NofParameters];
}

CDynamicPlanner::~CDynamicPlanner(void)
{
	delete m_RDDF;
	delete m_Traj;
	delete m_CostMapPlus;

	delete m_Alice_Full;
	
	if(m_AlgGeom != NULL)
	{
		delete m_AlgGeom;
	} 

	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		delete[] m_Weights[m_i];
	}
	delete[] m_Weights;

	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
			delete[] m_ControlPoints[m_i][m_j];
		}
		delete[] m_ControlPoints[m_i];
	}
	delete[] m_ControlPoints;
	
	delete[] m_NofWeights;
	delete[] m_NofDimensionsOfFlatOutputs;
	delete[] m_NofControlPoints;
	
	for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
	{
		for(m_j=0; m_j<m_NofStates; m_j++)
		{
			delete[] m_States[m_i][m_j];
		}

		for(m_j=0; m_j<m_NofInputs; m_j++)
		{
			delete[] m_Inputs[m_i][m_j];
		}
		
		delete[] m_States[m_i];
		delete[] m_Inputs[m_i];
	}
	delete[] m_Time;
	delete[] m_States;
	delete[] m_Inputs;
	
	delete[] m_LIC;
	delete[] m_LFC;		
}

void CDynamicPlanner::ActiveLoop(void)
{
	/* Get the current RDDF from Traffic Planner via RDDFTalker */
	cout << "about to receive an rddf ..."<< endl;
	WaitForRDDF();
	cout << "done waiting ..." << endl;
	UpdateRDDF();
	cout << "done updating ..." << endl;
	m_RDDF = &m_newRddf;
	cout << "done getting RDDF object ..."<< endl;
	cout.flush();

	/* Extract the data from the new RDDF file */
    	m_NofWaypoints = m_RDDF->getNumTargetPoints();
    	m_RDDFVector   = m_RDDF->getTargetPoints();

    	cout << "No. of Waypoints = " << m_NofWaypoints << endl;
  	cout << "Printing RDDF ... " << endl;
  	m_RDDF->print();
		
	/* Get current state from astate */
	UpdateState();
	
	cout << endl;
	cout << "Current State: " << endl;
	cout << "t   = " << m_state.Timestamp << endl;
	cout << "x   = " << m_state.Easting   << '\t' << "y   = " << m_state.Northing  << '\t' << "z   = " << m_state.Altitude << endl;
	/*
	cout << "xd  = " << m_state.Vel_N     << '\t' << "yd  = " << m_state.Vel_E     << '\t' << "zd  = " << m_state.Vel_D    << endl;
	cout << "xdd = " << m_state.Acc_N     << '\t' << "ydd = " << m_state.Acc_E     << '\t' << "zdd = " << m_state.Acc_D    << endl;
	cout << "p   = " << m_state.Roll      << '\t' << "q   = " << m_state.Pitch     << '\t' << "r   = " << m_state.Yaw      << endl;
	cout << "pd  = " << m_state.RollRate  << '\t' << "qd  = " << m_state.PitchRate << '\t' << "rd  = " << m_state.YawRate  << endl;
	cout << "pdd = " << m_state.RollAcc   << '\t' << "qdd = " << m_state.PitchAcc  << '\t' << "rdd = " << m_state.YawAcc   << endl;
	cout << endl;
	*/
	cout.flush();

	/* Initialize the trajectory */
	m_Traj->startDataInput();

	for(m_ell=0; m_ell<m_NofWaypoints-1; m_ell++)
	{
		m_start_waypoint = m_ell;
		m_stop_waypoint  = m_ell + 1;
		
		/* Construct a polytope from two consecutive waypoints in the RDDF file */
		m_Polytope = ConstructPolytope(m_start_waypoint, m_stop_waypoint, m_RDDFVector);
		
		m_NofDimensions = m_Polytope->getNofDimensions();
		m_NofPoints     = m_Polytope->getNofPoints();
		m_Points        = m_Polytope->getPoints();
		
		/*
		cout.precision(16);
		cout << "Points: " << endl;
		for(m_i=0; m_i<m_NofDimensions; m_i++)
		{
			for(m_j=0; m_j<m_NofPoints; m_j++){cout << m_Points[m_i][m_j] << "\t";}
			cout << endl;
		}
		cout << endl;
		*/

		/* Find the V-form and the H-form of the polytope */		
		m_AlgGeom->VertexAndFacetEnumeration(m_Polytope);

		m_NofRowsOfHform = m_Polytope->getNofRows();
		m_NofColsOfHform = m_Polytope->getNofColumns();

		//cout << "Rows = " << m_NofRowsOfHform << endl;
		//cout << "Cols = " << m_NofColsOfHform << endl;
		//cout.flush();

		/* Do not do anything if it fails */	
		if(m_NofRowsOfHform == 0 || m_NofColsOfHform == 0)
		{
			//cout << "Press any key to close application." << endl;
			//while(!_kbhit());
	
			/* Free Resources */
			for(m_i=0; m_i<m_NofDimensions; m_i++){delete[] m_Points[m_i];}
			delete[] m_Points;
	
			delete m_Polytope;
			return;
		}
		
		m_NofVertices = m_Polytope->getNofVertices();
		m_Vertices    = m_Polytope->getVertices();
	
		/*
		cout << m_NofVertices << endl;
		cout << endl;
	
		for(m_i=0; m_i<m_NofVertices; m_i++){cout << m_Vertices[m_i] << endl;}
		cout << endl;
		*/

		m_A = m_Polytope->getA();
		m_b = m_Polytope->getb();

		/*
		cout << "A = " << endl;
		for(m_i=0; m_i<m_NofRowsOfHform; m_i++)
		{
			for(m_j=0; m_j<m_NofColsOfHform; m_j++){cout << m_A[m_i][m_j] << '\t';}
			cout << endl;
		}
		cout << endl;

		cout << "b = " << endl;
		for(m_i=0; m_i<m_NofRowsOfHform; m_i++){cout << m_b[m_i] << endl;}
		cout << endl;
		*/

		m_x_start = m_RDDFVector[m_start_waypoint].Easting;
		m_y_start = m_RDDFVector[m_start_waypoint].Northing;
		m_v_start = m_RDDFVector[m_start_waypoint].maxSpeed;
		
		m_x_stop = m_RDDFVector[m_stop_waypoint].Easting;
		m_y_stop = m_RDDFVector[m_stop_waypoint].Northing;
		m_v_stop = m_RDDFVector[m_stop_waypoint].maxSpeed;
	
		m_theta_start = atan2(m_y_stop - m_y_start, m_x_stop - m_x_start);

		/* Modify OC Problem */
		m_LIC[0] = m_x_start;
		m_LIC[1] = m_y_start;
		m_LIC[2] = m_v_start/2;
		m_LIC[3] = m_theta_start;
		m_LIC[4] = 0;
		m_LIC[5] = 0;
		m_Alice_Full->setLinearIC(m_LIC);

		m_LFC[0] = m_x_stop;
		m_LFC[1] = m_y_stop;
		m_LFC[2] = m_v_start/2;
		m_LFC[3] = m_theta_start;
		m_LFC[4] = 0;
		m_LFC[5] = 0;
		m_Alice_Full->setLinearFC(m_LFC);
		
		m_Alice_Full->setFeasibleSet(m_NofRowsOfHform, m_NofColsOfHform, m_A, m_b);
		
		/* Set Parameters */
		m_Parameters[0]  = 5.43560;		// L
		m_Parameters[1]  = 2.13360;		// W
		m_Parameters[2]  = 1.06680;		// hcg
		m_Parameters[3]  = 0.00000;		// v_min
		m_Parameters[4]  = 1.40800;		// v_max
		m_Parameters[5]  =-3.00000;		// a_min
		m_Parameters[6]  = 0.98100;		// a_max
		m_Parameters[7]  =-0.44942;		// phi_min
		m_Parameters[8]  = 0.44942;		// phi_max
		m_Parameters[9]  =-1.30900;		// phid_min
		m_Parameters[10] = 1.30900;		// phid_max
		m_Parameters[11] = 9.81000;		// g
		m_Parameters[12] = 0.35200;		// vi
		m_Parameters[13] = 0.35200;		// vf
		m_Parameters[14] = 0.00000;		// ai
		m_Parameters[15] = 0.00000;		// af		
		
		m_Alice_Full->setParameters(m_Parameters);
		m_Alice_Full->getInitialGuess(m_start_waypoint, m_stop_waypoint, m_RDDFVector, m_Weights, m_ControlPoints);
	
		/* Use NURBSBasedOTG Library */
		m_OCProblem  = new COCProblem(m_Alice_Full);
		m_OCPSolver  = new COCPSolver(m_OCProblem);
				
		m_SolverInform = 0;
		m_OptimalCost  = 0;

        /* Solve OCProblem */
		cout << "----------- NURBSBasedOTG -----------" << endl;
		cout << "Solving Optimal Control Problem ..." << endl;
		m_InitialTime = clock();
		m_OCPSolver->SolveOCProblem(&m_SolverInform, &m_OptimalCost, m_Weights, m_ControlPoints);
		m_FinalTime   = clock();
		cout << "OCP Solver returned with the following flag: " << m_SolverInform << endl; 

		m_Duration = (double)(m_FinalTime-m_InitialTime)/CLOCKS_PER_SEC;
		cout << "Time NURBSBasedOTG took to solve the OCProblem  = "<< m_Duration << " secs" << endl;
		cout << "-------------------------------------" << endl;
		cout.flush();

		/* Recover Original System */
		m_OCProblem->RecoverSystem((const double** const) m_Weights, (const double*** const) m_ControlPoints, m_Time, (double*** const) m_States,(double*** const) m_Inputs);
	
		cout << "Original system recovered successfully ..." << endl;
		/*
		cout << "Recovered System " << endl;
		for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
		{
			cout << m_Time[m_i] << '\t';
			for(m_j=0; m_j<m_NofStates; m_j++)
			{
				for(m_k=0; m_k<m_NofDerivativesOfStates[m_j]+1; m_k++)
				{
				    cout << m_States[m_i][m_j][m_k] << '\t';
				}
			}
	
			for(m_j=0; m_j<m_NofInputs; m_j++)
			{
				for(m_k=0; m_k<m_NofDerivativesOfInputs[m_j]+1; m_k++)
				{
				     cout << m_Inputs[m_i][m_j][m_k] << '\t';
				}
			}
			cout << endl;
		}
		cout << endl;
		*/

		/* Store the trajectory */
		for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
		{
			m_Traj->addPoint(m_States[m_i][1][0],m_States[m_i][1][1],m_States[m_i][1][2],m_States[m_i][0][0],m_States[m_i][0][1],m_States[m_i][0][2]);
		}
			
	    	//cout << "Press any key to close application." << endl;
		//while(!_kbhit());		
		
		/* Free Resources */
		cout << "Freeing resources ..." << endl;
		for(m_i=0; m_i<m_NofDimensions; m_i++){delete[] m_Points[m_i];}
		delete[] m_Points;
		
		delete m_Polytope;
		delete m_Vertices;			
		
		for(m_i=0; m_i<m_NofRowsOfHform; m_i++){delete[] m_A[m_i];}
		delete[] m_A;
		delete[] m_b;
		
		delete m_OCPSolver;
		delete m_OCProblem;	
	}
	/* Sending  trajectory */
	SendTraj(m_SendSocket, m_Traj);

	return;
}

void CDynamicPlanner::SwitchOCProblem(const AliceProblemType* const apType)
{
	m_apType = *apType;	
}
