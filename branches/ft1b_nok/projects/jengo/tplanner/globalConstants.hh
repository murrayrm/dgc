#ifndef GLOBALCONSTANTS_HH_
#define GLOBALCONSTANTS_HH_

/*! \file
 * 
 * \brief Contains all the constants used in tplanner. 
 */

#include <string>
using namespace std;

// TODO: check to see if this is a global constant somewhere
//const double VEHICLE_LENGTH = 5.5; // meters

/*! If a vehicle is moving slower than this, we assume it is stopped. 
 Note: speed is in meters per second */
const double STOPPED_VELOCITY = 0.1;
/*! If a vehicle is moving slower than this, we assume it is queueing
  at an intersection. Note: speed is meters/sec */
const double QUEUEING_VELOCITY = 3;

/*! The number of possible states for Alice. */
const int NUM_STATES = 6;
/*! A description of each state. Used by tplanner::navRoad and 
 * (implicitly) by Environment::evaluate() */
const string modes[6] = { "Unknown mode",               
			  "Stop",                       /* 1 */
			  "Continue driving as before", /* 2 */
			  "Change lanes (left)",        /* 3 */
			  "Change lanes (right)",       /* 4 */
			  "Pass", };                    /* 5 */

#endif
