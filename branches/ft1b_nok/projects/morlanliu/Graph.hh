#ifndef GRAPH_HH_
#define GRAPH_HH_
#include "Vertex.hh"
#include <vector>
using namespace std;

/*! Graph class. Represents a graph of vertices and edges. A graph contains a
 *  vector of Vertex pointers.
 * \brief The Graph class used for the routePlanner.
 */
class Graph
{
public:
	Graph();
	virtual ~Graph();
  
/*! Returns the pointer of the vertex with segmentID, laneID, and waypointID
 *  contained in THIS. */
  Vertex* getVertex(int segmentID, int laneID, int waypointID);
  
/*! Returns the vector of vertex pointers contained in THIS. */
  vector<Vertex*> getVertices();
  
/*! Adds a vertex with segmentID, laneID, and waypointID to THIS. */
  bool addVertex(int segmentID, int laneID, int waypointID);
  
/*! Adds an edge from vertex with segmentID1, laneID1, waypointID1 to the
 *  vertex with segmentID2, laneID2, and waypointID2. */
  bool addEdge(int segmentID1, int laneID1, int waypointID1, 
  int segmentID2, int laneID2, int waypointID2);
  
/*! Prints the vertices contained in THIS. */
  void print();
  
private:
  vector<Vertex*> vertices;
};

#endif /*GRAPH_HH_*/
