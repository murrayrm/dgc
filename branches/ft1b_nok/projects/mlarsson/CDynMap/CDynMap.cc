#include "CDynMap.hh"
#include "AliceConstants.h"
#include "movingObstacle.hh"
#include <limits.h>   
#include <iostream>
#include <sys/time.h>
using namespace std;
 



CDynMap::CDynMap() : CMap()
{
  inputList      = new movingObstacle[MAX_NUM_OBSTACLES];
  obstaclesGrown = new movingObstacleProperties[MAX_NUM_OBSTACLES];
  IDlist         = new int[MAX_NUM_OBSTACLES];
  numobst        = 0;
}




CDynMap::~CDynMap()
{
  delete[] inputList;
  delete[] obstaclesGrown;
  delete[] IDlist;
}




int CDynMap::initMap(double UTMNorthing, double UTMEasting, int numRows, int numCols, double resRows, double resCols, int verboseLevel){
  int temp =  CMap::initMap(UTMNorthing, UTMEasting, numRows, numCols, resRows, resCols, verboseLevel);
  return temp;
}




int CDynMap::initMap(char *fileName)
{
  int ret = CMap::initMap(fileName);
  return ret;
}




bool CDynMap::update(movingObstacle* obstacle)
{
  int ID = obstacle->ID;

  if (ID >= MAX_NUM_OBSTACLES) {
    cerr << "CDynMap::update: Invalid obstacle ID." << endl;
    return false;
  }

  // Check if this obstacle ID is already in the list
  int i;
  for (i = 0; i < numobst; i++) {
    if (IDlist[i] == ID)
      break;
  }

  // If the ID was not in the list, add this obstacle:
  if (i == numobst) {
    if (numobst == MAX_NUM_OBSTACLES) {
      cerr << "CDynMap::update: Trying to add more than MAX_NUM_OBSTACLES (" << MAX_NUM_OBSTACLES << ") obstacles." << endl;
      return false;
    }
    IDlist[numobst] = ID;
    numobst++;
  }

  // copy all received obstacle data to CDynMap's internal representation
  memcpy(&inputList[ID], obstacle, sizeof(movingObstacle));

  // compute length and width of the obstacle and store in the obstaclesGrown[] array. These are the actual
  // length and width, before obstacle growing.
  if (obstacle->status == NEW) {
    obstaclesGrown[ID].length = hypot(obstacle->side1.N, obstacle->side1.E);
    obstaclesGrown[ID].width  = hypot(obstacle->side2.N, obstacle->side2.E);
  }
  
  return true;
}




void CDynMap::growObstacles(double cos_theta, double sin_theta)
#warning "Anisotropical growing not yet implemented"
{

  for (int i = 0; i < numobst; i++) {

    movingObstacle *obst = &inputList[ IDlist[i] ];
    movingObstacleProperties *grown = &obstaclesGrown[ IDlist[i] ];

    if (obst->status != INACTIVE && obst->status != OUTSIDE_MAP) {

      // Compute velocity and speed. This should eventually come directly
      // from detection and tracking.
      //grown->vel.N = (obst->cornerPos[9].N - obst->cornerPos[8].N) / TIME_RESOLUTION;
      //grown->vel.E = (obst->cornerPos[9].E - obst->cornerPos[8].E) / TIME_RESOLUTION;
      double speed = hypot(obst->vel.N, obst->vel.E);

      // Amount to grow. This should eventually be done anisotropically depending on
      // Alice's current orientation.
      double deltaTime = 0;
      //double deltaTime = TIME_GROW * speed;
      double deltaSpace = VEHICLE_LENGTH / 4;

 
      // Multiply the side vectors (corresponding to length and width respectively) with these numbers to lengthen them
      // by the proper amount.
      double lengthFactor = 1 + 2*(deltaTime + deltaSpace) / grown->length;   // Growing in space and time
      double widthFactor  = 1 + 2*deltaSpace / grown->width;                  // Growing in space (growing in time
                                                                              // gives no contribution)

      // Calculate new corner position relative the position corresponding to the time indicated in
      // the obstacle's timestamp:
      grown->cornerPos = obst->cornerPos[0] - obst->side1 * (deltaTime + deltaSpace) / grown->length - 
	obst->side2 * deltaSpace / grown->width;


      /*
      cout << setprecision(10);
      cout << __FILE__ << ":" << __LINE__ << ": new corner position: (" << grown->cornerPos.N << ", "
	   << grown->cornerPos.E << ")" << endl;
      */

      // Lengthen side vectors:
      grown->side1 = obst->side1 * lengthFactor;
      grown->side2 = obst->side2 * widthFactor;
      
    }
  }
}




bool CDynMap::getCellDataWin(int winRow, int winCol, double time)
{
  NEcoord point;
  Win2UTM(winRow, winCol, &point.N, &point.E);
  return getCellData(point, time);
}




bool CDynMap::getCellData(NEcoord point, double time)
{

  for(int i = 0; i < numobst; i++){

    movingObstacle *obst = &inputList[ IDlist[i] ];
    movingObstacleProperties *grown = &obstaclesGrown[ IDlist[i] ];

    if (obst->status != INACTIVE && obst->status != OUTSIDE_MAP) {
      
      // Time passed between obstacle's timestamp and specified time
      double deltaT = ( time - ((double) obst->timeStamp) / 1.0e6 );

      // Compute the vectors from 'point' to corner and 'point' to the opposite corner, while taking
      // into account the obstacle's velocity and the time between 'time' and the obstacle's timestamp.
      NEcoord tempvec1 = point - grown->cornerPos - obst->vel * deltaT;
      NEcoord tempvec2 = point - grown->cornerPos - obst->vel * deltaT - grown->side1 - grown->side2;

      // Check whether the specified spacetime location is inside an obstacle or not, and
      // return true or false accordingly.
      if( tempvec1 * grown->side1 >= 0   &&   tempvec1 * grown->side2 >= 0   &&
	  tempvec2 * grown->side1 <= 0   &&   tempvec2 * grown->side2 <= 0) {
	// There is at least one moving obstacle at this spacetime location.
	return false;
      }
    }
  } 

  // No moving obstacles  at this spacetime location
  return true;
}




void CDynMap::getDataRectangle(double UTMNorthing, double UTMEasting, double Time,
			    double cos_theta, double sin_theta, double radlat, double radlong, 
			    bool* pF, double* pX, double* pY, int* numCells)
{
  // Start out by growing all obstacles:
  growObstacles(cos_theta, sin_theta);



  // Most of the following code is a copy of CMap::getDataRectangle(). The main difference
  // is that getCellData() is called to get the content of a specific cell, rather than the memory
  // being accessed directly by getDataRectangle().
  NEcoord C1(cos_theta*radlong - sin_theta*radlat,
	     sin_theta*radlong + cos_theta*radlat);
  NEcoord C2(cos_theta*radlong + sin_theta*radlat,
	     sin_theta*radlong - cos_theta*radlat);
  
  NEcoord* pCornerS;
  NEcoord* pCornerW;
  
  if(fabs(C1.N) < fabs(C2.N))
    {
      if(C2.N > 0.0)
	C2 *= -1.0;
      
      if(C1.E > 0.0)
	C1 *= -1.0;
      
      pCornerS = &C2;
      pCornerW = &C1;
    }
  else
    {
      if(C1.N > 0.0)
	C1 *= -1.0;
      
      if(C2.E > 0.0)
	C2 *= -1.0;
      
      pCornerS = &C1;
      pCornerW = &C2;
    }
  
  int winrowS, winrowN, wincolE, wincolW;  // window coords of the cells at the corners of our rectangle
  int winrowM, wincolM;                    // window coords of the start of the memory
  
 Mem2Win(0, 0, &winrowM, &wincolM);
  
 UTM2Win(UTMNorthing + pCornerS->N, UTMEasting + pCornerW->E, &winrowS, &wincolW);
 UTM2Win(UTMNorthing - pCornerS->N, UTMEasting - pCornerW->E, &winrowN, &wincolE);
  
  int row, col;
  double midCellN, midCellE;
  double midCellEStartRow;
  double x,y;
  int firstRow, firstCol;
  int lastRow,  lastCol;
  *numCells = 0;
  
  
  firstCol = wincolW;
  lastRow  = min(winrowM, winrowN+1);
  lastCol  = min(wincolM, wincolE+1);
  if(firstCol < lastCol)
    {
      firstRow = winrowS;
      if(firstRow < lastRow)
	{
	  Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
	  midCellN         += 0.5*_resRows - UTMNorthing;
	  midCellEStartRow += 0.5*_resCols - UTMEasting;
	  for(row = firstRow; row < lastRow; row++)
	    {
	      midCellE = midCellEStartRow;
	      for(col = firstCol; col < lastCol; col++)
		{
		  x = midCellN*cos_theta + midCellE*sin_theta;
		  y = midCellN*sin_theta - midCellE*cos_theta;
		  if(fabs(x) < radlong && fabs(y) < radlat)
		    {
		      pF[*numCells] = getCellDataWin(row, col, Time);
		      pX[*numCells] = x;
		      pY[*numCells] = y;
		      (*numCells)++;
		    }
		  midCellE += _resCols;
		}
	      midCellN += _resRows;
	    }
	}
      
      firstRow = max(winrowM, winrowS);
      if(firstRow <= winrowN)
	{
	  Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
	  midCellN         += 0.5*_resRows - UTMNorthing;
	  midCellEStartRow += 0.5*_resCols - UTMEasting;
	  for(row = firstRow; row <= winrowN; row++)
	    {
	      midCellE = midCellEStartRow;
	      for(col = firstCol; col < lastCol; col++)
		{
		  x = midCellN*cos_theta + midCellE*sin_theta;
		  y = midCellN*sin_theta - midCellE*cos_theta;
		  if(fabs(x) < radlong && fabs(y) < radlat)
		    {
		      pF[*numCells] = getCellDataWin(row, col, Time);
		      pX[*numCells] = x;
		      pY[*numCells] = y;
		      (*numCells)++;
		    }
		  midCellE += _resCols;
		}
	      midCellN += _resRows;
	    }
	}
    }
  
  firstCol = max(wincolM, wincolW);
  if(firstCol <= wincolE)
    {
      firstRow = winrowS;
      if(firstRow < min(winrowM, winrowN+1))
	{
	  Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
	  midCellN         += 0.5*_resRows - UTMNorthing;
	  midCellEStartRow += 0.5*_resCols - UTMEasting;
	  for(row = firstRow; row < lastRow; row++)
	    {
	      midCellE = midCellEStartRow;
	      for(col = firstCol; col <= wincolE; col++)
		{
		  x = midCellN*cos_theta + midCellE*sin_theta;
		  y = midCellN*sin_theta - midCellE*cos_theta;
		  if(fabs(x) < radlong && fabs(y) < radlat)
		    {
		      pF[*numCells] = getCellDataWin(row, col, Time);
		      pX[*numCells] = x;
		      pY[*numCells] = y;
		      (*numCells)++;
		    }
		  midCellE += _resCols;
		}
	      midCellN += _resRows;
	    }
	}
      
      firstRow = max(winrowM, winrowS);
      if(firstRow <= winrowN)
	{
	  Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
	  midCellN         += 0.5*_resRows - UTMNorthing;
	  midCellEStartRow += 0.5*_resCols - UTMEasting;
	  for(row = firstRow; row <= winrowN; row++)
	    {
	      midCellE = midCellEStartRow;
	      for(col = firstCol; col <= wincolE; col++)
		{
		  x = midCellN*cos_theta + midCellE*sin_theta;
		  y = midCellN*sin_theta - midCellE*cos_theta;
		  if(fabs(x) < radlong && fabs(y) < radlat)
		    {
		      pF[*numCells] = getCellDataWin(row, col, Time);
		      pX[*numCells] = x;
		      pY[*numCells] = y;
		      (*numCells)++;
		    }
		  midCellE += _resCols;
		}
	      midCellN += _resRows;
	    }
	}
    }
}




bool CDynMap::checkRectangle(double UTMNorthing, double UTMEasting, double Time, double cos_theta, double sin_theta,
			double radlat, double radlong)
{
  // Start out by growing all obstacles:
  growObstacles(cos_theta, sin_theta);


  NEcoord C1(cos_theta*radlong - sin_theta*radlat,
	     sin_theta*radlong + cos_theta*radlat);
  NEcoord C2(cos_theta*radlong + sin_theta*radlat,
	     sin_theta*radlong - cos_theta*radlat);
  
  NEcoord* pCornerS;
  NEcoord* pCornerW;
  
  if(fabs(C1.N) < fabs(C2.N))
    {
      if(C2.N > 0.0)
	C2 *= -1.0;
      
      if(C1.E > 0.0)
	C1 *= -1.0;
      
      pCornerS = &C2;
      pCornerW = &C1;
    }
  else
    {
      if(C1.N > 0.0)
	C1 *= -1.0;
      
      if(C2.E > 0.0)
	C2 *= -1.0;
      
      pCornerS = &C1;
      pCornerW = &C2;
    }
  
  int winrowS, winrowN, wincolE, wincolW;  // window coords of the cells at the corners of our rectangle
  int winrowM, wincolM;                    // window coords of the start of the memory
  
 Mem2Win(0, 0, &winrowM, &wincolM);
  
 UTM2Win(UTMNorthing + pCornerS->N, UTMEasting + pCornerW->E, &winrowS, &wincolW);
 UTM2Win(UTMNorthing - pCornerS->N, UTMEasting - pCornerW->E, &winrowN, &wincolE);
  
  int row, col;
  double midCellN, midCellE;
  double midCellEStartRow;
  double x,y;
  int firstRow, firstCol;
  int lastRow,  lastCol;
  
  firstCol = wincolW;
  lastRow  = min(winrowM, winrowN+1);
  lastCol  = min(wincolM, wincolE+1);
  if(firstCol < lastCol)
    {
      firstRow = winrowS;
      if(firstRow < lastRow)
	{
	  Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
	  midCellN         += 0.5*_resRows - UTMNorthing;
	  midCellEStartRow += 0.5*_resCols - UTMEasting;
	  for(row = firstRow; row < lastRow; row++)
	    {
	      midCellE = midCellEStartRow;
	      for(col = firstCol; col < lastCol; col++)
		{
		  x = midCellN*cos_theta + midCellE*sin_theta;
		  y = midCellN*sin_theta - midCellE*cos_theta;
		  if(fabs(x) < radlong && fabs(y) < radlat)
		    {
		      if (!getCellDataWin(row, col, Time)) 
		      	return false; 
		    }
		  midCellE += _resCols;
		}
	      midCellN += _resRows;
	    }
	}
      
      firstRow = max(winrowM, winrowS);
      if(firstRow <= winrowN)
	{
	  Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
	  midCellN         += 0.5*_resRows - UTMNorthing;
	  midCellEStartRow += 0.5*_resCols - UTMEasting;
	  for(row = firstRow; row <= winrowN; row++)
	    {
	      midCellE = midCellEStartRow;
	      for(col = firstCol; col < lastCol; col++)
		{
		  x = midCellN*cos_theta + midCellE*sin_theta;
		  y = midCellN*sin_theta - midCellE*cos_theta;
		  if(fabs(x) < radlong && fabs(y) < radlat)
		    {
		      if (!getCellDataWin(row, col, Time)) 
		      	return false; 
		    }
		  midCellE += _resCols;
		}
	      midCellN += _resRows;
	    }
	}
    }
  
  firstCol = max(wincolM, wincolW);
  if(firstCol <= wincolE)
    {
      firstRow = winrowS;
      if(firstRow < min(winrowM, winrowN+1))
	{
	  Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
	  midCellN         += 0.5*_resRows - UTMNorthing;
	  midCellEStartRow += 0.5*_resCols - UTMEasting;
	  for(row = firstRow; row < lastRow; row++)
	    {
	      midCellE = midCellEStartRow;
	      for(col = firstCol; col <= wincolE; col++)
		{
		  x = midCellN*cos_theta + midCellE*sin_theta;
		  y = midCellN*sin_theta - midCellE*cos_theta;
		  if(fabs(x) < radlong && fabs(y) < radlat)
		    {
		      if (!getCellDataWin(row, col, Time)) 
		      	return false; 
		    }
		  midCellE += _resCols;
		}
	      midCellN += _resRows;
	    }
	}
      
      firstRow = max(winrowM, winrowS);
      if(firstRow <= winrowN)
	{
	  Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
	  midCellN         += 0.5*_resRows - UTMNorthing;
	  midCellEStartRow += 0.5*_resCols - UTMEasting;
	  for(row = firstRow; row <= winrowN; row++)
	    {
	      midCellE = midCellEStartRow;
	      for(col = firstCol; col <= wincolE; col++)
		{
		  x = midCellN*cos_theta + midCellE*sin_theta;
		  y = midCellN*sin_theta - midCellE*cos_theta;
		  if(fabs(x) < radlong && fabs(y) < radlat)
		    {
		      if (!getCellDataWin(row, col, Time)) 
		      	return false; 
		    }
		  midCellE += _resCols;
		}
	      midCellN += _resRows;
	    }
	}
    }
    return true; 
}

void CDynMap::getTimeData(double UTMNorthing, double UTMEasting, double Time, double deltaT,
		       bool *pF, double *pT, int *numCells)
#warning getTimeData() has not yet been properly implemented
{
  NEcoord point;
  point.N = UTMNorthing;
  point.E =  UTMEasting;
  (*numCells) = int( 2*deltaT / TIME_RESOLUTION );
  double t0 = Time - deltaT;

  for (int i=0 ; i < *numCells; i++) {
    pF[i] = getCellData(point, t0);
    pT[i] = t0;
    t0 += TIME_RESOLUTION;
  }
}
