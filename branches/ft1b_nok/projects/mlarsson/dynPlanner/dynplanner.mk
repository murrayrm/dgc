DYNPLANNER_PATH = $(DGC)/projects/mlarsson/dynPlanner

# Added by mlarsson:
#   RDDFPATHGENLIB
DYNPLANNER_DEPEND_LIBS = \
#	$(RDDFPATHGENLIB) \
	$(RDDFLIB) \
	$(GGISLIB) \
	$(CDYNMAPLIB) \
	$(CMAPLIB) \
	$(CMAPPLIB) \
	$(TRAJLIB) \
	$(CCPAINTLIB) \
	$(MODULEHELPERSLIB) \
	$(FRAMESLIB) \
	$(NLPSOLVERLIB)

DYNPLANNER_DEPEND_SOURCES = \
	$(DYNPLANNER_PATH)/dynplanner.cc \
	$(PLANNER_PATH)/ReactiveStage.cc \
	$(DYNPLANNER_PATH)/dynRefinementStage.h \
	$(DYNPLANNER_PATH)/dynRefinementSetup.cc \
	$(DYNPLANNER_PATH)/dynRefinementStage.cc \
	$(DYNPLANNER_PATH)/dynRefinementConstraintFuncs.cc \
	$(DYNPLANNER_PATH)/MapAccess.cc  \
	$(DYNPLANNER_PATH)/dynplanner.h \
	$(PLANNER_PATH)/ReactiveStage.h \
	$(PLANNER_PATH)/defs.h  \
	$(PLANNER_PATH)/specs.h \
	$(PLANNER_PATH)/specs.cc \
	$(DYNPLANNER_PATH)/Makefile

DYNPLANNER_DEPEND = $(DYNPLANNER_DEPEND_LIBS) $(DYNPLANNER_DEPEND_SOURCES)
