#include "ggis.h"
#include <iostream>
#include <iomanip>
#include <stdlib.h>
using namespace std;

int main(int argc, char *argv[])
{
  if (argc != 3) {
    cout << "Converts from lat-long to UTM coordinates." << endl;
    cout << "Usage: ll2utm long lat" << endl;
    return 0;
  }
  double lat = atof(argv[1]);
  double lon = atof(argv[2]);
  GisCoordLatLon latlon = {lon, lat};
  GisCoordUTM utm;
  if (gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL)) {
    cout << "(N E) =  (" << setprecision(10) << utm.n;
    cout << " " << setprecision(10) << utm.e << ")" << endl;
    cout << "zone = " << utm.zone << "; letter = " << utm.letter << endl;
    return 0;
  } else {
    cout << "Didn't work." << endl;
    return 1;
  }
}
