%
% Reads the performance log file and presents the stats
%
% log file format:
% planner result   ---   total time   ---   cost   ---   constraints
%      ---   integrateNE   ---   integrateT   ---   map access time
%      ---   nbr of map accesses
%perf = load('../dynPlannerModule/logs/perflog.dat');
perf = load('perflog_test2.dat');


figure(8);
clf;

% integrateNE, integrateT, etc.
bardata = [sum(perf(:, 3:4),2) perf(:, 7) sum(perf(:, 5:6),2)];
subplot(312);
hold on
bar(perf(:,2),'w');
bar(bardata,1);
hold off
xlimit = xlim;
title('computation time; total (white boxes), left to right: cost and constraints, map access, integrate NE and T');

% exit status
subplot(311);
exitflag = (perf(:,1) == 0) + (perf(:,1) == 4) + (perf(:,1) == 9);
plot(exitflag, '*');
axis([0 xlimit(2) -1 2]);
title('exit status; 1: succeeded, 0: failed');

% time per map access (in microseconds)
subplot(313);
plot(perf(:,7) ./ perf(:,8) * 1e6);
xlim(xlimit);
title('time per map access (um)');

% histogram over total computation time
figure(2);
clf;
subplot(121);
hist(nonzeros(perf(:,2).*exitflag));
title('histogram over total computation time for converging solutions');
subplot(122);
hist(nonzeros(perf(:,2).*(1-exitflag)));
title('histogram over total computation time for non-converging solutions');
