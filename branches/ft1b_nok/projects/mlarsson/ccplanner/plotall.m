clf;
hold on;
% plottraj
set(gca,'DataAspectRatio',[1 1 1]);
% plotRDDF
plotsetup

ftraj = loadtraj('ftraj');
plot(ftraj(:,4),ftraj(:,1),'r-','LineWidth',4);
plotlm