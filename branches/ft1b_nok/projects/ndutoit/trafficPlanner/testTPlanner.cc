#include "trafficUtils.hh"
//#include "TrafficPlanner.hh"
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>

// Test script to check different functions
int main()
{
	bool DEBUG = true;
	bool VERBOSE = true;
	// Specify boundary
  vector<double> x_B1, y_B1, x_B2, y_B2;
  double x_value, y_value;
  for (int ii=0; ii<11; ii++)
  {
  	x_value = double(ii);
  	y_value = 1;
    x_B1.push_back(x_value);
    y_B1.push_back(y_value);
    y_value = -1;
    x_B2.push_back(x_value);
    y_B2.push_back(y_value);
  }
  int lengthxB = x_B1.size();
  cout << "Boundaries given" << endl;
  for (int ii=0; ii<lengthxB;ii++)
  {
  	printf("%d: (xB1,yB1) = (%3.6f, %3.6f) and (xB2, yB2) = (%3.6f, %3.6f)\n",ii, x_B1[ii], y_B1[ii], x_B2[ii], y_B2[ii]);
  }  
  vector<Constraint> constraints;
  double dist = 20;
  bool insert_flag = true;
  constraints = boundaryConstraints(x_B1, y_B1, x_B2, y_B2, 0.5, 0, dist, insert_flag);
  cout << "boundaries obtained" << endl;
  for (int ii=0; ii<lengthxB;ii++)
  {
  	printf("%d: (xB1,yB1) = (%3.6f, %3.6f) and (xB2, yB2) = (%3.6f, %3.6f)\n",ii, x_B1[ii], y_B1[ii], x_B2[ii], y_B2[ii]);
  }    
  cout << "constraints generated" << endl;
  cout << "constraints.size() = " << constraints.size() << endl;
  for (int ii=0; ii<(int)constraints.size();ii++)
  {
  	printf("%d: (x1,y1) = (%3.6f, %3.6f) and (x2, y2) = (%3.6f, %3.6f)\n",ii, constraints[ii].xrange1, constraints[ii].yrange1, constraints[ii].xrange2, constraints[ii].yrange2);
  }
  
  //Mapper::Map m_localMap_test;
  //generateMapFromRNDF(m_rndf, m_localMap_test);
  //m_localMap_test.print();
}

