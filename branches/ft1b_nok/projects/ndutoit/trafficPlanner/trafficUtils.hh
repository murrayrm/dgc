/*! trafficUtils.hh
 * Author: Noel duToit
 * Jan 25 2007
 */
 
#ifndef TRAFFICUTILS
#define TRAFFICUTILS

#include <iostream>
#include <string>
#include "RNDF.hh"
#include <vector>
#include <list>
#include <math.h>
#include "../../nok/missionPlanner/SegGoals.hh"
#include "../../sidd/Mapper/include/Map.hh"
#include "AliceConstants.h"
#include "CMapPlus.hh"

using namespace std;
//using namespace Mapper;

#define EPS 0.05
#define BIGNUMBER 1000000000.0f 
#define PI 3.1416
#define LANEWIDTH 5.0f
#define ALICE_DESIRED_DECELERATION VEHICLE_MAX_DECEL
#define SEG_COMPLETE_DIST_TO_EXIT 1.0f
#define SEG_ALMOST_COMPLETE_DIST_TO_EXIT 10.0f
#define STOP_SPEED_SQR 1.0f
#define ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER (VEHICLE_WHEELBASE+DIST_FRONT_AXLE_TO_FRONT)
#define DIST_SWITCH_FROM_GPS_TO_SENSORS 10.0f
#define MAX_GPS_SHIFT LANEWIDTH 

enum TrafficRules
{ STAY_IN_LANE, PASSING, U_TURN, TRAFFIC_LINES, DRIVE_IN_CORRECT_DIRECTION, STAY_ON_ROAD }; 

/*!DPlannerStatus structure
 * This structure has not been created*/
struct DPlannerStatus
{
  /*!DPlannerStatus Constructor*/
  DPlannerStatus()
  {
    //initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  void print()
  {
  }
  int goalID;
  bool status;
};

/*!Constraint structure with the following fields:
 * The constraint of the form [A1 A2]8[x; y] <= B over the range (xrange1, xrange2) and (yrange1, yrange2) 
 * double A1
 * double A2
 * double B
 * double xrange1
 * double xrange2
 * double yrange1
 * double yrange2
 */
struct Constraint
{
  /*!Constraint constructor*/
	Constraint()
  {
    //initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
	double A1, A2, B, xrange1, xrange2, yrange1, yrange2;
};

/*!ConstraintSet structure is a vector of constraints.
 * Thus far there are three constraints:
 * rightBoundaryConstraint
 * leftBoundaryConstraint
 * stopLineConstraint
 */
struct ConstraintSet
{
	vector<Constraint> rightBoundaryConstraint, leftBoundaryConstraint, stopLineConstraint;
};

/*! point is a structure that is used in convRDDF and has two fields:
 * double x
 * double y
 */
struct point 
{
	double x ;
    double y ;
};

/*! Function that converts the elevation map into a cost map. Both are of type CMapPlus. 
 * The function takes the arguments:
 * elevationMap
 * costMap
 */
void elevation2Cost(CMapPlus, CMapPlus&);

/*!Returns a vector of type TrafficRules
 * The argument to the function is
 * segmentType of type SegmentType
 */
vector<TrafficRules> extractRules(SegmentType segmentType);

/*! Generates a set of constraints for the action of driving down a road segment
 * input arguments include:
 * laneLBCoord - left boundary points, vector<Mapper:Location>
 * laneRBCoord - right boundary points, vector<Mapper:Location>
 * laneLBType - left boundary type, Mapper::LaneBoundary::Divider
 * laneRBType - right boundary type, Mapper::LaneBoundary::Divider
 * trafficRules - set of traffic rules, vector<TrafficRules>
 * dlimit - distance to the end of the sensing range (do not use data too far away)
 * dist2Exit - distance to the end of the segment
 * x_A - Alice x position in Local coordinates
 * y_A - Alice y position in local coordinates
 */
ConstraintSet laneFollowing(vector<Mapper::Location> laneLBCoord, vector<Mapper::Location> laneRBCoord, Mapper::LaneBoundary::Divider laneLBType, Mapper::LaneBoundary::Divider laneRBType, vector<TrafficRules> trafficRules, double dLimit, double dist2Exit, double x_A, double y_A); 

/*! Generates a set of constraints for the action of driving through an intersection
 * input arguments include:
 * laneLBCoord - left boundary points of the virtual lane through intersection, vector<Mapper:Location>
 * laneRBCoord - right boundary points of the virtual lane through intersection, vector<Mapper:Location>
 * trafficRules - set of traffic rules, vector<TrafficRules>
 * dlimit - distance to the end of the sensing range (do not use data too far away)
 * x_A - Alice x position in Local coordinates
 * y_A - Alice y position in local coordinates
 */
ConstraintSet intersectionNav(vector<Mapper::Location> laneLBCoord, vector<Mapper::Location> laneRBCoord, vector<TrafficRules> trafficRules, double dLimit, double x_A, double y_A);

/*! Fit a line through a number of (x,y) points
 * input arguments are: 
 * x_toFit[] = array of x points, double
 * y_toFit[] = array of y points, double
 * sizeOfXToFit - number of points to be used during the fit, int
 * The function returns the:
 * slope - type double
 * intersection - the y-axis intersection, double
 */
void lineFit(double x_toFit[],double y_toFit[],int sizeOfXToFit,double& slope,double& intersection);

/*!Calculates the closest existing point on the specified boundary
 * input arguments to the function are:
 * xB, yB - vector of points on boundary (x and y respectively), type vector<double>
 * x, y - position of the point to which the closest existing boundary pt is to be found, double
 * returns index of the closest existing point
 */
int closestExistingBoundaryPt(vector<double> xB,vector<double> yB, double x, double y);

/*!Calculates the orthogonal projection of a specified point to the given boundary
 * input arguments are:
 * xB, yB - vector of points on boundary (x and y respectively), type vector<double>
 * x, y - position of the point that is to be projected to the boundary, double
 * ind_CP - the index of the closest existing point on the boundary, type int
 * x_Pt, y_Pt - the location of the projected point which is updated, type double
 * returns:
 * the index where this point is to be inserted
 * x_Pt, y_Pt - location of the projected point which is updated, type double 
 */
int closestPtOnBoundary(vector<double> xB,vector<double> yB, double x, double y, int ind_CP, double& x_Pt, double& y_Pt);

/*! Calculates the distance between two point (2D)
 * input arguments are: 
 * x1, y1 - location of the first point, type double 
 * x2, y2 - location of the second point, type double
 * returns distance between these points
 */
double distanceXY(double x1, double x2, double y1, double y2);

/*! Inserts a point on a given boundary in the given location
 * input arguments are:
 * xB, yB - vector of points on boundary (x and y respectively), type vector<double>
 * x, y - position of the point that is to be inserted in the boundary, double
 * index - the index where the point is to be inserted in the boundary, type int
 */
void insertPointOnBoundary(vector<double>& xB,vector<double>& yB,double x, double y, int index);

/*! Find a point at a distance along the boundary point
 * input arguments are:
 * xB, yB - vector of points on boundary (x and y respectively) and these get updated, type vector<double>
 * index_start - the index of point on the boundary from which the distance should be measured, int
 * index_inserted - the index where the point was inserted, this gets updated, type int
 * distance - the distance to be moved along the boundary
 * returns:
 * xB, yB - updated vector of point on boundary (x and y respectively), type vector<double>
 * index_inserted - the updated index where the point was inserted, type int
 */
void distanceAlongBoundary(vector<double>& xB, vector<double>& yB, int index_start, int& index_inserted, double distance);

/*! Generate the boundary constraints from the boundary specified
 * input arguments are:
 * xB1, yB1 - vector of points on boundary (x and y respectively) that is to be used to generate the constraints, type vector<double>
 * xB2, yB2 - vector of points on boundary (x and y respectively) that is to be used to determine which side of the line we want to be at, type vector<double>
 * x, y - point from which constraints are generated (use this to find the closest point on the boundary and then use this point as the first point of the constraints), double
 * distance - planning horizon or maximum distance along the boundary for which constraints are generated, double
 * insert_flag - boolean variable that says whether or not we want the insert the projected point from x,y on the boundary (there are some cases where this is undesired), bool
 * returns: a set of constraints, type vector<Constraint>
 */
vector<Constraint> boundaryConstraints(vector<double> xB1, vector<double> yB1, vector<double> xB2, vector<double> yB2, double x, double y, double distance, bool insert_flag);

/*! Generates RDDF's from the set of constraints
 * input arguments are:
 * line1, line2 - set of constraints for the two boundaries, type vector<Constraint>
 * currentSegGoals - segment goals for the current segment goals, type SegGoals
 * x_A, y_A - position from which the rddf's are to be sorted, double
 * laneWidth - laneWidth defined by the sets of constraints (that define the corridor), double
 * rddfx, rddfy - set of locations of the center points of the rddf's defined (updated), type vector<double>
 * rddfr - set of radii of the rddf's defined (updated), type vector<double>
 * returns:
 * rddfx, rddfy - updated set of locations of the center points of the rddf's defined, type vector<double>
 * rddfr - updated set of radii of the rddf's defined, type vector<double>  
 */
bool convRDDF( vector<Constraint> line1, vector<Constraint> line2, SegGoals currentSegGoals, double x_A, double y_A, double laneWidth, vector<double>& rddfx, vector<double>& rddfy, vector<double>& rddfr);

/*! A function used in convRDDF only
 * Not sure about the function, but presumably finds the closest point (orthogonal projection) of pt to lnseg
 */
point closept( double lnseg[4], double pt[2] );

/*! A function used in convRDDF only
 * Not sure about the function, but presumably finds the center of the linesegment defined by pt1 and pt2
 */
point midpt( double pt1[2], double pt2[2] );

/*! A function used in convRDDF only
 * Not sure about the function, but presumably calculates the radius of a circle centered at mpt and the two pts pt1 and pt2 (used to find the mpt)
 */
double radius( double mpt[2], double pt1[2], double pt2[2] );

/*! Generate a map object from the rndf
 * This is a copy of the function in Map
 * The inputs are:
 * m_rndf - the pointer of the rndf object, type RNDF*. The rndf object is received from gloNavMap
 * map - an initialized map object is updated in the function, type Mapper::Map
 * returns:
 * map - updated map
 */
void generateMapFromRNDF(RNDF*, Mapper::Map&);


#endif // TRAFFICUTILS
