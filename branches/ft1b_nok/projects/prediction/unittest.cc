#include "ParticleFilter.h"
#include <iostream>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <fstream>
#include <sstream> //For stringstream class

using namespace std;


//For propagating the civic state.
void propagateCivic(Vector& civic, double elapsed_time,  double accel_noise, double steer_noise)
{
  double a, K, d, delta_theta;

  //Add some accel noise.
  a = civic.getElement(4) + accel_noise;

  //Add some steering noise.
  K = civic.getElement(6); // + steer_noise;

  if(fabs(K) < 1E-6)
    {
      K = 1E-6;
    }
  
  //d = .5at^2 + vt
  d = .5*a*elapsed_time*elapsed_time + civic.getElement(5)*elapsed_time;
  
  delta_theta = K*d;
  
  
  //Now create a new particle and set the appropriate values.
  
  //x
  civic.setElement(1, civic.getElement(1) + (1/K)*(sin(civic.getElement(3) + delta_theta) - sin(civic.getElement(3))));
  
  //y
  civic.setElement(2, civic.getElement(2) - (1/K)*(cos(civic.getElement(3) + delta_theta) - cos(civic.getElement(3))));
  
  //theta2 = theta1 + delta_theta
  civic.setElement(3, civic.getElement(3) + delta_theta);
  
  //a2 = a1
  civic.setElement(4, a);
  
  //v2 = v1 + at
  civic.setElement(5, civic.getElement(5) + a*elapsed_time);
  
  //phi2 = phi1
  civic.setElement(6, K);
  
}



int main(void)
{
  ofstream civicfile("unittest_civic.dat");
  ofstream posteriors("unittest_complete_posteriors.dat");

  ofstream priors("unittest_complete_priors.dat");

  //We create a representation of my (KICKASS!) 2003 Honda Civic to serve as our "test vehicle" for this simulation.
  //We assume it drives in a straight line from the origin at a 45 degree bearing and a constant speed of 13.5 m/s (about 30 mph).

  Vector civic(6);

  civic.setElement(1,0);  //We start at the origin
  civic.setElement(2,0);
  civic.setElement(3,M_PI/4);  //45 degree heading
  civic.setElement(4, 0);  //No accel
  civic.setElement(5, 13.5); //13.5 m/s
  civic.setElement(6, 0);  //Zero curvature (straight line driving)


  for(int c = 1; c <= 6; c++)
    {
      civicfile <<civic.getElement(c)<<" ";
    }
  civicfile <<endl;
  

  //Now we generate a particle set to represent the vehicle.  Assume for the sake of argument that this is the first measurment that we've made of the vehicle, so that we know about where it is (up to uncertainty measurments in the ladar), but can only guess about its other characteristics.

  //For random number generation.
  gsl_rng* rand;
  gsl_rng_env_setup();
  const gsl_rng_type* T;
  T = gsl_rng_default;
  rand = gsl_rng_alloc(T);

  double dist_uncertainty = .05;  //Standard deviation in the ladar units, in m.
  double accel_uncertainty = .05;  //Process noise standard deviation for acceleration, in m/s^2
  double steer_uncertainty = .017;  //Process noise standard deviation for steering angle, in radians (about 1 degree)
  double length_uncertainty = .01;
  double width_uncertainty = .01;


  //Begin by generating a set of "numparts" particles.

  const int numparts = 25000;

  Vector* particles = new Vector[numparts];

  Vector new_particle(7);

  for(int i = 0; i < numparts; i++)
    {
      //Set x, y to be zero with a small measurement uncertainty.
      new_particle.setElement(1, gsl_ran_gaussian(rand, dist_uncertainty));
      new_particle.setElement(2, gsl_ran_gaussian(rand, dist_uncertainty));

      //We don't know anything about steering angle yet, so just assume that its a uniform distribution to represent complete uncertainty.
      new_particle.setElement(3, gsl_ran_flat(rand, 0, 2*M_PI));

      //We also don't know anything about the acceleration or velocity of the vehicle, so we use uniform distributions for these numbers as well.
      new_particle.setElement(4, gsl_ran_flat(rand, -2.5, 2.5)); //Accel, in m/s^2
      new_particle.setElement(5, gsl_ran_flat(rand, -6.7, 20.15));  //Velocity, about -15 to 45 mph
      new_particle.setElement(6, gsl_ran_flat(rand, (double) -1/6.0, (double) 1/6.0));  //Curvature, corresponds to turning radii between 6m (about 20 ft.) and \infinity (straight-line driving)
      new_particle.setElement(7, 1);  //Since we have no information yet about the car, we treat all particles as having the same probability.

      particles[i] = new_particle;
    }

  //Output the initial PDF.

  ofstream init("unittest_initial_pdf.dat");
  for(int v = 0; v < numparts; v++)
    {
      //Print out the contents of each particle in the pdf.
      for(int c = 1; c<= 6; c++)
	{
	  init <<particles[v].getElement(c)<<" ";
	}
      init<<endl;
    }

  init.close();

  //Ok, now initialize the filter.

  ParticleFilter party(particles, numparts, accel_uncertainty, steer_uncertainty, length_uncertainty, width_uncertainty, dist_uncertainty);		      
  
  cout<<"Starting simulation."<<endl;

  //We run the simulation for 5 seconds, at time intervals of .1 seconds.

  double* measurements = new double[2];

  double steer_noise, accel_noise;

  for(int i = 1; i <= 10; i++)
    {
      cout<<"Starting loop "<<i<<endl;

      steer_noise = gsl_ran_gaussian(rand, steer_uncertainty);
      accel_noise = gsl_ran_gaussian(rand, accel_uncertainty);

      propagateCivic(civic, .1, accel_noise, steer_noise);

      measurements[0] = civic.getElement(1);
      measurements[1] = civic.getElement(2);

      Vector* pred = party.propagatePDF(.1);

      party.updatePDF(.1, measurements);

      Vector* file = party.getPDF();

      //filename formatting shit.
      stringstream ss; //So we can convert numbers to strings.
      ss <<"unittest_posterior"<<i<<".dat";
      string intermediate;
      ss >> intermediate;
      char post [25];
      strcpy(post, intermediate.c_str()); //To copy the string intermediate into filename as a char*;
      
      //more filename formatting shit.
      stringstream ss2; //So we can convert numbers to strings.
      ss2 <<"unittest_prior"<<i<<".dat";
      string intermediate2;
      ss >> intermediate2;
      char prior [25];
      strcpy(prior, intermediate2.c_str()); //To copy the string intermediate into filename as a char*;


      ofstream outfile (post);
      ofstream outfile2(prior);

      outfile <<"#Starting loop "<<i<<"."<<endl<<endl;
      cout<<"Creating posterior pdf file "<<intermediate<<endl<<endl;

      for(int v = 0; v < numparts; v++)
	{
	  //Print out the contents of each particle in the pdf.
	  for(int c = 1; c<= 6; c++)
	    {
	      outfile <<file[v].getElement(c)<<" ";
	      posteriors <<file[v].getElement(c) <<" ";
	      outfile2 <<pred[v].getElement(c) <<" ";
	      priors<<pred[v].getElement(c)<<" ";
	    }
	  outfile<<endl;
	  outfile2<<endl;
	  posteriors<<endl;
	  priors<<endl;
	}

      for(int c = 1; c <= 6; c++)
	{
	  civicfile <<civic.getElement(c)<<" ";
	}
      civicfile <<endl;

      delete [] file;
      delete [] pred;

      outfile.close();
      outfile2.close();

    }

  ofstream predfile("unittest_predictions.dat");

  for (int i = 1; i <= 3; i++)
    {
      steer_noise = gsl_ran_gaussian(rand, steer_uncertainty);
      accel_noise = gsl_ran_gaussian(rand, accel_uncertainty);

      propagateCivic(civic, 1, accel_noise, steer_noise);

      for(int c = 1; c <= 6; c++)
	{
	  civicfile <<civic.getElement(c)<<" ";
	}
      civicfile <<endl;
    }

  cout<<"Propagation particle set forward in time to generate predictions."<<endl<<endl;

  Vector** particlesets = party.generatePrediction(10, 1);

  for(int i = 0; i < 3; i++)
    {
      cout<<"Generating prediction for timestep "<<i+1<<endl;
      for(int p = 0; p < numparts; p++)
	{
	  for(int c = 1; c<= 6; c++)
	    {
	      predfile <<particlesets[i][p].getElement(c)<<" ";
	    }
	  predfile<<endl;
	}
    }
  posteriors.close();
  priors.close();
  predfile.close();
}
