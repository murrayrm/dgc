#ifndef __POLYNOMIAL_H__
#define __POLYNOMIAL_H__

//A class representing standard mathematical polynomials.
//Author: David Rosen

class Polynomial
{
protected:
  
  int degree;
  double* coeffs;
  
public:
  
  //Constructors and assignment operator


  /**Creates a constant polynomial, initialized to 0. */
  Polynomial();
  
  /**Creates a polynomial of degree "deg" with coefficients from the "coefficients" array.  The coefficients are paired one-to-one with the powers of the independent such that increasing index in the array corresponds to increasing powers of x, i.e., coefficients[0] is the constant term, coefficients[1] is the coefficient for x, coefficients[2] is the coefficient for x^2, etc.  Note that this means that coefficients must be of length ("deg" + 1). */
  Polynomial(const int deg, const double* coefficients);

  /**Copy constructor. */
  Polynomial(const Polynomial& poly);

  /**Creates a constant polynomial with value "val". */
  Polynomial(const double val);

  /**Assignment operator. */
  Polynomial& operator = (const Polynomial poly);


  //Accessor methods


  /**Returns the degree of (*this). */
  int getDegree() const;

  /**Returns the coefficient of the power of x of degree "deg". */
  double getCoefficient(const int deg) const;
  


  //Mathematical operators  
  
 
  
  /**Standard polynomial addition. */
  Polynomial  operator + (const Polynomial& poly) const;

  /**Standard polynomial addition. */
  Polynomial operator + (const double val) const;
  
  /**Standard polynomial subtraction, returns (*this) - poly. */
  Polynomial operator - (const Polynomial& poly) const;

  /**Standard polynomial subtraction, returns (*this) - val. */
  Polynomial operator - (const double val) const;
  
  /**Standard polynomial multiplication. */
  Polynomial operator * (const Polynomial& poly) const;

  /**Standard polynomial multiplication. */
  Polynomial scalar (const double val) const;
  
  /**Evaluates the polynomial for the given value of x. */
  double operator () (const double x) const;

  /**Calculates the derivative of the polynomial (*this). */
  Polynomial derivative() const;
  
  /**Return the quotient (*this)/(x - root), where "root" is a root of (*this).  NOTE:  SYNTHETIC DIVISION ASSUMES THAT "root" IS A ROOT OF THE POLYNOMIAL, i.e., that (x - root) actually IS a factor of (*this). */
  Polynomial SyntheticDivision(const double root) const;


  /**Returns a pointer to a double array containing the real roots of (*this).  NOTE: the returned double array is filled in BEGINNING WITH THE LAST (greatest index) SLOT; in the event that the number of roots found is less than thedegree of the polynomial, begin collecting roots from the END of the array. */
  double* findRoots(const double guess, const double tolerance, const double step, const int maxLoops, int& found) const;


  //Logical operators and tests


 /**Equality operator. */
  bool operator == (const Polynomial& poly) const;

  /**Inequality operator. */
  bool operator != (const Polynomial& poly) const;


  //Print method


  /**Prints out the polynomial as an actual polynomial, i.e., in the forM "ax^n + bx^(n-1) + ...". */
  void print() const;

  /**Standard destructor. */
  ~Polynomial();
};

#endif //__POLYNOMIAL_H__


