function im2 = imcomplement(im)
%IMCOMPLEMENT Complement image.
%   IM2 = IMCOMPLEMENT(IM) computes the complement of the image IM.  IM
%   can be a binary, intensity, or truecolor image.  IM2 has the same class and
%   size as IM.
%
%   In the complement of a binary image, black becomes white and white becomes
%   black.  For example, the complement of this binary image, true(3), is
%   false(3).  In the case of a grayscale or truecolor image, dark areas
%   become lighter and light areas become darker.
%
%   Note
%   ----
%   If IM is double or single, you can use the expression 1-IM instead of this
%   function.  If IM is logical, you can use the expression ~IM instead of
%   this function.
%
%   Example
%   -------
%       I = imread('glass.png');
%       J = imcomplement(I);
%       figure, imshow(I), figure, imshow(J)
%
%   See also IMABSDIFF, IMADD, IMDIVIDE, IMLINCOMB, IMMULTIPLY, IMSUBTRACT. 

%   Copyright 1993-2004 The MathWorks, Inc.
%   $Revision: 1.12.4.5 $  $Date: 2004/08/10 01:40:05 $

imClass = class(im);
switch imClass
 case 'logical'
  im2 = ~im;
    
 case {'double','single'}
  im2 = 1 - im;
  
 case {'uint8','uint16','int16'}
  lut = intmax(imClass):-1:intmin(imClass);
  im2 = intlut(im,lut);

 case {'uint32'}
  im2 = intmax('uint32') - im;
  
 case {'int32','int8'}
  im2 = imlincomb(-1,im,double(intmax(imClass)),'double');
  im2 = imlincomb(1,im2,double(intmin(imClass)),imClass);
 otherwise
  eid = 'Images:imcomplement:invalidType';
  error(eid, '%s', 'Invalid input image class.')
end
