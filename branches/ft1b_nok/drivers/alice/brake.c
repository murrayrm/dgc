//Modified 2-3-05 by David Rosen for use with SDS

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "GlobalConstants.h"
#include "brake.h"

#ifdef BRAKE_SERIAL
#include <sstream>
#include <SerialStream.h>
using namespace LibSerial;
using namespace std;
#endif //BRAKE_

#ifdef BRAKE_SDS
#include "SDSPort.h"
int brake_serial_port = -1;     /* not a valid number */
const char brake_read[2] = {DGC_ALICE_BRAKE_READ, DGC_ALICE_BRAKE_EOT};
const char brake_command = DGC_ALICE_BRAKE_COMMAND;
const char brake_pressure_read[2] = {DGC_ALICE_BRAKE_PRESSURE_READ, DGC_ALICE_BRAKE_EOT};
//char brake_pressure_string[3];
#endif //BRAKE_SDS

#ifdef BRAKE_SERIAL
static SerialStream serial_port;
char brake_read = DGC_ALICE_BRAKE_READ;
char brake_command = DGC_ALICE_BRAKE_COMMAND;
//char brake_pressure_string[3];
char brake_pressure_read = DGC_ALICE_BRAKE_PRESSURE_READ;
#endif //BRAKE_serial

char brake_position = DGC_ALICE_BRAKE_ON;
//char brake_position_string[4] = "C15";  //initialize with brake on full

double brake_got_position;
double temp;




#ifdef BRAKE_SDS
SDSPort* brakeport;
int portCreated = 0;
extern char simulator_IP[];
#endif //BRAKE_SDS

#include <iostream>
using namespace std;


/**************************************************************************************!
 * brake_open (int port)
 * 
 * Author:      Will Coulter
 *
 * Description: Opens and configures the serial port for brake use.  Also
 *              applies full brakes.
 *
 * Arguments:   port - the number of the serial port we wish to use.
 *
 * Returns:     FALSE - we could not open and adequately configure serial port
 *              TRUE - we are a go
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_open (int port)
{
    /* no validation testing, assume serial port code will die gracefully */
#ifdef BRAKE_SERIAL  
	ostringstream portname;
	portname << "/dev/ttyS" << port;

	serial_port.Open(portname.str());
	if ( ! serial_port.good() )
	{
		cerr << "brake_open() error: Could not open " << portname.str() << endl ;
		return FALSE;
	}

	serial_port.clear();

	serial_port.SetBaudRate( SerialStreamBuf::BAUD_9600 ) ;
	if ( ! serial_port.good() )
	{
		cerr << "brake_open() error: Could not set the baud rate." << endl ;
		return FALSE;
	}
	serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
	if ( ! serial_port.good() )
	{
		cerr << "brake_open error: Could not disable the parity." << endl ;
		return FALSE;
	}
	serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
	if ( ! serial_port.good() )
	{
		cerr << "brake_open error: Could not turn off hardware flow control." << endl;
		return FALSE;
	}
	serial_port.SetNumOfStopBits( 2 ) ;
	if ( ! serial_port.good() )
	{
		cerr << "brake_open error: Could not set the number of stop bits."  << endl;
		return FALSE;
	}
	serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8) ;
	if ( ! serial_port.good() )
	{
		cerr << "brake_open() error: Could not set the char size." << endl ;
		return FALSE;
	}
#endif //BRAKE_SERIAL
  //  cout<<"Starting brake_open()"<<endl;

#ifdef BRAKE_SDS
     if(portCreated == 0)
     {
      brakeport = new SDSPort("192.168.0.60", port);
      if(brakeport->isOpen() == 1)
	{
	  //	  cout<<"Brake connection established"<<endl;
	  portCreated = 1;
	}
     }
     else
     {
       while(brakeport->isOpen() != 1)
       {
        brakeport->openPort();
       }
     }
#endif //BRAKE_SDS

    /* Try and make sure the vehicle can brake (can use the serial port) */
    if (brake_pause() != TRUE) {
      cout<<"Brake pause() failed in serial_open() command"<<endl;
        return FALSE;
    }
    if(brake_getposition() == -1)
      {
	return FALSE;
      }

    cout<<"Brake open executed correctly"<<endl;
    return TRUE;
}

/**************************************************
simulator_brake_open called instead of brake_open when running in gazebo(simulation mode)
*/

void simulator_brake_open(int port)
{
#ifdef BRAKE_SDS
  brake_serial_port = port;
  brakeport = new SDSPort(simulator_IP, port);
  portCreated = 1;
#endif //BRAKE_SDS
  //  cout<<"brake read in open" << brake_read << endl;
}



/*!
 * brake_close (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Sends a command to apply full brakes (not checking that they
 *              are) and then closes the serial port.
 *
 * Arguments:   None.
 *
 * Returns:     FALSE - Error closing the serial port.
 *              TRUE - Serial port has been closed.
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_close(void)
{
#ifdef BRAKE_SERIAL
	serial_port.Close();
#endif //BRAKE_serial

#ifdef BRAKE_SDS
    if(brakeport->closePort() == 0)
    {
      cout<<"brake close() executed correctly"<<endl;
     return TRUE;
    }
    cout<<"Brake close failed"<<endl;
    return FALSE;
#endif //BRAKE_SDS
    return TRUE;
}

/*!
 * brake_setposition (double position)
 * 
 * Author:      Will Coulter
 * Revision:    Jeff Lamb (JCL) 20 Jan 05
 *              Changed to string based communication
 *
 * Description: Sends the microcontroller the desired position of the braking
 *              actuator.  
 *
 * Arguments:   position - Desired actuator position; *must* be a value in
 *              [DGC_ALICE_BRAKE_MINPOS,DGC_ALICE_BRAKE_MAXPOS]
 *
 * Returns:     FALSE - desired position is invalid or serial error
 *              TRUE - position was sent over serial channel
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_setposition(double position)
{
  //  cout<<"Starting brake_setposition"<<endl;
  
  char brake_position_string[8];
  
  // if command is out of range, return error
  if (position > 1 || position < 0) return FALSE;

  // brake command will be in the form Cxx where xx is a number from 00 to 15
  position = position * DGC_ALICE_BRAKE_MAXPOS;
  brake_position_string[0] = brake_command;
  if(position < 10) {
    brake_position_string[1] = '0';
    sprintf(brake_position_string+2, "%d\n", (int) position);
  }
  else {
    sprintf(brake_position_string+1, "%d\n", (int) position);
  }
  
#ifdef BRAKE_SERIAL
  serial_port.clear();
  serial_port.write((char*) brake_position_string, strlen(brake_position_string));
  if (!serial_port.good()) return FALSE;
  
#endif //BRAKE_serial
  
#ifdef BRAKE_SDS
    if(brakeport->write((char*) brake_position_string) == -1)
      {
	//	cout<<"Brake command write failed in brake setposition"<<endl;
	return FALSE;
      }
#endif //BRAKE_sds
    //    cout << "BRAKE: returning TRUE from setposition" << endl;
    return TRUE;

}

/*!
 * brake_getposition (void)
 * 
 * Author:      Will Coulter
 * Revision:    Jeff Lamb (JCL) 20 Jan 2005
 *              Modified to receive position as a 2 character string
 *
 * Description: Returns the current position of the braking actuator.
 *
 * Arguments:   None.
 *
 * Returns:     If the integer returned is in the allowable range, it is
 *              returned.  Otherwise, a negative number is returned.
 *
 * Known Bugs:  None.
 **************************************************************************************/
double brake_getposition(void)
{
  char brake_position_string[4];
  brake_position_string[3] = '\0';
  //cout<<"Starting brake getposition"<<endl;

#ifdef BRAKE_SERIAL
  // Try sending command to get position  --  send R to get position
  //  cout << "BRAKE: executing brake_getposition()" << endl;
  serial_port.clear();
  serial_port.write(&brake_read, 1);
  //cout << "BRAKE: write succeeding in getposition" << endl;
  if (!serial_port.good()) {
    // cout << "BRAKE: write failed in getposition" << endl;
    return -1;
  }
#endif //BRAKE_serial
  
#ifdef BRAKE_SDS
  brakeport->clean();
  if(brakeport->write((char*) brake_read, 2) == -1)
    {
      //      cout<<"brake write in brake_getposition failed"<<endl;
      return -1;
    }
#endif //BRAKE_SDS
  
  //printf("brake read: %s", brake_read);
  
  

    /* Try actually retrieving position */
#ifdef BRAKE_SERIAL    
  usleep(30000);
  //  cout << "BRAKE: executing read in getposition" << endl;
  serial_port.clear();
  serial_port.read((char *) brake_position_string, 3);
  //cout << "BRAKE: read succeeded in getposition" << endl;
  if(!serial_port.good()) {
    //  cout << "BRAKE: read failed in getposition" << endl;
    return -1;
  }
  usleep(30000);
#endif //BRAKE_serial

#ifdef BRAKE_SDS
  usleep(30000);
  if(brakeport->read(3, (char*) brake_position_string, 70000) == -1)
    {
        /* The microcontroller or computer may be busy, so wait and try again.
           Yes, this is the poor man's read-with-timeout solution */
        usleep(DGC_ALICE_BRAKE_MAXDELAY);
  	if(brakeport->read(3, (char*) brake_position_string, 70000) == -1)
	  {
	    //cout<<"Brake read in brake getposition failed"<<endl;
	  /* now we have really failed, so return */
          //printf("brake string: %s", brake_position_string);  
	  return -1;
	  }
    }
#endif //BRAKE_SDS

  // check to see if returned value is within acceptable limits
  // if not, return errorr
   
  if (
      atoi(brake_position_string) > DGC_ALICE_BRAKE_MAXPOS ||
      atoi(brake_position_string) < DGC_ALICE_BRAKE_MINPOS
      )
    {
      return -1;
    }
  //  cout << "brake position string: "<< brake_position_string << endl;
    temp = atoi(brake_position_string);
    brake_got_position = temp / DGC_ALICE_BRAKE_MAXPOS;
    //    cout<<"brake getposition executed correctly with value "<< brake_got_position <<endl;
        return (double) brake_got_position;
}

/*!
 * brake_getpressure (void)
 * 
 * Author:      David Rosen
 * 
 * Description: Returns the current pressure in the brake line.
 *
 * Arguments:   None.
 *
 * Returns:     On a successful read, returns a positive integer representing 
 *              the pressure read by a pressure transducer connected to the 
 *              brake line; possible values are 0-255, although it should normal                
 *              ly be in the 15-200 range.  Returns -1 on an error.
 *  
 * 
 * Tully  2/27/05
 * Changes:     I have adjusted this function to work on a 0-1 scale as that is what we
 *              the rest of the vehicle is standardized to.  
 *              The normal range is now .058 to .78 
 *
 * Known Bugs:  None.
 **************************************************************************************/
double brake_getpressure()
{
  char brake_pressure_string[4];
    
  //Send request for brake pressure
#ifdef BRAKE_SERIAL
  //serial_clean_buffer(brake_serial_port);
  serial_port.clear();
  serial_port.write(&brake_pressure_read, 1);
  if (!serial_port.good()) return -1;
  
  usleep(30000);
  serial_port.clear();
  serial_port.read((char *) brake_pressure_string, 4);
  if (!serial_port.good()) return -1;
  usleep(30000);
  //cout << "brake pressure string: " << brake_pressure_string << endl;

#endif //BRAKE_serial

#ifdef BRAKE_SDS
	int bytesRead;
  brakeport->clean();
  if(!brakeport->write(brake_pressure_read, 2)) //Error in write
    {
      return -1;
    }
  usleep(30000);
  if((bytesRead = brakeport->read(3, brake_pressure_string, (int)(1E6))) < 3) 
    //We didn't get all of the transmission, so clean out the buffer of any leftover junk and return an error.
    {
      brakeport->clean();
      return -1;
    }
#endif //BRAKE_sds

  // cout<<"Brake read returned "<<brake_pressure_string<<endl;

  //If we've made it this far, we have the pressure data, so extract from the buffer and return
  temp = atoi(brake_pressure_string);
  double pressure = ((double) temp) /255;
  //  printf("%f is the pressure", pressure);
  if (pressure > 1 || pressure < 0) 
    return -1;

  return pressure;


}


/*!
 * brake_pause (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Commands full brake actuation.  Does not check to see that the
 *              command was executed.  This is just a wrapper around
 *              brake_setposition
 *
 * Arguments:   None.
 *
 * Returns:     FALSE - Serial error of some kind.
 *              TRUE - Command was sent over the serial channel.
 *
 * Known Bugs:  None.
 **************************************************************************************/
int brake_pause(void)
{
  return brake_setposition(1);
}

/*!
 * brake_disable (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Wrapper for brake_pause (void).
 **************************************************************************************/
int brake_disable(void)
{
    return brake_setposition(1);
}

/*!
 * brake_off (void)
 * 
 * Author:      Will Coulter
 *
 * Description: Turns the brake off, i.e. let's the vehicle roll freely.  Does
 *              not check to see that the command was executed.  Yet another
 *              wrapper for brake_setposition
 **************************************************************************************/
int brake_off(void)
{
    return brake_setposition(0);
}




