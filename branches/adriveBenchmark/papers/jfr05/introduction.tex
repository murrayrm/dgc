% Master File: JFR05.tex
\section{Introduction}

Team Caltech was formed in April of 2002 with the goal of designing a
vehicle that could compete in the 2004 DARPA Grand Challenge.  Our
2004 vehicle, Bob, completed the qualification course and traveled
approximately 1.3 miles of the 142-mile 2004 course.
In 2004-05, Team Caltech developed a new vehicle to participate in the
2005 DARPA Grand Challenge.  Through a Caltech course in
multi-disciplinary project design, over 50 undergraduates
participated in conceiving, designing, implementing
and testing our new vehicle, named ``Alice'' (Figure~\ref{fig:alice}).
\begin{figure}
  \centerline{\includegraphics[width=0.8\textwidth]{alice-gce.eps}}
  \caption{Caltech's 2005 DARPA Grand Challenge Entry, Alice.}
  \label{fig:alice}
\end{figure}
The team consisted of a broad range of students from different
disciplines and at different skill levels, working together to create
a sophisticated engineering system. The final race team completed the
implementation and optimization of the system over the summer as part
of the Caltech Summer Undergraduate Research Fellowship (SURF)
program.

Alice's design built on many standard techniques in robotics and
control, including state estimation using Kalman filters, sensor-based
terrain estimation and fusion, optimization-based planning through a
``map'' of the terrain, and feedback control at multiple levels of
abstraction.  A novel aspect of the design compared with many robots
built prior to the grand challenge was the high-speed nature
of the system: Alice was designed to travel through unstructured
environments at speeds of up to 15 m/s (35 mph) using multiple cameras
and LADARs across a network of high performance computers.  The raw
data rates for Alice approached 500 Mb/s in its race configuration and
plans were computed at up to 10 Hz.  This required careful attention
to data flow paths and processing distribution, as well as the use of
a highly networked control architecture.  In addition, Alice was
designed to operate in the presence of failures of the sensing and
planning systems, allowing a high level of fault tolerance.  Finally,
Alice was designed to allow efficient testing, including the use of a
street legal platform, rapid transition between manual and autonomous
driving and detailed logging, visualization and playback capabilities.

This paper describes the overall system design for Alice and provides
an analysis of the systems performance in desert testing, the national
qualification event, and the 2005 Grand Challenge race.  We focus
attention on three aspects of the system that were particularly
important to the system's performance: high-speed sensor
fusion, real-time trajectory generation and tracking, and supervisory
control.  Data and measurements are provided for a variety of subsystems
to demonstrate the capabilities of the component functions and the
overall system.

Alice's design built on many advances in robotics and control over the
past several decades.  The use of optimization-based techniques for
real-time trajectory generation built on our previous experience in
receding horizon control for motion control systems~\cite{Mil03-phd, 
mur+03-sec} and extended that work to include efficient methods for
cost evaluation along a terrain with sometimes sparse
data~\cite{DK05-ms}.  Our sensor fusion architecture and the use of
speed maps built on work at JPL by Goldberg and
Matthies~\cite{GMM02-ieeeac} and we benefited greatly from the work
of Dickmanns~\cite{Dic04-aim}.  The supervisory control architecture
that we implemented also relied heavily on concepts developed at JPL
by Rasmussen et al.~\cite{Ras01-ieeeac}.  Finally, the design
approaches of other teams, especially those of CMU~\cite{Urm04-thesisdraft, 
Urm+04-tr}, helped shape the overall design approach.

Section~\ref{sec:systemarch} describes our system architecture, from
the vehicle and associated hardware to the software design.
Section~\ref{sec:control} details the specifics of the vehicle
actuation and trajectory-following controller.  Our mapping and
planning algorithms are explained in Sections~\ref{sec:terrain}
and~\ref{sec:planning}, and our higher-level control and contingency
management is described in Section~\ref{sec:supercon}.  Experimental
results that illustrate the performance of our system are presented in
Section~\ref{sec:results}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "jfr05"
%%% End: 
