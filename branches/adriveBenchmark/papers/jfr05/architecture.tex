% Master File: jfr05.tex
\section{System Architecture}
\label{sec:systemarch}

Caltech's 2004 entry in the DARPA Grand Challenge utilized an arbiter based
planning architecture (similar to that in~\cite{Ros98-fsr}) in which
each terrain sensor ``voted'' on a set of 
steering angles and speeds, based on the goodness determined by those
sensors.  These votes were then combined by an arbiter, resulting in a
command-level fusion.  This approach had advantages in terms of development
(each voter could be developed and tested independently), but it was decided
that this approach would not be able to handle complex situations involving
difficult combinations of obstacles and terrain.  Hence a more
sophisticated architecture was developed for the 2005 race, based on
optimization-based planning.  In addition, a high level system specification
was used to guide the operation of each component in the system.

\subsection{System Specification}
\label{sec-systemspec}

Team Caltech's strategy for the race was embedded in its overall
system specification, which described the performance characteristics
for Alice and the team's operations. This system specification was
used to drive the specifications for individual components.  The
final system specification contained the following requirements:
\begin{enumerate}
  \item[S1)] 175 mile (282 km) range, 10 hours driving, 36 hours
  elapsed (with overnight shutdown and restart).

  \item[S2)] Traverse 15 cm high (or deep) obstacles at 7 m/s,
  30 cm obstacles at 1 m/s, 50 cm deep water (slowly), 30 cm deep sand
  and up to 15 deg slopes.  Detect and avoid situations that are worse than
  this.

  \item[S3)] Operate in dusty conditions, dawn to dusk with up to 2
  sensor failures. \label{spec-robust}

  \item[S4)] Average speed versus terrain type:
  \begin{center}\small
    \begin{tabular}{|l|cc|ccc|cc|c|}
      \hline
      Terrain type & 
        \multicolumn{2}{|c|}{Distance (\%)} & 
        \multicolumn{3}{|c|}{Speed (mph)} & 
        \multicolumn{2}{|c|}{Expected} & 
        Time (hr) \\
      & Min & Max & Min & Max & Exp & mi & \% & \\
      \hline
      Paved road & 1 & 10 & 20 & 40 & 30 & 18 & 10\% & 0.6 \\
      Dirt road & 40 & 60 & 10 & 40 & 25 & 132 & 75\% & 5.3 \\
      Trails & 20 & 30 & 5 & 15 & 10 & 18 & 10\% & 1.8 \\
      Off-road & 5 & 20 & 1 & 5 & 5 & 5 & 3\% & 1 \\
      Special & n/a & n/a & 2 & 2 & 2 & 2 & 1\% & 1 \\
      \hline
      Total & & & 1 & 40 & 25 & 175 & 100\% & 9.7 \\
      \hline
    \end{tabular}
  \end{center}  

  \item[S5)] Safe operation that avoids irreparable damage, with variable
  safety factor.

  \item[S6)] Safety driver w/ ability to immediately regain control of
  vehicle; 20 mph crash w/out injury.
  
  \item[S7)] Commercially available hardware and software; no government
  supported labor.

  \item[S8)] \$120K total equipment budget (excluding donations); labor
  based on student enrollment in CS/EE/ME
  75abc (multi-disciplinary systems engineering course) and 24
  full-time SURF students.

  \item[S9)] Rapid development and testing: street capable, 15 minute/2
  person setup. \label{spec-rapid}
\end{enumerate}

The speed versus terrain type specification (S4) was updated during the course
of development.  Expected mileages by terrain type were updated from analyses
of the 2004 Grand Challenge course, and expected speeds were selected to find 
a ``sweet spot'' that balances the trade-off between chance of completing the 
course (whose trend generally decreases with increased speed, and favors 
lower speeds) and minimizing completion time (which favors higher speeds).

One of the most important specifications was the ability to do 
rapid development and testing (S9).  This was chosen to 
to take advantage of being within a few hours
drive of desert testing areas: our goal was to test as much as
possible in race-like conditions.  Our vehicle from the 2004 Grand
Challenge, Bob, had to be towed by trailer to and from test sites.  We
had also removed Bob's steering wheel, which meant that he had to be
driven using a joystick.  This added unnecessary complication and
effort to the testing process.  With Alice, the transition from a
human driving to autonomous testing only required the operator to
insert a single pin to engage the brake mechanism and then flip a few
switches to turn on the other actuators.  This meant that we could
transition from human driving to an autonomous test in only 5 minutes.
Alice also supports a complement of four connected but independent interior 
workstations for development and testing, a vast improvement over Bob's design.

\subsection{Vehicle Selection}

To help achieve ease of development, a Ford E350 van 
chassis was obtained and modified for off-road use by Sportsmobile West of
Fresno, CA.  A diesel engine,
augmented by a 46-gallon fuel tank, was selected since it was well
suited to the conventional operating envelope of generally slow
speeds and long idle times during periods of debugging.

Sportsmobile's conversion is primarily intended for a type of driving known
as rock-crawling: low-speed operation over very rough terrain.  A
four-wheel-drive system distributes power through a geared transfer case
(manufactured by Advanced Adapters, Inc.) and the suspension rests in deep
compression, allowing the unsprung components to drop very quickly over
negative terrain discontinuities (see Figure~\ref{fig:alice-vehicle}(a)).  
\begin{figure}
  \begin{tabular}{ccc}
    \includegraphics[width=0.45\textwidth]{alice-frontdiff.eps}
    &\quad&
    \includegraphics[width=0.45\textwidth]{alice-backwork.eps} \\
    (a) && (b)
  \end{tabular}
  \caption{Vehicle features: (a) front differential, suspension, and
    front bumper/LADAR mount and (b) the rear workstation area with
    server array visible (as viewed through the rear doors).}
  \label{fig:alice-vehicle}
\end{figure}
Another key suspension feature is the use
of a reverse shackle design in the implementation of the front leaf spring
suspension.  As opposed to traditional designs, the reverse shackle
method places the extensible end of the leaf spring toward the rear of the
vehicle.  This provides better tracking at speed.  The high-knuckle front axle
design decreases minimum turning circle by 6\% to 45 feet in diameter.

The vehicle was purchased as a ``stripped chassis,'' which means
that the vehicle was bare behind the front seats.  
This allowed a custom interior design that included a central enclosure for 
the server array and four racing seats, two of which replaced the stock 
Ford captain's chairs in the front of the cabin.  A removable table was 
placed in front of the two rear seats, and five terminals were
positioned throughout the 
vehicle: one for each seat and one at the side door, accessible from outside the
vehicle.
Figure~\ref{fig:alice-vehicle}(b) shows a view of the interior of Alice 
from the rear. Thus equipped, the vehicle
is capable of supporting three developers and safety driver 
while mobile, or four developers while stationary.  

The electrical system consists of a 3 kilowatt generator mounted to the 
rear of the vehicle, producing 120 VAC.  This power is directed to
two 1500 watt 
inverter/chargers.  Power is passed through these inverters directly to the
loads without loss when the generator is running, with the
remaining power being diverted to charge the auxiliary battery bank.  The
battery bank consists of four 12 volt marine gel cell batteries rated at 210
amp-hours each, positioned beneath the floor at the rear of the vehicle to
keep the center of gravity low.  Power from the charged gel cell batteries
is diverted back through the inverters to power AC loads when the generator
is not functioning, or to the 12 volt systems powering the LADARs, IMU, GPS
and actuators.  The power system was tested and verified to operate in
high-shock, high-vibration environments for up to 22 hours between
generator refuelings.

\subsection{Computing and Networking}

One of the initial specifications for Team Caltech's second vehicle was to
be able to survive the failure of one or more computers.  The design
of the vehicle computing and networking systems was developed based on this
specification, even though this functionality was not implemented during the
race (due to shortness of time and the strong reliability of the hardware in
testing).  

The computing platform consisted of 6 Dell PowerEdge 750 servers with 3 GHz,
Pentium 4 processors and a single IBM eServer 326 with dual 2.2 GHz dual-core
AMD 64 processors.  All machines were configured to run
Linux; the Gentoo distribution~\cite{Ver+05-gentoo} was selected based on
performance testing early 
in the system design.  Each machine had two 1 Gb/s Ethernet interfaces,
although only one interface is used in the race configuration.  Originally
a serial device server was used to allow any machine to talk to any actuator
(all of which had serial interfaces), but the serial device server had
reliability problems and was eventually removed from the system.  A
wireless bridge and wireless access point were also used in the system
during testing to allow remote access to the vehicle network.

To allow software components to communicate in a machine-independent
fashion, a custom messaging and module management system called
``Skynet'' was developed.  Skynet was specified to provide
inter-computer communication between programs on different computers
or on the same computer completely transparently.  The code run on
Alice is therefore broken down in to discrete functional modules, each
derived from a Skynet class to allow Skynet to start and stop modules.
This is required because Skynet was also designed be able to start,
stop, and redistribute modules between computers based on
computational resources available, assuming hardware failures of any
type were possible.  Skynet's communication capabilities are built on
top of the Spread group communication toolkit~\cite{Ami+04-tr}.

In the race configuration, the ability to redistribute and run modules
on different computers was not implemented and module restart was
accomplished using runlevel functionality provided by
Linux~\cite{Ver+05-gentoo}.  A custom runlevel was created to put each
computer into race-ready mode upon entry, by running a series of
scripts to start the modules appropriate for each machine.  Should a
given module exit or die for any reason, it is immediately restarted
using the respawn setting in /etc/inittab.  Paired with other scripts
for recompiling and distributing our modules and configuration files,
this proved to be an extremely efficient way of upgrading software and
switching to autonomous operations during development and testing.

\subsection{Software Architecture}

A diagram of our general system architecture is shown in 
Figure~\ref{fig-sysarch}.  
\begin{figure}
  \centerline{\includegraphics[width=0.85\textwidth]{systemarch.eps}}
  \caption{Overall System Architecture for Alice.}
  \label{fig-sysarch}
\end{figure}
Environmental sensors (stereo vision and LADAR) are used to create an
elevation map of the terrain around the vehicle.  Our range sensor suite
consisted of multiple LADAR units and 
stereo camera pairs.  The data from these sensors, in combination with our
state estimate, creates an elevation map in the global reference frame.  The
elevation map consists of a grid of cells, centered on the vehicle, where
the value of each cell corresponded to the elevation of that cell.  The map
moves along with the vehicle, so as cells move some distance behind us, new
cells are created in front of the vehicle.  

This elevation map is then converted into a cost map by considering
aspects such as elevation gradient, quality of data in that cell, etc.
The cost map establishes a speed limit for each location in the
terrain around the vehicle.  In addition to terrain data, the speed
limits set in the Route Definition Data File (RDDF) and information
from road finding algorithms~\cite{RK05-mviv} are integrated at this
point.  The speed limit-based cost map allows the vehicle to traverse
rough sections of the terrain (slowly) and insures that the vehicle
attempts to make forward progress unless there was an insurmountable
obstacle (speed = 0) in its path.  The elevation and cost mapping
algorithms are discussed in more detail in Section~\ref{sec:terrain}.

Once the cost map is generated, it was passed onto the planner where a
time-optimal path is created that satisfies vehicle and map speed
constraints~\cite{DK05-ms}.  This path  is sent onto the path follower, which 
in turn computes and sends appropriate actuation commands (brake, throttle, and 
steering) to the vehicle actuation system.  The vehicle control systems are 
discussed in Section~\ref{sec:control} and the path planning algorithm is 
discussed in Section~\ref{sec:planning}.

The supervisory control module serves to detect and manage higher-level
system faults that other individual modules can not.  This includes
scenarios such as losing and reacquiring GPS, and being stuck on an undetected
obstacle.  The supervisory control module is also responsible for
maintaining forward progress in unusual conditions, such as the case of
running up against barbed wire (the failure mode for Team Caltech's 2004
entry).  This system is described in more detail in
Section~\ref{sec:supercon}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "jfr05"
%%% End: 
