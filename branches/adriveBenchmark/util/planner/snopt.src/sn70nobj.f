**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*     File  sn70nobj.f
*
*     s7chkd   s7chkg
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s7chkd( n, bl, bu, x, dx, d, nfeas )

      implicit           double precision (a-h,o-z)
      double precision   bl(n), bu(n), x(n), dx, d(n)

*     ==================================================================
*     s7chkd  checks that x + dx*d is feasible.
*     It is used by s7chkg and s8chkJ for the cheap gradient checks.
*
*     Original:    Looked at the sign of d for variables on a bound.
*     13 Mar 1992: dx added as a parameter to make certain that
*                  x + dx*d does not lie outside the bounds.
*                  d may be altered to achieve this.
*     09 Aug 1992: First version based on Minos routine m7chkd.
*     09 Aug 1992: Current version.
*     ==================================================================
      parameter        ( zero  = 0.0d+0 )
*     ------------------------------------------------------------------
      nfeas  = 0
      do 500, j = 1, n
         xj     = x(j)
         b1     = bl(j)
         b2     = bu(j)
         if (b1   .eq. b2  ) d(j) = zero

         if (d(j) .ne. zero) then

*           x(j) is not fixed, so there is room to move.
*           If xj + dx*dj is beyond one bound, reverse dj
*           and make sure it is not beyond the other.
*           Give up and use set dj = zero if both bounds are too close.

            dj     = d(j)
            xnew   = xj  +  dx*dj

            if (dj .gt. zero) then
               if (xnew .gt. b2) then
                  dj     = - dj
                  xnew   =   xj  +  dx*dj
                  if (xnew .lt. b1) dj = zero
               end if
            else
               if (xnew .lt. b1) then
                  dj     = - dj
                  xnew   =   xj  +  dx*dj
                  if (xnew .gt. b2) dj = zero
               end if
            end if

            d(j)   = dj
            if (dj .ne. zero) nfeas  = nfeas + 1
         end if
  500 continue

*     end of s7chkd
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine s7chkg( ierror, m, n, nnL,
     $                   nnCon, nnCon0, nnJac, nnObj, nnObj0,
     $                   fgwrap, fgCon, fgObj,
     $                   ne   , nka, ha  , ka,
     $                   neJac, nkg,       kg,
     $                   bl, bu, fObj, 
     $                   gObj , fCon , gCon , 
     $                   gObj2, fCon2, gCon2,
     $                   x, y, y1, y2, 
     $                   cu, lencu, iu, leniu, ru, lenru, 
     $                   cw, lencw, iw, leniw, rw, lenrw )

      implicit           double precision (a-h,o-z)
      external           fgwrap, fgCon, fgObj

      integer            ha(ne)
      integer            ka(nka), kg(nkg)

      double precision   bl(nnL), bu(nnL)
      double precision   gObj (nnObj0), fCon (nnCon0), gCon (neJac)
      double precision   gObj2(nnObj0), fCon2(nnCon0), gCon2(neJac)
      double precision   x(nnL)
      double precision   y(nnL), y1(nnL), y2(m)

      character*8        cu(lencu), cw(lencw)
      integer            iu(leniu), iw(leniw)
      double precision   ru(lenru), rw(lenrw)

*     ==================================================================
*     s7chkg  verifies the objective and constraint gradients using
*     finite differences.
*
*     First, a cheap heuristic test is performed, as in
*     subroutine chkgrd by the following authors:
*     Philip E. Gill, Walter Murray, Susan M. Picken and Hazel M. Barber
*     DNAC, National Physical Laboratory, England  (circa 1975).
*
*     Next, a more reliable test is performed on each component of the
*     gradient, for indices in the range  jverif(1)  thru  jverif(2).
*
*     lvlVer is the verify level, which has the following meaning:
*
*     -1         do not perform any check.
*      0         do the cheap test only.
*      1 or 3    do both cheap and full test on objective gradients.
*      2 or 3    do both cheap and full test on the Jacobian.
*
*     10 Oct 1998: First version based on combining s7chkJ and s7chkg.
*     11 Oct 1998: Current version of s7chk.
*     ==================================================================
      logical            cheap , first
      logical            done  , found
      logical            getCon , getObj
      integer            RightJ, RightG, WrongJ, WrongG
      logical            yes   , no
      character*4        key
      parameter         (zero = 0.0d+0, one = 1.0d+0, ok = 0.1d+0)
      parameter         (yes  = .true., no  = .false.)

      character*4        lWrong         , lRight
      data               lWrong /'bad?'/, lRight /'ok  '/
*     ------------------------------------------------------------------
      lvlVer    = iw( 78)
      nGotg1    = iw(186)       ! nGotg(1)
      nGotg2    = iw(187)       ! nGotg(2)

      if (lvlVer .lt. 0 .or. (nGotg1 .eq. 0 .and. nGotg2 .eq. 0)) return

      iPrint    = iw( 12)
      iSumm     = iw( 13)

      j1        = iw( 98)       ! jverif(1)
      j2        = iw( 99)       ! jverif(2)
      j3        = iw(100)       ! jverif(3)
      j4        = iw(101)       ! jverif(4)

      eps0      = rw(  2)
      eps5      = rw(  7)
      fdint1    = rw( 76)
      gdummy    = rw( 69)

      j1        = max( j1, 1     )
      j2        = min( j2, nnObj )
      j3        = max( j3, 1     )
      j4        = min( j4, nnJac )

      jFirst    = min( j1, j3    )
      jLast     = max( j2, j4    )

*     The problem functions are called to provide functions only.
*     nState indicates that there is nothing special about the call.

      modefg = 0
      nState = 0

*     cheap = do the cheap test only

      cheap     = lvlVer .eq. 0  .or.  jFirst .gt. jLast

      if (iPrint .gt. 0) then
         if ( cheap ) then
            write(iPrint, 1000)
         else
            write(iPrint, 1100)
         end if
      end if

*     --------------------------------------------
*     Cheap test.
*     --------------------------------------------
*     Generate a direction in which to perturb  x.

      yj    = one/nnL
      do 100, j =  1, nnL
         y(j)  =   yj
         y1(j) =   yj
         yj    = - yj * 0.99999d+0
  100 continue

*     If needed, alter y to ensure that it will be a feasible direction.
*     If this gives zero, go back to original y and forget feasibility.

      dx     = fdint1 * (one + dnrm1s( nnL, x, 1 ))
      call s7chkd( nnL, bl, bu, x, dx, y, nfeas )
      if (nfeas .eq. 0) call dcopy ( nnL, y1, 1, y, 1 )

*     ------------------------------------------------------------------
*     Do not perturb x(j) if the jth column contains unknown elements.
*     ------------------------------------------------------------------
      nKnown = 0
      l      = 0
      do 140, j = 1, nnL

*        Do not perturb x(j) if g(j) is unknown.

         if (j .le. nnObj) then
            if (gObj(j) .eq. gdummy) y(j) = zero
         end if

         if (j .le. nnJac) then
            k1    = ka(j)
            k2    = ka(j+1) - 1

            do 120, k = k1, k2
               ir    = ha(k)
               if (ir .gt. nnCon) go to 130
               l     = l + 1
               if (gCon(l) .eq. gdummy) y(j) = zero
  120       continue
         end if

  130    if (y(j) .ne. zero) nKnown = nKnown + 1
  140 continue

*     Quit if there are no derivatives to check.

      if (nKnown .eq. 0) go to 900

*     ------------------------------------------------------------------
*     Compute functions at a short step along  y.
*     ------------------------------------------------------------------
      dx     = fdint1 * (one + dasum( nnL, x, 1 ))
      do 150, j = 1, nnL
         y1(j) = x(j) + dx*y(j)
  150 continue

      getCon = nnJac .gt. 0
      getObj = nnObj .gt. 0

      call fgwrap( modefg, ierror, nState, getCon , getObj,
     $             m, n, neJac, nnL, 
     $             nnCon, nnCon0, nnJac, nnObj, nnObj0, 
     $             fgCon, fgObj,
     $             ne, nka, ha, ka, 
     $             fCon2, fObj2, gCon2, gObj2, y1, 
     $             cu, lencu, iu, leniu, ru, lenru, 
     $             cw, lencw, iw, leniw, rw, lenrw )
      if (ierror .ne. 0) go to 900

*     ------------------------------------------------------------------
*     Cheap test for the constraint Jacobian.
*     ------------------------------------------------------------------
*     Set   y2 = (fCon2 - fCon)/dx - Jacobian*y.  This should be small.

      if ( getCon ) then
         do 160, i  = 1, nnCon
            y2(i)  = (fCon2(i) - fCon(i))/dx
  160    continue

         l      = 0
         do 190, j = 1, nnJac
            yj     = y(j)

            k      = ka(j)
            kmax   = ka(j+1) - 1
            done   = no

*+          ------------------------------------------------------------
*+          while (k .le. kmax  .and.  .not. done) do
  180       if    (k .le. kmax  .and.  .not. done) then
               ir     = ha(k)
               if (ir .gt. nnCon) then
                  done = yes
               else
                  l      = l + 1
                  y2(ir) = y2(ir) - gCon(l)*yj
               end if
               k  = k + 1
               go to 180
*+          end while
*+          ------------------------------------------------------------
            end if
  190    continue

         imaxJ  = idamax( nnCon, y2, 1 )
         gmaxJ  = (fCon2(imaxJ) - fCon(imaxJ))/dx
         emaxJ  = abs( y2(imaxJ) )/(one + abs( gmaxJ ))
         if (emaxJ .le. ok) then
            if (iPrint .gt. 0) write(iPrint, 1401)
         else
            if (iPrint .gt. 0) write(iPrint, 1501)
            if (isumm  .gt. 0) write(iSumm , 1501)
         end if
         if (iPrint .gt. 0) write(iPrint, 1601) emaxJ, imaxJ
      end if

*     ------------------------------------------------------------------
*     Cheap test for the objective gradient.
*     ------------------------------------------------------------------
      if ( getObj ) then
         gp     = ddot  ( nnObj, gObj, 1, y, 1 )
         gmaxG  = (fObj2 - fObj)/dx
         emaxG  = abs(gmaxG  - gp)/(one + abs( gmaxG ))

*        Set an error indicator if emaxg is too large.

         if (emaxG .le. ok) then
            if (iPrint .gt. 0) write(iPrint, 1402)
         else
            if (iPrint .gt. 0) write(iPrint, 1502)
            if (iSumm  .gt. 0) write(iSumm , 1502)
         end if
         if (iPrint .gt. 0) write(iPrint, 1602) gp, gmaxG
      end if

      if ( cheap ) go to 900

*     ------------------------------------------------------------------
*     Proceed with the verification of column elements.
*     Evaluate columns  jFirst thru jLast  of the problem derivatives. 
*     ------------------------------------------------------------------
      if (iPrint .gt. 0) then
         if (j3 .le. j4) then
            write(iPrint, 2001)
         else
            write(iPrint, 2002)
         end if
      end if

      WrongJ =   0
      RightJ =   0
      WrongG =   0
      RightG =   0
      jmaxJ  =   0
      jmaxG  =   0
      emaxJ  = - one
      emaxG  = - one

      do 300, j = jFirst, jLast
         found  = no

         getCon  = j3 .le. j  .and.  j .le. j4
         getObj  = j1 .le. j  .and.  j .le. j2
         
         if ( getObj ) then
            if (gObj(j) .ne. gdummy) found = yes
         end if

         if ( getCon ) then

*           See if there are any known gradients in this column.

            l      = kg(j)
            k      = ka(j)
            kmax   = ka(j+1) - 1
            done   = no

*+          --------------------------------------------------------
*+          while (k .le. kmax  .and. .not.(found  .or.  done)) do
  200       if    (k .le. kmax  .and. .not.(found  .or.  done)) then
               ir = ha(k)
               if (ir .gt. nnCon) then
                  done = yes
               else
                  if (gCon(l) .ne. gdummy) found = yes
                  l    = l + 1
               end if
               k  = k + 1
               go to 200
*+          end while
*+          --------------------------------------------------------
            end if
         end if

         if ( found ) then
*           ------------------------------------------------------------
*           gObj(j) or an element of the jth column of J is known.
*           ------------------------------------------------------------
            xj     = x(j)
            dx     = fdint1 * (one + abs( xj ))
            if (bl(j) .lt. bu(j)  .and.  xj .ge. bu(j)) dx = -dx
            x(j)   = xj + dx

            call fgwrap( modefg, ierror, nState, getCon, getObj,
     $                   m, n, neJac, nnL, 
     $                   nnCon, nnCon0, nnJac, nnObj, nnObj0, 
     $                   fgCon, fgObj,
     $                   ne, nka, ha, ka, 
     $                   fCon2, fObj2, gCon2, gObj2, x, 
     $                   cu, lencu, iu, leniu, ru, lenru, 
     $                   cw, lencw, iw, leniw, rw, lenrw )
            if (ierror .ne. 0) goto 900

*           ------------------------------------------------------------
*           Estimate the jth column of the Jacobian.
*           Check the estimate against the user-supplied values.
*           Don't bother printing a line if it looks like an exact zero.
*           Look for nonzeros not in the sparse data structure,
*           ------------------------------------------------------------
            if ( getCon ) then
               do 250, i  = 1, nnCon
                  y2(i)  = (fCon2(i) - fCon(i))/dx
  250          continue

               l      = kg(j)
               k      = ka(j)
               first  = yes
               done   = no

*+             ---------------------------------------------------------
*+             while (k .le. kmax  .and.  .not. done) do
  260          if    (k .le. kmax  .and.  .not. done) then

                  ir     = ha(k)
                  if (ir .gt. nnCon) then
                     done = yes
                  else
                     gi     = gCon(l)
                     l      = l + 1

                     if (gi .ne. gdummy) then
                        gdiff  = y2(ir)
                        err    = abs( gdiff - gi )/(one + abs( gi ))

                        if (emaxJ .lt. err) then
                           emaxJ  = err
                           imaxJ  = ir
                           jmaxJ  = j
                        end if

                        if (err .le.  eps5) then
                           key    = lRight
                           RightJ = RightJ + 1
                        else
                           key    = lWrong
                           WrongJ = WrongJ + 1
                        end if
                
                        if (iPrint .gt. 0  .and.
     $                                   (abs(gi) + err .gt. eps0)) then
                           if ( first ) then
                              write(iPrint, 2101) j, xj, dx, 
     $                                            l, ir, gi, gdiff, key
                              first  = no
                           else
                              write(iPrint, 2201) l, ir, gi, gdiff, key
                           end if
                        end if
                        y2(ir) = gdummy
                     end if
                  end if

                  k  = k + 1
                  go to 260
*+             end while
*+             ---------------------------------------------------------
               end if

*              Check that all unverified elements of y2 are zero.

               do 270, i = 1, nnCon
                  if ( y2(i) .ne. gdummy) then
                     err =  abs(y2(i))

                     if (err .gt. eps0) then
                        WrongJ = WrongJ + 1

                        if (iPrint .gt. 0) then
                           write(iPrint, 2301) i, y2(i), lWrong
                        end if

                        if (emaxJ .lt. err) then
                           emaxJ  = err
                           imaxJ  = i
                           jmaxJ  = j
                        end if
                     end if
                  end if
  270          continue
            end if ! getCon

*           ------------------------------------------------------------
*           Estimate gObj(j)
*           ------------------------------------------------------------
            if ( getObj ) then
               gj    = gObj(j)
               gdiff = (fObj2 - fObj) / dx
               err   = abs( gdiff - gj )/(one + abs( gj ))

               if (err .gt. emaxG) then
                  emaxG  = err
                  jmaxG  = j
               end if

               if (err .le. eps5) then 
                  key    = lRight
                  RightG = RightG + 1
               else
                  key    = lWrong
                  WrongG = WrongG + 1
               end if

               if (abs( gj ) + err  .gt.  eps0) then
                  if (iPrint .gt. 0) then
                     if (j3 .le. j4) then
                        write(iPrint, 2102) j, xj, dx, gj, gdiff, key
                     else
                        write(iPrint, 2103) j, xj, dx, gj, gdiff, key
                     end if
                  end if
               end if
            end if ! getObj

            x(j)  = xj
         end if ! found
  300 continue

*     ------------------------------------------------------------------
*     Final tally of the good, the bad and the ugly.
*     ------------------------------------------------------------------
      if (j3 .le. j4) then
         if (iPrint .gt. 0) then
            if (WrongJ .eq. 0) then
               write(iPrint, 2501) RightJ, j3, j4
            else
               write(iPrint, 2601) WrongJ, j3, j4
            end if
            write(iPrint, 2701) emaxJ, imaxJ, jmaxJ
         end if

         if (emaxJ .ge. one) then

*           Bad gradients in  funcon.

            ierror = 8
            if (iPrint .gt. 0) write(iPrint, 9081)
            if (iSumm  .gt. 0) write(iSumm , 9081)
         end if
      end if

      if (j1 .le. j2) then
         if (iPrint .gt. 0) then
            if (WrongG .eq. 0) then
               write(iPrint, 2502) RightG, j1, j2
            else
               write(iPrint, 2602) WrongG, j1, j2
            end if
            write(iPrint, 2702) emaxG, jmaxG
         end if

         if (emaxG .ge. one) then

*           Bad gradients in  funobj.

            ierror = 7
            if (iPrint .gt. 0) write(iPrint, 9082)
            if (iSumm  .gt. 0) write(iSumm , 9082)
         end if
      end if

*     ------------------------------------------------------------------
*     Print a message if the problem functions are undefined at x.
*     ------------------------------------------------------------------
  900 if (ierror .lt. 0) then
         ierror = 6
         write(iPrint, 3000)
      end if

      return

 1000 format(/   ' Cheap test of user-supplied problem derivatives...')
 1100 format(////' Verification of user-supplied problem derivatives.')
 1401 format(/   ' The Jacobian seems to be OK.')
 1402 format(    ' The objective gradients seem to be OK.')
 1501 format(/   ' XXX  The Jacobian seems to be incorrect.')
 1502 format(    ' XXX  The objective gradients seem to be incorrect.')
 1601 format(/   ' -->  The largest discrepancy was', 1p, e12.2,
     $           '  in constraint', i6 /)
 1602 format(    ' Gradient projected in one direction', 1p, e20.11,
     $       /   ' Difference approximation           ',     e20.11)
 2001 format(//  ' Column       x(j)        dx(j)', 3x,
     $   ' Element no.    Row        Derivative    Difference approxn')
 2002 format(// 6x, 'j', 7x, 'x(j)', 8x, 'dx(j)',
     $             11x, 'g(j)', 9x, 'Difference approxn' /)
 2101 format(/ i7, 1p, e16.8, e10.2, 2i10,               2e18.8, 2x, a4)
 2102 format(  i7, 1p, e16.8, e10.2, 10x, ' Objective',  2e18.8, 2x, a4)
 2103 format(  i7, 1p, e16.8, e10.2,                     2e18.8, 2x, a4)
 2201 format(      33x, 2i10, 1pe18.8, e18.8, 2x, a4)

 2301 format(  9x, 'Nonzero not in sparse structure ', '??', 4x, i6,18x,
     $          1p, e18.8, 2x, a4 )
 2501 format(/ i7, '  Jacobian elements in cols ', i6, '  thru', i6,
     $             '  seem to be OK.')
 2502 format(/ i7, '  objective gradients out of', i6, '  thru', i6,
     $             '  seem to be OK.')
 2601 format(/ ' XXX  There seem to be', i6,
     $        '  incorrect Jacobian elements in cols', i6, '  thru', i6)
 2602 format(/ ' XXX  There seem to be', i6,
     $      '  incorrect objective gradients in cols', i6, '  thru', i6)
 2701 format(/ ' -->  The largest relative error was', 1p, e12.2,
     $         '   in row', i6, ',  column', i6 /)
 2702 format(/ ' -->  The largest relative error was', 1p, e12.2,
     $         '   in column', i6 /)
 3000 format(//' EXIT -- Problem functions are undefined',
     $         ' during gradient checking.')
 9081 format(/ ' EXIT -- subroutine funcon appears to be',
     $         ' giving incorrect gradients')
 9082 format(/ ' EXIT -- subroutine funobj appears to be',
     $         ' giving incorrect gradients')

*     end of s7chkg
      end

