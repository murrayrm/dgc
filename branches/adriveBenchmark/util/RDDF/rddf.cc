/**********************************************************************
 **********************************************************************
 * +FILE:        rddf.cc
 *    
 * +DESCRIPTION: rddf class methods
 *
 * 
 **********************************************************************
 *********************************************************************/

#include "rddf.hh"

// Constructor
RDDF::RDDF(bool tack_one_on)
{
	m_bParsed = false;

  loadFile(RDDF_FILE, tack_one_on);
	_additionalWidth = 0.0;
}
RDDF::RDDF(char *pFileName, bool tack_one_on)
{
	m_bParsed = false;

  if (pFileName != NULL)
    loadFile(pFileName, tack_one_on);
  else
    numTargetPoints = 0;
	_additionalWidth = 0.0;
}
// Destructor
RDDF::~RDDF()
{
}

RDDF &RDDF::operator=(const RDDF &other)
{
  numTargetPoints = other.getNumTargetPoints();
  targetPoints = other.getTargetPoints();
  return *this;
}

// API comments in header
int RDDF::loadFile(char* fileName, bool tack_one_on)
{
	if(m_bParsed)
		return 0;

  ifstream file;
  string line;
  RDDFData eachWaypoint;
  bool on_first_line = true;

  numTargetPoints = 0;
  
  file.open(fileName,ios::in);
  if(!file)
  {
    cout << "UNABLE TO OPEN THE RDDF FILE: " << fileName << endl;
    exit(1);
  }

  is_startup_prepended_rddf = false; // until we see otherwise

  while(!file.eof())
  {
    GisCoordLatLon latlon;
    GisCoordUTM utm;

    // Number
    getline(file,line,RDDF_DELIMITER);    
    // First input on a line should be checked for empty lines.
    if(line.empty()) {
      continue;
    }

    eachWaypoint.number = atoi(line.c_str()) - 1;  // -1 lets us stick things in arrays while
                                                   // keeping them with DARPA standards. 
                                                   // (i.e. starting w/ waypoint #1)
    if(eachWaypoint.number < 0)
      {
	if (eachWaypoint.number == MOD_RDDF_NUM - 1 && on_first_line == true)
	  {
	    is_startup_prepended_rddf = true;
	    eachWaypoint.number = 0 - 1; // -1 because we do +1 later
	  }
	else
	  {
	    cerr << "Input RDDF is not in correct DARPA format--using zero-based RDDF.  Exiting." << endl;
	    cerr << "Used waypoint number " << eachWaypoint.number + 1 << " in file."
		 << endl;
	    exit(1);
	  }
      }
    on_first_line = false;
    if (is_startup_prepended_rddf)
      eachWaypoint.number++;

    // Latitude
    getline(file,line,RDDF_DELIMITER);
    latlon.latitude = atof(line.c_str());
    
    // Longitude
    getline(file,line,RDDF_DELIMITER);
    latlon.longitude = atof(line.c_str());

    // Converting to UTM
    if(!gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL))
    {
      cerr << "Error converting coordinate " << eachWaypoint.number << " to utm\n";
      exit(1);
    }
    eachWaypoint.Northing = utm.n;
    eachWaypoint.Easting = utm.e;

    // Boundary offset and radius
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.offset = atof(line.c_str())*METERS_PER_FOOT;
    eachWaypoint.radius = eachWaypoint.offset;
    // Speed limit
    getline(file,line,'\n');
    eachWaypoint.maxSpeed = atof(line.c_str())*MPS_PER_MPH;

    // stop valgrind et al from complaining about uninitialized values.. we'll actually
    // deal with this a few lines later
    eachWaypoint.distFromStart = 0;

    if(numTargetPoints <= eachWaypoint.number)
    {
      targetPoints.push_back(eachWaypoint);
      numTargetPoints++;
    }
    else
    {
      break;
    }
  }

  if (tack_one_on &&
			targetPoints[numTargetPoints-1].maxSpeed == 0.0)
	{
		targetPoints[numTargetPoints-1].maxSpeed = LAST_WAYPOINT_SPEED;

		double yaw = getTrackLineYaw(numTargetPoints-2);
		double dist = LAST_WAYPOINT_DIST;

		// Set the new waypoint to be far away and have zero speed
		eachWaypoint.Northing = eachWaypoint.Northing + dist*cos(yaw);
		eachWaypoint.Easting  = eachWaypoint.Easting  + dist*sin(yaw);

		// Set the speed to be something small 
		targetPoints[numTargetPoints-1].maxSpeed = LAST_WAYPOINT_SPEED;
		eachWaypoint.maxSpeed = LAST_WAYPOINT_SPEED;
		eachWaypoint.number = eachWaypoint.number+1;

		// Tack on this additional waypoint
		targetPoints.push_back(eachWaypoint);
		numTargetPoints++;
	}

  /** calculate the distance so far and saves it in distFromStart
   * added by jason, 24 July 2005 */
  for (int curPt = 0; curPt < numTargetPoints; curPt++)
    if (curPt == 0)
      {
	// we're on the first point (index 0)
	targetPoints[curPt].distFromStart = 0;
      }
    else
      {
	// not on the first point
	targetPoints[curPt].distFromStart = targetPoints[curPt - 1].distFromStart +
	  getTrackLineDist(curPt - 1);
      }

  // debug
  // cout << "RDDF: " << numTargetPoints << " target points read !!!" << endl;
	m_bParsed = true;
  return(0);
}

/** Add a new point to the end of the RDDF */
void RDDF::addDataPoint(RDDFData &data_point)
{
  ++numTargetPoints;
  targetPoints.push_back(data_point);
}


// API comments in header file.
void RDDF::print(ostream & out)
{
  for (int i = 0; i < numTargetPoints; i++)
    targetPoints[i].display();
}



// API comments in header file.
double RDDF::getTrackLineYaw( int i )
{
  // Return the Yaw of waypoint i's trackline.
  return atan2( targetPoints[i+1].Easting - targetPoints[i].Easting, 
                targetPoints[i+1].Northing - targetPoints[i].Northing );
}

// API comments in header file.
double RDDF::getTrackLineDist( int i )
{
  // Return the distance between waypoints i and i+1
  return sqrt( pow(targetPoints[i+1].Easting - targetPoints[i].Easting, 2) + 
               pow(targetPoints[i+1].Northing - targetPoints[i].Northing, 2) );
}

// API comments in header file.
NEcoord RDDF::getPointAlongTrackLine( double n, double e, double distance,
  double* thetaAtTarget, double* corRad)
{
	NEcoord pos(n,e);
	return getPointAlongTrackLine(pos, distance, thetaAtTarget);
}

NEcoord RDDF::getPointAlongTrackLine( NEcoord& pos, double distance,
  double* thetaAtTarget, double* corRad)
{
  // i is the index of the corridor segment we are in
  int i = getCurrentWaypointNumberLast( pos.N, pos.E );

  double remaining = distance; // remaining distance
  double frac; // fraction to next waypoint

  // we either have a projection to the trackline segment or we don't
  // this calculates the normalized distance from waypoint i to i+1 of the 
  // projection to the trackline.  0 <= frac <= 1 from i to i+1.
  frac = ( (pos - targetPoints[i].ne_coord()) * 
    (targetPoints[i+1].ne_coord() - targetPoints[i].ne_coord()) )
    / pow(getTrackLineDist(i), 2);
  // saturate frac to be constrained to the line segment
  //frac = fmax( fmin( frac, 1.0 ), 0.0 );

  // Now, from the start coordinate, move along the trackline the specified 
  // distance (initialized by stepping back to previous waypoint and adding 
  // this distance to the remaining).
  remaining += frac * getTrackLineDist(i);
  double dist = getTrackLineDist(i);
  
  while( remaining > dist ) 
  {
    // advance to the next waypoint
    remaining -= dist;
    i++;
    dist = getTrackLineDist(i);
  }
  
  // targetCorridor is the vector pointing from the start to the end of the
  // corridor our final point will be in
  NEcoord targetCorridor = targetPoints[i+1].ne_coord() - targetPoints[i].ne_coord();
  
  // if we're asking for the target orientation to be computed, fill it in with
  // the argument of the targetCorridor
  if(thetaAtTarget != NULL)
    *thetaAtTarget = atan2(targetCorridor.E, targetCorridor.N);

  if(corRad != NULL) *corRad = getRadius(i);
  
  return targetPoints[i].ne_coord() + 
    targetCorridor * remaining * (1.0 / getTrackLineDist(i));
}


// API comments in header
int RDDF::getCurrentWaypointNumberFirst( NEcoord& pos, double bonus_ratio, double bonus_const )
{
  return getCurrentWaypointNumberFirst( pos.N, pos.E, bonus_ratio, bonus_const );
}

// API comments in header
int RDDF::getCurrentWaypointNumberLast( NEcoord& pos, double bonus_ratio, double bonus_const )
{
  return getCurrentWaypointNumberLast( pos.N, pos.E, bonus_ratio, bonus_const );
}


// API comments in header
int RDDF::getCurrentWaypointNumberFirst( double Northing, double Easting, double bonus_ratio, double bonus_const )
{
  // set the current position vector
  NEcoord curPos( Northing, Easting );
  // declare waypoint vectors
  NEcoord curWay;

  // minimum found distance to waypoint
  double minDist = 123456789;
  int minDistWay = 0;
  
  // cycle through all of the waypoints
  for( int i = 0; i<numTargetPoints-1 ; i++ ) 
  {
    curWay.N = targetPoints[i].Northing;
    curWay.E = targetPoints[i].Easting;
 
    // check to see if we are within the corridor segment
    // if we are, just return with the index of the segment
    if( isPointInCorridor( i, curPos.N, curPos.E, bonus_ratio, bonus_const ) )
    {
      //printf("------ I am within corridor segment %d -------\n", i ); 
      return i;
    }

    // if this waypoint is closer than any other we've found,
    // then remember so 
    if( (curPos - curWay).norm() < minDist ) 
    {
      minDist = (curPos - curWay).norm(); 
      minDistWay = i;
    }
  } 
  //printf("------ I am NOT within the corridor -------\n" ); 
  
  // if we get to this point in the function, it means that we are
  // **not** within the corridor.  In this case, return the index of
  // the waypoint that is closest to us
  return minDistWay;
}

// API comments in header
int RDDF::getCurrentWaypointNumberLast( double Northing, double Easting, double bonus_ratio, double bonus_const )
{
  // set the current position vector
  NEcoord curPos( Northing, Easting );
  // declare waypoint vectors
  NEcoord curWay;

  // minimum found distance to waypoint
  double minDist = 123456789;
  int minDistWay = 0;
  
  // cycle through all of the waypoints
  for( int i = numTargetPoints-2; i>=0 ; i-- ) 
  {
    curWay.N = targetPoints[i].Northing;
    curWay.E = targetPoints[i].Easting;
 
    // check to see if we are within the corridor segment
    // if we are, just return with the index of the segment
    if( isPointInCorridor( i, curPos.N, curPos.E, bonus_ratio, bonus_const ) )
    {
      //printf("------ I am within corridor segment %d -------\n", i ); 
      return i;
    }

    // if this waypoint is closer than any other we've found,
    // then remember so 
    if( (curPos - curWay).norm() < minDist ) 
    {
      minDist = (curPos - curWay).norm(); 
      minDistWay = i;
    }
  } 
  //printf("------ I am NOT within the corridor -------\n" ); 
  
  // if we get to this point in the function, it means that we are
  // **not** within the corridor.  In this case, return the index of
  // the waypoint that is closest to us
  return minDistWay;
}



int RDDF::createBobFile(char* fileName)
{
  ofstream file;
  int ind=0;
  file.open(fileName);
  if(!file)
  {
    cout << "UNABLE TO OPEN THE FILE" << endl;
    return(BAD_BOB_FILE);
  }
  file << "# This is a Bob format waypoint specification file.\n";
  file << "# The column format here is: \n";
  file << "# 1. character [ignored]\n";
  file << "# 2. Easting[m]\n";
  file << "# 3. Northing[m] \n";
  file << "# 4. lateral_offset[m] \n";
  file << "# 5. speed_limit [m/s] \n";
  file << "# NOTE THAT THESE ARE IN SI AS OPPOSED TO THE RDDF SPEC.\n#\n";
  for(ind=0;ind < numTargetPoints;ind++)
  {

    file << "0   " << targetPoints[ind].Northing << " " << targetPoints[ind].Easting 
         << " " << targetPoints[ind].offset << " " << targetPoints[ind].maxSpeed << "\n"; 
  }
  return(0);
}



int RDDF::createRDDFFile(char* fileName)
{
  ofstream file;
  file.open(fileName);
  file.precision(10);
  if(!file)
    {
      cout << "UNABLE TO OPEN THE FILE" << endl;
      return(BAD_BOB_FILE);
    }

  int point_num;
  GisCoordUTM utm;
  GisCoordLatLon latlon;
  for(int i = 0; i < numTargetPoints; i++)
    {
      point_num = i + 1;
      utm.zone = UTM_ZONE;
      utm.letter = UTM_LETTER;
      utm.n = targetPoints[i].Northing;
      utm.e = targetPoints[i].Easting;

      if (!gis_coord_utm_to_latlon (&utm, &latlon, GEODETIC_MODEL))
	{
	  cerr << "Error converting coordinate to latlon." << endl;
	  exit (1);
	}

      if (is_startup_prepended_rddf)
	{
	  if (i==0)
	    point_num = MOD_RDDF_NUM;
	  else
	    point_num--;
	}

      file << point_num << RDDF_DELIMITER
	   << latlon.latitude << RDDF_DELIMITER
	   << latlon.longitude << RDDF_DELIMITER
	   << targetPoints[i].offset / METERS_PER_FOOT << RDDF_DELIMITER
	   << targetPoints[i].maxSpeed / MPS_PER_MPH << endl;
    }
  return(0);
}



int RDDF::isPointInCorridor(int waypointNum, double Northing, double Easting, double bonus_ratio, double bonus_const )
{
  if(waypointNum<0 || waypointNum>=(numTargetPoints-1)) {
    return 0;
  }

  NEcoord cur_way(targetPoints[waypointNum].Northing, 
                  targetPoints[waypointNum].Easting);
  NEcoord pointRelative = NEcoord(Northing, Easting) - cur_way;
  NEcoord nextWPRelative = NEcoord(targetPoints[waypointNum+1].Northing, targetPoints[waypointNum+1].Easting) - cur_way;
  double rad = getRadius(waypointNum) * bonus_ratio + bonus_const;

  // Check if within the circular part of the corridor    
  // Within the circle around the current waypoint
  // -or- within the circle around the next waypoint
  if (pointRelative.norm() < rad ||
      (pointRelative - nextWPRelative).norm() < rad) 
  {
    return 1;
  }

	// rotate the point to lie in a coordinate system parallel to the RDDF segment
	double theta = atan2(nextWPRelative.E, nextWPRelative.N);
	NEcoord pointRotated( cos(theta)*pointRelative.N + sin(theta)*pointRelative.E,
											 -sin(theta)*pointRelative.N + cos(theta)*pointRelative.E);

	if(pointRotated.N >=  0.0 && pointRotated.N <= nextWPRelative.norm() &&
		 pointRotated.E >= -rad && pointRotated.E <= rad)
		return 1;

  return 0;
}

int RDDF::getCorridorSegmentBoundingBoxUTM(int waypointNum, 
					   double winBottomLeftUTMNorthing, double winBottomLeftUTMEasting,
					   double winTopRightUTMNorthing, double winTopRightUTMEasting,
					   double& BottomLeftUTMNorthing, double& BottomLeftUTMEasting, 
					   double& TopRightUTMNorthing, double& TopRightUTMEasting) 
{
  //int corType;
  int bottomOut = 0;
  int topOut = 0;
  double firstRadius, secondRadius, radius;
  firstRadius = getRadius(waypointNum);
  secondRadius = getRadius(waypointNum+1);
  radius = max(firstRadius, secondRadius);
  
  BottomLeftUTMNorthing = min(targetPoints[waypointNum].Northing-radius,
			      targetPoints[waypointNum+1].Northing-radius);
  TopRightUTMNorthing = max(targetPoints[waypointNum].Northing+radius,
			    targetPoints[waypointNum+1].Northing+radius);
  BottomLeftUTMEasting = min(targetPoints[waypointNum].Easting-radius,
			      targetPoints[waypointNum+1].Easting-radius);
  TopRightUTMEasting = max(targetPoints[waypointNum].Easting+radius,
			    targetPoints[waypointNum+1].Easting+radius);


  if(BottomLeftUTMNorthing < winBottomLeftUTMNorthing) 
  {
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    BottomLeftUTMNorthing = winBottomLeftUTMNorthing;
  }
  if(BottomLeftUTMEasting < winBottomLeftUTMEasting) 
  {
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    BottomLeftUTMEasting = winBottomLeftUTMEasting;
  }
  if(TopRightUTMNorthing > winTopRightUTMNorthing) 
  {
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    TopRightUTMNorthing = winTopRightUTMNorthing;
  }
  if(TopRightUTMEasting > winTopRightUTMEasting) 
  {
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    TopRightUTMEasting = winTopRightUTMEasting;
  }

  if(BottomLeftUTMNorthing > winTopRightUTMNorthing ||
     BottomLeftUTMEasting > winTopRightUTMEasting ||
     TopRightUTMNorthing < winBottomLeftUTMNorthing ||
     TopRightUTMEasting < winBottomLeftUTMEasting) 
  {
    return -1;
  }

  if(targetPoints[waypointNum+1].Northing - radius> winTopRightUTMNorthing ||
     targetPoints[waypointNum+1].Northing + radius< winBottomLeftUTMNorthing || 
     targetPoints[waypointNum+1].Easting - radius > winTopRightUTMEasting ||
     targetPoints[waypointNum+1].Easting + radius < winBottomLeftUTMEasting) 
  {
    topOut=1;
  }

  if(targetPoints[waypointNum].Northing - getRadius(waypointNum) > winTopRightUTMNorthing ||
     targetPoints[waypointNum].Northing + getRadius(waypointNum) < winBottomLeftUTMNorthing || 
     targetPoints[waypointNum].Easting - getRadius(waypointNum) > winTopRightUTMEasting ||
		 targetPoints[waypointNum].Easting + getRadius(waypointNum) < winBottomLeftUTMEasting) 
  {
    bottomOut = 1;
  }

  if(bottomOut == 1 && topOut == 1) return 3;
  if(bottomOut == 1 && topOut == 0) return 2;
  if(bottomOut == 0 && topOut == 1) return 1;

  return 0;
}


int RDDF::getWaypointNumAheadDist(double UTMNorthing, double UTMEasting, double distance) 
{
  int seed = getCurrentWaypointNumberFirst(UTMNorthing, UTMEasting);
  NEcoord curPos(UTMNorthing, UTMEasting);
  NEcoord wayPos(targetPoints[seed].Northing, targetPoints[seed].Easting);
  if((curPos - wayPos).norm() > distance) 
  {
    return (seed+1 > numTargetPoints-1) ? numTargetPoints-1 : seed + 1;
  }
  for(int i=seed; i<numTargetPoints-1; i++) 
  {
    wayPos.N = targetPoints[i].Northing;
    wayPos.E = targetPoints[i].Easting;
    if((curPos - wayPos).norm() > distance) 
    {
      return i;
    }
  }

  return numTargetPoints-1;
}


int RDDF::getWaypointNumBehindDist(double UTMNorthing, double UTMEasting, double distance) 
{
  int seed = getCurrentWaypointNumberFirst(UTMNorthing, UTMEasting);
  NEcoord curPos(UTMNorthing, UTMEasting);
  NEcoord wayPos(targetPoints[seed].Northing, targetPoints[seed].Easting);
  if((curPos - wayPos).norm() > distance) return (seed-1 < 0) ? 0 : seed-1;

  for(int i=seed; i>0; i--) 
  {
    wayPos.N = targetPoints[i].Northing;
    wayPos.E = targetPoints[i].Easting;
    if((curPos - wayPos).norm() > distance) 
    {
      return i;
    }
  }

  return 0;
}


double RDDF::getDistToTrackline( NEcoord pointCoords, int wayptNum )
{
  //We're making a right-triangle - we want to find one of the legs
  //So first we find the length of the other leg
  double distAlongTrackline = pow((NEcoord(pointCoords.N, pointCoords.E) - targetPoints[wayptNum].ne_coord()) * 
    (targetPoints[wayptNum+1].ne_coord() - targetPoints[wayptNum].ne_coord()), 2)/pow(getTrackLineDist(wayptNum), 2);
  //Then the length of the hypoteneuse
  double hypoteneuse = (NEcoord(pointCoords.N, pointCoords.E) - targetPoints[wayptNum].ne_coord()) * 
    (NEcoord(pointCoords.N, pointCoords.E) - targetPoints[wayptNum].ne_coord());

  //And return the result of the pythagorean theorem
  return sqrt(hypoteneuse - distAlongTrackline);
  //DO NOT COMMIT YET - FINISH TESTING  
}


double RDDF::getDistToTrackline( double northing, double easting, int wayptNum )
{
  NEcoord alicePos( northing, easting );
  return getDistToTrackline( alicePos, wayptNum );
}


double RDDF::getDistToTrackline(NEcoord pointCoords) 
{
  //Get the nearest waypoint
  int wayptNum = getCurrentWaypointNumberFirst(pointCoords.N, pointCoords.E);  
  return getDistToTrackline( pointCoords, wayptNum );
}


double RDDF::getRatioToTrackline(NEcoord pointCoords) 
{
  //Get the distance to 
  double dist = getDistToTrackline(pointCoords);

  int wayptNum = getCurrentWaypointNumberFirst(pointCoords.N, pointCoords.E);
 
  return dist/getOffset(wayptNum);
}

double RDDF::getDistToCorridorSegment(int wpt_num, const NEcoord & position,
				      double & distFromStart, double & ratio)
{
  /** Error checking */
  if (wpt_num < 0 || wpt_num >= numTargetPoints)
    throw std::string("RDDF::getDistToCorridorSegment:  invalid waypoint (< 0 or >= numTargetPoints)");

  /** If we're at the last waypoint, just return the distance */
  if (wpt_num == numTargetPoints - 1)
    {
      distFromStart = targetPoints[wpt_num].distFromStart;
      NEcoord something = position  - targetPoints[wpt_num].ne_coord();
      ratio = 1; // 0 or 1... same for last waypoint
      return (position - targetPoints[wpt_num].ne_coord()).norm();
    }

  /** get the corridor as a vector starting at (0,0) */
  NEcoord cor_vec = targetPoints[wpt_num + 1].ne_coord() - targetPoints[wpt_num].ne_coord();
  /** get the vector from first wpt to position */
  NEcoord pos_vec = position - targetPoints[wpt_num].ne_coord();

  double corDotPos = cor_vec * pos_vec;
  double corMagSquared = cor_vec.N * cor_vec.N + cor_vec.E * cor_vec.E;

  if (corDotPos < 0)
    {
      /** nearest point is corridor point at wpt_num */
      distFromStart = targetPoints[wpt_num].distFromStart;
      ratio = 0;
      return pos_vec.norm();
    }
  else if (corDotPos > corMagSquared)
    {
      /** nearest point is corridor point at wpt_num + 1 */
      distFromStart = targetPoints[wpt_num + 1].distFromStart;
      ratio = 1;
      return (position - targetPoints[wpt_num + 1].ne_coord()).norm();
    }
  else
    {
      /** nearest point is in middle of this segment
       * first, get vector from first pt to closest trackline pt */
      NEcoord cor_vec_part = cor_vec * (corDotPos / corMagSquared);
      distFromStart = targetPoints[wpt_num].distFromStart + cor_vec_part.norm();
      double corridor_length = targetPoints[wpt_num + 1].distFromStart - 
	targetPoints[wpt_num].distFromStart;
      ratio = corridor_length == 0 ? -1 : cor_vec_part.norm() / corridor_length;
      return (pos_vec - cor_vec_part).norm();
    }
}


double RDDF::exhaustiveNearestSearch(int & wpt_num, const NEcoord & position,
				     double & distFromStart, double & ratio)
{
  int cur_wpt = numTargetPoints - 1;
  double cur_dist_from_start = 0;
  int best_wpt_so_far = cur_wpt;
  double best_dist_so_far = 1e20;
  double best_dist_from_start = 0;
  double temp_ratio, this_dist;
  
  while (cur_wpt >= 0)
    {
      this_dist = getDistToCorridorSegment(cur_wpt, position,
					   cur_dist_from_start, temp_ratio);
      
      if (this_dist < best_dist_so_far)
	{
	  // we found a new best
	  best_dist_so_far = this_dist;
	  best_wpt_so_far = cur_wpt;
	  best_dist_from_start = cur_dist_from_start;
	}
      --cur_wpt;
    }

  wpt_num = best_wpt_so_far;
  distFromStart = best_dist_from_start;
  ratio = temp_ratio;

  return best_dist_so_far;
}


NEcoord RDDF::getCorridorPoint(int wayptNum, CORRIDOR_SIDE side, CORRIDOR_END end) {
  NEcoord waypt, offset;
  double angle;
  double width = getOffset(wayptNum);

  if(end==END_FRONT) {
    waypt = getWaypoint(wayptNum);
  } else {
    waypt = getWaypoint(wayptNum+1);
  }
  if(side==SIDE_RIGHT) {
    angle = getTrackLineYaw(wayptNum)+M_PI/2.0;
  } else {
    angle = getTrackLineYaw(wayptNum)-M_PI/2.0;
  }

  offset = NEcoord(width*cos(angle), width*sin(angle));

  return waypt + offset;
}


void RDDF::setExtraWidth(double additionalWidth) {
	_additionalWidth = additionalWidth;
}


bool RDDF::isPointInCorridor(NEcoord pos, double bonus_ratio, double bonus_const ) {
	return isPointInCorridor(pos.N, pos.E, bonus_ratio, bonus_const);
}


bool RDDF::isPointInCorridor(double Northing, double Easting, double bonus_ratio, double bonus_const) {
	int start = getCurrentWaypointNumberFirst(Northing, Easting, bonus_ratio, bonus_const);
	int last = getCurrentWaypointNumberLast(Northing, Easting, bonus_ratio, bonus_const);

	for(int i=start; i <= last; i++) {
		if(isPointInCorridor(i, Northing, Easting, bonus_ratio, bonus_const))
			return true;
	}

	return false;
}


int RDDF::findWaypointWithLargestWidth(NEcoord pos) {
	int first = getCurrentWaypointNumberFirst(pos);
	int last = getCurrentWaypointNumberLast(pos);
	
	double max_width = 0.0;
	int max_pt = 0;

	for(int i=first; i <= last; i++) {
		if(getOffset(i) > max_width) {
			max_pt = i;
			max_width = getOffset(i);
		}
	}

	return max_pt;
}
