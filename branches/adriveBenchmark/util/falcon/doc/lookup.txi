@c -*- texinfo -*-

@node lookup, matrix, traj, Top
@comment  node-name,  next,  previous,  up
@chapter Lookup Tables

@cindex Lookup Table

The lookup table module facilitates the approximation of nonlinear functions 
of arbitary dimension.  Each dimension is gridded uniformly, and the 
function's value is specified at each grid point.  Linear interpolation is
used to approximate values between grid points.  Essentially, this module
implements a mapping from a vector to another vector, with potentially
different dimensions.  A one dimensional lookup table differs from a 
trajectory in that the lookup table requires uniform spacing of points and
is optimized for random access, while the trajectory is optimized for
sequential access.


@menu
* introduction: lookup/intro.          introduction to lookup table package
* initialization: lookup/init.         initializing the lookup table
* file format: lookup/file.            lookup table file format
* lookup table functions: lookup/lu.   Lookup table functions
* table creation: lookup/manual.       Manual creation of lookup tables
* other functions: lookup/other.       Other functions
* function prototypes: lookup/proto.   Function prototypes
@end menu

@node lookup/intro, lookup/init, lookup, lookup
@section Introduction

The lookup table module allows the approximation of arbitrary nonlinear
functions.  The function's domain is gridded, and its value is specified
at each grid point.  Linear interpolation is used to calculate value
between grid points.  Any point in the function's domain can be calculated
in constant time.  

The routines use a special strucure, called @code{LU_DATA}.  The 
user should neither change any of the elements in the structure 
directly nor access them.  All necessary information can be 
obtained through function calls.  All functions begin with the
prefix @code{lu_}.  The structure (and also function prototypes)
are specified in @code{lutable.h.}

Lookup tables must be generated in advance and are saved as formatted
ASCII files.  This format,  described later, allows other programs to 
easily create and access the data.
Also, it avoids the need to recompile software when testing
different lookup tables.

Many functions perform some rudimentary error checking.  In the interest
of speed, it is assumed that the functions are passed a vaild, initialized
lookup table pointer.

@node lookup/init, lookup/file, lookup/intro, lookup
@section Initialization

The first step is to load a lookup table into memory.
This is accomplished using the function @code{lu_load}, which returns
a pointer to the lookup table.  If the load is not successful, the program
either hangs or returns a @code{NULL} pointer.  A sample program is 
shown below.
@cindex lu_load

@example
#include "lutable.h"

int main(argc,argv)
@{
    LU_DATA *data;

    data=lu_load(filename);

    /* other code goes here */

    lu_free(data);
@}
@end example

Since @code{lu_load} allocates memory using @code{malloc}, the user
may desire to free the space at some time (perhaps before loading a new
lookup table).  @code{lu_free} accomplishes this.
@cindex lu_free

Information about the size of the of the table is obtained from several
functions.  @code{lu_dimension} returns the dimension of the table.  
@code{lu_outputs} and @code{lu_interp} return the number of outputs and
the number of interpolated outputs, respectively.  @code{lu_elements}
returns the number of grid points in the lookup table.
@cindex lu_dimension
@cindex lu_outputs
@cindex lu_interp
@cindex lu_elements

@node lookup/file, lookup/lu, lookup/init, lookup
@section The File Format

The function @code{lu_load} reads a file whose first lines
specify the size and domain of the lookup table, and subsequent lines
contain the data.  All values are read as doubles, and extra
information is ignored.  The numbers can be separated by either
white space or tabs.  The following example shows the required
file format.

@example
Dimension  Outputs  Interpolated
dim1_low   dim1_step   dim1_high  dim1_count
dim2_low   dim2_step   dim2_high  dim2_count
...
dimN_low   dimN_step   dimN_high  dimN_count
dim1a dim2a ... dimNa      data1a  data1b  data1c  data1d ...
dim1a dim2a ... dimNb      data2a  data2b  data2c  data2d ...
and so on...
@end example

@code{Dimension} is the dimension of the domain of the lookup table.
@code{Outputs} is the number of outputs, the range of the lookup table.
@code{Interpolated} is the number of outputs which are interpolated.  It is
often useful (status variables, for example) to not interpolate an output.
Only the first @code{Interpolated} outputs are interpolated, the remaining
ones are not.
@code{dimI_low} and @code{dimI_high} specify the domain of the table in
Ith dimension.  @code{dimI_step} specifies the step size, and 
@code{dimI_count} is the number of grid points for the Ith dimension.
This contains redundant information, since low + (count - 1) * step = high.

Finally, the coordinates and data are listed.  Each line of the data specifies
the function's value at one grid point.  For an N-dimensional table with
M outputs, the first N numbers specify the coordinate and the remaining
numbers specify the output.  The order is such that first dimension is held
constant, while all other dimensions are incremented.  Since the position
in the datafile uniquely specifies the coordinate, the coordinate data is
redundant.  However, storing the data in a file greatly simplifies human
editing of lookup tables.  Currently, no verification of the coordinates
is done, though this might change in the future.

The following is a valid lookup table file.

@example
 3    2    1
 0    0.5  1    3
 1    0    1    0
 4    1    5    2
 0    1    4    1    0.4
 0    1    5    1    0.5
 0.5  1    4    1.5  0.45
 0.5  1    5    1.5  0.55
 1    1    4    2    0.5
 1    1    5    2    0.6
@end example

In this example, the lookup table maps 3 dimensions into 2.  The first
output is interpolated, and the second is not.  Also, one dimension is
held constant.

@node lookup/lu, lookup/manual, lookup/file, lookup
@section Reading From the Lookup Table

Data is obtained from the lookup table by using the command
@code{lu_lookup}.

@example
int lu_lookup(LU_DATA *data, double *coord, double *data);
@end example

All arguments to the function must be allocateed in advance, since
no memory allocation occurs during execution.  @code{coord} contains
the points a which the function's value is desired.  The value is
placed in @code{data}.  If either vector is incorrectly sized, errors
may occur.
@cindex lu_lookup
If the desired coordinate is outside of the function's domain, the 
coordinate is projected onto the domain of the function.
For outputs that are not interpolated, the value of that output at the
grid point whose coordinate is largest value which is less than or equal 
to the desired point (i.e. the lower corner) is returned.

@node lookup/manual, lookup/other, lookup/lu, lookup
@comment  node-name,  next,  previous,  up
@section On-line lookup table creation and modification

Since some applications create lookup tables or directly modify
them, more detailed access into the data structure is provided.
Several steps are required to manually create a lookup table.
First, a lookup table of specified dimension is created by 
@code{lu_create}.  The domain of each dimension is then set 
using @code{lu_setrange}.  The number of outputs is set by 
@code{lu_setoutput}.  Finally, the rest of the memory is allocated
by @code{lu_resize}.
@cindex lu_create
@cindex lu_setrange
@cindex lu_setoutput
@cindex lu_resize

Once the lookup table has been resized, @code{lu_setcoord} initializes 
the coordinates.  The coordinate of a row is obtained from 
@code{lu_coordinate},
@cindex lu_setcoord
@example
int lu_coordinate(LU_DATA *l, int row, double *coord);
@end example
@noindent
where @code{coord} is an array large enough to hold the coordinate.
@cindex lu_coordinate
The lookup table's values are set using @code{lu_setel} and 
@code{lu_getrow}.  @code{lu_setel} sets the value of one output of
one gridpoint.  @code{lu_getrow} returns the address of the array 
which stores all outputs for a specified grid point.  This allows
easier access to the table's data.  The function @code{lu_getel}
returns the value of one output of one gridpoint in the lookup table.
@cindex lu_getrow
@cindex lu_setel
@cindex lu_getel

@node lookup/other, lookup/proto, lookup/manual, lookup
@comment  node-name,  next,  previous,  up
@section Other functions

Basic information about the size of a lookup table is displayed by
@code{lu_dispinfo}.  @code{lu_write} saves a lookup table to a file.

@cindex lu_dispinfo
@cindex lu_write

There are many other functions which give a lot more power over lookup
tables, but they are not yet documented.

@node lookup/proto,  , lookup/other, lookup
@comment  node-name,  next,  previous,  up
@section Function prototypes

All function prototypes are included in @code{lutable.h}.

@example
LU_DATA *lu_load(char *file);
LU_DATA *lu_create(int ndim);
int lu_resize(LU_DATA *l);
int lu_lookup(LU_DATA *table, double *coord, double *data);
void lu_free(LU_DATA *table);
void lu_dispinfo(LU_DATA *l);
int lu_setrange(LU_DATA *l, int index, double low, double inc, double high,
		int steps);
int lu_setoutput(LU_DATA *l, int output, int interp);
int lu_setcoord(LU_DATA *l);
int lu_write(LU_DATA *l, char *file);
int     lu_setel(LU_DATA *l, int row, int col, double value);
double  lu_getel(LU_DATA *l, int row, int col);
double *lu_getrow(LU_DATA *l, int row);
int lu_dimension(LU_DATA *l);
int lu_outputs(LU_DATA *l);
int lu_interp(LU_DATA *l);
int lu_elements(LU_DATA *l);
int lu_coordinate(LU_DATA *l, int row, double *coord);
@end example
