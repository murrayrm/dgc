@c -*- texinfo -*-

@node changes,reference,state,Top
@chapter Changes from version 1.3 to version 2.0

This chapter documents changes in the sparrow library since version 1.3.

@itemize @bullet
@item All channel routines now start with the prefix @code{chn_}.  This
includes @code{chn_read} and @code{chn_write}.

@item The servo routines have been changed to use slightly different
names.  Use @code{servo_setup} to install a servo handler and
@code{servo_clean} to un-install the handler.  @xref{servo}.

@item There's a new channel interface in town.  The channel
configuration file looks very different, and some new flags have been added
for the devices, such as limit checking for some.  @xref{channel}.

@item A new device driver which generates certain functions, which for
instance might be used as reference signals, has been
written. @xref{channel}.

@item There are now two set of data capture routines. The old capture
routines are unchanged and the new asynchronously-dumped routines
follow them closely as far as interface goes.  They use the prefixes
@code{chn_capture_} and @code{chn_adcap_}, respectively. @xref{channel}.

@item @code{DEFAULT_FOREGROUND} and @code{DEFAULT_BACKGROUND} are now
@code{DD_DEFFG} and @code{DD_DEFBG} (in @file{display.h})

@end itemize
