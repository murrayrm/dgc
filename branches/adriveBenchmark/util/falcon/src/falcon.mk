FALCON_PATH = $(DGC)/util/falcon/src/

FALCON_DEPEND = \
	$(FALCON_PATH)/loadmat.c \
	$(FALCON_PATH)/loadmtx.c \
	$(FALCON_PATH)/lpv.c \
	$(FALCON_PATH)/lpv.h \
	$(FALCON_PATH)/lutable.c \
	$(FALCON_PATH)/lutable.h \
	$(FALCON_PATH)/lutable2.c \
	$(FALCON_PATH)/lutable3.c \
	$(FALCON_PATH)/mat_add.c \
	$(FALCON_PATH)/mat_det.c \
	$(FALCON_PATH)/mat_iden.c \
	$(FALCON_PATH)/mat_mult.c \
	$(FALCON_PATH)/mat_norm.c \
	$(FALCON_PATH)/mat_sub.c \
	$(FALCON_PATH)/matrix.c \
	$(FALCON_PATH)/matrix.h \
	$(FALCON_PATH)/state.c \
	$(FALCON_PATH)/state.h \
	$(FALCON_PATH)/traj.c \
	$(FALCON_PATH)/traj.h \
	$(FALCON_PATH)/traj2.c
