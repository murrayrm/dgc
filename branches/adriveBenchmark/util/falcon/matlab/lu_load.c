/*
 * lu_load - open a lookup table
 *
 * RMM, 14 May 96
 *
 */

#include <stdio.h>
#include "mex.h"
#include "falcon/lutable.h"

void mexFunction(nlhs, plhs, nrhs, prhs)
int nlhs, nrhs;
Matrix *plhs[], *prhs[];
{
    char filename[1024], address[16];
    LU_DATA *lut = NULL;

    if (nrhs != 1) {
	mexErrMsgTxt("lu_load: missing or invalid argument");
	return;
    }
    mxGetString(prhs[0], filename, 1024);

    if ((lut = lu_load(filename)) == NULL) {
	mexErrMsgTxt("lu_load: can't open lookup table");
	return;
    }

    sprintf(address, "0x%x", lut);
    plhs[0] =  mxCreateString(address);
}
