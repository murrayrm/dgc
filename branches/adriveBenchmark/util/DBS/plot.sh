#! /bin/sh

cat $1 | gawk '{print $2}' > pitch
cat $1 | gawk '{print ($4^2 + $5^2)^.5}' > fft_results

echo "plot 'pitch' with lines" | gnuplot -persist
echo "plot 'fft_results' with lines" | gnuplot -persist

