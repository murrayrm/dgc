#include"CTimberClient.hh"
#include"videoRecorder.hh"
#include"firewireCamera.hh"
#include"conversions.h"
#include"sparrowhawk.hh"
#include"vddvideo.h"
#include"sn_types.h"

#include<SDL/SDL.h>
#include <cv.h>
#include <highgui.h>

#include<sys/stat.h>
#include<unistd.h>

#include<iostream>

CVideoRecorder::CVideoRecorder(int skynet_key, bool sparrow)
  : CSkynetContainer(MODvideoRecorder, skynet_key),
    CTimberClient(timber_types::videoRecorder),
    m_pCamera(0), m_pInternalCamera(0), m_image(0), m_imageBW(0), m_image_buf(0), 
    m_exposure_pid(0.15,0,0,1.0), m_log_index_file(0), m_display_inited(false),
    m_screen(0), m_surf(0)
{
  m_terminate=false;
  m_bytes_loged.clear();
  m_auto_exposure=false;  // true
  m_exposure_level = 0;

  m_show_ROI=false;
  m_show_hist=false;
  m_hist_eq=false;

  m_ROI.x=0;
  m_ROI.y=0;
  m_ROI.width=0;
  m_ROI.height=0;

  DGCcreateMutex(&m_mutex);
  DGCcreateMutex(&m_write_mutex);
  DGCcreateMutex(&m_first_image_mutex);
  m_first_image = true;
  DGCcreateMutex(&m_camera_mutex);

  //Do sparrow bindings
  if(sparrow) {
    SparrowHawk().add_page(vddvideo, "Video Recorder");

    //camera
    SparrowHawk().rebind("StatCamFreq", &m_camera_cnt.freq);
    SparrowHawk().set_readonly("StatCamFreq");
    SparrowHawk().rebind("StatCamCnt", &m_camera_cnt.cnt);
    SparrowHawk().set_readonly("StatCamCnt");
    SparrowHawk().rebind("CamImgAvg", &m_image_avg);
    SparrowHawk().set_readonly("CamImgAvg");
    SparrowHawk().rebind("CamImgROIAvg", &m_image_ROI_avg);
    SparrowHawk().set_readonly("CamImgROIAvg");
    SparrowHawk().rebind("CamExposure", &m_exposure_level);
    SparrowHawk().set_setter("CamExposure", this, &CVideoRecorder::setter_exposure);
    SparrowHawk().rebind("ShowROI", &m_show_ROI);
    SparrowHawk().rebind("ROIx", &m_ROI.x);
    SparrowHawk().rebind("ROIy", &m_ROI.y);
    SparrowHawk().rebind("ROIw", &m_ROI.width);
    SparrowHawk().rebind("ROIh", &m_ROI.height);
    SparrowHawk().rebind("ShowHist", &m_show_hist);
    SparrowHawk().rebind("HistogramEq", &m_hist_eq);
    SparrowHawk().set_setter("ShowHist", this, &CVideoRecorder::setter_show_hist);
    SparrowHawk().rebind("CamBU", &m_whitebalance_bu);
    SparrowHawk().set_setter("CamBU", this, &CVideoRecorder::setter_whitebalance_bu);
    SparrowHawk().rebind("CamRV", &m_whitebalance_rv);
    SparrowHawk().set_setter("CamRV", this, &CVideoRecorder::setter_whitebalance_rv);
    
    //display
    SparrowHawk().rebind("StatDispFreq", &m_display_cnt.freq);
    SparrowHawk().set_readonly("StatDispFreq");
    SparrowHawk().rebind("StatDispCnt", &m_display_cnt.cnt);
    SparrowHawk().set_readonly("StatDispCnt");
    SparrowHawk().set_setter("DispFramerate", this, &CVideoRecorder::setter_desired_framerate, 0);
    SparrowHawk().set_getter("DispFramerate", this, &CVideoRecorder::getter_desired_framerate, 0);
    set_display_strings();
    SparrowHawk().set_notify("ResNone", this, &CVideoRecorder::display_mode_notification, 0);
    SparrowHawk().set_notify("Res1", this, &CVideoRecorder::display_mode_notification, 1);
    //    SparrowHawk().set_notify("Res2", this, &CVideoRecorder::display_mode_notification, 2);
    SparrowHawk().set_readonly("Res2", true);

    //log
    SparrowHawk().rebind("StatLogFreq", &m_log_cnt.freq);
    SparrowHawk().set_readonly("StatLogFreq");
    SparrowHawk().rebind("StatLogCnt", &m_log_cnt.cnt);
    SparrowHawk().set_readonly("StatLogCnt");
    SparrowHawk().set_setter("LogFramerate", this, &CVideoRecorder::setter_desired_framerate, 1);
    SparrowHawk().set_getter("LogFramerate", this, &CVideoRecorder::getter_desired_framerate, 1);
    SparrowHawk().set_string("LogFile", "");
    SparrowHawk().set_readonly("LogFile");
    set_loged_bytes();
    set_log_rate();
    SparrowHawk().set_readonly("StatLogedData");
    SparrowHawk().set_readonly("StatLogRate");
  }

  m_image_buf = new unsigned char[640*480*3];   //Big enough to handle all sizes
  setResolution(R320x240);

  setLogFramerate(15.0);
  setDisplayFramerate(15.0);

  setDisplay(false);

}

CVideoRecorder::~CVideoRecorder()
{
  closeCamera();      //Closes camera thread
  setDisplay(false);  //Closes windows etc.

  if(m_image_buf)
    delete []m_image_buf;
  m_image_buf=0;

  DGCdeleteMutex(&m_mutex);
  DGCdeleteMutex(&m_write_mutex);
  DGCdeleteMutex(&m_camera_mutex);
}

/*
**
** Camera handling
**
*/
void CVideoRecorder::setCamera(int num)
{
  if(m_pInternalCamera)
    delete m_pInternalCamera;

  m_pInternalCamera = new CFirewireCamera();
  if(!m_pInternalCamera->open(num)) {
    delete m_pInternalCamera;
    m_pInternalCamera=0;
  }

  setCamera(m_pInternalCamera);
}

void CVideoRecorder::setCamera(const char *uid)
{
  if(m_pInternalCamera)
    delete m_pInternalCamera;

  m_pInternalCamera = new CFirewireCamera();
  if(m_pInternalCamera->open(uid)) {
    delete m_pInternalCamera;
    m_pInternalCamera=0;
  }

  setCamera(m_pInternalCamera);
}

void CVideoRecorder::setCamera(CFirewireCamera *pCamera)
{
  if(m_pCamera)
    closeCamera();

  DGClockMutex(&m_camera_mutex);

  //Setup with this new camera
  m_pCamera = pCamera;
  if(m_pCamera) {
    m_pCamera->setWhitebalanceBU(.7);
    m_pCamera->setWhitebalanceRV(.5);
    m_whitebalance_bu = m_pCamera->whitebalanceBU();
    m_whitebalance_rv = m_pCamera->whitebalanceRV();

    //    m_pCamera->setAutoExposure(false);
    m_pCamera->setAutoExposure(true);

    //Make sure we wait for first image before we report!
    DGClockMutex(&m_first_image_mutex);
    m_first_image = true;

    DGCstartMemberFunctionThread(this, &CVideoRecorder::recorderThread);
  }
  DGCunlockMutex(&m_camera_mutex);
}

CFirewireCamera *CVideoRecorder::getCamera()
{
  return m_pCamera;
}

void CVideoRecorder::closeCamera()
{
  if(!m_pCamera)
    return;

  DGClockMutex(&m_camera_mutex);

  m_pCamera->close();
  m_pCamera=0;

  DGCunlockMutex(&m_camera_mutex);
}


/*
**
** Accessors
**
*/
int CVideoRecorder::width() const
{
  if(m_image)
    return m_image->width;
  return 0;
}

int CVideoRecorder::height() const
{
  if(m_image)
    return m_image->height;
  return 0;
}

//Get a copy of the image
void CVideoRecorder::imageCopy(IplImage *image)
{
  DGClockMutex(&m_first_image_mutex);
  DGClockMutex(&m_write_mutex);
  cvCopyImage(m_image, image);
  DGCunlockMutex(&m_write_mutex);
  DGCunlockMutex(&m_first_image_mutex);
}

//Get a copy of the b&w image
void CVideoRecorder::imageCopyBW(IplImage *image)
{
  DGClockMutex(&m_first_image_mutex);
  DGClockMutex(&m_write_mutex);
  cvCopyImage(m_imageBW, image);
  DGCunlockMutex(&m_write_mutex);
  DGCunlockMutex(&m_first_image_mutex);
}

bool CVideoRecorder::terminated() const
{
  return m_terminate;
}

double CVideoRecorder::logFramerate() const
{
  return m_log_cnt.freq;
}

double CVideoRecorder::cameraFramerate() const
{
  return m_camera_cnt.freq;
}

double CVideoRecorder::displayFramerate() const
{
  return m_display_cnt.freq;
}

long long CVideoRecorder::bytesLoged() const
{
  return m_bytes_loged.cnt;
}

double CVideoRecorder::bytesLogRate() const
{
  return m_bytes_loged.freq;
}


bool CVideoRecorder::display() const
{
  return m_screen!=NULL;
}

/*
**
** Setters
**
*/

void CVideoRecorder::setResolution(Resolution res)
{
  DGClockMutex(&m_mutex);

  //Delete old images

  if(res==R640x480)
    res=R640x480;

  //Create new images
  if(res==R320x240) {
    m_image = cvCreateImage( cvSize(320,240), IPL_DEPTH_8U, 3);
    m_imageBW = cvCreateImage( cvSize(320,240), IPL_DEPTH_8U, 1);
  } else {
    m_image = cvCreateImage( cvSize(640,480), IPL_DEPTH_8U, 3);
    m_imageBW = cvCreateImage( cvSize(640,480), IPL_DEPTH_8U, 1);
  }

  m_ROI.x=0;
  m_ROI.y=m_image->height/2;
  m_ROI.width=m_image->width;
  m_ROI.height = m_image->height - m_ROI.y;

  setDisplayNoLock(display());   //Update display 

  DGCunlockMutex(&m_mutex);

  set_display_strings();
}

void CVideoRecorder::setLogFramerate(double framerate)
{
  DGClockMutex(&m_mutex);
  if(framerate < 0.01)
    m_log_interval = 1e22;    //Enough s to never trigger :D
  else
    m_log_interval = 1/framerate;
  DGCunlockMutex(&m_mutex);
}

void CVideoRecorder::setDisplayFramerate(double framerate)
{
  DGClockMutex(&m_mutex);
  if(framerate < 0.01)
    m_display_interval = 1e22;
  else
    m_display_interval = 1/framerate;
  DGCunlockMutex(&m_mutex);
}

void CVideoRecorder::setDisplay(bool display)
{
  DGClockMutex(&m_mutex);
  setDisplayNoLock(display);
  DGCunlockMutex(&m_mutex);  

  set_display_strings();
}

//Used internally when already locked
void CVideoRecorder::setDisplayNoLock(bool display)
{
  if(display && !m_display_inited) {   //Initialize SDL
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_NOPARACHUTE) < 0) {
      return;
    }
    atexit(SDL_Quit);
    m_display_inited=true;
  }

  if(m_surf) {   //Delete display surfaces
    SDL_FreeSurface(m_surf);
    m_surf=0;
  }

  if(display && m_image) {    //Create new display surfaces
    m_screen = SDL_SetVideoMode(m_image->width, m_image->height, 
				24, SDL_DOUBLEBUF | SDL_ANYFORMAT);
    if(!m_screen) {
      return;
    }
    SDL_WM_SetCaption("Video Recorder", NULL);

    m_surf = SDL_CreateRGBSurface(SDL_SWSURFACE, m_image->width, 
		 m_image->height, 24,
                 m_screen->format->Rmask, m_screen->format->Gmask,
                 m_screen->format->Bmask, m_screen->format->Amask);
    if(!m_surf) {
      SDL_Quit();
      m_screen=0;
      return;
    }
  } else {
    if(m_screen)
      SDL_Quit();
    m_screen=0;
  }
}


/*
**
** Helper functions
**
*/

//Thread reading from camera, exposure controlling and saving to file
void CVideoRecorder::recorderThread()
{
  unsigned long long start;
  unsigned long long stop;

  double display_time = 0.0;   //"Accumulative" time between frames
  double log_time = 0.0;

  //cerr<<"Entered recorder thread"<<endl;
  DGCgettime(start);
  while(!m_terminate) {
    usleep(1000);   //Leave time when all mutexes are unlocked 

    if(!getImage())
      continue;

    //Calculate time for this loop and update log and display timers
    DGCgettime(stop);
    stop-=start;
    double time =  DGCtimetosec(stop);
    log_time += time;
    display_time += time;

    //Start timer for new loop
    DGCgettime(start);

    if(m_screen && display_time>=m_display_interval) {
      //Count down this display for acc. time
      display_time -= m_display_interval;
      if(display_time >= m_display_interval)
	display_time = 0;
      
      displayImage();
    }

    if(getLoggingEnabled() && log_time>=m_log_interval) {
      //Update accumulative time
      log_time -= m_log_interval;
      if(log_time >= m_log_interval)
	log_time = 0;
      
      logImage();
    }

    //unlock mutex
    DGCunlockMutex(&m_mutex);

    //Check if we've got input through SDL
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
      switch(event.type) {
      case SDL_QUIT:
	m_terminate=true;
	break;
      case SDL_KEYDOWN:
      case SDL_KEYUP:
	if(event.key.keysym.sym==27) {
	  m_terminate=true;
	  break;
	}
	if((event.key.keysym.mod & (KMOD_LCTRL | KMOD_RCTRL))!=0
	   && (event.key.keysym.sym=='c' || event.key.keysym.sym=='C')) {
	  m_terminate=true;
	  break;
	}
	break;
      }
    }
  }

  closeCamera();
}

bool CVideoRecorder::getImage()
{
  //Lock camera
  DGClockMutex(&m_camera_mutex);
  
  if(!m_pCamera) {
    DGCunlockMutex(&m_camera_mutex);
    return false;
  }
  
  unsigned char *buf = m_pCamera->capture();
  
  if(!buf) {
    //cerr<<"No capture buffer"<<endl;
    DGCunlockMutex(&m_camera_mutex);
    return false;
  }
  
  //Lock image
  DGClockMutex(&m_mutex);
  
  if(!m_image || !m_imageBW) {  //No image to output to
    //cerr<<"No images allocated"<<endl;
    m_pCamera->release();
    DGCunlockMutex(&m_mutex);
    return false;
  }

  DGClockMutex(&m_write_mutex);     //Write lock the images
  
  //convert to color image
  if(m_image->width==640) {
    BayerNearestNeighbor(buf, m_image_buf, 640, 480, BAYER_PATTERN_BGGR);
    cvSetData(m_image, m_image_buf, 640*3);
  } else if(m_image->width==320) {
    BayerDownsample(buf, m_image_buf, 320, 240, BAYER_PATTERN_BGGR);
    cvSetData(m_image, m_image_buf, 320*3);
  } else {   //Invalide image size
    m_pCamera->release();
    DGCunlockMutex(&m_camera_mutex);
    DGCunlockMutex(&m_mutex);
    return false;
  }
  
  m_pCamera->release();

  //convert to gray scale and do exposure control
  cvCvtColor(m_image, m_imageBW, CV_RGB2GRAY);

  DGCunlockMutex(&m_write_mutex);    //Write unlock the images

  if(m_first_image) {
    m_first_image = false;
    DGCunlockMutex(&m_first_image_mutex);
  }
  
  cvSetImageROI(m_imageBW, m_ROI);
  CvScalar s = cvAvg(m_imageBW);
  m_image_ROI_avg = s.val[0];
  
  cvResetImageROI(m_imageBW);
  s = cvAvg(m_imageBW);
  m_image_avg = s.val[0];

  //Calculate histogram over ROI
  calcBWHist(m_hist);
  if(m_hist_eq) {   //Equalize the images
    int cum[256];
    cum[0] = m_hist[0];
    for(int i=1; i<256; ++i)
      cum[i] = cum[i-1] + m_hist[i];

    //Renormalize
    for(int i=0; i<256; ++i)
      cum[i] = cum[i] * 255 / cum[255];

    //Update the image
    for(int j=0; j<m_imageBW->height; ++j) {
      for(int i=0; i<m_imageBW->width; ++i) {
	unsigned char *v = (unsigned char *)(m_imageBW->imageData +i + j*m_imageBW->widthStep);

	*v = (unsigned int)cum[ (int) *v ];
      }
    }

    cvCvtColor(m_imageBW, m_image, CV_GRAY2RGB);
  }
  
  m_exposure_level = m_pCamera->exposure();
  double expose = m_exposure_level + m_exposure_pid.step_forward((128.0-m_image_ROI_avg)/128.0);

  if(m_auto_exposure) {
    m_pCamera->setExposure(expose);
  }
  
  DGCunlockMutex(&m_camera_mutex);
  ++m_camera_cnt;

  return true;
}

void CVideoRecorder::displayImage()
{
  //Update screen
  if(SDL_LockSurface(m_surf)==0) {
    //cerr<<"Surface locked"<<endl;
    memcpy(m_surf->pixels, m_image->imageData, m_image->width*m_image->height*3);

    if(m_show_ROI && m_ROI.x>=0 && m_ROI.y>=0 &&
       m_ROI.x+m_ROI.width<=m_image->width && m_ROI.y+m_ROI.height<=m_image->height) {

      //Display the ROI
      char *d1 = (char*)m_surf->pixels + (m_ROI.x + m_image->width*m_ROI.y)*3;
      char *d2 = (char*)m_surf->pixels + (m_ROI.x + m_image->width*(m_ROI.y+m_ROI.height-1))*3;
      for(int i=0; i<m_ROI.width; ++i, d1+=3, d2+=3) {
	d1[0] = d2[0] = 0;
	d1[1] = d2[1] = 240;
	d1[2] = d2[2] = 0;	
      }

      d1 = (char*)m_surf->pixels + (m_ROI.x + m_image->width*m_ROI.y)*3;
      d2 = (char*)m_surf->pixels + ((m_ROI.x+m_ROI.width-1) + m_image->width*m_ROI.y)*3;
      for(int i=0; i<m_ROI.height; ++i, d1+=3*m_image->width, d2+=3*m_image->width) {
	d1[0] = d2[0] = 0;
	d1[1] = d2[1] = 240;
	d1[2] = d2[2] = 0;	
      }
    }

    if(m_show_hist && m_ROI.x>=0 && m_ROI.y>=0 &&
       m_ROI.x+m_ROI.width<=m_image->width && m_ROI.y+m_ROI.height<=m_image->height) {

      //Display the orig. hist on the bottom of the display
      int m=m_hist[0];
      for(int i=0; i<256; ++i)
	if(m_hist[i]>m)
	  m=m_hist[i];

      for(int i=0; i<256; ++i) {
	int h = m_image->height/2 * m_hist[i]/m;
	for(int j=m_image->height-1-h; j<m_image->height-1; ++j) {
	  ((char*)m_surf->pixels)[ (i+j*m_image->width)*3 +0] *= 0.5;
	  ((char*)m_surf->pixels)[ (i+j*m_image->width)*3 +1] *= 0.5;
	  ((char*)m_surf->pixels)[ (i+j*m_image->width)*3 +2] *= 250;
	}

      }

      /*
      //Display the eq curve
      int cum[256];
      cum[0] = m_hist[0];
      for(int i=1; i<256; ++i)
	cum[i] = cum[i-1] + m_hist[i];
      
      //Renormalize
      for(int k=0; k<256; ++k) {
	cum[k] = cum[k] * 256 / cum[255];

	int i = k;
	int j = 256-cum[k];
	((char*)m_surf->pixels)[ (i+j*m_image->width)*3 +0] *= 0.5;
	((char*)m_surf->pixels)[ (i+j*m_image->width)*3 +1] *= 0.5;
	((char*)m_surf->pixels)[ (i+j*m_image->width)*3 +2] *= 250;
      }
      */
      
      //Display the new hist on the bottom of the display
      //Calculate and show the histogram
      int hist[256];

      calcBWHist(hist);

      m=hist[0];
      for(int i=0; i<256; ++i)
	if(hist[i]>m)
	  m=hist[i];

      for(int i=0; i<256; ++i) {
	int h = m_image->height/2 * hist[i]/m;
	for(int j=m_image->height/2-h; j<m_image->height/2; ++j) {
	  ((char*)m_surf->pixels)[ (i+j*m_image->width)*3 +0] *= 0.5;
	  ((char*)m_surf->pixels)[ (i+j*m_image->width)*3 +1] *= 0.5;
	  ((char*)m_surf->pixels)[ (i+j*m_image->width)*3 +2] *= 250;
	}

      }
      

    }

    SDL_UnlockSurface(m_surf);
    
    SDL_Rect rc;
    rc.x=0;
    rc.y=0;
    rc.w = m_image->width;
    rc.h = m_image->height;
    if(SDL_BlitSurface(m_surf, &rc, m_screen, &rc) == 0) {
      SDL_Flip(m_screen);
      //cerr<<"Flipped"<<endl;
    }
    
    ++m_display_cnt;
  }
}

void CVideoRecorder::logImage()
{
  string dir = getLogDir();
  if(checkNewDirAndReset()) {
    //Reopen log index file, and reset counter
    if(m_log_index_file)
      fclose(m_log_index_file);
    string file = dir + "index.log";
    m_log_index_file = fopen(file.c_str(), "w");
    
    m_bytes_loged.clear();
    
    //Write header
    fprintf(m_log_index_file, "#Index file video recorder\n");
    fprintf(m_log_index_file, "#Format:  timestamp imagefile\n");
  }
  
  //Write image
  static char file[1024];
  snprintf(file, sizeof(file), "img%8.8i.jpg", m_log_cnt.cnt);
  
  if(m_log_index_file) {
    unsigned long long time;
    DGCgettime(time);

    fprintf(m_log_index_file, "%lli %s\n", time, file);
    fflush(m_log_index_file);
  }

  string f = dir + file;
  cvSaveImage(f.c_str(), m_image);

  struct stat s;
  if(stat(f.c_str(), &s)==0)
    m_bytes_loged += (long long)s.st_size;

  set_loged_bytes();
  set_log_rate();

  ++m_log_cnt;
}


//Calculate the histogram over the ROI in the BW image
void CVideoRecorder::calcBWHist(int *hist)
{
  for(int i=0; i<256; ++i)
    hist[i] = 0;

  int x0 = m_ROI.x;
  int x1 = m_ROI.x+m_ROI.width-1;
  int y0 = m_ROI.y;
  int y1 = m_ROI.y+m_ROI.height-1;

  if(x0<0)
    x0=0;
  if(x1>=m_imageBW->width)
    x1=m_imageBW->width;
  if(y0<0)
    y0=0;
  if(y1>=m_imageBW->height)
    y1=m_imageBW->height;

  unsigned char *data = (unsigned char *)m_imageBW->imageData + x0 + y0*m_imageBW->widthStep;
  
  for(int j=y0; j<y1; ++j, data+=m_imageBW->widthStep-(x1-x0)) {
    for(int i=x0; i<x1; ++i, ++data) {
      ++hist[ *data ];
    }
  }
}


/*
**
** Sparrow interface functions
**
*/
void CVideoRecorder::set_display_strings()
{
  if(!display())
    SparrowHawk().set_string("ResNone", "* None");
  else
    SparrowHawk().set_string("ResNone", "  None");
  if(display() && width()==320)
    SparrowHawk().set_string("Res1", "* 320x240");
  else
    SparrowHawk().set_string("Res1", "  320x240");
  //if(display() && width()==640)
  //  SparrowHawk().set_string("Res2", "* 640x480");
  //else
  //  SparrowHawk().set_string("Res2", "  640x480");    
}

void CVideoRecorder::set_loged_bytes()
{
  long long b = bytesLoged();
  static char buf[100];

  if(b<1000) {  //b
    sprintf(buf, "%i b    ", (int)b);
  } else if(b<1000000) {       //kb
    sprintf(buf, "%.1f kb    ", b/1000.0);
  } else if(b<1000000000) {    //Mb
    sprintf(buf, "%.1f Mb    ", b/1000000.0);
  } else if(b<1000000000000ll) { //Gb
    sprintf(buf, "%.1f Gb    ", b/1000000000.0);
  } else { //Tb
    sprintf(buf, "%.1f Tb   ", b/1000000000000.0);
  }
  SparrowHawk().set_string("StatLogedData", buf);
}

void CVideoRecorder::set_log_rate()
{
  double b = bytesLogRate();
  static char buf[100];

  if(b<1000) {  //b
    sprintf(buf, "%i b/s   ", (int)b);
  } else if(b<1000000) {       //kb
    sprintf(buf, "%.1f kb/s    ", b/1000.0);
  } else if(b<1000000000) {    //Mb
    sprintf(buf, "%.1f Mb/s    ", b/1000000.0);
  } else if(b<1000000000000ll) { //Gb
    sprintf(buf, "%.1f Gb/s    ", b/1000000000.0);
  } else { //Tb
    sprintf(buf, "%.1f Tb/s   ", b/1000000000000.0);
  }
  SparrowHawk().set_string("StatLogRate", buf);
}

double CVideoRecorder::getter_desired_framerate(int type)
{
  double interval=0.0;
  if(type==0)
    interval = m_display_interval;
  if(type==1)
    interval = m_log_interval;

  if(interval<1e-2)
    interval = 1e-2;
  if(interval>1e10)
    return 0.0;
  return 1/interval;
}

void CVideoRecorder::setter_desired_framerate(double *p, double c, int type)
{
  if(type==0)
    setDisplayFramerate(c);
  if(type==1)
    setLogFramerate(c);  
}

void CVideoRecorder::setter_exposure(double *p, double v)
{
  if(v==-1) {  //Set auto
    m_auto_exposure=true;
  } else {
    DGClockMutex(&m_camera_mutex);
    if(m_pCamera) {
      m_auto_exposure=false;
      m_pCamera->setExposure(v);
    }
    DGCunlockMutex(&m_camera_mutex);
  }
}

void CVideoRecorder::setter_whitebalance_bu(double *p, double v)
{
  DGClockMutex(&m_camera_mutex);
  if(m_pCamera) {
    m_pCamera->setWhitebalanceBU(v);
    m_whitebalance_bu = m_pCamera->whitebalanceBU();
  }
  DGCunlockMutex(&m_camera_mutex);
}

void CVideoRecorder::setter_whitebalance_rv(double *p, double v)
{
  DGClockMutex(&m_camera_mutex);
  if(m_pCamera) {
    m_pCamera->setWhitebalanceRV(v);
    m_whitebalance_rv = m_pCamera->whitebalanceRV();
  }
  DGCunlockMutex(&m_camera_mutex);
}

void CVideoRecorder::setter_show_hist(bool *p, bool v)
{
  m_show_hist = v;
}


void CVideoRecorder::display_mode_notification(int mode)
{
  //return;
  if(mode==0)
    setDisplay(false);
  if(mode==1) {
    setResolution(R320x240);
    setDisplay(true);
  }
  if(mode==2) {
    setResolution(R640x480);
    setDisplay(true);
  }
}
