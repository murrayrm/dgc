#ifndef _CTRAJPAINTER_HH_
#define _CTRAJPAINTER_HH_

#include "CMap.hh"
#include "traj.h"

/**
   @brief A utility class to write trajectories into the cost map.

   CTrajPainter takes a target map and layer, as input, and a trajectory
   to write from.  It writes that trajectory into the map as a series
   of cell-width line segments that travel from trajectory point to 
   trajectory point.  These segments are blurred to either side so as to 
   give the planning software something other than a thin line to optimize
   on.

*/

//class CCostFuser;    //Forward declaration
class CCostFuser;
class FusionMapperOptions;  //Forward declaration

class CTrajPainter {
public:
  /** Creates a CTrajPainter object. */
  CTrajPainter();

  /** Removes a CTrajPainter object. */
  ~CTrajPainter();
  
  /** 
      Paints a trajectory into the specified map.
      
      @param cmap A pointer to the map to be painted into.
      @param layer The layer to be painted into.
      @param pTraj A pointer to the trajectory to be painted.
      @param blurWidth The amount to blur the trajectory in the map; default is 6 meters.
      @param useDeltas Whether to use deltas or not for communication.
  */
  int PaintTraj(CMap *cmap, int layer, CTraj *pTraj, double blurWidth = 6.0, bool useDeltas = true);

  void SetCostPainter(CCostFuser *pFuser=NULL, FusionMapperOptions *pOpt=NULL) { costFuser = pFuser; fusionOptions = pOpt; }
private:
  int DrawSegment(NEcoord map1, NEcoord map2, NEcoord traj1, NEcoord traj2, double speed, double width);
  int LineCross(NEcoord map1, NEcoord map2, NEcoord traj1, NEcoord traj2);
  int PaintBlurredPoint(double N, double E, double dy, double dx, double width, double speed);
  int isPointInBox(NEcoord point, NEcoord map1, NEcoord map2);

  void updatePoint(double N, double E);

  // Misc inlines
  inline int CTrajPainter::between(double middle, double left, double right) {
    return ((middle > left) && (middle < right)) ? true : false;
  }

  inline double CTrajPainter::distance(NEcoord a, NEcoord b) {
    return sqrt((a.N-b.N)*(a.N-b.N) + (a.E-b.E)*(a.E-b.E));
  }

  CMap *trajMap;
  int trajLayer;
  bool useDeltasFlag;

  CCostFuser *costFuser;
  FusionMapperOptions *fusionOptions;
};

#endif
