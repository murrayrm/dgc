/** @file mat5.c
 * Matlab MAT version 5 file functions
 * @ingroup MAT
 */
/*
 * $Revision: 1.1.2.11 $ $State: Exp $
 * $Date: 2005/08/23 19:58:59 $
 */
/*
 * Copyright (C) 2005   Christopher C. Hulbert
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/* FIXME: Implement Unicode support */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <matio.h>

/* #ifndef HAVE_ZLIB */
/* #define HAVE_ZLIB */
/* #endif */

#ifdef HAVE_ZLIB
#include <zlib.h>
#endif

static const char *class_type_desc[16] = {"Undefined","Cell Array","Structure",
       "Object","Character Array","Sparse Array","Double Precision Array",
       "Single Precision Array", "8-bit, signed Integer Array",
       "8-bit, Unsigned Integer Array","16-bit, signed Integer Array",
       "16-bit, unsigned Integer Array","32-bit, signed Integer Array",
       "32-bit, unsigned Integer Array","Matlab Array","Compressed Data"};
static const char *data_type_desc[23] = {"Unknown","8-bit, signed integer",
       "8-bit, unsigned integer","16-bit, signed integer",
       "16-bit, unsigned integer","32-bit, signed integer",
       "32-bit, unsigned integer","IEEE 754 single-precision","RESERVED",
       "IEEE 754 double-precision","RESERVED","RESERVED",
       "64-bit, signed integer","64-bit, unsigned integer", "Matlab Array",
       "Compressed Data","Unicode UTF-8 Encoded Character Data",
       "Unicode UTF-16 Encoded Character Data",
       "Unicode UTF-32 Encoded Character Data","","String","Cell Array",
       "Structure"};

/*
 * -------------------------------------------------------------
 *   Private Functions
 * -------------------------------------------------------------
 */

/*----------- Functions to read data into type-specified arrays -----------*/
static int ReadDoubleData(mat_t *mat,double  *data,int data_type,int len);
static int ReadSingleData(mat_t *mat,float   *data,int data_type,int len);
static int ReadInt32Data (mat_t *mat,mat_int32_t *data,int data_type,int len);
static int ReadInt16Data (mat_t *mat,mat_int16_t *data,int data_type,int len);
static int ReadInt8Data  (mat_t *mat,mat_int8_t  *data,int data_type,int len);
static int ReadCompressedDoubleData(mat_t *mat,z_stream *z,double  *data,
               int data_type,int len);
static int ReadCompressedSingleData(mat_t *mat,z_stream *z,float   *data,
               int data_type,int len);
static int ReadCompressedInt32Data (mat_t *mat,z_stream *z,mat_int32_t *data,
               int data_type,int len);
static int ReadCompressedInt16Data (mat_t *mat,z_stream *z,mat_int16_t *data,
               int data_type,int len);
static int ReadCompressedInt8Data  (mat_t *mat,z_stream *z,mat_int8_t  *data,
               int data_type,int len);
/*-------------------------------------------------------------------------*/

/*----------- Functions to do partial I/O (slabs) ------------------------*/
static int ReadDataSlabN(mat_t *mat,void *data,int class_type,
               int data_type,int rank,int *dims,int *start,int *stride,
               int *edge);
static int ReadDataSlab2(mat_t *mat,void *data,int class_type,
               int data_type,int *dims,int *start,int *stride,int *edge);
static int ReadCompressedDataSlabN(mat_t *mat,z_stream *z,void *data,
               int class_type,int data_type,int rank,int *dims,int *start,
               int *stride,int *edge);
static int ReadCompressedDataSlab2(mat_t *mat,z_stream *z,void *data,
               int class_type,int data_type,int *dims,int *start,int *stride,
               int *edge);
/*-------------------------------------------------------------------------*/

static int WriteCellArrayField( mat_t *mat, matvar_t *matvar, 
                                int compress );
static int WriteStructField( mat_t *mat, matvar_t *matvar, 
                             int compress );
static int ReadNextStructField( mat_t *mat, matvar_t *matvar );
static int ReadNextCell( mat_t *mat, matvar_t *matvar );
static int WriteEmptyCharData(mat_t *mat, int N, int data_type);

/*
 *-------------------------------------------------------------------
 *
 *-------------------------------------------------------------------
 */

static int GetMatrixMaxBufSize( matvar_t *mat )
{
    int    nBytes = 0, data_size, tag_size = 8, array_flags_size = 8;
    int    len, nmemb = 1, i;

    if ( mat == NULL )
        return 0;

    /* Add the MAT_T_Matrix tag, Array Flags tag and space to the number of bytes */
    nBytes += tag_size + tag_size + array_flags_size;

    /* Get size of variable name, pad it to an 8 byte block, and add it to nBytes */
    if ( mat->name )
        len = strlen(mat->name);
    else
        len=8;
    if ( len % 8 == 0 )
        i = len;
    else
        len = len + 8 - len % 8;
    nBytes += tag_size + len;

    /* Get size of rank and dimensions, pad it to an 8 byte block, and add it to nBytes */
    for ( i = 0, len = 0; i < mat->rank; i++ )
        nmemb *= mat->dims[i];
    len = nmemb+nmemb % 2;
    nBytes += tag_size + len;

    switch ( mat->class_type ) {
        case MAT_C_DOUBLE:
            data_size = nmemb*8;
            nBytes += tag_size + data_size;
            if ( mat->isComplex )
                nBytes += tag_size + data_size;
            break;
        case MAT_C_SINGLE:
        case MAT_C_INT32:
        case MAT_C_UINT32:
            data_size = nmemb*4;
            nBytes += tag_size + data_size;
            if ( mat->isComplex )
                nBytes += tag_size + data_size;
            break;
        case MAT_C_INT16:
        case MAT_C_UINT16:
            data_size = nmemb*2;
            nBytes += tag_size + data_size;
            if ( mat->isComplex )
                nBytes += tag_size + data_size;
            break;
        case MAT_C_INT8:
        case MAT_C_UINT8:
        case MAT_C_CHAR:
            data_size = nmemb;
            nBytes += tag_size + data_size;
            if ( mat->isComplex )
                nBytes += tag_size + data_size;
            break;
    }
    
    return nBytes;
}

/*
 * --------------------------------------------------------------------------
 *    Routines to read data of any type into arrays of a specific type
 * --------------------------------------------------------------------------
 */

static int
ReadDoubleData(mat_t *mat,double *data,int data_type,int len)
{
    int bytesread = 0, data_size = 0, i;

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d;

            data_size = sizeof(double);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&d,data_size,1,mat->fp);
                    data[i] = doubleSwap(&d);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&d,data_size,1,mat->fp);
                    data[i] = d;
                }
            }
            break;
        }
        case MAT_T_SINGLE:
        {
            float f;

            data_size = sizeof(float);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&f,data_size,1,mat->fp);
                    data[i] = floatSwap(&f);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&f,data_size,1,mat->fp);
                    data[i] = f;
                }
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32;

            data_size = sizeof(mat_int32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i32,data_size,1,mat->fp);
                    data[i] = int32Swap(&i32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i32,data_size,1,mat->fp);
                    data[i] = i32;
                }
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32;

            data_size = sizeof(mat_uint32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui32,data_size,1,mat->fp);
                    data[i] = int32Swap(&ui32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui32,data_size,1,mat->fp);
                    data[i] = ui32;
                }
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16;

            data_size = sizeof(mat_int16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,data_size,1,mat->fp);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,data_size,1,mat->fp);
                    data[i] = i16;
                }
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16;

            data_size = sizeof(mat_uint16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui16,data_size,1,mat->fp);
                    data[i] = int16Swap(&ui16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui16,data_size,1,mat->fp);
                    data[i] = ui16;
                }
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8;

            data_size = sizeof(mat_int8_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i8,data_size,1,mat->fp);
                    data[i] = i8;
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i8,data_size,1,mat->fp);
                    data[i] = i8;
                }
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8;

            data_size = sizeof(mat_uint8_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui8,data_size,1,mat->fp);
                    data[i] = ui8;
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui8,data_size,1,mat->fp);
                    data[i] = ui8;
                }
            }
            break;
        }
    }
    bytesread *= data_size;
    return bytesread;
}

/*
 * Converts data of any type to data in double precision format
 */
static int
ReadCompressedDoubleData(mat_t *mat,z_stream *z,double *data,
    int data_type,int len)
{
    int nBytes = 0, data_size = 0, i;

#ifdef HAVE_ZLIB
    if ( (mat == NULL) || (data == NULL) || (z == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d;

            data_size = sizeof(double);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&d,data_size);
                    data[i] = doubleSwap(&d);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,data+i,data_size);
                }
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32;

            data_size = sizeof(mat_int32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i32,data_size);
                    data[i] = int32Swap(&i32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i32,data_size);
                    data[i] = i32;
                }
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32;

            data_size = sizeof(mat_uint32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui32,data_size);
                    data[i] = int32Swap(&ui32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui32,data_size);
                    data[i] = ui32;
                }
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16;

            data_size = sizeof(mat_int16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = i16;
                }
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16;

            data_size = sizeof(mat_uint16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui16,data_size);
                    data[i] = int16Swap(&ui16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui16,data_size);
                    data[i] = ui16;
                }
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8;

            data_size = sizeof(mat_uint8_t);
            for ( i = 0; i < len; i++ ) {
                InflateData(mat,z,&ui8,data_size);
                data[i] = ui8;
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8;

            data_size = sizeof(mat_int8_t);
            for ( i = 0; i < len; i++ ) {
                InflateData(mat,z,&i8,data_size);
                data[i] = i8;
            }
            break;
        }
    }
    nBytes = len*data_size;
#else
		Mat_Critical("Not compiled with zlib support");
#endif
    return nBytes;
}

static int
ReadSingleData(mat_t *mat,float *data,int data_type,int len)
{
    int bytesread = 0, data_size = 0, i;

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d;

            data_size = sizeof(double);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&d,data_size,1,mat->fp);
                    data[i] = doubleSwap(&d);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&d,data_size,1,mat->fp);
                    data[i] = d;
                }
            }
            break;
        }
        case MAT_T_SINGLE:
        {
            float f;

            data_size = sizeof(float);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&f,data_size,1,mat->fp);
                    data[i] = floatSwap(&f);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&f,data_size,1,mat->fp);
                    data[i] = f;
                }
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32;

            data_size = sizeof(mat_int32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i32,data_size,1,mat->fp);
                    data[i] = int32Swap(&i32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i32,data_size,1,mat->fp);
                    data[i] = i32;
                }
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32;

            data_size = sizeof(mat_uint32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui32,data_size,1,mat->fp);
                    data[i] = int32Swap(&ui32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui32,data_size,1,mat->fp);
                    data[i] = ui32;
                }
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16;

            data_size = sizeof(mat_int16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,data_size,1,mat->fp);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,data_size,1,mat->fp);
                    data[i] = i16;
                }
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16;

            data_size = sizeof(mat_uint16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui16,data_size,1,mat->fp);
                    data[i] = int16Swap(&ui16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui16,data_size,1,mat->fp);
                    data[i] = ui16;
                }
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8;

            data_size = sizeof(mat_int8_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i8,data_size,1,mat->fp);
                    data[i] = i8;
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i8,data_size,1,mat->fp);
                    data[i] = i8;
                }
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8;

            data_size = sizeof(mat_uint8_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui8,data_size,1,mat->fp);
                    data[i] = ui8;
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui8,data_size,1,mat->fp);
                    data[i] = ui8;
                }
            }
            break;
        }
    }
    bytesread *= data_size;
    return bytesread;
}

static int
ReadCompressedSingleData(mat_t *mat,z_stream *z,float *data,
    int data_type,int len)
{
    int nBytes = 0, data_size = 0, i;

#ifdef HAVE_ZLIB
    if ( (mat == NULL) || (data == NULL) || (z == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d;

            data_size = sizeof(double);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&d,data_size);
                    data[i] = doubleSwap(&d);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&d,data_size);
                    data[i] = d;
                }
            }
            break;
        }
        case MAT_T_SINGLE:
        {
            float f;

            data_size = sizeof(float);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&f,data_size);
                    data[i] = floatSwap(&f);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,data+i,data_size);
                }
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32;

            data_size = sizeof(mat_int32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i32,data_size);
                    data[i] = int32Swap(&i32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i32,data_size);
                    data[i] = i32;
                }
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32;

            data_size = sizeof(mat_uint32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui32,data_size);
                    data[i] = int32Swap(&ui32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui32,data_size);
                    data[i] = ui32;
                }
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16;

            data_size = sizeof(mat_int16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = i16;
                }
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16;

            data_size = sizeof(mat_uint16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui16,data_size);
                    data[i] = int16Swap(&ui16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui16,data_size);
                    data[i] = ui16;
                }
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8;

            data_size = sizeof(mat_uint8_t);
            for ( i = 0; i < len; i++ ) {
                InflateData(mat,z,&ui8,data_size);
                data[i] = ui8;
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8;

            data_size = sizeof(mat_int8_t);
            for ( i = 0; i < len; i++ ) {
                InflateData(mat,z,&i8,data_size);
                data[i] = i8;
            }
            break;
        }
    }
    nBytes = len*data_size;
#else
		Mat_Critical("Not compiled with zlib support");
#endif
    return nBytes;
}

static int
ReadInt32Data(mat_t *mat,mat_int32_t *data,int data_type,int len)
{
    int bytesread = 0, data_size = 0, i;

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d;

            data_size = sizeof(double);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&d,data_size,1,mat->fp);
                    data[i] = doubleSwap(&d);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&d,data_size,1,mat->fp);
                    data[i] = d;
                }
            }
            break;
        }
        case MAT_T_SINGLE:
        {
            float f;

            data_size = sizeof(float);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&f,data_size,1,mat->fp);
                    data[i] = floatSwap(&f);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&f,data_size,1,mat->fp);
                    data[i] = f;
                }
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32;

            data_size = sizeof(mat_int32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i32,data_size,1,mat->fp);
                    data[i] = int32Swap(&i32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i32,data_size,1,mat->fp);
                    data[i] = i32;
                }
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32;

            data_size = sizeof(mat_uint32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui32,data_size,1,mat->fp);
                    data[i] = int32Swap(&ui32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui32,data_size,1,mat->fp);
                    data[i] = ui32;
                }
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16;

            data_size = sizeof(mat_int16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,data_size,1,mat->fp);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,data_size,1,mat->fp);
                    data[i] = i16;
                }
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16;

            data_size = sizeof(mat_uint16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui16,data_size,1,mat->fp);
                    data[i] = int16Swap(&ui16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui16,data_size,1,mat->fp);
                    data[i] = ui16;
                }
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8;

            data_size = sizeof(mat_int8_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i8,data_size,1,mat->fp);
                    data[i] = i8;
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i8,data_size,1,mat->fp);
                    data[i] = i8;
                }
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8;

            data_size = sizeof(mat_uint8_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui8,data_size,1,mat->fp);
                    data[i] = ui8;
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui8,data_size,1,mat->fp);
                    data[i] = ui8;
                }
            }
            break;
        }
    }
    bytesread *= data_size;
    return bytesread;
}

static int
ReadCompressedInt32Data(mat_t *mat,z_stream *z,mat_int32_t *data,
    int data_type,int len)
{
    int nBytes = 0, data_size = 0, i;

#ifdef HAVE_ZLIB
    if ( (mat == NULL) || (data == NULL) || (z == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d;

            data_size = sizeof(double);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&d,data_size);
                    data[i] = doubleSwap(&d);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&d,data_size);
                    data[i] = d;
                }
            }
            break;
        }
        case MAT_T_SINGLE:
        {
            float f;

            data_size = sizeof(float);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&f,data_size);
                    data[i] = floatSwap(&f);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&f,data_size);
                    data[i] = f;
                }
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32;

            data_size = sizeof(mat_int32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i32,data_size);
                    data[i] = int32Swap(&i32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i32,data_size);
                    data[i] = i32;
                }
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32;

            data_size = sizeof(mat_uint32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui32,data_size);
                    data[i] = int32Swap(&ui32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui32,data_size);
                    data[i] = ui32;
                }
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16;

            data_size = sizeof(mat_int16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = i16;
                }
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16;

            data_size = sizeof(mat_uint16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui16,data_size);
                    data[i] = int16Swap(&ui16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui16,data_size);
                    data[i] = ui16;
                }
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8;

            data_size = sizeof(mat_uint8_t);
            for ( i = 0; i < len; i++ ) {
                InflateData(mat,z,&ui8,data_size);
                data[i] = ui8;
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8;

            data_size = sizeof(mat_int8_t);
            for ( i = 0; i < len; i++ ) {
                InflateData(mat,z,&i8,data_size);
                data[i] = i8;
            }
            break;
        }
    }
    nBytes = len*data_size;
#else
		Mat_Critical("Not compiled with zlib support");
#endif
    return nBytes;
}

static int
ReadInt16Data(mat_t *mat,mat_int16_t *data,int data_type,int len)
{
    int bytesread = 0, data_size = 0, i;

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d;

            data_size = sizeof(double);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&d,data_size,1,mat->fp);
                    data[i] = doubleSwap(&d);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&d,data_size,1,mat->fp);
                    data[i] = d;
                }
            }
            break;
        }
        case MAT_T_SINGLE:
        {
            float f;

            data_size = sizeof(float);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&f,data_size,1,mat->fp);
                    data[i] = floatSwap(&f);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&f,data_size,1,mat->fp);
                    data[i] = f;
                }
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32;

            data_size = sizeof(mat_int32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i32,data_size,1,mat->fp);
                    data[i] = int32Swap(&i32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i32,data_size,1,mat->fp);
                    data[i] = i32;
                }
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32;

            data_size = sizeof(mat_uint32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui32,data_size,1,mat->fp);
                    data[i] = int32Swap(&ui32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui32,data_size,1,mat->fp);
                    data[i] = ui32;
                }
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16;

            data_size = sizeof(mat_int16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,data_size,1,mat->fp);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,data_size,1,mat->fp);
                    data[i] = i16;
                }
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16;

            data_size = sizeof(mat_uint16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui16,data_size,1,mat->fp);
                    data[i] = int16Swap(&ui16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui16,data_size,1,mat->fp);
                    data[i] = ui16;
                }
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8;

            data_size = sizeof(mat_int8_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i8,data_size,1,mat->fp);
                    data[i] = i8;
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i8,data_size,1,mat->fp);
                    data[i] = i8;
                }
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8;

            data_size = sizeof(mat_uint8_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui8,data_size,1,mat->fp);
                    data[i] = ui8;
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui8,data_size,1,mat->fp);
                    data[i] = ui8;
                }
            }
            break;
        }
    }
    bytesread *= data_size;
    return bytesread;
}

static int
ReadCompressedInt16Data(mat_t *mat,z_stream *z,mat_int16_t *data,
    int data_type,int len)
{
    int nBytes = 0, data_size = 0, i;

#ifdef HAVE_ZLIB
    if ( (mat == NULL) || (data == NULL) || (z == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d;

            data_size = sizeof(double);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&d,data_size);
                    data[i] = doubleSwap(&d);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&d,data_size);
                    data[i] = d;
                }
            }
            break;
        }
        case MAT_T_SINGLE:
        {
            float f;

            data_size = sizeof(float);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&f,data_size);
                    data[i] = floatSwap(&f);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&f,data_size);
                    data[i] = f;
                }
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32;

            data_size = sizeof(mat_int32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i32,data_size);
                    data[i] = int32Swap(&i32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i32,data_size);
                    data[i] = i32;
                }
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32;

            data_size = sizeof(mat_uint32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui32,data_size);
                    data[i] = int32Swap(&ui32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui32,data_size);
                    data[i] = ui32;
                }
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16;

            data_size = sizeof(mat_int16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = i16;
                }
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16;

            data_size = sizeof(mat_uint16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui16,data_size);
                    data[i] = int16Swap(&ui16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui16,data_size);
                    data[i] = ui16;
                }
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8;

            data_size = sizeof(mat_uint8_t);
            for ( i = 0; i < len; i++ ) {
                InflateData(mat,z,&ui8,data_size);
                data[i] = ui8;
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8;

            data_size = sizeof(mat_int8_t);
            for ( i = 0; i < len; i++ ) {
                InflateData(mat,z,&i8,data_size);
                data[i] = i8;
            }
            break;
        }
    }
    nBytes = len*data_size;
#else
		Mat_Critical("Not compiled with zlib support");
#endif
    return nBytes;
}

static int
ReadInt8Data(mat_t *mat,mat_int8_t *data,int data_type,int len)
{
    int bytesread = 0, data_size = 0, i;

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d;

            data_size = sizeof(double);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&d,data_size,1,mat->fp);
                    data[i] = doubleSwap(&d);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&d,data_size,1,mat->fp);
                    data[i] = d;
                }
            }
            break;
        }
        case MAT_T_SINGLE:
        {
            float f;

            data_size = sizeof(float);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&f,data_size,1,mat->fp);
                    data[i] = floatSwap(&f);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&f,data_size,1,mat->fp);
                    data[i] = f;
                }
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32;

            data_size = sizeof(mat_int32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i32,data_size,1,mat->fp);
                    data[i] = int32Swap(&i32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i32,data_size,1,mat->fp);
                    data[i] = i32;
                }
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32;

            data_size = sizeof(mat_uint32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui32,data_size,1,mat->fp);
                    data[i] = int32Swap(&ui32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui32,data_size,1,mat->fp);
                    data[i] = ui32;
                }
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16;

            data_size = sizeof(mat_int16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,data_size,1,mat->fp);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,data_size,1,mat->fp);
                    data[i] = i16;
                }
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16;

            data_size = sizeof(mat_uint16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui16,data_size,1,mat->fp);
                    data[i] = int16Swap(&ui16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui16,data_size,1,mat->fp);
                    data[i] = ui16;
                }
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8;

            data_size = sizeof(mat_int8_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i8,data_size,1,mat->fp);
                    data[i] = i8;
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i8,data_size,1,mat->fp);
                    data[i] = i8;
                }
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8;

            data_size = sizeof(mat_uint8_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui8,data_size,1,mat->fp);
                    data[i] = ui8;
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&ui8,data_size,1,mat->fp);
                    data[i] = ui8;
                }
            }
            break;
        }
    }
    bytesread *= data_size;
    return bytesread;
}

static int
ReadCompressedInt8Data(mat_t *mat,z_stream *z,mat_int8_t *data,
    int data_type,int len)
{
    int nBytes = 0, data_size = 0, i;

#ifdef HAVE_ZLIB
    if ( (mat == NULL) || (data == NULL) || (z == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d;

            data_size = sizeof(double);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&d,data_size);
                    data[i] = doubleSwap(&d);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&d,data_size);
                    data[i] = d;
                }
            }
            break;
        }
        case MAT_T_SINGLE:
        {
            float f;

            data_size = sizeof(float);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&f,data_size);
                    data[i] = floatSwap(&f);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&f,data_size);
                    data[i] = f;
                }
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32;

            data_size = sizeof(mat_int32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i32,data_size);
                    data[i] = int32Swap(&i32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i32,data_size);
                    data[i] = i32;
                }
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32;

            data_size = sizeof(mat_uint32_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui32,data_size);
                    data[i] = int32Swap(&ui32);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui32,data_size);
                    data[i] = ui32;
                }
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16;

            data_size = sizeof(mat_int16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = i16;
                }
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16;

            data_size = sizeof(mat_uint16_t);
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui16,data_size);
                    data[i] = int16Swap(&ui16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&ui16,data_size);
                    data[i] = ui16;
                }
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8;

            data_size = sizeof(mat_uint8_t);
            for ( i = 0; i < len; i++ ) {
                InflateData(mat,z,&ui8,data_size);
                data[i] = ui8;
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8;

            data_size = sizeof(mat_int8_t);
            for ( i = 0; i < len; i++ ) {
                InflateData(mat,z,&i8,data_size);
                data[i] = i8;
            }
            break;
        }
    }
    nBytes = len*data_size;
#else
		Mat_Critical("Not compiled with zlib support");
#endif
    return nBytes;
}

static int
ReadCompressedCharData(mat_t *mat,z_stream *z,char *data,int data_type,int len)
{
    int nBytes = 0, data_size = 0, i;

#ifdef HAVE_ZLIB
    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_UTF8:
            data_size = 1;
            for ( i = 0; i < len; i++ )
                InflateData(mat,z,data+i,data_size);
            break;
        case MAT_T_INT8:
        case MAT_T_UINT8:
            data_size = 1;
            for ( i = 0; i < len; i++ )
                InflateData(mat,z,data+i,data_size);
            break;
        case MAT_T_INT16:
        case MAT_T_UINT16:
        {
            mat_uint16_t i16;

            data_size = 2;
            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    InflateData(mat,z,&i16,data_size);
                    data[i] = i16;
                }
            }
            break;
        }
        default:
            printf("Character data not supported type: %d",data_type);
            break;
    }
    nBytes = len*data_size;
#else
		Mat_Critical("Not compiled with zlib support");
#endif
    return nBytes;
}

static int
ReadCharData(mat_t *mat,char *data,int data_type,int len)
{
    int bytesread = 0, data_size = 0, i;

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_UTF8:
            for ( i = 0; i < len; i++ )
                bytesread += fread(data+i,1,1,mat->fp);
            break;
        case MAT_T_INT8:
        case MAT_T_UINT8:
            for ( i = 0; i < len; i++ )
                bytesread += fread(data+i,1,1,mat->fp);
            break;
        case MAT_T_INT16:
        case MAT_T_UINT16:
        {
            mat_uint16_t i16;

            if ( mat->byteswap ) {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,2,1,mat->fp);
                    data[i] = int16Swap(&i16);
                }
            } else {
                for ( i = 0; i < len; i++ ) {
                    bytesread += fread(&i16,2,1,mat->fp);
                    data[i] = i16;
                }
            }
            break;
        }
        default:
            printf("Character data not supported type: %d",data_type);
            break;
    }
    bytesread *= data_size;
    return bytesread;
}

/*
 *-------------------------------------------------------------------
 *  Routines to read "slabs" of data
 *-------------------------------------------------------------------
 */

static int
ReadDataSlabN(mat_t *mat,void *data,int class_type,int data_type,int rank,
    int *dims,int *start,int *stride,int *edge)
{
    int nBytes = 0, i, j, N, I = 0;
    int inc[10] = {0,}, cnt[10] = {0,}, dimp[10] = {0,};

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) ||
         (start == NULL) || (stride == NULL) || (edge    == NULL) ) {
        return 1;
    } else if ( rank > 10 ) {
        return 1;
    }

    switch ( class_type ) {
        case MAT_C_DOUBLE:
        {
            double *ptr;

            ptr = (double *)data;
            inc[0] = stride[0]-1;
            N = edge[0];
            if ( start[0] > 0 )
                fseek(mat->fp,Mat_SizeOf(data_type)*start[0],SEEK_CUR);
            for ( i = 1; i < rank; i++ ) {
                inc[i]  = stride[i]-1;
                dimp[i] = dims[i-1];
                for ( j = i ; j--; ) {
                    inc[i]  *= dims[j];
                    dimp[i] *= dims[j+1];
                }
                N *= edge[i];
                if ( start[i] > 0 )
                    fseek(mat->fp,Mat_SizeOf(data_type)*start[i]*dimp[i-1],SEEK_CUR);
                Mat_Message("inc[%d] = %d",i,inc[i]);
            }
            for ( i = 0; i < N; i+=edge[0] ) {
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadDoubleData(mat,(double*)data+i+j,data_type,1);
                    fseek(mat->fp,Mat_SizeOf(data_type)*(stride[0]-1),SEEK_CUR);
                    I += stride[0];
                }
                I += dims[0]-edge[0]*stride[0]-start[0];
                fseek(mat->fp,Mat_SizeOf(data_type)*
                      (dims[0]-edge[0]*stride[0]-start[0]),SEEK_CUR);
                for ( j = 1; j < rank-1; j++ ) {
                    cnt[j]++;
                    if ( (cnt[j] % edge[j]) == 0 ) {
                        cnt[j] = 0;
                        if ( (I % dimp[j]) != 0 ) {
                            fseek(mat->fp,Mat_SizeOf(data_type)*
                                  (dimp[j]-(I % dimp[j])),SEEK_CUR);
                            I += dimp[j]-(I % dimp[j]);
                        }
                    } else {
                        I += inc[j];
                        fseek(mat->fp,Mat_SizeOf(data_type)*inc[j],SEEK_CUR);
                        break;
                    }
                }
            }
            break;
        }
        case MAT_C_SINGLE:
        {
            float *ptr;

            ptr = (float *)data;
            inc[0] = stride[0]-1;
            N = edge[0];
            if ( start[0] > 0 )
                fseek(mat->fp,Mat_SizeOf(data_type)*start[0],SEEK_CUR);
            for ( i = 1; i < rank; i++ ) {
                inc[i]  = stride[i]-1;
                dimp[i] = dims[i-1];
                for ( j = i ; j--; ) {
                    inc[i]  *= dims[j];
                    dimp[i] *= dims[j+1];
                }
                N *= edge[i];
                if ( start[i] > 0 )
                    fseek(mat->fp,Mat_SizeOf(data_type)*start[i]*dimp[i-1],SEEK_CUR);
                Mat_Message("inc[%d] = %d",i,inc[i]);
            }
            for ( i = 0; i < N; i+=edge[0] ) {
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadSingleData(mat,(float*)data+i+j,data_type,1);
                    fseek(mat->fp,Mat_SizeOf(data_type)*(stride[0]-1),SEEK_CUR);
                    I += stride[0];
                }
                I += dims[0]-edge[0]*stride[0]-start[0];
                fseek(mat->fp,Mat_SizeOf(data_type)*
                      (dims[0]-edge[0]*stride[0]-start[0]),SEEK_CUR);
                for ( j = 1; j < rank-1; j++ ) {
                    cnt[j]++;
                    if ( (cnt[j] % edge[j]) == 0 ) {
                        cnt[j] = 0;
                        if ( (I % dimp[j]) != 0 ) {
                            fseek(mat->fp,Mat_SizeOf(data_type)*
                                  (dimp[j]-(I % dimp[j])),SEEK_CUR);
                            I += dimp[j]-(I % dimp[j]);
                        }
                    } else {
                        I += inc[j];
                        fseek(mat->fp,Mat_SizeOf(data_type)*inc[j],SEEK_CUR);
                        break;
                    }
                }
            }
            break;
        }
        case MAT_C_INT32:
        {
            mat_int32_t *ptr;

            ptr = (mat_int32_t *)data;
            inc[0] = stride[0]-1;
            N = edge[0];
            if ( start[0] > 0 )
                fseek(mat->fp,Mat_SizeOf(data_type)*start[0],SEEK_CUR);
            for ( i = 1; i < rank; i++ ) {
                inc[i]  = stride[i]-1;
                dimp[i] = dims[i-1];
                for ( j = i ; j--; ) {
                    inc[i]  *= dims[j];
                    dimp[i] *= dims[j+1];
                }
                N *= edge[i];
                if ( start[i] > 0 )
                    fseek(mat->fp,Mat_SizeOf(data_type)*start[i]*dimp[i-1],SEEK_CUR);
                Mat_Message("inc[%d] = %d",i,inc[i]);
            }
            for ( i = 0; i < N; i+=edge[0] ) {
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt32Data(mat,(mat_int32_t*)data+i+j,data_type,1);
                    fseek(mat->fp,Mat_SizeOf(data_type)*(stride[0]-1),SEEK_CUR);
                    I += stride[0];
                }
                I += dims[0]-edge[0]*stride[0]-start[0];
                fseek(mat->fp,Mat_SizeOf(data_type)*
                      (dims[0]-edge[0]*stride[0]-start[0]),SEEK_CUR);
                for ( j = 1; j < rank-1; j++ ) {
                    cnt[j]++;
                    if ( (cnt[j] % edge[j]) == 0 ) {
                        cnt[j] = 0;
                        if ( (I % dimp[j]) != 0 ) {
                            fseek(mat->fp,Mat_SizeOf(data_type)*
                                  (dimp[j]-(I % dimp[j])),SEEK_CUR);
                            I += dimp[j]-(I % dimp[j]);
                        }
                    } else {
                        I += inc[j];
                        fseek(mat->fp,Mat_SizeOf(data_type)*inc[j],SEEK_CUR);
                        break;
                    }
                }
            }
            break;
        }
        case MAT_C_UINT32:
        {
            mat_uint32_t *ptr;

            ptr = (mat_uint32_t *)data;
            inc[0] = stride[0]-1;
            N = edge[0];
            if ( start[0] > 0 )
                fseek(mat->fp,Mat_SizeOf(data_type)*start[0],SEEK_CUR);
            for ( i = 1; i < rank; i++ ) {
                inc[i]  = stride[i]-1;
                dimp[i] = dims[i-1];
                for ( j = i ; j--; ) {
                    inc[i]  *= dims[j];
                    dimp[i] *= dims[j+1];
                }
                N *= edge[i];
                if ( start[i] > 0 )
                    fseek(mat->fp,Mat_SizeOf(data_type)*start[i]*dimp[i-1],SEEK_CUR);
                Mat_Message("inc[%d] = %d",i,inc[i]);
            }
            for ( i = 0; i < N; i+=edge[0] ) {
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt32Data(mat,(mat_uint32_t*)data+i+j,data_type,1);
                    fseek(mat->fp,Mat_SizeOf(data_type)*(stride[0]-1),SEEK_CUR);
                    I += stride[0];
                }
                I += dims[0]-edge[0]*stride[0]-start[0];
                fseek(mat->fp,Mat_SizeOf(data_type)*
                      (dims[0]-edge[0]*stride[0]-start[0]),SEEK_CUR);
                for ( j = 1; j < rank-1; j++ ) {
                    cnt[j]++;
                    if ( (cnt[j] % edge[j]) == 0 ) {
                        cnt[j] = 0;
                        if ( (I % dimp[j]) != 0 ) {
                            fseek(mat->fp,Mat_SizeOf(data_type)*
                                  (dimp[j]-(I % dimp[j])),SEEK_CUR);
                            I += dimp[j]-(I % dimp[j]);
                        }
                    } else {
                        I += inc[j];
                        fseek(mat->fp,Mat_SizeOf(data_type)*inc[j],SEEK_CUR);
                        break;
                    }
                }
            }
            break;
        }
        case MAT_C_INT16:
        {
            mat_int16_t *ptr;

            ptr = (mat_int16_t *)data;
            inc[0] = stride[0]-1;
            N = edge[0];
            if ( start[0] > 0 )
                fseek(mat->fp,Mat_SizeOf(data_type)*start[0],SEEK_CUR);
            for ( i = 1; i < rank; i++ ) {
                inc[i]  = stride[i]-1;
                dimp[i] = dims[i-1];
                for ( j = i ; j--; ) {
                    inc[i]  *= dims[j];
                    dimp[i] *= dims[j+1];
                }
                N *= edge[i];
                if ( start[i] > 0 )
                    fseek(mat->fp,start[i]*dimp[i-1]*Mat_SizeOf(data_type),SEEK_CUR);
                Mat_Message("inc[%d] = %d",i,inc[i]);
            }
            for ( i = 0; i < N; i+=edge[0] ) {
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt16Data(mat,(mat_int16_t*)data+i+j,data_type,1);
                    fseek(mat->fp,Mat_SizeOf(data_type)*(stride[0]-1),SEEK_CUR);
                    I += stride[0];
                }
                I += dims[0]-edge[0]*stride[0]-start[0];
                fseek(mat->fp,Mat_SizeOf(data_type)*
                      (dims[0]-edge[0]*stride[0]-start[0]),SEEK_CUR);
                for ( j = 1; j < rank-1; j++ ) {
                    cnt[j]++;
                    if ( (cnt[j] % edge[j]) == 0 ) {
                        cnt[j] = 0;
                        if ( (I % dimp[j]) != 0 ) {
                            fseek(mat->fp,Mat_SizeOf(data_type)*
                                  (dimp[j]-(I % dimp[j])),SEEK_CUR);
                            I += dimp[j]-(I % dimp[j]);
                        }
                    } else {
                        I += inc[j];
                        fseek(mat->fp,Mat_SizeOf(data_type)*inc[j],SEEK_CUR);
                        break;
                    }
                }
            }
            break;
        }
        case MAT_C_UINT16:
        {
            mat_uint16_t *ptr;

            ptr = (mat_uint16_t *)data;
            inc[0] = stride[0]-1;
            N = edge[0];
            if ( start[0] > 0 )
                fseek(mat->fp,Mat_SizeOf(data_type)*start[0],SEEK_CUR);
            for ( i = 1; i < rank; i++ ) {
                inc[i]  = stride[i]-1;
                dimp[i] = dims[i-1];
                for ( j = i ; j--; ) {
                    inc[i]  *= dims[j];
                    dimp[i] *= dims[j+1];
                }
                N *= edge[i];
                if ( start[i] > 0 )
                    fseek(mat->fp,start[i]*dimp[i-1]*Mat_SizeOf(data_type),SEEK_CUR);
                Mat_Message("inc[%d] = %d",i,inc[i]);
            }
            for ( i = 0; i < N; i+=edge[0] ) {
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt16Data(mat,(mat_uint16_t*)data+i+j,data_type,1);
                    fseek(mat->fp,Mat_SizeOf(data_type)*(stride[0]-1),SEEK_CUR);
                    I += stride[0];
                }
                I += dims[0]-edge[0]*stride[0]-start[0];
                fseek(mat->fp,Mat_SizeOf(data_type)*
                      (dims[0]-edge[0]*stride[0]-start[0]),SEEK_CUR);
                for ( j = 1; j < rank-1; j++ ) {
                    cnt[j]++;
                    if ( (cnt[j] % edge[j]) == 0 ) {
                        cnt[j] = 0;
                        if ( (I % dimp[j]) != 0 ) {
                            fseek(mat->fp,Mat_SizeOf(data_type)*
                                  (dimp[j]-(I % dimp[j])),SEEK_CUR);
                            I += dimp[j]-(I % dimp[j]);
                        }
                    } else {
                        I += inc[j];
                        fseek(mat->fp,Mat_SizeOf(data_type)*inc[j],SEEK_CUR);
                        break;
                    }
                }
            }
            break;
        }
        case MAT_C_INT8:
        {
            mat_int8_t *ptr;

            ptr = (mat_int8_t *)data;
            inc[0] = stride[0]-1;
            N = edge[0];
            if ( start[0] > 0 )
                fseek(mat->fp,Mat_SizeOf(data_type)*start[0],SEEK_CUR);
            for ( i = 1; i < rank; i++ ) {
                inc[i]  = stride[i]-1;
                dimp[i] = dims[i-1];
                for ( j = i ; j--; ) {
                    inc[i]  *= dims[j];
                    dimp[i] *= dims[j+1];
                }
                N *= edge[i];
                if ( start[i] > 0 )
                    fseek(mat->fp,start[i]*dimp[i-1]*Mat_SizeOf(data_type),SEEK_CUR);
                Mat_Message("inc[%d] = %d",i,inc[i]);
            }
            for ( i = 0; i < N; i+=edge[0] ) {
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt8Data(mat,(mat_int8_t*)data+i+j,data_type,1);
                    fseek(mat->fp,Mat_SizeOf(data_type)*(stride[0]-1),SEEK_CUR);
                    I += stride[0];
                }
                I += dims[0]-edge[0]*stride[0]-start[0];
                fseek(mat->fp,Mat_SizeOf(data_type)*
                      (dims[0]-edge[0]*stride[0]-start[0]),SEEK_CUR);
                for ( j = 1; j < rank-1; j++ ) {
                    cnt[j]++;
                    if ( (cnt[j] % edge[j]) == 0 ) {
                        cnt[j] = 0;
                        if ( (I % dimp[j]) != 0 ) {
                            fseek(mat->fp,Mat_SizeOf(data_type)*
                                  (dimp[j]-(I % dimp[j])),SEEK_CUR);
                            I += dimp[j]-(I % dimp[j]);
                        }
                    } else {
                        I += inc[j];
                        fseek(mat->fp,Mat_SizeOf(data_type)*inc[j],SEEK_CUR);
                        break;
                    }
                }
            }
            break;
        }
        case MAT_C_UINT8:
        {
            mat_uint8_t *ptr;

            ptr = (mat_uint8_t *)data;
            inc[0] = stride[0]-1;
            dimp[0] = dims[0];
            N = edge[0];
            if ( start[0] > 0 )
                fseek(mat->fp,Mat_SizeOf(data_type)*start[0],SEEK_CUR);
            for ( i = 1; i < rank; i++ ) {
                inc[i]  = stride[i]-1;
                dimp[i] = dims[i-1];
                for ( j = i ; j--; ) {
                    inc[i]  *= dims[j];
                    dimp[i] *= dims[j+1];
                }
                N *= edge[i];
                if ( start[i] > 0 )
                    fseek(mat->fp,start[i]*dimp[i-1]*Mat_SizeOf(data_type),SEEK_CUR);
                Mat_Message("inc[%d] = %d",i,inc[i]);
            }
            for ( i = 0; i < N; i+=edge[0] ) {
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt8Data(mat,(mat_uint8_t*)data+i+j,data_type,1);
                    fseek(mat->fp,Mat_SizeOf(data_type)*(stride[0]-1),SEEK_CUR);
                    I += stride[0];
                }
                I += dims[0]-edge[0]*stride[0]-start[0];
                fseek(mat->fp,Mat_SizeOf(data_type)*
                      (dims[0]-edge[0]*stride[0]-start[0]),SEEK_CUR);
                for ( j = 1; j < rank-1; j++ ) {
                    cnt[j]++;
                    if ( (cnt[j] % edge[j]) == 0 ) {
                        cnt[j] = 0;
                        if ( (I % dimp[j]) != 0 ) {
                            fseek(mat->fp,Mat_SizeOf(data_type)*
                                  (dimp[j]-(I % dimp[j])),SEEK_CUR);
                            I += dimp[j]-(I % dimp[j]);
                        }
                    } else {
                        I += inc[j];
                        fseek(mat->fp,Mat_SizeOf(data_type)*inc[j],SEEK_CUR);
                        break;
                    }
                }
            }
            break;
        }
        default:
            nBytes = 0;
    }
    return nBytes;
}

static int
ReadDataSlab2(mat_t *mat,void *data,int class_type,int data_type,
    int *dims,int *start,int *stride,int *edge)
{
    int nBytes = 0, data_size, i, j;
    long pos, row_stride, col_stride;

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) ||
         (start == NULL) || (stride == NULL) || (edge    == NULL) ) {
        return 0;
    }

    data_size = Mat_SizeOf(data_type);

    switch ( class_type ) {
        case MAT_C_DOUBLE:
        {
            double *ptr;

            ptr = (double *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;
            pos = ftell(mat->fp);
            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadDoubleData(mat,ptr++,data_type,1);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_C_SINGLE:
        {
            float *ptr;

            ptr = (float *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;
            pos = ftell(mat->fp);
            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadSingleData(mat,ptr++,data_type,1);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_C_INT32:
        {
            mat_int32_t *ptr;

            ptr = (mat_int32_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;
            pos = ftell(mat->fp);
            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt32Data(mat,ptr++,data_type,1);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_C_UINT32:
        {
            mat_uint32_t *ptr;

            ptr = (mat_uint32_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;
            pos = ftell(mat->fp);
            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt32Data(mat,ptr++,data_type,1);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_C_INT16:
        {
            mat_int16_t *ptr;

            ptr = (mat_int16_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;
            pos = ftell(mat->fp);
            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt16Data(mat,ptr++,data_type,1);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_C_UINT16:
        {
            mat_uint16_t *ptr;

            ptr = (mat_uint16_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;
            pos = ftell(mat->fp);
            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt16Data(mat,ptr++,data_type,1);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_C_INT8:
        {
            mat_int8_t *ptr;

            ptr = (mat_int8_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;
            pos = ftell(mat->fp);
            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt8Data(mat,ptr++,data_type,1);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_C_UINT8:
        {
            mat_uint8_t *ptr;

            ptr = (mat_uint8_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;
            pos = ftell(mat->fp);
            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    ReadInt8Data(mat,ptr++,data_type,1);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        default:
            nBytes = 0;
    }
    return nBytes;
}

#ifdef HAVE_ZLIB
static int
ReadCompressedDataSlabN(mat_t *mat,z_stream *z,void *data,int class_type,
    int data_type,int rank,int *dims,int *start,int *stride,int *edge)
{
    int nBytes = 0, i, j, N, I = 0;
    int inc[10] = {0,}, cnt[10] = {0,}, dimp[10] = {0,};
    z_stream z_copy = {0,};

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) ||
         (start == NULL) || (stride == NULL) || (edge    == NULL) ) {
        return 1;
    } else if ( rank > 10 ) {
        return 1;
    }

    i = inflateCopy(&z_copy,z);
    switch ( class_type ) {
        case MAT_C_DOUBLE:
        {
            double *ptr;

            ptr = (double *)data;
            inc[0] = stride[0]-1;
            dimp[0] = dims[0];
            N = edge[0];
            if ( start[0] > 0 )
                InflateSkipData(mat,&z_copy,data_type,start[0]);
            for ( i = 1; i < rank; i++ ) {
                inc[i]  = stride[i]-1;
                dimp[i] = dims[i-1];
                for ( j = i ; j--; ) {
                    inc[i]  *= dims[j];
                    dimp[i] *= dims[j+1];
                }
                N *= edge[i];
                if ( start[i] > 0 )
                    InflateSkipData(mat,&z_copy,data_type,start[i]*dimp[i-1]);
            }
            for ( i = 0; i < N; i+=edge[0] ) {
                for ( j = 0; j < edge[0]-1; j++ ) {
                    ReadCompressedDoubleData(mat,&z_copy,ptr+i+j,data_type,1);
                    InflateSkipData(mat,&z_copy,data_type,(stride[0]-1));
                    I += stride[0];
                }
                ReadCompressedDoubleData(mat,&z_copy,ptr+i+j,data_type,1);
                I += stride[0];
                for ( j = 1; j < rank-1; j++ ) {
                    cnt[j]++;
                    if ( (cnt[j] % edge[j]) == 0 ) {
                        cnt[j] = 0;
                        if ( (I % dimp[j]) != 0 ) {
                            InflateSkipData(mat,&z_copy,data_type,
                                  dimp[j]-(I % dimp[j]));
                            I += dimp[j]-(I % dimp[j]);
                        }
                    } else {
                        I += dims[0]-edge[0]*stride[0]-start[0];
                        InflateSkipData(mat,&z_copy,data_type,
                              dims[0]-edge[0]*stride[0]-start[0]);
                        I += inc[j];
                        InflateSkipData(mat,&z_copy,data_type,inc[j]);
                        break;
                    }
                }
            }
            break;
        }
        default:
            nBytes = 0;
    }
    inflateEnd(&z_copy);
    return nBytes;
}

static int
ReadCompressedDataSlab2(mat_t *mat,z_stream *z,void *data,int class_type,
    int data_type,int *dims,int *start,int *stride,int *edge)
{
    int nBytes = 0, data_size, i, j, err;
    int pos, row_stride, col_stride;
    z_stream z_copy = {0,};

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) ||
         (start == NULL) || (stride == NULL) || (edge    == NULL) ) {
        return 0;
    }

    err = inflateCopy(&z_copy,z);
    switch ( class_type ) {
        case MAT_C_DOUBLE:
        {
            double *ptr;

            data_size = sizeof(double);
            ptr = data;
            row_stride = (stride[0]-1);
            col_stride = (stride[1]-1)*dims[0];
            InflateSkipData(mat,&z_copy,data_type,start[1]*dims[0]);
            for ( i = 0; i < edge[1]; i++ ) {
                InflateSkipData(mat,&z_copy,data_type,start[0]);
                for ( j = 0; j < edge[0]-1; j++ ) {
                    ReadCompressedDoubleData(mat,&z_copy,ptr++,data_type,1);
                    InflateSkipData(mat,&z_copy,data_type,stride[0]-1);
                }
                ReadCompressedDoubleData(mat,&z_copy,ptr++,data_type,1);
                pos = dims[0]-(edge[0]-1)*stride[0]-1-start[0] + col_stride;
                InflateSkipData(mat,&z_copy,data_type,pos);
            }
            break;
        }
        case MAT_C_SINGLE:
        {
            float *ptr;

            data_size = sizeof(float);
            ptr = data;
            row_stride = (stride[0]-1);
            col_stride = (stride[1]-1)*dims[0];
            InflateSkipData(mat,&z_copy,data_type,start[1]*dims[0]);
            for ( i = 0; i < edge[1]; i++ ) {
                InflateSkipData(mat,&z_copy,data_type,start[0]);
                for ( j = 0; j < edge[0]-1; j++ ) {
                    ReadCompressedSingleData(mat,&z_copy,ptr++,data_type,1);
                    InflateSkipData(mat,&z_copy,data_type,stride[0]-1);
                }
                ReadCompressedSingleData(mat,&z_copy,ptr++,data_type,1);
                pos = dims[0]-(edge[0]-1)*stride[0]-1-start[0] + col_stride;
                InflateSkipData(mat,&z_copy,data_type,pos);
            }
            break;
        }
        case MAT_C_INT32:
        {
            mat_int32_t *ptr;

            data_size = sizeof(mat_int32_t);
            ptr = data;
            row_stride = (stride[0]-1);
            col_stride = (stride[1]-1)*dims[0];
            InflateSkipData(mat,&z_copy,data_type,start[1]*dims[0]);
            for ( i = 0; i < edge[1]; i++ ) {
                InflateSkipData(mat,&z_copy,data_type,start[0]);
                for ( j = 0; j < edge[0]-1; j++ ) {
                    ReadCompressedInt32Data(mat,&z_copy,ptr++,data_type,1);
                    InflateSkipData(mat,&z_copy,data_type,stride[0]-1);
                }
                ReadCompressedInt32Data(mat,&z_copy,ptr++,data_type,1);
                pos = dims[0]-(edge[0]-1)*stride[0]-1-start[0] + col_stride;
                InflateSkipData(mat,&z_copy,data_type,pos);
            }
            break;
        }
        case MAT_C_UINT32:
        {
            mat_uint32_t *ptr;

            data_size = sizeof(mat_uint32_t);
            ptr = data;
            row_stride = (stride[0]-1);
            col_stride = (stride[1]-1)*dims[0];
            InflateSkipData(mat,&z_copy,data_type,start[1]*dims[0]);
            for ( i = 0; i < edge[1]; i++ ) {
                InflateSkipData(mat,&z_copy,data_type,start[0]);
                for ( j = 0; j < edge[0]-1; j++ ) {
                    ReadCompressedInt32Data(mat,&z_copy,ptr++,data_type,1);
                    InflateSkipData(mat,&z_copy,data_type,stride[0]-1);
                }
                ReadCompressedInt32Data(mat,&z_copy,ptr++,data_type,1);
                pos = dims[0]-(edge[0]-1)*stride[0]-1-start[0] + col_stride;
                InflateSkipData(mat,&z_copy,data_type,pos);
            }
            break;
        }
        case MAT_C_INT16:
        {
            mat_int16_t *ptr;

            data_size = sizeof(mat_int16_t);
            ptr = data;
            row_stride = (stride[0]-1);
            col_stride = (stride[1]-1)*dims[0];
            InflateSkipData(mat,&z_copy,data_type,start[1]*dims[0]);
            for ( i = 0; i < edge[1]; i++ ) {
                InflateSkipData(mat,&z_copy,data_type,start[0]);
                for ( j = 0; j < edge[0]-1; j++ ) {
                    ReadCompressedInt16Data(mat,&z_copy,ptr++,data_type,1);
                    InflateSkipData(mat,&z_copy,data_type,stride[0]-1);
                }
                ReadCompressedInt16Data(mat,&z_copy,ptr++,data_type,1);
                pos = dims[0]-(edge[0]-1)*stride[0]-1-start[0] + col_stride;
                InflateSkipData(mat,&z_copy,data_type,pos);
            }
            break;
        }
        case MAT_C_UINT16:
        {
            mat_uint16_t *ptr;

            data_size = sizeof(mat_uint16_t);
            ptr = data;
            row_stride = (stride[0]-1);
            col_stride = (stride[1]-1)*dims[0];
            InflateSkipData(mat,&z_copy,data_type,start[1]*dims[0]);
            for ( i = 0; i < edge[1]; i++ ) {
                InflateSkipData(mat,&z_copy,data_type,start[0]);
                for ( j = 0; j < edge[0]-1; j++ ) {
                    ReadCompressedInt16Data(mat,&z_copy,ptr++,data_type,1);
                    InflateSkipData(mat,&z_copy,data_type,stride[0]-1);
                }
                ReadCompressedInt16Data(mat,&z_copy,ptr++,data_type,1);
                pos = dims[0]-(edge[0]-1)*stride[0]-1-start[0] + col_stride;
                InflateSkipData(mat,&z_copy,data_type,pos);
            }
            break;
        }
        case MAT_C_INT8:
        {
            mat_int8_t *ptr;

            data_size = sizeof(mat_int8_t);
            ptr = data;
            row_stride = (stride[0]-1);
            col_stride = (stride[1]-1)*dims[0];
            InflateSkipData(mat,&z_copy,data_type,start[1]*dims[0]);
            for ( i = 0; i < edge[1]; i++ ) {
                InflateSkipData(mat,&z_copy,data_type,start[0]);
                for ( j = 0; j < edge[0]-1; j++ ) {
                    ReadCompressedInt8Data(mat,&z_copy,ptr++,data_type,1);
                    InflateSkipData(mat,&z_copy,data_type,stride[0]-1);
                }
                ReadCompressedInt8Data(mat,&z_copy,ptr++,data_type,1);
                pos = dims[0]-(edge[0]-1)*stride[0]-1-start[0] + col_stride;
                InflateSkipData(mat,&z_copy,data_type,pos);
            }
            break;
        }
        case MAT_C_UINT8:
        {
            mat_uint8_t *ptr;

            data_size = sizeof(mat_uint8_t);
            ptr = data;
            row_stride = (stride[0]-1);
            col_stride = (stride[1]-1)*dims[0];
            InflateSkipData(mat,&z_copy,data_type,start[1]*dims[0]);
            for ( i = 0; i < edge[1]; i++ ) {
                InflateSkipData(mat,&z_copy,data_type,start[0]);
                for ( j = 0; j < edge[0]-1; j++ ) {
                    ReadCompressedInt8Data(mat,&z_copy,ptr++,data_type,1);
                    InflateSkipData(mat,&z_copy,data_type,stride[0]-1);
                }
                ReadCompressedInt8Data(mat,&z_copy,ptr++,data_type,1);
                pos = dims[0]-(edge[0]-1)*stride[0]-1-start[0] + col_stride;
                InflateSkipData(mat,&z_copy,data_type,pos);
            }
            break;
        }
        default:
            nBytes = 0;
    }
    inflateEnd(&z_copy);
    return nBytes;
}
#endif

static int
WriteCharData(mat_t *mat, void *data, int N,int data_type)
{
    int nBytes = 0, bytesread = 0, i;
    mat_int8_t pad1 = 0;

    switch ( data_type ) {
        case MAT_T_UINT16:
        {
            nBytes = N*2;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            fwrite(data,2,N,mat->fp);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            break;
        }
        case MAT_T_INT8:
        case MAT_T_UINT8:
        {
            mat_uint8_t *ptr;
            mat_uint16_t c;

            /* Matlab can't read MAT_C_CHAR as uint8, needs uint16 */
            nBytes = N*2;
            data_type = MAT_T_UINT16;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            ptr = data;
            for ( i = 0; i < N; i++ ) {
                c = (mat_uint16_t)*(char *)ptr;
                fwrite(&c,2,1,mat->fp);
                ptr++;
            }
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            break;
        }
        case MAT_T_UTF8:
        {
            mat_uint8_t *ptr;

            nBytes = N;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            ptr = data;
            fwrite(ptr,1,nBytes,mat->fp);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            break;
        }
    }
    bytesread+=nBytes;
    return bytesread;
}

static int
WriteEmptyCharData(mat_t *mat, int N, int data_type)
{
    int nBytes = 0, bytesread = 0, i;
    mat_int8_t pad1 = 0;

    switch ( data_type ) {
        case MAT_T_UINT8: /* Matlab MAT_C_CHAR needs uint16 */
        case MAT_T_INT8:  /* Matlab MAT_C_CHAR needs uint16 */
            data_type = MAT_T_UINT16;
        case MAT_T_UINT16:
        {
            mat_uint16_t u16 = 0;
            nBytes = N*sizeof(mat_uint16_t);
            fwrite(&data_type,sizeof(mat_int32_t),1,mat->fp);
            fwrite(&nBytes,sizeof(mat_int32_t),1,mat->fp);
            for ( i = 0; i < N; i++ )
                fwrite(&u16,sizeof(mat_uint16_t),1,mat->fp);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            break;
        }
        case MAT_T_UTF8:
        {
            mat_uint8_t u8 = 0;
            nBytes = N;
            fwrite(&data_type,sizeof(mat_int32_t),1,mat->fp);
            fwrite(&nBytes,sizeof(mat_int32_t),1,mat->fp);
            for ( i = 0; i < N; i++ )
                fwrite(&u8,sizeof(mat_uint8_t),1,mat->fp);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            break;
        }
    }
    bytesread+=nBytes;
    return bytesread;
}

/*
 * Writes the data tags and empty data to the file to save space for the
 * variable when the actual data is written
 */
static int
WriteEmptyData(mat_t *mat,int N,int data_type)
{
    int nBytes = 0, data_size, i;

    if ( (mat == NULL) || (mat->fp == NULL) )
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double d = 0.0;

            data_size = sizeof(double);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            for ( i = 0; i < N; i++ )
                fwrite(&d,data_size,1,mat->fp);
            break;
        }
        case MAT_T_SINGLE:
        {
            float f = 0.0;

            data_size = sizeof(float);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            for ( i = 0; i < N; i++ )
                fwrite(&f,data_size,1,mat->fp);
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t i8 = 0;

            data_size = sizeof(mat_int8_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            for ( i = 0; i < N; i++ )
                fwrite(&i8,data_size,1,mat->fp);
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t ui8 = 0;

            data_size = sizeof(mat_uint8_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            for ( i = 0; i < N; i++ )
                fwrite(&ui8,data_size,1,mat->fp);
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t i16 = 0;

            data_size = sizeof(mat_int16_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            for ( i = 0; i < N; i++ )
                fwrite(&i16,data_size,1,mat->fp);
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t ui16 = 0;

            data_size = sizeof(mat_uint16_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            for ( i = 0; i < N; i++ )
                fwrite(&ui16,data_size,1,mat->fp);
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t i32 = 0;

            data_size = sizeof(mat_int32_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            for ( i = 0; i < N; i++ )
                fwrite(&i32,data_size,1,mat->fp);
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t ui32 = 0;

            data_size = sizeof(mat_uint32_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            for ( i = 0; i < N; i++ )
                fwrite(&ui32,data_size,1,mat->fp);
            break;
        }
        default:
            nBytes = 0;
    }
    return nBytes;
}

static int
WriteDataSlab2(mat_t *mat,void *data,int data_type,int *dims,int *start,
              int *stride,int *edge)
{
    int nBytes = 0, data_size, i, j;
    long pos, row_stride, col_stride;

    if ( (mat   == NULL) || (data   == NULL) || (mat->fp == NULL) ||
         (start == NULL) || (stride == NULL) || (edge    == NULL) ) {
        return 0;
    }

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double *ptr;

            data_size = sizeof(double);
            ptr = (double *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;

            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    fwrite(ptr++,data_size,1,mat->fp);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_T_SINGLE:
        {
            float *ptr;

            data_size = sizeof(float);
            ptr = (float *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;

            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    fwrite(ptr++,data_size,1,mat->fp);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t *ptr;

            data_size = sizeof(mat_int32_t);
            ptr = (mat_int32_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;

            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    fwrite(ptr++,data_size,1,mat->fp);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t *ptr;

            data_size = sizeof(mat_uint32_t);
            ptr = (mat_uint32_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;

            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    fwrite(ptr++,data_size,1,mat->fp);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t *ptr;

            data_size = sizeof(mat_int16_t);
            ptr = (mat_int16_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;

            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    fwrite(ptr++,data_size,1,mat->fp);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t *ptr;

            data_size = sizeof(mat_uint16_t);
            ptr = (mat_uint16_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;

            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    fwrite(ptr++,data_size,1,mat->fp);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t *ptr;

            data_size = sizeof(mat_int8_t);
            ptr = (mat_int8_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;

            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    fwrite(ptr++,data_size,1,mat->fp);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t *ptr;

            data_size = sizeof(mat_uint8_t);
            ptr = (mat_uint8_t *)data;
            row_stride = (stride[0]-1)*data_size;
            col_stride = stride[1]*dims[0]*data_size;

            fseek(mat->fp,start[1]*dims[0]*data_size,SEEK_CUR);
            for ( i = 0; i < edge[1]; i++ ) {
                pos = ftell(mat->fp);
                fseek(mat->fp,start[0]*data_size,SEEK_CUR);
                for ( j = 0; j < edge[0]; j++ ) {
                    fwrite(ptr++,data_size,1,mat->fp);
                    fseek(mat->fp,row_stride,SEEK_CUR);
                }
                pos = pos+col_stride-ftell(mat->fp);
                fseek(mat->fp,pos,SEEK_CUR);
            }
            break;
        }
        default:
            nBytes = 0;
    }
    return nBytes;
}

static int
WriteData(mat_t *mat,void *data,int N,int data_type)
{
    int nBytes = 0, data_size, i;

    if ((mat == NULL) || (data == NULL) || (mat->fp == NULL))
        return 0;

    switch ( data_type ) {
        case MAT_T_DOUBLE:
        {
            double *ptr;

            data_size = sizeof(double);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            ptr = (double *)data;
            for ( i = 0; i < N; i++ )
                fwrite(ptr+i,data_size,1,mat->fp);
            break;
        }
        case MAT_T_SINGLE:
        {
            float *ptr;

            data_size = sizeof(float);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            ptr = (float *)data;
            for ( i = 0; i < N; i++ )
                fwrite(ptr+i,data_size,1,mat->fp);
            break;
        }
        case MAT_T_INT8:
        {
            mat_int8_t *ptr;

            data_size = sizeof(mat_int8_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            ptr = (mat_int8_t *)data;
            for ( i = 0; i < N; i++ )
                fwrite(ptr+i,data_size,1,mat->fp);
            break;
        }
        case MAT_T_UINT8:
        {
            mat_uint8_t *ptr;

            data_size = sizeof(mat_uint8_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            ptr = (mat_uint8_t *)data;
            for ( i = 0; i < N; i++ )
                fwrite(ptr+i,data_size,1,mat->fp);
            break;
        }
        case MAT_T_INT16:
        {
            mat_int16_t *ptr;

            data_size = sizeof(mat_int16_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            ptr = (mat_int16_t *)data;
            for ( i = 0; i < N; i++ )
                fwrite(ptr+i,data_size,1,mat->fp);
            break;
        }
        case MAT_T_UINT16:
        {
            mat_uint16_t *ptr;

            data_size = sizeof(mat_uint16_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            ptr = (mat_uint16_t *)data;
            for ( i = 0; i < N; i++ )
                fwrite(ptr+i,data_size,1,mat->fp);
            break;
        }
        case MAT_T_INT32:
        {
            mat_int32_t *ptr;

            data_size = sizeof(mat_int32_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            ptr = (mat_int32_t *)data;
            for ( i = 0; i < N; i++ )
                fwrite(ptr+i,data_size,1,mat->fp);
            break;
        }
        case MAT_T_UINT32:
        {
            mat_uint32_t *ptr;

            data_size = sizeof(mat_uint32_t);
            nBytes = N*data_size;
            fwrite(&data_type,4,1,mat->fp);
            fwrite(&nBytes,4,1,mat->fp);
            ptr = (mat_uint32_t *)data;
            for ( i = 0; i < N; i++ )
                fwrite(ptr+i,data_size,1,mat->fp);
            break;
        }
        default:
            Mat_Critical("WriteData: Unrecognized data type %d",data_type);
            nBytes = 0;
    }
    return nBytes;
}

/*
 * Reads the next struct fields (fieldname length,names,data headers for all
 * the fields
 */
static int
ReadNextCell( mat_t *mat, matvar_t *matvar )
{
    int ncells, bytesread = 0, i, err;
    matvar_t **cells = NULL;

    if ( matvar->compression ) {
#ifdef HAVE_ZLIB
        mat_uint32_t uncomp_buf[16] = {0,};
        int      nbytes;
        mat_uint32_t array_flags; 

        ncells = 1;
        for ( i = 0; i < matvar->rank; i++ )
            ncells *= matvar->dims[i];
        matvar->data_size = sizeof(matvar_t *);
        matvar->nbytes    = ncells*matvar->data_size;
        matvar->data = malloc(matvar->nbytes);
        if ( !matvar->data )
            return bytesread;
        cells = (matvar_t **)matvar->data;
        for ( i = 0; i < ncells; i++ ) {
            cells[i] = (matvar_t *)calloc(1,sizeof(matvar_t));
            if ( !cells[i] ) {
                Mat_Critical("Couldn't allocate memory for cell %d", i);
                continue;
            }

            cells[i]->fpos = ftell(mat->fp)-matvar->z->avail_in;

            /* Read variable tag for cell */
            bytesread += InflateVarTag(mat,matvar,uncomp_buf);
            if ( mat->byteswap ) {
                int32Swap(uncomp_buf);
                int32Swap(uncomp_buf+1);
            }
            nbytes = uncomp_buf[1];
            if ( uncomp_buf[0] != MAT_T_MATRIX ) {
                Mat_Critical("cells[%d], Uncompressed type not MAT_T_MATRIX",i);
                Mat_VarFree(cells[i]);
                cells[i] = NULL;
            }
            cells[i]->compression = 1;
            bytesread += InflateArrayFlags(mat,matvar,uncomp_buf);
            nbytes -= 16;
            if ( mat->byteswap ) {
                int32Swap(uncomp_buf);
                int32Swap(uncomp_buf+1);
                int32Swap(uncomp_buf+2);
                int32Swap(uncomp_buf+3);
            }
            /* Array Flags */
            if ( uncomp_buf[0] == MAT_T_UINT32 ) {
               array_flags = uncomp_buf[2];
               cells[i]->class_type  = (array_flags & MAT_F_CLASS_T);
               cells[i]->isComplex   = (array_flags & MAT_F_COMPLEX);
               cells[i]->isGlobal    = (array_flags & MAT_F_GLOBAL);
               cells[i]->isLogical   = (array_flags & MAT_F_LOGICAL);
               if ( cells[i]->class_type == MAT_C_SPARSE ) {
                   /* Need to find a more appropriate place to store nzmax */
                   cells[i]->nbytes      = uncomp_buf[3];
               }
            } else {
                Mat_Critical("Expected MAT_T_UINT32 for Array Tags, got %d",
                               uncomp_buf[0]);
                bytesread+=InflateSkip(mat,matvar->z,nbytes);
            }
            bytesread += InflateDimensions(mat,matvar,uncomp_buf);
            nbytes -= 8;
            if ( mat->byteswap ) {
                int32Swap(uncomp_buf);
                int32Swap(uncomp_buf+1);
            }
            /* Rank and Dimension */
            if ( uncomp_buf[0] == MAT_T_INT32 ) {
                int j = 0;

                cells[i]->rank = uncomp_buf[1];
                nbytes -= cells[i]->rank;
                cells[i]->rank /= 4;
                cells[i]->dims = malloc(cells[i]->rank*sizeof(int));
                if ( mat->byteswap ) {
                    for ( j = 0; j < cells[i]->rank; j++ )
                        cells[i]->dims[j] = int32Swap(uncomp_buf+2+j);
                } else {
                    for ( j = 0; j < cells[i]->rank; j++ )
                        cells[i]->dims[j] = uncomp_buf[2+j];
                }
                if ( cells[i]->rank % 2 != 0 )
                    nbytes -= 4;
            }
            bytesread += InflateVarNameTag(mat,matvar,uncomp_buf);
            nbytes -= 8;
            cells[i]->z = calloc(1,sizeof(z_stream));
            err = inflateCopy(cells[i]->z,matvar->z);
            if ( err != Z_OK )
                Mat_Critical("inflateCopy returned error %d",err);
            cells[i]->datapos = ftell(mat->fp)-matvar->z->avail_in;
            if ( cells[i]->class_type == MAT_C_STRUCT )
                bytesread+=ReadNextStructField(mat,cells[i]);
            else if ( cells[i]->class_type == MAT_C_CELL )
                bytesread+=ReadNextCell(mat,cells[i]);
            fseek(mat->fp,cells[i]->datapos,SEEK_SET);
            bytesread+=InflateSkip(mat,matvar->z,nbytes);
        }
#else
        Mat_Critical("Not compiled with zlib support");
#endif

    } else {
        int ncells;
        mat_uint32_t buf[16];
        int      nbytes,nBytes;
        mat_uint32_t array_flags; 

        ncells = 1;
        for ( i = 0; i < matvar->rank; i++ )
            ncells *= matvar->dims[i];
        matvar->data_size = sizeof(matvar_t *);
        matvar->nbytes    = ncells*matvar->data_size;
        matvar->data = malloc(matvar->nbytes);
        if ( !matvar->data ) {
            Mat_Critical("Couldn't allocate memory for %s->data",matvar->name);
            return bytesread;
        }
        cells = (matvar_t **)matvar->data;
        for ( i = 0; i < ncells; i++ ) {
            cells[i] = (matvar_t *)calloc(1,sizeof(matvar_t));
            if ( !cells[i] ) {
                Mat_Critical("Couldn't allocate memory for cell %d", i);
                continue;
            }

            cells[i]->fpos = ftell(mat->fp);

            /* Read variable tag for cell */
            bytesread += fread(buf,4,2,mat->fp);
            if ( mat->byteswap ) {
                int32Swap(buf);
                int32Swap(buf+1);
            }
            nBytes = buf[1];
            if ( buf[0] != MAT_T_MATRIX ) {
                Mat_Critical("cells[%d] not MAT_T_MATRIX, fpos = %ld",i,ftell(mat->fp));
                Mat_VarFree(cells[i]);
                cells[i] = NULL;
                continue;
            }
            cells[i]->compression = 0;
#ifdef HAVE_ZLIB
            cells[i]->z = NULL;
#endif
            /* Read Array Flags and The Dimensions Tag */
            bytesread  += fread(buf,4,6,mat->fp);
            if ( mat->byteswap ) {
                int32Swap(buf);
                int32Swap(buf+1);
                int32Swap(buf+2);
                int32Swap(buf+3);
                int32Swap(buf+4);
                int32Swap(buf+5);
            }
            nBytes-=24;
            /* Array Flags */
            if ( buf[0] == MAT_T_UINT32 ) {
               array_flags = buf[2];
               cells[i]->class_type  = (array_flags & MAT_F_CLASS_T);
               cells[i]->isComplex   = (array_flags & MAT_F_COMPLEX);
               cells[i]->isGlobal    = (array_flags & MAT_F_GLOBAL);
               cells[i]->isLogical   = (array_flags & MAT_F_LOGICAL);
               if ( cells[i]->class_type == MAT_C_SPARSE ) {
                   /* Need to find a more appropriate place to store nzmax */
                   cells[i]->nbytes      = buf[3];
               }
            }
            /* Rank and Dimension */
            if ( buf[4] == MAT_T_INT32 ) {
                int j;
                nbytes = buf[5];
                nBytes-=nbytes;

                cells[i]->rank = nbytes / 4;
                cells[i]->dims = (int *)malloc(cells[i]->rank*sizeof(int));

                /* Assumes rank <= 16 */
                if ( cells[i]->rank % 2 != 0 ) {
                    bytesread+=fread(buf,4,cells[i]->rank+1,mat->fp);
                    nBytes-=4;
                } else
                    bytesread+=fread(buf,4,cells[i]->rank,mat->fp);

                if ( mat->byteswap ) {
                    for ( j = 0; j < cells[i]->rank; j++ )
                        cells[i]->dims[j] = int32Swap(buf+j);
                } else {
                    for ( j = 0; j < cells[i]->rank; j++ )
                        cells[i]->dims[j] = buf[j];
                }
            }
            /* Variable Name Tag */
            bytesread+=fread(buf,1,8,mat->fp);
            nBytes-=8;
            cells[i]->datapos = ftell(mat->fp);
            if ( cells[i]->class_type == MAT_C_STRUCT )
                bytesread+=ReadNextStructField(mat,cells[i]);
            if ( cells[i]->class_type == MAT_C_CELL )
                bytesread+=ReadNextCell(mat,cells[i]);
            fseek(mat->fp,cells[i]->datapos+nBytes,SEEK_SET);
        }
    }

    return bytesread;
}

/*
 * Reads the next struct fields (fieldname length,names,data headers for all
 * the fields
 */
static int
ReadNextStructField( mat_t *mat, matvar_t *matvar )
{
    int fieldname_size,nfields, bytesread = 0, i, err;
    matvar_t **fields = NULL;

    if ( matvar->compression ) {
#ifdef HAVE_ZLIB
        char    *ptr;
        mat_uint32_t uncomp_buf[16] = {0,};
        int      nbytes, j, nmemb = 1;
        mat_uint32_t array_flags; 

        for ( i = 0; i < matvar->rank; i++ )
            nmemb *= matvar->dims[i];

        /* Inflate Field name length */
        bytesread += InflateFieldNameLength(mat,matvar,uncomp_buf);
        if ( mat->byteswap ) {
            int32Swap(uncomp_buf);
            int32Swap(uncomp_buf+1);
        }
        if ( (uncomp_buf[0] & 0x0000ffff) == MAT_T_INT32 ) {
            fieldname_size = uncomp_buf[1];
        } else {
            Mat_Warning("Error getting fieldname size");
            return bytesread;
        }

        bytesread += InflateFieldNamesTag(mat,matvar,uncomp_buf);
        if ( mat->byteswap ) {
            int32Swap(uncomp_buf);
            int32Swap(uncomp_buf+1);
        }
        nfields = uncomp_buf[1];
        nfields = nfields / fieldname_size;
        matvar->data_size = sizeof(matvar_t *);
        matvar->nbytes    = nmemb*nfields*matvar->data_size;
        matvar->data      = malloc(matvar->nbytes);
        if ( !matvar->data )
            return 1;
        fields = (matvar_t **)matvar->data;
        if ( nfields*fieldname_size % 8 != 0 )
            i = 8-(nfields*fieldname_size % 8);
        else
            i = 0;
        ptr = malloc(nfields*fieldname_size+i);
        bytesread += InflateFieldNames(mat,matvar,ptr,nfields,fieldname_size,i);
        for ( i = 0; i < nfields; i++ ) {
            fields[i]       = calloc(1,sizeof(matvar_t));
            fields[i]->name = malloc(fieldname_size);
            memcpy(fields[i]->name,ptr+i*fieldname_size,fieldname_size);
            fields[i]->name[fieldname_size-1] = '\0';
        }
        for ( i = 1; i < nmemb; i++ ) {
            for ( j = 0; j < nfields; j++ ) {
                fields[i*nfields+j] = calloc(1,sizeof(matvar_t));
                fields[i*nfields+j]->name = strdup_printf("%s",fields[j]->name);
            }
        }

        for ( i = 0; i < nmemb*nfields; i++ ) {
            fields[i]->fpos = ftell(mat->fp)-matvar->z->avail_in;
            /* Read variable tag for struct field */
            bytesread += InflateVarTag(mat,matvar,uncomp_buf);
            if ( mat->byteswap ) {
                int32Swap(uncomp_buf);
                int32Swap(uncomp_buf+1);
            }
            nbytes = uncomp_buf[1];
            if ( uncomp_buf[0] != MAT_T_MATRIX ) {
                Mat_Critical("fields[%d], Uncompressed type not MAT_T_MATRIX",i);
                Mat_VarFree(fields[i]);
                fields[i] = NULL;
                continue;
            } else if ( nbytes == 0 ) {
                fields[i]->rank = 0;
                continue;
            }
            fields[i]->compression = 1;
            bytesread += InflateArrayFlags(mat,matvar,uncomp_buf);
            nbytes -= 16;
            if ( mat->byteswap ) {
                int32Swap(uncomp_buf);
                int32Swap(uncomp_buf+1);
                int32Swap(uncomp_buf+2);
                int32Swap(uncomp_buf+3);
            }
            /* Array Flags */
            if ( uncomp_buf[0] == MAT_T_UINT32 ) {
               array_flags = uncomp_buf[2];
               fields[i]->class_type  = (array_flags & MAT_F_CLASS_T);
               fields[i]->isComplex   = (array_flags & MAT_F_COMPLEX);
               fields[i]->isGlobal    = (array_flags & MAT_F_GLOBAL);
               fields[i]->isLogical   = (array_flags & MAT_F_LOGICAL);
               if ( fields[i]->class_type == MAT_C_SPARSE ) {
                   /* Need to find a more appropriate place to store nzmax */
                   fields[i]->nbytes      = uncomp_buf[3];
               }
            } else {
                Mat_Critical("Expected MAT_T_UINT32 for Array Tags, got %d",
                    uncomp_buf[0]);
                bytesread+=InflateSkip(mat,matvar->z,nbytes);
            }
            bytesread += InflateDimensions(mat,matvar,uncomp_buf);
            nbytes -= 8;
            if ( mat->byteswap ) {
                int32Swap(uncomp_buf);
                int32Swap(uncomp_buf+1);
            }
            /* Rank and Dimension */
            if ( uncomp_buf[0] == MAT_T_INT32 ) {
                int j = 0;

                fields[i]->rank = uncomp_buf[1];
                nbytes -= fields[i]->rank;
                fields[i]->rank /= 4;
                fields[i]->dims = malloc(fields[i]->rank*sizeof(int));
                if ( mat->byteswap ) {
                    for ( j = 0; j < fields[i]->rank; j++ )
                        fields[i]->dims[j] = int32Swap(uncomp_buf+2+j);
                } else {
                    for ( j = 0; j < fields[i]->rank; j++ )
                        fields[i]->dims[j] = uncomp_buf[2+j];
                }
                if ( fields[i]->rank % 2 != 0 )
                    nbytes -= 4;
            }
            bytesread += InflateVarNameTag(mat,matvar,uncomp_buf);
            nbytes -= 8;
            fields[i]->z = calloc(1,sizeof(z_stream));
            err = inflateCopy(fields[i]->z,matvar->z);
            if ( err != Z_OK ) {
                Mat_Critical("inflateCopy returned error %d",err);
            }
            fields[i]->datapos = ftell(mat->fp)-matvar->z->avail_in;
            if ( fields[i]->class_type == MAT_C_STRUCT )
                bytesread+=ReadNextStructField(mat,fields[i]);
            else if ( fields[i]->class_type == MAT_C_CELL )
                bytesread+=ReadNextCell(mat,fields[i]);
            fseek(mat->fp,fields[i]->datapos,SEEK_SET);
            bytesread+=InflateSkip(mat,matvar->z,nbytes);
        }
        free(ptr);
#else
        Mat_Critical("Not compiled with zlib support");
#endif
    } else {
        int fieldname_size,nfields;
        mat_uint32_t buf[16] = {0,};
        int      nbytes,nBytes,nmemb=1,j;
        mat_uint32_t array_flags; 

        for ( i = 0; i < matvar->rank; i++ )
            nmemb *= matvar->dims[i];

        bytesread+=fread(buf,4,2,mat->fp);
        if ( mat->byteswap ) {
            int32Swap(buf);
            int32Swap(buf+1);
        }
        if ( (buf[0] & 0x0000ffff) == MAT_T_INT32 ) {
            fieldname_size = buf[1];
        } else {
            Mat_Warning("Error getting fieldname size");
            return bytesread;
        }
        bytesread+=fread(buf,4,2,mat->fp);
        if ( mat->byteswap ) {
            int32Swap(buf);
            int32Swap(buf+1);
        }
        nfields = buf[1];
        nfields = nfields / fieldname_size;
        matvar->data_size = sizeof(matvar_t *);
        matvar->nbytes    = nmemb*nfields*matvar->data_size;
        matvar->data = malloc(matvar->nbytes);
        if ( !matvar->data )
            return bytesread;
        fields = (matvar_t **)matvar->data;
        for ( i = 0; i < nfields; i++ ) {
            fields[i] = (matvar_t *)calloc(1,sizeof(matvar_t));
            fields[i]->name = (char *)malloc(fieldname_size);
            bytesread+=fread(fields[i]->name,1,fieldname_size,mat->fp);
            fields[i]->name[fieldname_size-1] = '\0';
        }
        for ( i = 1; i < nmemb; i++ ) {
            for ( j = 0; j < nfields; j++ ) {
                fields[i*nfields+j] = (matvar_t *)calloc(1,sizeof(matvar_t));
                fields[i*nfields+j]->name = strdup_printf("%s",fields[j]->name);
            }
        }
        if ( (nfields*fieldname_size) % 8 ) {
            fseek(mat->fp,8-((nfields*fieldname_size) % 8),SEEK_CUR);
            bytesread+=8-((nfields*fieldname_size) % 8);
        }
        for ( i = 0; i < nmemb*nfields; i++ ) {

            fields[i]->fpos = ftell(mat->fp);

            /* Read variable tag for struct field */
            bytesread += fread(buf,4,2,mat->fp);
            if ( mat->byteswap ) {
                int32Swap(buf);
                int32Swap(buf+1);
            }
            nBytes = buf[1];
            if ( buf[0] != MAT_T_MATRIX ) {
                Mat_Critical("fields[%d] not MAT_T_MATRIX, fpos = %ld",i,ftell(mat->fp));
                Mat_VarFree(fields[i]);
                fields[i] = NULL;
                return bytesread;
            } else if ( nBytes == 0 ) {
                fields[i]->rank = 0;
                continue;
            }
            fields[i]->compression = 0;
#ifdef HAVE_ZLIB
            fields[i]->z = NULL;
#endif

            /* Read Array Flags and The Dimensions Tag */
            bytesread  += fread(buf,4,6,mat->fp);
            if ( mat->byteswap ) {
                int32Swap(buf);
                int32Swap(buf+1);
                int32Swap(buf+2);
                int32Swap(buf+3);
                int32Swap(buf+4);
                int32Swap(buf+5);
            }
            nBytes-=24;
            /* Array Flags */
            if ( buf[0] == MAT_T_UINT32 ) {
               array_flags = buf[2];
               fields[i]->class_type  = (array_flags & MAT_F_CLASS_T);
               fields[i]->isComplex   = (array_flags & MAT_F_COMPLEX);
               fields[i]->isGlobal    = (array_flags & MAT_F_GLOBAL);
               fields[i]->isLogical   = (array_flags & MAT_F_LOGICAL);
               if ( fields[i]->class_type == MAT_C_SPARSE ) {
                   /* Need to find a more appropriate place to store nzmax */
                   fields[i]->nbytes      = buf[3];
               }
            }
            /* Rank and Dimension */
            if ( buf[4] == MAT_T_INT32 ) {
                int j;

                nbytes = buf[5];
                nBytes-=nbytes;

                fields[i]->rank = nbytes / 4;
                fields[i]->dims = (int *)malloc(fields[i]->rank*sizeof(int));

                /* Assumes rank <= 16 */
                if ( fields[i]->rank % 2 != 0 ) {
                    bytesread+=fread(buf,4,fields[i]->rank+1,mat->fp);
                    nBytes-=4;
                } else
                    bytesread+=fread(buf,4,fields[i]->rank,mat->fp);

                if ( mat->byteswap ) {
                    for ( j = 0; j < fields[i]->rank; j++ )
                        fields[i]->dims[j] = int32Swap(buf+j);
                } else {
                    for ( j = 0; j < fields[i]->rank; j++ )
                        fields[i]->dims[j] = buf[j];
                }
            }
            /* Variable Name Tag */
            bytesread+=fread(buf,1,8,mat->fp);
            nBytes-=8;
            fields[i]->datapos = ftell(mat->fp);
            if ( fields[i]->class_type == MAT_C_STRUCT )
                bytesread+=ReadNextStructField(mat,fields[i]);
            else if ( fields[i]->class_type == MAT_C_CELL )
                bytesread+=ReadNextCell(mat,fields[i]);
            fseek(mat->fp,fields[i]->datapos+nBytes,SEEK_SET);
        }
    }

    return bytesread;
}

/*
 * Reads the next struct fields (fieldname length,names,data headers for all
 * the fields
 */
static int
ReadNextFunctionHandle(mat_t *mat, matvar_t *matvar)
{
    int nfunctions = 1, bytesread = 0, i;
    matvar_t **functions = NULL;

    for ( i = 0; i < matvar->rank; i++ )
        nfunctions *= matvar->dims[i];

    matvar->data = malloc(nfunctions*sizeof(matvar_t *));
    if ( matvar->data != NULL ) {
        matvar->data_size = sizeof(matvar_t *);
        matvar->nbytes    = nfunctions*matvar->data_size;
        functions = matvar->data;
        for ( i = 0 ; i < nfunctions; i++ )
            functions[i] = Mat_VarReadNextInfo(mat);
    } else {
        bytesread = 0;
        matvar->data_size = 0;
        matvar->nbytes    = 0;
    }

    return bytesread;
}

/*
 * ----------------------------------------
 *  Write routines for struct/cell fields
 * ----------------------------------------
 */
static int WriteCellArrayFieldInfo( mat_t *mat, matvar_t *matvar, 
                             int compress )
{
    mat_uint32_t array_flags = 0x0; 
    mat_int16_t  array_name_type = MAT_T_INT8;
    int      array_flags_type = MAT_T_UINT32, dims_array_type = MAT_T_INT32;
    int      array_flags_size = 8, pad4 = 0, matrix_type = MAT_T_MATRIX;
    mat_int8_t   pad1 = 0;
    int      nBytes, i, nmemb = 1;
    long     start = 0, end = 0;

    if ((matvar == NULL) || (mat == NULL))
        return 0;

#if 0
    nBytes = GetMatrixMaxBufSize(matvar);
#endif

    fwrite(&matrix_type,4,1,mat->fp);
    fwrite(&pad4,4,1,mat->fp);
    start = ftell(mat->fp);

    /* Array Flags */
    if ( matvar->rank > 1 && ( matvar->dims[0] > 1 || matvar->dims[1] > 1 ) &&
         matvar->class_type == MAT_C_INT32 ) {
        array_flags = MAT_C_DOUBLE & MAT_F_CLASS_T;
    } else {
        array_flags = matvar->class_type & MAT_F_CLASS_T;
    }
    if ( matvar->isComplex )
        array_flags |= MAT_F_COMPLEX;
    if ( matvar->isGlobal )
        array_flags |= MAT_F_GLOBAL;
    if ( matvar->isLogical )
        array_flags |= MAT_F_LOGICAL;

    if ( mat->byteswap )
        array_flags = int32Swap((mat_int32_t*)&array_flags);
    fwrite(&array_flags_type,4,1,mat->fp);
    fwrite(&array_flags_size,4,1,mat->fp);
    fwrite(&array_flags,4,1,mat->fp);
    fwrite(&pad4,4,1,mat->fp);
    /* Rank and Dimension */
    nBytes = matvar->rank * 4;
    fwrite(&dims_array_type,4,1,mat->fp);
    fwrite(&nBytes,4,1,mat->fp);
    for ( i = 0; i < matvar->rank; i++ ) {
        mat_int32_t dim;
        dim = matvar->dims[i];
        nmemb *= dim;
        fwrite(&dim,4,1,mat->fp);
    }
    if ( matvar->rank % 2 != 0 )
        fwrite(&pad4,4,1,mat->fp);
    /* Name of variable */
    if ( !matvar->name ) {
        fwrite(&array_name_type,2,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&pad4,4,1,mat->fp);
    } else if ( strlen(matvar->name) <= 4 ) {
        mat_int16_t array_name_len = (mat_int16_t)strlen(matvar->name);
        mat_int8_t  pad1 = 0;
        fwrite(&array_name_type,2,1,mat->fp);
        fwrite(&array_name_len,2,1,mat->fp);
        fwrite(matvar->name,1,array_name_len,mat->fp);
        for ( i = array_name_len; i < 4; i++ )
            fwrite(&pad1,1,1,mat->fp);
    } else {
        mat_int32_t array_name_len = (mat_int32_t)strlen(matvar->name);
        mat_int8_t  pad1 = 0;

        fwrite(&array_name_type,2,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&array_name_len,4,1,mat->fp);
        fwrite(matvar->name,1,array_name_len,mat->fp);
        if ( array_name_len % 8 )
            for ( i = array_name_len % 8; i < 8; i++ )
                fwrite(&pad1,1,1,mat->fp);
    }

    matvar->datapos = ftell(mat->fp);
    switch ( matvar->class_type ) {
        case MAT_C_DOUBLE:
        case MAT_C_SINGLE:
        case MAT_C_INT32:
        case MAT_C_UINT32:
        case MAT_C_INT16:
        case MAT_C_UINT16:
        case MAT_C_INT8:
        case MAT_C_UINT8:
            nBytes = WriteEmptyData(mat,nmemb,matvar->data_type);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            if ( matvar->isComplex ) {
                nBytes = WriteEmptyData(mat,nmemb,matvar->data_type);
                if ( nBytes % 8 )
                    for ( i = nBytes % 8; i < 8; i++ )
                        fwrite(&pad1,1,1,mat->fp);
            }
            break;
        case MAT_C_CHAR:
        {
            WriteEmptyCharData(mat,nmemb,matvar->data_type);
            break;
        }
        case MAT_C_CELL:
        {
            int nfields = matvar->nbytes / matvar->data_size;
            matvar_t **fields = (matvar_t **)matvar->data;

            for ( i = 0; i < nfields; i++ )
                WriteCellArrayFieldInfo(mat,fields[i],compress);
            break;
        }
        /* FIXME: Structures */
    }
    end = ftell(mat->fp);
    nBytes = (int)(end-start);
    fseek(mat->fp,(long)-(nBytes+4),SEEK_CUR);
    fwrite(&nBytes,4,1,mat->fp);
    fseek(mat->fp,end,SEEK_SET);
    return 0;
}

static int
WriteCellArrayField(mat_t *mat,matvar_t *matvar,int compress )
{
    mat_uint32_t array_flags = 0x0; 
    mat_int16_t  array_name_type = MAT_T_INT8,fieldname_type = MAT_T_INT32,fieldname_data_size=4;
    int      array_flags_type = MAT_T_UINT32, dims_array_type = MAT_T_INT32;
    int      array_flags_size = 8, pad4 = 0, matrix_type = MAT_T_MATRIX;
    mat_int8_t   pad1 = 0;
    int      nBytes, i, nmemb = 1, nzmax = 0;
    long     start = 0, end = 0;

    if ((matvar == NULL) || (mat == NULL))
        return 0;

#if 0
    nBytes = GetMatrixMaxBufSize(matvar);
#endif

    fwrite(&matrix_type,4,1,mat->fp);
    fwrite(&pad4,4,1,mat->fp);
    start = ftell(mat->fp);

    /* Array Flags */
    if ( matvar->rank > 1 && ( matvar->dims[0] > 1 || matvar->dims[1] > 1 ) &&
         matvar->class_type == MAT_C_INT32 ) {
        array_flags = MAT_C_DOUBLE & MAT_F_CLASS_T;
    } else {
        array_flags = matvar->class_type & MAT_F_CLASS_T;
    }
    if ( matvar->isComplex )
        array_flags |= MAT_F_COMPLEX;
    if ( matvar->isGlobal )
        array_flags |= MAT_F_GLOBAL;
    if ( matvar->isLogical )
        array_flags |= MAT_F_LOGICAL;
    if ( matvar->class_type == MAT_C_SPARSE )
        nzmax = ((sparse_t *)matvar->data)->nzmax;

    if ( mat->byteswap )
        array_flags = int32Swap((mat_int32_t*)&array_flags);
    fwrite(&array_flags_type,4,1,mat->fp);
    fwrite(&array_flags_size,4,1,mat->fp);
    fwrite(&array_flags,4,1,mat->fp);
    fwrite(&nzmax,4,1,mat->fp);
    /* Rank and Dimension */
    nBytes = matvar->rank * 4;
    fwrite(&dims_array_type,4,1,mat->fp);
    fwrite(&nBytes,4,1,mat->fp);
    for ( i = 0; i < matvar->rank; i++ ) {
        mat_int32_t dim;
        dim = matvar->dims[i];
        nmemb *= dim;
        fwrite(&dim,4,1,mat->fp);
    }
    if ( matvar->rank % 2 != 0 )
        fwrite(&pad4,4,1,mat->fp);
    /* Name of variable */
    if ( !matvar->name ) {
        fwrite(&array_name_type,2,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&pad4,4,1,mat->fp);
    } else if ( strlen(matvar->name) <= 4 ) {
        mat_int16_t array_name_len = (mat_int16_t)strlen(matvar->name);
        mat_int8_t  pad1 = 0;
        fwrite(&array_name_type,2,1,mat->fp);
        fwrite(&array_name_len,2,1,mat->fp);
        fwrite(matvar->name,1,array_name_len,mat->fp);
        for ( i = array_name_len; i < 4; i++ )
            fwrite(&pad1,1,1,mat->fp);
    } else {
        mat_int32_t array_name_len = (mat_int32_t)strlen(matvar->name);
        mat_int8_t  pad1 = 0;

        fwrite(&array_name_type,2,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&array_name_len,4,1,mat->fp);
        fwrite(matvar->name,1,array_name_len,mat->fp);
        if ( array_name_len % 8 )
            for ( i = array_name_len % 8; i < 8; i++ )
                fwrite(&pad1,1,1,mat->fp);
    }

    switch ( matvar->class_type ) {
        case MAT_C_DOUBLE:
        case MAT_C_SINGLE:
        case MAT_C_INT32:
        case MAT_C_UINT32:
        case MAT_C_INT16:
        case MAT_C_UINT16:
        case MAT_C_INT8:
        case MAT_C_UINT8:
        {
            mat_uint8_t *ptr;

            ptr = matvar->data;
            nBytes = WriteData(mat,matvar->data,nmemb,matvar->data_type);
            ptr += nBytes;
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            if ( matvar->isComplex ) {
                nBytes = WriteData(mat,ptr,nmemb,matvar->data_type);
                if ( nBytes % 8 )
                    for ( i = nBytes % 8; i < 8; i++ )
                        fwrite(&pad1,1,1,mat->fp);
            }
            break;
        }
        case MAT_C_CHAR:
            WriteCharData(mat,matvar->data,nmemb,matvar->data_type);
            break;
        case MAT_C_CELL:
        {
            int nfields = matvar->nbytes / matvar->data_size;
            matvar_t **fields = (matvar_t **)matvar->data;

            for ( i = 0; i < nfields; i++ )
                WriteCellArrayField(mat,fields[i],compress);
            break;
        }
        case MAT_C_STRUCT:
        {
            char **fieldnames, *padzero;
            int    fieldname_size, nfields;
            size_t maxlen = 0;
            matvar_t **fields = (matvar_t **)matvar->data;

            nfields = matvar->nbytes / (nmemb*matvar->data_size);
            fieldnames = (char **)malloc(nfields*sizeof(char *));
            for ( i = 0; i < nfields; i++ ) {
                fieldnames[i] = fields[i]->name;
                if ( strlen(fieldnames[i]) > maxlen )
                    maxlen = strlen(fieldnames[i]);
            }
            maxlen++;
            fieldname_size = maxlen;
            while ( nfields*fieldname_size % 8 != 0 )
                fieldname_size++;
            fwrite(&fieldname_type,2,1,mat->fp);
            fwrite(&fieldname_data_size,2,1,mat->fp);
            fwrite(&fieldname_size,4,1,mat->fp);
            fwrite(&array_name_type,2,1,mat->fp);
            fwrite(&pad1,1,1,mat->fp);
            fwrite(&pad1,1,1,mat->fp);
            nBytes = nfields*fieldname_size;
            fwrite(&nBytes,4,1,mat->fp);
            padzero = (char *)calloc(fieldname_size,1);
            for ( i = 0; i < nfields; i++ ) {
                fwrite(fieldnames[i],1,strlen(fieldnames[i]),mat->fp);
                fwrite(padzero,1,fieldname_size-strlen(fieldnames[i]),mat->fp);
            }
            free(fieldnames);
            for ( i = 0; i < nmemb*nfields; i++ )
                WriteStructField(mat,fields[i],compress);
            break;
        }
        case MAT_C_SPARSE:
        {
            sparse_t *sparse = matvar->data;

            nBytes = WriteData(mat,sparse->ir,sparse->nir,MAT_T_INT32);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            nBytes = WriteData(mat,sparse->jc,sparse->njc,MAT_T_INT32);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            nBytes = WriteData(mat,sparse->data,sparse->ndata,matvar->data_type);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
        }
    }
    end = ftell(mat->fp);
    nBytes = (int)(end-start);
    fseek(mat->fp,(long)-(nBytes+4),SEEK_CUR);
    fwrite(&nBytes,4,1,mat->fp);
    fseek(mat->fp,end,SEEK_SET);
    return 0;
}

static int WriteStructField( mat_t *mat, matvar_t *matvar, 
                             int compress )
{
    mat_uint32_t array_flags = 0x0; 
    mat_int16_t  array_name_type = MAT_T_INT8,fieldname_type = MAT_T_INT32,fieldname_data_size=4;
    int      array_flags_type = MAT_T_UINT32, dims_array_type = MAT_T_INT32;
    int      array_flags_size = 8, pad4 = 0, matrix_type = MAT_T_MATRIX;
    mat_int8_t   pad1 = 0;
    int      nBytes, i, nmemb = 1, nzmax = 0;
    long     start = 0, end = 0;

    if ( (matvar == NULL) || ( mat == NULL ))
        return 1;

#if 0
    nBytes = GetMatrixMaxBufSize(matvar);
#endif

    fwrite(&matrix_type,4,1,mat->fp);
    fwrite(&pad4,4,1,mat->fp);
    start = ftell(mat->fp);

    /* Array Flags */

    array_flags = matvar->class_type & MAT_F_CLASS_T;
    if ( matvar->isComplex )
        array_flags |= MAT_F_COMPLEX;
    if ( matvar->isGlobal )
        array_flags |= MAT_F_GLOBAL;
    if ( matvar->isLogical )
        array_flags |= MAT_F_LOGICAL;
    if ( matvar->class_type == MAT_C_SPARSE )
        nzmax = ((sparse_t *)matvar->data)->nzmax;

    if ( mat->byteswap )
        array_flags = int32Swap((mat_int32_t*)&array_flags);
    fwrite(&array_flags_type,4,1,mat->fp);
    fwrite(&array_flags_size,4,1,mat->fp);
    fwrite(&array_flags,4,1,mat->fp);
    fwrite(&nzmax,4,1,mat->fp);
    /* Rank and Dimension */
    nBytes = matvar->rank * 4;
    fwrite(&dims_array_type,4,1,mat->fp);
    fwrite(&nBytes,4,1,mat->fp);
    for ( i = 0; i < matvar->rank; i++ ) {
        mat_int32_t dim;
        dim = matvar->dims[i];
        nmemb *= dim;
        fwrite(&dim,4,1,mat->fp);
    }
    if ( matvar->rank % 2 != 0 )
        fwrite(&pad4,4,1,mat->fp);

    fwrite(&array_name_type,2,1,mat->fp);
    fwrite(&pad1,1,1,mat->fp);
    fwrite(&pad1,1,1,mat->fp);
    fwrite(&pad4,4,1,mat->fp);

    switch ( matvar->class_type ) {
        case MAT_C_DOUBLE:
        case MAT_C_SINGLE:
        case MAT_C_INT32:
        case MAT_C_UINT32:
        case MAT_C_INT16:
        case MAT_C_UINT16:
        case MAT_C_INT8:
        case MAT_C_UINT8:
        {
            mat_uint8_t *ptr;

            ptr = matvar->data;
            nBytes = WriteData(mat,matvar->data,nmemb,matvar->data_type);
            ptr += nBytes;
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            if ( matvar->isComplex ) {
                nBytes = WriteData(mat,ptr,nmemb,matvar->data_type);
                if ( nBytes % 8 )
                    for ( i = nBytes % 8; i < 8; i++ )
                        fwrite(&pad1,1,1,mat->fp);
            }
            break;
        }
        case MAT_C_CHAR:
            nBytes = WriteCharData(mat,matvar->data,nmemb,matvar->data_type);
            break;
        case MAT_C_CELL:
        {
            int nfields = matvar->nbytes / matvar->data_size;
            matvar_t **fields = (matvar_t **)matvar->data;

            for ( i = 0; i < nfields; i++ )
                WriteCellArrayField(mat,fields[i],compress);
            break;
        }
        case MAT_C_STRUCT:
        {
            char **fieldnames, *padzero;
            int    fieldname_size, nfields;
            size_t maxlen = 0;
            matvar_t **fields = (matvar_t **)matvar->data;

            nfields = matvar->nbytes / (nmemb*matvar->data_size);
            fieldnames = (char **)malloc(nfields*sizeof(char *));
            for ( i = 0; i < nfields; i++ ) {
                fieldnames[i] = fields[i]->name;
                if ( strlen(fieldnames[i]) > maxlen )
                    maxlen = strlen(fieldnames[i]);
            }
            maxlen++;
            fieldname_size = maxlen;
            while ( nfields*fieldname_size % 8 != 0 )
                fieldname_size++;
            fwrite(&fieldname_type,2,1,mat->fp);
            fwrite(&fieldname_data_size,2,1,mat->fp);
            fwrite(&fieldname_size,4,1,mat->fp);
            fwrite(&array_name_type,2,1,mat->fp);
            fwrite(&pad1,1,1,mat->fp);
            fwrite(&pad1,1,1,mat->fp);
            nBytes = nfields*fieldname_size;
            fwrite(&nBytes,4,1,mat->fp);
            padzero = (char *)calloc(fieldname_size,1);
            for ( i = 0; i < nfields; i++ ) {
                fwrite(fieldnames[i],1,strlen(fieldnames[i]),mat->fp);
                fwrite(padzero,1,fieldname_size-strlen(fieldnames[i]),mat->fp);
            }
            free(fieldnames);
            free(padzero);
            for ( i = 0; i < nmemb*nfields; i++ )
                WriteStructField(mat,fields[i],compress);
            break;
        }
        case MAT_C_SPARSE:
        {
            sparse_t *sparse = matvar->data;

            nBytes = WriteData(mat,sparse->ir,sparse->nir,MAT_T_INT32);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            nBytes = WriteData(mat,sparse->jc,sparse->njc,MAT_T_INT32);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            nBytes = WriteData(mat,sparse->data,sparse->ndata,matvar->data_type);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
        }
    }
    end = ftell(mat->fp);
    nBytes = (int)(end-start);
    fseek(mat->fp,(long)-(nBytes+4),SEEK_CUR);
    fwrite(&nBytes,4,1,mat->fp);
    fseek(mat->fp,end,SEEK_SET);
    return 0;
}

static void
ReadData(mat_t *mat, matvar_t *matvar)
{
    int nBytes, len = 0, i, byteswap, packed_type, data_in_tag = 0;
    long fpos;
    mat_uint32_t tag[2];

    if ( matvar == NULL )
        return;
    else if ( matvar->rank == 0 )        /* An empty data set */
        return;

    fpos = ftell(mat->fp);
    len = 1;
    byteswap = mat->byteswap;
    switch ( matvar->class_type ) {
        case MAT_C_DOUBLE:
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                fseek(mat->fp,matvar->datapos,SEEK_SET);

                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( byteswap )
                    int32Swap(tag);

                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    InflateDataType(mat,matvar,tag+1);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
#endif
            } else {
                fseek(mat->fp,matvar->datapos,SEEK_SET);
                fread(tag,4,1,mat->fp);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(tag+1,4,1,mat->fp);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
            }
            if ( nBytes == 0 ) {
                matvar->nbytes = 0;
                break;
            }
            for ( i = 0; i < matvar->rank; i++ )
                len *= matvar->dims[i];
            matvar->data_size = sizeof(double);
            matvar->data_type = MAT_T_DOUBLE;
            if ( matvar->isComplex ) {
                matvar->nbytes = 2*len*matvar->data_size;
                matvar->data = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_NONE) {
                    nBytes = ReadDoubleData(mat,(double*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                    fseek(mat->fp,4,SEEK_CUR);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        fseek(mat->fp,4,SEEK_CUR);
                    nBytes = ReadDoubleData(mat,(double*)matvar->data+len,
                               packed_type,len);
                } else if ( matvar->compression == COMPRESSION_ZLIB ) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedDoubleData(mat,matvar->z,
                                 (double*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
                    InflateSkip(mat,matvar->z,4);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        InflateSkip(mat,matvar->z,4);
                    nBytes = ReadCompressedDoubleData(mat,matvar->z,
                                 (double*)matvar->data+len,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                }
            } else {
                matvar->nbytes = len*matvar->data_size;
                matvar->data   = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedDoubleData(mat,matvar->z,
                                 (double*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadDoubleData(mat,(double*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            }
            break;
        case MAT_C_SINGLE:
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                fseek(mat->fp,matvar->datapos,SEEK_SET);

                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( byteswap )
                    int32Swap(tag);

                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    InflateDataType(mat,matvar,tag+1);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
#endif
            } else {
                fseek(mat->fp,matvar->datapos,SEEK_SET);
                fread(tag,4,1,mat->fp);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(tag+1,4,1,mat->fp);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
            }
            if ( nBytes == 0 ) {
                matvar->nbytes = 0;
                break;
            }
            for ( i = 0; i < matvar->rank; i++ )
                len *= matvar->dims[i];
            matvar->data_size = sizeof(float);
            matvar->data_type = MAT_T_SINGLE;
            if ( matvar->isComplex ) {
                matvar->nbytes = 2*len*matvar->data_size;
                matvar->data = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_NONE) {
                    nBytes = ReadSingleData(mat,(float*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                    fseek(mat->fp,4,SEEK_CUR);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        fseek(mat->fp,4,SEEK_CUR);
                    nBytes = ReadSingleData(mat,(float*)matvar->data+len,
                               packed_type,len);
                } else if ( matvar->compression == COMPRESSION_ZLIB ) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedSingleData(mat,matvar->z,
                                 (float*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
                    InflateSkip(mat,matvar->z,4);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        InflateSkip(mat,matvar->z,4);
                    nBytes = ReadCompressedSingleData(mat,matvar->z,
                                 (float*)matvar->data+len,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                }
            } else {
                matvar->nbytes = len*matvar->data_size;
                matvar->data = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedSingleData(mat,matvar->z,
                                 (float*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadSingleData(mat,(float*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            }
            break;
        case MAT_C_INT32:
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                fseek(mat->fp,matvar->datapos,SEEK_SET);

                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( byteswap )
                    int32Swap(tag);

                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    InflateDataType(mat,matvar,tag+1);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
#endif
            } else {
                fseek(mat->fp,matvar->datapos,SEEK_SET);
                fread(tag,4,1,mat->fp);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(tag+1,4,1,mat->fp);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
            }
            if ( nBytes == 0 ) {
                matvar->nbytes = 0;
                break;
            }
            for ( i = 0; i < matvar->rank; i++ )
                len *= matvar->dims[i];
            matvar->data_size = sizeof(mat_int32_t);
            matvar->data_type = MAT_T_INT32;
            if ( matvar->isComplex ) {
                matvar->nbytes = 2*len*matvar->data_size;
                matvar->data = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_NONE) {
                    nBytes = ReadInt32Data(mat,(mat_int32_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                    fseek(mat->fp,4,SEEK_CUR);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        fseek(mat->fp,4,SEEK_CUR);
                    nBytes = ReadInt32Data(mat,(mat_int32_t*)matvar->data+len,
                               packed_type,len);
                } else if ( matvar->compression == COMPRESSION_ZLIB ) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt32Data(mat,matvar->z,
                                 (mat_int32_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
                    InflateSkip(mat,matvar->z,4);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        InflateSkip(mat,matvar->z,4);
                    nBytes = ReadCompressedInt32Data(mat,matvar->z,
                                 (mat_int32_t*)matvar->data+len,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                }
            } else {
                matvar->nbytes = len*matvar->data_size;
                matvar->data   = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt32Data(mat,matvar->z,
                                 (mat_int32_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadInt32Data(mat,(mat_int32_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            }
            break;
        case MAT_C_UINT32:
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                fseek(mat->fp,matvar->datapos,SEEK_SET);

                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    InflateDataType(mat,matvar,tag+1);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
#endif
            } else {
                fseek(mat->fp,matvar->datapos,SEEK_SET);
                fread(tag,4,1,mat->fp);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(tag+1,4,1,mat->fp);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
            }
            if ( nBytes == 0 ) {
                matvar->nbytes = 0;
                break;
            }
            for ( i = 0; i < matvar->rank; i++ )
                len *= matvar->dims[i];
            matvar->data_size = sizeof(mat_uint32_t);
            matvar->data_type = MAT_T_UINT32;
            if ( matvar->isComplex ) {
                matvar->nbytes = 2*len*matvar->data_size;
                matvar->data = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_NONE) {
                    nBytes = ReadInt32Data(mat,(mat_int32_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                    fseek(mat->fp,4,SEEK_CUR);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        fseek(mat->fp,4,SEEK_CUR);
                    nBytes = ReadInt32Data(mat,(mat_int32_t*)matvar->data+len,
                               packed_type,len);
                } else if ( matvar->compression == COMPRESSION_ZLIB ) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt32Data(mat,matvar->z,
                                 (mat_int32_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
                    InflateSkip(mat,matvar->z,4);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        InflateSkip(mat,matvar->z,4);
                    nBytes = ReadCompressedInt32Data(mat,matvar->z,
                                 (mat_int32_t*)matvar->data+len,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                }
            } else {
                matvar->nbytes = len*matvar->data_size;
                matvar->data   = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt32Data(mat,matvar->z,
                                 (mat_int32_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadInt32Data(mat,(mat_int32_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            }
            break;
        case MAT_C_INT16:
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                fseek(mat->fp,matvar->datapos,SEEK_SET);

                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    InflateDataType(mat,matvar,tag+1);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
#endif
            } else {
                fseek(mat->fp,matvar->datapos,SEEK_SET);
                fread(tag,4,1,mat->fp);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(tag+1,4,1,mat->fp);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
            }
            if ( nBytes == 0 ) {
                matvar->nbytes = 0;
                break;
            }
            for ( i = 0; i < matvar->rank; i++ )
                len *= matvar->dims[i];
            matvar->data_size = sizeof(mat_int16_t);
            matvar->data_type = MAT_T_INT16;
            if ( matvar->isComplex ) {
                matvar->nbytes = 2*len*matvar->data_size;
                matvar->data = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_NONE) {
                    nBytes = ReadInt16Data(mat,(mat_int16_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                    fseek(mat->fp,4,SEEK_CUR);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        fseek(mat->fp,4,SEEK_CUR);
                    nBytes = ReadInt16Data(mat,(mat_int16_t*)matvar->data+len,
                               packed_type,len);
                } else if ( matvar->compression == COMPRESSION_ZLIB ) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt16Data(mat,matvar->z,
                                 (mat_int16_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
                    InflateSkip(mat,matvar->z,4);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        InflateSkip(mat,matvar->z,4);
                    nBytes = ReadCompressedInt16Data(mat,matvar->z,
                                 (mat_int16_t*)matvar->data+len,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                }
            } else {
                matvar->nbytes = len*matvar->data_size;
                matvar->data   = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt16Data(mat,matvar->z,
                                 (mat_int16_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadInt16Data(mat,(mat_int16_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            }
            break;
        case MAT_C_UINT16:
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                fseek(mat->fp,matvar->datapos,SEEK_SET);

                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    InflateDataType(mat,matvar,tag+1);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
#endif
            } else {
                fseek(mat->fp,matvar->datapos,SEEK_SET);
                fread(tag,4,1,mat->fp);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(tag+1,4,1,mat->fp);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
            }
            if ( nBytes == 0 ) {
                matvar->nbytes = 0;
                break;
            }
            for ( i = 0; i < matvar->rank; i++ )
                len *= matvar->dims[i];
            matvar->data_size = sizeof(mat_uint16_t);
            matvar->data_type = MAT_T_UINT16;
            if ( matvar->isComplex ) {
                matvar->nbytes = 2*len*matvar->data_size;
                matvar->data = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_NONE) {
                    nBytes = ReadInt16Data(mat,(mat_int16_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                    fseek(mat->fp,4,SEEK_CUR);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        fseek(mat->fp,4,SEEK_CUR);
                    nBytes = ReadInt16Data(mat,(mat_int16_t*)matvar->data+len,
                               packed_type,len);
                } else if ( matvar->compression == COMPRESSION_ZLIB ) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt16Data(mat,matvar->z,
                                 (mat_int16_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
                    InflateSkip(mat,matvar->z,4);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        InflateSkip(mat,matvar->z,4);
                    nBytes = ReadCompressedInt16Data(mat,matvar->z,
                                 (mat_int16_t*)matvar->data+len,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                }
            } else {
                matvar->nbytes = len*matvar->data_size;
                matvar->data   = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt16Data(mat,matvar->z,
                                 (mat_int16_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadInt16Data(mat,(mat_int16_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            }
            break;
        case MAT_C_INT8:
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                fseek(mat->fp,matvar->datapos,SEEK_SET);

                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    InflateDataType(mat,matvar,tag+1);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
#endif
            } else {
                fseek(mat->fp,matvar->datapos,SEEK_SET);
                fread(tag,4,1,mat->fp);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(tag+1,4,1,mat->fp);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
            }
            if ( nBytes == 0 ) {
                matvar->nbytes = 0;
                break;
            }
            for ( i = 0; i < matvar->rank; i++ )
                len *= matvar->dims[i];
            matvar->data_size = sizeof(mat_int8_t);
            matvar->data_type = MAT_T_INT8;
            if ( matvar->isComplex ) {
                matvar->nbytes = 2*len*matvar->data_size;
                matvar->data = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_NONE) {
                    nBytes = ReadInt8Data(mat,(mat_int8_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                    fseek(mat->fp,4,SEEK_CUR);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        fseek(mat->fp,4,SEEK_CUR);
                    nBytes = ReadInt8Data(mat,(mat_int8_t*)matvar->data+len,
                               packed_type,len);
                } else if ( matvar->compression == COMPRESSION_ZLIB ) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt8Data(mat,matvar->z,
                                 (mat_int8_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
                    InflateSkip(mat,matvar->z,4);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        InflateSkip(mat,matvar->z,4);
                    nBytes = ReadCompressedInt8Data(mat,matvar->z,
                                 (mat_int8_t*)matvar->data+len,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                }
            } else {
                matvar->nbytes = len*matvar->data_size;
                matvar->data   = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt8Data(mat,matvar->z,
                                 (mat_int8_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadInt8Data(mat,(mat_int8_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            }
            break;
        case MAT_C_UINT8:
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                fseek(mat->fp,matvar->datapos,SEEK_SET);

                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    InflateDataType(mat,matvar,tag+1);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
#endif
            } else {
                fseek(mat->fp,matvar->datapos,SEEK_SET);
                fread(tag,4,1,mat->fp);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(tag+1,4,1,mat->fp);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
            }
            if ( nBytes == 0 ) {
                matvar->nbytes = 0;
                break;
            }
            for ( i = 0; i < matvar->rank; i++ )
                len *= matvar->dims[i];
            matvar->data_size = sizeof(mat_uint8_t);
            matvar->data_type = MAT_T_UINT8;
            if ( matvar->isComplex ) {
                matvar->nbytes = 2*len*matvar->data_size;
                matvar->data = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_NONE) {
                    nBytes = ReadInt8Data(mat,(mat_int8_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                    fseek(mat->fp,4,SEEK_CUR);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        fseek(mat->fp,4,SEEK_CUR);
                    nBytes = ReadInt8Data(mat,(mat_int8_t*)matvar->data+len,
                               packed_type,len);
                } else if ( matvar->compression == COMPRESSION_ZLIB ) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt8Data(mat,matvar->z,
                                 (mat_int8_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
                    InflateSkip(mat,matvar->z,4);
                    /* If data is not in the tag, skip the bytes tag */
                    if ( !data_in_tag )
                        InflateSkip(mat,matvar->z,4);
                    nBytes = ReadCompressedInt8Data(mat,matvar->z,
                                 (mat_int8_t*)matvar->data+len,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                }
            } else {
                matvar->nbytes = len*matvar->data_size;
                matvar->data   = malloc(matvar->nbytes);
                if ( !matvar->data ) {
                    Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                    break;
                }
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt8Data(mat,matvar->z,
                                 (mat_int8_t*)matvar->data,packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadInt8Data(mat,(mat_int8_t*)matvar->data,
                                 packed_type,len);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            }
            break;
            break;
        case MAT_C_CHAR:
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                fseek(mat->fp,matvar->datapos,SEEK_SET);

                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    InflateDataType(mat,matvar,tag+1);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
#endif
            } else {
                fseek(mat->fp,matvar->datapos,SEEK_SET);
                fread(tag,4,1,mat->fp);
                if ( byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(tag+1,4,1,mat->fp);
                    if ( byteswap )
                        int32Swap(tag+1);
                    nBytes = tag[1];
                }
            }
            if ( nBytes == 0 ) {
                matvar->nbytes = 0;
                break;
            }
            for ( i = 0; i < matvar->rank; i++ )
                len *= matvar->dims[i];
            matvar->data_size = sizeof(char);
            /* FIXME: */
            matvar->data_type = MAT_T_UINT8;
            matvar->nbytes = len*matvar->data_size;
            matvar->data   = malloc(matvar->nbytes+1);
            if ( !matvar->data ) {
                Mat_Critical("Failed to allocate %d bytes",matvar->nbytes);
                break;
            }
            if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                nBytes = ReadCompressedCharData(mat,matvar->z,
                             (char*)matvar->data,packed_type,len);
                if ( (nBytes % 8) != 0 )
                    InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
            } else {
                nBytes = ReadCharData(mat,(char*)matvar->data,packed_type,len);
                if ( (nBytes % 8) != 0 )
                    fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
            }
            break;
        case MAT_C_STRUCT:
        {
            matvar_t **fields;
            int nfields = 0;

            if ( !matvar->nbytes || !matvar->data_size )
                break;
            nfields = matvar->nbytes / matvar->data_size;
            fields = (matvar_t **)matvar->data;
            for ( i = 0; i < nfields; i++ ) {
                ReadData(mat,fields[i]);
            }
            /* FIXME: */
            matvar->data_type = MAT_T_STRUCT;
            break;
        }
        case MAT_C_CELL:
        {
            matvar_t **cells;

            if ( !matvar->data ) {
                Mat_Critical("Data is NULL for Cell Array %s",matvar->name);
                break;
            }
            for ( i = 0; i < matvar->rank; i++ )
                len *= matvar->dims[i];
            cells = (matvar_t **)matvar->data;
            for ( i = 0; i < len; i++ )
                ReadData(mat,cells[i]);
            /* FIXME: */
            matvar->data_type = MAT_T_CELL;
            break;
        }
        case MAT_C_SPARSE:
        {
            int N;
            sparse_t *data;

            matvar->data_size = sizeof(sparse_t);
            matvar->data      = malloc(matvar->data_size);
            if ( matvar->data == NULL ) {
                Mat_Critical("ReadData: Allocation of data pointer failed");
                break;
            }
            data = matvar->data;
            data->nzmax  = matvar->nbytes;
            fseek(mat->fp,matvar->datapos,SEEK_SET);
            /*  Read ir    */
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( mat->byteswap )
                    int32Swap(tag);

                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    (void)ReadCompressedInt32Data(mat,matvar->z,
                             (mat_int32_t*)&N,MAT_T_INT32,1);
                }
#endif
            } else {
                fread(tag,4,1,mat->fp);
                if ( mat->byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(&N,4,1,mat->fp);
                    if ( mat->byteswap )
                        int32Swap(&N);
                }
            }
            data->nir = N / 4;
            data->ir = malloc(data->nir*sizeof(mat_int32_t));
            if ( data->ir != NULL ) {
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt32Data(mat,matvar->z,
                                 data->ir,packed_type,data->nir);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadInt32Data(mat,data->ir,packed_type,data->nir);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            } else {
                Mat_Critical("ReadData: Allocation of ir pointer failed");
                break;
            }
            /*  Read jc    */
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( mat->byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    (void)ReadCompressedInt32Data(mat,matvar->z,
                             (mat_int32_t*)&N,MAT_T_INT32,1);
                }
#endif
            } else {
                fread(tag,4,1,mat->fp);
                if ( mat->byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(&N,4,1,mat->fp);
                    if ( mat->byteswap )
                        int32Swap(&N);
                }
            }
            data->njc = N / 4;
            data->jc = malloc(data->njc*sizeof(mat_int32_t));
            if ( data->jc != NULL ) {
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedInt32Data(mat,matvar->z,
                                 data->jc,packed_type,data->njc);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadInt32Data(mat,data->jc,packed_type,data->njc);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            } else {
                Mat_Critical("ReadData: Allocation of jc pointer failed");
                break;
            }
            /*  Read data    */
            if ( matvar->compression ) {
#ifdef HAVE_ZLIB
                matvar->z->avail_in = 0;
                InflateDataType(mat,matvar,tag);
                if ( mat->byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    (void)ReadCompressedInt32Data(mat,matvar->z,
                             (mat_int32_t*)&N,MAT_T_INT32,1);
                }
#endif
            } else {
                fread(tag,4,1,mat->fp);
                if ( mat->byteswap )
                    int32Swap(tag);
                packed_type = tag[0] & 0x000000ff;
                if ( tag[0] & 0xffff0000 ) { /* Data is in the tag */
                    data_in_tag = 1;
                    nBytes = (tag[0] & 0xffff0000) >> 16;
                } else {
                    fread(&N,4,1,mat->fp);
                    if ( mat->byteswap )
                        int32Swap(&N);
                }
            }
            matvar->data_type = MAT_T_DOUBLE;
            data->ndata = N / Mat_SizeOf(packed_type);
            data->data = malloc(data->ndata*Mat_SizeOf(MAT_T_DOUBLE));
            if ( data->data != NULL ) {
                if ( matvar->compression == COMPRESSION_ZLIB) {
#ifdef HAVE_ZLIB
                    nBytes = ReadCompressedDoubleData(mat,matvar->z,
                                 data->data,packed_type,data->ndata);
                    if ( (nBytes % 8) != 0 )
                        InflateSkip(mat,matvar->z,8-(nBytes % 8));
#else
										Mat_Critical("Not compiled with zlib support");
#endif
                } else {
                    nBytes = ReadDoubleData(mat,data->data,packed_type,
                                 data->ndata);
                    if ( (nBytes % 8) != 0 )
                        fseek(mat->fp,8-(nBytes % 8),SEEK_CUR);
                }
            } else {
                Mat_Critical("ReadData: Allocation of data pointer failed");
                break;
            }
            break;
        }
        case MAT_C_FUNCTION:
        {
            matvar_t **functions;
            int nfunctions = 0;

            if ( !matvar->nbytes || !matvar->data_size )
                break;
            nfunctions = matvar->nbytes / matvar->data_size;
            functions = (matvar_t **)matvar->data;
            for ( i = 0; i < nfunctions; i++ ) {
                ReadData(mat,functions[i]);
            }
            /* FIXME: */
            matvar->data_type = MAT_T_FUNCTION;
            break;
        }
        default:
            Mat_Critical("ReadData: %d is not a supported Class", matvar->class_type);
    }
    fseek(mat->fp,fpos,SEEK_SET);

    return;
}

/*
 *====================================================================
 *                 Public Functions
 *====================================================================
 */

/** @brief Creates a new Matlab MAT file
 *
 * Tries to create a new Matlab MAT file with the given name and optional
 * header string.  If no header string is given, the default string
 * is used containing the software, version, and date in it.  If a header
 * string is given, at most the first 116 characters is written to the file.
 * The given header string need not be the full 116 characters, but MUST be
 * NULL terminated.
 * @ingroup MAT
 * @param matname Name of MAT file to create
 * @param hdr_str Optional header string, NULL to use default
 * @return A pointer to the MAT file or NULL if it failed.  This is not a
 * simple FILE * and should not be used as one.
 */
mat_t *
Mat_Create( char *matname, char *hdr_str )
{
    FILE *fp = NULL;
    mat_int16_t endian = 0, version;
    char *endianchar;
    mat_t *mat = NULL;
    size_t err;
    time_t t;

    fp = fopen(matname,"wb");
    if ( !fp )
        return NULL;

    mat = (mat_t *)malloc(sizeof(mat_t));
    if ( !mat )
        return NULL;

    t = time(NULL);
    mat->fp = fp;
    mat->filename = strdup_printf("%s",matname);
    mat->mode     = MAT_ACC_RDWR;
    mat->byteswap = 0;
    mat->header = (char *)malloc(128);
    mat->subsys_offset = (char *)malloc(16);
    memset(mat->header,' ',128);
    if ( hdr_str == NULL ) {
        err = mat_snprintf(mat->header,116,"MATLAB 5.0 MAT-file, Platform: %s, "
                "Created By: libmatio v%d.%d.%d on %s", MATIO_PLATFORM,
                MATIO_MAJOR_VERSION, MATIO_MINOR_VERSION, MATIO_RELEASE_LEVEL,
                ctime(&t));
    } else {
        err = mat_snprintf(mat->header,116,"%s",hdr_str);
    }
    mat->header[err] = ' ';
    mat_snprintf(mat->subsys_offset,15,"            ");
    mat->version = (int)0x0100;
    endianchar = (char *)&endian;
    endianchar[0]='I';
    endianchar[1]='M';

    version = 0x0100;

    err = fwrite((void*)mat->header,1,116,mat->fp);
    err = fwrite((void*)mat->subsys_offset,1,8,mat->fp);
    err = fwrite((void*)&version,2,1,mat->fp);
    err = fwrite((void*)endianchar,2,1,mat->fp);

    return mat;
}

/** @brief Opens an existing Matlab MAT file
 *
 * Tries to open a Matlab MAT file with the given name
 * @ingroup MAT
 * @param matname Name of MAT file to open
 * @return A pointer to the MAT file or NULL if it failed.  This is not a
 * simple FILE * and should not be used as one.
 */
mat_t *
Mat_Open( char *matname, int mode )
{
    FILE *fp = NULL;
    mat_int16_t tmp, tmp2;
    int     err;
    mat_t *mat = NULL;

    if ( mode == MAT_ACC_RDONLY ) {
        fp = fopen( matname, "rb" );
        if ( !fp )
            return NULL;
    } else if ( mode == MAT_ACC_RDWR ) {
        fp = fopen( matname, "r+b" );
        if ( !fp ) {
            mat = Mat_Create(matname,NULL);
            return mat;
        }
    } else {
        mat = Mat_Create(matname,NULL);
        return mat;
    }

    mat = (mat_t *)malloc(sizeof(mat_t));
    if ( !mat ) {
        Mat_Critical("Couldn't allocate memory for the MAT file");
        return NULL;
    }

    mat->fp = fp;
    mat->header = (char *)malloc(128);
    mat->subsys_offset = (char *)malloc(8);
    err = fread(mat->header,1,116,fp);
    mat->header[116] = '\0';
    err = fread(mat->subsys_offset,1,8,fp);
    err = fread(&tmp2,2,1,fp);
    fread (&tmp,1,2,fp);

    mat->byteswap = -1;
    if (tmp == 0x4d49)
        mat->byteswap = 0;
    else if (tmp == 0x494d) {
        mat->byteswap = 1;
        int16Swap(&tmp2);
    }
    mat->version = (int)tmp2;

    if ( mat->byteswap < 0 ) {
        Mat_Critical("%s does not seem to be a valid MAT file",matname);
        free(mat->header);
        free(mat->subsys_offset);
        free(mat);
        mat=NULL;
    } else if ( mat->version != 0x0100 ) {
        Mat_Critical("%s is not a version 5 MAT file", matname);
        free(mat->header);
        free(mat->subsys_offset);
        free(mat);
        mat=NULL;
    } else {
        mat->filename = strdup_printf("%s",matname);
        mat->mode = mode;
    }

    return mat;
}

/** @brief Closes an open Matlab MAT file
 *
 * Closes the given Matlab MAT file and frees any memory with it.
 * @ingroup MAT
 * @param mat Pointer to the MAT file
 * @retval 0
 */
int
Mat_Close( mat_t *mat )
{
    fclose(mat->fp);
    if ( mat->header )
        free(mat->header);
    if ( mat->subsys_offset )
        free(mat->subsys_offset);
    if ( mat->filename )
        free(mat->filename);
    free(mat);
    return 0;
}

/** @brief Rewinds a Matlab MAT file to the first variable
 *
 * Rewinds a Matlab MAT file to the first variable
 * @ingroup MAT
 * @param mat Pointer to the MAT file
 * @retval 0 on success
 */
int
Mat_Rewind( mat_t *mat )
{
    return fseek(mat->fp,128,SEEK_SET);
}

size_t
Mat_SizeOfClass(int class_type)
{
    switch (class_type) {
        case MAT_C_DOUBLE:
            return sizeof(double);
        case MAT_C_SINGLE:
            return sizeof(float);
        case MAT_C_INT32:
            return sizeof(mat_int32_t);
        case MAT_C_UINT32:
            return sizeof(mat_uint32_t);
        case MAT_C_INT16:
            return sizeof(mat_int16_t);
        case MAT_C_UINT16:
            return sizeof(mat_uint16_t);
        case MAT_C_INT8:
            return sizeof(mat_int8_t);
        case MAT_C_UINT8:
            return sizeof(mat_uint8_t);
        case MAT_C_CHAR:
            return sizeof(mat_int16_t);
        default:
            return 0;
    }
}

/*
 *===================================================================
 *    MAT Variable Functions
 *===================================================================
 */

/** @brief Creates A MAT Variable with the given name and data
 *
 * Creates a MAT variable that can be written to a Matlab MAT file with the
 * given name, data type, dimensions and data.  Rank should always be 2 or more.
 * i.e. Scalar values would have rank=2 and dims[2] = {1,1}.  Data type is
 * one of the MAT_T types.  MAT adds MAT_T_STRUCT and MAT_T_CELL to create
 * Structures and Cell Arrays respectively.  For MAT_T_STRUCT, data should be a
 * NULL terminated array of matvar_t * variables (i.e. for a 3x2 structure with
 * 10 fields, there should be 61 matvar_t * variables where the last one is
 * NULL).  For cell arrays, the NULL termination isn't necessary.  So to create
 * a cell array of size 3x2, data would be the address of an array of 6
 * matvar_t * variables.
 *
 * EXAMPLE:
 *   To create a struct of size 3x2 with 3 fields:
 * @code
 *     int rank=2, dims[2] = {3,2}, nfields = 3;
 *     matvar_t **vars;
 *
 *     vars = malloc((3*2*nfields+1)*sizeof(matvar_t *));
 *     vars[0]             = Mat_VarCreate(...);
 *        :
 *     vars[3*2*nfields-1] = Mat_VarCreate(...);
 *     vars[3*2*nfields]   = NULL;
 * @endcode
 *
 * EXAMPLE:
 *   To create a cell array of size 3x2:
 * @code
 *     int rank=2, dims[2] = {3,2};
 *     matvar_t **vars;
 *
 *     vars = malloc(3*2*sizeof(matvar_t *));
 *     vars[0]             = Mat_VarCreate(...);
 *        :
 *     vars[5] = Mat_VarCreate(...);
 * @endcode
 *
 * @ingroup MAT
 * @param name Name of the variable to create
 * @param class_type class type of the variable in Matlab(one of the mx Classes)
 * @param data_type data type of the variable (one of the MAT_T_ Types)
 * @param rank Rank of the variable
 * @param dims array of dimensions of the variable of size rank
 * @param data pointer to the data
 * @param opt 0, or bitwise or of the following options:
 * - MEM_CONSERVE to just use the pointer to the data and not copy the data
 *       itself.  Note that the pointer should not be freed until you are done
 *       with the mat variable.  The Mat_VarFree function will NOT free
 *       data that was created with MEM_CONSERVE, so free it yourself.
 * - MAT_F_COMPLEX to specify that the data is complex.  The data variable should
 *       be a contigouse piece of memory with the real part written first and
 *       the imaginary second
 * - MAT_F_GLOBAL to assign the variable as a global variable
 * - MAT_F_LOGICAL to specify that it is a logical variable
 * @return A MAT variable that can be written to a file or otherwise used
 */
matvar_t *
Mat_VarCreate(char *name,int class_type,int data_type,int rank,int *dims,
      void *data, int opt)
{
    int i, nmemb = 1, nfields = 0;
    matvar_t *matvar = NULL;

    if (dims == NULL) return NULL;

    matvar = (matvar_t *)malloc(sizeof(matvar_t));
    if ( !matvar )
        return NULL;
    memset(matvar,0,sizeof(matvar_t));
    matvar->name = NULL;
    matvar->dims = NULL;
    matvar->data = NULL;
#ifdef HAVE_ZLIB
    matvar->z    = NULL;
#endif
    matvar->isComplex = opt & MAT_F_COMPLEX;
    matvar->isGlobal  = opt & MAT_F_GLOBAL;
    matvar->isLogical = opt & MAT_F_LOGICAL;
    if ( name )
        matvar->name = strdup_printf("%s",name);
    matvar->rank = rank;
    matvar->dims = (int *)malloc(matvar->rank*sizeof(int));
    for ( i = 0; i < matvar->rank; i++ ) {
        matvar->dims[i] = dims[i];
        nmemb *= dims[i];
    }
    matvar->class_type = class_type;
    matvar->data_type  = data_type;
    switch ( data_type ) {
        case MAT_T_INT8:
            matvar->data_size = 1;
            break;
        case MAT_T_UINT8:
            matvar->data_size = 1;
            break;
        case MAT_T_INT16:
            matvar->data_size = 2;
            break;
        case MAT_T_UINT16:
            matvar->data_size = 2;
            break;
        case MAT_T_INT32:
            matvar->data_size = 4;
            break;
        case MAT_T_UINT32:
            matvar->data_size = 4;
            break;
        case MAT_T_SINGLE:
            matvar->data_size = sizeof(float);
            break;
        case MAT_T_DOUBLE:
            matvar->data_size = sizeof(double);
            break;
        case MAT_T_INT64:
            matvar->data_size = 8;
            break;
        case MAT_T_UINT64:
            matvar->data_size = 8;
            break;
        case MAT_T_UTF8:
            matvar->data_size = 1;
            break;
        case MAT_T_UTF16:
            matvar->data_size = 2;
            break;
        case MAT_T_UTF32:
            matvar->data_size = 4;
            break;
        case MAT_T_CELL:
            matvar->data_size = sizeof(matvar_t **);
            break;
        case MAT_T_STRUCT:
        {
            matvar_t **fields;

            if ( data == NULL )
                break;
            fields = data;
            nfields = 0;
            while ( fields[nfields] != NULL )
                nfields++;
            matvar->data_size = sizeof(matvar_t **);
            nmemb = nfields;
            break;
        }
        default:
            Mat_Error("Unrecognized data_type");
            Mat_VarFree(matvar);
            return NULL;
    }
    if ( matvar->class_type == MAT_C_CHAR ) {
        matvar->data_size = 1;
        nmemb++;
    } else if ( matvar->class_type == MAT_C_SPARSE ) {
        matvar->data_size = sizeof(sparse_t);
    }
    if ( matvar->isComplex )
        matvar->nbytes = 2*nmemb*matvar->data_size;
    else
        matvar->nbytes = nmemb*matvar->data_size;
    if ( data == NULL ) {
        matvar->data = NULL;
    } else if ( opt & MEM_CONSERVE ) {
        matvar->data         = (void*)data;
        matvar->mem_conserve = 1;
    } else {
        matvar->data   = (char *)malloc(matvar->nbytes);
        memcpy(matvar->data,data,matvar->nbytes);
        matvar->mem_conserve = 0;
    }

    return matvar;
}

/** @brief Deletes a variable from a file
 *
 * @ingroup MAT
 * @param mat Pointer to the mat_t file structure
 * @param name Name of the variable to delete
 * @returns 0 on success
 */
int
Mat_VarDelete(mat_t *mat, char *name)
{
    int   err = 1;
    char *tmp_name, *new_name, *temp;
    mat_t *tmp;
    matvar_t *matvar;

    temp     = strdup_printf("XXXXXX");
    tmp_name = mktemp(temp);
    tmp      = Mat_Create(tmp_name,mat->header);
    if ( tmp != NULL ) {
        while ( NULL != (matvar = Mat_VarReadNext(mat)) ) {
            if ( strcmp(matvar->name,name) )
                Mat_VarWrite(tmp,matvar,0);
            else
                err = 0;
            Mat_VarFree(matvar);
        }
        /* FIXME: Memory leak */
        new_name = strdup_printf("%s",mat->filename);
        fclose(mat->fp);
		
        if ( (err = remove(new_name)) == -1 ) {
            Mat_Critical("remove of %s failed",new_name);
        } else if ( !Mat_Close(tmp) && (err=rename(tmp_name,new_name))==-1) {
            Mat_Critical("rename failed oldname=%s,newname=%s",tmp_name,
                new_name);
        } else {
            tmp = Mat_Open(new_name,mat->mode);
            memcpy(mat,tmp,sizeof(mat_t));
        }
        free(tmp);
        free(new_name);
    }
    free(temp);
    return err;
}

/** @brief Duplicates a matvar_t structure
 *
 * Provides a clean function for duplicating a matvar_t structure.
 * @ingroup MAT
 * @param in pointer to the matvar_t structure to be duplicated
 * @param opt 0 does a shallow duplicate and only assigns the data pointer to
 *            the duplicated array.  1 will do a deep duplicate and actually
 *            duplicate the contents of the data.  Warning: If you do a shallow
 *            copy and free both structures, the data will be freed twice and
 *            memory will be corrupted.  This may be fixed in a later release.
 * @returns Pointer to the duplicated matvar_t structure.
 */
matvar_t *
Mat_VarDuplicate(const matvar_t *in, int opt)
{
    matvar_t *out;
    int i;

    out = malloc(sizeof(matvar_t));
    if ( out == NULL )
        return NULL;

    out->nbytes       = in->nbytes;
    out->rank         = in->rank;
    out->data_type    = in->data_type;
    out->data_size    = in->data_size;
    out->class_type   = in->class_type;
    out->isComplex    = in->isComplex;
    out->isGlobal     = in->isGlobal;
    out->isLogical    = in->isLogical;
    out->mem_conserve = in->mem_conserve;
    out->compression  = in->compression;
    out->fpos         = in->fpos;
    out->datapos      = in->datapos;

    out->name = NULL;
    out->dims = NULL;
    out->data = NULL;
#ifdef HAVE_ZLIB
    out->z    = NULL;
#endif

    if (in->name != NULL && (NULL != (out->name = malloc(strlen(in->name)+1))))
        memcpy(out->name,in->name,strlen(in->name)+1);

    out->dims = malloc(in->rank*sizeof(int));
    if ( out->dims != NULL )
        memcpy(out->dims,in->dims,in->rank*sizeof(int));
#ifdef HAVE_ZLIB
    if ( (in->z != NULL) && (NULL != (out->z = malloc(sizeof(z_stream)))) )
        inflateCopy(out->z,in->z);
#endif

    if ( !opt ) {
        out->data = in->data;
    } else if ( (in->data != NULL) && (in->class_type == MAT_C_STRUCT) ) {
        matvar_t **infields, **outfields;
        int nfields = 0; 

        out->data = malloc(in->nbytes);
        if ( out->data != NULL && in->data_size > 0 ) {
            nfields   = in->nbytes / in->data_size;
            infields  = (matvar_t **)in->data;
            outfields = (matvar_t **)out->data;
            for ( i = 0; i < nfields; i++ ) {
                outfields[i] = Mat_VarDuplicate(infields[i],opt);
            } 
        }
    } else if ( (in->data != NULL) && (in->class_type == MAT_C_CELL) ) {
        matvar_t **incells, **outcells;
        int ncells = 0; 

        out->data = malloc(in->nbytes);
        if ( out->data != NULL && in->data_size > 0 ) {
            ncells   = in->nbytes / in->data_size;
            incells  = (matvar_t **)in->data;
            outcells = (matvar_t **)out->data;
            for ( i = 0; i < ncells; i++ ) {
                outcells[i] = Mat_VarDuplicate(incells[i],opt);
            } 
        }
    } else if ( in->data != NULL ) {
        out->data = malloc(in->nbytes);
        if ( out->data != NULL )
            memcpy(out->data,in->data,in->nbytes);
    }
    return out;
}

/** @brief Frees all the allocated memory associated with the structure
 *
 * Frees memory used by a MAT variable.  Frees the data associated with a
 * MAT variable if it's non-NULL and MEM_CONSERVE was not used.
 * @ingroup MAT
 * @param matvar Pointer to the matvar_t structure
 */
void
Mat_VarFree(matvar_t *matvar)
{
    if ( !matvar )
        return;
    if ( matvar->dims )
        free(matvar->dims);
    if ( matvar->name )
        free(matvar->name);
    if ( (matvar->data != NULL) && (matvar->class_type == MAT_C_STRUCT || 
          matvar->class_type == MAT_C_CELL) && matvar->data_size > 0 ) {
        int i;
        matvar_t **fields = (matvar_t **)matvar->data;
        int nfields = matvar->nbytes / matvar->data_size;
        for ( i = 0; i < nfields; i++ )
            Mat_VarFree(fields[i]);
        free(matvar->data);
    } else if ( (matvar->data != NULL) && (!matvar->mem_conserve) &&
                (matvar->class_type == MAT_C_SPARSE) ) {
        sparse_t *sparse;
        sparse = matvar->data;
        if ( sparse->ir != NULL )
            free(sparse->ir);
        if ( sparse->jc != NULL )
            free(sparse->jc);
        if ( sparse->data != NULL )
            free(sparse->data);
        free(sparse);
    } else {
        if ( matvar->data && !matvar->mem_conserve )
            free(matvar->data);
    }
#ifdef HAVE_ZLIB
    if ( matvar->compression == COMPRESSION_ZLIB ) {
        inflateEnd(matvar->z);
        free(matvar->z);
    }
#endif
    /* FIXME: Why does this cause a SEGV? */
#if 0
    memset(matvar,0,sizeof(matvar_t));
#endif
    free(matvar);
}

void
Mat_VarFree2(matvar_t *matvar)
{
    if ( !matvar )
        return;
    if ( matvar->dims )
        free(matvar->dims);
    if ( matvar->name )
        free(matvar->name);
    if ( (matvar->data != NULL) && (matvar->class_type == MAT_C_STRUCT || 
          matvar->class_type == MAT_C_CELL) && matvar->data_size > 0 ) {
        int i;
        matvar_t **fields = (matvar_t **)matvar->data;
        int nfields = matvar->nbytes / matvar->data_size;
        for ( i = 0; i < nfields; i++ )
            Mat_VarFree(fields[i]);
        free(matvar->data);
    } else if ( (matvar->data != NULL) && (!matvar->mem_conserve) &&
                (matvar->class_type == MAT_C_SPARSE) ) {
        sparse_t *sparse;
        sparse = matvar->data;
        if ( sparse->ir != NULL )
            free(sparse->ir);
        if ( sparse->jc != NULL )
            free(sparse->jc);
        if ( sparse->data != NULL )
            free(sparse->data);
        free(sparse);
    } else {
        if ( matvar->data && !matvar->mem_conserve )
            free(matvar->data);
    }
#ifdef HAVE_ZLIB
    if ( matvar->compression == COMPRESSION_ZLIB )
        inflateEnd(matvar->z);
#endif
    /* FIXME: Why does this cause a SEGV? */
#if 0
    memset(matvar,0,sizeof(matvar_t));
#endif
}

/** @brief Calculate a single subscript from a dimension subscripts
 *
 * Calculates a single linear subscript (0-relative) given a 1-relative
 * subscript for each dimension.  The calculation uses the formula below where
 * index is the linear index, s is an array of length RANK where each element
 * is the subscript for the correspondind dimension, D is an array whose
 * elements are the dimensions of the variable.
 * \f[
 *   index = \sum\limits_{k=0}^{RANK-1} [(s_k - 1) \prod\limits_{l=0}^{k} D_l ]
 * \f]
 * @ingroup MAT
 * @param rank Rank of the variable
 * @param dims dimensions of the variable
 * @param subs Dimension subscripts
 * @return Single (linear) subscript
 */
int
Mat_CalcSingleSubscript(int rank,int *dims,int *subs)
{
    int index = 0, i, j, k, err = 0;

    for ( i = 0; i < rank; i++ ) {
        k = subs[i];
        if ( k > dims[i] ) {
            err = 1;
            Mat_Critical("Mat_CalcSingleSubscript: index out of bounds");
            break;
        } else if ( k < 1 ) {
            err = 1;
            break;
        }
        k--;
        for ( j = i; j--; )
            k *= dims[j];
        index += k;
    }
    if ( err )
        index = -1;

    return index;
}


/** @brief Calculate a dimension subscripts from a single(linear) subscript
 *
 * Calculates 1-relative subscripts for each dimension given a 0-relative
 * linear index.  Subscripts are calculated as follows where s is the array
 * of dimension subscripts, D is the array of dimensions, and index is the
 * linear index.
 * \f[
 *   s_k = \lfloor\frac{1}{L} \prod\limits_{l = 0}^{k} D_l\rfloor + 1
 * \f]
 * \f[
 *   L = index - \sum\limits_{l = k}^{RANK - 1} s_k \prod\limits_{m = 0}^{k} D_m
 * \f]
 * @ingroup MAT
 * @param rank Rank of the variable
 * @param dims dimensions of the variable
 * @param index linear index
 * @return Array of dimension subscripts
 */
int *
Mat_CalcSubscripts(int rank,int *dims,int index)
{
    int i, j, k, err = 0, *subs;
    double l;

    subs = malloc(rank*sizeof(int));
    l = index;
    for ( i = rank; i--; ) {
        k = 1;
        for ( j = i; j--; )
            k *= dims[j];
        subs[i] = floor(l / (double)k);
        l -= subs[i]*k;
        subs[i]++;
    }

    return subs;
}

/** @brief Returns a pointer to the Cell array at a specific index
 *
 * Returns a pointer to the Cell Array Field at the given 1-relative index
 * @ingroup MAT
 * @param matvar Pointer to the Cell Array MAT variable
 * @param ... index for each dimension
 * @return Pointer to the Cell Array Field on success, NULL on error
 */
matvar_t *
Mat_VarGetCell(matvar_t *matvar,int index)
{
    int       err = 0,nmemb = 1, i;
    matvar_t *cell = NULL;

    for ( i = 0; i < matvar->rank; i++ )
        nmemb *= matvar->dims[i];

    if ( index < nmemb )
        cell = *((matvar_t **)matvar->data + index);

    return cell;
}

/** @brief Finds cells of a cell array
 *
 * Finds cells of a cell array given a start, stride, and edge.  The cells
 * are placed in a pointer array.
 * @ingroup MAT
 * @param matvar Cell Array matlab variable
 * @param start vector of length rank with 0-relative starting coordinates for
 *              each diemnsion.
 * @param stride vector of length rank with strides for each diemnsion.
 * @param edge vector of length rank with the number of elements to read in
 *              each diemnsion.
 * @returns an array of pointers to the cells
 */
matvar_t **
Mat_VarGetCells(matvar_t *matvar,int *start,
    int *stride,int *edge)
{
    int i, j, N, I = 0;
    int inc[10] = {0,}, cnt[10] = {0,}, dimp[10] = {0,};
    matvar_t **cells;

    if ( (matvar == NULL) || (start == NULL) || (stride == NULL) ||  
         (edge == NULL) ) {
        return NULL;
    } else if ( matvar->rank > 10 ) {
        return NULL;
    }

    inc[0] = stride[0]-1;
    dimp[0] = matvar->dims[0];
    N = edge[0];
    I = start[0];
    for ( i = 1; i < matvar->rank; i++ ) {
        inc[i]  = stride[i]-1;
        dimp[i] = matvar->dims[i-1];
        for ( j = i ; j--; ) {
            inc[i]  *= matvar->dims[j];
            dimp[i] *= matvar->dims[j+1];
        }
        N *= edge[i];
        if ( start[i] > 0 )
            I += start[i]*dimp[i-1];
    }
    cells = malloc(N*sizeof(matvar_t *));
    for ( i = 0; i < N; i+=edge[0] ) {
        for ( j = 0; j < edge[0]; j++ ) {
            cells[i+j] = *((matvar_t **)matvar->data + I);
            I += stride[0];
        }
        for ( j = 1; j < matvar->rank-1; j++ ) {
            cnt[j]++;
            if ( (cnt[j] % edge[j]) == 0 ) {
                cnt[j] = 0;
                if ( (I % dimp[j]) != 0 ) {
                    I += dimp[j]-(I % dimp[j]);
                }
            } else {
                I += matvar->dims[0]-edge[0]*stride[0]-start[0];
                I += inc[j];
                break;
            }
        }
    }
    return cells;
}

/** @brief Finds cells of a cell array
 *
 * Finds cells of a cell array given a linear indexed start, stride, and edge.
 * The cells are placed in a pointer array.  The cells themself should not
 * be freed as they are part of the original cell array, but the pointer array
 * should be.
 * @ingroup MAT
 * @param matvar Cell Array matlab variable
 * @param start starting index
 * @param stride stride
 * @param edge Number of cells to get
 * @returns an array of pointers to the cells
 */
matvar_t **
Mat_VarGetCellsLinear(matvar_t *matvar,int start,int stride,int edge)
{
    int i, I = 0;
    matvar_t **cells;

    if ( matvar == NULL || matvar->rank > 10 ) {
        cells = NULL;
    } else {
        cells = malloc(edge*sizeof(matvar_t *));
        for ( i = 0; i < edge; i++ ) {
            cells[i] = *((matvar_t **)matvar->data + I);
            I += stride;
        }
    }
    return cells;
}

/** @brief Calculates the size of a matlab variable in bytes
 *
 * @ingroup MAT
 * @param matvar matlab variable
 * @returns size of the variable in bytes
 */
size_t
Mat_VarGetSize(matvar_t *matvar)
{
    int nmemb, i;
    size_t bytes = 0;

    if ( matvar->class_type == MAT_C_STRUCT ) {
        int nfields;
        matvar_t **fields;
        /* This is really nmemb*nfields, but we'll get a 
         * more accurate count of the bytes by loopoing over all of them
         */
        nfields = matvar->nbytes / matvar->data_size;
        fields  = matvar->data;
        for ( i = 0; i < nfields; i++ )
            bytes += Mat_VarGetSize(fields[i]);
    } else if ( matvar->class_type == MAT_C_CELL ) {
        int ncells;
        matvar_t **cells;

        ncells = matvar->nbytes / matvar->data_size;
        cells  = matvar->data;
        for ( i = 0; i < ncells; i++ )
            bytes += Mat_VarGetSize(cells[i]);
    } else {
        nmemb = 1;
        for ( i = 0; i < matvar->rank; i++ )
            nmemb *= matvar->dims[i];
        bytes += nmemb*Mat_SizeOfClass(matvar->class_type);
    }
    return bytes;
}

/** @brief Returns a pointer to a structure field
 *
 * Returns a pointer to the structure field at the given 0-relative index
 * @ingroup MAT
 * @param matvar Pointer to the Structure MAT variable
 * @param name_or_index Name of the field, or the 1-relative index of the field.
 * If the index is used, it should be the address of an integer variable whose
 * value is the index number.
 * @param opt BY_NAME if the name_or_index is the name or BY_INDEX of the index
 * was passed.
 * @param ... index for each dimension
 * @return Pointer to the Structure Field on success, NULL on error
 */
matvar_t *
Mat_VarGetStructField(matvar_t *matvar,void *name_or_index,int opt,int index)
{
    int       i, err = 0, nfields, nmemb;
    matvar_t *field = NULL;

    nmemb = 1;
    for ( i = 0; i < matvar->rank; i++ )
        nmemb *= matvar->dims[i];

    nfields = matvar->nbytes / (nmemb*sizeof(matvar_t *));

    if ( index >= nmemb )
        err = 1;

    if ( !err && (opt == BY_INDEX) ) {
        int field_index;

        field_index = *(int *)name_or_index;

        if ( field_index > nfields )
            Mat_Critical("Mat_VarGetStructField: field index out of bounds");
        else
            field = *((matvar_t **)matvar->data+index*nfields+field_index - 1);
    } else if ( !err && (opt == BY_NAME) ) {
        char *field_name;

        field_name = (char *)name_or_index;

        for ( i = 0; i < nfields; i++ ) {
            field = *((matvar_t **)matvar->data+index*nfields+i);
            if ( !strcmp(field->name,field_name) )
                break;
            else
                field = NULL;
        }
    }

    return field;
}

/** @brief Prints the variable information
 *
 * Prints to stdout the values of the @ref matvar_t structure
 * @ingroup MAT
 * @param matvar Pointer to the matvar_t structure
 * @param printdata set to 1 if the Variables data should be printed, else 0
 */
void Mat_VarPrint( matvar_t *matvar, int printdata )
{
    int i, j;

    if ( matvar == NULL )
        return;
    if ( matvar->name )
        Mat_Message("      Name: %s", matvar->name);
    Mat_Message("      Rank: %d", matvar->rank);
    if ( matvar->rank == 0 )
        return;
    if ( matvar->isComplex )
        Mat_Message("Class Type: %s (complex)",class_type_desc[matvar->class_type]);
    else
        Mat_Message("Class Type: %s",class_type_desc[matvar->class_type]);
    if ( matvar->data_type )
        Mat_Message(" Data Type: %s", data_type_desc[matvar->data_type]);
    if ( matvar->data != NULL && matvar->data_size > 0 ) {
        switch( matvar->class_type ) {
            case MAT_C_DOUBLE:
                if ( !printdata )
                    break;
                if ( matvar->rank > 2 ) {
                    printf("I can't print more than 2 dimensions\n");
                } else if ( matvar->rank == 1 && matvar->dims[0] > 15 ) {
                    printf("I won't print more than 15 elements in a vector\n");
                } else if ( matvar->rank == 2 && 
                         (matvar->dims[0] > 15 || matvar->dims[1] > 15) ) {
                    for ( i = 0; i < matvar->dims[0] && i < 15; i++ ) {
                        for ( j = 0; j < matvar->dims[1] && j < 15; j++ )
                            printf("%f ", ((double*)matvar->data)[matvar->dims[0]*j+i]);
                        if ( j < matvar->dims[1] )
                            printf("...");
                        printf("\n");
                    }
                    if ( i < matvar->dims[0] )
                        printf(".\n.\n.\n");
                } else if ( matvar->rank == 2 ) { 
                    for ( i = 0; i < matvar->dims[0]; i++ ) {
                        for ( j = 0; j < matvar->dims[1]; j++ )
                            printf("%f ", ((double*)matvar->data)[matvar->dims[0]*j+i]);
                        printf("\n");
                    }
                } else {
                    for ( i = 0; i < matvar->nbytes/matvar->data_size; i++ )
                        printf("%f\n", ((double*)matvar->data)[i]);
                }
                break;
            case MAT_C_SINGLE:
                if ( !printdata )
                    break;
                if ( matvar->rank > 2 ) {
                    printf("I can't print more than 2 dimensions\n");
                } else if ( matvar->rank == 1 && matvar->dims[0] > 15 ) {
                    printf("I won't print more than 15 elements in a vector\n");
                } else if ( matvar->rank == 2 && 
                         (matvar->dims[0] > 15 || matvar->dims[1] > 15) ) {
                    for ( i = 0; i < matvar->dims[0] && i < 15; i++ ) {
                        for ( j = 0; j < matvar->dims[1] && j < 15; j++ )
                            printf("%f ", ((float*)matvar->data)[matvar->dims[0]*j+i]);
                        if ( j < matvar->dims[1] )
                            printf("...");
                        printf("\n");
                    }
                    if ( i < matvar->dims[0] )
                        printf(".\n.\n.\n");
                } else if ( matvar->rank == 2 ) { 
                    for ( i = 0; i < matvar->dims[0]; i++ ) {
                        for ( j = 0; j < matvar->dims[1]; j++ )
                            printf("%f ", ((float*)matvar->data)[matvar->dims[0]*j+i]);
                        printf("\n");
                    }
                } else {
                    for ( i = 0; i < matvar->nbytes/matvar->data_size; i++ )
                        printf("%f\n", ((float*)matvar->data)[i]);
                }
                break;
            case MAT_C_INT32:
                if ( !printdata )
                    break;
                if ( matvar->rank > 2 ) {
                    printf("I can't print more than 2 dimensions\n");
                } else if ( matvar->rank == 1 && matvar->dims[0] > 15 ) {
                    printf("I won't print more than 15 elements in a vector\n");
                } else if ( matvar->rank == 2 && 
                         (matvar->dims[0] > 15 || matvar->dims[1] > 15) ) {
                    for ( i = 0; i < matvar->dims[0] && i < 15; i++ ) {
                        for ( j = 0; j < matvar->dims[1] && j < 15; j++ )
                            printf("%d ", ((mat_int32_t*)matvar->data)[matvar->dims[0]*j+i]);
                        if ( j < matvar->dims[1] )
                            printf("...");
                        printf("\n");
                    }
                    if ( i < matvar->dims[0] )
                        printf(".\n.\n.\n");
                } else if ( matvar->rank == 2 ) { 
                    for ( i = 0; i < matvar->dims[0]; i++ ) {
                        for ( j = 0; j < matvar->dims[1]; j++ )
                            printf("%d ", ((mat_uint32_t*)matvar->data)[matvar->dims[0]*j+i]);
                        printf("\n");
                    }
                } else {
                    for ( i = 0; i < matvar->nbytes/matvar->data_size; i++ )
                        printf("%d\n", ((mat_int32_t*)matvar->data)[i]);
                }
                break;
            case MAT_C_UINT32:
                if ( !printdata )
                    break;
                if ( matvar->rank > 2 ) {
                    printf("I can't print more than 2 dimensions\n");
                } else if ( matvar->rank == 1 && matvar->dims[0] > 15 ) {
                    printf("I won't print more than 15 elements in a vector\n");
                } else if ( matvar->rank == 2 && 
                         (matvar->dims[0] > 15 || matvar->dims[1] > 15) ) {
                    for ( i = 0; i < matvar->dims[0] && i < 15; i++ ) {
                        for ( j = 0; j < matvar->dims[1] && j < 15; j++ )
                            printf("%u ", ((mat_uint32_t*)matvar->data)[matvar->dims[0]*j+i]);
                        if ( j < matvar->dims[1] )
                            printf("...");
                        printf("\n");
                    }
                    if ( i < matvar->dims[0] )
                        printf(".\n.\n.\n");
                } else if ( matvar->rank == 2 ) { 
                    for ( i = 0; i < matvar->dims[0]; i++ ) {
                        for ( j = 0; j < matvar->dims[1]; j++ )
                            printf("%u ", ((mat_uint32_t*)matvar->data)[matvar->dims[0]*j+i]);
                        printf("\n");
                    }
                } else {
                    for ( i = 0; i < matvar->nbytes/matvar->data_size; i++ )
                        printf("%u\n", ((mat_int32_t*)matvar->data)[i]);
                }
                break;
            case MAT_C_INT16:
                if ( !printdata )
                    break;
                if ( matvar->rank > 2 ) {
                    printf("I can't print more than 2 dimensions\n");
                } else if ( matvar->rank == 1 && matvar->dims[0] > 15 ) {
                    printf("I won't print more than 15 elements in a vector\n");
                } else if ( matvar->rank == 2 && 
                         (matvar->dims[0] > 15 || matvar->dims[1] > 15) ) {
                    for ( i = 0; i < matvar->dims[0] && i < 15; i++ ) {
                        for ( j = 0; j < matvar->dims[1] && j < 15; j++ )
                            printf("%hd ", ((mat_int16_t*)matvar->data)[matvar->dims[0]*j+i]);
                        if ( j < matvar->dims[1] )
                            printf("...");
                        printf("\n");
                    }
                    if ( i < matvar->dims[0] )
                        printf(".\n.\n.\n");
                } else if ( matvar->rank == 2 ) { 
                    for ( i = 0; i < matvar->dims[0]; i++ ) {
                        for ( j = 0; j < matvar->dims[1]; j++ )
                            printf("%hd ", ((mat_uint16_t*)matvar->data)[matvar->dims[0]*j+i]);
                        printf("\n");
                    }
                } else {
                    for ( i = 0; i < matvar->nbytes/matvar->data_size; i++ )
                        printf("%hd\n", ((mat_int16_t*)matvar->data)[i]);
                }
                break;
            case MAT_C_UINT16:
                if ( !printdata )
                    break;
                if ( matvar->rank > 2 ) {
                    printf("I can't print more than 2 dimensions\n");
                } else if ( matvar->rank == 1 && matvar->dims[0] > 15 ) {
                    printf("I won't print more than 15 elements in a vector\n");
                } else if ( matvar->rank == 2 && 
                         (matvar->dims[0] > 15 || matvar->dims[1] > 15) ) {
                    for ( i = 0; i < matvar->dims[0] && i < 15; i++ ) {
                        for ( j = 0; j < matvar->dims[1] && j < 15; j++ )
                            printf("%hu ", ((mat_uint16_t*)matvar->data)[matvar->dims[0]*j+i]);
                        if ( j < matvar->dims[1] )
                            printf("...");
                        printf("\n");
                    }
                    if ( i < matvar->dims[0] )
                        printf(".\n.\n.\n");
                } else if ( matvar->rank == 2 ) { 
                    for ( i = 0; i < matvar->dims[0]; i++ ) {
                        for ( j = 0; j < matvar->dims[1]; j++ )
                            printf("%hu ", ((mat_uint16_t*)matvar->data)[matvar->dims[0]*j+i]);
                        printf("\n");
                    }
                } else {
                    for ( i = 0; i < matvar->nbytes/matvar->data_size; i++ )
                        printf("%hu\n", ((mat_int32_t*)matvar->data)[i]);
                }
                break;
            case MAT_C_INT8:
                if ( !printdata )
                    break;
                if ( matvar->rank > 2 ) {
                    printf("I can't print more than 2 dimensions\n");
                } else if ( matvar->rank == 1 && matvar->dims[0] > 15 ) {
                    printf("I won't print more than 15 elements in a vector\n");
                } else if ( matvar->rank == 2 && 
                         (matvar->dims[0] > 15 || matvar->dims[1] > 15) ) {
                    for ( i = 0; i < matvar->dims[0] && i < 15; i++ ) {
                        for ( j = 0; j < matvar->dims[1] && j < 15; j++ )
                            printf("%hd ", ((mat_int8_t*)matvar->data)[matvar->dims[0]*j+i]);
                        if ( j < matvar->dims[1] )
                            printf("...");
                        printf("\n");
                    }
                    if ( i < matvar->dims[0] )
                        printf(".\n.\n.\n");
                } else if ( matvar->rank == 2 ) {
                    for ( i = 0; i < matvar->dims[0]; i++ ) {
                        for ( j = 0; j < matvar->dims[1]; j++ )
                            printf("%hd ", ((mat_int8_t*)matvar->data)[matvar->dims[0]*j+i]);
                        printf("\n");
                    }
                } else {
                    for ( i = 0; i < matvar->nbytes/matvar->data_size; i++ )
                        printf("%hd\n", ((mat_int8_t*)matvar->data)[i]);
                }
                break;
            case MAT_C_UINT8:
                if ( !printdata )
                    break;
                if ( matvar->rank > 2 ) {
                    printf("I can't print more than 2 dimensions\n");
                } else if ( matvar->rank == 1 && matvar->dims[0] > 15 ) {
                    printf("I won't print more than 15 elements in a vector\n");
                } else if ( matvar->rank == 2 && 
                         (matvar->dims[0] > 15 || matvar->dims[1] > 15) ) {
                    for ( i = 0; i < matvar->dims[0] && i < 15; i++ ) {
                        for ( j = 0; j < matvar->dims[1] && j < 15; j++ )
                            printf("%hu ", ((mat_uint8_t*)matvar->data)[matvar->dims[0]*j+i]);
                        if ( j < matvar->dims[1] )
                            printf("...");
                        printf("\n");
                    }
                    if ( i < matvar->dims[0] )
                        printf(".\n.\n.\n");
                } else if ( matvar->rank == 2 ) {
                    for ( i = 0; i < matvar->dims[0]; i++ ) {
                        for ( j = 0; j < matvar->dims[1]; j++ )
                            printf("%hu ", ((mat_uint8_t*)matvar->data)[matvar->dims[0]*j+i]);
                        printf("\n");
                    }
                } else {
                    for ( i = 0; i < matvar->nbytes/matvar->data_size; i++ )
                        printf("%hu\n", ((mat_uint8_t*)matvar->data)[i]);
                }
                break;
            case MAT_C_CHAR:
                if ( !printdata )
                    break;
                if ( matvar->dims[0] == 1 ) {
                    printf("%s\n",(char *)matvar->data);
                } else {
                    int ndx = 0;
                    for ( i = 0; i < matvar->dims[1]; i++ ) {
                        ndx = i;
                        j = 0;
                        while ( j++ < matvar->dims[0] && 
                                *((char *)matvar->data+ndx) != '\0' ) {
                            printf("%c", *((char *)matvar->data+ndx));
                            ndx += matvar->dims[0];
                        }
                        printf("\n");
                    }
                }
                break;
            case MAT_C_STRUCT:
            {
                matvar_t **fields = (matvar_t **)matvar->data;
                int nfields = matvar->nbytes / matvar->data_size;
                Mat_Message("Fields[%d] {", nfields);
                for ( i = 0; i < nfields; i++ )
                    Mat_VarPrint(fields[i],printdata);
                Mat_Message("}");
                break;
            }
            case MAT_C_CELL:
            {
                matvar_t **fields = (matvar_t **)matvar->data;
                int nfields = matvar->nbytes / matvar->data_size;
                for ( i = 0; i < nfields; i++ )
                    Mat_VarPrint(fields[i],printdata);
                break;
            }
            case MAT_C_SPARSE:
            {
                sparse_t *sparse;

                sparse = matvar->data;
                for ( i = 0; i < sparse->njc-1; i++ ) {
                    for ( j = sparse->jc[i];
                          j < sparse->jc[i+1] && j < sparse->ndata; j++ )
                        Mat_Message("    (%d,%d)  %f", sparse->ir[j]+1,i+1,
                            sparse->data[j]);
                }
                break;
            }
            default:
                printf("I can't print this class\n");
        }
    } else {
        if ( printdata && !matvar->data  )
            Mat_Warning("Data is NULL");
        if ( printdata && matvar->data_size < 1 )
            Mat_Warning("data-size is %d",matvar->data_size);
    }
    Mat_Message("\n");
    return;
}

/** @brief Reads MAT variable data from a file
 *
 * Reads data from a MAT variable.  The variable must have been read by
 * Mat_VarReadInfo.
 * @ingroup MAT
 * @param mat MAT file to read data from
 * @param matvar MAT variable information
 * @param data pointer to store data in (must be pre-allocated)
 * @param start array of starting indeces
 * @param stride stride of data
 * @param edge array specifying the number to read in each direction
 * @retval 0 on success
 */
int
Mat_VarReadData(mat_t *mat,matvar_t *matvar,void *data,
      int *start,int *stride,int *edge)
{
    int err = 0;
    mat_int32_t tag[2];

    fseek(mat->fp,matvar->datapos,SEEK_SET);
    if ( matvar->compression == COMPRESSION_NONE ) {
        fread(tag,4,2,mat->fp);
        if ( mat->byteswap ) {
            int32Swap(tag);
            int32Swap(tag+1);
        }
        matvar->data_type = tag[0] & 0x000000ff;
        if ( tag[0] & 0xffff0000 ) { /* Data is packed in the tag */
            fseek(mat->fp,-4,SEEK_CUR);
        }
#ifdef HAVE_ZLIB
    } else if ( matvar->compression == COMPRESSION_ZLIB ) {
        matvar->z->avail_in = 0;
        InflateDataType(mat,matvar,tag);
        if ( mat->byteswap ) {
            int32Swap(tag);
            int32Swap(tag+1);
        }
        matvar->data_type = tag[0] & 0x000000ff;
        if ( !(tag[0] & 0xffff0000) ) {/* Data is NOT packed in the tag */
            InflateSkip(mat,matvar->z,4);
        }
#endif
    }

    if ( matvar->rank == 2 ) {
        if ( stride[0]*(edge[0]-1)+start[0]+1 > matvar->dims[0] )
            err = 1;
        else if ( stride[1]*(edge[1]-1)+start[1]+1 > matvar->dims[1] )
            err = 1;
        else if ( matvar->compression == COMPRESSION_NONE )
            ReadDataSlab2(mat,data,matvar->class_type,matvar->data_type,
                matvar->dims,start,stride,edge);
#ifdef HAVE_ZLIB
        else if ( matvar->compression == COMPRESSION_ZLIB )
            ReadCompressedDataSlab2(mat,matvar->z,data,matvar->class_type,
                matvar->data_type,matvar->dims,start,stride,edge);
#endif
    } else {
        if ( matvar->compression == COMPRESSION_NONE )
            ReadDataSlabN(mat,data,matvar->class_type,matvar->data_type,
                matvar->rank,matvar->dims,start,stride,edge);
        else if ( matvar->compression == COMPRESSION_ZLIB )
#ifdef HAVE_ZLIB
            ReadCompressedDataSlabN(mat,matvar->z,data,matvar->class_type,
                matvar->data_type,matvar->rank,matvar->dims,start,stride,edge);
#else
										Mat_Critical("Not compiled with zlib support");
#endif
    }
    if ( err )
        return err;

    switch(matvar->class_type) {
        case MAT_C_DOUBLE:
            matvar->data_type = MAT_T_DOUBLE;
            matvar->data_size = sizeof(double);
            break;
        case MAT_C_SINGLE:
            matvar->data_type = MAT_T_SINGLE;
            matvar->data_size = sizeof(float);
            break;
        case MAT_C_INT32:
            matvar->data_type = MAT_T_INT32;
            matvar->data_size = sizeof(mat_int32_t);
            break;
        case MAT_C_UINT32:
            matvar->data_type = MAT_T_UINT32;
            matvar->data_size = sizeof(mat_uint32_t);
            break;
        case MAT_C_INT16:
            matvar->data_type = MAT_T_INT16;
            matvar->data_size = sizeof(mat_int16_t);
            break;
        case MAT_C_UINT16:
            matvar->data_type = MAT_T_UINT16;
            matvar->data_size = sizeof(mat_uint16_t);
            break;
        case MAT_C_INT8:
            matvar->data_type = MAT_T_INT8;
            matvar->data_size = sizeof(mat_int8_t);
            break;
        case MAT_C_UINT8:
            matvar->data_type = MAT_T_UINT8;
            matvar->data_size = sizeof(mat_uint8_t);
            break;
    }

    return err;
}

/** @brief Reads all the data for a matlab variable
 *
 * Allocates memory for an reads the data for a given matlab variable.
 * @ingroup MAT
 * @param mat Matlab MAT file structure pointer
 * @param matvar Variable whose data is to be read
 * @returns non-zero on error
 */
int
Mat_VarReadDataAll(mat_t *mat,matvar_t *matvar)
{
    int err = 0, nbytes = 1;

    if ( (mat == NULL) || (matvar == NULL) )
        err = 1;
    else
        ReadData(mat,matvar);

    return err;
}

/** @brief Reads MAT variable data from a file
 *
 * Reads data from a MAT variable using a linear indexingmode. The variable
 * must have been read by Mat_VarReadInfo.
 * @ingroup MAT
 * @param mat MAT file to read data from
 * @param matvar MAT variable information
 * @param data pointer to store data in (must be pre-allocated)
 * @param start starting index
 * @param stride stride of data
 * @param edge number of elements to read
 * @retval 0 on success
 */
int
Mat_VarReadDataLinear(mat_t *mat,matvar_t *matvar,void *data,int start,
    int stride,int edge)
{
    int err = 0, nmemb = 1, i, data_type;
    mat_int32_t tag[2];

    fseek(mat->fp,matvar->datapos,SEEK_SET);
    if ( matvar->compression == COMPRESSION_NONE ) {
        fread(tag,4,2,mat->fp);
        if ( mat->byteswap ) {
            int32Swap(tag);
            int32Swap(tag+1);
        }
        data_type = tag[0] & 0x000000ff;
        if ( tag[0] & 0xffff0000 ) { /* Data is packed in the tag */
            fseek(mat->fp,-4,SEEK_CUR);
        }
#ifdef HAVE_ZLIB
    } else if ( matvar->compression == COMPRESSION_ZLIB ) {
        matvar->z->avail_in = 0;
        InflateDataType(mat,matvar,tag);
        if ( mat->byteswap ) {
            int32Swap(tag);
            int32Swap(tag+1);
        }
        data_type = tag[0] & 0x000000ff;
        if ( !(tag[0] & 0xffff0000) ) {/* Data is NOT packed in the tag */
            InflateSkip(mat,matvar->z,4);
        }
#endif
    }

    for ( i = 0; i < matvar->rank; i++ )
        nmemb *= matvar->dims[i];

    if ( stride*(edge-1)+start+1 > nmemb ) {
        err = 1;
    } else {
        stride--;
        switch(matvar->class_type) {
            case MAT_C_DOUBLE:
                matvar->data_type = MAT_T_DOUBLE;
                matvar->data_size = sizeof(double);
                if ( matvar->compression == COMPRESSION_NONE ) {
                    stride *= Mat_SizeOf(data_type);
                    fseek(mat->fp,start,SEEK_CUR);
                    for ( i = 0; i < edge; i++ ) {
                        ReadDoubleData(mat,(double*)data+i,data_type,1);
                        fseek(mat->fp,stride,SEEK_CUR);
                    }
#ifdef HAVE_ZLIB
                } else {
                    z_stream z_copy;

                    err = inflateCopy(&z_copy,matvar->z);
                    InflateSkipData(mat,&z_copy,data_type,start);
                    for ( i = 0; i < edge; i++ ) {
                        ReadCompressedDoubleData(mat,&z_copy,(double*)data+i,
                            data_type,1);
                        InflateSkipData(mat,&z_copy,data_type,stride);
                    }
                    inflateEnd(&z_copy);
#endif
                }
                break;
            case MAT_C_SINGLE:
                matvar->data_type = MAT_T_SINGLE;
                matvar->data_size = sizeof(float);
                if ( matvar->compression == COMPRESSION_NONE ) {
                    stride *= Mat_SizeOf(data_type);
                    for ( i = 0; i < edge; i++ ) {
                        ReadSingleData(mat,(float*)data+i,data_type,1);
                        fseek(mat->fp,stride,SEEK_CUR);
                    }
#ifdef HAVE_ZLIB
                } else {
                    z_stream z_copy;

                    err = inflateCopy(&z_copy,matvar->z);
                    InflateSkipData(mat,&z_copy,data_type,start);
                    for ( i = 0; i < edge; i++ ) {
                        ReadCompressedSingleData(mat,&z_copy,(float*)data+i,
                            data_type,1);
                        InflateSkipData(mat,&z_copy,data_type,stride);
                    }
                    inflateEnd(&z_copy);
#endif
                }
                break;
            case MAT_C_INT32:
                matvar->data_type = MAT_T_INT32;
                matvar->data_size = sizeof(mat_int32_t);
                if ( matvar->compression == COMPRESSION_NONE ) {
                    stride *= Mat_SizeOf(data_type);
                    for ( i = 0; i < edge; i++ ) {
                        ReadInt32Data(mat,(mat_int32_t*)data+i,data_type,1);
                        fseek(mat->fp,stride,SEEK_CUR);
                    }
#ifdef HAVE_ZLIB
                } else {
                    z_stream z_copy;

                    err = inflateCopy(&z_copy,matvar->z);
                    InflateSkipData(mat,&z_copy,data_type,start);
                    for ( i = 0; i < edge; i++ ) {
                        ReadCompressedInt32Data(mat,&z_copy,(mat_int32_t*)data+i,
                            data_type,1);
                        InflateSkipData(mat,&z_copy,data_type,stride);
                    }
                    inflateEnd(&z_copy);
#endif
                }
                break;
            case MAT_C_UINT32:
                matvar->data_type = MAT_T_UINT32;
                matvar->data_size = sizeof(mat_uint32_t);
                if ( matvar->compression == COMPRESSION_NONE ) {
                    stride *= Mat_SizeOf(data_type);
                    for ( i = 0; i < edge; i++ ) {
                        ReadInt32Data(mat,(mat_int32_t*)data+i,data_type,1);
                        fseek(mat->fp,stride,SEEK_CUR);
                    }
#ifdef HAVE_ZLIB
                } else {
                    z_stream z_copy;

                    err = inflateCopy(&z_copy,matvar->z);
                    InflateSkipData(mat,&z_copy,data_type,start);
                    for ( i = 0; i < edge; i++ ) {
                        ReadCompressedInt32Data(mat,&z_copy,(mat_int32_t*)data+i,
                            data_type,1);
                        InflateSkipData(mat,&z_copy,data_type,stride);
                    }
                    inflateEnd(&z_copy);
#endif
                }
                break;
            case MAT_C_INT16:
                matvar->data_type = MAT_T_INT16;
                matvar->data_size = sizeof(mat_int16_t);
                if ( matvar->compression == COMPRESSION_NONE ) {
                    stride *= Mat_SizeOf(data_type);
                    for ( i = 0; i < edge; i++ ) {
                        ReadInt16Data(mat,(mat_int16_t*)data+i,data_type,1);
                        fseek(mat->fp,stride,SEEK_CUR);
                    }
#ifdef HAVE_ZLIB
                } else {
                    z_stream z_copy;

                    err = inflateCopy(&z_copy,matvar->z);
                    InflateSkipData(mat,&z_copy,data_type,start);
                    for ( i = 0; i < edge; i++ ) {
                        ReadCompressedInt16Data(mat,&z_copy,(mat_int16_t*)data+i,
                            data_type,1);
                        InflateSkipData(mat,&z_copy,data_type,stride);
                    }
                    inflateEnd(&z_copy);
#endif
                }
                break;
            case MAT_C_UINT16:
                matvar->data_type = MAT_T_UINT16;
                matvar->data_size = sizeof(mat_uint16_t);
                if ( matvar->compression == COMPRESSION_NONE ) {
                    stride *= Mat_SizeOf(data_type);
                    for ( i = 0; i < edge; i++ ) {
                        ReadInt16Data(mat,(mat_int16_t*)data+i,data_type,1);
                        fseek(mat->fp,stride,SEEK_CUR);
                    }
#ifdef HAVE_ZLIB
                } else {
                    z_stream z_copy;

                    err = inflateCopy(&z_copy,matvar->z);
                    InflateSkipData(mat,&z_copy,data_type,start);
                    for ( i = 0; i < edge; i++ ) {
                        ReadCompressedInt16Data(mat,&z_copy,(mat_int16_t*)data+i,
                            data_type,1);
                        InflateSkipData(mat,&z_copy,data_type,stride);
                    }
                    inflateEnd(&z_copy);
#endif
                }
                break;
            case MAT_C_INT8:
                matvar->data_type = MAT_T_INT8;
                matvar->data_size = sizeof(mat_int8_t);
                if ( matvar->compression == COMPRESSION_NONE ) {
                    stride *= Mat_SizeOf(data_type);
                    for ( i = 0; i < edge; i++ ) {
                        ReadInt8Data(mat,(mat_int8_t*)data+i,data_type,1);
                        fseek(mat->fp,stride,SEEK_CUR);
                    }
#ifdef HAVE_ZLIB
                } else {
                    z_stream z_copy;

                    err = inflateCopy(&z_copy,matvar->z);
                    InflateSkipData(mat,&z_copy,data_type,start);
                    for ( i = 0; i < edge; i++ ) {
                        ReadCompressedInt8Data(mat,&z_copy,(mat_int8_t*)data+i,
                            data_type,1);
                        InflateSkipData(mat,&z_copy,data_type,stride);
                    }
                    inflateEnd(&z_copy);
#endif
                }
                break;
            case MAT_C_UINT8:
                matvar->data_type = MAT_T_UINT8;
                matvar->data_size = sizeof(mat_uint8_t);
                if ( matvar->compression == COMPRESSION_NONE ) {
                    stride *= Mat_SizeOf(data_type);
                    for ( i = 0; i < edge; i++ ) {
                        ReadInt8Data(mat,(mat_int8_t*)data+i,data_type,1);
                        fseek(mat->fp,stride,SEEK_CUR);
                    }
#ifdef HAVE_ZLIB
                } else {
                    z_stream z_copy;

                    err = inflateCopy(&z_copy,matvar->z);
                    InflateSkipData(mat,&z_copy,data_type,start);
                    for ( i = 0; i < edge; i++ ) {
                        ReadCompressedInt8Data(mat,&z_copy,(mat_int8_t*)data+i,
                            data_type,1);
                        InflateSkipData(mat,&z_copy,data_type,stride);
                    }
                    inflateEnd(&z_copy);
#endif
                }
                break;
        }
    }

    return err;
}

/** @brief Reads the information of the next variable in a MAT file
 *
 * Reads the next variable's information (class,flags-complex/global/logical,
 * rank,dimensions, name, etc) from the Matlab MAT file.  After reading, the MAT
 * file is positioned past the current variable.
 * @ingroup MAT
 * @param mat Pointer to the MAT file
 * @param name Name of the variable to read
 * @return Pointer to the @ref matvar_t structure containing the MAT
 * variable information
 */
matvar_t *
Mat_VarReadNextInfo( mat_t *mat )
{
    int err, data_type, nBytes, i;
    long  fpos;
    matvar_t *matvar = NULL;
    mat_uint32_t array_flags; 
    long     bytesread = 0;

    if( mat == NULL )
        return NULL;

    fpos = ftell(mat->fp);
    err = fread(&data_type,4,1,mat->fp);
    if ( !err )
        return NULL;
    err = fread(&nBytes,4,1,mat->fp);
    if ( mat->byteswap ) {
        int32Swap(&data_type);
        int32Swap(&nBytes);
    }
    switch ( data_type ) {
#ifdef HAVE_ZLIB
        case MAT_T_COMPRESSED:
        {   
            mat_uint32_t uncomp_buf[16] = {0,};
            int      nbytes;

            matvar = (matvar_t *)malloc(sizeof(matvar_t));
            matvar->name = NULL;
            matvar->data = NULL;
            matvar->dims = NULL;
            matvar->nbytes = 0;
            matvar->data_type = 0;
            matvar->class_type = 0;
            matvar->data_size = 0;
            matvar->mem_conserve = 0;
            matvar->compression = 1;
            matvar->fpos = fpos;

            matvar->z = calloc(1,sizeof(z_stream));
            matvar->z->zalloc = NULL;
            matvar->z->zfree  = NULL;
            matvar->z->opaque = NULL;
            matvar->z->next_in   = NULL;
            matvar->z->next_out  = NULL;
            matvar->z->avail_in  = 0;
            matvar->z->avail_out = 0;
            err = inflateInit(matvar->z);
            if ( err != Z_OK ) {
                Mat_Critical("inflateInit2 returned %d",err);
                Mat_VarFree(matvar);
                break;
            }

            /* Read Variable tag */
            bytesread += InflateVarTag(mat,matvar,uncomp_buf);
            if ( mat->byteswap ) {
                int32Swap(uncomp_buf);
                int32Swap(uncomp_buf+1);
            }
            nbytes = uncomp_buf[1];
            if ( uncomp_buf[0] != MAT_T_MATRIX ) {
                Mat_Critical("Uncompressed type not MAT_T_MATRIX");
                fseek(mat->fp,nBytes-bytesread,SEEK_CUR);
                Mat_VarFree(matvar);
                matvar = NULL;
                break;
            }
            /* Inflate Array Flags */
            bytesread += InflateArrayFlags(mat,matvar,uncomp_buf);
            if ( mat->byteswap ) {
                int32Swap(uncomp_buf);
                int32Swap(uncomp_buf+2);
                int32Swap(uncomp_buf+3);
            }
            /* Array Flags */
            if ( uncomp_buf[0] == MAT_T_UINT32 ) {
               array_flags = uncomp_buf[2];
               matvar->class_type  = (array_flags & MAT_F_CLASS_T);
               matvar->isComplex   = (array_flags & MAT_F_COMPLEX);
               matvar->isGlobal    = (array_flags & MAT_F_GLOBAL);
               matvar->isLogical   = (array_flags & MAT_F_LOGICAL);
               if ( matvar->class_type == MAT_C_SPARSE ) {
                   /* Need to find a more appropriate place to store nzmax */
                   matvar->nbytes      = uncomp_buf[3];
               }
            }
            /* Inflate Dimensions */
            bytesread += InflateDimensions(mat,matvar,uncomp_buf);
            if ( mat->byteswap ) {
                int32Swap(uncomp_buf);
                int32Swap(uncomp_buf+1);
            }
            /* Rank and Dimension */
            if ( uncomp_buf[0] == MAT_T_INT32 ) {
                nbytes = uncomp_buf[1];
                matvar->rank = nbytes / 4;
                matvar->dims = (int *)malloc(matvar->rank*sizeof(int));
                if ( mat->byteswap ) {
                    for ( i = 0; i < matvar->rank; i++ )
                        matvar->dims[i] = int32Swap(&(uncomp_buf[2+i]));
                } else {
                    for ( i = 0; i < matvar->rank; i++ )
                        matvar->dims[i] = uncomp_buf[2+i];
                }
            }
            /* Inflate variable name tag */
            bytesread += InflateVarNameTag(mat,matvar,uncomp_buf);
            if ( mat->byteswap )
                int32Swap(uncomp_buf);
            /* Name of variable */
            if ( uncomp_buf[0] == MAT_T_INT8 ) {    /* Name not in tag */
                int len;
                if ( mat->byteswap )
                    len = int32Swap(uncomp_buf+1);
                else
                    len = uncomp_buf[1];

                if ( len % 8 == 0 )
                    i = len;
                else
                    i = len+(8-(len % 8));
                matvar->name = (char *)malloc(i+1);
                /* Inflate variable name */
                bytesread += InflateVarName(mat,matvar,matvar->name,i);
                matvar->name[len] = '\0';
            } else if ( ((uncomp_buf[0] & 0x0000ffff) == MAT_T_INT8) && 
                        ((uncomp_buf[0] & 0xffff0000) != 0x00) ) {
                /* Name packed in tag */
                int len;
                len = (uncomp_buf[0] & 0xffff0000) >> 16;
                matvar->name = (char *)malloc(len+1);
                memcpy(matvar->name,uncomp_buf+1,len);
                matvar->name[len] = '\0';
            }
            if ( matvar->class_type == MAT_C_STRUCT )
                ReadNextStructField(mat,matvar);
            else if ( matvar->class_type == MAT_C_CELL )
                ReadNextCell(mat,matvar);
            fseek(mat->fp,-(int)matvar->z->avail_in,SEEK_CUR);
            matvar->datapos = ftell(mat->fp);
            fseek(mat->fp,nBytes+8+fpos,SEEK_SET);
            break;
        }
#endif
        case MAT_T_MATRIX:
        {
            int      nbytes;
            mat_uint32_t buf[32];
            size_t   bytesread = 0;

            matvar = (matvar_t *)malloc(sizeof(matvar_t));
            matvar->name = NULL;
            matvar->data = NULL;
            matvar->dims = NULL;
#ifdef HAVE_ZLIB
            matvar->z    = NULL;
#endif
            matvar->nbytes = 0;
            matvar->data_type = 0;
            matvar->mem_conserve = 0;
            matvar->compression = 0;
            matvar->data_size = 0;

            matvar->fpos = fpos;
            /* Read Array Flags and The Dimensions Tag */
            bytesread  += fread(buf,4,6,mat->fp);
            if ( mat->byteswap ) {
                int32Swap(buf);
                int32Swap(buf+1);
                int32Swap(buf+2);
                int32Swap(buf+3);
                int32Swap(buf+4);
                int32Swap(buf+5);
            }
            /* Array Flags */
            if ( buf[0] == MAT_T_UINT32 ) {
               array_flags = buf[2];
               matvar->class_type  = (array_flags & MAT_F_CLASS_T);
               matvar->isComplex   = (array_flags & MAT_F_COMPLEX);
               matvar->isGlobal    = (array_flags & MAT_F_GLOBAL);
               matvar->isLogical   = (array_flags & MAT_F_LOGICAL);
               if ( matvar->class_type == MAT_C_SPARSE ) {
                   /* Need to find a more appropriate place to store nzmax */
                   matvar->nbytes      = buf[3];
               }
            }
            /* Rank and Dimension */
            if ( buf[4] == MAT_T_INT32 ) {
                nbytes = buf[5];

                matvar->rank = nbytes / 4;
                matvar->dims = (int *)malloc(matvar->rank*sizeof(int));

                /* Assumes rank <= 16 */
                if ( matvar->rank % 2 != 0 )
                    bytesread+=fread(buf,4,matvar->rank+1,mat->fp);
                else
                    bytesread+=fread(buf,4,matvar->rank,mat->fp);

                if ( mat->byteswap ) {
                    for ( i = 0; i < matvar->rank; i++ )
                        matvar->dims[i] = int32Swap(buf+i);
                } else {
                    for ( i = 0; i < matvar->rank; i++ )
                        matvar->dims[i] = buf[i];
                }
            }
            /* Variable Name Tag */
            bytesread+=fread(buf,4,2,mat->fp);
            if ( mat->byteswap )
                int32Swap(buf);
            /* Name of variable */
            if ( buf[0] == MAT_T_INT8 ) {    /* Name not in tag */
                int len;

                if ( mat->byteswap )
                    len = int32Swap(buf+1);
                else
                    len = buf[1];
                if ( len % 8 == 0 )
                    i = len;
                else
                    i = len+(8-(len % 8));
                bytesread+=fread(buf,1,i,mat->fp);

                matvar->name = (char *)malloc(len+1);
                memcpy(matvar->name,buf,len);
                matvar->name[len] = '\0';
            } else if ( ((buf[0] & 0x0000ffff) == MAT_T_INT8) &&
                        ((buf[0] & 0xffff0000) != 0x00) ) {
                /* Name packed in the tag */
                int len;

                len = (buf[0] & 0xffff0000) >> 16;
                matvar->name = (char *)malloc(len+1);
                memcpy(matvar->name,buf+1,len);
                matvar->name[len] = '\0';
            }
            if ( matvar->class_type == MAT_C_STRUCT )
                (void)ReadNextStructField(mat,matvar);
            else if ( matvar->class_type == MAT_C_CELL )
                (void)ReadNextCell(mat,matvar);
            else if ( matvar->class_type == MAT_C_FUNCTION )
                (void)ReadNextFunctionHandle(mat,matvar);
            matvar->datapos = ftell(mat->fp);
            fseek(mat->fp,nBytes+8+fpos,SEEK_SET);
            break;
        }
        default:
            Mat_Message("%d is not valid (MAT_T_MATRIX or MAT_T_COMPRESSED", data_type);
            return NULL;
    }

    return matvar;
}


/** @brief Reads the information of a variable with the given name from a MAT file
 *
 * Reads the named variable (or the next variable if name is NULL) information
 * (class,flags-complex/global/logical,rank,dimensions,and name) from the
 * Matlab MAT file
 * @ingroup MAT
 * @param mat Pointer to the MAT file
 * @param name Name of the variable to read
 * @return Pointer to the @ref matvar_t structure containing the MAT
 * variable information
 */
matvar_t *
Mat_VarReadInfo( mat_t *mat, char *name )
{

    int err, data_type, nBytes;
    long  fpos, curpos;
    matvar_t *matvar = NULL;

    if ( (mat == NULL) || (name == NULL) )
        return NULL;

    fpos = ftell(mat->fp);
    fseek(mat->fp,128,SEEK_SET);
    do {
        err = fread(&data_type,4,1,mat->fp);
        if ( !err )
            return NULL;
        err = fread(&nBytes,4,1,mat->fp);
        if ( mat->byteswap ) {
            int32Swap(&data_type);
            int32Swap(&nBytes);
        }
        curpos = ftell(mat->fp);
        fseek(mat->fp,-8,SEEK_CUR);
        matvar = Mat_VarReadNextInfo(mat);
        if ( matvar ) {
            if ( !matvar->name ) {
                Mat_VarFree(matvar);
                matvar = NULL;
            } else if ( strcmp(matvar->name,name) ) {
                Mat_VarFree(matvar);
                matvar = NULL;
            }
        }
        fseek(mat->fp,curpos+nBytes,SEEK_SET);
    } while ( !matvar && !feof(mat->fp) );

    fseek(mat->fp,fpos,SEEK_SET);
    return matvar;
}

/** @brief Reads the variable with the given name from a MAT file
 *
 * Reads the next variable in the Matlab MAT file
 * @ingroup MAT
 * @param mat Pointer to the MAT file
 * @param name Name of the variable to read
 * @return Pointer to the @ref matvar_t structure containing the MAT
 * variable information
 */
matvar_t *
Mat_VarRead( mat_t *mat, char *name )
{
    long  fpos;
    matvar_t *matvar = NULL;;

    if ( (mat == NULL) || (name == NULL) )
        return NULL;

    fpos = ftell(mat->fp);
    matvar = Mat_VarReadInfo(mat,name);
    if ( matvar )
        ReadData(mat,matvar);
    fseek(mat->fp,fpos,SEEK_SET);
    return matvar;
}

/** @brief Reads the next variable in a MAT file
 *
 * Reads the next variable in the Matlab MAT file
 * @ingroup MAT
 * @param mat Pointer to the MAT file
 * @return Pointer to the @ref matvar_t structure containing the MAT
 * variable information
 */
matvar_t *
Mat_VarReadNext( mat_t *mat )
{
    int err, data_type, nBytes;
    long fpos;
    matvar_t *matvar = NULL;

    if ( feof(mat->fp) )
        return NULL;
    err = fread(&data_type,4,1,mat->fp);
    if ( err != 1 )
        return NULL;
    err = fread(&nBytes,4,1,mat->fp);
    if ( err != 1 )
        return NULL;
    /* Read position so we can assure we advance past this variable whether
     * we read it or not and no matter what happens while reading it
     */
    if ( mat->byteswap) {
        int32Swap(&data_type);
        int32Swap(&nBytes);
    }
    fpos = ftell(mat->fp);
    fseek(mat->fp,-8,SEEK_CUR);
    matvar = Mat_VarReadNextInfo(mat);
    if ( matvar )
        ReadData(mat,matvar);
    fseek(mat->fp,fpos+nBytes,SEEK_SET);
    return matvar;
}

/** @brief Writes the given MAT variable to a MAT file
 *
 * Writes the MAT variable information stored in matvar to the given MAT file.
 * The variable will be written to the end of the file.
 * @ingroup MAT
 * @param mat MAT file to write to
 * @param matvar MAT variable information to write
 * @param compress Whether or not to compress the data (Currently not implemented)
 * @retval 0 on success
 */
int
Mat_VarWriteInfo(mat_t *mat, matvar_t *matvar )
{
    mat_uint32_t array_flags = 0x0; 
    mat_int16_t  array_name_type = MAT_T_INT8,fieldname_type = MAT_T_INT32,fieldname_data_size=4;
    mat_int8_t  pad1 = 0;
    int      array_flags_type = MAT_T_UINT32, dims_array_type = MAT_T_INT32;
    int      array_flags_size = 8, pad4 = 0, matrix_type = MAT_T_MATRIX;
    int      nBytes, i, nmemb = 1;
    long     start = 0, end = 0;

#if 0
    nBytes = GetMatrixMaxBufSize(matvar);
#endif

    fseek(mat->fp,0,SEEK_END);         /* Always write at end of file */
    fwrite(&matrix_type,4,1,mat->fp);
    fwrite(&pad4,4,1,mat->fp);
    start = ftell(mat->fp);

    /* Array Flags */

    array_flags = matvar->class_type & MAT_F_CLASS_T;
    if ( matvar->isComplex )
        array_flags |= MAT_F_COMPLEX;
    if ( matvar->isGlobal )
        array_flags |= MAT_F_GLOBAL;
    if ( matvar->isLogical )
        array_flags |= MAT_F_LOGICAL;

    fwrite(&array_flags_type,4,1,mat->fp);
    fwrite(&array_flags_size,4,1,mat->fp);
    fwrite(&array_flags,4,1,mat->fp);
    fwrite(&pad4,4,1,mat->fp);
    /* Rank and Dimension */
    nBytes = matvar->rank * 4;
    fwrite(&dims_array_type,4,1,mat->fp);
    fwrite(&nBytes,4,1,mat->fp);
    for ( i = 0; i < matvar->rank; i++ ) {
        mat_int32_t dim;
        dim = matvar->dims[i];
        nmemb *= dim;
        fwrite(&dim,4,1,mat->fp);
    }
    if ( matvar->rank % 2 != 0 )
        fwrite(&pad4,4,1,mat->fp);
    /* Name of variable */
    if ( strlen(matvar->name) <= 4 ) {
        mat_int16_t array_name_len = (mat_int16_t)strlen(matvar->name);
        mat_int8_t  pad1 = 0;
        fwrite(&array_name_type,2,1,mat->fp);
        fwrite(&array_name_len,2,1,mat->fp);
        fwrite(matvar->name,1,array_name_len,mat->fp);
        for ( i = array_name_len; i < 4; i++ )
            fwrite(&pad1,1,1,mat->fp);
    } else {
        mat_int32_t array_name_len = (mat_int32_t)strlen(matvar->name);
        mat_int8_t  pad1 = 0;

        fwrite(&array_name_type,2,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&array_name_len,4,1,mat->fp);
        fwrite(matvar->name,1,array_name_len,mat->fp);
        if ( array_name_len % 8 )
            for ( i = array_name_len % 8; i < 8; i++ )
                fwrite(&pad1,1,1,mat->fp);
    }

    matvar->datapos = ftell(mat->fp);
    switch ( matvar->class_type ) {
        case MAT_C_DOUBLE:
        case MAT_C_SINGLE:
        case MAT_C_INT32:
        case MAT_C_UINT32:
        case MAT_C_INT16:
        case MAT_C_UINT16:
        case MAT_C_INT8:
        case MAT_C_UINT8:
            nBytes = WriteEmptyData(mat,nmemb,matvar->data_type);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            if ( matvar->isComplex ) {
                nBytes = WriteEmptyData(mat,nmemb,matvar->data_type);
                if ( nBytes % 8 )
                    for ( i = nBytes % 8; i < 8; i++ )
                        fwrite(&pad1,1,1,mat->fp);
            }
            break;
        case MAT_C_CHAR:
        {
            WriteEmptyCharData(mat,nmemb,matvar->data_type);
            break;
        }
        case MAT_C_CELL:
        {
            int nfields = matvar->nbytes / matvar->data_size;
            matvar_t **fields = (matvar_t **)matvar->data;

            for ( i = 0; i < nfields; i++ )
                WriteCellArrayFieldInfo(mat,fields[i],0);
            break;
        }
#if 0
        case MAT_C_STRUCT:
        {
            char **fieldnames, *padzero;
            int maxlen = 0, fieldname_size;
            int nfields = matvar->nbytes / matvar->data_size;
            matvar_t **fields = (matvar_t **)matvar->data;

            fieldnames = (char **)malloc(nfields*sizeof(char *));
            for ( i = 0; i < nfields; i++ ) {
                fieldnames[i] = fields[i]->name;
                if ( strlen(fieldnames[i]) > maxlen )
                    maxlen = strlen(fieldnames[i]);
            }
            maxlen++;
            fieldname_size = maxlen;
            while ( nfields*fieldname_size % 8 != 0 )
                fieldname_size++;
            fwrite(&fieldname_type,2,1,mat->fp);
            fwrite(&fieldname_data_size,2,1,mat->fp);
            fwrite(&fieldname_size,4,1,mat->fp);
            fwrite(&array_name_type,2,1,mat->fp);
            fwrite(&pad1,1,1,mat->fp);
            fwrite(&pad1,1,1,mat->fp);
            nBytes = nfields*fieldname_size;
            fwrite(&nBytes,4,1,mat->fp);
            padzero = (char *)calloc(fieldname_size,1);
            for ( i = 0; i < nfields; i++ ) {
                fwrite(fieldnames[i],1,strlen(fieldnames[i]),mat->fp);
                fwrite(padzero,1,fieldname_size-strlen(fieldnames[i]),mat->fp);
            }
            free(fieldnames);
            free(padzero);
            for ( i = 0; i < nfields; i++ )
                WriteStructField(mat,fields[i],compress);
            break;
        }
#endif
    }
    end = ftell(mat->fp);
    nBytes = (int)(end-start);
    fseek(mat->fp,(long)-(nBytes+4),SEEK_CUR);
    fwrite(&nBytes,4,1,mat->fp);
    fseek(mat->fp,end,SEEK_SET);

    return 0;
}

/** @brief Writes the given data to the MAT variable
 *
 * Writes data to a MAT variable.  The variable must have previously been
 * written with Mat_VarWriteInfo.
 * @ingroup MAT
 * @param mat MAT file to write to
 * @param matvar MAT variable information to write
 * @param data pointer to the data to write
 * @param start array of starting indeces
 * @param stride stride of data
 * @param edge array specifying the number to read in each direction
 * @retval 0 on success
 */
int
Mat_VarWriteData(mat_t *mat,matvar_t *matvar,void *data,
      int *start,int *stride,int *edge)
{
    int err = 0;

    fseek(mat->fp,matvar->datapos+8,SEEK_SET);

    if ( matvar->rank == 2 ) {
        if ( stride[0]*(edge[0]-1)+start[0]+1 > matvar->dims[0] )
            err = 1;
        else if ( stride[1]*(edge[1]-1)+start[1]+1 > matvar->dims[1] )
            err = 1;
        else
            WriteDataSlab2(mat,data,matvar->data_type,matvar->dims,start,
                 stride,edge);
    }

    return err;
}

/** @brief Writes the given MAT variable to a MAT file
 *
 * Writes the MAT variable information stored in matvar to the given MAT file.
 * The variable will be written to the end of the file.
 * @ingroup MAT
 * @param mat MAT file to write to
 * @param matvar MAT variable information to write
 * @param compress Whether or not to compress the data (Currently not implemented)
 * @retval 0 on success
 */
int
Mat_VarWrite( mat_t *mat, matvar_t *matvar, int compress )
{
    mat_uint32_t array_flags = 0x0; 
    mat_int16_t  array_name_type = MAT_T_INT8,fieldname_type = MAT_T_INT32,fieldname_data_size=4;
    mat_int8_t  pad1 = 0;
    int      array_flags_type = MAT_T_UINT32, dims_array_type = MAT_T_INT32;
    int      array_flags_size = 8, pad4 = 0, matrix_type = MAT_T_MATRIX;
    int      nBytes, i, nmemb = 1, nzmax = 0;
    long     start = 0, end = 0;

#if 0
    nBytes = GetMatrixMaxBufSize(matvar);
#endif

    fseek(mat->fp,0,SEEK_END);         /* Always write at end of file */
    fwrite(&matrix_type,4,1,mat->fp);
    fwrite(&pad4,4,1,mat->fp);
    start = ftell(mat->fp);

    /* Array Flags */

    array_flags = matvar->class_type & MAT_F_CLASS_T;
    if ( matvar->isComplex )
        array_flags |= MAT_F_COMPLEX;
    if ( matvar->isGlobal )
        array_flags |= MAT_F_GLOBAL;
    if ( matvar->isLogical )
        array_flags |= MAT_F_LOGICAL;
    if ( matvar->class_type == MAT_C_SPARSE )
        nzmax = ((sparse_t *)matvar->data)->nzmax;

    fwrite(&array_flags_type,4,1,mat->fp);
    fwrite(&array_flags_size,4,1,mat->fp);
    fwrite(&array_flags,4,1,mat->fp);
    fwrite(&nzmax,4,1,mat->fp);
    /* Rank and Dimension */
    nBytes = matvar->rank * 4;
    fwrite(&dims_array_type,4,1,mat->fp);
    fwrite(&nBytes,4,1,mat->fp);
    for ( i = 0; i < matvar->rank; i++ ) {
        mat_int32_t dim;
        dim = matvar->dims[i];
        nmemb *= dim;
        fwrite(&dim,4,1,mat->fp);
    }
    if ( matvar->rank % 2 != 0 )
        fwrite(&pad4,4,1,mat->fp);
    /* Name of variable */
    if ( strlen(matvar->name) <= 4 ) {
        mat_int16_t array_name_len = (mat_int16_t)strlen(matvar->name);
        mat_int8_t  pad1 = 0;
        fwrite(&array_name_type,2,1,mat->fp);
        fwrite(&array_name_len,2,1,mat->fp);
        fwrite(matvar->name,1,array_name_len,mat->fp);
        for ( i = array_name_len; i < 4; i++ )
            fwrite(&pad1,1,1,mat->fp);
    } else {
        mat_int32_t array_name_len = (mat_int32_t)strlen(matvar->name);
        mat_int8_t  pad1 = 0;

        fwrite(&array_name_type,2,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&pad1,1,1,mat->fp);
        fwrite(&array_name_len,4,1,mat->fp);
        fwrite(matvar->name,1,array_name_len,mat->fp);
        if ( array_name_len % 8 )
            for ( i = array_name_len % 8; i < 8; i++ )
                fwrite(&pad1,1,1,mat->fp);
    }

    switch ( matvar->class_type ) {
        case MAT_C_DOUBLE:
        case MAT_C_SINGLE:
        case MAT_C_INT32:
        case MAT_C_UINT32:
        case MAT_C_INT16:
        case MAT_C_UINT16:
        case MAT_C_INT8:
        case MAT_C_UINT8:
        {
            mat_uint8_t    *ptr;

            ptr = matvar->data;
            nBytes = WriteData(mat,matvar->data,nmemb,matvar->data_type);
            ptr += nBytes;
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            if ( matvar->isComplex ) {
                nBytes = WriteData(mat,ptr,nmemb,matvar->data_type);
                if ( nBytes % 8 )
                    for ( i = nBytes % 8; i < 8; i++ )
                        fwrite(&pad1,1,1,mat->fp);
            }
            break;
        }
        case MAT_C_CHAR:
        {
            WriteCharData(mat,matvar->data,nmemb,matvar->data_type);
            break;
        }
        case MAT_C_CELL:
        {
            int nfields = matvar->nbytes / matvar->data_size;
            matvar_t **fields = (matvar_t **)matvar->data;

            for ( i = 0; i < nfields; i++ )
                WriteCellArrayField(mat,fields[i],compress);
            break;
        }
        case MAT_C_STRUCT:
        {
            char     **fieldnames, *padzero;
            int        fieldname_size, nfields;
            size_t     maxlen = 0;
            matvar_t **fields = (matvar_t **)matvar->data;

            nfields = matvar->nbytes / (nmemb*matvar->data_size);
            fieldnames = (char **)malloc(nfields*sizeof(char *));
            for ( i = 0; i < nfields; i++ ) {
                fieldnames[i] = fields[i]->name;
                if ( strlen(fieldnames[i]) > maxlen )
                    maxlen = strlen(fieldnames[i]);
            }
            maxlen++;
            fieldname_size = maxlen;
            while ( nfields*fieldname_size % 8 != 0 )
                fieldname_size++;
            fwrite(&fieldname_type,2,1,mat->fp);
            fwrite(&fieldname_data_size,2,1,mat->fp);
            fwrite(&fieldname_size,4,1,mat->fp);
            fwrite(&array_name_type,2,1,mat->fp);
            fwrite(&pad1,1,1,mat->fp);
            fwrite(&pad1,1,1,mat->fp);
            nBytes = nfields*fieldname_size;
            fwrite(&nBytes,4,1,mat->fp);
            padzero = (char *)calloc(fieldname_size,1);
            for ( i = 0; i < nfields; i++ ) {
                fwrite(fieldnames[i],1,strlen(fieldnames[i]),mat->fp);
                fwrite(padzero,1,fieldname_size-strlen(fieldnames[i]),mat->fp);
            }
            free(fieldnames);
            free(padzero);
            for ( i = 0; i < nmemb*nfields; i++ )
                WriteStructField(mat,fields[i],compress);
            break;
        }
        case MAT_C_SPARSE:
        {
            sparse_t *sparse = matvar->data;

            nBytes = WriteData(mat,sparse->ir,sparse->nir,MAT_T_INT32);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            nBytes = WriteData(mat,sparse->jc,sparse->njc,MAT_T_INT32);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
            nBytes = WriteData(mat,sparse->data,sparse->ndata,matvar->data_type);
            if ( nBytes % 8 )
                for ( i = nBytes % 8; i < 8; i++ )
                    fwrite(&pad1,1,1,mat->fp);
        }
    }
    end = ftell(mat->fp);
    nBytes = (int)(end-start);
    fseek(mat->fp,(long)-(nBytes+4),SEEK_CUR);
    fwrite(&nBytes,4,1,mat->fp);
    fseek(mat->fp,end,SEEK_SET);
    return 0;
}
