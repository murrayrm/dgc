#include "skynetd.h"
#include "skynetd_static.h"
#include <string.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <getopt.h>

/* this file initializes static data for skynetd */

struct sn_module* mod_list_head;
sn_sock sock_list[NUM_SOCKS];   /*stdout, stdin, stderr already exist */
int chirp_modlist_sock;
char soc_filename[108];         /* max path size as usable by bind */
int to_fork;                    /* to fork or not to fork */
char* iface;                    /* which interface to use */
#ifndef MACOSX
const char* default_iface = "eth0";     //use eth0 by default
#else
const char* default_iface = "en0";
#endif
uint32_t myip;                  /* ip address of iface in network byte order */
int key;
short int num_rmsg;             /* stores number of reliable messages sent */
struct rmultimsgi* rmultiinhead;
int debug_level;                /* used to specify verbosity of debug output*/
long long drop_rmultiin_last;

void init_data(int argc, char** argv)
{
  parsecli(argc, argv); //init_key(argc, argv);
  if(to_fork) debug_level=0;  //have no debugging device if forking
  init_soc_filename();
  myip = get_my_ip();
  num_rmsg = 0;
  mod_list_head = NULL;
  init_sock_list();
  //rmulmsgbufi.present = FALSE;
  mk_modcom_sock();
  mk_chirp_modlist_sock();
  mk_rmulti_listen_sock();
  rmultiinhead = NULL;
  SNgettime(&drop_rmultiin_last);
}

void parsecli(int argc, char** argv)
{
  extern char *optarg;
  extern int optind;
  char c;
  char* default_key = getenv("SKYNET_KEY");
  debug_level = 0;            //default is no debug info
  to_fork = 1;                //default is to fork
  iface = (char*) default_iface;      
  while ((c = getopt(argc, argv, ":d:i:f")) != -1) {   
    switch(c){
      case 'd':
        debug_level = atoi(optarg);
        break;
      case 'i':
        iface = optarg;
        break;
      case 'f':
        to_fork = 0;
        break;
      case ':':
        usage();
        break;
      case '?':
        usage();
        break;
    }
  }
  if(optind == argc - 1) key = atoi(argv[optind]);
  else if (default_key != NULL) key = atoi(default_key);
  else usage();
}

void usage()
{
    printf("usage: skynetd [-d debug_leve] [-i iface] [-f]  key \n");
    printf("debug_level is 0 to 3, with increasing verbosity.  Default is 0\n");
    printf("-f makes skynetd run in the foreground\n");
    printf("no debugging will happen if skynetd is in the background\n");
    printf("iface is which network interface to run on.  Default is eth0.\n");
    printf("Use lo if no interface is available.  (Network traffic will\n");
    printf("still be generated if lo is used and an interface is available).\n");
#ifdef NDEBUG
    printf("compiled with NDEBUG .  Most debugging facilities will be disabled");
#endif 
    exit(2);
}

void init_key(int argc, char** argv)
{
  if (2 <= argc)
  {
    key  = atoi(argv[1]);
  }
  else
  {
    exit(2);
  }
}

void init_soc_filename()
{
  char str_key[20];                 /*enough for an int*/
  strcpy(soc_filename, SOC_BASENAME);
  strcat(soc_filename, ".");
  sprintf(str_key, "%d", key);
  strcat(soc_filename, str_key);
}

void init_sock_list()
{
  int i;
  for (i=0; i < NUM_SOCKS; i++)
  {
    sock_list[i].present = FALSE;
    sock_list[i].data.module = NULL;
    sock_list[i].checktime = 0;
  }
}

uint32_t get_my_ip(){
  struct ifaddrs *myaddrs, *ifa;
  struct sockaddr_in *s4 = NULL;
  uint32_t ret;
  int status;

  status = getifaddrs(&myaddrs);
  if (status != 0)
  {
    perror("getifaddrs");
    exit(1);
  }

  for (ifa = myaddrs; ifa != NULL; ifa = ifa->ifa_next)
  {
    if ((ifa->ifa_addr != NULL) && ((ifa->ifa_flags & IFF_UP) != 0))
    {
      if ( strcmp(ifa->ifa_name, iface) == 0)
      {
        s4 = (struct sockaddr_in *)(ifa->ifa_addr);
      }
    }
  }
  if( s4 == NULL)
  {
    fprintf(stderr, "get_my_ip: eth0 not found.  Network unconfigured?\n");
    exit(1);
  }
  ret = ntohl((s4->sin_addr).s_addr);
  freeifaddrs(myaddrs);
  return ret;
}


int mk_modcom_sock(){
  int s = socket(AF_UNIX, SOCK_STREAM, 0);
  struct sockaddr_un addr;
  strcpy(addr.sun_path, soc_filename);
  addr.sun_family = AF_UNIX;
  unlink(soc_filename);
  if (bind (s, (struct sockaddr *) &addr,
    strlen(addr.sun_path) + sizeof (addr.sun_family)) <0 )
  {
    perror("bind");
    if (errno == ESPIPE)
    {
      fprintf(stderr, "perhaps another skynetd is running with the same key\n");
      fprintf(stderr, "or %s is stale.  Try to delete it.\n", soc_filename);
      exit(1);
    }
  }
  //change socket properties so others can connect
 chmod(soc_filename, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP 
        | S_IXGRP | S_IXOTH);
  int listen_succeed = listen(s, BACKLOG) ;
  if ( listen_succeed == -1 ) {
    perror("listen");
    exit(1);
  }
  if(s < NUM_SOCKS){
    sock_list[s].present = 1;
    sock_list[s].type = listening;
    sock_list[s].data.module = NULL;
    sock_list[s].checktime = 0;
  }
  else{
    fprintf(stderr, "modcom_sock > NUM_SOCKS: Increase NUM_SOCKS.  s= %d\n", s);
    exit(1);
  }
  return s;
}

int mk_chirp_modlist_sock(){
  struct sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(dg_port);
  bzero (&addr.sin_zero, 8);
  get_dg_addr(SNmodlist, &addr.sin_addr, key);
  int s = socket(AF_INET, SOCK_DGRAM, 0);
  if (s < 0 )
  {
    perror("mk_chirp_modlist_sock: socket");
    exit(1);
  }

  struct ip_mreq my_mreq;
  my_mreq.imr_multiaddr = addr.sin_addr;
  my_mreq.imr_interface.s_addr = htonl (INADDR_ANY);

  /*if (setsockopt (s, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *) &my_mreq,
      sizeof (my_mreq)))
    {
      perror ("setsockopt");
      exit (1);
    }

  if (bind (s, (struct sockaddr *) &addr, sizeof (addr)) < 0)
    {
      printf ("%s: cannot bind port %d\n", inet_ntoa (addr.sin_addr),
        dg_port);
      exit (1);
    }
*/
  int connect_suceed = connect (s, (struct sockaddr *) &addr,
                                sizeof (struct sockaddr));
  if (connect_suceed == -1)
  {
    perror ("connect");
    exit (1);
  }
  
  if(s < NUM_SOCKS){
    sock_list[s].present = 1;
    sock_list[s].type = chirp_modlist_type;
    sock_list[s].data.module = NULL;
    chirp_modlist_sock = s;                 //set global socket name
    set_checktime(s, CHIRP_PERIOD);
  }
  else{
    fprintf(stderr, "chirp_modlist_sock > NUM_SOCKS: Increase NUM_SOCKS. s= %d\n"
      , s);
    exit(1);
  }
  
  if (debug_level >= 1) printf("chirp_modlist addr: %u\n", addr.sin_addr.s_addr);
  
  return s;
}

int mk_rmulti_listen_sock(){
  struct sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(dg_port);
  bzero (&addr.sin_zero, 8);
  get_dg_addr(rmulti, &addr.sin_addr, key);
  int s = socket(AF_INET, SOCK_DGRAM, 0);
  if (s < 0 )
  {
    perror("mk_rmulti_listen_sock: socket");
    exit(1);
  }

  struct ip_mreq my_mreq;
  my_mreq.imr_multiaddr = addr.sin_addr;
  my_mreq.imr_interface.s_addr = htonl (INADDR_ANY);

  if (setsockopt (s, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *) &my_mreq,
      sizeof (my_mreq)))
    {
      perror ("setsockopt");
      exit (1);
    }

  if (bind (s, (struct sockaddr *) &addr, sizeof (addr)) < 0)
    {
      printf ("%s: cannot bind port %d\n", inet_ntoa (addr.sin_addr),
        dg_port);
      exit (1);
    }

    /* TODO connect if you want to, to discriminate against sending modulename and ID */
  if(s < NUM_SOCKS){
    sock_list[s].present = 1;
    sock_list[s].type = rmulti_listen;
    sock_list[s].data.module = NULL;
    sock_list[s].checktime = 0;
  }
  else{
    fprintf(stderr, "rmulti_listen_sock > NUM_SOCKS: Increase NUM_SOCKS. s= %d\n"
      , s);
    exit(1);
  }
  
  if (debug_level >= 1) printf("rmulti_listen addr: %u\n", addr.sin_addr.s_addr);
  
  return s;
}

