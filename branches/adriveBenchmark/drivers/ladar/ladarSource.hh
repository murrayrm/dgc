#ifndef __LADARSOURCE_H__
#define __LADARSOURCE_H__

//System Include Files
#include <iostream>
#include <fstream>
#include <sys/stat.h>

//DGC Include Files
#include <sick_driver.h>
#include <frames/frames.hh>
#include <VehicleState.hh>
#include <DGCutils>
#include <ladar/riegl_driver.hh>

#define MAX_SCAN_POINTS 1000

using namespace std;

typedef struct {
  int logRaw;
  int logState;
  int logRanges;
  int logRelative;
  int logUTM;
} ladarSourceLoggingOptions;

typedef struct{
  int numPoints;
  unsigned long long timeStamp;

	XYZcoord offset;
	RPYangle angles;

  struct datastruct {
    double ranges;
    double angles;
  } data[MAX_SCAN_POINTS];
} ladarMeasurementStruct;

//#define LADARMEASUREMENTSTRUCTBASE (sizeof(ladarMeasurementStruct)-sizeof(ladarMeasurementStruct::data))
#define LADARMEASUREMENTSTRUCTBASE (sizeof(int)+sizeof(unsigned long long) + sizeof(XYZcoord) + sizeof(RPYangle))



/*---------------------------------------------------------------------------*/
/**
  @brief A LADAR driver class

  ladarSource is the LADAR equivalent of stereoSource, and is a replacement for
  the myriad LADAR drivers that existed before.  (Actually, it technically
  combines them into a single driver.)  The basic idea with ladarSource is that
  when you initialize it, you tell it to read from file, simulation, or an
  actual ladar.  You also give it a path to a configuration file, which tells
  ladarSource what type of LADAR you're using (SICK, Riegl, etc.) as well as the
  physical constants associated with the LADAR (XYZ, RPY, scan resolution, etc).

  Then, all you have to do is scan, update the LADAR's state (so it can
  transform the scan points into UTM coordinates), and tell the ladarSource to
  log if you like.

  Specific functions that must be modified as new lower-level drivers are added
  to ladarSource have been marked with the phrase "Future Changes" - if you are
  adding a lower level driver, check to make sue that you update all of these
  functions.

*/
/*---------------------------------------------------------------------------*/
class ladarSource {
public:
  //! A basic constructor, initializes variables to 0 and such
  ladarSource();
  //! A basic destructor - doesn't do any cleanup yet!
  ~ladarSource();
  
  /*-------------------------------------------------------------------------*/
  /**
     @brief   Initialize the ladarSource
     @param   sourceType       Use files, sim, or actual ladar
     @param   ladarIDFilename  Path/filename to the config file for the ladar
     @param   logFilename      Prefix for log files (optional)
     @param   loggingEnabled   Flag to disable logging (optional)
     @return  Standard ladarSource status code

     This initializes the ladarSource object with the given parameters.  For
     example, to create a ladarSource object that reads from files, you would
     do:
     
     @code
     myLadar.init(SOURCE_FILES, "config/Ladar.ini.roof", "prefix_to_read_from", 0);
     @endcode

     init should only be called once per object.  If it is called more than once
     on a ladarSource object, further behavior of that object is undefined.

     Future Changes: If you add a LADAR that has any initialization routines,
     make sure you add them to this method.
  */
  /*-------------------------------------------------------------------------*/
  int init(int sourceType, char *ladarIDFilename, char *logFilename = "", int loggingEnabled = 0, int optladar=0);

  //! Returns the XYZ coordinates of the LADAR relative to vehicle origin
  XYZcoord getLadarPosition() {return _ladarPosOffset;};

  //! Returns the RPY angle of the LADAR relative to the vehicle frame
  RPYangle getLadarAngle() {return _ladarAngleOffset;};
  
  /*-------------------------------------------------------------------------*/
  /**
     @brief   Sets the ladar's position and angle for coord transforms
     @param   ladarPosOffset    XYZ of the LADAR wrt the vehicle origin
     @param   ladarAngleOffset  RPY of the LADAR wrt the vehicle frame
     @return  Standard ladarSource status code

     This sets the XYZ and RPY of the LADAR with respect to the vehicle frame.
     This overrides what the ladarSource object read from the configuration file
     passed to it on initialization.

  */
  /*-------------------------------------------------------------------------*/
  int setLadarFrame(XYZcoord ladarPosOffset, RPYangle ladarAngleOffset);

  //! Not yet implemented!
  int stop();

	void startScanning();
	void stopScanning();

  /*-------------------------------------------------------------------------*/
  /**
     @brief   Tells the ladarSource object to scan
     @return  Local timestamp of when the scan took place

     This performs a scan, reading and processing the raw data so it is ready
     for use, and returns a timestamp of the local time of the computer as close
     to the time when the actual scan was taken as possible.

  */
  /*-------------------------------------------------------------------------*/
  unsigned long long scan();

  //! Not yet implemented!
  unsigned long long scan(unsigned long long timestamp, int useLoggedState = 1);
  
  //! Returns the current logging options for the ladarSource object
  ladarSourceLoggingOptions getLoggingOptions() {return _loggingOpts;};

  //! Returns a pointer to a string containing the base filename used for logging
  char* getLogFilename() {return _currentFilename;};

  /*-------------------------------------------------------------------------*/
  /**
    @brief  Starts automatic logging by the ladarSource object
    @param  loggingOpts  Struct describing what ladarSource should log
    @param  baseFilename Base filename to use while logging
    @return Standard ladarSource status code

    This is the function you call to make the ladarSource object automatically
    start logging.  It will then automatically log the data types specified in
    the logging options you give it.

  */
  /*-------------------------------------------------------------------------*/
  int startLogging(ladarSourceLoggingOptions loggingOpts, const char *baseFilename="");

  //! Toggles the status of all logging, and returns whether logging is enabled
  int toggleLogging();

  //! Disables logging, closes files, and returns a standard status code
  int stopLogging();

  //! Returns the code of the current source type
  int currentSourceType() {return _currentSourceType;};

  //! Returns the index of the current scan
  int scanIndex() {return _scanIndex;};

  //! Resets the scan index to 0, returns a standard status code
  int resetScanIndex();

	//! Power cycles the LADAR
	int reset();

  /*-------------------------------------------------------------------------*/
  /**
    @brief  Sets the current vehicle state for the LADAR
    @return Standard ladarSource status code

    This function updates the vehicle state the ladarSource object uses to
    transform scans into UTM points.  If you want the UTM points to make sense,
    you must use this function.
  */
  /*-------------------------------------------------------------------------*/
  int updateState(VehicleState state);

  //! Returns the number of valid points in the latest scan
  int numPoints();

  //! Returns the range (in meters) of the ith point of the latest scan
  double range(int i);

  //! Returns the angle (in radians) of the ith point of the latest scan
  double angle(int i);

	unsigned long long timestamp(int i);

  /*-------------------------------------------------------------------------*/
  /**
     @brief   Returns the XYZ coords (relative to LADAR) of the ith point of 
              the latest scan
     @param   i :  integer index of scan point
     @return  XYZcoord in the ladar coordinate frame 
  */
  /*-------------------------------------------------------------------------*/
  XYZcoord ladarPoint(int i);

  //! Returns the NED coords (in UTM) of the ith point of the latest scan
  NEDcoord UTMPoint(int i);

  //! Returns the current state associated with the LADAR scan
  VehicleState currentState() {return _currentState;};

  //! Returns a pointer to a string containing a text error/status message
  char* getErrorMessage() {return _errorMessage;};

  //! Returns the resolution of the LADAR in radians
  double getResolutionRads() {return _resolution*M_PI/180.0;};
  
  //! Various status messages the ladarSource methods may return
  enum eLadarSourceStatus {
    OK,
    UNKNOWN_SOURCE,
    NO_FILE,
    NOT_IMPLEMENTED,    

    UNKNOWN_ERROR
  };

  //! The different source types a LADAR may be
  enum eLadarSourceType {
    SOURCE_UNASSIGNED,
    SOURCE_LADAR,
    SOURCE_FILES,
    
    SOURCE_UNKNOWN
  };

  /**
    The different types of LADARs supported so far
    Future Changes: If another LADAR type is added that must act differently in
    the code, add it here
  */
  enum eLadarType {
    TYPE_SICK_LMS_221,
    TYPE_RIEGL, 
    TYPE_UNKNOWN
  };

private:
  /**
    Function to parse a configuration file, and assign parameters appropriately
    Future Changes: If a LADAR with more features (or more parameters that must
    be read at initialization) is added, make sure you change this function so
    that it can read the new parameters from the config files
  */
  int parseConfigFile(char* configFilename);

  /**
    Given a string describing a LADAR, returns the proper eLadarType
    Future Changes: If you add another LADAR type, make sure to adjust this
    function appropriately
  */
  int getLadarType(char* ladarString);

  /**
    Given a eLadarType describing a LADAR type, returns a character string
    describing the LADAR.
    Future Changes: If you add another LADAR type, make sure to adjust this
    function appropriately
  */
  char* getLadarType(int ladarInt);

  //Struct to store the logging options
  ladarSourceLoggingOptions _loggingOpts;

  //Struct to store the position/orientation of the LADAR with respect to the
  //vehicle origin
  frames _ladarFrame;

  //This actually stores the XYZ position of the LADAR with respect to the
  //vehicle origin, and is used to update the _ladarFrame variable
  XYZcoord _ladarPosOffset;

  //This actually stores the RPY orientation of the LADAR with respect to the
  //vehicle frame, and is used to update the _ladarFrame variable
  RPYangle _ladarAngleOffset;
  
  //Stores the state of the vehicle to make UTM coorindates
  VehicleState _currentState;

  //Actual Ladars
  SickLMS200 _sickLadar;
  CRieglLadar _riegl;
  
  //Base filename to build log filenames from
  char _currentFilename[256];

  //Configuration file to read config info from
  char _configFilename[256];

  //File pointers to the log files for various data
  //FILE* _logFileRaw;  
  FILE* _logFileState;  
  FILE* _logFileRelative;  
  FILE* _logFileRanges;  
  FILE* _logFileUTM;  

  //Flags to indicate whether logging is enabled or paused
  int _loggingEnabled;
  int _loggingPaused;

  //Variables to store the current source type and ladar type being used
  int _currentSourceType;
  int _currentLadarType;

  int _numPoints;
  int _scanIndex;
  double _ranges[MAX_SCAN_POINTS];
  double _angles[MAX_SCAN_POINTS];
	unsigned long long _timestamps[MAX_SCAN_POINTS];

  unsigned long long _latestScanTimestamp;

  int _scanAngle; // total angular field of view (DEGREES)
  double _resolution; // angular resolution (DEGREES)
  double _ladarUnits; // fraction of meter for range reporting

  // calculated angle between full right and first scan measurement 
  // angle (positive by definition).  The Sicks scan counterclockwise
  // from above, meaning that the scans sweep from right to left in
  // front of the sensor.
  double _scanAngleOffset; 


  /* String in which to store text error messages
    Future Changes: If you add functionality and want more error 
    messages (and the ability to read text error messages - then make 
    use of this string, and have code that uses ladarSource read from 
    it and print it to the screen.
  */
  char _errorMessage[1024];
};

#endif  //__LADARSOURCE_H__
