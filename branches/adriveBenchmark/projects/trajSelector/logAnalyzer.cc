#include <fstream>
#include <iostream>
#include <trajSelector.h>

using namespace std;

#define ANALYSIS_FILE "trajSelector.anal"

bool completedOneLoop = false;

string log_directory;
string header_file_location;
string log_file_location;
string result_file_location;

int main(int argc, char* argv[])
{

  if(argc <= 1)
    {
      cout<<"Error, no directory specified - you need to enter in the directory where the log files can be found.  Enter in a relative path to the directory containing the file you would like to analyze (i.e., logs/2005_08_26/21_45_30/)."<<endl;
      cout<<"MAKE SURE THAT THE DIRECTORY NAME ENDS WITH A '/', OR NOTHING WILL WORK RIGHT!  YOU HAVE BEEN WARNED!!!"<<endl;
      exit(1);
    }

  cout<<endl<<"Obtaining files located in directory: "<<argv[1]<<endl;

  string log_directory(argv[1]);

  header_file_location = log_directory + "trajSelector/" + HEADER_FILE;
  log_file_location = log_directory + "trajSelector/" + LOG_FILE;
  result_file_location = log_directory + "trajSelector/" + ANALYSIS_FILE;

  ifstream headerFile(header_file_location.c_str());
  ifstream logFile(log_file_location.c_str());
  ofstream resultFile(result_file_location.c_str());

  cout<<"Creating analysis file: "<<result_file_location<<endl;
  resultFile<<"Results for log files: "<<header_file_location<<" and "<<log_file_location<<":"<<endl<<endl;

  cout<<endl<<"Operational Planners: "<<endl;
  resultFile<<"Operational Planners: "<<endl;

  int numPlanners = 0;
  int currentPlanner;
  double hysteresisParameter;
  
  int blockLength;  //Length of a single cycle block of data
  
  //Keep track of the number of times we've switched trajs
  
  long int numSwitches = 0;
  long int numErrors = 0;
  
  //keep track of the number of selection cycles
  long int numCycles = 0;
  
  //Keep track of how many times each planner was selected
  long int* plannersSelected;
  
  bool* excessiveSpeeds;
  bool* badTrajs;
  
  for(int i = 0; i < 5; i++)
    {
      headerFile >> currentPlanner;
      if(currentPlanner == 1)
	{
	  numPlanners++;
	  resultFile<<"Planner "<<i<<endl;
	  cout<<"Planner "<<i<<endl;
	}
    }

  resultFile<<endl;
  
  headerFile >> hysteresisParameter;

  resultFile<<"Hysteresis Parameter: "<<hysteresisParameter<<endl<<endl;
  cout<<"Hysteresis Parameter: "<<hysteresisParameter<<endl<<endl;

  resultFile<<"Analysis data follows: "<<endl<<endl;
  cout<<"Analysis data follows: "<<endl<<endl;
  
  int previousPlanFollowed, plannerNum, newPlanToFollow;
  plannersSelected = new long int [numPlanners];
  
  for(int i =0; i < numPlanners; i++)
    {
      plannersSelected [i] = 0;
    }
  
  blockLength = numPlanners + 5;
  
  excessiveSpeeds = new bool[numPlanners];
  badTrajs = new bool[numPlanners];
  double avgSpeeds [numPlanners];
  double elapsedTimes [numPlanners];
  
  bool allExcessiveSpeeds;
  
  int boolVal;

  //Used to create a correspondence between the number assigned to each of the planners and their index number in the calculations below.
  
  int plannerIndex[numPlanners]; 
  int previousPlanIndex;
  int newPlanIndex;
  
  while(!logFile.eof())
    {
      numCycles++;
      
      allExcessiveSpeeds = true;
      
      //Need to remember to check log file format - this could be off
      logFile >> previousPlanFollowed;
      
      //Get data out of file
      for(int i = 0; i < numPlanners; i++)
	{
	  //Look at planner number
	  logFile >>  plannerNum;

	  if(!completedOneLoop)
	    {
	      plannerIndex[i] = plannerNum;  
	    }
	  
	  //Record the planner's average speed
	  logFile >> avgSpeeds [i];
	  
	  //Record the elapsed time
	  logFile >> elapsedTimes[i];
	}
      
      //Read bool arrays
      
      //Read excessive speeds array
      for(int i = 0; i < 5; i++)
	{
	  logFile >> boolVal;
	  
	  if(boolVal == 1)
	    {
	      excessiveSpeeds[i] = true;
	    }
	  
	  else
	    {
	      excessiveSpeeds[i] = false;
	    }
	  
	}
      
      //Read badTrajs array
      for(int i = 0; i < 5; i++)
	{
	  logFile >> boolVal;
	  
	  if(boolVal == 1)
	    {
	      badTrajs[i] = true;
	    }
	  else
	    {
	      badTrajs[i] = false;
	    }
	  
	}
      
      logFile >> newPlanToFollow;
      
      //Ok, now we've gotten all the data.  Need to do some kind of analysis on it.
      
      for(int i = 0; i <numPlanners; i++)
	{
	  if(!excessiveSpeeds [i])
	    {
	      allExcessiveSpeeds = false;
	    }
	}
      
      for(int i = 0; i < numPlanners; i++)
	{
	  if(plannerIndex[i] == previousPlanFollowed)
	    {
	      previousPlanIndex = i;
	    }
	  
	  
	  
	  if(plannerIndex[i] == newPlanToFollow)
	    {
	      newPlanIndex = i;
	    }
	}	
      
      double maxSpeed  = 0;
      int bestTrajIndex = 0;  //Index of the traj we currently think is the best candidate.
      
      if(!allExcessiveSpeeds)  //At least one planner has okay speeds
	{
	  for(int i = 0; i < numPlanners; i++)
	    {	

	      if(i == previousPlanIndex)  //This traj should get a speed bonus, as we were previously following it
		{
		  if((maxSpeed < avgSpeeds[i] * (1+hysteresisParameter)) && !badTrajs[i] && !excessiveSpeeds[i])
		  {
		    maxSpeed = avgSpeeds[i] * (1 + hysteresisParameter);
		    bestTrajIndex = i;
		  }
		}
		     
	      else
		{
		  if((maxSpeed < avgSpeeds[i]) && !badTrajs[i] && !excessiveSpeeds[i])
		    {
		      maxSpeed = avgSpeeds[i];
		      bestTrajIndex = i;
		    }
		}
	    }
	}
      
      //Every traj had excessive speeds, so we should pick the one with the lowest average speed
      
      else
	{
	  for(int i = 0; i < numPlanners; i++)
	    {
	      if((avgSpeeds[i] < avgSpeeds [bestTrajIndex]) && !badTrajs[i])
		{
		  bestTrajIndex =i;
		}
	    }
	}
	  
      if(bestTrajIndex != newPlanIndex)
	{
	  //Error in selection; should record here.
	  numErrors++;
	  cout<<"Error in selection cycle "<< numCycles<<" at line "<< (numCycles -1)*blockLength +1<<endl;
	  resultFile<<"Error in selection cycle "<< numCycles<< " at line "<< (numCycles -1)*blockLength +1<<endl;
	 
	}
      
      //Record which planner got selected, and if a switch occurred.
      
      plannersSelected[newPlanIndex]++;


      if(newPlanToFollow != previousPlanFollowed)
	{
	  numSwitches++;
	  cout<<"Traj switching detected in selection cycle "<<numCycles<< " at line "<<(numCycles -1)*blockLength +1<<endl;
	  resultFile<<"Traj switching detected in selection cycle "<<numCycles<< " at line "<<(numCycles -1)*blockLength +1<<endl;
	}

      if(!completedOneLoop)
	{
	  completedOneLoop = true;
	}

    } //End of while loop
  
      //Now we've read the file and extracted all the useful data, so we should record it, or at least print it out.
  
  cout<<endl<<"trajSelector performed "<<numCycles << " selections."<<endl;
  resultFile<<endl<<"trajSelector performed "<<numCycles << " selections."<<endl;
  
  for(int i = 0; i < numPlanners; i++)
    {
      cout<<"Planner "<<plannerIndex[i]<<" was selected "<< plannersSelected[i] <<" times, or "<< (double) 100*plannersSelected[i]/numCycles <<"% of all selections."<<endl;
      resultFile<<"Planner "<<plannerIndex[i]<<" was selected "<< plannersSelected[i] <<" times, or "<< (double) 100*plannersSelected[i]/numCycles <<"% of all selections."<<endl;
    }

  cout<<"trajSelector performed "<<numSwitches<<" trajectory switches during execution."<<endl;
  resultFile<<"trajSelector performed "<<numSwitches<<" trajectory switches during execution."<<endl;
  
  cout<<"trajSelector had "<<numErrors<< " errors during selection."<<endl;
  resultFile<<"trajSelector had "<<numErrors<< " errors during selection."<<endl;

  headerFile.close();
  logFile.close();
  resultFile.close();
}
