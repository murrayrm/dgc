FOLLOW_PATH = $(DGC)/projects/cds110b

FOLLOW_DEPEND_LIBS = $(FALCONLIB) \
										 $(MATIOLIB) \
							 			 $(SPARROWLIB) \
										 $(PIDCONTROLLERLIB) \
                     $(TRAJLIB) \
										 $(CPIDLIB) \
                     $(SKYNETLIB) \
                     $(MODULEHELPERSLIB)

FOLLOW_DEPEND_SOURCE = \
											 $(FOLLOW_PATH)/FollowClient.hh \
											 $(FOLLOW_PATH)/FollowClient.cc \
											 $(FOLLOW_PATH)/follow.cc 	    

FOLLOW_DEPEND = \
							  $(FOLLOW_DEPEND_LIBS) \
						    $(FOLLOW_DEPEND_SOURCE)