/*
 * FollowClient.cc - skynet module for follow
 *
 * RMM, 10 Dec 05
 *
 */

#include "FollowClient.hh"

FollowClient::FollowClient(int sn_key) 
	: CSkynetContainer(MODfollow, sn_key)
{
  cerr << "FollowClient initalized on key " << sn_key << "\n";

	int Y_FRONT = 1;
	int A_HEADING_FRONT = 0;
	int A_HEADING_REAR = 0;

	//Start direct copying from TrajFollower constructor
  EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
  EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : ( A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));
	//End direct copying from TrajFollower constructor
	m_speedController = new CPID_Controller(yerrorside, aerrorside);

// 	cerr << "Loading controller from file " << ctrlFilename << endl;

// 	mat_t* pMat = Mat_Open(ctrlFilename, MAT_ACC_RDONLY);

	//MATFile* pMat = matOpen(ctrlFilename, "r");
	m_lateralController = NULL;


	m_traj = NULL;

	setupFiles();

	controlRate = 10.0;

	sprintf(logFilename, "");

	loggingEnabled = false;
	useAutoLogNaming = true;
	controlEnabled = disabled;

}

/* 
 * Member function for reading state
 *
 */
// void FollowClient::ReadState()
// {
//   cerr << "Calling ReadState() function\n";

//   while (1) {
//     // Update the current state (via broadcast message)
//     UpdateState();
// 		UpdateActuatorState();

//     // Put the data in a simple array for use by the controller
//     inp[XPOS] = m_state.Northing - xorigin;
//     inp[YPOS] = m_state.Easting - yorigin;
//     inp[TPOS] = m_state.Yaw;

//     inp[XVEL] = m_state.Vel_N;
//     inp[YVEL] = m_state.Vel_E;
//     inp[TVEL] = m_state.YawRate;
// 		sleep(1);
//   }
// }

/*
 * Member function for reading trajectories
 *
 */
// void FollowClient::ReadTraj()
// {
//   cerr << "Calling ReadTraj() function\n";

//   while (1) {
//     sleep(1);
//   }
// }

/*
 * Member function for executing control loop
 *
 */
void FollowClient::ControlLoop()
{
//   cerr << "Calling ControlLoop() function\n";

	double accelCmd, phi, vRef = 10, accel_Norm, steer_Norm;

  drivecmd_t my_command;
  m_adriveMsgSocket = m_skynet.get_send_sock(SNdrivecmd);

	string logs_location = "";

	double* controllerOutput;

	unsigned long long timeNow, timeDiff;

	double numSecTotal;

	unsigned long long numMicroSecTotal;
	unsigned long long microSecStartProcessing;
	unsigned long long microSecStopProcessing;
	double trajVector[11];

  while (1) {
		DGCgettime(microSecStartProcessing);		

    // Update the current state (via broadcast message)
    UpdateState();
		UpdateActuatorState();

		if(controlEnabled == enabled) {
			DGCgettime(timeNow);
			timeDiff = timeNow-timeStart;
			currentTime = DGCtimetosec(timeDiff);
		} else if(controlEnabled == disabled) {
			currentTime = 0.0;
		}


		if(controlEnabled == enabled) {
			if(traj_read(m_traj, trajVector, currentTime) == 2) {
				disableControl(0);
			} else {
				outFF[V] = trajVector[0];
				outFF[PHI] = trajVector[1];

				for(int i=0; i<NUMINP; i++) {
					inp[NUMINP+i] = trajVector[2+i];
				}
				//Reorigin the desired stuff
				inp[NUMINP+XPOS]-=xorigin;
				inp[NUMINP+YPOS]-=yorigin;
			}
		} else if(controlEnabled == disabled) {
			if(m_traj != NULL) 
				traj_read(m_traj, trajVector, currentTime);
			outFF[V] = trajVector[0];
			outFF[PHI] = trajVector[1];
			
			for(int i=0; i<NUMINP; i++) {
				inp[NUMINP+i] = trajVector[2+i];
			}			
			//Reorigin the desired stuff
			inp[NUMINP+XPOS]-=xorigin;
			inp[NUMINP+YPOS]-=yorigin;
		}


    // Put the data in a simple array for use by the controller
    inp[XPOS] = m_state.Northing - xorigin;
    inp[YPOS] = m_state.Easting - yorigin;
    inp[TPOS] = m_state.Yaw;

    inp[XVEL] = m_state.Vel_N;
    inp[YVEL] = m_state.Vel_E;
    inp[TVEL] = m_state.YawRate;

		inp[XACC] = m_state.Acc_N;
		inp[YACC] = m_state.Acc_E;
		inp[TACC] = m_state.YawAcc;


		//Perform velocity control
		if(outOverride[V])
			vRef = outRef[V];
		else
			vRef = outFF[V];
		m_speedController->getVelocityControl_NoErrorChecking(&m_state, &m_actuatorState, &accelCmd, &phi, vRef);

		outCtrl[V] = accelCmd;
// 		outFF[V] = ;
		outCmd[V] = outGain[V]*(outCtrl[V]);// + outFF[V]);
		outState[V] = m_state.Speed2();

		accel_Norm = fmax(-1.0, fmin(outCmd[V], 1.0));
		my_command.my_actuator = accel;
		if(controlEnabled == enabled) 
			my_command.number_arg = accel_Norm;
		else
			my_command.number_arg = -1.0;
		m_skynet.send_msg(m_adriveMsgSocket, &my_command, sizeof(my_command), 0);

		outState[PHI] = m_actuatorState.m_steerpos*VEHICLE_MAX_AVG_STEER;
		//Perform steering control
		if(controlEnabled == enabled) {
			controllerOutput = ss_compute(m_lateralController, inp);
			outCtrl[PHI] = controllerOutput[0];
		//outFF[PHI] = ;
		outCmd[PHI] = outGain[PHI]*(outCtrl[PHI] + outFF[PHI]);

		steer_Norm = outCmd[PHI]/VEHICLE_MAX_AVG_STEER;
		steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);
		}

		my_command.my_actuator = steer;
		if(controlEnabled == enabled) 
			my_command.number_arg = steer_Norm;
		else
			my_command.number_arg = 0.0;
		m_skynet.send_msg(m_adriveMsgSocket, &my_command, sizeof(my_command), 0);

		//Calculate Errors
		for(int i=0; i<NUMINP; i++) {
			err[i] = inp[i] - inp[i+NUMINP];
		}

		if(loggingEnabled) writeLog();


		numSecTotal = 1.0/controlRate;
		numMicroSecTotal = (unsigned long long)(numSecTotal*1.0e6);


		DGCgettime(microSecStopProcessing);



		if(numMicroSecTotal - 1000 > (microSecStopProcessing - microSecStartProcessing)) {
			unsigned long long sleepTime;
			DGCusleep(numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing) - 1000);
			DGCgettime(sleepTime);
			actualRate = 1.0e6/((double)(sleepTime - microSecStartProcessing));
// 			if(((sleepTime-microSecStopProcessing) > (numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)))) {
// 				timeDiffFoo = ((sleepTime-microSecStopProcessing) - (numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)));
// 			} else {
// 				timeDiffFoo = ((numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)) - (sleepTime-microSecStopProcessing));
// 			}
// 			sprintf(statusMessage, "%llu, %llu, should sleep for: %llu, slept for: %llu, diff is %llu", 
// 							numMicroSecTotal, microSecStopProcessing-microSecStartProcessing, 
// 							numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing),
// 							sleepTime-microSecStopProcessing,
// 							timeDiffFoo);
// 							(sleepTime-microSecStopProcessing) - (numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)));
		} else {
			actualRate = 1.0e6/((double)(microSecStopProcessing - microSecStartProcessing));
		}
			 
  }
}



void FollowClient::writeLog() {
	if(logFile.is_open()) {
		unsigned long long actualTime;
		DGCgettime(actualTime);
		logFile << actualTime 
						<< " " << currentTime 
						<< " " << controlEnabled 
						<< " " << outState[V] 
						<< " " << outState[PHI]
						<< " " << outCmd[V]
						<< " " << outCmd[PHI];
		logFile << " " << setprecision(10) << inp[XPOS]+xorigin
						<< " " << setprecision(10) << inp[YPOS]+yorigin;

		for(int i=2; i<NUMINP; i++) {
			logFile << " " << setprecision(10) << inp[i];
		}

		logFile << " " << setprecision(10) << inp[NUMINP+XPOS]+xorigin
						<< " " << setprecision(10) << inp[NUMINP+YPOS]+yorigin;

		for(int i=2+NUMINP; i<NUMINP*2; i++) {
			logFile << " " << setprecision(10) << inp[i];
		}

		logFile << endl;
	} else {
		cout << "file ainb't open" << endl;
	}
}


bool FollowClient::toggleLogging() {
	if(loggingEnabled) {
		logFile.close();
		loggingEnabled = false;
	} else {
		loggingEnabled = true;
		if(useAutoLogNaming) {
			time_t* currentTime;
			time(currentTime);
			tm* tmstruct = localtime(currentTime);
			sprintf(logFilename, "%04d%02d%02d_%02d%02d%02d.log", tmstruct->tm_year + 1900, 
							tmstruct->tm_mon + 1, tmstruct->tm_mday, tmstruct->tm_hour, 
							tmstruct->tm_min, tmstruct->tm_sec); 
		}

		if(logFile.is_open())
			logFile.close();
		logFile.open(logFilename, ofstream::out | ofstream::app);
	}

	return loggingEnabled;
}


bool FollowClient::setControlStatus(FollowClient::ctrlStatus status) {
	bool returnVal = false;

	switch(controlEnabled) {
	case enabled:
		switch(status) {
		case enabled:
			break;
		case paused:
			controlEnabled = status;
			DGCgettime(timePause);
			returnVal = true;
			break;
		default:
		case disabled:
			controlEnabled = status;
			returnVal = true;
			break;
		}
		break;
	case paused:
		switch(status) {
		case enabled:
			unsigned long long timeResume;
			DGCgettime(timeResume);
			timeStart = timeStart + timeResume - timePause;
			controlEnabled = status;
			returnVal = true;
			break;
		case paused:
			break;
		default:
		case disabled:
			controlEnabled = status;
			returnVal = true;
			break;
		}
		break;
	default:
	case disabled:
		switch(status) {
		case enabled:
			if((m_traj != NULL &&
				 m_lateralController != NULL) ||
				 false) {
				DGCgettime(timeStart);
				controlEnabled = status;
				traj_reset(m_traj);
				returnVal = true;
			} else {
				sprintf(statusMessage, "Could not resume control since file is missing!");
			}
			break;
		case paused:
			break;
		default:
		case disabled:
			break;
		}
		break;
	}

	return returnVal;
}


void FollowClient::setupFiles() {
	if(m_lateralController != NULL)
		ss_free(m_lateralController);

	if(m_traj != NULL)
		traj_free(m_traj);


	m_lateralController = ss_load(ctrlFilename);
	if(m_lateralController == NULL) {
		sprintf(statusMessage, "Error loading controller file '%s'", ctrlFilename);
	} else {
		sprintf(statusMessage, "Controller loaded!");
	}
		

// 	cerr << "Loading traj from file " << trajFilename << endl;
	m_traj = traj_load(trajFilename);
	if(m_traj == NULL) {
		sprintf(statusMessage, "%s, Error loading trajectory file '%s'", statusMessage, trajFilename);
	} else {
		sprintf(statusMessage, "%s, Trajectory loaded!", statusMessage);
	}
	
	

	
// 	cerr << "Traj loaded successfully!" << endl;

// 	cerr << "Controller loaded succesfully!" << endl;

	for(int i=0; i<NUMOUT; i++) {
		outGain[i] = 1.0;
		outCtrl[i] = 0.0;
		outFF[i] = 0.0;
		outCmd[i] = 0.0;
		outOverride[i] = 0;
		outRef[i] = 0.0;
		outState[i] = 0.0;
	}


}

