#ifndef CPATH_HH
#define CPATH_HH



using namespace std;

/********************************************
 * Options available to the user
 */

/** Whether or not to actually send traj's over skynet */
//#define SEND_TRAJ 1


#include "StateClient.h"
#include "TrajTalker.h"
#include <pthread.h>
#include <unistd.h>
#include "pathLib.hh"
#include "herman.hh"



#include "AliceConstants.h" 
#include <iomanip>
#include <DGCutils>



/**
 * CPath class.
 */
class CPath
{
public:
  /** Contstructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  CPath(char * rddf_file);

  /** Standard destructor */
  ~CPath();

  void CPath::SeedFromLocation(double n, double e, double yaw, CTraj * destination_traj, 
			       double merge_length);

  void UpdateState(double n, double e);

  double GetChopBehind();
  double GetChopAhead();
  void SetChopBehind(double value);
  void SetChopAhead(double value);

private:
  // variables
  corridorstruct corridor_whole;
  pathstruct path_whole_sparse;
  herman* path_whole_sparse_rddf;
  double chop_behind;
  double chop_ahead;

  /** called when we need to know where we are (So we know about which point to
   * crop the traj */
  void GetLocation(vector<double> & location);

  VehicleState m_state;
};

#endif  // CPATH_HH
