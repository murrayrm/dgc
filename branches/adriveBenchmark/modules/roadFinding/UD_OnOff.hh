//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_OnOff
//----------------------------------------------------------------------------

#ifndef UD_ONOFF_DECS

//----------------------------------------------------------------------------

#define UD_ONOFF_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "opencv_utils.hh"

#include "UD_ImageSource.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#ifndef PI
#define PI      3.14159265358979323846
#endif

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

#define NOROADVP_ERROR   1
#define SUNGLARE_ERROR   2
#define SHADOW_ERROR     3

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_OnOff
{
  
 public:

  // variables
  bool using_OnOff;

  double KL_now;                              ///< current KL divergence
  double KL_threshold;                        ///< minimum KL divergence between vote function and uniform distribution to think 
                                             ///< that this image is good
  int KL_time_window;                        ///< how many past images to take into account for filtering on road decision 
  int KL_short_time_window;                    ///< how many past images to take into account for filtering off road decision 
  float KL_majority_fraction;                ///< what fraction of images in time window must be good for filtering
  int print_onoff;                           ///< print on/off road decision parameters as they are computed?
  int do_onoff;                              ///< carry out on/off road decision computations on this image?

  int KL_index;                              ///< index in time window-sized buffer that current image on/off road confidence occupies
  float *KL_history;                         ///< time window-sized buffer of image on/off road confidence values
  int KL_history_full;                       ///< flag indicating whether KL_time_window frames have been processed yet
  int KL_num_vote_levels;                    ///< maximum number of votes a particular location in vote function can receive
  float *KL_histogram;                       ///< distribution of how many locations (pixels) in vote function had how many votes
  float KL_uniform_prob;                     ///< 1 / KL_num_vote_levels in every bin of KL_histogram is uniform distribution

  float onroad_confidence;                   ///< fraction of measured KL values in time window that exceed KL_threshold
  int onroad_state;                          ///< TRUE if we believe vehicle can see a road, FALSE if not (confidence >= majority fraction) 
  int noroad_reason;                         ///< why onroad_state is FALSE

  IplImage *redim;                           ///< scratch image for reddening display when off-road
  IplImage *yellowim;                        ///< scratch image for yellowing display when sun saturation
  IplImage *blueim;                          ///< scratch image for blueing display when vehicle shadow
  IplImage *colorim;                         ///< product of color indicator and source image

  IplImage *saturation_mask;                 ///< 1 = source image pixel is saturated, 0 = pixel is not saturated

  double SAT_now;                            ///< current maximum column saturation fraction
  double SAT_threshold;                      ///< maximum column saturation fraction to think that this image is good
  int SAT_time_window;                       ///< how many past images to take into account for filtering on road decision 
  float SAT_majority_fraction;               ///< what fraction of images in time window must be good for filtering
  int SAT_index;                              ///< index in time window-sized buffer that current image on/off road confidence occupies
  float *SAT_history;                         ///< time window-sized buffer of image on/off road confidence values
  int SAT_history_full;                       ///< flag indicating whether SHAD_time_window frames have been processed yet

  double SHAD_azi_threshold;                      ///< minimum azimuthal difference to think that this image is bad
  double SHAD_alt_threshold;                      ///< maximum altitude to think that this image is bad
  int SHAD_time_window;                       ///< how many past images to take into account for filtering on road decision 
  float SHAD_majority_fraction;               ///< what fraction of images in time window must be good for filtering
  int SHAD_index;                              ///< index in time window-sized buffer that current image on/off road confidence occupies
  float *SHAD_azi_history;                         ///< time window-sized buffer of image on/off road confidence values
  float *SHAD_alt_history;                         ///< time window-sized buffer of image on/off road confidence values
  int SHAD_history_full;                       ///< flag indicating whether SAT_time_window frames have been processed yet

  // functions

  UD_OnOff(UD_ImageSource *, float, int, float, float);

  void compute_confidence(IplImage *, IplImage *);
  float KL_from_uniform(IplImage *);
  void process_command_line_flags(int, char **);

  int too_dark(double);
  //  int possible_glare(double, double, double);
  int possible_shadow(double, double, double, double);
  int shadow_or_darkness(IplImage *, double, double, double, double);

  int sun_saturation(IplImage *);

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
