/* This Is A Initial Try At Building A Threaded Adrive.  * It Lays Down The Functions Needed For Each Actuator.  
 * And Provides All The Structure.  
 * Tully Foote
 * 12/29/04
 */

/* This now holds most of the funcitons called by adrive. The main function
 * is in adriveMain.c 
 * Tully Foote
 * 8/09/05
 */



#include "adrive.h"
#include "askynet_monitor.hh"

#include "sparrow/display.h"     /* sparrow display header */
#include "sparrow/debug.h"       /* sparrow debugging routines */
#include "addisp.h"			/* adrive display table */

using namespace std;


// Set initial values of flags.  
int run_sparrow_thread = false;
int run_skynet_thread = false;
int run_supervisory_thread = false;
int run_logging_thread = false;
int run_obdii_thread = false;
pthread_cond_t steer_calibrated_flag;
int simulation_flag = false;

// A place to store the IP of the simulator
char simulator_IP[15];


struct vehicle_t my_vehicle;		/* The vehicle data structure */

int flag_gas_condition(long x);
int flag_trans_condition(long x);
int flag_brake_condition(long x);
int flag_steer_condition(long x);
int flag_steer_enable(long x);


/******************** READ CONFIG FILE ******************/
/* This function opens adrive.config and fills in the vehicle struct.
 * The most important parts are reading the ports for the actuators,
 * as well as finding out which threads are disabled.  By default the vehicle
 * will be in expected race ready condition. */

void read_config_file(vehicle_t & my_vehicle) 
{
  ifstream infile("adrive.config");

  std::string cmd;
  event << "Reading Config File";
  stringstream dummy_string;
  while ( ! infile.eof() )
    {
      infile >> cmd;
      /* TO BE SAFE ALL OF THESE WRITINGS WOULD NEED TO BE MUTEX LOCKED
       * BUT SINCE THIS IS A ONETIME START UP BEFORE THE OTHER THREADS START 
       * IT IS OK.  IF THAT CHANGES LOCKING MUST BE ADDED.  */
      // Serial Ports
      if( cmd == "gas_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_GAS].port;
	  dummy_string.str(""); 
	  dummy_string << "using gas_port " << my_vehicle.actuator[ACTUATOR_GAS].port << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "brake_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_BRAKE].port;
	  dummy_string.str(""); 
	  dummy_string << "using brake_port" << my_vehicle.actuator[ACTUATOR_BRAKE].port << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "trans_port" )
	{
	  infile >> my_vehicle.actuator_trans.port;
	  dummy_string.str(""); 
	  dummy_string << "using trans_port "<< my_vehicle.actuator_trans.port << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "estop_port" )
	{
	  infile >> my_vehicle.actuator_estop.port;
	  dummy_string.str(""); dummy_string << "using estop_port" << my_vehicle.actuator_estop.port << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "steer_port" )
	{
	  infile >> my_vehicle.actuator[ACTUATOR_STEER].port;
	  dummy_string.str(""); 
	  dummy_string << "using steer_port " << my_vehicle.actuator[ACTUATOR_STEER].port << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "obdii_port" )
	{
	  infile >> my_vehicle.actuator_obdii.port;
	  dummy_string.str(""); 
	  dummy_string <<"using steer_port " << my_vehicle.actuator[ACTUATOR_STEER].port << endl;
	  event << dummy_string.str();
	}


      /***********************************
       * Check for disabled threads.
       ***********************************/
      // Status Threads
      else if ( cmd == "enable_steer_status" )
	{
	  my_vehicle.actuator[ACTUATOR_STEER].status_enabled = 1;
	  infile >> my_vehicle.actuator[ACTUATOR_STEER].status_sleep_length;
	  dummy_string.str(""); 
	  dummy_string << "STEER Status enabled, sleep length " <<  my_vehicle.actuator[ACTUATOR_STEER].status_sleep_length;
	  event << dummy_string.str();
	}
      else if ( cmd == "enable_brake_status" )
	{
	  my_vehicle.actuator[ACTUATOR_BRAKE].status_enabled = 1;
	  infile >> my_vehicle.actuator[ACTUATOR_BRAKE].status_sleep_length;
	  dummy_string.str(""); 
	  dummy_string << "BRAKE Status enabled, sleep length " <<  my_vehicle.actuator[ACTUATOR_BRAKE].status_sleep_length << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "enable_gas_status" )
	{
	  my_vehicle.actuator[ACTUATOR_GAS].status_enabled = 1;
	  infile >> my_vehicle.actuator[ACTUATOR_GAS].status_sleep_length;
	  dummy_string.str(""); 
	  dummy_string << "GAS Status enabled, sleep length " <<  my_vehicle.actuator[ACTUATOR_GAS].status_sleep_length << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "enable_trans_status" )
	{
	  my_vehicle.actuator_trans.status_enabled = 1;
	  infile >> my_vehicle.actuator_trans.status_sleep_length;
	  dummy_string.str(""); 
	  dummy_string << "TRANS Status enabled, sleep length " <<  my_vehicle.actuator_trans.status_sleep_length << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "enable_estop_status" )
	{
	  my_vehicle.actuator_estop.status_enabled = 1;
	  infile >> my_vehicle.actuator_estop.status_sleep_length;
	  dummy_string.str(""); 
	  dummy_string << "ESTOP Status enabled, sleep length " <<  my_vehicle.actuator_estop.status_sleep_length << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "enable_obdii_status" )
	{
	  my_vehicle.actuator_obdii.status_enabled = 1;
	  infile >> my_vehicle.actuator_obdii.status_sleep_length;
	  dummy_string.str(""); 
	  dummy_string << "OBDII Status enabled, sleep length " <<  my_vehicle.actuator_obdii.status_sleep_length << endl;
	  event << dummy_string.str();
	}

      // Command Threads
      else if ( cmd == "enable_steer_command" )
	{
	  my_vehicle.actuator[ACTUATOR_STEER].command_enabled = 1;
	  infile >> my_vehicle.actuator[ACTUATOR_STEER].command_timeout;
	  infile >> my_vehicle.actuator[ACTUATOR_STEER].command_sleep_length;
	  dummy_string.str(""); 
	  dummy_string << "STEER command sleep length "<< my_vehicle.actuator[ACTUATOR_STEER].command_sleep_length << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "enable_brake_command" )
	{
	  my_vehicle.actuator[ACTUATOR_BRAKE].command_enabled = 1;
	  infile >> my_vehicle.actuator[ACTUATOR_BRAKE].command_timeout;
	  infile >> my_vehicle.actuator[ACTUATOR_BRAKE].command_sleep_length;
	  //cout << my_vehicle.actuator[ACTUATOR_BRAKE].command_sleep_length << "was read for brake" << endl;
	  dummy_string.str(""); 
	  dummy_string << "BRAKE command sleep length "<< my_vehicle.actuator[ACTUATOR_BRAKE].command_sleep_length << " timeout " << my_vehicle.actuator[ACTUATOR_BRAKE].command_timeout << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "enable_gas_command" )
	{
	  my_vehicle.actuator[ACTUATOR_GAS].command_enabled = 1;
	  infile >> my_vehicle.actuator[ACTUATOR_GAS].command_timeout;
	  infile >> my_vehicle.actuator[ACTUATOR_GAS].command_sleep_length;
	  dummy_string.str(""); 
	  dummy_string << "GAS command sleep length "<< my_vehicle.actuator[ACTUATOR_GAS].command_sleep_length << " timeout " << my_vehicle.actuator[ACTUATOR_GAS].command_timeout << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "enable_trans_command" )
	{
	  my_vehicle.actuator_trans.command_enabled = 1;
	  infile >> my_vehicle.actuator_trans.command_timeout;
	  infile >> my_vehicle.actuator_trans.command_sleep_length;
	  dummy_string.str(""); 
	  dummy_string << "TRANS command sleep length "<< my_vehicle.actuator_trans.command_sleep_length << " timeout " << my_vehicle.actuator_trans.command_timeout << endl;
	  event << dummy_string.str();
	}
      else if ( cmd == "enable_estop_command" )
      	{
	  my_vehicle.actuator_estop.command_enabled = 1;
	  infile >> my_vehicle.actuator_estop.command_timeout;
	  infile >> my_vehicle.actuator_estop.command_sleep_length;
	  dummy_string.str(""); 
	  dummy_string << "ESTOP command sleep length "<< my_vehicle.actuator_estop.command_sleep_length << " timeout " << my_vehicle.actuator_estop.command_timeout << endl;
	  event << dummy_string.str();
	}


      // Check for Sparrow and Skynet interface enabled
      else if ( cmd == "enable_sparrow" )
	{
	  //The default is false
	  run_sparrow_thread = true;
	  event << "SPARROW ENABLED" ;
	}	  
      else if ( cmd == "enable_skynet" )
	{
	  //The default is false
	  run_skynet_thread = true;
	  event << "SKYNET ENABLED";
	}	  
      else if ( cmd == "enable_interlocks" )
	{
	  //The default is false
	  my_vehicle.protective_interlocks = true;
	  event << "INTERLOCKS ENAVLED";
	}	  
      else if ( cmd == "enable_automatic_timber_logging" )
	{
	  //The default is false
	  my_vehicle.automatic_timber_logging = true;
	  event << "AUTOMATIC TIMBER STARTING";
	}	  
      else if ( cmd == "enable_logging" )
	{
	  //The default is false
	  run_logging_thread = true;
	  event.enable();
	  event << "ADRIVE LOGGING ENABLED";
	  
	}	  
      else if ( cmd == "enable_ignition" )
	{
	  //The default is false
	  my_vehicle.enable_ignition = true;
	  event << "IGNITION Conrol ENABLED";
	}	  
      else if ( cmd == "enable_supervisory_thread" )
	{
	  // Fill in the delay for the supervisory thread.  
	  infile >> my_vehicle.supervisory_delay;
	  //The default is false
	  run_supervisory_thread = true;
	  dummy_string.str(""); 
	  dummy_string << "SUPERVISORY THREAD ENABLED delay: " << my_vehicle.supervisory_delay << endl;
	  event << dummy_string.str();
	}	  
      else if ( cmd == "enable_superCon" )
	{
	  // Fill in the timeout for the superCon updates  
	  infile >> my_vehicle.superCon_timeout;
	  //The default is false
	  my_vehicle.run_superCon = true;
	  // Put us into pause for superCon until we recieve messages
	  my_vehicle.actuator_estop.command = EPAUSE;
	  estop_log << "SUERCON PAUSE at startup";
	  dummy_string.str(""); 
	  dummy_string << "SUPERCON ENABLED timeout:" <<  my_vehicle.superCon_timeout << endl;
	  event << dummy_string.str();
	}	  
      else if ( cmd == "enable_simulation" )
	{
	  //The default is false
	  simulation_flag = true;
	  infile >> simulator_IP;
	  dummy_string.str(""); 
	  dummy_string << "SIMULATOR ENABLED simulator_IP:" <<  simulator_IP << endl;
	  event << dummy_string.str();
	}	  
      
      else if ( cmd == "obdii_speed_priority" )
	{
	  //The default is false
	  infile >> my_vehicle.actuator_obdii.speed_priority;
	  dummy_string.str(""); 
	  dummy_string << "OBDII speed_priority set to:" << my_vehicle.actuator_obdii.speed_priority << endl;
	  event << dummy_string.str();
	}
    }  
  

}


/******************* SPARROW KEY BINDING FUNCTIONS ****************/

/* Sparrow requires the bound key to return int and take a long
 * these are wrapper functions to allow the pthread_cond_broadcastto
 * activate the command threads.  */

// Callback functions from sparrow to start command threads.  
/*! The gas function call for sparrow bind key to execute a gas command. */
int flag_gas_condition(long x)
{
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );
  return 0;
}
/*! The brake function call for sparrow bind key to execute a brake command. */
int flag_brake_condition(long x)
{
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
  return 0;
}
/*! The steer function call for sparrow bind key to execute a steer command. */
int flag_steer_condition(long x)
{
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_STEER].cond_flag );
  return 0;
}
/*! The trans function call for sparrow bind key to execute a steer command. */
int flag_trans_condition(long x)
{
  pthread_cond_broadcast( & my_vehicle.actuator_trans.cond_flag );
  return 0;
}
/*! The steer function call for sparrow bind key to set steer velocity. */
int flag_steer_velocity(long x)
{
  execute_steer_velocity(my_vehicle.actuator[ACTUATOR_STEER].velocity);
  return 0;
}
/*! The steer function call for sparrow bind key to set steer acceleration. */
int flag_steer_acceleration(long x)
{
  execute_steer_acceleration(my_vehicle.actuator[ACTUATOR_STEER].acceleration);
  return 0;
}


/*! This function is for the sparrow binding to reenable the steering motor
 * after being manually diabled. */
int flag_steer_enable(long x)
{
  steer_enable_overide();
  return 0;
}

/******************** updateEstopPosition ************************/
/*
 * A function to update the overall position of the estop based 
 * on each individual input.  */


int updateEstopPosition(vehicle_t * pVehicle)
{
  if (pVehicle->actuator_estop.astop == DISABLE ||
      pVehicle->actuator_estop.command == DISABLE ||
      pVehicle->actuator_estop.dstop == DISABLE)
    {
      //  cout << "SETTING DISABLE" << endl;
      pVehicle->actuator_estop.position = DISABLE;
      set_disable();
    }

  else if
    (pVehicle->actuator_estop.astop == EPAUSE ||
     pVehicle->actuator_estop.command == EPAUSE ||
     pVehicle->actuator_estop.dstop == EPAUSE)
    {
      //cout << "SETTING EPAUSE" << endl;
      pVehicle->actuator_estop.position = EPAUSE;
      set_pause();
    }
  
  else 
    {
      // Renable the steering if returning to run 
      if (pVehicle->actuator_estop.position != RUN)
	{
	  
	  if (pVehicle->actuator[ACTUATOR_STEER].command_enabled == 1)
	    {
	      pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_STEER].mutex);
	      steer_enable();
	      int steer_enabled_check_dummy = steer_enabled_check();
	      pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_STEER].mutex);
	      if (steer_enabled_check_dummy != true)
		{
		  event << "Returning to run, failed to enable steering repausing";
		  estop_log << "ADRIVE PAUSE, failed to enable steering when reenabling";
		  execute_adrive_pause();
		  return -1;
		}
	    }
	  else 
	    {
	      // Steering command not enabled, therefore not enabling
	    }
	    
	  //cout << "SETTIGN RUN" << endl;
	  pVehicle->actuator_estop.position = RUN;
	}
    }
  return 1;
}
  


/******************** LOGGING THREAD *******************************/
/* This is depricated by the timber aspect of the broadcast thread
 * but I am leaving it in just in case something specific should be
 * logged for debugging purposes.  */

/*! The function that is called as the logging thread of adrive if enabled.  
 * This function periodically records the actuators commanded and 
 * actual positions to file. */

void* logging_main (void* arg){
  char testlog[255];
  char datestamp[255];
  unsigned long long current_time;
  fstream testlogfile;
  time_t t = time(NULL);
  tm *local;
  

  local = localtime(&t);
  
  //sprintf(testlog,"adrive_log.dat");
  sprintf(datestamp, "%02d%02d%04d_%02d%02d%02d",
	  local->tm_mon+1, local->tm_mday, local->tm_year+1900,
	  local->tm_hour, local->tm_min, local->tm_sec);
 
  sprintf(testlog, "logs/adrive_%s.log", datestamp);
    
  while (run_logging_thread)
    {
      //      printf("Running Logging thread");
      // Fed up with DGCutils
      //	timeval tv;
      //	gettimeofday(&tv, NULL);
      //	current_time = (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;      
      DGCgettime(current_time);
      testlogfile.open(testlog, fstream::out|fstream::app);
      testlogfile.precision(10);
      testlogfile <<  current_time << '\t';
      testlogfile <<  my_vehicle.actuator[ACTUATOR_STEER].command << '\t' << my_vehicle.actuator[ACTUATOR_STEER].position << '\t';
      testlogfile <<  my_vehicle.actuator[ACTUATOR_BRAKE].command << '\t' << my_vehicle.actuator[ACTUATOR_BRAKE].position << '\t' << my_vehicle.actuator[ACTUATOR_BRAKE].pressure << "\t";
      testlogfile <<  my_vehicle.actuator[ACTUATOR_GAS].command   << '\t' << my_vehicle.actuator[ACTUATOR_GAS].position   << "\r\n" ;
      testlogfile.close();
      //DGCgettime(current_time);
      //cout << current_time << "Before the logging sleep\n";
      usleep(LOGGING_DELAY);
      //DGCgettime(current_time);
      //cout << current_time << "Before the logging sleep\n";
    }
  return 0;
}



/************************* SPARROW THREAD *************************/

/*! This function runs as the sparrow thread of adrive. When sparrow is 
 * enabled this thread waits for the steering to calibrate then will take over
 * the screen with the UI.  
 *
 **** Caution should be exercized when using this interface there are
 * no protections against bad values and all fields are editable.  
 * */
void* sparrow_main (void* arg)
{
  //printf("Sparrow main is running\n");
  event << "SPARROW THREAD Starting";
  pthread_mutex_lock(& my_vehicle.actuator[ACTUATOR_STEER].mutex);
  
  //printf("command %d, status %d\n", my_vehicle.actuator[ACTUATOR_STEER].command_enabled, my_vehicle.actuator[ACTUATOR_STEER].status_enabled);

  // This checks whether the steering is going to be started up.  The calibration procedure needs user inputs
  // at the prompt and the sparrow display needs to wait until the calibration is finshed.  
  if( my_vehicle.actuator[ACTUATOR_STEER].command_enabled || my_vehicle.actuator[ACTUATOR_STEER].status_enabled)
    {
      //    printf("the steering is enabled I will wait\n");
      pthread_cond_wait(&steer_calibrated_flag, &my_vehicle.actuator[ACTUATOR_STEER].mutex);
      // printf("sparrow has been awakened\n");
    }
  pthread_mutex_unlock(& my_vehicle.actuator[ACTUATOR_STEER].mutex);

  /* Initialize the screen and run the main display loop */
  if (dd_open() < 0) { dbg_error("can't open display"); exit(-1); }
  if (dd_usetbl(addisp) < 0) { dbg_error("can't open display table"); exit(-1); }
  //*! Pressing g executes the gas command */
  dd_bindkey( 'g' , flag_gas_condition   ); 
  //*! Pressing b executes the brake command */
  dd_bindkey( 'b' , flag_brake_condition );
  //*! Pressing s executes the steer command */
  dd_bindkey( 's' , flag_steer_condition );
  //*! Pressing e reenables steering */
  dd_bindkey( 'e' , flag_steer_enable );
  //*! Pressing c sets steering acceleration */
  dd_bindkey( 'c' , flag_steer_acceleration );
  //*! Pressing v sets steering velocity*/
  dd_bindkey( 'v' , flag_steer_velocity    );
  //*! Pressing t execute trans command*/
  dd_bindkey( 't' , flag_trans_condition    );
  dd_loop();
  dd_close();

  sleep(3);
  exit(0);
}



/********************** STATUS THREAD *************************/

/*! This is the generic thread function for all actuator status loops.  
 * Each different actuator status thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_status_thread_function(void* arg)
{
  actuator_t *act = (actuator_t*) arg;
  stringstream dummy_string;
  dummy_string << act->name << " STATUS Starting";
  event << dummy_string.str();

  printf("Starting Thread: %s Status\n", act->name);

  pthread_once(&(act->start_bit), act->execute_init);

  while( !my_vehicle.shutdown && act->status_enabled) {  
    // Begin timing
    act->act_status_loop_delay->begin();
    dummy_string.str("");
    dummy_string << act->name << " STATUS Executing";
    event << dummy_string.str();
    /* This if statement is a hack for currently the estop status call will
     * block until a change in state. Adrive relies on quick resonses even if they
     * do nothing or are redundant.  */
    // Lock the mutex
    pthread_mutex_lock(&act->mutex);
    // Execute the function
    if ( act->execute_status() == ERROR) 
      {
	dummy_string.str("");
	dummy_string << act->name << " STATUS FAILED";
	event << dummy_string.str();
	//printf("%s Status: SOMETHING BAD HAPPENED\n", act->name);
      }
    else {
      act->status_loop_counter++;
      //printf("%s Status: Que Bueno! %d\n", act->name, act->status_loop_counter);
    }

    // Unlock the mutex
    pthread_mutex_unlock(&act->mutex);
    
    // Sleep before repeating
    usleep(act->status_sleep_length);

    // End timing
    act->act_status_loop_delay->end();
   }

   // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
   /* Record the delays before shutting down */
  act->act_status_loop_delay->report();
  return NULL;
 }



 /*********************** COMMAND THREAD ************************/

 /*! This is the generic thread function for all actuator command loops.  
  * Each different actuator command thread it dynamically created from this
  * template if it is enabled when adrive is started. */
 void* actuator_command_thread_function(void* arg)
 {
   actuator_t *act = (actuator_t*) arg;
   struct timespec ts;
   struct timeval tp;
   int retval;
   stringstream dummy_string;
   dummy_string << act->name << " COMMAND Starting";
   event << dummy_string.str();

   pthread_once(&(act->start_bit), act->execute_init);

   while( !my_vehicle.shutdown && act->command_enabled) {  
     dummy_string.str("");
     dummy_string << act->name << " COMMAND Executing";
     event << dummy_string.str();

     // Lock the mutex
     pthread_mutex_lock(&act->mutex);

     gettimeofday(&tp, NULL);
     ts.tv_sec = tp.tv_sec;
     ts.tv_nsec = tp.tv_usec * 1000;
     ts.tv_sec += act->command_timeout;

     /* Wait for a pthread broadcast flag or timeout */
     retval = pthread_cond_timedwait(&act->cond_flag, &act->mutex, &ts);

     // End the preexecution timer
     //cout<<"ending preexecution timer"<<endl;
     act->act_preexecution_delay->end();
     //Start the execution timer
     //     cout <<"Starting the execution timer" << endl;
     act->act_execution_delay->begin();
     

     /* If timed out, respond */
     if (ETIMEDOUT == retval)
       {
	 /* Respond with a pause if interlocks enabled */
	 if (my_vehicle.protective_interlocks == true)
	   {
	     dummy_string.str("");
	     dummy_string << act->name  << "COMMAND: Timed out w/ interlocks enabled going to adrive pause"<< endl;
	     event << dummy_string.str();
	     estop_log << dummy_string.str();
	     execute_adrive_pause(); 
	   }
	 /* Don't respond if interlocks not enabled */
	 else
	   {
	     dummy_string.str("");
	     dummy_string << act->name  << "COMMAND: Timed out w/o interlocks enabled not doing anything about it "<< endl;
	     event << dummy_string.str();
	   }
       }
     /* If the command failed, record it and report it */
     if ( act->execute_command(act->command) == ERROR)
       {
	 dummy_string.str("");
	 dummy_string << act->name << " COMMAND FAILED";
	 event << dummy_string.str();
      }

     /* Unlock the mutex */
     pthread_mutex_unlock(&act->mutex);   

     /* Sleep to prevent overloading the serial interface */
     usleep(act->command_sleep_length);
     //cout << "Ending the execution delay" << endl;
     // Close out the execution timer
     act->act_execution_delay->end();
   }
   
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.

   /* Record the delays before shutting down */
   act->act_preexecution_delay->report();
   act->act_execution_delay->report();

   return NULL;
 }

/***************************** OBDII THREAD ********************************/

/*! This is the OBDII copy of the generic thread function for all actuator status loops.  
 * Each different actuator status thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_obdii_status_thread_function(void* arg)
{
  obdii_t *act = (obdii_t*) arg;

  printf("Starting Thread:OBDII Status\n");

  pthread_once(&(act->start_bit), act->execute_init);

  while( !my_vehicle.shutdown && act->status_enabled) {  
    // Lock the mutex
    event << "OBDII STATUS executing"; 
    pthread_mutex_lock(&act->mutex);

    // Execute the function
    if ( act->execute_status() == -1) 
      {
	my_vehicle.actuator_obdii.status = 0;
	event << "OBDII STATUS FAILED";
	usleep(OBDII_ERROR_TIMEOUT);
      }
    else 
      {
	act->status_loop_counter++;
	//Commenting out since it returns positive 
	/* Specifically  OBDII read returns positive even if there is on actuator
	 * so this is not a good measure of an error */
	//      my_vehicle.actuator_obdii.status = 1;
	
      }

    // Unlock the mutex
    pthread_mutex_unlock(&act->mutex);
  
    // Sleep before repeating
    usleep(act->status_sleep_length);
  }
  
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
  return NULL;
}

/***************************** ESTOP THREAD ********************************/

/*! This is the ESTOP copy of the generic thread function for all actuator status loops.  
 * Each different actuator status thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_estop_status_thread_function(void* arg)
{
  estop_t *act = (estop_t*) arg;
  
  event << "ESTOP STATUS Thread Starting";
  pthread_once(&(act->start_bit), act->execute_init);
  
  while( !my_vehicle.shutdown && act->status_enabled) {  
    event << "ESTOP STATUS Executing";
    // Lock the mutex
    pthread_mutex_lock(&act->mutex);
    
    // Execute the function
    if ( act->execute_status() == -1) 
      {
	event << "ESTOP STATUS FAILED";
	usleep(act->status_sleep_length);
      }
    else 
      {
	act->status_loop_counter++;
      }

    // Unlock the mutex
    pthread_mutex_unlock(&act->mutex);
    
    //Update the estop position given the new position
    updateEstopPosition(&my_vehicle);

    
    // Sleep before repeating
#ifdef ESTOP_SDS
    /* This sleep is only used for the ESTOP_SDS because the serial port blocks 
     * whereas the SDS will return the latest message.  */
    
    //    cout << "Sleeping for SDS" << endl;
    usleep(act->status_sleep_length);
#endif //ESTOP_SDS
  }
  
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
  return NULL;
}


/********************** TRANS STATUS THREAD *************************/

/*! This is the generic thread function for all actuator status loops.  
 * Each different actuator status thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_trans_status_thread_function(void* arg)
{
  trans_t *act = (trans_t*) arg;

  printf("Starting Thread: %s Status\n should be trans", act->name);
  event << "TRANS STATUS starting";
  pthread_once(&(act->start_bit), act->execute_init);

  while( !my_vehicle.shutdown && act->status_enabled) {  
    event << "TRANS STATUS executing";
    /* This if statement is a hack for currently the estop status call will
     * block until a change in state. Adrive relies on quick resonses even if they
     * do nothing or are redundant.  */
	// Lock the mutex
	pthread_mutex_lock(&act->mutex);
    // Execute the function
    if ( act->execute_status() == ERROR) 
      {
	event << "TRANS STATUS FAILED";
      }
    else {
      act->status_loop_counter++;
    }
    
    // Unlock the mutex
    pthread_mutex_unlock(&act->mutex);
    
    // Sleep before repeating
    usleep(act->status_sleep_length);
  }
  
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
  return NULL;
}



/*********************** TRANS COMMAND THREAD ************************/

/*! This is the generic thread function for all actuator command loops.  
 * Each different actuator command thread it dynamically created from this
 * template if it is enabled when adrive is started. */
void* actuator_trans_command_thread_function(void* arg)
{
  trans_t *act = (trans_t*) arg;
  struct timespec ts;
  struct timeval tp;
  event << "TRANS COMMAND Starting";
  printf("Starting Thread: %s Command\n should be trans", act->name);

  pthread_once(&(act->start_bit), act->execute_init);
  cout << "TRANS returned from init" << endl;
  while( !my_vehicle.shutdown && act->command_enabled) {  
    event << "TRANS COMMAND executing";
    // Lock the mutex
    pthread_mutex_lock(&act->mutex);

    gettimeofday(&tp, NULL);
    ts.tv_sec = tp.tv_sec;
    ts.tv_nsec = tp.tv_usec * 1000;
    ts.tv_sec += act->command_sleep_length;
    
    if (ETIMEDOUT == pthread_cond_timedwait(&act->cond_flag, &act->mutex, &ts))
      {
	/* For the trans don't error, just continue as usual.  It does not expect regular commands
	 * it will just time out and make sure it's doing what it was commanded to do. */
	//  execute_adrive_pause();		    
      }

    if ( act->execute_command(act->command) == ERROR) 
      {
	event << "TRANS COMMAND FAILED";
      }
    
    // Unlock the mutex
    pthread_mutex_unlock(&act->mutex);   
    
    /* Sleep to prevent overloading the serial interface */
    usleep(act->command_sleep_length);
  }
  
  // SHUTDOWN CODE GOES HERE INCLUDING SYNCHRONIZATION WITH STATUS THREAD.
  return NULL;
}



/**************************SUPERVISORY_THREAD *************************/



void * supervisory_thread (void * arg)
{
  // The return value that will keep track of the number of errors.
  int error_counter;

  //Timer variables for engine restarting
  unsigned long long the_current_time;
  static unsigned long long end_sleep_time;
  static int timeout_enabled;

  
  // Counters to make sure that the status loops are still incrementing
  int last_estop_count = my_vehicle.actuator_estop.status_loop_counter;
  int last_steer_count = my_vehicle.actuator[ACTUATOR_STEER].status_loop_counter;
  int last_trans_count = my_vehicle.actuator_trans.status_loop_counter;
  int last_brake_count = my_vehicle.actuator[ACTUATOR_BRAKE].status_loop_counter;
  int last_gas_count = my_vehicle.actuator[ACTUATOR_GAS].status_loop_counter;

  /* Record that supervisory thread started */
  event << "SUPERVISORY THREAD: Started";


  //wait for steering calibration so as not to 
  // This checks whether the steering is going to be started up.  The calibration procedure needs user inputs
  // at the prompt and the sparrow display needs to wait until the calibration is finshed.  
  if( my_vehicle.actuator[ACTUATOR_STEER].command_enabled || my_vehicle.actuator[ACTUATOR_STEER].status_enabled)
    {
      //    printf("the steering is enabled I will wait\n");
      pthread_cond_wait(&steer_calibrated_flag, &my_vehicle.actuator[ACTUATOR_STEER].mutex);
      // printf("sparrow has been awakened\n");
    }
  pthread_mutex_unlock(& my_vehicle.actuator[ACTUATOR_STEER].mutex);
  
  
  // Give one cycle before starting to poll
  usleep(my_vehicle.supervisory_delay);

  // Semi infinite loop.
  while (!my_vehicle.shutdown)
    {
      event << "SUPERVISORY THREAD starting loop";

      // Zero the error counter
      error_counter = 0;
      // Check that estop is still recieving commands
      if (my_vehicle.actuator_estop.status_enabled)
	{
	  if (last_estop_count == my_vehicle.actuator_estop.status_loop_counter)
	    {
	      error_counter++;

	      my_vehicle.actuator_estop.status = 0;
	      execute_adrive_pause();
	      estop_log << "ESTOP stopped updating adrive pause";
	      cerr << "ESTOP didn't update, going to pause" << endl;
	      my_vehicle.actuator_estop.status = 0;
	      /*pthread_mutex_lock(&my_vehicle.actuator_estop.mutex);
	       *execute_estop_close();
	       *execute_estop_init();
	       *pthread_mutex_unlock(&my_vehicle.actuator_estop.mutex);*/
	      event << "SUPERVISORY THREAD ERROR failed estop test, restarting estop";
	    }
	  else
	    {
	      last_estop_count = my_vehicle.actuator_estop.status_loop_counter;
	      event << "SUPERVISORY THREAD passed estop test";
	    }
	}
      
      // Make sure that the engine is running properly
      if (my_vehicle.enable_ignition)
	{      
	  if (my_vehicle.actuator_trans.command_enabled)
	    {
	      if (my_vehicle.actuator_estop.position != DISABLE)
		{
		  int dummy_engine_condition = engine_condition();

		  if (dummy_engine_condition == ENGINE_OFF)
		    {
		      event << "SUPERVISORY THREAD Engine is off, Setting Engine to RUN";
		      error_counter++;
		      execute_adrive_pause();
		      estop_log << "ADRIVE PAUSE  ENGINE OFF";
		      my_vehicle.actuator_trans.ignition_command = I_RUN;
		      pthread_cond_broadcast( & my_vehicle.actuator_trans.cond_flag );
		    }
		  else if (dummy_engine_condition == ENGINE_STOPPED)
		    {
		      event << "SUPERVISORY THREAD Engine Stopped: Starting Engine";
		      error_counter++;
		      execute_engine_startup();
		    }
		  else if (dummy_engine_condition == ENGINE_UNKNOWN)
		    {
		      error_counter++;
		      event << "SUPERVISORY THREAD Engine Unknown:  Will periodically try to start";

		      // Test whether the right amount of time has passed.  
		      if (!timeout_enabled)
			{
			  timeout_enabled = true;
			  DGCgettime(end_sleep_time);
			  end_sleep_time += UNKNOWN_ENGINE_CONDITION_RESTART_TIMEOUT * 1000000;
			}
		      else
			{
			  DGCgettime(the_current_time);
			  
			  if (the_current_time > end_sleep_time)
			    {
			      // Reset the timout
			      timeout_enabled = false;
			      event << "SUPERVISORY THREAD Engine Unknown finished waiting: Starting Engine";
			      execute_engine_startup();
			    }
			  
			
			  else
			    {
			      event << "SUPERVISORY THREAD Engine Unknown waiting to start";
			    }
			}

		    }
		  else if (dummy_engine_condition == ENGINE_RUNNING)
		    {
		      event << "SUPERVISORY THREAD Engine is Running and good";
		      //Alls good the engine is running
		    }
		  else
		    {
		      error_counter++;
		      execute_adrive_pause();
		      estop_log << "ADRIVE PAUSE ENGINE CONDITION NOT A VALID POSTION ";
		      event << "SUPERVISORY THREAD ERROR Engine Condition INCORRECT PARAMETER";
		    }  
		}// End of if estop != DISABLE
	    }
	}

      // Check that steer is still recieving commands and is enabled if in run
      if (my_vehicle.actuator[ACTUATOR_STEER].status_enabled)
	{
	  // If we're in run, make sure that the steering is enabled
	  if (my_vehicle.actuator_estop.position == RUN) 
	    {
	      pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_STEER].mutex);
	      int steer_enabled_check_dummy = steer_enabled_check();
	      pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_STEER].mutex);
	      if (steer_enabled_check_dummy != TRUE)
		{
		  event << "STEER NOT ENABLED WHILE IN RUN - PAUSING" ;
		  estop_log << "ADRIVE PAUSE   STEER NOT ENABLED WHILE IN RUN - PAUSING" ;
		  execute_adrive_pause();
		  error_counter++;
		}
	     	    }

	  if (last_steer_count == my_vehicle.actuator[ACTUATOR_STEER].status_loop_counter)
	    {
	      error_counter++;
	      my_vehicle.actuator[ACTUATOR_STEER].status = 0;
	      execute_adrive_pause();
	      estop_log << "ADRIVE PAUSE STEER didn't update, going to pause";
	      event << "SUPERVISORY THREAD ERROR STEER didn't update, restarting";
	      my_vehicle.actuator[ACTUATOR_STEER].status = 0;
	      //pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_STEER].mutex);
	      //execute_steer_close();
	      //execute_steer_init();
	      //pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_STEER].mutex);
	      
	    }
	  else
	    {
	      last_steer_count = my_vehicle.actuator[ACTUATOR_STEER].status_loop_counter;
	      
	      event << "SUPERVISORY THREAD STEER passed test";
	      //   cout << "passed STEER counter test" << endl;
	    }
	}
      // Check that brake is still recieving commands
      if (my_vehicle.actuator[ACTUATOR_BRAKE].status_enabled)
	{
	  if (last_brake_count == my_vehicle.actuator[ACTUATOR_BRAKE].status_loop_counter)
	    {
	      error_counter++;
	      my_vehicle.actuator[ACTUATOR_BRAKE].status = 0;
	      execute_adrive_pause();
	      event << "SUPERVISORY THREAD ERROR BRAKE didn't update, restarting";
	      estop_log << "ADRIVE PAUSE BRAKE didn't update, going to pause";

	      my_vehicle.actuator[ACTUATOR_BRAKE].status = 0;

	      // break the status loop free it it's not returning
	      //pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex);
	      //pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex);
	      //  cout << "I'm restarting the brake" << endl;
	      //execute_brake_close();
	      //execute_brake_init();
	      //cout << "Brake Init returned" << endl;
	      //pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_BRAKE].mutex);
	    }
	  else
	    {
	      last_brake_count = my_vehicle.actuator[ACTUATOR_BRAKE].status_loop_counter;

	      event << "SUPERVISORY THREAD BRAKE passed test";
	    }
	}
      // Check that gas is still recieving commands
      if (my_vehicle.actuator[ACTUATOR_GAS].status_enabled)
	{
	  if (last_gas_count == my_vehicle.actuator[ACTUATOR_GAS].status_loop_counter)
	    {
	      error_counter++;
	      my_vehicle.actuator[ACTUATOR_GAS].status = 0;
	      execute_adrive_pause();	      
	      event << "SUPERVISORY THREAD ERROR GAS didn't update, restarting";
	      estop_log << "ADRIVE PAUSE GAS didn't update, going to pause" ;


	      my_vehicle.actuator[ACTUATOR_GAS].status = 0;
	      // Breaking out of status loop 
	      // problem if it's already trying to init??
	      // pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_GAS].mutex);
	      //pthread_mutex_lock(&my_vehicle.actuator[ACTUATOR_GAS].mutex);
	      //execute_gas_close();
	      //execute_gas_init();
	      //pthread_mutex_unlock(&my_vehicle.actuator[ACTUATOR_GAS].mutex);
	    }
	  else
	    {
	      last_gas_count = my_vehicle.actuator[ACTUATOR_GAS].status_loop_counter;
	      event << "SUPERVISORY THREAD GAS passed test";
	    }
	}
      // Check that trans is still recieving commands
      if (my_vehicle.actuator_trans.status_enabled)
	{
	  if (last_trans_count == my_vehicle.actuator_trans.status_loop_counter)
	    {
	      error_counter++;
	      my_vehicle.actuator_trans.status = 0;
	      execute_adrive_pause();
	      estop_log << "ADRIVE PAUSE TRANS didn't update, going to pause";

	      event << "SUPERVISORY THREAD ERROR TRANS didn't update, restarting";
	      my_vehicle.actuator_trans.status = 0;
	      //pthread_mutex_lock(&my_vehicle.actuator_trans.mutex);
	      //execute_trans_close();
	      //execute_trans_init();
	      //pthread_mutex_unlock(&my_vehicle.actuator_trans.mutex);
	    }
	  else
	    {
	      last_trans_count = my_vehicle.actuator_trans.status_loop_counter;
	      event << "SUPERVISORY THREAD TRANS passed test";
	    }
	}
      // Check that the obdii is still valid
      if (my_vehicle.actuator_obdii.status_enabled == ON)
	{
	  if (my_vehicle.actuator_obdii.status != 1)
	    {
	      error_counter++;
	      my_vehicle.actuator_obdii.status = 0;
	      //OBDII is not valid
	      event << "SUPERVISORY THREAD ERROR OBDII didn't update, restarting";

	      //pthread_mutex_lock(&my_vehicle.actuator_obdii.mutex);
	      //execute_obdii_close();
	      //execute_obdii_init();
	      //pthread_mutex_unlock(&my_vehicle.actuator_obdii.mutex);
	      
	    }
	}
      
      
      /* Future imlimentations 
       * Check all actuators for tracking errors
       * 
       * make sure that commands are still coming in before reenabling
       */
      
      
      // If we have no errors reset astop to run.  
      if (error_counter == 0)
	{
	  my_vehicle.gui_status = 0; 
	  event << "SUPERVISORY THREAD loop finished no errors, going to run";
	  estop_log << "ADRIVE RUN SUPERVISORY THREAD loop finished no errors, going to run";
	  remove_adrive_pause();
	}
      else
	{
	  event << "SUPERVISORY THREAD had errors sleeping then retrying";
	  my_vehicle.gui_status = 10;
	}
      
      my_vehicle.super_count++;
      usleep(my_vehicle.supervisory_delay);
      // End of inner loop.
    }

  // End of supervisory thread.  
  return NULL;
}



int remove_adrive_pause()
{
  my_vehicle.actuator_estop.astop = RUN;
  return 1;
}


int execute_adrive_pause()
{
  my_vehicle.actuator_estop.astop = EPAUSE;
  set_pause();
  
  return 1;  
}

/* A function to determine how hard to brake based on the vehicle speed */
double pause_speed()
{
  if (my_vehicle.actuator_obdii.status != 1)
    return BRAKE_PAUSE_POSITION;
  else 
    {
      // Normalized velocity from 25 m/s to a .75 scale and then subtract from 1
      // Goal: 2.5 braking at 25 m/s, full brake at zero velocity.  
      return 1 - fabs((my_vehicle.actuator_obdii.VehicleWheelSpeed / 25)) * .75;
    }
}


int set_pause (void )
{
  my_vehicle.actuator[ACTUATOR_GAS].command = 0;
  my_vehicle.actuator[ACTUATOR_BRAKE].command = pause_speed();
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
  return 1;
}

int set_disable (void)
{
  my_vehicle.actuator[ACTUATOR_GAS].command = 0;
  my_vehicle.actuator[ACTUATOR_BRAKE].command = 1;
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );
  pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
  sleep(DISABLE_TIME_UNTIL_ENGINE_OFF);
  my_vehicle.actuator_trans.ignition_command = I_OFF;
  pthread_cond_broadcast( & my_vehicle.actuator_trans.cond_flag );
  return 1;
}
