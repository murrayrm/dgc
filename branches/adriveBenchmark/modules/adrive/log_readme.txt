When logging is enabled in adrive.config the file adrive_(date/time stamp).log created. 

The fields are as follows in the log file.  They are all tab delimited.

unsigned long long time //Current time from DGCgettime
(-1 1)	 Steering command
(-1 1)	 Steering position
(0 1)	 Brake command
(0 1)	 Brake position
(0 1)	 Brake pressure
(0 1)	 gas command
(0 1)	 gas positin


