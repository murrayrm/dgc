function makeZigzag()

#this file creates a static trajectory in the shape of a zigzag
#at velocity specified by v, at angles specified by theta.
#to change the length of the segments, change what dist goes to,
#and to change the spacing of points, change the incrementing of dist.

#This function will mainly be useful for trajFollower to have trajectories
# that are specified exactly, with no ff around the step turn, and constant
# velocity (as opposed to planner and pathgen paths)

     v = 2;
     oldn = 3831723;
     olde = 449273;

     N = [];
     E = [];
     Ndot = [];
     Edot = [];
     Ndotdot = [];
     Edotdot = [];

     count = 0;

     for theta=10:10:120 
 
          sn = sin(0);
          cs = cos(0);

          for dist = 0:.1:30

               count = count + 1;
               
               N = [N;dist*sn+oldn];
               E = [E;dist*cs+olde];
               Ndot = [Ndot; v*sn];
               Edot = [Edot; v*cs];
               Ndotdot = [Ndotdot; 0];
               Edotdot = [Edotdot; 0];

          endfor
	  
          oldn = N(count);
          olde = E(count);

# and again for turns the other way...

          sn = sin(theta*pi/180);
          cs = cos(theta*pi/180);

          for dist = 0:.1:30

               count = count + 1;
               
               N = [N;dist*sn+oldn];
               E = [E;dist*cs+olde];
               Ndot = [Ndot; v*sn];
               Edot = [Edot; v*cs];
               Ndotdot = [Ndotdot; 0];
               Edotdot = [Edotdot; 0];

          endfor
	  
          oldn = N(count);
          olde = E(count);
     endfor

     traj = [N,Ndot,Ndotdot,E,Edot,Edotdot];

plot(N,E);

     save -text zigzag2.traj traj
