package edu.caltech.me75;
import java.io.*;
import java.util.Hashtable;
import java.util.regex.*;

/**
 * A map class
 * @author Xiang Jerry He
 */


/* 	currently cannot handle multi-line comments */
public class Map
{
	/** number of segments */
	int num_segments;
	/** number of zones */
	int num_zones;
	/** collection of all segments in this map */
	protected Segment[] segs;
	/** collection of all zones in this map */
	protected Zone[] zones;
	/** miscellaneous information */
	protected Hashtable<String, String> info;

	
	Map() {
		info = new Hashtable<String, String>();
	}
	

	/**
	 * Read in and parse a map.  
	 * 
	 * @param in the buffered reader for the RNDF
	 * @throws IOException if we fail to read in the file
	 * @throws MapFormatException if the file is incorrectly formatted
	 */
	void readMap(BufferedReader in) throws IOException, MapFormatException {
		String line = in.readLine();
		String[] tokens;
		while(!line.startsWith("segment")) {
			if(line.matches("\\w+.*")) {
				tokens = line.split("\\s");
				System.out.println(line);
				info.put(tokens[0], tokens[1]);
			}
			line = in.readLine();
		}
		num_segments = Integer.parseInt(info.get("num_segments"));
		segs = new Segment[num_segments];
		System.out.println("the number of lanes is");
		System.out.println("number of segments is "+num_segments);
		num_zones = Integer.parseInt(info.get("num_zones"));
		zones = new Zone[num_zones];
		System.out.println("number of zones is " + num_zones);
		for(int i=0; i < num_segments; i++) {
			segs[i]=new Segment();
		    (segs[i]).readSegment(in);  
		}
		/**
		 * reading in all zones
		 * @author David Waylonis
		 */
		for(int i=0; i < num_zones; i++) {
			zones[i]=new Zone();
		    (zones[i]).readZone(in);  
		}
		/** testing end of file */
		System.out.println(in.readLine());
		System.out.println(in.readLine());
		System.out.println(in.readLine());
		//assert(in.readLine().equals("end_file"));
	}
}