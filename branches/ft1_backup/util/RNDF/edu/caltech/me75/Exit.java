package edu.caltech.me75;

/**
 * Represents an exit point from a zone.  An exit consists of an exit perimeter point
 * and an entry waypoint, which are connected.
 * @author David Waylonis
 */
public class Exit {

	WayPoint exitPoint;
	WayPoint entryPoint;
	
	/**
	 * Default constructor.  Takes in an exit perimeter point and an entry waypoint.
	 * 
	 * @param exit the exit perimeter point
	 * @param entry the entry waypoint
	 */
	public Exit(WayPoint exit, WayPoint entry) {
		
		exitPoint = exit;
		entryPoint = entry;
	}
}
