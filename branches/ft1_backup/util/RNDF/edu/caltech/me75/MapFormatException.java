package edu.caltech.me75;

/**
 * an Exception class to handle ill-formated RNDF files
 * @author Xiang Jerry He
 */

public class MapFormatException extends Exception {

	public MapFormatException() {
		super();
	}

	public MapFormatException(String message) {
		super(message);
	}

	public MapFormatException(String message, Throwable cause) {
		super(message, cause);
	}

	public MapFormatException(Throwable cause) {
		super(cause);
	}

}
