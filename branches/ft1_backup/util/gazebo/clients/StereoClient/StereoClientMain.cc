#include "StereoClient.hh"
#include <getopt.h>

#define HELP     (1<<0)

using namespace std;

int main(int argc, char *argv[])
{
  // Argument handling
  int c;  
  while (1)
  {
    /* getopt_long stores the option index here. */
    int option_index = 0;
    static struct option long_options[] =
      { // name         argument?          flag  value
        {"help",        no_argument      , NULL, HELP},
        {0, 0, 0, 0}
      };

    char short_options[] = "lh";
    c = getopt_long_only (argc, argv, short_options, long_options, &option_index);
    /* Detect the end of the options. */
    if(c == -1)
      break;

    switch(c)
    {
    case 'h':
    case HELP:
      cout << endl 
      << "Usage: ./StereoClient " << endl 
      << endl
      << "There are currently no switches..." << endl;
      return 0;
    case '?': // invalid options makes us not want to start
      return 1;
    
    default:
       printf ("?? returned character code 0%o ??\n", c);
       return 1;          
    }
  } // end while(1)

  // Done parsing arguments, let's begin...
  printf("Starting StereoClient\n");
  StereoClient myClient;
  myClient.init();
  int k = 0;
  while (k < 10)
  {
    myClient.grab();
    //k++;
  }

  return 0;
}
