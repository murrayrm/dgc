/*
 *  Gazebo - Outdoor Multi-Robot Simulator
 *  Copyright (C) 2003  
 *     Nate Koenig & Andrew Howard
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/* Desc: Model for a Pioneer2AT
 * Author: Andrew Howard
 * Date: 8 May 2003
 * CVS: $Id: SickLMS200.hh,v 1.18 2004/06/01 17:12:50 natepak Exp $
 */

#ifndef SICKLMS200_HH
#define SICKLMS200_HH

#include "Body.hh"
#include "Model.hh"

// Forward declarations
class Geom;
class RayGeom;
class GeomData;

typedef struct _gz_laser_t gz_laser_t;


class SickLMS200 : public Model
{
  // Constructor, destructor
  public: SickLMS200( World *world );
  public: virtual ~SickLMS200();
  
  // Load the model
  public: virtual int Load( WorldFile *file, WorldFileNode *node );

  // Initialize the model
  public: virtual int Init( WorldFile *file, WorldFileNode *node );

  // Finalize the model
  public: virtual int Fini();

  // Update the model state
  public: virtual void Update( double step );

  // Ray-intersection callback
  private: static void UpdateCallback( void *data, dGeomID o1, dGeomID o2 );

  // Load ODE stuff
  private: int OdeLoad( WorldFile *file, WorldFileNode *node );

  // Initialize ODE
  private: int OdeInit( WorldFile *file, WorldFileNode *node );

  // Finalize ODE
  private: int OdeFini();

  // Initialize rays
  private: int RayInit();

  // Finalize rays
  private: int RayFini();
  
  // Initialize the external interface
  private: int IfaceInit();

  // Finalize the external interface
  private: int IfaceFini();

  // Update the data in the external interface
  private: void IfacePutData();

  // ODE objects
  private: Body *body;
  private: Geom *bodyGeoms[8];
  
  // Ray list
  private: int rayCount;
  private: RayGeom **rays;

  // Ray space for collision detector
  private: dSpaceID superSpaceId;
  private: dSpaceID raySpaceId;
  
  // External interfaces
  private: gz_laser_t *laser_iface;
  private: gz_fiducial_t *fiducial_iface;

  // Laser settings
  private: int rangeCount;
  private: double laserMinRange, laserMaxRange;
  private: double laserPeriod;
  private: double laserTime;
  private: double scanAngle; // M_PI or (110*M_PI/180.0) radians
};


#endif
