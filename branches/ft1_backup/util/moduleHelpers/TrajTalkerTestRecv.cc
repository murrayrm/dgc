#include "TrajTalker.h"

class CTrajTalkerTestRecv : public CTrajTalker
{
public:
	CTrajTalkerTestRecv(int sn_key)
		: CSkynetContainer(SNtrajtalkertestrecv, sn_key)
	{
		CTraj traj;

		cout << "about to listen...";
		int trajSocket = m_skynet.listen(SNtraj, SNtrajtalkertestsend);
		cout << " listening!" << endl;
		while(true)
		{
			cout << "about to receive a traj...";
			RecvTraj(trajSocket, &traj);
			cout << " received a traj!" << endl;
		}
	}
};

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;

	CTrajTalkerTestRecv test(key);
}
