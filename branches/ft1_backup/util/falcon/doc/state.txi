@c -*- texinfo -*-

@node     state,  lpv,  matrix,    Top
@comment  node-name,  next,  previous,  up
@chapter State Space Computations

@cindex state space computations

This module automates the implementation of discrete state space
controllers and difference equations.

@menu
* introduction: state/intro.          introduction to state module
* initialization: state/init.         initializing the state
* file format: state/file.            state file format
* manual creation: state/manual.      manual creation of the structure
* element access: state/el.           modifying the structure
* equation usage: state/ss.           executing the equations
* other functions: state/other.       other functions
* function prototypes: state/proto.   Function prototypes
@end menu

@node state/intro, state/init, state, state
@section Introduction

The state space computation module provides a quick method for
implementing digital controllers within sparrow.  All functions
use the fast forms from the matrix computation module.

The routines use a special strucure, called @code{STATE_SPACE}.  This
structure consists of several matrices.  The 
user should neither change any of the elements in the structure 
nor directly access them.  All necessary information can be 
obtained through function calls.

All functions begin with the prefix @code{ss_}.  The structure 
(and also function prototypes) are specified in @code{state.h}.

State space systems are loaded from either @code{MATLAB}
data files or from a formatted ASCII files.
Features exist for modifying the state space system from within sparrow.


@node state/init, state/file, state/intro, state
@section Initialization

The first step is to load the matrices that define the state space
system into memory.  This is accomplished
using @code{ss_load}, which returns a pointer to the state space system.
If the load is not successful, the program returns a @code{NULL} pointer.
The state system is removed from memory by @code{ss_free}
@cindex ss_load
@cindex ss_free


@node state/file, state/manual, state/init, state
@comment  node-name,  next,  previous,  up
@section The File Format

State space system files are special forms of the matrix files.
The function @code{ss_load} loads a state space system from
a matrix file, either ASCII or binary.
The file must contain four matrices,
named "A," "B," "C," and "D."  (The punctuation is not part of the
matrix name.)
@cindex ss_load

@node state/manual, state/el, state/file, state
@comment  node-name,  next,  previous,  up
@section On-line State Space System Creation

During control synthesis and other applications, it is necessary
to create a new state space system.  The function @code{ss_create}
returns a pointer to a new @code{STATE_SPACE} structure, with the
matrices created, but set to size zero.  If a new @code{STATE_SPACE}
cannot be created, a @code{NULL} is returned.  The name of the system
is set to "NONAME."
@cindex ss_create

The next step is to set the size of the system using @code{ss_resize}.
If the matrices are already the correct size, @code{ss_resize} does not
change them.  Thus, one can easily change the number of outputs from a
system without changing A matrix.  @code{ss_resize} returns an integer
indicating its actions.  A value of -1 indicates that the state space
system is malformed and should be recreated.
The two commands, @code{ss_create} and @code{ss_resize}, are
combined into a short hand notation, @code{ss_init}.
@cindex ss_resize
@cindex ss_init

The command@code{ss_set_name} changes the name of the state space system. 
A name can be up to 19 characters long and cannot contain spaces.  A sample
program is shown below.
@cindex ss_set_name

@example
#include "state.h"

int main(argc,argv)
@{
    STATE_SPACE *example;

    data=ss_init(4,2,3); /* 4 states, 2 inputs, 3 outputs */
    ss_set_name(example,"example");

    /* other code goes here */

    ss_free(example);
@}
@end example

After initializtion, the functions @code{ss_get_nstates},
@code{ss_get_noutputs}, and @code{ss_get_ninputs} are
used to return the number of states, outputs, and inputs, respectively.

A call to @code{ss_get_name} returns a pointer to the name of the
state.  The command @code{ss_print} displays the state in a pretty
format (at least for small matrices).

@node state/el, state/ss, state/manual, state
@comment  node-name,  next,  previous,  up
@section Modifying the State Space System

In some cases it is useful to change some of the state space matrices,
without loading an entire new state space system.  However, improper
use of these functions can cause memory problems.

The first of these functions, @code{mat_el_get}, returns a pointer
the any of the state space matrices.  For instance
@code{mat_print(ss_el_get(sys,'A'))}
will print the A matrix.  It is important to note that the second
argument is a @code{char}, and not a string.  
Either upper of lower case is acceptable.
@cindex ss_el_get

The second function, @code{mat_el_set}, is used to change the system
matrices.  For example, @code{ss_el_set(sys,'b',newB)}
changes the B matrix to @code{newB}.  @code{s_el_set} sets pointers
to matrices; it does not copy matrices.
If the previous B
matrix was allocated be @code{ss_init}, its memory is now lost.
To avoid this, a third function, @code{ss_el_free}, is provided.
This function should be called before @code{ss_el_set} if the 
matrix being replaced is no longer needed.  In addition, if other
parts of the program modify @code{newB}, then the state space system
is modified, too.  @code{ss_el_set} performs no error checking.
@cindex ss_el_set

When changing matrices, the state space matrices may become incompatible.
@code{ss_verify} ensures that the system is valid.  An integer, indicating
what actions were taken, is returned.  If the sizes are
incompatible, -1 is returned.  If all sizes are compatible with the
existing structure, 0 is returned.
@cindex ss_verify


@node state/ss, state/other, state/el, state
@comment  node-name,  next,  previous,  up
@section State Equation Execution

The main use of these routines is to implement
x(k+1)=Ax(k) + Bu(k), y(k)=Cx(k) + Du(k).

Once the matrices A,B,C, and D are loaded, then the initial state, $x_0$,
should be set using @code{ss_set_ic}.  For each step, the input is set using
@code{ss_set_input}.  @code{ss_equation} executes the equations (including
updating the state)  and @code{ss_output} returns the output.  These last
three commands, which are usually called immediately after each other, are
identical to the one command @code{ss_compute}.  It is recommended that
only @code{ss_compute} be used.
@cindex ss_set_ic
@cindex ss_set_input
@cindex ss_equation
@cindex ss_output
@cindex ss_compute

The commands @code{ss_state} and @code{ss_nextstate} return the current state
and next state, respectively.  @code{ss_output} will always return the current
output.
@cindex ss_state
@cindex ss_nextstate
@cindex ss_output

The following sequence of commands illustrates the behavior of the state
evolution.  

@example
sample_code()@{
  STATE_SPACE *s;
  double *x0, *u0, *y0, *temp;

  /* initialize x0, u0 */

  s=ss_matlab_load("file.mat"); /* Load the state matrices */
  ss_set_ic(S, x0);             /* Set the initial condition */
  y0=ss_compute(S, u0);         /* execute the zeroth iteration */
  temp=ss_state(S);             /* returns x0 */
  temp=ss_nextstate(S);         /* returns x1 */
  temp=ss_output(S)             /* also returns y0 */
  y1=ss_compute(S, &u1)         /* execute the first iteration */

  /* more code */
@}
@end example


@node state/other, state/proto, state/ss, state
@comment  node-name,  next,  previous,  up
@section Other State Space Functions

The function @code{ss_copy} copies state space systems.  It copies both
the matrices and the current state, but does not copy the system's name.
@cindex ss_copy

@code{ss_print} displays the size of the state space system and its
matrices.
@cindex ss_print

@node state/proto,  , state/other, state
@comment  node-name,  next,  previous,  up
@section Function Prototypes

All function prototypes are included in @code{state.h}.

@example
STATE_SPACE *ss_init(int nstates, int ninputs, int noutputs);
STATE_SPACE *ss_create();

int ss_setup(STATE_SPACE *s, MATRIX *list);
int ss_resize(STATE_SPACE *s, int nstates, int ninputs, int noutputs);

int ss_resize_state(STATE_SPACE *s, int nstates);
int ss_resize_inputs(STATE_SPACE *s, int ninputs);
int ss_resize_outputs(STATE_SPACE *s, int noutputs);
char *ss_get_name(STATE_SPACE *s);
int ss_set_name(STATE_SPACE *s, char *name);

int ss_get_nstates(STATE_SPACE *s);
int ss_get_noutputs(STATE_SPACE *s);
int ss_get_ninputs(STATE_SPACE *s);

MATRIX *ss_el_get(STATE_SPACE *s, char which);
int ss_el_set(STATE_SPACE *s, char which, MATRIX *mat);
int ss_el_free(STATE_SPACE *s, char which);
int ss_verify(STATE_SPACE *s);

void ss_reset(STATE_SPACE *s);
void ss_free(STATE_SPACE *s);

int ss_copy(STATE_SPACE *dst, STATE_SPACE *src);

void ss_print(STATE_SPACE *s);

int ss_set_ic(STATE_SPACE *s, double *ic);
double *ss_compute(STATE_SPACE *s, double *input);

int ss_set_input(STATE_SPACE *s, double *input);
void ss_equation(STATE_SPACE *s);
double *ss_output(STATE_SPACE *s);

double *ss_state(STATE_SPACE *s);
double *ss_nextstate(STATE_SPACE *s);

STATE_SPACE *ss_load(char *fname);
int ss_verify(STATE_SPACE *s);
#endif

@end example


