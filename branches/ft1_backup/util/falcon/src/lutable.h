#ifndef __LUTABLE_INCLUDED__
#define __LUTABLE_INCLUDED__

typedef struct lu_info {
  double low;      /* The low value */
  double inc;      /* The step size */
  double high;     /* The high value */
  int    nsteps;   /* redundant, but handy. */
} LU_INFO;

typedef struct lu_data {
  int    ndim;       /* Dimension of the lookup table */
  int    nout;       /* Number of output data fields  */
  int    ninterp;    /* Number of outputs to interpolate */
                     /* The first ninterp of nout are interpolated */
  int    elements;   /* Total number of elements in the list */
  struct lu_info *info;
  double **coords;
  double **data;
  int *offsets;   /* Number that must be skipped for an increment */
  int *base;      /* Working array, lower corner */
  int *outside;   /* Working array, are we within table bounds? */
} LU_DATA;


LU_DATA *lu_load(char *file);
LU_DATA *lu_create(int ndim);
int lu_resize(LU_DATA *l);
int lu_lookup(LU_DATA *table, double *coord, double *data);
void lu_free(LU_DATA *table);
void lu_dispinfo(LU_DATA *l);
int lu_setrange(LU_DATA *l, int index, double low, double inc, double high,
		int steps);
int lu_setoutput(LU_DATA *l, int output, int interp);
int lu_setcoord(LU_DATA *l);
int lu_write(LU_DATA *l, char *file);

int     lu_setel(LU_DATA *l, int row, int col, double value);
double  lu_getel(LU_DATA *l, int row, int col);
double *lu_getrow(LU_DATA *l, int row);

/* Accessor functions */
int lu_dimension(LU_DATA *l);
int lu_outputs(LU_DATA *l);
int lu_interp(LU_DATA *l);
int lu_elements(LU_DATA *l);
int lu_coordinate(LU_DATA *l, int row, double *coord);

#endif  /* __LUTABLE_INCLUDED__ */
