
/* 
 * Desc: Window for displaying images
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <jplv/Fl_Glv_Window.H>

#include "sensnet.h"
#include "sensnet_types.h"
#include "sensnet_stereo.h"


// Window for displaying images
class ImageWin : public Fl_Window
{
  public:

  // Constructor
  ImageWin(sensnet_t *sensnet, int x, int y, int w, int h, int menuh);

  public:

  // Handle menu options
  static void onSensor(Fl_Widget *w, int option);

  // Handle menu options
  static void onView(Fl_Widget *w, int option);

  // Draw window
  static void onDraw(Fl_Glv_Window *win, ImageWin *self);

  // Check for updates
  int update();

  public:

  // Local menu
  Fl_Menu_Bar *menubar;

  // GL window
  Fl_Glv_Window *glwin;

  // Sensnet handle
  sensnet_t *sensnet;
  
  // Requested sensor id
  int sensorId;

  // Current blob id
  int blobId;

  // Blob data buffer
  sensnet_stereo_blob_t blob;

  // Menu options
  const Fl_Menu_Item *view_rgb, *view_disp;
};
