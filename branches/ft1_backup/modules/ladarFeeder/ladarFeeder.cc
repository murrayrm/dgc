
#if USE_SENSNET
#include <pose3.h>
#include <sensnet.h>
#include <sensnet_types.h>
#include <sensnet_ladar.h>
#endif

#include "ladarFeeder.hh"

#define CONFIG_PATH_PREFIX      "./config/ladar/LadarCal.ini."
#define CONFIG_BUMPER_SUFFIX    "bumper"
#define CONFIG_BUMPRIGHT_SUFFIX "bumpright"	
#define CONFIG_BUMPLEFT_SUFFIX  "bumpleft"	
#define CONFIG_ROOF_SUFFIX      "roof"
#define CONFIG_ROOFRIGHT_SUFFIX "roofright"
#define CONFIG_ROOFLEFT_SUFFIX 	"roofleft"
#define CONFIG_SMALL_SUFFIX     "small"
#define CONFIG_RIEGL_SUFFIX     "riegl"
#define CONFIG_FRONT_SUFFIX     "front"
#define CONFIG_REAR_SUFFIX      "rear"	

#define ROLL_AVG_POINTS     3
#define ROLL_CALIB_EPS      0.00001
#define ROLL_CALIB_THRESH   0.001
#define PITCH_CALIB_EPS     0.00001
#define PITCH_CALIB_THRESH  0.001

#define PFIXME fprintf(stderr, "%s [%d]: FIXME\n", __FILE__, __LINE__)

/* This is the contructor for LadarFeeder; a constructor is a member function 
 * that is automatically called when an object of that class is declared. A 
 * constructor is used to initialize the values of some or all member variables 
 * and to do any other sort of initialization that may be needed. 
 * 
 * this constructor is called in ladarFeederMain.cc line 422 --JM
 */
LadarFeeder::LadarFeeder(int skynetKey, LadarFeederOptions ladarOptsVal) 
  : CSkynetContainer(MODladarFeeder, skynetKey), 
    CStateClient(!(ladarOptsVal.optSource==ladarSource::SOURCE_FILES)), 
    CTimberClient(timber_types::ladarFeeder) {

                ladarOpts = ladarOptsVal; //just pass over the ladarOpts in ladarFeederMain to this private variable --JM
		_QUIT = 0;
		_PAUSE = 0;
		_STEP = 0;
		_EOL = 0;
		_numDeltas = 0;

		if(ladarOptsVal.optPause) {
			_PAUSE = 1;
		}

		char configFilename[256];
		switch(ladarOpts.optLadar) { 

		  //all these SN* types are declared in sn_types.h which is included through a long chain of includes:
		  //ladarFeeder.hh->MapDeltaTalker.h->SkynetContainer.h->sn_msg.hh->sn_types.h;
		  //_msg* is defined in ladarFeeder.hh; the following chunk of code just deals with
		  //spread message packaging

		case LADAR_BUMPER:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_BUMPER_SUFFIX);
			_msgTypeFusedElev = SNladarBumperDeltaMap;
			_msgTypeMeanElev = SNladarBumperDeltaMapElev;
			_msgTypeStdDev = SNladarBumperDeltaMapStdDev;
			_msgTypeNum = SNladarBumperDeltaMapNum;
			_msgTypePitch = SNladarBumperDeltaMapPitch;
			break;
		case LADAR_ROOF:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_ROOF_SUFFIX);
			_msgTypeFusedElev = SNladarRoofDeltaMap;
			_msgTypeMeanElev = SNladarRoofDeltaMapElev;
			_msgTypeStdDev = SNladarRoofDeltaMapStdDev;
			_msgTypeNum = SNladarRoofDeltaMapNum;
			_msgTypePitch = SNladarRoofDeltaMapPitch;
			break;
		case LADAR_SMALL:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_SMALL_SUFFIX);
			_msgTypeFusedElev = SNladarSmallDeltaMap;
			_msgTypeMeanElev = SNladarSmallDeltaMapElev;
			_msgTypeStdDev = SNladarSmallDeltaMapStdDev;
			_msgTypeNum = SNladarSmallDeltaMapNum;
			_msgTypePitch = SNladarSmallDeltaMapPitch;
			break;
		case LADAR_RIEGL:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_RIEGL_SUFFIX);
			_msgTypeFusedElev = SNladarRieglDeltaMap;
			_msgTypeMeanElev = SNladarRieglDeltaMapElev;
			_msgTypeStdDev = SNladarRieglDeltaMapStdDev;
			_msgTypeNum = SNladarRieglDeltaMapNum; 
			_msgTypePitch = SNladarRieglDeltaMapPitch;
			break;
		case LADAR_FRONT:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_FRONT_SUFFIX);
			_msgTypeFusedElev = SNladarFrontDeltaMap;
			_msgTypeMeanElev = SNladarFrontDeltaMapElev;
			_msgTypeStdDev = SNladarFrontDeltaMapStdDev;
			_msgTypeNum = SNladarFrontDeltaMapNum;
			_msgTypePitch = SNladarFrontDeltaMapPitch;
			break;
		case LADAR_ROOFRIGHT:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_ROOFRIGHT_SUFFIX);
			_msgTypeFusedElev = SNladarRoofRightDeltaMap;
			_msgTypeMeanElev = SNladarRoofRightDeltaMapElev;
			_msgTypeStdDev = SNladarRoofRightDeltaMapStdDev;
			_msgTypeNum = SNladarRoofRightDeltaMapNum;
			_msgTypePitch = SNladarRoofRightDeltaMapPitch;
			break;
		case LADAR_ROOFLEFT:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_ROOFLEFT_SUFFIX);
			_msgTypeFusedElev = SNladarRoofLeftDeltaMap;
			_msgTypeMeanElev = SNladarRoofLeftDeltaMapElev;
			_msgTypeStdDev = SNladarRoofLeftDeltaMapStdDev;
			_msgTypeNum = SNladarRoofLeftDeltaMapNum;
			_msgTypePitch = SNladarRoofLeftDeltaMapPitch;
			break;
		case LADAR_BUMPRIGHT:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_BUMPRIGHT_SUFFIX);
			_msgTypeFusedElev = SNladarBumpRightDeltaMap;
			_msgTypeMeanElev = SNladarBumpRightDeltaMapElev;
			_msgTypeStdDev = SNladarBumpRightDeltaMapStdDev;
			_msgTypeNum = SNladarBumpRightDeltaMapNum;
			_msgTypePitch = SNladarBumpRightDeltaMapPitch;
			break;
		case LADAR_BUMPLEFT:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_BUMPLEFT_SUFFIX);
			_msgTypeFusedElev = SNladarBumpLeftDeltaMap;
			_msgTypeMeanElev = SNladarBumpLeftDeltaMapElev;
			_msgTypeStdDev = SNladarBumpLeftDeltaMapStdDev;
			_msgTypeNum = SNladarBumpLeftDeltaMapNum;
			_msgTypePitch = SNladarBumpLeftDeltaMapPitch;
			break;
		case LADAR_REAR:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_REAR_SUFFIX);
			_msgTypeFusedElev = SNladarRearDeltaMap;
			_msgTypeMeanElev = SNladarRearDeltaMapElev;
			_msgTypeStdDev = SNladarRearDeltaMapStdDev;
			_msgTypeNum = SNladarRearDeltaMapNum;
			_msgTypePitch = SNladarRearDeltaMapPitch;
			break;
		default:
			if(ladarOpts.optSource==ladarSource::SOURCE_FILES) {
				sprintf(configFilename, "%s.header", ladarOpts.logFilenamePrefix);
			} else {
				PFIXME;
			}
      break;
		}

		if(ladarOpts.optLadar != LADAR_RIEGL) {
		  ladarMap.initMap(CONFIG_FILE_DEFAULT_MAP); 
		  //CONFIG_FILE_DEFAULT_MAP is defined in MapConstants.h; loads a basic 500x500 sized map at (0.0, 0.0) UTM --JM
		} else {
			ladarMap.initMap(CONFIG_FILE_LOWRES_MAP);
		}

		layerID_ladarElev       = ladarMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV,           true);
		layerID_ladarElevFused  = ladarMap.addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV, true);
		layerID_ladarElevStdDev = ladarMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV,              true);
		layerID_ladarElevNum    = ladarMap.addLayer<double>(CONFIG_FILE_LADAR_NUM,                 true);
		layerID_ladarElevPitch  = ladarMap.addLayer<double>(CONFIG_FILE_LADAR_PITCH,               true);
  
		if(ladarOpts.optTimber) {
			getPlaybackDir();
			sprintf(ladarOpts.logFilenamePrefix, "%sladar_%s", getPlaybackDir().c_str(), ladarOpts.ladarString);
		}

		/* this next line will make a call to the "init" ftn declared in ladarSource.cc which 
		 * makes a call to the "Setup" ftn in sick_driver.cc which essentially starts a thread 
		 * that continually reads from the SICK laser sensor specified in the configFilename 
		 * passed as one of the arguments here --JM
		 * */
		if(ladarObject.init(ladarOpts.optSource, configFilename, ladarOpts.logFilenamePrefix,ladarOpts.optLog, 
					ladarOpts.optLadar)!=ladarSource::OK) {
			printf("%s [%d]: LADAR could not initialize properly.  Error was '%s.'  Quitting.\n",
						 __FILE__, __LINE__, ladarObject.getErrorMessage());
			exit(1);
		}

		if(ladarOpts.optReset) {
			printf("%s [%d]: Resetting the LADAR.\n", __FILE__, __LINE__);
			ladarObject.reset();
			printf("%s [%d]: Done!  If this was the Reigl, nothing happened - otherwise try accessing the LADAR again in 1 minute\n", __FILE__, __LINE__);
			exit(1);
		}

		ladarPositionOffset = ladarObject.getLadarPosition(); //this just stores the translation between Veh Ref Frame and Ladar Ref Frame
		ladarAngleOffset = ladarObject.getLadarAngle(); //this just stores the translation between Veh Ref Frame and Ladar Ref Frame

		DGCcreateMutex(&ladarMapMutex);

		processingTime = 0;
		for(int i=0; i<CYCLES_TO_TIME_OVER; i++) {
			timingArray[i] = 0;
		}
		timingArrayIndex = 0;

		_estopRun = false;
		//_careAboutEstop = true;
  
#ifdef LADAR_GUI
		mapDisp = new MapDisplayThread("ladarFeeder", new MapConfig);
		mapDisp->get_map_config()->set_cmap(&(ladarMap), &ladarMapMutex);
		mapDisp->get_map_config()->set_state_struct(&m_state);

		if(ladarOpts.optMapDisp) {
			mapDisp->guithread_start_thread();
		}
#else
		if(ladarOpts.optMapDisp) {
			cout << __FILE__ << "[" << __LINE__ << "]: "
					 << "Built-in GUI support was not enabled during compilation.  To enable, please define the environment variable LADAR_GUI" << endl
					 << "(For example, do: 'export LADAR_GUI=1')" << endl;
			exit(1);
		}
#endif
	}


LadarFeeder::~LadarFeeder() {
  //FIX ME
  PFIXME;
  printf("Thank you for using the ladarmapper destructor\n");
}


void LadarFeeder::ActiveLoop() {
  unsigned long long scan_timestamp;

  NEDcoord UTMPoint;

  unsigned long long start, end;

  ladarMeasurementStruct myMeasurement; //defined in ladarSources.hh --JM

  //unsigned long loopCounter = 0;

  int socket_num_ladar = 0;
  
#if USE_SENSNET
  // Sensnet interface
  sensnet_t *sensnet;
  sensnet_id_t sensorId = SENSNET_NULL_SENSOR;
#endif

  if(ladarOpts.optLadar==LadarFeeder::LADAR_ROOF) {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas_roof);
  }
  else if(ladarOpts.optLadar==LadarFeeder::LADAR_BUMPER) {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas_bumper);
#if USE_SENSNET
    sensorId = SENSNET_MF_BUMPER_LADAR;
#endif
  }
  else if(ladarOpts.optLadar==LadarFeeder::LADAR_BUMPLEFT) {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas_bumpleft);
#if USE_SENSNET
    sensorId = SENSNET_LF_BUMPER_LADAR;
#endif
  }
  else if(ladarOpts.optLadar==LadarFeeder::LADAR_BUMPRIGHT) {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas_bumpright);
#if USE_SENSNET
    sensorId = SENSNET_RF_BUMPER_LADAR;
#endif
  }
  else if(ladarOpts.optLadar==LadarFeeder::LADAR_ROOFLEFT) {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas_roofleft);
#if USE_SENSNET
    sensorId = SENSNET_LF_ROOF_LADAR;
#endif
  }
  else if(ladarOpts.optLadar==LadarFeeder::LADAR_ROOFRIGHT) {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas_roofright);
#if USE_SENSNET
    sensorId = SENSNET_RF_ROOF_LADAR;
#endif
  }
  else if(ladarOpts.optLadar==LadarFeeder::LADAR_REAR) {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas_rear);
  }		
  else {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas);
  }

#if USE_SENSNET
  assert(sensorId != SENSNET_NULL_SENSOR);
  
  // Initialize sensnet
  sensnet = sensnet_alloc();
  if (sensnet_connect(sensnet, NULL, ladarOpts.optSNKey, sensorId /* HACK WRONG */) != 0)
  {
    fprintf(stderr, "unable to connect to sensnet\n");
    return;
  }
#endif

  RPYangle ladarOrientation = ladarObject.getLadarAngle();
  double measHeightVar;

  while(!_QUIT) {
    if(!_PAUSE || _STEP) {
      _STEP = 0;
      /*
        if(ladarObject.scanIndex() >= ladarOpts.optMaxScans && ladarOpts.optMaxScans>=0) {
        return;
        }
      */
      DGCgettime(start);
      
      if(!ladarOpts.optLog) {
        if(getLoggingEnabled()) {
          if(checkNewDirAndReset()) {
            ladarObject.stopLogging();
            ladarOpts.loggingOpts.logRaw = 1;	
            ladarOpts.loggingOpts.logState = 1;
            ladarOpts.loggingOpts.logRanges = 0;
            ladarOpts.loggingOpts.logRelative = 0;
            ladarOpts.loggingOpts.logUTM = 0;
            sprintf(ladarOpts.logFilenamePrefix, "%sladar_%s", getLogDir().c_str(), ladarOpts.ladarString);
            ladarObject.startLogging(ladarOpts.loggingOpts, ladarOpts.logFilenamePrefix);
          }
        } else {
          ladarObject.stopLogging();
        }
      }
      
      //ladarObject.updateState(m_state) must come after ladarObject.scan()
      //printf("%s [%d]: Got here\n", __FILE__, __LINE__);
      if(!ladarOpts.optTimber) {
        scan_timestamp = ladarObject.scan();
      } else {
        unsigned long long timberTime = getPlaybackTime();
        scan_timestamp = ladarObject.scan(timberTime);
        blockUntilTime(scan_timestamp);
        if(scan_timestamp==0) {
          //exit(0);
          _EOL = 1;
          sleep(1);
          return;
        }
      }
      // update for sparrow display
      m_active_time = scan_timestamp;
      
      //if(offset==0) offset = playback_state.playback_time_point;
      UpdateState(scan_timestamp, true);
      if(ladarOpts.optZeroAltitude) {
        m_state.Altitude = 0.0;
      }
      ladarObject.updateState(m_state);
      
      
      DGClockMutex(&ladarMapMutex);
      ladarMap.updateVehicleLoc(m_state.Northing, m_state.Easting); //updates the vehicle location in the map for ladar meas transf -- JM
      
      // road finding ladar information
      
      myMeasurement.numPoints = ladarObject.numPoints();
      
      myMeasurement.timeStamp = m_state.Timestamp;

      myMeasurement.offset = ladarObject.getLadarPosition();
      myMeasurement.angles = ladarObject.getLadarAngle();

      myMeasurement.data[0].ranges = ladarObject.range(0);
      myMeasurement.data[0].angles = ladarObject.angle(0);

 
      /* I think the way ladarOpts.optCalibrate works is that when ladarFeeder is run like the following:
       *
       * $ ./ladarFeeder --ladar ro --calibrate 2
       * 
       * by hand which makes sense; why else would you want to enter this portion of the active loop
       * unless you explicitly told it to, in which case you are aware of the fact that you
       * are calibrating the lasers --JM
       */ 
      if(ladarOpts.optCalibrate) {
        RPYangle newRPYCalib = ladarObject.getLadarAngle();
        XYZcoord newXYZCalib = ladarObject.getLadarPosition();
        double newRoll;
        double newPitch;
        newRoll = GetCalibRoll();
        if(ladarOpts.optCalibrate==2)
          cout << setprecision(10) << "Calibrating... ";
        if(fabs(newRoll) < 0.5) {
          if(ladarOpts.optCalibrate==2)
            cout << "Changing roll from: " << newRPYCalib.R << " to " << newRPYCalib.R+newRoll << endl;
          newRPYCalib.R += newRoll;
          ladarObject.setLadarFrame(newXYZCalib, newRPYCalib);
        } else {
          newPitch = GetCalibPitch();
          if(fabs(newPitch) < 0.5) {
            if(ladarOpts.optCalibrate==2)
              cout << "Changing pitch from: " << newRPYCalib.P << " to " << newRPYCalib.P+newPitch << endl;
            newRPYCalib.P += newPitch;
            ladarObject.setLadarFrame(newXYZCalib, newRPYCalib);
          } else {
            if(ladarOpts.optCalibrate==2)
              cout << "Final calibration obtained.  Roll=" << newRPYCalib.R << ", Pitch=" << newRPYCalib.P << " - quitting"  << endl;
            exit(0);
          }
        }
      }
      //_estopRun is basically set to true/false in the ReceiveData_estop thread; basically, the following:
      // if _estopRun is true (i.e. we can run) or we don't careAboutEstop, then ...
      //
      if(_estopRun || !_careAboutEstop) {
        for(int i=1; i<ladarObject.numPoints(); i++) {      
          myMeasurement.data[i].ranges = ladarObject.range(i);
          myMeasurement.data[i].angles = ladarObject.angle(i);
	  
          //Do not uncomment - breaks synchronization
          // 				UpdateState(ladarObject.timestamp(i), true);
          // 				if(ladarOpts.optZeroAltitude) {
          // 					m_state.Altitude = 0.0;
          // 				}
          // 				ladarObject.updateState(m_state);
	  
	  
          UTMPoint = ladarObject.UTMPoint(i);
          //ladarMap.setDataUTM_Delta<double>(layerID_ladarElev, UTMPoint.N, UTMPoint.E, UTMPoint.D);
          CElevationFuser tempCell = ladarMap.getDataUTM<CElevationFuser>(layerID_ladarElevFused, UTMPoint.N, UTMPoint.E);
	  
          if(ladarOpts.optKF) {
            measHeightVar = fabs(ladarObject._ladarSigma*sin(m_state.Pitch+ladarOrientation.P));
            tempCell.fuse_KFElevation(UTMPoint, scan_timestamp, measHeightVar);
          } else {
            tempCell.fuse_MeanElevation(UTMPoint, scan_timestamp);
          }
          ladarMap.setDataUTM_Delta<CElevationFuser>(layerID_ladarElevFused, UTMPoint.N, UTMPoint.E, tempCell);
          ladarMap.setDataUTM_Delta<double>(layerID_ladarElev, UTMPoint.N, UTMPoint.E, tempCell.getMeanElevation());
          if(ladarOpts.optKF) {
            ladarMap.setDataUTM_Delta<double>(layerID_ladarElevStdDev, UTMPoint.N, UTMPoint.E, tempCell.getHeightVar()*1000);
          } else {
            ladarMap.setDataUTM_Delta<double>(layerID_ladarElevStdDev, UTMPoint.N, UTMPoint.E, tempCell.getStdDev());
          }
          ladarMap.setDataUTM_Delta<double>(layerID_ladarElevNum, UTMPoint.N, UTMPoint.E, (double)tempCell.getNumPoints());
          ladarMap.setDataUTM_Delta<double>(layerID_ladarElevPitch, UTMPoint.N, UTMPoint.E, m_state.Pitch*100.0);
        }
      }
      
      if(ladarOpts.optLadar==LadarFeeder::LADAR_ROOF || ladarOpts.optLadar==LadarFeeder::LADAR_BUMPER || ladarOpts.optLadar==LadarFeeder::LADAR_BUMPLEFT || ladarOpts.optLadar==LadarFeeder::LADAR_BUMPRIGHT || ladarOpts.optLadar==LADAR_ROOFLEFT || ladarOpts.optLadar==LADAR_ROOFRIGHT || ladarOpts.optLadar==LADAR_REAR) {
        static int cnt=0;
        if (cnt==LADARFEEDER_SEND_SCAN_INTERVAL) // Only send every Nth scan
        { 
          //cout<<"Sending "<<myMeasurement.numPoints<<" points  "<<LADARMEASUREMENTSTRUCTBASE + 2*sizeof(double)*myMeasurement.numPoints<<" bytes"<<endl;
          m_skynet.send_msg(socket_num_ladar, ((void*)&myMeasurement), LADARMEASUREMENTSTRUCTBASE + 2*sizeof(double)*myMeasurement.numPoints,0);

#if USE_SENSNET
          // Send data over senset also
          if (true)
          {
            int i;
            sensnet_ladar_blob_t blob;

            blob.blob_type = SENSNET_LADAR_BLOB;
            blob.sensor_id = sensorId;
            blob.scanid = this->_numDeltas;

            pose3_t pose;
            pose.pos = vec3_set(ladarPositionOffset.X + VEHICLE_AXLE_DISTANCE,
                                ladarPositionOffset.Y,
                                ladarPositionOffset.Z);
            pose.rot = quat_from_rpy(ladarAngleOffset.R + M_PI,
                                     ladarAngleOffset.P,
                                     ladarAngleOffset.Y);
            pose3_to_mat44f(pose, blob.sens2veh);

            blob.num_points = myMeasurement.numPoints;
            for (i = 0; i < blob.num_points; i++)
            {
              blob.points[i][0] = myMeasurement.data[i].angles - M_PI/2; // HACK TODO
              blob.points[i][1] = myMeasurement.data[i].ranges;

              //printf("\n\n %d %f %f\n", i,
              //       myMeasurement.data[i].angles,
              //       myMeasurement.data[i].ranges);
            }

            sensnet_write(sensnet, sensorId, SENSNET_LADAR_BLOB, blob.scanid, sizeof(blob), &blob);
          }
#endif          

          cnt=0;
        }
        cnt++;
      }

      DGCunlockMutex(&ladarMapMutex);

      if(ladarOpts.optDeltaRate == 0.0) {
        SendMapDeltas();
      }
      
      /*
        if(ladarOpts.optDelay!=-1) {
        usleep((unsigned int)(ladarOpts.optDelay*1e6));
        }
        if(ladarOpts.optRate!=-1) {
        usleep((unsigned int)((1/ladarOpts.optRate)*1e6));
        } 
      */

      DGCgettime(end);
      timingArray[timingArrayIndex%CYCLES_TO_TIME_OVER] = end-start;      
      timingArrayIndex++;
      unsigned long long totalProcessingTime = 0;
      for(int i=0; i<CYCLES_TO_TIME_OVER; i++) {
        totalProcessingTime+=timingArray[i];
      }
      processingTime = totalProcessingTime/CYCLES_TO_TIME_OVER;
      
#ifdef LADAR_GUI
      if(ladarOpts.optMapDisp) {
        mapDisp->notify_update();
      }
#endif
    } else {
      DGCusleep(500000);
    }
  }
}


void LadarFeeder::ReceiveDataThread() {
  printf("Data receiving not yet");
}


void LadarFeeder::SendDataThread() {
  unsigned long long start, end;

  unsigned long long totalCycleTime = (unsigned long long)(1.0e6/ladarOpts.optDeltaRate);

  while(!_QUIT) {
    DGCgettime(start);
    SendMapDeltas();
    DGCgettime(end);
    if(end-start < totalCycleTime) {
      usleep(totalCycleTime - (end - start));
    }
  }    
}


void LadarFeeder::SaveMapsThread() {
  unsigned long long start, end;
	char mapName[128];
  unsigned long long totalCycleTime = (unsigned long long)(1.0e6/ladarOpts.optSaveMapsRate);

  while(!_QUIT) {
		if(!ladarOpts.optTimber) {
			DGCgettime(start);
		} else {
			start = getPlaybackTime();
		}

		DGClockMutex(&ladarMapMutex);    

		sprintf(mapName, "%s-elev-%lld", ladarOpts.ladarString, start);
		ladarMap.saveLayer<double>(layerID_ladarElev, mapName, true);

		sprintf(mapName, "%s-stddev-%lld", ladarOpts.ladarString, start);
		ladarMap.saveLayer<double>(layerID_ladarElevStdDev, mapName, true);

		sprintf(mapName, "%s-num-%lld", ladarOpts.ladarString, start);
		ladarMap.saveLayer<double>(layerID_ladarElevNum, mapName, true);

		DGCunlockMutex(&ladarMapMutex);

		if(!ladarOpts.optTimber) {
			DGCgettime(end);
			if(end-start < totalCycleTime) {
				usleep(totalCycleTime - (end - start));
			}
		} else {
			end = getPlaybackTime();
			if(end-start < totalCycleTime) {
				blockUntilTime(start+totalCycleTime);
			}
		}
  }    
}


void LadarFeeder::SendMapDeltas() {
  CDeltaList* deltaPtr = NULL;
  int socket_num = 0;
  int socket_num_fused = 0;
  int socket_num_stddev = 0;
  int socket_num_num = 0;
	int socket_num_pitch = 0;

  socket_num = m_skynet.get_send_sock(_msgTypeMeanElev);
  socket_num_fused = m_skynet.get_send_sock(_msgTypeFusedElev);
  socket_num_stddev = m_skynet.get_send_sock(_msgTypeStdDev);
  socket_num_num = m_skynet.get_send_sock(_msgTypeNum);
	socket_num_pitch = m_skynet.get_send_sock(_msgTypePitch);

  unsigned long long scan_timestamp;

  DGClockMutex(&ladarMapMutex);    
  DGCgettime(scan_timestamp);
  
  deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElev);
  if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1 && ladarOpts.optSendElev==1) {
    SendMapdelta(socket_num, deltaPtr);
    _numDeltas++;
  }
  ladarMap.resetDelta<double>(layerID_ladarElev);    
  
  deltaPtr = ladarMap.serializeDelta<CElevationFuser>(layerID_ladarElevFused);
  if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1) {
    SendMapdelta(socket_num_fused, deltaPtr);
    _numDeltas++;
  }
  ladarMap.resetDelta<CElevationFuser>(layerID_ladarElevFused);    

  deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElevStdDev);
  if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1 && ladarOpts.optSendStdDev==1) {
    SendMapdelta(socket_num_stddev, deltaPtr);
    _numDeltas++;
  }
  ladarMap.resetDelta<double>(layerID_ladarElevStdDev);    

  deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElevNum);
  if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1 && ladarOpts.optSendNum==1) {
    SendMapdelta(socket_num_num, deltaPtr);
    _numDeltas++;
  }
  ladarMap.resetDelta<double>(layerID_ladarElevNum);    
  
  deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElevPitch);
  if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1 && ladarOpts.optSendPitch==1) {
    SendMapdelta(socket_num_pitch, deltaPtr);
    _numDeltas++;
  }
  ladarMap.resetDelta<double>(layerID_ladarElevPitch);    
  
  DGCunlockMutex(&ladarMapMutex);
}

void LadarFeeder::ReceiveDataThread_EStop() {
	int oldStatus = -1;

	while(!_QUIT) {
		WaitForNewActuatorState();
		if(m_actuatorState.m_about_to_unpause == true || 
			 (m_actuatorState.m_estoppos == 2 && m_state.Speed2() > 0.1)){		
			_estopRun = true;
		} else{
			_estopRun = false;
		}
	}
}


double LadarFeeder::GetCalibRoll() {
	double avg_roll_s, avg_roll_e;

	if (ladarObject.numPoints() == 0){
	  if(ladarOpts.optCalibrate==2)
	    cout << "No Points obtained from the LADAR" << endl;
	  return 0;
	}
	
	avg_roll_s = 0;
	for (int i = 0 ;i < ROLL_AVG_POINTS-1; i++)
		avg_roll_s += ladarObject.UTMPoint(i).D;
	avg_roll_s /= ROLL_AVG_POINTS;

	avg_roll_e = 0;
	for (int i = ladarObject.numPoints() - ROLL_AVG_POINTS ;i < ladarObject.numPoints(); i++)
		avg_roll_e += ladarObject.UTMPoint(i).D;
	avg_roll_e /= ROLL_AVG_POINTS;

	if(fabs(avg_roll_s - avg_roll_e) < ROLL_CALIB_THRESH) 
	  return -1.0;

	if(ladarOpts.optCalibrate==2)
	  cout << "Got a diff of " << fabs(avg_roll_s - avg_roll_e) 
	       << " and s was " << avg_roll_s << " and e was " << avg_roll_e << " ";

	if (avg_roll_s < avg_roll_e)
		return  ROLL_CALIB_EPS;
	else 
		return  -ROLL_CALIB_EPS;

	
}

double LadarFeeder::GetCalibPitch() {
	double avg_pitch;
	avg_pitch = 0;

	if (ladarObject.numPoints() == 0){
	  if(ladarOpts.optCalibrate==2)
	    cout << "No Points obtained from the LADAR" << endl;
	  return 0;
	}
	
	for (int i = 0 ;i < ladarObject.numPoints(); i++)
		avg_pitch += ladarObject.UTMPoint(i).D;
	avg_pitch /= ladarObject.numPoints();

	if(fabs(avg_pitch) < PITCH_CALIB_THRESH)
	  return -1.0;

	if(ladarOpts.optCalibrate==2)
	  cout << "Got a elev of " << avg_pitch << " ";

	if (avg_pitch < 0) 
		return -PITCH_CALIB_EPS;
	else
		return PITCH_CALIB_EPS;
}
