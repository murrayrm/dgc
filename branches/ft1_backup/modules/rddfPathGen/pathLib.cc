#include "pathLib.hh"
#include "trajDFE.hh"



/**
 * Prints paths to cout.
 * @Param path the path to be printed.
 */
void printPath(pathstruct path)
{
  //###
  //  cout << "Path has " << path.numPoints << " points." << endl;
  //###
  for(unsigned int i = 0; i < path.numPoints; i++)
    {
      cout << "[ " 
	   << path.e[i] << ", "
	   << path.n[i] << ", "
	   << path.ve[i] << ", "
	   << path.vn[i] << ", "
	   << path.ae[i] << ", "
	   << path.an[i] << " ]\n";
    }
}



/**
 * Returns an empty path that points can be added to at a later time.
 */
pathstruct emptyPath()
{
  pathstruct ret;
  ret.numPoints = 0;
  ret.currentPoint = 0;
  return ret;
}



/**
 * Adds a point to path.
 * @param path the path to add the point to.
 * @param e the easting at the point you wish to add.
 * @param n the northing at the point you wish to add.
 * @param ve the easting velocity at the point you wish to add.
 * @param vn the northing velocity at the point you wish to add.
 * @param ae the easting acceleration at the point you wish to add.
 * @param an the northing acceleration at the point you wish to add.
 */
void addToPath(pathstruct & path, double e, double n, 
	       double ve, double vn, double ae, double an, double cor_width)
{
  path.numPoints++;
  path.e.push_back(e);
  path.n.push_back(n);
  path.ve.push_back(ve);
  path.vn.push_back(vn);
  path.ae.push_back(ae);
  path.an.push_back(an);
  path.corWidth.push_back(cor_width);
  //###
  //cout << "\nPath currently is:\n";
  //printPath(path);
  //###
}



/**
 * Adds a point to path.
 * @param path the path to add the point to.
 * @param e the easting at the point you wish to add.
 * @param n the northing at the point you wish to add.
 * @param vel the velocity at the point in vector form
 * @param ae the easting acceleration at the point you wish to add.
 * @param an the northing acceleration at the point you wish to add.
 */
void addToPath(pathstruct & path, double e, double n, 
	       vector<double> vel, double ae, double an, double cor_width)
{
  path.numPoints++;
  path.e.push_back(e);
  path.n.push_back(n);
  path.ve.push_back(vel[0]);
  path.vn.push_back(vel[1]);
  path.ae.push_back(ae);
  path.an.push_back(an);
  path.corWidth.push_back(cor_width);
  //###
  //cout << "\nPath currently is:\n";
  //printPath(path);
  //###
}



/**
 * Adds a point to path.
 * @param path the path to add the point to.
 * @param e the easting at the point you wish to add.
 * @param n the northing at the point you wish to add.
 * @param vel the velocity at the point in vector form
 * @param vel the acceleration at the point in vector form
 */
void addToPath(pathstruct & path, double e, double n, 
	       vector<double> vel, vector<double> accel, double cor_width)
{
  path.numPoints++;
  path.e.push_back(e);
  path.n.push_back(n);
  path.ve.push_back(vel[0]);
  path.vn.push_back(vel[1]);
  path.ae.push_back(accel[0]);
  path.an.push_back(accel[1]);
  path.corWidth.push_back(cor_width);
  //###
  //cout << "\nPath currently is:\n";
  //printPath(path);
  //###
}



/**
 * adds the points in path2 to the end of path1.
 */
void MergePaths(pathstruct & path1, pathstruct & path2)
{
  for (unsigned int i = 0; i < path2.numPoints; ++i)
      addToPath(path1,
		path2.e[i], path2.n[i],
		path2.ve[i], path2.vn[i],
		path2.ae[i], path2.an[i]);
}



vector<double> GetLocation(pathstruct & path, unsigned int point)
{
  vector<double> ret(2);
  ret[0] = path.n[point];
  ret[1] = path.e[point];

  return ret;
}



/**
 * Scales the velocity at the current point.  The direction of the velocity 
 * stays the same; the acceleration changes so that the radius of the turn
 * stays the same.
 * @param the path containing the point of interest
 * @param current_point the point whose velocity will soon be changed.
 * @param velocity_desired the velocity will be scaled such that the 
 *         magnitude of the final velocity is velocity_desired
 */
void SetVel(pathstruct& path, unsigned int current_point, double velocity_desired)
{
  vector<double> vel(2); //velocity vector
  vector<double> accel(2); //acceleration vector
  vector<double> accel_radial(2); //radial component of acceleration
  //set values to origional values in path struct
  vel[0] = path.vn[current_point];
  vel[1] = path.ve[current_point];
  accel[0] = path.an[current_point];
  accel[1] = path.ae[current_point];
  if (vecmagsquared(vel)) //faster than vecmag(), (jason's Idea)
    {
      //calculate radial component of acceleration
      accel_radial = accel - vel*vecdot(vel,accel)/vecmagsquared(vel);
      //remove radial component of acceleration
      accel = accel-accel_radial;
      //calculate new (desired) vector values
      accel_radial = pow(velocity_desired/vecmag(vel),2) * accel_radial;
      vel = velocity_desired * (vel/vecmag(vel));
      //add new radial component back into acceleration vector
      accel = accel+accel_radial;
      path.vn[current_point] = vel[0];
      path.ve[current_point] = vel[1];
      path.an[current_point] = accel[0];
      path.ae[current_point] = accel[1];
    }
}



/**
 * Defunct function.  Under the old implementation, StoreWholePath was the
 * main function that RddfPathGen called.  Now it's more complicated, because
 * RddfPathGen has a sense of where we are, requiring a more complicated
 * implementation.
 */
void StoreWholePath(CTraj & traj, const corridorstruct & cor)
{
  // figure out whole path
  pathstruct path;
  path = Path_From_Corridor(cor);

  // make velocities agreeable
  // doSomethingWithVelocities(path);

  // transfer data from path to traj
  int N = path.numPoints;
  double n[3], e[3];
  traj.startDataInput();
  for(int i = 0; i < N; i++)
    {
      e[0] = path.e[i];
      e[1] = path.ve[i];
      e[2] = path.ae[i];
      n[0] = path.n[i];
      n[1] = path.vn[i];
      n[2] = path.an[i];
      traj.inputWithDiffs(n, e);
    }
  
  //###
  //  cout << "Traj that we're about to send is: " << endl;
  //  traj.print();
  //###
}



/**
 * Stores a path (in pathstruct form) in a traj (in CTraj form)
 * @param path the path to be stored
 * @param traj the traj to fill with the path data
 */
void StorePath(CTraj & traj, const pathstruct & path)
{
  // transfer data from path to traj
  int N = path.numPoints;
  double n[3], e[3];
  traj.startDataInput();
  for(int i = 0; i < N; i++)
    {
      e[0] = path.e[i];
      e[1] = path.ve[i];
      e[2] = path.ae[i];
      n[0] = path.n[i];
      n[1] = path.vn[i];
      n[2] = path.an[i];
      traj.inputWithDiffs(n, e);
    }
}



/**
 * Writes a path to a stream, typically cout or a file stream
 * @param path the path to be written
 * @param outstream the stream which the path will be written to.
 */
void WritePath(const pathstruct & path, ostream & outstream)
{
  // took this out per laura's request...
  //outstream << "% Generated by RddfPathGen" << endl
  //	  << "% format is N, dN, ddN, E, dE, ddE" << endl;

  int width = 12;
  for (unsigned int i = 0; i < path.numPoints; i++)
    {
      // we'll output in the traj format N, dN, ddN, E, dE, ddE
      outstream << setprecision(3) << setiosflags(ios::showpoint) << setiosflags(ios::fixed)
		<< setw(width) << path.n[i] << setw(width) << path.vn[i]
		<< setw(width) << path.an[i] << setw(width) << path.e[i]
		<< setw(width) << path.ve[i] << setw(width) << path.ae[i]
		<< endl;
    }
}



/**
 * A wrapper for WritePath in which you can just specify a filename
 * to write the path to.  WritePathToFile will overwrite any prior
 * file at the specified location.
 * @param path the path that will be written to a file.
 * @param filename a string containing the name of the file to be written.
 */
void WritePathToFile(const pathstruct & path, char filename[])
{
  ofstream output_file;
  output_file.open(filename, ios::trunc | ios::out);
  if(!output_file.is_open())
    cout << "\n ! Unable to open file " << filename << " , no data will be written!" << endl;
  else
    WritePath(path, output_file);
  
  output_file.close();
}



/**
 * This is the main function called on a regular basis by RddfPathGen.
 * Give it a sparse path, and it will
 * update its internal representation of our current location, and return a dense path
 * that has been cropped to a certain distance ahead and behind the closest point on 
 * the path.
 *
 * @param path_whole_sparse the path we're going to be generating the dense path from
 * @param location the current location of the vehicle as a vector (Northing, Easting).
 * @param chopBehind the distance behind our current locaitn that the dense path will be
 *     chopped off at
 * @param chopAhead the distance ahead of our current location that the dense path will be
 *     chopped off at
 *
 *  Usage example:
 *   pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, CHOPBEHIND, CHOPAHEAD);
 */
pathstruct DensifyAndChop(pathstruct & path_whole_sparse, vector<double> location,
			  double chop_behind, double chop_ahead,
			  int waypoint_num_override)
{
  //
  // pathstruct DensifyAndChop(const pathstruct & path_whole_sparse,
  //                           const double & chop_behind, const double & chop_ahead)
  //
  // Changes:
  //   2-8-2004, Jason Yosinski, created
  //
  // Function:
  //   Generates from a sparse path a dense path that starts a distance
  //   chopBehind behind our current location and chopAhead ahead of
  //   our current location (determined by querying vstate)
  // 
  // Input:
  //   path_whole_dense
  //     the path we're going to be generating the dense path from
  //   chopBehind
  //     the distance behind our current locaitn that the dense path will be
  //     chopped off at
  //   chopAhead
  //     the distance ahead of our current location that the dense path will be
  //     chopped off at
  //
  // Usage example:
  //   pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, CHOPBEHIND, CHOPAHEAD);
  //
  
  //###
  //cout << "Got into DensifyAndChop" << endl;
  //###

  // Update path_whole_sparse.currentPoint to reflect our new location
  UpdateCurrentPoint(path_whole_sparse, location);
  int curpt = path_whole_sparse.currentPoint;
  cout << "location is " << location[0] << " " << location[1] << endl;
  cout << "UpdateCurPoint: point " << curpt
       << " (" << location[0] - path_whole_sparse.n[curpt]
       << ", " << location[1] - path_whole_sparse.e[curpt] << ")" << endl;

  if (waypoint_num_override != -1)
    {
      curpt = waypoint_num_override;
      path_whole_sparse.currentPoint = curpt;
      cout << "        herman: point " << curpt
       << " (" << location[0] - path_whole_sparse.n[curpt]
       << ", " << location[1] - path_whole_sparse.e[curpt] << ")" << endl;
    }

  //  cout << "Update1 CP = " << path_whole_sparse.currentPoint;

  // Chop our current whole sparse path down to a managable size before we densify it
  pathstruct chopped_sparse_path = ChopPath(path_whole_sparse, chop_behind, chop_ahead);
  //  cout << "    Chop1 CP = " << chopped_sparse_path.currentPoint;

  // Densify the path
  pathstruct prechopped_dense_path = DensifyPath(chopped_sparse_path);

  // update the currentPoint in the dense path
  UpdateCurrentPoint(prechopped_dense_path, location);
  //  cout << "    Update2 CP = " << prechopped_dense_path.currentPoint;

  // further trim our dense path (to avoid sending a huge amount of data over the network)
  pathstruct chopped_dense_path = ChopPath(prechopped_dense_path, chop_behind, chop_ahead);
  //  cout << "    Chop2 CP = " << chopped_dense_path.currentPoint << endl;

  //###
  //cout << "About to leave DensifyAndChop" << endl;
  //###

  // just to get it to compile...
  return chopped_dense_path;
}



/**
 * Updates where we are on a given path.  The function accomplishes this by stepping
 * along the path from the current point until a step brings it farther from location.
 * The funciton stores the point as the last point before it went farther away.
 * It works this way so it can deal with overlapping paths.
 * In theory, there is a pathological example where our algorithm breaks down
 * (when RddfPathGen is started in the middle of a long corridor with loops) and
 * becomes stuck, thinking we are closest to a point quite far from the actual
 * vehicle location.  However, if at any point we're farther than MAXDISTANCEFROMPATH
 * from the point on the path we think is closest, UpdateCurrentPoint will do an
 * exhaustive search of all points to guarantee our choice of the closest point is
 * the correct one.
 * 
 * @param path the path whose currentPoint is being updated.
 * @param vehicle_location the current location of the vehicle as a vector
 *        (Northing, Easting)
 *
 *
 * LARGE CHANGE
 * Now we just do an exhaustive seach... no more stepping.
 */
void UpdateCurrentPoint(pathstruct & path, vector<double> & vehicle_location)
{
  // 
  // Changes:
  //   2-11-2005, Jason Yosinski, created
  //
  // Function:
  //   Finds the point on a path closest to the current location.  The function
  //   accomplishes this by stepping along the path from the current point
  //   until a step brings it farther from location.  The funciton stores the point
  //   as the last point before it went farther away.  It works this way so it can
  //   deal with overlapping paths.
  //

  double best_dist = 1.0e20;
  int best_wpt = 0;
  double current_distance;
  NEcoord location(vehicle_location[0], vehicle_location[1]);
  NEcoord path_point;
  for (int i = 0; i < path.numPoints; i++)
    {
      path_point.N = path.n[i];
      path_point.E = path.e[i];
      current_distance = (location-path_point).norm();
      if (current_distance < best_dist)
	{
	  best_dist = current_distance;
	  best_wpt = i;
	}
    }
  path.currentPoint = best_wpt;
  
  /*
    old stepping code... commented out by jason 9-5-5
  if (path.currentPoint < 0 || path.currentPoint > path.numPoints + 1)
    cerr << "UpdateCurrentPoint:  Error - path.currentPoint = " << path.currentPoint << endl;

  double current_distance, next_distance;
  vector<double> current_location(2);  // N,E format
  vector<double> next_location(2);  // N,E format

  // change this to 1 if you want to loop around an rddf many times
  const bool CONTINUE_PAST_LAST_POINT = true;
  if (CONTINUE_PAST_LAST_POINT)
    {
      current_location[0] = path.n[path.currentPoint];
      current_location[1] = path.e[path.currentPoint];
      current_distance = vecmag(current_location - vehicle_location);
    }

  while(true)
    {
      if (path.currentPoint >= path.numPoints - 1)
	// we've reached the last point
	break;

      current_location[0] = path.n[path.currentPoint];
      current_location[1] = path.e[path.currentPoint];
      current_distance = vecmag(current_location - vehicle_location);

      next_location[0] = path.n[path.currentPoint + 1];
      next_location[1] = path.e[path.currentPoint + 1];
      next_distance = vecmag(next_location - vehicle_location);

      if (next_distance > current_distance)
	break;
      else
	// we're better off at the next point, so make that our current point
	// and continue through the loop
	path.currentPoint++;
    }


  // check to see if we're within MAXDISTANCEFROMPATH of the point before we break
  if (current_distance > MAXDISTANCEFROMPATH)
    {
      // we should go through the whole path and find the absolute closest 
      // point
      cout << "Farther than " << MAXDISTANCEFROMPATH << "m, new closest is ";
      unsigned int closest_point_so_far = 0;
      // grab the first point
      current_location[0] = path.n[0];
      current_location[1] = path.e[0];
      current_distance = vecmag(current_location - vehicle_location);
      double closest_distance_so_far = current_distance;
      
      for (unsigned int i = 0; i < path.numPoints; i++)
	{
	  current_location[0] = path.n[i];
	  current_location[1] = path.e[i];
	  current_distance = vecmag(current_location - vehicle_location);
	  if (current_distance < closest_distance_so_far)
	    {
	      closest_distance_so_far = current_distance;
	      closest_point_so_far = i;
	    }
	}
      cout << closest_point_so_far << " at dist=" 
	   << closest_distance_so_far << endl;
      path.currentPoint = closest_point_so_far;
    }
  
  //###
  //printf("(first, cur, last, dist) = (%i, %i, %i, %f)", 0, path.currentPoint, path.numPoints, current_distance);
  //cout << endl;
  //###
  */
}



/**
 * Moves the current point a certain distance along a dense path.
 * Positive is further along the path, Negative is backward along the path.
 * If either end of the path is reached before the distance goal, the function
 * stops.
 * @return the actual distance along the path that the point was moved
 */
double MoveCurrentPoint(pathstruct & path, double distance_goal)
{
  if (distance_goal < 0)
    cerr << "ERROR: MoveCurrentPoint can't move points backwards!" << endl;

  //assuming dist is positive
  unsigned int last_point_index = path.numPoints-1; // to avoid confusion
  double dist_so_far = 0, previous_dist = -1;

  // get to the first point that's too far
  unsigned int cur_point = path.currentPoint;
  while(cur_point < last_point_index && dist_so_far < distance_goal)
    {
      previous_dist = dist_so_far;
      dist_so_far += DistanceBetweenPoints(path, cur_point, cur_point + 1);
      cur_point++;
    }

  // see if the cur_point or the one before it was best
  if (fabs(dist_so_far - distance_goal) < fabs(previous_dist - distance_goal))
    {
      path.currentPoint = cur_point;
      return dist_so_far;
    }
  else
    {
      path.currentPoint = cur_point - 1;
      return previous_dist;
    }
}



/**
 * Computes the SIGNED distance between two points on a path.  If point2 is further
 * along the path than point1, distance will be positive; if point1 is further, it will
 * be negative.
 */
double DistanceBetweenPoints(pathstruct & path, unsigned int point1, unsigned int point2)
{
  unsigned int last_point_index = path.numPoints-1; // to avoid confusion

  if (point1 < 0 || point2 < 0 
      || point1 > last_point_index || point2 > last_point_index)
    {
      printf("first point=0 last_point_index=%d point1=%d point2=%d",
	     last_point_index, point1, point2);
      cout.flush();
      cerr << "ERROR in DistanceBetweenPoints: Out of bounds." << endl;
    }

  if (point1 == point2)
    return 0;
  else
    {
      double dist_so_far = 0;
      int distance_sign = ((point2 > point1)? 1 : -1);
      if (distance_sign == -1)
	{
	  unsigned int temp = point1; point1 = point2; point2 = temp;
	}
      // now we're working under the assumption that point1 < point2, and
      // they're both in the bounds
      vector<double> location1(2),location2(2);
      for (unsigned int cur_point = point1; cur_point < point2; ++cur_point)
	{
	  location1 = GetLocation(path, cur_point);
	  location2 = GetLocation(path, cur_point+1);
	  dist_so_far += vecmag(location2 - location1);
	}
      return (distance_sign * dist_so_far);
    }
}



/**
 * Chops long paths into shorter paths to save resources.
 * From the path.currentPoint, Chop path will step back a total distance
 *  of chop_behind or the distance to the beginning of the path, whichever
 * is smaller, and throw away whatever part of the path is before that
 * point. Similarly, ChopPath will step forward a distance of chop_ahead
 * or the distance to the end of the path, whichever is smaller, and throw
 * the rest of the path away.
 * 
 * @param path the path to be chopped
 * @param chop_behind the distance backwards along the path from the current
 *           point that the path will be chopeed off at
 * @param chop_ahead the distance forwards along the path from the current
 *           point that the path will be chopeed off at
 */
pathstruct ChopPath(const pathstruct & path, 
		    const double chop_behind, const double chop_ahead)
{
  // 
  // Changes:
  //   2-11-2005, Jason Yosinski, created
  //
  // Function:
  //   From the path.currentPoint, Chop path will step
  //   back a total distance of chop_behind or the distance to the beginning
  //   of the path, whichever is smaller, and throw away whatever part of
  //   the path is before that point.
  //   Similarly, ChopPath will step forward a distance of chop_ahead or the
  //   distance to the end of the path, whichever is smaller, and throw the
  //   rest of the path away.
  //
  

  // Find the index of the furthest back point
  int furthest_back_index = path.currentPoint;
  if (furthest_back_index > 0)
    {
      furthest_back_index--; // step back one to account for the fact that we
                             // may actually be in the middle of a segment
      double dist_back_sofar = 0;
      vector<double> point1(2), point2(2); // (N,E) format
      while(furthest_back_index > 0 && dist_back_sofar < chop_behind)
	{
	  furthest_back_index--;
	  point2[0] = path.n[furthest_back_index+1];
	  point2[1] = path.e[furthest_back_index+1];
	  point1[0] = path.n[furthest_back_index];
	  point1[1] = path.e[furthest_back_index];
	  dist_back_sofar += vecmag(point2-point1);
	}
    }


  // Find the index of the furthest ahead point
  int furthest_ahead_index = path.currentPoint;
  int last_point_index = path.numPoints-1; // to avoid confusion
  if (furthest_ahead_index < last_point_index)
    {
      furthest_ahead_index++; // step ahead one to account for the fact that we
                             // may actually be in the middle of a segment
      double dist_ahead_sofar = 0;
      vector<double> point1(2), point2(2); // (N,E) format
      while(furthest_ahead_index < last_point_index
	    && dist_ahead_sofar < chop_ahead)
	{
	  furthest_ahead_index++;
	  point2[0] = path.n[furthest_ahead_index];
	  point2[1] = path.e[furthest_ahead_index];
	  point1[0] = path.n[furthest_ahead_index-1];
	  point1[1] = path.e[furthest_ahead_index-1];
	  dist_ahead_sofar += vecmag(point2-point1);
	}
    }


  //printf("(%d, %d, %d, %d, %d)", 0, furthest_back_index, path.currentPoint,
  //	 furthest_ahead_index, last_point_index);
  //cout << endl;

  // create and store a new path using furthest_back_index and 
  // furthest_ahead_index
  pathstruct ret = emptyPath();
  if (furthest_ahead_index > last_point_index
      || furthest_ahead_index < 0
      || furthest_back_index < 0
      || furthest_back_index > last_point_index)
    {
      printf("furthest_ahead_index=%d last_point=%d furthest_back=%d",
	     furthest_ahead_index, last_point_index, furthest_back_index);
      cout.flush();
      cerr << "ERROR in Chop Path:  this really shouldn't be happening..." << endl;
    }
  for (int i = furthest_back_index; i <= furthest_ahead_index; i++)
    addToPath(ret, path.e[i], path.n[i],
	      path.ve[i], path.vn[i], path.ae[i], path.an[i]);
  int chopped_off_beginning = furthest_back_index;
  ret.currentPoint = path.currentPoint - chopped_off_beginning;


  // returtn that bad boy
  return ret;
}



/**
 * Densifies sparse paths.
 * This script creates a dense path from a sparse path.  I use the
 * accelerations and velocities specified in the given path to determine the
 * course of the path between path points.  I assume the path will be
 * contiguous using the above information. ie, the velocity at point 2 will
 * be achieved by maintaining the acceleration specified at point 1 until I
 * get to point 2.  Under these assumptions, the path has a continuous
 * time derivative and the second derivative is piecewise continuous.  All I
 * have to do is fill in the spaces between points with more points and fill
 * out the already smooth path.  If the path sent to this function is not
 * smooth then this function will fail (probably silently).
 *
 * Note, the density of the dense path output is specified in RddfPathGen.hh
 * as DENSITY.
 * 
 * @param path_sparse the sparse path to be densified
 */
pathstruct DensifyPath(const pathstruct & path_sparse)
{
  /* Ben Pickett

     this script creates a dense path from a sparse path.  I use the
     accelerations and velocities specified in the given path to determine the
     course of the path between path points.  I assume the path will be
     contiguous using the above information. ie, the velocity at point 2 will
     be achieved by maintaining the acceleration specified at point 1 until I
     get to point 2.  Under these assumptions, the path has a continuous
     time derivative and the second derivative is piecewise continuous.  All I
     have to do is fill in the spaces between points with more points and fill
     out the already smooth path.  If the path sent to this function is not
     smooth then this function will fail (probably silently).
  */

  double step = DENSITY; // defined in RddfPathGen.hh

  pathstruct path_dense = emptyPath();
  int N = path_sparse.numPoints;

  if (N < 2)
    return path_sparse;

  for(int i = 0; i < N-1; i++)
    {
      //###
      //cout << "In main DensifyPath loop, i = " << i << endl;
      //###
      vector<double> point0(2); point0[0] = path_sparse.e[i]; point0[1] = path_sparse.n[i];
      vector<double> velocity0(2); velocity0[0] = path_sparse.ve[i]; velocity0[1] = path_sparse.vn[i];
      vector<double> accel0(2); accel0[0] = path_sparse.ae[i]; accel0[1] = path_sparse.an[i];
      double speed0 = vecmag(velocity0);
      double accelmag = vecmag(accel0);
      vector<double> point1(2); point1[0] = path_sparse.e[i+1]; point1[1] = path_sparse.n[i+1];
      
      vector<double> normvel0(2);
      if (speed0 != 0)
	normvel0 = velocity0 / speed0;  // normalize the velocity
      else
	normvel0 = velocity0;  // velocity0 is (0,0)
      
      vector<double> normaccel0(2), acceltan(2), accelperp(2);
      if (accelmag != 0)
	{
	  normaccel0 = accel0 / accelmag;  // normalize the acceleration
	  // component of accel in tangential direction
	  acceltan = accelmag * vecdot(normvel0, normaccel0) * normvel0;
	  // component of accel in perpendicular direction
	  accelperp = accel0 - acceltan;
	}
      else
	{
	  normaccel0 = accel0;  // accel0 is (0,0)
	  // component of accel in tangential direction (is (0,0))
	  acceltan = accel0;
	  // component of accel in perpendicular direction (is (0,0))
	  accelperp = accel0;
	}
      
      
      
      if (vecmag(accelperp) < 1e-9)  // 1e-9 is a tolerance to figure out if path is straight or not
	{
	  // this is a straght path segment
	  double length = vecmag(point1 - point0);
	  double sign = vecdot(normvel0, normaccel0);
	  for (double ds = 0; ds < length; ds += step)
	    {
	      vector<double> point = point0 + normvel0 * ds;
	      
	      // sign: direction of accel. +1 means forward, -1 means backwards.
	      // this works because normvel0 and normaccel0 have unit magnitudes and are
	      // parallel.  If accelmag == 0 then either equation is valid.
	      
	      addToPath(path_dense, point[0], point[1],
			normvel0 * sqrt(pow(speed0,2) + 2 * sign * accelmag * ds),
			accel0);
	    }
	}
      else
	{
	  // this is a curved path segment
	  double radius = (speed0 * speed0) / vecmag(accelperp);
	  vector<double> center = point0 + accelperp * radius / vecmag(accelperp);
	  vector<double> ray0 = point0 - center;
	  vector<double> ray1 = point1 - center;
	  
	  double theta_min = atan2(ray0[1], ray0[0]);
	  double theta_max = atan2(ray1[1], ray1[0]);
	  double theta_step = step / radius;
	  
	  // force sweep angle to be between -pi and pi
	  double sweep = atan2(sin(theta_max-theta_min), cos(theta_max-theta_min));
	  
	  if (sweep < 0) theta_step *= -1;  // case for decreasing theta
	  
	  for (double theta = theta_min; fabs(theta_min+sweep-theta) > fabs(theta_step); theta += theta_step)
	    {
	      //###
	      //printf("theta=%f  theta_min=%f  sweep=%f  theta_step=%f  theta_min+sweep-theta=%f",
	      //       theta, theta_min, sweep, theta_step, theta_min+sweep-theta);
	      //cout << endl;
	      //###
	      vector<double> ray(2); ray[0] = cos(theta); ray[1] = sin(theta);
	      vector<double> point = center + radius * ray;
	      
	      double sign;
	      if (sweep > 0) sign = 1; else sign = -1;
	      
	      // add points along the circular arc
	      vector<double> normray = ray / vecmag(ray);
	      vector<double> normtang(2); normtang[0] = cos(theta + sign * PI/2); normtang[1] = sin(theta + sign * PI/2);
	      addToPath(path_dense, point[0], point[1],
			speed0 * normtang,
			-accelmag * normray);
	      
	    } 
	}
    }
  
  /* for debugging trajDFE functinality */
  TrajDFE(path_dense);
  /* end debug*/
  
  return path_dense;
}



/**
 * makes a spline from loca
 */
pathstruct SplineFromHereToThere(double here_n, double here_n_dot,
				 double here_e, double here_e_dot,
				 double there_n, double there_n_dot,
				 double there_e, double there_e_dot,
				 double density)
{
  //spline
  double cn_0, cn_1, cn_2, cn_3, ce_0, ce_1, ce_2, ce_3;
  /*
    for a * [cn_0; cn_1; cn_2; cn_3] = [here_n; here_n_dot; there_n; there_n_dot]
a =

  1  0  0  0
  0  1  0  0
  1  1  1  1
  0  1  2  3

inv(a) =

   1   0   0   0
   0   1   0   0
  -3  -2   3  -1
   2   1  -2   1
  */
  cn_0 = here_n;
  cn_1 = here_n_dot;
  cn_2 = -3*here_n + -2*here_n_dot + 3*there_n + -1*there_n_dot;
  cn_3 = 2*here_n + 1*here_n_dot + -2*there_n + 1*there_n_dot;

  ce_0 = here_e;
  ce_1 = here_e_dot;
  ce_2 = -3*here_e + -2*here_e_dot + 3*there_e + -1*there_e_dot;
  ce_3 = 2*here_e + 1*here_e_dot + -2*there_e + 1*there_e_dot;

  //###
  //   printf("there_n=%f, here_n=%f, there_e=%f, here_e=%f",there_n, here_n, there_e, here_e); cout << endl;
  //   cout << "density is " << density << endl;
  //###
  double total_distance = hypot(there_n-here_n, there_e-here_e);
  unsigned int divisions = (int)(total_distance/density + 1.0); //+1 to round up
  double step_size = 1.0/divisions;
  //###
  //   cout << "divisions in frac form is " << total_distance/density + 1.0 << endl;
  //   printf("total_distance=%f, divisions=%d, step_size=%f",total_distance, divisions, step_size); cout << endl;
  //###

  double t, this_n, this_e;
  pathstruct ret = emptyPath();
  for (unsigned int i = 0; i < divisions; ++i) // never quite get to last point
    {
      t = i * step_size;
      this_n = cn_0 + cn_1*t + cn_2*t*t + cn_3*t*t*t;
      this_e = ce_0 + ce_1*t + ce_2*t*t + ce_3*t*t*t;
      addToPath(ret, this_e, this_n, 1234.0, 1234.0, 1234.0, 1234.0);
    }

  return ret;
}



/**
 * Defunct function.  See comment for StoreWholePath.
 */
void StorePathFragment(CTraj & traj, const corridorstruct & cor)
{
  cout << "Still need to write store path fragment" << endl;
}



/* not currently implemented
corridorstruct file2corridor(ifstream & file)
{
  // 
  // function corridor = file2corridor(file)
  //
  // Changes: 
  //   12-7-2004, Jason Yosinski, created (in Matlab, some code from plot_corridor.m)
  //   12-29-2004, Jason Yosinski, ported to C++
  //
  // Function:
  //   This function reads a *.bob format RDDF file and outputs the
  //   corrosponding corridorstruct for use with functions such as
  //   Path_From_Corridor
  //
  // Input:
  //   ifstream file - must be *.bob format
  //
  // Output:
  //   corridor in the standard corridorstruct format
  //
  // Usage example:
  //   char* filename = "bobfile.bob";
  //   ofstream file(filename);
  //   corridorstruct cor = file2corridor(file);

  // read in values from the file
  // [type,eastings,northings,radii,vees] = 
  //     textread(file, '%s%f%f%f%f', 'commentstyle', 'shell');

  // corridor = [eastings, northings, radii, vees];
  cout << "Need to finish file2corridor!  Returning blank corridor instead!" << endl;

  corridorstruct ret;
  return ret;
}
*/



/**
 * Computes the length limited curve characteristics.  For a 3 point corridor
 * segment, computes the degrees eclipsed by the accompanying turn, and the
 * location of the start and end of the turn.
 *
 * @return the radius of the curve
 * @param theta half of the exterior angle between the two line segments 
 *          joining the three points, MEASURED IN RADIANS.
 * @param start_x the x of the point at the start of the turn
 * @param start_y the y of the point at the start of the turn
 * @param corridor_segment the 3 point corridor segment that the turn
 *     must fit in.
 */
double length_limited_curve(double & theta,
			    double & start_x,
			    double & start_y,
			    corridorstruct corridor_segment)
{
  //
  // function [radius, theta, start_x, start_y] = length_limited_curve(corridor_segment)
  //
  // Changes:
  //   12-6-2004, Benjamin Pickett, created (in Matlab)
  //   12-28-2004, Jason Yosinski, ported to C++
  //
  // Function:
  //   Update: now returns radius. 
  //   old stuff from matlab:
  //   This function takes a corridor segment consisting of 3 points
  //       corridor_segment = [x1 y1 width1;
  //                           x2 y2 width2;
  //                           x3 y3 width3]
  // and calculates the circular arc with a radius defined by creating a
  // smooth curve (continuous first derivative) such that:
  //       1.  The arc starts tangent to the centerline between point1 and
  //           point2.
  //       2.  The arc ends tangent to the centerline between point2 and
  //           point3.
  //       3.  The start and end points are equidistant from the corner of the
  //           corridor.
  //       4.  The distance from (3) is defined to be the minimum of the two
  //           leg lengths (point1 to point2 and point2 to point3).
  //
  //   The function returns the radius of the arc in question, the x and y
  //   locations of the start of the arc (start_x and start_y), and theta.
  //   Theta is half of the exterior angle between the two line segments 
  //   joining the three points, MEASURED IN RADIANS.
  //
  // Usage example:
  //   [radius, theta, start_x, start_y] = width_limited_curve(corridor_segment)
  //
  
  // extract points from corridor_segment
  vector<double> point1(2); point1[0] = corridor_segment.e[0]; point1[1] = corridor_segment.n[0];
  vector<double> point2(2); point2[0] = corridor_segment.e[1]; point2[1] = corridor_segment.n[1];
  vector<double> point3(2); point3[0] = corridor_segment.e[2]; point3[1] = corridor_segment.n[2];

  // vectors from point1 to point2 and point2 to point3
  vector<double> ray12 = point2 - point1;
  vector<double> ray23 = point3 - point2;

  // take the half the shorter of the two distances to the corner
  double d = min(vecmag(ray12), vecmag(ray23)) / 2;

  // use half angle formula and dot product to find cos
  // ( cos = u*v/|u||v| )
  theta = .5 * acos(vecdot(ray12, ray23) / (vecmag(ray12)*vecmag(ray23)));
  
  double radius = d / tan(theta);

  // distance d from corner in the direction of v1
  vector<double> start = point2 - (d*ray12)/vecmag(ray12);
  start_x = start[0];
  start_y = start[1];

  return radius;
}



/**
 * Computes the width limited curve characteristics.  For a 3 point corridor
 * segment, computes the degrees eclipsed by the accompanying turn, and the
 * location of the start and end of the turn.
 *
 * @return the radius of the curve
 * @param theta half of the exterior angle between the two line segments 
 *          joining the three points, MEASURED IN RADIANS.
 * @param start_x the x of the point at the start of the turn
 * @param start_y the y of the point at the start of the turn
 * @param corridor_segment the 3 point corridor segment that the turn
 *     must fit in.
 */
double width_limited_curve(double & theta,
			   double & start_x,
			   double & start_y, 
			   corridorstruct corridor_segment)
{
  // 
  // function [radius theta start_x start_y] = width_limited_curve(corridor_segment)
  //
  // Changes: 
  //   12-6-2004, Jason Yosinski, created (in Matlab)
  //   12-26-2004, Jason Yosinski, ported to C++
  //
  // Function:
  //   This function returns the radius
  //   It takes a corridor segment consisting of 3 points
  //       corridor_segment = [x1 y1 width1;
  //                           x2 y2 width2;
  //                           x3 y3 width3]
  //   and calculates the circular arc with the largest possible radius such
  //   that:
  //       1. The arc starts along and tangent to the centerline between
  //           point1 and point2
  //       2. The arc ends along and tangent to the centerline between point2
  //           and point3
  //       3. The arc just touches the inside corner of the two corridors
  //           (meaning if it were any larger, the path would go outside the
  //           corridor)
  //   Note that the widths are radii rather than diameters, that is, the
  //   corridor is actually 20 meters wide from outside edge to outside edge
  //   if width = 10 meters
  //
  //   The function returns the radius of the arc in question, the x and y
  //   locations of the start of the arc (start_x and start_y), and theta.
  //   Theta is half of the exterior angle between the two line segments 
  //   joining the three points, MEASURED IN RADIANS.
  //
  // Usage example:
  //   [radius theta start_x start_y] = width_limited_curve(corridor_segment)
  // extract points and widths from corridor_segment

  // useful quantities to define for manipulation
  vector<double> point1(2); point1[0] = corridor_segment.e[0]; point1[1] = corridor_segment.n[0];
  vector<double> point2(2); point2[0] = corridor_segment.e[1]; point2[1] = corridor_segment.n[1];
  vector<double> point3(2); point3[0] = corridor_segment.e[2]; point3[1] = corridor_segment.n[2];
  double width12 = corridor_segment.width[0];
  double width23 = corridor_segment.width[1];

  // rays from point1 to point2 and point2 to point3
  vector<double> ray12 = point2 - point1;
  vector<double> ray23 = point3 - point2;
//   cout << "\nDEBUG point1 = "; vecprint(point1);
//   cout << "\nDEBUG point2 = "; vecprint(point2);
//   cout << "\nDEBUG point3 = "; vecprint(point3);

  // min_width is the limiting corridor width, namely the smaller of the first
  // and second corridor widths
  double min_width = min(width12, width23);

  // calculate theta (and return it)
  theta = .5 * acos(vecdot(ray12, ray23) / (vecmag(ray12)*vecmag(ray23)));
//   cout << "\nDEBUG ray12 = "; vecprint(ray12);
//   cout << "\nDEBUG ray23 = "; vecprint(ray23);
//   cout << "\nDEBUG acos of " << vecdot(ray12, ray23) << " / " << (vecmag(ray12)*vecmag(ray23));

  // calculate the radius
  double radius = min_width / (1 - cos(theta));

  // calculate b, which is the distance from the intersection of the two
  // centerlines to the start of the curve
  double b = radius * tan(theta);
  
  // calculate the start of curve point
  vector<double> start = point2 - (ray12 / vecmag(ray12)) * b;
  
  // return the start point and radius
  start_x = start[0];
  start_y = start[1];
  //  cout << "radius of width limited curve: " << radius << endl;
  //  cout << "and corresponding angle: " << theta*180/PI << endl;
  return radius;
}



/**
 * Figures acceleration from radius and velocity.
 * This function takes a corridor segment consisting of 3 points
 *        corridor_segment = [x1 y1 width1;
 *                            x2 y2 width2;
 *                            x3 y3 width3]
 * and a predefined radius of curvature for the desired path.  The function
 * returns the centripetal acceleration required to begin traveling in a
 * circular arc from a point on the first leg of path segment (x1,y1) to
 * (x2,y2).  The acceleration returned by this function is designed to be
 * one of the components needed for defining a segment of path to send to
 * path following.
 *
 * @return the acceleration vector
 * @param corridor_segment the 3 point corridor segment to be used for calculation
 * @param r radius
 * @param v velocit at start of turn
 */
vector<double> curve2accel(corridorstruct corridor_segment,
			   double r,
			   double v)
{
  //
  // function accel = curve2accel( corridor_segment, radius, speed)
  //
  // Changes:
  //   12-7-2004, Benjamin Pickett, created (in Matlab)
  //   12-28-2004, Jason Yosinski, ported to C++
  //
  // Function:
  //   This function takes a corridor segment consisting of 3 points
  //       corridor_segment = [x1 y1 width1;
  //                           x2 y2 width2;
  //                           x3 y3 width3]
  // and a predefined radius of curvature for the desired path.  The function
  // returns the centripetal acceleration required to begin traveling in a
  // circular arc from a point on the first leg of path segment (x1,y1) to
  // (x2,y2).  The acceleration returned by this function is designed to be
  // one of the components needed for defining a segment of path to send to
  // path following.
  //
  // Usage example:
  //   accel = curve2accel( corridor_segment, radius, speed)
  
  // extract points from corridor_segment
  vector<double> point1(2); point1[0] = corridor_segment.e[0]; point1[1] = corridor_segment.n[0];
  vector<double> point2(2); point2[0] = corridor_segment.e[1]; point2[1] = corridor_segment.n[1];
  vector<double> point3(2); point3[0] = corridor_segment.e[2]; point3[1] = corridor_segment.n[2];

  // vectors from point1 to point2 and point2 to point3
  vector<double> ray12 = point2-point1;
  vector<double> ray23 = point3-point2;

  //project ray23 onto ray12
  vector<double> projray23ray12 = ray12 * vecdot(ray12, ray23) / pow(vecmag(ray12),2);

  //normalize orthogonal component of v23 and scale by |a| = v^2/r
  vector<double> accel = (pow(v, 2)/r) * (ray23-projray23ray12) / vecmag(ray23-projray23ray12);

  return accel;
}



/**
 * Generates a path from a corridor.  This function is called exactly once
 * by RddfPathGen, to generate the intial complete sparse path.
 *
 * @return sparse path
 * @param corridor the corridor to be used in generating the path
 */
pathstruct Path_From_Corridor(corridorstruct corridor)
{
  // 
  // Changes: 
  //   12-6-2004, Jason Yosinski, created (in Matlab)
  //   12-26-2004, Jason Yosinski, ported to C++
  //
  // Function:
  //   This function generates a somewhat optimal (max speed for any given
  //   segment is within a factor of sqrt(2) of the optimal max speed I
  //   think...) path from a corridor
  //
  // Input:
  //   struct corridorstruct
  //   {
  //     // number of points in corridor.  last point is endpoint.
  //     int points;
  //     
  //     // eastings, northings, corridor widths, rddf imposed speed limits.
  //     // width[1] is the width of the corridor between (e[1], n[1]) and 
  //     // (e[2], n[2]).  speedLimit[1] is the speed limit imposed by the
  //     // rddf on the first corridor segment.  width[points] and
  //     // speedLimit[points] are irrelevant.
  //     listofsomesort e, n, width, speedLimit;
  //   }
  //
  // Output:
  //   struct pathstruct
  //   {
  //     // number of points in the path.  The last point is the end point
  //     // of the path, so ve, vn, ae, and an are irrelevant at the last
  //     // point
  //     int points;
  //     
  //     // eastings, northings, eastward velocity, northward velocity
  //     // eastward accel, northward accel
  //     listofsomesort e, n, ve, vn, ae, an;
  //   }
  //
  // Usage example:
  //   see function prototype
  //
  
  
  // get number of points in corridor
  int N = corridor.numPoints;

  // create a blank path
  pathstruct path = emptyPath();
  
  
  // figure out if some l00zer passed us a messed up corridor with zero or one
  // points in it
  if (N == 0)
    {
      cout << "You fool - the corridor passed to path_from_corridor is empty!" << endl;
      return path;
    }
  
  else if (N == 1)
    {
      // corridor is just a single point, so i guess our path says we're
      // already there! (path_from_corridor is passive-agressive)

      addToPath(path, corridor.e[0], corridor.n[0], 0, 0, 0, 0, corridor.width[0]);
      return path;
    }
  
  else if (N == 2)
    {
      // corridor is two points, so our path is a straight line
      // connecting them
      
      // direction (get, then normalize)
      vector<double> direction(2);
      direction[0] = corridor.e[1] - corridor.e[0];
      direction[1] = corridor.n[1] - corridor.n[0];
      direction = direction / vecmag(direction);
      // speedLimit
      double speedLimit = corridor.speedLimit[0];

      addToPath(path, corridor.e[0], corridor.n[0], direction[0] * speedLimit,
		direction[1] * speedLimit, 0, 0, corridor.width[0]);
      addToPath(path, corridor.e[1], corridor.n[1], 0, 0, 0, 0, corridor.width[0]);
    }
  
  else
    {
      // *whew*... finally... a challenge - a corridor with at least 2 points
      // in it!
      
      // do first path segment (starting location to halfway down first segment)
      // direction (get, then normalize)
      vector<double> direction(2);
      direction[0] = corridor.e[1] - corridor.e[0];
      direction[1] = corridor.n[1] - corridor.n[0];
      direction = direction / vecmag(direction);
      // speedLimit
      double speedLimit = corridor.speedLimit[0];
      
      // store first path segment
      addToPath(path, corridor.e[0], corridor.n[0], direction[0] * speedLimit,
		direction[1] * speedLimit, 0, 0, corridor.width[0]);
      //###
      //cout << "corridor.e " << corridor.e[0] << "  "
      //	   << "corridor.n " << corridor.n[0] << "  "
      //	   << "vx " << direction[0] * speedLimit << "  "
      //	   << "vy " << direction[1] * speedLimit << endl;
      //cout << "did first path segment, path currently is:" << endl;
      //printPath(path);
      //cout << endl;
      //###

      // do rest of path segments
      for (int i = 1; i <= N-2; i++)
	{
	  // our story starts with a particular i.  we now start along the
	  // corridor at a position equal to the midpoint of the "ith" segment
	  
	  // some useful stuff...

	  //###
	  //cout << "got here, i =" << i << endl;
	  //###

	  corridorstruct corridor_segment;
	  for (int j = 0; j <= 2; j++)
	    {
	      corridor_segment.numPoints = 3;
	      corridor_segment.e.push_back(corridor.e[i+j-1]);
	      corridor_segment.n.push_back(corridor.n[i+j-1]);
	      corridor_segment.width.push_back(corridor.width[i+j-1]);
	      corridor_segment.speedLimit.push_back(corridor.speedLimit[i+j-1]);
	    }
	  
	  //###
	  //cout << "constructed corridor_segment" << endl;
	  //###
	  
	  // extract points from corridor_segment
	  vector<double> point1(2); point1[0] = corridor_segment.e[0];
	  point1[1] = corridor_segment.n[0];
	  vector<double> point2(2); point2[0] = corridor_segment.e[1];
	  point2[1] = corridor_segment.n[1];
	  vector<double> point3(2); point3[0] = corridor_segment.e[2];
	  point3[1] = corridor_segment.n[2];
	  
	  // vectors from point1 to point2 and point2 to point3
	  vector<double> ray12 = point2-point1;
	  vector<double> ray23 = point3-point2;
	  
	  // directions (normalized rays)
	  
	  vector<double> dir12 = ray12 / vecmag(ray12);
	  vector<double> dir23 = ray23 / vecmag(ray23);
	  
	  // speed limits imposed by the rddf
	  double sl12 = corridor_segment.speedLimit[0];
	  double sl23 = corridor_segment.speedLimit[1];
	  
	  // if the three points are in a line, then we just add point2 to the path
	  if (vecmagsquared(dir23-dir12) < .0000001)
	    {
	      // points are in a straight line, so add point 2 and continue on
	      addToPath(path, point2[0], point2[1], dir23 * sl23, 0, 0,
			fmax(corridor_segment.width[0], corridor_segment.width[1]));
	    }
	  else
	    {	  
	      // figure out whether the length of the segment or the width of the
	      // corridor limits the curvature
	      double radiusW, thetaW, start_xW, start_yW;
	      double radiusL, thetaL, start_xL, start_yL;
	      
	      //###
	      //cout << "here before curves" << endl;
	      //###
	      // get width limited data
	      radiusW = width_limited_curve(thetaW, start_xW, start_yW, corridor_segment);
	      
	      //###
	      //cout << "width done, radius = " << radiusW << endl;
	      //###
	      // get length limited data
	      radiusL = length_limited_curve(thetaL, start_xL, start_yL, corridor_segment);
	      //###
	      //cout << "length done, radius = " << radiusL << endl;
	      //###
	      
	      //###
	      //cout << endl;
	      //printf("[i, radiusL, radiusW] = [%d, %f, %f]", i, radiusL, radiusW);
	      //cout << endl;
	      //disp('[i, radiusL, radiusW] =');
	      //disp([i, radiusL, radiusW]);
	      //###
	      
	      // we now have two curves, each limited by a different factor.  we want
	      // to use the one with the stricter limitation, namely the one with the
	      // smaller radius of curvature.
	      
	      //###
	      //rads = [rads min(radiusL, radiusW)];
	      //###
	      if (radiusL < radiusW)
		{
		  // the limiting factor is the LENGTH of one of the corridor
		  // segments (the shorter one), so next we'll figure out which
		  // segment it was...
		  
		  // velocity at beginning of curve
		  vector<double> vel = dir12 * min(sl12, sl23);

		  if (vecmag(point2 - point1) < vecmag(point3 - point2))
		    {
		      // the first segment limited the curve, so the path continues
		      // here with a curve
		      // cout << "1adding point at " << start_xL << ", " << start_yL << endl;
		      addToPath(path, start_xL, start_yL, vel,
				curve2accel(corridor_segment, radiusL, min(sl12, sl23)),
				fmax(corridor_segment.width[0], corridor_segment.width[1]));
		      
		      // once that curve is done, the path continues on in a straight
		      // line to at least the next midpoint
		      // end of curve point (which != next midpoint)
		      vector<double> end_of_curve_point = point2 + dir23 * radiusL * tan(thetaL);
		      // cout << "2adding point at " << end_of_curve_point[0] << ", " 
		      //   << end_of_curve_point[1] << endl;
		      addToPath(path, end_of_curve_point[0], end_of_curve_point[1],
				dir23 * min(sl12, sl23), 0, 0,
				corridor_segment.width[1]);
		    }
		  else
		    {
		      // the second segment limited the curve, so the path continues
		      // here first with a straght segment
		      vector<double> start_of_curve_point = point1 + .5 * ray12;
		      //cout << "3adding point at " << start_of_curve_point[0] << ", " 
		      //   << start_of_curve_point[1] << endl;
		      addToPath(path, start_of_curve_point[0], start_of_curve_point[1],
				dir12 * sl12, 0, 0,
				corridor_segment.width[0]);
		      
		      // then comes the curve, which ends at the next midpoint
		      //cout << "4adding point at " << start_xL << ", " << start_yL << endl;
		      addToPath(path, start_xL, start_yL, dir12 * min(sl12, sl23),
				curve2accel(corridor_segment, radiusL, min(sl12, sl23)),
				fmax(corridor_segment.width[0], corridor_segment.width[1]));
		    }
		}
	      else
		{
		  // the limiting factor is the WIDTH of the corridor, so we need a
		  // straight segment, then a curve, then another straight piece.
		  vector<double> straight_start = point1 + .5 * ray12;
		  //cout << "5adding point at " << straight_start[0] << ", " 
		  //   << straight_start[1] << endl;
		  addToPath(path, straight_start[0], straight_start[1],
			    dir12 * sl12, 0, 0,
			    corridor_segment.width[0]);
		  //cout << "6adding point at " << start_xW << ", " << start_yW << endl;
		  addToPath(path, start_xW, start_yW, dir12 * min(sl12, sl23),
			    curve2accel(corridor_segment, radiusW, min(sl12, sl23)),
			    fmax(corridor_segment.width[0], corridor_segment.width[1]));
		  straight_start = point2 + dir23 * radiusW * tan(thetaW);
		  //cout << "7adding point at " << straight_start[0] << ", " << straight_start[1] << endl;
		  addToPath(path, straight_start[0], straight_start[1], dir23 * sl23,
			    0, 0, corridor_segment.width[1]);
		}
	    }
	}
    }
  
  
  // do last actual path segment (straight line from midpoint of last
  // corridor segment to the last point in the corridor)

  // second last point
  vector<double> point1(2); point1[0] = corridor.e[N-2]; point1[1] = corridor.n[N-2];
  // last point
  vector<double> point2(2); point2[0] = corridor.e[N-1]; point2[1] = corridor.n[N-1];
  // ray from second last point to last point
  vector<double> ray12 = point2 - point1;
  // speed limit of last stretch of corridor
  double sl12 = corridor.speedLimit[N-2];
  // midpoint of last stretch of corridor
  vector<double> last_midpoint = .5 * (point1 + point2);

  //###
  //cout << "Two more addToPath's to go..." << endl;
  //###
  addToPath(path, last_midpoint[0], last_midpoint[1],
	    ray12 / vecmag(ray12) * sl12, 0, 0, corridor.width[N-2]);

  
  // tack on a final point to the path (so everyone else knows how long the
  // last path segment should be
  addToPath(path, point2[0], point2[1], 0, 0, 0, 0, corridor.width[N-1]);

  return path;
}
