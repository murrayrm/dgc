#ifndef FUSIONMAPPER_HH
#define FUSIONMAPPER_HH

#include <unistd.h>
#include <pthread.h>

#include "MapdeltaTalker.h"
#include "DGCutils"
//#include "fusionMapperTabSpecs.hh"
//#include "TabClient.h"

#include "CMapPlus.hh"
#include "rddf.hh"
#include "herman.hh"
#include "CCorridorPainter.hh"
//#include "CCostPainter.hh"
#include "MapConstants.h"
#include "RoadPainter.hh"
#include "Road.hh"
#include "AliceConstants.h"
#include "TrajTalker.h"
#include "CTerrainCostPainter.hh"
#include "CCostFuser.hh"
#include "StateClient.h"
#include "interface_superCon_fmap.hh"
#include "SuperConClient.hh"

#define MAX_DELTA_SIZE 999999
#define MAX_ROAD_SIZE 50

#define NUM_LADAR_LAYERS 7
#define NUM_STEREO_LAYERS 2
#define NUM_ELEV_LAYERS (NUM_LADAR_LAYERS + NUM_STEREO_LAYERS)

#define DEFAULT_WING_WIDTH 2.0

using namespace std;

//Sparrow Display Functions
int user_quit(long arg);
int user_pause(long arg);
int user_reset(long arg);
int user_change(long arg);
int user_repaint(long arg);
int user_reevaluate(long arg);
int user_save(long arg);
int user_clear(long arg);


struct FusionMapperOptions {
  int optRoad, optSupercon;
  int optSNKey;
  char configFilename[256];
  int sendRDDF;
  double processingRate;
  double deltaRate;
  double optWingWidth;
  int optKF;
};


class FusionMapperStatusInfo {
private:
  unsigned long long startProcessTime;
  unsigned long long endProcessTime;
  unsigned long long endLockTime;
  unsigned long long endStage1Time;
  unsigned long long endStage2Time;

public:
  int numMsgs;
  int msgSize;
  double msgSizeAvg;
  double processTime;
  double processTimeAvg;
  double lockTime;
  double lockTimeAvg;
  double stage1Time;
  double stage1TimeAvg;
  double stage2Time;
  double stage2TimeAvg;
  double remainingTime;
  double remainingTimeAvg;

  FusionMapperStatusInfo() {reset();};
  ~FusionMapperStatusInfo() {};

  void startProcessTimer() {
    DGCgettime(startProcessTime);
  };
	
  void endLockTimer() {
	  DGCgettime(endLockTime);
  };

  void endStage1Timer() {
	  DGCgettime(endStage1Time);
  };

  void endStage2Timer() {
	  DGCgettime(endStage2Time);
  };

  void endProcessTimer(int newMsgSize) {
	  DGCgettime(endProcessTime);
	  msgSize = newMsgSize;
	  msgSizeAvg = (msgSize + numMsgs*msgSizeAvg)/(numMsgs+1);
	  processTime = ((double)(endProcessTime-startProcessTime))/(1000.0);
	  processTimeAvg = (processTime + processTimeAvg*numMsgs)/(numMsgs+1);
	  lockTime = ((double)(endLockTime-startProcessTime))/(1000.0);
	  lockTimeAvg = (lockTime + lockTimeAvg*numMsgs)/(numMsgs+1);
	  stage1Time = ((double)(endStage1Time - endLockTime))/(1000.0);
	  stage1TimeAvg = (stage1Time + stage1TimeAvg*numMsgs)/(numMsgs+1);
	  stage2Time = ((double)(endStage2Time - endStage1Time))/(1000.0);
	  stage2TimeAvg = (stage2Time + stage2TimeAvg*numMsgs)/(numMsgs+1);
	  remainingTime = ((double)(endProcessTime - endStage2Time))/(1000.0);
	  remainingTimeAvg = (remainingTime + remainingTimeAvg*numMsgs)/(numMsgs+1);
	  numMsgs++;
  };

  void reset() {
    numMsgs=0;
    msgSize=0;
    msgSizeAvg=0.0;
    processTime=0.0;
    processTimeAvg=0.0;
    lockTime = 0.0;
    lockTimeAvg = 0.0;
    stage1Time = 0.0;
    stage1TimeAvg = 0.0;
    stage2Time = 0.0;
    stage2TimeAvg = 0.0;
    remainingTime = 0.0;
    remainingTimeAvg = 0.0;
    startProcessTime = 0;
    endProcessTime = 0;
    endLockTime = 0;
    endStage1Time = 0;
    endStage2Time = 0;
  };
}; //end of FusionMapperStatusInfo class definition


struct FusionMapperElevationLayer {
  int elevLayerNum;
  int costLayerNum;
  int terrainPainterIndex;
  int costPainterIndex;
  int corridorLayerNum;
  CCorridorPainter* corridorPainter; 
  bool sendCost;
  sn_msg msgTypeCost;
  CMapPlus* map;
  CTerrainCostPainter* terrainPainter;
  sn_msg msgTypeElev;
  char name[32];
  char optionsFilename[128];
  FusionMapperStatusInfo statusInfo;
  bool enabled;
  CTerrainCostPainterOptions options;
  double relWeight;
  pthread_mutex_t shiftMutex;
  
};

class FusionMapper : public CStateClient, public CMapdeltaTalker, public CTrajTalker, public CSuperConClient {
public:
  FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg, int waitForState);
  ~FusionMapper();
  
  void ActiveLoop();
  
  void ReceiveDataThread_ElevFeeder(void* pArg);

//   void ReceiveDataThread_LadarRoof();
//   void ReceiveDataThread_LadarBumper();
//   void ReceiveDataThread_LadarRiegl();
//   void ReceiveDataThread_LadarSmall();
//   void ReceiveDataThread_StereoMedium();
//   void ReceiveDataThread_StereoShort();

  void ReceiveDataThread_Road();
  
  void ReceiveDataThread_Supercon();
  
  void ReceiveDataThread_SuperconActions();

  void ReceiveDataThread_EStop();

  void UpdateSparrowVariablesLoop();

  void PlannerListenerThread();
  
  void SparrowDisplayLoop();

  void ReadConfigFile();

  void WriteConfigFile();

  
  //void SendDataThread();

  FusionMapperElevationLayer layerArray[NUM_ELEV_LAYERS];

private:

  FusionMapperOptions mapperOpts;

  //Info Structs for each module we receive from
  FusionMapperStatusInfo _infoStaticPainter;  
  FusionMapperStatusInfo _infoStereoFeeder;
  FusionMapperStatusInfo _infoRoad;
	FusionMapperStatusInfo _infoSupercon;
  //And one for output info
  FusionMapperStatusInfo _infoFusionMapper;

  //char* m_pStaticMapDelta;
  char* m_pStereoMapDelta;
  char* m_pSuperconMapDelta;
  double* m_pRoadInfo;
  

  //double* m_pRoadInfo;

  double* m_pRoadReceive;

  double* m_pRoadX;
  double* m_pRoadY;
  double* m_pRoadW;
  int m_pRoadPoints;

  pthread_rwlock_t allMapsRWLock;

  pthread_mutex_t roadMutex;

  pthread_mutex_t kfMutex;

  CMapPlus fusionMap;
  //	CMapPlus lowResMap;
  CMapPlus roadMap;
  CMapPlus swapMapA, swapMapB;

  RDDF fusionRDDF;
  RDDF widerRDDF;
  herman* fusionHerman;

  CCorridorPainter* fusionCorridorPainter;
  CCorridorPainterOptions _corridorOptsFinalCost;
  CCorridorPainterOptions _corridorOptsReference;
  CCorridorPainterOptions _corridorOptsWings;
  CCorridorPainter *lowResCorridorPainter;

  CTerrainCostPainter* defaultTerrainPainter;
  CTerrainCostPainter* lowResTerrainPainter;

  CCostFuser* costFuser;
  CCostFuserOptions costFuserOpts;

  int layerID_road;
  int layerID_roadCounter;

  int layerID_costFinal;
 
  int layerID_corridor;
  int layerID_corridor_lowres;

  int layerID_superCon;

  int layerID_fusedKF;
  int layerID_fusedElev;
  int layerID_fusedVar;

  int _status;

// Temporary way to debug
// ofstream delta_log;

  int _QUIT;
  int _PAUSE;
  int _PAINTWHILEPAUSED;
  /*!Used for guiTab.*/
//   SfusionmapperTabInput  m_input;
//   SfusionmapperTabOutput m_output;
};

#endif
