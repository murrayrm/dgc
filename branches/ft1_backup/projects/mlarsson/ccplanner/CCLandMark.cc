#ifndef _CCLANDMARK_CC_
#define _CCLANDMARK_CC_
#include <iostream>
#include "CCLandMark.h"

#ifndef PI
#define PI 3.1415927 
#endif
inline int sign(double a) { return (a == 0.0) ? 0 : (a<0.0 ? -1 : 1); }; 

/** Returns arg(x,y) in range -Pi to +Pi */
inline double arg(double x, double y)
{
	double theta = atan(y/x);
	if (x>0.0)
		return theta; 
	else
		return theta + sign(y) * PI;
}	

inline double arg(NEcoord d) {return arg(d.E,d.N); };

/** Constructor */
CCLandMark::CCLandMark() 
{
	m_exploredDistSqd=10000000;
	m_atFinish=false;
	m_pParent=NULL;
	m_g = 0.0;
	m_h = 0.0;
	m_f = 0.0;
	m_children.clear();
}

/** Set the path data and parent pointer of this node. Use this to calculate the cost function g. */
void CCLandMark::setPathParentCost(CCElementaryPath *path, CCLandMark *parent, CVectorMap *map)
{
	m_path = *path; 
	m_pos = path->back();
	m_theta = path->back().m_theta;
	m_pParent = parent; 
	m_pParent->m_children.push_back(this);
	cost(map); 
}

// Note to self: it is essential that this function is called in the correct order by the recursive update
// function, i.e. descending the tree. CalculateF (i.e. finishHasChanged) must be called after this to
// use the updated G values. 
void CCLandMark::cost(CVectorMap *map)
{
	double laneCost; 
	// We should only apply this in normal road driving. 
	// In any of the safety regions - e.g. parking zone or crossing, there is no lane cost. 
	if (map->pointInSafetyZone(m_pos))
	{
		// !!! Maybe this should be finite - otherwise may bias towards driving through car parks 
		// as a short cut! 
		laneCost = 0.0; 
	}
	else
	{
		double distSqd, yaw; 
		map->getDistanceAndYawOfNearestLaneBoundary(m_pos,&distSqd,&yaw); 
		double sinAngleDifference = sin(yaw-m_theta);
		double orientationCost = sinAngleDifference * sinAngleDifference;
		if (distSqd < CCLANDMARK_MIN_DISTSQD_FOR_COST)
		{
			distSqd = CCLANDMARK_MIN_DISTSQD_FOR_COST; 
		}
		double distCost = 1.0 / distSqd; 
		laneCost = (CCSPATIALPLANNER_LANE_ORIENTATION_COST * orientationCost +  CCSPATIALPLANNER_LANE_DIST_COST*distCost) * m_path.m_length;
	}
	m_g = m_pParent->m_g + m_path.cost() + laneCost;
	m_f = m_h + m_g; 
}

void CCLandMark::updateLaneCost(CVectorMap *map)
{
	// If this is not the start node. 
	if (m_pParent != NULL)
	{
		cost(map); 
	}
	
	for (vector<CCLandMark*>::iterator i=m_children.begin(); i!=m_children.end(); i++)
	{
		if ((*i) != NULL) 
		{
			 (*i)->updateLaneCost(map);
		}
	}
}

/** Calculate the heuristic - an estimate of the cost from this node to the finish. */
void CCLandMark::calculateF(CCLandMark *finish)
{
	NEcoord d = m_pos - finish->m_pos;
	double dn = d.norm();
	m_h  = dn + CSPATIALPLANNER_PREFERRED_DIST * CSPATIALPLANNER_PREFERRED_DIST / dn; 
	m_f = m_h + m_g; 
}

/** Log position and orientation to file. */ 
void CCLandMark::log(ostream *f)
{
	*f  << m_pos.E << " " << m_pos.N << " " << m_theta << endl; 
}

/** Log position and orientation to file. */ 
void CCLandMark::printAll(ostream *f)
{
	log(f);
	for (vector<CCLandMark*>::iterator i=m_children.begin(); i!=m_children.end(); i++)
	{
		(*i)->printAll(f); 
  }
}

/** Recursive function to return the number of points in a traj. */
int CCLandMark::getNumPoints()
{
	if (m_pParent == NULL)
		return m_path.size();
	return m_path.size() + m_pParent->getNumPoints();
}

/** Recursive function to build a CTraj trajectory. */
int CCLandMark::getTraj(CTraj *t)
{
	int i=0;
	if (m_pParent != NULL)
		i = m_pParent->getTraj(t);
	m_path.getTraj(t,&i); 
	return i; 
}

CCLandMark::~CCLandMark()
{
}

/** Recursively delete descendants of this node. */
void CCLandMark::deleteDescendants()
{
	// Check there are actually some children to delete
	if (m_children.size() > 0)
		// Iterate through them 
		for (vector<CCLandMark*>::iterator i=m_children.begin(); i!=m_children.end(); i++)
		{
			// Recursively delete children 
			if ((*i) != NULL) 
			{
	  		(*i)->deleteDescendants();
	  		delete (*i);
	  	}
	  	// And remove the pointer from our children vector.
	  	m_children.erase(i); 
	  	i--;
	  }
}

/** Recursively prune the tree of all branches that go outside the updated legal region. */ 
void CCLandMark::pruneTree(CVectorMap *map)
{
	/// Check there are actually some children to worry about. 
	if (m_children.size() > 0)
		/// Iterate through all the children, checking whether the path to them is still legal
		for (vector<CCLandMark*>::iterator i=m_children.begin(); i!=m_children.end(); i++)
		{
			if ((*i) != NULL) 
			{
				/// If the path has become illegal delete this child and all its descendants as we
				/// no longer know how to reach them. 
				if (!(*i)->m_path.isLegal(map))
				{
	  			(*i)->deleteDescendants();
	  			delete (*i);
	  			m_children.erase(i); 
	  			i--;
	  		}
	  		else
	  		{
	  			/// Recursive
	  			(*i)->pruneTree(map);
	  		}
	  	}
	  	else
	  	{
	  		/// Delete NULL pointers for cleanliness. 
	  		m_children.erase(i); 
	  		i--;
	  	}
	  }
}

/** Return the node with the lowest F value in the tree. */
CCLandMark* CCLandMark::findLowestF(double minDistSq)
{ 
	// if we are a finish node return NULL
	if (m_atFinish)
		return NULL;
	bool exploredAtThisResolution = (minDistSq >= m_exploredDistSqd);
	// find the lowest value of f from us and all our descendants
	double lowestF = exploredAtThisResolution ? 10000000 : m_f; 
	CCLandMark* landmarkWithLowestF = exploredAtThisResolution ? NULL : this;

	for (vector<CCLandMark*>::iterator i=m_children.begin(); i!=m_children.end(); i++)
	{
		// recursive
		CCLandMark* m = (*i)->findLowestF(minDistSq);
		if (m!=NULL)
			if (m->m_f < lowestF) 
			{
				landmarkWithLowestF = m;
				lowestF = m->m_f;
			}
	}
	return landmarkWithLowestF; 
}

/** The finish node has moved: make the necessary changes to the graph. */
void CCLandMark::finishHasChanged(CCLandMark *finish, bool reverse, CVectorMap *map)
{
	// Recalculate F for this node with the new finish position
  calculateF(finish); 
  
  // Iterate through all children (and therefore all descendants recursively)  
	for (vector<CCLandMark*>::iterator i=m_children.begin(); i!=m_children.end(); i++)
	{
	  if (!(*i)->m_atFinish)
	  {
	  	(*i)->finishHasChanged(finish,reverse,map);
		}
		else
		{
			// If this child is an old finish node it is now redundant and should be deleted:
			delete *i; 
      m_children.erase(i); 
      i--;
    }
	}
	
	// Re-try search on all the nodes. 
	search(finish,false,map); 
	if (reverse) search(finish,true,map); 
}

/** Calculates a path from this landmark to finish and checks whether it is an acceptable route */
bool CCLandMark::search(CCLandMark *finish, bool reverse, CVectorMap *map) 
{
	CCLandMark* p1 = reverse ? finish : this; 
	CCLandMark* p2 = reverse ? this : finish;
		
	NEcoord d = p2->m_pos - p1->m_pos;
	if ((d.N*d.N + d.E*d.E) > CSPATIALPLANNER_MAX_SEARCH_DIST_SQUARED)
		return false; 
	double beta = arg(d);

	// Are the postures symmetric? 
	if (abs((p1->m_theta - beta) + (p2->m_theta - beta)) < CSPATIALPLANNER_EPS_ARE_SYMMETRIC)
	{
		CCElementaryPath path;
		// posture happen to be symmetric, so we can find an elementary path between them 
		if (!path.findSigmaL(p1,p2))
			return false; // if the required curvature was not feasible
		// generate the path
		path.calculate();
	
		path.m_reverse = reverse;
		// rotate/translate the path and see if it is allowed (ignore reverse, pretend going forward!)
		if (path.startFrom(reverse ? p2 : p1,map))
		{
		// !!! Might want to make p1->child = p2 etc if using this function not as SEARCH
			CCLandMark *newEndpoint = new CCLandMark();
			*newEndpoint = *finish;
			newEndpoint->setPathParentCost(&path,this,map);
			newEndpoint->m_atFinish = true;
			return true; 
		}
		else
			return false; 
	}
	else
	{
		// postures are not symmetric:
		return generateBielementaryPath(p1, p2, reverse,map); 
	}
}

/** Check whether angle is between start and finish in direction clockwise. */ 
bool inbetween(double angle,double start,double finish,bool clockwise)
{
	if (angle < -PI) angle += 2.0*PI;
	if (angle > PI) angle -= 2.0*PI;
	if (clockwise)
	{
		if (start>finish)
			return (start>angle)&&(angle>finish);
		else
			return ((finish<angle)&&(angle<PI))||((-PI<angle)&&(angle<start));
	}
	else
	{
		if (start<finish)
			return (start<angle)&&(angle<finish);
		else
			return ((start<angle)&&(angle<PI))||((-PI<angle)&&(angle<finish));
	}
}
 
/** Generates a bielementary path between p1 and p2, in the direction specified by reverse */ 
bool CCLandMark::generateBielementaryPath(CCLandMark *p1, CCLandMark *p2, bool reverse, CVectorMap *map)
{
	// intermediate posture, q, which is symmetric to both p1 and p2
	CCLandMark *q = new CCLandMark();
	
	// We should really check if p1 and p2 are symmetric already! 
	// try to find a bielementary path from p1 to p2
	double deflection /*alpha*/ = p2->m_theta - p1->m_theta; 
	double beta;
	CCElementaryPath path1, path2; 
	
	if (abs(deflection) < CSPATIALPLANNER_EPS_IS_PARALLEL) // parallel postures 
	{
		// the intermediate posture is at the midpoint of p1 and p2
		q->m_pos = (p1->m_pos + p2->m_pos)/2;
		// beta = arg(p2->m_pos - p1->m_pos);
		beta = atan((p2->m_pos.N - p1->m_pos.N)/(p2->m_pos.E - p1->m_pos.E));
		// theta1 - beta = - (theta2 - beta) from Kanayama =>
		// => theta2 = beta - (theta1 - beta)
		q->m_theta = beta - (p1->m_theta - beta);
		
		if (!path1.findSigmaL(p1,q))
			return false; 
		if (!path2.findSigmaL(q,p2))
			return false; 
	}
	else
	{
		// from Kanayama this circle is the locus of the intermediate posture
		NEcoord centre;
		
		double c = 1.0/tan(deflection/2); 
		centre.E = (p1->m_pos.E + p2->m_pos.E + c * (p1->m_pos.N - p2->m_pos.N))/2;
		centre.N = (p1->m_pos.N + p2->m_pos.N + c * (p2->m_pos.E - p1->m_pos.E))/2;
		NEcoord d = p2->m_pos - centre; 
		double radius = d.norm(); 

		// !!! Kanayama uses Newton-Raphson iteration to minimise curvature whilst varying
		// q. For the moment just iterate around correct bit of circle
		// parameterize position of q in terms of angle from centre of circle
		double minCost = 10000000.0;
		double minAngle = 0.0;
		CCLandMark minIntPosture; 
		double start = arg(p1->m_pos - centre);
		double finish = arg(p2->m_pos - centre);
		double increment = sign(deflection)*CSPATIALPLANNER_INT_INTERVAL/radius; 
		bool intermediateFound = false; 
		
		// Iterate around circle
		for (double angle = start + increment; inbetween(angle,start,finish,increment<0); angle += increment)
		{
			if (angle < -PI) angle += 2.0*PI;
			if (angle > PI) angle -= 2.0*PI;
			
			d.N = radius * sin(angle);
			d.E = radius * cos(angle);
			q->m_pos = centre + d;
			beta = arg(q->m_pos - p1->m_pos);
			q->m_theta = beta - (p1->m_theta - beta); 
			
			CCElementaryPath dummy1, dummy2; 
			 
			if (!dummy1.findSigmaL(p1,q))
				continue; 
			if (!dummy2.findSigmaL(q,p2))
				continue; 
				
			/* More consistent to use same cost function everywhere
			// cost = total curvature
			double cost = abs(dummy1.getCurvature()) + abs(dummy2.getCurvature());
			// it is interesting to use this as the cost function instead: */
			double cost = abs(dummy1.m_sigma) + abs(dummy2.m_sigma);
			
			// !!! In future might want to position these to include lane discipline as a cost
			// double cost = dummy1.cost() + dummy2.cost();
			
			if (cost < minCost)
			{
				intermediateFound = true; 
				minCost = cost;
				minAngle = angle;
				path1 = dummy1;
				path2 = dummy2; 
				minIntPosture = *q; 
			}
		}
		
		if (!intermediateFound)
		{
			return false; 
		}
			
		*q = minIntPosture;
	}
	
	path1.m_reverse = path2.m_reverse = reverse;
	
	if (reverse) 
	{
		CCLandMark *dummy = p1;
		p1 = p2; 
		p2 = dummy; 
		path1.m_sigma *= -1.0;
		path2.m_sigma *= -1.0;
	}
	
	CCElementaryPath *first, *second; 
	first = reverse ? &path2 : &path1;
	second = reverse ? &path1 : &path2;
	
	first->calculate();
	
	// Now we have found q we can generate the two bielementary trajectories
	if (first->startFrom(p1,map))
	{
		//logfile << "First bit done" << endl;
		q->setPathParentCost(first,p1,map);
		second->calculate();
		// generate path1
		if (second->startFrom(q,map))
		{
			CCLandMark *newEndpoint = new CCLandMark();
			newEndpoint->setPathParentCost(second,q,map);
			newEndpoint->m_atFinish = true;
			return true; 
		}
	}
	delete q; 
	return false; 
}


/** Recursive function to "take a cutting" */
CCLandMark* CCLandMark::choose(CCLandMark *calledBy,NEcoord *pos)
{
	if (m_pParent == NULL)
	{
		if (m_children.size() > 1)
		{
			cout << "CCLandMark::choose - Initial path choosen, deleting lower branches." << endl;
			for (vector<CCLandMark*>::iterator i=m_children.begin(); i!=m_children.end(); i++)
				if (*i != calledBy)
				{
					if ((*i) != NULL)
					{
						(*i)->deleteDescendants();
						delete *i;
					} 
					m_children.erase(i);
					i--;
				}
		}		
		return calledBy; 
	}
	else 
	{
		CCLandMark* returnedLM = m_pParent->choose(this,pos);
		if (returnedLM == this)
		{
			// We are the first landmark to go to. 
			NEcoord sep = *pos - m_pos; 
			if (sep.norm() < CSPATIALPLANNER_MAX_DIST_ERROR)
			{
				cout << "CCLandMark::choose - Node reached, deleting parent." << endl;
				// We've made it to the first landmark
				delete m_pParent;
				m_pParent = NULL;
			}
		}
		return returnedLM;
	}
}

/** Recursive function to return the endpoint with the lowest G value. More maintainable than list of pointers. */
CCLandMark* CCLandMark::findBestEndpoint()
{ 
	// if we are a finish node return this
	if (m_atFinish)
		return this;
	// find the lowest value of g from all endpoints in our descendants
	double lowestG = 10000000; 
	CCLandMark* endpointWithLowestG = NULL;
	for (vector<CCLandMark*>::iterator i=m_children.begin(); i!=m_children.end(); i++)
	{
		CCLandMark* m = (*i)->findBestEndpoint();
		if (m!=NULL)
			if (m->m_g < lowestG) 
			{
				endpointWithLowestG = m;
				lowestG = m->m_g;
			}
	}
	return endpointWithLowestG; 
}


/** Find the distance of the nearest node to c from c. */
bool CCLandMark::nodeWithinTolerance(NEcoord c, double minSqdDist)
{ 
	// Find the squared distance of this node from c
	NEcoord separation = m_pos - c;
	double distSq = separation.N * separation.N + separation.E * separation.E;
	// If it is less than minSqdDist from c then we have our answer: there is a node within the tolerance
	if (distSq < minSqdDist) return true; 
	// Iterate through all descendants, propagating up the result
	for (vector<CCLandMark*>::iterator i=m_children.begin(); i!=m_children.end(); i++)
		if ((*i)->nodeWithinTolerance(c,minSqdDist))
			return true; 
	// If none of the descendants, or this node, are within the tolerance, return false
	return false; 
}

#endif


