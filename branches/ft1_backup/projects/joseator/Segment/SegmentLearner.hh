/****************************************************************
 *  Project    : Learning and adaptation, SURF 2006             *
 *  File       : SegementLearner.hh                             *
 *  Author     : Jose Torres                                    *
 *  Modified   : 7-27-06                                        *
 ****************************************************************/      

#ifndef SEGMENTLEARNER_HH
#define SEGMENTLEARNER_HH

#include "SegmentNode.hh"
#include "StateClient.h"
#include "SkynetContainer.h"
#include <iostream>
#include <cstring>
#include <iomanip>
#include <cmath>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <stdio.h>
#include <time.h>
#include <strstream>

using namespace std;

/** SegmentLearner class. This class stores road features of 
 *  all the segment ALICE has traversed. It receives two types
 *  of messages. The first type is SNrndfCheckPoint and it 
 *  contains a starting checkpoint and a destination checkpoint.
 *  The second message does not have it's own message type. It
 *  uses the SNrdnfCheckpont type to simulate sending the 
 *  coordinates of road edges. Currently, this program can 
 *  only save the road edges. The program saves road features 
 *  by segment. For example, it saves all the road features of
 *  segment 1 in a files named 1_segment. When a transition
 *  message is received, it creates a new file accordingly.
 */
class SegmentLearner : public CStateClient 
{
  
private:
  /** variable used to be able to dynamiclly create files 
   *  with corresponding name*/
  int number;

  /** pointer to one end of a list. CURRENTLY, NO LIST IS USED
   *  THEREFORE POINTER NOT USED */
  SegmentNode* entrance_A;

  /** pointer to one end of a list. CURRENTLY, NO LIST IS USED
   *  THEREFORE POINTER NOT USED */
  SegmentNode* entrance_B;

  /** struct used to send road edge info via messages*/
  struct Road
  {
    double edge_a;
    double edge_b;
    double x;
    double y;
  };

  /** struct used to send message that a segment transition has occured*/
  struct Transition
  {
    int segment;
    bool segmentTransition;
    
  };
  
public:
  /** constructor*/
  SegmentLearner(int skynet_key);
  
  /** destructor*/
  ~SegmentLearner();
  
  /** function adds node to a list. CURRENTLY NO LIST IS BEING USED*/
  void addNode(double edgeRoad_a, double edgeRoad_b);

  /** function that controls program. function creates files with 
   *  corresponding segment names and saves data. Function also calls
   *  checkForTransition to see if a segment transition has occured
   */
  void active();
  
  /** function checks for segment transition messages. Assigns the 
   *  member variable 'number' to the segment currently traversing
   */
  bool checkForTransition(ofstream &outData);
  
};

#endif
