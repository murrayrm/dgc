#ifndef __VECTOR_H__
#define __VECTOR_H__

/**A standard mathematical vector class. */
/**Author: David Rosen */

class Vector
{
 protected:
  
  double* m_vector;
  int dimension;

 public:

  /**Creates a 1-dimensional vector, initialized to 0.*/
  Vector();

  /**Creates a vector of dimension d, initialized to all 0's. */
  Vector(const int d);

  /**Copy constructor. */
  Vector(const Vector& other);

  /**Assignment operator. */
  Vector& operator = (const Vector& b);
  
  /**Returns the dimension of the calling vector. */
  int getDimension() const;

  /**Returns the element in the "component" component of the vector. */
  double getElement(const int component) const;

  /**Sets the "component" component of the vector to value "val". */
  void setElement(const int component, const double val);

  /**Standard vector addition. */
  Vector  operator + (const Vector& b) const;

  /**Standard vector subtraction, return (*this) - b. */
  Vector operator - (const Vector& b) const;

  /**Multiplies the elements in this vector by the constant k. */
  Vector scalar(const double k) const;

  /**Vector dot product. */
  double operator * (const Vector& b) const;

  /**Vector cross product (*this) x b (requires that (*this) and b are of dimension 3). */
  Vector cross(const Vector& b) const;

  /**Computes the projection of (*this) onto b. */
  Vector proj(const Vector& b) const;

  /**Calculates the norm of (*this). */
  double norm() const;

  /**Returns a unit vector that points in the same direction as (*this). */
  Vector unit() const;

  /**The angle between (*this) and b, in radians. */
  double angle(const Vector& b) const;

  /**Equality operator. */
  bool operator == (const Vector& b) const;

  /**Inequality operator. */
  bool operator != (const Vector& b) const;

  /**Determines if (*this) and b are orthogonal. */
  bool isOrthogonal(const Vector& b) const;

  /**Prints out the vector (in a row format). */
  void print() const;

  /**Standard destructor. */
  ~Vector();
};

#endif // __VECTOR_H__
