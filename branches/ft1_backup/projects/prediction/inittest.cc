#include "ParticleFilter.h"
#include <iostream>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <fstream>

using namespace std;


int main(void)
{
  
  ofstream outfile("inittest.dat");
  
  //Now we generate a particle set to represent the vehicle.  Assume for the sake of argument that this is the first measurment that we've made of the vehicle, so that we know about where it is (up to uncertainty measurments in the ladar), but can only guess about its other characteristics.
  
  //For random number generation.
  gsl_rng* rand;
  gsl_rng_env_setup();
  const gsl_rng_type* T;
  T = gsl_rng_default;
  rand = gsl_rng_alloc(T);

  double dist_uncertainty = .05;  //Standard deviation in the ladar units, in m.



  //Begin by generating a set of "numparts" particles.

  const int numparts = 5000;

  Vector* particles = new Vector[numparts];

  Vector new_particle(9);

  for(int i = 0; i < numparts; i++)
    {
      //Set x, y to be zero with a small measurement uncertainty.
      new_particle.setElement(1, gsl_ran_gaussian(rand, dist_uncertainty));
      new_particle.setElement(2, gsl_ran_gaussian(rand, dist_uncertainty));
      
      //We don't know anything about heading angle yet, so just assume that its a uniform distribution to represent complete uncertainty.
      new_particle.setElement(3, gsl_ran_flat(rand, 0, 2*M_PI));

      //We also don't know anything about the acceleration or velocity of the vehicle, so we use uniform distributions for these numbers as well.
      new_particle.setElement(4, gsl_ran_flat(rand, -2.5, 2.5)); //Accel, in m/s^2
      new_particle.setElement(5, gsl_ran_flat(rand, -6.7, 20.15));  //Velocity, about -15 to 45 mph
      new_particle.setElement(6, gsl_ran_flat(rand, (double)-1/6.0, (double) 1/6.0));  //Path curvature; corresponds to a turning radius between 6 meters (about 20 ft.) and \infinity (straight-line driving).

      particles[i] = new_particle;
    }
  
  for(int v = 0; v < numparts; v++)
    {
      //Print out the contents of each particle in the pdf.
      for(int c = 1; c<= 8; c++)
	{
	  outfile <<particles[v].getElement(c)<<" ";
	}
      outfile<<endl;
    }
  
  delete [] particles;
  
  outfile.close();
}
