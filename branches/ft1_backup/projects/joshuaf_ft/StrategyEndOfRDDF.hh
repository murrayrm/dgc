#ifndef STRATEGY_ENDOFRDDF_HH
#define STRATEGY_ENDOFRDDF_HH

//SUPERCON STRATEGY TITLE: END OF RDDF Strategy
//HEADER FILE (.hh)
//DESCRIPTION: Strategy to handle the END OF RDDF condition
//under these conditions Alice is paused, and a VERY long time-out
//is initiated (at this point we are waiting for DARPA to pause/
//disable us)

#include "Strategy.hh"
#include "sc_specs.h"
#include "stage_iterator.hh"

namespace s_endofrddf {

#define ENDOFRDDF_STAGE_LIST(_) \
  _(estop_pause, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(wait_for_DARPA, )
DEFINE_ENUM(endofrddf_stage_names, ENDOFRDDF_STAGE_LIST)

}

using namespace std;
using namespace s_endofrddf;

class CStrategyEndOfRDDF : public CStrategy
{

/** METHODS **/
public:

  CStrategyEndOfRDDF() : CStrategy() {}  
  ~CStrategyEndOfRDDF() {}


  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};

#endif
