#ifndef STRATEGY_UTURN_HH
#define STRATEGY_UTURN_HH

//SUPERCON STRATEGY TITLE: U-turn - SOMETIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: 
//It has been determined that Alice needs to be able to turn around.
//She will do this by making a 3-point turn. Which is a monster.
//Note: This lives in Strategy LturnReverse.hh & .cc only for field test
//purposes.  The main copies of this file are elsewhere and appropriately
//named.

#include "Strategy.hh"
#include "sc_specs.h"
#include "../../projects/mlarsson/ccplanner/CCLandMark.h"

namespace s_uturn {

#define UTURN_STAGE_LIST(_) \
  _(estop_pause_1, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
/*  _(broaden_corridor, ) */ \
/*  _(exit_RNDF, ) */ \
  _(place_waypoint_exit, ) \
  _(check_for_uturn, ) /*also serves as find_waypoint_1*/ \
  _(place_waypoint_1, ) \
/* Transition directly to estop_run_exit if can make it to exit waypoint */ \
  _(estop_run_1, ) \
  _(estop_pause_2, ) \
  _(find_waypoint_2, ) \
  _(place_waypoint_2, ) \
  _(gear_reverse, ) \
  _(trajF_reverse, ) \
  _(estop_run_2, ) \
  _(estop_pause_3, ) \
  _(gear_drive, ) \
  _(trajF_forwards, ) \
/* Transition back to check_for_uturn here */ \
  _(estop_run_exit, ) \
/*  _(reenter_RNDF, ) */ \
/*  _(wait_new_route, ) */
DEFINE_ENUM(uturn_stage_names, UTURN_STAGE_LIST)

}

using namespace std;
using namespace s_uturn;

class CStrategyLturnReverse : public CStrategy
{
private:
  CWayPoint *ExitWpt, *NextWpt;
  
public:

  /** CONSTRUCTORS **/
  CStrategyLturnReverse () : CStrategy() {ExitWpt = new CWayPoint; NextWpt = 0;}  

  /** DESTRUCTOR **/
  ~CStrategyLturnReverse() {delete ExitWpt; if (NextWpt != 0) delete NextWpt;}


  /** MUTATORS **/

  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};

#endif
