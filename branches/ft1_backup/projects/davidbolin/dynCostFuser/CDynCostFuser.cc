#include "CDynCostFuser.hh"

CDynCostFuser::CDynCostFuser(CMapPlus* movingObstMap, int layerNumMovingObst, 
			     CMapPlus* mapOutput, int layerNumFinalCost,
			     CMapPlus* mapRDDF, int layerNumRDDF,
			     CMapPlus* mapChangesA, CMapPlus* mapChangesB,
			     CDynCostFuserOptions* options, 
			     CCorridorPainterOptions* corridorOptions) : CCostFuser(mapOutput, layerNumFinalCost, 
										    mapRDDF, layerNumRDDF, mapChangesA, 
										    mapChangesB, options, corridorOptions){


  _options = options;
  _movingObstMap = movingObstMap;
  _layerNumMovingObst = layerNumMovingObst;

  //cout << "_options:" << _options << ", options: " << options << endl; 
  if(_options->optDA){
    for(int i=0; i < 2; i++) {
      _layerNumDAChanges[i] = _mapChanges[i]->addLayer<int>(CONFIG_FILE_CHANGES, true);
    }
  }
}



unsigned long long CDynCostFuser::markChangesCost(NEcoord point) {
  unsigned long long waitTime;

  //Mark the changes in the CostChanges layer and the data association layer
  _mapChanges[_currentFillingMapIndex]->setDataUTM_Delta<int>(_layerNumCostChanges[_currentFillingMapIndex], 
							      point.N, point.E, 1, &_writeDeltaLock);			

  if(_options->optDA){
    _mapChanges[_currentFillingMapIndex]->setDataUTM_Delta<int>(_layerNumDAChanges[_currentFillingMapIndex], 
								point.N, point.E, 1, &_writeDeltaLock);
  }

  int currentRow, currentCol, maxRow, minRow, maxCol, minCol;
	
  _mapChanges[_currentFillingMapIndex]->UTM2Win(point.N, point.E, &currentRow, &currentCol);
	
  //Now we do some interpolation
  _mapChanges[_currentFillingMapIndex]->constrainRows(currentRow, _options->maxCellsToInterpolate, minRow, maxRow);
  _mapChanges[_currentFillingMapIndex]->constrainCols(currentCol, _options->maxCellsToInterpolate, minCol, maxCol);
  
  for(int r=minRow; r<=maxRow; r++) {
    for(int c=minCol; c<=maxCol; c++) {
      _mapChanges[_currentFillingMapIndex]->setDataWin_Delta<int>(_layerNumInterpolationChanges[_currentFillingMapIndex], 
								  r, c, 1, &_writeDeltaLock);
    }
  }
  return waitTime;
}


unsigned long long CDynCostFuser::markChangesTerrain(int layerIndex, NEcoord point) {
  unsigned long long waitTime;

  /**
  Check if the resolution of the output map is lower than the resolution of the elevation map.
  If so, more than one cell have to be updated in the output map.
  */
  if(_mapPtrsElevCost[layerIndex]->getResRows() > _mapOutput->getResRows() ||
     _mapPtrsElevCost[layerIndex]->getResCols() > _mapOutput->getResCols()) {
    int numRows = 2;
    int numCols = 2;
    int currentRow, currentCol;
    NEcoord tempPoint;
    _mapChanges[_currentFillingMapIndex]->UTM2Win(point.N, point.E, &currentRow, &currentCol);
    for(int r = currentRow; r < currentRow + numRows; r++) {
      for(int c = currentCol; c < currentCol + numCols; c++) {
	_mapChanges[_currentFillingMapIndex]->Win2UTM(r, c, &tempPoint.N, &tempPoint.E);
	_mapChanges[_currentFillingMapIndex]->setDataUTM_Delta<int>(_layerNumElevChanges[_currentFillingMapIndex], 
								    tempPoint.N, tempPoint.E, 1);

	waitTime+=markChangesCost(tempPoint);
      }
    }
  } else {
    _mapChanges[_currentFillingMapIndex]->setDataUTM_Delta<int>(_layerNumElevChanges[_currentFillingMapIndex], point.N, point.E, 1);			
    waitTime+=markChangesCost(point);
  }
	return waitTime;
}



unsigned long long CDynCostFuser::fuseChangesCost(NEcoord newMapCenter){
 
  //Change current processing map and update the vehilce location.
  unsigned long long waitTime;
  DGCwritelockRWLock(&_swapLock, &waitTime);
  _mapChanges[_currentProcessingMapIndex]->updateVehicleLoc(newMapCenter.N, newMapCenter.E);
  int temp = _currentFillingMapIndex;
  _currentFillingMapIndex = _currentProcessingMapIndex;
  _currentProcessingMapIndex = temp;
  DGCunlockRWLock(&_swapLock);
  
  //fuse the terrain layers
  fuseChangesTerrain();
 
  NEcoord point;
  //get the number of cells which should be updated in the output layer.
  int numCells = _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaSize(_layerNumCostChanges[_currentProcessingMapIndex]);
  
  //set the speed-limits in the output layer for the cells that were fused in fuseChangesTerrain
  for(int i=0; i < numCells; i++) {
    _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaVal<int>(i, _layerNumCostChanges[_currentProcessingMapIndex], 
								     &point.N, &point.E);
    fuseCellCost(point);
  }
  
  _numCellsFused = numCells;
  
  _mapChanges[_currentProcessingMapIndex]->resetDelta<int>(_layerNumCostChanges[_currentProcessingMapIndex]);
  
  //Now interpolate changes, i.e. fill in no data cells if they have enough neighbors.
  interpolateChanges();
  //finally, if the data association option is turned on, do some data association 
  //to correct imprecise moving obstacle information.
  if(_options->optDA){
    dataAssociation();
  }
  
  return waitTime;
}


CCostFuser::STATUS CDynCostFuser::fuseChangesTerrain() {
  NEcoord point;
  //get the number of cells which should be fused.
  int numCells = _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaSize(_layerNumElevChanges[_currentProcessingMapIndex]);
  
  for(int i=0; i < numCells; i++) {
    //get the coordinate of the cell which should be fused, the coordinate is saved in point.
    _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaVal<int>(i, _layerNumElevChanges[_currentProcessingMapIndex], 
								     &point.N, &point.E);
    fuseCellTerrain(point);
  }
  
  _mapChanges[_currentProcessingMapIndex]->resetDelta<int>(_layerNumElevChanges[_currentProcessingMapIndex]);
  
  return OK;
}


CCostFuser::STATUS CDynCostFuser::interpolateChanges() {
  NEcoord point;
  //get the number of cells which should be interpolated.
  int numCells = _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaSize(_layerNumInterpolationChanges[_currentProcessingMapIndex]);
  
  for(int i=0; i < numCells; i++) {
    //get the coordinate of the cell which should be fused, the coordinate is saved in point.
    _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaVal<int>(i, _layerNumInterpolationChanges[_currentProcessingMapIndex], 
								     &point.N, &point.E);
    //we don't want to interpolate cell which belong to moving obstacles, since this information won't be used and since 
    //it would overwrite the speed-limit set by fuseCellCost (which is the one we want the cells to have)
    if(!_movingObstMap->getDataUTM<bool>(_layerNumMovingObst, point.N, point.E)){
      interpolateCell(point);
    }
  }
  
  _mapChanges[_currentProcessingMapIndex]->resetDelta<int>(_layerNumInterpolationChanges[_currentProcessingMapIndex]);
  
  return OK;
}

bool CDynCostFuser::updateNewMoving(movingObstacle obstacle, bool addObstacle) {
 
  //calculate limits for the search of cells to change in the obstacle map.

  int i = 0;
  double maxNorthTemp, minNorthTemp, maxEastTemp, minEastTemp;

  double maxNorth = max(obstacle.cornerPos[i].N + obstacle.side1.N + obstacle.side2.N, 
			max(obstacle.cornerPos[i].N, max(obstacle.cornerPos[i].N + obstacle.side1.N, 
							 obstacle.cornerPos[i].N + obstacle.side2.N)));

  double maxEast = max(obstacle.cornerPos[i].E + obstacle.side1.E + obstacle.side2.E, 
		       max(obstacle.cornerPos[i].E, max(obstacle.cornerPos[i].E + obstacle.side1.E, 
							obstacle.cornerPos[i].E + obstacle.side2.E)));

  double minNorth = min(obstacle.cornerPos[i].N + obstacle.side1.N + obstacle.side2.N, 
			min(obstacle.cornerPos[i].N, min(obstacle.cornerPos[i].N + obstacle.side1.N, 
							 obstacle.cornerPos[i].N + obstacle.side2.N)));

  double minEast = min(obstacle.cornerPos[i].E + obstacle.side1.E + obstacle.side2.E, 
		       min(obstacle.cornerPos[i].E, min(obstacle.cornerPos[i].E + obstacle.side1.E, 
							obstacle.cornerPos[i].E + obstacle.side2.E)));


  i++;
  for( ; i < NBR_OF_SAMPLES; i++){

   maxNorthTemp = max(obstacle.cornerPos[i].N + obstacle.side1.N + obstacle.side2.N, 
		      max(obstacle.cornerPos[i].N, max(obstacle.cornerPos[i].N + obstacle.side1.N, 
						       obstacle.cornerPos[i].N + obstacle.side2.N)));

   maxEastTemp = max(obstacle.cornerPos[i].E + obstacle.side1.E + obstacle.side2.E, 
		     max(obstacle.cornerPos[i].E, max(obstacle.cornerPos[i].E + obstacle.side1.E, 
						      obstacle.cornerPos[i].E + obstacle.side2.E)));

   minNorthTemp = min(obstacle.cornerPos[i].N + obstacle.side1.N + obstacle.side2.N, 
		      min(obstacle.cornerPos[i].N, min(obstacle.cornerPos[i].N + obstacle.side1.N, 
						       obstacle.cornerPos[i].N + obstacle.side2.N)));

   minEastTemp = min(obstacle.cornerPos[i].E + obstacle.side1.E + obstacle.side2.E, 
		     min(obstacle.cornerPos[i].E, min(obstacle.cornerPos[i].E + obstacle.side1.E, 
						      obstacle.cornerPos[i].E + obstacle.side2.E)));

  if(maxNorthTemp>maxNorth)
    maxNorth = maxNorthTemp;
  if(maxEastTemp>maxEast)
    maxEast = maxEastTemp;
  if(minNorthTemp<minNorth)
    minNorth = minNorthTemp;
  if(minEastTemp<minEast)
    minEast = minEastTemp;
}

  int maxRows, maxCols, minRows, minCols;
  int corner1Win[NBR_OF_SAMPLES][2];
  int corner2Win[NBR_OF_SAMPLES][2];
  int corner3Win[NBR_OF_SAMPLES][2];
  int corner4Win[NBR_OF_SAMPLES][2];
  int side1Win[NBR_OF_SAMPLES][2];
  int side2Win[NBR_OF_SAMPLES][2];

  _movingObstMap->UTM2Win(maxNorth, maxEast, &maxRows, &maxCols);
  _movingObstMap->UTM2Win(minNorth, minEast, &minRows, &minCols);

  for(int k=0; k< NBR_OF_SAMPLES;k++){
    _movingObstMap->UTM2Win(obstacle.cornerPos[k].N, obstacle.cornerPos[k].E, &corner1Win[k][0], &corner1Win[k][1]);

    _movingObstMap->UTM2Win(obstacle.cornerPos[k].N + obstacle.side1.N, obstacle.cornerPos[k].E + obstacle.side1.E, 
			    &corner2Win[k][0], &corner2Win[k][1]);

    _movingObstMap->UTM2Win(obstacle.cornerPos[k].N + obstacle.side1.N + obstacle.side2.N, 
			    obstacle.cornerPos[k].E + obstacle.side1.E + obstacle.side2.E, 
			    &corner3Win[k][0], &corner3Win[k][1]);

    _movingObstMap->UTM2Win(obstacle.cornerPos[k].N + obstacle.side2.N, obstacle.cornerPos[k].E + obstacle.side2.E, 
			    &corner4Win[k][0], &corner4Win[k][1]);
    side1Win[k][0] = corner2Win[k][0] - corner1Win[k][0];
    side1Win[k][1] = corner2Win[k][1] - corner1Win[k][1];
    side2Win[k][0] = corner4Win[k][0] - corner1Win[k][0];
    side2Win[k][1] = corner4Win[k][1] - corner1Win[k][1];  
  }

  NEcoord point;

  //Check all cells inside the calculated search area. The inner for-loop goes through 
  //the vehicles old positions.
  for(int i=minRows; i<=maxRows; i++){
    for(int j = minCols; j <= maxCols; j++){
      for(int k=0; k<NBR_OF_SAMPLES; k++){	
	int tempvec1[2] = {i - corner1Win[k][0], j - corner1Win[k][1]};
	int tempvec2[2] = {i - corner3Win[k][0], j - corner3Win[k][1]};
	//does the cell belong to the moving obstacle? If so,set the cell in the MO map
	//to true and mark the change.

	if(dot(tempvec1, side1Win[k])>=0 && dot(tempvec1, side2Win[k])>=0 && 
	   dot(tempvec2, side2Win[k])<=0 && dot(tempvec2, side1Win[k])<=0){
	  bool& mapValue = _movingObstMap->getDataWin<bool>(_layerNumMovingObst, i, j);
	  if(addObstacle){
	    mapValue = true;
	    _movingObstMap-> Win2UTM(i,j,&point.N, &point.E);	
	    markChangesCost(point);
	  }else{
	    mapValue = false;
	  }
	  break;
	}      
      }
    } 
  }
  return true;
}


bool CDynCostFuser::updateActiveMoving(movingObstacle obstacle, bool addObstacle) {

  int corner1Win[2], corner2Win[2], corner3Win[2], corner4Win[2];
  int side1Win[2], side2Win[2];
 
  _movingObstMap->UTM2Win(obstacle.cornerPos[0].N, obstacle.cornerPos[0].E, 
			  &corner1Win[0], &corner1Win[1]);

  _movingObstMap->UTM2Win(obstacle.cornerPos[0].N + obstacle.side1.N, 
			  obstacle.cornerPos[0].E + obstacle.side1.E, 
			  &corner2Win[0], &corner2Win[1]);

  _movingObstMap->UTM2Win(obstacle.cornerPos[0].N + obstacle.side1.N + obstacle.side2.N, 
			  obstacle.cornerPos[0].E + obstacle.side1.E + obstacle.side2.E, 
			  &corner3Win[0], &corner3Win[1]);

  _movingObstMap->UTM2Win(obstacle.cornerPos[0].N + obstacle.side2.N, 
			  obstacle.cornerPos[0].E + obstacle.side2.E, 
			  &corner4Win[0], &corner4Win[1]);

  side1Win[0] = corner2Win[0] - corner1Win[0];
  side1Win[1] = corner2Win[1] - corner1Win[1];
  side2Win[0] = corner4Win[0] - corner1Win[0];
  side2Win[1] = corner4Win[1] - corner1Win[1];  
  
  //calculate limits for the search of cells to change in the moving obstacle map:
  int maxRows, maxCols, minRows, minCols;

  maxRows = max(corner1Win[0] + side1Win[0] + side2Win[0], 
		max(corner1Win[0], max(corner1Win[0] + side1Win[0], corner1Win[0] + side2Win[0])));

  maxCols = max(corner1Win[1] + side1Win[1] + side2Win[1], 
		max(corner1Win[1], max(corner1Win[1] + side1Win[1],corner1Win[1] + side2Win[1])));
  
  minRows = min(corner1Win[0] + side1Win[0] + side2Win[0], 
		min(corner1Win[0], min(corner1Win[0] + side1Win[0], corner1Win[0] + side2Win[0])));
  
  minCols = min(corner1Win[1] + side1Win[1] + side2Win[1], 
		min(corner1Win[1], min(corner1Win[1] + side1Win[1],corner1Win[1] + side2Win[1])));

  NEcoord point;

  //Check all cells inside the calculated search area. The inner for-loop goes through 
  //the vehicles old positions.
  for(int i=minRows; i<=maxRows; i++){
    for(int j = minCols; j <= maxCols; j++){	
      int tempvec1[2] = {i - corner1Win[0], j - corner1Win[1]};
      int tempvec2[2] = {i - corner3Win[0], j - corner3Win[1]};
	//does the cell belong to the moving obstacle? If so,set the cell in the MO map
	//to true and mark the change.

      if(dot(tempvec1, side1Win)>=0 && dot(tempvec1, side2Win)>=0 && 
	 dot(tempvec2, side2Win)<=0 && dot(tempvec2, side1Win)<=0){
	bool& mapValue = _movingObstMap->getDataWin<bool>(_layerNumMovingObst, i, j);
	if(addObstacle){
	  mapValue = true;
	  _movingObstMap-> Win2UTM(i,j,&point.N, &point.E);	
	  markChangesCost(point);
	}else{
	  mapValue = false;
	}
      }    
    }
  } 
  return true;
}

CCostFuser::STATUS CDynCostFuser::fuseCellCost(NEcoord point) {


  double finalSpeed = 0.0;
  double speedNoDataVal = _mapRDDF->getLayerNoDataVal<double>(_layerNumRDDF);
  double rddfSpeed = _mapRDDF->getDataUTM<double>(_layerNumRDDF, point.N, point.E);
  double superConSpeed = _mapSuperCon->getDataUTM<double>(_layerNumSuperCon, point.N, point.E);
  double superConNoDataVal = _mapSuperCon->getLayerNoDataVal<double>(_layerNumSuperCon);
  bool& movingObstVal = _movingObstMap->getDataUTM<bool>(_layerNumMovingObst, point.N, point.E);

  //Are we using data association?
  if(_options->optDA){

    if(rddfSpeed != speedNoDataVal) {
      //Get the data from the terrain fusion.		   
      finalSpeed = _mapOutput->getDataUTM<double>(_layerNumCombinedElevCost, point.N, point.E);
      double roadSpeed = _mapRoad->getDataUTM<double>(_layerNumRoad, point.N, point.E);
      double roadNoDataSpeed = _mapRoad->getLayerNoDataVal<double> (_layerNumRoad);
      double terrainCostNoDataVal = _mapOutput->getLayerNoDataVal<double>(_layerNumCombinedElevCost);

      //Set the speed-limit to the max speed and remove the data from the cell in the terrain layers 
      //if there is a moving obstacle at the cell and if the cell has been fused as an obstacle or as 
      //a no data area.
      if(movingObstVal){
	if(finalSpeed == terrainCostNoDataVal || finalSpeed < DA_THRESHOLD){
	
	  finalSpeed = setMOspeed(point);
	  _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, finalSpeed);
	//Remove data belonging to MO in the terrain Layers.
	for(int i=0; i < _numElevLayers; i++) {
	  double& layerCost = _mapPtrsElevCost[i]->getDataUTM<double>(_layerNumsElevCost[i], point.N, point.E);
	  double noData  =  _mapPtrsElevCost[i]->getLayerNoDataVal<double>(_layerNumsElevCost[i]);
	  layerCost = noData;
	}
	double& layerCost = _mapOutput->getDataUTM<double>(_layerNumCombinedElevCost, point.N, point.E);
	double noData =  _mapOutput->getLayerNoDataVal<double>(_layerNumCombinedElevCost);
	layerCost =noData;


	// If the cell has been fused to a high speed-limit area, the moving obstacle classification information
	//is probably wrong, so fuse the cell as usual and remove the cell from the moving obstacle map.
	} else {
	  movingObstVal = false;
	  if(superConSpeed == superConNoDataVal){
	    if(finalSpeed != terrainCostNoDataVal) {
	      if( finalSpeed > 1.0) {
		paintRoadCell(&finalSpeed,&roadSpeed,NULL,&roadNoDataSpeed);
	      } else if(finalSpeed < MIN_SPEED && finalSpeed > speedNoDataVal) {
		finalSpeed = speedNoDataVal;
	      }
	      
	      finalSpeed  = (rddfSpeed > 0) ? finalSpeed = fmin(finalSpeed, rddfSpeed) : finalSpeed = fmin(finalSpeed, -1.0*rddfSpeed);
	      
	      //And of course, we don't want a speed higher than our max speed
	      finalSpeed = fmin(finalSpeed, _options->maxSpeed);
	      //And we want the larger of speeds from above and the min speed of the map
	      finalSpeed = fmax(finalSpeed, speedNoDataVal + EPSILON_SPEED);
	      
	      _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, finalSpeed);
	      
	    } else if(_options->paintRoadWithoutElevation) {
	      if(roadSpeed != roadNoDataSpeed){
		finalSpeed = -3.0;
		finalSpeed  = (rddfSpeed > 0) ?
		  finalSpeed = fmin(finalSpeed, rddfSpeed) :
		  finalSpeed = fmin(finalSpeed, -1.0*rddfSpeed);				
		
		_mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, finalSpeed);
	      }
	    }
	  } else{
	    _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, superConSpeed);
	  }
	}
	//If there is no moving obstacle at the cell, just fuse the cell as usual.
      } else {
	if(superConSpeed == superConNoDataVal){
	  if(finalSpeed != terrainCostNoDataVal) {
	    if( finalSpeed > 1.0) {
	      paintRoadCell(&finalSpeed,&roadSpeed,NULL,&roadNoDataSpeed);
	    } else if(finalSpeed < MIN_SPEED && finalSpeed > speedNoDataVal) {
	      finalSpeed = speedNoDataVal;
	    }
	    
	    finalSpeed  = (rddfSpeed > 0) ? finalSpeed = fmin(finalSpeed, rddfSpeed) : finalSpeed = fmin(finalSpeed, -1.0*rddfSpeed);
	    
	    //And of course, we don't want a speed higher than our max speed
	    finalSpeed = fmin(finalSpeed, _options->maxSpeed);
	    //And we want the larger of speeds from above and the min speed of the map
	    finalSpeed = fmax(finalSpeed, speedNoDataVal + EPSILON_SPEED);
	    
	    _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, finalSpeed);
	    
	  } else if(_options->paintRoadWithoutElevation) {
	    if(roadSpeed != roadNoDataSpeed){
	      finalSpeed = -3.0;
	      finalSpeed  = (rddfSpeed > 0) ?
		finalSpeed = fmin(finalSpeed, rddfSpeed) :
		finalSpeed = fmin(finalSpeed, -1.0*rddfSpeed);				
	      
	      _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, finalSpeed);
	    }
	  }
	} else{
	  _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, superConSpeed);
	}
      }
    }
 
    //If data association i turned off:
  } else {

    if(rddfSpeed != speedNoDataVal) {

      //If there is no moving obstacle at the cell, fuse cell as usual.
      if(!movingObstVal){
	//Get the data from the terrain fusion.		   
	finalSpeed = _mapOutput->getDataUTM<double>(_layerNumCombinedElevCost, point.N, point.E);
	double roadSpeed = _mapRoad->getDataUTM<double>(_layerNumRoad, point.N, point.E);
	double roadNoDataSpeed = _mapRoad->getLayerNoDataVal<double> (_layerNumRoad);
	double terrainCostNoDataVal = _mapOutput->getLayerNoDataVal<double>(_layerNumCombinedElevCost);

	if(superConSpeed == superConNoDataVal){
	  if(finalSpeed != terrainCostNoDataVal) {
	    if( finalSpeed > 1.0) {
	      paintRoadCell(&finalSpeed,&roadSpeed,NULL,&roadNoDataSpeed);
	    } else if(finalSpeed < MIN_SPEED && finalSpeed > speedNoDataVal) {
	      finalSpeed = speedNoDataVal;
	    }
	    
	    finalSpeed  = (rddfSpeed > 0) ? finalSpeed = fmin(finalSpeed, rddfSpeed) : finalSpeed = fmin(finalSpeed, -1.0*rddfSpeed);
	    
	    //And of course, we don't want a speed higher than our max speed
	    finalSpeed = fmin(finalSpeed, _options->maxSpeed);
	    //And we want the larger of speeds from above and the min speed of the map
	    finalSpeed = fmax(finalSpeed, speedNoDataVal + EPSILON_SPEED);
	    
	    _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, finalSpeed);
	    
	  } else if(_options->paintRoadWithoutElevation) {
	    if(roadSpeed != roadNoDataSpeed){
	      finalSpeed = -3.0;
	      finalSpeed  = (rddfSpeed > 0) ?
		finalSpeed = fmin(finalSpeed, rddfSpeed) :
		finalSpeed = fmin(finalSpeed, -1.0*rddfSpeed);				
	      
	      _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, finalSpeed);
	    }
	  }
	} else{
	  _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, superConSpeed);
	}

	//if there is a moving obstacle at the cell, set the speed-limit to the max speed of the road
      }else{
	finalSpeed = setMOspeed(point);
	_mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, finalSpeed);
	//_mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E, rddfSpeed); 
	
      }
    }
  }
  return OK;
}




CCostFuser::STATUS CDynCostFuser::dataAssociation(){
  NEcoord point;
  int numCells = _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaSize(_layerNumDAChanges[_currentProcessingMapIndex]);
  for(int i=0; i < numCells; i++) {
    _mapChanges[_currentProcessingMapIndex]->getCurrentDeltaVal<int>(i, _layerNumDAChanges[_currentProcessingMapIndex], &point.N, &point.E);
    dataAssociationCell(point);
  }
  
  _mapChanges[_currentProcessingMapIndex]->resetDelta<int>(_layerNumDAChanges[_currentProcessingMapIndex]);
  
  return OK;
}


CCostFuser::STATUS CDynCostFuser::dataAssociationCell(NEcoord point) {
  int r, c; //contains point in Win coordinates
  _mapOutput->UTM2Win(point.N, point.E, &r, &c);
  bool MO = _movingObstMap->getDataWin<bool>(_layerNumMovingObst, r, c);
  //is there a moving obstacle at the cell?
  if(MO) { 
    int minLocalRow, maxLocalRow, minLocalCol, maxLocalCol;
    //Make sure that we don't try to look for cells outside the map
    _mapOutput->constrainRows(r, 1, minLocalRow, maxLocalRow);
    _mapOutput->constrainCols(c, 1, minLocalCol, maxLocalCol);
     
    //check all neighbors to the cell
    for(int localR=minLocalRow; localR<=maxLocalRow; localR++) {
      for(int localC=minLocalCol; localC<=maxLocalCol; localC++) {
	//does the neighboring cell also belong to the moving obstacle?
	//if it doesn't but has been fused as an obstacle anyway, it probably
	//belongs to the moving obstacle, so change the speedlimit and check its neighbors.
	bool& movingObst = _movingObstMap->getDataWin<bool>(_layerNumMovingObst, localR, localC);
	if(!movingObst) {
	  double currentCellCost = _mapOutput->getDataWin<double>(_layerNumCombinedElevCost, localR, localC);
	  double noDataVal = _mapOutput->getLayerNoDataVal<double>(_layerNumCombinedElevCost);
	  if(currentCellCost < DA_THRESHOLD && currentCellCost != noDataVal){ 
	    NEcoord newPoint;
	    movingObst = true;
	    _mapOutput->Win2UTM(localR, localC,&newPoint.N, &newPoint.E);
	    double finalSpeed = setMOspeed(newPoint);
	    _mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, newPoint.N, newPoint.E, finalSpeed);
	    dataAssociationCell(newPoint);
	  }
	}
      }
    }
  }
  return OK;
}


int CDynCostFuser::subtractMovingRec(NEcoord point) {
  int r, c; //contains point in Win coordinates
  _movingObstMap->UTM2Win(point.N, point.E, &r, &c);
  bool& MO = _movingObstMap->getDataWin<bool>(_layerNumMovingObst, r, c);
  //cerr << "Entered subtractMoving rec, MO value at (" << point.N << "," << point.E << ") is " << MO << endl;
  if(MO){
    MO = false;
    int minLocalRow, maxLocalRow, minLocalCol, maxLocalCol;
    //Make sure that we don't try to look for cells outside the map
    _movingObstMap->constrainRows(r, 1, minLocalRow, maxLocalRow);
    _movingObstMap->constrainCols(c, 1, minLocalCol, maxLocalCol);
    for(int localR=minLocalRow; localR<=maxLocalRow; localR++) {
      for(int localC=minLocalCol; localC<=maxLocalCol; localC++) {
	bool& movingObst = _movingObstMap->getDataWin<bool>(_layerNumMovingObst, localR, localC);
	if(movingObst) {
	  NEcoord newPoint;
	  _movingObstMap->Win2UTM(localR, localC,&newPoint.N, &newPoint.E);
	  //cout << "Subtracting moving at: (" << newPoint.N << "," << newPoint.E << ")" << endl;
	  //movingObst = false;
	  subtractMovingRec(newPoint);

	}
      }
    }
  }
return 0;
}



CCostFuser::STATUS CDynCostFuser::markChangesCostAll() {
  NEcoord point;
  for(int r=0; r < _mapOutput->getNumRows(); r++) {
    for(int c=0; c < _mapOutput->getNumCols(); c++) {
      _mapOutput->Win2UTM(r, c, &point.N, &point.E);
      markChangesCost(point);
      
    }
  }  
  return OK;
}

double CDynCostFuser::setMOspeed(NEcoord point){
  double speedlimit;
  speedlimit = _mapOutput->getDataUTM<double>(_layerNumOutputCost,_mapOutput->getVehLocUTMNorthing(),_mapOutput->getVehLocUTMEasting()); 
  double rddfSpeed = _mapRDDF->getDataUTM<double>(_layerNumRDDF, point.N, point.E); 
  if(speedlimit>rddfSpeed || speedlimit<DA_THRESHOLD)
    speedlimit=rddfSpeed;
  return speedlimit;
}
