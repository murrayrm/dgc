#include "GPSPoint.hh"
using namespace std;

GPSPoint::GPSPoint(int m, int n, int p, double latitude, double longitude)
{
  segmentID = m;
  laneID = n;
  waypointID = p;
  this->latitude = latitude;
  this->longitude = longitude;
  entry = exit = false;
}

GPSPoint::~GPSPoint()
{
}

int GPSPoint::getSegmentID()
{
  return this->segmentID;
}

int GPSPoint::getLaneID()
{
  return this->laneID;
}

int GPSPoint::getWaypointID()
{
  return this->waypointID;
}

double GPSPoint::getLatitude()
{
  return this->latitude;
}

double GPSPoint::getLongitude()
{
  return this->longitude;
}

NEcoord GPSPoint::getNEcoord()
{
  GisCoordLatLon waypoint_LL;
  GisCoordUTM waypoint_UTM;
  NEcoord waypoint;

  waypoint_LL.latitude = latitude;
  waypoint_LL.longitude = longitude;

  gis_coord_latlon_to_utm(&waypoint_LL,
			  &waypoint_UTM,
			  GIS_GEODETIC_MODEL_WGS_84);

  waypoint.N = waypoint_UTM.n;
  waypoint.E = waypoint_UTM.e;

  return waypoint;
}

vector<GPSPoint*> GPSPoint::getEntryPoints()
{
  return entryPoints;
}

void GPSPoint::setEntry()
{
  this->entry = true;
}

void GPSPoint::setExit(GPSPoint* entryGPSPoint)
{
  this->exit = true;
  this->entryPoints.push_back(entryGPSPoint);
}

bool GPSPoint::isEntry()
{
  return this->entry;
}

bool GPSPoint::isExit()
{
  return this->exit;
}
