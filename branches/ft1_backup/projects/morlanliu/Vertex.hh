#ifndef VERTEX_HH_
#define VERTEX_HH_
#include "Edge.hh"
#include <vector>
#include <iostream>
using namespace std;

/*! Vertex class. Represents a vertex of a graph. A vertex contains a
 *  segmentID, laneID, and waypointID. A vertex may contain one or more
 *  edges.
 * \brief The Vertex class used in the Graph class.
 */
class Vertex
{
public:
/*! All vertices have a segmentID, laneID, and waypointID. */
	Vertex(int segmentID, int laneID, int waypointID);
	virtual ~Vertex();
  
/*! Returns the segmentID of THIS. */
  int getSegmentID();
  
/*! Returns the laneID of THIS. */
  int getLaneID();
  
/*! Returns the waypointID of THIS. */
  int getWaypointID();
  
/*! Returns the vector of edges contained in THIS. */
  vector<Edge*>* getEdges();
  
/*! Adds a pointer to an edge to the vector of edges. */
  void addEdge(Edge* edge);
  
/*! Prints the segmentID, laneID, and waypointID of THIS. */
  void print();
  
private:
  int segmentID, laneID, waypointID;
  vector<Edge*> edges;
};

#endif /*VERTEX_HH_*/
