#ifndef WAYPOINT_HH_
#define WAYPOINT_HH_
#include "GPSPoint.hh"
#include <vector>
#include <iostream>
using namespace std;

/*! Waypoint class. The Waypoint class represents a waypoint provided by the
 * RNDF file. All waypoints have a segment ID, lane ID, waypoint ID, longitude,
 * and latitude. Waypoints are designated as a checkpoint, entry waypoint,
 * exit waypoint, or a stop sign.
 * \brief The waypoint class represents a waypoint provided by the RNDF file.
 */
class Waypoint : public GPSPoint
{
public:
/*! All waypoints have a segment ID, lane ID, waypoint ID, longitude, and
 * latitude. Initially, each waypoint is not designated as a checkpoint,
 * entry waypoint, exit waypoint, nor a stop sign. */
	Waypoint(int segmentID, int laneID, int waypointID,
    double latitude, double longitude);
	virtual ~Waypoint();

/*! If this waypoint is also a checkpoint, returns the checkpoint ID. */
  int getCheckpointID();
  
/*! Sets THIS as a checkpoint and sets the checkpoint ID. */
  void setCheckpointID(int checkpointID);
  
/*! Sets THIS as a stop sign. */
  void setStopSign();
  
/*! Returns TRUE iff THIS is a checkpoint. */
  bool isCheckpoint();
  
/*! Returns TRUE iff THIS is a stop sign. */
  bool isStopSign();
  
protected:
  int checkpointID;
  
/*! Waypoints can be designated as a checkpoint, stop sign, entry waypoint, 
 *  or exit waypoint. */  
  bool checkpoint, stopSign;
};

#endif
