#ifndef PARKINGSPOT_HH_
#define PARKINGSPOT_HH_
#include "Waypoint.hh"
using namespace std;

/*! ParkingSpot class. The ParkingSpot class represents a parking spot provided
 *  by the RNDF file. All parking spots have a zone ID, spotID, and two waypoints.
 *  Parking spots may have an optional spot width.
 * \brief The ParkingSpot class represents a parking spot provided by the RNDF.
 */
class ParkingSpot
{
public:
/*! All parking spots have a zoneID or spotID. */
	ParkingSpot(int zoneID, int spotID);
	virtual ~ParkingSpot();
  
/*! Sets the spot width of THIS. */
  void setSpotWidth(int spotWidth);
  
/*! Sets waypointID of THIS to WAYPOINT. */
  void setWaypoint(int waypointID, Waypoint* waypoint);
  
/*! Returns the zoneID of THIS. */
  int getZoneID();
  
/*! Returns the spotID of THIS. */
  int getSpotID();
  
/*! Returns the spot width of THIS. */
  int getSpotWidth();
  
/*! Returns a pointer to a Waypoint with the waypointID passed in as an argument. */
  Waypoint* getWaypoint(int waypointID);
  
private:
  int zoneID, spotID, spotWidth;
  Waypoint* waypoint1;
  Waypoint* waypoint2;
};

#endif /*PARKINGSPOT_H_*/
