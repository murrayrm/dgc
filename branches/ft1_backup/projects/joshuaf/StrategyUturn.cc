//SUPERCON STRATEGY TITLE: U-turn - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyUturn.hh"
#include "StrategyHelpers.hh"

void CStrategyUturn::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE
  currentStageName = uturn_stage_names_asString( uturn_stage_names(stgItr.nextStage()) );
  
  switch( stgItr.nextStage() ) {

  case s_uturn::estop_pause_1: 
    //superCon e-stop pause Alice prior to shifting into reverse
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->validAliceStop == false ) {
	SuperConLog().log( STR, WARNING_MSG, "WARNING: Uturn called when not in a valid stop" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::broaden_corridor: //includes both lanes in valid driving space
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be while restructuring the corridor
	//and waypoints (creating local RNDF, basically)
	SuperConLog().log( STR, WARNING_MSG, "Uturn: e-stop pause not yet in place, cannot broaden corridor -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }

      if ( m_pdiag->AliceStationary == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Alice not yet stationary, cannot broaden corridor -> will poll" );
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE stage_useWholeRoad ()!!!! Lives in StrategyInterface.cc      
//	m_pStrategyInterface->stage_fm_useWholeRoad ();
	m_pStrategyInterface->stage_tp_useWholeRoad ();
//	Broaden the corridor to include the entire road
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::exit_RNDF: //takes Alice out of normal course/waypoint sequence
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "UturnReverse: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

/*      if( m_pdiag->fm_use_whole_road_complete == false ) {
	//Fussion mapper NOT using whole road to plan path yet. 
	//DEFINE fm_use_whole_road_complete!!!
	//Lives in Diagnostic.cc, interface_superCon_fmap.hh, StateServer.cc, SuperCon.hh
	SuperConLog().log( STR, WARNING_MSG, "Uturn: FM not using whole road, cannot exit RNDF -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_broadenRDDF, stgItr.currentStageCount() );	
      }
*/      
      if( m_pdiag->tp_use_whole_road_complete == false ) {
	//Traffic planner NOT using whole road to plan path yet. 
	//DEFINE tp_use_whole_road_complete!!!
	//Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
	SuperConLog().log( STR, WARNING_MSG, "Uturn: TP not using whole road, cannot exit RNDF -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_broadenRDDF, stgItr.currentStageCount() );	
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE stage_exitRoute()!!!! Lives in StrategyInterface.cc      
	m_pStrategyInterface->stage_exitRoute();
	//exit RNDF-scale route to allow violation of sensible route during
	//U-turn exectution
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;
    
  //Places an exit waypoint at the last point of the turn  
  case s_uturn::place_waypoint_exit:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->exitRoute_complete == false ) {
        //NOT yet stepped out of the RNDF-scale route 
        //DEFINE exitRoute_complete!!!
        //Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
        SuperConLog().log( STR, WARNING_MSG, "Uturn: TPlanner still in RNDF, cannot place waypoints -> will poll" );
        skipStageOps = true;
	checkForPersistLoop( this, persist_loop_exit_route, stgItr.currentStageCount() );	
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE how and where to place waypoints      
	m_pStrategyInterface->stage_placeWaypoint (/*?*/);
	//stage_placeWaypoint as yet undefined!!!
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  //Checks if we can reach the desired exit waypoint simply by turning toward
  //it.
  case s_uturn::check_for_uturn:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->placeWaypoint_complete == false ) {
	//DEFINE exitRoute_complete!!!
        //Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Waypoint-exit not yet placed" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_alterWaypoint, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
        SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	//NEED TO DEFINE how and where to place waypoints      
	m_pStrategyInterface->stage_findWaypoint (/*?*/);
	//stage_findWaypoint as yet undefined!!!
	stgItr.incrStage();
      }
    }
    break;

  //If findWaypoint ends up returning a waypoint, this places a waypoint at the
  //first point of the 3-point turn. If it does not return a waypoint, that
  //means that Alice can successfully find her way out of the u-turn on a
  //simple trajectory, so step ahead to estop_run_exit
  case s_uturn::place_waypoint_1:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->findWaypoint_complete == false) {
	//DEFINE findWaypoint_complete!!!
        //Needs: Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_pln.hh (new section), StateServer.cc (new section)
        skipStageOps = true;
      }

	//DEFINE exitRoute_complete!!!
        //Needs: Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_pln.hh (new section), StateServer.cc (new section)
      if(( skipStageOps == false ) && (m_pdiag->PlnFailure_Easting == 0 )) {
	  stgItr.SetStage(uturn_stage_names::estop_run_exit);
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE how and where to place waypoints      
	m_pStrategyInterface->stage_placeWaypoint (/*?m_pdiag->PlnFailure_Easting, m_pdiag->PlnFailure_Northing?*/);
	//stage_placeWaypoint as yet undefined!!!
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  //superCon e-stop run Alice now that she is ready to go to waypoint 1  
  case s_uturn::estop_run_1:
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "LturnReverse: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }      

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->placeWaypoint_complete == false ) {
	//DEFINE exitRoute_complete!!!
        //Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Waypoint-1 not yet placed" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_alterWaypoint, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_run);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::estop_pause_2: //pauses Alice again when she gets to waypoint 1
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "UturnReverse: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->atWaypoint == false ) {
         //NOT yet at Waypoint 1 
        //DEFINE atWaypoint!!!
        //Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
	//Use reached_end_of_traj (from trajF interfaces struct) to define this
        SuperConLog().log( STR, WARNING_MSG, "Uturn: Not yet at Waypoint 1 -> will poll" );
        skipStageOps = true;
	/* Should there be a timeout option here? If so, how long should it be?
	checkForPersistLoop( this, persist_loop_?, stgItr.currentStageCount() );*/	
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::delete_waypoint_0: //gets rid of wpt 1 before placing to wpt 2
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "UturnReverse: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be before things get under way
	SuperConLog().log( STR, WARNING_MSG, "Uturn: e-stop pause not yet in place, cannot delete waypt 1 -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }

      if ( m_pdiag->AliceStationary == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Alice not yet stationary, cannot cannot delete waypt 1 -> will poll" );
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE how and where to delete waypoints      
	m_pStrategyInterface->stage_deleteWaypoint (/*?*/);
	//stage_deleteWaypoint as yet undefined!!!
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  //places a waypoint at point 2 of the 3-point turn
  case s_uturn::place_waypoint_2:
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->deleteWaypoint_complete == false ) {
        //Have NOT yet deleted old waypoint 
        //DEFINE deleteWaypoint_complete!!!
        //Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
        SuperConLog().log( STR, WARNING_MSG, "Uturn: 1st waypoint not deleted, cannot place 2nd -> will poll" );
        skipStageOps = true;
	checkForPersistLoop( this, persist_loop_alterWaypoint, stgItr.currentStageCount() );	
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE how and where to place waypoints      
	m_pStrategyInterface->stage_placeWaypoint (/*?*/);
	//stage_placeWaypoint as yet undefined!!!
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;
    
  case s_uturn::gear_reverse: //send-gear change command to adrive ?-->reverse
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if ( m_pdiag->placeWaypoint_complete ) {
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Waypoint not yet in place -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_alterWaypoint, stgItr.currentStageCount() );
      }
      
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be to change gear, don't send gear change
	//command until Alice is paused
	SuperConLog().log( STR, WARNING_MSG, "Uturn: superCon e-stop pause not yet in place -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }
      
      if( m_pdiag->safeGearChangeConds == false ) {
	//conditions NOT yet safe for Alice to change gear -> don't send gear change command yet

	skipStageOps = true;	
	SuperConLog().log( STR, WARNING_MSG, "Uturn: alice not yet stationary (safeGearChangeConds == false), can't change gear -> will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_changeGear(sc_interface::reverse_gear);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::trajF_reverse: //send change-mode command to TrajFollower ?-->reverse-MODE
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->adriveGearReverse == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "Uturn - Alice not yet in reverse gear -> will poll" );
	checkForPersistLoop( this, persist_loop_gearChange, stgItr.currentStageCount() );	
      }

      if( m_pdiag->superConPaused == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, ERROR_MSG, "ERROR: Uturn - not yet paused (will NOT change TFmode yet) - will poll" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_changeTFmode( tf_reverse );
        //Need to define (overload) stage_changeTFmode (...) for one arg!!!!
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;
    
  case s_uturn::estop_run_2: //superCon e-stop run Alice now that she is ready to REVERSE
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }      

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->trajFReverse == false ) {
	SuperConLog().log( STR, WARNING_MSG, "Uturn: TrajFollower not yet in reverse mode" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_trajfModeChange, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_run);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::estop_pause_3: //pauses Alice again when she gets to waypoint 2
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "UturnReverse: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->atWaypoint == false ) {
         //NOT yet at Waypoint 2 
        //DEFINE atWaypoint!!!
        //Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
	//Maybe do this check internally with State data & Waypoint data? Would
	//probably be easier for traffic planner to check. Ask Jessica.
        SuperConLog().log( STR, WARNING_MSG, "Uturn: Not yet at Waypoint 2 -> will poll" );
        skipStageOps = true;
	/* Should there be a timeout option here? If so, how long should it be?
	checkForPersistLoop( this, persist_loop_?, stgItr.currentStageCount() );*/	
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
        skipStageOps = true;
	/* Should there be a timeout option here? If so, how long should it be?
	checkForPersistLoop( this, persist_loop_?, stgItr.currentStageCount() );*/	
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::delete_waypoint_1: //gets rid of wpt 1 before placing to wpt 3
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "UturnReverse: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
        skipStageOps = true;
	/* Should there be a timeout option here? If so, how long should it be?
	checkForPersistLoop( this, persist_loop_?, stgItr.currentStageCount() );*/	
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::delete_waypoint_1: //gets rid of wpt 1 before placing to wpt 3
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "UturnReverse: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be before things get under way
	SuperConLog().log( STR, WARNING_MSG, "Uturn: e-stop pause not yet in place, cannot delete waypt 2 -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }

      if ( m_pdiag->AliceStationary == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Alice not yet stationary, cannot cannot delete waypt 2 -> will poll" );
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE how and where to delete waypoints      
	m_pStrategyInterface->stage_deleteWaypoint (/*?*/);
	//stage_deleteWaypoint as yet undefined!!!
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::gear_drive: //send-gear change command to adrive ?-->drive
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if ( m_pdiag->placeWaypoint_complete ) {
	SuperConLog().log( STR, WARNING_MSG, "Uturn: Waypoint not yet in place -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_alterWaypoint, stgItr.currentStageCount() );
      }
      
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be to change gear, don't send gear change
	//command until Alice is paused
	SuperConLog().log( STR, WARNING_MSG, "Uturn: superCon e-stop pause not yet in place -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }
      
      if( m_pdiag->safeGearChangeConds == false ) {
	//conditions NOT yet safe for Alice to change gear -> don't send gear change command yet

	skipStageOps = true;	
	SuperConLog().log( STR, WARNING_MSG, "Uturn: alice not yet stationary (safeGearChangeConds == false), can't change gear -> will poll");
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_changeGear(sc_interface::drive_gear);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "LturnReverse - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_uturn::trajF_forwards: //send change-mode command to TrajFollower ?-->reverse-MODE
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->adriveGearReverse == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "Uturn - Alice not yet in drive gear -> will poll" );
	checkForPersistLoop( this, persist_loop_gearChange, stgItr.currentStageCount() );	
      }

      if( m_pdiag->superConPaused == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, ERROR_MSG, "ERROR: Uturn - not yet paused (will NOT change TFmode yet) - will poll" );
      }
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
        SuperConLog().log( STR, WARNING_MSG, "Uturn: 1st waypoint not deleted, cannot place 2nd -> will poll" );
        skipStageOps = true;
	checkForPersistLoop( this, persist_loop_alterWaypoint, stgItr.currentStageCount() );	
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE stage_exitRoute()!!!! Lives in StrategyInterface.cc      
	m_pStrategyInterface->stage_enterRoute();
	//exit RNDF-scale route to allow violation of sensible route during
	//U-turn exectution
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }

  case s_uturn::estop_run_exit:
    //We are currently lined up to leave the uturn maneuver by reaching the
    //exit waypoint defined in place_exit_waypoint stage. We should be in
    //trajF_forwards mode before proceding.
    {
/* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->trajFReverse == false ) {
	SuperConLog().log( STR, WARNING_MSG, "Uturn: TrajFollower not yet in reverse mode" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_trajfModeChange, stgItr.currentStageCount() );
      }

      if( m_pdiag->superConPaused == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, ERROR_MSG, "ERROR: Uturn - not yet paused (will NOT change TFmode yet) - will poll" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_run);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }

  case s_uturn::reenter_RNDF:
    //As soon as we get to the exit waypoint for the Uturn, Alice will want
    //to start listening to RoutePlanner again to set up the next series of
    //waypoints to follow to get back on course for the rest of the mission.
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }      

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE stage_enterRoute()!!!! Lives in StrategyInterface.cc
	m_pStrategyInterface->stage_enterRoute();
	//exit RNDF-scale route to allow violation of sensible route during
	//U-turn exectution
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }

    }
    break;
    
  case s_uturn::wait_new_route: 
    //Road block placed in Route map, Alice turned around. Wait for route 
    //planner to succeed. If it does NOT, then delete the oldest road block
    //and try again until we get a valid route. It is nice to turn around
    //before checking for a valid route because road blocks allow U-turns on
    //streets where the Route Planner thinks they are illegal.
    { 
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) ); 

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      
     /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

     /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
     if( m_pdiag->enterRoute_complete == false ) {
        //NOT yet stepped back in to the RNDF-scale route 
        //DEFINE enterRoute_complete!!!
        //Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
        SuperConLog().log( STR, WARNING_MSG, "Uturn: TPlanner still outside RNDF, cannot use Route -> will poll" );
        skipStageOps = true;
	checkForPersistLoop( this, persist_loop_exit_route, stgItr.currentStageCount() );	
      }
      
     if( m_pdiag->routeReceived == false ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "StrategyUturn: planner NOT yet output a valid route -> will poll");

	//Timeout handled here as this deletes the oldest road block and
	//tries to plan a route again. Resets the stage timer for next try.
        if( stgItr.currentStageCount() > TIMEOUT_LOOP_WAIT_FOR_NEW_ROUTE ) {
	  SuperConLog().log( STR, WARNING_MSG, "WARNING: StrategyUturn - wait for rte timed out -> kill oldest block" );
	  //DeleteOldRoadBlock -- How?
	  stgItr.setCnt (0)		  
	}	
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	transitionStrategy( StrategyNominal, "Uturn - Strategy complete --> nominal" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Uturn - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

    /** END OF STRATEGY **/
    
  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyUturn::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyUturn::stepForward - default case in nextStep switch() reached" );
      transitionStrategy(StrategyNominal, "ERROR: Uturn - default stage reached!" );
    }
  }
}


void CStrategyUturn::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "U-turn - Exiting" );
}

void CStrategyUturn::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  skipStageOps.reset(); //resets to FALSE 
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "U-turn - Entering" );
}


