#ifndef INTERFACE_SUPERCON_ADRIVE_HH
#define INTERFACE_SUPERCON_ADRIVE_HH

//FILE DESCRIPTION: Header file that contains the definitions of data structures that are
//used in direct communication between SuperCon and adrive

namespace sc_interface
{

//SC --> ADRIVE: adrive command struct definition
#include "adrive_skynet_interface_types.h"

//SC --> ADRIVE: adrive transmission arguments
#define TRANS_LIST(_) \
  _( reverse_gear, = -1 ) \
  _( park_gear, = 0 ) \
  _( drive_gear, = 1 )
DEFINE_ENUM(adriveTransCmd, TRANS_LIST)
    
//SC --> ADRIVE: adrive estop arguments
#define ESTOP_LIST(_) \
  _( estp_disable, = 0 ) \
  _( estp_pause, = 1 ) \
  _( estp_run, = 2 )
DEFINE_ENUM(adriveEstpCmd, ESTOP_LIST)    
    
//ADRIVE --> SC: used to decode messages from adrive
enum obd2StatusValue { OBD2_OFF = 0, OBD2_ON = 1 };
  
}

#endif
