RDDFPLANNER_PATH = $(DGC)/projects/ndutoit/rddfPlanner

RDDFPLANNER_DEPEND_MODULES = \
	$(MODULEHELPERSLIB) \
	$(SKYNETLIB) \
	$(RDDFLIB) \
	$(GGISLIB) \
	$(TRAJLIB) \
	$(TIMBERLIB) \
	$(PLANNERLIB) \
	$(NLPSOLVERLIB) \
	$(SPARROWLIB) \
	$(SPARROWHAWKLIB)

RDDFPLANNER_DEPEND_SOURCES = \
	$(RDDFPLANNER_PATH)/RddfPlanner.hh \
	$(RDDFPLANNER_PATH)/RddfPlanner.cc \
	$(RDDFPLANNER_PATH)/pathLib.hh \
	$(RDDFPLANNER_PATH)/pathLib.cc \
	$(RDDFPLANNER_PATH)/vectorOps.hh \
	$(RDDFPLANNER_PATH)/vectorOps.cc \
	$(RDDFPLANNER_PATH)/trajDFE.hh \
	$(RDDFPLANNER_PATH)/trajDFE.cc

RDDFPLANNER_DEPEND = \
	$(RDDFPLANNER_DEPEND_MODULES) \
	$(RDDFPLANNER_DEPEND_SOURCES)
