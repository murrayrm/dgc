#ifndef _have_obsv
#define _have_obsv


#include <math.h>

#include "DGCutils"           // I'm sure it will be useful for something

#include "AliceConstants.h"

#include "falcon/matrix.h"

#include <iostream>
#include <fstream>
#include <iomanip>

class CObserver {
 public:
	CObserver();
	~CObserver();

	bool loadFile(char* filename);

	void set_initial_conditions(double* x_hat);

	double* obs_compute(double* input, double gamma);

	bool loaded();

	MATRIX *Q, *R, *P, *dP, *x_hat;
	MATRIX *A, *B, *C, *L, *y, *F;

	MATRIX *old_x_hat, *dx_hat, *cor;

	unsigned long long last_update;
	
	double output[6];

	double covar[36];

	double last_v;
	double last_phi;

	ofstream logFile;

 private:
	bool first_run;

	bool obs_create();
	bool obs_setup(MATRIX* list);
};

#endif 
