
V_0 = 3;   % speed of the vehicle- meters/sec

% loop gain
K = .05;
% frequency at which the derivative term rolls off
T_r = 15;
% derivative gain
T_d = 5;
% integral gain
T_i = 40;

run run_gains

save v_3set1.mat