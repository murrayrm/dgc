#include "ladarFeeder.hh"

#define CONFIG_PATH_PREFIX "./config/ladar/LadarCal.ini."
#define CONFIG_BUMPER_SUFFIX "bumper"
#define CONFIG_ROOF_SUFFIX "roof"
#define CONFIG_SMALL_SUFFIX "small"
#define CONFIG_RIEGL_SUFFIX "riegl"
#define CONFIG_FRONT_SUFFIX "front"

#define ROLL_AVG_POINTS 3
#define ROLL_CALIB_EPS 0.00001
#define ROLL_CALIB_THRESH 0.001
#define PITCH_CALIB_EPS 0.00001
#define PITCH_CALIB_THRESH 0.001

#define PFIXME fprintf(stderr, "%s [%d]: FIXME\n", __FILE__, __LINE__)

LadarFeeder::LadarFeeder(int skynetKey, LadarFeederOptions ladarOptsVal) 
  : CSkynetContainer(MODladarFeeder, skynetKey), 
    CStateClient(!(ladarOptsVal.optSource==ladarSource::SOURCE_FILES)), 
    CTimberClient(timber_types::ladarFeeder) {

		ladarOpts = ladarOptsVal;
		_QUIT = 0;
		_PAUSE = 0;
		_STEP = 0;
		_EOL = 0;
		_numDeltas = 0;

		if(ladarOptsVal.optPause) {
			_PAUSE = 1;
		}

		char configFilename[256];
		switch(ladarOpts.optLadar) {
		case LADAR_BUMPER:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_BUMPER_SUFFIX);
			_msgTypeFusedElev = SNladarBumperDeltaMap;
			_msgTypeMeanElev = SNladarBumperDeltaMapElev;
			_msgTypeStdDev = SNladarBumperDeltaMapStdDev;
			_msgTypeNum = SNladarBumperDeltaMapNum;
			_msgTypePitch = SNladarBumperDeltaMapPitch;
			break;
		case LADAR_ROOF:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_ROOF_SUFFIX);
			_msgTypeFusedElev = SNladarRoofDeltaMap;
			_msgTypeMeanElev = SNladarRoofDeltaMapElev;
			_msgTypeStdDev = SNladarRoofDeltaMapStdDev;
			_msgTypeNum = SNladarRoofDeltaMapNum;
			_msgTypePitch = SNladarRoofDeltaMapPitch;
			break;
		case LADAR_SMALL:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_SMALL_SUFFIX);
			_msgTypeFusedElev = SNladarSmallDeltaMap;
			_msgTypeMeanElev = SNladarSmallDeltaMapElev;
			_msgTypeStdDev = SNladarSmallDeltaMapStdDev;
			_msgTypeNum = SNladarSmallDeltaMapNum;
			_msgTypePitch = SNladarSmallDeltaMapPitch;
			break;
		case LADAR_RIEGL:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_RIEGL_SUFFIX);
			_msgTypeFusedElev = SNladarRieglDeltaMap;
			_msgTypeMeanElev = SNladarRieglDeltaMapElev;
			_msgTypeStdDev = SNladarRieglDeltaMapStdDev;
			_msgTypeNum = SNladarRieglDeltaMapNum; 
			_msgTypePitch = SNladarRieglDeltaMapPitch;
			break;
		case LADAR_FRONT:
			sprintf(configFilename, "%s%s", CONFIG_PATH_PREFIX, CONFIG_FRONT_SUFFIX);
			_msgTypeFusedElev = SNladarFrontDeltaMap;
			_msgTypeMeanElev = SNladarFrontDeltaMapElev;
			_msgTypeStdDev = SNladarFrontDeltaMapStdDev;
			_msgTypeNum = SNladarFrontDeltaMapNum;
			_msgTypePitch = SNladarFrontDeltaMapPitch;
			break;
		default:
			if(ladarOpts.optSource==ladarSource::SOURCE_FILES) {
				sprintf(configFilename, "%s.header", ladarOpts.logFilenamePrefix);
			} else {
				PFIXME;
			}
      break;
		}

		if(ladarOpts.optLadar != LADAR_RIEGL) {
			ladarMap.initMap(CONFIG_FILE_DEFAULT_MAP);
		} else {
			ladarMap.initMap(CONFIG_FILE_LOWRES_MAP);
		}

		layerID_ladarElev       = ladarMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV,           true);
		layerID_ladarElevFused  = ladarMap.addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV, true);
		layerID_ladarElevStdDev = ladarMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV,              true);
		layerID_ladarElevNum    = ladarMap.addLayer<double>(CONFIG_FILE_LADAR_NUM,                 true);
		layerID_ladarElevPitch  = ladarMap.addLayer<double>(CONFIG_FILE_LADAR_PITCH,               true);
  
		if(ladarOpts.optTimber) {
			getPlaybackDir();
			sprintf(ladarOpts.logFilenamePrefix, "%sladar_%s", getPlaybackDir().c_str(), ladarOpts.ladarString);
		}

		if(ladarObject.init(ladarOpts.optSource, 
												configFilename, 
												ladarOpts.logFilenamePrefix, 
												ladarOpts.optLog, 
												ladarOpts.optLadar)!=ladarSource::OK) {
			printf("%s [%d]: LADAR could not initialize properly.  Error was '%s.'  Quitting.\n",
						 __FILE__, __LINE__, ladarObject.getErrorMessage());
			exit(1);
		}

		if(ladarOpts.optReset) {
			printf("%s [%d]: Resetting the LADAR.\n", __FILE__, __LINE__);
			ladarObject.reset();
			printf("%s [%d]: Done!  If this was the Reigl, nothing happened - otherwise try accessing the LADAR again in 1 minute\n", __FILE__, __LINE__);
			exit(1);
		}

		ladarPositionOffset = ladarObject.getLadarPosition();
		ladarAngleOffset = ladarObject.getLadarAngle();

		DGCcreateMutex(&ladarMapMutex);

		processingTime = 0;
		for(int i=0; i<CYCLES_TO_TIME_OVER; i++) {
			timingArray[i] = 0;
		}
		timingArrayIndex = 0;

		_estopRun = false;
		//_careAboutEstop = true;
  
#ifdef LADAR_GUI
		mapDisp = new MapDisplayThread("ladarFeeder", new MapConfig);
		mapDisp->get_map_config()->set_cmap(&(ladarMap), &ladarMapMutex);
		mapDisp->get_map_config()->set_state_struct(&m_state);

		if(ladarOpts.optMapDisp) {
			mapDisp->guithread_start_thread();
		}
#else
		if(ladarOpts.optMapDisp) {
			cout << __FILE__ << "[" << __LINE__ << "]: "
					 << "Built-in GUI support was not enabled during compilation.  To enable, please define the environment variable LADAR_GUI" << endl
					 << "(For example, do: 'export LADAR_GUI=1')" << endl;
			exit(1);
		}
#endif
	}


LadarFeeder::~LadarFeeder() {
  //FIX ME
  PFIXME;
  printf("Thank you for using the ladarmapper destructor\n");
}


void LadarFeeder::ActiveLoop() {
  unsigned long long scan_timestamp;

  NEDcoord UTMPoint;

  unsigned long long start, end;

  ladarMeasurementStruct myMeasurement;

  //unsigned long loopCounter = 0;

  int socket_num_ladar = 0;
  
  if(ladarOpts.optLadar==LadarFeeder::LADAR_ROOF) {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas_roof);
  }
  else if(ladarOpts.optLadar==LadarFeeder::LADAR_BUMPER) {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas_bumper);
  }
  else {
    socket_num_ladar = m_skynet.get_send_sock(SNladarmeas);
  }

char pointsFileName[256];
  sprintf(pointsFileName, "points.dat");
   if(m_outputPoints.is_open()) 
    {
      m_outputPoints.close();
    }
  m_outputPoints.open(pointsFileName);
  m_outputPoints << setprecision(5);


	RPYangle ladarOrientation = ladarObject.getLadarAngle();
	double measHeightVar;

  while(!_QUIT) {
    if(!_PAUSE || _STEP) {
      _STEP = 0;
      /*
				if(ladarObject.scanIndex() >= ladarOpts.optMaxScans && ladarOpts.optMaxScans>=0) {
				return;
				}
      */
      DGCgettime(start);

      if(!ladarOpts.optLog) {
				if(getLoggingEnabled()) {
					if(checkNewDirAndReset()) {
						ladarObject.stopLogging();
						ladarOpts.loggingOpts.logRaw = 1;	
						ladarOpts.loggingOpts.logState = 1;
						ladarOpts.loggingOpts.logRanges = 0;
						ladarOpts.loggingOpts.logRelative = 0;
						ladarOpts.loggingOpts.logUTM = 0;
						sprintf(ladarOpts.logFilenamePrefix, "%sladar_%s", getLogDir().c_str(), ladarOpts.ladarString);
						ladarObject.startLogging(ladarOpts.loggingOpts, ladarOpts.logFilenamePrefix);
					}
				} else {
					ladarObject.stopLogging();
				}
      }

      //ladarObject.updateState(m_state) must come after ladarObject.scan()
      //printf("%s [%d]: Got here\n", __FILE__, __LINE__);
      if(!ladarOpts.optTimber) {
				scan_timestamp = ladarObject.scan();
      } else {
				unsigned long long timberTime = getPlaybackTime();
				scan_timestamp = ladarObject.scan(timberTime);
				blockUntilTime(scan_timestamp);
				if(scan_timestamp==0) {
					//exit(0);
					_EOL = 1;
					sleep(1);
					return;
				}
      }
      // update for sparrow display
      m_active_time = scan_timestamp;

      //if(offset==0) offset = playback_state.playback_time_point;
      UpdateState(scan_timestamp, true);
      if(ladarOpts.optZeroAltitude) {
				m_state.Altitude = 0.0;
      }
      ladarObject.updateState(m_state);


      DGClockMutex(&ladarMapMutex);
      ladarMap.updateVehicleLoc(m_state.Northing, m_state.Easting);

      // road finding ladar information

      myMeasurement.numPoints = ladarObject.numPoints();

      myMeasurement.timeStamp = m_state.Timestamp;

			myMeasurement.offset = ladarObject.getLadarPosition();
			myMeasurement.angles = ladarObject.getLadarAngle();

      myMeasurement.data[0].ranges = ladarObject.range(0);
      myMeasurement.data[0].angles = ladarObject.angle(0);

      if(ladarOpts.optCalibrate) {
				RPYangle newRPYCalib = ladarObject.getLadarAngle();
				XYZcoord newXYZCalib = ladarObject.getLadarPosition();
				double newRoll;
				double newPitch;
				newRoll = GetCalibRoll();
				if(ladarOpts.optCalibrate==2)
					cout << setprecision(10) << "Calibrating... ";
				if(fabs(newRoll) < 0.5) {
					if(ladarOpts.optCalibrate==2)
						cout << "Changing roll from: " << newRPYCalib.R << " to " << newRPYCalib.R+newRoll << endl;
					newRPYCalib.R += newRoll;
					ladarObject.setLadarFrame(newXYZCalib, newRPYCalib);
				} else {
					newPitch = GetCalibPitch();
					if(fabs(newPitch) < 0.5) {
						if(ladarOpts.optCalibrate==2)
							cout << "Changing pitch from: " << newRPYCalib.P << " to " << newRPYCalib.P+newPitch << endl;
						newRPYCalib.P += newPitch;
						ladarObject.setLadarFrame(newXYZCalib, newRPYCalib);
					} else {
						if(ladarOpts.optCalibrate==2)
							cout << "Final calibration obtained.  Roll=" << newRPYCalib.R << ", Pitch=" << newRPYCalib.P
									 << " - quitting"  << endl;
						exit(0);
					}
				}
      }

			if(_estopRun || !_careAboutEstop) {
				for(int i=1; i<ladarObject.numPoints(); i++) {      
					myMeasurement.data[i].ranges = ladarObject.range(i);
					myMeasurement.data[i].angles = ladarObject.angle(i);

					//Do not uncomment - breaks synchronization
					// 				UpdateState(ladarObject.timestamp(i), true);
					// 				if(ladarOpts.optZeroAltitude) {
					// 					m_state.Altitude = 0.0;
					// 				}
					// 				ladarObject.updateState(m_state);

				
					UTMPoint = ladarObject.UTMPoint(i);
					//ladarMap.setDataUTM_Delta<double>(layerID_ladarElev, UTMPoint.N, UTMPoint.E, UTMPoint.D);
					CElevationFuser tempCell = ladarMap.getDataUTM<CElevationFuser>(layerID_ladarElevFused, UTMPoint.N, UTMPoint.E);

					if(ladarOpts.optKF) {
						measHeightVar = abs(ladarObject._ladarSigma*sin(m_state.Pitch+ladarOrientation.P));
						tempCell.fuse_KFElevation(UTMPoint, scan_timestamp, measHeightVar);
					} else {
						tempCell.fuse_MeanElevation(UTMPoint, scan_timestamp);
					}
					ladarMap.setDataUTM_Delta<CElevationFuser>(layerID_ladarElevFused, UTMPoint.N, UTMPoint.E, tempCell);
					ladarMap.setDataUTM_Delta<double>(layerID_ladarElev, UTMPoint.N, UTMPoint.E,
																						tempCell.getMeanElevation());
					if(ladarOpts.optKF) {
						ladarMap.setDataUTM_Delta<double>(layerID_ladarElevStdDev, UTMPoint.N, UTMPoint.E,
																							tempCell.getHeightVar()*1000);
					} else {
						ladarMap.setDataUTM_Delta<double>(layerID_ladarElevStdDev, UTMPoint.N, UTMPoint.E,
																							tempCell.getStdDev());
					}
					ladarMap.setDataUTM_Delta<double>(layerID_ladarElevNum, UTMPoint.N, UTMPoint.E,
																						(double)tempCell.getNumPoints());
					ladarMap.setDataUTM_Delta<double>(layerID_ladarElevPitch, UTMPoint.N, UTMPoint.E,
																						m_state.Pitch*100.0);
				}
			}

      if(ladarOpts.optLadar==LadarFeeder::LADAR_ROOF
				 || ladarOpts.optLadar==LadarFeeder::LADAR_BUMPER) {
				static int cnt=0;
	
				if (cnt==LADARFEEDER_SEND_SCAN_INTERVAL) // Only send every Nth scan
					{ 
						//cout<<"Sending "<<myMeasurement.numPoints<<" points  "<<LADARMEASUREMENTSTRUCTBASE + 2*sizeof(double)*myMeasurement.numPoints<<" bytes"<<endl;
						m_skynet.send_msg(socket_num_ladar, ((void*)&myMeasurement), 
															LADARMEASUREMENTSTRUCTBASE + 2*sizeof(double)*myMeasurement.numPoints,0);
						cnt=0;
					}
				cnt++;
      }

      DGCunlockMutex(&ladarMapMutex);

      if(ladarOpts.optDeltaRate == 0.0) {
				SendMapDeltas();
      }



			/*
				if(ladarOpts.optDelay!=-1) {
				usleep((unsigned int)(ladarOpts.optDelay*1e6));
				}
				if(ladarOpts.optRate!=-1) {
				usleep((unsigned int)((1/ladarOpts.optRate)*1e6));
				} 
      */
      DGCgettime(end);
      timingArray[timingArrayIndex%CYCLES_TO_TIME_OVER] = end-start;      
      timingArrayIndex++;
      unsigned long long totalProcessingTime = 0;
      for(int i=0; i<CYCLES_TO_TIME_OVER; i++) {
				totalProcessingTime+=timingArray[i];
      }
      processingTime = totalProcessingTime/CYCLES_TO_TIME_OVER;

#ifdef LADAR_GUI
      if(ladarOpts.optMapDisp) {
				mapDisp->notify_update();
      }
#endif
    } else {
			DGCusleep(500000);
		}
  }
}


void LadarFeeder::ReceiveDataThread() {
  printf("Data receiving not yet");
}


void LadarFeeder::SendDataThread() {
  unsigned long long start, end;

  unsigned long long totalCycleTime = (unsigned long long)(1.0e6/ladarOpts.optDeltaRate);

  while(!_QUIT) {
    DGCgettime(start);
    SendNED();
    SendMapDeltas();
    DGCgettime(end);
    if(end-start < totalCycleTime) {
      usleep(totalCycleTime - (end - start));
    }
  }    
}


void LadarFeeder::SaveMapsThread() {
  unsigned long long start, end;
	char mapName[128];
  unsigned long long totalCycleTime = (unsigned long long)(1.0e6/ladarOpts.optSaveMapsRate);

  while(!_QUIT) {
		if(!ladarOpts.optTimber) {
			DGCgettime(start);
		} else {
			start = getPlaybackTime();
		}

		DGClockMutex(&ladarMapMutex);    

		sprintf(mapName, "%s-elev-%lld", ladarOpts.ladarString, start);
		ladarMap.saveLayer<double>(layerID_ladarElev, mapName, true);

		sprintf(mapName, "%s-stddev-%lld", ladarOpts.ladarString, start);
		ladarMap.saveLayer<double>(layerID_ladarElevStdDev, mapName, true);

		sprintf(mapName, "%s-num-%lld", ladarOpts.ladarString, start);
		ladarMap.saveLayer<double>(layerID_ladarElevNum, mapName, true);

		DGCunlockMutex(&ladarMapMutex);

		if(!ladarOpts.optTimber) {
			DGCgettime(end);
			if(end-start < totalCycleTime) {
				usleep(totalCycleTime - (end - start));
			}
		} else {
			end = getPlaybackTime();
			if(end-start < totalCycleTime) {
				blockUntilTime(start+totalCycleTime);
			}
		}
  }    
}


void LadarFeeder::SendMapDeltas() {
  CDeltaList* deltaPtr = NULL;
  int socket_num = 0;
  int socket_num_fused = 0;
  int socket_num_stddev = 0;
  int socket_num_num = 0;
	int socket_num_pitch = 0;

  socket_num = m_skynet.get_send_sock(_msgTypeMeanElev);
  socket_num_fused = m_skynet.get_send_sock(_msgTypeFusedElev);
  socket_num_stddev = m_skynet.get_send_sock(_msgTypeStdDev);
  socket_num_num = m_skynet.get_send_sock(_msgTypeNum);
	socket_num_pitch = m_skynet.get_send_sock(_msgTypePitch);

  unsigned long long scan_timestamp;

  DGClockMutex(&ladarMapMutex);    
  DGCgettime(scan_timestamp);
  
  deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElev);
  if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1 && ladarOpts.optSendElev==1) {
    SendMapdelta(socket_num, deltaPtr);
    _numDeltas++;
  }
  ladarMap.resetDelta<double>(layerID_ladarElev);    
  
  deltaPtr = ladarMap.serializeDelta<CElevationFuser>(layerID_ladarElevFused);
  if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1) {
    SendMapdelta(socket_num_fused, deltaPtr);
    _numDeltas++;
  }
  ladarMap.resetDelta<CElevationFuser>(layerID_ladarElevFused);    

  deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElevStdDev);
  if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1 && ladarOpts.optSendStdDev==1) {
    SendMapdelta(socket_num_stddev, deltaPtr);
    _numDeltas++;
  }
  ladarMap.resetDelta<double>(layerID_ladarElevStdDev);    

  deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElevNum);
  if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1 && ladarOpts.optSendNum==1) {
    SendMapdelta(socket_num_num, deltaPtr);
    _numDeltas++;
  }
  ladarMap.resetDelta<double>(layerID_ladarElevNum);    
  
  deltaPtr = ladarMap.serializeDelta<double>(layerID_ladarElevPitch);
  if(!deltaPtr->isShiftOnly() && ladarOpts.optCom==1 && ladarOpts.optSendPitch==1) {
    SendMapdelta(socket_num_pitch, deltaPtr);
    _numDeltas++;
  }
  ladarMap.resetDelta<double>(layerID_ladarElevPitch);    
  
  DGCunlockMutex(&ladarMapMutex);
}


void LadarFeeder::SendNED() {

  //really, just want to send ladarObject.UTMPoint array...how to do this??

  int numPts = ladarObject.numPoints();
  NEDcoord  pointsToSend[181]; //must be a constant msg size...
  ///FIXME::CHANGE STRUCT TO INCLUDE # OF POINTS AND TIMESTAMP

  //  cout<<"num points: "<<numPts<<endl;
  m_outputPoints<<m_state.Northing<<' '<<m_state.Easting<<' ';
  // building the array of NEDcoords to send
  for(int i=0; i<numPts; i++) {
      pointsToSend[i].N = ladarObject.UTMPoint(i).N;
      pointsToSend[i].E = ladarObject.UTMPoint(i).E;
      pointsToSend[i].D = ladarObject.UTMPoint(i).D;
      m_outputPoints<<ladarObject.UTMPoint(i).N<<' '<<ladarObject.UTMPoint(i).E<<' ';
  }
  //and filling in the rest...
  for(int i=numPts; i<181; i++) {
    pointsToSend[i].N = 0;
    pointsToSend[i].E = 0;
    pointsToSend[i].D = 0;
    m_outputPoints<<"0 0 ";
  }
  m_outputPoints<<endl;

  cout<<pointsToSend[0].N<<endl;

  int socket_num = 0;
  socket_num = m_skynet.get_send_sock(SNbumperLadarNED);

  unsigned long long scan_timestamp;

  DGCgettime(scan_timestamp);
  //type, *msg, size
  m_skynet.send_msg(socket_num, ((void*)&pointsToSend), 181*sizeof(NEDcoord));
  cout<<"sent msg!!"<<endl;
  
}


void LadarFeeder::ReceiveDataThread_EStop() {
	int oldStatus = -1;

	while(!_QUIT) {
		WaitForNewActuatorState();
		if(m_actuatorState.m_about_to_unpause == true || 
			 (m_actuatorState.m_estoppos == 2 && m_state.Speed2() > 0.1)){		
			_estopRun = true;
		} else{
			_estopRun = false;
		}
	}
}


double LadarFeeder::GetCalibRoll() {
	double avg_roll_s, avg_roll_e;

	if (ladarObject.numPoints() == 0){
	  if(ladarOpts.optCalibrate==2)
	    cout << "No Points obtained from the LADAR" << endl;
	  return 0;
	}
	
	avg_roll_s = 0;
	for (int i = 0 ;i < ROLL_AVG_POINTS-1; i++)
		avg_roll_s += ladarObject.UTMPoint(i).D;
	avg_roll_s /= ROLL_AVG_POINTS;

	avg_roll_e = 0;
	for (int i = ladarObject.numPoints() - ROLL_AVG_POINTS ;i < ladarObject.numPoints(); i++)
		avg_roll_e += ladarObject.UTMPoint(i).D;
	avg_roll_e /= ROLL_AVG_POINTS;

	if(fabs(avg_roll_s - avg_roll_e) < ROLL_CALIB_THRESH) 
	  return -1.0;

	if(ladarOpts.optCalibrate==2)
	  cout << "Got a diff of " << fabs(avg_roll_s - avg_roll_e) 
	       << " and s was " << avg_roll_s << " and e was " << avg_roll_e << " ";

	if (avg_roll_s < avg_roll_e)
		return  ROLL_CALIB_EPS;
	else 
		return  -ROLL_CALIB_EPS;

	
}

double LadarFeeder::GetCalibPitch() {
	double avg_pitch;
	avg_pitch = 0;

	if (ladarObject.numPoints() == 0){
	  if(ladarOpts.optCalibrate==2)
	    cout << "No Points obtained from the LADAR" << endl;
	  return 0;
	}
	
	for (int i = 0 ;i < ladarObject.numPoints(); i++)
		avg_pitch += ladarObject.UTMPoint(i).D;
	avg_pitch /= ladarObject.numPoints();

	if(fabs(avg_pitch) < PITCH_CALIB_THRESH)
	  return -1.0;

	if(ladarOpts.optCalibrate==2)
	  cout << "Got a elev of " << avg_pitch << " ";

	if (avg_pitch < 0) 
		return -PITCH_CALIB_EPS;
	else
		return PITCH_CALIB_EPS;
}
