function debugCartesian()

points = load('points.dat');

figure();
% axis([399000,399040,3781260,3781320]);
hold on;

cars = load('carsC.dat');
[rows, cols] = size(cars)

foo = 399006.7;
bah = 3781281.5;
for j=1:1:rows
    pause(.1)
    x_pts = points(2*j-1,:)-foo;
    y_pts = points(2*j,:)-bah;
    hold off;
    plot(x_pts,y_pts,'k.','LineWidth',1);

    xi=[];
    yi=[];
    x1=[];
    y1=[];
    x2=[];
    y2=[];

    for i=1:1:cols/6
        if cars(j,6*i)*cars(j,6*i-5)~=0
           xi = [xi, cars(j,6*i-5)-foo];
           yi = [yi, cars(j,6*i-4)-bah];
           x1 = [x1, cars(j,6*i-3)-foo];
           y1 = [y1, cars(j,6*i-2)-bah];
           x2 = [x2, cars(j,6*i-1)-foo];
           y2 = [y2, cars(j,6*i)-bah];
        end
    end
    hold on;
    plot(xi,yi,'ro',x1,y1,'bo',x2,y2,'go');
%    axis([398970,399035,3781270,3781320]);
    axis([-40,70,-5,40]);
    drawnow;
end

end