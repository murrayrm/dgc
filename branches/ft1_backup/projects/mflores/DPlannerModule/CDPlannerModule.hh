/*
 *  CDPlannerModule.hh
 *  CDPlannerModule
 *
 *  Created by Melvin E. Flores on 11/26/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#include "AlgebraicGeometry.h"
#include "DPlannerUtilities.h"
#include "Alice_Full.hh"
#include "OTG_OCProblem.h"
#include "OTG_OCPSolver.h"
#include "OTG_Utilities.h"

#include "StateClient.h"
#include "TrajTalker.h"
#include "RDDFTalker.hh"
#include "CPath.hh"
#include "CMap.hh"
#include "CMapPlus.hh"
#include "MapConstants.h"
#include "DGCutils"

enum AliceProblemType{apt_Full, apt_Nominal, apt_BackingUp};

class CDPlannerModule:public CStateClient, public CTrajTalker, public CRDDFTalker
{
    public:
    	CDPlannerModule(int SkynetKey, bool WAIT_STATE);
       ~CDPlannerModule(void);

		void ActiveLoop(void);
		void SwitchOCProblem(const AliceProblemType* const apType);

    private:
		int m_i;
		int m_j;
		int m_k;
		int m_ell;
		
		int m_Error;
		
		RDDF*     m_RDDF;
		CTraj*    m_Traj;
		CMapPlus* m_CostMapPlus;

		RDDFVector m_RDDFVector;

		int m_SendSocket;
		int m_RDDFSocket;
		
   		AliceProblemType m_apType;

		clock_t m_InitialTime;
		clock_t m_FinalTime;
		double m_Duration;
		
	   	CAlice_Full*    m_Alice_Full;
		COCProblem*     m_OCProblem;
		COCPSolver*     m_OCPSolver;
		CAlgebraicGeometry* m_AlgGeom;
	
		int m_SolverInform;
		double m_OptimalCost;
				
		int  m_NofFlatOutputs;
		int* m_NofWeights;
		int* m_NofDimensionsOfFlatOutputs;
		int* m_NofControlPoints;
												
		double**  m_Weights;
		double*** m_ControlPoints;
		
		int m_NofConstraintCollocPoints;
		int m_NofStates;
		int m_NofInputs;		
		
		double*  m_Time;
		double** m_States;
		double** m_Inputs;
		
		int m_NofWaypoints;
		int m_start_waypoint;
		int m_stop_waypoint;
		
		CPolytope* m_Polytope;
		
		int m_NofDimensions;
		int m_NofPoints;
		double** m_Points;
		
		int m_NofVertices;
		int* m_Vertices;
		
		int m_NofRowsOfHform;
		int m_NofColsOfHform;
		
		double** m_A;
		double*  m_b;
	
		double m_x_start;
		double m_y_start;
		double m_v_start;
		
		double m_x_stop;
		double m_y_stop;
		double m_v_stop;
	
		double m_theta_start;
		
		int     m_NofLIC;
		double* m_LIC;
		
		int     m_NofLFC;
		double* m_LFC;
	
		
};

