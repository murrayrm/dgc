/* 
	DPlannerUtilities.cc
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	Created on 12-14-2006.
*/ 

#define _USE_MATH_DEFINES

#ifdef __cplusplus
extern "C" {
#endif

#include "DPlannerMath.h"

#ifdef __cplusplus
}
#endif

#include <math.h>
#include <iostream>
#include <iomanip>

#include "AlgebraicGeometry.hh"
#include "DPlannerUtilities.hh"

using namespace std;

CPolytope* ConstructPolytope(int start_waypoint, int stop_waypoint, double** RDDF_Data)
{
	CPolytope* Polytope = NULL;

	int i = 0;
	int j = 0;
	int k = 0;
	
	double th = 0.0;

	int NofSides      = 8;
	int NofDimensions = 2;
	int NofPoints     = NofSides + 2; 

	double slope  = 0.0;
	double theta  = 0.0;
	double offset = 0.0;

	double** Points = new double*[NofDimensions];
	for(i=0; i<NofDimensions; i++)
	{
		Points[i] = new double[NofPoints];
	}

	double x_start = RDDF_Data[start_waypoint][1];
	double y_start = RDDF_Data[start_waypoint][2];
	double v_start = RDDF_Data[start_waypoint][3];
	double r_start = RDDF_Data[start_waypoint][4];

	double x_stop = RDDF_Data[stop_waypoint][1];
	double y_stop = RDDF_Data[stop_waypoint][2];
	double v_stop = RDDF_Data[stop_waypoint][3];
	double r_stop = RDDF_Data[stop_waypoint][4];

	slope = (y_stop - y_start)/(x_stop - x_start);
	theta = atan2(y_stop - y_start,x_stop - x_start);

	k = 0;

	offset = M_PI_2 + theta;
	for(th = 0.0 + offset; th <= M_PI + offset; th = th + 2*M_PI/NofSides)
	{
		Points[0][k] = x_start + r_start*cos(th);
		k = k + 1;
	}

	offset = -M_PI_2 + theta;
	for(th = 0.0 + offset; th <= M_PI + offset; th = th + 2*M_PI/NofSides)
	{
		Points[0][k] = x_stop + r_start*cos(th);
		k = k + 1;
	}

	k = 0;

	offset = M_PI_2 + theta;
	for(th = 0.0 + offset; th <= M_PI + offset; th = th + 2*M_PI/NofSides)
	{
		Points[1][k] = y_start + r_start*sin(th);
		k = k + 1;
	}

	offset = -M_PI_2 + theta;
	for(th = 0.0 + offset; th <= M_PI + offset; th = th + 2*M_PI/NofSides)
	{
		Points[1][k] = y_stop + r_start*sin(th);
		k = k + 1;
	}

	Polytope = new CPolytope(NofDimensions, NofPoints, Points);
	
	return Polytope;
}

void ConstructInitialGuess(int NofFlatOutputs, NURBS* Nurbs, TimeInterval* timeInterval, int start_waypoint, int stop_waypoint, double** RDDF_Data, double** Weights, double*** ControlPoints)
{
	CNURBSBuilder NurbsLib;

	int i   = 0;
	int j   = 0;
	int k   = 0;
	int ell = 0;
	int n   = 0;

	for(i=0; i<NofFlatOutputs; i++)
	{
		for(j=0; j<Nurbs[i].NofWeights; j++){Weights[i][j] = 1.0;}
	}

	int FlatOutput = 0;

	int Np = Nurbs[FlatOutput].NofPolynomials;
	int d  = Nurbs[FlatOutput].DegreeOfPolynomials;
	int Cs = Nurbs[FlatOutput].CurveSmoothness;
	
	double* BreakPoints = new double[Nurbs[FlatOutput].NofBreakPoints];
    CreateTimeVector(timeInterval, &Nurbs[FlatOutput].NofBreakPoints, BreakPoints);

	int NofCollocPoints = Np*((d+1)-(Cs+1))+(Cs+1);
	double* CollocPoints = new double[NofCollocPoints];

	CreateTimeVector(timeInterval, &NofCollocPoints, CollocPoints);
	
	double* A = new double[NofCollocPoints*NofCollocPoints];
	for(i=0; i<NofCollocPoints*NofCollocPoints; i++){A[i] = 0.0;}

	int Left = 0;
	double** BMatrix = new double*[Nurbs[FlatOutput].NofDerivatives];
	for(k=0; k<Nurbs[FlatOutput].NofDerivatives; k++){BMatrix[k] = new double[Nurbs[FlatOutput].OrderOfPolynomials];}

	int index = 0;
	for(i=0; i<NofCollocPoints; i++)
	{
		NurbsLib.SPCol(Nurbs, BreakPoints, CollocPoints[i], &Left, BMatrix);
		for(ell = Left-Nurbs[FlatOutput].DegreeOfPolynomials; ell <= Left; ell++)
		{
			index = ell*NofCollocPoints + i;
			A[index]  = BMatrix[0][ell-(Left-Nurbs[FlatOutput].DegreeOfPolynomials)];
		}
	}

	int NofChars = 1;
	char* stype = new char[1];
	stype[0] = 'N';
	int len_stype = (int) strlen(stype);

	int NofRowsOfA = NofCollocPoints;
	int NofColsOfA = NofCollocPoints;
	int LDA        = NofCollocPoints;

	int* PivotIndices = new int[NofColsOfA];
	int SolverInform;

	double x_start = RDDF_Data[start_waypoint][1];
	double y_start = RDDF_Data[start_waypoint][2];
	double x_stop = RDDF_Data[stop_waypoint][1];
	double y_stop = RDDF_Data[stop_waypoint][2];
	double slope = (y_stop - y_start)/(x_stop - x_start);
	double theta = atan2(y_stop - y_start,x_stop - x_start);

	int NofRowsOfxs = NofCollocPoints;
	int NofColsOfxs = 1;

	double* xs = new double[NofCollocPoints];
	double delta_xs = (x_stop-x_start)/(NofCollocPoints-1);
	xs[0] = x_start; xs[NofCollocPoints-1] = x_stop;
	for(i=1; i<NofCollocPoints-1; i++){xs[i] = x_start + i*delta_xs;}

	int NofRowsOfys = NofCollocPoints;
	int NofColsOfys = 1;

	double* ys = new double[NofCollocPoints];
	double delta_ys = (y_stop-y_start)/(NofCollocPoints-1);
	ys[0] = y_start; ys[NofCollocPoints-1] = y_stop;
	for(i=1; i<NofCollocPoints-1; i++){ys[i] = y_start + i*delta_ys;}
	
	dgetrf_(&NofRowsOfA, &NofColsOfA, A, &LDA, PivotIndices, &SolverInform);
	dgetrs_(stype, &NofColsOfA, &NofColsOfxs, A, &NofRowsOfA, PivotIndices, xs, &NofRowsOfxs, &SolverInform, len_stype);
	dgetrs_(stype, &NofColsOfA, &NofColsOfys, A, &NofRowsOfA, PivotIndices, ys, &NofRowsOfys, &SolverInform, len_stype);

	for(k=0; k<Nurbs[FlatOutput].NofControlPoints; k++)
	{
		ControlPoints[FlatOutput][0][k] = xs[k];
		ControlPoints[FlatOutput][1][k] = ys[k];
	}

	FlatOutput = 1;
	for(k=0; k<Nurbs[FlatOutput].NofControlPoints; k++)
	{
		ControlPoints[FlatOutput][0][k] = RDDF_Data[start_waypoint][3]/2;
	}	
	
	FlatOutput = 2;
	for(k=0; k<Nurbs[FlatOutput].NofControlPoints; k++)
	{
		ControlPoints[FlatOutput][0][k] = theta;
	}	

	FlatOutput = 3;
	for(k=0; k<Nurbs[FlatOutput].NofControlPoints; k++)
	{
		ControlPoints[FlatOutput][0][k] = 10;
	}	

	FlatOutput = 4;
	double a_max  =  0.98100/4;
	double a_min  = -3.00000/4;
	double delta_a = (a_max-a_min)/(Nurbs[FlatOutput].NofControlPoints-1);
	ControlPoints[FlatOutput][0][0] = a_min; ControlPoints[FlatOutput][0][Nurbs[FlatOutput].NofControlPoints-1] = a_max;
	for(i=1; i<Nurbs[FlatOutput].NofControlPoints-1; i++)
	{
		ControlPoints[FlatOutput][0][i] = a_min + i*delta_a;
	}

	FlatOutput = 5;
	double phi_max =  0.44942/4;
	double phi_min = -0.44942/4;
	double delta_phi = (phi_max-phi_min)/(Nurbs[FlatOutput].NofControlPoints-1);
	ControlPoints[FlatOutput][0][0] = phi_min; ControlPoints[FlatOutput][0][Nurbs[FlatOutput].NofControlPoints-1] = phi_max;
	for(i=1; i<Nurbs[FlatOutput].NofControlPoints-1; i++)
	{
		ControlPoints[FlatOutput][0][i] = phi_min + i*delta_phi;
	}

	delete[] A;

	for(k=0; k<Nurbs[FlatOutput].NofDerivatives; k++)
	{
		delete[] BMatrix[k];
	}
	delete[] BMatrix;

	delete[] BreakPoints;
	delete[] CollocPoints;

	delete[] PivotIndices;
	delete[] xs;
	delete[] ys;
	delete[] stype;

}
