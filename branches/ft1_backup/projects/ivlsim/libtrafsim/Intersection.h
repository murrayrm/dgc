#ifndef INTERSECTION_H_
#define INTERSECTION_H_

#include "Object.h"
#include "Road.h"
#include "Environment.h"

namespace TrafSim
{

class Road;
class Car;

/// ROW_GO means that you have the right of way, ROW_GAP means that you don't.
enum RightOfWay    { ROW_GO = 0, ROW_GAP };
/// An enumeration describing the three maneuvers that can be performed at a 4-way intersection.
enum CrossManeuver { CROSS_LEFT = 0, CROSS_RIGHT, CROSS_STRAIGHT };
typedef std::pair<Road*, unsigned int> RoadLane;

/// Internal data structure
struct ManeuverDestMap
{
	RoadLane data[3];
};

/// Internal data structure
struct ManeuverLaneMap
{
	Lane* data[3];
};

/// Parameters that allow you to influence the output of the automated intersection geometry
/// generation algorithm. Just play around with them if you want to see what they do. The default
/// values are generally acceptable, but it may be fruitful to fine-tune them for particular intersections.
struct IntersectionGeometryParams
{
	float narrowing;
	float sharpness;
	float leftTightening;
};

/**
 * An intersection object ties together one or more roads, and provides cars with information about
 * how to get from one lane on one road, to another lane on another road. The intersection object also
 * controls the queueing behavior of cars that wish to enter it, signals cars that have the right of way,
 * and lets them know which car they should be following. In the case of an intersection with major and 
 * minor streams of traffic, the intersection object can let approaching cars know whether they must wait
 * for a gap in oncoming traffic, and can provide lanes that represent opposing streams of traffic to examine.
 * 
 * NOTE FOR EXTERNAL CARS: A car enters the intersection's queue when it asks for right of way information.
 * 	That means, if you don't at call rightOfWay() when you approach an intersection, you will never be officially
 *  enqueued, and the other cars won't know when they're supposed to yield to you. Furthermore, you must call
 *  exitIntersection() once you finish crossing, or else other strange things may happen to the queue. In summary,
 *  USE THE RIGHT-OF-WAY AND EXIT-INTERSECTION FUNCTIONS!
 * 
 * NOTE FOR FUTURE MAINTAINERS: XIntersection and YIntersection are near-duplicates of each other. Disaster.
 */
class Intersection: public Object
{
public:
	/**
	 * No complex initialization happens in the constructor.
	 */
	Intersection();
	virtual ~Intersection();
	virtual std::string getClassID() const { return "Intersection"; }
	/**
	 * Specific intersections can visualize themselves in different ways. Usually this
	 * just involves drawing the intersection border splines.
	 */
	virtual void draw() {}
	/**
	 * Connect the 'starting' end of a road (corresponding to t=0.0 of its spline) to the intersection.
	 * \param road The road to connect
	 * \return True on success, False on failure.
	 */
	virtual bool hookStart(Road& road) { return true; }
	/**
	 * Connect the 'ending' end of a road (corresponding to t=1.0 of its spline) to the intersection.
	 * \param road The road to connect
	 * \return True on success, False on failure.
	 */
	virtual bool hookEnd(Road& road) { return true; } 
	/**
	 * For a given road and a given lane on that road, make it illegal to perform the given maneuver.
	 * All physically possible maneuvers are legal by default.
	 * \param startRoad The road from which the maneuver in question begins
	 * \param startLane The lane from which the maneuver in question begins
	 * \param maneuver The maneuver to make illegal.
	 */
	virtual bool blockManeuver(Road const& startRoad, unsigned int startLane, CrossManeuver maneuver) { return true; }
	/**
	 * For a given road and a given lane on that road, make it legal to perform the given maneuver.
	 * All physically possible maneuvers are legal by default.
	 * \param startRoad The road from which the maneuver in question begins
	 * \param startLane The lane from which the maneuver in question begins
	 * \param maneuver The maneuver to make legal.
	 */
	virtual bool unblockManeuver(Road const& startRoad, unsigned int startLane, CrossManeuver maneuver) { return true; }
	/**
	 * This is a VERY expensive call that performs a lot of pre-processing. It must be called before the intersection
	 * can actually be used to move cars around. Ideally, all intersections would have their geometry computed before
	 * an environment is saved, as computing the geometry of 10-20 intersections may take as long as a minute.
	 * \param env The environment the intersection is to reside in.
	 * \return True on success, False on failure
	 */
	virtual bool computeGeometry(Environment& env) { return true; }
	/**
	 * Finds the road and lane index that the given maneuver would take you to.
	 * \param startRoad The road from which the maneuver in question begins
	 * \param startLane The lane from which the maneuver in question begins
	 * \param maneuver The maneuver in question
	 * \return A Road* and unsigned int pair describing the road and lane that the maneuver ends up in
	 */
	 virtual RoadLane getDestination(Road const& startRoad, unsigned int startLane, CrossManeuver maneuver) const
		{ return RoadLane(); }
	/**
	 * Gets the lane that describes the geometry of the given maneuver.
	 * \param startRoad The road from which the maneuver in question begins
	 * \param startLane The lane from which the maneuver in question begins
	 * \param maneuver The maneuver in question
	 * \return A Lane* corresponding to the maneuver in question.
	 */
	virtual Lane* getLane(Road const& startRoad, unsigned int startLane, CrossManeuver maneuver)
		{ return 0; }
	/**
	 * Toggles the existence of a stop sign on the given road.
	 * \param road The road on which to place or remove a stop sign.
	 * \param stopSign True if there is to be a stop sign, False if there isn't.
	 * \return True on success, False on failure
	 */
	virtual bool setStop(Road const& road, bool stopSign) { return false; }
	/**
	 * \param road A road hooked to this intersection to examine
	 * \return True if there is a stop sign on said road, False otherwise
	 */
	virtual bool isStop(Road const& road) { return false; }
	/**
	 * Determines whether a given car has the right of way, and enqueues it into the intersection.
	 * \param thisCar The car to enqueue
	 * \param road The road the car is currently on
	 * \param followCar This parameter will be filled with the next car in line, if there is currently a queue.
	 * \return The car's right-of-way status
	 */
	virtual RightOfWay rightOfWay(Car* thisCar, Road const& road, Car** followCar) { return ROW_GO; }
	/**
	 * Informs the intersection that a car has completed its crossing maneuver and has left the intersection.
	 * \param thisCar The car that has exited the intersection.
	 */
	virtual void exitIntersection(Car* thisCar) { }
	/**
	 * Fetches lanes representing opposing streams of traffic at an intersection, which would interfere with 
	 * the specified maneuver.
	 * \param startRoad The road from which the maneuver in question begins
	 * \param startLane The lane from which the maneuver in question begins
	 * \param maneuver The maneuver in question
	 * \param result A vector to fill with lanes that may interfere with the given maneuver.
	 */
	virtual void getOpposingTraffic(Road const& startRoad, unsigned int startLane, CrossManeuver maneuver,
									std::vector<Lane*>& result) { result.clear(); }
	/**
	 * \param boundsArray A vector to fill with splines representing the physical bounds of the intersection.
	 */
	virtual void getBoundaries(std::vector<Spline*>& boundsArray) {}
	/**
	 * \return The geometric parameters of the intersection.
	 */
	IntersectionGeometryParams const& getParams() const { return params; }
	/**
	 * \param newParams The new geometric parameters of the intersection.
	 */
	void setParams(IntersectionGeometryParams const& newParams) {}
	
	/// A helper function, for Python's benefit. Python doesn't understand downcasting, so
	/// this function does it explicitly so that objects extracted from an Environment can be used.
	/// \param source The object pointer to be cast down to a Intersection
	/// \return A Intersection*, pointing to the same object as source.	
	static Intersection* downcast(Object* source);
	
protected:
	/// Writes the object to the output stream, in standard format.
	/// \param os The stream to write to.
	virtual void serialize(std::ostream& os) const {}
	/// Reads the object from the input stream, in standard format.
	/// \param is The stream to read from.
	virtual void deserialize(std::istream& is) {}
	
	IntersectionGeometryParams params;
};

/**
 * By default, all roads start hooked at both ends to DeadEnds, which are merely default intersections
 * that contain no crossing maneuvers and no geometry. When a car reaches a dead end, it is deleted
 * and no longer participates in the simulation.
 */
class DeadEnd: public Intersection
{
public:
	DeadEnd() {}
	virtual ~DeadEnd() {}
	
	virtual std::string getClassID() const { return "DeadEnd"; }
};

}

#endif /*INTERSECTION_H_*/
