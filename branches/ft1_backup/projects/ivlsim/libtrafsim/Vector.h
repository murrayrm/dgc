#ifndef VECTOR_H_
#define VECTOR_H_

#include "Serialization.h"

namespace TrafSim
{
	
/**
 * A 2D vector (the mathematical kind) utility class for TrafSim. 
 * Defines all the basic arithmetic operations, as well as an 
 * assortment of convenience functions.
 */
class Vector
{
public:
	/* ******** CONSTRUCTORS ******** */

	/// The default constructor creates the zero vector.
	Vector();
	
	/// A constructor for defining vectors inline.
	Vector(float x, float y);
	
	/* ******** BINARY ARITHMETIC OPERATIONS ******** */
	
	/// Binary plus operator.
	/// \param rhs The right hand side of the operation
	/// \return A new vector
	Vector operator+(Vector const& rhs) const;

	/// Binary minus operator. 
	/// \param rhs The right hand side of the operation
	/// \return A new vector	
	Vector operator-(Vector const& rhs) const;
	
	/// Binary multiply operator. 
	/// \param rhs The right hand side of the operation
	/// \return A new vector
	Vector operator*(float rhs) const;
	
	/// Binary divide operator.
	/// \param rhs The right hand side of the operation. Must be non-zero.
	/// \return A new vector
	Vector operator/(float rhs) const;
	
	/// Binary dot product operator.
	/// \return The dot product of the two input vectors
	static float dot(Vector const& v1, Vector const& v2);
	
	/// A convenience function, more efficient than calling (v1 - v2).length().
	/// \return The square of the distance between the vectors
	static float dist2(Vector const& v1, Vector const& v2); 
	
	/* ******** UNARY ARITHMETIC OPERATIONS ******** */
	
	/// Unary minus operator.
	/// \return The arithmetic negative of the vector
	Vector operator-() const;
	
	/// Unary plus operator.
	/// \param rhs The right hand side of the operation
	/// \return *this
	Vector& operator+=(Vector const& rhs);

	/// Unary minus operator. 
	/// \param rhs The right hand side of the operation
	/// \return *this
	Vector& operator-=(Vector const& rhs);
	
	/// Unary multiply operator. 
	/// \param rhs The right hand side of the operation
	/// \return *this
	Vector& operator*=(float rhs);
	
	/// Unary divide operator. 
	/// \param rhs The right hand side of the operation. Must be non-zero.
	/// \return *this
	Vector& operator/=(float rhs);
	
	/// sqrt(x^2 + y^2).
	/// \return The length of the vector
	float length() const;
	
	/// Normalizes the vector non-destructively.
	/// The input vector must be of non-zero length; otherwise, results are undefined.
	/// \return A new vector, of length 1.0
	Vector normalized() const;
	
	/// Gets one of the perpendicular vectors to the input vector.
	/// \return A perpendicular
	Vector perpendicular() const;
	
	/// Normalizes the vector destructively.
	/// The input vector must be of non-zero length; otherwise, results are undefined.
	void normalize();
	
	/* ******** UTILITY OPERATIONS ******** */
	
	/// Prints the vector's string representation to an output stream.
	/// \param os The output stream to print to
	/// \param v The vector object to parse
	/// \return The same output stream 
	friend std::ostream& operator<<(std::ostream& os, Vector const& v);
	
	/// Reads a vector's string representation from an input stream.
	/// \param is The input stream to read from
	/// \param v The vector object to parse
	/// \return The same input stream
	friend std::istream& operator>>(std::istream& is, Vector& v);
	
	/// Renders a vector's graphical representation. You can see it with a viewport.
	/// Note that it is not safe to call this function from a thread other than the one
	/// you opened the viewport in.
	/// \param origin The world location of the origin to draw the vector arrow from
	/// \param arrowSize The size (in OpenGL dimensions) of the arrowhead
	void draw(Vector const& origin = Vector(), float  arrowSize = 1.0f) const;
	
	/// Finds the point of intersection of the two line segments defined
	/// by the input vectors. Returns false if no such point exists.
	/// \param seg1pt1 The first point defining the first segment
	/// \param seg1pt2 The second point defining the first segment
	/// \param seg2pt1 The first point defining the second segment
	/// \param seg2pt2 The second point defining the second segment
	/// \param output Receives the point of intersection
	/// \return True if output holds the point of intersection, false if no
	/// 		such point exists.
	static bool intersection(Vector const& seg1pt1, Vector const& seg1pt2,
							 Vector const& seg2pt1, Vector const& seg2pt2,
							 Vector& output);
							 
	/* ******** PUBLIC DATA ******** */
	
	float x;  ///< The x-coordinate of the vector.
	float y;  ///< The y-coordinate of the vector.
};

}

#endif /*VECTOR_H_*/
