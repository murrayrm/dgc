//SUPERCON STRATEGY TITLE: Traffic Jam - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyTrafficJam.hh"
#include "StrategyHelpers.hh"

void CStrategyTrafficJam::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE
  currentStageName = traffic_jam_stage_names_asString( traffic_jam_stage_names(stgItr.nextStage()) );
  
  if ( ( stgItr.nextStage() == s_trafficjam::estop_pause ) && 
	  ( m_pdiag->superConPaused == true ) )
  {
    stgItr.setStage( s_trafficjam::cut_in_line );
  }
  switch( stgItr.nextStage() ) {

  case s_trafficjam::estop_pause: 
  //superCon e-stop pauses Alice if she is not already paused
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->validAliceStop == false ) {
	SuperConLog().log( STR, WARNING_MSG, "WARNING: Traffic Jam called when not in a valid stop" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(sc_interface::estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Traffic Jam - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;


  case s_trafficjam::cut_in_line: 
    //Alice will cut one place in line every 2 secs until she is at the front    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->intersectionProgress == true) {
	//see StrategyIntersection.cc      
	transitionStrategy( StrategyIntersection, "Traffic Jam - progress at intersection --> back to Intersection" );
        SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Intersection - Entering" );
	stgItr.incrStage();
      }
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( stgItr.currentStageCount < TIMEOUT_LOOP_WAIT_TWO_SECS ) {
	SkipStageOps = true;
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//Need to define stage_cutInLine
	m_pStrategyInterface->stage_cutInLine();
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "TrafficJam - Stage - %s completed", currentStageName.c_str() );
	//Need to define queuePosition
	if (m_pdiag->queuePosition == 1) {
	  stgItr.incrStage();
	}
	else {
	  stgItr.reset();
	}
      }
    }
    break;

  case s_trafficjam::check_for_Traj_1: 
    //Checks if we ar currently getting a Traj from planning.
    //If we are, then we can proceed through the intersection(skip to estop_run)
    //If we are not, then we broaden the corridor (next step).
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->intersectionProgress == true) {
	//see StrategyIntersection.cc      
	transitionStrategy( StrategyIntersection, "Traffic Jam - progress at intersection --> back to Intersection" );
        SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Intersection - Entering" );
	stgItr.incrStage();
      }
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "TrafficJam - Stage - %s completed", currentStageName.c_str() );
	if (m_pdiag->plnValidTraj == false) {
	  stgItr.incrStage();
	}
	else {
	  stgItr.setStage(6); //skips ahead to wait_for_route
	}
      }
    }
    break;

  case s_trafficjam::broaden_corridor: //includes whole intersection in driving space
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfRDDF( (*m_pdiag), currentStageName.c_str() );
      }

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->intersectionProgress == true) {
	//see StrategyIntersection.cc      
	transitionStrategy( StrategyIntersection, "Traffic Jam - progress at intersection --> back to Intersection" );
        SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Intersection - Entering" );
	stgItr.incrStage();
      }

      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->superConPaused == false ) {
	//NOT superCon paused - need to be while restructuring the corridor
	//and waypoints (creating local RNDF, basically)
	SuperConLog().log( STR, WARNING_MSG, "TrafficJam: e-stop pause not yet in place, cannot broaden corridor -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }

      if ( m_pdiag->AliceStationary == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "TrafficJam: Alice not yet stationary, cannot broaden corridor -> will poll" );
	checkForPersistLoop( this, persist_loop_waitstationary, stgItr.currentStageCount() );
      }
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	//NEED TO DEFINE stage_useWholeRoad ()!!!! Lives in StrategyInterface.cc
	m_pStrategyInterface->stage_tp_useWholeRoad ();
//	Broaden the corridor to include the entire road
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Traffic Jam - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_trafficjam::check_for_Traj_2:
    //Checks if we ar currently getting a Traj from planning.
    //If we are, then we can proceed through the intersection(wait_for_route)
    //If we are not, then we block off the road we're trying to get to
    //(next step).
    {
    /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->intersectionProgress == true) {
	//see StrategyIntersection.cc      
	transitionStrategy( StrategyIntersection, "Traffic Jam - progress at intersection --> back to Intersection" );
        SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Intersection - Entering" );
	stgItr.incrStage();
      }
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->tp_use_whole_road_complete == false ) {
	//Traffic planner NOT using whole road to plan path yet. 
	//DEFINE tp_use_whole_road_complete!!!
	//Lives in Diagnostic.cc, SuperCon.hh
	//Needs: interface_superCon_tplanner.hh (new file), StateServer.cc (new section)
	SuperConLog().log( STR, WARNING_MSG, "TrafficJam: TP not using whole road, cannot exit RNDF -> will poll" );
	skipStageOps = true;
	checkForPersistLoop( this, persist_loop_broadenRDDF, stgItr.currentStageCount() );	
      }

      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "TrafficJam - Stage - %s completed", currentStageName.c_str() );
	if (m_pdiag->plnValidTraj == false) {
	  stgItr.incrStage();
	}
	else {
	  stgItr.setStage(6); //skips ahead to wait_for_route
	}
      }
    }
    break;

  case s_trafficjam::place_block:
    //when it turns out that we can't make it to the entry waypoint for the
    //segment we planned to go to, SuperCon tells the route planner that there
    //is a road block at that entry waypoint.
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Traffic Jam: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
	if( m_pdiag->intersectionProgress == true) {
	//see StrategyIntersection.cc      
	transitionStrategy( StrategyIntersection, "Traffic Jam - progress at intersection --> back to Intersection" );
        SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Intersection - Entering" );
	stgItr.incrStage();
      }
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->NextWaypt == 0 ) {
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "StrategyTrafficJam - not recieving location of next wayoint from TPlanner -> will poll" ); 
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	//SuperCon should place a Route level obstacle on the blocked entry waypoint -- How?

	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "RoadBlock - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;
  case s_trafficjam::wait_for_route: 
    //Asks the route planner if we can successfully generate a path
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Uturn: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->intersectionProgress == true) {
	//see StrategyIntersection.cc      
	transitionStrategy( StrategyIntersection, "Traffic Jam - progress at intersection --> back to Intersection" );
        SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Intersection - Entering" );
	stgItr.incrStage();
      }
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->block_placed_complete == false ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "StrategyTrafficJam - confirmation of road block placement NOT yet received from RouteMapper -> will poll" );
	//Timeout handled here. If we fail to make a place a block, then if we 
	//try to go back to the same waypoint again, we should place the block
	//next time, hopefully.
        if( stgItr.currentStageCount() > TIMEOUT_LOOP_PLACE_BLOCK ) {
	  SuperConLog().log( STR, WARNING_MSG, "WARNING: StrategyTrafficJam - place block timed out -> hope for best" );
	}
     if( m_pdiag->routeReceived == false ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "StrategyTrafficJam: planner NOT yet output a valid route -> will poll");

	//Timeout handled here as this deletes the oldest road block and
	//tries to plan a route again. Resets the stage timer for next try.
        if( stgItr.currentStageCount() > TIMEOUT_LOOP_WAIT_FOR_NEW_ROUTE ) {
	  SuperConLog().log( STR, WARNING_MSG, "WARNING: StrategyTrafficJam - wait for route timed out -> kill oldest block" );
	  //DeleteOldRoadBlock -- How?
	  stgItr.setCnt (0)		  
	}	
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	transitionStrategy( StrategyNominal, "TrafficJam - Strategy complete --> nominal" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "TrafficJam - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

    /** END OF STRATEGY **/
    
  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyTrafficJam::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyTrafficJam::stepForward - default case in nextStep switch() reached" );
      transitionStrategy(StrategyNominal, "ERROR: TrafficJam - default stage reached!" );
    }
  }
}


void CStrategyTrafficJam::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "U-turn - Exiting" );
}

void CStrategyTrafficJam::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  skipStageOps.reset(); //resets to FALSE 
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "U-turn - Entering" );
}


