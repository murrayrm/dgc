/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#ifndef SEGGOALSTALKER_HH
#define SEGGOALSTALKER_HH_

#include "SkynetContainer.h"
#include "pthread.h"
#include <unistd.h>
#include "SegGoals.hh"

#include <string>
using namespace std;


class CSegGoalsTalker : virtual public CSkynetContainer
{
  pthread_mutex_t m_segGoalsdataBufferMutex;
  pthread_mutex_t m_segGoalsStatusdataBufferMutex;
  
  char* m_pSegGoalsDataBuffer;
  char* m_pSegGoalsStatusDataBuffer;

public:
  CSegGoalsTalker();
  ~CSegGoalsTalker();
  
  bool SendSegGoals(int segGoalsSocket, SegGoals* pSegGoals, pthread_mutex_t* pMutex = NULL);
  bool RecvSegGoals(int segGoalsSocket, SegGoals* pSegGoals, pthread_mutex_t* pMutex = NULL, string* pOutstring = NULL);
  
  bool SendSegGoalsStatus(int tplannerStatusSocket, SegGoalsStatus* tplannerStatus, pthread_mutex_t* pMutex = NULL);
  bool RecvSegGoalsStatus(int tplannerStatusSocket, SegGoalsStatus* tplannerStatus, pthread_mutex_t* pMutex = NULL, string* pOutstring = NULL);

  void WaitForSegGoalsData(int segGoalsSocket);
};

#endif //  SEGGOALSTALKER_HH
