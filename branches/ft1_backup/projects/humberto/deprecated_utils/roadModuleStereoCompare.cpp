#include <stdio.h>
#include <stdlib.h>

#include "stereovision/stereoSource.hh"
#include "stereovision/stereoProcess.hh"
#include "frames/frames.hh"
#include "../../util/cMap/CMapPlus.hh"

#include <iostream>

int main(int argc, char *argv[]) {

	stereoSource mySource;
	stereoSource myEdgeSource;
	stereoProcess myProcess;
	stereoProcess myEdgeProcess;

//	mySource.init(0, 640, 480, "/tmp/logs/2006_07_20/21_24_21/stereoFeeder/stereo_short", "bmp");
	mySource.init(0, 640, 480, "/home/tallberto/dgc/projects/humberto/images/stereo", "bmp");
	myEdgeSource.init(0, 640, 480, "/home/tallberto/dgc/projects/humberto/images/edge", "bmp");
	
	myProcess.init(0, "../../drivers/stereovision/config/stereovision/SVSCal.ini.short",
								 "../../drivers/stereovision/config/stereovision/SVSParams.ini.short",
								 "../../drivers/stereovision/config/stereovision/CamCal.ini.short",
								 "temp",
								 "bmp",
								 0,
								 "../../drivers/stereovision/config/stereovision/SunParams.ini.short",
								 "../../drivers/stereovision/config/stereovision/PointCutoffs.ini.short");
	myEdgeProcess.init(0, "../../drivers/stereovision/config/stereovision/SVSCal.ini.short",
								 "../../drivers/stereovision/config/stereovision/SVSParams.ini.short",
								 "../../drivers/stereovision/config/stereovision/CamCal.ini.short",
								 "temp",
								 "bmp",
								 0,
								 "../../drivers/stereovision/config/stereovision/SunParams.ini.short",
								 "../../drivers/stereovision/config/stereovision/PointCutoffs.ini.short");

	VehicleState myState;
	NEDcoord myCoord;
	NEDcoord myEdgeCoord;
	
	int numValidPixels;
	int returner;
	
	CMapPlus stereoMap;
	stereoMap.initMap(0, 0, 600, 400, 0.1, 0.1, 0);

	int validLayer = stereoMap.addLayer<double>(0.0, 0.0);  		//existing points are 1, others 0
	int realLayer = stereoMap.addLayer<double>(0.0, -32.0);		//height
	int smoothLayer = stereoMap.addLayer<double>(0.0, -32.0);	
	int edgeLayer = stereoMap.addLayer<double>(0.0, -32.0);		//detected edges
	
	while(mySource.pairIndex()<2 && myEdgeSource.pairIndex()<2) {
		if(mySource.grab() && myEdgeSource.grab()) {
			numValidPixels=0;
			myProcess.loadPair(mySource.pair(), myState);
			myEdgeProcess.loadPair(myEdgeSource.pair(), myState);
			myProcess.calcRect();
			myEdgeProcess.calcRect();
			myProcess.calcDisp();
			myEdgeProcess.calcDisp();

			//from the left camera
			for(int x=0; x<640; x++) {
			   for(int y=0; y<480; y++) {
					if(myProcess.SinglePoint(x,y,&myCoord) && myEdgeProcess.SinglePoint(x,y,&myEdgeCoord)) {
						numValidPixels++;
						returner = stereoMap.setDataUTM<int>(validLayer, myCoord.N, myCoord.E, 1);
						returner = stereoMap.setDataUTM<int>(edgeLayer, myEdgeCoord.N, myEdgeCoord.E, 1);
						cout << "FrameNum: " << mySource.pairIndex() - 1 << " " << x << ", " << y << " Point: " << myCoord.N << ", " << myCoord.E << ", " << myCoord.D << endl;
						cout << "FrameEdgeNum: " << myEdgeSource.pairIndex() - 1 << " " << x << ", " << y << " Point: " << myEdgeCoord.N << ", " << myEdgeCoord.E << ", " << myEdgeCoord.D << endl;
					}
				}
			}
			cout << "FrameNum: " << mySource.pairIndex() - 1 << " " << numValidPixels << " valid pixels"  << endl;
		} else {
			if(mySource.pairIndex()%100 == 0) cout << "Frame: " << mySource.pairIndex() << endl;
		}
 	}
  
  return 0;
}

