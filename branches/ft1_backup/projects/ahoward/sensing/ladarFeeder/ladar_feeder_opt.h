/* ladar_feeder_opt.h */

/* File autogenerated by gengetopt version 2.18  */

#ifndef LADAR_FEEDER_OPT_H
#define LADAR_FEEDER_OPT_H

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef CMDLINE_PARSER_PACKAGE
#define CMDLINE_PARSER_PACKAGE "ladarFeeder"
#endif

#ifndef CMDLINE_PARSER_VERSION
#define CMDLINE_PARSER_VERSION "0.0"
#endif

struct gengetopt_args_info
{
  const char *help_help; /* Print help and exit help description.  */
  const char *version_help; /* Print version and exit help description.  */
  char * spread_daemon_arg;	/* Spread daemon.  */
  char * spread_daemon_orig;	/* Spread daemon original value given at command line.  */
  const char *spread_daemon_help; /* Spread daemon help description.  */
  int skynet_key_arg;	/* Skynet key (default='0').  */
  char * skynet_key_orig;	/* Skynet key original value given at command line.  */
  const char *skynet_key_help; /* Skynet key help description.  */
  char * module_id_arg;	/* Module id (default='MODladarFeeder').  */
  char * module_id_orig;	/* Module id original value given at command line.  */
  const char *module_id_help; /* Module id help description.  */
  char * sensor_id_arg;	/* Sensor id of source camera (default='').  */
  char * sensor_id_orig;	/* Sensor id of source camera original value given at command line.  */
  const char *sensor_id_help; /* Sensor id of source camera help description.  */
  int enable_log_flag;	/* Enable data logging (default=off).  */
  const char *enable_log_help; /* Enable data logging help description.  */
  char * ladar_arg;	/* Ladar ID (default='').  */
  char * ladar_orig;	/* Ladar ID original value given at command line.  */
  const char *ladar_help; /* Ladar ID help description.  */
  char * port_arg;	/* Serial port (default='/dev/ttyUSB0').  */
  char * port_orig;	/* Serial port original value given at command line.  */
  const char *port_help; /* Serial port help description.  */
  int disable_sparrow_flag;	/* Disable Sparrow display (default=off).  */
  const char *disable_sparrow_help; /* Disable Sparrow display help description.  */
  int sim_flag;	/* Run as simulator (default=off).  */
  const char *sim_help; /* Run as simulator help description.  */
  char * replay_arg;	/* Replay log file.  */
  char * replay_orig;	/* Replay log file original value given at command line.  */
  const char *replay_help; /* Replay log file help description.  */
  char * sens_pos_arg;	/* Sensor position in vehicle frame (default='0 0 0').  */
  char * sens_pos_orig;	/* Sensor position in vehicle frame original value given at command line.  */
  const char *sens_pos_help; /* Sensor position in vehicle frame help description.  */
  char * sens_rot_arg;	/* Sensor rotation in vehicle frame (Euler angles) (default='3.142 0 0').  */
  char * sens_rot_orig;	/* Sensor rotation in vehicle frame (Euler angles) original value given at command line.  */
  const char *sens_rot_help; /* Sensor rotation in vehicle frame (Euler angles) help description.  */
  
  int help_given ;	/* Whether help was given.  */
  int version_given ;	/* Whether version was given.  */
  int spread_daemon_given ;	/* Whether spread-daemon was given.  */
  int skynet_key_given ;	/* Whether skynet-key was given.  */
  int module_id_given ;	/* Whether module-id was given.  */
  int sensor_id_given ;	/* Whether sensor-id was given.  */
  int enable_log_given ;	/* Whether enable-log was given.  */
  int ladar_given ;	/* Whether ladar was given.  */
  int port_given ;	/* Whether port was given.  */
  int disable_sparrow_given ;	/* Whether disable-sparrow was given.  */
  int sim_given ;	/* Whether sim was given.  */
  int replay_given ;	/* Whether replay was given.  */
  int sens_pos_given ;	/* Whether sens-pos was given.  */
  int sens_rot_given ;	/* Whether sens-rot was given.  */

} ;

extern const char *gengetopt_args_info_purpose;
extern const char *gengetopt_args_info_usage;
extern const char *gengetopt_args_info_help[];

int cmdline_parser (int argc, char * const *argv,
  struct gengetopt_args_info *args_info);
int cmdline_parser2 (int argc, char * const *argv,
  struct gengetopt_args_info *args_info,
  int override, int initialize, int check_required);
int cmdline_parser_file_save(const char *filename,
  struct gengetopt_args_info *args_info);

void cmdline_parser_print_help(void);
void cmdline_parser_print_version(void);

void cmdline_parser_init (struct gengetopt_args_info *args_info);
void cmdline_parser_free (struct gengetopt_args_info *args_info);

int cmdline_parser_configfile (char * const filename,
  struct gengetopt_args_info *args_info,
  int override, int initialize, int check_required);

int cmdline_parser_required (struct gengetopt_args_info *args_info,
  const char *prog_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* LADAR_FEEDER_OPT_H */
