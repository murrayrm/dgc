
#include "fusionMapper.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#include <iostream>


using namespace std;

int QUIT=0;
int PAUSE=0;
int RESET=0;
int CHANGE=0;
int REPAINT=0;
int REEVALUATE=0;
int SAVE=0;
int CLEAR=0;
int PAINTWHILEPAUSED = 0;

int sparrowSNKey;

double sparrowAggrTuningRDDFScaling;
double sparrowAggrTuningMaxSpeed;

char sparrowNameFeeder0[32];
char sparrowNameFeeder1[32];
char sparrowNameFeeder2[32];
char sparrowNameFeeder3[32];
char sparrowNameFeeder4[32];
char sparrowNameFeeder5[32];
char sparrowNameFeeder6[32];

int sparrowStatusFeeder[MAX_INPUT_ELEV_LAYERS];
int sparrowSendCostFeeder[MAX_INPUT_ELEV_LAYERS];
int sparrowNumMsgsFeeder[MAX_INPUT_ELEV_LAYERS];
int sparrowMsgSizeFeeder[MAX_INPUT_ELEV_LAYERS];
double sparrowMsgSizeAvgFeeder[MAX_INPUT_ELEV_LAYERS];
double sparrowTimeLockFeeder[MAX_INPUT_ELEV_LAYERS];
double sparrowTimeStage1Feeder[MAX_INPUT_ELEV_LAYERS];
double sparrowTimeStage2Feeder[MAX_INPUT_ELEV_LAYERS];
double sparrowTimeRemainingFeeder[MAX_INPUT_ELEV_LAYERS];
double sparrowTimeTotalFeeder[MAX_INPUT_ELEV_LAYERS];

int sparrowStatusRoadFinder;
int sparrowNumMsgsRoadFinder;
int sparrowMsgSizeRoadFinder;
double sparrowMsgSizeAvgRoadFinder;
double sparrowTimeLockRoadFinder;
double sparrowTimeStage1RoadFinder;
double sparrowTimeStage2RoadFinder;
double sparrowTimeRemainingRoadFinder;
double sparrowTimeTotalRoadFinder;

int sparrowStatusSupercon;
int sparrowNumMsgsSupercon;
int sparrowMsgSizeSupercon;
double sparrowMsgSizeAvgSupercon;
double sparrowTimeTotalSupercon;
double sparrowTimeLockSupercon;
double sparrowTimeStage1Supercon;
double sparrowTimeStage2Supercon;
double sparrowTimeRemainingSupercon;

int sparrowStatusFusionMapper;
int sparrowSendCostFusionMapper;
int sparrowNumMsgsFusionMapper;
int sparrowMsgSizeFusionMapper;
double sparrowMsgSizeAvgFusionMapper;
double sparrowTimeLockFusionMapper;
double sparrowTimeStage1FusionMapper;
double sparrowTimeStage2FusionMapper;
double sparrowTimeRemainingFusionMapper;
double sparrowTimeTotalFusionMapper;

double sparrowRDDFNoDataSpeed;
int maxCellsToInterpolate;
int minNumSupportingCellsForInterpolation;
int paintRoadWithoutElevation;

double sparrowWingWidth;
double sparrowWingSpeed;

char sdErrorMessage[128];

short numCellsFused;

#include "costtable.h"
#include "vddtable.h"


void FusionMapper::UpdateSparrowVariablesLoop() {
	sprintf(sdErrorMessage, "None");

 	int mapDeltaSocketCost[NUM_ELEV_LAYERS];
	for(int i=0; i < NUM_ELEV_LAYERS; i++)
	  mapDeltaSocketCost[i] = m_skynet.get_send_sock(layerArray[i].msgTypeCost);

	sparrowAggrTuningRDDFScaling = _corridorOptsReference.optRDDFscaling;
	sparrowAggrTuningMaxSpeed = costFuserOpts.maxSpeed = _corridorOptsReference.optMaxSpeed;
	sparrowRDDFNoDataSpeed = _corridorOptsFinalCost.optMaxSpeed;

	maxCellsToInterpolate = costFuserOpts.maxCellsToInterpolate;
	minNumSupportingCellsForInterpolation = costFuserOpts.minNumSupportingCellsForInterpolation;
	paintRoadWithoutElevation = costFuserOpts.paintRoadWithoutElevation;

	sparrowWingWidth = mapperOpts.optWingWidth;
	sparrowWingSpeed = _corridorOptsWings.optMaxSpeed;

	sparrowSNKey = mapperOpts.optSNKey;

	sprintf(sparrowNameFeeder0, "%s", layerArray[0].name);
	sprintf(sparrowNameFeeder1, "%s", layerArray[1].name);
	sprintf(sparrowNameFeeder2, "%s", layerArray[2].name);
	sprintf(sparrowNameFeeder3, "%s", layerArray[3].name);
	sprintf(sparrowNameFeeder4, "%s", layerArray[4].name);
	sprintf(sparrowNameFeeder5, "%s", layerArray[5].name);
	sprintf(sparrowNameFeeder6, "%s", layerArray[6].name);

  int savedMapCount = 0;

	PAINTWHILEPAUSED = _PAINTWHILEPAUSED;

    while(!_QUIT) {
      //printf("Got here\n");
    //Update variables that changed in the display
    _PAINTWHILEPAUSED = PAINTWHILEPAUSED;
    _PAUSE = PAUSE;
    _QUIT = QUIT;

    if(CHANGE) {
      CHANGE = 0;

			for(int i=0; i < NUM_ELEV_LAYERS; i++) {
				layerArray[i].enabled = (bool)sparrowStatusFeeder[i];
				layerArray[i].sendCost = (bool)sparrowSendCostFeeder[i];
			}
      mapperOpts.optRoad   = sparrowStatusRoadFinder;
			mapperOpts.sendRDDF = sparrowSendCostFusionMapper;

      mapperOpts.optSupercon = sparrowStatusSupercon;

      _corridorOptsReference.optRDDFscaling = sparrowAggrTuningRDDFScaling;
			costFuserOpts.maxSpeed = _corridorOptsReference.optMaxSpeed = sparrowAggrTuningMaxSpeed;
      _corridorOptsFinalCost.optMaxSpeed = sparrowRDDFNoDataSpeed;

			costFuserOpts.maxCellsToInterpolate = maxCellsToInterpolate;
			costFuserOpts.minNumSupportingCellsForInterpolation = minNumSupportingCellsForInterpolation;

			costFuserOpts.paintRoadWithoutElevation = paintRoadWithoutElevation;

			mapperOpts.optWingWidth = sparrowWingWidth;
			_corridorOptsWings.optMaxSpeed = sparrowWingSpeed;

			widerRDDF.setExtraWidth(mapperOpts.optWingWidth);

			WriteConfigFile();
    }

    //Update read-only variables
		for(int i=0; i < NUM_ELEV_LAYERS; i++) {
			sparrowNumMsgsFeeder[i] = layerArray[i].statusInfo.numMsgs;
			sparrowMsgSizeFeeder[i] = layerArray[i].statusInfo.msgSize;
			sparrowMsgSizeAvgFeeder[i] = layerArray[i].statusInfo.msgSizeAvg;
			sparrowTimeTotalFeeder[i] = layerArray[i].statusInfo.processTimeAvg;
			//sparrowTimeLockFeeder[i] = layerArray[i].statusInfo.lockTimeAvg;
			sparrowTimeRemainingFeeder[i] = layerArray[i].statusInfo.remainingTimeAvg;
			sparrowTimeStage1Feeder[i] = layerArray[i].statusInfo.stage1TimeAvg;
			sparrowTimeStage2Feeder[i] = layerArray[i].statusInfo.stage2TimeAvg;
			sparrowStatusFeeder[i] = (int)layerArray[i].enabled;
			sparrowSendCostFeeder[i] = (int)layerArray[i].sendCost;
		}

		sparrowSendCostFusionMapper = mapperOpts.sendRDDF;

		numCellsFused = costFuser->getNumCellsFused();

    sparrowStatusRoadFinder = mapperOpts.optRoad;
    sparrowStatusSupercon = mapperOpts.optSupercon;
    sparrowStatusFusionMapper = 1;
  
		sparrowNumMsgsRoadFinder = _infoRoad.numMsgs;
		sparrowMsgSizeRoadFinder = _infoRoad.msgSize;
		sparrowMsgSizeAvgRoadFinder = _infoRoad.msgSizeAvg;
		sparrowTimeTotalRoadFinder = _infoRoad.processTimeAvg;
		sparrowTimeLockRoadFinder = _infoRoad.lockTimeAvg;
		sparrowTimeStage1RoadFinder = _infoRoad.stage1Time;
		sparrowTimeStage2RoadFinder = _infoRoad.stage2TimeAvg;
		sparrowTimeRemainingRoadFinder = _infoRoad.remainingTimeAvg;

		sparrowNumMsgsSupercon = _infoSupercon.numMsgs;
    sparrowMsgSizeSupercon = _infoSupercon.msgSize;
    sparrowMsgSizeAvgSupercon = _infoSupercon.msgSizeAvg;
    sparrowTimeTotalSupercon = _infoSupercon.processTime;
		sparrowTimeLockSupercon = _infoSupercon.lockTimeAvg;
		sparrowTimeStage1Supercon = _infoSupercon.stage1TimeAvg;
		sparrowTimeStage2Supercon = _infoSupercon.stage2TimeAvg;
		sparrowTimeRemainingSupercon = _infoSupercon.remainingTimeAvg;

    sparrowNumMsgsFusionMapper = _infoFusionMapper.numMsgs;
    sparrowMsgSizeFusionMapper = _infoFusionMapper.msgSize;
    sparrowMsgSizeAvgFusionMapper = _infoFusionMapper.msgSizeAvg;
		sparrowTimeStage1FusionMapper = _infoFusionMapper.stage1TimeAvg;
		sparrowTimeStage2FusionMapper = _infoFusionMapper.stage2TimeAvg;
		sparrowTimeRemainingFusionMapper = _infoFusionMapper.remainingTimeAvg;
    sparrowTimeTotalFusionMapper = _infoFusionMapper.processTimeAvg;
    sparrowTimeLockFusionMapper = _infoFusionMapper.lockTimeAvg;
		
    if(RESET) {
			for(int i=0; i < NUM_ELEV_LAYERS; i++) 
				layerArray[i].statusInfo.reset();
      _infoFusionMapper.reset();
      _infoStaticPainter.reset();
      RESET = 0;
    }

    if(REPAINT) {
      REPAINT = 0;
			sprintf(sdErrorMessage, "Resending entire map, please wait...");
			dd_redraw(0);
      int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
      DGCwritelockRWLock(&allMapsRWLock);
      CDeltaList* deltaList = fusionMap.serializeFullMapDelta<double>(layerID_costFinal);
      SendMapdelta(socket_num, deltaList);
      fusionMap.resetDelta<double>(layerID_costFinal);
			for(int i=0; i < NUM_ELEV_LAYERS; i++) {
				if(layerArray[i].sendCost) {
					deltaList = fusionMap.serializeDelta<double>(layerArray[i].costLayerNum, 0);
					if(!deltaList->isShiftOnly()) {
						SendMapdelta(mapDeltaSocketCost[i], deltaList);
						fusionMap.resetDelta<double>(layerArray[i].costLayerNum);
					}
				}
			}
      DGCunlockRWLock(&allMapsRWLock);
			sprintf(sdErrorMessage, "Done resending entire map");
			dd_redraw(0);
    }

    if(REEVALUATE) {
      REEVALUATE = 0;
			sprintf(sdErrorMessage, "Reevaluating entire map, please wait...");
			dd_redraw(0);
      DGCwritelockRWLock(&allMapsRWLock);
			fusionMap.clearLayer(layerID_corridor);
			fusionMap.clearLayer(layerID_costFinal);
			fusionCorridorPainter->repaintAll();
			for(int i=0; i < NUM_ELEV_LAYERS; i++) {
				layerArray[i].map->clearLayer(layerArray[i].costLayerNum);
				layerArray[i].terrainPainter->readOptionsFromFile(layerArray[i].terrainPainterIndex, layerArray[i].optionsFilename);
				layerArray[i].terrainPainter->repaintAll(layerArray[i].terrainPainterIndex,
																					layerArray[i].costPainterIndex);
			}
			costFuser->markChangesCostAll();
 			costFuser->fuseChangesCost(NEcoord(m_state.Northing, m_state.Easting));
      DGCunlockRWLock(&allMapsRWLock);
			sprintf(sdErrorMessage, "Done reevaluating entire map.");
			dd_redraw(0);
    }

		if(CLEAR) {
      //DGClockMutex(&m_mapMutex);
			sprintf(sdErrorMessage, "Clearing entire map, please wait...");
			dd_redraw(0);
      DGCwritelockRWLock(&allMapsRWLock);
			costFuser->clearBuffers();
			fusionMap.clearMap();
			roadMap.clearMap();
			for(int i=0; i < NUM_ELEV_LAYERS; i++) {
				layerArray[i].map->clearMap();
			}
      //DGCunlockMutex(&m_mapMutex);
			CLEAR = 0;
      DGCunlockRWLock(&allMapsRWLock);
			sprintf(sdErrorMessage, "Done clearing entire map.");
			dd_redraw(0);
		}

    if(SAVE) {
      SAVE = 0;
      //DGClockMutex(&m_mapMutex);
			sprintf(sdErrorMessage, "Saving maps, please wait...");
			dd_redraw(0);
      char mapName[128];
      for(int i=0; i<fusionMap.getNumLayers(); i++) {
				sprintf(mapName, "fusionMapperMap-%03d-Layer-%02d-(%s)", savedMapCount, i,
								fusionMap.getLayerLabel(i));
				fusionMap.saveLayer<double>(i, mapName, false);
				fusionMap.saveLayer<double>(i, mapName, true);
      }
      for(int j=0; j< NUM_ELEV_LAYERS; j++){
	
	for(int i=0; i < (layerArray[j].map)->getNumLayers(); i++) {
	  sprintf(mapName, "fusionMapperElevMap_%03d-Map_%02d-Layer_%02d-(%s)", savedMapCount, j, i,
		  fusionMap.getLayerLabel(i));
	  (layerArray[j].map)->saveLayer<double>(i, mapName, false);
	  (layerArray[j].map)->saveLayer<double>(i, mapName, true);
	}}
      for(int i=0; i<roadMap.getNumLayers(); i++) {
	sprintf(mapName, "fusionMapperRoadMap-%03d-Layer-%02d-(%s)", savedMapCount, i,
		roadMap.getLayerLabel(i));
	roadMap.saveLayer<double>(i, mapName, false);
	roadMap.saveLayer<double>(i, mapName, true);
      }
	
      savedMapCount++;
      //DGCunlockMutex(&m_mapMutex);
			sprintf(sdErrorMessage, "Done saving maps");
			dd_redraw(0);
    }

    usleep(10000);
  }

}

void FusionMapper::SparrowDisplayLoop() 
{

  dbg_all = 0;

  if (dd_open() < 0) exit(1);

  dd_bindkey('Q', user_quit);
  dd_bindkey('q', user_quit);
  dd_bindkey('P', user_pause);
  dd_bindkey('p', user_pause);
  dd_bindkey('R', user_reset);
  dd_bindkey('r', user_reset);
  dd_bindkey('S', user_save);
  dd_bindkey('s', user_save);
  dd_bindkey('A', user_repaint);
  dd_bindkey('a', user_repaint);
  dd_bindkey('E', user_reevaluate);
  dd_bindkey('e', user_reevaluate);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();	
  dd_close();
  _QUIT = 1;
  //sleep(1);
}

int user_quit(long arg)
{
  QUIT = 1;
  return DD_EXIT_LOOP;
}


int user_reset(long arg)
{
  RESET = 1;
  return 0;
}

int user_pause(long arg)
{
  if(PAUSE) {
    PAUSE=0;
  } else {
    PAUSE=1;
  }
  return 0;
}

int user_change(long arg) {
  CHANGE=1;
  return 0;
}

int user_repaint(long arg) {
  REPAINT=1;
  return 0;
}


int user_save(long arg) {
  SAVE = 1;
  return 0;
}


int user_reevaluate(long arg) {
  REEVALUATE=1;
  return 0;
}


int user_clear(long arg) {
	CLEAR=1;
	return 0;
}
