#include <iostream>
#include <getopt.h>
#include <vector>
#include "LocalMapTalker.hh"
#include "rndf.hh"

enum {
  OPT_NONE,
  OPT_HELP,
  OPT_RNDF,
  NUM_OPTS
};

using namespace std;
using namespace Mapper;

// Default options
char* RNDFFileName = "../../../nok/missionPlanner/DARPA_RNDF.txt"; // name of RNDF file


/* The name of this program. */
const char * program_name;

/* Prints usage information for this program to STREAM (typically stdout 
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream, "  --rndf RNDFFileName \tSpecifies the filename RNDFFileName for RNDF.\n"); 
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  int ch;
  int option_index = 0;

  /* An array describing valid long options. */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"help",       no_argument,       0,           OPT_HELP},
    {"rndf",       required_argument, 0,           OPT_RNDF},
    {0,0,0,0}
  };

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long_only(argc, argv, "", long_options, &option_index)) != -1)
  {
    switch(ch)
    {
      case OPT_HELP:
        /* User has requested usage information. Print it to standard
           output, and exit with exit code zero (normal 
           termination). */
        print_usage(stdout, 0);
	break;

      case '?': /* The user specified an invalid option. */
        /* Print usage information to standard error, and exit with exit
           code one (indicating abnormal termination). */
        print_usage(stderr, 1);
	break;

      case OPT_RNDF:
	RNDFFileName = optarg;
	break;

      case -1: /* Done with options. */
        break;
    }
  }

  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
  {
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  }
  else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  
  RNDF* m_rndf = new RNDF();
  cout << "created empty rndf" << endl;
  m_rndf->loadFile(RNDFFileName);

  // Create the talker and the map
  CLocalMapTalker mapTalker = CLocalMapTalker(MODmapping, sn_key, true);
  cout << "creating map" << endl;
  Map* locMap_p = new Map(m_rndf);
  cout << "finish creating map" << endl;
  locMap_p->print();
  
  
	
	// Receive a stopline
	// 1) Create a stopline receiver
	// The receiver will check if any stopline messages are waiting,
	// and if so receive them.  It will take the topmost one, avg. 
	// all of the contained stoplines together and output a single 
	// stopline (with center average coordinates).
	
	// Do the main thing here
	while (true) {
		cout << "about to send a map...";
		mapTalker.SendLocalMap(locMap_p);
		cout << " sent a map!" << endl;
		sleep(1);
	}
	
	return 0;
}

//
//
//int main(int argc, char *argv[])
//{
//	char* RNDFFileName = "../../../nok/missionPlanner/DARPA_RNDF.txt"; // name of RNDF file
//	// Get the skynet key
//	int key = (argc >1) ? atoi(argv[1]) : 0;
//
//	// Create the talker and the map
//	CLocalMapTalker mapTalker = CLocalMapTalker(MODmapping, key, true);
//	Map* locMap_p = new Map();
//	
//	// Receive a stopline
//	// 1) Create a stopline receiver
//	// The receiver will check if any stopline messages are waiting,
//	// and if so receive them.  It will take the topmost one, avg. 
//	// all of the contained stoplines together and output a single 
//	// stopline (with center average coordinates).
//	
//	// Do the main thing here
//	cout << "about to send a map...";
//	mapTalker.SendLocalMap(locMap_p);
//	cout << " sent a map!" << endl;
//}
