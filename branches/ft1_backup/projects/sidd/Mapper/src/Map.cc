#include "Map.hh"
#include "LogicalRoadObject.hh"
#include "RoadObject.hh"
#include <iostream>

using namespace std;

namespace Mapper
{
/* Basic constructor */
Map::Map(vector<Segment> initialSegs)
{
	segments = initialSegs;
}
/* Default, static map constructor */
Map::Map()
{
/*
	// TODO: Finish this after Noel sends his map.
	// Create the correct number of blank lane objects.
	// Use the spoof method to populate the lanes.
	// Construct the segment.
	// Populate the map.
	
	// Create the waypoints
	Location loc0 = {396422, 3778147}; // first seg 
	Location loc1 = {396432, 3778147};
	Location loc2 = {396442, 3778147};
	Location loc3 = {396452, 3778147}; // second seg
	Location loc4 = {396462, 3778147};
	Location loc5 = {396472, 3778147};
	
	// Build the centerlines 
	vector<Location> centerline0;
	centerline0.push_back(loc0);
	std::cout << centerline0[0].x << "," << centerline0[1].y;
	centerline0.push_back(loc1);
	centerline0.push_back(loc2);
	vector<Location> centerline1;
	centerline1.push_back(loc3);
	centerline1.push_back(loc4);
	centerline1.push_back(loc5);
	
	// Build the lanes, only one per segment
	Lane lane1_1 = Lane(1, centerline0);
	Lane lane2_1 = Lane(1, centerline1);
	vector<Lane> seg1_lanes;
	vector<Lane> seg2_lanes;
	seg1_lanes.push_back(lane1_1);
	seg1_lanes.push_back(lane1_1);
	seg2_lanes.push_back(lane2_1);
	seg2_lanes.push_back(lane2_1);
	
	// Finally, build the segments and add them to the map
	Segment seg1 = Segment(1, seg1_lanes);
	Segment seg2 = Segment(2, seg2_lanes);
	segments.push_back(seg1);
	segments.push_back(seg1); // duplication is not a mistake, has to do with 0 vs 1-indexing
	segments.push_back(seg2);
*/
}
/* RNDF Constructor */
// Constructs a map out of an RNDF object.
// RNDF contains segments, segments contain lanes, lanes contain waypoints 
// Map contains segments, segments contain lanes.
// I don't know how well this will compile, so I've included alternate
// code, commented out.
Map::Map(std::RNDF* RNDFObj)
{
  //std::doodles * dood = new std::doodles();
  //doodles * dood = new doodles();
  //int knood = dood->getKanoodles()



  //...
  int numwpnts;
  std::Waypoint * wpnt;
  std::Segment * rseg;
  std::Lane * rlane;
  
  for (int ii=1;ii<=RNDFObj->getNumOfSegments();ii++)
  {
  	cout << "segment " << ii << endl;
    //Make a new segment
    vector<Lane> seg_lanes;

    //Get the segment
    rseg = RNDFObj->getSegment(ii);

    for (int jj=1;jj <= rseg->getNumOfLanes();jj++)
    {
    	cout << "lane " << jj << endl;
      rlane = rseg->getLane(jj);
      numwpnts = rlane->getNumOfWaypoints();

      vector<Location> centerline;

      for (int nn=1;nn<=numwpnts;nn++)
      {
      	cout << "waypoint " << nn << endl;
		wpnt = rlane->getWaypoint(nn);
		Location loc = { wpnt->getNorthing(),wpnt->getEasting() };

		centerline.push_back(loc);
      }
      //Make a new lane object
      Lane lane = Lane(jj,centerline);

      //put all of the lanes in a segment
      seg_lanes.push_back(lane);

      if(jj==1)
      {//compensating for 1-indexing
		Lane lane = Lane(jj,centerline);
		seg_lanes.push_back(lane);
      }
    }

    //push that segment onto the vector of segments
    Segment seg = Segment(ii,seg_lanes);

    //put that segment on the vector of segments
    segments.push_back(seg);
    
    if (ii==1)
    {//compensating for 1-indexing
      Segment seg = Segment(ii,seg_lanes);
      segments.push_back(seg);
    }
  }
  print();
  cout << "finish constructing map" << endl;
}

/* Pretty print the segment object. */
void Map::print() const
{
	unsigned int i;
	
	std::cout << "Object Map: \n";
	for (i=0; i<segments.size(); i++) 
	{
		segments[i].print();
	}
}

/* Retrieve a segment given its ID. */
Segment Map::getSegment(int segmentID) const 
{
	Segment reqSegment = segments[segmentID]; //The segment requested.
	int reqSegID = reqSegment.getID();
	
	/* Check that the segment ID matches the requested
	 * segment ID. */
	if (reqSegID != segmentID)
		throw "The requested segment ID does not match the actual ID of the segment returned.\n";
		
	return reqSegment;
}
vector<Segment> Map::getAllSegs() const
{
	return segments;
}

/* Destructor */
Map::~Map()
{
}

}
