#include "LocalMapTalker.hh"

using namespace std;
using namespace Mapper;

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;

	CLocalMapTalker mapTalker = CLocalMapTalker(MODtrafficplanner, key, false);
	
	//vector<Segment> segments;
	//Map map(segments);
	Map *map_p;

	int size;
	cout << "about to receive a map...";
	mapTalker.RecvLocalMap(map_p, &size);
	cout << " received a map!" << endl;
	
}
