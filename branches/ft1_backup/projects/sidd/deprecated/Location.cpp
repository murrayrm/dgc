#include "Location.h"

namespace Mapper
{

Location::Location(double xloc, double yloc)
{
	x = xloc;
	y = yloc;
}

// Get x location
double Location::getXLoc() const
{
	return x;
}
// Get y location
double Location::getYLoc() const
{
	return y;
}

Location::~Location()
{
}

}
