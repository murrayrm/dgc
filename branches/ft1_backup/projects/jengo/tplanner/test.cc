/*! \file 
 *
 * Ignore this file. It's just used to test different
 * functionalities of C++ during development. 
 */

// just fiddling with arrays

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
//#include "../../morlanliu/RNDF.hh"

/*#include <fstream>
  #include "coords.hh"*/
using namespace std;

vector<string> strings;

void createVector()
{

  string one = "one. ";
  strings.push_back(one);
  string two = "two. ";
  strings.push_back(two);

  cout<<one<<&one<<endl;
  cout<<two<<&two<<endl;

}

void loadFile(char* fileName)
{
  //int i;
  ifstream file;
  string line;
  string word;
  
  file.open(fileName, ios::in);

  if(!file)
    {
      cout << "Error: Unable to open the RNDF file: " << fileName << endl;
      exit(0);
    } 
  
  getline(file, line);
  
  istringstream lineStream(line, ios::in);
  
  lineStream >> word;
  
  if(word == "RNDF_name")
    {
      cout<<"RNDF"<<endl;
      
      file.close();
      // parseExit(&file);
    }
  else if(word == "MDF_name")
    {
      cout<<"MDF";
    }
  else
    exit(0);
}

/*
double* evaluate(int number) {
  double* thing = new double[1];
  thing[0] = 12.3;
  return thing;
}
*/

double * evaluate() {

  double *states = new double[2];
  
  cout<<states<<endl;
  
  return states;

}

int main() {
  /*  
  // double *what = evaluate(2);

  if (0) cout << "0 is true" << endl;
  if (1) cout << "1 is true" <<endl;
  if (-1) cout << "-1 is true" << endl;
  

  //cout << what[0] << endl;

  NEcoord n(4,2);
  n.display();
  
  ofstream output("tplanner_log");

  output << "test." << endl;
  
  output<< n.display();

  output.close();
  

  for (int i=0; i<10; i++) {
    if (i==5)
      i+=3;
    cout<<i<<endl;
  }


  double a = 7;
  double *b;
  b = &a;
  cout<<a<<endl;
  cout<<b<<endl;

  
  loadFile("Sample_RNDF.txt");

  double *states = evaluate();
  
  cout<<states<<endl;

  delete states;

  int b = 15;

  int* a;
  a = &b;
  cout<<*a<<endl;



  createVector();

  cout<<strings.at(0)<<&strings.at(0);

  cout<<endl;

  vector<string>* manyStrings;

  manyStrings = new vector<string>[2];

  manyStrings[0].push_back("hello ");
  manyStrings[0].push_back("world");
  manyStrings[1].push_back("!");

  for (int i=0; i<2; i++) {
    for (vector<string>::iterator it = manyStrings[i].begin();
	 it != manyStrings[i].end(); it++) {
      cout<<*it;
    }
  }
	
  vector<string>::iterator it = manyStrings[0].begin();
  it++;
  cout<<*(it-1);
  cout<<*it;
     


  fstream temp("temp.txt");
  
  double test1;

  temp.getline(test1,' ');
  cout<<test1;

  temp.close();

  */

  char* pointer;

  if (pointer)
    delete[] pointer;

  else
    cout<<"not deleted."<<endl;

  return 0;
}
