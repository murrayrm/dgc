/*
 * sick_driver.h - Header file for SICK LMS 2xx LADAR driver
 * Various authors, 2004
 *
 * This is the header file for the SICK ladar drivers.  Documentation
 * added in Jun 2006 by Richard Murray.
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <termios.h>
#include <unistd.h>
#include <netinet/in.h>  /* for struct sockaddr_in, htons(3) */
#include <sys/ioctl.h>
#include <math.h>
#include <sstream>
#include <fstream>
#include <linux/serial.h>

#include <DGCutils>

#include "ladar_buffer.hh"

#define HAVE_HI_SPEED_SERIAL

#define DEFAULT_LASER_PORT "/dev/ttyS1"
#define DEFAULT_LASER_PORT_RATE 38400
#define DEFAULT_LASER_RETRIES 3
#define MAX_LADAR_SCAN 401
#define MAX_CONFIG_FILE_LINE 1024
#define SIZE_TIMESTAMP_BUFFER 100

using namespace std;

/** Raw LADAR data structure */
struct laser_data_t {
  uint16_t ranges[MAX_LADAR_SCAN];	/**< LADAR range data  */
  uint16_t intensity[MAX_LADAR_SCAN];	/**< LADAR intensity data  */
  unsigned long long timestamps[MAX_LADAR_SCAN];
					/**< Timestampes for each scan  */
  int min_angle;			/**< LADAR start angle  */
  int max_angle;			/**< LADAR stop angle  */
  int resolution;			/**< angle between data points (units???) */
  int range_count;			/**< number of segments  */
  float range_res;			/**< resolution (units???)  */
  unsigned long long ladar_timestamp;	/**< timestamp for this measurement  */
};

// The laser device class.
/**
 * SICK LADAR device interface
 *
 * SickLMS200 is the device driver for the LMS200 series of LADARS
 * (including the LMS 221 and LMS 291 that we use on Alice.  It is
 * usually accessed through the ladarSource class, which provides a
 * higher level interface.  The driver is based on a driver written by
 * Kasper Stoy at USC (but has been pretty heavily modified).
 *
 * This driver works by reading a configuration file that specifies
 * the parameters for the LADAR and then starts a thread that
 * continuously reads the data from the LADAR.  The driver can also
 * log the data to a file and read logged data back from the file.
 */
class SickLMS200 {
public:

    SickLMS200();	/**< Constructor (initailize mutexes)  */
    ~SickLMS200();	/**< Deconstructor (free mutexes)  */

   /**
    * @brief   initialize the LADAR device
    * @param   filename    configuration file with LADAR info
    * @param   setconfigfile  save config file name (optional)
    * 
    * This function initializes the ladar based on parameters that
    * are specified in a configuration file.  I can't quite figure
    * out what the setconfigfile parameter does; seems to be used
    * internally (???).
    *
    * The setup function starts the a thread (MainThread) that
    * actually reads from the LADAR unit.
    *
    * Configuration file format: \n
    * serialnum: serial number for the LADAR unit \n
    * rate: communcations rate (9600 or 38400) \n
    * delay: delay between opening terminal and connecting, in seconds \n
    * resolution: desired scan resolution, in degrees \n
    * units: units to use for range data (???) \n
    * scan_angle: desired scan angle, in degrees \n
    * authority: 0 (neither), 1 (master), 2 (slave) \n
    *
    * Note that the ladarSource::init function also reads the same
    * configuration file, but has additional elements that it parses.
    */
    int Setup(char *filename, bool setconfigfile = true);

    /**
     *  Reset the ladar.
     *  Returns true on success
     */
    bool Reset();

    /**
     *  Get errors from the LADAR and dump to file.
     * 
     *  This function sends a 0x32 control code to the LADAR and dumps
     *  the resulting output to the file "ladarerrors".  It returns
     *  true if the errors were properly retrieved.
     */
    bool DumpErrors(void);

    /** LADAR data (buffered) */
    CLadarDataBuffer m_laserData;

    /** Return the number of data points in a scan */
    int getNumDataPoints();

    /** 
     * Turn raw data logging on 
     * 
     * @param filename file to store logged data
     *
     * Turn on data logging.  If the file already exists, data is
     * appeneded to the file, otherwise the file is created.  Data is
     * logged to the file in the following format:
     *
     *  timestamp (\%llu) \n
     *  range_count (\%d) \n
     *  range[] (\%d) \n
     *
     */
    int startLogging(char* filename);

    /** Turn loggifng off */
    int stopLogging();

    /** Check to see if logging is turned on */
    bool isLogging();

    /** 
     * Set up LADAR driver from file.
     *
     * Configures the LADAR driver using a file and sets the log file
     * (for input or output???).
     */
    int SetupFromFile(char *configFilename, char* logFilename);

    /**
     * Read in a LADAR scan from log file
     *
     * This function will read data from a logged data file.
     * Returns the timestamp for the data that was returned.
     */
    unsigned long long Scan(unsigned long long timestampThresh);

    /**
     * Get data from LADAR, when available
     *
     * @param ranges array for storing range data
     * @param angles array for storing angle of each scan point
     * @param latestScanTimestamp timestamp for start of scan
     * @param timestamps timestamp for each scan
     * @param errorMessage error message (if any)
     *
     * This function waits until there is new data from the ladar and
     * returns the data in the function arguments.  Returns the number
     * of good points.
     */
    int GetScanFrame(double* ranges, double* angles,
		     unsigned long long* latestScanTimestamp,
		     unsigned long long* timestamps,
		     char* errorMessage);


    /**
     * Thread to collect data from the LADAR.
     *
     * This function gets called as a thread by Setup() and is
     * responsible for reading data from the LADAR.  It appears to
     * simply run an infinite loop that reads the scans and stores
     * them in m_laserData.
     *
     */
    void MainThread(void);

    /// Request data from the laser. Returns true on success
    bool RequestLaserData(void);

    /// Request data from the laser to stop. Returns true on success
    bool RequestLaserDataStop(void);

private:
    // for detecting a new scan condition
    bool            m_bHaveNewScan;
    pthread_cond_t  m_newScanCondition;
    pthread_mutex_t m_newScanCondMutex;

    pthread_mutex_t m_laserDataMutex;
    DGCcondition    m_requestDataCondition;

    FILE* _logFileRaw;
		FILE* _errorFile;
    char _logFilename[2048];
    char _errorFilename[2048];
    unsigned long long lastTime;

    //the timestamp buffer, the index into it, whether the buffer is full,
    // and the sum of all the timestamps
    unsigned long long m_timestamps[SIZE_TIMESTAMP_BUFFER];
    int                m_timestampBufferIndex;
    bool               m_bTimestampBufferFull;
    unsigned long long m_timestampsSum;

    /// Compute the start and end scan segments based on the current
    /// resolution and scan angles.  Returns 0 if the configuration is
    /// valid.
    bool CheckScanConfig();
    
    /// Open the terminal.
    /// Returns 0 on success
    int OpenTerm();

    /// Sets the serial device to either block or not block. These
    /// return true on success.
    bool SetBlockingMode();
    bool SetNonBlockingMode();

    /// Close the terminal.
    /// Returns 0 on success
    int CloseTerm();
    
    /// Set the terminal speed.
    /// Valid values are 9600 and 38400
    /// Returns 0 on success
    int ChangeTermSpeed(int speed);

    int CheckStatus(uint8_t status);

    /// Get the laser type.
    bool GetLaserType(char *buffer, size_t bufflen);

    /// Set the laser data rate.
    /// Valid values are 9600 and 38400
    /// Returns true on success
    bool SetLaserSpeed(int speed);

    /// Put the laser into configuration mode.
    bool EnterInstallationMode();

    // Set the laser configuration
    // Returns true on success
    bool SetLaserConfig(bool intensity);

    /// Change the resolution of the laser.
    bool SetLaserRes(int angle, int res);
    
    /// Read range data from laser.
    bool ReadScanFromLaser(uint16_t *data, size_t datalen);

    /// Write newest data to the data struct.
    void PutData(laser_data_t *pFrom);

private:
    void getTTYport(char* pTTY);

    /// Write a packet to the laser.
    ssize_t WriteToLaser(uint8_t *data, ssize_t len); 
    
    /// Read a packet from the laser.
    ssize_t ReadFromLaser(uint8_t *data, ssize_t maxlen,
			  bool ack = false, int timeout = -1,
			  bool checkStatus = true);

    /// Calculates CRC for a telegram.
    unsigned short CreateCRC(uint8_t *data, ssize_t len);

    int ParseConfigFile(char *filename);

    int CheckErrors();

    int DumpConfig();

  //  protected:
public:

    /// Laser pose in robot cs.
    double pose[3];
    double size[2];
    
    /// Name of device used to communicate with the laser
    char device_name[MAX_CONFIG_FILE_LINE];
    
    /// laser device file descriptor
    int laser_fd;           

    /// Starup delay
    int startup_delay;
  
    /// Scan width and resolution.
    int scan_width, scan_res;

    /// Start and end scan angles (for restricted scan).  These are in
    /// units of 0.01 degrees.
    int min_angle, max_angle;
    
    /// Start and end scan segments (for restricted scan).  These are
    /// the values used by the laser.
    int scan_min_segment, scan_max_segment;
		int scan_num_segments;

    /// Range resolution (in mm)
    int range_res;

    // for compatibility with ladarSource. Somebody fix this. Please.
    double _resolution, _ladarUnits, _scanAngleOffset;
    int _scanAngle;

    /// Turn intensity data on/off.
    bool intensity;

    bool can_do_hi_speed;
    int port_rate;
    int current_rate;  

    enum LADAR_AUTHORITY_TYPE {
      NEITHER = 0,
      MASTER = 1,
      SLAVE = 2
    };

    char _configFilename[256];

    LADAR_AUTHORITY_TYPE _ladarAuthority;

    string _errorMessage;

#ifdef HAVE_HI_SPEED_SERIAL
  struct serial_struct old_serial;
#endif
};

////////////////////////////////////////////////////////////////////////////////
// Device codes

#define STX     '\x02'
#define ACK     0xA0
#define NACK    0x92
#define CRC16_GEN_POL 0x8005


////////////////////////////////////////////////////////////////////////////////
// Error macros
#define RETURN_ERROR(erc, m) {return erc;}
 
////////////////////////////////////////////////////////////////////////////////
