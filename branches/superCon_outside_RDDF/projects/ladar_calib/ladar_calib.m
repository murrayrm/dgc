function [x, resnorm] = ladar_calib( varargin )
%
% function ladar_calib.m
% 
% Purpose: 
%   Assuming flat ground, with known sensor mounting height, fit the pitch 
%   and roll that match a given range image.
% 
% Inputs:
%   plotQ (optional) = number indicating whether or not to generate and update
%                      a plot that shows the range image
%
% Outputs:
%   x = array of pitch and roll values, of size Nx2 (N = # of scans)  
% 
% Changes:
%   2005-03-20 - LBC, created.
%

if( nargin > 0 )
  plotQ = varargin{1};
else
  plotQ = 0;
end

% example LADAR log file
filename = 'sample.log';
disp(['Loading data from ', filename])
scansdata = load(filename);
len = size(scansdata,1)
        
% scan angles (110 degree mode)
thdata = [-50:1:50] * pi/180;

% conversion stuff for scan log order
roofOffset = 50 * pi/180; % *scan yaw* offset in radians
roofResolution = 0.5; % degrees

% now reconstruct the pitch and roll from the range image
% pick a starting guess of a flat vehicle
x0 = [-6*pi/180 0];

% construct the pitch and roll estimates from the scans
for i=1:len-1

  disp(['scan number ',num2str(i),'...'])  

  thdata = [];
  rdata = [];

  for j=2:202

    % append the valid scans to the list
    % TODO: Check against LADAR Characterization Wiki page for other error value
    if scansdata(i,j) < 8191

      thdata = [thdata roofOffset - ((j-2) * roofResolution * pi/180)];
      rdata = [rdata 0.01*scansdata(i,j)];

    end

  end


% The commented out line is the Matlab version of the non-linear curve
%	fit. The following lines are the Octave version
%  [x(i,:),resnorm(i)] = lsqcurvefit(@range_fn,x0,thdata,rdata);
	[rsol, psol] = leasqr(thdata', rdata', x0', @range_fn);
	x(i,:) = psol';
  resnorm(i) = norm(rdata' - rsol);

  if( plotQ )
    figure(1), plot(thdata, rdata,'.'), axis([-1 1 0 80]), grid on;
    title(['Estimated pitch = ',num2str(x(i,1)),', roll = ',num2str(x(i,2))])
    drawnow
  end

end

% These were taken out for Octave compatibility
disp('Statistics for pitch and roll estimates...')
disp(['pitch (min, mean, max) = ', num2str(min(x(:,1))), ', ', ...
         num2str(mean(x(:,1))), ', ', num2str(max(x(:,1))) ])

disp(['roll (min, mean, max) = ', num2str(min(x(:,2))), ', ', ...
         num2str(mean(x(:,2))), ', ', num2str(max(x(:,2))) ])


function F = range_fn( thdata, psrs  )
% The ideal range returned on perfectly flat ground from a single-axis LADAR 
% unit as a function of the scan angle (th), pitch of the sensor (ps), roll of 
% the sensor (rs) and sensor height (zs) is given below
%
% Inputs:
%   psrs = vector of pitch and roll of the sensor ([ps rs])
%   th = scan angles of the sensor
%

% z offset of sensor, meters (with respect to the vehicle frame)
% this should be the negative of the height of the sensor above flat ground
zs = -2.413;

ps = psrs(1);
rs = psrs(2);

F = zs ./ (cos(thdata).*sin(ps) - cos(ps).*sin(rs).*sin(thdata));
