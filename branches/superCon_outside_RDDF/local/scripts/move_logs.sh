#!/bin/bash

# Script to move log files to a specific server
# NOTE: In order for this script to work, you need to have exchanged ssh public
#       keys between the race computers that has the logs and the server that
#       shall recieve the logs. Otherwise than that, this should work just fine
#       as long as the race computers and the modules paths remains the same.

USER=henrik             # this one you wanna change for sure!

# check the planning startup checklist for these
ARBITER_COMP=titanium  
LADAR_COMP=osmium
STEREO_COMP=iridium

# check the planning startup checklist for these
ARBITER_LOGDIR=dgc/bob/RaceModules/Arbiter2/logs
LADAR_LOGDIR=dgc/bob/RaceModules/LADARMapper/testRuns
STEREO_LOGDIR=dgc/bob/DevModules/StereoMapper

LOGPREFIX=$1
TARGET=$2
ARBITER="cd $ARBITER_LOGDIR; scp $LOGPREFIX* $TARGET/$LOGPREFIX/arbiter;"
LADAR="cd $LADAR_LOGDIR; scp $LOGPREFIX* $TARGET/$LOGPREFIX/ladar;"
STEREO="cd $STEREO_LOGDIR; scp $LOGPREFIX* $TARGET/$LOGPREFIX/stereo;"

if [[ ! "$1" || ! "$2" ]]; then
  echo Usage: $0 logprefix target
  echo 
  echo " logprefix   must be a common prefix of all log files (logprefix* will be copied)"
  echo " target      must be of the format username@host:path used for scp"
  echo
  echo "NOTE: This script is VERY sensitive to changes in where the"
  echo "      different modules are executed. Please edit if changed!"
  echo "NOTE: Edit this script if the desired username is not $USER"
  echo
  echo "What this script does:"
  echo " 1. Creates a logprefix dir at the target location"
  echo " 2. Copies logprefix* files from different race computers to that dir"
  exit 0;
fi

echo "In order for this script to work, the race computes must have ssh keys"
echo "exchanged with the target computer"

# A little smart check to see if the target is available: create dirs named after logprefix and try to copy them to the target computer.

mkdir $LOGPREFIX
mkdir $LOGPREFIX/arbiter
mkdir $LOGPREFIX/stereo
mkdir $LOGPREFIX/ladar

if(scp -r $LOGPREFIX $TARGET); then
  
  xterm -sl 9999 -geometry 100x30-500-300 -e "echo $ARBITER_COMP: Arbiter2 logs; \
  ssh $USER@$ARBITER_COMP '$ARBITER'; \
  echo; echo; echo -n $ARBITER_COMP: copying finished. Press enter to exit; \
  read foo" &

  xterm -sl 9999 -geometry 100x30-400-300 -e "echo $LADAR_COMP: LADAR logs; \
  ssh $USER@$LADAR_COMP '$LADAR'; \
  echo; echo; echo -n $LADAR_COMP: copying finished. Press enter to exit; \
  read foo" &
  
  xterm -sl 9999 -geometry 100x30-300-300 -e "echo $STEREO_COMP: Stereo logs; \
  ssh $USER@$STEREO_COMP '$STEREO'; \
  echo; echo; echo -n $STEREO_COMP: copying finished. Press enter to exit; \
  read foo" &

  # clean up the dirs we created
  rm -r $LOGPREFIX 
else
  echo "Couldn't create dir at $TARGET, aborting moving logs!"
fi

exit 0;
