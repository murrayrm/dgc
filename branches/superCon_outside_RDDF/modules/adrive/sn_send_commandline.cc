/*! This is the simple way to test adrive for properly listening over skynet
 * for commands.  Run this program and follow the prompts.  It can simply 
 * send steer, brake, and gas commands over skynet.  */

#include "sn_msg.hh"
#include "string.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#define  SLEEP_TIME  100000 //in microsec.
#define MAX_LINE_LENGTH 99  //characters
#include "adrive_skynet_interface_types.h"

double get_command_from_prompt(void);
  
int main(int argc, char** argv)
{
  //  modulename myself = SNadrive_commander;
  sn_msg myoutput = SNdrivecmd;
  int sn_key;
  char* default_key = getenv("SKYNET_KEY");

  /* start out initializing skynet */
  if (2 == argc)
  {
    sn_key  = atoi(argv[1]);
  }
  else if (default_key != NULL )
  {
    sn_key = atoi(default_key);
  }
  else
  {
    printf("usage: sn_send_test key\nDefault key is $SKYNET_KEY\n");
    exit(2);
  }

  modulename myself = SNadrive_commander;
  skynet Skynet(myself, sn_key); 

  int socket = Skynet.get_send_sock(myoutput);

  /* Loop infinitely prompting the user then executing */
  while(1)
    {
      //      int socket;
      char inputstring[MAX_LINE_LENGTH];
      drivecmd_t my_command;
      my_command.my_command_type = set_position;
      printf("Enter a for accel, s for steering,");
      printf("or c to set steer acceleration, or v to set steer velocity,");
      printf(" e for estop command or t for transmission:");
 
      scanf("%99s", inputstring);
      switch (inputstring[0])
	{
	case 's':
	  my_command.my_actuator = steer;
	  my_command.number_arg = get_command_from_prompt();
	  break;
	case 'a':
	  my_command.my_actuator = accel;
	  my_command.number_arg = get_command_from_prompt();
	  break;
	case 'v':
	  my_command.my_actuator = steer;
	  my_command.my_command_type = set_velocity;
	  my_command.number_arg = get_command_from_prompt();
	  break;
	case 'c':
	  my_command.my_actuator = steer;
	  my_command.my_command_type = set_acceleration;
	  my_command.number_arg = get_command_from_prompt();
	  break;
	case 'e':
	  my_command.my_actuator = estop;
	  my_command.number_arg = get_command_from_prompt();
	  break;
	case 't':
	  my_command.my_actuator = trans;
	  my_command.number_arg = get_command_from_prompt();
	  break;
	  
	default:
	  printf("invalid actuator please try again");
	  break;
	}
      
      /* Send out the skynet command */
  Skynet.send_msg(socket, (void*)&my_command, sizeof(my_command), 0);
  printf("sent message %d %d %g\n", my_command.my_actuator, my_command.my_command_type, my_command.number_arg);

  usleep(SLEEP_TIME);      
    }
  
  return 0;
}


/* Get the command from the user */
double get_command_from_prompt(void)
{
  char inputstring[100];
     printf("\nEnter a command: steer, accel: [-1,1]\n estop: (0=DISABLE,1=PAUSE,2=RUN)\n steer  accel, steer velocity [0-1]\ntransmission 1 = drive 0 =  park and -1 = reverse::");
     scanf("%99s", inputstring);
     return strtod(inputstring, NULL);
}      
