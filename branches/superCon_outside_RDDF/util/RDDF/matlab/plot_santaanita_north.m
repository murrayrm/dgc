% script plot_santaanita_north.m
%
% Lars Cremean
% 2 May 05

wp = plot_corridor_file('../routes/santaanita_north_perimeter.bob',[0 0]);
%text(wp(end,1), wp(end,2), 'perimeter')

wp = plot_corridor_file('../routes/santaanita_oval_north.bob',[0 0]);
text(wp(end,1), wp(end,2), 'oval')

wp = plot_corridor_file('../routes/sitevisit_obstacles.bob',[0 0]);
text(wp(end,1), wp(end,2), 'obstacles')

wp = plot_corridor_file('../routes/sitevisit1.bob',[0 0]);
text(wp(end,1), wp(end,2), 'first')

wp = plot_corridor_file('../routes/sitevisit2.bob',[0 0]);
text(wp(end,1), wp(end,2), 'second')

wp = plot_corridor_file('../routes/sitevisit3.bob',[0 0]);
text(wp(end,1), wp(end,2), 'third')
