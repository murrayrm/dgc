NTHETA   = 7;  # how many variables represent the theta spline. quadratic spline: N-1 segments
MTHETA   = 5; # how many collocation points in each spline segment

NSPEED   = 6; # how many spline segments represent the speed spline. linear: num variables = num segments
MSPEED   = 5;  # how many collocation points in each spline segment

FINEMULTIPLE = 80;

# manually have to make sure that (NTHETA-1)*MTHETA = NSPEED*MSPEED

#theta
#dtheta
#ddtheta


# these are in the constraint order:
# N-1 left  d1 value
# N-1 right d1 value
# N-2 val continuity
# 1 initial condition (val)
#---------
# 3N-3


# a * allpolynomialcoeffs = b = W * y
# y = [d1; theta(0)]
a = zeros(3*(NTHETA-1), 3*(NTHETA-1));

# go through the 1st derivative points
for k=1:(NTHETA-1)
	a(k         ,:) = (NTHETA-1) * [zeros(1,3*(k-1)) 0 1 0 zeros(1,3*(NTHETA-k-1))];
	a(k+NTHETA-1,:) = (NTHETA-1) * [zeros(1,3*(k-1)) 0 1 2 zeros(1,3*(NTHETA-k-1))];
end

# continuous value
for k=1:(NTHETA-2)
	a(2*NTHETA-2+k,:) += [zeros(1,3*(k-1)) 1 1 1 zeros(1,3*(NTHETA-k-1))];
	a(2*NTHETA-2+k,:) -= [zeros(1,3*k)     1 0 0 zeros(1,3*(NTHETA-k-2))];
end

# val at the beginning
a(3*NTHETA-3,:) = [1 0 0 zeros(1,3*(NTHETA-2))];



# speed: state is speed at control points other than the first

filename = "dercoeffs.dat";
mkdercoeffs_writecollocationdata;

fil = fopen("speedestimatematrix.dat", "w+b", "ieee-le");
v = valcoeffs_speed;
L = rows(v);
fwrite(fil, inv(v'*v)*v', "double");
fclose(fil);


MTHETA *= FINEMULTIPLE;
MSPEED *= FINEMULTIPLE;

filename = "dercoeffsfine.dat";
mkdercoeffs_writecollocationdata;

fil = fopen("stateestimatematrix.dat", "w+b", "ieee-le");
v = valcoeffs(:,1:NTHETA);
fwrite(fil, inv(v'*v)*v', "double");
v = valcoeffs_speed;
fwrite(fil, inv(v'*v)*v', "double");
fclose(fil);

# write out the data. don't include the last column (offset), since it's trivial
dercoeffmatrix = inv(a) * W;
fil = fopen("dercoeffsmatrix.dat", "w+b", "ieee-le");
fwrite(fil, dercoeffmatrix(:,1:NTHETA) , "double");
fclose(fil);












#N  = 6; # how many variables represent this spline
#M1 = 20;  # how many collocation points in each spline segment
#M2 = 20;  # how many collocation points in each spline segment
#MFINE = 400;
#
#
##v     theta
#a     dtheta
#jerk  ddtheta


# these are in the constraint order:
# N-1 left  value
# N-1 right value
# N-2 d1 continuity
# 1 initial condition (d1)
#---------
# 3N-3
#
#
## a * allpolynomialcoeffs = b = W * y
## y = [val; d1(0)]
#a = zeros(3*(N-1), 3*(N-1));
#
## go through the values
#for k=1:(N-1)
#	a(k,:)     = [zeros(1,3*(k-1)) 1 0 0 zeros(1,3*(N-k-1))];
#	a(k+N-1,:) = [zeros(1,3*(k-1)) 1 1 1 zeros(1,3*(N-k-1))];
#end
#
## continuous d1
#for k=1:(N-2)
#	a(2*N-2+k,:) += [zeros(1,3*(k-1)) 0 1 2 zeros(1,3*(N-k-1))];
#	a(2*N-2+k,:) -= [zeros(1,3*k)     0 1 0 zeros(1,3*(N-k-2))];
#end
#
## val at the beginning
#a(3*N-3,:) = (N+1) * [0 1 0 zeros(1,3*(N-2))];
#
#
#M = M1;
#filename = "dercoeffs1.dat";
#mkdercoeffs_writecollocationdata;
#
#M = M2;
#filename = "dercoeffs2.dat";
#mkdercoeffs_writecollocationdata;
#
#M = MFINE;
#filename = "dercoeffsfine.dat";
#mkdercoeffs_writecollocationdata;
#
#v = valcoeffs(:,2:(N+1));
#fil = fopen("stateestimatematrix.dat", "w+b", "ieee-le");
#fwrite(fil, inv(v'*v)*v', "double");
#fclose(fil);
#
## write out the data. don't include the first column, since it's trivial
#dercoeffmatrix = inv(a) * W;
#fil = fopen("dercoeffsmatrix.dat", "w+b", "ieee-le");
#fwrite(fil, dercoeffmatrix(:,2:(N+1)) , "double");
#fclose(fil);
#
#
