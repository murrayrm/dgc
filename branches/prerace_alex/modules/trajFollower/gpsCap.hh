#ifndef ASTATE_TEST_HH
#define ASTATE_TEST_HH

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "VehicleState.hh"
#include "sn_msg.hh"
#include "DGCutils"
#include "StateClient.h"
#include "ggis.h"
#include "rddf.hh"

class gpsCap : public CStateClient {

  ofstream m_outputGPS;

  unsigned long long starttime;

  double timecalc;

public:
  gpsCap(int);
  void Active();


};

#endif
