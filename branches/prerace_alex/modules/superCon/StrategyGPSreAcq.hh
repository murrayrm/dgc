#ifndef STRATEGY_GPSREACQ_HH
#define STRATEGY_GPSREACQ_HH

//SUPERCON STRATEGY TITLE: GPS re-acquisition - ANYTIME STRATEGY
//HEADER FILE (.hh)
//DESCRIPTION: Strategy to handle the re-acquisition of GPS data by
//astate after an outage, where it is assumed that incorporating the
//newly re-acquired GPS data would result in a significant jump in
//astate's position estimate.
//NOTE: astate initiates this strategy by requesting from superCon
//that action be taken to ready Alice for the incorporation of the new
//GPS data into the astate estimate (which it is assumed will result
//in a significant jump in astate's output)

#include "Strategy.hh"
#include "sc_specs.h"
#include "stage_iterator.hh"

namespace s_gpsreacq {

#define GPSREACQ_STAGE_LIST(_) \
  _(estop_pause, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(confirm_pause_to_astate, ) \
  _(wait_astate_response, ) \
  _(clear_cmap, ) \
  _(trans_nominal, )
DEFINE_ENUM(gpsreacq_stage_names, GPSREACQ_STAGE_LIST)

}

using namespace std;
using namespace s_gpsreacq;

class CStrategyGPSreAcq : public CStrategy
{

/** METHODS **/
public:

  CStrategyGPSreAcq() : CStrategy() {}  
  ~CStrategyGPSreAcq() {}


  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

  /** HELPER METHODS **/
  bool checkAstateMoveForwardNoClear( bool checkBool, const SCdiagnostic *pdiag );

};

#endif
