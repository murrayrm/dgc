/* This is an implementation of a shell class that inherits from AdriveTImberClient
 * because adrive is written primarily in C and thus cannot inherit the class
 * as a whole. */


#ifndef ADRIVETIMBERCLIENT_HH
#define ADRIVETIMBERCLIENT_HH

#include <string>
#include <pthread.h>
#include <unistd.h>
#include <sstream>

#include <iomanip>


#include "CTimberClient.hh"
#include "StateClient.h"
#include "DGCutils"
using namespace std;




/**
 * AdriveTimberClient class.
 * input: none
 * output: none
 */
class AdriveTimberClient : public CTimberClient
{
public:
  /** Contstructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  AdriveTimberClient(int skynetKey);

  /** Standard destructor */
  ~AdriveTimberClient();

private:

};

#endif  // ADRIVETIMBERCLIENT_HH
