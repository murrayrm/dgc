//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_VP_MultiTracker.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// set drawing behavior for particles

void UD_VP_MultiTracker::set_show_particles(int value) 
{ 
  pos->set_show_particles(value);
  vel->set_show_particles(value);
}

//----------------------------------------------------------------------------

/// overriding UD_VP_Tracker version of this

void UD_VP_MultiTracker::update()
{
  pos->update();
  vel->update();
}

//----------------------------------------------------------------------------

/// overriding UD_VP_Tracker version of this

void UD_VP_MultiTracker::pf_reset()
{
  pos->pf_reset();
  vel->pf_reset();
}

//----------------------------------------------------------------------------

/// wrapper around vanishing point multi-tracker

UD_VP_MultiTracker *initialize_UD_VP_MultiTracker(UD_VanishingPoint *vp)
{
  printf("shia\n");

  return new UD_VP_MultiTracker(vp);
}

//----------------------------------------------------------------------------

/// constructor for multi-tracker.  instantiates a position tracker and
/// a velocity tracker

UD_VP_MultiTracker::UD_VP_MultiTracker(UD_VanishingPoint *vp) : UD_VP_Tracker(vp, 0, NULL, NULL, NULL)

{
  printf("blah1\n");

  pos = initialize_UD_VP_PositionTracker(vp);
  vel = initialize_UD_VP_VelocityTracker(vp);

  printf("blah2\n");
}

//----------------------------------------------------------------------------

/// don't need this 

void UD_VP_MultiTracker::pf_samp_prior(UD_Vector *samp)
{
  printf("shouldn't be in multi-tracker pf_samp_prior()\n");
  exit(1);
}

//----------------------------------------------------------------------------

/// don't need this

void UD_VP_MultiTracker::pf_dyn_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  printf("shouldn't be in multi-tracker pf_dyn_state()\n");
  exit(1);
}

//----------------------------------------------------------------------------

/// don't need this

void UD_VP_MultiTracker::pf_samp_state(UD_Vector *old_samp, UD_Vector *new_samp)
{
  printf("shouldn't be in multi-tracker pf_samp_state()\n");
  exit(1);
}

//----------------------------------------------------------------------------

/// don't need this

double UD_VP_MultiTracker::pf_condprob_zx(UD_Vector *samp)
{
  printf("shouldn't be in multi-tracker pf_condprob_zx()\n");
  exit(1);
}

//----------------------------------------------------------------------------

/// write current estimates of position and velocity vanishing point trackers

void UD_VP_MultiTracker::write(FILE *fp)
{
  write(fp, "\n");
}

//----------------------------------------------------------------------------

/// write current estimates of position and velocity vanishing point trackers
/// with specified terminal string

void UD_VP_MultiTracker::write(FILE *fp, char *terminal_string)
{
  pos->write(fp, ", ");
  vel->write(fp, terminal_string);
}

//----------------------------------------------------------------------------

/// draw position and velocity tracker parameters
/// candidate region

void UD_VP_MultiTracker::draw(int r, int g, int b)
{
  pos->draw(r, g, b);
  vel->draw(255 - r, 255 - g, 255 - b);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
