#!/bin/bash
#setups the users home directory to run gazebo and player interface
BASEDIR=~/local
PIConfig=~/dgc/local/files/cds.playerinterface.config
PIDir=~/dgc/bob/DevModules/PlayerInterface
GRIDFILE=~/dgc/local/grid_cds
IPFILE=~/dgc/local/ip
# backup oldfiles

if [ -f "$BASEDIR/grid" ]; then 
    echo "Backing up old grid file"
    cp $BASEDIR/grid $BASEDIR/oldgrid
fi

if [ ! -d "$BASEDIR" ]; then
    echo " Making $BASEDIR"
    mkdir $BASEDIR
    echo " Setting up CDS IP file in $BASEDIR"
    cp $IPFILE $BASEDIR/ip
fi

#now install cds specific files
echo " Setting up CDS grid file in $BASEDIR"
cp $GRIDFILE $BASEDIR/grid


#now install CDS config file 
echo "Setting up CDS PlayerInterface Config"
cp $PIConfig $PIDir/config
#source dgc.bashrc
echo  " Don't forget to source ~/dgc/local/files/dgc.bashrc"
