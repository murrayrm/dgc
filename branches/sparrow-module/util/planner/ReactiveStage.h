#ifndef _REACTIVESTAGE_H_
#define _REACTIVESTAGE_H_

#include "AliceConstants.h"
#include "CPath.hh"
#include "CMap.hh"
#include "traj.h"
#include "specs.h"

#define MAX_NUM_LAYERS             30
#define MAX_NUM_NODES_PER_LAYER    100

struct SNode
{
	NEcoord m_pos;
	double  m_distToPrevSq;
	double  m_bestCost;
	int     m_backPointer;

	SNode()
	{
		m_bestCost = 0.0;
	}
};

struct SLayer
{
	SNode*  m_pNodes;
	int     m_numNodes;
	double  m_sinTheta; // orientation of RDDF at this layer level
	double  m_cosTheta;
	double  m_radLow, m_radHigh;
	NEcoord m_layerMiddle;
	bool    m_bPassableNodata;

	SLayer()
	{
		m_pNodes = new SNode[MAX_NUM_NODES_PER_LAYER];
		m_numNodes = 0;
	}
	~SLayer()
	{
		delete m_pNodes;
	}
};

class CReactiveStage
{
	CTraj m_trajgentraj;
	CPath m_trajgenpath;
	SLayer* m_pLayers;

	CMap*   m_pMap;
	int     m_mapLayer;
	RDDF*   m_pRDDF;
	CTraj*  m_pTraj;

	double  m_initSpeed;

	void buildLayer(int layer, NEcoord& pos, double sinTheta, double cosTheta, double halfwidth,
									double maxLeft, double maxRight,
									double spacingAcrossLayer, bool bPassableNodata = false,
									CTraj* pPrevTraj = NULL);
	void processLayerToPoint(int layerIndex, int nextNodeIndex);
	double costBetweenNodes(SNode& node1, SNode& node2, bool bPassableNodata);

public:
	int run(VehicleState* pVehState, bool bIsStateFromAstate = true,
					CTraj* pPrevTraj = NULL);

	CReactiveStage(CMap *pMap, int mapLayerID, RDDF* pRDDF, bool USE_MAXSPEED = false);
	~CReactiveStage();
	CTraj* getTraj() { return m_pTraj; }
	CTraj* getSeedTraj() { return &m_trajgentraj; }
};

#endif // _REACTIVESTAGE_H_
