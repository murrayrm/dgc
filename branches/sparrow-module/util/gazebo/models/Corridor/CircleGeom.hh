#ifndef CIRCLEGEOM_HH
#define CIRCLEGEOM_HH

#include <gazebo/Geom.hh>

/** A simple red circle.
 * This body draws a circle 1cm above the ground (to avoid rendering issues with
 * objects close to each other) in a red color. It was originally made for the
 * purpose of drawing waypoints in the Gazebo world.
 *
 * Author: Henrik Kjellander
 * $Id$
 */
class CircleGeom : public Geom
{
  /**
   * Constructor creates a red circle
   * @param body the body
   * @param dSpaceID space id
   * @param posE   x-coordinate (Easting in meters)
   * @param posN   y-coordinate (Northing in meters)
   * @param rad    radius (meters)
   */
  public: CircleGeom( Body *body, const dSpaceID space, const double posE, const double posN, const double rad );

  // Destructor
  public: virtual ~CircleGeom();

  // Render the geom (GL)
  public: virtual void Render(RenderOptions *opt);

  // Flag set if the circle needs redrawing
  private: bool dirty;

  // Where the drawing takes place
  private: void draw();
  
  // Attributes of the circle
  public: double posE, posN;
  public: double radius;
  
};


#endif
