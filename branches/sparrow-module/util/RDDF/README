team/RDDF
=========

Lars Cremean, 14 Feb 04

Normal way to keep track of waypoint specification files in this directory:

*.rddf - RDDF format (defined by DARPA) files
*.bob  - Bob format files

rddf2bob.sh  -  Script to convert RDDF to Bob format
bob2rddf.sh  -  Script to convert Bob to RDDF format

If you want to test a new set of waypoints which already exist in Bob (or RDDF) format:

1. Run the script to convert your file to RDDF format (if it's in Bob format):
   # ./rddf2bob.sh < yourfile.bob > yourfile.rddf

2. Symbolically link "rddf.dat" to your rddf file.  This file is the universal one which
different modules will use (e.g. MatlabDisplay, GlobalPlanner, ...).
   # ln -fs yourfile.rddf rddf.dat

Format for *.rddf Files
======================
{int (unused), latitude[deg], longitude[deg], Offset[ft], Speed[mph]}

Format for *.bob Files
======================
{int (unused), Easting[m], Northing[m], Offset[m], Speed[m/s]}


Notes from DARPA_RACE_RDDF.bob:
==============================

Minimum lateral boundary offset:   1.829m = 6ft
Maximum lateral boundary offset: 243.840m = 800ft
Median  lateral boundary offset:   2.438m = 8ft
Average lateral boundary offset:   3.244m = 10.64ft 

Minimum speed limit:  2.235 m/s = 5mph
Maximum speed limit: 26.823 m/s = 60mph
Median  speed limit:  8.941 m/s = 20mph
Average speed limit:  9.664 m/s = 21.62mph


