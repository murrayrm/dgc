#include"RoadPainter.hh"
//#include"CCostFuser.hh"
#include "CCostFuser.hh"
#include"Rasterization.hh"
#include"PainterFunctors.hh"

#include"CMap.hh"

#include<math.h>

#include<iostream>
#include<iomanip>
using namespace std;

//When defined outputs a bunch of debug messages
//#define PROFILER_DEBUG_ALL

static const double eps = 1e-6;

CRoadPainter::CRoadPainter()
{
  m_praster = new CRasterization();
  m_cnt=10.0;
}

CRoadPainter::~CRoadPainter()
{
  if(m_praster)
    delete m_praster;
  m_praster=0;
}

void CRoadPainter::paint(const Road &road, CMap *pMap, int layer, int cnt_layer, CCostFuser *pFuser)
{
  if(road.length<2)
    return;

  //Instansiate the painter functor
  //PainterFunctorUniform pf(pMap, layer, pFuser, 1.0);
  //PainterFunctorDistance pf(pMap, layer, pFuser, 10.0);
  //PainterFunctorRelDistance pf(pMap, layer, pFuser, 10.0);
  PainterFunctorThresRelDistance pf(pMap, layer, cnt_layer, m_cnt, pFuser, 10.0);
  
  //cout<<"Painting road "<<m_cnt<<endl;

  //Convert the road segments to polygons
  Polygon p(4);

  //Init the first coordinates from the direction of the first road segment
  double dN = road[1].N - road[0].N;
  double dE = road[1].E - road[0].E;
  double norm = 1/sqrt(dN*dN + dE*dE);
  dN *= norm;
  dE *= norm;

  p[0].x = road[0].N - dE * road[0].width/2;
  p[0].y = road[0].E + dN * road[0].width/2;
  pMap->roundUTMToCellBottomLeft(p[0].x,p[0].y, &p[0].x, &p[0].y);
  
  p[1].x = road[0].N + dE * road[0].width/2;
  p[1].y = road[0].E - dN * road[0].width/2;
  pMap->roundUTMToCellBottomLeft(p[1].x,p[1].y, &p[1].x, &p[1].y);

  for(int i=1; i<road.length; ++i) {
    if(road[i].N==road[i-1].N && road[i].E==road[i-1].E)
      continue;  //0 length road
    
    //Calc direction vector through this point
    if(i==road.length-1) {
      dN = road[i].N - road[i-1].N;
      dE = road[i].E - road[i-1].E;
      norm = 1/sqrt(dN*dN + dE*dE);
      dN *= norm;
      dE *= norm;
    } else {
      if(road[i+1].N==road[i-1].N && road[i+1].E==road[i-1].E)
	continue;  //0 length road
      dN = road[i+1].N - road[i-1].N;
      dE = road[i+1].E - road[i-1].E;
      norm = 1/sqrt(dN*dN + dE*dE);
      dN *= norm;
      dE *= norm;
    }

    //Calculate the further most points of the painting rect
    p[2].x = road[i].N + dE * road[i].width/2;
    p[2].y = road[i].E - dN * road[i].width/2;
    pMap->roundUTMToCellBottomLeft(p[2].x,p[2].y, &p[2].x, &p[2].y);
  
    p[3].x = road[i].N - dE * road[i].width/2;
    p[3].y = road[i].E + dN * road[i].width/2;
    pMap->roundUTMToCellBottomLeft(p[3].x,p[3].y, &p[3].x, &p[3].y);

    //Draw this road section
    pf.set_center_line(road[i-1].N, road[i-1].E, road[i-1].width, road[i].N, road[i].E, road[i].width);
    pf.set_speed(road[i].speed);

    //pf.set_center_line(road[i-1].N, road[i-1].E, road[i].N, road[i].E);

    #ifdef PROFILER_DEBUG_ALL
      cerr<<"  Painting road section: "<<setprecision(10)<<road[i-1].N<<", "<<road[i-1].E<<" - "<<road[i].N<<", "<<road[i].E<<endl;
    #endif
    //    PainterFunctorUniform p2(pMap, layer, pFuser, 1.0);
    m_praster->rasterize(p, pMap->getResRows(), pMap->getResCols(), pf);

    //Switch end coordinates down to start coords for next segment
    p[0].x = p[3].x;
    p[0].y = p[3].y;
    p[1].x = p[2].x;
    p[1].y = p[2].y;
  }

  //cout<<"Painted "<<pf.pixels<<" ("<<pf.pixels2<<")"<<endl;
}

void CRoadPainter::paint(const Road &road, const Road &old_road, CMap *pMap, int layer, int cnt_layer, CCostFuser *pFuser)
{
  ++m_cnt;    //Increase the counter

  paint(road, pMap, layer, cnt_layer, pFuser);

  if(old_road.length<2)
    return;

  //Instansiate the painter functor
  //Delete the counter
  PainterFunctorUniform pf_old(pMap, layer, cnt_layer, m_cnt, pFuser, 0.0);

  //Convert the road segments to polygons
  Polygon p(4);

  //Init the first coordinates from the direction of the first road segment
  double dN = old_road[1].N - old_road[0].N;
  double dE = old_road[1].E - old_road[0].E;
  double norm = 1/sqrt(dN*dN + dE*dE);
  dN *= norm;
  dE *= norm;

  p[0].x = old_road[0].N - dE * old_road[0].width/2;
  p[0].y = old_road[0].E + dN * old_road[0].width/2;
  pMap->roundUTMToCellBottomLeft(p[0].x,p[0].y, &p[0].x, &p[0].y);
  
  p[1].x = old_road[0].N + dE * old_road[0].width/2;
  p[1].y = old_road[0].E - dN * old_road[0].width/2;
  pMap->roundUTMToCellBottomLeft(p[1].x,p[1].y, &p[1].x, &p[1].y);
  
  for(int i=1; i<old_road.length; ++i) {
    //Calc end coordinates
    if(i==old_road.length-1) {
      dN = old_road[i].N - old_road[i-1].N;
      dE = old_road[i].E - old_road[i-1].E;
      norm = 1/sqrt(dN*dN + dE*dE);
      dN *= norm;
      dE *= norm;
    } else {
      dN = old_road[i+1].N - old_road[i-1].N;
      dE = old_road[i+1].E - old_road[i-1].E;
      norm = 1/sqrt(dN*dN + dE*dE);
      dN *= norm;
      dE *= norm;
    }

    p[2].x = old_road[i].N + dE * old_road[i].width/2;
    p[2].y = old_road[i].E - dN * old_road[i].width/2;
    pMap->roundUTMToCellBottomLeft(p[2].x,p[2].y, &p[2].x, &p[2].y);
  
    p[3].x = old_road[i].N - dE * old_road[i].width/2;
    p[3].y = old_road[i].E + dN * old_road[i].width/2;
    pMap->roundUTMToCellBottomLeft(p[3].x,p[3].y, &p[3].x, &p[3].y);

    //Draw this road section
    if(i!=1)   //don't delete first segment
      m_praster->rasterize(p, pMap->getResRows(), pMap->getResCols(), pf_old);

    //Switch end coordinates down to start coords for next segment
    p[0].x = p[3].x;
    p[0].y = p[3].y;
    p[1].x = p[2].x;
    p[1].y = p[2].y;

  }

  //cout<<"Deleted "<<pf_old.pixels<<" ("<<pf_old.pixels2<<")"<<endl;
}

//direction in rads of vector
double CRoadPainter::direction(double x, double y) const
{
  if(x<eps && x>-eps) {  //x==0
    if(y<0)
      return M_PI;
    return 0;
  }

  double dir = atan(y/x);
  
  if(y<0)
    return dir+M_PI;

  if(dir<0)
    dir+=2*M_PI;

  return dir;
}


