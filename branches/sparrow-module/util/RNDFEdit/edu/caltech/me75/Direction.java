package edu.caltech.me75;
/**
 * Direction.java
 * An enumerated class to represent the various directions
 * @author Xiang Jerry He
 */

public class Direction {
	    /** a string to represent whether this direction is 
	     * up, down, left, right
	     */
		private final String name;
		private Direction(String name) { this.name = name; }
		
		public String toString() { return name; } 
		public static final Direction UP = new Direction(" up ");
		public static final Direction DOWN = new Direction("down");
		public static final Direction LEFT = new Direction("left");
		public static final Direction RIGHT = new Direction("right");
}
		