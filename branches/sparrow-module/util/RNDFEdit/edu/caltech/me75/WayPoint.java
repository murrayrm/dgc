package edu.caltech.me75;

/**
 * A class to represent a Way point
 * @author Xiang Jerry He
 *
 */
public class WayPoint {
	/** representing the id as an array
	 * for example, 12.3.16 would be [12 3 16] in this case 
	 */
    int[] id; /* include this info in toString? */
    /** x coordinate */
    double x;
    /** y coordinate */
    double y;
    
    private WayPoint() {} // Implicit super constructor: do not use
    
    /**
     * @param ident the id of this waypoint
     * @param x     the x coordinate of this waypoint
     * @param y     the y coordinate of this waypoint
     */
    WayPoint(String ident, double x, double y) {
    	this.x = x;
    	this.y = y;
    	
    	id = WayPoint.toIntRep(ident);
    }
    
    /**
     * @param id    the id of this waypoint
     * @param x     the x coordinate of this waypoint
     * @param y     the y coordinate of this waypoint
     */
     
	WayPoint(int[] id, double x, double y) {
		this.id = id;
		this.x = x;
		this.y = y;
	}
	
	/**
	 * 
	 * @param way the id of this waypoint in string form
	 * @return the id of this waypoint in array form
	 */
	public static int[] toIntRep(String way) {
		
		String[] tokens = way.split(".");
    	int[] iRep = new int[tokens.length];
    	for (int i = 0; i < tokens.length; i++) {
    		iRep[i] = Integer.parseInt(tokens[i]);
    	}
    	
    	return iRep;
	}
	
	public String toString() {
		return (x + " " + y);
	}
}