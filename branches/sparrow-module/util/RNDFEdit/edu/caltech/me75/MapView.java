package edu.caltech.me75;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.*;
import javax.swing.JPanel;
import java.util.Vector;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * MapView.java
 * A class to represent the display component 
 * for the RNDF map
 * @author Xiang Jerry He 
 */

public class MapView extends JPanel {
	
	/** the button panel of MapViewer */
	private boolean stopSignVisible=false;
	private MapViewer viewer;
	/** the map the viewer is displaying */
	private Map cMap;
	/** holds the perspective of this map
	 * more specifically the center and range of all way points 
	 */
	protected Perspective p; 

	/** current x position of the mouse point */
	private volatile int x;
	/** current y position of the mouse point */
	private volatile int y;

	
	/**
	 * a set of defaults for the rendering of the map
	 * e.g. the color and size of various objects
	 * @author Xiang Jerry He 
	 */
	private static class Default {
		public static final Color wayPointColor= Color.black;
		public static final Color laneColor = Color.blue; 
		public static final Color zonePerimColor = Color.red;
		public static final double wayPointSize = 3;
		public static final int waypointDisplayPrecision = 7;
		public static final int stopSignSize = 12;
	}
	
	/**
	 * reverses the current visibility of stop signs
	 */
	
	protected void toggleStopSignVisibility() {
		stopSignVisible = !stopSignVisible;
	}
	
	/**
	 * 
	 * @param viewer the MapViewer that is associated with this viewing component
	 */
	public MapView(MapViewer viewer) {
		super();
		this.viewer = viewer;
		cMap = null;
	}
	/**
	 * @param map the map that will be displayed 
	 */
	public MapView( Map map, MapViewer viewer ) {
		super();
		this.viewer = viewer;
		setMap(map);
	        
	}
	
	/**
	 * this sets our current map to reference to Map
	 * 
	 * warning: I used direct referencing rather than Map 
	 * copying to improve performance, but is is imperative that
	 * Map is never modified after calling setMap, or else it'll
	 * have unintended side-effects 
	 * 
	 * @param Map the Map that our instance variable cmap will reference to
	 */
	public void setMap(Map Map) {
	    this.cMap = Map; 
	    p = new Perspective(cMap,this.getWidth(), this.getHeight());
	    /** updates the current mouse location */
	    this.addMouseMotionListener(new MouseMotionListener() {
	    	public void mouseMoved(MouseEvent e) {
	    		x = e.getX();
	    		y = e.getY();
	    		double rfactor = Math.pow(10.0, Default.waypointDisplayPrecision);
	    		Double lattitude = new Double(Math.round(p.pixel2Lat(x)*rfactor)/rfactor);
	    		Double longitude = new Double(Math.round(p.pixel2Lon(y)*rfactor)/rfactor);
	    		viewer.xcoord.setText(lattitude.toString());
	    		viewer.ycoord.setText(longitude.toString());
	    	}
	    	public void mouseDragged(MouseEvent e) {
	    		
	    	}
	    	
	    });
	    /* display the new map */                                                                        
		repaint();         
	}                             
	
	/**
	 * 
	 * @return the current displayed RNDF Map
	 */
	public Map getMap() { 
	    return cMap;
	}
	

	/**
	 * @param g2 the Graphics2D component
	 * @param l the lane to be painted
	 * @param ps the perspective of the map
	 */
	public void paintLane(Graphics2D g2, Lane l, Perspective ps) {
		Vector<WayPoint> w = l.waypoints;
		System.out.println("\tpainting " + w.size() + " waypoints");
		int nwaypoint = w.size();
		WayPoint first = w.get(0);
		g2.setColor(Default.laneColor);
		GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
		thislane.moveTo(ps.cX(first.x), ps.cY(first.y));
		for(WayPoint p:w) {
			g2.setColor(Default.wayPointColor);
			double r = Default.wayPointSize;
			g2.fill(new Ellipse2D.Double(ps.cX(p.x)-r/2.0, ps.cY(p.y)-r/2.0, r, r));
			g2.setColor(Default.laneColor);
		    thislane.lineTo(ps.cX(p.x), ps.cY(p.y));
		}
		g2.draw(thislane);
		
		System.out.println("\t\tpainting "+l.stopSign.size()+" stop signs");
		if(stopSignVisible) {
			for(WayPoint st:(l.stopSign)) {
				g2.setColor(Color.RED);
				int r = Default.stopSignSize;
				g2.fill(new Ellipse2D.Double(ps.cX(st.x)-r/2.0, ps.cY(st.y)-r/2.0,r,r));
				//repaint the way point
				g2.setColor(Default.wayPointColor);
				g2.fill(new Ellipse2D.Double(ps.cX(st.x), ps.cY(st.y), 3, 3));
			}
		}
		g2.setColor(Default.laneColor);
	}
	/*
	 * 
	 * @param perimeter the perimeter to be painted
	 * @param ps the perspective of the map
	 * @author David Waylonis
	 */
	public void paintPerimeter(Graphics2D g2, Perimeter per, Perspective ps) {
		Vector<WayPoint> w = per.points;
		System.out.println("\tpainting " + w.size() + " waypoints");
		int nwaypoint = w.size();
		
		WayPoint first = w.get(0);
		GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
		thislane.moveTo(ps.cX(first.x), ps.cY(first.y));
		double r = Default.wayPointSize;
		for(WayPoint p:w) {
			g2.draw(new Ellipse2D.Double(ps.cX(p.x), ps.cY(p.y), r, r));
		    thislane.lineTo(ps.cX(p.x), ps.cY(p.y));
		}
		thislane.lineTo(ps.cX(w.get(0).x), ps.cY(w.get(0).y));
		g2.setColor(Default.zonePerimColor);
		g2.draw(thislane);
	}
	
	/**
	 * @param g2 the graphics component
	 * @param s the Spot to be painted
	 * @param ps the perspective of the map
	 */
	public void paintSpot(Graphics2D g2, Spot s, Perspective ps) {
		Vector<WayPoint> w = new Vector<WayPoint>();
		w.add(s.inPoint);
		w.add(s.checkPoint);
		System.out.println("\tpainting " + w.size() + " waypoints");
		int nwaypoint = w.size();
		
		WayPoint first = w.get(0);
		GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
		thislane.moveTo(ps.cX(first.x), ps.cY(first.y));
		for(WayPoint p:w) {
			g2.draw(new Ellipse2D.Double(ps.cX(p.x), ps.cY(p.y), 3, 3));
		    thislane.lineTo(ps.cX(p.x), ps.cY(p.y));
		}
		g2.setColor(Color.RED);
		g2.draw(thislane);
	}
	
	/**
	 * paints the visual representation of the RNDF file
	 * @param g the Graphics component
	 */
	protected void paintComponent( Graphics g ) {
		super.paintComponent(g);
		Graphics g2 = g.create();
        Graphics2D g2d = (Graphics2D)g2;
		//g2d.draw(new Rectangle2D.Double(10, 10, 800 ,600));
		//g2d.translate(25, -13);
		//g2d.translate(100, -50);
		//g2d.scale(4, 4);
        
		if(cMap != null) {
		    //p.setPerspective(this.getWidth(), this.getHeight());
			for(Segment s: cMap.segs) {
				System.out.println("painting segment " + s.segment_name);
				for(Lane l: s.lanes) {
					System.out.println("\tpainting Lane with width " + l.lane_width);
					paintLane(g2d, l, p);
				}
			}
			for(Zone z: cMap.zones) {
				System.out.println("painting zone " + z.zone_name);
				paintPerimeter(g2d, z.perimeter, p);
				for(Spot s: z.spots) {
					paintSpot(g2d, s, p);
				}
			}
		}
	}
}