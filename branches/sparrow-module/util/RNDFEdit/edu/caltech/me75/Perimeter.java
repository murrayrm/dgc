package edu.caltech.me75;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Vector;

/**
 * Used to represent and parse a zone perimeter.  A perimeter consists of multiple
 * points that outline the zone.
 * @author David Waylonis
 */
public class Perimeter {

	int[] perimeter_id;
	int num_perimeterpoints;
	Vector<WayPoint> points;
	Vector<Exit> exits;
	
	/**
	 * Default constructor.  Takes in a perimeter ID by which the perimeter can
	 * be identified.
	 * 
	 * @param id the perimeter ID
	 */
	public Perimeter(int[] id) {
		
		perimeter_id = id;
	}
	
	/**
	 * Read in and parse a perimeter from an RNDF file.  The perimeter ID should have
	 * already been read.
	 * 
	 * @param br the buffered reader for the RNDF file
	 * @throws IOException if we cannot read the file
	 * @throws MapFormatException if the RNDF is improperly formed
	 */
	public void readPerimeter(BufferedReader br) throws IOException, MapFormatException {
		
		System.out.println("Zone perimeter");
		  String line = br.readLine();
		  System.out.println(line);
		  String[] tokens = line.split("\\s");
		  /*assert(tokens[0].equals("segment"));
		  System.out.println("token0 is "+ tokens[0]);
		  id = Integer.parseInt(tokens[1]);
		  System.out.println("segment ID is " + id); */
		  points = new Vector<WayPoint>();
		  exits = new Vector<Exit>();
		  while(!line.startsWith("end_perimeter")) {
			  
		       System.out.println(line);
		       tokens = line.split("\\s");
		       if(line.startsWith("num_perimeterpoints")) {
		    	   System.out.println("numperimetertoken: " + tokens[1]);
		    	   num_perimeterpoints = Integer.parseInt(tokens[1]);
		       }
		       else if(tokens[0].equals("exit")) {
		    	   if (tokens.length < 3) {
		    		   throw new MapFormatException("Incorrectly formatted \"exit\" line!");
		    	   }
		    	   exits.add(new Exit(new WayPoint(tokens[1], -1, -1), new WayPoint(tokens[2], -1, -1)));
		       }
		       else if(!tokens[0].startsWith("/")) {
		    	   try {
		    		   points.add(new WayPoint(tokens[0], Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2])));
		    	   }
		    	   catch(Exception e) {}
		       }
		       line = br.readLine();
		  }
		  System.out.println("number of perimeter points is " + num_perimeterpoints);
		  System.out.println(line);
	}
}
