package edu.caltech.me75;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Used to represent and read in a zone from an RNDF file.  A zone consists of
 * a perimeter and a list of spots.
 */
public class Zone {

	int zone_id;
	int num_spots;
	String zone_name;
	Perimeter perimeter;
	Spot[] spots;
	
	
	/**
	 * Read in and parse a zone.  Find the perimeter and spots corresponding
	 * to the zone.
	 * 
	 * @param br the buffered reader for the RNDF
	 * @throws IOException if we fail to read in the file
	 * @throws MapFormatException if the file is incorrectly formatted
	 */
	void readZone(BufferedReader br) throws IOException, MapFormatException {
		
		System.out.println("Entering zone");
		  String line = br.readLine();
		  System.out.println(line);
		  String[] tokens = line.split("\\s");
		  /*assert(tokens[0].equals("segment"));
		  System.out.println("token0 is "+ tokens[0]);
		  id = Integer.parseInt(tokens[1]);
		  System.out.println("segment ID is " + id); */
		  while(!line.startsWith("spot")) {
			  
		       System.out.println(line);
		       tokens = line.split("\\s");
		       if(line.startsWith("num_spots")) {
		    	   System.out.println("numspottoken: " + tokens[1]);
		    	   num_spots = Integer.parseInt(tokens[1]);
		       }
		       else if(tokens[0].equals("zone_name")) {
		    	   zone_name = tokens[1];
		       }
		       else if(tokens[0].equals("perimeter")) {
		    	   perimeter = new Perimeter(WayPoint.toIntRep(tokens[1]));
		    	   perimeter.readPerimeter(br);
		       }
		       line = br.readLine();
		  }
		  System.out.println("number of spots is " + num_spots);
		  System.out.println("zone name is " + zone_name);
		  System.out.println(line);
	      tokens = line.split("\\s");
		  spots = new Spot[num_spots];
		  for(int i=0; i < num_spots; i++) {
			  spots[i] = new Spot(tokens[1]);
			  spots[i].readSpot(br);
			  line = br.readLine();
			  tokens = line.split("\\s");
		  }
		  assert(br.readLine().equals("end_zone"));
	}
}
