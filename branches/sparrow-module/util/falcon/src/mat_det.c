/*
 * mat_det.c - general purpose matrix routines
 *
 * version 0.50b, August 1995
 *
 * Copyright 1995, Michael Kantner, kantner@hot.caltech.edu
 *
 * Do not distribute without the consent of the author
 * No warranties are implied.  Use at your own risk.
 */

#include "matrix.h"

int mat_det(double *det, MATRIX *mat){

  if (mat == (MATRIX *)0) return -1;
  if (mat->ncols != mat->nrows) return -1;

  *det=mat_det_f(mat);

  return 0;
}

double mat_det_f(MATRIX *mat){

  register int i,j,k;
  register int rows = mat_get_rows(mat);
  register int sign = 1;
  register double det = 0;
  MATRIX *minor;

  if (rows==1) 
    return mat->real[0];
  if (rows==2)
    return (mat->real[0]*mat->real[3] - mat->real[1]*mat->real[2]);

  minor = mat_init(rows-1,rows-1);
  
  for (i=0; i<rows; i++){
    int cnt=0;

    for (j=1; j<rows; j++)      /* We skip the first column always */
      for (k=0; k<rows; k++){   /* Skip the appropriate row of stuff */
	if (k==i) continue;
	minor->real[cnt] = mat->real[j*rows+k];
	cnt++;
      }
    det = det + sign*mat->real[i]*mat_det_f(minor);
    sign *= -1;
  }

  mat_free(minor);
  return det;
}
