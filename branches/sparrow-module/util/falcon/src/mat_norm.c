/*
 * mat_norm.c - general purpose matrix routines
 *
 * version 0.50b, August 1995
 *
 * Copyright 1995, Michael Kantner, kantner@hot.caltech.edu
 *
 * Do not distribute without the consent of the author
 * No warranties are implied.  Use at your own risk.
 */

#include "matrix.h"
#include <math.h>

int mat_normalize(MATRIX *mat){ /* dst = a + b */
  register int i;
  register double maxval = 0.0;

  if (mat == (MATRIX *)0)  return -1;

  for (i = 0; i < mat->nrows*mat->ncols; i++)
    if (fabs(*(mat->real + i)) > maxval) 
      maxval = fabs(*(mat->real + i));

  for (i = 0; i < mat->nrows*mat->ncols; i++)
    *(mat->real + i) = *(mat->real + i)/maxval;

  return 0;
}

