#ifndef __TRAJ_INCLUDED__
#define __TRAJ_INCLUDED__

/*
 * traj.h - the header file for the trajectory data structure
 *
 * Trajectories are optimized 1-d lookup tables.
 */

typedef struct trajectory_data {
  double *time;
  double **data;
  int current;
  int nrows;			/* The number of time steps */
  int ncols;			/* The number of outputs */
  int flag;
} TRAJ_DATA;

#define TRAJF_BADTIME   0x0001

/* Creation and sizing */
TRAJ_DATA *traj_create(void);
int traj_resize(TRAJ_DATA *trjp, int rows, int cols);
void traj_free(TRAJ_DATA *trjp);

/* Automated creation */
TRAJ_DATA *traj_init(int rows, int cols);
TRAJ_DATA *traj_load(char *file);


int traj_reset(TRAJ_DATA *trjp);
int traj_setcurrent(TRAJ_DATA *trjp, int row);
int traj_read(TRAJ_DATA *trjp, double *vector, double time);
int traj_rate(TRAJ_DATA *trjp, double *vector, double time);

void traj_disp(TRAJ_DATA *trjp);

/* Accessor fucntions */
int traj_outputs(TRAJ_DATA *trjp);
int traj_rows(TRAJ_DATA *trjp);
int traj_current(TRAJ_DATA *trjp);
int traj_status(TRAJ_DATA *trjp);
int traj_row(TRAJ_DATA *trjp, int row, double *vector, double *other);

/* Changing individual rows */
int traj_setrow(TRAJ_DATA *trjp, int row, double time, double *data);

/* Save a trajectory to disk */
int traj_write(TRAJ_DATA *trjp, char *filename);

/*
 * Old function names, kept for posterity
 */
int traj_numcols(TRAJ_DATA *trjp);
int traj_numrows(TRAJ_DATA *trjp);
int traj_getcurrent(TRAJ_DATA *trjp);
int traj_datachange(TRAJ_DATA *trjp, double time, double *data, int row);
int traj_table(TRAJ_DATA *trjp, double *vector, int index);
int traj_table2(TRAJ_DATA *trjp, double *vector, double *other, int index);
int traj_rowset(TRAJ_DATA *trjp, int row);


#endif /* __TRAJ_INCLUDED__ */
