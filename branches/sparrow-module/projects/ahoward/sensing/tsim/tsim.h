/* 
 * Desc: Toy-world simulator for DGC
 * Date: 06 December 2006
 * Author: Andrew Howard
 * SVN: $Id$
*/

#ifndef TSIM_H
#define TSIM_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

/** @file

@brief TSim: simple block-world simulator for DGC.

**/
  
/// @brief Simulator context.
typedef struct tsim tsim_t;

/// @brief Create context and allocate resources.
tsim_t *tsim_alloc();

/// @brief Destroy context and free all resources.
int tsim_free(tsim_t *self);

/// @brief Initialize simulator.
///
/// @param[in] self Context
/// @param[in] filename Path to simulator world files.
/// @returns Returns 0 on success.
int tsim_open(tsim_t *self, char *filename);

/// @brief Finalize simulator.
///
/// @param[in] self Context
/// @returns Returns 0 on success.
int tsim_close(tsim_t *self);

/// @brief Generate an image from a simple camera.
///
/// @param[in] self Context
/// @param[in] veh_pose Vehicle pose in global frame (x,y,z,roll,pitch,yaw).
/// @param[in] cam2veh Camera-to-vehicle transform.
/// @param[in] cx,cy Image center (pixels).
/// @param[in] sx,sy Focal length (pixels).
/// @param[in] cols,rows Image size (pixels).
/// @param[in] channels Color channels (1 = mono or 3 = RGB).
/// @param[in] size Size of image buffer (must be at least channels * cols * rows).  
/// @param[in] data Image buffer.
int tsim_gen_image(tsim_t *self, double veh_pose[6], float cam2veh[4][4],
                   float cx, float cy, float sx, float sy, int cols, int rows, int channels,
                   int size, uint8_t *data);

  
#if USE_GL

/// @brief Get the simulation boundaries in the global frame.
int tsim_get_bounds(tsim_t *self,
                    double *ax, double *ay, double *az, double *bx, double *by, double *bz);

/// @brief Render map using GL.
///
/// The map is rendered in the simulation frame.  Use
/// tsim_get_bounds() to get the translation from global to simulation
/// frame.
//  
/// @param[in] self Context.
int tsim_render_map(tsim_t *self);

/// @brief Render vehicle at the given pose.
///
/// @param[in] self Context.
/// @param[in] pose Vehicle pose in UTM frame (x,y,z,roll,pitch,yaw).
int tsim_render_vehicle(tsim_t *self, double veh_pose[6]);

#endif

#ifdef __cplusplus
}
#endif

#endif
