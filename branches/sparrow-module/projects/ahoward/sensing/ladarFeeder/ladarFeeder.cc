
/* 
 * Desc: Ladar feeder module using JPLV
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sparrow/display.h>
#include <pose3.h>

#include <sn_msg.hh>
#include <sn_types.h>
#include <VehicleState.hh>
#include <sensnet.h>
#include <sensnet_ladar.h>

#include "sick/sick_driver.h"
#include "ladar_log.h"
#include "ladar_feeder_opt.h"



/// @brief Ladar feeder class
class LadarFeeder
{
  public:   

  /// Default constructor
  LadarFeeder();

  /// Default destructor
  ~LadarFeeder();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  /// Parse the config file
  int parseConfigFile(const char *configDir);

  /// Initialize feeder for live capture
  int initLive(const char *configDir);
  
  /// Finalize feeder for live capture
  int finiLive();

  /// Capture a scan 
  int captureLive();
  
  /// Initialize feeder for log replay
  int initReplay(const char *logDir);

  /// Finalize feeder for log replay
  int finiReplay();

  /// Capture a scan from the log
  int captureReplay();

  /// Initialize feeder for simulated capture
  int initSim(const char *configDir);
  
  /// Finalize feeder for simulated capture
  int finiSim();

  /// Capture a simulated scan 
  int captureSim();

  /// Get the predicted vehicle state
  int getState(double timestamp);

  /// Initialize everything except capture
  int init();

  /// Finalize everything except capture
  int fini();
  
  /// Process a scan
  int process();

  /// Publish data over sensnet
  int writeSensnet();

  public:
  
  /// Initialize log for writing
  int logOpenWrite(const char *configDir);

  /// Initialize log for reading
  int logOpenRead(const char *logDir);

  /// Finalize log
  int logClose();

  /// Write the current images to the log
  int logWrite();

  /// Read the current scan from the log (used during playback)
  int logRead();

  public:

  /// Sparrow callback; occurs in sparrow thread
  static int onUserQuit(long);

  /// Sparrow callbacks; occurs in sparrow thread
  static int onUserPause(long);
    
  /// Sparrow callbacks; occurs in sparrow thread
  static int onUserLog(long);
  
  /// Sparrow thread
  pthread_t spThread;

  /// Mutex for thead sync
  pthread_mutex_t spMutex;

  public:

  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon; int skynetKey;

  /* REMOVE
  // Our module id (SkyNet)
  modulename moduleId;
  char *moduleName;
  */

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;
  char *sensorName;

  // What mode are we in?
  enum {modeLive, modeReplay, modeSim} mode;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Is logging enabled?
  bool logging;

  // Id of the ladar
  int ladarId;

  // Port we are talking to
  const char *ladarPort;

  // SICK driver
  sick_driver_t *sick;
  
  // Sensor-to-vehicle transform
  float sens2veh[4][4];

  // Calibration stats (display only).
  // Sensor position (x,y,z) and rotation (roll,pitch,yaw).
  vec3_t sensPos, sensRot;
  
  // Log file (reading or writing, depending on mode)
  ladar_log_t *log;

  // SensNet context
  sensnet_t *sensnet;

  // Current scan id
  int scanId;

  // Current scan time
  double scanTime;

  // Current state data
  sensnet_state_t state;

  // Point data (bearing and range)
  int numPoints;
  float points[361][2];
  
  // Start time for computing stats
  double startTime;
  
  // Capture stats
  int capCount;
  double capTime, capRate, capPeriod;

  // Logging stats
  int logCount, logSize;
};


// Pointer to the one instance of this class.  Sparrow
// needs this in order to work correctly.
static LadarFeeder *self;


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stdout, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
LadarFeeder::LadarFeeder()
{
  memset(this, 0, sizeof(*this));

  this->scanId = -1;

  return;
}


// Default destructor
LadarFeeder::~LadarFeeder()
{
  return;
}


// Parse the command line
int LadarFeeder::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;

  /* REMOVE
  // Fill out module id
  this->moduleName = this->options.module_id_arg;
  this->moduleId = modulenamefromString(this->moduleName);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->moduleName);
  */
  
  // Fill out sensor id
  this->sensorName = this->options.sensor_id_arg;
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  return 0;
}


// Parse the config file
int LadarFeeder::parseConfigFile(const char *configDir)
{  
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG", configDir, this->sensorName);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Set the serial port
  this->ladarPort = this->options.port_arg;
  
  // Parse transform
  float px, py, pz;
  float rx, ry, rz;
  if (sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz) < 3)
    return ERROR("syntax error in sensor pos argument");
  if (sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz) < 3)
    return ERROR("syntax error in sensor rot argument");
    
  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2veh);
  
  // Record euler angles for display only
  this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;

  return 0;
}


// Initialize feeder for live capture
int LadarFeeder::initLive(const char *configDir)
{
  // Load configuration file
  if (this->parseConfigFile(configDir) != 0)
    return -1;

  if (this->ladarPort == NULL)
    return ERROR("ladar port is not set");

  /* TODO
  // Fill out the ladar id
  this->ladarId = atoi(ladarId);
  if (this->ladarId <= 0)
    return ERROR("invalid ladar id: %s", this->ladarId);
  */

  // Initialize the ladar
  this->sick = sick_driver_alloc();
  assert(this->sick);

  // Try connecting at 500000
  if (sick_driver_open(this->sick, this->ladarPort, 500000, 500000, 100, 10) != 0)
  {
    // Try connecting at 9600
    if (sick_driver_open(this->sick, this->ladarPort, 9600, 500000, 100, 10) != 0)
      return ERROR("unable to connect to ladar %s", ladarId);
  }

  if (this->options.enable_log_flag)
    if (this->logOpenWrite(configDir) != 0)
      return -1;
    
  this->mode = modeLive;
  
  return 0;
}


// Finalize feeder for live capture
int LadarFeeder::finiLive()
{
  if (this->log)
    this->logClose();

  sick_driver_close(this->sick);
  sick_driver_free(this->sick);
  this->sick = NULL;
    
  return 0;
}


// Capture a scan 
int LadarFeeder::captureLive()
{
  int i, numRanges;
  float ranges[181];

  this->numPoints = 0;

  // Read data from sensor.
  // TODO add return codes and check them so we can ignore CRC errors.
  if (sick_driver_read(this->sick, 181, &numRanges, ranges, NULL) != 0)
    return 0;

  // Unpack the data.  Assumes standard format for SICK.
  for (i = 0; i < numRanges; i++)
  {
    this->points[i][0] = (i - numRanges/2) * M_PI/180;
    this->points[i][1] = ranges[i];
  }
  this->numPoints = i;
  
  this->scanId += 1;
  this->scanTime = dgc_gettimeofday();

  // Get the matching state data
  if (this->getState(this->scanTime) != 0)
    return -1;
  
  return 0;
}


// Initialize feeder for log replay
int LadarFeeder::initReplay(const char *logDir)
{
  // Load configuration file
  if (this->parseConfigFile(logDir) != 0)
    return -1;

  // Open log file for reading
  if (this->logOpenRead(logDir) != 0)
    return -1;

  this->ladarPort = "replay";
    
  this->mode = modeReplay;
  
  return 0;
}


// Finalize feeder for log replay
int LadarFeeder::finiReplay()
{
  this->logClose();
  
  return 0;
}


// Capture a scan from the log
int LadarFeeder::captureReplay()
{
  // MAGIC
  // 75Hz
  usleep(13000);
  
  if (this->logRead() != 0)
    return -1;
  
  return 0;
}



// Initialize feeder for simulated capture
int LadarFeeder::initSim(const char *configDir)
{
  //char filename[256];

  // Fill out the ladar id
  /* TODO
  this->ladarId = atoi(ladarId);
  if (this->ladarId <= 0)
    return ERROR("invalid ladar id: %s", ladarId);
  */

  // Load configuration file
  if (this->parseConfigFile(configDir) != 0)
    return -1;

  this->ladarPort = "sim";

  // Initialize logging
  if (this->options.enable_log_flag)
    if (this->logOpenWrite(configDir) != 0)
      return -1;

  this->mode = modeSim;
  
  return 0;
}


// Finalize feeder for simulated capture
int LadarFeeder::finiSim()
{    
  return 0;
}


// Capture a simulated scan 
int LadarFeeder::captureSim()
{
  double timestamp;
  int i;
  float a, b, d;
  float pt, pr;

  // Simulate 75Hz
  usleep(13000);
  
  timestamp = dgc_gettimeofday();

  // Get the current state data
  if (this->getState(timestamp) != 0)
    return -1;

  a = this->sens2veh[2][0];
  b = this->sens2veh[2][1];
  d = this->sens2veh[2][3] - VEHICLE_TIRE_RADIUS;
  
  // Compute intersection with nominal ground plane
  for (i = 0; i < 181; i++)
  {
    // Range and bearing of point on the ground (sensor frame)
    pt = (i - 90) * M_PI / 180;
    pr = -d / (a * cos(pt) + b * sin(pt));
    if (pr < 0 || pr > 80 || !finite(pr)) // MAGIC
      pr = 80;    
    this->points[i][0] = pt;
    this->points[i][1] = pr;
  }
  this->numPoints = i;
  
  this->scanId += 1;
  this->scanTime = timestamp;

  return 0;
}


// Initialize everything except image capture
int LadarFeeder::init()
{    
  if (true)
  {
    // Initialize SensNet
    this->sensnet = sensnet_alloc();
    if (sensnet_connect(this->sensnet,
                        this->spreadDaemon, this->skynetKey, this->sensorId) != 0)
      return ERROR("unable to connect to sensnet");

    // Subscribe to vehicle state messages
    if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState), 100) != 0)
      return ERROR("unable to join state group");
  }

  if (!this->options.disable_sparrow_flag)
  {
    // This looks weird, but we include the display table here so that
    // the pointers are initialized correctly.  The table is allocated
    // statically, so we can only have one instance of this class.
    #include "ladarFeederSp.h"

    // Initialize CLI
    if (dd_open() < 0)
      return ERROR("unable to open display");
    dd_usetbl(display);

    // Bind keys
    dd_bindkey('q', onUserQuit);
    dd_bindkey('Q', onUserQuit);
    dd_bindkey('p', onUserPause);
    dd_bindkey('P', onUserPause);
    dd_bindkey('l', onUserLog);
    dd_bindkey('L', onUserLog);
    
    // Kick off thread for console interface
    pthread_mutex_init(&this->spMutex, NULL);
    pthread_create(&this->spThread, NULL, (void* (*) (void*)) dd_loop, this);
  }

  return 0;
}


// Finalize everything except capture
int LadarFeeder::fini()
{
  // Clean up the CLI
  if (!this->options.disable_sparrow_flag)
  {
    pthread_join(this->spThread, NULL);
    pthread_mutex_destroy(&this->spMutex);
    dd_close();
  }
  
  // Clean up SensNet
  if (sensnet_disconnect(this->sensnet) != 0)
    return ERROR("unable to connect to sensnet");
  sensnet_free(this->sensnet);
  
  return 0;
}


// Get the predicted vehicle state
int LadarFeeder::getState(double timestamp)
{
  int blob_id, blob_len;
  VehicleState state;

  memset(&this->state, 0, sizeof(this->state));
    
  // Get the state newest data in the cache
  if (sensnet_peek(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, &blob_id, &blob_len) != 0)
    return -1;
  if (blob_id >= 0)
  {
    if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR,
                     SNstate, blob_id, sizeof(state), &state) != 0)
      return 0; // TODO something
  }

  // TODO: do prediction by comparing timestamps
  this->state.timestamp = (double) state.Timestamp / 1e6;

  // Set local pose
  this->state.pose_local[0] = state.localX;
  this->state.pose_local[1] = state.localY;
  this->state.pose_local[2] = state.localZ;
  this->state.pose_local[3] = state.localRoll;
  this->state.pose_local[4] = state.localPitch;
  this->state.pose_local[5] = state.localYaw;

  // Set global pose
  this->state.pose_global[0] = state.Northing;
  this->state.pose_global[1] = state.Easting;
  this->state.pose_global[2] = state.Altitude;
  this->state.pose_global[3] = state.Roll;
  this->state.pose_global[4] = state.Pitch;
  this->state.pose_global[5] = state.Yaw;

  // Set velocities
  this->state.vel_vehicle[0] = state.Vel_N;
  this->state.vel_vehicle[1] = state.Vel_E;
  this->state.vel_vehicle[2] = state.Vel_D;
  this->state.vel_vehicle[3] = state.RollRate;
  this->state.vel_vehicle[4] = state.PitchRate;
  this->state.vel_vehicle[5] = state.YawRate;

  //MSG("state %.3f %.3f %.3f", (double) state.Timestamp * 1e-6, state.Northing, state.Easting);
  
  return 0;
}


// Process a scan
int LadarFeeder::process()
{
  // TODO?
  
  return 0;
}


// Publish data
int LadarFeeder::writeSensnet()
{
  int i;
  pose3_t pose;
  sensnet_ladar_blob_t blob;

  // Construct the blob header
  blob.blob_type = SENSNET_LADAR_BLOB;
  blob.sensor_id = this->sensorId;
  blob.scanid = this->scanId;
  blob.timestamp = this->scanTime;
  blob.state = this->state;

  // Sensor to vehicle transform
  memcpy(blob.sens2veh, this->sens2veh, sizeof(this->sens2veh));

  // Vehicle to local transform
  pose.pos = vec3_set(blob.state.pose_local[0],
                      blob.state.pose_local[1],
                      blob.state.pose_local[2]);
  pose.rot = quat_from_rpy(blob.state.pose_local[3],
                           blob.state.pose_local[4],
                           blob.state.pose_local[5]);  
  pose3_to_mat44f(pose, blob.veh2loc);  

  // Copy the scan data
  blob.num_points = this->numPoints;
  for (i = 0; i < this->numPoints; i++)
  {
    assert(i < (int) (sizeof(blob.points) / sizeof(blob.points[0])));
    assert(i < (int) (sizeof(this->points) / sizeof(this->points[0])));
    blob.points[i][0] = this->points[i][0];
    blob.points[i][1] = this->points[i][1];
  }
  
  // Write blob
  if (sensnet_write(this->sensnet, this->sensorId, SENSNET_LADAR_BLOB,
                    this->scanId, sizeof(blob), &blob) != 0)
    return ERROR("unable to write blob");

  return 0;
}


// Initialize logging for writing
int LadarFeeder::logOpenWrite(const char *configDir)
{
  time_t t;
  char timestamp[64];
  char logDir[256];
  char cmd[256];
  ladar_log_header_t header;

  // Construct log name
  t = time(NULL);
  strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
  snprintf(logDir, sizeof(logDir), "%s-%s", timestamp, this->sensorName);
    
  // Create log file
  this->log = ladar_log_alloc();
  assert(this->log);

  // Construct log header
  memset(&header, 0, sizeof(header));
  header.ladar_id = this->ladarId;

  // Open log file for writing
  if (ladar_log_open_write(this->log, logDir, &header) != 0)
    return ERROR("unable to open log: %s", logDir);

  // Copy configuration files
  snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s", configDir, this->sensorName, logDir);
  system(cmd);

  return 0;
}


// Initialize log for reading
int LadarFeeder::logOpenRead(const char *logDir)
{
  ladar_log_header_t header;
  
  // Open log file for reading
  this->log = ladar_log_alloc();
  assert(this->log);
  if (ladar_log_open_read(this->log, logDir, &header) != 0)
    return ERROR("unable to open log: %s", logDir);

  // Fill out the ladar id
  this->ladarId = header.ladar_id;
  
  return 0;
}


// Finalize log
int LadarFeeder::logClose()
{
  if (!this->log)
    return 0;

  ladar_log_close(this->log);
  ladar_log_free(this->log);
  this->log = NULL;
  
  return 0;
}


// Write the current scan to the log
int LadarFeeder::logWrite()
{
  int i;
  ladar_log_scan_t scan;
  
  if (!this->log)
    return 0;

  // Construct the scan header
  scan.scanid = this->scanId;
  scan.timestamp = this->scanTime;

  scan.state.timestamp = this->state.timestamp;
  memcpy(scan.state.pose_local, this->state.pose_local, sizeof(scan.state.pose_local));
  memcpy(scan.state.pose_global, this->state.pose_global, sizeof(scan.state.pose_global));
  memcpy(scan.state.vel_vehicle, this->state.vel_vehicle, sizeof(scan.state.vel_vehicle));
  memcpy(scan.sens2veh, this->sens2veh, sizeof(scan.sens2veh));

  // Copy the scan data
  for (i = 0; i < this->numPoints; i++)
  {
    assert(i < (int) (sizeof(this->points) / sizeof(this->points[0])));
    assert(i < (int) (sizeof(scan.points) / sizeof(scan.points[0])));
    scan.points[i][0] = this->points[i][0];
    scan.points[i][1] = this->points[i][1];
  }
  scan.num_points = i;
  
  // Write to log
  if (ladar_log_write(this->log, &scan) != 0)
    return ERROR("unable to write to log");

  // Update stats (size is very approximate)
  this->logCount++;
  this->logSize = this->logCount * sizeof(scan) / 1024 / 1024;
  
  return 0;
}


// Read the current scan from the log
int LadarFeeder::logRead()
{
  int i;
  ladar_log_scan_t scan;

  if (!this->log)
    return 0;
  
  // Read scan from log
  if (ladar_log_read(this->log, &scan) != 0)
    return ERROR("unable to read log");

  // Read scan header
  this->scanId = scan.scanid;
  this->scanTime = scan.timestamp;

  this->state.timestamp = scan.state.timestamp;
  memcpy(this->state.pose_local, scan.state.pose_local, sizeof(this->state.pose_local));
  memcpy(this->state.pose_global, scan.state.pose_global, sizeof(this->state.pose_global));
  memcpy(this->state.vel_vehicle, scan.state.vel_vehicle, sizeof(this->state.vel_vehicle));
  memcpy(this->sens2veh, scan.sens2veh, sizeof(this->sens2veh));

  // Read scan data
  for (i = 0; i < scan.num_points; i++)
  {
    assert(i < (int) (sizeof(this->points) / sizeof(this->points[0])));
    assert(i < (int) (sizeof(scan.points) / sizeof(scan.points[0])));
    this->points[i][0] = scan.points[i][0];
    this->points[i][1] = scan.points[i][1];
  }
  this->numPoints = i;

  //MSG("read %d points", this->numPoints);
    
  return 0;
}


// Handle user events; occurs in sparrow thread
int LadarFeeder::onUserQuit(long)
{
  assert(self);
  self->quit = true;
  return DD_EXIT_LOOP;
}


// Handle user events; occurs in sparrow thread
int LadarFeeder::onUserPause(long)
{
  assert(self);
  self->pause = !self->pause;
  return 0;
}


// Handle user events; occurs in sparrow thread
int LadarFeeder::onUserLog(long)
{
  assert(self);
  self->logging = !self->logging;
  return 0;
}



// Main program thread
int main(int argc, char **argv)
{
  int status;

  // Create feeder
  self = new LadarFeeder();
  assert(self);
 
  // Parse command line options
  if (self->parseCmdLine(argc, argv) != 0)
    return -1;

  if (self->options.sim_flag)
  {
    // Initialize for simulated capture
    if (self->initSim(".") != 0)
      return -1;
  }
  else if (self->options.replay_given)
  {
    // Initialize for replay
    if (self->initReplay(self->options.replay_arg) != 0)
      return -1;
  }
  else
  {
    // Initialize for live capture
    if (self->initLive(".") != 0)
      return -1;
  }

  // Initialize everything
  if (self->init() != 0)
    return -1;

  self->startTime = dgc_gettimeofday();
  
  // Start processing
  while (!self->quit)
  {
    // Capture incoming scan directly into the ladar buffers
    if (self->mode == LadarFeeder::modeSim)
    {
      if (self->captureSim() != 0)
        break;
    }
    else if (self->mode == LadarFeeder::modeReplay)
    {
      if (self->captureReplay() != 0)
        break;
    }
    else
    {
      if (self->captureLive() != 0)
        break;
    }

    // Compute some diagnostics
    self->capCount += 1;
    self->capTime = dgc_gettimeofday() - self->startTime;
    self->capRate = (float) self->capCount / self->capTime;
    self->capPeriod = 1000.0 / self->capRate;
    
    // If paused, give up our time slice.
    if (self->pause)
    {
      usleep(0);
      continue;
    }
    
    // Process one scan
    pthread_mutex_lock(&self->spMutex);
    status = self->process();
    pthread_mutex_unlock(&self->spMutex);
    if (status != 0)
      break;

    // Publish data
    if (self->writeSensnet() != 0)
      break;
    
    // Write to the log
    if (self->options.enable_log_flag && self->logging)
      self->logWrite();
  }
  
  // Clean up
  self->fini();
  if (self->mode == LadarFeeder::modeSim)
    self->finiSim();
  else if (self->mode == LadarFeeder::modeReplay)
    self->finiReplay();
  else
    self->finiLive();
  delete self;

  MSG("program exited cleanly");
  
  return 0;
}
