#ifndef _CVECTORMAP_CC_
#define _CVECTORMAP_CC_
#include "CVectorMap.h"

inline double min(double x, double y) { return (x < y) ? x : y; };
inline double max(double x, double y) { return (x > y) ? x : y; };
	
// Print the data to a file
void CMapObject::print(ofstream *f)
{
		for (int i = 0; i < m_numVertices; i++) 
			(*f) << m_vertices[i].E << " " << m_vertices[i].N << endl;
}

/** Interpolate. */
void CMapObject::interpolate(double res)
{
	CMapObject original; 
	original = *this; 
	m_numVertices = 0; 
	for (int i=0; i<original.m_numVertices-1; i++) 
	{
		NEcoord d = original.m_vertices[i+1] - original.m_vertices[i];
		double norm = d.norm();
		d /= norm;
		for (double dist = 0.0; dist < norm; dist += res)
		{
			m_vertices[m_numVertices++] = original.m_vertices[i] + d * dist;
			if (m_numVertices>=CVECTORMAP_MAX_VERTICES)
				return;
		}
	}
}

/**
	Find the nearest point to coord p and return the index and squared distance. Note
	that this function assumes that the first local minimum it meets is the global minimum. 
*/
void CLaneBoundary::getNearestPoint(NEcoord p, double *minDistSqd, int *index)
{
	*minDistSqd = 10000.0;
	*index = 0; 
	
	// Iterate along the line 
	for (int i=0; i<m_numVertices; i++)
	{
		// Find the squared distance to p 
		NEcoord d = m_vertices[i] - p;
		double distSqd = d.N*d.N + d.E*d.E;
		// If it is less than the previous value update minDistSqd
		if (distSqd < *minDistSqd)
		{
			*minDistSqd = distSqd; 
			*index = i;
		}
		// If it is more than the previous value then we have passed the nearest point, the stored values are best
		else
		{
			return; 
		}
	}
}

/** 
	Find yaw at particular index.
*/
double CLaneBoundary::getYaw(int index)
{
	// If we are indexing the last point, use the yaw for the previous point 
	if (index > (m_numVertices - 2)) 
	{
		index = m_numVertices - 2; 
	}

	// The vector pointing along the line	
	NEcoord d = m_vertices[index + 1] - m_vertices[index];

	// Not sure I trust atan2 but anyway... 
	return atan2(d.N,d.E);
}
	
/** 
	Find the distance to and yaw of the nearest point on a lane boundary to p. 
	Expensive, but is used only for calculating cost for each landmark rather than for
	every point along a path. Returns whether the routine was successful. 
*/ 
bool CVectorMap::getDistanceAndYawOfNearestLaneBoundary(NEcoord p, double* distanceSqd, double* yaw)
{
	double minDistSqd = 100000.0;
	int closestLane = -1; 
	int closestPointOnLane = -1;
	// Iterate through all the lane boundaries looking for the closest  
	for (int i = 0; i < m_numLaneBoundaries; i++)
	{
		double distSqd;
		int index; 
		// Find the nearest point on this lane
		m_pLaneBoundary[i]->getNearestPoint(p, &distSqd, &index);
		// If it is the nearest yet remember it
		if (distSqd < minDistSqd)
		{
			minDistSqd = distSqd;
			closestLane = i;
			closestPointOnLane = index; 
		}
	}
	
	// If we found a lane return the requested values
	if (closestLane != -1)
	{
		*distanceSqd = minDistSqd; 
		*yaw = m_pLaneBoundary[closestLane]->getYaw(closestPointOnLane);
		return true; // success
	}
	return false; // failure (presumably no lanes had been setup, i.e. m_numLaneBoundaries = 0)
}
		
/** Returns whether it is legal for Alice to be at a particular point p. */
bool CVectorMap::pointIsLegal(NEcoord p)
{
	// hackty hack hack
	if (m_pRDDF != NULL)
	{
		if (!m_pRDDF->isPointInCorridor(p))
		{
			return false;
		}
	}
	else	
	// If the point is outside the legal driving region we cannot drive here. 
	if (!m_LegalRegion.pointInRegion(p))
	{
		return false; 
	}
	
	// If the point is inside a static obstacle we cannot drive here either. 
	for (int i = 0; i < m_numStaticObstacles; i++)
	{
		if (m_pStaticObstacle[i]->pointInRegion(p))
		{
			return false; 
		}
	}
	
	// Otherwise we can drive here. 
	return true;
}

/** Returns whether point p is in a safety zone. */
bool CVectorMap::pointInSafetyZone(NEcoord p)
{
	// Check all the safety zones: 
	for (int i = 0; i < m_numSafetyZones; i++)
	{
		if (m_pSafetyZone[i]->pointInRegion(p))
		{
			return true; 
		}
	}
	
	// Otherwise return false
	return false;
}

/** Just use the RDDF class functionality to check if points are inside the legal region.  */
void CVectorMap::fakeUsingRDDF(RDDF *pRDDF)
{
	m_pRDDF = pRDDF;
}

// Returns whether p is within the region
int CPolygonObject::pointInRegion(NEcoord p)
{	
	// This code does work but until a module exists (probably as part of traffic planning) to send us
	// the legal driving region it's somewhat redundant except for in unit testing! 
  int i, j, c = 0;
  for (i = 0, j = m_numVertices-1; i < m_numVertices; j = i++) {
    if ((((m_vertices[i].E <= p.E) && (p.E < m_vertices[j].E)) ||
         ((m_vertices[j].E <= p.E) && (p.E < m_vertices[i].E))) &&
        (p.N < (m_vertices[j].N - m_vertices[i].N) * (p.E - m_vertices[i].E) / (m_vertices[j].E - m_vertices[i].E) + m_vertices[i].N))
      c = !c;
  }
  return c;
}

/** Constructor. */
CVectorMap::CVectorMap()
{
	m_pRDDF = NULL;
	m_numSafetyZones = 0; 
	m_numStaticObstacles = 0; 
	m_numLaneBoundaries = 0; 
}

/*-------------------------------- CURRENTLY UNUSED! ------------------------------------------------ 

// Is the line COMPLETELY in the region? (no intersects allowed!) 
bool CPolygonObject::lineInRegion(NEcoord p1, NEcoord p2)
{
	bool p1in = pointInRegion(p1);
	bool p2in = pointInRegion(p2);
	NEcoord midpoint = (p1 + p2)*0.5;
	bool midpointin = pointInRegion(midpoint);
	// for now just worry about edges and midpoints
	return (p1in&&p2in&&midpointin);

	// !!! This doesn't work, not sure why. 
	NEcoord q1, q2, in;
	double A1,B1,C1,A2,B2,C2,det;
	cout << "1" << endl;
	// check whether line intersects any edges - will be more important once we have obstacles! 
	for (int i = 0; i < m_numVertices-1; i++) 
	{
		q1 = m_vertices[i];
		q2 = m_vertices[i+1];
		A1 = p2.N-p1.N;
		B1 = p1.E-p2.E;
		C1 = A1*p1.E+B1*p1.N;
		A2 = q2.N-q1.N;
		B2 = q1.E-q2.E;
		C2 = A1*q1.E+B1*q1.N;
		det = A1*B2 - A2*B1;
    if(det == 0.0)
        continue;
    else
    {
        in.E = (B2*C1 - B1*C2)/det;
        in.N = (A1*C2 - A2*C1)/det;
    }
    if ((min(p1.E,p2.E) < in.E)&&(in.E < max(p1.E,p2.E))&&(min(q1.E,q2.E) < in.E)&&(in.E < max(q1.E,q2.E)))
   	{
   		cout << "WATEVER" << endl;
    	return false; 
    }
   }
   cout << "3" << endl;
   return true; 
 }
    

// This doesn't work very well when there are sharp (acute) angles in the RDDF... 
void CVectorMap::generateFromRDDF(RDDF *pRDDF)
{
	int numPoints = pRDDF->getNumTargetPoints();
	ofstream rddfUTM("matlab_data/rddfUTM.spat");
	NEcoord d;
	// Find first point
	NEcoord c = pRDDF->getWaypoint(0);
	double yaw = pRDDF->getTrackLineYaw(0);
	double yaw1, yaw2; 
	double radius = pRDDF->getRadius(0);
	d.N = radius * cos(yaw);
	d.E = radius * sin(yaw); 
	m_vertices[0] = m_vertices[2*numPoints+2] = c - d;
	m_numVertices = 2*numPoints+3;
	// Find endpoint
	c = pRDDF->getWaypoint(numPoints-1);
	yaw = pRDDF->getTrackLineYaw(numPoints-2);
	radius = pRDDF->getRadius(numPoints-1);
	d.N = radius * cos(yaw);
	d.E = radius * sin(yaw);  
	m_vertices[numPoints + 1] = c + d;
	// Iterate along RDDF adding points on either side 
	cout << "Number of waypoints in RDDF: " << numPoints << endl;
	for (int i=0; i<numPoints; i++)
	{
		c = pRDDF->getWaypoint(i);
		if (i!=0) yaw1 = pRDDF->getTrackLineYaw(i-1);
		yaw2 = pRDDF->getTrackLineYaw(i);
		if (i==0)
			yaw = yaw2;
		else if (i==(numPoints-1))
			yaw = yaw1;
		else 
			yaw = (yaw1 + yaw2) / 2.0;
		radius = pRDDF->getRadius(i);
		rddfUTM << c.E << " " << c.N << endl;
		d.N = - radius * sin(yaw);
		d.E = + radius * cos(yaw); 
		m_vertices[i+1] = c + d; 
		m_vertices[2*numPoints+1-i] = c - d; 
	}
}
 */
#endif
