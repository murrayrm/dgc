load landmarks.spat;
[r,c]=size(landmarks);
hold on
for i=[1:r]
	start = landmarks(i,1:2);
	length = 0.6;
	start(2,1) = start(1,1) + length*cos(landmarks(i,3));
	start(2,2) = start(1,2) + length*sin(landmarks(i,3));
	plot(start(:,1),start(:,2),'k-','LineWidth',3);
	% pause
end

load targets.spat;
[r,c]=size(targets);
hold on
for i=[1:r]
	start = targets(i,1:2);
	length = 0.8;
	start(2,1) = start(1,1) + length*cos(targets(i,3));
	start(2,2) = start(1,2) + length*sin(targets(i,3));
	% plot(start(:,1),start(:,2),'g-','LineWidth',3);
	% pause
end