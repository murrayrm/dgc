#include <iostream>
using namespace std;
#include "CSpatialPlanner.h"
#include "VehicleState.hh"
#include "CVectorMap.h"
#include "CCLandMark.h"
#include <string>
#include <sstream>
#include <rddf.hh>
#ifndef PI
#define PI 3.1415927 
#endif
int main(int argc, char *argv[])
{
	ofstream startfinishFile("matlab_data/teststartfinish.txt");
	ofstream regionFile("matlab_data/testlegalregion.txt");
	ofstream laneFile1("matlab_data/lane1");
	ofstream laneFile2("matlab_data/lane2");
	ofstream laneFile3("matlab_data/lane3");
	
	/*---------------- Setup a vector map -----------------------*/
	CVectorMap map; 
	
	// Legal driving region. 
	map.m_LegalRegion.m_vertices[0].N = 0.0;
	map.m_LegalRegion.m_vertices[0].E = 0.0;
	
	map.m_LegalRegion.m_vertices[1].N = 0.0;
	map.m_LegalRegion.m_vertices[1].E = 100.0;
	
	map.m_LegalRegion.m_vertices[2].N = 100.0;
	map.m_LegalRegion.m_vertices[2].E = 100.0;
	
	map.m_LegalRegion.m_vertices[3].N = 100.0;
	map.m_LegalRegion.m_vertices[3].E = 0.0;
	
	map.m_LegalRegion.m_vertices[4].N = 0.0;
	map.m_LegalRegion.m_vertices[4].E = 0.0;
	
	map.m_LegalRegion.m_numVertices = 5; 
	
	map.m_LegalRegion.print(&regionFile);
	
	// Lanes
	CLaneBoundary left, middle, right; 

	left.m_vertices[0].N = 0.0; 
	left.m_vertices[0].E = 40.0; 
	
	left.m_vertices[1].N = 10.0; 
	left.m_vertices[1].E = 40.0; 
	
	left.m_vertices[2].N = 30.0; 
	left.m_vertices[2].E = 30.0; 
	
	left.m_vertices[3].N = 40.0; 
	left.m_vertices[3].E = 30.0; 
	
	left.m_vertices[4].N = 60.0; 
	left.m_vertices[4].E = 40.0; 
	
	left.m_vertices[5].N = 60.0; 
	left.m_vertices[5].E = 40.0; 
	
	left.m_vertices[6].N = 100.0; 
	left.m_vertices[6].E = 40.0;
	
	NEcoord offset;
	offset.N = 0.0;
	offset.E = 10.0;
	
	for (int i=0; i<7; i++)
	{
		middle.m_vertices[i] = left.m_vertices[i] + offset;
		right.m_vertices[i] = middle.m_vertices[i] + offset;
	}
	
	left.m_numVertices = 7; 
	middle.m_numVertices = 7; 
	right.m_numVertices = 7; 
	
	left.interpolate(4.0);
	middle.interpolate(4.0);
	right.interpolate(4.0);
	
	map.m_numLaneBoundaries = 3;
	map.m_pLaneBoundary[0] = &left; 
	map.m_pLaneBoundary[1] = &middle; 
	map.m_pLaneBoundary[2] = &right; 
	
	left.print(&laneFile1);
	middle.print(&laneFile2);
	right.print(&laneFile3);

	// Start position	
	VehicleState v;
	CCLandMark start; 
	start.m_pos.E = v.Easting = 55.0;
	start.m_pos.N = v.Northing = 5.0;
	v.Yaw = 0.0;
	start.m_theta = PI/2 - v.Yaw;
	start.log(&startfinishFile);
	
	// Finish position
	CCLandMark target; 
	target.m_pos.E = 55.5;
	target.m_pos.N = 95.0;
	target.m_theta = 1.55; 
	target.log(&startfinishFile);
	
	/*---------------- Create the spatial planner ----------------*/ 
	CSpatialPlanner* p = new CSpatialPlanner(&map); 
	
	
	/* Test using RDDF
	RDDF rddf("rddf.dat");
	CSpatialPlanner* p = new CSpatialPlanner(&rddf); 
	VehicleState v;
	v.Easting = 442536;
	v.Northing = 3.83434e+06;
	v.Yaw = 1.55;
	
				int nearestPoint = m_pRDDF->getCurrentWaypointNumberFirst(pos); 
			int nextPoint = nearestPoint + 4; 
			int numPoints = m_pRDDF->getNumTargetPoints(); 
			if (nextPoint >= numPoints) nextPoint = numPoints - 1;
			m_finish.m_pos = m_pRDDF->getWaypoint(nextPoint);
			m_finish.m_theta = m_pRDDF->getTrackLineYaw(nextPoint);
			if (m_finish.m_theta > PI) m_finish.m_theta -= 2*PI;
			if (m_finish.m_theta < -PI) m_finish.m_theta += 2*PI; 
			m_finish.log(&targetsFile);
			
	*/
	
	/*--------------- Input arguments: runtime and reverse allowed? ------ */ 
	double time = 0.1;
	bool reversedAllowed = false; 
	if (argc > 1) 
	{
		string s = argv[1];
		istringstream ss(s); 
		ss >> time; 
	}
	if (argc > 2) 
	{
		string s = argv[2];
		istringstream ss(s); 
		ss >> reversedAllowed; 
	}
	
	
	p->run(&v,target,time,reversedAllowed);
	
	// Change target position
	// p->run(&v,target,time,reversedAllowed);
	
	// Update start position 
	// p->run(&v,target,time,reversedAllowed);
	

	
	ofstream landmarkFile("matlab_data/landmarks.spat"); 
	landmarkFile.precision(2); 
	landmarkFile.setf(ios::fixed);
	p->saveTree(&landmarkFile);
	// Change map

	map.m_LegalRegion.print(&regionFile);
	
  return 0;
}

/* ifstream f("teststartfinish.txt");
  
  if (!f.is_open())
		logfile << "Failed to open file: " << "teststartfinish.txt" << endl; 
	else
	{ 
		f >> (m_start->m_pos.E);
		f >> (m_start->m_pos.N);
		f >> (m_start->m_theta);
		m_start->m_depth = 0;
		f >> m_finish.m_pos.E;
		f >> m_finish.m_pos.N;
		f >> m_finish.m_theta;
	}

	ifstream f2("testmap.txt");

	int i = 0;
	if (!f2.is_open())
		logfile << "Failed to open file: " << "testmap.txt" << endl; 
	else
		do { 
				f2 >> m_map.m_vertices[i].E;
				f2 >> m_map.m_vertices[i].N;
				i++;
		} while (!f2.eof()); 
	logfile << "Read " << i << " vertices of legal region." << endl; 
	
	m_map.m_numVertices=i; 
	
	run(timeAllowed,reverseAllowed); */

