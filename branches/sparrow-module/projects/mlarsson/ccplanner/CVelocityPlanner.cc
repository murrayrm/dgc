#ifndef _CVELOCITY_PLANNER_CC_
#define _CVELOCITY_PLANNER_CC_
#include "math.h"
#include "CVelocityPlanner.h"
#include "AliceConstants.h"
#include "CDynMap.hh"
#include <iostream>
#define CVELOCITYPLANNER_TIME_HORIZON 3.0
#define CVELOCITYPLANNER_MAXACCELERATION 1.0
#define CVELOCITYPLANNER_MINACCELERATION -3.0
#define CVELOCITYPLANNER_NUM_KS 20
#define CVELOCITYPLANNER_MAX_SPEED 15.0

/** Constructor. */ 
CVelocityPlanner::CVelocityPlanner(CDynMap *pDynMap)
{
	m_pDynMap = pDynMap;
}

/** Main velocity planning loop. */ 
int CVelocityPlanner::run(VehicleState *state, double initialTime, CTraj *traj)
{
	cout << "CVelocityPlanner::run planning velocity. " << endl; 
  m_pTraj = traj; 
  double initialVelocity = state->Speed2(); 
  if (initialVelocity < 0.1) initialVelocity = 0.1;
  double maxK = CVELOCITYPLANNER_MAXACCELERATION / initialVelocity;
  double minK = CVELOCITYPLANNER_MINACCELERATION / initialVelocity;
  double kInterval = (maxK - minK) / CVELOCITYPLANNER_NUM_KS; 
  
  // Find the nearest point on the trajectory to our position and use this as the start 
  int startIndex = traj->getClosestPoint(state->Northing_rear(),state->Easting_rear()); 

/*
	// Set low speed HACK
	int endIndex = m_pTraj->getNumPoints() - 1;  
	double speed = 1.0; 
	for (int i=0; i<endIndex ; i++)
	{
		NEcoord herePos = m_pTraj->getNEcoord(i); 
    NEcoord d = m_pTraj->getNEcoord(i+1) - herePos;
    double dnorm = d.norm(); 
    double cos_theta = d.E / dnorm;
    double sin_theta = d.N / dnorm;
    m_pTraj->setPoint(i,herePos.N,speed*sin_theta,0.0,herePos.E,speed*cos_theta,0.0); 
  }
  return 0;
 */
 
  // Iterate through different spatial accelerations, starting with a high value and 
  // working down. Use the highest acceleration that avoids collisions and the current speed limit. 
  for (double k = maxK; k >= minK; k -= kInterval) 
    if (tryK(k,startIndex,initialTime,initialVelocity))
    {
    	double a = k * initialVelocity; 
    	cout << "Desired acceleration: " << a << endl; 
      return 0;
     }
  cout << "Help! Could not find an acceleration that avoids collision!" << endl; 
  return 1; 
}

/** Try a certain value of k, where acceleration = k * v   */ 
bool CVelocityPlanner::tryK(double k,int startIndex, double initialTime, double initialVelocity)
{  
  double velocity = initialVelocity;
  double time = initialTime; 
  double s0 = 0.0;
  double s1 = 0.0;
  int i = startIndex; 
  int endIndex = m_pTraj->getNumPoints() - 1;  
  while (time < CVELOCITYPLANNER_TIME_HORIZON)
    {
      // Don't want to go so fast that we reach the end of our trajectory. 
      if (i >= endIndex)
				return !m_stopAtEnd;
      NEcoord herePos = m_pTraj->getNEcoord(i); 
      NEcoord d = m_pTraj->getNEcoord(i+1) - herePos;
      double dnorm = d.norm(); 
 
      s1 += dnorm;
      
      /* Having makefile issues with checkRectangle so removing this check for now. 
      double cos_theta = d.N / dnorm;
      double sin_theta = d.E / dnorm; 
      if (!m_pDynMap->checkRectangle(herePos.N,herePos.E,time,cos_theta,sin_theta,VEHICLE_LENGTH, VEHICLE_WIDTH))
				return false; */
      velocity = initialVelocity + k * s0;
      if (velocity > CVELOCITYPLANNER_MAX_SPEED)
      	return false;
      double acceleration = k * velocity;
      if ((acceleration > CVELOCITYPLANNER_MAXACCELERATION)||(acceleration < CVELOCITYPLANNER_MINACCELERATION))
      	return false; 
      m_pTraj->setSpeed(i,velocity);
      // It's ok to stop, but we should stop iterating. Don't want to start reversing here. 
      if (velocity <= 0.0)
				return true; 
      time += 1 / k * log ( (initialVelocity + k * s1) / (initialVelocity + k * s0) ); 
      i++;
      s0 = s1;
    }
  return true;  
}

/*

void CDynRefinementStage::outputTraj(CTraj* pTraj)
{
	int i;
	double NEout[6];
	double n,e;


	char notrans = 'N';
	int rows = NUM_COLLOCATION_POINTS_FINE;
	int cols = NUM_POINTS;
	double alpha = 1.0;
	double beta = 0.0;
	int incr = 1;

	// Use the solver state to theta matrices to transform the solver state vector into
	// yaw angles and their derivatives on a fine collocation spacing.
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffsfine, &rows,
				 m_pSolverState, &incr, &beta, m_pOutputTheta, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffsfine, &rows,
				 m_pSolverState, &incr, &beta, m_pOutputDTheta, &incr);

	// Compute the speed values along the trajectory on a fine collocation spacing
	cols = NUM_SPEED_SEGMENTS;
	dgemv_(&notrans, &rows, &cols, &alpha, m_valcoeffsfine_speed, &rows,
				 m_pSolverState+NUM_POINTS, &incr, &beta, m_pOutputSpeed, &incr);
	dgemv_(&notrans, &rows, &cols, &alpha, m_d1coeffsfine_speed, &rows,
				 m_pSolverState+NUM_POINTS, &incr, &beta, m_pOutputDSpeed, &incr);


	n = e = 0.0;

	// Compute the spline coefficients corresponding to the steering angle spline
	// along the trajectory. These are needed by integrateNEfine(), which is called
	// further down in this function.
	getSplineCoeffsTheta(m_pSolverState);

	pTraj->startDataInput();

	// Loop through the collocation points (fine spacing) and output N, E and derivatives
	// to pTraj
	for(i=0; i<NUM_COLLOCATION_POINTS_FINE-1; i++)
	{
		m_pOutputTheta[i] += m_initTheta;
		m_pOutputSpeed[i] += m_initSpeed;

		double sin_m_pOutputTheta = sin(m_pOutputTheta[i]);
		double cos_m_pOutputTheta = cos(m_pOutputTheta[i]);

		NEout[0] = m_pSolverState[NPnumVariables-1]*n + m_initN;
		NEout[3] = m_pSolverState[NPnumVariables-1]*e + m_initE;
		NEout[1] = m_pOutput Speed[i]*cos_m_pOutputTheta;
		NEout[4] = m_pOutputSpeed[i]*sin_m_pOutputTheta;
		NEout[2] = m_pOutputSpeed[i] / m_pSolverState[NPnumVariables-1] *
			( m_pOutputDSpeed[i] * cos_m_pOutputTheta - m_pOutputSpeed[i]*m_pOutputDTheta[i]*sin_m_pOutputTheta );
		NEout[5] = m_pOutputSpeed[i] / m_pSolverState[NPnumVariables-1] *
			( m_pOutputDSpeed[i] * sin_m_pOutputTheta + m_pOutputSpeed[i]*m_pOutputDTheta[i]*cos_m_pOutputTheta );
		pTraj->setPoint(NEout+0, NEout+3);

		// Integrate through to next point along trajectory on fine collocation spacing
		integrateNEfine(i, n, e);
	}
}
*/

#endif


