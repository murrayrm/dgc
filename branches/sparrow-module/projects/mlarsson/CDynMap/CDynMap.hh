#ifndef __CDYNMAP_HH__
#define __CDYNMAP_HH__


#include "CMapPlus.hh"
#include "movingObstacle.hh"


/**
   Maximum number of moving obstacles that can be handled simultaneously by the dynamic map.
*/
#define MAX_NUM_OBSTACLES           300

/**
   The resolution of the time sampling ([s])
*/
#define TIME_RESOLUTION             0.1

/**
   The amount by which to perform growing in time ([s])
*/
#define TIME_GROW                   1



/**
   CDynMap handles the moving obstacles (dynamic map) used by the dynamic planner. It inherits from CMap to get access
   to functionality related to the different coordinate frames, such as transforming between NE and win coordinates.
*/
class CDynMap : public CMap {

private:

  /**
     inputList is an array containing the latest messages received from detection and tracking. It contains
     present state (position, size, orientation, velocity) and tracked history. The array is indexated using
     obstacle ID.
  */
  movingObstacle *inputList;


  /**
     This is an internal struct used to store information about the grown moving obstacles. It is updated every time
     getDataRectangle() is called, since Alice's orientation could have changed between different calls to that function.
     The length and width properties, however, are only updated every time update() is called, since these shouldn't be recomputed
     unless there is a chance that they have changed. The array is indexated using obstacle ID.
  */
  struct movingObstacleProperties {
    NEcoord cornerPos;
    NEcoord corner2Pos;
    NEcoord side1;
    NEcoord side2;
    double speed;
    double length;             // These are length and width of obstacles *before* they are grown.
    double width;              //   -- " --
  } *obstaclesGrown;


  /**
     This array contains the ID numbers of the obstacles that have been added so far. The IDs are
     inserted in the list in the order they arrive from tracking and detection. The number of
     IDs that have been inserted at each time instant is equal to numobst.
  */
  int *IDlist;

  /**
     Stores the number of obstacles
  */
  int numobst;



  /**
     Performs obstacle growing given that Alice currently has the orientation
     defined by cos_theta and sin_theta. This function is called from getDataRectangle(), since
     the moving obstacles have to be grown according to Alice's current orientation.
  */
  void growObstacles(double cos_theta, double sin_theta);


  /**
    Returns whether a moving obstacle will be at a specified spacetime coordinate, where the space coordinate
    is provided in the window frame. True is returned if moving obstacles are abscent at that coordinate.

    Before calling getCellData(), growObstacles() should be called to make sure that growing takes place;
    getCellData() uses the obstacle information stored in obstaclesGrown[], so the elements of this array have to
    be up to date.
  */
  bool getCellDataWin(int winRow, int winCol, double time);
  

  /**
    Returns whether a moving obstacle will be at a specified spacetime coordinate. True is returned
    if moving obstacles are abscent at that coordinate.

    Before calling getCellData(), growObstacles() should be called to make sure that growing takes place;
    getCellData() uses the obstacle information stored in obstaclesGrown[], so the elements of this array have to
    be up to date.
  */
  bool getCellData(NEcoord point, double time);


 public:
  CDynMap::CDynMap();
  ~CDynMap();

  /**
     Initialize map and add moving obstacle layer.
   */
  int initMap(double UTMNorthing, double UTMEasting, int numRows, int numCols, double resRows, double resCols, int verboseLevel);


  /**
     Initialize map and add moving obstacle layer.
   */
  int initMap(char* fileName);


  /**
    Updates the internal list of moving obstacle tracking data with the obstacle data passed as an argument to
    this function.
    Returns false if the obstacle couldn't be added, i.e. if the maximum number of obstacles would be exceeded.
    Returns true if the obstacle was added.
  */
  bool update(movingObstacle* obstacle);


  /**
     Returns an array with map values according to the format of CMap::getDataRectangle().
     It is assumed that enough memory has been allocated for pF, pX, pY and numCells.
  */
  void getDataRectangle(double UTMNorthing, double UTMEasting, double time, double cos_theta, double sin_theta,
			double radlat, double radlong, bool *pF, double *pX, double *pY, int *numCells);
			
			
  /**
     Returns whether Alice can be at the specified position and orientation at 
     time time without colliding with a vehicle. Radlat and radlong specify Alice's length and width respectively. 
  */
  bool checkRectangle(double UTMNorthing, double UTMEasting, double time, double cos_theta, double sin_theta,
			double radlat, double radlong);



  /**
     Returns an array with map values during time interval (Time +- deltaT) at a
     specified location. Map values are stored in pF, and the corresponding time instants in pT.
     *numCells contains the number of time points that where retrieved.
     It is assumed that enough memory has been allocated for pF,pT,numCells.
  */
  void getTimeData(double UTMNorthing, double UTMEasting, double Time, double deltaT,
  		   bool *pF, double *pT, int *numCells);


};


#endif
