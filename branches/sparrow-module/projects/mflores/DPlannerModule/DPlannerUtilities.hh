/* 
	DPlannerUtilities.hh
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	Created on 12-12-2006.
*/ 

#include "Polytope.h"
#include "OTG_Interfaces.h" 
#include "OTG_NURBSBuilder.h"

#include "rddf.hh"

CPolytope* ConstructPolytope(int start_waypoint, int stop_waypoint, RDDFVector rddfvector);
void ConstructInitialGuess(int NofFlatOutputs, NURBS* Nurbs, TimeInterval* timeInterval, int start_waypoint, int stop_waypoint, RDDFVector rddfvector, double** Weights, double*** ControlPoints);

