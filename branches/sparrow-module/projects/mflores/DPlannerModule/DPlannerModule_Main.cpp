/*
 *  DPlannerModule_Main.cpp
 *  DPlannerModule
 *
 *  Created by Melvin E. Flores on 11/21/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "CDPlannerModule.hh"

using namespace std;

int main(int argc, char* const argv[])
{ 
	cout << "Dynamic Planner is starting..." << endl;
	int SkynetKey   = 0;
	char* pSkynetKey = getenv("SKYNET_KEY");
	if(pSkynetKey != NULL)
	{
	  SkynetKey = atoi(pSkynetKey);
	}
	
	bool WAIT_STATE =  true;

	CDPlannerModule* DPlannerModule = new CDPlannerModule(SkynetKey, WAIT_STATE);
	DPlannerModule->ActiveLoop();

    	cout << "Dynamic Planner is ending." << endl;
	//while(!_kbhit());
	
	return (int) 0;
} 


