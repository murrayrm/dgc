%Plot in 3D where gimbal is pointing
%from file 'log'

data = load('log');
[e f] = size(data);
nc = 201;
e1 = e / nc;

%statedata = load('states');
%stateadvance = 10;

for d=1:1
    
    figure;
    box on;
    % Have to run the file and check the limits and compute the argumetns of xlim, yim, zlim
    xlim([0 400]);
    ylim([0 400]);
    zlim([0 2]);

    tb = (d-1) * nc + 1;
    te = d * nc;
    plot3(data(tb:te,6), data(tb:te,7), data(tb:te,8), 'bo');
    %unnecessary: line(data(1:e,5), data(1:e,6), data(1:e,7), 'Color', 'c')
    hold on;
    
    init = [0 0 0];
    pos = init + [-2 0 1.5];
    plot3(pos(1), pos(2), pos(3), 'x');
    line([init(1) pos(1)], [init(2) pos(2)], [init(3) pos(3)], 'Color', 'b')
        
    for d1=tb:te
        
        z = pos(3);
        x = (z * (tan(deg2rad(data(d1,10))))) / (sqrt((tan(deg2rad(data(d1,11))))^2 + 1));
        y = tan(deg2rad(data(d1,11))) * x;
    
        x = x - pos(1);
        y = y - pos(2);
        z = z - pos(3);
    
        switch(data(d1,1))
            case 1
                line([pos(1) x],[pos(2) y],[pos(3) z], 'Color', 'g');
            case 2
                line([pos(1) x],[pos(2) y],[pos(3) z], 'Color', 'r');
        end

    %hold off;
    %pause(2)

    end
end