#ifndef __CDYNCOSTFUSER_HH__
#define __CDYNCOSTFUSER_HH__

#include <stdlib.h>
#include <math.h>
#include "CMap.hh"
#include "CMapPlus.hh"
#include "MapConstants.h"
#include "CCorridorPainter.hh"
#include "frames/coords.hh"
#include "CElevationFuser.hh"
#include "../movingObstacle.hh"
#include "CCostFuser.hh"
#define max(a,b) (((a)>(b))?(a):(b))
#define min(a,b) (((a)<(b))?(a):(b))

/**
If a cell have been fused to a speed lower than this, the data association function
will consider it to belong to the moving obstacle.
*/
#define DA_THRESHOLD  _mapRDDF->getDataUTM<double>(_layerNumRDDF, point.N, point.E)*0.8 

struct CDynCostFuserOptions : CCostFuserOptions {
  bool optDA;
};


/*---------------------------------------------------------------------------*/

/**
@brief The class used for sensor fusion in dynamic environments.

DynamicFuser is the class used for sensor fusion in dynamic environments. 
It inherits CCostFuser and uses most of its functions unchanged. This is 
somewhat unclean since some functions are copied unchanged to this class.
The reason for inheriting CCostFuser is that many other functions which take 
CCostFusers as inputs would also have to be rewritten if it didn't
*/

/*---------------------------------------------------------------------------*/

class CDynCostFuser : public CCostFuser {

private:
  //! This code is copied from CCostFuser.
  CCostFuser::STATUS fuseChangesTerrain(); 
  /*---------------------------------------------------------------------------*/
  /**
     @brief Used for filling in no-data areas of the map

     This function calls interpolateCell which checks whether a no-data area
     has enough neighboring cells with data. If it has, the cell will be filled
     in by interpolating the neighboring cell values. interpolateCell will not
     be called if the cell belongs to a moving obstacle since this would overwrite
     the speedlimit which have been given to the cell by fuseCellCost.
  */
  /*---------------------------------------------------------------------------*/
  CCostFuser::STATUS interpolateChanges();
  double MOspeed;

public:
/*---------------------------------------------------------------------------*/
/**
   @brief   Basic constructor.
   @param   movingObstMap       The map containing the moving obstacles.
   @param   layerNumMovingObst  The number of the layer containing the moving obstacles. 
   @param   outputMap           The map where the final speed limits get saved.           
   @param   layerNumFinalCost   The number of the layer where the speed limits get saved.
   @param   rddfMap             The RDDF map
   @param   layerNumRDDF        The number of the layer containing the RDDF information.
   @param   changesMapA         Map containing information of which cells that need to be updated 
   @param   changesMapB         Map containing information of which cells that need to be updated
   @param   options             containing info about max speed etc, see CCostFuser for more info.

This constructor calls the CCostFuser constructor and sets _layerNumMovingObst and _movingObstMap. 

*/
/*---------------------------------------------------------------------------*/
  CDynCostFuser(CMapPlus* movingObstMap, int layerNumMovingObst,
	       CMapPlus* outputMap, int layerNumFinalCost,
	       CMapPlus* rddfMap, int layerNumRDDF,
	       CMapPlus* changesMapA, CMapPlus* changesMapB,
	       CDynCostFuserOptions* options,
	       CCorridorPainterOptions* corridorOptions = NULL);
  
  CDynCostFuserOptions* _options;
  int _layerNumMovingObst;  //the layer ID for the layer containing the moving obstacle information.
  CMapPlus* _movingObstMap; //the map containing the moving obstacles
  int _layerNumDAChanges[2];//Contains the cells which should be updatated with dataAssociation. 

/*---------------------------------------------------------------------------*/
/** 
@brief   Called to create the final speed limits in all cells that need to be updated 
@param   newMapCenter   The new center of the map.

This code is copied from CCostFuser. The function fist changes which map to process,
then calls fuseChangesTerrain which does the terrain fusion. Finally fuseCellCost is 
called and the final speed limits for all deltas in mapChanges are calculated.

*/
/*---------------------------------------------------------------------------*/
  unsigned long long fuseChangesCost(NEcoord newMapCenter);

/*---------------------------------------------------------------------------*/
/**
   @brief   This is where the final speed limit of a cell is created.
   @param   point   The point where we want to create a speed limit.

This is the function which sets the final speed-limit for a cell. It fuses
the cell slightly differently depending on if data association is turned on 
or not. If data association is turned on, it does the following:
1. check if the cell belongs to a moving obstacle in the moving obstacle map.
2. if it does, and if the value from the terrain fusion is lower than a
specified threshold, the speed-limit is set using setMOspeed. The data
is also erased from the individual sensor maps.
3. if the cell belongs to a moving obstacle, but has been fused to a high 
speed limit, the moving obstacle info is probably wrong. The cell is changed
in the moving obstacle map and the value from the terrain fusion is used.

If data association is turned off, cells belonging to moving obstacles are 
set using setMOspeed, all other cells are set using the old costFuser 
method.

*/
/*---------------------------------------------------------------------------*/
  
  CCostFuser::STATUS fuseCellCost(NEcoord point);

/*---------------------------------------------------------------------------*/
/**
   @brief   Used for adding or removing moving obstacles
   @param   obstacle     The obstacle which will be added or removed
   @param   addObstacle  if true, the obstacle will be added to the map

If addObstacle is true, the function will add the obstacles current position
to the moving obstacle map. If addObstacle is false, the obstacle will instead 
be removed.

*/
/*---------------------------------------------------------------------------*/

  bool updateActiveMoving(movingObstacle obstacle, bool addObstacle);

/*---------------------------------------------------------------------------*/
/**
   @brief   Used for adding or removing new moving obstacles
   @param   obstacle    The obstacle which will be added or removed
   @param   addObstacle if true, the obstacle will be added to the map

If addObstacle is true, the function will add the obstacles current position
as well as all its old positions to the map. If addObstacle is false, the
obstacle will instead be removed. This Function is needed because the moving
obstacle detection algorithm cannot classify an obstacle as moving before it
has analysed a certain number of ladar scans. So when a new obstacle is detected,
this function adds all its old positions to the map to prevent it from causing
zero-velocity regions in the map which should be max-speed areas.
 

*/
/*---------------------------------------------------------------------------*/
  bool updateNewMoving(movingObstacle obstacle, bool addObstacle);

/*---------------------------------------------------------------------------*/
/**
   @brief   Corrects imprecise moving obstacle information
   @param   point   The cell where data assiciation should be done

This is a recursive function used for correcting imprecise moving obstacle
information. The recursion starts at point, if it belongs to a MO, the function 
checks if it has neighbors which are not members of the MO but have been 
fused to low speed limits. If so, the cells does probably belong to the MO.
The speed-limit is changed and a recursive call is made. 
*/
/*---------------------------------------------------------------------------*/
  CCostFuser::STATUS dataAssociationCell(NEcoord point);

/*---------------------------------------------------------------------------*/
/**
   @brief   Is used to call dataAssociationCell

This function takes all new deltas and calls dataAssociationCell with those 
deltas as arguments.
*/
/*---------------------------------------------------------------------------*/

  CCostFuser::STATUS dataAssociation();

/*---------------------------------------------------------------------------*/
/**
   @brief   Marks cells which should be changed in the output map
   @param   point   The cell which should be changed

This function adds point to a number of maps containing cells which should
be changed in the output map. The function is identical to the function in
CCostFuser except that point also gets added to the dataAssociation layer.

*/
/*---------------------------------------------------------------------------*/
  unsigned long long markChangesCost(NEcoord point);

/*---------------------------------------------------------------------------*/

/**
This function is a copy of markCangesTerrain in CCostFuser. 
*/
/*---------------------------------------------------------------------------*/
  unsigned long long markChangesTerrain(int layerIndex, NEcoord point);

/*---------------------------------------------------------------------------*/
/**
   @brief   Used to delete moving obstacles from the moving obstacle map.
   @param   point   A point which belongs to the moving obstacle

This is a recursive function used for removing moving obstacles from the 
moving obstacle map. When data association is done, the function has to 
be recursive since the data association recursion may add cells to the MO 
map which may be outside the rectangle given by the MO classification module.
The function checks whether point belongs to a moving obstacle, if so, 
the cell is set to false in the MO map and the cells neighbors are also checked.
*/
/*---------------------------------------------------------------------------*/
  int subtractMovingRec(NEcoord point);

/*---------------------------------------------------------------------------*/
/**
   @brief   Used to mark all cells in the map as changed.
*/
/*---------------------------------------------------------------------------*/
  CCostFuser::STATUS markChangesCostAll();

/*---------------------------------------------------------------------------*/
/**
   @brief   Returns the speed-limit a cell should be set to when a MO is removed.
   @param   point   The point a speed-limit should be calculated for.

Point gets set to the speed-limit of the cell we are currently at, if this cell
should be a low-speed area or an area with a speed-limit higher than the rddf
speed at point, the speed-limit will be set to the rddf speed. 
*/
/*---------------------------------------------------------------------------*/

  double setMOspeed(NEcoord point);


};

#endif
