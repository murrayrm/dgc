#include "Color.h"
#include <cassert>

namespace TrafSim
{

/* ******** CONSTRUCTORS ******** */

Color::Color(): r(1.0), g(1.0), b(1.0), a(1.0) {}

Color::Color(float _r, float _g, float _b, float _a):
		r(_r), g(_g), b(_b), a(_a) {}
		
/* ******** BINARY ARITHMETIC OPERATIONS ******** */

Color Color::operator+(Color const& rhs) const
{
	return Color(r + rhs.r, g + rhs.g, b + rhs.b, a + rhs.a);
}

Color Color::operator-(Color const& rhs) const
{
	return Color(r - rhs.r, g - rhs.g, b - rhs.b, a - rhs.a);
}

Color Color::operator*(Color const& rhs) const
{
	return Color(r * rhs.r, g * rhs.g, b * rhs.b, a * rhs.a);
}

Color Color::operator/(Color const& rhs) const
{
	assert(rhs.r != 0 && rhs.g != 0 && rhs.b != 0 && rhs.a != 0);
	
	return Color(r / rhs.r, g / rhs.g, b / rhs.b, a / rhs.a);
}

Color Color::operator*(float rhs) const
{
	return Color(r * rhs, g * rhs, b * rhs, a * rhs);
}

Color Color::operator/(float rhs) const
{
	assert(rhs != 0);
	
	return Color(r / rhs, g / rhs, b / rhs, a / rhs);
}

/* ******** UNARY ARITHMETIC OPERATIONS ******** */

Color& Color::operator+=(Color const& rhs)
{
	r += rhs.r;
	g += rhs.g;
	b += rhs.b;
	a += rhs.a;
	
	return *this;
}
	
Color& Color::operator-=(Color const& rhs)
{
	r -= rhs.r;
	g -= rhs.g;
	b -= rhs.b;
	a -= rhs.a;
	
	return *this;
}

Color& Color::operator*=(Color const& rhs)
{
	r *= rhs.r;
	g *= rhs.g;
	b *= rhs.b;
	a *= rhs.a;
	
	return *this;
}

Color& Color::operator/=(Color const& rhs)
{
	assert(rhs.r != 0 && rhs.g != 0 && rhs.b != 0 && rhs.a != 0);
	
	r /= rhs.r;
	g /= rhs.g;
	b /= rhs.b;
	a /= rhs.a;
	
	return *this;
}

Color& Color::operator*=(float rhs)
{
	r *= rhs;
	g *= rhs;
	b *= rhs;
	a *= rhs;
	
	return *this;
}

Color& Color::operator/=(float rhs)
{
	assert(rhs != 0);
	
	r /= rhs;
	g /= rhs;
	b /= rhs;
	a /= rhs;
	
	return *this;
}

/* ******** UTILITY OPERATIONS ******** */

extern "C" void glColor4f(float, float, float, float);

void glSetColor(Color const& value)
{
	glColor4f(value.r, value.g, value.b, value.a);
}

std::ostream& operator<<(std::ostream& os, Color const& c)
{
	return os << "Color(" << c.r << ", " << c.g << ", " << c.b << ", " << c.a << ")";
}
	
std::istream& operator>>(std::istream& is, Color& c)
{
	is.exceptions ( std::istream::eofbit | std::istream::failbit | std::istream::badbit );
	
	try
	{
		matchString(is, "Color");
		matchString(is, "(");
		is >> c.r;
		matchString(is, ",");
		is >> c.g;
		matchString(is, ",");
		is >> c.b;
		matchString(is, ",");
		is >> c.a;
		matchString(is, ")");
	}
	catch(ParseError& e)
	{
		e.info = "Failed to parse Color:\n" + e.info;
		throw;
	}
	catch(std::exception& e)
	{
		throw ParseError("Failed to parse Color:\n" + std::string(e.what()) );
	}
	
	return is;
}

}
