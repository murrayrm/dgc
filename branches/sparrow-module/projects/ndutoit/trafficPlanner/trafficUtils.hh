#ifndef TRAFFICUTILS
#define TRAFFICUTILS

#include <iostream>
#include <string>
#include "rndf.hh"
#include <vector>
#include <list>
#include <math.h>
#include "SegGoalsTalker.hh"
#include "Map.hh"
#include "AliceConstants.h"
#include "CMapPlus.hh"

using namespace std;
//using namespace Mapper;

#define EPS 0.05
#define BIGNUMBER 1000000000.0f 
#define PI 3.1416
#define LANEWIDTH 5.0f
#define ALICE_DESIRED_DECELERATION VEHICLE_MAX_DECEL
#define SEG_COMPLETE_DIST_TO_EXIT 1.0f
#define SEG_ALMOST_COMPLETE_DIST_TO_EXIT 10.0f
#define STOP_SPEED_SQR 1.0f
#define ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER (VEHICLE_WHEELBASE+DIST_FRONT_AXLE_TO_FRONT)
#define DIST_SWITCH_FROM_GPS_TO_SENSORS 10.0f
#define MAX_GPS_SHIFT LANEWIDTH 

enum TrafficRules
{ STAY_IN_LANE, PASSING, U_TURN, TRAFFIC_LINES, DRIVE_IN_CORRECT_DIRECTION, STAY_ON_ROAD }; 

struct DPlannerStatus
{
  DPlannerStatus()
  {
    //initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  void print()
  {
  }
  int goalID;
  bool status;
};

struct Constraint
{
	Constraint()
  {
    //initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
	double A1, A2, B, xrange1, xrange2, yrange1, yrange2;
};

struct ConstraintSet
{
	vector<Constraint> rightBoundaryConstraint, leftBoundaryConstraint, stopLineConstraint;
};
	
struct point 
{
	double x ;
    double y ;
};

void elevation2Cost(CMapPlus, CMapPlus&);

vector<TrafficRules> extractRules(SegmentType segmentType);

ConstraintSet laneFollowing(vector<Mapper::Location> laneLBCoord, vector<Mapper::Location> laneRBCoord, Mapper::LaneBoundary::Divider laneLBType, Mapper::LaneBoundary::Divider laneRBType, vector<TrafficRules> trafficRules, double dLimit, double dist2Exit, double x_A, double y_A); 

ConstraintSet intersectionNav(vector<Mapper::Location> laneLBCoord, vector<Mapper::Location> laneRBCoord, vector<TrafficRules> trafficRules, double dLimit, double x_A, double y_A);

//ConstraintSet laneFollowing_local(vector<Location> laneLBCoord, vector<Location> laneRBCoord, Divider laneLBType, Divider laneRBType, vector<TrafficRules> trafficRules, double dLimit, double dist2Exit, double x_A, double y_A); 

void lineFit(double x_toFit[],double y_toFit[],int sizeOfXToFit,double& slope,double& intersection);

///Calculates the closest existing point on the specified boundary - returns index of this point, and gives x,y location
int closestExistingBoundaryPt(vector<double> xB,vector<double> yB, double x, double y);

///Calculates the orthogonal projection of the specified point on the given boundary - returns the index where this point is to be inserted, and the x,y location 
int closestPtOnBoundary(vector<double> xB,vector<double> yB, double x, double y, int ind_CP, double& x_Pt, double& y_Pt);

/// Calculates the distance between (x1,y1) and (x2, y2)
double distanceXY(double x1, double x2, double y1, double y2);

void insertPointOnBoundary(vector<double>& xB,vector<double>& yB,double x, double y, int index);

void distanceAlongBoundary(vector<double>& xB, vector<double>& yB, int index_start, int& index_inserted, double distance);

vector<Constraint> boundaryConstraints(vector<double> xB1, vector<double> yB1, vector<double> xB2, vector<double> yB2, double x, double y, double distance, bool insert_flag);

//LocalMapObject spoofLocalMapData(int segmentID, int numLanes, int numTravelLanes, double laneWidth, int senseMin, int senseMax, int xSpacing);  

//SegGoals spoofSegmentGoal();

//  bool convRDDF( vector<Constraint> line1, vector<Constraint> line2, SegGoals currentSegGoals, double x_A, double y_A, double laneWidth, RDDF* newRddf);
bool convRDDF( vector<Constraint> line1, vector<Constraint> line2, SegGoals currentSegGoals, double x_A, double y_A, double laneWidth, vector<double>& rddfx, vector<double>& rddfy, vector<double>& rddfr);

point closept( double lnseg[4], double pt[2] );

point midpt( double pt1[2], double pt2[2] );

double radius( double mpt[2], double pt1[2], double pt2[2] );

void generateMapFromRNDF(RNDF*, Mapper::Map&);


#endif // TRAFFICUTILS
