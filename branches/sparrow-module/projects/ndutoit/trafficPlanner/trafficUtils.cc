#include "trafficUtils.hh"

void elevation2Cost(CMapPlus elevationMap, CMapPlus& costMap)
{
  costMap = elevationMap;
}

ConstraintSet laneFollowing(vector<Mapper::Location> laneLBCoord, vector<Mapper::Location> laneRBCoord, Mapper::LaneBoundary::Divider laneLBType, Mapper::LaneBoundary::Divider laneRBType, vector<TrafficRules> trafficRules, double dLimit, double dist2Exit, double x_A, double y_A) 
{
  ConstraintSet constraintSet;
  vector<double> x_RB;
  vector<double> y_RB;
  vector<double> x_LB;
  vector<double> y_LB;
  for (unsigned ii = 0; ii < laneLBCoord.size(); ii++)
    {
      x_LB.push_back(laneLBCoord[ii].x);
      y_LB.push_back(laneLBCoord[ii].y);
    }
  for (unsigned ii = 0; ii < laneRBCoord.size(); ii++)
    {
      x_RB.push_back(laneRBCoord[ii].x);
      y_RB.push_back(laneRBCoord[ii].y);
    }
  //cout << "reading in data..." << endl;
  //for (unsigned ii=0;ii<x_LB.size();ii++)
  //{
  //printf("LB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_LB[ii], y_LB[ii]);
  //}
  // for (unsigned ii=0;ii<x_RB.size();ii++)
  //{
  //printf("RB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_RB[ii], y_RB[ii]);
  //}

  // find points on boundary closest to me
  int ind_RB_CP, ind_LB_CP, ind_RB_Ins, ind_LB_Ins, index;
  double x_temp, y_temp;
  ind_RB_CP = closestExistingBoundaryPt(x_RB, y_RB, x_A, y_A);
  ind_RB_Ins = closestPtOnBoundary(x_RB, y_RB, x_A, y_A, ind_RB_CP, x_temp, y_temp);
  // insert these points into boundary
  insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RB_Ins);
  // same for LB
  ind_LB_CP = closestExistingBoundaryPt(x_LB, y_LB, x_A, y_A);
  ind_LB_Ins = closestPtOnBoundary(x_LB, y_LB, x_A, y_A, ind_LB_CP, x_temp, y_temp);
  insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LB_Ins);
  //cout << "after inserting closest boundary pt..." << endl;
  //for (unsigned ii=0;ii<x_LB.size();ii++)
  // {
  //printf("LB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_LB[ii], y_LB[ii]);
  //}
  //for (unsigned ii=0;ii<x_RB.size();ii++)
  //{
  //printf("RB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_RB[ii], y_RB[ii]);
  //}

  // insert a point a distance away from the point if we are far
  // enough away from the end of the segment.
  if (dLimit < dist2Exit)
    {
      distanceAlongBoundary(x_RB, y_RB, ind_RB_Ins, index, dLimit);
      distanceAlongBoundary(x_LB, y_LB, ind_LB_Ins, index, dLimit);    	
    }

  //cout << "after inserting a point at distance dLimit..." << endl;
  //for (unsigned ii=0;ii<x_LB.size();ii++)
  //{
  //printf("LB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_LB[ii], y_LB[ii]);
  //}
  //for (unsigned ii=0;ii<x_RB.size();ii++)
  //{
  //printf("RB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_RB[ii], y_RB[ii]);
  //}

  bool insert_flag = true;
  bool stay_on_road = false;
  bool traffic_lines = false;	
  // lane right boundary
  switch (laneRBType)
  {
    case ROAD_EDGE:
      for (unsigned ii = 0; ii<trafficRules.size(); ii++)
      {
        if (trafficRules[ii] == STAY_ON_ROAD)
        {
          stay_on_road = true;
          break;
        }
      }
      if (stay_on_road)
      {
        constraintSet.rightBoundaryConstraint = boundaryConstraints(x_RB, y_RB, x_LB, y_LB, x_A, y_A, dLimit, insert_flag);
      }
      break;
    case BROKEN_WHITE:
    case SOLID_WHITE:
    case DOUBLE_YELLOW:
      for (unsigned ii = 0; ii<trafficRules.size(); ii++)
      {
        if (trafficRules[ii] == TRAFFIC_LINES)
        {
          traffic_lines = true;
          break;
        }
      }        
      if (traffic_lines)
      {
        constraintSet.rightBoundaryConstraint = boundaryConstraints(x_RB, y_RB, x_LB, y_LB, x_A, y_A, dLimit, insert_flag);
      }
      break;  
    default:
      cerr << "ERROR -> Wrong Divider type" << endl;
  } // switch
        
  // lane left boundary
  stay_on_road = false;
  traffic_lines = false;
  switch (laneRBType)
    {
    case ROAD_EDGE:
      for (unsigned ii = 0; ii<trafficRules.size(); ii++)
	{
	  if (trafficRules[ii] == STAY_ON_ROAD)
	    {
	      stay_on_road = true;
	      break;
	    }
	}
      if (stay_on_road)
	{
	  constraintSet.leftBoundaryConstraint = boundaryConstraints(x_LB, y_LB, x_RB, y_RB, x_A, y_A, dLimit, insert_flag);
	}
      break;
    case BROKEN_WHITE:
    case SOLID_WHITE:
    case DOUBLE_YELLOW:
      for (unsigned ii = 0; ii<trafficRules.size(); ii++)
	{
	  if (trafficRules[ii] == TRAFFIC_LINES)
	    {
	      traffic_lines = true;
	      break;
	    }
	}
      if (traffic_lines)
	{
	  constraintSet.leftBoundaryConstraint = boundaryConstraints(x_LB, y_LB, x_RB, y_RB, x_A, y_A, dLimit, insert_flag);
	}
                
      break;
    default:
      cerr << "ERROR -> Wrong Divider type" << endl;
    } // switch

  return constraintSet;
		
}


ConstraintSet intersectionNav(vector<Mapper::Location> laneLBCoord, vector<Mapper::Location> laneRBCoord, vector<TrafficRules> trafficRules, double dLimit, double x_A, double y_A) 
{
  ConstraintSet constraintSet;
  vector<double> x_RB, y_RB, x_LB, y_LB;
  for (unsigned ii = 0; ii < laneLBCoord.size(); ii++)
    {
      x_LB.push_back(laneLBCoord[ii].x);
      y_LB.push_back(laneLBCoord[ii].y);
    }
  for (unsigned ii = 0; ii < laneRBCoord.size(); ii++)
    {
      x_RB.push_back(laneRBCoord[ii].x);
      y_RB.push_back(laneRBCoord[ii].y);
    }

  // for (unsigned ii=0;ii<x_LB.size();ii++)
    //{
      //  printf("LB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_LB[ii], y_LB[ii]);
      //}
  //for (unsigned ii=0;ii<x_RB.size();ii++)
    //{
      // printf("RB (x,y) %d = (%3.6f,%3.6f)\n", ii, x_RB[ii], y_RB[ii]);
      // }

  bool insert_flag = false;
  constraintSet.rightBoundaryConstraint = boundaryConstraints(x_RB, y_RB, x_LB, y_LB, x_RB[0], y_RB[0], dLimit, insert_flag);
  constraintSet.leftBoundaryConstraint = boundaryConstraints(x_LB, y_LB, x_RB, y_RB, x_LB[0], y_LB[0], dLimit, insert_flag);
  return constraintSet;
}

vector<TrafficRules> extractRules(SegmentType segmentType)
{
  vector<TrafficRules> trafficRules;
  switch (segmentType)
    {
      /* {STAY_IN_LANE, PASSING, U_TURN, TRAFFIC_LINES, DRIVE_IN_CORRECT_DIRECTION, STAY_ON_ROAD}*/
    case ROAD_SEGMENT:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;
      // NEED TO GO THROUGH THESE AND DEFINE LATER  
    case PARKING_ZONE:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    case INTERSECTION:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    case PREZONE:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    case UTURN:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    case PAUSE:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    case END_OF_MISSION:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
      break;  
    default:
      trafficRules.push_back(STAY_IN_LANE);
      trafficRules.push_back(PASSING); 
      trafficRules.push_back(TRAFFIC_LINES);
      trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
      trafficRules.push_back(STAY_ON_ROAD);
    }
	
  // print the traffic rules to the screen to check
  //for (unsigned ii=0;ii<trafficRules.size();ii++)
  //{
  //	cout << "Traffic Rule " << ii << " is " << trafficRules[ii]<<endl;
  //}
	
  return trafficRules;
}

int closestExistingBoundaryPt(vector<double> xB,vector<double> yB, double x, double y)
{
  // function that finds index of point on boundary (xB,yB) that is closest 
  // to specified point x,y
	int ind_CP;
  int lengthxB = xB.size();
  double distance = BIGNUMBER;
  
  // closest point on Boundary
  for (int ii = 0; ii<lengthxB; ii++)
  {
    double distance_temp = sqrt(pow(x-xB[ii],2)+pow(y-yB[ii],2));
    if (distance_temp<distance)
    {
      distance=distance_temp;
      ind_CP=ii;
    }
  }
  return ind_CP;
}

int closestPtOnBoundary(vector<double> xB,vector<double> yB, double x, double y, int ind_CP, double& x_Pt, double& y_Pt)
{
  // function that finds index of point on boundary (xB,yB) that is closest 
  // to specified point x,y and then calculates the actual closest pt on the boundary
  // This function receives in the argument the index of the closest existing boundary point (ind_CP)

  // Do a least-squares line fit to number of points around this point
  // for now use 3 points
  int ind_Insert;
  double slope, intersection;	
  double x_toFit[3], y_toFit[3];
  int sizeOfXToFit;
  int lengthxB = (int)xB.size();
  if (ind_CP > 0 && ind_CP+1 < lengthxB)
    {
      sizeOfXToFit = 3;
      x_toFit[0] = xB[ind_CP-1];
      x_toFit[1] = xB[ind_CP];
      x_toFit[2] = xB[ind_CP+1];
      y_toFit[0] = yB[ind_CP-1];
      y_toFit[1] = yB[ind_CP];
      y_toFit[2] = yB[ind_CP+1];
      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
    }
  else if (ind_CP == 0 && ind_CP+1 < lengthxB)
    {
      sizeOfXToFit = 2;
      x_toFit[0] = xB[ind_CP];
      x_toFit[1] = xB[ind_CP+1];
      y_toFit[0] = yB[ind_CP];
      y_toFit[1] = yB[ind_CP+1];
      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
    }
  else if (ind_CP > 0 && ind_CP+1 == lengthxB)
    {
      sizeOfXToFit = 2;
      x_toFit[0] = xB[ind_CP-1];
      x_toFit[1] = xB[ind_CP];
      y_toFit[0] = yB[ind_CP-1];
      y_toFit[1] = yB[ind_CP];
      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
    }
  else
    {
      x_Pt = xB[ind_CP];
      y_Pt = yB[ind_CP];
      ind_Insert = ind_CP;
      return ind_Insert;
    }
  // then find best approximation to the point on the line
  // transform into coordinate frame at begin pt of line x-axis along line
  //double theta = atan(slope);
  double theta = atan2(y_toFit[sizeOfXToFit-1]-y_toFit[0], x_toFit[sizeOfXToFit-1]-x_toFit[0]); 
  double R11 = cos(theta);
  double R12 = -sin(theta);
  double R21 = sin(theta);
  double R22 = cos(theta);
  double d_vec1 = xB[ind_CP];
  double d_vec2 = yB[ind_CP];
  double posXYPt_local1 = x;
  double posXYPt_local2 = y;    
  double posXYPt_line1 = R11*(posXYPt_local1-d_vec1)+R21*(posXYPt_local2-d_vec2);
  // double posXYPt_line2 = R12*(posXYPt_local1-d_vec1)+R22*(posXYPt_local2-d_vec2); 
  // find projection of point onto x-axis, which is aligned with line
  double posBDPt_line1 = posXYPt_line1;
  double posBDPt_line2 = 0;
  //    cout << "posBDPt_line1 = " << posBDPt_line1 << endl; 
  // transform new point back to local frame
  double posBDPt_local1 = d_vec1 + R11*posBDPt_line1 + R12*posBDPt_line2;
  double posBDPt_local2 = d_vec2 + R21*posBDPt_line1 + R22*posBDPt_line2;
  x_Pt = posBDPt_local1;
  y_Pt = posBDPt_local2;
  // if the point lies on the line (pos x-axis), then give index of next point
  if (posBDPt_line1>0)
    ind_Insert = ind_CP+1;
  else
    { 
      ind_Insert = ind_CP;
      //ind_CP = ind_CP+1;
    }
  return ind_Insert;
}

double distanceXY(double x1, double x2, double y1, double y2)
{
  double dist = sqrt(pow(x1-x2,2)+pow(y1-y2,2));
  return dist;
}

void lineFit(double x_toFit[],double y_toFit[],int sizeOfXToFit,double& slope,double& intersection)
{
  // do a least squares fit - assumed only error in y-direction
  double sumX=0, sumX2=0, sumY=0, sumXY=0;
	
  for (int ii=0;ii<sizeOfXToFit;ii++)
    {
      sumX += x_toFit[ii];
      sumY += y_toFit[ii];
      sumX2 += x_toFit[ii]*x_toFit[ii];
      sumXY += x_toFit[ii]*y_toFit[ii];		
    }
  intersection = (sumY*sumX2-sumX*sumXY)/(sizeOfXToFit*sumX2-sumX*sumX);
  slope = (sizeOfXToFit*sumXY-sumX*sumY)/(sizeOfXToFit*sumX2-sumX*sumX);
}

void insertPointOnBoundary(vector<double>& xB,vector<double>& yB,double x, double y, int index)
{
  // check if the points are too close to each other
  if (sqrt(pow(xB[index]-x,2)+pow(yB[index]-y,2)) > 2*EPS)
    {
      xB.insert(xB.begin()+index,x);
      yB.insert(yB.begin()+index,y);
    }
}

void distanceAlongBoundary(vector<double>& xB, vector<double>& yB, int index_start, int& index_inserted, double distance)
{
	
	double dAlongBoundary = 0;
	int lengthxB = (int)xB.size();
	if(distance>0)
	{
	  for (int ii=index_start; ii+1<lengthxB; ii++)
	  {
	  	dAlongBoundary = dAlongBoundary + sqrt(pow(xB[ii]-xB[ii+1],2)+pow(yB[ii]-yB[ii+1],2));
	    
      if (dAlongBoundary>distance)
			{
	  		// find a point on the line segment at distance
			  double slope, intersection;	        
			  double x_toFit[3], y_toFit[3];
			  int sizeOfXToFit;
			  if (ii > 0)
			    {
			      sizeOfXToFit = 3;
			      x_toFit[0] = xB[ii-1];
			      x_toFit[1] = xB[ii];
			      x_toFit[2] = xB[ii+1];
			      y_toFit[0] = yB[ii-1];
			      y_toFit[1] = yB[ii];
			      y_toFit[2] = yB[ii+1];	            
			      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
			    }
			  else if (ii == 0)
			    {
			      sizeOfXToFit = 2;
			      x_toFit[0] = xB[ii];
			      x_toFit[1] = xB[ii+1];
			      y_toFit[0] = yB[ii];
			      y_toFit[1] = yB[ii+1];	            
			      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
			    }
			  // in coordinate frame centered at (xB(ii),yB(ii)), with x-axis aligned with the line, specify
			  // distance along x-axis and then transform into local frame
			  //double theta = atan(slope);
			  double theta = atan2(y_toFit[sizeOfXToFit-1]-y_toFit[0], x_toFit[sizeOfXToFit-1]-x_toFit[0]);
			  double R11 = cos(theta);
			  double R12 = -sin(theta);
			  double R21 = sin(theta);
			  double R22 = cos(theta);
			  double d_vec1 = xB[ii];
			  double d_vec2 = yB[ii];
			  double posXY_line1 = sqrt(pow(xB[ii]-xB[ii+1],2)+pow(yB[ii]-yB[ii+1],2))-(dAlongBoundary-distance);
			  double posXY_line2 = 0;    
			  // transform new point back to local frame
			  double posXY_local1 = d_vec1 + R11*posXY_line1 + R12*posXY_line2;
			  double posXY_local2 = d_vec2 + R21*posXY_line1 + R22*posXY_line2;
			  if (sqrt(pow(xB[ii]-posXY_local1,2)+pow(yB[ii]-posXY_local2,2)) > 2*EPS)
			  {
			    index_inserted = ii+1;
			    insertPointOnBoundary(xB, yB, posXY_local1, posXY_local2, index_inserted);
			    break;
			  }
			}
		}
	}
	else 
	{
	  for (int ii=index_start; ii==1; ii--)
	  {
	  	dAlongBoundary = dAlongBoundary + sqrt(pow(xB[ii-1]-xB[ii],2)+pow(yB[ii-1]-yB[ii],2));
	    
      if (dAlongBoundary>fabs(distance))
			{
	  		// find a point on the line segment at distance
			  double slope, intersection;	        
			  double x_toFit[3], y_toFit[3];
			  int sizeOfXToFit;
			  if (ii+1 < lengthxB)
			    {
			      sizeOfXToFit = 3;
			      x_toFit[0] = xB[ii+1];
			      x_toFit[1] = xB[ii];
			      x_toFit[2] = xB[ii-1];
			      y_toFit[0] = yB[ii+1];
			      y_toFit[1] = yB[ii];
			      y_toFit[2] = yB[ii-1];	            
			      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
			    }
			  else if (ii+1 == lengthxB)
			    {
			      sizeOfXToFit = 2;
			      x_toFit[0] = xB[ii];
			      x_toFit[1] = xB[ii-1];
			      y_toFit[0] = yB[ii];
			      y_toFit[1] = yB[ii-1];	            
			      lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
			    }
			  // in coordinate frame centered at (xB(ii),yB(ii)), with x-axis aligned with the line, specify
			  // distance along x-axis and then transform into local frame
			  //double theta = atan(slope);
			  double theta = atan2(y_toFit[sizeOfXToFit-1]-y_toFit[0], x_toFit[sizeOfXToFit-1]-x_toFit[0]);
			  double R11 = cos(theta);
			  double R12 = -sin(theta);
			  double R21 = sin(theta);
			  double R22 = cos(theta);
			  double d_vec1 = xB[ii];
			  double d_vec2 = yB[ii];
			  double posXY_line1 = sqrt(pow(xB[ii]-xB[ii-1],2)+pow(yB[ii]-yB[ii-1],2))-(dAlongBoundary-fabs(distance));
			  double posXY_line2 = 0;    
			  // transform new point back to local frame
			  double posXY_local1 = d_vec1 + R11*posXY_line1 + R12*posXY_line2;
			  double posXY_local2 = d_vec2 + R21*posXY_line1 + R22*posXY_line2;
			  if (sqrt(pow(xB[ii]-posXY_local1,2)+pow(yB[ii]-posXY_local2,2)) > 2*EPS)
			  {
			    index_inserted = ii-1;
			    insertPointOnBoundary(xB, yB, posXY_local1, posXY_local2, index_inserted);
			    break;
			  }
			  else
			  	index_inserted = ii;
			}
		}
	}
}

vector<Constraint> boundaryConstraints(vector<double> xB1, vector<double> yB1, vector<double> xB2, vector<double> yB2, double x, double y, double distance, bool insert_flag)
{
  vector<Constraint> constraints;
  if (xB1.size() <= 1)
  {
    cerr << "Not enough points in boundary (in boundaryConstraints function)." << endl; 
    return constraints;
  }
  // closest point on B1
  double x_temp_B1, y_temp_B1;
  double x_temp_B2, y_temp_B2;	
  int ind_Insert_B1, ind_Insert_B2, ind_CP_B1, ind_CP_B2;
  if (xB1.size()>1)
  {
    ind_CP_B1 = closestExistingBoundaryPt(xB1, yB1, x, y);
    ind_Insert_B1 = closestPtOnBoundary(xB1, yB1, x, y, ind_CP_B1, x_temp_B1, y_temp_B1);
  }
  else
  {
    ind_Insert_B1 = 0;
    ind_CP_B1 = 0;
    x_temp_B1 = xB1[ind_CP_B1];
    y_temp_B1 = yB1[ind_CP_B1];
  }
  // closest point on B2
  if (xB2.size()>1)
  {
    ind_CP_B2 = closestExistingBoundaryPt(xB2, yB2, x, y);
    ind_Insert_B2 = closestPtOnBoundary(xB2, yB2, x, y, ind_CP_B2, x_temp_B2, y_temp_B2);
  }
  else
  {
    ind_Insert_B2 = 0;
    ind_CP_B2 = 0;
    x_temp_B2 = xB2[ind_CP_B2];
    y_temp_B2 = yB2[ind_CP_B2];
  }
  // flag that says whether or not I need to insert these points into the
  // boundary so that these points are at position index
  int ind_B1, ind_B2;
  if (insert_flag)
  {
    insertPointOnBoundary(xB1,yB1,x_temp_B1,y_temp_B1,ind_Insert_B1);
    insertPointOnBoundary(xB2,yB2,x_temp_B2,y_temp_B2,ind_Insert_B2);
    ind_B1 = ind_Insert_B1;
    ind_B2 = ind_Insert_B2;
  }
  else
  {
    ind_B1 = ind_CP_B1;
    ind_B2 = ind_CP_B2;
  }

  // Generate constraints for the all the points on the boudary in
  // the next X meters along the boundary
  double dAlongBoundary = 0;
  double slope, intersection;
  if (ind_B1+1 == (int)xB1.size())
  {
    ind_B1--;
  }
  for (int ii = ind_B1; ii+1<(int)xB1.size(); ii++)
  {
    Constraint constraint;
    // check that denominator is ~= 0
    if (xB1[ii+1]-xB1[ii] != 0)
	  {
      slope=(yB1[ii+1]-yB1[ii])/(xB1[ii+1]-xB1[ii]);
    }
    else
    {
      slope = ((yB1[ii+1]-yB1[ii])/fabs(yB1[ii+1]-yB1[ii]))*BIGNUMBER;
    }
      
    intersection=yB1[ii]-slope*xB1[ii];
    // Check which side the closest point on the left boundary is
    // for the line segment on the right boundary closest to you
    double value = yB2[ind_B2] - slope*xB2[ind_B2] - intersection;
    if (value > 0)
    {
      // I also want to be on this side -> above line
      // segment, but need to specify as Ax-B<=0
      constraint.A1 = slope;
      constraint.A2 = -1; 
      constraint.B = -intersection;
    }
    else if (value < 0)
    {
      constraint.A1 = -slope;
      constraint.A2 = 1;
      constraint.B = intersection;
    }
    else
    {
      cerr<<"ERROR in boundary constraint generation function "<<endl;
    }
    constraint.xrange1 = xB1[ii];
    constraint.xrange2 = xB1[ii+1];
    constraint.yrange1 = yB1[ii];
    constraint.yrange2 = yB1[ii+1];	
    constraints.push_back(constraint); 
    dAlongBoundary += sqrt(pow(xB1[ii]-xB1[ii+1],2)+pow(yB1[ii]-yB1[ii+1],2));
    if (dAlongBoundary - distance > 0)
    {
      return constraints;
    }
  }
  return constraints;
}

//bool convRDDF( vector<Constraint> line1, vector<Constraint> line2, SegGoals currentSegGoals, double x_A, double y_A, double laneWidth, RDDF* newRddf) 
bool convRDDF( vector<Constraint> line1, vector<Constraint> line2, SegGoals currentSegGoals, double x_A, double y_A, double laneWidth, vector<double>& rddfx, vector<double>& rddfy, vector<double>& rddfr)
{  
  int i, j, k, l1, l2 ;

  //%%%%%%%%%%%%%%%%%%%%%%%%
  //% FIND CLOSEST SEGMENTS
  //%%%%%%%%%%%%%%%%%%%%%%%%
  bool error = false ;
  //double apos[] = {x_A, y_A};
  int numline1seg = line1.size();
  //cout << "numline1seg = " << numline1seg << endl;
  int numline2seg = line2.size();
  //cout << "numline2seg = " << numline2seg << endl;
  double l1p[numline1seg+1][2], l2p[numline2seg+1][2] ;
  for( i = 0 ; i < numline1seg ; i++ ) {
    l1p[i][0] = line1[i].xrange1 ;
    l1p[i][1] = line1[i].yrange1 ;
    //printf("line1[%d] (x1,y1) = (%3.6f,%3.6f)\n", i, l1p[i][0], l1p[i][1]);
  }
  l1p[numline1seg][0] = line1[numline1seg-1].xrange2 ;
  l1p[numline1seg][1] = line1[numline1seg-1].yrange2 ;
  //printf("line1[%d] (x2,y2) = (%3.6f,%3.6f)\n", i, l1p[numline1seg][0], l1p[numline1seg][1]);
  
  int numline1pts = numline1seg + 1 ;

  for( i = 0; i < numline2seg ; i++ ) {
    l2p[i][0] = line2[i].xrange1 ;
    l2p[i][1] = line2[i].yrange1 ;
    //printf("line2[%d] (x1,y1) = (%3.6f,%3.6f)\n", i, l2p[i][0], l2p[i][1]);
  }
  l2p[numline2seg][0] = line2[numline2seg-1].xrange2 ;
  l2p[numline2seg][1] = line2[numline2seg-1].yrange2 ;
  //printf("line2[%d] (x1,y1) = (%3.6f,%3.6f)\n", numline2seg, l2p[numline2seg][0], l2p[numline2seg][1]);
  int numline2pts = numline2seg + 1 ;
  //cout << "numline2pts = " << numline2pts << endl;
  // Check that end points of two lines are not the same (full lane obstacle)
  if( (l1p[numline1seg][0] == l2p[numline2seg][0]) && (l1p[numline1seg][1] == l2p[numline2seg][1]) )
    {
      error = true ;
      cerr << "ERROR convRDDF: (l1p[numline1seg][0] == l2p[numline2seg][0]) && (l1p[numline1seg][1] == l2p[numline2seg][1])" << endl;
    }

  double mpt[2] = {0, 0} ;
  double r = 0 ;

  // Line 1
  //cout << "line 1" << endl;
  double min1, min2, dist, xdiff, ydiff ;
  int min1num, min2num;
  bool skip = false ;
  double a[2], b[2], maga, magb, adotb, th, check[4], check1[4], check2[4] ;
  point cpt_, cpt1_, cpt2_, mpt_ ;
  double cpt[2], pnt[2] ;
  double rddfln1[numline1pts][3], rddfln2[numline2pts][3] ;
  for( i = 0 ; i < numline1pts ; i++ ) {
    min1 = BIGNUMBER ;
    min2 = BIGNUMBER ;
    min1num = 0 ;
    min2num = 0 ;
    for( j = 0; j < numline2pts; j++ ) {
      xdiff = l1p[i][0] - l2p[j][0] ;
      ydiff = l1p[i][1] - l2p[j][1] ;
      dist = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
      if( dist < min1 ) {
	if( dist < min2 ) {
	  min1 = min2 ;
	  min1num = min2num ;
	  min2 = dist ;
	  min2num = j ;
	} else {
	  min1 = dist ;
	  min1num = j ;
	}
      }
    }
    skip = false ;
    if( (i == 0) || (i == (numline1pts-1))) {
      if( i == 0 ) {
	a[0] = l1p[0][0] - l2p[0][0] ;
	a[1] = l1p[0][1] - l2p[0][1] ;
	b[0] = l2p[1][0] - l2p[0][0] ;
	b[1] = l2p[1][1] - l2p[0][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      } else {
	a[0] = l1p[numline1seg][0] - l2p[numline2seg][0] ;
	a[1] = l1p[numline1seg][1] - l2p[numline2seg][1] ;
	b[0] = l2p[numline2seg-1][0] - l2p[numline2seg][0] ;
	b[1] = l2p[numline2seg-1][1] - l2p[numline2seg][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      }
      th = acos( adotb/(maga * magb) ) ;
      if( th > (PI / 2 + EPS) ) 
	skip = true ;
    }
    if( skip ) {
      mpt[0] = 0 ;
      mpt[1] = 0 ;
      r = 0 ;
    } else {
      pnt[0] = l1p[i][0] ;
      pnt[1] = l1p[i][1] ;
      if( (min1num + 1 == min2num) || (min2num + 1 == min1num)) {
	//fprintf( stderr, "min1num = %i, minnum2 = %i\n" , min1num, min2num) ;
	if( min1num < min2num ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check[k] = l2p[min1num][k] ;
	    else
	      check[k] = l2p[min1num+1][k-2];
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check[k] = l2p[min2num][k] ;
	    else
	      check[k] = l2p[min2num+1][k-2] ;
	  }
	}
	cpt_ = closept( check, pnt ) ;
	cpt[0] = cpt_.x ;
	cpt[1] = cpt_.y ;
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      }  else if( (min1num + 2 == min2num) || (min2num + 2 == min1num)) {
	if( min1num < min2num ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if ( k < 2 ) {
	      check1[k] = l2p[min1num][k] ;
	      check2[k] = l2p[min2num-1][k] ;
	    } else {
	      check1[k] = l2p[min1num+1][k-2] ;
	      check2[k] = l2p[min2num][k-2] ;
	    }
	  }
	} else { 
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) {
	      check1[k] = l2p[min2num][k] ;
	      check2[k] = l2p[min1num-1][k] ;
	    } else {
	      check1[k] = l2p[min2num+1][k-2] ;
	      check2[k] = l2p[min1num][k-2] ;
	    }
	  }
	}
	cpt1_ = closept(check1, pnt) ;
	cpt2_ = closept(check2, pnt) ;
	double xdiff1 = cpt1_.x - pnt[0] ;
	double ydiff1 = cpt1_.y - pnt[1] ;
	double xdiff2 = cpt2_.x - pnt[0];
	double ydiff2 = cpt2_.y - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ) {
	  cpt[0] = cpt1_.x ;
	  cpt[1] = cpt1_.y ;
	} else {
	  cpt[0] = cpt2_.x ;
	  cpt[1] = cpt2_.y ;
	}
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      } else {
	if( min1num < (numline2pts-1) ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check1[k] = l2p[min1num][k] ;
	    else
	      check1[k] = l2p[min1num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check1[k] = l2p[min1num-1][k] ;
	    else 
	      check1[k] = l2p[min1num][k-2] ;
	  }
	}
	if( min2num < (numline2pts-1) ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check2[k] = l2p[min2num][k] ;
	    else
	      check2[k] = l2p[min2num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check2[k] = l2p[min2num-1][k] ;
	    else 
	      check2[k] = l2p[min2num][k-2] ;
	  }
	}
	cpt1_ = closept(check1, pnt) ;
	cpt2_ = closept(check2, pnt) ;
	double xdiff1 = cpt1_.x - pnt[0] ;
	double ydiff1 = cpt1_.y - pnt[1] ;
	double xdiff2 = cpt2_.x - pnt[0];
	double ydiff2 = cpt2_.y - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ){
	  cpt[0] = cpt1_.x ;
	  cpt[1] = cpt1_.y ;
	} else {
	  cpt[0] = cpt2_.x ;
	  cpt[1] = cpt2_.y ;
	}
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      }
    }
    if( r > laneWidth / 2 )
      r = laneWidth / 2 ;

    rddfln1[i][0] = mpt[0] ;
    rddfln1[i][1] = mpt[1] ;
    rddfln1[i][2] = r ;
    //printf("rddfln1[%d] (x,y) = (%3.6f,%3.6f)\n", i, rddfln1[i][0], rddfln1[i][1]);
  }

  // Line 2
  //cout << "line 2" << endl;
  for( i = 0 ; i < numline2pts ; i++ ) {
    min1 = BIGNUMBER ;
    min2 = BIGNUMBER ;
    min1num = 0 ;
    min2num = 0 ;
    for( j = 0 ; j < numline1pts ; j++ ) {
      xdiff = l2p[i][0] - l1p[j][0] ;
      ydiff = l2p[i][1] - l1p[j][1] ;
      dist = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
      if( dist < min1 ) {
	if( dist < min2 ) {
	  min1 = min2 ;
	  min1num = min2num ;
	  min2 = dist ;
	  min2num = j ;
	} else {
	  min1 = dist ;
	  min1num = j ;
	}
      }
    }
    skip = false ;
    if( (i == 0) || (i == numline2seg) ) {
      if( i == 0 ) { 
	a[0] = l2p[0][0] - l1p[0][0] ;
	a[1] = l2p[0][1] - l1p[0][1] ;
	b[0] = l1p[1][0] - l1p[0][0] ;
	b[1] = l1p[1][1] - l1p[0][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      } else {
	a[0] = l2p[numline2pts-1][0] - l1p[numline1pts-1][0] ;
	a[1] = l2p[numline2pts-1][1] - l1p[numline1pts-1][1] ;
	b[0] = l1p[numline1pts-2][0] - l1p[numline1pts-1][0] ;
	b[1] = l1p[numline1pts-2][1] - l1p[numline1pts-1][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      }
      th = acos( adotb/(maga * magb) ) ;
      if( th > (PI / 2 + EPS) ) 
	skip = true ;

    }
    if( skip ) {
      mpt[0] = 0 ;
      mpt[1] = 0 ;
      r = 0 ;
    } else {
      pnt[0] = l2p[i][0] ;
      pnt[1] = l2p[i][1] ;
      //    fprintf( stderr, "min1 = %i, min2 = %i\n", min1num, min2num ) ;
      if( (min1num + 1 == min2num) || (min2num + 1 == min1num) ) {
	if( min1num < min2num ) {
	  for( k = 0 ; k < 4 ; k++ ){
	    if( k < 2 )
	      check[k] = l1p[min1num][k] ;
	    else
	      check[k] = l1p[min1num+1][k-2];
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check[k] = l1p[min2num][k] ;
	    else
	      check[k] = l1p[min2num+1][k-2];
	  }
	}
	cpt_ = closept( check, pnt ) ;
	cpt[0] = cpt_.x ;
	cpt[1] = cpt_.y ;
	//       fprintf( stderr, "1: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      } else if( (min1num + 2 == min2num) || (min2num + 2 == min1num)) {
	if( min1num < min2num) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if ( k < 2 ) {
	      check1[k] = l1p[min1num][k] ;
	      check2[k] = l1p[min2num-1][k] ;
	    } else {
	      check1[k] = l1p[min1num+1][k-2] ;
	      check2[k] = l1p[min2num][k-2] ;
	    }
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if ( k < 2 ) {
	      check1[k] = l1p[min2num][k] ;
	      check2[k] = l1p[min1num-1][k] ;
	    } else {
	      check1[k] = l1p[min2num+1][k-2] ;
	      check2[k] = l1p[min1num][k-2] ;
	    }
	  }
	}
	cpt1_ = closept(check1, pnt) ;
	cpt2_ = closept(check2, pnt) ;
	double xdiff1 = cpt1_.x - pnt[0] ;
	double ydiff1 = cpt1_.y - pnt[1] ;
	double xdiff2 = cpt2_.x - pnt[0];
	double ydiff2 = cpt2_.y - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ) {
	  cpt[0] = cpt1_.x ;
	  cpt[1] = cpt1_.y ;
	} else {
	  cpt[0] = cpt2_.x ;
	  cpt[1] = cpt2_.y ;
	}
	//       fprintf( stderr, "2: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      } else {
	if( min1num < numline1seg ) {
	  for( k = 0 ; k < 4 ; k++ ){
	    if( k < 2 ) 
	      check1[k] = l1p[min1num][k] ;
	    else
	      check1[k] = l1p[min1num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check1[k] = l1p[min1num-1][k] ;
	    else
	      check1[k] = l1p[min1num][k-2] ;
	  }
	}
	if( min2num < numline1seg ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check2[k] = l1p[min2num][k] ;
	    else
	      check2[k] = l1p[min2num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check2[k] = l1p[min2num-1][k] ;
	    else
	      check2[k] = l1p[min2num][k-2] ;
	  }
	}
	cpt1_ = closept(check1, pnt) ;
	cpt2_ = closept(check2, pnt) ;
	double xdiff1 = cpt1_.x - pnt[0] ;
	double ydiff1 = cpt1_.y - pnt[1] ;
	double xdiff2 = cpt2_.x - pnt[0];
	double ydiff2 = cpt2_.y - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ) {
	  cpt[0] = cpt1_.x ;
	  cpt[1] = cpt1_.y ;
	} else {
	  cpt[0] = cpt2_.x ;
	  cpt[1] = cpt2_.y ;
	}
	//       fprintf( stderr, "3: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
	mpt_ = midpt( cpt, pnt ) ;
	mpt[0] = mpt_.x ;
	mpt[1] = mpt_.y ;
	r = radius( mpt, pnt, cpt ) ;
      }
    }
    if( r > laneWidth / 2)
      r = laneWidth / 2 ;

    rddfln2[i][0] = mpt[0] ;
    rddfln2[i][1] = mpt[1] ;
    rddfln2[i][2] = r ;
    
    //printf("rddfln2[%d] (x,y) = (%3.6f,%3.6f)\n", i, rddfln2[i][0], rddfln2[i][1]);
  }

  //%%%%%%%%%%%%%%%%%%%%%%%%
  //% SORT RDDF POINTS
  //%%%%%%%%%%%%%%%%%%%%%%%%
  //cout << "sort rddf points" << endl;
  l1 = 0 ;
  l2 = 0 ;
  // int count // It is now passed as an argument to the function
  //count = 0 ;
  double xdiff1, ydiff1, dist1, xdiff2, ydiff2, dist2 ;
  //int nlp = numline1pts + numline2pts ;
  //cout << "numline1pts = " << numline1pts << endl;
  //cout << "numline2pts = " << numline2pts << endl;
  //cout << "nlp = " << nlp << endl;
  //double rddf[nlp][3];	// this is now passed as an argument to the function
  while( (l1 < numline1pts) || (l2 < numline2pts) ) 
    {
      //cout << "here now 1" << endl;
      if( (rddfln1[l1][0] == 0) && (rddfln1[l1][1] == 0) && (rddfln1[l1][2] == 0))
        l1++;
      if( (rddfln2[l2][0] == 0) && (rddfln2[l2][1] == 0) && (rddfln2[l2][2] == 0))
        l2++ ;
        
      if( l1 < numline1pts ) 
	{
	  xdiff1 = rddfln1[l1][0] - rddfln1[0][0] ;
	  ydiff1 = rddfln1[l1][1] - rddfln1[0][1] ;
	  dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	} 
      //else
      //dist1 = -1 ;
      //cout << "here now 2" << endl;
      if( l2 < numline2pts ) 
	{
	  xdiff2 = rddfln2[l2][0] - rddfln2[0][0] ;
	  ydiff2 = rddfln2[l2][1] - rddfln2[0][1] ;
	  dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	} 
      //else
      //dist2 = -1 ;
      //cout << "here now 3" << endl;
      //if( dist1 < dist2 || dist2 < 0 ) 
      if( (l1 < numline1pts) && (dist1 <= dist2) || (l2 >= numline2pts) )
	{
//	  rddf[count][0] = rddfln1[l1][0];
//	  rddf[count][1] = rddfln1[l1][1];
//	  rddf[count][2] = rddfln1[l1][2];
		rddfx.push_back(rddfln1[l1][0]);
		rddfy.push_back(rddfln1[l1][1]);
		rddfr.push_back(rddfln1[l1][2]);
	  //printf("rddf[%d] from rddfln1 (x,y) = (%3.6f,%3.6f), (%3.6f,%3.6f)\n", count, rddf[count][0], rddf[count][1], rddfln1[l1][0], rddfln1[l1][1]);
	  l1++ ;
	  //count++ ;
	  
	} 
      else if ( l2 < numline2pts )
	{
//	  rddf[count][0] = rddfln2[l2][0];
//	  rddf[count][1] = rddfln2[l2][1];
//	  rddf[count][2] = rddfln2[l2][2];
		rddfx.push_back(rddfln2[l2][0]);
		rddfy.push_back(rddfln2[l2][1]);
		rddfr.push_back(rddfln2[l2][2]);
	  //printf("rddf[%d] from rddfln2 (x,y) = (%3.6f,%3.6f)\n", count, rddf[count][0], rddf[count][1]);
	  l2++ ;
	  //count++ ;

	}

    } 
  
  //cout << "RDDF points count = " << count << endl;


  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  //% CHECK FOR CORRIDOR FEASIBILITY
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  //cout << "checking feasibility" << endl;
  //cout << "rddfr.size() = " << rddfr.size() << endl;
  for( unsigned ii = 0 ; ii < rddfr.size() ; ii++ ) 
  {
    //cout << "radius [" << ii << "] = " << rddfr[ii] << endl;
    if( rddfr[ii] < VEHICLE_WIDTH / 2 )
    {
			error = true ;
			cerr << "ERROR convRDDF: rddfr[ii] < VEHICLE_WIDTH / 2" << endl;
    }
    //int dummy;
    //cin >> dummy;
  }
	//cout << "exiting convRDDF loop" << endl;
  return(error) ;
}

point closept( double lnseg[4], double pt[2] ) 
{ 
  double min[2], max[2], a[2], b1[2], b2[2] ;
  double maga, magb, adotb1, adotb2, minth, maxth, distmin, distln, distmax ;

  min[0] = lnseg[0] ;
  min[1] = lnseg[1] ;
  max[0] = lnseg[2] ;
  max[1] = lnseg[3] ;

  a[0] = pt[0] - min[0] ;
  a[1] = pt[1] - min[1] ;
  b1[0] = max[0] - min[0] ;
  b1[1] = max[1] - min[1] ;
  maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
  magb = sqrt( b1[0] * b1[0] + b1[1] * b1[1] ) ;
  adotb1 = a[0] * b1[0] + a[1] * b1[1] ;
  minth = acos(adotb1/(maga*magb)) ;
  distmin = maga ;
  distln = magb ;

  a[0] = pt[0] - max[0] ;
  a[1] = pt[1] - max[1] ;
  b2[0] = min[0] - max[0] ;
  b2[1] = min[1] - max[1] ;
  maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
  adotb2 = a[0] * b2[0] + a[1] * b2[1] ;
  maxth = acos(adotb2/(maga*magb)) ;
  distmax = maga ;

  double mdist1, a1, mdist2, a2, check, apt1[2], apt2[2] ;
  point cpt ;
  if( (minth <= PI/2) && (maxth <= PI/2 ) && (minth > 0) && (maxth > 0)) {
    mdist1 = distmin * sin(minth) ;
    a1 = mdist1 / tan(minth) ;
    mdist2 = distmax * sin(maxth) ;
    a2 = mdist2 / tan(maxth) ;
    check = fabs( a1 + a2 - distln ) ;
    apt1[0] = ( a1 / distln ) * b1[0] + min[0] ;
    apt1[1] = ( a1 / distln ) * b1[1] + min[1] ;
    apt2[0] = ( a2 / distln ) * b2[0] + max[0] ;
    apt2[1] = ( a2 / distln ) * b2[1] + max[1] ;
    if( check < 0.1 ){ 
      cpt.x = ( apt1[0] + apt2[0] ) / 2 ;
      cpt.y = ( apt1[1] + apt2[1] ) / 2 ;
    } else {
      if( mdist1 < mdist2 ) {
	cpt.x = apt1[0] ;
	cpt.y = apt1[1] ;
      } else { 
	cpt.x = apt2[0] ;
	cpt.y = apt2[1] ;
      }
    }
  } else {
    if( distmin < distmax ) {
      cpt.x = min[0] ;
      cpt.y = min[1] ;
    } else {
      cpt.x = max[0] ;
      cpt.y = max[1] ;
    }
  }
  return(cpt) ;
}

point midpt( double pt1[2], double pt2[2] ) 
{ 
  point mpt ;

  mpt.x = (pt1[0] + pt2[0]) / 2 ;
  mpt.y = (pt1[1] + pt2[1]) / 2 ;

  return(mpt) ;
}

double radius( double mpt[2], double pt1[2], double pt2[2] ) 
{
  double xdiff, ydiff, dist1, dist2, r ;

  xdiff = (mpt[0] - pt1[0]) ;
  ydiff = (mpt[1] - pt1[1]) ;
  dist1 = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
  xdiff = (mpt[0] - pt2[0]) ;
  ydiff = (mpt[1] - pt2[1]) ;
  dist2 = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
  if( dist1 <= dist2 )
    r = dist1 ;
  else
    r = dist2 ;

  return(r) ;
}

void generateMapFromRNDF(RNDF* RNDFObj, Mapper::Map& map)
{
  int numwpnts;
  //Waypoint * wpnt; 
  //std::GPSPoint::Waypoint * wpnt;
    
  vector<Mapper::Segment> segments;
   
  //For every segment in the RNDF
  for (int ii=1; ii <= RNDFObj->getNumOfSegments();ii++)
  {
    
    //Make a new segment
    vector<Mapper::Lane> seg_lanes;
      
    //Get the segment
    //std::Segment * rseg = RNDFObj->getSegment(ii);
      
    //For every lane in the segment
    //for (int jj=0;jj<rseg->getNumberofLanes();jj++)
    for (int jj=1; jj<=(RNDFObj->getSegment(ii))->getNumOfLanes(); jj++)
    {
      //std::Lane * rlane = rseg->getLane(jj);
      //numwpnts = rlane->getNumberofWaypoinse();
      numwpnts = (( RNDFObj->getSegment(ii))->getLane(jj))->getNumOfWaypoints();
      vector<Mapper::Location> centerline;
      
      for (int nn=1; nn<=numwpnts; nn++)
	{ 
	  Mapper::Location loc = { ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getEasting(),
				   ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getNorthing() };
	              
	  centerline.push_back (loc);        
	  
//	  //check to see if the waypoint is a stopline
//	  if ((((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn))->isStopSign())
//	    {
//	      //Mapper::StopLine stopLine = Mapper::StopLine("", ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getEasting(), 
//				//		   ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getNorthing() );
//				Mapper::StopLine stopLine = Mapper::StopLine("", loc.x, loc.y);
//	      lane.addStopLine(stopLine);
//	      printf("Stopline loc = (%3.6f, %3.6f)\n",stopLine.getXLoc(), stopLine.getYLoc());
//	    } 
	   
	}
      
      //Make a new lane object
      Mapper::Lane lane = Mapper::Lane(jj, centerline);
      
      // Add stoplines to the lanes as necessary
      for (int nn=1; nn<=numwpnts; nn++)
	{ 
	  //check to see if the waypoint is a stopline
	  if ((((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn))->isStopSign())
	    {
	      Mapper::StopLine stopLine = Mapper::StopLine("", ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getEasting(), 
						   ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getNorthing() + 1);
				//Mapper::StopLine stopLine = Mapper::StopLine("", loc.x, loc.y);
	      lane.addStopLine(stopLine);
	      //cout << "segmentID = " << ii << endl;
	      //cout << "laneID = " << lane.getID() << endl;
	      //vector<Mapper::StopLine> stoplines_temp = lane.getStopLines();
	      //cout << "stoplines_temp.size() = " << stoplines_temp.size() << endl;
	      //printf("Stopline loc = (%3.6f, %3.6f)\n",stopLine.getXLoc(), stopLine.getYLoc());
	      //printf("Stopline loc = (%3.6f, %3.6f)\n",((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getEasting(), ((RNDFObj->getSegment(ii))->getLane(jj))->getWaypoint(nn)->getNorthing());
	    } 
  }
      
		  
      //put all of the lanes in a segment 
      seg_lanes.push_back(lane);
      
      if (jj==1)
	{ //again, compensating for 1-indexing
	  Mapper::Lane lane = Mapper::Lane(jj, centerline);
	  seg_lanes.push_back(lane);       
    	}
        
    }
        
    //push that segment onto the vector of segments
    Mapper::Segment seg = Mapper::Segment(ii,seg_lanes);
    //put that segment on the vector of segments 
    segments.push_back(seg);
        
    if (ii==1)
    { //compensating for 1-indexing
      Mapper::Segment seg = Mapper::Segment(ii,seg_lanes);
      segments.push_back(seg);
    }
    //Mapper::Lane lanes_temp = seg.getLane(1); 
    //vector<Mapper::StopLine> stoplines_temp2 = lanes_temp.getStopLines();
   	//cout << "stoplines_temp2.size() = " << stoplines_temp2.size() << endl;
     
  }
  map = Mapper::Map(segments);
  //Mapper::Segment seg_temp = map.getSegment(1);
  //Mapper::Lane lanes_temp2 = seg_temp.getLane(1); 
  //vector<Mapper::StopLine> stoplines_temp3 = lanes_temp2.getStopLines();
  //cout << "stoplines_temp3.size() = " << stoplines_temp3.size() << endl;
  
}
