#ifndef TRAFFICPLANNER_HH
#define TRAFFICPLANNER_HH

#include "StateClient.h"
#include "CMapPlus.hh"
#include "MapdeltaTalker.h"
#include "MapConstants.h"
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include "rddf.hh"
#include "AliceConstants.h"
#include "trafficUtils.hh"
#include "../../../util/moduleHelpers/RDDFTalker.hh"
#include "../../sidd/Mapper/include/LocalMapTalker.hh"
#include "rndf.hh"
#include "SegGoalsTalker.hh"
#include "../../nok/missionPlanner/GloNavMapTalker.hh"
#include <vector>
#include <math.h>
//#include "../Mapper/include/Map.hh"
#include "Map.hh"



/****************************************************************************/
/* 						Temporary defs										*/
/****************************************************************************/
//enum Divider { DOUBLE_YELLOW=0, SOLID_WHITE=1, BROKEN_WHITE=2, ROAD_EDGE=3 };
enum PlanningHorizon {CURRENT_SEGMENT, NEXT_SEGMENT, NEXT_NEXT_SEGMENT};



/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/


class CTrafficPlanner : public CStateClient, public CSegGoalsTalker, public CGloNavMapTalker, public CMapdeltaTalker, public CRDDFTalker, public CLocalMapTalker
{ 
  
  RNDF* m_rndf;
  int m_rndfRevNum;                // RNDF revision#
  pthread_mutex_t m_GloNavMapMutex;

  // basic constructor of local map
  //vector<Mapper::Segment> localMapSegments;
  Mapper::Map m_localMap_test;
  Mapper::Map m_localMap;
  //Mapper::Map m_localMap;
  pthread_mutex_t m_LocalMapMutex;
  pthread_cond_t m_LocalMapRecvCond;
  pthread_mutex_t m_LocalMapRecvMutex;
  bool m_bReceivedAtLeastOneLocalMap;
  //CLocalMapTalker mapTalker;
  int localMapSocket;                // The skynet socket for receiving local map from mapping
  
  //vector<Obstacle*> m_obstacle;
  //pthread_mutex_t m_ObstacleMutex;

  CMapPlus m_costMap;
  CMapPlus m_elevationMap;
  pthread_mutex_t m_CostMapMutex;

  int m_fullMapRequestSocket;
  int m_GloNavMapRequestSocket;
  int m_GloNavMapSocket;

  DPlannerStatus* m_dplannerStatus;
  pthread_mutex_t m_DPlannerStatusMutex;

  list<SegGoals> m_segGoals;
  pthread_mutex_t m_SegGoalsMutex;
  int segGoalsSocket;                // The skynet socket for receiving segment goals from mplanner

  /*! Whether at least one delta was received */
  bool m_bReceivedAtLeastOneDelta;
  pthread_mutex_t m_deltaReceivedMutex;
  pthread_cond_t m_deltaReceivedCond;

public:
  /*! Contstructor */
  CTrafficPlanner(int skynetKey, bool bWaitForStateFill, char*);
  /*! Standard destructor */
  ~CTrafficPlanner();

  /*! this is the function that continually reads map deltas and updates the map */
  void getMapDeltasThread();

  void getLocalMapThread();

  /*! this is the function that continually reads dplanner status*/
  void getDPlannerStatusThread();

  /*! this is the function that continually receives segment goals from mplanner */
  void getSegGoalsThread();

  void TPlanningLoop(void);

 

private:
  /*! This function is used to request the full map from mapping */
  void requestFullElevationMap();

  void requestGloNavMap();

  };

#endif  // TRAFFICPLANNER_HH

