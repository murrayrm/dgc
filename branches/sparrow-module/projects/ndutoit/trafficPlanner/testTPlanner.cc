#include "trafficUtils.hh"
#include "TrafficPlanner.hh"

// Test script to check different functions
int main()
{
  vector<double> x_B, y_B;
  for (ii=0; ii<10; ii++)
  {
    x_B.push_back(ii);
    y_B.push_back(1);
  }
  
  double x_pt = 4.8;
  double y_pt = 0.5;
  double x_temp, ytemp;
  int ind_CP = closestExistingBoundaryPt(x_B, y_B, x_pt, y_pt);
  printf("ind_CP = %d\n", ind_CP);
  int ind_Ins = closestPtOnBoundary(x_B, y_B, x_pt, y_pt, ind_CP, x_temp, y_temp);  
  printf("ind_Ins = %d\n", ind_Ins);
  printf("Closest point on boundary = (%3.6f, %3.6f)\n", x_temp, y_temp);
}

