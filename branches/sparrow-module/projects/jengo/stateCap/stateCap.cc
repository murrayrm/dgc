#include "stateCap.hh"
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

stateCap::stateCap(int skynetKey) : CSkynetContainer(SNastate, skynetKey) {

  // open the log file

  char logFileName[256];
  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);
  
  sprintf(logFileName, "../logs/state_log_%04d%02d%02d_%02d%02d%02d.log", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

  m_output.open(logFileName);
 
}

void stateCap::getData() {

  m_output << fixed << showpoint << setprecision(6);
  cout << fixed << showpoint << setprecision(6);

  UpdateState();

  // get start time
  long long initial_time = m_state.Timestamp/100000;

  while(true) {
  
    UpdateState();
  
    // access members from VehicleState
    position.setvals(m_state.Northing, m_state.Easting);
    velocity.setvals(m_state.Vel_N, m_state.Vel_E);
    acceleration.setvals(m_state.Acc_N, m_state.Acc_E);
    current_time = ((double)(m_state.Timestamp/100000 - initial_time))/10;

    // write to a file

    m_output << current_time << ",";
    m_output << position.N << "," << position.E << ",";
    m_output << velocity.norm() << ",";
    m_output << acceleration.norm() << endl;

    // print to stdout

    cout << current_time << " ";
    cout << position.N << " " << position.E << " ";
    cout << velocity.norm() << " ";
    cout << acceleration.norm() << endl;
        
    // wait for a little while (about a tenth of a second)
    DGCusleep(100000);

  }



}

int main() {
 
  // do something with skynet key
  int skynet_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if ( pSkynetkey == NULL)
    {
      cerr << "SKYNET_KEY environment variable isn't set." << endl;
    } else
      {
	skynet_key = atoi(pSkynetkey);
      }
  cerr << "Constructing skynet with KEY = " << skynet_key << endl;

  // get some data
  stateCap s(skynet_key);
  s.getData();

  return 0;

}
