#include "astateSim.hh"
#include <fstream>

using namespace std;

/*! The name of the file to load into the astate simulator. */
const char* FILENAME = "../logs/gps_20060824_122110.dat";

/*! \brief Test class for astateSim
 *
 * Inherits from CStateSimClient and creats an astateSim object, which it
 * then uses to load a simulation file and read back the coordinates. 
 *
 * To use: 
 * - Generate a simulation file as outlined in astateSim::loadSimFile
 * - Enter the filename as the FILENAME attribute in astateSimTest.cc
 * - run make
 * - run astateSimTest and enter the distance between points at the prompt
 */
class astateSimTest : public CStateSimClient {

public:

  /*! Constructor. Creates a skynet container for receiving messages. */
  astateSimTest(int skynetKey) 
    : CStateSimClient(skynetKey) { }

  /*! Destructor. */
  ~astateSimTest() { }

  /*! Uses astateSim::UpdateState() to cycles through all the coordinates;
   * prints them to stdout */
  void printState();

  /*---------------------------------------------------------------------*/
  void writeState();
  /*---------------------------------------------------------------------*/

};

void astateSimTest::printState()
{

  loadSimFile(FILENAME);

  while(UpdateStateSim())
    m_state_sim.ne_coord().display();

}

/*---------------------------------------------------------------------*/
void astateSimTest::writeState()
{

  loadSimFile(FILENAME);

  int i=1;

  //To change (...).dat to (...)_UTM.dat
  char UTM_FILENAME_TEMP[100];
  strcpy(UTM_FILENAME_TEMP,FILENAME);
  UTM_FILENAME_TEMP[strlen(UTM_FILENAME_TEMP)-4] = '\0';
  strcat(UTM_FILENAME_TEMP, "_UTM.dat"); 
  const char* UTM_FILENAME = UTM_FILENAME_TEMP;

  ofstream fileToWrite(UTM_FILENAME);
  fileToWrite.precision(15);

  while(UpdateStateSim())
    {
      fileToWrite<<m_state_sim.Northing<<" "<<m_state_sim.Easting<<endl;
      i++;
    }

  fileToWrite.close();
}
/*---------------------------------------------------------------------*/

int main() {

  //////////////////////
  // Store skynet key //
  //////////////////////

  cout<<endl;  
  int skynet_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if ( pSkynetkey == NULL)
    {
      cerr << "SKYNET_KEY environment variable isn't set." << endl;
    } else
      {
	skynet_key = atoi(pSkynetkey);
      }
  cerr << "Constructing skynet with KEY = " << skynet_key << endl;

  ///////////////
  // Run tests //
  ///////////////

  astateSimTest test1(skynet_key);

  test1.printState();

  /*----------------------------------------------------------
  char opt;
  
  cout << "Print(p) or write(w)?" << endl;
  cin >> opt;
  switch(opt)
    {
    case 'p':
      test1.printState();
      break;
    case 'w':
      test1.writeState();
      break;
    }
  ----------------------------------------------------------*/

  return 0;
}
