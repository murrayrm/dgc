#ifndef STATE_HH
#define STATE_HH

#include "globalConstants.hh"
/*! Going to include RNDF instead of sending skynet messages
 * because it's useful to have Waypoint, Lane, etc classes for tplanner. 
 *
 * NOTE: The traffic planner creates a version of the RNDF in the Environment,
 * and then uses POINTERS to access the information in any of its other
 * functions. This way we only have one version of all the stuff in the 
 * RNDF hanging around.
 */
#include "../../morlanliu/RNDF.hh" /* trunk/projects/morlanliu/ */
#include "StateClient.h"
#include "../astateSim/astateSim.hh"

using namespace std;

/*! \brief Stores information about a vehicle, such as its location
 * and characteristics. 
 *
 * Can be used for tracking other vehicles as well as
 * our vehicle. This class is used by tplanner and will probably become
 * private once that module is merged into the projects directory.
 */
class State {

 private:
  
  /*! The probable mode a vehicle is in. This won't be used for our
   * vehicle, since hopefully tplanner will know our current mode,
   * but instead it is used to store what prediction says other vehicles
   * are doing.
   */
  int mode;

  /*! ID tag for the vehicle. All vehicles in a single mission will have
   * a unique identifier 
   *
   * NOTE: Alice's ID is always zero.
   */
  int vehicleID;
  
  /*! Position of the vehicle */
  NEcoord pos;
  /*! Velocity of the vehicle */
  NEcoord vel;

  /*! The lane the vehicle is in */
  Lane *currLane;
  /*! the lane the vehicle is transitioning to (0 if no transition) */
  Lane *destLane;
  
 public:

  /*! Constructor.
   * \param id The vechicle ID.
   * \param position The vehicle position.
   * \param velocity The vehicle velocity.
   * \param lane The current vehicle lane.
   */
  State(int id, NEcoord position, NEcoord velocity);

  /*! Destructor */
  ~State();

  ///////////////
  // Accessors //
  ///////////////

  /*! \return The vehicle mode */
  int getMode() const { return mode; }
  /*! \return The vehicle ID */
  int getID() const { return vehicleID; }
  /*! \return The vehicle position */
  NEcoord getPos() const { return pos; }
  /*! \return The vehicle velocity */
  NEcoord getVel() const { return vel; }
  /*! \return The current vehicle lane */
  Lane* getCurrLane() const { return currLane; }
  /*! \return The vehicle's destination lane if transitioning and
   * the current lane if there is no transition. */
  Lane* getDestLane() const { return destLane; }
  /*! \return true if the vehicle is stopped */
  bool stopped() const;
  /*! \return true if we believe that the vehicle is queued at
   * an intersection. */
  bool inQueue(NEcoord intersection) const;
  /*! \return true of the vehicle is going the same direction we are. */
  bool inRightLane(Lane* firstRightLane) const;

  ///////////////
  // Modifiers //
  ///////////////

  /*! Sets the vehicle mode. */
  void setMode(const int m);
  /*! Sets the vehicle position. 
   * Will eventually check to see if the change in position => a change
   * in the current lane.
   */
  void setPos(const NEcoord newPos);
  /*! Sets the vehicle position. 
   * Will eventually check to see if the change in position => a change
   * in the current lane.
   */
  void setPos(const double x, const double y);
  /*! Sets the vehicle velocity. */
  void setVel(const NEcoord newVel);
  /*! Sets the vehicle velocity. */
  void setVel(const double x, const double y);
  /*! Sets the current lane. */
  void setCurrLane(Lane* lane);
  /*! Sets the destination lane. */
  void setDestLane(Lane* lane);
  
};

#endif
