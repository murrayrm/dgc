using namespace std;

#include "trackMO.hh"

int QUIT_PRESSED = 0;

//constructor. what all do I need it to do??
trackMO::trackMO(int sn_key, char* scanFile) 
  : CSkynetContainer(MODtrackMO, sn_key, &status_flag)
{

  strcpy(datafile, scanFile);
  cout<<"datafile: "<<datafile<<endl;

  numCars = 0;
  numFrames = 0;

  //initializes the stack of open positions in the car array
  for(int i=(MAXNUMCARS-1);i>=0; i--){
    openObjs.push_back(i);
    openCars.push_back(i);
  }

  //setting up matrics for KF calculations
  //#warning "these need to be tuned!"
  double dt = .0133; 
  double sa = .05; //std dev of system noise -- tune!
  double sz = .1; //std dev of measurement noise -- should be fxn of range/theta!!!

  A.resetSize(2,2);
  double Aelems[] = {1,dt,0,1};
  A.setelems(Aelems);

  C.resetSize(1,2);
  double Celems[] = {1,0};
  C.setelems(Celems);

  I.resetSize(2,2);
  double Ielems[] = {1,0,0,1};
  I.setelems(Ielems);

  R.resetSize(2,2);
  double Relems[] = {0,0,0,sa};
  R.setelems(Relems);

  Q.resetSize(1,1);
  double Qelems[] = {sz};
  Q.setelems(Qelems);


  setupLogs();

  m_send_socket = m_skynet.get_send_sock(SNmovingObstacle);

  cout<<"trackMO(...) Finished"<<endl;
}

trackMO::~trackMO() 
{

}

/******************************************************************************/
/******************************************************************************/
void trackMO::Active() 
{

  cout<< "Starting Active Function" <<endl;

  while(!QUIT_PRESSED) 
    {

    } // end while(!QUIT_PRESSED) 

  cout<< "Finished Active state" <<endl;
}


/** 
 * This is called each time trackMO receives a new scan, either
 * from reading the next line in a file, or receiving a skynet
 * message. from this point on, both types of input are treated
 * the same. 
 **/
void trackMO::processNewScan() {
 
  numFrames++;
  segmentScan();  

  prepObjects();
  classifyObjects();
  checkMotion();

  //  cleanCars();
  sendObjects();

  KFupdate();
  KFpredict();

  return;
}

/**
 * This function segments the scan currently stored in newPoints
 **/
void trackMO::segmentScan()
{

  //  cout<<"entering segmentScan()"<<endl;
  double currN, currE, scanN, scanE, currRange;

  //#warning "This is vehicle state, not LADAR's origin...Ladar is .95m in front of vehicle origin (according to LadarCal.ini.bumper)...assuming that .965 is rel to front axle, and that state is reported at rear"
  if(datafile[0]=='\0') {
    UpdateState();
    currN = m_state.Northing;// +  (LADARXOFFSET) * cos(m_state.Yaw);
    currE = m_state.Easting;// + (LADARXOFFSET) * sin(m_state.Yaw); 
  } else {

#ifdef USE_STATE_FOR_READING
    UpdateState();
    currN = m_state.Northing;// +  (LADARXOFFSET) * cos(m_state.Yaw);
    currE = m_state.Easting;// +  (LADARXOFFSET) * sin(m_state.Yaw); 
#else
    currN = 0;
    currE = 0;
#endif
  }

  int tempNum = 0; //number objects segmented from curr scan
  int currSize = 1; //size of current object

  posSegments[0][0] = 0;
  posSegments[0][1] = 0;

  for(int i=0; i<NUMSCANPOINTS; i++) {
    scanN = newPoints[i].N;
    scanE = newPoints[i].E;

   if((scanN == 0 && scanE == 0) || (scanN == currN && scanE == currE))
      currRange = 0;
    else
      currRange = dist(currN, currE, scanN, scanE);
    ranges[i] = currRange;
    angles[i] = atan2(scanE - currE, scanN - currN);
  }

  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    diffs[i] = ranges[i+1] - ranges[i];
  }

 
  // This is the actual segmentation functionality...
  // the above was just setting up the data we need

  int tempPos[NUMSCANPOINTS][2];
  int tempSize[NUMSCANPOINTS];

  double distThresh;
  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    if(ranges[i] > 80.0) {
      distThresh = 0;
    } else {
    //threshold should be scaled based on distance from ladar
    distThresh = max(.10,ranges[i]*.0175*SCALINGFACTOR);
    //    cout<<"range: "<<ranges[i]<<"   threshold: "<<distThresh<<endl;
    }
    //if the distance is too large to be the same object
    //note that this distance is the distance btwn point i and pt i+1
    if(fabs(diffs[i]) > distThresh) {
 
      currSize = 1;
      tempNum++;
 
      //set endpoint of previous object
      tempPos[tempNum-1][1] = i; 

      //set beginning point of new object
      tempPos[tempNum][0] = i+1;

      //initialize size of new object
      tempSize[tempNum] = currSize;

    } else { //this point belongs to current object
      
      currSize++;
      tempSize[tempNum] = currSize;

    } //end checking diff

  } //end cycling through points

  tempPos[0][0] = 0;
  tempPos[tempNum][1] = NUMSCANPOINTS-1;

  int st,en;
  int origin;
  numSegments = 0;
  for(int i=0; i<=tempNum; i++) {
    st = tempPos[i][0];
    en = tempPos[i][1];
    origin=0; 
    //    cout<<"ranges: ";
    for(int j=st; j<=en; j++) {
      //      cout<<ranges[j]<<", ";
      if(ranges[j] > 80) {
	origin=1;
      } else {
	//	cout<<'('<<newPoints[j].N<<' '<<newPoints[j].E<<"), ";
      }
    }
    //    cout<<endl;
    //checking that this isn't a group of no-return points
    if(origin==0 && tempSize[i]>=MINNUMPOINTS) {
      posSegments[numSegments][0]=st;
      posSegments[numSegments][1]=en;
      sizeSegments[numSegments] = tempSize[i];
      numSegments++;
    }
  }

  //#warning "ADD CONVEX HULL REQUIREMENT HERE!"

  //now, trying to output the segmentation, to check that it's correct
  for(int i=0; i<numSegments;i++) {
      m_outputSegments<<posSegments[i][0]<<' '<<posSegments[i][1]<<' ';
  }
  for(int i=numSegments; i<NUMSCANPOINTS; i++) {
    m_outputSegments<<"-1 -1 ";
  }
  m_outputSegments<<endl;
} //end of segmentScan()



/**
 * this function checks each car against every other, removing
 * any potential duplicates.
 *
 * next functionality to be added is checking last-updated time...
 * if we haven't seen an MO for some time, we don't care about it
 **/
//#warning "change this from cars to objcars!!"
void trackMO::cleanCars() {

  //  cout<<"entering cleanCars()"<<endl;

  double diff;
  int age1, age2;
  double car1e, car1n, car2e, car2n;

  //checking if car is too old
  for(unsigned int k = 0; k<usedCars.size(); k++) {
    int l = usedCars.at(k);
    if((numFrames - cars[l].lastSeen) > MOTIMEOUT) {
      cout<<"Car is too old -- removing #"<<cars[l].ID<<" (last seen at "<<cars[l].lastSeen<<", curr time is "<<numFrames<<endl;
      removeCar(&cars[l],k);
    }
  } //end checking age()

  //checking if 2 cars are the same
  for(unsigned int m=0; m<usedCars.size(); m++) {
    int i=usedCars.at(m);
    for(unsigned int n=m+1; n<usedCars.size(); n++) {
      int j = usedCars.at(n);
	age1 = cars[i].histPointer;
	age2 = cars[j].histPointer;

	//#warning "how do I want to clculate this? should be consistent w/ rest of code"
      //      diff = traveledDist(&cars[i], &cars[j]);
	car1e = cars[i].lastE[(age1-1)%10];
	car1n = cars[i].lastN[(age1-1)%10];
	car2e = cars[j].lastE[(age2-1)%10];
	car2n = cars[j].lastN[(age2-1)%10];

      diff = dist(car1e, car1n, car2e, car2n);
      //      cout<<"distance between cars i&j = "<<diff<<" i, j = "<<i<<' '<<j<<endl;
      if(diff < MAXTRAVEL) {
	if(age1 < age2) {
	  //	  cout<<"removing car #"<<i<<" (w/in "<<diff<<"m of car #"<<j<<')'<<endl<<flush;
	  removeCar(&cars[i],m);
	} else {
	  //	  cout<<"removing car#"<<j<<" (w/in "<<diff<<"m of car #"<<i<<')'<<endl;
	  removeCar(&cars[j],n);
	}
      }//end checking against MAXTRAVEL
    }  // end n for loop
  }    // end m for loop
}      //end cleanCars()

//#warning "does this break the reference from object?"
//#warning "change this to work on cars, not just cars"
/**
 * removes car by putting its index back on the stack
 **/
void trackMO::removeCar(carRepresentation* oldcar, int i) {

  //  cout<<"entering removeCar"<<endl;

  //sending msg that we're removing the car
  movingObstacle obstacle;
  obstacle.ID = oldcar->ID;

  obstacle.status = 3;
  DGCgettime(obstacle.timeStamp);
  int msgSize = sizeof(movingObstacle);
  m_skynet.send_msg(m_send_socket, &obstacle, msgSize,0);

  //putting index back on stack
  openCars.push_back(usedCars.at(i));
  carIterator = usedCars.begin();
  carIterator += i;
  usedCars.erase(carIterator);

}

/**
 * This code goes through all the new segments, picks out the ones
 * that correspond to objects that we care about (based on number of
 * points in object, and that the object isn't a group of no-return
 * points) 
 * It then updates the candidates array with the appropriate information
**/
void trackMO::prepObjects() {

  //  cout<<"entering prepObjects()"<<endl;
  //  cout<<"STARTING prepObjects()"<<endl;
 
  numCandidates = 0;

  int objStart, objEnd;
  double dStart, dEnd;

  for(int i=0; i<numSegments; i++) {

    int objectsize = sizeSegments[i];

    if(objectsize >= MINNUMPOINTS) {

      objStart = posSegments[i][0];
      objEnd = posSegments[i][1];

      //if dStart or dEnd > 0, that means that that end of the object 
      //is in the foreground, <0 means the object next to it is in the 
      //foreground, and 0 means that it's at the edge of a scan
#warning "check that ranges don't get set to 0 for no-return values before we get here"
      if(objStart == 0) {
	dStart = 0;
      } else {
	dStart = -diffs[objStart - 1];
      }
      if(objEnd == NUMSCANPOINTS - 1) {
	dEnd = 0;
      } else {
	dEnd = diffs[objEnd];
      }

      //want to fit center to object here, and put it in candidates array....

      if(ranges[objStart] < 80.0 && ranges[objEnd] < 80.0 ) {

	double cenN, cenE;
	double sumN = 0;
	double sumE = 0;

	for(int j=0;j<objectsize;j++) {
	  sumE += newPoints[j+objStart].E;
	  sumN += newPoints[j+objStart].N;
	}

	cenN = sumN / objectsize;
	cenE = sumE / objectsize;

	candidates[numCandidates].N = cenN;
	candidates[numCandidates].E = cenE; 
	//	cout<<"in prep objects, new object's N is: "<<candidates[numCandidates].N<<endl;

	candidates[numCandidates].dStart = dStart;
	candidates[numCandidates].dEnd = dEnd; 

	vector<NEcoord> tmppts;
	vector<double> rngs;
	vector<double> angs;

	NEcoord tmp;

	//#warning "keeping this many points may be a problem"
	for(int m=0;m<objectsize;m++) {
	  tmp.N = newPoints[m+objStart].N;
	  tmp.E = newPoints[m+objStart].E;
	  tmppts.push_back(tmp);
	  rngs.push_back(ranges[m+objStart]);
	  angs.push_back(angles[m+objStart]);
	}
	candidates[numCandidates].scans.push_back(tmppts);
	candidates[numCandidates].ranges.push_back(rngs);
	candidates[numCandidates].angles.push_back(angs);

	numCandidates++; //we've found a new object

      } //end ranges condition
    }   //end num points condition
  }     //end cycling through segments
}   //end of prepObjects()


/**
 * This function assumes that the scan has been segmented, and 
 * classifies the new objects into: already-seen object, already-seen
 * car, and new object. Then, it calls the appropriate function to update
 * the representations.
 **/
void trackMO::classifyObjects() 
{
  //  cout<<"STARTING trackObjects()"<<endl;
  double distTraveled;

  int taken[numCandidates];
  for(int i=0;i<numCandidates;i++)
    taken[i] = 0;

  double oldN, oldE;
  double newN, newE;

  /**
   * First, we cycle through all previously seen objects to see if the new 
   * object matches one of them
   **/
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int k = usedObjs.at(i);
    oldN = objects[k].N;
    oldE = objects[k].E;

    for(int j=0; j<numCandidates; j++) {
      if(taken[j] == 0) { //if new object hasn't already been classified
	newN = candidates[j].N;
	newE = candidates[j].E;
	distTraveled = dist(oldN, oldE, newN, newE);

	if(distTraveled < MAXTRAVEL) { //if they match
	  //	  cout<<"new object matched old object...updating!"<<endl;
	  taken[j] = 1;
	  updateObject(&objects[k], &candidates[j]);

	} //end checking distance traveled
      } //end checking of newObj is taken
    } //end cycling through new objects
  } //end cycling through old objects


  /**
   * Next, we cycle through all previously seen cars to see if obj matches
   * This functionality goes AFTER matching newobjs to oldobjs cuz we don't want 
   * to match an already seen lamppost in the car's path to the car...
   **/
  double Nmin, Nmax, Emin, Emax;
  double dn1, de1, dn2, de2;
  int foo = 0;
  for(unsigned int i=0; i<usedCars.size(); i++) {
    int k = usedCars.at(i);

    oldN = cars[k].Nmu_hat.getelem(0,0);
    oldE = cars[k].Emu_hat.getelem(0,0);
    //    oldN = cars[k].n;
    //    oldE = cars[k].e;
    dn1 = cars[k].dn1;
    de1 = cars[k].de1;
    dn2 = cars[k].dn2;
    de2 = cars[k].de2;

    Nmin = min(oldN, min(oldN+dn1, min(oldN+dn2, oldN+dn1+dn2))) - CARMARGIN;
    Nmax = max(oldN, max(oldN+dn1, max(oldN+dn2, oldN+dn1+dn2))) + CARMARGIN;
    Emin = min(oldE, min(oldE+de1, min(oldE+de2, oldE+de1+de2))) - CARMARGIN;
    Emax = max(oldE, max(oldE+de1, max(oldE+de2, oldE+de1+de2))) + CARMARGIN;


    for(int j=0; j<numCandidates; j++) {
      if(taken[j] == 0) { //this obj has not already been claimed
	newN = candidates[j].N;
	newE = candidates[j].E;
	//check if this obj matches car
	if(newN < Nmax && newN > Nmin && newE < Emax && newE > Emin) {
	  //the obj and car should be associated
	 
	  taken[j] = 1;
	  cout<<"new obj matched car #"<<k<<"-- updating!!"<<endl;
	  //	  cout<<"center points for object: "<<newN<<' '<<newE<<endl;
	  //	  cout<<setprecision(8)<<"before update, N and E for slot "<<k<<" are: "<<cars[k].n<<' '<<cars[k].e<<endl;
	  updateCar(&cars[k], &candidates[j]);
	  foo++;
	  //	  cout<<setprecision(8)<<"after update, N and E for slot "<<k<<" are: "<<cars[k].n<<' '<<cars[k].e<<endl;
	}  //end of checking if newObj and car match
      }   //end of checking if obj already claimed
    }    //end cycling thru newObjs
  }     //end cycling thru numCars

  for(int i=foo; i<MAXNUMCARS; i++) {
    m_outputTest<<"-1 -1 -1 -1 -1 -1 ";
  }
  m_outputTest<<endl;

  /**
   * Finally, we take care of the new objects that don't match anything we've
   * seen before
   **/
  for(int i=0; i<numCandidates; i++) {
    if(taken[i] == 0) { //this is a lonely, unwanted new object
      cout<<"new object didn't match anything -- creating new obj!"<<endl;
      addObject(&candidates[i]);
    } //end checking if object already classified
  } //end cycling through numCandidates

}

//#warning "may want to break this up into diff functions (build model, evaluate model)"
//this is code from probMoving, buildModel and probModel
#warning "may want to add more checks before checking every object's motion, every time"
void trackMO::checkMotion() {

for(unsigned int i = 0; i< usedObjs.size(); i++) {
  int index = usedObjs.at(i);
  // cout<<"ENTERING checkMotion() with index = "<<index<<setprecision(10)<<endl;
  if(objects[index].timesSeen >= 20) {
#ifdef CHECK_MOTION_PROBABILITY
  vector<NEcoord> scan1(objects[index].scans.front());
  vector<double> scan1R(objects[index].ranges.front());
  vector<double> scan1A(objects[index].angles.front());
  //  scan1 = vector
  int numpts = scan1.size();
  double xpt[numpts];
  double ypt[numpts];
  double grrR[numpts];
  double grrA[numpts];


  //  cout<<"numPts = "<<numpts<<endl;
  vector<NEcoord>::iterator objI;
  vector<double>::iterator objR;
  vector<double>::iterator objA;
  objI = scan1.begin();
  objR = scan1R.begin();
  objA = scan1A.begin();
  int i=0;
  while(objI != scan1.end()) {
    //  for(int i=0;i<numpts;i++) {

    xpt[i] = (*objI).N;
    ypt[i] = (*objI).E;
    grrR[i] = *objR;
    grrA[i] = *objA;
    //    cout<<"i,N,E: "<<i<<' '<<ypt[i]<<' '<<xpt[i];
    //    cout<<" grrr!"<<endl;
    objI++;
    objR++;
    objA++;
    i++;
  }

  double s = .01; //side length (m)
  int  buffer = 5; //how many points on each side to add


  //now, we need to build the model, based on the first scan
  double x1, x2, y1, y2, m, mp, r, xa, ya, xb, yb;
  int start;
  int length = 2*buffer;
  for(int i=0;i<numpts-1;i++) {
    r = sqrt(pow((xpt[i]-xpt[i+1]),2)+pow((ypt[i]-ypt[i+1]),2));
    length += (int)floor(r/s);
  }

  //  cout<<"expecting dimensions: "<<2*buffer+1<<' '<<length<<endl;
  double model[2*buffer+1][length][2]; //TODO: ??? should be the total seg length
  int len = 0;
  //for each segment, adding onto the "model"
  for(int i=0; i<numpts-1; i++) {
    x1 = xpt[i];
    x2 = xpt[i+1];
    y1 = ypt[i];
    y2 = ypt[i+1];

    r = sqrt(pow(x1-x2,2)+pow(y1-y2,2));

    //    m = (y2-y1)/(x2-x1);
    m = (x2-x1)/(y2-y1);
    mp = -1/m;

    ya = sqrt(1/(1*m*m));
    yb = sqrt(1/(1*mp*mp));
    if(y2<y1) {
      ya = -1*ya;
      yb = -1*yb;
    }
    xa = m*ya;
    xb = mp*yb;

    //    cout<<"points..."<<x1<<' '<<y1<<' '<<x2<<' '<<y2<<endl;
    //    cout<<"ranges, angles: "<<grrR[i]<<' '<<grrA[i]<<endl;
    //    cout<<"slopes: "<<m<<' '<<mp<<endl;
    //    cout<<"delta coords..."<<xa<<' '<<ya<<' '<<xb<<' '<<yb<<endl;

    if(i==0)
      start = -buffer;
    else
      start = buffer;
    double xt, yt;
    for(int j=start; j<(floor(r/s)+buffer);j++) {
      for(int k=-buffer; k<=buffer; k++) {
	
        xt = x1 + (j)*xa*s + (k)*xb*s;
        yt = y1 + (j)*ya*s + (k)*yb*s;
	//	cout<<"trying to add "<<xt<<' '<<yt<<" to the model, using j&k: "<<j<<' '<<k;
	//	cout<<k+buffer<<", "<<len<<", "<<xt<<", "<<yt<<"; ";
	//	cout<<len<<"; ";
	model[k+buffer][len][0] = xt;
	model[k+buffer][len][1] = yt;
	//	cout<<"  grr!"<<endl;

      }
      len++;
      //      cout<<endl;
    }
    //    cout<<endl;
  }

  //now that the model's build, need to calculate probabilities
  //(what's covered in probModel.m)
  vector<vector<NEcoord> >::iterator neIt;
  vector<vector<double> >::iterator rIt;
  vector<vector<double> >::iterator aIt;

  neIt = objects[index].scans.begin();
  neIt++;
  rIt = objects[index].ranges.begin();
  rIt++;
  aIt = objects[index].angles.begin();
  aIt++;

  Matrix P;
  Matrix P1;
  Matrix P2;
  Matrix mu;
  Matrix pos;
  Matrix diff;

  P = Matrix(2,2);
  P1 = Matrix(2,2);
  P2 = Matrix(2,2);
  mu = Matrix(2,1);
  pos = Matrix(2,1);
  diff = Matrix(2,1);



  //  for(int i=1; i<5; i++) {
  double probscan;
  double fp[objects[index].scans.size()-1];
  int ns = 0; //what scan # we're on
  while(neIt != objects[index].scans.end()) {
    //    cout<<"trying to access NEcoord..."<<endl;

    probscan = 1;
    vector<NEcoord> scan(*neIt);
    vector<double> ang(*aIt);
    vector<double> ran(*rIt);

    //creating x,y,theta,range vectors
    unsigned int scanSize = scan.size();
    //    double xx[scanSize];
    //    double yy[scanSize];
    //    double ra[scanSize];
    //    double an[scanSize];
    double x,y,r,a;
    double temp,temp1, temp2;
    double probpt,det;
    Matrix power(1,1);
    //    cout<<"calculating for scanSize = "<<scanSize<<endl;
    for(unsigned int j=0;j<scanSize;j++) {
      x = scan.at(j).N;
      y = scan.at(j).E;
      r = ran.at(j);
      a = ang.at(j);

      mu.setelem(0,0,x);
      mu.setelem(1,0,y);

      P1.setelem(0,0,2*sin(a)*sin(a));
      P1.setelem(0,1,-sin(2*a));
      P1.setelem(1,0,-sin(2*a));
      P1.setelem(1,1,2*cos(a)*cos(a));

      P2.setelem(0,0,2*cos(a)*cos(a));
      P2.setelem(0,1,sin(2*a));
      P2.setelem(1,0,sin(2*a));
      P2.setelem(1,1,2*sin(a)*sin(a));

      //#warning "would be nice if Matrix class could handle multiplication by a scalar, and determinants..."
      temp1 = .5*r*r*STDDEVANGLES*STDDEVANGLES;
      temp2 = .5*STDDEVRANGES*STDDEVRANGES;
      P.setelem(0,0,temp1*P1.getelem(0,0) + temp2*P2.getelem(0,0));
      P.setelem(0,1,temp1*P1.getelem(0,1) + temp2*P2.getelem(0,1));
      P.setelem(1,0,temp1*P1.getelem(1,0) + temp2*P2.getelem(1,0));
      P.setelem(1,1,temp1*P1.getelem(1,1) + temp2*P2.getelem(1,1));

      det = 2*PI*(P.getelem(0,0)*P.getelem(1,1)-P.getelem(0,1)*P.getelem(1,0));
      //now, ready to take the probability of this point across whole model...
      probpt = 0;
      //  double model[2*buffer+1][length][2];
      for(int k = 0; k<2*buffer+1; k++) {
	for(int l=0; l<length; l++) {
	  pos.setelem(0,0,model[k][l][0]);
	  pos.setelem(1,0,model[k][l][1]);
	  diff = pos-mu;
          power = diff.transpose()*P.inverse()*diff;
	  temp = pow(det,-.5)*exp(-.5*power.getelem(0,0));
	  probpt += s*s*temp;
	}
      }
      //      cout<<"prob for point "<<j<<" = "<<probpt<<endl;
      probscan = probscan*probpt;
    }
    neIt++;
    rIt++;
    aIt++;
    fp[ns] = probscan/scanSize;
    ns++;
    cout<<"prob for scan = " <<probscan/scanSize<<endl;
  }

  //now, testing if this is moving
  if(fp[0]>0 && fp[1]>0 &&fp[2]>0 && fp[3]>0) {
    if((fp[0]/fp[1]>1.5) && (fp[1]/fp[2]>1.5) && (fp[2]/fp[3]>1.5)) {
      cout<<"THIS OBJ IS MOVING!!!"<<endl;
      obj2car(i);
    }
  }
#endif
#ifdef CHECK_MOTION_KF_VEL
  
  double velN, velE, vel;
  velN = objects[index].Nmu.getelem(1,0);
  velE = objects[index].Emu.getelem(1,0);
  vel = sqrt(velN*velN+velE*velE);
  if(vel > MINSPEED) {
    cout<<"THIS OBJ IS MOVING!!!"<<endl;
    obj2car(i);
  }
  
#endif
}
}
} //end checkMotion

//goes through and sends each car (vel is now checked BEFORE obj turned into car)
void trackMO::sendObjects() {
  //  cout<<"entering sendObjects()"<<endl;
  //  cout<<"number of cars: "<<usedCars.size()<<endl;

  movingObstacle obstacle;
  double de1, de2, dn1, dn2;

  int sent = 0;
  for(unsigned int k=0; k<usedCars.size(); k++) {
    int j = usedCars.at(k);

    int ptr = cars[j].histPointer; //the first place seen (if > 9 observations)
    //#warning "do we want any more conditions other than speed when sending cars?"
    //    #warning "this shouldn't stay...only put here so that I get good debugging file" 

    sent++;
    obstacle.ID = cars[j].ID;
    obstacle.status=2;

    //#warning "FIXME: actually put history into this message"
    for(int i = 0; i<10; i++){
      obstacle.cornerPos[i].N = cars[j].lastN[(ptr-1)%10]; 
      obstacle.cornerPos[i].E = cars[j].lastE[(ptr-1)%10]; 
    }

    //sides for whole history are going to be the same
    dn1 = cars[j].dn1;
    de1 = cars[j].de1;
    dn2 = cars[j].dn2;
    de2 = cars[j].de2;
    double Nest, Eest;
    Nest = cars[j].Nmu.getelem(0,0);
    Eest = cars[j].Emu.getelem(0,0);
    //#warning "may want to 'grow' objects here, a little (not long/wide enuf, looking @ gui)"
    obstacle.side1.N = dn1;
    obstacle.side1.E = de1;
    obstacle.side2.N = dn2;
    obstacle.side2.E = de2;

    obstacle.vel.N = cars[j].Nmu.getelem(1,0);
    obstacle.vel.E = cars[j].Emu.getelem(1,0);

    DGCgettime(obstacle.timeStamp);

    //      cout<<endl<<"Sending car # "<<j<<endl;
    //      cout<<"position and velocity: ";
    //      cout<<cars[j].lastN[(ptr-1)%10]-m_state.Northing<<' '<<cars[j].lastE[(ptr-1)%10]-m_state.Easting<<' ';
    //      cout<<cars[j].velN<<' '<<cars[j].velE<<endl;
    //      cout<<"sides(N,E): "<<dn1<<' '<<de1<<' '<<dn2<<' '<<de2<<endl;
    //      cout<<"age (numpoints, last seen): "<<cars[j].histPointer<<' '<<obstacle.timeStamp-cars[j].lastSeen<<endl;

    int msgSize = sizeof(movingObstacle);
    m_skynet.send_msg(m_send_socket, &obstacle, msgSize, 0);
#warning "we know that cars[j].{lastN,lastE} aren't up-to-date...fix me!!"
    m_outputSent<<Nest<<' '<<Eest<<' '<<Nest+dn1<<' '<<Eest+de1<<' '
		<<Nest+dn2<<' '<<Eest+de2<<' '<<Nest+dn1+dn2<<' '<<Eest+de1+de2<<' ';
    m_outputSentSegs<<cars[j].segStart<<' '<<cars[j].segEnd<<' ';

  } // end checking each car

  //make the output file's lines all the same length
  for(int k=sent; k<=MAXNUMSENT; k++) {
    m_outputSent<<"-1 -1 -1 -1 -1 -1 -1 -1 ";
    m_outputSentSegs<<"-1 -1 ";
  }
  m_outputSent<<endl;
  m_outputSentSegs<<endl;

  //  cout<<"exiting sendObjects()"<<endl;
  return;
}




/**
 * function: fitCar
 * input: takes in set of NE points that have been grouped together as an object,
 *   and finds the two lines that fit them best (think rectangle)
 * output: parameters describing this rectangle
 **/

//#warning "should add idea of uncertainty to position of car!"
int trackMO::fitCar(double* points, int numPoints, carRepresentation* newCar, double vN, double vE) {
  int corner = 0;
  //  cout<<"entering fitCar with "<<numPoints<<" points"<<endl;

  double minerror, MSE1, MSE2;
  int breakpoint;
  //calculating w/out corner, or new best fit
  double temp1[6];
  double temp2[6];
  //calculating corner
  double temp3[6];
  double temp4[6];

  //returns the fit in temp 1 w/ format {a, b, n1, e1, n2, e2}
  minerror = fitLine(points, numPoints, temp1);

  //figuring which corner to add (see p. 60 for docs)
  //want to get points relative to Alice's position
  double relPoints[2*numPoints];
  for(int i=0; i<numPoints; i++) {
    relPoints[2*i] = points[2*i] - m_state.Easting;
    relPoints[2*i+1] = points[2*i+1] - m_state.Northing;
  }

  //  fitLine(relPoints, numPoints, temp3);
  //now, adding a seg that points away from alice (see p.88)
  double th;
  th = atan2(temp1[4]-temp1[2], temp1[5]-temp1[3]); //slope of line
  double n,e;
#warning "the signs on these are going to be incorrect in some cases..."
  n = temp1[2] - MINDIMENSION * cos(th);
  e = temp1[3] - MINDIMENSION * sin(th);
  //creates the second line segment, in global coords
#warning "need to set a & b properly!!"
#warning "if we actually use this, then  need to change dn2, de2 to reflect that..."
  temp2[0] = 0;
  temp2[1] = 0;
  temp2[2] = temp1[2];
  temp2[3] = temp1[3];
  temp2[4] = n;
  temp2[5] = e;

  //now, temp1 and temp2 are the line segments, if no corner is found
  //need to see if there's a better fit, w/ a corner

  //now, find min error, but requiring each leg to have at 
  //least 2 points, and each leg contains the breakpoint
  for(int i=2; i<numPoints-2; i++) {
    double points1[2*i];
    double points2[2*(numPoints-i+1)];

    for(int j=0; j<2*i; j++) {
      points1[j] = points[j];
    }
    for(int j=0; j<2*(numPoints-i+1); j++) {
      points2[j] = points[j+2*(i-1)];
    }

    MSE1 = fitLine(points1, i, temp3);
    MSE2 = fitLine2(points2, numPoints-i+1, temp4, temp3[1]);

    //    cout<<"breaking at point "<<i<<" w/ minerror, MSE1, MSE2: "<<minerror<<' '<<MSE1<<' '<<MSE2<<endl;

    //if we've reached a new minimum error, this is our current best-fit, and
    //should be copied to temp1 & temp2
   if(minerror > (MSE1+ MSE2)) {
     //     cout<<"new min error!!!"<<endl;
      minerror = MSE1 + MSE2;
      breakpoint = i;
      memcpy(temp1, temp3, 6*sizeof(double));
      memcpy(temp2, temp4, 6*sizeof(double));
      corner = 1;
    }
  }

  //  cout<<"corner posns for seg1: "<<temp1[2]<<' '<<temp1[3]<<' '<<temp1[4]<<' '<<temp1[5]<<endl;
  //  cout<<"corner posns for seg2: "<<temp2[2]<<' '<<temp2[3]<<' '<<temp2[4]<<' '<<temp2[5]<<endl;



  //now, need to get these two segments into the format of a car
  //  cout<<"tryign to orient deltas properly...vels are: "<<vN<<' '<<vE<<endl;
  //calculating their intersection (to get the N,E points we define car as)
  double ni, ei; //northing and easting intersections
  double dn1, de1, dn2, de2; //deltas for segs
  if(temp2[0] ==0 && temp2[1] == 0) {
    ni = temp1[2];
    ei = temp1[3];
  } else {
    ei = (temp1[0] - temp2[0])/(temp1[1] - temp2[1]);
    ni = temp1[0] + temp1[1]*ei;
  }
  dn1 = temp1[4] - ni; //seg 1 deltas
  de1 = temp1[5] - ei;
  dn2 = temp2[4] - ni; //seg 2 deltas
  de2 = temp2[5] - ei;
  //  cout<<"initial corner pos and deltas: "<<ni<<' '<<ei<<' '<<dn1<<' '<<de1<<' '<<dn2<<' '<<de2<<endl;
  //  cout<<"input velocity: "<<vN<<' '<<vE<<endl;
  newCar->n = ni; //corner coords
  newCar->e = ei;
  newCar->dn1 = dn1; //seg 1 deltas
  newCar->de1 = de1;
  newCar->dn2 = dn2; //seg 2 deltas
  newCar->de2 = de2;



  //now, making sure the corner is in the right position (using vE and vN) see p.85 - 87
  double thv, th1, th2; //velocity and segment angles
  double tn, te; //temp holders for switching values
  thv = atan2(vN,vE);
  th1 = atan2(dn1, de1);
  th2 = atan2(dn2, de2);
  cout<<"angles (vel, 1, 2): "<<thv<<' '<<th1<<' '<<th2<<endl;

  //putting ni,ei at back bumper
  if( sin(thv-th1) < sin(thv-th2)) { //(dn1, de1) is parallel direction of velocity
    if(cos(thv-th1) > 0) { //they're oriented the same
      cout<<"//all is good...(n,e) is at back, (dn1, de1) is pointed along velocity"<<endl;
    } else {
      cout<<" //oriented opposite, so need to flip d1"<<endl;
      ni = ni + dn1;
      ei = ei + de1;
      dn1 = -dn1;
      de1 = -de1;
    }
  } else {
    cout<<"//(dn2, de2) is parallel to velocity...need to switch d1 and d2"<<endl;
    tn = dn1;
    te = de1;
    if(cos(thv-th2) > 0) { //same orientation
      cout<<"correct d2 orientation"<<endl;
      // ni,ei are fine, only need to switch 
      dn1 = dn2;
      de1 = de2;
      dn2 = tn;
      de2 = te;
    } else { //opp orientation
      cout<<"need to switch d2 orientation"<<endl;
      ni = ni+dn2;
      ei = ei+dn2;
      dn1 = -dn2;
      de1 = -de2;
      dn2 = tn;
      de2 = te;
    }
  }

  /**
  newCar->n = ni; //corner coords
  newCar->e = ei;
  newCar->dn1 = dn1; //seg 1 deltas
  newCar->de1 = de1;
  newCar->dn2 = dn2; //seg 2 deltas
  newCar->de2 = de2;
  **/

  //putting ni,ei at RIGHT side of back bumper (i.e. th2 and thv+PI/2 are in same dir)
  th2 = atan2(dn2, de2); //need to recalculate...
  if(cos(thv + PI/2 - th2) > 0) { //they're in same direction
    //everything is set up properly
  } else { //need to switch
    ni = ni+dn2;
    ei = ei+de2;
    dn2 = -dn2;
    de2 = -de2;
  }

  //WHEW. car is set up correctly
  //  cout<<"fit a car, w/ params: "<<ni<<' '<<ei<<' '<<dn1<<' '<<de1<<' '<<dn2<<' '<<de2<<endl;
  /**
  newCar->n = ni; //corner coords
  newCar->e = ei;
  newCar->dn1 = dn1; //seg 1 deltas
  newCar->de1 = de1;
  newCar->dn2 = dn2; //seg 2 deltas
  newCar->de2 = de2;
  **/


  return corner;
} //end fitCar

/** 
 *Function: uses the method of least-squares to fit a line of the form y = a + b * x
 * to the input points, then transforms that to a line of form [alpha, rho, r1, r2]
 * uses eqns found on mathworld.wolfram.com
 * returns: the r-squared value of the fit, and changes the data in fit
 **/

double trackMO::fitLine(double* points, int numPoints, double* fit) {

  //  cout<<"entering fitLine"<<endl;

  double a,b; //vars for least-squares fit

  //compute mean e and n coordinates
  double e, n;
  double eerr, nerr, therr, err;

  double ssee = 0;
  double ssnn = 0;
  double ssen = 0;

  double ebar = 0;
  double nbar = 0;
  //  cout<<"entered fit line"<<endl;

  for(int i=0;i<numPoints;i++) {
    e = points[2*i];
    n = points[2*i + 1];
//  cout<<n<<' '<<e<<' ';

    ebar += e;
    nbar += n;

    ssee += e*e;
    ssnn += n*n;
    ssen += e*n;

  }
//  cout<<endl;

  ebar = ebar/numPoints; 
  nbar = nbar/numPoints;

  //  cout<<"mean n,e "<<nbar<<", "<<ebar<<endl;

  ssee = ssee - numPoints*ebar*ebar;
  ssnn = ssnn - numPoints*nbar*nbar;
  ssen = ssen - numPoints*ebar*nbar;

  b = ssen/ssee;
  a = nbar - b*ebar;

  //now, need to have some measure of the error
  //p. 59 explains this calculation
  double SE = 0;
  for(int i=0; i<numPoints;i++) {
    e = points[2*i];
    n = points[2*i + 1];
    nerr = n - a - b*e;
    eerr = e - (n-a)/b;
    therr = atan2(nerr,eerr);
    err = fabs(eerr*sin(therr));
    SE += err*err;
  }

  //vars for chosen representation
  double theta, rho;
  double e1, n1, e2, n2,  ei, ni;
  if(a<0) { //they're both pos, or both negative (see p. 36 of notebook)
    theta = atan2(b,1)+3*PI/2; //angle of line perpendicular to one with slope b
  }  else {
    theta = atan2(b,1)+PI/2;
  }

  //now, need to find intersection pt of y = a+b*e and y=-(1/b)*e ... 
  //this is at x = -a*b/(b*b+1), and y = a/(b*b+1);
  ei = a/(tan(theta)-b);
  ni = a+a*b/(tan(theta)-b);
  rho = sqrt(ei*ei+ni*ni);


  //technically, r1 and r2 should be the 1st and last entry in the array 
  //projected onto the line defined by theta and rho....
  //for now, I'm going to be lazy, and just define it as the distance
//#warning "HACK!!"
  e1 = points[0];
  n1 = points[1];
  e2 = points[2*numPoints-2];
  n2 = points[2*numPoints-1];
  //  cout<<"and, endpoints of the segment: "<<n1<<' '<<e1<<' '<<n2<<' '<<e2<<endl;

  //need to project these points onto the line (theta, rho) see p.55
  double ai, bi, ci, di; //parameters for calculating intersect
  double pn1, pe1, pn2, pe2; //projected points
  ai = n1 - e1 * tan(theta);
  bi = tan(theta);
  ci = rho / cos(theta - PI/2);
  di = tan(PI/2 + theta);

  pe1 = (ai - ci)/(di - bi);
  pn1 = ai + bi*pe1;

  ai = n2 - e2 * tan(theta);

  pe2 = (ai - ci)/(di - bi);
  pn2 = ai + bi*pe2;

  //  cout<<"projected points: "<<pn1<<' '<<pe1<<' '<<pn2<<' '<<pe2<<endl;

  fit[0] = a;
  fit[1] = b;
  fit[2] = pn1;
  fit[3] = pe1;
  fit[4] = pn2;
  fit[5] = pe2;

  return SE;
}



/** 
 *Function: uses the method of least-squares to fit a line of the form b = a + b * e
 * to the input points, given that it must be perpendicular to line w/ slope given
 * Then, transforms that to a line of form [alpha, rho, r1, r2]
 * uses eqns found on mathworld.wolfram.com
 * returns: the r-squared value of the fit, and changes the data in fit
 **/

double trackMO::fitLine2(double* points, int numPoints, double* fit, double slope) {

  //  cout<<"entering fitline2"<<endl;

  double a,b; //vars for least-squares fit

  //compute mean e and y coordinates
  double eerr, nerr, err;
  double e, n;

  double ebar = 0;
  double nbar = 0;
  for(int i=0;i<numPoints;i++) {
    e = points[2*i];
    n = points[2*i + 1];

    ebar += e;
    nbar += n;
  }

  ebar = ebar/numPoints; 
  nbar = nbar/numPoints;

  //b must be perpendicular to line described by theta (i.e. parallel to theta)
  b = -1/slope; 
  a = nbar - b*ebar;

  //now, need to have some measure of the error
  double SE = 0;
  for(int i=0; i<numPoints;i++) {
    e = points[2*i];
    n = points[2*i + 1];
    nerr = n - a - b*e;
    eerr = e - (n-a)/b;
    err = sqrt(nerr*nerr + eerr*eerr);
    SE += err*err;
  }

  //vars for chosen representation
  double theta, rho;
  double e1, n1, e2, n2,  ei, ni;
  if(a<0) { //they're both pos, or both negative (see p. 36 of notebook)
    theta = atan2(b,1)+3*PI/2; //angle of line perpendicular to one with slope b
  }  else {
    theta = atan2(b,1)+PI/2;
  }

  //now, need to find intersection pt of y = a+b*x and y=-(1/b)*x ... 
  //this is at x = -a*b/(b*b+1), and y = a/(b*b+1);
  ei = a/(tan(theta)-b);
  ni = a+a*b/(tan(theta)-b);
  rho = sqrt(ei*ei+ni*ni);

  //technically, r1 and r2 should be the 1st and last entry in the array 
  //projected onto the line defined by theta and rho....
  //for now, I'm going to be lazy, and just define it as the distance
//#warning "HACK!!"
  e1 = points[0];
  n1 = points[1];
  e2 = points[2*numPoints-2];
  n2 = points[2*numPoints-1];

  //need to project these points onto the line (theta, rho) see p.55
  double ai, bi, ci, di; //parameters for calculating intersect
  double pn1, pe1, pn2, pe2; //projected points
  ai = n1 - e1 * tan(theta);
  bi = tan(theta);
  ci = rho / cos(theta - PI/2);
  di = tan(PI/2 + theta);

  pe1 = (ai - ci)/(di - bi);
  pn1 = ai + bi*pe1;

  ai = n2 - e2 * tan(theta);

  pe2 = (ai - ci)/(di - bi);
  pn2 = ai + bi*pe2;

  fit[0] = a;
  fit[1] = b;
  fit[2] = pn1;
  fit[3] = pe1;
  fit[4] = pn2;
  fit[5] = pe2;

  return SE;
}


/**
 * function that finds the perpendicular distance between two lines, the
 * first described by two end points, the next by theta and rho
 *
 * see p.55 for an explanation
 **/
double trackMO::perpDist(double e1, double e2, double n1, double n2, double theta, double rho) {

  //  cout<<"entering perpDist"<<endl;

  double a, b, c, d, ei, ni, dist1, dist2, avgdist;

  a = n1 - e1*tan(theta);
  b = tan(theta);
  c = rho/cos(PI/2-theta);
  d = tan(PI/2+theta);

  ei = (a-c)/(d-b);
  ni = a+b*ei;

  dist1 = dist(e1,n1,ei,ni);
 
  a = n2 - e2*tan(theta);
  b = tan(theta);
  c = rho/cos(PI/2-theta);
  d = tan(PI/2+theta);

  ei = (a-c)/(d-b);
  ni = a+b*ei;

  dist2 = dist(e2,n2,ei,ni);
  avgdist = (dist1+dist2)/2;

  //  cout<<"ei, ni: "<<ei<<' '<<ni<<"  e1, n1: "<<e1<<' '<<n1<<"  e2, n2: "<<e2<<' '<<n2<<endl;

  return avgdist; 
}

#warning "this code should remove the object at index, in addition to making a new car...possibly replace with addCar(&object), and removeObject(index)"
void trackMO::obj2car(int index) {
  cout<<"trying to turn an object into a car"<<endl;
  if(usedCars.size() < MAXNUMCARS-1) { 
    int i = usedObjs.at(index);
  vector<NEcoord> points(objects[i].scans.back());

  int temp;
  temp = openCars.back();
  openCars.pop_back();
  usedCars.push_back(temp);

 int numPoints = points.size();
 //  cout<<"this has "<<numPoints<<" points..."<<endl;

  double pts[2*numPoints];
  for(int i=0; i<numPoints; i++) {
    pts[2*i] = points.at(i).E;
    pts[2*i+1] = points.at(i).N;
  }

  int corner;
  corner = fitCar(pts,numPoints,&cars[temp],objects[i].Nmu.getelem(1,0), objects[i].Emu.getelem(1,0)); 

  cars[temp].Nmu.resetSize(2,1);
  cars[temp].Emu.resetSize(2,1);
  cars[temp].Nsigma.resetSize(2,2);
  cars[temp].Esigma.resetSize(2,2);
  cars[temp].Nmu_hat.resetSize(2,1);
  cars[temp].Emu_hat.resetSize(2,1);
  cars[temp].Nsigma_hat.resetSize(2,2);
  cars[temp].Esigma_hat.resetSize(2,2);

  double NMelems[] = {cars[temp].n, objects[i].Nmu.getelem(1,0)};
  double EMelems[] = {cars[temp].e, objects[i].Emu.getelem(1,0)};
  double NSelems[] = {0,0,0,0};
  double ESelems[] = {0,0,0,0};

  //	  cout<<"setting matrix mu-naught to: "<<cars[temp].N<<" and "<<cars[temp].E<<endl;

  cars[temp].Nmu.setelems(NMelems);
  cars[temp].Nsigma.setelems(NSelems);
  cars[temp].Emu.setelems(EMelems);
  cars[temp].Esigma.setelems(ESelems);

  cars[temp].ID = numCars;
  
  numCars++; //we've found a car

  removeObject(index);

  } else {
    cerr<<"not enough space in cars array!!"<<endl;
  }
  cout<<"exiting obj2car()"<<endl;
}

//removes object by putting its index back on openObjs, and removing 
//the index from usedObjs
void trackMO::removeObject(int index) {
  openObjs.push_back(usedObjs.at(index));
  objIterator = usedObjs.begin();
  objIterator+=index;
  usedObjs.erase(objIterator);

}

/**
 * This function is called when it is determined that a new object
 * has no match in already seen objects or cars
 * It's job is to add an object to the object array, if there is room.
 **/
void trackMO::addObject(objectRepresentation* newObj) {

  if(usedObjs.size() < MAXNUMCARS) {

    int temp;
    temp = openObjs.back();
    openObjs.pop_back();
    usedObjs.push_back(temp);

    objects[temp].N = newObj->N;
    objects[temp].E = newObj->E;
    objects[temp].dStart = newObj->dStart;
    objects[temp].dEnd = newObj->dEnd;

    objects[temp].timesSeen = 1;
    objects[temp].lastSeen = numFrames;

    //transferring vectors of scan/range/angle history
    vector<NEcoord> newscans(newObj->scans.back());
    vector<double> newranges(newObj->ranges.back());
    vector<double> newangles(newObj->angles.back());

    objects[temp].scans.push_back(newscans);
    objects[temp].ranges.push_back(newranges);
    objects[temp].angles.push_back(newangles);
 
    //initizlizing KF variables
    objects[temp].Nmu.resetSize(2,1);
    objects[temp].Emu.resetSize(2,1);
    objects[temp].Nsigma.resetSize(2,2);
    objects[temp].Esigma.resetSize(2,2);
    objects[temp].Nmu_hat.resetSize(2,1);
    objects[temp].Emu_hat.resetSize(2,1);
    objects[temp].Nsigma_hat.resetSize(2,2);
    objects[temp].Esigma_hat.resetSize(2,2);

    double NMelems[] = {objects[temp].N, 0};
    double EMelems[] = {objects[temp].E, 0};
    double NSelems[] = {1,0,0,1};
    double ESelems[] = {1,0,0,1};
    //    cout<<"when adding new object, N is: "<<objects[temp].N<<endl;
    objects[temp].Nmu.setelems(NMelems);
    objects[temp].Nsigma.setelems(NSelems);
    objects[temp].Emu.setelems(EMelems);
    objects[temp].Esigma.setelems(ESelems);
    //    cout<<" and Nmu is: "<<objects[temp].Nmu.getelem(0,0)<<endl;

  } else {
    cerr<<"trying to add obj, no space in buffer"<<endl;

  } //end checking that there's enough room to add new object
} //end addObject


/**
 * This function is called when it is determined that a newobject 
 * matches an old one. As such, it simply replaces the center position, 
 * and adds the latest scan/ranges data
**/
void trackMO::updateObject(objectRepresentation* oldObj, objectRepresentation* newObj) {

  oldObj->N = newObj->N;
  oldObj->E = newObj->E;

  oldObj->dStart = newObj->dStart;
  oldObj->dEnd = newObj->dEnd;

  vector<NEcoord> newscans(newObj->scans.back());
  vector<double> newranges(newObj->ranges.back());
  vector<double> newangles(newObj->angles.back());

  oldObj->scans.push_back(newscans);
  oldObj->ranges.push_back(newranges);
  oldObj->angles.push_back(newangles);

  oldObj->timesSeen = oldObj->timesSeen + 1;
  oldObj->lastSeen = numFrames;

}

/**
 * this function is called when it is determined that a new object
 * matches an already seen car.
 **/
//#warning "FIXME: this update is very incomplete -- only replaces old data, doesn't merge at all"
//#warning "At this point, simply replaces Car w/ car formed by Points...add in sam's update rules"
void trackMO::updateCar(carRepresentation* Car, objectRepresentation* newObj) {

  //  cout<<"entering updateCar"<<endl;
  vector<NEcoord> points(newObj->scans.back());
  double dStart = newObj->dStart;
  double dEnd = newObj->dEnd;

  int numPoints = points.size();

  double pts[2*numPoints];
  for(int i=0; i<numPoints; i++) {
    pts[2*i] = points.at(i).E;
    pts[2*i+1] = points.at(i).N;
  }
  //  cout<<"points in object: "<<pts[0]<<' '<<pts[1]<<' '<<pts[2*numPoints-2]<<' '<<pts[2*numPoints-1]<<endl;
  //  cout<<"center of object: "<<newObj->N<<' '<<newObj->E<<endl;
  carRepresentation newCar;
  //  cout<<"updating car #"<<Car->ID<<" at "<<Car->n<<' '<<Car->e<<endl;
  fitCar(pts,numPoints,&newCar, Car->Nmu.getelem(1,0), Car->Emu.getelem(1,0)); 

  m_outputTest<<newCar.n<<' '<<newCar.e<<' '<<newCar.dn1<<' '<<newCar.de1<<' '<<newCar.dn2<<' '<<newCar.de2<<' ';

  //  cout<<"Params for old car: "<<Car->n<<' '<<Car->de1<<' '<<Car->dn1<<' '<<Car->e<<' '<<Car->dn2<<' '<<Car->de2<<endl;
  //  cout<<"Params for new car: "<<newCar.n<<' '<<newCar.de1<<' '<<newCar.dn1<<' '<<newCar.e<<' '<<newCar.dn2<<' '<<newCar.de2<<endl;

  /**
   * depending on how much of the car we see in the old measurement, update
   * the old car (if we see the corner, that carries more information than 
   * seeing only part of a segment. see p. 86-87
   * we determine how much of car we saw by occlusions
   **/
  int sawFront = 0; //whether dStart&dEnd say we actually saw the boundary of car
  int sawBack = 0;

  //need angles to front/back of car (rel to alice), to determine how they 
  //correspond to dStart&dEnd. will be in range of 0-PI (how ladar works)
  double th1, th2;
#warning "for here (and elsewhere) will need to add yaw when using real data"
  double foo1, foo2, bar1, bar2;
  foo1 = Car->n - m_state.Northing;
  foo2 = Car->e - m_state.Easting;
  bar1 = Car->n + Car->dn1 - m_state.Northing;
  bar2 = Car->e + Car->de1 - m_state.Easting;
  cout<<"deltas: "<<foo1<<' '<<foo2<<' '<<bar1<<' '<<bar2<<endl;
  th1 = atan2(Car->n - m_state.Northing, Car->e - m_state.Easting);
  th2 = atan2(Car->n + Car->dn1 - m_state.Northing, Car->e + Car->de1 - m_state.Easting);
  //  th1 = atan2(Car->e - m_state.Easting, Car->n - m_state.Northing);
  //  th2 = atan2(Car->e + Car->de1 - m_state.Easting, Car->n + Car->dn1 - m_state.Northing);

  cout<<"thetas: "<<th1<<' '<<th2<<endl;
  if(th1 < th2) { // n,e (back bumper) correspond to start, and d1 (front bumper) correspond to end
    //  if(1==1) { // for now, we're guaranteed that back == start
    if(dStart >= 0) { //start was in foreground
      sawBack = 1;
    }
    if(dEnd >= 0) { //end was in foreground
      sawFront = 1;
    }
  } else { // back bumper corresponds to end
    if(dStart >= 0) {
      sawFront = 1;
    }
    if(dEnd >= 0) {
      sawBack = 1;
    }
  }

  //now that we know whether to trust start/end coords, can calculate updates
  if(sawBack == 1) {
    if(sawFront == 1) {
//      cout<<" //have full information on this car, can entirely replace"<<endl;
      //#warning "replacing isn't the best option here...better to update d's to take into account ALL information"
    double dn, de;
    dn = newCar.n + newCar.dn1 - Car->n - Car->dn1;
    de = newCar.e + newCar.de1 - Car->e - Car->de1;
    Car->n = Car->n + dn;
    Car->e = Car->e + de;
#warning "for some reason, we think we've seen the back when we havent...thus, only using deltas here"
    //      Car->n = newCar.n;
    //      Car->e = newCar.e;
      //      Car->dn1 = newCar.dn1;
      //      Car->de1 = newCar.de1;
      //      Car->dn2 = newCar.dn2;
      //      Car->de2 = newCar.de2;
    } else {
      //    cout<<" //only know back bumper, only update position"<<endl;
      Car->n = newCar.n;
      Car->e = newCar.e;
    }
  } else if (sawFront == 1) {
    //    cout<<"//only know front bumper, only update deltas"<<endl;
    double dn, de;
    dn = newCar.n + newCar.dn1 - Car->n - Car->dn1;
    de = newCar.e + newCar.de1 - Car->e - Car->de1;
    Car->n = Car->n + dn;
    Car->e = Car->e + de;
  }

  //if we were able to update car, update history/log of when seen
  if(sawFront + sawBack > 0) {
    //setting up history vector...
    int histptr = Car->histPointer;

    //  cout<<"setting ptr to "<<histptr<<" plus 1"<<endl;
    Car->histPointer = histptr + 1;
    Car->lastE[histptr%10] = Car->e;
    Car->lastN[histptr%10] = Car->n;

    Car->timesSeen = Car->timesSeen + 1;
    Car->lastSeen = numFrames;
  } else {
    //    cout<<"tried to update car, not enough information"<<endl<<endl;
  }
} //end updateCar


void trackMO::KFupdate() {

  //  cout<<"entering KFupdate"<<endl;

  Matrix K1;
  K1 = Matrix(2,2);
  Matrix K2;
  K2 = Matrix(2,2);
  Matrix Nerr;
  Nerr = Matrix(1,1);
  Matrix Eerr;
  Eerr = Matrix(1,1);
  Matrix tmp;
  tmp = Matrix(1,1);
  Matrix tmp2;
  tmp2 = Matrix(1,1);
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int j = usedObjs.at(i);
    //    cout<<"last seen: "<<objects[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
    if(objects[j].timesSeen > 1) {
      if(objects[j].lastSeen == numFrames) {
        double grr[] = {objects[j].N};
        tmp.setelems(grr);
	//	cout<<"Northing: "<<objects[j].N<<" and, predicted northing: "<<objects[j].Nmu_hat.getelem(0,0)<<endl;
        Nerr = tmp - C*objects[j].Nmu_hat;
	//	cout<<"Nerror: "<<Nerr.getelem(0,0)<<' ';
        K1 = objects[j].Nsigma_hat * C.transpose() * (C*objects[j].Nsigma_hat*C.transpose() + Q).inverse();
        objects[j].Nmu = objects[j].Nmu_hat + K1 * Nerr;
        objects[j].Nsigma = (I - K1*C) * objects[j].Nsigma_hat;

        double grr2[] = {objects[j].E};
        tmp2.setelems(grr2);

        Eerr = tmp2 - C*objects[j].Emu_hat;
	//	cout<<" and, Eerror: "<<Eerr.getelem(0,0)<<endl;
        K2 = objects[j].Esigma_hat * C.transpose() * (C*objects[j].Esigma_hat*C.transpose() + Q).inverse();
        objects[j].Emu = objects[j].Emu_hat + K2 * Eerr;
        objects[j].Esigma = (I - K2*C) * objects[j].Esigma_hat;
	//	cout<<"new vel (actually calculated) "<<objects[j].Nmu.getelem(1,0)<<' '<<objects[j].Emu.getelem(1,0)<<endl;

      } else {

	objects[j].Nmu = objects[j].Nmu_hat;
	objects[j].Nsigma = objects[j].Nsigma_hat;
	objects[j].Emu = objects[j].Emu_hat;
	objects[j].Esigma = objects[j].Esigma_hat;

      }

    }
  }


  for(unsigned int i=0; i<usedCars.size(); i++) {
    //    cout<<"entering loop w/ i = "<<i<<endl;
    int j = usedCars.at(i);
    //    cout<<"last seen: "<<cars[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
    if(cars[j].timesSeen > 1) {
      if(cars[j].lastSeen == numFrames) {

	double grr[] = {cars[j].n};
	tmp.setelems(grr);

	//	cout<<"line 2239"<<endl;
        Nerr = tmp - C*cars[j].Nmu_hat;
	//	cout<<"Nerr for car: "<<Nerr.getelem(0,0)<<endl;
        K1 = cars[j].Nsigma_hat * C.transpose() * (C*cars[j].Nsigma_hat*C.transpose() + Q).inverse();
	//	cout<<"line 2242"<<endl;
        cars[j].Nmu = cars[j].Nmu_hat + K1 * Nerr;
	//	cout<<"new n,ndot: "<<cars[j].Nmu.getelem(0,0)<<' '<<cars[j].Nmu.getelem(1,0)<<endl;
        cars[j].Nsigma = (I - K1*C) * cars[j].Nsigma_hat;

	//	cout<<"ln 2246"<<endl;
	double grr2[] = {cars[j].e};
	tmp2.setelems(grr2);

	//	cout<<"ln 2250"<<endl;
        Eerr = tmp2 - C*cars[j].Emu_hat;
        K2 = cars[j].Esigma_hat * C.transpose() * (C*cars[j].Esigma_hat*C.transpose() + Q).inverse();
	//	cout<<"ln 2253"<<endl;
        cars[j].Emu = cars[j].Emu_hat + K2 * Eerr;
        cars[j].Esigma = (I - K2*C) * cars[j].Esigma_hat;

      } else {
	//	cout<<"ln 2258"<<endl;
	cars[j].Nmu = cars[j].Nmu_hat;
	cars[j].Nsigma = cars[j].Nsigma_hat;
	//	cout<<"ln 2261"<<endl;
	cars[j].Emu = cars[j].Emu_hat;
	//	cout<<"ln 2263"<<endl;
	cars[j].Esigma = cars[j].Esigma_hat;
	//	cout<<"ln 2264"<<endl;
      }
    }
  }

  //  cout<<"exiting KFupdate"<<endl;
}
 
void trackMO::KFpredict() {

  //  cout<<"entering KFpredict()"<<endl;

  for(unsigned int i=0; i<usedObjs.size() ; i++) {
    int j = usedObjs.at(i);

    objects[j].Nmu_hat =    A*objects[j].Nmu;
    objects[j].Nsigma_hat = A*objects[j].Nsigma*A.transpose() + R;
    objects[j].Emu_hat =    A*objects[j].Emu;
    objects[j].Esigma_hat = A*objects[j].Esigma*A.transpose() + R;
    m_outputObjs<<objects[j].Nmu.getelem(0,0)<<' '<<objects[j].Emu.getelem(0,0)<<' '<<objects[j].Nmu_hat.getelem(0,0)<<' '<<objects[j].Emu_hat.getelem(0,0)<<' '<<objects[j].Nmu.getelem(1,0)<<' '<<objects[j].Emu.getelem(1,0)<<' ';
  } //end cycling through numObjects

  for(unsigned int i=usedObjs.size(); i<MAXNUMCARS; i++) {
    m_outputObjs<<"-1 -1 -1 -1 -1 -1 ";
  }
  m_outputObjs<<endl;


  //and, objcars

 for(unsigned int i=0; i<usedCars.size(); i++) {
    int j = usedCars.at(i);

    //    cout<<"ln 2300"<<endl;
    cars[j].Nmu_hat =    A*cars[j].Nmu;
    cars[j].Nsigma_hat = A*cars[j].Nsigma*A.transpose() + R;
    //    cout<<"ln2303"<<endl;
    cars[j].Emu_hat =    A*cars[j].Emu;
    cars[j].Esigma_hat = A*cars[j].Esigma*A.transpose() + R;
 } //end cycling through numCars

} //end KFpredict()





/**
 * This is the function that allows trackMO to receive LADAR
 * data from the ladarFeeder. 
 *
 * We read in the NEDcoord that's send, and store it in our
 * local NEcoord newPoints[]
 *
 * At this point, all of the trackMO code is event driven by
 * when we receive a scan... 
 **/
void trackMO::Comm()
{
  int scanSocket;
  int bytesReceived;
  scanSocket = m_skynet.listen(SNbumperLadarNED, SNladarfeeder);
  char * m_pDataBuffer;

  int bytesToReceive = NUMSCANPOINTS*sizeof(NEDcoord);
  m_pDataBuffer = new char[bytesToReceive];

  NEDcoord* dataReceived;

  while(1==1)
    {
      bytesReceived = m_skynet.get_msg(scanSocket, m_pDataBuffer, bytesToReceive, 0);
      dataReceived = (NEDcoord*)m_pDataBuffer;

      if(bytesReceived != bytesToReceive)
	{
	  cerr << "trackMO::Comm(): skynet error" << endl;
	}

      //now, need to copy data into newPoints
      for(int i=0;i<NUMSCANPOINTS; i++) {
	newPoints[i].E=dataReceived[i].E;
	newPoints[i].N=dataReceived[i].N;
      }

      //outputting data in a format that matlab can read in
      for(int i=0;i<NUMSCANPOINTS;i++) {
	m_outputPoints<<newPoints[i].E<<' ';
      }
      m_outputPoints<<endl;
      for(int i=0;i<NUMSCANPOINTS;i++) {
	m_outputPoints<<newPoints[i].N<<' ';
      }
      m_outputPoints<<endl;

      processNewScan();
    }
}


/**
 * This is the function that allows trackMO to read in LADAR
 * scans from a text file, simplifying some testing.
 * like Comm(), this function drives all the rest..
 **/
void trackMO::ReadFile()
{
  //input string handling borrowed from traj.cc
  ifstream inputstream(datafile);
  int dist;
#ifdef USE_STATE_FOR_READING
  UpdateState();
#endif
  int count = 0;
  int index = 0;

  DGCgettime(m_timeBegin);

  while(inputstream) {

    /** If we've read in the equivalent of a new scan, send it off, 
     * reset index, and get the next character **/
    if(count%NUMSCANPOINTS == 0 && count > 0) {
//#warning "do I ever do anything w/ this data, or is it thrown out???"
      inputstream>>dist;

      for(int i=0; i<NUMSCANPOINTS; i++) {
	double angle = PI*i/NUMSCANPOINTS;

#ifdef USE_STATE_FOR_READING
	//raw range file is in cm, need to convert to m
        newPoints[i].N = .01*ranges[i]*sin(angle) + m_state.Northing;
        newPoints[i].E = .01*ranges[i]*cos(angle) + m_state.Easting;
#else
        newPoints[i].N = .01*ranges[i]*sin(angle);
        newPoints[i].E = .01*ranges[i]*cos(angle);
#endif

      }

      //outputting data in a format that matlab can load
      for(int i=0;i<NUMSCANPOINTS;i++) {
        m_outputPoints<<newPoints[i].E<<' ';
      }
      m_outputPoints<<endl;
      for(int i=0;i<NUMSCANPOINTS;i++) {
        m_outputPoints<<newPoints[i].N<<' ';
      }
      m_outputPoints<<endl;

      processNewScan();

      DGCgettime(m_timeEnd);
      m_timeDiff = m_timeEnd - m_timeBegin;

      //      cout<<"time elapsed: "<<m_timeDiff<<" goal time: "<<100000/75<<endl;
 
      if(m_timeDiff<100000/75) //If we're faster than 75Hz
        usleep(100000/75 - m_timeDiff);

      DGCgettime(m_timeBegin);

      index = 0;
    }
    else
      inputstream>>dist;
    
    //8191 is the no-data value
    /** trying to make diff measurements better  
   if(dist==8191)
      ranges[index] = 0;
      else **/
      ranges[index]=dist; 


    index++;
    count++;


  } // end while(inputstream)

  cout<<"program finished, no more inputstream"<<endl;
  return;
}


void trackMO::setupLogs() {

  //setup log files
  char pointsFileName[256];
  sprintf(pointsFileName, "points.dat");
   if(m_outputPoints.is_open()) 
    {
      m_outputPoints.close();
    }
  m_outputPoints.open(pointsFileName);
  m_outputPoints << setprecision(20);

  char segmentsFileName[256];
  sprintf(segmentsFileName, "segments.dat");
   if(m_outputSegments.is_open()) 
    {
      m_outputSegments.close();
    }
  m_outputSegments.open(segmentsFileName);
  m_outputSegments << setprecision(20);

  char carsPFileName[256];
  sprintf(carsPFileName, "carsP.dat");
   if(m_outputCarsP.is_open()) 
    {
      m_outputCarsP.close();
    }
  m_outputCarsP.open(carsPFileName);
  m_outputCarsP << setprecision(20);

  char carsCFileName[256];
  sprintf(carsCFileName, "carsC.dat");
   if(m_outputCarsC.is_open()) 
    {
      m_outputCarsC.close();
    }
  m_outputCarsC.open(carsCFileName);
  m_outputCarsC << setprecision(20);


  char sentFileName[256];
  sprintf(sentFileName, "sent.dat");
   if(m_outputSent.is_open()) 
    {
      m_outputSent.close();
    }
  m_outputSent.open(sentFileName);
  m_outputSent << setprecision(20);

  char sentObjsFileName[256];
  sprintf(sentObjsFileName, "sentObjs.dat");
   if(m_outputSentObjs.is_open()) 
    {
      m_outputSentObjs.close();
    }
  m_outputSentObjs.open(sentObjsFileName);
  m_outputSentObjs << setprecision(20);

  char sentSegsFileName[256];
  sprintf(sentSegsFileName, "sentSegs.dat");
   if(m_outputSentSegs.is_open()) 
    {
      m_outputSentSegs.close();
    }
  m_outputSentSegs.open(sentSegsFileName);
  m_outputSentSegs << setprecision(20);

  char testFileName[256];
  sprintf(testFileName, "test.dat");
   if(m_outputTest.is_open()) 
    {
      m_outputTest.close();
    }
  m_outputTest.open(testFileName);
  m_outputTest << setprecision(20);

  char objsFileName[256];
  sprintf(objsFileName, "objs.dat");
   if(m_outputObjs.is_open()) 
    {
      m_outputObjs.close();
    }
  m_outputObjs.open(objsFileName);
  m_outputObjs << setprecision(20);

}
