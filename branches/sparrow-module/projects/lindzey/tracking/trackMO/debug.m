function debug()

points = load('points.dat');
cars = load('cars.dat');
segments = load('segments.dat');
[foo,segrow] = size(segments);

x1 = [];
x2 = [];
y1 = [];
y2 = [];

[rows,cols] = size(points);
tempcolor = rand(181,3);
%figure(1);

for i=1:rows/2
%for i=1:1
%for i=rows/2:rows/2
    
    pause(.01);
    currmin = segments(i,1);
    currmax = segments(i,2);
    currcount = 1;
hold off;
    temp = rand(1,3);
    for k=1:181
        if k > currmax  % if we've got the next segment
            currcount = currcount + 1;
            currmin = segments(i,2*currcount - 1);
            currmax = segments(i,2*currcount);

        end
        if (currmax - currmin) > 2
%          plot(points(2*i-1,k),points(2*i,k),'.','MarkerFaceColor',temp, 'MarkerEdgeColor',tempcolor(currcount,:));
        end
        hold on;
    end

    x1=[];
    y1=[];
    x2=[];
    y2=[];   
    
    for j=1:100
        if abs(cars(i,4*j)) > 0
           t1 = cars(i,4*j-3); 
           t2 = cars(i,4*j-2); 
           t3 = cars(i,4*j-1);
           t4 = cars(i,4*j);

           plot([t1,t3],[t2,t4],'-mo');
%           plot([t1,t3],[t2,t4],'-mo','MarkerEdgeColor','k','MarkerFaceColor',tempcolor(j,:),'MarkerSize',5);

           %plot(t1,t2,'LineWidth',5,t3,t4,'LineWidth',5);
           x1 = [x1,t1];
           y1 = [y1,t2];
           x2 = [x2,t3];
           y2 = [y2,t4];
        end
    end
    
%    plot(x1,y1,'ro',x2,y2,'go');

    
    hold off;
    axis([398960,399020,3781280,3781340]);
    drawnow
end