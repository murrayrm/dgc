
% define the matricies for the system, so we can plot its response
V_0 = 7;   % speed of the vehicle- meters/sec

% loop gain
K = .0035;
% frequency at which the derivative term rolls off
T_r = 15;
% derivative gain
T_d = 10;
% integral gain
T_i = 25;

run run_gains

save v_7set3.mat
