% controller for V_0 = 10m/s

close all;
clear all;


% define the matricies for the system, so we can plot its response
V_0 = -5;   % speed of the vehicle- meters/sec
L = 3;      % wheel base of the vehicle in meters

A = [ 0 V_0; 0 0 ];
B = [ 0; V_0/L ];        % I want to plot the frequency response of just the
% process, so there isn't going to be any control yet
C = [ 1 0 ];        % Y is the output
D = 0;

proc = ss(A, B, C, D);
process = tf(proc);

% controller

s = tf( 's' );

% loop gain
K = .005;
% frequency at which the derivative term rolls off
T_r = 7;
% derivative gain
T_d = 15;
% integral gain
T_i = 30;

controller = K * (1 + T_d * s / ((s / T_r) + 1) + 1/(T_i * s));
L = process * controller;
closed_loop = L / (1 + L);

%figure;
%margin(L);
%figure;
%rlocus(closed_loop);
%figure;
%nyquist(L);
%figure;
%step(closed_loop);

[cont_num, cont_den] = tfdata(controller, 'v');

[Ac, Bc, Cc, Dc] = tf2ss(cont_num, cont_den);

% convert the continuous time system to a discrete time one
Ts = 0.1;       % follow runs at 10hz..
A = eye( size(Ac) ) + Ts * Ac;
B = Ts * Bc;
C = Cc;
D = Dc;

% transfer functions and ss systems will cause the mat lab file loader in
% follow to segfault
clear s proc process controller L closed_loop;

