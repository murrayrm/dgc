#ifndef LOCATION_H_
#define LOCATION_H_

namespace Mapper
{

class Location
{
public:
	Location(double, double);
	virtual ~Location();
	
	double getXLoc() const;
	double getYLoc() const;
private:
	double x;
	double y;
};

}

#endif /*LOCATION_H_*/
