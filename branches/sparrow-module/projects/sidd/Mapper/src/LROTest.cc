// Test protocol:p
// Create a Segment.
// Create 2 Lanes.
// Populate those lanes w/ road objects.
// Print out the segments (which should cause everything else to print)
#include "RoadObject.hh"
#include "LogicalRoadObject.hh"

#include <iostream>
#include <vector>
using std::vector;

using namespace std;
using namespace Mapper;

int main()
{
	// Create lane boundaries.
	LaneBoundary lb0("Lane Boundary 0", 10.0, 10.0, LaneBoundary::SOLID_WHITE);
	lb0.addLocPoint(15.0, 10.0);
	lb0.addLocPoint(20.0, 10.0);
	lb0.addLocPoint(25.0, 10.0);
	lb0.addLocPoint(30.0, 10.0);
	LaneBoundary lb1("Lane Boundary 1", 10.0, 15.0, LaneBoundary::DOUBLE_YELLOW);
	lb1.addLocPoint(15.0, 15.0);
	lb1.addLocPoint(20.0, 15.0);
	lb1.addLocPoint(25.0, 15.0);
	lb1.addLocPoint(30.0, 15.0);
	LaneBoundary lb2("Lane Boundary 2", 10.0, 20.0, LaneBoundary::SOLID_WHITE);
	lb2.addLocPoint(15.0, 20.0);
	lb2.addLocPoint(20.0, 20.0);
	lb2.addLocPoint(25.0, 20.0);
	lb2.addLocPoint(30.0, 20.0);
	// Create two lanes.
	Lane lane0(0, lb0, lb1);
	Lane lane1(1, lb1, lb2);
	vector<Lane> lanes;
	lanes.push_back(lane0);
	lanes.push_back(lane1);
	// Create a segment.
	Segment seg0(0, lanes);
	
	// Print everything out.
	seg0.print();
}
