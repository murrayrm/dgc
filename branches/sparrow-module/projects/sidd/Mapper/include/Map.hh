#ifndef MAP_H_
#define MAP_H_

#include "RNDF.hh"
#include "LogicalRoadObject.hh"
#include <vector>
using std::vector;

namespace Mapper
{

class Map
{
public:
	/* Basic Constructor */
	Map(vector<Segment>);
	/* RNDF Constructor */
	Map(std::RNDF* RNDFObj);
	/* Default, fixed map constructor */
	Map();
	
	/* Map methods */
	/* Retrieve a segment. */
	Segment getSegment(int) const;
	/* Retrieve all segments */
	vector<Segment> getAllSegs() const;
	/* Pretty print the map. */
	void print() const;
	/* Destructor */
	virtual ~Map();
private:
	vector<Segment> segments;
};

}

#endif /*MAP_H_*/
