#ifndef ROADOBJECT_HH_
#define ROADOBJECT_HH_

#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>

namespace Mapper
{

/* Constants */
const double DEFAULT_LANE_WIDTH = 12.0;

/* Location structure */
struct Location
{
	double x;
	double y;	
};

class RoadObject
{
public:
	RoadObject(const string &, double, double);
	RoadObject();
	virtual ~RoadObject();
	
	void setName(const string &);
	string getName() const; // not sure why the const is there
	
	double getXLoc() const;
	double getYLoc() const;
private:
	string name;
	Location loc; 
};

class StopLine : public RoadObject
{
public:
	StopLine(const string &, double, double);
	virtual ~StopLine();
	void print() const;
};

class Checkpoint : public RoadObject
{
public:
	Checkpoint( const string &, double, double );
	virtual ~Checkpoint();
	void print() const;
};

class LaneBoundary : public RoadObject
{
public:
	//TODO This is a public enumerated type.  I don't know if this is good software 
	// engineering practice.
	enum Divider { DOUBLE_YELLOW=0, SOLID_WHITE=1, BROKEN_WHITE=2, ROAD_EDGE=3 };
	
	/* Constructor */
	LaneBoundary(const string &, double, double, Divider);
	/* More useful constructor */
	LaneBoundary(const string &, vector<Location>, Divider);
	/* Empty Constructor */
	LaneBoundary();
	
	/* Destructor */
	virtual ~LaneBoundary();
	
	void setBoundary(Divider);
	Divider getBoundary() const;
	void print() const;
	
	// Accessor functions for the location description.
	void addLocPoint(double, double);
	vector<Location> getCoords() const;
private:
	Divider boundary;
	vector<Location> coords; // Line coordinates represented as a vector of points.	
};

} // End RoadObject.h
#endif /*ROADOBJECT_HH_*/
