#ifndef __MATRIX_H__
#define __MATRIX_H__
#include <fstream>
#include "Polynomial.h"
#include "Vector.h"

using namespace std;

/**A standard mathematical matrix class. */
/**Author: David Rosen */

class Matrix
{
 
protected:

  int rows, columns;
  double* matrix;

 public:

  //Constructors and assignment operator

  /**Default constructor; creates a 1 x 1 matrix initialized to 0. */
  Matrix();

  /**Creates an r x c matrix, initialized to all 0's. */
  Matrix(const int r, const int c);

  /**Creates an order x order matrix, initialized to 0's. */
  Matrix(const int order);

  /**Copy constructor. */
  Matrix(const Matrix& other);

  /**Creates an n x 1 column matrix from v, where v is an n-dimensional vector. */
  Matrix(const Vector& v);

  /**Returns an identity matrix of order "order" */
  static Matrix identity(const int order);

  /**Assignment operator. */
  Matrix& operator = (const Matrix& b);



  //Accessor methods



  /**Returns the number of rows in the matrix. */
  int getRows() const;

  /**Returns the number of columns in the matrix. */
  int getColumns() const;

  /**Returns the order of (*this) (note that the order is only defined for square matrices, so this method requires that (*this) be square). */
  int getOrder() const;

  /**Returns the element in row "row" and column "col". */
  double getElement(const int row, const int col) const;



  //Modifiers and mathematical operators



  /**Sets the element in row "row" and column "col" to value "val". */
  void setElement(const int row, const int col, const double val);

  /**Standard matrix addition. */
  Matrix operator + (const Matrix& b) const;

  /**Standard matrix subtraction, returns (*this) - b. */
  Matrix operator - (const Matrix& b) const;

  /**Scalar multiplication; multiplies every element in the matrix by the constant k. */
  Matrix scalar(const double k) const;

  //Matrix multiplication, return (*this) * b
  Matrix operator * (const Matrix& b) const;

  /**Forms the matrix product (*this) * v, where v is treated as a column matrix.  Useful if you want to treat the matrix as representing a linear transformation on a vector, and you want to be able to use methods from the Vector class on a result.  If you want to treat the result as a Matrix, then convert v to a Matrix by using the Matrix (Vector& ) copy constructor first. */
  Vector operator * (const Vector& v) const;

  /**Returns the determinant of (*this), requires a square matrix. */
  double determinant() const;

  /**Returns a matrix that is the reduced row echelon form of the calling matrix. */
  Matrix rref() const;

  /**Returns the characteristic polynomial associated with a matrix. */
  Polynomial CharacteristicPolynomial() const;

  /**Returns a pointer to a double array containing the real eigenvalues of the matrix.  NOTE: This algorithm uses numerical root-finding (Newton's Method), which can sometimes fail to converge to a root; additionally, because it is a purely numerical method,  it also finds ONLY REAL ROOTS.  The roots are inserted into the double array starting at the END (i.e., beginning at the array element with the GREATEST INDEX NUMBER), and are added FROM THE BACK TOWARDS THE FRONT.  Therefore, in the event that the number of roots found is less than the order of the matrix, you should read the elements in the array BEGINNING AT THE BACK AND MOVING TOWARDS THE FRONT. */
  double* eigenvalues(const double guess, const double tolerance, const double step, const int maxLoop, int& numfound) const;

  /**Returns the submatrix of the given matrix formed by removing row "row" and column "col". */
  Matrix submatrix(const int row, const int col) const;

  /**Returns the cofactor of the given element. */
  double cofactor(const int row, const int col) const;

  /**Returns the transpose of the matrxi. */
  Matrix transpose() const;

  /**Requires that the matrix be square. */
  Matrix inverse() const;

  /**Raises (*this) to the power (power).  Note that this is only defined for square matrices. */
  Matrix operator ^ (const int power) const;



  //Logical operators and tests



  /**Equality operator. */
  bool operator == (const Matrix& b) const;

  /**Inequality operator. */
  bool operator != (const Matrix& b) const;

  /**Returns true if the matrix is square. */
  bool isSquare() const;

  /**Returns true if the matrix is an orthogonal (real-valued unitary) matrix. */
  bool isOrthogonal() const;  //An orthogonal matrix is a real-valued unitary matrix.

  /**Returns true if the matrix is a normal (real-valued Hermitian) matrix. */
  bool isNormal() const;  //A normal matrix is a real-valued Hermitian matrix.

  /**Returns true if the matrix is symmetric. */
  bool isSymmetric() const;

  /**Returns true if the matrix is skew-symmetric. */
  bool isSkewSymmetric() const;

  /**Returns true if (*this) is the inverse of b. */
  bool isInverse(const Matrix& b) const;

  /**Interchanges rows a and b of a given matrix. */
  void interchange_rows(int a, int b);


  //I/O methods


  /**Prints out the matrix as an array of values in rows and columns. */
  void print() const;

  /**Writes the matrix into the given file in the following format:
     rows columns

     (array of elements follows, printed in the same format as in the print() method).

  **/
  void write(ofstream& outfile) const;

  /**Reads in data written in the format from above, and returns a new matrix constructed from that data. */
  static Matrix read(ifstream& infile);


  /**Standard destructor. */
  ~Matrix();
};

#endif //__MATRIX_H__
