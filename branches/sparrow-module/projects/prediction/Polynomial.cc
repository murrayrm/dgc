#ifndef __POLYNOMIAL_CC__
#define __POLYNOMIAL_CC__

#include "Polynomial.h"
#include <math.h>
#include <iostream>

using namespace std;

Polynomial::Polynomial()
{
  degree = 0;
  coeffs = new double[1];
  coeffs[0] = 0;
}

Polynomial::Polynomial(const int deg, const double* coefficients)
{
  degree = deg;
  coeffs = new double[deg+1];
  for(int i = 0; i <= deg; i++)
    {
      coeffs[i] = coefficients[i];
    }
}

Polynomial::Polynomial(const Polynomial& poly)
{
  degree = poly.getDegree();
  coeffs = new double[degree + 1];
  for(int i = 0; i <= degree; i++)
    {
      coeffs[i] = poly.getCoefficient(i);
    }
}

Polynomial::Polynomial(const double val)
{
  degree = 0;
  coeffs = new double[1];
  coeffs[0] = val;
}

int Polynomial::getDegree() const
{
  return degree;
}

double Polynomial::getCoefficient(const int deg) const
{
  if(deg < 0 || deg > degree)
    {
      return 0;
    }
  else
    {
      return  coeffs[deg];
    }
}


Polynomial& Polynomial::operator = (const Polynomial poly)
{
  if(&poly != this) //Check for self-assignment
    {
      degree = poly.getDegree();
      double* temp = coeffs;
      coeffs = new double [degree + 1];
      for(int i = 0; i <= degree; i++)
	{
	  coeffs [i] = poly.getCoefficient(i);
	}
      delete [] temp;
    }
  
  return (*this);
}

bool Polynomial::operator == (const Polynomial& poly) const
{
  if(getDegree() != poly.getDegree())
    {
      return false;
    }
  else
    {
      bool value = true;
      for(int i = 0; i <= degree; i++)
	{
	  if(getCoefficient(i) != poly.getCoefficient(i))
	    {
	      value = false;
	    }
	}
      return value;
    }
}

bool Polynomial:: operator != (const Polynomial& poly) const
{
  return !((*this) == poly);
}

Polynomial Polynomial::operator + (const Polynomial& poly) const
{
  int deg;

  if(getDegree() > poly.getDegree())
    {
      deg = getDegree();
    }
  else
    {
      deg = poly.getDegree();

    }

  double* cos = new double[deg + 1];

  for(int i = 0; i <= degree; i++)
    {
      cos[i] = getCoefficient(i) + poly.getCoefficient(i);
    }

  Polynomial answer(deg, cos);
  delete [] cos;

  return answer;
}

Polynomial Polynomial::operator + (const double val) const
{

  double* cos = new double [getDegree() + 1];

  for(int i = getDegree(); i >= 1; i--)
    {
      cos[i] = getCoefficient(i);
    }

  cos[0] = (getCoefficient(0) + val);

  Polynomial answer(getDegree(), cos);

  delete [] cos;

  return answer;
}

Polynomial Polynomial::operator - (const Polynomial& poly) const
{
  int deg;

  if(getDegree() == poly.getDegree()) //Need to see if they have common coefficients, as this might results in a polynomial of lesser degree
    {
      deg = getDegree() + 1;
      
      bool haveSameCoefficients = true;
      
      while(deg > 0 && haveSameCoefficients)
  	{
  	  deg--;
	  if(getCoefficient(deg) != poly.getCoefficient(deg))
	    {
	      haveSameCoefficients = false;
	    }
	}
    }
  
  if(getDegree() > poly.getDegree())
    {
      deg = getDegree();
    }
  else
    {
      deg = poly.getDegree();
      
    }
  
  double* cos = new double[deg + 1];
  
  for(int i = 0; i <= deg; i++)
    {
      cos[i] = getCoefficient(i) - poly.getCoefficient(i);
    }
  
  Polynomial answer(deg, cos);
  delete [] cos;
  
  return answer;
}

Polynomial Polynomial::operator - (const double val) const
{
  double* cos = new double [getDegree() + 1];

  for(int i = getDegree(); i >= 1; i--)
    {
      cos[i] = getCoefficient(i);
    }

  cos[0] = (getCoefficient(0) - val);

  Polynomial answer(getDegree(), cos);

  delete [] cos;

  return answer;
}

Polynomial Polynomial::operator * (const Polynomial& poly) const
{
  int deg = getDegree() + poly.getDegree();

  double val;
  double* cos = new double [deg + 1];

  //Note that in polynomial multiplication, the x^t term in the product of the two polynomials is the sum of the products of the terms in the two factor polynomials whose degrees sum to t.
  for(int i = 0; i <= deg; i++)
    {
      val = 0;
      for(int t = 0; t <= i; t++)
	{
	  val += (getCoefficient(t)*poly.getCoefficient(i-t));
	}
      cos[i] = val;
    }

  Polynomial answer(deg, cos);

  delete [] cos;
  return answer;
}

Polynomial Polynomial::scalar(const double val) const
{
  double* cos = new double [getDegree() + 1];

  for(int i = getDegree(); i >= 0; i--)
    {
      cos[i] = getCoefficient(i)*val;
    }

  Polynomial answer(getDegree(), cos);

  delete [] cos;

  return answer;
}

double Polynomial::operator() (const double x) const
{
  double answer = coeffs[0];
  double power;

  for(int i = 1; i <= degree; i++)
    {
      power = 1;

      for(int p = 1; p <= i; p++) //Calculate value of x^i
	{
	  power *= x;
	}
      answer += (power*coeffs[i]);
    }
  return answer;
}

Polynomial Polynomial::derivative() const
{
  
  double cos[degree];
      
      for(int i = degree - 1; i >= 0; i--)
	{
	  cos[i] = (i+1)*getCoefficient(i+1);
	}
      
      Polynomial deriv(degree - 1, cos);

      return deriv;
}



//NOTE:  SYNTHETIC DIVISION ASSUMES THAT "factor" IS A ROOT OF THE POLYNOMIAL
Polynomial Polynomial::SyntheticDivision(const double root) const
{
  int deg = degree - 1; 
  double* cos = new double [deg + 1];

  cos[deg] = getCoefficient(deg + 1);

  for(int i = deg - 1; i >=0; i--)
    {
      cos[i] = getCoefficient(i+1)+root*cos[i+1];
    }

  Polynomial answer(deg, cos);

  delete [] cos;

  return answer;
}

double* Polynomial::findRoots(const double guess, const double tolerance, const double step, const int maxLoops, int& found) const
{
  int deg = getDegree();

  found = 0;

  double* roots = new double [degree];

  Polynomial depressed = (*this);

  int loops = 0;
  //If degree >= 3, use Newton's Method
  while(deg >= 3)  
    {
      Polynomial fprime = depressed.derivative();
      Polynomial f2prime = fprime.derivative();
      
      double previous_guess = guess;
      double fprime_val = fprime(previous_guess);
      double f2prime_val;
      double converge;
      if(fprime_val == 0 )  //Can't have division by zero, so we need to adjust the previous guess by a small amount
	{
	  previous_guess += step;
	  fprime_val = fprime(previous_guess);
	}

      f2prime_val = f2prime(previous_guess);

      converge = f2prime_val*depressed(previous_guess)/(fprime_val*fprime_val);

      if(converge < 0 )  //Interested in absolute values
	{
	  converge *= -1;
	}

      while(converge >= 1  && loops <= maxLoops)  //If we choose our initial guess in here, we're not guaranteed to converge, so we need to adjust the guess by "step" until we're in a convergent region.
	{
	  previous_guess += step;
	  fprime_val = fprime(previous_guess);
	  if(fprime_val == 0 )  //Can't have division by zero, so we need to adjust the previous guess by a small amount
	    {
	      previous_guess += step;
	      fprime_val = fprime(previous_guess);
	    }
	  f2prime_val = f2prime(previous_guess);

	  converge = f2prime_val*depressed(previous_guess)/(fprime_val*fprime_val);

	  if(converge < 0)
	    {
	      converge *= -1;
	    }
	  loops++;
	}

      //Ok, now we're either in a convergent region, or we're taking too long to arrive at a solution.  In either case, we need to start actually finding some roots.


      double latest_guess = previous_guess - depressed(previous_guess)/fprime(previous_guess);
      
      double diff = latest_guess - previous_guess;
      
      if(diff <0)
	{
	  diff *= -1;
	}
      
      loops = 0;
      
      while(diff > tolerance  && loops <= maxLoops)
	{
	  previous_guess = latest_guess;

	  fprime_val = fprime(previous_guess);
	  
	  if(fprime_val == 0)  //Can't have division by zero, so we need to adjust the previous guess by a small amount
	    {
	      previous_guess += step;
	    }
	  
	  latest_guess = previous_guess - depressed(previous_guess)/fprime(previous_guess);
	  
	  diff = latest_guess - previous_guess;
	  
	  if(diff <0)
	    {
	      diff *= -1;
	    }
	  
	  loops++;
	}
      
      if(diff <= tolerance) //We've found a root
	{
	  found++;
	  roots[getDegree() - found] = latest_guess;
	  depressed = depressed.SyntheticDivision(latest_guess);
	  
	  deg--;
	}

      else  //We've gone as far as we can; ABORT!
	{
	  return roots;
	}
    }

  if(deg == 2)
    {
      double a = depressed.getCoefficient(2);
      double b = depressed.getCoefficient(1);
      double c = depressed.getCoefficient(0);

      double discriminant = b*b - 4*a*c;

      if(discriminant < 0) //Check for roundoff error.
	{
	  if(-1*discriminant <= tolerance)  //We may have experienced roundoff error in a polynomial with a double root.
	    {
	      discriminant *= -1;
	    }
	}

      if(discriminant >= 0)
	{
	  roots[0] = (-b + sqrt(discriminant))/(2*a);
	  roots[1] = (-b - sqrt(discriminant))/(2*a);
		      
	  found += 2;
	  return roots;
	}
      else
	{
	  return roots;
	}
    }

  //else (deg == 1)
    
  roots[0] = -depressed.getCoefficient(0)/depressed.getCoefficient(1);
  found += 1;
  return roots;
  
}
	

void Polynomial::print() const
{

  for(int i = getDegree(); i >= 2; i--)
    {
      cout<<getCoefficient(i)<<"x^"<<i<<" ";
      if(getCoefficient(i-1) >= 0)
	{
	  cout<<"+ ";
	}
    }

  if(getDegree() >= 1)
  {
    cout<<getCoefficient(1)<<"x ";
    if(getCoefficient(0) >= 0)
      {
	cout<<"+ ";
      }
  }

  cout<<getCoefficient(0)<<" ";
}

Polynomial::~Polynomial()
{
  delete [] coeffs;
}

#endif //__POLYNOMIAL_CC__
