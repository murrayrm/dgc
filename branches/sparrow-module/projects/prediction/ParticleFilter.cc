#include "ParticleFilter.h"
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <iostream>

using namespace std;

void ParticleFilter::propagateParticle(const Vector& previous, Vector& next, const double elapsed_time) const
{
  //  cout<<"Entering method propagateParticle()"<<endl;
  double K, a, delta_theta, d;
  //Add some acceleration noise.
  a = previous.getElement(4) + gsl_ran_gaussian(rand, accel_noise);
  
  //Add some steering (curvature) noise.
  K = previous.getElement(6) + gsl_ran_gaussian(rand, steer_noise);
  
  if(fabs(K) < 1E-6)  //Can't allow this!
    {
      K = 1E-6;  //We'll fudge this value VERY slightly to avoid divide by zero.
    }
  
  //d = .5at^2 + vt
  d = .5*a*elapsed_time*elapsed_time + previous.getElement(5)*elapsed_time;
  
  delta_theta = K*d;
  
  
  //Now create a new particle and set the appropriate values.
  
 
  //x
  next.setElement(1, previous.getElement(1) + (1/K)*(sin(previous.getElement(3) + delta_theta) - sin(previous.getElement(3))));
  
  //y
  next.setElement(2, previous.getElement(2) - (1/K)*(cos(previous.getElement(3) + delta_theta) - cos(previous.getElement(3))));
  
  //theta2 = theta1 + delta_theta
  next.setElement(3, previous.getElement(3) + delta_theta);
  
  //a2 = a1
  next.setElement(4, a);
  
  //v2 = v1 + at
  next.setElement(5, previous.getElement(5) + a*elapsed_time);
  
  //phi2 = phi1
  next.setElement(6, K);

  //Probability of propagated particle is equal to the original particle in case of lack of any additional information.
  next.setElement(7, previous.getElement(7));

  //cout<<"Leaving method propagateParticle()"<<endl;

}


ParticleFilter::ParticleFilter(const Vector* initial_set, const int parts, const double accel, const double steer, const double length, const double width, const double dist)
{
  //Initializes our current set of particles to the set passed in as the argument of the constructor.
  numParticles = parts;
  particles = new Vector[numParticles];

  //Copy over the set of old particles into the Vector so that we maintain a LOCAL copy (to prevent side effects).

  for(int i = 0; i < numParticles; i++)
    {
      particles[i] = initial_set[i];
    }

  gsl_rng_env_setup();

  const gsl_rng_type* T;
  T = gsl_rng_default;
  rand = gsl_rng_alloc(T);

  accel_noise = accel;
  steer_noise = steer;
  length_noise = length;
  width_noise = width;
  dist_measure_noise = dist;
}
Vector* ParticleFilter::propagatePDF(const double elapsed_time)  const

{
  cout<<"Entering method propagatePDF()"<<endl;

  Vector new_part(7);

  //Our new (propagated) set of particles.
  Vector* newlist = new Vector[numParticles];

  double K, a, delta_theta, d;

  for(int i = 0; i < numParticles; i++)
    {
      propagateParticle(particles[i], new_part, elapsed_time);
      newlist[i] = new_part;
    }

  cout<<"Leaving method propagatePDF()"<<endl;

  return newlist;
}


void ParticleFilter::updatePDF(const double elapsed_time, const double* sensor_data)
{

  cout<<"Entering method updatePDF()"<<endl;

  //First, propagate the curren set of particles up through the current time.
  Vector* prop = propagatePDF(elapsed_time);

  double x_error, y_error, x_prob, y_prob;  //In standard deviations.

  double max_range = 1/(sqrt(2*M_PI) * dist_measure_noise);

  for(int i = 0; i < numParticles; i++)
    {
      //For each particle, assume that that particle represents the actual state of the vehicle.  Compute the "error" in the measurement, treating any deviation from the predicted state as error.

      x_error = (sensor_data[0] - prop[i].getElement(1));
      y_error = (sensor_data[1] - prop[i].getElement(2));

      //Now we determine the probability of obtaining the errors in measurement determined above.  We do this via an approximation to the normal density.  We partition up the normal PDF into 8 "bins", where each bin corresponds to a range of values of 1 standard deviation.  The probability associated with a measurement is "equal" to the probability of a normally distributed random variable falling within that bin.

      //Probability weight of x error.

      x_prob = max_range*exp(-x_error*x_error/(2*dist_measure_noise*dist_measure_noise));

      //Probability weight of y error.

      y_prob = max_range*exp(-y_error*y_error/(2*dist_measure_noise*dist_measure_noise));


      //Multiply the current probability of the particle by the probabilty obtained above.
      prop[i].setElement(7, prop[i].getElement(7)*x_prob*y_prob);

    }

  //Ok, now we need to normalize the particle weights.

  double t = 0; //Total particles weight.

  for(int i = 0; i < numParticles; i++)
    {
      t += particles[i].getElement(7);
    }

 

  double factor = 1/t;  //Normalization constant.

  //cout<<"Normalization constant = "<<factor<<endl;

  for(int i = 0; i < numParticles; i++)
    {
      particles[i].setElement(7, particles[i].getElement(7)*factor);
    }

#warning:"We need something better than resampling every timestemp.  We should implement some form of effective sample size based resampling."
  bool need_resample = true;

  if(need_resample)
    {
      //Ok, now resample the particles.
      resample(prop);
    }

  else
    {
      Vector* temp = particles;
      particles = prop;
      delete [] temp;
    }
  cout<<"Leaving method updatePDF()"<<endl;
}


void ParticleFilter::resample(Vector* sample_set)
{
  cout<<"Entering method resample()"<<endl;
  //Now that we have assigned weights to all the particles, we construct a new list of particles selected with a probability proportional to the particle weight. Note that the maximum value that the Gaussian distribution attains is f(0) = 1/(sqrt(2*M_PI)*sigma)

  int new_length = 0;
  int pos = 0;
  double r;
  int last_printed = 0;

  Vector* posterior = new Vector[numParticles];

  while (new_length < numParticles)
    {
      r = gsl_ran_flat(rand, 0, 1);  //We've normalized our particle weights, so we use an interval of [0,1]

      //       cout<<"Particle "<<pos<<" has probability "<<sample_set[pos].getElement(9)<<"."<<endl;
      //Determine whether or not to add the particle to the new list according to the above logic.
      if(r < sample_set[pos].getElement(7))
	{
	  posterior[new_length] = sample_set[pos];
	  posterior[new_length].setElement(7, 1);  //Need to set particle weight back to 1 after resampling.
	  new_length++;
	}
      pos++;

      if(pos >= numParticles)  //We've gotten to the end of our propagated particle list, but we've not yet filled up our posterior particle set. Therefore, return to the front of the list and continue sampling particles.
	{
	  pos = 0;
	}

      if((new_length % 10000 == 0) && (new_length != 0) && (new_length != last_printed))
	{
	  cout<<"Resampled particle list has "<<new_length <<" particles."<<endl;
	  last_printed = new_length;
	}
    }

  //Ok, by now, we've filled up our posterior list.  Make it official by setting our internal particle list to this list. 
  
  Vector* temp = particles;
  particles = posterior;

  delete [] temp;
  delete [] sample_set;

  cout<<"Leaving method resample()"<<endl;
}

Vector* ParticleFilter::getPDF() const
{
  Vector* PDF = new Vector[numParticles];
  for(int i= 0; i< numParticles; i++)
    {
      PDF[i] = particles[i];
    }

  return PDF;
}

Vector** ParticleFilter::generatePrediction(const int numsteps, const double interval)
{
  Vector** particleSets = new Vector*[numsteps];

  for(int i = 0; i < numsteps; i++)
    {
      //Create a new Vector* to hold all of the particles for the PDF at this timestep.
      particleSets[i] = new Vector[numParticles];
    }

  Vector def(7);


  //Need to instantiate these lists to contain vectors of the correct dimension.
  for(int i = 0; i < numsteps; i++)
    {
      for(int p = 0; p < numParticles; p++)
	{
	  particleSets[i][p] = def;
	}
    }

  for(int i = 0; i < numsteps; i++)
    {
      if(i == 0)  //Start by generating the first predicted particle set from the last updated belief.
	{
	  for(int p = 0; p < numParticles; p++)
	    {
	      propagateParticle(particles[p], particleSets[0][p], interval);
	    }
	}

      else  //Use the previous propagated set to generate the current set.
	{
	  for(int p = 0; p < numParticles; p++)
	    {
	      propagateParticle(particleSets[i-1][p], particleSets[i][p], interval);
	    }
	}
    }

  return particleSets;
}

	      

ParticleFilter::~ParticleFilter()
{
  delete [] particles;
}
