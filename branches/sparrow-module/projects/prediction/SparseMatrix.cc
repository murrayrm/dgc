#ifndef __SPARSEMATRIX_CC__
#define __SPARSEMATRIX_CC__
#include <math.h>
#include "SparseMatrix.h"
#include "Vector.h"
#include "Polynomial.h"
#include "UndefinedOperation.h"
#include "IndexOutOfBounds.h"
#include <fstream>

using namespace std;

SparseMatrix::SparseMatrix()
{
  numRows = 1;
  numCols = 1;
}

SparseMatrix::SparseMatrix(const int r, const int c)
{
  if((r < 1) || (c < 1))
    {
      throw IndexOutOfBounds("Error: Matrices must be at least 1 x 1.");
    }

  numRows = r;
  numCols = c;
}

SparseMatrix::SparseMatrix(const int order)
{
  if(order  < 1)
    {
      throw IndexOutOfBounds("Error: Matrices must be at least 1 x 1.");
    }
  numRows = order;
  numCols = order;
}

SparseMatrix::SparseMatrix(const SparseMatrix& other)
{
  numRows = other.getRows();
  numCols = other.getColumns();
  rows = other.rows;
}

SparseMatrix::SparseMatrix(const Vector& v)
{
  numRows = v.getDimension();
  numCols = 1;

  list<Row>::iterator iter;
  for(int r = 1; r <= numRows; r++)
    {
      if(v.getElement(r) != 0)
	{
	  rows.push_back(Row(r));
	  iter = rows.end(); //end points to NULL, NOT to the last element, so we need to back it up once to point to the last element.
	  iter--;
	  (*iter).appendColumn(1, v.getElement(r));
	}
    }
}

SparseMatrix& SparseMatrix::operator = (const SparseMatrix& other)
{
  if(this != &other) //Check for self-assignment
    {
      rows = other.rows;
      numRows = other.getRows();
      numCols = other.getColumns();
    }
  return (*this);
}

SparseMatrix& SparseMatrix::operator = (const Matrix& other)
{
  rows.clear();
  numRows = other.getRows();
  numCols = other.getColumns();

  for(int r = 1; r <= numRows; r++)
    {
      for(int c = 1; c <= numCols; c++)
	{
	  appendElement(r, c, other.getElement(r, c));
	}
    }

  return (*this);
}


int SparseMatrix::getRows() const
{
  return numRows;
}

int SparseMatrix::getColumns() const
{
  return numCols;
}

int SparseMatrix::getOrder() const
{
  if(!isSquare())
    {
      throw UndefinedOperation("Error:  The order property is only defined for square matrices.");
    }
  return numRows;
}

double SparseMatrix::getElement(const int row, const int col) const
{
  if((row < 1) || (row >  numRows) ||  (col < 1) ||  (col >  numCols))
    {
      throw IndexOutOfBounds();
    }

  if(rows.empty())
    {
      return 0;
    }
  else
    {
      list<Row>::const_iterator iter = rows.begin();
      
      int pos = 1;
      int list_length = rows.size();
      bool search = true;
      int rowVal;
      
      while(search && (pos <= list_length))
	{
	  rowVal = (*iter).getRow();
	  
	  if(rowVal == row) //We've found the right row, so return the value in the appropriate column.
	    {
	      return (*iter).getValue(col);
	    }
	  
	  else if(rowVal > row)  //We've passed where it should be, so it's not in the list
	    {
	      return 0;
	    }
	  
	  else //We're not there yet, keep searching.
	    {
	      iter++;
	      pos++;
	    }
	}

      //If we're here, it means that we've run off the edge of the list, so the element we're looking for isn't in the list at all
      return 0;
    }
}

void SparseMatrix::setElement(const int row, const int col, const double val)
{
  if((row < 1) || (row >  numRows) ||  (col < 1) ||  (col >  numCols))
    {
      throw IndexOutOfBounds();
    }

  if(rows.empty()) 
    {
      //We WON'T append if it's zero, as we aren't explicitly representing zeros.
      if(val != 0)
	{
	  appendElement(row, col, val);
	}
      //Else do nothing 
    }
  else
    {
      //We have at least one list element, so this isn't a null pointer.
      list<Row>::iterator iter = rows.begin();
      
      int pos = 1;
      int list_length = rows.size();
      bool search = true;
      int rowVal;
      
      //Note that iter points to an object of type Row, so *iter is an object of type Row
      while(search && (pos <= list_length))
	{
	  rowVal = (*iter).getRow();
	  if(rowVal == row)  //We've found the right column number
	    {
	      search = false;
	      (*iter).setValue(col, val);

	      //Now we need to check if this row has zero elements in it, since we've removed one.
	      if((val == 0) && (*iter).isEmpty())
		{
		      rows.erase(iter);
		}
	    }
	  else if (rowVal > row) //We've just passed the row position that we want to insert into, so we want to insert a value into the row that precedes this one, unless, of course, it's zero.
	    {
	      search = false;
	      if(val != 0)
		{
		  rows.insert(iter, Row(row));
		  //Now back the iterator up so that it points to the row we just added.
		  iter--;
		  (*iter).setValue(col, val);
		}
	      //Else do nothing; we're not going to insert a zero element.
	    }
	  else
	    {
	      //We haven't found a column value equal to or greater than what we're looking for, so keep looking.
	      iter++;
	      pos++;
	    }
	}
      
      if(search && (val != 0)) //The only way we can exit the for loop with search == true is if we've run off the back end of the list, so append this element.
	{
	  appendElement(row, col, val);
	}
    }
}

void SparseMatrix::appendElement(const int row, const int col, const double val)
{
  if((row < 1) || (row >  numRows) ||  (col < 1) ||  (col >  numCols))
    {
      throw IndexOutOfBounds();
    }

  if(val != 0)
    {
      //First, check to see if the matrix is currently empty.
      if(rows.empty())
	{
	  rows.push_back(Row(row));
	  list<Row>::iterator iter = rows.end();
	  iter--;
	  (*iter).appendColumn(col, val);
	}
      else //The list isn't empty.  Check to see if the element we're adding is in the last row of the matrix, or if we need to add a new row.
	{
	  list<Row>::iterator iter = rows.end();
	  iter--;
	  if((*iter).getRow() == row) //Ok, we need to add the element to this row
	    {
	      (*iter).appendColumn(col, val);
	    }
	  else //We need to add a new row
	    {
	      rows.push_back(Row(row));
	      iter = rows.end();
	      iter--;
	      (*iter).appendColumn(col, val);
	    }
	}
    }
}

void SparseMatrix::appendRow(const Row& other)
{
  rows.push_back(other);
}

SparseMatrix SparseMatrix::operator + (const SparseMatrix& other) const
{
  
  if((numRows != other.getRows()) || (numCols != other.getColumns()))
    {
      throw UndefinedOperation("Error: Matrix addition is only defined for matrices having the same dimensions.");
    }
  
  SparseMatrix answer(numRows, numCols);
  int this_length = rows.size();
  int other_length = other.rows.size();
  
  int this_pos = 1;
  int other_pos = 1;
  
  list<Row>::const_iterator this_list = rows.begin();
  list<Row>::const_iterator other_list = other.rows.begin();
  
  while((this_pos <= this_length) && (other_pos <= other_length))  //There are more elements in each list to iterate through.
    {
      if((*this_list).getRow() == (*other_list).getRow())  //These two row elements have the same row index, so we need to add their sum to the 
	{
	  answer.appendRow((*this_list) + (*other_list));
	  this_pos++;
	  other_pos++;
	  this_list++;
	  other_list++;
	}
      else if((*this_list).getRow() < (*other_list).getRow())
	{
	  answer.appendRow(*this_list);
	  this_pos++;
	  this_list++;
	    }
      else
	{
	  answer.appendRow(*other_list);
	  other_pos++;
	  other_list++;
	}
    }
  
  //If we're down here, one or both of the lists have run out of elements to iterate through, so just run through the elements in the remaining list.
  while(this_pos <= this_length)
    {
      answer.appendRow(*this_list);
      this_pos++;
      this_list++;
    }
  
  while(other_pos <= other_length)
    {
      answer.appendRow(*other_list);
      other_pos++;
      other_list++;
    }
  
  return answer;
}

Matrix SparseMatrix::operator + (const Matrix& other) const
{
  if((numRows != other.getRows()) || (numCols != other.getColumns()))
    {
      throw UndefinedOperation("Error: Matrix addition is only defined for matrices having the same dimensions.");
    }
  
  Matrix answer = other;

  int len = rows.size();

  list<Row>::const_iterator rowIter = rows.begin();
  list<ColumnElement>::const_iterator colIter; 

  for(int r = 1; r <= len; r++)
    {
      int cols = (*rowIter).length();
      colIter = (*rowIter).getIterator();
      for(int c = 1; c <= cols; c++)
	{
	  answer.setElement((*rowIter).getRow(), (*colIter).getCol(), answer.getElement((*rowIter).getRow(), (*colIter).getCol()) + (*colIter).getVal());
	  colIter++;
	}
      rowIter++;
    }

  return answer;
}

Matrix operator + (const Matrix& A, const SparseMatrix& B)
{
  if((A.getRows() != B.getRows()) || (A.getColumns() != B.getColumns()))
    {
      throw UndefinedOperation("Error: Matrix addition is only defined for matrices having the same dimensions.");
    }
  
  Matrix answer = A;

  int len = B.rows.size();

  list<Row>::const_iterator rowIter = B.rows.begin();
  list<ColumnElement>::const_iterator colIter; 

  for(int r = 1; r <= len; r++)
    {
      int cols = (*rowIter).length();
      colIter = (*rowIter).getIterator();
      for(int c = 1; c <= cols; c++)
	{
	  answer.setElement((*rowIter).getRow(), (*colIter).getCol(), answer.getElement((*rowIter).getRow(), (*colIter).getCol()) + (*colIter).getVal());
	  colIter++;
	}
      rowIter++;
    }

  return answer;
}



SparseMatrix SparseMatrix::operator - (const SparseMatrix& other) const
{
  
  if((numRows != other.getRows()) || (numCols != other.getColumns()))
    {
      throw UndefinedOperation("Error: Matrix addition is only defined for matrices having the same dimensions.");
    }
  
  SparseMatrix answer(numRows, numCols);
  int this_length = rows.size();
  int other_length = other.rows.size();
  
  int this_pos = 1;
  int other_pos = 1;
  
  list<Row>::const_iterator this_list = rows.begin();
  list<Row>::const_iterator other_list = other.rows.begin();
  
  while((this_pos <= this_length) && (other_pos <= other_length))  //There are more elements in each list to iterate through.
    {
      if((*this_list).getRow() == (*other_list).getRow())  //These two column elements have the same column index, so we need to add their sum to the 
	{
	  answer.appendRow((*this_list) - (*other_list));
	  this_pos++;
	  other_pos++;
	  this_list++;
	  other_list++;
	}
      else if((*this_list).getRow() < (*other_list).getRow())
	{
	  answer.appendRow(*this_list);
	  this_pos++;
	  this_list++;
	}
      else
	{
	  answer.appendRow((*other_list).scalar(-1));
	  other_pos++;
	  other_list++;
	}
    }
  
  //If we're down here, one or both of the lists have run out of elements to iterate through, so just run through the elements in the remaining list.
  while(this_pos <= this_length)
    {
      answer.appendRow(*this_list);
      this_pos++;
      this_list++;
    }
  
  while(other_pos <= other_length)
    {
      answer.appendRow((*other_list).scalar(-1));
      other_pos++;
      other_list++;
	}
  
  return answer;
}

Matrix SparseMatrix::operator - (const Matrix& other) const
{
  if((numRows != other.getRows()) || (numCols != other.getColumns()))
    {
      throw UndefinedOperation("Error: Matrix addition is only defined for matrices having the same dimensions.");
    }
  
  Matrix answer = other.scalar(-1);

  int len = rows.size();

  list<Row>::const_iterator rowIter = rows.begin();
  list<ColumnElement>::const_iterator colIter; 

  for(int r = 1; r <= len; r++)
    {
      int cols = (*rowIter).length();
      colIter = (*rowIter).getIterator();
      for(int c = 1; c <= cols; c++)
	{
	  answer.setElement((*rowIter).getRow(), (*colIter).getCol(), answer.getElement((*rowIter).getRow(), (*colIter).getCol()) + (*colIter).getVal());
	  colIter++;
	}
      rowIter++;
    }

  return answer;
}

Matrix operator - (const Matrix& A, const SparseMatrix& B)
{
  if((A.getRows() != B.getRows()) || (A.getColumns() != B.getColumns()))
    {
      throw UndefinedOperation("Error: Matrix addition is only defined for matrices having the same dimensions.");
    }
  
  Matrix answer = A;

  int len = B.rows.size();

  list<Row>::const_iterator rowIter = B.rows.begin();
  list<ColumnElement>::const_iterator colIter; 

  for(int r = 1; r <= len; r++)
    {
      int cols = (*rowIter).length();
      colIter = (*rowIter).getIterator();
      for(int c = 1; c <= cols; c++)
	{
	  answer.setElement((*rowIter).getRow(), (*colIter).getCol(), answer.getElement((*rowIter).getRow(), (*colIter).getCol()) - (*colIter).getVal());
	  colIter++;
	}
      rowIter++;
    }

  return answer;
}

SparseMatrix SparseMatrix::scalar(const double k) const
{
  SparseMatrix answer(numRows, numCols);
  
  if(k == 0)
    {
      return answer;
    }

  else
    {
      int len = rows.size();

      list<Row>::const_iterator iter = rows.begin();

      for(int r = 1; r <= len; r++)
	{
	  answer.appendRow((*iter).scalar(k));
	  iter++;
	}

      return answer;
    }
}

SparseMatrix SparseMatrix::operator * (const SparseMatrix& other) const
{
  if(numCols != other.getRows())
    {
      throw UndefinedOperation("Error: The matrix product AB is only defined if A has the same number of columns as B has rows.");
    }

  SparseMatrix answer(numRows, other.getColumns());

  list<Row>::const_iterator rowIter = rows.begin();
  list<ColumnElement>::const_iterator colIter;  //Iterator to the ColumnElements in a given row.

  double val;
  int rowLen = rows.size();
  int colLen;

  for(int i = 1; i <= rowLen; i++)  //Iterate through the nonzero rows of the matrix.
    {
      colLen = (*rowIter).length();  //Length of the row we're currently pointing to.

      for(int c = 1; c <= other.getColumns(); c++)  //Iterate through the columns of the PRODUCT matrix to find inner products.
	{
	  colIter = (*rowIter).getIterator();
	  val = 0;

	  for(int prod = 1; prod <= colLen; prod++)  //Iterate within a row to multiply non-zero row elements with corresponding column elements from matrix other
	    {
	      val += ((*colIter).getVal())*other.getElement((*colIter).getCol(), c);
	      colIter++;
	    } 

	  answer.appendElement((*rowIter).getRow(), c, val);
	}

      rowIter++;
    }

  return answer;
}

Matrix SparseMatrix::operator * (const Matrix& other) const
{
  if(numCols != other.getRows())
    {
      throw UndefinedOperation("Error: The matrix product AB is only defined if A has the same number of columns as B has rows.");
    }

  Matrix answer(numRows, other.getColumns());

  list<Row>::const_iterator rowIter = rows.begin();
  list<ColumnElement>::const_iterator colIter;  //Iterator to the ColumnElements in a given row.

  double val;
  int rowLen = rows.size();
  int colLen;

  for(int i = 1; i <= rowLen; i++)  //Iterate through the nonzero rows of the matrix.
    {
      colLen = (*rowIter).length();  //Length of the row we're currently pointing to.

      for(int c = 1; c <= other.getColumns(); c++)  //Iterate through the columns of matrix "other" to find inner products.
	{
	  colIter = (*rowIter).getIterator();
	  val = 0;

	  for(int prod = 1; prod <= colLen; prod++)  //Iterate within a row to multiply non-zero row elements with corresponding column elements from matrix other
	    {
	      val += ((*colIter).getVal())*other.getElement((*colIter).getCol(), c);
	      colIter++;
	    } 

	  answer.setElement((*rowIter).getRow(), c, val);
	}

      rowIter++;
    }

  return answer;
}

Matrix operator * (const Matrix& A, const SparseMatrix& B)
{
  if(A.getColumns() != B.getRows())
    {
      throw UndefinedOperation("Error: The matrix product AB is only defined if A has the same number of columns as B has rows.");
    }

  Matrix answer(A.getRows(), B.getColumns());
  for(int r = 1; r<= A.getRows(); r++)
    {
      for(int c = 1; c<= B.getColumns(); c++)
	{
	  double product = 0;
	  for(int i = 1; i<= A.getColumns(); i++)
	    {
	      product += A.getElement(r,i)*B.getElement(i,c);
	    }
	  answer.setElement(r,c,product);
	}
    }
  return answer;
}

Vector SparseMatrix::operator * (const Vector& v) const
{
  if(numCols != v.getDimension())
    {
      throw UndefinedOperation("Error:  The product of a matrix and a vector is only defined if the number of columns of the matrix is equal to the dimension of the vector.");
    }
 
  Vector answer(numRows);

  double inner;

  list<Row>::const_iterator Rowiter = rows.begin();

  int length = rows.size();

  for(int r = 1; r <= length; r++)
    {
      inner = 0;
      list<ColumnElement>::const_iterator Coliter= (*Rowiter).getIterator();

      int colLength = (*Rowiter).length();

      for(int c = 1; c<= colLength; c++)
	{
	  inner += (*Coliter).getVal()*v.getElement((*Coliter).getCol());
	  Coliter++;
	}

      answer.setElement((*Rowiter).getRow(), inner);
      Rowiter++;
    }
  return answer;
}


double SparseMatrix::determinant() const
{
  if(!isSquare())
      {
	throw UndefinedOperation("Error: Determinants are only defined for square matrices.");
      }

  if(rows.empty())
    {
      return 0;
    }

  if(getOrder() == 1)
    {
      return getElement(1,1);
    }
    
  else if(getOrder() == 2)
    {
      return getElement(1,1)*getElement(2,2) - getElement(1,2)*getElement(2,1);
    }

  else //We want to expand by minors across the first row of the matrix (ROW 1!!!)
    {
      if( (*(rows.begin())).getRow() != 1) //If the first row number of the rows stored internally isn't 1, then it means that this matrix has all zeros in the first row, in which case, its determinant is obviously zero.
	{
	  return 0;
	}
      else
	{
	  double answer = 0;
	  list<ColumnElement>::const_iterator iter = (*(rows.begin())).getIterator(); //Iterator to the ColumnElements of the first row of this matrix.
	  int len = (*(rows.begin())).length();
	  
	  for(int i = 1; i<= len; i++)
	    {
	      double temp = (*iter).getVal() * (submatrix(1, (*iter).getCol())).determinant()*pow(-1, 1+(*iter).getCol());
	      answer += temp;
	      iter++;
	    }

	  return answer;
	}
    }
}


SparseMatrix SparseMatrix::submatrix(const int row, const int col) const
{
  if((row < 1) || (row >  numRows) ||  (col < 1) ||  (col >  numCols))
    {
      throw IndexOutOfBounds();
    }

  SparseMatrix answer(numRows - 1, numCols - 1);

  list<Row>::const_iterator Rowiter = rows.begin();
  list<ColumnElement>::const_iterator Coliter;

  int len = rows.size();

  for(int i = 1; i <= len; i++)
    {
      if((*Rowiter).getRow() != row) //This isn't the row we're removing, so we can add all the element in it to the resultant matrix, except for the element in column "col"
	{

	  Row* add;
	  
	  if((*Rowiter).getRow() < row) //We don't need to shift this row index.
	    {
	      add = new Row((*Rowiter).getRow());
	    }
	  else
	    {
	      add = new Row ((*Rowiter).getRow() - 1);
	    }

	  Coliter = (*Rowiter).getIterator();

	  for(int c = 1; c<= (*Rowiter).length(); c++)
	    {
	      if((*Coliter).getCol() != col)  //This isn't the column we're leaving out, so append this column/val pair to the row we're making.
		{

		  if((*Coliter).getCol() < col) //We don't need to shift this row index.
		    {
		      add->appendColumn((*Coliter).getCol(), (*Coliter).getVal());
		    }
		  else
		    {
		      add->appendColumn((*Coliter).getCol()-1, (*Coliter).getVal());
		    }
		}
	      Coliter++;
	    }
	  if(!(add->isEmpty()))  //If column "col" was the only column in this row, we don't want to add this row to the resultant matrix (since it's now empty).
	    {
	      answer.appendRow((*add));
	    }
	  delete add;
	}
      Rowiter++;
    }

  return answer;
}


double SparseMatrix::cofactor(const int row, const int col) const
{

  if(!isSquare())
    {
      throw UndefinedOperation("Error: Cofactors of elements in a matrix are only defined for square matrices."); 
    }

  if((row < 1) || (row >  numRows) ||  (col < 1) ||  (col >  numCols))
    {
      throw IndexOutOfBounds();
    }
  
  return submatrix(row, col).determinant()*pow(-1, row+col);
}

SparseMatrix SparseMatrix::transpose() const
{
  SparseMatrix answer(numCols, numRows);

  list<Row>::const_iterator rowIter = rows.begin();

  list<ColumnElement>::const_iterator colIter;

  int n_rows = rows.size();

  for(int r = 1; r <= n_rows; r++)
    {
      colIter = (*rowIter).getIterator();

      int n_cols = (*rowIter).length();

      for(int c = 1; c<= n_cols; c++)
	{
	  answer.setElement((*colIter).getCol(), (*rowIter).getRow(), (*colIter).getVal());
	  colIter++;
	}
      rowIter++;
    }

  return answer;
}

SparseMatrix SparseMatrix::inverse() const
{
  if(!isSquare())
    {
      throw UndefinedOperation("Error: Cofactors of elements in a matrix are only defined for square matrices."); 
    }
  
  SparseMatrix answer(numRows, numCols);

  for(int r = 1; r <= numRows; r++)
    {
      for(int c = 1; c<= numCols; c++)
	{
	  answer.setElement(r, c, (*this).cofactor(r, c));
	}
    }

  answer = answer.transpose();

  answer = answer.scalar(1/determinant());

  return answer;
}



bool SparseMatrix:: operator == (const SparseMatrix& other) const
{
  if(rows.size() != other.rows.size())
    {
      return false;
    }
  else
    {
      list<Row>::const_iterator this_list = rows.begin();
      list<Row>::const_iterator other_list = other.rows.begin();
      int len = rows.size();
      int pos = 1;
      
      while (pos <= len)
	{
	  if(((*this_list).getRow() != (*other_list).getRow()) || ((*this_list) != (*other_list)))
	    {
	      return false;
	    }
	  pos++;
	  this_list++;
	  other_list++;
	}
      return true;
    } 
}

bool SparseMatrix::operator != (const SparseMatrix& other) const
{
  return !((*this) == other);
}

bool SparseMatrix::isSquare() const
{
  return (numRows == numCols);
}



bool SparseMatrix::isSymmetric() const
{
  bool temp;
  if(!isSquare())
    {
      temp =  false;
    }
  else
    {
      temp = ((*this) == (*this).transpose());
    }
  return temp;
}

bool SparseMatrix::isSkewSymmetric() const
{
  bool temp;
  if(!isSquare())
    {
      temp = false;
    }
  else
    {
      temp = ((*this).scalar(-1) == (*this).transpose());
    }
  return temp;
}

bool SparseMatrix::isInverse(const SparseMatrix& b) const
{
  return ((*this).inverse() == b);
}

void SparseMatrix::print() const
{
  for(int r = 1; r <= numRows; r++)
  {
    for(int c = 1; c <= numCols; c++)
      {
	cout<< getElement(r, c) <<" ";
      }
    cout<<endl;
  }
}


void SparseMatrix::write(ofstream& outfile) const
{
  outfile <<getRows()<<" "<<getColumns()<<endl;

  for(int r = 1; r <= getRows(); r++)
    {
      for(int c = 1; c <= getColumns(); c++)
	{
	  outfile<< getElement(r, c) <<" ";
	}
      outfile<<endl;
    }
}

SparseMatrix SparseMatrix::read(ifstream& infile)
{
  int rows, cols;
  double element;

  infile >> rows;
  infile >> cols;

  SparseMatrix answer(rows, cols);

  for(int r = 1; r <= rows; r++)
    {
      for(int c = 1; c <= cols; c++)
	{
	  infile >> element;
	  answer.appendElement(r, c, element);
	}
    }
  return answer;
}

void SparseMatrix::printMemory() const
{
  list<Row>::const_iterator rowIter = rows.begin();

  int rowLength = rows.size();

  for(int r = 1; r <= rowLength; r++)
    {
      list<ColumnElement>::const_iterator colIter = (*rowIter).getIterator();
      cout<<"Row: "<<(*rowIter).getRow()<<endl;
      int colLength = (*rowIter).length();

      for(int c = 1; c <= colLength; c++)
	{
	  cout<<" Col: "<<(*colIter).getCol()<<" Value: "<<(*colIter).getVal()<<endl;
	  colIter++;
	}
      cout<<endl;

      rowIter++;
    }
}




SparseMatrix::~SparseMatrix()
{
  //Nothing to do; as the list<Row> object is declared statically, it will automatically be cleaned up when this SparseMatrix is cleaned up (it will go out of scope).
}


#endif //__SPARSEMATRIX_CC__
