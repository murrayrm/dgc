 /****************************************************************
 *  Project    : Learning and adaptation, SURF 2006              *
 *  File       : TraversedPath.hh                                *
 *  Description: First version of program that stores Alice's    *
 *               traversed paths. This file contains declaration *
 *               of the class TraversedPath                      *
 *  Author     : Jose Torres                                     *
 *  Modified   : 6-23-06                                         *
 *****************************************************************/      

#ifndef TEST_HH
#define TEST_HH

#include "StateClient.h"
#include "SkynetContainer.h"

#include <iostream>
#include <cstring>
#include <iomanip>
#include <cmath>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <stdio.h>
#include <time.h>
#define GPS_CAPTURE_FREQUENCY 0.5
using namespace std;


/*! TravesedPath class. This class stores all the paths ALICE
 *  has driven. It currently creates a file "savedPaths" and
 *  stores all the coordinates in this file. This class also 
 *  stores the path using a dynamicly created double list.
 * \brief Here goes a short description of what this class does
 *  This class creates a list with nodes  for each location 
 *  ALICE has been at. Each node in the list also has a list 
 *  attached to it, representing all the locations ALICE went 
 *  immediately after that corresponding location 
 */
class Test : public CStateClient 
{  
private: 
  struct Checkpoint
  {
    double startX;
    double startY;
    double endX;
    double endY;
  };
  
  
public:
  
  /*! constructor */
  Test(int skynet_key);
  
  /*! destructor, destroys dynamicly created lists  */
  ~Test();                                   

  void createPseudoCheckpoints();
  
};

#endif
