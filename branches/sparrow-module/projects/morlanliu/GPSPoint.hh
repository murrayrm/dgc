#ifndef GPSPOINT_HH_
#define GPSPOINT_HH_
#include <vector>
#include <iostream>
#include "ggis.h"
#include "frames/coords.hh"
using namespace std;

/*! GPSPoint class. The GPS point class represents a generic GPS point.
 *  Waypoints and perimeter points are subclasses of GPS points.
 *  All GPS points have a longitude and latitude. GPS points can be designated
 *  as entry entry points and/or exit points.
 *  \brief The waypoint class represents a waypoint provided by the RNDF file.
 */
class GPSPoint
{
public:
/*! All GPS points have a longitude and latitude. GPS points can be designated
 *  as entry points and/or exit points. */
  GPSPoint(int segmentID, int laneID, int waypointID, double latitude, double longitude);
  virtual ~GPSPoint();
  
/*! Returns the segment ID of THIS. */
  int getSegmentID();
  
/*! Returns the lane ID of THIS. */
  int getLaneID();

/*! Returns the waypoint ID of THIS. */
  int getWaypointID();
  
/*! Returns the latitude of THIS. */
  double getLatitude();
  
/*! Returns the longitude of THIS. */
  double getLongitude();

  /*! Returns the Northing and Easting coordinates of THIS. */
  NEcoord getNEcoord();
  
/*! Returns the vector of pointers to entry GPS points. */
  vector<GPSPoint*> getEntryPoints();
  
/*! Sets THIS as an entry GPS point. */
  void setEntry();
  
/*! Sets THIS as an exit GPS point and adds entryGPSPoint to the array
 *  entryPoints. */
  void setExit(GPSPoint* entryGPSPoint);
  
/*! Returns TRUE iff THIS is an entry GPS point. */
  bool isEntry();
  
/*! Returns TRUE iff THIS is an exit GPS point. */
  bool isExit();
  
protected:
/*  Segments are identified using the form 'M'. The Nth lane of segment M has
 *  identifier 'M.N'. The waypoints of each lane are similarly identified 
 *  such that the Pth waypoint of lane M.N is 'M.N.P'.*/
  int segmentID, laneID, waypointID;

/*  GPS latitude and logitude fields are floats with six decimal places 
 *  and represent points in the WGS84 coordinate system.*/
  double latitude, longitude;
  
/*  GPS points can be designated as an entry and/or  an exit point. */
  bool entry, exit;
  
/*  Exit GPS points have related entry GPS points. */
  vector<GPSPoint*> entryPoints;
};

#endif /*GPSPOINT_HH_*/
