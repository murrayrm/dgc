/* AState_APX.cc (miniAstate)
 * 
 * AState class functions to interface with Applanix POS-LV.
 *
 * Stefano Di Cairano, NOV-06
 *
 *
 */

#include "AState.hh"
//#include <cassert.hh>
//#include "Applanix.hh"
//#define NAVMOD_MSG_LENGTH 16

/*  We don't need this because setup is done offline
void AState::apxInit()
{
  
  if (_astateOpts.useReplay == 1) 
  {
    rawApx apxIn; //I keep this structure but maybe we do not need it anymore since the time should be a field on every packet
    
    apxReplayStream.read((char*)&apxIn, sizeof(rawApx));
    DGClockMutex(&m_ApxDataMutex);
    apxLogStart = apxIn.time;
    DGCunlockMutex(&m_ApxDataMutex);

    DGClockMutex(&m_MetaStateMutex);
    _metaState.apxEnabled = 1;
    DGCunlockMutex(&m_MetaStateMutex);
  } 
  else 
  {
    int ackNav=0; 					      //ack = 1, the message has been correctly received
  	// **********************************************************************************	
  	// ******* we may not need this first message if we set autostart from POSView*******
  	// **********************************************************************************
  	
    apxNavModeMsg nMsg;
    buildNavModeMsg(&nMsg,1);
    char outNavModBuffer[NAVMOD_MSG_LENGTH];
    serialNavModeMsg(&nMsg, outNavModBuffer);
    int attempt = 0;
  	
    while ((!ackNav)&& (attempt<10))
    {
      int comStatus=apxTcpSend(&apxControlSocket, outNavModBuffer, NAVMOD_MSG_LENGTH);
      if(comStatus > 0)
      {
      	char bufferIn[ACK_MSG_LENGTH];
      	apxAckMsg aMsg;
	cout << "here we are" << endl;
  	comStatus=apxTcpReceive(&apxControlSocket, bufferIn, ACK_MSG_LENGTH );
        cout << "NavMode Communication: "<<comStatus<<endl;
        cout << strerror(errno) <<endl;
	sleep(1);
  	if (comStatus > 0)
  	{
  	  parseAckMsg(bufferIn,ACK_MSG_LENGTH,&aMsg);
	  if(aMsg.responseCode<4)
  	  {
  	    ackNav=1; //the ack is positive
  	  }
  	  else   	      
  	  {
  	    cout << "Negative Acknowledge, code: "<< aMsg.responseCode << endl;	
  	  }
        }
      }
      ++attempt;
    }	  	
    apxSelGroupsMsg gMsg;
    buildSelGroupsMsg(&gMsg,2);
  
    char outBuffer[SEL_MSG_LENGTH];
    serialSelGroupsMsg(&gMsg, outBuffer, SEL_MSG_LENGTH);
  
    
    attempt = 0;
    int ackSel = 0;
    while (ackNav && (!ackSel)&& (attempt<10))
    {
      int comStatus=apxTcpSend(&apxControlSocket, outBuffer, SEL_MSG_LENGTH);
      if(comStatus > 0)
      {
      	char bufferIn[ACK_MSG_LENGTH];
	apxAckMsg aMsg;      	  	
  	comStatus=apxTcpReceive(&apxControlSocket, bufferIn, ACK_MSG_LENGTH );
  	if (comStatus > 0)
  	{
  	  parseAckMsg(bufferIn,ACK_MSG_LENGTH,&aMsg);
  	  if(aMsg.responseCode<4)
  	  {
  	    ackSel=1; //the ack is positive
  	  }
  	  else   	      
  	  {
  	    cout << "Negative Acknowledge, code: "<< aMsg.responseCode << endl;	
  	  }
        }
      } 
      ++attempt;
    }
 
    DGClockMutex(&m_MetaStateMutex);
    _metaState.apxEnabled = ackNav+ackSel-1; // 1: Ok; 0: activated but group is not selected; -1: unable to activate
    if(_metaState.apxEnabled < 1)
    {
      cerr << "AState::apxInit(): initialization of POS-LV failed. "  << endl;
      sleep(2);
    }
    DGCunlockMutex(&m_MetaStateMutex);
  }    
}  
*/

void AState::apxReadThread()
{
  unsigned long long nowTime;
  unsigned long long rawApxTime;

  rawApx apxIn;
  apxNavData navData;
  
  int readError = 0;
  char buffer[NAV_GRP_LENGTH];
  
  while (!quitPressed) 
  { 
    int comStatus = 0;
    memset(buffer,'\0',sizeof(buffer)); //reset the buffer
    while (comStatus < 1)  
    {   
      comStatus = apxTcpReceive(&apxDataSocket, buffer, sizeof(buffer));
    }
    parseNavData(buffer, NAV_GRP_LENGTH, &navData);
    DGCgettime(rawApxTime); // time stamp as soon as data read.
    
    if(_options.logRaw==1)
    {
      apxLogStream.write((char*)&apxIn, sizeof(rawApx));
      if(apxBufferReadInd % 1500 == 1)
      {
        apxLogStream.flush();
      }
    }
    
    DGClockMutex(&m_ApxDataMutex);

    if (((apxBufferReadInd + 1) % APX_DATA_BUFFER_SIZE) == (apxBufferLastInd % APX_DATA_BUFFER_SIZE))
    {
      apxBufferFree.bCond = false;
      DGCunlockMutex(&m_ApxDataMutex);
      DGCWaitForConditionTrue(apxBufferFree);
      DGClockMutex(&m_ApxDataMutex);
    }
    ++apxBufferReadInd;
  
    memcpy(&(apxIn.data), &navData, sizeof(apxNavData));
    apxIn.time = rawApxTime;
    memcpy(&apxBuffer[apxBufferReadInd % APX_DATA_BUFFER_SIZE], &apxIn, sizeof(rawApx));
    DGCunlockMutex(&m_ApxDataMutex);

    DGCSetConditionTrue(newData);

  }   
}
  

 
/* We don't need this because nothing is going to be changed
void AState::apxControlThread()
{  
   int transNumber = 10;   //let me start from here, the first transactions are in init()
   char outBuffer[CONTROL_MSG_LENGTH];
   char inBuffer[ACK_MSG_LENGTH];
   // unsigned short control = 0;     // later on we can add different commands here
   int ackNav = 0;
   
   while(!quitPressed)
   { 
     if(DEBUG_LEVEL>4)
     {
       cout << "this is the CONTROL thread working" << endl;
       sleep(1);
     }
     apxControlMsg cMsg;
     buildControlMsg(&cMsg, transNumber, 0);
     serialControlMsg(&cMsg, outBuffer);
     ackNav = 0;
     
     int comStatus=apxTcpSend(&apxControlSocket, outBuffer, CONTROL_MSG_LENGTH);
     apxAckMsg aMsg;      	  	
  	 comStatus=apxTcpReceive(&apxControlSocket, inBuffer, ACK_MSG_LENGTH );
  	 if (comStatus > 0)
  	 {
  	   parseAckMsg(inBuffer,ACK_MSG_LENGTH,&aMsg);
  	   if(aMsg.responseCode<4)
  	   {
  	     ackNav=1; //the ack is positive
  	   }
  	   else   	      
  	   {
  	     cout << "Negative Acknowledge, code: "<< aMsg.responseCode << endl;	
  	   }
     }
     transNumber++;
   }
}	  	
*/
