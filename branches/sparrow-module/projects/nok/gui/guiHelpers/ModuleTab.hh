#include "DGCutils"
#include "DGCTab.hh"
#include "TabStruct.h"

#include <sstream>
//Obviously, the handi work of Dima
//Obviously, the Henry's typing
#define ModuleTabDummy2(x) S##x##Tab
#define ModuleTabDummy1(x) ModuleTabDummy2(x)
#define ModuleTab ModuleTabDummy1(TABNAME)


class ModuleTab : public DGCTab
{
  stringstream m_instream;
  stringstream m_outstream;
  SModuleTabInput m_inStruct;
  SModuleTabOutput m_outStruct;
#ifdef TABPLOTS
  point m_inputPoints;
#endif //TABPLOTS

public:
  /**
   * Constructor.
   *
   */
  ModuleTab(skynet* messenger)
    : DGCTab(messenger)
  {
    init();
  };
  /**
   *
   * 
   */
  void init()
  {
#define CREATEWIDGET(valueType, name, defaultValue, widgetType, xLabel, yLabel, xValue, yValue) \
	add##widgetType##Widget(#name, #defaultValue, xLabel, yLabel, xValue, yValue);
    TABINPUTS(CREATEWIDGET)    
    TABOUTPUTS(CREATEWIDGET)    

#ifdef TABPLOTS
#define ADDPLOT(title, xLabel, yLabel, xMin, xMax, majorXTick, numMinorXTicks, yMin, yMax, majorYTick, numMinorYTicks, autoscale, legend) \
        addPlot(#title, #xLabel, #yLabel, xMin, xMax, majorXTick, numMinorXTicks, yMin, yMax, majorYTick, numMinorYTicks, autoscale, #legend);
      TABPLOTS(ADDPLOT)

      m_plotSocket = m_skynet->listen(SNModulePlotInput, ALLMODULES);
    DGCstartMemberFunctionThread(this, &ModuleTab::updatePlot);
#endif //TABPLOTS

    m_inputSocket = m_skynet->listen(SNModuleTabInput, ALLMODULES);
    m_outputSocket = m_skynet->get_send_sock(SNModuleTabOutput);
    DGCstartMemberFunctionThread(this, &ModuleTab::updateTab);
  };
  /**
   *
   *
   */
  virtual void updateTab()
  {
    // make it use fixed notation
    m_instream.setf(ios::fixed, ios::floatfield);

    while(true)
      {
	m_skynet->get_msg(m_inputSocket,&m_inStruct, sizeof(m_inStruct),0);
	
#define UPDATEWIDGET(valueType, name, defaultValue, widgetType, xLabel, yLabel, xValue, yValue) \
	m_instream << m_inStruct.name; \
        setWidgetValue(m_instream, xValue, yValue); \
        m_instream.str(""); \
        m_instream.clear();

    
    TABINPUTS(UPDATEWIDGET)

      }

  };


  void updateClient()
  {
    m_outstream.str(""); 
    m_outstream.clear();

		// the __dummy_val variable is necessary to allow access to the packed structure.
#define UPDATESTRUCT(valueType, name, defaultValue, widgetType, xLabel, yLabel, xValue, yValue) \
        getWidgetValue(m_outstream, xValue, yValue); \
				{ valueType __dummy_val; m_outstream >> __dummy_val; m_outStruct.name = __dummy_val; } \
        m_outstream.str(""); \
        m_outstream.clear();
      

    TABOUTPUTS(UPDATESTRUCT)

      m_skynet->send_msg(m_outputSocket, &m_outStruct, sizeof(m_outStruct), 0);
  };

  /**
   * 
   */
  virtual void updatePlot()
  {
#ifdef TABPLOTS
    while(true)
      {
	m_skynet->get_msg(m_plotSocket, &m_inputPoints, sizeof(m_inputPoints), 0);
	m_plotArray->addDataPoint(m_inputPoints);
      }
#endif //TABPLOT   
  }
};
