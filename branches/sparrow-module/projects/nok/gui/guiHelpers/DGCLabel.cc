/**
 * DGCLabel.cc
 * Revision History:
 * 07/08/2005  hbarnor  Created
 * $Id: DGCLabel.cc 8530 2005-07-11 06:26:58Z hbarnor $
 */

#include "DGCLabel.hh"

//#warning "To be deleted" 
//#include <iostream>

DGCLabel::DGCLabel(const Glib::ustring& label)
  : DGCWidget(DGCLABEL),
    Label(label)
{
  //
}


string DGCLabel::getText()
{
  return get_text();
}

void DGCLabel::displayNewValue()
{
  //cout << "Label update display" << endl;
  set_text(m_displayValue);
}
