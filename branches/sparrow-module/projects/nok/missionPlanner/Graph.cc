/*!**
 * Morlan Liu and Nok Wongpiromsarn
 * December 8, 2006
 */

#include "Graph.hh"
#define DEBUG_LEVEL 0
using namespace std;

Graph::Graph()
{
  vertices.clear();
}

Graph::~Graph()
{
  for(unsigned i = 0; i < vertices.size(); i++)
    delete vertices[i];
}

Vertex* Graph::getVertex(int segmentID, int laneID, int waypointID)
{
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* temp = vertices[i];
    
    if(temp->getSegmentID() == segmentID &&
      temp->getLaneID() == laneID &&
      temp->getWaypointID() == waypointID)
      return temp;
  }
  
  return NULL;
}

vector<Vertex*> Graph::getVertices()
{
  return vertices;
}

int Graph::getNumVertices()
{
  return vertices.size();
}

bool Graph::addVertex(int segmentID, int laneID, int waypointID)
{
  if(getVertex(segmentID, laneID, waypointID) != NULL)
    return false;
    
  vertices.push_back(new Vertex(segmentID, laneID, waypointID));
  
  return true;
}

bool Graph::removeVertex(int segmentID, int laneID, int waypointID)
{
  Vertex* vertex = getVertex(segmentID, laneID, waypointID);
  if(vertex == NULL)
    return false;
    
  bool vertexFound = false;
  unsigned i = 0;
  while (!vertexFound && i < vertices.size())
  {
    if(vertices[i] == vertex)
    {
      vertexFound = true;
      vertices.erase(vertices.begin()+i,vertices.begin()+i+1);
    }
    i++;
  }
  
  return vertexFound;
}
  
Edge* Graph::getEdge(Vertex* vertex1, Vertex* vertex2)
{  
  if(vertex1 == NULL || vertex2 == NULL)
    return NULL;

  vector<Edge*> edges = vertex1->getEdges();
      
  unsigned i = 0;
  while (i < edges.size())
  {
    if((edges[i])->getNext() == vertex2)
    {
      return edges[i];
    }
    i++;
  }
  
  return NULL;
}

Edge* Graph::getEdge(int segmentID1, int laneID1, int waypointID1,
  int segmentID2, int laneID2, int waypointID2)
{
  Vertex* vertex1 = getVertex(segmentID1, laneID1, waypointID1);    
  Vertex* vertex2 = getVertex(segmentID2, laneID2, waypointID2);
  return getEdge(vertex1, vertex2);
}

bool Graph::addEdge(int segmentID1, int laneID1, int waypointID1,
  int segmentID2, int laneID2, int waypointID2)
{
  return addEdge(segmentID1, laneID1, waypointID1,
		 segmentID2, laneID2, waypointID2, 0, 0);
}

bool Graph::addEdge(int segmentID1, int laneID1, int waypointID1,
  int segmentID2, int laneID2, int waypointID2, double length, double weight)
{
  Vertex* vertex1 = getVertex(segmentID1, laneID1, waypointID1);
  
  if(vertex1 == NULL)
    return false;
    
  Vertex* vertex2 = getVertex(segmentID2, laneID2, waypointID2);
  
  if(vertex2 == NULL)
    return false;

  Edge* edge = new Edge(vertex1, vertex2);

  if (getEdge(segmentID1, laneID1, waypointID1, segmentID2, laneID2, waypointID2) == NULL)
  {
    vertex1->addEdge(edge);
    edge->setLength(length);
    edge->setWeight(weight);

    if (DEBUG_LEVEL > 0)
    {
      cout << "Added edge from " << segmentID1 << "." << laneID1 << "." << waypointID1 
           << " to " << segmentID2 << "." << laneID2 << "." << waypointID2 << endl;
    }
          
    return true;
  }
  else
  {
    if (DEBUG_LEVEL > 0)
    {
      cout << "Cannot add edge from " << segmentID1 << "." << laneID1 << "." << waypointID1 
	         << " to " << segmentID2 << "." << laneID2 << "." << waypointID2
           << ". This edge already exists."<< endl;
    }
    return false;
  }
}

bool Graph::addEdge(Edge* edge)
{
  Vertex* vertex1 = edge->getPrevious();
  Vertex* vertex2 = edge->getNext();

  if (getEdge(vertex1, vertex2) == NULL)
  {
    vertex1->addEdge(edge);

    if (DEBUG_LEVEL > 0)
    {
      cout << "Added edge from " << vertex1->getSegmentID() << "."
           << vertex1->getLaneID() << "." << vertex1->getWaypointID() 
           << " to " << vertex2->getSegmentID() << "." << vertex2->getLaneID() << "."
           << vertex2->getWaypointID() << endl;
    }
          
    return true;
  }
  else
  {
    if (DEBUG_LEVEL > 0)
    {
      cout << "Cannot add edge from " << vertex1->getSegmentID() << "."
           << vertex1->getLaneID() << "." << vertex1->getWaypointID()
           << " to " << vertex2->getSegmentID() << "." << vertex2->getLaneID()
           << "." << vertex2->getWaypointID() << ". This edge already exists."<< endl;
    }
    return false;
  }  
}

bool Graph::removeEdge(int segmentID1, int laneID1, int waypointID1,
  int segmentID2, int laneID2, int waypointID2)
{
  Vertex* vertex1 = getVertex(segmentID1, laneID1, waypointID1);
  
  if(vertex1 == NULL)
    return false;

  Edge* edge = getEdge(segmentID1, laneID1, waypointID1,
		       segmentID2, laneID2, waypointID2);
  if(edge == NULL)
    return false;

  return vertex1->removeEdge(edge);
}

void Graph::print()
{
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    vertices[i]->print();
    vector<Edge*> edges = vertices[i]->getEdges();
    for(unsigned j = 0; j < edges.size(); j++)
    {
      cout << endl << "  -> ";
      edges[j]->getNext()->print();
    }
    cout << endl << endl;
  }
}
