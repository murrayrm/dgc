/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include "GloNavMapTalker.hh"
#define DEBUG_LEVEL 0

using namespace std;

CGloNavMapTalker::CGloNavMapTalker()
{
  m_bufferSize = GLONAVMAP_MAX_BUFFER_SIZE;
  m_pGloNavMapDataBuffer = new char[m_bufferSize];

  DGCcreateMutex(&m_gloNavMapDataBufferMutex);
}

CGloNavMapTalker::~CGloNavMapTalker()
{
  delete [] m_pGloNavMapDataBuffer;  
  DGCdeleteMutex(&m_gloNavMapDataBufferMutex);
}

bool CGloNavMapTalker::RecvGloNavMap(int GloNavMapSocket, RNDF* receivedRndf, int* pSize)
{
  int bytesToReceive = m_bufferSize;
  char* pBuffer = m_pGloNavMapDataBuffer;
  SRNDFHeader* rndfHeader = new SRNDFHeader();
  vector<Segment*> segments;
  vector<Zone*> zones;
  vector<Waypoint*> checkpoints;
  vector<Lane*> lanes;
  vector<PerimeterPoint*> perimeterPoints;
  vector<ParkingSpot*> spots;
  vector<Waypoint*> waypoints;

  // Build the mutex list. We want to protect the data buffer.
  int numMutices = 1;
  pthread_mutex_t* ppMutices[numMutices];
  ppMutices[0] = &m_gloNavMapDataBufferMutex;

  // Get the Global Navigation Map (RNDF) data from skynet. We want to receive the whole RNDF,
  // locking the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  *pSize = m_skynet.get_msg(GloNavMapSocket, m_pGloNavMapDataBuffer, bytesToReceive, 0,
      ppMutices, false, numMutices);
      
  if(*pSize <= 0)
  {
	cerr << "CGloNavMapTalker::RecvGloNavMap(): skynet error" << endl;
	DGCunlockMutex(&m_gloNavMapDataBufferMutex);
	return false;
  }
  
  // Decode the message.
  memcpy(rndfHeader, pBuffer, sizeof(SRNDFHeader));
  pBuffer += sizeof(SRNDFHeader);
  
  int numOfSegments = rndfHeader->numOfSegments;
  int numOfZones = rndfHeader->numOfZones;
  int numOfCheckpoints = rndfHeader->numOfCheckpoints;
  int numOfLanes = rndfHeader->numOfLanes;
  int numOfPerimeterPoints = rndfHeader->numOfPerimeterPoints;
  int numOfSpots = rndfHeader->numOfSpots;
  int numOfWaypoints = rndfHeader->numOfWaypoints;
  
  for (int i = 0; i < numOfPerimeterPoints; i++)
  	perimeterPoints.push_back(new PerimeterPoint(0,0,0,0,0));
  for (int i = 0; i < numOfWaypoints; i++)
  	waypoints.push_back(new Waypoint(0,0,0,0,0));
  segments.resize(numOfSegments);
  zones.resize(numOfZones);
  lanes.resize(numOfLanes);
  spots.resize(numOfSpots);
  checkpoints.resize(numOfCheckpoints);
  
  Waypoint* tmpWaypoint = new Waypoint(0,0,0,0,0);
  for (int i = 0; i < numOfWaypoints; i++)
  {
  	int waypointSize, numOfEntries;
  	memcpy(&waypointSize, pBuffer, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(&numOfEntries, pBuffer, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(tmpWaypoint, pBuffer, waypointSize);
  	pBuffer += waypointSize;
    
    if (DEBUG_LEVEL > 0)
    {
      cout << tmpWaypoint->getSegmentID() << "." << tmpWaypoint->getLaneID() << "." 
           << tmpWaypoint->getWaypointID() << endl;
      cout << "is Entry: " << tmpWaypoint->isEntry() << endl;
      cout << "is Exit: " << tmpWaypoint->isExit() << endl;
      cout << "is Checkpoint " << tmpWaypoint->isCheckpoint() << endl;
      cout << "checkpoint ID: " << tmpWaypoint->getCheckpointID() << endl;
      cout << "is Stop sign: " << tmpWaypoint->isStopSign() << endl;
    }
    
  	waypoints[i]->setIDs(tmpWaypoint->getSegmentID(), tmpWaypoint->getLaneID(),
        tmpWaypoint->getWaypointID());
  	waypoints[i]->setNorthingEasting(tmpWaypoint->getNorthing(), tmpWaypoint->getEasting());
    
  	if (tmpWaypoint->isEntry())
    {
      waypoints[i]->setEntry();
    }
    
  	if (tmpWaypoint->isCheckpoint())
    {
      waypoints[i]->setCheckpointID(tmpWaypoint->getCheckpointID());
    }
    
  	if (tmpWaypoint->isStopSign())
    {
  		waypoints[i]->setStopSign();
    }

    if (DEBUG_LEVEL > 0)
    {
      cout << waypoints[i]->getSegmentID() << "." << waypoints[i]->getLaneID() << "."
           << waypoints[i]->getWaypointID() << endl;
      cout << "is Entry: " << waypoints[i]->isEntry() << endl;
      cout << "is Exit: " << waypoints[i]->isExit() << endl;
      cout << "is Checkpoint " << waypoints[i]->isCheckpoint() << endl;
      cout << "checkpoint ID: " << waypoints[i]->getCheckpointID() << endl;
      cout << "is Stop sign: " << waypoints[i]->isStopSign() << endl;
    }
    
  	int waypointIndex;
  	for (int j = 0; j < numOfEntries; j++)
  	{
    	memcpy(&waypointIndex, pBuffer, sizeof(int));
   		pBuffer += sizeof(int);
      if (waypointIndex < numOfWaypoints)
      {
        waypoints[i]->setExit((GPSPoint*)waypoints[waypointIndex]);
      }
	  	else
      {
        waypoints[i]->setExit((GPSPoint*)perimeterPoints[waypointIndex - numOfWaypoints]);
      }
  	}
  }
  
  PerimeterPoint* tmpPerimeterPoint = new PerimeterPoint(0,0,0,0,0);
  for (int i = 0; i < numOfPerimeterPoints; i++)
  {
  	int perimeterSize, numOfEntries;
  	memcpy(&perimeterSize, pBuffer, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(&numOfEntries, pBuffer, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(tmpPerimeterPoint, pBuffer, perimeterSize);
  	pBuffer += perimeterSize;
  	perimeterPoints[i]->setIDs(tmpPerimeterPoint->getSegmentID(),
        tmpPerimeterPoint->getLaneID(), tmpPerimeterPoint->getWaypointID());
  	perimeterPoints[i]->setNorthingEasting(tmpPerimeterPoint->getNorthing(),
        tmpPerimeterPoint->getEasting());
  	if (tmpPerimeterPoint->isEntry())
  		perimeterPoints[i]->setEntry();
  	int waypointIndex;
  	for (int j = 0; j < numOfEntries; j++)
  	{
      memcpy(&waypointIndex, pBuffer, sizeof(int));
  		pBuffer += sizeof(int);
  		if (waypointIndex < numOfWaypoints)
      {
	  		perimeterPoints[i]->setExit((GPSPoint*)waypoints[waypointIndex]);
      }
	  	else
      {
	  		perimeterPoints[i]->
            setExit((GPSPoint*)perimeterPoints[waypointIndex - numOfWaypoints]);
      }
  	}
  }
  
  for (int i = 0; i < numOfCheckpoints; i++)
  {
  	int checkpointSize = sizeof(int);
  	int waypointIndex;
  	memcpy(&waypointIndex, pBuffer, checkpointSize);
  	pBuffer += checkpointSize;
    checkpoints[i] = waypoints[waypointIndex];
  }
  
  Lane* tmpLane = new Lane();
  for (int i = 0; i < numOfLanes; i++)
  {
  	int laneSize, numOfWaypoints;
  	memcpy(&laneSize, pBuffer, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(&numOfWaypoints, pBuffer, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(tmpLane, pBuffer, laneSize);
  	pBuffer += laneSize;
  	lanes[i] = new Lane(tmpLane->getSegmentID(), tmpLane->getLaneID(), numOfWaypoints);
  	lanes[i]->setLaneWidth(tmpLane->getLaneWidth());
  	lanes[i]->setDirection(tmpLane->getDirection());
  	lanes[i]->setLeftBoundary(tmpLane->getLeftBoundary());
  	lanes[i]->setRightBoundary(tmpLane->getRightBoundary());
  	int waypointIndex;
  	for (int j = 0; j < numOfWaypoints; j++)
  	{
  		memcpy(&waypointIndex, pBuffer, sizeof(int));
  		pBuffer += sizeof(int);
  		lanes[i]->addWaypoint(waypoints[waypointIndex]);
  	}
  }

  if (DEBUG_LEVEL > 0)
  {
    for (int i = 0; i < numOfLanes; i++)
    {
    	cout << "Lane " << lanes[i]->getSegmentID() << "." << lanes[i]->getLaneID() << endl;
    	cout << "Left boundary: " << lanes[i]->getLeftBoundary() << endl;
    	cout << "Right boundary: " << lanes[i]->getRightBoundary() << endl;
    	
    	vector<Waypoint*> waypointsInLane = lanes[i]->getAllWaypoints();
    	for (unsigned j=0; j<waypointsInLane.size(); j++)
    	{
    		waypointsInLane[j]->print();
    		cout << endl;
    	}
    }
  }

  
  for (int i = 0; i < numOfSpots; i++)
  {
  	int spotSize;
  	memcpy(&spotSize, pBuffer, sizeof(int));
  	pBuffer += sizeof(int);
  	spots[i] = new ParkingSpot();
  	memcpy(spots[i], pBuffer, spotSize);
  	pBuffer += spotSize;
  	int waypointIndex;
    memcpy(&waypointIndex, pBuffer, sizeof(int));
    pBuffer += sizeof(int);
    spots[i]->setWaypoint(1, waypoints[waypointIndex]);
    memcpy(&waypointIndex, pBuffer, sizeof(int));
    pBuffer += sizeof(int);
    spots[i]->setWaypoint(2, waypoints[waypointIndex]);
  }
  
  Segment* tmpSegment = new Segment();
  for (int i = 0; i < numOfSegments; i++)
  {
  	int segmentSize;
  	memcpy(&segmentSize, pBuffer, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(tmpSegment, pBuffer, segmentSize);
  	pBuffer += segmentSize;
  	segments[i] = new Segment(tmpSegment->getSegmentID(), tmpSegment->getNumOfLanes());
  	segments[i]->setSpeedLimits(tmpSegment->getMinSpeed(), tmpSegment->getMaxSpeed());
  	int numOfLanes = tmpSegment->getNumOfLanes();
  	int laneIndex;
  	for (int j = 0; j < numOfLanes; j++)
  	{
  		memcpy(&laneIndex, pBuffer, sizeof(int));
  		pBuffer += sizeof(int);
  		segments[i]->addLane(lanes[laneIndex]);
  	}
  }
  
  Zone* tmpZone = new Zone();
  for (int i = 0; i < numOfZones; i++)
  {
  	int zoneSize, numOfPerimeterPoints;
  	memcpy(&zoneSize, pBuffer, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(&numOfPerimeterPoints, pBuffer, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(tmpZone, pBuffer, zoneSize);
  	pBuffer += zoneSize;
  	zones[i] = new Zone(tmpZone->getZoneID(), tmpZone->getNumOfSpots());
  	zones[i]->setSpeedLimits(tmpZone->getMinSpeed(), tmpZone->getMaxSpeed());
  	int myIndex;
  	for (int j = 0; j < numOfPerimeterPoints; j++)
  	{
  		memcpy(&myIndex, pBuffer, sizeof(int));
  		pBuffer += sizeof(int);
  		zones[i]->addPerimeterPoint(perimeterPoints[myIndex - numOfWaypoints]);
  	}
  	for (int j = 0; j < tmpZone->getNumOfSpots(); j++)
  	{
  		memcpy(&myIndex, pBuffer, sizeof(int));
  		pBuffer += sizeof(int);
  		zones[i]->addParkingSpot(spots[myIndex]);
  	}
  }
  
  DGCunlockMutex(&m_gloNavMapDataBufferMutex);

  if (DEBUG_LEVEL > 0)
  {
    for (int i = 0; i < numOfLanes; i++)
    {
    	cout << lanes[i]->getSegmentID() << "." << lanes[i]->getLaneID();
    	cout << endl;
    }
  }

  // Create the RNDF object
  *(receivedRndf) = RNDF(numOfSegments, numOfZones);
  for (int i=0; i < numOfSegments; i++)
  {
  	receivedRndf->addSegment(segments[i]);
  }
  for (int i=0; i < numOfZones; i++)
  {
  	receivedRndf->addZone(zones[i]);
  }
  for (int i=0; i < numOfCheckpoints; i++)
  {
  	receivedRndf->addCheckpoint(checkpoints[i]);
  }
  
  return true;
}


bool CGloNavMapTalker::SendGloNavMap(int GloNavMapSocket, RNDF* rndf)
{
  int bytesToSend = 0;
  int bytesSent;
  char* pBuffer = m_pGloNavMapDataBuffer;
  vector<Segment*> segments;
  vector<Zone*> zones;
  vector<Waypoint*> checkpoints;
  vector<Lane*> lanes;
  vector<PerimeterPoint*> perimeterPoints;
  vector<ParkingSpot*> spots;
  vector<Waypoint*> waypoints;
  SRNDFHeader* rndfHeader = new SRNDFHeader();
  int numOfSegments, numOfZones, numOfCheckpoints, numOfLanes;
  int numOfPerimeterPoints, numOfSpots, numOfWaypoints;
  
  
  // First, get the rndfHeader information
  numOfSegments = rndf->getNumOfSegments();
  numOfZones = rndf->getNumOfZones();
  
  segments = rndf->getAllSegments();
  zones = rndf->getAllZones();
  checkpoints = rndf->getAllCheckpoints();
  numOfCheckpoints = checkpoints.size();
  
  if ((unsigned)numOfSegments != segments.size())
  {
  	cerr << "SendGloNavMap Error: numOfSegments = " << numOfSegments
         << " while segments.size() = " <<	segments.size() << endl;
  	return false;
  }
  
  if ((unsigned)numOfZones != zones.size())
  {
  	cerr << "SendGloNavMap Error: numOfZones = " << numOfZones << " while zones.size() = "
  	     <<	zones.size() << endl;
  	return false;
  }
  
  // Get all the objects we want to send
  numOfLanes = 0;
  numOfWaypoints = 0;
  int lastLaneIndex = 0;
  int lastGPSPointIndex = 0;
  for (int i = 0; i < numOfSegments; i++)
  {
  	int numOfLanesInSegment = segments[i]->getNumOfLanes();
  	numOfLanes += numOfLanesInSegment;
  	vector<Lane*> lanesInSegment = segments[i]->getAllLanes();
  	if ((unsigned)numOfLanesInSegment != lanesInSegment.size())
  	{
  		cerr << "SendGloNavMap Error: segment " << segments[i]->getSegmentID()
           << " numOfLanes = " << numOfLanesInSegment << " while lanes.size() = "
           <<	lanesInSegment.size() << endl;
  		return false;
  	}
  	for (int j = 0; j < numOfLanesInSegment; j++)
  	{
  		lanesInSegment[j]->setMyIndex(lastLaneIndex);
  		lastLaneIndex++;
  		lanes.push_back(lanesInSegment[j]);
  		int numOfWaypointsInLane = lanesInSegment[j]->getNumOfWaypoints();
  		numOfWaypoints += numOfWaypointsInLane;
  		vector<Waypoint*> waypointsInLane = lanesInSegment[j]->getAllWaypoints();
  		if ((unsigned)numOfWaypointsInLane != waypointsInLane.size())
  		{
  			cerr << "SendGloNavMap Error: segment " << segments[i]->getSegmentID()
  			     << " lane " << lanesInSegment[j]->getLaneID() << " numOfWaypoints = "
  		     	 << numOfWaypointsInLane << " while waypoints.size() = "
             <<	waypointsInLane.size() << endl;
  			return false;
  		}
  		for (int k = 0; k < numOfWaypointsInLane; k++)
  		{
  			waypointsInLane[k]->setMyIndex(lastGPSPointIndex);
  			lastGPSPointIndex++;
  			waypoints.push_back(waypointsInLane[k]);
  		}		
  	}
  }
  
  numOfPerimeterPoints = 0;
  numOfSpots = 0;
  int lastSpotIndex = 0;
  for (int i = 0; i < numOfZones; i++)
  {
  	int numOfSpotsInZone = zones[i]->getNumOfSpots();
  	numOfSpots += numOfSpotsInZone;
  	vector<ParkingSpot*> spotsInZone = zones[i]->getAllSpots();
  		
  	if ((unsigned)numOfSpotsInZone != spotsInZone.size())
  	{
  		cerr << "SendGloNavMap Error: zone " << zones[i]->getZoneID() << " numOfSpots = "
  		     << numOfSpotsInZone << " while spots.size() = " <<	spotsInZone.size() << endl;
  		return false;
  	}
  	for (int j = 0; j < numOfSpotsInZone; j++)
  	{
  		spotsInZone[j]->setMyIndex(lastSpotIndex);
  		lastSpotIndex++;
  		spots.push_back(spotsInZone[j]);
  		int numOfWaypointsInSpot = 2;
  		numOfWaypoints += numOfWaypointsInSpot;
  		Waypoint* spotWaypoint1 = spotsInZone[j]->getWaypoint(1);
  		Waypoint* spotWaypoint2 = spotsInZone[j]->getWaypoint(2);
  		if (spotWaypoint1 == NULL)
  		{
  			cerr << "SendGloNavMap Error: zone " << zones[i]->getZoneID() << " parking spot "
             << spotsInZone[j]->getSpotID() << " has no waypoint1" << endl;
  			return false;
  		}
  		if (spotWaypoint2 == NULL)
  		{
  			cerr << "SendGloNavMap Error: zone " << zones[i]->getZoneID() << " parking spot "
             << spotsInZone[j]->getSpotID() << " has no waypoint2" << endl;
  			return false;
  		}
  		spotWaypoint1->setMyIndex(lastGPSPointIndex);
  		lastGPSPointIndex++;
  		spotWaypoint2->setMyIndex(lastGPSPointIndex);
  		lastGPSPointIndex++;
  		waypoints.push_back(spotWaypoint1);
  		waypoints.push_back(spotWaypoint2);
  	}
  }
  
  for (int i = 0; i < numOfZones; i++)
  {
  	int numOfPerimeterPointsInZone = zones[i]->getNumOfPerimeterPoints();
  	numOfPerimeterPoints += numOfPerimeterPointsInZone;
  	vector<PerimeterPoint*> perimeterPointsInZone = zones[i]->getAllPerimeterPoints();
  	
  	if ((unsigned)numOfPerimeterPointsInZone != perimeterPointsInZone.size())
  	{
  		cerr << "SendGloNavMap Error: zone " << zones[i]->getZoneID()
           << " numOfPerimeterPoints = " << numOfPerimeterPointsInZone
           << " while perimeterPoints.size() = " <<	perimeterPointsInZone.size() << endl;
  		return false;
  	}
  	for (int j = 0; j < numOfPerimeterPointsInZone; j++)
  	{
  		perimeterPointsInZone[j]->setMyIndex(lastGPSPointIndex);
  		lastGPSPointIndex++;
  		perimeterPoints.push_back(perimeterPointsInZone[j]);
  	}
  }
  
  /* Start encoding */
  rndfHeader->numOfSegments = numOfSegments;
  rndfHeader->numOfZones = numOfZones;
  rndfHeader->numOfCheckpoints = numOfCheckpoints;
  rndfHeader->numOfLanes = numOfLanes;
  rndfHeader->numOfPerimeterPoints = numOfPerimeterPoints;
  rndfHeader->numOfSpots = numOfSpots;
  rndfHeader->numOfWaypoints = numOfWaypoints;
  
  DGClockMutex(&m_gloNavMapDataBufferMutex);  
  memcpy(pBuffer, (char*)(rndfHeader), sizeof(SRNDFHeader));
  pBuffer += sizeof(SRNDFHeader);
  bytesToSend += sizeof(SRNDFHeader);
  
  for (int i = 0; i < numOfWaypoints; i++)
  {
  	int waypointSize = sizeof(*(waypoints[i]));
  	vector<GPSPoint*> entryWaypoints = waypoints[i]->getEntryPoints();
  	int numOfEntries = entryWaypoints.size();
  	memcpy(pBuffer, &waypointSize, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(pBuffer, &numOfEntries, sizeof(int));
  	pBuffer += sizeof(int);
  	bytesToSend += 2*sizeof(int);
  	memcpy(pBuffer, waypoints[i], waypointSize);
  	pBuffer += waypointSize;
  	bytesToSend += waypointSize;
  	for (int j = 0; j < numOfEntries; j++)
  	{
  		int myIndex = entryWaypoints[j]->getMyIndex();
  		memcpy(pBuffer, &myIndex, sizeof(int));
  		pBuffer += sizeof(int);
  		bytesToSend += sizeof(int);
  	}
  }
  
  for (int i = 0; i < numOfPerimeterPoints; i++)
  {
  	int perimeterSize = sizeof(*(perimeterPoints[i]));
  	vector<GPSPoint*> entryWaypoints = perimeterPoints[i]->getEntryPoints(); 
  	int numOfEntries = entryWaypoints.size();
  	memcpy(pBuffer, &perimeterSize, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(pBuffer, &numOfEntries, sizeof(int));
  	pBuffer += sizeof(int);
  	bytesToSend += 2*sizeof(int);
  	memcpy(pBuffer, perimeterPoints[i], perimeterSize);
  	pBuffer += perimeterSize;
  	bytesToSend += perimeterSize;
  	for (int j = 0; j < numOfEntries; j++)
  	{
  		int myIndex = entryWaypoints[j]->getMyIndex();
  		memcpy(pBuffer, &myIndex, sizeof(int));
  		pBuffer += sizeof(int);
  		bytesToSend += sizeof(int);
  	}
  }
  
  for (int i = 0; i < numOfCheckpoints; i++)
  {
  	int checkpointSize = sizeof(int);
  	int myIndex = checkpoints[i]->getMyIndex();
  	memcpy(pBuffer, &myIndex, checkpointSize);
  	pBuffer += checkpointSize;
  	bytesToSend += checkpointSize;
  }
  
  for (int i = 0; i < numOfLanes; i++)
  {
  	int laneSize = sizeof(*(lanes[i]));
  	int numOfWaypoints = lanes[i]->getNumOfWaypoints();
  	memcpy(pBuffer, &laneSize, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(pBuffer, &numOfWaypoints, sizeof(int));
  	pBuffer += sizeof(int);
  	bytesToSend += 2*sizeof(int);
  	memcpy(pBuffer, lanes[i], laneSize);
  	pBuffer += laneSize;
  	bytesToSend += laneSize;
  	vector<Waypoint*> waypointsInLane = lanes[i]->getAllWaypoints();
  	for (int j = 0; j < numOfWaypoints; j++)
  	{
  		int myIndex = waypointsInLane[j]->getMyIndex();
  		memcpy(pBuffer, &myIndex, sizeof(int));
  		pBuffer += sizeof(int);
  		bytesToSend += sizeof(int);
  	}
  }
  
  for (int i = 0; i < numOfSpots; i++)
  {
  	int spotSize = sizeof(*(spots[i]));
  	memcpy(pBuffer, &spotSize, sizeof(int));
  	pBuffer += sizeof(int);
  	bytesToSend += sizeof(int);
  	memcpy(pBuffer, spots[i], spotSize);
  	pBuffer += spotSize;
  	bytesToSend += spotSize;
	int myIndex = spots[i]->getWaypoint(1)->getMyIndex();
	memcpy(pBuffer, &myIndex, sizeof(int));
	pBuffer += sizeof(int);
	bytesToSend += sizeof(int);
	myIndex = spots[i]->getWaypoint(2)->getMyIndex();
	memcpy(pBuffer, &myIndex, sizeof(int));
	pBuffer += sizeof(int);
	bytesToSend += sizeof(int);
  }
  
  
  for (int i = 0; i < numOfSegments; i++)
  {
  	int segmentSize = sizeof(*(segments[i]));
  	memcpy(pBuffer, &segmentSize, sizeof(int));
  	pBuffer += sizeof(int);
  	bytesToSend += sizeof(int);
  	memcpy(pBuffer, segments[i], segmentSize);
  	pBuffer += segmentSize;
  	bytesToSend += segmentSize;
  	vector<Lane*> lanesInSegment = segments[i]->getAllLanes();
  	int numOfLanes = segments[i]->getNumOfLanes();
  	for (int j = 0; j < numOfLanes; j++)
  	{
  		int myIndex = lanesInSegment[j]->getMyIndex();
  		memcpy(pBuffer, &myIndex, sizeof(int));
  		pBuffer += sizeof(int);
  		bytesToSend += sizeof(int);
  	}
  }
  
  for (int i = 0; i < numOfZones; i++)
  {
  	int zoneSize = sizeof(*(zones[i]));
  	int numOfPerimeterPoints = zones[i]->getNumOfPerimeterPoints();
  	memcpy(pBuffer, &zoneSize, sizeof(int));
  	pBuffer += sizeof(int);
  	memcpy(pBuffer, &numOfPerimeterPoints, sizeof(int));
  	pBuffer += sizeof(int);
  	bytesToSend += 2*sizeof(int);\
  	memcpy(pBuffer, zones[i], zoneSize);
  	pBuffer += zoneSize;
  	bytesToSend += zoneSize;
  	vector<PerimeterPoint*> perimeterPointsInZone = zones[i]->getAllPerimeterPoints();
  	for (int j = 0; j < numOfPerimeterPoints; j++)
  	{
  		int myIndex = perimeterPointsInZone[j]->getMyIndex();
  		memcpy(pBuffer, &myIndex, sizeof(int));
  		pBuffer += sizeof(int);
  		bytesToSend += sizeof(int);
  	}
  	vector<ParkingSpot*> spotsInZone = zones[i]->getAllSpots();
  	for (int j = 0; j < zones[i]->getNumOfSpots(); j++)
  	{
  		int myIndex = spotsInZone[j]->getMyIndex();
  		memcpy(pBuffer, &myIndex, sizeof(int));
  		pBuffer += sizeof(int);
  		bytesToSend += sizeof(int);
  	}  	
  }
 
  if (bytesToSend > GLONAVMAP_MAX_BUFFER_SIZE)
  {
  	cerr << "SendGloNavMap Error: bytesToSend > GLONAVMAP_MAX_BUFFER_SIZE."
         << " Change GLONAVMAP_MAX_BUFFER_SIZE in SegGoalsTalker.hh to at least "
         << bytesToSend << endl;
  	return false;
  }

  bytesSent = m_skynet.send_msg(GloNavMapSocket, m_pGloNavMapDataBuffer, bytesToSend, 0);
  
  DGCunlockMutex(&m_gloNavMapDataBufferMutex);  
  
  // cout << m_bufferSize << " " << bytesSent;
  
  if(bytesSent != bytesToSend)
  {
    cerr << "CGloNavMapTalker::SendGloNavMap(): sent " << bytesSent
         << " bytes while expected to send " << bytesToSend << " bytes" << endl;
    return false;
  }

  return true;
}

bool CGloNavMapTalker::NewGloNavMap(int GloNavMapSocket)
{
  return m_skynet.is_msg(GloNavMapSocket);
}
