/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#include "MissionPlanner.hh"
#include "mplannertable.h"

using namespace std;

CMissionPlanner::CMissionPlanner(int skynetKey, bool bWaitForStateFill, bool nosparrow,
    char* RNDFFileName, char* MDFFileName, bool usingGloNavMapLib, int debugLevel,
    bool verbose)
    : CSkynetContainer(MODmissionplanner, skynetKey), CStateClient(bWaitForStateFill)
{
  m_snKey = skynetKey;
  m_nosparrow = nosparrow;
  m_usingGloNavMapLib = usingGloNavMapLib;
  
  DEBUG_LEVEL = debugLevel;
  
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }
  
  m_GloNavMapRequestSocket = m_skynet.get_send_sock(SNglobalGloNavMapRequest);
  m_GloNavMapSocket = m_skynet.listen(SNglobalGloNavMapFromGloNavMapLib, MODgloNavMapLib);
  m_segGoalsSocket = m_skynet.get_send_sock(SNsegGoals);

  if(m_GloNavMapSocket < 0)
  {
    cerr << "MissionPlanner::getWorldMapThread(): skynet listen returned error" << endl;
    exit(1);
  }

  DGCcreateMutex(&m_GloNavMapMutex);
  DGCcreateMutex(&m_GloNavMapReceivedMutex);
  DGCcreateMutex(&m_FirstSegGoalsStatusReceivedMutex);
  DGCcreateMutex(&m_SegGoalsSeqMutex);
  DGCcreateMutex(&m_SegGoalsStatusMutex);
  DGCcreateMutex(&m_NextCheckpointIndexMutex);
  DGCcreateMutex(&m_ExecGoalsMutex);
  DGCcreateMutex(&m_ObstaclesMutex);
  DGCcreateCondition(&m_GloNavMapReceivedCond);
  DGCcreateCondition(&m_FirstSegGoalsStatusReceivedCond);
  
  if (m_usingGloNavMapLib)
  {
    cout << "waiting for gloNavMap" << endl;
    requestGloNavMap();
    if (DEBUG_LEVEL > 0 || VERBOSITY_LEVEL > 0)
    {
      cout << "got gloNavMap" << endl;
    }
  }
  else
  {
    m_rndf = new RNDF();
    if (!m_rndf->loadFile(RNDFFileName))
    {
      cerr << "Error:  Unable to load RNDF file " << RNDFFileName
           << ", exiting program" << endl;
      exit(1);
    }
    m_rndf->assignLaneDirection();
  }
  
  m_rndfRevNum = 0;
  
  if (DEBUG_LEVEL > 1)
  {
    m_rndf->print();
  }
  else if (DEBUG_LEVEL > 0)
  {
    for (int i=1; i <= m_rndf->getNumOfSegments(); i++)
    {
      for (int j=1; j <= m_rndf->getSegment(i)->getNumOfLanes(); j++)
      {
        cout << "Lane " << i << "." << j << ":\t\t"
             << m_rndf->getLane(i,j)->getDirection() << endl;
      }
    }
  }

  if (!loadMDFFile(MDFFileName, m_rndf))
  {
    cerr << "Error: Unable to load MDF file " << MDFFileName << ", exiting program"
         << endl;
    exit(1);
  }
  
  m_checkpointGoalID = new int[m_checkpointSequence.size()];
  m_nextCheckpointIndex = 0;

  m_rndfGraph = new Graph();
  addAllVertices(m_rndf, m_rndfGraph);
  addAllEdges(m_rndf, m_rndfGraph);

  if (DEBUG_LEVEL > 2)
  {
    cout << endl << endl << "RNDFGRAPH:" << endl;
    m_rndfGraph->print();
  }
  
  m_nextGoalID = 1;

  // Initialize goals
  initializeSegGoals();

  m_receivedSegGoalsStatus = new SegGoalsStatus();
  m_numSegGoalsRequests = 0;

  // Initialize sparrow variables
  for (int i=0; i < NUM_SEGGOALS_DISPLAYED; i++)
  {
    sparrowNextGoalIDs[i] = 0;
    sparrowNextSegments1[i] = 0;
    sparrowNextLanes1[i] = 0;
    sparrowNextWaypoints1[i] = 0;
    sparrowNextSegments2[i] = 0;
    sparrowNextLanes2[i] = 0;
    sparrowNextWaypoints2[i] = 0;
  }

  m_bReceivedAtLeastOneSegGoalsStatus = false;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
CMissionPlanner::~CMissionPlanner() 
{
  delete m_rndf;
  while(m_edgesRemoved.size() > 0)
  {
    delete m_edgesRemoved.front();
    m_edgesRemoved.pop_front();
  }
  for (unsigned i=0; i < m_checkpointSequence.size(); i++)
  {
    delete m_checkpointSequence[i];
  }
  delete m_checkpointGoalID;
  delete m_rndfGraph;
  delete m_receivedSegGoalsStatus;
  for (unsigned i=0; i < m_obstacles.size(); i++)
  {
    delete m_obstacles[i];
  }
  delete shp;
  delete [] m_checkpointGoalID;
  delete [] sparrowNextGoalIDs;
  delete [] sparrowNextSegments1;
  delete [] sparrowNextLanes1;
  delete [] sparrowNextWaypoints1;
  delete [] sparrowNextSegments2;
  delete [] sparrowNextLanes2;
  delete [] sparrowNextWaypoints2;

  DGCdeleteMutex(&m_GloNavMapMutex);
  DGCdeleteMutex(&m_GloNavMapReceivedMutex);
  DGCdeleteMutex(&m_FirstSegGoalsStatusReceivedMutex);
  DGCdeleteMutex(&m_SegGoalsSeqMutex);
  DGCdeleteMutex(&m_SegGoalsStatusMutex);
  DGCdeleteMutex(&m_NextCheckpointIndexMutex);
  DGCdeleteMutex(&m_ExecGoalsMutex);
  DGCdeleteMutex(&m_ObstaclesMutex);
  DGCdeleteCondition(&m_GloNavMapReceivedCond);
  DGCdeleteCondition(&m_FirstSegGoalsStatusReceivedCond);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::SparrowDisplayLoop()
{
  // SparrowHawk display
  CSparrowHawk &sh = SparrowHawk();  
  shp = &sh;
  
  sh.add_page(mplannertable, "Main");

  sh.rebind("snkey", &m_snKey);
  sh.rebind("tplanner_goalID", &(m_segGoalsStatus.goalID));
  sh.set_string("tplanner_status", "UNKNOWN");
  sh.set_readonly("snkey");
  sh.set_readonly("tplanner_goalID");
  sh.set_readonly("tplanner_status");

  sh.rebind("nextGoal1ID", &(sparrowNextGoalIDs[0]));
  sh.rebind("nextGoal1_segment1", &(sparrowNextSegments1[0]));
  sh.rebind("nextGoal1_lane1", &(sparrowNextLanes1[0]));
  sh.rebind("nextGoal1_waypoint1", &(sparrowNextWaypoints1[0]));
  sh.rebind("nextGoal1_segment2", &(sparrowNextSegments2[0]));
  sh.rebind("nextGoal1_lane2", &(sparrowNextLanes2[0]));
  sh.rebind("nextGoal1_waypoint2", &(sparrowNextWaypoints2[0]));
  sh.set_string("nextGoal1_type", "UNKNOWN");
  sh.rebind("minSpeed", &sparrowMinSpeed);
  sh.rebind("maxSpeed", &sparrowMaxSpeed);
  sh.rebind("illegalPassingAllowed", &sparrowIllegalPassingAllowed);
  sh.rebind("stopAtExit", &sparrowStopAtExit);
  sh.rebind("isExitCheckpoint", &sparrowIsExitCheckpoint);
  sh.set_readonly("nextGoal1ID");
  sh.set_readonly("nextGoal1_segment1");
  sh.set_readonly("nextGoal1_lane1");
  sh.set_readonly("nextGoal1_waypoint1");
  sh.set_readonly("nextGoal1_segment2");
  sh.set_readonly("nextGoal1_lane2");
  sh.set_readonly("nextGoal1_waypoint2");
  sh.set_readonly("nextGoal1_type");
  sh.set_readonly("minSpeed");
  sh.set_readonly("maxSpeed");
  sh.set_readonly("illegalPassingAllowed");
  sh.set_readonly("stopAtExit");
  sh.set_readonly("isExitCheckpoint");

  sh.rebind("nextGoal2ID", &(sparrowNextGoalIDs[1]));
  sh.rebind("nextGoal2_segment1", &(sparrowNextSegments1[1]));
  sh.rebind("nextGoal2_lane1", &(sparrowNextLanes1[1]));
  sh.rebind("nextGoal2_waypoint1", &(sparrowNextWaypoints1[1]));
  sh.rebind("nextGoal2_segment2", &(sparrowNextSegments2[1]));
  sh.rebind("nextGoal2_lane2", &(sparrowNextLanes2[1]));
  sh.rebind("nextGoal2_waypoint2", &(sparrowNextWaypoints2[1]));
  sh.set_string("nextGoal2_type", "UNKNOWN");
  sh.set_readonly("nextGoal2ID");
  sh.set_readonly("nextGoal2_segment1");
  sh.set_readonly("nextGoal2_lane1");
  sh.set_readonly("nextGoal2_waypoint1");
  sh.set_readonly("nextGoal2_segment2");
  sh.set_readonly("nextGoal2_lane2");
  sh.set_readonly("nextGoal2_waypoint2");
  sh.set_readonly("nextGoal2_type");

  sh.rebind("nextGoal3ID", &(sparrowNextGoalIDs[2]));
  sh.rebind("nextGoal3_segment1", &(sparrowNextSegments1[2]));
  sh.rebind("nextGoal3_lane1", &(sparrowNextLanes1[2]));
  sh.rebind("nextGoal3_waypoint1", &(sparrowNextWaypoints1[2]));
  sh.rebind("nextGoal3_segment2", &(sparrowNextSegments2[2]));
  sh.rebind("nextGoal3_lane2", &(sparrowNextLanes2[2]));
  sh.rebind("nextGoal3_waypoint2", &(sparrowNextWaypoints2[2]));
  sh.set_string("nextGoal3_type", "UNKNOWN");
  sh.set_readonly("nextGoal3ID");
  sh.set_readonly("nextGoal3_segment1");
  sh.set_readonly("nextGoal3_lane1");
  sh.set_readonly("nextGoal3_waypoint1");
  sh.set_readonly("nextGoal3_segment2");
  sh.set_readonly("nextGoal3_lane2");
  sh.set_readonly("nextGoal3_waypoint2");
  sh.set_readonly("nextGoal3_type");

  sh.rebind("nextGoal4ID", &(sparrowNextGoalIDs[3]));
  sh.rebind("nextGoal4_segment1", &(sparrowNextSegments1[3]));
  sh.rebind("nextGoal4_lane1", &(sparrowNextLanes1[3]));
  sh.rebind("nextGoal4_waypoint1", &(sparrowNextWaypoints1[3]));
  sh.rebind("nextGoal4_segment2", &(sparrowNextSegments2[3]));
  sh.rebind("nextGoal4_lane2", &(sparrowNextLanes2[3]));
  sh.rebind("nextGoal4_waypoint2", &(sparrowNextWaypoints2[3]));
  sh.set_string("nextGoal4_type", "UNKNOWN");
  sh.set_readonly("nextGoal4ID");
  sh.set_readonly("nextGoal4_segment1");
  sh.set_readonly("nextGoal4_lane1");
  sh.set_readonly("nextGoal4_waypoint1");
  sh.set_readonly("nextGoal4_segment2");
  sh.set_readonly("nextGoal4_lane2");
  sh.set_readonly("nextGoal4_waypoint2");
  sh.set_readonly("nextGoal4_type");

  sh.rebind("nextGoal5ID", &(sparrowNextGoalIDs[4]));
  sh.rebind("nextGoal5_segment1", &(sparrowNextSegments1[4]));
  sh.rebind("nextGoal5_lane1", &(sparrowNextLanes1[4]));
  sh.rebind("nextGoal5_waypoint1", &(sparrowNextWaypoints1[4]));
  sh.rebind("nextGoal5_segment2", &(sparrowNextSegments2[4]));
  sh.rebind("nextGoal5_lane2", &(sparrowNextLanes2[4]));
  sh.rebind("nextGoal5_waypoint2", &(sparrowNextWaypoints2[4]));
  sh.set_string("nextGoal5_type", "UNKNOWN");
  sh.set_readonly("nextGoal5ID");
  sh.set_readonly("nextGoal5_segment1");
  sh.set_readonly("nextGoal5_lane1");
  sh.set_readonly("nextGoal5_waypoint1");
  sh.set_readonly("nextGoal5_segment2");
  sh.set_readonly("nextGoal5_lane2");
  sh.set_readonly("nextGoal5_waypoint2");
  sh.set_readonly("nextGoal5_type");

  sh.rebind("nextGoal6ID", &(sparrowNextGoalIDs[5]));
  sh.rebind("nextGoal6_segment1", &(sparrowNextSegments1[5]));
  sh.rebind("nextGoal6_lane1", &(sparrowNextLanes1[5]));
  sh.rebind("nextGoal6_waypoint1", &(sparrowNextWaypoints1[5]));
  sh.rebind("nextGoal6_segment2", &(sparrowNextSegments2[5]));
  sh.rebind("nextGoal6_lane2", &(sparrowNextLanes2[5]));
  sh.rebind("nextGoal6_waypoint2", &(sparrowNextWaypoints2[5]));
  sh.set_string("nextGoal6_type", "UNKNOWN");
  sh.set_readonly("nextGoal6ID");
  sh.set_readonly("nextGoal6_segment1");
  sh.set_readonly("nextGoal6_lane1");
  sh.set_readonly("nextGoal6_waypoint1");
  sh.set_readonly("nextGoal6_segment2");
  sh.set_readonly("nextGoal6_lane2");
  sh.set_readonly("nextGoal6_waypoint2");
  sh.set_readonly("nextGoal6_type");

  sh.rebind("nextCheckpointIndex", &sparrowNextCheckpointID);
  sh.rebind("nextCheckpointSegmentID", &sparrowNextCheckpointSegmentID);
  sh.rebind("nextCheckpointLaneID", &sparrowNextCheckpointLaneID);
  sh.rebind("nextCheckpointWaypointID", &sparrowNextCheckpointWaypointID);
  sh.rebind("nextCheckpointGoalID", &sparrowNextCheckpointGoalID);
  sh.set_readonly("nextCheckpointIndex");
  sh.set_readonly("nextCheckpointSegmentID");
  sh.set_readonly("nextCheckpointLaneID");
  sh.set_readonly("nextCheckpointWaypointID");
  sh.set_readonly("nextCheckpointGoalID");
  

  sh.rebind("obstacleID", &obstacleID);
  sh.rebind("obstacleSegmentID", &obstacleSegmentID);
  sh.rebind("obstacleLaneID", &obstacleLaneID);
  sh.rebind("obstacleWaypointID", &obstacleWaypointID);

  sh.set_notify("sparrowInsertObstacle", this, &CMissionPlanner::sparrowInsertObstacle);
  sh.run();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::UpdateSparrowVariablesLoop()
{
  while(true)
  {
    list<SegGoals> executingGoals;
    DGClockMutex(&m_ExecGoalsMutex);	
    if (m_executingGoals.size() > 0)
    {
      executingGoals.assign(m_executingGoals.begin(), m_executingGoals.end());
      sparrowMinSpeed = executingGoals.front().minSpeedLimit;
      sparrowMaxSpeed = executingGoals.front().maxSpeedLimit;
      sparrowIllegalPassingAllowed = executingGoals.front().illegalPassingAllowed;
      sparrowStopAtExit = executingGoals.front().stopAtExit;
      sparrowIsExitCheckpoint = executingGoals.front().isExitCheckpoint;
    }
    DGCunlockMutex(&m_ExecGoalsMutex);	

    if (m_receivedSegGoalsStatus->status == ACCEPT)
    {
      shp->set_string("tplanner_status", "ACCEPT   ");
    }
    else if (m_receivedSegGoalsStatus->status == REJECT)
    {
      shp->set_string("tplanner_status", "REJECT   ");
    }
    else if (m_receivedSegGoalsStatus->status == COMPLETED)
    {
      shp->set_string("tplanner_status", "COMPLETED");
    }
    else if (m_receivedSegGoalsStatus->status == FAILED)
    {
      shp->set_string("tplanner_status", "FAILED   ");
    }
    else
    {
      shp->set_string("tplanner_status", "UNKNOWN  ");
    }

    DGClockMutex(&m_NextCheckpointIndexMutex);
    sparrowNextCheckpointID = m_nextCheckpointIndex + 1;
    sparrowNextCheckpointSegmentID =
        m_checkpointSequence[m_nextCheckpointIndex]->getSegmentID();         
    sparrowNextCheckpointLaneID =
        m_checkpointSequence[m_nextCheckpointIndex]->getLaneID(); 
    sparrowNextCheckpointWaypointID =
        m_checkpointSequence[m_nextCheckpointIndex]->getWaypointID(); 
    sparrowNextCheckpointGoalID = m_checkpointGoalID[m_nextCheckpointIndex];
    DGCunlockMutex(&m_NextCheckpointIndexMutex);

    int i = 0;
    list<SegGoals> segGoalsSeq;	
    DGClockMutex(&m_SegGoalsSeqMutex);	
    if (m_segGoalsSeq.size() > 0)
    {
      segGoalsSeq.assign(m_segGoalsSeq.begin(), m_segGoalsSeq.end());
    }
    DGCunlockMutex(&m_SegGoalsSeqMutex);

    char* nextGoalType;

    while (i < NUM_SEGGOALS_DISPLAYED && executingGoals.size() > 0)
    {
      sparrowNextGoalIDs[i] = (executingGoals.front()).goalID;
      sparrowNextSegments1[i] = (executingGoals.front()).entrySegmentID;
      sparrowNextLanes1[i] = (executingGoals.front()).entryLaneID;
      sparrowNextWaypoints1[i] = (executingGoals.front()).entryWaypointID;
      sparrowNextSegments2[i] = (executingGoals.front()).exitSegmentID;
      sparrowNextLanes2[i] = (executingGoals.front()).exitLaneID;
      sparrowNextWaypoints2[i] = (executingGoals.front()).exitWaypointID;

      switch (i)
      {
        case 0:
          nextGoalType = "nextGoal1_type";
          break;
        case 1:
          nextGoalType = "nextGoal2_type";
          break;
        case 2:
          nextGoalType = "nextGoal3_type";
          break;
        case 3:
          nextGoalType = "nextGoal4_type";
          break;
        case 4:
          nextGoalType = "nextGoal5_type";
          break;
        default:
          nextGoalType = "nextGoal6_type";
      }

      if ((executingGoals.front()).segment_type == ROAD_SEGMENT)
      {
        shp->set_string(nextGoalType, "ROAD_SEGMENT");
      }
      else if ((executingGoals.front()).segment_type == PARKING_ZONE)
      {
        shp->set_string(nextGoalType, "PARKING_ZONE");
      }
      else if ((executingGoals.front()).segment_type == INTERSECTION)
      {
        shp->set_string(nextGoalType, "INTERSECTION");
      }
      else if ((executingGoals.front()).segment_type == PREZONE)
      {
        shp->set_string(nextGoalType, "PREZONE     ");
      }
      else if ((executingGoals.front()).segment_type == UTURN)
      {
        shp->set_string(nextGoalType, "UTURN       ");
      }
      else if ((executingGoals.front()).segment_type == PAUSE)
      {
        shp->set_string(nextGoalType, "PAUSE       ");
      }
      else if ((executingGoals.front()).segment_type == END_OF_MISSION)
      {
        shp->set_string(nextGoalType, "END_OF_MISSION");
      }
      else
      {
        shp->set_string(nextGoalType, "UNKNOWN");
      }

      executingGoals.pop_front();
      i++;
    }
    
    while (i < NUM_SEGGOALS_DISPLAYED && segGoalsSeq.size() > 0)
    {
      sparrowNextGoalIDs[i] = (segGoalsSeq.front()).goalID;
      sparrowNextSegments1[i] = (segGoalsSeq.front()).entrySegmentID;
      sparrowNextLanes1[i] = (segGoalsSeq.front()).entryLaneID;
      sparrowNextWaypoints1[i] = (segGoalsSeq.front()).entryWaypointID;
      sparrowNextSegments2[i] = (segGoalsSeq.front()).exitSegmentID;
      sparrowNextLanes2[i] = (segGoalsSeq.front()).exitLaneID;
      sparrowNextWaypoints2[i] = (segGoalsSeq.front()).exitWaypointID;

      switch (i)
      {
        case 0:
          nextGoalType = "nextGoal1_type";
          break;
        case 1:
          nextGoalType = "nextGoal2_type";
          break;
        case 2:
          nextGoalType = "nextGoal3_type";
          break;
        case 3:
          nextGoalType = "nextGoal4_type";
          break;
        case 4:
          nextGoalType = "nextGoal5_type";
          break;
        default:
          nextGoalType = "nextGoal6_type";
      }

      if ((segGoalsSeq.front()).segment_type == ROAD_SEGMENT)
      {
        shp->set_string(nextGoalType, "ROAD_SEGMENT");
      }
      else if ((segGoalsSeq.front()).segment_type == PARKING_ZONE)
      {
        shp->set_string(nextGoalType, "PARKING_ZONE");
      }
      else if ((segGoalsSeq.front()).segment_type == INTERSECTION)
      {
        shp->set_string(nextGoalType, "INTERSECTION");
      }
      else if ((segGoalsSeq.front()).segment_type == PREZONE)
      {
        shp->set_string(nextGoalType, "PREZONE     ");
      }
      else if ((segGoalsSeq.front()).segment_type == UTURN)
      {
        shp->set_string(nextGoalType, "UTURN       ");
      }
      else if ((segGoalsSeq.front()).segment_type == PAUSE)
      {
        shp->set_string(nextGoalType, "PAUSE       ");
      }
      else if ((segGoalsSeq.front()).segment_type == END_OF_MISSION)
      {
        shp->set_string(nextGoalType, "END_OF_MISSION");
      }
      else
      {
        shp->set_string(nextGoalType, "UNKNOWN");
      }

      segGoalsSeq.pop_front();
      i++;
    }
    usleep(500000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::sparrowInsertObstacle()
{  
  // Check that the segment ID, lane ID and waypoint ID are valid
  DGClockMutex(&m_GloNavMapMutex);
  int numOfSegments = m_rndf->getNumOfSegments();
  int numOfZones = m_rndf->getNumOfZones();
  int numOfSegmentsAndZones = numOfSegments + numOfZones;

  if (obstacleSegmentID < 1 || obstacleSegmentID > numOfSegmentsAndZones)
  {
    shp->log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
        obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    shp->log("Segment ID must be between 1 and %d\n", numOfSegmentsAndZones);
    DGCunlockMutex(&m_GloNavMapMutex);
    return;
  }

  if (obstacleSegmentID <= numOfSegments)
  {
    int numOfLanes = m_rndf->getSegment(obstacleSegmentID)->getNumOfLanes();
    if (obstacleLaneID < 1 || obstacleLaneID > numOfLanes)
    {
      shp->log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      shp->log("Lane ID must be between 1 and %d for segment %d\n",
          numOfLanes, obstacleSegmentID);
      DGCunlockMutex(&m_GloNavMapMutex);
      return;
    }
  }
  else if (obstacleLaneID != 0)
  {
    int numOfSpots = m_rndf->getZone(obstacleSegmentID)->getNumOfSpots();
    if (obstacleLaneID < 1 || obstacleLaneID > numOfSpots)
    {
      shp->log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      shp->log("Lane ID must be between 0 and %d for segment %d\n",
          numOfSpots, obstacleSegmentID);
      DGCunlockMutex(&m_GloNavMapMutex);
      return;
    }
  }

  if (obstacleSegmentID <= numOfSegments)
  {
    int numOfWaypoints =
        m_rndf->getLane(obstacleSegmentID, obstacleLaneID)->getNumOfWaypoints();
    if (obstacleWaypointID < 1 || obstacleWaypointID > numOfWaypoints)
    {
      shp->log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      shp->log("Waypoint ID must be between 1 and %d for segment %d lane %d\n",
          numOfWaypoints, obstacleSegmentID, obstacleLaneID);
      DGCunlockMutex(&m_GloNavMapMutex);
      return;
    }
  }
  else if (obstacleLaneID == 0)
  {
    int numOfPerimeterPoints =
        m_rndf->getZone(obstacleSegmentID)->getNumOfPerimeterPoints();
    if (obstacleWaypointID < 1 || obstacleWaypointID > numOfPerimeterPoints)
    {
      shp->log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      shp->log("Waypoint ID must be between 1 and %d for segment %d lane %d\n",
          numOfPerimeterPoints, obstacleSegmentID, obstacleLaneID);
      DGCunlockMutex(&m_GloNavMapMutex);
      return;
    }
  }
  else if (obstacleWaypointID < 1 || obstacleWaypointID > 2)
  {
    shp->log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
        obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    shp->log("Waypoint ID must be between 1 and 2 for segment %d lane %d\n",
        obstacleSegmentID, obstacleLaneID);
    DGCunlockMutex(&m_GloNavMapMutex);
    return;
  }
  DGCunlockMutex(&m_GloNavMapMutex);
  
  int obstacleIndex = getObstacleIndex(obstacleID);
  DGClockMutex(&m_ObstaclesMutex);  
  if ((unsigned)obstacleIndex < m_obstacles.size())
  {
    if (m_obstacles[obstacleIndex]->getSegmentID() != obstacleSegmentID)
    {
      int newObstacleID = obstacleID + 1;
      while((unsigned)getObstacleIndex(newObstacleID) < m_obstacles.size())
      {
        newObstacleID++;
      }
      shp->log("Obstacle ID %d already exists on segment %d. Changed obstacle ID to %d\n",
          obstacleID, m_obstacles[obstacleIndex]->getSegmentID(), newObstacleID);
      Obstacle* obstacle =
          new Obstacle(newObstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      m_obstacles.push_back(obstacle);
      shp->log("Added obstacle %d at segment %d lane %d waypoint %d to the obstacle list\n",
          newObstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    }
    else
    {
      bool obstructedLaneAdded =
          m_obstacles[obstacleIndex]->addObstructedLane(obstacleLaneID, obstacleWaypointID);
      if (obstructedLaneAdded)
      {
        shp->log("Obstacle %d on segment %d: Added obstructed lane %d waypoint %d\n",
            obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      }
      else
      {
        shp->log("Obstacle %d on segment %d: Cannot add obstructed lane %d waypoint %d.\n",
		        obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
        shp->log("This obstructed lane already exists.\n");
        shp->log("Change obstacle ID if you still want to add this obstacle");
      }
    }
  }
  else
  {
    Obstacle* obstacle =
        new Obstacle(obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    m_obstacles.push_back(obstacle);
    shp->log("Added obstacle %d at segment %d lane %d waypoint %d to the obstacle list\n",
        obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
  }
  DGCunlockMutex(&m_ObstaclesMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
int CMissionPlanner::getObstacleIndex(int obstacleID)
{
  /* DO NOT LOCK THE OBSTACLE MUTEX HERE. IT'S ALREADY LOCKED BY sparrowInsertObstacle() */
  int i = 0;
  while ((unsigned)i < m_obstacles.size() && m_obstacles[i]->getObstacleID() != obstacleID)
  {
    i++;
  }
  
  return i;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<Obstacle*> CMissionPlanner::getAllObstaclesOnSegment(int segmentID)
{
  /* DO NOT LOCK THE OBSTACLE MUTEX HERE. IT'S ALREADY LOCKED BY MPlanningLoop() */
  vector<Obstacle*> obstaclesOnSegment;
  for (unsigned i = 0; i < m_obstacles.size(); i++)
  {
    if (m_obstacles[i]->getSegmentID() == segmentID)
    {
      obstaclesOnSegment.push_back(m_obstacles[i]);
    }
  }
  
  return obstaclesOnSegment;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::getTPlannerStatusThread()
{
  // The skynet socket for receiving tplanner status
  int tplannerStatusSocket = m_skynet.listen(SNtplannerStatus, MODtrafficplanner);
  // SegGoalsStatus* m_receivedSegGoalsStatus = new SegGoalsStatus();
  if(tplannerStatusSocket < 0)
  {
    cerr << "MissionPlanner::getTPlannerStatusThread(): skynet listen returned error"
         << endl;
    exit(1);
  }

  while(true)
  {
    bool tPlannerStatusReceived =
        RecvSegGoalsStatus(tplannerStatusSocket, m_receivedSegGoalsStatus);
        
    if (tPlannerStatusReceived)
    {
      if (m_nosparrow)
      {
        cout << "Goal " << m_receivedSegGoalsStatus->goalID << " status: "
             << m_receivedSegGoalsStatus->status << endl;
      }
      else
      {
        SparrowHawk().log("Goal %d status %d\n", m_receivedSegGoalsStatus->goalID,
            (int) m_receivedSegGoalsStatus->status);
      }

      if (m_receivedSegGoalsStatus->goalID == 0)
      {
	m_numSegGoalsRequests++;
      }
      else if (m_receivedSegGoalsStatus->status == COMPLETED)
      {
        // Removed the completed goal from m_executingGoals
        DGClockMutex(&m_ExecGoalsMutex);	
        if (m_executingGoals.size() > 0)
        {
          while (m_executingGoals.size() > 0 && 
              m_receivedSegGoalsStatus->goalID != (m_executingGoals.front()).goalID)
          {
            m_executingGoals.pop_front();
          }
          if (m_executingGoals.size() == 0)
          {
            cerr << "m_executingGoals.size() == 0" << endl;
          }
          else
          {
            m_executingGoals.pop_front();
          }
        }
        else  // if (m_receivedSegGoalsStatus->goalID != 0)
        {
          cerr << "I did not send goal " << m_receivedSegGoalsStatus->goalID
               << " to traffic planner" << "." << endl;
        }
        DGCunlockMutex(&m_ExecGoalsMutex);

        // Determine if the next checkpoint is crossed.
        DGClockMutex(&m_NextCheckpointIndexMutex);
        if (m_receivedSegGoalsStatus->goalID == m_checkpointGoalID[m_nextCheckpointIndex])
        {
          if (m_nosparrow)
          {
            cout << endl << "checkpoint " << m_nextCheckpointIndex+1 << " (Waypoint " ;
            m_checkpointSequence[m_nextCheckpointIndex]->print();
            cout << ") is crossed" << endl << endl;
          }
          else
          {
            SparrowHawk().log("checkpoint %d (Waypoint %d.%d.%d) is crossed\n",
                m_nextCheckpointIndex + 1, 
                m_checkpointSequence[m_nextCheckpointIndex]->getSegmentID(), 
                m_checkpointSequence[m_nextCheckpointIndex]->getLaneID(), 
                m_checkpointSequence[m_nextCheckpointIndex]->getWaypointID());
          }
          
          if ((unsigned)m_nextCheckpointIndex + 1 < m_checkpointSequence.size())
          {
            m_nextCheckpointIndex++;
          }
          else
          {
            if (m_nosparrow)
              cout << endl << "MISSION COMPLETED !!!" << endl << endl;
            else
              SparrowHawk().log("MISSION COMPLETED !!!\n");
          }
        }
        DGCunlockMutex(&m_NextCheckpointIndexMutex);
      }
      
      DGClockMutex(&m_SegGoalsStatusMutex);
      m_segGoalsStatus.goalID = m_receivedSegGoalsStatus->goalID;
      m_segGoalsStatus.status = m_receivedSegGoalsStatus->status;
      m_segGoalsStatus.currentSegmentID = m_receivedSegGoalsStatus->currentSegmentID;
      m_segGoalsStatus.currentLaneID = m_receivedSegGoalsStatus->currentLaneID;
      m_segGoalsStatus.lastWaypointID = m_receivedSegGoalsStatus->lastWaypointID;
      // The following line causes the mission planner to be killed. Not sure why.
      //m_segGoalsStatus = *m_receivedSegGoalsStatus;
      DGCunlockMutex(&m_SegGoalsStatusMutex);

      if (!m_bReceivedAtLeastOneSegGoalsStatus)
      {
        DGCSetConditionTrue(m_bReceivedAtLeastOneSegGoalsStatus,
            m_FirstSegGoalsStatusReceivedCond, m_FirstSegGoalsStatusReceivedMutex);
      }
    }
    usleep(100000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::sendSegGoalsThread()
{
  int numSegGoalsRequests = 0;
  if (m_nosparrow)
  {
    cout << "sendSegGoalsThread(): Waiting for traffic planner" << endl;
  }
  else
  {
    SparrowHawk().log("sendSegGoalsThread(): Waiting for traffic planner\n");
  }

  // don't send goals until traffic planner starts listening
  DGCWaitForConditionTrue(m_bReceivedAtLeastOneSegGoalsStatus,
      m_FirstSegGoalsStatusReceivedCond, m_FirstSegGoalsStatusReceivedMutex);

  if (m_nosparrow)
  {
    cout << "Starting sendSegGoalsThread" << endl;
  }
  else
  {
    SparrowHawk().log("Starting sendSegGoalsThread\n");
  }

  while(true)
  {   
    DGClockMutex(&m_SegGoalsStatusMutex);
    int lastGoalID = m_segGoalsStatus.goalID;
    Status lastGoalStatus = m_segGoalsStatus.status;
    DGCunlockMutex(&m_SegGoalsStatusMutex);
    
    DGClockMutex(&m_SegGoalsSeqMutex);
    DGClockMutex(&m_ExecGoalsMutex);

    // If the tplanner just restarts, resend the goals previously sent but not completed
    if (lastGoalID == 0 && numSegGoalsRequests < m_numSegGoalsRequests)
    {
      while (m_executingGoals.size() > 0)
      {
	m_segGoalsSeq.push_front(m_executingGoals.back());
	m_executingGoals.pop_back();
      }
      if (m_segGoalsSeq.size() > 0)
      {
	lastGoalID = (m_segGoalsSeq.front()).goalID - 1;
      }
      else
      {
	lastGoalID = 0;
      }
      numSegGoalsRequests++;
    }

    if (lastGoalStatus == COMPLETED && m_segGoalsSeq.size() > 0 && 
        lastGoalID + LEAST_NUM_SEGGOALS_TPLANNER_STORED >= (m_segGoalsSeq.front()).goalID)
    {
      if (m_nosparrow)
      {
        cout << "Sending goal " << (m_segGoalsSeq.front()).goalID << " to tplanner." << endl;
      }
      else
      {
        SparrowHawk().log("Sending goal %d to tplanner\n", (m_segGoalsSeq.front()).goalID);
      }

      m_executingGoals.push_back(m_segGoalsSeq.front());
      SendSegGoals(m_segGoalsSocket, &(m_segGoalsSeq.front()));
      m_segGoalsSeq.pop_front();
    }
    DGCunlockMutex(&m_ExecGoalsMutex);	
    DGCunlockMutex(&m_SegGoalsSeqMutex);
    usleep(100000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::MPlanningLoop(void)
{
  vector<Vertex*> route;
  bool replan;
  DGClockMutex(&m_NextCheckpointIndexMutex);
  int lastPlanCheckpointIndex =  m_nextCheckpointIndex;
  DGCunlockMutex(&m_NextCheckpointIndexMutex);

  SegGoalsStatus segGoalsStatus;

  while(true)
  {
    UpdateActuatorState();

    // plan from current position if we're not in run
    // plan from previous plan if we're in run and at least one plan came through

    // if we're not in RUN, plan from veh state
/*
    if(m_actuatorState.m_estoppos != RUN)
    {
      replan = true;
      if (m_nosparrow)
        cout << "I'm not in RUN" <<endl;
      else
        SparrowHawk().log("I'm not in RUN\n");
    }
    else
*/
    replan = false;

    DGClockMutex(&m_SegGoalsStatusMutex);
    segGoalsStatus = m_segGoalsStatus;
    // Update traffic planner status to COMPLETED. If the traffic planner cannot execute
    // the new goals, the getTPlannerStatusThread will set this to false.
    m_segGoalsStatus.status = COMPLETED;  
    DGCunlockMutex(&m_SegGoalsStatusMutex);

    if(m_bReceivedAtLeastOneSegGoalsStatus && segGoalsStatus.status != COMPLETED &&
        segGoalsStatus.status != ACCEPT)
    {
      replan = true;
    }

    DGClockMutex(&m_SegGoalsSeqMutex);
    int numSegGoalsStored = m_segGoalsSeq.size();
    DGCunlockMutex(&m_SegGoalsSeqMutex);
    
    // Compute new goals starting from current position or from last waypoint
    // (given by the traffic planner) if failure occurs.    
    if (replan)
    {      
      if (m_nosparrow)
        cout << endl << "Goal " << segGoalsStatus.goalID << " failed : Replanning" << endl;
      else
        SparrowHawk().log("Goal %d failed : Replanning\n", segGoalsStatus.goalID);

      m_nextGoalID = segGoalsStatus.goalID;

      SegGoals failedGoal, executingGoal;
      list<SegGoals> executingGoals;
      DGClockMutex(&m_ExecGoalsMutex);
      executingGoals.assign(m_executingGoals.begin(), m_executingGoals.end());
      DGCunlockMutex(&m_ExecGoalsMutex);

      if (executingGoals.size() == 0)
      {
        cerr << "Cannot find the failed goal in m_executingGoals" << endl;
      }
      else
      {
        executingGoal = executingGoals.front();
        // Find the failed goal in m_executingGoals	
        if (executingGoals.size() > 0)
        {
          while (executingGoals.size() > 0 &&
              segGoalsStatus.goalID != (executingGoals.front()).goalID)
          {
            executingGoals.pop_front();
          }

          if (executingGoals.size() == 0)
          {
            cerr << "Cannot find the failed goal in m_executingGoals" << endl;
          }
          else
          {
            bool roadBlocked = false;
            failedGoal = executingGoals.front();

            // Set the obstructed level of the corresponding edge
            DGClockMutex(&m_GloNavMapMutex);
            Edge* failedEdge = m_rndfGraph->getEdge(failedGoal.entrySegmentID,
                failedGoal.entryLaneID, failedGoal.entryWaypointID,
                failedGoal.exitSegmentID, failedGoal.exitLaneID, failedGoal.exitWaypointID);
            if (failedEdge != NULL)
            {
              failedEdge->setObstructedLevel(failedEdge->getObstructedLevel() + 1);
              // If we need to replan more than the maximum number of replans allowed,
              // set the road to be fully blocked.
              if (failedEdge->getObstructedLevel() > MAX_OBSTRUCTED_LEVEL)
              {
                if (m_nosparrow)
                {
                  cout << "Setting the edge from waypoint " << failedGoal.entrySegmentID
                       << "." << failedGoal.entryLaneID << "."
                       << failedGoal.entryWaypointID << " to " << failedGoal.exitSegmentID
                       << "." << failedGoal.exitLaneID << "." << failedGoal.exitWaypointID
                       << " to be fully blocked" << endl;
              	}
                else
              	{
                  SparrowHawk().
                      log("Setting the edge from waypoint %d.%d.%d to %d.%d.%d",
                      failedGoal.entrySegmentID, failedGoal.entryLaneID,
                      failedGoal.entryWaypointID, failedGoal.exitSegmentID,
                      failedGoal.exitLaneID, failedGoal.exitWaypointID);
                  SparrowHawk().log("to be fully blocked");
                }
                roadBlocked = true;
              }
            }
            else
            {
              cerr << "Failed goal " << failedGoal.goalID << ": Edge not in graph" << endl;
            }
            DGCunlockMutex(&m_GloNavMapMutex);
            // Remove this edge from graph if the obstacle fully obstructs the road.
            // ASSUME THAT THERE IS NO OBSTACLE THAT CAN FULLY BLOCK A ZONE OR AN
            // INTERSECTION
            DGClockMutex(&m_GloNavMapMutex);
            int numOfSegments = m_rndf->getNumOfSegments();
            DGCunlockMutex(&m_GloNavMapMutex);

            if (failedGoal.entrySegmentID == failedGoal.exitSegmentID &&
                failedGoal.entrySegmentID <= numOfSegments)
            {
              DGClockMutex(&m_ObstaclesMutex);
              vector<Obstacle*> obstaclesOnFailedSegment =
                  getAllObstaclesOnSegment(failedGoal.entrySegmentID);
              DGCunlockMutex(&m_ObstaclesMutex);
              unsigned i = 0;

              // Find the waypoint on the entry lane closest to the exit point of the
              // failed goal
              GPSPoint* exitPoint;
              int lastWaypointOnFailedGoalID;
              DGClockMutex(&m_GloNavMapMutex);
              if (failedGoal.exitLaneID != 0)
              {
                exitPoint = m_rndf->getWaypoint(failedGoal.exitSegmentID,
                    failedGoal.exitLaneID, failedGoal.exitWaypointID);
              }
              else
              {
              	exitPoint = m_rndf->getPerimeterPoint(failedGoal.exitSegmentID,
                    failedGoal.exitWaypointID);
              }
              
              if (exitPoint == NULL)
              {
                cerr << "Failed goal " << failedGoal.goalID << " Exit point not in graph"
                     << endl;
              }
              else if (failedGoal.entryLaneID != failedGoal.exitLaneID)
              {
                double distance;
                GPSPoint* lastWaypointOnFailedGoal =
                    findClosestGPSPoint(exitPoint->getNorthing(), exitPoint->getEasting(), 
							      failedGoal.exitSegmentID, failedGoal.entryLaneID, distance, m_rndf);
                // Add +1 to make sure that the closest GPSPoint is at least behind the
                // exit point
                lastWaypointOnFailedGoalID = lastWaypointOnFailedGoal->getWaypointID() + 1;
              }
              else
              {
                lastWaypointOnFailedGoalID = failedGoal.exitWaypointID;
              }
              DGCunlockMutex(&m_GloNavMapMutex);

              // Determine if the obstacle fully blocks the road
              while (!roadBlocked && i < obstaclesOnFailedSegment.size())
              {
                DGClockMutex(&m_GloNavMapMutex);
                if ((obstaclesOnFailedSegment[i])->getNumOfLanesBlocked() ==
                    (m_rndf->getSegment(failedGoal.entrySegmentID))->getNumOfLanes())
                {
                  unsigned j = 0;
                  vector<int> lanesBlocked = (obstaclesOnFailedSegment[i])->getLaneIDs();
                  vector<int> waypointsBlocked = 
                      (obstaclesOnFailedSegment[i])->getWaypointIDs();
                  while (!roadBlocked && j < lanesBlocked.size())
                  {
                    if (lanesBlocked[j] == failedGoal.entryLaneID &&
                        waypointsBlocked[j] >= failedGoal.entryWaypointID &&
                        waypointsBlocked[j] < lastWaypointOnFailedGoalID)
                    {
                      roadBlocked = true;
                    }
                    j++;
                  }
                }
                DGCunlockMutex(&m_GloNavMapMutex);
                i++;
              }
            }
          
            if (roadBlocked)
            {
              if (m_nosparrow)
              {
                cout << "Removing edge from waypoint " << failedGoal.entrySegmentID << "."
                     << failedGoal.entryLaneID << "." << failedGoal.entryWaypointID << " to "
                     << failedGoal.exitSegmentID << "." << failedGoal.exitLaneID << "."
                     << failedGoal.exitWaypointID << endl;
              }
              else
              {
                SparrowHawk().log("Removing edge from waypoint %d.%d.%d to %d.%d.%d\n",
                    failedGoal.entrySegmentID, failedGoal.entryLaneID,
                    failedGoal.entryWaypointID, failedGoal.exitSegmentID,
                    failedGoal.exitLaneID, failedGoal.exitWaypointID);
              }
            
              DGClockMutex(&m_GloNavMapMutex);
              // Remove the edge corresponding to the failed segment goals from the graph
              m_rndfGraph->removeEdge(failedGoal.entrySegmentID, failedGoal.entryLaneID,
                  failedGoal.entryWaypointID, failedGoal.exitSegmentID, failedGoal.exitLaneID,
                  failedGoal.exitWaypointID);
              DGCunlockMutex(&m_GloNavMapMutex);
              m_edgesRemoved.push_back(failedEdge);
            }
          }
        }
      }
     
      resetGoals();
      DGClockMutex(&m_NextCheckpointIndexMutex);
      lastPlanCheckpointIndex =  m_nextCheckpointIndex;
      DGCunlockMutex(&m_NextCheckpointIndexMutex);
    
      int lastWaypointSegmentID = segGoalsStatus.currentSegmentID;
      int lastWaypointLaneID = segGoalsStatus.currentLaneID;
      int lastWaypointWaypointID = segGoalsStatus.lastWaypointID;
    
      if ((lastWaypointSegmentID == 0 || lastWaypointWaypointID == 0))
      {
        lastWaypointSegmentID = executingGoal.entrySegmentID;
        lastWaypointLaneID = executingGoal.entryLaneID;
        lastWaypointWaypointID = executingGoal.entryWaypointID;
      }
    
      GPSPoint* lastWaypoint;
      DGClockMutex(&m_GloNavMapMutex);
      if (lastWaypointLaneID == 0)
      {
        lastWaypoint = m_rndf->getPerimeterPoint(lastWaypointSegmentID,
            lastWaypointWaypointID);
      }
      else
      {
        lastWaypoint = m_rndf->getWaypoint(lastWaypointSegmentID, lastWaypointLaneID,
            lastWaypointWaypointID);
      }
      DGCunlockMutex(&m_GloNavMapMutex);

      // Plan from current position
      DGClockMutex(&m_GloNavMapMutex);
      vector<SegGoals> segGoalsSeq = planFromCurrentPosition(lastWaypoint,
          lastPlanCheckpointIndex);
      DGCunlockMutex(&m_GloNavMapMutex);

      DGClockMutex(&m_SegGoalsSeqMutex);
      int startPrintingIndex = (int)m_segGoalsSeq.size();
      for(unsigned i = 0; i < segGoalsSeq.size(); i++)
      {
        m_segGoalsSeq.push_back(segGoalsSeq[i]);
      }
    
      if (m_segGoalsSeq.size() > 0)
      {
        m_checkpointGoalID[lastPlanCheckpointIndex] = (m_segGoalsSeq.back()).goalID;
        m_nextGoalID = (m_segGoalsSeq.back()).goalID + 1;
      }
      else if (lastPlanCheckpointIndex > 1)
      {
        m_checkpointGoalID[lastPlanCheckpointIndex] =
            m_checkpointGoalID[lastPlanCheckpointIndex - 1];
      }
      else
      {
        m_checkpointGoalID[lastPlanCheckpointIndex] = 0;
      }
    
      // Print out the mission
      if (m_nosparrow)
      {
        printMission(m_segGoalsSeq, startPrintingIndex);
        cout<<endl;  
      }
      else
      {
        printMissionOnSparrow(m_segGoalsSeq, startPrintingIndex);
      }
    
      DGCunlockMutex(&m_SegGoalsSeqMutex);
    }

    // Update current goals and next goals when the traffic planner finish the current goals.
    else if (numSegGoalsStored < LEAST_NUM_SEGGOALS_STORED &&
        unsigned(lastPlanCheckpointIndex) + 1 < m_checkpointSequence.size())
    {
      DGClockMutex(&m_GloNavMapMutex);
      vector<SegGoals> segGoalsSeq = planNextMission(lastPlanCheckpointIndex);
      DGCunlockMutex(&m_GloNavMapMutex);

      DGClockMutex(&m_SegGoalsSeqMutex);
      int startPrintingIndex = (int)m_segGoalsSeq.size();
      for(unsigned i = 0; i < segGoalsSeq.size(); i++)
      {
        m_segGoalsSeq.push_back(segGoalsSeq[i]);
      }
      
      if (m_segGoalsSeq.size() > 0)
      {
      	m_checkpointGoalID[lastPlanCheckpointIndex+1] = (m_segGoalsSeq.back()).goalID;
      	m_nextGoalID = (m_segGoalsSeq.back()).goalID + 1;
      }
      else
      {
      	m_checkpointGoalID[lastPlanCheckpointIndex+1] =
            m_checkpointGoalID[lastPlanCheckpointIndex];
      }
      
      // Print out the mission
      if (m_nosparrow)
      {
        printMission(m_segGoalsSeq, startPrintingIndex);
        cout << endl;  
      }
      else
      {
      	printMissionOnSparrow(m_segGoalsSeq, startPrintingIndex);
      }
      	
      DGCunlockMutex(&m_SegGoalsSeqMutex);
      lastPlanCheckpointIndex++;
    }
    usleep(100000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::requestGloNavMap()
{
  if (!m_usingGloNavMapLib)
  	return;
  	
  int deltasize;
  RNDF* receivedRndf = new RNDF();

  bool bRequestMap = true;
  //m_skynet.send_msg(m_GloNavMapRequestSocket,&bRequestMap, sizeof(bool) , 0);

  if (m_nosparrow)
    cout << "Waiting for Global Navigation Map" << endl;
  else
    SparrowHawk().log( "Waiting for Global Navigation Map\n");

  while (!NewGloNavMap(m_GloNavMapSocket))
  {
    m_skynet.send_msg(m_GloNavMapRequestSocket,&bRequestMap, sizeof(bool) , 0);
    sleep(5);
  }

  bool GloNavMapReceived = RecvGloNavMap(m_GloNavMapSocket, receivedRndf, &deltasize);

  if (GloNavMapReceived)
  {
    if (m_nosparrow)
      cout << "Received a new GloNav map" << endl;
    else
      SparrowHawk().log( "Received a new GloNav map\n");
    
    DGClockMutex(&m_GloNavMapMutex);
    m_rndf = receivedRndf;
    DGCunlockMutex(&m_GloNavMapMutex);

    // set the condition to signal that the first world map was received
    if (!m_bReceivedAtLeastOneGloNavMap)
    {
      DGCSetConditionTrue(m_bReceivedAtLeastOneGloNavMap, m_GloNavMapReceivedCond,
      m_GloNavMapReceivedMutex);
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::initializeSegGoals()
{
  DGClockMutex(&m_GloNavMapMutex);
  vector<SegGoals> currentMission = planFromCurrentPosition(NULL, m_nextCheckpointIndex);
  DGCunlockMutex(&m_GloNavMapMutex);
  DGClockMutex(&m_SegGoalsSeqMutex);
  for(unsigned i = 0; i < currentMission.size(); i++)
  {
    m_segGoalsSeq.push_back(currentMission[i]);
  }

  DGClockMutex(&m_NextCheckpointIndexMutex);
  m_checkpointGoalID[m_nextCheckpointIndex] = (m_segGoalsSeq.back()).goalID;
  m_nextGoalID = (m_segGoalsSeq.back()).goalID + 1;
  DGCunlockMutex(&m_NextCheckpointIndexMutex);

  // Print out the mission
  if (m_nosparrow)
  {
    printMission(m_segGoalsSeq, 0);
    cout<<endl;  
  }
  else
  {
  	printMissionOnSparrow(m_segGoalsSeq, 0);
  }
  DGCunlockMutex(&m_SegGoalsSeqMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> CMissionPlanner::planFromCurrentPosition(GPSPoint* lastWaypoint,
    int nextCheckpointIndex)
{
  double distance, cost;
  vector<Vertex*> route;
  Waypoint* nextCheckpoint = m_checkpointSequence[nextCheckpointIndex];
  vector<SegGoals> segGoalsSeq;
  GPSPoint* closestGPSPoint;
  bool pointAdded;
  Vertex* vertex1;

  UpdateState();

  if (lastWaypoint == NULL)
  {
    closestGPSPoint = findClosestGPSPoint(m_state.Northing_front(), m_state.Easting_front(),
        0, 0, distance, m_rndf);
  }
  else if (lastWaypoint->getLaneID() != 0)
  {
    closestGPSPoint = m_rndf->getWaypoint(lastWaypoint->getSegmentID(),
        lastWaypoint->getLaneID(), lastWaypoint->getWaypointID());
  }
  else
  {
    closestGPSPoint = m_rndf->getPerimeterPoint(lastWaypoint->getSegmentID(), 
        lastWaypoint->getWaypointID());
  }
  
  pointAdded = addGPSPointToGraph (closestGPSPoint, m_rndfGraph, m_rndf);
  vertex1 = m_rndfGraph->getVertex(closestGPSPoint->getSegmentID(),
      closestGPSPoint->getLaneID(), closestGPSPoint->getWaypointID());

  Vertex* vertex2 = m_rndfGraph->getVertex(nextCheckpoint->getSegmentID(),
      nextCheckpoint->getLaneID(), nextCheckpoint->getWaypointID());
  if (vertex2 == NULL)
  {
    addGPSPointToGraph (nextCheckpoint, m_rndfGraph, m_rndf);
    vertex2 = m_rndfGraph->getVertex(nextCheckpoint->getSegmentID(),
        nextCheckpoint->getLaneID(), nextCheckpoint->getWaypointID());
    if (m_nosparrow)
    {
      cout << "Checkpoint ";
      nextCheckpoint->print();
      cout << " is not in the RNDFGraph." << endl;
    }
    else
    {
      SparrowHawk().log("Checkpoint %d.%d.%d is not in the RNDFGraph \n", 
			   nextCheckpoint->getSegmentID(), nextCheckpoint->getLaneID(),
         nextCheckpoint->getWaypointID() );
    }
  }

  bool routeFound = findRoute(vertex1, vertex2, m_rndf, m_rndfGraph, route, cost);
  if (!routeFound)
  {
    // Add uturn to segGoalsSeq
    segGoalsSeq = addUturn(closestGPSPoint, nextCheckpoint, vertex2);
    if (segGoalsSeq.size() == 0)
    {
      if (m_edgesRemoved.size() > 0)
      {
        Edge* edgeToAdd = m_edgesRemoved.front();
        if (m_nosparrow)
        {
          cout << "Cannot find route after allowing u-turn: Adding the removed edge from "
               << edgeToAdd->getPrevious()->getSegmentID()
               << "." << edgeToAdd->getPrevious()->getLaneID() << "."
               << edgeToAdd->getPrevious()->getWaypointID() << " to "
               << edgeToAdd->getNext()->getSegmentID() << "."
               << edgeToAdd->getNext()->getLaneID() << "."
               << edgeToAdd->getNext()->getWaypointID() << " to the graph" << endl;
        }
        else
        {
          SparrowHawk().log("Cannot find route after allowing u-turn");
          SparrowHawk().log("Adding the removed edge from %d.%d.%d to %d.%d.%d to the graph",
              edgeToAdd->getPrevious()->getSegmentID(),
              edgeToAdd->getPrevious()->getLaneID(),
              edgeToAdd->getPrevious()->getWaypointID(),
              edgeToAdd->getNext()->getSegmentID(),
              edgeToAdd->getNext()->getLaneID(), edgeToAdd->getNext()->getWaypointID());
        }
        edgeToAdd->setObstructedLevel(MAX_OBSTRUCTED_LEVEL);
        m_rndfGraph->addEdge(edgeToAdd);
        m_edgesRemoved.pop_front();
        segGoalsSeq = planFromCurrentPosition(lastWaypoint, nextCheckpointIndex);
      }
      else
      {
        cerr << "Cannot find route after adding all the removed edges back to the graph" 
             << endl;
        exit(1);
      }
    }
  }
  else if (lastWaypoint == NULL)
  {
    segGoalsSeq = findSegGoals(route, m_rndf, m_rndfGraph, m_nextGoalID + 1);
    SegGoals firstSeg;
    firstSeg.goalID = m_nextGoalID;
    firstSeg.entrySegmentID = 0;
    firstSeg.entryLaneID = 0;
    firstSeg.entryWaypointID = 0;
    if (segGoalsSeq.size() > 0)
    {
      firstSeg.exitSegmentID = segGoalsSeq[0].entrySegmentID;
      firstSeg.exitLaneID = segGoalsSeq[0].entryLaneID;
      firstSeg.exitWaypointID = segGoalsSeq[0].entryWaypointID;
      firstSeg.minSpeedLimit = 0;
    }
    else
    {
      firstSeg.exitSegmentID = route[0]->getSegmentID();
      firstSeg.exitLaneID = route[0]->getLaneID();
      firstSeg.exitWaypointID = route[0]->getWaypointID();
      firstSeg.minSpeedLimit = 0;      
    }
    if (firstSeg.exitSegmentID <= m_rndf->getNumOfSegments())
    {
      firstSeg.maxSpeedLimit = m_rndf->getSegment(firstSeg.exitSegmentID)->getMinSpeed();
    }
    else
    {
      firstSeg.maxSpeedLimit = m_rndf->getZone(firstSeg.exitSegmentID)->getMaxSpeed();
    }
    firstSeg.segment_type = PARKING_ZONE;
    firstSeg.illegalPassingAllowed = false;
    if (firstSeg.exitLaneID == 0)
    {
      firstSeg.stopAtExit = false;
    }
    else
    {
      firstSeg.stopAtExit = m_rndf->getWaypoint(firstSeg.exitSegmentID,
          firstSeg.exitLaneID, firstSeg.exitWaypointID)->isStopSign();
    }
    firstSeg.isExitCheckpoint = false;
    segGoalsSeq.insert(segGoalsSeq.begin(), firstSeg);
    if (m_nosparrow)
    {
      cout << "Drive from current position (" << m_state.Northing_front() << ", "
           << m_state.Easting_front() << ") " << distance << " meters to waypoint ";
      closestGPSPoint->print();
      cout << " at (" << closestGPSPoint->getNorthing() << ", "
           << closestGPSPoint->getEasting() << ")" << endl << endl;
    }
    else
    {
      SparrowHawk().log("Drive from current position (%4f, %4f)",
          m_state.Northing_front(), m_state.Easting_front());
      SparrowHawk().log("%4f meters", distance);
      SparrowHawk().log("to waypoint %d.%d.%d at (%4f, %4f) \n\n",
          closestGPSPoint->getSegmentID(), closestGPSPoint->getLaneID(), 
			closestGPSPoint->getWaypointID(), closestGPSPoint->getNorthing(),
          closestGPSPoint->getEasting());
    }
  }
  else
  {
    segGoalsSeq = findSegGoals(route, m_rndf, m_rndfGraph, m_nextGoalID);
    if (segGoalsSeq.size() > 0)
    {
    	segGoalsSeq[0].entrySegmentID = 0;
    	segGoalsSeq[0].entryLaneID = 0;
    	segGoalsSeq[0].entryWaypointID = 0;
    }
  }

  if (segGoalsSeq.size() > 0)
  {
  	// The exit point of the last segment goal is a checkpoint
  	segGoalsSeq[segGoalsSeq.size()-1].isExitCheckpoint = true;
  }

  // If this is the last checkpoint, add END_OF_MISSION goal.
  if ((unsigned)nextCheckpointIndex + 1 == m_checkpointSequence.size())
  {
    addEndOfMission(segGoalsSeq);
  }
      
  if (pointAdded)
  {
    m_rndfGraph->removeVertex(closestGPSPoint->getSegmentID(),
        closestGPSPoint->getLaneID(), closestGPSPoint->getWaypointID());
  }


  if (m_nosparrow)
  {
    cout << "Mission " << nextCheckpointIndex + 1;
    cout << ": from waypoint "<<  closestGPSPoint->getSegmentID() << "."
         << closestGPSPoint->getLaneID() << "." << closestGPSPoint->getWaypointID();
    cout << " to waypoint " << nextCheckpoint->getSegmentID() << "."
         << nextCheckpoint->getLaneID() << "."
         << nextCheckpoint->getWaypointID() << endl;
  }
  else
  {
  	SparrowHawk().log("Mission %d: from waypoint %d.%d.%d to waypoint %d.%d.%d",
        nextCheckpointIndex + 1, closestGPSPoint->getSegmentID(),
  		  closestGPSPoint->getLaneID(), closestGPSPoint->getWaypointID(),
        nextCheckpoint->getSegmentID(), nextCheckpoint->getLaneID(),
        nextCheckpoint->getWaypointID());
  }


  return segGoalsSeq;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> CMissionPlanner::planNextMission(int currentCheckpointIndex)
{
  vector<Vertex*> route;
  double cost;
  Waypoint* currentCheckpoint = m_checkpointSequence[currentCheckpointIndex];
  Waypoint* nextCheckpoint = m_checkpointSequence[currentCheckpointIndex + 1];
  vector<SegGoals> segGoalsSeq;

  Vertex* vertex1 = m_rndfGraph->getVertex(currentCheckpoint->getSegmentID(),
      currentCheckpoint->getLaneID(), currentCheckpoint->getWaypointID());
  Vertex* vertex2 = m_rndfGraph->getVertex(nextCheckpoint->getSegmentID(),
      nextCheckpoint->getLaneID(), nextCheckpoint->getWaypointID());

  if (vertex1 == NULL)
  {
    addGPSPointToGraph(currentCheckpoint, m_rndfGraph, m_rndf);
    vertex1 = m_rndfGraph->getVertex(currentCheckpoint->getSegmentID(),
        currentCheckpoint->getLaneID(), currentCheckpoint->getWaypointID());
    if (m_nosparrow)
    {
      cout << "Checkpoint ";
      currentCheckpoint->print();
      cout << " is not in the RNDFGraph." << endl;
    }
    else
    {
      SparrowHawk().log("Checkpoint %d.%d.%d is not in the RNDFGraph \n", 
			currentCheckpoint->getSegmentID(), currentCheckpoint->getLaneID(),
          currentCheckpoint->getWaypointID() );    
    }
  }
  if (vertex2 == NULL)
  {
    addGPSPointToGraph(nextCheckpoint, m_rndfGraph, m_rndf);
    vertex2 = m_rndfGraph->getVertex(nextCheckpoint->getSegmentID(),
        nextCheckpoint->getLaneID(), nextCheckpoint->getWaypointID());
    if (m_nosparrow)
    {
      cout << "Checkpoint ";
      nextCheckpoint->print();
      cout << " is not in the RNDFGraph." << endl;
    }
    else
    {
      SparrowHawk().log("Checkpoint %d.%d.%d is not in the RNDFGraph \n", 
			nextCheckpoint->getSegmentID(), nextCheckpoint->getLaneID(),
          nextCheckpoint->getWaypointID() );
    }
  }

  bool routeFound = findRoute(vertex1, vertex2, m_rndf, m_rndfGraph, route, cost);
  if (!routeFound)
  {
    // Add uturn to segGoalsSeq
    segGoalsSeq = addUturn(currentCheckpoint, nextCheckpoint, vertex2);
    if (segGoalsSeq.size() == 0)
    {
      if (m_edgesRemoved.size() > 0)
      {
        Edge* edgeToAdd = m_edgesRemoved.front();
        if (m_nosparrow)
        {
          cout << "Cannot find route after allowing u-turn: Adding the removed edge from "
               << edgeToAdd->getPrevious()->getSegmentID() << "."
               << edgeToAdd->getPrevious()->getLaneID() << "."
               << edgeToAdd->getPrevious()->getWaypointID() << " to "
               << edgeToAdd->getNext()->getSegmentID() << "."
               << edgeToAdd->getNext()->getLaneID() << "."
               << edgeToAdd->getNext()->getWaypointID() << " to the graph" << endl;
        }
        else
        {
          SparrowHawk().log("Cannot find route after allowing u-turn");
          SparrowHawk().log("Adding the removed edge from %d.%d.%d to %d.%d.%d to the graph",
              edgeToAdd->getPrevious()->getSegmentID(),
              edgeToAdd->getPrevious()->getLaneID(),
              edgeToAdd->getPrevious()->getWaypointID(),
              edgeToAdd->getNext()->getSegmentID(),
              edgeToAdd->getNext()->getLaneID(), edgeToAdd->getNext()->getWaypointID());
        }
        edgeToAdd->setObstructedLevel(MAX_OBSTRUCTED_LEVEL);
        m_rndfGraph->addEdge(edgeToAdd);
        m_edgesRemoved.pop_front();
        segGoalsSeq = planNextMission(currentCheckpointIndex);
      }
      else
      {
        cerr << "Cannot find route after adding all the removed edges back to the graph"
             << endl;
        exit(1);
      }
    }
  }
  else
  {
    segGoalsSeq = findSegGoals(route, m_rndf, m_rndfGraph, m_nextGoalID);
  }

  // The exit point of the last segment goal is a checkpoint
  if (segGoalsSeq.size() > 0)
  {
    segGoalsSeq[segGoalsSeq.size()-1].isExitCheckpoint = true;
  }
  
  // If this is the last checkpoint, add END_OF_MISSION goal.
  if ((unsigned)currentCheckpointIndex+2 == m_checkpointSequence.size())
  {
    addEndOfMission(segGoalsSeq);
  }

  if (m_nosparrow)
  {
    cout << endl <<  "Mission " << currentCheckpointIndex + 2;
    if ((unsigned)currentCheckpointIndex+2 == m_checkpointSequence.size())
    {
      cout << " (last mission)";
    }
    cout << ": from waypoint " << currentCheckpoint->getSegmentID() << "."
         << currentCheckpoint->getLaneID() << "." << currentCheckpoint->getWaypointID();
    cout << " to waypoint " << nextCheckpoint->getSegmentID() << "."
         << nextCheckpoint->getLaneID() << "." << nextCheckpoint->getWaypointID() << endl;
  }
  else
  {
  	SparrowHawk().log("Mission %d: from waypoint %d.%d.%d to waypoint %d.%d.%d",
        currentCheckpointIndex + 2, currentCheckpoint->getSegmentID(),
        currentCheckpoint->getLaneID(), currentCheckpoint->getWaypointID(),
        nextCheckpoint->getSegmentID(), nextCheckpoint->getLaneID(),
        nextCheckpoint->getWaypointID());
  }
  
  return segGoalsSeq;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> CMissionPlanner::addUturn(GPSPoint* waypoint1, GPSPoint* waypoint2,
    Vertex* vertex2)
{
  if (waypoint1->getSegmentID() > m_rndf->getNumOfSegments())
  {
    cerr << "Cannot make uturn in a zone" << endl;
    exit(1);
  }

  vector<Vertex*> route;
  vector<SegGoals> segGoalsSeq;
  double cost;
  bool routeFound = false;
  Vertex* vertex1;

  if (m_nosparrow)
  {
    cout << "no route from ";
    waypoint1->print();
    cout << " to ";
    waypoint2->print();
    cout << " without making u-turn" << endl;
  }
  else
  {
    SparrowHawk().log("no route from %d.%d.%d to %d.%d.%d without making u-turn\n",
        waypoint1->getSegmentID(), waypoint1->getLaneID(), waypoint1->getWaypointID(),
        waypoint2->getSegmentID(), waypoint2->getLaneID(), waypoint2->getWaypointID());
  }
  
  vector<Lane*> nonAdjLanes = getNonAdjacentLanes(m_rndf->getLane(waypoint1->getSegmentID(), 
								  waypoint1->getLaneID()), m_rndf);
  unsigned nonAdjLaneIndex = 0;
  while (!routeFound && nonAdjLaneIndex < nonAdjLanes.size())
  {
    double distance;
    GPSPoint* uturnGPSPoint = findClosestGPSPoint(waypoint1->getNorthing(),
        waypoint1->getEasting(), waypoint1->getSegmentID(), 
        nonAdjLanes[nonAdjLaneIndex]->getLaneID(), distance, m_rndf);
    bool uturnPointAdded = addGPSPointToGraph (uturnGPSPoint, m_rndfGraph, m_rndf);
    vertex1 = m_rndfGraph->getVertex(uturnGPSPoint->getSegmentID(),
        uturnGPSPoint->getLaneID(), uturnGPSPoint->getWaypointID());
    routeFound = findRoute(vertex1, vertex2, m_rndf, m_rndfGraph, route, cost);
    if (uturnPointAdded)
    {
      m_rndfGraph->removeVertex(uturnGPSPoint->getSegmentID(), uturnGPSPoint->getLaneID(),
          uturnGPSPoint->getWaypointID());
    }
    nonAdjLaneIndex++;
  }

  if (!routeFound)
  {
    return segGoalsSeq;
  }
  
  segGoalsSeq = findSegGoals(route, m_rndf, m_rndfGraph, m_nextGoalID + 1);
  SegGoals firstSeg;
  firstSeg.goalID = m_nextGoalID;
  firstSeg.entrySegmentID = 0;
  firstSeg.entryLaneID = 0;
  firstSeg.entryWaypointID = 0;
  firstSeg.exitSegmentID = segGoalsSeq[0].entrySegmentID;
  firstSeg.exitLaneID = segGoalsSeq[0].entryLaneID;
  firstSeg.exitWaypointID = segGoalsSeq[0].entryWaypointID;
  firstSeg.minSpeedLimit = 0;
  if (segGoalsSeq[0].entrySegmentID <= m_rndf->getNumOfSegments())
  {
    firstSeg.maxSpeedLimit =
        m_rndf->getSegment(segGoalsSeq[0].entrySegmentID)->getMinSpeed();
  }
  else
  {
    firstSeg.maxSpeedLimit = m_rndf->getZone(segGoalsSeq[0].entrySegmentID)->getMaxSpeed();
  }
  firstSeg.segment_type = UTURN;
  firstSeg.illegalPassingAllowed = false;
  if (firstSeg.exitLaneID == 0)
  {
    firstSeg.stopAtExit = false;
  }
  else
  {
    firstSeg.stopAtExit = m_rndf->getWaypoint(firstSeg.exitSegmentID, firstSeg.exitLaneID,
        firstSeg.exitWaypointID)->isStopSign();
  }
  firstSeg.isExitCheckpoint = false;
  segGoalsSeq.insert(segGoalsSeq.begin(), firstSeg);

  return segGoalsSeq;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::addEndOfMission(vector<SegGoals>& segGoalsSeq)
{
  SegGoals lastSeg;
  lastSeg.goalID = segGoalsSeq[segGoalsSeq.size()-1].goalID + 1;;
  lastSeg.entrySegmentID = 0;
  lastSeg.entryLaneID = 0;
  lastSeg.entryWaypointID = 0;
  lastSeg.exitSegmentID = 0;
  lastSeg.exitLaneID = 0;
  lastSeg.exitWaypointID = 0;
  lastSeg.minSpeedLimit = 0;
  lastSeg.maxSpeedLimit = 0;
  lastSeg.segment_type = END_OF_MISSION;
  lastSeg.illegalPassingAllowed = false;
  lastSeg.stopAtExit = true;
  lastSeg.isExitCheckpoint = false;
  segGoalsSeq.push_back(lastSeg);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::resetGoals()
{
  DGClockMutex(&m_SegGoalsSeqMutex);
  m_segGoalsSeq.clear();
  DGCunlockMutex(&m_SegGoalsSeqMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool CMissionPlanner::loadMDFFile(char* fileName, RNDF* rndf)
{
  ifstream file;
  string line;
  string word;

  file.open(fileName, ios::in);

  if(!file)
  {
    cerr << "Error: " << fileName << " file not found." << endl;
    return false;
  } 
  
  getline(file, line);
    
  istringstream lineStream(line, ios::in);
  
  lineStream >> word;

  if(word == "MDF_name")
  {
    parseCheckpoint(&file, rndf);
    
    parseSpeedLimit(&file, rndf);
    
    file.close();
    
    return true;
  }
  else
  {
  	file.close();
    return false;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::parseCheckpoint(ifstream* file, RNDF* rndf)
{
  string line, word;
  char letter;
  int ckptID;  
  
  while(word != "end_checkpoints")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      ckptID = atoi(word.c_str());
      m_checkpointSequence.push_back(rndf->getCheckpoint(ckptID));
    }
    else
    {
      continue;
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::parseSpeedLimit(ifstream* file, RNDF* rndf)
{
  int segmentID;
  double minSpeed, maxSpeed;
  string line, word;
  char letter;
  
  while(word != "end_speed_limits")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    if(letter >= '0' && letter <= '9')
    {
      lineStream >> segmentID;
      lineStream >> minSpeed;
      lineStream >> maxSpeed;

      minSpeed = minSpeed * MPS_PER_MPH;
      maxSpeed = maxSpeed * MPS_PER_MPH;
      
      setSpeedLimits(segmentID, minSpeed, maxSpeed, rndf);
    }
    else
    {
      lineStream >> word;
      continue;      
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::setSpeedLimits(int segOrZoneID, double minSpeed, double maxSpeed,
    RNDF* rndf)
{
  if(segOrZoneID <= rndf->getNumOfSegments() + rndf->getNumOfZones() &&
      segOrZoneID > rndf->getNumOfSegments())
  {
    Zone* zone = rndf->getZone(segOrZoneID);
    zone->setSpeedLimits(minSpeed, maxSpeed);

    if (DEBUG_LEVEL > 2 && m_nosparrow)
    {
      cout << "The minimum speed of zone " << segOrZoneID << " is " << minSpeed
           << " mph, the maximum speed is " << maxSpeed << " mph." << endl;
    }
  }
  else
  {
    Segment* segment = rndf->getSegment(segOrZoneID);
    segment->setSpeedLimits(minSpeed, maxSpeed);
    
    
    if (DEBUG_LEVEL > 2 && m_nosparrow)
    {
      cout << "The minimum speed of segment " << segOrZoneID << " is " << minSpeed
           << " mph, the maximum speed is " << maxSpeed << " mph." << endl;
    }
  }  
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void CMissionPlanner::printMissionOnSparrow(list<SegGoals> segGoals, int startIndex)
{
  int i = 0;
  while (segGoals.size() > 0 && i < startIndex)
  {
    segGoals.pop_front();
    i++;
  }

  int goalID;
  int entrySegmentID, entryLaneID, entryWaypointID;
  int exitSegmentID, exitLaneID, exitWaypointID;
  double minSpeedLimit, maxSpeedLimit;
  bool illegalPassingAllowed, stopAtExit, isExitCheckpoint;
  SegmentType segment_type;
  
  while (segGoals.size() > 0)
  {
    SegGoals currentGoal = segGoals.front();
  	goalID = currentGoal.goalID;
  	entrySegmentID = currentGoal.entrySegmentID;
  	entryLaneID = currentGoal.entryLaneID;
  	entryWaypointID = currentGoal.entryWaypointID;
  	exitSegmentID = currentGoal.exitSegmentID;
  	exitLaneID = currentGoal.exitLaneID;
  	exitWaypointID = currentGoal.exitWaypointID;
  	minSpeedLimit = currentGoal.minSpeedLimit;
  	maxSpeedLimit = currentGoal.maxSpeedLimit;
  	illegalPassingAllowed = currentGoal.illegalPassingAllowed;
  	stopAtExit = currentGoal.stopAtExit;
  	isExitCheckpoint = currentGoal.isExitCheckpoint;
  	segment_type = currentGoal.segment_type;
    SparrowHawk().log("GOAL %d: %d.%d.%d -> %d.%d.%d",  goalID, entrySegmentID, entryLaneID,
        entryWaypointID, exitSegmentID, exitLaneID, exitWaypointID);
    switch(segment_type)
    {
      case ROAD_SEGMENT:
        SparrowHawk().log("  ROAD_SEGMENT");
        break;
      case PARKING_ZONE:
        SparrowHawk().log("  PARKING_ZONE");
        break;
      case INTERSECTION:
        SparrowHawk().log("  INTERSECTION");
        break;
      case PREZONE:
        SparrowHawk().log("  PREZONE");
        break;
      case UTURN:
        SparrowHawk().log("  UTURN");
        break;
      case PAUSE:
        SparrowHawk().log("  PAUSE");
        break;
      case END_OF_MISSION:
        SparrowHawk().log("  END_OF_MISSION");
        break;
      default:
        SparrowHawk().log("  UNKNOWN");
    }
    SparrowHawk().log("  Min Speed: %4.1f, Max Speed: %4.1f", minSpeedLimit, maxSpeedLimit);
    if (illegalPassingAllowed)
    {
      SparrowHawk().log("  Illegal passing allowed");
    }
    segGoals.pop_front();
  }	
}
