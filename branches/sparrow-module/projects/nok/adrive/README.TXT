Basic overview:

*Adrive must be run on skynet1!

*Before running, edit the config file (adrive.config, in bin) such that you are only enabling what you need. The default setting should be appropriate for a full integration test. If you are having trouble with estop/obdii, comment out those lines.

*To run, type "./adrive" in bin

*As it stands now, you must manually set astop and command fields to 2 in the sparrow display. 
To do so, use the arrow keys to highlight the value then press " = 2" then enter. When the safety driver switches to run mode the darpa field should become 2. Position should be the min of command, darpa and astop. 
**If you've disabled estop in the config file, manually set position to 2.

*astop error code: astop error is displayed on the sparrow screen using hexadecimal notation.
**02:  Steering command is set enabled but steering is not actually enabled.
**04:  Some actuator command loop timed out with interlocks enabled.
**08:  ESTOP status is enabled but ESTOP is not receiving commands.
**10:  Ignition and transmission command are enabled and estop is not disable but the engine is off or the engine condition is not valid.
**20:  estop=RUN, steering status is set enabled, but steering is not actually enabled.
**40:  Steering status is enabled but steering is not receiving commands
**80:  Brake status is set enabled but brake is not receiving commands.
**100: Gas status is set enabled by gas is not receiving commands.
**200: Tranmission status is set enabled but transmission is not receiving commands.
**400: estop = DISABLE.

*enabling interlocks will cause ADrive to time out after x seconds if it doesn't receive a command from trajFollower. The supervisory thread continually checks whether the status counters are updating and will attempt to open/close the ports if it senses a failure (meanwhile, it executes an ADrive pause.)

*If estop is enabled, adrive will log automatically when put in run by the hardware (when darpa=2 in the estop sparrow display) and will stop the timber log when dropping out of run.

* Proper operation of actuators can be observed by seeing the counters in the top right corner incrementing.  If a specific actuator status is enabled but is not counting there is either a hardware problem or the actuator is not connected properly.  Check that the actuator is on and enabled.  Check that the port is correct.  

************************
Current hacks

* The vehicle velocity is multiplied by the transmission actuator positoin to give negative velocity when in reverse.  If you are not running with tranmission active but are running with OBDII in astate make sure that the transmission is shifted into the drive position.  Remember conditions for shifting below.  

*****************************************
Protections in adrive
* the steering will disable in PAUSE  below .1 m/s
* the steering will reenable when entering RUN

* transmission will not shift if:
	estop position != PAUSE
	obdii status != 1
	obdii Wheel Speed > .1

* steering will not execute if:
	obdii status == 1 && Wheel Sspeed < .1
	estop position == PAUSE && obdii status == 1 && Wheel Sspeed < 2

* only estop messages will be recieved when estop position == DISABLE

* OBDII status is updated by the supervisory thread after startup

* if not recieving commands over skynet astop will oscillate into and out of pause

* the supervisory thread will not recover a failed actuator.  In case of a failed actuator restart adrive

*****************************************
Note on working with serial ports
with the new install on skynet1 this should not be a problem, but just in case.

Make sure that you are on skynet1!

Serial is now working.  
If you are having problems with USB to serial here are some hints.

Make sure that you have permissions to the devices. THe line 
 chmod 777 /dev/ttyS* 
works well. 

I have linked ttyS10 and ttyS11 to ttyUSB0 and ttyUSB1 respectively.  

If the device reports not there try 
 mknod /dev/ttyUSB0 c 188 0  
or 
 mknod /dev/ttyUSB1 c 188 1  ***NOTE: the 1 vs 0 at the end.***

if that still doesn't work and you can't communicate with 
 cat </dev/ttySX & cat >/dev/ttySX            where X is the port you want.

try 
 minicom -s
set the port under the serial device config
set the speed to 9600

try talking to the port manually


