//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Error.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// print message then exit program.  tacks a newline on in console version
// (newline not expected so that popping up a window for the message 
// will look right)

void UD_error(char *message, ...)
{
  va_list args;
  
#ifdef UD_CONSOLE

  int message_length;
  char *new_message;

  message_length = strlen(message);
  new_message = (char *) calloc(message_length + 1, sizeof(char));
  strcpy(new_message, message);
  strcat(new_message, "\n");

  //  va_start(args, new_message);
  va_start(args, message);

  vprintf(new_message, args);

  va_end(args);

#endif

#ifdef UD_WINDOWED

  printf("no windowed UD_Error() at this time\n");
  exit(1);
 
#endif

  //  printf("%s\n", message);

  // force a seg fault so that we can do a backtrace in gdb
  //  UD_Matrix *M = NULL;
  //  printf("%f\n", M->rc[3][5]);

  //while (1);

  UD_exit(1);
  //  exit(1);
}

//----------------------------------------------------------------------------

void UD_exit(int exit_code)
{
  // force a seg fault so that we can do a backtrace in gdb
  //  UD_Matrix *M = NULL;
  //  printf("%f\n", M->rc[3][5]);

  exit(exit_code);
}

//----------------------------------------------------------------------------

// print to stdout and flush for immediate display

void UD_flush_printf(char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);

  vprintf(fmt, args);

  va_end(args);

  //  printf("%s\n", fmt);

  fflush(stdout);
}

//----------------------------------------------------------------------------

// print to stdout and echo to file fp if fp != NULL

void UD_log_printf(FILE *fp, char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);

  vprintf(fmt, args);

  if (fp) {
    vfprintf(fp, fmt, args);
    fflush(fp);
  }

  va_end(args);
}

//----------------------------------------------------------------------------

// same behavior as printf

void UD_printf(char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);

  vprintf(fmt, args);

  va_end(args);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
