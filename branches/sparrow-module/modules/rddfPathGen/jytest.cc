#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <math.h>
#include <iomanip>
#include <vector>
#include "pathLib.hh"



int main()
{
  /*
  vector<double> v1(5); //{1, 2, 3, 4, 5};
  
  //vector<int> v3;
  v1[0] = 1; v1[1] = 2; v1[2] = 3; v1[3] = 4; v1[4] = 5;
  //v2 = v1;
  vector<double> v2 = v1;
  double temp[] = {5, 6, 7, 8, 9};
  v2 = array2vector(5, temp);
  
  
  cout << "\nv1 is "; vecprint(v1);
  cout << "\nv2 is "; vecprint(v2);
  cout << "\nv1+1 is "; vecprint(v1 + 1);
  cout << "\nv1+1.0 is "; vecprint(v1 + 1.0);
  cout << "\nv1 is "; vecprint(v1);
  cout << "\nv1+v2 is "; vecprint(v1+v2);
  cout << "\nv1*10 is "; vecprint(v1*10);
  cout << "\n10*v1 is "; vecprint(10*v1);
  cout << "\nv1*v2 is "; vecprint(v1*v2);
  cout << "\nv1/v2 is "; vecprint(v1/v2);
  cout << "\nmin of 3 and 5 is " << min(3, 5);
  cout << "\ncos of pi/3 is " << cos(3.1415926535898/3);
  cout << "\nacos of .5 is " << acos(.5);
  cout << endl;

  corridorstruct cor, blank_cor;
  cout << "created cor" << endl;
  cor.numPoints = 3;
  cout << "set points" << endl;

  cor.e.push_back(0); cor.e.push_back(0); cor.e.push_back(100);
  cor.n.push_back(0); cor.n.push_back(100); cor.n.push_back(200);
  cor.width.push_back(10); cor.width.push_back(10); cor.width.push_back(10);
  for(int i = 1; i <= 3; i++) cor.speedLimit.push_back(10);

  cout << "\nDEBUG cor.e = "; vecprint(cor.e);
  cout << "\nDEBUG cor.n = "; vecprint(cor.n);
  cout << "\nDEBUG cor.width = "; vecprint(cor.width);
  cout << "\nDEBUG cor.speedLimit = "; vecprint(cor.speedLimit);

  //cor.e[0] = 0; cor.e[1] = 0; cor.e[2] = 100; 
  //  cor.n[0] = 0; cor.n[1] = 100; cor.n[2] = 200; 
  //cor.width[0] = 10; cor.width[1] = 10; cor.width[2] = 10;
  cout << "set other stuff" << endl;

  double theta, start_x, start_y, radius;
  radius = width_limited_curve(theta, start_x, start_y, cor);
  cout << "\n[radius, theta, start_x, start_y] = ["
       << radius << ", " << theta << ", " << start_x << ", " << start_y << "]";

  radius = length_limited_curve(theta, start_x, start_y, cor);
  cout << "\n[radius, theta, start_x, start_y] = ["
       << radius << ", " << theta << ", " << start_x << ", " << start_y << "]";
  cout << endl;




  pathstruct path = emptyPath();
  addToPath(path, cor.e[0], cor.n[0], 10, 10, 0, 0);
//   path.numPoints = 0;
//   path.e[0] = cor.e[0];
//   path.n[0] = cor.n[0];
//   path.ve[0] = path.vn[0] = path.ae[0] = path.an[0] = 0;
  cout << "\n First path is \n"; printPath(path);
  addToPath(path, 50, 50, 15, 7, 8, 9);
  cout << "Path becomes \n"; printPath(path);
  */

  corridorstruct cor;

  cor.numPoints = 4;
  cor.e.push_back(0); cor.e.push_back(0); cor.e.push_back(100); cor.e.push_back(200);
  cor.n.push_back(0); cor.n.push_back(100); cor.n.push_back(200); cor.n.push_back(200);
  cor.width.push_back(20); cor.width.push_back(20);
  cor.width.push_back(10); cor.width.push_back(10);
  for(int i = 1; i <= 4; i++) cor.speedLimit.push_back(5);

  pathstruct path;
  cout << "Initial Path is: " << endl;
  printPath(path);

  path = Path_From_Corridor(cor);

  cout << "Final Path is: " << endl;
  printPath(path);





  cout << "\n:)";

  /*
    char input [100];
    int i,n;
    long * l;
    cout << "How many numbers do you want to type in? ";
    cin.getline (input,100); i=atoi (input);
    l= new long[i];
    if (l == NULL) exit (1);
    for (n=0; n<i; n++)
    {
    cout << "Enter number: ";
    cin.getline (input,100); l[n]=atol (input);
    }
    cout << "You have entered: ";
    for (n=0; n<i; n++)
    cout << l[n] << ", ";
    delete[] l;
    
    
    
    vector<int> v( 5, 1 );
    for( int i = 0; i < 10; i++ ) {
    cout << "Element " << i << " is " << v[i] << endl;
  */



  return 0;
}
