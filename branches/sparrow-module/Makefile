# Makefile - master makefile for DGC software
# Build team, 2004-05
#
# This makefile is used to make the main software for Team Caltech.
# When it is working properly, you should be able to just type 'make'
# and get the basic software that you need.
#
# Currently available targets:
#   * all - standard software for running Alice
#   * gui - graphical user interface (requires graphics libraries)
#   * simulator - kinematic simulation
#
# Targets for evolving version of the code
#   * rddfPlanner - simple planner that takes RDDFs from tplanner
#   * tplanner - traffic planner
#   * mplanner - mission planner
#   * nokgui - Nok's version of the gui

DGC = .
include $(DGC)/global.mk

# This is the list of subdirectories to be cleaned
SUBDIRS = drivers modules util

# append .clean to directory names to clean
# this prevents "make modules" from cleaning modules
CLEAN_RULES = $(SUBDIRS:=.clean)

.PHONY: all clean uninstall wipe astate stereo ladar util tags
.PHONY: $(CLEAN_RULES) $(DGC_BIN_INFO)

all: nogui
new: all mplanner tplanner rddfPlanner nokgui

world: all simulator gui stereo road 

64: planner mapper $(RDDFPREPBIN) $(DGC_BIN_INFO)

32: $(DGC_BIN_INFO) \
    $(ASTATEBIN) \
    $(LADARFEEDERBIN) \
    $(STEREOFEEDERBIN) \
    $(TRAJFOLLOWERBIN) \
    $(ADRIVEBIN) \
    $(SUPERCONBIN) \
    $(DBSBIN) \
    $(RDDFPREPBIN) \
    $(DISTRDDFBIN) \
    $(GUIBIN) \
    $(ROADFINDINGBIN) \
    $(LOGPLAYERBIN) \
    $(SIMULATORBIN)

nogui: $(DGC_BIN_INFO) \
    $(TRAJFOLLOWERBIN) \
    $(FUSIONMAPPERBIN) \
    $(PLANNERMODULEBIN) \
    $(ADRIVEBIN) \
    $(LADARFEEDERBIN) \
    $(ASTATEBIN) \
    $(LOGPLAYERBIN) \
    $(VIDEORECORDERBIN) \
    $(RDDFPREPBIN) \
    $(DISTRDDFBIN)

# Rules for individual programs
gui: $(GUIBIN)
adrive: $(ADRIVEBIN) 
stereo: $(STEREOFEEDERBIN)
superCon: $(SUPERCONBIN)
ladar: $(LADARFEEDERBIN)
fusion: $(FUSIONMAPPERBIN)
mapper: fusion
planner: $(PLANNERMODULEBIN)
state: $(ASTATEBIN)
timber: $(TIMBERBIN)
road: $(ROADFINDINGBIN)
simulator: $(SIMULATORBIN)
rddfPlanner: $(RDDFPLANNERBIN)

# Targets for nonstandard programs (not yet integrated into build process)
tplanner: mplanner;	make -C projects/ndutoit/trafficPlanner
mplanner:;		make -C projects/nok/missionPlanner
nokgui:;		make -C projects/nok/gui install
libserial:;		make -C external libserial

sim: simulator

util:
	make -C util install

gazebo:
	make -C util/gazebo/ install

# 
# Special rules for installing packages that are part of YaM
#
# As we migrate modules to YaM, we are maintaining a single module
# location to avoid replication and errors.  The rules in this section
# handle this transition by installing YaM modules in $(DGC)/yam and
# compiling code from there.
#

$(DGC)/YaM/%:;	svn checkout $(DGCSVN)/Modules/$*/trunk $@
sparrow: $(DGC)/YaM/sparrow
	make -C $^ install

uninstall: $(CLEAN_RULES) wipe

clean: $(CLEAN_RULES)
	find . -name \*.o -delete
	@echo
	@echo '*** NOTE: By popular demand, "make clean" no longer removes the include, lib, and bin directories. Use "make uninstall" to do this. ***'
	@echo 'Also: YaM directories are *not* cleaned up'

wipe:
	(cat $(BINDIR)/todo_timber >> $(DGC)/todo_timber 2>/dev/null; true)
	rm -rf lib bin

info: $(DGC_BIN_INFO)

$(CLEAN_RULES):
	$(MAKE) -C $(@:.clean=) clean

$(DGC_BIN_INFO):
	echo -ne "hostname  : " > $@ ; hostname >> $@
	echo -ne "\ndate      : " >> $@ ; date >> $@
	echo -ne "\npwd       : " >> $@ ; pwd >> $@
	echo -ne "\nsvn info  :\n\n" >> $@ ; svn info >> $@

tags: etags

etags:
	find . -path '*/.svn' -prune -o -print | grep -E \\.h\$$\|\\.hh\$$\|\\.c\$$\|\\.cc\$$\|\\.cpp\$$ | grep -v "#" | etags --declarations --defines --globals -l c -l c++ --members -

ctags:
	find . -path '*/.svn' -prune -o -print | grep -E \\.h\$$\|\\.hh\$$\|\\.c\$$\|\\.cc\$$\|\\.cpp\$$ | grep -v "#" | ctags --declarations --defines --globals -l c -l c++ --members -
