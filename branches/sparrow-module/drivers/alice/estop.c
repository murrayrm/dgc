// estop drivers for DGC 2005 Alice
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>

#include "estop.h"
#include "SDSPort.h"
#include "vehports.h"
#include "GlobalConstants.h"

#include <sstream>
#include <SerialStream.h>
using namespace std;
using namespace LibSerial;
static SerialStream serial_port;


int estopPortFlag = FALSE;
#ifdef ESTOP_SDS
SDSPort* estopPort;
#endif //ESTOP_SDS
extern char simulator_IP[];
char estop_read[ESTOP_BIG_INPUT_LENGTH];



// For ESTOP_SERIAL
char estop_position_char;


int estop_open(int port)
{
#ifdef ESTOP_SDS
  if (estopPortFlag == FALSE)
    {
      estopPort = new SDSPort("192.168.0.60", port);
      estopPortFlag = TRUE;
    }
  else estopPort->openPort();
  
  if (estopPort->isOpen() == 1)
    {
      return TRUE;
    }
  else return FALSE;
#endif //ESTOP_SDS
#ifdef ESTOP_SERIAL

	ostringstream portname;
	portname << "/dev/ttyS" << port;

	serial_port.Open(portname.str());
	if ( ! serial_port.good() )
	{
		cerr << "estop_open() error: Could not open " << portname.str() << endl ;
		return FALSE;
	}
	serial_port.SetBaudRate( SerialStreamBuf::BAUD_9600 ) ;
	if ( ! serial_port.good() )
	{
		cerr << "estop_open() error: Could not set the baud rate." << endl ;
		return FALSE;
	}
	serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
	if ( ! serial_port.good() )
	{
		cerr << "estop_open error: Could not disable the parity." << endl ;
		return FALSE;
	}
	serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
	if ( ! serial_port.good() )
	{
		cerr << "estop_open error: Could not turn on software flow control." << endl;
		return FALSE;
	}
	serial_port.SetNumOfStopBits( 1 ) ;
	if ( ! serial_port.good() )
	{
		cerr << "estop_open error: Could not set the number of stop bits."  << endl;
		return FALSE;
	}
	serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8) ;
	if ( ! serial_port.good() )
	{
		cerr << "estop_open() error: Could not set the char size." << endl ;
		return FALSE;
	}

#endif //ESTOP_SERIAL

  return true;
}



void simulator_estop_open(int port) {
#ifdef ESTOP_SDS
  estopPort = new SDSPort(simulator_IP, port);
  estopPortFlag = TRUE;
#endif //ESTOP_SDS
#ifdef ESTOP_SERIAL
  printf( "You shouldn't try to open the simulator over ESTOP_SERIAL\n");
#endif //ESTOP_SERIAL
}
  


int estop_close() {
#ifdef ESTOP_SDS
  if (estopPort->closePort() == 0) return TRUE;
  else return FALSE;
#endif //ESTOP_SDS
#ifdef ESTOP_SERIAL
  serial_port.Close(); 
#endif //ESTOP_SERIAL
  return TRUE;
}


int estop_status(void) {
#ifdef ESTOP_SDS
  int read_length;
  read_length = estopPort->read(ESTOP_BIG_INPUT_LENGTH, estop_read, 10000);
  //  printf("estop read is %c\n", estop_read[read_length - 1]);
  if (estop_read[read_length - 1] == RUN_CHAR) return RUN;
  else if (estop_read[read_length - 1] == DISABLE_CHAR) return DISABLE;
  else return EPAUSE;
#endif //ESTOP_SDS

#ifdef ESTOP_SERIAL
	static bool gotError = false;
	/*if(gotError)
	{
		usleep(200000);
		gotError = false;
		cerr << "clearing error" <<endl;
		}*/
	serial_port.read(&estop_position_char, 1);
	
	if (!serial_port.good())
	  {
	    cerr << "Error reading estop serial port!" << endl;
	    serial_port.clear();
	    gotError = true;
	    return -1;//ERROR_HAPPENED
	  }

  // cout << "estop position: '" << estop_position_char << "'" << endl;
  if (estop_position_char == RUN_CHAR) return RUN;
  if (estop_position_char == DISABLE_CHAR) return DISABLE;

#endif //ESTOP_SERIAL
  //This is just for redundancy
  return EPAUSE;
}
