#ifndef __MAPCONSTANTS_H__
#define __MAPCONSTANTS_H__

#define MAX_SPEED 22.35
#define EPSILON_SPEED 0.001

//Basic Map Config File
#define CONFIG_FILE_DEFAULT_MAP "config/cMap/Map.default.dat"
#define CONFIG_FILE_SPEED_MAP   "config/cMap/Map.speed.dat"

//Cost Config Files
#define CONFIG_FILE_COST             "config/cMap/Layer.cost.dat"
#define CONFIG_FILE_RDDF             "config/cMap/Layer.rddf.dat"
#define CONFIG_FILE_STATIC           "config/cMap/Layer.static.dat"
#define CONFIG_FILE_ROAD             "config/cMap/Layer.road.dat"
#define CONFIG_FILE_SUPERCON         "config/cMap/Layer.superCon.dat"
#define CONFIG_FILE_CHANGES          "config/cMap/Layer.changes.dat"

//LADAR Layer Config Files
#define CONFIG_FILE_LADAR_FUSED_ELEV "config/cMap/Layer.ladarFusedElev.dat"
#define CONFIG_FILE_LADAR_MEAN_ELEV  "config/cMap/Layer.ladarMeanElev.dat"
#define CONFIG_FILE_LADAR_STDDEV     "config/cMap/Layer.ladarStdDev.dat"
#define CONFIG_FILE_LADAR_NUM        "config/cMap/Layer.ladarNum.dat"

//Stereo Layer Config Files
#define CONFIG_FILE_STEREO_FUSED_ELEV "config/cMap/Layer.stereoFusedElev.dat"
#define CONFIG_FILE_STEREO_MEAN_ELEV "config/cMap/Layer.stereoMeanElev.dat"

#endif //__MAPCONSTANTS_H__
