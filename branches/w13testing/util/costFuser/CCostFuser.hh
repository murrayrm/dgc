#ifndef __CCOSTFUSER_HH__
#define __CCOSTFUSER_HH__

#include <stdlib.h>
#include <math.h>

#include "CMap.hh"
#include "MapConstants.h"


#define MAX_INPUT_COST_LAYERS 6


struct CCostFuserOptions {
	double maxSpeed;

	int maxCellsToInterpolate;
	int minNumSupportingCellsForInterpolation;
};


class CCostFuser {
 public:

	CCostFuser(CMap* outputMap, int layerNumFinalCost,
						 CMap* rddfMap, int layerNumRDDF,
						 CCostFuserOptions* options);
	~CCostFuser();

		enum STATUS {
		OK,
		ERROR
	};

	int addLayerElevCost(CMap* inputMap, int layerNumElevCost, double relWeight);
	STATUS addLayerRoad(CMap* inputMap, int layerNumRoad);
	STATUS addLayerSuperCon(CMap* inputMap, int layerNumSuperCon);

	STATUS markChangesCost(NEcoord point);
	STATUS markChangesTerrain(int layerIndex, NEcoord point);

	STATUS fuseChangesCost();
	STATUS fuseChangesTerrain();
	STATUS interpolateChanges();

	STATUS fuseCellTerrain(NEcoord point);
	STATUS fuseCellCost(NEcoord point);
	STATUS interpolateCell(NEcoord point);

	CCostFuserOptions* _options;

	int _numElevLayers;
	CMap* _mapPtrsElevCost[MAX_INPUT_COST_LAYERS];
	int _layerNumsElevCost[MAX_INPUT_COST_LAYERS];
	double _relWeights[MAX_INPUT_COST_LAYERS];

	int _layerNumRDDF;
	CMap* _mapRDDF;

	int _layerNumRoad;
	CMap* _mapRoad;

	int _layerNumSuperCon;
	CMap* _mapSuperCon;

	CMap* _mapOutput;
	int _layerNumCombinedElevCost;
	int _layerNumOutputCost;
	int _layerNumElevChanges;
	int _layerNumCostChanges;
	int _layerNumInterpolationChanges;
};

#endif
