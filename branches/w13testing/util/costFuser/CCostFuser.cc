#include "CCostFuser.hh"

CCostFuser::CCostFuser(CMap* mapOutput, int layerNumFinalCost,
											 CMap* mapRDDF, int layerNumRDDF,
											 CCostFuserOptions* options) {
	_options = options;

	_mapOutput = mapOutput;
	_layerNumOutputCost = layerNumFinalCost;

	_mapRDDF = mapRDDF;
	_layerNumRDDF = layerNumRDDF;

	_layerNumCombinedElevCost = _mapOutput->addLayer<double>(CONFIG_FILE_COST, false);
	_layerNumElevChanges = _mapOutput->addLayer<int>(CONFIG_FILE_CHANGES, true);
	_layerNumCostChanges = _mapOutput->addLayer<int>(CONFIG_FILE_CHANGES, true);
	_layerNumInterpolationChanges = _mapOutput->addLayer<int>(CONFIG_FILE_CHANGES, true);
 
	_numElevLayers = 0;
	_layerNumRoad = -1;
	_layerNumSuperCon = -1;
}


CCostFuser::~CCostFuser() {

}


int CCostFuser::addLayerElevCost(CMap* inputMap, int layerNumElevCost, double relWeight) {
	if(_numElevLayers==MAX_INPUT_COST_LAYERS) {
		return ERROR;
	}

	_mapPtrsElevCost[_numElevLayers] = inputMap;
	_layerNumsElevCost[_numElevLayers] = layerNumElevCost;
	_relWeights[_numElevLayers] = relWeight;

	_numElevLayers++;

	return _numElevLayers-1;
}


CCostFuser::STATUS CCostFuser::addLayerRoad(CMap* inputMap, int layerNumRoad) {
	_mapRoad = inputMap;
	_layerNumRoad = layerNumRoad;

	return OK;
}


CCostFuser::STATUS CCostFuser::addLayerSuperCon(CMap* inputMap, int layerNumSuperCon) {
	_mapSuperCon = inputMap;
	_layerNumSuperCon = layerNumSuperCon;

	return OK;
}


CCostFuser::STATUS CCostFuser::markChangesCost(NEcoord point) {
	_mapOutput->setDataUTM_Delta<int>(_layerNumCostChanges, point.N, point.E, 1);			
	
	return OK;
}


CCostFuser::STATUS CCostFuser::markChangesTerrain(int layerIndex, NEcoord point) {
	_mapOutput->setDataUTM_Delta<int>(_layerNumElevChanges, point.N, point.E, 1);			
	
	int currentRow, currentCol, maxRow, minRow, maxCol, minCol;
	
	_mapOutput->UTM2Win(point.N, point.E, &currentRow, &currentCol);
	
	//Now we do some interpolation
  _mapOutput->constrainRows(currentRow, _options->maxCellsToInterpolate, minRow, maxRow);
  _mapOutput->constrainCols(currentCol, _options->maxCellsToInterpolate, minCol, maxCol);

	if(_mapPtrsElevCost[layerIndex]->getResRows() > _mapOutput->getResRows() ||
		 _mapPtrsElevCost[layerIndex]->getResCols() > _mapOutput->getResCols()) {
		
	}

  for(int r=minRow; r<=maxRow; r++) {
    for(int c=minCol; c<=maxCol; c++) {
			_mapOutput->setDataWin_Delta<int>(_layerNumInterpolationChanges, r, c, 1);
    }
  }

	return markChangesCost(point);
}


CCostFuser::STATUS CCostFuser::fuseChangesCost() {
	fuseChangesTerrain();

	NEcoord point;
	int numCells = _mapOutput->getCurrentDeltaSize(_layerNumCostChanges);

	for(int i=0; i < numCells; i++) {
		_mapOutput->getCurrentDeltaVal<int>(i, _layerNumCostChanges, &point.N, &point.E);
		fuseCellCost(point);
	}

	_mapOutput->resetDelta<int>(_layerNumCostChanges);

	return OK;
}


CCostFuser::STATUS CCostFuser::fuseChangesTerrain() {
	NEcoord point;
	int numCells = _mapOutput->getCurrentDeltaSize(_layerNumElevChanges);

	for(int i=0; i < numCells; i++) {
		_mapOutput->getCurrentDeltaVal<int>(i, _layerNumElevChanges, &point.N, &point.E);
		fuseCellTerrain(point);
	}

	_mapOutput->resetDelta<int>(_layerNumElevChanges);

	return OK;
}


CCostFuser::STATUS CCostFuser::interpolateChanges() {
	NEcoord point;
	int numCells = _mapOutput->getCurrentDeltaSize(_layerNumInterpolationChanges);

	for(int i=0; i < numCells; i++) {
		_mapOutput->getCurrentDeltaVal<int>(i, _layerNumInterpolationChanges, &point.N, &point.E);
		interpolateCell(point);
	}

	_mapOutput->resetDelta<int>(_layerNumInterpolationChanges);

	return OK;
}


CCostFuser::STATUS CCostFuser::fuseCellTerrain(NEcoord point) {
	double totalCost = 0.0;
	double layerCost;
	double numLayersUsed = 0.0;

	for(int i=0; i < _numElevLayers; i++) {
		layerCost = _mapPtrsElevCost[i]->getDataUTM<double>(_layerNumsElevCost[i], point.N, point.E);
		if(layerCost != _mapPtrsElevCost[i]->getLayerNoDataVal<double>(_layerNumsElevCost[i])) {
			totalCost += layerCost*_relWeights[i];
			numLayersUsed+=_relWeights[i];
		}
	}
	_mapOutput->setDataUTM<double>(_layerNumCombinedElevCost, point.N, point.E, totalCost/(numLayersUsed));

	return OK;
}


CCostFuser::STATUS CCostFuser::fuseCellCost(NEcoord point) {
	double finalSpeed = 0.0;
	double speedNoDataVal = _mapRDDF->getLayerNoDataVal<double>(_layerNumRDDF);
	double rddfSpeed = _mapRDDF->getDataUTM<double>(_layerNumRDDF, point.N, point.E);

	if(rddfSpeed != speedNoDataVal) {
		finalSpeed = _mapOutput->getDataUTM<double>(_layerNumCombinedElevCost, point.N, point.E);

		finalSpeed = fmin(finalSpeed, rddfSpeed);
		//And of course, we don't want a speed higher than our max speed
		finalSpeed = fmin(finalSpeed, _options->maxSpeed);
		//And we want the larger of speeds from above and the min speed of the map
		finalSpeed = fmax(finalSpeed, speedNoDataVal + EPSILON_SPEED);


		_mapOutput->setDataUTM_Delta<double>(_layerNumOutputCost, point.N, point.E,
																				 finalSpeed);
	}

	return OK;
}


CCostFuser::STATUS CCostFuser::interpolateCell(NEcoord point) {
	//int currentRow, currentCol, maxRow, minRow, maxCol, minCol;
	int r, c;

	_mapOutput->UTM2Win(point.N, point.E, &r, &c);

	double currentCellCost = _mapOutput->getDataWin<double>(_layerNumCombinedElevCost, r, c);
	double speedNoDataVal = _mapOutput->getLayerNoDataVal<double>(_layerNumCombinedElevCost);
	double rddfSpeed = _mapRDDF->getDataUTM<double>(_layerNumRDDF, point.N, point.E);

	if(currentCellCost == speedNoDataVal &&
		 rddfSpeed != speedNoDataVal) {
		
		int minLocalRow, maxLocalRow, minLocalCol, maxLocalCol;
		double localCellCost;
		//CElevationFuser localCellFused;
		//double localNorthing, localEasting;

		_mapOutput->constrainRows(r, 1, minLocalRow, maxLocalRow);
		_mapOutput->constrainCols(c, 1, minLocalCol, maxLocalCol);
		double localCost=0.0;
		int numSupporters=0;
		//_mapOutput->Win2UTM(r, c, &localNorthing, &localEasting);
		for(int localR=minLocalRow; localR<=maxLocalRow; localR++) {
			for(int localC=minLocalCol; localC<=maxLocalCol; localC++) {
				localCellCost = _mapOutput->getDataWin<double>(_layerNumCombinedElevCost, localR, localC);
				//localCellFused = _inputMap->getDataWin<CElevationFuser>(_fusedLayerNum, localR, localC);
				if(localCellCost != speedNoDataVal) {
					localCost+=localCellCost;
					numSupporters++;
				}
			}
		}

		if(numSupporters >= _options->minNumSupportingCellsForInterpolation) {
			double finalSpeed = localCost/((double)numSupporters);
			finalSpeed = fmin(finalSpeed, rddfSpeed);
			//And of course, we don't want a speed higher than our max speed
			finalSpeed = fmin(finalSpeed, _options->maxSpeed);
			//And we want the larger of speeds from above and the min speed of the map
			finalSpeed = fmax(finalSpeed, speedNoDataVal + EPSILON_SPEED);
			_mapOutput->setDataWin_Delta<double>(_layerNumOutputCost, r, c, finalSpeed);
		}
	}
  

	return OK;
}
