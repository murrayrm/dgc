% darpa rddf path generation demo
%
% just run this file in matlab to see a demo of:
%     plot_corridor_file.m
%     path_from_corridor.m
%     plot_path.m
%


file = 'corridor_simple.bob';
%file = '../routes/qid_original.bob';
% file = 'DARPA_RACE_RDDF.bob';
plot_corridor_file(file);
cor = file2corridor(file);
path = path_from_corridor(cor);
plot_path(path);

%{
% this doesn't work for some reason...
% racefile = '~/dgc/RDDF/routes/DARPA_RACE_RDDF.bob'
% so if you want to plot out the corridor and path for last year's race,
% you should copy the DARPA_RACE_RDDF.bob file from the ../routes/
% directory into this current directory /RDDF/matlab
racefile = 'DARPA_RACE_RDDF.bob';


% scratch area

% execute the lines below to plot the corridor and path generated by
% RddfPathGen
corridor_file = 'bob.dat';
traj_file = 'rddf.traj';
plot_corridor_file(corridor_file);
traj = importdata(traj_file);
plot_path_dense(traj);



% converts from traj to path
% path(:,1) = traj(:,4); path(:,2) = traj(:,1); path(:,3) = traj(:,5); path(:,4) = traj(:,2); path(:,5) = traj(:,6); path(:,6) = traj(:,3);
% converts from path to traj
% path(:,1) = traj(:,2); path(:,2) = traj(:,4); path(:,3) = traj(:,6); path(:,4) = traj(:,1); path(:,5) = traj(:,3); path(:,6) = traj(:,5);

% OLD STUFF....browse if you're interested
% 
% %{
% % plot a basic corridor
% plot_corridor('corridor.txt');
% 
% % import a basic path
% path = importdata('path - basic.txt');
% % and plot it
% plot_path(path);
% %}
% 
% 
% % basic example...debugging
% file = 'corridor_basic.txt';
% plot_corridor2(file);
% cor = file2corridor(file);
% path = path_from_corridor(cor);
% plot_path(path);
% 
% 
% %{
% % basic example...debugging
% file = 'corridor_ben.txt';
% plot_corridor2(file);
% cor = file2corridor(file);
% path = path_from_corridor(cor);
% plot_path(path);
% 
% 
% 
% 
% % race from last year
% file = 'corridor_race0_cut.txt';
% plot_corridor2(file);
% cor = file2corridor(file);
% path = path_from_corridor(cor);
% plot_path(path);
% 
% %}
% 
% 

%}