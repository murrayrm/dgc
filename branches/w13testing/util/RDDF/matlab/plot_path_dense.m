function plot_path_dense(varargin)
% 
% function plot_path_dense(path_matrix)
% function plot_path_dense(path_matrix, offset)
%
% Changes: 
%   1-11-2005, Jason Yosinski, created
%
% Function:
%   This function plots a dense path (it does no interpolation).  If
%   interpolation is what you are looking for, use plot_path.m.  It also
%   plots lines representing the velocity and acceleration at a point.
%   These can be turned on or off by modifying the 'parameters' section
%   in this *.m file.
%
% Input:
%   path_matrix = [x1, y1, vx1, vy1, ax1, ay1;
%                  x2, y2, vx2, vy2, ax2, ay2;
%                  ..  ..  ...  ...  ...  ...;
%                  xN, yN, vxN, vyN, axN, ayN]
%       The last point in a path matrix is the endpoint of the path and
%       will be simply plotted as a point.  vxN, vyN, axN, and ayN are
%       irrelevant.
%   offset = [x_offset, y_offset]
%       These offsets will be subtracted from every point in the path, 
%       effectively translating the entire path in space.  If offset is not
%       used, the path will be translated so that the path starts at the
%       point (0,0)
%
% Output:
%   none
%
% Usage Example:
%   plot_path_dense(path);
%   plot_path_dense(path, [10, 20]);
%


% parameters to be set by user
resolution = 1;        % plot every <resolution> points.  If resolution is
                       % 1, then every point will be plotted.  If
                       % resolution is 2, every other point...
point_style = 'ko';    % style for points (see help plot)
vel_style = 'b-';      % style used for paths (see help plot)
vel_length_scale = 5;  % scale all the vel lines by this amount
accel_style = 'r-';    % style used for accelerations
accel_length_scale=9; % scale all the accel lines by this amount
line_width = .5;       % width of path lines (in points), default = .5

plot_vels_perp_to_path = 1;  % plots the velocities pointing to the left of the
                             % path instead of straight ahead

hold on;

% set varargin defaults
if(nargin < 1)
    error('Need an argument (path_matrix)');
end

path = varargin{1};
N = size(path,1);

% the format defined by traj is [x xd xdd y yd ydd]
TRAJ_INPUT_FORMAT = 1;
if( TRAJ_INPUT_FORMAT )
  % copy the input path (format defined in traj.h)      
  path2 = path; % path2 = [x xd xdd y yd ydd]
  % reassign path to the format originally expected here
  %                  xN, yN, vxN, vyN, axN, ayN]
  path(:,1) = path2(:,4);
  path(:,2) = path2(:,1);
  path(:,3) = path2(:,5);
  path(:,4) = path2(:,2);
  path(:,5) = path2(:,6);
  path(:,6) = path2(:,3);
end

if(nargin == 1)
    % if they don't specify an offset, then we'll assume they want the path
    % plotted starting at (0,0)
    path(:,1) = path(:,1) - ones(N,1) .* path(1,1);   % offset the x's
    path(:,2) = path(:,2) - ones(N,1) .* path(1,2);   % offset the y's
elseif(nargin > 1)
    offset = varargin{2};
    path(:,1) = path(:,1) - ones(N,1) .* offset(1);   % offset the x's
    path(:,2) = path(:,2) - ones(N,1) .* offset(2);   % offset the y's
end


% plot all the points and their vel and accel lines
for i = 1:resolution:N
    point = path(i,1:2);
    vel = path(i,3:4) * vel_length_scale;    % scale the velocity (just to
                                             % make the lines reasonable
    if (plot_vels_perp_to_path)
        vel = ([0 -1; 1 0] * vel')';  % rotate vels by PI/2 CCW
    end

    accel = path(i,5:6) * accel_length_scale;  % scale the accel
    
    % construct a set of two points to draw the line for vel and accel
    vel_double_point = [point; point + vel];
    accel_double_point = [point; point + accel];
    
    % plot the point
    plot(point(1), point(2), point_style);
    % plot the vel line
    plot(vel_double_point(:,1), vel_double_point(:,2), vel_style);
    % plot the accel line
    plot(accel_double_point(:,1), accel_double_point(:,2), accel_style);
end


% make it look nice
axis tight
axis equal
hold on
%grid on
