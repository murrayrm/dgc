#include "pid.hh"
#include <fstream>
#include <iostream>
#include <unistd.h>
using namespace std;

int main(void)
{
  //cout << "Reading in a sample trajectory file." << endl;
	//ifstream infile("sample.traj");
  //Cpid traj(3,infile);

  // Arguments are Kp, Ki, Kd, sat
  printf("Control gains are 1.0, 0.1, 1.0\n" );
  printf("Integral saturation is at 10.0\n" );
  Cpid mypid( 1.0, 0.1, 1.0, 10.0 );
                 
  double control;
  
  printf("Sending constant 0.1 error signal.\n" );
  for( int i=0; i<10; i++ )
  {
    control = mypid.step_forward( 0.1 );
    printf("control = %f\n", control );
    // Sleep for 100 ms.
    usleep(100000);
  }

  //cout << "Printing the traj." << endl;
  //traj.print();

  //cout << "Saving the traj to output.traj." << endl;
  //ofstream outfile("output.traj");
  //traj.print(outfile);

  cout << "Exiting." << endl;
  return 0;
}
