#ifndef PAINTERFUNCTORS_HH
#define PAINTERFUNCTORS_HH

//#include"CCostPainter.hh"
#include "CCostFuser.hh"
#include"CMap.hh"

#include"Distances.hh"

#include<iostream>
#include<iomanip>
using namespace std;

//Defines the different ways we can paint road into map

//The one we normaly use is the first one, and in the definition
//of the operator function the transfer function from distance
//to speed is done

/*
**
** Paints colored on distance/width but thresholds value and only updates 
**   changes > one threshold level
**
*/
class PainterFunctorThresRelDistance
{
 public:
  PainterFunctorThresRelDistance(CMap *pMap, int layer, CCostFuser *pFuser, double speed)
    : m_pmap(pMap), m_layer(layer), m_pfuser(pFuser), dist(0,0,0,0,0,0), m_speed(speed)
  {
  }

  void set_center_line(double N1, double E1, double w1, double N2, double E2, double w2) {
    dist.set(N1,E1,w1, N2, E2,w2);
  }
  
  void set_speed(double s) { m_speed=s; }

  //Let the painted speed depend on the distance as:
  //                    f1
  //          ______   /
  //    /----/      \----\   <-s0
  //    |                |
  //   /                  \    <--- f2
  //   |                  |
  //                     ^
  //                    d0

  //Where f1 is first degree and f2 is first degree  (in d^2 so effectivly a 2nd degree)
  //Let the cutoffs be d0 and s0
  //
  // f1(d) = (s0-1)/d0 * d2 + 1
  //
  // f2(d) = s0/(d0-1) * d2 - s0/(d0-1)
  //
  //
  // The result is further thresholded
  //

#define CALC_LEVEL(x)   ((x)<=d0 ? ((s0-1)/d0 *(x) + 1) : (s0/(d0-1) *(x) - s0/(d0-1)))
  void operator()(double N, double E) {
    const double s0 = .8;
    const double d0 = .6;

    //The levels corresponding to d0*5  i.e. level[(int) d*5 +1]
    static const double level[] = {
      1.0,
      CALC_LEVEL(0.0),
      CALC_LEVEL(0.2),
      CALC_LEVEL(0.4),
      CALC_LEVEL(0.6),
      CALC_LEVEL(0.8),
      CALC_LEVEL(1.0),
      0.0
    };

    //paint
    int d2 = (int) (5*dist(N,E));

    if(d2<=0)
      d2=0;
    if(d2>5)
      d2=5;

    //Increase one step to get "buffer" items on edges (avoids extra boundary checking)
    ++d2;

    double cur = m_pmap->getDataUTM<double>(m_layer, N, E) / m_speed;

    if(cur>level[d2-1] || cur<level[d2+1]) {
      m_pmap->setDataUTM(m_layer, N, E, level[d2] * m_speed);
      
      if(m_pfuser) {
	m_pfuser->markChangesCost(NEcoord(N,E));
      }
    }
  }

 protected:
  CMap *m_pmap;
  int m_layer;
  CCostFuser *m_pfuser;
  RoadDistanceRel2 dist;
  double m_speed;
};

/*
**
** Paints colored on distance/width
**
*/
class PainterFunctorRelDistance
{
 public:
  PainterFunctorRelDistance(CMap *pMap, int layer, CCostFuser *pFuser, double speed)
    : m_pmap(pMap), m_layer(layer), m_pfuser(pFuser), dist(0,0,0,0,0,0), m_speed(speed)
  {
  }

  void set_center_line(double N1, double E1, double w1, double N2, double E2, double w2) {
    dist.set(N1,E1,w1, N2, E2,w2);
  }
  
  void set_speed(double s) { m_speed=s; }

  //Let the painted speed depend on the distance as:
  //                    f1
  //          ______   /
  //    /----/      \----\   <-s0
  //    |                |
  //   /                  \    <--- f2
  //   |                  |
  //                     ^
  //                    d0

  //Where f1 is first degree and f2 is first degree  (in d^2 so effectivly a 2nd degree)
  //Let the cutoffs be d0 and s0
  //
  // f1(d) = (s0-1)/d0 * d2 + 1
  //
  // f2(d) = s0/(d0-1) * d2 - s0/(d0-1)

  void operator()(double N, double E) {
    //paint
    double d2 = dist(N,E);

    const double s0 = .8;
    const double d0 = .6;

    if(d2<d0) {
      d2 = (s0-1)/d0 *d2 + 1;
    } else {
      d2 = s0/(d0-1) *d2 - s0/(d0-1);
    }

    m_pmap->setDataUTM(m_layer, N, E, d2 * m_speed);

    if(m_pfuser) {
      m_pfuser->markChangesCost(NEcoord(N,E));
    }
  }

 protected:
  CMap *m_pmap;
  int m_layer;
  CCostFuser *m_pfuser;
  RoadDistanceRel2 dist;
  double m_speed;
};


/*
**
** Paints distance scaled color
**
*/
class PainterFunctorDistance
{
 public:
  PainterFunctorDistance(CMap *pMap, int layer, CCostFuser *pFuser, double speed)
    : m_pmap(pMap), m_layer(layer), m_pfuser(pFuser), m_speed(speed), dist(0,0,0,0)
  {
  }

  void set_speed(double s) { m_speed=s; }

  void set_center_line(double N1, double E1, double w1, double N2, double E2, double w2) {
    dist.set(N1,E1, N2, E2);
  }

  void operator()(double N, double E) {
    //paint

    m_pmap->setDataUTM(m_layer, N, E, dist(N,E));
    //m_pmap->setDataUTM(m_layer, N, E, (N-m_N1)*(N-m_N1) + (E-m_E1)*(E-m_E1) );

    if(m_pfuser) {
      m_pfuser->markChangesCost(NEcoord(N,E));
    }
  }

 protected:
  CMap *m_pmap;
  int m_layer;
  CCostFuser *m_pfuser;
  double m_speed;
  RoadDistance2 dist;

  double m_N1, m_E1;
};

/*
**
** Paints a uniform color
**
*/
class PainterFunctorUniform
{
 public:
  PainterFunctorUniform(CMap *pMap, int layer, CCostFuser *pFuser, double val)
    : m_pmap(pMap), m_layer(layer), m_pfuser(pFuser), m_val(val)
  {
  }

  void set_speed(double s) { m_val=s; }

  void set_center_line(double N1, double E1, double w1, double N2, double E2, double w2) {
  }

  void operator()(double N, double E) {
    m_pmap->setDataUTM(m_layer, N, E, m_val);

    //cout<<"Painting " <<setprecision(10)<<N<<" " <<E<<endl;
    if(m_pfuser) {
      m_pfuser->markChangesCost(NEcoord(N,E));
    }
  }

 protected:
  CMap *m_pmap;
  int m_layer;
  CCostFuser *m_pfuser;
  double m_val;
};

#endif
