#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

#include "sharemem.hh"
#include "test_class.hh"

CShmWriter writer;

void sigint(int bla) {
//	writer.DeInit();
	exit(1);
}

int main(int argc, char *argv[]) {
	int data = 0;
	CShmTest test; 

	signal(SIGINT, sigint);
	

	if (!writer.Init("/tmp/shmdemo.pid", sizeof(int))) return 1;

	if ((argc == 2)&&( argv[1][0] == 'd')) {
		return 0; //destroying left mem 
	}
	
	void *d;
	while (1) { 
		writer.WaitForRequest();
		
		/*
		d = writer.MemConnect();
		*(int*)d = data;
		writer.MemDisconnect();
		*/
		
		writer.Mem2ShCpy(&test,sizeof(CShmTest));
		writer.RequestComplete();
		data++;
		test.Inc();
		printf("Written %d\n", data);
	}

	return 0;
}
