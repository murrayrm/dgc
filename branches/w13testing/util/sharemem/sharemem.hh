#ifndef SHAREMEM_H
#define SHAREMEM_H

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>

#define SHM_ERROR 0 
#define SHM_SUCCESS 1

int ShmSetSemVal(int semid, int semnum, int val);
int ShmLockSem(int semid, int semnum = 0);
int ShmUnLockSem(int semid, int semnum = 0);

int ShmGetKey(char *fname, int cr, key_t &key);
int ShmConnectMem(key_t key, int &semid, int &shmid, int size);


#define MAX_CHARS 256

#define SHM_NUM_SEM 2

#define SHM_WRITE_SEM 0
#define SHM_READ_SEM 1

#define SHM_DEBUG 0
class CShmReader {
	private: 
		int semid;
		int shmid;
		key_t key;
	public:
		CShmReader();
		int Init(char *key_file, int size); 
		int DeInit();
		
		int Request(); 
		
		int Sh2MemCpy(void *buffer, int size);
		
		~CShmReader();
};


class CShmWriter { 
	private: 
		int semid;
		int shmid;
		key_t key; 
		char key_fname[MAX_CHARS];
		void *shm_ptr;
	public:
		CShmWriter();
		~CShmWriter();
	
		int Init(char *key_file, int size);
		int DeInit();
		
		int WaitForRequest(); 
		int RequestComplete();

		int Mem2ShCpy(void *buffer, int size);


		void *MemConnect();
		int MemDisconnect();
};

#endif
