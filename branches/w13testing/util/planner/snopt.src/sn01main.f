************************************************************************
*                                                                      *
*     File  sn01main.f                                                 *
*                                                                      *
*     Generic main program for stand-alone SQOPT.                      *
*                                                                      *
************************************************************************
*                                                                      *
*                               S Q O P T                              *
*                                                                      *
*    Sparse Quadratic Optimization                                     * 
*                                                                      *
*                      Version 5.3-3                  Aug 31, 1998     *
*                                                                      *
*    Philip E. Gill    Walter  Murray          Michael A. Saunders     *
*    UC San Diego      Stanford University     Stanford University     *
*                                                                      *
*----------------------------------------------------------------------*
*     (C) 1992--1998  Regents of the University of California          *
*                     and the Trustees of Stanford University          *
*                                                                      *
*     This software is NOT in the public domain. Its use is governed   *
*     by a license agreement with either Stanford University or the    *
*     University of California.  It is a breach of copyright to make   *
*     copies except as authorized by the license agreement.            *
*                                                                      *
*     This material is based upon work partially supported by the      *
*     National Science Foundation under Grants DMI-9204208 and         *
*     DMI-9204547; and the Office of Naval Research Grant              *
*     N00014-90-J-1242.                                                *
************************************************************************
*
*  SQOPT Fortran source files:
*
*  1. sn01main   Main program
*  2. sn09time   Machine-dependent timing routine
*  3. sn10unix   Machine-dependent routines
*  4. sn12sqzz   Top-level SQOPT routines and auxiliaries
*  5. sn15blas   Level-1 Basic Linear Algebra Subprograms (a subset)
*  6. sn17util   linear algebra subprograms
*  7. sn20amat   Core allocation and manipulation of the ( A -I )
*  8. sn25bfac   Basis factorization routines
*  9. sn27LU     LU factorization routines
* 10. sn30spec   SPECS file routines
* 11. sn35mps    MPS file routines
* 12. sn40bfil   Basis file and solution output routines
* 13. sn50lp     Routines for the primal simplex method
* 14. sn55qp     Routines for quadratic programming
* 15. sn57qopt   QP and Memory allocation routines called by SQOPT 
* 16. sn65rmod   For maintaining R, the approximate reduced Hessian
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      program            sqmps
      implicit           double precision (a-h,o-z)

*     ------------------------------------------------------------------
*     This is the default main program for SQOPT
*     It provides all of the necessary workspace.
*     ------------------------------------------------------------------
      parameter         (lencw = 150000, leniw = 400000, lenrw = 600000)
      character*8        cw(lencw)
      integer            iw(leniw)
      double precision   rw(lenrw)

      call sqmps1( cw, lencw, iw, leniw, rw, lenrw )

*     end of main program for stand-alone SQOPT.
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine sqmps1( cw, lencw, iw, leniw, rw, lenrw )

      implicit           double precision (a-h,o-z)
      character*8        cw(lencw)
      integer            iw(leniw)
      double precision   rw(lenrw)

*     ==================================================================
*     sqmps1 is used for the stand-alone version of the optimizer.
*     It is called by the main program (or equivalent driver).
*     It repeatedly looks for a new problem in the SPECS file
*     and asks for it to be solved, until s3file returns inform gt 1,
*     which means an ENDRUN card was found in the SPECS file,
*     or end-of-file was encountered.
*
*     15 Nov 1991: First version based on Minos 5.4 routine minos1.
*     16 May 1998: Current version.
*     ==================================================================
      character*30       title
      integer            plInfy

      external           s3opt
      external           qpHx, sqHx

      parameter         (plInfy    =  70)

      parameter         (maxru     =   2)
      parameter         (maxiu     =   4)
      parameter         (maxcu     =   6)

      parameter         (maxrw     =   3)
      parameter         (maxiw     =   5)
      parameter         (maxcw     =   7)

      parameter         (iSpecs    =  11)
      parameter         (iPrint    =  12)
      parameter         (iSumm     =  13)

      parameter         (nnCon     =  21)
      parameter         (nnJac     =  22)
      parameter         (nnObj     =  23)
      parameter         (nnL       =  24)
      parameter         (ngObj     =  26)
      parameter         (nnH       =  27)

      parameter         (lvlTim    =  77)

      parameter         (maxm      = 133)
      parameter         (maxn      = 134)
      parameter         (maxne     = 135)
*     -------------------------------------------------------------------

      if (lencw .lt. 500 .or. leniw .lt. 500 .or. lenrw .lt. 500) then 
*        ---------------------------------------------------------------
*        Not enough workspace to do ANYTHING!
*        ---------------------------------------------------------------
         inform = 41
         write(*, 9000)
         go to 999
      end if

*     ------------------------------------------------------------------
*     Define global files (reader, printer, etc.)
*     ------------------------------------------------------------------
      iw(iSpecs) = 4
      iw(iPrint) = 15
      iw(iSumm ) = 6
      call s1file( 'Default files will be opened', iw, leniw )

*     ==================================================================
*     Loop through each problem in the SPECS file.
*     ==================================================================
      do 100, loop = 1, 100000
         ncalls    = loop

         call s3undf( cw, lencw, iw, leniw, rw, lenrw )

*        ---------------------------------------------------------------
*        Initialize some global values.
*        ---------------------------------------------------------------
         rw(plInfy) = 1.0d+20
         iw(lvlTim) = 3
 
         iw(maxru) = 500        ! rw(1:500) contains sqopt variables 
         iw(maxrw) = lenrw
         iw(maxiu) = 500        ! iw(1:500) contains sqopt variables   
         iw(maxiw) = leniw
         iw(maxcu) = 500        ! cw(1:500) contains sqopt variables   
         iw(maxcw) = lencw

*        Initialize timers.
      
         iw(lvlTim) = 1
         call s1time( 0, 0, iw, leniw, rw, lenrw )
      
*        Initialize dimensions that can be set in the specs file.

         iw(nnCon )  =   0
         iw(nnJac )  =   0
         iw(nnObj )  =   0
         iw(ngObj )  =   0
         iw(nnL   )  =   0
         iw(nnH   )  =   0
      
         iw(maxm  )  =   0
         iw(maxn  )  =   0
         iw(maxne )  =   0

*        ---------------------------------------------------------------
*        Define the SQOPT title and read the Specs file.
*        ---------------------------------------------------------------
         call sqtitl( title )
         call s1init( title, iw, leniw, rw, lenrw )
         call s3fils( ncalls, iw(iSpecs), s3opt,
     $                title, iw(iPrint), iw(iSumm), inform,
     $                cw, lencw, iw, leniw, rw, lenrw )
         if (inform .ge. 2) then
            inform = 100 + inform 
            go to 999
         end if
      
         call s1file( 'Open files defined in specs file', iw, leniw  )

*        Set undefined MPS options to their default values.

         call s3dflt( cw, lencw, iw, leniw, rw, lenrw )

*        ---------------------------------------------------------------
*        Get values of the problem dimensions:
*        following variables:
*           maxm , maxn , maxne
*           maxS ,  maxR 
*           ngObj
*        Initialize  a, ha, ka, 
*                 bl, bu, iObj, and  ObjAdd. 
*        Compute the array pointers accordingly.
*        ---------------------------------------------------------------
         call s1time( 1, 0, iw, leniw, rw, lenrw )
         call s3inpt( ierror,
     $                iw(maxm), iw(maxn), iw(maxne),
     $                iw(nnCon), iw(nnJac), iw(nnObj), 
     $                m, n, ne,
     $                iObj, ObjAdd,
     $                mincw, miniw, minrw,
     $                cw, lencw, iw, leniw, rw, lenrw )
         call s1time(-1, 0, iw, leniw, rw, lenrw )
         if (ierror .ne. 0) return

*        Record n, m, ne and iObj for s5dflt.

         iw( 15)    = m
         iw( 16)    = n
         iw( 17)    = ne
         iw(218)    = iObj

*        ---------------------------------------------------------------
*        Check options.
*        Open any files needed for this problem.
*        ---------------------------------------------------------------
         call s5dflt( 'Check parameters', 
     $                cw, lencw, iw, leniw, rw, lenrw )
         call s1file( 'Open files defined in specs file', iw, leniw  )
      
*        ---------------------------------------------------------------
*        Print the options if iPrint > 0, Print level > 0, lvlPrm > 0.
*        ---------------------------------------------------------------
         call s5dflt( 'Print parameters', 
     $                cw, lencw, iw, leniw, rw, lenrw )

*        ---------------------------------------------------------------
*        Compute the storage requirements for SQOPT  from the following
*        variables:
*           m    ,  n   , ne
*           maxS ,  maxR 
*           ngObj, nnH
*        All are now known.
*        ---------------------------------------------------------------
         maxR  = iw( 56)
         maxS  = iw( 57)

         call s5Mem ( ierror, iw(iPrint), iw(iSumm),
     $                m, n, ne,
     $                iw(ngObj), iw(nnH),
     $                maxR, maxS, 
     $                iw(maxcw), iw(maxiw), iw(maxrw),
     $                lencw, leniw, lenrw,
     $                mincw, miniw, minrw, iw )
         if (ierror .gt. 0) go to 100

*        Fetch the addresses of the problem arrays (set in s3inpt).

         la      = iw(251)
         lha     = iw(252)
         lka     = iw(253)
         lbl     = iw(254)
         lbu     = iw(255)
         lxs     = iw(256)
         lpi     = iw(257)
         lhs     = iw(259)
         lhElas  = iw(260)
         lNames  = iw(261)

         call iload ( n , 0, iw(lhElas)  , 1 )
         call iload ( m , 3, iw(lhElas+n), 1 )  

         nb      = n   + m
         nka     = n   + 1 
         nName   = nb
         lrc     = lpi + m

         ldummy  = lbl
         lgObj   = lbl

         lenb    = 0
         lenx0   = 0

*        ------------------------------------------------------------------
*        Solve the problem.
*        ------------------------------------------------------------------
         call s5solv( sqHx, qpHx, 'Cold', lenb, lenx0, m,
     $                n, nb, ne, nka, nName,
     $                iObj, ObjAdd, ObjQP, Objtru,
     $                nInf, sInf,
     $                rw(la), iw(lha), iw(lka), 
     $                rw(ldummy), rw(lbl), rw(lbu), rw(lgObj),
     $                cw(lNames),
     $                iw(lhElas), iw(lhs), rw(ldummy), rw(lxs),
     $                rw(lpi), rw(lrc),
     $                inform, nS, 
     $                cw, lencw, iw, leniw, rw, lenrw,
     $                cw, lencw, iw, leniw, rw, lenrw  )

*        Print times for all clocks (if lvlTim > 0).

         call s1time( 0, 2, iw, leniw, rw, lenrw )

  100 continue
*     ==================================================================
*     End of loop through SPECS file.
*     ==================================================================

  999 return

 9000 format(  ' EXIT -- SQOPT character, integer and real work arrays',
     $         ' must each have at least 500 elements')

*     end of sqmps1
      end

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine qpHx  ( nnH, x, Hx, nState,
     $                   cu, lencu, iu, leniu, ru, lenru )

      implicit           double precision (a-h,o-z)
      double precision   Hx(nnH), x(nnH)

      character*8        cu(lencu)
      integer            iu(leniu)
      double precision   ru(lenru)

*     ==================================================================
*     This version of qpHx is a dummy routine used for solving
*     LP's with the stand-alone version of sqopt.
*     It should never be called by SQOPT.
*     
*     Warn the user (via the standard output) that it has been called.
*     ==================================================================
      nOut = 6
      if (nOut .gt. 0) write(nOut, 9000)
      return

 9000 format(/ ' XXX dummy qpHx has been called in error.')
     
*     end of qpHx
      end



