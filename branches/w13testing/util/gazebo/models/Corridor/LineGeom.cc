/**
 * Author: Henrik Kjellander
 * $Id$
 */
#include "LineGeom.hh"

//////////////////////////////////////////////////////////////////////////////
// Constructor
LineGeom::LineGeom( Body *body, const dSpaceID space, 
                    const double fromE, const double fromN, 
                    const double toE, const double toN)
  : Geom( space )
{ 
  // must set something in order to have it render. Just a 0 sized box.
  this->SetGeom( body, dCreateBox( space, 0, 0, 0 ), NULL, false);
  
  // Save the coordinates
  this->fromE = fromE;
  this->fromN = fromN;
  this->toE = toE; 
  this->toN = toN;

  this->SetColor( GzColor(0, 0, 0, 1.0));
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Destructor
LineGeom::~LineGeom()
{
  return;
}

//////////////////////////////////////////////////////////////////////////////
// Render the geom (GL)
void LineGeom::Render(RenderOptions *opt)
{
  bool dirty;
  GLuint listId;
  RenderOptions listOpt;

  // Recover stored display list for this camera
  this->GetList(opt->cameraIndex, &listId, &listOpt);

  // See if the current display list is dirty
  dirty = this->dirty;
  dirty |= (listId == 0);
  dirty |= (opt->displayMaterials != listOpt.displayMaterials);
  dirty |= (opt->displayRays != listOpt.displayRays);

  this->dirty = false;

  // Generate the display list
  if (dirty)
  {
    if (listId == 0)
      listId = glGenLists(1);
    glNewList(listId, GL_COMPILE);

    // Set material properties
    if (opt->displayMaterials)
    {
      glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, this->colorAmbient);
      glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, this->colorDiffuse);
      glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, this->colorSpecular);
      glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, this->colorEmission);
    }

    draw();

    // Unset material properties (so other geoms dont get emissive)
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, GzColor(0, 0, 0, 1));

    // Store list options
    this->SetList(opt->cameraIndex, listId, *opt);
    glEndList();
  }

  // Call the display list
  if (listId)
    glCallList(listId);

  return;
}

void LineGeom::draw()
{
  float z = 0.01; // let's draw 1 cm above the ground for better visibility
  glBegin(GL_LINES);
  glVertex3f(fromE, fromN, z );
  glVertex3f(toE, toN, z);
  glEnd();
}
