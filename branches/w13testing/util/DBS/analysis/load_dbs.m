## loads data

dirs{1} = ('/tmp/logs/2005_09_01/19_16_37');
dirs{2} = ('/tmp/logs/2005_09_01/19_17_03');
dirs{3} = ('/tmp/logs/2005_09_01/19_18_46');
dirs{4} = ('/tmp/logs/2005_09_01/19_20_07');
dirs{5} = ('/tmp/logs/2005_09_01/19_21_41');
dirs{6} = ('/tmp/logs/2005_09_01/19_26_27');
dirs{7} = ('/tmp/logs/2005_09_01/19_27_29');
dirs{8} = ('/tmp/logs/2005_09_01/19_28_47');
## pavement runs
for i = 1:8
  pave{i} = load(strcat(dirs{i}, '/DBS/dbs.dat'));
end

dirs{1} = ('/tmp/logs/2005_09_01/19_36_45');
dirs{2} = ('/tmp/logs/2005_09_01/19_37_38');
dirs{3} = ('/tmp/logs/2005_09_01/19_38_42');
dirs{4} = ('/tmp/logs/2005_09_01/19_40_18');
dirs{5} = ('/tmp/logs/2005_09_01/19_41_37');
dirs{6} = ('/tmp/logs/2005_09_01/19_43_12');
dirs{7} = ('/tmp/logs/2005_09_01/19_54_43');
dirs{8} = ('/tmp/logs/2005_09_01/19_56_26');
## block runs
for i = 1:8
  wood{i} = load(strcat(dirs{i}, '/DBS/dbs.dat'));
end

dirs{1} = ('/tmp/logs/2005_09_01/20_16_39');
dirs{2} = ('/tmp/logs/2005_09_01/20_18_08');
dirs{3} = ('/tmp/logs/2005_09_01/20_19_02');
## parking stop runs
for i = 1:3
  conc{i} = load(strcat(dirs{i}, '/DBS/dbs.dat'));
end
