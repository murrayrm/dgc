/* This is the object oriented version */

#ifndef sn_msg_HH
#define sn_msg_HH

#include <stddef.h>
#include "sn_types.h"

#include <pthread.h>

#define MAX_SIZE_HOSTNAME 64
struct SSkynetID
{
	char       host[MAX_SIZE_HOSTNAME];
	modulename name;
};

/** note: many of these addresses are claimed for specific services.  Sending
   messages with TTL > 1 may hose some of these services and piss people off.
   Conversely, if our router is configured to forward multicast traffic from
   outside, it will piss us off
*/
#define MIN_ADDR "224.0.2.0"
#define MAX_ADDR "238.255.255.255"
#if 0
const int MIN_PORT = 1024;  /* can be opened by non-priveleged process */
const int MAX_PORT = 65535;
#endif


#define dg_port  38475 //random



#define sn_BLOCK    0   /*wait till a message is ready*/
#define sn_NOBLOCK  1   /*return immidieatly*/
/* sn_msg and modulename types are temporary.  Eventually their functionality
   will be replaced by an rc file
*/

/* muxsock is a hack.  It is designed to allow a listening channel to read from
 * two independent sockets which look like one to the user.  I'm using this
 * struct to avoid changing the user interface to skynet.
 */

struct muxsock
{
  unsigned short int mcast;
  union{
    unsigned short int ux;
    unsigned short int type;
  }aux;
};

/*struct sn_chan
{
  int mcast;
  int ux;
  sn_msg type;
}*/

class skynet
{
public:
	skynet(modulename myname, int key);    //instantiate with MTA key and 
	//register

	skynet(const skynet &other);
	
	uint32_t get_id();                 /* get your module id */
	~skynet();                  //unregister

	/** listen takes as an argument the type of messages you want 
			to listen to and the name of the module that is sending them.  You
			can't listen to all messages of a all types.  Return value is the
			channel to use.  return value of -1 indicates error.  This returns
      a skynet channel, not a socket.  You cannot use socket commands directly.
	*/

	int listen(sn_msg mytype, modulename somemodule);

	/** sn_select(channel) waits for data to be available on that channel */
	void sn_select(int channel);
	
  /** stream_listen returns a tcp connection */  
  int stream_listen(sn_msg mytype, modulename somemodule);
  
  /** sn_get_msg gets a message.  Channel is what you got from sn_listen.  *
	 mybuf is a buffer that the user must allocate.  bufsize is the size in bytes
	 of that buffer.  options is an ORed list of the following: 0 is
	 default. pMutex, if specified, will lock before the data is written and
	 unlock immediately after. If bReleaseMutexWhenDone is false, the mutex will
	 not be unlocked. Return value is #of bytes read, or -1 on error.
	*/

	//vector sn_get_msg(int socket, int options);
	size_t get_msg(int channel, void *mybuf, size_t bufsize, int options);
	size_t get_msg(int channel, void *mybuf, size_t bufsize, int options,
								 pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true, int numMutices = 1);

  /**stream_get_msg must be used when receiving messages on a tcp connection*/
  size_t stream_get_msg(int channel, void *mybuf, size_t bufsize, int options);
  size_t stream_get_msg(int channel, void *mybuf, size_t bufsize, int options,
												pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true, int numMutices = 1);

	/** sn_get_send_sock gets a socket to send stuff to.  It returns the
	 * number of
	 the socket, or -1 on error.  It's not neccesary to have this call,
	 but it makes it more efficient to send messages.
	*/
        
	int get_send_sock(sn_msg type);

	/** get_stream_sock makes a tcp connection.  remote_addr must be in network
      byte order */

  int get_stream_sock(sn_msg type, struct in_addr* remote_addr);
  
  /** sn_send_msg sends a messages.  You cannot choose to whom to send a *
	 message, you may only send it.  return value is the number of bytes sent.  -1
	 on eror.  Options are as follows: 0 is default. pMutex, if specified, will
	 lock before the data is sent and unlock immediately after. If
	 bReleaseMutexWhenDone is false, the mutex will not be unlocked. Return value
	 is #of bytes sent, or -1 on error.
	*/
        
	//size_t send_msg(int socket, vector msg, int options);
	size_t send_msg(int channel, void* msg, size_t msgsize, int options);
	size_t send_msg(int channel, void* msg, size_t msgsize, int options,
									pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true, int numMutices = 1);

  /** stream_send_msg must be used when sending messages on a tcp connection*/
 	size_t stream_send_msg(int channel, void* msg, size_t msgsize, int options);
 	size_t stream_send_msg(int channel, void* msg, size_t msgsize, int options,
									pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true, int numMutices = 1);
       
	/** sn_add_global tells skynetd to allocate some space for name,
	 * without initializing it.  return value is 0 on success, -1 on
	 * error.
	 */
        
	int add_global(char* name, size_t varsize);

	/** sn_write_global writes var and skynetd then pushes it out to the
	 * other skynetd instances.  returns 0 on success, -1 on error. */
        
	int write_global(void* var, char* name, size_t size);

	/** sn_read_global returns a pointer to a global variable within the
	 * shared memory region.  sn_global_map must have already been called.
	 * returns null on error
	 */
        
	void* read_global(char* name);

	/** sn_listen_global tells skynetd to begin updating the variable name.
			it is neccesary to call this before using sn_read_global .  returns
			0 on success, -1 on error.
	*/
        
	int listen_global(char* name);

	/** sn_unlisten_global tells skynetd to stop updating variable name.
	 * It is not
	 strictly neccesary to ever use this call, but doing so may improve
	 system performance.  returns 0 on success, -1 on error.
	*/
        
	int unlisten_global(char* name);

	/** sn_remove_global tells skynetd to deallocate variable name and
	 * discontinue
	 updating it.  It is not strictly neccesary to use this call, but
	 doing so may improve system performance.
	*/
        
	int remove_global(char* name);

	/** tells skynetd to reallocate space for variable name.  returns 0 on
	 * success,
	 -1 on error.
	*/
        
	int change_global(char* name, size_t size);

	/** heartbeat is a keepalive signal.  Call it at least once every 5
	 * seconds or skynetd will think your module has died.
	 */

	int heartbeat();

	/** pause() is for timing.  It will block until skynetd decides that
	 * your module should run
	 */ 
        
	int pause();

private:
	int key;
	int modcom_sock;
	uint32_t id;
	modulename name;
  /* listen_mutex makes listen() thread safe*/      
  pthread_mutex_t listen_mutex;
	/* mk_modcom_sock gets a socket to communicate with skynetd */
        
	void mk_modcom_sock();

  void snd_register();
  
  /* get_large_sock() opens a unix domain stream connection with skynetd
   * for purposes of receiving large messages
   */
  int get_large_sock(sn_msg type);
  
  /* GLOBAL FUNCTIONS ONLY BELOW */
	/* sn_global_map maps a region of shared memory created by skynetd.
	 * happens on registration  */
        
	void* global_map();
        

};
#endif
