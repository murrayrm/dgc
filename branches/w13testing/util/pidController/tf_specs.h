/** shamelessly pirated from Dima to make my updating
 * of parameters more elegant */

#ifndef _SPECS_H_
#define _SPECS_H_

#include <math.h>

/** this should contain "_(type, name, init value)\" */
#define SPECLIST(_) \
  _(int, DEBUG_LEVEL, 0) \
  \
  /** indices into the error arrays */ \
  _(int, FRONT, 0)\
  _(int, REAR, 1) \
  _(int, YAW, 2) \
  \
  _(int, NUM_TRAJ_POINTS_TO_END_OF_TRAJ_TO_START_STOPPING, 100) \
  \
  /** Distance in meters from the path (perpendicular distance) \
  * outside of which the controller will poitn Alice directly \
  * toards (perpendicular to) the path */ \
  _(double, PERP_THETA_DIST, 20.0) \
  \
  /** alpha and beta gains for the combind error controler */ \
  _(double, ALPHA_GAIN, 0.4) \
  \
  _(double, BETA_GAIN, (2.0*ALPHA_GAIN*PERP_THETA_DIST/(M_PI - M_PI*ALPHA_GAIN))) \
  /** value of combined error that causes the modified \
  * commanded speed (which has a max value = v_traj \
  * although its current value is scaled by a negative \
  * gradient linear slope which has a y-intercept of \
  * v_traj, and a calculated -ve gradient, until the \
  * saturation point at a small speed, and a relatively \
  * large combined error */ \
  _(double, C_ERROR_SAT, 2.0) \
  \
  /**
  * Lookahead *as a multiple of the wheelbase* used \
  * when determining the lateral controller feed-forward \ 
  * term - the position to which the closest point on \
  * the trajectory is used to calculate the feed-forward \
  * term is calculated by moving along a straight line \
  * drawn between the two axles by a distance equal to\
  * (wheelbase*LOOKAHEAD_FF_LAT_REFPOS), note that \
  * LOOKAHEAD_FF_LAT_REFPOS can be EITHER POSITIVE OR NEGATIVE*/ \
  _(double, LOOKAHEAD_FF_LAT_REFPOS, 0.0) \
  \
  /**This is the 'small speed' at which the reference \
  * speed is set when the combined error is greater\
  * than the C_ERROR_SAT */ \
    _(double, MINSPEED, 0.01) \
  \
  _(double, V_LAT_ERROR_SAT, 4.0) \
  \
  /** IF an error occurs (for example if Alice gets close \
  * enough to the end of the Traj etc) then the accelCmd \
  * is set to this value to bring Alice to a stop quickly */ \
  _(double, ERROR_ACCEL_CMD, -5.0) \
  \
  /** IF TrajFollower reaches a point on the 'current' trajectory \
  * where the traj-speed is < MAX_SPEED_TAKEN_TO_BE_STATIONARY \
  * then TrajFollower assumes that it is NOT being asked to proceed \
  * at some very small, but finite speed, but rather that it is \
  * being commanded to STOP (as there are reasons why dealing with \
  * actual ZERO speed commands are difficult, hence it is preferable \
  * for the planner to command a VERY small positive speed to indicate \
  * a STOP - this needs to be interpreted correctly as a STOP by \
  * TrajFollower */ \
  _(double, MAX_SPEED_TAKEN_TO_BE_STATIONARY, 0.1) \
  \
  /** The speed for which the input gains are suitable */ \
  _(double, STANDARD_SPEED, 5.0) \
\
  /** Proportional gain for the standard speed */ \
  _(double, PGAIN, .1) \
\
/** Integral gain for the standard speed */ \
  _(double, IGAIN, .01) \
\
  /**Derivative gain for the standard speed */ \
  _(double, DGAIN, .08) \
\
  /** Width of regions for gain scheduling. in m/s */ \
  _(double, REGION_WIDTH, 1.0) \
\
  /** overlap between regions to prevent jumping
   * back and forth between adjacent ones. */ \
  _(double, REGION_OVERLAP, 0.1)

#define EXTERNDEFINESPEC(type, name, val) \
extern type name;

#define DEFINESPEC(type, name, val) \
type name;

#define EXTERNDEFINESPECS namespace specs { SPECLIST(EXTERNDEFINESPEC) }; using namespace specs
#define DEFINESPECS namespace specs { SPECLIST(DEFINESPEC)	}; using namespace specs


EXTERNDEFINESPECS;


extern void readspecs(void);

#endif // _SPECS_H_
