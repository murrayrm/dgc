#include "pseudocon.hh"

using namespace std;
using namespace superCon;


StatePrinter::StatePrinter(int skynetKey) : m_skynet(MODsupercon, skynetKey)
{
  // do something here if you want
}




StatePrinter::~StatePrinter()
{
  // Do something
}



void StatePrinter::ActiveLoop()
{
  // set up message counter
  int message_counter = 0;
  int sock;
  int recsock;
  int adrivesock;
  int brec;
  int btorec;
  char* m_pDataBuffer;
  int pseudoType;
  double commandArg;
  int test;
  superConTrajFcmd com;
  newSpeedCapCmd speedCap;
  
  drivecmd_t* comp;

  drivecmd_t my_command;
  memset(&my_command, 0, sizeof(my_command));
  my_command.my_actuator = estop;
  my_command.number_arg = 2;
  
  cout << "\n\nCurrent SKYNET_KEY: ";
  cout.flush();
  system("echo $SKYNET_KEY");

  sock = m_skynet.get_send_sock(SNsuperconTrajfCmd);
  adrivesock = m_skynet.get_send_sock(SNdrivecmd);
  int speedCapSock = m_skynet.get_send_sock(SNtrajFspeedCapCmd);
  int speedCapSize = sizeof(newSpeedCapCmd);
  recsock = m_skynet.listen(SNsuperconTrajfCmd, MODsupercon);
  receiveAdrivecmdSock = m_skynet.listen(SNdrivecmd, MODsupercon);
  btorec = sizeof(superConTrajFcmd);
  m_pDataBuffer = new char[btorec];
  cout << "size of com is: " << btorec<<endl;
  cout << "send socket is: " << sock << endl;
  
  // Loop is trapped here during execution
  while(true)
    {
      
      // grab where we are and store it as location, so we only have to access astate
      // once per time through the loop

      cout << "Enter desired functionality" <<endl<<"1 for reverse/estop testing"<<endl<<"2 for speedcap testing"<<endl;
      cin >> test;

      if(test == 1)
	{

      cout << "Enter Command: "<< endl;
      cout << "Options: 1 = fwd, 2 = rev, 3 = estpP, 4 = estpR, 5 = listen & print only [actuator command messages]" << endl;
      cin  >> pseudoType;
      switch( pseudoType ) {

      case 1: //fwd
	{
	  memset(&my_command, 0, sizeof(my_command));
	  my_command.my_actuator = estop;
	  my_command.my_command_type = set_position;
	  my_command.number_arg = ESTP_PAUSE;
	  m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	  cout << "sent estop pause" << endl;

	  memset(&my_command, 0, sizeof(my_command));
	  my_command.my_actuator = trans;
	  my_command.my_command_type = set_position;	  	  
	  my_command.number_arg = GEAR_DRIVE;
	  m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	  cout << "sent trans --> DRIVE" << endl;

	  com.commandType = tf_forwards;
	  com.distanceToReverse = 0;
	  m_skynet.send_msg(sock, &com, sizeof(com),0);
	  
	  memset(&my_command, 0, sizeof(my_command));
	  my_command.my_actuator = estop;
	  my_command.my_command_type = set_position;
	  my_command.number_arg = ESTP_RUN;
	  m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	  cout << "sent estop run" << endl;
	}
	break;

      case 2: //rev
	{
	  memset(&my_command, 0, sizeof(my_command));
	  my_command.my_actuator = estop;
	  my_command.my_command_type = set_position;
	  my_command.number_arg = ESTP_PAUSE;
	  m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	  cout << "sent estop pause" << endl;

	  cout << "Enter the distance to reverse in meters" << endl;
	  cin >> commandArg;
	  
	  memset(&my_command, 0, sizeof(my_command));
	  my_command.my_actuator = trans;
	  my_command.my_command_type = set_position;
	  my_command.number_arg = GEAR_REVERSE;
	  m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	  cout << "sent trans --> REVERSE" << endl;

	  com.commandType = tf_reverse;
	  com.distanceToReverse = commandArg;
	  m_skynet.send_msg(sock, &com, sizeof(com),0);
	  cout << "Message sent - reversing..." << endl;

	  memset(&my_command, 0, sizeof(my_command));
	  my_command.my_actuator = estop;
	  my_command.my_command_type = set_position;
	  my_command.number_arg = ESTP_RUN;
	  m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	  cout << "sent estop run" << endl;

	}
	break;
	
      case 3: //estpP
	{
	  memset(&my_command, 0, sizeof(my_command));
	  my_command.my_actuator = estop;
	  my_command.my_command_type = set_position;
	  my_command.number_arg = ESTP_PAUSE;
	  m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	  cout << "sent estop pause" << endl;
	}
	break;

      case 4: //estpR
	{
	  memset(&my_command, 0, sizeof(my_command));
	  my_command.my_actuator = estop;
	  my_command.my_command_type = set_position;
	  my_command.number_arg = ESTP_RUN;
	  m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	  cout << "sent estop run" << endl;
	}
	break;

      case 5: //listen & print only
	{
	  m_skynet.get_msg( receiveAdrivecmdSock, m_pDataBuffer, sizeof(drivecmd_t), 0 );
	  comp = (drivecmd_t*)m_pDataBuffer;
	  cout << "Actuator:" << comp->my_actuator << endl;
	  cout << "Command type:" << comp->my_command_type << endl;
	  cout << "number arg:" << comp->number_arg << endl; 
	}
	break;
      }
      cout << "finished processing commands" << endl;
    }

      if(test == 2) {
	int action;
	cout<<"enter action {1=addmax, 2=addmin, 3=removemax 4=removemin}"<<endl;
	cin>>action;
	if(action==1) speedCap.m_speedCapAction=add_max_speedcap;
if(action==2) speedCap.m_speedCapAction=add_min_speedcap;
if(action==3) speedCap.m_speedCapAction=remove_max_speedcap;
if(action==4) speedCap.m_speedCapAction=remove_min_speedcap;


 double cmd;
 cout<<"enter desired speed"<<endl;
 cin>>cmd;
 speedCap.m_speedCapArgument = cmd;
 speedCap.m_speedCapSndModule = TF_MOD_SUPERCON;
 m_skynet.send_msg(speedCapSock, &speedCap, speedCapSize);
      }
	

      }//end of speedcap options    
}



int main()
{
  //Setup skynet
  int intSkynetKey = 0;

  char* ptrSkynetKey = getenv("SKYNET_KEY");
  
  if(ptrSkynetKey == NULL)
    {
      cout << "Unable to get skynet key!" << endl;
      return 0;
    }
  else
    {
      //cout << "Got to StatePrinter int main" << endl;
      intSkynetKey = atoi(ptrSkynetKey);
      StatePrinter StatePrinterObj(intSkynetKey);
      
      
      //cout << "Entering ActiveLoop." << endl;
      StatePrinterObj.ActiveLoop();
    }
  
  return 0;
}
