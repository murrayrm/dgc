/* SUPERCON STRATEGY TITLE: Nominal
 * HEADER FILE (.hh)
 * DESCRIPTION: This strategy is the NOMINAL operation strategy, and the system
 * should transition to, and remain in, this strategy if none of the required
 * entry/continuation conditions for the other strategies are met.  Any route
 * that could be taken by the system by transitioning between different strategies
 * SHOULD return the system to the nominal state
 */

#include "Strategy.hh"
#include "sc_specs.h"

#include<iostream>
using namespace std;

class CStrategyNominal : public CStrategy
{

private:

  //# of the next stage to be executed - Stage #'s START at 1 (i.e. when
  //entering a new strategy nextStage = 1) - this is stored internally
  //by any strategy class instance so the information does not have to
  //be passed in stepForward(...)
  int nextStage;
  
  //latching bool instance to track whether to skip the operations for the
  //current stage -> value (true/false) updated by the entry operations.
  //NOTE: stage operations SKIPPED, iff skipStageOps == true.
  bool_latched skipStageOps;

public:
  CStrategyNominal() : CStrategy() {}
  ~CStrategyNominal() {}

  //Step one step forward using the (new) supplied diagnostic rule results
  void stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface );
  //We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
  void leave( CStrategyInterface *m_pStrategyInterface );
  //We are entering this state, do initialisation (NO NEW STATE TRANSFERS)
  void enter( CStrategyInterface *m_pStrategyInterface );
};


