//SUPERCON STRATEGY TITLE: estop pause NOT (caused by) superCon - BLOCKING-ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyEstopPausedNOTsuperCon.hh"
#include "StrategyHelpers.hh"

void CStrategyEstopPausedNOTsuperCon::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE

  switch( nextStage ) {

  case 1: //A NON-superCon (adrive/DARPA) estop pause has occurred - hence
          //WAIT in this strategy & stage until the pause is removed (->RUN)
          //when pause is removed, transition to NOMINAL strategy
    {
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL
       * STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* TERMINATION & TRANSITION CONDITIONS GENERIC TO *ALL* STAGES IN *THIS*
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( m_pdiag->pausedNOTsuperCon == true ) {
	//still paused --> don't transition to nominal strategy yet
	skipStageOps = true;
      }
      //NOTE - nominal strategy resets the superCon estop to pause on entry
      //hence there are no considerations of the superCon estop pause position
      //in this strategy/stage

      /* TERMINATION & TRANSITION CONDITIONS SPECIFIC TO THIS STAGE OF THE
       * STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT - note that these
       * operations are ONLY executed IFF skipStageOps = FALSE */
      if( skipStageOps == false ) {
	
	transitionStrategy(StrategyNominal, "EstopPausedNOTsuperCon - Finished Stage 1 (no longer adrive/DARPA estop paused)");	
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "EstopPausedNOTsuperCon - Finished stage 1: NO longer adrive/DARPA estop paused --> NOMINAL");
	//DO NOT update nextStage here - this strategy should continually loop
	//in this stage until the !superCon estop -> RUN
      }
    }
    break;
    
    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: EstopPausedNOTsuperCon::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: EstopPausedNOTsuperCon::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: CStrategyEstopPausedNOTsuperCon - default stage reached!");
    }

  }
  
}


void CStrategyEstopPausedNOTsuperCon::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "EstopPausedNOTsuperCon - Exiting");
}

void CStrategyEstopPausedNOTsuperCon::enter( CStrategyInterface *m_pStrategyInterface )
{
  //reset the nextStage counter (starts at ONE, NOT zero) as the
  //strategy is being entered --> first stage to execute should be
  //stage #1
  nextStage = 1;
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "EstopPausedNOTsuperCon - Entering");
}
