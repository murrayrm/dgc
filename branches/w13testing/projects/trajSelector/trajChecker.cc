#include "MapAccess.h"
#include "MapConstants.h"
#include "trajChecker.h"
#include <iostream>
#include <math.h>
//#include "DGCutils"

using namespace std;

int deltaSize;

int previousTraj_num;  //Number of the planner whose traj we were previously following; used for logging.


trajChecker::trajChecker(int num, int sn_key)
  :trajSelector(num, sn_key)
{
  cout<<"Entering constructor."<<endl;

  cout<<"Constructing trajChecker with Skynet key = "<<sn_key<<endl;

  cout<<"Beginning initialization."<<endl;
  //Initialize the number of planners the vehicle has, and the bonus to give the currently followed traj.
  numTrajs = num;

  //Initialize the arrays to hold the trajs, the traj updates, the average speed of each traj, and the boolean array to determine which planners to listern for.

  trajs = new CTraj[numTrajs];
  trajUpdates = new CTraj[numTrajs];

  avgSpeeds = new double[numTrajs];

  plannerWeights = new double[numTrajs];

  listenForTrajs = new bool[numTrajs];

  excessiveSpeeds = new bool[numTrajs];

  badTrajs = new bool[numTrajs];

  trajTimes = new unsigned long long[numTrajs];

  trajElapsedTimes = new double[numTrajs];

  times = new pthread_mutex_t[numTrajs];

  trajSocket = m_skynet.get_send_sock(SNselectorTraj);

  cout<<"Reading in config file."<<endl;
  //Read in the config file to determine which planners to listen for.
  char buffer[30];
  ifstream infile("trajChecker.config");
  
  infile >> PERCENT_BONUS;

  int active;
  double bonus;


  //Parse the lines in the file to determine which planners to listen for and what priority bonus to give them.

  for(int i = 0; i < numTrajs; i++)
    {
      infile >> active;
      if(active == 1)
	{
	  listenForTrajs[i] = true;
	}
      else
	{
	  listenForTrajs[i] = false;
	}

      infile >> bonus;
      plannerWeights[i] = bonus;

      infile.getline(buffer, 30);  //Read and junk the rest of the line; we don't need it.

    }
  
 
  cout<<"Creating mutexes."<<endl;
  //Need to create mutexes and blank trajs for planners that we're listening for.


  if(listenForTrajs[0])
    {
      cout<<"Creating deliberativeTraj mutex"<<endl;
      DGCcreateMutex(&deliberativeTraj);
    }

  if(listenForTrajs[1])
    {
      cout<<"Creating reactiveTraj mutex"<<endl;
      DGCcreateMutex(&reactiveTraj);
    }

  if(listenForTrajs[2])
    {
      cout<<"Creating blankTraj Mutex"<<endl;
      DGCcreateMutex(&blankTraj);
    }

  if(listenForTrajs[3])
    {
      cout<<"Creating roadTraj mutex"<<endl;
      DGCcreateMutex(&roadTraj);

    }

  if(listenForTrajs[4])
    {
      cout<<"Creating rddfTraj Mutex."<<endl;
      DGCcreateMutex(&rddfTraj);
    }

  //Create mutexes to keep track of accss to the array of times.
  for(int i = 0; i < numTrajs; i++)
    {
      if(listenForTrajs[i])
	{
	  DGCcreateMutex(&times[i]);
	}
    }

  //Create mutex to control access to previous traj info
  DGCcreateMutex(&previousTrajMutex);

  //Set up the map stuff
  DGCcreateMutex(&mapMutex);
  
  mapFullRequestSocket = m_skynet.get_send_sock(SNfullmaprequest);
  mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
  mapDelta = new char[MAX_DELTA_SIZE];



  //Initialize the map
  m_map.initMap(CONFIG_FILE_DEFAULT_MAP);

  //Insert the speed layer
  speedLayer = m_map.addLayer<double>(CONFIG_FILE_COST);

  //Get the vehicle's current location.
  cout<<"Updating state."<<endl;
  CStateClient::UpdateState();

  vehicleUTMNorthing = m_state.Northing_rear();
  vehicleUTMEasting = m_state.Easting_rear();

  m_map.updateVehicleLoc(vehicleUTMNorthing, vehicleUTMEasting);

  cout<<"Initializing map."<<endl;
 
  bool bRequestMap = true;
  m_skynet.send_msg(mapFullRequestSocket, &bRequestMap, sizeof(bool), 0);  //Request and then receive initial map info.
  mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
  if(mapDeltaSocket < 0)
    {
      cerr << "trajSelector constructor returned  skynet listen error." << endl;
    }
  
  //Receive map info. 
  RecvMapdelta(mapDeltaSocket, mapDelta, &deltaSize);


  cout<<"Received initial map info"<<endl;
  //Apply initial map info.
  m_map.applyDelta<double>(speedLayer, mapDelta, deltaSize);
  



  //Wait here to receive the first set of trajs.

  cout<<"Waiting for first set of trajs to get here."<<endl;

  if(listenForTrajs[0])
    {
      cout<<"Waiting for deliberative traj."<<endl;
      deliberativeSocket = m_skynet.listen(SNtraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.

      RecvTraj(deliberativeSocket, &trajUpdates[0]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[0]);

      cout<<"Received deliberative traj."<<endl;
    }

 if(listenForTrajs[1])
    {
      cout<<"Waiting for reactive traj."<<endl;
      reactiveSocket = m_skynet.listen(SNreactiveTraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.

      RecvTraj(reactiveSocket, &trajUpdates[1]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[1]);

      cout<<"Received reactive traj."<<endl;
    }


 if(listenForTrajs[2])
    {
      cout<<"Waiting for blank traj."<<endl;
      #warning:"Need a new skynet type for blank planner trajs here."
      blankSocket = m_skynet.listen(SNplannertraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.

     RecvTraj(blankSocket, &trajUpdates[2]);  //Receive the traj Update the buffer.

     DGCgettime(trajTimes[2]);

      cout<<"Received reactive traj."<<endl;
    }

 if(listenForTrajs[3])
    {
      cout<<"Waiting for road following traj."<<endl;
      #warning:"Need to add a new traj for plans coming from roadFinding."
      roadSocket = m_skynet.listen(SNplannertraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.
      RecvTraj(roadSocket, &trajUpdates[3]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[3]);

      cout<<"Received road following traj."<<endl;

    }

 if(listenForTrajs[4])
    {
      cout<<"Waiting for rddf traj."<<endl;
      rddfSocket = m_skynet.listen(SNRDDFtraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.

      RecvTraj(rddfSocket, &trajUpdates[4]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[4]);

      cout<<"Received rddf traj."<<endl;
    }


 //Initialize the traj we are following.  Note that the lowest-numbered active traj in the array trajs has priority by default.
  for(int i = numTrajs - 1; i >= 0; i--)
    {
      if(listenForTrajs[i])
	{
	  trajWeAreFollowing = i;
	}
    }

  cout<<"Constructor finished."<<endl;
}

void trajChecker::check()
{

  cout<<"Starting main program loop."<<endl;

  //Start up the various function threads.
  
  cout<<"Starting map listener thread."<<endl;
  DGCstartMemberFunctionThread<trajChecker>(this, &trajChecker::mapListener);

  cout<<"Starting deliberative listener thread."<<endl;
  DGCstartMemberFunctionThread<trajChecker>(this, &trajChecker::deliberativeListener);

  cout<<"Starting reactive listener thread."<<endl;
  DGCstartMemberFunctionThread<trajChecker>(this, &trajChecker::reactiveListener);

  //Logging shiznit

  string log_directory;
 
  string LOG_FILE_name;
  string HEADER_FILE_name;

  bool loggingEnabled;
  bool newSession; //Records whether or not this is the first time we're entering data into a log file.

  ofstream outfile;
  ofstream header_file;

  int logging_level;


  while(true)
    {

      previousTraj_num = trajWeAreFollowing;
      cout<<"Entering method select()"<<endl;
      //Update the list of trajs to the most current available set of trajs.
      
      cout<<"Updating trajs."<<endl;
      
      //Get the deliberative planner's traj.
      DGClockMutex(&deliberativeTraj);
      trajs[0] = trajUpdates[0];
      DGCunlockMutex(&deliberativeTraj);

      //Get the reactive planner's traj
      DGClockMutex(&reactiveTraj);
      trajs[1] = trajUpdates[1];
      DGCunlockMutex(&reactiveTraj);
      
      //Check to see if loggin is enabled with each loop execution.
      //loggingEnabled = getLoggingEnabled();

      if(loggingEnabled)
	{
	  logging_level = getMyLoggingLevel();
	  //If logging is enabled and this is a new session, we need to output a new header file.
	  newSession = checkNewDirAndReset();
	  if(newSession)
	    {
	      log_directory = getLogDir();
	      HEADER_FILE_name = log_directory + HEADER_FILE;
	      LOG_FILE_name = log_directory + LOG_FILE;

	      cout<<"Creating new header file: "<<HEADER_FILE_name<<endl;
	      header_file.open(HEADER_FILE_name.c_str(), ios::out | ios::trunc);
	      for(int i = 0; i < numTrajs;i ++)
		{
		  header_file<<(listenForTrajs[i] ? 1 : 0)<<endl;
		}

	      header_file<<PERCENT_BONUS;

	      header_file.close();

	      if(outfile.is_open())
		{
		  outfile.close();
		}

	      outfile.open(LOG_FILE_name.c_str(), ios::out | ios::trunc);
	    }
	}
      
      
      cout<<"Beginning selection."<<endl;
      
      //Reset the array to keep track of excessive speeds.
      for(int i = 0; i < numTrajs; i++)
	{
	  excessiveSpeeds[i] = false;
	}

      //Reset the array to keep track of bad trajs.

      for(int i = 0; i < numTrajs; i++)
	{
	  badTrajs[i] = false;
	}


            
      //Determines the traversal time for each of the various trajs.
      double time, speed, distance, sectionDistance, trajAvgSpeed;
      
      double* northing; 
      double* easting;
      double* n1;
      double* e1;
      double mapSpeed;
      double yaw;
      double junk1, junk2, junk3;  //Needed for accessing Dima's function, as it takes several arguments that it uses to return data that we don't care to know about.

      unsigned long long timeDifference;  //Keeps track of current/elapsed time
      unsigned long long currentTime;

      //Begin processing deliberative planner's traj.
            
      cout<<"Processing traj from deliberative planner."<<endl;
      
      //Calculate average speed

      DGCgettime(currentTime);  //Record the current time.
      DGClockMutex(&times[0]);
	     
      timeDifference = currentTime -  trajTimes[0];
	  
      trajElapsedTimes[0] = DGCtimetosec(timeDifference);  //Calculate the elapsed time from when this traj was received until right now.
      DGCunlockMutex(&times[0]);
	  
      cout<<"Elapsed time for the deliberative planner  is "<<trajElapsedTimes[0]<<" seconds."<<endl;
	  
      //Check to see if the deliberative traj is too old.

      if(trajElapsedTimes[0] > TRAJ_TIMEOUT)  //Deliberative traj is not okay.
	{	      
	  badTrajs[0] = true;
	}
	      
      time = 0; //Set the traversal time for the current traj to zero.
	      
      distance = 0;  //Set the total distance for the current traj to zero.
	      
      //Determine the number of points in the current traj
      int numPoints = trajs[0].getNumPoints(); 
	      
      //Get the data for the current traj
      northing = trajs[0].getNdiffarray(0);
      easting = trajs[0].getEdiffarray(0);
      n1 = trajs[0].getNdiffarray(1);
      e1 = trajs[0].getNdiffarray(1);
	      
      for(int point = 0; point <= numPoints - 2; point++)
	{
	  sectionDistance = sqrt( pow( (northing[point+1] - northing[point]), 2) + pow( (easting[point+1] - easting[point]), 2) ); //Compute the straight-line distance from the curent point to the next point on the traj
	  speed = sqrt( pow( n1[point], 2) + pow( e1[point], 2 ) ); //Compute the speed at which the vehicle will be traversing this section
	  time += sectionDistance / speed;  //Increment the total time appropriately
	  distance += sectionDistance; //Increment the total distance appropriately.
		  
#warning:"May also want to include something about lookahead distance of the object we're looking at as well (i.e., only consider trajs to have excessive speeds for nearby obstacles, as if an just popped up on our sensors, the planners probably just haven't had time to take account of it yet, whereas if it's close to us, that could indicate planner failure.  May also want to take into consideration the speed at which we're currently traveling when taking into consideration the lookahead distance."
	  //Use Dima's function to check if we're going too fast for the map value at this point.
		  
		  
	  yaw = atan(e1[point]/n1[point]);  // = tangent of yaw.
	  DGClockMutex(&mapMutex);
	  getContinuousMapValueDiffGrown(&m_map, speedLayer, northing[point], easting[point], yaw, &mapSpeed, &junk1, &junk2, &junk3);
	  DGCunlockMutex(&mapMutex);
	  if(speed > mapSpeed)  //This traj has excessive speed.
	    {
	      excessiveSpeeds[0] = true;
	    }
		  
	}
	      
      if(excessiveSpeeds[0])
		{
		  cout<<"Deliberative planner has generated a plan with excessive speed."<<endl;
		}
	      
      trajAvgSpeed = distance / time;  //Find the average traj speed by dividing total distance by total time.
	      
      avgSpeeds[0] = trajAvgSpeed; //Store the time value for this traj
      cout<<"Calculated average speed for the deliberative traj is "<<avgSpeeds[0]<<" m/s"<<endl;


      //End processing of deliberative traj.

      //Deliberative traj has priority, so if it's traj is ok, send it.

      if(!badTrajs[0] && !excessiveSpeeds)
	{
	  trajWeAreFollowing = 0;
	  cout<<"Finished selection.  Sending deliberative traj with average speed "<<avgSpeeds[0]<<" m/s"<<endl;
	  //Publish the traj that we want to follow (stored in the Ctraj array in slot trajs[0])
	  SendTraj(trajSocket, &trajs[trajWeAreFollowing]);
	}


      else  //Something's not right with the deliberative planner, so we need to evaluate reactive now.
	{

	  cout<<"Something's not right with the deliberative planner.  Processing traj from reactive planner"<<endl;
      
	  //Calculate average speed

	  DGCgettime(currentTime);  //Record the current time.
	  DGClockMutex(&times[1]);
	  
	  timeDifference = currentTime -  trajTimes[1];
	  
	  trajElapsedTimes[1] = DGCtimetosec(timeDifference);  //Calculate the elapsed time from when this traj was received until right now.
	  DGCunlockMutex(&times[1]);
	  
	  cout<<"Elapsed time for the reactive planner is "<<trajElapsedTimes[1]<<" seconds."<<endl;
	  
	  //Check to see if the deliberative traj is too old.
	  
	  if(trajElapsedTimes[1] > TRAJ_TIMEOUT)  //Deliberative traj is ok.
	    {	      
	      badTrajs[1] = true;
	    }
	  
	  time = 0; //Set the traversal time for the current traj to zero.
	  
	  distance = 0;  //Set the total distance for the current traj to zero.
	  
	  //Determine the number of points in the current traj
	  int numPoints = trajs[1].getNumPoints(); 
	  
	  //Get the data for the current traj
	  northing = trajs[1].getNdiffarray(0);
	  easting = trajs[1].getEdiffarray(0);
	  n1 = trajs[1].getNdiffarray(1);
	  e1 = trajs[1].getNdiffarray(1);
	  
	  for(int point = 0; point <= numPoints - 2; point++)
	    {
	      sectionDistance = sqrt( pow( (northing[point+1] - northing[point]), 2) + pow( (easting[point+1] - easting[point]), 2) ); //Compute the straight-line distance from the curent point to the next point on the traj
	      speed = sqrt( pow( n1[point], 2) + pow( e1[point], 2 ) ); //Compute the speed at which the vehicle will be traversing this section
	      time += sectionDistance / speed;  //Increment the total time appropriately
	      distance += sectionDistance; //Increment the total distance appropriately.
	      
#warning:"May also want to include something about lookahead distance of the object we're looking at as well (i.e., only consider trajs to have excessive speeds for nearby obstacles, as if an just popped up on our sensors, the planners probably just haven't had time to take account of it yet, whereas if it's close to us, that could indicate planner failure.  May also want to take into consideration the speed at which we're currently traveling when taking into consideration the lookahead distance."
	      //Use Dima's function to check if we're going too fast for the map value at this point.
	      
	      
	      yaw = atan(e1[point]/n1[point]);  // = tangent of yaw.
	      DGClockMutex(&mapMutex);
	      getContinuousMapValueDiffGrown(&m_map, speedLayer, northing[point], easting[point], yaw, &mapSpeed, &junk1, &junk2, &junk3);
	      DGCunlockMutex(&mapMutex);
	      if(speed > mapSpeed)  //This traj has excessive speed.
		{
		  excessiveSpeeds[1] = true;
		}
	      
	    }
	      
	  if(excessiveSpeeds[1])
	    {
	      cout<<"Reactive planner has generated a plan with excessive speed."<<endl;
	    }
	  
	  trajAvgSpeed = distance / time;  //Find the average traj speed by dividing total distance by total time.
	  
	  avgSpeeds[1] = trajAvgSpeed; //Store the time value for this traj
	  cout<<"Calculated average speed for the reactive traj is "<<avgSpeeds[1]<<" m/s"<<endl;
	  //End processing of reactive traj.  Now we need to determine what to do.

	  if(!badTrajs[1] && !excessiveSpeeds[1]) //If reactive is ok, send it instead.
	    {
	      trajWeAreFollowing = 1;
	      cout<<"Finished selection.  Sending reactive traj with average speed "<<avgSpeeds[0]<<" m/s"<<endl;
	      //Publish the traj that we want to follow (stored in the Ctraj array in slot trajs[0])
	      SendTraj(trajSocket, &trajs[trajWeAreFollowing]);
	    }

	  //Ok, if we've gotten this far, it's because deliberative AND reactive have problems.  Deliberative may have failed because it has excessive speeds, or because it couldn't converge to a solution quickly enough, or whatever.  In any case, reactive is more robust against outright failure, so modify its speed profile and send it out.

	  else if(!badTrajs[1])
	    {
	      modifyTraj(trajs[1]);
	      trajWeAreFollowing = 1;
	      cout<<"Finished selection.  Sending reactive traj with average speed "<<avgSpeeds[0]<<" m/s"<<endl;
	      //Publish the traj that we want to follow (stored in the Ctraj array in slot trajs[0])
	      SendTraj(trajSocket, &trajs[trajWeAreFollowing]);
	    }

	  #warning "OK, if we EVER find ourselves here, we're in SERIOUS TROUBLE.  I HAVE NO IDEA WHAT TO DO IN THIS SITUATION.  We should probably make this a fault condition of some kind."
	}

      DGClockMutex(&previousTrajMutex);
      previousTraj = trajs[trajWeAreFollowing];
      DGCunlockMutex(&previousTrajMutex);
      
      cout<<"Method select() finished."<<endl<<endl;

      //Now we need to write all this awesome data that we've just received/calculated to our super-cool log files.

      if(loggingEnabled && (logging_level != 0))
	{

	  //Print out the number of the planner whose traj we were previously following

	  outfile<<previousTraj_num<<endl;

	  for(int i = 0; i < numTrajs; i++)
	    {
	      if(listenForTrajs[i])
		{
		  //Print out a planner id number, avg speed, and elapsed time since reception on one line
		  outfile<<i<<" "<<avgSpeeds[i]<<" "<<trajElapsedTimes[i]<<endl;
		  //Print out the actual traj itself that this corresponds to ONLY IF logging_level == 3, as this leads to GIGANTIC log files.
		  if(logging_level ==3)
		    {
		      outfile<<trajs[i].getNumPoints()<<endl;
		      trajs[i].print(outfile);
		      outfile<<endl;
		    }
		}
	    }

	  //
	  if(logging_level >= 2)
	    {
	      //Now print out the values stored in the excessiveSpeeds array on one line; true = 1, false = 0.
	      for(int i = 0; i < numTrajs; i++)
		{
		  outfile<<(excessiveSpeeds[i] ? 1 : 0)<<" ";
		}
	      
	      outfile<<endl;

	      //Print out the values of the badTrajs array in the same fashion.
	      for(int i = 0; i < numTrajs; i++)
		{
		  outfile<<(badTrajs[i] ? 1 : 0)<<" ";
		}
	      
	      outfile<<endl;
	    }

	  //Finally, print out the number of the traj that we're currently following, followed by TWO carriage returns (to create a blank line to signal the end of a selection cycle.
	  outfile<<trajWeAreFollowing<<endl<<endl;
	}      

      usleep(DELAY_TIME*(double) 1E6);
    } 
}

trajChecker::~trajChecker()
{

}
