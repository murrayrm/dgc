#include "trajSelector.h"
#include <iostream>
#include <math.h>
//#include "DGCutils"
#include "TrajTalker.h"
#include "MapAccess.h"
#include <fstream>
#include "GlobalConstants.h"
#include "MapConstants.h"
#include "CTimberClient.hh"
#include "AliceConstants.h" //Used for DFE stuff

using namespace std;

bool firstSelection = true;  //Keeps track of whether or not this is the first selection cycle.

int deltasize;

int previousTrajNum;  //Number of the planner whose traj we were previously following; used for logging.


trajSelector::trajSelector(int num, int sn_key)
  :CSkynetContainer(SNtrajSelector, sn_key), CStateClient(true), CTimberClient(timber_types::trajSelector), CSuperConClient("SLT")
{
  cout<<"Entering constructor."<<endl;

  cout<<"Constructing trajSelector with Skynet key = "<<sn_key<<endl;

  cout<<"Beginning initialization."<<endl;
  //Initialize the number of planners the vehicle has, and the bonus to give the currently followed traj.
  numTrajs = num;

  //Initialize the arrays to hold the trajs, the traj updates, the average speed of each traj, and the boolean array to determine which planners to listern for.

  trajs = new CTraj[numTrajs];
  trajUpdates = new CTraj[numTrajs];

  avgSpeeds = new double[numTrajs];

  plannerWeights = new double[numTrajs];

  listenForTrajs = new bool[numTrajs];

  minCellSpeed = new double [numTrajs];

  excessiveSpeeds = new bool[numTrajs];

  badTrajs = new bool[numTrajs];

  trajTimes = new unsigned long long[numTrajs];

  trajElapsedTimes = new double[numTrajs];

  times = new pthread_mutex_t[numTrajs];

  trajSocket = m_skynet.get_send_sock(SNselectorTraj);

  cout<<"Reading in config file."<<endl;
  //Read in the config file to determine which planners to listen for.
  char buffer[30];
  ifstream infile("trajSelector.config");
  
  infile >> PERCENT_BONUS;

  int active;
  double bonus;


  //Parse the lines in the file to determine which planners to listen for and what priority bonus to give them.

  for(int i = 0; i < numTrajs; i++)
    {
      infile >> active;;
      if(active == 1)
	{
	  listenForTrajs[i] = true;
	}
      else
	{
	  listenForTrajs[i] = false;
	}

      infile >> bonus;
      plannerWeights[i] = bonus;

      infile.getline(buffer, 30);  //Read and junk the rest of the line; we don't need it.

    }
  
  cout<<"Creating mutexes."<<endl;
  //Need to create mutexes and blank trajs for planners that we're listening for.


  if(listenForTrajs[0])
    {
      cout<<"Creating deliberativeTraj mutex"<<endl;
      DGCcreateMutex(&deliberativeTraj);
    }

  if(listenForTrajs[1])
    {
      cout<<"Creating reactiveTraj mutex"<<endl;
      DGCcreateMutex(&reactiveTraj);
    }

  if(listenForTrajs[2])
    {
      cout<<"Creating blankTraj Mutex"<<endl;
      DGCcreateMutex(&blankTraj);
    }

  if(listenForTrajs[3])
    {
      cout<<"Creating roadTraj mutex"<<endl;
      DGCcreateMutex(&roadTraj);

    }

  if(listenForTrajs[4])
    {
      cout<<"Creating rddfTraj Mutex."<<endl;
      DGCcreateMutex(&rddfTraj);
    }

  //Create mutexes to keep track of accss to the array of times.
  for(int i = 0; i < numTrajs; i++)
    {
      if(listenForTrajs[i])
	{
	  DGCcreateMutex(&times[i]);
	}
    }

  //Create mutex to control access to previous traj info
  DGCcreateMutex(&previousTrajMutex);

  //Set up the map stuff
  DGCcreateMutex(&mapMutex);
  
  mapFullRequestSocket = m_skynet.get_send_sock(SNfullmaprequest);
  mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
  mapDelta = new char[MAX_DELTA_SIZE];



  //Initialize the map
  m_map.initMap(CONFIG_FILE_DEFAULT_MAP);

  //Insert the speed layer
  speedLayer = m_map.addLayer<double>(CONFIG_FILE_COST);

  //Get the vehicle's current location.
  cout<<"Updating state."<<endl;
  CStateClient::UpdateState();

  vehicleUTMNorthing = m_state.Northing_rear();
  vehicleUTMEasting = m_state.Easting_rear();

  m_map.updateVehicleLoc(vehicleUTMNorthing, vehicleUTMEasting);

  cout<<"Initializing map."<<endl;
 
  bool bRequestMap = true;
  m_skynet.send_msg(mapFullRequestSocket, &bRequestMap, sizeof(bool), 0);  //Request and then receive initial map info.
  mapDeltaSocket = m_skynet.listen(SNfusiondeltamap, SNfusionmapper);
  if(mapDeltaSocket < 0)
    {
      cerr << "trajSelector constructor returned  skynet listen error." << endl;
    }
  
  //Receive map info. 
  RecvMapdelta(mapDeltaSocket, mapDelta, &deltasize);


  cout<<"Received initial map info"<<endl;
  //Apply initial map info.
  m_map.applyDelta<double>(speedLayer, mapDelta, deltasize);


  //Initialize condition stuff.

  newData_bool = false;
  DGCcreateMutex(&newData_mutex);
  DGCcreateCondition(&newData_condition);

  //Create arrays and mutexes to keep track of which set of data has been updated recently.
  hasNewData = new bool[numTrajs + 1]; //One bool for each of the planners, plus one for the map.
  hasNewDataMutexes = new pthread_mutex_t[numTrajs + 1];

  for(int i = 0; i < numTrajs; i++)  //Initialize the bools/mutexes for the trajs.
    {
      DGCcreateMutex(&hasNewDataMutexes[i]);
      hasNewData[i] = false;
    }

  //Need one for the map.
  DGCcreateMutex(&hasNewDataMutexes[numTrajs]);
  hasNewData[numTrajs] = false;

  //Wait here to receive the first set of trajs.

  cout<<"Waiting for first set of trajs to get here."<<endl;

  if(listenForTrajs[0])
    {
      cout<<"Waiting for deliberative traj."<<endl;
      deliberativeSocket = m_skynet.listen(SNtraj, ALLMODULES);  //Establish a skynet connection to listen for deliberative trajs.

      RecvTraj(deliberativeSocket, &trajUpdates[0]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[0]);

      cout<<"Received deliberative traj."<<endl;
    }

 if(listenForTrajs[1])
    {
      cout<<"Waiting for reactive traj."<<endl;
      reactiveSocket = m_skynet.listen(SNreactiveTraj, ALLMODULES);  //Establish a skynet connection to listen for reactive planner trajs.

      RecvTraj(reactiveSocket, &trajUpdates[1]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[1]);

      cout<<"Received reactive traj."<<endl;
    }


 if(listenForTrajs[2])
    {
      cout<<"Waiting for blank traj."<<endl;
      #warning:"Need a new skynet type for blank planner trajs here."
      blankSocket = m_skynet.listen(SNplannertraj, ALLMODULES);  //Establish a skynet connection to listen for blank-planner trajs.

     RecvTraj(blankSocket, &trajUpdates[2]);  //Receive the traj Update the buffer.

     DGCgettime(trajTimes[2]);

      cout<<"Received reactive traj."<<endl;
    }

 if(listenForTrajs[3])
    {
      cout<<"Waiting for road following traj."<<endl;
      roadSocket = m_skynet.listen(SNroadtraj, ALLMODULES);  //Establish a skynet connection to listen for road-finding trajs.
      RecvTraj(roadSocket, &trajUpdates[3]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[3]);

      cout<<"Received road following traj."<<endl;

    }

 if(listenForTrajs[4])
    {
      cout<<"Waiting for rddf traj."<<endl;
      rddfSocket = m_skynet.listen(SNRDDFtraj, ALLMODULES);  //Establish a skynet connection to listen for RDDFPathGen trajs.

      RecvTraj(rddfSocket, &trajUpdates[4]);  //Receive the traj Update the buffer.

      DGCgettime(trajTimes[4]);

      cout<<"Received rddf traj."<<endl;
    }


 //Initialize the traj we are following.  Note that the lowest-numbered active traj in the array trajs has priority by default.
  for(int i = numTrajs - 1; i >= 0; i--)
    {
      if(listenForTrajs[i])
	{
	  trajWeAreFollowing = i;
	}
    }

  cout<<"Constructor finished."<<endl;

}


void trajSelector::select()
{

  cout<<"Starting main program loop."<<endl;

  //Start up the various function threads.

  cout<<"Starting map listener thread."<<endl;
  DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::mapListener);

  if(listenForTrajs[0])
    {
      cout<<"Starting deliberative listener thread."<<endl;
      DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::deliberativeListener);
    }

  if(listenForTrajs[1])
    {
      cout<<"Starting reactive listener thread."<<endl;
      DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::reactiveListener);
  
    }

  if(listenForTrajs[2])
    {
      cout<<"Starting blank listener thread."<<endl;
      DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::blankListener);
    }

  if(listenForTrajs[3])
    {
      cout<<"Starting road following listener thread."<<endl;
      DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::roadListener);
    }

  if(listenForTrajs[4])
    {
      cout<<"Starting rddfPathGen listener thread."<<endl;
      DGCstartMemberFunctionThread<trajSelector>(this, &trajSelector::rddfListener);
    }


  //Logging shiznit

  string log_directory;
 
  string LOG_FILE_name;
  string HEADER_FILE_name;

  bool loggingEnabled;
  bool newSession; //Records whether or not this is the first time we're entering data into a log file.

  ofstream outfile;
  ofstream header_file;

  int logging_level;


  while(true)
    {

      previousTrajNum = trajWeAreFollowing;
      cout<<"Entering method select()"<<endl;
      //Update the list of trajs to the most current available set of trajs.
      
      cout<<"Updating trajs."<<endl;
      
      if(listenForTrajs[0])
	{
	  //Get the deliberative planner's traj.
	  DGClockMutex(&deliberativeTraj);
	  trajs[0] = trajUpdates[0];
	  DGCunlockMutex(&deliberativeTraj);
	}
      
      if(listenForTrajs[1])
	{
	  //Get the reactive planner's traj
	  DGClockMutex(&reactiveTraj);
	  trajs[1] = trajUpdates[1];
	  DGCunlockMutex(&reactiveTraj);
	}
      
      if(listenForTrajs[2])
	{
	  //Get the blank planner's traj.
	  DGClockMutex(&blankTraj);
	  trajs[2] = trajUpdates[2];
	  DGCunlockMutex(&blankTraj);
	}
      
      if(listenForTrajs[3])
	{
	  //Get the roadfinding traj.
	  DGClockMutex(&roadTraj);
	  trajs[3] = trajUpdates[3];
	  DGCunlockMutex(&roadTraj);
	}
      
      if(listenForTrajs[4])
	{
	  //Get the rddfPathGen traj.
	  DGClockMutex(&rddfTraj);
	  trajs[4] = trajUpdates[4];
	  DGCunlockMutex(&rddfTraj);
	}

      if(firstSelection)
	{
	  previousTraj = trajs[trajWeAreFollowing];
	  firstSelection = false;
	}

      //Check to see if loggin is enabled with each loop execution.
      loggingEnabled = getLoggingEnabled();

      for(int i = 0; i < numTrajs; i++)
	{
	  if(listenForTrajs[i] && (i != previousTrajNum)) //We need to modify this traj before we can use it to ensure spacial and temporal continuity during switching.
	    {
	      modifyTraj(trajs[i]);
	    }
	}


      if(loggingEnabled)
	{
	  logging_level = getMyLoggingLevel();
	  //If logging is enabled and this is a new session, we need to output a new header file.
	  newSession = checkNewDirAndReset();
	  if(newSession)
	    {
	      log_directory = getLogDir();
	      HEADER_FILE_name = log_directory + HEADER_FILE;
	      LOG_FILE_name = log_directory + LOG_FILE;

	      header_file<<PERCENT_BONUS;

	      cout<<"Creating new header file: "<<HEADER_FILE_name<<endl;
	      header_file.open(HEADER_FILE_name.c_str(), ios::out | ios::trunc);
	      for(int i = 0; i < numTrajs;i ++)
		{
		  header_file<<(listenForTrajs[i] ? 1 : 0)<<" "<<plannerWeights[i]<<endl;
		}



	      header_file.close();

	      if(outfile.is_open())
		{
		  outfile.close();
		}

	      outfile.open(LOG_FILE_name.c_str(), ios::out | ios::trunc);
	    }
	}
      
      
      cout<<"Beginning selection."<<endl;
      
      //Reset the array to keep track of excessive speeds.
      for(int i = 0; i < numTrajs; i++)
	{
	  excessiveSpeeds[i] = false;
	}

      //Reset the array to keep track of bad trajs.

      for(int i = 0; i < numTrajs; i++)
	{
	  badTrajs[i] = false;
	}


            
      //Determines the traversal time for each of the various trajs.
      double time, speed, distance, sectionDistance, trajAvgSpeed;
      
      double* northing; 
      double* easting;
      double* n1;
      double* e1;
      double mapSpeed;
      double yaw;
      double junk1, junk2, junk3;  //Needed for accessing Dima's function, as it takes several arguments that it uses to return data that we don't care to know about.

      bool currentTrajHasNewData, mapHasNewData;

      unsigned long long timeDifference;  //Keeps track of current/elapsed time
      unsigned long long currentTime;


            
      cout<<"Calculating average speeds for all active trajs."<<endl;
      
      //Calculate average speeds for all trajs
      for(int currentTraj = 0; currentTraj <= numTrajs - 1; currentTraj++)
	{

	  //cout<<"Starting at top of for loop with currentTraj = "<<currentTraj<<endl;

	  DGClockMutex(&hasNewDataMutexes[currentTraj]);  //Need to protect this data
	  currentTrajHasNewData = hasNewData[currentTraj];
	  DGCunlockMutex(&hasNewDataMutexes[currentTraj]);

	  DGClockMutex(&hasNewDataMutexes[numTrajs]);
	  mapHasNewData = hasNewData[numTrajs];
	  DGCunlockMutex(&hasNewDataMutexes[numTrajs]);
	  

	  //cout<<"Finished locking mutexes at start of for loop."<<endl;
	  if(listenForTrajs[currentTraj] && (hasNewData[currentTraj] || hasNewData[numTrajs])) //If we're listening for the current traj and the traj has new data or the map has new data, we need to calculate some stuff.
	    {
	      DGCgettime(currentTime);  //Record the current time.
	      DGClockMutex(&times[currentTraj]);
	     
	      timeDifference = currentTime -  trajTimes[currentTraj];

	      trajElapsedTimes[currentTraj] = DGCtimetosec(timeDifference);  //Calculate the elapsed time from when this traj was received until right now.
	      DGCunlockMutex(&times[currentTraj]);

	      cout<<"Elapsed time for planner "<<currentTraj<<" is "<<trajElapsedTimes[currentTraj]<<" seconds."<<endl;

	      //Check to see if the traj is too old.  If it is, we know we're not going to use it, so skip all the processing below and just print an error message.

	      if(trajElapsedTimes[currentTraj] <= TRAJ_TIMEOUT)  //This traj is ok.
		{	      
		  time = 0; //Set the traversal time for the current traj to zero.

		  distance = 0;  //Set the total distance for the current traj to zero.

		  //Determine the number of points in the current traj
		  int numPoints = trajs[currentTraj].getNumPoints(); 
		  
		  //Get the data for the current traj
		  northing = trajs[currentTraj].getNdiffarray(0);
		  easting = trajs[currentTraj].getEdiffarray(0);
		  n1 = trajs[currentTraj].getNdiffarray(1);
		  e1 = trajs[currentTraj].getNdiffarray(1);

		  minCellSpeed[currentTraj] = 100000;
		  
		  for(int point = 0; point <= numPoints - 2; point++)
		    {
		      sectionDistance = hypot((northing[point+1] - northing[point]), (easting[point+1] - easting[point]));; //Compute the straight-line distance from the curent point to the next point on the traj
		      speed = hypot(n1[point], e1[point]); //Compute the speed at which the vehicle will be traversing this section
		      time += sectionDistance / speed;  //Increment the total time appropriately
		      distance += sectionDistance; //Increment the total distance appropriately.
		      
#warning:"May also want to include something about lookahead distance of the object we're looking at as well (i.e., only consider trajs to have excessive speeds for nearby obstacles, as if an just popped up on our sensors, the planners probably just haven't had time to take account of it yet, whereas if it's close to us, that could indicate planner failure.  May also want to take into consideration the speed at which we're currently traveling when taking into consideration the lookahead distance."
		      //Use Dima's function to check if we're going too fast for the map value at this point.
		      
		      
		      yaw = atan2(e1[point], n1[point]);  // = tangent of yaw.

		      DGClockMutex(&mapMutex);
		      getContinuousMapValueDiffGrown(&m_map, speedLayer, northing[point], easting[point], yaw, &mapSpeed, &junk1, &junk2, &junk3);
		      DGCunlockMutex(&mapMutex);

		      if(mapSpeed < minCellSpeed[currentTraj])
			{
			  minCellSpeed[currentTraj] = mapSpeed;  //Keep track of the min speed that the vehicle's traj passes through in the map for SuperCon
			}

		      if(speed > mapSpeed*(1+SPEED_ERROR_MARGIN))  //This traj has excessive speed.
			{
			  excessiveSpeeds[currentTraj] = true;
			}
		      
		    }
		  
		  if(excessiveSpeeds[currentTraj])
		    {
		      cout<<"Planner "<<currentTraj<<" has generated a plan with excessive speed."<<endl;
		    }
	      
		  trajAvgSpeed = distance / time;  //Find the average traj speed by dividing total distance by total time.
		  
		  avgSpeeds[currentTraj] = trajAvgSpeed; //Store the time value for this traj
		  cout<<"Calculated average speed for traj "<<currentTraj<<" is "<<avgSpeeds[currentTraj]<<" m/s"<<endl;
		}

	      else  //This traj has not been updated in too long.  We can assume that the planner that generated this traj has crapped out, so we should ignore its input.
		{
		  badTrajs[currentTraj] = true;
		  cout<<"Planner "<<currentTraj<<" has timed out.  Ignoring input."<<endl;
		}
	    }

	  //cout<<"Locking mutex hasNewDataMutexes "<<currentTraj<<" at bottom of for loop."<<endl;
	  DGClockMutex(&hasNewDataMutexes[currentTraj]);  //Now that we've looked at this data, it's no longer new.
	  hasNewData[currentTraj] = false;
	  //cout<<"Unlocking mutex hasNewDataMutexes "<<currentTraj<<" at bottom of for loop."<<endl;
	  DGCunlockMutex(&hasNewDataMutexes[currentTraj]);
	  //cout<<"Control is at bottom of for loop for currentTraj = "<<currentTraj<<endl;
	}


      
      //Then determine if all of the planners have failed to generate a safe path.
      
      bool allExcessiveSpeeds = true;
      for(int i = 0; i < numTrajs; i++)
	{
	  if(!excessiveSpeeds[i])
	    {
	      allExcessiveSpeeds = false;
	    }
	}
      
      
      if(!allExcessiveSpeeds)  //At least one planner has generated a plan that looks ok.
	{
	  //Give the average speed of the trajectory that we're currently following a "bonus" or "weight" as a form of hysteresis to prevent chaotic traj switching.

	  //Find the max average speed of all the trajs.
	  double maxSpeed = 0;
	  
	  for(int currentTraj = 0; currentTraj <= numTrajs - 1; currentTraj++)   //For each time in the array of traversal times
	    {
	      if(listenForTrajs[currentTraj])
		{

		  if(currentTraj == trajWeAreFollowing)  //This traj should get a speed bonus of PERCENT_BONUS in the calculation
		    {

		      if((avgSpeeds[currentTraj] * (1 + PERCENT_BONUS)* (1 + plannerWeights[currentTraj]) > maxSpeed)  && !excessiveSpeeds[currentTraj] && !badTrajs[currentTraj]) 
			{
			  maxSpeed = avgSpeeds[currentTraj]*(1 + PERCENT_BONUS);  //If the max speed found up until this point is less than the speed we're looking at right now, set the current speed to be the new max.
			  trajWeAreFollowing = currentTraj;  //Set the traj associated with this time to be the traj that we're currently following.
			}
		    }


		  else  //We're not currently following this traj, so it shouldn't get a speed bonus.
		    {
		      if((avgSpeeds[currentTraj]* (1 + plannerWeights[currentTraj]) > maxSpeed) && !excessiveSpeeds[currentTraj] && !badTrajs[currentTraj])
			{
			maxSpeed = avgSpeeds[currentTraj];
			trajWeAreFollowing = currentTraj;
			}
		    }
			 
		}
	    }      
	}
      
      else   //If we EVER find ourselves here, it means that we're in SERIOUS TROUBLE.  Oh well.  DAMN THE TORPEDOS!!!  Actually, if all of the planners have exessive speeds, we really should pick the slowest plan
	{
	  //Find the min average speed of all the trajs.
	  double minSpeed = avgSpeeds[0];
	  
	  for(int currentTraj = 1; currentTraj <= numTrajs - 1; currentTraj++)   //For each time in the array of traversal times
	    {
	      if(listenForTrajs[currentTraj])
		{
		  if( (avgSpeeds[currentTraj] < minSpeed) && !badTrajs[currentTraj])
		    {
		      minSpeed = avgSpeeds[currentTraj];  //If the min speed found up until this point is greater than the speed we're looking at right now, set the current speed to be the new minimum.
		      trajWeAreFollowing = currentTraj;  //Set the traj associated with this time to be the traj that we're currently following.
		    }
		}
	    }
	}
      
      cout<<"Finished selection.  Sending traj "<<trajWeAreFollowing<<" with average speed "<<avgSpeeds[trajWeAreFollowing]<<" m/s"<<endl;
      //Publish the traj that we want to follow (stored in the Ctraj array in slot trajs[trajWeAreFollowing])
      SendTraj(trajSocket, &trajs[trajWeAreFollowing]);

      scMessage( int( min_speed_in_current_traj ), minCellSpeed[trajWeAreFollowing]);

      DGClockMutex(&previousTrajMutex);
      previousTraj = trajs[trajWeAreFollowing];
      DGCunlockMutex(&previousTrajMutex);
      
      cout<<"Method select() finished."<<endl<<endl;

      //Now we need to write all this awesome data that we've just received/calculated to our super-cool log files.

      if(loggingEnabled && (logging_level != 0))
	{

	  //Print out the number of the planner whose traj we were previously following

	  outfile<<previousTrajNum<<endl;

	  for(int i = 0; i < numTrajs; i++)
	    {
	      if(listenForTrajs[i])
		{
		  //Print out a planner id number, avg speed, and elapsed time since reception on one line
		  outfile<<i<<" "<<avgSpeeds[i]<<" "<<trajElapsedTimes[i]<<endl;
		  //Print out the actual traj itself that this corresponds to ONLY IF logging_level == 3, as this leads to GIGANTIC log files.
		  if(logging_level ==3)
		    {
		      outfile<<trajs[i].getNumPoints()<<endl;
		      trajs[i].print(outfile);
		      outfile<<endl;
		    }
		}
	    }

	  //
	  if(logging_level >= 2)
	    {
	      //Now print out the values stored in the excessiveSpeeds array on one line; true = 1, false = 0.
	      for(int i = 0; i < numTrajs; i++)
		{
		  outfile<<(excessiveSpeeds[i] ? 1 : 0)<<" ";
		}
	      
	      outfile<<endl;

	      //Print out the values of the badTrajs array in the same fashion.
	      for(int i = 0; i < numTrajs; i++)
		{
		  outfile<<(badTrajs[i] ? 1 : 0)<<" ";
		}
	      
	      outfile<<endl;
	    }

	  //Finally, print out the number of the traj that we're currently following, followed by TWO carriage returns (to create a blank line to signal the end of a selection cycle.
	  outfile<<trajWeAreFollowing<<endl<<endl;
	}

	
      //If this selection was triggered by new map info, also need to reset that variable, too.
      //cout<<"Setting new map data = false;"<<endl;
      DGClockMutex(&hasNewDataMutexes[numTrajs]);
      hasNewData[numTrajs] = false;
      DGCunlockMutex(&hasNewDataMutexes[numTrajs]);      

      //Wait until we need to do something again.
      //cout<<"Setting new data bool = false"<<endl;
      DGClockMutex(&newData_mutex);
      newData_bool = false;
      DGCunlockMutex(&newData_mutex);
      DGCWaitForConditionTrue(newData_bool, newData_condition, newData_mutex);

      cout<<"Control has reached the bottom of the while loop."<<endl;

    }
}


void trajSelector::deliberativeListener()
{

  //  cout<<"Entering method deliberativeListener()."<<endl;

  while(true)
    {
      WaitForTrajData(deliberativeSocket);  //Wait for data to become available.
      DGClockMutex(&deliberativeTraj);
      RecvTraj(deliberativeSocket, &trajUpdates[0]);  //Receive the traj Update the buffer.
      DGCunlockMutex(&deliberativeTraj);      //Lock the mutex associated with the buffer for this traj.

      DGClockMutex(&times[0]);
      DGCgettime(trajTimes[0]);
      DGCunlockMutex(&times[0]);

      //Flag this traj as new.
      //cout<<"Setting new traj data = true for deliberative planner."<<endl;
      DGClockMutex(&hasNewDataMutexes[0]);
      hasNewData[0] = true;
      DGCunlockMutex(&hasNewDataMutexes[0]);

      DGCSetConditionTrue(newData_bool, newData_condition, newData_mutex);

      //cout<<"Received deliberative traj."<<endl;
    }
  //cout<<"Exiting method deliberativeListener()."<<endl;
}



void trajSelector::reactiveListener()
{

  //cout<<"Entering method reactiveListener()."<<endl;

  while(true)
    {
      WaitForTrajData(reactiveSocket);  //Wait for data to become available.
      DGClockMutex(&reactiveTraj);
      RecvTraj(reactiveSocket, &trajUpdates[1]);  //Recv the traj.
      DGCunlockMutex(&reactiveTraj);      //Lock the mutex associated with the buffer for this traj.

      DGClockMutex(&times[1]);
      DGCgettime(trajTimes[1]);
      DGCunlockMutex(&times[1]);

      //Flag this traj as new.
      //cout<<"Setting new traj data = true for reactive planner."<<endl;
      DGClockMutex(&hasNewDataMutexes[1]);
      hasNewData[1] = true;
      DGCunlockMutex(&hasNewDataMutexes[1]);

      DGCSetConditionTrue(newData_bool, newData_condition, newData_mutex);

      //cout<<"Received reactive traj."<<endl;


    }

  //cout<<"Exiting method reactiveListener()"<<endl;
}

void trajSelector::blankListener()
{
  //cout<<"Entering method blankListener()"<<endl;

  while(true)
    {
      WaitForTrajData(blankSocket);  //Wait for data to become available.
      DGClockMutex(&blankTraj);
      RecvTraj(blankSocket, &trajUpdates[2]);  //Receive the traj.
      DGCunlockMutex(&blankTraj);      //Lock the mutex associated with the buffer for this traj.

      DGClockMutex(&times[2]);
      DGCgettime(trajTimes[2]);
      DGCunlockMutex(&times[2]);

      //Flag this traj as new.
      //cout<<"Setting new traj data = true for blank planner."<<endl;
      DGClockMutex(&hasNewDataMutexes[2]);
      hasNewData[2] = true;
      DGCunlockMutex(&hasNewDataMutexes[2]);
      
      DGCSetConditionTrue(newData_bool, newData_condition, newData_mutex);

      //cout<<"Received blank traj."<<endl;     
    }

  //cout<<"Exiting method blankListener()"<<endl;
}


void trajSelector::roadListener()
{
  //cout<<"Entering method roadListener()"<<endl;

  while(true)
    {
      WaitForTrajData(roadSocket);  //Wait for data to become available.
      DGClockMutex(&roadTraj);
      RecvTraj(roadSocket, &trajUpdates[3]);  //Receive  the traj.
      DGCunlockMutex(&roadTraj);      //Lock the mutex associated with the buffer for this traj.

      DGClockMutex(&times[3]);
      DGCgettime(trajTimes[3]);
      DGCunlockMutex(&times[3]);

      //Flag this traj as new.
      //cout<<"Setting new traj data = true for road planner."<<endl;
      DGClockMutex(&hasNewDataMutexes[3]);
      hasNewData[3] = true;
      DGCunlockMutex(&hasNewDataMutexes[3]);

      DGCSetConditionTrue(newData_bool, newData_condition, newData_mutex);

      //cout<<"Received road traj."<<endl;


    }

  //cout<<"Exiting method roadListener()"<<endl;
}

void trajSelector::rddfListener()
{

  //cout<<"Entering method rddfListener()"<<endl;

  while(true)
    {
      WaitForTrajData(rddfSocket);  //Wait for data to become available.
      DGClockMutex(&rddfTraj);
      RecvTraj(rddfSocket, &trajUpdates[4]);  //Receive the traj.
      DGCunlockMutex(&rddfTraj);      //Lock the mutex associated with the buffer for this traj.

      DGClockMutex(&times[4]);
      DGCgettime(trajTimes[4]);
      DGCunlockMutex(&times[4]);

      //Flag this traj as new.
      //cout<<"Setting new traj data = true for rddf."<<endl;
      DGClockMutex(&hasNewDataMutexes[4]);
      hasNewData[4] = true;
      DGCunlockMutex(&hasNewDataMutexes[4]);

      DGCSetConditionTrue(newData_bool, newData_condition, newData_mutex);

      //cout<<"Received rddf traj."<<endl;


    }

  //cout<<"Exiting method rddfListener()"<<endl;
}



void trajSelector::mapListener()
{
  //cout<<"Entering method mapListener()"<<endl;
  while(true)
    {
      CStateClient::UpdateState();  //Get vehicle state update.
      vehicleUTMNorthing = m_state.Northing_rear();
      vehicleUTMEasting = m_state.Easting_rear();
      //cout<<"Updated vehicle state."<<endl;

      DGClockMutex(&mapMutex);
      //Center the map on the vehicle's new location.
      m_map.updateVehicleLoc(vehicleUTMNorthing, vehicleUTMEasting);
      DGCunlockMutex(&mapMutex);


      //Get map deltas.
      RecvMapdelta(mapDeltaSocket, mapDelta, &deltasize);

      //Apply the map deltas.
      DGClockMutex(&mapMutex);
      m_map.applyDelta<double>(speedLayer, mapDelta, deltasize);
      //cout<<"Updated map."<<endl;
      DGCunlockMutex(&mapMutex);

      //Flag this map as new.
      //cout<<"Setting new  data = true for the map."<<endl;
      DGClockMutex(&hasNewDataMutexes[numTrajs]);
      hasNewData[numTrajs] = true;
      DGCunlockMutex(&hasNewDataMutexes[numTrajs]);

      DGCSetConditionTrue(newData_bool, newData_condition, newData_mutex);

    }
  //cout<<"Exiting method mapListener()"<<endl;
}


#warning:"This code may be on acid, as I wasn't thinking clearly when I wrote it."
void trajSelector::modifyTraj(CTraj& trajToModify)
{
  DGClockMutex(&previousTrajMutex);

  int branchPoint = previousTraj.getPointAhead(0, LOOKAHEAD_DIST);  //Get the index of the first point on the traj that is at least LOOKAHEAD_DIST meters from the beginning.

  double* pNarray = previousTraj.getNdiffarray(0);
  double* pEarray = previousTraj.getEdiffarray(0);
  double Northing = pNarray[branchPoint];
  double Easting = pEarray[branchPoint];
  double NDiff = (previousTraj.getNdiffarray(1))[branchPoint];
  double EDiff = (previousTraj.getEdiffarray(1))[branchPoint];

  DGCunlockMutex(&previousTrajMutex);

  int length = trajToModify.getNumPoints();
  double* Narray = trajToModify.getNdiffarray(0);
  double* Earray = trajToModify.getEdiffarray(0);

  double x,y;
  double angle;
  int joinPoint = 0;  //Index of spot to connect previous traj to on the traj that we are modifying.

  //Find the first point on the traj we're receiving that falls within a cone of angle CONE ANGLE.  If no such point exists, choose the last point on the end of the traj.

  do
    {
      x = Earray[joinPoint] - Easting;
      y = Narray[joinPoint] - Northing;
      angle = acos((x*EDiff + y*NDiff) / ( hypot(x,y) * hypot(EDiff, NDiff)) );
      joinPoint++;
    } while((joinPoint < length) && (angle > (CONE_ANGLE/2)));

  joinPoint--;

  //Create the new (joining) traj here.

  CTraj modifiedTraj(3);
  modifiedTraj.fillWithCubicSpline(pNarray[branchPoint], previousTraj.getNorthingDiff(branchPoint, 1), pEarray[branchPoint], previousTraj.getEastingDiff(branchPoint, 1), Narray[joinPoint], trajToModify.getNorthingDiff(joinPoint, 1), Earray[joinPoint], trajToModify.getEastingDiff(joinPoint, 1), .2);

  int joinTrajLength = modifiedTraj.getNumPoints();
  
  //Assign a speed profile to the joining section.
  assignSpeedProfile(modifiedTraj);
  
  //Check for assigning duplicate points in trajs, and fill in the section from the previous traj that we want to follow.
  if(hypot(Northing - modifiedTraj.getNorthing(0), Easting - modifiedTraj.getEasting(0)) == 0)
    {
      //The end of the section of the previous traj that we're following and the beginning of the intermediate traj are the same point, so don't include the last point on the prevoius traj.
      modifiedTraj.prepend(&previousTraj, 0, branchPoint - 1);
    }
  else
    {
      //The last point of the section of the previous traj that we're following is ok.
      modifiedTraj.prepend(&previousTraj, 0, branchPoint);
    }
  

  //Fill in the section from the traj we're switching to that we want to follow.
  if(hypot(trajToModify.getNorthing(joinPoint) - modifiedTraj.getNorthing(joinTrajLength - 1), trajToModify.getEasting(joinPoint) - modifiedTraj.getEasting(joinTrajLength - 1)) == 0)
    {
      //The beginning of the section that we're following on the traj we're connecting to and the end of the intermediate traj are the same point, so don't include the first point on the section of the traj that we're connecting to.
      modifiedTraj.append(&trajToModify, joinPoint + 1, length - 1);
    }
  else
    {
      //The last point of the previous traj is ok.
      modifiedTraj.append(&trajToModify, joinPoint, length - 1);
    }

  modifiedTraj.feasiblize(VEHICLE_MAX_LATERAL_ACCEL , VEHICLE_MAX_ACCEL, VEHICLE_MAX_DECEL, 1);

  trajToModify = modifiedTraj;

}

void trajSelector::assignSpeedProfile(CTraj& trajToModify)
{
  //We should already have spacial here, so use the spacial profile and map to fill in speeds and accelerations.

  int numPoints = trajToModify.getNumPoints();

  CTraj newTraj; 



  if(numPoints >=3)  //Traj must be at least 3 points long for this to work correctly; we use numerical techniques that require the loss of a data point for each order of derivative that is calculated.
    {
      double* Narray = trajToModify.getNdiffarray(0);
      double* Earray = trajToModify.getEdiffarray(0);
      
      
      double N1array [numPoints-1];
      double N2array [numPoints-2];
      double E1array [numPoints-1];
      double E2array [numPoints-2];
      
      double junk1, junk2, junk3;  //Unneeded placeholder variables that need to exist to match Dima's mapAccess signature.
      
      double yaw, mapSpeed;
      
      double time;
      double ndiff [numPoints]; //Keep track of the distances between successive points in the traj.
      double ediff [numPoints];
      
      for(int i = 0; i <= numPoints - 2; i++)
	{
	  
	  //Assign speeds here.
	  
	  ediff[i] = Earray[i+1] - Earray[i];
	  ndiff[i] = Narray[i+1] - Narray[i];
	  
	  yaw = atan2(ediff[i], ndiff[i]);  // = tangent of yaw.
	  
	  DGClockMutex(&mapMutex);
	  getContinuousMapValueDiffGrown(&m_map, speedLayer, Narray[i], Earray[i], yaw, &mapSpeed, &junk1, &junk2, &junk3);
	  DGCunlockMutex(&mapMutex);
	  
	  
	  //Assign speeds
	  N1array[i] = mapSpeed*cos(yaw)*(1 - SPEED_MARGIN); //Set each speed in the array 
	  E1array[i] = mapSpeed*sin(yaw)*(1 - SPEED_MARGIN); 
	}
      
      
      //Now we need to fill in accelerations.
      
      for(int i = 0; i <= numPoints - 3; i++)
	{
	  time = hypot(ediff[i], ndiff[i])/hypot(N1array[i], E1array[i]);  //Time = dist/speed
	  
	  N2array[i] = (N1array[i+1] - N1array[i])/time;  //Acceleration = (delta speed)/time
	  E2array[i] = (E1array[i+1] - E1array[i])/time;
	}
    

      //Now create a new traj using the above arrays.
      double Nvector[3];
      double Evector[3];
     
      newTraj.setNumPoints(numPoints - 2);
      newTraj.startDataInput();
      
      for(int i = 0; i < numPoints - 3; i++)
	{
	  Nvector[0] = Narray[i];
	  Nvector[1] = N1array[i];
	  Nvector[2] = N2array[i];
	  
	  Evector[0] = Earray[i];
	  Evector[1] = E1array[i];
	  Evector[2] = E2array[i];
	  
	  newTraj.inputWithDiffs(Nvector, Evector);
	}
    }
  
  else if(numPoints == 2)
    {
      newTraj.setNumPoints(2);
      newTraj.startDataInput();
      double ndiff, ediff;

      double junk1, junk2, junk3;  //Unneeded placeholder variables that need to exist to match Dima's mapAccess signature.
      
      double yaw, speed_1, speed_2, acceleration;

      ndiff = trajToModify.getNorthing(1) - trajToModify.getNorthing(0);
      ediff = trajToModify.getEasting(1) - trajToModify.getEasting(0);

      yaw = atan2(ediff, ndiff);

      DGClockMutex(&mapMutex);
      getContinuousMapValueDiffGrown(&m_map, speedLayer, trajToModify.getNorthing(0), trajToModify.getEasting(0), yaw, &speed_1, &junk1, &junk2, &junk3);
      getContinuousMapValueDiffGrown(&m_map, speedLayer, trajToModify.getNorthing(1), trajToModify.getEasting(1), yaw, &speed_2, &junk1, &junk2, &junk3);
      DGCunlockMutex(&mapMutex);

      acceleration = (speed_2 - speed_1) / (hypot(ndiff, ediff) / speed_1 );  //acceleration = del. speed / del. time = del speed. / (del dist. / speed.)

      newTraj.setPoint(0, trajToModify.getNorthing(0), speed_1*cos(yaw), acceleration*cos(yaw), trajToModify.getEasting(0), speed_1*sin(yaw), acceleration*sin(yaw) );

      

      newTraj.setPoint(1, trajToModify.getNorthing(1), speed_2*cos(yaw)*SPEED_MARGIN, 0, trajToModify.getEasting(1), speed_2*sin(yaw)*SPEED_MARGIN, 0 );


    }

  else //numPoints == 1
    {
      newTraj.setNumPoints(1);
      newTraj.startDataInput();
      DGClockMutex(&mapMutex);
      double speed = m_map.getDataUTM<double>(speedLayer, trajToModify.getNorthing(0), trajToModify.getEasting(0));
      DGCunlockMutex(&mapMutex);

      speed = speed*(1/sqrt((double)2))*SPEED_MARGIN;

      newTraj.setPoint(0, trajToModify.getNorthing(0), speed, 0, trajToModify.getEasting(0), speed, 0);
    }

  trajToModify = newTraj;
}



bool trajSelector::isFeasible(CTraj& trajToCheck)
{
  int length = trajToCheck.getNumPoints();

  double* N1array = trajToCheck.getNdiffarray(1);  //Northing speed array
  double* N2array = trajToCheck.getNdiffarray(2);  //Northing acceleration array
  double* E1array = trajToCheck.getEdiffarray(1);  //Easting speed array
  double* E2array = trajToCheck.getEdiffarray(2);  //Easting acceleration array

  //Vectors that store (northing, easting) values.
  double heading_vector[2]; //Points in the direction of the vehicle's current heading.
  double lateral_vector[2]; //Points laterally to the current direction of the vehicle.
  double acceleration_vector[2];  //The instantaneous acceleration of the vehicle at this point.
  double projection[2];  //Vector used as a storage container for projections of other vectors.

  double cosine;  //Cosine of the angle between the heading and acceleration vectors (used to determine if we are accelerating or decelerating.
  double scalar;  //Used to store scalars for scalar multiplication of vectors.
  double mag;  //Used to store magnitudes of vectors.

  bool feasible = true;

  for(int i = 0; i < length; i++)
    {
      //I (heart) vectors :-).

      //Fill in the values for the appropriate vectors by reading them out of the traj.
      heading_vector[0] = N1array[i];
      heading_vector[1] = E1array[i];

      acceleration_vector[0] = N2array[i];
      acceleration_vector[1] = E2array[i];
      
      //Go dot product!
      cosine = (heading_vector[0]*acceleration_vector[0] + heading_vector[1]*acceleration_vector[1]) / ( hypot(heading_vector[0], heading_vector[1]) * hypot(acceleration_vector[0], acceleration_vector[1]) );

      //Find the projection of the acceleration vector along the traj.

      scalar = (heading_vector[0]*acceleration_vector[0] + heading_vector[1]*acceleration_vector[1]) / ( pow( hypot(heading_vector[0], heading_vector[1]) , 2) );

      projection[0] = heading_vector[0]*scalar;
      projection[1] = heading_vector[1]*scalar;
      mag = hypot(projection[0], projection[1]);  //Magnitude of projection of acceleration vector onto heading vector (i.e., magnitude of acceleration/deceleration).

      if(cosine >=0)  //We know that our projection of acceleration onto heading points in the direction of forward progress, so we're accelerating.
	{
	 
	  if(mag > VEHICLE_MAX_ACCEL)  //Traj accelerates too fast.
	    {
	      feasible = false;
	      cout<<"Traj failed DFE test at point "<<i<<"; forward acceleration was too high.  Forward accleration at this point is: "<<mag<<" m/s^2"<<endl;
	    }
	}

      else //We know that our projection of acceleration onto heading points opposite the direction of forward progress, so we're decelerating.
	{
	  if(mag > VEHICLE_MAX_DECEL)  //Traj decelerates too fast.
	    {
	      feasible = false;
	      cout<<"Traj failed DFE test at point "<<i<<"; deceleration was too high.  Deceleration at this point is: "<<mag<<" m/s^2"<<endl;
	    }
	}

      //Now need to calculate a vector that points perpendicular to our heading vector.  Lateral Acceleration = Acceleration - (parallel acceleration)

      lateral_vector[0] = acceleration_vector[0] - projection[0];
      lateral_vector[1] = acceleration_vector[1] - projection[1];
      mag = hypot(lateral_vector[0], lateral_vector[1]);  //Magnitude of projection of acceleration vector onto vector perpendicular to heading vector (i.e., magnitude of lateral acceleration).

      if(mag > VEHICLE_MAX_LATERAL_ACCEL)
	{
	  feasible = false;
	  cout<<"Traj failed DFE test at point "<<i<<"; lateral acceleration was too high.  Lateral acceleration at this point is: "<<mag<<" m/s^2"<<endl;
	}
    }

  return feasible;
}

trajSelector::~trajSelector()
{
  delete [] trajs;
  delete [] avgSpeeds;
  delete [] plannerWeights;
  delete [] trajUpdates;
  delete [] mapDelta;
  delete [] excessiveSpeeds;
  delete [] badTrajs;
  delete [] trajTimes;
  delete [] trajElapsedTimes;
  delete [] minCellSpeed;

  for(int i = 0; i < numTrajs; i++)
    {
      if(listenForTrajs[i])
	{
	  DGCdeleteMutex(&times[i]);
	  DGCdeleteMutex(&hasNewDataMutexes[i]);
	}
    }

  if(listenForTrajs[0])
    {
      DGCdeleteMutex(&deliberativeTraj);
    }

 if(listenForTrajs[1])
    {
      DGCdeleteMutex(&reactiveTraj);
    }

 if(listenForTrajs[2])
    {
      DGCdeleteMutex(&blankTraj);
    }

 if(listenForTrajs[3])
    {
      DGCdeleteMutex(&roadTraj);
    }

 if(listenForTrajs[4])
    {
      DGCdeleteMutex(&rddfTraj);
    }

 DGCdeleteMutex(&mapMutex);

  delete [] listenForTrajs;
  delete [] hasNewData;

  DGCdeleteMutex(&newData_mutex);
  DGCdeleteCondition(&newData_condition);

}
