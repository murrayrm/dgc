#include "trajSelector.h"

class trajChecker: public trajSelector
{

 public:

  trajChecker(int num, int sn_key);
  
  void check();
  
  ~trajChecker();
};
