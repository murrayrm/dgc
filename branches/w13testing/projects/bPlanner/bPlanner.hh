#ifndef BPLANNER_HH
#define BPLANNER_HH

#include "sn_msg.hh"
#include <iostream>
#include <iomanip>
#include <string>
#include "SkynetContainer.h"
#include "DGCutils"
#include "bPlannerConfig.hh"
#include "raid.hh"

using namespace std;


class bPlanner : virtual public CSkynetContainer, virtual public raid
{
public:
  bPlanner(int des_skynet_key);
  ~bPlanner();

};

#endif  // BPLANNER_HH
