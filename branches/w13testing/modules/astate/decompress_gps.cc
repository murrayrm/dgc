#include <stdlib.h>
#include <iostream.h>
#include <stdio.h>
#include <unistd.h>
#include <fstream.h>
#include "gps.hh"

struct raw_GPS
{
      unsigned long long time;
      gpsData data;
};

int main(int argc, char **argv)
{
  if (argc < 3) {
    cout << "Syntax: decompress_gps infile outfile";
    return 0;
  }
  char infile[100];
  char outfile[100];
  ifstream input_stream;
  ofstream output_stream;
  unsigned int nav_mode;

  raw_GPS gps_in;
  gpsDataWrapper my_gpsdata;

  unsigned long long gpstime;

  sprintf(infile, "%s", argv[1]);
  sprintf(outfile, "%s", argv[2]);

  input_stream.open(infile);
  output_stream.open(outfile);

  output_stream.precision(40);

  while (input_stream) {

    input_stream.read((char*)&gps_in, sizeof(raw_GPS));

    memcpy(&my_gpsdata.data, &(gps_in.data), sizeof(gpsData));

    gpstime = gps_in.time;

    nav_mode = (unsigned int)gps_in.data.nav_mode;

    output_stream << gpstime << "\t";
    output_stream << gps_in.data.lat << "\t";
    output_stream << gps_in.data.lng << "\t";
    output_stream << gps_in.data.altitude << "\t";
    output_stream << gps_in.data.ellipsoidal_height << "\t";
    output_stream << gps_in.data.vel_n << "\t";
    output_stream << gps_in.data.vel_e << "\t";
    output_stream << gps_in.data.vel_u << "\t";
    output_stream << gps_in.data.gdop << "\t";
    output_stream << gps_in.data.position_fom << "\t";
    output_stream << nav_mode << endl;
    /*    output_stream << gps_in.data.gdop << "\t";
    output_stream << gps_in.data.pdop << "\t";
    output_stream << gps_in.data.hdop << "\t";
    output_stream << gps_in.data.vdop << "\t";
    output_stream << gps_in.data.tdop << "\t";
    output_stream << gps_in.data.tfom << "\t";
    output_stream << gps_in.data.sats_used << endl;*/
  }
}

