//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_LadarSource.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void UD_LadarSourceArray::add_ladar(UD_LadarSource *new_ladsrc) 
{ 
  ladsrc[num_ladars] = new_ladsrc; 
  ladsrc[num_ladars]->index = num_ladars; 
  num_ladars++;
  for (int i = 0; i < num_ladars; i++)
    printf("index %i\n", ladsrc[i]->index);
}

//----------------------------------------------------------------------------

static int compdist(const void *d1, const void *d2) {
  float *di1 = (float *) d1;
  float *di2 = (float *) d2;
  return (*di1 > *di2);
}

/// what is radius of smallest circle (up to radius maxrad) with center at (x, z) 
/// containing n ladar hits?

double UD_LadarSourceArray::n_hit_radius(float x, float z, float maxrad, int n)
{
  int i, j, count;

  // compute distances to all hit points
  
  for (j = 0, count = 0; j < num_ladars; j++)
    for (i = 0; i < ladsrc[j]->num_rays; i++) {
    if (ladsrc[j]->dangerous[i])
      centerline_dist[count++] = hypot(x - ladsrc[j]->vehicle_x[i], 
    				       z - ladsrc[j]->vehicle_z[i]);
    //   else
    //    centerline_dist[i] = maxrad + 1;
  }

  if (count < n)
    return maxrad;

  // sort
  
  //  qsort(centerline_dist, num_rays, sizeof(float), compdist);
  qsort(centerline_dist, count, sizeof(float), compdist);

  // go min(entry n, maxrad)

  if (centerline_dist[n-1] > maxrad)
    return maxrad;
  else
    return centerline_dist[n-1];
}

//----------------------------------------------------------------------------

/// how much "stuff" projects into gap defined by [left_edge, right_edge]?
/// hit points further down the road are not weighted as highly

float UD_LadarSourceArray::density_in_gap(float left_edge, float right_edge, float sigma)
{
  int i;
  float density;

  for (i = 0, density = 0; i < num_ladars; i++)
    density += ladsrc[i]->density_in_gap(left_edge, right_edge, sigma);
  
  return density;
}

//----------------------------------------------------------------------------

/// direction is an angle in radians

void UD_LadarSourceArray::baseline_project(float direction)
{
  int i;

  for (i = 0; i < num_ladars; i++)
    ladsrc[i]->baseline_project(direction);
}

//----------------------------------------------------------------------------

void UD_LadarSourceArray::set_win_deltas(float dx, float dy)
{
  for (int i = 0; i < num_ladars; i++) { 
    ladsrc[i]->win_dx = dx; 
    ladsrc[i]->win_dy = dy; 
  }
}

//----------------------------------------------------------------------------

void UD_LadarSourceArray::capture(UD_ImageSource *imsrc)
{
  for (int i = 0; i < num_ladars; i++)
    ladsrc[i]->capture(imsrc);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for ladar calibration class.  takes intrinsic and extrinsic 
/// (relative to vehicle coordinate system) parameters

UD_LadarCalibration::UD_LadarCalibration(int num, float first_theta, float delta_theta, float x, float y, float z, float pitch)
{
  int i;

  // local names

  num_rays = num;
  first_ray_theta = first_theta;
  ray_delta_theta = delta_theta;
  dx = x;
  dy = y;
  dz = z;
  pitch_angle = pitch;

  // body

  sin_pan_angle = (float *) calloc(num_rays, sizeof(float));
  cos_pan_angle = (float *) calloc(num_rays, sizeof(float));

  float pan_angle;

  for (i = 0; i < num_rays; i++) {
    pan_angle = DEG2RAD(first_ray_theta + ray_delta_theta * (float) i);
    sin_pan_angle[i] = sin(pan_angle);
    cos_pan_angle[i] = cos(pan_angle);
  }

  last_ray_theta = RAD2DEG(pan_angle);
  max_range = 100.0;

  sin_pitch_angle = sin(pitch_angle);
  cos_pitch_angle = cos(pitch_angle);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for UD_LadarSource base class

UD_LadarSource::UD_LadarSource(UD_LadarCalibration *ladarcal)
{
  if (!ladarcal) {
    printf("ladar source requires calibration information\n");
    exit(1);
  }

  ladar_type = 0;

  // local names

  cal = ladarcal;
  num_rays = cal->num_rays;

  win_dx = win_dy = 0;

  range = (float *) calloc(num_rays, sizeof(float));
  angle = (float *) calloc(num_rays, sizeof(float));
  
  vehicle_x = (float *) calloc(num_rays, sizeof(float));
  vehicle_y = (float *) calloc(num_rays, sizeof(float));
  vehicle_z = (float *) calloc(num_rays, sizeof(float));
  
  image_x = (float *) calloc(num_rays, sizeof(float));
  image_y = (float *) calloc(num_rays, sizeof(float));

  baseline_x = (float *) calloc(num_rays, sizeof(float));
  dangerous = (int *) calloc(num_rays, sizeof(int));
}

//----------------------------------------------------------------------------

/// weight nearer hits higher

float UD_LadarSource::density_in_gap(float left_edge, float right_edge, float sigma)
{
  int i;
  float density;

  for (i = 0, density = 0; i < num_rays; i++)
    if (dangerous[i] && baseline_x[i] >= left_edge && baseline_x[i] <= right_edge) 
      density += exp(-sigma * vehicle_z[i]);
    
  return density;
}

//----------------------------------------------------------------------------

/// is ray i both in range and either >= bumper height hight or <= -bumper height low?

int UD_LadarSource::is_dangerous(int i)
{
  //  return (range[i] != NO_RETURN && fabs(vehicle_y[i]) > DANGER_HEIGHT);
  return (range[i] != NO_RETURN);
}

//----------------------------------------------------------------------------

void UD_LadarSource::baseline_project(float direction)
{
  int i;

  for (i = 0; i < num_rays; i++) {
    //    dangerous[i] = is_dangerous(i);
    baseline_x[i] = vehicle_x[i] - vehicle_z[i] * tan(direction);
  }
}

//----------------------------------------------------------------------------

/// draw ladar hit points projected onto image source.  redness is 
/// proportional to departure from planarity (positive or negative)

void UD_LadarSource::draw(UD_CameraCalibration *cal)
{
  int i;
  float r, g;

  if (!cal) {
    printf("no camera calibration defined\n");
    exit(1);
  }

  if (statesrc->is_synced()) {

    compute_image_positions(cal);

    return;

    glPointSize(2);
    glBegin(GL_POINTS);
    for (i = 0; i < num_rays; i++) {
      if (range[i] != NO_RETURN) {
	r = 5.0*fabs(vehicle_y[i]);
	g = 5.0*(1 - fabs(vehicle_y[i]));
	glColor3f(MIN2(r,1), MAX2(g,0),0);
	glVertex2f(image_x[i] + win_dx, image_y[i] + win_dy);
      }
    }
    glEnd();
  }
}

//----------------------------------------------------------------------------

/// project all ladar hit points onto image source with
/// particular calibration

void UD_LadarSource::compute_image_positions(UD_CameraCalibration *cal)
{
  int i;
  float cx, cy, cz;
  
  if (!cal) {
    printf("no camera calibration defined\n");
    exit(1);
  }

  // put ladar source or at least ladar points into road follower class

  for (i = 0; i < num_rays; i++) {
    //    ladar2vehicle(i, range[i], &vehicle_x[i], &vehicle_y[i], &vehicle_z[i]);
    cal->vehicle2camera(vehicle_x[i], vehicle_y[i], vehicle_z[i], &cx, &cy, &cz);
    cal->camera2image(cx, cy, cz, &image_x[i], &image_y[i]);      
  }

//   for (i = 0; i < num_rays; i++)
//     ladar2image(i, range[i], &image_x[i], &image_y[i], &vehicle_y[i], cal);
}

//----------------------------------------------------------------------------

/// transform one ladar hit point in ladar coordinates to vehicle coordinates

void UD_LadarSource_Scans::ladar2vehicle(int index, float range, float *vx, float *vy, float *vz)
{
  // spherical to rectangular coordinate transformation

  *vx = range * cal->sin_pan_angle[index];
  *vy = range * cal->cos_pan_angle[index] * cal->sin_pitch_angle;
  *vz = range * cal->cos_pan_angle[index] * cal->cos_pitch_angle;

  // translation from ladar position to vehicle origin

  *vx -= cal->dx;
  *vy -= cal->dy;
  *vz -= cal->dz;
}

//----------------------------------------------------------------------------

/// transform one ladar hit point in ladar coordinates to image coordinates.
/// h = vertical distance from ground plane in meters

void UD_LadarSource::ladar2image(int index, float range, float *x, float *y, float *h, UD_CameraCalibration *cal)
{
  float vx, vy, vz, cx, cy, cz;

  printf("do not call ladar2image\n");
  exit(1);

  //  ladar2vehicle(index, range, &vx, &vy, &vz);
  //  cal->vehicle2camera(vx, vy, vz, &cx, &cy, &cz);
  //  cal->camera2image(cx, cy, cz, x, y);      
  // *h = vy;
}

//----------------------------------------------------------------------------

/// utility rounding function

double UD_LadarSource::myround(double x)
{
  if (x - floor(x) >= 0.5)
    return ceil(x);
  else
    return floor(x);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#ifdef SKYNET

/// constructor for derived Skynet LadarSource class

UD_LadarSource_Skynet::UD_LadarSource_Skynet(UD_LadarCalibration *ladarcal, float mdiff) : UD_LadarSource(ladarcal)
{
  ladar_type = 1;

  printf("max = %i\n", RFComm->RFGetMaxScanPoints());

  skynet_angle = (double *) calloc(RFComm->RFGetMaxScanPoints(), sizeof(double));
  skynet_range = (double *) calloc(RFComm->RFGetMaxScanPoints(), sizeof(double));

  statesrc = new UD_StateSource_Skynet(mdiff);
}

//----------------------------------------------------------------------------

/// capture the current ladar scan--we have no choice about synchronization

void UD_LadarSource_Skynet::capture(UD_ImageSource *imsrc)
{
  capture();
}

//----------------------------------------------------------------------------

/// transform one ladar hit point in ladar coordinates to vehicle coordinates

void UD_LadarSource_Skynet::ladar2vehicle(int index, float range, float *vx, float *vy, float *vz)
{
  // spherical to rectangular coordinate transformation

  *vx = range * sin(angle[index]);
  *vy = range * cos(angle[index]) * cal->sin_pitch_angle;
  *vz = range * cos(angle[index]) * cal->cos_pitch_angle;

  // translation from ladar position to vehicle origin

  *vx -= cal->dx;
  *vy -= cal->dy;
  *vz -= cal->dz;
}

//----------------------------------------------------------------------------

/// capture the current ladar scan--we have no choice about synchronization

void UD_LadarSource_Skynet::capture(double target_timestamp)
{
  capture();
}

//----------------------------------------------------------------------------

/// simply read next ladar scan from skynet

void UD_LadarSource_Skynet::capture()
{
  int i, num_scans;
  //  double inv_rad_delta;
  unsigned long long timestamp;

  // access RFComm structure containing ladar data

  //  statesrc->get();
  //  printf("skynet needs to get state\n");
  //  exit(1);

  //  printf("trying to capture ladar %i\n", index);
  num_scans = RFComm->RFGetLadarScan((Ladar)index, skynet_angle, skynet_range, timestamp);
  //num_scans = RFComm->RFGetLadarScan(1, skynet_angle, skynet_range, timestamp);
  //  printf("got %i ladar scans (expecting %i)\n", num_scans, num_rays);
  //  fflush(stdout);

  // if no scan, put in a max range one

  //  for (i = 0; i < num_rays; i++)
  //    range[i] = NO_RETURN;

  // iterate through scans and angles (there may be less than 201, since max scans are discarded),
  // and put each scan in correctly indexed array slot

  // assuming my ladar calibration is correct
  
  //  inv_rad_delta = -1.0 / DEG2RAD(cal->ray_delta_theta);
  //  printf("%f %f\n", cal->ray_delta_theta, inv_rad_delta);
  //  exit(1);

  //  printf("%f %f\n", cal->first_ray_theta, DEG2RAD(cal->first_ray_theta));
  //  exit(1);

  /*
  for (i = 1; i < num_scans; i++) {
    index = (int) myround((skynet_angle[i] - DEG2RAD(cal->first_ray_theta)) * inv_rad_delta);
    printf("%i %lf %lf (%i %f)\n", i, skynet_range[i], skynet_angle[i], index, skynet_angle[i] * inv_rad_delta);
    if (index >= 0 && index < num_rays)
      range[index] = skynet_range[i];
  }
  */

  num_rays = num_scans;

  for (i = 0; i < num_rays; i++) {
    range[i] = (float) skynet_range[i+1];
    angle[i] =  0.5 * PI -(float) skynet_angle[i+1];
  }

  // convert to vehicle coordinates

  for (i = 0; i < num_rays; i++) {
    ladar2vehicle(i, range[i], &vehicle_x[i], &vehicle_y[i], &vehicle_z[i]);
    dangerous[i] = is_dangerous(i);
  }
}

//----------------------------------------------------------------------------

void UD_LadarSource_Skynet::init_log2file(char *filename)
{
  ladar_logfp = fopen(filename, "w");
}

//----------------------------------------------------------------------------

void UD_LadarSource_Skynet::log2file(int relframe, double timestampval)
{
  statesrc->read();

  fprintf(ladar_logfp, "%i %lf %lf %lf %lf %lf %i ", relframe, timestampval, statesrc->northing, statesrc->easting, statesrc->heading, statesrc->pitch, num_rays);
  for (int i = 0; i < num_rays; i++) 
    fprintf(ladar_logfp, "%f:%f ", angle[i], range[i]);
  fprintf(ladar_logfp, "\n");
  fflush(ladar_logfp);
}

#endif

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// read timestamp, # scans info that precedes actual ray range values in ladar scans file

void UD_LadarSource_Scans::read_header()
{
  long long int temp_timestamp;
  double temp_double_timestamp;
  int result, temp_scans;

  if (scan_type == SCANS_LADAR_LOG) 
    result = fscanf(scans_fp, "%lf ", &temp_double_timestamp);
  else if (scan_type == RAW_LADAR_LOG) {

    fscanf(scans_fp, "%Li ", &temp_timestamp);
						
    // eat repetition of number of rays that follows timestamp and
    // precedes actual range values

    fscanf(scans_fp, "%i ", &temp_scans);
    
    //    timestamp = 0.000001 * (double) temp_timestamp;
  }
  else {
    printf("unsupported ladar log type\n");
    exit(1);
  }
}

//----------------------------------------------------------------------------

/// capture the ladar scan closest in time to the current image source frame.
/// this is designed to work with an image_list source which has a log file
/// containing timestamps

void UD_LadarSource_Scans::capture(UD_ImageSource *imsrc)
{
  if (imsrc)
    capture(imsrc->timestamp);
  else
    capture();
}

//----------------------------------------------------------------------------

/// get ladar scan in log file closest to timestamp.   

void UD_LadarSource_Scans::capture(double target_timestamp)
{
  int relative_index, i;

  relative_index = statesrc->sync(target_timestamp);
  for (i = 0; i < relative_index; i++)
    capture();
}

//----------------------------------------------------------------------------

/// simply read next ladar scan from file

void UD_LadarSource_Scans::capture()
{
  int i;
  float f;

  // "capture" data

  read_header();

  for (i = 0; i < num_rays; i++) {

    // WTF??? this writes over last_scans_pos when it's a class member 
    fscanf(scans_fp, "%f ", &(range[i]));

    // convert to meters (assuming raw numbers are in cm and max is 8191)
    if (range[i] >= 8183)
      range[i] = NO_RETURN;
    else
      range[i] *= 0.01;
  }
  fscanf(scans_fp, "\n");

  for (i = 0; i < num_rays; i++) {
    ladar2vehicle(i, range[i], &vehicle_x[i], &vehicle_y[i], &vehicle_z[i]);
    dangerous[i] = is_dangerous(i);
  }
}

//----------------------------------------------------------------------------

/// skip comments at beginning of Caltech ladar scan file

void UD_LadarSource_Scans::eat_comments(FILE *fp)
{
  char c;
  char str[80];

  while (1) {
    c = fgetc(fp);
    ungetc(c, fp);
    if (c == '%') {
//      fgets (str, 79, fp);
      do {
	c = fgetc(fp);
	//	printf("%c", c);
      } while (c != '\n');
    }
    else
      break;
  }
}

//----------------------------------------------------------------------------

/// constructor for derived Scans LadarSource class.  opens the Caltech
/// scans log file specified in source_path and uses calibration parameters
/// in ladarcal

UD_LadarSource_Scans::UD_LadarSource_Scans(char *source_path, UD_LadarCalibration *ladarcal, float mdiff) : UD_LadarSource(ladarcal)
{
  int len;
  char *state_filename;

  ladar_type = 2;

  scans_fp = fopen(source_path, "r");
  if (!scans_fp) {
    printf("no such scans log\n");
    exit(1);
  }
  eat_comments(scans_fp);

  // old ladar scans log type ends in "log", new type ends in "raw"
  // old ladar state log also ends in "log", new type ends in "state"

  len = strlen(source_path) - 3;
  state_filename = (char *) calloc(MAXSTRLEN, sizeof(char));
  strcpy(state_filename, source_path);

  if (!strcmp("raw", (char *) &source_path[len])) {
    scan_type = RAW_LADAR_LOG;
    sprintf((char *) &state_filename[len], "state");
  }
  else if (!strcmp("log", (char *) &source_path[len])) {
    scan_type = SCANS_LADAR_LOG;
    for (int x = 0; x < strlen(state_filename); x++) {
      if (state_filename[x] == 'c' && 
	  state_filename[x+1] == 'a' && 
	  state_filename[x+2] == 'n' && 
	  state_filename[x+3] == 's') {
	sprintf((char *) &state_filename[x], "tate");
	state_filename[x+4]='_';
	break;
      }
    }
	//    exit(1);
    //    int x = strlen(state_filename)-25;
    //    sprintf((char *) &state_filename[x], "state");
    //    state_filename[x+5]='_';
  }
  else {
    printf("unsupported ladar log type\n");
    exit(1);
  }

  statesrc = new UD_StateSource_Scans(state_filename, mdiff);
}

//----------------------------------------------------------------------------

/// look for and process any ladar-specific command-line flags

void UD_LadarSource_Scans::process_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
