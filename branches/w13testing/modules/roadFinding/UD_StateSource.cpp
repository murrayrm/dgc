//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_StateSource.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// utility rounding function

double UD_StateSource::myround(double x)
{
  if (x - floor(x) >= 0.5)
    return ceil(x);
  else
    return floor(x);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#ifdef SKYNET

/// constructor for derived Skynet StateSource class

UD_StateSource_Skynet::UD_StateSource_Skynet(double mdiff) : UD_StateSource(mdiff)
{
  //  printf("constructing skynet state\n");
  state_type = 1; 
}

//----------------------------------------------------------------------------

void UD_StateSource_Skynet::get(double *northing, double *easting, double *heading)
{
  RFComm->RFUpdateState();

  *northing = RFComm->RFNorthing();
  *easting = RFComm->RFEasting();
  *heading = RFComm->RFYaw();
}

//----------------------------------------------------------------------------

int UD_StateSource_Skynet::read()
{
  RFComm->RFUpdateState();

  northing = RFComm->RFNorthing();
  easting = RFComm->RFEasting();
  heading = RFComm->RFYaw();
  pitch = RFComm->RFPitch();

  sunpos(northing, easting, 11, &sunalt, &sunazi);   // assuming UTM zone 11
}

#endif

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// shouldn't need to do this, but making it a member caused a problem
// when reading ranges in

long last_state_pos;

//----------------------------------------------------------------------------

void UD_StateSource_Scans::get(double *n, double *e, double *h)
{
  *n = northing;
  *e = easting;
  *h = heading;
}

//----------------------------------------------------------------------------

/// look ahead at next ladar scan's timestamp.  do this so that we can
/// tell whether we would get better time synchronization
/// by using it instead of the current scan

int UD_StateSource_Scans::peek_read()
{
  long pos;

  pos = ftell(state_fp);
  if (!read())
    return 0;
  fseek(state_fp, pos, SEEK_SET);

  return 1;
}

//----------------------------------------------------------------------------

/// move file pointer in state log to entry with timestamp closest to 
/// target_timestamp and return relative index--i.e., how many entries
/// were jumped to get there (file pointer does not rewind between calls)

int UD_StateSource_Scans::sync(double target_timestamp)
{
  int i, result, relative_state_num;
  double last_diff;

  if (!target_timestamp) {
    printf("invalid target timestamp--did you specify -image_timestamps?\n");
    exit(1);
  }
 
  relative_state_num = 0;

  state_seek();
  state_num = last_state_num;

  state_tell();         // where are we?
  result = read();

  if (!result)    // no more states in log file
    diff = NONE;
  else {
    last_diff = fabs(timestamp - target_timestamp);
    
    do {
      result = peek_read();

      diff = fabs(timestamp - target_timestamp);

      if (result && diff < last_diff) {
	state_num++;
	relative_state_num++;
	state_tell();
	read();
	last_state_num = state_num;
	last_diff = diff;
// 	printf("state %i: %lf -> %lf (%lf)\n", state_num, timestamp, last_diff, target_timestamp);
// 	fflush(stdout);
      }
      else {
// 	printf("discrepancy increased to %lf from %lf with last_state_pos = %li\n", diff, last_diff, last_state_pos);
// 	fflush(stdout);
	break;
      }
    } while (result);
      
    // finish up

    diff = last_diff;
    //    printf("holding last diff = %lf with last pos %li\n", last_diff, last_state_pos);
  }

  return relative_state_num;
}

//----------------------------------------------------------------------------

// copy of UD_Aerial version.  Should probably be merged somehow

#define WGS_84_EQUATORIAL_RADIUS        6378137
#define WGS_84_SQUARE_OF_ECCENTRICITY	0.00669438
#define RDDF_COLS   5

void LLtoUTM(const double Lat, const double Long, double *UTMNorthing, double *UTMEasting, int *ZoneNumber)
{
  double a = WGS_84_EQUATORIAL_RADIUS;
  double eccSquared = WGS_84_SQUARE_OF_ECCENTRICITY;
  double k0 = 0.9996;
  
  double LongOrigin;
  double eccPrimeSquared;
  double N, T, C, A, M;
  double LongTemp,LatRad,LongRad;
  double LongOriginRad;
  
  //Make sure the longitude is between -180.00 .. 179.9
  LongTemp = (Long+180.0)-(int)((Long+180)/360)*360.0-180.0; // -180.00 .. 179.9;
  
  LatRad = DEG2RAD(Lat);
  LongRad = DEG2RAD(LongTemp);
  
  *ZoneNumber = (int)((LongTemp + 180)/6) + 1;
  
  if( Lat >= 56.0 && Lat < 64.0 && LongTemp >= 3.0 && LongTemp < 12.0 )
    *ZoneNumber = 32;
  
  // Special zones for Svalbard
  if( Lat >= 72.0 && Lat < 84.0 ) 
    {
      if(      LongTemp >= 0.0  && LongTemp <  9.0 ) *ZoneNumber = 31;
      else if( LongTemp >= 9.0  && LongTemp < 21.0 ) *ZoneNumber = 33;
      else if( LongTemp >= 21.0 && LongTemp < 33.0 ) *ZoneNumber = 35;
      else if( LongTemp >= 33.0 && LongTemp < 42.0 ) *ZoneNumber = 37;
    }
  LongOrigin = (*ZoneNumber - 1)*6 - 180 + 3;  //+3 puts origin in middle of zone
  LongOriginRad = DEG2RAD(LongOrigin);
  
  eccPrimeSquared = (eccSquared)/(1-eccSquared);
  
  N = a/sqrt(1-eccSquared*sin(LatRad)*sin(LatRad));
  T = tan(LatRad)*tan(LatRad);
  C = eccPrimeSquared*cos(LatRad)*cos(LatRad);
  A = cos(LatRad)*(LongRad-LongOriginRad);
  
  M = a*((1	- eccSquared/4		- 3*eccSquared*eccSquared/64	- 5*eccSquared*eccSquared*eccSquared/256)*LatRad 
	 - (3*eccSquared/8	+ 3*eccSquared*eccSquared/32	+ 45*eccSquared*eccSquared*eccSquared/1024)*sin(2*LatRad)
	 + (15*eccSquared*eccSquared/256 + 45*eccSquared*eccSquared*eccSquared/1024)*sin(4*LatRad) 
	 - (35*eccSquared*eccSquared*eccSquared/3072)*sin(6*LatRad));
  
  *UTMEasting = (double)(k0*N*(A+(1-T+C)*A*A*A/6
			       + (5-18*T+T*T+72*C-58*eccPrimeSquared)*A*A*A*A*A/120)
			 + 500000.0);
  
  *UTMNorthing = (double)(k0*(M+N*tan(LatRad)*(A*A/2+(5-T+9*C+4*C*C)*A*A*A*A/24
					       + (61-58*T+T*T+600*C-330*eccPrimeSquared)*A*A*A*A*A*A/720)));
  if (Lat < 0)
    *UTMNorthing += 10000000.0; //10000000 meter offset for southern hemisphere
}

//----------------------------------------------------------------------------

/// load next state from log file.  assumes file pointer is at beginning of a state
/// (before timestamp); returns 0 if no more states left

char ss[100];
char tok[100];
char *token;

int UD_StateSource_Scans::read()
{
  int i, result, tempint;
  long long int temp_long, temp_long2;
  double temp;

  // (0) LADARTime [usec] | (1) StateTime[usec] | (2) Northing[m] | (3) Easting [m] (4) Altitude[m] | (5) Vel_N[m/s] | (6) Vel_E[m/s] | (7) Vel_U[m/s] | (8) Acc_N[m/s^2] | (9) Acc_E[m/s^2] | (10) Acc_U[m/s^2] | (11) Roll[rad] | (12) Pitch[rad] | (13) Yaw[rad] | (14) RollRate[rad/s] | (15) PitchRate[rad/s] | (16) YawRate[rad/s] | (17) RollAcc[rad/s^2] | (18) PitchAcc[rad/s^2] | (19) YawAcc[rad/s^2]

  if (scan_type == RAW_LADAR_LOG) {

    result = fscanf(state_fp, "%Li %Li ", &temp_long, &temp_long2);
    timestamp = 0.000001 * (double) temp_long;
    result = fscanf(state_fp, "%lf %lf ", &northing, &easting);
    result = fscanf(state_fp, "%lf ", &temp); // altitude (m)
    result = fscanf(state_fp, "%lf ", &temp); // vel_N (m/s)
    result = fscanf(state_fp, "%lf ", &temp); // vel_E (m/s)
    result = fscanf(state_fp, "%lf ", &temp); // vel_altitude (m/s)
    result = fscanf(state_fp, "%lf ", &northing_acceleration); // acc_N (m/s^2)
    result = fscanf(state_fp, "%lf ", &easting_acceleration); // acc_E (m/s^2)
    result = fscanf(state_fp, "%lf ", &temp); // acc_altitude (m/s^2)
    result = fscanf(state_fp, "%lf ", &temp); // roll
    result = fscanf(state_fp, "%lf ", &temp); // pitch
    result = fscanf(state_fp, "%lf ", &heading);
    for (i = 0; i < 6; i++)    // roll rate, pitch rate, yaw rate, roll acc, pitch acc, yaw acc
      result = fscanf(state_fp, "%lf ", &temp);
  }


  else if (scan_type == UNNATURAL_LADAR_LOG) {

    result = fscanf(state_fp, "%lf %lf %lf ", &timestamp, &timestamp, &timestamp);
    result = fscanf(state_fp, "%lf %lf ", &northing, &easting);
    result = fscanf(state_fp, "%lf ", &temp); // altitude (m)
    result = fscanf(state_fp, "%lf ", &temp); // vel_N (m/s)
    result = fscanf(state_fp, "%lf ", &temp); // vel_E (m/s)
    result = fscanf(state_fp, "%lf ", &temp); // vel_altitude (m/s)
    result = fscanf(state_fp, "%lf ", &northing_acceleration); // acc_N (m/s^2)
    result = fscanf(state_fp, "%lf ", &easting_acceleration); // acc_E (m/s^2)
    result = fscanf(state_fp, "%lf ", &temp); // acc_altitude (m/s^2)
    result = fscanf(state_fp, "%lf ", &temp); // roll
    result = fscanf(state_fp, "%lf ", &temp); // pitch
    result = fscanf(state_fp, "%lf ", &heading);
    for (i = 0; i < 5; i++)    // roll rate, pitch rate, yaw rate, roll acc, pitch acc, yaw acc
      result = fscanf(state_fp, "%lf ", &temp);
  }

  // (0) Time[sec] | (1) Easting[m] | (2) Northing[m] | (3) Altitude[m] | (4) Vel_E[m/s] | (5) Vel_N[m/s] | (6) Vel_U[m/s] | (7) Speed[m/s] | (8) Accel[m/s/s] | (9) Pitch[rad] | (10) Roll[rad] | (11) Yaw[rad] | (12) PitchRate[rad/s] | (13) RollRate[rad/s] | (14) YawRate[rad/s]

  else if (scan_type == SCANS_LADAR_LOG) {

    result = fscanf(state_fp, "%lf ", &timestamp); // time (s)
    result = fscanf(state_fp, "%lf %lf ", &easting, &northing);
    result = fscanf(state_fp, "%lf ", &temp); // altitude (m)
    result = fscanf(state_fp, "%lf ", &temp); // vel_E (m/s)
    result = fscanf(state_fp, "%lf ", &temp); // vel_N (m/s)
    result = fscanf(state_fp, "%lf ", &temp); // vel_altitude (m/s)
    result = fscanf(state_fp, "%lf ", &temp); // speed (m/s)
    result = fscanf(state_fp, "%lf ", &temp); // accel (m/s)
    result = fscanf(state_fp, "%lf ", &temp); // pitch
    result = fscanf(state_fp, "%lf ", &temp); // roll
    result = fscanf(state_fp, "%lf ", &heading);
    for (i = 0; i < 3; i++)    // pitch rate, roll rate, yaw rate
      result = fscanf(state_fp, "%lf ", &temp);

  }
  else if (scan_type == DAT_STATE_LOG) {

    result = fscanf(state_fp, "%lf ", &timestamp); // time (s)
    result = fscanf(state_fp, "%lf %lf ", &northing, &easting);
    for (i = 0; i < 8; i++)    
      result = fscanf(state_fp, "%lf ", &temp);
    // heading somewhere in between
    for (i = 0; i < 8; i++)    
      result = fscanf(state_fp, "%lf ", &temp);
  }
  else if (scan_type == RDDF_STATE_LOG) {
    
    //    UD_sleep(.1);

    double latitude, longitude;

//     if (feof(state_fp)) {
//       printf("end of RDDF\n");
//       exit(1);
//     }

    fgets(ss, 100, state_fp);
    token = strtok( ss,",");
    for (int cnt = 0; cnt < 1; cnt++) {
      temp = atof(token); token = strtok(NULL, ",");
    }
    latitude = atof(token);  token = strtok(NULL, ",");
    longitude = atof(token); token = strtok(NULL, ",");
    //    printf("%i %f %f\n", (int) temp, latitude, longitude);
    for (int cnt = 0; cnt < 2; cnt++) {
      temp = atof(token); token = strtok(NULL, ",");
    }

    LLtoUTM(latitude, longitude, &northing, &easting, &tempint);
  }
  else {
    printf("unsupported state log type\n");
    exit(1);
  }

  if (result != EOF)
    return TRUE;
  else
    return FALSE;
}

//----------------------------------------------------------------------------

void UD_StateSource_Scans::state_tell()
{
  last_state_pos = ftell(state_fp);
}

//----------------------------------------------------------------------------

void UD_StateSource_Scans::state_seek()
{
  fseek(state_fp, last_state_pos, SEEK_SET);
}

//----------------------------------------------------------------------------

/// skip comments at beginning of Caltech ladar state file

void UD_StateSource_Scans::eat_comments(FILE *fp)
{
  char c;
  char str[80];

  while (1) {
    c = fgetc(fp);
    ungetc(c, fp);
    if (c == '%') {
      do {
	c = fgetc(fp);
      } while (c != '\n');
    }
    else
      break;
  }
}

//----------------------------------------------------------------------------

/// constructor for derived Scans StateSource class.  opens the Caltech
/// state log file specified in source_path 

UD_StateSource_Scans::UD_StateSource_Scans(char *source_path, double mdiff) : UD_StateSource(mdiff)
{
  int len;

  //  printf("constructing scans state\n");
  state_type = 2;
 
  len = strlen(source_path) - 3;

  if (!strcmp("ate", (char *) &source_path[len])) 
    scan_type = RAW_LADAR_LOG;
  else if (!strcmp("log", (char *) &source_path[len])) 
    scan_type = SCANS_LADAR_LOG;
  else if (!strcmp("unn", (char *) &source_path[len])) 
    scan_type = UNNATURAL_LADAR_LOG;
  else if (!strcmp("dat", (char *) &source_path[len])) 
    scan_type = DAT_STATE_LOG;
  else if (!strcmp("ddf", (char *) &source_path[len])) 
    scan_type = RDDF_STATE_LOG;
  else {
    printf("unsupported state log type\n");
    exit(1);
  }

  state_fp = fopen(source_path, "r");
  if (!state_fp && scan_type != SCANS_LADAR_LOG) {
    printf("no such state log %s\n", source_path);
    exit(1);
  }
  else if (!state_fp && scan_type == SCANS_LADAR_LOG) {
    sprintf((char *) &source_path[len], "unn");
    state_fp = fopen(source_path, "r");
    if (!state_fp) {
      printf("no such state log %s\n", source_path);
      exit(1);
    }
    else
      scan_type = UNNATURAL_LADAR_LOG;
  }
  eat_comments(state_fp);

  state_tell();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
