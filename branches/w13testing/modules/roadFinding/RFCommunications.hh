#ifndef RFCOMMUNICATIONS_HH
#define RFCOMMUNICATIONS_HH

#include "StateClient.h"
#include "VehicleState.hh"
#include "SkynetContainer.h"
#include "sn_msg.hh"
#include "sn_types.h"
#include "TrajTalker.h"
#include <math.h>
#include "ladar/ladarSource.hh"
#include "DGCutils"

#include "smart_counter.hh"

enum Ladar {
  Bumper = 0,
  Roof = 1
};

class RFCommunications : public CStateClient, public CTrajTalker
{
public:
  RFCommunications(int skynetkey, bool waitstate=true);
  ~RFCommunications();

  /*
  **
  ** Both these functions eventually result in roads and trajs getting sent
  **
  */
  void RFSendTraj(double dir, double lat);
  void RFSendRoad(double *x, double *y, double *width, int size);

  /*
  **
  ** Return ladar scans and maximal number of points in ladar scan,
  ** angle is in radians with 0 to the right
  **
  */
  int RFGetMaxScanPoints();
  int RFGetLadarScan(Ladar ladar, double *angle, double *range, unsigned long long &tStamp);

  /*
  **
  ** Force a state update and read of current position
  **
  */
  void RFUpdateState();
  double RFNorthing();
  double RFEasting();
  double RFYaw();
  double RFPitch();

 private:   //Private helpers
  //Ladar reader thread
  void readLadarThread(void *ladar);  //Takes as parameter which ladar to use

private:
  bool m_running;      ///< Flags threads to terminate 

  /*
  **
  ** General settings
  **
  */
  double m_road_speed;
  double m_extend_backward;     //m to extend backwards (to make the road pass under us)
  bool m_send_road;
  bool m_send_traj;

  int m_send_every_x_road;

  /*
  **
  ** ladar data/mutex, 0=bumper, 1=roof
  **
  */
  volatile bool m_use_ladar[2];
  ladarMeasurementStruct m_ladar_info[2];
  pthread_mutex_t m_ladar_mutex[2];
  int m_ladar_socket[2];
  smart_counter<int> m_ladar_cnt[2];    //Number of ladar scans

  /*
  **
  ** Trajectory sending parameters
  **
  */
  int m_traj_socket;         //Socket for traj sending
  CTraj m_traj;              //Declare here as it allocates ~2.4Mb!!!!
  int m_traj_length;         //length of trajectory sending to trajFolllower
  int m_traj_density;        //See constructor
  int m_traj_max_vel;        //Max speed traj will send
  smart_counter<int> m_traj_cnt;  //Number of trajectories sent

  /*
  **
  ** Road sending parameters
  **
  */
  int m_road_socket;      //Socket for road to map
  smart_counter<int> m_road_cnt;  //Number of roads sent
};

#endif
