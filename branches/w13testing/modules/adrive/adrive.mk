ADRIVE_PATH = $(DGC)/modules/adrive
#Libraries
ADRIVE_LIBS = $(SDSLIB) $(SKYNETLIB) $(SPARROWLIB) $(CDD) $(ALICELIB) $(MODULEHELPERSLIB) $(TIMBERLIB)

#Source
ADRIVE_ADRIVE_CFILES = 	$(ADRIVE_PATH)/actuators.c \
			$(ADRIVE_PATH)/adrive.c \
			$(ADRIVE_PATH)/askynet_monitor.cc \
			$(ADRIVE_PATH)/sn_send_commandline.cc \
			$(ADRIVE_PATH)/AdriveTimberClient.cc \
			$(ADRIVE_PATH)/adriveMain.c \
			$(ADRIVE_PATH)/ATimberBox.cc \

ADRIVE_ADRIVE_HEADERS = $(ADRIVE_PATH)/actuators.h \
			$(ADRIVE_PATH)/adrive.h \
			$(ADRIVE_PATH)/askynet_monitor.hh \
			$(ADRIVE_PATH)/addisp.dd \
			$(ADRIVE_PATH)/adrive_skynet_interface_types.h \
			$(ADRIVE_PATH)/AdriveTimberClient.hh \
			$(ADRIVE_PATH)/ATimberBox.hh \

ADRIVE_ADRIVE_DEPEND =  $(ADRIVE_ADRIVE_CFILES) $(ADRIVE_ADRIVE_HEADERS) 


ADRIVE_DEPEND = $(ADRIVE_ADRIVE_DEPEND) $(ADRIVE_LIBS)
#$(ADRIVE_VEHLIB_DEPEND) 
