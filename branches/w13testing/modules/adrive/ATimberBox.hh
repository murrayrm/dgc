#ifndef __ATIMBERBOX_HH__
#define __ATIMBERBOX_HH__

#include "SkynetContainer.h"
#include "CTimber.hh"

class AtimberBox : virtual public CSkynetContainer {
public:
  AtimberBox(int skynetKey);
  ~AtimberBox();


  void timberStart();
  void timberStop();

private:
  int timberMsgSocket;
  
};


#endif //__ATIMBERBOX_HH__
