#include "fusionMapper.hh"
#include <unistd.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"

#include <iostream>


using namespace std;

int QUIT=0;
int PAUSE=0;
int RESET=0;
int CHANGE=0;
int REPAINT=0;
int REEVALUATE=0;
int SAVE=0;
int CLEAR=0;

int sparrowSNKey;

double sparrowAggrTuningRDDFScaling;
double sparrowAggrTuningMaxSpeed;

char sparrowNameFeeder0[32];
char sparrowNameFeeder1[32];
char sparrowNameFeeder2[32];
char sparrowNameFeeder3[32];

int sparrowStatusFeeder[MAX_INPUT_ELEV_LAYERS];
int sparrowSendCostFeeder[MAX_INPUT_ELEV_LAYERS];
int sparrowNumMsgsFeeder[MAX_INPUT_ELEV_LAYERS];
int sparrowMsgSizeFeeder[MAX_INPUT_ELEV_LAYERS];
double sparrowMsgSizeAvgFeeder[MAX_INPUT_ELEV_LAYERS];
double sparrowTimeLockFeeder[MAX_INPUT_ELEV_LAYERS];
double sparrowTimeStage1Feeder[MAX_INPUT_ELEV_LAYERS];
double sparrowTimeStage2Feeder[MAX_INPUT_ELEV_LAYERS];
double sparrowTimeRemainingFeeder[MAX_INPUT_ELEV_LAYERS];
double sparrowTimeTotalFeeder[MAX_INPUT_ELEV_LAYERS];

int sparrowStatusStereoFeeder;
int sparrowNumMsgsStereoFeeder;
int sparrowMsgSizeStereoFeeder;
double sparrowMsgSizeAvgStereoFeeder;
double sparrowTimeTotalStereoFeeder;

int sparrowStatusGPSFinder;
int sparrowNumMsgsGPSFinder;
int sparrowMsgSizeGPSFinder;
double sparrowMsgSizeAvgGPSFinder;
double sparrowTimeTotalGPSFinder;

int sparrowStatusRoadFinder;
int sparrowNumMsgsRoadFinder;
int sparrowMsgSizeRoadFinder;
double sparrowMsgSizeAvgRoadFinder;
double sparrowTimeTotalRoadFinder;

int sparrowStatusSupercon;
int sparrowNumMsgsSupercon;
int sparrowMsgSizeSupercon;
double sparrowMsgSizeAvgSupercon;
double sparrowTimeTotalSupercon;

int sparrowStatusFusionMapper;
int sparrowNumMsgsFusionMapper;
int sparrowMsgSizeFusionMapper;
double sparrowMsgSizeAvgFusionMapper;
double sparrowTimeLockFusionMapper;
double sparrowTimeStage1FusionMapper;
double sparrowTimeStage2FusionMapper;
double sparrowTimeRemainingFusionMapper;
double sparrowTimeTotalFusionMapper;

double sparrowRDDFNoDataSpeed;
int maxCellsToInterpolate;
int minNumSupportingCellsForInterpolation;


char sdErrorMessage[128];

#include "costtable.h"
#include "vddtable.h"


void FusionMapper::UpdateSparrowVariablesLoop() {
  m_input.SkynetKey = sparrowSNKey = mapperOpts.optSNKey;

	sprintf(sdErrorMessage, "None");

 	int mapDeltaSocketCost[4];
	for(int i=0; i < 4; i++)
		mapDeltaSocketCost[i] = m_skynet.get_send_sock(layerArray[i].msgTypeCost);

	sparrowAggrTuningRDDFScaling = _corridorOptsReference.optRDDFscaling;
	sparrowAggrTuningMaxSpeed = costFuserOpts.maxSpeed = _corridorOptsReference.optMaxSpeed;
	sparrowRDDFNoDataSpeed = _corridorOptsFinalCost.optMaxSpeed;

	maxCellsToInterpolate = costFuserOpts.maxCellsToInterpolate;
	minNumSupportingCellsForInterpolation = costFuserOpts.minNumSupportingCellsForInterpolation;

	sprintf(sparrowNameFeeder0, "%s", layerArray[0].name);
	sprintf(sparrowNameFeeder1, "%s", layerArray[1].name);
	sprintf(sparrowNameFeeder2, "%s", layerArray[2].name);
	sprintf(sparrowNameFeeder3, "%s", layerArray[3].name);

  int savedMapCount = 0;

    while(!_QUIT) {
      //printf("Got here\n");
    //Update variables that changed in the display
    if(CHANGE) {
      CHANGE = 0;

			for(int i=0; i < 4; i++) {
				layerArray[i].enabled = (bool)sparrowStatusFeeder[i];
				layerArray[i].sendCost = (bool)sparrowSendCostFeeder[i];
			}
      //mapperOpts.optStereo = sparrowStatusStereoFeeder;
      //mapperOpts.optStatic = sparrowStatusGPSFinder;
      mapperOpts.optRoad   = sparrowStatusRoadFinder;

      _corridorOptsReference.optRDDFscaling = sparrowAggrTuningRDDFScaling;
			costFuserOpts.maxSpeed = _corridorOptsReference.optMaxSpeed = sparrowAggrTuningMaxSpeed;
      _corridorOptsFinalCost.optMaxSpeed = sparrowRDDFNoDataSpeed;

			costFuserOpts.maxCellsToInterpolate = maxCellsToInterpolate;
			costFuserOpts.minNumSupportingCellsForInterpolation = minNumSupportingCellsForInterpolation;

			costFuserOpts.maxCellsToInterpolate = 1;
			costFuserOpts.minNumSupportingCellsForInterpolation=5;
    }

    //Update read-only variables
		for(int i=0; i < 4; i++) {
			sparrowNumMsgsFeeder[i] = layerArray[i].statusInfo.numMsgs;
			sparrowMsgSizeFeeder[i] = layerArray[i].statusInfo.msgSize;
			sparrowMsgSizeAvgFeeder[i] = layerArray[i].statusInfo.msgSizeAvg;
			sparrowTimeTotalFeeder[i] = layerArray[i].statusInfo.processTimeAvg;
			sparrowTimeLockFeeder[i] = layerArray[i].statusInfo.lockTimeAvg;
			sparrowTimeRemainingFeeder[i] = layerArray[i].statusInfo.remainingTimeAvg;
			sparrowTimeStage1Feeder[i] = layerArray[i].statusInfo.stage1TimeAvg;
			sparrowTimeStage2Feeder[i] = layerArray[i].statusInfo.stage2TimeAvg;
			sparrowStatusFeeder[i] = (int)layerArray[i].enabled;
			sparrowSendCostFeeder[i] = (int)layerArray[i].sendCost;
		}

    //sparrowStatusLadarFeederBumper = mapperOpts.optLadar;
    //m_input.oSF = sparrowStatusStereoFeeder = mapperOpts.optStereo;
    //m_input.oGF = sparrowStatusGPSFinder = mapperOpts.optStatic;
    m_input.oRF = sparrowStatusRoadFinder = mapperOpts.optRoad;
    m_input.oFM = sparrowStatusFusionMapper = 1;
  
    m_input.nmSF = sparrowNumMsgsStereoFeeder = _infoStereoFeeder.numMsgs;
    m_input.msSF = sparrowMsgSizeStereoFeeder = _infoStereoFeeder.msgSize;
    m_input.amsSF = sparrowMsgSizeAvgStereoFeeder = _infoStereoFeeder.msgSizeAvg;
    m_input.tSF = sparrowTimeTotalStereoFeeder = _infoStereoFeeder.processTime;

    m_input.nmGF = sparrowNumMsgsGPSFinder = _infoStaticPainter.numMsgs;
    m_input.msGF = sparrowMsgSizeGPSFinder = _infoStaticPainter.msgSize;
    m_input.amsGF = sparrowMsgSizeAvgGPSFinder = _infoStaticPainter.msgSizeAvg;
    m_input.tGF = sparrowTimeTotalGPSFinder = _infoStaticPainter.processTime;

		m_input.nmSC = sparrowNumMsgsSupercon = _infoSupercon.numMsgs;
    m_input.msSC = sparrowMsgSizeSupercon = _infoSupercon.msgSize;
    m_input.amsSC = sparrowMsgSizeAvgSupercon = _infoSupercon.msgSizeAvg;
    m_input.tSC = sparrowTimeTotalSupercon = _infoSupercon.processTime;

    m_input.nmFM = sparrowNumMsgsFusionMapper = _infoFusionMapper.numMsgs;
    m_input.msFM = sparrowMsgSizeFusionMapper = _infoFusionMapper.msgSize;
    m_input.amsFM = sparrowMsgSizeAvgFusionMapper = _infoFusionMapper.msgSizeAvg;
		sparrowTimeStage1FusionMapper = _infoFusionMapper.stage1TimeAvg;
		sparrowTimeStage2FusionMapper = _infoFusionMapper.stage2TimeAvg;
		sparrowTimeRemainingFusionMapper = _infoFusionMapper.remainingTimeAvg;
    m_input.tFM = sparrowTimeTotalFusionMapper = _infoFusionMapper.processTimeAvg;
    sparrowTimeLockFusionMapper = _infoFusionMapper.lockTimeAvg;

    _PAUSE = PAUSE;
    _QUIT = QUIT;
    if(RESET) {
			for(int i=0; i<4; i++) 
				layerArray[i].statusInfo.reset();
      //_infoLadarFeederBumper.reset();
      _infoFusionMapper.reset();
      _infoStaticPainter.reset();
      RESET = 0;
    }

    if(REPAINT) {
      REPAINT = 0;
			sprintf(sdErrorMessage, "Resending entire map, please wait...");
			dd_redraw(0);
      int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
      DGClockMutex(&m_mapMutex);
      CDeltaList* deltaList = fusionMap.serializeFullMapDelta<double>(layerID_costFinal);
      SendMapdelta(socket_num, deltaList);
      fusionMap.resetDelta<double>(layerID_costFinal);
			for(int i=0; i<4; i++) {
				if(layerArray[i].sendCost) {
					deltaList = fusionMap.serializeDelta<double>(layerArray[i].costLayerNum, 0);
					if(!deltaList->isShiftOnly()) {
						SendMapdelta(mapDeltaSocketCost[i], deltaList);
						fusionMap.resetDelta<double>(layerArray[i].costLayerNum);
					}
				}
			}
      DGCunlockMutex(&m_mapMutex);
			sprintf(sdErrorMessage, "Done resending entire map");
			dd_redraw(0);
    }

    if(REEVALUATE) {
      REEVALUATE = 0;
			sprintf(sdErrorMessage, "Reevaluating entire map, please wait...");
			dd_redraw(0);
      DGClockMutex(&m_mapMutex);
			fusionMap.clearLayer(layerID_corridor);
			//fusionCorridorReference.repaintAll();
			fusionMap.clearLayer(layerID_costFinal);
			fusionCorridorPainter->repaintAll();
			for(int i=0; i < 4; i++) {
				fusionMap.clearLayer(layerArray[i].costLayerNum);
				defaultTerrainPainter->readOptionsFromFile(layerArray[i].terrainPainterIndex, layerArray[i].optionsFilename);
				defaultTerrainPainter->repaintAll(layerArray[i].terrainPainterIndex,
																					layerArray[i].costPainterIndex);
			}
			costFuser->fuseChangesCost();
			costFuser->interpolateChanges();			
      DGCunlockMutex(&m_mapMutex);
			sprintf(sdErrorMessage, "Done reevaluating entire map.");
			dd_redraw(0);
    }

		if(CLEAR) {
      DGClockMutex(&m_mapMutex);
			fusionMap.clearMap();
      DGCunlockMutex(&m_mapMutex);
			CLEAR = 0;
		}

    if(SAVE) {
      SAVE = 0;
      DGClockMutex(&m_mapMutex);
      char mapName[128];
      for(int i=0; i<fusionMap.getNumLayers(); i++) {
				sprintf(mapName, "fusionMapperMap-%03d-Layer-%02d-(%s)", savedMapCount, i,
								fusionMap.getLayerLabel(i));
				fusionMap.saveLayer<double>(i, mapName, false);
				fusionMap.saveLayer<double>(i, mapName, true);
      }
      savedMapCount++;
      DGCunlockMutex(&m_mapMutex);
    }

    usleep(10000);
  }

}

void FusionMapper::SparrowDisplayLoop() 
{

  dbg_all = 0;

  if (dd_open() < 0) exit(1);

  dd_bindkey('Q', user_quit);
  dd_bindkey('q', user_quit);
  dd_bindkey('P', user_pause);
  dd_bindkey('p', user_pause);
  dd_bindkey('R', user_reset);
  dd_bindkey('r', user_reset);
  dd_bindkey('S', user_save);
  dd_bindkey('s', user_save);
  dd_bindkey('A', user_repaint);
  dd_bindkey('a', user_repaint);
  dd_bindkey('E', user_reevaluate);
  dd_bindkey('e', user_reevaluate);

  dd_usetbl(vddtable);

  usleep(500000); // Wait a bit, because other threads will print some stuff out
  dd_loop();	
  dd_close();
  _QUIT = 1;
  //sleep(1);
}

int user_quit(long arg)
{
  QUIT = 1;
  return DD_EXIT_LOOP;
}


int user_reset(long arg)
{
  RESET = 1;
  return 0;
}

int user_pause(long arg)
{
  if(PAUSE) {
    PAUSE=0;
  } else {
    PAUSE=1;
  }
  return 0;
}

int user_change(long arg) {
  CHANGE=1;
  return 0;
}

int user_repaint(long arg) {
  REPAINT=1;
  return 0;
}


int user_save(long arg) {
  SAVE = 1;
  return 0;
}


int user_reevaluate(long arg) {
  REEVALUATE=1;
  return 0;
}


int user_clear(long arg) {
	CLEAR=1;
	return 0;
}
