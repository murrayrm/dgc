#include "fusionMapperTabSpecs.hh"
#include "fusionMapper.hh"

FusionMapper::FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg, int waitForState)
  : CSkynetContainer(SNfusionmapper, skynetKey),
    CStateClient(waitForState),
		CModuleTabClient(&m_input, &m_output)
{
  printf("Welcome to fusionmapper init\n");
  _QUIT = 0;
  _PAUSE = 0;

  mapperOpts = mapperOptsArg;

  // static allocated for memory efficiency, if larger than 100 pts, change this
  m_pRoadReceive = new double[MAX_ROAD_SIZE * 3 + 1];
  m_pRoadX = new double[MAX_ROAD_SIZE];
  m_pRoadY = new double[MAX_ROAD_SIZE];
  m_pRoadW = new double[MAX_ROAD_SIZE];
  m_pRoadPoints = 0;

  fusionMap.initMap(CONFIG_FILE_DEFAULT_MAP);


  layerID_costFinal      = fusionMap.addLayer<double>(CONFIG_FILE_COST,            true);
  layerID_corridor  = fusionMap.addLayer<double>(CONFIG_FILE_RDDF, true);
  layerID_road      = fusionMap.addLayer<double>(CONFIG_FILE_ROAD);

	costFuser = new CCostFuser(&fusionMap, layerID_costFinal, &fusionMap, layerID_corridor, &costFuserOpts);
	defaultTerrainPainter = new CTerrainCostPainter(&fusionMap, layerID_corridor, costFuser);

	layerArray[0].msgTypeElev = SNladarRoofDeltaMap;
	layerArray[0].msgTypeCost = SNladarRoofDeltaMapCost;
	layerArray[0].relWeight = 1.0;
	sprintf(layerArray[0].name, "LADAR (Roof)");
	sprintf(layerArray[0].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.roof");

	layerArray[1].msgTypeElev = SNladarSmallDeltaMap;
	layerArray[1].msgTypeCost = SNladarSmallDeltaMapCost;
	layerArray[1].relWeight = 0.1;
	sprintf(layerArray[1].name, "LADAR (Small)");
	sprintf(layerArray[1].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.small");

	layerArray[2].msgTypeElev = SNladarRieglDeltaMap;
	layerArray[2].msgTypeCost = SNladarRieglDeltaMapCost;
	layerArray[3].relWeight = 0.01;
	sprintf(layerArray[2].name, "LADAR (Riegl)");
	sprintf(layerArray[2].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.riegl");

	layerArray[3].msgTypeElev = SNladarBumperDeltaMap;
	layerArray[3].msgTypeCost = SNladarBumperDeltaMapCost;
	sprintf(layerArray[3].name, "LADAR (Bumper)");
	sprintf(layerArray[3].optionsFilename, "config/fusionMapper/opts.fusionMapper.ladar.bumper");	
	
	for(int i=0; i < 4; i++) {
		layerArray[i].elevLayerNum = fusionMap.addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV);
		layerArray[i].map = &fusionMap;
		layerArray[i].sendCost = true;
		layerArray[i].enabled = true;
		layerArray[i].terrainPainterIndex = defaultTerrainPainter->addElevLayer(layerArray[i].elevLayerNum, 
																																					&layerArray[i].options, layerArray[i].sendCost);
		layerArray[i].costLayerNum = defaultTerrainPainter->_costLayerNums[layerArray[i].terrainPainterIndex];
		layerArray[i].costPainterIndex = costFuser->addLayerElevCost(layerArray[i].map, layerArray[i].costLayerNum,
																																 layerArray[i].relWeight);
	}

	costFuserOpts.maxSpeed = 10.0; //defaultTerrainPainterOpts.maxSpeed = 10.0;
	costFuserOpts.maxCellsToInterpolate = 1;
	costFuserOpts.minNumSupportingCellsForInterpolation=5;
	
//   layerID_fusedLadarRoof = fusionMap.addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV, true);
//   layerID_fusedLadarSmall = fusionMap.addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV, true);
//   layerID_fusedLadarRiegl = fusionMap.addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV, true);
//   layerID_fusedLadarBumper = fusionMap.addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV, true);


  layerID_fusedYetTemp = fusionMap.addLayer<int>(0, -1, true);
  layerID_fusedYetFinal =  fusionMap.addLayer<int>(0, -1, true);

  // layerID_supercon = fusionMap.addLayer<int>(CONFIG_FILE_SUPERCON, false);
 layerID_supercon = fusionMap.addLayer<int>(0, -1, false);

  fusionCorridorPainter = new CCorridorPainter(&(fusionMap), 
																					 //layerID_costFinal, 
																					 &(fusionRDDF));
																					 //1);
	fusionCorridorPainter->addLayer(layerID_costFinal, &_corridorOptsFinalCost, 1);
	_corridorOptsFinalCost.optRDDFscaling = 1.0;
	_corridorOptsFinalCost.optMaxSpeed = 1.0;

	fusionCorridorPainter->addLayer(layerID_corridor, &_corridorOptsReference, 1);
	_corridorOptsReference.optRDDFscaling = 1.0;
	_corridorOptsReference.optMaxSpeed = 10.0;

  //fusionCorridorReference.initPainter(&(fusionMap), layerID_corridor, &(fusionRDDF), 1);
  

//   fusionCostPainter.initPainter(&(fusionMap),
// 				layerID_costFinal, layerID_costRoof, layerID_costSmall, layerID_costRiegl, layerID_costBumper,layerID_corridor ,layerID_road, 	&(speedMap),
// 				layerID_sampledCost,
// 				layerID_sampledCorridor,
// 				layerID_fusedYetFinal,
// 				layerID_supercon
// 				);  

  DGCcreateMutex(&m_mapMutex);

  ReadConfigFile();
  //fusionCostPainter.setMapperOpts(& mapperOpts );
  //fusionCorridorPainter.setPainterOptions(&(mapperOpts.corridorOpts));
	//double temp = mapperOpts.corridorOpts.optMaxSpeed;
  //mapperOpts.corridorOpts.optMaxSpeed = mapperOpts.optMaxSpeed;
  //fusionCorridorReference.setPainterOptions(&(mapperOpts.corridorOpts));
  //mapperOpts.corridorOpts.optMaxSpeed = temp;
}


FusionMapper::~FusionMapper() {
  //FIX ME
  DGCdeleteMutex(&m_mapMutex);
  printf("Thank you for using the fusionmapper destructor\n");
}


void FusionMapper::ActiveLoop() {
  //TODO - Give this a sparrow/sngui display
  printf("Entering FusionMapper active-loop\n");
  printf("Press Ctrl+C to quit\n");
  //fusionCostPainter.setMapperOpts(&mapperOpts);
	int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
	int socketMapDeltaCorridor = m_skynet.get_send_sock(SNdeltaMapCorridor);
 	int mapDeltaSocketCost[4];
	for(int i=0; i < 4; i++)
		mapDeltaSocketCost[i] = m_skynet.get_send_sock(layerArray[i].msgTypeCost);

  NEcoord exposedRow[4], exposedCol[4];
 
	NEcoord frontLeftAliceCorner, frontRightAliceCorner, rearLeftAliceCorner, rearRightAliceCorner;

	int readOptsCtr = 0;

	while(!_QUIT) {
		int deltaSize = 0;
		//UpdateState();
		WaitForNewState();
		_infoFusionMapper.startProcessTimer();
		if(!_PAUSE) {
		  DGClockMutex(&m_mapMutex);
			_infoFusionMapper.endLockTimer();
			fusionMap.updateVehicleLoc(m_state.Northing, m_state.Easting);
		
			if(fusionMap.getExposedRowBoxUTM(exposedRow) ||
				 fusionMap.getExposedColBoxUTM(exposedCol)) {
	  
				//Just in case we evaluated the first condition of the if statement to true:
				fusionMap.getExposedColBoxUTM(exposedCol);

				//first calculate the stddev on our position
// 				double posUncertainty = hypot(m_state.NorthConf, m_state.EastConf);
// 				if(fusionRDDF.getExtraWidth() > ceil(posUncertainty/fusionMap.getResRows())*fusionMap.getResRows()) {
// 					fusionRDDF.setExtraWidth(ceil(posUncertainty/fusionMap.getResRows())*fusionMap.getResRows());
// 					//start shrinking
// 				} else if(fusionRDDF.getExtraWidth() < floor(posUncertainty/fusionMap.getResRows())*fusionMap.getResRows()) {
// 					fusionRDDF.setExtraWidth(floor(posUncertainty/fusionMap.getResRows())*fusionMap.getResRows());
// 					fusionCorridorReference.repaintAll();
// 					fusionCorridorPainter.repaintAll();
// 					//fusionCostPainter.repaintAll(1);
// 				}
		 
				
				
				// 				frontLeftAliceCorner = NEcoord(m_state.Northing + DIST_FRONT_AXLE_TO_FRONT*cos(m_state.Yaw) + VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																			 m_state.Easting + DIST_FRONT_AXLE_TO_FRONT*sin(m_state.Yaw) - VEHICLE_WIDTH/2.0*cos(m_state.Yaw));
				// 				frontRightAliceCorner = NEcoord(m_state.Northing + DIST_FRONT_AXLE_TO_FRONT*cos(m_state.Yaw) - VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																				m_state.Easting + DIST_FRONT_AXLE_TO_FRONT*sin(m_state.Yaw) + VEHICLE_WIDTH/2.0*cos(m_state.Yaw));
				// 				rearLeftAliceCorner = NEcoord(m_state.Northing + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*cos(m_state.Yaw) + VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																			m_state.Easting + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*sin(m_state.Yaw) - VEHICLE_WIDTH/2.0*cos(m_state.Yaw));
				// 				rearRightAliceCorner = NEcoord(m_state.Northing + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*cos(m_state.Yaw) - VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																			 m_state.Easting + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*sin(m_state.Yaw) + VEHICLE_WIDTH/2.0*cos(m_state.Yaw));

				// 				//Check to see if Alice is inside the corridor - if not
				// 				if(!fusionRDDF.isPointInCorridor(frontLeftAliceCorner) ||
				// 					 !fusionRDDF.isPointInCorridor(frontRightAliceCorner) ||
				// 					 !fusionRDDF.isPointInCorridor(rearLeftAliceCorner) ||
				// 					 !fusionRDDF.isPointInCorridor(rearRightAliceCorner)) {
				// 					//Then we need to expand the corridor where we are now and repaint the whole map
				// 					int largestWayptNum = fusionRDDF.findWaypointWithLargestWidth(NEcoord(m_state.Northing, m_state.Easting));
				// 					double currentOffset = fusionRDDF.getOffset(largestWayptNum) - fusionRDDF.getExtraWidth();
				// 					double distanceToTrackline = fmax(fmax(fusionRDDF.getDistToTrackline(frontLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(frontRightAliceCorner)),
				// 																						fmax(fusionRDDF.getDistToTrackline(rearLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(rearRightAliceCorner)));
				// 					double widthToAdd = distanceToTrackline - currentOffset;
				// 					fusionRDDF.setExtraWidth(ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows());
				// 					//cout << "expanding to " << ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows() << endl;
				// 					fusionCorridorPainter.repaintAll();
				// 					fusionSampledCorridorPainter.repaintAll();
				// 					if(!mapperOpts.optJeremy) {
				// 						//mapperOpts.optRDDFScaling,mapperOpts.optMaxSpeed);
				// 						fusionCostPainter.repaintAll(1);
				// 					} else {
				// 						fusionCostPainter.uberRepaintAll(1);
				// 					}
				// 				} else if(fusionRDDF.getExtraWidth()!=0.0) {
				// 					//We're inside the RDDF, but it may be too big - see if we should shrink it
				// 					int largestWayptNum = fusionRDDF.findWaypointWithLargestWidth(NEcoord(m_state.Northing, m_state.Easting));
				// 					double currentOffset = fusionRDDF.getOffset(largestWayptNum) - fusionRDDF.getExtraWidth();
				// 					double distanceToTrackline = fmax(fmax(fusionRDDF.getDistToTrackline(frontLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(frontRightAliceCorner)),
				// 																						fmax(fusionRDDF.getDistToTrackline(rearLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(rearRightAliceCorner)));
				// 					double widthToAdd = distanceToTrackline - currentOffset;
				// 					if(fusionRDDF.getExtraWidth() > ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows()) {
				// 						fusionRDDF.setExtraWidth(fmax(ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows(), 0.0));
				// 						//cout << "Shrinking to " << fmax(ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows(), 0.0) << endl;
				// 					} 
				// 					fusionCorridorPainter.paintChanges(exposedRow, exposedCol);
				// 					fusionCorridorReference.paintChanges(exposedRow, exposedCol);
				// 				} else {
				// 					fusionCorridorPainter.paintChanges(exposedRow, exposedCol);
				// 					fusionCorridorReference.paintChanges(exposedRow, exposedCol);
				// 				}
				// 			}      
				fusionCorridorPainter->paintChanges(exposedRow, exposedCol);
// fusionCorridorReferencepaintChanges(exposedRow, exposedCol);
				readOptsCtr++;

// 				for(int i=0; i<4; i++) {
// 					if(readOptsCtr%40 == 0)
// 						defaultTerrainPainter->readOptionsFromFile(layerArray[i].terrainPainterIndex, layerArray[i].optionsFilename);
// 					defaultTerrainPainter->paintChanges(layerArray[i].terrainPainterIndex,
// 																							layerArray[i].costPainterIndex);
// 					if(layerArray[i].sendCost) {
// 						CDeltaList* deltaList = fusionMap.serializeDelta<double>(layerArray[i].costLayerNum, 0);
// 						if(!deltaList->isShiftOnly()) {
// 							SendMapdelta(mapDeltaSocketCost[i], deltaList);
// 							fusionMap.resetDelta<double>(layerArray[i].costLayerNum);
// 						}
// 					}
// 				}

				_infoFusionMapper.endStage1Timer();
				costFuser->fuseChangesCost();
				costFuser->interpolateChanges();
				_infoFusionMapper.endStage2Timer();
			}

			unsigned long long timestamp;
			DGCgettime(timestamp);
	
		
			CDeltaList* deltaList = NULL;
			     
			deltaList = fusionMap.serializeDelta<double>(layerID_costFinal, timestamp);	
			if(!deltaList->isShiftOnly()) {
				SendMapdelta(socket_num, deltaList);
				deltaSize = 0;
				for(int i=0; i<deltaList->numDeltas; i++) {
					deltaSize += deltaList->deltaSizes[i];
				}
				_infoFusionMapper.endProcessTimer(deltaSize);	
				fusionMap.resetDelta<double>(layerID_costFinal);
			}

			deltaList = fusionMap.serializeDelta<double>(layerID_corridor, timestamp);	
			if(!deltaList->isShiftOnly()) {
				SendMapdelta(socketMapDeltaCorridor, deltaList);
				fusionMap.resetDelta<double>(layerID_corridor);
			}

			DGCunlockMutex(&m_mapMutex);    
		} else {
			DGCusleep(500000);
		}
	}

	printf("%s [%d]: Active loop quitting\n", __FILE__, __LINE__);
}


void FusionMapper::ReceiveDataThread_ElevFeeder(void* pArg) {
	int index = (int)pArg;

  int mapDeltaSocketElev = m_skynet.listen(layerArray[index].msgTypeElev, ALLMODULES);
 	int mapDeltaSocketCost = m_skynet.get_send_sock(layerArray[index].msgTypeCost);
  double UTMNorthing;
  double UTMEasting;

  CElevationFuser::STATUS overwrittenStatus = CElevationFuser::OK;

	int counter=0;
	int readOptsCtr = 0;

	char* pMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      int numreceived = m_skynet.get_msg(mapDeltaSocketElev, pMapDelta, MAX_DELTA_SIZE, 0);
      layerArray[index].statusInfo.startProcessTimer();
      if(layerArray[index].enabled) {
				if(numreceived>0) {
					DGClockMutex(&m_mapMutex);
					layerArray[index].statusInfo.endLockTimer();
					int numCells = layerArray[index].map->getDeltaSize(pMapDelta);
					for(int i=0; i<numCells; i++) {
					  CElevationFuser receivedData = layerArray[index].map->getDeltaVal<CElevationFuser>(i, layerArray[index].elevLayerNum, pMapDelta, &UTMNorthing, &UTMEasting);
						layerArray[index].map->setDataUTM<CElevationFuser>(layerArray[index].elevLayerNum, UTMNorthing, UTMEasting, receivedData);
					  defaultTerrainPainter->markChanges(layerArray[index].terrainPainterIndex, UTMNorthing, UTMEasting, overwrittenStatus);
					}
					layerArray[index].statusInfo.endStage1Timer();
					if(counter%20==0)
						defaultTerrainPainter->paintChanges(layerArray[index].terrainPainterIndex,
																								layerArray[index].costPainterIndex);
					if(readOptsCtr%75 == 0)
						defaultTerrainPainter->readOptionsFromFile(layerArray[index].terrainPainterIndex, layerArray[index].optionsFilename);
					counter++;
					readOptsCtr++;
					layerArray[index].statusInfo.endStage2Timer();
					if(layerArray[index].sendCost) {
						CDeltaList* deltaList = fusionMap.serializeDelta<double>(layerArray[index].costLayerNum, 0);
						if(!deltaList->isShiftOnly()) {
							SendMapdelta(mapDeltaSocketCost, deltaList);
							fusionMap.resetDelta<double>(layerArray[index].costLayerNum);
						}
					}
					layerArray[index].statusInfo.endProcessTimer(numreceived);
					DGCunlockMutex(&m_mapMutex);
				}
			}
		}
  }
  delete pMapDelta; 
}


// void FusionMapper::ReceiveDataThread_LadarRoof() {
//   int mapDeltaSocket = m_skynet.listen(SNladarRoofDeltaMap, MODladarFeeder);
//   double UTMNorthing;
//   double UTMEasting;


//   CElevationFuser::STATUS overwrittenStatus = CElevationFuser::OK;

//    m_pLadarMapDelta = new char[MAX_DELTA_SIZE];
//   while(!_QUIT) { 
//     if(!_PAUSE) {
//       int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pLadarMapDelta, MAX_DELTA_SIZE, 0);
//       _infoLadarFeeder.startProcessTimer();
//       if(mapperOpts.optLadar) {
// 				if(numreceived>0) {
// 					DGClockMutex(&m_mapMutex);
// 					int cellsize = fusionMap.getDeltaSize(m_pLadarMapDelta);
// 					for(int i=0; i<cellsize; i++) {
// 					  CElevationFuser receivedData = fusionMap.getDeltaVal<CElevationFuser>(i, layerID_fusedLadarRoof, m_pLadarMapDelta, &UTMNorthing, &UTMEasting);
// 					  if(mapperOpts.optAverage) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedLadarRoof, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MeanElevation(receivedData);
// 					  } else if(mapperOpts.optMax) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedLadarRoof, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MaxElevation(receivedData);
// 					  } else {
// 					    fusionMap.setDataUTM<CElevationFuser>(layerID_fusedLadarRoof, UTMNorthing, UTMEasting, receivedData);
// 					  }
					 					 
// 					  fusionCostPainter.paintChangesNotify(UTMNorthing, UTMEasting, overwrittenStatus, layerID_fusedLadarRoof, layerID_fusedYetTemp);

// 					}
// 					fusionCostPainter.paintNotifications(layerID_fusedYetTemp, layerID_fusedLadarRoof, layerID_fusedYetTemp);
// 					DGCunlockMutex(&m_mapMutex);
// 					_infoLadarFeeder.endProcessTimer(numreceived);
// 				}     
//       }
//     }
//   }
//   delete m_pLadarMapDelta; 
// }


// void FusionMapper::ReceiveDataThread_LadarSmall() {
//   int mapDeltaSocket = m_skynet.listen(SNladarSmallDeltaMap, MODladarFeeder);
//   double UTMNorthing;
//   double UTMEasting;


//   CElevationFuser::STATUS overwrittenStatus = CElevationFuser::OK;

//    m_pLadarMapDelta = new char[MAX_DELTA_SIZE];
//   while(!_QUIT) { 
//     if(!_PAUSE) {
//       int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pLadarMapDelta, MAX_DELTA_SIZE, 0);
//       _infoLadarFeeder.startProcessTimer();
//       if(mapperOpts.optLadar) {
// 				if(numreceived>0) {
// 					DGClockMutex(&m_mapMutex);
// 					int cellsize = fusionMap.getDeltaSize(m_pLadarMapDelta);
// 					for(int i=0; i<cellsize; i++) {
// 					  CElevationFuser receivedData = fusionMap.getDeltaVal<CElevationFuser>(i, layerID_fusedLadarSmall, m_pLadarMapDelta, &UTMNorthing, &UTMEasting);
// 					  if(mapperOpts.optAverage) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedLadarSmall, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MeanElevation(receivedData);
// 					  } else if(mapperOpts.optMax) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedLadarSmall, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MaxElevation(receivedData);
// 					  } else {
// 					    fusionMap.setDataUTM<CElevationFuser>(layerID_fusedLadarSmall, UTMNorthing, UTMEasting, receivedData);
// 					  }
					 					 
// 					  fusionCostPainter.paintChangesNotify(UTMNorthing, UTMEasting, overwrittenStatus, layerID_fusedLadarSmall, layerID_fusedYetTemp);

// 					}
// 					fusionCostPainter.paintNotifications(layerID_fusedYetTemp, layerID_fusedLadarSmall, layerID_fusedYetTemp);
// 					DGCunlockMutex(&m_mapMutex);
// 					_infoLadarFeeder.endProcessTimer(numreceived);
// 				}     
//       }
//     }
//   }
//   delete m_pLadarMapDelta; 
// }



// void FusionMapper::ReceiveDataThread_LadarRiegl() {
//   int mapDeltaSocket = m_skynet.listen(SNladarRieglDeltaMap, MODladarFeeder);
//   double UTMNorthing;
//   double UTMEasting;


//   CElevationFuser::STATUS overwrittenStatus = CElevationFuser::OK;

//    m_pLadarMapDelta = new char[MAX_DELTA_SIZE];
//   while(!_QUIT) { 
//     if(!_PAUSE) {
//       int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pLadarMapDelta, MAX_DELTA_SIZE, 0);
//       _infoLadarFeeder.startProcessTimer();
//       if(mapperOpts.optLadar) {
// 				if(numreceived>0) {
// 					DGClockMutex(&m_mapMutex);
// 					int cellsize = fusionMap.getDeltaSize(m_pLadarMapDelta);
// 					for(int i=0; i<cellsize; i++) {
// 					  CElevationFuser receivedData = fusionMap.getDeltaVal<CElevationFuser>(i, layerID_fusedLadarRiegl, m_pLadarMapDelta, &UTMNorthing, &UTMEasting);
// 					  if(mapperOpts.optAverage) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedLadarRiegl, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MeanElevation(receivedData);
// 					  } else if(mapperOpts.optMax) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedLadarRiegl, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MaxElevation(receivedData);
// 					  } else {
// 					    fusionMap.setDataUTM<CElevationFuser>(layerID_fusedLadarRiegl, UTMNorthing, UTMEasting, receivedData);
// 					  }
					 					 
// 					  fusionCostPainter.paintChangesNotify(UTMNorthing, UTMEasting, overwrittenStatus, layerID_fusedLadarRiegl, layerID_fusedYetTemp);

// 					}
// 					fusionCostPainter.paintNotifications(layerID_fusedYetTemp, layerID_fusedLadarRiegl, layerID_fusedYetTemp);
// 					DGCunlockMutex(&m_mapMutex);
// 					_infoLadarFeeder.endProcessTimer(numreceived);
// 				}     
//       }
//     }
//   }
//   delete m_pLadarMapDelta; 
// }


// void FusionMapper::ReceiveDataThread_LadarBumper() {
//   int mapDeltaSocket = m_skynet.listen(SNladarBumperDeltaMap, MODladarFeeder);
//   double UTMNorthing;
//   double UTMEasting;


//   CElevationFuser::STATUS overwrittenStatus = CElevationFuser::OK;

//    m_pLadarMapDelta = new char[MAX_DELTA_SIZE];
//   while(!_QUIT) { 
//     if(!_PAUSE) {
//       int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pLadarMapDelta, MAX_DELTA_SIZE, 0);
//       _infoLadarFeeder.startProcessTimer();
//       if(mapperOpts.optLadar) {
// 				if(numreceived>0) {
// 					DGClockMutex(&m_mapMutex);
// 					int cellsize = fusionMap.getDeltaSize(m_pLadarMapDelta);
// 					for(int i=0; i<cellsize; i++) {
// 					  CElevationFuser receivedData = fusionMap.getDeltaVal<CElevationFuser>(i, layerID_fusedLadarBumper, m_pLadarMapDelta, &UTMNorthing, &UTMEasting);
// 					  if(mapperOpts.optAverage) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedLadarBumper, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MeanElevation(receivedData);
// 					  } else if(mapperOpts.optMax) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedLadarBumper, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MaxElevation(receivedData);
// 					  } else {
// 					    fusionMap.setDataUTM<CElevationFuser>(layerID_fusedLadarBumper, UTMNorthing, UTMEasting, receivedData);
// 					  }
					 					 
// 					  fusionCostPainter.paintChangesNotify(UTMNorthing, UTMEasting, overwrittenStatus, layerID_fusedLadarBumper, layerID_fusedYetTemp);

// 					}
// 					fusionCostPainter.paintNotifications(layerID_fusedYetTemp, layerID_fusedLadarBumper, layerID_fusedYetTemp);
// 					DGCunlockMutex(&m_mapMutex);
// 					_infoLadarFeeder.endProcessTimer(numreceived);
// 				}     
//       }
//     }
//   }
//   delete m_pLadarMapDelta; 
// }







// void FusionMapper::ReceiveDataThread_StereoShort() {
//   int mapDeltaSocket = m_skynet.listen(SNstereoShortDeltaMap, MODstereofeeder);
//   double UTMNorthing;
//   double UTMEasting;


//   CElevationFuser::STATUS overwrittenStatus = CElevationFuser::OK;

//    m_pStereoMapDelta = new char[MAX_DELTA_SIZE];
//   while(!_QUIT) { 
//     if(!_PAUSE) {
//       int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pStereoMapDelta, MAX_DELTA_SIZE, 0);
//       _infoStereoFeeder.startProcessTimer();
//       if(mapperOpts.optStereo) {
// 				if(numreceived>0) {
// 					DGClockMutex(&m_mapMutex);
// 					int cellsize = fusionMap.getDeltaSize(m_pStereoMapDelta);
// 					for(int i=0; i<cellsize; i++) {
// 					  CElevationFuser receivedData = fusionMap.getDeltaVal<CElevationFuser>(i, layerID_fusedStereoShort, m_pStereoMapDelta, &UTMNorthing, &UTMEasting);
// 					  if(mapperOpts.optAverage) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedStereoShort, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MeanElevation(receivedData);
// 					  } else if(mapperOpts.optMax) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedStereoShort, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MaxElevation(receivedData);
// 					  } else {
// 					    fusionMap.setDataUTM<CElevationFuser>(layerID_fusedStereoShort, UTMNorthing, UTMEasting, receivedData);
// 					  }
					 					 
// 					  fusionCostPainter.paintChangesNotify(UTMNorthing, UTMEasting, overwrittenStatus, layerID_fusedStereoShort, layerID_fusedYetTemp);

// 					}
// 					fusionCostPainter.paintNotifications(layerID_fusedYetTemp, layerID_fusedStereoShort, layerID_fusedYetTemp);
// 					DGCunlockMutex(&m_mapMutex);
// 					_infoStereoFeeder.endProcessTimer(numreceived);
// 				}     
//       }
//     }
//   }
//   delete m_pStereoMapDelta; 
// }


// void FusionMapper::ReceiveDataThread_StereoMedium() {
//   int mapDeltaSocket = m_skynet.listen(SNstereoMediumDeltaMap, MODstereofeeder);
//   double UTMNorthing;
//   double UTMEasting;


//   CElevationFuser::STATUS overwrittenStatus = CElevationFuser::OK;

//    m_pStereoMapDelta = new char[MAX_DELTA_SIZE];
//   while(!_QUIT) { 
//     if(!_PAUSE) {
//       int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pStereoMapDelta, MAX_DELTA_SIZE, 0);
//       _infoStereoFeeder.startProcessTimer();
//       if(mapperOpts.optStereo) {
// 				if(numreceived>0) {
// 					DGClockMutex(&m_mapMutex);
// 					int cellsize = fusionMap.getDeltaSize(m_pStereoMapDelta);
// 					for(int i=0; i<cellsize; i++) {
// 					  CElevationFuser receivedData = fusionMap.getDeltaVal<CElevationFuser>(i, layerID_fusedStereoMedium, m_pStereoMapDelta, &UTMNorthing, &UTMEasting);
// 					  if(mapperOpts.optAverage) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedStereoMedium, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MeanElevation(receivedData);
// 					  } else if(mapperOpts.optMax) {
// 					    CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedStereoMedium, UTMNorthing, UTMEasting);
// 					    overwrittenStatus = mapData.fuse_MaxElevation(receivedData);
// 					  } else {
// 					    fusionMap.setDataUTM<CElevationFuser>(layerID_fusedStereoMedium, UTMNorthing, UTMEasting, receivedData);
// 					  }
					 					 
// 					  fusionCostPainter.paintChangesNotify(UTMNorthing, UTMEasting, overwrittenStatus, layerID_fusedStereoMedium, layerID_fusedYetTemp);

// 					}
// 					fusionCostPainter.paintNotifications(layerID_fusedYetTemp, layerID_fusedStereoMedium, layerID_fusedYetTemp);
// 					DGCunlockMutex(&m_mapMutex);
// 					_infoStereoFeeder.endProcessTimer(numreceived);
// 				}     
//       }
//     }
//   }
//   delete m_pStereoMapDelta; 
// }





/*


void FusionMapper::ReceiveDataThread_Stereo() {
  int mapDeltaSocket = m_skynet.listen(SNstereodeltamap, MODstereofeeder);
  double UTMNorthing;
  double UTMEasting;
  int cellsize;

  m_pStereoMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      _infoStereoFeeder.startProcessTimer();

      RecvMapdelta(mapDeltaSocket, m_pStereoMapDelta, &cellsize);
      if(mapperOpts.optStereo) {
				int numreceived = cellsize;
				cellsize = fusionMap.getDeltaSize(m_pStereoMapDelta); 
				DGClockMutex(&m_mapMutex);
				for(int i=0; i<cellsize; i++) {
					CElevationFuser receivedData = fusionMap.getDeltaVal<CElevationFuser>(i, layerID_fusedElev, m_pLadarMapDelta, &UTMNorthing, &UTMEasting);
					if(mapperOpts.optAverage) {
						CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMNorthing, UTMEasting);
						mapData.fuse_MeanElevation(receivedData);
					} else if(mapperOpts.optMax) {
						CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMNorthing, UTMEasting);
						mapData.fuse_MaxElevation(receivedData);
					} else {
						fusionMap.setDataUTM<CElevationFuser>(layerID_ladarElev, UTMNorthing, UTMEasting, receivedData);
					}
					CElevationFuser currentData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMNorthing, UTMEasting);
					fusionMap.setDataUTM<double>(layerID_elev, UTMNorthing, UTMEasting, currentData.getMeanElevation());
					fusionCostPainter.paintChanges(UTMNorthing, UTMEasting, 1);
				}
				DGCunlockMutex(&m_mapMutex);
				_infoStereoFeeder.endProcessTimer(numreceived);
      }     
    }
    
  }
  delete m_pStereoMapDelta; 
}

*/
void FusionMapper::ReceiveDataThread_Road()
{
  int road_socket = m_skynet.listen(SNroad2map, SNroadfinding);

  bool first_road=true;
  int cur_road=0;
  Road road[2];
  CRoadPainter painter;

  printf("RoadThread started\n");

  int bytes=0;
  int cnt=0;

  while(!_QUIT) {
    //Read road data over skynet, make sure we always get the last transfer
    //so that we don't build up the buffert
    int received;
    do {
      received = m_skynet.get_msg(road_socket, &road[cur_road], sizeof(Road), 0);
      if(received < road[cur_road].data_size()) {
				printf("Error receiving road data\n");
				continue;
      }
      
      bytes +=received;
      ++cnt;
      //printf("Received %i messages, %i bytes, %i road points\n", cnt, bytes, road[cur_road].length);
    } while(m_skynet.is_msg(road_socket));
    
    if(received < road[cur_road].data_size())   //Invalid road data, get next package
      continue;
    
    _infoRoad.startProcessTimer();
    if(mapperOpts.optRoad) {      
      //Paint the trajectory into the map
      DGClockMutex(&m_mapMutex);
      //myCMap.clearLayer(layerNum[SNGUI_LAYER_ROAD]);

      if(!first_road) {
				painter.paint(road[cur_road], road[1-cur_road], &fusionMap, layerID_road, costFuser);
      } else {
				painter.paint(road[cur_road], &fusionMap, layerID_road, costFuser);
      }

      first_road=false;
      cur_road = 1-cur_road;
      
      DGCunlockMutex(&m_mapMutex);
      _infoRoad.endProcessTimer(received);
    }
  }
}

void FusionMapper::ReceiveDataThread_Supercon() {
  int mapDeltaSocket = m_skynet.listen(SNsuperConMapAction, MODsupercon);
  double UTMNorthing;
  double UTMEasting;

  int cellval;

  m_pSuperconMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      _infoSupercon.startProcessTimer();
      int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pSuperconMapDelta, MAX_DELTA_SIZE, 0);
      if(mapperOpts.optSupercon) {
				if(numreceived>0) {
					DGClockMutex(&m_mapMutex);	  
					// Begin cutting here?
					int cellsize = fusionMap.getDeltaSize(m_pSuperconMapDelta);
					for(int i=0; i<cellsize; i++) {
						cellval = fusionMap.getDeltaVal<int>(i, layerID_supercon, m_pSuperconMapDelta, &UTMNorthing, &UTMEasting);
						fusionMap.setDataUTM<int>(layerID_supercon, UTMNorthing, UTMEasting, cellval);
						//fusionCostPainter.fuseChanges(UTMNorthing, UTMEasting  );
					}

				
					DGCunlockMutex(&m_mapMutex);
					_infoSupercon.endProcessTimer(numreceived);
				}     
      }
    }
    usleep(100);
  }
  delete m_pSuperconMapDelta; 
}


void FusionMapper::PlannerListenerThread() { 
	int mapDeltaSocket = m_skynet.listen(SNfullmaprequest, SNplanner);
	int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
	bool bRequestMap;
	while(!_QUIT)
		{
			int numreceived =  m_skynet.get_msg(mapDeltaSocket, &bRequestMap, MAX_DELTA_SIZE, 0);
			//cout << "Received "  << numreceived << " Messages"  << endl; 
			if(numreceived>0) {
				DGClockMutex(&m_mapMutex);
				if(bRequestMap == true) {
					unsigned long long timestamp;
					DGCgettime(timestamp);
					CDeltaList* deltaList = fusionMap.serializeFullMapDelta<double>(layerID_costFinal, timestamp);

					if(!SendMapdelta(socket_num, deltaList))
						cerr << "FusionMapper::PlannerListenerThread(): couldn't send delta" << endl;
					else
						fusionMap.resetDelta<double>(layerID_costFinal);
				} 
				DGCunlockMutex(&m_mapMutex);
			}
		}
}


void FusionMapper::ReceiveDataThread_EStop() {
	int oldStatus = -1;

	while(!_QUIT) {
		WaitForNewActuatorState();
		if(m_actuatorState.m_estoppos == 2 && oldStatus != 2) {
			DGClockMutex(&m_mapMutex);
			fusionMap.clearMap();
			fusionCorridorPainter->repaintAll();
			//fusionCorridorReference.repaintAll();
			//fusionSampledCorridorPainter.repaintAll();
			//fusionCostPainter.repaintAll(1);
			DGCunlockMutex(&m_mapMutex);
		}
		oldStatus = m_actuatorState.m_estoppos;
	}
}


void FusionMapper::ReadConfigFile() {
  FILE* configFile = NULL;
  char fieldBuffer[512];
  char fieldName[256];
  char fieldValue[256];

  configFile = fopen(mapperOpts.configFilename, "r");
  if(configFile != NULL) {
    while(!feof(configFile)) {
      fgets(fieldBuffer, 512, configFile);
      if(strncmp(fieldBuffer, "%", 1)!=0) {
				sscanf(fieldBuffer, "%s %s\n", fieldName, fieldValue);
				if(strcmp(fieldName, "Max_Speed:")==0) {
					costFuserOpts.maxSpeed = atof(fieldValue);
				} else if(strcmp(fieldName, "RDDF_Speed_Scaling:")==0) {
					_corridorOptsReference.optRDDFscaling = atof(fieldValue);
				} else if(strcmp(fieldName, "Inside_RDDF_No_Data_Speed:")==0) {
					_corridorOptsFinalCost.optMaxSpeed = atof(fieldValue);
				} else {
					printf("%s [%d]: Unknown field '%s' with value '%s' - pausing\n", __FILE__, __LINE__, fieldName, fieldValue);
				}
      }
    }
    fclose(configFile);
  }

}


void FusionMapper::WriteConfigFile() {
  FILE* configFile = NULL;

  configFile = fopen(mapperOpts.configFilename, "w");
  if(configFile != NULL) {
    fprintf(configFile, "%% FusionMapper Aggressiveness Tuning Configuration File\n");
    fprintf(configFile, "RDDF_Speed_Scaling: %lf\n", _corridorOptsReference.optRDDFscaling);
    fprintf(configFile, "Inside_RDDF_No_Data_Speed: %lf\n", _corridorOptsFinalCost.optMaxSpeed);
    fprintf(configFile, "Max_Speed: %lf\n", costFuserOpts.maxSpeed);
    fclose(configFile);
  }
}
