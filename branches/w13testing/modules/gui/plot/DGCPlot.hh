/**
 * DGCPlot.hh
 * Revision History:
 * 08/01/2005  hbarnor  Created
 * $Id$
 */

#ifndef DGCPLOT_HH
#define DGCPLOT_HH


#include <gtkmm/fixed.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtkextra/gtkplot.h>
#include <gtkextra/gtkplotcanvas.h>
#include <gtkextra/gtkplotcanvasplot.h>
#include <gtkextra/gtkplotdata.h>
#include <gtkextra/gtkplotcanvastext.h>

#include "DGCPlotData.hh"

#define DGC_SYMBOL_SIZE 5 //used to be 10
#define DGC_SYMBOL_LINE_WIDTH  1 //used to be 2 - this is for teh symbol
#define DGC_LINE_WIDTH  1 //used to be 2

using namespace std;
/**
 * Customized Plot class for the Tab API. 
 * Based off the testrealtime code.
 */
class DGCPlot : public Gtk::Fixed
{
public:
  /**
   * Default constructor for the plot.
   * Constructor creates a plot window.
   */
  DGCPlot(GtkWidget* myParent );
  /**
   * Destructor - if it was not obvious.
   *
   */
  ~DGCPlot();
  /**
   * Add a pair of points to the plot. 
   * TODO
   */
  void addDataPoint(point newData);
  /**
   * Set the title of the plot. 
   * @param title - the title to be displayed. 
   * See this bugnote if the text does not show up properly.
   * http://bugzilla.gnome.org/show_bug.cgi?id=104341
   */
  void setTitle(string title);
  /**
   * Set the x-axis label. 
   * @param xLabel - the label to be displayed.
   */
  void setXLabel(string xLabel);
  /**
   * Set the y-axis label. 
   * @param yLabel - the label to be displayed.
   */
  void setYLabel(string yLabel);
  /**
   * Set the initial range and ticks of the graph.
   */
  void setXRange(double xMin, double xMax, double majorTick, int numMinorTicks);
  /**
   * Set the initial range and ticks of the graph.
   */
  void setYRange(double yMin, double yMax, double majorTick, int numMinorTicks);
  /**
   * Set autoscale to true or false.
   * @param autoscale - true if the graph should autoscale and false
   * otherwise.  
   * 
   */
  void setAutoScale(bool autoscale)
  {
    m_autoscale = autoscale;
  };
  /**
   * Displays a legend in the upper right window.
   * Displays a legend in the upper right window.Legends are off by
   * default.
   * @param display - true if a legend should be displayed. 
   */
  void displayLegend(bool display);
  /**
   * Adds text to the legend.
   * @param legend - the text to identify the legend.
   */
  void setLegend(string legend);
  
  
private:   // private member functions


private:   // private members  

  //gboolean update();
  bool m_autoscale;
  /**
   * The parent widget in which the plot is displayed.
   */
  GtkWidget * m_parent;
  /**
   * The canvas wrapper for the plot.
   */
  GtkPlotCanvasChild *child;
  /**
   * The canvas for displaying the title.
   */
  GtkPlotCanvasChild * titleChild;

  /**
   * Widget representing the current plot.
   */
  GtkWidget *plotInstance;
  /**
   * DND Canvas to hold the current plot.
   * This is the parent canvas in which everything is drawn. 
   */
  GtkWidget *canvas;
  /**
   * An instance of gdkcolor for setting colors.
   */
  GdkColor color;
  /**
   * The representation of the data being drawn. 
   */
  GtkPlotData *dataset;
  /**
   * Pointer array used to hold the xdata.
   */
  gdouble *px;
  /**
   * Pointer array used to hold the ydata.
   */
  gdouble *py;

};

#endif //DGCPLOT_HH
