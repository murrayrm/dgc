#ifndef PATHLIB_HH
#define PATHLIB_HH

// pathLib.hh contains all the helper functions for RddfPathGen
#include <iostream>
#include <fstream>

using namespace std;

#include <iomanip>
#include "vectorOps.hh"
#include "traj.h"

struct pathstruct
{
  /** 
   * number of points in the path.  The last point is the end point
   * of the path, so ve, vn, ae, and an are irrelevant at the last
   * point */
  unsigned int numPoints;

  /* the index of the point in the path that we were closest to
   * last time we checked.  it's necessary to keep track of this
   * to prevent errors due to the path crossing over.
   * The currentPoint is zero based */
  unsigned int currentPoint;
    
  /* eastings, northings, eastward velocity, northward velocity
   * eastward accel, northward accel */
  vector<double> e, n, ve, vn, ae, an;
};



struct corridorstruct
{
  /* number of points in corridor.  last point is just an endpoint. */
  unsigned int numPoints;
    
  /* eastings, northings, corridor widths, rddf imposed speed limits.
   * width[1] is the width of the corridor between (e[1], n[1]) and 
   * (e[2], n[2]).  speedLimit[1] is the speed limit imposed by the
   * rddf on the first corridor segment.  width[points] and
   * speedLimit[points] are irrelevant. */
  vector<double> e, n, width, speedLimit;
};


#include "RddfPathGen.hh"
//#include "trajDFE.hh"
#include <math.h>
#define PI 3.1415926535898





void printPath(pathstruct path);
pathstruct emptyPath();

void addToPath(pathstruct & path, double e, double n, double ve, double vn, double ae, double an);
void addToPath(pathstruct & path, double e, double n, vector<double> vel, double ae, double an);
void addToPath(pathstruct & path, double e, double n, vector<double> vel, vector<double> accel);
void MergePaths(pathstruct & path1, pathstruct & path2);

vector<double> GetLocation(pathstruct & path, unsigned int point);

void SetVel(pathstruct & path, unsigned int current_point, double velocity_desired);

void StoreWholePath(CTraj & traj, const corridorstruct & cor);
void StorePath(CTraj & traj, const pathstruct & path);
void StorePathFragment(CTraj & traj, const corridorstruct & cor);

void WritePath(const pathstruct & path, ostream & outstream);
void WritePathToFile(const pathstruct & path, char filename[]);

pathstruct DensifyAndChop(pathstruct&, vector<double>,double, double);
void UpdateCurrentPoint(pathstruct & path, vector<double> & vehicle_location);
double MoveCurrentPoint(pathstruct & path, double distance_goal);
double DistanceBetweenPoints(pathstruct & path, unsigned int point1, unsigned int point2);

pathstruct ChopPath(const pathstruct & path, 
		    const double chop_behind, const double chop_ahead);
pathstruct DensifyPath(const pathstruct & path_sparse);

pathstruct SplineFromHereToThere(double here_n, double here_n_dot,
				 double here_e, double here_e_dot,
				 double there_n, double there_n_dot,
				 double there_e, double there_e_dot,
				 double density);

pathstruct PathFromLocation(VehicleState state);

double length_limited_curve(double & theta, double & start_x, double & start_y, corridorstruct corridor_segment);
double width_limited_curve(double & theta, double & start_x, double & start_y, corridorstruct corridor_segment);
vector<double> curve2accel(corridorstruct corridor_segment, double r,double v);

pathstruct Path_From_Corridor(corridorstruct corridor);



#endif //PATHLIB_HH
