#include "rddf.hh"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fstream>

int main(int argc, char **argv) {
  string line;
  double desiredLat, desiredLon;
  int waypoint;
  double lat, lon, offset, speed;
  char input[50];
  
  if (argc != 2) {
    cerr << "Syntax: waypointSearch infile" << endl;
    exit(1);
  }

  ifstream fileIn;
  fileIn.open(argv[1], ios::in);

  cout << "Enter the latitude degrees (no minutes), south is negative!: " << endl;
  scanf("%s", input);
  desiredLat = atof(input);

  cout << "Enter the latitude minutes: " << endl;
  scanf("%s", input);
  if (desiredLat > 0) {
    desiredLat += atof(input)/60.0;
  } else {
    desiredLat -= atof(input)/60.0;
  }

  cout << "Enter the longitude degrees (no minutes), west is negative!: " << endl;
  scanf("%s", input);
  desiredLon = atof(input);

  cout << "Enter the longitude minutes: " << endl;
  scanf("%s", input);
  if (desiredLon > 0) {
    desiredLon += atof(input)/60.0;
  } else {
    desiredLon -= atof(input)/60.0;
  }

  int i = 0;
  while(!fileIn.eof()) {
    // Read in value.
    if(i == 4) { // last element in RDDF line
      getline(fileIn,line,'\n');
    } else {
      getline(fileIn,line,RDDF_DELIMITER);
    }

    // Drop blank lines
    if (line.empty()) {
      continue;
    }
    if(i == 0) {
      waypoint = atoi(line.c_str()); 
    } else if(i == 1) {
      lat = atof(line.c_str());
    } else if(i == 2) {
      lon = atof(line.c_str());
    } else if(i == 3) {
      offset = atof(line.c_str());
    } else if(i == 4) {
      speed = atof(line.c_str());
      if ((lat - desiredLat < 0.0002) && (lat - desiredLat > -0.0002) 
	  && (lon - desiredLon < 0.0002) && (lon - desiredLon > -0.0002)) {
	cout << "Waypoint: " << waypoint << " Lat: " << lat
	     << " Lon: " << lon << " Offset: " << offset
	     << " Speed: " << speed << endl;
      }
    }

    // Iterate the item being looked at
    i = (i + 1)%5;
  }
  fileIn.close();
  return 0;
}

