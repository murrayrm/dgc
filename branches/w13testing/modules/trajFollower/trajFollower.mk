TRAJFOLLOWER_PATH = $(DGC)/modules/trajFollower

TRAJFOLLOWER_DEPEND_LIBS = $(SPARROWHAWKLIB) \
                           $(SPARROWLIB) \
                           $(TRAJLIB) \
                           $(PIDCONTROLLERLIB) \
                           $(CPIDLIB) \
                           $(SKYNETLIB) \
                           $(MODULEHELPERSLIB) \
                           $(FINDFORCELIB) \
                           $(ADRIVELIB) \
                           $(TIMBERLIB) \
                           $(RDDFLIB) \
                           $(RDDFPATHGENLIB) \
			   $(SUPERCONCLIENTLIB)

TRAJFOLLOWER_DEPEND_SOURCE = \
                           $(TRAJFOLLOWER_PATH)/TrajFollower.hh \
                           $(TRAJFOLLOWER_PATH)/TrajFollowerMain.cc \
                           $(TRAJFOLLOWER_PATH)/sparrow.cc \
                           $(TRAJFOLLOWER_PATH)/fwd_trajF_sparrow.dd \
                           $(TRAJFOLLOWER_PATH)/rev_trajF_sparrow.dd \
                           $(TRAJFOLLOWER_PATH)/StateForH.cc \
                           $(TRAJFOLLOWER_PATH)/StateForH.hh \
                           $(TRAJFOLLOWER_PATH)/gpsCap.cc \
                           $(TRAJFOLLOWER_PATH)/gpsCap.hh \
                           $(TRAJFOLLOWER_PATH)/trajFollowerTabSpecs.hh

TRAJFOLLOWER_DEPEND = $(TRAJFOLLOWER_DEPEND_LIBS) \
                      $(TRAJFOLLOWER_DEPEND_SOURCE)
