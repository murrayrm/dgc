#include <unistd.h>

#include <sys/time.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include "trajFollowerTabSpecs.hh"
using namespace std;

#include "TrajFollower.hh"
#include "sn_msg.hh"
#include "adrive_skynet_interface_types.h"
#include "DGCutils"
#include "find_force.hh"

int QUIT_PRESSED = 0;

// These extern's relate to the '--' command line options for TrajFollower (see TrajFollowerMain.cc for set-up details)
extern int           NOSTATE;
extern int           NODISPLAY;
extern int           USEMODEMAN;       // Use ModeManModule
extern int           SIM;
extern unsigned long SLEEP_TIME;
extern int           A_HEADING_REAR;
extern int           A_HEADING_FRONT;
extern int           A_YAW;
extern int           Y_REAR;
extern int           Y_FRONT;
extern int           LATERAL_FF_OFF;
extern double        HACK_STEER_COMMAND;
extern bool          USE_HACK_STEER;
extern int           USE_DBS;


#define MIN_SPEED 0.01


/******************************************************************************/
/******************************************************************************/
TrajFollower::TrajFollower(int sn_key, char* pTrajfile, char* pLindzey) 
  : CSkynetContainer(SNtrajfollower, sn_key), CTimberClient(timber_types::trajFollower), CModuleTabClient(&m_input, &m_output), CSuperConClient("TJF")
{
  RDDFData firstpoint;
  cout<<"Starting TrajFollower(...)"<<endl;

  logs_enabled = getLoggingEnabled();
  //logs_enabled = 1;
  if(logs_enabled)
    { 
      logs_location = getLogDir();
    } else 
      {
	logs_location = "logs/";
      }

  lindzey_logs_location = string(pLindzey);
  
  logs_newDir = checkNewDirAndReset();
  //cout<<"logs_newDir = "<<logs_newDir<<endl;
  //cout<<"logs_enabled = "<<logs_enabled<<endl;
  //logs_newDir = 1;
  //setupLogFiles();

  

  DGCcreateMutex(&m_trajMutex);
  DGCcreateMutex(&m_speedCapMutex);
  DGCcreateMutex(&m_reverseMutex);
  DGCcreateMutex(&m_historyMutex);

  m_drivesocket = m_skynet.get_send_sock(SNdrivecmd);

  m_trajCount = 0;
  m_nActiveCount = 0;
  m_steer_cmd = 0.0;
  m_accel_cmd = 0.0;
  DGCgettime(m_timeBegin);


  EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
  EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : ( A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));

  m_pPIDcontroller = new CPID_Controller(yerrorside,
					 aerrorside,
					 LATERAL_FF_OFF == 0, logs_location, lindzey_logs_location, false);
  reversecontroller = new CPID_Controller(YERR_BACK,
					  aerrorside,
					  LATERAL_FF_OFF == 0, logs_location, lindzey_logs_location, true);

  cout<<"FINISHED CONSTRUCTING CONTROLLER"<<endl;

  DGCgettime(grr_timestamp);

  UpdateState(grr_timestamp,true);
  
  firstpoint.number = 1;
  firstpoint.Northing = m_state.Northing;
  firstpoint.Easting = m_state.Easting;
  firstpoint.maxSpeed = RSPEED;
  firstpoint.radius = 10;
  history[0] = firstpoint;
  historysize = 1;
  latestpoint = 0;

  reverse = 0;
  distancelefttoreverse = 0;

 
  // 3rd order traj. If we've a static path, use it
  if(pTrajfile[0] == '\0')
    {
      m_pTraj   = new CTraj(3);
      cout<<"no static path"<<endl;
    }
  else
    {
      m_pTraj   = new CTraj(3, pTrajfile);
      //cout<<"ln81 TF.cc, creating new trajectory with file: "<<pTrajfile<<endl;
      //need to set path, cuz in main function only reset when
      //new traj is received.  		
      m_pPIDcontroller->SetNewPath(m_pTraj, &m_state);
    }

  // print data format information in a header at the top of the file
  // Note that this header now reflects the or
  //  cout << "% Time[sec] | Northing[m] | Easting[m] | Altitude[m]"
  //       << " | Vel_N[m/s] | Vel_E[m/s] | Vel_D[m/s]"
  //       << " | Acc_N[m/s] | Acc_E[m/s] | Acc_D[m/s]"
  //       << " | Roll[rad] | Pitch[rad] | Yaw[rad]"
  //       << " | RollRate[rad/s] | PitchRate[rad/s] | YawRate[rad/s]"
  //       << " | RollAcc[rad/s] | PitchAcc[rad/s] | YawAcc[rad/s]"

  // leave this at the end 

  cout<<"TrajFollower(...) Finished"<<endl;
}

TrajFollower::~TrajFollower() 
{
  delete m_pTraj;
  delete m_pPIDcontroller;
  delete reversecontroller;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollower::Active() 
{
  // timers to make sure our control frequency is what we set (not
  // affected by how long it takes to compute each cycle)
  unsigned long long timestamp1, timestamp2;

  m_speedCapMax = 100.0;
  m_speedCapMin = -1.0;


  cout<< "Starting Active Function" <<endl;

  drivecmd_t my_command;
  memset(&my_command, 0, sizeof(my_command)); // init the cmd to 0 to appease the memprofiler gods
  if(logs_enabled)
    {
      setupLogFiles();
    }

  while(!QUIT_PRESSED) 
    {
      // time checkpoint at the start of the cycle
      DGCgettime(timestamp1);

      // increment module counter
      m_nActiveCount++;

      //check logging status
      logs_enabled = getLoggingEnabled(); 
      //logs_enabled = 1;
      // cout<<"are logs enabled?:"<<logs_enabled<<endl;
      logs_newDir = checkNewDirAndReset();
      if(logs_newDir) {
	//	cout<<"new logging directory! "<<endl;
	setupLogFiles();
      }

      // Get state from the simulator or vstate 
      /* This sets m_state */



  DGCgettime(grr_timestamp);

  UpdateState(grr_timestamp,true);



      //Calculate the required vehicle speed
      m_Speed2 = m_state.Speed2();
      //Calculate angular values in degrees
      m_PitchDeg = ( ( m_state.Pitch / M_PI ) * 180 );
      m_RollDeg = ( ( m_state.Roll / M_PI ) * 180 );
      m_YawDeg = ( ( m_state.Yaw / M_PI ) * 180 );
      m_PitchRateDeg = ( ( m_state.PitchRate / M_PI ) * 180 );
      m_RollRateDeg = ( ( m_state.RollRate / M_PI ) * 180 );
      m_YawRateDeg = ( ( m_state.YawRate / M_PI ) * 180 );


      UpdateActuatorState();
      //cout << "trans is: " << m_actuatorState.m_transpos << " estop is: " << m_actuatorState.m_estoppos << endl;
      //cout<< "trans is: %d, estop is: %d", m_actuatorState.m_transpos,  m_actuatorState.m_estoppos<<endl;
      UpdateReverse();

      /** if our speed was too low, set it as if we're moving in the same direction
       * at the minimum speed WITH THE WHEELS POINTED STRAIGHT */
      if( m_state.Speed2() < MIN_SPEED )
	{
	  m_state.Vel_N = MIN_SPEED * cos(m_state.Yaw);
	  m_state.Vel_E = MIN_SPEED * sin(m_state.Yaw);
	}

      /* Compute the control inputs. */


      if(reverse != 0)
	{
	  DGClockMutex(&m_trajMutex);
	  reversecontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, logs_enabled, logs_location, logs_newDir);
	  DGCunlockMutex(&m_trajMutex);
	}
      else
	{
	  DGClockMutex(&m_trajMutex);
	  DGClockMutex(&m_speedCapMutex);
	  m_pPIDcontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, logs_enabled, logs_location, logs_newDir,m_speedCapMin,m_speedCapMax);
	  DGCunlockMutex(&m_trajMutex);
	  DGCunlockMutex(&m_speedCapMutex);
	}

      //cout<<"got control variables!"<<endl;


      double steer_Norm, accel_Norm;
		
      steer_Norm = m_steer_cmd/VEHICLE_MAX_AVG_STEER;
      steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);

#ifdef NO_ACC_FF
      //bounding accelNorm by -1 and 1
      accel_Norm = max(-1.0, min(m_accel_cmd, 1.0));
#else
      accel_Norm = get_pedal(m_state.Speed2(), m_accel_cmd*VEHICLE_MASS, 1);
#endif

      if(USE_HACK_STEER == true)
	{
	  steer_Norm = HACK_STEER_COMMAND;
	  accel_Norm = 0;
	}

      my_command.my_command_type = set_position;

      my_command.my_actuator = steer;
      my_command.number_arg = steer_Norm;
      m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
      my_command.my_actuator = accel;
      my_command.number_arg = accel_Norm;
      m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
      //my_command.my_actuator = trans;
      //my_command.number_arg = (double)(1-2*fmin(reverse,1));
      //m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
      //my_command.my_actuator = estop;
      //my_command.number_arg = 2;
      //m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
 
      //TODO wait until we get to the end

      statusComm();
      // time checkpoint at the end of the cycle
      DGCgettime(timestamp2);

      /** now sleep for SLEEP_TIME usec less the amount of time the
       * computation took */
      int delaytime = SLEEP_TIME - (timestamp2 - timestamp1);
      if(delaytime > 0)
	usleep(delaytime);


    } // end while(!QUIT_PRESSED) 

  cout<< "Finished Active state" <<endl;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollower::Comm()
{
  int trajSocket;
  //int bytesLeft;
  //int bytesReceived;
  //int bytesToReceive;
  //char *pInTraj;
  //char* m_pDataBuffer;
  //int i;

  /** determines what type of trajs to listen for */
#ifdef USE_SNRDDFTRAJ
  trajSocket = m_skynet.listen(SNRDDFtraj, SNRddfPathGen);
#elif USE_SNREACTIVETRAJ
  trajSocket = m_skynet.listen(SNreactiveTraj, SNreactive);
#elif USE_SNROADFINDINGTRAJ
  trajSocket = m_skynet.listen(SNroadFinding, SNroadfinding);
#elif USE_SNSTATICTRAJ
  trajSocket = m_skynet.listen(SNstaticTraj, SNstaticpainter);
#else
  trajSocket = m_skynet.listen(SNtraj, SNplanner);
#endif
  
  while(!QUIT_PRESSED)
    {
      RecvTraj(trajSocket, m_pTraj);
	
      /** commenting this out as we're logging plans WAY too quickly.
      ** will make this an option soon, this is just a quick fix
      m_pTraj->print(m_outputPlans);
      m_outputPlans << endl;
      m_outputPlanStarts << m_pTraj->getNorthing(0) << ' '
      << m_pTraj->getEasting(0) << endl;
      */

      if(reverse != 0)
	{
	  usleep(1);
	}
      else
	{
	  //printf("PID with default traj\n");
	  DGClockMutex(&m_trajMutex);
	  m_pPIDcontroller->SetNewPath(m_pTraj, &m_state);
	  ReverseTrajGen(MAXHISTORYSIZE);
	  DGCunlockMutex(&m_trajMutex);
	}

      m_trajCount++;
    }
}


void TrajFollower::superconComm()
{
  int bytesReceived;
  int bytesToReceive;
  char* m_pDataBuffer;
  superConTrajFcmd* command;
  int reverseSocket; 
  //double distancetogo;
  //int endpoint;
  //RDDFData firstpoint;
  //CTraj traj;
  reverseSocket = m_skynet.listen(SNsuperconTrajfCmd, MODsupercon);
  //cout <<"init superconcomm on socket: " << reverseSocket << endl;
  bytesToReceive = sizeof(superConTrajFcmd);
  m_pDataBuffer = new char[bytesToReceive];

  while(!QUIT_PRESSED)
    {
      bytesReceived = m_skynet.get_msg(reverseSocket, m_pDataBuffer, bytesToReceive, 0);
      command = (superConTrajFcmd*)m_pDataBuffer;
      //printf("Got a: %d\n", (int)*m_pDataBuffer);
      if(bytesReceived != bytesToReceive)
	{
	  cerr << "Trajfollower::superconComm(): skynet error" << endl;
	}
      else if((*command).commandType == tf_forwards) //go forward again;
	{
	  //printf("got a forward!\n");
	  DGClockMutex(&m_reverseMutex);
	  reverse = 0;
	  //printf("reverse is: %d\n", reverse);
	  DGCunlockMutex(&m_reverseMutex);
	}
      else if(((*command).commandType == tf_reverse)&& ((*command).distanceToReverse > 0)) // we need to reverse
	{
	  //Do not allow mode-changes *unless* Alice is e-stop paused
	  if( m_actuatorState.m_estoppos == ESTP_PAUSE ) {
	    
	    //printf("got a backward: %d\n", (int)*m_pDataBuffer);
	    DGClockMutex(&m_reverseMutex);
	    reverse = (*command).distanceToReverse;
	    //printf("reverse is: %d\n", reverse);
	    //TODO adjust gains
	    reversetraj = ReverseTrajGen(reverse);
	    reversecontroller->SetNewPath(&reversetraj, &m_state);
	    DGCunlockMutex(&m_reverseMutex);

	  } else {
	    cout<< "ERROR: Mode change request received when Alice was NOT e-stop paused"<<endl;;
	    cerr << "ERROR: Mode change request received when Alice was NOT e-stop paused" << endl;
	  }
	}
      else
	{
	  cerr << "Trajfollower::superconComm(): invalid supercon message" << endl;
	}
      //printf("looping superconComm\n");
    }
  delete m_pDataBuffer;
}


void TrajFollower::statusComm()
{
  trajFstatusStruct status;
  int superconSocket;
  superconSocket = m_skynet.get_send_sock(SNtrajFstatus);
  if(reverse == 0)
    {
      status.currentMode = tf_forwards;
    }
  else
    {
      status.currentMode = tf_reverse;
    }
  
  DGClockMutex(&m_trajMutex);
  status.currentVref = m_pPIDcontroller->access_VRef();
  DGCunlockMutex(&m_trajMutex);
  
  DGClockMutex(&m_speedCapMutex);
  status.largestMINspeedCap = m_speedCapMin;
  status.smallestMAXspeedCap = m_speedCapMax;
  DGCunlockMutex(&m_speedCapMutex);
  
  if(m_skynet.send_msg(superconSocket, &status, sizeof(status), 0) != sizeof(status))
    {
      cerr << "TrajFollower::statusCom: error: msg size does not match send msg size" << endl;
    }
}


void TrajFollower::speedCapComm()
{
  int bytesReceived;
  int bytesToReceive;
  newSpeedCapCmd m_DataBuffer;
  newSpeedCapCmd* m_pDataBuffer = &m_DataBuffer;
  int speedCapSocket; 
  double* module_status;

  //second argument ignored
  speedCapSocket = m_skynet.listen(SNtrajFspeedCapCmd, MODsupercon);
  //cout <<"init speedcapcomm on socket: " << speedCapSocket << endl;
  bytesToReceive = sizeof(newSpeedCapCmd);
  //m_pDataBuffer = new newSpeedCapCmd;

  //for each module, I'll need to store two values: curr min/max speed limits
  int num_modules = (int)NUM_SENDING_MODULES;
  module_status = new double[2*num_modules];

  //initialize array to -1.0 to represent no command
  for(int bar=1; bar <= num_modules; bar++) {
    module_status[2*bar-2] = -1.0;
    module_status[2*bar-1] = 100.0;
  }

  while(!QUIT_PRESSED)
    {
      //      cout<<"waiting for message..."<<endl;
      bytesReceived = m_skynet.get_msg(speedCapSocket, m_pDataBuffer, bytesToReceive, 0);
      ofstream outfil("recvd");
      outfil.write((char*)m_pDataBuffer, 16);
      outfil.close();
      cout<<"got a speedcap message"<<endl;
      //cout<<"happiness! I got a speedCap msg!"<<endl;
      //cout<<"received from module: "<<m_pDataBuffer->m_speedCapSndModule<<endl;
      //cout<<"received action: "<<m_pDataBuffer->m_speedCapAction<<endl;
      //cout<<"requested setting: "<<m_pDataBuffer->m_speedCapArgument<<endl;
      if((!USE_DBS) && (m_pDataBuffer->m_speedCapSndModule == TF_MOD_DBS))
	{
	  continue;
	}
      if(bytesReceived <= 0)
	{
	  cerr << "Trajfollower::speedCapComm(): skynet error" << endl;
	}
      else if(m_pDataBuffer->m_speedCapAction == add_max_speedcap)
	{
	  //don't need mutex cuz no other threads will be accessing this array =)
	  //DGClockMutex(&m_speedCapMutex);
          double val = m_pDataBuffer->m_speedCapArgument;
	  //cout << "setting max to: "<< val << endl;
           module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)+1] = val;
	  //DGCunlockMutex(&m_speedCapMutex);
	}

      else if(m_pDataBuffer->m_speedCapAction == add_min_speedcap)
	{
	  //DGClockMutex(&m_speedCapMutex);
double minval = m_pDataBuffer->m_speedCapArgument;
//	  cout<<"a setting min to: " << minval << endl;
	  module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)] = minval;
	  //DGCunlockMutex(&m_speedCapMutex);
	}

      else if(m_pDataBuffer->m_speedCapAction == remove_max_speedcap)
	{
	  //	  cout<<"remove_max_speedcap"<<endl;
	  //DGClockMutex(&m_speedCapMutex);
	  module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)+1] = 100.0;
	  //DGCunlockMutex(&m_speedCapMutex);
	}

      else if(m_pDataBuffer->m_speedCapAction == remove_min_speedcap)
	{
	  //	  cout<<"remove_min_speedcap"<<endl;
	  //DGClockMutex(&m_speedCapMutex);
	  module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)] = -1.0;
	  //DGCunlockMutex(&m_speedCapMutex);
	}
      
      double max = 100;
      double min = -1;
      
      //DGClockMutex(&m_speedCapMutex);
      
      //cout<<" calculating min and max"<<endl;
      for(int foo = 0; foo < num_modules; foo++)
	{
	  
	  //find max of the min speed caps
	  if (module_status[2*foo] > min)
	    min = module_status[2*foo];
	  
	  //find min of the max speed caps
	  if(module_status[2*foo+1] < max)
	    max = module_status[2*foo+1];
	  
	}
      //cout<<"min and max: " << min << ' ' << max << endl;
      //DGCunlockMutex(&m_speedCapMutex)
      //sets speed limit to value computed again
      
      DGClockMutex(&m_speedCapMutex);
      //cout<<"unlocked the mutex"<<endl;
      m_speedCapMin = min;
      m_speedCapMax = max;
      
      DGCunlockMutex(&m_speedCapMutex);
    }
  delete m_pDataBuffer;

}    


CTraj TrajFollower::ReverseTrajGen(double distance)
{
  //This code was shamelessly cut and pasted from RDDFPathGen
  CTraj ptraj;
  corridorstruct corridor_whole;
  vector<double> location(2);

  DGCgettime(grr_timestamp);

  UpdateState(grr_timestamp,true);


  //  UpdateState();
  location[0] = m_state.Northing;
  location[1] = m_state.Easting;
  int socket;
  int index;
  int firstpoint;
  if(latestpoint - historysize+1<0)
    {
      firstpoint = latestpoint - historysize + 1 + MAXHISTORYSIZE;
    }
  else
    {
      firstpoint = latestpoint - historysize + 1;
    }
  double historylength = sqrt(pow(m_state.Northing - history[firstpoint].Northing ,2) + pow(m_state.Easting - history[firstpoint].Easting ,2));
  socket =  m_skynet.get_send_sock(SNtrajReverse);
  
  if(distance > historylength)
    {
      //cout << "asked to plan too far backwards(" <<distance << "), setting distance to historylength" << endl;
      distance = historylength;
    }
  if(historysize <2)
    {
      //cout << "can't backup less than 1 meter" << endl;
      scMessage((int)completed_reversing_action);
      return ptraj;
    }
  //printf("ReverseTrajGen called. Distance is: %d\n", distance);

  // store data in corridor
  corridor_whole.numPoints = historysize;
  //cout << "historysize is: " << historysize << " latestpoint is: " << latestpoint << endl;
  int i;
  for( i = 0; i < historysize; i++)
    {
      if(latestpoint - i <0)
	{
	  index = latestpoint - i + MAXHISTORYSIZE;
	}
      else
	{
	  index = latestpoint - i;
	}
      //cout << "index is: " << index << endl;
      
      // shouldn't get any segfaults here, but who knows...
      corridor_whole.e.push_back(history[index].Easting);
      corridor_whole.n.push_back(history[index].Northing);
      //shrink the corridor width to account for vehicle width, tracking error
      corridor_whole.width.push_back(history[index].radius);
      corridor_whole.speedLimit.push_back(history[index].maxSpeed);
      //cout<<"N: %d, Easting: %f, latestpoint: %d, dist: %d", i, corridor_whole.e[i], latestpoint, distance<<endl;
    }
  pathstruct& path_whole_sparse = *(new pathstruct);
  path_whole_sparse = Path_From_Corridor(corridor_whole);
  
  path_whole_sparse.currentPoint = 0; // we haven't went anywhere yet, so we're still at the start of the path
	  
  // get the chopped dense traj (the one we send over skynet), in path format
  pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location, 0, distance + VEHICLE_AXLE_DISTANCE);
  //pathstruct path_chopped_dense = path_whole_dense;  /**< send whole each time */
  StorePath(ptraj, path_chopped_dense);
  SendTraj(socket, &ptraj);
  //printf("ReverseTrajGen exiting\n");
  delete &path_whole_sparse;
  return ptraj;
}

void TrajFollower::setupLogFiles()
{
  //cout<<"Entered setupLogFiles"<<endl;
  if(1) {
    //cout<<"new directory, resetting outptu files"<<endl;
    logs_location = getLogDir();

    char plansFileName[256];
    char testFileName[256];
	
     char plansFilePath[256];
    char testFilePath[256];

    sprintf(plansFileName, "plans.dat");

    //cout<<"trying to make stateFilePath"<<endl;
    string temp;
    temp = logs_location + plansFileName;
    strcpy(plansFilePath, temp.c_str());

    //    cout<<"plansFilePath = "<<plansFilePath<<endl;

     
      m_outputPlans << setprecision(20);

    sprintf(testFileName, "test.dat");

    temp = logs_location + testFileName;
    strcpy(testFilePath, temp.c_str());

       m_outputTest << setprecision(20);
	
  }
}

void::TrajFollower::UpdateReverse()
{
  int endpoint;
  int pointstoremove;
  RDDFData newpoint;
  RDDFData firstpoint;

  if ((sqrt(pow((m_state.Northing - history[latestpoint].Northing),2) + pow((m_state.Easting - history[latestpoint].Easting),2))> HISTORYSPACING) && (reverse==0))
    {
      newpoint.Northing = m_state.Northing;
      newpoint.Easting = m_state.Easting;
      newpoint.number = 0;
      newpoint.radius = 10;
      newpoint.maxSpeed = RSPEED;
      /*
      for(int i = historysize; i > 0; i--)
	{
	  if(i != MAXHISTORYSIZE)
	    {
	      history[i] = history[i-1];
	      history[i].number = i;
	    }
	}
      */
      DGClockMutex(&m_historyMutex);
      if(latestpoint == MAXHISTORYSIZE-1)
	{
	  latestpoint = 0;
	}
      else
	{
	  latestpoint++;
	} 
      history[latestpoint] = newpoint;
      if(historysize < MAXHISTORYSIZE)
	{
	  historysize++;
	}
      DGCunlockMutex(&m_historyMutex);
    }

  if(reverse != 0)
    {
      distancelefttoreverse = sqrt(pow(m_state.Northing - reversetraj.lastN() ,2) + pow(m_state.Easting - reversetraj.lastE(),2))- 2*VEHICLE_AXLE_DISTANCE;
      //printf("waiting until end of traj, dist is: %f, point is: %f, %f\n",distancelefttoreverse, reversetraj.lastN(), reversetraj.lastE());
      if(m_accel_cmd < -4.5) //TODO need to do "and stopped" or something. and find a good distance
	{
	  //printf("got to end of traj\n");
	  endpoint = 0;
	  for(int i=0;i<historysize;i++) //find the closest point in the history
	    {
	      if(sqrt(pow(history[endpoint].Northing - m_state.Northing ,2) + pow(history[endpoint].Easting - m_state.Easting,2)) > sqrt(pow(history[i].Northing - m_state.Northing ,2) + pow(history[i].Easting - m_state.Easting ,2)))
		{
		  endpoint=i;
		}
	    }
	  if(latestpoint - endpoint < 0)
	    {
	      pointstoremove = latestpoint - endpoint + MAXHISTORYSIZE;
	    }
	  else
	    {
	      pointstoremove = latestpoint - endpoint;
	    }
	  
  DGCgettime(grr_timestamp);

  UpdateState(grr_timestamp,true);

  //	  UpdateState();
	  firstpoint.number = 1;
	  firstpoint.Northing = m_state.Northing;
	  firstpoint.Easting = m_state.Easting;
	  firstpoint.maxSpeed = RSPEED;
	  firstpoint.radius = 10;

	  DGClockMutex(&m_historyMutex);
	  latestpoint = endpoint; 
	  history[latestpoint] = firstpoint; //set latestpoint to where we are now
	  historysize = historysize - pointstoremove;
	  DGCunlockMutex(&m_historyMutex);
	  
	  //TODO message supercon, wait for reply then reverse =0
	  //sleep(10);
	  //printf("commanding forward again\n");

	  scMessage((int)completed_reversing_action);

	  //DGClockMutex(&m_reverseMutex);
	  //reverse = 0;
	  //DGCunlockMutex(&m_reverseMutex);
	}
      
    }
  else
    {
      distancelefttoreverse = 0;
    }
}

// end TrajFollower.cc
