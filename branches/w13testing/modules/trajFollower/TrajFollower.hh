#ifndef TRAJFOLLOWER_HH
#define TRAJFOLLOWER_HH


/***** DEFINE STATEMENTS TO CHANGE WHICH MODULE TO LISTEN TO *****/
//#define USE_SNRDDFTRAJ
//#define USE_SNREACTIVETRAJ
//#define USE_SNROADFINDINGTRAJ
//#define USE_SNSTATICTRAJ

#define MAXHISTORYSIZE 200
#define HISTORYSPACING 1
#define RSPEED 1

//estop defines required (used to check whether Alice is paused, which
//is a requirement in order to change trajFollower's mode (Forwards/Reverse)
#define ESTP_PAUSE 1
#define ESTP_RUN 2

#include "StateClient.h"
#include "TrajTalker.h"
#include "CTimberClient.hh"
#include <pthread.h>
#include <fstream>
#include "trajFollowerTabSpecs.hh"
#include "TabClient.h"
#include "rddf.hh"
#include "pathLib.hh"
#include "CPath.hh"
#include "TrajTalker.h"
#include "AliceConstants.h"
#include "trajF_speedCap_cmd.hh"
#include "trajF_status_struct.hh"
#include "interface_superCon_trajF.hh"
#include "SuperConClient.hh"

using namespace std;
using namespace superCon;

#include "traj.h"
#include "PID_Controller.hh"

class TrajFollower : public CStateClient, public CTrajTalker, public CTimberClient, public CModuleTabClient, public CSuperConClient
{
  /*! The steer command to send to adrive (in SI units). */
  double m_steer_cmd;

  /*! The accel command to send to adrive (in SI units). */
  double m_accel_cmd;

  unsigned long long grr_timestamp;

  /*! The trajectory controller. */
  CPID_Controller* m_pPIDcontroller;
  CPID_Controller* reversecontroller;

  /*! how many times the while loop in the Active function has run.  */
  int m_nActiveCount;

  /*! The number of trajectories we have received. */
  int m_trajCount;

  /*! The current trajectory to follow. */
  CTraj* m_pTraj;

  /*! Time that module started. */
  unsigned long long m_timeBegin; 

  /*! min and max speed caps */
  double m_speedCapMin, m_speedCapMax;

  /*! Speed - 2D [m/s], (for sparrowHawk display)  */
  double m_Speed2;
  
  /*! Angular state variables in DEGREES (converted from radians) */
  double m_PitchRateDeg, m_RollRateDeg, m_YawRateDeg, m_PitchDeg, m_RollDeg, m_YawDeg;

  /*! vector of position history. */
  RDDFData history[MAXHISTORYSIZE];
  int historysize;
  int latestpoint;
  CTraj reversetraj;
  double distancelefttoreverse;
  double reverse; // distance to reverse

  // vars related to timber
  bool logs_enabled;
  string logs_location;
  string lindzey_logs_location;
  bool logs_newDir;


  /*! Logs to output the plans and the state */
  ofstream m_outputPlans, m_outputTest;

	/*! The skynet socket for the communication with drive */
	int m_drivesocket;

	/*! The mutex to protect the traj we're following, and the speedCap calculations */
	pthread_mutex_t	m_trajMutex, m_speedCapMutex, m_reverseMutex, m_historyMutex;

	/*! The skynet socket for receiving trajectories */
	int m_trajSocket;
 
  /*!Used for guiTab.*/
  StrajfollowerTabInput  m_input;
  StrajfollowerTabOutput m_output;

public:
  TrajFollower(int sn_key, char* pTrajfile, char* pLindzey);
  ~TrajFollower();

  void Active();

  void Comm();

  void superconComm();

  void speedCapComm();

  void statusComm();

  void setupLogFiles();

  void Init_SparrowDisplayTable();
  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

  double access_SmallestMAXspeedC() { return m_speedCapMax; }
  double access_LargestMINspeedC() { return m_speedCapMin; }

  double access_PitchRateDeg() { return m_PitchRateDeg; }
  double access_RollRateDeg() { return m_RollRateDeg; }
  double access_YawRateDeg() { return m_YawRateDeg; }
  double access_PitchDeg() { return m_PitchDeg; }
  double access_RollDeg() { return m_RollDeg; }
  double access_YawDeg() { return m_YawDeg; }


private:

  CTraj ReverseTrajGen(double distance);
  void UpdateReverse();
  
};

#endif
