/**
 * StereoClient.cc
 * Player client for stereo cameras
 *
 * Revision History:
 * 02/22/05 hbarnor,ryanf created
 */

#include "StereoClient.hh"


StereoClient::StereoClient() : stereoSource()
{
  
}

StereoClient::~StereoClient()
{
  unsubscribe();
  disconnect();
  delete alice;
  // cvDestroyWindow("Testing Image");  // if debugging
}

int StereoClient::init()
{
  readConfig();
  connect();
  subscribe();

  // Will be trashed--required to grab image
  CvSize imgSize;
  imgSize.width = 2;
  imgSize.height = 2;

  for (int i = 0; i < MAX_CAMERAS; i++)
  {
    _images[i] = cvCreateImage(imgSize, 8, 3);
  }
#warning "what does this really mean" 
  return -1;
}

int StereoClient::grab()
{
  //cout <<  "DataRequest" <<alice->RequestData() << endl;
  //while(alice->Peek())
  //{
  //cout << "data is available" << endl;
  alice->Read();
  //  cout << "Image Size is " << leftCamera->imageSize << endl;
      //break;
      //}
  //cout << "#leftCamera" << endl;
  //cout << leftCamera->image << endl;
  //cout << "#righttCamera" << endl;
  //  cout << rightCamera->image << endl;

  //  for (int i = 0; i < leftCamera->imageSize; i++)
  //{
  //  cout << (int)leftCamera->image[i] << ",";
  //}

  //cout << endl;

  for (int i = 0; i < MAX_CAMERAS; i++)
  {
    cvReleaseImage(&_images[i]);
  }

  CvSize imgSize;
  imgSize.width = leftCamera->width;
  imgSize.height = leftCamera->height;

  for (int i = 0; i < MAX_CAMERAS; i++)
  {
    _images[i] = cvCreateImage(imgSize, IPL_DEPTH_8U, 3);
  }

  // cout << leftCamera->imageSize << ", " << leftCamera->width << ", " << leftCamera->height << endl;

  int count = 0;
  for (int j = 0; j < imgSize.height; j++)
  {
    for (int i = 0; i < imgSize.width; i++)
    {
      for (int k = BIT_DEPTH - 1; k >= 0; k--)
      {
	
	((uchar*)(_images[LEFT_CAM]->imageData + _images[LEFT_CAM]->widthStep * j))[i*BIT_DEPTH + k] = (char)((int)leftCamera->image[count]);
	((uchar*)(_images[RIGHT_CAM]->imageData + _images[RIGHT_CAM]->widthStep * j))[i*BIT_DEPTH + k] = (char)((int)rightCamera->image[count]);
	count++;
      }
    }
  } 

  // test image with cv viewer

  // DEBUG
  //  cvNamedWindow("Testing Image", 1);
  //cvShowImage("Testing Image", _images[LEFT_CAM]);
  //cvWaitKey(1);

  // END DEBUG
  // usleep(1000000);
  //cvDestroyWindow("Testing Image");


  return stereoSource_OK;
}

int StereoClient::stop()
{
  for (int i = 0; i < MAX_CAMERAS; i++)
  {
    cvReleaseImage(&_images[i]);
  }

  return -1;
}

void StereoClient::readConfig()
{
  // set the defaults:
  host = PLAYER_GAZEBO_HOST;
  port = PLAYER_GAZEBO_PORT;

  cout << "StereoClient::readConfig: reading file '" << CONFIG_FILENAME 
       << "'" <<endl;
  ifstream file(CONFIG_FILENAME);
  if(!file.is_open())
  {
    cout << "StereoClient::readConfig: Cannot read file '" << CONFIG_FILENAME
    << "', using defaults = " << host << ":" << port << endl;
    return;
  }
  // if a file was found, use those settings instead:
  string line;
  getline(file, line);
  host = line;
  getline(file, line);
  port = atoi(line.c_str());
  cout << "StereoClient::readConfig: read host:port = "
  << host << ":" << port << endl;
}

void StereoClient::connect()
{
  cout << "StereoClient.Connect: connecting to " << host
  << ":" << port << endl;
  
  // Create a PlayerClient and connect it
  alice = new PlayerClient(host.c_str(), port);     
  // Connect the to the Player server if not already connected
  while(!alice->Connected())
    {
    if(alice->Connect(host.c_str(),port) != 0) 
      {
	cout << "PlayerClient.Connect: connect failed, trying again in ";
	for(int i = 5; i >= 0; i--)
	  {
	    usleep(1000000); // sleep 1 sec
	    cout << i << " " << flush;
	  }
	cout << endl;
      }
    }
  cout << "PlayerClient.Connect: connect successful!" << endl;  
  // Set the data mode
  // With this mode we get only data when we ask for it
  cout << "StereoClient.SetDataMode: setting data mode" << endl;
  if(alice->SetDataMode(DATA_MODE) != 0)
  {
    cout << "StereoClient.SetDataMode: failed! " << endl;
    exit(-1);
  }
  cout << "StereoClient.SetDataMode: successful! " << endl; 
}

void StereoClient::disconnect()
{
  // disconnect the from alice
  cout << "disconnecting the StereoClient" << endl;
  if(alice->Disconnect() != 0)
    {
      cout << "disconnect failed!" << endl;
    }
  else
    {
      cout << "disconnect successful!" << endl;
    }
}

void StereoClient::subscribe()
{
  unsigned char access;
  unsigned char mode = PLAYER_READ_MODE;
  // establish two  camera proxies, initially closed and unconnected
  leftCamera = new CameraProxy(alice,0,'c');
  rightCamera = new CameraProxy(alice,1,'c');
  cout << "LeftCamera: Subscribing (read only)" << endl;
  if(leftCamera->ChangeAccess(mode,&access) < 0 || (access != mode))
    {
      cout << "LeftCamera: error subscribing driver: " << leftCamera->driver_name << endl
	   << "LeftCamera: access: " << access << endl;
      exit(-1);
    }
  cout << "LeftCamera: Subscribed successfully driver : " << leftCamera->driver_name << endl;
  cout << "RightCamera: Subscribing (read only)" << endl;
  if(rightCamera->ChangeAccess(mode,&access) < 0 || (access != mode))
    {
      cout << "RightCamera: error subscribing driver: " << rightCamera->driver_name << endl
	   << "RightCamera: access: " << access << endl;
      exit(-1);
    }
  cout << "RightCamera: Subscribed successfully driver : " << rightCamera->driver_name << endl;
}

void StereoClient::unsubscribe()
{
  unsigned char access;
  //unsubscribe the two cameras
  if((leftCamera->ChangeAccess(PLAYER_CLOSE_MODE, &access) < 0) ||
     (access != PLAYER_CLOSE_MODE))
    {
      cout << "LeftCamera: failed to unsubscribe " << endl;
    }
  else
    {
      cout << "LeftCamera: unsubscribed successfully " << endl;;
      delete leftCamera;
    }
   if((rightCamera->ChangeAccess(PLAYER_CLOSE_MODE, &access) < 0) ||
     (access != PLAYER_CLOSE_MODE))
    {
      cout << "RightCamera: failed to unsubscribe " << endl;
    }
  else
    {
      cout << "RightCamera: unsubscribed successfully " << endl;;
      delete rightCamera;
    }
}
