#include "demo.h"

int main(){
    char  msg_buf[1024];
    int sock_fd, cliLen;
    struct sockaddr_in cliAddr;

    struct sockaddr_in multi_addr ;
    multi_addr.sin_family = AF_INET ;
    multi_addr.sin_port = htons(PORT);
    inet_aton(MADDR, &(multi_addr.sin_addr));
    bzero(&multi_addr.sin_zero, 8);

    struct ip_mreq my_mreq;
    inet_aton(MADDR, &(my_mreq.imr_multiaddr));
    my_mreq.imr_interface.s_addr = htonl(INADDR_ANY);

    /*
    printf("%s\n", inet_ntoa(my_mreq.imr_multiaddr));
    printf("%s\n", inet_ntoa(my_mreq.imr_interface));
    printf("%d\n", ntohs(multi_addr.sin_port));
    */

    sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock_fd == -1) {
        perror("socket");
        exit(1);
      }
    struct sockaddr_in servAddr;
    servAddr.sin_family=AF_INET;
    servAddr.sin_addr.s_addr=htonl(INADDR_ANY);
    servAddr.sin_port=htons(PORT);  

    //Allow multiple sockets on same computer to listen to same port
    u_int yes=1;
    if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) <0){
        perror("Reusing addr failed");
        exit(1);
    }
    
    if(bind(sock_fd,(struct sockaddr *) &servAddr, sizeof(servAddr))<0) {
        printf("%s : cannot bind port %d \n",MADDR,PORT);
        exit(1);
    }

    if (setsockopt(sock_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *) &my_mreq, sizeof(my_mreq))){
        perror("setsockopt");
        exit(1);
     }

/*    int connect_suceed =
        connect(sock_fd, (struct sockaddr*)&multi_addr, sizeof(struct sockaddr));
    if ( connect_suceed == -1 ) {
        perror("connect");
        exit(1);
    }*/

    while (1){
        cliLen=sizeof(cliAddr);
        recvfrom(sock_fd, msg_buf, 1024,0, (struct sockaddr *) &cliAddr, &cliLen);
        printf("%s\n", msg_buf);
    }
}
