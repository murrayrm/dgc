[imuin, gpsin] = loaddata('imudata.log','gpsdata.log');
[posbuffer, Xbuffer, Jbuffer] = strapdownRPY(imuin, gpsin);
figure(1)
plot(posbuffer(:,7),posbuffer(:,6),'b-',posbuffer(:,4),posbuffer(:,3),'ro');
figure(2)
subplot(2,1,1)
plot(posbuffer(:,1),posbuffer(:,9),'b-',posbuffer(:,1),posbuffer(:,5),'ro');
subplot(2,1,2);
plot(posbuffer(:,1),posbuffer(:,15),'b-',posbuffer(:,1),posbuffer(:,16),'ro');
