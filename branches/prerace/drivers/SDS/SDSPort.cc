#include "SDSPort.h"
#include <iostream>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/types.h>

using namespace std;

SDSPort::SDSPort(char* ip, int portNum)
{
  isConnected = 0; //Connection not yet established
  port = portNum;
  
  SDS.sin_family = AF_INET;
  SDS.sin_addr.s_addr = inet_addr(ip);
  SDS.sin_port = htons(port + 7999);

  //Create a copy of the address info (this is because the close() method blows away the underlying representation of the address, for some unfathomable reason... it sucks).
  copy = new sockaddr_in();
  (*copy) = SDS;

  int counter = 0;
  while(isOpen() != 1 && counter <= 10) //The port hasn't established a connection, and we've tried less than 10 times.
    {
      openPort();
      counter++;
    }
}

int SDSPort::write(const char* data)
{
  if(isOpen() != 1) //The port isn't open, so try to open it
    {
      if(openPort() == -1) //Failed to open the port
	{
	  return -1; //Error, so return -1
	}
    }

  //If we've made it this far, the port is open, so write.
  return send(sock, data, strlen(data), 0);
}

int SDSPort::write(const char* data, const int bytesToWrite)
{
  if(isOpen() != 1) //The port isn't open, so try to open it
    {
      if(openPort() == -1) //Failed to open the port
	{
	  return -1; //Error, so return -1
	}
    }

  //If we've made it this far, the port is open, so write.
  return send(sock, data, bytesToWrite, 0);
}

int SDSPort::read(const int bytesToRead, char* buffer, const int time)
{
  if(isOpen() != 1) //The port isn't open so try to open it.
    {
      if(openPort() == -1) //Failed to open the port
	{
	  return -1; //Error, so return -1
	}
    }

  FD_ZERO(&rfds);
  FD_SET(sock, &rfds);
  timeout.tv_sec = time/1000000; //Sets the timeout in seconds
  timeout.tv_usec = time%1000000; //Sets the timeout in microseconds

  if(select(sock+1, &rfds, NULL, NULL, &timeout) == 1) //If the SDSPort's device has data ready to be sent out
    {
      int bytesRead = recv(sock, buffer, bytesToRead, 0);
      return bytesRead; //Return the number of bytes actually read.
    }
  else
    {
      return -1;  // Connection timed out, the device didn't have any data to send
    }
}

int SDSPort:: peek(const int bytesToRead, char* buffer, const int time)
{
  if(isOpen() != 1) //The port isn't open so try to open it.
    {
      if(openPort() == -1) //Failed to open the port
	{
	  return -1; //Error, so return -1
	}
    }

  FD_ZERO(&rfds);
  FD_SET(sock, &rfds);
  timeout.tv_sec = time/1000000; //Sets the timeout in seconds
  timeout.tv_usec = time%1000000; //Sets the timeout in microseconds

  if(select(sock+1, &rfds, NULL, NULL, &timeout) == 1) //If the SDSPort's device has data ready to be sent out
    {
      int bytesRead = recv(sock, buffer, bytesToRead, MSG_PEEK);
      return bytesRead; //Return the number of bytes actually read.
    }
  else
    {
      return -1;  // Connection timed out, the device didn't have any data to send
    }
}


int SDSPort::read_until(const int maxBytesToRead, char* buffer, const int time, char find)
{
  if(isOpen() != 1) //The port isn't open so try to open it.
    {
      if(openPort() == -1) //Failed to open the port
	{
	  return -1; //Error, so return -1
	}
    }

  char temp[255];
  int bytesRead = 0;

  for(int i = 0; i<= 254; i++)
    {
      temp[i] = '?'; //We don't want to accidentally pick up leftover garbage as the intended character, so initialize it to something we probably aren't looking for
    }

  FD_ZERO(&rfds);
  FD_SET(sock, &rfds);
  timeout.tv_sec = time/1000000; //Sets the timeout in seconds
  timeout.tv_usec = time%1000000; //Sets the timeout in microseconds

  if(select(sock+1, &rfds, NULL, NULL, &timeout) == 1) //If the SDSPort's device has data ready to be sent out
    {
      int length = -1; //Stores the position of the desired character.
      int charPosition = 0;
      int attempts = 1;

      while((charPosition+1 <= maxBytesToRead) && (bytesRead >= 0) && (temp[charPosition] != find) && (attempts <= 255))//We haven't yet reached the limit of the number of bytes to search, we haven't reached the end of the message, and we haven't found the character yet; we also haven't gotten stuck in an infinite loop.      
	{
	  bytesRead = peek(charPosition+1, temp, time);
	  temp[bytesRead] = 0;

	  if(bytesRead == charPosition+1) // We got back the full message
	    {
	      if(temp[charPosition] == find) //We've reached the desired character, so keep track of ths value.
		{
		  length = charPosition+1; //Need to add 1 to charPosition due t stupid "logical" indexing
		  bytesRead = -2; //To break out of while loop.
		}
	      else //We haven't found the character yet, so increment the count and keep looking.
		{
		  charPosition++;
		}
	    }
	  attempts++;
	}

      if(bytesRead == -2) //We've actually found the character we're looking for
	{
	  bytesRead = SDSPort::read(length, buffer, time);
	}

      return bytesRead;
    }

  else
    {
      return -1;  // Connection timed out, the device didn't have any data to send
    }
}



int SDSPort::request(const char* requestString, const int bytesToRead, char* buffer, const int time)
{
  write(requestString); //Send a request string
  int result = read(bytesToRead,buffer, time); //Listen for a response using the read() method.
  return result; //Return the number of bytes read (or -1 for timeout)
}

int SDSPort::rpt_request(const char* requestString, const int bytesToRead, char* buffer, const int time, const int numTries)
{
  int result;
  for(int attempt = 1; attempt <= numTries; attempt++)
    {
      clean();
      result = request(requestString, bytesToRead, buffer, time);
      if (result == bytesToRead)
	{
	  return result; //Success! Return the number of bytes read (a full message)
	}
    }
  return result; //Not completely successful; return the number of bytes read on the last read (or -1 for error).
}

void SDSPort::clean()
{
  char* wastebuffer = new char[256];

  read(256, wastebuffer, 1000);
  delete wastebuffer;
}

int SDSPort::openPort()
{
  if(isOpen() != 1) //The port isn't already open, so open it
    {
      sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  
      if(sock < 0) //Error in port configuration
	{
	  return -1;
	}

      if(connect(sock, (const sockaddr*) copy, sizeof((*copy))) < 0) //Error in connecting
	{
	  return -1;
	}
    }

  isConnected = 1; //Everything's ok :-)

  return 0;
}

int SDSPort::closePort()
{
  int result = 0;

  //if(isOpen() != 0) //The port isn't already closed, so close it
  // {
  //    isConnected = 0;
  //    result = close(sock);

  //    delete copy; //Make sure we don't have a memory leak.

  //    copy = new sockaddr_in(); //Replace the stuff that close() blew away
  //    (*copy) = SDS;
  //  }

  return result;
}

int SDSPort::isOpen()
{
  return isConnected;
}

SDSPort::~SDSPort()
{
  closePort();
  close(sock);
  delete copy;
}
