#ifndef STRATEGY_HH
#define STRATEGY_HH

#include "SuperCon.hh"
#include "SuperConLog.hh"
#include "SuperConClient.hh"
#include "bool_counter.hh"
#include "bool_latched.hh"
#include "stage_iterator.hh"
#include "enumstring.h"

#include "sc_specs.h"

#include "StrategyInterface.hh"

#include <string>
#include <vector>
#include <stdio.h>

using namespace std;
using namespace sc_interface;

/** ALL ACTIONS FOR WHICH THEIR EXIST VALID PERSISTANCE LOOP THRESHOLDS **/
#define PERSIST_CHECK_ACTION_LIST(_) \
  _( persist_loop_estop, = 0 ) \
  _( persist_loop_astateResponse, ) \
  _( persist_loop_gearChange, ) \
  _( persist_loop_trajfModeChange, ) \
  _( persist_loop_trajfReverseFinished, ) \
  _( persist_loop_slowAdvanceApproach, ) \
  _( persist_loop_UnseenObstWaitForStationary, ) \
  _( persist_loop_waitstationary, ) \
  _( persist_loop_mapdeltasapplied, ) \
  _( persist_loop_clearmap, ) \
  _( persist_loop_waitforDARPA, ) \
  _( persist_loop_broadenRDDF, )
DEFINE_ENUM(persistCheckAction, PERSIST_CHECK_ACTION_LIST)

class CDiagnostic;    //Forward declaration
class CStrategyInterface; //Forward declaration

///////////////////////////////////////////////////////////////////////////////
//Class used to map state transfers at compile time
class CStrategyMapper
{
private:
  CStrategyMapper();   //Singleton class... use private default constructor
public:
  CStrategyMapper(SCStrategy from, SCStrategy to, const char *reason);

  static void print(const CDiagnostic &diag);   //Diagnostic used for name lookup

private:
  static CStrategyMapper *pInstance;

  struct Transition {
    SCStrategy to;
    SCStrategy from;
    string reason;
  };
  vector<Transition> m_transition;
};

///////////////////////////////////////////////////////////////////////////////
//Class declares virtual base class for strategies
class CStrategy
{

/** METHODS **/
public:
  
  /*** CONSTRUCTOR & DESTRUCTOR  ***/
  CStrategy();
  virtual ~CStrategy();


  /*** COMMON STRATEGY ACTION METHODS ***/

  //Step one step forward using the suppled diagnostic rules
  //Description: Step forward and execute the next stage of the current strategy:
  //Move to the current strategy and evaluate the termination (if TRUE move 
  //to nominal strategy) and transition (if TRUE move to another (not Nominal) strategy)
  //conditions for the next stage to be executed in the current strategy.
  //IF ALL of the termination & transition criteria evaluate to FALSE
  //then the method will enter and execute the next stage in the current
  //strategy.
  virtual void stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) =0;
 
  //We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
  //Description: Leave (& clean up) the current strategy for another strategy:
  //A termination or transition condition in the current strategy has 
  //evaluated to TRUE (this includes the condition that the end of the
  //current strategy has been reached).  Hence make the new strategy the
  //current strategy, and clean-up the old strategy (one being left) so
  //that all internal variables whose values are  NOT to be retained for the
  //next time that this strategy is used are RESET to their default (initial)
  //values.
  virtual void leave( CStrategyInterface *m_pStrategyInterface ) =0;

  //We are entering this state, do initialization (NO NEW STATE TRANSFERS)
  //Description: Enter (& intialise) the current strategy for the first time (i.e.
  //enter a new strategy):
  //A termination or transition condition in the previous strategy evaluated
  //to TRUE.  Intialise any required internal variables in the
  //current (new) strategy to their starting values
  virtual void enter( CStrategyInterface *m_pStrategyInterface ) =0;

  
  /** COMMON STRATEGY LOOPING PERSISTENCE CHECKS **/
  //This method takes the action for which a persist timeout it to be checked
  //(enum) AND the current value of the integer to be referenced against
  //which pre-defined persistence loop reset counter for the action type specified
  //is to be compared.  A pointer to the object calling this method is
  //also passed as an argument, so that transitionStrategy can be called
  //from within the method, as no instance of CStrategy (un-derived) exists
  //IF counter > DEFINED_PERSIST_LOOP_RESET [for specified action] then
  //transition to NOMINAL STRATEGY, ELSE do nothing
  bool checkForPersistLoop( CStrategy* pCaller, const persistCheckAction checkAction, int currentCount );
  
  
  /** COMMON STRATEGY TERMINATION METHODS **/

  //Termination methods evaluate whether to LEAVE the current strategy, and
  //return to the NOMINAL strategy, and are CONDITIONAL (i.e. IF a test
  //evaluates to true, transition to nominal, otherewise proceed
  
  //terminate current strategy IF planner is producing a trajectory that does
  //NOT pass through either a TERRAIN OR SUPERCON ZERO-SPEED (each use
  //different zeros) area
  bool term_PlannerNotNFP( const SCdiagnostic &diag, const char* StrategySpecificStr );
  
  
  /** COMMON STRATEGY TRANSITION METHODS (move from current strategy to another one) **/
  //Conditional methods that return a boolean, =TRUE iff we should transition to
  //the strategy for which this is the transition method, otherwise they return FALSE
  
  /** --> ANYTIME STRATEGY METHODS **/

  // ? --> UNSEEN OBSTACLE strategy conditional transition method
  // NOTE: THIS IS CURRENTLY SPECIFIC TO DRIVING FORWARDS
  bool trans_UnseenObstacle( CStrategyInterface &m_StrategyInterface, const SCdiagnostic &diag, const char* StrategySpecificStr );
  
  // ? --> GPS-REACQ (GPS-reacquisition) strategy conditional transition method
  bool trans_GPSreAcq( const SCdiagnostic &diag, const char* StrategySpecificStr );

  // ? --> ESTOP PAUSED NOT SUPERCON strategy conditional transition method
  bool trans_EstopPausedNOTsuperCon( const SCdiagnostic &diag, const char* StrategySpecificStr );

  // ? --> LTURN REVERSE strategy conditional transition method
  bool trans_LturnReverse( const SCdiagnostic &diag, const char* StrategySpecificStr );

  // ? --> PLANNER FAILED NO TRAJ strategy conditional transition method
  bool trans_PlnFailedNoTraj( const SCdiagnostic &diag, const char* StrategySpecificStr );
  
  // ? --> END OF RDDF strategy conditional transition method
  bool trans_EndOfRDDF( const SCdiagnostic &diag, const char* StrategySpecificStr );  

  // ? --> OUTSIDE RDDF strategy transition method
  bool trans_OutsideRDDF( const SCdiagnostic &diag, const char* StrategySpecificStr );


  /** --> SOMETIME/NOMINAL STRATEGY METHODS **/

  // ? --> SLOW ADVANCE strategy conditional transition method
  bool trans_SlowAdvance( const SCdiagnostic &diag, const char* StrategySpecificStr );

  /* SOMETIME-STRATEGY TERMINATION/TRANSITION CONDITION EVALUATION METHOD  */
  //Method calls all termination & transition evaluation methods (which are common
  //to all *sometime* strategies by definition), and returns a boolean which is
  //TRUE if *ANY* of the termination or transition conditions occur, and FALSE
  //otherwise
  bool checkAnytimeEntryConds( CStrategyInterface *pStrategyInterface, const SCdiagnostic *pdiag, const char* StrategySpecificStr );


protected:  //Helper functions for inherited classes
  void transitionStrategy(SCStrategy to, const char *reason);
  void doSetDoubleState(double SCstate::*pState, double val);
  void doSetIntState(int SCstate::*pState, int val);
  SCStrategy getID() const { return m_ID; }
  
  //wrapper for sprintf() that returns (by value) the string constructed
  //in the buffer produced by sprintf()
  char* strprintf( const char *str, ... );

/** VARIABLES **/

protected: //these should be inherited by all (derived) specific strategu
           //classes

  //stage iterator - used to hold the nextStage & currentStageCount (the number
  //of times the current stage has been looped through)
  stage_iterator stgItr;

  //latching bool instance to track whether to skip the operations for the
  //current stage -> value (true/false) updated by the entry operations.
  //NOTE: stage operations SKIPPED, iff skipStageOps == true.
  bool_latched skipStageOps;

  //enum name of the current stage in the strategy being executed 
  //(obtained using enumstring.h)
  string currentStageName;

private:    //Prevents inherited classes from access this member
  SCStrategy m_ID;
  CDiagnostic *m_pDiag;      //These variables are set from CDiagnostic (ie. friend) curring registration

  friend class CDiagnostic;
};


#endif
