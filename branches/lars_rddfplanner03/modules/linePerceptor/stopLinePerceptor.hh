#ifndef STOPLINEPERCEPTOR_HH_
#define STOPLINEPERCEPTOR_HH_

#include "mcv.hh"
#include "inversePerspectiveMapping.hh"

//#define DEBUG_GET_STOP_LINES



///line structure with start and end points
typedef struct Line
{
    FLOAT_POINT2D startPoint;
    FLOAT_POINT2D endPoint;
}Line;


//Structure to hold line perceptor settings
typedef struct StopLinePerceptorConf
{
    ///width of IPM image to use
    FLOAT ipmWidth;
    ///height of IPM image
    FLOAT ipmHeight;
    ///width of line we are detecting
    FLOAT lineWidth;
    ///height of line we are detecting
    FLOAT lineHeight;
    ///kernel size to use for filtering 
    unsigned char kernelWidth;
    unsigned char kernelHeight;
    ///lower quantile to use for thresholding the filtered image
    FLOAT lowerQuantile;
    ///whether to return local maxima or just the maximum
    bool localMaxima;
    ///the type of grouping to use, default 0 (ignored for now)
    unsigned char groupingType;
    ///whether to binarize the thresholded image or use the 
    ///raw filtered image
    bool binarize;
    //unsigned char topClip;
    ///threshold for line scores to declare as line
    FLOAT detectionThreshold;
}StopLinePerceptorConf;


//function definitions


/**
 * This function gets a 1-D gaussian filter with specified
 * std deviation and range
 * 
 * \param kernel input mat to hold the kernel (2*w+1x1) 
 *      column vector (already allocated)
 * \param w width of kernel is 2*w+1
 * \param sigma std deviation
 */
void mcvGetGaussianKernel(CvMat *kernel, 
    unsigned char w, FLOAT sigma);
    

/**
 * This function gets a 1-D second derivative gaussian filter 
 * with specified std deviation and range
 * 
 * \param kernel input mat to hold the kernel (2*w+1x1) 
 *      column vector (already allocated)
 * \param w width of kernel is 2*w+1
 * \param sigma std deviation
 */
void mcvGet2DerivativeGaussianKernel(CvMat *kernel, 
    unsigned char w, FLOAT sigma);
    

/**
 * This function filters the input image looking for horizontal
 * or vertical lines with specific width or height.
 *
 * \param inImage the input image
 * \param outImage the output image in IPM
 * \param wx width of kernel window in x direction = 2*wx+1 
 * (default 2)
 * \param wy width of kernel window in y direction = 2*wy+1 
 * (default 2)
 * \param sigmax std deviation of kernel in x (default 1)
 * \param sigmay std deviation of kernel in y (default 1)
 * \param lineType type of the line
 *      FILTER_LINE_HORIZONTAL (default)
 *      FILTER_LINE_VERTICAL
 */ 
 
#define FILTER_LINE_HORIZONTAL 0
#define FILTER_LINE_VERTICAL 1
void mcvFilterLines(const CvMat *inImage, CvMat *outImage,
    unsigned char wx=2, unsigned char wy=2, FLOAT sigmax=1,
    FLOAT sigmay=1, unsigned char lineType=FILTER_LINE_HORIZONTAL);    
    
    
/** This function groups the input filtered image into 
 * horizontal or vertical lines.
 * 
 * \param inImage input image
 * \param lines returned detected lines (vector of points)
 * \param lineScores scores of the detected lines (vector of floats)
 * \param lineType type of lines to detect
 *      HV_LINES_HORIZONTAL (default) or HV_LINES_VERTICAL
 * \param linePixelWidth width (or height) of lines to detect
 * \param localMaxima whether to detect local maxima or just get
 *      the maximum
 * \param detectionThreshold threshold for detection
 */ 
#define HV_LINES_HORIZONTAL 0
#define HV_LINES_VERTICAL   1
void mcvGetHVLines(const CvMat *inImage, vector<Line> *lines,
    vector<FLOAT> *lineScores, unsigned char lineType=HV_LINES_HORIZONTAL, 
    FLOAT linePixelWidth=1., bool binarize=false, bool localMaxima=false, 
    FLOAT detectionThreshold=1.);
    

/** This function binarizes the input image i.e. nonzero elements
 * become 1 and others are 0.
 * 
 * \param inImage input & output image
 */ 
void mcvBinarizeImage(CvMat *inImage);

/** This function gets the maximum value in a vector (row or column) 
 * and its location
 * 
 * \param inVector the input vector
 * \param max the output max value
 * \param maxLoc the location (index) of the first max
 * 
 */ 
void mcvGetVectorMax(const CvMat *inVector, double *max, int *maxLoc);

/** This function gets the qtile-th quantile of the input matrix
 * 
 * \param mat input matrix
 * \param qtile required input quantile probability
 * \return the returned value
 * 
 */
FLOAT mcvGetQuantile(const CvMat *mat, FLOAT qtile);
        
/** This function thresholds the image below a certain value to the threshold
 * so: outMat(i,j) = inMat(i,j) if inMat(i,j)>=threshold
 *                 = threshold otherwise
 * 
 * \param inMat input matrix
 * \param outMat output matrix
 * \param threshold threshold value
 * 
 */
void mcvThresholdLower(const CvMat *inMat, CvMat *outMat, FLOAT threshold);


/** This function detects stop lines in the input image using IPM 
 * transformation and the input camera parameters. The returned lines 
 * are in a vector of Line objects, having start and end point in 
 * input image frame.
 * 
 * \param image the input image
 * \param stopLines a vector of returned stop lines in input image coordinates 
 * \param cameraInfo the camera parameters
 * \param stopLineConf parameters for stop line detection
 *
 * 
 */
void mcvGetStopLines(const CvMat *inImage, vector<Line> *stopLines, 
    const CameraInfo *cameraInfo, 
    const StopLinePerceptorConf *stopLineConf);

/** This function converts an array of lines to a matrix (already allocated)
 * 
 * \param lines input vector of lines
 * \param size number of lines to convert
 * \return the converted matrix, it has 2x2*size where size is the
 *  number of lines, first row is x values (start.x, end.x) and second
 *  row is y-values
 * 
 * 
 */
void mcvLines2Mat(const vector<Line> *lines, CvMat *mat);

/** This function converts matrix into n array of lines
 * 
 * \param mat input matrix , it has 2x2*size where size is the
 *  number of lines, first row is x values (start.x, end.x) and second
 *  row is y-values
 * \param  lines the rerurned vector of lines
 * 
 * 
 */
void mcvMat2Lines(const CvMat *mat, vector<Line> *lines);

/** This function intersects the input line with the given bounding box
 * 
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
void mcvIntersectLineWithBB(const Line *inLine, const CvSize bbox,
    Line *outLine);
    
/** This function checks if the given point is inside the bounding box
 * specified
 * 
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
bool mcvIsPointInside(FLOAT_POINT2D point, CvSize bbox);

/** This function converts an INT mat into a FLOAT mat (already allocated)
 * 
 * \param inMat input INT matrix
 * \param outMat output FLOAT matrix
 * 
 */
void mcvMatInt2Float(const CvMat *inMat, CvMat *outMat);

    

/** This function draws a line onto the passed image
 * 
 * \param image the input iamge
 * \param line input line
 * \param line color
 * \param width line width
 * 
 */
void mcvDrawLine(CvMat *image, Line line, CvScalar color=CV_RGB(0,0,0), int width=1);


/** This initializes the stoplineperceptorinfo structure
 * 
 * \param fileName the input file name
 * \param stopLineConf the structure to fill
 *
 * 
 */
 void mcvInitStopLinePerceptorConf(char * const fileName, 
    StopLinePerceptorConf *stopLineConf);

void SHOW_LINE(const Line line, char str[]="Line:");


#endif /*STOPLINEPERCEPTOR_HH_*/
