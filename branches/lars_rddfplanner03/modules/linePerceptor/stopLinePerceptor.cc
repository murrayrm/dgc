

#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

#include "cv.h"
#include "highgui.h"

#include "mcv.hh"
#include "inversePerspectiveMapping.hh"
#include "stopLinePerceptor.hh"

#include "stopLinePerceptorOpt.h"

#include "ranker.h"


/**
 * This function filters the input image looking for horizontal
 * or vertical lines with specific width or height.
 *
 * \param inImage the input image
 * \param outImage the output image in IPM
 * \param wx width of kernel window in x direction = 2*wx+1 
 * (default 2)
 * \param wy width of kernel window in y direction = 2*wy+1 
 * (default 2)
 * \param sigmax std deviation of kernel in x (default 1)
 * \param sigmay std deviation of kernel in y (default 1)
 * \param lineType type of the line
 *      FILTER_LINE_HORIZONTAL (default)
 *      FILTER_LINE_VERTICAL
 */ 
 void mcvFilterLines(const CvMat *inImage, CvMat *outImage,
    unsigned char wx, unsigned char wy, FLOAT sigmax,
    FLOAT sigmay, unsigned char lineType)
{
    //define x
    CvMat *x = cvCreateMat(2*wx+1, 1, FLOAT_MAT_TYPE);
    //define y
    CvMat *y = cvCreateMat(2*wy+1, 1, FLOAT_MAT_TYPE);
        
    //create the convoultion kernel
    switch (lineType)
    {
        case FILTER_LINE_HORIZONTAL:
            //guassian in x direction
            mcvGetGaussianKernel(x, wx, sigmax);
            //derivative of guassian in y direction
            mcvGet2DerivativeGaussianKernel(y, wy, sigmay);            
            break;  
        
        case FILTER_LINE_VERTICAL:
            //guassian in y direction
            mcvGetGaussianKernel(y, wy, sigmay);
            //derivative of guassian in x direction
            mcvGet2DerivativeGaussianKernel(x, wx, sigmax);            
            break;
    }
    
    //combine the 2D kernel
    CvMat *kernel = cvCreateMat(2*wy+1, 2*wx+1, FLOAT_MAT_TYPE);
    cvGEMM(y, x, 1, 0, 1, kernel, CV_GEMM_B_T);
    
    //subtract the mean
    CvScalar mean = cvAvg(kernel);
    cvSubS(kernel, mean, kernel); 
    
    #ifdef DEBUG_GET_STOP_LINES
        SHOW_MAT(kernel, "Kernel:");
    #endif
    
    //do the filtering
    cvFilter2D(inImage, outImage, kernel);
    
    //clean
    cvReleaseMat(&x);
    cvReleaseMat(&y);
    cvReleaseMat(&kernel);            
}

/**
 * This function gets a 1-D gaussian filter with specified
 * std deviation and range
 * 
 * \param kernel input mat to hold the kernel (2*w+1x1) 
 *      column vector (already allocated)
 * \param w width of kernel is 2*w+1
 * \param sigma std deviation
 */
void mcvGetGaussianKernel(CvMat *kernel, 
    unsigned char w, FLOAT sigma)
{
    //get variance
    sigma *= sigma;
    
    //get the kernel
    for (double i=-w; i<=w; i++)
        CV_MAT_ELEM(*kernel, FLOAT_MAT_ELEM_TYPE, int(i+w), 0) = 
           (FLOAT_MAT_ELEM_TYPE) exp(-(.5/sigma)*(i*i));    
}

/**
 * This function gets a 1-D second derivative gaussian filter 
 * with specified std deviation and range
 * 
 * \param kernel input mat to hold the kernel (2*w+1x1) 
 *      column vector (already allocated)
 * \param w width of kernel is 2*w+1
 * \param sigma std deviation
 */
void mcvGet2DerivativeGaussianKernel(CvMat *kernel, 
    unsigned char w, FLOAT sigma)
{
    //get variance
    sigma *= sigma;
    
    //get the kernel
    for (double i=-w; i<=w; i++)
        CV_MAT_ELEM(*kernel, FLOAT_MAT_ELEM_TYPE, int(i+w), 0) = 
           (FLOAT_MAT_ELEM_TYPE)  
           (exp(-.5*i*i)/sigma - (i*i)*exp(-(.5/sigma)*i*i)/(sigma*sigma));    
}


/** This function groups the input filtered image into 
 * horizontal or vertical lines.
 * 
 * \param inImage input image
 * \param lines returned detected lines (vector of points)
 * \param lineScores scores of the detected lines (vector of floats)
 * \param lineType type of lines to detect
 *      HV_LINES_HORIZONTAL (default) or HV_LINES_VERTICAL
 * \param linePixelWidth width (or height) of lines to detect
 * \param localMaxima whether to detect local maxima or just get
 *      the maximum
 * \param detectionThreshold threshold for detection
 */ 
void mcvGetHVLines(const CvMat *inImage, vector <Line> *lines,
    vector <FLOAT> *lineScores, unsigned char lineType, 
    FLOAT linePixelWidth, bool binarize, bool localMaxima, 
    FLOAT detectionThreshold)
{
    CvMat * image = cvCloneMat(inImage);
    //binarize input image if to binarize
    if (binarize)
    {
        mcvBinarizeImage(image);                
    }
    
    //get sum of lines through horizontal or vertical
    //sumLines is a column vector
    CvMat sumLines, *sumLinesp;
    int maxLineLoc;
    switch (lineType)
    {
    case HV_LINES_HORIZONTAL:
        sumLinesp = cvCreateMat(image->height, 1, FLOAT_MAT_TYPE);
        cvReduce(inImage, sumLinesp, 1, CV_REDUCE_SUM);
        cvReshape(sumLinesp, &sumLines, 0, 0);
        //max location for a detected line
        maxLineLoc = inImage->height-1;
        break;
    case HV_LINES_VERTICAL:
        sumLinesp = cvCreateMat(1, image->width, FLOAT_MAT_TYPE);
        cvReduce(inImage, sumLinesp, 1, CV_REDUCE_SUM);
        cvReshape(sumLinesp, &sumLines, 0, inImage->width);
        //max location for a detected line
        maxLineLoc = inImage->width-1;
        break;
    }
    //SHOW_MAT(&sumLines, "sumLines:");
    
    //smooth it
    CvMat *smooth = cvCreateMat(int(linePixelWidth+.5)+1, 1, FLOAT_MAT_TYPE);
    cvSet(smooth, cvRealScalar(1.));
    cvFilter2D(&sumLines, &sumLines, smooth);
    //SHOW_MAT(&sumLines, "sumLines:");
    
    
    //get the max and its location
    vector <int> sumLinesMaxLoc;
    vector <double> sumLinesMax;
    int maxLoc; double max;
    mcvGetVectorMax(&sumLines, &max, &maxLoc);
    sumLinesMaxLoc.push_back(maxLoc);
    sumLinesMax.push_back(max);
    
    //TODO: put the local maxima stuff here
    if (localMaxima)
    {
    }
    
    //process the found maxima
    for (int i=0; i<sumLinesMax.size(); i++)
    {
        //check if above threshold then process it, otherwise
        //ignore it
        if (sumLinesMax[i] >= detectionThreshold)
        {
            //TODO: perform sub-pixel accuracy
            
            //TODO: get line extent
            
            //put the extracted line
            Line line;
            switch (lineType)
            {
            case HV_LINES_HORIZONTAL:
                line.startPoint.x = 0.5;             
                line.startPoint.y = sumLinesMaxLoc[i]+.5;
                line.endPoint.x = inImage->width-.5; 
                line.endPoint.y = line.startPoint.y;  
                break;
            case HV_LINES_VERTICAL:
                line.startPoint.x = sumLinesMaxLoc[i]+.5;   
                line.startPoint.y = .5;
                line.endPoint.x = line.startPoint.x;     
                line.endPoint.y = inImage->height-.5;  
                break;
            }
            (*lines).push_back(line);
            if (lineScores)
                (*lineScores).push_back(sumLinesMax[i]);
        }//if
    }//for
    
    //clean
    cvReleaseMat(&sumLinesp);
    cvReleaseMat(&smooth);
    sumLinesMax.clear();
    sumLinesMaxLoc.clear();
    cvReleaseMat(&image);
}

/** This function binarizes the input image i.e. nonzero elements
 * become 1 and others are 0.
 * 
 * \param inImage input & output image
 */ 
void mcvBinarizeImage(CvMat *inImage)
{
    if (CV_MAT_TYPE(inImage->type)==FLOAT_MAT_TYPE)
    {
        for (int i=0; i<inImage->height; i++)
            for (int j=0; j<inImage->width; j++)
                if (CV_MAT_ELEM(*inImage, FLOAT_MAT_ELEM_TYPE, i, j) != 0.f)
                    CV_MAT_ELEM(*inImage, FLOAT_MAT_ELEM_TYPE, i, j)=1;
    }
    else if (CV_MAT_TYPE(inImage->type)==INT_MAT_TYPE)
    {
        for (int i=0; i<inImage->height; i++)
            for (int j=0; j<inImage->width; j++)
                if (CV_MAT_ELEM(*inImage, INT_MAT_ELEM_TYPE, i, j) != 0)
                    CV_MAT_ELEM(*inImage, INT_MAT_ELEM_TYPE, i, j)=1;
    }
    else
    {
        cerr << "Unsupported type in mcvBinarizeImage\n";
        exit(1);   
    }                                                    
}


/** This function gets the maximum value in a vector (row or column) 
 * and its location
 * 
 * \param inVector the input vector
 * \param max the output max value
 * \param maxLoc the location (index) of the first max
 * 
 */ 
#define MCV_VECTOR_MAX(type)  \
    /*row vector*/ \
    if (inVector->height==1) \
    { \
        /*initial value*/ \
        tmax = (double) CV_MAT_ELEM(*inVector, type, 0, inVector->width-1); \
        tmaxLoc = inVector->width-1; \
        /*loop*/ \
        for (int i=inVector->width-1; i>=0; i--) \
        { \
            if (tmax<CV_MAT_ELEM(*inVector, type, 0, i)) \
            { \
                tmax = CV_MAT_ELEM(*inVector, type, 0, i); \
                tmaxLoc = i; \
            } \
        } \
    } \
    /*column vector */ \
    else \
    { \
        /*initial value*/ \
        tmax = (double) CV_MAT_ELEM(*inVector, type, inVector->height-1, 0); \
        tmaxLoc = inVector->height-1; \
        /*loop*/ \
        for (int i=inVector->height-1; i>=0; i--) \
        { \
            if (tmax<CV_MAT_ELEM(*inVector, type, i, 0)) \
            { \
                tmax = (double) CV_MAT_ELEM(*inVector, type, i, 0); \
                tmaxLoc = i; \
            } \
        } \
    } \

void mcvGetVectorMax(const CvMat *inVector, double *max, int *maxLoc)
{
    double tmax;
    int tmaxLoc;
            
    if (CV_MAT_TYPE(inVector->type)==FLOAT_MAT_TYPE)
    {
        MCV_VECTOR_MAX(FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inVector->type)==INT_MAT_TYPE)
    {
        MCV_VECTOR_MAX(INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetVectorMax\n";
        exit(1); 
    }
    
    
    //return
    if (max)
        *max = tmax;
    if (maxLoc)
        *maxLoc = tmaxLoc;
}

/** This function gets the qtile-th quantile of the input matrix
 * 
 * \param mat input matrix
 * \param qtile required input quantile probability
 * \return the returned value
 * 
 */
FLOAT mcvGetQuantile(const CvMat *mat, FLOAT qtile)
{
    //make it a row vector
    CvMat rowMat;
    cvReshape(mat, &rowMat, 0, 1);
    
    //get the quantile
    FLOAT qval;
    qval = quantile((FLOAT*) rowMat.data.ptr, rowMat.width, qtile);
    
    return qval;
}


/** This function thresholds the image below a certain value to the threshold
 * so: outMat(i,j) = inMat(i,j) if inMat(i,j)>=threshold
 *                 = threshold otherwise
 * 
 * \param inMat input matrix
 * \param outMat output matrix
 * \param threshold threshold value
 * 
 */
void mcvThresholdLower(const CvMat *inMat, CvMat *outMat, FLOAT threshold)
{

#define MCV_THRESHOLD_LOWER(type) \
     for (int i=0; i<inMat->height; i++) \
        for (int j=0; j<inMat->width; j++) \
            if ( CV_MAT_ELEM(*inMat, type, i, j)<threshold) \
                CV_MAT_ELEM(*outMat, type, i, j)=(type) threshold; \
                
    //check if to copy into outMat or not
    if (inMat != outMat)
        cvCopy(inMat, outMat);
        
    //check type
    if (CV_MAT_TYPE(inMat->type)==FLOAT_MAT_TYPE)
    {
        MCV_THRESHOLD_LOWER(FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inMat->type)==INT_MAT_TYPE)
    {
        MCV_THRESHOLD_LOWER(INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetVectorMax\n";
        exit(1); 
    }                            
}

/** This function detects stop lines in the input image using IPM 
 * transformation and the input camera parameters. The returned lines 
 * are in a vector of Line objects, having start and end point in 
 * input image frame.
 * 
 * \param image the input image
 * \param stopLines a vector of returned stop lines in input image coordinates 
 * \param cameraInfo the camera parameters
 * \param stopLineConf parameters for stop line detection
 *
 * 
 */
void mcvGetStopLines(const CvMat *inImage, vector<Line> *stopLines, 
    const CameraInfo *cameraInfo, 
    const StopLinePerceptorConf *stopLineConf)
{
    //input size
    CvSize inSize = cvSize(inImage->width, inImage->height);
    
    //TODO: smooth image
    CvMat *image = cvCloneMat(inImage);
    //cvSmooth(image, image, CV_GAUSSIAN, 5, 5, 1, 1);
    
    //Get IPM
    CvSize ipmSize = cvSize(stopLineConf->ipmWidth, 
        stopLineConf->ipmHeight);    
    IPMInfo ipmInfo;
    CvMat * ipm;
    ipm = cvCreateMat(ipmSize.height, ipmSize.width, inImage->type);
    //mcvGetIPM(inImage, ipm, &ipmInfo, cameraInfo);    
    mcvGetIPM(image, ipm, &ipmInfo, cameraInfo);
    
    //smooth the IPM
    //cvSmooth(ipm, ipm, CV_GAUSSIAN, 5, 5, 1, 1);
        
    //debugging 
    #ifdef DEBUG_GET_STOP_LINES
        CvMat *dbIpmImage = cvCreateMat(ipm->height, ipm->width, ipm->type);
        cvCopy(ipm, dbIpmImage);
    #endif

           
    //compute stop line width: 2000 mm
    FLOAT stopLinePixelWidth = stopLineConf->lineWidth * 
        ipmInfo.xScale;
    //stop line pixel height: 12 inches = 12*25.4 mm
    FLOAT stopLinePixelHeight = stopLineConf->lineHeight  * 
        ipmInfo.yScale;
    //kernel dimensions
    //unsigned char wx = 2;
    //unsigned char wy = 2;
    FLOAT sigmax = stopLinePixelWidth;
    FLOAT sigmay = stopLinePixelHeight;
    
    //filter the IPM image
    mcvFilterLines(ipm, ipm, stopLineConf->kernelWidth, 
        stopLineConf->kernelHeight, sigmax, sigmay, 
        FILTER_LINE_HORIZONTAL);     
    

    //zero out negative values
    mcvThresholdLower(ipm, ipm, 0);
    
    //compute quantile: .985
    FLOAT qtileThreshold = mcvGetQuantile(ipm, stopLineConf->lowerQuantile);
    mcvThresholdLower(ipm, ipm, qtileThreshold);

    //debugging 
    #ifdef DEBUG_GET_STOP_LINES
        CvMat *dbIpmImageThresholded = cvCreateMat(ipm->height, ipm->width, ipm->type);
        cvCopy(ipm, dbIpmImageThresholded);    
    #endif
    
    //group stop lines
    //vector <Line> ipmStopLines;
    vector <FLOAT> lineScores;
    mcvGetHVLines(ipm, stopLines, &lineScores, HV_LINES_HORIZONTAL, 
        stopLinePixelHeight, stopLineConf->binarize, 
        stopLineConf->localMaxima, stopLineConf->detectionThreshold);
        
    #ifdef DEBUG_GET_STOP_LINES
        vector <Line> dbIpmStopLines = *stopLines;
        //print out lineScores
        cout << "LineScores:";
        for (int i=0; i<lineScores.size(); i++)
            cout << lineScores[i] << " ";
        cout << "\n";
    #endif

    //check if returned anything
    if (stopLines->size()!=0)
    {                
        //convert the line into world frame
        for (unsigned int i=0; i<stopLines->size(); i++)
        {
            Line *line;
            line = &(*stopLines)[i];
            
            mcvPointImIPM2World(&(line->startPoint), &ipmInfo);
            mcvPointImIPM2World(&(line->endPoint), &ipmInfo);
        }
        
        //convert them from world frame into camera frame
        CvMat *mat = cvCreateMat(2, 2*stopLines->size(), FLOAT_MAT_TYPE);
        mcvLines2Mat(stopLines, mat);
        stopLines->clear();
        mcvTransformGround2Image(mat, mat, cameraInfo);
        mcvMat2Lines(mat, stopLines);
        //clear
        cvReleaseMat(&mat);
                
        //clip the lines found
        for (unsigned int i=0; i<stopLines->size(); i++)
            mcvIntersectLineWithBB(&(*stopLines)[i], inSize, &(*stopLines)[i]);        
    }        
    
    //debugging
    #ifdef DEBUG_GET_STOP_LINES
        //show the IPM image
        SHOW_IMAGE(dbIpmImage, "IPM image");
        //thresholded ipm
        SHOW_IMAGE(dbIpmImageThresholded, "Thresholded IPM image");    
        //draw lines in IPM image
        for (int i=0; i<dbIpmStopLines.size(); i++)
        {   
            mcvDrawLine(dbIpmImage, dbIpmStopLines[i], CV_RGB(0,0,0), 3);        
        }    
        SHOW_IMAGE(dbIpmImage, "IPM with lines");
        //draw lines on original image
        //CvMat *image = cvCreateMat(inImage->height, inImage->width, CV_32FC3);
        //cvCvtColor(inImage, image, CV_GRAY2RGB);
        //CvMat *image = cvCloneMat(inImage);
        for (int i=0; i<stopLines->size(); i++)
        {
            //SHOW_POINT((*stopLines)[i].startPoint, "start");
            //SHOW_POINT((*stopLines)[i].endPoint, "end");
            mcvDrawLine(image, (*stopLines)[i], CV_RGB(255,0,0), 3);
        }    
        SHOW_IMAGE(image, "Detected lines");
        //cvReleaseMat(&image);
        cvReleaseMat(&dbIpmImage);
        cvReleaseMat(&dbIpmImageThresholded);
        dbIpmStopLines.clear();    
    #endif //DEBUG_GET_STOP_LINES 

    //clear
    cvReleaseMat(&ipm);
    cvReleaseMat(&image);
    //ipmStopLines.clear();    
}



/** This function converts an array of lines to a matrix (already allocated)
 * 
 * \param lines input vector of lines
 * \param size number of lines to convert
 * \return the converted matrix, it has 2x2*size where size is the
 *  number of lines, first row is x values (start.x, end.x) and second
 *  row is y-values
 * 
 * 
 */
void mcvLines2Mat(const vector<Line> *lines, CvMat *mat)
{
    //allocate the matrix
    //*mat = cvCreateMat(2, size*2, FLOAT_MAT_TYPE);
    
    //loop and put values
    int j;
    for (int i=0; i<lines->size(); i++)
    {
        j = 2*i;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j) =
            (*lines)[i].startPoint.x;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j) = 
            (*lines)[i].startPoint.y;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j+1) =
             (*lines)[i].endPoint.x;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j+1) = 
            (*lines)[i].endPoint.y;
    }       
}


/** This function converts matrix into n array of lines
 * 
 * \param mat input matrix , it has 2x2*size where size is the
 *  number of lines, first row is x values (start.x, end.x) and second
 *  row is y-values
 * \param  lines the rerurned vector of lines
 * 
 * 
 */
void mcvMat2Lines(const CvMat *mat, vector<Line> *lines)
{

    Line line;
    //loop and put values
    for (int i=0; i<int(mat->width/2); i++)
    {
        int j = 2*i;
        //get the line
        line.startPoint.x = 
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j);
        line.startPoint.y =
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j); 
        line.endPoint.x =    
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j+1);
        line.endPoint.y =
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j+1);
        //push it
        lines->push_back(line);            
    }    
}



/** This function intersects the input line with the given bounding box
 * 
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
void mcvIntersectLineWithBB(const Line *inLine, const CvSize bbox,
    Line *outLine)
{
    //put output
    outLine->startPoint.x = inLine->startPoint.x;
    outLine->startPoint.y = inLine->startPoint.y;
    outLine->endPoint.x = inLine->endPoint.x;
    outLine->endPoint.y = inLine->endPoint.y;

    //check which points are inside
    bool startInside, endInside;
    startInside = mcvIsPointInside(inLine->startPoint, bbox);
    endInside = mcvIsPointInside(inLine->endPoint, bbox);
    
    //now check        
    if (!(startInside && endInside))
    {
        //difference
        FLOAT deltax, deltay;
        deltax = inLine->endPoint.x - inLine->startPoint.x;
        deltay = inLine->endPoint.y - inLine->startPoint.y;
        //hold parameters
        FLOAT t[4]={2,2,2,2};
        FLOAT xup, xdown, yleft, yright;
        
        //intersect with top and bottom borders: y=0 and y=bbox.height-1
        if (deltay==0) //horizontal line
        {
            xup = xdown = bbox.width+2;
        }
        else
        {
            t[0] = -inLine->startPoint.y/deltay;
            xup = inLine->startPoint.x + t[0]*deltax;
            t[1] = (bbox.height-inLine->startPoint.y)/deltay;
            xdown = inLine->startPoint.x + t[1]*deltax;
        }
        
        //intersect with left and right borders: x=0 and x=bbox.widht-1
        if (deltax==0) //horizontal line
        {
            yleft = yright = bbox.height+2;
        }
        else
        {
            t[2] = -inLine->startPoint.x/deltax;
            yleft = inLine->startPoint.y + t[2]*deltay;
            t[3] = (bbox.width-inLine->startPoint.x)/deltax;
            yright = inLine->startPoint.y + t[3]*deltay;
        }
        
        //points of intersection
        FLOAT_POINT2D pts[4] = {{xup, 0},{xdown,bbox.height},
               {0, yleft},{bbox.width, yright}};
                       
        //now decide which stays and which goes
        int i;
        if (!startInside)
        {
            bool cont=true;
            for (i=0; i<4 && cont; i++)
            {
                if (t[i]>=0 && t[i]<=1 && mcvIsPointInside(pts[i],bbox))
                {
                    outLine->startPoint.x = pts[i].x;
                    outLine->startPoint.y = pts[i].y;
                    t[i] = 2;
                    cont = false; 
                }
            }
        }
        if (!endInside)
        {
            bool cont=true;
            for (i=0; i<4 && cont; i++)
            {
                if (t[i]>=0 && t[i]<=1 && mcvIsPointInside(pts[i],bbox))
                {
                    outLine->endPoint.x = pts[i].x;
                    outLine->endPoint.y = pts[i].y;
                    t[i] = 2;
                    cont = false;
                }
            }           
        }   
    }
}


/** This function checks if the given point is inside the bounding box
 * specified
 * 
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
bool mcvIsPointInside(FLOAT_POINT2D point, CvSize bbox)
{
    return (point.x>=0 && point.x<=bbox.width 
        && point.y>=0 && point.y<=bbox.height) ? true : false;    
} 


/** This function converts an INT mat into a FLOAT mat (already allocated)
 * 
 * \param inMat input INT matrix
 * \param outMat output FLOAT matrix
 * 
 */
void mcvMatInt2Float(const CvMat *inMat, CvMat *outMat)
{
    for (int i=0; i<inMat->height; i++)
        for (int j=0; j<inMat->width; j++)
            CV_MAT_ELEM(*outMat, FLOAT_MAT_ELEM_TYPE, i, j) = 
                (FLOAT_MAT_ELEM_TYPE) 
                CV_MAT_ELEM(*inMat, INT_MAT_ELEM_TYPE, i, j)/255;
}


/** This function draws a line onto the passed image
 * 
 * \param image the input iamge
 * \param line input line
 * \param line color
 * \param width line width
 * 
 */
void mcvDrawLine(CvMat *image, Line line, CvScalar color, int width)
{
    cvLine(image, cvPoint((int)line.startPoint.x,(int)line.startPoint.y),
            cvPoint((int)line.endPoint.x,(int)line.endPoint.y), 
            color, width);
}

/** This initializes the stoplineperceptorinfo structure
 * 
 * \param fileName the input file name
 * \param stopLineConf the structure to fill
 *
 * 
 */
 void mcvInitStopLinePerceptorConf(char * const fileName, 
    StopLinePerceptorConf *stopLineConf)
{
  //parsed camera data
    StopLinePerceptorParserInfo stopLineParserInfo;
    //read the data
    assert(stopLinePerceptorParser_configfile(fileName, &stopLineParserInfo, 0, 1, 1)==0);
    //init the strucure
    stopLineConf->ipmWidth = stopLineParserInfo.ipmWidth_arg;
    stopLineConf->ipmHeight = stopLineParserInfo.ipmHeight_arg;
    stopLineConf->lineWidth = stopLineParserInfo.lineWidth_arg;
    stopLineConf->lineHeight = stopLineParserInfo.lineHeight_arg;
    stopLineConf->kernelWidth = stopLineParserInfo.kernelWidth_arg;
    stopLineConf->kernelHeight = stopLineParserInfo.kernelHeight_arg;
    stopLineConf->lowerQuantile = 
        stopLineParserInfo.lowerQuantile_arg;
    stopLineConf->localMaxima = 
        stopLineParserInfo.localMaxima_arg;
    stopLineConf->groupingType = stopLineParserInfo.groupingType_arg;
    stopLineConf->binarize = stopLineParserInfo.binarize_arg;
    stopLineConf->detectionThreshold = 
        stopLineParserInfo.detectionThreshold_arg;
}
        
void SHOW_LINE(const Line line, char str[]) 
{
    cout << str;
    cout << "(" << line.startPoint.x << "," << line.startPoint.y << ")";
    cout << "->";
    cout << "(" << line.endPoint.x << "," << line.endPoint.y << ")";
    cout << "\n";
} 

void dummy()
{
}
