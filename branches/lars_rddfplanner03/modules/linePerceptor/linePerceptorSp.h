/* Object ID's (offset into table) */

/* Allocate space for buffer storage */
static char buffer[48];

static DD_IDENT display[] = {
{1, 1, (void *)("LinePerceptor Rev: 11975"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 1, (void *)("Spread:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 28, (void *)("Lines:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 41, (void *)("("), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 47, (void *)(")"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 1, (void *)("Key   :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 28, (void *)("Fakes:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 1, (void *)("Module:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 28, (void *)("Line 1:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 1, (void *)("Frame :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 1, (void *)("Rate  :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 15, (void *)("Hz"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 24, (void *)("ms"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 1, (void *)("Latency"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 1, (void *)("["), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 8, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 16, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 23, (void *)("]"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 9, (void *)(&(stereoBlob.frameid)), dd_short, "%d", buffer+0, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "stereoBlob.frameid", -1},
{3, 42, (void *)(&(numStops)), dd_short, "%d", buffer+8, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "numStops", -1},
{3, 35, (void *)(&(totalStops)), dd_short, "%d", buffer+16, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "totalStops", -1},
{4, 35, (void *)(&(totalFakes)), dd_short, "%d", buffer+24, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "totalFakes", -1},
{5, 36, (void *)(&(lineMsg.lines[0].a[0])), dd_float, "%+5.2f", buffer+32, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "lineMsg.lines[0].a[0]", -1},
{5, 42, (void *)(&(lineMsg.lines[0].a[1])), dd_float, "%+5.2f", buffer+40, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "lineMsg.lines[0].a[1]", -1},
{13, 3, (void *)("QUIT"), dd_label, "NULL", (char *) NULL, 1, onUserQuit, (long)(long) 0, 0, 0, Button, "", -1},
{13, 10, (void *)("PAUSE"), dd_label, "NULL", (char *) NULL, 1, onUserPause, (long)(long) 0, 0, 0, Button, "", -1},
{13, 18, (void *)("FAKE"), dd_label, "NULL", (char *) NULL, 1, onUserFake, (long)(long) 0, 0, 0, Button, "", -1},
DD_End};
