
/* 
 * Desc: Sensor viewer utility
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <FL/Fl.H>
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <jplv/Fl_Glv_Window.H>
#include <jplv/jplv_image.h>

#include "sensnet.h"
#include "sensnet_types.h"
#include "sensViewerOpt.h"
#include "ImageWin.hh"
#include "WorldWin.hh"


// Application 
class App
{
  public:

  // Default constructor
  App();

  // Destructor
  ~App();

  // Initialize GUI
  int init(int cols, int rows);

  // Finalize styff
  int fini();

  public:

  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  // Loop through the image windows and update group affliations
  void updateImageGroups(sensnet_t *sensnet);

  // Loop through the image windows and update blob data
  void updateImageBlobs(sensnet_t *sensnet,
                        int sensorId, int blobType, int blobId, int blobLen);

  public:
  
  // Command-line options
  struct gengetopt_args_info options;
  
  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;

  // Image windows
  int numImageWins;
  ImageWin *imageWins[4];

  // 3D window
  WorldWin *worldwin;

  // Menu items
  const Fl_Menu_Item *action_pause;

  // Should we quit?
  bool quit;

  // Spread settings
  char *spreadDaemon; int skynetKey;

  // SensNet context
  sensnet_t *sensnet;

} app_t;


// Commands
enum
{
  APP_ACTION_PAUSE = 0x1000,
};


// Set up the menu
static Fl_Menu_Item menuitems[] =
{
  {"&File", 0, 0, 0, FL_SUBMENU},    
  {"E&xit", FL_CTRL + 'q', (Fl_Callback*) App::onExit},
  {0},
  {"&Action", 0, 0, 0, FL_SUBMENU},    
  {"Pause", ' ', NULL, (void*) APP_ACTION_PAUSE, FL_MENU_TOGGLE},
  {0},
  {0},
};


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
App::App()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
App::~App()
{
  return;
}


// Initialize stuff
int App::init(int cols, int rows)
{
  int w, h;
  int i;
  ImageWin *imageWin;
  
  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);  
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, -1) != 0)
    return ERROR("unable to connect to sensnet");
  
  w = cols / 4;
  h = rows / 4;
  
  this->mainwin = new Fl_Window(4 * w, 4 * h + 30, "DGC Sensor Viewer");
  this->mainwin->user_data(this);
  this->mainwin->size_range(4 * w, 3 * h + 30);

  this->mainwin->begin();
    
  // Set up the main window
  this->menubar = new Fl_Menu_Bar(0, 0, 4 * w, 30);
  this->menubar->user_data(this);
  this->menubar->menu(menuitems); 
    
  // Create image windows
  this->numImageWins = sizeof(this->imageWins) / sizeof(this->imageWins[0]);
  for (i = 0; i < this->numImageWins; i++)
  {
    imageWin = this->imageWins[i] = new ImageWin(this->sensnet, i*w, 30, w, h, 25);
    imageWin->user_data(this);
  }

  // Create world window
  this->worldwin = new WorldWin(this->sensnet, 0, 30 + 25 + h, 4*w, 3*h, 25);

  this->mainwin->end();

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  // Hook up menu options
  const Fl_Menu_Item *item;
  for (i = 0; i < this->menubar->menu()->size(); i++)
  {
    item = this->menubar->menu() + i;
    if (item->user_data() == (void*) APP_ACTION_PAUSE)
      this->action_pause = item;
  }

  return 0;
}


// Finalize styff
int App::fini()
{
  // Clean up SensNet
  if (sensnet_disconnect(this->sensnet) != 0)
    return ERROR("unable to connect to sensnet");
  sensnet_free(this->sensnet);

  return 0;
}


// Handle menu callbacks
void App::onExit(Fl_Widget *w, int option)
{
  App *self;

  self = (App*) w->user_data();
  self->quit = true;

  return;
}


// Handle idle callbacks
void main_idle(App *self)
{
  int i;
  ImageWin *imageWin;

  if (!self->action_pause->value())
  {
    // Update image windows
    for (i = 0; i < self->numImageWins; i++)
    {
      imageWin = self->imageWins[i];
      imageWin->update();
    }

    // Update the world window
    self->worldwin->update();
  }

  // Sleepy bye-bye
  usleep(10000);
  
  return;
}


// Main loop
int main(int argc, char *argv[])
{
  App *app;
  
  app = new App();

  // Parse options
  if (cmdline_parser(argc, argv, &app->options) != 0)
  {
    cmdline_parser_print_help();
    return -1;
  }

  // Fill out the spread name
  if (app->options.spread_daemon_given)
    app->spreadDaemon = app->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    app->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (app->options.skynet_key_given)
    app->skynetKey = app->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    app->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    app->skynetKey = 0;
  
  // Initialize gui
  if (app->init(512, 394) != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) main_idle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  delete app;

  MSG("exited cleanly");
  
  return 0;
}
