 
/* 
 * Desc: Sink for ladar blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include "sensnet.h"
#include "sensnet_ladar.h"


// Display data for ladar blobs
class LadarSink
{
  public:

  // Constructor
  LadarSink();
  
  // Generate range point cloud 
  void predrawPointCloud();

  // Predraw camera footprint 
  void predrawFootprint();

  public:

  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId;

  // Blob data buffer
  sensnet_ladar_blob_t blob;
  
  // Are we enabled?
  bool enable;

  // Have we joined the group?
  bool joined;

  // Set flag if the data needs predrawing
  bool dirty;

  // GL Drawing lists
  int cloudList, footList;  
};
