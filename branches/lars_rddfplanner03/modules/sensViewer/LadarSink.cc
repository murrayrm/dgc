
/* 
 * Desc: Sink for ladar blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <string.h>
#include <stdio.h>
#include <GL/glu.h>

#include <AliceConstants.h>
#include "LadarSink.hh"


// Constructor
LadarSink::LadarSink()
{
  memset(this, 0, sizeof(*this));

  this->sensorId = SENSNET_NULL_SENSOR;
  this->blobId = -1;
  
  return;
}


// Generate range point cloud in sensor frame
void LadarSink::predrawPointCloud()
{
  int i;
  float pa, pr, px, py, pz;
  sensnet_ladar_blob_t *blob;

  if (this->blobId < 0)
    return;
  
  if (this->cloudList == 0)
    this->cloudList = glGenLists(1);

  glNewList(this->cloudList, GL_COMPILE);

  glPushAttrib(GL_POINT_BIT);
  glPointSize(2.0);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_POINTS);
  glColor3f(0, 0, 1);
      
  blob = &this->blob;
  
  for (i = 0; i < blob->num_points; i++)
  {
    pa = blob->points[i][0];
    pr = blob->points[i][1];
    if (pr > 0)
    {
      sensnet_ladar_blob_ir_to_xyz(blob, pa, pr, &px, &py, &pz);
      glVertex3f(px, py, pz);
    }
  }

  glEnd();
  glPopAttrib();

  glEndList();
    
  return;
}


// Predraw sensor footprint
void LadarSink::predrawFootprint()
{
  int i;
  float a, b, d;
  float pt, pr;
  float px, py, pz;

  if (this->blobId < 0)
    return;

  if (this->footList == 0)
    this->footList = glGenLists(1);
  glNewList(this->footList, GL_COMPILE);

  a = this->blob.sens2veh[2][0];
  b = this->blob.sens2veh[2][1];
  d = this->blob.sens2veh[2][3] - VEHICLE_TIRE_RADIUS;

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glColor4f(0, 0, 1, 0.50);
  glBegin(GL_POLYGON);
  
  glVertex3f(0, 0, 0);  
  for (i = 0; i < this->blob.num_points; i++)
  {
    // Range and bearing of point on the ground (sensor frame)
    pt = (i - this->blob.center) * this->blob.scale;
    pr = -d / (a * cos(pt) + b * sin(pt));
    if (pr < 0 || pr > 80 || !finite(pr)) // MAGIC
      pr = 80;
    sensnet_ladar_blob_ir_to_xyz(&this->blob, i, pr, &px, &py, &pz);
    glVertex3f(px, py, pz);
  }
  
  glEnd();
  glDisable(GL_BLEND);
  
  glEndList();
  
  return;
}
