
/* 
 * Desc: Window for displaying 3D stuff
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <jplv/Fl_Glv_Window.H>
 
#include "sensnet.h"

#include "StereoSink.hh"
#include "LadarSink.hh"


// Window for displaying images
class WorldWin : public Fl_Window
{
  public:

  // Constructor
  WorldWin(sensnet_t *sensnet, int x, int y, int w, int h, int menuh);

  public:

  // Handle menu options
  static void onSensor(Fl_Widget *w, int option);
  
  // Handle menu options
  static void onView(Fl_Widget *w, int option);

  // Draw window
  static void onDraw(Fl_Glv_Window *win, WorldWin *self);

  private:

  // Switch to the given frame; i.e., sets up the GL coordinate
  // transforms such that (0, 0, 0) will map to the given pose.
  void pushFrame(pose3_t pose);

  // Switch to given frame (homogeneous transform)
  void pushFrame(float m[4][4]);

  // Switch to given frame (translation + euler angles)
  void pushFrame(double pose[6]);
    
  // Revert to previous frame
  void popFrame();

  // Draw a set of axes
  void drawAxes(double size);

  // Predraw the robot
  void predrawAlice();

  // Predraw a ground grid
  void predrawGrid();

  // Predraw road lines
  int predrawLines();

  public:

  // Update blob data
  int update();

  public:

  // Local menu
  Fl_Menu_Bar *menubar;

  // GL window
  Fl_Glv_Window *glwin;

  // Menu options
  const Fl_Menu_Item *view_vehicle, *view_local, *view_footprint;

  // Sensnet handle
  sensnet_t *sensnet;

  // List of stereo sinks
  int numStereoSinks;
  StereoSink stereoSinks[8];

  // List of ladar sinks
  int numLadarSinks;
  LadarSink ladarSinks[8];
  
  // Most recent state data
  sensnet_state_t state;

  // Some display lists
  int aliceList, gridList, lineList;
};
