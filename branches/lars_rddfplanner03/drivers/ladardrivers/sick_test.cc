/*********************************************************************************/
/* SICK LASER DRIVER V0.9                                                        */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs (See README for details) */
/*********************************************************************************/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define ANGLE 100
#define RES 0.50

#include "sick_driver.h"

int main(int argc, char *argv[]) {
  unsigned char data[1024];
  int result;
  int status;

  if(argc < 2) {
    printf("Serial device must be specified on cmd line (ex /dev/ttyS9)\n");
    exit(-1);
  }

  printf("Attempting to communicated with LADAR on %s\n",argv[1]);

  /* Create the LADAR object */
  SickLMS200 laserDevice;

  /* Set up the LADAR based on the port given on the command line */
  result = laserDevice.Setup(argv[1]);
  printf("numDataPoints(): %d\n", laserDevice.getNumDataPoints());

  /* Now we can start using the LADAR */
  /* No code yet; Albert will fill things in */

  return(0);
}
