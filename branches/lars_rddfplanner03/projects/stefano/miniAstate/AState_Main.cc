#include <getopt.h>
#include "AState.hh"
#include <stdlib.h>
#include "DGCutils"


AState *ss;


int main(int argc, char **argv)
{
  //skynet key is hard coded now
  int sn_key = 77;
/*  const char* cd = (argv[1]);	
  int sn_key = atoi(cd);	*/
  char *pKey = getenv("SKYNET_KEY");
  if (pKey == NULL)
  {
    cerr << "Must set SKYNET_KEY environment variable"<< endl;
    exit(1);
  }
  else
  {
    sn_key = atoi(pKey);
  }
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  
  char *pUseSparrow =getenv("SPARROW"); 
  char *pLogRaw = getenv("LOGRAW");
  
  astateOpts options;
  options.useSparrow=1;
  options.logRaw==0;
  
  if(pUseSparrow != NULL)
  {
    int useSparrow = atoi(pUseSparrow);
    if(useSparrow == 0)
    {
      options.useSparrow = 0;
    }
  }
  
  if(pLogRaw != NULL)
  {
    int logRaw = atoi(pLogRaw);
    if(logRaw==1)
    {
      options.logRaw =1;
    }
  }
  
  //Create AState server:
  ss = new AState(sn_key,options);

  ss->active();

  delete ss;

  return 0;
}
