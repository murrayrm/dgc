/* AState.cc - new vehicle state estimator
 *
 * SDC Nov 06
 *
 * */


#include "AState.hh"
#include "Matrix.hh"
#include "sparrow/display.h"
#include "sparrow/flag.h"

#define square(x) ((x)*(x))

extern astateOpts inputAstateOpts;
extern unsigned short setStateMode(unsigned short);
GisCoordLatLon latsForVehState;
GisCoordUTM utmForVehState;

void AState::updateStateThread()
{
  unsigned long long stateTime = 0;
  unsigned long long apxTime = 0;
  rawApx apxEstimate;
  
  int newApx = 0;
  int count = 0;   //Counter that increments every loop (used to limit broadcast rate)
  
  
  while (!quitPressed)
  {
    /*Load Apx Data*/
    if(DEBUG_LEVEL>4)
    {
      cout << "this is the UPDATE thread working" << endl; 
      sleep(1);
    }
    DGClockMutex(&m_ApxDataMutex);
    
    if (apxBufferReadInd > apxBufferLastInd)
    {
      DGCSetConditionTrue(apxBufferFree);

      apxBufferLastInd++;

      int ind = apxBufferLastInd % APX_BUFFER_SIZE;

      
      // Compute averages for IMU valuesthe integral of imu data to remove bias
      apxEstimate=apxBuffer[ind];


      apxTime = apxBuffer[ind].time;

      if(stateTime < apxTime)
      {
	    stateTime = apxTime;
      }

      newApx = 1;
      apxCount++;

    }
    
    DGCunlockMutex(&m_ApxDataMutex);

    if (newApx == 0)
    {
      newData.bCond = false;
      DGCWaitForConditionTrue(newData);
    }
    else
    { 
      latsForVehState.latitude = apxEstimate.data.latitude;
      latsForVehState.longitude = apxEstimate.data.longitude;
      gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84);
      
      DGClockMutex(&m_VehicleStateMutex);
      _vehicleState.Northing = apxEstimate.data.latitude; 
      _vehicleState.Easting = apxEstimate.data.longitude;
      _vehicleState.Altitude = -apxEstimate.data.altitude; //POS-LV gives positive altitude
      _vehicleState.GPS_Northing = apxEstimate.data.latitude ;
      _vehicleState.GPS_Easting = apxEstimate.data.longitude;
      _vehicleState.raw_YawRate = apxEstimate.data.heading;
      _vehicleState.Vel_N = apxEstimate.data.northVelocity;
      _vehicleState.Vel_E = apxEstimate.data.eastVelocity;
      _vehicleState.Vel_D = apxEstimate.data.downVelocity;
      _vehicleState.Acc_N = 0; //we get accelerations w.r.t. reference frame. need to transform them
      _vehicleState.Acc_E = 0;
      _vehicleState.Acc_D = 0;
      _vehicleState.RollAcc = 0; //we don't get these
      _vehicleState.PitchAcc = 0;
      _vehicleState.YawAcc = 0;
      _vehicleState.RollRate = apxEstimate.data.rollRate;
      _vehicleState.PitchRate = apxEstimate.data.pitchRate;
      _vehicleState.YawRate = apxEstimate.data.yawRate;
      _vehicleState.Roll = apxEstimate.data.roll;
      _vehicleState.Pitch =  apxEstimate.data.pitch;
      _vehicleState.Yaw =  apxEstimate.data.heading;	

      //We don't get confidences yet

      _vehicleState.NorthConf = 1;
      _vehicleState.EastConf = 1;
      _vehicleState.HeightConf = 1;
      _vehicleState.RollConf = 1;
      _vehicleState.PitchConf = 1;
      _vehicleState.YawConf = 1;
      DGCunlockMutex(&m_VehicleStateMutex);

      /* Compute the error between the Novatel & state estimate for sparrow */
      stateMode = setStateMode((unsigned short)apxEstimate.data.alignStatus);
  	
      if (count++ % 8 == 0)
      {	
        if (_astateOpts.fastMode == 0) 
        {
	  broadcast();
	}
      }
    }
  }
}
/*
Matrix nav2earth(double yaw, double pitch, double roll)
{
  Matrix R(3,3);
#ifdef UNUSED
  double ca=cos(yaw);
  double cb=cos(pitch);
  double cg=cos(roll);
  double sa=sin(yaw);
  double sb=sin(pitch);
  double sg=sin(roll);

  R[]={(ca*cb*cg-sa*sg), (-ca*cb*sg-sa*cg), (ca*sb), (sa*cb*cg+ca*sg), (-ca*cb*sg+ca*cg), (sa*sb), (-sb*cg), (sb*sg), (cb)};  
 call.\n" ) ;
perror( NULL ) ;#endif  

  return(R);
}
*/
/*! this function converts the apxStatus into AState statemode.
 * preinit correspond to the initial solution
 * init correspond to a large error (coarse leveling or heading error > 15 )
 * nav is for full navigation or heading error < 15 deg (fine alignement)
 * oh_shit is for a missing initial solution/ system errors
 * */ 
unsigned short setStateMode(unsigned short apxStatus)
{
  unsigned short stateMode;

	switch(apxStatus)
	{
      case 0 : stateMode = NAV;
               break;
	  case 1 : stateMode = NAV;
               break;
	  case 2 : stateMode = INIT;
               break;
	  case 3 : stateMode = INIT;
               break;
	  case 4 : stateMode = INIT;
               break;
	  case 5 : stateMode = INIT;
               break;
	  case 6 : stateMode = INIT;
               break;
	  case 7 : stateMode = PREINIT;
               break;
	  case 8 : stateMode = OH_SHIT;
               break;
      default : cerr << "unadmissible apxStatus code received: " << apxStatus << endl;
	}    		  
	return(stateMode);
}
