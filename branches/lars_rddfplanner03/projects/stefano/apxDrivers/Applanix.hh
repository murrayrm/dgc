#ifndef APPLANIX_HH
#define APPLANIX_HH

#include <string.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>

#define APX_CONTROL_PORT 5601
#define APX_DATA_PORT 5602
#define APX_IP_ADDR "192.168.0.100" 
#define LENGTH_BYTE 6
#define LENGTH_OFFSET 8
#define SEL_MSG_LENGTH 20
#define NAVMOD_MSG_LENGTH 16
#define CONTROL_MSG_LENGTH 16
#define NAV_GRP_LENGTH 140
#define ACK_MSG_LENGTH 52
#define APX_BUFFER_SIZE 4096 //for now we can also set it to 140, this is for test



/* ************************************
 * ********** APX_DATA STRUCT *********
 * ************************************/
 
typedef struct apxNavData
{
  char groupStart[5];      //increased by 1 because of null termination
  unsigned short ID;
  unsigned short byteCount;
	
	// time and distance -> maybe another struct
  double time1;
  double time2;
  double distanceTag;
  char timeTypes;
  char distanceType;
	
  double latitude;
  double longitude;
  double altitude;	
	
  float northVelocity;
  float eastVelocity;
  float downVelocity;
	
  double roll;
  double pitch;
  double heading;
  double wander;
	
  float trackAngle;
  float speed;
	
	
  float rollRate;
  float pitchRate;
  float yawRate;
		
  float longitudinalAcc;
  float transverseAcc;
  float downAcc;
	
  char alignStatus;
  char pad;
  unsigned short checksum;
  char groupEnd[3]; //increased by 1 because of null termination		
};

void parseNavData(char* inBuffer, int size, apxNavData* msg);




/* *****************************************
 * ************* Navigation Mode ***********
 * *****************************************/


// *********** MESSAGE **********************

typedef struct apxNavModeMsg
{ 
  apxNavModeMsg()    //this is the constructor, it fills the standard fields
  {  
    char init[5]="$MSG";
	char end[3]="$#";
	strcpy(msgStart,init);
	strcpy(msgEnd,end);
	ID=50;
	byteCount=8;
	pad = '\0';
	checksum=0;
  }
  char msgStart[5]; //increased by 1 because of null termination-> don' t send the add byte!!!
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  char navMode;
  char pad;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination-> don' t send the add byte!!!
};

// ********* BUILD MESSAGE ***************

void buildNavModeMsg(apxNavModeMsg* , unsigned short );
// ******** SERIALIZE NAV MODE MESSAGE *******

void serialNavModeMsg(apxNavModeMsg* , char*);







/* *****************************************
 * ************** SELECT DATA GROUPS *******
 * *****************************************/

// ********** MESSAGE ***********************

typedef struct apxSelGroupsMsg
{
  apxSelGroupsMsg()    //this is the constructor, it fills the standard fields
  {  
    char init[5]="$MSG";
	char end[3]="$#";
	strcpy(msgStart,init);
	strcpy(msgEnd,end);
	ID=52;
	checksum=0;
  } 	
	
  char msgStart[5]; //increased by 1 because of null termination-> don' t send the add byte!!!
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  unsigned short numberOfGroups;
  unsigned short groupID;  //I set to 10 the max number of groups we can get. This can be changed in the future
  unsigned short dataRate;
  unsigned short pad;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination-> don' t send the add byte!!!	
};


// ********* BUILD MESSAGE ***************

void buildSelGroupsMsg(apxSelGroupsMsg* , unsigned short );

// ******** SERIALIZE MESSAGE *******

void serialSelGroupsMsg(apxSelGroupsMsg* , char*, int );


/* *****************************************
 * ***** ACKNOWLEDGE MESSAGE ****************
 * *****************************************/
 
typedef struct apxAckMsg
{ 
  char msgStart[5]; //increased by 1 because of null termination
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  unsigned short recMsgID;
  unsigned short responseCode;
  char newParamStatus;
  char rejectedParamName[32];
  unsigned short pad;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination			
};


void parseAckMsg(char* , int , apxAckMsg* );



/* *****************************************
 * ******** CONTROL MESSAGE ****************
 * *****************************************/
 
typedef struct apxControlMsg
{
  apxControlMsg()    //this is the constructor, it fills the standard fields
  {  
    char init[5]="$MSG";
	char end[3]="$#";
	strcpy(msgStart,init);
	strcpy(msgEnd,end);
	byteCount = 8;
	ID=90;
	checksum=0;
  } 	
	
  char msgStart[5]; //increased by 1 because of null termination-> don' t send the add byte!!!
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  unsigned short control;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination-> don' t send the add byte!!!	
};


// ********* BUILD MESSAGE ***************

void buildControlMsg(apxControlMsg* , unsigned short, unsigned short );

// ******** SERIALIZE MESSAGE *******

void serialControlMsg(apxControlMsg* , char* );



/* ****************************************************
 * ****************** CHECKSUM ************************
 * ****************************************************/

// *************COMPUTE CHECKSUM **********************

unsigned short apxCompChecksum(char* ,int );

// *************VERIFY CHECKSUM **********************

unsigned short apxVerChecksum(char* ,int );





/* ****************************************************
*  ************* TCP-IP functions *********************
*  ****************************************************/

// **************** SEND ******************************

short apxTcpSend(int* , char* , short );
// **************** RECEIVE ******************

short apxTcpReceive(int* , char* , short );

#endif //APPLANIX_HH
