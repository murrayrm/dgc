% this function is playing with plotting convex hulls of an image
function plotCH(index,file)
tic

%index is which frame of the raw file to plot
data = load(file);
[row,col]=size(data);
ranges = data(index, 3:col);
newRanges = data(index+50,3:col);
[row,col] = size(ranges)

% setting the colors with which to fill the diff area types
colorOccupied = [0,.5,.5];
%colorUnknown = 1 - .5 * [rand(1), rand(1), rand(1)]
%colorObscured = 1 - .5 * [rand(1), rand(1), rand(1)]
colorUnknown = [.5,.7,.8];
colorObscured = [.5, .8, .7];

figure(1); hold off; axis([-2000,6000,-1000,5000]);hold on;
figure(2); hold off; axis square;
figure(3); hold off; axis([-2000,6000,-1000,5000]); hold on;


% we don't want lots of points out at max range...setting to the origin
for i=1:row
    for j=1:col
        if ranges(i,j)==8191 
            ranges(i,j)=0;
        end
        if newRanges(i,j)==8191 
            newRanges(i,j)=0;
        end

    end
end

% want the start and end positions of objects in image
[foo,posObjects,sizeObjects] = segmentImage(ranges);

% want the number of objects in image
[numObj, bar] = size(posObjects);

% for plotting and some polygon fxns, need data in cartesian
temp = [1:col];
angles = 2*pi*temp/360;
[x,y] = pol2cart(angles,ranges);
[x2,y2] = pol2cart(angles,newRanges);

figure(1);
plot(x,y,'rx');
figure(3);
plot(x,y,'k.');



% grrr. dont want to have to set a specific size for this...
% by initializing to -1, if BOTH x and y value are neg, then you're at an
% unfilled space, and should ignore
spaceOccupied = zeros(numObj,2,max(sizeObjects)) - 1;
countSpaceOccupied = 0;

% now, find and plot the convhull of each object
% currently, this assumes that even isolated points are reported as objects
% at same time, define the occupied space (for later plotting)
for i=1:1:numObj
    % our criteria for calling something an object for the convex hull purpose
    if sizeObjects(i) > 2 && abs(foo(i,1)) > 0
        objStart = posObjects(i,1);
        objEnd = posObjects(i,2);
        countSpaceOccupied = countSpaceOccupied + 1;
        edgesOccupied(countSpaceOccupied,:) = [objStart, objEnd];
        temp = convhull(x(objStart:objEnd),y(objStart:objEnd));
        
        figure(3); hold on;
fill(x(temp+objStart-1),y(temp+objStart-1),colorOccupied);
plot(x(temp+objStart-1),y(temp+objStart-1),'rx');
figure(2);

        fill(x(temp+objStart-1),y(temp+objStart-1),colorOccupied);
        hold on;
        
        %trying to update occupiedSpace matrix. need to figure elegant way
        %to access this data (it's not of standard length...arrrgh)
        grrr = [x(temp+objStart-1);y(temp+objStart-1)];
        spaceOccupied(i,:,1:size(temp)) = grrr;
    end
end

% now, defining the space we know to be obscured
[row,col] = size(edgesOccupied);
spaceObscured = zeros(row, 2, 4);
figure(2);
hold on;

for i=1:1:row
    objStart = edgesOccupied(i,1);
    objEnd = edgesOccupied(i,2);
    bounds = [8191, ranges(objStart), ranges(objEnd), 8191];
    slice = [angles(objStart), angles(objStart), angles(objEnd), angles(objEnd)];
    [newX,newY] = pol2cart(slice,bounds);
    spaceObscured(i,1,1:4) = newX;
    spaceObscured(i,2,1:4) = newY;
    fill(newX,newY,colorObscured);
end

% and, identifying the space about which we are unsure...
spaceUnknown = zeros(row + 1, 2, 4);
btwnRanges = [8191, 4000, 4000, 8191];
for i=1:1:row+1
    if i==1
        objEnd = edgesOccupied(i,1);
        slice = [angles(1), angles(1), angles(objEnd), angles(objEnd)];
    elseif i==row+1
        objStart = edgesOccupied(row,2);
        len = length(angles);
        slice = [angles(objStart), angles(objStart), angles(len), angles(len)];
    else
        objStart = edgesOccupied(i-1,2);
        objEnd = edgesOccupied(i,1);
        slice = [angles(objStart), angles(objStart), angles(objEnd), angles(objEnd)];
    end
    [newX,newY] = pol2cart(slice,btwnRanges);
    spaceUnknown(i,1,1:4) = newX;
    spaceUnknown(i,2,1:4) = newY;
    fill(newX,newY,colorUnknown);
end


figure(2);
%plot(x2,y2,'r+');
plot(x2,y2,'rx');
hold on;

% now, trying to classify points from the next scan...

% Since the LADARs have noise w/ a Gaussian distribution, its not realistic
% to insist on obstacles being in EXACTLY the same place in each scan. I
% believe that sigma ~ 5 cm, so I'll define the obstacles to be w/in 10 cm
% of where they're placed the the LADARs and state estimate. Note that this
% is only in distance, not angle. The ladars only have 1deg angular
% resolution, so at 30 m distance, there's up to 50
%
% I don't think that it makes sense to check point-by-point, because if an
% obstacle has moved only a little, it'll still be w/in the same area, and
% classified as part of the same object...better to compare positions of
% the whole, not of individual returns.

% Checking if each point is in unknown space
numReturns = length(x2)
inUnknown = zeros(row+1, numReturns);
for i=1:1:(row+1)
    inUnknown(i,:) = inpolygon(x2, y2, spaceUnknown(i,1,:), spaceUnknown(i,2,:));
end
inObscured = zeros(row, numReturns);
for i=1:1:row
    inObscured(i,:) = inpolygon(x2, y2, spaceObscured(i,1,:), spaceObscured(i,2,:));
end
inOccupied = zeros(row, numReturns);
for i=1:1:row
    inOccupied(i,:) = inpolygon(x2, y2, spaceOccupied(i,1,:), spaceOccupied(i,2,:));
end

sumUnknown = sum(inUnknown,1);
sumObscured = sum(inObscured,1);
sumOccupied = sum(inOccupied,1);

% checks for the cases where there was no LADAR return
for i=1:1:numReturns
    if newRanges(i) == 0
        noReturn(i) = 1;
    else
        noReturn(i) = 0;
    end
end

inFree = 1 - or(sumUnknown, or(sumObscured, or(sumOccupied,noReturn)));


figure(4);
imagesc([sumUnknown; sumObscured; sumOccupied;inFree;noReturn]);

end