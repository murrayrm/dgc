function debugSegmentation(index)
% This function reads in a file containing all of the points, and a file
% with the segmented indexes. It will then plot each segment as a different
% color, as well as a line connecting the endpoints.
% Used for checking the segmentScan() function in trackMO

%contains two rows for each scan...first for Xcoords, second for Ycoords
points = load('points.dat');
size(points)
%contains one row for each scan. the index(Cstyle) of the first pt in segment,
%then the index of the last point in segment. not that c's indices start at
%0, while matlab's start at 1. after all segments have been recorded,
%values are reported as -1
segments = load('segments.dat');
[row,col] = size(segments);
segs = segments(index,:);


xpts = points(2*index - 1,:);
ypts = points(2*index,:);

figure(); hold on;
%axis([398960,399020,3781280,3781340]);
%   axis([398960,399040,3781240,3781340]);
plot(xpts,ypts,'k.');

%plot each segment a different color, and draw the line connecting the
%endpoints
for i=1:1:col/2
    if segs(2*i) ~= -1
        startPt = segs(2*i-1)+1;
        endPt = segs(2*i)+1;
        xtemp = xpts(startPt:endPt);
        ytemp = ypts(startPt:endPt);
        xends = [xpts(startPt), xpts(endPt)];
        yends = [ypts(startPt), ypts(endPt)];        
        
        plot(xtemp,ytemp,'.','MarkerEdgeColor',rand(1,3));
        plot(xends,yends,'k');
    end
end