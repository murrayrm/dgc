#include "trackMO.hh"
#include <getopt.h>

#include "DGCutils"
#include "sn_msg.hh"

#define FILE_OPT           1

/* The name of this program. */
const char * program_name;
char* logNamePrefix = NULL;

int main(int argc, char **argv) 
{
  /* Set the default arguments that won't need external access here. */
  cout<<"entering trackMOmain"<<endl;


  /* Temporary character. */
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
    static struct option long_options[] = 
    {
      // first: long option (--option) string
      // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
      // third: if pointer, set variable to value of fourth argument
      //        if NULL, getopt_long returns fourth argument
      {"file",        1, NULL,               FILE_OPT},
      {"help",        0, NULL,               'h'},
      {NULL,          0, NULL,               0}
    };


  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");


  /* log file to read in from */
  char scanFile[256];
  scanFile[0] = '\0';

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
    {
      switch(ch)
	{
	case FILE_OPT:
	  strcpy(scanFile, optarg);
	  cout<<"got a file name!!"<<endl;
	  break;
      
	case 'h':
	  //print_usage(stdout, 0);
	  cout<<"This should be a help msg"<<endl;

	case '?': // The user specified an invalid option. 
	  //	  print_usage(stderr, 1);

        case -1: // Done with options. 
	  break;
	}
}
    


  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");


  if( pSkynetkey == NULL )
    {
      cerr << "SKYNET_KEY environment variable isn't set" << endl;
    }
  else
  sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  trackMO myTrackMO(sn_key,scanFile);
  cerr << "Done constructing TrackMO" << endl;

  cout<<"Using scans from file: "<<scanFile<<endl;

  if(scanFile[0] == '\0')
    DGCstartMemberFunctionThread(&myTrackMO, &trackMO::Comm);
  else
    DGCstartMemberFunctionThread(&myTrackMO, &trackMO::ReadFile);

  myTrackMO.Active();

  return 0;
} // end main()
