#ifndef TPLANNER__HH
#define TPLANNER__HH

// TODO: put in some sort of error handling (at least asserts)

#include "Environment.hh"
#include <iostream>
#include <iomanip>

using namespace std;

/*! \brief The traffic planner uses information from other modules to 
 * determine future legal and strategic paths.
 *
 * The traffic planner uses information from sensing, 
 * RNDF, and other modules to determine possible legal (and, at this time, 
 * strategic) routes which it sends to the dynamic/path planner. 
 *
 * Follows these basic steps:
 * - Creates an Environment and fills in all the variables
 * - Determines location, and then enters either the navRoad or 
 *   navIntersection functions (future: navZone for parking lots, etc)
   - Stays in this function, using the Environment::getPossibleStates() 
 *   to determine best course of action, until a change of location
 *   happens (ex: we queue up at an intersection)
 * - Every time one of these functions completes an iteration, it sends
 *   the next action to corridorGen, which will generate a corridor to
 *   send to the path planner.
 */
class TPlanner {

public:

  /* Constructor. */
  TPlanner();
  /* Destructor. */
  ~TPlanner();

  /* Whether or not this is an interactive session */
  bool interactive;
  /* Whether this is a simulation */
  bool simulation;

  /* The skynet key */
  int skynet_key;

  /* The RNDF file */
  char* rndf_filename;
  /* The astateSim file */
  char* sim_filename;
  /* The lane boundaries file */
  char* lanes_filename;

  /*! Sets up the traffic planner */
  void setup();

  /*! Runs an iteration of the traffic planner. */
  void runIteration();

  /*! Uses the Environment to print the road to stdout. */
  void printRoad();

private:

  /*! The current status 
   * 0 => continue on road
   * 1 => need to change segment (ie, new Environment)
   * 2 => change to intersection mode
   */
  int status;

  /* The Environment */
  Environment* env;

  /*! Used when navigating a normal stretch of road. Uses 
   * Environment::getPossibleStates() to determine possible future states.
   * Eventually will send these states to corridorGen to make a weighted
   * corridor representation.
   * 
   * \param env The current Environment.
   */
  void navRoad();

  /*! Navigates an intersection. Only runs once per intersection. Once it is
   * our turn to go, uses corridorGen to send a path to the next segment to
   * the path planner.
   *
   * \param env The current environment
   */
  void navIntersection();

  /*! Generates a weighted corridor that represents the desirability of
   * future actions.
   * Note: This is not yet implemented and may very well be implemented
   * by the path planner or some module in between.
   */
  void corridorGen(double *nextActions);

};

#endif
