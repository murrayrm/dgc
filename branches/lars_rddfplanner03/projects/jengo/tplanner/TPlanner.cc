#include "TPlanner.hh"
#include <iostream>
#include <iomanip>

using namespace std;

TPlanner::TPlanner()
{
  cout<<"** TPlanner constructor **"<<endl;

  interactive = false;
  simulation = false;
  /*  rndf_filename = RNDF_FILENAME;
  sim_filename = SIM_FILENAME;
  lanes_filename = LANES_FILENAME; */
}

TPlanner::~TPlanner()
{
  cout<<"** TPlanner destructor **"<<endl;

  delete env;
  delete[] rndf_filename;
  delete[] sim_filename;
  delete[] lanes_filename;
}


void TPlanner::setup()
{

  if (interactive) {
    cout<<"Running in interactive mode."<<endl;
  }
  if (simulation) {
    cout<<"Running in astate simulation mode."<<endl;
    cout<<"  Using simulation file: "<<sim_filename<<endl;
    cout<<"  Using lane boundaries file: "<<lanes_filename<<endl;
  }
  cout<<"Using RNDF: "<<rndf_filename<<endl;

  if (simulation) {
    env = new SimulationEnvironment(rndf_filename,
				    skynet_key,
				    sim_filename,
				    lanes_filename);
  }
  else if (interactive) {
    env = new InteractiveEnvironment(rndf_filename);
  }
  else {
    env = new AliceEnvironment(rndf_filename, skynet_key);
  }

  cout<<"Running setup: "<<endl;
  env->setup();

  // set to nav road
  // TODO: if the computers crash or something like that, and tplanner is
  // restarted in the middle of a mission, this is NOT a good assumption to
  // make. so maybe get this information from where we are on the map or 
  // the route planner or something
  status = 0;

}


void TPlanner::runIteration()
{

  cout<<"Running an iteration"<<endl;

  ////////////////////////////
  // Update the Environment //
  ////////////////////////////

  env->update();
    
  /////////////////////
  // Navigate a road //
  /////////////////////

  if (status == 0) {
    navRoad();
  }

  //////////////////////////////
  // Navigate an intersection //
  //////////////////////////////

  else if (status == 2) {
    navIntersection();   
  } 

  /////////////////////
  // Change segments //
  /////////////////////

  else if (status == 1) {
    delete env;
    setup();   
  }

  else {
    cout<<"Error: unknown status.";
    exit(0);
  }

}

void TPlanner::printRoad()
{
  env->printRoad();
}

void TPlanner::navRoad()
{
  
  cout<<"Calculating possible states for next iteration... "<<endl;
  double* possibleStates = env->getPossibleStates();
  
  cout<<endl;
  cout<<"Possible states: " << endl;
  for (int i=0; i<NUM_STATES; i++) {
    if (possibleStates[i]) { // if ith state is possible
      cout <<"  "<< modes[i] <<", "<<possibleStates[i]<< endl;
    }
  }

  // determine which mode is the best
  int bestChoice = 1;
  double highest = 0;
  for (int i=NUM_STATES-1; i>0; i--) {
    if (possibleStates[i]>=highest) {
      highest = possibleStates[i];
      bestChoice=i;
    }
  }

  cout<<endl<<"Decision:"<<endl;
  
  if (bestChoice==1) {
    // Stop
    cout<<"  Stopping and going into intersection mode."<<endl;
    status = 2;
    return;
  }
  else if (bestChoice==2) {
    // Continue driving
    cout<<"  Continue driving in same lane."<<endl;
    status = 0;
    return;
  }
  else if (bestChoice==3) {
    // change lanes to left
    cout<<"  Changing lanes to the left."<<endl;
    status = 0;
    return;
  }
  else if (bestChoice==4) {
    // change lanes to right
    cout<<"  Changing lanes to the right."<<endl;
    status = 0;
    return;
  }
  else if (bestChoice==5) {
    // Pass
    cout<<"  Passing stopped vehicle."<<endl;
    cout<<"  Stop, wait for left lane to be clear, then"<<endl;
    cout<<"  Send a corridor that passes around the vehicle at the";
    cout<<" correct distance."<<endl;
    status = 0;
    return;
  }

  // if we get here, the environment has been too constraining
  // continue in current lane, but reduce speed
  cout<<"No possible states found. Continue in same lane but reduce speed.";
  cout<<endl;

  status = 0;
} /* end of navRoad */


void TPlanner::navIntersection() {

  char input;

  // what we want to know about other cars:
  //   what section of the intersection are they in?
  //   are they in front of us in the queue?
  //   

  bool ignore1, ignore2,ignore3;
  ignore1 = ignore2 = ignore3 = false;
  // queuePos is Alice's queue position
  // The sizes give the number of cars in each intersection position
  int size0, size1, size2, size3, queuePos;
  size0 = size1 = size2 = size3 = queuePos = 0;
  
  cout<<endl;
  // build the intersection
  cout<<"    |"<<endl;
  cout<<"    2"<<endl;
  cout<<"    |"<<endl;
  cout<<"--3-+-1--"<<endl;
  cout<<"    |"<<endl;
  cout<<"    0"<<endl;
  cout<<"    |"<<endl;

  cout<<"We can ignore a segment if:"<<endl;
  cout<<" There is no road there"<<endl;
  cout<<" The road is one-way leading away from intersection"<<endl;
  cout<<" There is not a stop sign there"<<endl<<endl;

  cout<<"Ignore 1? (y/n)  ";
  cin>>input;
  if (input=='y') ignore1 = true;

  cout<<"Ignore 2? (y/n)  ";
  cin>>input;
  if (input=='y') ignore2 = true;

  cout<<"Ignore 3? (y/n)  ";
  cin>>input;
  if (input=='y') ignore3 = true;

  double destLane;
  cout<<endl<<"Destination lane? ";
  cin>>destLane;

  // Stage 1: We're behind 1 or more cars before the stop line
  cout<<"How many cars in front of Alice (at 0)? ";
  cin>>size0;

  for (int i = size0; i>0; i--) {
    cout<<endl;
    cout<<i<<" cars in front of Alice."<<endl;

    cout<<"Here tplanner would wait for sensing to tell it when the car"<<endl;
    cout<<"in front has moved. If it doesn't move, tplanner alerts"<<endl;
    cout<<"supercon that we have a possible traffic jam situation."<<endl;

    cout<<"Continue? (y,q) ";
    cin >> input; if (input=='q') { exit(0); }
  }

  // Stage 2: We're at the stop line

  cout<<endl;
  cout<<"Alice is now at the stop line. We now care about the cars at"<<endl;
  cout<<"other parts of the intersection. (n,q) ";
  cin >> input; if (input=='q') { exit(0); }

  if(!ignore1) {
    cout<<"Car at 1? (y/n) ";
    cin>>input;
    if (input=='y') size1++; queuePos++;
  }

  if(!ignore2) {
    cout<<"Car at 2? (y/n) ";
    cin>>input;
    if (input=='y') size2++; queuePos++;
  }

  if(!ignore3) {
    cout<<"Car at 3? (y/n) ";
    cin>>input;
    if (input=='y') size3++; queuePos++;
  }
  
  cout<<endl<<"Once again, we wait for sensing to tell us when the cars"<<endl;
  cout<<" are gone and let supercon know what's going on the whole time.";
  cout<<" (n, q) ";
  cin>>input; if (input=='q') { exit(0); }
  
  if (size1)
    cout<<"Car 1 leaves"<<endl;
  if (size2)
    cout<<"Car 2 leaves"<<endl;
  if (size3)
    cout<<"Car 3 leaves"<<endl;

  // Stage 3: Our turn to go 
  cout<<endl;
  cout<<"It's now Alice's turn to go. So we send a corridor to the"<<endl;
  cout<<"planner and we go to the next segment. (n,q) ";
  cin>>input; if (input=='q') { exit(0); }

  cout<<"Entering lane "<<destLane<<"."<<endl;

  // changing segments
  status = 1;

} /* end of navIntersection */


// TODO: fill this in
void TPlanner::corridorGen(double *nextActions) { }

