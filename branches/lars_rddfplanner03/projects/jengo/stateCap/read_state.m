% Takes in a stateCap state log and plots the velocity vs time and 
% the acceleration vs time

clear all;

% column 1: time (tenths of a second)
% 2: position, N
% 3: position, E
% 4: velocity, m/s
% 5: acceleration, m/s2

state = dlmread('state_log_20061103_172432.log');

state(:,2) = state(:,2) - state(1,2);
state(:,3) = state(:,3) - state(1,3);

for (i=1:size(state,1))
    
    norm(i) = sqrt(state(i,2)^2 + state(i,3)^2)
end

start = 1;
finish = size(state,1);

plot(state(start:finish,1),state(start:finish,4), ...
    state(start:finish,1),state(start:finish,5) ...
    ,state(start:finish,1),norm);
legend('show');
legend('velocity (m/s)','acceleration (m/s^2)');
xlabel('time (sec)');
ylabel('Alice state');
grid on;