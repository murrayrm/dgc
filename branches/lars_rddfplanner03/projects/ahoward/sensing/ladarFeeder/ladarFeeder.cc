
/* 
 * Desc: Ladar feeder module using JPLV
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sparrow/display.h>

#include <jplv/pose3.h>

#include "sn_msg.hh"
#include "sn_types.h"
#include "VehicleState.hh"
#include "sensnet.h"
#include "sensnet_ladar.h"

#include "ladarFeederOpt.h"



/// @brief Ladar feeder class
class LadarFeeder
{
  public:   

  /// Default constructor
  LadarFeeder();

  /// Default destructor
  ~LadarFeeder();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);

  /// Initialize feeder for live capture
  int initLive();
  
  /// Finalize feeder for live capture
  int finiLive();

  /// Capture a scan 
  int captureLive();
  
  /// Initialize feeder for log replay
  int initReplay(const char *logdir);

  /// Finalize feeder for log replay
  int finiReplay();

  /// Capture a scan from the log
  int captureReplay();

  /// Initialize feeder for simulated capture
  int initSim();
  
  /// Finalize feeder for simulated capture
  int finiSim();

  /// Capture a simulated scan 
  int captureSim();

  /// Get the predicted vehicle state
  int getState(double timestamp);
  
  /// Initialize everything except capture
  int init();

  /// Finalize everything except capture
  int fini();
  
  /// Process a scan
  int process();

  /// Write the current images to the log
  int writeLog();

  /// Publish data over sensnet
  int writeSensnet();

  public:

  /// Sparrow callback; occurs in sparrow thread
  static int onUserQuit(long);

  /// Sparrow callbacks; occurs in sparrow thread
  static int onUserPause(long);
    
  /// Sparrow callbacks; occurs in sparrow thread
  static int onUserLog(long);
  
  /// Sparrow thread
  pthread_t spThread;

  /// Mutex for thead sync
  pthread_mutex_t spMutex;

  public:

  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon; int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;
  char *moduleName;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;
  char *sensorName;

  // What mode are we in?
  enum {modeLive, modeReplay, modeSim} mode;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Is logging enabled?
  bool logging;

  // Id of the ladar
  int ladarId;

  // Port we are talking to
  const char *ladarPort;

  // Log file (reading or writing, depending on mode)
  // TODO dgc_ladar_log_t *log;
  
  // Sensor-to-vehicle transform
  float sens2veh[4][4];

  // Calibration stats (display only).
  // Sensor position (x,y,z) and rotation (roll,pitch,yaw).
  vec3_t sensPos, sensRot;
  
  // SensNet context
  sensnet_t *sensnet;

  // Current scan id
  int scanId;

  // Current scan time
  double scanTime;

  // Current state data
  sensnet_state_t state;

  // Published blob data
  sensnet_ladar_blob_t blob;

  // Start time for computing stats
  double startTime;
  
  // Capture stats
  int capCount;
  double capTime, capRate, capPeriod;

  // Logging stats
  int logCount, logSize;
};


// Pointer to the one instance of this class.  Sparrow
// needs this in order to work correctly.
static LadarFeeder *self;


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stdout, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
LadarFeeder::LadarFeeder()
{
  memset(this, 0, sizeof(*this));

  this->scanId = -1;

  return;
}


// Default destructor
LadarFeeder::~LadarFeeder()
{
  return;
}


// Parse the command line
int LadarFeeder::parseCmdLine(int argc, char **argv)
{
  char *configDir;
  char filename[256];
  
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleName = this->options.module_id_arg;
  this->moduleId = modulenamefromString(this->moduleName);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->moduleName);
  
  // Fill out sensor id
  this->sensorName = this->options.sensor_id_arg;
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // Location for configuration files
  configDir = ".";
  
  // Load options from the configuration file
  snprintf(filename, sizeof(filename), "%s/%s.CFG", configDir, this->sensorName);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    return ERROR("unable to process configuration file %s", filename);

  // Parse transform
  float px, py, pz;
  float rx, ry, rz;
  sscanf(this->options.sens_pos_arg, "%f %f %f", &px, &py, &pz);
  sscanf(this->options.sens_rot_arg, "%f %f %f", &rx, &ry, &rz);

  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2veh);
  
  // Record euler angles for display only
  this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;

  return 0;
}


// Initialize feeder for live capture
int LadarFeeder::initLive()
{
  /* TODO
  char filename[256];

  // Fill out the ladar id
  this->ladarId = atoi(ladarId);
  if (this->ladarId <= 0)
    return ERROR("invalid ladar id: %s", this->ladarId);

  // Initialize the ladar
  this->ladar = bb_ladar_alloc(ladarId);
  if (!this->ladar)
    return ERROR("unable to connect to ladar %s", ladarId);
  if (bb_ladar_init(this->ladar, ladarConfig) != 0)
    return -1;

  // Record key image dimensions
  this->imageLevel = this->options.level_arg;  
  this->imageCols = this->ladar->image.width >> this->imageLevel;
  this->imageRows = this->ladar->image.height >> this->imageLevel;
  this->imageChannels = 1; // HACK this->ladar->image.channels;

  MSG("image %dx%dx%d", this->imageCols, this->imageRows, this->imageChannels);
      
  // Load ladar model
  snprintf(filename, sizeof(filename), "%s/%d-left.cahvor", configDir, this->ladarId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->leftModel, filename) != 0)
    return ERROR("unable to load ladar model %s : %s", filename, jplv_error_str());

  // Load ladar model
  snprintf(filename, sizeof(filename), "%s/%d-right.cahvor", configDir, this->ladarId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->rightModel, filename) != 0)
    return ERROR("unable to load ladar model %s : %s", filename, jplv_error_str());

  // Load ladar transform
  snprintf(filename, sizeof(filename), "%s/%d.transform", configDir, this->ladarId);
  MSG("loading %s", filename);
  if (dgc_transform_read(this->cam2veh, filename) != 0)
    return ERROR("unable to load ladar transform %s : %s", filename, jplv_error_str());

  // Create buffer for raw image capture
  this->capImage = jplv_image_alloc(this->imageCols << this->imageLevel,
                                    this->imageRows << this->imageLevel,
                                    this->imageChannels, 16, 0, NULL);

  if (this->options.enable_log_flag)
  {
    time_t t;
    char logdir[1024];
    dgc_ladar_log_header_t header;

    // Construct log name
    t = time(NULL);
    strftime(logdir, sizeof(logdir), "ladar-%F-%a-%H-%M", localtime(&t));
    
    // Create log file
    this->log = dgc_ladar_log_alloc();
    assert(this->log);

    // Construct log header
    memset(&header, 0, sizeof(header.channels));
    header.ladar_id = this->ladarId;
    header.cols = this->imageCols;
    header.rows = this->imageRows;
    header.channels = this->imageChannels;

    // Open log file for writing
    if (dgc_ladar_log_open_write(this->log, logdir, &header) != 0)
      return ERROR("unable to open log: %s", logdir);

    // Write ladar models to log
    snprintf(filename, sizeof(filename), "%s/%d-left.cahvor", logdir, this->ladarId);
    jplv_cmod_write(&this->leftModel, filename, NULL);
    snprintf(filename, sizeof(filename), "%s/%d-right.cahvor", logdir, this->ladarId);
    jplv_cmod_write(&this->rightModel, filename, NULL);
    snprintf(filename, sizeof(filename), "%s/%d.transform", logdir, this->ladarId);
    dgc_transform_write(this->cam2veh, filename);
  }
  */
  
  this->mode = modeLive;
  
  return 0;
}


// Finalize feeder for live capture
int LadarFeeder::finiLive()
{
  /* TODO
  if (this->log)
  {
    dgc_ladar_log_close(this->log);
    dgc_ladar_log_free(this->log);
    this->log = NULL;
  }
  
  bb_ladar_fini(this->ladar);
  bb_ladar_free(this->ladar);
  this->ladar = NULL;
  */
    
  return 0;
}


// Capture a scan 
int LadarFeeder::captureLive()
{
  /* TODO
  int exp;
  jplv_image_t *leftImage, *rightImage;
  
  // Get pointers to the raw image buffers
  leftImage = jplv_ladar_get_image(self->ladar,
                                    JPLV_LADAR_STAGE_RAW, JPLV_LADAR_LADAR_LEFT);
  rightImage = jplv_ladar_get_image(self->ladar,
                                     JPLV_LADAR_STAGE_RAW, JPLV_LADAR_LADAR_RIGHT);
    
  // Read images off the ladar
  bb_ladar_capture(this->ladar, this->capImage);

  // Demultiplex the image and measure overall image exposure
  bb_ladar_demux_mono(this->capImage, this->imageLevel, &exp, leftImage, rightImage);
  
  //MSG("%d %d", this->scanId, exp);

  // Set measured exposure
  bb_ladar_auto_gain(this->ladar, 127, exp);
  
  this->scanId += 1;
  this->scanTime = this->ladar->timestamp;

  // Get the matching state data
  if (this->getState(this->scanTime) != 0)
    return -1;
  */
  
  return 0;
}


// Initialize feeder for log replay
int LadarFeeder::initReplay(const char *logdir)
{
  /* TODO
  char filename[256];
  dgc_ladar_log_header_t header;
  
  // Open log file for reading
  this->log = dgc_ladar_log_alloc();
  assert(this->log);
  if (dgc_ladar_log_open_read(this->log, logdir, &header) != 0)
    return ERROR("unable to open log: %s", logdir);

  MSG("image %dx%dx%d", header.cols, header.rows, header.channels);

  this->imageLevel = 0;  
  this->imageCols = header.cols;
  this->imageRows = header.rows;
  this->imageChannels = header.channels;

  // Fille out the ladar id
  this->ladarId = header.ladar_id;

  // Load ladar model
  snprintf(filename, sizeof(filename), "%s/%d-left.cahvor", logdir, this->ladarId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->leftModel, filename) != 0)
    return ERROR("unable to load ladar model %s : %s", filename, jplv_error_str());
  
  // Load ladar model
  snprintf(filename, sizeof(filename), "%s/%d-right.cahvor", logdir, this->ladarId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->rightModel, filename) != 0)
    return ERROR("unable to load ladar model %s : %s", filename, jplv_error_str());

  // Ladar transform
  snprintf(filename, sizeof(filename), "%s/%d.transform", logdir, this->ladarId);
  MSG("loading %s", filename);
  if (dgc_transform_read(this->cam2veh, filename) != 0)
    return ERROR("unable to load ladar transform %s : %s", filename, jplv_error_str());
  
  this->mode = modeReplay;
  */
  
  return 0;
}


// Finalize feeder for log replay
int LadarFeeder::finiReplay()
{
  /* TODO
  dgc_ladar_log_close(this->log);
  dgc_ladar_log_free(this->log);
  this->log = NULL;
  */
  
  return 0;
}


// Capture a scan from the log
int LadarFeeder::captureReplay()
{
  /* TODO
  dgc_ladar_log_tag_t tag;
  jplv_image_t *leftImage, *rightImage;
  
  // Get pointers to the raw image buffers
  leftImage = jplv_ladar_get_image(self->ladar,
                                    JPLV_LADAR_STAGE_RAW, JPLV_LADAR_LADAR_LEFT);
  rightImage = jplv_ladar_get_image(self->ladar,
                                     JPLV_LADAR_STAGE_RAW, JPLV_LADAR_LADAR_RIGHT);

  // TESTING
  usleep(100000);

  // Read image data from log
  if (dgc_ladar_log_read(this->log, &tag, leftImage->data_size,
                          leftImage->data, rightImage->data) != 0)
    return ERROR("unable to read log");

  this->scanId = tag.scanid;
  this->scanTime = tag.timestamp;
  this->state = tag.state;
  */
  
  return 0;
}



// Initialize feeder for simulated capture
int LadarFeeder::initSim()
{
  //char filename[256];

  // Fill out the ladar id
  /* TODO
  this->ladarId = atoi(ladarId);
  if (this->ladarId <= 0)
    return ERROR("invalid ladar id: %s", ladarId);
  */

  this->ladarPort = "sim";
  
  this->mode = modeSim;
  
  return 0;
}


// Finalize feeder for simulated capture
int LadarFeeder::finiSim()
{    
  return 0;
}


// Capture a simulated scan 
int LadarFeeder::captureSim()
{
  double timestamp;
  int i;
  float a, b, d;
  float pt, pr;

  // Simulate 75Hz
  usleep(13000);
  
  timestamp = dgc_gettimeofday();

  // Get the current state data
  if (this->getState(timestamp) != 0)
    return -1;

  // MAGIC
  this->blob.num_ranges = 181;
  this->blob.center = 90;
  this->blob.scale = M_PI/180;

  a = this->blob.sens2veh[2][0];
  b = this->blob.sens2veh[2][1];
  d = this->blob.sens2veh[2][3] - VEHICLE_TIRE_RADIUS;
  
  // Compute intersection with nominal ground plane
  for (i = 0; i < this->blob.num_ranges; i++)
  {
    // Range and bearing of point on the ground (sensor frame)
    pt = (i - this->blob.center) * this->blob.scale;
    pr = -d / (a * cos(pt) + b * sin(pt));
    if (pr < 0 || pr > 80 || !finite(pr)) // MAGIC
      pr = 80;
    this->blob.ranges[i] = pr;
  }
  
  this->scanId += 1;
  this->scanTime = timestamp;

  return 0;
}


// Initialize everything except image capture
int LadarFeeder::init()
{    
  if (true)
  {
    // Initialize SensNet
    this->sensnet = sensnet_alloc();
    if (sensnet_connect(this->sensnet,
                        this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
      return ERROR("unable to connect to sensnet");

    // Subscribe to vehicle state messages
    if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState), 10) != 0)
      return ERROR("unable to join state group");
  }

  if (!this->options.disable_sparrow_flag)
  {
    // This looks weird, but we include the display table here so that
    // the pointers are initialized correctly.  The table is allocated
    // statically, so we can only have one instance of this class.
    #include "ladarFeederSp.h"

    // Initialize CLI
    if (dd_open() < 0)
      return ERROR("unable to open display");
    dd_usetbl(display);

    // Bind keys
    dd_bindkey('q', onUserQuit);
    dd_bindkey('Q', onUserQuit);
    dd_bindkey('p', onUserPause);
    dd_bindkey('P', onUserPause);
    
    // Kick off thread for console interface
    pthread_mutex_init(&this->spMutex, NULL);
    pthread_create(&this->spThread, NULL, (void* (*) (void*)) dd_loop, this);
  }

  return 0;
}


// Finalize everything except capture
int LadarFeeder::fini()
{
  // Clean up the CLI
  if (!this->options.disable_sparrow_flag)
  {
    pthread_join(this->spThread, NULL);
    pthread_mutex_destroy(&this->spMutex);
    dd_close();
  }
  
  // Clean up SensNet
  if (sensnet_disconnect(this->sensnet) != 0)
    return ERROR("unable to connect to sensnet");
  sensnet_free(this->sensnet);
  
  return 0;
}


// Get the predicted vehicle state
int LadarFeeder::getState(double timestamp)
{
  int blob_id, blob_len;
  VehicleState state;
  double northOffset, eastOffset;
  pose3_t pose;

  // Get the state newest data in the cache
  if (sensnet_peek(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, &blob_id, &blob_len) != 0)
    return -1;
  if (blob_id >= 0)
  {
    if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR,
                     SNstate, blob_id, sizeof(state), &state) != 0)
      return -1;
  }
  else
  {
    memset(&state, 0, sizeof(state));
  }

  // TODO: do prediction by comparing timestamps
  this->state.timestamp = (double) state.Timestamp / 1e6;

  // HACK TESTING
  northOffset = state.Northing - fmod(state.Northing, 1000);
  eastOffset = state.Easting - fmod(state.Easting, 1000);

  // Set local pose
  this->state.pose_local[0] = state.Northing - northOffset;
  this->state.pose_local[1] = state.Easting - eastOffset;
  this->state.pose_local[2] = state.Altitude;
  this->state.pose_local[3] = state.Roll;
  this->state.pose_local[4] = state.Pitch;
  this->state.pose_local[5] = state.Yaw;

  // Set local transform
  pose.pos = vec3_set(state.Northing - northOffset, state.Easting - eastOffset, 0);
  pose.rot = quat_from_rpy(state.Roll, state.Pitch, state.Yaw);
  pose3_to_mat44d(pose, this->state.homo_local);

  // Set global pose
  this->state.pose_global[0] = state.Northing;
  this->state.pose_global[1] = state.Easting;
  this->state.pose_global[2] = state.Altitude;
  this->state.pose_global[3] = state.Roll;
  this->state.pose_global[4] = state.Pitch;
  this->state.pose_global[5] = state.Yaw;

  // Set global transform
  pose.pos = vec3_set(state.Northing, state.Easting, 0);
  pose.rot = quat_from_rpy(state.Roll, state.Pitch, state.Yaw);
  pose3_to_mat44d(pose, this->state.homo_global);

  // Set velocities
  this->state.vel_vehicle[0] = state.Vel_N;
  this->state.vel_vehicle[1] = state.Vel_E;
  this->state.vel_vehicle[2] = state.Vel_D;
  this->state.vel_vehicle[3] = state.RollRate;
  this->state.vel_vehicle[4] = state.PitchRate;
  this->state.vel_vehicle[5] = state.YawRate;

  //MSG("state %.3f %.3f %.3f", (double) state.Timestamp * 1e-6, state.Northing, state.Easting);
  
  return 0;
}


// Process a scan
int LadarFeeder::process()
{

  // TODO?
  
  return 0;
}


// Write the current images to the log
int LadarFeeder::writeLog()
{
  /* TODO
  dgc_ladar_log_tag_t tag;
  jplv_image_t *leftImage, *rightImage;
  
  if (!this->log)
    return 0;

  // Construct the image tag
  tag.scanid = this->scanId;
  tag.timestamp = this->scanTime;
  tag.state = this->state;
  
  // Get current raw images
  leftImage = jplv_ladar_get_image(this->ladar,
                                    JPLV_LADAR_STAGE_RAW, JPLV_LADAR_LADAR_LEFT);
  rightImage = jplv_ladar_get_image(this->ladar,
                                     JPLV_LADAR_STAGE_RAW, JPLV_LADAR_LADAR_RIGHT);

  // Write to log
  if (dgc_ladar_log_write(this->log, &tag,
                           leftImage->data_size, leftImage->data, rightImage->data) != 0)
    return ERROR("unable to write to log");

  // Update stats
  this->logCount++;
  this->logSize = this->logCount * leftImage->data_size * 2 / 1024 / 1024;
  */
  
  return 0;
}


// Publish data
int LadarFeeder::writeSensnet()
{
  this->blob.blob_type = SENSNET_LADAR_BLOB;
  this->blob.sensor_id = this->sensorId;
  this->blob.scanid = this->scanId;
  this->blob.timestamp = this->scanTime;
  this->blob.state = this->state;

  // Do a sanity check; if the timestamps are bogus, dont sent the data
  if (fabs(this->blob.timestamp - this->blob.state.timestamp) > 0.100)
    return MSG("mismatched timestamps %.3f %.3f; discarding blob",
               this->blob.timestamp, this->blob.state.timestamp);
  
  // Copy transform
  assert(sizeof(this->sens2veh) == sizeof(this->blob.sens2veh));
  memcpy(this->blob.sens2veh, this->sens2veh, sizeof(this->sens2veh));

  // TODO copy scan
  
  // Write blob
  if (sensnet_write(this->sensnet, this->sensorId, SENSNET_LADAR_BLOB,
                    this->scanId, sizeof(this->blob), &this->blob) != 0)
    return ERROR("unable to write blob");

  return 0;
}


// Handle user events; occurs in sparrow thread
int LadarFeeder::onUserQuit(long)
{
  assert(self);
  self->quit = true;
  return DD_EXIT_LOOP;
}


// Handle user events; occurs in sparrow thread
int LadarFeeder::onUserPause(long)
{
  assert(self);
  self->pause = !self->pause;
  return 0;
}


// Handle user events; occurs in sparrow thread
int LadarFeeder::onUserLog(long)
{
  assert(self);
  self->logging = !self->logging;
  return 0;
}



// Main program thread
int main(int argc, char **argv)
{
  int status;

  // Create feeder
  self = new LadarFeeder();
  assert(self);
 
  // Parse command line options
  if (self->parseCmdLine(argc, argv) != 0)
    return -1;

  if (self->options.sim_flag)
  {
    // Initialize for simulated capture
    if (self->initSim() != 0)
      return -1;
  }
  else if (self->options.replay_given)
  {
    // Initialize for replay
    if (self->initReplay(self->options.replay_arg) != 0)
      return -1;
  }
  else
  {
    // Initialize for live capture
    if (self->initLive() != 0)
      return -1;
  }

  // Initialize everything
  if (self->init() != 0)
    return -1;

  self->startTime = dgc_gettimeofday();
  
  // Start processing
  while (!self->quit)
  {
    // Capture incoming scan directly into the ladar buffers
    if (self->mode == LadarFeeder::modeSim)
    {
      if (self->captureSim() != 0)
        break;
    }
    else if (self->mode == LadarFeeder::modeReplay)
    {
      if (self->captureReplay() != 0)
        break;
    }
    else
    {
      if (self->captureLive() != 0)
        break;
    }

    // Compute some diagnostics
    self->capCount += 1;
    self->capTime = dgc_gettimeofday() - self->startTime;
    self->capRate = (float) self->capCount / self->capTime;
    self->capPeriod = 1000.0 / self->capRate;
    
    // If paused, give up our time slice.
    if (self->pause)
    {
      usleep(0);
      continue;
    }
    
    // Process one scan
    pthread_mutex_lock(&self->spMutex);
    status = self->process();
    pthread_mutex_unlock(&self->spMutex);
    if (status != 0)
      break;

    // Publish data
    if (self->writeSensnet() != 0)
      break;
    
    // Write to the log
    if (self->options.enable_log_flag && self->logging)
      self->writeLog();
  }
  
  // Clean up
  self->fini();
  if (self->mode == LadarFeeder::modeSim)
    self->finiSim();
  else if (self->mode == LadarFeeder::modeReplay)
    self->finiReplay();
  else
    self->finiLive();
  delete self;

  MSG("program exited cleanly");
  
  return 0;
}
