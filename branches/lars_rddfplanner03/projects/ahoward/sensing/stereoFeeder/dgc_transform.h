
/*
 * Desc: Functions for reading/writing sensor/vehicle transforms.
 * Date: 17 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef DGC_TRANSFORM_H
#define DGC_TRANSFORM_H

#ifdef __cplusplus
extern "C"
{
#endif

/// Write sensor-to-vehicle transform
int dgc_transform_write(float m[4][4], const char *filename);

/// Read sensor-to-vehicle transform
int dgc_transform_read(float m[4][4], const char *filename);

#ifdef __cplusplus
}
#endif

#endif
