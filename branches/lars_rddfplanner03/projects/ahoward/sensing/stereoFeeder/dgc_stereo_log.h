
/* Desc: Stereo data logging
 * Author: Andrew Howard
 * Date: 9 Nov 2005
 * CVS: $Id$
 */

#ifndef DGC_STEREO_LOG_H
#define DGC_STEREO_LOG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <rpc/xdr.h>
  
#include "sensnet_types.h"
  

/// @brief Log version number
#define DGC_STEREO_LOG_VERSION 0x02


/// @brief Log header data
typedef struct
{
  // File version (read-only)
  int version;

  // Camera id 
  int camera_id;
  
  // Image dimensions
  int cols, rows, channels;
  
} dgc_stereo_log_header_t;


/// @brief Image tag data
typedef struct
{
  // Image index (read-only)
  int index;

  // Frame id
  int frame_id;

  // Frame timestamp
  double timestamp;

  // State data
  sensnet_state_t state;
  
} dgc_stereo_log_tag_t;


/// @brief Logging module.
typedef struct
{
  /// Log directory
  char *logdir;

  // Metadata file
  FILE *file;

  // Metadata stream
  XDR xdrs;

  // Number of frames written/read
  int num_frames;

} dgc_stereo_log_t;


/// @brief Allocate object
dgc_stereo_log_t *dgc_stereo_log_alloc();

/// @brief Free object
void dgc_stereo_log_free(dgc_stereo_log_t *self);

/// @brief Open file for writing
int dgc_stereo_log_open_write(dgc_stereo_log_t *self,
                              const char *logdir, dgc_stereo_log_header_t *header);

/// @brief Open file for reading
int dgc_stereo_log_open_read(dgc_stereo_log_t *self,
                             const char *logdir, dgc_stereo_log_header_t *header);

/// @brief Close the log
int dgc_stereo_log_close(dgc_stereo_log_t *self);

/// @brief Write an image to the log
///
/// @param[in] tag Image tag.
/// @param[in] data_size Size of image data, in bytes.
/// @param[in] left_data Left image data.
/// @param[in] right_data Right image data.
int dgc_stereo_log_write(dgc_stereo_log_t *self, dgc_stereo_log_tag_t *tag,
                         int data_size, const uint8_t *left_data, const uint8_t *right_data);

/// @brief Read an image from the log
///
/// @param[out] tag Image tag.
/// @param[in] data_size Size of image data, in bytes.
/// @param[out] left_data Left image data.
/// @param[out] right_data Right image data.
int dgc_stereo_log_read(dgc_stereo_log_t *self, dgc_stereo_log_tag_t *tag,
                        int data_size, uint8_t *left_data, uint8_t *right_data);


#ifdef __cplusplus
}
#endif

#endif
