/* 
 * Desc: Functions for reading/writing sensor/vehicle transforms.
 * Date: 17 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "dgc_transform.h"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Write vehicle transform
int dgc_transform_write(float m[4][4], const char *filename)
{
  int row;
  FILE *file;

  MSG("writing transform %s", filename);
  
  file = fopen(filename, "w");
  if (!file)
    return ERROR("unable to open %s : %s", filename, strerror(errno));

  for (row = 0 ; row < 4 ; row++)
  {
    fprintf(file, "TRANSFORM %d %lf %lf %lf %lf\n",
            row, m[row][0], m[row][1], m[row][2], m[row][3]);
  }
  
  fclose(file);

  return 0;
}


// Read vehicle transform
int dgc_transform_read(float m[4][4], const char *filename)
{
  int row;
  double a, b, c, d;
  char line[256];
  FILE *file;

  MSG("loading transform %s", filename);
  
  file = fopen(filename, "r");
  if (!file)
    return ERROR("unable to open %s : %s", filename, strerror(errno));

  while (true)
  {
    if (fgets(line, sizeof(line), file) == NULL)
      break;    
    if (sscanf(line, "TRANSFORM %d %lf %lf %lf %lf\n", &row, &a, &b, &c, &d) == 5)
    {
      assert(row >= 0 && row < 4);
      m[row][0] = a;
      m[row][1] = b;
      m[row][2] = c;
      m[row][3] = d;
    }
  }
  
  fclose(file);

  return 0;
}
