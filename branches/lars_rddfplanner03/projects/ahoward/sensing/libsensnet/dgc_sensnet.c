
/* 
 * Desc: DGC sensnet library for accessing sensor data.
 * Date: 08 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sp.h>

#include "dgc_utils.h"
#include "dgc_sensnet.h"


// Reserved sensor id for Skynet messages.
// This must match the enumerated sensor id value.
#define DGC_SKYNET_SENSOR 1

// Maximum number of message types in cache
#define DGC_SENSNET_MAX_FIFOS 16


// Data packet.
typedef struct
{
  // Data source
  int sensor_id;

  // Data type
  int blob_type, blob_id, blob_len;

  // Chunks of blob data
  int chunk_offset, chunk_len;
  unsigned char chunk_data[0x8000];

} dgc_sensnet_packet_t;


// FIFO slot for one blob
typedef struct
{
  int blob_id;
  int blob_len, max_len;
  unsigned char *blob_data;
  
} dgc_sensnet_slot_t;


// FIFO queues for caching message data.
typedef struct
{
  // Sensor ID and blob type (constant)
  int sensor_id, blob_type;

  // Current blob id and len (variable)
  int current_blob_id, current_blob_len;
  
  // Number of joins 
  int num_joins;

  // Circular queue of blobs
  int num_slots, max_slots;
  dgc_sensnet_slot_t *slots;
  
} dgc_sensnet_fifo_t;


// Sensnet library context.
struct dgc_sensnet
{
  // Server name
  char *spread_name;
  
  // Spread user key
  int spread_key;

  // Private name
  char private_name[MAX_PRIVATE_NAME];
  
  // Workspace for composing group strings
  char group[MAX_GROUP_NAME];

  // Spread mail box
  int mbox;

  // Message cache: FIFO queues for each message type
  int num_fifos;
  dgc_sensnet_fifo_t fifos[DGC_SENSNET_MAX_FIFOS];

  // Thread control
  bool quit;
  pthread_t thread;
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  pthread_mutex_t cond_mutex;
};


// Library thread: read message from Spread and populate the fifos
int dgc_sensnet_main(dgc_sensnet_t *self);



// Create context and allocate resources.
dgc_sensnet_t *dgc_sensnet_alloc()
{
  dgc_sensnet_t *self;

  self = (dgc_sensnet_t*) calloc(1, sizeof(dgc_sensnet_t));
  
  return self;
}


// Destroy context and free all resources.
int dgc_sensnet_free(dgc_sensnet_t *self)
{
  int i, j;
  dgc_sensnet_fifo_t *fifo;  

  for (i = 0; i < self->num_fifos; i++)
  {
    fifo = self->fifos + i;
    for (j = 0; j < fifo->max_slots; j++)
      free(fifo->slots[j].blob_data);
  }  

  if (self->spread_name)
    free(self->spread_name);

  free(self);
    
  return 0;
}


// Initialize connection.
int dgc_sensnet_connect(dgc_sensnet_t *self,
                        const char *spread_name, int spread_key, int module_id)
{
  int status;
	char private_group[MAX_GROUP_NAME];

  if (!spread_name)
    spread_name = getenv("SPREAD_DAEMON");
  if (!spread_name)
    spread_name = "4803";
  
  if (spread_key < 0 && getenv("SPREAD_KEY"))
    spread_key = atoi(getenv("SPREAD_KEY"));
  if (spread_key < 0)
    spread_key = 0;

  assert(spread_name);
  assert(spread_key >= 0);

  self->spread_name = strdup(spread_name);
  self->spread_key = spread_key;
  snprintf(self->private_name, sizeof(self->private_name), "mod:%d", module_id);

  MSG("connecting %s %d %s", spread_name, spread_key, self->private_name);
    
  // Create connection
  status = SP_connect(spread_name, self->private_name, 0, 0, &self->mbox, private_group);
  if (status != ACCEPT_SESSION)
    return ERROR("spread connect failed: %d", status);
  
  // Start the reading thread
  if (pthread_mutex_init(&self->mutex, NULL) != 0)
    return ERROR("unable to create mutex: %s", strerror(errno));
  if (pthread_cond_init(&self->cond, NULL) != 0)
    return ERROR("unable to create condition: %s", strerror(errno));
  if (pthread_mutex_init(&self->cond_mutex, NULL) != 0)
    return ERROR("unable to create mutex: %s", strerror(errno));

  if (pthread_create(&self->thread, NULL, (void*(*)(void*)) dgc_sensnet_main, self) != 0)
    return ERROR("unable to create thread: %s", strerror(errno));

  return 0;
}


// Terminate connection.
int dgc_sensnet_disconnect(dgc_sensnet_t *self)
{
  self->quit = true;
  
  if (pthread_join(self->thread, NULL) != 0)
    return ERROR("unable to join thread: %s", strerror(errno));
  
  if (pthread_mutex_destroy(&self->mutex) != 0)
    return ERROR("unable to destroy mutex: %s", strerror(errno));
  if (pthread_cond_destroy(&self->cond) != 0)
    return ERROR("unable to destroy condition: %s", strerror(errno));
  if (pthread_mutex_destroy(&self->cond_mutex) != 0) 
    return ERROR("unable to destroy mutex: %s", strerror(errno));

  SP_disconnect(self->mbox);
  
  return 0;
}


// Get the spread server name
const char *dgc_sensnet_spread_name(dgc_sensnet_t *self)
{
  return self->spread_name;
}


// Get the spread key value
int dgc_sensnet_spread_key(dgc_sensnet_t *self)
{
  return self->spread_key;
}


// Get the spread module name
const char *dgc_sensnet_private_name(dgc_sensnet_t *self)
{
  return self->private_name;
}


// Create a group string for the given message type.
char *dgc_sensnet_make_group(dgc_sensnet_t *self, int sensor_id, int blob_type)
{
  if (sensor_id == DGC_SKYNET_SENSOR)
  {
    // Construct group name for skynet messages.
    snprintf(self->group, sizeof(self->group),
             "%d_%d", self->spread_key, blob_type);
  }
  else
  {
    // Construct group name for sensnet messages
    snprintf(self->group, sizeof(self->group),
             "sensnet:%d:%02d:%02d", self->spread_key, sensor_id, blob_type);
  }
  return self->group;
}


/* REMOVE
void skynet::makeGroupName(string* pGroupName, sn_msg type)
{
	ostringstream groupNameStream;
	groupNameStream << m_key << '_' << type;
	*pGroupName = groupNameStream.str();
	if(strlen(pGroupName->c_str()) > MAX_GROUP_NAME)
	{
		cerr << "skynet key too long. why's it so fucking long?????" << endl;
		cerr << m_key << "?!?!?!?!?!?!?!?!?!?" << endl;
		exit(-1);
	}
}
*/


// Find the FIFO for a given message type
dgc_sensnet_fifo_t *dgc_sensnet_find_fifo(dgc_sensnet_t *self, int sensor_id, int blob_type)
{
  int i;
  dgc_sensnet_fifo_t *fifo;

  for (i = 0; i < self->num_fifos; i++)
  {
    fifo = self->fifos + i;
    if (fifo->sensor_id == sensor_id && fifo->blob_type == blob_type)
      return fifo;
  }
  
  return NULL;
}


// Join a particular sensor group.
int dgc_sensnet_join(dgc_sensnet_t *self,
                     int sensor_id, int blob_type, int blob_len, int max_slots)
{
  int i, status;
  dgc_sensnet_fifo_t *fifo;

  pthread_mutex_lock(&self->mutex);
    
  // Find fifo for this message type
  fifo = dgc_sensnet_find_fifo(self, sensor_id, blob_type);
  
  if (!fifo)
  {
    MSG("joining %d:%d, %d bytes, %d slots", sensor_id, blob_type, blob_len, max_slots);
                
    // Join the group
    status = SP_join(self->mbox, dgc_sensnet_make_group(self, sensor_id, blob_type));
    if (status != 0)
    {
      pthread_mutex_unlock(&self->mutex);
      return ERROR("spread join failed: %d", status);
    }

    // Create fifo
    assert(self->num_fifos < sizeof(self->fifos) / sizeof(self->fifos[0]));
    fifo = self->fifos + self->num_fifos++;
    fifo->sensor_id = sensor_id;
    fifo->blob_type = blob_type;
    fifo->current_blob_id = -1;
    fifo->current_blob_len = -1;
    fifo->num_joins = 0;
    fifo->num_slots = 0;
    fifo->max_slots = max_slots;
    fifo->slots = calloc(max_slots, sizeof(fifo->slots[0]));
    for (i = 0; i < fifo->max_slots; i++)
    {
      fifo->slots[i].max_len = blob_len;
      fifo->slots[i].blob_data = calloc(1, blob_len);
    }
  }

  // Keep track of how many times we have joined
  fifo->num_joins++;

  pthread_mutex_unlock(&self->mutex);
    
  return 0;
}


// Leave a particular sensor group.
int dgc_sensnet_leave(dgc_sensnet_t *self, int sensor_id, int blob_type)
{
  int i, status;
  dgc_sensnet_fifo_t *fifo;

  pthread_mutex_lock(&self->mutex);
    
  // Find fifo for this message type
  fifo = dgc_sensnet_find_fifo(self, sensor_id, blob_type);
  if (!fifo)
  {
    pthread_mutex_unlock(&self->mutex);
    return ERROR("unknown blob type");
  }

  // Keep track of number of joins
  fifo->num_joins--;
  
  if (fifo->num_joins == 0)
  {
    MSG("leaving %d:%d", sensor_id, blob_type);
        
    // Leave the group
    status = SP_leave(self->mbox, dgc_sensnet_make_group(self, sensor_id, blob_type));
    if (status != 0)
    {
      pthread_mutex_unlock(&self->mutex);
      return ERROR("spread leave failed: %d", status);
    }
  
    // Delete fifo
    for (i = 0; i < fifo->max_slots; i++)
      free(fifo->slots[i].blob_data);
    free(fifo->slots);
    self->num_fifos--;
    memmove(fifo, fifo + 1, (self->num_fifos + fifo - self->fifos) * sizeof(self->fifos[0]));
  }

  pthread_mutex_unlock(&self->mutex);
    
  return 0;
}


// Write new sensor data.
int dgc_sensnet_write(dgc_sensnet_t *self, int sensor_id,
                      int blob_type, int blob_id, int blob_len, const void *blob_data)
{
  int status;
  const char *group;
  dgc_sensnet_packet_t packet;
  int i, num_chunks, chunk_len, chunk_offset;

  pthread_mutex_lock(&self->mutex);
  
  // Compute the group string
  group = dgc_sensnet_make_group(self, sensor_id, blob_type);

  // Compose packet
  packet.sensor_id = sensor_id;
  packet.blob_type = blob_type;
  packet.blob_id = blob_id;
  packet.blob_len = blob_len;

  // See how many chunks we need
  chunk_len = sizeof(packet.chunk_data);
  num_chunks = blob_len / chunk_len;
  if (chunk_len * num_chunks < blob_len)
    num_chunks += 1;

  // Break blob into chunks and send them individually.  We use FIFO
  // mode to ensure they arrive in order.
  chunk_offset = 0;  
  for (i = 0; i < num_chunks; i++)
  {
    if (chunk_offset + chunk_len > blob_len)
      chunk_len = blob_len - chunk_offset;

    packet.chunk_offset = chunk_offset;
    packet.chunk_len = chunk_len;
    memcpy(packet.chunk_data, (char*) blob_data + chunk_offset, chunk_len);

    chunk_offset += chunk_len;
    
    status = SP_multicast(self->mbox, FIFO_MESS | SELF_DISCARD,
                          group, 0x7000, sizeof(packet), (char*) &packet);
    if (status < 0)
    {
      pthread_mutex_unlock(&self->mutex);
      return ERROR("spread send failed: %d", status);
    }
  }

  pthread_mutex_unlock(&self->mutex);
    
  return 0;
}


// Wait for new sensor data (blocking).
int dgc_sensnet_wait(dgc_sensnet_t *self)
{
  // Wait for the condition to be signaled
  pthread_mutex_lock(&self->cond_mutex);
  pthread_cond_wait(&self->cond, &self->cond_mutex);  
  pthread_mutex_unlock(&self->cond_mutex);

  return 0;
}


// Check for the latest sensor data (non-blocking).
int dgc_sensnet_peek(dgc_sensnet_t *self, int sensor_id, int blob_type,
                     int *blob_id, int *blob_len)
{
  dgc_sensnet_fifo_t *fifo;

  pthread_mutex_lock(&self->mutex);
    
  // Get the fifo for this packet
  fifo = dgc_sensnet_find_fifo(self, sensor_id, blob_type);
  if (!fifo)
  {
    pthread_mutex_unlock(&self->mutex);
    return ERROR("unknown sensor id or blob type");
  }

  // Get the current value
  *blob_id = fifo->current_blob_id;
  *blob_len = fifo->current_blob_len;

  pthread_mutex_unlock(&self->mutex);
    
  return 0;
}


// Read data from the cache.
int dgc_sensnet_read(dgc_sensnet_t *self, int sensor_id, int blob_type,
                    int blob_id, int blob_len, void *blob_data)
{
  int i;
  dgc_sensnet_fifo_t *fifo;
  dgc_sensnet_slot_t *slot;

  pthread_mutex_lock(&self->mutex);
    
  // Get the fifo for this message type
  fifo = dgc_sensnet_find_fifo(self, sensor_id, blob_type);
  if (!fifo)
  {
    pthread_mutex_unlock(&self->mutex);
    return ERROR("unknown sensor id or blob type");
  }

  // Find the matching blob id in the fifo
  for (i = 0; i < fifo->num_slots && i < fifo->max_slots; i++)
  {
    slot = fifo->slots + i;
    if (slot->blob_id == blob_id)
    {
      if (blob_len < slot->blob_len)
      {
        pthread_mutex_unlock(&self->mutex);        
        return ERROR("blob length mismatch: slot %d arg %d", slot->blob_len, blob_len);
      }
      memcpy(blob_data, slot->blob_data, blob_len);
      pthread_mutex_unlock(&self->mutex);      
      return 0;
    }
  }
  
  pthread_mutex_unlock(&self->mutex);  
  return ERROR("blob id %d is not in cache", blob_id);
}


// Library thread: read message from Spread and populate the fifos
int dgc_sensnet_main(dgc_sensnet_t *self)
{
  int i;
  int num_groups, mess_count, mess_len, endian;
  int16_t msg_type;
  service service_type;
  char sender[MAX_GROUP_NAME];
  char groups[1][MAX_GROUP_NAME];
  int16_t dummy;
  dgc_sensnet_packet_t packet;
  dgc_sensnet_fifo_t *fifo;
  dgc_sensnet_slot_t *slot;

  MSG("starting SensNet thread");
  
  while (!self->quit)
  {
    // See if there are any messages
    mess_count = SP_poll(self->mbox);
    if (mess_count < 0)
    {
      ERROR("spread poll failed: %d", mess_count);
      break;
    }

    // No messages, so loop back
    if (mess_count == 0)
    {
      usleep(0);
      continue;
    }
    
    // Wait for a chunk
    service_type = dummy = endian = 0;  
    mess_len = SP_receive(self->mbox, &service_type, sender,
                          sizeof(groups)/sizeof(groups[0]), &num_groups, groups,
                          &msg_type, &endian, sizeof(packet), (char*) &packet);
    if (mess_len < 0)
    {
      ERROR("spread recv failed: %d", mess_len);
      break;
    }

    pthread_mutex_lock(&self->mutex);

    if (msg_type < 0x7000)
    {
      // REMOVE
      //MSG("got skynet message type %d %d", msg_type, mess_len);

      // Get the fifo for this message type
      fifo = dgc_sensnet_find_fifo(self, DGC_SKYNET_SENSOR, msg_type);
      if (fifo)
      {
        // Create a new slot
        slot = fifo->slots + (fifo->num_slots++ % fifo->max_slots);
        slot->blob_id = fifo->num_slots;

        // Copy the data
        slot->blob_len = mess_len;
        assert(mess_len <= slot->max_len);
        memcpy(slot->blob_data, &packet, mess_len);

        // Update the fifo current values
        fifo->current_blob_id = slot->blob_id;
        fifo->current_blob_len = slot->blob_len;

        // Signal the waiting thread
        pthread_mutex_lock(&self->cond_mutex);
        pthread_cond_signal(&self->cond);  
        pthread_mutex_unlock(&self->cond_mutex);
      }      
    }
    else
    {    
      // Get the fifo for this message type
      fifo = dgc_sensnet_find_fifo(self, packet.sensor_id, packet.blob_type);
      if (fifo)
      {
        slot = NULL;
    
        // If this is the first chunk...
        if (packet.chunk_offset == 0)
        {
          // Create a new slot
          slot = fifo->slots + (fifo->num_slots++ % fifo->max_slots);
          slot->blob_id = packet.blob_id;
        }
        else
        {
          // Find a matching slot
          for (i = 0; i < fifo->max_slots; i++)
          {
            if (fifo->slots[i].blob_id == packet.blob_id)
            {
              slot = fifo->slots + i;
              break;
            }
          }
        }

        // If we found a slot...
        if (slot)
        {
          // Copy the data
          slot->blob_len = packet.blob_len;
          assert(slot->max_len >= packet.blob_len);
          assert(slot->blob_len >= packet.chunk_offset + packet.chunk_len);
          memcpy(slot->blob_data + packet.chunk_offset, packet.chunk_data, packet.chunk_len);
  
          // If this is the last chunk...
          if (packet.chunk_offset + packet.chunk_len >= packet.blob_len)
          {
            // Update the fifo current values
            fifo->current_blob_id = packet.blob_id;
            fifo->current_blob_len = packet.blob_len;

            // Signal the waiting thread
            pthread_mutex_lock(&self->cond_mutex);
            pthread_cond_signal(&self->cond);  
            pthread_mutex_unlock(&self->cond_mutex);
          }
        }
      }
    }
    
    pthread_mutex_unlock(&self->mutex);
  }

  MSG("exiting SensNet thread");
  
  return 0;
}




// Test data
#define DUMMY_SENSOR_ID 1
#define DUMMY_BLOB_TYPE 1


#if DGC_SENSNET_TEST_WRITE

// Test writing
int main(int argc, const char *argv[])
{
  dgc_sensnet_t *sensnet;
  int i;
  int blob_size, *blob;
  
  blob_size = 500000;
  blob = malloc(blob_size);

  sensnet = dgc_sensnet_alloc();
  if (dgc_sensnet_connect(sensnet, "4803", 1234, 0) < 0)
    return -1;
  
  for (i = 0; i < 1000; i++)
  {
    blob[0] = i * 10;
    MSG("sending %d %d", i, blob[0]);
    if (dgc_sensnet_write(sensnet, DUMMY_SENSOR_ID,
                          DUMMY_BLOB_TYPE, i, blob_size, blob) != 0)
      break;
    usleep(100000);
  }
  
  dgc_sensnet_disconnect(sensnet);
  dgc_sensnet_free(sensnet);
  
  free(blob);

  MSG("exiting cleanly");
  
  return 0;
}

#endif


#if DGC_SENSNET_TEST_READ

// Test wait and cache read
int main(int argc, const char *argv[])
{
  dgc_sensnet_t *sensnet;
  int i, id, len;
  int sensor_id;
  int blob_type;
  int blob_size, *blob;
  
  blob_size = 700000;
  blob = malloc(blob_size);

  sensnet = dgc_sensnet_alloc();
  if (dgc_sensnet_connect(sensnet, "4803", 1234, 1) < 0)
    return -1;

  sensor_id = DUMMY_SENSOR_ID;
  blob_type = DUMMY_BLOB_TYPE;
  
  if (dgc_sensnet_join(sensnet, sensor_id, blob_type, blob_size, 5) < 0)
    return -1;
    
  MSG("waiting");
      
  for (i = 0; i < 2000; i++)
  {
    if (dgc_sensnet_wait(sensnet) != 0)
      break;
    if (dgc_sensnet_peek(sensnet, sensor_id, blob_type, &id, &len) != 0)
      break;
    assert(len <= blob_size);
    if (dgc_sensnet_read(sensnet, sensor_id, blob_type, id, len, blob) != 0)
      break;
    MSG("recieved %d %d", id, blob[0]);
  }

  if (dgc_sensnet_leave(sensnet, sensor_id, blob_type) < 0)
    return -1;

  dgc_sensnet_disconnect(sensnet);
  dgc_sensnet_free(sensnet);
  
  free(blob);

  MSG("exiting cleanly");
    
  return 0;
}

#endif
