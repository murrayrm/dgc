
/* 
 * Desc: DGC stereo blob helper functions
 * Date: 08 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef DGC_STEREO_BLOB_H
#define DGC_STEREO_BLOB_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "dgc_sensnet_types.h"


/// Fast conversion from disparity to range.
static  __inline__ \
void dgc_stereo_blob_crd_to_xyz(dgc_stereo_blob_t *self,
                                int c, int r, uint16_t d, float *x, float *y, float *z)
{
  *z = self->sx / ((float) (int) d) * self->disp_scale * self->baseline;
  *x = (c - self->cx) / self->sx * *z;
  *y = (r - self->cy) / self->sy * *z;
  return;
}


#ifdef __cplusplus
}
#endif

#endif
