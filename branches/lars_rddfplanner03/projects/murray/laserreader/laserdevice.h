/*********************************************************************************/
/* SICK LASER DRIVER V0.9                                                        */
/* Copyright (C) 2000 - Kasper Stoy - USC Robotics Labs (See README for details) */
/*********************************************************************************/
#ifndef LASERDEVICE
#define LASERDEVICE
#include <pthread.h>

#include "device.h"

class CLaserDevice:public CDevice {
 private:
  pthread_t thread;           // the thread that continuously reads from the laser 
  unsigned char data[1024];   // array holding the most recent laser scan
  int debuglevel;             // debuglevel 0=none, 1=basic, 2=everything


  ssize_t WriteToLaser( unsigned char *data, ssize_t len ); 

  /* methods used to decode the response from the laser */
  ssize_t RecieveAck();
  void DecodeStatusByte( unsigned char byte );
  bool CheckDatagram( unsigned char *datagram );

  /* methods used to configure the laser */
  int Request38k();
  int RequestData();
  int ChangeMode( );

 public:

  CLaserDevice(char *port);
  int Setup();
  void Run();
  int Shutdown();
  void GetData();
  void GetData( unsigned char * );
  void PutData( unsigned char * );
  int GetPoint();
  void UpdateScan() {LockNGetData();}

  /* these should really be private but the run thread needs access to them */
  int laser_fd;               // laser device file descriptor
  char LASER_SERIAL_PORT[11]; // device used to communicate with the laser

  /* Calculates CRC for a telegram */
  unsigned short CreateCRC( unsigned char* commData, unsigned int uLen );
  int Index;
  int Scan[361]; // accessable array  
};

#endif
