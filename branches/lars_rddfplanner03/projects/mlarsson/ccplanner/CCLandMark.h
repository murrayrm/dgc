#ifndef _CCLANDMARK_H_
#define _CCLANDMARK_H_
#include "frames/coords.hh"
#include <fstream>
#include <iostream>
#include "CCElementaryPath.h"
#include "CVectorMap.h"
#include "traj.h"
#include <vector>

#define 	CSPATIALPLANNER_SIGMAS									11
#define 	CSPATIALPLANNER_LENGTHS									4
#define 	CSPATIALPLANNER_PREFERRED_DIST					5
#define 	CSPATIALPLANNER_MAX_DIST_ERROR					0.5
#define		CSPATIALPLANNER_EPS_IS_PARALLEL  				0.01
#define		CSPATIALPLANNER_EPS_ARE_SYMMETRIC  			0.02
#define		CSPATIALPLANNER_INT_INTERVAL  					5 
#define		CCSPATIALPLANNER_LANE_ORIENTATION_COST  1.0
#define		CCSPATIALPLANNER_LANE_DIST_COST  				4.0
#define		CCLANDMARK_MIN_DISTSQD_FOR_COST					1.0
#define		CSPATIALPLANNER_MAX_SEARCH_DIST_SQUARED  20.0*20.0

/** Designed for future communication with supercon/trafficplanner/routeplanner. */
struct CWayPoint 
{
	NEcoord m_pos;
	double m_yaw;
};

/** The basic node of my tree. Stores a vector of pointers to child nodes, position and orientation, 
and the segment of from the parent node to this node, stored as a CCElementaryPath.

TODO: Various implementations of a CTree class exist on the internet. Their stability may be better than
my structure because they separate the nodes forming the tree structure from the data they reference. 
If memory leaks are a problem this design might be better, either using one of these existing classes
or yourself (whoever YOU are!).  */
class CCLandMark 
{
public:
	/*-------------------- Methods -------------------------------*/
	
	/** Calculate the cost associated with this nodes alignment and proximity to lane boundaries. */
	double calculateLaneCost(CVectorMap *map);

	/** function to log landmark position/orientation to a file */
	void log(ostream *f);
	
	/** Function to recursively save the entire tree to a file. */
	void printAll(ostream *f); 

	/** recursive function to find the lowest f value in the tree */
	CCLandMark* findLowestF(double minDist);

	/** returns whether there is a node within minSqdDist of c */
	bool nodeWithinTolerance(NEcoord c, double minSqdDist); 
	
	/** recursive function to save the entire trajectory */
	int getTraj(CTraj *t);

	/** The finish node has moved: make the necessary changes to the graph. */
	void finishHasChanged(CCLandMark *finish, bool reverse, CVectorMap *map);
	
	/** Calculates a path from this landmark to finish and checks whether it is an acceptable route */
	bool search(CCLandMark *finish, bool reverse, CVectorMap *map) ;
	
	/** Generates a bielementary path between p1 and p2, in the direction specified by reverse */ 
	bool generateBielementaryPath(CCLandMark *p1, CCLandMark *p2, bool reverse, CVectorMap *map);
	
	/** Recursive function to return the endpoint with the lowest G value. More maintainable than list of pointers. */
	CCLandMark* findBestEndpoint();
	
	/** Recursive function to "take a cutting" */
	CCLandMark* choose(CCLandMark *calledBy,NEcoord *pos);
	
	/** Recursive function to delete all the descendents of this node. */
	void deleteDescendants();
	
	/** Recursively prune the tree of all branches that go outside the updated legal region. */ 
	void pruneTree(CVectorMap *map);
	
	/** recursive function to find the number of points in a trajectory */
	int getNumPoints();
	
	/** recursive function to find the number of points in a trajectory */
	void updateLaneCost(CVectorMap *map);
	
	/** Calculate the heuristic - an estimate of the cost from this node to the finish. */
	void calculateF(CCLandMark *finish);
	
	/** Calculate the cost associated with this node. */
	void cost(CVectorMap *map);
	
	/** Set the path data and parent pointer of this node. Use this to calculate the cost function g. */
	void setPathParentCost(CCElementaryPath *path, CCLandMark *parent, CVectorMap *map);
	
	/*-------------------------- Variables -------------------------------*/
	
	/** position of vehicle */
	NEcoord m_pos;

	/** orientation of the vehicle at this node */
	double m_theta;

	/** curvature of path at this point: relates to steering angle */
	double m_k;

	/** whether explore has been run on this node already */
	double m_exploredDistSqd;

	/** whether this node is an endpoint */
	bool m_atFinish;

	/** vector of child nodes */
	vector<CCLandMark*> m_children;

	/** pointer to parent node */
	CCLandMark* m_pParent;

	/** path from parent landmark to this one */
	CCElementaryPath m_path;
	
	/** a* style variables */
	double m_f, m_g, m_h;
	
	/** constructor  */
	CCLandMark();

	/** destructor */
	~CCLandMark();

};

#endif
