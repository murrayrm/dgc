
v0 = 5.404300836
v1 = 5.414879299
Sf = 29.80364957
N = 31;
% dynRefinementConstraintFuncs.cc:970: deltaT (1) = 4.509126538
% dynRefinementConstraintFuncs.cc:987: m_collSF   = 29.80291084
% dynRefinementConstraintFuncs.cc:988: deltaT (2) = 4.479503206
% dynRefinementConstraintFuncs.cc:993: m_pColl[1] = 4.479503206
% dynRefinementConstraintFuncs.cc:954: v0 = 5.404300836
% dynRefinementConstraintFuncs.cc:955: v1 = 5.414879299
% dynRefinementConstraintFuncs.cc:970: deltaT (1) = 0.1848569496
% dynRefinementConstraintFuncs.cc:987: m_collSF   = 29.80364957
% dynRefinementConstraintFuncs.cc:988: deltaT (2) = 0.1836470582
% dynRefinementConstraintFuncs.cc:993: m_pCollT[30] = 13.25180146

a = v1-v0;
b = v1+v0;
a_2 = a*a;
b_2 = b*b;
b_4 = b_2*b_2;
b_5 = b_4*b;

deltaT = 2 * ( a_2 .* (a_2/5 + b_2/3) + b_4) ./ b_5
deltaT = deltaT * Sf / (N-1)

M=5000;
s = linspace(0,1,M);
deltaT_true = sum(1./(v0+(v1-v0)*s)) / (M-1)


