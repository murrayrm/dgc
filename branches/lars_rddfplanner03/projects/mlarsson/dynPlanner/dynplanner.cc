#include "dynplanner.h"
#include "DGCutils"
#include "MapAccess.h"
#include <iomanip>

#define USE_SPATIAL_PLANNER
#define USE_VELOCITY_PLANNER
#define USE_PREVIOUS_TRAJ     1

// New constructor
CDynPlanner::CDynPlanner(CMap *pMap, int mapLayerID, CDynMap *pDynMap, RDDF* pRDDF, bool USE_MAXSPEED, bool bGetVProfile)
	: m_refinementStage(pMap, mapLayerID, pDynMap, pRDDF, USE_MAXSPEED, bGetVProfile),
	  m_reactiveStage(pMap, mapLayerID, pRDDF, USE_MAXSPEED),
	  m_spatialPlanner(pRDDF),
	  m_velocityPlanner(pDynMap),
	  m_interm("interm"),
	  m_after("after"),
	  m_pMap(pMap),
	  m_pDynMap(pDynMap),
	  m_layer(mapLayerID)
{
	m_interm << setprecision(20);
	m_after  << setprecision(20);
}




// Old constructor
CDynPlanner::CDynPlanner(CMap *pMap,  int mapLayerID, RDDF* pRDDF, bool USE_MAXSPEED, bool bGetVProfile)
	: m_refinementStage(pMap, mapLayerID, pRDDF, USE_MAXSPEED, bGetVProfile),
	  m_reactiveStage  (pMap, mapLayerID, pRDDF, USE_MAXSPEED),
	  m_interm("interm"),
	  m_after("after"),
	  m_pMap(pMap),
	  m_pDynMap(NULL),
	  m_layer(mapLayerID)
{
	m_interm << setprecision(20);
	m_after  << setprecision(20);
}

CDynPlanner::~CDynPlanner()
{
}




// New plan() method.
int CDynPlanner::plan(VehicleState *pState, double Time, bool bIsStateFromAstate, CTraj* pPrevTraj)
{
	// copy the state so we can modify it if we need
	VehicleState stateCopy = *pState;

	cout << endl << "CDynPlanner::plan(VehicleState *pState)" << endl;


#ifdef USE_SPATIAL_PLANNER

	m_spatialPlanner.run(&stateCopy);
	CTraj *seedtraj = m_spatialPlanner.getTraj(); 

#elif USE_PREVIOUS_TRAJ==1

	// Find a seed for the refinement stage planner
	CPath seedpath(RDDF_FILE);
	CTraj *seedtraj = new CTraj;

	// If pPrevTraj == NULL we don't have any previous, reliable traj, so we just get a seed entirely from CPath.
	// Otherwise, we want to use the previous plan as a seed, prolonged so that it can hold the new plan.
	if (pPrevTraj == NULL) {
	  cout << "Seed from CPath" << endl;
	  seedpath.SeedFromLocation(pState->Northing, pState->Easting, pState->Yaw, seedtraj, PATHGENSEED_MERGEDIST);
	} else {
	  cout << "Seed from previous traj" << endl;
	  // find indices of the start and end points on the previously computed trajectory
	  int start = pPrevTraj->getClosestPoint(pState->Northing, pState->Easting);
	  int end   = pPrevTraj->getNumPoints() - 1;
	  // find a CPath seed starting at the end point on the previous trajectory
	  double vel_N = pPrevTraj->getNorthingDiff(end, 1);
	  double vel_E = pPrevTraj->getEastingDiff(end, 1);
	  double yaw = atan2(vel_E, vel_N);
	  seedpath.SeedFromLocation(pPrevTraj->getNorthing(end), pPrevTraj->getEasting(end),
				    yaw, seedtraj, PATHGENSEED_MERGEDIST);
	  seedtraj->prepend(pPrevTraj, start, end);
	}

#else
	
	// Find a seed for the refinement stage planner
	CPath seedpath(RDDF_FILE);
	CTraj *seedtraj = new CTraj;
	cout << "Seed only from CPath" << endl;
	cout << setprecision(10);
	cout << __FILE__ << ":" << __LINE__ << ": Current state: (N, E) = " << "(" << stateCopy.Northing << ", " << stateCopy.Easting << "), yaw = " << stateCopy.Yaw<< endl;
	seedpath.SeedFromLocation(stateCopy.Northing, stateCopy.Easting, stateCopy.Yaw, seedtraj, PATHGENSEED_MERGEDIST);
	/*
	unsigned long long t1, t2;
	DGCgettime(t1);
	int res_reactive = m_reactiveStage.run(&stateCopy, bIsStateFromAstate,
					       pPrevTraj);
	DGCgettime(t2);
	t2-=t1;
	cout << "reactiveStage: " << DGCtimetosec(t2) << " seconds" << endl;

	seedtraj = m_reactiveStage.getTraj();
	*/

#endif

	// Save the seed
	ofstream file("seedtraj");
	seedtraj->print(file);
	file.close();
	
	int res = 0;
	
#ifdef USE_VELOCITY_PLANNER	

	res = m_velocityPlanner.run(&stateCopy, Time, m_spatialPlanner.getTraj());
	
#else

	// Print some specs to the screen
	readspecs();
	cout << __FILE__ << ":" << __LINE__ << ":" << " LEAST_TARGET_DIST = " << LEAST_TARGET_DIST << endl;
	

	// Run the stage 2 planner
	res = m_refinementStage.run(&stateCopy, Time, seedtraj);

#endif

	/*
	// Save speed limits throughout seed trajectory
	file.open("../matlab/vlimitseed.m");
	cout << __FILE__ << ":" << __LINE__ << ": Speed limits (fine coll points)" << endl;
	file << "vlimit_coll_fine = [";
	for (int ii = 0; ii<seedtraj->getNumPoints(); ii++) {
	  NEcoord point(seedtraj->getNorthing(ii), seedtraj->getEasting(ii));
	  file << " " << m_pMap->getDataUTM<double>(m_layer,point.N, point.E);
	}
	file << "];" << endl;

	file << "s_coll_fine = linspace(0,1,length(vlimit_coll_fine));" << endl;
	file << "figure; plot(s_coll_fine,vlimit_coll_fine,'*');" << endl;
	file.close();
	*/

	return res;
}




// Old plan() method.
// pState is the state from astate
int CDynPlanner::plan(VehicleState *pState, bool bIsStateFromAstate, CTraj* pPrevTraj)
{
	// copy the state so we can modify it if we need
	VehicleState stateCopy = *pState;

	cout << endl << "CDynPlanner::plan(VehicleState *pState)" << endl;
// 	cout << '{';
//  	cout << stateCopy.Northing << ", " << stateCopy.Vel_N << ", " << stateCopy.Acc_N << "," << endl;
//  	cout << stateCopy.Easting  << ", " << stateCopy.Vel_E << ", " << stateCopy.Acc_E << "," << endl;
//  	cout << stateCopy.Yaw << ", " << stateCopy.YawRate << "}" << endl 
// 			 << stateCopy.Northing_rear() << ' ' <<  stateCopy.Easting_rear() << endl;



	unsigned long long t1, t2;
	DGCgettime(t1);
	int res = m_reactiveStage.run(&stateCopy, bIsStateFromAstate,
				      pPrevTraj);
	DGCgettime(t2);
	t2-=t1;
	cout << "reactiveStage: " << DGCtimetosec(t2) << " seconds" << endl;

	if(res == 0 || res == 4 || res == 9)
	{
		res = m_refinementStage.run(&stateCopy, m_reactiveStage.getTraj(), bIsStateFromAstate);
	}

	return res;
}




double CDynPlanner::getVProfile(CTraj* pTraj)
{
	m_refinementStage.m_bGetVProfile = true;

	*(m_refinementStage.getTraj()) = *pTraj;

	VehicleState vehstate(pTraj->getNorthingDiff(0, 0), pTraj->getNorthingDiff(0, 1), pTraj->getNorthingDiff(0, 2),
			      pTraj->getEastingDiff (0, 0), pTraj->getEastingDiff (0, 1), pTraj->getEastingDiff (0, 2));
	m_refinementStage.run(&vehstate, pTraj, false);

	m_refinementStage.m_bGetVProfile = false;

	return m_refinementStage.getObjective();
}




CTraj* CDynPlanner::getTraj(void)
{
#ifdef USE_VELOCITY_PLANNER
	return m_spatialPlanner.getTraj();
#else
	return m_refinementStage.getTraj();
#endif
}

CTraj* CDynPlanner::getSeedTraj(void)
{
	return m_reactiveStage.getTraj();
// 	return m_reactiveStage.getSeedTraj();
}




CTraj* CDynPlanner::getIntermTraj(void)
{
	return m_refinementStage.getSeedTraj();
}




double CDynPlanner::getLength(void)
{
	return m_refinementStage.getLength();
}




double CDynPlanner::getMinSpeedOn(CTraj& traj)
{
	const double step = 1.0;
	int index = 0;
	int newindex = 0;
	double minspeed = 1.0e6;

	do
	{
		index = newindex;

		double speed;
		double dummy;
		getContinuousMapValueDiffGrown(1, m_pMap, m_layer, false, 0.0,
					       traj.getNorthing(index),
					       traj.getEasting(index),
					       atan2(traj.getEastingDiff(index,1),
						     traj.getNorthingDiff(index,1)),
					       &speed, &dummy, &dummy, &dummy);

		minspeed = fmin(speed, minspeed);
		newindex = traj.getPointAhead( index, step );
	} while(newindex != index);

	return minspeed;
}
