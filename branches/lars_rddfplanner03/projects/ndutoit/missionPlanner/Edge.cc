#include "Edge.hh"
using namespace std;

Edge::Edge(Vertex* next)
{
  this->next = next;
  this->weight = 0;
}

Edge::~Edge()
{
}

double Edge::getWeight()
{
  return weight;
}

double Edge::getLength()
{
  return length;
}

Vertex* Edge::getNext()
{
  return next;
}

void Edge::setWeight(double weight)
{
  this->weight = weight;
}

void Edge::setLength(double length)
{
  this->length = length;
}

void Edge::setNext(Vertex* next)
{
  this->next = next;
}
