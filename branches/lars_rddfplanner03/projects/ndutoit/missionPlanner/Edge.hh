#ifndef EDGE_HH_
#define EDGE_HH_
#define COST_CHANGE_LANE 100.0f
#define COST_ZONE 200.0f
#define COST_STOP_SIGN 300.0f
#define COST_INTERSECTION 500.0f
#define COST_UTURN 2000.0f
#define COST_KTURN 3000.0f
#define INTERSECTION_SPEED 20.0f
using namespace std;

class Vertex;

/*! Edge class. Represents an edge of a graph. An edge contains an edge weight
 *  and a vertex pointer to the vertex it is connected to.
 * \brief The Edge class used in the Graph class.
 */
class Edge
{
public:
/*! All edges have a vertex pointer to the vertex it is connected to. */
	Edge(Vertex* next);
	virtual ~Edge();
  
/*! Returns the edge weight of THIS. */
  double getWeight();

/*! Returns the length of the segment corresponding to THIS. */
  double getLength();
  
/*! Returns a vertex pointer to the vertex THIS is connected to. */
  Vertex* getNext();
  
/*! Sets the edge weight of THIS. */
  void setWeight(double weight);

/*! Sets the length of the segment corresponding to THIS. */
  void setLength(double length);
  
/*! Sets the vertex pointer to the vertex THIS is connected to. */
  void setNext(Vertex* next);
  
private:
  double weight, length;
  Vertex* next;
};

#endif /*EDGE_HH_*/
