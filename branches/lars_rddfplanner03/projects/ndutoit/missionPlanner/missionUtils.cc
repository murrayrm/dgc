#include "missionUtils.hh"

//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Add the following vertices to rndfGraph
// * exit points
// * entry points
// * checkpoints
// * entry perimeter points
// * exit perimeter points
// * parking spot waypoints
//-------------------------------------------------------------------------------------------------------------------------------
void addAllVertices(RNDF* rndf, Graph* rndfGraph)
{
  int numOfSegments = rndf->getNumOfSegments();
  int numOfZones = rndf->getNumOfZones();
  
  // Adding all entry, exit, and checkpoint waypoints to the graph.

  for(int i = 1; i <= numOfSegments; i++)
  {    
    for(int j = 1; j <= rndf->getSegment(i)->getNumOfLanes(); j++)
    {
      for(int k = 1; k <= rndf->getLane(i, j)->getNumOfWaypoints(); k++)
      {
        Waypoint* waypoint = rndf->getWaypoint(i, j, k);
        
        if(waypoint->isEntry() || waypoint->isExit() || waypoint->isCheckpoint())
          rndfGraph->addVertex(i, j, k);
      }
    }    
  }
  
  // Adding all entry and exit perimeter points and parking spot waypoints.
  
  for(int i = numOfSegments + 1; i <= numOfSegments + numOfZones; i++)
  {
    
    // Adding all entry and exit perimeter points.
    
    for(int j = 1; j <= rndf->getZone(i)->getNumOfPerimeterPoints(); j++)
    {
      PerimeterPoint* perimeterPoint = rndf->getPerimeterPoint(i, j);
      
      if(perimeterPoint->isEntry() || perimeterPoint->isExit())
        rndfGraph->addVertex(i, 0, j);
    }
    
    // Adding all parking spot checkpoints.
    
    for(int j = 1; j <= rndf->getZone(i)->getNumOfSpots(); j++)
    {
        Waypoint* waypoint = rndf->getWaypoint(i, j, 1);
          
        if(waypoint->isCheckpoint())
          rndfGraph->addVertex(i, j, 1);
          
        waypoint = rndf->getWaypoint(i, j, 2);
          
        if(waypoint->isCheckpoint())
          rndfGraph->addVertex(i, j, 2);
    }
  }
  
  //cout << "Vertices added to graph: " << endl;
  //rndfGraph->print();
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Add the following edges to rndfGraph
// * exit point -> entry points
// * entry point -> closest exit points
// * closest entry points -> checkpoint
// * checkpoint -> closest exit points
// * parking spot -> exit points
// * entry perimeter point -> exit perimeter points
// * entry perimenter point -> parking spots
//-------------------------------------------------------------------------------------------------------------------------------
void addAllEdges(RNDF* rndf, Graph* rndfGraph)
{
  vector<Vertex*> vertices = rndfGraph->getVertices();
  
  /* Adding all edges from exit waypoints. */  
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    GPSPoint* exit;
    vector<GPSPoint*> entries;
    
    int exitSegmentID = vertex->getSegmentID();
    int exitLaneID = vertex->getLaneID();
    int exitWaypointID = vertex->getWaypointID();
    
    if(exitLaneID == 0)
      exit = rndf->getPerimeterPoint(exitSegmentID, exitWaypointID);
    else
      exit = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID);
        
    if(!exit->isExit())
      continue;
    else
      entries = exit->getEntryPoints();

    // Adding all edges from exit waypoints to their entry waypoints.
    for(unsigned j = 0; j < entries.size(); j++)
    {
      GPSPoint* entry = entries[j];
      double length = computeDistanceLL(entry->getLatitude(), entry->getLongitude(), exit->getLatitude(), exit->getLongitude());
      double weight = length/INTERSECTION_SPEED;
      if (exitLaneID != 0)
      {
	weight += COST_INTERSECTION;
	if (rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID)->isStopSign())
	  weight += COST_STOP_SIGN;
      }
      rndfGraph->addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
			 entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID(), length, weight);
    }
            
    // Adding edges from all parking spot checkpoints to their exit waypoints.
    if(exitLaneID == 0)
    {
      for(int j = 1; j <= rndf->getZone(exitSegmentID)->getNumOfSpots(); j++)
      {
	Waypoint* spot = rndf->getWaypoint(exitSegmentID, j, 2);
	if(spot->isCheckpoint())
	{
	  double length = computeDistanceLL(exit->getLatitude(), exit->getLongitude(), spot->getLatitude(), spot->getLongitude());
	  double weight = length/(rndf->getZone(exitSegmentID)->getMaxSpeed()) + COST_ZONE;
	  rndfGraph->addEdge(exitSegmentID, j, 2, 
			     exitSegmentID, exitLaneID, exitWaypointID, length, weight);
	}
      }
    }

    // Adding all edges from exit waypoints to their next waypoint on the same lane.
    else if(exitWaypointID < rndf->getLane(exitSegmentID, exitLaneID)->getNumOfWaypoints())
    {
      GPSPoint* entry = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID+1);
      double length = computeDistanceLL(entry->getLatitude(), entry->getLongitude(), exit->getLatitude(), exit->getLongitude());
      double weight = length/(rndf->getSegment(exitSegmentID)->getMaxSpeed()) + COST_INTERSECTION;
      if (rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID)->isStopSign())
	weight += COST_STOP_SIGN;
      rndfGraph->addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
			 entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID(), length, weight);
    }
  }
  
  // Adding all edges from entry waypoints.
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    GPSPoint* entry;
    
    int entrySegmentID = vertex->getSegmentID();
    int entryLaneID = vertex->getLaneID();
    int entryWaypointID = vertex->getWaypointID();
    
    // Adding all edges from entry perimeter points of zones.

    if(entryLaneID == 0)
    {
      entry = rndf->getPerimeterPoint(entrySegmentID, entryWaypointID);
      
      if(!entry->isEntry())
        continue;
        
      addEdgesFromEntryPerimeter(rndf, rndfGraph, entry);
    }
    else
    {
      entry = rndf->getWaypoint(entrySegmentID, entryLaneID, entryWaypointID);
            
      if(!entry->isEntry())
        continue;

      addEdgesFromEntry(rndf, rndfGraph, entry);
    }
  }


  // Adding edges to/from all checkpoints.
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    Waypoint* checkpoint;
    
    int checkpointSegmentID = vertex->getSegmentID();
    int checkpointLaneID = vertex->getLaneID();
    int checkpointWaypointID = vertex->getWaypointID();
        
    if(checkpointLaneID == 0 || checkpointSegmentID > rndf->getNumOfSegments())
      continue;
    else
      checkpoint = rndf->getWaypoint(checkpointSegmentID, checkpointLaneID, checkpointWaypointID);

    if(!checkpoint->isCheckpoint())
      continue;
    
    // Adding edges to all checkpoints from their closest entry waypoints.

    Waypoint* previousWaypoint = rndf->getWaypoint(checkpointSegmentID, checkpointLaneID, checkpointWaypointID);
    bool entryFound = false;
    int j = checkpointWaypointID - 1;
    double length = 0;
    while(!entryFound && j >= 1)
    {
      Waypoint* entry = rndf->getWaypoint(checkpointSegmentID, checkpointLaneID, j);
      length += computeDistanceLL(entry->getLatitude(), entry->getLongitude(),
				previousWaypoint->getLatitude(), previousWaypoint->getLongitude());
      if(entry->isEntry())
      {
	double weight = length;
	rndfGraph->addEdge(checkpointSegmentID, checkpointLaneID, j,
			   checkpointSegmentID, checkpointLaneID, checkpointWaypointID, length, weight);
	entryFound = true;
      }
      j--;
      previousWaypoint = entry;
    }
   
    // Adding edges from all checkpoints to their closest exit waypoints.
    addEdgesFromEntry(rndf, rndfGraph, checkpoint);
  }
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Add edges from the specified entry point to its closest exit points (both on the same lane and adjacent lanes).
//-------------------------------------------------------------------------------------------------------------------------------
void addEdgesFromEntry(RNDF* rndf, Graph* rndfGraph, GPSPoint* entry)
{      
  int entrySegmentID = entry->getSegmentID();
  int entryLaneID = entry->getLaneID();
  int entryWaypointID = entry->getWaypointID();

  // Adding edges from this entry waypoint to its next exit waypoints in the same lane.
  bool exitFound = false;
  int j = entryWaypointID + 1;
  GPSPoint* previousWaypoint = entry;
  double length = 0;
  while(!exitFound && j <= rndf->getLane(entrySegmentID, entryLaneID)->getNumOfWaypoints())
  {
    Waypoint* exit = rndf->getWaypoint(entrySegmentID, entryLaneID, j);
    length += computeDistanceLL(previousWaypoint->getLatitude(), previousWaypoint->getLongitude(),
				exit->getLatitude(), exit->getLongitude());
    if(exit->isExit())
    {
      double weight = length;
      rndfGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
			 entrySegmentID, entryLaneID, j, length, weight);
      exitFound = true;
    }
    j++;
    previousWaypoint = exit;
  }

/*
  // Adding edges from this entry waypoint to exit waypoints in adjacent, same direction lanes.
  vector<int> adjacentLanes = getAdjacentLanes(rndf->getLane(entrySegmentID, entryLaneID), rndf);
  for(unsigned j = 0; j < adjacentLanes.size(); j++)
  {
    Lane* adjacentLane = rndf->getLane(entrySegmentID, adjacentLanes[j]);
    int numOfWaypoints = adjacentLane->getNumOfWaypoints();
        
    for(int k = 2; k <= numOfWaypoints; k++)
    {
      if(adjacentLane->getWaypoint(k)->isExit())
                       rndfGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
		       entrySegmentID, adjacentLane->getLaneID(), k);
    }   
  }
*/
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Add edges from the specified entry perimeter point to all its exit perimeter points and to all the checkpoints in the zone.
//-------------------------------------------------------------------------------------------------------------------------------
void addEdgesFromEntryPerimeter(RNDF* rndf, Graph* rndfGraph, GPSPoint* entry)
{
  int entrySegmentID = entry->getSegmentID();
  int entryLaneID = entry->getLaneID();
  int entryWaypointID = entry->getWaypointID();

  // Adding edges from this entry perimeter points to all exit perimeter points.

  // The commented line below is what Marlan wrote. I'm not sure why j should start from entryWaypointID+1.
  // for(int j = entryWaypointID + 1; j <= rndf->getZone(entrySegmentID)->getNumOfPerimeterPoints(); j++)
  for(int j = 1; j <= rndf->getZone(entrySegmentID)->getNumOfPerimeterPoints(); j++)
  {
    GPSPoint* exit = rndf->getPerimeterPoint(entrySegmentID, j);
    if(exit->isExit())
    {
      double length = computeDistanceLL(entry->getLatitude(), entry->getLongitude(), exit->getLatitude(), exit->getLongitude());
      double weight = length/(rndf->getZone(entrySegmentID)->getMaxSpeed()) + COST_ZONE;
      rndfGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
			 entrySegmentID, 0, j, length, weight);
    }
  }
            
  // Adding edges from this entry perimeter point to all parking spot checkpoints.
  
  for(int j = 1; j <= rndf->getZone(entrySegmentID)->getNumOfSpots(); j++)
  {
    Waypoint* spot = rndf->getWaypoint(entrySegmentID, j, 2);
    if(spot->isCheckpoint())
    {
      double length = computeDistanceLL(entry->getLatitude(), entry->getLongitude(), spot->getLatitude(), spot->getLongitude());
      double weight = length + COST_ZONE;
      rndfGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
			 entrySegmentID, j, 2, length, weight);
    }
  }
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Compute the optimal route from vertex1 to vertex2 in rndfGraph using Dijkstra's algorithm
//-------------------------------------------------------------------------------------------------------------------------------
bool findRoute(Vertex* vertex1, Vertex* vertex2, RNDF* rndf, Graph* rndfGraph, vector<Vertex*> &route, double &cost)
{
  vector<Vertex*> unvisitedVertices = rndfGraph->getVertices();
  vector<Vertex*> visitedVertices;
  Vertex* cheapestVertex;
  vector<Edge*> edgesFromCheapestVertex;
  float minCostToCome;

  for(unsigned i = 0; i < unvisitedVertices.size(); i++)
    unvisitedVertices[i]->setCostToCome(INFINITE_COST_TO_COME);

  vertex1->setCostToCome(0);

  bool routeFound = false;

  while(!routeFound && unvisitedVertices.size() > 0)
  {
    // Find the vertex with minimum cost to come
    cheapestVertex = unvisitedVertices[0];
    minCostToCome = cheapestVertex->getCostToCome();
    for(unsigned i = 1; i < unvisitedVertices.size(); i++)
    {
      if (unvisitedVertices[i]->getCostToCome() < minCostToCome)
      {
	cheapestVertex = unvisitedVertices[i];
	minCostToCome = cheapestVertex->getCostToCome();
      }
    }

    // Add the vertex with minimum cost to come to visitedVertices.
    visitedVertices.push_back(cheapestVertex);
    removeVertex(unvisitedVertices, cheapestVertex);
    if (cheapestVertex == vertex2)
      routeFound = true;
    else
    {
      // Update cost to come
      edgesFromCheapestVertex = cheapestVertex->getEdges();
      for(unsigned i = 0; i < edgesFromCheapestVertex.size(); i++)
      {
	float currentCost = edgesFromCheapestVertex[i]->getNext()->getCostToCome();
	float newCost = cheapestVertex->getCostToCome() + edgesFromCheapestVertex[i]->getWeight();
	if (currentCost > newCost)
	{
	  edgesFromCheapestVertex[i]->getNext()->setCostToCome(newCost);
	  edgesFromCheapestVertex[i]->getNext()->setPreviousVertex(cheapestVertex);
	}
      }
    }
  }

  cost = vertex2->getCostToCome();

  if(vertex2->getCostToCome() == INFINITE_COST_TO_COME)
    routeFound = false;
  else
  {
    Vertex* currentVertex = vertex2;
    vector<Vertex*> reverseRoute;
    bool vertex1Found = false;
    while (!vertex1Found)
    {
      int currentSegmentID = currentVertex->getSegmentID();
      int currentLaneID = currentVertex->getLaneID();
      int currentWaypointID = currentVertex->getWaypointID();
      GPSPoint* current;
      if(currentLaneID == 0)
	current = rndf->getPerimeterPoint(currentSegmentID, currentWaypointID);
      else
	current = rndf->getWaypoint(currentSegmentID, currentLaneID, currentWaypointID);
      if (currentVertex == vertex1 || currentVertex == vertex2 || current->isEntry() || current->isExit())
	reverseRoute.push_back(currentVertex);
      if (currentVertex == vertex1)
	vertex1Found = true;
      currentVertex = currentVertex->getPreviousVertex();
    }
    for(int i = reverseRoute.size()-1; i >= 0; i--)
      route.push_back(reverseRoute[i]);
  }

  return routeFound;
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Determine the segment-level goals from route.
//-------------------------------------------------------------------------------------------------------------------------------
vector<SegGoals> findSegGoals(vector<Vertex*> route, RNDF* rndf)
{
  return findSegGoals(route, rndf, 1);
}


vector<SegGoals> findSegGoals(vector<Vertex*> route, RNDF* rndf, int firstSegGoalsID)
{
  vector<SegGoals> segGoals;
  if (route.size() > 0)
  {
    for(unsigned i = 0; i < route.size()-1; i++)
    {
      int entrySegmentID = route[i]->getSegmentID();
      int entryLaneID = route[i]->getLaneID();
      int entryWaypointID = route[i]->getWaypointID();
      int exitSegmentID = route[i+1]->getSegmentID();
      int exitLaneID = route[i+1]->getLaneID();
      int exitWaypointID = route[i+1]->getWaypointID();
      GPSPoint* entryPoint;
      GPSPoint* exitPoint;
      SegGoals currentSegGoals;

      if(entryLaneID == 0)
	entryPoint = rndf->getPerimeterPoint(entrySegmentID, entryWaypointID);
      else
	entryPoint = rndf->getWaypoint(entrySegmentID, entryLaneID, entryWaypointID);
      if(exitLaneID == 0)
	exitPoint = rndf->getPerimeterPoint(exitSegmentID, exitWaypointID);
      else
	exitPoint = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID);
      
      currentSegGoals.goalID = firstSegGoalsID + i;
      currentSegGoals.entries.push_back(entryPoint);
      currentSegGoals.exits.push_back(exitPoint);
      currentSegGoals.segment_type = findType(route[i], route[i+1], rndf);
      currentSegGoals.action = "DRIVE";
      segGoals.push_back(currentSegGoals);
    }
  }
  return segGoals;
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Determine the type of a segment from vertex1 to vertex2. possible types are
// * ROAD_SEGMENT
// * PARKING_ZONE
// * INTERSECTION
// * PREZONE
//-------------------------------------------------------------------------------------------------------------------------------
string findType(Vertex* vertex1, Vertex* vertex2, RNDF* rndf)
{
    int vertex1SegmentID = vertex1->getSegmentID();
    int vertex1LaneID = vertex1->getLaneID();
    int vertex1WaypointID = vertex1->getWaypointID();
    int vertex2SegmentID = vertex2->getSegmentID();
    int vertex2LaneID = vertex2->getLaneID();
    int vertex2WaypointID = vertex2->getWaypointID();
    int numofSegments = rndf->getNumOfSegments();
    
    /* The case where vertex1 represents a perimeter point .
       The only possible types are
       * PARKING_ZONE (entry perimeter point -> parking spot checkpoint OR
                       entry perimeter point -> exit perimeter point)
       * ROAD_SEGMENT (exit perimeter point -> entry point) */
    if(vertex1LaneID == 0)
    {
      PerimeterPoint* segmentEntry = rndf->getPerimeterPoint(vertex1SegmentID, vertex1WaypointID);
      if (segmentEntry->isEntry())
      {
	if (vertex2LaneID == 0)
	{
	  PerimeterPoint* segmentExit = rndf->getPerimeterPoint(vertex2SegmentID, vertex2WaypointID);
	  if(segmentExit->isExit())
	    return "PARKING_ZONE";
	}
	else if (vertex2SegmentID > numofSegments)
	{
	  Waypoint* spot = rndf->getWaypoint(vertex2SegmentID,vertex2LaneID, vertex2WaypointID);
	  if (spot->isCheckpoint())
	    return "PARKING_ZONE";
	}
      }
      else if (segmentEntry->isExit())
      {
	if (vertex2SegmentID <= numofSegments)
	{
	  Waypoint* segmentExit = rndf->getWaypoint(vertex2SegmentID, vertex2LaneID, vertex2WaypointID);
	  if (segmentExit->isEntry())
	    return "ROAD_SEGMENT";
	}
      }
    }

    /* The case where vertex1 represents a parking spot
       The only posible type is PARKING_ZONE (parking spot checkpoint -> exit perimeter point) */
    else if(vertex1SegmentID > numofSegments)
    {
      Waypoint* spot = rndf->getWaypoint(vertex1SegmentID,vertex1LaneID, vertex1WaypointID);
      if (spot->isCheckpoint() && vertex2LaneID == 0)
      {
	PerimeterPoint* segmentExit = rndf->getPerimeterPoint(vertex2SegmentID, vertex2WaypointID);
	if(segmentExit->isExit())
	  return "PARKING_ZONE";
      }
    }

    /* The case where vertex1 represents an entry, exit, or checkpoint waypoint.
       The only possible types are
       * ROAD_SEGMENT (entry -> exit OR entry -> checkpoint OR checkpoint -> exit)
       * PREZONE (exit -> entry perimeter point)
       * INTERSECTION (exit -> entry)
       Note that the order of the if-else statement here is important because a checkpoint can also be an entry or exit point */
    else
    {
      Waypoint* segmentEntry = rndf->getWaypoint(vertex1SegmentID, vertex1LaneID, vertex1WaypointID);
      if (segmentEntry->isEntry())
      {
	if (vertex2SegmentID <= numofSegments)
	{
	  Waypoint* segmentExit = rndf->getWaypoint(vertex2SegmentID, vertex2LaneID, vertex2WaypointID);
	  if (segmentExit->isExit() || segmentExit->isCheckpoint())
	    return "ROAD_SEGMENT";
	}
      }
      else if (segmentEntry->isExit())
      {	
	if (vertex2LaneID == 0)
	{
	  PerimeterPoint* segmentExit = rndf->getPerimeterPoint(vertex2SegmentID, vertex2WaypointID);
	  if(segmentExit->isEntry())
	    return "PREZONE";
	}
	else if (vertex2SegmentID <= numofSegments)
	{
	  Waypoint* segmentExit = rndf->getWaypoint(vertex2SegmentID, vertex2LaneID, vertex2WaypointID);
	  if(segmentExit->isEntry())
	    return "INTERSECTION";
	}
      }
      else if (segmentEntry->isCheckpoint())
      {
	if (vertex2SegmentID <= numofSegments)
	{
	  Waypoint* segmentExit = rndf->getWaypoint(vertex2SegmentID, vertex2LaneID, vertex2WaypointID);
	  if (segmentExit->isExit())
	    return "ROAD_SEGMENT";
	}
      }
    }

  return "UNKNOWN";
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Find the closest waypoint or perimeter point on the specified segment and lane. If segmentID = 0, then find the closest 
// waypoint on any segment. If laneID = 0, then find the closest waypoint on the specified segment on any lane.
//-------------------------------------------------------------------------------------------------------------------------------
GPSPoint* findClosestGPSPoint(double northing, double easting, int segmentID, int laneID, double& distance, RNDF* rndf)
{
  int numOfSegments = rndf->getNumOfSegments();
  int numOfZones = rndf->getNumOfZones();
  double closestDistance;
  GPSPoint* closestGPSPoint;
  double GPSPointNorthing, GPSPointEasting;
  char UTMZone[4];
  int RefEllipsoid = 23;

  // The case where we're on a segment
  if (segmentID != 0 && laneID != 0 && segmentID <= numOfSegments && laneID <= rndf->getSegment(segmentID)->getNumOfLanes())
  {
    closestGPSPoint = rndf->getWaypoint(segmentID, laneID, 1);
    LLtoUTM(RefEllipsoid, closestGPSPoint->getLatitude(), closestGPSPoint->getLongitude(), GPSPointNorthing, GPSPointEasting, UTMZone);
    closestDistance = sqrt(square(northing - GPSPointNorthing) + square(easting - GPSPointEasting));

    for (int k = 1; k <= rndf->getLane(segmentID, laneID)->getNumOfWaypoints(); k++)
    {
      GPSPoint* waypoint = rndf->getWaypoint(segmentID, laneID, k);
      LLtoUTM(RefEllipsoid, waypoint->getLatitude(), waypoint->getLongitude(), GPSPointNorthing, GPSPointEasting, UTMZone);
      double newDistance = sqrt(square(northing - GPSPointNorthing) + square(easting - GPSPointEasting));
      if (newDistance < closestDistance)
      {
	closestGPSPoint = waypoint;
	closestDistance = newDistance;
      }    
    }
  }

  else if (segmentID != 0 && segmentID <= numOfSegments)
  {
    closestGPSPoint = findClosestGPSPoint(northing, easting, segmentID, 1, closestDistance, rndf);
    for (int j = 1; j <= rndf->getSegment(segmentID)->getNumOfLanes(); j++)
    {
      double newDistance;
      GPSPoint* newClosestGPSPoint = findClosestGPSPoint(northing, easting, segmentID, j, newDistance, rndf);
      if (newDistance < closestDistance)
      {
	closestGPSPoint = newClosestGPSPoint;
	closestDistance = newDistance;
      }    
    }
  }

  // The case where we're in a zone
  else if (segmentID != 0 && segmentID <= numOfSegments + numOfZones)
  {
    closestGPSPoint = rndf->getPerimeterPoint(segmentID, 1);
    LLtoUTM(RefEllipsoid, closestGPSPoint->getLatitude(), closestGPSPoint->getLongitude(), GPSPointNorthing, GPSPointEasting, UTMZone);
    closestDistance = sqrt(square(northing - GPSPointNorthing) + square(easting - GPSPointEasting));

    // Check all the perimeter points
    for(int j = 1; j <= rndf->getZone(segmentID)->getNumOfPerimeterPoints(); j++)
    {
      GPSPoint* perimeterPoint = rndf->getPerimeterPoint(segmentID, j);
      LLtoUTM(RefEllipsoid, perimeterPoint->getLatitude(), perimeterPoint->getLongitude(), 
	      GPSPointNorthing, GPSPointEasting, UTMZone);
      double newDistance = sqrt(square(northing - GPSPointNorthing) + square(easting - GPSPointEasting));
      if (newDistance < closestDistance)
      {
	closestGPSPoint = perimeterPoint;
	closestDistance = newDistance;
      }
    }
    
    // Check all the parking spot waypoints
    for(int j = 1; j <= rndf->getZone(segmentID)->getNumOfSpots(); j++)
    {
      for(int k = 1; k <= 2; k++)
      {
	GPSPoint* waypoint = rndf->getWaypoint(segmentID, j, k);
	LLtoUTM(RefEllipsoid, waypoint->getLatitude(), waypoint->getLongitude(), GPSPointNorthing, GPSPointEasting, UTMZone);
	double newDistance = sqrt(square(northing - GPSPointNorthing) + square(easting - GPSPointEasting));
	if (newDistance < closestDistance)
	{
	  closestGPSPoint = waypoint;
	  closestDistance = newDistance;
	}    
      }
    }
  }

  else
  {
    closestGPSPoint = findClosestGPSPoint(northing, easting, 1, 0, closestDistance, rndf);
    
    for (int i = 2; i <= numOfSegments + numOfZones; i++)
    {
      double newDistance;
      GPSPoint* newClosestGPSPoint = findClosestGPSPoint(northing, easting, i, 0, newDistance, rndf);
      if (newDistance < closestDistance)
      {
	closestGPSPoint = newClosestGPSPoint;
	closestDistance = newDistance;
      }    
    }
  }

  distance = closestDistance;
  return closestGPSPoint;
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Add a vertex corresponding to the specified GPSPoint (can be waypoint or perimeter point) to the graph with appropriate edges
//-------------------------------------------------------------------------------------------------------------------------------
bool addGPSPointToGraph (GPSPoint* point, Graph* rndfGraph, RNDF* rndf)
{
  int segmentID = point->getSegmentID();
  int laneID = point->getLaneID();
  int waypointID = point->getWaypointID();

  if (!rndfGraph->addVertex(segmentID, laneID, waypointID))
    return false;

  // In the case we're in a zone.
  if (segmentID > rndf->getNumOfSegments())
    addEdgesFromEntryPerimeter(rndf, rndfGraph, point);

  // In the case we're on a segment
  else
    addEdgesFromEntry(rndf, rndfGraph, point);

  return true;
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Find all the exit points that have entry as one of their associated entry points.
//-------------------------------------------------------------------------------------------------------------------------------
vector<Vertex*> findAllExits(Vertex* entryVertex, RNDF* rndf, Graph* rndfGraph)
{
  vector<Vertex*> vertices = rndfGraph->getVertices();
  vector<Vertex*> exitVertices;
  int entrySegmentID = entryVertex->getSegmentID();
  int entryLaneID = entryVertex->getLaneID();
  int entryWaypointID = entryVertex->getWaypointID();
  GPSPoint* entry;
  if(entryLaneID == 0)
    entry = rndf->getPerimeterPoint(entrySegmentID, entryWaypointID);
  else
    entry = rndf->getWaypoint(entrySegmentID, entryLaneID, entryWaypointID);
  if(!entry->isEntry())
    return exitVertices;

  int exitSegmentID;
  int exitLaneID;
  int exitWaypointID;
  GPSPoint* exit;
  for(unsigned i=0; i<vertices.size(); i++)
  {
    Vertex* exitVertex = vertices[i];
    exitSegmentID = exitVertex->getSegmentID();
    exitLaneID = exitVertex->getLaneID();
    exitWaypointID = exitVertex->getWaypointID();
    if(exitLaneID == 0)
      exit = rndf->getPerimeterPoint(exitSegmentID, exitWaypointID);
    else
      exit = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID);
    vector<GPSPoint*> allEntryPoints = exit->getEntryPoints();
    for(unsigned j=0; j<allEntryPoints.size(); j++)
    {
      if(allEntryPoints[j] == entry)
	exitVertices.push_back(exitVertex);
    }
  }
  return exitVertices;
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Print out the sequence of segment-level goals.
//-------------------------------------------------------------------------------------------------------------------------------
void printMission(vector<SegGoals> segGoals)
{
  for (unsigned i = 0; i < segGoals.size(); i++)
  {
    segGoals[i].print();
    cout<<endl;
  }
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Remove vertex from vertices
//-------------------------------------------------------------------------------------------------------------------------------
bool removeVertex(vector<Vertex*>& vertices, Vertex* vertex)
{
  unsigned i = 0;
  bool vertexFound = false;
  while(i < vertices.size())
  {
    if(vertices[i] != vertex)
      i++;
    else
    {
      vertices.erase(vertices.begin()+i,vertices.begin()+i+1);
      vertexFound = true;
    }
  }
  return vertexFound;
}


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Compute the distance between two GPSPoints
//-------------------------------------------------------------------------------------------------------------------------------
double computeDistanceLL(double lat1, double lon1, double lat2, double lon2)
{
  double UTMNorthing1, UTMEasting1, UTMNorthing2, UTMEasting2;
  char UTMZone[4];
  int RefEllipsoid = 23;

  LLtoUTM(RefEllipsoid, lat1, lon1, UTMNorthing1, UTMEasting1, UTMZone);
  LLtoUTM(RefEllipsoid, lat2, lon2, UTMNorthing2, UTMEasting2, UTMZone);

  return  sqrt(square(UTMEasting2 - UTMEasting1) + square(UTMNorthing2 - UTMNorthing1));
}


double avgSpeed(int segmentID, RNDF* rndf)
{
  return avgSpeed(rndf->getSegment(segmentID));
}

double avgSpeed(Segment* segment)
{
  return (segment->getMinSpeed() + segment->getMaxSpeed()) / 2;
}

bool adjacentSameDirection(Lane* lane1, Lane* lane2)
{
  return (lane1->getDirection() == lane2->getDirection());
  /* Morlan's method
    return (computeDistanceLL(lane1->getWaypoint(1), lane2->getWaypoint(1)) <= 6.096);
  */
}

vector<int> getAdjacentLanes(Lane* lane, RNDF* rndf)
{
  vector<int> adjacentLanes;
  int segmentID = lane->getSegmentID();
  int numOfLanes = rndf->getSegment(segmentID)->getNumOfLanes();

  for(int i = 1; i <= numOfLanes; i++)
    if(i != lane->getLaneID() && adjacentSameDirection(lane, rndf->getLane(segmentID, i)))
      adjacentLanes.push_back(i);
      
  return adjacentLanes;
}
  

double square(double x)
{
  return x * x;
}

