#include "Segment.hh"
#include "Lane.hh"
#include <string>
#include <iostream>
using namespace std;

Segment::Segment(int m, int numOfLanes)
{
  this->segmentID = m;
  this->numOfLanes = numOfLanes;
  minSpeed = maxSpeed = 0;
}

Segment::~Segment()
{
  for(unsigned i = 0; i < lanes.size(); i++)
    delete lanes[i];
}

int Segment::getSegmentID()
{
  return this->segmentID;
}

int Segment::getNumOfLanes()
{
  return this->numOfLanes;
}

string Segment::getSegmentName()
{
  return this->segmentName;
}

void Segment::setSegmentName(string segmentName)
{
  this->segmentName = segmentName;
}

int Segment::getMinSpeed()
{
  return minSpeed;
}

int Segment::getMaxSpeed()
{
  return maxSpeed;
}

void Segment::setSpeedLimits(int minSpeed, int maxSpeed)
{
  this->minSpeed = minSpeed;
  this->maxSpeed = maxSpeed;
}

Lane* Segment::getLane(int laneID)
{
  for(unsigned i = 0; i < lanes.size(); i++)
  {
    if (lanes[i]->getLaneID() == laneID)
      return lanes[i];
  }
  return NULL;
}

void Segment::addLane(Lane* lane)
{  
  if(lane->getSegmentID() == segmentID)
      this->lanes.push_back(lane);
}
