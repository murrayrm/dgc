#ifndef RNDF_HH_
#define RNDF_HH_
#include "Segment.hh"
#include "Lane.hh"
#include "Waypoint.hh"
#include "Zone.hh"
#include "GPSPoint.hh"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
using namespace std;

/*! RNDF class. The RNDF class represents a RNDF and MDF, route network
 * definition file and Mission Data File provided by DARPA. RNDF and MDF 
 * files are loaded using the loadFile(char* fileName) method. Segments and 
 * Zones are contained in the RNDF class. 
 * \brief The RNDF class represents a RNDF and MDF provided by DARPA.
 */
class RNDF
{
  public:
/*! All RNDFs initially contain no information. */
    RNDF();
  	virtual ~RNDF();

/*! Returns a pointer to a Segment with the segmentID passed in as an argument. */
    Segment* getSegment(int segmentID);
    
/*! Returns a pointer to a Lane with the segmentID and laneID passed in as 
 * arguments. */    
    Lane* getLane(int segmentID, int laneID);
    
/*! Returns a pointer to a Waypoint with the segmentOrZoneID, laneID and
 *  waypointID passed in as arguments. */  
    Waypoint* getWaypoint(int segmentOrZoneID, int laneID, int waypointID);
    
/*! Returns a pointer to a Zone with the zoneID passed in as an argument. */
    Zone* getZone(int zoneID);
    
/*! Returns a pointer to a PerimeterPoint with the zoneID and perimeterPointID
 *  passed in as arguments. */
    PerimeterPoint* getPerimeterPoint(int zoneID, int perimeterPointID);
    
/*! Returns a pointer to a ParkingSpot with the zoneID and spotID passed in as 
 *  arguments. */
    ParkingSpot* getSpot(int zoneID, int spotID);
    
/*! Returns the number of segments contained in THIS. */
    int getNumOfSegments();
    
/*! Returns the number of zones contained in THIS. */
    int getNumOfZones();
    
/*! Returns the vector of checkpoint IDs. */
    vector<Waypoint*> getMission();
    
/*! Insert a waypoint right before the given waypoint (waypointID). */
    void addExtraWaypoint(int, int, int, double, double);

/*! Loads a RNDF or MDF. */
    bool loadFile(char* fileName);
    
  private:
    int numOfSegments, numOfZones;
    vector<Segment*> segments;
    vector<Zone*> zones;
    vector<int> mission;               // The vector of checkpoint IDs that we have to visit in order
    vector<Waypoint*> ckpts;           // The vector of waypoints that are checkpoints
    vector<int> ckptIDs;               // The checkpoint ID of the correspoinding waypoints stored in ckpts
    
    void parseSegment(ifstream*);
    void parseLane(ifstream*, int);
    void parseWaypoint(ifstream*, int, int);
    void parseZone(ifstream*);
    void parsePerimeter(ifstream*, int);
    void parseSpot(ifstream*, int);
    void parseExit(ifstream*);
    void parseCheckpoint(ifstream*);
    void parseSpeedLimit(ifstream*);
    
    void setSegmentsAndZones(ifstream*);
    
    void addSegment(int, int);
    void setSegmentName(int, string);
    void setSpeedLimits(int, int, int);
    
    void addLane(int, int, int);
    void setLaneWidth(int, int, int);
    void setLeftBoundary(int, int, string);
    void setRightBoundary(int, int, string);
    
    void addWaypoint(int, int, int, double, double);
    void setCheckpoint(int, int, int, int);
    Waypoint* getCheckpoint(int);
    void setStopSign(int, int, int);
    void setExit(int, int, int, int, int, int);
    
    void addZone(int, int);
    void setZoneName(int, string);
    void addPerimeterPoint(int, int, double, double);
    
    void addSpot(int, int);
    void setSpotWidth(int, int, int);
    void addSpotWaypoint(int, int, int, double, double);
};

#endif /*RNDF_HH_*/
