#ifndef MISSIONPLANNER_HH
#define MISSIONPLANNER_HH

#include "StateClient.h"
#include "estop.h"
#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <string>
#include "RNDF.hh"
#include "Graph.hh"
#include "LatLon2UTM.hh"
#include "missionUtils.hh"
#include "SegGoalsTalker.hh"
#include "GloNavMapTalker.hh"
#include <vector>
#include <math.h>


/**
 * MissionPlanner class.
 */
class CMissionPlanner : public CStateClient, public CGloNavMapTalker, public CSegGoalsTalker
{
  RNDF* m_rndf;
  int m_rndfRevNum;                // RNDF revision#

  vector<SegGoals> m_currentSegGoalsSeq;
  double m_currentSegGoalsSeqCost;
  vector<SegGoals> m_nextSegGoalsSeq;
  double m_nextSegGoalsSeqCost;

  SegGoalsStatus* tplannerStatus;

  RNDF* m_receivedRndf;
  int m_receivedRndfRevNum;

  Graph* m_rndfGraph;

  vector<Waypoint*> m_checkpointSequence;
  int m_nextCheckpointIndex;

  bool isCurrentSegGoalsCompleted;


  // Whether at least one RNDF object was received
  bool m_bReceivedAtLeastOneGloNavMap;
  pthread_mutex_t m_GloNavMapReceivedMutex;
  pthread_cond_t m_GloNavMapReceivedCond;


  // The mutex to protect the RNDF we're planning on
  pthread_mutex_t m_GloNavMapMutex;

  // The skynet socket for sending segment-level goals
  int m_segGoalsSocket;
  // The skynet socket for communication with Global Navigation Map to request the RNDF object
  int m_GloNavMapRequestSocket;


public:
  /*! Contstructor */
  CMissionPlanner(int skynetKey, bool bWaitForStateFill, RNDF* rndf);
  /*! Standard destructor */
  ~CMissionPlanner();

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface.  This method does not run a loop. */
  void UpdateSparrowVariablesLoop();

  /*! This is the function that continually runs the planner in a loop */
  void MPlanningLoop(void);

  /*! this is the function that continually reads Global Navigation map */
  void getGloNavMapThread();

  /*! this is the function that continually reads tplanner status and update the executing goalID*/
  void getTPlannerStatusThread();

  /*! This function is used to request the full map from World Map */
  void requestFullWorldMap();

private:
  void initializeSegGoals();

};

#endif  // MISSIONPLANNER_HH
