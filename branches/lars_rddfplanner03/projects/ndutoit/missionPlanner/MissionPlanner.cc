#include "MissionPlanner.hh"
#include "DGCutils"
#include "StateClient.h"
#include <pthread.h>

using namespace std;

#define MAX_DELTA_SIZE 100000

CMissionPlanner::CMissionPlanner(int sn_key, bool bWaitForStateFill, RNDF* rndf)
	: CSkynetContainer(MODmissionplanner, sn_key),
	  CStateClient(bWaitForStateFill)
{
  m_rndf = rndf;
  m_rndfRevNum = 0;
  m_checkpointSequence = rndf->getMission();
  m_nextCheckpointIndex = 0;
  m_rndfGraph = new Graph();
  addAllVertices(m_rndf, m_rndfGraph);
  addAllEdges(m_rndf, m_rndfGraph);
  initializeSegGoals();  
  tplannerStatus = new SegGoalsStatus();
  isCurrentSegGoalsCompleted = false;
  m_segGoalsSocket = m_skynet.get_send_sock(SNsegGoals);
  m_GloNavMapRequestSocket = m_skynet.get_send_sock(SNgloNavMapRequest);
  DGCcreateMutex(&m_GloNavMapMutex);
  DGCcreateMutex(&m_GloNavMapReceivedMutex);
  DGCcreateCondition(&m_GloNavMapReceivedCond);
}

CMissionPlanner::~CMissionPlanner() 
{
  delete m_rndf;
  delete m_receivedRndf;
  delete m_rndfGraph;

  DGCdeleteMutex(&m_GloNavMapMutex);
  DGCdeleteMutex(&m_GloNavMapReceivedMutex);
  DGCdeleteCondition(&m_GloNavMapReceivedCond);
}

void CMissionPlanner::getGloNavMapThread()
{
  // The skynet socket for receiving Global Navigation map
  int GloNavMapSocket = m_skynet.listen(SNgloNavMap, MODgloNavMapLib);
  if(GloNavMapSocket < 0)
    cerr << "MissionPlanner::getWorldMapThread(): skynet listen returned error" << endl;

  while(true)
  {
    int deltasize;
    bool GloNavMapChanged = RecvGloNavMap(GloNavMapSocket, m_receivedRndf, &deltasize);

    // set the condition to signal that the first world map was received
    if (!m_bReceivedAtLeastOneGloNavMap && GloNavMapChanged)
      DGCSetConditionTrue(m_bReceivedAtLeastOneGloNavMap, m_GloNavMapReceivedCond, m_GloNavMapReceivedMutex);
  }
}

void CMissionPlanner::getTPlannerStatusThread()
{
  // The skynet socket for receiving tplanner status
  int tplannerStatusSocket = m_skynet.listen(SNtplannerStatus, MODmissionplanner);
  if(tplannerStatusSocket < 0)
    cerr << "MissionPlanner::getTPlannerStatusThread(): skynet listen returned error" << endl;

  while(true)
  {
    RecvSegGoalsStatus(tplannerStatusSocket, tplannerStatus);
  }
}

void CMissionPlanner::MPlanningLoop(void) 
{
  vector<Vertex*> route;
  bool bPlanFromCurrentPosition;

  cout << "Waiting for a world map" << endl;

/*
  // don't try to plan until at least one world map is received.
  DGCWaitForConditionTrue(m_bReceivedAtLeastOneRndf, m_GloNavMapReceivedCond, m_GloNavMapReceivedMutex);
*/
  while(true)
  {
    UpdateActuatorState();

    // plan from current position if we're not in run
    // plan from previous plan if we're in run and at least one plan came through

    // if we're not in RUN, plan from veh state

    if(m_actuatorState.m_estoppos != RUN)
      bPlanFromCurrentPosition = true;
    else
      bPlanFromCurrentPosition = false;
    
    // Update current goals if we're not in run.
    if (bPlanFromCurrentPosition && tplannerStatus->status)
    {
      double distance;
      Waypoint* currentMission = m_checkpointSequence[m_nextCheckpointIndex];
      UpdateState();

      DGClockMutex(&m_GloNavMapMutex);
      GPSPoint* closestGPSPoint = findClosestGPSPoint(m_state.Northing, m_state.Easting, 0, 0, distance, m_rndf);
      bool pointAdded = addGPSPointToGraph (closestGPSPoint, m_rndfGraph, m_rndf);
      Vertex* vertex1 = m_rndfGraph->getVertex(closestGPSPoint->getSegmentID(), closestGPSPoint->getLaneID(),
					       closestGPSPoint->getWaypointID());
      Vertex* vertex2 = m_rndfGraph->getVertex(currentMission->getSegmentID(), currentMission->getLaneID(),
					       currentMission->getWaypointID());
      cout << "finding route from ";
      closestGPSPoint->print();
      cout << " to ";
      currentMission->print();
      bool routeFound = findRoute(vertex1, vertex2, m_rndf, m_rndfGraph, route, m_currentSegGoalsSeqCost);
      if (!routeFound)
	cout<<": route not found"<<endl;
      m_currentSegGoalsSeq = findSegGoals(route, m_rndf);

      if (pointAdded)
	m_rndfGraph->removeVertex(closestGPSPoint->getSegmentID(), closestGPSPoint->getLaneID(), closestGPSPoint->getWaypointID());

      DGCunlockMutex(&m_GloNavMapMutex);
    }
    // Update current goals and next goals when the traffic planner finish the current goals.
    else if (isCurrentSegGoalsCompleted)
    {
      m_nextCheckpointIndex++;
      m_currentSegGoalsSeq = m_nextSegGoalsSeq;
      m_currentSegGoalsSeqCost = m_nextSegGoalsSeqCost;
      DGClockMutex(&m_GloNavMapMutex);
      Waypoint* currentMission = m_checkpointSequence[m_nextCheckpointIndex];
      Waypoint* nextMission = m_checkpointSequence[m_nextCheckpointIndex + 1];
      Vertex* vertex1 = m_rndfGraph->getVertex(currentMission->getSegmentID(), currentMission->getLaneID(), 
					       currentMission->getWaypointID());
      Vertex* vertex2 = m_rndfGraph->getVertex(nextMission->getSegmentID(), nextMission->getLaneID(), nextMission->getWaypointID());
      findRoute(vertex1, vertex2, m_rndf, m_rndfGraph, route, m_nextSegGoalsSeqCost);
      m_nextSegGoalsSeq = findSegGoals(route, m_rndf);
      DGCunlockMutex(&m_GloNavMapMutex);
    }
  }
}

void CMissionPlanner::requestFullWorldMap()
{
/*
  bool bRequestMap = true;
  m_skynet.send_msg(m_mapRequestSocket,&bRequestMap, sizeof(bool) , 0);
*/
}

void CMissionPlanner::initializeSegGoals()
{
  double distance;
  vector<Vertex*> route;
  Waypoint* currentMission = m_checkpointSequence[m_nextCheckpointIndex];
  Waypoint* nextMission = m_checkpointSequence[m_nextCheckpointIndex + 1];
  UpdateState();

  DGClockMutex(&m_GloNavMapMutex);
  GPSPoint* closestGPSPoint = findClosestGPSPoint(m_state.Northing, m_state.Easting, 0, 0, distance, m_rndf);
  bool pointAdded = addGPSPointToGraph (closestGPSPoint, m_rndfGraph, m_rndf);
  cout << endl;
  Vertex* vertex1 = m_rndfGraph->getVertex(closestGPSPoint->getSegmentID(), closestGPSPoint->getLaneID(),
					 closestGPSPoint->getWaypointID());
  Vertex* vertex2 = m_rndfGraph->getVertex(currentMission->getSegmentID(), currentMission->getLaneID(), currentMission->getWaypointID());
  Vertex* vertex3 = m_rndfGraph->getVertex(nextMission->getSegmentID(), nextMission->getLaneID(), nextMission->getWaypointID());
  findRoute(vertex1, vertex2, m_rndf, m_rndfGraph, route, m_currentSegGoalsSeqCost);
  m_currentSegGoalsSeq = findSegGoals(route, m_rndf, 1);
  findRoute(vertex2, vertex3, m_rndf, m_rndfGraph, route, m_nextSegGoalsSeqCost);
  m_nextSegGoalsSeq = findSegGoals(route, m_rndf);
  
  cout << "Starting from waypoint ";
  closestGPSPoint->print();
  cout << " to waypoint ";
  currentMission->print();
  cout << endl;
  printMission(m_currentSegGoalsSeq);
  cout<<endl;
  cout << "next mission: from waypoint ";
  currentMission->print();
  cout << " to waypoint ";
  nextMission->print();
  cout << endl;
  printMission(m_nextSegGoalsSeq);
  cout << endl;
  
  
  if (pointAdded)
    m_rndfGraph->removeVertex(closestGPSPoint->getSegmentID(), closestGPSPoint->getLaneID(), closestGPSPoint->getWaypointID());
  DGCunlockMutex(&m_GloNavMapMutex);
}
