#ifndef MISSIONUTILS
#define MISSIONUTILS

#include <iostream>
#include <string>
#include "RNDF.hh"
#include "Graph.hh"
#include "LatLon2UTM.hh"
#include <vector>
#include <math.h>
using namespace std;

struct SegGoals
{
  SegGoals()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    // memset(this, 0, sizeof(*this));
  }
  void print()
  {
    cout<<"( ";
    for(unsigned i = 0; i < entries.size(); i++)
    {
      entries[i]->print();
      cout<<"  ";
    }
    cout << ")\t -> \t( ";
    for(unsigned i = 0; i < exits.size(); i++)
    {
      exits[i]->print();
      cout<<"  ";
    }
    cout<<")\t\t"<<segment_type;
  }
  int goalID;
  int globalMapRevisionNumber;
  vector<GPSPoint*> entries;
  vector<GPSPoint*> exits;
  int perf_level;
  string segment_type;
  string action;
};

struct SegGoalsStatus
{
  SegGoalsStatus()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int goalID;
  int currentLaneID;
  bool status;
  string leftConstraint;
  string rightConstraint;
  string frontConstraint;
};


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Add the following vertices to rndfGraph
// * exit points
// * entry points
// * checkpoints
// * entry perimeter points
// * exit perimeter points
// * parking spot waypoints
//-------------------------------------------------------------------------------------------------------------------------------
void addAllVertices(RNDF*, Graph*);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Add the following edges to rndfGraph
// * exit point -> entry points
// * entry point -> closest exit points
// * closest entry points -> checkpoint
// * checkpoint -> closest exit points
// * parking spot -> exit points
// * entry perimeter point -> exit perimeter points
// * entry perimenter point -> parking spots
//-------------------------------------------------------------------------------------------------------------------------------
void addAllEdges(RNDF*, Graph*);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Add edges from the specified entry point to its closest exit points (both on the same lane and adjacent lanes).
//-------------------------------------------------------------------------------------------------------------------------------
void addEdgesFromEntry(RNDF*, Graph*, GPSPoint*);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Add edges from the specified entry perimeter point to all its exit perimeter points and to all the checkpoints in the zone.
//-------------------------------------------------------------------------------------------------------------------------------
void addEdgesFromEntryPerimeter(RNDF*, Graph*, GPSPoint*);

//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Compute the optimal route from vertex1 to vertex2 in rndfGraph using Dijkstra's algorithm
//-------------------------------------------------------------------------------------------------------------------------------
bool findRoute(Vertex*, Vertex*, RNDF*, Graph*, vector<Vertex*>&, double&);



//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Determine the segment-level goals from route.
//-------------------------------------------------------------------------------------------------------------------------------
vector<SegGoals> findSegGoals(vector<Vertex*>, RNDF*);
vector<SegGoals> findSegGoals(vector<Vertex*>, RNDF*, int);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Determine the type of a segment from vertex1 to vertex2. possible types are
// * ROAD_SEGMENT
// * PARKING_ZONE
// * INTERSECTION
// * PREZONE
//-------------------------------------------------------------------------------------------------------------------------------
string findType(Vertex*, Vertex*, RNDF*);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Find the closest waypoint or perimeter point on the specified segment and lane. If segmentID = 0, then find the closest 
// waypoint on any segment. If laneID = 0, then find the closest waypoint on the specified segment on any lane.
//-------------------------------------------------------------------------------------------------------------------------------
GPSPoint* findClosestGPSPoint(double, double, int, int, double&, RNDF*);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Add a vertex corresponding to the specified GPSPoint (can be waypoint or perimeter point) to the graph with appropriate edges
//-------------------------------------------------------------------------------------------------------------------------------
bool addGPSPointToGraph (GPSPoint*, Graph*, RNDF*);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Find all the exit points that have entry as one of their associated entry points.
//-------------------------------------------------------------------------------------------------------------------------------
vector<Vertex*> findAllExits(Vertex*, RNDF*, Graph*);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Print out the sequence of segment-level goals.
//-------------------------------------------------------------------------------------------------------------------------------
void printMission(vector<SegGoals>);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Remove vertex from vertices
//-------------------------------------------------------------------------------------------------------------------------------
bool removeVertex(vector<Vertex*>&, Vertex*);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Compute the distance between two GPSPoints
//-------------------------------------------------------------------------------------------------------------------------------
double computeDistanceLL(double, double, double, double);


//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
// Other useful helper functions
//-------------------------------------------------------------------------------------------------------------------------------
double avgSpeed(int, RNDF*);
double avgSpeed(Segment*);
bool adjacentSameDirection(Lane*, Lane*);
vector<int> getAdjacentLanes(Lane*, RNDF*);
double square(double);


#endif
