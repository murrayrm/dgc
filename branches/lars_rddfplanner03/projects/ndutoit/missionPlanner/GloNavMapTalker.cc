#include "GloNavMapTalker.hh"

using namespace std;

CGloNavMapTalker::CGloNavMapTalker() {
  m_pDataBuffer = new char[sizeof(RNDF)];
  DGCcreateMutex(&m_dataBufferMutex);
}

CGloNavMapTalker::~CGloNavMapTalker() {
  delete [] m_pDataBuffer;  
  DGCdeleteMutex(&m_dataBufferMutex);
}

bool CGloNavMapTalker::RecvGloNavMap(int GloNavMapSocket, RNDF* receivedRndf, int* pSize) {
  int bytesToReceive;
  char* pBuffer = m_pDataBuffer;

  // Build the mutex list. We want to protect the data buffer.
  int numMutices = 1;
  pthread_mutex_t* ppMutices[numMutices];
  ppMutices[0] = &m_dataBufferMutex;

  // Get the Global Navigation Map (RNDF) data from skynet. We want to receive the whole RNDF, locking
  // the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  bytesToReceive = sizeof(RNDF);
  *pSize = m_skynet.get_msg(GloNavMapSocket, m_pDataBuffer, bytesToReceive, 0, ppMutices, false, numMutices);
  if(*pSize == bytesToReceive)
  {
    memcpy(receivedRndf, pBuffer, sizeof(RNDF));
    DGCunlockMutex(&m_dataBufferMutex);
    return true;
  }

  DGCunlockMutex(&m_dataBufferMutex);  
  return false;
}


bool CGloNavMapTalker::SendGloNavMap(int GloNavMapSocket, RNDF* rndf) {
  int bytesToSend;
  int bytesSent;
  char* pBuffer = m_pDataBuffer;
  
  DGClockMutex(&m_dataBufferMutex);  
  memcpy(pBuffer, (char*)rndf, sizeof(RNDF));

  bytesToSend = sizeof(RNDF);

  bytesSent = m_skynet.send_msg(GloNavMapSocket, m_pDataBuffer, bytesToSend, 0);
  
  DGCunlockMutex(&m_dataBufferMutex);  
  
  if(bytesSent != bytesToSend)
  {
    cerr << "CGloNavMapTalker::SendGloNavMap(): sent " << bytesSent << " bytes while expected to send " << bytesToSend << " bytes" << endl;
    return false;
  }

  return true;
}
