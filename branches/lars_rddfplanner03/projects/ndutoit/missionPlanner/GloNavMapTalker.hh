#ifndef GLONAVMAPTALKER_HH
#define GLONAVMAPTALKER_HH

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "SkynetContainer.h"
#include "DGCutils"
#include "CMap.hh"
#include "RNDF.hh"

class CGloNavMapTalker : virtual public CSkynetContainer {
  pthread_mutex_t m_dataBufferMutex;
  char* m_pDataBuffer;

public:
  CGloNavMapTalker();
  ~CGloNavMapTalker();

  bool SendGloNavMap(int mapdeltaSocket, RNDF* receivedRndf);
  bool RecvGloNavMap(int mapdeltaSocket, RNDF* rndf, int* pSize);
};

#endif // GLONAVMAPTALKER_HH
