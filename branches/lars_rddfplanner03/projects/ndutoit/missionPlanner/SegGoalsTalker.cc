#include "SegGoalsTalker.hh"
#include "DGCutils"

#include <iostream>
using namespace std;

CSegGoalsTalker::CSegGoalsTalker()
{
  m_pSegGoalsDataBuffer = new char[sizeof(SegGoals)];
  m_pSegGoalsStatusDataBuffer = new char[sizeof(SegGoalsStatus)];
  
  DGCcreateMutex(&m_segGoalsdataBufferMutex);
  DGCcreateMutex(&m_segGoalsStatusdataBufferMutex);
}

CSegGoalsTalker::~CSegGoalsTalker()
{
  delete [] m_pSegGoalsDataBuffer;
  delete [] m_pSegGoalsStatusDataBuffer;
  
  DGCdeleteMutex(&m_segGoalsdataBufferMutex);
  DGCdeleteMutex(&m_segGoalsStatusdataBufferMutex);
}

bool CSegGoalsTalker::SendSegGoals(int segGoalsSocket, SegGoals* pSegGoals, pthread_mutex_t* pMutex)
{
  int bytesToSend;
  int bytesSent;
  char* pBuffer = m_pSegGoalsDataBuffer;
  
  DGClockMutex(&m_segGoalsdataBufferMutex);
  if(pMutex != NULL)
    DGClockMutex(pMutex);
  
  memcpy(pBuffer, (char*)pSegGoals, sizeof(SegGoals));

  bytesToSend = sizeof(SegGoals);

  // 	cerr << "SegGoalsTalker: about to send " << bytesToSend << " bytes" << endl;

  bytesSent = m_skynet.send_msg(segGoalsSocket, m_pSegGoalsDataBuffer, bytesToSend, 0);
  
  DGCunlockMutex(&m_segGoalsdataBufferMutex);
  if(pMutex != NULL)
    DGCunlockMutex(pMutex);
  
  
  if(bytesSent != bytesToSend)
  {
    cerr << "CSegGoalsTalker::SendSegGoals(): sent " << bytesSent << " bytes while expected to send " << bytesToSend << " bytes" << endl;
    return false;
  }

  return true;
}

bool CSegGoalsTalker::RecvSegGoals(int segGoalsSocket, SegGoals* pSegGoals, pthread_mutex_t* pMutex, string* pOutstring)
{
  int bytesToReceive;
  int bytesReceived;
  char* pBuffer = m_pSegGoalsDataBuffer;

  // Build the mutex list. We want to protect the data buffer and, if requested, the segGoals
  int numMutices = 1;
  pthread_mutex_t* ppMutices[2];
  ppMutices[0] = &m_segGoalsdataBufferMutex;
  if(pMutex != NULL)
  {
    ppMutices[1] = pMutex;
    numMutices++;
  }

  // Get the segGoals data from skynet. We want to receive the whole segGoals, locking
  // the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  bytesToReceive = sizeof(SegGoals);
  bytesReceived = m_skynet.get_msg(segGoalsSocket, m_pSegGoalsDataBuffer, bytesToReceive, 0, ppMutices, false, numMutices);
  if(bytesReceived <= 0)
  {
    cerr << "CSegGoalsTalker::RecvSegGoals(): skynet error" << endl;
    DGCunlockMutex(&m_segGoalsdataBufferMutex);
    if(pMutex != NULL)
      DGCunlockMutex(pMutex);
    return false;
  }
  
  if(pOutstring != NULL)
  {
    pOutstring->append((char*)&bytesReceived, sizeof(bytesReceived));
    pOutstring->append(m_pSegGoalsDataBuffer, bytesReceived);
  }

// 	cerr << "SegGoalsTalker: received " << bytesReceived << " bytes" << endl;

  memcpy(pSegGoals, pBuffer, sizeof(SegGoals));

  DGCunlockMutex(&m_segGoalsdataBufferMutex);
  if(pMutex != NULL)
    DGCunlockMutex(pMutex);
  
  return true;
}

bool CSegGoalsTalker::RecvSegGoalsStatus(int tplannerStatusSocket, SegGoalsStatus* tplannerStatus, 
					 pthread_mutex_t* pMutex, string* pOutstring)
{
  int bytesToReceive;
  int bytesReceived;
  char* pBuffer = m_pSegGoalsStatusDataBuffer;

  // Build the mutex list. We want to protect the data buffer and, if requested, the segGoals
  int numMutices = 1;
  pthread_mutex_t* ppMutices[2];
  ppMutices[0] = &m_segGoalsStatusdataBufferMutex;
  if(pMutex != NULL)
  {
    ppMutices[1] = pMutex;
    numMutices++;
  }

  // Get the segGoals status data from skynet. We want to receive the whole status, locking
  // the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  bytesToReceive = sizeof(SegGoals);
  bytesReceived = m_skynet.get_msg(tplannerStatusSocket, m_pSegGoalsStatusDataBuffer, bytesToReceive, 0, ppMutices, false, numMutices);
  if(bytesReceived <= 0)
  {
    cerr << "CSegGoalsTalker::RecvSegGoalsStatus(): skynet error" << endl;
    DGCunlockMutex(&m_segGoalsStatusdataBufferMutex);
    if(pMutex != NULL)
      DGCunlockMutex(pMutex);
    return false;
  }
  
  if(pOutstring != NULL)
  {
    pOutstring->append((char*)&bytesReceived, sizeof(bytesReceived));
    pOutstring->append(m_pSegGoalsStatusDataBuffer, bytesReceived);
  }

// 	cerr << "SegGoalsTalker: received " << bytesReceived << " bytes" << endl;

  memcpy(tplannerStatus, pBuffer, sizeof(SegGoalsStatus));

  DGCunlockMutex(&m_segGoalsStatusdataBufferMutex);
  if(pMutex != NULL)
    DGCunlockMutex(pMutex);
  
  return true;
}

void CSegGoalsTalker::WaitForSegGoalsData(int segGoalsSocket)
{
  m_skynet.sn_select(segGoalsSocket);
}
