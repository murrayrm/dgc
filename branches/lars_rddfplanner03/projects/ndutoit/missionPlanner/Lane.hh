#ifndef LANE_HH_
#define LANE_HH_
#include <string>
#include <vector>
#include "Waypoint.hh"
using namespace std;

/*! Lane class. The Lane class represents a lane provided by the RNDF file.
 * All lanes have a segment ID, lane ID, and number of waypoints. Lanes may
 * have optional information including lane width and boundaries.
 * \brief The Lane class represents a lane provided by the RNDF file.
 */
class Lane
{
public:
/*! All lanes have a segment ID, lane ID, and number of waypoints. */
	Lane(int, int, int);
	virtual ~Lane();

/*! Returns the segment ID of THIS. */
  int getSegmentID();
  
/*! Returns the lane ID of THIS. */
  int getLaneID();
  
/*! Returns the number of waypoints contained in THIS. */
  int getNumOfWaypoints();
  
/*! Returns the lane width of THIS. */
  int getLaneWidth();
  
/*! Returns the left boundary of THIS. */
  string getLeftBoundary();
  
/*! Returns the right boundary of THIS. */
  string getRightBoundary();
  
/*! Sets the lane width of THIS. */
  void setLaneWidth(int);
  
/*! Sets the left boundary of THIS. */
  void setLeftBoundary(string);
  
/*! Sets the right boundary of THIS. */
  void setRightBoundary(string);
  
/*! Sets the number of waypoints of THIS. */
  void setNumOfWaypoints(int);
  
/*! Returns a pointer to a Waypoint with the waypoint ID passed in as an
 *  argument. */
  Waypoint* getWaypoint(int);
  
/*! Adds a waypoint to the array of waypoints contained in THIS. */
  void addWaypoint(Waypoint*);

/*! Returns the direction of THIS */
  int getDirection();
  
private:
  int segmentID, laneID, numOfWaypoints, laneWidth;
  int direction;
  string leftBoundary, rightBoundary;
  
/*! Each lane contains an array of waypoints of size numOfWaypoints. */
  vector<Waypoint*> waypoints;
};

#endif /*LANE_HH_*/
