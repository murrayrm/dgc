TRAFFICPLANNER_PATH = $(DGC)/projects/ndutoit/trafficPlanner

TRAFFICPLANNER_DEPEND_LIBS = \
	$(CMAPLIB) \
	$(CMAPPLIB) \
	$(RDDFLIB) \
	$(SPARROWLIB) \
	$(SKYNETLIB) \
	$(MODULEHELPERSLIB) \
	$(DGC)/projects/nok/missionPlanner/rndf.a \
	$(DGC)/projects/nok/missionPlanner/SegGoalsTalker.o \
	$(DGC)/projects/nok/missionPlanner/GloNavMapTalker.o \
	$(GGISLIB)

TRAFFICPLANNER_DEPEND_SOURCE = \
	$(TRAFFICPLANNER_PATH)/TrafficPlanner.hh \
	$(TRAFFICPLANNER_PATH)/TrafficPlanner.cc \
	$(TRAFFICPLANNER_PATH)/TrafficPlannerMain.cc \
        $(TRAFFICPLANNER_PATH)/Makefile

TRAFFICPLANNER_DEPEND = \
	$(TRAFFICPLANNER_DEPEND_LIBS) \
	$(TRAFFICPLANNER_DEPEND_SOURCE)