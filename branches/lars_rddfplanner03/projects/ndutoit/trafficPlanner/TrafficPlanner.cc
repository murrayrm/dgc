#include "TrafficPlanner.hh"
#include "DGCutils"

using namespace std;

#define MAX_DELTA_SIZE 100000
GisCoordLatLon latlon;
GisCoordUTM utm;

CTrafficPlanner::CTrafficPlanner(int sn_key, bool bWaitForStateFill, char* RNDFFileName)
	: CSkynetContainer(MODtrafficplanner, sn_key),
	  CStateClient(bWaitForStateFill),
          m_bReceivedAtLeastOneDelta(false)
{
  utm.zone = 11;
  utm.letter = 'S';

  m_GloNavMapRequestSocket = m_skynet.get_send_sock(SNlocalGloNavMapRequest);
  m_GloNavMapSocket = m_skynet.listen(SNlocalGloNavMap, MODgloNavMapLib);
  /* DON'T FORGET TO CHANGE THE NAME OF SKYNET SOCKET */
  m_fullMapRequestSocket = m_skynet.get_send_sock(SNfullmaprequest);
  requestGloNavMap();
  m_rndfRevNum = 0;

  m_rndf->print();
  /*
  m_rndf = new RNDF();
  if (!m_rndf->loadFile(RNDFFileName))
  {
    cerr << "Error:  Unable to load RNDF file " << RNDFFileName << ", exiting program" << endl;
    exit(1);
  }
  m_rndf->assignLaneDirection();
  m_rndfRevNum = 0;
  */

  localMapSocket = m_skynet.listen(SNtrafficLocalMap, MODmapping);
  m_bReceivedAtLeastOneLocalMap = false;
  
  if(localMapSocket < 0)
    cerr << "TrafficPlanner: skynet listen returned error" << endl;

  m_bReceivedAtLeastOneDelta = false;
  segGoalsSocket = m_skynet.listen(SNsegGoals, MODmissionplanner);
  if(segGoalsSocket < 0)
    cerr << "TrafficPlanner: skynet listen returned error" << endl;

  DGCcreateMutex(&m_GloNavMapMutex);
  DGCcreateMutex(&m_LocalMapMutex);
  DGCcreateMutex(&m_LocalMapRecvMutex);
  DGCcreateMutex(&m_ObstacleMutex);
  DGCcreateMutex(&m_CostMapMutex);
  DGCcreateMutex(&m_DPlannerStatusMutex);
  DGCcreateMutex(&m_SegGoalsMutex);
  DGCcreateMutex(&m_deltaReceivedMutex);
  DGCcreateCondition(&m_LocalMapRecvCond);
  DGCcreateCondition(&m_deltaReceivedCond);
}

CTrafficPlanner::~CTrafficPlanner() 
{
  delete m_rndf;
  
  for (unsigned i=0; i < m_obstacle.size(); i++)
  	delete m_obstacle[i];
  	
  delete m_dplannerStatus;

  DGCdeleteMutex(&m_GloNavMapMutex);
  DGCdeleteMutex(&m_LocalMapMutex);
  DGCdeleteMutex(&m_ObstacleMutex);
  DGCdeleteMutex(&m_CostMapMutex);
  DGCdeleteMutex(&m_DPlannerStatusMutex);
  DGCdeleteMutex(&m_SegGoalsMutex);
  DGCdeleteMutex(&m_deltaReceivedMutex);
  DGCdeleteCondition(&m_deltaReceivedCond);
}

void CTrafficPlanner::getMapDeltasThread()
{
  int mapLayer;
  char* elevationMapDelta = new char[MAX_DELTA_SIZE];

  // constants in GlobalConstants.h
  m_elevationMap.initMap(CONFIG_FILE_DEFAULT_MAP);

  // The skynet socket for receiving map deltas (from fusionmapper)
  int mapDeltaSocket = m_skynet.listen(SNdeltaElevationMap, MODmapping);
  if(mapDeltaSocket < 0)
    cerr << "CTrafficPlanner::getMapDeltasThread(): skynet listen returned error" << endl;

  while(true)
  {
    int deltasize;
    RecvMapdelta(mapDeltaSocket, elevationMapDelta, &deltasize);
    m_elevationMap.applyDelta<double>(mapLayer, elevationMapDelta, deltasize);

    // set the condition to signal that the first delta was received
    if(!m_bReceivedAtLeastOneDelta)
      DGCSetConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);
  }
}

void CTrafficPlanner::getLocalMapThread()
{
	//vector<Mapper::Segment> segments;
	Mapper::Map receivedLocalMap(localMapSegments);
	//Mapper::Map map;
	int size;
	
	while(true)
	{
		bool localMapReceived = RecvLocalMap(localMapSocket, &receivedLocalMap, &size);
		if (localMapReceived)
    	{
      		DGClockMutex(&m_LocalMapMutex);
      		m_localMap2 = receivedLocalMap;
	      	DGCunlockMutex(&m_LocalMapMutex);
	      	if (!m_bReceivedAtLeastOneLocalMap)
				DGCSetConditionTrue(m_bReceivedAtLeastOneLocalMap, m_LocalMapRecvCond, m_LocalMapRecvMutex);
    	}
	//m_localMap2.print();
	}

}

void CTrafficPlanner::getDPlannerStatusThread()
{
  // The skynet socket for receiving dplanner status
  int dplannerStatusSocket = m_skynet.listen(SNdplannerStatus, MODdynamicplanner);
  DPlannerStatus* dplannerStatus = new DPlannerStatus();
  if(dplannerStatusSocket < 0)
    cerr << "TrafficPlanner::getDPlannerStatusThread(): skynet listen returned error" << endl;

//  while(true)
//  {
//    bool dPlannerStatusReceived = RecvDPlannerStatus(dplannerStatusSocket, dplannerStatus);
//    /* YOU NEED TO FIGURE OUT WHAT TO DO HERE */
//    if (dPlannerStatusReceived)
//    {
//      DGClockMutex(&m_DPlannerStatusMutex);
//      m_dplannerStatus = dplannerStatus;
//      DGCunlockMutex(&m_DPlannerStatusMutex);      
//    }
//  }
}

void CTrafficPlanner::getSegGoalsThread()
{
  SegGoals* segGoals = new SegGoals();
  while(true)
  {
    bool segGoalsReceived = RecvSegGoals(segGoalsSocket, segGoals);
    if (segGoalsReceived)
    {
      DGClockMutex(&m_SegGoalsMutex);
      m_segGoals.push_back(*segGoals);
      DGCunlockMutex(&m_SegGoalsMutex);
    }
  }
}

void CTrafficPlanner::requestGloNavMap()
{

  int deltasize;
  RNDF* receivedRndf = new RNDF();

  bool bRequestMap = true;
  m_skynet.send_msg(m_GloNavMapRequestSocket,&bRequestMap, sizeof(bool) , 0);

  cout << "Waiting for Global Navigation Map" << endl;

  bool GloNavMapReceived = RecvGloNavMap(m_GloNavMapSocket, receivedRndf, &deltasize);
  
  //GloNavMapReceived = true;
  //************************************************************
  
  if (GloNavMapReceived)
  {
    cout << "Received a new GloNav map" << endl;    
    
    DGClockMutex(&m_GloNavMapMutex);
    m_rndf = receivedRndf;
    DGCunlockMutex(&m_GloNavMapMutex);
  }
}


void CTrafficPlanner::TPlanningLoop(void)
{
	/***************************************************************************/
	/* CONFIRM THAT WE HAVE JOIN THE SEGGOALS GROUP */
		
  	SegGoalsStatus* segGoalsStatus = new SegGoalsStatus();
  	segGoalsStatus->goalID = 0;
  	segGoalsStatus->status = COMPLETED;

  	int segGoalsStatusSocket = m_skynet.get_send_sock(SNtplannerStatus);
	int rddfSocket = m_skynet.get_send_sock(SNrddf);

  	if(segGoalsStatusSocket < 0)
    	cerr << "TrafficPlanner: skynet get_send_sock returned error" << endl;

  	// Notify the mission planner that I'm joinning the group
	bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
	if (!statusSent)
    	cout << "Error sending notification" << endl;
	else
    	cout << "Successfully notified mission planner that I'm joining the group" <<  endl;


	//cout << "TPlanningLoop(): Waiting for elevation map" << endl;

	// don't send goals until traffic planner starts listening
	//DGCWaitForConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);
	
	//cout << "Waiting for local map ...";
	//DGCWaitForConditionTrue(m_bReceivedAtLeastOneLocalMap, m_LocalMapRecvCond, m_LocalMapRecvMutex);
	//cout << "done" << endl;
	/******************************************************************************/
	// OUTER LOOP - runs continuously
	
	while(true)
	{
		
		// SPOOF SOME SEGMENT GOALS DATA
		SegGoals segmentGoals = spoofSegmentGoal();
		m_segGoals.push_back(segmentGoals);
				
		// Check that I have enough goals to plan with
  	  	SegGoals currentSegGoals, nextSegGoals, nextNextSegGoals;
    	DGClockMutex(&m_SegGoalsMutex);
    	unsigned numSegGoals = m_segGoals.size();
    	if (numSegGoals > 0)
    	{
    		cout << endl;
			cout << "Using new segment goal: ";
    		
      		currentSegGoals = m_segGoals.front();
      		
      		cout << "current goalID " << currentSegGoals.goalID << endl;
      		cout << endl;
      		m_segGoals.pop_front();
    	}
    	DGCunlockMutex(&m_SegGoalsMutex);
    	
    	/*************************************************************************/
    	/* Decide how far I need to plan */
//    	PlanningHorizon planningHorizon;    	
//    	switch (currentSegGoals.segment_type)
//    	{
//    		case ROAD_SEGMENT:
//    			if (currentSegGoals.stopAtExit)
//    			{
//    				cout << "Need to plan in current segment only" << endl;
//    				planningHorizon = CURRENT_SEGMENT;
//    			} else
//    			{
//    				if (nextSegGoals.segment_type == INTERSECTION)
//    				{
//    					cout << "Need to plan to the next next segment (intersection and road segment beyond)"
//    					planningHorizon = NEXT_NEXT_SEGMENT;
//    				} else if (nextSegGoals.segment_type == PARKING_ZONE)
//    				{
//    					cout << "Need to plan in the parking zone only" << endl;
//    					planningHorizon = CURRENT_SEGMENT;
//    				}
//    			}
//    			break;
//    		case INTERSECTION:
//    			cout << "Need to plan to the next segment (road segment)" << endl;
//    			planningHorizon = NEXT_SEGMENT;
//    			break;
//    		case PARKING_ZONE:
//    			cout << "Need to plan in the current segment only" << endl;
//    			planningHorizon = CURRENT_SEGMENT;
//    			break;
//    		case UTURN:
//    			cout << "Need to plan in the current segment only" << endl;
//    			planningHorizon = CURRENT_SEGMENT;
//    			break;
//    		default:
//    			cout << "Something is likely wrong in segment type specification, or deciding how far I need to plan" << endl;
//    			planningHorizon = CURRENT_SEGMENT; //bring vehicle to safe stop so that we can replan or something
//    	}    	
    	
    	/*************************************************************************/
    	// INNER LOOP - planning loop
    	bool stayInLoopFlag = true;
		while(stayInLoopFlag)
		{
			double x_A,y_A, vx_A, vy_A;
			double dist2Exit;
			cout << endl;
	    	if (numSegGoals > 0)
	    	{
				/************************************************************************/
			    // READ ALICE POSITION    
	//		    UpdateState(); // from here I get m_state, which has fields m_state.Northing and m_state.Easting
	//		    double x_A = m_state.Easting;
	//		    double y_A = m_state.Northing;
				cout << "Enter Alice position: x = ";
				cin >> x_A;
				cout << "Enter Alice position: y = ";
				cin >> y_A;			
				cout << "Enter Alice velocity: vx = ";
				cin >> vx_A;
				cout << "Enter Alice velocity: vy = ";
				cin >> vy_A;
				y_A = y_A + 3778142;
				x_A = x_A + 396412;	
	
				/************************************************************************/
	 		    // EXTRACT INFO FROM LOCAL MAP
	 		    // NEED TO LOCK MUTEX HERE
	 		    m_localMap.print();
	 		    cout << "here now" << endl;
	 		    Mapper::Segment LMcurrentSegment = m_localMap.getSegment(1);
				//Mapper::Segment LMcurrentSegment = m_localMap.getSegment(currentSegGoals.entrySegmentID);
				cout << "here now" << endl;
				//Mapper::Segment LMnextSegment = m_localMap.getSegment(currentSegGoals.entrySegmentID);
				//Mapper::Segment LMnextNextSegment = m_localMap.getSegment(currentSegGoals.entrySegmentID);
				//Mapper::Segment localMapSegment = m_localMap.getSegment(1);
				// NEED TO UNLOCK MUTEX HERE
				Mapper::Lane LMCS_entryLane = LMcurrentSegment.getLane(currentSegGoals.entryLaneID);
				vector<Mapper::Location> LB = LMCS_entryLane.getLB();
				vector<Mapper::Location> RB = LMCS_entryLane.getRB();
				printf("Testing Map accessor functions.\n");
				printf("length of LB vector = %d/n", LB.size());
				
	    		cout << "Spoofing road object data...";
	 		    int segmentID = currentSegGoals.entrySegmentID;
			    int numLanes = 1;
			    int numTravelLanes = 1;
			    int senseMin = 0;
			    int senseMax = 100;
			    double laneWidth = 10;
			    int xSpacing = 10;
				LocalMapObject m_localMap2 = spoofLocalMapData(segmentID, numLanes, numTravelLanes, laneWidth, senseMin, senseMax, xSpacing);
				//LocalMapObject m_localMap = spoofLocalMapData2();
				cout << "done" << endl;
	    		
				/************************************************************************/
	 		    // EXTRACT INFO FROM RNDF
				//Segment* rndfSegment = m_rndf->getSegment(segmentID); // this is in the RNDF class
				Lane* CSLane = m_rndf->getLane(currentSegGoals.entrySegmentID, currentSegGoals.entryLaneID);
				Waypoint* CSWaypt;
				for (int ii=0; ii<CSLane->getNumOfWaypoints(); ii++)
				{
					CSWaypt = m_rndf->getWaypoint(currentSegGoals.entrySegmentID, currentSegGoals.entryLaneID, ii+1);
					double CSWayptX = CSWaypt->getEasting();
					double CSWayptY = CSWaypt->getNorthing();
					printf("Waypt %d location (from RNDF) = [%3.3f, %3.3f]\n", ii+1, CSWayptX, CSWayptY);
				}
				// THIS WORKS
		
				/************************************************************************/
			    /* CONVERT TO COST MAP */
			    // Convert elevation map to cost map
			    cout << "Converting elevation map to cost...";
				CMapPlus m_costMap;
				DGClockMutex(&m_CostMapMutex);
			    elevation2Cost(m_elevationMap, m_costMap);
			    DGCunlockMutex(&m_CostMapMutex);		    
				cout << "done" << endl;
				
				/************************************************************************/
			    /* EXTRACT TRAFFIC RULES */
			    cout << "Extract traffic rules...";
			    vector<TrafficRules> trafficRules;
		   		trafficRules = extractRules(currentSegGoals.segment_type);
				cout << "done" << endl;
				
				/************************************************************************/
			    /* PARAMETER ESTIMATION */
				int currentLane = 1;
				double dLimit = 50;
				Waypoint* CSExitWaypt = m_rndf->getWaypoint(currentSegGoals.exitSegmentID, currentSegGoals.exitLaneID, currentSegGoals.exitWaypointID);
				double CSExitWayptX = CSExitWaypt->getEasting();
				double CSExitWayptY = CSExitWaypt->getNorthing();
				double dist2Exit = sqrt(pow(CSExitWayptX-x_A,2) + pow(CSExitWayptY-y_A,2));
				//double dist2Exit = 100;	
				cout << "Distance to exit = " << dist2Exit << endl;			
				/************************************************************************/
				/* DECISION MAKING */
				
				
				/************************************************************************/	
				/* GENERATE CONSTRAINTS */
				cout << "Generating constraints...";
				ConstraintSet constraintSet;
				vector<Location> currentLaneLBCoord, currentLaneRBCoord;
				Divider currentLaneLBType, currentLaneRBType;
				
				if (currentSegGoals.segment_type == ROAD_SEGMENT)
				{
					currentLaneLBType =  m_localMap2.segment.lane[currentLane-1].laneLBType;
					currentLaneRBType =  m_localMap2.segment.lane[currentLane-1].laneRBType;
					for (unsigned ii=0;ii<m_localMap2.segment.lane[currentLane-1].laneLBCoord.size();ii++)
					{
						currentLaneLBCoord.push_back(m_localMap2.segment.lane[currentLane-1].laneLBCoord[ii]);
					} 
					for (unsigned ii=0;ii<m_localMap2.segment.lane[currentLane-1].laneRBCoord.size();ii++)
					{
						currentLaneRBCoord.push_back(m_localMap2.segment.lane[currentLane-1].laneRBCoord[ii]);
					} 
				} 				
				
				// Put all the constraints into a set
				constraintSet = laneFollowing(currentLaneLBCoord, currentLaneRBCoord, currentLaneLBType, currentLaneRBType, trafficRules, dLimit, dist2Exit, x_A, y_A);//works
				cout << "done" << endl;
			
//				cout << "number of LB constraints = " << constraintSet.leftBoundaryConstraint.size() << endl;
//				cout << "number of RB constraints = " << constraintSet.rightBoundaryConstraint.size() << endl;
//				for (int ii=0;ii<(int)constraintSet.leftBoundaryConstraint.size();ii++)
//				{
//				    printf("LB constraint [%d]: xrange = [%3.3f\t, %3.3f] and yrange = [%3.3f\t, %3.3f]\n", ii, 
//				    	constraintSet.leftBoundaryConstraint[ii].xrange1, constraintSet.leftBoundaryConstraint[ii].xrange2,  
//				    	constraintSet.leftBoundaryConstraint[ii].yrange1, constraintSet.leftBoundaryConstraint[ii].yrange2);
//				}
//				for (int ii=0;ii<(int)constraintSet.rightBoundaryConstraint.size();ii++)
//				{
//				    printf("RB constraint [%d]: xrange = [%3.3f\t, %3.3f] and yrange = [%3.3f\t, %3.3f]\n", ii, 
//				    	constraintSet.rightBoundaryConstraint[ii].xrange1, constraintSet.rightBoundaryConstraint[ii].xrange2,  
//				    	constraintSet.rightBoundaryConstraint[ii].yrange1, constraintSet.rightBoundaryConstraint[ii].yrange2);
//				}

				/************************************************************************/    
			    /* MERGE CONSTRAINTS */
				
				
				/************************************************************************/    
			    /* POST-PROCESS CONSTRAINTS 											*/
			    /* THIS IS A QUICK HACK TO GET RDDF's FROM CONSTRAINTS					*/
			    cout << "Generating rddf...";
			    
			    //RDDF* newRddf = new RDDF("rddf.dat",true);
			    
			    RDDF* newRddf = new RDDF(NULL, true);
	    		
	    		// THIS IS WHERE CONVRDDF FITS IN
			    bool rddfError;
			    rddfError = convRDDF(constraintSet.rightBoundaryConstraint, 
			    	constraintSet.leftBoundaryConstraint, x_A, y_A, laneWidth, newRddf);
			    
			    if (rddfError)
			    {
			    	cout << "Error with rddf generation" << endl;
			    	// NEED TO SEND mplanner FAILURE MESSAGE
			    }
				cout << "done" << endl;
	
	
				/************************************************************************/    
			    /* SEND DATA TO DPLANNER */
			    
			    cout << "Sending rddf...";

				SendRDDF(rddfSocket, newRddf);
				cout << "done" << endl;
			    cout << "printing rddf..." << endl;
			    // Print to screen
			    newRddf->print();
			    // Print to file
				FILE * pFile;
			   	pFile = fopen ("rddfFile.txt","w");
			   	int numOfWayPts = newRddf->getNumTargetPoints(); 
			   	RDDFVector rddfPoints = newRddf->getTargetPoints();
			   	for (int ii=0; ii<numOfWayPts; ii++)
			   	{
			      	fprintf (pFile, "%i\t%f\t%3.9f\t%3.9f\t%3.9f\t%3.9f\t%f\t%f\t%f\t\n", 
			      		rddfPoints[ii].number, rddfPoints[ii].distFromStart, 
			      		rddfPoints[ii].Northing, rddfPoints[ii].Easting, 
			      		rddfPoints[ii].longitude, rddfPoints[ii].latitude, 
			      		rddfPoints[ii].maxSpeed, rddfPoints[ii].offset, 
			      		rddfPoints[ii].radius);
			   	}
			   	fprintf(pFile, "\n");
			   	fclose (pFile);
				cout << "done" << endl;
	    	} // END: if (numOfSegGoals>0)
	    	
	    	/************************************************************************/    
			/* DECIDE WHEN SEGMENT COMPLETED */
			// FOR NOW COMPARE POSITION WITH EXIT_WAYPT AND VELOCITY SHOULD BE SMALL AS WELL
			// NEED TO WORK ON THIS SOME MORE
			cout << "Checking if segment completed...";
			if ((dist2Exit<1) && ((sqrt(pow(vx_A,2)+pow(vy_A,2)))<1))
			{
				segGoalsStatus->status = COMPLETED;
	    		stayInLoopFlag = false;
	    		cout << "YES!" << endl;
			}
			else
				cout << "not yet" << endl;
		} //WHILE STAY IN LOOP
		
		/************************************************************************/    
	    /* SEND STATUS TO MPLANNER */
      	    cout << "Sending status to mplanner...";
	    segGoalsStatus->goalID = currentSegGoals.goalID;
	    cout << "goal ID = " << segGoalsStatus->goalID << endl;
	    bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
	    if (!statusSent)
			cout << "Error sending status" << endl;
	    else
			cout << "Successfully sent status for goal " << segGoalsStatus->goalID << endl;
		
	}
}



void CTrafficPlanner::elevation2Cost(CMapPlus elevationMap, CMapPlus& costMap)
{
  costMap = elevationMap;
}

ConstraintSet CTrafficPlanner::laneFollowing(vector<Location> laneLBCoord, vector<Location> laneRBCoord, Divider laneLBType, Divider laneRBType, vector<TrafficRules> trafficRules, double dLimit, double dist2Exit, double x_A, double y_A) 
{
	ConstraintSet constraintSet;
 	vector<double> x_RB;
    vector<double> y_RB;
    vector<double> x_LB;
    vector<double> y_LB;
    
    for (unsigned ii = 0; ii < laneLBCoord.size(); ii++)
    {
    	x_LB.push_back(laneLBCoord[ii].x);
    	y_LB.push_back(laneLBCoord[ii].y);
    }
    for (unsigned ii = 0; ii < laneRBCoord.size(); ii++)
    {
    	x_RB.push_back(laneRBCoord[ii].x);
    	y_RB.push_back(laneRBCoord[ii].y);
    }
	
	// find points on boundary closest to me
	int ind_RB_CP, ind_LB_CP, ind_RB_Ins, ind_LB_Ins;
	double x_temp, y_temp;
	double distance = closestBoundaryPt(x_RB,y_RB,x_A,y_A, ind_RB_CP, ind_RB_Ins, x_temp, y_temp);
	// insert these points into boundary
    insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RB_Ins);
    // same for LB
	distance = closestBoundaryPt(x_LB,y_LB,x_A, y_A, ind_LB_CP, ind_LB_Ins, x_temp, y_temp);        
    insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LB_Ins);
    // insert a point a distance away from the point if we are far
    // enough away from the end of the segment.
    if (dLimit < dist2Exit)
    {
    	distanceAlongBoundary(x_RB, y_RB, ind_RB_Ins, dLimit);
    	distanceAlongBoundary(x_LB, y_LB, ind_LB_Ins, dLimit);    	
    }
    bool insert_flag = true;
	bool stay_on_road = false;
	bool traffic_lines = false;	
    // lane right boundary
        switch (laneRBType)
        {
        	case ROAD_EDGE:
            	for (unsigned ii = 0; ii<trafficRules.size(); ii++)
            	{
            		if (trafficRules[ii] == STAY_ON_ROAD)
            		{
	            		stay_on_road = true;
	            		break;
            		}
	            }
	            if (stay_on_road)
	            {
	            	constraintSet.rightBoundaryConstraint = boundaryConstraints(x_RB, y_RB, x_LB, y_LB, x_A, y_A, dLimit, insert_flag);
	            }
	            break;
            case BROKEN_WHITE:
            case SOLID_WHITE:
            case DOUBLE_YELLOW:
    	        for (unsigned ii = 0; ii<trafficRules.size(); ii++)
        	   	{
            		if (trafficRules[ii] == TRAFFIC_LINES)
           			{
	           			traffic_lines = true;
	           			break;
           			}
	            }
            
                if (traffic_lines)
                {
					constraintSet.rightBoundaryConstraint = boundaryConstraints(x_RB, y_RB, x_LB, y_LB, x_A, y_A, dLimit, insert_flag);
                }
                
                break;
            default:
            	cerr << "ERROR -> Wrong Divider type" << endl;
        } // switch
        
        // lane left boundary
    	stay_on_road = false;
		traffic_lines = false;
        switch (laneRBType)
        {
        	case ROAD_EDGE:
            	for (unsigned ii = 0; ii<trafficRules.size(); ii++)
            	{
            		if (trafficRules[ii] == STAY_ON_ROAD)
            		{
	            		stay_on_road = true;
	            		break;
            		}
	            }
	            if (stay_on_road)
	            {
	            	constraintSet.leftBoundaryConstraint = boundaryConstraints(x_LB, y_LB, x_RB, y_RB, x_A, y_A, dLimit, insert_flag);
	            }
	            break;
            case BROKEN_WHITE:
            case SOLID_WHITE:
            case DOUBLE_YELLOW:
    	        for (unsigned ii = 0; ii<trafficRules.size(); ii++)
        	   	{
            		if (trafficRules[ii] == TRAFFIC_LINES)
           			{
	           			traffic_lines = true;
	           			break;
           			}
	            }
                if (traffic_lines)
                {
					constraintSet.leftBoundaryConstraint = boundaryConstraints(x_LB, y_LB, x_RB, y_RB, x_A, y_A, dLimit, insert_flag);
                }
                
                break;
            default:
            	cerr << "ERROR -> Wrong Divider type" << endl;
        } // switch

	return constraintSet;
		
}

vector<TrafficRules> CTrafficPlanner::extractRules(SegmentType segmentType)
{
	vector<TrafficRules> trafficRules;
	switch (segmentType)
	{
		/* {STAY_IN_LANE, PASSING, U_TURN, TRAFFIC_LINES, DRIVE_IN_CORRECT_DIRECTION, STAY_ON_ROAD}*/
		case ROAD_SEGMENT:
			trafficRules.push_back(STAY_IN_LANE);
			trafficRules.push_back(PASSING); 
			trafficRules.push_back(TRAFFIC_LINES);
			trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
			trafficRules.push_back(STAY_ON_ROAD);
			break;
		// NEED TO GO THROUGH THESE AND DEFINE LATER  
	 	case PARKING_ZONE:
			trafficRules.push_back(STAY_IN_LANE);
			trafficRules.push_back(PASSING); 
			trafficRules.push_back(TRAFFIC_LINES);
			trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
			trafficRules.push_back(STAY_ON_ROAD);
			break;  
		case INTERSECTION:
			trafficRules.push_back(STAY_IN_LANE);
			trafficRules.push_back(PASSING); 
			trafficRules.push_back(TRAFFIC_LINES);
			trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
			trafficRules.push_back(STAY_ON_ROAD);
			break;  
		case PREZONE:
			trafficRules.push_back(STAY_IN_LANE);
			trafficRules.push_back(PASSING); 
			trafficRules.push_back(TRAFFIC_LINES);
			trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
			trafficRules.push_back(STAY_ON_ROAD);
			break;  
		case UTURN:
			trafficRules.push_back(STAY_IN_LANE);
			trafficRules.push_back(PASSING); 
			trafficRules.push_back(TRAFFIC_LINES);
			trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
			trafficRules.push_back(STAY_ON_ROAD);
			break;  
		case PAUSE:
			trafficRules.push_back(STAY_IN_LANE);
			trafficRules.push_back(PASSING); 
			trafficRules.push_back(TRAFFIC_LINES);
			trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
			trafficRules.push_back(STAY_ON_ROAD);
			break;  
		case END_OF_MISSION:
			trafficRules.push_back(STAY_IN_LANE);
			trafficRules.push_back(PASSING); 
			trafficRules.push_back(TRAFFIC_LINES);
			trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
			trafficRules.push_back(STAY_ON_ROAD);
			break;  
		default:
			trafficRules.push_back(STAY_IN_LANE);
			trafficRules.push_back(PASSING); 
			trafficRules.push_back(TRAFFIC_LINES);
			trafficRules.push_back(DRIVE_IN_CORRECT_DIRECTION);
			trafficRules.push_back(STAY_ON_ROAD);
	}
	
	// print the traffic rules to the screen to check
	//for (unsigned ii=0;ii<trafficRules.size();ii++)
	//{
	//	cout << "Traffic Rule " << ii << " is " << trafficRules[ii]<<endl;
	//}
	
	return trafficRules;
}

double CTrafficPlanner::closestBoundaryPt(vector<double> xB,vector<double> yB, double x, double y, int& ind_CP, int& ind_Insert, double& x_Pt, double& y_Pt)
{
	// function that finds index of point on boundary (xB,yB) that is closest 
	// to specified point x,y and then calculates the actual closest pt on the boundary
	
	int lengthxB = xB.size();
	double distance = 1e10;
	// closest point on Boundary
	for (int ii = 0; ii<lengthxB; ii++)
	{
	    double distance_temp = sqrt(pow(x-xB[ii],2)+pow(y-yB[ii],2));
	    if (distance_temp<distance)
	    {
	        distance=distance_temp;
	        ind_CP=ii;
	    }
	}
	
	// Do a least-squares line fit to number of points around this point
	// for now use 3 points

	double slope, intersection;	
	if (ind_CP > 0 && ind_CP+1 < lengthxB)
	{
		int sizeOfXToFit = 3;
		double x_toFit[] = {xB[ind_CP-1],xB[ind_CP],xB[ind_CP+1]};
		double y_toFit[] = {yB[ind_CP-1],yB[ind_CP],yB[ind_CP+1]};
//	    cout << "x_toFit[0] = " << x_toFit[0] << endl;
//	    cout << "x_toFit[1] = " << x_toFit[1] << endl;
//	    cout << "x_toFit[2] = " << x_toFit[2] << endl;
//	    cout << "y_toFit[0] = " << y_toFit[0] << endl;
//	    cout << "y_toFit[1] = " << y_toFit[1] << endl;
//	    cout << "y_toFit[2] = " << y_toFit[2] << endl;
	    lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
	}
	else if (ind_CP == 0 && ind_CP+1 < lengthxB)
	{
		int sizeOfXToFit = 2;
		double x_toFit[] = {xB[ind_CP],xB[ind_CP+1]};
		double y_toFit[] = {yB[ind_CP],yB[ind_CP+1]};
	    lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
	}
	else if (ind_CP > 0 && ind_CP+1 == lengthxB)
	{
		int sizeOfXToFit = 2;
		double x_toFit[] = {xB[ind_CP-1],xB[ind_CP]};
		double y_toFit[] = {yB[ind_CP-1],yB[ind_CP]};
	    lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
	}
	else
	{
		x_Pt = xB[ind_CP];
		y_Pt = yB[ind_CP];
		ind_Insert = ind_CP;
		return distance;
	}
	// then find best approximation to the point on the line
	// transform into coordinate frame at begin pt of line x-axis along line
    double theta = atan(slope);
//    cout << "theta = " << theta << endl;
    double R11 = cos(theta);
    double R12 = -sin(theta);
    double R21 = sin(theta);
    double R22 = cos(theta);
//    cout << "R11 = " << R11 << endl;
//    cout << "R12 = " << R12 << endl;
//    cout << "R21 = " << R21 << endl;
//    cout << "R22 = " << R22 << endl;
    double d_vec1 = xB[ind_CP];
    double d_vec2 = yB[ind_CP];
    double posXYPt_local1 = x;
	double posXYPt_local2 = y;    
    double posXYPt_line1 = R11*(posXYPt_local1-d_vec1)+R21*(posXYPt_local2-d_vec2);
    // double posXYPt_line2 = R12*(posXYPt_local1-d_vec1)+R22*(posXYPt_local2-d_vec2); 
    // find projection of point onto x-axis, which is aligned with line
    double posBDPt_line1 = posXYPt_line1;
    double posBDPt_line2 = 0;
//    cout << "posBDPt_line1 = " << posBDPt_line1 << endl; 
    // transform new point back to local frame
    double posBDPt_local1 = d_vec1 + R11*posBDPt_line1 + R12*posBDPt_line2;
    double posBDPt_local2 = d_vec2 + R21*posBDPt_line1 + R22*posBDPt_line2;
    x_Pt = posBDPt_local1;
    y_Pt = posBDPt_local2;
    // if the point lies on the line (pos x-axis), then give index of next point
    if (posBDPt_line1>EPS)
        ind_Insert = ind_CP+1;
    else 
    	ind_Insert = ind_CP;
    distance = sqrt(pow(x_Pt-x,2)+pow(y_Pt-y,2));
    return distance;
}

void CTrafficPlanner::lineFit(double x_toFit[],double y_toFit[],int sizeOfXToFit,double& slope,double& intersection)
{
	// do a least squares fit - assumed only error in y-direction
	double sumX=0, sumX2=0, sumY=0, sumXY=0;
	
//	cout << "lineFit Function" << endl;
//	
//	for (int ii = 0; ii < sizeOfXToFit; ii++)
//	{
//	    cout << "x_toFit[" << ii << "] = " << x_toFit[ii] << endl;
//	    cout << "y_toFit[" << ii << "] = " << y_toFit[ii] << endl;
//	}
	
	for (int ii=0;ii<sizeOfXToFit;ii++)
	{
		sumX += x_toFit[ii];
//		cout << "Loop # " << ii << " sumY = " << sumY << endl;
		sumY += y_toFit[ii];
		sumX2 += x_toFit[ii]*x_toFit[ii];
		sumXY += x_toFit[ii]*y_toFit[ii];		
	}
	
//	cout << "sumX = " << sumX << " sumY = " << sumY << "sumX2 = " << sumX2 << "sumXY = " << sumXY << endl;
	
	intersection = (sumY*sumX2-sumX*sumXY)/(sizeOfXToFit*sumX2-sumX*sumX);
	slope = (sizeOfXToFit*sumXY-sumX*sumY)/(sizeOfXToFit*sumX2-sumX*sumX);
	
//	cout << "Slope of fitted line = " << slope << endl;
//	cout << "Intersection of fitted line = " << intersection << endl;
}

void CTrafficPlanner::insertPointOnBoundary(vector<double>& xB,vector<double>& yB,double x, double y,int index)
{
	double eps = 0.05;
	// check if the points are too close to each other
	if (sqrt(pow(xB[index]-x,2)+pow(yB[index]-y,2)) > eps)
	{
		xB.insert(xB.begin()+index,x);
		yB.insert(yB.begin()+index,y);
	}
}

void CTrafficPlanner::distanceAlongBoundary(vector<double>& xB, vector<double>& yB, int& index, double distance)
{
	double dAlongBoundary = 0;
	int lengthxB = xB.size();
	for (int ii=index; ii+1<lengthxB; ii++)
	{
	    dAlongBoundary += sqrt(pow(xB[ii]-xB[ii+1],2)+pow(yB[ii]-yB[ii+1],2));
	    
	    if (dAlongBoundary>distance)
	    {
	        // find a point on the line segment at distance
			double slope, intersection;	        
	        if (ii > 0)
	        {
				int sizeOfXToFit = 3;
				double x_toFit[] = {xB[ii-1],xB[ii],xB[ii+1]};
				double y_toFit[] = {yB[ii-1],yB[ii],yB[ii+1]};	            
	           	lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
	        }
	        else if (ii == 0)
	        {
				int sizeOfXToFit = 2;
				double x_toFit[] = {xB[ii],xB[ii+1]};
				double y_toFit[] = {yB[ii],yB[ii+1]};	            
	           	lineFit(x_toFit,y_toFit,sizeOfXToFit,slope,intersection);
	        }
	        // in coordinate frame centered at (xB(ii),yB(ii)), with x-axis aligned with the line, specify
	        // distance along x-axis and then transform into local frame
		    double theta = atan(slope);
		    double R11 = cos(theta);
		    double R12 = -sin(theta);
		    double R21 = sin(theta);
		    double R22 = cos(theta);
		    double d_vec1 = xB[ii];
		    double d_vec2 = yB[ii];
		    double posXY_line1 = sqrt(pow(xB[ii]-xB[ii+1],2)+pow(yB[ii]-yB[ii+1],2))-(dAlongBoundary-distance);
			double posXY_line2 = 0;    
		    // transform new point back to local frame
		    double posXY_local1 = d_vec1 + R11*posXY_line1 + R12*posXY_line2;
		    double posXY_local2 = d_vec2 + R21*posXY_line1 + R22*posXY_line2;
	        if (sqrt(pow(xB[ii]-posXY_local1,2)+pow(yB[ii]-posXY_local2,2)) > EPS)
	        {
	            index = ii+1;
	            insertPointOnBoundary(xB, yB, posXY_local1, posXY_local2, index);
	            break;
	        }
	        else
	        {
	            index = ii;
	            break;
	        }
	    }
	}
}

vector<Constraint> CTrafficPlanner::boundaryConstraints(vector<double> xB1, vector<double> yB1, vector<double> xB2, vector<double> yB2, double x, double y, double distance, bool insert_flag)
{
	vector<Constraint> constraints;
		
	// closest point on B1
	double x_temp_B1, y_temp_B1;
	double x_temp_B2, y_temp_B2;	
	int ind_B1_Ins, ind_B2_Ins, ind_B1_CP, ind_B2_CP;
	double dist;
	dist = closestBoundaryPt(xB1, yB1, x, y, ind_B1_CP, ind_B1_Ins, x_temp_B1, y_temp_B1);	
	// closest point on B2
	if (xB2.size()>1)
	{
	    dist = closestBoundaryPt(xB2, yB2, x, y, ind_B1_CP, ind_B2_Ins, x_temp_B2, y_temp_B2);
	}
	else
	{
	    ind_B2_Ins = 1;
	    ind_B2_CP = 1;
	    x_temp_B2 = xB2[ind_B2_CP];
	    y_temp_B2 = yB2[ind_B2_CP];
	}
	// flag that says whether or not I need to insert these points into the
	// boundary so that these points are at position index
	if (insert_flag)
	{
		insertPointOnBoundary(xB1,yB1,x_temp_B1,y_temp_B1,ind_B1_Ins);
		insertPointOnBoundary(xB2,yB2,x_temp_B2,y_temp_B2,ind_B2_Ins);
	}
	
	// Generate constraints for the all the points on the boudary in
	// the next X meters along the boundary
	double dAlongBoundary = 0;
	double slope, intersection;
	for (int ii = ind_B1_Ins; ii+1<(int)xB1.size(); ii++)
	{
	    Constraint constraint;
	    // check that denominator is ~= 0
	    if (xB1[ii+1]-xB1[ii] != 0)
	    {
	        slope=(yB1[ii+1]-yB1[ii])/(xB1[ii+1]-xB1[ii]);
	    }
	    else
	    {
	        slope = ((yB1[ii+1]-yB1[ii])/abs(yB1[ii+1]-yB1[ii]))*BIGNUMBER;
	    }
	    intersection=yB1[ii]-slope*xB1[ii];
	    // Check which side the closest point on the left boundary is
	    // for the line segment on the right boundary closest to you
	    double value = yB2[ind_B2_CP] - slope*xB2[ind_B2_CP] - intersection;
	    if (value > 0)
	    {
	        // I also want to be on this side -> above line
	        // segment, but need to specify as Ax-B<=0
	        constraint.A1 = slope;
	        constraint.A2 = -1; 
	        constraint.B = -intersection;
	    }
	    else if (value < 0)
	    {
	        constraint.A1 = -slope;
	        constraint.A2 = 1;
	        constraint.B = intersection;
	    }
	    else
	    {
	        cerr<<"ERROR in boundary constraint generation function "<<endl;
	    }
	    constraint.xrange1 = xB1[ii];
	    constraint.xrange2 = xB1[ii+1];
	    constraint.yrange1 = yB1[ii];
	    constraint.yrange2 = yB1[ii+1];	
	    constraints.push_back(constraint); 
	    dAlongBoundary += sqrt(pow(xB1[ii]-xB1[ii+1],2)+pow(yB1[ii]-yB1[ii+1],2));
//	    cout << "dAlongBoundary = " << dAlongBoundary << endl;
	    if (abs(dAlongBoundary - distance) < EPS)
	    {
	        return constraints;
	    }
	}
	return constraints;
}

LocalMapObject CTrafficPlanner::spoofLocalMapData(int segmentID, int numLanes, int numTravelLanes, double laneWidth, int senseMin, int senseMax, int xSpacing)
{
	LocalMapObject localMapObject;
	localMapObject.segment.lane.resize(numLanes);
	//double x_orig = 396411.894;
	double x_orig = 396412;
	//double y_orig = 3778141.879;
	double y_orig = 3778142;
	// Specify lane boundaries
	double x_divider, y_divider;
	// define the x values
	Location tmpLoc; 
	for (int ii = senseMin; ii<=senseMax; ii=ii+xSpacing)
	{
		double x_rand1 = (double)rand()/RAND_MAX;
		//double x_rand2 = (double)rand()/RAND_MAX;
		x_divider = static_cast<double>(ii) + x_orig;
		for (int jj = 0; jj<numTravelLanes; jj++)
		{
		    // Right boundary
			y_divider = (numLanes-jj-0.5)*laneWidth + y_orig;
			tmpLoc.x = x_divider - x_rand1; tmpLoc.y = y_divider;
			localMapObject.segment.lane[jj].laneRBCoord.push_back(tmpLoc);
		    // Left boundary
		    y_divider = (numLanes-jj-1.5)*laneWidth + y_orig;
		    tmpLoc.x = x_divider; tmpLoc.y = y_divider;
			localMapObject.segment.lane[jj].laneLBCoord.push_back(tmpLoc);
		}
		for (int jj = numTravelLanes; jj<numLanes; jj++)
		{
		    // Right boundary
			y_divider = (numLanes-jj-1.5)*laneWidth + y_orig;
			tmpLoc.x = x_divider - x_rand1; tmpLoc.y = y_divider;
			localMapObject.segment.lane[jj].laneRBCoord.push_back(tmpLoc);
		    // Left boundary
		    y_divider = (numLanes-jj-0.5)*laneWidth + y_orig;
		    tmpLoc.x = x_divider; tmpLoc.y = y_divider;
			localMapObject.segment.lane[jj].laneLBCoord.push_back(tmpLoc);    
		}
	}
//	localMapObject.segment.lane[numLanes-1].laneLBCoord.resize(localMapObject.segment.lane[0].laneLBCoord.size());
//	localMapObject.segment.lane[numLanes-1].laneRBCoord.resize(localMapObject.segment.lane[0].laneRBCoord.size());	
	// Lane 1 boundary types
	localMapObject.segment.lane[0].laneRBType = ROAD_EDGE;
	localMapObject.segment.lane[0].laneLBType = BROKEN_WHITE;
	// Lane 2 boundary types
	localMapObject.segment.lane[1].laneRBType = BROKEN_WHITE;
	localMapObject.segment.lane[1].laneLBType = DOUBLE_YELLOW;
	// Lane 3 boundary types
	localMapObject.segment.lane[2].laneRBType = BROKEN_WHITE;
	localMapObject.segment.lane[2].laneLBType = DOUBLE_YELLOW;
	// Lane 4 boundary types
	localMapObject.segment.lane[3].laneRBType = ROAD_EDGE;
	localMapObject.segment.lane[3].laneLBType = BROKEN_WHITE;
	
	// Specify a stop line at the end of the road segment
	// place a stop line in Lane 4
	double x_stop[2] = {50+x_orig, 50+x_orig};
	double y_stop[2] = {-2.5 + y_orig, 2.5 + y_orig};
	tmpLoc.x = x_stop[0]; tmpLoc.y = y_stop[0];
	localMapObject.segment.stopLine.coordinates.push_back(tmpLoc);
	tmpLoc.x = x_stop[1]; tmpLoc.y = y_stop[1];
	localMapObject.segment.stopLine.coordinates.push_back(tmpLoc);
		
	// Specify a checkpoint in a road segment
	// Place a chkpt in Lane 1 at x location -5;
//	double x_chkpt = 70+x_orig;
//	double y_chkpt = 0+y_orig;	
//	localMapObject.segment.checkPoint.coordinates.x = x_chkpt;
//	localMapObject.segment.checkPoint.coordinates.y = y_chkpt;
	//cout << "Finished map generation" << endl;
	
	FILE * pFile;

   	pFile = fopen ("myLane.txt","w");
   	for (unsigned jj=0; jj<localMapObject.segment.lane[0].laneRBCoord.size(); jj++)
   	{
   		//cout << "jj = " << jj << endl;
 	  	for (int ii=0 ; ii<numLanes ; ii++)
   		{
   			//cout << "Lane " << ii << endl;
   			double x1 = localMapObject.segment.lane[ii].laneRBCoord[jj].x;
   			double y1 = localMapObject.segment.lane[ii].laneRBCoord[jj].y;
   			double x2 = localMapObject.segment.lane[ii].laneLBCoord[jj].x;
   			double y2 = localMapObject.segment.lane[ii].laneLBCoord[jj].y;
			//cout << "x_RB[" << jj << "] = " << x1 << " and y_RB[" << jj << "] = " << y1 << endl;
      		//cout << "x_LB[" << jj << "] = " << x2 << " and y_LB[" << jj << "] = " << y2 << endl;
      		fprintf (pFile, "%f\t%f\t%f\t%f\t", x1, y1, x2, y2);
   		}
   		fprintf(pFile, "\n");
   }
   fclose (pFile);
   
//   	for (unsigned jj=0; jj<localMapObject.segment.lane[0].laneRBCoord.size(); jj++)
//   	{
//   		//cout << "jj = " << jj << endl;
// 	  	for (int ii=0 ; ii<numLanes ; ii++)
//   		{
//   			//cout << "Lane " << ii << endl;
//   			double xroad = localMapObject.segment.lane[ii].laneRBCoord[jj].x;
//   			double yroad = localMapObject.segment.lane[ii].laneRBCoord[jj].y;
//   			utm.n = yroad;
//			utm.e = xroad;
//			gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
////			cout << "Lat = " << latlon.latitude << endl;
////			cout << "Long = " << latlon.longitude << endl;
//			printf("Lat = %3.9f  Long = %3.9f \n", latlon.latitude, latlon.longitude);
//   		}
//   }
  
//   cout << "Finished map printing" << endl;
	
	return localMapObject;
}

LocalMapObject CTrafficPlanner::spoofLocalMapData2()
{
	LocalMapObject localMapObject;
	localMapObject.segment.lane.resize(1);
	// Specify lane boundaries
	double x_road[8], y_road[8], x_divider, y_divider;
	// define the x values
	// This is based off the rddf captured for the Steele lot - 12-4-06
	// Calculate the slope of the line connecting
	Location tmpLoc; 
	double slope = (3778077.687 - 3778141.879)/(396403.467 - 396411.894);
	cout << "slope = " << slope << endl;
	double theta = atan2(3778077.687 - 3778141.879,396403.467 - 396411.894);
	cout << "Theta = " << theta << endl;
	double d_vec1 = 3778141.879;
	double d_vec2 = 396411.894;	
	double R11 = cos(theta);
    double R12 = -sin(theta);
    double R21 = sin(theta);
    double R22 = cos(theta);
    for (int ii=0; ii<8; ii++)
    {
    	x_road[ii] = d_vec1 + R11*(10*ii) + R12*(0);
    	y_road[ii] = d_vec2 + R21*(10*ii) + R22*(0);
    	x_divider = d_vec1 + R11*(10*ii) + R12*(3);
		y_divider = d_vec2 + R21*(10*ii) + R22*(3);
		tmpLoc.x = x_divider; tmpLoc.y = y_divider;
		localMapObject.segment.lane[0].laneLBCoord.push_back(tmpLoc);
    	x_divider = d_vec1 + R11*(10*ii) + R12*(-3);
		y_divider = d_vec2 + R21*(10*ii) + R22*(-3);
		tmpLoc.x = x_divider; tmpLoc.y = y_divider;
		localMapObject.segment.lane[0].laneRBCoord.push_back(tmpLoc);
    }
    x_road[7] = 396403.467;
	y_road[7] = 3778077.687;
	x_divider = d_vec1 + R11*((396403.467 - 396411.894)/cos(theta)) + R12*(3);
	y_divider = d_vec2 + R21*((396403.467 - 396411.894)/cos(theta)) + R22*(3);
	tmpLoc.x = x_divider; tmpLoc.y = y_divider;
	localMapObject.segment.lane[1].laneLBCoord.push_back(tmpLoc);
	x_divider = d_vec1 + R11*((396403.467 - 396411.894)/cos(theta)) + R12*(-3);
	y_divider = d_vec2 + R21*((396403.467 - 396411.894)/cos(theta)) + R22*(-3);
	tmpLoc.x = x_divider; tmpLoc.y = y_divider;
	localMapObject.segment.lane[0].laneRBCoord.push_back(tmpLoc);	 

	// Lane 1 boundary types
	localMapObject.segment.lane[0].laneRBType = ROAD_EDGE;
	localMapObject.segment.lane[0].laneLBType = DOUBLE_YELLOW;

/*	
	// Specify a stop line at the end of the road segment
	// place a stop line in Lane 4
	double x_stop[2] = {100, 100};
	double y_stop[2] = {-2.5, 2.5};
	tmpLoc.x = x_stop[0]; tmpLoc.y = y_stop[0];
	localMapObject.segment.stopLine.coordinates.push_back(tmpLoc);
	tmpLoc.x = x_stop[1]; tmpLoc.y = y_stop[1];
	localMapObject.segment.stopLine.coordinates.push_back(tmpLoc);
		
	// Specify a checkpoint in a road segment
	// Place a chkpt in Lane 1 at x location -5;
	double x_chkpt = 50;
	double y_chkpt = 0;	
	localMapObject.segment.checkPoint.coordinates.x = x_chkpt;
	localMapObject.segment.checkPoint.coordinates.y = y_chkpt;
	//cout << "Finished map generation" << endl;
*/
	
	FILE * pFile;
	int numLanes = 1;
   	pFile = fopen ("myLane.txt","w");
   	for (unsigned jj=0; jj<localMapObject.segment.lane[0].laneRBCoord.size(); jj++)
   	{
   		//cout << "jj = " << jj << endl;
 	  	for (int ii=0 ; ii<numLanes ; ii++)
   		{
   			//cout << "ii = " << ii << endl;
   			double x1 = localMapObject.segment.lane[ii].laneRBCoord[jj].x;
   			double y1 = localMapObject.segment.lane[ii].laneRBCoord[jj].y;
   			double x2 = localMapObject.segment.lane[ii].laneLBCoord[jj].x;
   			double y2 = localMapObject.segment.lane[ii].laneLBCoord[jj].y;
   			double x3 = x_road[ii];
   			double y3 = y_road[ii];
   			cout << "x_road[" << ii << "] = " << x3 << " and y_road[" << ii << "] = " << y3 << endl;
			cout << "x_RB[" << ii << "] = " << x1 << " and y_RB[" << ii << "] = " << y1 << endl;
      		cout << "x_LB[" << ii << "] = " << x2 << " and y_LB[" << ii << "] = " << y2 << endl;
      		fprintf (pFile, "%f\t%f\t%f\t%f\t%f\t%f\t", x1, y1, x3, y3, x2, y2);
   		}
   		fprintf(pFile, "\n");
   }
   fclose (pFile);
   cout << "Finished map printing" << endl;
	
	return localMapObject;
}

SegGoals CTrafficPlanner::spoofSegmentGoal()
{
	SegGoals segmentGoal;
	segmentGoal.goalID = 1;
  	segmentGoal.globalMapRevisionNumber = 1;
  	segmentGoal.entrySegmentID = 1;
  	segmentGoal.entryLaneID = 1;
  	segmentGoal.entryWaypointID = 1;
  	segmentGoal.exitSegmentID = 1;
  	segmentGoal.exitLaneID = 1;
  	segmentGoal.exitWaypointID = 3;
  	segmentGoal.minSpeedLimit = 0;
  	segmentGoal.maxSpeedLimit = 10;
  	segmentGoal.illegalPassingAllowed = false;
  	segmentGoal.stopAtExit = true;
  	segmentGoal.isExitCheckpoint = false;
  	segmentGoal.perf_level = 0;
  	segmentGoal.segment_type = ROAD_SEGMENT;	

/*  	
  	// print this information to check that is is correct
  	cout << "goalID = " << segmentGoal.goalID << endl;
  	cout << "globalMapRevisionNumber = " << segmentGoal.globalMapRevisionNumber << endl;
  	cout << "entrySegmentID = " << segmentGoal.entrySegmentID << endl;
  	cout << "entryLaneID = " << segmentGoal.entryLaneID << endl;
  	cout << "entryWaypointID = " << segmentGoal.entryWaypointID << endl;
  	cout << "exitSegmentID = " << segmentGoal.exitSegmentID << endl;
  	cout << "exitLaneID = " << segmentGoal.exitLaneID << endl;
  	cout << "exitWaypointID = " << segmentGoal.exitWaypointID << endl;
  	cout << "minSpeedLimit = " << segmentGoal.maxSpeedLimit << endl;
  	cout << "illegalPassingAllowed = " << segmentGoal.illegalPassingAllowed << endl;
  	cout << "stopAtExit = " << segmentGoal.stopAtExit << endl;
  	cout << "isExitCheckpoint = " << segmentGoal.isExitCheckpoint << endl;
  	cout << "perf_level = " << segmentGoal.perf_level << endl;
  	cout << "segment_type = " << segmentGoal.segment_type << endl;
*/
  	return segmentGoal;
}

bool CTrafficPlanner::convRDDF( vector<Constraint> line1, vector<Constraint> line2, double x_A, double y_A, double laneWidth, RDDF* newRddf) 
{  
  int i, j, k, l1, l2 ;

  //%%%%%%%%%%%%%%%%%%%%%%%%
  //% FIND CLOSEST SEGMENTS
  //%%%%%%%%%%%%%%%%%%%%%%%%
  bool error = false ;
  double apos[] = {x_A, y_A};
  int numline1seg = line1.size();
  cout << "numline1seg = " << numline1seg << endl;
  int numline2seg = line2.size();
  cout << "numline2seg = " << numline2seg << endl;
  double l1p[numline1seg+1][2], l2p[numline2seg+1][2] ;
    for( i = 0 ; i < numline1seg ; i++ ) {
    l1p[i][0] = line1[i].xrange1 ;
 //   cout << "line1 [" << i << "] xrange1 = " << l1p[i][0] << endl; 
    l1p[i][1] = line1[i].yrange1 ;
 //   cout << "line1 [" << i << "] yrange1 = " << l1p[i][1] << endl;
  }
  l1p[numline1seg][0] = line1[numline1seg-1].xrange2 ;
  l1p[numline1seg][1] = line1[numline1seg-1].yrange2 ;
 // cout << "line1 [" << numline1seg << "] xrange1 = " << l1p[numline1seg][0] << endl; 
 // cout << "line1 [" << numline1seg << "] yrange1 = " << l1p[numline1seg][1] << endl;
  
  int numline1pts = numline1seg + 1 ;

  for( i = 0; i < numline2seg ; i++ ) {
    l2p[i][0] = line2[i].xrange1 ;
     //  cout << "line2 [" << i << "] xrange1 = " << l2p[i][0] << endl; 
    l2p[i][1] = line2[i].yrange1 ;
   // cout << "line2 [" << i << "] yrange1 = " << l2p[i][1] << endl;
  }
  l2p[numline2seg][0] = line2[numline2seg-1].xrange2 ;
  l2p[numline2seg][1] = line2[numline2seg-1].yrange2 ;
  // cout << "line2 [" << numline2seg << "] xrange1 = " << l2p[numline2seg][0] << endl; 
 // cout << "line2 [" << numline2seg << "] yrange1 = " << l2p[numline2seg][1] << endl;
  int numline2pts = numline2seg + 1 ;

// Check that end points of two lines are not the same (full lane obstacle)
  if( (l1p[numline1seg][0] == l2p[numline2seg][0]) && 
      (l1p[numline1seg][1] == l2p[numline2seg][1]) ){
    error = true ;
  }

  double mpt[2] = {0, 0} ;
  double r = 0 ;

// Line 1
  double min1, min2, dist, xdiff, ydiff ;
  int min1num, min2num;
  bool skip = false ;
  double a[2], b[2], maga, magb, adotb, th, check[4], check1[4], check2[4] ;
  point cpt_, cpt1_, cpt2_, mpt_ ;
  double cpt[2], pnt[2] ;
  double rddfln1[numline1pts][3], rddfln2[numline2pts][3] ;
  for( i = 0 ; i < numline1pts ; i++ ) {
    min1 = BIGNUMBER ;
    min2 = BIGNUMBER ;
    min1num = 0 ;
    min2num = 0 ;
    for( j = 0; j < numline2pts; j++ ) {
        xdiff = l1p[i][0] - l2p[j][0] ;
        ydiff = l1p[i][1] - l2p[j][1] ;
        dist = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
        if( dist < min1 ) {
            if( dist < min2 ) {
                min1 = min2 ;
                min1num = min2num ;
                min2 = dist ;
                min2num = j ;
            } else {
                min1 = dist ;
                min1num = j ;
            }
        }
    }
    skip = false ;
    if( (i == 0) || (i == (numline1pts-1))) {
       if( i == 0 ) {
           a[0] = l1p[0][0] - l2p[0][0] ;
           a[1] = l1p[0][1] - l2p[0][1] ;
           b[0] = l2p[1][0] - l2p[0][0] ;
           b[1] = l2p[1][1] - l2p[0][1] ;
           maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
           magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
           adotb = a[0] * b[0] + a[1] * b[1] ;
       } else {
           a[0] = l1p[numline1seg][0] - l2p[numline2seg][0] ;
           a[1] = l1p[numline1seg][1] - l2p[numline2seg][1] ;
           b[0] = l2p[numline2seg-1][0] - l2p[numline2seg][0] ;
           b[1] = l2p[numline2seg-1][1] - l2p[numline2seg][1] ;
           maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
           magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
           adotb = a[0] * b[0] + a[1] * b[1] ;
       }
       th = acos( adotb/(maga * magb) ) ;
       if( th > (PI / 2 + EPS) ) 
           skip = true ;
    }
    if( skip ) {
        mpt[0] = 0 ;
        mpt[1] = 0 ;
        r = 0 ;
    } else {
        pnt[0] = l1p[i][0] ;
        pnt[1] = l1p[i][1] ;
        if( (min1num + 1 == min2num) || (min2num + 1 == min1num)) {
        	//fprintf( stderr, "min1num = %i, minnum2 = %i\n" , min1num, min2num) ;
            if( min1num < min2num ) {
                for( k = 0 ; k < 4 ; k++ ) {
                  if( k < 2 )
                    check[k] = l2p[min1num][k] ;
                  else
                    check[k] = l2p[min1num+1][k-2];
                }
            } else {
                for( k = 0 ; k < 4 ; k++ ) {
                  if( k < 2 ) 
                    check[k] = l2p[min2num][k] ;
                  else
                    check[k] = l2p[min2num+1][k-2] ;
                }
            }
            cpt_ = closept( check, pnt ) ;
            cpt[0] = cpt_.x ;
            cpt[1] = cpt_.y ;
            mpt_ = midpt( cpt, pnt ) ;
            mpt[0] = mpt_.x ;
            mpt[1] = mpt_.y ;
            r = radius( mpt, pnt, cpt ) ;
        }  else if( (min1num + 2 == min2num) || (min2num + 2 == min1num)) {
            if( min1num < min2num ) {
              for( k = 0 ; k < 4 ; k++ ) {
                if ( k < 2 ) {
                  check1[k] = l2p[min1num][k] ;
                  check2[k] = l2p[min2num-1][k] ;
                } else {
                  check1[k] = l2p[min1num+1][k-2] ;
                  check2[k] = l2p[min2num][k-2] ;
                }
              }
            } else { 
              for( k = 0 ; k < 4 ; k++ ) {
                if( k < 2 ) {
                  check1[k] = l2p[min2num][k] ;
                  check2[k] = l2p[min1num-1][k] ;
                } else {
                  check1[k] = l2p[min2num+1][k-2] ;
                  check2[k] = l2p[min1num][k-2] ;
                }
              }
            }
            cpt1_ = closept(check1, pnt) ;
            cpt2_ = closept(check2, pnt) ;
            double xdiff1 = cpt1_.x - pnt[0] ;
            double ydiff1 = cpt1_.y - pnt[1] ;
            double xdiff2 = cpt2_.x - pnt[0];
            double ydiff2 = cpt2_.y - pnt[1] ;
            double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
            double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
            if( dist1 < dist2 ) {
                cpt[0] = cpt1_.x ;
                cpt[1] = cpt1_.y ;
            } else {
                cpt[0] = cpt2_.x ;
                cpt[1] = cpt2_.y ;
            }
            mpt_ = midpt( cpt, pnt ) ;
            mpt[0] = mpt_.x ;
            mpt[1] = mpt_.y ;
            r = radius( mpt, pnt, cpt ) ;
        } else {
            if( min1num < (numline2pts-1) ) {
              for( k = 0 ; k < 4 ; k++ ) {
                if( k < 2 ) 
                  check1[k] = l2p[min1num][k] ;
                else
                  check1[k] = l2p[min1num+1][k-2] ;
              }
            } else {
              for( k = 0 ; k < 4 ; k++ ) {
                if( k < 2 )
                  check1[k] = l2p[min1num-1][k] ;
                else 
                  check1[k] = l2p[min1num][k-2] ;
              }
            }
            if( min2num < (numline2pts-1) ) {
              for( k = 0 ; k < 4 ; k++ ) {
                if( k < 2 ) 
                  check2[k] = l2p[min2num][k] ;
                else
                  check2[k] = l2p[min2num+1][k-2] ;
              }
            } else {
              for( k = 0 ; k < 4 ; k++ ) {
                if( k < 2 )
                  check2[k] = l2p[min2num-1][k] ;
                else 
                  check2[k] = l2p[min2num][k-2] ;
              }
            }
            cpt1_ = closept(check1, pnt) ;
            cpt2_ = closept(check2, pnt) ;
            double xdiff1 = cpt1_.x - pnt[0] ;
            double ydiff1 = cpt1_.y - pnt[1] ;
            double xdiff2 = cpt2_.x - pnt[0];
            double ydiff2 = cpt2_.y - pnt[1] ;
            double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
            double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
            if( dist1 < dist2 ){
                cpt[0] = cpt1_.x ;
                cpt[1] = cpt1_.y ;
            } else {
                cpt[0] = cpt2_.x ;
                cpt[1] = cpt2_.y ;
            }
            mpt_ = midpt( cpt, pnt ) ;
            mpt[0] = mpt_.x ;
            mpt[1] = mpt_.y ;
            r = radius( mpt, pnt, cpt ) ;
        }
    }
    if( r > laneWidth / 2 )
      r = laneWidth / 2 ;

    rddfln1[i][0] = mpt[0] ;
    rddfln1[i][1] = mpt[1] ;
    rddfln1[i][2] = r ;
    //printf("rddfln1 x[%d] = %f\n", i, rddfln1[i][0]);
  }

// Line 2
  for( i = 0 ; i < numline2pts ; i++ ) {
    min1 = BIGNUMBER ;
    min2 = BIGNUMBER ;
    min1num = 0 ;
    min2num = 0 ;
    for( j = 0 ; j < numline1pts ; j++ ) {
        xdiff = l2p[i][0] - l1p[j][0] ;
        ydiff = l2p[i][1] - l1p[j][1] ;
        dist = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
        if( dist < min1 ) {
            if( dist < min2 ) {
                min1 = min2 ;
                min1num = min2num ;
                min2 = dist ;
                min2num = j ;
            } else {
                min1 = dist ;
                min1num = j ;
            }
        }
    }
    skip = false ;
    if( (i == 0) || (i == numline2seg) ) {
       if( i == 0 ) { 
           a[0] = l2p[0][0] - l1p[0][0] ;
           a[1] = l2p[0][1] - l1p[0][1] ;
           b[0] = l1p[1][0] - l1p[0][0] ;
           b[1] = l1p[1][1] - l1p[0][1] ;
           maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
           magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
           adotb = a[0] * b[0] + a[1] * b[1] ;
       } else {
           a[0] = l2p[numline2pts-1][0] - l1p[numline1pts-1][0] ;
           a[1] = l2p[numline2pts-1][1] - l1p[numline1pts-1][1] ;
           b[0] = l1p[numline1pts-2][0] - l1p[numline1pts-1][0] ;
           b[1] = l1p[numline1pts-2][1] - l1p[numline1pts-1][1] ;
           maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
           magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
           adotb = a[0] * b[0] + a[1] * b[1] ;
       }
       th = acos( adotb/(maga * magb) ) ;
       if( th > (PI / 2 + EPS) ) 
           skip = true ;

    }
    if( skip ) {
        mpt[0] = 0 ;
        mpt[1] = 0 ;
        r = 0 ;
    } else {
        pnt[0] = l2p[i][0] ;
        pnt[1] = l2p[i][1] ;
    //    fprintf( stderr, "min1 = %i, min2 = %i\n", min1num, min2num ) ;
        if( (min1num + 1 == min2num) || (min2num + 1 == min1num) ) {
            if( min1num < min2num ) {
              for( k = 0 ; k < 4 ; k++ ){
                  if( k < 2 )
                    check[k] = l1p[min1num][k] ;
                  else
                    check[k] = l1p[min1num+1][k-2];
                }
            } else {
              for( k = 0 ; k < 4 ; k++ ) {
                  if( k < 2 )
                    check[k] = l1p[min2num][k] ;
                  else
                    check[k] = l1p[min2num+1][k-2];
                }
            }
            cpt_ = closept( check, pnt ) ;
            cpt[0] = cpt_.x ;
            cpt[1] = cpt_.y ;
     //       fprintf( stderr, "1: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
            mpt_ = midpt( cpt, pnt ) ;
            mpt[0] = mpt_.x ;
            mpt[1] = mpt_.y ;
            r = radius( mpt, pnt, cpt ) ;
        } else if( (min1num + 2 == min2num) || (min2num + 2 == min1num)) {
            if( min1num < min2num) {
              for( k = 0 ; k < 4 ; k++ ) {
                if ( k < 2 ) {
                  check1[k] = l1p[min1num][k] ;
                  check2[k] = l1p[min2num-1][k] ;
                } else {
                  check1[k] = l1p[min1num+1][k-2] ;
                  check2[k] = l1p[min2num][k-2] ;
                }
              }
            } else {
              for( k = 0 ; k < 4 ; k++ ) {
                if ( k < 2 ) {
                  check1[k] = l1p[min2num][k] ;
                  check2[k] = l1p[min1num-1][k] ;
                } else {
                  check1[k] = l1p[min2num+1][k-2] ;
                  check2[k] = l1p[min1num][k-2] ;
                }
              }
            }
            cpt1_ = closept(check1, pnt) ;
            cpt2_ = closept(check2, pnt) ;
            double xdiff1 = cpt1_.x - pnt[0] ;
            double ydiff1 = cpt1_.y - pnt[1] ;
            double xdiff2 = cpt2_.x - pnt[0];
            double ydiff2 = cpt2_.y - pnt[1] ;
            double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
            double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
            if( dist1 < dist2 ) {
                cpt[0] = cpt1_.x ;
                cpt[1] = cpt1_.y ;
            } else {
                cpt[0] = cpt2_.x ;
                cpt[1] = cpt2_.y ;
            }
     //       fprintf( stderr, "2: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
            mpt_ = midpt( cpt, pnt ) ;
            mpt[0] = mpt_.x ;
            mpt[1] = mpt_.y ;
            r = radius( mpt, pnt, cpt ) ;
        } else {
            if( min1num < numline1seg ) {
              for( k = 0 ; k < 4 ; k++ ){
                if( k < 2 ) 
                  check1[k] = l1p[min1num][k] ;
                else
                  check1[k] = l1p[min1num+1][k-2] ;
              }
            } else {
              for( k = 0 ; k < 4 ; k++ ) {
                if( k < 2 ) 
                  check1[k] = l1p[min1num-1][k] ;
                else
                  check1[k] = l1p[min1num][k-2] ;
              }
            }
            if( min2num < numline1seg ) {
              for( k = 0 ; k < 4 ; k++ ) {
                if( k < 2 ) 
                  check2[k] = l1p[min2num][k] ;
                else
                  check2[k] = l1p[min2num+1][k-2] ;
              }
            } else {
              for( k = 0 ; k < 4 ; k++ ) {
                if( k < 2 ) 
                  check2[k] = l1p[min2num-1][k] ;
                else
                  check2[k] = l1p[min2num][k-2] ;
              }
            }
            cpt1_ = closept(check1, pnt) ;
            cpt2_ = closept(check2, pnt) ;
            double xdiff1 = cpt1_.x - pnt[0] ;
            double ydiff1 = cpt1_.y - pnt[1] ;
            double xdiff2 = cpt2_.x - pnt[0];
            double ydiff2 = cpt2_.y - pnt[1] ;
            double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
            double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
            if( dist1 < dist2 ) {
                cpt[0] = cpt1_.x ;
                cpt[1] = cpt1_.y ;
            } else {
                cpt[0] = cpt2_.x ;
                cpt[1] = cpt2_.y ;
            }
     //       fprintf( stderr, "3: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
            mpt_ = midpt( cpt, pnt ) ;
            mpt[0] = mpt_.x ;
            mpt[1] = mpt_.y ;
            r = radius( mpt, pnt, cpt ) ;
        }
    }
    if( r > laneWidth / 2)
        r = laneWidth / 2 ;

    rddfln2[i][0] = mpt[0] ;
    rddfln2[i][1] = mpt[1] ;
    rddfln2[i][2] = r ;
  //  printf("rddfln2 x[%d] = %f\n", i, rddfln2[i][0]);
  }

//%%%%%%%%%%%%%%%%%%%%%%%%
//% SORT RDDF POINTS
//%%%%%%%%%%%%%%%%%%%%%%%%
  l1 = 0 ;
  l2 = 0 ;
  int count = 0 ;
  double xdiff1, ydiff1, dist1, xdiff2, ydiff2, dist2 ;
  int nlp = numline1pts + numline2pts ;
  double rddf[nlp][3] ;
  while( (l1 < numline1pts) || (l2 < numline2pts) ) 
  {
    if( (rddfln1[l1][0] == 0) && (rddfln1[l1][1] == 0) && (rddfln1[l1][2] == 0))
        l1++;
    if( (rddfln2[l2][0] == 0) && (rddfln2[l2][1] == 0) && (rddfln2[l2][2] == 0))
        l2++ ;
    if( l1 < numline1pts ) 
    {
      xdiff1 = rddfln1[l1][0] - apos[0] ;
      ydiff1 = rddfln1[l1][1] - apos[1] ;
      dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
    } else
        dist1 = -1 ;

    if( l2 < numline2pts ) 
    {
        xdiff2 = rddfln2[l2][0] - apos[0] ;
        ydiff2 = rddfln2[l2][1] - apos[1] ;
        dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
    } else
        dist2 = -1 ;

    if( dist1 < dist2 || dist2 < 0 ) 
    {
        rddf[count][0] = rddfln1[l1][0];
        rddf[count][1] = rddfln1[l1][1];
        rddf[count][2] = rddfln1[l1][2];
        l1++ ;
        count++ ;
    } else 
    {
        rddf[count][0] = rddfln2[l2][0];
        rddf[count][1] = rddfln2[l2][1];
        rddf[count][2] = rddfln2[l2][2];
        l2++ ;
        count++ ;
    }
  } 
  
	cout << "RDDF points count = " << count << endl;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//% convert into format to be sent to dplanner
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	RDDFData rddfData;
	for (int ii = 0; ii < count; ii++)
	{
	// convert to rddf format
	utm.n = rddf[ii][1];
	utm.e = rddf[ii][0];
	gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
	
	rddfData.number = ii+1;
	rddfData.Northing = rddf[ii][1];
	rddfData.Easting = rddf[ii][0];
	rddfData.maxSpeed = 10;
	rddfData.offset = rddf[ii][2];
	rddfData.radius = rddf[ii][2];
	rddfData.latitude = latlon.latitude;
	rddfData.longitude = latlon.longitude;

	newRddf->addDataPoint(rddfData);

	}  

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//% CHECK FOR CORRIDOR FEASIBILITY
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for( i = 0 ; i < count ; i++ ) {
  	//cout << "radius [" << i << "] = " << rddf[i][2] << endl;
    if( rddf[i][2] < ALICEWIDTH / 2 )
        error = true ;
  }

  return(error) ;
}

point CTrafficPlanner::closept( double lnseg[4], double pt[2] ) 
{ 
  double min[2], max[2], a[2], b1[2], b2[2] ;
  double maga, magb, adotb1, adotb2, minth, maxth, distmin, distln, distmax ;

  min[0] = lnseg[0] ;
  min[1] = lnseg[1] ;
  max[0] = lnseg[2] ;
  max[1] = lnseg[3] ;

  a[0] = pt[0] - min[0] ;
  a[1] = pt[1] - min[1] ;
  b1[0] = max[0] - min[0] ;
  b1[1] = max[1] - min[1] ;
  maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
  magb = sqrt( b1[0] * b1[0] + b1[1] * b1[1] ) ;
  adotb1 = a[0] * b1[0] + a[1] * b1[1] ;
  minth = acos(adotb1/(maga*magb)) ;
  distmin = maga ;
  distln = magb ;

  a[0] = pt[0] - max[0] ;
  a[1] = pt[1] - max[1] ;
  b2[0] = min[0] - max[0] ;
  b2[1] = min[1] - max[1] ;
  maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
  adotb2 = a[0] * b2[0] + a[1] * b2[1] ;
  maxth = acos(adotb2/(maga*magb)) ;
  distmax = maga ;

  double mdist1, a1, mdist2, a2, check, apt1[2], apt2[2] ;
  point cpt ;
  if( (minth <= PI/2) && (maxth <= PI/2 ) && (minth > 0) && (maxth > 0)) {
    mdist1 = distmin * sin(minth) ;
    a1 = mdist1 / tan(minth) ;
    mdist2 = distmax * sin(maxth) ;
    a2 = mdist2 / tan(maxth) ;
    check = abs( a1 + a2 - distln ) ;
    apt1[0] = ( a1 / distln ) * b1[0] + min[0] ;
    apt1[1] = ( a1 / distln ) * b1[1] + min[1] ;
    apt2[0] = ( a2 / distln ) * b2[0] + max[0] ;
    apt2[1] = ( a2 / distln ) * b2[1] + max[1] ;
    if( check < 0.1 ){ 
        cpt.x = ( apt1[0] + apt2[0] ) / 2 ;
        cpt.y = ( apt1[1] + apt2[1] ) / 2 ;
    } else {
        if( mdist1 < mdist2 ) {
            cpt.x = apt1[0] ;
            cpt.y = apt1[1] ;
        } else { 
            cpt.x = apt2[0] ;
            cpt.y = apt2[1] ;
        }
    }
  } else {
    if( distmin < distmax ) {
        cpt.x = min[0] ;
        cpt.y = min[1] ;
    } else {
        cpt.x = max[0] ;
        cpt.y = max[1] ;
    }
  }
  return(cpt) ;
}

point CTrafficPlanner::midpt( double pt1[2], double pt2[2] ) 
{ 
  point mpt ;

  mpt.x = (pt1[0] + pt2[0]) / 2 ;
  mpt.y = (pt1[1] + pt2[1]) / 2 ;

  return(mpt) ;
}

double CTrafficPlanner::radius( double mpt[2], double pt1[2], double pt2[2] ) 
{
  double xdiff, ydiff, dist1, dist2, r ;

  xdiff = (mpt[0] - pt1[0]) ;
  ydiff = (mpt[1] - pt1[1]) ;
  dist1 = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
  xdiff = (mpt[0] - pt2[0]) ;
  ydiff = (mpt[1] - pt2[1]) ;
  dist2 = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
  if( dist1 <= dist2 )
    r = dist1 ;
  else
    r = dist2 ;

  return(r) ;
}


