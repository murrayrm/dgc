#include "Lane.hh"
#include "Waypoint.hh"
#include <string>
#include <iostream>
using namespace std;

Lane::Lane(int m, int n, int numOfWaypoints)
{
  this->segmentID = m;
  this->laneID = n;
  this->numOfWaypoints = numOfWaypoints;
}

Lane::~Lane()
{
  for(unsigned i = 0; i < waypoints.size(); i++)
    delete waypoints[i];
}

int Lane::getSegmentID()
{
  return this->segmentID;
}

int Lane::getLaneID()
{
  return this->laneID;
}

int Lane::getNumOfWaypoints()
{
  return this->numOfWaypoints;
}

int Lane::getLaneWidth()
{
  return this->laneWidth;
}

string Lane::getLeftBoundary()
{
  return this->leftBoundary;
}

string Lane::getRightBoundary()
{
  return this->rightBoundary;
}

void Lane::setLaneWidth(int laneWidth)
{
  this->laneWidth = laneWidth;
}

void Lane::setLeftBoundary(string leftBoundary)
{
  this->leftBoundary = leftBoundary;
}

void Lane::setRightBoundary(string rightBoundary)
{
  this->rightBoundary = rightBoundary;
}

Waypoint* Lane::getWaypoint(int waypointID)
{
  if(waypointID <= this->numOfWaypoints && waypointID > 0)
    return this->waypoints[waypointID - 1];
  else
    return NULL;
}

void Lane::addWaypoint(Waypoint* waypoint)
{
  if(waypoint->getSegmentID() == segmentID
    && waypoint->getLaneID() == laneID
    && waypoint->getWaypointID() > 0 
    && waypoint->getWaypointID() <= numOfWaypoints)
      this->waypoints.push_back(waypoint);
}
