#include "RNDF.hh"

RNDF::RNDF()
{
  this->numOfSegments = this->numOfZones = 0;
}

RNDF::~RNDF()
{
  unsigned i = 0;
  
  for(i = 0; i < segments.size(); i++)
    delete segments[i];
    
  for(i = 0; i < zones.size(); i++)
    delete zones[i];
}

Segment* RNDF::getSegment(int segmentID)
{
  if(segmentID <= this->numOfSegments && segmentID > 0)
    return this->segments[segmentID - 1];
  else
    return NULL;
}

Lane* RNDF::getLane(int segmentID, int laneID)
{
  if(segmentID <= this->numOfSegments && segmentID > 0)
    return getSegment(segmentID)->getLane(laneID);
  else
    return NULL;
}

Waypoint* RNDF::getWaypoint(int segmentOrZoneID, int laneID, int waypointID)
{
  if(segmentOrZoneID <= this->numOfSegments && segmentOrZoneID > 0)
    return getLane(segmentOrZoneID, laneID)->getWaypoint(waypointID);
  else if(segmentOrZoneID <= this->numOfSegments + this->numOfZones 
    && segmentOrZoneID > this->numOfSegments && laneID != 0)
    return getSpot(segmentOrZoneID, laneID)->getWaypoint(waypointID);
  else
    return NULL;
}

Zone* RNDF::getZone(int zoneID)
{
  if(zoneID <= this->numOfSegments + this->numOfZones && zoneID > this->numOfSegments)
    return this->zones[zoneID - this->numOfSegments - 1];
  else
    return NULL;
}

PerimeterPoint* RNDF::getPerimeterPoint(int zoneID, int perimeterPointID)
{
  if(zoneID <= this->numOfSegments + this->numOfZones && zoneID > this->numOfSegments)
    return getZone(zoneID)->getPerimeterPoint(perimeterPointID);
  else
    return NULL;
}

ParkingSpot* RNDF::getSpot(int zoneID, int spotID)
{
  if(zoneID <= this->numOfSegments + this->numOfZones && zoneID > this->numOfSegments)
    return getZone(zoneID)->getParkingSpot(spotID);
  else
    return NULL;
}

int RNDF::getNumOfSegments()
{
  return this->numOfSegments;
}

int RNDF::getNumOfZones()
{
  return this->numOfZones;
}

vector<int> RNDF::getMission()
{
  return checkpoints;
}

void RNDF::addSegment(int segmentID, int numOfLanes)
{
  if(segmentID <= this->numOfSegments && segmentID > 0)
  {
    this->segments.push_back(new Segment(segmentID, numOfLanes));
    
    cout << "Segment " << segmentID << " with " << numOfLanes << " lanes." << endl;
  }
}

void RNDF::setSegmentName(int segmentID, string segmentName)
{
  getSegment(segmentID)->setSegmentName(segmentName);
}

void RNDF::setSpeedLimits(int segOrZoneID, int minSpeed, int maxSpeed)
{
  if(segOrZoneID <= this->numOfSegments + this->numOfZones && segOrZoneID > this->numOfSegments)
  {
    Zone* zone = getZone(segOrZoneID);
    zone->setSpeedLimits(minSpeed, maxSpeed);
    
    cout << "The minimum speed of zone " << segOrZoneID << " is " << minSpeed << " mph, the maximum speed is " << maxSpeed << " mph." << endl;
  }
  else
  {
    Segment* segment = getSegment(segOrZoneID);
    segment->setSpeedLimits(minSpeed, maxSpeed);

    cout << "The minimum speed of segment " << segOrZoneID << " is " << minSpeed << " mph, the maximum speed is " << maxSpeed << " mph." << endl;
  }  
}

void RNDF::addLane(int segmentID, int laneID, int numOfWaypoints)
{
  Segment* parentSegment = getSegment(segmentID);
  
  if(parentSegment != NULL)
  {
    parentSegment->addLane(new Lane(segmentID, laneID, numOfWaypoints));
    
    cout << "  Lane " << segmentID << "." << laneID << " with " << numOfWaypoints << " waypoints." << endl;
  }
}

void RNDF::addWaypoint(int segmentID, int laneID, int waypointID, double lat, double lon)
{
  Lane* parentLane = getLane(segmentID, laneID);
  
  if(parentLane != NULL)
  {
    parentLane->addWaypoint(new Waypoint(segmentID, laneID, waypointID, lat, lon));
    
    cout << setprecision(9);
    
    cout << "    Waypoint " << segmentID << "." << laneID << "." <<  waypointID << "@" << "(" << lat << "," << lon << ")" << endl;
  }
}

void RNDF::setLaneWidth(int segmentID, int laneID, int laneWidth)
{
  Lane* parentLane = getLane(segmentID, laneID);
  
  if(parentLane != NULL)
  {
    parentLane->setLaneWidth(laneWidth);
    cout << "  Lane width is " << laneWidth << " ft." << endl;
  }
}

void RNDF::setLeftBoundary(int segmentID, int laneID, string leftBoundary)
{
  Lane* parentLane = getLane(segmentID, laneID);
  
  if(parentLane != NULL)
  {
    parentLane->setLeftBoundary(leftBoundary);
    cout << "  Left boundary is a " << leftBoundary << "." << endl;
  }
}

void RNDF::setRightBoundary(int segmentID, int laneID, string rightBoundary)
{
  Lane* parentLane = getLane(segmentID, laneID);
  
  if(parentLane != NULL)
  {
    parentLane->setRightBoundary(rightBoundary);
    cout << "  Right boundary is a " << rightBoundary << "." << endl;
  }
}

void RNDF::setCheckpoint(int segmentID, int laneID, int waypointID, int checkpointID)
{
  Waypoint* waypoint = getWaypoint(segmentID, laneID, waypointID);
  
  if(waypoint != NULL)
  {
    waypoint->setCheckpointID(checkpointID);
    cout << "    Waypoint " << segmentID << "." << laneID << "." << waypointID << " is checkpoint " << checkpointID << "." << endl;
  }
}

void RNDF::setStopSign(int segmentID, int laneID, int waypointID)
{
  Waypoint* waypoint = getWaypoint(segmentID, laneID, waypointID);
  
  if(waypoint != NULL)
  {
    waypoint->setStopSign();
    cout << "    Waypoint " << segmentID << "." << laneID << "." << waypointID << " has a stop sign." << endl;
  }
}

void RNDF::setExit(int entrySegmentID, int entryLaneID, int entryWaypointID, int exitSegmentID, int exitLaneID, int exitWaypointID)
{
  GPSPoint* entryWaypoint;
  GPSPoint* exitWaypoint;
  
  if(entryLaneID == 0)
    entryWaypoint = getPerimeterPoint(entrySegmentID, entryWaypointID);
  else
    entryWaypoint = getWaypoint(entrySegmentID, entryLaneID, entryWaypointID);
    
  if(exitLaneID == 0)
    exitWaypoint = getPerimeterPoint(exitSegmentID, exitWaypointID);
  else
    exitWaypoint = getWaypoint(exitSegmentID, exitLaneID, exitWaypointID);
    
  if(entryWaypoint != NULL && exitWaypoint != NULL)
  {
    entryWaypoint->setEntry();
    exitWaypoint->setExit(entryWaypoint);
    
    cout << "Waypoint " << exitSegmentID << "." << exitLaneID << "." << exitWaypointID << " is an exit waypoint and has entry waypoint " << entrySegmentID << "." << entryLaneID << "." << entryWaypointID << endl;
  }
}

void RNDF::addZone(int zoneID, int numOfSpots)
{
  if(zoneID <= this->numOfSegments + this->numOfZones && zoneID > this->numOfSegments)
  {
    this->zones.push_back(new Zone(zoneID, numOfSpots));
    
    cout << "Zone " << zoneID << " with " << numOfSpots << " parking spots." << endl;
  }
}

void RNDF::setZoneName(int zoneID, string zoneName)
{
  getZone(zoneID)->setZoneName(zoneName);
}

void RNDF::addPerimeterPoint(int zoneID, int perimeterPointID, double lat, double lon)
{
  Zone* parentZone = getZone(zoneID);
  
  if(parentZone != NULL)
  {
    parentZone->addPerimeterPoint(new PerimeterPoint(zoneID, 0, perimeterPointID, lat, lon));
    
    cout << "  Perimeterpoint " << zoneID << ".0." << perimeterPointID <<  "@(" << lat << "," << lon << ")." << endl;
  }
}

void RNDF::addSpot(int zoneID, int spotID)
{
  Zone* parentZone = getZone(zoneID);
  
  if(parentZone != NULL)
  {
    parentZone->addParkingSpot(new ParkingSpot(zoneID, spotID));
    
    cout << "  Parking spot " << zoneID << "." << spotID << endl;
  }
}  

void RNDF::setSpotWidth(int zoneID, int spotID, int spotWidth)
{
  ParkingSpot* parentSpot = getSpot(zoneID, spotID);
  
  if(parentSpot != NULL)
  {
    parentSpot->setSpotWidth(spotWidth);
    cout << "  Parking spot width is " << spotWidth << " ft." << endl;
  }
}

void RNDF::addSpotWaypoint(int zoneID, int spotID, int waypointID, double lat, double lon)
{
  ParkingSpot* parentSpot = getSpot(zoneID, spotID);
  
  if(parentSpot != NULL)
  {
    parentSpot->setWaypoint(waypointID, new Waypoint(zoneID, spotID, waypointID, lat, lon));
    
    cout << "    Parking spot waypoint " << zoneID << "." << spotID << "." << waypointID << "@(" << lat << "," << lon << ")" << endl;
  }
}

bool RNDF::loadFile(char* fileName)
{
  int i;
  ifstream file;
  string line;
  string word;
  
  file.open(fileName, ios::in);

  if(!file)
  {
    cout << "Error: " << fileName << " file not found." << endl;
    return false;
  } 
  
  getline(file, line);
  
  istringstream lineStream(line, ios::in);
  
  lineStream >> word;
  
  if(word == "RNDF_name")
  {
    setSegmentsAndZones(&file);
    
    for(i = 0; i < this->numOfSegments; i++)
      parseSegment(&file);
      
    for(i = 0; i < this->numOfZones; i++)
      parseZone(&file);
      
    file.close();
    file.open(fileName, ios::in);
      
    parseExit(&file);
    
    return true;
  }
  else if(word == "MDF_name")
  {
    parseCheckpoint(&file);
    
    parseSpeedLimit(&file);
    
    return true;
  }
  else
    return false;
}

void RNDF::parseSegment(ifstream* file)
{
  int segmentID, numOfLanes;
  string segmentName, line, word;
  char letter;
  
  while(word != "end_segment")
  {
    letter = file->peek();
    
    if(letter == 'l')
    {
        parseLane(file, segmentID);
        
        continue;
    }
  
    getline(*file, line);
      
    istringstream lineStream(line, ios::in);

    lineStream >> word;
          
    if(word != "segment" && word != "num_lanes" && word != "segment_name")
      continue;
    else if(word == "segment")
      lineStream >> segmentID;
    else if(word == "num_lanes")
    {
      lineStream >> numOfLanes;
      
      addSegment(segmentID, numOfLanes);
    }
    else if(word == "segment_name")
    {
      string segmentName;
      
      lineStream >> segmentName;
      
      setSegmentName(segmentID, segmentName);
    }
    else
      continue;
  }
}

void RNDF::parseLane(ifstream* file, int segmentID)
{
  int numOfWaypoints, laneWidth, laneID;
  unsigned i;
  string leftBoundary, rightBoundary, line, word;
  vector<string> checkpoints;
  vector<string> stops;
  char letter;
  
  laneWidth = 0;
  
  while(word != "end_lane")
  {
    letter = file->peek();
    
    if(letter >= '0' && letter <= '9')
      parseWaypoint(file, segmentID, laneID);
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(word != "lane" && word != "num_waypoints" && word != "lane_width" &&
      word != "left_boundary" && word != "right_boundary" && word != "exit"
      && word != "checkpoint" && word != "stop")
      continue;
    else if(word == "lane")
    {
      lineStream >> word;
      
      int periodPosition = word.find(".", 0);
      
      istringstream charStream(word.substr(periodPosition + 1, word.size()), ios::in);
      
      charStream >> laneID;
    }
    else if(word == "num_waypoints")
    {
      lineStream >> numOfWaypoints;

      addLane(segmentID, laneID, numOfWaypoints);
    }
    else if(word == "lane_width")
    {
      string temp;
      
      lineStream >> temp;
      
      istringstream laneWidthStream(temp, ios::in);
      
      laneWidthStream >> laneWidth;
      
      setLaneWidth(segmentID, laneID, laneWidth);
    }
    else if(word == "left_boundary")
    {
      lineStream >> leftBoundary;
      
      setLeftBoundary(segmentID, laneID, leftBoundary);
    }
    else if(word == "right_boundary")
    {
      lineStream >> rightBoundary;
      
      setRightBoundary(segmentID, laneID, rightBoundary);
    }
    else if(word == "checkpoint")
    {
      string waypoint, checkpoint;
     
      lineStream >> waypoint;
      lineStream >> checkpoint;
      
      checkpoints.push_back(waypoint + "." + checkpoint);
    }
    else if(word == "stop")
    {
      lineStream >> word;
      
      stops.push_back(word);
    }
    else
      continue;
  }
  
  if(!checkpoints.empty())
  {
    for(i = 0; i < checkpoints.size(); i++)
    {
      int checkpointID, waypointID;
      string temp;
      
      temp = checkpoints[i];
      
      int firstPeriod = temp.find(".", 0);
      int secondPeriod = temp.find(".", firstPeriod + 1);
      int thirdPeriod = temp.find(".", secondPeriod + 1);

      istringstream waypointStream(temp.substr(secondPeriod + 1, thirdPeriod), ios::in);
      istringstream checkpointStream(temp.substr(thirdPeriod + 1, temp.size()), ios::in);
      
      waypointStream >> waypointID;
      checkpointStream >> checkpointID;
      
      setCheckpoint(segmentID, laneID, waypointID, checkpointID);
    } 
  }
  
  if(!stops.empty())
  {
    for(i = 0; i < stops.size(); i++)
    {
      int waypointID;
      string temp;
      
      temp = stops[i];
      
      int firstPeriod = temp.find(".", 0);
      int secondPeriod = temp.find(".", firstPeriod + 1);

      istringstream waypointStream(temp.substr(secondPeriod + 1, temp.size()), ios::in);
      
      waypointStream >> waypointID;
      
      setStopSign(segmentID, laneID, waypointID);
    } 
  }
}

void RNDF::parseWaypoint(ifstream* file, int segmentID, int laneID)
{
  int waypointID;
  double longitude, latitude;
  string line, word; 

  char letter = 'a';
  
  while(letter != 'e')
  {
    letter = file->peek();
    
    if(letter == 'e')
      continue;
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
      
    int firstPeriod = word.find(".", 0);
    int secondPeriod = word.find(".", firstPeriod + 1);
    
    istringstream charStream(word.substr(secondPeriod + 1, word.size()), ios::in);
    
    charStream >> waypointID;
    
    lineStream >> latitude;
    
    lineStream >> longitude;
    
    addWaypoint(segmentID, laneID, waypointID, latitude, longitude);
  }
}

void RNDF::parseZone(ifstream* file)
{
  int zoneID, numOfSpots;
  string zoneName, line, word;
  char letter;
  
  while(word != "end_zone")
  {
    letter = file->peek();
    
    if(letter == 'p')
    {
      parsePerimeter(file, zoneID);
        
      continue;
    }
    
    if(letter == 's')
    {
      parseSpot(file, zoneID);
      
      continue;
    }
  
    getline(*file, line);
      
    istringstream lineStream(line, ios::in);

    lineStream >> word;
          
    if(word != "zone" && word != "num_spots" && word != "zone_name")
      continue;
    else if(word == "zone")
      lineStream >> zoneID;
    else if(word == "num_spots")
    {
      lineStream >> numOfSpots;
      
      addZone(zoneID, numOfSpots);
    }
    else if(word == "zone_name")
    {
      string zoneName;
      
      lineStream >> zoneName;
      
      setZoneName(zoneID, zoneName);
    }
    else
      continue;
  }
}

void RNDF::parsePerimeter(ifstream* file, int zoneID)
{
  int perimeterPointID;
  double longitude, latitude;
  string line, word;
  char letter;
  
  while(word != "end_perimeter")
  {      
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      int firstPeriod = word.find(".", 0);
      int secondPeriod = word.find(".", firstPeriod + 1);
      
      istringstream charStream(word.substr(secondPeriod + 1, word.size()), ios::in);
      
      charStream >> perimeterPointID;
      
      lineStream >> latitude;
      
      lineStream >> longitude;
      
      addPerimeterPoint(zoneID, perimeterPointID, latitude, longitude);
    }
  }
}

void RNDF::parseSpot(ifstream* file, int zoneID)
{
  string line, word;
  char letter;
  int spotID, spotWidth, waypointID;
  double latitude, longitude; 
  vector<string> checkpoints;
  
  while(word != "end_spot")
  {
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
      
    if(word == "spot")
    {
      lineStream >> word;
      
      int period = word.find(".", 0);
      
      istringstream charStream(word.substr(period + 1, word.size()), ios::in);
      
      charStream >> spotID;
      
      addSpot(zoneID, spotID);
    }
    else if(word == "spot_width")
    {
      lineStream >> spotWidth;
      
      setSpotWidth(zoneID, spotID, spotWidth);
    }
    else if(word == "checkpoint")
    {
      string waypoint, checkpoint;
     
      lineStream >> waypoint;
      lineStream >> checkpoint;
      
      checkpoints.push_back(waypoint + "." + checkpoint);
    }
    else if(letter >= '0' && letter <= '9')
    {      
      int firstPeriod = word.find(".", 0);
      int secondPeriod = word.find(".", firstPeriod + 1);
      
      istringstream charStream(word.substr(secondPeriod + 1, word.size()), ios::in);
      
      charStream >> waypointID;
      
      lineStream >> latitude;
      
      lineStream >> longitude;
      
      addSpotWaypoint(zoneID, spotID, waypointID, latitude, longitude);
    }
    else
      continue;
  }
  
  if(!checkpoints.empty())
  {
    for(unsigned i = 0; i < checkpoints.size(); i++)
    {
      int checkpointID, waypointID;
      string temp;
      
      temp = checkpoints[i];
      
      int firstPeriod = temp.find(".", 0);
      int secondPeriod = temp.find(".", firstPeriod + 1);
      int thirdPeriod = temp.find(".", secondPeriod + 1);

      istringstream waypointStream(temp.substr(secondPeriod + 1, thirdPeriod), ios::in);
      istringstream checkpointStream(temp.substr(thirdPeriod + 1, temp.size()), ios::in);
      
      waypointStream >> waypointID;
      checkpointStream >> checkpointID;
      
      setCheckpoint(zoneID, spotID, waypointID, checkpointID);
    } 
  }
}

void RNDF::parseExit(ifstream* file)
{
  string line, word;
  int entrySegmentID, entryLaneID, entryWaypointID, exitSegmentID, exitLaneID, exitWaypointID;
  
  while(word != "end_file")
  {
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(word == "exit")
    {
      lineStream >> word;
      
      int firstPeriod = word.find(".", 0);
      int secondPeriod = word.find(".", firstPeriod + 1);
      
      istringstream exitSID(word.substr(0, firstPeriod), ios::in);
      istringstream exitLID(word.substr(firstPeriod + 1, secondPeriod), ios::in);
      istringstream exitWPID(word.substr(secondPeriod + 1, word.size()), ios::in);
      
      exitSID >> exitSegmentID;
      
      exitLID >> exitLaneID;
      
      exitWPID >> exitWaypointID;
      
      lineStream >> word;
      
      firstPeriod = word.find(".", 0);
      secondPeriod = word.find(".", firstPeriod + 1);
      
      istringstream entrySID(word.substr(0, firstPeriod), ios::in);
      istringstream entryLID(word.substr(firstPeriod + 1, secondPeriod), ios::in);
      istringstream entryWPID(word.substr(secondPeriod + 1, word.size()), ios::in);
      
      entrySID >> entrySegmentID;
      
      entryLID >> entryLaneID;
      
      entryWPID >> entryWaypointID;
      
      setExit(entrySegmentID, entryLaneID, entryWaypointID, exitSegmentID, exitLaneID, exitWaypointID);
    }    
  }
}

void RNDF::parseCheckpoint(ifstream* file)
{
  string line, word;
  char letter;
  
  while(word != "end_checkpoints")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
      checkpoints.push_back(letter);
    else
      continue;      
  }
}

void RNDF::parseSpeedLimit(ifstream* file)
{
  int segmentID, minSpeed, maxSpeed;
  string line, word;
  char letter;
  
  while(word != "end_speed_limits")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    if(letter >= '0' && letter <= '9')
    {
      lineStream >> segmentID;
      lineStream >> minSpeed;
      lineStream >> maxSpeed;
      
      setSpeedLimits(segmentID, minSpeed, maxSpeed);
    }
    else
    {
      lineStream >> word;
      
      continue;      
    }
  }
}

void RNDF::setSegmentsAndZones(ifstream* file)
{
  bool set = false;
  
  while(!set)
  {
    string word, line;
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(word != "num_segments" && word != "num_zones")
      continue;
    
    if(word == "num_segments")
      lineStream >> this->numOfSegments;
    
    if(word == "num_zones")
    {
      lineStream >> this->numOfZones;
        
      set = true;
    }
  }
}
