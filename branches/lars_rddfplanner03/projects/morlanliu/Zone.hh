#ifndef ZONE_HH_
#define ZONE_HH_
#include "PerimeterPoint.hh"
#include "ParkingSpot.hh"
#include <string>
#include <vector>
using namespace std;

/*! Zone class. The Zone class represents a zone provided by the RNDF file.
 * All zones have a zone ID, a fixed number of parking spots, and a minimum
 * and maximum speed limit. Zones have a fixed perimeter characterized by
 * a fixed number of perimeter points. Zones may have an optional name.
 * \brief The Zone class represents a zone provided by the RNDF file.
 */
class Zone
{
public:
/*! All Zones have a zone ID and a fixed number of parking spots. */
	Zone(int zoneID, int numOfSpots);
	virtual ~Zone();

/*! Sets the zone name of THIS. */
  void setZoneName(string zoneName);
  
/*! Sets the minimum and maximum speed limit of THIS. */
  void setSpeedLimits(int minSpeed, int maxSpeed);
  
/*! Adds a perimeter point to the array of perimeter points contained in THIS. */
  void addPerimeterPoint(PerimeterPoint* perimeterPoint);
  
/*! Adds a parking spot to the array of parking spots contained in THIS. */
  void addParkingSpot(ParkingSpot* parkingSpot);

/*! Returns the zone ID of THIS. */
  int getZoneID();
  
/*! Returns the number of parking spots contained in THIS. */
  int getNumOfSpots();
  
/*! Returns the number of perimeter points contained in THIS. */
  int getNumOfPerimeterPoints();
  
/*! Returns the zone name of THIS. */
  string getZoneName();
  
/*! Returns the minimum speed of THIS. */
  int getMinSpeed();
  
/*! Returns the maximum speed of THIS. */
  int getMaxSpeed();
 
/*! Returns a pointer to a perimeter point with the perimeterPointID passed in
 *  as an argument. */
  PerimeterPoint* getPerimeterPoint(int perimeterPointID);
  
/*! Returns a pointer to a parking spot with the spotID passed in as an argument. */
  ParkingSpot* getParkingSpot(int spotID);
  
private:
/* Zones are identified with an integer > 0. All zones have a finite 
 * number of parkings spots in them. Zones also have a minimum and
 * maximum speed allowed. */
  int zoneID, numOfSpots, minSpeed, maxSpeed;
  
/* Optionally, a zone may also have a zone name. */
  string zoneName;
  
/* Each zone contains a zone perimeter. */
  vector<PerimeterPoint*> perimeter;
  
/* Each zone contains an array of parking spots of size numOfSpots. */
  vector<ParkingSpot*> spots;
};

#endif /*ZONE_HH_*/
