#include "Zone.hh"

using namespace std;

Zone::Zone(int zoneID, int numOfSpots)
{
  this->zoneID = zoneID;
  this->numOfSpots = numOfSpots;
  this->zoneName.clear();
  minSpeed = maxSpeed = 0;
}

Zone::~Zone()
{
  unsigned i = 0;
  
  for(i = 0; i < perimeter.size(); i++)
    delete perimeter[i];
    
  for(i = 0; i < spots.size(); i++)
    delete spots[i];
}

void Zone::setZoneName(string zoneName)
{
  this->zoneName = zoneName;
}

void Zone::setSpeedLimits(int minSpeed, int maxSpeed)
{
  this->minSpeed = minSpeed;
  this->maxSpeed = maxSpeed;
}

void Zone::addPerimeterPoint(PerimeterPoint* perimeterPoint)
{
  this->perimeter.push_back(perimeterPoint);
}

void Zone::addParkingSpot(ParkingSpot* parkingSpot)
{
  this->spots.push_back(parkingSpot);
}

int Zone::getZoneID()
{
  return this->zoneID;
}

int Zone::getNumOfSpots()
{
  return this->numOfSpots;
}

int Zone::getMinSpeed()
{
  return minSpeed;
}

int Zone::getMaxSpeed()
{
  return maxSpeed;
}

int Zone::getNumOfPerimeterPoints()
{
  return perimeter.size();
}

string Zone::getZoneName()
{
  return this->zoneName;
}

PerimeterPoint* Zone::getPerimeterPoint(int perimeterPointID)
{
  if(perimeterPointID > 0 && (unsigned) perimeterPointID <= this->perimeter.size())
    return this->perimeter[perimeterPointID - 1];
  else
    return NULL;
}

ParkingSpot* Zone::getParkingSpot(int spotID)
{
  if(spotID > 0 && spotID <= getNumOfSpots())
    return this->spots[spotID - 1];
  else
    return NULL;
}
