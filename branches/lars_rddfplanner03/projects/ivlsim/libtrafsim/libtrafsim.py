# This file was created automatically by SWIG 1.3.29.
# Don't modify this file, modify the SWIG interface instead.
# This file is compatible with both classic and new-style classes.

import _libtrafsim
import new
new_instancemethod = new.instancemethod
def _swig_setattr_nondynamic(self,class_type,name,value,static=1):
    if (name == "thisown"): return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'PySwigObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name,None)
    if method: return method(self,value)
    if (not static) or hasattr(self,name):
        self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)

def _swig_setattr(self,class_type,name,value):
    return _swig_setattr_nondynamic(self,class_type,name,value,0)

def _swig_getattr(self,class_type,name):
    if (name == "thisown"): return self.this.own()
    method = class_type.__swig_getmethods__.get(name,None)
    if method: return method(self)
    raise AttributeError,name

def _swig_repr(self):
    try: strthis = "proxy of " + self.this.__repr__()
    except: strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

import types
try:
    _object = types.ObjectType
    _newclass = 1
except AttributeError:
    class _object : pass
    _newclass = 0
del types


class PySwigIterator(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, PySwigIterator, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, PySwigIterator, name)
    def __init__(self): raise AttributeError, "No constructor defined"
    __repr__ = _swig_repr
    __swig_destroy__ = _libtrafsim.delete_PySwigIterator
    __del__ = lambda self : None;
    def value(*args): return _libtrafsim.PySwigIterator_value(*args)
    def incr(*args): return _libtrafsim.PySwigIterator_incr(*args)
    def decr(*args): return _libtrafsim.PySwigIterator_decr(*args)
    def distance(*args): return _libtrafsim.PySwigIterator_distance(*args)
    def equal(*args): return _libtrafsim.PySwigIterator_equal(*args)
    def copy(*args): return _libtrafsim.PySwigIterator_copy(*args)
    def next(*args): return _libtrafsim.PySwigIterator_next(*args)
    def previous(*args): return _libtrafsim.PySwigIterator_previous(*args)
    def advance(*args): return _libtrafsim.PySwigIterator_advance(*args)
    def __eq__(*args): return _libtrafsim.PySwigIterator___eq__(*args)
    def __ne__(*args): return _libtrafsim.PySwigIterator___ne__(*args)
    def __iadd__(*args): return _libtrafsim.PySwigIterator___iadd__(*args)
    def __isub__(*args): return _libtrafsim.PySwigIterator___isub__(*args)
    def __add__(*args): return _libtrafsim.PySwigIterator___add__(*args)
    def __sub__(*args): return _libtrafsim.PySwigIterator___sub__(*args)
    def __iter__(self): return self
PySwigIterator_swigregister = _libtrafsim.PySwigIterator_swigregister
PySwigIterator_swigregister(PySwigIterator)

SDLK_UNKNOWN = _libtrafsim.SDLK_UNKNOWN
SDLK_FIRST = _libtrafsim.SDLK_FIRST
SDLK_BACKSPACE = _libtrafsim.SDLK_BACKSPACE
SDLK_TAB = _libtrafsim.SDLK_TAB
SDLK_CLEAR = _libtrafsim.SDLK_CLEAR
SDLK_RETURN = _libtrafsim.SDLK_RETURN
SDLK_PAUSE = _libtrafsim.SDLK_PAUSE
SDLK_ESCAPE = _libtrafsim.SDLK_ESCAPE
SDLK_SPACE = _libtrafsim.SDLK_SPACE
SDLK_EXCLAIM = _libtrafsim.SDLK_EXCLAIM
SDLK_QUOTEDBL = _libtrafsim.SDLK_QUOTEDBL
SDLK_HASH = _libtrafsim.SDLK_HASH
SDLK_DOLLAR = _libtrafsim.SDLK_DOLLAR
SDLK_AMPERSAND = _libtrafsim.SDLK_AMPERSAND
SDLK_QUOTE = _libtrafsim.SDLK_QUOTE
SDLK_LEFTPAREN = _libtrafsim.SDLK_LEFTPAREN
SDLK_RIGHTPAREN = _libtrafsim.SDLK_RIGHTPAREN
SDLK_ASTERISK = _libtrafsim.SDLK_ASTERISK
SDLK_PLUS = _libtrafsim.SDLK_PLUS
SDLK_COMMA = _libtrafsim.SDLK_COMMA
SDLK_MINUS = _libtrafsim.SDLK_MINUS
SDLK_PERIOD = _libtrafsim.SDLK_PERIOD
SDLK_SLASH = _libtrafsim.SDLK_SLASH
SDLK_0 = _libtrafsim.SDLK_0
SDLK_1 = _libtrafsim.SDLK_1
SDLK_2 = _libtrafsim.SDLK_2
SDLK_3 = _libtrafsim.SDLK_3
SDLK_4 = _libtrafsim.SDLK_4
SDLK_5 = _libtrafsim.SDLK_5
SDLK_6 = _libtrafsim.SDLK_6
SDLK_7 = _libtrafsim.SDLK_7
SDLK_8 = _libtrafsim.SDLK_8
SDLK_9 = _libtrafsim.SDLK_9
SDLK_COLON = _libtrafsim.SDLK_COLON
SDLK_SEMICOLON = _libtrafsim.SDLK_SEMICOLON
SDLK_LESS = _libtrafsim.SDLK_LESS
SDLK_EQUALS = _libtrafsim.SDLK_EQUALS
SDLK_GREATER = _libtrafsim.SDLK_GREATER
SDLK_QUESTION = _libtrafsim.SDLK_QUESTION
SDLK_AT = _libtrafsim.SDLK_AT
SDLK_LEFTBRACKET = _libtrafsim.SDLK_LEFTBRACKET
SDLK_BACKSLASH = _libtrafsim.SDLK_BACKSLASH
SDLK_RIGHTBRACKET = _libtrafsim.SDLK_RIGHTBRACKET
SDLK_CARET = _libtrafsim.SDLK_CARET
SDLK_UNDERSCORE = _libtrafsim.SDLK_UNDERSCORE
SDLK_BACKQUOTE = _libtrafsim.SDLK_BACKQUOTE
SDLK_a = _libtrafsim.SDLK_a
SDLK_b = _libtrafsim.SDLK_b
SDLK_c = _libtrafsim.SDLK_c
SDLK_d = _libtrafsim.SDLK_d
SDLK_e = _libtrafsim.SDLK_e
SDLK_f = _libtrafsim.SDLK_f
SDLK_g = _libtrafsim.SDLK_g
SDLK_h = _libtrafsim.SDLK_h
SDLK_i = _libtrafsim.SDLK_i
SDLK_j = _libtrafsim.SDLK_j
SDLK_k = _libtrafsim.SDLK_k
SDLK_l = _libtrafsim.SDLK_l
SDLK_m = _libtrafsim.SDLK_m
SDLK_n = _libtrafsim.SDLK_n
SDLK_o = _libtrafsim.SDLK_o
SDLK_p = _libtrafsim.SDLK_p
SDLK_q = _libtrafsim.SDLK_q
SDLK_r = _libtrafsim.SDLK_r
SDLK_s = _libtrafsim.SDLK_s
SDLK_t = _libtrafsim.SDLK_t
SDLK_u = _libtrafsim.SDLK_u
SDLK_v = _libtrafsim.SDLK_v
SDLK_w = _libtrafsim.SDLK_w
SDLK_x = _libtrafsim.SDLK_x
SDLK_y = _libtrafsim.SDLK_y
SDLK_z = _libtrafsim.SDLK_z
SDLK_DELETE = _libtrafsim.SDLK_DELETE
SDLK_WORLD_0 = _libtrafsim.SDLK_WORLD_0
SDLK_WORLD_1 = _libtrafsim.SDLK_WORLD_1
SDLK_WORLD_2 = _libtrafsim.SDLK_WORLD_2
SDLK_WORLD_3 = _libtrafsim.SDLK_WORLD_3
SDLK_WORLD_4 = _libtrafsim.SDLK_WORLD_4
SDLK_WORLD_5 = _libtrafsim.SDLK_WORLD_5
SDLK_WORLD_6 = _libtrafsim.SDLK_WORLD_6
SDLK_WORLD_7 = _libtrafsim.SDLK_WORLD_7
SDLK_WORLD_8 = _libtrafsim.SDLK_WORLD_8
SDLK_WORLD_9 = _libtrafsim.SDLK_WORLD_9
SDLK_WORLD_10 = _libtrafsim.SDLK_WORLD_10
SDLK_WORLD_11 = _libtrafsim.SDLK_WORLD_11
SDLK_WORLD_12 = _libtrafsim.SDLK_WORLD_12
SDLK_WORLD_13 = _libtrafsim.SDLK_WORLD_13
SDLK_WORLD_14 = _libtrafsim.SDLK_WORLD_14
SDLK_WORLD_15 = _libtrafsim.SDLK_WORLD_15
SDLK_WORLD_16 = _libtrafsim.SDLK_WORLD_16
SDLK_WORLD_17 = _libtrafsim.SDLK_WORLD_17
SDLK_WORLD_18 = _libtrafsim.SDLK_WORLD_18
SDLK_WORLD_19 = _libtrafsim.SDLK_WORLD_19
SDLK_WORLD_20 = _libtrafsim.SDLK_WORLD_20
SDLK_WORLD_21 = _libtrafsim.SDLK_WORLD_21
SDLK_WORLD_22 = _libtrafsim.SDLK_WORLD_22
SDLK_WORLD_23 = _libtrafsim.SDLK_WORLD_23
SDLK_WORLD_24 = _libtrafsim.SDLK_WORLD_24
SDLK_WORLD_25 = _libtrafsim.SDLK_WORLD_25
SDLK_WORLD_26 = _libtrafsim.SDLK_WORLD_26
SDLK_WORLD_27 = _libtrafsim.SDLK_WORLD_27
SDLK_WORLD_28 = _libtrafsim.SDLK_WORLD_28
SDLK_WORLD_29 = _libtrafsim.SDLK_WORLD_29
SDLK_WORLD_30 = _libtrafsim.SDLK_WORLD_30
SDLK_WORLD_31 = _libtrafsim.SDLK_WORLD_31
SDLK_WORLD_32 = _libtrafsim.SDLK_WORLD_32
SDLK_WORLD_33 = _libtrafsim.SDLK_WORLD_33
SDLK_WORLD_34 = _libtrafsim.SDLK_WORLD_34
SDLK_WORLD_35 = _libtrafsim.SDLK_WORLD_35
SDLK_WORLD_36 = _libtrafsim.SDLK_WORLD_36
SDLK_WORLD_37 = _libtrafsim.SDLK_WORLD_37
SDLK_WORLD_38 = _libtrafsim.SDLK_WORLD_38
SDLK_WORLD_39 = _libtrafsim.SDLK_WORLD_39
SDLK_WORLD_40 = _libtrafsim.SDLK_WORLD_40
SDLK_WORLD_41 = _libtrafsim.SDLK_WORLD_41
SDLK_WORLD_42 = _libtrafsim.SDLK_WORLD_42
SDLK_WORLD_43 = _libtrafsim.SDLK_WORLD_43
SDLK_WORLD_44 = _libtrafsim.SDLK_WORLD_44
SDLK_WORLD_45 = _libtrafsim.SDLK_WORLD_45
SDLK_WORLD_46 = _libtrafsim.SDLK_WORLD_46
SDLK_WORLD_47 = _libtrafsim.SDLK_WORLD_47
SDLK_WORLD_48 = _libtrafsim.SDLK_WORLD_48
SDLK_WORLD_49 = _libtrafsim.SDLK_WORLD_49
SDLK_WORLD_50 = _libtrafsim.SDLK_WORLD_50
SDLK_WORLD_51 = _libtrafsim.SDLK_WORLD_51
SDLK_WORLD_52 = _libtrafsim.SDLK_WORLD_52
SDLK_WORLD_53 = _libtrafsim.SDLK_WORLD_53
SDLK_WORLD_54 = _libtrafsim.SDLK_WORLD_54
SDLK_WORLD_55 = _libtrafsim.SDLK_WORLD_55
SDLK_WORLD_56 = _libtrafsim.SDLK_WORLD_56
SDLK_WORLD_57 = _libtrafsim.SDLK_WORLD_57
SDLK_WORLD_58 = _libtrafsim.SDLK_WORLD_58
SDLK_WORLD_59 = _libtrafsim.SDLK_WORLD_59
SDLK_WORLD_60 = _libtrafsim.SDLK_WORLD_60
SDLK_WORLD_61 = _libtrafsim.SDLK_WORLD_61
SDLK_WORLD_62 = _libtrafsim.SDLK_WORLD_62
SDLK_WORLD_63 = _libtrafsim.SDLK_WORLD_63
SDLK_WORLD_64 = _libtrafsim.SDLK_WORLD_64
SDLK_WORLD_65 = _libtrafsim.SDLK_WORLD_65
SDLK_WORLD_66 = _libtrafsim.SDLK_WORLD_66
SDLK_WORLD_67 = _libtrafsim.SDLK_WORLD_67
SDLK_WORLD_68 = _libtrafsim.SDLK_WORLD_68
SDLK_WORLD_69 = _libtrafsim.SDLK_WORLD_69
SDLK_WORLD_70 = _libtrafsim.SDLK_WORLD_70
SDLK_WORLD_71 = _libtrafsim.SDLK_WORLD_71
SDLK_WORLD_72 = _libtrafsim.SDLK_WORLD_72
SDLK_WORLD_73 = _libtrafsim.SDLK_WORLD_73
SDLK_WORLD_74 = _libtrafsim.SDLK_WORLD_74
SDLK_WORLD_75 = _libtrafsim.SDLK_WORLD_75
SDLK_WORLD_76 = _libtrafsim.SDLK_WORLD_76
SDLK_WORLD_77 = _libtrafsim.SDLK_WORLD_77
SDLK_WORLD_78 = _libtrafsim.SDLK_WORLD_78
SDLK_WORLD_79 = _libtrafsim.SDLK_WORLD_79
SDLK_WORLD_80 = _libtrafsim.SDLK_WORLD_80
SDLK_WORLD_81 = _libtrafsim.SDLK_WORLD_81
SDLK_WORLD_82 = _libtrafsim.SDLK_WORLD_82
SDLK_WORLD_83 = _libtrafsim.SDLK_WORLD_83
SDLK_WORLD_84 = _libtrafsim.SDLK_WORLD_84
SDLK_WORLD_85 = _libtrafsim.SDLK_WORLD_85
SDLK_WORLD_86 = _libtrafsim.SDLK_WORLD_86
SDLK_WORLD_87 = _libtrafsim.SDLK_WORLD_87
SDLK_WORLD_88 = _libtrafsim.SDLK_WORLD_88
SDLK_WORLD_89 = _libtrafsim.SDLK_WORLD_89
SDLK_WORLD_90 = _libtrafsim.SDLK_WORLD_90
SDLK_WORLD_91 = _libtrafsim.SDLK_WORLD_91
SDLK_WORLD_92 = _libtrafsim.SDLK_WORLD_92
SDLK_WORLD_93 = _libtrafsim.SDLK_WORLD_93
SDLK_WORLD_94 = _libtrafsim.SDLK_WORLD_94
SDLK_WORLD_95 = _libtrafsim.SDLK_WORLD_95
SDLK_KP0 = _libtrafsim.SDLK_KP0
SDLK_KP1 = _libtrafsim.SDLK_KP1
SDLK_KP2 = _libtrafsim.SDLK_KP2
SDLK_KP3 = _libtrafsim.SDLK_KP3
SDLK_KP4 = _libtrafsim.SDLK_KP4
SDLK_KP5 = _libtrafsim.SDLK_KP5
SDLK_KP6 = _libtrafsim.SDLK_KP6
SDLK_KP7 = _libtrafsim.SDLK_KP7
SDLK_KP8 = _libtrafsim.SDLK_KP8
SDLK_KP9 = _libtrafsim.SDLK_KP9
SDLK_KP_PERIOD = _libtrafsim.SDLK_KP_PERIOD
SDLK_KP_DIVIDE = _libtrafsim.SDLK_KP_DIVIDE
SDLK_KP_MULTIPLY = _libtrafsim.SDLK_KP_MULTIPLY
SDLK_KP_MINUS = _libtrafsim.SDLK_KP_MINUS
SDLK_KP_PLUS = _libtrafsim.SDLK_KP_PLUS
SDLK_KP_ENTER = _libtrafsim.SDLK_KP_ENTER
SDLK_KP_EQUALS = _libtrafsim.SDLK_KP_EQUALS
SDLK_UP = _libtrafsim.SDLK_UP
SDLK_DOWN = _libtrafsim.SDLK_DOWN
SDLK_RIGHT = _libtrafsim.SDLK_RIGHT
SDLK_LEFT = _libtrafsim.SDLK_LEFT
SDLK_INSERT = _libtrafsim.SDLK_INSERT
SDLK_HOME = _libtrafsim.SDLK_HOME
SDLK_END = _libtrafsim.SDLK_END
SDLK_PAGEUP = _libtrafsim.SDLK_PAGEUP
SDLK_PAGEDOWN = _libtrafsim.SDLK_PAGEDOWN
SDLK_F1 = _libtrafsim.SDLK_F1
SDLK_F2 = _libtrafsim.SDLK_F2
SDLK_F3 = _libtrafsim.SDLK_F3
SDLK_F4 = _libtrafsim.SDLK_F4
SDLK_F5 = _libtrafsim.SDLK_F5
SDLK_F6 = _libtrafsim.SDLK_F6
SDLK_F7 = _libtrafsim.SDLK_F7
SDLK_F8 = _libtrafsim.SDLK_F8
SDLK_F9 = _libtrafsim.SDLK_F9
SDLK_F10 = _libtrafsim.SDLK_F10
SDLK_F11 = _libtrafsim.SDLK_F11
SDLK_F12 = _libtrafsim.SDLK_F12
SDLK_F13 = _libtrafsim.SDLK_F13
SDLK_F14 = _libtrafsim.SDLK_F14
SDLK_F15 = _libtrafsim.SDLK_F15
SDLK_NUMLOCK = _libtrafsim.SDLK_NUMLOCK
SDLK_CAPSLOCK = _libtrafsim.SDLK_CAPSLOCK
SDLK_SCROLLOCK = _libtrafsim.SDLK_SCROLLOCK
SDLK_RSHIFT = _libtrafsim.SDLK_RSHIFT
SDLK_LSHIFT = _libtrafsim.SDLK_LSHIFT
SDLK_RCTRL = _libtrafsim.SDLK_RCTRL
SDLK_LCTRL = _libtrafsim.SDLK_LCTRL
SDLK_RALT = _libtrafsim.SDLK_RALT
SDLK_LALT = _libtrafsim.SDLK_LALT
SDLK_RMETA = _libtrafsim.SDLK_RMETA
SDLK_LMETA = _libtrafsim.SDLK_LMETA
SDLK_LSUPER = _libtrafsim.SDLK_LSUPER
SDLK_RSUPER = _libtrafsim.SDLK_RSUPER
SDLK_MODE = _libtrafsim.SDLK_MODE
SDLK_COMPOSE = _libtrafsim.SDLK_COMPOSE
SDLK_HELP = _libtrafsim.SDLK_HELP
SDLK_PRINT = _libtrafsim.SDLK_PRINT
SDLK_SYSREQ = _libtrafsim.SDLK_SYSREQ
SDLK_BREAK = _libtrafsim.SDLK_BREAK
SDLK_MENU = _libtrafsim.SDLK_MENU
SDLK_POWER = _libtrafsim.SDLK_POWER
SDLK_EURO = _libtrafsim.SDLK_EURO
SDLK_UNDO = _libtrafsim.SDLK_UNDO
SDLK_LAST = _libtrafsim.SDLK_LAST
KMOD_NONE = _libtrafsim.KMOD_NONE
KMOD_LSHIFT = _libtrafsim.KMOD_LSHIFT
KMOD_RSHIFT = _libtrafsim.KMOD_RSHIFT
KMOD_LCTRL = _libtrafsim.KMOD_LCTRL
KMOD_RCTRL = _libtrafsim.KMOD_RCTRL
KMOD_LALT = _libtrafsim.KMOD_LALT
KMOD_RALT = _libtrafsim.KMOD_RALT
KMOD_LMETA = _libtrafsim.KMOD_LMETA
KMOD_RMETA = _libtrafsim.KMOD_RMETA
KMOD_NUM = _libtrafsim.KMOD_NUM
KMOD_CAPS = _libtrafsim.KMOD_CAPS
KMOD_MODE = _libtrafsim.KMOD_MODE
KMOD_RESERVED = _libtrafsim.KMOD_RESERVED
class Vector(_object):
    """Proxy of C++ Vector class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, Vector, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, Vector, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> Vector
        __init__(self, float x, float y) -> Vector

        You can construct a vector one of two ways:

        >>> print Vector()
        Vector(0, 0)
        >>> print Vector(1, 1)
        Vector(1, 1)


        """
        this = _libtrafsim.new_Vector(*args)
        try: self.this.append(this)
        except: self.this = this
    def __add__(*args):
        """
        __add__(self, Vector rhs) -> Vector

        Arithmetic addition:

        >>> print Vector(1, 2) + Vector(3, 4)
        Vector(4, 6)


        """
        return _libtrafsim.Vector___add__(*args)

    def __sub__(*args):
        """
        __sub__(self, Vector rhs) -> Vector

        Arithmetic subtraction or negation:

        >>> print Vector(1, 2) - Vector(3, 4)
        Vector(-2, -2)
        >>> print -Vector(1, 2)
        Vector(-1, -2)


        """
        return _libtrafsim.Vector___sub__(*args)

    def __mul__(*args):
        """
        __mul__(self, float rhs) -> Vector

        Scalar multiplication:

        >>> print Vector(1, 2) * 4.0
        Vector(4, 8)


        """
        return _libtrafsim.Vector___mul__(*args)

    def __div__(*args):
        """
        __div__(self, float rhs) -> Vector

        Scalar division:

        >>> print Vector(1, 2) / 4.0
        Vector(0.25, 0.5)

        If rhs is zero, the results are undefined. 
        If debugging is enabled, a failed assertion will terminate the program.


        """
        return _libtrafsim.Vector___div__(*args)

    def dot(*args):
        """
        dot(Vector v1, Vector v2) -> float

        Dot product.

        >>> Vector.dot( Vector(1.0, 1.0), Vector(1.0, 0.0) )
        1.0


        """
        return _libtrafsim.Vector_dot(*args)

    if _newclass:dot = staticmethod(dot)
    __swig_getmethods__["dot"] = lambda x: dot
    def dist2(*args):
        """
        dist2(Vector v1, Vector v2) -> float

        Square of the distance between two vectors.
        More efficient than (v1 - v2).length().

        >>> Vector.dist2( Vector(0, 3), Vector(4, 0) )
        25.0


        """
        return _libtrafsim.Vector_dist2(*args)

    if _newclass:dist2 = staticmethod(dist2)
    __swig_getmethods__["dist2"] = lambda x: dist2
    def __neg__(*args):
        """
        __neg__(self) -> Vector

        Arithmetic subtraction or negation:

        >>> print Vector(1, 2) - Vector(3, 4)
        Vector(-2, -2)
        >>> print -Vector(1, 2)
        Vector(-1, -2)


        """
        return _libtrafsim.Vector___neg__(*args)

    def __iadd__(*args):
        """
        __iadd__(self, Vector rhs) -> Vector

        Destructive vector addition:

        >>> v = Vector(1, 2)
        >>> v += Vector(3, 4)
        >>> print v
        Vector(4, 6)


        """
        return _libtrafsim.Vector___iadd__(*args)

    def __isub__(*args):
        """
        __isub__(self, Vector rhs) -> Vector

        Destructive vector subtraction:

        >>> v = Vector(1, 2)
        >>> v -= Vector(3, 4)
        >>> print v
        Vector(-2, -2)


        """
        return _libtrafsim.Vector___isub__(*args)

    def __imul__(*args):
        """
        __imul__(self, float rhs) -> Vector

        Destructive scalar multiplication:

        >>> v = Vector(1, 2)
        >>> v *= 10.0
        >>> print v
        Vector(10, 20)


        """
        return _libtrafsim.Vector___imul__(*args)

    def __idiv__(*args):
        """
        __idiv__(self, float rhs) -> Vector

        Destructive scalar division:

        >>> v = Vector(1, 2)
        >>> v /= 10.0
        >>> print v
        Vector(0.1, 0.2)

        If rhs is zero, the results are undefined. 
        If debugging is enabled, a failed assertion will terminate the program.


        """
        return _libtrafsim.Vector___idiv__(*args)

    def length(*args):
        """
        length(self) -> float

        sqrt(v.x^2 + v.y^2)

        >>> Vector(3.0, 4.0).length()
        5.0


        """
        return _libtrafsim.Vector_length(*args)

    def normalized(*args):
        """
        normalized(self) -> Vector

        Returns a new vector, scaled down to length 1.

        >>> print Vector(1.0, 1.0).normalized()
        Vector(0.707107, 0.707107)

        If the vector's original length is zero, the results are undefined. 
        If debugging is enabled, a failed assertion will terminate the program.


        """
        return _libtrafsim.Vector_normalized(*args)

    def perpendicular(*args):
        """
        perpendicular(self) -> Vector

        Returns one of the vector's perpendiculars.

        >>> print Vector(1.0, 2.0).perpendicular()
        Vector(-2, 1)


        """
        return _libtrafsim.Vector_perpendicular(*args)

    def normalize(*args):
        """
        normalize(self)

        The destructive equivalent of Vector.normalized().

        >>> v = Vector(1.0, 1.0)
        >>> v.normalize()
        >>> print v
        Vector(0.707107, 0.707107)

        If the vector's original length is zero, the results are undefined. 
        If debugging is enabled, a failed assertion will terminate the program.


        """
        return _libtrafsim.Vector_normalize(*args)

    def draw(*args):
        """
        draw(self, Vector origin=Vector(), float arrowSize=1.0f)
        draw(self, Vector origin=Vector())
        draw(self)
        """
        return _libtrafsim.Vector_draw(*args)

    def intersection(*args):
        """
        intersection(seg1pt1, seg1pt2, seg2pt1, seg2pt2, output) -> bool

        Computes the point of intersection of the line segments defined
        by the given vectors. If no such point exists, returns false.
        Otherwise, stores the result in the Vector named 'output' and returns true.

        >>> v = Vector()
        >>> Vector.intersection( Vector(0.0, 0.0), Vector(1.0, 1.0),
        ...			 Vector(1.0, 0.0), Vector(0.0, 1.0),
        ...			 v ) 
        True
        >>> print v
        Vector(0.5, 0.5)
        >>> Vector.intersection( Vector(0.0, 0.0), Vector(1.0, 1.0),
        ...			 Vector(1.0, 0.0), Vector(1.1, 1.0),
        ...			 v )
        False
        >>> print v
        Vector(0.5, 0.5)


        """
        return _libtrafsim.Vector_intersection(*args)

    if _newclass:intersection = staticmethod(intersection)
    __swig_getmethods__["intersection"] = lambda x: intersection
    __swig_setmethods__["x"] = _libtrafsim.Vector_x_set
    __swig_getmethods__["x"] = _libtrafsim.Vector_x_get
    if _newclass:x = property(_libtrafsim.Vector_x_get, _libtrafsim.Vector_x_set)
    __swig_setmethods__["y"] = _libtrafsim.Vector_y_set
    __swig_getmethods__["y"] = _libtrafsim.Vector_y_get
    if _newclass:y = property(_libtrafsim.Vector_y_get, _libtrafsim.Vector_y_set)
    def __str__(*args):
        """
        __str__(self) -> string

        A vector is represented by the format Vector(x, y).
        This format is readable by Vector.parse()

        >>> s = str(Vector(10.0, 20.0))
        >>> s
        'Vector(10, 20)'
        >>> print Vector.parse(s)
        Vector(10, 20)


        """
        return _libtrafsim.Vector___str__(*args)

    def parse(*args):
        """
        parse(string str) -> Vector

        A vector is represented by the format Vector(x, y).
        This format is readable by Vector.parse()

        >>> s = str(Vector(10.0, 20.0))
        >>> s
        'Vector(10, 20)'
        >>> print Vector.parse(s)
        Vector(10, 20)


        """
        return _libtrafsim.Vector_parse(*args)

    if _newclass:parse = staticmethod(parse)
    __swig_getmethods__["parse"] = lambda x: parse
    __swig_destroy__ = _libtrafsim.delete_Vector
    __del__ = lambda self : None;
Vector_swigregister = _libtrafsim.Vector_swigregister
Vector_swigregister(Vector)

def Vector_dot(*args):
  """
    Vector_dot(Vector v1, Vector v2) -> float

    Dot product.

    >>> Vector.dot( Vector(1.0, 1.0), Vector(1.0, 0.0) )
    1.0


    """
  return _libtrafsim.Vector_dot(*args)

def Vector_dist2(*args):
  """
    Vector_dist2(Vector v1, Vector v2) -> float

    Square of the distance between two vectors.
    More efficient than (v1 - v2).length().

    >>> Vector.dist2( Vector(0, 3), Vector(4, 0) )
    25.0


    """
  return _libtrafsim.Vector_dist2(*args)

def Vector_intersection(*args):
  """
    Vector_intersection(seg1pt1, seg1pt2, seg2pt1, seg2pt2, output) -> bool

    Computes the point of intersection of the line segments defined
    by the given vectors. If no such point exists, returns false.
    Otherwise, stores the result in the Vector named 'output' and returns true.

    >>> v = Vector()
    >>> Vector.intersection( Vector(0.0, 0.0), Vector(1.0, 1.0),
    ...			 Vector(1.0, 0.0), Vector(0.0, 1.0),
    ...			 v ) 
    True
    >>> print v
    Vector(0.5, 0.5)
    >>> Vector.intersection( Vector(0.0, 0.0), Vector(1.0, 1.0),
    ...			 Vector(1.0, 0.0), Vector(1.1, 1.0),
    ...			 v )
    False
    >>> print v
    Vector(0.5, 0.5)


    """
  return _libtrafsim.Vector_intersection(*args)

def Vector_parse(*args):
  """
    Vector_parse(string str) -> Vector

    A vector is represented by the format Vector(x, y).
    This format is readable by Vector.parse()

    >>> s = str(Vector(10.0, 20.0))
    >>> s
    'Vector(10, 20)'
    >>> print Vector.parse(s)
    Vector(10, 20)


    """
  return _libtrafsim.Vector_parse(*args)

class Color(_object):
    """Proxy of C++ Color class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, Color, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, Color, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> Color
        __init__(self, float r, float g, float b, float a) -> Color

        You can construct a color one of two ways:

        >>> print Color()
        Color(1, 1, 1, 1)
        >>> print Color(0.1, 0.2, 0.3, 0.4)
        Color(0.1, 0.2, 0.3, 0.4)

        The default Color is opaque white.

        """
        this = _libtrafsim.new_Color(*args)
        try: self.this.append(this)
        except: self.this = this
    def __add__(*args):
        """
        __add__(self, Color rhs) -> Color

        Additive blending:

        >>> print Color(0.1, 0.1, 0.1, 0.1) + Color(0.1, 0.2, 0.3, 0.4)
        Color(0.2, 0.3, 0.4, 0.5)


        """
        return _libtrafsim.Color___add__(*args)

    def __sub__(*args):
        """
        __sub__(self, Color rhs) -> Color

        Subtractive blending:

        >>> print Color(0.1, 0.1, 0.1, 0.1) - Color(0.1, 0.2, 0.3, 0.4)
        Color(0, -0.1, -0.2, -0.3)


        """
        return _libtrafsim.Color___sub__(*args)

    def __mul__(*args):
        """
        __mul__(self, Color rhs) -> Color
        __mul__(self, float rhs) -> Color

        Brightness scaling, or channel-wise modulation:

        >>> print Color(0.2, 0.2, 0.2, 0) * Color(0.1, 0.2, 0.3, 0.4)
        Color(0.02, 0.04, 0.06, 0)
        >>> print Color(0.1, 0.2, 0.3, 0.4) * 5.0
        Color(0.5, 1, 1.5, 2)


        """
        return _libtrafsim.Color___mul__(*args)

    def __div__(*args):
        """
        __div__(self, Color rhs) -> Color
        __div__(self, float rhs) -> Color

        Brightness scaling, or channel-wise modulation:

        >>> print Color(0.2, 0.2, 0.2, 0) / Color(0.1, 0.2, 0.3, 0.4)
        Color(2, 1, 0.666667, 0)
        >>> print Color(0.1, 0.2, 0.3, 0.4) / 5.0
        Color(0.02, 0.04, 0.06, 0.08)

        If rhs or any component of the denominator is zero, the results are undefined. 
        If debugging is enabled, a failed assertion will terminate the program.


        """
        return _libtrafsim.Color___div__(*args)

    def __iadd__(*args):
        """
        __iadd__(self, Color rhs) -> Color

        Destructive additive blending:

        >>> c = Color(0.1, 0.1, 0.1, 0.1)
        >>> c += Color(0.1, 0.2, 0.3, 0.4)
        >>> print c
        Color(0.2, 0.3, 0.4, 0.5)


        """
        return _libtrafsim.Color___iadd__(*args)

    def __isub__(*args):
        """
        __isub__(self, Color rhs) -> Color

        Destructive subtractive blending:

        >>> c = Color(0.1, 0.1, 0.1, 0.1)
        >>> c -= Color(0.1, 0.2, 0.3, 0.4)
        >>> print c
        Color(0, -0.1, -0.2, -0.3)


        """
        return _libtrafsim.Color___isub__(*args)

    def __imul__(*args):
        """
        __imul__(self, Color rhs) -> Color
        __imul__(self, float rhs) -> Color

        Destructive brightness scaling, or channel-wise modulation:

        >>> c = Color(0.2, 0.2, 0.2, 0)
        >>> c *= Color(0.1, 0.2, 0.3, 0.4)
        >>> print c
        Color(0.02, 0.04, 0.06, 0)
        >>> c = Color(0.1, 0.2, 0.3, 0.4)
        >>> c *= 5.0
        >>> print c
        Color(0.5, 1, 1.5, 2)


        """
        return _libtrafsim.Color___imul__(*args)

    def __idiv__(*args):
        """
        __idiv__(self, Color rhs) -> Color
        __idiv__(self, float rhs) -> Color

        Destructive brightness scaling, or channel-wise modulation:

        >>> c = Color(0.2, 0.2, 0.2, 0)
        >>> c /= Color(0.1, 0.2, 0.3, 0.4)
        >>> print c
        Color(2, 1, 0.666667, 0)
        >>> c = Color(0.1, 0.2, 0.3, 0.4)
        >>> c /= 5.0
        >>> print c
        Color(0.02, 0.04, 0.06, 0.08)

        If rhs or any component of the denominator is zero, the results are undefined. 
        If debugging is enabled, a failed assertion will terminate the program.


        """
        return _libtrafsim.Color___idiv__(*args)

    __swig_setmethods__["r"] = _libtrafsim.Color_r_set
    __swig_getmethods__["r"] = _libtrafsim.Color_r_get
    if _newclass:r = property(_libtrafsim.Color_r_get, _libtrafsim.Color_r_set)
    __swig_setmethods__["g"] = _libtrafsim.Color_g_set
    __swig_getmethods__["g"] = _libtrafsim.Color_g_get
    if _newclass:g = property(_libtrafsim.Color_g_get, _libtrafsim.Color_g_set)
    __swig_setmethods__["b"] = _libtrafsim.Color_b_set
    __swig_getmethods__["b"] = _libtrafsim.Color_b_get
    if _newclass:b = property(_libtrafsim.Color_b_get, _libtrafsim.Color_b_set)
    __swig_setmethods__["a"] = _libtrafsim.Color_a_set
    __swig_getmethods__["a"] = _libtrafsim.Color_a_get
    if _newclass:a = property(_libtrafsim.Color_a_get, _libtrafsim.Color_a_set)
    def __str__(*args):
        """
        __str__(self) -> string

        A color is represented by the format Color(r, g, b, a).
        This format is readable by Color.parse()

        >>> s = str(Color(0.1, 0.2, 0.3, 0.4))
        >>> s
        'Color(0.1, 0.2, 0.3, 0.4)'
        >>> print Color.parse(s)
        Color(0.1, 0.2, 0.3, 0.4)


        """
        return _libtrafsim.Color___str__(*args)

    def parse(*args):
        """
        parse(string str) -> Color

        A color is represented by the format Color(r, g, b, a).
        This format is readable by Color.parse()

        >>> s = str(Color(0.1, 0.2, 0.3, 0.4))
        >>> s
        'Color(0.1, 0.2, 0.3, 0.4)'
        >>> print Color.parse(s)
        Color(0.1, 0.2, 0.3, 0.4)


        """
        return _libtrafsim.Color_parse(*args)

    if _newclass:parse = staticmethod(parse)
    __swig_getmethods__["parse"] = lambda x: parse
    __swig_destroy__ = _libtrafsim.delete_Color
    __del__ = lambda self : None;
Color_swigregister = _libtrafsim.Color_swigregister
Color_swigregister(Color)

def Color_parse(*args):
  """
    Color_parse(string str) -> Color

    A color is represented by the format Color(r, g, b, a).
    This format is readable by Color.parse()

    >>> s = str(Color(0.1, 0.2, 0.3, 0.4))
    >>> s
    'Color(0.1, 0.2, 0.3, 0.4)'
    >>> print Color.parse(s)
    Color(0.1, 0.2, 0.3, 0.4)


    """
  return _libtrafsim.Color_parse(*args)


def glSetColor(*args):
  """
    glSetColor(Color value)

    Sets the OpenGL state to use this Color as the rendering color.
    Note that internally, OpenGL color channels are clamped to [0, 1].
    It is often convenient to allow for unclamped color values in
    program logic, but be aware that any such extraneous information
    is ignored by OpenGL.

    >>> glSetColor( Color(0.5, 0.5, 0.5, 0.5) )

    (The next object to be rendered should appear a translucent gray).

    """
  return _libtrafsim.glSetColor(*args)
EMPTY = _libtrafsim.EMPTY
MOUSEMOTION = _libtrafsim.MOUSEMOTION
MOUSEBUTTONDOWN = _libtrafsim.MOUSEBUTTONDOWN
MOUSEBUTTONUP = _libtrafsim.MOUSEBUTTONUP
KEYDOWN = _libtrafsim.KEYDOWN
KEYUP = _libtrafsim.KEYUP
REPAINT = _libtrafsim.REPAINT
QUIT = _libtrafsim.QUIT
LEFT = _libtrafsim.LEFT
MIDDLE = _libtrafsim.MIDDLE
RIGHT = _libtrafsim.RIGHT
class ViewportEvent(_object):
    """Proxy of C++ ViewportEvent class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, ViewportEvent, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, ViewportEvent, name)
    __repr__ = _swig_repr
    __swig_setmethods__["type"] = _libtrafsim.ViewportEvent_type_set
    __swig_getmethods__["type"] = _libtrafsim.ViewportEvent_type_get
    if _newclass:type = property(_libtrafsim.ViewportEvent_type_get, _libtrafsim.ViewportEvent_type_set)
    __swig_setmethods__["button"] = _libtrafsim.ViewportEvent_button_set
    __swig_getmethods__["button"] = _libtrafsim.ViewportEvent_button_get
    if _newclass:button = property(_libtrafsim.ViewportEvent_button_get, _libtrafsim.ViewportEvent_button_set)
    __swig_setmethods__["pos"] = _libtrafsim.ViewportEvent_pos_set
    __swig_getmethods__["pos"] = _libtrafsim.ViewportEvent_pos_get
    if _newclass:pos = property(_libtrafsim.ViewportEvent_pos_get, _libtrafsim.ViewportEvent_pos_set)
    def __init__(self, *args): 
        """__init__(self) -> ViewportEvent"""
        this = _libtrafsim.new_ViewportEvent(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_ViewportEvent
    __del__ = lambda self : None;
ViewportEvent_swigregister = _libtrafsim.ViewportEvent_swigregister
ViewportEvent_swigregister(ViewportEvent)

class Viewport(_object):
    """
    This class encapsulates the functionality of a window that displays some portion of a 2D
    OpenGL rendering environment. (see doxygen docs for more details).

    >>> v = Viewport()
    >>> v.open(100, 100, 'Test Viewport')
    True
    >>> # the following should be done in a loop:
    >>> event = ViewportEvent()
    >>> while v.pollEvents(event): pass  # in a real program, process events here
    >>> # do some drawing or some such
    >>> v.repaint()
    True
    >>> v.close()
    True


    """
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, Viewport, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, Viewport, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> Viewport"""
        this = _libtrafsim.new_Viewport(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_Viewport
    __del__ = lambda self : None;
    def open(*args):
        """
        open(self, unsigned int width, unsigned int height, string caption) -> bool

        Viewport.open() creates a new viewport window. Note that multiple calls to open() 
        will only reset the parameters of the first window rather than opening a new one.
        Note that most calls to Viewport functions will have to take place in the same thread
        as the original call to Viewport.open().

        >>> v = Viewport()
        >>> v.repaint()    # can't do this before calling open()
        False
        >>> v.open(100, 100, 'Test Viewport')
        True
        >>> v.repaint()
        True
        >>> print v.getPhysicalPosition()
        Vector(0, 0)
        >>> v.setPhysicalPosition( Vector(100, 100) )
        True
        >>> print v.getPhysicalPosition()
        Vector(100, 100)
        >>> v.open(200, 200, 'Test Viewport')
        True
        >>> print v.getPhysicalPosition()
        Vector(0, 0)
        >>> v.close()
        True


        """
        return _libtrafsim.Viewport_open(*args)

    def close(*args):
        """
        close(self) -> bool

        This function can be called at any time to shutdown a viewport. However,
        it should be made from the same thread from which the call to Viewport.open()
        was made.

        >>> v = Viewport()
        >>> v.close()
        True
        >>> v.open(100, 100, 'Test Viewport')
        True
        >>> v.repaint()
        True
        >>> v.close()
        True
        >>> v.repaint()
        False


        """
        return _libtrafsim.Viewport_close(*args)

    def getScreenWidth(*args):
        """getScreenWidth(self) -> unsigned int"""
        return _libtrafsim.Viewport_getScreenWidth(*args)

    def getScreenHeight(*args):
        """getScreenHeight(self) -> unsigned int"""
        return _libtrafsim.Viewport_getScreenHeight(*args)

    def getPhysicalSize(*args):
        """getPhysicalSize(self) -> Vector"""
        return _libtrafsim.Viewport_getPhysicalSize(*args)

    def getPhysicalPosition(*args):
        """getPhysicalPosition(self) -> Vector"""
        return _libtrafsim.Viewport_getPhysicalPosition(*args)

    def isPanZoomEnabled(*args):
        """isPanZoomEnabled(self) -> bool"""
        return _libtrafsim.Viewport_isPanZoomEnabled(*args)

    def resize(*args):
        """
        resize(self, unsigned int width, unsigned int height) -> bool

        Changes the on-screen dimensions of the viewport window. Also scales the physical dimensions accordingly,
        so that there is no aspect ratio distortion. It is not safe to call this function from a different thread
        than the one the viewport was opened in. This call will also fail if the window is not currently open.

        >>> v = Viewport()
        >>> v.resize(200, 200)
        False
        >>> v.open(300, 300, 'Test Viewport')
        True
        >>> v.getScreenWidth(), v.getScreenHeight()
        (300, 300)
        >>> print v.getPhysicalSize()
        Vector(100, 100)
        >>> v.resize(150, 600)
        True
        >>> v.getScreenWidth(), v.getScreenHeight()
        (150, 600)
        >>> print v.getPhysicalSize()
        Vector(50, 200)
        >>> v.close()
        True


        """
        return _libtrafsim.Viewport_resize(*args)

    def enablePanZoom(*args):
        """enablePanZoom(self)"""
        return _libtrafsim.Viewport_enablePanZoom(*args)

    def disablePanZoom(*args):
        """disablePanZoom(self)"""
        return _libtrafsim.Viewport_disablePanZoom(*args)

    def setPhysicalSize(*args):
        """
        setPhysicalSize(self, Vector size) -> bool

        Changes the 'zoom' setting of the viewport. It is not safe to call this function from a different thread
        than the one the viewport was opened in. This call will also fail if the window is not currently open.
        	
        >>> v = Viewport()
        >>> v.setPhysicalSize( Vector(1,1) )
        False
        >>> v.open(100, 100, 'Test Viewport')
        True
        >>> v.setPhysicalSize( Vector(1,1) )
        True
        >>> print v.getPhysicalSize()
        Vector(1, 1)
        >>> v.close()
        True


        """
        return _libtrafsim.Viewport_setPhysicalSize(*args)

    def setPhysicalPosition(*args):
        """
        setPhysicalPosition(self, Vector position) -> bool

        Changes the 'pan' setting of the viewport. It is not safe to call this function from a different thread
        than the one the viewport was opened in. This call will also fail if the window is not currently open.
        	
        >>> v = Viewport()
        >>> v.setPhysicalPosition( Vector(100,100) )
        False
        >>> v.open(100, 100, 'Test Viewport')
        True
        >>> v.setPhysicalPosition( Vector(100,100) )
        True
        >>> print v.getPhysicalPosition()
        Vector(100, 100)
        >>> v.close()
        True


        """
        return _libtrafsim.Viewport_setPhysicalPosition(*args)

    def setBackgroundColor(*args):
        """setBackgroundColor(self, Color c) -> bool"""
        return _libtrafsim.Viewport_setBackgroundColor(*args)

    def pollEvents(*args):
        """pollEvents(self, ViewportEvent viewportEvent) -> bool"""
        return _libtrafsim.Viewport_pollEvents(*args)

    def repaint(*args):
        """repaint(self) -> bool"""
        return _libtrafsim.Viewport_repaint(*args)

Viewport_swigregister = _libtrafsim.Viewport_swigregister
Viewport_swigregister(Viewport)

class Object(_object):
    """
    See the doxygen documentation for details about this abstract base class.

    """
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, Object, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, Object, name)
    def __init__(self): raise AttributeError, "No constructor defined"
    __repr__ = _swig_repr
    __swig_destroy__ = _libtrafsim.delete_Object
    __del__ = lambda self : None;
    def getName(*args):
        """getName(self) -> string"""
        return _libtrafsim.Object_getName(*args)

    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.Object_getClassID(*args)

    def getParent(*args):
        """getParent(self) -> string"""
        return _libtrafsim.Object_getParent(*args)

    def setParent(*args):
        """setParent(self, string parentName)"""
        return _libtrafsim.Object_setParent(*args)

    def __eq__(*args):
        """__eq__(self, Object rhs) -> bool"""
        return _libtrafsim.Object___eq__(*args)

    def draw(*args):
        """draw(self)"""
        return _libtrafsim.Object_draw(*args)

    def simulate(*args):
        """simulate(self, float ticks) -> bool"""
        return _libtrafsim.Object_simulate(*args)

    def __str__(*args):
        """__str__(self) -> string"""
        return _libtrafsim.Object___str__(*args)

    def parse(*args):
        """parse(string str) -> Object"""
        return _libtrafsim.Object_parse(*args)

    if _newclass:parse = staticmethod(parse)
    __swig_getmethods__["parse"] = lambda x: parse
Object_swigregister = _libtrafsim.Object_swigregister
Object_swigregister(Object)

def Object_parse(*args):
  """Object_parse(string str) -> Object"""
  return _libtrafsim.Object_parse(*args)

class Environment(_object):
    """
    See the doxygen documentation for details about Environments.

    >>> env = Environment()
    >>> env.addObject( TestObject() )
    'TestObject'
    >>> env.addObject( TestObject() )
    'TestObject___1'
    >>> env.draw()
    ... # 'Called TestObject.draw()'       
    ... # 'Called TestObject___1.draw()'
    >>> env.simulate(2.0)   # this should kill all the TestObjects
    ... # 'Called TestObject.simulate()'
    ... # 'Called TestObject___1.simulate()'
    >>> print env					# and indeed they're all gone now
    Environment( 
    bottomLeftBound --> Vector(0, 0)
    topRightBound --> Vector(100, 100)
    nameDirectory --> collection(0) {
     }
    nameList --> collection(1) {
    TestObject : 2 ,
     }
     )
    >>> env.clearObjects()


    """
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, Environment, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, Environment, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> Environment

        Nothing special happens in the constructor, so you can 
        create new environments without fear.


        >>> print Environment()
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(0) {
         }
        nameList --> collection(0) {
         }
         )


        """
        this = _libtrafsim.new_Environment(*args)
        try: self.this.append(this)
        except: self.this = this
    def addObject(*args):
        """
        addObject(self, Object object, string proposedName="") -> string
        addObject(self, Object object) -> string

        Adds a reference to the given object, trying to give it the proposedName.
        if proposedName is already in use, it will generate a new unique name.

        Also, when a class constructed in Python is passed into addObject,
        Python gives up ownership of the object so that it doesn't get prematurely
        garbage collected.

        >>> env = Environment()
        >>> env.addObject( TestObject() )
        'TestObject'
        >>> env.addObject( TestObject(), 'MyObject' )
        'MyObject'
        >>> env.addObject( TestObject(), 'MyObject' )
        'MyObject___1'
        >>> print env
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(3) {
        MyObject : TestObject( reference --> None ) ,
        MyObject___1 : TestObject( reference --> None ) ,
        TestObject : TestObject( reference --> None ) ,
         }
        nameList --> collection(2) {
        MyObject : 2 ,
        TestObject : 1 ,
         }
         )
        >>> env.clearObjects()


        """
        return _libtrafsim.Environment_addObject(*args)

    def removeObject(*args):
        """
        removeObject(self, string objName, bool canDelete=True) -> bool
        removeObject(self, string objName) -> bool

        Removes an object from the directory. If canDelete is true, also frees the object's memory.
        Also note that there are NO safeguards against dangling references. If an object is removed
        while another object refers to it, by name or otherwise, the results are unspecified.

        >>> env = Environment()
        >>> env.addObject( TestObject() )
        'TestObject'
        >>> env.addObject( TestObject() )
        'TestObject___1'
        >>> print env
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(2) {
        TestObject : TestObject( reference --> None ) ,
        TestObject___1 : TestObject( reference --> None ) ,
         }
        nameList --> collection(1) {
        TestObject : 2 ,
         }
         )
        >>> env.removeObject('TestObject___2')
        False
        >>> env.removeObject('TestObject')
        True
        >>> print env
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(1) {
        TestObject___1 : TestObject( reference --> None ) ,
         }
        nameList --> collection(1) {
        TestObject : 2 ,
         }
         )
        >>> env.clearObjects()


        """
        return _libtrafsim.Environment_removeObject(*args)

    def renameObject(*args):
        """
        renameObject(self, string oldName, string newName) -> string

        Changes an object's name.

        >>> env = Environment()
        >>> env.addObject(TestObject())
        'TestObject'
        >>> obj = env.getObject('TestObject')
        >>> env.renameObject('TestObject__1', 'MyObject')
        ''
        >>> env.addObject(TestObject(), 'MyObject')
        'MyObject'
        >>> env.renameObject('TestObject', 'MyObject') 
        'MyObject___1'
        >>> obj == env.getObject('MyObject___1')
        True
        >>> env.getObject('TestObject')
        >>> print env
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(2) {
        MyObject : TestObject( reference --> None ) ,
        MyObject___1 : TestObject( reference --> None ) ,
         }
        nameList --> collection(2) {
        MyObject : 2 ,
        TestObject : 1 ,
         }
         )
        >>> env.clearObjects()


        """
        return _libtrafsim.Environment_renameObject(*args)

    def getObject(*args):
        """
        getObject(self, string objName) -> Object

        Looks up an object by name. O(log n) in the number of objects.

        >>> env = Environment()
        >>> env.addObject(TestObject())
        'TestObject'
        >>> env.getObject('NotAnObject')
        >>> print env.getObject('TestObject')
        TestObject( reference --> None )
        >>> env.clearObjects()


        """
        return _libtrafsim.Environment_getObject(*args)

    def getObjectsByClassID(*args):
        """
        getObjectsByClassID(self, string classID) -> ObjectMap

        Returns a name->object dict of all the objects with the given classID.

        >>> env = Environment()
        >>> env.addObject(TestObject())
        'TestObject'
        >>> env.addObject(TestObject())
        'TestObject___1'
        >>> [name for name in env.getObjectsByClassID('TestObject').keys()]
        ['TestObject', 'TestObject___1']
        >>> env.clearObjects()


        """
        return _libtrafsim.Environment_getObjectsByClassID(*args)

    def clearObjects(*args):
        """
        clearObjects(self, bool canDelete=True)
        clearObjects(self)

        Resets the environment to its original state. All objects are removed, and deleted if
        canDelete is set to true. The name directory is also cleared out.

        >>> env = Environment()
        >>> env.addObject(TestObject())
        'TestObject'
        >>> env.addObject(TestObject())
        'TestObject___1'
        >>> print env
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(2) {
        TestObject : TestObject( reference --> None ) ,
        TestObject___1 : TestObject( reference --> None ) ,
         }
        nameList --> collection(1) {
        TestObject : 2 ,
         }
         )
        >>> env.clearObjects()
        >>> print env
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(0) {
         }
        nameList --> collection(0) {
         }
         )


        """
        return _libtrafsim.Environment_clearObjects(*args)

    def getBottomLeftBound(*args):
        """getBottomLeftBound(self) -> Vector"""
        return _libtrafsim.Environment_getBottomLeftBound(*args)

    def getTopRightBound(*args):
        """getTopRightBound(self) -> Vector"""
        return _libtrafsim.Environment_getTopRightBound(*args)

    def setBounds(*args):
        """setBounds(self, Vector bottomLeft, Vector topRight)"""
        return _libtrafsim.Environment_setBounds(*args)

    def draw(*args):
        """
        draw(self)

        Renders each of the environment's components. Call this from a 
        thread in which you opened a Viewport.

        >>> env = Environment()
        >>> env.addObject( TestObject() )
        'TestObject'
        >>> env.addObject( TestObject() )
        'TestObject___1'
        >>> env.draw()
        ... # 'Called TestObject.draw()'       
        ... # 'Called TestObject___1.draw()'
        >>> env.clearObjects()


        """
        return _libtrafsim.Environment_draw(*args)

    def simulate(*args):
        """
        simulate(self, float ticks)

        Tells each of the environment's components to update their state by another
        time step, by calling their own simulate() functions. Any component that returns
        'false' from simulate() gets automatically removed from the environment and deleted.

        >>> env = Environment()
        >>> env.addObject( TestObject() )
        'TestObject'
        >>> env.addObject( TestObject() )
        'TestObject___1'
        >>> env.simulate(0.0)   # TestObjects continue to live so long as ticks < 1.0
        ... # 'Called TestObject.draw()'       
        ... # 'Called TestObject___1.draw()'
        >>> print env
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(2) {
        TestObject : TestObject( reference --> None ) ,
        TestObject___1 : TestObject( reference --> None ) ,
         }
        nameList --> collection(1) {
        TestObject : 2 ,
         }
         )
        >>> env.simulate(2.0) # Now they die
        ... # 'Called TestObject.draw()'       
        ... # 'Called TestObject___1.draw()'
        >>> print env
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(0) {
         }
        nameList --> collection(1) {
        TestObject : 2 ,
         }
         )


        """
        return _libtrafsim.Environment_simulate(*args)

    def __str__(*args):
        """__str__(self) -> string"""
        return _libtrafsim.Environment___str__(*args)

    def parse(*args):
        """
        parse(string str) -> Environment

        An environment can save and restore its entire state in textual form. Here's how.

        >>> env = Environment()
        >>> env.addObject( TestObject(), 'Object1' )
        'Object1'
        >>> env.addObject( TestObject(), 'Object2' )
        'Object2'
        >>> env.addObject( TestObject(), 'Object3' )
        'Object3'
        >>> obj1 = TestObject.downcast( env.getObject( 'Object1' ) )   # turn the received Object into a TestObject
        >>> obj2 = TestObject.downcast( env.getObject( 'Object2' ) )
        >>> obj3 = TestObject.downcast( env.getObject( 'Object3' ) )
        >>> obj1.reference = obj3
        >>> obj2.reference = obj3
        >>> obj3.reference = obj1 
        >>> print env	
        ... # even though the references are pure pointers, the
        ... # environment knows which objects they are referring to
        ... # (so long as they are toplevel objects directly named
        ... # by the environment)
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(3) {
        Object1 : TestObject( reference --> Object3 ) ,
        Object2 : TestObject( reference --> Object3 ) ,
        Object3 : TestObject( reference --> Object1 ) ,
         }
        nameList --> collection(3) {
        Object1 : 1 ,
        Object2 : 1 ,
        Object3 : 1 ,
         }
         )
        >>> s1 = str(env)
        >>> env2 = Environment.parse(s1)   # construct a new environment from a string
        >>> print env2
        Environment( 
        bottomLeftBound --> Vector(0, 0)
        topRightBound --> Vector(100, 100)
        nameDirectory --> collection(3) {
        Object1 : TestObject( reference --> Object3 ) ,
        Object2 : TestObject( reference --> Object3 ) ,
        Object3 : TestObject( reference --> Object1 ) ,
         }
        nameList --> collection(3) {
        Object1 : 1 ,
        Object2 : 1 ,
        Object3 : 1 ,
         }
         )
        >>> env2obj1 = TestObject.downcast( env2.getObject('Object1') )
        >>> env2obj2 = TestObject.downcast( env2.getObject('Object2') )
        >>> env2obj3 = TestObject.downcast( env2.getObject('Object3') )
        >>> env2obj1.reference == env2obj3
        True
        >>> env2obj2.reference == env2obj3
        True
        >>> env2obj3.reference == env2obj1
        True
        >>> env2obj1 == obj1
        False
        >>> env == env2
        False

        So everything is restored in the new environment exactly as it was in the original.


        """
        return _libtrafsim.Environment_parse(*args)

    if _newclass:parse = staticmethod(parse)
    __swig_getmethods__["parse"] = lambda x: parse
    __swig_destroy__ = _libtrafsim.delete_Environment
    __del__ = lambda self : None;
Environment_swigregister = _libtrafsim.Environment_swigregister
Environment_swigregister(Environment)

def Environment_parse(*args):
  """
    Environment_parse(string str) -> Environment

    An environment can save and restore its entire state in textual form. Here's how.

    >>> env = Environment()
    >>> env.addObject( TestObject(), 'Object1' )
    'Object1'
    >>> env.addObject( TestObject(), 'Object2' )
    'Object2'
    >>> env.addObject( TestObject(), 'Object3' )
    'Object3'
    >>> obj1 = TestObject.downcast( env.getObject( 'Object1' ) )   # turn the received Object into a TestObject
    >>> obj2 = TestObject.downcast( env.getObject( 'Object2' ) )
    >>> obj3 = TestObject.downcast( env.getObject( 'Object3' ) )
    >>> obj1.reference = obj3
    >>> obj2.reference = obj3
    >>> obj3.reference = obj1 
    >>> print env	
    ... # even though the references are pure pointers, the
    ... # environment knows which objects they are referring to
    ... # (so long as they are toplevel objects directly named
    ... # by the environment)
    Environment( 
    bottomLeftBound --> Vector(0, 0)
    topRightBound --> Vector(100, 100)
    nameDirectory --> collection(3) {
    Object1 : TestObject( reference --> Object3 ) ,
    Object2 : TestObject( reference --> Object3 ) ,
    Object3 : TestObject( reference --> Object1 ) ,
     }
    nameList --> collection(3) {
    Object1 : 1 ,
    Object2 : 1 ,
    Object3 : 1 ,
     }
     )
    >>> s1 = str(env)
    >>> env2 = Environment.parse(s1)   # construct a new environment from a string
    >>> print env2
    Environment( 
    bottomLeftBound --> Vector(0, 0)
    topRightBound --> Vector(100, 100)
    nameDirectory --> collection(3) {
    Object1 : TestObject( reference --> Object3 ) ,
    Object2 : TestObject( reference --> Object3 ) ,
    Object3 : TestObject( reference --> Object1 ) ,
     }
    nameList --> collection(3) {
    Object1 : 1 ,
    Object2 : 1 ,
    Object3 : 1 ,
     }
     )
    >>> env2obj1 = TestObject.downcast( env2.getObject('Object1') )
    >>> env2obj2 = TestObject.downcast( env2.getObject('Object2') )
    >>> env2obj3 = TestObject.downcast( env2.getObject('Object3') )
    >>> env2obj1.reference == env2obj3
    True
    >>> env2obj2.reference == env2obj3
    True
    >>> env2obj3.reference == env2obj1
    True
    >>> env2obj1 == obj1
    False
    >>> env == env2
    False

    So everything is restored in the new environment exactly as it was in the original.


    """
  return _libtrafsim.Environment_parse(*args)

class TestObject(Object):
    """
    A trivial Object subclass, used in the Environment class's doctests. Simply writes to 
    stdout to report when its draw() and simulate() functions are called. Has a single 
    Object* member to test the Environment's reference resolving functionality.

    See the doxygen documentation for more details.

    """
    __swig_setmethods__ = {}
    for _s in [Object]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, TestObject, name, value)
    __swig_getmethods__ = {}
    for _s in [Object]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, TestObject, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> TestObject"""
        this = _libtrafsim.new_TestObject(*args)
        try: self.this.append(this)
        except: self.this = this
    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.TestObject_getClassID(*args)

    def draw(*args):
        """draw(self)"""
        return _libtrafsim.TestObject_draw(*args)

    def simulate(*args):
        """simulate(self, float ticks) -> bool"""
        return _libtrafsim.TestObject_simulate(*args)

    def downcast(*args):
        """downcast(Object source) -> TestObject"""
        return _libtrafsim.TestObject_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
    __swig_setmethods__["reference"] = _libtrafsim.TestObject_reference_set
    __swig_getmethods__["reference"] = _libtrafsim.TestObject_reference_get
    if _newclass:reference = property(_libtrafsim.TestObject_reference_get, _libtrafsim.TestObject_reference_set)
    def __str__(*args):
        """__str__(self) -> string"""
        return _libtrafsim.TestObject___str__(*args)

    def parse(*args):
        """parse(string str) -> TestObject"""
        return _libtrafsim.TestObject_parse(*args)

    if _newclass:parse = staticmethod(parse)
    __swig_getmethods__["parse"] = lambda x: parse
    __swig_destroy__ = _libtrafsim.delete_TestObject
    __del__ = lambda self : None;
TestObject_swigregister = _libtrafsim.TestObject_swigregister
TestObject_swigregister(TestObject)

def TestObject_downcast(*args):
  """TestObject_downcast(Object source) -> TestObject"""
  return _libtrafsim.TestObject_downcast(*args)

def TestObject_parse(*args):
  """TestObject_parse(string str) -> TestObject"""
  return _libtrafsim.TestObject_parse(*args)

class Spline(Object):
    """Proxy of C++ Spline class"""
    __swig_setmethods__ = {}
    for _s in [Object]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, Spline, name, value)
    __swig_getmethods__ = {}
    for _s in [Object]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, Spline, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> Spline
        __init__(self, VectorArray points, unsigned int resolution=100) -> Spline
        __init__(self, VectorArray points) -> Spline
        __init__(self, Spline original, float displacement, float relativeResolution=0.05f, 
            bool reversed=False) -> Spline
        __init__(self, Spline original, float displacement, float relativeResolution=0.05f) -> Spline
        __init__(self, Spline original, float displacement) -> Spline
        __init__(self, JoinDescArray parts) -> Spline
        """
        this = _libtrafsim.new_Spline(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_Spline
    __del__ = lambda self : None;
    def initialize(*args):
        """
        initialize(self, VectorArray points, unsigned int resolution=100)
        initialize(self, VectorArray points)
        initialize(self, Spline original, float displacement, float relativeResolution=0.05f, 
            bool reversed=False)
        initialize(self, Spline original, float displacement, float relativeResolution=0.05f)
        initialize(self, Spline original, float displacement)
        initialize(self, JoinDescArray parts) -> CarPath
        """
        return _libtrafsim.Spline_initialize(*args)

    def cacheData(*args):
        """
        cacheData(self, bool regenerate=False)
        cacheData(self)
        """
        return _libtrafsim.Spline_cacheData(*args)

    def isCached(*args):
        """isCached(self) -> bool"""
        return _libtrafsim.Spline_isCached(*args)

    def evalCoordinate(*args):
        """evalCoordinate(self, float t) -> Vector"""
        return _libtrafsim.Spline_evalCoordinate(*args)

    def evalNormal(*args):
        """evalNormal(self, float t) -> Vector"""
        return _libtrafsim.Spline_evalNormal(*args)

    def evalCurvature(*args):
        """evalCurvature(self, float t) -> float"""
        return _libtrafsim.Spline_evalCurvature(*args)

    def evalLength(*args):
        """evalLength(self, float t1, float t2) -> float"""
        return _libtrafsim.Spline_evalLength(*args)

    def evalDisplacement(*args):
        """evalDisplacement(self, float t0, float dist) -> float"""
        return _libtrafsim.Spline_evalDisplacement(*args)

    def closestToPoint(*args):
        """
        closestToPoint(self, Vector point, unsigned int startIndex=0, unsigned int endIndex=-1) -> float
        closestToPoint(self, Vector point, unsigned int startIndex=0) -> float
        closestToPoint(self, Vector point) -> float
        """
        return _libtrafsim.Spline_closestToPoint(*args)

    def getInterference(*args):
        """
        getInterference(self, Spline otherSpline, std::vector<(TrafSim::SplineInterferenceDesc,std::allocator<(TrafSim::SplineInterferenceDesc)>)> result, 
            float threshold, 
            float subResolution=10.0f)
        getInterference(self, Spline otherSpline, std::vector<(TrafSim::SplineInterferenceDesc,std::allocator<(TrafSim::SplineInterferenceDesc)>)> result, 
            float threshold)
        """
        return _libtrafsim.Spline_getInterference(*args)

    def getResolution(*args):
        """getResolution(self) -> unsigned int"""
        return _libtrafsim.Spline_getResolution(*args)

    def getCoordinateData(*args):
        """getCoordinateData(self) -> Vector"""
        return _libtrafsim.Spline_getCoordinateData(*args)

    def getNormalData(*args):
        """getNormalData(self) -> Vector"""
        return _libtrafsim.Spline_getNormalData(*args)

    def getLengthData(*args):
        """getLengthData(self) -> float"""
        return _libtrafsim.Spline_getLengthData(*args)

    def getCurvatureData(*args):
        """getCurvatureData(self) -> float"""
        return _libtrafsim.Spline_getCurvatureData(*args)

    def _mapTCoord(*args):
        """_mapTCoord(self, float t) -> std::pair<(unsigned int,float)>"""
        return _libtrafsim.Spline__mapTCoord(*args)

    def downcast(*args):
        """downcast(Object source) -> Spline"""
        return _libtrafsim.Spline_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.Spline_getClassID(*args)

    def draw(*args):
        """
        draw(self)
        draw(self, float t0, float t1)
        """
        return _libtrafsim.Spline_draw(*args)

    __swig_setmethods__["points"] = _libtrafsim.Spline_points_set
    __swig_getmethods__["points"] = _libtrafsim.Spline_points_get
    if _newclass:points = property(_libtrafsim.Spline_points_get, _libtrafsim.Spline_points_set)
    __swig_setmethods__["color"] = _libtrafsim.Spline_color_set
    __swig_getmethods__["color"] = _libtrafsim.Spline_color_get
    if _newclass:color = property(_libtrafsim.Spline_color_get, _libtrafsim.Spline_color_set)
    def __str__(*args):
        """__str__(self) -> string"""
        return _libtrafsim.Spline___str__(*args)

    def parse(*args):
        """parse(string str) -> Spline"""
        return _libtrafsim.Spline_parse(*args)

    if _newclass:parse = staticmethod(parse)
    __swig_getmethods__["parse"] = lambda x: parse
Spline_swigregister = _libtrafsim.Spline_swigregister
Spline_swigregister(Spline)

def Spline_downcast(*args):
  """Spline_downcast(Object source) -> Spline"""
  return _libtrafsim.Spline_downcast(*args)

def Spline_parse(*args):
  """Spline_parse(string str) -> Spline"""
  return _libtrafsim.Spline_parse(*args)

class LaneInterferenceDesc(_object):
    """Proxy of C++ LaneInterferenceDesc class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, LaneInterferenceDesc, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, LaneInterferenceDesc, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> LaneInterferenceDesc
        __init__(self, LaneSegment _mySeg, LaneSegment _otherSeg, float _lengthRatio) -> LaneInterferenceDesc
        """
        this = _libtrafsim.new_LaneInterferenceDesc(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_setmethods__["mySeg"] = _libtrafsim.LaneInterferenceDesc_mySeg_set
    __swig_getmethods__["mySeg"] = _libtrafsim.LaneInterferenceDesc_mySeg_get
    if _newclass:mySeg = property(_libtrafsim.LaneInterferenceDesc_mySeg_get, _libtrafsim.LaneInterferenceDesc_mySeg_set)
    __swig_setmethods__["otherSeg"] = _libtrafsim.LaneInterferenceDesc_otherSeg_set
    __swig_getmethods__["otherSeg"] = _libtrafsim.LaneInterferenceDesc_otherSeg_get
    if _newclass:otherSeg = property(_libtrafsim.LaneInterferenceDesc_otherSeg_get, _libtrafsim.LaneInterferenceDesc_otherSeg_set)
    __swig_setmethods__["lengthRatio"] = _libtrafsim.LaneInterferenceDesc_lengthRatio_set
    __swig_getmethods__["lengthRatio"] = _libtrafsim.LaneInterferenceDesc_lengthRatio_get
    if _newclass:lengthRatio = property(_libtrafsim.LaneInterferenceDesc_lengthRatio_get, _libtrafsim.LaneInterferenceDesc_lengthRatio_set)
    __swig_destroy__ = _libtrafsim.delete_LaneInterferenceDesc
    __del__ = lambda self : None;
LaneInterferenceDesc_swigregister = _libtrafsim.LaneInterferenceDesc_swigregister
LaneInterferenceDesc_swigregister(LaneInterferenceDesc)

class Lane(Object):
    """Proxy of C++ Lane class"""
    __swig_setmethods__ = {}
    for _s in [Object]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, Lane, name, value)
    __swig_getmethods__ = {}
    for _s in [Object]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, Lane, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> Lane
        __init__(self, Spline spline) -> Lane
        """
        this = _libtrafsim.new_Lane(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_Lane
    __del__ = lambda self : None;
    def initialize(*args):
        """initialize(self, Spline spline)"""
        return _libtrafsim.Lane_initialize(*args)

    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.Lane_getClassID(*args)

    def downcast(*args):
        """downcast(Object source) -> Lane"""
        return _libtrafsim.Lane_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
    def simulate(*args):
        """simulate(self, float ticks) -> bool"""
        return _libtrafsim.Lane_simulate(*args)

    def draw(*args):
        """draw(self)"""
        return _libtrafsim.Lane_draw(*args)

    def addObject(*args):
        """
        addObject(self, Object object, float t=0.0f) -> bool
        addObject(self, Object object) -> bool
        """
        return _libtrafsim.Lane_addObject(*args)

    def removeObject(*args):
        """removeObject(self, Object object) -> bool"""
        return _libtrafsim.Lane_removeObject(*args)

    def setT(*args):
        """setT(self, Object object, float newT) -> bool"""
        return _libtrafsim.Lane_setT(*args)

    def getT(*args):
        """getT(self, Object object) -> float"""
        return _libtrafsim.Lane_getT(*args)

    def mapT(*args):
        """mapT(self, Lane otherLane, float t) -> float"""
        return _libtrafsim.Lane_mapT(*args)

    def mapTangent(*args):
        """mapTangent(self, Lane otherLane, float t, float mappedT) -> float"""
        return _libtrafsim.Lane_mapTangent(*args)

    def addInterference(*args):
        """
        addInterference(self, Lane otherLane, float threshold, unsigned int subResolution=10) -> bool
        addInterference(self, Lane otherLane, float threshold) -> bool
        """
        return _libtrafsim.Lane_addInterference(*args)

    def getInterferenceAfter(*args):
        """getInterferenceAfter(self, Lane otherLane, float t) -> LaneInterferenceDesc"""
        return _libtrafsim.Lane_getInterferenceAfter(*args)

    def interferesWith(*args):
        """interferesWith(self, Lane otherLane) -> bool"""
        return _libtrafsim.Lane_interferesWith(*args)

    def contains(*args):
        """contains(self, Lane otherLane) -> bool"""
        return _libtrafsim.Lane_contains(*args)

    def getObjectBefore(*args):
        """
        getObjectBefore(self, float t, string classID="object", bool interference=True) -> LaneObject
        getObjectBefore(self, float t, string classID="object") -> LaneObject
        getObjectBefore(self, float t) -> LaneObject
        """
        return _libtrafsim.Lane_getObjectBefore(*args)

    def getObjectAfter(*args):
        """
        getObjectAfter(self, float t, string classID="object", bool interference=True) -> LaneObject
        getObjectAfter(self, float t, string classID="object") -> LaneObject
        getObjectAfter(self, float t) -> LaneObject
        """
        return _libtrafsim.Lane_getObjectAfter(*args)

    __swig_setmethods__["spline"] = _libtrafsim.Lane_spline_set
    __swig_getmethods__["spline"] = _libtrafsim.Lane_spline_get
    if _newclass:spline = property(_libtrafsim.Lane_spline_get, _libtrafsim.Lane_spline_set)
Lane_swigregister = _libtrafsim.Lane_swigregister
Lane_swigregister(Lane)

def Lane_downcast(*args):
  """Lane_downcast(Object source) -> Lane"""
  return _libtrafsim.Lane_downcast(*args)

class MultiLane(Lane):
    """Proxy of C++ MultiLane class"""
    __swig_setmethods__ = {}
    for _s in [Lane]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, MultiLane, name, value)
    __swig_getmethods__ = {}
    for _s in [Lane]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, MultiLane, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> MultiLane
        __init__(self, LaneSegmentArray subLanes) -> MultiLane
        """
        this = _libtrafsim.new_MultiLane(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_MultiLane
    __del__ = lambda self : None;
    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.MultiLane_getClassID(*args)

    def initialize(*args):
        """initialize(self, LaneSegmentArray subLanes)"""
        return _libtrafsim.MultiLane_initialize(*args)

    def downcast(*args):
        """downcast(Object source) -> MultiLane"""
        return _libtrafsim.MultiLane_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
    def simulate(*args):
        """simulate(self, float ticks) -> bool"""
        return _libtrafsim.MultiLane_simulate(*args)

    def draw(*args):
        """draw(self)"""
        return _libtrafsim.MultiLane_draw(*args)

    def addObject(*args):
        """
        addObject(self, Object object, float t=0.0f) -> bool
        addObject(self, Object object) -> bool
        """
        return _libtrafsim.MultiLane_addObject(*args)

    def removeObject(*args):
        """removeObject(self, Object object) -> bool"""
        return _libtrafsim.MultiLane_removeObject(*args)

    def setT(*args):
        """setT(self, Object object, float newT) -> bool"""
        return _libtrafsim.MultiLane_setT(*args)

    def getT(*args):
        """getT(self, Object object) -> float"""
        return _libtrafsim.MultiLane_getT(*args)

    def mapT(*args):
        """
        mapT(self, Lane otherLane, float t) -> float
        mapT(self, MultiLane otherLane, float t) -> float
        """
        return _libtrafsim.MultiLane_mapT(*args)

    def addInterference(*args):
        """
        addInterference(self, Lane otherLane, float threshold, unsigned int subResolution=10) -> bool
        addInterference(self, Lane otherLane, float threshold) -> bool
        """
        return _libtrafsim.MultiLane_addInterference(*args)

    def getInterferenceAfter(*args):
        """getInterferenceAfter(self, Lane otherLane, float t) -> LaneInterferenceDesc"""
        return _libtrafsim.MultiLane_getInterferenceAfter(*args)

    def interferesWith(*args):
        """interferesWith(self, Lane otherLane) -> bool"""
        return _libtrafsim.MultiLane_interferesWith(*args)

    def contains(*args):
        """contains(self, Lane otherLane) -> bool"""
        return _libtrafsim.MultiLane_contains(*args)

    def getObjectBefore(*args):
        """
        getObjectBefore(self, float t, string classID="object", bool interference=True) -> LaneObject
        getObjectBefore(self, float t, string classID="object") -> LaneObject
        getObjectBefore(self, float t) -> LaneObject
        """
        return _libtrafsim.MultiLane_getObjectBefore(*args)

    def getObjectAfter(*args):
        """
        getObjectAfter(self, float t, string classID="object", bool interference=True) -> LaneObject
        getObjectAfter(self, float t, string classID="object") -> LaneObject
        getObjectAfter(self, float t) -> LaneObject
        """
        return _libtrafsim.MultiLane_getObjectAfter(*args)

MultiLane_swigregister = _libtrafsim.MultiLane_swigregister
MultiLane_swigregister(MultiLane)

def MultiLane_downcast(*args):
  """MultiLane_downcast(Object source) -> MultiLane"""
  return _libtrafsim.MultiLane_downcast(*args)

ROW_GO = _libtrafsim.ROW_GO
ROW_GAP = _libtrafsim.ROW_GAP
CROSS_LEFT = _libtrafsim.CROSS_LEFT
CROSS_RIGHT = _libtrafsim.CROSS_RIGHT
CROSS_STRAIGHT = _libtrafsim.CROSS_STRAIGHT
class ManeuverDestMap(_object):
    """Proxy of C++ ManeuverDestMap class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, ManeuverDestMap, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, ManeuverDestMap, name)
    __repr__ = _swig_repr
    __swig_setmethods__["data"] = _libtrafsim.ManeuverDestMap_data_set
    __swig_getmethods__["data"] = _libtrafsim.ManeuverDestMap_data_get
    if _newclass:data = property(_libtrafsim.ManeuverDestMap_data_get, _libtrafsim.ManeuverDestMap_data_set)
    def __init__(self, *args): 
        """__init__(self) -> ManeuverDestMap"""
        this = _libtrafsim.new_ManeuverDestMap(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_ManeuverDestMap
    __del__ = lambda self : None;
ManeuverDestMap_swigregister = _libtrafsim.ManeuverDestMap_swigregister
ManeuverDestMap_swigregister(ManeuverDestMap)

class ManeuverLaneMap(_object):
    """Proxy of C++ ManeuverLaneMap class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, ManeuverLaneMap, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, ManeuverLaneMap, name)
    __repr__ = _swig_repr
    __swig_setmethods__["data"] = _libtrafsim.ManeuverLaneMap_data_set
    __swig_getmethods__["data"] = _libtrafsim.ManeuverLaneMap_data_get
    if _newclass:data = property(_libtrafsim.ManeuverLaneMap_data_get, _libtrafsim.ManeuverLaneMap_data_set)
    def __init__(self, *args): 
        """__init__(self) -> ManeuverLaneMap"""
        this = _libtrafsim.new_ManeuverLaneMap(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_ManeuverLaneMap
    __del__ = lambda self : None;
ManeuverLaneMap_swigregister = _libtrafsim.ManeuverLaneMap_swigregister
ManeuverLaneMap_swigregister(ManeuverLaneMap)

class IntersectionGeometryParams(_object):
    """Proxy of C++ IntersectionGeometryParams class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, IntersectionGeometryParams, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, IntersectionGeometryParams, name)
    __repr__ = _swig_repr
    __swig_setmethods__["narrowing"] = _libtrafsim.IntersectionGeometryParams_narrowing_set
    __swig_getmethods__["narrowing"] = _libtrafsim.IntersectionGeometryParams_narrowing_get
    if _newclass:narrowing = property(_libtrafsim.IntersectionGeometryParams_narrowing_get, _libtrafsim.IntersectionGeometryParams_narrowing_set)
    __swig_setmethods__["sharpness"] = _libtrafsim.IntersectionGeometryParams_sharpness_set
    __swig_getmethods__["sharpness"] = _libtrafsim.IntersectionGeometryParams_sharpness_get
    if _newclass:sharpness = property(_libtrafsim.IntersectionGeometryParams_sharpness_get, _libtrafsim.IntersectionGeometryParams_sharpness_set)
    __swig_setmethods__["leftTightening"] = _libtrafsim.IntersectionGeometryParams_leftTightening_set
    __swig_getmethods__["leftTightening"] = _libtrafsim.IntersectionGeometryParams_leftTightening_get
    if _newclass:leftTightening = property(_libtrafsim.IntersectionGeometryParams_leftTightening_get, _libtrafsim.IntersectionGeometryParams_leftTightening_set)
    def __init__(self, *args): 
        """__init__(self) -> IntersectionGeometryParams"""
        this = _libtrafsim.new_IntersectionGeometryParams(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_IntersectionGeometryParams
    __del__ = lambda self : None;
IntersectionGeometryParams_swigregister = _libtrafsim.IntersectionGeometryParams_swigregister
IntersectionGeometryParams_swigregister(IntersectionGeometryParams)

class Intersection(Object):
    """Proxy of C++ Intersection class"""
    __swig_setmethods__ = {}
    for _s in [Object]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, Intersection, name, value)
    __swig_getmethods__ = {}
    for _s in [Object]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, Intersection, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> Intersection"""
        this = _libtrafsim.new_Intersection(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_Intersection
    __del__ = lambda self : None;
    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.Intersection_getClassID(*args)

    def draw(*args):
        """draw(self)"""
        return _libtrafsim.Intersection_draw(*args)

    def hookStart(*args):
        """hookStart(self, Road road) -> bool"""
        return _libtrafsim.Intersection_hookStart(*args)

    def hookEnd(*args):
        """hookEnd(self, Road road) -> bool"""
        return _libtrafsim.Intersection_hookEnd(*args)

    def blockManeuver(*args):
        """blockManeuver(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> bool"""
        return _libtrafsim.Intersection_blockManeuver(*args)

    def unblockManeuver(*args):
        """unblockManeuver(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> bool"""
        return _libtrafsim.Intersection_unblockManeuver(*args)

    def computeGeometry(*args):
        """computeGeometry(self, Environment env) -> bool"""
        return _libtrafsim.Intersection_computeGeometry(*args)

    def getDestination(*args):
        """getDestination(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> RoadLane"""
        return _libtrafsim.Intersection_getDestination(*args)

    def getLane(*args):
        """getLane(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> Lane"""
        return _libtrafsim.Intersection_getLane(*args)

    def setStop(*args):
        """setStop(self, Road road, bool stopSign) -> bool"""
        return _libtrafsim.Intersection_setStop(*args)

    def isStop(*args):
        """isStop(self, Road road) -> bool"""
        return _libtrafsim.Intersection_isStop(*args)

    def rightOfWay(*args):
        """rightOfWay(self, Car thisCar, Road road, Car followCar) -> int"""
        return _libtrafsim.Intersection_rightOfWay(*args)

    def exitIntersection(*args):
        """exitIntersection(self, Car thisCar)"""
        return _libtrafsim.Intersection_exitIntersection(*args)

    def getOpposingTraffic(*args):
        """
        getOpposingTraffic(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver, 
            std::vector<(p.TrafSim::Lane,std::allocator<(p.TrafSim::Lane)>)> result)
        """
        return _libtrafsim.Intersection_getOpposingTraffic(*args)

    def getBoundaries(*args):
        """getBoundaries(self, SplineArray boundsArray)"""
        return _libtrafsim.Intersection_getBoundaries(*args)

    def getParams(*args):
        """getParams(self) -> IntersectionGeometryParams"""
        return _libtrafsim.Intersection_getParams(*args)

    def setParams(*args):
        """setParams(self, IntersectionGeometryParams newParams)"""
        return _libtrafsim.Intersection_setParams(*args)

    def downcast(*args):
        """downcast(Object source) -> Intersection"""
        return _libtrafsim.Intersection_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
Intersection_swigregister = _libtrafsim.Intersection_swigregister
Intersection_swigregister(Intersection)

def Intersection_downcast(*args):
  """Intersection_downcast(Object source) -> Intersection"""
  return _libtrafsim.Intersection_downcast(*args)

class DeadEnd(Intersection):
    """Proxy of C++ DeadEnd class"""
    __swig_setmethods__ = {}
    for _s in [Intersection]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, DeadEnd, name, value)
    __swig_getmethods__ = {}
    for _s in [Intersection]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, DeadEnd, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> DeadEnd"""
        this = _libtrafsim.new_DeadEnd(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_DeadEnd
    __del__ = lambda self : None;
    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.DeadEnd_getClassID(*args)

DeadEnd_swigregister = _libtrafsim.DeadEnd_swigregister
DeadEnd_swigregister(DeadEnd)

class CarProperties(_object):
    """Proxy of C++ CarProperties class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, CarProperties, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, CarProperties, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> CarProperties"""
        this = _libtrafsim.new_CarProperties(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_setmethods__["size"] = _libtrafsim.CarProperties_size_set
    __swig_getmethods__["size"] = _libtrafsim.CarProperties_size_get
    if _newclass:size = property(_libtrafsim.CarProperties_size_get, _libtrafsim.CarProperties_size_set)
    __swig_setmethods__["color"] = _libtrafsim.CarProperties_color_set
    __swig_getmethods__["color"] = _libtrafsim.CarProperties_color_get
    if _newclass:color = property(_libtrafsim.CarProperties_color_get, _libtrafsim.CarProperties_color_set)
    __swig_setmethods__["comfyAccel"] = _libtrafsim.CarProperties_comfyAccel_set
    __swig_getmethods__["comfyAccel"] = _libtrafsim.CarProperties_comfyAccel_get
    if _newclass:comfyAccel = property(_libtrafsim.CarProperties_comfyAccel_get, _libtrafsim.CarProperties_comfyAccel_set)
    __swig_setmethods__["maxAccel"] = _libtrafsim.CarProperties_maxAccel_set
    __swig_getmethods__["maxAccel"] = _libtrafsim.CarProperties_maxAccel_get
    if _newclass:maxAccel = property(_libtrafsim.CarProperties_maxAccel_get, _libtrafsim.CarProperties_maxAccel_set)
    __swig_setmethods__["maxAngularAccel"] = _libtrafsim.CarProperties_maxAngularAccel_set
    __swig_getmethods__["maxAngularAccel"] = _libtrafsim.CarProperties_maxAngularAccel_get
    if _newclass:maxAngularAccel = property(_libtrafsim.CarProperties_maxAngularAccel_get, _libtrafsim.CarProperties_maxAngularAccel_set)
    __swig_setmethods__["safetyTime"] = _libtrafsim.CarProperties_safetyTime_set
    __swig_getmethods__["safetyTime"] = _libtrafsim.CarProperties_safetyTime_get
    if _newclass:safetyTime = property(_libtrafsim.CarProperties_safetyTime_get, _libtrafsim.CarProperties_safetyTime_set)
    __swig_setmethods__["minDistance"] = _libtrafsim.CarProperties_minDistance_set
    __swig_getmethods__["minDistance"] = _libtrafsim.CarProperties_minDistance_get
    if _newclass:minDistance = property(_libtrafsim.CarProperties_minDistance_get, _libtrafsim.CarProperties_minDistance_set)
    __swig_setmethods__["carWaitFactor"] = _libtrafsim.CarProperties_carWaitFactor_set
    __swig_getmethods__["carWaitFactor"] = _libtrafsim.CarProperties_carWaitFactor_get
    if _newclass:carWaitFactor = property(_libtrafsim.CarProperties_carWaitFactor_get, _libtrafsim.CarProperties_carWaitFactor_set)
    __swig_setmethods__["gapAcceptFactor"] = _libtrafsim.CarProperties_gapAcceptFactor_set
    __swig_getmethods__["gapAcceptFactor"] = _libtrafsim.CarProperties_gapAcceptFactor_get
    if _newclass:gapAcceptFactor = property(_libtrafsim.CarProperties_gapAcceptFactor_get, _libtrafsim.CarProperties_gapAcceptFactor_set)
    __swig_setmethods__["accelDelta"] = _libtrafsim.CarProperties_accelDelta_set
    __swig_getmethods__["accelDelta"] = _libtrafsim.CarProperties_accelDelta_get
    if _newclass:accelDelta = property(_libtrafsim.CarProperties_accelDelta_get, _libtrafsim.CarProperties_accelDelta_set)
    __swig_setmethods__["curveEvalInterval"] = _libtrafsim.CarProperties_curveEvalInterval_set
    __swig_getmethods__["curveEvalInterval"] = _libtrafsim.CarProperties_curveEvalInterval_get
    if _newclass:curveEvalInterval = property(_libtrafsim.CarProperties_curveEvalInterval_get, _libtrafsim.CarProperties_curveEvalInterval_set)
    __swig_destroy__ = _libtrafsim.delete_CarProperties
    __del__ = lambda self : None;
CarProperties_swigregister = _libtrafsim.CarProperties_swigregister
CarProperties_swigregister(CarProperties)

class CarInterface(Object):
    """Proxy of C++ CarInterface class"""
    __swig_setmethods__ = {}
    for _s in [Object]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, CarInterface, name, value)
    __swig_getmethods__ = {}
    for _s in [Object]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, CarInterface, name)
    def __init__(self): raise AttributeError, "No constructor defined"
    __repr__ = _swig_repr
    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.CarInterface_getClassID(*args)

    def getLaneVelocity(*args):
        """getLaneVelocity(self) -> float"""
        return _libtrafsim.CarInterface_getLaneVelocity(*args)

    def getCurrentLane(*args):
        """getCurrentLane(self) -> Lane"""
        return _libtrafsim.CarInterface_getCurrentLane(*args)

    def setProperties(*args):
        """setProperties(self, CarProperties props)"""
        return _libtrafsim.CarInterface_setProperties(*args)

    def getProperties(*args):
        """getProperties(self) -> CarProperties"""
        return _libtrafsim.CarInterface_getProperties(*args)

    __swig_destroy__ = _libtrafsim.delete_CarInterface
    __del__ = lambda self : None;
CarInterface_swigregister = _libtrafsim.CarInterface_swigregister
CarInterface_swigregister(CarInterface)

class Car(CarInterface):
    """Proxy of C++ Car class"""
    __swig_setmethods__ = {}
    for _s in [CarInterface]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, Car, name, value)
    __swig_getmethods__ = {}
    for _s in [CarInterface]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, Car, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> Car"""
        this = _libtrafsim.new_Car(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_Car
    __del__ = lambda self : None;
    def draw(*args):
        """draw(self)"""
        return _libtrafsim.Car_draw(*args)

    def simulate(*args):
        """simulate(self, float ticks) -> bool"""
        return _libtrafsim.Car_simulate(*args)

    def switchLane(*args):
        """
        switchLane(self, Road newRoad, unsigned int newLaneIndex, float startT=0.0f)
        switchLane(self, Road newRoad, unsigned int newLaneIndex)
        """
        return _libtrafsim.Car_switchLane(*args)

    def setPath(*args):
        """
        setPath(self, std::vector<(TrafSim::CrossManeuver,std::allocator<(TrafSim::CrossManeuver)>)> path)
        setPath(self, CarPath path)
        """
        return _libtrafsim.Car_setPath(*args)

    def getLaneVelocity(*args):
        """getLaneVelocity(self) -> float"""
        return _libtrafsim.Car_getLaneVelocity(*args)

    def getCurrentLane(*args):
        """getCurrentLane(self) -> Lane"""
        return _libtrafsim.Car_getCurrentLane(*args)

    def downcast(*args):
        """downcast(Object source) -> Car"""
        return _libtrafsim.Car_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
Car_swigregister = _libtrafsim.Car_swigregister
Car_swigregister(Car)

def Car_downcast(*args):
  """Car_downcast(Object source) -> Car"""
  return _libtrafsim.Car_downcast(*args)

class ExternalCar(CarInterface):
    """Proxy of C++ ExternalCar class"""
    __swig_setmethods__ = {}
    for _s in [CarInterface]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, ExternalCar, name, value)
    __swig_getmethods__ = {}
    for _s in [CarInterface]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, ExternalCar, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> ExternalCar"""
        this = _libtrafsim.new_ExternalCar(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_ExternalCar
    __del__ = lambda self : None;
    def initialize(*args):
        """
        initialize(self, Environment env, string gridName="")
        initialize(self, Environment env)
        """
        return _libtrafsim.ExternalCar_initialize(*args)

    def draw(*args):
        """draw(self)"""
        return _libtrafsim.ExternalCar_draw(*args)

    def simulate(*args):
        """simulate(self, float ticks) -> bool"""
        return _libtrafsim.ExternalCar_simulate(*args)

    def getLaneVelocity(*args):
        """getLaneVelocity(self) -> float"""
        return _libtrafsim.ExternalCar_getLaneVelocity(*args)

    def getCurrentLane(*args):
        """getCurrentLane(self) -> Lane"""
        return _libtrafsim.ExternalCar_getCurrentLane(*args)

    def getLaneTCoord(*args):
        """getLaneTCoord(self) -> float"""
        return _libtrafsim.ExternalCar_getLaneTCoord(*args)

    def setWorldPosition(*args):
        """setWorldPosition(self, Vector position)"""
        return _libtrafsim.ExternalCar_setWorldPosition(*args)

    def getWorldPosition(*args):
        """getWorldPosition(self) -> Vector"""
        return _libtrafsim.ExternalCar_getWorldPosition(*args)

    def setVelocity(*args):
        """setVelocity(self, Vector velVector)"""
        return _libtrafsim.ExternalCar_setVelocity(*args)

    def getVelocity(*args):
        """getVelocity(self) -> Vector"""
        return _libtrafsim.ExternalCar_getVelocity(*args)

    def setAcceleration(*args):
        """setAcceleration(self, Vector accelVector)"""
        return _libtrafsim.ExternalCar_setAcceleration(*args)

    def getAcceleration(*args):
        """getAcceleration(self) -> Vector"""
        return _libtrafsim.ExternalCar_getAcceleration(*args)

    def downcast(*args):
        """downcast(Object source) -> ExternalCar"""
        return _libtrafsim.ExternalCar_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
ExternalCar_swigregister = _libtrafsim.ExternalCar_swigregister
ExternalCar_swigregister(ExternalCar)

def ExternalCar_downcast(*args):
  """ExternalCar_downcast(Object source) -> ExternalCar"""
  return _libtrafsim.ExternalCar_downcast(*args)

CF_FAIL_WAIT = _libtrafsim.CF_FAIL_WAIT
CF_FAIL_SKIP = _libtrafsim.CF_FAIL_SKIP
CF_FAIL_OVERLAP = _libtrafsim.CF_FAIL_OVERLAP
class CarFactory(Object):
    """Proxy of C++ CarFactory class"""
    __swig_setmethods__ = {}
    for _s in [Object]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, CarFactory, name, value)
    __swig_getmethods__ = {}
    for _s in [Object]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, CarFactory, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> CarFactory"""
        this = _libtrafsim.new_CarFactory(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_CarFactory
    __del__ = lambda self : None;
    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.CarFactory_getClassID(*args)

    def draw(*args):
        """draw(self)"""
        return _libtrafsim.CarFactory_draw(*args)

    def simulate(*args):
        """simulate(self, float ticks) -> bool"""
        return _libtrafsim.CarFactory_simulate(*args)

    def setRemainingCars(*args):
        """setRemainingCars(self, int nCars)"""
        return _libtrafsim.CarFactory_setRemainingCars(*args)

    def getRemainingCars(*args):
        """getRemainingCars(self) -> int"""
        return _libtrafsim.CarFactory_getRemainingCars(*args)

    def setCreationRate(*args):
        """setCreationRate(self, double rate)"""
        return _libtrafsim.CarFactory_setCreationRate(*args)

    def getCreationRate(*args):
        """getCreationRate(self) -> double"""
        return _libtrafsim.CarFactory_getCreationRate(*args)

    def setCRRandomDelta(*args):
        """setCRRandomDelta(self, double delta)"""
        return _libtrafsim.CarFactory_setCRRandomDelta(*args)

    def getCRRandomDelta(*args):
        """getCRRandomDelta(self) -> double"""
        return _libtrafsim.CarFactory_getCRRandomDelta(*args)

    def setCarProperties(*args):
        """
        setCarProperties(self, CarProperties low, CarProperties high)
        setCarProperties(self, CarProperties props)
        """
        return _libtrafsim.CarFactory_setCarProperties(*args)

    def getCarProperties(*args):
        """getCarProperties(self) -> std::pair<(TrafSim::CarProperties,TrafSim::CarProperties)>"""
        return _libtrafsim.CarFactory_getCarProperties(*args)

    def setRandomSeed(*args):
        """setRandomSeed(self, unsigned int seed)"""
        return _libtrafsim.CarFactory_setRandomSeed(*args)

    def getRandomSeed(*args):
        """getRandomSeed(self) -> unsigned int"""
        return _libtrafsim.CarFactory_getRandomSeed(*args)

    def setFailMode(*args):
        """setFailMode(self, CarFactoryFailMode mode)"""
        return _libtrafsim.CarFactory_setFailMode(*args)

    def getFailMode(*args):
        """getFailMode(self) -> int"""
        return _libtrafsim.CarFactory_getFailMode(*args)

    def setPath(*args):
        """
        setPath(self, std::vector<(TrafSim::CrossManeuver,std::allocator<(TrafSim::CrossManeuver)>)> path)
        setPath(self, CarPath path)
        """
        return _libtrafsim.CarFactory_setPath(*args)

    def getPath(*args):
        """getPath(self) -> std::vector<(TrafSim::CrossManeuver,std::allocator<(TrafSim::CrossManeuver)>)>"""
        return _libtrafsim.CarFactory_getPath(*args)

    def setPosition(*args):
        """setPosition(self, Road road, unsigned int laneIndex, float t)"""
        return _libtrafsim.CarFactory_setPosition(*args)

    def getRoad(*args):
        """getRoad(self) -> Road"""
        return _libtrafsim.CarFactory_getRoad(*args)

    def getLaneIndex(*args):
        """getLaneIndex(self) -> unsigned int"""
        return _libtrafsim.CarFactory_getLaneIndex(*args)

    def getT(*args):
        """getT(self) -> float"""
        return _libtrafsim.CarFactory_getT(*args)

    def downcast(*args):
        """downcast(Object source) -> CarFactory"""
        return _libtrafsim.CarFactory_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
CarFactory_swigregister = _libtrafsim.CarFactory_swigregister
CarFactory_swigregister(CarFactory)

def CarFactory_downcast(*args):
  """CarFactory_downcast(Object source) -> CarFactory"""
  return _libtrafsim.CarFactory_downcast(*args)

class Road(Object):
    """Proxy of C++ Road class"""
    __swig_setmethods__ = {}
    for _s in [Object]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, Road, name, value)
    __swig_getmethods__ = {}
    for _s in [Object]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, Road, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> Road
        __init__(self, Environment env, Spline source, unsigned int numForwardLanes, 
            unsigned int numReverseLanes, float laneWidth=5.0f) -> Road
        __init__(self, Environment env, Spline source, unsigned int numForwardLanes, 
            unsigned int numReverseLanes) -> Road
        """
        this = _libtrafsim.new_Road(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_Road
    __del__ = lambda self : None;
    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.Road_getClassID(*args)

    def downcast(*args):
        """downcast(Object source) -> Road"""
        return _libtrafsim.Road_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
    def draw(*args):
        """draw(self)"""
        return _libtrafsim.Road_draw(*args)

    def initialize(*args):
        """
        initialize(self, Environment env, Spline source, unsigned int numForwardLanes, 
            unsigned int numReverseLanes, float laneWidth=5.0f)
        initialize(self, Environment env, Spline source, unsigned int numForwardLanes, 
            unsigned int numReverseLanes)
        """
        return _libtrafsim.Road_initialize(*args)

    def getLane(*args):
        """getLane(self, unsigned int index) -> Lane"""
        return _libtrafsim.Road_getLane(*args)

    def getForwardLane(*args):
        """getForwardLane(self, unsigned int index) -> Lane"""
        return _libtrafsim.Road_getForwardLane(*args)

    def getReverseLane(*args):
        """getReverseLane(self, unsigned int index) -> Lane"""
        return _libtrafsim.Road_getReverseLane(*args)

    def numLanes(*args):
        """numLanes(self) -> unsigned int"""
        return _libtrafsim.Road_numLanes(*args)

    def numForwardLanes(*args):
        """numForwardLanes(self) -> unsigned int"""
        return _libtrafsim.Road_numForwardLanes(*args)

    def numReverseLanes(*args):
        """numReverseLanes(self) -> unsigned int"""
        return _libtrafsim.Road_numReverseLanes(*args)

    def isReverse(*args):
        """isReverse(self, unsigned int lane) -> bool"""
        return _libtrafsim.Road_isReverse(*args)

    def getSpline(*args):
        """getSpline(self) -> Spline"""
        return _libtrafsim.Road_getSpline(*args)

    def getStart(*args):
        """getStart(self) -> Intersection"""
        return _libtrafsim.Road_getStart(*args)

    def getEnd(*args):
        """getEnd(self) -> Intersection"""
        return _libtrafsim.Road_getEnd(*args)

    def setStart(*args):
        """setStart(self, Intersection newStart)"""
        return _libtrafsim.Road_setStart(*args)

    def setEnd(*args):
        """setEnd(self, Intersection newEnd)"""
        return _libtrafsim.Road_setEnd(*args)

    def getLaneWidth(*args):
        """getLaneWidth(self) -> float"""
        return _libtrafsim.Road_getLaneWidth(*args)

    def getSpeedLimit(*args):
        """getSpeedLimit(self) -> float"""
        return _libtrafsim.Road_getSpeedLimit(*args)

    def setSpeedLimit(*args):
        """setSpeedLimit(self, float limit)"""
        return _libtrafsim.Road_setSpeedLimit(*args)

    def getBoundary(*args):
        """
        getBoundary(self, float t, int side, bool reversed=False) -> Vector
        getBoundary(self, float t, int side) -> Vector
        """
        return _libtrafsim.Road_getBoundary(*args)

Road_swigregister = _libtrafsim.Road_swigregister
Road_swigregister(Road)

def Road_downcast(*args):
  """Road_downcast(Object source) -> Road"""
  return _libtrafsim.Road_downcast(*args)

class XIntersection(Intersection):
    """Proxy of C++ XIntersection class"""
    __swig_setmethods__ = {}
    for _s in [Intersection]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, XIntersection, name, value)
    __swig_getmethods__ = {}
    for _s in [Intersection]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, XIntersection, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> XIntersection"""
        this = _libtrafsim.new_XIntersection(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_XIntersection
    __del__ = lambda self : None;
    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.XIntersection_getClassID(*args)

    def draw(*args):
        """draw(self)"""
        return _libtrafsim.XIntersection_draw(*args)

    def hookStart(*args):
        """hookStart(self, Road road) -> bool"""
        return _libtrafsim.XIntersection_hookStart(*args)

    def hookEnd(*args):
        """hookEnd(self, Road road) -> bool"""
        return _libtrafsim.XIntersection_hookEnd(*args)

    def blockManeuver(*args):
        """blockManeuver(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> bool"""
        return _libtrafsim.XIntersection_blockManeuver(*args)

    def unblockManeuver(*args):
        """unblockManeuver(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> bool"""
        return _libtrafsim.XIntersection_unblockManeuver(*args)

    def computeGeometry(*args):
        """computeGeometry(self, Environment env) -> bool"""
        return _libtrafsim.XIntersection_computeGeometry(*args)

    def getDestination(*args):
        """getDestination(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> RoadLane"""
        return _libtrafsim.XIntersection_getDestination(*args)

    def getLane(*args):
        """getLane(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> Lane"""
        return _libtrafsim.XIntersection_getLane(*args)

    def setStop(*args):
        """setStop(self, Road road, bool stopSign) -> bool"""
        return _libtrafsim.XIntersection_setStop(*args)

    def isStop(*args):
        """isStop(self, Road road) -> bool"""
        return _libtrafsim.XIntersection_isStop(*args)

    def rightOfWay(*args):
        """rightOfWay(self, Car thisCar, Road road, Car followCar) -> int"""
        return _libtrafsim.XIntersection_rightOfWay(*args)

    def exitIntersection(*args):
        """exitIntersection(self, Car thisCar)"""
        return _libtrafsim.XIntersection_exitIntersection(*args)

    def getOpposingTraffic(*args):
        """
        getOpposingTraffic(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver, 
            std::vector<(p.TrafSim::Lane,std::allocator<(p.TrafSim::Lane)>)> result)
        """
        return _libtrafsim.XIntersection_getOpposingTraffic(*args)

    def getBoundaries(*args):
        """getBoundaries(self, SplineArray boundsArray)"""
        return _libtrafsim.XIntersection_getBoundaries(*args)

    def downcast(*args):
        """downcast(Object source) -> XIntersection"""
        return _libtrafsim.XIntersection_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
XIntersection_swigregister = _libtrafsim.XIntersection_swigregister
XIntersection_swigregister(XIntersection)

def XIntersection_downcast(*args):
  """XIntersection_downcast(Object source) -> XIntersection"""
  return _libtrafsim.XIntersection_downcast(*args)

class YIntersection(Intersection):
    """Proxy of C++ YIntersection class"""
    __swig_setmethods__ = {}
    for _s in [Intersection]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, YIntersection, name, value)
    __swig_getmethods__ = {}
    for _s in [Intersection]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, YIntersection, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> YIntersection"""
        this = _libtrafsim.new_YIntersection(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_YIntersection
    __del__ = lambda self : None;
    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.YIntersection_getClassID(*args)

    def draw(*args):
        """draw(self)"""
        return _libtrafsim.YIntersection_draw(*args)

    def hookStart(*args):
        """hookStart(self, Road road) -> bool"""
        return _libtrafsim.YIntersection_hookStart(*args)

    def hookEnd(*args):
        """hookEnd(self, Road road) -> bool"""
        return _libtrafsim.YIntersection_hookEnd(*args)

    def blockManeuver(*args):
        """blockManeuver(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> bool"""
        return _libtrafsim.YIntersection_blockManeuver(*args)

    def unblockManeuver(*args):
        """unblockManeuver(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> bool"""
        return _libtrafsim.YIntersection_unblockManeuver(*args)

    def computeGeometry(*args):
        """computeGeometry(self, Environment env) -> bool"""
        return _libtrafsim.YIntersection_computeGeometry(*args)

    def getDestination(*args):
        """getDestination(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> RoadLane"""
        return _libtrafsim.YIntersection_getDestination(*args)

    def getLane(*args):
        """getLane(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver) -> Lane"""
        return _libtrafsim.YIntersection_getLane(*args)

    def setStop(*args):
        """setStop(self, Road road, bool stopSign) -> bool"""
        return _libtrafsim.YIntersection_setStop(*args)

    def isStop(*args):
        """isStop(self, Road road) -> bool"""
        return _libtrafsim.YIntersection_isStop(*args)

    def rightOfWay(*args):
        """rightOfWay(self, Car thisCar, Road road, Car followCar) -> int"""
        return _libtrafsim.YIntersection_rightOfWay(*args)

    def exitIntersection(*args):
        """exitIntersection(self, Car thisCar)"""
        return _libtrafsim.YIntersection_exitIntersection(*args)

    def getOpposingTraffic(*args):
        """
        getOpposingTraffic(self, Road startRoad, unsigned int startLane, CrossManeuver maneuver, 
            std::vector<(p.TrafSim::Lane,std::allocator<(p.TrafSim::Lane)>)> result)
        """
        return _libtrafsim.YIntersection_getOpposingTraffic(*args)

    def getBoundaries(*args):
        """getBoundaries(self, SplineArray boundsArray)"""
        return _libtrafsim.YIntersection_getBoundaries(*args)

    def downcast(*args):
        """downcast(Object source) -> YIntersection"""
        return _libtrafsim.YIntersection_downcast(*args)

    if _newclass:downcast = staticmethod(downcast)
    __swig_getmethods__["downcast"] = lambda x: downcast
YIntersection_swigregister = _libtrafsim.YIntersection_swigregister
YIntersection_swigregister(YIntersection)

def YIntersection_downcast(*args):
  """YIntersection_downcast(Object source) -> YIntersection"""
  return _libtrafsim.YIntersection_downcast(*args)

class LaneGrid(Object):
    """Proxy of C++ LaneGrid class"""
    __swig_setmethods__ = {}
    for _s in [Object]: __swig_setmethods__.update(_s.__swig_setmethods__)
    __setattr__ = lambda self, name, value: _swig_setattr(self, LaneGrid, name, value)
    __swig_getmethods__ = {}
    for _s in [Object]: __swig_getmethods__.update(_s.__swig_getmethods__)
    __getattr__ = lambda self, name: _swig_getattr(self, LaneGrid, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self) -> LaneGrid"""
        this = _libtrafsim.new_LaneGrid(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _libtrafsim.delete_LaneGrid
    __del__ = lambda self : None;
    def partition(*args):
        """
        partition(self, Environment env, float tileSize, Vector lowerLeft, 
            Vector topRight)
        """
        return _libtrafsim.LaneGrid_partition(*args)

    def getLanePosition(*args):
        """getLanePosition(self, Vector worldPos, Lane laneOut) -> float"""
        return _libtrafsim.LaneGrid_getLanePosition(*args)

    def getClassID(*args):
        """getClassID(self) -> string"""
        return _libtrafsim.LaneGrid_getClassID(*args)

LaneGrid_swigregister = _libtrafsim.LaneGrid_swigregister
LaneGrid_swigregister(LaneGrid)

class ObjectDictPair(_object):
    """Proxy of C++ ObjectDictPair class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, ObjectDictPair, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, ObjectDictPair, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> ObjectDictPair
        __init__(self, string __a, Object __b) -> ObjectDictPair
        __init__(self, ObjectDictPair __p) -> ObjectDictPair
        """
        this = _libtrafsim.new_ObjectDictPair(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_setmethods__["first"] = _libtrafsim.ObjectDictPair_first_set
    __swig_getmethods__["first"] = _libtrafsim.ObjectDictPair_first_get
    if _newclass:first = property(_libtrafsim.ObjectDictPair_first_get, _libtrafsim.ObjectDictPair_first_set)
    __swig_setmethods__["second"] = _libtrafsim.ObjectDictPair_second_set
    __swig_getmethods__["second"] = _libtrafsim.ObjectDictPair_second_get
    if _newclass:second = property(_libtrafsim.ObjectDictPair_second_get, _libtrafsim.ObjectDictPair_second_set)
    def __len__(self): return 2
    def __repr__(self): return str((self.first, self.second))
    def __getitem__(self, index): 
      if not (index % 2): 
        return self.first
      else:
        return self.second
    def __setitem__(self, index, val):
      if not (index % 2): 
        self.first = val
      else:
        self.second = val
    __swig_destroy__ = _libtrafsim.delete_ObjectDictPair
    __del__ = lambda self : None;
ObjectDictPair_swigregister = _libtrafsim.ObjectDictPair_swigregister
ObjectDictPair_swigregister(ObjectDictPair)

class ObjectDict(_object):
    """Proxy of C++ ObjectDict class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, ObjectDict, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, ObjectDict, name)
    __repr__ = _swig_repr
    def iterator(*args):
        """iterator(self, PyObject PYTHON_SELF) -> PySwigIterator"""
        return _libtrafsim.ObjectDict_iterator(*args)

    def __iter__(self): return self.iterator()
    def __nonzero__(*args):
        """__nonzero__(self) -> bool"""
        return _libtrafsim.ObjectDict___nonzero__(*args)

    def __len__(*args):
        """__len__(self) -> size_type"""
        return _libtrafsim.ObjectDict___len__(*args)

    def __getitem__(*args):
        """__getitem__(self, key_type key) -> mapped_type"""
        return _libtrafsim.ObjectDict___getitem__(*args)

    def __setitem__(*args):
        """__setitem__(self, key_type key, mapped_type x)"""
        return _libtrafsim.ObjectDict___setitem__(*args)

    def __delitem__(*args):
        """__delitem__(self, key_type key)"""
        return _libtrafsim.ObjectDict___delitem__(*args)

    def has_key(*args):
        """has_key(self, key_type key) -> bool"""
        return _libtrafsim.ObjectDict_has_key(*args)

    def keys(*args):
        """keys(self) -> PyObject"""
        return _libtrafsim.ObjectDict_keys(*args)

    def values(*args):
        """values(self) -> PyObject"""
        return _libtrafsim.ObjectDict_values(*args)

    def items(*args):
        """items(self) -> PyObject"""
        return _libtrafsim.ObjectDict_items(*args)

    def __contains__(*args):
        """__contains__(self, key_type key) -> bool"""
        return _libtrafsim.ObjectDict___contains__(*args)

    def key_iterator(*args):
        """key_iterator(self, PyObject PYTHON_SELF) -> PySwigIterator"""
        return _libtrafsim.ObjectDict_key_iterator(*args)

    def value_iterator(*args):
        """value_iterator(self, PyObject PYTHON_SELF) -> PySwigIterator"""
        return _libtrafsim.ObjectDict_value_iterator(*args)

    def __iter__(self): return self.key_iterator()
    def iterkeys(self): return self.key_iterator()
    def itervalues(self): return self.value_iterator()
    def iteritems(self): return self.iterator()
    def __init__(self, *args): 
        """
        __init__(self) -> ObjectDict
        __init__(self, ObjectDict ?) -> ObjectDict
        """
        this = _libtrafsim.new_ObjectDict(*args)
        try: self.this.append(this)
        except: self.this = this
    def empty(*args):
        """empty(self) -> bool"""
        return _libtrafsim.ObjectDict_empty(*args)

    def size(*args):
        """size(self) -> size_type"""
        return _libtrafsim.ObjectDict_size(*args)

    def clear(*args):
        """clear(self)"""
        return _libtrafsim.ObjectDict_clear(*args)

    def swap(*args):
        """swap(self, ObjectDict v)"""
        return _libtrafsim.ObjectDict_swap(*args)

    def get_allocator(*args):
        """get_allocator(self) -> allocator_type"""
        return _libtrafsim.ObjectDict_get_allocator(*args)

    def begin(*args):
        """
        begin(self) -> iterator
        begin(self) -> const_iterator
        """
        return _libtrafsim.ObjectDict_begin(*args)

    def end(*args):
        """
        end(self) -> iterator
        end(self) -> const_iterator
        """
        return _libtrafsim.ObjectDict_end(*args)

    def rbegin(*args):
        """
        rbegin(self) -> reverse_iterator
        rbegin(self) -> const_reverse_iterator
        """
        return _libtrafsim.ObjectDict_rbegin(*args)

    def rend(*args):
        """
        rend(self) -> reverse_iterator
        rend(self) -> const_reverse_iterator
        """
        return _libtrafsim.ObjectDict_rend(*args)

    def count(*args):
        """count(self, key_type x) -> size_type"""
        return _libtrafsim.ObjectDict_count(*args)

    def erase(*args):
        """
        erase(self, key_type x) -> size_type
        erase(self, iterator position)
        erase(self, iterator first, iterator last)
        """
        return _libtrafsim.ObjectDict_erase(*args)

    def find(*args):
        """
        find(self, key_type x) -> iterator
        find(self, key_type x) -> const_iterator
        """
        return _libtrafsim.ObjectDict_find(*args)

    def lower_bound(*args):
        """
        lower_bound(self, key_type x) -> iterator
        lower_bound(self, key_type x) -> const_iterator
        """
        return _libtrafsim.ObjectDict_lower_bound(*args)

    def upper_bound(*args):
        """
        upper_bound(self, key_type x) -> iterator
        upper_bound(self, key_type x) -> const_iterator
        """
        return _libtrafsim.ObjectDict_upper_bound(*args)

    __swig_destroy__ = _libtrafsim.delete_ObjectDict
    __del__ = lambda self : None;
ObjectDict_swigregister = _libtrafsim.ObjectDict_swigregister
ObjectDict_swigregister(ObjectDict)

class VectorArray(_object):
    """Proxy of C++ VectorArray class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, VectorArray, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, VectorArray, name)
    __repr__ = _swig_repr
    def iterator(*args):
        """iterator(self, PyObject PYTHON_SELF) -> PySwigIterator"""
        return _libtrafsim.VectorArray_iterator(*args)

    def __iter__(self): return self.iterator()
    def __nonzero__(*args):
        """__nonzero__(self) -> bool"""
        return _libtrafsim.VectorArray___nonzero__(*args)

    def __len__(*args):
        """__len__(self) -> size_type"""
        return _libtrafsim.VectorArray___len__(*args)

    def pop(*args):
        """pop(self) -> value_type"""
        return _libtrafsim.VectorArray_pop(*args)

    def __getslice__(*args):
        """__getslice__(self, difference_type i, difference_type j) -> VectorArray"""
        return _libtrafsim.VectorArray___getslice__(*args)

    def __setslice__(*args):
        """__setslice__(self, difference_type i, difference_type j, VectorArray v)"""
        return _libtrafsim.VectorArray___setslice__(*args)

    def __delslice__(*args):
        """__delslice__(self, difference_type i, difference_type j)"""
        return _libtrafsim.VectorArray___delslice__(*args)

    def __delitem__(*args):
        """__delitem__(self, difference_type i)"""
        return _libtrafsim.VectorArray___delitem__(*args)

    def __getitem__(*args):
        """__getitem__(self, difference_type i) -> value_type"""
        return _libtrafsim.VectorArray___getitem__(*args)

    def __setitem__(*args):
        """__setitem__(self, difference_type i, value_type x)"""
        return _libtrafsim.VectorArray___setitem__(*args)

    def append(*args):
        """append(self, value_type x)"""
        return _libtrafsim.VectorArray_append(*args)

    def empty(*args):
        """empty(self) -> bool"""
        return _libtrafsim.VectorArray_empty(*args)

    def size(*args):
        """size(self) -> size_type"""
        return _libtrafsim.VectorArray_size(*args)

    def clear(*args):
        """clear(self)"""
        return _libtrafsim.VectorArray_clear(*args)

    def swap(*args):
        """swap(self, VectorArray v)"""
        return _libtrafsim.VectorArray_swap(*args)

    def get_allocator(*args):
        """get_allocator(self) -> allocator_type"""
        return _libtrafsim.VectorArray_get_allocator(*args)

    def begin(*args):
        """
        begin(self) -> iterator
        begin(self) -> const_iterator
        """
        return _libtrafsim.VectorArray_begin(*args)

    def end(*args):
        """
        end(self) -> iterator
        end(self) -> const_iterator
        """
        return _libtrafsim.VectorArray_end(*args)

    def rbegin(*args):
        """
        rbegin(self) -> reverse_iterator
        rbegin(self) -> const_reverse_iterator
        """
        return _libtrafsim.VectorArray_rbegin(*args)

    def rend(*args):
        """
        rend(self) -> reverse_iterator
        rend(self) -> const_reverse_iterator
        """
        return _libtrafsim.VectorArray_rend(*args)

    def pop_back(*args):
        """pop_back(self)"""
        return _libtrafsim.VectorArray_pop_back(*args)

    def erase(*args):
        """
        erase(self, iterator pos) -> iterator
        erase(self, iterator first, iterator last) -> iterator
        """
        return _libtrafsim.VectorArray_erase(*args)

    def __init__(self, *args): 
        """
        __init__(self) -> VectorArray
        __init__(self, VectorArray ?) -> VectorArray
        __init__(self, size_type size) -> VectorArray
        __init__(self, size_type size, value_type value) -> VectorArray
        """
        this = _libtrafsim.new_VectorArray(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(*args):
        """push_back(self, value_type x)"""
        return _libtrafsim.VectorArray_push_back(*args)

    def front(*args):
        """front(self) -> value_type"""
        return _libtrafsim.VectorArray_front(*args)

    def back(*args):
        """back(self) -> value_type"""
        return _libtrafsim.VectorArray_back(*args)

    def assign(*args):
        """assign(self, size_type n, value_type x)"""
        return _libtrafsim.VectorArray_assign(*args)

    def resize(*args):
        """
        resize(self, size_type new_size)
        resize(self, size_type new_size, value_type x)
        """
        return _libtrafsim.VectorArray_resize(*args)

    def insert(*args):
        """
        insert(self, iterator pos, value_type x) -> iterator
        insert(self, iterator pos, size_type n, value_type x)
        """
        return _libtrafsim.VectorArray_insert(*args)

    def reserve(*args):
        """reserve(self, size_type n)"""
        return _libtrafsim.VectorArray_reserve(*args)

    def capacity(*args):
        """capacity(self) -> size_type"""
        return _libtrafsim.VectorArray_capacity(*args)

    __swig_destroy__ = _libtrafsim.delete_VectorArray
    __del__ = lambda self : None;
VectorArray_swigregister = _libtrafsim.VectorArray_swigregister
VectorArray_swigregister(VectorArray)

class LaneObject(_object):
    """Proxy of C++ LaneObject class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, LaneObject, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, LaneObject, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> LaneObject
        __init__(self, float __a, Object __b) -> LaneObject
        __init__(self, LaneObject __p) -> LaneObject
        """
        this = _libtrafsim.new_LaneObject(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_setmethods__["first"] = _libtrafsim.LaneObject_first_set
    __swig_getmethods__["first"] = _libtrafsim.LaneObject_first_get
    if _newclass:first = property(_libtrafsim.LaneObject_first_get, _libtrafsim.LaneObject_first_set)
    __swig_setmethods__["second"] = _libtrafsim.LaneObject_second_set
    __swig_getmethods__["second"] = _libtrafsim.LaneObject_second_get
    if _newclass:second = property(_libtrafsim.LaneObject_second_get, _libtrafsim.LaneObject_second_set)
    def __len__(self): return 2
    def __repr__(self): return str((self.first, self.second))
    def __getitem__(self, index): 
      if not (index % 2): 
        return self.first
      else:
        return self.second
    def __setitem__(self, index, val):
      if not (index % 2): 
        self.first = val
      else:
        self.second = val
    __swig_destroy__ = _libtrafsim.delete_LaneObject
    __del__ = lambda self : None;
LaneObject_swigregister = _libtrafsim.LaneObject_swigregister
LaneObject_swigregister(LaneObject)

class JoinDesc(_object):
    """Proxy of C++ JoinDesc class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, JoinDesc, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, JoinDesc, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> JoinDesc
        __init__(self, Spline _segment, float _t_start, float _t_end) -> JoinDesc
        """
        this = _libtrafsim.new_JoinDesc(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_setmethods__["segment"] = _libtrafsim.JoinDesc_segment_set
    __swig_getmethods__["segment"] = _libtrafsim.JoinDesc_segment_get
    if _newclass:segment = property(_libtrafsim.JoinDesc_segment_get, _libtrafsim.JoinDesc_segment_set)
    __swig_setmethods__["segmentName"] = _libtrafsim.JoinDesc_segmentName_set
    __swig_getmethods__["segmentName"] = _libtrafsim.JoinDesc_segmentName_get
    if _newclass:segmentName = property(_libtrafsim.JoinDesc_segmentName_get, _libtrafsim.JoinDesc_segmentName_set)
    __swig_setmethods__["t_start"] = _libtrafsim.JoinDesc_t_start_set
    __swig_getmethods__["t_start"] = _libtrafsim.JoinDesc_t_start_get
    if _newclass:t_start = property(_libtrafsim.JoinDesc_t_start_get, _libtrafsim.JoinDesc_t_start_set)
    __swig_setmethods__["t_end"] = _libtrafsim.JoinDesc_t_end_set
    __swig_getmethods__["t_end"] = _libtrafsim.JoinDesc_t_end_get
    if _newclass:t_end = property(_libtrafsim.JoinDesc_t_end_get, _libtrafsim.JoinDesc_t_end_set)
    __swig_destroy__ = _libtrafsim.delete_JoinDesc
    __del__ = lambda self : None;
JoinDesc_swigregister = _libtrafsim.JoinDesc_swigregister
JoinDesc_swigregister(JoinDesc)

class LaneSegment(_object):
    """Proxy of C++ LaneSegment class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, LaneSegment, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, LaneSegment, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """
        __init__(self) -> LaneSegment
        __init__(self, Lane _segment, float _t_start, float _t_end) -> LaneSegment
        """
        this = _libtrafsim.new_LaneSegment(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_setmethods__["segment"] = _libtrafsim.LaneSegment_segment_set
    __swig_getmethods__["segment"] = _libtrafsim.LaneSegment_segment_get
    if _newclass:segment = property(_libtrafsim.LaneSegment_segment_get, _libtrafsim.LaneSegment_segment_set)
    __swig_setmethods__["segmentName"] = _libtrafsim.LaneSegment_segmentName_set
    __swig_getmethods__["segmentName"] = _libtrafsim.LaneSegment_segmentName_get
    if _newclass:segmentName = property(_libtrafsim.LaneSegment_segmentName_get, _libtrafsim.LaneSegment_segmentName_set)
    __swig_setmethods__["t_start"] = _libtrafsim.LaneSegment_t_start_set
    __swig_getmethods__["t_start"] = _libtrafsim.LaneSegment_t_start_get
    if _newclass:t_start = property(_libtrafsim.LaneSegment_t_start_get, _libtrafsim.LaneSegment_t_start_set)
    __swig_setmethods__["t_end"] = _libtrafsim.LaneSegment_t_end_set
    __swig_getmethods__["t_end"] = _libtrafsim.LaneSegment_t_end_get
    if _newclass:t_end = property(_libtrafsim.LaneSegment_t_end_get, _libtrafsim.LaneSegment_t_end_set)
    __swig_destroy__ = _libtrafsim.delete_LaneSegment
    __del__ = lambda self : None;
LaneSegment_swigregister = _libtrafsim.LaneSegment_swigregister
LaneSegment_swigregister(LaneSegment)

class JoinDescArray(_object):
    """Proxy of C++ JoinDescArray class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, JoinDescArray, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, JoinDescArray, name)
    __repr__ = _swig_repr
    def iterator(*args):
        """iterator(self, PyObject PYTHON_SELF) -> PySwigIterator"""
        return _libtrafsim.JoinDescArray_iterator(*args)

    def __iter__(self): return self.iterator()
    def __nonzero__(*args):
        """__nonzero__(self) -> bool"""
        return _libtrafsim.JoinDescArray___nonzero__(*args)

    def __len__(*args):
        """__len__(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::size_type"""
        return _libtrafsim.JoinDescArray___len__(*args)

    def pop(*args):
        """pop(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type"""
        return _libtrafsim.JoinDescArray_pop(*args)

    def __getslice__(*args):
        """
        __getslice__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::difference_type i, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::difference_type j) -> JoinDescArray
        """
        return _libtrafsim.JoinDescArray___getslice__(*args)

    def __setslice__(*args):
        """
        __setslice__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::difference_type i, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::difference_type j, 
            JoinDescArray v)
        """
        return _libtrafsim.JoinDescArray___setslice__(*args)

    def __delslice__(*args):
        """
        __delslice__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::difference_type i, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::difference_type j)
        """
        return _libtrafsim.JoinDescArray___delslice__(*args)

    def __delitem__(*args):
        """__delitem__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::difference_type i)"""
        return _libtrafsim.JoinDescArray___delitem__(*args)

    def __getitem__(*args):
        """__getitem__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::difference_type i) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type"""
        return _libtrafsim.JoinDescArray___getitem__(*args)

    def __setitem__(*args):
        """
        __setitem__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::difference_type i, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type x)
        """
        return _libtrafsim.JoinDescArray___setitem__(*args)

    def append(*args):
        """append(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type x)"""
        return _libtrafsim.JoinDescArray_append(*args)

    def empty(*args):
        """empty(self) -> bool"""
        return _libtrafsim.JoinDescArray_empty(*args)

    def size(*args):
        """size(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::size_type"""
        return _libtrafsim.JoinDescArray_size(*args)

    def clear(*args):
        """clear(self)"""
        return _libtrafsim.JoinDescArray_clear(*args)

    def swap(*args):
        """swap(self, JoinDescArray v)"""
        return _libtrafsim.JoinDescArray_swap(*args)

    def get_allocator(*args):
        """get_allocator(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::allocator_type"""
        return _libtrafsim.JoinDescArray_get_allocator(*args)

    def begin(*args):
        """
        begin(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::iterator
        begin(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::const_iterator
        """
        return _libtrafsim.JoinDescArray_begin(*args)

    def end(*args):
        """
        end(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::iterator
        end(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::const_iterator
        """
        return _libtrafsim.JoinDescArray_end(*args)

    def rbegin(*args):
        """
        rbegin(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::reverse_iterator
        rbegin(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::const_reverse_iterator
        """
        return _libtrafsim.JoinDescArray_rbegin(*args)

    def rend(*args):
        """
        rend(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::reverse_iterator
        rend(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::const_reverse_iterator
        """
        return _libtrafsim.JoinDescArray_rend(*args)

    def pop_back(*args):
        """pop_back(self)"""
        return _libtrafsim.JoinDescArray_pop_back(*args)

    def erase(*args):
        """
        erase(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::iterator pos) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::iterator
        erase(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::iterator first, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::iterator last) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::iterator
        """
        return _libtrafsim.JoinDescArray_erase(*args)

    def __init__(self, *args): 
        """
        __init__(self) -> JoinDescArray
        __init__(self, JoinDescArray ?) -> JoinDescArray
        __init__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::size_type size) -> JoinDescArray
        __init__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::size_type size, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type value) -> JoinDescArray
        """
        this = _libtrafsim.new_JoinDescArray(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(*args):
        """push_back(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type x)"""
        return _libtrafsim.JoinDescArray_push_back(*args)

    def front(*args):
        """front(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type"""
        return _libtrafsim.JoinDescArray_front(*args)

    def back(*args):
        """back(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type"""
        return _libtrafsim.JoinDescArray_back(*args)

    def assign(*args):
        """
        assign(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::size_type n, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type x)
        """
        return _libtrafsim.JoinDescArray_assign(*args)

    def resize(*args):
        """
        resize(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::size_type new_size)
        resize(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::size_type new_size, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type x)
        """
        return _libtrafsim.JoinDescArray_resize(*args)

    def insert(*args):
        """
        insert(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::iterator pos, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type x) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::iterator
        insert(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::iterator pos, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::size_type n, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::value_type x)
        """
        return _libtrafsim.JoinDescArray_insert(*args)

    def reserve(*args):
        """reserve(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::size_type n)"""
        return _libtrafsim.JoinDescArray_reserve(*args)

    def capacity(*args):
        """capacity(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Spline)>)>::size_type"""
        return _libtrafsim.JoinDescArray_capacity(*args)

    __swig_destroy__ = _libtrafsim.delete_JoinDescArray
    __del__ = lambda self : None;
JoinDescArray_swigregister = _libtrafsim.JoinDescArray_swigregister
JoinDescArray_swigregister(JoinDescArray)

class LaneSegmentArray(_object):
    """Proxy of C++ LaneSegmentArray class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, LaneSegmentArray, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, LaneSegmentArray, name)
    __repr__ = _swig_repr
    def iterator(*args):
        """iterator(self, PyObject PYTHON_SELF) -> PySwigIterator"""
        return _libtrafsim.LaneSegmentArray_iterator(*args)

    def __iter__(self): return self.iterator()
    def __nonzero__(*args):
        """__nonzero__(self) -> bool"""
        return _libtrafsim.LaneSegmentArray___nonzero__(*args)

    def __len__(*args):
        """__len__(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::size_type"""
        return _libtrafsim.LaneSegmentArray___len__(*args)

    def pop(*args):
        """pop(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type"""
        return _libtrafsim.LaneSegmentArray_pop(*args)

    def __getslice__(*args):
        """
        __getslice__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::difference_type i, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::difference_type j) -> LaneSegmentArray
        """
        return _libtrafsim.LaneSegmentArray___getslice__(*args)

    def __setslice__(*args):
        """
        __setslice__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::difference_type i, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::difference_type j, 
            LaneSegmentArray v)
        """
        return _libtrafsim.LaneSegmentArray___setslice__(*args)

    def __delslice__(*args):
        """
        __delslice__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::difference_type i, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::difference_type j)
        """
        return _libtrafsim.LaneSegmentArray___delslice__(*args)

    def __delitem__(*args):
        """__delitem__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::difference_type i)"""
        return _libtrafsim.LaneSegmentArray___delitem__(*args)

    def __getitem__(*args):
        """__getitem__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::difference_type i) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type"""
        return _libtrafsim.LaneSegmentArray___getitem__(*args)

    def __setitem__(*args):
        """
        __setitem__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::difference_type i, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type x)
        """
        return _libtrafsim.LaneSegmentArray___setitem__(*args)

    def append(*args):
        """append(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type x)"""
        return _libtrafsim.LaneSegmentArray_append(*args)

    def empty(*args):
        """empty(self) -> bool"""
        return _libtrafsim.LaneSegmentArray_empty(*args)

    def size(*args):
        """size(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::size_type"""
        return _libtrafsim.LaneSegmentArray_size(*args)

    def clear(*args):
        """clear(self)"""
        return _libtrafsim.LaneSegmentArray_clear(*args)

    def swap(*args):
        """swap(self, LaneSegmentArray v)"""
        return _libtrafsim.LaneSegmentArray_swap(*args)

    def get_allocator(*args):
        """get_allocator(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::allocator_type"""
        return _libtrafsim.LaneSegmentArray_get_allocator(*args)

    def begin(*args):
        """
        begin(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::iterator
        begin(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::const_iterator
        """
        return _libtrafsim.LaneSegmentArray_begin(*args)

    def end(*args):
        """
        end(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::iterator
        end(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::const_iterator
        """
        return _libtrafsim.LaneSegmentArray_end(*args)

    def rbegin(*args):
        """
        rbegin(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::reverse_iterator
        rbegin(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::const_reverse_iterator
        """
        return _libtrafsim.LaneSegmentArray_rbegin(*args)

    def rend(*args):
        """
        rend(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::reverse_iterator
        rend(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::const_reverse_iterator
        """
        return _libtrafsim.LaneSegmentArray_rend(*args)

    def pop_back(*args):
        """pop_back(self)"""
        return _libtrafsim.LaneSegmentArray_pop_back(*args)

    def erase(*args):
        """
        erase(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::iterator pos) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::iterator
        erase(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::iterator first, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::iterator last) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::iterator
        """
        return _libtrafsim.LaneSegmentArray_erase(*args)

    def __init__(self, *args): 
        """
        __init__(self) -> LaneSegmentArray
        __init__(self, LaneSegmentArray ?) -> LaneSegmentArray
        __init__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::size_type size) -> LaneSegmentArray
        __init__(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::size_type size, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type value) -> LaneSegmentArray
        """
        this = _libtrafsim.new_LaneSegmentArray(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(*args):
        """push_back(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type x)"""
        return _libtrafsim.LaneSegmentArray_push_back(*args)

    def front(*args):
        """front(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type"""
        return _libtrafsim.LaneSegmentArray_front(*args)

    def back(*args):
        """back(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type"""
        return _libtrafsim.LaneSegmentArray_back(*args)

    def assign(*args):
        """
        assign(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::size_type n, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type x)
        """
        return _libtrafsim.LaneSegmentArray_assign(*args)

    def resize(*args):
        """
        resize(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::size_type new_size)
        resize(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::size_type new_size, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type x)
        """
        return _libtrafsim.LaneSegmentArray_resize(*args)

    def insert(*args):
        """
        insert(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::iterator pos, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type x) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::iterator
        insert(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::iterator pos, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::size_type n, 
            std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::value_type x)
        """
        return _libtrafsim.LaneSegmentArray_insert(*args)

    def reserve(*args):
        """reserve(self, std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::size_type n)"""
        return _libtrafsim.LaneSegmentArray_reserve(*args)

    def capacity(*args):
        """capacity(self) -> std::vector<(TrafSim::LinearSegmentDesc<(TrafSim::Lane)>)>::size_type"""
        return _libtrafsim.LaneSegmentArray_capacity(*args)

    __swig_destroy__ = _libtrafsim.delete_LaneSegmentArray
    __del__ = lambda self : None;
LaneSegmentArray_swigregister = _libtrafsim.LaneSegmentArray_swigregister
LaneSegmentArray_swigregister(LaneSegmentArray)

class CarPath(_object):
    """Proxy of C++ CarPath class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, CarPath, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, CarPath, name)
    __repr__ = _swig_repr
    def iterator(*args):
        """iterator(self, PyObject PYTHON_SELF) -> PySwigIterator"""
        return _libtrafsim.CarPath_iterator(*args)

    def __iter__(self): return self.iterator()
    def __nonzero__(*args):
        """__nonzero__(self) -> bool"""
        return _libtrafsim.CarPath___nonzero__(*args)

    def __len__(*args):
        """__len__(self) -> size_type"""
        return _libtrafsim.CarPath___len__(*args)

    def pop(*args):
        """pop(self) -> value_type"""
        return _libtrafsim.CarPath_pop(*args)

    def __getslice__(*args):
        """__getslice__(self, difference_type i, difference_type j) -> CarPath"""
        return _libtrafsim.CarPath___getslice__(*args)

    def __setslice__(*args):
        """__setslice__(self, difference_type i, difference_type j, CarPath v)"""
        return _libtrafsim.CarPath___setslice__(*args)

    def __delslice__(*args):
        """__delslice__(self, difference_type i, difference_type j)"""
        return _libtrafsim.CarPath___delslice__(*args)

    def __delitem__(*args):
        """__delitem__(self, difference_type i)"""
        return _libtrafsim.CarPath___delitem__(*args)

    def __getitem__(*args):
        """__getitem__(self, difference_type i) -> value_type"""
        return _libtrafsim.CarPath___getitem__(*args)

    def __setitem__(*args):
        """__setitem__(self, difference_type i, value_type x)"""
        return _libtrafsim.CarPath___setitem__(*args)

    def append(*args):
        """append(self, value_type x)"""
        return _libtrafsim.CarPath_append(*args)

    def empty(*args):
        """empty(self) -> bool"""
        return _libtrafsim.CarPath_empty(*args)

    def size(*args):
        """size(self) -> size_type"""
        return _libtrafsim.CarPath_size(*args)

    def clear(*args):
        """clear(self)"""
        return _libtrafsim.CarPath_clear(*args)

    def swap(*args):
        """swap(self, CarPath v)"""
        return _libtrafsim.CarPath_swap(*args)

    def get_allocator(*args):
        """get_allocator(self) -> allocator_type"""
        return _libtrafsim.CarPath_get_allocator(*args)

    def begin(*args):
        """
        begin(self) -> iterator
        begin(self) -> const_iterator
        """
        return _libtrafsim.CarPath_begin(*args)

    def end(*args):
        """
        end(self) -> iterator
        end(self) -> const_iterator
        """
        return _libtrafsim.CarPath_end(*args)

    def rbegin(*args):
        """
        rbegin(self) -> reverse_iterator
        rbegin(self) -> const_reverse_iterator
        """
        return _libtrafsim.CarPath_rbegin(*args)

    def rend(*args):
        """
        rend(self) -> reverse_iterator
        rend(self) -> const_reverse_iterator
        """
        return _libtrafsim.CarPath_rend(*args)

    def pop_back(*args):
        """pop_back(self)"""
        return _libtrafsim.CarPath_pop_back(*args)

    def erase(*args):
        """
        erase(self, iterator pos) -> iterator
        erase(self, iterator first, iterator last) -> iterator
        """
        return _libtrafsim.CarPath_erase(*args)

    def __init__(self, *args): 
        """
        __init__(self) -> CarPath
        __init__(self, CarPath ?) -> CarPath
        __init__(self, size_type size) -> CarPath
        __init__(self, size_type size, value_type value) -> CarPath
        """
        this = _libtrafsim.new_CarPath(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(*args):
        """push_back(self, value_type x)"""
        return _libtrafsim.CarPath_push_back(*args)

    def front(*args):
        """front(self) -> value_type"""
        return _libtrafsim.CarPath_front(*args)

    def back(*args):
        """back(self) -> value_type"""
        return _libtrafsim.CarPath_back(*args)

    def assign(*args):
        """assign(self, size_type n, value_type x)"""
        return _libtrafsim.CarPath_assign(*args)

    def resize(*args):
        """
        resize(self, size_type new_size)
        resize(self, size_type new_size, value_type x)
        """
        return _libtrafsim.CarPath_resize(*args)

    def insert(*args):
        """
        insert(self, iterator pos, value_type x) -> iterator
        insert(self, iterator pos, size_type n, value_type x)
        """
        return _libtrafsim.CarPath_insert(*args)

    def reserve(*args):
        """reserve(self, size_type n)"""
        return _libtrafsim.CarPath_reserve(*args)

    def capacity(*args):
        """capacity(self) -> size_type"""
        return _libtrafsim.CarPath_capacity(*args)

    __swig_destroy__ = _libtrafsim.delete_CarPath
    __del__ = lambda self : None;
CarPath_swigregister = _libtrafsim.CarPath_swigregister
CarPath_swigregister(CarPath)

class SplineArray(_object):
    """Proxy of C++ SplineArray class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, SplineArray, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, SplineArray, name)
    __repr__ = _swig_repr
    def iterator(*args):
        """iterator(self, PyObject PYTHON_SELF) -> PySwigIterator"""
        return _libtrafsim.SplineArray_iterator(*args)

    def __iter__(self): return self.iterator()
    def __nonzero__(*args):
        """__nonzero__(self) -> bool"""
        return _libtrafsim.SplineArray___nonzero__(*args)

    def __len__(*args):
        """__len__(self) -> size_type"""
        return _libtrafsim.SplineArray___len__(*args)

    def pop(*args):
        """pop(self) -> value_type"""
        return _libtrafsim.SplineArray_pop(*args)

    def __getslice__(*args):
        """__getslice__(self, difference_type i, difference_type j) -> SplineArray"""
        return _libtrafsim.SplineArray___getslice__(*args)

    def __setslice__(*args):
        """__setslice__(self, difference_type i, difference_type j, SplineArray v)"""
        return _libtrafsim.SplineArray___setslice__(*args)

    def __delslice__(*args):
        """__delslice__(self, difference_type i, difference_type j)"""
        return _libtrafsim.SplineArray___delslice__(*args)

    def __delitem__(*args):
        """__delitem__(self, difference_type i)"""
        return _libtrafsim.SplineArray___delitem__(*args)

    def __getitem__(*args):
        """__getitem__(self, difference_type i) -> value_type"""
        return _libtrafsim.SplineArray___getitem__(*args)

    def __setitem__(*args):
        """__setitem__(self, difference_type i, value_type x)"""
        return _libtrafsim.SplineArray___setitem__(*args)

    def append(*args):
        """append(self, value_type x)"""
        return _libtrafsim.SplineArray_append(*args)

    def empty(*args):
        """empty(self) -> bool"""
        return _libtrafsim.SplineArray_empty(*args)

    def size(*args):
        """size(self) -> size_type"""
        return _libtrafsim.SplineArray_size(*args)

    def clear(*args):
        """clear(self)"""
        return _libtrafsim.SplineArray_clear(*args)

    def swap(*args):
        """swap(self, SplineArray v)"""
        return _libtrafsim.SplineArray_swap(*args)

    def get_allocator(*args):
        """get_allocator(self) -> allocator_type"""
        return _libtrafsim.SplineArray_get_allocator(*args)

    def begin(*args):
        """
        begin(self) -> iterator
        begin(self) -> const_iterator
        """
        return _libtrafsim.SplineArray_begin(*args)

    def end(*args):
        """
        end(self) -> iterator
        end(self) -> const_iterator
        """
        return _libtrafsim.SplineArray_end(*args)

    def rbegin(*args):
        """
        rbegin(self) -> reverse_iterator
        rbegin(self) -> const_reverse_iterator
        """
        return _libtrafsim.SplineArray_rbegin(*args)

    def rend(*args):
        """
        rend(self) -> reverse_iterator
        rend(self) -> const_reverse_iterator
        """
        return _libtrafsim.SplineArray_rend(*args)

    def pop_back(*args):
        """pop_back(self)"""
        return _libtrafsim.SplineArray_pop_back(*args)

    def erase(*args):
        """
        erase(self, iterator pos) -> iterator
        erase(self, iterator first, iterator last) -> iterator
        """
        return _libtrafsim.SplineArray_erase(*args)

    def __init__(self, *args): 
        """
        __init__(self) -> SplineArray
        __init__(self, SplineArray ?) -> SplineArray
        __init__(self, size_type size) -> SplineArray
        __init__(self, size_type size, value_type value) -> SplineArray
        """
        this = _libtrafsim.new_SplineArray(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(*args):
        """push_back(self, value_type x)"""
        return _libtrafsim.SplineArray_push_back(*args)

    def front(*args):
        """front(self) -> value_type"""
        return _libtrafsim.SplineArray_front(*args)

    def back(*args):
        """back(self) -> value_type"""
        return _libtrafsim.SplineArray_back(*args)

    def assign(*args):
        """assign(self, size_type n, value_type x)"""
        return _libtrafsim.SplineArray_assign(*args)

    def resize(*args):
        """
        resize(self, size_type new_size)
        resize(self, size_type new_size, value_type x)
        """
        return _libtrafsim.SplineArray_resize(*args)

    def insert(*args):
        """
        insert(self, iterator pos, value_type x) -> iterator
        insert(self, iterator pos, size_type n, value_type x)
        """
        return _libtrafsim.SplineArray_insert(*args)

    def reserve(*args):
        """reserve(self, size_type n)"""
        return _libtrafsim.SplineArray_reserve(*args)

    def capacity(*args):
        """capacity(self) -> size_type"""
        return _libtrafsim.SplineArray_capacity(*args)

    __swig_destroy__ = _libtrafsim.delete_SplineArray
    __del__ = lambda self : None;
SplineArray_swigregister = _libtrafsim.SplineArray_swigregister
SplineArray_swigregister(SplineArray)

__naked_CLASS_METHOD = Environment.addObject

def __disown_METHOD(*args):
	args[1].thisown = 0
	return __naked_CLASS_METHOD(*args)

__disown_METHOD.__doc__ = __naked_CLASS_METHOD.__doc__
Environment.addObject = __disown_METHOD

if __name__ == '__main__':
	import doctest
	doctest.testmod()



