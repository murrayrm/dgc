#include "CurveConstraint.h"
#include <cmath>

#define min(a,b) ( ((a)<(b))?(a):(b) )

namespace TrafSim
{
	
float curveConstraint(Spline const& spline, float t, float vel, 
					  float linearAccel, float angularAccel)
{
	unsigned int index = spline._mapTCoord(t).first;
	
	// get zeroT, the lowest T at which the car can come to a complete stops
	float dist   = 0.5f * vel * vel / linearAccel;
	float zeroT  = spline.evalDisplacement(t, dist);
	if (zeroT == -1.0f) zeroT = 1.0f;
	
	// [index, index2] are the bounds of the spline's data points that we need to consider 
	unsigned int index2 = spline._mapTCoord(zeroT).first;
	unsigned int diff = 
		min(index2 + 2, spline.cacheResolution + 1) - index;
	
	// for each data point, find the highest possible velocity that can be achieved at that point
	// without violating the angular acceleration constraint.
	float* speedBuffer = new float[diff];
	for (unsigned int i = 0; i < diff; ++i)
	{
		float curve = spline.curvature[index + i];
		if (curve != 0.0f)
			speedBuffer[i] = sqrt(angularAccel / abs(curve));
		else
			speedBuffer[i] = 10000.0f;
	}
	
	// now propagate the results backwards, ensuring that it's possible to decelerate to 
	// decelerate to an acceptable velocity at each point, without violating the linear acceleration constraint.
	for (unsigned int i = diff - 2; i < diff; --i)    // hack: unsigned int wraparound
	{
		float lenDelta = spline.lengths[index + i + 1];
		float newMax = sqrt(2 * lenDelta * linearAccel +
							speedBuffer[i + 1] *
							speedBuffer[i + 1]);
		if (newMax < speedBuffer[i])
			speedBuffer[i] = newMax;
	}
	
	// and we're done
	float result = speedBuffer[1];
	delete[] speedBuffer;
	return result;
}	

}
