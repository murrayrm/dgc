################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Car.cpp \
../CarFactory.cpp \
../CarProperties.cpp \
../Color.cpp \
../CurveConstraint.cpp \
../Environment.cpp \
../ExternalCar.cpp \
../IDM.cpp \
../Intersection.cpp \
../Lane.cpp \
../LaneGrid.cpp \
../Object.cpp \
../Road.cpp \
../Serialization.cpp \
../Spline.cpp \
../Vector.cpp \
../Viewport.cpp \
../XIntersection.cpp \
../YIntersection.cpp 

CXX_SRCS += \
../libtrafsim_wrap.cxx 

OBJS += \
./Car.o \
./CarFactory.o \
./CarProperties.o \
./Color.o \
./CurveConstraint.o \
./Environment.o \
./ExternalCar.o \
./IDM.o \
./Intersection.o \
./Lane.o \
./LaneGrid.o \
./Object.o \
./Road.o \
./Serialization.o \
./Spline.o \
./Vector.o \
./Viewport.o \
./XIntersection.o \
./YIntersection.o \
./libtrafsim_wrap.o 

CPP_DEPS += \
./Car.d \
./CarFactory.d \
./CarProperties.d \
./Color.d \
./CurveConstraint.d \
./Environment.d \
./ExternalCar.d \
./IDM.d \
./Intersection.d \
./Lane.d \
./LaneGrid.d \
./Object.d \
./Road.d \
./Serialization.d \
./Spline.d \
./Vector.d \
./Viewport.d \
./XIntersection.d \
./YIntersection.d 

CXX_DEPS += \
./libtrafsim_wrap.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -D_REENTRANT -I/usr/include/stlport -I/usr/include/python2.4 -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.cxx
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -D_REENTRANT -I/usr/include/stlport -I/usr/include/python2.4 -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


