
#include <iostream>
#include <math.h>
#include <assert.h>

using namespace std;

#include "cv.h"
#include "highgui.h"

#include "mcv.hh"





//some helper functions for debugging

//print the matrix passed to it
void SHOW_MAT(const CvMat *pmat, char str[])
{  
    cout << str << "\n";
    for(int i=0; i<pmat->rows; i++) 
    {
        for(int j=0; j<pmat->cols; j++) 
           cout << cvGetReal2D(pmat, i, j)  << " ";
        cout << "\n"; 
    }
}

void SHOT_MAT_TYPE(const CvMat *pmat)
{
    cout << CV_MAT_TYPE(pmat->type) << "\n";
}

void SHOW_IMAGE(const CvMat *pmat, char str[])
{
    //cout << "channels:" << CV_MAT_CN(pmat->type) << "\n";
    //scale it
    //CvMat *mat = cvCreateMat(pmat->height, pmat->width, pmat->type);
    //cvCopy(pmat, mat);
    CvMat *mat = cvCloneMat(pmat);
    assert(mat);
    mcvScaleMat(mat, mat);
    //show it
    //cout << "in\n";
    cvNamedWindow(str, 1); 
    cvShowImage(str, mat); 
    cvWaitKey(0);
    //cvDestroyWindow(str);
    //clear
    cvReleaseMat(&mat);     
    //cout << "out\n";
}

void SHOW_POINT(const FLOAT_POINT2D pt, char str[])
{
    
    cout << str << "(" << pt.x << "," << pt.y << ")\n";   
}



/**
 * This function scales the input image to have values 0->1
 * 
 * \param inImage the input image
 * \param outImage hte output iamge
 */
void mcvScaleMat(const CvMat *inMat, CvMat *outMat)
{
    //if (CV_MAT_DEPTH(inMat->type)
    //get the min and subtract it
    double min;
    cvMinMaxLoc(inMat, &min, 0, 0, 0, 0);
    cvSubS(inMat, cvRealScalar(min), outMat);
    
    //get max and divide by it
    double max;
    cvMinMaxLoc(outMat, 0, &max, 0, 0, 0);
    cvConvertScale(outMat, outMat, 1/max);
}
