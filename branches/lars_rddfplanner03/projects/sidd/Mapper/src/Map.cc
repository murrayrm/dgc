#include "Map.hh"
#include "LogicalRoadObject.hh"
#include "RoadObject.hh"

namespace Mapper
{
/* Basic constructor */
Map::Map(vector<Segment> initialSegs)
{
	segments = initialSegs;
}
/* Default, static map constructor */
Map::Map()
{
	// TODO: Finish this after Noel sends his map.
	// Create the correct number of blank lane objects.
	// Use the spoof method to populate the lanes.
	// Construct the segment.
	// Populate the map.
	
	// Create the waypoints
	Location loc0 = {34.13908, -118.12341}; // first seg 
	Location loc1 = {34.13908, -118.12330};
	Location loc2 = {34.13908, -118.12319};
	Location loc3 = {34.13908, -118.12308}; // second seg
	Location loc4 = {34.13908, -118.12298};
	Location loc5 = {34.13908, -118.12287};
	
	// Build the centerlines 
	vector<Location> centerline0;
	centerline0.push_back(loc0);
	centerline0.push_back(loc1);
	centerline0.push_back(loc2);
	vector<Location> centerline1;
	centerline1.push_back(loc3);
	centerline1.push_back(loc4);
	centerline1.push_back(loc5);
	
	// Build the lanes, only one per segment
	Lane lane1_1 = Lane(1, centerline0);
	Lane lane2_1 = Lane(1, centerline1);
	vector<Lane> seg1_lanes;
	vector<Lane> seg2_lanes;
	seg1_lanes.push_back(lane1_1);
	seg1_lanes.push_back(lane1_1);
	seg2_lanes.push_back(lane2_1);
	seg2_lanes.push_back(lane2_1);
	
	// Finally, build the segments and add them to the map
	Segment seg1 = Segment(1, seg1_lanes);
	Segment seg2 = Segment(2, seg2_lanes);
	segments.push_back(seg1);
	segments.push_back(seg1); // duplication is not a mistake, has to do with 0 vs 1-indexing
	segments.push_back(seg2);
}
/* RNDF Constructor */
/*Map::Map(RNDF RNDFObj)
{
	// For every segment
		// For every lane within the segment
			// Instantiate the lane object.
}*/

/* Pretty print the segment object. */
void Map::print() const
{
	unsigned int i;
	
	std::cout << "Object Map: \n";
	for (i=0; i<segments.size(); i++) 
	{
		segments[i].print();
	}
}

/* Retrieve a segment given its ID. */
Segment Map::getSegment(int segmentID) const 
{
	Segment reqSegment = segments[segmentID]; //The segment requested.
	int reqSegID = reqSegment.getID();
	
	/* Check that the segment ID matches the requested
	 * segment ID. */
	if (reqSegID != segmentID)
		throw "The requested segment ID does not match the actual ID of the segment returned.\n";
		
	return reqSegment;
}
vector<Segment> Map::getAllSegs() const
{
	return segments;
}

/* Destructor */
Map::~Map()
{
}

}
