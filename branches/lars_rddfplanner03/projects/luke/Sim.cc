#include "Sim.hh"
#include "DGCutils"

SIM_DATUM d;

int QUIT_PRESSED = 0;
int PAUSED       = 1;
int RESET_PRESSED        = 0;
vehicle_t* g_pVehicle;
//int user_quit();
int pause_simulation();
int unpause_simulation();
int reset_simulation();

asim::asim(int sn_key, bool bSimpleModel, double steerLag, double steerRateLimit, int noise, bool estop, bool trans)
  : CSkynetContainer(SNasim, sn_key),
    m_bSimpleModel(bSimpleModel),
    m_steerLag(steerLag),
    CModuleTabClient(&m_input, &m_output),
    m_steerRateLimit(steerRateLimit)
{
  // clear the state struct and actuators
  d.accel_cmd = 0;
  d.steer_cmd = 0;
  d.trans_cmd = GEAR_DRIVE;

  simnoise = noise;
  use_estop = estop;
  use_trans = trans;
  m_pVehicle = new vehicle_t;
  g_pVehicle = m_pVehicle;

  pause_simulation();

  SimInit();

  DGCcreateMutex(&m_stateMutex);

  broadcast_statesock = m_skynet.get_send_sock(SNstate);
}

asim::~asim()
{
  delete m_pVehicle;
}

void asim::SimInit(void)
{
  DGCgettime(d.lastUpdate);
  simState.readFile("simInitState.dat");

#if 0
  double ststate[]=
    {3834528.7036177786067, 1.9625494408429147164, -0.12031789308008045136,
     442624.57282297825441, 0.75617731768330342934, 0.21415203500229329503,
     0.1120598116244396758, 0.18951424501995356442}
  ;

  simState.n   = ststate[0];
  simState.e   = ststate[3];
  simState.nd  = ststate[1];
  simState.ed  = ststate[4];
  simState.ndd = ststate[2];
  simState.edd = ststate[5];
  simState.yaw = ststate[6];
  simState.yawd = ststate[7];
  simState.phi = atan(simState.yawd * VEHICLE_WHEELBASE / hypot(ststate[1], ststate[4]));
#endif


  if(m_bSimpleModel)
    {
      simEngineSimple.Init(simState, 0.0);
      // this sets the front state to match the simState. The large argument
      // effectively sets ndd and edd to 0
      simEngineSimple.set_front_state(1.0e10);
    }
  else
    {
      simEngine.Init(simState, 0.0, m_steerLag, m_steerRateLimit);
      // this sets the front state to match the simState. The large argument
      // effectively sets ndd and edd to 0
      simEngine.set_front_state(1.0e10);
    }
}

void asim::broadcast()
{
  pthread_mutex_t* pMutex = &m_stateMutex;

  if (m_skynet.send_msg(broadcast_statesock,
			&d.SS,
			sizeof(d.SS),
			0,
			&pMutex)
      != sizeof(d.SS))
    {
      cerr << "asim::broadcast(): didn't send right size state message" << endl;
    }
}

void asim::Active() 
{
  unsigned long long dt;
  unsigned long long biasedTime;
  cout << "entering asim::Active loop" << endl;

  m_pVehicle->actuator_estop.command = ESTP_RUN;

  while (true) 
    {
      DGCusleep(25000);
    
      // handle time update
      DGCgettime(dt);    
      dt -= d.lastUpdate;
      DGCgettime(d.lastUpdate);
      
      d.accel_cmd = m_pVehicle->actuator[ACTUATOR_GAS].command - m_pVehicle->actuator[ACTUATOR_BRAKE].command;
      // The actuator command is normalized [-1,1], as is steer_cmd
      d.steer_cmd = m_pVehicle->actuator[ACTUATOR_STEER].command;

      if(use_trans)
	{
	  d.trans_cmd = m_pVehicle->actuator_trans.command; //uncomment to accept trans commands
	}
      //cout << "estop is: " <<  m_pVehicle->actuator_estop.command << endl;
    
      // now perform state update with known time step
      if(m_bSimpleModel)
	simEngineSimple.SetCommands(d.steer_cmd, d.accel_cmd, d.trans_cmd);
      else
	simEngine.SetCommands(d.steer_cmd, d.accel_cmd, d.trans_cmd);
    
      switch(m_output.EnterCommand)
	{
	  //case 'Q': user_quit(); m_output.EnterCommand = 'Z'; break;
	case 'P': pause_simulation(); m_output.EnterCommand = 'Z'; break;
	case 'U': unpause_simulation(); m_output.EnterCommand = 'Z'; break;
	case 'R': reset_simulation(); m_output.EnterCommand = 'Z'; break;
	default: break;
	}
      m_input.simulationPAUSED = PAUSED;

      if( !PAUSED ) 
	{
	  if(m_bSimpleModel)
	    simEngineSimple.RunSim(DGCtimetosec(dt));
	  else
	    simEngine.RunSim(DGCtimetosec(dt));
	} 
      if( RESET_PRESSED )
	{
	  SimInit();
	  RESET_PRESSED = 0;
	}
      updateState();
    
      // now set the actuator outputs (simulate them!)
      m_pVehicle->actuator[ACTUATOR_STEER].position = d.phi / VEHICLE_MAX_AVG_STEER;
      m_pVehicle->actuator[ACTUATOR_GAS]  .position = d.accel_cmd > 0.0 ?  d.accel_cmd : 0.0;
      m_pVehicle->actuator[ACTUATOR_BRAKE].position = d.accel_cmd < 0.0 ? -d.accel_cmd : 0.0;
      m_pVehicle->actuator_trans.position = d.trans_cmd;
      if(use_estop)
	{
	  m_pVehicle->actuator_estop.position = m_pVehicle->actuator_estop.command;
	  m_pVehicle->actuator_estop.astop = ESTP_RUN;
	  m_pVehicle->actuator_estop.dstop = ESTP_RUN;
	}
      
      //I'm sorry - wheelspeed = vehicle speed :(
      m_pVehicle->actuator_obdii.VehicleWheelSpeed = sqrt( pow(m_input.Vel_N, 2) + pow(m_input.Vel_E, 2) );
      
      broadcast();
    }
}

void asim::updateState(void)
{
  unsigned long long time;
  DGCgettime(time);
  if(time - noisetime > 1000000)
    {
      noisetime = time;
    }
  
  if(m_bSimpleModel)
    simState = simEngineSimple.GetFrontState();
  else
    simState = simEngine.GetFrontState();

  // Updating the state struct
  DGClockMutex(&m_stateMutex);
  m_input.Northing = d.SS.Northing = simState.n + simnoise*.0000001*(time - noisetime);
  m_input.Easting = d.SS.Easting  = simState.e + simnoise*.0000001*(time - noisetime);
  m_input.Vel_N = d.SS.Vel_N    = simState.nd;
  m_input.Vel_E = d.SS.Vel_E    = simState.ed;
  m_input.Acc_N = d.SS.Acc_N    = simState.ndd;
  m_input.Acc_E = d.SS.Acc_E    = simState.edd;
  m_input.YawRateRad = d.SS.YawRate  = simState.yawd;	
  m_input.YawRad = d.SS.Yaw      = simState.yaw;
  d.phi = simState.phi;
	//HACK -- need to make this raw data look more like raw data somehow
	d.SS.GPS_Northing = d.SS.Northing;
	d.SS.GPS_Easting = d.SS.Easting;
	d.SS.raw_YawRate = d.SS.YawRate;

  m_input.Altitude = d.SS.Altitude = 0.0;
  m_input.Acc_D = d.SS.Acc_D    = 0.0;
  m_input.YawAccel = d.SS.YawAcc   = 0.0;
  m_input.RollAccel = d.SS.RollAcc  = 0.0;
  m_input.PitchAccel = d.SS.PitchAcc = 0.0;
  m_input.Vel_D = d.SS.Vel_D    = 0.0;
  m_input.PitchRate = d.SS.PitchRate= 0.0;
  m_input.RollRate = d.SS.RollRate = 0.0;
  m_input.Pitch = d.SS.Pitch    = 0.0;
  m_input.Roll = d.SS.Roll     = 0.0;

	// done indirectly in this way to bypass reference access to a packed member
	unsigned long long timestamp;
  DGCgettime(timestamp);
	d.SS.Timestamp = timestamp;

  DGCunlockMutex(&m_stateMutex);
}

// TODO: Set the shutdown flag in the quit function
/*int user_quit()
  {
  return DD_EXIT_LOOP;
  }*/

// Pause the simulation
int pause_simulation()
{
  g_pVehicle->actuator_estop.position = ESTP_PAUSE;
  PAUSED = 1;
  return PAUSED;
}

// Unpause the simulation
int unpause_simulation()
{
  g_pVehicle->actuator_estop.position = ESTP_RUN;
  PAUSED = 0;
  return PAUSED;
}

// Reset the simulation
int reset_simulation()
{
  RESET_PRESSED = 1;
  return RESET_PRESSED;
}
