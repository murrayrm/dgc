 /****************************************************************
 *  Project    : Learning and adaptation, SURF 2006              *
 *  File       : Learner.hh                                      *
 *  Author     : Jose Torres                                     *
 *  Modified   : 7-19-06                                         *
 *****************************************************************/      

#ifndef LEARNER_HH
#define LEARNER_HH

#include "Node.hh"
#include "StateClient.h"
#include "SkynetContainer.h"
//#include "datum.hh"
#include <iostream>
#include <cstring>
#include <iomanip>
#include <cmath>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <stdio.h>
#include <time.h>
#define GPS_CAPTURE_FREQUENCY 0.5
using namespace std;

/** Learner class. This class stores all the paths ALICE
 *  has driven. It currently creates a file "savedPaths" and
 *  stores all the coordinates in this file. This class also 
 *  stores the path using a dynamicly created double list.
 * \brief 
 *  This class creates a list with nodes  for each location 
 *  ALICE has been at. Each node in the list also has a list 
 *  attached to it, representing all the locations ALICE went 
 *  immediately after that corresponding location 
 */
class Learner : public CStateClient 
{
  
private: 
  

  /** used to point to the first node in list representing 
   *  ALICE's first position */
  Node* start;
  
  /** used to point to the last node in list representing
   *  ALICE's last position  */
  Node* end;              
  
  /** keeps track of the number of nodes in location list */
  int locationListLength;

  //hack to send mock transition message
  int numSeg; 

  /** struct used for simulate checkpoint messages from global planner */
  struct Checkpoint
  {
    double startX, startY, endX, endY;
    
  };
  
  /** struct used to simulate segment transition messages from global planner*/
  struct Transition
  {
    int segment;
    bool segmentTransition;
  };
 
public:

  /** constructor */
  Learner(double northing, double easting, int skynet_key);
  
  /** destructor, destroys dynamicly created lists  */
  ~Learner();                                   
  
  /** Function adds new node positions to previously traveled list. There is 
   *  a list that contains all the positions ALICE has been at. Each 
   *  position also has a list attached to it, representing all the 
   *  locations ALICE went immediately after that corresponding location */
  void addNode(double lastNorthing, double lastEasting, double northing, double easting); 
  
  /** Function prints all the positions ALICE has been at, not necessary
   *  in any order */
  void printPath();
  
  /** Given a current position, ths function prints all the traversed 
   *  paths previously taken from current position */
  void printPath(double x, double y);
  
  /** Function deletes "Location" list */
  void destroyLocations(); 
  
  /** Function deletes "Path" list */
  //void destroyPaths(Node* startPath); 
  void destroyPaths(); 
  
  /** Function takes absolute value of parameter. 
   *  User defined function */
  double abs(double x);
  
  /** Function searches "locations", README file defines locations*/
  Node* search(double x, double y);
  
  /** Function searches "paths", README file defines paths */
  Node* searchPath(double x, double y);
  
  void print(double x, double y);
  
  /** General function that is modified to test any portion of code */
  void testFunction();
  
  /** Function writes path to file and receives updates from Astate
   *  Function also calls addNode */
  void active();
  
  /** Function searches "savedPaths" file for a path that takes 
   *  you to the checkpoints received from global planner */
  bool searchFile(double startX, double startY, double destinationX, double destinationY);
  
  /** Function outputs path to checkpoint received from global planner*/
  void pathToCheckPoint(double startX, double startY, double destinationX, double destinationY);
  
  /** Function is used to simulate global planner. It sends 
   *  messages (type SNrndfCheckpoint) containing the checkpoints */
  void sendCheckpoint(double startX, double startY, double destinationX, double destinationY, bool segmentTransition);
  
  /** temp function attempting to open database*/
  void createDatabase();
};

#endif
