#include "ProcessMan.hh"
#include "RingMonitor.hh"

#include <getopt.h>
#include <assert.h>


int OPTKEY = -1;

/**
 *The name of this program. 
 */
const char * program_name;

/**
 * Prints usage information for this program to STREAM (typically
 * stdout or stderr), and exit the program with EXIT_CODE. Does not
 * return.  
 */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
           "  --key -k          Use key .\n"
           "  --help, -h        Display this message.\n" );
  exit(exit_code);
}


int main(int argc, char *argv[])
{
  /** 
   * Temporary character. 
   */
  int ch;
  
  /** 
   * A string listing valid short options letters. 
   */
  const char* const short_options = "h k";
  /**
   * An array describing valid long options. 
   */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"key",        1, NULL,              'k'},
    {"help",       0, NULL,              'h'},
    {NULL,         0, NULL,              0}
  };
  /**
   * Remember the name of the program, to incorporate in messages. The
   * name is stored in argv[0].  
   */
  program_name = argv[0];
  printf("\n");  
  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
    {
      switch(ch)
	{
	case 'h':
	  /* User has requested usage information. Print it to standard
	     output, and exit with exit code zero (normal 
	     termination). */
	  print_usage(stdout, 0);

	case '?': /* The user specified an invalid option. */
	  /* Print usage information to standard error, and exit with exit
	     code one (indicating abnormal termination). */
	  print_usage(stderr, 1);
	case 'k':
	  OPTKEY = atoi(optarg);
	  break;
	case -1: /* Done with options. */
	  break;
	}
    }
  int sn_key = 0;  
  if(OPTKEY == -1)
    {
      cerr << "Searching for skynet KEY " << endl;	  
      char* pSkynetkey = getenv("SKYNET_KEY");
      if( pSkynetkey == NULL )
	{
	  cerr << "SKYNET_KEY environment variable isn't set" << endl;
	}
      else
	{
	  sn_key = atoi(pSkynetkey);
	}
    }
  else
    {
      sn_key = OPTKEY;
    }
  cout << "SKYNET_KEY is " << sn_key << endl;
  //ProcessMan myMan(sn_key);
  RingMonitor test;
  
  return 0;
}
