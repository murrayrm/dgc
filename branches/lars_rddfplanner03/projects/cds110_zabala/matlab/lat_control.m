% Lateral controller

clear all;

V_0 = 10;
l = 3;

A = [0   0    0     0     1 ;
     0   0   V_0    0     0 ;
     0   0    0  -V_0/l   0 ;
     0   0    0     0     0 ;
     0   0    0     0     0];
 
B = [0 0 ;
     0 0 ;
     0 0 ;
     1 0 ;
     0 1];

C = eye(5);

D = zeros(5,2);

% initial conditions
init = [0; 1; 0; 0; 0];

% determine gains

p = [-1.1 -3.5 -3.3 -3.7 -1.1];
K = place(A,B,p);


sim lat_control_dynamics;

plot(current_state.time, current_state.signals.values(:,2));


%plot(z.time,z.signals.values(:,1),z.time,z.signals.values(:,2),z.time, ...
%    z.signals.values(:,3));