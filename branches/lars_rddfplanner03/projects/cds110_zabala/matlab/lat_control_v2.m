% Lateral controller

clear all;

V = 3.5;
l = 3;

% choose some eigenvalues to make the gains

eig1 = -.1;
eig2 = .7;
eig3 = -2;

k1 = eig1 * eig2 * eig3 / V^2;
k2 = -1*(eig1 * eig2 + eig1 * eig3 + eig2 * eig3) / V;
k3 = -1*(eig1 + eig2 + eig3);

K = [-1*k1 -1*k2 -1*k3];

A_sim = [0 V 0; 
    0 0 V/l; 
    0 0 0];

B_sim = [0;
     0;
     1];

C_sim = eye(3);

D_sim = [0;
         0;
         0];

% initial conditions
init = [1; 0; 0];

% determine gains

% HW 4 problem 6

V = 10;
l = 3;

% "linearization" of A, with the control input k
A_lin = [0 V 0; 0 0 V/l; -1*k1 -1*k2 -1*k3];

% stable if the real parts of the eigenvalues are all less than zero
eigenvalues = eig(A_lin);
display(eigenvalues);

if(real(eigenvalues(1))<0 && real(eigenvalues(3))<0 && real(eigenvalues(3))<0)

    % model with no time delay
    sim hw4_6_c;

    figure;
    plot(z.time,z.signals.values(:,1),z.time,z.signals.values(:,2),z.time, ...
        z.signals.values(:,3));
    legend('show')
    legend('y','theta','phi')
    title(['eig1=',num2str(eig1),' | eig2=',num2str(eig2),' | eig3=',num2str(eig3)]);

    load ../follow/lateral_03;
    D(2) = -1*k1; 
    D(3) = -1*k2*2; 
    D(6) = -1*k3;
    D(11) = k1;
    D(12) = k2*2;
    D(15) = k3;
    save('../follow/lateral_04','A','B','C','D');
    
    % now model the time delay and see if that affects stability
    sim lat_control_dynamics;

    figure;
    plot(current_state.time, current_state.signals.values(:,1), ...
        current_state.time, current_state.signals.values(:,2), ...
        current_state.time, current_state.signals.values(:,3));

    legend('show')
    legend('y','theta','phi')
    title(['SIMULINK: k1=',num2str(k1),' | k2=',num2str(k2),' | k3=',num2str(k3)]);

end