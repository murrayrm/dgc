/*
 *  DPlannerModule.cpp
 *  DPlannerModule
 *
 *  Created by Melvin E. Flores on 11/26/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#include <time.h>
#include <iostream>
#include <iomanip>

#include "CDPlannerModule.hh"

CDPlannerModule:: CDPlannerModule(int SkynetKey, bool WAIT_STATE):CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE)
{
	m_i = 0;
	m_j = 0;
	m_Error = 0;
	
	m_Duration = 0.0;
	
	/* Create an RDDF object (from a file, for now) */
	m_RDDF = new RDDF("rddf.dat");
	if(m_RDDF != NULL)
	{
	   cout << "RDDF object was created successfully" << endl;		
	}
	
	/* Create a Traj object (z, zd, zdd) */
	m_Traj = new CTraj(3);
	if(m_RDDF != NULL)
	{
	   cout << "Traj object was created successfully" << endl;		
	}
	
	
	m_CostMapPlus = new CMapPlus();
	if(m_CostMapPlus != NULL)
	{
	   cout << "Cost Map Plus object was created successfully" << endl;		
	}

	/*
	m_CostMap = new CMap();
	if(m_CostMap != NULL)
	{
	   cout << "Cost Map object was created successfully" << endl;
	}
	*/

	/* 
	 * Create Alice problems: Currently only one is present - Alice_Nominal
	 * In the future there will be a set of Alice problems being created here. 
	 */ 
	m_apType = apt_Nominal;
	 
	m_Alice_Nominal = new CAlice_Nominal();
	if(m_Alice_Nominal != NULL)
	{
	   cout << "Alice_Nominal problem definition was created successfully" << endl;	
	}

	m_OCProblem     = new COCProblem(m_Alice_Nominal);
	if(m_OCProblem != NULL)
	{
	   cout << "OC Problem was created successfully" << endl;
	}		

	m_OCPSolver     = new COCPSolver(m_OCProblem);
	if(m_OCPSolver != NULL)
	{
	   cout << "OCP Solver was created successfully" << endl;
	}
	
	m_SendSocket = m_skynet.get_send_sock(SNtraj);
		
	m_NofFlatOutputs             = m_OCProblem->GetNofFlatOutputs();
	m_NofWeights                 = new int[m_NofFlatOutputs];
	m_NofDimensionsOfFlatOutputs = new int[m_NofFlatOutputs];
	m_NofControlPoints           = new int[m_NofFlatOutputs];
		
	m_OCProblem->GetNofWeights((int* const) m_NofWeights);
	m_OCProblem->GetNofDimensionsOfFlatOutputs((int* const) m_NofDimensionsOfFlatOutputs);
	m_OCProblem->GetNofControlPoints((int* const) m_NofControlPoints);
		
	m_Weights       = new double* [m_NofFlatOutputs];
	m_ControlPoints = new double**[m_NofFlatOutputs];
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		m_Weights[m_i] = new double[m_NofWeights[m_i]];
		m_ControlPoints[m_i] = new double*[m_NofDimensionsOfFlatOutputs[m_i]];
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
			m_ControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
		}
	}				
}

CDPlannerModule::~CDPlannerModule(void)
{
	delete m_RDDF;
	delete m_Traj;
	delete m_CostMapPlus;
	//delete m_CostMap;

	delete m_OCPSolver;
	delete m_OCProblem;
	delete m_Alice_Nominal;
	
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		delete[] m_Weights[m_i];
	}
	delete[] m_Weights;

	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
			delete[] m_ControlPoints[m_i][m_j];
		}
		delete[] m_ControlPoints[m_i];
	}
	delete[] m_ControlPoints;
	
	delete[] m_NofWeights;
	delete[] m_NofDimensionsOfFlatOutputs;
	delete[] m_NofControlPoints;	
}

void CDPlannerModule::ActiveLoop(void)
{
	/* Get current positon of Alice */
	UpdateState();
	
	cout << endl;
	cout << "Current State: " << endl;
	cout << "t   = " << m_state.Timestamp << endl;
	cout << "x   = " << m_state.Easting   << '\t' << "y   = " << m_state.Northing  << '\t' << "z   = " << m_state.Altitude << endl;
	cout << "xd  = " << m_state.Vel_N     << '\t' << "yd  = " << m_state.Vel_E     << '\t' << "zd  = " << m_state.Vel_D    << endl;
	cout << "xdd = " << m_state.Acc_N     << '\t' << "ydd = " << m_state.Acc_E     << '\t' << "zdd = " << m_state.Acc_D    << endl;
	cout << "p   = " << m_state.Roll      << '\t' << "q   = " << m_state.Pitch     << '\t' << "r   = " << m_state.Yaw      << endl;
	cout << "pd  = " << m_state.RollRate  << '\t' << "qd  = " << m_state.PitchRate << '\t' << "rd  = " << m_state.YawRate  << endl;
	cout << "pdd = " << m_state.RollAcc   << '\t' << "qdd = " << m_state.PitchAcc  << '\t' << "rdd = " << m_state.YawAcc   << endl;
	cout << endl;
	//cout << "Current location: " << m_state.Northing << " N, " << m_state.Easting << " E" << endl;

	/* Extract the data from the new RDDF file */
    	int NofWayPoints = m_RDDF->getNumTargetPoints(); 
	cout << "No. of Waypoints = " << NofWayPoints << endl;
	
	m_RDDFVector = m_RDDF->getTargetPoints();
	for(m_i=0; m_i < m_RDDF->getNumTargetPoints(); m_i++)
	{
		cout << "Waypoint_" << m_RDDFVector[m_i].number+1 << endl;
		cout << "x_coordinate = " << m_RDDFVector[m_i].Easting << endl;
		cout << "y_coordinate = " << m_RDDFVector[m_i].Northing << endl;
		cout << "Maximum Speed = " << m_RDDFVector[m_i].maxSpeed << endl;
		cout << "Radius = " << m_RDDFVector[m_i].radius << endl;
		cout << endl;
	}

	/* Save a file with the RDDF Data */
	int NofRows = NofWayPoints;	int NofCols = 5;
	double** Data = new double*[NofRows];
	for(m_i=0; m_i < NofRows ; m_i++)
	{
		Data[m_i] = new double[NofCols];
		
		Data[m_i][0] = m_RDDFVector[m_i].number + 1;
		Data[m_i][1] = m_RDDFVector[m_i].Easting;
		Data[m_i][2] = m_RDDFVector[m_i].Northing;
		Data[m_i][3] = m_RDDFVector[m_i].maxSpeed;
		Data[m_i][4] = m_RDDFVector[m_i].radius;
	}
	
	bool bAppend = false;
	const char* const sFileName = "RDDFData.dat";
	m_Error = OTG_WriteFile(sFileName, &NofRows, &NofCols, (const double** const) Data, bAppend);

	cout << "Printing RDDF - First time" << endl;
  	m_RDDF->print();
  	
	//int rddfSocket = m_skynet.listen(SNrddf,MODtrafficplanner);
	int rddfSocket = m_skynet.listen(SNrddf, MODrddftalkertestsend);

	cout << "about to receive an rddf ...\n";
	RecvRDDF(rddfSocket, m_RDDF);

  	cout << "Printing RDDF - Second time" << endl;
  	m_RDDF->print();

	/* Obtain OCProblem initial condition - reading from file for now.  */
	const char* const m_sInitFileName = "Alice_InitialConditions.dat";
	m_Error = OTG_ReadFile(m_sInitFileName, &m_NofFlatOutputs, m_NofDimensionsOfFlatOutputs, m_NofWeights, (double** const) m_Weights, m_NofControlPoints, (double*** const) m_ControlPoints);

	m_SolverInform = 0;
	m_OptimalCost  = 0;

        /* Solve OCProblem */
	cout << "----------- NURBSBasedOTG -----------" << endl;
	cout << "Solving Optimal Control Problem ..." << endl;
	m_InitialTime = clock();
	m_OCPSolver->SolveOCProblem(&m_SolverInform, &m_OptimalCost, m_Weights, m_ControlPoints);
	m_FinalTime   = clock();
	cout << "OCP Solver returned with the following flag: " << m_SolverInform << endl; 

	m_Duration = (double)(m_FinalTime-m_InitialTime)/CLOCKS_PER_SEC;
	cout << "Time NURBSBasedOTG took to solve the OCProblem  = "<< m_Duration << " secs" << endl;
	cout << "-------------------------------------" << endl;

	/* Send Trajectory - saving to file for now */
	const char* const m_sSolFileName = "Alice_OptimalSolution.dat";
	bool Append = false;
	m_Error = OTG_WriteFile(m_sSolFileName, &m_NofFlatOutputs, m_NofDimensionsOfFlatOutputs, m_NofWeights, (double** const) m_Weights, m_NofControlPoints, (double*** const) m_ControlPoints, Append);

	/* Initialize the trajectory */
	m_Traj->setNumPoints(2);

	/* Store the trajectory */
	m_Traj->setNorthing(0, m_state.Northing);
	m_Traj->setEasting(0, m_state.Easting);

	m_Traj->setNorthing(1, m_state.Northing + 10);
	m_Traj->setEasting(1, m_state.Easting - 5);

	/* Sending  trajectory */
	SendTraj(m_SendSocket, m_Traj);

	return;
}

void CDPlannerModule::SwitchOCProblem(const AliceProblemType* const apType)
{
	m_apType = *apType;	
}

