/*
 *  CDPlannerModule.hh
 *  CDPlannerModule
 *
 *  Created by Melvin E. Flores on 11/26/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

//#include <pthread.h>

#include "Alice_Nominal.h"
#include "OTG_OCProblem.h"
#include "OTG_OCPSolver.h"
#include "OTG_Utilities.h"

#include "StateClient.h"
#include "TrajTalker.h"
#include "RDDFTalker.hh"
#include "CPath.hh"
#include "CMap.hh"
#include "CMapPlus.hh"
#include "MapConstants.h"
#include "DGCutils"

enum AliceProblemType{apt_Nominal, apt_BackingUp};

class CDPlannerModule:public CStateClient, public CTrajTalker, public CRDDFTalker
{
    public:
    	CDPlannerModule(int SkynetKey, bool WAIT_STATE);
       ~CDPlannerModule(void);

		void ActiveLoop(void);
		void SwitchOCProblem(const AliceProblemType* const apType);

    private:
		int m_i;
		int m_j;
		int m_Error;
		
		RDDF*     m_RDDF;
		CTraj*    m_Traj;
		CMapPlus* m_CostMapPlus;
		//CMap* m_CostMap;

		RDDFVector m_RDDFVector;

		int m_SendSocket;
   		AliceProblemType m_apType;

		clock_t m_InitialTime;
		clock_t m_FinalTime;
		double m_Duration;
	
	   	CAlice_Nominal* m_Alice_Nominal;
		COCProblem*     m_OCProblem;
		COCPSolver*     m_OCPSolver;
				
		int m_SolverInform;
		double m_OptimalCost;
				
		int  m_NofFlatOutputs;
		int* m_NofWeights;
		int* m_NofDimensionsOfFlatOutputs;
		int* m_NofControlPoints;
												
		double**  m_Weights;
		double*** m_ControlPoints;
		
};

