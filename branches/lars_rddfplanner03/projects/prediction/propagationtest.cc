#include "Vector.h"
#include <fstream>
#include <math.h>


using namespace std;


void propagateCivic(Vector& civic, double elapsed_time)
{

  double a, K, d, delta_theta;

  Vector new_part(6);

  a = civic.getElement(4);

  K = civic.getElement(6);

  if(fabs(K) < 1E-6)
    {
      K = 1E-6;
    }

      //d = .5at^2 + vt
  d = .5*a*elapsed_time*elapsed_time + civic.getElement(5)*elapsed_time;
  
  delta_theta = K*d;


  //Now create a new particle and set the appropriate values.


  //x
  new_part.setElement(1, civic.getElement(1) + (1/K)*(sin(civic.getElement(3) + delta_theta) - sin(civic.getElement(3))));

  //y
  new_part.setElement(2, civic.getElement(2) - (1/K)*(cos(civic.getElement(3) + delta_theta) - cos(civic.getElement(3))));

  //theta2 = theta1 + delta_theta
  new_part.setElement(3, civic.getElement(3) + delta_theta);

  //a2 = a1
  new_part.setElement(4, a);

  //v2 = v1 + at
  new_part.setElement(5, civic.getElement(5) + a*elapsed_time);

  //phi2 = phi1
  new_part.setElement(6, K);

  civic = new_part;
}

int main(void)
{

  //Initialize my KICKASS 2003 Honda Civic.
  Vector civic(6);

  civic.setElement(1, 0);  //Start at origin.
  civic.setElement(2, 0);  //Start at origin.  
  civic.setElement(3, M_PI/4);  //Heading 45 degrees northeast
  civic.setElement(4, 0);  //Zero acceleration.
  civic.setElement(5, 10); //10 m/s
  civic.setElement(6, (double)1/3.0);  //Curvature


  ofstream outfile("propagationtest.dat");

  //Ok, now propagate the particle forward in time for 15 seconds, .1 seconds at a time.

  for(int i = 1; i <= 150; i++)
    {
      propagateCivic(civic, .1);

	for(int c = 1; c <= 6; c++)
	  {
	    outfile << civic.getElement(c) <<" ";
	  }
	outfile <<"Distance from center of turn = "<<sqrt(civic.getElement(1)*civic.getElement(1) + (civic.getElement(2) + 10.326)*(civic.getElement(2) + 10.326));  //(x - 0)^2 + (y - 10.3)^2 = distance from center of turn, should be a constant in the log.

      outfile<<endl;
    }
  
  outfile.close();
}









