#ifndef __GENERALMATRIX_H__
#define __GENERALMATRIX_H__

#include <iostream>
#include <assert.h>
#include <math.h>
#include "IndexOutOfBounds.h"
#include "UndefinedOperation.h"

using namespace std;


//NOTE:  Although this class can be used simply as a storage container, it is meant to be used as a generalized matrix for the manipulation of MATHEMATICAL objects.  As such, much of the functionality below depends on class T supporting methods matching the following signatures.

//T (const long double copy)

//T (const T& copy)

//T operator = (const T& other)

//T operator + (const T& other) const

//T operator - (const T& other) const

//T operator * (const long double scalar) const

//T operator * (const T& other) const

//T operator / (const T& other) const

//T operator == (const T& other) const

//T operator != (const T& other) const

template <class T>
class GeneralMatrix
{

 protected:

  int rows, columns;

  T* matrix;

 public:

  GeneralMatrix(const int r, const int c)
  {
      if((r < 1) || (c < 1))
	{
	  throw IndexOutOfBounds("Error: Matrices must be at least 1 x 1.");
	}

      rows = r;
      columns = c;
 
  matrix = new T [rows*columns];
  }

  GeneralMatrix(const int order)
  {
    if(order < 1)
      {
	throw IndexOutOfBounds("Error: Matrices must be at least 1 x 1.");
      }
    
    rows = order;
    columns = order;
    matrix = new T[rows*columns];
 
  }

  GeneralMatrix(const GeneralMatrix<T>& other)
  {
    rows = other.getRows();
    columns = other.getColumns();
    matrix = new T[rows*columns];
    
    for(int r = 1; r<= rows; r++)
      {
	for(int c = 1; c <= columns; c++)
	  setElement(r,c, other.getElement(r,c));
      }
  }

  GeneralMatrix<T>& operator = (const GeneralMatrix<T>& b)
  {
 
    if(&b != this) //Check for self-assignment
      {
	T* temp = matrix;
	rows = b.getRows();
	columns = b.getColumns();
	matrix = new T[rows*columns];
      
	for(int r = 1; r<= rows; r++)
	  {
	    for(int c = 1; c <= columns; c++)
	      {
		setElement(r,c, b.getElement(r,c));
	      }
	  }
	delete [] temp;
      }

    return (*this);
  }


  int getRows() const
  {
    return rows;
  }

  int getColumns() const
  {
    return columns;
  }

  int getOrder() const
  {
    if(!isSquare())
      {
	throw UndefinedOperation("Error:  The order property is only defined for square matrices.");
      }
    return rows;
  }
  
  T getElement(const int row, const int col) const
  {
    if((row < 1) || (row >  rows) ||  (col < 1) ||  (col >  columns))
      {
	throw IndexOutOfBounds();
      }
    
    return matrix[(row - 1)*columns + (col-1)];
  }

  void setElement(const int row, const int col, const T& val)
  {
    if((row < 1) || (row >  rows) ||  (col < 1) ||  (col >  columns))
      {
	throw IndexOutOfBounds();
      }
    
    matrix[(row - 1)*columns  + (col-1)] = val;
  }

  GeneralMatrix<T> operator + (const GeneralMatrix<T>& b) const
  {
    if((getRows() != b.getRows()) ||  (getColumns() != b.getColumns()))
      {
	throw UndefinedOperation("Error: Matrix addition is only defined for matrices having the same dimensions.");
      }

    GeneralMatrix<T> answer(getRows(), getColumns());
    for(int r = 1; r <= getRows(); r++)
      {
	for(int c = 1; c <= getColumns(); c++)
	  {
	    answer.setElement(r,c, getElement(r,c) + b.getElement(r,c));
	  }
      }
    return answer;
  }

  GeneralMatrix<T> operator - (const GeneralMatrix<T>& b) const
  {
    if((getRows() != b.getRows()) ||  (getColumns() != b.getColumns()))
      {
	throw UndefinedOperation("Error: Matrix subtraction is only defined for matrices having the same dimensions.");
      }

    GeneralMatrix<T> answer(getRows(), getColumns());
    for(int r = 1; r <= getRows(); r++)
      {
	for(int c = 1; c <= getColumns(); c++)
	{
	  answer.setElement(r,c, getElement(r,c) - b.getElement(r,c));
	}
      }
    return answer;
  }

  GeneralMatrix<T> scalar(const long double k) const
  {
    GeneralMatrix<T> answer(getRows(), getColumns());
    for(int r = 1; r<= getRows(); r++)
      {
	for(int c = 1; c<= getColumns(); c++)
	  answer.setElement(r,c, getElement(r,c)*k);
      }
    return answer;
  }

  T determinant() const
  {
    if(!isSquare())
      {
	throw UndefinedOperation("Error: Determinants are only defined for square matrices.");
      }
    
    if(getOrder() == 1)
      {
	return getElement(1,1);
      }
    
    else if(getOrder() == 2)
      {
	return getElement(1,1)*getElement(2,2) - getElement(1,2)*getElement(2,1);
      }

    else
      {

	T answer = getElement(1,1)*cofactor(1,1);

	for(int c = 2; c<= getOrder(); c++)
	  {
	    answer = answer + getElement(1, c)*cofactor(1,c);
	  }

	return answer;
      }
  }

  //Returns the submatrix of the given matrix formed by removing row "row" and column "col".
  GeneralMatrix<T> submatrix(const int row, const int column) const
  {
    if((row < 1) || (row >  rows) ||  (column < 1) ||  (column >  columns))
      {
	throw IndexOutOfBounds();
      }

    GeneralMatrix<T> temp(getRows() - 1, getColumns() - 1);
    int a = 1;
    int b;
    for(int r = 1; r <= getRows(); r++)
      {
	if(r != row)
	  {
	    b = 1;
	    for(int c = 1; c <= getColumns(); c++)
	      {
		if(c != column)
		  {
		    temp.setElement(a,b, getElement(r,c));
		    b++;
		  }
	      }
	    a++;
	  }
      }
    
    return temp;
  }
  
  T cofactor(const int row, const int col) const
  {
    if(!isSquare())
      {
	throw UndefinedOperation("Error: Cofactors of elements in a matrix are only defined for square matrices."); 
      }
    
    return submatrix(row, col).determinant()*pow(-1, row+col);
  }

  GeneralMatrix<T> transpose() const
  {
    GeneralMatrix<T> temp(getRows(), getColumns());
    for(int r = 1; r <= getRows(); r++)
      {
	for(int c = 1; c <= getColumns(); c++)
	  {
	    temp.setElement(c, r, getElement(r,c));
	  }
      }
    
    return temp;
  }

  GeneralMatrix<T> inverse() const
    {
      if(!isSquare())
	{
	  throw UndefinedOperation("Error: Only square matrices can have inverses."); 
	}
      
      T det = determinant();

      if(det == (T)0)
	{
	  throw UndefinedOperation("Error: Singular matrices do not have inverses.");
	}

      GeneralMatrix<T> temp(getOrder());
      for(int r = 1; r <= getOrder(); r++)
	{
	  for(int c = 1; c <= getOrder(); c++)
	    {
	      temp.setElement(r,c, cofactor(r,c));
	    }
	}
      temp = temp.transpose();

      for(int r = 1; r<= rows; r++)
	{
	  for(int c = 1; c<= columns; c++)
	    {
	      temp.setElement(r, c, getElement(r, c)/det);
	    }
	}
      return temp;
}
  
  bool operator == (const GeneralMatrix<T>& b) const
  {
    bool temp = true;
    if((getRows() != b.getRows()) || (getColumns() != b.getColumns()))
      {
	temp = false;
      }
    
    else
      {
	for(int r = 1; r <= getRows(); r++)
	  {
	    for(int c = 1; c <= getColumns(); c++)
	      {
		if(getElement(r,c) != b.getElement(r,c))
		  {
		    temp = false;
		  }
	      }
	  }
      }
    return temp;
  }

  bool operator != (const GeneralMatrix<T>& b) const
    {
      return !((*this) == b);
    }

  bool isSquare() const
  {
    return (getRows() == getColumns());
  }
  
  bool isSymmetric() const
  {
    bool temp;
    if(!isSquare())
      {
	temp =  false;
      }
    else
      {
	temp = ((*this) == (*this).transpose());
      }
    return temp;
  }
  
  bool isSkewSymmetric() const
  {
    bool temp;
    if(!isSquare())
      {
	temp = false;
      }
    else
      {
	temp = ((*this).scalar(-1) == (*this).transpose());
      }
    return temp;
  }

  void interchange_rows(int a, int b)
  {
    T temp [getColumns()];

    //Copy elements from row a into this array
    for(int i = 0; i < getColumns(); i++)
      {
	temp[i] = getElement(a, i + 1);
      }

    //Copy elements from row b into row a
    for(int c = 1; c <= getColumns(); c++)
      {
	setElement(a, c, getElement(b, c));
      }

    //Copy elements from the array (a's old elements) into b
    for(int c = 1; c <= getColumns(); c++)
      {
	setElement(b, c, temp[c-1]);
      }
  }

  void print() const
  {
    for(int r = 1; r <= rows; r++)
      {
	for(int c = 1; c <= columns; c++)
	  {
	    getElement(r, c).print();
	    cout<<" ";
	  }
	cout<<endl;
      }
  }

  ~GeneralMatrix()
  {
    delete [] matrix;
  }

};

#endif // __GENERALMATRIX_H__
