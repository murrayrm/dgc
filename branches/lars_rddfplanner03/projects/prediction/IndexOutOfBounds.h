#ifndef __INDEXOUTOFBOUNDS_H__
#define __INDEXOUTOFBOUNDS_H__

class IndexOutOfBounds
{

protected:

char* error;
  
public:
  

  IndexOutOfBounds(char* message = "Error: Index out of bounds")
  {
    error = message;
  }

  ~IndexOutOfBounds()
  {
    delete [] error;
  }
};

#endif //__INDEXOUTOFBOUNDS_H__
