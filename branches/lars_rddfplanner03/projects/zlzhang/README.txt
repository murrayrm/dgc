
                                       _______________        ______________
processed image (from Humberto)  -->  |__roadFinding__|  --> |__laneMarker__|
      _______________
-->  |__roadMapper__|  --> traffic map




First, I will receive processed images as well as the original stereovision
image from Humberto's module (roadFinding), which will provide detail infor-
mation about the position of the road relative to the vehicle.  Then I will run
visionScan on the processed image and identify the possible boundaries of the 
lanes (multiple threads on continuous linear patterns of large contrasting point
s). The lane painter will double check with the original image of the stereo-
vision and identify those point patterns as lanes.  Then the lanePainter will 
pass info to the roadMapper, which will provide detail description about the 
boudaries of the lanes, the traffic direction of each lane and the position of 
the curbs.  




