#ifndef BOOL_COUNTER_HH
#define BOOL_COUNTER_HH

//Defines a class working as a boolean, but keeping count of state changes

class bool_counter
{
 public:
  bool_counter() : data(false), cnt(0) {}
  bool_counter(bool b) : data(b), cnt(0) {}
  bool_counter(const bool_counter &bc) : data(bc.data), cnt(bc.cnt) {}
  
  bool_counter &operator=(bool b) {
    data = b;
    if(data)
      ++cnt;
    else
      cnt=0;

    return *this;
  }

  bool_counter &operator=(const bool_counter &bc) {
    data = bc.data;
    cnt = bc.cnt;
    return *this;
  }

  bool operator==(const bool_counter &bc) const {
    return data == bc.data;
  }

  bool operator==(const bool &b) const {
    return data == b;
  }    

  void reset() { cnt=0; }

  int count() const { return cnt; }

  operator bool() { return data; }
  operator bool() const { return data; }

  //Sparrow interface functions
  bool getter() { return data; }
  void setter(bool *p, bool val) { operator=(val); }

  int get_count() { return cnt; }
  void set_count(int *p, int c) { cnt = c; }
 private:
  bool data;
  int cnt;
};



#endif //BOOL_COUNTER_HH
