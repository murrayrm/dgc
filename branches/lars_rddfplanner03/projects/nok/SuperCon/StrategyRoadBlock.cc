//SUPERCON STRATEGY TITLE: Road Block - SOMETIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyRoadBlock.hh"
#include "StrategyHelpers.hh"

void CStrategyRoadBlock::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE
  currentStageName = roadblock_stage_names_asString( roadblock_stage_names(stgItr.nextStage()) );
  
  switch( stgItr.nextStage() ) {

  case s_roadblock::place_obstacle: 
    //superCon places an obstacle in the Route map, making the segment unusable
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Road Block: %s", currentStageName.c_str() ) );

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      if( m_pdiag->validAliceStop == false ) {
	SuperConLog().log( STR, WARNING_MSG, "WARNING: Road Block called when not in a valid stop" );
      }

      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	stage_
	//SuperCon should place a Route level obstacle on the current segmenet at this point -- How?

	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "RoadBlock - Stage - %s completed", currentStageName.c_str() );	
	stgItr.incrStage();
      }
    }
    break;

  case s_roadblock::make_uturn:
    //Road block should be placed in the the route map, so we need
    //to turn around so that the route mapper will give us a route that
    //is consistent with the fact that we will be heading out that way.
    { 
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = checkAnytimeEntryConds( m_pStrategyInterface, m_pdiag, strprintf( "Road Block: %s", currentStageName.c_str() ) ); 

      if( skipStageOps == false ) {
	skipStageOps = trans_EndOfMission( (*m_pdiag), currentStageName.c_str() );
      }
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT*/
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      // Need to implement some block placement completion check in diags
      if( m_pdiag->block_placed_complete == false ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "StrategyRoadBlock - confirmation of road block placement NOT yet received from RouteMapper -> will poll" );
	//Timeout handled here. If Route map for some reason refuses to put the
	//block into the map, it shouldn't present an immediate problem. This
	//is because by the time we listen to the route planner again, we will
	//facing away from the barrier. The next time we head down this street,
	//we should register the road block if it hasn't been cleared.
        if( stgItr.currentStageCount() > TIMEOUT_LOOP_PLACE_BLOCK ) {
	  SuperConLog().log( STR, WARNING_MSG, "WARNING: StrategyRoadBlock - place block timed out -> hope for best" );
	}

      }      
 
      /* ALL conditional checks should be completed ABOVE this point */       	  
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if (skipStageOps == false) {
	transitionStrategy( StrategyUturn , "RoadBlock - Strategy complete --> Uturn" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "RoadBlock - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }      
    }
 
  /** END OF STRATEGY **/
    
  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyRoadBlock::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyRoadBlock::stepForward - default case in nextStep switch() reached" );
      transitionStrategy(StrategyNominal, "ERROR: RoadBlock - default stage reached!" );
    }
  }
}


void CStrategyRoadBlock::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Road Block - Exiting" );
}

void CStrategyRoadBlock::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  skipStageOps.reset(); //resets to FALSE 
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "Road Block - Entering" );
}


