#ifndef TRAFFICPLANNER_HH
#define TRAFFICPLANNER_HH

#include "StateClient.h"
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <string>
#include "missionUtils.hh"
#include "SegGoalsTalker.hh"

class CTrafficPlanner : public CStateClient, public CSegGoalsTalker
{
  SegGoalsStatus* segGoalsStatus;

public:
  /*! Contstructor */
  CTrafficPlanner(int skynetKey, bool bWaitForStateFill);
  /*! Standard destructor */
  ~CTrafficPlanner();
  void TPlanningLoop(void);
};

#endif  // TRAFFICPLANNER_HH
