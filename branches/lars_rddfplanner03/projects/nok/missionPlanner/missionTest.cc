#include "missionUtils.hh"

int main(int argc, char* argv[])
{ 
  RNDF* rndf;
  rndf = new RNDF();

  if (argc != 3)
  {
    argv[1] = "DARPA_RNDF.txt";
    argv[2] = "DARPA_MDF.txt";
  }

  if(!rndf->loadFile(argv[1]))
  {
    cout << "Error: Unable to load RNDF file, exiting program." << endl;
    exit(0);
  }

  assignLaneDirection(rndf);
  
  for (int i=1; i <= rndf->getNumOfSegments(); i++)
  {
    for (int j=1; j <= rndf->getSegment(i)->getNumOfLanes(); j++)
    {
      cout << "Lane " << i << "." << j << ":\t\t" << rndf->getLane(i,j)->getDirection() << endl;
    }
  }
  
  Graph* rndfGraph = new Graph();
  
  addAllVertices(rndf, rndfGraph);
  
  addAllEdges(rndf, rndfGraph);

/*
  // Test addGPSPointToGraph
  bool pointAdded = addGPSPointToGraph (rndf->getWaypoint(9, 2, 3), rndfGraph, rndf);
  if (pointAdded)
  {
    vector<Vertex*> vertices = rndfGraph->getVertices();

    for (unsigned i = 0; i < vertices.size(); i++)
    {
      vector<Edge*> edges = vertices[i]->getEdges();
      for (unsigned j = 0; j < edges.size(); j++)
      {
	vertices[i]->print();
	cout<<" -> ";
	edges[j]->getNext()->print();
	cout<<"\t\t"<<edges[j]->getWeight()<<endl;
      }
    }
  }
*/

/*
  // Test MDFparser
  vector<Waypoint*> mission = rndf->getMission();
  for (unsigned i = 0; i < mission.size(); i++)
  {
    mission[i]->print();
    cout<<endl;
  }
*/

/*
  // Test findRoute and findType
  vector<Vertex*> vertices = rndfGraph->getVertices();
  vector<Vertex*> route;
  bool routeFound;
  double cost;

  Vertex* vertex1 = rndfGraph->getVertex(1, 2, 5);
  Vertex* vertex2 = rndfGraph->getVertex(11, 1, 4);

  routeFound = findRoute(vertex1, vertex2, rndf, rndfGraph, route, cost);

  cout<<"Route from waypoint ";
  vertex1->print();
  cout<<" to waypoint ";
  vertex2->print();
  cout<<endl;
  if (!routeFound)
    cout << "No route exists" << endl;

  for (unsigned i = 0; i < route.size()-1; i++)
  {
    route[i]->print();
    cout << "\t -> \t";
    route[i+1]->print();
    cout<<"\t\t"<<findType(route[i], route[i+1], rndf);
    cout<<"\t\t"<<route[i]->getCostToCome()<<endl;
  }
*/

/*
  // Test findSegGoals
  vector<Vertex*> vertices = rndfGraph->getVertices();
  vector<Vertex*> route;
  bool routeFound;
  double cost;

  Vertex* vertex1 = rndfGraph->getVertex(9, 2, 3);
  Vertex* vertex2 = rndfGraph->getVertex(4, 1, 3);

  routeFound = findRoute(vertex1, vertex2, rndf, rndfGraph, route, cost);
  vector<SegGoals> segGoals = findSegGoals(route, rndf);
  printMission(segGoals);
*/

/*
  // Test findClosestGPSPoint
  GPSPoint* point = rndf->getWaypoint(3,1,2);
  double northing, easting, distance;
  char UTMZone[4];
  int RefEllipsoid = 23;

  LLtoUTM(RefEllipsoid, point->getLatitude(), point->getLongitude(), northing, easting, UTMZone);
  northing += 100;
  GPSPoint* closestPoint = findClosestGPSPoint(northing, easting, 0, 0, distance, rndf);

  cout << "The closest waypoint to the position (" << northing << ", " << easting << "): " << closestPoint->getSegmentID() <<
    "." << closestPoint->getLaneID() << "." << closestPoint->getWaypointID() <<endl;
  cout << "Distance: " << distance << endl;

  
  GPSPoint* point2 = rndf->getWaypoint(3,2,13);
  double northing2, easting2, distance2;

  LLtoUTM(RefEllipsoid, point2->getLatitude(), point2->getLongitude(), northing2, easting2, UTMZone);
  distance2 = sqrt(square(easting2 - easting) + square(northing2 - northing));  

  cout << "The distance from 3.2.13: " << distance2 << endl;
*/

/*
  // Test removeEdge
  vector<Edge*> edges = rndfGraph->getVertex(13, 1, 8)->getEdges();
  for (unsigned i = 0; i < edges.size(); i++)
    edges[i]->getNext()->print();
  bool edgeRemoved = rndfGraph->removeEdge(13,1,8,13,1,9);
  cout<<edgeRemoved<<endl;
  edges = rndfGraph->getVertex(13, 1, 8)->getEdges();
  if (edges.size() == 0)
    cout<<"edge removed"<<endl;
  for (unsigned i = 0; i < edges.size(); i++)
    edges[i]->getNext()->print();
*/

/*
  // Test findAllExits
  Vertex* entryVertex = rndfGraph->getVertex(13, 1, 8);
  vector<Vertex*> allExits = findAllExits(entryVertex, rndf, rndfGraph);
  cout<<"Exit points associated with waypoint ";
  entryVertex->print();
  cout<<": ";
  for(unsigned i = 0; i < allExits.size(); i++){
    (allExits[i])->print();
    cout<<"  ";
  }
  cout<<endl;
*/
  delete rndfGraph;  
  delete rndf;
  
  return 0;
}
