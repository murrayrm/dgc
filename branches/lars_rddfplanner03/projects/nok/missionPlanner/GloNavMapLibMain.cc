#include <getopt.h>
#include <iostream>
#include "GloNavMapLib.hh"
#include "DGCutils"

enum {
  OPT_NONE,
  OPT_HELP,
  OPT_RNDF,
  NUM_OPTS
};

using namespace std;

// Default options
int NOWAIT = 0;                        // wait for state data to fill
char* RNDFFileName = "DARPA_RNDF.txt"; // name of RNDF file


/* The name of this program. */
const char * program_name;

/* Prints usage information for this program to STREAM (typically stdout 
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream, "  --nowait \t\tDo not wait for state to fill, plan from vehicle state.\n"); 
  fprintf( stream, "  --rndf RNDFFileName \tSpecifies the filename RNDFFileName for RNDF.\n"); 
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  int ch;
  int option_index = 0;

  /* An array describing valid long options. */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"nowait",     no_argument,       &NOWAIT,        1},
    {"help",       no_argument,       0,           OPT_HELP},
    {"rndf",       required_argument, 0,           OPT_RNDF},
    {0,0,0,0}
  };

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long_only(argc, argv, "", long_options, &option_index)) != -1)
  {
    switch(ch)
    {
      case OPT_HELP:
        /* User has requested usage information. Print it to standard
           output, and exit with exit code zero (normal 
           termination). */
        print_usage(stdout, 0);
	break;

      case '?': /* The user specified an invalid option. */
        /* Print usage information to standard error, and exit with exit
           code one (indicating abnormal termination). */
        print_usage(stderr, 1);
	break;

      case OPT_RNDF:
	RNDFFileName = optarg;
	break;

      case -1: /* Done with options. */
        break;
    }
  }

  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
  {
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  }
  else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  CGloNavMapLib* pGloNavMapLib = new CGloNavMapLib(sn_key, RNDFFileName);
  
  DGCstartMemberFunctionThread(pGloNavMapLib, &CGloNavMapLib::sendGlobalGloNavMapThread);
  DGCstartMemberFunctionThread(pGloNavMapLib, &CGloNavMapLib::sendLocalGloNavMapThread);

  pGloNavMapLib->getGlobalGloNavMapThread();

  return 0;
}
