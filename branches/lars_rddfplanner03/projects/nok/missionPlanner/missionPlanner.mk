MISSIONPLANNER_PATH = $(DGC)/projects/nok/missionPlanner

MISSIONPLANNER_DEPEND_LIBS = \
	$(SPARROWLIB) \
	$(SKYNETLIB) \
	$(MODULEHELPERSLIB) \
	$(SPARROWHAWKLIB) \
	$(GGISLIB)
	
MAPLIBS = \
	../../sidd/Mapper/Map.o \
	../../sidd/Mapper/LogicalRoadObject.o \
	../../sidd/Mapper/RoadObject.o

MISSIONPLANNER_DEPEND_SOURCE = \
	$(MISSIONPLANNER_PATH)/MissionPlanner.hh \
	$(MISSIONPLANNER_PATH)/MissionPlanner.cc \
	$(MISSIONPLANNER_PATH)/MissionPlannerMain.cc \
        $(MISSIONPLANNER_PATH)/Makefile

MISSIONPLANNER_DEPEND = \
	$(MISSIONPLANNER_DEPEND_LIBS) \
	$(MISSIONPLANNER_DEPEND_SOURCE)