#include "LocalMapTalker.hh"

using namespace std;
using namespace Mapper;

class CLocalMapTalkerTestRecv : public CLocalMapTalker
{
public:
	CLocalMapTalkerTestRecv(int sn_key)
		: CSkynetContainer(MODtrafficplanner, sn_key)
	{
		vector<Segment> segments;
		Map map(segments);

		cout << "about to listen...";
		int mapSocket = m_skynet.listen(SNtrafficLocalMap, MODmapping);
		cout << " listening!" << endl;
//		while(true)
//		{
			int size;
			cout << "about to receive a map...";
			RecvLocalMap(mapSocket, &map, &size);
			cout << " received a map!" << endl;
//		}
	}
};

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;

	CLocalMapTalkerTestRecv test(key);
}
