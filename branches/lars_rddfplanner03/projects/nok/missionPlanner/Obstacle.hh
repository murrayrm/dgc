#ifndef OBSTACLE_HH_
#define OBSTACLE_HH_
#include <string>
#include <vector>
using namespace std;

/*! Obstacle class. All obstacles have a segment ID, lane IDs, and waypoint IDs that
 *  it is behind. Obstacles may have optional information including obstalce width.
 * \brief The Obstacle class represents an obstacle.
 */
class Obstacle
{
public:
/*! All obstacles have a segment ID, lane IDs, and waypoint IDs. */
  Obstacle(int, int, vector<int>, vector<int>);
  Obstacle(int, int, int, int);
  virtual ~Obstacle();

/*! Returns the ID of THIS. */
  int getObstacleID();

/*! Returns the segment ID of THIS. */
  int getSegmentID();
  
/*! Returns the lane ID of THIS. */
  vector<int> getLaneIDs();
  
/*! Returns the waypoint ID that THIS is behind. */
  vector<int> getWaypointIDs();

/*! Returns the number of lanes that THIS blocks */
  int getNumOfLanesBlocked();
  
/*! Returns the width of THIS. */
  double getObstacleWidth();
    
/*! Sets the width of THIS. */
  void setObstacleWidth(int);

/*! Adds obstructed lane and waypoint */
  bool addObstructedLane(int laneID, int waypointID);
  
private:
  int obstacleID;
  int segmentID;
  vector<int> laneIDs, waypointIDs;
  double width;
};

#endif /*OBSTACLE_HH_*/
