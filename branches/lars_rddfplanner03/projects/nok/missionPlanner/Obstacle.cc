#include "Obstacle.hh"
#include <string>
#include <iostream>
using namespace std;

Obstacle::Obstacle(int id, int m, vector<int> n, vector<int> p)
{
  obstacleID = id;
  segmentID = m;
  laneIDs = n;
  waypointIDs = p;
}

Obstacle::Obstacle(int id, int m, int n, int p)
{
  obstacleID = id;
  segmentID = m;
  laneIDs.push_back(n);
  waypointIDs.push_back(p);
}

Obstacle::~Obstacle()
{
  // Nothing needs to be done here
}

int Obstacle::getObstacleID()
{
  return obstacleID;
}

int Obstacle::getSegmentID()
{
  return segmentID;
}

vector<int> Obstacle::getLaneIDs()
{
  return laneIDs;
}

vector<int> Obstacle::getWaypointIDs()
{
  return waypointIDs;
}

int Obstacle::getNumOfLanesBlocked()
{
  return (int)laneIDs.size();
}

double Obstacle::getObstacleWidth()
{
  return width;
}

void Obstacle::setObstacleWidth(int width)
{
  this->width = width;
}

bool Obstacle::addObstructedLane(int laneID, int waypointID)
{
  bool obsFound = false;
  unsigned i = 0;
  while (!obsFound && i < laneIDs.size())
  {
    if (laneIDs[i] == laneID)
      obsFound = true;
    i++;
  }
  if (!obsFound)
  {
    laneIDs.push_back(laneID);
    waypointIDs.push_back(waypointID);
  }
  return !obsFound;
}
