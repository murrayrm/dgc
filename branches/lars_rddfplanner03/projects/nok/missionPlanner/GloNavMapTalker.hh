#ifndef GLONAVMAPTALKER_HH
#define GLONAVMAPTALKER_HH

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "SkynetContainer.h"
#include "DGCutils"
#include "RNDF.hh"

#define GLONAVMAP_MAX_BUFFER_SIZE 500000

struct SRNDFHeader
{
  SRNDFHeader()
  {
    memset(this, 0, sizeof(*this));
  }
  int numOfSegments;
  int numOfZones;
  int numOfCheckpoints;
  int numOfLanes;
  int numOfPerimeterPoints;
  int numOfSpots;
  int numOfWaypoints;
};

class CGloNavMapTalker : virtual public CSkynetContainer {
  pthread_mutex_t m_gloNavMapDataBufferMutex;
  char* m_pGloNavMapDataBuffer;

public:
  CGloNavMapTalker();
  ~CGloNavMapTalker();

  bool SendGloNavMap(int GloNavMapSocket, RNDF* receivedRndf);
  bool RecvGloNavMap(int GloNavMapSocket, RNDF* rndf, int* pSize);
  
private:
  int m_bufferSize;
};

#endif // GLONAVMAPTALKER_HH
