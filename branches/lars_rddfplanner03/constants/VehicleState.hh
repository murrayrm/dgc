/* VehicleState.hh - platform-independent state struct
 *
 * Lars Cremean
 * 6 Jan 05
 * 
 * This file defines platform-independent state structs intended for use by 
 * various code that constructs, sends or receives state information.
 */

#ifndef VEHICLESTATE_HH
#define VEHICLESTATE_HH

#include <string.h>
#include <math.h>
#include <stdio.h>

// Now needed for VEHICLE_WHEELBASE and the calculation of the partial state at 
// the rear axle of the vehicle.
#include "AliceConstants.h"
#include "frames/coords.hh" // for NEcoord

/** New, clean struct for vehicle state representation.  Originally implemented 
 * as GetVehicleStateMsg in bob/vehlib/VState.hh, but moved here because it is 
 * platform-independent. All variables are defined with respect to the global 
 * coordinate system. */
struct VehicleState
{
	/** Default constructor */
	VehicleState()
	{
		// initialize the state to zeros to appease the memory profilers
		memset(this, 0, sizeof(*this));
	}

  /** This constructor initializes a VehicleState structure from derivative data
			that could come from a CTraj point. Note that no state transformation is
			performed */
	VehicleState(double n, double nd, double ndd,
							 double e, double ed, double edd)
	{
		Northing = n;
		Easting = e;
		Vel_N = nd;
		Vel_E = ed;
		Acc_N = ndd;
		Acc_E = edd;

		Altitude = Vel_D = Acc_D = 0.0;
		Pitch = Roll = RollRate = PitchRate = RollAcc = PitchAcc = 0.0;

		Yaw = atan2(ed, nd);
		YawRate = (nd*edd - ndd*ed) / (nd*nd + ed*ed);
		YawAcc = 0.0;
	}

  /** Timestamp represents UNIX-time. This is the value of microseconds since
   *  the epoch (1/1/1970) */
  unsigned long long Timestamp;

  /** Cartesian UTM Northing coordinate for UTM zone 11S (meters).  This is the 
   * X-axis in our coordinate system conventions, and corresponds to latitude. 
   */
  double Northing;
  /** Cartesian UTM Easting coordinate for UTM zone 11S (meters).  This is the 
   * Y-axis in our coordinate system conventions, and corresponds to longitude.  
   */
  double Easting;
  /** "Altitude" is the Z-axis corresponding to the global coordinate frame.  
   * It is positive /downward/ and zero at sea-level, in meters. */
  double Altitude;

  /** Cartesian UTM Northing coordinate for UTM zone 11S (meters).  This is the 
   * X-axis in our coordinate system conventions, and corresponds to latitude. 
	 * THIS IS FOR NAVCOM
   */
  double GPS_Northing;
  /** Cartesian UTM Easting coordinate for UTM zone 11S (meters).  This is the 
   * Y-axis in our coordinate system conventions, and corresponds to longitude.  
	 * THIS IS FOR NAVCOM
   */
  double GPS_Easting;

  /** Component of linear velocity in the Northing direction (m/s).  */
  double Vel_N;
  /** Component of linear velocity in the Easting direction (m/s).  */
  double Vel_E;
  /** Component of linear velocity in the downward direction (m/s).  */
  double Vel_D;

  /** Component of linear acceleration in the Northing direction (m/s/s).  */
  double Acc_N;
  /** Component of linear acceleration in the Easting direction (m/s/s).  */
  double Acc_E;
  /** Component of linear acceleration in the downward direction (m/s/s).  */
  double Acc_D;

  /** Rotation of the vehicle about the X-axis, positive according the 
   * right-hand rule (positive roll is top-right) (radians).  */
  double Roll;
  /** Rotation of the vehicle about the vehicle Y-axis, positive according the 
   * right-hand rule (positive nose-up) (radians). */
  double Pitch;
  /** Rotation of the vehicle about the Z-axis, measured clockwise from North.  
   * i.e. East is Yaw=pi/2 (radians). Note that if you orient your coordinate
   * system with Northing as x and Easting as y, this becomes the standard angle
   * measurement that starts at the x axis and goes counterclockwise.  */
  double Yaw;

  /** RollRate is d(Roll)/dt (rad/s). */
  double RollRate;
  /** PitchRate is d(Pitch)/dt (rad/s). */
  double PitchRate;
  /** YawRate is d(Yaw)/dt (rad/s). */
  double YawRate;

  /** YawRate is d(Yaw)/dt (rad/s). 
	    This is a raw measurement from the IMU, transformed to the same coordinates as the normal YawRate
	    So it's basically like YawRate, only without any Kalman filtering applied */
	double raw_YawRate;

  /** d^2(Roll)/dt^2 (rad/s/s). */
  double RollAcc;
  /** d^2(Pitch)/dt^2 (rad/s/s). */
  double PitchAcc;
  /** d^2(Yaw)/dt^2 (rad/s/s). */
  double YawAcc;

  double NorthConf;
  double EastConf;
  double HeightConf;
  double RollConf;
  double PitchConf;
  double YawConf;

	//This is a unitless measure of the goodness of the GPS data
	double gamma;


  /** Accessor function to get an NEcoord format of the front of the Vehicle. */
  NEcoord ne_coord() const
  {
    NEcoord ret;
    ret.N = Northing;
    ret.E = Easting;
    return ret;
  }

  /** The scalar component of the velocity vector in the ground plane (m/s). */
  double Speed2(void) 
  {
    return hypot(Vel_N, Vel_E);
  }

  /** The scalar component of the velocity vector in 3D (m/s).  Measurement
   * of Vel_D may have low accuracy, so be careful. */
  double Speed3(void) 
  {
    return sqrt(Vel_N*Vel_N + Vel_E*Vel_E + Vel_D*Vel_D);
  }

  /** The scalar component of the acceleration vector in the ground plane 
   * (m/s/s).  Note that this is NOT the same as d(Speed)/dt, and is not the 
   * longitudinal component of the acceleration vector. */
  double Accel2(void) 
  {
    return hypot(Acc_N, Acc_E);
  }  
  
  /** The scalar component of the acceleration vector in 3D (m/s/s). Note that 
   * this is NOT the same as d(Speed)/dt, and is not the longitudinal component 
   * of the acceleration vector. Measurement of Acc_D may have low accuracy, so 
   * be careful. */
  double Accel3(void) 
  {
    return sqrt(Acc_N*Acc_N + Acc_E*Acc_E + Acc_D*Acc_D);
  }

  /** The Northing of the ground beneath the center of the rear axle [m].  The 
   * return value for this accessor function is calculated based on the nominal 
   * (front-axle) coordinates.  This only implements the /planar/ coordinate 
   * transformation. */
  double Northing_rear(void)
  {
    return (Northing - VEHICLE_WHEELBASE*cos(Yaw));
  }

  /** The Easting of the ground beneath the center of the rear axle [m].  The 
   * return value for this accessor function is calculated based on the nominal 
   * (front-axle) coordinates.  This only implements the /planar/ coordinate 
   * transformation. */
  double Easting_rear(void)
  {
    return (Easting - VEHICLE_WHEELBASE*sin(Yaw));
  }

  /** The GPS_Northing of the ground beneath the center of the rear axle [m].  The 
   * return value for this accessor function is calculated based on the nominal 
   * (front-axle) coordinates.  This only implements the /planar/ coordinate 
   * transformation. FOR NOVATEL */
  double GPS_Northing_rear(void)
  {
    return (GPS_Northing - VEHICLE_WHEELBASE*cos(Yaw));
  }

  /** The GPS_Easting of the ground beneath the center of the rear axle [m].  The 
   * return value for this accessor function is calculated based on the nominal 
   * (front-axle) coordinates.  This only implements the /planar/ coordinate 
   * transformation. FOR NOVATEL */
  double GPS_Easting_rear(void)
  {
    return (GPS_Easting - VEHICLE_WHEELBASE*sin(Yaw));
  }



  /** The first derivative of the Northing at the center of the rear axle of 
   * the vehicle [m/s].  The return value for this accessor function is 
   * calculated based on the nominal (front-axle) coordinates.  This only 
   * implements the /planar/ coordinate transformation. */
  double Vel_N_rear(void)
  {
    return (Vel_N + VEHICLE_WHEELBASE*YawRate*sin(Yaw));
  }

  /** The first derivative of the Easting at the center of the rear axle of the 
   * vehicle [m/s].  The return value for this accessor function is calculated 
   * based on the nominal (front-axle) coordinates.  This only implements the 
   * /planar/ coordinate transformation. */
  double Vel_E_rear(void)
  {
    return (Vel_E - VEHICLE_WHEELBASE*YawRate*cos(Yaw));
  }

	/** Estimate of YawRate based on the bicycle model and cartesian derivatives */
	double YawRateEstimate(void)
	{
		return (Vel_N*Acc_E - Acc_N*Vel_E) / (Vel_N*Vel_N + Vel_E*Vel_E);
	}


  /** Prints the VehicleState member variables */
  void print(void)
  {
    printf("Timestamp = %lld\n", Timestamp);
    printf("Northing  = %f\n", Northing);
    printf("Easting   = %f\n", Easting);
    printf("Altitude  = %f\n", Altitude);
    printf("Vel_N     = %f\n", Vel_N);
    printf("Vel_E     = %f\n", Vel_E);
    printf("Vel_D     = %f\n", Vel_D);
    printf("Acc_N     = %f\n", Acc_N);
    printf("Acc_E     = %f\n", Acc_E);
    printf("Acc_D     = %f\n", Acc_D);
    printf("Roll      = %f\n", Roll);
    printf("Pitch     = %f\n", Pitch);
    printf("Yaw       = %f\n", Yaw);
    printf("RollRate  = %f\n", RollRate);
    printf("PitchRate = %f\n", PitchRate);
    printf("YawRate   = %f\n", YawRate);
    printf("RollAcc   = %f\n", RollAcc);
    printf("PitchAcc  = %f\n", PitchAcc);
    printf("YawAcc    = %f\n\n", YawAcc);
  }
  
}  __attribute__((packed)) ;;

#endif
