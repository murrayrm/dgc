# DGC Globaly Included Makefile
# Include this at the top of every makefile after setting DGC
.PHONY: redirect folders all
redirect: folders all

# Tool definitions
CC = gcc
CPP= g++
FC= g77
INCLUDE=-I$(INCLUDEDIR)
CFLAGS += -g -Wall $(INCLUDE)
CPPFLAGS += -pthread $(CFLAGS)

LDFLAGS += -g -pthread -Wall -L$(LIBDIR) # REMOVE -ltspread -lserial 

ifeq (1,$(OPTIMIZE))
CFLAGS += -D NDEBUG -O3
endif

ifeq (1,${USE_RTAI})
CFLAGS += -D USE_RTAI
LDFLAGS += -lrtai
endif

INSTALL = install
INSTALL_DATA = ln -f
INSTALL_PROGRAM = ln -f 
#INSTALL_DATA = $(INSTALL) -m 644 
#INSTALL_PROGRAM = $(INSTALL) -m 754
MAKEDEPEND = makedepend -Y
CDD = $(BINDIR)/cdd

ifneq (,$(findstring AMD64,$(CPPFLAGS)))
MATH_LIBS_NOSOLVER  = -lacml -lg2c
else
MATH_LIBS_NOSOLVER  = -lblas -latlas -llapack
endif

MATH_LIBS = -lnlpsolver $(MATH_LIBS_NOSOLVER) 

# Rule to make the target folders and copy constants before any rules execute
ifneq (1,${MAKINGFIRST})
MAKEFIRST := $(shell export MAKINGFIRST=1 && $(MAKE) first)
endif

# Directory constants
BINDIR = $(DGC)/bin
LIBDIR = $(DGC)/lib
INCLUDEDIR = $(DGC)/include

DGC_BIN_INFO = $(BINDIR)/info.txt

# headers library locations
#  fake libraries, don't link with these :)
RPGSEEDERHDR        = $(LIBDIR)/rpgSeederHeaderLib.a

# header include makefiles - must have HEADERNAME_PATH and HEADERNAME_DEPEND 
# defined within
include $(DGC)/modules/rddfPathGen/rpgSeederHeader.mk

# drivers library locations
ALICELIB            = $(LIBDIR)/alice.a
IMU_FASTCOMLIB      = $(LIBDIR)/imu_fastcom.a
SDSLIB              = $(LIBDIR)/SDS.a
LADARDRIVERSLIB     = $(LIBDIR)/ladardrivers.a
STEREOSOURCELIB     = $(LIBDIR)/stereoSource.a
LADARSOURCELIB      = $(LIBDIR)/ladarSource.a
GPSLIB              = $(LIBDIR)/gps.a
NOVATELLIB          = $(LIBDIR)/novatel.a

# driver include makefiles - must have DRIVERNAME_PATH and DRIVERNAME_DEPEND 
# defined within
include $(DGC)/drivers/alice/alice.mk
include $(DGC)/drivers/gpsSDS/gps.mk
include $(DGC)/drivers/gps/gps.mk
include $(DGC)/drivers/novatel/novatel.mk
include $(DGC)/drivers/imu_fastcom/imu_fastcom.mk
include $(DGC)/drivers/SDS/SDS.mk
include $(DGC)/drivers/ladardrivers/ladardrivers.mk
include $(DGC)/drivers/stereovision/stereoSource.mk
include $(DGC)/drivers/ladar/ladarSource.mk

# Util library locations
ADRIVELIB           = $(LIBDIR)/adrive.a
CMAPLIB             = $(LIBDIR)/CMap.a
CMAPPLIB            = $(LIBDIR)/CMapPlus.a
CCPAINTLIB          = $(LIBDIR)/CCorridorPainter.a
CTERRAINCOSTPAINTERLIB       = $(LIBDIR)/CTerrainCostPainter.a
CCOSTFUSERLIB       = $(LIBDIR)/CCostFuser.a
CPIDLIB             = $(LIBDIR)/libpid.a
DGCUTILS            = $(LIBDIR)/DGCutils.o
FINDFORCELIB        = $(LIBDIR)/findforce.a	
FRAMESLIB           = $(LIBDIR)/frames.a
GGISLIB             = $(LIBDIR)/ggis.a
LADARCLIENTLIB      = $(LIBDIR)/LadarClient.o
MAPDISPLAYLIB       = $(LIBDIR)/MapDisplay.a
MODULEHELPERSLIB    = $(LIBDIR)/libmodulehelpers.a 
PIDCONTROLLERLIB    = $(LIBDIR)/PID_Controller.a
PLANNERLIB          = $(LIBDIR)/planner.a
RDDFLIB             = $(LIBDIR)/rddf.a
RDDFPATHGENLIB      = $(LIBDIR)/rddfPathGen.a
SKYNETLIB           = $(LIBDIR)/libskynet.a
SPARROWLIB          = $(LIBDIR)/sparrow.a
FALCONLIB	    			= $(LIBDIR)/falcon.a
MATIOLIB	          = $(LIBDIR)/libmatio.a
STEREOPROCESSLIB    = $(LIBDIR)/stereoProcess.a
TRAJLIB             = $(LIBDIR)/traj.a
GEOMETRYLIB         = $(LIBDIR)/geometry.a
CTRAJPAINTERLIB     = $(LIBDIR)/CTrajPainter.a
CELEVFUSERLIB       = $(LIBDIR)/CElevationFuser.a
PROFILERLIB         = $(LIBDIR)/profiler.a
ROADFINDINGLIB      = $(LIBDIR)/road.a
ROADPAINTERLIB      = $(LIBDIR)/RoadPainter.a
SPARROWHAWKLIB      = $(LIBDIR)/SparrowHawk.a
NLPSOLVERLIB        = $(LIBDIR)/libnlpsolver.a
VIDEORECORDERLIB    = $(LIBDIR)/videoRecorder.a
SHAREMEMLIB  	    = $(LIBDIR)/sharemem.a
MATRIXLIB           = $(LIBDIR)/matrix.a
RDDFPREPLIB         = $(LIBDIR)/rddfPrep.a
SERIALLIB	    = $(LIBDIR)/libserial.a

# util include makefiles - must have UTILNAME_PATH and UTILNAME_DEPEND defined 
# within
include $(DGC)/util/cMap/CMap.mk
include $(DGC)/util/corridorPainter/CCorridorPainter.mk
include $(DGC)/util/costFuser/CCostFuser.mk
include $(DGC)/util/terrainCostPainter/CTerrainCostPainter.mk
include $(DGC)/util/pid/Cpid.mk
include $(DGC)/util/moduleHelpers/DGCutils.mk
include $(DGC)/util/controllerUtils/force/find_force.mk
include $(DGC)/util/frames/frames.mk
include $(DGC)/util/latlong/latlong.mk
include $(DGC)/util/mapDisplay/MapDisplay.mk
include $(DGC)/util/moduleHelpers/ModuleHelpers.mk
include $(DGC)/util/pidController/PID_Controller.mk
include $(DGC)/util/planner/planner.mk
include $(DGC)/util/RDDF/Rddf.mk
include $(DGC)/util/skynet/skynet.mk
include $(DGC)/util/sparrow/sparrow.mk
include $(DGC)/util/falcon/src/falcon.mk
include $(DGC)/util/matio/matio.mk
include $(DGC)/util/stereoProcess/stereoProcess.mk
include $(DGC)/util/traj/traj.mk
include $(DGC)/util/geometry/geometry.mk
include $(DGC)/util/CTrajPainter/CTrajPainter.mk
include $(DGC)/util/RoadPainter/RoadPainter.mk
include $(DGC)/util/profiler/profiler.mk
include $(DGC)/util/SparrowHawk/SparrowHawk.mk
include $(DGC)/util/videoRecorder/videoRecorder.mk
include $(DGC)/util/sharemem/sharemem.mk
include $(DGC)/util/matrix/matrix.mk
include $(DGC)/util/rddfPrep/rddfPrep.mk

# Module binary/library locations
ADRIVEBIN           = $(BINDIR)/adrive
ASTATEBIN           = $(BINDIR)/astate
FUSIONMAPPERBIN     = $(BINDIR)/fusionMapper
GUIBIN              = $(BINDIR)/gui
LADARFEEDERBIN      = $(BINDIR)/ladarFeeder
PLANNERMODULEBIN    = $(BINDIR)/plannerModule
RDDFPATHGENBIN      = $(BINDIR)/rddfPathGen
MINIPMBIN           = $(BINDIR)/DGCKeepAlive
SIMULATORBIN        = $(BINDIR)/simulator
STEREOFEEDERBIN     = $(BINDIR)/stereoFeeder
SUPERCONBIN         = $(BINDIR)/superCon
TRAJFOLLOWERBIN     = $(BINDIR)/trajFollower
FOLLOWBIN	    = $(BINDIR)/follow
ROADFINDINGBIN      = $(BINDIR)/road
LOGPLAYERBIN        = $(BINDIR)/logplayer
LOADSPEWBIN         = $(BINDIR)/loadspewbin
VIDEORECORDER       = $(BINDIR)/videoRecorder
# TRAJSELECTORBIN   = $(BINDIR)/trajSelector
DBSBIN              = $(BINDIR)/DBS
RDDFPREPBIN         = $(BINDIR)/rddfPrep
DISTRDDFBIN         = $(BINDIR)/distributeRddf.sh

# module include makefiles - must have MODULENAME_PATH and MODULENAME_DEPEND 
# defined within
include $(DGC)/modules/adrive/adrive.mk
include $(DGC)/modules/astate/astate.mk
include $(DGC)/modules/fusionMapper/fusionMapper.mk
include $(DGC)/modules/miniPM/minipm.mk
include $(DGC)/modules/gui/gui.mk
include $(DGC)/modules/ladarFeeder/ladarFeeder.mk
include $(DGC)/modules/plannerModule/plannerModule.mk #Must come after planner
include $(DGC)/modules/rddfPathGen/rddfPathGen.mk
include $(DGC)/modules/simulator/simulator.mk # This needs to come after adrive.
include $(DGC)/modules/stereoFeeder/stereoFeeder.mk
include $(DGC)/modules/trajFollower/trajFollower.mk
include $(DGC)/util/gazebo/clients/LadarClient/ladarClient.mk
include $(DGC)/util/gazebo/clients/FakeState/FakeState.mk
include $(DGC)/modules/roadFinding/roadFinding.mk
include $(DGC)/util/logplayer/logplayer.mk
include $(DGC)/util/elevationFuser/elevationFuser.mk
include $(DGC)/modules/superCon/superCon.mk
# include $(DGC)/projects/trajSelector/trajSelector.mk
include $(DGC)/projects/cds110b/follow.mk

# other library locations
TIMBERLIB           = $(LIBDIR)/timber.a
SUPERCONCLIENTLIB   = $(LIBDIR)/superConClient.a
# REACTIVEPLANNERLIB  = $(LIBDIR)/reactivePlanner.a

# other include makefiles
include $(DGC)/modules/timber/timber.mk
# include $(DGC)/projects/reactivePlanner/reactivePlanner.mk

# DGC Global Makefile Rules
first: folders

folders: $(INCLUDEDIR) $(LIBDIR) $(BINDIR)

# Driver libraries
$(ALICELIB): $(ALICE_DEPEND)
	$(MAKE) install -C $(ALICE_PATH)

$(IMU_FASTCOMLIB): $(IMU_FASTCOM_DEPEND)
	$(MAKE) install -C $(IMU_FASTCOM_PATH)

$(SDSLIB): $(SDS_DEPEND)
	$(MAKE) install -C $(SDS_PATH)

$(LADARDRIVERSLIB): $(LADARDRIVERS_DEPEND)
	$(MAKE) install -C $(LADARDRIVERS_PATH)

$(STEREOSOURCELIB): $(STEREOSOURCE_DEPEND)
	$(MAKE) install -C $(STEREOSOURCE_PATH)

$(LADARSOURCELIB): $(LADARSOURCE_DEPEND)
	$(MAKE) install -C $(LADARSOURCE_PATH)

$(GPSLIB): $(GPS_DEPEND)
	$(MAKE) install -C  $(GPS_PATH)

$(NOVATELLIB): $(NOVATEL_DEPEND)
	$(MAKE) install -C  $(NOVATEL_PATH)


# Utility libraries
$(ADRIVELIB): $(ADRIVE_DEPEND) 
	$(MAKE) install -C  $(ADRIVE_PATH)

$(CMAPLIB): $(CMAP_DEPEND)
	$(MAKE) install -C $(CMAP_PATH)

$(CMAPPLIB): $(CMAP_DEPEND)
	$(MAKE) install -C $(CMAP_PATH)

$(CCPAINTLIB): $(CCORRIDORPAINTER_DEPEND)
	$(MAKE) install -C $(CCORRIDORPAINTER_PATH)

$(CCOSTFUSERLIB): $(CCOSTFUSER_DEPEND)
	$(MAKE) install -C $(CCOSTFUSER_PATH)

$(CTERRAINCOSTPAINTERLIB): $(CTERRAINCOSTPAINTER_DEPEND)
	$(MAKE) install -C $(CTERRAINCOSTPAINTER_PATH)

$(CELEVFUSERLIB): $(CELEVATIONFUSER_DEPEND)
	$(MAKE) install -C $(CELEVATIONFUSER_PATH)

$(CPIDLIB): $(CPID_DEPEND)
	$(MAKE) install -C $(CPID_PATH)

$(CTRAJPAINTERLIB): $(CTRAJPAINTER_DEPEND)
	$(MAKE) install -C $(CTRAJPAINTER_PATH)

$(DGCUTILS): $(DGCUTILS_DEPEND)
	$(MAKE) install -C $(DGCUTILS_PATH)

$(FINDFORCELIB): $(FINDFORCE_DEPEND)
	$(MAKE) install -C $(FINDFORCE_PATH)

$(FRAMESLIB): $(FRAMES_DEPEND)
	$(MAKE) install -C  $(FRAMES_PATH)

$(GEOMETRYLIB): $(GEOMETRY_DEPEND)
	$(MAKE) install -C  $(GEOMETRY_PATH)

$(GGISLIB): $(LATLONG_DEPEND)
	$(MAKE) install -C $(LATLONG_PATH)

$(LADARCLIENTLIB): $(LADARCLIENT_DEPEND)
	$(MAKE) install -C $(LADARCLIENT_PATH)

$(MAPDISPLAYLIB): $(MAPDISPLAY_DEPEND)
	$(MAKE) install -C $(MAPDISPLAY_PATH)

$(MODULEHELPERSLIB): $(MODULEHELPERS_DEPEND)
	$(MAKE) install -C $(MODULEHELPERS_PATH)

$(PIDCONTROLLERLIB): $(PIDCONTROLLER_DEPEND)
	$(MAKE) install -C $(PIDCONTROLLER_PATH)

$(PLANNERLIB): $(PLANNER_DEPEND)
	$(MAKE) install -C  $(PLANNER_PATH)

$(PROFILERLIB): $(PROFILER_DEPEND)
	$(MAKE) install -C  $(PROFILER_PATH)

$(RDDFLIB): $(RDDF_DEPEND)
	$(MAKE) install -C  $(RDDF_PATH)

$(RDDFPATHGENLIB): $(RDDFPATHGEN_DEPEND)
	cd $(RDDFPATHGEN_PATH) && $(MAKE)

$(ROADFINDINGLIB): $(UDROAD_DEPEND)
	$(MAKE) install -C $(UDROAD_PATH)

$(ROADPAINTERLIB): $(ROADPAINTER_DEPEND)
	$(MAKE) install -C $(ROADPAINTER_PATH)

$(SKYNETLIB): $(SKYNET_DEPEND)
	$(MAKE) install -C $(SKYNET_PATH)

$(SPARROWLIB): $(SPARROW_DEPEND)
	$(MAKE) install -C $(SPARROW_PATH)

$(FALCONLIB): $(FALCON_DEPEND)
	$(MAKE) install -C $(FALCON_PATH)

$(MATIOLIB): $(MATIO_DEPEND)
	$(MAKE) install -C $(MATIO_PATH)

$(SPARROWHAWKLIB): $(SPARROWHAWK_DEPEND)
	$(MAKE) install -C$(SPARROWHAWK_PATH)

$(STEREOPROCESSLIB): $(STEREOPROCESS_DEPEND)
	$(MAKE) install -C $(STEREOPROCESS_PATH)

$(SUPERCONCLIENTLIB): $(SUPERCONCLIENT_DEPEND)
	$(MAKE) install_lib -C $(SUPERCON_PATH)

$(TIMBERLIB): $(TIMBER_DEPEND)
	cd $(TIMBER_PATH) && $(MAKE) 

$(TRAJLIB): $(TRAJ_DEPEND)
	$(MAKE) install -C $(TRAJ_PATH)

$(MATRIXLIB): $(MATRIX_DEPEND)
	$(MAKE) install -C $(MATRIX_PATH)

$(RDDFPREPLIB): $(RDDFPREP_DEPEND)
	$(MAKE) install -C $(RDDFDEPEND_PATH)

# External libraries
$(NLPSOLVERLIB):;	$(MAKE) nlpsolver -C $(DGC)/external
$(SERIALLIB):;		$(MAKE) libserial -C $(DGC)/external

# Module binaries
$(ADRIVEBIN): $(ADRIVE_DEPEND) 
	$(MAKE) install -C  $(ADRIVE_PATH)

$(ASTATEBIN): $(ASTATE_DEPEND)
	$(MAKE) install -C  $(ASTATE_PATH)

$(FUSIONMAPPERBIN): $(FUSIONMAPPER_DEPEND)
	$(MAKE) install -C $(FUSIONMAPPER_PATH)

$(GUIBIN): $(GUI_DEPEND)
	$(MAKE) install -C $(GUI_PATH)

$(LADARFEEDERBIN): $(LADARFEEDER_DEPEND)
	$(MAKE) install -C $(LADARFEEDER_PATH)

$(MINIPMBIN): $(MINIPM_DEPEND)
	$(MAKE) install -C $(MINIPM_PATH)


$(PLANNERMODULEBIN): $(PLANNERMODULE_DEPEND)
	$(MAKE) install -C $(PLANNERMODULE_PATH)

$(RDDFPATHGENBIN): $(RDDFPATHGEN_DEPEND)
	cd $(RDDFPATHGEN_PATH) && $(MAKE)

$(SIMULATORBIN): $(SIMULATOR_DEPEND)
	$(MAKE) install -C $(SIMULATOR_PATH)

$(STEREOFEEDERBIN): $(STEREOFEEDER_DEPEND)
	$(MAKE) install -C $(STEREOFEEDER_PATH)

$(TRAJFOLLOWERBIN): $(TRAJFOLLOWER_DEPEND)
	$(MAKE) install -C $(TRAJFOLLOWER_PATH)

$(FOLLOWBIN): $(FOLLOW_DEPEND)
	$(MAKE) install -C $(FOLLOW_PATH)

$(ROADFINDINGBIN): $(UDROAD_DEPEND)
	$(MAKE) install -C $(UDROAD_PATH)

$(LOGPLAYERBIN): $(LOGPLAYER_DEPEND)
	$(MAKE) install -C $(LOGPLAYER_PATH)

$(LOADSPEWBIN): $(LOGPLAYER_DEPEND)
	$(MAKE) install -C $(LOGPLAYER_PATH)

$(TIMBERBIN): $(TIMBER_DEPEND)
	cd $(TIMBER_PATH) && $(MAKE) 

$(VIDEORECORDERBIN): $(VIDEORECORDER_DEPEND)
	$(MAKE) install -C $(VIDEORECORDER_PATH)

$(SUPERCONBIN): $(SUPERCON_DEPEND)
	$(MAKE) install -C $(SUPERCON_PATH)

$(TRAJSELECTORBIN): $(TRAJSELECTOR_DEPEND)
	$(MAKE) install -C $(TRAJSELECTOR_PATH)

$(DBSBIN): 
	$(MAKE) install -C util/DBS/

$(RDDFPREPBIN): 
	$(MAKE) install -C $(RDDFPREP_PATH)

$(DISTRDDFBIN): 
	$(MAKE) install -C $(RDDFPREP_PATH)

# Other library installations
$(VIDEORECORDERLIB): $(VIDEORECORDER_DEPEND)
	$(MAKE) install_lib -C $(VIDEORECORDER_PATH)

$(REACTIVEPLANNERLIB): $(REACTIVEPLANNER_DEPEND)
	$(MAKE) install -C $(REACTIVEPLANNER_PATH)

$(RPGSEEDERHDR): $(RPGSEEDERHDR_DEPEND)
	cd $(RPGSEEDERHDR_PATH) && $(MAKE) headers


# installs by default
$(CDD): $(SPARROWLIB)

$(INCLUDEDIR):
	cd $(DGC) && mkdir include

$(LIBDIR):
	cd $(DGC) && mkdir lib

$(BINDIR):
	cd $(DGC) && mkdir bin


