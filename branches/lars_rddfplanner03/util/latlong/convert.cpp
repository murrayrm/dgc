#include <iostream>
#include "LatLong.h"

#include <fstream>
#include <iomanip>


using namespace std;

int main(void)
{
   double lat, lon, easting, northing;
   LatLong data(0,0);

   ifstream inf("way.pts");

   cout << setprecision(10);

   while(! inf.eof() )
   {
        inf>>lat;
	inf>>lon;
	data.set_latlon(lat, lon);
	data.get_UTM(&easting, &northing);
	
	cout << easting << "\t" << northing << endl;

   }
   return 0;
}	

