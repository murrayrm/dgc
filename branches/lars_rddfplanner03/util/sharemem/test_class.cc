#include "test_class.hh"
#include <stdio.h>

CShmTest::CShmTest() {
	i = 0;
}

CShmTest::~CShmTest() {
	i = 0;
}

void CShmTest::Inc() {
	i++;
}

void CShmTest::Print() {
	printf("got %d\n", i);
}

