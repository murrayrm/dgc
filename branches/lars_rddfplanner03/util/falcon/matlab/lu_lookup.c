/*
 * lu_lookup - open a lookup table
 *
 * RMM, 14 May 96
 *
 */

#include <stdio.h>
#include "mex.h"
#include "falcon/lutable.h"

void mexFunction(nlhs, plhs, nrhs, prhs)
int nlhs, nrhs;
Matrix *plhs[], *prhs[];
{
    char address[16];
    LU_DATA *lut = NULL;
    int nout;

    /* Check to make sure we have the proper number of arguments */
    if (nrhs != 2) {
	mexErrMsgTxt("lu_lookup: missing or invalid argument");
	return;
    }

    /* Open up the lookup table */
    mxGetString(prhs[0], address, 1024);
    /* Get the address of the table */
    if (sscanf(address, "0x%x", &lut) != 1) {
	mexErrMsgTxt("lu_lookup: invalid address");
	return;
    }

    /* Figure out the result (no error checking for now) */
    nout = lu_outputs(lut);
    if (nlhs == 1) {
	double *data, *coord;

	/* Allocate space and set up some pointers */
	plhs[0] = mxCreateFull(1, nout, REAL);
	coord = mxGetPr(prhs[1]);
	data = mxGetPr(plhs[0]);

	/* Look up the data */
	lu_lookup(lut, coord, data);

#	ifdef DEBUG
	printf("coord = %g %g\n", coord[0], coord[1]);
	printf("data =  %g %g\n", data[0], data[1]);
#	endif
    }
}
