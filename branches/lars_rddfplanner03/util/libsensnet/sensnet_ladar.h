/* 
 * Desc: SensNet ladar blob
 * Date: 3 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef SENSNET_LADAR_H
#define SENSNET_LADAR_H

#include <stdint.h>
#include <math.h>
#include "sensnet_types.h"

/** @file

@brief Ladar blob and some useful accessors.

*/

/// @brief Maximum image dimensions
#define SENSNET_LADAR_BLOB_MAX_POINTS 181
  

/// @brief Ladar scan data.
///
typedef struct
{  
  /// Blob type (must be SENSNET_LADAR_BLOB)
  int blob_type;

  /// Sensor ID for originating sensor
  int sensor_id;

  /// Scan id
  int scanid;

  /// Image timestamp
  double timestamp;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  sensnet_state_t state;

  /// Sensor-to-vehicle transformation (homogeneous matrix)
  float sens2veh[4][4];

  /// Scan center (scan index that corresponds to +x)
  int center;

  /// Scan scale: bearing = (index - center) * scale.
  float scale;

  /// Number of points
  int num_points;
  
  /// Range data
  float points[SENSNET_LADAR_BLOB_MAX_POINTS][2];
  
} sensnet_ladar_blob_t __attribute__((packed));


/// @brief Fast conversion from index to (x,y,z) in sensor frame
static  __inline__ void sensnet_ladar_blob_ir_to_xyz(sensnet_ladar_blob_t *self,
                                                     float pa, float pr,
                                                     float *x, float *y, float *z)
{
  *x = pr * cos(pa);
  *y = pr * sin(pa);
  *z = 0;  
  return;
}



#endif
