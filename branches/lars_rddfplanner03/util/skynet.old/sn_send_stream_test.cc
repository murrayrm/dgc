#include "sn_msg.hh"
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
extern int h_errno;

int main(int argc, char** argv){
  int sn_key;
  char* hostname;
  char* default_key = getenv("SKYNET_KEY");
  if (3 == argc)
  {
    sn_key  = atoi(argv[1]);
  }
  else if (default_key != NULL )
  {
    sn_key = atoi(default_key);
  }
  else
  {
    printf("usage: sn_send_test [key] target_ip\nDefault key is $SKYNET_KEY\n");
    exit(2);
  }
  if (argc >= 2)
  {
    hostname = argv[argc - 1];
  }
  else
  {
    printf("usage: sn_send_test [key] target_ip\nDefault key is $SKYNET_KEY\n");
    exit(2);
  }
  struct hostent *remote ;
  if ((remote = gethostbyname(hostname)) == NULL) {
    herror("gethostbyname") ;
    exit(1) ;
  }
  struct in_addr remote_addr = *((struct in_addr * )remote->h_addr);
 
  modulename myself = SNstereo;
  sn_msg myoutput = SNpointcloud;
 
  skynet Skynet(myself, sn_key);
  printf("My module id is: %u\n", Skynet.get_id());
  int socket = Skynet.get_stream_sock(myoutput, &remote_addr);
  char* mymsg = "Hello, World!";
  Skynet.stream_send_msg(socket, (void*)mymsg, strlen(mymsg)+1, 0);
  return 0;
  }
