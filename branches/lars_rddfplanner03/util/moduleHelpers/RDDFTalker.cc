#include "RDDFTalker.hh"
#include "DGCutils"

#include <iostream>
#include <strstream>
#include <iomanip>

using namespace std;

/******************************************************************************/
CRDDFTalker::CRDDFTalker()
{
  m_pRDDFDataBuffer = new char[RDDF_DATA_PACKAGE_SIZE];
  
  DGCcreateMutex(&m_RDDFDataBufferMutex);
}

/******************************************************************************/
CRDDFTalker::~CRDDFTalker()
{
  delete [] m_pRDDFDataBuffer;
  
  DGCdeleteMutex(&m_RDDFDataBufferMutex);
}

/******************************************************************************/
bool CRDDFTalker::SendRDDF(int rddfSocket, RDDF* pRddf, pthread_mutex_t* pMutex)
{
  int bytesToSend;
  int bytesSent;
  char* pBuffer = m_pRDDFDataBuffer;

  // Initialize
  char tmp[100] = "\0";
  pBuffer[0] = '\0';
  RDDFVector wp = pRddf->getTargetPoints();

  for (int i=0; i<pRddf->getNumTargetPoints(); i++)
  {
    sprintf(tmp, "%d,%.8f,%.8f,%.3f,%.3f\n", 
      wp[i].number, wp[i].latitude, wp[i].longitude, 
      wp[i].radius/METERS_PER_FOOT, wp[i].maxSpeed/MPS_PER_MPH);
  
    strncat(pBuffer, tmp, strlen(tmp));

    //cout << tmp;
  }

  //printf("\n[%s:%d] \n", __FILE__, __LINE__);
  //cout << pBuffer;
    
  DGClockMutex(&m_RDDFDataBufferMutex);
  if (pMutex != NULL)
  {
    DGClockMutex(pMutex);
  }

  bytesToSend = RDDF_DATA_PACKAGE_SIZE;

  bytesSent = m_skynet.send_msg(rddfSocket, m_pRDDFDataBuffer, bytesToSend, 0);
  
  DGCunlockMutex(&m_RDDFDataBufferMutex);
  if (pMutex != NULL)
  {
    DGCunlockMutex(pMutex);
  }
  
  if (bytesSent != bytesToSend)
  {
    cerr << "CRDDFTalker::SendRDDF(): sent " << bytesSent << 
      " bytes while expected to send " << bytesToSend << " bytes" << endl;

    return false;
  }
  
  return true;
}


/******************************************************************************/
bool CRDDFTalker::RecvRDDF(int rddfSocket, RDDF* pRddf, pthread_mutex_t* pMutex, 
                           string* pOutstring)
{
  int bytesToReceive;
  int bytesReceived;
  char* pBuffer = m_pRDDFDataBuffer;

  // Build the mutex list. We want to protect the data buffer
  int numMutices = 1;
  pthread_mutex_t* ppMutices[2];
  ppMutices[0] = &m_RDDFDataBufferMutex;
  if (pMutex != NULL)
  {
    ppMutices[1] = pMutex;
    numMutices++;
  }

  // Get the RDDF data from skynet. We want to receive the whole segGoals, locking
  // the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  bytesToReceive = RDDF_DATA_PACKAGE_SIZE;
  
  bytesReceived = m_skynet.get_msg(rddfSocket, pBuffer, bytesToReceive, 
                                   0, ppMutices, false, numMutices);

  if (bytesReceived != bytesToReceive)
  {
    cerr << "CRDDFTalker::RecvRDDF(): received" << bytesReceived << 
      "bytes while expected to receive " << bytesToReceive << "bytes" << endl;

    DGCunlockMutex(&m_RDDFDataBufferMutex);
    if (pMutex != NULL)
    {
      DGCunlockMutex(pMutex);
    } 
    return false;
  }
  
  if (pOutstring != NULL)
  {
    pOutstring->append((char*)&bytesReceived, sizeof(bytesReceived));
    pOutstring->append(m_pRDDFDataBuffer, bytesReceived);
  }

  //printf("Raw characters received: ");
  //for (int i=0; i<RDDF_DATA_PACKAGE_SIZE; i++) 
  //{ 
  //  printf("%c",pBuffer[i]); 
  //}
  //printf("\n");

  /* Load the character array into an istream and call loadStream */
  istrstream recvstream(pBuffer, RDDF_DATA_PACKAGE_SIZE);
  
  //printf("[%s:%d] \n", __FILE__, __LINE__);
  //cout << recvstream.str() << endl;

  pRddf->loadStream(recvstream);

  DGCunlockMutex(&m_RDDFDataBufferMutex);
  if (pMutex != NULL)
  {
    DGCunlockMutex(pMutex);
  }

  return true;
}

