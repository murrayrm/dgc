#include "RDDFTalker.hh"

class CRDDFTalkerTestRecv : public CRDDFTalker
{
public:
	CRDDFTalkerTestRecv(int sn_key)
		: CSkynetContainer(MODrddftalkertestrecv, sn_key)
	{
		RDDF rddf;

		cout << "about to listen...";
		int rddfSocket = m_skynet.listen(SNrddf, MODrddftalkertestsend);
		cout << " listening!" << endl;
		while(true)
		{
			cout << "about to receive an rddf...\n";
			RecvRDDF(rddfSocket, &rddf);
      printf("[%s:%d] \n", __FILE__, __LINE__);
      rddf.print();
			cout << " received an rddf!" << endl;
		}
	}
};

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;

	CRDDFTalkerTestRecv test(key);
}
