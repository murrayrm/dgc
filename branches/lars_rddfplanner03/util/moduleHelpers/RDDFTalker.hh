#ifndef _RDDFTALKER_HH_
#define _RDDFTALKER_HH_

#include "SkynetContainer.h"
#include "rddf.hh"
#include "pthread.h"
#include <unistd.h>

#include <string>
using namespace std;

/** The maximum size of the data that can be sent across skynet for 
 * sending an RDDF.  Units of bytes. */
#define RDDF_DATA_PACKAGE_SIZE 10000

class CRDDFTalker : virtual public CSkynetContainer
{
  pthread_mutex_t m_RDDFDataBufferMutex;
  
  /** character buffer that matches the RDDF format */
  char* m_pRDDFDataBuffer;

public:
  CRDDFTalker();
  ~CRDDFTalker();
  
  bool SendRDDF(int rddfSocket, RDDF* pRddf, pthread_mutex_t* pMutex = NULL);
  
  bool RecvRDDF(int rddfSocket, RDDF* pRddf, pthread_mutex_t* pMutex = NULL, 
                string* pOutstring = NULL);
  
};

#endif // _RDDFTALKER_HH_
