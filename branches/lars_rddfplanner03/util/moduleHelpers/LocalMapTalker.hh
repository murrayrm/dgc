#ifndef LOCALMAPTALKER_HH
#define LOCALMAPTALKER_HH

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "SkynetContainer.h"
#include "DGCutils"
#include "../../projects/sidd/Mapper/Map.h"

#define LOCALMAP_MAX_BUFFER_SIZE 50000

class CLocalMapTalker : virtual public CSkynetContainer {
  pthread_mutex_t m_localMapDataBufferMutex;
  char* m_pLocalMapDataBuffer;

public:
  CLocalMapTalker();
  ~CLocalMapTalker();

  bool SendLocalMap(int localMapSocket, Mapper::Map* localMap);
  bool RecvLocalMap(int localMapSocket, Mapper::Map* localMap, int* pSize);
};

#endif //  LOCALMAPTALKER_HH
