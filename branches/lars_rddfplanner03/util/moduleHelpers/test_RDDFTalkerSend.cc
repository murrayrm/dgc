#include "RDDFTalker.hh"

class CRDDFTalkerTestSend : public CRDDFTalker
{
public:
	CRDDFTalkerTestSend(int sn_key)
		: CSkynetContainer(MODrddftalkertestsend, sn_key)
	{
		int i;
		RDDF rddf("test.rddf");

		RDDFVector waypoints = rddf.getTargetPoints();

    for(i=0; i<rddf.getNumTargetPoints(); i++)
		{
      waypoints[i].display();
		}

		cout << "about to get_send_sock...";
		int rddfSocket = m_skynet.get_send_sock(SNrddf);
		cout << " get_send_sock returned" << endl;
		
    for(i=0; i<5; i++)
		{
			cout << "about to send an rddf ("<<i+1<<" of 5)...";
			SendRDDF(rddfSocket, &rddf);
			cout << " sent an rddf!" << endl;
      flush(cout);

      // Test sending different sizes of RDDFs
      RDDFData wp;
      wp.number = i+2;
      wp.latitude = 34+i*0.0001;
      wp.longitude = -117-i*0.0001;
      wp.radius = i*10;
      wp.offset = i*15;
      wp.maxSpeed = i*i;

      rddf.addDataPoint(wp);

 			usleep(1000000);
		}
  }
};

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;

	CRDDFTalkerTestSend test(key);
}
