// find_force.hh
// Force lookup for longitudinal control
// H Huang - 2/18/2005

#ifndef FIND_FORCE
#define FIND_FORCE

#include "AliceConstants.h"
#include <math.h>
#include <iostream>

using namespace std;

// gear labels
enum GEAR_INDEX{
  GEARR,
  GEAR1,
  GEAR2,
  GEAR3,
  GEAR4,
  GEAR5,
};

// get a particular pedal setting (-1 to 1) for a desired force
// NOTE: This should be the only function in this file that you call
double get_pedal(double v, double f, int gear);

// get a throttle setting for a particular force and velocity
double get_throttle(double v, double f, int gear);

// get a force on tires for a particular throttle setting
double get_force(double v, double throttle, int gear);

// get a brake setting for a particular force
double get_brake(double f);

#endif
