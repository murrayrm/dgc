#include <math.h>
#include <stdio.h>
/*#include "gnuplot_i.c"*/
/*#include "nrutil.c"*/
#include "gnuplot_i.h"

int main(int argc, char *argv[]) {
  gnuplot_ctrl    *h ; /*Gnuplot-Control*/
  double **       z;
  int             i,j ;

  z=dmatrix(1,20,1,20); /* Dynamic memory allocation*/

  h = gnuplot_init() ; /*Init the Gnuplot-Window*/
  gnuplot_set_xlabel(h,"x"); /*Set label of x-Axes*/
  gnuplot_set_ylabel(h,"y"); /*Set label of y-Axes*/
  gnuplot_set_zlabel(h,"z"); /*Set label of y-Axes*/
  gnuplot_set_title(h,"gnuplot_surf - Demonstration"); /*Set Title*/
  gnuplot_set_grid(h); /* Turns grid on*/
  gnuplot_setstyle(h,"lines"); 
  /* Set style
     possible values:  
       - lines
       - points
       - linespoints
       - impulses
       - dots
       - steps
       - errorbars
       - boxes
       - boxeserrorbars 
  */

  /*Range:
  gnuplot_set_xrange(h,1.0,10.0); 
  gnuplot_set_yrange(h,1.0,10.0);
  gnuplot_set_zrange(h,0.0,1.0);*/

  for (i=1 ; i<=20 ; i++) {
    for (j=1; j<=20 ; j++) {
      z[i][j]= 0.5*sin((double)i)* 0.5*cos((double)j)-10;
    }
  }


  gnuplot_surfrange(h,z,1,20,1,20,"Surface - Plot");

  printf("press ENTER to continue\n");
  while (getchar()!='\n') {} /* Wait for keypress */

  gnuplot_close(h) ; /*Close the gnuplot-window*/
  free_dmatrix(z,1,20,1,20);
  return 1;
}
