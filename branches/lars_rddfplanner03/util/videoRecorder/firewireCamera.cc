#include"firewireCamera.hh"
#include"conversions.h"

#include<stdio.h>

CFirewireCamera::CFirewireCamera() : m_handle(0)
{
}

CFirewireCamera::~CFirewireCamera()
{
  close();
}

/*
**
** camera initialization and closing
**
*/
bool CFirewireCamera::open(int num)
{
  close();
  
  m_handle = dc1394_create_handle(0);
  if(!m_handle) {
    fprintf(stderr, "Unable to create dc1394 handle\n");
    return false;
  }

  int num_cams = 0;
  nodeid_t * cam_nodes = dc1394_get_camera_nodes(m_handle, &num_cams, 0);
  if(num<0 || num>=num_cams) {
    fprintf(stderr, "Could not open camera number %i\n", num);
    dc1394_free_camera_nodes(cam_nodes);
    dc1394_destroy_handle(m_handle);
    m_handle=0;
    return false;
  }

  if(!setupCamera(cam_nodes[num])) {
    fprintf(stderr, "Could not setup camera\n", num);
    dc1394_free_camera_nodes(cam_nodes);
    close();
    return false;
  }

  dc1394_free_camera_nodes(cam_nodes);

  return true;
}

bool CFirewireCamera::open_uid(unsigned long long uid)
{
  close();
  
  m_handle = dc1394_create_handle(0);
  if(!m_handle) {
    fprintf(stderr, "Unable to create dc1394 handle\n");
    return false;
  }

  int num_cams = 0;
  nodeid_t * cam_nodes = dc1394_get_camera_nodes(m_handle, &num_cams, 0);
  
  int i;
  for(i=0; i<num_cams; ++i) {
    dc1394_camerainfo info;
    if(dc1394_get_camera_info(m_handle, cam_nodes[i], &info)!=DC1394_SUCCESS)
      continue;

    if(info.euid_64==uid)
      break;
  }

  if(i==num_cams) {
    fprintf(stderr, "Could not find camera with id %Lx\n", uid);
    dc1394_free_camera_nodes(cam_nodes);
    dc1394_destroy_handle(m_handle);
    m_handle=0;
    return false;
  }

  if(!setupCamera(cam_nodes[i])) {
    fprintf(stderr, "Could not setup camera\n");
    dc1394_free_camera_nodes(cam_nodes);
    close();
    return false;
  }

  dc1394_free_camera_nodes(cam_nodes);

  return true;
}

bool CFirewireCamera::open(const char *uid)
{
  //Parse uid to unsigned long long
  unsigned long long uid_ll;
  sscanf(uid, "%Lx", &uid_ll);
  return open_uid(uid_ll);
}

bool CFirewireCamera::isOpen() const
{
  return m_handle!=NULL;
}

void CFirewireCamera::close()
{
  if(!m_handle)
    return;

  dc1394_dma_unlisten(m_handle, &m_camera);
  dc1394_dma_release_camera(m_handle, &m_camera);
  dc1394_stop_iso_transmission(m_handle, m_camera.node);

  //Close the handle
  if(m_handle)
    dc1394_destroy_handle(m_handle);
  m_handle=0;
}

  
/*
**
** Camera information
**
*/

void CFirewireCamera::print_all_info()
{
  raw1394handle_t handle;
  struct raw1394_portinfo ports[4];

  //Loop through all cameras an print their information
  handle = raw1394_new_handle();
  if(handle==NULL) {
    printf("Unable to open handle\n");
    return;
  }

  //Get number of ports
  int numPorts = raw1394_get_port_info(handle, ports, numPorts);
  raw1394_destroy_handle(handle);
  handle = NULL;

  //Go through all ports
  for(int i=0; i<numPorts; ++i) {
    handle = dc1394_create_handle(i);
    if(!handle) {
      printf("Unable to get handle for port %i\n", i);
      continue;
    }
    printf("Checking port %i\n", i);

    int numCameras = 0;
    nodeid_t * camera_nodes = dc1394_get_camera_nodes(handle, &numCameras, 0);
    for(int j=0; j<numCameras; ++j) {
      dc1394_camerainfo info;
      if(dc1394_get_camera_info(handle, camera_nodes[j], &info) == DC1394_SUCCESS) {
	dc1394_print_camera_info(&info);

	dc1394_feature_set features;
	dc1394_get_camera_feature_set(handle, camera_nodes[j], &features);
	dc1394_print_feature_set(&features);
      }
    }
    dc1394_free_camera_nodes(camera_nodes);

    if(handle)
      dc1394_destroy_handle(handle);
  }
}


/*
**
** Features
**
*/

double CFirewireCamera::exposure()
{
  unsigned int e;

  dc1394_get_exposure(m_handle, m_camera.node, &e);

  return (e-m_exposure_min)/(double)(m_exposure_max/m_exposure_min);
}

bool CFirewireCamera::autoExposure()
{
  dc1394bool_t bt;
  dc1394_is_feature_auto(m_handle, m_camera.node, FEATURE_EXPOSURE, &bt);
  return bt!=0;
}

void CFirewireCamera::setExposure(double d)
{
  if(d<0)
    d=0.0;
  if(d>1)
    d=1.0;

  unsigned int e;

  e = (unsigned int)(d*((double)(m_exposure_max-m_exposure_min)) + (double)m_exposure_min);

  dc1394_set_exposure(m_handle, m_camera.node, e);
}

void CFirewireCamera::setAutoExposure(bool b)
{
  dc1394_auto_on_off(m_handle, m_camera.node, FEATURE_EXPOSURE, b ? 1 : 0);
}


//Whitebalance
double CFirewireCamera::whitebalanceBU()
{
  unsigned int bu, rv;
  dc1394_get_white_balance(m_handle, m_camera.node, &bu, &rv);
  return (bu-m_whitebalance_min)/(double)(m_whitebalance_max-m_whitebalance_min);
}
double CFirewireCamera::whitebalanceRV()
{
  unsigned int bu, rv;
  dc1394_get_white_balance(m_handle, m_camera.node, &bu, &rv);
  return (rv-m_whitebalance_min)/(double)(m_whitebalance_max-m_whitebalance_min);
}
void CFirewireCamera::setWhitebalanceBU(double d)
{
  unsigned int bu = (unsigned int) ((m_whitebalance_max-m_whitebalance_min)*d) + m_whitebalance_min;
  unsigned int rv = (unsigned int) ((m_whitebalance_max-m_whitebalance_min)*whitebalanceRV()) + m_whitebalance_min;

  dc1394_set_white_balance(m_handle, m_camera.node, bu, rv);
}
void CFirewireCamera::setWhitebalanceRV(double d)
{
  unsigned int bu = (unsigned int) ((m_whitebalance_max-m_whitebalance_min)*whitebalanceBU()) + m_whitebalance_min;
  unsigned int rv = (unsigned int) ((m_whitebalance_max-m_whitebalance_min)*d) + m_whitebalance_min;

  dc1394_set_white_balance(m_handle, m_camera.node, bu, rv);
}


/*
**
** Image capture
**
*/

unsigned char *CFirewireCamera::capture()
{
  if(dc1394_dma_single_capture(&m_camera) != DC1394_SUCCESS)
    return NULL;
  
  return (unsigned char*)m_camera.capture_buffer;
}

void CFirewireCamera::release()
{
  dc1394_dma_done_with_buffer(&m_camera);
}

/*
**
** Helper functions
**
*/

bool CFirewireCamera::setupCamera(nodeid_t cam)
{
  int channel = 0;
  int buffers = 10;
  int drop = 1;
  if(dc1394_dma_setup_capture(m_handle, cam, channel, FORMAT_VGA_NONCOMPRESSED,
			      MODE_640x480_MONO, SPEED_400, FRAMERATE_30,
			      buffers, drop, "/dev/video1394/0",
			      &m_camera) != DC1394_SUCCESS) {
    return false;
  }

  if(dc1394_start_iso_transmission(m_handle, m_camera.node)!=DC1394_SUCCESS) {
    dc1394_dma_unlisten(m_handle, &m_camera);
    dc1394_dma_release_camera(m_handle, &m_camera);
    return false;
  }

  dc1394_get_min_value(m_handle, m_camera.node, FEATURE_EXPOSURE, &m_exposure_min);
  dc1394_get_max_value(m_handle, m_camera.node, FEATURE_EXPOSURE, &m_exposure_max);

  dc1394_get_min_value(m_handle, m_camera.node, FEATURE_WHITE_BALANCE, &m_whitebalance_min);
  dc1394_get_max_value(m_handle, m_camera.node, FEATURE_WHITE_BALANCE, &m_whitebalance_max);

  return true;
}
