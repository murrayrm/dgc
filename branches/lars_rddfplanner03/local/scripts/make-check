#!/bin/bash

# runs some basic checks of make
# currently only checks includes

DGC="../.."

if [ $# -eq 0 ] ; then
    echo "  Usage:"
    echo "    ~/dgc/local/scripts $ $0 option"
    echo "    (must be run from local/scripts directory)"
    echo
    echo "  Options:"
    echo
    echo "    setup:              make and store log.  Do a make uninstall"
    echo "                          before running this, and run this before"
    echo "                          running other checks."
    echo "    include-check, ic:  using log, check for bad includes."
    echo "    output-check, oc:   using log, check for what files are made."
    echo "                          (duplicate files are bad)"
    exit 1
fi



case $1 in
    "setup" )
	echo
	echo "Making everything (takes at least 2 minutes)..."
	echo
	make -C $DGC > makelog
	;;

    "include-check" | "ic" )
	cat makelog | gawk '{for (i = 1; i <= NF; i++) print $i " "}' | grep "\-I" | sort > includes
	cat includes \
	    | grep -v "\-I./ " \
	    | grep -v "include" \
	    | grep -v "/constants " \
	    | grep -v "\-I. " > badincludes
	echo "Whew! ok, that was fun...now for the rundown"
	echo
	echo
	echo "Summary of files created:"
	echo "  makelog       contains the output of the make command"
	echo "  includes      contains a sorted list of all the things"
	echo "                  include commands that were caught"
	echo "  badincludes   contains all the include commands deemed"
	echo "                  bad by this program"
	echo "  badlines      contains the bad lines from the makelog"
	echo
	echo "Offending lines in makelog (highlighting may not include all offensive regions):"
	echo
	cat makelog | grep --color -f badincludes > badlines
	cat makelog | grep --color -f badincludes
	;;
    
    "output-check" | "oc" )
	echo "Duplicate files (number of times made, file name)"
	echo "---------------------------------------------------"
	cat makelog \
	    | grep "g++" \
	    | gawk 'BEGIN {n=0} {for (i = 1; i <= NF; i++) {if (n == 1) {print $i; n=0;} if ($i == "-o") n=1;}}' \
	    | sort \
	    | uniq -cd
	;;
    
    * )
	echo "Unrecognized option: $1"
esac


