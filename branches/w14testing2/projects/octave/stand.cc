/* Found at http://www.octave.org/mailing-lists/help-octave/2000/1307 */

#include <iostream>
#include <octave/oct.h>

using namespace std;

main()
{ 
  ColumnVector dx(3); 
  // Indices are zero-based!  Trying to access elements out of range 
  // will cause a segfault.
  dx(0) = 1.0;
  dx(1) = 2.0;
  dx(2) = 3.0;

  double dot;

  //RowVector dxt(3);
  //dxt = dx.transpose();
  //dot = dxt * dx;

  dot = dx.transpose() * dx;
  printf("dot = %f\n", dot);
  cout << "dot = " << dot << endl;

  cout << "dx = (" << dx(0) << ", " << dx(1) << ", " << dx(2) << ")" << endl;
  // had trouble with this one.
  //cout << "dx = " << dx << endl;
}
