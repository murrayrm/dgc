/* SerialDevice.cpp for initiating a serial port and sending and receiving */
/* packets to and from the port*/

// Written by Sam Pfister 7/25/04


#include "SerialDevice.hpp"


SerialDevice::SerialDevice() 
	:_fd(-1)
{
	_debug_input = 0;
	_debug_output = 0;
}
    
SerialDevice::~SerialDevice() 
{

	  closeport();

}

SerialDevice::SerialDevice(const SerialDevice& s) 
{
  _fd = -1;
}

SerialDevice& SerialDevice::operator= (const SerialDevice& s) 
{
  if (this == &s)
    return *this;

  _fd = -1;
  return *this;
}

int SerialDevice::initport(const string& device, int baud) 
{
  if (DEBUG)	cout << endl << endl  << "Function SerialDevice::Initport()" << endl;
  
  struct termios newtio;

  //cout << "(from SerialDevice.cpp) Opening port: " << device <<endl;
  
  if (_fd >= 0){
		cout << "(from SerialDevice.cpp) Port already open" << device << endl;
    closeport();
	
  }
  
  /* Open Serial device : Can be O_NDELAY for delayed read or
		 O_NONBLOCK for no delay*/
  _fd = open( device.c_str(), O_RDWR | O_NOCTTY | O_NDELAY );
  if (_fd < 0){
    //cout << "(from SerialDevice.cpp) Can't open port" << device << endl;
		_fd = -1;
    return(0);
  }
  
  tcgetattr(_fd,&_oldtio); /* save current serial port settings */
  bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
  
  /* 
		 BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
		 CRTSCTS : output hardware flow control (only used if the cable has
		 all necessary lines. See sect. 7 of Serial-HOWTO)
		 CS8     : 8n1 (8bit,no parity,1 stopbit)
		 CLOCAL  : local connection, no modem contol
		 CREAD   : enable receiving characters
  */
  //newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
  newtio.c_cflag = B19200 | CS8 | CLOCAL | CREAD;
  /*
		IGNPAR  : ignore bytes with parity errors
		ICRNL   : map CR to NL (otherwise a CR input on the other computer
		will not terminate input)
		otherwise make device raw (no other input processing)
  */
  newtio.c_iflag = IGNPAR;
  //   newtio.c_iflag = IGNPAR | ICRNL;
  //      newtio.c_iflag = 0;
  /*
		Raw output.
  */
  newtio.c_oflag = 0;
  
  newtio.c_lflag = 0;
  
  /* 
		 initialize all control characters 
		 default values can be found in /usr/include/termios.h, and are given
		 in the comments, but we don't need them here
  */
  // newtio.c_cc[VINTR]    = 0;     /* Ctrl-c */ 
  //newtio.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
  //newtio.c_cc[VERASE]   = 0;     /* del */
  //newtio.c_cc[VKILL]    = 0;     /* @ */
  newtio.c_cc[VEOF]     = 0;     /* Ctrl-d */
  //newtio.c_cc[VTIME]    = 0;     /* inter-character timer unused */
  //newtio.c_cc[VMIN]     = 1;     /* blocking read until 1 character arrives*/
  //newtio.c_cc[VSWTC]    = 0;     /* '\0' */
  newtio.c_cc[VSTART]   = 0;     /* Ctrl-q */ 
  newtio.c_cc[VSTOP]    = 0;     /* Ctrl-s */
  newtio.c_cc[VSUSP]    = 0;     /* Ctrl-z */
  newtio.c_cc[VEOL]     = 0;     /* '\0' */
  newtio.c_cc[VREPRINT] = 0;     /* Ctrl-r */
  newtio.c_cc[VDISCARD] = 0;     /* Ctrl-u */
  newtio.c_cc[VWERASE]  = 0;     /* Ctrl-w */
  newtio.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
  newtio.c_cc[VEOL2]    = 0;     /* '\0' */
  
  
  
  /*
		now clean the modem line and activate the settings for the port
  */
  tcflush(_fd, TCIFLUSH);
  
  tcsetattr(_fd,TCSANOW,&newtio);
  
  return(1);
  /* DONE WITH SETTING PORT SETTINGS*/
  
}



string SerialDevice::readport( int insize) 
{
  char tempinarr[insize];
  string instring = "";
  int totalsize = 0;
  int thissize = 0;
  bool signalflag = false;
  
  if (DEBUG)	cout << endl << "(from serial.cc) Function SerialDevice::ReadIn()" << endl;
  // tcflush(_fd, TCIFLUSH);
  
	
  for (int i = 0; i <50; ++i){  
		usleep(10000); 
    //    cout << "in read loop " << endl;
    
    thissize = read(_fd,tempinarr,insize );
    // cout << thissize << " ";
    if (thissize<0){
      //cerr << "(from SerialDevice.cpp) Error reading from port";
    }
    if (thissize >0){
      if (_debug_input){
				int outbyte;
				if (!signalflag) {
					cout << "  INPUT | " ;
				}
				for (int j = 0; j < thissize ; ++j){
					outbyte = (int)tempinarr[j];
					outbyte = outbyte&0xff;
					if (outbyte < 0x0f)
						cout << hex << "0" << outbyte <<" | "; 
					else
						cout << hex << outbyte <<" | "; 
				}
				
				
      }
      signalflag= true;

      instring.append(tempinarr,thissize);

      totalsize = totalsize+thissize;
      assert(totalsize==(int)instring.length());
    }
    else if (signalflag) {
      // cout << "bytes read " << instring.length() << endl;
      if (_debug_input) cout << endl;
			return(instring);
    }
    
    // wait if command is not completed

  }

      if (_debug_input & signalflag) cout << endl;

  return(instring);
  
}



void SerialDevice::writeport(const string & outstring) 
{
  
  int n;
  unsigned int i;
  //   if (DEBUG)	cout << endl << endl  << "Function SerialDevice::WriteOut()" << endl;
  
  
  if(_debug_output){
    //cout << "String Size = "<< dec << outstring.length()  << endl;
		cout << "OUTPUT | " ;
		int outbyte;
		for (i = 0;i<outstring.length();++i){
			outbyte = (int)outstring[i];
			outbyte = outbyte&0xff;
			if (outbyte < 0x0f)
				cout << hex << "0" << outbyte <<" | "; 
			else
				cout << hex << outbyte <<" | "; 
		}
  }
  
  
  //tcflush(_fd, TCIFLUSH);
  n = write(_fd, outstring.c_str(), outstring.length());
  
  if (n<0){
    //cerr <<"(from SerialDevice.cpp) Error writing to port";
  }
} 


void SerialDevice::closeport() 
{
  if (DEBUG)	cout << endl << endl  << "Function SerialDevice::close()" << endl;
  /* restore the old port settings and close port*/
  if (_fd>=0){
		cout << "Closing Port" << endl;
		tcsetattr(_fd,TCSANOW,&_oldtio);
		if (close(_fd) < 0){
		  //cout << "(from SerialDevice.cpp) Error closing port" << endl;
		  //cerr << "(from SerialDevice.cpp) Error closing port" ;
				}
			_fd = -1;
	}
}
