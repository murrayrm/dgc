#include "CCorridorPainter.hh"

CCorridorPainter::CCorridorPainter(CMap* inputMap, RDDF* inputRDDF) {
  _inputMap = inputMap;
  _inputRDDF = inputRDDF;


  _prevBottomLeftRowMultiple = _inputMap->getWindowBottomLeftUTMNorthingRowResMultiple();
  _prevBottomLeftColMultiple = _inputMap->getWindowBottomLeftUTMEastingColResMultiple();
  
  _behindWaypointNum = 0;
  _aheadWaypointNum = 0; 
  
  _startingIndices = new int[inputMap->getNumRows()];
  _endingIndices = new int[inputMap->getNumRows()];
  _tracklineStartingIndices = new int[inputMap->getNumRows()];
  _tracklineEndingIndices = new int[inputMap->getNumCols()];

  _lastUsedWayptNumberRowBox = 0;
  _lastUsedWayptNumberColBox = 0;
  
  _startRow = _endRow = 0;

	_lastUsedWayptNum = 0;

	_numLayers = 0;
}


CCorridorPainter::~CCorridorPainter() {
}


void CCorridorPainter::addLayer(int layerNum, CCorridorPainterOptions* painterOpts, int useDelta) {
	_layerNums[_numLayers] = layerNum;
	_useDeltas[_numLayers] = useDelta;
	_painterOpts[_numLayers] = painterOpts;

  _painterOpts[_numLayers]->optRDDFscaling = 1.0;
  _painterOpts[_numLayers]->optMaxSpeed = 999.0;
  _painterOpts[_numLayers]->paintCenterTrackline = CCorridorPainterOptions::DONT;
  _painterOpts[_numLayers]->whatToAdjust = CCorridorPainterOptions::RDDF;
  _painterOpts[_numLayers]->adjustmentVal = 1.0;
	
	_numLayers++;
}


// int CCorridorPainter::initPainter(CMap* inputMap, 
// 																	int layerNum, 
// 																	RDDF* inputRDDF, 
// 																	int useDelta) {
//   _layerNum = layerNum;


//   _prevBottomLeftRowMultiple = _inputMap->getWindowBottomLeftUTMNorthingRowResMultiple();
//   _prevBottomLeftColMultiple = _inputMap->getWindowBottomLeftUTMEastingColResMultiple();
  
//   _behindWaypointNum = 0;
//   _aheadWaypointNum = 0; 
  
//   _useDelta = useDelta;

//   _startingIndices = new int[inputMap->getNumRows()];
//   _endingIndices = new int[inputMap->getNumRows()];
//   _tracklineStartingIndices = new int[inputMap->getNumRows()];
//   _tracklineEndingIndices = new int[inputMap->getNumCols()];

//   return 0;
//}


void CCorridorPainter::paintChanges(NEcoord rowBox[], NEcoord colBox[]) {
				    //double RDDFscaling, double maxSpeed) {
  //How the algorithm will work
  //Do rowbox and col box identically, this is for one of them
  //if we know a given waypt boundary intersects the border:
  //calculate where the western line intersects the border of the box
  //trace western ray through the map until it exits the grid
  //if it doesn't exit the grid, follow the next line, etc.
  //trace eastern ray through, same process


  const CLineSegment rowBoxWSide(rowBox[0], rowBox[1]);
  const CLineSegment rowBoxNSide(rowBox[1], rowBox[2]);
  const CLineSegment rowBoxESide(rowBox[2], rowBox[3]);
  const CLineSegment rowBoxSSide(rowBox[3], rowBox[0]);

  NEcoord rStart, rEnd, lStart, lEnd;
  NEcoord intersectionPoint;
  //cout << "making rowbox" << endl;
  CRectangle rowBoxRect(rowBox);
  //cout << "making colbox" << endl;
  CRectangle colBoxRect(colBox);

  int rMinRow, rMinCol, rMaxRow, rMaxCol;
  int cMinRow, cMinCol, cMaxRow, cMaxCol;

  _inputMap->UTM2Win(rowBox[0].N, rowBox[0].E, &rMinRow, &rMinCol);
  _inputMap->UTM2Win(rowBox[2].N, rowBox[2].E, &rMaxRow, &rMaxCol);
  _inputMap->UTM2Win(colBox[0].N, colBox[0].E, &cMinRow, &cMinCol);
  _inputMap->UTM2Win(colBox[2].N, colBox[2].E, &cMaxRow, &cMaxCol);
  
  if(rMinRow < 0) rMinRow = 0;
  if(rMinCol < 0) rMinCol = 0;
  if(cMinRow < 0) cMinRow = 0;
  if(cMinCol < 0) cMinCol = 0;
  if(rMaxRow >= _inputMap->getNumRows()) rMaxRow = _inputMap->getNumRows()-1;
  if(cMaxRow >= _inputMap->getNumRows()) cMaxRow = _inputMap->getNumRows()-1;
  if(rMaxCol >= _inputMap->getNumCols()) rMaxCol = _inputMap->getNumRows()-1;
  if(cMaxCol >= _inputMap->getNumCols()) cMaxCol = _inputMap->getNumRows()-1;
  
	int lastUsedWayptForward = -1;
	int lastUsedWayptBackward = _inputRDDF->getNumTargetPoints()-1;
	int maxWayptOffset = max(_inputRDDF->getNumTargetPoints()-1 - _lastUsedWayptNum,
													 _lastUsedWayptNum);
	double speed[MAX_CORRIDOR_LAYERS];

  for(int wayptOffset = 0; wayptOffset < maxWayptOffset; wayptOffset++) {
		int forwardWaypt = _lastUsedWayptNum + wayptOffset;
		int backwardWaypt = _lastUsedWayptNum - wayptOffset;
		if(backwardWaypt < 0) backwardWaypt = 0;
		if(forwardWaypt >= _inputRDDF->getNumTargetPoints()-1) forwardWaypt = _inputRDDF->getNumTargetPoints()-2;
		
	//for(int i=0; i < _inputRDDF->getNumTargetPoints()-1; i++) {
		int i = backwardWaypt;

    CLineSegment trackline(_inputRDDF->getWaypoint(i),
			   _inputRDDF->getWaypoint(i+1));
    CRectangle rddfRect(_inputRDDF->getWaypoint(i),
			_inputRDDF->getWaypoint(i+1),
			_inputRDDF->getOffset(i));
    CCircle rddfCircle(_inputRDDF->getWaypoint(i),
		       _inputRDDF->getOffset(i));
    CCircle nextCircle(_inputRDDF->getWaypoint(i+1),
		       _inputRDDF->getOffset(i));

		for(int index=0; index < _numLayers; index++) 
			speed[index] = fmin(_inputRDDF->getWaypointSpeed(i) * _painterOpts[index]->optRDDFscaling,
													_painterOpts[index]->optMaxSpeed);

// 		for(int index=0; index < _numLayers; index++) {
// 			if(_painterOpts[index]->whatToAdjust==CCorridorPainterOptions::RDDF &&
// 				 _painterOpts[index]->paintCenterTrackline!=CCorridorPainterOptions::DONT) {
// 				if(_painterOpts[index]->paintCenterTrackline==CCorridorPainterOptions::PERCENTAGE) {
// 					speed[index] = fmin(_inputRDDF->getWaypointSpeed(i) * 
// 															_painterOpts[index]->optRDDFscaling * 
// 															_painterOpts[index]->adjustmentVal,
// 															_painterOpts[index]->optMaxSpeed);
// 				} else {
// 					speed[index] = fmin((_inputRDDF->getWaypointSpeed(i) *
// 															 _painterOpts[index]->optRDDFscaling) +
// 															_painterOpts[index]->adjustmentVal,
// 															_painterOpts[index]->optMaxSpeed);
// 				}
// 			} else {
// 				speed[index] = fmin(_inputRDDF->getWaypointSpeed(i) * _painterOpts[index]->optRDDFscaling,
// 														_painterOpts[index]->optMaxSpeed);
// 			}

// 			if(_painterOpts[index]->whatToAdjust!=CCorridorPainterOptions::RDDF) {
// 				if(_painterOpts[index]->paintCenterTrackline==CCorridorPainterOptions::PERCENTAGE) {
// 					tracklineSpeed[index] = fmin(_inputRDDF->getWaypointSpeed(i) * 
// 																			 _painterOpts[index]->optRDDFscaling *
// 																			 _painterOpts[index]->adjustmentVal,
// 																			 _painterOpts[index]->optMaxSpeed);
// 				} else if (_painterOpts[index]->paintCenterTrackline==CCorridorPainterOptions::SHIFT) {
// 					tracklineSpeed[index] = fmin((_inputRDDF->getWaypointSpeed(i) * 
// 																				_painterOpts[index]->optRDDFscaling) + 
// 																			 _painterOpts[index]->adjustmentVal,
// 																			 _painterOpts[index]->optMaxSpeed);
// 				}
// 			} else {
// 				tracklineSpeed[index] = fmin(_inputRDDF->getWaypointSpeed(i) * _painterOpts[index]->optRDDFscaling,
// 																		 _painterOpts[index]->optMaxSpeed);
// 			}
// 		}

    if(rowBoxRect.checkIntersection(rddfCircle)==CRectangle::INTERSECTION_YES) {
      //cout << "intersection of circle " << i << " with rowbox" << endl;
      paintCircle(rddfCircle, speed,
									rMinRow, rMaxRow, rMinCol, rMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
    }
    if(colBoxRect.checkIntersection(rddfCircle)==CRectangle::INTERSECTION_YES) {
      //cout << "intersection of circle " << i << " with colbox" << endl;
      paintCircle(rddfCircle, speed,
									cMinRow, cMaxRow, cMinCol, cMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
    }

    if(rowBoxRect.checkIntersection(nextCircle)==CRectangle::INTERSECTION_YES) { 
      //cout << "intersection of next circle " << i+1 << " with rowbox" << endl;
      paintCircle(nextCircle, speed,
									rMinRow, rMaxRow, rMinCol, rMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
    }
    if(colBoxRect.checkIntersection(nextCircle)==CRectangle::INTERSECTION_YES) {
      //cout << "intersection of next circle " << i+1 << " with colbox" << endl;
      paintCircle(nextCircle, speed,
									cMinRow, cMaxRow, cMinCol, cMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
    }

// 		for(int index=0; index < _numLayers; index++) {
 			if(rddfRect.checkIntersection(rowBoxRect)==CRectangle::INTERSECTION_YES) {
 				paintRectangle(trackline, i, speed,
 											 rMinRow, rMaxRow, rMinCol, rMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
// 				if(_painterOpts[index]->paintCenterTrackline != CCorridorPainterOptions::DONT)
// 					paintTrackline(i, tracklineSpeed[index],
// 												 rMinRow, rMaxRow, rMinCol, rMaxCol, index);
 			}
 			if(rddfRect.checkIntersection(colBoxRect)==CRectangle::INTERSECTION_YES) {
 				paintRectangle(trackline, i, speed,
 											 cMinRow, cMaxRow, cMinCol, cMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
// 				if(_painterOpts[index]->paintCenterTrackline != CCorridorPainterOptions::DONT)
// 					paintTrackline(i, tracklineSpeed[index],
// 												 cMinRow, cMaxRow, cMinCol, cMaxCol, index);			
// 			}
			}

		i = forwardWaypt;

    trackline = CLineSegment(_inputRDDF->getWaypoint(i),
			   _inputRDDF->getWaypoint(i+1));
    rddfRect = CRectangle(_inputRDDF->getWaypoint(i),
			_inputRDDF->getWaypoint(i+1),
			_inputRDDF->getOffset(i));
    rddfCircle = CCircle(_inputRDDF->getWaypoint(i),
		       _inputRDDF->getOffset(i));
    nextCircle = CCircle(_inputRDDF->getWaypoint(i+1),
		       _inputRDDF->getOffset(i));

		for(int index=0; index < _numLayers; index++) 
			speed[index] = fmin(_inputRDDF->getWaypointSpeed(i) * _painterOpts[index]->optRDDFscaling,
													_painterOpts[index]->optMaxSpeed);

    if(rowBoxRect.checkIntersection(rddfCircle)==CRectangle::INTERSECTION_YES) {
      paintCircle(rddfCircle, speed,
									rMinRow, rMaxRow, rMinCol, rMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
    }
    if(colBoxRect.checkIntersection(rddfCircle)==CRectangle::INTERSECTION_YES) {
      paintCircle(rddfCircle, speed,
									cMinRow, cMaxRow, cMinCol, cMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
    }

    if(rowBoxRect.checkIntersection(nextCircle)==CRectangle::INTERSECTION_YES) { 
      paintCircle(nextCircle, speed,
									rMinRow, rMaxRow, rMinCol, rMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
    }
    if(colBoxRect.checkIntersection(nextCircle)==CRectangle::INTERSECTION_YES) {
      paintCircle(nextCircle, speed,
									cMinRow, cMaxRow, cMinCol, cMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
    }

 			if(rddfRect.checkIntersection(rowBoxRect)==CRectangle::INTERSECTION_YES) {
 				paintRectangle(trackline, i, speed,
 											 rMinRow, rMaxRow, rMinCol, rMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
 			}
 			if(rddfRect.checkIntersection(colBoxRect)==CRectangle::INTERSECTION_YES) {
 				paintRectangle(trackline, i, speed,
 											 cMinRow, cMaxRow, cMinCol, cMaxCol);
			if(i > lastUsedWayptForward) lastUsedWayptForward = i;
			if(i < lastUsedWayptBackward) lastUsedWayptBackward = i;
			}
			

			if(lastUsedWayptForward != -1 && lastUsedWayptBackward != _inputRDDF->getNumTargetPoints()-1 &&
				 lastUsedWayptForward!=forwardWaypt && lastUsedWayptBackward!=backwardWaypt &&
				 lastUsedWayptForward!=backwardWaypt && lastUsedWayptBackward!=forwardWaypt) {
				double forwardDist = (_inputRDDF->getWaypoint(lastUsedWayptForward) -
															_inputRDDF->getWaypoint(forwardWaypt)).norm();
				double backwardDist = (_inputRDDF->getWaypoint(lastUsedWayptBackward) - 
															 _inputRDDF->getWaypoint(backwardWaypt)).norm();
				double forwardDist2 = (_inputRDDF->getWaypoint(lastUsedWayptForward) -
															_inputRDDF->getWaypoint(backwardWaypt)).norm();
				double backwardDist2 = (_inputRDDF->getWaypoint(lastUsedWayptBackward) - 
															 _inputRDDF->getWaypoint(forwardWaypt)).norm();
				double distThresh = hypot(_inputMap->getResRows()*_inputMap->getNumRows(),
																 _inputMap->getResCols()*_inputMap->getNumCols());
				if(forwardDist > distThresh && backwardDist > distThresh &&
					 forwardDist2 > distThresh && backwardDist2 > distThresh) {
// 					cout << "QUITTING distthresh was " << distThresh
// 							 << " backdist was " << backwardDist << " to waypt " << backwardWaypt << " from waypt " << lastUsedWayptBackward
// 							 << " foredist was " << forwardDist << " to waypt " << forwardWaypt << " from waypt " << lastUsedWayptForward
// 							 << endl;
					return;
// 				} else {
// 					cout << "not qutting distthresh was " << distThresh
// 							 << " backdist was " << backwardDist << " to waypt " << backwardWaypt << " from waypt " << lastUsedWayptBackward
// 							 << " foredist was " << forwardDist << " to waypt " << forwardWaypt << " from waypt " << lastUsedWayptForward
// 							 << endl;
				}
// 			} else {
// 				cout << "lasusedfwd=" << lastUsedWayptForward 
// 						 << ", fwdwaypt=" << forwardWaypt
// 						 << ", lastusedbkwd=" << lastUsedWayptBackward
// 						 << ", bckwaypt=" << backwardWaypt << endl;
			}

				 

	}
	//}
	
	if(lastUsedWayptForward != -1 &&
		 lastUsedWayptBackward != -1) 
		_lastUsedWayptNum = (lastUsedWayptForward-lastUsedWayptBackward)/2 + lastUsedWayptBackward;
	else 
		_lastUsedWayptNum = (_inputRDDF->getNumTargetPoints()-1)/2;

}


void CCorridorPainter::findStartingIndices(NEcoord bottomPoint, NEcoord middlePoint,
					   NEcoord topPoint,
					   int minRow, int maxRow,
					   int minCol, int maxCol) {
  /*
  int middleCol;
  int middleRow;
  _inputMap->UTM2Win(middlePoint.N, middlePoint.E, &middleRow, &middleCol);
  _startingIndices[middleRow] = middleCol;
  */
  traceRay(middlePoint, bottomPoint, _startingIndices,
  //traceRay(bottomPoint, middlePoint, _startingIndices,
	   minRow, maxRow, minCol, maxCol, 1);
  traceRay(middlePoint, topPoint, _startingIndices,
  //traceRay(topPoint, middlePoint, _startingIndices,
	   minRow, maxRow, minCol, maxCol, 1);
}

void CCorridorPainter::findEndingIndices(NEcoord bottomPoint, NEcoord middlePoint,
					 NEcoord topPoint,
					 int minRow, int maxRow,
					 int minCol, int maxCol) {
  /*
  int middleCol;
  int middleRow;
  _inputMap->UTM2Win(middlePoint.N, middlePoint.E, &middleRow, &middleCol);
  _startingIndices[middleRow] = middleCol;
  */
  //traceRay(bottomPoint, middlePoint, _endingIndices, 
  traceRay(middlePoint, bottomPoint, _endingIndices,
	   minRow, maxRow, minCol, maxCol, -1);
  //traceRay(topPoint, middlePoint, _endingIndices, 
  traceRay(middlePoint, topPoint, _endingIndices,
	   minRow, maxRow, minCol, maxCol, -1);
}


void CCorridorPainter::traceRay(NEcoord start, NEcoord end, int indexList[],
				int minRow, int maxRow,
				int minCol, int maxCol, int outOnWhichSide) {
  int startRow, startCol, endRow, endCol;
  _inputMap->UTM2Win(start.N, start.E, &startRow, &startCol);
  _inputMap->UTM2Win(end.N, end.E, &endRow, &endCol);

  if(startRow == endRow) {
    if(startRow >= minRow && startRow <= maxRow) {
      if(startCol < endCol) {
	indexList[startRow] = startCol;
      } else {
	indexList[startRow] = endCol;
      }
    return;
    }
  }

  int d_row;
  if(startRow < endRow) {
    d_row = 1;
  } else {
    d_row = -1;
  }
  int d_col;
  if(startCol < endCol) {
    d_col = 1;
  } else {
    d_col = -1;
  }
  /*
  cout << "srow is " << startRow << ", erow is " << endRow << " so d_row is " << d_row 
       << " and scol is " << startCol << " and ecol is " << endCol 
       << " and d_col is " << d_col << endl;
  */

  int row = startRow;
  int col = startCol;
  int colToSet;
  double slope = (start.N-end.N)/(start.E-end.E);
  
  double t_x=0, t_y=0;

  double resCols = _inputMap->getResCols();
  double resRows = _inputMap->getResRows();

  double yShift = resCols*slope;
  double xShift = resRows/slope;

  colToSet = col;
  if(colToSet < minCol) colToSet = minCol;
  if(colToSet > maxCol) colToSet = maxCol;
  if(row >= minRow && row <=maxRow) 
    indexList[row] = colToSet;

  NEcoord xPoint, yPoint, firstRowParallelIntercept, firstColParallelIntercept;
  _inputMap->roundUTMToCellBottomLeft(start.N, start.E,
				      &firstRowParallelIntercept.N,
				      &firstColParallelIntercept.E);
  firstRowParallelIntercept.N += (d_row > 0) ? resRows : 0;
  firstColParallelIntercept.E += (d_col > 0) ? resCols : 0;
  firstRowParallelIntercept.E = (firstRowParallelIntercept.N - start.N)/slope + start.E;
  firstColParallelIntercept.N = (firstColParallelIntercept.E - start.E)*slope + start.N;

  /*
  if(_debuggit==1 && indexList==_endingIndices) {
    cout << setprecision(10) << endl;
    cout << "First rpari is ";
    firstRowParallelIntercept.display();
    cout << "First cpari is ";
    firstColParallelIntercept.display();
    cout << "And start is ";
    start.display();
    cout << "end is ";
    end.display();
    cout << "xshift is " << xShift << " yshift is " << yShift 
	 << " slope is " << slope << endl;
    cout << endl << "starting" << endl;
  }
  */

  while(row != endRow) {
    xPoint.N = firstRowParallelIntercept.N + d_row*abs(row-startRow)*resRows;
    xPoint.E = firstRowParallelIntercept.E + d_row*abs(row-startRow)*xShift;
    yPoint.N = firstColParallelIntercept.N + d_col*abs(col-startCol)*yShift;
    yPoint.E = firstColParallelIntercept.E + d_col*abs(col-startCol)*resCols;
    /*
    if(_debuggit==1 && indexList==_endingIndices) {
      cout << endl;
      cout << "xpoint is: " << setprecision(10);
      xPoint.display();
      cout << "ypoint is ";
      yPoint.display();
    }
    */
    t_x = (start-xPoint).norm();
    t_y = (start-yPoint).norm();

    if(t_x < t_y) {
      //row goes up or down
      /*
      if(_debuggit==1 && indexList==_endingIndices) {
	cout << "choosing xpoint (ropar) as closer" << endl;
	cout << "At row=" << row << ", col=" << col << ", moving to row=";
      }
      */
      row+=d_row;
      /*
    if(_debuggit==1 && indexList==_endingIndices)
      cout << row << ", t_y is " << t_y << ", tx is " << t_x << endl;
      */
      colToSet = col;
      if(colToSet < minCol) {
	/*
	cout << "Col is " << col
	     << " setting to mincol " << minCol
	     << " row is " << row << endl;
	*/
	if(outOnWhichSide==-1) {
	  colToSet = minCol-1;
	} else {
	  colToSet = minCol;	
	}
      }
      if(colToSet > maxCol) {
	/*
	cout << "Col is " << col
	     << " setting to maxcol " << maxCol
	     << " row is " << row << endl;
	*/
	if(outOnWhichSide==1) {
	  colToSet = maxCol+1;
	} else {
	  colToSet = maxCol;
	}
      }
      if(row >= minRow && row <= maxRow)
	indexList[row] = colToSet;
    } else {
      /*
      if(_debuggit==1 && indexList==_endingIndices) {
	cout << "choosing ypoint (colpar) as closer" << endl;
      cout << "At row=" << row << ", col=" << col << ", moving to col=";
      }*/
      col+=d_col;
      /*
    if(_debuggit==1 && indexList==_endingIndices)      
      cout << col << ", t_y is " << t_y << ", tx is " << t_x << endl;
      */
    }
  }
}

void CCorridorPainter::paintCircle(CCircle circle, double speed[],
				   int minRow, int maxRow,
				   int minCol, int maxCol) {
  int startRow, endRow, tempInt;
  _inputMap->UTM2Win(circle._center.N-circle._radius, circle._center.E, &startRow, &tempInt);
  if(startRow < minRow)
    startRow = minRow;
  if(startRow > maxRow)
    startRow = maxRow;
  
  _inputMap->UTM2Win(circle._center.N+circle._radius, circle._center.E, &endRow, &tempInt);
  if(endRow < minRow)
    endRow = minRow;
  if(endRow > maxRow)
    endRow = maxRow;

  double x_val, y_val, offset;
  double start_x_val, end_x_val;
  int col, startCol, endCol;

  double origSpeed, newSpeed;
  for(int row=startRow; row<=endRow; row++) {
    _inputMap->Win2UTM(row, tempInt, &y_val, &x_val);
    _inputMap->roundUTMToCellBottomLeft(y_val, x_val, &y_val, &x_val);
    offset = sqrt(pow(circle._radius,2) - pow(y_val-circle._center.N,2));
    start_x_val = circle._center.E - offset;
    end_x_val = circle._center.E + offset;
    _inputMap->UTM2Win(y_val, start_x_val, &tempInt, &startCol);
    _inputMap->UTM2Win(y_val, end_x_val, &tempInt, &endCol);
    if(!((startCol > maxCol && endCol > maxCol) ||
       (startCol < minCol && endCol < minCol))) {
      if(startCol < minCol) startCol = minCol;
      if(startCol > maxCol) startCol = maxCol;
      if(endCol < minCol) endCol = minCol;
      if(endCol > maxCol) endCol = maxCol;
      for(col=startCol; col <=endCol; col++) {
				for(int index=0; index < _numLayers; index++) {
					origSpeed = _inputMap->getDataWin<double>(_layerNums[index], row, col);
					newSpeed = (speed[index] < 0) ? speed[index] : fmax(speed[index], origSpeed);
					if(_useDeltas[index]) {
						_inputMap->setDataWin_Delta<double>(_layerNums[index], row, col, newSpeed);
					} else {
						_inputMap->setDataWin<double>(_layerNums[index], row, col, newSpeed);
					}		 
				}
      }
    }
  }
}



void CCorridorPainter::paintRectangle(CLineSegment trackline, int i, double speed[], 
				      int minRow, int maxRow,
				      int minCol, int maxCol) {
  int tempInt;

  NEcoord normalVector(-(trackline.endPoint.E - trackline.startPoint.E),
		       trackline.endPoint.N - trackline.startPoint.N);
  normalVector/=normalVector.norm();
  NEcoord bottomRight = trackline.startPoint + normalVector*_inputRDDF->getOffset(i);
  NEcoord bottomLeft = trackline.startPoint - normalVector*_inputRDDF->getOffset(i);
  NEcoord topRight = trackline.endPoint + normalVector*_inputRDDF->getOffset(i);
  NEcoord topLeft = trackline.endPoint - normalVector*_inputRDDF->getOffset(i);
  NEcoord bottom, leftMiddle, rightMiddle, top;
  if(trackline.startPoint.E < trackline.endPoint.E) {
    bottom = bottomRight;
    leftMiddle = bottomLeft;
    rightMiddle = topRight;
    top = topLeft;
  } else {
    bottom = bottomLeft;
    leftMiddle = topLeft;
    rightMiddle = bottomRight;
    top = topRight;
  }
  _inputMap->UTM2Win(bottom.N, bottom.E, &_startRow, &tempInt);
  if(_startRow < minRow)
    _startRow = minRow;
  if(_startRow > maxRow)
    _startRow = maxRow;
  
  _inputMap->UTM2Win(top.N, top.E, &_endRow, &tempInt);
  if(_endRow < minRow)
    _endRow = minRow;
  if(_endRow > maxRow)
    _endRow = maxRow;
  
  findStartingIndices(bottom, leftMiddle, top,
		      minRow, maxRow, minCol, maxCol);
  findEndingIndices(bottom, rightMiddle, top,
		      minRow, maxRow, minCol, maxCol);
  double origSpeed, newSpeed;
  for(int row = _startRow; row <= _endRow; row++) {
    for(int col = _startingIndices[row]; col <= _endingIndices[row]; col++) {
			for(int index=0; index < _numLayers; index++) {
				origSpeed = _inputMap->getDataWin<double>(_layerNums[index], row, col);
				newSpeed = (speed[index] < 0) ? speed[index] : fmax(speed[index], origSpeed);
				if(_useDeltas[index]) {
					_inputMap->setDataWin_Delta<double>(_layerNums[index], row, col, newSpeed);
				} else {
					_inputMap->setDataWin<double>(_layerNums[index], row, col, newSpeed);
				}
      }
    }
  }
}


void CCorridorPainter::repaintAll() {
  NEcoord rowBox[4], colBox[4];

  _inputMap->getEntireMapBoxUTM(rowBox);
  for(int i=0; i<4; i++)
    colBox[i] = NEcoord(0.0, 0.0);

  paintChanges(rowBox, colBox);
	       //, RDDFscaling, maxSpeed);
}


// void CCorridorPainter::setPainterOptions(CCorridorPainterOptions* painterOpts) {
//   _painterOpts = *painterOpts;
// }


void CCorridorPainter::paintTrackline(int i, double speed,
				      int minRow, int maxRow,
				      int minCol, int maxCol, int index) {
  int tempInt;

  NEcoord bottom=_inputRDDF->getWaypoint(i);
  NEcoord top = _inputRDDF->getWaypoint(i+1);

  if(bottom.N > top.N) {
    NEcoord temp = top;
    top = bottom;
    bottom = temp;
  }

  _inputMap->UTM2Win(bottom.N, bottom.E, &_startRow, &tempInt);
  if(_startRow < minRow)
    _startRow = minRow;
  if(_startRow > maxRow)
    _startRow = maxRow;
  
  _inputMap->UTM2Win(top.N, top.E, &_endRow, &tempInt);
  if(_endRow < minRow)
    _endRow = minRow;
  if(_endRow > maxRow)
    _endRow = maxRow;
  
  traceRay(bottom, top, _tracklineStartingIndices,
	   minRow, maxRow, minCol, maxCol, 1);
  traceRay(top, bottom, _tracklineEndingIndices,
	   minRow, maxRow, minCol, maxCol, -1);  


  for(int row = _startRow; row <= _endRow; row++) {
    for(int col = _tracklineStartingIndices[row]; col <= _tracklineEndingIndices[row]; col++) {
			if(_useDeltas[index]) {
				_inputMap->setDataWin_Delta<double>(_layerNums[index], row, col, speed);
			} else {
				_inputMap->setDataWin<double>(_layerNums[index], row, col, speed);
			}		 
    }
  }
}


void CCorridorPainter::paintData(NEcoord vehicleLoc, double yaw, double speed, double distanceAhead, double distanceBehind) {
	NEcoord endPoint = NEcoord(vehicleLoc.N+distanceAhead*cos(yaw),
														 vehicleLoc.E+distanceAhead*sin(yaw));
	NEcoord startPoint = NEcoord(vehicleLoc.N-distanceBehind*cos(yaw),
															 vehicleLoc.E-distanceBehind*sin(yaw));
//_inputRDDF->getPointAlongTrackLine(vehicleLoc, distance);

	CLineSegment centerLine(endPoint, startPoint);

	double* speeds = new double[_numLayers];

	for(int i=0; i<_numLayers; i++)
		speeds[i] = speed;

	int i = _inputRDDF->findWaypointWithLargestWidth(vehicleLoc);

	paintRectangle(centerLine, i, speeds, 0, _inputMap->getNumRows()-1, 0, _inputMap->getNumCols()-1);

	free(speeds);
}
