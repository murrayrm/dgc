#include "CTerrainCostPainter.hh"

#define PARSEOPT(type, name, val) \
	if(fieldName == #name) \
    linestream >> _options[layerIndex]->name; \
  else

#define PARSEOPTS OPTIONSLIST(PARSEOPT)

#define DEFAULTOPT(type, name, val) \
	_options[_numLayers]->name = val;

#define DEFAULTOPTS OPTIONSLIST(DEFAULTOPT)


CTerrainCostPainter::CTerrainCostPainter(CMapPlus* map,  CCostFuser* costFuser) {
	_inputMap = map;
	//_layerNumRDDF = layerNumRDDF;
	_numLayers = 0;
	_costFuser = costFuser;

  _resRows = _inputMap->getResRows();
  _resCols = _inputMap->getResCols();
	_numRows = _inputMap->getNumRows();
	_numCols = _inputMap->getNumCols();
       
}


CTerrainCostPainter::~CTerrainCostPainter() {

}

int CTerrainCostPainter::addElevLayer(int elevLayer, CTerrainCostPainterOptions* layerOptions, bool makeCostUseDeltas, bool useDefaults) {	
	_elevLayerNums[_numLayers] = elevLayer;
	_costLayerNums[_numLayers] = _inputMap->addLayer<double>(CONFIG_FILE_COST, makeCostUseDeltas);	
	_changesLayerNums[_numLayers] = _inputMap->addLayer<int>(CONFIG_FILE_CHANGES, true);
	_options[_numLayers] = layerOptions;

	if(useDefaults)
		DEFAULTOPTS;

	recomputeKernel(_numLayers);

	_numLayers++;

	return _numLayers-1;
}


unsigned long long CTerrainCostPainter::paintCell(int layerIndex, double UTMNorthing, double UTMEasting, int costFuserLayerIndex) {
	//Are we inside the map bounds?
  if (_inputMap->checkBoundsUTM (UTMNorthing, UTMEasting) != CMap::CM_OK)
    return ERROR;
	//Are we inside the RDDF?
  /*  if(_inputMap->getDataUTM<double>(_layerNumRDDF, UTMNorthing, UTMEasting) ==
     _inputMap->getLayerNoDataVal<double>(_layerNumRDDF) &&
		 _options[layerIndex]->paintInsideRDDFOnly == true)
		 return ERROR;*/
	//Do we have elevation data?
	if(_inputMap->getDataUTM<CElevationFuser>(_elevLayerNums[layerIndex], UTMNorthing, UTMEasting) ==
		 _inputMap->getLayerNoDataVal<CElevationFuser>(_elevLayerNums[layerIndex]))
		return ERROR;

	//Initialize some variables for later use
  double finalSpeed = MAX_SPEED;
  double obstacleSpeed = MAX_SPEED;
  double speedNoDataVal = _inputMap->getLayerNoDataVal<double>(_costLayerNums[layerIndex]);

  int currentRow, currentCol;
  int kernelRowSize, kernelColSize;
  _inputMap->UTM2Win(UTMNorthing, UTMEasting, &currentRow, &currentCol);
	kernelRowSize = _options[layerIndex]->kernelSize;
	kernelColSize = kernelRowSize;
	
	//Bound the kernel if we're close to the edge of the map
  int minRow, maxRow, minCol, maxCol;
  _inputMap->constrainRows(currentRow, kernelRowSize, minRow, maxRow);
  _inputMap->constrainCols(currentCol, kernelColSize, minCol, maxCol);

	CElevationFuser cellFused;
  CElevationFuser fusedNoDataVal = _inputMap->getLayerNoDataVal<CElevationFuser>(_elevLayerNums[layerIndex]);
	double totalGrad = 0.0;
	int numSupportingCellsUsed = 0;
  CElevationFuser fusedElevData = _inputMap->getDataUTM<CElevationFuser>(_elevLayerNums[layerIndex], UTMNorthing, UTMEasting);
	double currentCellElev = fusedElevData.getMeanElevation();

  if(fusedElevData.getStdDev() > _options[layerIndex]->stdDevThreshold) {
    obstacleSpeed = speedNoDataVal+2.0*EPSILON_SPEED;
  }

	int localMinRow, localMaxRow, localMinCol, localMaxCol;

	if(_options[layerIndex]->veloGenAlgNum == 4) {
		_inputMap->constrainRows(currentRow, kernelRowSize, minRow, maxRow);
		_inputMap->constrainCols(currentCol, kernelColSize, minCol, maxCol);
		for(int r=minRow; r<=maxRow; r++) {
			for(int c=minCol; c<=maxCol; c++) {
				CElevationFuser& centerCell = _inputMap->getDataWin<CElevationFuser>(_elevLayerNums[layerIndex], r, c);
				if(centerCell != fusedNoDataVal) {
					_inputMap->constrainRows(r, _options[layerIndex]->averagingKernelSize, localMinRow, localMaxRow);
					_inputMap->constrainCols(c, _options[layerIndex]->averagingKernelSize, localMinCol, localMaxCol);
					double newElev = 0.0;
					int validCellCtr = 0;
					for(int lr = localMinRow; lr<= localMaxRow; lr++) {
						for(int lc = localMinCol; lc <= localMaxCol; lc++) {
							cellFused = _inputMap->getDataWin<CElevationFuser>(_elevLayerNums[layerIndex], r, c);
							if(cellFused != fusedNoDataVal) {
								newElev+=cellFused.getMeanElevation();							
								validCellCtr++;
							}
						}
					}
					centerCell.setMeanElevation(newElev/((double)validCellCtr));
				}
			}
		}
	}


  _inputMap->constrainRows(currentRow, kernelRowSize, minRow, maxRow);
  _inputMap->constrainCols(currentCol, kernelColSize, minCol, maxCol);		

  for(int r = minRow; r<=maxRow; r++) {
    for(int c = minCol, i=0; c<=maxCol; c++, i++) {
      cellFused = _inputMap->getDataWin<CElevationFuser>(_elevLayerNums[layerIndex], r, c);
      if(cellFused != fusedNoDataVal && !(r==currentRow && c==currentCol)) {
				totalGrad += fabs((currentCellElev - cellFused.getMeanElevation())*_options[layerIndex]->filterKernel[c-currentCol + kernelColSize + (kernelRowSize*2+1)*(r-currentRow + kernelRowSize)]);
				numSupportingCellsUsed++;
			}
    }
  }
	double avgGrad = totalGrad/((double)numSupportingCellsUsed);

	//We should use at least some number of cells before we think
	//we have enough information to assign this cell a speed
  if(numSupportingCellsUsed < _options[layerIndex]->minNumSupportingCells)
    return ERROR;


  if(_options[layerIndex]->veloGenAlgNum ==3){
    finalSpeed = generateVeloFromGrad3(fabs(avgGrad*100.0), _options[layerIndex]->zeroGradSpeed, _options[layerIndex]->unpassableObsHeight,_options[layerIndex]->veloTweak1,_options[layerIndex]->veloTweak2);
  } else{
		finalSpeed = atan(fabs(avgGrad*100.0), _options[layerIndex]->veloTweak1, _options[layerIndex]->zeroGradSpeed/(2.0*0.44704),
											-_options[layerIndex]->zeroGradSpeed/(2.0*0.44704), _options[layerIndex]->veloTweak2, 0.44704, _options[layerIndex]->veloTweak3);
  }

	//We want the smaller of speeds from our obstacle detector and our bumpiness detector
  finalSpeed = fmin(finalSpeed, obstacleSpeed);


// 	//And we want the smaller of speeds from above and the RDDF
//   finalSpeed = fmin(finalSpeed, _inputMap->getDataUTM<double>(_layerNumRDDF, UTMNorthing, UTMEasting));
	//And of course, we don't want a speed higher than our max speed
	finalSpeed = fmin(finalSpeed, _options[layerIndex]->maxSpeed);
	//And we want the larger of speeds from above and the min speed of the map
  finalSpeed = fmax(finalSpeed, speedNoDataVal + EPSILON_SPEED);

  //don't paint obstacles at a far distance
  if(_options[layerIndex]->obsDistLimit < _numRows*_resRows){  //no need to waste time computing stuff we won't use
    double distance = pow((pow((currentRow-_numRows/2)*_resRows ,2.0  )+pow((currentCol-_numCols/2)*_resCols,2.0)),.5);
    if(distance > _options[layerIndex]->obsDistLimit ){
      finalSpeed = fmax( _options[layerIndex]->distantObsSpeed, finalSpeed);
    }
  }

	//Paint our speed
  _inputMap->setDataUTM_Delta<double>(_costLayerNums[layerIndex], UTMNorthing, UTMEasting, finalSpeed);

  //  DGClockMutex(fusionMutex);
  return _costFuser->markChangesTerrain(costFuserLayerIndex, NEcoord(UTMNorthing, UTMEasting));
  //DGCunlockMutex(fusionMutex);

	//return OK;
}

	
CTerrainCostPainter::STATUS CTerrainCostPainter::markChanges(int layerIndex, double UTMNorthing, double UTMEasting, CElevationFuser::STATUS forgetSurrounding) {
	int currentRow, currentCol, minRow, minCol, maxRow, maxCol;
  int kernelRowSize = _options[layerIndex]->kernelSize;
  int kernelColSize = kernelRowSize;
	_inputMap->UTM2Win(UTMNorthing, UTMEasting, &currentRow, &currentCol);

	unsigned long long neighborsTimestamp = _inputMap->getDataWin<CElevationFuser>(_elevLayerNums[layerIndex], currentRow, currentCol).getTimestamp();
	//CElevationFuser::STATUS forgetMoreSurrounding;

  _inputMap->constrainRows(currentRow, kernelRowSize, minRow, maxRow);
  _inputMap->constrainCols(currentCol, kernelColSize, minCol, maxCol);
	for(int r=minRow; r	<= maxRow; r++) {
		for(int c=minCol; c<=maxCol; c++) {
			_inputMap->setDataWin_Delta<int>(_changesLayerNums[layerIndex], r, c, 1);			
		}
	}

	if(forgetSurrounding == CElevationFuser::OK_OVERWRITTEN) {
  _inputMap->constrainRows(currentRow, kernelRowSize*2, minRow, maxRow);
  _inputMap->constrainCols(currentCol, kernelColSize*2, minCol, maxCol);
	for(int r=minRow; r <= maxRow; r++) {
		for(int c=minCol; c <= maxCol; c++) {
			CElevationFuser& tempCell2 = _inputMap->getDataWin<CElevationFuser>(_elevLayerNums[layerIndex], r, c);
			tempCell2.resetNoData(neighborsTimestamp);
		}
	}
	}
	return OK;
}

	
unsigned long long CTerrainCostPainter::paintChanges(int layerIndex, int costFuserLayerIndex) {
	double UTMNorthing=0.0, UTMEasting=0.0;
	int numCells = _inputMap->getCurrentDeltaSize(_changesLayerNums[layerIndex]);
	unsigned long long waitTime = 0;

	DGCreadlockRWLock(&(_costFuser->_swapLock));
	for(int i=0; i < numCells; i++) {
		_inputMap->getCurrentDeltaVal<int>(i, _changesLayerNums[layerIndex], &UTMNorthing, &UTMEasting);				
		waitTime+=paintCell(layerIndex, UTMNorthing, UTMEasting, costFuserLayerIndex);
	}
	DGCunlockRWLock(&(_costFuser->_swapLock));

	_inputMap->resetDelta<int>(_changesLayerNums[layerIndex]);

	return OK;
}


CTerrainCostPainter::STATUS CTerrainCostPainter::repaintAll(int layerIndex, int costFuserLayerIndex) {
	double UTMNorthing, UTMEasting;

	for(int r=0; r < _inputMap->getNumRows(); r++) {
		for(int c=0; c < _inputMap->getNumCols(); c++) {
			_inputMap->Win2UTM(r, c, &UTMNorthing, &UTMEasting);
			markChanges(layerIndex, UTMNorthing, UTMEasting, CElevationFuser::OK);
		}
	}

	paintChanges(layerIndex, costFuserLayerIndex);

	return OK;
}


CTerrainCostPainter::STATUS CTerrainCostPainter::recomputeKernel(int layerIndex) {
	int kernelRowSize = _options[layerIndex]->kernelSize;
  int kernelColSize = kernelRowSize;
	double sigma = _options[layerIndex]->sigma;

	for(int r=0; r<kernelRowSize*2+1; r++) {
		for(int c=0; c<kernelColSize*2+1; c++) {
			//Make a simple gaussian kernel
			_options[layerIndex]->filterKernel[c+ r*(kernelRowSize*2+1)] = exp(-(pow((double)(r-kernelRowSize),2.0)+pow((double)(c-kernelColSize),2.0))/(2.0*pow(sigma,2.0))) /
				(sigma*sqrt(2*M_PI));
		}
	}

	return OK;
}


CTerrainCostPainter::STATUS CTerrainCostPainter::readOptionsFromFile(int layerIndex, const char* filename) {
  ifstream optionsFile(filename);
  string line;
  string fieldName;

	int oldKernelSize = _options[layerIndex]->kernelSize;
	double oldSigma = _options[layerIndex]->sigma;

  // do nothing if the specs file wasn't opened properly
  if(optionsFile) {
    while(optionsFile) {		
      getline(optionsFile,line);
      istringstream linestream(line);
      linestream >> fieldName;
      
      PARSEOPTS;
    }
  }

	if(oldKernelSize != _options[layerIndex]->kernelSize ||
		 oldSigma != _options[layerIndex]->sigma)
		recomputeKernel(layerIndex);

  return OK;
}


double CTerrainCostPainter::generateVeloFromGrad3(double gradient, double zeroGradSpeed, double unpassableObsHeight, double tweak1, double tweak2){
   double temp = fabs (gradient);
  if (temp < unpassableObsHeight){
   temp =  pow((unpassableObsHeight - pow(temp,tweak2)/pow(unpassableObsHeight,tweak2 - 1)) * zeroGradSpeed / unpassableObsHeight,tweak1)*pow(zeroGradSpeed,1.0-tweak1);
 
 
   // cout << tweak1 <<  " t1 t2 " << tweak1 << endl;
  //  cout << gradient << " gr  velo  " << temp  << endl;
  return temp;
  }
 else{
   //    return _inputMap->getLayerNoDataVal<double>(_layerNumRDDF);
   //   return RDDFnoDataVal;
	 return 0;
}
}


double CTerrainCostPainter::atan(double val, double xCenter, double yCenter, double verticalHalfRange, double steepness, double multiplier, double shifter) {
	return multiplier*(verticalHalfRange*2/M_PI*tanh(steepness*(val - xCenter)) + yCenter) + shifter;
}
