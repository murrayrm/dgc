#include "sn_msg.hh"
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <stdlib.h>
using namespace std;

#include "DGCutils"

#define MAX_NUM_MAILBOXES 100
#define I_AM_HERE_PERIOD  1000000

skynet::skynet (modulename snname, int snkey)
{
  m_key          = snkey;
  m_name         = snname;

	m_pMailboxes   = new mailbox[MAX_NUM_MAILBOXES];
	m_pGroupnames  = new string[MAX_NUM_MAILBOXES];
	m_numMailboxes = 0;
	m_highestOpenMailboxIndex = 0;

	m_sendMailbox = spreadConnect();

	DGCcreateMutex(&m_mailboxMutex);

	// start the id-broadcasting threads
	DGCstartMemberFunctionThread(this, &skynet::I_am_here);
}

skynet::~skynet()
{
	for(int i=0; i<m_numMailboxes; i++)
	{
		SP_leave(m_pMailboxes[i], m_pGroupnames[i].c_str());
		SP_disconnect(m_pMailboxes[i]);
	}

	SP_disconnect(m_sendMailbox);

	delete[] m_pMailboxes;
	delete[] m_pGroupnames;
	DGCdeleteMutex(&m_mailboxMutex);
}

/** This function opens a spread mailbox and returns the index of the
		mailbox. It cycles through various private connection names until spread
		accepts one */
mailbox skynet::spreadConnect(void)
{
	int res;
	ostringstream privateNameStream;
	char privateGroup[MAX_GROUP_NAME];

	mailbox mbox;

	const char *pDaemon;
	char *pDaemonEnv = getenv("SPREAD_DAEMON");
	pDaemon = (pDaemonEnv == NULL) ? "4803" : pDaemonEnv;
	do
	{
		privateNameStream.str("");
		privateNameStream << (++m_highestOpenMailboxIndex);
		res = SP_connect( pDaemon, privateNameStream.str().c_str(), 0, 0, &mbox, privateGroup);
	} while(res == REJECT_NOT_UNIQUE);
	if(res != ACCEPT_SESSION)
	{
		cerr << "skynet::spreadConnect: couldn't SP_connect(): " << endl;
		SP_error(res);
		exit(-1);
	}

	return mbox;
}

/** listen takes as an argument the type of messages you want to listen to and
		the name of the module that is sending them.  You can't listen to all
		messages of a all types.  Return value is the channel to use.  return value
		of -1 indicates error.  This returns an index into the m_pMailboxes array
*/
int skynet::listen(sn_msg type, modulename somemodule)
{
	DGClockMutex(&m_mailboxMutex);

	int res;

	m_pMailboxes[m_numMailboxes] = spreadConnect();

	string groupName;
	makeGroupName(&groupName, type);

	res = SP_join(m_pMailboxes[m_numMailboxes], groupName.c_str());
	if(res == 0)
	{
		m_pGroupnames[m_numMailboxes] = groupName;
		res = m_numMailboxes++;
		DGCunlockMutex(&m_mailboxMutex);
		return res;
	}

	cerr << "SP_join fucked up" << endl;
	SP_error(res);
	DGCunlockMutex(&m_mailboxMutex);
	exit(-1);
}

/** sn_get_send_sock gets a socket to send stuff to.  It returns the type. The
		assumption is that the same skynet object will be used to both get the send
		socket and to send the messages*/
int skynet::get_send_sock(sn_msg type)
{
	return (int)type;
}

void skynet::sn_select(int mboxidx)
{
  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(m_pMailboxes[mboxidx], &readfds);

  if(select(m_pMailboxes[mboxidx] + 1, &readfds, NULL, NULL, NULL) < 0)
  {
    perror("skynet::sn_select(): select failed. fuck");
    exit(-1);
  }
}

/** check to see if more messages are available */
bool skynet::is_msg(int mboxidx)
{
  return SP_poll(m_pMailboxes[mboxidx]) > 0;
}

/** sn_get_msg gets a message.  mboxidx is what you got from sn_listen.  mybuf
		is a buffer that the user must allocate.  bufsize is the size in bytes of
		that buffer.  options ignored. pMutex, if specified, will lock before the
		data is written and unlock immediately after. If bReleaseMutexWhenDone is
		false, the mutex will not be unlocked. Return value is #of bytes read
*/
size_t skynet::get_msg (int mboxidx, void *mybuf, size_t bufsize, int options)
{
	return get_msg(mboxidx, mybuf, bufsize, options, NULL, false, 0);
}

size_t skynet::get_msg (int mboxidx, void *mybuf, size_t bufsize, int options,
												pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone, int numMutices)
{
	service messageService = DROP_RECV;
	char    sender[MAX_GROUP_NAME];
	int     numGroups;
	int16   messtype;
	int     endianMismatch;
	int     numreceived;

	if(numMutices != 0)
	{
		sn_select(mboxidx);
		for(int i=0; i<numMutices; i++)
			DGClockMutex(ppMutex[i]);
	}
		
	numreceived = SP_receive(m_pMailboxes[mboxidx], &messageService, sender, 0, &numGroups, NULL, &messtype, &endianMismatch, bufsize, (char*)mybuf);

	if(bReleaseMutexWhenDone)
		for(int i=0; i<numMutices; i++)
			DGCunlockMutex(ppMutex[i]);

	if(numreceived < 0)
	{
		cerr << "skynet::get_msg(): spread error:" << endl;
		SP_error(numreceived);
	}

	return numreceived;
}

/** sn_send_msg sends a message.  You cannot choose to whom to send a message,
		you may only send it.  return value is the number of bytes sent. options are
		ignored. pMutex, if specified, will lock before the data is sent and unlock
		immediately after. If bReleaseMutexWhenDone is false, the mutex will not be
		unlocked.
*/
size_t skynet::send_msg (int type, void *msg, size_t msgsize, int options)
{
	return send_msg (type, msg, msgsize, options, NULL, false, 0);
}

size_t skynet::send_msg (int type, void *msg, size_t msgsize, int options,
												 pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone, int numMutices)
{
	for(int i=0; i<numMutices; i++)
		DGClockMutex(ppMutex[i]);

	int retval;
	string groupName;
	makeGroupName(&groupName, (sn_msg)type);

	int numsent = SP_multicast(m_sendMailbox, FIFO_MESS | SELF_DISCARD, groupName.c_str(), 0, msgsize, (const char*)msg);
	if(numsent == (int)msgsize)
		retval = msgsize;
	else
	{
		cerr << "skynet::send_msg failed: "<< endl;
		SP_error(numsent);
		retval = 0;
		if(numsent == CONNECTION_CLOSED)
			exit(-1);
	}

	if(bReleaseMutexWhenDone)
		for(int i=0; i<numMutices; i++)
			DGCunlockMutex(ppMutex[i]);

	return retval;
}

size_t skynet::send_msg (int type, const scatter* msgs, int options)
{
	return send_msg (type, msgs, options, NULL, false, 0);
}

size_t skynet::send_msg (int type, const scatter* msgs, int options,
			 pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone, int numMutices)
{
	for(int i=0; i<numMutices; i++)
		DGClockMutex(ppMutex[i]);

	int retval;
	string groupName;
	makeGroupName(&groupName, (sn_msg)type);

	int numsent = SP_scat_multicast(m_sendMailbox, FIFO_MESS | SELF_DISCARD, groupName.c_str(), 0, msgs);
	int msgsize = 0;
	for(int i=0; i<msgs->num_elements; i++) {
	  msgsize+=msgs->elements[i].len;
	}

	if(numsent == msgsize)
		retval = msgsize;
	else
	{
		cerr << "skynet::send_msg failed: "<< endl;
		SP_error(numsent);
		retval = 0;
		if(numsent == CONNECTION_CLOSED)
			exit(-1);
	}

	if(bReleaseMutexWhenDone)
		for(int i=0; i<numMutices; i++)
			DGCunlockMutex(ppMutex[i]);

	return retval;
}

void skynet::makeGroupName(string* pGroupName, sn_msg type)
{
	ostringstream groupNameStream;
	groupNameStream << m_key << '_' << type;
	*pGroupName = groupNameStream.str();
	if(strlen(pGroupName->c_str()) > MAX_GROUP_NAME)
	{
		cerr << "skynet key too long. why's it so fucking long?????" << endl;
		cerr << m_key << "?!?!?!?!?!?!?!?!?!?" << endl;
		exit(-1);
	}
}

void skynet::I_am_here(void)
{
	int sendsock = get_send_sock(SNmodlist);

	SSkynetID id;
	memset(id.host, 0, MAX_SIZE_HOSTNAME);
	if(	gethostname(id.host, MAX_SIZE_HOSTNAME) != 0 )
	{
		cerr << "skynet::I_am_here() failed trying to get the hostname" << endl;
		return;
	}
	id.name = m_name;
	id.unique = m_sendMailbox;
	while(true)
	{
		DGCusleep(I_AM_HERE_PERIOD);

		send_msg(sendsock, &id, sizeof(id));
	}
}
