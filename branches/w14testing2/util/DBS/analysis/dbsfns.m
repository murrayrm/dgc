## fake fn
1;

function y = logi(x)
  y = 1 ./ (1 + exp(-x));
end

function doplots(data, start, finish, norm)
  figure(1);
  plotarea(data, start, finish, norm);
  figure(2);
  plotmax(data, start, finish, norm);
  figure(3);
  plotbump(data, start, finish, norm);
end

function plotarea1(mat, start, finish, timestart, timeend)
  plotmat = [];
  for i = 1:size(mat,1)
    if (mat(i,1)./1000000 >= timestart & mat(i,1)./1000000 <= timeend)
      plotmat = [plotmat; mat(i,:)];
    end
  end
  format long;
  [plotmat(1,1), plotmat(size(plotmat,1),1)] ./ 1000000
  times = (plotmat(:,1) - plotmat(1,1)) ./ 1000000;
  temp = sum( plotmat(:,start:finish)' );
  plot(times, temp);
end

function [r, beta, x1, x2, y1, y2] = fitme(data)
##  x1 = data(1:2:621, 8:367);
##  x2 = data(2:2:621, 8:367);
##  y1 = data(1:2:621, 374);
##  y2 = data(2:2:621, 374);

##  x1 = data(1:311, [168:187, 328:347]);
##  x2 = data(312:621, [168:187, 328:347]);
##  y1 = data(1:311, 374);
##  y2 = data(312:621, 374);

##  x1 = data(1:2:26, [168:187, 328:347]);
##  x2 = data(2:2:26, [168:187, 328:347]);
##  y1 = data(1:2:26, 374);
##  y2 = data(2:2:26, 374);
##  x1 = [sum(x1(:,1:20),2) sum(x1(:,21:40),2)];
##  x2 = [sum(x2(:,1:20),2) sum(x2(:,21:40),2)];

  ## when we sum before hand in load_dbs
  x1 = data(1:2:26, 3:6);
  x2 = data(2:2:26, 3:6);
  y1 = data(1:2:26, 7);
  y2 = data(2:2:26, 7);

#  x1 = [ones(size(x1,1),1), x1];
#  x2 = [ones(size(x2,1),1), x2];

  xtot = [x1; x2]
  ytot = [y1; y2]

  [beta, sigma, r] = ols(ytot, xtot);
  beta
  y1out = x1 * beta;
  y2out = x2 * beta;
  ytotout = xtot * beta;
  y1out = min(1, y1out);
  y2out = min(1, y2out);
  ytotout = min(1, ytotout);

##  xtest = linspace(0,10,100);
##  ytest = xtest * beta;

##  plot(y1, y1out, 'ro',
##       y2, y2out, 'bo');
  plot(ytot, ytotout, 'ro');
##  plot(xtest, ytest, 'b-',
##       x1, y1, 'ro',
##       x2, y2, 'bo');

#  r = [corrcoef(x2, y2)];
#  r = [corrcoef(x2, y2); 0; corrcoef(y1, y1out); 0; corrcoef(y2, y2out)];
  r = [corrcoef(x2, y2); 0; corrcoef(ytot, ytotout)];
end

function plotmandata(manual_data, start, finish)
  temp = sum(manual_data(:,start:finish), 2) .* (1.5658 .* (.6 ./ (1 + exp(- (manual_data(:,2) - 8) ./ 2)) + .2));
  plot(temp, manual_data(:,374), 'bo');
end

function plotarea(data, start, finish, norm)
  ## assume data is in a struct
  N = size(data, 2);
  
  first = 0;
  
  for i = 1:N
    temp = sum( data{i}(:,start:finish)' );
    if (first == 0)
      hold off;
      first = 1;
    else
      hold on;
    end
    plot(temp);
  end
  hold off;
  
end

function plotmax(data, start, finish, norm)
  ## assume data is in a struct
  N = size(data, 2);
  
  temp = [];
  for i = 1:N
    temp = [temp max(sum( data{i}(:,start:finish)' ))];
  end
  plot((5:5:40) / 2.23, temp / norm);
  title('max of summed area');

  t = 1:18;
  hold on;
  ##  plot(t, atan(.25*(t - 7)) * .35 + .4);
  plot(t, .6 * logi((t-8) / 2) + .2);
  hold off;
end

function plotbump(data, start, finish, norm)
  ## assume data is in a struct
  N = size(data, 2);
  
  temp = [];
  for i = 1:N
    temp = [temp (max(sum( data{i}(:,start:finish)' ))./norm) / (1.5658 * (.6 * logi((i*5/2.23-8) / 2) + .2))];
  end
  plot((5:5:40) / 2.23, temp);
  title('bumpiness based on max');
end

function plotstddev(data, start, finish, norm)
  ## assume data is in a struct
  N = size(data, 2);
  
  temp = [];
  for i = 1:N
    temp = [temp std(sum( data{i}(:,start:finish)' ))];
  end
  plot(temp / norm);
  hold off;
  title('std dev of summed area');
end
