#ifndef RAID_HH
#define RAID_HH

#include <iostream>
#include <string>

#define DEFAULT_DEBUG      0

#define RAID_INFO(_) \
_(fncall, 0) \
_(normal, 0) \
_(error, 0)

using namespace std;


#define RAID_NAMES(enum_name, default_val) enum_name,
namespace rmsg
{
  enum types
    {
      RAID_INFO(RAID_NAMES)
      LAST
    };
}

/** used in the raid contructor to set all elements of the m_raid_state array
 * to their default values */
#define SET_RAID_DEFAULTS(enum_name, default_val) m_raid_state[rmsg::enum_name]=default_val;

#define bugme BUGME(__FILE__, __LINE__)

#define pd(message, rmsg_enum_val, required_level) \
printDebug(message, rmsg_enum_val, required_level, __FILE__, __LINE__)

/**
 * The raid class is used for debugging purposes.  Classes wishing to use
 * raid debugging capabilities should derive from it, probably by using
 * `virtual public raid'.
 */
class raid
{
public:
  raid();
  ~raid();

  /** Prints a message only if the required element of m_raid_state is greater than 
   * `required_level'
   *
   * @param message The message which will be printed
   * @param rmsg_enum_val The index of the associated debug type in m_raid_state.
   *                      Should be specified as rmsg::type 
   * @param required_level The message will be printed only if the current value
   *                      in m_raid_state[rmsg_enum_val] >= required_level 
   * @param file The filename that contains the call to printDebug.
   * @param line The line number of the call to printDebug.  */
  void BUGME(string file, int line) {cout << "[" << file << ":" << line << "]" << endl;}

  void printDebug(string message, int rmsg_enum_val, double required_level, string file = "___", int line = 0);

private:
  /** an array which stores all current debug levels */
  double* m_raid_state;
};

#endif  // RAID_HH
