#include "herman.hh"

using namespace std;

/* herman one arg constructor */
herman::herman(char* rddf_file)
  : RDDF(rddf_file)
{
  pd("herman::herman start", rmsg::fncall, 1);
  hermanInit();  
  pd("herman::herman finish", rmsg::fncall, 1);
}



/* herman no arg constructor */
herman::herman()
  : RDDF(RDDF_FILE)
{
  pd("herman::herman start", rmsg::fncall, 1);
  hermanInit();  
  pd("herman::herman finish", rmsg::fncall, 1);
}



/* Destructor */
herman::~herman()
{
  pd("herman::~herman start", rmsg::fncall, 1);
  delete m_herman_state;
  pd("herman::~herman finish", rmsg::fncall, 1);
}



/* constructor helper */
void herman::hermanInit()
{
  pd("herman::hermanInit start", rmsg::fncall, 1);
  m_herman_state = new hermanState;
  m_herman_state->cur_waypoint = 0;
  m_herman_state->cur_dist_from_start = 0;
  pd("herman::hermanInit finish", rmsg::fncall, 1);
}



void herman::updateState(const VehicleState & new_state)
{
  pd("herman::updateState start", rmsg::fncall, 1);
  /** First, attempt to move FORWARD along the trackline, one point
   * at a time, until we stop making progress. */
  int cur_wpt = m_herman_state->cur_waypoint;

  double cur_dist_from_start;
  double this_dist;

  double ratio = 2;
  /** first step back along the centerline, if necessary */
  cout << "herman::updateState stepping, starting at wpt " << cur_wpt 
       << " ratio " << ratio << endl;
  while (true)
    {
      this_dist = getDistToCorridorSegment(cur_wpt, new_state.ne_coord(),
					   cur_dist_from_start, ratio);
      cout << "  (back) " << this_dist << " = getDist(" << cur_wpt << ")"
	   << "  ratio returned is " << ratio << endl;
      if (ratio == 0 || ratio == -1)
	{
	  cur_wpt--;
	  cout << "herman::updateState stepping backward, now at wpt " << cur_wpt
	       << " ratio " << ratio << endl;
	}
      else
	break;
      if (cur_wpt < 0)
	{
	  cur_wpt = 0;
	  ratio = 0;
	  break;
	}
    }

  /** then, step forward along the trackline, if necessary */
  while (true)
    {
      this_dist = getDistToCorridorSegment(cur_wpt, new_state.ne_coord(),
					   cur_dist_from_start, ratio);
      cout << "  (fwd)  " << this_dist << " = getDist(" << cur_wpt << ")"
	   << "  ratio returned is " << ratio << endl;
      if (ratio > .99999 || ratio == -1)
	{
	  cur_wpt++;
	  cout << "herman::updateState stepping forward, now at wpt " << cur_wpt
	       << " ratio " << ratio << endl;
	}
      else
	break;
      if (cur_wpt >= numTargetPoints - 1)
	{
	  cur_wpt = numTargetPoints - 1;
	  ratio = 1;
	  break;
	}
    }
  cout << "herman::updateState stepping finished at wpt " << cur_wpt
       << " ratio " << ratio << endl;


  /** Second, if that attempt failed, then do an exhaustive search
   * of all RDDF corridors and use the best one.
   * We'll define a failed attempt by being more than some number
   * of rddf path widths away from the centerline of the corridor */
  if (this_dist > targetPoints[cur_wpt].radius * MAX_DIST_FROM_PATH_ASPECT_RATIO)
    {
      // crap... gotta do an exhaustive search
      ostringstream oss;
      oss << "Too far from corridor centerline (" << this_dist
	  << "m > " << targetPoints[cur_wpt].radius * MAX_DIST_FROM_PATH_ASPECT_RATIO
	  << " m), doing exhaustive search... ";

      cur_wpt = numTargetPoints - 1;
      cur_dist_from_start = 0;
      int best_wpt_so_far = cur_wpt;
      double best_dist_so_far = DBL_MAX;
      double best_dist_from_start = 0;
      double junk;

      while (cur_wpt >= 0)
	{
	  this_dist = getDistToCorridorSegment(cur_wpt, new_state.ne_coord(),
					       cur_dist_from_start, junk);
	  
	  if (this_dist < best_dist_so_far)
	    {
	      // we found a new best
	      best_dist_so_far = this_dist;
	      best_wpt_so_far = cur_wpt;
	      best_dist_from_start = cur_dist_from_start;
	    }
	    --cur_wpt;

	}

      oss << "new best is wpt " << best_wpt_so_far << " at dist of "
	  << best_dist_so_far << "m.";
      pd(oss.str(), rmsg::error, 0);

      cur_wpt = best_wpt_so_far;
      cur_dist_from_start = best_dist_from_start;
    }

  m_herman_state->cur_waypoint = cur_wpt;
  m_herman_state->cur_dist_from_start = cur_dist_from_start;
  pd("herman::updateState finish", rmsg::fncall, 1);
}



/* returns current point */
int herman::getCurrentPoint()
{
  pd("herman::getCurrentPoint", rmsg::fncall, 1);
  return m_herman_state->cur_waypoint;
}

