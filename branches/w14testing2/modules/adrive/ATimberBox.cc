#include "ATimberBox.hh"

AtimberBox::AtimberBox(int skynetKey)
  : CSkynetContainer(SNadrive, skynetKey) {
  
  timberMsgSocket = m_skynet.get_send_sock(SNguiToTimberMsg);
}


AtimberBox::~AtimberBox() {

}


 void AtimberBox::timberStart() {
   CTimber::GUI_MSG_TYPES msg = CTimber::START;
   m_skynet.send_msg(timberMsgSocket, &msg, sizeof(CTimber::GUI_MSG_TYPES));
 }


 void AtimberBox::timberStop() {
   CTimber::GUI_MSG_TYPES msg = CTimber::STOP;
   m_skynet.send_msg(timberMsgSocket, &msg, sizeof(CTimber::GUI_MSG_TYPES));
 }


AdriveEventLogger::AdriveEventLogger(char * logging_path){
    
  logfile.open (logging_path, fstream::out | fstream::app);
}

AdriveEventLogger::~ AdriveEventLogger(){
  logfile.close();  
}

int AdriveEventLogger::operator<<(string input_string){
  DGCgettime(current_time);
  logfile << current_time << '\t' << input_string << endl;
  return 1;
}


AdriveEventLogger event("adrive.event");
