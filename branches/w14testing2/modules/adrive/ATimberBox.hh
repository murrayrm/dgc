#ifndef __ATIMBERBOX_HH__
#define __ATIMBERBOX_HH__

#include "SkynetContainer.h"
#include "CTimber.hh"

class AtimberBox : virtual public CSkynetContainer {
public:
  AtimberBox(int skynetKey);
  ~AtimberBox();


  void timberStart();
  void timberStop();

private:
  int timberMsgSocket;
  
};

class AdriveEventLogger {

public:
  AdriveEventLogger(char * pathname);
  ~AdriveEventLogger();

  int operator<<(string input_string);


private:
  unsigned long long current_time;
  fstream logfile;

};

extern AdriveEventLogger event;
#endif //__ATIMBERBOX_HH__
