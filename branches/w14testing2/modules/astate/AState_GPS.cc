/* AState_GPS.cc - Functions for dealing with the GPS
 *
 * JML 09 Jun 05
 *
 * */

#include "AState.hh"

void AState::gpsInit()
{

  unsigned long long rawGpsTime;
  //If we are in replay mode, read in first gps reading.
  if (_astateOpts.useReplay == 1) {
    if (_metaState.imuEnabled == 1) {
      // Line up times on gps and imu logs.
      gpsLogStart = imuLogStart;
    } else {
      rawGPS gpsIn;
      gpsReplayStream.read((char *)&gpsIn, sizeof(rawGPS));
      
      rawGpsTime = gpsIn.time;      
      gpsLogStart = rawGpsTime;
    }
    DGClockMutex(&m_MetaStateMutex);
    _metaState.gpsEnabled = 1;
    DGCunlockMutex(&m_MetaStateMutex);
  } else {
    DGClockMutex(&m_MetaStateMutex);
    _metaState.gpsEnabled = gps_open(GPSPORT);
    DGCunlockMutex(&m_MetaStateMutex);
    if(_metaState.gpsEnabled == -1)
      {
	cerr << "AState::gpsInit(): failed to open GPS" << endl;
      }
  }
}


void AState::gpsThread()
{

  /* Definition moved to AState.hh for Sparrow access
 
    int gpsValid; 

  */

  int lastMode = -1;

  char *gpsReadBuffer;

  if ((gpsReadBuffer = (char *) malloc(2000)) == NULL)
    {
      cerr << "AState::gpsThread(): memory allocation error" << endl;
    }

  //Temporary time variable
  unsigned long long nowTime;
  unsigned long long rawGpsTime;

  //Variables for reading in raw data
  rawGPS gpsIn;
  rawGPS gpsOut;
  gpsDataWrapper wrappedGpsData;

  spGPSDisable = 0;
	
  while (!quitPressed) {
    if (_astateOpts.useReplay == 1) {
      if (!gpsReplayStream) {
	return;
      }
      //If in replay mode read in from stream then wait
      //until we've reached time we would have read point

      gpsReplayStream.read((char *)&gpsIn, sizeof(rawGPS));
      rawGpsTime = gpsIn.time - gpsLogStart + startTime;

      if (_astateOpts.fastMode == 0) {
	DGCgettime(nowTime);
	while (rawGpsTime > nowTime) {
	  usleep(10);
	  DGCgettime(nowTime);
	}
      }
      memcpy(&(wrappedGpsData.data), &(gpsIn.data), sizeof(gpsData));
    } else {
      //Otherwise do an actual read.
      gps_read(gpsReadBuffer, 1000);  //Need to determine if this is blocking...
      DGCgettime(rawGpsTime); // Time stamp as soon as data read.
      if (gps_msg_type(gpsReadBuffer) == GPS_MSG_PVT) {
	if (wrappedGpsData.update_gps_data(gpsReadBuffer) == -1) {
	  //Ignored corrupt PVT
	  continue;
	}
	if (_astateOpts.logRaw == 1) {
	  gpsOut.time = rawGpsTime;
	  memcpy(&(gpsOut.data), &(wrappedGpsData.data), sizeof(gpsData));
	  gpsLogStream.write((char *)&gpsOut, sizeof(rawGPS));
	}
      } else {
	continue;
      }
    }

    gpsValid = (wrappedGpsData.data.nav_mode & NAV_VALID) && 1;

    if (_astateOpts.useSparrow && wrappedGpsData.data.nav_mode != lastMode) {
      SparrowHawk().set_string("gpsMode", gps_mode(wrappedGpsData.data.nav_mode));
      lastMode = wrappedGpsData.data.nav_mode;
    }

    if (spGPSDisable == 1) {
      continue;
    }

    DGClockMutex(&m_HeartbeatMutex);
    _heartbeat.gps = true;
    if(gpsValid) {
      _heartbeat.gpsValid = true;
    }
    DGCunlockMutex(&m_HeartbeatMutex);
    
    if (gpsValid) {


      DGClockMutex(&m_GPSDataMutex);

      if (((gpsBufferReadInd + 1) % GPS_BUFFER_SIZE) == (gpsBufferLastInd % GPS_BUFFER_SIZE)) {
	gpsBufferFree.bCond = false;

	DGCunlockMutex(&m_GPSDataMutex);

	DGCWaitForConditionTrue(gpsBufferFree);
      

	DGClockMutex(&m_GPSDataMutex);
      }

      ++gpsBufferReadInd;
      
      memcpy(&(gpsIn.data),&(wrappedGpsData.data), sizeof(gpsData));
      gpsIn.time = rawGpsTime;

      memcpy(&(gpsBuffer[gpsBufferReadInd % GPS_BUFFER_SIZE]), &gpsIn, sizeof(rawGPS));
      
      DGCunlockMutex(&m_GPSDataMutex);    

      DGCSetConditionTrue(newData);
    }
    
    // Occasionally flush the buffer
    if(gpsBufferReadInd % 100 == 1 && _astateOpts.logRaw) {
      gpsLogStream.flush();
    }
  }
  free(gpsReadBuffer);
}



