/* AState_IMU.cc - new vehicle state estimator
 *
 * JML 09 Jun 05
 *
 * */

#include "AState.hh"

void AState::imuInit()
{
  if (_astateOpts.useReplay == 1) {
    rawIMU imuIn;
    rawIMUOld imuInOld;

    if (_astateOpts.oldLog) {
      imuReplayStream.read((char*)&imuInOld, sizeof(rawIMUOld));

      DGClockMutex(&m_IMUDataMutex);
      imuLogStart = imuInOld.time;
      DGCunlockMutex(&m_IMUDataMutex);
    } else {
      imuReplayStream.read((char*)&imuIn, sizeof(rawIMU));

      DGClockMutex(&m_IMUDataMutex);
      imuLogStart = imuIn.time;
      DGCunlockMutex(&m_IMUDataMutex);
    }



    DGClockMutex(&m_MetaStateMutex);
    _metaState.imuEnabled = 1;
    DGCunlockMutex(&m_MetaStateMutex);
  } else {
    DGClockMutex(&m_MetaStateMutex);
    _metaState.imuEnabled = IMU_open();
    if(_metaState.imuEnabled == -1) {
      cerr << "AState::imuInit(): failed to open IMU" << endl;
    }
    DGCunlockMutex(&m_MetaStateMutex);
  }
}

void AState::imuThread()
{
  unsigned long long nowTime;
  unsigned long long rawImuTime;

  rawIMU imuIn;
  rawIMU imuOut;
  IMU_DATA imuData;
  IMU_DEBUG imuDebug;
  rawIMUOld imuInOld;

  imuDebug.status_word = 0;
  imuDebug.mode_word = 0;
  imuDebug.mux_id = -1;
  imuDebug.value = 0;

  spIMUDisable = 0;

  int readError = 0;

  while (!quitPressed) {
    if (_astateOpts.useReplay == 1) {
      if (!imuReplayStream) {
	quitPressed = 1;
	return;
      }
      if (_astateOpts.oldLog) {
	imuReplayStream.read((char*)&imuInOld, sizeof(rawIMUOld));
	memcpy(&imuData, &(imuInOld.data), sizeof(IMU_DATA));
	rawImuTime = imuInOld.time - imuLogStart + startTime;
      } else {
	imuReplayStream.read((char*)&imuIn, sizeof(rawIMU));
	memcpy(&imuData, &(imuIn.data), sizeof(IMU_DATA));
	memcpy(&imuDebug, &(imuIn.debug), sizeof(IMU_DEBUG));
	rawImuTime = imuIn.time - imuLogStart + startTime;
      }

      //Wait until time has caught up with raw input
      if (_astateOpts.fastMode == 0) {
	DGCgettime(nowTime);
	while( rawImuTime > nowTime ) {
	  usleep(1);
	  DGCgettime(nowTime);
	}
      }
    } else {
      readError = IMU_read(&imuData, &imuDebug); // Need to add new code for IMU_read!
      DGCgettime(rawImuTime); // time stamp as soon as data read.
    }
    if (_astateOpts.logRaw == 1) {
      imuOut.time = rawImuTime;
     
      memcpy(&(imuOut.data), &imuData, sizeof(IMU_DATA));
      memcpy(&(imuOut.debug), &imuDebug, sizeof(IMU_DEBUG));
      
      imuLogStream.write((char*)&imuOut, sizeof(rawIMU));
    }

    if (spIMUDisable == 1) {
      continue;
    }


    gyroError = ((imuDebug.status_word >> 2) & 1);
    accelError = ((imuDebug.status_word >> 3) & 1);
    powerError = ((imuDebug.status_word >> 7) & 1);

    //If any of these errors surface, need to restart IMU so might as well let this thread end.
    if (gyroError || accelError || powerError || readError == -1) {
      _metaState.imuEnabled = -1;
      return;
    }

    DGClockMutex(&m_HeartbeatMutex);
    _heartbeat.imu = true;
    DGCunlockMutex(&m_HeartbeatMutex);

    DGClockMutex(&m_IMUDataMutex);

    if (((imuBufferReadInd + 1) % IMU_BUFFER_SIZE) == (imuBufferLastInd % IMU_BUFFER_SIZE)) {
      imuBufferFree.bCond = false;

      DGCunlockMutex(&m_IMUDataMutex);

      DGCWaitForConditionTrue(imuBufferFree);


      DGClockMutex(&m_IMUDataMutex);
    }

    ++imuBufferReadInd;

    memcpy(&(imuIn.data), &imuData, sizeof(IMU_DATA));
    memcpy(&(imuIn.debug), &imuDebug, sizeof(IMU_DEBUG));

    imuIn.time = rawImuTime;


    memcpy(&imuBuffer[imuBufferReadInd % IMU_BUFFER_SIZE], &imuIn, sizeof(rawIMU));

    DGCunlockMutex(&m_IMUDataMutex);

    DGCSetConditionTrue(newData);

    if(imuBufferReadInd % 1500 == 1 && _astateOpts.logRaw) {
      imuLogStream.flush();
    }
  }
}
