#include "AState.hh"

//MetaState is solely responsible for Keeping track of which sensors are enabled / active and restarting them
//If necessary.  This should happen on a level almost entirely outside of AState_Update.cc

using namespace sc_interface;

void AState::metaStateThread() {

  _heartbeat.gps = false;
  _heartbeat.gpsValid = false;
  _heartbeat.nov = false;
  _heartbeat.novValid = false;
  _heartbeat.imu = false;
  _heartbeat.obd = false;

  // give time for the heartbeats to start
  sleep(1);

  while(!quitPressed) {

    DGClockMutex(&m_MetaStateMutex);        
    DGClockMutex(&m_HeartbeatMutex);
    if(_metaState.gpsEnabled) {

      if (_heartbeat.gps) {
	_metaState.gpsActive = 1;
      } else {
	_metaState.gpsActive = 0;
      }

      if(_heartbeat.gpsValid) {
	_metaState.gpsPvtValid = 1;
      } else {
	_metaState.gpsPvtValid = 0;
      }

    }

    if(_metaState.novEnabled) {

      if (_heartbeat.nov) {
	_metaState.novActive = 1;
      } else {
	_metaState.novActive = 0;
      }

      if(_heartbeat.novValid) {
	_metaState.novPvtValid = 1;
      } else {
	_metaState.novPvtValid = 0;
      }

    }

    if(_metaState.imuEnabled) {

      if(_heartbeat.imu) {
	_metaState.imuActive = 1;
      } else {
	_metaState.imuActive = 0;
      }

    }

    if(_metaState.obdActive) {

      if(_heartbeat.obd) {
	_metaState.obdActive = 1;
      } else {
	_metaState.obdActive = 0;
      }

    }

    _heartbeat.gps = false;
    _heartbeat.gpsValid = false;
    _heartbeat.nov = false;
    _heartbeat.novValid = false;
    _heartbeat.imu = false;
    _heartbeat.obd = false;

    DGCunlockMutex(&m_HeartbeatMutex);
    DGCunlockMutex(&m_MetaStateMutex);
    

    
    // Mode setting.
    if(stateMode != PREINIT && stateMode != INIT) {

      if (_metaState.imuActive == 0) {
	stateMode = FALLBACK;
      }

      DGClockMutex(&m_jumpMutex);
      if (stateJump == JUMP_REQUESTED) {
	scMessage((int)need_to_stop_GPS_reacquired);	
	SparrowHawk().log("Requested stop for GPS reacquire");
      }

      if (stateJump == JUMP_DEFERRED) {
	scMessage((int)move_without_clear);
      }

      if (_metaState.gpsActive == 0 && _metaState.novActive == 0 && 
	  (stateJump == JUMP_OK || stateJump == JUMP_REQUESTED)) {
	stateJump = JUMP_DEFERRED;
      }

      if (stateJump == JUMP_DONE) {
	scMessage((int)move_with_clear);
	stateJump = JUMP_NONE;
      }
      DGCunlockMutex(&m_jumpMutex);
      
    }
    sleep(1);
  }
}

void AState::allowJump() {
    DGClockMutex(&m_jumpMutex);    
    stateJump = JUMP_OK;
    DGCunlockMutex(&m_jumpMutex);
}

void AState::getJumpMessage() {
  
  superconAstateCmd jumpCommand;

  while (!quitPressed) {

    if (m_skynet.get_msg(superconSock, &jumpCommand, sizeof(superconAstateCmd), 0) != sizeof(superconAstateCmd)) {
      cerr << "Didn't receive right size superconAStateCmd" << endl;
    }
    DGClockMutex(&m_jumpMutex);    
    if (jumpCommand.commandType == ok_to_add_gps && stateJump == JUMP_REQUESTED) {

      stateJump = JUMP_OK;

    }  
    DGCunlockMutex(&m_jumpMutex);
  }
}
