#ifndef STRATEGY_INTERFACE_HH
#define STRATEGY_INTERFACE_HH

#include "SuperCon.hh"
#include "SuperConLog.hh"
#include "SuperConClient.hh"
#include "bool_counter.hh"
#include "sc_specs.h"

#include "interface_superCon_adrive.hh"
#include "interface_superCon_trajF.hh"
#include "interface_superCon_astate.hh"
#include "interface_superCon_superCon.hh"
#include "trajF_speedCap_cmd.hh"

#include "AliceConstants.h"
#include "CMapPlus.hh"
#include "MapdeltaTalker.h"
#include "GlobalConstants.h"
#include "SuperConHelpers.hh"
#include "sn_types.h"
#include "MapConstants.h"

#include <pthread.h>
#include <math.h>
#include <map>
#include <string.h>

#define MAX_SPARROW_DISPLAY_STATUS_STRING_LENGTH 72
#define SOCKET_NOT_REGISTERED -1

namespace strategyIface {
#define X_COORD 0
#define Y_COORD 1

#define NORTHING_COORD 0
#define EASTING_COORD 1
}


using namespace std;
using namespace sc_interface;
using namespace strategyIface;

//Forward declaration:
class CDiagnostic;

class CStrategyInterface : public CMapdeltaTalker {

/** VARIABLES **/
private:

  /* SKYNET SOCKET INITIALISATION */
  //STL map to hold the numbers of the sockets used to send skynet messages
  //of types specified in the string element of the map, the integer
  //elements of the map store the socket# for the message type once intialised
  map<sn_msg, int> StrategySNsocketList; //map
  map<sn_msg, int>::iterator msg_itr; //iterator for use by sendSkynetMsg(...)

  /* SUPERCON CMAP DECLARATIONS */
  //CMap instance used by superCon to make updates to the superCon
  //layer in the (complete) map output by FusionMapper
  CMap superConMap;

  //int to hold the layer ID for the superCon map layer - set
  //when the superCon layer is added to the map
  int layerID_superCon;

  //Mutex used to lock the superCon CMap when accessing it
  pthread_mutex_t superConMapMutex;
  
  //Map delta pointer
  CDeltaList* deltaPtr;

  /* OTHER VARIABLES REQUIRED */
  //Pointer to the current copy of the SC-state table upon which the rules are
  //evaluated - this allows interface methods to have access to the current SCstate
  SCstate* m_pSCstate;
  SCdiagnostic* m_pSCDiagnostic;

  //pointer to the CDiagnostic instance (to access public methods of the class)
  CDiagnostic* m_pCDiagnostic;

  //boolean to track whether superCon was started with the --silent option
  //(referenced by the sendSkynetMsg(...) method to determine whether to
  //actually send the message over skynet
  bool silentOptionSelected;
  

/** METHODS **/
public:

  /** CONSTRUCTORS **/
  CStrategyInterface( const int skynetKey, SCstate* p_SCstate, SCdiagnostic* p_SCDiagnostic, CDiagnostic* p_CDiagnostic, bool usrSpef_silentOptionSelected );

  /** DESTRUCTOR **/
  ~CStrategyInterface();


  /** MUTATOR METHODS **/
  void updateVehPosInSuperconMap(const SCstate &m_SCstate);


  /** COMMON-STAGE METHODS **/

  //Send superCon e-stop signal to adrive (pause OR run ONLY) - arguments
  //are specified in interface_superCon_adrive.hh
  //Description: Method checks the current status of the superCon
  //e-stop, IF the current e-stop status = eStopType then, no
  //further action is taken, IF the current e-stop status != eStopType
  //then a message is sent to adrive to change the superCon e-stop
  //status, once the messages have been sent the method returns
  //(cannot wait for confirmation that the status has updated as the
  //strategies do not have access to the dynamic copy of the SCstate table)
  void stage_superConEstop( const adriveEstpCmd eStopType );

  //Send a command to adrive to change gear - the gear to be changed into
  //is passed as a char in the argument, argument options are detailed in
  //interface 
  void stage_changeGear( const adriveTransCmd desiredGear );

  //Send a command to TrajFollower to change mode - options are:
  // tf_forwards
  // tf_reverse
  // IF mode = tf_reverse, then there is an additional DOUBLE argument = distance in 
  // meters to reverse
  void stage_changeTFmode( const trajFmode desiredMode, const double distanceToReverse_meters );

  //Add a superCon obstacle along (the current position of) Alice's front bumper:
  //Description: This method takes the current position of Alice (Northing, easting yaw, ... from p_SCstate)
  //it takes NUM_POINTS_TO_EVALUATE_ALONG_BUMPER evenly spaced
  //points along the front bumper and sends a map delta for each point 
  //with a value = SUPERCON_OBSTACLE_SPEED_MS
  void stage_addForwardUnseenObstacle();
  
  //Modify the superCon MAX/MIN speed-cap [in METERS/SECOND] in trajFollower, 
  //note that IF a new MAX/MIN speed-cap sent, then it replaces any previous superCon 
  //speed-caps (although not others) regardless of whether the new speed-cap 
  //is above or below the old one.
  //Argument options detailed in the trajFollower interface header file
  void stage_modifyTrajFSpeedCap( const speedCapAction commandType, const double speedCap );

  //Clear the *FusionMapper* cost-map, AND the cost-map used by the *plannerModule*
  //this method would be used after a jump in astate due to re-acquisition of GPS
  //prior to un-pausing Alice and allowing her to proceed to ensure that the only
  //obstacles that are painted into the map are ones that are actually in Alice's
  //field of view *now* and not ones that have been dragged into her map as a 
  //result of the state jump
  /*** HOW WILL FM & PLN CONFIRM THAT THIS HAS HAPPENED? - WHERE SHOULD THE
   *** COFIRMATION TAKE PLACE? scMessages? - but from whom?***/
  void stage_clearFMandPLNcostMaps();  //INCOMPLETE - NOT READY FOR USE

  //Send a skynet message to astate to signal that an action/activity has
  //taken place - using the enums & structures defined in the superCon - astate
  //interface header file
  void stage_signalToAstate( const SC_astateCmdType commandType );


private:

  /** HELPER METHODS **/

  //Wrapper for the skynet::send_msg(...) method which references a boolean passed
  //into the CStrategyInterface class on construction which is set by the user
  //appending the --silent option when starting superCon
  void sendSkynetMsg( sn_msg msg_type, void* msg_pointer, size_t msg_size );

};

#endif
