//SUPERCON STRATEGY TITLE: Wheel-Spin - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyWheelSpin.hh"
#include "StrategyHelpers.hh"

bool CStrategyWheelSpin::EstopPausedNOTsuperCon_trans_condition( bool checkBool, const SCdiagnostic *pdiag, const char* StrategySpecificStr ) {

  if( checkBool == false ) {
    checkBool = trans_EstopPausedNOTsuperCon( (*pdiag), StrategySpecificStr );
  }  
  return checkBool;
}

void CStrategyWheelSpin::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE
  currentStageName = wheelspin_stage_names_asString( wheelspin_stage_names(stgItr.nextStage()) );    

  switch( stgItr.nextStage() ) {

  case s_wheelspin::estop_pause:
    //Wheel-spin detected - superCon e-stop pause the vehicle immediately to
    //stop Alice digging herself in.
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = EstopPausedNOTsuperCon_trans_condition( skipStageOps, m_pdiag, strprintf( "WheelSpin: %s", currentStageName.c_str() ) );

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
            
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Wheelspin: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_wheelspin::trans_unseenobstacle: 
    //wait until e-stop pause applied, and vehicle && wheels are stationary
    //and then transition to UnseenObstacle
    {
      
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      skipStageOps = EstopPausedNOTsuperCon_trans_condition( skipStageOps, m_pdiag, strprintf( "WheelSpin: %s", currentStageName.c_str() ) );

      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      if( m_pdiag->superConPaused == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "WheelSpin - Alice not paused - will poll");
	checkForPersistLoop( this, persist_loop_estop, stgItr.currentStageCount() );	
      }
      
      if( m_pdiag->aliceStationary == false ) {
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "WheelSpin - Alice not stationary - will poll");	
      }

      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT*/
      if( skipStageOps == false ) {
	
	transitionStrategy( StrategyLoneRanger, "WheelSpin - detected wheel-sping, stopped Alice -> UnseenObstacle to mark the area" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "Wheelspin: stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;

    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyWheelSpin::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyWheelSpin::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: WheelSpin - default stage reached!");
    }

  }
  
}


void CStrategyWheelSpin::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "WheelSpin - Exiting");
}

void CStrategyWheelSpin::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset();
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "WheelSpin - Entering");
}
