//TITLE: SuperCon SINGLETON Logging Class SOURCE FILE
//DESCRIPTION: Singleton class (just like in Highlander, 'there can only be one!')
//that contains all logging methods (for BOTH SPARROWHAWK & file logging using
//TIMBER).  Note that the methods contained wtihin this class are the ONLY
//way through which any part of superCon can log (to either the SparrowHawk
//status tab, OR a file (using Timber)

//WARNING: This class ASSUMES that DIFFERENT threads will **NEVER** want 
//to log to the SAME FILE
#warning CSuperConLog ASSUMES that DIFFERENT threads will **NEVER** want to log to the same file! - make sure this is NOT violated

#include "SuperConLog.hh"
#include "SuperCon.hh"

/** Methods to return a pointer to the SOLE instance of CSuperConLog (obtained through
 ** the Instance() method (& create sole instance if not already created) **/

//remember! - these methods are OUTSIDE of the CSuperConLog class

//This version would be used at any point to obtain a pointer to the
//singleton class (to access public methods in this class) 
CSuperConLog &SuperConLog() {
  return *CSuperConLog::Instance();
}

//This overload of the method would be used in the Main file (as it requires
//knowledge of the Timber logging level which is a parameter set on
//startup (--option)
CSuperConLog &SuperConLog( int skynet_key, int usrSpefTimberLogLevel, bool nodisplay ) {
  return ( *CSuperConLog::Instance( skynet_key, usrSpefTimberLogLevel, nodisplay ) );
}

/* Initialise static vars */
//Initialise the pointer to the sole instance
CSuperConLog* CSuperConLog::pinstance = NULL;

//Initialise the flag used to determine whether to pass msgs
//to the sparrowhawk display
bool CSuperConLog::passMsgsToSparrowHawk = true;

//Set sparrowHawk & timber logging to amazingly annoying logging level
//(log absolutely everything - i.e. the *last* logging level in the list)
//upon initialisation
int CSuperConLog::timberLoggingLevel = ( LAST_LOG_LEVEL_VALUE - 1 );

//disable timber logging by default on initialisation
bool CSuperConLog::timberLoggingEnabled = false;
bool CSuperConLog::timberLoggingLevelSet = false;




/** BASE CLASS METHODS **/
//Constructor
CSuperConLog::CSuperConLog( int skynet_key ) 
  : CSkynetContainer(MODsupercon, skynet_key), CTimberClient(timber_types::superCon)
{
  //Initialise var used for timestamps
  t = time(NULL);  
}

//Destructor
CSuperConLog::~CSuperConLog() {

  //close the open logging files here (probably never used, but hey)
  scStateServer_file.close();
  scMsgs_file.close();
  scState_file.close();
}

//return pointer to the sole existing instance, or create sole instance and
//return pointer to it if the instance does not exist
CSuperConLog* CSuperConLog::Instance() {
  
  if( pinstance == NULL ) {    

    //This point should ONLY be reached if an ERROR has occurred (cannot
    //continue, as it would require creating the m_skynet instance WITHOUT
    //a skynet key -> exit.  NOTE - reaching this point would be as a result
    //of a USER INDUCED error
    cerr << "FATAL ERROR: CSuperConLog::Instance() - You called Instance() BEFORE Instance(skynet_key), CANNOT proceed (as cannot instantiate m_skynet with no skynet key) - EXITING" << endl;
    exit(1);
  }
  //instance already exists -> return pointer to it
  return pinstance;
}

//This overload Intended to be used by the Main method to set the Timber 
//logging level set by the user on the command line
CSuperConLog* CSuperConLog::Instance( int sn_key, int TimberLogLevel, bool nodisplay ) {
  
  passMsgsToSparrowHawk = !nodisplay;
  
  /* Timber Logging */
  
  if( pinstance == NULL ) {
    //sole instance not yet created
    
    //create the sole instance
    pinstance = new CSuperConLog( sn_key );
    
    if( timberLoggingLevelSet == false ) {

      timberLoggingEnabled = pinstance->initTimber( TimberLogLevel );

      if( timberLoggingEnabled == false ) {
	//cerr << "ERROR: you wanted to log using timber, BUT timber logging is NOT enabled (is timber running?) \n\n ERROR: you wanted to log using timber, BUT timber logging is NOT enabled (is timber running?)" << endl;
      }
      
    } else { //this point should never be reached

      //timber log level already set - ignore argument & flag error message
      //cerr << "ERROR: you tried to set the timber logging level twice!" << endl;
      //cerr << "FUNKY ERROR: If you are reading this message then something has gone BADLY wrong (pinstance reset to NULL after singleton instance created) - Investigate NOW!" << endl;
      return pinstance;
    }
  } else {
    //cerr << "ERROR: you tried to set the timber logging level twice!" << endl;
  }
  
  //return pointer to sole instance
  return pinstance;
}

/** LOGGING METHODS  **/

//REMEMBER: These can & will be called by multiple threads (potentially
//simultaneously) hence they need to be thread-safe.  Hence there are no
//static members of the methods - ALSO these methods ASSUME that DIFFERENT
//threads will **NEVER** want to log to the SAME FILE.  

//UN-CONDITIONAL (no test) combined SparrowHawk/Timber logging method [accepts
//arguments like printf(...)]
void CSuperConLog::log( superConComponents comp, int logLevel, const char *str, ... ) {
   
  char log_buf[LOGGING_BUFFER_SIZE]; //has to be thread-safe - so re-declare

  //number of characters NOT output to log when the logging method was
  //called because the buffer was too small (used for error checking)
  int bufSize; 
  
  //Construct the string passed into the method in printf() form into a string
  //in the log buffer, NOTE - vsnprintf() like the printf() family will only
  //write 'size' characters into the buffer, and will return the number of
  //characters that were not written in the case when the buffer was too small
  va_list lst;  
  va_start(lst, str);
  bufSize = vsnprintf(log_buf, sizeof(log_buf)-1, str, lst);
  va_end(lst);
  
  if( bufSize > LOGGING_BUFFER_SIZE ) {
    //cerr << "ERROR: Your log message was > LOGGING_BUFFER_SIZE - it has been truncated" << endl;
    if( passMsgsToSparrowHawk == true ) {
      //SparrowHawk().log( "ERROR: Your log message was > LOGGING_BUFFER_SIZE - it has been truncated" );
      SparrowHawk().log( "ERROR: MESSAGE TOO LONG" );
    }
  }

  if( logLevel <= SPARROW_LOG_LEVEL && passMsgsToSparrowHawk == true ) {
    //Send message to SparrowHawk display (STATUS tab)
    SparrowHawk().log(log_buf);
  }
  
  if( logLevel <= timberLoggingLevel && timberLoggingEnabled == true ) {
    //Log message to a file using timber (which file is determined by 
    //writeTimber(...) using the superCon component which authored the
    //message to be logged (comp)
    writeTimber( comp, log_buf );
  }
}

//CONDITIONAL (test must == true for message to be logged) combined SparrowHawk/Timber 
//logging method [accepts arguments like printf(...)]
void CSuperConLog::log( superConComponents comp, bool test, int logLevel, const char *str, ... ) {

  if( test == true ) {
    
    char log_buf[LOGGING_BUFFER_SIZE];
    
    //number of characters NOT output to log when the logging method was
    //called because the buffer was too small (used for error checking)
    int bufSize; 
    
    //Construct the string passed into the method in printf() form into a string
    //in the log buffer, NOTE - vsnprintf() like the printf() family will only
    //write 'size' characters into the buffer, and will return the number of
    //characters that were not written in the case when the buffer was too small
    va_list lst;  
    va_start(lst, str);
    bufSize = vsnprintf(log_buf, sizeof(log_buf)-1, str, lst);
    va_end(lst);
    
    if( bufSize > LOGGING_BUFFER_SIZE ) {
      //cerr << "ERROR: Your log message was > LOGGING_BUFFER_SIZE - it has been truncated" << endl;
      if( passMsgsToSparrowHawk == true ) {
	//SparrowHawk().log( "ERROR: Your log message was > LOGGING_BUFFER_SIZE - it has been truncated" );
	SparrowHawk().log( "ERROR: MESSAGE TOO LONG" );
      }
    }
   
    if( logLevel <= SPARROW_LOG_LEVEL && passMsgsToSparrowHawk == true ) {
      //Send message to SparrowHawk display (STATUS tab)
      SparrowHawk().log(log_buf);
    }
    
    if( logLevel <= timberLoggingLevel && timberLoggingEnabled == true ) {
      //Log message to a file using timber (which file is determined by 
      //writeTimber(...) using the superCon component which authored the
      //message to be logged (comp)
      writeTimber( comp, log_buf );
    }
  }
}


/** HELPER METHODS **/

//performs set-up required for timber (file) logging
bool CSuperConLog::initTimber( int TimberLogLevel )
{  

  timberLoggingLevel = TimberLogLevel;

  return true;
}

//writes the string (buffer) authored by [comp] to the output files
//configured in the switch() statement internal to the method.  ALL
//entries are pre-fixed with a timestamp (HHMMSS), then <tab>, and then
//the buffer passed in the argument, and EOL is written *after* each
//buffer
void CSuperConLog::writeTimber( superConComponents comp, char *buffer )
{
  if(!verifyLogging())
    return;

  //update to local time
  local = localtime(&t);
  sprintf(shortTimeStamp, "%02d%02d%02d", local->tm_hour, local->tm_min, local->tm_sec);
 
  /* This statement denotes where/if log messages are actually written to a
   * log file */
  switch( comp ) {
    
  case SRV:
    scStateServer_file << shortTimeStamp <<"\t"<< buffer << endl;
    break;
    
  case DGN:
    scMsgs_file << shortTimeStamp <<"\t"<< buffer << endl;
    break;

  case SIF:
    scMsgs_file << shortTimeStamp <<"\t"<< buffer << endl;
    break;

  case STR:
    scMsgs_file << shortTimeStamp <<"\t"<< buffer << endl;      
    break;

  default:
    //cerr << "ERROR: you entered a superCon component ["<<comp<<"] in a call to CSuperConLog::log(...) for which NO entry in the writeTimber(...) method exists (don't know where to log)" << endl;
    break;
  }
  
  //Check status of the output streams
  if( !scStateServer_file.good() ) {
    //cerr << "ERROR: write error occurred for file: "<< specific_scStateLog_name << " - STOPPING TIMBER LOGGING" << endl;
    //SparrowHawk().log( "ERROR: write error occurred for file: %s - STOPPING TIMBER LOGGING", specific_scStateLog_name );
    timberLoggingEnabled = false;
  }
  if( !scMsgs_file.good() ) {
    //cerr << "ERROR: write error occurred for file: "<< specific_scMsgs_name << " - STOPPING TIMBER LOGGING" << endl;
    //SparrowHawk().log( "ERROR: write error occurred for file: %s - STOPPING TIMBER LOGGING", specific_scMsgs_name );
    timberLoggingEnabled = false;
  }  

}


void CSuperConLog::writeState(SCstate *pState)
{
  if(!verifyLogging())
    return;

  unsigned long long time;
  DGCgettime(time);
  scState_file<<time<<" ";
  pState->log_write(scState_file);
  scState_file<<endl;
}

bool CSuperConLog::readState(SCstate *pState)
{
  if(!verifyLogging())
    return false;

  unsigned long long time;
  scState_file>>time;
  pState->log_read(scState_file);

  return true;
}


/*** Helper functions ***/

//open or reopen files
bool CSuperConLog::verifyLogging()
{
  bool checkTimber;

  //Ask timber whether logging is enabled (through timber)
  if( !getLoggingEnabled() )
    return false;
  
  //Set-up log file locations
  string dir = getLogDir();
  if(checkNewDirAndReset()) {
    //Close open files
    if(scStateServer_file.is_open())
      scStateServer_file.close();
    if(scMsgs_file.is_open())
      scMsgs_file.close();
    if(scState_file.is_open())
      scState_file.close();

    //Open all the files
    string filename = dir + LOGFILE_STATESERVER;
    scStateServer_file.open( filename.c_str(), ios::out);
    if( passMsgsToSparrowHawk == true ) {
      SparrowHawk().log("Loging State Server to file: %s", filename.c_str());
    }
    
    filename = dir + LOGFILE_MSG;
    scMsgs_file.open( filename.c_str(), ios::out);
    if( passMsgsToSparrowHawk == true ) {
      SparrowHawk().log("Loging Msgs to file: %s", filename.c_str());
    }
    
    filename = dir + LOGFILE_STATE;
    scState_file.open( filename.c_str(), ios::out);
    if( passMsgsToSparrowHawk == true ) {
      SparrowHawk().log("Loging State Table to file: %s", filename.c_str());
    }

    if(!scStateServer_file.is_open() || !scMsgs_file.is_open() || !scState_file.is_open()) {
      //error opening log files - do NOT try to log using timber
      cerr << "ERROR: failed to open log files properly (is_open() failed) - TIMBER LOGGING DISABLED!" << endl;
      if( passMsgsToSparrowHawk == true ) {
	SparrowHawk().log( "ERROR: failed to open log files properly (is_open() failed) - TIMBER LOGGING DISABLED!" );
      }
      checkTimber = false;
    }
  }
  
  return true;
}
