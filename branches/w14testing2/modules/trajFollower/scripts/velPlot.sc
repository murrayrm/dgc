#!/bin/sh -x

rm -f state path yerr FF

export TIME=20050804_085903.dat

paste lindzey$TIME | awk '{print NR, $11}' > ref
paste lindzey$TIME | awk '{print NR, $12}' > act
paste lindzey$TIME | awk '{print NR, $13}' > err
paste lindzey$TIME | awk '{print NR, $15}' > accCmd

echo "plot 'ref', 'act', 'accCmd' with dots" | gnuplot -persist


