
/**
 * StartTab.h
 * Revision History:
 * 02/17/2005  hbarnor  Created
 */

#ifndef STARTTAB_HH
#define STARTTAB_HH

#include <gtkmm/box.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/button.h>
#include <gtkmm/frame.h>
#include <gtkmm/table.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/table.h>
#include <gtkmm/textview.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textbuffer.h>
#include <gtkmm/spinbutton.h>
#include <glibmm/ustring.h>
#include <string>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

#include <fstream>
#include "StatusTab.hh"

#include "sn_msg.hh"


using namespace std;

#define DEBUG false

/**
 * StartTab class.
 * $Id$ 
 */

static string _module = "Module:";
//static int pathmax = PATH_MAX;

// Forward declare struct
struct command
{
  char type[20];   //name of the command
  char action;     // the action to perform S is start and k is kill 
  pid_t PID;         //modules's PID if the module could be started or if it is already running. 
                   //It is 0 if module could not be started
  int compID;     // unique computer ID
};

struct ButtonStatus
{
  Gtk::CheckButton * btn;
  struct command cmd;
};

class StartTab : public Gtk::HBox
{
  //All row representing a single module
  class ModuleRow {
  public:
    Gtk::CheckButton *chkBtn;          //Select/deselect module
    Gtk::CheckButton *kplBtn;          //KeepAlive/NokeepAlive a  module
    Gtk::Label *moduleLbl;             //Module name display
    Gtk::Entry *cmdEnt;                //Command line entry box
    Gtk::SpinButton *cpuSpn;           //cpu number to run on
    Gtk::Button *startBtn;             //Start btn
    Gtk::Button *killBtn;              //Kill btn

    struct command cmd;
  };

public:
  /**
   * Default constructor.
   */
  StartTab();
  /**
   * Constructor with skynet key .
   */
  StartTab(int sn_key,  sigc::signal<void>* addTabSig);
  /**
   * StartTab Destructor - for taking care of dynamically 
   * allocated memory. 
   */
  ~StartTab();
  /**
   * addModule - adds a checkbox with the name of the module passed
   * and also the destination computer 
   *
   */
  void addModule(string snModName, string cmdline, int destComp, bool start);
  /**
   * Start any sub-threads that need to be started.
   * Needs to start StatusTab directory.
   */
  void startSubThreads();
  void listen();
  /**
   * Returns a pointer to a managed tab.
   * Wrapper for StatusTab's get Tab
   * @return the widget to be added.
   */
  Widget* getTab()
  {
    return m_statusTab.getTab();
  };
  /**
   * Returns the name of the tab to be added.
   * Returns the current value of tabName
   * @return The name of the widget to be added.
   */
  string getTabName()
  {
    return m_statusTab.getTabName();
  };
  /**
   * TODO
   */
  void updateConfig(string fileName, string snModName, bool start);
  /**
   * TODO
   */
  void readFile(string fileName);
   /**
   * statusDisplay - logs feedback messages from moduleStarter
   * and displays it in a text box
   */
  void statusDisplay();
private:
  /**
   *
   */
  static const time_t sleep_secs;
  
  static const long sleep_nsecs;

  /**
   * My skynet key.
   */
  int m_sn_key;
  /**
   * Listen thread for skynet messages.
   */
  Glib::Thread *  statusTab_thread; 
  Gtk::Entry * rootDir;
  /**
   * Status Display 
   */
  /**
   * List of all modules currently displayed on the tab
   */
  list<ModuleRow *> moduleRows;
  /**
   * initialize skynet
   *
   */
  void initSN();
  /**
   * TODO
   *
   */


  void executeSkynet(char action, ModuleRow *row);
  /**
   * buttonBox - holds the start and stop buttons
   */
  Gtk::Frame modulesFrm; // frame to hold list of modules
  Gtk::VBox modulesVBox; // VBox to hold list of modules and start all / exit all buttons
  Gtk::Table modulesTbl; // the tbl in which the module list resides
  Gtk::HButtonBox modulesBtnBox; // box to contain the start/kill selected buttons
  Gtk::Button modulesSSelBtn; // module start selected button
  Gtk::Button modulesKSelBtn; // module kill selected button
  Gtk::Frame statusDisplayFrm; // frame to hold status
  Gtk::VBox m_VBox;
  Gtk::ScrolledWindow statWindow;
  Gtk::TextView statText;
  Glib::RefPtr<Gtk::TextBuffer> buffer;
  Gtk::TextBuffer::iterator statPos;

  StatusTab m_statusTab;
  //took me 3 hours to figure this out
  Glib::Dispatcher updateDisplay;
  string myConfigFile;
  static const int NUM_CHAR = 256; 
  /**
   *Storage for messages
   */
  char mesg[NUM_CHAR];


  /**
   * a skynet client
   */
  skynet m_skynet;  
  /*
   * ModeMan Start action
   */
  void startModules();
  void startModule(ModuleRow *row);

  /*
   * modeman exit action 
   */
  void killModules();
  void killModule(ModuleRow *row);
  /**
   * Skynet send socket
   */
  int cmdSendSocket;
  /**
   * sender of status messages    
   */
   modulename sender;
   /**
   * mesgs i recive, this is the
   * moduleStarter status message
   */
  sn_msg myInput;
  /**
   * adds a message to the status tab
   */
  void addMessage();

};

#endif
