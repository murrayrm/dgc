#include "stereoFeeder.hh"
#include <DGCutils>

int QUIT=0;
int PAUSE=0;
int STEP=0;
int CLEAR=0;
int RESEND=0;

double time_lock;
double time_cap;
double time_rect;
double time_disp;
double time_fill;
double time_eval;
double time_vote;
double time_total;
double time_debug;
double time_full;

extern int optSparrow, optVote, optPair, optExpCtrl, optLevel;
extern double optDelay, optRate;

#define CALIB_PATH_PREFIX "./config/stereovision/"

#define CALIB_SVSCAL_NAME    "SVSCal.ini"
#define CALIB_SVSPARAMS_NAME "SVSParams.ini"
#define CALIB_CAMCAL_NAME    "CamCal.ini"
#define CALIB_CAMID_NAME     "CamID.ini"

#define CALIB_SHORT_RANGE_SUFFIX ".short"
#define CALIB_LONG_RANGE_SUFFIX  ".long"
#define CALIB_SHORT_COLOR_SUFFIX ".short_color"
#define CALIB_HOMER_SUFFIX       ".homer"

#define SUNPARAMS_NAME "SunParams.ini"

#define DEFAULT_LOGGING_FREQUENCY 0.5

#define DEBUG(x) {cout << x << endl;}

using namespace std;

extern int QUIT;
extern int PAUSE;

extern double time_lock;
extern double time_cap;
extern double time_rect;
extern double time_disp;
extern double time_fill;
extern double time_eval;
extern double time_vote;
extern double time_total;
extern double time_debug;
extern double time_full;

extern char debugFilenamePrefix[200];
extern char logFilenamePrefix[200];
extern char sourceFilenamePrefix[200];
extern char optFormat[10];
extern int logFrames, logRect, logDisp, logState, logMaps, logTime, logVotes;
extern int optDisplay, optPair, optUseCameras, optSim, optFile, optSparrow, optMaxFrames, optMaxLoops, optZeroAltitude, optOneFile;
extern bool waitForState;

double time_ms = 0;
double rate = 0;
int nbrSentMessages = 0;

int pairIndex;

int lastPair;

StereoFeeder::StereoFeeder(int skynetKey) 
  : CSkynetContainer(MODstereofeeder, skynetKey), CTimberClient(timber_types::stereoFeeder),
		CStateClient(waitForState) {
  //Initialize our calibration file stuff to defined defaults
  
  last_logtime=0;
  prevLogLine = 0;

  sprintf(calibFileObject[SHORT_RANGE].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(calibFileObject[SHORT_RANGE].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(calibFileObject[SHORT_RANGE].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_SHORT_RANGE_SUFFIX);
  sprintf(calibFileObject[SHORT_RANGE].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_SHORT_RANGE_SUFFIX);

  sprintf(calibFileObject[LONG_RANGE].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(calibFileObject[LONG_RANGE].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(calibFileObject[LONG_RANGE].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_LONG_RANGE_SUFFIX);
  sprintf(calibFileObject[LONG_RANGE].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_LONG_RANGE_SUFFIX);

  sprintf(calibFileObject[SHORT_COLOR].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(calibFileObject[SHORT_COLOR].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(calibFileObject[SHORT_COLOR].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_SHORT_COLOR_SUFFIX);
  sprintf(calibFileObject[SHORT_COLOR].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_SHORT_COLOR_SUFFIX);

  sprintf(calibFileObject[HOMER].SVSCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSCAL_NAME, CALIB_HOMER_SUFFIX);
  sprintf(calibFileObject[HOMER].SVSParamFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_SVSPARAMS_NAME, CALIB_HOMER_SUFFIX);
  sprintf(calibFileObject[HOMER].CamCalFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMCAL_NAME, CALIB_HOMER_SUFFIX);
  sprintf(calibFileObject[HOMER].CamIDFilename, "%s%s%s", CALIB_PATH_PREFIX, CALIB_CAMID_NAME, CALIB_HOMER_SUFFIX);
  
  if(optUseCameras) {
    if(sourceObject.init(0, calibFileObject[optPair].CamIDFilename, logFilenamePrefix, optFormat, calibFileObject[optPair].SVSParamFilename)!=stereoSource_OK) {
      fprintf(stderr, "Error while initializing cameras!  Quitting...\n");
      exit(1);
    }
  }

  char* sunParams;
  char* cutOffsParamsFilename;
  if(optPair==SHORT_RANGE){
    sunParams = "./config/stereovision/SunParams.ini.short";
    cutOffsParamsFilename = "./config/stereovision/PointCutoffs.ini.short";
  }
  else if(optPair == LONG_RANGE){
    sunParams = "./config/stereovision/SunParams.ini.long";
    cutOffsParamsFilename = "./config/stereovision/PointCutoffs.ini.long";
  }
  
  processObject.init(0,
		     calibFileObject[optPair].SVSCalFilename,
		     calibFileObject[optPair].SVSParamFilename,
		     calibFileObject[optPair].CamCalFilename,
		     logFilenamePrefix,
		     optFormat, 0, sunParams, cutOffsParamsFilename);

  if(optSim || optFile) {
    sourceObject.init(0, processObject.imageSize.width, processObject.imageSize.height, sourceFilenamePrefix, optFormat, 1);
  }
  stereoMap.initMap(CONFIG_FILE_DEFAULT_MAP);

  layerID_stereoAvgDbl = stereoMap.addLayer<double>(CONFIG_FILE_STEREO_MEAN_ELEV, true);
    layerID_fusedElev = stereoMap.addLayer<CElevationFuser>(CONFIG_FILE_STEREO_FUSED_ELEV, true); 

  DGCcreateMutex(&cameraMutex);

  if(optDisplay) {
    processObject.show();
  }

  if(optExpCtrl) {
    sourceObject.startExposureControl(processObject.subWindow);
  }

  DGCcreateMutex(&mapMutex);
}


StereoFeeder::~StereoFeeder() {
#warning "Need to implement deconstructor"
  printf("Thank you for using the stereoFeeder destructor\n");
}


void StereoFeeder::ActiveLoop() {
  int socket_num_mean;
  int socketNumFused;
  if (optPair == SHORT_RANGE) {
    socketNumFused  = m_skynet.get_send_sock(SNstereoShortDeltaMap);    
		socket_num_mean = m_skynet.get_send_sock(SNstereoShortDeltaMapElev);
  }
  else { //if (optPair == LONG_RANGE)
    socketNumFused  = m_skynet.get_send_sock(SNstereoLongDeltaMap);    
		socket_num_mean = m_skynet.get_send_sock(SNstereoLongDeltaMapElev);
  }

  CDeltaList* deltaPtr = NULL;

  unsigned long long playbackTime = 0;
  unsigned long long newPlaybackTime;

  int lineNum;
  unsigned long long grabTimestamp;
  
  while(!QUIT) {
    if(optSparrow) {
      UpdateSparrowVariablesLoop();
    }
    usleep(10000);


    if(!PAUSE) {
      if(optMaxLoops == 0) {
	if(optSparrow) {
	  user_quit(0);
	  sleep(1);
	}
	QUIT = 1;
	return;
      }

  
      if(optMaxLoops>0) optMaxLoops--;
      
      unsigned long long startFull, endFull, resultFull;
      DGCgettime(startFull);
      
      NEDcoord UTMPoint, Point;
      //unsigned long long start, end, result;
      unsigned long long startTotal, endTotal, resultTotal;
    

      DGCgettime(startTotal);
      {
	DGClockMutex(&cameraMutex);

	if(optFile) {
	  newPlaybackTime = getPlaybackTime();
	  unsigned long long ihate;
	  DGCgettime(ihate);
	  //printf("current time is %llu, dgcgettime is %llu\n", newPlaybackTime, ihate);
	  playbackTime = 0;
	  FILE *stateLogFile = NULL;
	  char stateLogFilename[256];
	  sprintf(stateLogFilename, "%s.state", sourceFilenamePrefix);
	  stateLogFile = fopen(stateLogFilename, "r");
	  if(stateLogFile != NULL) {
	    //while(newPlaybackTime >= playbackTime && !feof(stateLogFile)) {
	    do {  
	      fscanf(stateLogFile, "%d %llu %llu %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		     &lineNum,
		     &grabTimestamp,
		     &m_state.Timestamp, 
		     &m_state.Northing, &m_state.Easting, &m_state.Altitude,
		     &m_state.Vel_N, &m_state.Vel_E, &m_state.Vel_D,
		     &m_state.Acc_N, &m_state.Acc_E, &m_state.Acc_D,
		     &m_state.Roll, &m_state.Pitch, &m_state.Yaw,
		     &m_state.RollRate, &m_state.PitchRate, &m_state.YawRate,
		     &m_state.RollAcc, &m_state.PitchAcc, &m_state.YawAcc);
	      if (optZeroAltitude == 1) {
		m_state.Altitude = 0;
	      }
	      playbackTime = grabTimestamp;
	      // cout << "feeder: just read line number " << lineNum << " from .state file" << endl;
	      // cout << "checking for file... " << endl;
	      char testFilename[256];
	      sprintf(testFilename, "%s%.5d-L.%s", sourceFilenamePrefix, lineNum, optFormat);
	      FILE *testFile = NULL;
	      if (lineNum > prevLogLine) {
		testFile = fopen(testFilename, "r");
		if (testFile != NULL) {
		  // cout << "and there was a file!" << endl;
		  // cout << "I think I've found a file with name: " << testFilename << endl;
		  fclose(testFile);
		  prevLogLine = lineNum;
		  break;
		} 
	      }
	    } while (!feof(stateLogFile));
	    if (feof(stateLogFile)) {
	      cout << "Stereofeeder: Reached the end of the stereo state log." << endl;
	      exit(0);
	    }
	    //cout << "feeder: Settled for line/file number " << lineNum << endl;
	    if(sourceObject.grab(sourceFilenamePrefix, optFormat, lineNum)) {
	      //cout << "feeder: found a file!" << endl; 
	    }
	    else {
	      cout << "stereoFeeder: did not find a file when it should have!" << endl;
	      usleep(1000*1000*1000);
	    }
	      fclose(stateLogFile);
	    blockUntilTime(grabTimestamp);
	  } 
	  else {
	    if(optSparrow) {
	      user_quit(0);
	      sleep(1);
	    }
	    QUIT = 1;
	    return;
	  }
	}
      
	if(stereoProcess_OK != processObject.loadPair(sourceObject.pair(), m_state)) {
	  printf("Error while grabbing images from cameras at line %d in %s!  Quitting...\n", __LINE__, __FILE__);
	  return;
	}
	pairIndex = sourceObject.pairIndex();
	DGCunlockMutex(&cameraMutex);
      }
	// cout << "starting to process image pair" << endl;
      tempState = processObject.currentState();

      processObject.pitchControl();
      processObject.calcRect();

      processObject.calcDisp();

      // XYZcoord RelativePoint; commented out since nothing appears to use this

      DGClockMutex(&mapMutex);
      stereoMap.updateVehicleLoc(tempState.Northing, tempState.Easting);
      //      ofstream tempFile("/tmp/UTMPointCloud.txt", ios::app);  	
      
      if (processObject.generateUTMPointCloud()) {
	while(processObject.getNextUTMPoint(&UTMPoint)) {
	  //  tempFile << "N: " << UTMPoint.N << ", E: " << UTMPoint.E << ", D: " << UTMPoint.D << endl;
	  CElevationFuser tempCell = stereoMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMPoint.N, UTMPoint.E);
	  double before = tempCell.getMeanElevation();
	  CElevationFuser::CELL_TYPE ctBefore = tempCell.getCellType();
	  tempCell.fuse_MeanElevation(UTMPoint, tempState.Timestamp);
	  stereoMap.setDataUTM_Delta<CElevationFuser>(layerID_fusedElev, UTMPoint.N, UTMPoint.E, tempCell);
	  if((tempCell.getMeanElevation()==1.0 || tempCell.getMeanElevation()==0.0) && tempCell.getCellType() != CElevationFuser::OUTSIDE_MAP)
	    cout << "Holy crap " << setprecision(10) << UTMPoint.N << "," << UTMPoint.E << " = " << tempCell.getMeanElevation()
		 << " and fused in" << UTMPoint.D
		 << " and before was " << before
		 << " and celltype was " << ctBefore << " and now is " << tempCell.getCellType() << endl;
	  stereoMap.setDataUTM_Delta<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E, tempCell.getMeanElevation());
	  if(stereoMap.getDataUTM<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E) ==
	     stereoMap.getLayerNoDataVal<double>(layerID_stereoAvgDbl)) 
	    printf("%s [%d]: Setting a no data val of %lf into the map!?!?!\n", __FILE__, __LINE__, stereoMap.getDataUTM<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E));
	  } 
      }
      else {
	//	tempFile << "Could not generate UTMPointcloud!" << endl;
      }
      //   tempFile.close();

      /*
       for(int y=processObject.subWindow.y; y<processObject.subWindow.y+processObject.subWindow.height; y++) {
	 //for(int y=0; y<processObject.subWindow.height; y++) {
	for(int x=processObject.subWindow.x; x<processObject.subWindow.x+processObject.subWindow.width; x++) {
	  //for(int x=0; x<processObject.subWindow.width; x++) {
	
	  if(processObject.SinglePoint(x, y, &UTMPoint)) {
	    CElevationFuser tempCell = stereoMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMPoint.N, UTMPoint.E);
	    double before = tempCell.getMeanElevation();
	    CElevationFuser::CELL_TYPE ctBefore = tempCell.getCellType();
	    tempCell.fuse_MeanElevation(UTMPoint, tempState.Timestamp);
	    stereoMap.setDataUTM_Delta<CElevationFuser>(layerID_fusedElev, UTMPoint.N, UTMPoint.E, tempCell);
	    if((tempCell.getMeanElevation()==1.0 || tempCell.getMeanElevation()==0.0) && tempCell.getCellType() != CElevationFuser::OUTSIDE_MAP)
	      cout << "Holy crap " << setprecision(10) << UTMPoint.N << "," << UTMPoint.E << " = " << tempCell.getMeanElevation()
		   << " and fused in" << UTMPoint.D
		   << " and before was " << before
		   << " and celltype was " << ctBefore << " and now is " << tempCell.getCellType() << endl;
	    stereoMap.setDataUTM_Delta<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E, tempCell.getMeanElevation());
	    if(stereoMap.getDataUTM<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E) ==
	       stereoMap.getLayerNoDataVal<double>(layerID_stereoAvgDbl)) 
	      printf("%s [%d]: Setting a no data val of %lf into the map!?!?!\n", __FILE__, __LINE__, stereoMap.getDataUTM<double>(layerID_stereoAvgDbl, UTMPoint.N, UTMPoint.E));
	    
	    
	  }
	}
      }
      */
      
      //SKYNET
      deltaPtr = stereoMap.serializeDelta<CElevationFuser>(layerID_fusedElev);
      
      //code to send delta
      if(!deltaPtr->isShiftOnly()) {
	
	//cout << " calling sendMapdelta" << endl;
	
	SendMapdelta(socketNumFused, deltaPtr);
      }
      //stereoMap.resetDelta(layerID_stereoAvgDbl);
      stereoMap.resetDelta<CElevationFuser>(layerID_fusedElev);
      
      
      deltaPtr = stereoMap.serializeDelta<double>(layerID_stereoAvgDbl);
      //DEBUG("deltaSize is " << deltaSize)
      //code to send delta
      if(!deltaPtr->isShiftOnly()) {
	
	//  cout << " calling sendMapdelta" << endl;
	
	++nbrSentMessages;      
	SendMapdelta(socket_num_mean, deltaPtr);
      }
      stereoMap.resetDelta<double>(layerID_stereoAvgDbl);
      //stereoMap.resetDelta(layerID_fusedElev);
      DGCunlockMutex(&mapMutex);
      
      DGCgettime(endTotal);
      resultTotal = endTotal-startTotal;  
      time_total = DGCtimetosec(resultTotal)*1000;
      
      if(logRect) processObject.saveRect(logFilenamePrefix, optFormat, pairIndex);
      if(logDisp) processObject.saveDisp(logFilenamePrefix, optFormat, pairIndex);
      if(logMaps) {
	char logMapsFilename[256];
	sprintf(logMapsFilename, "%s%.6d", logFilenamePrefix, pairIndex);
      }
      
      time_ms = DGCtimetosec(resultTotal)*1000;
      rate = 1/DGCtimetosec(resultTotal);
      
      DGCgettime(endFull);
      resultFull = endFull-startFull;
      time_full = DGCtimetosec(resultFull)*1000;
      
      if(logTime) {
	FILE *timeLogFile = NULL;
	char timeLogFilename[256];
	sprintf(timeLogFilename, "%s.time", logFilenamePrefix);
	
	timeLogFile = fopen(timeLogFilename, "a");
	if(timeLogFile) {
	  fprintf(timeLogFile, "%.6d %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		  pairIndex,
		  time_lock,
		  time_cap,
		  time_rect,
		  time_disp,
		  time_fill,
		  time_eval,
		  time_vote,
		  time_total,
		  time_debug,
		  time_full);
	  fclose(timeLogFile);
	}
      }
    }
  }
}
  
  
void StereoFeeder::ImageCaptureThread() {
  unsigned long long grabTimestamp;

  while(true && !QUIT) {
    if(!PAUSE && (optUseCameras || optSim)) {
      if(optMaxFrames == 0) {
	if(optSparrow) {
	  user_quit(0);
	  sleep(1);
	}
	QUIT = 1;
	return;
      }
      
      if(optMaxFrames>0) optMaxFrames--;
      
      {
	DGClockMutex(&cameraMutex);
	if(optUseCameras) {
	  grabTimestamp = sourceObject.grab();
	  UpdateState(grabTimestamp);
	  if(optZeroAltitude)
	    m_state.Altitude = 0;
	} else if(optSim) {
	  usleep(33000);
	  if(sourceObject.grab() || true) {
	    FILE *stateLogFile = NULL;
	    char stateLogFilename[256];
	    sprintf(stateLogFilename, "%s.state", sourceFilenamePrefix);
	    stateLogFile = fopen(stateLogFilename, "r");
	    if(stateLogFile != NULL) {
	      int lineNum=-1;
	      while(lineNum != sourceObject.pairIndex()-1) {
		fscanf(stateLogFile, "%d: %lld %lld %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		       &lineNum,
		       &grabTimestamp,
		       &m_state.Timestamp, 
		       &m_state.Northing, &m_state.Easting, &m_state.Altitude,
		       &m_state.Vel_N, &m_state.Vel_E, &m_state.Vel_D,
		       &m_state.Acc_N, &m_state.Acc_E, &m_state.Acc_D,
		       &m_state.Roll, &m_state.Pitch, &m_state.Yaw,
		       &m_state.RollRate, &m_state.PitchRate, &m_state.YawRate,
		       &m_state.RollAcc, &m_state.PitchAcc, &m_state.YawAcc);
	      }
	      fclose(stateLogFile);
	    }
	    if(optOneFile) {
	      DGCunlockMutex(&cameraMutex);
	      return;
	    }
	  } else {
	    if(optSparrow) {
	      user_quit(0);
	      sleep(1);
	    }
	    QUIT = 1;
	    return;
	  }
	}
	DGCunlockMutex(&cameraMutex);
      }

      if(getLoggingEnabled()) {
	if(checkNewDirAndReset()) {
	  logFrames = 1;
	  logState = 1;
	  sprintf(logFilenamePrefix, "%sstereo", getLogDir().c_str());
	}
      } else {
	logFrames = 0;
	logState = 0;
      }
      
      
      if(logFrames) {
	int level = getMyLoggingLevel();
	//int level =optLevel;
	if( level >=3){
	  DGCgettime(last_logtime);
	  sourceObject.save(logFilenamePrefix, optFormat, sourceObject.pairIndex());
	}      
	else if(level==2){
	  unsigned long long current_time;
	  DGCgettime(current_time);
	  if((double)(current_time -last_logtime)> (1000000.0/(double)DEFAULT_LOGGING_FREQUENCY)){
	    last_logtime = current_time;
	    sourceObject.save(logFilenamePrefix, optFormat, sourceObject.pairIndex());
	  }
	}
      }
      if(logState) {
	FILE *stateLogFile = NULL;
	char stateLogFilename[256];
	sprintf(stateLogFilename, "%s.state", logFilenamePrefix);
	
	stateLogFile = fopen(stateLogFilename, "a");
	if(stateLogFile) {
	  fprintf(stateLogFile, "%.6d %llu %llu %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		  sourceObject.pairIndex(),
		  grabTimestamp,
		  m_state.Timestamp, 
		  m_state.Northing, m_state.Easting, m_state.Altitude,
		  m_state.Vel_N, m_state.Vel_E, m_state.Vel_D,
		  m_state.Acc_N, m_state.Acc_E, m_state.Acc_D,
		  m_state.Roll, m_state.Pitch, m_state.Yaw,
		  m_state.RollRate, m_state.PitchRate, m_state.YawRate,
		  m_state.RollAcc, m_state.PitchAcc, m_state.YawAcc);
	  fclose(stateLogFile);
	}
      }
      usleep(100);
      if(optDelay!=-1) {
	usleep((unsigned int)(optDelay*1e6));
      }
      if(optRate!=-1) {
	usleep((unsigned int)((1/optRate)*1e6));
      }
    }
  }    
}
  
