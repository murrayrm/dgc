/**
 * DGCKeepAlive.cc
 * Revision History:
 * 10/09/2005 hbarnor created
 * $Id$
 * DGCKeepAlive - wrapper that starts up DGCModules and logs
 * information to enable processMonitoring. 
 */

#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <ctime>

#include <limits.h>

#include "pm.h"

#define DEBUG 0

using namespace std;
// forward declare

/**
 * spawn - forks, then calls execv to execute the module with the
 * options passed. 
 * @param argv - the arguments to the command and the command itself. 
 */
void spawn(char *argv[]);
/**
 * logProcess - logs the pid, the command and its arguments to the
 * DIR_RUN directory. Also logs the start time, the user, the key to
 * the DIR_LOG directory. 
 * @param pid - process id of process being kept alive.
 * @param argc - the number of elements in argument array.
 * @param argv - command and arguments of process being kept alive.
 * @return 0 if everything is okay -1 otherwise.
 */
int logProcess(pid_t pid, int argc, char * argv[]);
/**
 * logRun - helper function that logs the pid and command line
 * arguments.  
 * @param pid - process id of process being kept alive.
 * @param argc - the number of elements in argument array.
 * @param argv - command and arguments of process being kept alive.
 * @return 0 if everything is okay -1 otherwise.
 */
int logRun(pid_t pid, int argc, char * argv[]);

/**
 * logLog - helper function that logs the time, the user, the key 
 * and the command line arguments.  
 * @param argc - the number of elements in argument array.
 * @param argv - command and arguments of process being kept alive.
 * @return 0 if everything is okay -1 otherwise.
 */
int logLog(int argc, char * argv[]);
/**
 * fileExists - returns true if a filename passed as arg exists. 
 * @param filename - the name of the file to be tests.
 * @return true if it exists and false otherwise.
 */
bool fileExists(const char * filename);

//string command;

int main(int argc, char *argv[])
{  
  //pid_t pid = spawn(argv+1);  
  pid_t pid = getpid();
  logProcess(pid,argc-1,argv+1);
#if DEBUG
  cout << "Pid is " << pid << endl; 
#endif
  spawn(argv+1);  
}


void spawn(char * argv[])
{
  int status = execv(argv[0],argv);
  cerr << "Forked Module  exiting ....." << flush << endl;
  cerr << endl << endl;
  exit(status);
  // should never get this far 
}

int logProcess(pid_t pid, int argc, char * argv[])
{
  if ( (logRun(pid, argc, argv)) == 0)
    {
      return logLog(argc, argv);
    }
  else
    {
      return -1;
    }
}

int logRun(pid_t pid, int argc, char * argv[])
{
  stringstream fileName;
  //create file name
  int idx = 0;
  do
    {
      fileName.str("");
      fileName.clear();
      fileName << RUN_DIR << "/" << argv[0] << "." << idx++ << ".pid";
    }
  while(fileExists(fileName.str().c_str()));
    
#if DEBUG
  cout << fileName.str() << endl;
#endif
  ofstream runFile(fileName.str().c_str());
  if (! runFile) // Always test file open
    {
      cerr << "Error opening output file" << endl;
      return -1;
    }
  runFile << pid << flush << endl;
  runFile << argv[0];
  int i;
  for(i = 1; i < argc; i++)
    {
      runFile << " " << argv[i];
    }
  char* pSkynetkey = getenv("SKYNET_KEY");
  char path[PATH_MAX];
  getcwd(path, PATH_MAX);
  runFile << endl << path << endl;;
  runFile << pSkynetkey << flush << endl;
  runFile.close();
  return 0;  
}

int logLog( int argc, char * argv[])
{
  stringstream fileName;
  //create file name
  fileName << LOG_DIR << "/DGCKeepAlive.log";
#if DEBUG
  cout << fileName.str() << endl;
#endif
  ofstream logFile(fileName.str().c_str(), ios::app);
  if (! logFile) // Always test file open
    {
      cerr << "Error opening output file" << endl;
      return -1;
    }
  //get local time
  time_t seconds;
  struct tm * ltime;
  char * dispTime;
  time(&seconds);
  ltime = localtime(&seconds);
  dispTime = asctime(ltime);
  dispTime[strlen(dispTime)-1] = '\0';
  char * user = getlogin();
  char* pSkynetkey = getenv("SKYNET_KEY");
#if DEBUG 
  cout << "Time is " << dispTime << endl;
  cout << "User is " << user << endl;
  cout << "Environment key is" << pSkynetkey << endl;
#endif
  logFile << "[ " << dispTime << " ] " << user << " executed: (";
  logFile << argv[0];
  int i;
  for(i = 1; i < argc; i++)
    {
      logFile << " " << argv[i];
    }
  logFile <<") with $SKYNET_KEY=" << pSkynetkey;
  logFile << flush << endl;
  logFile.close();
  return 0;
}

bool fileExists(const char * filename)
{
  bool flag = false;
  fstream fin;
  fin.open(filename,ios::in);
  if( fin.is_open() )
    {
#if DEBUG
      cout<<"file exists"<<endl;
#endif
      flag=true;
    }
  fin.close();
  return flag;
}
  //need to check that we have args

  //   stringstream tempStream;
  //   int i;
  //   command.clear();
  //   tempStream << argv[1];
  //   for(i = 2; i < argc; i++)
  //     {
  //       tempStream << " " << argv[i];
  //     }
  //   command = tempStream.str();
  //  cout << command << endl;

//   int k = 0;
//   do
//     {
//       cout << environ[k++] << endl;;      
//     }
//   while(environ[k] != NULL);
//   exit(1);

