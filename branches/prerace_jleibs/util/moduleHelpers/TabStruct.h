#define SModuleTabInputDummy2(x) S##x##TabInput
#define SModuleTabInputDummy1(x) SModuleTabInputDummy2(x)
#define SModuleTabInput SModuleTabInputDummy1(TABNAME)

#define SModuleTabOutputDummy2(x) S##x##TabOutput
#define SModuleTabOutputDummy1(x) SModuleTabOutputDummy2(x)
#define SModuleTabOutput SModuleTabOutputDummy1(TABNAME)

#define SNModuleTabInputDummy2(x) SN##x##TabInput
#define SNModuleTabInputDummy1(x) SNModuleTabInputDummy2(x)
#define SNModuleTabInput SNModuleTabInputDummy1(TABNAME)

#define SNModuleTabOutputDummy2(x) SN##x##TabOutput
#define SNModuleTabOutputDummy1(x) SNModuleTabOutputDummy2(x)
#define SNModuleTabOutput SNModuleTabOutputDummy1(TABNAME)

#define SNModulePlotDummy2(x) SN##x##PlotInput
#define SNModulePlotDummy1(x) SNModulePlotDummy2(x)
#define SNModulePlotInput SNModulePlotDummy1(TABNAME)

#define STRUCTELEM(valueType, name, defaultValue, widgetType, xLabel, yLabel, xValue, yValue) \
	valueType name;

#define STRUCTELEMINIT(valueType, name, defaultValue, widgetType, xLabel, yLabel, xValue, yValue) \
	name = defaultValue;

struct SModuleTabInput
{
	TABINPUTS(STRUCTELEM)

	SModuleTabInput()
	{
		TABINPUTS(STRUCTELEMINIT)
	}
} __attribute__((packed));

struct SModuleTabOutput
{
	TABOUTPUTS(STRUCTELEM)

	SModuleTabOutput()
	{
		TABOUTPUTS(STRUCTELEMINIT)
	}
} __attribute__((packed));
