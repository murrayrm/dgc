/*
 *  Gazebo - Outdoor Multi-Robot Simulator
 *  Copyright (C) 2003  
 *     Nate Koenig & Andrew Howard
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/* Desc: Model for a SickLMS200
 * Author: Andrew Howard
 * Date: 8 May 2003
 * CVS: $Id: SickLMS200.cc,v 1.39 2004/06/01 17:12:50 natepak Exp $
 */

#include <assert.h>
#include <math.h>

#include "gazebo.h"
#include "World.hh"
#include "WorldFile.hh"
#include "RayGeom.hh"
#include "BoxGeom.hh"
#include "CylinderGeom.hh"
#include "SickLMS200.hh"



//////////////////////////////////////////////////////////////////////////////
// Creation function
Model *NewSickLMS200( World *world )
{
  return new SickLMS200(world);
}


//////////////////////////////////////////////////////////////////////////////
// Constructor
SickLMS200::SickLMS200( World *world )
    : Model( world )
{
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Destructor
SickLMS200::~SickLMS200()
{
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Load the model
int SickLMS200::Load( WorldFile *file, WorldFileNode *node )
{
  
  // Number of range readings
  this->rangeCount = node->GetInt("rangeCount", 361);

  // Number of simulated  rays
  this->rayCount = node->GetInt("rayCount", 91);
  
  this->laserPeriod = node->GetTime("scanPeriod", 1.000);
  this->laserMaxRange = node->GetLength("maxRange", 8.192);
  this->laserMinRange = node->GetLength("minRange", 0.20);
  this->scanAngle = (110*M_PI/180.0);
  // Create the ODE objects
  if (this->OdeLoad( file, node ) != 0)
    return -1;
 
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Load ODE objects
int SickLMS200::OdeLoad( WorldFile *file, WorldFileNode *node )
{
  dReal length, width, height, mass;
  
  length = 0.15;
  width = 0.15;
  height = 0.20;
  mass = 7.0;

  this->body = new Body( this->world );

  this->bodyGeoms[0] = new BoxGeom( this->modelSpaceId, length, width, 
                                    0.2 * height );
  this->bodyGeoms[0]->SetRelativePosition(GzVectorSet(0, 0, (-0.5 + 0.2 / 2) * height) );
  this->bodyGeoms[0]->SetMass( mass / 3 );
  this->bodyGeoms[0]->SetColor( GzColor(0, 0, 1) );
  this->body->AttachGeom( this->bodyGeoms[0] );
  
  this->bodyGeoms[1] = new BoxGeom( this->modelSpaceId, length, width, 
                                    0.4 * height );
  this->bodyGeoms[1]->SetRelativePosition(GzVectorSet(0, 0, (+0.5 - 0.4 / 2) * height));
  this->bodyGeoms[1]->SetMass( mass / 3 );
  this->bodyGeoms[1]->SetColor( GzColor(0, 0, 1) );
  this->body->AttachGeom( this->bodyGeoms[1] );
  
  this->bodyGeoms[2] = new BoxGeom( this->modelSpaceId, length / 2, width, 
                                    0.9 * height );
  this->bodyGeoms[2]->SetRelativePosition(GzVectorSet( -0.25 * length, 0, 0));
  this->bodyGeoms[2]->SetMass( mass / 3 );
  this->bodyGeoms[2]->SetColor( GzColor(0, 0, 1) );
  this->body->AttachGeom( this->bodyGeoms[2] );
  
  this->bodyGeoms[3] = new CylinderGeom( this->modelSpaceId, length / 2, 
                                         0.9 * height );
  this->bodyGeoms[3]->SetRelativePosition( GzVectorSet(0, 0, 0) );
  this->bodyGeoms[3]->SetMass( 0 );
  this->bodyGeoms[3]->SetColor( GzColor(0, 0, 0) );
  this->body->AttachGeom( this->bodyGeoms[3] );

  if (this->RayInit() != 0)
    return -1;
 
  this->AddBody( this->body, true );
    
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize the model
int SickLMS200::Init( WorldFile *file, WorldFileNode *node )
{  
  // Initialize ODE objects
  if (this->OdeInit(file, node) != 0)
    return -1;

  // Initialize the rays
 
  // Initialize external interface
  if (this->IfaceInit() != 0)
    return -1;

  this->laserTime = this->world->GetSimTime();
  
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize ODE
int SickLMS200::OdeInit( WorldFile *file, WorldFileNode *node )
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize rays
int SickLMS200::RayInit()
{
  int i;
  double angle;
  GzVector pos, axis;  

  // Create a space to contain the ray space
  this->superSpaceId = dSimpleSpaceCreate( 0 );
    
  // Create a space to contain all the rays
  this->raySpaceId = dSimpleSpaceCreate( this->superSpaceId );
  
  dGeomSetCategoryBits((dGeomID) this->raySpaceId, GZ_LASER_COLLIDE);
  dGeomSetCollideBits((dGeomID) this->raySpaceId, ~GZ_LASER_COLLIDE);
  
  this->rays = new RayGeom* [this->rayCount];

  // Create the laser rays
  for (i = 0; i < this->rayCount; i++)
  {
    angle = i * scanAngle / (this->rayCount - 1) - scanAngle / 2;
        
    // Compute the ray axis
    axis = GzVectorSet( cos( angle ), sin( angle ), 0 );

    // Compute the ray start position (offset from body origin so the
    // rays dont intersect with the body).  This real laser also has a
    // minimum range value.
    pos.x = this->laserMinRange * axis.x;
    pos.y = this->laserMinRange * axis.y;
    pos.z = this->laserMinRange * axis.z;

    this->rays[i] = new RayGeom( this->raySpaceId, this->laserMaxRange - this->laserMinRange );
      
    this->rays[i]->Set( pos, axis );
    this->rays[i]->SetColor( GzColor(0, 0, 1) );
    this->rays[i]->SetCategoryBits( GZ_LASER_COLLIDE );
    this->rays[i]->SetCollideBits( ~GZ_LASER_COLLIDE );
    this->rays[i]->SetCameraBits( GZ_LASER_CAMERA );

    this->body->AttachGeom( this->rays[i] );
  }

  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Initialize the external interface
int SickLMS200::IfaceInit()
{
  this->laser_iface = gz_laser_alloc();
  if (gz_laser_create( this->laser_iface, this->world->gz_server, this->GetId() ) != 0)
    return -1;

  this->fiducial_iface = gz_fiducial_alloc();
  if (gz_fiducial_create( this->fiducial_iface, this->world->gz_server, this->GetId() ) != 0)
    return -1;

  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize the model
int SickLMS200::Fini()
{
  // Finalize external interface  
  this->IfaceFini();

  // Finalize ODE objects
  this->OdeFini();

  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize ODE
int SickLMS200::OdeFini()
{
  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Finalize the external interface
int SickLMS200::IfaceFini()
{
  gz_fiducial_destroy( this->fiducial_iface );
  gz_fiducial_free( this->fiducial_iface );
  this->fiducial_iface = NULL;
    
  gz_laser_destroy( this->laser_iface );
  gz_laser_free( this->laser_iface );
  this->laser_iface = NULL;

  return 0;
}


//////////////////////////////////////////////////////////////////////////////
// Update the model
void SickLMS200::Update( double step )
{
  int i;
  GzVector body_pos;
  GzQuatern body_rot;
  GzVector pos, axis;
  double angle;
  RayGeom *ray;

  // Update the laser periodically
  if (this->world->GetSimTime() - this->laserTime < this->laserPeriod)
    return;
  this->laserTime = this->world->GetSimTime();

  // Get the position and orientation of the laser body
  body_pos = this->body->GetPosition();
  body_rot = this->body->GetRotation();

  // Reset the ray length
  for (i = 0; i < this->rayCount; i++)
  {
    ray = this->rays[i];
    ray->SetLength( this->laserMaxRange - this->laserMinRange );
    ray->retro = 0.0;
    ray->fiducial = -1;
  }

  //printf("do collide\n");

  // Do the collision detection
  dSpaceCollide2( (dGeomID) (this->superSpaceId),
                  (dGeomID) (this->world->GetSpaceId()),
                  this, &UpdateCallback );
  
  // Update the mmap interface with the new data
  IfacePutData();
  
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Callback for ray intersection test
void SickLMS200::UpdateCallback( void *data, dGeomID o1, dGeomID o2 )
{
  SickLMS200 *self;

  self = (SickLMS200*) data;

  // If either of the geoms is the ray space, test the spaces
  if (dGeomIsSpace( o1 ) || dGeomIsSpace(o2))
  {
    //printf("%p %p : %p\n", o1, o2, self->raySpaceId);
    
    if (dGeomGetSpace(o1) == self->superSpaceId || dGeomGetSpace(o2) == self->superSpaceId)
    {
      //printf("ray space collide\n");
      dSpaceCollide2( o1, o2, self, &UpdateCallback );
    }
    if (dGeomGetSpace(o1) == self->raySpaceId || dGeomGetSpace(o2) == self->raySpaceId)
    {
      //printf("geom space collide\n");
      dSpaceCollide2( o1, o2, self, &UpdateCallback );
    }
  }
  else
  {
    // They should never be in the same space
    assert(dGeomGetSpace(o1) != dGeomGetSpace(o2));

    RayGeom* ray = NULL;
    Geom *geom = NULL;
    
    // See if it is a ray (it could be a transform geom)
    if (dGeomGetClass(o1) == dGeomTransformClass)
    {
      //printf("o1 geom class %p %d %d\n",
      //       o1, dGeomGetClass(o1), dGeomGetClass(dGeomTransformGetGeom(o1)));

      if (dGeomTransformGetGeom(o1))
      {
        if (dGeomGetClass(dGeomTransformGetGeom(o1)) == dRayClass)
        {
          ray = (RayGeom*)(dGeomGetData(dGeomTransformGetGeom(o1)));
          assert(dGeomGetSpace(o1) == self->raySpaceId);

          if (dGeomTransformGetGeom(o2))
            geom = (Geom*) (dGeomGetData(dGeomTransformGetGeom(o2)));
        }
      }
    }
    if (dGeomGetClass(o2) == dGeomTransformClass)
    {
      //printf("o2 geom class %p %d %d\n",
      //       o2, dGeomGetClass(o2), dGeomGetClass(dGeomTransformGetGeom(o2)));

      if (dGeomTransformGetGeom(o2))
      {
        if (dGeomGetClass(dGeomTransformGetGeom(o2)) == dRayClass)
        {
          ray = (RayGeom*)(dGeomGetData(dGeomTransformGetGeom(o2)));
          assert(dGeomGetSpace(o2) == self->raySpaceId);

          if (dGeomTransformGetGeom(o1))
            geom = (Geom*) (dGeomGetData(dGeomTransformGetGeom(o1)));
        }
      }
    }
    
    if (ray)
    {
      int n;
      dContactGeom contact[1];
      n = dCollide( o1, o2, 1, &contact[0], sizeof(dContact) );

      //if (n > 0)
      //  printf("ray contact %f\n", contact[0].depth);
      
      if (n > 0 && contact[0].depth < ray->GetLength())
      {
        if (geom != NULL && geom->GetLaser() != 0)
        {
          ray->SetLength( contact[0].depth );
          if (geom != NULL)
          {
            ray->retro = geom->GetRetro();
            ray->fiducial =  geom->GetFiducial();
          }
          else
          {
            ray->retro = 0.0;
            ray->fiducial = -1;
          }
        }
      }
    }
  }
  return;
}


//////////////////////////////////////////////////////////////////////////////
// Update the data in the interface
void SickLMS200::IfacePutData()
{
  int i, j, ja, jb, count;
  RayGeom *ray, *raya, *rayb;
  gz_fiducial_fid_t *fid;
  double r, b;
  int v;
  double ax, ay, bx, by, cx, cy;

  gz_laser_lock(this->laser_iface, 1);

  // Data timestamp
  this->laser_iface->data->time = this->world->GetSimTime();
    
  // Read out the laser range data
  this->laser_iface->data->min_angle = -scanAngle / 2;
  this->laser_iface->data->max_angle = +scanAngle / 2;
  this->laser_iface->data->res_angle = scanAngle / (this->rangeCount - 1);
  this->laser_iface->data->max_range = this->laserMaxRange;
  this->laser_iface->data->range_count = this->rangeCount;

  assert( this->laser_iface->data->range_count < GZ_LASER_MAX_RANGES );

  // Interpolate the range readings from the rays
  for (i = 0; i < this->rangeCount; i++)
  {
    b = (double) i * (this->rayCount - 1) / (this->rangeCount - 1);
    ja = (int) floor(b);
    jb = Min(ja + 1, this->rayCount - 1);    
    b = b - floor(b);

    assert(ja >= 0 && ja < this->rayCount);
    assert(jb >= 0 && jb < this->rayCount);
    
    raya = this->rays[ja];
    rayb = this->rays[jb];

    r = rayb->GetLength() - raya->GetLength();

    // Range is linear interpolation if values are close,
    // and min if they are very different
    if (fabs(r) < 0.10)
      r = (1 - b) * raya->GetLength() + b * rayb->GetLength();
    else
      r = Min(raya->GetLength(), rayb->GetLength());

    // Intensity is either-or
    v = (int) raya->retro || (int) rayb->retro;

    assert(i < GZ_LASER_MAX_RANGES);
    this->laser_iface->data->ranges[i] = this->laserMinRange + r;
    this->laser_iface->data->intensity[i] = v;
  }

  gz_laser_unlock(this->laser_iface);


  gz_fiducial_lock(this->fiducial_iface, 1);

  // Data timestamp
  this->fiducial_iface->data->time = this->world->GetSimTime();
  this->fiducial_iface->data->fid_count = 0;

  // TODO: clean this up
  count = 0;
  for (i = 0; i < this->rayCount; i++)
  {
    ray = this->rays[i];
    if (ray->fiducial < 0)
      continue;

    // Find the end of the fiducial
    for (j = i + 1; j < this->rayCount; j++)
    {
      ray = this->rays[j];
      if (ray->fiducial < 0)
        break;
    }
    j--;

    // Need at least three points to get orientation
    if (j - i + 1 >= 3)
    {
      ray = this->rays[i];
      r = this->laserMinRange + ray->GetLength();
      b = -scanAngle / 2 + i * scanAngle / (this->rayCount - 1);
      ax = r * cos(b);
      ay = r * sin(b);

      ray = this->rays[j];
      r = this->laserMinRange + ray->GetLength();
      b = -scanAngle / 2 + j * scanAngle / (this->rayCount - 1);
      bx = r * cos(b);
      by = r * sin(b);

      cx = (ax + bx) / 2;
      cy = (ay + by) / 2;
      
      assert(count < GZ_FIDUCIAL_MAX_FIDS);
      fid = this->fiducial_iface->data->fids + count++;

      fid->id = ray->fiducial;
      fid->pose[0] = sqrt(cx * cx + cy * cy);
      fid->pose[1] = atan2(cy, cx);
      fid->pose[2] = atan2(by - ay, bx - ax) + M_PI / 2;
    }

    // Fewer points get no orientation
    else
    {
      ray = this->rays[i];
      r = this->laserMinRange + ray->GetLength();
      b = -scanAngle / 2 + i * scanAngle / (this->rayCount - 1);
      ax = r * cos(b);
      ay = r * sin(b);

      ray = this->rays[j];
      r = this->laserMinRange + ray->GetLength();
      b = -scanAngle / 2 + j * scanAngle / (this->rayCount - 1);
      bx = r * cos(b);
      by = r * sin(b);

      cx = (ax + bx) / 2;
      cy = (ay + by) / 2;
      
      assert(count < GZ_FIDUCIAL_MAX_FIDS);
      fid = this->fiducial_iface->data->fids + count++;

      fid->id = ray->fiducial;
      fid->pose[0] = sqrt(cx * cx + cy * cy);
      fid->pose[1] = atan2(cy, cx);
      fid->pose[2] = atan2(cy, cx) + M_PI;
    }

    i = j;
    
    //printf("fiducial %d %.2f %.2f %.2f\n",
    //       fid->id, fid->pose[0], fid->pose[1], fid->pose[2]);
  }

  this->fiducial_iface->data->fid_count = count;
  
  gz_fiducial_unlock(this->fiducial_iface);
  
  return;
}


