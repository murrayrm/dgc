/**
 * StereoClient.hh
 * Player client for stereo cameras
 *
 * Revision History:
 * 02/22/05 hbarnor,ryanf created
 */

#ifndef STEREOCLIENT_HH
#define STEREOCLIENT_HH

/** the name of the config file containing custom host and port */
#define CONFIG_FILENAME "config"
/** the host to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_HOST "localhost"
/** the port to connect to the Player Server at (if no config file found) */
#define PLAYER_GAZEBO_PORT 6665



#include "stereovision/stereoSource.hh"
#include "playerclient.h"
#include <opencv/cv.h>
#include <opencv/cvaux.h>
#include <opencv/highgui.h>
#include <string>
#include <fstream>
#include <iostream>


using namespace std;

/** 
 * The data mode we use for player communication 
 * We will pull new data when we need it. This is 
 * synonymous with the way the real hardware works. 
 */
const unsigned char DATA_MODE = PLAYER_DATAMODE_PULL_ALL;
const int BIT_DEPTH = 3;

class StereoClient : public stereoSource
{
  public:
  /**
   * Constructor
   */
  StereoClient();
  /**
   * Destructor - for memory management
   */
  ~StereoClient();
  /**
   * This method is not transparent to terrain
   * Will connect player server based on config file.
   * localhost if no config file exists.
   * will subscribe to leftCamera and rightCamera
   * these will have to be defined in the world file
   */   
  int init();
  /**
   * Overrides default stop method 
   * Will cleanly disconnect from player
   *
   */
  int stop();
  /**
   * Get a left and right image 
   * from gazebo
   */
  int grab();

protected:
  /**
   * readConfig - if config file exist
   * read info on host riunning player
   * server
   */
  void readConfig();
  /** Tries to connect to the Player server until it succeedes */
  void connect();
  /** Disconnects the player client */
  void disconnect();
  /* Subscribes to the different devices, you must run connect() first */
  void subscribe();
  /* Unsubscribes the different devices, good to do before disconnect() */
  void unsubscribe();
private:
  /**
   * Our client instance
   */
  PlayerClient *alice; 
  /**
   * Our left camera
   */
  CameraProxy *leftCamera;
    /**
   * Our right camera
   */
  CameraProxy *rightCamera;
  /**
   * the host running the player server
   */
  string host;
  /**
   * the port running the player
   * server
   */
  int port;

  /**
   * The OpenCV image format to store
   * Might be better to make the declaration 
   * in the super class protected so that we have 
   * access to it
   */
  IplImage* _images[MAX_CAMERAS];
  
};

#endif
