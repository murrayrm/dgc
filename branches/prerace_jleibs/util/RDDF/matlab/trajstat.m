## plots stats on trajs
## usage trajstat('filename');

function trajstat(filename)
  traj = load(filename);
  N = 1:size(traj,1);
  vel = (traj(:,2).^2 + traj(:,5).^2) .^ .5;
  norm_vel = [traj(:,2) ./ vel, traj(:,5) ./ vel];
  accel = [traj(:,3), traj(:,6)];
  accel_long = norm_vel .* [dot(norm_vel, accel, 2) dot(norm_vel, accel, 2)];
  accel_long_mag = (accel_long(:,1).^2 + accel_long(:,2).^2) .^ .5;
  accel_lat = accel - accel_long;
  accel_lat_mag = (accel_lat(:,1).^2 + accel_lat(:,2).^2) .^ .5;
  plot(N, vel,
       N, accel_long_mag,
       N, accel_lat_mag);
  legend('vel', 'accel_long', 'accel_lat');
endfunction
