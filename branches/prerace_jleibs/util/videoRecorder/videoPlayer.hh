#ifndef VIDEOPLAYER_HH
#define VIDEOPLAYER_HH

#include "CTimberClient.hh"
#include "DGCutils"
#include "smart_counter.hh"
#include <cv.h>

#include "pid.hh"

class SDL_Surface;   //Forward declaration

class CVideoPlayer : public CTimberClient
{
 public:
  CVideoPlayer(int skynet_key, bool sparrow);
  ~CVideoPlayer();
  
 public:
  /*
  **
  ** Playback
  **
  */
  void playback(bool threaded=false);

  /*
  **
  ** Accessors
  **
  */
  bool terminated() const;

  double displayFramerate() const;

  long long bytesRead() const;
  double bytesReadRate() const;

 protected:
  /*
  **
  ** Helper functions
  **
  */
  void setDisplay(bool show);

  //Thread reading from camera, exposure controlling and saving to file
  void playerThread();
  void displayImage(IplImage *image);

 protected:
  /*
  **
  ** Data members
  **
  */

  //General data
  pthread_mutex_t m_mutex;
  bool m_terminate;

  //Log data
  smart_counter<long long> m_bytes_read;
  FILE *    m_log_index_file;

  //Display data
  bool m_display_inited;
  SDL_Surface *m_screen;
  SDL_Surface *m_surf;

  //Framerate & framerate counters
  smart_counter<int> m_display_cnt;

 protected:
  /*
  **
  ** Sparrow interface functions
  **
  */
  void set_read_bytes();
  void set_read_rate();
};

#endif // VIDEOPLAYER_HH
