#! /bin/sh

# This script runs rddfPrep to add our current position to the beginning
# of the RDDF, and then copies the modified rddf to all the race machines

COMPUTERS="skynet1 skynet2 skynet3 skynet5 skynet6 skynet7 sitka"
SSHFROM="skynet7"
BINDIR="/race/bin/"
RACE_USER="race"

EXEC_NAME="./rddfPrep"
RDDF="rddf.dat"

THISCOMP=`hostname`

if test ! -x $EXEC_NAME; then
    echo "*** Error:  executable $EXEC_NAME not found."
    exit 1;
fi

if test "$THISCOMP" != "$SSHFROM"; then
    echo "*** Error:  this script must be run from $SSHFROM"
    exit 1;
fi

echo
echo "Running $EXEC_NAME"
echo
$EXEC_NAME
echo
echo "done."
echo

for comp in $COMPUTERS; do
    if test "$comp" = "$SSHFROM"; then
	continue
    fi
    echo -n "Copying $RDDF to $comp:$BINDIR ... "
    scp $RDDF $RACE_USER@$comp:$BINDIR$RDDF
    echo "done."
    echo -ne "Verifying that remote RDDF matches ... "
    failed=1
    scp $RACE_USER@$comp:$BINDIR$RDDF ./$RDDF.$comp && `diff $RDDF $RDDF.$comp` 1>/dev/null 2>/dev/null && failed=0
    if test $failed -eq 0; then
	echo -e "\033[01;32mremote RDDF matches.\033[00;00m"
    else
	echo -e "\033[01;31mRDDF distribution to $comp failed!\033[00;00m"
    fi
    rm $RDDF.$comp
    echo

done

