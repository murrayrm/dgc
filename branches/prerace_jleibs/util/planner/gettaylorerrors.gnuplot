plot [0:pi/2] cos(x+dth/2) - ( cos(x)-sin(x)*(dth/2)-cos(x)*(dth/2)**2.0/2.0 )
gnuplot> plot [0 : 0.117257] 1.0-x/2.0-x**2.0/8.0-x**3.0/16.0-5.0*x**4/128.0, sqrt(1-x)
gnuplot> plot [0 : 0.117257] 1.0-x/2.0-x**2.0/8.0-x**3.0/16.0-5.0*x**4/128.0 - sqrt(1-x) 
gnuplot> plot [0 : 0.117257] 1.0-x/2.0-x**2.0/8.0-x**3.0/16.0 - sqrt(1-x)               
gnuplot> plot [0 : 0.117257] 1.0-x/2.0-x**2.0/8.0 - sqrt(1-x)            
gnuplot> plot [0 : 0.117257] 1.0-x/2.0-x**2.0/8.0-x**3.0/16.0-5.0*x**4/128.0 - sqrt(1-x)
gnuplot> plot [0 : 0.117257] 1.0-x/2.0-x**2.0/8.0-x**3.0/16.0-5.0*x**4/128.0 - sqrt(1-x)
gnuplot> plot [0 : 0.117257] 1.0-x/2.0-x**2.0/8.0-x**3.0/16.0 - sqrt(1-x)               
gnuplot> plot [0 : 0.117257] 1.0-x/2.0-x**2.0/8.0 - sqrt(1-x)            
gnuplot> plot [0 : 0.117257] 1.0-x/2.0 - sqrt(1-x)           
gnuplot> plot [-0.3424:0.3424] 1.0 - x**2.0/2.0 - x**4.0/8.0- sqrt(1-x*x)               
gnuplot> plot [-0.3424:0.3424] 1.0 - x**2.0/2.0 - x**4.0/8.0- sqrt(1-x*x) 
gnuplot> plot [-0.3424:0.3424] 1.0 - x**2.0/2.0 - x**4.0/8.0 - x**6.0/16.0 - sqrt(1-x*x)  
gnuplot> plot [0 : 0.117257] 1.0-x/2.0-x**2.0/8.0-x**3.0/16.0 - sqrt(1-x)                


sqrt( (-cos(0.05)+1)^2.0 + (sin(0.05) )^2.0)
sqrt( (-cos(0.05)+1)^2.0 + (sin(0.05) - 0.05 )^2.0)
sqrt( (-cos(0.05)+1- (0.05^2)/2 )^2.0 + (sin(0.05) - 0.05 )^2.0)
sqrt( (-cos(0.05)+1- (0.05^2)/2 )^2.0 + (sin(0.05) - 0.05 + (0.05^3)/6)^2.0)
