//This is the strategy interface class .cc file
/*
 * BEWARE!! - THESE HEADER COMMENTS ARE MOSTLY DEPRECATED AND WERE ESSENTIALLY
 * NOTES-TO-SELF DURING CONSTRUCTION - A PROPER (CORRECT) HEADER WILL BE
 * APPEARING SHORTLY
 *
 * NOTE - this class should only ever have ONE instance 
 * Things to go in here:
 * All stage methods that are used by ANY strategy and require contact with
 * the outside world (i.e. use skynet messages) - all these methods should
 * be PUBLIC - so that they can be accessed by all of the strategies witout
 * them having to deriive from the class - necessary as otherwise they will
 * call the default constructor when they are constructed, and there should
 * only be one instance.
 * 
 * set-up of all skynet socket message handling (note that the sockets are
 * all called (only) by method which exist in this interface class - all of the
 * sockets need to be set-up in the interface class constructor (called by
 * the Diagnostic class) 
 * 
 * interface class owns the map (and the diagnostic table creates the instance
 * of the interface class
 * 
 * need to have a public method in Diagnostic that returns a pointer to the map
 * instance, and this method is called by the methods that are in the
 * interface class which access the map, note that these methods must be public
 * methods so that they can be accesed by any of the specific strategies
 *
 * need to have a method that updates the vehicle position in the map - this
 * will need to be a public function in the interface class that is the front end
 * for the vehicle location update method from CMap.
 * 
 * 
 * Relationship to other classes:
 * Diagnostic --> constructs & 'owns' the sole instance of this class
 *                and handles all map updates (through PUBLIC methods in the
 *                interface class
 * GenericStrategy --> No relationship
 * SpecificStrategy --> uses the shared stage methods (all PUBLIC) - does NOT
 *                      derive from the interface class (as the interface class
 *                      handles all of the map set-up and creation, and owns
 *                      the map)
 */

#include "StrategyInterface.hh"
#include "enumstring.h"

/** CONSTRUCTORS **/

//Default no argument constructor
CStrategyInterface::CStrategyInterface( const int skynetKey, SCstate *p_SCstate, SCdiagnostic *p_SCDiagnostic, CDiagnostic *p_CDiagnostic, bool usrSpef_silentOptionSelected )
  : CSkynetContainer(MODsupercon, skynetKey), CMapdeltaTalker()
{
  
  /* STRATEGY SKYNET MSG. SOCKET INITIALISATION */
  //Initialise sockets for all message types SENT by strategy instances
  //using the common interface methods in the CStrategyInterface class  

  /** LIST OF ALL SKYNET MESSAGE TYPES SENT BY THIS CLASS **/
  sn_msg msgs[7]= { SNdrivecmd,
                    SNsuperconPlnCmd,
                    SNsuperConMapAction,
                    SNsuperconTrajfCmd,
                    SNsuperconAstateCmd,
                    SNtrajFspeedCapCmd,
                    SNsuperConMapDelta };

  cout << "SIZE of msg: " << sizeof(msgs) << endl;

  for(int i =0; i< ((sizeof(msgs) ) / sizeof(sn_msg) ); ++i) {
    StrategySNsocketList[ msgs[i] ] = m_skynet.get_send_sock( msgs[i] );
    //SuperConLog().log( SIF, WARNING_MSG, "SN socket for: %s = %i", sn_msg_asString( msgs[i] ), StrategySNsocketList[ msgs[i] ] );
    cout << "SN socket for: " << sn_msg_asString( msgs[i] ) << "= " << StrategySNsocketList[ msgs[i] ] << endl;
  }

  /* SUPERCON MAP INITIALISATION */
  superConMap.initMap( CONFIG_FILE_DEFAULT_MAP );
  deltaPtr = NULL;
  //Add the superCon layer to the superCon instance of the map
  layerID_superCon = superConMap.addLayer<double>( CONFIG_FILE_SUPERCON, true );
  //Mutex for the superCon map
  DGCcreateMutex(&superConMapMutex);
  
  //Update the private member silentOptionSelected flag
  silentOptionSelected = usrSpef_silentOptionSelected;

  m_pSCstate = p_SCstate;
  m_pSCDiagnostic = p_SCDiagnostic;
  m_pCDiagnostic = p_CDiagnostic;
}


/** DESTRUCTOR **/
CStrategyInterface::~CStrategyInterface() {
  DGCdeleteMutex(&superConMapMutex);
}


/** HELPER METHODS **/

//NOTE - you will have to add any new skynet message types used into the switch
//statement in the sendSkynetMsg(...) method detailing how the message should be
//logged (TIMBER OR SKYNET) if you want them to be logged (iff log-level = OUTPUT_SN_MSGS)
void CStrategyInterface::sendSkynetMsg( sn_msg msg_type, void* msg_pointer, size_t msg_size ) {
  
  if( silentOptionSelected == false ) {    
    //SEND skynet messages (logging dependent upon logging config)
    //get an interator to the socket # for the message type of the message to be sent
    msg_itr = StrategySNsocketList.find( msg_type );
    m_skynet.send_msg(msg_itr->second, msg_pointer, msg_size, 0); //last arg not used
  }

  switch( msg_type ) {
    
  case SNdrivecmd:
    {
      //recast the void* to a pointer to a data struct of the type passed in
      //this skynet message type
      drivecmd_t* temp_msg_pointer; //used so that switch() statement isn't repeated    
      temp_msg_pointer = (drivecmd_t*) msg_pointer;
      SuperConLog().log( SIF, OUTPUT_SN_MSGS, "SNMSG: %s - Actuator: %s, Command: %s", sn_msg_asString(msg_type),  adrive_actuator_type_enum_t_asString( temp_msg_pointer->my_actuator ), adrive_command_type_enum_t_asString( temp_msg_pointer->my_command_type ), temp_msg_pointer->number_arg );      
    }
    break;
    
  case SNsuperconTrajfCmd:
    {
      //recast the void* to a pointer to a data struct of the type passed in
      //this skynet message type
      superConTrajFcmd* temp_msg_pointer; //used so that switch() statement isn't repeated    
      temp_msg_pointer = (superConTrajFcmd*) msg_pointer;
      SuperConLog().log( SIF, OUTPUT_SN_MSGS, "SNMSG: %s - Command Type: %s, Distance to reverse: %i", sn_msg_asString(msg_type), trajFmode_asString( temp_msg_pointer->commandType ), temp_msg_pointer->distanceToReverse );      
    }
    break;
      
  case SNtrajFspeedCapCmd:
    {
      //recast the void* to a pointer to a data struct of the type passed in
      //this skynet message type
      newSpeedCapCmd* temp_msg_pointer; //used so that switch() statement isn't repeated    
      temp_msg_pointer = (newSpeedCapCmd*) msg_pointer;
      SuperConLog().log( SIF, OUTPUT_SN_MSGS, "SNMSG: %s - SpeedCap action: %s, SpeedCap argument: %lf", sn_msg_asString(msg_type), speedCapAction_asString( temp_msg_pointer->m_speedCapAction ), temp_msg_pointer->m_speedCapArgument );      
    }
    break;

  case SNsuperconAstateCmd:
    {
      //recast the void* to a pointer to a data struct of the type passed in
      //this skynet message type
      superconAstateCmd* temp_msg_pointer; //used so that switch() statement isn't repeated    
      temp_msg_pointer = (superconAstateCmd*) msg_pointer;
      SuperConLog().log( SIF, OUTPUT_SN_MSGS, "SNMSG: %s - Astate Command action: %s", sn_msg_asString(msg_type), SC_astateCmdType_asString( temp_msg_pointer->commandType ) );      
    }
    break;
    
  case SNsuperConMapAction:
    {
      //recast the void* to a pointer to a data struct of the type passed in
      //this skynet message type
      superCon_map_action* temp_msg_pointer; //used so that switch() statement isn't repeated    
      temp_msg_pointer = (superCon_map_action*) msg_pointer;
      SuperConLog().log( SIF, OUTPUT_SN_MSGS, "SNMSG: %s - fusionMapper command: %s", sn_msg_asString(msg_type), mapActionCmdType_asString( temp_msg_pointer->mapActionCmd ) );      
    }
    break;

  default:
    {
      SuperConLog().log( SIF, ERROR_MSG, "SNMSG ERROR: msg. type %s NOT present in sendSkynetMsg(...) switch() statement", sn_msg_asString(msg_type) );
    }
  }//NOTE - Map deltas are handled separately
}


/** MUTATOR METHODS **/

//updates the position of the map (relative to the global frame) using
//the current vehicle position - vehicle remains at the center of the map
void CStrategyInterface::updateVehPosInSuperconMap(const SCstate &m_SCstate) {

  //LOCK the superCon map and Update the SuperCon map so that it is
  //centered on the vehicle's current location (stored in the
  //m_SCstate struct) PRIOR to stepping forward in the current strategy,
  //as strategies can update the map - UNLOCK the map once finished
  DGClockMutex(&superConMapMutex);
  superConMap.updateVehicleLoc(m_SCstate.northing, m_SCstate.easting);
  DGCunlockMutex(&superConMapMutex);
}


/** COMMON-STAGE METHODS - shared by all strategies **/

//Send superCon e-stop signal to adrive of type dictated by eStopType
void CStrategyInterface::stage_superConEstop( const adriveEstpCmd eStopType ) {

  //adrive command struct (initialise the command to zero
  //to appease the memory allocation gods)
  drivecmd_t superCon_adriveCmd;
  memset( &superCon_adriveCmd, 0, sizeof( superCon_adriveCmd ) );

  //actuate the e-stop
  superCon_adriveCmd.my_actuator = estop;
  superCon_adriveCmd.my_command_type = set_position;
  superCon_adriveCmd.number_arg = eStopType; 

  if( eStopType == sc_interface::estp_run || eStopType == sc_interface::estp_pause ) {
    sendSkynetMsg( SNdrivecmd, &superCon_adriveCmd, sizeof(superCon_adriveCmd) );
    SuperConLog().log( SIF, STRATEGY_STAGE_MSG, "sending superCon e-stop: %s command to adrive", adriveEstpCmd_asString(eStopType) );
  } else {
    cerr <<"CStrategyInterface::stage_superConEstop - Invalid e-stop command type :" << eStopType << endl;
    SuperConLog().log( SIF, ERROR_MSG, "CStrategyInterface::stage_superConEstop - Invalid e-stop command type: %s (remember that superCon is NOT allowed to send disables", adriveEstpCmd_asString(eStopType) );
  }
}

//Send a command to adrive to change gear
void CStrategyInterface::stage_changeGear( const adriveTransCmd desiredGear ) {
  
  //adrive command struct (initialise the command to zero
  //to appease the memory allocation gods)
  drivecmd_t superCon_adriveCmd;
  memset( &superCon_adriveCmd, 0, sizeof( superCon_adriveCmd ) );
  
  //actuate the transmission
  superCon_adriveCmd.my_actuator = trans;
  superCon_adriveCmd.my_command_type = set_position;
  superCon_adriveCmd.number_arg = double(desiredGear);
  
  SuperConLog().log( SIF, DEBUG_MSGS, "Actuator: %s \n", adrive_actuator_type_enum_t_asString( superCon_adriveCmd.my_actuator ) );
  SuperConLog().log( SIF, DEBUG_MSGS, "command type: %s \n", adrive_command_type_enum_t_asString( superCon_adriveCmd.my_command_type ) );
  SuperConLog().log( SIF, DEBUG_MSGS, "number arg: %lf \n", superCon_adriveCmd.number_arg );
  SuperConLog().log( SIF, DEBUG_MSGS, "desired gear: %s \n", adriveTransCmd_asString(desiredGear) );
  //cout << "desired gear: " << desiredGear << "number_arg: " << superCon_adriveCmd.number_arg << endl;
  
  if (desiredGear == sc_interface::drive_gear || desiredGear == sc_interface::park_gear || desiredGear == sc_interface::reverse_gear ) {
    sendSkynetMsg( SNdrivecmd, &superCon_adriveCmd, sizeof(superCon_adriveCmd) );
    SuperConLog().log( SIF, STRATEGY_STAGE_MSG, "sending transmission command: %s to adrive", adriveTransCmd_asString(desiredGear) );
  } else {
    cerr << "ERROR: CStrategyInterface::stage_changeGear you muppet! - Invalid transmission type: "<< desiredGear << endl;
    SuperConLog().log( SIF, ERROR_MSG, "ERROR: CStrategyInterface::stage_changeGear you muppet! - Invalid transmission type: %i", desiredGear );
  } 
}


void CStrategyInterface::stage_changeTFmode( const trajFmode desiredMode, const double distanceToReverse_meters ) {
  
  //Set-up the command message
  superConTrajFcmd newCommand;
  newCommand.commandType = desiredMode;
  newCommand.distanceToReverse = distanceToReverse_meters;
  
  if( desiredMode == tf_forwards || desiredMode == tf_reverse ) {
    sendSkynetMsg( SNsuperconTrajfCmd, &newCommand, sizeof(newCommand) );
    SuperConLog().log( SIF, STRATEGY_STAGE_MSG, "Changing trajFollower mode to: %s", trajFmode_asString(desiredMode));
  } else {
    cerr << "ERROR: CStrategyInterface::stage_changeTFmode you muppet! you entereed an INVALID mode: "<< desiredMode << endl;
    SuperConLog().log( SIF, ERROR_MSG, "ERROR: CStrategyInterface::stage_changeTFmode you muppet! you entered an INVALID mode: %i", desiredMode );
  }
}

//Add a  superCon obstacles across the front of Alice's current position
//(width of obstacle is specified as a #define)
void CStrategyInterface::stage_addForwardUnseenObstacle() {
  
  //look-up the skynet socket to use to send map-deltas to fusionmapper
  msg_itr = StrategySNsocketList.find( SNsuperConMapDelta );
  
  //Matrix = [ROW][COLUMN]
  //2D array with columns (0) = X & (1) = Y in vehicle coordinates
  double vehCoord_obstaclePoints [NUM_POINTS_TO_EVALUATE_ALONG_OBSTACLE][2];
  double glbCoord_obstaclePoints [NUM_POINTS_TO_EVALUATE_ALONG_OBSTACLE][2];
  double dist_obstacle_to_refP;
  double transformYaw;
  
  dist_obstacle_to_refP = ( DIST_REAR_AXLE_TO_FRONT - OFFSET_OF_VEHICLE_ORIGIN_FROM_REAR_AXLE );
  transformYaw = -m_pSCstate->yaw;

  for( int i=0; i < NUM_POINTS_TO_EVALUATE_ALONG_OBSTACLE; i++ ) {
    
    vehCoord_obstaclePoints[i][X_COORD] = dist_obstacle_to_refP;
    vehCoord_obstaclePoints[i][Y_COORD] = ( (SUPERCON_OBSTACLE_WIDTH / 2) - (i * (SUPERCON_OBSTACLE_WIDTH / NUM_POINTS_TO_EVALUATE_ALONG_OBSTACLE)) ) ;
        
    /* transform vehicle coord array to global coords */
    //use the rotation matrix with -ve yaw = theta, as clockwise from north is +ve in
    //global coords
    glbCoord_obstaclePoints[i][NORTHING_COORD] = m_pSCstate->northing + ( vehCoord_obstaclePoints[i][X_COORD] * cos( transformYaw ) ) + ( vehCoord_obstaclePoints[i][Y_COORD] * sin( transformYaw ) );
    glbCoord_obstaclePoints[i][EASTING_COORD] = m_pSCstate->easting + ( vehCoord_obstaclePoints[i][X_COORD] * -sin( transformYaw ) ) + ( vehCoord_obstaclePoints[i][Y_COORD] * cos( transformYaw ) );  
    
    //Apply the update to the map
    superConMap.setDataUTM_Delta<double>(layerID_superCon, glbCoord_obstaclePoints[i][NORTHING_COORD], glbCoord_obstaclePoints[i][EASTING_COORD], SUPERCON_OBSTACLE_SPEED_MS);
    SuperConLog().log( SIF, STRATEGY_STAGE_MSG, "CStategyInterface::stage_addForwardUnseenObstacle - applying updates to the local map");
  }

  //Set-up the map-delta
  deltaPtr = superConMap.serializeDelta<double>(layerID_superCon);
  
  if( !deltaPtr->isShiftOnly() ) {
    if( silentOptionSelected == false ) {
      //NOT in silent mode --> send map-updates
      SendMapdelta( msg_itr->second, deltaPtr );    
      SuperConLog().log( SIF, STRATEGY_STAGE_MSG, "CStategyInterface::stage_addForwardUnseenObstacle - map updates sent to fusionMapper");
    } else {
      //in SILENT mode --> DON'T send map-updates
      SuperConLog().log( SIF, OUTPUT_SN_MSGS, "MAP-DELTAs suppressed as running in SILENT mode" );
    }
  } else {
    cerr <<"ERROR: CStrategyInterface::stage_addForwardUnseenObstacle - No map updates made!"<< endl;
    SuperConLog().log( SIF, ERROR_MSG, "ERROR: CStrategyInterface::stage_addForwardUnseenObstacle - No map updates made!" );
  }

  //reset delta (i.e. changes to the map before this point will not be
  //sent in the next delta.
  superConMap.resetDelta<double>(layerID_superCon);
}


//Send a new MAX/MIN speed-cap in METERS/SECOND to trajFollower
void CStrategyInterface::stage_modifyTrajFSpeedCap( const speedCapAction commandType, const double speedCap ) {
  
  newSpeedCapCmd m_newSpeedCapCmd;
  m_newSpeedCapCmd.m_speedCapSndModule = TF_MOD_SUPERCON; //set sending module = superCon
  m_newSpeedCapCmd.m_speedCapAction = commandType;
  m_newSpeedCapCmd.m_speedCapArgument = speedCap;

  if ( commandType == add_max_speedcap || commandType == add_min_speedcap || commandType == remove_max_speedcap || commandType == remove_min_speedcap ) {
    sendSkynetMsg( SNtrajFspeedCapCmd, &m_newSpeedCapCmd, sizeof(m_newSpeedCapCmd) );
    SuperConLog().log( SIF, STRATEGY_STAGE_MSG, "Sent speedCap command: %s (speeCap: %lf) to trajFollower", speedCapAction_asString( commandType ), speedCap );
  } else {
    cerr << "ERROR: CStrategyInterface::stage_modifyTrajFSpeedCap - You are a MUPPET! - you entered an INVALID speedcap commandType" << endl;
    SuperConLog().log( SIF, ERROR_MSG, "ERROR: CStrategyInterface::stage_modifyTrajFSpeedCap - You are a MUPPET! - you entered an INVALID commandType");
  }
}

void CStrategyInterface::stage_clearMaps( const mapActionCmdType commandType ) {
  
  if( commandType == clear_all_maps_everywhere ) {

    superCon_map_action m_superCon_map_action;
    m_superCon_map_action.mapActionCmd = commandType;
    m_superCon_map_action.dblArg = DBL_ZERO; //NOT USED HERE
    sendSkynetMsg( SNsuperConMapAction, &m_superCon_map_action, sizeof(m_superCon_map_action) );
    SuperConLog().log( SIF, STRATEGY_STAGE_MSG, "Sent clear map command: %s to fmapper", mapActionCmdType_asString( commandType ) );

  } else {
    cerr << "ERROR: CStrategyInterface::stage_clearMaps - you are a MUPPET!! you entered an INVALID commandType" << endl;
    SuperConLog().log( SIF, ERROR_MSG, "ERROR: CStrategyInterface::stage_clearMaps - you are a MUPPET!! you entered an INVALID commandType" );
  }
}

void CStrategyInterface::stage_signalToAstate( const SC_astateCmdType commandType ) {

  superconAstateCmd m_superconAstateCmd;
  m_superconAstateCmd.commandType = commandType;
  
  if ( commandType == sc_interface::ok_to_add_gps || commandType == sc_interface::timed_out_waiting_for_astate ) {
    sendSkynetMsg( SNsuperconAstateCmd, &m_superconAstateCmd, sizeof(m_superconAstateCmd) );
    SuperConLog().log( SIF, STRATEGY_STAGE_MSG, "Sent signal: %s to astate", SC_astateCmdType_asString( commandType ) );
  } else {
    cerr << "ERROR: CStrategyInterface::stage_signalToAstate - You are a MUPPET! - you entered an INVALID astate signal commandType" << endl;
    SuperConLog().log( SIF, ERROR_MSG, "ERROR: CStrategyInterface::stage_signalToAstate - You are a MUPPET! - you entered an INVALID commandType");
  }  
}
