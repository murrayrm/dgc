#define TABNAME Timber

// fields: type, name, default value, widgetType, xLabel, yLabel, xVal, yVal
// everything is TAB-RELATIVE. Thus input is FROM the client INTO the tab
#define TABINPUTS(_) \
  _(int, SKYNET_KEY, -1, Label, 0,0, 1,0 )  \
  _(int, DEBUG_LEVEL, -1, Label, 0,1, 1,1 )  \
  _(int, LOGGING_ENABLED, 0, Label, 0,2, 1,2 )  \
  _(int, TODOFILE_OPEN, 0, Label, 0,3, 1,3 )  \
  _(char[50], TODOFILE_NAME, "", Label, 0,4, 1,4 )  \
  _(unsigned long, SESSION_TIMESTAMP, 0, Label, 0,5, 1,5 )  \
  _(unsigned long long, REAL_TIME_POINT, 0, Label, 0,6, 1,6 )  \
  _(unsigned long long, PLAY_TIME_POINT, 0, Label, 0,7, 1,7 )  \
  _(double, PLAY_SPEED, 0, Label, 0,8, 1,8 )

#define TABOUTPUTS(_) \
  _(int, DEBUG_LEVEL, 0, Label, 0,10, 1,10 )  \
  _(int, LOGGING_ENABLED, 0, Label, 0,11, 1,11 )  \
  _(double,  PLAYBACK_TIME, 0, Entry, 0 , 12, 1,12  ) \
  _(double,  PLAYBACK_SPEED, 0, Entry, 0 , 13, 1,13  ) \
  _(int,  PLAYBACK_PAUSED, 0, Entry, 0 , 14, 1,14  )


//#undef TABNAME
