using namespace std;

#include "TrajFollower.hh"

int QUIT_PRESSED = 0;

// These extern's relate to the '--' command line options for TrajFollower (see TrajFollowerMain.cc for set-up details)
extern int           NOSTATE;
extern int           NODISPLAY;
extern int           SIM;
extern unsigned long SLEEP_TIME;
extern int           A_HEADING_REAR;
extern int           A_HEADING_FRONT;
extern int           A_YAW;
extern int           Y_REAR;
extern int           Y_FRONT;
extern double        HACK_STEER_COMMAND;
extern bool          USE_HACK_STEER;
extern int           USE_DBS;

//speed below which we manually set speeds (so as to not have divide-by-zero errors)
#define MIN_SPEED 0.01


/******************************************************************************/
/******************************************************************************/
TrajFollower::TrajFollower(int sn_key, char* pTrajfile, char* pLindzey) 
  : CSkynetContainer(SNtrajfollower, sn_key, &status_flag), CTimberClient(timber_types::trajFollower), CModuleTabClient(&m_input, &m_output), CSuperConClient("TJF")
{

  // getting start time
  DGCgettime(m_timeBegin);

  cout<<"Starting TrajFollower(...)"<<endl;

  DGCcreateMutex(&m_trajMutex);
  DGCcreateMutex(&m_speedCapMutex);
  DGCcreateMutex(&m_reverseMutex);
  DGCcreateMutex(&m_historyMutex);

  //initializing variables to make valgrind happy =)
  status_flag = 0;
  m_trajCount = 0;
  m_nActiveCount = 0;
  m_steer_cmd = 0.0;
  m_accel_cmd = 0.0;
  m_speedCapMax = 100.0;
  m_speedCapMin = -1.0;
  m_drivesocket = m_skynet.get_send_sock(SNdrivecmd);
  reverse = false;
  reverseDist = 0.0;
  historyDist = 0.0;
  distSinceLastAdd = 0.0;
  pointIndex = 0;
  sizeIndex = 0;
  logs_enabled = false;
  logs_location = "";
  logs_newDir = false;
  valid_state = true;
  old_valid_state = true;
  lindzey_logs_location = string(pLindzey);

  //determining where we calculate errors from. default is YERR_FRONT and AERR_YAW.
  EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
  EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : ( A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));

  //creating the pidControllers
  m_pPIDcontroller = new CPID_Controller(yerrorside, aerrorside, lindzey_logs_location, false);
  reversecontroller = new CPID_Controller(YERR_BACK, aerrorside, lindzey_logs_location, true);

  cout<<"FINISHED CONSTRUCTING CONTROLLERS"<<endl;

  // 3rd order traj. If we've been passed a static path, use it
  if(pTrajfile[0] == '\0')
    {
      m_pTraj   = new CTraj(3);
      cout<<"no static path"<<endl;
    }
  else
    {
      m_pTraj   = new CTraj(3, pTrajfile);
       //need to set path, cuz in main function only reset when new traj is received.  		
      m_pPIDcontroller->SetNewPath(m_pTraj, &m_state);
    }

  cout<<"TrajFollower(...) Finished"<<endl;
}

TrajFollower::~TrajFollower() 
{
  delete m_pTraj;
  delete m_pPIDcontroller;
  delete reversecontroller;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollower::Active() 
{

  // timers to make sure our control frequency is what we set (not
  // affected by how long it takes to compute each cycle)
  unsigned long long timestamp1, timestamp2;



  cout<< "Starting Active Function" <<endl;

  drivecmd_t my_command;
  // init the cmd to 0 to appease the memprofiler gods
  memset(&my_command, 0, sizeof(my_command)); 

  UpdateState();
  lastN = m_state.Northing;
  lastE = m_state.Easting;
  historyN[0] = lastN;
  historyE[0] = lastE;


  while(!QUIT_PRESSED) 
    {
      // time checkpoint at the start of the cycle
      DGCgettime(timestamp1);

      // increment module counter
      m_nActiveCount++;

      //check logging status
      logs_enabled = getLoggingEnabled(); 
      logs_newDir = checkNewDirAndReset();
      if(logs_newDir) {
	setupLogFiles();
      }

      // Get state from the simulator or astate
      // note that this sets m_state 
      UpdateActuatorState();
      if(reverse==false) UpdateReverse();

      //calculations for the sparrowHawk display
  DGCgettime(grr_timestamp);
  old_valid_state = valid_state;
  valid_state = UpdateState(grr_timestamp,true);
  //cout<<"called update state. valid? "<<valid_state<<endl;



      //Calculate the required vehicle speed
      m_Speed2 = m_state.Speed2();
      //Calculate angular values in degrees
      m_PitchDeg = ( ( m_state.Pitch / M_PI ) * 180 );
      m_RollDeg = ( ( m_state.Roll / M_PI ) * 180 );
      m_YawDeg = ( ( m_state.Yaw / M_PI ) * 180 );
      m_PitchRateDeg = ( ( m_state.PitchRate / M_PI ) * 180 );
      m_RollRateDeg = ( ( m_state.RollRate / M_PI ) * 180 );
      m_YawRateDeg = ( ( m_state.YawRate / M_PI ) * 180 );


      /** if our speed was too low, set it as if we're moving in the same direction
       * at the minimum speed WITH THE WHEELS POINTED STRAIGHT */
      if( m_state.Speed2() < MIN_SPEED )
	{
	  m_state.Vel_N = MIN_SPEED * cos(m_state.Yaw);
	  m_state.Vel_E = MIN_SPEED * sin(m_state.Yaw);
	}



      /* Compute the control inputs. */
      if(reverse == true)
      {
	  DGClockMutex(&m_trajMutex);
	  at_end = reversecontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, &m_out_yerr ,logs_enabled, logs_location, logs_newDir);
          if(at_end==true) {
	      scMessage((int)completed_reversing_action);
          }      

          DGCunlockMutex(&m_trajMutex);
	}
      else
	{
	  DGClockMutex(&m_trajMutex);
	  DGClockMutex(&m_speedCapMutex);
	  m_pPIDcontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, &m_out_yerr, logs_enabled, logs_location, logs_newDir,m_speedCapMin,m_speedCapMax);
	  DGCunlockMutex(&m_speedCapMutex);
	  DGCunlockMutex(&m_trajMutex);
	}


      /**apply DFE to proposed steering commands before shipping them off to adrive 
       * please note that pidController returns steering command in radians */
      //compute normalized steering commands from cmd in radians		
      double proposed_steer_Norm, steer_Norm, accel_Norm;
      proposed_steer_Norm = m_steer_cmd/VEHICLE_MAX_AVG_STEER;
      
      //data from driving in circes.
#warning "magic numbers here! need to tune this!"

#ifdef USE_DFE
  steer_Norm = DFE(proposed_steer_Norm, m_state.Speed2());
#else 
  steer_Norm = proposed_steer_Norm;
#endif



      //apply bound to the data
      steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);

      accel_Norm = fmax(-1.0, fmin(m_accel_cmd, 1.0));

      //for data collection purposes...
      if(USE_HACK_STEER == true)
	{
	  steer_Norm = HACK_STEER_COMMAND;
	  accel_Norm = 0;
	}


      //shipping commands off to adrive
      my_command.my_command_type = set_position;

      //if our current state is bad, tell adrive to disable for the time being
      // This first cut does not auto-recover
#warning "WE NEED A RECOVERY CONDITION HERE, OR THIS WILL BE THE END OF RACE. CURRENTLY SET THIS WAY FOR SAFETY"
      //      if((valid_state == false || old_valid_state == false) && false) {
      if(valid_state == false) {
      	cout<<"bad state, setting steer = 0, brake = -1"<<endl;
	//	my_command.my_actuator = estop;
	//my_command.number_arg = 0;
	//m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
        
        my_command.my_actuator = steer;
        my_command.number_arg = 0.0;
        m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
        my_command.my_actuator = accel;
        my_command.number_arg = -1.0;
        m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);


      } else { 
        my_command.my_actuator = steer;
        my_command.number_arg = steer_Norm;
        m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
        my_command.my_actuator = accel;
        my_command.number_arg = accel_Norm;
        m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
      }
      //broadcasts trajFollower status
      statusComm();


      //to be in nominal, we need to be receiving trajectories that
      //are close enough to be followed
      if(m_out_yerr < 1.5 && m_trajCount > 0) 
	{
	  status_flag = 0;
	} else {
	  status_flag = 10;
	}


      // time checkpoint at the end of the cycle
      DGCgettime(timestamp2);

      /** now sleep for SLEEP_TIME usec less the amount of time the
       * computation took */
      int delaytime = SLEEP_TIME - (timestamp2 - timestamp1);
      if(delaytime > 0)
	usleep(delaytime);


    } // end while(!QUIT_PRESSED) 

  cout<< "Finished Active state" <<endl;
}







/**  
 *function: DFE
 *inputs: desired steering command (on a scale of -1 to 1)
 *action: compares desired steering angle to curr angle and speed, and
 *   places hard boundaries on the angle and angle's rate of change
 *   as a function of speed
 *outputs: new, bounded steering command
 **/
#warning "the DFE function in TrajFollower needs attention!"

double TrajFollower::DFE(double proposed_cmd, double speed)
{
  double max_safe_cmd, new_cmd;

  //setting boundary conditions. below 4m/s any command is safe.
  //Likewise, a cmd of .1 should be safe at any speed.
  if(proposed_cmd < 0.1 || speed < 4.0) return proposed_cmd;

  max_safe_cmd = -.05 + 25.05/pow(speed,2);

  //  cout<<"max safe, proposed: "<<max_safe_cmd<<' '<<proposed_cmd<<endl;
  if(fabs(max_safe_cmd) - fabs(proposed_cmd) < 0) 
    {
      cout<<"DFE applied!!"<<endl;
    } 

  new_cmd =  fmax(fmin(proposed_cmd, max_safe_cmd), -max_safe_cmd);

  return new_cmd;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollower::Comm()
{
  int trajSocket;

  /** determines what type of trajs to listen for */
#ifdef USE_SNRDDFTRAJ
  trajSocket = m_skynet.listen(SNRDDFtraj, SNRddfPathGen);
#elif USE_SNREACTIVETRAJ
  trajSocket = m_skynet.listen(SNreactiveTraj, SNreactive);
#elif USE_SNROADFINDINGTRAJ
  trajSocket = m_skynet.listen(SNroadFinding, SNroadfinding);
#elif USE_SNSTATICTRAJ
  trajSocket = m_skynet.listen(SNstaticTraj, SNstaticpainter);
#else
  trajSocket = m_skynet.listen(SNtraj, SNplanner);
#endif
  
  while(!QUIT_PRESSED)
    {
      RecvTraj(trajSocket, m_pTraj);
	
      /** commenting this out as we're logging plans WAY too quickly.
      ** will make this an option soon, this is just a quick fix
      m_pTraj->print(m_outputPlans);
      m_outputPlans << endl;
      m_outputPlanStarts << m_pTraj->getNorthing(0) << ' '
      << m_pTraj->getEasting(0) << endl;
      */

      DGClockMutex(&m_trajMutex);
      m_pPIDcontroller->SetNewPath(m_pTraj, &m_state);
      DGCunlockMutex(&m_trajMutex);

      m_trajCount++;
    }
}


void TrajFollower::superconComm()
{
  int bytesReceived;
  int bytesToReceive;
  char* m_pDataBuffer;
  superConTrajFcmd* command;
  int reverseSocket; 
  reverseSocket = m_skynet.listen(SNsuperconTrajfCmd, MODsupercon);
  bytesToReceive = sizeof(superConTrajFcmd);
  m_pDataBuffer = new char[bytesToReceive];

  while(!QUIT_PRESSED)
    {
      bytesReceived = m_skynet.get_msg(reverseSocket, m_pDataBuffer, bytesToReceive, 0);

      command = (superConTrajFcmd*)m_pDataBuffer;

      if(bytesReceived != bytesToReceive)
	{
	  cerr << "Trajfollower::superconComm(): skynet error" << endl;
	}
      else if((*command).commandType == tf_forwards) //go forward again;
	{
	  DGClockMutex(&m_reverseMutex);

	  //if we were previously reversing, update our buffer
          if(reverse==true) Resume();
	
          reverse = false;
	
	  DGCunlockMutex(&m_reverseMutex);
	}
      else if(((*command).commandType == tf_reverse)&& ((*command).distanceToReverse > 0)) // we need to reverse
	{
	    
	    DGClockMutex(&m_reverseMutex);

	    //if we were previously also reversing, we need to update our buffer
            if(reverse==true) Resume();
	
            reverseDist = (*command).distanceToReverse;
	    reverse = true;
	    reversetraj = ReverseTrajGen(reverseDist);
	    reversecontroller->SetNewPath(&reversetraj, &m_state);

	    DGCunlockMutex(&m_reverseMutex);

	}
      else
	{
	  cerr << "Trajfollower::superconComm(): invalid supercon message" << endl;
	}
    }
  delete m_pDataBuffer;
}


void TrajFollower::statusComm()
{
  trajFstatusStruct status;
  int superconSocket;
  superconSocket = m_skynet.get_send_sock(SNtrajFstatus);

  if(reverse == false)
    {
      status.currentMode = tf_forwards;
    }
  else
    {
      status.currentMode = tf_reverse;
    }
  
  DGClockMutex(&m_trajMutex);
  status.currentVref = m_pPIDcontroller->access_VRef();
  DGCunlockMutex(&m_trajMutex);
  
  DGClockMutex(&m_speedCapMutex);
  status.largestMINspeedCap = m_speedCapMin;
  status.smallestMAXspeedCap = m_speedCapMax;
  DGCunlockMutex(&m_speedCapMutex);
  
  if(m_skynet.send_msg(superconSocket, &status, sizeof(status), 0) != sizeof(status))
    {
      cerr << "TrajFollower::statusCom: error: msg size does not match send msg size" << endl;
    }
}


void TrajFollower::speedCapComm()
{
  int bytesReceived;
  int bytesToReceive;
  newSpeedCapCmd m_DataBuffer;
  newSpeedCapCmd* m_pDataBuffer = &m_DataBuffer;
  int speedCapSocket; 
  double* module_status;

  //second argument ignored
  speedCapSocket = m_skynet.listen(SNtrajFspeedCapCmd, MODsupercon);
  bytesToReceive = sizeof(newSpeedCapCmd);

  //for each module, I'll need to store two values: curr min/max speed limits
  int num_modules = (int)NUM_SENDING_MODULES;
  module_status = new double[2*num_modules];

  //initialize array to -1.0 to represent no command
  for(int bar=1; bar <= num_modules; bar++) {
    module_status[2*bar-2] = -1.0;
    module_status[2*bar-1] = 100.0;
  }

  while(!QUIT_PRESSED)
    {

      bytesReceived = m_skynet.get_msg(speedCapSocket, m_pDataBuffer, bytesToReceive, 0);
      ofstream outfil("recvd");
      outfil.write((char*)m_pDataBuffer, 16);
      outfil.close();

      if((!USE_DBS) && (m_pDataBuffer->m_speedCapSndModule == TF_MOD_DBS))
	{
	  continue;
	}
      if(bytesReceived <= 0)
	{
	  cerr << "Trajfollower::speedCapComm(): skynet error" << endl;
	}
      else if(m_pDataBuffer->m_speedCapAction == add_max_speedcap)
	{

          double val = m_pDataBuffer->m_speedCapArgument;
           module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)+1] = val;

	}

      else if(m_pDataBuffer->m_speedCapAction == add_min_speedcap)
	{

          double minval = m_pDataBuffer->m_speedCapArgument;
	  module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)] = minval;

	}

      else if(m_pDataBuffer->m_speedCapAction == remove_max_speedcap)
	{
	  module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)+1] = 100.0;

	}

      else if(m_pDataBuffer->m_speedCapAction == remove_min_speedcap)
	{
	  module_status[2*((int)m_pDataBuffer->m_speedCapSndModule)] = -1.0;
	}
      
      double max = 100;
      double min = -1;
      

      /** calculating min and max speedcaps */
      for(int foo = 0; foo < num_modules; foo++)
	{
	  
	  //find max of the min speed caps
	  if (module_status[2*foo] > min)
	    min = module_status[2*foo];
	  
	  //find min of the max speed caps
	  if(module_status[2*foo+1] < max)
	    max = module_status[2*foo+1];
	  
	}

      // sets the vars the PID_Controller accesses
      DGClockMutex(&m_speedCapMutex);
      m_speedCapMin = min;
      m_speedCapMax = max;
      DGCunlockMutex(&m_speedCapMutex);
    }
  delete m_pDataBuffer;

}    


void TrajFollower::setupLogFiles()
{

    logs_location = getLogDir();

    char testFileName[256];

    char testFilePath[256];
	  
    sprintf(testFileName, "test.dat");

    string temp;
    temp = logs_location + testFileName;
    strcpy(testFilePath, temp.c_str());

    m_outputTest << setprecision(20);

    sprintf(testFileName, "test.dat");

    temp = logs_location + testFileName;
    strcpy(testFilePath, temp.c_str());

    m_outputTest << setprecision(20);
	
}



void::TrajFollower::UpdateReverse()
{
  double distTraveled = hypot(m_state.Northing-lastN, m_state.Easting-lastE);

  cout<<setprecision(20);

  //if we haven't traveled far enough since last added a point, return
  if(distTraveled > HISTORYSPACING)
  {
    //if we reach this point, we've traveled far enough to need to add a new point
    //lock mutex because both the active and the superConComm thread can change the history
    DGClockMutex(&m_historyMutex);
#warning "better to subtract off first point, than assume exactly equal to 200"
    historyDist = fmin(MAXHISTORYSIZE*HISTORYSPACING, fmax(0, historyDist + distTraveled));
    //    cout<<"historyDist @ ln 616: "<<historyDist<<endl;
    pointIndex++;
    sizeIndex = min(sizeIndex+1, MAXHISTORYSIZE);

    historyN[pointIndex%MAXHISTORYSIZE] = lastN;
    historyE[pointIndex%MAXHISTORYSIZE] = lastE;
    historyDiff[pointIndex%MAXHISTORYSIZE] = distTraveled;
    DGCunlockMutex(&m_historyMutex);

  lastN = m_state.Northing;
  lastE = m_state.Easting;

  }
}

/**
 * function: Resume
 * Action: this function is in charge of updating our history buffer after we've
 *    reversed some distance. 
 */

void TrajFollower::Resume()
{

  /**need to find the closest point BEHIND us in the history
   * traverse the history from current point backwards until the 
   * distance from point i-1 to curr pos is larger than the 
   * distance from point i to curr pos */

  double currDist, nextDist, currN, currE, nextN, nextE;

  //want first currDist to be oldest point in buffer. 
  //this will be one after pointIndex if sizeIndex = max history size (all spaces are filled)
  //or the point sizeIndex before pointIndex
  //pointIndex ++;
  currN = historyN[pointIndex % MAXHISTORYSIZE];
  currE = historyE[pointIndex % MAXHISTORYSIZE];

  if(pointIndex == 0)
    {
      //      cout<<"asked to resume before building proper history"<<endl;
      historyN[pointIndex] = m_state.Northing;
      historyE[pointIndex] = m_state.Easting;
    }else
      {

  //incrementing the pointer because we want to construct the new buffer starting with 
  //the point furthest away (i.e. pointIndex + 1) 
  currDist = hypot((m_state.Northing - currN),
		   (m_state.Easting - currE));
  //cout<<"currentDist in resumeForward returns: "<<currDist<<endl;

  //history dist keeps track of the distance between pointIndex -1 and pointIndex
  historyDist -= historyDiff[pointIndex % MAXHISTORYSIZE];
  //    cout<<"historyDist @ ln 658: "<<historyDist<<endl;
  pointIndex--; 
  sizeIndex--; 
  nextN = historyN[pointIndex % MAXHISTORYSIZE];
  nextE = historyE[pointIndex % MAXHISTORYSIZE];

  nextDist = hypot((m_state.Northing - nextN),
		   (m_state.Easting - nextE));
  //  cout<<"currDist, nextDist at line 675: "<<currDist<<' '<<nextDist<<endl;

  /** while the next point older in the history is closer to us than the newer one, 
   * keep checking */                                                                                        
  while(nextDist < currDist && sizeIndex > 0)
    {
      historyDist -= historyDiff[pointIndex % MAXHISTORYSIZE];
      //    cout<<"historyDist @ ln 672: "<<historyDist<<endl;
      pointIndex--; 
      sizeIndex--; 
      currN = nextN;
      currE = nextE;
      nextN = historyN[pointIndex % MAXHISTORYSIZE];
      nextE = historyE[pointIndex % MAXHISTORYSIZE];

      currDist = nextDist;
      nextDist = hypot((m_state.Northing - nextN),
	        	   (m_state.Easting - nextE));

    }

  //at this point, pointIndex points to the point that's closest behind us in the buffer
  //makes the last point in our history be our current position
  //incrementing pointIndex, since we're about to add another point
  pointIndex++;
  //  cout<<"current N and E points: "<<currN<<' ' << currE<<endl;
  historyDist += hypot(m_state.Northing - currN, m_state.Easting-currE);
  historyDist = fmin(MAXHISTORYSIZE*HISTORYSPACING, fmax(0, historyDist));
  //    cout<<"historyDist @ ln 693: "<<historyDist<<endl;
  historyN[pointIndex % MAXHISTORYSIZE] = m_state.Northing;
  historyE[pointIndex % MAXHISTORYSIZE] = m_state.Easting;

  lastN = m_state.Northing;
  lastE = m_state.Easting;
  sizeIndex = min(sizeIndex+1, MAXHISTORYSIZE);
      }
}

CTraj TrajFollower::ReverseTrajGen(double distance)
{

  //This code was shamelessly cut and pasted from RDDFPathGen
  CTraj ptraj;
  corridorstruct corridor_whole;
  vector<double> location(2);
  int reverseSocket;
  location[1]=m_state.Easting;
  location[0]=m_state.Northing;

  reverseSocket =  m_skynet.get_send_sock(SNtrajReverse);
  
  if(distance > historyDist)
    {
#warning "should throw an exception to superCon here--no, but alex needs way to keep backing up off of contingency plan. Laura: make a bug for alex. also, need way to reset my reverse history"
      cout<<"asked to back up too far. "<<endl;
      cout<<"Distance requested = "<<distance<<";   Distance in history: "<<historyDist<<endl;
      distance = historyDist;
    }
  if(pointIndex < 2 )
    {
      cout << "can't backup less than 1 meter" << endl;
	  scMessage((int)completed_reversing_action);    
#warning "do i need to initialize ptraj in some way before returning it??"
      return ptraj;
    }

  // store data in corridor

  int i = pointIndex;
  corridor_whole.numPoints = 1;
  double tempDistance = historyDiff[i%MAXHISTORYSIZE];
  corridor_whole.n.push_back(historyN[i%MAXHISTORYSIZE]);
  corridor_whole.e.push_back(historyE[i%MAXHISTORYSIZE]);
  i--;

  cout << "corridor_whole.n.size() = " << corridor_whole.n.size() << endl;
  cout << "corridor_whole.numPoints = " << corridor_whole.numPoints << endl;

  /**this creates the corridor from which we will create the RDDF */
  while(tempDistance < distance)
    {

      tempDistance += historyDiff[i%MAXHISTORYSIZE];

      corridor_whole.e.push_back(historyE[i%MAXHISTORYSIZE]);
      corridor_whole.n.push_back(historyN[i%MAXHISTORYSIZE]);
     
      corridor_whole.width.push_back(RWIDTH);
      corridor_whole.speedLimit.push_back(RSPEED);
      corridor_whole.numPoints += 1;

      i--;

    }

  pathstruct& path_whole_sparse = *(new pathstruct);
  path_whole_sparse = Path_From_Corridor(corridor_whole);

 // we haven't gone anywhere yet, so we're still at the start of the path
  path_whole_sparse.currentPoint = 0;

  // get the chopped dense traj (the one we send over skynet), in path format
#warning "this seems to segfault when given too small a number"

  pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location, 0, distance + VEHICLE_AXLE_DISTANCE);

  StorePath(ptraj, path_chopped_dense);

  SendTraj(reverseSocket, &ptraj);

  delete &path_whole_sparse;
  return ptraj;
}
