function analyzeStability()

     v = 2;
     p = .1;
     i = .05;
     d = .08;

     L = 3.55;
     alpha = 1;

     A = [0,v,0;0,0,v/L;0,0,alpha];
     B = [0;0;-alpha];
     C = [1,0,0;0,1,0;0,0,1];

