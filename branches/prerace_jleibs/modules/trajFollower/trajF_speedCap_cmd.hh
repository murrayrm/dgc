#ifndef TRAJF_SPEEDCAP_CMD_HH
#define TRAJF_SPEEDCAP_CMD_HH


//FILE DESCRIPTION: Header file that contains the definition of the data structures
//and enum commands used to send speed-cap commands to trajF by any module
//this data structure should be sent using - 
//Skynet message type: SNtrajFspeedCapCmd
#define SPEEDCAP_ACTION_LIST(_) \
  _( add_max_speedcap, = 0 ) \
  _( add_min_speedcap, ) \
  _( remove_max_speedcap, ) \
  _( remove_min_speedcap, )
DEFINE_ENUM( speedCapAction, SPEEDCAP_ACTION_LIST )

//if you want your module to send speedCap messages, first talk to Alex about how
//it fits in the overall architecture, and then with Laura.
//NUM_SENDING_MODULES should always be the last in this list.
enum speedCapSndModule { TF_MOD_SUPERCON, TF_MOD_DBS, NUM_SENDING_MODULES };

struct newSpeedCapCmd {

  speedCapSndModule m_speedCapSndModule;
  speedCapAction m_speedCapAction;
  double m_speedCapArgument;

};

#endif
