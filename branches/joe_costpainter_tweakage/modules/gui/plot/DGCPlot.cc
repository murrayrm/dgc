/**
 * DGCPlot.cc
 * Revision History:
 * 08/01/2005  hbarnor  Created
 * $Id$
 */

#include "DGCPlot.hh"
#include <iostream>

/**
 * gdouble x = 0;
 * gboolean update(gpointer data)
 * {
 * //sleep(3);
 * // generate new Y -data 
 *   //std::cout << " Waking up " << std::endl;  
 *   gdouble y; 
 * DGCPlot* myFix = (DGCPlot*)data;
 *
 *   y = rand()%10/10.;
 *  x += 1.0;
 *   //std::cout << " Calling add "<< x << "," << y << " " << std::endl;
 * myFix->addDataPoint(x, y);
 *  //std::cout << "Plotted : " << x << "," << y << " " << std::endl;
 * //sleep(2);
 *
 * return TRUE;
 * }
 */

DGCPlot::DGCPlot(GtkWidget* myParent)
  : m_autoscale(false)
{
  m_parent = myParent;
  gint page_width, page_height;
  gfloat scale = 0.5;
 
  page_width = (gint) (GTK_PLOT_LETTER_W * scale);
  page_height = (gint) (GTK_PLOT_LETTER_H * scale);

  // DND Canvas 
  canvas = gtk_plot_canvas_new(page_width, page_height, 1.);
  GTK_PLOT_CANVAS_UNSET_FLAGS(GTK_PLOT_CANVAS(canvas), GTK_PLOT_CANVAS_DND_FLAGS);
  
  // add canvas to the container
  gtk_box_pack_start(GTK_BOX(myParent),canvas, TRUE, TRUE,0); 
  // set properties on the canvas
  gdk_color_parse("light blue", &color);
  gdk_color_alloc(gtk_widget_get_colormap(canvas), &color);
  gtk_plot_canvas_set_background(GTK_PLOT_CANVAS(canvas), &color);
  gtk_widget_show(canvas);
  //create a plot instance and set some properties
  plotInstance = gtk_plot_new_with_size(NULL, 0.65,0.45);
  gdk_color_parse("light yellow", &color);
  gdk_color_alloc(gtk_widget_get_colormap(plotInstance), &color);
  gtk_plot_set_background(GTK_PLOT(plotInstance), &color);
  // set the properties of the legend
  gtk_plot_legends_set_attributes(GTK_PLOT(plotInstance), NULL, 0, NULL,&color);
  gtk_plot_set_legends_border(GTK_PLOT(plotInstance), GTK_PLOT_BORDER_LINE, 3);
  gtk_plot_legends_move(GTK_PLOT(plotInstance), 0.01, 0.01);
  gtk_plot_hide_legends(GTK_PLOT(plotInstance));

  
  gdk_color_parse("white", &color);
  gdk_color_alloc(gtk_widget_get_colormap(canvas), &color);
  //begin

  gtk_plot_axis_set_labels_style(gtk_plot_get_axis(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_TOP), GTK_PLOT_LABEL_FLOAT, GTK_PLOT_LABEL_FLOAT);
  gtk_plot_axis_set_labels_style(gtk_plot_get_axis(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_BOTTOM),GTK_PLOT_LABEL_FLOAT, GTK_PLOT_LABEL_FLOAT);
  gtk_plot_axis_set_visible(gtk_plot_get_axis(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_TOP), TRUE);
  gtk_plot_axis_set_visible(gtk_plot_get_axis(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_RIGHT), TRUE);
  //gtk_plot_grids_set_visible(GTK_PLOT(plotInstance), TRUE, TRUE, TRUE, TRUE);
 
  child = gtk_plot_canvas_plot_new(GTK_PLOT(plotInstance));
  //gtk_plot_canvas_put_child(GTK_PLOT_CANVAS(canvas), child, .15, .15, .80, .65);
  gtk_plot_canvas_put_child(GTK_PLOT_CANVAS(canvas), child, .15, .09, 0.9, 0.88);
  gtk_widget_show(plotInstance);
  //add the a canvas for the title
  titleChild = gtk_plot_canvas_text_new("Times-BoldItalic", 15, 0, NULL, NULL, TRUE, GTK_JUSTIFY_CENTER, "Default Title");
  gtk_plot_canvas_put_child(GTK_PLOT_CANVAS(canvas), titleChild, .45, .05, 0., 0.);
  // turn off some unneeded axis 
  gtk_plot_axis_hide_title(gtk_plot_get_axis(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_RIGHT));
  gtk_plot_axis_hide_title(gtk_plot_get_axis(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_TOP));
  // initialize the dataset and set ist properties
  dataset = GTK_PLOT_DATA(gtk_plot_data_new());
  gtk_plot_add_data(GTK_PLOT(plotInstance), dataset);
  gtk_widget_show(GTK_WIDGET(dataset));
  
  gdk_color_parse("red", &color);
  gdk_color_alloc(gdk_colormap_get_system(), &color);  
  gtk_plot_data_set_symbol(dataset, GTK_PLOT_SYMBOL_CROSS, GTK_PLOT_SYMBOL_OPAQUE, DGC_SYMBOL_SIZE, DGC_SYMBOL_LINE_WIDTH, &color, &color);
  gtk_plot_data_set_line_attributes(dataset, GTK_PLOT_LINE_SOLID, GDK_CAP_NOT_LAST, GDK_JOIN_MITER, DGC_LINE_WIDTH, &color);
  //update the display with th changes
  gtk_plot_canvas_paint(GTK_PLOT_CANVAS(canvas));
  gtk_plot_canvas_refresh(GTK_PLOT_CANVAS(canvas));
  // am not sure what this does, am guessing it allows for scrolling. 
  gtk_plot_clip_data(GTK_PLOT(plotInstance), TRUE);
  gtk_widget_show_all(myParent);
  //  gint32 timer;  
  //timer = gtk_timeout_add(300, update, this);
}


DGCPlot::~DGCPlot()
{
  delete child;
  delete dataset;
  delete plotInstance;
  delete canvas;
}

void DGCPlot::addDataPoint(point newData)
{
  GtkPlot *plot;
  plot = GTK_PLOT(plotInstance);
  // values used in updating the range of the display 
  gdouble xmin, xmax;
  //create space for new data
  if(dataset->num_points == 0)
    {
      // allocate initial memory 
      px = (gdouble * )g_malloc(sizeof(gdouble));
      py = (gdouble *)g_malloc(sizeof(gdouble));
    }
  else
    {
      px = (gdouble *)g_realloc(px, (dataset->num_points+1)*sizeof(gdouble));
      py = (gdouble *)g_realloc(py, (dataset->num_points+1)*sizeof(gdouble));
    }      
  // add the data
  px[dataset->num_points] = newData.x;
  py[dataset->num_points] = newData.y;
  
  //actually add the data
  gtk_plot_data_set_numpoints(dataset, dataset->num_points+1); 
  gtk_plot_data_set_x(dataset, px); 
  gtk_plot_data_set_y(dataset, py); 
  // update the ranges if necessary and draw the points
  gtk_plot_get_xrange(plot, &xmin , &xmax);
  if(m_autoscale)
    {
      gtk_plot_autoscale(GTK_PLOT(plotInstance));
    }
  else
    {  
      if(newData.x > xmax)
	{
	  gtk_plot_set_xrange(plot, xmin + 5. , xmax + 5.);
	  //gtk_plot_canvas_paint(GTK_PLOT_CANVAS(canvas));
	  //gtk_widget_queue_draw(GTK_WIDGET(canvas));
	} 
      else 
	{
	  gtk_plot_data_draw_points(dataset, 1);
	  //gtk_plot_paint(plot);
	  //gtk_plot_refresh(canvas, NULL);
	}
    }
  gtk_plot_canvas_paint(GTK_PLOT_CANVAS(canvas));
  gtk_widget_queue_draw(GTK_WIDGET(canvas));
}


void DGCPlot::setTitle(string title)
{
  gtk_plot_canvas_text_set_attributes(GTK_PLOT_CANVAS_TEXT(titleChild),"Times-BoldItalic", 15, 0, NULL, NULL, TRUE, GTK_JUSTIFY_CENTER, title.c_str());  
}

void DGCPlot::setXLabel(string xLabel)
{
  gtk_plot_axis_set_title(gtk_plot_get_axis(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_BOTTOM), xLabel.c_str());
}

void DGCPlot::setYLabel(string yLabel)
{  
  gtk_plot_axis_set_title(gtk_plot_get_axis(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_LEFT), yLabel.c_str());  
}

void DGCPlot::setYRange(double yMin, double yMax, double majorTick, int numMinorTicks)
{
  gtk_plot_set_yrange(GTK_PLOT(plotInstance), yMin, yMax);
  gtk_plot_set_ticks(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_Y, majorTick, numMinorTicks);
}

void DGCPlot::setXRange(double xMin, double xMax, double majorTick, int numMinorTicks)
{
  gtk_plot_set_xrange(GTK_PLOT(plotInstance), xMin, xMax);
  gtk_plot_set_ticks(GTK_PLOT(plotInstance), GTK_PLOT_AXIS_X, majorTick, numMinorTicks);
}


void DGCPlot::displayLegend(bool display)
{
  if(display)
    {
      gtk_plot_show_legends(GTK_PLOT(plotInstance));
    }
  else
    {
      gtk_plot_show_legends(GTK_PLOT(plotInstance));
    }
}

void DGCPlot::setLegend(string legend)
{
    gtk_plot_data_set_legend(dataset, legend.c_str());
}
