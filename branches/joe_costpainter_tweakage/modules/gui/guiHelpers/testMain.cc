

#include <GL/gl.h>
#include <GL/glu.h>

#include <gtkmm.h>
#include <gtkglmm.h>

#include "DGCEntry.hh"
#include <iostream> 
//#include <glibmm.h>

using namespace std;

int main(int argc, char *argv[])
{
  Gtk::Main * kit = new Gtk::Main(0, NULL);
  Gtk::GL::init(0, NULL);
  Glib::ustring myTest1("test1"); 
  
  DGCWidget* myWid = new DGCEntry(myTest1);
  DGCWidget* myArray[3][3];
  
  myTest1 = myTest1 + "managed 1" ;
  myArray[0][0] = manage(new DGCEntry(myTest1));
  myTest1 = "managed2" ;
  myArray[0][1] = manage(new DGCEntry(myTest1));
  cout << myWid->getText() << endl;
  
  Glib::ustring myTest2("test2"); 
  myWid->setText(myTest2);
  
  cout << "From 0,0: " << myArray[0][0]->getText() << " now setting " << endl;
  myArray[0][0]->setText(myTest2);
  cout << "From 0,0: " << myArray[0][0]->getText() << " now setting " << endl;
  myArray[0][1]->setText(myTest1);
}
