/*
 * CTimber - timber class member functions
 *
 * Jason Yosinski, Apr 05
 * Documented by Richard and Lars, 16 Apr 05
 *
 */

#include "CTimber.hh"

using namespace std;

/* Ctimber constructor */
CTimber::CTimber(int des_skynet_key, string playback_location)
  : CSkynetContainer(MODtimber, des_skynet_key)
{
  /* Initialize data structure */
  skynet_key = des_skynet_key;
  session_timestamp = new char[MAX_TIMESTAMP_LENGTH];
  logging_on = false; // turn logging off when we start
  // initialize all values to their appropriate defaults
  TIMBER_LOG_INFO(SET_DEFAULT_LOG_LEVELS)
  // initialize map for changing individual logging levels
  TIMBER_LOG_INFO(TIMBER_MAP_STR2NUM_LONG_INIT)
  TIMBER_LOG_INFO(TIMBER_MAP_STR2NUM_SHORT_INIT)

  shutdown = false;
  debug_level = DEFAULT_DEBUG;

  /* Initialize the playback data structure */
  playback_state.playback_speed = 0;
  playback_state.real_time_point = 0;
  playback_state.playback_time_point = 0;
  playback_state.playback_location = playback_location;

  /* Initialize commands to be processed (??) */
  todoSet.insert(""); // because getCommand returns a "" when
                      // we don't do anything

  // make some mutexes before we start the threads
  DGCcreateMutex(&session_timestamp_mutex);
  DGCcreateMutex(&todoListFile_mutex);
  DGCcreateMutex(&logging_on_mutex);
  DGCcreateMutex(&shutdown_mutex);
  DGCcreateMutex(&debug_level_mutex);
  DGCcreateMutex(&playback_state_mutex);

  /* Start send and receive threads */
  DGCstartMemberFunctionThread(this, &CTimber::RecvThread);
  DGCstartMemberFunctionThread(this, &CTimber::RecvFromGuiThread);
  DGCstartMemberFunctionThread(this, &CTimber::SendThread);
}



/* CTimber destructor */
CTimber::~CTimber()
{
  delete [] session_timestamp;

  /* Get rid of mutexes that were used by the threads */
  DGCdeleteMutex(&session_timestamp_mutex);
  DGCdeleteMutex(&todoListFile_mutex);
  DGCdeleteMutex(&logging_on_mutex);
  DGCdeleteMutex(&shutdown_mutex);
  DGCdeleteMutex(&debug_level_mutex);
  DGCdeleteMutex(&playback_state_mutex);
}


/**
 *
 * ActiveLoop - this is the where user input is accepted.
 *
 * This function prompts for use input and acts on it.  The actually
 * data is taken inside of the receive thread.
 *
 */
void CTimber::TimberActiveLoop()
{
  char* line_buffer = new char[MAX_INPUT_LENGTH];
  string user_cmd;

  /* Print out a header to let the user know that we are here */
  cout << "\n"
       << "\nTimber Control"
       << "\n====================="
       << "\nType help for a list of commands." << endl;

  /* Loop until quit */
  while (true)
    {
      cout << PROMPT;

      line_buffer[0] = '\0';
      cin.getline(line_buffer, MAX_INPUT_LENGTH);
      istringstream iss(line_buffer);
      // cout << "got the whole line and it was \"" << iss.str() << "\"" << endl;
      user_cmd = "";
      iss >> user_cmd;
      // cin >> user_cmd;

      if (user_cmd == "help")
	cout << OUTPUT << "  General Commands\n"
	     << OUTPUT << "------------------------\n"
	     << OUTPUT << "   stat, s - print status of logging and playback\n"
	     << OUTPUT << "        d0 - set debug level 0\n"
	     << OUTPUT << "        d1 - set debug level 1\n"
	     << OUTPUT << "        d2 - set debug level 2\n"
	     << OUTPUT << "  quit (q) - quit\n"
	     << OUTPUT << "\n"
	     << OUTPUT << "  Logging Commands:\n"
	     << OUTPUT << "------------------------\n"
	     << OUTPUT << "     start - start logging\n"
	     << OUTPUT << "      stop - stop logging\n"
	     << OUTPUT << "   restart - restart logging\n"
	     << OUTPUT << "set name # - set module `name' to logging level `#'\n"
	     << OUTPUT << "\n"
	     << OUTPUT << "  Playback Commands:\n"
	     << OUTPUT << "------------------------\n"
	     << OUTPUT << "    play # - start playing back the data at time #.  # is in seconds, and.\n"
	     << OUTPUT << "               is relative to the start of the run.  Speed will be set to\n"
	     << OUTPUT << "               1 if it was 0.\n"
	     << OUTPUT << "    time # - same as play, but speed is not changed.\n"
	     << OUTPUT << "   speed # - set the playback speed to #.  # is the ratio of playback\n"
	     << OUTPUT << "   (or sp #)   time to real time, so .5 would be half speed.  Negative\n"
	     << OUTPUT << "               will be playing in reverse, but most likely will be very\n"
	     << OUTPUT << "               inefficient.\n"
	     << OUTPUT << " pause (p) - set playback speed to 0.\n"
	     << OUTPUT << "resume (r) - set playback speed to 1.\n"
	     << OUTPUT << "Note: aplay and atime are like play and time, but absolute rather than relative." << endl;

      else if (user_cmd == "stat" || user_cmd == "s")
	{
	  cout << OUTPUT << "stat called" << endl;
	  printStatus();
	}
      else if (user_cmd == "quit" || user_cmd =="q")
	{
	  cout << OUTPUT << "quit called" << endl;
	  cout << OUTPUT << "Whenever you're ready to collect all the logs onto one computer, run\n"
	       << OUTPUT << "grab_logs.sh.  Run `./grablogs' (with no aguments) for help." << endl;
	  DGClockMutex(&shutdown_mutex);
	  shutdown = true;
	  DGCunlockMutex(&shutdown_mutex);
	  break;
	}
      else if (user_cmd == "stop")
	{
	  cout << OUTPUT << "stop called" << endl;
	  DGClockMutex(&logging_on_mutex);
	  logging_on = false;
	  DGCunlockMutex(&logging_on_mutex);
	}
      else if (user_cmd == "d0")
	{
	  cout << OUTPUT << "debug level 0" << endl;
	  setDebugLevel(0);
	  setTalkerDebugLevel(0);
	}
      else if (user_cmd == "d1")
	{
	  cout << OUTPUT << "debug level 1" << endl;
	  setDebugLevel(1);
	  setTalkerDebugLevel(1);
	}
      else if (user_cmd == "d2")
	{
	  cout << OUTPUT << "debug level 2" << endl;
	  setDebugLevel(2);
	  setTalkerDebugLevel(2);
	}
      else if (user_cmd == "start")
	{
	  cout << OUTPUT << "start called" << endl;
	  DGClockMutex(&logging_on_mutex);
	  if (logging_on)
	    {
	      DGCunlockMutex(&logging_on_mutex);
	      cout << OUTPUT << "(ignored because logging is already on)" << endl;
	    }
	  else
	    {
	      /* Turn on data logging */
	      logging_on = true;
	      DGCunlockMutex(&logging_on_mutex);
	      setupNewSession();
	    }
	}
      else if (user_cmd == "restart")
	{
	  cout << OUTPUT << "restart called" << endl;
	  DGClockMutex(&logging_on_mutex);
	  if (!logging_on)
	    {
	      logging_on = true;
	      DGCunlockMutex(&logging_on_mutex);
	      cout << OUTPUT << "but logging wasn't started in the first place." << endl;
	      cout << OUTPUT << "We'll assume you meant \"start\" though." << endl;
	    }
	  
	  DGCunlockMutex(&logging_on_mutex);
	  setupNewSession();
	}
      else if (user_cmd == "set")
	{
	  cout << OUTPUT << "set called" << endl;
	  DGClockMutex(&logging_on_mutex);
	  string module_name;
	  int new_level;
	  iss >> module_name >> new_level;
	  // check to make sure the module name they entered is valid
	  if (MAPNAME.find(module_name) == MAPNAME.end())
	    {
	      cout << OUTPUT << "ERROR:  What is this \"" << module_name << "\" you speak of?" << endl;
	    }
	  else
	    {
	      LEVEL_ARRAY_NAME[MAPNAME[module_name]] = new_level;
	      cout << OUTPUT << "logging level for \"" << module_name << "\" is now " << new_level << endl;
	    }
	  DGCunlockMutex(&logging_on_mutex);	  
	}
      else if (user_cmd == "play")
	{
	  cout << OUTPUT << "play called, ";
	  unsigned long long real_time, play_time;
	  iss >> play_time;

	  lockPlaybackState();
	  play_time += humanTime2Unix(playback_state.playback_location);
	  DGCgettime(real_time);

	  playback_state.real_time_point = real_time;
	  playback_state.playback_time_point = play_time * (unsigned long long)1e6;
	  if (playback_state.playback_speed == 0)
	    playback_state.playback_speed = 1;

	  cout << OUTPUT << "new time point is (real_time, play_time, speed) = (" << real_time/(unsigned long long)1e6
	       << ", " << play_time << ", " << playback_state.playback_speed << ")" << endl;

	  unlockPlaybackState();
	}
      else if (user_cmd == "aplay")
	{
	  cout << OUTPUT << "aplay called, ";
	  unsigned long long real_time, play_time;
	  iss >> play_time;
	  DGCgettime(real_time);

	  lockPlaybackState();
	  playback_state.real_time_point = real_time;
	  playback_state.playback_time_point = play_time * (unsigned long long)1e6;
	  if (playback_state.playback_speed == 0)
	    playback_state.playback_speed = 1;

	  cout << OUTPUT << "new time point is (real_time, play_time, speed) = (" << real_time/(unsigned long long)1e6
	       << ", " << play_time << ", " << playback_state.playback_speed << ")" << endl;

	  unlockPlaybackState();
	}
      else if (user_cmd == "time")
	{
	  cout << OUTPUT << "time called, ";
	  unsigned long long real_time, play_time;
	  iss >> play_time;

	  lockPlaybackState();
	  play_time += humanTime2Unix(playback_state.playback_location);
	  DGCgettime(real_time);

	  playback_state.real_time_point = real_time;
	  playback_state.playback_time_point = play_time * (unsigned long long)1e6;

	  cout << OUTPUT << "new time point is (real_time, play_time, speed) = (" << real_time/(unsigned long long)1e6
	       << ", " << play_time << ", " << playback_state.playback_speed << ")" << endl;

	  unlockPlaybackState();
	}
      else if (user_cmd == "atime")
	{
	  cout << OUTPUT << "atime called, ";
	  unsigned long long real_time, play_time;
	  iss >> play_time;
	  DGCgettime(real_time);

	  lockPlaybackState();
	  playback_state.real_time_point = real_time;
	  playback_state.playback_time_point = play_time * (unsigned long long)1e6;

	  cout << OUTPUT << "new time point is (real_time, play_time, speed) = (" << real_time/(unsigned long long)1e6
	       << ", " << play_time << ", " << playback_state.playback_speed << ")" << endl;

	  unlockPlaybackState();
	}
      else if (user_cmd == "speed" || user_cmd == "sp")
	{
	  cout << OUTPUT << "speed called, ";
	  double speed;
	  iss >> speed;
	  cout << OUTPUT << "new speed is " << speed << endl;

	  setNewSpeedNow(speed);
	}
      else if (user_cmd == "pause" || user_cmd == "p")
	{
	  cout << OUTPUT << "pause called" << endl;
	  cout << OUTPUT << "new speed is " << 0 << endl;
	  setNewSpeedNow(0);
	}
      else if (user_cmd == "resume" || user_cmd == "r")
	{
	  cout << OUTPUT << "resume called" << endl;
	  cout << OUTPUT << "new speed is " << 1 << endl;
	  setNewSpeedNow(1);
	}
      else if (user_cmd == "replay" || user_cmd == "rp")
	{
	  cout << OUTPUT << "replay called" << endl;
	  cout << OUTPUT << "new speed is " << 0 << endl;

	  setNewSpeedNow(0);

	  unsigned long long real_time, play_time;
	  play_time = 0;

	  lockPlaybackState();
	  play_time += humanTime2Unix(playback_state.playback_location);
	  DGCgettime(real_time);

	  playback_state.real_time_point = real_time;
	  playback_state.playback_time_point = play_time * (unsigned long long)1e6;

	  cout << OUTPUT << "new time point is (real_time, play_time, speed) = (" << real_time/(unsigned long long)1e6
	       << ", " << play_time << ", " << playback_state.playback_speed << ")" << endl;

	  unlockPlaybackState();
	}
      else if (user_cmd == "rpr")
	{
	  cout << OUTPUT << "replay/resume called" << endl;
	  cout << OUTPUT << "new speed is " << 1 << endl;

	  setNewSpeedNow(0);

	  unsigned long long real_time, play_time;
	  play_time = 0;

	  lockPlaybackState();
	  play_time += humanTime2Unix(playback_state.playback_location);
	  DGCgettime(real_time);

	  playback_state.real_time_point = real_time;
	  playback_state.playback_time_point = play_time * (unsigned long long)1e6;

	  cout << OUTPUT << "new time point is (real_time, play_time, speed) = (" << real_time/(unsigned long long)1e6
	       << ", " << play_time << ", " << 1 << ")" << endl;

	  unlockPlaybackState();

	  setNewSpeedNow(1);
	}
      else if (user_cmd == "")
	continue;
      else
	cout << OUTPUT << "unknown command \"" << user_cmd << "\"" << endl;

      fflush(stdin);
    }

  /* Wait for everyone to shut down */
#warning Should be done by rejoining threads
  sleep(1);

  delete [] line_buffer;
}



string CTimber::getTimestamp()
{
  DGClockMutex(&session_timestamp_mutex);
  string ret(session_timestamp);
  DGCunlockMutex(&session_timestamp_mutex);
  return ret;
}



int CTimber::getDebugLevel()
{
  DGClockMutex(&debug_level_mutex);
  int ret = debug_level;
  DGCunlockMutex(&debug_level_mutex);
  return ret;
}



void CTimber::setDebugLevel(int value)
{
  DGClockMutex(&debug_level_mutex);
  debug_level = value;
  DGCunlockMutex(&debug_level_mutex);
}



int CTimber::sendDebug()
{
  DGClockMutex(&debug_level_mutex);
  if (debug_level > 0)
    cout << "beginning of sendDebug" << endl;
  DGCunlockMutex(&debug_level_mutex);

  char* test_string = new char[MESSAGE_LENGTH];
  memset(test_string, 'A', MESSAGE_LENGTH-1);
  test_string[MESSAGE_LENGTH-1] = '\0';
  SendTimberString(sendSocket, test_string);
  delete [] test_string;

  DGClockMutex(&debug_level_mutex);
  if (debug_level > 0)
    cout << "end of sendDebug" << endl;
  DGCunlockMutex(&debug_level_mutex);

  return 0;
}


/* Thread for receiving logging information */
void CTimber::RecvThread()
{
  DGClockMutex(&debug_level_mutex);
  if (debug_level > 0)
    cout << "CTimber::RecvThread() has been called" << endl;
  DGCunlockMutex(&debug_level_mutex);
  
  int recvSocket = m_skynet.listen(SNtimberstring, ALLMODULES);
  char* p_buffer = new char[MESSAGE_LENGTH];
  char* command = new char[MESSAGE_LENGTH];
  
  while (!shutdown)
    {
      // get a message
      DGClockMutex(&debug_level_mutex);
      if (debug_level > 0)
	cout << "trying to receive message...";
      DGCunlockMutex(&debug_level_mutex);

      RecvTimberString(recvSocket, p_buffer);
      
      DGClockMutex(&debug_level_mutex);
      if (debug_level > 0)
	cout << "got message \"" << p_buffer << "\"" << endl;
      DGCunlockMutex(&debug_level_mutex);
      
      getCommand(command, p_buffer);

      if (command[0] != '\0')
	{
	  // actuall have a command that we may want to add to the set
	  DGClockMutex(&debug_level_mutex);
	  if (debug_level > 0)
	    cout << "adding command to set \"" << command << "\"... ";
	  DGCunlockMutex(&debug_level_mutex);
      
	  pair <set<string>::iterator, bool> insertResult;
	  insertResult = todoSet.insert(string(command));
	  if ( (insertResult.second) )
	    {
	      DGClockMutex(&debug_level_mutex);
	      if (debug_level > 0)
		cout << "unique command!" << endl;
	      DGCunlockMutex(&debug_level_mutex);
	  
	      DGClockMutex(&todoListFile_mutex);

	      DGClockMutex(&debug_level_mutex);
	      if (debug_level > 0)
		cout << "adding to file \"" << command << " " << LOG_DESTINATION << "\"" << endl;
	      DGCunlockMutex(&debug_level_mutex);

	      todoListFile << command << " " << LOG_DESTINATION << endl;
	      DGCunlockMutex(&todoListFile_mutex);
	    }
	  else
	    {
	      DGClockMutex(&debug_level_mutex);
	      if (debug_level > 0)
		cout << "duplicate." << endl;
	      DGCunlockMutex(&debug_level_mutex);
	    }
	}

      // go back to the beginning of the while loop
    }

  /* Flush the todo list */
  cout << "Closing todo list" << endl;
  if (todoListFile.is_open())
    {
      todoListFile.flush();
      todoListFile.close();
    }
  
  delete [] p_buffer;
  delete [] command;
}


void CTimber::RecvFromGuiThread() {
  int recvSocket = m_skynet.listen(SNguiToTimberMsg, ALLMODULES);
  GUI_MSG_TYPES msg;
  while(!shutdown) {
    m_skynet.get_msg(recvSocket, &msg, sizeof(GUI_MSG_TYPES));
    cout << endl;
    switch(msg) {
    case RESUME:
      cout << OUTPUT << "resume called (via gui)" << endl;
      cout << OUTPUT << "new speed is " << 1 << endl;    
      setNewSpeedNow(1);
      break;
    case PAUSE:
      cout << OUTPUT << "pause called (via gui)" << endl;
      cout << OUTPUT << "new speed is " << 0 << endl;
      setNewSpeedNow(0);
      break;
    case REPLAY:
      cout << OUTPUT << "replay called (via gui)" << endl;
      cout << OUTPUT << "new speed is " << 0 << endl;
      
      setNewSpeedNow(0);
      
      unsigned long long real_time, play_time;
      play_time = 0;
      
      lockPlaybackState();
      play_time += humanTime2Unix(playback_state.playback_location);
      DGCgettime(real_time);
      
      playback_state.real_time_point = real_time;
      playback_state.playback_time_point = play_time * (unsigned long long)1e6;
      
      cout << OUTPUT << "new time point is (real_time, play_time, speed) = (" << real_time/(unsigned long long)1e6
	   << ", " << play_time << ", " << playback_state.playback_speed << ")" << endl;
      
      unlockPlaybackState();
      break;
    case START:
      cout << OUTPUT << "start called (via gui)" << endl;
      DGClockMutex(&logging_on_mutex);
      if (logging_on)
	{
	  DGCunlockMutex(&logging_on_mutex);
	  cout << OUTPUT << "(ignored because logging is already on)" << endl;
	}
      else
	{
	  /* Turn on data logging */
	  logging_on = true;
	  DGCunlockMutex(&logging_on_mutex);
	  setupNewSession();
	}
      break;
    case STOP:
      cout << OUTPUT << "stop called (via gui)" << endl;
      DGClockMutex(&logging_on_mutex);
      logging_on = false;
      DGCunlockMutex(&logging_on_mutex);
      break;
    case RESTART:
      cout << OUTPUT << "restart called (via gui)" << endl;
      DGClockMutex(&logging_on_mutex);
      if (!logging_on)
	{
	  logging_on = true;
	  DGCunlockMutex(&logging_on_mutex);
	  cout << OUTPUT << "but logging wasn't started in the first place." << endl;
	  cout << OUTPUT << "We'll assume you meant \"start\" though." << endl;
	}
      
      DGCunlockMutex(&logging_on_mutex);
      setupNewSession();
      break;
    default:
      cout << OUTPUT << __FILE__ << "[" << __LINE__ << "]: "
	   << "Unknown message of type " << (int)msg << " received from the gui."
	   << endl;
      break;
    }
    cout << PROMPT;
    fflush(stdout);
  }

}


/** 
 * should just constantly be sending out the current timestamp to
 * be used for logging, as well as individual logging levels
 */
void CTimber::SendThread()
{
  char* session_timestamp_copy = new char[MAX_TIMESTAMP_LENGTH];
  char* session_timestamp_old = new char[MAX_TIMESTAMP_LENGTH];
  char* message = new char[MESSAGE_LENGTH];

  while (!shutdown)
    {
      /** send the logging on or off string */
      message[0] = '\0';
      if (!logging_on)
	strcat(message, TAG_LOGOFF);
      else
	{
	  DGClockMutex(&session_timestamp_mutex);
	  strcpy(session_timestamp_copy, session_timestamp);
	  DGCunlockMutex(&session_timestamp_mutex);
	  
	  // do we have to do anything special for new logs?
	  // if (strcmp(session_timestamp_copy, session_timestamp_old) != 0)
	  strcat(message, TAG_LOGTIMEON);
	  strcat(message, session_timestamp_copy);
	  
	  ostringstream oss;
	  for (int i = 0; i < timber_types::LAST; i++)
	      oss << " " << LEVEL_ARRAY_NAME[i];
	  strcat(message, oss.str().c_str());
	}

      SendTimberString(sendSocket, message);
      

      /** send the playback string */
      message[0] = '\0';
      lockPlaybackState();
      strcat(message, TAG_PLAYINFO);
      unsigned long long time;
      DGCgettime(time);
      sprintf(message, "%s  %-30llu%-30llu%f  %s", TAG_PLAYINFO, playback_state.real_time_point,
	      playback_state.playback_time_point, playback_state.playback_speed,
	      playback_state.playback_location.c_str());
      SendTimberString(sendSocket, message);
      // cout << "**************" << message << "************" << endl;
      unlockPlaybackState();


      /** sleep to wait for the next cycle */
      usleep((unsigned int)(1000000.0 * SERVER_SLEEP));
    }

  delete [] message;
  delete [] session_timestamp_old;
  delete [] session_timestamp_copy;
}



void CTimber::setupNewSession()
{
  // we also need to update the timestamp at this point
  unsigned long long the_time;
  DGCgettime(the_time);
  long long int the_time_round = DGCtimetollsec(the_time);
  DGClockMutex(&session_timestamp_mutex);
  sprintf(session_timestamp, "%lli", the_time_round);
  string timestring(session_timestamp);
  cout << OUTPUT << "starting new session, new timestamp is " <<  timestring 
       << " (" << unixTime2Human(timestring) << ")" << endl;

  // and create a new todo file (base + timestamp)
  char* filename = new char[MAX_PATH_LENGTH]; filename[0] = '\0';
  strcat(filename, TODOFILE);
  if (TODO_TIMESTAMP_NAME)
    strcat(filename, session_timestamp);
  DGCunlockMutex(&session_timestamp_mutex);
  if (!todoListFile.is_open())
    todoListFile.open(filename, ofstream::app);

  if (TODO_EXECUTABLE)
    {
      //change permissions
      char* permissions_command = new char[MAX_COMMAND_LENGTH];
      strcat(permissions_command, TODOPERMISSIONS);
      strcat(permissions_command, " ");
      strcat(permissions_command, filename);
      system(permissions_command);
      delete [] permissions_command;

      todoListFile << TODOHEADER << endl;
    }

  if (TODO_TIMESTAMP_CMNT)
    todoListFile << "# " << timestring << endl;

  delete [] filename;
}



void CTimber::getCommand(char* command, char* message)
{
  command[0] = '\0';
  int length = strlen(message);

  //cout << " 1 command is \"" << command << "\"" << endl;
  if (length >= TAGLENGTH)
    {
      if ( (strncmp(message, TAG_ADDFILE, TAGLENGTH) == 0) )
	{
	  if (TODO_EXECUTABLE)
	    {
	      strcat(command, CMD_ADDFILE);
	      //cout << " 2 command is \"" << command << "\"" << endl;
	      strcat(command, " ");
	      //cout << " 3 command is \"" << command << "\"" << endl;
	      strcat(command, (message + TAGLENGTH));
	      //cout << " 4 command is \"" << command << "\"" << endl;
	      strcat(command, " ");
	      //cout << " 5 command is \"" << command << "\"" << endl;
	      catDGCDirFromFile(command);
	      //cout << " 6 command is \"" << command << "\"" << endl;
	    }
	  else
	    {
	      strcat(command, (message + TAGLENGTH));
	    }
	}
      else if ( (strncmp(message, TAG_ADDFOLDER, TAGLENGTH) == 0) )
	{
	  if (TODO_EXECUTABLE)
	    {
	      // make sure it ends with a '/', since it's a folder
	      // (makes it more obvious)
	      if (message[strlen(message) - 1] != '/')
		strcat(message, "/");

	      strcat(command, CMD_ADDFOLDER);
	      //cout << " 2 command is \"" << command << "\"" << endl;
	      strcat(command, " ");
	      //cout << " 3 command is \"" << command << "\"" << endl;
	      strcat(command, (message + TAGLENGTH));
	      //cout << " 4 command is \"" << command << "\"" << endl;
	      strcat(command, " ");
	      //cout << " 5 command is \"" << command << "\"" << endl;
	      catDGCDirFromFile(command);
	      //cout << " 6 command is \"" << command << "\"" << endl;
	    }
	  else
	    {
	      strcat(command, (message + TAGLENGTH));
	    }
	}
    }
}



/**
 * Attempts to read in the DGCDIRFILE in the current directory to get
 * the path to the root (usually will be "/home/user/dgc")
 */
void CTimber::catDGCDirFromFile(char* dest)
{
  ifstream file(DGCDIRFILE);
  if (!file.is_open())
    cout << "COULDN'T OPEN \"" << DGCDIRFILE << "\"!" << endl;
  string from_file;
  file >> from_file;
  strcat(dest, from_file.c_str());

  file.close();

  DGClockMutex(&debug_level_mutex);
  if (debug_level > 0)
    cout << "CTimber::catDGCDirFromFile() - cat'ed \"" << from_file << "\"" << endl;
  DGCunlockMutex(&debug_level_mutex);
}



void CTimber::lockPlaybackState() { DGClockMutex(&playback_state_mutex); }
void CTimber::unlockPlaybackState() { DGCunlockMutex(&playback_state_mutex); }



void CTimber::printStatus()
{
  DGClockMutex(&logging_on_mutex);
  DGClockMutex(&todoListFile_mutex);
  DGClockMutex(&session_timestamp_mutex);
  lockPlaybackState();

  string the_time_stamp = string(session_timestamp);
  // calculate playback time as double
  unsigned long long unsigned_cur_r;
  DGCgettime(unsigned_cur_r);
  long long cur_r, pt_r, pt_p;
  cur_r = (long long)unsigned_cur_r;
  pt_r = (long long)playback_state.real_time_point;
  pt_p = (long long)playback_state.playback_time_point;
  double speed = playback_state.playback_speed;
  double cur_playback_time = .000001 * (double)(pt_p + (long long)(speed * (double)(cur_r - pt_r)));  

  cout         << OUTPUT << "General"
       << endl << OUTPUT << "------------------------"
       << endl << OUTPUT << "                 skynet key   " << skynet_key
       << endl << OUTPUT << "                debug level   " << getDebugLevel()
       << endl << OUTPUT << ""
       << endl << OUTPUT << "Logging (overall)"
       << endl << OUTPUT << "------------------------"
       << endl << OUTPUT << "            logging enabled   " << (string)bool2str(logging_on)
       << endl << OUTPUT << "       todoListFile (open?)   " << bool2str(todoListFile.is_open())
       << endl << OUTPUT << "        todoListFile (name)   " << (string)TODOFILE
       << endl << OUTPUT << "          session timestamp   " << string(session_timestamp)
       << endl << OUTPUT << ""
       << endl << OUTPUT << "Logging (module specific logging levels)"
       << endl << OUTPUT << "------------------------"
       TIMBER_LOG_INFO(PRINT_INDIVIDUAL_STATUS)
       << endl << OUTPUT << ""
       << endl << OUTPUT << "Playback"
       << endl << OUTPUT << "------------------------"
       << endl << OUTPUT << "         playback directory   \"" << playback_state.playback_location << "\""
       << endl << OUTPUT << "      real time point (ull)   " << playback_state.real_time_point
       << endl << OUTPUT << "  playback time point (ull)   " << playback_state.playback_time_point
       << endl << OUTPUT << "    playback speed (double)   " << playback_state.playback_speed
       << endl << OUTPUT << "    cur playback time (abs)   ";
  printf("%f", cur_playback_time);
  cout << endl << OUTPUT << "    cur playback time (rel)   " << cur_playback_time - humanTime2Unix(playback_state.playback_location) << endl;

  DGCunlockMutex(&logging_on_mutex);
  DGCunlockMutex(&todoListFile_mutex);
  DGCunlockMutex(&session_timestamp_mutex);
  unlockPlaybackState();
}



string CTimber::bool2str(bool thebool)
{
  if (thebool)
    return "true";
  else
    return "false";
}



void CTimber::setNewSpeedNow(double new_speed)
{
  double old_speed;
  unsigned long long unsigned_cur_r;
  long long pt_r, pt_p, cur_r, cur_p;
  DGCgettime(unsigned_cur_r);
  cur_r = (long long)unsigned_cur_r;
  
  lockPlaybackState();
  // save copies of old data:
  pt_r = (long long)playback_state.real_time_point;
  pt_p = (long long)playback_state.playback_time_point;
  old_speed = playback_state.playback_speed;
  
  playback_state.playback_speed = new_speed;
  playback_state.real_time_point = unsigned_cur_r;
  playback_state.playback_time_point = (unsigned long long) \
    (pt_p + (long long)(old_speed * (double)(cur_r - pt_r)));
  unlockPlaybackState();
}



long long CTimber::humanTime2Unix(string dir_string)
{
  // typical string we'll want to convert
  // /tmp/logs/2005_07_21/20_00_49/
  
  string ds = dir_string;
  printDebug("humanTime2Unix called with string \"" + ds + "\"", 2);
  // remove last `/' from string, if it exists
  if (ds[ds.length()-1] == '/')
    ds = ds.substr(0, ds.length()-1);

  // starting at a location 12 from the end, seach for the previous `/' character
  int front_chop = ds.rfind("/", ds.length()-12);
  ds = ds.substr(front_chop + 1);
  // now string is something like 2005_07_21/20_00_49

  // remove all the _'s
  while (true)
    {
      unsigned int loc = ds.find("_");
      if (loc == string::npos)
        break;
      else
        ds.replace(loc, 1, " ");
    }

  // remove all /'s
  while (true)
    {
      unsigned int loc = ds.find("/");
      if (loc == string::npos)
        break;
      else
        ds.replace(loc, 1, " ");
    }
  printDebug("dir string, stripped of extraneous chars is \"" + ds + "\"", 2);

  int year, month, day, hour, min, sec;
  stringstream sst(ds);
  sst >> year >> month >> day >> hour >> min >> sec;

  time_t ret_time;
  time(&ret_time);
  struct tm* tmstruct;
  tmstruct = localtime(&ret_time);
  tmstruct->tm_year = year - 1900;
  tmstruct->tm_mon = month - 1;
  tmstruct->tm_mday = day;
  tmstruct->tm_hour = hour;
  tmstruct->tm_min = min;
  tmstruct->tm_sec = sec;
 
  ret_time = mktime(tmstruct);

  {
    ostringstream oss;
    oss << "humanTime2Unix returning time value of \"" << (unsigned long long)ret_time << "\"";
    printDebug(oss.str(), 2);
  }
  return (long long)ret_time;
}



/** prints out debug messages */
void CTimber::printDebug(string message, double req_level, bool newline)
{
  //  cout << "debug level is " << getDebugLevel() << " and required is " << req_level << endl;
  if (getDebugLevel() >= req_level)
    {
      cout << message;
      if (newline)
        cout << endl;
    }
}



string CTimber::unixTime2Human(string unix_time)
{
  time_t thetime = (time_t)atol(unix_time.c_str());
  tm* tmstruct = localtime(&thetime);
  char* ret_char = new char[30];  // 30 sounds good
  char* buffer = new char[30];  // 30 sounds good
  ret_char[0] = '\0';
  buffer[0] = '\0';
  //cout << "year is " << tmstruct->tm_year << endl;
  sprintf(buffer, "%04d", tmstruct->tm_year + 1900);  strcat(ret_char, buffer);
  strcat(ret_char, "_");
  sprintf(buffer, "%02d", tmstruct->tm_mon + 1);  strcat(ret_char, buffer);
  strcat(ret_char, "_");
  sprintf(buffer, "%02d", tmstruct->tm_mday);  strcat(ret_char, buffer);
  strcat(ret_char, "/");
  sprintf(buffer, "%02d", tmstruct->tm_hour);  strcat(ret_char, buffer);
  strcat(ret_char, "_");
  sprintf(buffer, "%02d", tmstruct->tm_min);  strcat(ret_char, buffer);
  strcat(ret_char, "_");
  sprintf(buffer, "%02d", tmstruct->tm_sec);  strcat(ret_char, buffer);

  string ret(ret_char);
  return ret;
}

