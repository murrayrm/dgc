//FILE DESCRIPTION: This header file defines the trajFstatusStruct data structure sent in the skynet
//message type: SNtrajFstatus, the structure houses internal variables in trajFollower that need to
//be broadcast to the outside world (as other modules need to know their values) - including superCon

//SC --> TRAJF: TrajFollower execution mode commands/position
enum trajFmode { TF_FORWARDS, TF_REVERSE };

struct trajFstatusStruct {
  
  trajFmode currentMode;
  double currentVref;
  double smallestMAXspeedCap;
  double largestMINspeedCap;
  
};
