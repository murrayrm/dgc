#ifndef SIM_HH
#define SIM_HH

#include <time.h>
#include <unistd.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>                  // min, max

#include "VehicleState.hh"
#include "sn_msg.hh"

#include "models/SimModel.hh"
#include "models/SimModelSimple.hh"

#include "simulatorTabSpecs.hh"
#include "TabClient.h"

#include "adrive_skynet_interface_types.h"
#include "adrive.h"
using namespace std;



struct SIM_DATUM
{
  /** Vehicle state struct, from VehicleState.hh. */
  VehicleState SS;
  /** Steering command [-1,1]. */
  double steer_cmd;
  /** Acceleration command [-1,1]. */
  double accel_cmd;
  /** Time stamp (UNIX time in us). */
  unsigned long long lastUpdate;
  /** Steering angle [rad]. */
  double phi;
  /** transmission command */
  double trans_cmd;
};

class asim : public CModuleTabClient
{
	bool m_bSimpleModel;
	double m_steerLag, m_steerRateLimit;
        unsigned long long  noisetime;
        int simnoise;
        bool use_estop, use_trans;

	StateReport	simState;
	SimModelSimple	simEngineSimple;
	SimModel		    simEngine;

	vehicle_t*  m_pVehicle;

	pthread_mutex_t m_stateMutex;

	void SimInit(void);
	void updateState(void);

	int broadcast_statesock;
 
  /*!Used for guiTab.*/
  SasimTabInput  m_input;
  SasimTabOutput m_output;

public:
  asim(int sn_key, bool bSimpleModel, double steerLag, double steerRateLimit, int noise, bool estop, bool trans);
  ~asim();

  void StateComm();
  void DriveComm();

  void Active();

  void broadcast();

  void SimUpdate(float deltaT);

  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

	struct vehicle_t* getADriveVehicle(void)
	{
		return m_pVehicle;
	}
};

#endif
