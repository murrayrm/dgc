#include <stdlib.h>
#include <iostream.h>
#include <stdio.h>
#include <unistd.h>
#include <fstream.h>
#include "imu_fastcom.h"

int main(int argc, char **argv)
{
  IMU_DATA my_imudata;
  IMU_DEBUG my_debug;
  char outputfile[100];
  fstream output_stream;
  sprintf(outputfile,"%s",argv[1]);
  output_stream.open(outputfile, fstream::out);
  IMU_open();
  output_stream.precision(40);
  while (1) {
    IMU_read(&my_imudata, &my_debug); // Need to kadd new code for IMU_read!
    output_stream << my_imudata.dvx << "\t";
    output_stream << my_imudata.dvy << "\t";
    output_stream << my_imudata.dvz << "\t";
    output_stream << my_imudata.dtx << "\t";
    output_stream << my_imudata.dty << "\t";
    output_stream << my_imudata.dtz << endl;
  }
}

