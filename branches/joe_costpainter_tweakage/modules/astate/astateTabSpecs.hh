#define TABNAME astate

// fields: type, name, default value, widgetType, xLabel, yLabel, xVal, yVal
// everything is TAB-RELATIVE. Thus input is FROM the client INTO the tab
#define TABINPUTS(_) \
  _(char, rawGPS, 'Z', Label, 0, 0, 1, 0) \
	_(double, error, 0., Label, 0, 1, 1, 1) \
  _(double, gpsN, 0., Label, 0, 2, 1, 2) \
  _(double, gpsE, 0., Label, 0, 3, 1, 3) \
  _(double, gpsA, 0., Label, 0, 4, 1, 4) \
  _(double, gpsVN, 0., Label, 0, 5, 1, 5) \
  _(double, gpsVE, 0., Label, 0, 6, 1, 6) \
  _(char, rawIMU, 'Z', Label, 2, 0, 3, 0) \
  _(double, dvx, 0., Label, 2, 1, 3, 1) \
  _(double, dvy, 0., Label, 2, 2, 3, 2) \
  _(double, dvz, 0., Label, 2, 3, 3, 3) \
  _(double, dtx, 0., Label, 2, 4, 3, 4) \
  _(double, dty, 0., Label, 2, 5, 3, 5) \
  _(double, dtz, 0., Label, 2, 6, 3, 6) \
  _(char, rawMag, 'Z', Label, 4, 0, 5, 0) \
  _(double, heading, 0., Label, 4, 1, 5, 1) \
  _(char, kFilter, 'Z', Label, 4, 3, 5, 3) \
  _(double, imuCount, 0., Label, 4, 4, 5, 4) \
  _(double, gpsCount, 0., Label, 4, 5, 5, 5) \
  _(double, magCount, 0., Label, 4, 6, 5, 6) \
  _(double, timestamp, 0., Label, 0, 7, 1, 7) \
  _(double, N, 0., Label, 0, 8, 1, 8) \
  _(double, E, 0., Label, 0, 9, 1, 9) \
  _(double, D, 0., Label, 0, 10, 1, 10) \
  _(double, R, 0., Label, 0, 11, 1, 11) \
  _(double, P, 0., Label, 0, 12, 1, 12) \
  _(double, Y, 0., Label, 0, 13, 1, 13) \
  _(double, dN, 0., Label, 2, 8, 3, 8) \
  _(double, dE, 0., Label, 2, 9, 3, 9) \
  _(double, dD, 0., Label, 2, 10, 3, 10) \
  _(double, dR, 0., Label, 2, 11, 3, 11) \
  _(double, dP, 0., Label, 2, 12, 3, 12) \
  _(double, dY, 0., Label, 2, 13, 3, 13) \
  _(double, ddN, 0., Label, 4, 8, 5, 8) \
  _(double, ddE, 0., Label, 4, 9, 5, 9) \
  _(double, ddD, 0., Label, 4, 10, 5, 10) \
  _(double, ddR, 0., Label, 4, 11, 5, 11) \
  _(double, ddP, 0., Label, 4, 12, 5, 12) \
  _(double, ddY, 0., Label, 4, 13, 5, 13) \
  _(double, ibx, 0., Label, 0, 14, 1, 14) \
  _(double, iby, 0., Label, 2, 14, 3, 14) \
  _(double, ibz, 0., Label, 4, 14, 5, 14) \
  _(int, skynet, 0, Label, 0, 15, 1, 15) \
  _(int, stationary, 0, Label, 2, 15, 3, 15) \
  _(int, navmode, 0, Label, 4, 15, 5, 15)

#define TABOUTPUTS(_) \
//  _(char, EnterCommand, 'Z', Entry, 2, 14, 3, 14)

//#undef TABNAME
