/*
 * Kalman initialization
 * 8/14/05 EJC
 */


#include "AState.hh"

#define _NUM_STATES 10
#define _3D_NUM_MEAS 6
#define _2D_NUM_MEAS 4
#define _OBD_NUM_MEAS 3

void AState::kfInit(){
  kfX = Matrix(_NUM_STATES);
  kfP = Matrix(_NUM_STATES,_NUM_STATES);
  
  kfdXdt = Matrix(_NUM_STATES,_NUM_STATES);
  kfA = Matrix(_NUM_STATES,_NUM_STATES);
  kfQ = Matrix(_NUM_STATES,_NUM_STATES);

  //Values for 3D GPS Measurement

  kfH3d = Matrix(_3D_NUM_MEAS,_NUM_STATES);
  kfZ3d = Matrix(_3D_NUM_MEAS);
  kfR3d = Matrix(_3D_NUM_MEAS,_3D_NUM_MEAS);

  kfH3d.setelem(0,0,1);
  kfH3d.setelem(1,1,1);
  kfH3d.setelem(2,2,1);
  kfH3d.setelem(3,3,1);
  kfH3d.setelem(4,4,1);
  kfH3d.setelem(5,5,1);

  // Values for 2D GPS Measurement

  kfH2d = Matrix(_2D_NUM_MEAS,_NUM_STATES);
  kfZ2d = Matrix(_2D_NUM_MEAS);
  kfR2d = Matrix(_2D_NUM_MEAS,_2D_NUM_MEAS);

  kfH2d.setelem(0,0,1);
  kfH2d.setelem(1,1,1);
  kfH2d.setelem(2,3,1);
  kfH2d.setelem(3,4,1);

  // Values for OBD-based Measurement
  kfHobd = Matrix(_OBD_NUM_MEAS, _NUM_STATES);
  kfZobd = Matrix(_OBD_NUM_MEAS);
  kfRobd = Matrix(_OBD_NUM_MEAS, _OBD_NUM_MEAS);

  kfHobd.setelem(0, 3, 1);
  kfHobd.setelem(1, 4, 1);
  kfHobd.setelem(2, 5, 1);


#warning need to set kfHobd values

  // K is never preset--no initialization needed
  // R is never preset--no initialization needed

  readCovariances();


  DGClockMutex(&m_MetaStateMutex);
  _metaState.kfEnabled = 1;  
  DGCunlockMutex(&m_MetaStateMutex);
}

void AState::readCovariances() { 
  int i;

  // Everything set to zero in the Matrix(row, col) creation
  // A, K, X, Z, dXdt don't need to change
  // I needs to go to Identity
  // Set elsewhere for H, dXdt
  // Read in for P, Q, R  

  double pVals[_NUM_STATES];
  double qVals[_NUM_STATES];
  
  // If we get more states, we may have to change h, but
  // right now it's straightforward.

  FILE *cf = fopen("config/kalman.config", "r");
  char line[50];
  char errorCov[50];
  double tempCov;

  // Read the entire file.
  if(cf != NULL) {
    // Get one line at a time...
    while(fgets(line, sizeof(line), cf)) {
      // And scan it to see if it has a string identifying a matrix.
      if(sscanf(line, "%s\n", errorCov) == 1) {
	// For example, here if it matches "P:"...
	if(strcmp(errorCov, "P:") == 0) {
	  // We write in a line for each diagonal value of P
	  for(i = 0; i < _NUM_STATES; i++) {
	    fgets(line, sizeof(line), cf);
	    // And check to make sure we read it ok.
	    if(sscanf(line, "%lf\n", &tempCov) == 1) {
	      pVals[i] = tempCov;
	    } else {
	      cerr << "Error reading pVals[" << i 
		   << "] in kalman.config." << endl;
	      pVals[i] = 0;
	    }
	  }
	} else if(strcmp(errorCov, "Q:") == 0) {
	  // We do the same thing for "Q:"
	  for(i = 0; i < _NUM_STATES; i++) {
	    fgets(line, sizeof(line), cf);
	    if(sscanf(line, "%lf\n", &tempCov) == 1) {
	      qVals[i] = tempCov;
	    } else {
	      cerr << "Error reading qVals[" << i 
		   << "] in kalman.config." << endl;
	      qVals[i] = 0;
	    }	    
	  }
	} else if(strcmp(errorCov, "pError:") == 0) {
	  // We do the same thing for "pError:"
	  fgets(line, sizeof(line), cf);
	  if(sscanf(line, "%lf\n", &tempCov) == 1) {
	    pError = tempCov;
	  } else {
	    cerr << "Error reading pError in kalman.config." << endl;
	  }
	} else {
	  // Nothing to do.
	}
      }
    }
  } else {
    cerr << "Error: Read failure in kalman.config--KF covariances "
	 << "not initialized!" << endl;
  }
  fclose(cf);

  for(i = 0; i < _NUM_STATES; i++) { //cols
    kfP.setelem(i,i,pVals[i]); 
    kfQ.setelem(i,i,qVals[i]); 
  }
}
