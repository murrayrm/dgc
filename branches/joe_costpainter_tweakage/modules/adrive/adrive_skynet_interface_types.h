#ifndef _ADRIVE_SKYNET_INTERFACE_TYPES_H
#define _ADRIVE_SKYNET_INTERFACE_TYPES_H
/**
 * Basically, this header defines the actuator
 * and command type and has two possible fields.  

 *Say the desired effect is command the steering to .75.
 *create the struct in memory.  aka %drivecmd my_struct;
 *then set %my_actuator = steer;
 *set %my_command_type = set_position;
 *and give the value %number_arg = .75;

 *When skynet gets this message.  It will read the actuator and command type.  
 *And set_position knows to look at the numeric_arg as opposed to the string.  

 *This is for expandability.  Such as if settings need to be changed on some
 *actuaor.  set_parameter would look at the string_arg, and ignore the number. 
 **/


/*! This is the list of all the actuator types for which adrive will 
 * listen for commands, gas and brake individually are legacy. They will 
 * eventually go away. */
typedef enum
  {
    steer,
    accel,
    estop,
    trans
  } adrive_actuator_type_enum_t;

/*! These are the command types for the actuators.  */
typedef enum
  {
    //  get_position,
    // get_pressure,
    set_position,
    // set_parameter,
    // get_status,
    // get_error
    // set_parameter,
    set_velocity,
    set_acceleration,
  } adrive_command_type_enum_t;

/*! These are the return_types for adrive when it is returning a request 
 * for status. */
/* I am removing return functionality but leaving the code in place in 
 * case it wants to be reimplemented */
typedef enum
  {
    return_status,
    return_position,
    return_pressure,
    return_error
  } adrive_return_type_enum_t;  


/*! This is the structure of all adrive commands. Include this header and 
* sent all adrive commands in this struct.  */
typedef struct 
{
  adrive_actuator_type_enum_t my_actuator;
  adrive_command_type_enum_t my_command_type;
  double number_arg;
  char string_arg[25];
} drivecmd_t;

/*! This the struct in which adrive will return position requests.  Include
* this header and parse it directly out of this struct.  */
typedef struct
{
  adrive_actuator_type_enum_t my_actuator;
  adrive_return_type_enum_t my_return_type;
  double number_arg;
  char string_arg[25];
} drivecom_t;


#endif //_ADRIVE_SKYNET_INTERFACE_TYPES_H

