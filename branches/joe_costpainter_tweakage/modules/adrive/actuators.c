/*!* This file holds standardized function calls from adrive
 * which in turn call the individual driver for actuators. */

#include "actuators.h"


// Look for the vehicle structure. 
extern struct vehicle_t my_vehicle;
extern int simulation_flag;
extern char simulator_IP;

/******************Brake Function Calls *****************
 * are below */
/*
 * functionalitiy with multithreaded adrive.  Basically standardized function
 * calls that call functions already defined in the existing code.  
 *  Tully Foote
 * 
 ************************************************************************/
/*! The standadized function call to execute brake command*/
int execute_brake_command( double command ) 
{
  double brake_command;
  brake_command = command;
  //printf("Executing Brake Command %d\n", command);
  if (brake_command >= 0 && brake_command <= 1)
    {
      if (brake_setposition(brake_command) == FALSE)
	{
	  // serial write failed
	  my_vehicle.actuator[ACTUATOR_BRAKE].error = TRUE;
	  return FALSE;
	}
      else
	return TRUE;
    }
  else
    // command out of range
    return FALSE;
}

/*! The standadized function call to execute brake status*/
int execute_brake_status()
{
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator_trans.update_time);

  double temp_val = 0;
  //printf("Executing Brake Status \n");
  /* No feedback for this either yet.  Set to commanded position. */
  //my_vehicle.actuator[ACTUATOR_BRAKE].position = my_vehicle.actuator[ACTUATOR_BRAKE].command;
  // I have reimplimented the brake feedback loop since it should now be functional.
  
  //  brake_getposition();
  if ( (temp_val = brake_getposition()) == -1)
    {
      my_vehicle.actuator[ACTUATOR_BRAKE].error = TRUE;
      return FALSE;
    }
  else 
    // cout << "!!!!!!!!!!!!!!!!!!!!!!!!temp_vall is " << temp_val << " for the prake position" <<endl;
    my_vehicle.actuator[ACTUATOR_BRAKE].position = temp_val;
  if ( (temp_val = brake_getpressure()) == -1)
    {
      my_vehicle.actuator[ACTUATOR_BRAKE].error = TRUE;
      return FALSE;
    }
  else 
    my_vehicle.actuator[ACTUATOR_BRAKE].pressure = temp_val;
  
  return TRUE;
}

/*! The standadized function call to execute brake initialization*/
void execute_brake_init()
{
  printf("Executing Brake Init \n");
  if (simulation_flag)
    {
      //      printf("Sim");
      simulator_brake_open(my_vehicle.actuator[ACTUATOR_BRAKE].port);
    }
  else if(brake_open(my_vehicle.actuator[ACTUATOR_BRAKE].port) != TRUE)
    {
      printf("Brake open failed\n");
      my_vehicle.actuator[ACTUATOR_BRAKE].status = 0;

    }
  else
    {
      //printf("Brake OK\n");
      my_vehicle.actuator[ACTUATOR_BRAKE].status = 1;
    }
}



/******************Trans Function Calls *****************
 * are below */
/**********************These are placeholder functions */

/*! The standadized function call to execute transmission command*/
int execute_trans_command( double command ) 
{
  //printf("Executing Trans Command \n");
  // make sure we actually have to send a command
  if (my_vehicle.actuator_trans.command != my_vehicle.actuator_trans.position)
    {
      if (my_vehicle.actuator_estop.position == PAUSE )  //Make sure we're in pause
	{
	  if (my_vehicle.actuator_obdii.status == 1)  // Make sure OBDII is running
	    {
	      if (my_vehicle.actuator_obdii.VehicleWheelSpeed < SPEED_STOPPED_THRESHOLD)  
		// Make sure we're stopped.  
		{
		  // If all those are met shift.
		  trans_setposition((int) my_vehicle.actuator_trans.command);
		}
	      else
		{
		  // Not at zero speed
		  //	  cout << "NOT STOPPED NO SHIFTING!" << endl;
		}
	    }
	  else 
	    {
#warning Uncomment for race
	      //cout << "OBDII invalid no shifting" << endl;
	      // If OBDII has failed put execution of tran status here.  
	      //trans_setposition((int) my_vehicle.actuator_trans.command);
	      // THhis is commented out to prevent unsafe shifting during testing
	      // During the race we should not allow OBDII failure to block shifting completely	      
	    }
	}
      else
	{
	  // We're not in pause
	}
    }


#warning FIX THIS BEFORE USING THE IGNITION  
#ifdef UNUSED      
  if (my_vehicle.actuator_trans.ignition_command != my_vehicle.actuator_trans.ignition_position)
    {
      

      if (0)
	{
	  ign_setposition((int)my_vehicle.actuator_trans.ignition_command);
	}
  }
#endif //unused  
  
  if (my_vehicle.actuator_trans.led1_command != my_vehicle.actuator_trans.led1_position)
    {
      led_setposition(1, my_vehicle.actuator_trans.led1_command);
    }
  if (my_vehicle.actuator_trans.led2_command != my_vehicle.actuator_trans.led2_position)
    {
      led_setposition(1, my_vehicle.actuator_trans.led2_command);
    }
  if (my_vehicle.actuator_trans.led3_command != my_vehicle.actuator_trans.led3_position)
    {
      led_setposition(1, my_vehicle.actuator_trans.led3_command);
    }
  if (my_vehicle.actuator_trans.led4_command != my_vehicle.actuator_trans.led4_position)
    {
      led_setposition(1, my_vehicle.actuator_trans.led4_command);
    }
  
  return 1;
  
}

/*! The standadized function call to execute transmission status*/
int execute_trans_status()
{
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator_trans.update_time);

  //printf("Executing Trans Status \n");
  my_vehicle.actuator_trans.position = trans_getposition();
  my_vehicle.actuator_trans.ignition_position = ign_getposition();
  my_vehicle.actuator_trans.led1_position = led_getposition(1);
  my_vehicle.actuator_trans.led2_position = led_getposition(2);
  my_vehicle.actuator_trans.led3_position = led_getposition(3);
  my_vehicle.actuator_trans.led4_position = led_getposition(4);
  return 1;
}

/*! The standadized function call to execute transmission initialization*/
void execute_trans_init()
{
  printf("Executing Trans Init \n");
  my_vehicle.actuator_trans.status = igntrans_open(my_vehicle.actuator_trans.port);
}



/******************Estop Function Calls *****************
 * are below */
/**********************These are placeholder functions */
/*! The standadized function call to execute estop command*/
int execute_estop_command( double command ) 
{
  printf("Executing Estop Command %g, you should not be doing this!\n", command);
  return 1;
}

/*! The standadized function call to execute estop status*/
int execute_estop_status()
{
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator_estop.update_time);
  unsigned long long the_current_time;
  static unsigned long long end_sleep_time;
  static int timeout_enabled;

  //  printf("about to read status");
  int  current_estop_status =  estop_status();
  //    printf("i have read status %d, replacing %d\n", current_estop_status, my_vehicle.actuator_estop.dstop);

  //printf("Executing Estop Status \n");

  /* This is a level of estop that adrive will react to a disable without commands
   * coming down the chain.  
   *
   * In case of disable adrive will:
   * Put the brakes on full
   * Set the throttle to zero
   * Center the steering wheel
   *
   */
  if ( current_estop_status == DISABLE)
    {
      // Make sure that we're not timing for a reenable
      timeout_enabled = false;
      
      // Record that we're in darpa diaable
      my_vehicle.actuator_estop.dstop = DISABLE; 
      
      //Apply Full Brakes
      my_vehicle.actuator[ACTUATOR_BRAKE].command = 1; 
      pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
      
      // Center the steering
      my_vehicle.actuator[ACTUATOR_STEER].command = 0; 
      pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_STEER].cond_flag );

      // Zero the gas
      my_vehicle.actuator[ACTUATOR_GAS].command = 0;
      pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );

    }

  /* This is the case when we are in pause.
   * if we are not using mode man this will be enabled by the --nomm flag.  
   * gas will be set to zero and brake to half whenever in pause.
   */

  else if ( current_estop_status == PAUSE)
    {
      // Make sure that we're not timing for a reenable
      timeout_enabled = false;
      
      // Record that we're in darpa pause
      my_vehicle.actuator_estop.dstop = PAUSE; 
      
      //Apply Partial  Brakes defined in adrive.h
      my_vehicle.actuator[ACTUATOR_BRAKE].command = BRAKE_PAUSE_POSITION; 
      pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_BRAKE].cond_flag );
      
      // Zero the gas
      my_vehicle.actuator[ACTUATOR_GAS].command = 0;
      pthread_cond_broadcast( & my_vehicle.actuator[ACTUATOR_GAS].cond_flag );
      
      //Check if this is returning from a diable and reenable the steering automatically
      if (my_vehicle.actuator_estop.dstop == DISABLE)
	{
	  //cout << "overiding steering disable in pause loop" << endl;
	  pthread_mutex_lock(&(my_vehicle.actuator[ACTUATOR_STEER].mutex));
	  steer_enable_overide();
	  pthread_mutex_unlock(&(my_vehicle.actuator[ACTUATOR_STEER].mutex));
	}
    }
  
  
  // Deal with the 5 second delay required coming out of pause into run.  
  //   Including the possibility of jumping straight out of disable.  

  else if (my_vehicle.actuator_estop.dstop != RUN &&  current_estop_status == RUN)
    {
      //      cout << "trying to go to run" << endl;
      if (!timeout_enabled)
	{
	  timeout_enabled = true;
	  DGCgettime(end_sleep_time);
	  end_sleep_time += DARPA_PAUSE_TIMEOUT;
	  //cout << end_sleep_time << " is what the end time is set to" << endl;
	  return 1;
	}
      else
	{
	  DGCgettime(the_current_time);
	  //  cout << the_current_time << " is the current time" << endl;
	  // printf("about to sleep for 5");
	  //cout << end_sleep_time - the_current_time << " is the current  time diff" << endl;
	  
	  
	  if (the_current_time > end_sleep_time)
	    {
	      // Reset the timout
	      timeout_enabled = false;
	      //	      cout << "resetting the timeout" << endl;
	      my_vehicle.actuator_estop.dstop = RUN;
	      return 1; 
	    }
	  else
	    {
	      //	      printf("i have not yet slept for 5");
	      return 1;
	    }
	  
	  	
	    
	    //THey didn't match so don't act.
	  return 1;
	}
    }
  // Finally return run since all other cases should be exhausted.  
  my_vehicle.actuator_estop.dstop =  current_estop_status;
  //  printf("ending estops_execute_status()\n");
  return 1;
}

/*! The standadized function call to execute estop initialization*/
void execute_estop_init()
{
  //printf("Executing Estop Init \n");
  if(estop_open(my_vehicle.actuator_estop.port) != TRUE)
    {
      // If it fails set the status to 0.  
      printf("EStop failed to initialize\n");
      my_vehicle.actuator_estop.status = 0;
    }
  else
    {
      my_vehicle.actuator_estop.status = 1;
    }
}

void execute_estop_close()
{
  estop_close();
}


/******************Steer Function Calls *****************
 * are below */

/*! The standadized function call to execute steer command*/
int execute_steer_command( double command ) 
{
  double steer_command = command;
  //printf("Executing Steer Command %d\n", steer_command);
  if (my_vehicle.protective_interlocks)
    {
      // Check that the OBDII is working and if so we are moving
      if (my_vehicle.actuator_obdii.status == 1)
	{
	  // Don't turn the wheel when we are stopped
	  if (my_vehicle.actuator_obdii.VehicleWheelSpeed <= SPEED_STOPPED_THRESHOLD)
	    {
	      //	  printf("We're effectively stopped no steer");
	      return 0;
	    }
	  // Don't turn the wheel when we're stopping for a pause
	  // less than 2 mps
	  if (my_vehicle.actuator_estop.position == PAUSE && 
	      my_vehicle.actuator_obdii.VehicleWheelSpeed <= 1)
	    {
	      if (my_vehicle.actuator_estop.position == PAUSE && 
		  my_vehicle.actuator_obdii.VehicleWheelSpeed <= 2)
		// printf("We're slow and paused no steer");
		// steer_disable();
		
		return 0;
	    }
	}
    }

  if (steer_command <= 1 && steer_command >= -1)
    {
      steer_heading(steer_command);
      return 1;
    }
  else 
    return 0;
}

/*! The standadized function call to execute steering velocity */
int execute_steer_velocity( double command ) 
{
  double steer_command = command;
  //printf("Executing Steer Command %d\n", steer_command);
  if (steer_command <= 1 && steer_command >= 0)
    {
      // Check whether the actuator is enabled first
      if (my_vehicle.actuator[ACTUATOR_STEER].command_enabled == 1)
	steer_setvel(steer_command);
      return 1;
    }
  else 
    return 0;
}

/*! The standadized function call to execute steering acceleration */
int execute_steer_acceleration( double command ) 
{
  double steer_command = command;
  //printf("Executing Steer Command %d\n", steer_command);
  if (steer_command <= 1 && steer_command >= 0)
    {
      // Check whether the actuator is enabled first
      if (my_vehicle.actuator[ACTUATOR_STEER].command_enabled == 1)
	steer_setaccel(steer_command);
      return 1;
    }
  else 
    return 0;
}




/*! The standadized function call to execute steer status*/
int execute_steer_status()
{
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator_trans.update_time);

  int retval;
  //  printf("Executing Steer Status \n");

  
  // removed JCL 24 May 2005 due to new steer_getposition implementation 
  //steer_state_update(FALSE, FALSE);
  //usleep(100000);
  //#warning THis sleep is very bad! Required due to  SDS bug.  
  

  my_vehicle.actuator[ACTUATOR_STEER].position = (double) steer_getheading();
  return retval;
}

/*! The standadized function call to execute steer initialization*/
void execute_steer_init()
{
  //printf("Executing Steer Init \n");

  if(simulation_flag)
    {
      simulator_steer_open(my_vehicle.actuator[ACTUATOR_STEER].port);
      pthread_cond_broadcast(&steer_calibrated_flag);
    }
  
  else if(steer_open(my_vehicle.actuator[ACTUATOR_STEER].port) != TRUE)
    {
      my_vehicle.actuator[ACTUATOR_STEER].status = 0;
      //abort();
      // This will start up things that are waiting on the steering.  
      printf("Steer init failed, continuing anyway.\n");
      pthread_cond_broadcast(&steer_calibrated_flag);
      
    }
  else
	{
	  //printf("Steer_opened properly\n");
	  my_vehicle.actuator[ACTUATOR_STEER].status = 1;
	  pthread_cond_broadcast(&steer_calibrated_flag);
	  //printf("I ihave sent the wakeup\n");
	}
}

void execute_steer_close()
{
  steer_close();
}





/******************Gas Function Calls *****************
 * are below */

/*! The standadized function call to execute gas initialization*/
int execute_gas_command( double command ) 
{
  double throttle_command;
  throttle_command = command;
  //  printf("Executing Gas Command %f\n", throttle_command);
  if (throttle_command <= 1 && throttle_command >=0)
    {
      if ( throttle_setposition(throttle_command) == FALSE )
	{
	  my_vehicle.actuator[ACTUATOR_GAS].error = 1;
	  // serial write failed
	  return FALSE;
	}
      return TRUE;
    }
  // command out of range
  return FALSE;
}

/*! The standadized function call to execute gas status*/
int execute_gas_status()
{
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator_trans.update_time);

  double retval;
  //printf("Executing Gas Status \n");
  if (  (retval = throttle_getposition()) == -1) //AKA an error
  {
    my_vehicle.actuator[ACTUATOR_GAS].error = 1;
    return FALSE;
  }
  else 
    my_vehicle.actuator[ACTUATOR_GAS].position = retval;
  //printf("Throttle get_position() returns %d\n", throttle_getposition());
  return TRUE;
}

/*! The standadized function call to execute gas initialization*/
void execute_gas_init()
{
  //printf("Executing Gas Init \n");
  if(simulation_flag)
    {
      simulator_throttle_open(my_vehicle.actuator[ACTUATOR_GAS].port);
      printf("Throttle open failed!!!!!!\n");
      my_vehicle.actuator[ACTUATOR_GAS].status = 0;
      //      exit(1);
    }
  else if(throttle_open(my_vehicle.actuator[ACTUATOR_GAS].port) != TRUE)
    {
      printf("THROTTLE: open failed!!!!!!\n");
      printf("THROTTLE: Is the serial cable plugged into the controller box and /dev/ttyS%d?\n", my_vehicle.actuator[ACTUATOR_GAS].port);
      printf("THROTTLE: Is the throttle power switch on the dashboard turned on?\n");
      printf("THROTTLE: Is the light switch on the estop box turned on?\n");
      printf("THROTTLE: Is the estop in run or pause mode?\n");
      printf("THROTTLE: Are the amber strobe lights on the roof flashing?\n");

      my_vehicle.actuator[ACTUATOR_GAS].status = 0;
      //      exit(1);
    }
  else
    {
      my_vehicle.actuator[ACTUATOR_GAS].status = 1;
      //printf("Throttle OK\n");
    }
}

void execute_gas_close()
{
  throttle_close();
}


/*** THIS IS THE CODE FOR THE OBDII INTERFACE ***/
/*! The standadized function call to execute brake status*/
int execute_obdii_status()
{
  //Stamp the time that the data was updated.  
  DGCgettime(my_vehicle.actuator_trans.update_time);

  //printf("Executing OBDII Status \n");
  int retval = 0;
  my_vehicle.actuator_obdii.my_obdii_driver.getRPM(  my_vehicle.actuator_obdii.engineRPM );
  my_vehicle.actuator_obdii.my_obdii_driver.getTimeSinceEngineStart(my_vehicle.actuator_obdii.TimeSinceEngineStart);
  my_vehicle.actuator_obdii.my_obdii_driver.getVehicleSpeed(my_vehicle.actuator_obdii.VehicleWheelSpeed);
  my_vehicle.actuator_obdii.my_obdii_driver.getEngineCoolantTemp(my_vehicle.actuator_obdii.EngineCoolantTemp);
  my_vehicle.actuator_obdii.my_obdii_driver.getTorque(my_vehicle.actuator_obdii.WheelForce  );
  my_vehicle.actuator_obdii.WheelForce *= my_vehicle.actuator_obdii.CurrentGearRatio * TORQUE_CONVERTER_EFFICIENCY * DIFFERENTIAL_RATIO / VEHICLE_TIRE_RADIUS * US_FOOT_POUNDS_TO_NEWTON_METERS;
  my_vehicle.actuator_obdii.my_obdii_driver.getGlowPlugLampTime(my_vehicle.actuator_obdii.GlowPlugLampTime);
  my_vehicle.actuator_obdii.my_obdii_driver.getThrottlePosition(my_vehicle.actuator_obdii.ThrottlePosition);
  my_vehicle.actuator_obdii.my_obdii_driver.getCurrentGearRatio(my_vehicle.actuator_obdii.CurrentGearRatio);
  my_vehicle.actuator_obdii.my_obdii_driver.getBatteryVoltage(my_vehicle.actuator_obdii.BatteryVoltage);  
  return retval;
}

/*! The standadized function call to execute obdii initialization*/
void execute_obdii_init()
{
  //  ostringstream devicestring("");
  //devicestring << "/dev/ttyS" << ;
  //cout << devicestring.str() <<endl;
  my_vehicle.actuator_obdii.status = my_vehicle.actuator_obdii.my_obdii_driver.connect((int)my_vehicle.actuator_obdii.port);
}


int execute_obdii_command(double arg)
{
  cout << "You shouldn't be commanding the OBDII yet" << endl;
  return FALSE;
}

int obdii_is_valid()
{
  if ( my_vehicle.actuator_obdii.status_enabled == 1)
    {
      // Check for obdii validity
      if ( my_vehicle.actuator_obdii.EngineCoolantTemp == -40)
	{
	  my_vehicle.actuator_obdii.status = 0;
	  return 0;
	}
      else 
	{	
	  my_vehicle.actuator_obdii.status = 1;
	  return 1;
	}
    }
  return 0;
}

void execute_obdii_close()
{
  my_vehicle.actuator_obdii.my_obdii_driver.disconnect();
}
