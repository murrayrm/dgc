/* This is an implementation of a shell class that inherits from AdriveTImberClient
 * because adrive is written primarily in C and thus cannot inherit the class
 * as a whole. */


#include "AdriveTimberClient.hh"
using namespace std;

AdriveTimberClient::AdriveTimberClient(int skynetKey) : CSkynetContainer(SNadrive, skynetKey), CTimberClient(timber_types::adrive)
{
  // do something here if you want
}




AdriveTimberClient::~AdriveTimberClient()
{
  // Do we need to destruct anything??
  cout << "AdriveTimberClient destructor has been called." << endl;
}
