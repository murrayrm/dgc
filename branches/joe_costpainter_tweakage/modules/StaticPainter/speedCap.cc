using namespace std;
#include "rddf.hh"
#include "ggis.h"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fstream>
#define MAX_RDDF_FILE_PATH_CHARS 256

int main(int argc, char **argv) {
  double listedSpeed, speed;
  char inputfile[MAX_RDDF_FILE_PATH_CHARS];
  char outputfile[MAX_RDDF_FILE_PATH_CHARS];
  char speedTemp[20];
  string line;

  cout << "Enter the filename to be capped: " << endl;
  scanf("%s", inputfile);
  
  cout << "Enter the output filename: " << endl;
  scanf("%s", outputfile);

  cout << "Enter the velocity in mph to cap the speed at: " << endl;
  scanf("%s", speedTemp);
  speed = atof(speedTemp);
  
  ifstream fileIn;
  fileIn.open(inputfile, ios::in);

  ofstream fileOut;
  fileOut.open(outputfile, ios::out);

#warning There has got to be a less kludgy way to do this.
  int i = 0;
  while(!fileIn.eof()) {
    getline(fileIn,line,RDDF_DELIMITER);
    if (i == 4) {
      listedSpeed = atof(line.c_str());
      fileOut << (listedSpeed > speed? speed: listedSpeed) << RDDF_DELIMITER;
    } else {
      fileOut << line << RDDF_DELIMITER;
    }
    i = (i + 1)%7;
  }
}
