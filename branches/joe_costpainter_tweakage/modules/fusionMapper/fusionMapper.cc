#include "fusionMapperTabSpecs.hh"
#include "fusionMapper.hh"

FusionMapper::FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg, int waitForState)
  : CSkynetContainer(SNfusionmapper, skynetKey),
    CStateClient(waitForState),
		CModuleTabClient(&m_input, &m_output)
{
  printf("Welcome to fusionmapper init\n");
  _QUIT = 0;
  _PAUSE = 0;

  mapperOpts = mapperOptsArg;

  // static allocated for memory efficiency, if larger than 100 pts, change this
  m_pRoadReceive = new double[MAX_ROAD_SIZE * 3 + 1];
  m_pRoadX = new double[MAX_ROAD_SIZE];
  m_pRoadY = new double[MAX_ROAD_SIZE];
  m_pRoadW = new double[MAX_ROAD_SIZE];
  m_pRoadPoints = 0;

  fusionMap.initMap(CONFIG_FILE_DEFAULT_MAP);


  speedMap.initMap(CONFIG_FILE_SPEED_MAP);
  layerID_sampledCost = speedMap.addLayer<double>(CONFIG_FILE_COST, true);
  layerID_sampledCorridor = speedMap.addLayer<double>(CONFIG_FILE_RDDF);
	

  layerID_cost      = fusionMap.addLayer<double>(CONFIG_FILE_COST,            true);
  layerID_corridor  = fusionMap.addLayer<double>(CONFIG_FILE_RDDF);
  layerID_static    = fusionMap.addLayer<double>(CONFIG_FILE_STATIC);
  layerID_road      = fusionMap.addLayer<double>(CONFIG_FILE_ROAD);
	layerID_ladarElev = fusionMap.addLayer<double>(CONFIG_FILE_LADAR_MEAN_ELEV, true);
	layerID_fusedElev = fusionMap.addLayer<CElevationFuser>(CONFIG_FILE_LADAR_FUSED_ELEV, false);
	layerID_fusedElevStdDev = fusionMap.addLayer<double>(CONFIG_FILE_LADAR_STDDEV, true);
	layerID_fusedYet = fusionMap.addLayer<int>(0, -1, true);
	//layerID_supercon = fusionMap.addLayer<int>(CONFIG_FILE_SUPERCON, false);

  fusionCorridorPainter.initPainter(&(fusionMap), 
																		layerID_cost, 
																		&(fusionRDDF),
																		1.0,
																		1e-3,
																		5.0,
																		4.5,
																		0.5,
																		1);

  fusionCorridorReference.initPainter(&(fusionMap), layerID_corridor, &(fusionRDDF), 1.0, 1e-3, 5, 4.5, .5, 0);
  


	fusionSampledCorridorPainter.initPainter(&(speedMap), 
																					 layerID_sampledCost, 
																					 &(fusionRDDF),
																					 1.0,
																					 1e-3,
																					 5.0,
																					 4.5,
																					 0.5,
																					 1);

	fusionSampledCorridorReference.initPainter(&(speedMap), layerID_sampledCorridor, &(fusionRDDF), 1.0, 1e-3, 5, 4.5, .5, 0);


  fusionCostPainter.initPainter(&(fusionMap),
																layerID_cost,
																layerID_fusedElev,
																layerID_corridor, layerID_static,layerID_road, layerID_fusedElev, mapperOpts.optNonBinary,
																layerID_grown, 1.5, 0,
																&(speedMap),
																layerID_sampledCost,
																layerID_sampledCorridor,
																layerID_fusedYet,
																layerID_supercon
																);  

  DGCcreateMutex(&m_mapMutex);

  ReadConfigFile();
  fusionCostPainter.setMapperOpts(& mapperOpts );
  fusionCorridorPainter.setPainterOptions(&(mapperOpts.corridorOpts));
	fusionSampledCorridorPainter.setPainterOptions(&(mapperOpts.corridorOpts));
  double temp = mapperOpts.corridorOpts.optMaxSpeed;
  mapperOpts.corridorOpts.optMaxSpeed = mapperOpts.optMaxSpeed;
  fusionCorridorReference.setPainterOptions(&(mapperOpts.corridorOpts));
	fusionSampledCorridorReference.setPainterOptions(&(mapperOpts.corridorOpts));
  mapperOpts.corridorOpts.optMaxSpeed = temp;
}


FusionMapper::~FusionMapper() {
  //FIX ME
  DGCdeleteMutex(&m_mapMutex);
  printf("Thank you for using the fusionmapper destructor\n");
}


void FusionMapper::ActiveLoop() {
  //TODO - Give this a sparrow/sngui display
  printf("Entering FusionMapper active-loop\n");
  printf("Press Ctrl+C to quit\n");
  fusionCostPainter.setMapperOpts(&mapperOpts);
	int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
	int lowResSpeedDeltaSendingSocket = m_skynet.get_send_sock(SNlowResSpeedDeltaMap);
  int elevDeltaSendingSocket = m_skynet.get_send_sock(SNfusionElevDeltaMap);
  int stddevDeltaSendingSocket = m_skynet.get_send_sock(SNfusionDeltaMapStdDev);

  NEcoord exposedRow[4], exposedCol[4];

	NEcoord frontLeftAliceCorner, frontRightAliceCorner, rearLeftAliceCorner, rearRightAliceCorner;

	while(!_QUIT) {
		int deltaSize = 0;
		//UpdateState();
		WaitForNewState();
		_infoFusionMapper.startProcessTimer();
		if(!_PAUSE) {
	
			DGClockMutex(&m_mapMutex);
			fusionMap.updateVehicleLoc(m_state.Northing, m_state.Easting);

			speedMap.updateVehicleLoc(m_state.Northing, m_state.Easting);
		
			if(fusionMap.getExposedRowBoxUTM(exposedRow) ||
				 fusionMap.getExposedColBoxUTM(exposedCol)) {
	  
				//Just in case we evaluated the first condition of the if statement to true:
				fusionMap.getExposedColBoxUTM(exposedCol);
				
				// 				frontLeftAliceCorner = NEcoord(m_state.Northing + DIST_FRONT_AXLE_TO_FRONT*cos(m_state.Yaw) + VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																			 m_state.Easting + DIST_FRONT_AXLE_TO_FRONT*sin(m_state.Yaw) - VEHICLE_WIDTH/2.0*cos(m_state.Yaw));
				// 				frontRightAliceCorner = NEcoord(m_state.Northing + DIST_FRONT_AXLE_TO_FRONT*cos(m_state.Yaw) - VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																				m_state.Easting + DIST_FRONT_AXLE_TO_FRONT*sin(m_state.Yaw) + VEHICLE_WIDTH/2.0*cos(m_state.Yaw));
				// 				rearLeftAliceCorner = NEcoord(m_state.Northing + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*cos(m_state.Yaw) + VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																			m_state.Easting + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*sin(m_state.Yaw) - VEHICLE_WIDTH/2.0*cos(m_state.Yaw));
				// 				rearRightAliceCorner = NEcoord(m_state.Northing + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*cos(m_state.Yaw) - VEHICLE_WIDTH/2.0*sin(m_state.Yaw),
				// 																			 m_state.Easting + (DIST_FRONT_AXLE_TO_FRONT - VEHICLE_LENGTH)*sin(m_state.Yaw) + VEHICLE_WIDTH/2.0*cos(m_state.Yaw));
				// 				/*
				// 				cout << setprecision(10);
				// 				cout << "fl: "; frontLeftAliceCorner.display();
				// 				cout << "fr: "; frontRightAliceCorner.display();
				// 				cout << "bl: "; rearLeftAliceCorner.display();
				// 				cout << "br: "; rearRightAliceCorner.display();
				// 				cout << "guh? "; NEcoord(m_state.Northing + DIST_FRONT_AXLE_TO_FRONT*cos(m_state.Yaw),
				// 																 m_state.Easting + DIST_FRONT_AXLE_TO_FRONT*sin(m_state.Yaw)).display();
				// 				cout << "orig "; NEcoord(m_state.Northing, m_state.Easting).display();
				// 				cout << "cos is: " << VEHICLE_WIDTH/2.0*cos(m_state.Yaw)
				// 						 << ", sin is " << VEHICLE_WIDTH/2.0*sin(m_state.Yaw);
				// 				cout << endl;
				// 				*/

				// 				//Check to see if Alice is inside the corridor - if not
				// 				if(!fusionRDDF.isPointInCorridor(frontLeftAliceCorner) ||
				// 					 !fusionRDDF.isPointInCorridor(frontRightAliceCorner) ||
				// 					 !fusionRDDF.isPointInCorridor(rearLeftAliceCorner) ||
				// 					 !fusionRDDF.isPointInCorridor(rearRightAliceCorner)) {
				// 					//Then we need to expand the corridor where we are now and repaint the whole map
				// 					int largestWayptNum = fusionRDDF.findWaypointWithLargestWidth(NEcoord(m_state.Northing, m_state.Easting));
				// 					double currentOffset = fusionRDDF.getOffset(largestWayptNum) - fusionRDDF.getExtraWidth();
				// 					double distanceToTrackline = fmax(fmax(fusionRDDF.getDistToTrackline(frontLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(frontRightAliceCorner)),
				// 																						fmax(fusionRDDF.getDistToTrackline(rearLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(rearRightAliceCorner)));
				// 					/*
				// 					cout << "Dist is " << distanceToTrackline 
				// 							 << ", fl: " << fusionRDDF.getDistToTrackline(frontLeftAliceCorner)
				// 							 << ", fr: " << fusionRDDF.getDistToTrackline(frontRightAliceCorner)
				// 							 << ", bl: " << fusionRDDF.getDistToTrackline(rearLeftAliceCorner)
				// 							 << ", br: " << fusionRDDF.getDistToTrackline(rearRightAliceCorner)
				// 							 << endl;
				// 					*/
				// 					double widthToAdd = distanceToTrackline - currentOffset;
				// 					fusionRDDF.setExtraWidth(ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows());
				// 					//cout << "expanding to " << ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows() << endl;
				// 					fusionCorridorPainter.repaintAll();
				// 					fusionSampledCorridorPainter.repaintAll();
				// 					if(!mapperOpts.optJeremy) {
				// 						//mapperOpts.optRDDFScaling,mapperOpts.optMaxSpeed);
				// 						fusionCostPainter.repaintAll(1);
				// 					} else {
				// 						fusionCostPainter.uberRepaintAll(1);
				// 					}
				// 				} else if(fusionRDDF.getExtraWidth()!=0.0) {
				// 					//We're inside the RDDF, but it may be too big - see if we should shrink it
				// 					int largestWayptNum = fusionRDDF.findWaypointWithLargestWidth(NEcoord(m_state.Northing, m_state.Easting));
				// 					double currentOffset = fusionRDDF.getOffset(largestWayptNum) - fusionRDDF.getExtraWidth();
				// 					double distanceToTrackline = fmax(fmax(fusionRDDF.getDistToTrackline(frontLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(frontRightAliceCorner)),
				// 																						fmax(fusionRDDF.getDistToTrackline(rearLeftAliceCorner),
				// 																								 fusionRDDF.getDistToTrackline(rearRightAliceCorner)));
				// 					/*
				// 					cout << "Dist is " << distanceToTrackline 
				// 							 << ", fl: " << fusionRDDF.getDistToTrackline(frontLeftAliceCorner)
				// 							 << ", fr: " << fusionRDDF.getDistToTrackline(frontRightAliceCorner)
				// 							 << ", bl: " << fusionRDDF.getDistToTrackline(rearLeftAliceCorner)
				// 							 << ", br: " << fusionRDDF.getDistToTrackline(rearRightAliceCorner)
				// 							 << endl;
				// 					*/
				// 					double widthToAdd = distanceToTrackline - currentOffset;
				// 					if(fusionRDDF.getExtraWidth() > ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows()) {
				// 						fusionRDDF.setExtraWidth(fmax(ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows(), 0.0));
				// 						//cout << "Shrinking to " << fmax(ceil(widthToAdd/fusionMap.getResRows())*fusionMap.getResRows(), 0.0) << endl;
				// 					} 
				// 					fusionCorridorPainter.paintChanges(exposedRow, exposedCol);
				// 					fusionCorridorReference.paintChanges(exposedRow, exposedCol);
				// 				} else {
				// 					fusionCorridorPainter.paintChanges(exposedRow, exposedCol);
				// 					fusionCorridorReference.paintChanges(exposedRow, exposedCol);
				// 				}
				// 			}      
				// 			/*
				// 				if(speedMap.getExposedRowBoxUTM(exposedRow) ||
				// 				speedMap.getExposedColBoxUTM(exposedCol)) {
	  
				// 				//Just in case we evaluated the first condition of the if statement to true:
				// 				speedMap.getExposedColBoxUTM(exposedCol);
	  
				// 				fusionSampledCorridorPainter.paintChanges(exposedRow, exposedCol);
				// 				fusionSampledCorridorReference.paintChanges(exposedRow, exposedCol);
				// 				}      	
				// 			*/
				fusionCorridorPainter.paintChanges(exposedRow, exposedCol);
				fusionCorridorReference.paintChanges(exposedRow, exposedCol);
			}

			unsigned long long timestamp;
			DGCgettime(timestamp);
	
		
			speedMap.interpolateFromOtherMap<double>(layerID_sampledCost, &fusionMap, layerID_cost);

			CDeltaList* deltaList = NULL;
	

			deltaList = speedMap.serializeDelta<double>(layerID_sampledCost, timestamp);
			if(!deltaList->isShiftOnly()) {
				SendMapdelta(lowResSpeedDeltaSendingSocket, deltaList);
				deltaSize = 0;
				for(int i=0; i<deltaList->numDeltas; i++) {
					deltaSize += deltaList->deltaSizes[i];
				}
				_infoFusionMapper.endProcessTimer(deltaSize);	
				speedMap.resetDelta<double>(layerID_sampledCost);
			}
		     
			deltaList = fusionMap.serializeDelta<double>(layerID_cost, timestamp);	
			if(!deltaList->isShiftOnly()) {
				SendMapdelta(socket_num, deltaList);
				deltaSize = 0;
				for(int i=0; i<deltaList->numDeltas; i++) {
					deltaSize += deltaList->deltaSizes[i];
				}
				_infoFusionMapper.endProcessTimer(deltaSize);	
				fusionMap.resetDelta<double>(layerID_cost);
			}

			deltaList = fusionMap.serializeDelta<double>(layerID_ladarElev, timestamp);
			if(!deltaList->isShiftOnly()) {
				SendMapdelta(elevDeltaSendingSocket, deltaList);
				fusionMap.resetDelta<double>(layerID_ladarElev);
			}
	
			deltaList = fusionMap.serializeDelta<double>(layerID_fusedElevStdDev, timestamp);
			if(!deltaList->isShiftOnly()) {
				SendMapdelta(stddevDeltaSendingSocket, deltaList);
				fusionMap.resetDelta<double>(layerID_fusedElevStdDev);
			}

			DGCunlockMutex(&m_mapMutex);    
		} else {
			DGCusleep(500000);
		}
	}

	printf("%s [%d]: Active loop quitting\n", __FILE__, __LINE__);
}


void FusionMapper::ReceiveDataThread_Ladar() {
  int mapDeltaSocket = m_skynet.listen(SNladarDeltaMapFused, MODladarFeeder);
  double UTMNorthing;
  double UTMEasting;

  //int elevDeltaSendingSocket = m_skynet.get_send_sock(SNfusionElevDeltaMap);

  m_pLadarMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pLadarMapDelta, MAX_DELTA_SIZE, 0);
      _infoLadarFeeder.startProcessTimer();
      if(mapperOpts.optLadar) {
				if(numreceived>0) {
					DGClockMutex(&m_mapMutex);
					int cellsize = fusionMap.getDeltaSize(m_pLadarMapDelta);
					for(int i=0; i<cellsize; i++) {
						CElevationFuser receivedData = fusionMap.getDeltaVal<CElevationFuser>(i, layerID_fusedElev, m_pLadarMapDelta, &UTMNorthing, &UTMEasting);				
						if(mapperOpts.optAverage) {
							CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMNorthing, UTMEasting);
							mapData.fuse_MeanElevation(receivedData);
						} else if(mapperOpts.optMax) {
							CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMNorthing, UTMEasting);
							mapData.fuse_MaxElevation(receivedData);
						} else {
							fusionMap.setDataUTM<CElevationFuser>(layerID_fusedElev, UTMNorthing, UTMEasting, receivedData);
						}
						CElevationFuser currentData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMNorthing, UTMEasting);
						fusionMap.setDataUTM_Delta<double>(layerID_ladarElev, UTMNorthing, UTMEasting, currentData.getMeanElevation());
						fusionMap.setDataUTM_Delta<double>(layerID_fusedElevStdDev, UTMNorthing, UTMEasting,
																							 currentData.getStdDev());

						fusionCostPainter.paintChangesNotify(UTMNorthing, UTMEasting);

					}
					fusionCostPainter.paintNotifications();
					DGCunlockMutex(&m_mapMutex);
					_infoLadarFeeder.endProcessTimer(numreceived);
				}     
      }
    }
  }
  delete m_pLadarMapDelta; 
}


void FusionMapper::ReceiveDataThread_Static() {
  int mapDeltaSocket = m_skynet.listen(SNstaticdeltamap, MODstaticpainter);
  double UTMNorthing;
  double UTMEasting;

  double cellval;

  m_pStaticMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      _infoStaticPainter.startProcessTimer();
      int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pStaticMapDelta, MAX_DELTA_SIZE, 0);
      if(mapperOpts.optStatic) {
				if(numreceived>0) {
					DGClockMutex(&m_mapMutex);	  
					// Begin cutting here?
					int cellsize = fusionMap.getDeltaSize(m_pStaticMapDelta);
					for(int i=0; i<cellsize; i++) {
						cellval = fusionMap.getDeltaVal<double>(i, layerID_static, m_pStaticMapDelta, &UTMNorthing, &UTMEasting);
						fusionMap.setDataUTM<double>(layerID_static, UTMNorthing, UTMEasting, cellval);
	    


						fusionCostPainter.paintChangesNotify(UTMNorthing, UTMEasting);

					}

					fusionCostPainter.paintNotifications();
					DGCunlockMutex(&m_mapMutex);
					_infoStaticPainter.endProcessTimer(numreceived);
				}     
      }
    }
    usleep(100);
  }
  delete m_pStaticMapDelta; 
}


void FusionMapper::ReceiveDataThread_Stereo() {
  int mapDeltaSocket = m_skynet.listen(SNstereodeltamap, MODstereofeeder);
  double UTMNorthing;
  double UTMEasting;
  int cellsize;

  m_pStereoMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      _infoStereoFeeder.startProcessTimer();

      RecvMapdelta(mapDeltaSocket, m_pStereoMapDelta, &cellsize);
      if(mapperOpts.optStereo) {
				int numreceived = cellsize;
				cellsize = fusionMap.getDeltaSize(m_pStereoMapDelta); 
				DGClockMutex(&m_mapMutex);
				for(int i=0; i<cellsize; i++) {
					CElevationFuser receivedData = fusionMap.getDeltaVal<CElevationFuser>(i, layerID_fusedElev, m_pLadarMapDelta, &UTMNorthing, &UTMEasting);
					if(mapperOpts.optAverage) {
						CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMNorthing, UTMEasting);
						mapData.fuse_MeanElevation(receivedData);
					} else if(mapperOpts.optMax) {
						CElevationFuser& mapData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMNorthing, UTMEasting);
						mapData.fuse_MaxElevation(receivedData);
					} else {
						fusionMap.setDataUTM<CElevationFuser>(layerID_ladarElev, UTMNorthing, UTMEasting, receivedData);
					}
					CElevationFuser currentData = fusionMap.getDataUTM<CElevationFuser>(layerID_fusedElev, UTMNorthing, UTMEasting);
					fusionMap.setDataUTM<double>(layerID_elev, UTMNorthing, UTMEasting, currentData.getMeanElevation());
					fusionCostPainter.paintChanges(UTMNorthing, UTMEasting, 1);
				}
				DGCunlockMutex(&m_mapMutex);
				_infoStereoFeeder.endProcessTimer(numreceived);
      }     
    }
    
  }
  delete m_pStereoMapDelta; 
}


void FusionMapper::ReceiveDataThread_Road()
{
  int road_socket = m_skynet.listen(SNroad2map, SNroadfinding);

  bool first_road=true;
  int cur_road=0;
  Road road[2];
  CRoadPainter painter;

  printf("RoadThread started\n");

  int bytes=0;
  int cnt=0;

  while(!_QUIT) {
    //Read road data over skynet, make sure we always get the last transfer
    //so that we don't build up the buffert
    int received;
    do {
      received = m_skynet.get_msg(road_socket, &road[cur_road], sizeof(Road), 0);
      if(received < road[cur_road].data_size()) {
				printf("Error receiving road data\n");
				continue;
      }
      
      bytes +=received;
      ++cnt;
      //printf("Received %i messages, %i bytes, %i road points\n", cnt, bytes, road[cur_road].length);
    } while(m_skynet.is_msg(road_socket));
    
    if(received < road[cur_road].data_size())   //Invalid road data, get next package
      continue;
    
    _infoRoad.startProcessTimer();
    if(mapperOpts.optRoad) {      
      //Paint the trajectory into the map
      DGClockMutex(&m_mapMutex);
      //myCMap.clearLayer(layerNum[SNGUI_LAYER_ROAD]);

      if(!first_road) {
				painter.paint(road[cur_road], road[1-cur_road], &fusionMap, layerID_road, &fusionCostPainter);
      } else {
				painter.paint(road[cur_road], &fusionMap, layerID_road, &fusionCostPainter);
      }

      first_road=false;
      cur_road = 1-cur_road;
      
      DGCunlockMutex(&m_mapMutex);
      _infoRoad.endProcessTimer(received);
    }
  }
}

void FusionMapper::ReceiveDataThread_Supercon() {
  int mapDeltaSocket;
	//mapDeltaSocket = m_skynet.listen(SN, MODsupercon);
  double UTMNorthing;
  double UTMEasting;

  double cellval;

  m_pSuperconMapDelta = new char[MAX_DELTA_SIZE];
  while(!_QUIT) { 
    if(!_PAUSE) {
      _infoSupercon.startProcessTimer();
      int numreceived = m_skynet.get_msg(mapDeltaSocket, m_pSuperconMapDelta, MAX_DELTA_SIZE, 0);
      if(mapperOpts.optSupercon) {
				if(numreceived>0) {
					DGClockMutex(&m_mapMutex);	  
					// Begin cutting here?
					int cellsize = fusionMap.getDeltaSize(m_pSuperconMapDelta);
					for(int i=0; i<cellsize; i++) {
						cellval = fusionMap.getDeltaVal<int>(i, layerID_supercon, m_pSuperconMapDelta, &UTMNorthing, &UTMEasting);
						fusionMap.setDataUTM<int>(layerID_supercon, UTMNorthing, UTMEasting, cellval);
						fusionCostPainter.paintChangesNotify(UTMNorthing, UTMEasting);
					}

					fusionCostPainter.paintNotifications();
					DGCunlockMutex(&m_mapMutex);
					_infoSupercon.endProcessTimer(numreceived);
				}     
      }
    }
    usleep(100);
  }
  delete m_pSuperconMapDelta; 
}


void FusionMapper::PlannerListenerThread() { 
	int mapDeltaSocket = m_skynet.listen(SNfullmaprequest, SNplanner);
	int socket_num = m_skynet.get_send_sock(SNfusiondeltamap);
	bool bRequestMap;
	while(!_QUIT)
		{
			int numreceived =  m_skynet.get_msg(mapDeltaSocket, &bRequestMap, MAX_DELTA_SIZE, 0);
			//cout << "Received "  << numreceived << " Messages"  << endl; 
			if(numreceived>0) {
				DGClockMutex(&m_mapMutex);
				if(bRequestMap == true) {
					unsigned long long timestamp;
					DGCgettime(timestamp);
					CDeltaList* deltaList = fusionMap.serializeFullMapDelta<double>(layerID_cost, timestamp);

					if(!SendMapdelta(socket_num, deltaList))
						cerr << "FusionMapper::PlannerListenerThread(): couldn't send delta" << endl;
					else
						fusionMap.resetDelta<double>(layerID_cost);
				} 
				DGCunlockMutex(&m_mapMutex);
			}
		}
}


void FusionMapper::ReceiveDataThread_EStop() {
	int oldStatus = -1;

	while(!_QUIT) {
		WaitForNewActuatorState();
		if(m_actuatorState.m_estoppos == 2 && oldStatus != 2) {
			DGClockMutex(&m_mapMutex);
			fusionMap.clearMap();
			fusionCorridorPainter.repaintAll();
			fusionCorridorReference.repaintAll();
			//fusionSampledCorridorPainter.repaintAll();
			fusionCostPainter.repaintAll(1);
			DGCunlockMutex(&m_mapMutex);
		}
		oldStatus = m_actuatorState.m_estoppos;
	}
}


void FusionMapper::ReadConfigFile() {
  FILE* configFile = NULL;
  char fieldBuffer[512];
  char fieldName[256];
  char fieldValue[256];

  configFile = fopen(mapperOpts.configFilename, "r");
  if(configFile != NULL) {
    while(!feof(configFile)) {
      fgets(fieldBuffer, 512, configFile);
      if(strncmp(fieldBuffer, "%", 1)!=0) {
				sscanf(fieldBuffer, "%s %s\n", fieldName, fieldValue);
				if(strcmp(fieldName, "Unpassable_Obstacle_Height:")==0) {
					mapperOpts.optUnpassableObsHeight = atof(fieldValue);
				} else if(strcmp(fieldName, "Zero_Gradient_Speed:")==0) {
					mapperOpts.optZeroGradSpeed = atof(fieldValue);
				} else if(strcmp(fieldName, "Max_Speed:")==0) {
					mapperOpts.optMaxSpeed = atof(fieldValue);
				} else if(strcmp(fieldName, "RDDF_Speed_Scaling:")==0) {
					mapperOpts.corridorOpts.optRDDFscaling = atof(fieldValue);
				} else if(strcmp(fieldName, "Use_Area_Gradient:")==0) {
					mapperOpts.optUseAreaGradient = atoi(fieldValue);
				} else if(strcmp(fieldName, "Area_Gradient_Scaling:")==0) {
					mapperOpts.optAreaGradScaling = atof(fieldValue);
				} else if(strcmp(fieldName, "Static_Scaling:")==0) {
					mapperOpts.optStaticScaling = atof(fieldValue);
				} else if(strcmp(fieldName, "Road_Scaling:")==0) {
					mapperOpts.optRoadScaling = atof(fieldValue);
				} else if(strcmp(fieldName, "Overall_Aggressiveness:")==0) {
					//Not yet implemented
				} else if(strcmp(fieldName, "Velo_Alg:")==0) {
					mapperOpts.optVeloGenerationAlg = atoi(fieldValue);
				} else if(strcmp(fieldName, "Velo_Tweak1:")==0) {
					mapperOpts.optVeloTweak1 = atof(fieldValue);
				} else if(strcmp(fieldName, "Velo_Tweak2:")==0) {
					mapperOpts.optVeloTweak2 = atof(fieldValue);
				} else if(strcmp(fieldName, "Area_Grad_Scaling:")==0) {
					mapperOpts.optAreaGradScaling = atof(fieldValue);
				} else if(strcmp(fieldName, "Area_Grad:")==0) {
					mapperOpts.optUseAreaGradient = atof(fieldValue);
				} else if(strcmp(fieldName, "Std_Dev_Scaling:")==0) {
					mapperOpts.optStdDevScaling = atof(fieldValue);
				} else if(strcmp(fieldName, "Std_Dev_Weight:")==0) {
					mapperOpts.optStdDevWeight = atof(fieldValue);
				} else if(strcmp(fieldName, "Link:")==0) {
					//Not yet implemented
				} else if(strcmp(fieldName, "Paint_Center_Trackline:")==0) {
					mapperOpts.corridorOpts.paintCenterTrackline  = 
						(CCorridorPainterOptions::PAINT_CENTER_TRACKLINE_TYPE)atoi(fieldValue);
				} else if(strcmp(fieldName, "What_To_Adjust:")==0) {
					mapperOpts.corridorOpts.whatToAdjust = 
						(CCorridorPainterOptions::WHAT_TO_ADJUST_TYPE)atoi(fieldValue);
				} else if(strcmp(fieldName, "Adjustment_Val:")==0) {
					mapperOpts.corridorOpts.adjustmentVal = atof(fieldValue);
				} else if(strcmp(fieldName, "Inside_RDDF_No_Data_Speed:")==0) {
					mapperOpts.corridorOpts.optMaxSpeed = atof(fieldValue);
				} else {
					printf("%s [%d]: Unknown field '%s' with value '%s' - pausing\n", __FILE__, __LINE__, fieldName, fieldValue);
				}
      }
    }
    fclose(configFile);
  }

}


void FusionMapper::WriteConfigFile() {
  FILE* configFile = NULL;

  configFile = fopen(mapperOpts.configFilename, "w");
  if(configFile != NULL) {
    fprintf(configFile, "%% FusionMapper Aggressiveness Tuning Configuration File\n");
    fprintf(configFile, "Link: %d\n", 0);
    fprintf(configFile, "Overall_Aggressiveness: %lf\n", 1.0);
    fprintf(configFile, "RDDF_Speed_Scaling: %lf\n", mapperOpts.corridorOpts.optRDDFscaling);
    fprintf(configFile, "Inside_RDDF_No_Data_Speed: %lf\n", mapperOpts.corridorOpts.optMaxSpeed);
    fprintf(configFile, "Max_Speed: %lf\n", mapperOpts.optMaxSpeed);
    fprintf(configFile, "Zero_Gradient_Speed: %lf\n", mapperOpts.optZeroGradSpeed);
    fprintf(configFile, "Velo_Alg: %d\n", mapperOpts.optVeloGenerationAlg);
    fprintf(configFile, "Velo_Tweak1: %lf\n", mapperOpts.optVeloTweak1);
    fprintf(configFile, "Velo_Tweak2: %lf\n", mapperOpts.optVeloTweak2);
    fprintf(configFile, "Area_Grad_Scaling: %lf\n", mapperOpts.optAreaGradScaling);
    fprintf(configFile, "Area_Grad: %lf\n", mapperOpts.optUseAreaGradient);
    fprintf(configFile, "Std_Dev_Scaling: %lf\n", mapperOpts.optStdDevScaling);
    fprintf(configFile, "Std_Dev_Weight: %lf\n", mapperOpts.optStdDevWeight);
    fprintf(configFile, "Unpassable_Obstacle_Height: %lf\n", mapperOpts.optUnpassableObsHeight);
    fprintf(configFile, "Use_Area_Gradient: %d\n", mapperOpts.optUseAreaGradient);
    fprintf(configFile, "Area_Gradient_Scaling: %lf\n", mapperOpts.optAreaGradScaling);
    fprintf(configFile, "Static_Scaling: %lf\n", mapperOpts.optStaticScaling);
    fprintf(configFile, "Road_Scaling: %lf\n", mapperOpts.optRoadScaling);
    fprintf(configFile, "Paint_Center_Trackline: %d\n", mapperOpts.corridorOpts.paintCenterTrackline);
    fprintf(configFile, "What_To_Adjust: %d\n", mapperOpts.corridorOpts.whatToAdjust);
    fprintf(configFile, "Adjustment_Val: %lf\n", mapperOpts.corridorOpts.adjustmentVal);
    fclose(configFile);
  }
}
