//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_ImageSource.hh"

#define PI 3.1415926535

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for camera calibration class.  contains basic intrinsic variables
/// and extrinsic variables describing camera pose in vehicle coordinate system

UD_CameraCalibration::UD_CameraCalibration(int image_width, int image_height, 
					   float horiz_fov,
					   float focal_x, float focal_y, float principal_x, float principal_y, 
					   float x, float y, float z, float pitch, float yaw)
{
  scale_factor = 1;

  w = image_width;
  h = image_height;

  fx = focal_x;
  fy = focal_y;

  px = principal_x;
  py = principal_y;

  hfov = horiz_fov;
  vfov = hfov * (float) h / (float) w;

  dx = x;
  dy = y;
  dz = z;
  
  pitch_angle = pitch;

  sin_pitch_angle = sin(pitch_angle);
  cos_pitch_angle = cos(pitch_angle);

  yaw_angle = yaw;
}

//----------------------------------------------------------------------------

/// apply transformation taking 3-D point in vehicle coordinates to camera coordinates

void UD_CameraCalibration::vehicle2camera(float vx, float vy, float vz, float *cx, float *cy, float *cz)
{
  float new_cy, new_cz;

  // translation from vehicle to camera coords

  *cx = vx + dx;
  *cy = vy + dy;
  *cz = vz + dz;

  //  printf("%.2f %.2f %.2f %.2f %.2f %.2f", vx, vy, vz, *cx, *cy, *cz);

  // rotate by pitch

  // cx unchanged
  new_cz = *cz * cos_pitch_angle + *cy * sin_pitch_angle;
  new_cy = *cz * -sin_pitch_angle + *cy * cos_pitch_angle;

  //  printf(" %.2f %.2f\n", new_cy, new_cz);

  *cy = new_cy;
  *cz = new_cz;
}

//----------------------------------------------------------------------------

/// transformation projecting 3-D point in camera coordinates to 3-D image coordinates
/// (neglects radial distortion)

void UD_CameraCalibration::camera2image(float cx, float cy, float cz, float *x, float *y)
{
  // pinhole projection
  
  *x = px + fx * cx / cz;
  *y = py + fy * cy / cz;
  
  //  printf("%.2f %.2f\n", *x, *y);
  
  // radial distortion
  
  // ???

  // scaling for level of image pyramid we're working at

  *x *= scale_factor;
  *y *= scale_factor;
}

//Sparrow display getter/setter functions
double UD_CameraCalibration::get_yaw_deg()
{
  return yaw_angle * 180 / PI;
}

void UD_CameraCalibration::set_yaw_deg(double *pData, double val)
{
  yaw_angle = val / 180 *  PI;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for UD_ImageSource base class

UD_ImageSource::UD_ImageSource()
{
  frame_filename = (char *) calloc(MAXSTRLEN, sizeof(char));
  timestamp_filename = NULL;
  timestamp = 0;
}

//----------------------------------------------------------------------------

/// how many commas are in this string?  used to differentiate between log 
/// files with just timestamps and ones that also have state

int UD_ImageSource::count_commas(char *logstring)
{
  int i, num_commas;

  for (i = 0, num_commas = 0; i < strlen(logstring); i++)
    if (logstring[i] == ',')
      num_commas++;

  return num_commas;
}

//----------------------------------------------------------------------------

/// utility rounding function

double UD_ImageSource::myround(double x)
{
  if (x - floor(x) >= 0.5)
    return ceil(x);
  else
    return floor(x);
}

//----------------------------------------------------------------------------

/// given image source width and desired width at which to do image processing, 
/// determines how many pyramid levels (each one is half of previous one's
/// dimensions) are necessary

void UD_ImageSource::calculate_num_pyramid_levels(int width, int target_width)
{
  double val;

  if (target_width < 40) {
    printf("requested image width too small\n");
    exit(1);
  }

  //  val = floor(log((double) width / (double) target_width) / log(2.0));
  val = myround(log((double) width / (double) target_width) / log(2.0));
  num_pyramid_levels = 1 + MAX2(0, val);

  source_im = (IplImage **) calloc(num_pyramid_levels, sizeof(IplImage *));
  gray_source_im = (IplImage **) calloc(num_pyramid_levels, sizeof(IplImage *));
}

//----------------------------------------------------------------------------

/// allocate image buffer for each level of pyramid

void UD_ImageSource::initialize_pyramid()
{
  int i;

  for (i = 1; i < num_pyramid_levels; i++) {
    source_im[i] = cvCreateImage(cvSize(source_im[i - 1]->width / 2, source_im[i - 1]->height / 2), IPL_DEPTH_8U, 3);
    gray_source_im[i] = cvCreateImage(cvSize(source_im[i - 1]->width / 2, source_im[i - 1]->height / 2), IPL_DEPTH_8U, 1);
  }	

  im = source_im[num_pyramid_levels - 1];
  gray_im = gray_source_im[num_pyramid_levels - 1];
}

//----------------------------------------------------------------------------

/// reduce first level of pyramid (the raw captured image) to second level, 
/// second to third, and so on until final image winds up in im

void UD_ImageSource::pyramidize()
{
  int i;

  for (i = 0; i < num_pyramid_levels - 1; i++) 
    cvPyrDown(source_im[i], source_im[i + 1]);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for derived Live UD_ImageSource.  initializes connections with
/// all attached firewire cameras supporting the indicated color/size mode; 
/// camera_num specifies which camera subsequently captured images will come from
/// automatically captures first image and pyramidizes it

UD_ImageSource_Live::UD_ImageSource_Live(UD_CameraCalibration *camcal, int camera_num, int camera_mode, int end, int target_width) : UD_ImageSource()
{
  int w, h;

  absolute_frame = start_frame = 0;
  end_frame = end;
  relative_frame = 0;

  cam_num = camera_num;
  cam = new UD_FirewireCamera(camera_mode);
  is_grayscale = camera_mode == MODE_640x480_MONO;

  calculate_num_pyramid_levels(cam->width, target_width);

  source_im[0] = cvCreateImage(cvSize(cam->width, cam->height), IPL_DEPTH_8U, 3);
  gray_source_im[0] = cvCreateImage(cvSize(cam->width, cam->height), IPL_DEPTH_8U, 1);

  initialize_pyramid();
  capture();   

  cal = camcal;
  if (cal)
    cal->scale_factor = (float) im->width / (float) cal->w;
}

//----------------------------------------------------------------------------

void UD_ImageSource_Live::setup_timestamp(char *filename)
{
  // nothing to do since we're getting timestamps from system time
}

//----------------------------------------------------------------------------

/// get timestamp for this captured image from system's gettimeofday() function

void UD_ImageSource_Live::get_timestamp()
{
  UD_timestamp(&timestamp_secs, &timestamp_usecs);
  timestamp = (double) timestamp_secs + 0.000001 * (double) timestamp_usecs;
}

//----------------------------------------------------------------------------

/// get new image from firewire camera 

int UD_ImageSource_Live::capture()
{
  if (end_frame != NONE && absolute_frame > end_frame) {
    printf("done\n");
    exit(1);
  }

  cam->capture(source_im[0], cam_num);
  pyramidize();
  absolute_frame++;
  relative_frame++;

  get_timestamp();

  return TRUE;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for derived AVI ImageSource class

UD_ImageSource_AVI::UD_ImageSource_AVI(UD_CameraCalibration *camcal, char *source_path, int start, int end, int target_width)
{
  int i;

  absolute_frame = start_frame = start;
  end_frame = end;
  relative_frame = 0;

  movie_source = open_UD_Movie(source_path);
  is_grayscale = FALSE;

  // frame 0 is "gotten" automatically in open_UD_Movie()

  if (start_frame > 0) {
    for (i = 0; i < start_frame; i++)
      get_frame_UD_Movie(movie_source, 0);
  }
  else if (start_frame < 0) {
    printf("negative AVI start frame number not supported\n");
    exit(1);
  }

  calculate_num_pyramid_levels(movie_source->im->width, target_width);

  source_im[0] = movie_source->im;
  gray_source_im[0] = cvCreateImage(cvSize(source_im[0]->width, source_im[0]->height), IPL_DEPTH_8U, 1);

  initialize_pyramid();
  pyramidize();

  cal = camcal;
  if (cal)
    cal->scale_factor = (float) im->width / (float) cal->w;
}

//----------------------------------------------------------------------------

/// initialize timestamp log file reading

void UD_ImageSource_AVI::setup_timestamp(char *filename)
{
  timestamp_filename = (char *) calloc(MAXSTRLEN, sizeof(char));
  strcpy(timestamp_filename, filename);
  timestamp_fp = fopen(timestamp_filename, "r");
  if (!timestamp_fp) {
    printf("no such timestamp logfile\n");
    exit(1);
  }

  // not sure what needs to be done for AVIs
  //  rewind(filelist_fp);    
  //  goto_image(start_frame);
}

//----------------------------------------------------------------------------

/// get AVI timestamp from current system time

void UD_ImageSource_AVI::get_timestamp()
{
  UD_timestamp(&timestamp_secs, &timestamp_usecs);
  timestamp = (double) timestamp_secs + 0.000001 * (double) timestamp_usecs;
}

//----------------------------------------------------------------------------

/// capture next AVI frame

int UD_ImageSource_AVI::capture()
{
  int i;

  if (absolute_frame > movie_source->lastframe || (end_frame != NONE && absolute_frame > end_frame)) {
    printf("done\n");
    exit(1);
  }

  // get next frame

  get_frame_UD_Movie(movie_source, absolute_frame);   // source_im[0] points to movie_source->im
  pyramidize();
  absolute_frame++;
  relative_frame++;

  get_timestamp();

  return TRUE;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

int UD_ImageSource_Image::count_lines(FILE *fp)
{
  int count;
  char dummystr[MAXSTRLEN];
  char *result;

  count = 0;
  do {
    result = fgets(dummystr, MAXSTRLEN, fp);
    if (result)
      count++;
  } while (result);

  rewind(fp);

  return count;
}

//----------------------------------------------------------------------------

/// constructor for derived Image ImageSource class.  opens file listing images
/// to use (can be a single image, or permute order that directory naming would
/// give)

UD_ImageSource_Image::UD_ImageSource_Image(UD_CameraCalibration *camcal, char *source_path, int start, int end, int target_width)
{
  IplImage *temp_im;

  start_frame = start;
  end_frame = end;
  absolute_frame = start_frame;

  filename = (char *) calloc(MAXSTRLEN, sizeof(char));

  // generate this file with "find <path> | sort > filename.txt" (and remove first line of file)

  filelist_fp = fopen(source_path, "r");
  if (!filelist_fp) {
    printf("no such file list\n");
    exit(1);
  }

  if (end == NONE)
    end_frame = count_lines(filelist_fp) - 1;

  goto_image(start_frame);   // need this here for advanced frame starts with no ladar
  if (!read_next_image(&temp_im)) {
    printf("no images in file list\n");
    exit(1);
  }

  is_grayscale = FALSE;

  calculate_num_pyramid_levels(temp_im->width, target_width);

  source_im[0] = cvCloneImage(temp_im);
  cvReleaseImage(&temp_im);
  gray_source_im[0] = cvCreateImage(cvSize(source_im[0]->width, source_im[0]->height), IPL_DEPTH_8U, 1);

  initialize_pyramid();
  pyramidize();

  cal = camcal;
  if (cal) 
    cal->scale_factor = (float) im->width / (float) cal->w;
}

//----------------------------------------------------------------------------

/// initialize timestamp log file reading

void UD_ImageSource_Image::setup_timestamp(char *filename)
{
  printf("setting up %s\n", filename);

  timestamp_filename = (char *) calloc(MAXSTRLEN, sizeof(char));
  strcpy(timestamp_filename, filename);
  timestamp_fp = fopen(timestamp_filename, "r");
  if (!timestamp_fp) {
    printf("no such timestamp logfile\n");
    exit(1);
  }

  rewind(filelist_fp);    
  goto_image(start_frame);
  printf("timestamp = %lf\n", timestamp);
  //  exit(1);
}

//----------------------------------------------------------------------------

/// read next image sequence timestamp from log file

void UD_ImageSource_Image::get_timestamp()
{
  int framenum, num_log_params, result;
  long int secs, usecs, long_framenum;
  float vx, vy, lateral_x;
  int onroad_state;

  // reading timestamp from log file

  if (timestamp_fp) {
    fgets(timestamp_filename, MAXSTRLEN, timestamp_fp);   // using filename string to hold line from log file
     num_log_params = 1 + count_commas(timestamp_filename);
     if (num_log_params == 3)                       // old format: just image number and timestamp
      result = sscanf(timestamp_filename, "%i, %li, %li\n", &framenum, &secs, &usecs);
    else if (num_log_params == 4) {                      // old format with trailing comma tacked on
      result = sscanf(timestamp_filename, "%d, %ld, %ld,\n", &framenum, &secs, &usecs);
    }
    else if (num_log_params == 7)                  // assume new format: image number, timestamp, and state 
      result = sscanf(timestamp_filename, "%i, %li, %li, %f, %f, %f, %i\n", &framenum, &secs, &usecs, &vx, &vy, &lateral_x, &onroad_state);
    else {
      printf("unknown log file format with apparently %i logged parameters\n", num_log_params);
      exit(1);
    }

    timestamp = (double) secs + 0.000001 * (double) usecs;
  }

  // taking timestamp from system time

  else {
    UD_timestamp(&timestamp_secs, &timestamp_usecs);
    timestamp = (double) timestamp_secs + 0.000001 * (double) timestamp_usecs;
  }
}

//----------------------------------------------------------------------------

/// capture next image specified in sequence file.  closes last image and opens
/// new one

int UD_ImageSource_Image::capture()
{
  cvReleaseImage(&source_im[0]);

  if ((end_frame != NONE && absolute_frame > end_frame) || !read_next_image(&source_im[0])) {
    printf("done\n");
    exit(1);
  }
   
  if (num_pyramid_levels == 1)
    im = source_im[0];    // have to keep resetting because read_next_image() makes fresh pointer every time  
  else
    pyramidize();
  absolute_frame++;
  relative_frame++;

  get_timestamp();
    
  return TRUE;
}

//----------------------------------------------------------------------------

/// position file pointer at beginning of line n of image sequence listing

int UD_ImageSource_Image::goto_image(int n)
{
  int i;

  for (i = 0; i < n; i++) {
    if (!fgets(filename, MAXSTRLEN, filelist_fp)) {
      printf("no such line in image list file\n");
      exit(1);
    }
    get_timestamp();
  }
}

//----------------------------------------------------------------------------

/// return pointer to next image specified in sequence file.  
/// sequence file contains a full-path filename of an input image on each line.
/// returns TRUE if filename gotten; FALSE if EOF encountered
 
int UD_ImageSource_Image::read_next_image(IplImage **im)
{
  IplImage *tempim;

  if (!fgets(filename, MAXSTRLEN, filelist_fp))
    return FALSE;
  filename[strlen(filename)-1] = '\0';

  /*
  *im = cvLoadImage(filename);
  //  printf("w = %i, h = %i\n", (*im)->width, (*im)->height);
  cvConvertImage(*im, *im, CV_CVTIMG_SWAP_RB);  // RGB->BGR
  */

  tempim = cvLoadImage(filename);

  if (!tempim)
    return FALSE;

  // it's a color 3-channel image

  if (tempim->nChannels == 3) {
    *im = tempim;
    cvConvertImage(*im, *im, CV_CVTIMG_SWAP_RB);  // RGB->BGR
  }

  // it's a grayscale 1-channel image

  else {
    *im = cvCreateImage(cvSize(tempim->width, tempim->height), IPL_DEPTH_8U, 3);
    cvConvertImage(tempim, *im);    // color to gray
    cvReleaseImage(&tempim);
  }

  return TRUE;
}


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for derived Live UD_ImageSource_VideoRecorder.
/// Initializes a connection to a camera through the videorecorder
/// interface which simultaniously records a video of the camera input

#ifdef SKYNET

#include"sparrowhawk.hh"

UD_ImageSource_VideoRecorder::UD_ImageSource_VideoRecorder(UD_CameraCalibration *camcal, CVideoRecorder &vr, int end, int target_width) : UD_ImageSource(), m_vr(vr)
{
  absolute_frame = start_frame = 0;
  end_frame = end;
  relative_frame = 0;

  is_grayscale = true;

  calculate_num_pyramid_levels(m_vr.width(), target_width);

  source_im[0] = cvCreateImage(cvSize(m_vr.width(), m_vr.height()), IPL_DEPTH_8U, 3);
  gray_source_im[0] = cvCreateImage(cvSize(m_vr.width(), m_vr.height()), IPL_DEPTH_8U, 1);

  initialize_pyramid();
  capture();   

  cal = camcal;
  if (cal)
    cal->scale_factor = (float) im->width / (float) cal->w;
}

//----------------------------------------------------------------------------

void UD_ImageSource_VideoRecorder::setup_timestamp(char *filename)
{
  // nothing to do since we're getting timestamps from system time
}

//----------------------------------------------------------------------------

/// get timestamp for this captured image from system's gettimeofday() function

void UD_ImageSource_VideoRecorder::get_timestamp()
{
  UD_timestamp(&timestamp_secs, &timestamp_usecs);
  timestamp = (double) timestamp_secs + 0.000001 * (double) timestamp_usecs;
}

//----------------------------------------------------------------------------

/// get new image from firewire camera 

int UD_ImageSource_VideoRecorder::capture()
{
  if (end_frame != NONE && absolute_frame > end_frame) {
    printf("done\n");
    exit(1);
  }

  m_vr.imageCopy(source_im[0]);

  pyramidize();
  absolute_frame++;
  relative_frame++;

  get_timestamp();

  return TRUE;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

UD_ImageSource_Timber::UD_ImageSource_Timber(UD_CameraCalibration *camcal, int skynet_key)
  : CSkynetContainer(SNroadfinding, skynet_key), CTimberClient(timber_types::roadFinding)
{
  absolute_frame = 0;
  end_frame = -1;
  relative_frame = 0;

  is_grayscale = true;

  cerr<<"Waiting for first timber frame"<<endl;
  capture();   //Get the width and height

  cal = camcal;
  if (cal)
    cal->scale_factor = (float) im->width / (float) cal->w;

  m_current_time = 0;
}


int UD_ImageSource_Timber::capture()
{
  unsigned long long image_time;

  static char buf[1024];
  static char filename[1024];

  bool jump=false;

  //Get the next image
  string dir;
  dir = getPlaybackDir();

  //If new directory or time has moved backwards
  if(checkNewPlaybackDirAndReset() || m_current_time>getPlaybackTime()) {
    if(m_index_file)
      fclose(m_index_file);
    
    string file = dir + "road.log";
    SparrowHawk().log("Open log file %s", file.c_str());
    m_index_file = fopen(file.c_str(), "r");
    if(!m_index_file) {
      cerr<<"Error opening log index file "<<file<<endl;
      exit(1);
    }

    jump=true;
    
    SparrowHawk().log("Loaded log index file %s", file.c_str());
  }
  
  //Read from index file to get next filename
  if(!m_index_file)
    return FALSE;

  unsigned long long time_diff = getPlaybackTime() - m_current_time;
  if(time_diff<0 || time_diff>10000000 || DGCtimetosec(time_diff)>2.0)   //Time skip
    jump=true;

  m_current_time = getPlaybackTime();
  image_time = 0;
  
  bool breakout=false;
  do {
    do {  //Find next - none commented - line
      if(!fgets(buf, sizeof(buf), m_index_file)) {
	if(feof(m_index_file)) {
	  //End of file
	  usleep(10000);
	  return FALSE;
	}
	cerr<<"Error reading line from index file"<<endl;
	return FALSE;
      }
    } while(buf[0]=='#');
    
    sscanf(buf, "%lli %s", &image_time, filename);
    SparrowHawk().log("Processing image %lli %s", image_time, filename);
  } while(jump && image_time < m_current_time);   //If we made a jump, wait for the right image

  string file = dir + filename;

  if(im)
    cvReleaseImage(&im);
  im = cvLoadImage(file.c_str());


  //Block until time
  blockUntilTime(image_time);

  return TRUE;
}


void UD_ImageSource_Timber::setup_timestamp(char *filename)
{
  // nothing to do since we're getting timestamps from system time
}

//----------------------------------------------------------------------------

/// get timestamp for this captured image from system's gettimeofday() function

void UD_ImageSource_Timber::get_timestamp()
{
  UD_timestamp(&timestamp_secs, &timestamp_usecs);
  timestamp = (double) timestamp_secs + 0.000001 * (double) timestamp_usecs;
}

#endif //SKYNET

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

