//----------------------------------------------------------------------------
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Logger.hh"


#ifdef SKYNET
#include"sparrowhawk.hh"
#endif

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for printing utility class

UD_Reporter::UD_Reporter()
{
  // defaults

  interval                              = 10;
  do_timing                             = FALSE;
  print_image_number                    = FALSE;
}

//----------------------------------------------------------------------------

/// start precision frame rate timer if reporting interval
/// indicates that results should be printed now

void UD_Reporter::start_timer(UD_ImageSource *imsrc)
{
  if (do_timing && !(imsrc->relative_frame % interval)) 
    start_time = (double) cvGetTickCount() / cvGetTickFrequency();
}

//----------------------------------------------------------------------------

/// stop precision frame rate timer and report results

void UD_Reporter::end_timer(UD_ImageSource *imsrc)
{
  if (do_timing && !(imsrc->relative_frame % interval)) {
    finish_time = (double) cvGetTickCount() / cvGetTickFrequency();
    printf("%i: %.2lf iterations per second (%lf)\n", imsrc->absolute_frame, (double) interval * 1000000.0 / (finish_time - start_time), imsrc->timestamp);
  }

  if (print_image_number)
    printf("%i\n", imsrc->relative_frame);
}

//----------------------------------------------------------------------------

/// handle parameter changes relating to reporting and logging

void UD_Reporter::process_command_line_flags(int argc, char **argv) 
{
  int i;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],                             "-time")) {
      do_timing = TRUE;
      interval = atoi(argv[i+1]);
    }
    else if (!strcmp(argv[i],                        "-printnum")) 
      print_image_number = TRUE;
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/// constructor for class to log timesamp information and save
/// input images to file

UD_Logger::UD_Logger(char *source_path, int image_logging_interval, UD_LadarSourceArray *ladarray)
{
#ifdef SKYNET
  SparrowHawk().rebind("LogRoadImage", &m_log_images);
#endif
  m_log_images = true;

  // local values

  if (image_logging_interval > 0)
    save_image_interval = image_logging_interval;
  else
    save_image_interval = 0;

  // body

  // get timestamp string

  timestamp = UD_datetime_string();

  // open files

  if (!strcmp(source_path, "stdout")) {
    fp = stdout;
    save_image_interval = 0;
  }
  else {
    image_filename = (char *) calloc(256, sizeof(char));
    filename = (char *) calloc(256, sizeof(char));
    command = (char *) calloc(256, sizeof(char));
    path = (char *) calloc(256, sizeof(char));
    
    sprintf(command, "mkdir %s%s", source_path, timestamp);
    system(command);
    sprintf(filename, "%s%s/%s.log", source_path, timestamp, timestamp);

    printf("logging %s\n", timestamp);

    strcpy(path, source_path);

    fp = fopen(filename, "w");

    sprintf(filename, "%s%s/%s.lad", source_path, timestamp, timestamp);
    printf("%s\n", filename);
    //exit(1);

#ifdef SKYNET
    ladar_filename = (char *) calloc(256, sizeof(char));
    sprintf(ladar_filename, "%s%s/%s.lad", source_path, timestamp, timestamp);
    ladarray->ladsrc[0]->init_log2file(ladar_filename);
#endif
    
    if (!fp) {
      printf("no such logging place\n");
    //  exit(1);
    }
  }

  //  free(timestamp);
}

//----------------------------------------------------------------------------

/// write current image (not input image--this is after pyramidization)
/// being processed to file.  calling function responsible for determining
/// that this is happening with the right frequency

void UD_Logger::write(UD_ImageSource *imsrc, UD_RoadFollower *rf)
{
  // image number
  if(!fp)
    return;

  fprintf(fp, "%07i, ", imsrc->relative_frame);

  // timestamp

  fprintf(fp, "%li, %06li, ", imsrc->timestamp_secs, imsrc->timestamp_usecs);

  // tracker state (send road tracker as argument...)

  if (rf) 
    rf->write(fp);
  else
    fprintf(fp, "\n");

  // flush file to force write

  fflush(fp);

  // image

  if (save_image_interval && !(imsrc->relative_frame % save_image_interval)) {

#ifdef SKYNET
    rf->ladarray->ladsrc[0]->log2file(imsrc->relative_frame, (double) imsrc->timestamp_secs + 0.000001 * (double) imsrc->timestamp_usecs);
#endif

    sprintf(image_filename, "%s%s/%s_%i_%06i.jpg", path, timestamp, timestamp, imsrc->num_pyramid_levels - 1, imsrc->relative_frame);
    
    if (imsrc->is_grayscale) {   
      cvConvertImage(imsrc->im, imsrc->gray_im);    // color to gray
      if(m_log_images)
	cvSaveImage(image_filename, imsrc->gray_im);
    }
    
    else {
      cvConvertImage(imsrc->im, imsrc->im, CV_CVTIMG_SWAP_RB);  // BGR -> RGB
      //    cvConvertImage(imsrc->im, imsrc->im, CV_CVTIMG_SWAP_RB | CV_CVTIMG_FLIP);  // BGR -> RGB
      if(m_log_images)
	cvSaveImage(image_filename, imsrc->im);
    }
  }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#ifdef SKYNET

#include"DGCutils"

/// constructor for class to log timesamp information and save
/// input images through timber

UD_LoggerTimber::UD_LoggerTimber(int skynet_key)
  : CSkynetContainer(SNroadfinding, skynet_key),
    CTimberClient(timber_types::roadFinding), m_log_nr(0)
{
}

//----------------------------------------------------------------------------

/// write current image (not input image--this is after pyramidization)
/// being processed to file.  calling function responsible for determining
/// that this is happening with the right frequency

void UD_LoggerTimber::write(UD_ImageSource *imsrc, UD_RoadFollower *rf)
{
  if(!getLoggingEnabled())    //No logging enabled
    return;

  string dir = getLogDir();
  if(checkNewDirAndReset() || !m_index_file) {
    if(m_index_file)
      fclose(m_index_file);

    string filename = dir + "road.log";
    m_index_file = fopen(filename.c_str(), "w");

    m_log_nr=0;
  }


  //Generate image filename
  char buf[50];
  sprintf(buf, "road%06i.jpg", m_log_nr);
  ++m_log_nr;

  unsigned long long timestamp;
  DGCgettime(timestamp);

  if(m_index_file) {
    fprintf(m_index_file, "%llu %s\n", timestamp, buf);
    fflush(m_index_file);
  }

  //Save image
  string filename = dir + buf;
  cvSaveImage(filename.c_str(), imsrc->im);
}


#endif  //SKYNET
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
