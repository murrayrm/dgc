//----------------------------------------------------------------------------
//
//  Road following code based on vanishing point estimation
//  from dominant orientation voting
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2005, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_Aerial
//----------------------------------------------------------------------------

#ifndef UD_AERIAL_DECS

//----------------------------------------------------------------------------

#define UD_AERIAL_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#include <GL/glut.h>

#include "opencv_utils.hh"

#include "UD_RoadFollower.hh"
#include "UD_LadarSource.hh"
#include "UD_StateSource.hh"

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#ifndef MAXSTRLEN
#define MAXSTRLEN  256
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))

#define RDDF_COLS   5

#define RDDF_INDEX  0
#define RDDF_LAT    1     // converted to UTM northing
#define RDDF_LONG   2     // converted to UTM easting
#define RDDF_WIDTH  3     // should be in meters
#define RDDF_SPEED  4     // should be in meters / s
#define RDDF_ZONE   5     // UTM zone

#define RDDF_NORTHING    1     
#define RDDF_EASTING     2     

#define WGS_84_EQUATORIAL_RADIUS        6378137
#define WGS_84_SQUARE_OF_ECCENTRICITY	0.00669438

#define TERRASERVER_TILE_SIDELENGTH     200    // meters (1 m / pix)
#define INV_TERRASERVER_TILE_SIDELENGTH 0.005  

#define MODE_INTERNET_FETCH             1      // get tiles covering RDDF or state log NOT in cache and QUIT
#define MODE_INTERNET_NOFETCH           2      // don't get tiles not in cache (i.e., assume they're there)

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_RoadFollower;

class UD_Aerial
{
  
 public:

  int run_mode;
  char imfilename[MAXSTRLEN];
  IplImage *mosaic;
  int num_tiles, tiles_per_side;
  GLuint *tilename;
  int *tilelut;
  IplImage **tileim;
  UD_DominantOrientations **tiledom;
  UD_StateSource *statesrc;
  int window_id;                             ///< GLUT identifier for this window 
  int width, height;                         ///< aerial window dimensions
  int rddf_num_waypoints;
  double **rddf;
  double safetyMargin;           // how much to expand corridors by to make sure we've got surrounding map info
  int tileXMin, tileYMin, tileXMax, tileYMax;
  int currentTileInitialized;
  int currentTileX, currentTileY;
  double currentTileEasting, currentTileNorthing;    // lower-left corner of tile
  char *tilePath;
  int tileZone;
  int max_history_pts, num_history_pts, cur_history_pt;
  double *northing_history, *easting_history;
  int num_safety_pts;
  double *safety_dx, *safety_dy;
  int num_circle_pts;
  double *circle_dx, *circle_dy;

  int drawblock;
  int blocksize;
  int **block;
  int block_x, block_y;

  // functions

  UD_Aerial(char *, char *, char *, UD_StateSource *);
  void finish_construction(char *, char *, char *, UD_StateSource *);

  int is_RDDF(char *);
  void rasterize_pathfile(char *);

  int easting_pixel(double easting) { return (int) round(easting - 488000); }
  int northing_pixel(double northing) { return (int) round(3838800 - northing); }

  unsigned char *read_PPM(char *, int *, int *);
  void setup_tile_textures();

  void getTileUTMCoords(int, int, double *, double *);

  void initialize_display(int, int);

  static void sdisplay() { aerial->display(); }
  static void skeyboard(unsigned char key, int x, int y) { aerial->keyboard(key, x, y); }
  static void smouse(int button, int state, int x, int y) { aerial->mouse(button, state, x, y); }

  void idle();
  void display();
  void keyboard(unsigned char, int, int);
  void mouse(int, int, int, int);

  void draw_circle(double, double, double, int, int, int);
  void draw_arrowhead(double, double, double, double, int, int, int);
  void draw_string(char *, int, int, int, int, int); 

  void read_RDDF(char *);
  void LLtoUTM(const double, const double, double *, double *, int *);
  void draw_horizontal_line(int, int, int, int, IplImage *);
  void draw_table_point(int, int y, int *, int *, int);
  void dda_table_line(double, double, double, double, int *, int *, int);
  void rasterize_circle(double, double, double, int, IplImage *);
  void rasterize_rectangle(double, double, double, double, double, double, double, double, int, IplImage *);
  void rasterize_RDDF(int, IplImage *);

  void load_grid_tile(int, int, int, int);
  void print_grid();
  void scroll_tile_grid(int, int, int, int);

  int haveTileImage(int, int);
  void getUTMImage(double, double);
  void getTileImage(int, int);

  int getTileNumber(double xy) { return (int) (INV_TERRASERVER_TILE_SIDELENGTH * xy); }

  int tile2im_x(int x) { return x - tileXMin; }   
  int tile2im_y(int y) { return tileYMax - y; }   

  double utm2win_x(double x) { return (width/2 + x - statesrc->easting); }
  double utm2win_y(double y) { return (height/2 + y - statesrc->northing); }

  double win2utm_x(double x) { return (x - width/2 + statesrc->easting); }
  double win2utm_y(double y) { return (y - height/2 + statesrc->northing); }

  int rc2index(int r, int c) { return (r + 1) * tiles_per_side + c + 1; }

  // which "relative" tile (e.g., (0, 0) is center, (-1, -1) is lower-left, etc.) is UTM coord. in?

  int utm2tile_x(double x) { return (getTileNumber(x) - getTileNumber(statesrc->easting)); }
  int utm2tile_y(double y) { return (getTileNumber(y) - getTileNumber(statesrc->northing)); }

  int utm2tile(double x, double y) { return rc2index(utm2tile_y(y), utm2tile_x(x)); }

  // what are pixel coordinates within that tile of UTM coord.?

  int utm2tilepix_x(double x) { return (int) (x - TERRASERVER_TILE_SIDELENGTH * getTileNumber(x)); }
  int utm2tilepix_y(double y) { return (int) (y - TERRASERVER_TILE_SIDELENGTH * getTileNumber(y)); }


  static UD_Aerial *aerial;
};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
