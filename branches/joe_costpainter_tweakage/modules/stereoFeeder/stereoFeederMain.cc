#include "stereoFeeder.hh"
#include <getopt.h>
#include <DGCutils>

enum {
  OPT_NONE, 
  OPT_LOG,
  OPT_NAME,
  OPT_MAXFRAMES,
  OPT_MAXLOOPS,
  OPT_VERBOSE,
  OPT_VOTE,
  OPT_NOVOTE,
  OPT_HELP,
  OPT_FORMAT,
  OPT_SIM,
  OPT_PAIR,
  OPT_FILE,
  OPT_SNKEY,
  OPT_DELAY,
  OPT_RATE,
  OPT_EXPCTRL,
	OPT_ONEFILE,

  NUM_OPTS
};

using namespace std;

void printStereoHelp();

char sourceFilenamePrefix[200]="None";
char logFilenamePrefix[200]="temp";
char optFormat[10]="bmp";
int logFrames=0, logRect=0, logDisp=0, logState=0, logMaps=0, logTime=0, logVotes=0, logPoints=0;
int optVote=1, optSparrow=1, optDisplay=0, optMaxFrames=-1, optMaxLoops=-1, optVerbose=0, 
	optUseCameras=1, optName=0, optWait=1, optLog=0, optPair=SHORT_RANGE, 
	optSim=0, optFile=0, optStep=1, optSNKey=-1, optExpCtrl=0, optZeroAltitude=0,
	optOneFile=0;
double optDelay=-1, optRate=-1;
char paramFile[256];


int main(int argc, char** argv) {
  int c;
  char pairString[256] = "Short Range";

  static struct option long_options[] = {
    //Options that don't require arguments:
    {"sparrow", no_argument, &optSparrow, 1},
    {"nosparrow", no_argument, &optSparrow, 0},
    {"help",      no_argument, 0, OPT_HELP},
    {"vote",  no_argument, &optVote, 1},
    {"novote", no_argument, &optVote, 0},
    {"display", no_argument, &optDisplay, 1},
    {"wait", no_argument, &optWait, 1},
    {"nowait", no_argument, &optWait, 0},
    {"expctrl", no_argument, &optExpCtrl, 1},
    {"zero", no_argument, &optZeroAltitude, 1},
    //Options that require arguments
    {"name", required_argument, 0, OPT_NAME}, 
    {"log",    required_argument, 0, OPT_LOG},
    {"pair", required_argument, 0, OPT_PAIR},
    {"sim", required_argument, 0, OPT_SIM},
    {"file", required_argument, 0, OPT_FILE},
    {"format", required_argument, 0, OPT_FORMAT},
    {"maxframes", required_argument, 0, OPT_MAXFRAMES},
    {"maxloops", required_argument, 0, OPT_MAXLOOPS},
    {"snkey", required_argument, 0, OPT_SNKEY},
    {"delay", required_argument, 0, OPT_DELAY},
    {"rate", required_argument, 0, OPT_RATE},
		{"onefile", required_argument, 0, OPT_ONEFILE},
    //Options that have optional arguments
    {"verboselevel", optional_argument, 0, OPT_VERBOSE},
    {0,0,0,0}
  };
  
  while (1) {
    int option_index = 0;
    
    c = getopt_long_only(argc, argv, "", long_options, &option_index);
    if(c == -1) break;

    switch(c) {
    case OPT_VERBOSE:
      if(optarg != NULL) {
	optVerbose=atoi(optarg);
      } else {
	optVerbose=1;
      }
      break;
    case '?':
    case OPT_HELP:
      printStereoHelp();
      exit(1);
      break;
    case OPT_NAME:
      optName = 1;
      sprintf(logFilenamePrefix, "%s", optarg);
      break;
    case OPT_LOG:
      optLog = 1;
      if(optarg) {
	if(strstr(optarg, "all")) {	
	  logFrames = 1;
	  logRect = 1;
	  logDisp = 1;
	  logState = 1;
	  logMaps = 1;
	  logTime = 1;
	  logVotes = 1;
	  logPoints = 1;
	  break;
  	}
	if(strstr(optarg, "base")) {
	  logFrames = 1;
	  logState = 1;
	  break;
	}
	if(strstr(optarg, "quickdebug") || strstr(optarg, "qd") || strstr(optarg, "debug")) {
	  logDisp = 1;
	  logVotes = 1;
	  break;
	}
	if(strstr(optarg, "alldebug") || strstr(optarg, "ad")) {
	  logRect = 1;
	  logDisp = 1;
	  logMaps = 1;
	  logTime = 1;
	  logVotes = 1;
	  logPoints = 1;
	  break;
	}
	if(strchr(optarg, 'f')) logFrames = 1;
	if(strchr(optarg, 'd'))	logDisp = 1;
	if(strchr(optarg, 'r')) logRect = 1;
	if(strchr(optarg, 'm')) logMaps = 1;
	if(strchr(optarg, 's')) logState = 1;
	if(strchr(optarg, 't')) logTime = 1;
	if(strchr(optarg, 'v')) logVotes = 1;
	if(strchr(optarg, 'p')) logPoints = 1; 
      }
      break;
    case OPT_FORMAT:
      sprintf(optFormat, "%s", optarg);
      break;
    case OPT_MAXFRAMES:
      optMaxFrames = atoi(optarg);
      break;
    case OPT_MAXLOOPS:
      optMaxLoops = atoi(optarg);
      break;
    case OPT_PAIR:
      optUseCameras = 1;
      if(strcmp(optarg, "short")==0 || strcmp(optarg, "s")==0) {
	optPair = SHORT_RANGE;
	sprintf(pairString, "Short Range");
      } else if(strcmp(optarg, "long")==0 || strcmp(optarg, "l")==0) {
	optPair = LONG_RANGE;
	sprintf(pairString, "Long Range");
      } else if(strcmp(optarg, "short_color")==0 || strcmp(optarg, "sc")==0) {
	optPair = SHORT_COLOR;
	sprintf(pairString, "Short Color");
      } else if(strcmp(optarg, "homer")==0 || strcmp(optarg, "h")==0) {
	optPair = HOMER;
	sprintf(pairString, "Homer");
      } else {
	printf("No such pair as '%s' is known - please use short, long, short_color, or homer\n", optarg);
	printStereoHelp();
	exit(1);
      }
      break;
    case OPT_FILE:
      optFile = 1;
      optUseCameras = 0;
      sprintf(sourceFilenamePrefix, "%s", optarg);
      break;
		case OPT_ONEFILE:
			optOneFile = 1;
    case OPT_SIM:
      optSim = 1;
      optUseCameras = 0;
      sprintf(sourceFilenamePrefix, "%s", optarg);
      break;
    case OPT_SNKEY:
      optSNKey = atoi(optarg);
      break;
    case OPT_DELAY:
      optDelay = atof(optarg);
      break;
    case OPT_RATE:
      optRate = atof(optarg);
      break;
    default:
      if(c!=0) {
	printf("Unknown option %d!\n", c);
	printStereoHelp();
	exit(1);
      }
    }
  }

  //Setup skynet
  int intSkynetKey = 0;
  char* ptrSkynetKey = getenv("SKYNET_KEY");
  if(ptrSkynetKey == NULL && optSNKey==-1) {
    //FIX ME
    printf("no sn key");
    return 0;
  } else {
    printf("Welcome to StereoFeederMain\n");
    if(optSNKey==-1) {
      intSkynetKey = atoi(ptrSkynetKey);
    } else {
      intSkynetKey = optSNKey;
    }
	}

  if(optLog && !optName) {
    printf("You must specify a base filename (using the --name option) to do command-line logging!\n");
    printStereoHelp();
    exit(1);
  }
  if(optDelay!=-1 && optRate!=-1) {
    printf("You cannot specify both a delay and a frame rate - choose one!\n");
    printStereoHelp();
    exit(1);
  }
  
  printf("Option Summary - General:\n");
  printf("SkynetKey      - %d\n", intSkynetKey);
  printf("Voting         - %d\n", optVote);
  printf("Sparrow        - %d\n", optSparrow);
  printf("Display        - %d\n", optDisplay);
  printf("Verbose Level  - %d\n", optVerbose);

  if(optZeroAltitude) 
    printf("Using Zero Alt - True!\n");
  if(optDelay!=-1) {
    printf("Delay          - %lfsec\n", optDelay);
  }
  if(optRate!=-1) {
    printf("Rate           - %lfHz\n", optRate);
  }

  if(optUseCameras) {
    printf("Using Cameras  - %s\n", pairString);
  } else {
    if(optFile) printf("Using Files    - %s\n", sourceFilenamePrefix);
    if(optSim)     printf("Real-Time Sim  - %s\n", sourceFilenamePrefix);
		if(optOneFile) printf("Using One File - %s0001\n", sourceFilenamePrefix);
  }
  if(optMaxFrames != -1) {
    printf("Maximum Frames - %d\n", optMaxFrames);
  }
  if(optMaxLoops != -1) {
    printf("Maximum Loops  - %d\n", optMaxLoops);
  }
  if(optLog) {
    printf("Option Summary - Logging:\n");
    printf("Log File Name  - %s\n", logFilenamePrefix);
    printf("Format         - %s\n", optFormat);
    printf("Frames: %d | Rect:   %d\n", logFrames, logRect);
    printf("State:  %d | Maps:   %d\n", logState, logMaps);
    printf("Disp:   %d | Time:   %d\n", logDisp, logTime);
    printf("Votes:  %d | Points: %d\n", logVotes, logPoints);
  }
  if(logMaps) printf("WARNING!  Logging maps significantly slows down the program\n");
  if(logFrames && !logState) printf("WARNING!  Logging frames but not state means you won't be able to run these images in simulation!\n");
  if(optVerbose>1 && optSparrow) printf("WARNING!  Using a verbose level greater than 1 may wreak havoc on the Sparrow display!\n");
  if(optSim && optFile) {
    printf("Cannot use both real-time sim (--sim) and slow files (--file)\n");
    printStereoHelp();
    exit(1);
  }
  
  printf(" == HIT CTRL+C TO CANCEL ==\n");
  const int waitTime = 7;
  if(optWait) {
    for(int i=0; i<waitTime; i++) {
      printf("Program will start in %d seconds...\r", waitTime-i);
      fflush(stdout);
      sleep(1);
    }
  }
  
	StereoFeeder* stereoFeederObj = new StereoFeeder(intSkynetKey);
    
    
	//start extra threads here

	DGCstartMemberFunctionThread(stereoFeederObj, &StereoFeeder::ImageCaptureThread);
	if(optSparrow==1) {
		DGCstartMemberFunctionThread(stereoFeederObj, &StereoFeeder::SparrowDisplayLoop);
	}
	
	stereoFeederObj->ActiveLoop();

  return 0;
}


void printStereoHelp() {
  printf("Usage: StereoPlanner [OPTION]\n");
  printf("Runs the DGC StereoPlanner software\n\n");

  printf("  --sparrow           Shows the sparrow display\n");
  printf("  --nosparrow         Does not show the sparrow display\n");
  printf("  --help              Prints this help message\n");
  printf("  --vote              Makes sure the StereoPlanner sends votes to the Arbiter\n");
  printf("  --novote            Prevents the StereoPlanner from voting\n");
  printf("  --display           Displays real-time rectified and disparity images\n");
  printf("  --wait              Enables a wait before the program begins\n");
  printf("  --nowait            Disables a wait before the program begins\n");
  printf("  --log LOG           Enables logging of varous data denoted by LOG (described below), must use --name\n");
  printf("  --name NAME         Specifies the base filename NAME to be used when logging data (default is temp)\n");
  printf("  --pair PAIR         Use the pair of cameras denoted by PAIR (described below)\n");
  printf("  --sim SIM           Run off of logged images (dropping frames) with the base filename SIM\n");
  printf("  --file FILE         Run off of logged images (using every frame) with the base filename FILE\n");
  printf("  --format FORMAT     Reads logged frames and saves frames using format FORMAT (default is bmp)\n");
  printf("  --maxframes NUM     Runs the code for NUM frame grabs\n");
  printf("  --maxloops NUM      Runs the code for NUM processing loops\n");
  printf("  --zero              Assumes zero altitude (ignores altitude from state)\n");
	printf("  --onefile FILE      Continually run stereo off of just one file with the base filename FILE\n");
  printf("  --verboselevel NUM  Not yet supported\n");

  printf("\nIn --log LOG, LOG must be some combination of the following:\n");
  printf("f=frames, d=disparity, r=rectified images, m=maps, s=state, t=timing data, v=votes, p=point clouds.\n");
  printf("Additionally the shortcuts all=fdrmstvp, base=fs, quickdebug=dv, alldebug=rdmvtp each work individually.\n");

  printf("\nIn --pair PAIR, PAIR must be one of the following:\n");
  printf("Short Range: short, s; Long Range: long, l; Short Color: short_color, sc; Homer: homer, h;\n");
}
