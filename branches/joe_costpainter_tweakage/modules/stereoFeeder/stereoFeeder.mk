STEREOFEEDER_PATH = $(DGC)/modules/stereoFeeder

STEREOFEEDER_DEPEND_LIBS = \
			   $(DGCUTILS) \
			   $(MODULEHELPERSLIB) \
			   $(SKYNETLIB) \
			   $(TIMBERLIB) \
			   $(SPARROWLIB) \
			   $(STEREOSOURCELIB) \
			   $(STEREOPROCESSLIB) \
			   $(FRAMESLIB) \
			   $(CMAPPLIB) \
			   $(CMAPLIB) \
			   $(GPLIB) \
			   $(CELEVFUSERLIB) \
			   $(CPIDLIB) \
			   -lpthread -lncurses

STEREOFEEDER_DEPEND_SOURCES = \
	$(STEREOFEEDER_PATH)/stereoFeeder.cc \
	$(STEREOFEEDER_PATH)/stereoFeeder.hh \
	$(STEREOFEEDER_PATH)/stereoFeederDisplay.cc \
	$(STEREOFEEDER_PATH)/stereoFeederDisplay.dd \
	$(STEREOFEEDER_PATH)/stereoFeederMain.cc \
	$(STEREOFEEDER_PATH)/Makefile

STEREOFEEDER_DEPEND = $(STEREOFEEDER_DEPEND_LIBS) $(STEREOFEEDER_DEPEND_SOURCES)
