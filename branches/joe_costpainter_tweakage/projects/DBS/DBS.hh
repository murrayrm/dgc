#ifndef DBS_HH
#define DBS_HH

#include "StateClient.h"
#include <pthread.h>
#include <unistd.h>
#include <sstream>
#include "AliceConstants.h" 
#include "sn_msg.hh"
#include "sn_types.h"
#include <iomanip>
#include <string>
#include "DGCutils"
#include <math.h>
#include <fftw3.h>
#include <map>
#include "gnuplot_i.h"
#include "trajF_speedCap_cmd.hh"

using namespace std;


#define DBS_NUM_VALUES 40
#define MAXRANGE 2
#define FREQUENCY 2
#define AVESIZE 10

#define ASTATE_FIELDS 19
#define ASTATE_FIELD_FFT_START 2    // don't make this < 2
#define ASTATE_FIELD_FFT_END   19

#define DO_CURRENT_INFO_PLOT  0
#define CURRENT_INFO_RANGE    1     /**< from -value to value */

#define DO_HISTORY_PLOT       1
#define HISTORY_LENGTH        150
#define HISTORY_RANGE_MIN     0
#define HISTORY_RANGE_MAX     10

/** name, multiplier*/
#define HIST_INFO(_) \
_(pitch_1_20,          6) \
_(pitch_5_8,          15) \
_(zdot_1_20,          .6) \
_(zdot_10_20,          4) \
_(speed,              .5) \
_(bumpiness,          10) \
_(worst_bumpiness,    10) \
_(decaying_bumpiness, 10) \
_(speed_cap,          .5) \
_(worst_speed_cap,    .5) \
_(decaying_speed_cap, .5)

/** name, enum val, state mulitiplier (for plot), fft multiplier (for plot) */
#define STATE_INFO(_) \
_(Timestamp,=1, 1  , 1  ) \
_(Northing,   , 1  , 1  ) \
_(Easting,    , 1  , 1  ) \
_(Altitude,   , 1  , 1  ) \
_(Vel_N,      , 1  , 1  ) \
_(Vel_E,      , 1  , 1  ) \
_(Vel_D,      , 1  , 1  ) \
_(Acc_N,      , 1  , 1  ) \
_(Acc_E,      , 1  , 1  ) \
_(Acc_D,      , 1  , 1  ) \
_(Roll,       , 1  , 1  ) \
_(Pitch,      , 1  , 1  ) \
_(Yaw,        , 1  , 1  ) \
_(RollRate,   , 1  , 1  ) \
_(PitchRate,  , 1  , 1  ) \
_(YawRate,    , 1  , 1  ) \
_(RollAcc,    , 1  , 1  ) \
_(PitchAcc,   , 1  , 1  ) \
_(YawAcc,     , 1  , 1  )

#define HIST_INDEX(hist_enum, i) hist_enum * HISTORY_LENGTH + i

#define STATE_ENUM(name, val, s_mult, f_mult) name val,
#define HIST_ENUMS(name, mult) name,

#define STATE_FIELD_MAP(name, val, s_mult, f_mult) state_field_map[asf::name] = \
(string)#name + " (x " + #s_mult + ")";
#define STATE_FIELD_FFT_MAP(name, val, s_mult, f_mult) state_field_fft_map[asf::name] = \
(string)#name + " fft (x " + #f_mult + ")";
#define HIST_FIELD_MAP(name, mult) hist_field_map[hist::name] = (string)#name + " (x " + #mult + ")";
#define HIST_MULT_MAP(name, mult) hist_multiplier_map[hist::name] = mult;
#define STATE_MULT_MAP(name, val, s_mult, f_mult) state_multiplier_map[asf::name] = s_mult;
#define STATE_FFT_MULT_MAP(name, val, s_mult, f_mult) state_fft_multiplier_map[asf::name] = f_mult;

namespace asf // vehicle state fields
{
  enum the_enum
    {
      STATE_INFO(STATE_ENUM)
      LAST
    };
}

namespace areas
{
  enum areas_enum
    {
      pitch_1_20 = 0,
      pitch_5_8,
      zdot_1_20,
      zdot_10_20,
      LAST
    };
}

namespace hist
{
  enum hist_enum
    {
      HIST_INFO(HIST_ENUMS)
      LAST
    };
}


/**
 * DBS class.
 * input: state
 * output: bumpiness, speed cap
 */
class DBS : public CStateClient
{
public:
  /** Contstructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  DBS(int skynetKey);

  /** Standard destructor */
  ~DBS();

  
  /** Main program loop... execution is trapped here */
  void ActiveLoop();

  //Return all mean values
  void stats(VehicleState &means, VehicleState &vars);
  
  void fftcalculatorLastN();
  //int GetRoadType();


private:
  /** Array that stores the offsets within a VehicleState struct for all
   * the different elements */
  double VehicleState::*vs_table[ASTATE_FIELDS+1];

  void PrintValue(fftw_complex* fft);
  void PrintDebug(fftw_complex* in, fftw_complex* out);
  void PrintValueSum(fftw_complex* fftA);



  /********************************************************
   *
   * DBS State stuff
   *
   ********************************************************/

  /** retrieves the most recent state values and recalculates all FFT's
   * and area stuff */
  void UpdateDBSState();

  struct DBS_state
  {
    /** State buffer (last DBS_NUM_VALUES) */
    VehicleState astate_buffer[DBS_NUM_VALUES];

    /** Most recent FFT's 
     * (ASTATE_FIELDS + 1 because field 19 requires
     * 20 spaces) */
    double astate_fft[ASTATE_FIELDS + 1][DBS_NUM_VALUES];

    /** Area under most recent FFT's */
    struct FFTArea
    {
      double area;
      int field;
      int start;
      int end;
    };

    FFTArea* fft_area;

    /** DBS_state constructor */
    DBS_state::DBS_state()
    {
      cout << "before, fft_area is " << fft_area << endl;
      for (int field = 0; field < ASTATE_FIELDS; field++)
	for (int i = 0; i < DBS_NUM_VALUES; i++)
	  astate_fft[field][i] = 0;
      cout << "middle, fft_area is " << fft_area << endl;
      cout << "DBS_state constructor, creating new fft_area with length "
	   << areas::LAST << "." << endl;
      fft_area = new FFTArea[areas::LAST];
      cout << "after, fft_area is " << fft_area << endl;
      for (int i = 0; i < areas::LAST; i++)
	{
	  cout << "setting fft_area[" << i << "].area = 0" << endl;
	  this->fft_area[i].area = 0;
	}
      
      fft_area[areas::pitch_1_20].field = asf::Pitch;
      fft_area[areas::pitch_1_20].start = 1;
      fft_area[areas::pitch_1_20].end = 20;
      fft_area[areas::pitch_5_8].field = asf::Pitch;
      fft_area[areas::pitch_5_8].start = 5;
      fft_area[areas::pitch_5_8].end = 8;
      fft_area[areas::zdot_1_20].field = asf::Vel_D;
      fft_area[areas::zdot_1_20].start = 1;
      fft_area[areas::zdot_1_20].end = 20;
      fft_area[areas::zdot_10_20].field = asf::Vel_D;
      fft_area[areas::zdot_10_20].start = 10;
      fft_area[areas::zdot_10_20].end = 20;

      cout << "made it thorugh DBS constructor" << endl;
    }
    DBS_state::~DBS_state() { delete [] fft_area; }
  };

  /* keeps track of anything we need from one iteration of DBS to
   * the next */
  DBS_state * m_dbs_state;



  /********************************************************
   *
   * FFT stuff
   *
   ********************************************************/

  /** handle to the FFT plan, which is generated once and run many
   * times*/
  fftw_plan plan;

  /** buffers used for the FFT function */
  fftw_complex *fft_in;
  fftw_complex *fft_out;



  /********************************************************
   *
   * Plotting stuff
   *
   ********************************************************/

  /** updates the plotting state based on the new m_dbs_state */
  void UpdatePlotState();

  /** Plots the current ffts and states */
  void PlotCurrentInfo();
  gnuplot_ctrl* current_info_plot;

  void PlotAstateField(int field);
  void PlotFFT(int field);

  /** Plots the history of variables */
  void PlotHistory();
  gnuplot_ctrl* history_plot;

  void PlotThisHist(int hist_field);

  /** use the push functions to keep track of stuff over time */
  void PushArea(int hist_field, int area);
  void PushSpeed(int hist_field);
  void PushDouble(int hist_field, double value);
  
  struct Plot_state
  {
    double* current_info_ranges;
    double* history_ranges;

    double* history_data;
  };

  /** keeps track of everthing needed for plotting */
  Plot_state * m_plot_state;

  /** map from ints to names of fields (for labeling
   * the graphs) */
  map<int, string> state_field_map;
  map<int, string> state_field_fft_map;
  map<int, double> state_multiplier_map;
  map<int, double> state_fft_multiplier_map;
  map<int, string> hist_field_map;
  map<int, double> hist_multiplier_map;



  /********************************************************
   *
   * Conclusions
   *
   ********************************************************/
  void UpdateConclusions();

  double GetBumpiness();

  struct DBS_Conclusions
  {
    double bumpiness;
    double worst_bumpiness;
    double decaying_bumpiness;
    double speed_cap;
    double worst_speed_cap;
    double decaying_speed_cap;
  };

  DBS_Conclusions m_conc;

};


#endif  // DBS_HH
