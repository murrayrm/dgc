#include "DBS.hh"

using namespace std;

#define bugme BUGME(__FILE__, __LINE__)
void BUGME(string file, int line) {cout << " [" << file << ":" << line << "]" << endl;}

extern int NOSKYNET;                /**< for command line argument */

DBS::DBS(int skynetKey) : CSkynetContainer(MODDBS, skynetKey)
{
  m_dbs_state = new DBS_state;
  m_plot_state = new Plot_state;

  m_plot_state->history_data = new double[hist::LAST * HISTORY_LENGTH];
  for (int i = 0; i < hist::LAST*HISTORY_LENGTH; i++)
    m_plot_state->history_data[i] = 0;

  // initialize plots
  if (DO_CURRENT_INFO_PLOT)
    current_info_plot = gnuplot_init();
  m_plot_state->current_info_ranges = new double [DBS_NUM_VALUES];
  for (int i = 0; i < DBS_NUM_VALUES; i++)
    m_plot_state->current_info_ranges[i] = -CURRENT_INFO_RANGE;
  m_plot_state->current_info_ranges[0] = CURRENT_INFO_RANGE;

  if (DO_HISTORY_PLOT)
    history_plot = gnuplot_init();
  m_plot_state->history_ranges = new double [HISTORY_LENGTH];
  for (int i = 0; i < HISTORY_LENGTH; i++)
    m_plot_state->history_ranges[i] = HISTORY_RANGE_MIN;
  m_plot_state->history_ranges[0] = HISTORY_RANGE_MAX;


  // get all offsets within VehicleState and save them in our array
  //  vs_table[0] = 0x123; // not used
  //  vs_table[asf::Timestamp] = 0x; // not used; can't take fft of timestamp
  vs_table[asf::Northing] = &VehicleState::Northing;
  vs_table[asf::Easting] = &VehicleState::Easting;
  vs_table[asf::Altitude] = &VehicleState::Altitude;
  vs_table[asf::Vel_N] = &VehicleState::Vel_N;
  vs_table[asf::Vel_E] = &VehicleState::Vel_E;
  vs_table[asf::Vel_D] = &VehicleState::Vel_D;
  vs_table[asf::Acc_N] = &VehicleState::Acc_N;
  vs_table[asf::Acc_E] = &VehicleState::Acc_E;
  vs_table[asf::Acc_D] = &VehicleState::Acc_D;
  vs_table[asf::Roll] = &VehicleState::Roll;
  vs_table[asf::Pitch] = &VehicleState::Pitch;
  vs_table[asf::Yaw] = &VehicleState::Yaw;
  vs_table[asf::RollRate] = &VehicleState::RollRate;
  vs_table[asf::PitchRate] = &VehicleState::PitchRate;
  vs_table[asf::YawRate] = &VehicleState::YawRate;
  vs_table[asf::RollAcc] = &VehicleState::RollAcc;
  vs_table[asf::PitchAcc] = &VehicleState::PitchAcc;
  vs_table[asf::YawAcc] = &VehicleState::YawAcc;

  /*
  // more ugliness to initialize the map from enum to string
  state_field_map[asf::Timestamp] = "Timestamp";
  state_field_map[asf::Northing] = "Northing";
  state_field_map[asf::Easting] = "Easting";
  state_field_map[asf::Altitude] = "Altitude";
  state_field_map[asf::Vel_N] = "Vel_N";
  state_field_map[asf::Vel_E] = "Vel_E";
  state_field_map[asf::Vel_D] = "Vel_D";
  state_field_map[asf::Acc_N] = "Acc_N";
  state_field_map[asf::Acc_E] = "Acc_E";
  state_field_map[asf::Acc_D] = "Acc_D";
  state_field_map[asf::Roll] = "Roll";
  state_field_map[asf::Pitch] = "Pitch";
  state_field_map[asf::Yaw] = "Yaw";
  state_field_map[asf::RollRate] = "RollRate";
  state_field_map[asf::PitchRate] = "PitchRate";
  state_field_map[asf::YawRate] = "YawRate";
  state_field_map[asf::RollAcc] = "RollAcc";
  state_field_map[asf::PitchAcc] = "PitchAcc";
  state_field_map[asf::YawAcc] = "YawAcc";
  */
  STATE_INFO(STATE_FIELD_MAP)
  STATE_INFO(STATE_FIELD_FFT_MAP)
  STATE_INFO(STATE_MULT_MAP)
  STATE_INFO(STATE_FFT_MULT_MAP)

  HIST_INFO(HIST_FIELD_MAP)
  HIST_INFO(HIST_MULT_MAP)

  // initialize FFT plannner
  fft_in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  fft_out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  plan = fftw_plan_dft_1d(DBS_NUM_VALUES, fft_in, fft_out, FFTW_FORWARD, FFTW_ESTIMATE);
}



DBS::~DBS()
{
  cout << "DBS destructor has been called." << endl;
  delete [] m_dbs_state;
  delete [] m_plot_state;
  fftw_destroy_plan(plan);
  fftw_free(fft_in);
  fftw_free(fft_out);
  gnuplot_close(current_info_plot);
  gnuplot_close(history_plot);
}



void DBS::fftcalculatorLastN()
{
  // make a new chunk of memory to use as a buffer for the last
  // N values
  VehicleState* buffer = new VehicleState[DBS_NUM_VALUES];  

  // get last DBS_NUM_VALUES state values and store them in our buffer,
  // using GetLastNValues(DBS_NUM_VALUES,destination
  GetLastNValues(DBS_NUM_VALUES,buffer);

  // do this:
  fftw_complex *in;
  fftw_complex *out;
  fftw_plan p;
  in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  
  // copy the pitch from our buffer to "in"
  int n;
  for(n=0;n<DBS_NUM_VALUES;n++){
    in[n][0] = buffer[n].Pitch;
  }
  
  // do the fft
  p = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);

  // do something with the fft results
  PrintDebug(in, out);
  PrintValueSum(out);

  // at end, free the memory
  fftw_destroy_plan(p);
  fftw_free(in); fftw_free(out);
  delete [] buffer; 
}



/*
void DBS::stats(VehicleState &means, VehicleState &vars) {
  double vR, sumR, mVR,r,sumVR,varR,vPR,sumPR,mVPR,pR,sumVPR,varPR,vRR,sumRR,mVRR,rR,sumVRR,varRR,sumP,mVP,p,sumVP,varP,vfDD,sumfDD,mVfDD,fDD,sumVfDD,varfDD,vsDD,sumsDD,mVsDD,sDD,sumVsDD,varsDD,maxAP,maxAR;
  int n;  
  
  sumPR=0;
  sumVPR=0; 
  sumR=0;
  sumVR=0;
  sumRR=0;
  sumVRR=0;
  sumP=0;
  sumVP=0;
  
  sumfDD=0;
  sumVfDD=0;
  sumsDD=0;
  sumVsDD=0;
  n=0;
 
   
  //MaxAP = 10.87;
  // MaxAR = 7.96;

   
     //if(fftAP<MaxAP && fftAR<MaxAR){
       //Hur sager jag till systemet att sakte ner
       //else
   //Hur sager jag till systemet att oka hastigheten
   
   //terrainclassification
   //while(n!= DBS_NUM_VALUES ){
     //if fftAP<0.6
	  //pavedroad
//if fftAP<0.6 && fftAP<5
//  cout<<dirtroad<<;
//  else
//    cout<<offroad<<;

 while(n!=DBS_NUM_VALUES){
   r = m_dbs_states[n].Roll;
   sumR = sumR + r ;
   n++;
 }
 means.Roll = sumR/DBS_NUM_VALUES; 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   r = m_dbs_states[n].Roll;
   sumVR  =sumVR + pow(r-means.Roll,2);
   n++;
 }
 vars.Roll =sumVR/DBS_NUM_VALUES;

//"MeanValue and Variance rollrate"
 n=0;
 while(n!=DBS_NUM_VALUES){
   rR = m_dbs_states[n].RollRate;
   sumRR = sumRR   + rR ;
   n++;
 }
 means.RollRate = sumRR  /DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   rR = m_dbs_states[n].RollRate;
   sumVRR  =sumVRR + pow(r-mVRR,2);
   n++;
 }
 vars.RollRate = sumVRR/DBS_NUM_VALUES;

// pitchrate MeanValue and Variance pitchrate"
 n=0;
 while(n!=DBS_NUM_VALUES){
   pR = m_dbs_states[n].PitchRate;
   sumPR = sumPR + pR ;
   n++;
 }
 means.PitchRate = sumPR  /DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   pR = m_dbs_states[n].PitchRate;
   sumVPR = sumVPR + pow(pR-means.PitchRate,2);
   n++;
 }
 vars.PitchRate = sumPR/DBS_NUM_VALUES;

 //MeanValue and Variance pitch
 n=0;
 while(n!=DBS_NUM_VALUES){
   p = m_dbs_states[n].Pitch;
   sumP = sumP + p ;
   n++;
 }
 means.Pitch = sumP/DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   p = m_dbs_states[n].Pitch;
   sumVP  =sumVP + pow(p-means.Pitch,2);
   n++;
 }
 vars.Pitch = sumVP/DBS_NUM_VALUES;

// first derivates down, mean value and varianceMeanValue and Variance
 n=0;
 while(n!=DBS_NUM_VALUES){
   fDD = m_dbs_states[n].Vel_D;
   sumfDD = sumfDD + fDD;
   n++;
 }
 means.Vel_D = sumfDD  /DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   fDD = m_dbs_states[n].Vel_D;
   sumVfDD  =sumVfDD+ pow(fDD-means.Vel_D,2);
   n++;
 }
 vars.Vel_D = sumVfDD/DBS_NUM_VALUES;

// second derivates down, meanvalue and variance 
 n=0;
 while(n!=DBS_NUM_VALUES){
   sDD = m_dbs_states[n].Acc_D;
   sumsDD = sumsDD + sDD;
   n++;
 }
 means.Acc_D = sumsDD/DBS_NUM_VALUES; 
 
 n=0;
 while(n!=DBS_NUM_VALUES){ 
   sDD = m_dbs_states[n].Acc_D;
   sumVsDD=sumVsDD+ pow(sDD-means.Acc_D,2);
   n++;
 }
 vars.Acc_D = sumVsDD/DBS_NUM_VALUES;

}
*/



void DBS::ActiveLoop()
{
  int socket = m_skynet.get_send_sock(SNtrajFspeedCapCmd);
  newSpeedCapCmd speed_cap_container;
  speed_cap_container.m_speedCapSndModule = TF_MOD_DBS;
  speed_cap_container.m_speedCapAction = ADD_MAX_SPEEDCAP;

  int counter = 0;
  // main loop
  while (true)
    {
      counter++;
      UpdateDBSState();

      UpdateConclusions();


      UpdatePlotState();

      /** Replot stuff */
      if (DO_CURRENT_INFO_PLOT)
	PlotCurrentInfo();
      if (DO_HISTORY_PLOT)
	PlotHistory();


      cout << setw(3) << counter << " (bumpiness, speed_cap) = (" << m_conc.bumpiness 
	   << ", " << m_conc.speed_cap << ")";

      cout << "  " << m_dbs_state->fft_area[areas::zdot_1_20].area << endl;

      speed_cap_container.m_speedCapArgument = m_conc.decaying_speed_cap;
      m_skynet.send_msg(socket, &speed_cap_container, sizeof(speed_cap_container), 0);

      usleep(1000000/FREQUENCY);
    }

  //  gnuplot_close(handle);  // deletes files in /tmp or /usr/tmp or something
}



void DBS::UpdateDBSState()
{
  /** get last DBS_NUM_VALUES state values and store them in our buffer,
   * using GetLastNValues(DBS_NUM_VALUES,destination) */
  GetLastNValues(DBS_NUM_VALUES, m_dbs_state->astate_buffer);

  /** take the FFT's of all variables we're interested in */
  for (int field = ASTATE_FIELD_FFT_START; field <= ASTATE_FIELD_FFT_END; field++)
    {
      for (int i = 0; i < DBS_NUM_VALUES; i++)
	fft_in[i][0] = m_dbs_state->astate_buffer[i].*(vs_table[field]);
      fftw_execute(plan);
      for (int i = 0; i < DBS_NUM_VALUES; i++)
 	(m_dbs_state->astate_fft)[field][i] = sqrt(fft_out[i][0]*fft_out[i][0]+fft_out[i][1]*fft_out[i][1]);
    }
    
  /** recalculate area variables */
  for (int area_index = 0; area_index < areas::LAST; area_index++)
    {
      m_dbs_state->fft_area[area_index].area = 0;
      for (int i = m_dbs_state->fft_area[area_index].start; i < m_dbs_state->fft_area[area_index].end; i++)
	m_dbs_state->fft_area[area_index].area += 
	  m_dbs_state->astate_fft[m_dbs_state->fft_area[area_index].field][i];
    }
  
  //   /** recalculate means and stddevs */
  // #define CALC_FFT_HIST(handle, variable, iterations) **backslash**
      // m_dbs_state->##handle##_mean = m_dbs_state->##handle##_stddev = 0; **backslash**
      // for(int i = iterations - 1; i > 0; i--) **backslash**
      // { m_dbs_state->##handle##_history[i] =  m_dbs_state->##handle##_history[i-1]; **backslash**
      // m_dbs_state->##handle##_mean += handle##_history[i]; } **backslash**
      // m_dbs_state->##handle##_history[0] = m_dbs_state->variable; **backslash**
      // m_dbs_state->##handle##_mean += handle##_history[0]; **backslash**
      // for(int i = 0 i < iterations; i++) **backslash**
      // { m_dbs_state->##handle##_stddev += pow((m_dbs_state->##handle##_history[i]- handle##_mean),2); }**backslash**
      // m_dbs_state->##handle##_stddev = pow(m_dbs_state->##handle##_stddev / iterations, .5);
      
      //   FFT_HIST_INFO(CALC_FFT_HIST)
}



void DBS::UpdatePlotState()
{
  PushArea(areas::pitch_1_20, hist::pitch_1_20);
  PushArea(areas::pitch_5_8, hist::pitch_5_8);
  PushArea(areas::zdot_1_20, hist::zdot_1_20);
  PushArea(areas::zdot_10_20, hist::zdot_10_20);
  PushSpeed(hist::speed);
  PushDouble(hist::bumpiness, m_conc.bumpiness);
  PushDouble(hist::worst_bumpiness, m_conc.worst_bumpiness);
  PushDouble(hist::decaying_bumpiness, m_conc.decaying_bumpiness);
  PushDouble(hist::speed_cap, m_conc.speed_cap);
  PushDouble(hist::worst_speed_cap, m_conc.worst_speed_cap);
  PushDouble(hist::decaying_speed_cap, m_conc.decaying_speed_cap);
}



void DBS::PlotCurrentInfo()
{
  gnuplot_resetplot(current_info_plot);

  // plot a range box to make it not 'blink'
  gnuplot_setstyle(current_info_plot, "dots");
  gnuplot_plot_x(current_info_plot, m_plot_state->current_info_ranges, 2, "-");
  gnuplot_setstyle(current_info_plot, "lines");

  PlotAstateField(asf::Pitch);
  PlotAstateField(asf::Yaw);
  PlotFFT(asf::Pitch);
  PlotFFT(asf::Yaw);
}



void DBS::PlotHistory()
{
  gnuplot_resetplot(history_plot);

  // plot a range box to make it not 'blink'
  gnuplot_setstyle(history_plot, "dots");
  gnuplot_plot_x(history_plot, m_plot_state->history_ranges, 2, "-");
  gnuplot_setstyle(history_plot, "lines");

  PlotThisHist(hist::speed);
  PlotThisHist(hist::bumpiness);
  //  PlotThisHist(hist::worst_bumpiness);
  PlotThisHist(hist::decaying_bumpiness);
  PlotThisHist(hist::speed_cap);
  //  PlotThisHist(hist::worst_speed_cap);
  PlotThisHist(hist::decaying_speed_cap);
  PlotThisHist(hist::pitch_1_20);
  //  PlotThisHist(hist::pitch_5_8);
  PlotThisHist(hist::zdot_1_20);
  //  PlotThisHist(hist::zdot_10_20);
}



void DBS::UpdateConclusions()
{
  m_conc.bumpiness = GetBumpiness();
  
  /** worst_bumpiness */
  double worst_bumpiness = m_conc.bumpiness; // worst is highest
  double last_n_seconds = 10;
  int first_i = (int)(-1.0 * last_n_seconds*FREQUENCY + (double)HISTORY_LENGTH - 1);
  if (first_i < 0)
    {
      first_i = 0;
      cerr << "history isn't long enough for that!!";
    }
  for (int i = HISTORY_LENGTH-1; i >= first_i; i--)
    worst_bumpiness = fmax(worst_bumpiness, m_plot_state->history_data[HIST_INDEX(hist::bumpiness,i)]);
  m_conc.worst_bumpiness = worst_bumpiness;
  
  /** decaying_bumpiness */
  double decaying_bumpiness = m_conc.bumpiness; // worst is highest
  last_n_seconds = 10;
  double decay_time_constant = 80; //seconds
  first_i = (int)(-1.0 * last_n_seconds*FREQUENCY + (double)HISTORY_LENGTH - 1);
  if (first_i < 0)
    {
      first_i = 0;
      cerr << "history isn't long enough for that!!";
    }
  for (int i = HISTORY_LENGTH-1; i >= first_i; i--)
    {
      double this_decay_multiplier = exp(-1.0 * (double)(HISTORY_LENGTH - i) / 
					 (double)FREQUENCY / decay_time_constant);
      decaying_bumpiness = fmax(decaying_bumpiness, this_decay_multiplier
				* m_plot_state->history_data[HIST_INDEX(hist::bumpiness,i)]);
      //      printf("(i,mult,arg) = (%d, %f, %f)\n", i, this_decay_multiplier, -1.0 * (double)(HISTORY_LENGTH - i) / (double)FREQUENCY / decay_time_constant);
    }
  m_conc.decaying_bumpiness = decaying_bumpiness;

  m_conc.speed_cap = fmax(20.0 * (1 - m_conc.bumpiness),  2.0);
  m_conc.worst_speed_cap = fmax(20.0 * (1 - m_conc.worst_bumpiness),  2.0);
  m_conc.decaying_speed_cap = fmax(20.0 * (1 - m_conc.decaying_bumpiness),  2.0);
  //  printf("(b, wb) = (%f, %f)\n", m_conc.bumpiness, m_conc.worst_bumpiness);
  //  cout << "speed cap being set to " << m_conc.speed_cap << endl;
}



void DBS::PlotAstateField(int field)
{
  double* buffer = new double[DBS_NUM_VALUES];
  for (int i = 0; i < DBS_NUM_VALUES; i++)
    buffer[i] = m_dbs_state->astate_buffer[i].*(vs_table[field]) * state_multiplier_map[field];
  string title_str = state_field_map[field];
  gnuplot_plot_x(current_info_plot, buffer, DBS_NUM_VALUES, (char*)title_str.c_str());
  delete [] buffer;
}



void DBS::PlotFFT(int field)
{
  double* buffer = new double[DBS_NUM_VALUES/2];
  for (int i = 0; i < DBS_NUM_VALUES/2; i++)
    buffer[i] = m_dbs_state->astate_fft[field][i] * state_fft_multiplier_map[field];
  string title_str = state_field_fft_map[field];
  buffer[0] = 0;  // don't plot the average
  gnuplot_plot_x(current_info_plot, buffer, DBS_NUM_VALUES/2, (char*)title_str.c_str());
  delete [] buffer;
}



void DBS::PlotThisHist(int hist_field)
{
  double* buffer = new double[HISTORY_LENGTH];
  for (int i = 0; i < HISTORY_LENGTH; i++)
    buffer[i] = hist_multiplier_map[hist_field] * m_plot_state->history_data[HIST_INDEX(hist_field, i)];
  gnuplot_plot_x(history_plot, buffer, HISTORY_LENGTH, (char*)hist_field_map[hist_field].c_str());
}



void DBS::PushArea(int hist_field, int area)
{
  for (int i = 0; i < HISTORY_LENGTH - 1; i++)
    m_plot_state->history_data[HIST_INDEX(hist_field, i)] = 
      m_plot_state->history_data[HIST_INDEX(hist_field, i+1)];
  m_plot_state->history_data[HIST_INDEX(hist_field, HISTORY_LENGTH - 1)] = m_dbs_state->fft_area[area].area;
}



void DBS::PushSpeed(int hist_field)
{
  for (int i = 0; i < HISTORY_LENGTH - 1; i++)
    m_plot_state->history_data[HIST_INDEX(hist_field, i)] = 
      m_plot_state->history_data[HIST_INDEX(hist_field, i+1)];
  m_plot_state->history_data[HIST_INDEX(hist_field, HISTORY_LENGTH - 1)] = 
    hypot(m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Vel_E, m_dbs_state->astate_buffer[DBS_NUM_VALUES-1].Vel_N);
}



void DBS::PushDouble(int hist_field, double value)
{
  for (int i = 0; i < HISTORY_LENGTH - 1; i++)
    m_plot_state->history_data[HIST_INDEX(hist_field, i)] = 
      m_plot_state->history_data[HIST_INDEX(hist_field, i+1)];
  m_plot_state->history_data[HIST_INDEX(hist_field, HISTORY_LENGTH - 1)] = value;
}



void DBS::PrintValue(fftw_complex* fft) 
{ int n;  
 for(n=0;n<DBS_NUM_VALUES;n++){
      cout << "fft[n][0]* = " << fft[n][0] << endl;
    }    
}



void DBS::PrintDebug(fftw_complex* in, fftw_complex* out)
{
  cout << setw(3) << "number" 
       << setw(13) << "in(real)" 
       << setw(13) << "in(imag)" 
       << setw(13) << "out(real)"
       << setw(13) << "out(imag)"
       << endl;
  for (int i = 0; i < DBS_NUM_VALUES; i++)
    {
      cout << setw(3) << i
	   << setw(13) << in[i][0]
	   << setw(13) << in[i][1]
	   << setw(13) << out[i][0]
	   << setw(13) << out[i][1]
	   << endl;
    }
}



void DBS::PrintValueSum(fftw_complex* fftA)  
{
  int n;
  double sumfft = 0;
  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      double fft =fftA[n][0];
      sumfft =sumfft +fft;
    }
  cout<<"sumfft"<<sumfft<<endl; 
}

double DBS::GetBumpiness()
{
  double bumpiness;
  // the old way
  bumpiness = .07*sqrt(m_dbs_state->fft_area[areas::zdot_1_20].area);

  // the new way
  bumpiness = atan(m_dbs_state->fft_area[areas::zdot_1_20].area / 10) * .8;

  if(bumpiness < 0)
    {
      bumpiness = 0;
    }
  else if(bumpiness > 1)
    {
      bumpiness = 1;
    }
  return bumpiness;
}



/*
int DBS::GetRoadType()
{
  double pitch[DBS_NUM_VALUES];
  double roll[DBS_NUM_VALUES];
  double zdot[DBS_NUM_VALUES];
  roadtype=-1;
  pitcharea=0;
  pitchhighestValue=0;
  // make a new chunk of memory to use as a buffer for the last
  // N values
  VehicleState* buffer = new VehicleState[DBS_NUM_VALUES];  

  // get last DBS_NUM_VALUES state values and store them in our buffer,
  // using GetLastNValues(DBS_NUM_VALUES,destination
  GetLastNValues(DBS_NUM_VALUES,buffer);

  // do this:
  fftw_complex *in;
  fftw_complex *out;
  fftw_plan fft;
  in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * DBS_NUM_VALUES);
  
  // copy the pitch from our buffer to "in"
  int n;
  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      in[n][0] = buffer[n].Pitch;
      in[n][1] = 0;
    }
  
  // do the fft
  fft = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(fft);

  // do something with the fft results
  PrintDebug(in, out);
  PrintValueSum(out);
  
  for( int n=1;n<DBS_NUM_VALUES;n++)
    { 
      pitch[n] = sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]);
    }
  pitch[0]=0;
  for( int n=0;n<DBS_NUM_VALUES;n++)
    {
      pitcharea += pitch[n];
      //cout << "n: " << n << " abs: " << sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]) << " real: " << out[n][0] << "complex: " << out[n][1] << endl;
      if (pitch[n] > pitchhighestValue)
	{
	  pitchhighestValue = pitch[n];
	}
    }
  pitcharea3= 0;
  for( int n=0;n<3;n++)
    {
      pitcharea3 += pitch[n]; 
    }
  pitch10=0;
  for(int n=10;n<DBS_NUM_VALUES/2;n++)
    {
      pitch10 += pitch[n];
    }
  
for(n=0;n<DBS_NUM_VALUES;n++)
    {
      in[n][0] = buffer[n].Roll;
    }
  
  // do the fft
  fft = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(fft);

  for( int n=0;n<DBS_NUM_VALUES;n++)
    { 
      roll[n] = sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]);
    }

  // do something with the fft results
  //PrintDebug(in, out);
  //PrintValueSum(out);
  rollarea=0;
  for( int n=0;n<DBS_NUM_VALUES;n++)
    {
      rollarea += roll[n];
      if (roll[n] > rollhighestValue)
	{
	  rollhighestValue = roll[n];
	}
    }
  rollarea5to8= 0;
  for( int n=4;n<7;n++)
    {
      rollarea5to8 += roll[n]; 
    }

  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      in[n][0] = buffer[n].Vel_D;
    }
  
  // do the fft
  fft = fftw_plan_dft_1d(DBS_NUM_VALUES, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(fft);

  for( int n=0;n<DBS_NUM_VALUES;n++)
    { 
      zdot[n] = sqrt(out[n][0]*out[n][0]+ out[n][1]*out[n][1]);
    }

  Zdotarea=0;

  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      Zdotarea += zdot[n];
    }

  for(n=0;n<DBS_NUM_VALUES;n++)
    {
      meanspeed += sqrt(buffer[n].Vel_N*buffer[n].Vel_N + buffer[n].Vel_E*buffer[n].Vel_E);
    }
  meanspeed = meanspeed/DBS_NUM_VALUES;
  cout << "pitcharea is: " << pitcharea << " pitchhigestValue is: " << pitchhighestValue << endl;
  if(pitcharea<.5) //fix me
    {
      roadtype = 0;
    }
else  if(pitchhighestValue >1)
    {
      roadtype=3;
    }
  else 
    {
      roadtype = -1;
  }

// at end, free the memory
  fftw_destroy_plan(fft);
  fftw_free(in); fftw_free(out);
  delete [] buffer;
  return roadtype; 
}  
*/
