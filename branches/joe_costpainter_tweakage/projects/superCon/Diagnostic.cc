#include"Diagnostic.hh"

//Include all strategies
#include"Strategy.hh"
#include"StrategyNominal.hh"
#include"StrategyMoving.hh"

#include<sys/time.h>
#include<iostream>
using namespace std;

CDiagnostic::CDiagnostic(int sn_key)
  : CSkynetContainer(MODsupercon, sn_key), CTimberClient(timber_types::superCon), CSuperConClient("SC ")
{
  //Reset all strategy pointers
  for(int i=0; i<(int)StrategyLast; ++i) {
    m_strategies[i].name = "";
    m_strategies[i].pStrategy = 0;
  }

  DGCcreateCondition(&m_state_event);
  DGCcreateMutex(&m_state_mutex);
}

CDiagnostic::~CDiagnostic()
{
  //Delete all registered strategies
  for(int i=0; i<(int)StrategyLast; ++i) {
    if(m_strategies[i].pStrategy)
      delete m_strategies[i].pStrategy;
    m_strategies[i].pStrategy = 0;
  }

  DGCdeleteCondition(&m_state_event);
  DGCdeleteMutex(&m_state_mutex);
}

//////////////////////////////////////////////////////////////////////////////////
//Hands over execution to diagnostics class... assumes the state server is running
void CDiagnostic::run()
{
  init();    //Initialize all strategies

  //Enter nominal state!
  m_current_strategy = StrategyNominal;
  if(m_strategies[m_current_strategy].pStrategy)
    m_strategies[m_current_strategy].pStrategy->enter(m_diagnostics);

  cerr<<"CDiagnostic::run - entering main message loop"<<endl;

  //Variables used to keep track of supercon processing frequency
  int freq_cnt=0;
  unsigned long long freq_time;
  DGCgettime(freq_time);

  //Enter message pump
  pthread_mutex_lock(&m_state_mutex);    //Normaly we own the state
  while(1) {
    //Demand new state set
    m_state_new=false;

    while(!m_state_new) {
      //Send message demanding new data
      scMessage(0, &m_state, &m_state_new, &m_state_mutex, &m_state_event);

      //cerr<<"CDiagnostic::run - sent request for new state data"<<endl;

      //wait for state to be updated, then relock the mutex
      timespec ts;
      timeval tv;
      gettimeofday(&tv, NULL);
      ts.tv_sec = tv.tv_sec+1;   //Wait for state for 1s then make new request if new state was received
      ts.tv_nsec = tv.tv_usec*1000;

      pthread_cond_timedwait(&m_state_event, &m_state_mutex, &ts);
      
      if(!m_state_new) {
	cerr<<"CDiagnostic::run - Error receiving new state data, re-requiring state from CStateServer"<<endl;
      }
    }

    //Used to limit the frequency of supercon
    unsigned long long start;
    DGCgettime(start);

    //cerr<<"CDiagnostic::run - new state data received, calculate diagnostics"<<endl;

    //New state received, start processing
    updateDiagnostics(m_state, m_diagnostics);

    if(m_strategies[m_current_strategy].pStrategy) {
      m_strategies[m_current_strategy].pStrategy->step(m_diagnostics);
    } else {
      cerr<<"Diagnostics modul currently in incorrect strategy: "<<m_current_strategy<<" transsision to nominal"<<endl;
      transsisionStrategy(StrategyNominal, "Transsision from invalid strategy to nominal");
    }

    //Calculate running frequency and cap the running frequency
    ++freq_cnt;

    unsigned long long end;
    DGCgettime(end);

    //Calculate running frequency of supercon
    if((end-freq_time) > 1e6) {   //Recalc frequency every second
      cout<<"Supercon diagnostics running at: "<<freq_cnt/(double)((end-freq_time)/1e6)<<" Hz"<<endl;

      freq_time = end;
      freq_cnt = 0;
    }
    
    if((end-start)< 1e6/SUPERCON_FREQ)
      usleep( static_cast<unsigned int>(1e6/SUPERCON_FREQ - (end-start)) );
  }

  pthread_mutex_unlock(&m_state_mutex);
}

//////////////////////////////////////////////////////////////////////////////////
//   Internal functions related to strategies and rules
//Initialize all strategies etc.
void CDiagnostic::init()
{
  registerStrategy(StrategyNominal, "Nominal", new CStrategyNominal());
  registerStrategy(StrategyMoving, "Moving", new CStrategyMoving());
}

//Update all diagnostic rules from the supplied state structure
void CDiagnostic::updateDiagnostics(const SCState &state, SCDiagnostic &diag)
{
  //INSERT RULES FOR state TO diagnostics, remember to set
  //values both to true and to false, no default values exist,
  //none altered variables retain last value

  diag.moving = (state.avg_vel > 1);
  diag.cnt = (int)state.cnt;
}

//////////////////////////////////////////////////////////////////////////////////
//Interface functions (for CStrategy etc.)
void CDiagnostic::transsisionStrategy(SCStrategy to, const char *reason)
{
  //Transfer to the new strategy

  //Signal leaving the old strategy
  if(m_strategies[m_current_strategy].pStrategy)
    m_strategies[m_current_strategy].pStrategy->leave(m_diagnostics);

  m_current_strategy = to;

  //Signal enter new strategy
  if(m_strategies[m_current_strategy].pStrategy)
    m_strategies[m_current_strategy].pStrategy->enter(m_diagnostics);
}

void CDiagnostic::setState(double SCState::*pState, double val)
{
  //Send supercon message to set variable
  scMessage(1, pState, val);
}

string CDiagnostic::strategyName(SCStrategy id) const
{
  if((int)id<0 || (int)id>=StrategyLast)
    return string("Unknown strategy");

  if(!m_strategies[id].pStrategy)
    return m_strategies[id].name + "(not regitered!!!)";

  return m_strategies[id].name;
}

//////////////////////////////////////////////////////////////////////////////////
//   Internal helper functions
void CDiagnostic::registerStrategy(SCStrategy id, const char *name, CStrategy *pStrategy)
{
  //Initialize the strategy pointers
  pStrategy->m_ID = id;
  pStrategy->m_pDiag = this;

  //Add to list
  m_strategies[id].name = name;
  m_strategies[id].pStrategy = pStrategy;
}
