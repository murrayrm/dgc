#ifndef DIAGNOSTICS_HH
#define DIAGNOSTICS_HH

#include"SuperCon.hh"
#include"SuperConClient.hh"

#include"CTimberClient.hh"
#include"SkynetContainer.h"

#include<pthread.h>
#include<fstream>
#include<vector>
using namespace std;

class CStrategy;        //Forward declaration

class CDiagnostic : virtual public CSkynetContainer, public CTimberClient, public CSuperConClient
{
public:
  CDiagnostic(int sn_key);
  ~CDiagnostic();

  void run();        //Hands over execution to diagnostics class... assumes the state server is running

protected:           //Internal functions related to strategies and rules
  void init();       //Initialize all strategies etc.
  void updateDiagnostics(const SCState &state, SCDiagnostic &diag);

public:   //Interface functions (for CStrategy etc.)
  void transsisionStrategy(SCStrategy to, const char *reason);
  void setState(double SCState::*pState, double val);
  string strategyName(SCStrategy id) const;

protected:           //Internal helper functions
  void registerStrategy(SCStrategy id, const char *name, CStrategy *pStrategy);

private:
  bool m_state_new;
  pthread_mutex_t m_state_mutex;        //Mutex locking the m_state struct
  pthread_cond_t m_state_event;
  SCState m_state;
  SCDiagnostic m_diagnostics;

  SCStrategy m_current_strategy;        //Currently active strategy

  //List of all registered strategies
  struct Strategies
  {
    string name;
    CStrategy *pStrategy;
  };
  Strategies m_strategies[StrategyLast];
};



#endif
