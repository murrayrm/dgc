#ifndef SUPERCONHELPERS_HH
#define SUPERCONHELPERS_HH

#include<iostream>
using namespace std;

//Define helper functions for supercon

//Defines used to create state and diagnosis structs
//These are used to ensure initialization is done correctly
//and so that print functions and gui associations are done
//automaticaly

#define STATE_ITEM(item, default, desc) \
  double item;

#define STATE_RESET(item, default, desc) \
  item = default;

#define STATE_PRINT(item, default, desc) \
   cout<<#item " = "<<item<<" ("<<desc<<")"<<endl;

#define DEFINE_STATES(list) \
   struct SCState { \
     SCState() { reset(); } \
                            \
     list(STATE_ITEM)       \
                            \
     void reset() {         \
       list(STATE_RESET)    \
     }                      \
     void print() {         \
       list(STATE_PRINT)    \
     }                      \
   };



#endif
