#include"Strategy.hh"

class CStrategyNominal : public CStrategy
{
public:
  CStrategyNominal() : CStrategy() {}
  virtual ~CStrategyNominal() {}

  //Step one step forward using the suppled diagnostic rules
  virtual void step(const SCDiagnostic &diag);
  //We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
  virtual void leave(const SCDiagnostic &diag);
  //We are entering this state, do initialization (NO NEW STATE TRANSFERS)
  virtual void enter(const SCDiagnostic &diag);
};
