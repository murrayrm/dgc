SUPERCON_PATH = $(DGC)/projects/superCon

SUPERCON_DEPEND_LIBS = $(SPARROWLIB) \
                           $(SKYNETLIB) \
                           $(MODULEHELPERSLIB) \
                           $(TIMBERLIB)

SUPERCON_DEPEND_SOURCE = \
                           $(SUPERCON_PATH)/SuperCon.hh \
                           $(SUPERCON_PATH)/SuperConMain.cc \
                           $(SUPERCON_PATH)/StateServer.cc \
                           $(SUPERCON_PATH)/StateServer.hh \
                           $(SUPERCON_PATH)/Diagnostic.cc \
                           $(SUPERCON_PATH)/Diagnostic.hh \
                           $(SUPERCON_PATH)/Strategy.cc \
                           $(SUPERCON_PATH)/Strategy.hh \
                           $(SUPERCON_PATH)/SuperConClient.hh \
                           $(SUPERCON_PATH)/SuperConClient.cc \
                           $(SUPERCON_PATH)/SuperConInterface.cc \
                           $(SUPERCON_PATH)/SuperConInterface.hh

SUPERCON_DEPEND = $(SUPERCON_DEPEND_LIBS) \
                      $(SUPERCON_DEPEND_SOURCE)
