#include"Strategy.hh"

class CStrategyMoving : public CStrategy
{
public:
  CStrategyMoving() : CStrategy() {}
  virtual ~CStrategyMoving() {}

  //Step one step forward using the suppled diagnostic rules
  virtual void step(const SCDiagnostic &diag);
  //We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
  virtual void leave(const SCDiagnostic &diag);
  //We are entering this state, do initialization (NO NEW STATE TRANSFERS)
  virtual void enter(const SCDiagnostic &diag);
};
