#ifndef SUPERCON_HH
#define SUPERCON_HH

#include"SuperConHelpers.hh"

//Contains global defines of state structure and diagnostic variables

#define SUPERCON_FREQ 10     //Desired running frequency of supercon

//Define states, the 3 parameters are var_name, default_value, description
#define STATE_LIST(_) \
/* States */ \
  /* astate */ \
  _(northing, 0.0, "") \
  _(easting, 0.0, "") \
  _(n_vel, 0.0, "") \
  _(e_vel, 0.0, "") \
  /* supercon */ \
  _(copy_timestamp, 0.0, "Timestamp of the copy to diagnostics") \
/* Meta states */ \
  _(avg_vel, 0.0, "") \
/* Diagnostics feedback */ \
  _(cnt, 0.0, "Counts number of consecuative times in nominal")

DEFINE_STATES(STATE_LIST);

struct SCDiagnostic
{
  bool moving;

  int cnt;          //I know... just bools.... just testing
};

//Strategies should be numbered in increasing order (they'll be indexed by this number in an array)
enum SCStrategy
  {
    StrategyNone=0,         //"Null" value, should never be used (except internaly)
    StrategyNominal,
    StrategyMoving,
    StrategyLast
  };


#endif   //SUPERCON_HH
