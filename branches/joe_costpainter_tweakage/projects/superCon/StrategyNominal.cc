#include"StrategyNominal.hh"

#include<iostream>
using namespace std;

#include "StrategyHelpers.hh"

//Step one step forward using the suppled diagnostic rules
void CStrategyNominal::step(const SCDiagnostic &diag)
{
  //cout<<"Nominal state stepping "<<endl;
  if(diag.moving)
    transsisionStrategy(StrategyMoving, "Started moving");
  if(diag.cnt==10)
    transsisionStrategy(StrategyMoving, "I want to move!");

  //Update the counter
  setState(cnt, diag.cnt+1);
}

//We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
void CStrategyNominal::leave(const SCDiagnostic &diag)
{
  cout<<"Nominal state leaving - cnt = "<<diag.cnt<<endl;
}

//We are entering this state, do initialization (NO NEW STATE TRANSFERS)
void CStrategyNominal::enter(const SCDiagnostic &diag)
{
  cout<<"Nominal state entering"<<endl;
  setState(cnt, 0);
}

