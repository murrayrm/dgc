//NOTE: due to the defines, this file might interfer w CDiagnostic and
//should therfor be included last of all include files and only in Strategy files!!!

#ifndef STRATEGY_HELPERS_HH
#define STRATEGY_HELPERS_HH


///////////////////////////////////////////////////////////////////////////////
//Helpers implemented through defines to get extra info on compile time
//These helpers must be called from within the derived CStrategy class

#define transsisionStrategy(toState, reason) \
              { static CStrategyMapper localMapper(getID(), toState, reason); \
                doTranssisionStrategy(toState, reason); }

//Set the state, this effect will not be propagated until the next state update takes place
#define setState(state, val) { doSetState(&SCState::state, val); }

#endif
