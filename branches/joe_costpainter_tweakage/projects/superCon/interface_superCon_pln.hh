//FILE DESCRIPTION: Header file that contains the definitions of data structures that are
//used in direct communication between SuperCon and the plannerModule (including the
//traj-selection/modification components of the plannerModule)

/*** PLANNER MODULE --> SUPERCON ***/

//PLN --> SC command type enum - used as the throwIDs in scThrows() & scCThrows()
//sent TO superConFROM plannerModule
enum PLNtoSCcmdType { VALID_TRAJ, TERRAIN_OBSTACLE, SUPERCON_OBSTACLE };



/*** SUPERCON --> PLANNER MODULE ***/

//SC --> PLN command type enum - used to declare the type of command being sent TO plannerModule
//FROM SuperCon using the superConPlnCmd struct & SNsuperconPlnCmd message type
enum SCtoPLNcmdType { ADDSPEEDCAP, REMOVESPEEDCAP };

//Description: struct sent by SuperCon in the SNsuperconPlnCmd
//skynet message type, the message contains commands for the traj-selection/modification
//component of the plannerModule.
struct superConPlnCmd
{
  //integer to denote the command type
  SCtoPLNcmdType commandType;
  //numeric argument passed with the command type
  double argument;
};
