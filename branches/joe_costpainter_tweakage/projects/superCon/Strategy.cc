#include"Strategy.hh"
#include"Diagnostic.hh"

#include<iostream>
using namespace std;

///////////////////////////////////////////////////////////////////////////////
//Class used to mapp state transfers at compile time
CStrategyMapper *CStrategyMapper::pInstance;

CStrategyMapper::CStrategyMapper()
{
}

CStrategyMapper::CStrategyMapper(SCStrategy from, SCStrategy to, const char *reason)
{
  if(!pInstance)
    pInstance = new CStrategyMapper();

  Transsision trans;
  trans.to = to;
  trans.from = from;
  trans.reason = reason;

  pInstance->m_transsision.push_back(trans);
}


//Diagnostic used for name lookup
void CStrategyMapper::print(const CDiagnostic &diag)
{
  //Print all transsisions
  if(!pInstance) {
    cout<<"No singleton instance of CStrategyMapper, are any transsisions defined?"<<endl;
    return;
  }

  cout<<"All registered transsisions"<<endl;

  for(vector<Transsision>::iterator itr = pInstance->m_transsision.begin(); itr!= pInstance->m_transsision.end(); ++itr) {
    cout<<diag.strategyName(itr->from)<<"("<<itr->from<<") -> "<<diag.strategyName(itr->to)<<"("<<itr->to<<") - "<<itr->reason<<endl;
  }
}

//////////////////////////////////////////////////////////////////////////////
//Class declares virtual base class for strategies
CStrategy::CStrategy()
  : m_ID(StrategyNone), m_pDiag(0)
{
}

CStrategy::~CStrategy()
{
  m_pDiag = 0;
}

void CStrategy::doTranssisionStrategy(SCStrategy to, const char *reason)
{
  if(m_pDiag)
    m_pDiag->transsisionStrategy(to, reason);
  else
    cerr<<"Pointer to diagnostics class not instansiated"<<endl;
}

void CStrategy::doSetState(double SCState::*pState, double val)
{
  if(m_pDiag)
    m_pDiag->setState(pState, val);
  else
    cerr<<"Pointer to diagnostics class not instansiated"<<endl;
}
