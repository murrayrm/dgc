//FILE DESCRIPTION: Header file that contains the definitions of data structures that are
//used in direct communication between SuperCon and the trajFollower
//
//NOTE: The struct and enums that define/enable the setting/resetting of speed-caps
//in trajFollower are defined in: trajF_speedCap_cmd.hh (note that they do NOT
//use the same message type)  

#include "trajF_status_struct.hh" //contains enum definition of trajFmode type 

/* Use the scMessage method */
//Used for trajFollower --> superCon (conditional) messages *using the scMessage method/message
//ONLY* - these enums should be used in the msgID field of the scMessage, ALSO
//the msgID field only takes an integer - HENCE you should RECAST one of the
//enums (type: scMsgID_TJF) - to an integer (you should recast whichever member
//of the enum details what you want to do - superCon will then perform the inverse
//recast statement so that the code on either side is cleaner - there are reasons
//why this has to be done)
enum scMsgID_TJF { COMPLETED_REVERSING_ACTION };


/* Use the standard skynet message type: SNsuperconTrajfCmd */
//struct to be sent to house commands sent from superCon --> trajFollower
struct superConTrajFcmd
{
  //command type being sent
  trajFmode commandType;
  //distance to reverse in meters [only used if commandType = TF_REVERSE]
  double distanceToReverse;
};
