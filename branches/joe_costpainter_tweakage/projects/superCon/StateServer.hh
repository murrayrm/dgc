#ifndef SCSTATESERVER_HH
#define SCSTATESERVER_HH

#include"SuperCon.hh"

#include"CTimberClient.hh"
#include"SkynetContainer.h"

#include"cbuffer.hh"

#include<pthread.h>
#include<fstream>
#include<vector>
using namespace std;

class CStateServer : virtual public CSkynetContainer, public CTimberClient
{
private:    //Internal structures
  typedef void (CStateServer::*MetaDataUpdateFunc)();
  struct MessageType
  {
    sn_msg  type;
    vector<MetaDataUpdateFunc>   metadata_links;
  };
  friend bool operator==(const CStateServer::MessageType &msg, sn_msg type);

  struct SkynetMsg
  {
    int priority;
    sn_msg type;
    void *pData;
  };
  friend bool operator<(const CStateServer::SkynetMsg &msg1, const CStateServer::SkynetMsg &msg2);
public:
  CStateServer(int sn_key);
  ~CStateServer();

  bool activate();                     //Initialize all members and start all threads, returns once message pump has started

private:     //Private data
  vector<MessageType> m_msg_types;      //All the different message types to be handled

  pthread_mutex_t m_msg_mutex;      //The mutex to lock the message heap
  pthread_cond_t m_msg_event;       //Trigger for when a msg is added to the heap
  vector<SkynetMsg> m_msg_heap;     //Heap of waiting messages, sorted on priority

private:     //Internal helper/access functions
  void registerSkynetMessage(sn_msg type, void (CStateServer::*threadProc)(), vector<MetaDataUpdateFunc> metadata = vector<MetaDataUpdateFunc>());
  void registerSkynetMessage(sn_msg type, void (CStateServer::*threadProc)(), MetaDataUpdateFunc fc1) { 
    vector<MetaDataUpdateFunc> metadata; metadata.push_back(fc1); registerSkynetMessage(type, threadProc, metadata);
  }
  void registerSkynetMessage(sn_msg type, void (CStateServer::*threadProc)(), MetaDataUpdateFunc fc1, MetaDataUpdateFunc fc2) { 
    vector<MetaDataUpdateFunc> metadata; metadata.push_back(fc1); metadata.push_back(fc2); registerSkynetMessage(type, threadProc, metadata);
  }
  void registerSkynetMessage(sn_msg type, void (CStateServer::*threadProc)(), MetaDataUpdateFunc fc1, MetaDataUpdateFunc fc2, MetaDataUpdateFunc fc3) { 
    vector<MetaDataUpdateFunc> metadata; metadata.push_back(fc1); metadata.push_back(fc2); metadata.push_back(fc3); registerSkynetMessage(type, threadProc, metadata);
  }
  void registerSkynetMessage(sn_msg type, void (CStateServer::*threadProc)(), MetaDataUpdateFunc fc1, MetaDataUpdateFunc fc2, MetaDataUpdateFunc fc3, MetaDataUpdateFunc fc4) { 
    vector<MetaDataUpdateFunc> metadata; metadata.push_back(fc1); metadata.push_back(fc2); metadata.push_back(fc3); metadata.push_back(fc4); registerSkynetMessage(type, threadProc, metadata);
  }
  void registerSkynetMessage(sn_msg type, void (CStateServer::*threadProc)(), MetaDataUpdateFunc fc1, MetaDataUpdateFunc fc2, MetaDataUpdateFunc fc3, MetaDataUpdateFunc fc4, MetaDataUpdateFunc fc5) { 
    vector<MetaDataUpdateFunc> metadata; metadata.push_back(fc1); metadata.push_back(fc2); metadata.push_back(fc3); metadata.push_back(fc4); metadata.push_back(fc5); registerSkynetMessage(type, threadProc, metadata);
  }
  void registerSkynetMessage(sn_msg type, void (CStateServer::*threadProc)(), MetaDataUpdateFunc fc1, MetaDataUpdateFunc fc2, MetaDataUpdateFunc fc3, MetaDataUpdateFunc fc4, MetaDataUpdateFunc fc5, MetaDataUpdateFunc fc6) { 
    vector<MetaDataUpdateFunc> metadata; metadata.push_back(fc1); metadata.push_back(fc2); metadata.push_back(fc3); metadata.push_back(fc4); metadata.push_back(fc5); metadata.push_back(fc6); registerSkynetMessage(type, threadProc, metadata);
  }

  void pushMsg(SkynetMsg &msg);    //Thread safe push onto the heap
  void pushMsg(int priority, sn_msg type, void *pData);   //Thread safe push onto the heap
  SkynetMsg popMsg();              //Thread safe pop from the heap

private:  //Placeholders for state variables and meta-state data
  //Struct to hold metadatacalculations
  struct SCMetaStateData
  {
    cbuffer<double, 20> vels;

  };
  SCState m_state;
  SCMetaStateData m_statedata;

  void messagePump();              //Main message pump, takes a message from the heap and enters into state

private:     //Handlers for all different skynet messages we listen to,
             //this handler is generic and just listens and adds the message to the heap
  void sn_astate();
  void sn_supercon();

private:     //Metastate update functions
  void ms_astate();

};

//Operators needed for working with contained structures
bool operator==(const CStateServer::MessageType &msg, sn_msg type);
bool operator<(const CStateServer::SkynetMsg &msg1, const CStateServer::SkynetMsg &msg2);

#endif
