#include "StateServer.hh"

#include"DGCutils"

#include"VehicleState.hh"
#include"SuperConInterface.hh"

#include<math.h>
#include<algorithm>
using namespace std;

CStateServer::CStateServer(int sn_key)
  : CSkynetContainer(MODsupercon, sn_key), CTimberClient(timber_types::superCon)
{
  //Create mutex used to control access to heap
  DGCcreateMutex(&m_msg_mutex);
  DGCcreateCondition(&m_msg_event);
}

CStateServer::~CStateServer()
{
  DGCdeleteMutex(&m_msg_mutex);
  DGCdeleteCondition(&m_msg_event);
}

bool CStateServer::activate()
{
  //Start messagePump
  DGCstartMemberFunctionThread(this, &CStateServer::messagePump);

//Register all skynet messages with their associated meta-state update functions
  registerSkynetMessage(SNstate, &CStateServer::sn_astate, &CStateServer::ms_astate);
  registerSkynetMessage(SNsuperconMessage, &CStateServer::sn_supercon);

//Make sure all threads have time to go into their active loops
  usleep(10000);

  return true;
}

void CStateServer::registerSkynetMessage(sn_msg type, void (CStateServer::*threadProc)(), vector<MetaDataUpdateFunc> metadata)
{
  MessageType msg;

  msg.type = type;
  msg.metadata_links = metadata;
  m_msg_types.push_back(msg);

  DGCstartMemberFunctionThread(this, threadProc);
}


void CStateServer::pushMsg(SkynetMsg &msg)
{
  DGClockMutex(&m_msg_mutex);

  m_msg_heap.push_back(msg);
  push_heap(m_msg_heap.begin(), m_msg_heap.end());

  pthread_cond_signal(&m_msg_event);         //Signal that the heap is not empty
  DGCunlockMutex(&m_msg_mutex);
}

//Thread safe push onto the heap
void CStateServer::pushMsg(int priority, sn_msg type, void *pData)
{
  SkynetMsg msg;
  msg.priority = priority;
  msg.type = type;
  msg.pData = pData;
  pushMsg(msg);
}


CStateServer::SkynetMsg CStateServer::popMsg()
{
  pthread_mutex_lock(&m_msg_mutex);
  while(m_msg_heap.empty())
    pthread_cond_wait(&m_msg_event, &m_msg_mutex);
  
  //We have exclusive access to the heap
  pop_heap(m_msg_heap.begin(), m_msg_heap.end());
  SkynetMsg msg = m_msg_heap.back();
  m_msg_heap.pop_back();

  pthread_mutex_unlock(&m_msg_mutex);

  return msg;
}

void CStateServer::messagePump()
{
  cerr<<"CStateServer::messagePump - Entered message pump"<<endl;

  while(1) {    //Don't allow graceful termination
    SkynetMsg msg = popMsg();

    //cerr<<"CStateServer::messagePump - Received message of type "<<msg.type<<endl;

    //Process the message to update state
    switch(msg.type) {   //Convert pData to correct type and deallocate when done
    case SNstate:
      {
	VehicleState *pState = (VehicleState*)msg.pData;

	m_state.northing = pState->Northing;
	m_state.easting = pState->Easting;
	m_state.n_vel = pState->Vel_N;
	m_state.e_vel = pState->Vel_E;

	//Free memory
	delete pState;
	msg.pData=0;
      }
      break;
    case SNsuperconMessage:
      {
	supercon_msg_cmd *pMsg = (supercon_msg_cmd*)msg.pData;
	
	if(strncmp(pMsg->module_id, "SC", 2)==0) {
	  //Received a supercon request
	  switch(pMsg->msg_id) {
	  case 0:    //Message type 0, copy state data to supplied pointer (this requires things running in same process)
	    {
	      //Update the state
	      unsigned long long ts;
	      DGCgettime(ts);
	      m_state.copy_timestamp = ts;

	      //Copy state
	      int npos=0;
	      SCState *pState;
	      pthread_cond_t *pEvent;
	      pthread_mutex_t *pMutex;
	      bool *pFlag;
	      sc_deserialize(pMsg, npos, pState);      //Get the supplied pointer
	      sc_deserialize(pMsg, npos, pFlag);
	      sc_deserialize(pMsg, npos, pMutex);
	      sc_deserialize(pMsg, npos, pEvent);      //Get the supplied event

	      DGClockMutex(pMutex);

	      memcpy(pState, &m_state, sizeof(SCState));  //Copy the current state data to the supplied pointer
	      *pFlag=true;
	      pthread_cond_signal(pEvent);                //Signal the event

	      DGCunlockMutex(pMutex);
	    }
	    break;
	  case 1:    //Update value of double valued state variable
	    {
	      int npos=0;
	      double SCState::*pVar;
	      double val;

	      sc_deserialize(pMsg, npos, pVar);
	      sc_deserialize(pMsg, npos, val);         //Value to set it too

	      m_state.*pVar = val;
	    }
	    break;
	  default:
	    cerr<<"Unknown supercon message type "<<pMsg->type<<" from module "<<pMsg->module_id[0]<<pMsg->module_id[1]<<pMsg->module_id[2]<<endl;
	  }
	} else {
	  cerr<<"Supercon message received from unknown module"<<pMsg->module_id[0]<<pMsg->module_id[1]<<pMsg->module_id[2]<<endl;
	}
	
	//Free memory
	delete pMsg;
	msg.pData=0;
      }
      break;
    default:
      break;
    }

    if(msg.pData) {
      //Memory not deallocated
      cerr<<"Memory not deallocated when handling message type:"<<msg.type<<endl;
    }

    //Update all necissery meta-states
    vector<MessageType>::iterator itr = find(m_msg_types.begin(), m_msg_types.end(), msg.type);
    if(itr!=m_msg_types.end()) {
      for(vector<MetaDataUpdateFunc>::iterator itr2 = itr->metadata_links.begin(); itr2!=itr->metadata_links.end(); ++itr2)
	(this->**itr2)();
    }
  }
}

void CStateServer::sn_astate()
{
  cerr<<"sn_astate started"<<endl;

  int socket = m_skynet.listen(SNstate, ALLMODULES);
  VehicleState *pState = new VehicleState;
  
  while(true) {   //Don't allow graceful termination
    if(m_skynet.get_msg(socket, pState, sizeof(VehicleState), 0) != sizeof(VehicleState)) {
      cerr<<"Incorrect number of bytes received in SNstate message"<<endl;
      continue;
    }
    //Send message
    pushMsg(0, SNstate, pState);

    //Allocate new buffer for next message
    pState = new VehicleState;
  }
}

void CStateServer::sn_supercon()
{
  cerr<<"sn_supercon started"<<endl;

  int socket = m_skynet.listen(SNsuperconMessage, ALLMODULES);
  supercon_msg_cmd *pMsg = new supercon_msg_cmd;

  while(true) {   //Don't allow graceful termination
    if(m_skynet.get_msg(socket, pMsg, sizeof(supercon_msg_cmd), 0) < (int)SUPERCON_MSG_CMD_CORE) {
      cerr<<"Incorrect number of bytes received in SNstate message"<<endl;
      continue;
    }
    if(pMsg->type != scMessage)    //If it was something else than a message, don't handle it
      continue;

    //cout<<"Received supercon message - id = "<<pMsg->msg_id<<"  size = "<<pMsg->user_bytes<<endl;

    //Send message
    int prior = 0;
    if(strncmp(pMsg->module_id, "SC", 2)==0) {
      //Received a supercon request
      if(pMsg->msg_id==0)   //Higher priority for request for state
	prior = 1;
      if(pMsg->msg_id==1)   //Make sure all state setttings requested by supercon (diagnostics - strategies) are propagated before a state update
	prior = 2;
    }

    pushMsg(prior, SNsuperconMessage, pMsg);

    //Allocate new buffer for next message
    pMsg = new supercon_msg_cmd;
  }
}

void CStateServer::ms_astate()
{
  //Update the running averages of northing and southing  
  double sum = m_state.avg_vel*m_statedata.vels.size();
  if(m_statedata.vels.full())
    sum -= m_statedata.vels.last();
  m_statedata.vels.add( sqrt(m_state.n_vel*m_state.n_vel + m_state.e_vel*m_state.e_vel));
  sum += m_statedata.vels.first();
  m_state.avg_vel = sum / m_statedata.vels.size();

  //  cerr<<"ms_astate called - set avg_vel = "<<m_state.avg_vel<<endl;
}

//////////////////////////////////////////////////////////////////////////////////////
//Operators needed for working with contained structures
bool operator==(const CStateServer::MessageType &msg, sn_msg type) {
  return msg.type==type;
}

bool operator<(const CStateServer::SkynetMsg &msg1, const CStateServer::SkynetMsg &msg2) {
  return msg1.priority<msg2.priority;
}
