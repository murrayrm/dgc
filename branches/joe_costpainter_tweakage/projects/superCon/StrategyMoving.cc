#include"StrategyMoving.hh"

#include<iostream>
using namespace std;

#include "StrategyHelpers.hh"

//Step one step forward using the suppled diagnostic rules
void CStrategyMoving::step(const SCDiagnostic &diag)
{
  //cout<<"Moving state stepping"<<endl;
  if(!diag.moving)
    transsisionStrategy(StrategyNominal, "Stopped moving");
}

//We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
void CStrategyMoving::leave(const SCDiagnostic &diag)
{
  cout<<"Moving state leaving"<<endl;
}

//We are entering this state, do initialization (NO NEW STATE TRANSFERS)
void CStrategyMoving::enter(const SCDiagnostic &diag)
{
  cout<<"Moving state entering"<<endl;
}

