#ifndef BOOL_COUNTER_HH
#define BOOL_COUNTER_HH

//Defines a class working as a boolean, but keeping count of state changes

class bool_counter
{
 public:
  bool_counter() : data(false), cnt(0) {}
  bool_counter(bool b) : data(b), cnt(0) {}
  bool_counter(const bool_counter &bc) : data(bc.data), cnt(bc.cnt) {}
  
  bool_counter &operator=(bool b) {
    data = b;
    if(data)
      ++cnt;
    else
      cnt=0;

    return *this;
  }

  bool_counter &operator=(const bool_counter &bc) {
    data = bc.data;
    cnt = bc.cnt;
    return *this;
  }


  void reset() { cnt=0; }

  int count() const { return cnt; }

  operator bool() { return data; }

 private:
  bool data;
  int cnt;
};



#endif //BOOL_COUNTER_HH
