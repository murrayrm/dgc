#ifndef STRATEGY_HH
#define STRATEGY_HH

#include "SuperCon.hh"
#include<string>
#include<vector>
using namespace std;

class CDiagnostic;    //Forward declaration

///////////////////////////////////////////////////////////////////////////////
//Class used to mapp state transfers at compile time
class CStrategyMapper
{
private:
  CStrategyMapper();   //Singleton class... use private default constructor
public:
  CStrategyMapper(SCStrategy from, SCStrategy to, const char *reason);

  static void print(const CDiagnostic &diag);   //Diagnostic used for name lookup

private:
  static CStrategyMapper *pInstance;

  struct Transsision {
    SCStrategy to;
    SCStrategy from;
    string reason;
  };
  vector<Transsision> m_transsision;
};

///////////////////////////////////////////////////////////////////////////////
//Class declares virtual base class for strategies
class CStrategy
{
public:
  CStrategy();
  virtual ~CStrategy();

public:    //Control loop for strategies
  //Step one step forward using the suppled diagnostic rules
  virtual void step(const SCDiagnostic &diag) =0;
  //We are leaving this state, do clean up (NO NEW STATE TRANSFERS)
  virtual void leave(const SCDiagnostic &diag) =0;
  //We are entering this state, do initialization (NO NEW STATE TRANSFERS)
  virtual void enter(const SCDiagnostic &diag) =0;

protected:  //Helper functions for inherited classes
  void doTranssisionStrategy(SCStrategy to, const char *reason);
  void doSetState(double SCState::*pState, double val);
  SCStrategy getID() const { return m_ID; }

private:    //Prevents inherited classes from access this member
  SCStrategy m_ID;
  CDiagnostic *m_pDiag;      //These variables are set from CDiagnostic (ie. friend) curring registration

  friend class CDiagnostic;
};



#endif
