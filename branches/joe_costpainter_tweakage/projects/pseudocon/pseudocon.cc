#include "pseudocon.hh"

using namespace std;



StatePrinter::StatePrinter(int skynetKey) : m_skynet(MODsupercon, skynetKey)
{
  // do something here if you want
}




StatePrinter::~StatePrinter()
{
  // Do something
}



void StatePrinter::ActiveLoop()
{
  // set up message counter
  int message_counter = 0;
  int sock;
  int recsock;
  int adrivesock;
  int brec;
  int btorec;
  char* m_pDataBuffer;
  double command;
  superConTrajFcmd com;
  superConTrajFcmd* comp;
  
  drivecmd_t my_command;
  memset(&my_command, 0, sizeof(my_command));
  my_command.my_actuator = estop;
  my_command.number_arg = 2;
  
  // echo SKYNET_KEY
  cout << "\n\nCurrent SKYNET_KEY: ";
  cout.flush();
  system("echo $SKYNET_KEY");

  sock = m_skynet.get_send_sock(SNsuperconTrajfCmd);
  adrivesock = m_skynet.get_send_sock(SNdrivecmd);
  recsock = m_skynet.listen(SNsuperconTrajfCmd, MODsupercon);
  btorec = sizeof(superConTrajFcmd);
  m_pDataBuffer = new char[btorec];
  cout << "size of com is: " << btorec<<endl;
  cout << "send socket is: " << sock << endl;
  
  // Loop is trapped here during execution
  while(true)
    {
      
      // grab where we are and store it as location, so we only have to access astate
      // once per time through the loop
      cout << "Enter Command:  "<< endl;
      cin  >> command;
      if(command == 0.0)
	{
	  com.commandType = TF_FORWARDS;
	  com.distanceToReverse = 0;
	}
      else
	{
	  com.commandType = TF_REVERSE;
	  com.distanceToReverse = command;
	}
      my_command.number_arg = (int)command;
      //cout << "command is " << my_command.number_arg << endl;
      m_skynet.send_msg(sock, &com, sizeof(com),0);
      //m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
      m_skynet.get_msg(recsock, m_pDataBuffer, btorec,0);
      comp = (superConTrajFcmd*)m_pDataBuffer;
      cout << "got: " << (*comp).distanceToReverse << endl;     
    }

}



int main()
{
  //Setup skynet
  int intSkynetKey = 0;

  char* ptrSkynetKey = getenv("SKYNET_KEY");
  
  if(ptrSkynetKey == NULL)
    {
      cout << "Unable to get skynet key!" << endl;
      return 0;
    }
  else
    {
      //cout << "Got to StatePrinter int main" << endl;
      intSkynetKey = atoi(ptrSkynetKey);
      StatePrinter StatePrinterObj(intSkynetKey);
      
      
      //cout << "Entering ActiveLoop." << endl;
      StatePrinterObj.ActiveLoop();
    }
  
  return 0;
}
