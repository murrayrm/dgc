#ifndef PSEUDOCON_HH
#define PSEUDOCON_HH

using namespace std;

//#include "StateClient.h"
#include "sn_msg.hh"
#include "sn_types.h"
#include "interface_superCon_trajF.hh"
#include "adrive_skynet_interface_types.h"
#include <stdio.h>
#include <iostream>

class StatePrinter 
{
public:
  /** Contstructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  StatePrinter(int skynetKey);

  /** Standard destructor */
  ~StatePrinter();

  /** The main loop where execution is trapped */
  void ActiveLoop();

private:

  skynet m_skynet;

};

#endif  // STATEPRINTER_HH
