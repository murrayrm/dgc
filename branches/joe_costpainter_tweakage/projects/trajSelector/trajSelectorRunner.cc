#include "trajSelector.h"
#include <iostream>

using namespace std;

int main(void)
{

  char* keyPtr = getenv("SKYNET_KEY");

  int key;

  if(keyPtr == NULL) //No SKYNET_KEY environment variable has been set.
    {
      cout<<"No Skynet environemnt variable has been set.  Input the Skynet key to use: "<<endl;
      cin>>key;
    }
  else
    {
      key = atoi(keyPtr);
    }

  trajSelector selector(5, .15, key);

  selector.select();
}
