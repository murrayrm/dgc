#include "traj.h"
#include "DGCutils"
#include "TrajTalker.h"
#include "CMapPlus.hh"
#include "MapdeltaTalker.h"
#include "StateClient.h"
#include "CTimberClient.hh"

class trajChecker: public CTrajTalker, public CMapdeltaTalker, public CStateClient, public CTimberClient
{
 private:
 
  double PERCENT_BONUS;   //"Bonus" to apply to the traj that we're currently following relative to other trajs; a form of hysteresis to prevent chaotic traj switching.

  CTraj trajs [2];  //The trajs that we're currently comparing against one another for publishing.
  CTraj trajUpdates [2];  //Buffers for incoming traj data to be stored in while we are comparing trajs in the "trajs" array.

  CTraj previousTraj;  //The last traj that was sent out.

  double avgSpeeds[2];  //The average speed of the traj in the corresponding slot in the "trajs" array.

  int trajWeAreFollowing;  //The number of the slot in the "trajs" array that has the traj that has been selected.




  //Mutexes to control access to the trajUpdates array
  pthread_mutex_t deliberativeTraj;

  pthread_mutex_t reactiveTraj;

  pthread_mutex_t mapMutex;

  pthread_mutex_t previousTrajMutex;

  //Mutex array to keep track of the time of reception of each traj.

  pthread_mutex_t times [2];

  //bool array to keep track of which trajs we are listening for.
  bool listenForTrajs [2];

  //bool array to keep track of whether or not a traj contains any points with excessive speeds.  Note that we MAY want to use these trajs at some point.
  bool excessiveSpeeds [2];

  //bool array to keep track of whether or not a traj is "bad".  Any traj on this list will NEVER BE SELECTED.
  bool badTrajs [2];

  //Array to keep track of when trajs were last received.
  unsigned long long trajTimes [2];
  double trajElapsedTimes [2];


#define DELAY_TIME .05 //Time in seconds that trajChecker will hang for at the end of its selection cycle.
#define TRAJ_TIMEOUT 3  //The maximum age (in seconds) of a plan that we will consider sending forward - if any traj is older than this value, we will consider the planner that generated it to have crapped out, and will stop listening to it.
#define LOOKAHEAD_DIST 1 //Distance (in meters) that we should look ahead on the previously planned traj from which to plan our joining segment between trajs
#define CONE_ANGLE M_PI/4
  CMapPlus m_map;  //The cost map of the terrain immediately around the vehicle.

#define LOG_FILE "trajChecker.dat"
#define HEADER_FILE "trajChecker.header"


  //The map delta buffer.
#define MAX_DELTA_SIZE 1000000
#define SPEED_NO_DATA 0.01

  char* mapDelta;

  int speedLayer; //The ID number for the speed layer in the costMap







  //Communications sockets


  //Sockets for receiving traj data from planners.
  int deliberativeSocket, reactiveSocket;


  //Socket for publishing the selected traj.
  int trajSocket;

  //Sockets for receiving map data.
  int mapFullRequestSocket;

  int mapDeltaSocket;





  //State data

  double vehicleUTMNorthing;
  double vehicleUTMEasting;


  //Private helper methods.
  
  
  
  //Threaded functions to listen for traj updates.  These will write trajs into the buffer "trajUpdates" as they come in.
  void deliberativeListener();
  
  void reactiveListener();
  
  
  //Threaded function to listen for the cost map updates.
  
  void mapListener();
  
  

  //Modifies trajs to begin at a point a designated distance in front of the vehicle (guarantees that trajs will be continuous during switching.
  void modifyTraj(CTraj& trajToModify);
  
  //Modifies trajs coming in from spacial-path-only sources to assign a speed profile w/ dynamic feasibility.
  void assignSpeedProfile();  
  
  
 public:
  
  trajChecker(double bonus, int sn_key);
  
  
  //The main program loop for trajChecker.  Selects the optimum traj; selection is determined by the traj with the greatest average speed.  The current traj is given a percentage bonus determined by the PERCENT_BONUS variable.
  void check();    
  

  ~trajChecker();
};
