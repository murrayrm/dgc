// GPS control and communications code
// Jeremy Gillula Summer '03
#include "novatel.hh"
#include <iostream>

#include <sstream>
#include <SerialStream.h>

using namespace LibSerial;

SerialStream nov_serial_port;

using namespace std;

// Default constructor for the novatelDataWrapper class
novDataWrapper::novDataWrapper() {}

// Default destructor for the novatelDataWrapper class
novDataWrapper::~novDataWrapper() {}


//This function decodes a NOVATEL PVT message in pvt_buffer, and updates the data accordingy
//The decoding follows the spec in the NavCom reference manual under message B1
int novDataWrapper::update_nov_data(char *buffer) {
  int retval = -1;


  unsigned char H =  get_u08(buffer + 3);

  data.msg_type = (int) get_u16(buffer + 4);


  switch (data.msg_type) {
  case BESTPOS:
    data.sol_status = get_u32(buffer + H + 0);
    data.pos_type = get_u32(buffer + H + 4);
    data.lat = get_double(buffer + H + 8);
    data.lon = get_double(buffer + H + 16);
    data.hgt = get_double(buffer + H + 24);
    data.undulation = get_float(buffer + H + 32);
    data.lat_std = get_float(buffer + H + 40);
    data.lon_std = get_float(buffer + H + 44);
    data.hgt_std = get_float(buffer + H + 48);
    retval = 1;
    break;
  case BESTVEL:
    data.sol_status = get_u32(buffer + H + 0);
    data.vel_type = get_u32(buffer + H + 4);
    data.latency = get_float(buffer + H + 8);
    data.hor_spd = get_double(buffer + H + 16);
    data.trk_gnd = get_double(buffer + H + 24);
    data.vert_spd = get_double(buffer + H + 32);
    retval = 1;
    break;
  default:
    break;
  }

  return retval;
}

// Initialize NOVATEL Unit to output PVT messages at the desired rate
int nov_open(int com)
{
  
  ostringstream port;
  port << "/dev/ttyS" << com;
  
  nov_serial_port.Open(port.str());
  if ( ! nov_serial_port.good() ) 
    {
      cerr << "nov_open() error: Could not open port." << endl ;
    }
  nov_serial_port.SetBaudRate( SerialStreamBuf::BAUD_9600 ) ;
  if ( ! nov_serial_port.good() )
    {
      cerr << "nov_open() error: Could not set the baud rate." << endl ;
      return -1;
    }
  nov_serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8) ;
  if ( ! nov_serial_port.good() )
    {
      cerr << "nov_open() error: Could not set the char size." << endl ;
      return -1;
    }

  char* output;
  output = "unlogall com1\r\n\0";
  nov_write(output, strlen(output));
  output = "log com1 bestposb onnew\r\n\0";
  nov_write(output, strlen(output));
  output = "log com1 bestvelb onnew\r\n\0";
  nov_write(output, strlen(output));
  
  return 1;

}


//Closes the GPS's serial port
int nov_close()
{

  nov_serial_port.Close();
  return 1;

}

int nov_read(char* orig_buffer, int buf_length)
{

  //Make sure the buffer is big enough...
  if(buf_length < 8)
    {
      cerr << "gps.cc: gps_read(): Buffer for storing GPS message too small." << endl;
      return -1;
    }
  
  // keep going until we get a valid message
  while(true)
    {
      // Clear out the gps serial buffer.  Chances are there is a lot of 
      // data piled up in the receive queue that we do not want, rather
      // we want the NEXT valid PVT. 
      nov_serial_port.sync();
      
      char dummy[1024];
      nov_serial_port.getline(dummy, 1024, '\xAA');

      orig_buffer[0] = '\xAA';
      
      nov_serial_port.read(orig_buffer+1, 3);

      if (orig_buffer[1] == '\x44' && orig_buffer[2] == '\x12') {

	unsigned char hdr_length = get_u08(orig_buffer+3);

	nov_serial_port.read(orig_buffer+4, hdr_length - 4);

	unsigned short msg_length = get_u16(orig_buffer + 8);
      
	nov_serial_port.read(orig_buffer + hdr_length, msg_length);

	return 1;
      } else {
	continue;
      }
    }
} // end gps_read()

int nov_write(char *buffer, int buf_length) {
  nov_serial_port.write(buffer, buf_length);
  return 1;
}

//Extracts the GPS command_id from the GPS message stored in buffer
//Follows the spec for GPS messages in the NavCom reference manual
int nov_msg_type(char * buffer) {
  return (int) get_u16(buffer+4);
}




//The following functions use a bunch of bit-wise arithmetic to decode and encode
//integers of a specific bit-length
unsigned int get_u08(char* ptr) {
  unsigned char result = *(unsigned char *) ptr;
  return result;
}

unsigned int get_u16(char* ptr) {
  unsigned short result = *(unsigned short *) ptr;
  return result;
}

unsigned int get_u32(char* ptr) {
  unsigned long result = *(unsigned long *) ptr;
  return result;
}

int get_s08(char* ptr) {
  signed char result = *(signed char *) ptr;
  return result;
}

int get_s16(char* ptr) {
  signed short result = *(signed short *) ptr;
  return result;
}

int get_s32(char* ptr) {
  signed long result = *(signed long *) ptr;
  return result;
}

float get_float(char* ptr) {
  float result = *(float *) ptr;
  return result;
}

double get_double(char* ptr) {
  double result = *(double *) ptr;
  return result;
}

