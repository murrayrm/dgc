#ifndef NOV
#define NOV

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "SDSPort.h"


//using namespace std;

//GPS Message Types
#define BESTPOS 42
#define BESTVEL 99

//Struct which contains all the data returned by a PVT (position velocity time) message from the GPS unit
//Refer to message B1 in the NavCom technical reference for information on the different varables
struct novData {
  int msg_type;

  int sol_status;

  int pos_type;
  double lat;
  double lon;
  double hgt;
  float undulation;
  float lat_std;
  float lon_std;
  float hgt_std;

  int vel_type;
  float latency;
  double hor_spd;
  double trk_gnd;
  double vert_spd;
};


//A class to wrap the GPS PVT data around
class novDataWrapper {
 public:
  //Constructors/Destructors
  novDataWrapper();
  ~novDataWrapper();

  //Updates the data in the object based on the GPS message stored in pvt_buffer
  int update_nov_data(char *buffer);

  //Data
  novData data;
  
 private:
};


//GPS Functions
//Initializes the GPS, and makes it output PVT messages at the desired rate
int nov_open(int com);

//Or close the gps by simply closing the serial port
int nov_close();


//Gets the first GPS message waiting in the serial buffer, and stores it in orig_buffer
//If the message is longer than buf_length (which should be the length of orig_buffer) then it returns an error
int nov_read(char *orig_buffer, int buf_length);

int nov_write(char *buffer, int buf_length);

 unsigned int get_u08(char* ptr);
unsigned int get_u16(char* ptr);
unsigned int get_u24(char* ptr);
unsigned int get_u32(char* ptr);

int get_s08(char* ptr);
int get_s16(char* ptr);
int get_s24(char* ptr);
int get_s32(char* ptr);

float get_float(char* ptr);
double get_double(char* ptr);

#endif /* GPS_H */
