/******************************************************************
 **
 **  test.cpp
 **
 **    Time-stamp: <2003-06-13 14:12:11 evo>
 **
 **    Author: Sam Pfister
 **    Created: Thu May 22 21:53:37 2003
 **
 **
 ******************************************************************
 **
 **
 **
 ******************************************************************/
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <math.h>
#include "OBDIIDriver.hpp"
#include "SDSPort.h"
using namespace std;

#ifdef STEER_SERIAL
#include <SerialStream.h>
using namespace LibSerial;
#endif


int main(int argc, char* argv[])
{
 
	int debugval = 0;
	if (argc >1){
		debugval = atoi(argv[1]);
		cout << "debugval  = " << debugval <<  endl;
	}
	OBDIIDriver obd;
		

	int portnum  = 4; //"/dev/ttyS0";
	
 	cout << "(from OBDII_test.cpp) connecting " << endl;	
	
	if (obd.connect(portnum) < 0){
		cout << "(from OBDII_test.cpp) Unable to open serial port "<< portnum << endl;
	}
	else{
		cout << "(from OBDII_test.cpp) Serial port " << portnum << " opened"<< endl;
		
	 	obd.setdebug(debugval);
		
		double doubleval = 0.0;
		int retval = 0;
		char charval = 0x00;
		int intval = 0;	
	


		cout << "getRPM                       ";
		retval = obd.getRPM(doubleval);
		cout << "return val = " << retval << "  data val = " << doubleval << endl;
			
		cout << "getTimeSinceEngineStart      " ;
		retval = obd.getTimeSinceEngineStart(intval);
		cout << "return val = "<< retval << "  data val = " << intval << endl;

		
		cout << "getVehicleSpeed              ";
		retval = obd.getVehicleSpeed(doubleval);
		cout << "return val = " << retval << "  data val = "<< doubleval << endl;
		
		
		cout << "getEngineCoolantTemp         ";
		retval = obd.getEngineCoolantTemp(doubleval);
		cout << "return val = "<< retval << "  data val = " << doubleval << endl;
		

		cout << "getThrottlePosition          " ;
	  retval = obd.getThrottlePosition(doubleval);
		cout << "return val = "<< retval << "  data val = " << doubleval << endl;
		

		cout << "getTorque                    ";
		retval = obd.getTorque(doubleval);
		cout << "return val = " << retval << "  data val = "<< doubleval << endl;


		cout << "getGlowPlugLampTime          " ;
		retval = obd.getGlowPlugLampTime(intval);
		cout << "return val = "<< retval << "  data val = " << intval << endl;

		
		cout << "getCurrentGearRatio          ";
		retval = obd.getCurrentGearRatio(doubleval);
		cout << "return val = "<< retval << "  data val = " << doubleval << endl;


		cout << "GlowPlugLightOn?             " ;
		retval = obd.GlowPlugLightOn(intval);
		cout << "return val = "<< retval << "  data val = " << intval << endl;


		cout << "getCurrentGear               ";
		retval = obd.getCurrentGear(intval);
		cout << "return val = "<< retval << "  data val = " << intval << endl;

		
		cout << "getTransmissionPosition      "; 
		retval = obd.getTransmissionPosition(charval);
		cout << "return val = "<< retval << "  data val = " << charval << endl;
		

		cout << "getTorqueConvClutchDutyCycle "; 
		retval = obd.getTorqueConvClutchDutyCycle(doubleval);
		cout << "return val = "<< retval << "  data val = " << doubleval << endl;



		cout << "getTorqueConvControlState    " ;
		retval = obd.getTorqueConvControlState(intval);
		cout << "return val = "<< retval << "  data val = " << intval << endl;


		cout << "getBatteryVoltage            " ;
		retval = obd.getBatteryVoltage(doubleval);
		cout << "return val = "<< retval << "  data val = " << doubleval << endl;
		obd.disconnect();
	}

	return(1);
}
