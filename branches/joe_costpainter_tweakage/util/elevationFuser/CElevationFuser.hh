#ifndef __CELEVATIONFUSER_HH__
#define __CELEVATIONFUSER_HH__

#include <math.h>
#include <stdlib.h>
#include <iostream>

#include "frames/coords.hh"

using namespace std;

class CElevationFuser {
public:
  CElevationFuser();
  ~CElevationFuser();
  
  enum CELL_TYPE {
    OUTSIDE_MAP = 0,
    EMPTY = 1,
    DATA  = 2
  };

  enum STATUS {
    OK,
    ERROR
  };

  enum DISPLAY_TYPE {
    ALL = 0,
    ELEVATION = 1
  };

  struct ElevationFuserData {
    double meanElevation;
    double meanSquaredElevation;
    double numPoints;

    double meanN;
    double meanE;
  
    DISPLAY_TYPE displayType;

    CELL_TYPE cellType;
  };

  STATUS setOutsideMap();
  STATUS resetNoData();
  CELL_TYPE getCellType() const;
  DISPLAY_TYPE getDisplayType() const;

  //Simplest fusion possible - unweighted averaging
  STATUS fuse_MeanElevation(NEDcoord otherPoint);
  STATUS fuse_MeanElevation(CElevationFuser otherCell);

  //Note that since positive is down for us, this actually ends up putting the
  //minimum value in the current cell
  STATUS fuse_MaxElevation(NEDcoord otherPoint);
  STATUS fuse_MaxElevation(CElevationFuser otherCell);

  ElevationFuserData getData() const;
  double getMeanElevation() const;
  double getMeanN() const;
  double getMeanE() const;
  double getMeanSquaredElevation() const;
  double getStdDev() const;
  double getNumPoints() const;

  bool operator== (const CElevationFuser other) const;
  bool operator!= (const CElevationFuser other) const;

  CElevationFuser& operator=(const CElevationFuser& other); 

  STATUS setDisplayType(DISPLAY_TYPE displayType);

  friend istream& operator>> (istream& is, CElevationFuser& destination);

private:
  ElevationFuserData _data;
};

ostream& operator<< (ostream& os, const CElevationFuser& source);

#endif //__CELEVATIONFUSER_HH__
