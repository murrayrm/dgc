#ifndef __CCOSTPAINTER_HH__
#define __CCOSTPAINTER_HH__

#include <stdlib.h>
#include <math.h>

#include "CMap.hh"
#include "CCorridorPainter.hh"
#include "CElevationFuser.hh"
#include "geometry/Geometry.hh"

#define MAX_SPEED 22.35

struct FusionMapperOptions {
  int optLadar, optStereo, optStatic, optRoad, optSupercon;
  int optSNKey;
  int optAverage;
  int optMax;

  CCorridorPainterOptions corridorOpts;
  int optNonBinary;
  int optZeroGradSpeedAsRDDFPercentage;
  double optZeroGradSpeed;
  double optUnpassableObsHeight;
  char configFilename[255];
  int optUseAreaGradient;
  double optObstacleDistanceLimit, optDistantObstacleSpeed;
  double optAreaGradScaling;
  double optStaticScaling, optRoadScaling;
  double optStdDevWeight;
  int optVeloGenerationAlg;
  double optVeloTweak1, optVeloTweak2, optVeloTweak3;
  double optStdDevScaling;
  double optGradPowerTweak, optOtherPowerTweak;
  double optMaxSpeed;

  double stdDevThreshold;
  int kernelSize;
	double sigma;
	int maxInterpolateCells;
	int minNumSupportingCells;
	int minNumSupportingCellsForInterpolation;
};


class CCostPainter
{
public:
  
  CCostPainter ();
  ~CCostPainter ();

  int initPainter (CMap * inputMap, int costLayerNum, int elevLayerNum,
                   int corridorLayerNum, int staticLayerNum, int roadLayerNum,int fusedLayerNum ,int nonBinary,
                   int growLayerNum, double radius, int conservativeGrowing,
									 CMap* speedMap = NULL, int sampledCostLayerNum = 0, int sampledCorridorLayerNum = 0,
									 int fusedYetLayerNum = 0, int superconLayerNum = 0);

  int paintChanges (double UTMNorthing, double UTMEasting, 
                    bool paintSurrounding);

  int repaintAll(bool paintSurrounding);
	//int uberRepaintAll(bool paintSurrounding);

  double generateVeloFromGrad (double gradient, double zeroGrowSpeed,
                               double unpassableObsHeight);
  double generateVeloFromGrad2 (double gradient, double zeroGradSpeed,
			       double unpassableObsHeight, double tweak1,double  tweak2);
  double generateVeloFromGrad3 (double gradient, double zeroGradSpeed, double unpassableObsHeight,
				double tweak1, double tweak2);

  void paintRoad( double * terrainSpeed, double * roadSpeed, double * roadScaling, double * noData);
 
  void setMapperOpts(FusionMapperOptions * mapperOpts);

  //  int calcAreaGradient(double UTMNorthing, double UTMEasting, double &  gradientNorthing, double &  gradientEasting);

  //  double gradientCorrection(double gradient, double  areaGradient, double gradScaling);

  // int growChanges (double UTMNorthing, double UTMEasting);

  // int growChanges (NEcoord exposedArea[]);

  //int paintChangesUber(double UTMNorthing, double UTMEasting, bool paintSurrounding);

	int paintChangesNotify(double UTMNorthing, double UTMEasting);

	int paintNotifications();

  //Returns multipler*(a/(gradient+b)+c)
  double inverse(double gradient, double a, double b, double c, double multiplier = 1.0);

  //Returns multiplier*(m*gradient+b)
  double linear(double gradient, double m, double b, double multiplier = 1.0);

	double atan(double val, double xCenter, double yCenter, double verticalHalfRange, double steepness, double multiplier);
  
  FusionMapperOptions  _mapperOpts;
  enum
  {
    CCOSTPAINTER_NOT_PAINTED = 0,
    CCOSTPAINTER_OUTSIDE_MAP = -1,

    CCOSTPAINTER_NUM_TYPES
  };


private:
  CMap* _inputMap;
	CMap* _speedMap;

  int _nonBinary;
  int _costLayerNum;
  int _elevLayerNum;
  int _corridorLayerNum;
  int _growLayerNum;
  int _staticLayerNum;
  int _roadLayerNum;
  int _fusedLayerNum;

	int _fusedYetLayerNum;

	int _superconLayerNum;

	int _sampledCorridorLayerNum;
	int _sampledCostLayerNum;

  int _numRows;
  int _numCols;

  int _numMaskPts;

  double _radius;
  int _conservativeGrowing;

  int *_maskRow;
  int *_maskCol;

	double logKernel[289];;

  void constrainRows(int currentRow, int numRows, int& minRow, int& maxRow);
  void constrainCols(int currentCol, int numCols, int& minCol, int& maxCol);

  void constrainRange(int start, int numDiff, int min, int max, int& resultMin, int& resultMax);
};


#endif //__CCOSTPAINTER_HH__
