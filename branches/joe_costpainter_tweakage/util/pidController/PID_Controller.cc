/*
 *CPID_Controller: PID Lateral & Longitudinal Trajectory Follower 
 *implemented using the wrapper classes (HISTORIC:PathFollower (MTA)),
 * (HISTORIC:sn_TrajFollower (Skynet))TrajFollower (CURRENT: Skynet)
 */

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include <unistd.h>
#include <iostream> 
#include <iomanip>
#include <time.h>
#include <string.h>

#include "AliceConstants.h" // for vehicle constants
#include "PID_Controller.hh"

using namespace std;

/**
 * FUNCTION:  Constructor for the PID_Controller class
 * ACTIONS: Initializes variables, creates the pid objects. 
 *  m_YerrorIndex indicates where (front=1, rear=0) yerror 
 * to be calculated from, and m_AerrorIndex does the same
 * for angle error.
*/
CPID_Controller::CPID_Controller(EYerrorside yerrorside, EAerrorside aerrorside,bool bUseLateralFF,string logs_location, string lindzey_logs_location, bool rev): m_bUseLateralFF(bUseLateralFF)
{
  reverse = rev;

  /** if a filename was input, log to that */
if(lindzey_logs_location!="") 
    {
      m_outputLindzey.open(lindzey_logs_location.c_str());
      m_outputLindzey<<setprecision(20);
    }


  /** initializing variable that indicates
   * whether we're too close to the end of the
   * trajectory to continue at our current speed
   */
    too_close_to_end = false;

  readspecs();

  /** 
   * checking if we're using the gain schedulin   
   * option. if so, setup the controllers with the 
   * proper initial gains, and if not, do it the
   * old way 
   */
  if(reverse)
    {
      m_pSpeedPID   = new Cpid(LongGainRevFilename);
      m_pLateralPID = new Cpid(LatGainRevFilename);
    }
  else
    {

      initializeGainScheduling();

      double p,i,d;
      p = min(.18,getPgain());
      i = min(.1,getIgain());
      d = getDgain();

      //      SparrowHawk().log( "initial gains: p,i,d: %d, %d, %d", p, i, d );
      cout<<" iniital gains: p,i,d: "<<p<<' '<<i<<' '<<d<<endl;

      /** someday, may use gain scheduling for speed
       * control, but not yet */
      m_pSpeedPID   = new Cpid(LongGainFilename);

#warning "integral terms are wonderful. but only if they're defined with a good saturation value!!!"
      //for now, using 10 for saturation. should be changed!!!
      m_pLateralPID = new Cpid(p,i,d,.2);
      cout<<"lateral pid constructed with above gains"<<endl;

    }

  /** 
   * initializes the variables that indicate where errors
   * should be calculated from 
   */
  m_YerrorIndex = (yerrorside==YERR_FRONT ? FRONT : REAR);
  m_AerrorIndex = (aerrorside==AERR_FRONT ? FRONT : REAR); 
  // NOTE that aerror==yaw does NOT get its own index
  m_AerrorYaw = aerrorside==AERR_YAW;


  /** reset all values to be stored. this makes memory 
   * profilers not complain about uninitialized data */
  m_Aerror[m_AerrorIndex] = 0.0;
  m_Yerror[m_YerrorIndex] = 0.0;
  m_Cerror                = 0.0;
  m_Verror                = 0.0;
  m_AccelFF               = 0.0;
  m_pitchFF               = 0.0;
  m_speedFF               = 0.0;
  m_AccelFB               = 0.0;
  m_accelCmd              = 0.0;
  m_SteerFF               = 0.0;
  m_SteerFB               = 0.0;
  m_steerCmd              = 0.0;
  m_trajSpeed             = 0.0;
  m_oldSteerCmd           = 0.0;
  m_refSpeed              = 0.0;
  m_actSpeed              = 0.0;
  m_minSpeed              = -1.0;
  cout<<"initialized max Speed"<<endl;
  m_maxSpeed              = 100.0;
  trajF_current_state_Timestamp = 0.0;
  trajf_status_string[0] = '\0';
  we_are_paused           = 1;
  we_are_stopped          = 0;


  //for information & confirmation
  cout <<"You selected:"<<' '<<m_YerrorIndex<<"for y-error reference position"<<endl;
  cout <<"You selected:"<<' '<<m_AerrorIndex<<"for a-error reference position"<<endl;
  cout<<"0 indicates rear axle, 1 indicates front axle"<<endl;

  cout << "CPID_Controller constructor exiting" << endl;
}



/*
 *FUNCTION: destructor
 *ACTION: frees memory initialized in the two pid objects
 *INPUTS: none
 *OUTPUTS: none
 */

CPID_Controller::~CPID_Controller()
{
  delete m_pSpeedPID;
  delete m_pLateralPID;
}



/* 
 * FUNCTION: getControlVariables
 * ACTION: this is the main interface to the controller. This function is
 * called by trajFollower to get the desired accel and phi commands.  The
 * architecture it is based on can be seen in Alex's "trajfollower_flow.jpg"
 *
 * INPUTS:  current vehicle state
 * OUTPUTS: accel and phi, passed back through the provided pointers
 */

void CPID_Controller::getControlVariables(VehicleState *pState, ActuatorState *pActuator, double *accel, double *phi, bool logs_enabled, string logs_location, bool logs_newDir, double speedCapMin, double speedCapMax)
{
  /** sets the error flag to false at beginning of every loop */
  exception_error_flag = false;

  //cout<<"entering getControlVariables"<<endl;
  /** If a new directory has been given to us, re-setup logs */
  if(logs_newDir) 
    {
      setup_Output(logs_location);
    }

  /** If our reference is empty (if we haven't yet gotten 
   * a reference), set 0 inputs and return */
  if(m_traj.getNumPoints() == 0)
  {
    *accel = 0.0;
    *phi   = 0.0;
    return;
  }

  m_minSpeed = speedCapMin;
  m_maxSpeed = speedCapMax;


  /** checking if parameter file has been changed */
  readspecs();

  /** check if gain scheduling is supposed to be in use, 
   * and if so, update the gains */
  updateGainScheduling();

 
  /** compute the state of the two axles */
  compute_FrontAndRearState_veh(pState);

  /** computes the traj index to be used for error computations */
#warning "Check case where we are facing the wrong direction and argument is nonzero."
  oldIndex = m_refIndex;
  m_refIndex = getClosestIndex(LOOKAHEAD_FF_LAT_REFPOS);
  SparrowHawk().log("reference Index: %d",m_refIndex);

  /** computes the new distance to the end of the trajectory */
  calculateDistToEnd();
  SparrowHawk().log("distance to end: %lf",dist_to_end);

  /** compute the desired state of the two axles */
  compute_FrontAndRearState_ref();

  
  /******* LATERAL CONTROL SETUP AND IMPLEMENTATION *******/
  
  
  /** Now compute our deviations from the reference state: */
  compute_YError(); // The spatial error (distance to reference point)
  compute_AError(); // The angular error (error in the angle)
  compute_CError(); // The combined error (linear combination of the y and angle errors)
  
  /** compute the commanded steering value */
  compute_SteerCmd();




  /******* LONGITUDINAL CONTROL SETUP AND IMPLEMENTATION *******/

  
  /** compute the actual speed */
  m_actSpeed = hypot(m_vehDN[FRONT], m_vehDE[FRONT]);

  /** Calculate the reference speed */
  compute_refSpeed();

  /** Calculate velocity error */
  m_Verror = m_refSpeed - m_actSpeed;

#ifdef NO_ACC_FF 
  compute_pitchFF(pState->Pitch);
  compute_speedFF();
#endif
  compute_AccFF();

  /** Calculate the feed-back acceleration */
  m_AccelFB = m_pSpeedPID->step_forward(m_Verror);

  /** add FF and FB to get command */
  m_accelCmd = m_speedFF + m_pitchFF + m_AccelFB;
  
  //m_accelCmd = m_AccelFB;
  
  /** Check that the commanded acceleration is < +/- saturation
   * if not, saturate the steering command */
  m_accelCmd = fmax(fmin(m_accelCmd, VEHICLE_MAX_ACCEL), -VEHICLE_MAX_DECEL);


  /** checking if we're approaching the end of the path */
  
 #warning "this should be scaled with current velocity"  


  /** TESTS FOR EXCEPTIONS - AND HANDLING OF ANY EXCEPTIONS FOUND **/

  /**
   * SPARROW STATUS FIELD TRY/(THROW) - used to test for exceptions that 
   * require that the STATUS field in the sparrow display is updated to
   * reflect that the exception has occurred
   */
  try {
  
    /** 
     * IF TrajFollower reaches a point on the 'current' trajectory
     * where there are only this many traj-points until the end of the
     * trajectory then it will assume that something has gone wrong
     * (e.g. the planner has died) and that it is no-longer receiving
     * trajectories - when this occurs it will set the accel cmd to 
     * ERROR_ACCEL_CMD (defined in header file) to command a hard-stop
     */

  #warning "need to check all calculations like this...what action should I take??"

    //     cout<<"isnan(m_STeerFB)"<<isnan(m_SteerFB)<<endl;
    if(isnan(m_SteerFB))
      {
	
	m_SteerFB = 0;
        conditionalStringThrow(true, "SteerFB is nan. reset to 0");
      }
    
 #warning "removed the reverse condition here. ln 333 of PID_Controller.cc"
    //PLEASE write code that is readable, rather than optimized.
    if(dist_to_end <= m_actSpeed*SPEED_TO_STOPPING_DIST_MAPPING || too_close_to_end)
    {
      /** 
       * we're too close to the end of the trajectory
       * and should continue slowing down until we 
       * reach a stop.
       */
      too_close_to_end = true; 

      /**
       *this will be conveted to a brake command later...
       * currently in m/s^2. hopefully, this will 
       * correpond to stomping on the brakes 
       */
      m_accelCmd = ERROR_ACCEL_CMD;
      
      /** Register than an exception has occurred and update the status string */

      /** REMEMBER - YOU ARE LIMITED TO MAX_SPARROW_DISPLAY_STATUS_STRING_LENGTH
       * CHARACTERS (sparrow display size)
       *                             12345678901234567890123456789012       */
      conditionalStringThrow( true, "Too close to end of trj STOPPING");
      
    }
    
    /** 
     * IF the TrajFollower is sent a reference speed command with NEGATIVE ACCELERATION,
     * where'very small' is defined as being < MAX_SPEED_TAKEN_TO_BE_STATIONARY
     * (this is a defined variable in the header file), then the TrajFollower
     * will assume that it is being commanded to STOP (or remain stationary)
     * but due to complications with dealing with ZERO SPEED, when the planner
     * wants to STOP, it often commmands VERY SMALL SPEEDS.  In this situation
     * the trajfollower will set the accel command to ERROR_ACCEL_CMD
     * (defined in the header file) - this should bring Alice to an abrupt STOP
     */
    if(m_refSpeed < MAX_SPEED_TAKEN_TO_BE_STATIONARY && m_AccelFF <= 0.0) {
      m_accelCmd = ERROR_ACCEL_CMD;

      /** register than an exception has occurred and update the status string */

      /** make str short enuf!       12345678901234567890123456789012     */       
      conditionalStringThrow( true, "Trj spd<zero val &decel STOPPING");
          
    }

  } // END of SPARROW STATUS FIELD TRY

  /** 
   * SPARROW STATUS FIELD CATCH - used to catch all strings that need to be
   * displayed in the STATUS field of the sparrow display AFTER AN EXCEPTION
   * HAS OCCURRED
   */
  catch (std::string caught_status_string) {

    /**
     * Length (# of characters) of the caught STATUS string 
     * that is to be displayed in the sparrow display STATUS 
     * FIELD - IF this is > MAX_SPARROW_DISPLAY_STATUS_STRING_LENGTH 
     * (the maximum allowed length of the status string based 
     * on the limitations of the SPARROW display - this is 
     * defined in the PID_Controller.hh HEADER FILE 
     */
    int caught_string_length;

    /** Determine the length of the caught string */
    caught_string_length = caught_status_string.length();
    
    if ( caught_string_length < MAX_SPARROW_DISPLAY_STATUS_STRING_LENGTH ) {
    
      /**
       * The length of the exception string thrown by the 
       * user is within the size that can be displayed on 
       * the sparrow display - hence display their message 
       * (as the exception has occurred) - (trajf_status_string 
       * is the string accessed and displayed by the sparrow display)
       */
      strcpy ( trajf_status_string, caught_status_string.c_str() );

    } else {
    
      /**
       * IF this point is reached then: 
       * caught_string_length > MAX_SPARROW_DISPLAY_STATUS_STRING_LENGTH
       * hence the string thrown is too long to display 
       * on the sparrow display in the STATUS FIELD - hence 
       * replace it with a warning message to tell the module 
       * owner to fix their string so that it fits!
       */

      strcpy ( trajf_status_string, "exception msg string too long!" );    

    }

  } //END of catch

  
  /**
   * CHECK the exception_error_flag boolean to determine IF an exception
   * or error was found in the most recent cycle of the active loop of the
   * controller, if the exception_error_flag is FALSE, then NO exception
   * occurred in the most recent cycle and hence the status displayed in
   * the sparrow display should be updated so that it states that everything
   * is FINE/OK etc
   * NOTE - the exception_error_flag is ALWAYS updated to FALSE at the
   * START of the controller active loop (and is reset to TRUE whenever
   * an exception occurs in the loop).
   * NOTE ALSO that the conditionalStringThrow(...) method automatically
   * updates the exception_error_flag to TRUE whenever the exception test passed
   * in its argument equates to TRUE.
   */
  if ( exception_error_flag == false ) {

    strcpy ( trajf_status_string, "Everything is awesome! :)");

  }




  /******* SETTING THE OUTPUT VARIABLE POINTERS *******/
#ifdef USE_DFE
  *phi = DFE(m_steerCmd, pActuator->m_steerpos);
#else
  *accel = m_accelCmd;
#endif
  *phi   = m_steerCmd;

  /** takes care of logging variables and outputting debug messages */
  output_Vars(logs_enabled, pState, pActuator);
  output_Debug();


  we_are_paused = pActuator->m_estoppos;
#warning "we need a better way of telling if we are actually stopped"
  we_are_stopped = pActuator->m_VehicleWheelSpeed;


  /** if we're paused, we want to reset the integral and derivative errors 
   * because they become irrelevant. (steer commands are still passed through
   * in pause, so we only want to reset if we're also stopped 
   * */
  if((we_are_stopped==0) && (we_are_paused==1 || we_are_paused==0)) 
  {
    //cout<<"We're paused/disabled. Resetting controller"<<endl;
    m_pLateralPID->reset();
    m_pSpeedPID->reset();
    *accel = 0.0;
    *phi   = 0.0;
    return;
  }


}



/**
 *FUNCTION: SetNewPath
 *ACTIN: the method by which PathFollower can inform CPID_Controller of new plans.
 *inputs: pointer to new path
 *outputs: none; it changes internal variables in CPID_Controller
 */
void CPID_Controller::SetNewPath(CTraj* pTraj, VehicleState *state)
{

  /** Set the member variable */
  m_traj = *pTraj;

  /** 
   * reset variable that keeps track of whether
   * we are too close to the end of the path to 
   * continue at our current speed
   */
  too_close_to_end = false;

  /** received a new trajectory, so reset the PID controllers 
   * if speedPID is reset every time, it tends to cause alternate
   * revs/tapping on brakes (we think)
   */
  // m_pSpeedPID->reset();

  /** we really shouldn't reset lateralPID, as the trajs
   * are supposed to be continuous. 
   * the only time that they won't be is when we've strayed
   * sufficiently far from the path that planner says "I 
   * give up--try this path instead"
   * I either need to reference that constant in planner,
   * or find some way of my own to only reset when planner 
   * resets.
   */
#warning "find way to only reset when necessary!!"
  //m_pLateralPID->reset();
   

  /** making valgrind happy by calling this before getclosestindex */
  compute_FrontAndRearState_veh(state);

  /** receiving a new path resets the distance to the end of traj 
   * but first, gotta calculate closest point (cuz until we have 
   * this value, the initializing function doesnt know where to 
   * start from */
  m_refIndex = getClosestIndex(LOOKAHEAD_FF_LAT_REFPOS);
  initializeDistToEnd();

}


/** 
 *Function: initializeDistToEnd
 *Action: computes the distance in meters from current 
 * traj point to end of trajectory. Somewhat crude
 * calculation, as it uses everybody's favorite theorem,
 * thus assuming no curvature to the path. This won't 
 * matter because traj points are fairly closesly spaced, 
 * and this distance will only matter in some failure state.
 *Inputs: none (accesses global variables m_traj and 
 * m_refIndex
 *Outputs: none (changes global variable dist_to_end
 */
void CPID_Controller::initializeDistToEnd()
{
  int curr_point = m_refIndex;
  int total_points = m_traj.getNumPoints();
  double total_dist = 0;
  double temp_dist, n_diff, e_diff;
  for(int i = curr_point; i < total_points - 1; i++)
  {
    n_diff = m_traj.getNorthingDiff(i, 0) - m_traj.getNorthingDiff(i+1, 0);
    e_diff = m_traj.getEastingDiff(i, 0) - m_traj.getEastingDiff(i+1, 0);

    temp_dist = pow(pow(n_diff,2)+pow(e_diff,2),.5);
    
    total_dist += temp_dist;
  }

  dist_to_end = total_dist;

}

/** Function: calculateDistToEnd
 * Action: calculates how much we've traveled since the 
 *   last call of calculateDistToEnd, and subtract that
 *   from the total distance
 * Inputs: none (uses oldIndex, m_refIndexand m_traj)
 * Outputs: none (changes dist_to_end)
 */
void CPID_Controller::calculateDistToEnd() 
{
 int curr_point = oldIndex;;
  int total_points = m_refIndex;
  double total_dist = 0;
  double temp_dist, n_diff, e_diff;
  for(int i = curr_point; i < total_points; i++)
  {
    n_diff = m_traj.getNorthingDiff(i, 0) - m_traj.getNorthingDiff(i+1, 0);
    e_diff = m_traj.getEastingDiff(i, 0) - m_traj.getEastingDiff(i+1, 0);
    temp_dist = pow(pow(n_diff,2)+pow(e_diff,2),.5);
    total_dist += temp_dist;
  }

  dist_to_end = dist_to_end - total_dist;
  //cout<<"updated value of distance to end  "<<dist_to_end<<endl;

}



/*
 *Function: getClosestIndex
 *Action: computes the closest index on the reference trajectory
 *   to the point on the vehicle specified by 'frac'
 *Inputs: double frac. where on vehicle to compute from
 *Outputs: integer index to trajectory
 */

int CPID_Controller::getClosestIndex(double frac)
{
  
  NEcoord rear(m_vehN[REAR], m_vehE[REAR]);
  NEcoord front(m_vehN[FRONT], m_vehE[FRONT]);

  /** Calculate the point that is "frac" distance from 
   * the rear axle to the front axle location.*/

  NEcoord look = rear * (1.0 - frac) + front * frac;
 
  return m_traj.getClosestPoint(look.N, look.E);
}




/*! 
 *Function: compute_FrontAndRearState_veh
 *Action: uses the state struct to initialize member variables containing
 *  information about both axles
 *Inputs: state of vehicle
 *Outputs: none
 */

void CPID_Controller::compute_FrontAndRearState_veh(VehicleState* pState)
{
  /** get the various elements of state for the two axles. 
   *If we're moving very slowly, assume that the wheels are straight */

  m_vehN      [REAR] = pState->Northing_rear();
  m_vehE      [REAR] = pState->Easting_rear();
  m_vehDN     [REAR] = pState->Vel_N_rear();
  m_vehDE     [REAR] = pState->Vel_E_rear();
  m_vehAngle  [REAR] = m_AerrorYaw ? pState->Yaw :
		(pState->Speed2() > MINSPEED ? atan2(m_vehDE[REAR], m_vehDN[REAR]) : pState->Yaw);

  m_vehN      [FRONT] = pState->Northing;
  m_vehE      [FRONT] = pState->Easting;
  m_vehDN     [FRONT] = pState->Vel_N;
  m_vehDE     [FRONT] = pState->Vel_E;
  m_vehAngle[FRONT] = (pState->Speed2() > MINSPEED ?
                       atan2(m_vehDE[FRONT], m_vehDN[FRONT]) : pState->Yaw);

  m_vehYaw            = pState->Yaw;
  m_yawdot            = pState->YawRate;
 
  trajF_current_state_Timestamp = pState->Timestamp;
}



/*! 
 *Function: compute_FrontAndRearState_ref
 *Action: This function computes the pertinent information about the reference state
 *   of both axles into the data structures in the class
 *Inputs: none
 *Outputs: none (initializes variables)
*/
void CPID_Controller::compute_FrontAndRearState_ref()
{
  m_refN      [REAR] = m_traj.getNorthingDiff(m_refIndex, 0);
  m_refE      [REAR] = m_traj.getEastingDiff (m_refIndex, 0);
  m_refDN     [REAR] = m_traj.getNorthingDiff(m_refIndex, 1);
  m_refDE     [REAR] = m_traj.getEastingDiff (m_refIndex, 1);
  m_refDDN_REAR      = m_traj.getNorthingDiff(m_refIndex, 2);
  m_refDDE_REAR      = m_traj.getEastingDiff (m_refIndex, 2);
  m_refAngle[REAR] = atan2(m_refDE[REAR], m_refDN[REAR]);

  int hackConstant = 10;

 hackDN = m_traj.getNorthingDiff(m_refIndex+hackConstant, 1);
 hackDE = m_traj.getNorthingDiff(m_refIndex+hackConstant, 1);
 hackDDN = m_traj.getNorthingDiff(m_refIndex+hackConstant, 2);
 hackDDE = m_traj.getNorthingDiff(m_refIndex+hackConstant, 2);

  m_refN      [FRONT] = m_refN[REAR] + VEHICLE_WHEELBASE*cos(m_refAngle[REAR]);
  m_refE      [FRONT] = m_refE[REAR] + VEHICLE_WHEELBASE*sin(m_refAngle[REAR]);

	double speedRear = hypot(m_refDN[REAR], m_refDE[REAR]);

  m_refDN     [FRONT] = m_refDN[REAR] - VEHICLE_WHEELBASE*sin(m_refAngle[REAR]) * (m_refDN[REAR]*m_refDDE_REAR - m_refDDN_REAR*m_refDE[REAR]) / speedRear/speedRear;
  m_refDE     [FRONT] = m_refDE[REAR] + VEHICLE_WHEELBASE*cos(m_refAngle[REAR]) * (m_refDN[REAR]*m_refDDE_REAR - m_refDDN_REAR*m_refDE[REAR]) / speedRear/speedRear;
  m_refAngle[FRONT]   = atan2(m_refDE[FRONT], m_refDN[FRONT]);

}



/*
 *Function: compute_AccFF
 *Action: computes desired acceleration based on the current trajectory
 *Inputs: none
 *Outputs: none (changes member variable)
 */
void CPID_Controller::compute_AccFF() 
{
#warning "Check m_AccelFF here"
  m_AccelFF = (m_refDN[REAR]*m_refDDN_REAR + m_refDE[REAR]*m_refDDE_REAR) 
              / hypot(m_refDN[REAR], m_refDE[REAR]);

}

#warning "compute_pitchFF needs work!!"
  void CPID_Controller::compute_pitchFF(double pitch)
{
  //  double pitch = m_pState->Pitch;
  double force = 9.8*sin(pitch);
  double cmd;
  if(force > 0) 
    {
      //this was too strong 
      //cmd = force/1.55 + .11;
#warning "this counts throttle dead band twice. oops!"
      cmd = force/3.0 + .11;
    } else if(force ==0) {
      cmd = 0;
    }
  else {
      cmd = force/3.67 - .21;
    }
  m_pitchFF = cmd;;
}


/** This is a FF term that tells us what throttle command we need to maintain 
 *  VRef. Note that this will result in lag when slowing down.
 *  Possibly need a lookahead term that tells us if we need to use it or not. 
 */

  void CPID_Controller::compute_speedFF()
{
  double cmd;
  if(m_refSpeed < 15) {
    cmd = (m_refSpeed + 9) / 90;
  } else {
    cmd = (m_refSpeed - 8) / 32;
  }
  m_speedFF = cmd;
}


/**
 *Function: compute_SteerCmd
 *Action: computes steering command (in radians) by individually
 *   calculating FF and FB, then adding
 *Inputs: none
 *Outputs: none (changes internal variable)
 **/

void CPID_Controller::compute_SteerCmd()
{
  /** calculate the feed-forward (lateral) phi term. 
   * If the user disabled the lateral feed forward term, 
   * this will get set to 0
   */
  compute_SteerFF();

  /** calculate the feedback (lateral) phi term */
  m_SteerFB = m_pLateralPID->step_forward(m_Cerror);


  /** calculate the steering command output */
  if(reverse)
    {
      m_steerCmd = m_SteerFB - m_SteerFF;
    }
  else
    {
      m_steerCmd = m_SteerFB + m_SteerFF;
    }


  /* Check the commanded steering angle is < +/- saturation, 
   * if not saturate steering command */
  m_steerCmd = fmax(fmin(m_steerCmd, VEHICLE_MAX_RIGHT_STEER), -VEHICLE_MAX_LEFT_STEER);

  /** stores command for future reference */
  m_oldSteerCmd = m_steerCmd;

}



/**
 *function: compute_SteerFF
 *action: This feed-forward term uses the first and second order 
 *   northing & easting derivatives included in the traj file to 
 *   determine the nominal steering angle that would be required
 *   to follow the path - (calculations based upon the closest 
 *   point in the traj file to the vehicles current position).  
 *   The nominal steering angle is calculated based upon
 *   the nominal angle (commanded by the point in the traj file) 
 *   using the REAR AXLE centred model, i.e. this uses function 
 *   uses the CURRENT REAR TRAJ-POINT for the spatialn variables & 
 *   their derivatives used. This is important, and CANNOT be 
 *   changed to the FRONT axle system, as in the equations for the 
 *   FRONT axled model, there is an additional UNKNOWN term which is 
 *   very tricky to evaluate, and hence the Planner uses the REAR 
 *   axled model and the Controller MUST be exactly consistent with 
 *   the planner for lateral feed-forwared, as it uses the second 
 *   derivatives (whose values are dependent upon the model used)
 *input: none
 *output: none (changes internal var for feed-forward steering angle (radians)
 */

void CPID_Controller::compute_SteerFF()
{
  // if the user disabled the lateral feed-forward term, set it to 0
  if(!m_bUseLateralFF)
  {
    m_SteerFF = 0.0;
    return;
  }

#warning "should this be from reference speed or somehow scaled to actual speed? What about looking ahead a few points?"
#ifdef USE_HACK_STEER_FF
  double speedRef = hypot(hackDN, hackDE);
  
  m_SteerFF = atan(VEHICLE_WHEELBASE *
                   (hackDN*hackDDE - hackDDN*hackDE)
                   / pow(speedRef, 3.0));
#else

  double speedRef = hypot(m_refDN[REAR], m_refDE[REAR]);
  
  m_SteerFF = atan(VEHICLE_WHEELBASE *
                   (m_refDN[REAR]*m_refDDE_REAR - m_refDDN_REAR*m_refDE[REAR])
                   / pow(speedRef, 3.0));
#endif
}


/**
 *function: compute_YError
 *action: Function to calculate the y-error for any given position of Alice, y-error (m) is
 *defined as the PERPENDICULAR distance of Alice's reference point from the CURRENT 
 *path-segment (this is a straight line segment drawn between the CURRENT and NEXT traj-points)
 */
void CPID_Controller::compute_YError()
{
  // the deviations in northing and easting
  double errN, errE;

  //cout<<"reference position: "<<m_refN[FRONT]<<' '<<m_refE[FRONT]<<endl;
  if(reverse)
    {
      errN = m_refN[FRONT] - m_vehN[FRONT];
      errE = m_refE[FRONT] - m_vehE[FRONT];
      m_Yerror[FRONT] = -(-sin(m_refAngle[FRONT])*errN +
			 cos(m_refAngle[FRONT])*errE);
      
      errN = m_refN[REAR] - m_vehN[REAR];
      errE = m_refE[REAR] - m_vehE[REAR];
      m_Yerror[REAR]  = -(-sin(m_refAngle[REAR])*errN +
			 cos(m_refAngle[REAR])*errE);
    }
  else
    {
      errN = m_refN[FRONT] - m_vehN[FRONT];
      errE = m_refE[FRONT] - m_vehE[FRONT];
      m_Yerror[FRONT] = (-sin(m_refAngle[FRONT])*errN +
			 cos(m_refAngle[FRONT])*errE);
      
      errN = m_refN[REAR] - m_vehN[REAR];
      errE = m_refE[REAR] - m_vehE[REAR];
      m_Yerror[REAR]  = (-sin(m_refAngle[REAR])*errN +
			 cos(m_refAngle[REAR])*errE);
    }
}



/**
 *Function: compute_Aerror
 *Action: This function calculates the angle error, defined as 
 *    [DESIRED angle (rad) - ACTUAL angle (rad)], where the 
 *    DESIRED angle is calculated from the first derivatives 
 *    of northing & easting at the CURRENT traj-point, and the 
 *    ACTUAL angle is taken as the YAW stored in astate.
 *    Note that it uses the N/E convention, as it uses the *GLOBAL 
 *    reference frame (NOT the vehicle reference frame)
 *Inputs: none
 *Output: angle in radians returned as type double
 **/
void CPID_Controller::compute_AError() 
{
  if(reverse)
    {
      m_Aerror[FRONT] = -(m_refAngle[FRONT] - m_vehAngle[FRONT] - M_PI);
      m_Aerror[REAR]  = -(m_refAngle[REAR]  - m_vehAngle[REAR] - M_PI);
    }
  else
    {
      m_Aerror[FRONT] = m_refAngle[FRONT] - m_vehAngle[FRONT];
      m_Aerror[REAR]  = m_refAngle[REAR]  - m_vehAngle[REAR];
    }

  //Constrain the value of h_error to be within the range -pi -> pi    
  m_Aerror[FRONT] = atan2(sin(m_Aerror[FRONT]), cos(m_Aerror[FRONT]));
  m_Aerror[REAR]  = atan2(sin(m_Aerror[REAR]),  cos(m_Aerror[REAR]));
}




/**
 *Function: compute_Cerror
 *Action: Function to calculate the combination error (m_Cerror) from the y_error and h_error
 *   using the values of alpha & beta (gains) specified in the header file associated
 *   with this source file.  Alpha represents the differential weighting between the
 *   y-error and h-error in the combination error equation, and Beta is used to magnify
 *   the h-error so that it has a magnitude approximately equal to the y-error for 
 *   'equal' badness
 *
 *    Uses y-error_tilda, which is y-error after consideration of the perpendicular
 *    theta distance, this is the y-error, greater than which the
 *    controller commands a phi such that Alice heads directly
 *    towards the path (angle perpendicular to that of the path)
 *    y_error_tilda is m_Yerror, saturated so that 
 *    -PERP_THETA_DIST <= y_error_tilda <= PERP_THETA_DIST
 *
 *Input: y-error and h-error values output from compute_YError and compute_Aerror
 *Output: perpendicular distance from the traj-line, in meters
 */

void CPID_Controller::compute_CError()
{
  double y_error_tilda;

  double selectedYError = m_Yerror[m_YerrorIndex];
  double selectedAerror = m_Aerror[m_AerrorIndex];

  y_error_tilda  = fmax(fmin(selectedYError, PERP_THETA_DIST), -PERP_THETA_DIST);
  if(reverse)
    {
      m_Cerror = ((.6*y_error_tilda) + ((1.0-.6)*8.49*selectedAerror));//BETA_GAIN = 8.49, ALPHA_GAIN = .4
    }
  else
    {
      m_Cerror = ((ALPHA_GAIN*y_error_tilda) + ((1.0-ALPHA_GAIN)*BETA_GAIN*selectedAerror));
    }
}





/**  
 *function: DFE
 *inputs: desired steering command (on a scale of -1 to 1)
 *action: compares desired steering angle to curr angle and speed, and
 *   places hard boundaries on the angle and angle's rate of change
 *   as a function of speed
 *outputs: new, bounded steering command
 **/
#warning "the DFE function in PID_Controller needs attention!"

double CPID_Controller::DFE(double cmd, double old)
{
  /** I'd like this to take in the actual steering angle, but since I 
   * don't currently have that we have to use previously commanded one instead
   */
  double speed,diff, maxCmd, maxDiff,newCmd;
  speed = hypot(m_vehDN[FRONT], m_vehDE[FRONT]);

  diff = cmd-old;
  //these will be the functions of speed that limit

	/*
	 *               a=v*theta_dot             //for circular motion with
	 *   ->  theta_dot=a/v                     //with a and v scalar
	 *
	 *       theta_dot=v*sin(phi)/l            //from bicycle model
	 *   ->        a/v=v*sin(phi)/l
	 *   ->      |phi|=arcsin(a*l/v^2)         //for a*l/v^2<=1
	 *   ->      |phi|~=a*l/v^2                //for small phi
	 */
	if(speed == 0) speed = .001;	
	maxCmd = asin(VEHICLE_MAX_LATERAL_ACCEL*VEHICLE_WHEELBASE/(speed*speed));

	/*I doubt that we could actually damage the vehicle by turning the wheel too
		fast, so maxDiff can probably be removed. --Ryan Cable 2005-03-28*/
        //this is currently a pass-through
	//maxDiff = DFE_DELTA_SLOPE*speed+DFE_DELTA_OFFSET;
	maxDiff = .1;
  newCmd = cmd;

  if(diff > maxDiff) 
  {
    //cout<<"diff too large"<<endl;
    newCmd = maxDiff+old;
  }
  if(diff < -maxDiff) 
  {
    //cout<<"diff too small"<<endl; 
    newCmd = -maxDiff+old;
  }
  /**
  if(newCmd > maxCmd)
  {
    if( DEBUG_LEVEL > 1 ) 
    { 
      cout<<"new command too large"<<endl; 
    }
    newCmd = maxCmd;
  }    

  if(newCmd < -maxCmd) 
  {
    if( DEBUG_LEVEL > 1 ) 
    { 
      cout<<"new command too small"<<endl;
    }
    newCmd = -maxCmd;
  }

  */
  //note, this is in radians
  return newCmd;

}




/*
 *Function: compute_trajSpeed
 *Action: calculates the speed that the trajectory actually
 *   indicates that we should be going, based on Edot and Ndot
 */

void CPID_Controller::compute_trajSpeed() 
{
  //taking the desired velocity to be the sqrt of E' and N' squared
  m_trajSpeed = hypot(m_refDN[FRONT], m_refDE[FRONT]);
}



/*
 *Function: compute_refSpeed
 *Action: calculates the reference speed that we will actually
 *   be controlling around. Note that this is NOT equivalent
 *   to m_trajSpeed, because we reduce the speed as a function
 *   of how far off the trajectory we are
 * 
 *   The code tries to implement what's described in the pictorial below, 
 *   where the horiz. axis is fabs(combined error) and the vertical axis is 
 *   reference speed.
 *
 *           - <- v_traj (m_trajSpeed)
 *            \ 
 *             \
 *              \ 
 *               \-------- <- V_LAT_ERROR_SAT
 *           
 *        0--x   ^ 
 *           |   |
 *           0   C_ERROR_SAT  
 *
 *   IF the c-error is > the saturation value, saturate the 
 *   reference speed at the specified saturation speed, otherwise 
 *   set the reference speed to a value determined by a linear -ve 
 *   gradient function with a y-intercept equal to the desired speed (from traj) 
 */

void CPID_Controller::compute_refSpeed() 
{


  //Retrieve the desired speed
  compute_trajSpeed();

#ifdef DONT_LOWER_SPEED

  //        SparrowHawk().log("traj acceleration, traj speed: %f %f",m_AccelFF, m_trajSpeed);

  /** this now checks if were meant to be speeding up/slowing down
   * This should eliminate the problem of taking forever to get up to speed */
	if(m_trajSpeed < MINIMUM_TRACKABLE_SPEED && m_AccelFF > 0) {
           m_refSpeed = MINIMUM_TRACKABLE_SPEED;
	   //           SparrowHawk().log("new ref speed (set to 1): %f",m_refSpeed);
	}
	else if(m_trajSpeed < MINIMUM_TRACKABLE_SPEED && m_AccelFF < 0){
           m_refSpeed = 0.0;
	   // SparrowHawk().log("new ref speed (set to 0): %f",m_refSpeed);
	}
	else {
           m_refSpeed = m_trajSpeed;
           //SparrowHawk().log("new ref speed (no adjustment needed): %f",m_refSpeed);
	}

#else

  // Handle the case where the traj speed is slow anyway
  if( m_trajSpeed <= V_LAT_ERROR_SAT )
  {
    m_refSpeed = m_trajSpeed;
  } else { 
    m_refSpeed = m_trajSpeed + fabs(m_Cerror)*(V_LAT_ERROR_SAT - m_trajSpeed)/C_ERROR_SAT;
  }
  
  // Conditional to ensure that the vref speed is NEVER negative (i.e. not trying to slow down
  // below zero, without this check this condition can occur, and is a parasitic case
  if( m_refSpeed <= V_LAT_ERROR_SAT )
  {
	m_refSpeed = V_LAT_ERROR_SAT;
  }


  if(fabs(m_Cerror) >= C_ERROR_SAT) 
  {
    m_refSpeed = V_LAT_ERROR_SAT;
  }
#endif

 

  if(m_maxSpeed < 90 && m_maxSpeed < m_refSpeed)
    {
      //      cout<<"adjusting max speed! "<<endl;
      //double temp = min(m_refSpeed, m_maxSpeed);
      m_refSpeed = m_maxSpeed;
           SparrowHawk().log("new ref speed (applying max speed): %f",m_refSpeed);

    }

 if(m_minSpeed > 0 && m_minSpeed > m_refSpeed)
    {
      //      cout << "applied min speed" << endl;
    m_refSpeed = m_minSpeed;
           SparrowHawk().log("new ref speed (applying min speed): %f",m_refSpeed);
 
    }
   return;

}



/******** CODE FOR GAIN SCHEDULING ******/
/** Well, i wanted to put this in a separate file,
 * but it just caused too many problems...*/


void CPID_Controller::initializeGainScheduling()
{
  /** trajFollower should never be started excep
   * when Alice is stationary, so it's ok to 
   * initialize to 0.0 */
  scheduling_region = 0.0;
  return;
}

void CPID_Controller::updateGainScheduling()
{
  if(changeGains())
    {

      double p,i,d;
      p = min(.18,getPgain());
      i = min(.1,getIgain());
      d = getDgain();

      bumplessTransfer(p,i,d);

      //cout<<"gains after running updateGainScheduling"<<endl;

      m_pLateralPID->set_Kp(p);
      m_pLateralPID->set_Ki(i);
      m_pLateralPID->set_Kd(d);
    }

}


/** this function resets the integral error to prevent the transfer
 * between different gain scheduling regions causing an abrupt
 * jump in the control signal */
void CPID_Controller::bumplessTransfer(double p, double i, double d)
{

  double oldP,oldI,oldD,errP,errD,errI,diff,newI;
  oldP = m_pLateralPID->get_Kp();
  oldI = m_pLateralPID->get_Ki();
  oldD = m_pLateralPID->get_Kd();
  errP = m_pLateralPID->getP();
  errI = m_pLateralPID->getI();
  errD = m_pLateralPID->getD();

  diff = (oldP-p)*errP + (oldI)*errI + (oldD-d)*errD;
  if(i!=0)
    newI = diff/i;
  else
    newI = 0;

  m_pLateralPID->setI(newI);

}

bool CPID_Controller::changeGains()
{
  double max = scheduling_region + REGION_WIDTH + REGION_OVERLAP;
  double min = scheduling_region - REGION_OVERLAP;

  if(m_actSpeed < min | m_actSpeed > max)
    { 
      scheduling_region = REGION_WIDTH * floor(m_actSpeed/REGION_WIDTH);
      return true;
    } else {
      return false;
    }

}

/** 
 * calculates and returns the proportional gain
 * using the assumption that gains should be 
 * inversely proportional to speed
 */
double CPID_Controller::getPgain()
{
  double tempGainP;
  /** since the regions start at 0, want to 
   * control based on the median velocity. */
  //cout<<"from getPgain: "<<PGAIN<<" "<<STANDARD_SPEED<<" "<<REGION_WIDTH<<endl;


#ifdef USE_LINEAR_SCHEDULING

  /** now, for linearly proportional. max will be double the input gain, slope will be xxx for now */
  tempGainP = 2 * PGAIN  -  PGAIN * (scheduling_region + (.5 * REGION_WIDTH)) / STANDARD_SPEED;
#else

  /** This one is if i do inversely proportional to speed */
  tempGainP = (PGAIN * STANDARD_SPEED) / (scheduling_region + (.5 * REGION_WIDTH));
#endif

  return tempGainP;
}

/** 
 * calculates and returns the integral gain
 * using the assumption that gains should be 
 * inversely proportional to speed
 */
double CPID_Controller::getIgain()
{
  double tempGainI;
  /** since the regions start at 0, want to 
   * control based on the median velocity. */

#ifdef USE_LINEAR_SCHEDULING
  /** This one is for linearly proportional */
  tempGainI = 2 * IGAIN  -  IGAIN * (scheduling_region + (.5 * REGION_WIDTH)) / STANDARD_SPEED;

#else

  /** This one is for inversely proportional. */
  tempGainI = (IGAIN * STANDARD_SPEED) / (scheduling_region + .5 * REGION_WIDTH);

#endif

  return tempGainI;
}

/** 
 * calculates and returns the derivative gain
 * using the assumption that gains should be 
 * inversely proportional to speed
 */
double CPID_Controller::getDgain()
{
  double tempGainD;
  /** since the regions start at 0, want to 
   * control based on the median velocity. */


#ifdef USE_LINEAR_SCHEDULING 
  /** this one is for linearly proportional */
  tempGainD = 2 * DGAIN  -  DGAIN * (scheduling_region + (.5 * REGION_WIDTH)) / STANDARD_SPEED;

#else

  /** This one if for inversely proportional */
  tempGainD = (DGAIN * STANDARD_SPEED) / (scheduling_region + .5 * REGION_WIDTH);

#endif

  return tempGainD;
}
 

/************* END CODE FOR GAIN SCHEDULING *******/


/*
 *Function: output_Vars
 *Action: condenses the printing of internal variables to one
 *    place, to make the code more readable. since each function
 *    tends to be called once, linearly, this works just fine.
 */
void CPID_Controller::output_Vars(bool logs_enabled, VehicleState* m_state, ActuatorState* m_actuator)
{
  double p,i,d,pv,iv,dv; 
  p=m_pLateralPID->get_Kp();
  i=m_pLateralPID->get_Ki();
  d=m_pLateralPID->get_Kd();

  pv=m_pLateralPID->getP();
  iv=m_pLateralPID->getI();
  dv=m_pLateralPID->getD();


  if(logs_enabled) {
    //cout<<"outputVars entered, and logs are enabled! "<<endl;

  //logging the variables used (what all the consts in the .hh file were set to)
#warning "These logged constants are not correct if USE_LAT_GAIN_FILE is defined!"
  m_outputVars<<"VEHICLE_MAX_ACCEL = "<<VEHICLE_MAX_ACCEL<<endl;
  m_outputVars<<"VEHICLE_MAX_DECEL = "<<VEHICLE_MAX_DECEL<<endl;
  m_outputVars<<"VEHICLE_MAX_LATERAL_ACCEL = "<<VEHICLE_MAX_LATERAL_ACCEL<<endl;
  m_outputVars<<"DFE_DELTA_SLOPE = "<<DFE_DELTA_SLOPE<<endl;
  m_outputVars<<"DFE_DELTA_OFFSET = "<<DFE_DELTA_OFFSET<<endl;
  m_outputVars<<"PERP_THETA_DIST = "<<PERP_THETA_DIST<<endl;
  m_outputVars<<"ALPHA_GAIN = "<<ALPHA_GAIN<<endl;
  m_outputVars<<"BETA_GAIN = "<<BETA_GAIN<<endl;


  //  cout<<"trying to output to m_outputLogs"<<endl;
 
  m_outputLogs

    /**internal variables */

<<m_refN[REAR]<<' '<<m_refE[REAR]<<' '
    <<m_Yerror[FRONT] <<' '<<m_Aerror[REAR] <<' '<<m_Cerror<<' '
		 <<m_SteerFF<<' '<<m_SteerFB<<' '
		 <<p<<' '<<i<<' '<<d<<' '
<<m_refSpeed<<' '<<m_actSpeed<<' '<<m_Verror<<' '
<<m_steerCmd<<' '<<m_accelCmd<<' '<<' '<<

    /** the state varaibles ... */

			m_state->Northing << ' ' <<
			m_state->Easting << ' ' <<
			m_state->Altitude << ' ' <<
			m_state->Vel_N << ' ' <<
			m_state->Vel_E << ' ' <<
			m_state->Vel_D << ' ' <<
			m_state->Acc_N << ' ' <<
			m_state->Acc_E << ' ' <<
			m_state->Acc_D << ' ' <<
			m_state->Roll << ' ' <<
			m_state->Pitch << ' ' <<
			m_state->Yaw << ' ' <<
			m_state->RollRate << ' ' <<
			m_state->PitchRate << ' ' <<
			m_state->YawRate << ' ' <<
			m_state->RollAcc << ' ' <<
			m_state->PitchAcc << ' ' <<
			m_state->YawAcc << ' ' <<
			m_state->Northing_rear() << ' ' <<                //Northing position of center of *REAR* axle
			m_state->Easting_rear() << ' ' <<                 //Easting position of center of *REAR* axle
			m_state->Vel_N_rear() << ' ' <<                   //1st diff of Northing at center of *REAR* axle
			m_state->Vel_E_rear() << ' '                  //1st diff of Easting at center of *REAR* axle

    /** and now for the adrive variables **/
<< m_state->Timestamp << ' '



<<  m_actuator->m_steerstatus << ' ' << m_actuator->m_steerpos << ' ' << m_actuator->m_steercmd << ' '
	  
	<<  m_actuator->m_gasstatus << ' ' << m_actuator->m_gaspos << ' ' << m_actuator->m_gascmd << ' '
	  
	  <<  m_actuator->m_brakestatus << ' ' << m_actuator->m_brakepos << ' ' << m_actuator->m_brakecmd << ' ' << m_actuator->m_brakepressure <<' '
	  
	 <<  m_actuator->m_estopstatus << ' ' << m_actuator->m_estoppos << ' '

	 <<  m_actuator->m_transstatus << ' ' << m_actuator->m_transpos << ' ' << m_actuator->m_transcmd << ' '
	  
	 <<  m_actuator->m_obdiistatus << ' ' << m_actuator->m_engineRPM << ' ' << m_actuator->m_TimeSinceEngineStart << ' ' << m_actuator->m_VehicleWheelSpeed <<' '
         <<  m_actuator->m_EngineCoolantTemp << ' ' << m_actuator->m_WheelForce << ' ' << m_actuator->m_GlowPlugLampTime << ' ' << m_actuator->m_CurrentGearRatio
<<m_refIndex << ' '
<<m_traj.getEastingDiff(m_refIndex,0)<<' '<<m_traj.getEastingDiff(m_refIndex,1) << ' ' << m_traj.getEastingDiff(m_refIndex,2)<< ' '<<m_traj.getNorthingDiff(m_refIndex,0)<< ' ' <<m_traj.getNorthingDiff(m_refIndex,1)<< ' ' <<m_traj.getNorthingDiff(m_refIndex,2)

    // current traj point

    //current values of pid (not gains, but value in the pid util)
<<pv<<' '<<iv<<' '<<dv 

<<endl;




  }

  if(m_outputLindzey.is_open()) {


  /** outputs in format (all coordinates are rear-axle)
ref N
ref E
yerr
aerr
cerr
steering ff
steering fb
p
i
d
vehicle speed
ref speed
error speed
accel Cmd
steer Cmd
full state struct
full actuator struct 

  */

  m_outputLindzey

    /**internal variables */

<<m_refN[REAR]<<' '<<m_refE[REAR]<<' '
    <<m_Yerror[REAR] <<' '<<m_Aerror[REAR] <<' '<<m_Cerror<<' '
		 <<m_SteerFF<<' '<<m_SteerFB<<' '
		 <<p<<' '<<i<<' '<<d<<' '
<<m_refSpeed<<' '<<m_actSpeed<<' '<<m_Verror<<' '
<<m_steerCmd<<' '<<m_accelCmd<<' '<<' '<<

    /** the state varaibles ... */

			m_state->Northing << ' ' <<
			m_state->Easting << ' ' <<
			m_state->Altitude << ' ' <<
			m_state->Vel_N << ' ' <<
			m_state->Vel_E << ' ' <<
			m_state->Vel_D << ' ' <<
			m_state->Acc_N << ' ' <<
			m_state->Acc_E << ' ' <<
			m_state->Acc_D << ' ' <<
			m_state->Roll << ' ' <<
			m_state->Pitch << ' ' <<
			m_state->Yaw << ' ' <<
			m_state->RollRate << ' ' <<
			m_state->PitchRate << ' ' <<
			m_state->YawRate << ' ' <<
			m_state->RollAcc << ' ' <<
			m_state->PitchAcc << ' ' <<
			m_state->YawAcc << ' ' <<
			m_state->Northing_rear() << ' ' <<                //Northing position of center of *REAR* axle
			m_state->Easting_rear() << ' ' <<                 //Easting position of center of *REAR* axle
			m_state->Vel_N_rear() << ' ' <<                   //1st diff of Northing at center of *REAR* axle
			m_state->Vel_E_rear() << ' '                  //1st diff of Easting at center of *REAR* axle

    /** and now for the adrive variables **/
<< m_state->Timestamp << ' '



<<  m_actuator->m_steerstatus << ' ' << m_actuator->m_steerpos << ' ' << m_actuator->m_steercmd << ' '
	  
	<<  m_actuator->m_gasstatus << ' ' << m_actuator->m_gaspos << ' ' << m_actuator->m_gascmd << ' '
	  
	  <<  m_actuator->m_brakestatus << ' ' << m_actuator->m_brakepos << ' ' << m_actuator->m_brakecmd << ' ' << m_actuator->m_brakepressure <<' '
	  
	 <<  m_actuator->m_estopstatus << ' ' << m_actuator->m_estoppos << ' '

	 <<  m_actuator->m_transstatus << ' ' << m_actuator->m_transpos << ' ' << m_actuator->m_transcmd << ' '
	  
	 <<  m_actuator->m_obdiistatus << ' ' << m_actuator->m_engineRPM << ' ' << m_actuator->m_TimeSinceEngineStart << ' ' << m_actuator->m_VehicleWheelSpeed <<' '
         <<  m_actuator->m_EngineCoolantTemp << ' ' << m_actuator->m_WheelForce << ' ' << m_actuator->m_GlowPlugLampTime << ' ' << m_actuator->m_CurrentGearRatio
<<m_refIndex << ' '
<<m_traj.getEastingDiff(m_refIndex,0)<<' '<<m_traj.getEastingDiff(m_refIndex,1) << ' ' << m_traj.getEastingDiff(m_refIndex,2)<< ' '<<m_traj.getNorthingDiff(m_refIndex,0)<< ' ' <<m_traj.getNorthingDiff(m_refIndex,1)<< ' ' <<m_traj.getNorthingDiff(m_refIndex,2)

    // current traj point

    //more ff terms
<< ' ' <<  m_pitchFF << ' ' << m_speedFF

<<endl;
  }


}



/*
 *THROW exception method for STRINGS 
 */
void CPID_Controller::conditionalStringThrow( const bool exception_condition, const string string_to_throw ) {

  if ( exception_condition == true ) {

    //Update the exception_error_flag to TRUE to indicate that an
    //exception has occurred
    exception_error_flag = true;

    //The exception condition was TRUE - hence an exception has occurred
    //therefore hrow the string passed in the method (called string_to_throw)
    throw std::string(string_to_throw);
    
    //REMEMBER - NO CODE AFTER THE THROW WILL BE EXCECUTED - EVER!

  }

}



/*
 *Function: setup_Output
 *Action: initializes all the streams necessary to log data
 */

void CPID_Controller::setup_Output(string logs_location)
{
  //logs_location = getLogDir();

  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);


  char logsFileName[FILE_NAME_LENGTH];
  char varsFileName[FILE_NAME_LENGTH];
  char testFileName[FILE_NAME_LENGTH];

  char logsFilePath[FILE_NAME_LENGTH];
  char varsFilePath[FILE_NAME_LENGTH];
  char testFilePath[FILE_NAME_LENGTH];


  char lnCmd[FILE_NAME_LENGTH];

  //this creates the output file names as a function of time called  
  sprintf(varsFileName, "vars.dat");
  sprintf(logsFileName, "logs.dat");
  sprintf(testFileName, "test.dat");



  string temp;



  temp = logs_location + varsFileName;
  strcpy(varsFilePath, temp.c_str());

  temp = logs_location + logsFileName;
  strcpy(logsFilePath, temp.c_str());

  temp = logs_location + testFileName;
  strcpy(testFilePath, temp.c_str());

  /** Closes the streams if necessary */
    if(m_outputVars.is_open()) 
    {
      m_outputVars.close();
    }
   if(m_outputLogs.is_open()) 
    {
      m_outputLogs.close();
    }
  
   if(m_outputTest.is_open()) 
    {
      m_outputTest.close();
    }


  /** opens the files so that functions can write to them */
  m_outputVars.open(varsFilePath);
  m_outputLogs.open(logsFilePath);
  m_outputTest.open(testFilePath);

  // make state.dat, angle.dat,yerror.dat,waypoint.dat link to newet files
  //I'm not sure how/why it works, but I copied it from Dima, and it seems to work perfectly
  lnCmd[0] = '\0';
  strcat(lnCmd, "cd logs; ln -fs ");
  strcat(lnCmd, testFileName);
  strcat(lnCmd, " test.dat; cd ..");
  system(lnCmd);

  m_outputVars <<setprecision(20);
  m_outputTest <<setprecision(20);
  m_outputLogs << setprecision(20);
}



/*
 *Function: output_Debug
 *Action: gathers all the debug messages in one place. This is only 
 *   useful for messages meant to allow people to check the values
 *   of certain member variables while it's running. 
 *   Any output for the purpose of seeing if a particular point in 
 *   the code has been reached will have to be moved back.
 */

void CPID_Controller::output_Debug()
{
  if(DEBUG_LEVEL > 0) 
  {
    printf("At %s:%d\n", __FILE__, __LINE__);
    printf("Yerror[FRONT, REAR] = [%f, %f]\n", m_Yerror[FRONT], m_Yerror[REAR] );
    printf("Aerror[FRONT, REAR] = [%f, %f] DEGREES\n", 
           m_Aerror[FRONT]*RAD_TO_DEGREE, m_Aerror[REAR]*RAD_TO_DEGREE );
    printf("Cerror = %f\n", m_Cerror );

    cout << "REAR veh diff N" << ' ' <<m_vehDN[REAR] << endl;
    cout << "REAR veh diff E" << ' ' <<m_vehDE[REAR] << endl;
 
    printf("REAR REF (N, E) = (%f, %f)\n", m_refN[REAR], m_refE[REAR] );
    printf("REAR REF (Nd, Ed) = (%f, %f)\n", m_refDN[REAR], m_refDE[REAR] );
    printf("REAR REF (Ndd, Edd) = (%f, %f)\n", m_refDDN_REAR, m_refDDE_REAR );
    printf("REAR REF HEADING = %f deg\n", m_refAngle[REAR]*RAD_TO_DEGREE );

  }

  if( DEBUG_LEVEL > 1 )
  {
    cout << "m_refIndex: " << m_refIndex << endl;

    cout<<"accel Cmd, FF, FB: "<<m_accelCmd<<' '<<m_AccelFF<<' '<<m_AccelFB<<endl;

    cout<<"FRONT yerror, aerror: "<<m_Yerror[FRONT]<<' '<<m_Aerror[FRONT]<<endl;
    cout<<"REAR yerror, aerror: "<<m_Yerror[REAR]<<' '<<m_Aerror[REAR]<<endl;

    cout << "FRONT veh diff N" << ' ' <<m_vehDN[FRONT] << endl;
    cout << "FRONT veh diff E" << ' ' <<m_vehDE[FRONT] << endl;

    printf("FRONT REF (N, E) = (%f, %f)\n", m_refN[FRONT], m_refE[FRONT] );
    printf("FRONT REF (Nd, Ed) = (%f, %f)\n", m_refDN[FRONT], m_refDE[FRONT] );
    printf("FRONT REF HEADING = %f deg\n", m_refAngle[FRONT]*180.0/M_PI );

    // note that the output is in RADIANS
    printf("(m_steerCmd, m_SteerFF, m_SteerFB) = (%f, %f, %f)\n", 
             m_steerCmd, m_SteerFF, m_SteerFB);

    cout<<"N ddot, E ddot and Accel FF: "<<m_refDDN_REAR<<' '<<m_refDDE_REAR
        <<' '<<m_AccelFF<<' '<<m_AccelFF<<endl;

  }

}
