#include "stereoProcess.hh"
#include <fstream>
#include "profiler.hh"

stereoProcess::stereoProcess() {
  _pairIndex = -1;
  memset(_currentFilename, 0, 256);
  memset(_currentFileType, 0, 10);

  showingDisp = FALSE;
  showingRectLeft = FALSE;
  showingRectRight = FALSE;

  DGCcreateMutex(&_paramsMutex);  
}


stereoProcess::~stereoProcess() {
}


int stereoProcess::init(int verboseLevel, char* SVSCalFilename, char* SVSParamsFilename, char* CamParamsFilename, 
			char *baseFilename, char *baseFileType, int num) {
  DGClockMutex(&_paramsMutex);
  _verboseLevel = verboseLevel;
  memcpy(_currentFilename, baseFilename, strlen(baseFilename));
  memcpy(_currentFileType, baseFileType, strlen(baseFileType));
  _pairIndex = num;

  //Initialize the SVS variables
  videoObject = new svsStoredImages();
  processObject = new svsMultiProcess();

  //Load the internal calibration data
  FILE *svsCalFile = NULL;
  svsCalFile = fopen(SVSCalFilename, "r");
  if(svsCalFile != NULL) {
    fclose(svsCalFile);
    videoObject->ReadParams(SVSCalFilename);
    videoObject->Start();
  } else {
    return stereoProcess_NO_FILE;
  }

  //Load the SVS parameter data
  //Variables for getting the SVS parameters
  int corrsize, thresh, ndisp, offx, svsUnique, multi;

  //Variables for resizing the image
  int width, height;
  //Variables for using a sub-window of the resized image
  int sub_window_x_offset, sub_window_y_offset, sub_window_width, sub_window_height, use_sub_window_temp;
  bool use_sub_window;
  FILE *svsParamsFile = NULL;

  svsParamsFile = fopen(SVSParamsFilename, "r");
  
  if(svsParamsFile != NULL) {
    fscanf(svsParamsFile, "corrsize: %d\nthresh: %d\nndisp: %d\noffx: %d\nunique: %d\nwidth: %d\nheight: %d\nminpoints: %d\nuse_sub_window: %d\nsub_window_x_offset: %d\nsub_window_y_offset: %d\nsub_window_width: %d\nsub_window_height: %d\nmulti: %d\nblobActive: %d\nblobTresh: %d\nblobDispTol: %d", &corrsize, &thresh, &ndisp, &offx, &svsUnique, &width, &height, &numMinPoints, &use_sub_window_temp, &sub_window_x_offset, &sub_window_y_offset, &sub_window_width, &sub_window_height, &multi, &m_blobActive, &m_blobTresh, &m_blobDispTol);
    
    if(use_sub_window_temp == 1) {
      use_sub_window = TRUE;
    } else {
      use_sub_window = FALSE;
    }

    videoObject->Load(0, 0, NULL, NULL, NULL, NULL, FALSE, FALSE);
    imageObject = videoObject->GetImage(1);
    
    imageObject->dp.corrsize = corrsize;
    imageObject->dp.thresh = thresh;
    imageObject->dp.ndisp = (int)ndisp;
    imageObject->dp.offx = offx;
    imageObject->dp.unique = svsUnique;

    processObject->doIt = multi;

    imageSize = cvSize(width, height);
    
    subWindow = cvRect(sub_window_x_offset, sub_window_y_offset, sub_window_width, sub_window_height);
    fclose(svsParamsFile);
  } else {
    return stereoSource_NO_FILE;
  }

  //Initialize the frame variable
  double cameraRoll, cameraPitch, cameraYaw;
  XYZcoord cameraOffset(0, 0, 0);
  FILE *camParamsFile = NULL;

  camParamsFile = fopen(CamParamsFilename, "r");

  if(camParamsFile != NULL) {
    fscanf(camParamsFile, "x: %lf\ny: %lf\nz: %lf\npitch: %lf\nroll: %lf\nyaw: %lf",
	   &cameraOffset.X, &cameraOffset.Y, &cameraOffset.Z,
	   &cameraPitch, &cameraRoll, &cameraYaw);
    
    cameraFrame.initFrames(cameraOffset, cameraPitch, cameraRoll, cameraYaw);
    
    fclose(camParamsFile);
  } else {
    return stereoSource_NO_FILE;
  }

  for(int i=0; i < MAX_CAMERAS; i++) {
    tempImages[i] = cvCreateImage(imageSize, IPL_DEPTH_8U, 1);
    stereoImages[i] = cvCreateImage(imageSize, IPL_DEPTH_8U, 1);
  }

  DGCunlockMutex(&_paramsMutex);

  return stereoSource_OK;
}


int stereoProcess::loadPair(stereoPair pair, VehicleState state) {
  return loadPair(pair.left, pair.right, state);
}

int stereoProcess::loadPair(unsigned char *left, unsigned char *right, VehicleState state) {

  cvSetData(tempImages[LEFT_CAM], (char *)left, imageSize.width);
  cvSetData(tempImages[RIGHT_CAM], (char *)right, imageSize.width);


  for(int i=0; i<MAX_CAMERAS; i++) {
    cvCopy(tempImages[i], stereoImages[i]);
  }

  videoObject->Load(stereoImages[LEFT_CAM]->width, stereoImages[LEFT_CAM]->height,
		    (unsigned char*) stereoImages[LEFT_CAM]->imageData,
		    (unsigned char*) stereoImages[RIGHT_CAM]->imageData,
		    NULL, NULL,
		    FALSE, FALSE);

  _currentState = state;
  NEDcoord camPos(_currentState.Northing, _currentState.Easting, _currentState.Altitude);
  cameraFrame.updateState(camPos, _currentState.Pitch, _currentState.Roll, _currentState.Yaw);

  _pairIndex++;

  return stereoProcess_OK;
}


int stereoProcess::calcRect() {

  imageObject = videoObject->GetImage(1); //this line of code is beautiful
   
  /*
    videoObject->Load(imageObject->sp.linelen, subWindow.height,
    imageObject->Left() + imageObject->sp.linelen*subWindow.y,
    imageObject->Right() + imageObject->sp.linelen*subWindow.y,
    NULL, NULL,
    FALSE, FALSE); 
 
    imageObject = videoObject->GetImage(1);

    svsSP *mySP;

    mySP = imageObject->GetSP();
    mySP->subwarp = TRUE;
    mySP->subwindow = TRUE;
    mySP->left_ix = subWindow.x;
    mySP->left_iy = subWindow.y;
    mySP->left_width = subWindow.width;
    mySP->left_height = subWindow.height;
    mySP->right_ix = subWindow.x;
    mySP->right_iy = subWindow.y;
    mySP->right_width = subWindow.width;
    mySP->right_height = subWindow.height;
    mySP->linelen = imageSize.width;
    mySP->lines = imageSize.height;

    imageObject->SetSP(mySP);
  */
  if(showingRectLeft) {
    fltk_check();
    rectLeftWindow->DrawImage(imageObject, svsLEFT);
    if(!rectLeftWindow->visible_r()) showingRectLeft = FALSE;
  }
  if(showingRectRight) {
    fltk_check();
    rectRightWindow->DrawImage(imageObject, svsRIGHT);
    if(!rectRightWindow->visible_r()) showingRectRight = FALSE;
  }

  return stereoProcess_OK;
}


int stereoProcess::calcDisp() {
  DGClockMutex(&_paramsMutex);
  
  processObject->CalcStereo(imageObject);
  DGCunlockMutex(&_paramsMutex);
  
  // blob filter starts here
  if (m_blobActive == 1) {
#warning stereoprocess.cc:blobfilter image width and heigth should be set with respect to any subwindowing, not hardcoded!
    
    //    CProfile p("blob filter version_3");
    dispIm = imageObject->Disparity();
    memset(c,0,sizeof(c));
    int k = 1; //current color
    vector<int> count;
    count.push_back(m_blobTresh); // don't remove color 0 == no disparity
    //ofstream tempFile("/tmp/blobflood.txt", ios::app);      
    
    for (int pixel = 0; pixel < 307200; pixel++) {
      //tempFile << "pixel: " << pixel << endl;
      if (dispIm[pixel] > -1) {
	if (c[pixel] == 0) {
	  //c[pixel] = k;
	  //  tempFile << "flooding pixel " << pixel << endl;
	  flood(pixel,k,dispIm);
	  k++;
	  count.push_back(0);
	}
	else {
	  count.at(c[pixel])++;
	}
      }
    }
    
    for (int pixel = 0; pixel < 307200; pixel++) {
      if (count.at(c[pixel]) < m_blobTresh) {
	dispIm[pixel] = -1;
      }
    }
    //tempFile.close();
  }    
  // blob filter ends here

  if(showingDisp) {
    fltk_check();
    dispWindow->DrawImage(imageObject, svsDISPARITY);
    if(!dispWindow->visible_r()) showingDisp = FALSE;
  }
  
  return stereoProcess_OK;
}

  
int stereoProcess::save() {
  int saveRectStatus = saveRect();
  int saveDispStatus = saveDisp();
  if(saveRectStatus!=stereoProcess_OK) {
    return saveRectStatus;
  }
  if(saveDispStatus!=stereoProcess_OK) {
    return saveDispStatus;
  }

  return stereoProcess_OK;
}


int stereoProcess::save(char *baseFilename, char *baseFileType, int num) {
  int saveRectStatus = saveRect(baseFilename, baseFileType, num);
  int saveDispStatus = saveDisp(baseFilename, baseFileType, num);
  if(saveRectStatus!=stereoProcess_OK) {
    return saveRectStatus;
  }
  if(saveDispStatus!=stereoProcess_OK) {
    return saveDispStatus;
  }

  return stereoProcess_OK;
}


int stereoProcess::saveDisp() {
  return saveDisp(_currentFilename, _currentFileType, _pairIndex);
}


int stereoProcess::saveDisp(char *baseFilename, char *baseFileType, int num) {
  char fullFilename[256];

  sprintf(fullFilename, "%s%.4dDisp.%s", baseFilename, num, baseFileType);

  imageObject->SaveDisparity(fullFilename, TRUE);

  return stereoProcess_OK;
}


int stereoProcess::saveRect() {
  return saveRect(_currentFilename, _currentFileType, _pairIndex);
}


int stereoProcess::saveRect(char *baseFilename, char *baseFileType, int num) {
  char fullFilename[256];

  sprintf(fullFilename, "%s%.4dRect", baseFilename, num);

  imageObject->SaveToFile(fullFilename);
    
  return stereoProcess_OK;
}



int stereoProcess::show(int width, int height) {
  showRect(width, height);
  showDisp(width, height);
  return stereoProcess_OK;
}


int stereoProcess::showDisp(int width, int height) {
  if(width == 0) width = imageSize.width;
  if(height == 0) height = subWindow.height;

  if(!showingDisp) {
    dispWindow = new svsWindow(100,100,width,height);
    showingDisp = TRUE;
  }
  dispWindow->show();

  return stereoProcess_OK;
}


int stereoProcess::showRect(int width, int height) {
  int showRectLeftStatus = showRectLeft();
  int showRectRightStatus = showRectRight();
  if(showRectLeftStatus!=stereoProcess_OK) {
    return showRectLeftStatus;
  }
  if(showRectRightStatus!=stereoProcess_OK) {
    return showRectRightStatus;
  }

  return stereoProcess_OK;
}


int stereoProcess::showRectLeft(int width, int height) {
  if(!showingRectLeft) {
    rectLeftWindow = new svsWindow(0,0, imageSize.width, subWindow.height);
    showingRectLeft = TRUE;
  }
  rectLeftWindow->show();

  return stereoProcess_OK;
}


int stereoProcess::showRectRight(int width, int height) {
  if(!showingRectRight) {
    rectRightWindow = new svsWindow(50, 50, imageSize.width, subWindow.height);
    showingRectRight = TRUE;
  }
  rectRightWindow->show();

  return stereoProcess_OK;
}


unsigned char* stereoProcess::disp() {
  fprintf(stderr, "stereoProcess::disp() not yet implemented\n");
  return NULL;
}


unsigned char* stereoProcess::rectLeft() {
  fprintf(stderr, "stereoProcess::rectLeft() not yet implemented\n");
  return NULL;
}


unsigned char* stereoProcess::rectRight() {
  fprintf(stderr, "stereoProcess::rectRight() not yet implemented\n");
  return NULL;
}


stereoPair stereoProcess::rectPair() {
  stereoPair temp;
  temp.left = rectLeft();
  temp.right = rectRight();
  return temp;
}


int stereoProcess::numPoints() {
  return imageObject->numPoints;
}


bool stereoProcess::validPoint(int i) {
  if(i < numPoints() && i >= 0) {
    return (imageObject->pts3D[i].A == 1); // since svs 4.1d, A is no longer a float
  } 
  else {
    return false;
  }
}


NEDcoord stereoProcess::UTMPoint(int i) {
  NEDcoord retVal(0, 0, 0);
  XYZcoord cameraPoint;

  if(validPoint(i)) {
    //According to the SVS documentation:
    //The camera's Z-axis is forward - so our X-axis
    cameraPoint.X = imageObject->pts3D[i].Z;
    //The camera's X axis is right - so our Y-axis
    cameraPoint.Y = imageObject->pts3D[i].X; 
    //The camera's Y-axis is down - so our Z-axis
    cameraPoint.Z = imageObject->pts3D[i].Y;
    retVal = cameraFrame.transformS2N(cameraPoint);
  }

  return retVal;
}


XYZcoord stereoProcess::Point(int i) {
  XYZcoord retVal(0, 0, 0);

  if(validPoint(i)) {
    retVal.X = imageObject->pts3D[i].X;
    retVal.Y = imageObject->pts3D[i].Y;
    retVal.Z = imageObject->pts3D[i].Z;

  }

  return retVal;
}


bool stereoProcess::SinglePoint(int x, int y, NEDcoord* resultPoint) {
  XYZcoord tempPoint(0, 0, 0);
  NEDcoord transformedPoint(0, 0, 0);

  if(x > 0 && x < stereoImages[0]->width &&
     y > 0 && y < stereoImages[0]->height) {
    /*
      ofstream tempFile("/tmp/singlePoint.txt", ios::app);
      tempFile << "x is " << x << " and y is " << y << endl;
      tempFile.close();
    */
    
    if(processObject->CalcPoint3D(x, y, imageObject, &tempPoint.Y, &tempPoint.Z, &tempPoint.X)) {
      
      tempPoint.X = tempPoint.X; // I will remove this line, and the two following, unless someone explains what they do...
      tempPoint.Y = tempPoint.Y;
      tempPoint.Z = tempPoint.Z;

      // the svs documentation is incorrect! calcpoint3d returns true for invalid points, so we need to check ourselves.
      if (tempPoint.X <= 0) { // allow no points behind the cameras 
	return false;
      }

      transformedPoint = cameraFrame.transformS2N(tempPoint);

      
      (resultPoint->N) = transformedPoint.N;
      (resultPoint->E) = transformedPoint.E;
      (resultPoint->D) = transformedPoint.D;
      
      return true;
    }
  }

  return false;
}


bool stereoProcess::calc3D() {
  //return processObject->Calc3D(imageObject, subWindow.x, subWindow.y, subWindow.width, subWindow.height);
  return processObject->Calc3D(imageObject);
}


bool stereoProcess::SingleRelativePoint(int x, int y, XYZcoord* resultPoint) {
  XYZcoord tempPoint(0, 0, 0);
  XYZcoord transformedPoint(0, 0, 0);

  if(x > 0 && x < stereoImages[0]->width &&
     y > 0 && y < stereoImages[0]->height) {
    /*
      ofstream tempFile("/tmp/singleRelativePoint.txt", ios::app);
      tempFile << "x is " << x << " and y is " << y << endl;
      tempFile.close();
    */

    if(processObject->CalcPoint3D(x, y, imageObject, &tempPoint.Y, &tempPoint.Z, &tempPoint.X)) {

      tempPoint.X = tempPoint.X;
      tempPoint.Y = tempPoint.Y;
      tempPoint.Z = tempPoint.Z;
      
      // the svs documentation is incorrect! calcpoint3d returns true for invalid points, so we need to check ourselves.
      if (tempPoint.X <= 0) { // allow no points behind the cameras 
	return false;
      }
      
      transformedPoint = cameraFrame.transformS2B(tempPoint);
      
      (resultPoint->X) = transformedPoint.X;
      (resultPoint->Y) = transformedPoint.Y;
      (resultPoint->Z) = transformedPoint.Z;
      
      return true;
    } 
  }

  return false;
}

VehicleState stereoProcess::currentState() {
  return _currentState;
}


int stereoProcess::pairIndex() {
  return _pairIndex;
}


int stereoProcess::resetPairIndex() {
  _pairIndex = 0;

  return stereoProcess_OK;
}


int stereoProcess::corrsize() {
  return imageObject->dp.corrsize;
}


int stereoProcess::thresh() {
  return imageObject->dp.thresh;
}


int stereoProcess::ndisp() {
  return imageObject->dp.ndisp;
}


int stereoProcess::offx() {
  return imageObject->dp.offx;
}


int stereoProcess::svsUnique() {
  return imageObject->dp.unique;
}

int stereoProcess::multi() {
  return processObject->doIt;
}


int stereoProcess::setSVSParams(int corrsize, int thresh, int ndisp, int offx, int svsUnique, int multi, int subwindow_x_off, int subwindow_y_off, int subwindow_width, int subwindow_height) {
  DGClockMutex(&_paramsMutex);

  if(corrsize != -1) imageObject->dp.corrsize = corrsize;
  if(thresh != -1) imageObject->dp.thresh = thresh;
  if(ndisp != -1) imageObject->dp.ndisp = ndisp;
  if(offx != -1) imageObject->dp.offx = offx;
  if(svsUnique != -1) imageObject->dp.unique = svsUnique;
  if(multi != -1) processObject->doIt = (multi!=0);

  if(subwindow_x_off != -1) subWindow.x = subwindow_x_off;
  if(subwindow_y_off != -1) subWindow.y = subwindow_y_off;
  if(subwindow_width != -1) subWindow.width = subwindow_width;
  if(subwindow_height != -1) subWindow.height = subwindow_height;

  DGCunlockMutex(&_paramsMutex);
  return stereoProcess_OK;
}

int stereoProcess::blobActive() {
  return m_blobActive;
}

int stereoProcess::blobTresh() {
  return m_blobTresh;
}

int stereoProcess::blobDispTol() {
  return m_blobDispTol;
}

int stereoProcess::setBlobParams(int blobActive, int blobTresh, int blobDispTol) {
  m_blobActive = blobActive;
  m_blobTresh = blobTresh;
  m_blobDispTol = blobDispTol;
  return stereoProcess_OK;
}

void stereoProcess::flood(int pixel, int k, short* dispIm) {
  //ofstream tempFile("/tmp/blobflood.txt", ios::app);
  deque<int> q;
  c[pixel] = k;
  q.push_front(pixel);
  //tempFile << "push_front of pixel " << pixel << endl;
  while(!q.empty()) {
    pixel = q.front();
    q.pop_front();
    //tempFile << "pop front of pixel " << pixel << endl;
    int m = dispIm[pixel];
    
    //up
    if ((pixel>639) && (c[pixel-640]==0) && (dispIm[pixel-640] > -1) && (abs(dispIm[pixel-640]-m) < m_blobDispTol)) { 
      c[pixel-640] = k;
      q.push_front(pixel-640);
      //tempFile << "push_front of pixel " << pixel-640 << endl;
    }
    
    //left
    if (((pixel%640) > 0) && (c[pixel-1]==0) && (dispIm[pixel-1] > -1) && (abs(dispIm[pixel-1]-m) < m_blobDispTol)) {
      c[pixel-1] = k;
      q.push_front(pixel-1);
      //tempFile << "push_front of pixel " << pixel-1 << endl;

    }

    //down 
    if ((pixel<306560) && (c[pixel+640]==0) && (dispIm[pixel+640] > -1) && (abs(dispIm[pixel+640]-m) < m_blobDispTol)) { 
      c[pixel+640] = k;
      q.push_front(pixel+640);
      //tempFile << "push_front of pixel " << pixel+640 << endl;
    }

    //right
    if (((pixel%640) < 639) && (c[pixel+1]==0) && (dispIm[pixel+1] > -1) && (abs(dispIm[pixel+1]-m) < m_blobDispTol)) { 
      c[pixel+1] = k;
      q.push_front(pixel+1);
      //tempFile << "push_front of pixel " << pixel+1 << endl;
    }
    
  }
  //tempFile << "Blob number " << k << " complete" << endl;
}
