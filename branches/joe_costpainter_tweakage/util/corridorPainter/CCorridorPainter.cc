#include "CCorridorPainter.hh"

CCorridorPainter::CCorridorPainter() {
  _inputMap = NULL;
  _inputRDDF = NULL;

  _prevBottomLeftRowMultiple = 0;
  _prevBottomLeftColMultiple = 0;

  _behindWaypointNum = 0;
  _aheadWaypointNum = 0;

  _useDelta = 0;
  
  _lastUsedWayptNumberRowBox = 0;
  _lastUsedWayptNumberColBox = 0;
  
  _startingIndices = NULL;
  _endingIndices = NULL;
  _tracklineStartingIndices = NULL;
  _tracklineEndingIndices = NULL;

  _startRow = _endRow = 0;

  _painterOpts.optRDDFscaling = 1.0;
  _painterOpts.optMaxSpeed = 999.0;
  _painterOpts.paintCenterTrackline = CCorridorPainterOptions::DONT;
  _painterOpts.whatToAdjust = CCorridorPainterOptions::RDDF;
  _painterOpts.adjustmentVal = 1.0;
}


CCorridorPainter::~CCorridorPainter() {

}


int CCorridorPainter::initPainter(CMap* inputMap, 
				  int layerNum, 
				  RDDF* inputRDDF, 
				  double insideCorridorVal, double outsideCorridorVal,
				  double tracklineVal, double edgeVal, double percentSwitch, int useDelta) {
  _inputMap = inputMap;
  _layerNum = layerNum;
  _inputRDDF = inputRDDF;

  _insideCorridorVal = insideCorridorVal;
  _outsideCorridorVal = outsideCorridorVal;
  _tracklineVal = tracklineVal;
  _edgeVal = edgeVal;
  _percentSwitch = percentSwitch;


  _prevBottomLeftRowMultiple = _inputMap->getWindowBottomLeftUTMNorthingRowResMultiple();
  _prevBottomLeftColMultiple = _inputMap->getWindowBottomLeftUTMEastingColResMultiple();
  
  _behindWaypointNum = 0;
  _aheadWaypointNum = 0; 
  
  _useDelta = useDelta;

  _startingIndices = new int[inputMap->getNumRows()];
  _endingIndices = new int[inputMap->getNumRows()];
  _tracklineStartingIndices = new int[inputMap->getNumRows()];
  _tracklineEndingIndices = new int[inputMap->getNumCols()];

  return 0;
}


#ifdef CP_OBS
void CCorridorPainter::paintChangesV2(NEcoord rowBox[], NEcoord colBox[], double RDDFscaling, double maxSpeed) {
  int row, col;
  int currentWaypoint;
  double waypointBottomLeftNorthing, waypointBottomLeftEasting;
  double waypointTopRightNorthing, waypointTopRightEasting;
  double Northing, Easting;
  NEcoord cellTopLeft, cellTopRight, cellBottomLeft, cellBottomRight;

  int prevWaypointNum[3];
  int nextWaypointNum[3];

  prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindDist(_inputMap->getVehLocUTMNorthing(),
                                                            _inputMap->getVehLocUTMEasting(),
                                                            1000);
  nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadDist(_inputMap->getVehLocUTMNorthing(),
                                                           _inputMap->getVehLocUTMEasting(),
                                                           1000) - 1;

  double boxBottomLeftUTMNorthing[2];
  double boxBottomLeftUTMEasting[2];
  double boxTopRightUTMNorthing[2];
  double boxTopRightUTMEasting[2];

  int corridorBoxBottomLeftRow;
  int corridorBoxBottomLeftCol;
  int corridorBoxTopRightRow;
  int corridorBoxTopRightCol;

  boxBottomLeftUTMNorthing[0] = rowBox[0].N;
  boxBottomLeftUTMEasting[0] = rowBox[0].E;
  boxTopRightUTMNorthing[0] = rowBox[2].N;
  boxTopRightUTMEasting[0] = rowBox[2].E;

  boxBottomLeftUTMNorthing[1] = colBox[0].N;
  boxBottomLeftUTMEasting[1] = colBox[0].E;
  boxTopRightUTMNorthing[1] = colBox[2].N;
  boxTopRightUTMEasting[1] = colBox[2].E;
  

  for(int i=0; i<2; i++) {
    //printf("[%d] %s: \n", __LINE__, __FILE__);
    //get the waypoints for each box  
    //printf("Box %d: (%d, %d) to (%d, %d)\n", i,
    //boxBottomLeftRow[i], boxBottomLeftCol[i],
    //boxTopRightRow[i], boxTopRightCol[i]);
    /*
      _inputMap->Win2UTM(boxBottomLeftRow[i], boxBottomLeftCol[i],
      &(boxBottomLeftUTMNorthing[i]), &(boxBottomLeftUTMEasting[i]));
      _inputMap->Win2UTM(boxTopRightRow[i], boxTopRightCol[i],
      &(boxTopRightUTMNorthing[i]), &(boxTopRightUTMEasting[i]));
    */
    //printf("Drawing from %d to %d\n", prevWaypointNum[0], nextWaypointNum[0]);
    /*
      prevWaypointNum[i] = _inputRDDF->getWaypointNumBehindOut(currentWaypoint,
      boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
      boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
      nextWaypointNum[i] = _inputRDDF->getWaypointNumAheadOut(currentWaypoint,
      boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
      boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
    */
    //for(int j=3; j<4; j++) {
    for(int j=prevWaypointNum[0]; j <= nextWaypointNum[0]; j++) { 
      //get coords for waypt box
      int temp = _inputRDDF->getCorridorSegmentBoundingBoxUTM(j, 
                                                              boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
                                                              boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i],
                                                              waypointBottomLeftNorthing, waypointBottomLeftEasting,
                                                              waypointTopRightNorthing, waypointTopRightEasting);

      _inputMap->UTM2Win(waypointBottomLeftNorthing, waypointBottomLeftEasting,
                         &corridorBoxBottomLeftRow, &corridorBoxBottomLeftCol);
      _inputMap->UTM2Win(waypointTopRightNorthing, waypointTopRightEasting,
                         &corridorBoxTopRightRow, &corridorBoxTopRightCol);

      // force the values into range
      corridorBoxBottomLeftRow = max(corridorBoxBottomLeftRow-2, 0);
      corridorBoxBottomLeftCol = max(corridorBoxBottomLeftCol-2, 0);
      corridorBoxTopRightRow   = min(corridorBoxTopRightRow+2, _inputMap->getNumRows()-1);
      corridorBoxTopRightCol   = min(corridorBoxTopRightCol+2, _inputMap->getNumCols()-1);
      /*  
        printf("%d: (%d=%lf, %d=%lf) to (%d=%lf, %d=%lf)\n", j,
        corridorBoxBottomLeftRow, waypointBottomLeftNorthing,
        corridorBoxBottomLeftCol, waypointBottomLeftEasting,
        corridorBoxTopRightRow, waypointTopRightNorthing,
        corridorBoxTopRightCol, waypointTopRightEasting);
      */
      //printf("[%d] %s: \n", __LINE__, __FILE__);
      if(temp >= 0) {
        for(row=corridorBoxBottomLeftRow; row <= corridorBoxTopRightRow; row++) {
          for(col=corridorBoxBottomLeftCol; col <= corridorBoxTopRightCol; col++) {
            if(_inputMap->getDataWin<double>(_layerNum, row, col) == _inputMap->getLayerNoDataVal<double>(_layerNum) ||
	       _inputMap->getDataWin<double>(_layerNum, row, col) == _outsideCorridorVal) {
	      //if(_inputMap->getDataWin<double>(_layerNum, row, col) == _outsideCorridorVal) {
              _inputMap->Win2UTM(row, col, &(cellBottomLeft.N), &(cellBottomLeft.E));
	      cellTopLeft.N = cellBottomLeft.N + _inputMap->getResRows();
	      cellTopRight.N = cellBottomLeft.N + _inputMap->getResRows();
	      cellBottomRight.N = cellBottomLeft.N;

	      cellBottomRight.E = cellBottomLeft.E + _inputMap->getResCols();
	      cellTopRight.E = cellBottomLeft.E + _inputMap->getResCols();
	      cellTopLeft.E = cellBottomLeft.E;

	      double tempVal;

              tempVal = _inputRDDF->isPointInCorridor(j, cellBottomLeft.N, cellBottomLeft.E) &
		_inputRDDF->isPointInCorridor(j, cellBottomRight.N, cellBottomRight.E) &
		_inputRDDF->isPointInCorridor(j, cellTopLeft.N, cellTopLeft.E) &
		_inputRDDF->isPointInCorridor(j, cellTopRight.N, cellTopRight.E);

	      //tempVal = _inputRDDF->isPointInCorridor(j, cellBottomLeft.N, cellBottomLeft.E);
              int inNext = 0;
              if(j+1<_inputRDDF->getNumTargetPoints()) {

		inNext = _inputRDDF->isPointInCorridor(j+1, cellBottomLeft.N, cellBottomLeft.E) &
		  _inputRDDF->isPointInCorridor(j+1, cellBottomRight.N, cellBottomRight.E) &
		  _inputRDDF->isPointInCorridor(j+1, cellTopLeft.N, cellTopLeft.E) &
		  _inputRDDF->isPointInCorridor(j+1, cellTopRight.N, cellTopRight.E);

                //inNext = _inputRDDF->isPointInCorridor(j+1, cellBottomLeft.N, cellBottomLeft.E);
              }
              int inPrev = 0;
              if(j-1>0) {

		inPrev = _inputRDDF->isPointInCorridor(j-1, cellBottomLeft.N, cellBottomLeft.E) &
		_inputRDDF->isPointInCorridor(j-1, cellBottomRight.N, cellBottomRight.E) &
		_inputRDDF->isPointInCorridor(j-1, cellTopLeft.N, cellTopLeft.E) &
		_inputRDDF->isPointInCorridor(j-1, cellTopRight.N, cellTopRight.E);

                //inPrev= _inputRDDF->isPointInCorridor(j-1, cellBottomLeft.N, cellBottomLeft.E);
              }

              if(tempVal == 1 || inNext==1 || inPrev==1) {
		double prevSpeed = 0.0;
		double currSpeed = 0.0;
		double nextSpeed = 0.0,  currentVelo;
		if(inPrev) {
		  prevSpeed = _inputRDDF->getWaypointSpeed(j-1);
		}
		if(inNext) {
		  nextSpeed = _inputRDDF->getWaypointSpeed(j+1);
		}
		currSpeed = _inputRDDF->getWaypointSpeed(j);
		currentVelo = _inputMap->getDataWin<double>(_layerNum, row, col);
		if(currentVelo!=_inputMap->getLayerNoDataVal<double>(_layerNum) && 
		   currentVelo!=_inputMap->getLayerOutsideMapVal<double>(_layerNum)) {
		  
		  
		  if(_useDelta == 1){
		    _inputMap->setDataWin_Delta<double>(_layerNum, row, col,
							min(maxSpeed, 
							    RDDFscaling*min(max(max(prevSpeed, nextSpeed), currSpeed), currentVelo)));
		  } else{
		    _inputMap->setDataWin<double>(_layerNum, row, col,
						  min(maxSpeed, 
						      RDDFscaling*min(max(max(prevSpeed, nextSpeed), currSpeed), currentVelo)));		
		  }
		} else {
		  if(_useDelta == 1){
		    _inputMap->setDataWin_Delta<double>(_layerNum, row, col,
							min(maxSpeed,
							    RDDFscaling*max(max(prevSpeed, nextSpeed), currSpeed)));
		  } else{
		    _inputMap->setDataWin<double>(_layerNum, row, col,
						  min(maxSpeed,
						      RDDFscaling*max(max(prevSpeed, nextSpeed), currSpeed)));
		  }
		}
	      } else if(_inputMap->getDataWin<double>(_layerNum,row,col)!=_outsideCorridorVal) {
		if(_useDelta == 1){
		  _inputMap->setDataWin_Delta<double>(_layerNum, row, col,_outsideCorridorVal); 
		} else{
		  _inputMap->setDataWin<double>(_layerNum, row, col,_outsideCorridorVal); 
		}
              }
	      
              //printf("%s [%d]: Setting data! to %lf\n", __FILE__, __LINE__, tempVal);
            }
          }
        }
      }
    }
  }

}


int CCorridorPainter::paintChanges() {
  int row, col;
  int currentWaypoint;
  double waypointBottomLeftNorthing, waypointBottomLeftEasting;
  double waypointTopRightNorthing, waypointTopRightEasting;
  double Northing, Easting;

  int newBottomLeftRowMultiple = _inputMap->getWindowBottomLeftUTMNorthingRowResMultiple();
  int newBottomLeftColMultiple = _inputMap->getWindowBottomLeftUTMEastingColResMultiple();
  int deltaRows, deltaCols;

  //Box 0 is row box
  //Box 1 is col box
  //Box 2 is combo box
  int boxBottomLeftRow[3];
  int boxBottomLeftCol[3];
  int boxTopRightRow[3];
  int boxTopRightCol[3];

  double boxBottomLeftUTMNorthing[3];
  double boxBottomLeftUTMEasting[3];
  double boxTopRightUTMNorthing[3];
  double boxTopRightUTMEasting[3];

  int prevWaypointNum[3];
  int nextWaypointNum[3];

  int corridorBoxBottomLeftRow;
  int corridorBoxBottomLeftCol;
  int corridorBoxTopRightRow;
  int corridorBoxTopRightCol;

  //get the seed
  currentWaypoint = _inputRDDF->getCurrentWaypointNumber(_inputMap->getVehLocUTMNorthing(), _inputMap->getVehLocUTMEasting());
  //printf("Seed was %d\n", currentWaypoint);

  //get the corners
  deltaRows = newBottomLeftRowMultiple - _prevBottomLeftRowMultiple;
  deltaCols = newBottomLeftColMultiple - _prevBottomLeftColMultiple;
  
  if(abs(deltaRows) >= _inputMap->getNumRows() ||
     abs(deltaCols) >= _inputMap->getNumCols()) {
    //printf("Redrawing rddf\n");
    //printf("%s [%d]: \n", __FILE__, __LINE__);
    _inputMap->Win2UTM(0, 0,
		       &(boxBottomLeftUTMNorthing[0]), &(boxBottomLeftUTMEasting[0]));
    _inputMap->Win2UTM(_inputMap->getNumRows()-1, _inputMap->getNumCols()-1,
		       &(boxTopRightUTMNorthing[0]), &(boxTopRightUTMEasting[0]));

    prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000);
    nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000) - 1;
    //printf("Drawing from %d to %d\n", prevWaypointNum[0], nextWaypointNum[0]);

    for(int j=prevWaypointNum[0]; j <= nextWaypointNum[0]; j++) { 
      //get coords for waypt box
      int temp = _inputRDDF->getCorridorSegmentBoundingBoxUTM(j, 
							      boxBottomLeftUTMNorthing[0], boxBottomLeftUTMEasting[0],
							      boxTopRightUTMNorthing[0], boxTopRightUTMEasting[0],
							      waypointBottomLeftNorthing, waypointBottomLeftEasting,
							      waypointTopRightNorthing, waypointTopRightEasting);
      _inputMap->UTM2Win(waypointBottomLeftNorthing, waypointBottomLeftEasting,
			 &corridorBoxBottomLeftRow, &corridorBoxBottomLeftCol);
      _inputMap->UTM2Win(waypointTopRightNorthing, waypointTopRightEasting,
			 &corridorBoxTopRightRow, &corridorBoxTopRightCol);
      if(temp >= 0) {
	for(row=corridorBoxBottomLeftRow; row <= corridorBoxTopRightRow; row++) {
	  for(col=corridorBoxBottomLeftCol; col <= corridorBoxTopRightCol; col++) {
	    if(_inputMap->getDataWin<double>(_layerNum, row, col) != _insideCorridorVal) {
	      _inputMap->Win2UTM(row, col, &Northing, &Easting);
	      double tempVal = _inputRDDF->isPointInCorridor(j, Northing, Easting);
	      if(tempVal == 1) {
		tempVal = _insideCorridorVal;
	      } else {
		tempVal = _outsideCorridorVal;
	      }
	      //printf("%s [%d]: Setting data!\n", __FILE__, __LINE__);
	      if(_useDelta==1) {
		_inputMap->setDataWin_Delta<double>(_layerNum, row, col, 
						    tempVal);
	      } else {
		_inputMap->setDataWin<double>(_layerNum, row, col, 
					      tempVal);
	      }
	    }
	  }
	}
      }
    }

    //after = TVNow();
    //printf("Shift all took %lf, paint took %lf, trans took %lf, set took %lf/%d=%lf\n", 
    //(after-before).dbl(), painting, trans/((double)counter), sets, counter, sets/((double)counter));
  } else {

  if(deltaRows>=0) {
    //we moved north, empty area is at top
    boxBottomLeftRow[2] = _inputMap->getNumRows()-deltaRows;
    boxTopRightRow[2] = _inputMap->getNumRows() - 1;
    boxBottomLeftRow[0] = _inputMap->getNumRows() - deltaRows;
    boxTopRightRow[0] = _inputMap->getNumRows() - 1;
    boxBottomLeftRow[1] = 0;
    boxTopRightRow[1] = _inputMap->getNumRows() - deltaRows;
  } else {
    //moved south, empty at bottom
    boxBottomLeftRow[2] = 0;
    boxTopRightRow[2] = -1*deltaRows;
    boxBottomLeftRow[0] = 0;
    boxTopRightRow[0] = -1*deltaRows;
    boxBottomLeftRow[1] = -1*deltaRows;
    boxTopRightRow[1] = _inputMap->getNumRows() -1;
  }
  if(deltaCols>=0) {
    //we moved east, empty area is at right
    boxBottomLeftCol[2] = _inputMap->getNumCols() - deltaCols;
    boxTopRightCol[2] = _inputMap->getNumCols() -1;

    boxBottomLeftCol[1] = _inputMap->getNumCols() - deltaCols;
    boxTopRightCol[1] = _inputMap->getNumCols() - 1;
    boxBottomLeftCol[0] = 0;
    boxTopRightCol[0] = _inputMap->getNumCols() - deltaCols;
  } else {
    //moved west, empty at left
    boxBottomLeftCol[2] = 0;
    boxTopRightCol[2] = -1*deltaCols;

    boxBottomLeftCol[1] = 0;
    boxTopRightCol[1] = -1*deltaCols;
    boxBottomLeftCol[0] = -1*deltaCols;
    boxTopRightCol[0] = _inputMap->getNumCols()-1;
  }
  /*
  prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindOut(currentWaypoint,
							   _inputMap->getWindowBottomLeftUTMNorthing(),
							   _inputMap->getWindowBottomLeftUTMEasting(),
							   _inputMap->getWindowTopRightUTMNorthing(),
							   _inputMap->getWindowTopRightUTMEasting());
  nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadOut(currentWaypoint,
							  _inputMap->getWindowBottomLeftUTMNorthing(),
							  _inputMap->getWindowBottomLeftUTMEasting(),
							  _inputMap->getWindowTopRightUTMNorthing(),
							  _inputMap->getWindowTopRightUTMEasting());
  */
    prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000);
    nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000) - 1;


  for(int i=0; i<3; i++) {
    //get the waypoints for each box  
    //printf("Box %d: (%d, %d) to (%d, %d)\n", i,
    //boxBottomLeftRow[i], boxBottomLeftCol[i],
    //boxTopRightRow[i], boxTopRightCol[i]);
    _inputMap->Win2UTM(boxBottomLeftRow[i], boxBottomLeftCol[i],
		       &(boxBottomLeftUTMNorthing[i]), &(boxBottomLeftUTMEasting[i]));
    _inputMap->Win2UTM(boxTopRightRow[i], boxTopRightCol[i],
		       &(boxTopRightUTMNorthing[i]), &(boxTopRightUTMEasting[i]));
    
    /*
    prevWaypointNum[i] = _inputRDDF->getWaypointNumBehindOut(currentWaypoint,
							     boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							     boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
    nextWaypointNum[i] = _inputRDDF->getWaypointNumAheadOut(currentWaypoint,
							    boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							    boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
    */
    for(int j=prevWaypointNum[0]; j <= nextWaypointNum[0]; j++) { 
      //get coords for waypt box
      int temp = _inputRDDF->getCorridorSegmentBoundingBoxUTM(j, 
							      boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							      boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i],
							      waypointBottomLeftNorthing, waypointBottomLeftEasting,
							      waypointTopRightNorthing, waypointTopRightEasting);

      _inputMap->UTM2Win(waypointBottomLeftNorthing, waypointBottomLeftEasting,
			 &corridorBoxBottomLeftRow, &corridorBoxBottomLeftCol);
      _inputMap->UTM2Win(waypointTopRightNorthing, waypointTopRightEasting,
			 &corridorBoxTopRightRow, &corridorBoxTopRightCol);
      /*
      printf("%d: (%d=%lf, %d=%lf) to (%d=%lf, %d=%lf)\n", j,
	     corridorBoxBottomLeftRow, waypointBottomLeftNorthing,
	     corridorBoxBottomLeftCol, waypointBottomLeftEasting,
	     corridorBoxTopRightRow, waypointTopRightNorthing,
	     corridorBoxTopRightCol, waypointTopRightEasting);
      */
      if(temp >= 0) {
	for(row=corridorBoxBottomLeftRow; row <= corridorBoxTopRightRow; row++) {
	  for(col=corridorBoxBottomLeftCol; col <= corridorBoxTopRightCol; col++) {
	    _inputMap->Win2UTM(row, col, &Northing, &Easting);
	    if(_inputMap->getDataWin<double>(_layerNum, row, col) != _insideCorridorVal) {
	      double tempVal = _inputRDDF->isPointInCorridor(j, Northing, Easting);
	      if(tempVal == 1) {
		tempVal = _insideCorridorVal;
	      } else {
		tempVal = _outsideCorridorVal;
	      }
	      if(_useDelta==1) {
		_inputMap->setDataWin_Delta<double>(_layerNum, row, col, 
						    tempVal);
	      } else {
		_inputMap->setDataWin<double>(_layerNum, row, col, 
					      tempVal);
	      }
	      //printf("%s [%n]: Setting data!\n", __FILE__, __LINE__);
	    }
	  }
	}
      }
    }
  }
  }

  _prevBottomLeftRowMultiple = newBottomLeftRowMultiple;
  _prevBottomLeftColMultiple = newBottomLeftColMultiple;

  return 0;
}
#endif


#ifdef CP_OBS
int CCorridorPainter::paintChanges_TracklineSlope() {
  int row, col;
  int currentWaypoint;
  double waypointBottomLeftNorthing, waypointBottomLeftEasting;
  double waypointTopRightNorthing, waypointTopRightEasting;
  double Northing, Easting;

  int newBottomLeftRowMultiple = _inputMap->getWindowBottomLeftUTMNorthingRowResMultiple();
  int newBottomLeftColMultiple = _inputMap->getWindowBottomLeftUTMEastingColResMultiple();
  int deltaRows, deltaCols;

  //Box 0 is row box
  //Box 1 is col box
  //Box 2 is combo box
  int boxBottomLeftRow[3];
  int boxBottomLeftCol[3];
  int boxTopRightRow[3];
  int boxTopRightCol[3];

  double boxBottomLeftUTMNorthing[3];
  double boxBottomLeftUTMEasting[3];
  double boxTopRightUTMNorthing[3];
  double boxTopRightUTMEasting[3];

  int prevWaypointNum[3];
  int nextWaypointNum[3];

  int corridorBoxBottomLeftRow;
  int corridorBoxBottomLeftCol;
  int corridorBoxTopRightRow;
  int corridorBoxTopRightCol;

  //get the seed
  currentWaypoint = _inputRDDF->getCurrentWaypointNumber(_inputMap->getVehLocUTMNorthing(), _inputMap->getVehLocUTMEasting());
  //printf("Seed was %d\n", currentWaypoint);

  //get the corners
  deltaRows = newBottomLeftRowMultiple - _prevBottomLeftRowMultiple;
  deltaCols = newBottomLeftColMultiple - _prevBottomLeftColMultiple;


  if(abs(deltaRows) >= _inputMap->getNumRows() ||
     abs(deltaCols) >= _inputMap->getNumCols()) {
    //printf("Redrawing rddf\n");
    _inputMap->Win2UTM(0, 0,
		       &(boxBottomLeftUTMNorthing[0]), &(boxBottomLeftUTMEasting[0]));
    _inputMap->Win2UTM(_inputMap->getNumRows()-1, _inputMap->getNumCols()-1,
		       &(boxTopRightUTMNorthing[0]), &(boxTopRightUTMEasting[0]));

    prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000);
    nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000) - 1;
    //printf("Drawing from %d to %d\n", prevWaypointNum[0], nextWaypointNum[0]);

    for(int j=prevWaypointNum[0]; j <= nextWaypointNum[0]; j++) { 
      //get coords for waypt box
      int temp = _inputRDDF->getCorridorSegmentBoundingBoxUTM(j, 
							      boxBottomLeftUTMNorthing[0], boxBottomLeftUTMEasting[0],
							      boxTopRightUTMNorthing[0], boxTopRightUTMEasting[0],
							      waypointBottomLeftNorthing, waypointBottomLeftEasting,
							      waypointTopRightNorthing, waypointTopRightEasting);
      _inputMap->UTM2Win(waypointBottomLeftNorthing, waypointBottomLeftEasting,
			 &corridorBoxBottomLeftRow, &corridorBoxBottomLeftCol);
      _inputMap->UTM2Win(waypointTopRightNorthing, waypointTopRightEasting,
			 &corridorBoxTopRightRow, &corridorBoxTopRightCol);
      if(temp >= 0) {
	for(row=corridorBoxBottomLeftRow; row <= corridorBoxTopRightRow; row++) {
	  for(col=corridorBoxBottomLeftCol; col <= corridorBoxTopRightCol; col++) {
	    /*
	    printf("outside is %lf, this is %lf\n",
		   _outsideCorridorVal,
		   _inputMap->getDataWin<double>(_layerNum, row, col));
	    */
	    if(_inputMap->getDataWin<double>(_layerNum, row, col) == _outsideCorridorVal) {
	      _inputMap->Win2UTM(row, col, &Northing, &Easting);
	      double tempVal = _inputRDDF->isPointInCorridor(j, Northing, Easting);
	      int inNext = 0;
	      if(j+1<_inputRDDF->getNumTargetPoints()) {
		inNext = _inputRDDF->isPointInCorridor(j+1, Northing, Easting);
	      }
	      int inPrev = 0;
	      if(j-1>0) {
		int inPrev= _inputRDDF->isPointInCorridor(j-1, Northing, Easting);
	      }

	      if(tempVal == 1) {
		//tempVal = _inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting));
		//printf("Ratio is %lf\n", tempVal);
		//printf("tv: %lf, ev %lf, ov: %lf, ps: %lf\n", _tracklineVal, _edgeVal, _outsideCorridorVal, _percentSwitch);
		double ratio = _inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting));
		if((ratio) < (1-_percentSwitch)) {
		  //printf("not steep\n");
		  tempVal = _edgeVal+(_tracklineVal - _edgeVal)*((1-_percentSwitch - ratio)/(1-_percentSwitch));
		} else if(inNext || inPrev){
		  tempVal = _edgeVal+(_tracklineVal - _edgeVal)*((1-_percentSwitch - ratio)/(1-_percentSwitch));
		} else {
		  //printf("Steep\n");
		  tempVal = _outsideCorridorVal + (_edgeVal - _outsideCorridorVal)*(1-ratio)/(_percentSwitch);
		}
		//printf("Ratio is %lf, val is %lf\n", ratio, tempVal);
		//tempVal = ratio;
		//tempVal = _edgeVal + (_tracklineVal - _edgeVal)*(1-_inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting)));
	      } else {
		tempVal = _outsideCorridorVal;
	      }
	      //printf("%s [%d]: Setting data!\n", __FILE__, __LINE__);
	      if(_useDelta==1) {
	      _inputMap->setDataWin_Delta<double>(_layerNum, row, col, 
					 tempVal);
	      } else {
	      _inputMap->setDataWin<double>(_layerNum, row, col, 
					    tempVal);
	      }
	    }
	  }
	}
      }
    }

    //after = TVNow();
    //printf("Shift all took %lf, paint took %lf, trans took %lf, set took %lf/%d=%lf\n", 
    //(after-before).dbl(), painting, trans/((double)counter), sets, counter, sets/((double)counter));
  } else {
    //printf("[%d] %s: deltarows: %d, deltacols %d\n", __LINE__, __FILE__, deltaRows, deltaCols);
  if(deltaRows>=0) {
    //we moved north, empty area is at top
    boxBottomLeftRow[2] = _inputMap->getNumRows()-deltaRows;
    boxTopRightRow[2] = _inputMap->getNumRows() - 1;
    boxBottomLeftRow[0] = _inputMap->getNumRows() - deltaRows;
    boxTopRightRow[0] = _inputMap->getNumRows() - 1;
    boxBottomLeftRow[1] = 0;
    boxTopRightRow[1] = _inputMap->getNumRows() - deltaRows;
  } else {
    //moved south, empty at bottom
    boxBottomLeftRow[2] = 0;
    boxTopRightRow[2] = -1*deltaRows;
    boxBottomLeftRow[0] = 0;
    boxTopRightRow[0] = -1*deltaRows;
    boxBottomLeftRow[1] = -1*deltaRows;
    boxTopRightRow[1] = _inputMap->getNumRows() -1;
  }
  if(deltaCols>=0) {
    //we moved east, empty area is at right
    boxBottomLeftCol[2] = _inputMap->getNumCols() - deltaCols;
    boxTopRightCol[2] = _inputMap->getNumCols() -1;

    boxBottomLeftCol[1] = _inputMap->getNumCols() - deltaCols;
    boxTopRightCol[1] = _inputMap->getNumCols() - 1;
    boxBottomLeftCol[0] = 0;
    boxTopRightCol[0] = _inputMap->getNumCols() - deltaCols;
  } else {
    //moved west, empty at left
    boxBottomLeftCol[2] = 0;
    boxTopRightCol[2] = -1*deltaCols;

    boxBottomLeftCol[1] = 0;
    boxTopRightCol[1] = -1*deltaCols;
    boxBottomLeftCol[0] = -1*deltaCols;
    boxTopRightCol[0] = _inputMap->getNumCols()-1;
  }
  /*
  prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindOut(currentWaypoint,
							   _inputMap->getWindowBottomLeftUTMNorthing(),
							   _inputMap->getWindowBottomLeftUTMEasting(),
							   _inputMap->getWindowTopRightUTMNorthing(),
							   _inputMap->getWindowTopRightUTMEasting());
  nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadOut(currentWaypoint,
							  _inputMap->getWindowBottomLeftUTMNorthing(),
							  _inputMap->getWindowBottomLeftUTMEasting(),
							  _inputMap->getWindowTopRightUTMNorthing(),
							  _inputMap->getWindowTopRightUTMEasting());
  */
    prevWaypointNum[0] = _inputRDDF->getWaypointNumBehindDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							      1000);
    nextWaypointNum[0] = _inputRDDF->getWaypointNumAheadDist(_inputMap->getVehLocUTMNorthing(),
							      _inputMap->getVehLocUTMEasting(),
							     1000) - 1;
    //printf("[%d] %s: \n", __LINE__, __FILE__);
  
  for(int i=0; i<3; i++) {
    //printf("[%d] %s: \n", __LINE__, __FILE__);
    //get the waypoints for each box  
    //printf("Box %d: (%d, %d) to (%d, %d)\n", i,
    //boxBottomLeftRow[i], boxBottomLeftCol[i],
    //boxTopRightRow[i], boxTopRightCol[i]);
    _inputMap->Win2UTM(boxBottomLeftRow[i], boxBottomLeftCol[i],
		       &(boxBottomLeftUTMNorthing[i]), &(boxBottomLeftUTMEasting[i]));
    _inputMap->Win2UTM(boxTopRightRow[i], boxTopRightCol[i],
		       &(boxTopRightUTMNorthing[i]), &(boxTopRightUTMEasting[i]));
    
    //printf("Drawing from %d to %d\n", prevWaypointNum[0], nextWaypointNum[0]);
    /*
    prevWaypointNum[i] = _inputRDDF->getWaypointNumBehindOut(currentWaypoint,
							     boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							     boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
    nextWaypointNum[i] = _inputRDDF->getWaypointNumAheadOut(currentWaypoint,
							    boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							    boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i]);
    */
    for(int j=prevWaypointNum[0]; j <= nextWaypointNum[0]; j++) { 
      //get coords for waypt box
      int temp = _inputRDDF->getCorridorSegmentBoundingBoxUTM(j, 
							      boxBottomLeftUTMNorthing[i], boxBottomLeftUTMEasting[i],
							      boxTopRightUTMNorthing[i], boxTopRightUTMEasting[i],
							      waypointBottomLeftNorthing, waypointBottomLeftEasting,
							      waypointTopRightNorthing, waypointTopRightEasting);

      _inputMap->UTM2Win(waypointBottomLeftNorthing, waypointBottomLeftEasting,
			 &corridorBoxBottomLeftRow, &corridorBoxBottomLeftCol);
      _inputMap->UTM2Win(waypointTopRightNorthing, waypointTopRightEasting,
			 &corridorBoxTopRightRow, &corridorBoxTopRightCol);
      /*
      printf("%d: (%d=%lf, %d=%lf) to (%d=%lf, %d=%lf)\n", j,
	     corridorBoxBottomLeftRow, waypointBottomLeftNorthing,
	     corridorBoxBottomLeftCol, waypointBottomLeftEasting,
	     corridorBoxTopRightRow, waypointTopRightNorthing,
	     corridorBoxTopRightCol, waypointTopRightEasting);
      */
      //printf("[%d] %s: \n", __LINE__, __FILE__);
      if(temp >= 0) {
	for(row=corridorBoxBottomLeftRow; row <= corridorBoxTopRightRow; row++) {
	  for(col=corridorBoxBottomLeftCol; col <= corridorBoxTopRightCol; col++) {
	    if(_inputMap->getDataWin<double>(_layerNum, row, col) == _outsideCorridorVal) {
	      _inputMap->Win2UTM(row, col, &Northing, &Easting);
	      double tempVal = _inputRDDF->isPointInCorridor(j, Northing, Easting);
	      int inNext = 0;
	      if(j+1<_inputRDDF->getNumTargetPoints()) {
		inNext = _inputRDDF->isPointInCorridor(j+1, Northing, Easting);
	      }
	      int inPrev = 0;
	      if(j-1>0) {
		int inPrev= _inputRDDF->isPointInCorridor(j-1, Northing, Easting);
	      }

	      if(tempVal == 1) {
		//tempVal = _inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting));
		double ratio = _inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting));
		if((ratio) < (1-_percentSwitch)) {
		  tempVal = _edgeVal+(_tracklineVal - _edgeVal)*((1-_percentSwitch - ratio)/(1-_percentSwitch));
		} else if(inNext || inPrev){
		  tempVal = _edgeVal+(_tracklineVal - _edgeVal)*((1-_percentSwitch - ratio)/(1-_percentSwitch));
		} else {
		  tempVal = _outsideCorridorVal + (_edgeVal - _outsideCorridorVal)*(1-ratio)/(_percentSwitch);
		}
		//tempVal = ratio;
		//tempVal = _edgeVal + (_tracklineVal - _edgeVal)*(1-_inputRDDF->getRatioToTrackline(NEcoord(Northing, Easting)));
		//tempVal = _insideCorridorVal;
	      } else {
		tempVal = _outsideCorridorVal;
	      }
	      if(_useDelta==1) {
		_inputMap->setDataWin_Delta<double>(_layerNum, row, col, 
						    tempVal);
	      } else {
		_inputMap->setDataWin<double>(_layerNum, row, col, 
					      tempVal);
	      }
	      //printf("%s [%d]: Setting data! to %lf\n", __FILE__, __LINE__, tempVal);
	    }
	  }
	}
      }
    }
  }
  }

  //printf("[%d] %s: \n", __LINE__, __FILE__);
  _prevBottomLeftRowMultiple = newBottomLeftRowMultiple;
  _prevBottomLeftColMultiple = newBottomLeftColMultiple;

  double northing[4];
  double easting[4];

  int int_northing[4];
  int int_easting[4];

  row = 0;
  col = 1;
  int combo=2;

  if(deltaRows>=0) {
    printf("CP North\n");
    northing[0] = boxBottomLeftUTMNorthing[col];
    northing[1] = boxTopRightUTMNorthing[col];
    northing[2] = boxTopRightUTMNorthing[col];
    northing[3] = boxBottomLeftUTMNorthing[col];
  } else {
    printf("CP South\n");
    northing[0] = boxBottomLeftUTMNorthing[col];
    northing[1] = boxTopRightUTMNorthing[col];
    northing[2] = boxTopRightUTMNorthing[col];
    northing[3] = boxBottomLeftUTMNorthing[col];
  }
  if(deltaCols>=0) {
    printf("CP East\n");
    easting[0] = boxBottomLeftUTMEasting[col];
    easting[1] = boxBottomLeftUTMEasting[col];
    easting[2] = boxTopRightUTMEasting[col];
    easting[3] = boxTopRightUTMEasting[col];
  } else {
    printf("CP West\n");
    easting[0] = boxBottomLeftUTMEasting[col];
    easting[1] = boxBottomLeftUTMEasting[col];
    easting[2] = boxTopRightUTMEasting[col];
    easting[3] = boxTopRightUTMEasting[col];
  }

  printf("CP %d %d\n", deltaRows, deltaCols);
  printf("CP Points are: ");
  /*
  printf("(%lf, %lf), ", northing[0], easting[0]);
  printf("(%lf, %lf), ", northing[1], easting[1]);
  printf("(%lf, %lf), ", northing[2], easting[2]);
  printf("(%lf, %lf), ", northing[3], easting[3]);
  */
  for(int i=0; i<4; i++) {
    _inputMap->UTM2Win(northing[i], easting[i], &(int_northing[i]), &(int_easting[i]));
    printf("(%d, %d), ", int_northing[i], int_easting[i]);
  }
  printf("\n");

  return 0;
}
#endif

void CCorridorPainter::paintChanges(NEcoord rowBox[], NEcoord colBox[]) {
				    //double RDDFscaling, double maxSpeed) {
  //How the algorithm will work
  //Do rowbox and col box identically, this is for one of them
  //if we know a given waypt boundary intersects the border:
  //calculate where the western line intersects the border of the box
  //trace western ray through the map until it exits the grid
  //if it doesn't exit the grid, follow the next line, etc.
  //trace eastern ray through, same process


  const CLineSegment rowBoxWSide(rowBox[0], rowBox[1]);
  const CLineSegment rowBoxNSide(rowBox[1], rowBox[2]);
  const CLineSegment rowBoxESide(rowBox[2], rowBox[3]);
  const CLineSegment rowBoxSSide(rowBox[3], rowBox[0]);

  NEcoord rStart, rEnd, lStart, lEnd;
  NEcoord intersectionPoint;
  //cout << "making rowbox" << endl;
  CRectangle rowBoxRect(rowBox);
  //cout << "making colbox" << endl;
  CRectangle colBoxRect(colBox);

  int rMinRow, rMinCol, rMaxRow, rMaxCol;
  int cMinRow, cMinCol, cMaxRow, cMaxCol;

  _inputMap->UTM2Win(rowBox[0].N, rowBox[0].E, &rMinRow, &rMinCol);
  _inputMap->UTM2Win(rowBox[2].N, rowBox[2].E, &rMaxRow, &rMaxCol);
  _inputMap->UTM2Win(colBox[0].N, colBox[0].E, &cMinRow, &cMinCol);
  _inputMap->UTM2Win(colBox[2].N, colBox[2].E, &cMaxRow, &cMaxCol);
  
  if(rMinRow < 0) rMinRow = 0;
  if(rMinCol < 0) rMinCol = 0;
  if(cMinRow < 0) cMinRow = 0;
  if(cMinCol < 0) cMinCol = 0;
  if(rMaxRow >= _inputMap->getNumRows()) rMaxRow = _inputMap->getNumRows()-1;
  if(cMaxRow >= _inputMap->getNumRows()) cMaxRow = _inputMap->getNumRows()-1;
  if(rMaxCol >= _inputMap->getNumCols()) rMaxCol = _inputMap->getNumRows()-1;
  if(cMaxCol >= _inputMap->getNumCols()) cMaxCol = _inputMap->getNumRows()-1;
  

  for(int i=0; i<_inputRDDF->getNumTargetPoints()-1; i++) {
    CLineSegment trackline(_inputRDDF->getWaypoint(i),
			   _inputRDDF->getWaypoint(i+1));
    CRectangle rddfRect(_inputRDDF->getWaypoint(i),
			_inputRDDF->getWaypoint(i+1),
			_inputRDDF->getOffset(i));
    CCircle rddfCircle(_inputRDDF->getWaypoint(i),
		       _inputRDDF->getOffset(i));
    CCircle nextCircle(_inputRDDF->getWaypoint(i+1),
		       _inputRDDF->getOffset(i));

    double tracklineSpeed;
    double speed;
    if(_painterOpts.whatToAdjust==CCorridorPainterOptions::RDDF &&
       _painterOpts.paintCenterTrackline!=CCorridorPainterOptions::DONT) {
      if(_painterOpts.paintCenterTrackline==CCorridorPainterOptions::PERCENTAGE) {
	speed = fmin(_inputRDDF->getWaypointSpeed(i) * 
		     _painterOpts.optRDDFscaling * 
		     _painterOpts.adjustmentVal,
		     _painterOpts.optMaxSpeed);
      } else {
	speed = fmin((_inputRDDF->getWaypointSpeed(i) *
		      _painterOpts.optRDDFscaling) +
		     _painterOpts.adjustmentVal,
		     _painterOpts.optMaxSpeed);
      }
    } else {
      speed = fmin(_inputRDDF->getWaypointSpeed(i) * _painterOpts.optRDDFscaling,
		   _painterOpts.optMaxSpeed);
    }

    if(_painterOpts.whatToAdjust!=CCorridorPainterOptions::RDDF) {
      if(_painterOpts.paintCenterTrackline==CCorridorPainterOptions::PERCENTAGE) {
	tracklineSpeed = fmin(_inputRDDF->getWaypointSpeed(i) * 
			      _painterOpts.optRDDFscaling *
			      _painterOpts.adjustmentVal,
			      _painterOpts.optMaxSpeed);
      } else if (_painterOpts.paintCenterTrackline==CCorridorPainterOptions::SHIFT) {
	tracklineSpeed = fmin((_inputRDDF->getWaypointSpeed(i) * 
			       _painterOpts.optRDDFscaling) + 
			      _painterOpts.adjustmentVal,
			      _painterOpts.optMaxSpeed);
      }
    } else {
      tracklineSpeed = fmin(_inputRDDF->getWaypointSpeed(i) * _painterOpts.optRDDFscaling,
			    _painterOpts.optMaxSpeed);
    }


    if(rowBoxRect.checkIntersection(rddfCircle)==CRectangle::INTERSECTION_YES) {
      //cout << "intersection of circle " << i << " with rowbox" << endl;
      paintCircle(rddfCircle, speed,
		  rMinRow, rMaxRow, rMinCol, rMaxCol);
    }
    if(colBoxRect.checkIntersection(rddfCircle)==CRectangle::INTERSECTION_YES) {
      //cout << "intersection of circle " << i << " with colbox" << endl;
      paintCircle(rddfCircle, speed,
		  cMinRow, cMaxRow, cMinCol, cMaxCol);
    }

    if(rowBoxRect.checkIntersection(nextCircle)==CRectangle::INTERSECTION_YES) { 
      //cout << "intersection of next circle " << i+1 << " with rowbox" << endl;
      paintCircle(nextCircle, speed,
		  rMinRow, rMaxRow, rMinCol, rMaxCol);
    }
    if(colBoxRect.checkIntersection(nextCircle)==CRectangle::INTERSECTION_YES) {
      //cout << "intersection of next circle " << i+1 << " with colbox" << endl;
      paintCircle(nextCircle, speed,
		  cMinRow, cMaxRow, cMinCol, cMaxCol);
    }

    if(rddfRect.checkIntersection(rowBoxRect)==CRectangle::INTERSECTION_YES) {
      paintRectangle(trackline, i, speed,
		     rMinRow, rMaxRow, rMinCol, rMaxCol);
      //printf("Speed of rddf is %lf\n", speed);
      if(_painterOpts.paintCenterTrackline != CCorridorPainterOptions::DONT)
	paintTrackline(i, tracklineSpeed,
		       rMinRow, rMaxRow, rMinCol, rMaxCol);
    }
    if(rddfRect.checkIntersection(colBoxRect)==CRectangle::INTERSECTION_YES) {
      //printf("Speed of rddf is %lf\n", speed);
      paintRectangle(trackline, i, speed,
		     cMinRow, cMaxRow, cMinCol, cMaxCol);
      if(_painterOpts.paintCenterTrackline != CCorridorPainterOptions::DONT)
	paintTrackline(i, tracklineSpeed,
		       cMinRow, cMaxRow, cMinCol, cMaxCol);
    }

    /*
    double oldval;
    if(i==30) {
      cout << "row box is " <<  rMaxRow-rMinRow << " rows tall and "
	   << rMaxCol - rMinCol << " cols wide" << endl;
      cout << "col box is " <<  cMaxRow-cMinRow << " rows tall and "
	   << cMaxCol - cMinCol << " cols wide" << endl;
      cout << "our bounds are rowbox, rows from " << rMinRow << " to "
	   << rMaxRow << " and cols from " << rMinCol << " to "
	   << rMaxCol << endl;
      cout << "our bounds are colbox, rows from " << cMinRow << " to "
	   << cMaxRow << " and cols from " << cMinCol << " to "
	   << cMaxCol << endl;
      for(int r=cMinRow; r<=cMaxRow; r++) {
	for(int c=cMinCol; c<=cMaxCol; c++) {
	  oldval = _inputMap->getDataWin<double>(_layerNum, r, c);
	  _inputMap->setDataWin_Delta<double>(_layerNum, r, c, oldval+10.0);
	} 
      }
      for(int r=rMinRow; r<=rMaxRow; r++) {
	for(int c=rMinCol; c<=rMaxCol; c++) {
	  oldval = _inputMap->getDataWin<double>(_layerNum, r, c);
	  _inputMap->setDataWin_Delta<double>(_layerNum, r, c, oldval+5.0);
	} 
      }
      sleep(2);
    }
    */
  }
}


void CCorridorPainter::findStartingIndices(NEcoord bottomPoint, NEcoord middlePoint,
					   NEcoord topPoint,
					   int minRow, int maxRow,
					   int minCol, int maxCol) {
  /*
  int middleCol;
  int middleRow;
  _inputMap->UTM2Win(middlePoint.N, middlePoint.E, &middleRow, &middleCol);
  _startingIndices[middleRow] = middleCol;
  */
  traceRay(middlePoint, bottomPoint, _startingIndices,
  //traceRay(bottomPoint, middlePoint, _startingIndices,
	   minRow, maxRow, minCol, maxCol, 1);
  traceRay(middlePoint, topPoint, _startingIndices,
  //traceRay(topPoint, middlePoint, _startingIndices,
	   minRow, maxRow, minCol, maxCol, 1);
}

void CCorridorPainter::findEndingIndices(NEcoord bottomPoint, NEcoord middlePoint,
					 NEcoord topPoint,
					 int minRow, int maxRow,
					 int minCol, int maxCol) {
  /*
  int middleCol;
  int middleRow;
  _inputMap->UTM2Win(middlePoint.N, middlePoint.E, &middleRow, &middleCol);
  _startingIndices[middleRow] = middleCol;
  */
  //traceRay(bottomPoint, middlePoint, _endingIndices, 
  traceRay(middlePoint, bottomPoint, _endingIndices,
	   minRow, maxRow, minCol, maxCol, -1);
  //traceRay(topPoint, middlePoint, _endingIndices, 
  traceRay(middlePoint, topPoint, _endingIndices,
	   minRow, maxRow, minCol, maxCol, -1);
}


void CCorridorPainter::traceRay(NEcoord start, NEcoord end, int indexList[],
				int minRow, int maxRow,
				int minCol, int maxCol, int outOnWhichSide) {
  int startRow, startCol, endRow, endCol;
  _inputMap->UTM2Win(start.N, start.E, &startRow, &startCol);
  _inputMap->UTM2Win(end.N, end.E, &endRow, &endCol);

  if(startRow == endRow) {
    if(startRow >= minRow && startRow <= maxRow) {
      if(startCol < endCol) {
	indexList[startRow] = startCol;
      } else {
	indexList[startRow] = endCol;
      }
    return;
    }
  }

  int d_row;
  if(startRow < endRow) {
    d_row = 1;
  } else {
    d_row = -1;
  }
  int d_col;
  if(startCol < endCol) {
    d_col = 1;
  } else {
    d_col = -1;
  }
  /*
  cout << "srow is " << startRow << ", erow is " << endRow << " so d_row is " << d_row 
       << " and scol is " << startCol << " and ecol is " << endCol 
       << " and d_col is " << d_col << endl;
  */

  int row = startRow;
  int col = startCol;
  int colToSet;
  double slope = (start.N-end.N)/(start.E-end.E);
  
  double t_x=0, t_y=0;

  double resCols = _inputMap->getResCols();
  double resRows = _inputMap->getResRows();

  double yShift = resCols*slope;
  double xShift = resRows/slope;

  colToSet = col;
  if(colToSet < minCol) colToSet = minCol;
  if(colToSet > maxCol) colToSet = maxCol;
  if(row >= minRow && row <=maxRow) 
    indexList[row] = colToSet;

  NEcoord xPoint, yPoint, firstRowParallelIntercept, firstColParallelIntercept;
  _inputMap->roundUTMToCellBottomLeft(start.N, start.E,
				      &firstRowParallelIntercept.N,
				      &firstColParallelIntercept.E);
  firstRowParallelIntercept.N += (d_row > 0) ? resRows : 0;
  firstColParallelIntercept.E += (d_col > 0) ? resCols : 0;
  firstRowParallelIntercept.E = (firstRowParallelIntercept.N - start.N)/slope + start.E;
  firstColParallelIntercept.N = (firstColParallelIntercept.E - start.E)*slope + start.N;

  /*
  if(_debuggit==1 && indexList==_endingIndices) {
    cout << setprecision(10) << endl;
    cout << "First rpari is ";
    firstRowParallelIntercept.display();
    cout << "First cpari is ";
    firstColParallelIntercept.display();
    cout << "And start is ";
    start.display();
    cout << "end is ";
    end.display();
    cout << "xshift is " << xShift << " yshift is " << yShift 
	 << " slope is " << slope << endl;
    cout << endl << "starting" << endl;
  }
  */

  while(row != endRow) {
    xPoint.N = firstRowParallelIntercept.N + d_row*abs(row-startRow)*resRows;
    xPoint.E = firstRowParallelIntercept.E + d_row*abs(row-startRow)*xShift;
    yPoint.N = firstColParallelIntercept.N + d_col*abs(col-startCol)*yShift;
    yPoint.E = firstColParallelIntercept.E + d_col*abs(col-startCol)*resCols;
    /*
    if(_debuggit==1 && indexList==_endingIndices) {
      cout << endl;
      cout << "xpoint is: " << setprecision(10);
      xPoint.display();
      cout << "ypoint is ";
      yPoint.display();
    }
    */
    t_x = (start-xPoint).norm();
    t_y = (start-yPoint).norm();

    if(t_x < t_y) {
      //row goes up or down
      /*
      if(_debuggit==1 && indexList==_endingIndices) {
	cout << "choosing xpoint (ropar) as closer" << endl;
	cout << "At row=" << row << ", col=" << col << ", moving to row=";
      }
      */
      row+=d_row;
      /*
    if(_debuggit==1 && indexList==_endingIndices)
      cout << row << ", t_y is " << t_y << ", tx is " << t_x << endl;
      */
      colToSet = col;
      if(colToSet < minCol) {
	/*
	cout << "Col is " << col
	     << " setting to mincol " << minCol
	     << " row is " << row << endl;
	*/
	if(outOnWhichSide==-1) {
	  colToSet = minCol-1;
	} else {
	  colToSet = minCol;	
	}
      }
      if(colToSet > maxCol) {
	/*
	cout << "Col is " << col
	     << " setting to maxcol " << maxCol
	     << " row is " << row << endl;
	*/
	if(outOnWhichSide==1) {
	  colToSet = maxCol+1;
	} else {
	  colToSet = maxCol;
	}
      }
      if(row >= minRow && row <= maxRow)
	indexList[row] = colToSet;
    } else {
      /*
      if(_debuggit==1 && indexList==_endingIndices) {
	cout << "choosing ypoint (colpar) as closer" << endl;
      cout << "At row=" << row << ", col=" << col << ", moving to col=";
      }*/
      col+=d_col;
      /*
    if(_debuggit==1 && indexList==_endingIndices)      
      cout << col << ", t_y is " << t_y << ", tx is " << t_x << endl;
      */
    }
  }
}

void CCorridorPainter::paintCircle(CCircle circle, double speed,
				   int minRow, int maxRow,
				   int minCol, int maxCol) {
  int startRow, endRow, tempInt;
  _inputMap->UTM2Win(circle._center.N-circle._radius, circle._center.E, &startRow, &tempInt);
  if(startRow < minRow)
    startRow = minRow;
  if(startRow > maxRow)
    startRow = maxRow;
  
  _inputMap->UTM2Win(circle._center.N+circle._radius, circle._center.E, &endRow, &tempInt);
  if(endRow < minRow)
    endRow = minRow;
  if(endRow > maxRow)
    endRow = maxRow;

  double x_val, y_val, offset;
  double start_x_val, end_x_val;
  int col, startCol, endCol;
  /*
  cout << setprecision(10) << "srow is " << startRow << " and erow is " << endRow << endl;
  cout << "the rad is " << circle._radius << " and is ";
  circle._center.display();
  */
  double origSpeed;
  for(int row=startRow; row<=endRow; row++) {
    _inputMap->Win2UTM(row, tempInt, &y_val, &x_val);
    _inputMap->roundUTMToCellBottomLeft(y_val, x_val, &y_val, &x_val);
    //cout << "y_val is: " << y_val << endl;
    offset = sqrt(pow(circle._radius,2) - pow(y_val-circle._center.N,2));
    start_x_val = circle._center.E - offset;
    end_x_val = circle._center.E + offset;
    //cout << "x_val was " << x_val << endl;
    _inputMap->UTM2Win(y_val, start_x_val, &tempInt, &startCol);
    //_startingIndices[row] = startCol;
    _inputMap->UTM2Win(y_val, end_x_val, &tempInt, &endCol);
    if(!((startCol > maxCol && endCol > maxCol) ||
       (startCol < minCol && endCol < minCol))) {
      //cout << "actually painting" << endl;
      if(startCol < minCol) startCol = minCol;
      if(startCol > maxCol) startCol = maxCol;
      if(endCol < minCol) endCol = minCol;
      if(endCol > maxCol) endCol = maxCol;
      for(col=startCol; col <=endCol; col++) {
	origSpeed = _inputMap->getDataWin<double>(_layerNum, row, col);
	if(_useDelta) {
	  _inputMap->setDataWin_Delta<double>(_layerNum, row, col, fmax(speed, origSpeed));
	} else {
	  _inputMap->setDataWin<double>(_layerNum, row, col, fmax(speed, origSpeed));
	}		 
      }
      //_startingIndices[row] = endCol;
      //_inputMap->setDataWin_Delta<double>(_layerNum, row, startCol, speed);
      //_inputMap->setDataWin_Delta<double>(_layerNum, row, endCol, speed);
    }
  }
}



void CCorridorPainter::paintRectangle(CLineSegment trackline, int i, double speed, 
				      int minRow, int maxRow,
				      int minCol, int maxCol) {
  int tempInt;

  NEcoord normalVector(-(trackline.endPoint.E - trackline.startPoint.E),
		       trackline.endPoint.N - trackline.startPoint.N);
  normalVector/=normalVector.norm();
  NEcoord bottomRight = trackline.startPoint + normalVector*_inputRDDF->getOffset(i);
  NEcoord bottomLeft = trackline.startPoint - normalVector*_inputRDDF->getOffset(i);
  NEcoord topRight = trackline.endPoint + normalVector*_inputRDDF->getOffset(i);
  NEcoord topLeft = trackline.endPoint - normalVector*_inputRDDF->getOffset(i);
  NEcoord bottom, leftMiddle, rightMiddle, top;
  if(trackline.startPoint.E < trackline.endPoint.E) {
    bottom = bottomRight;
    leftMiddle = bottomLeft;
    rightMiddle = topRight;
    top = topLeft;
  } else {
    bottom = bottomLeft;
    leftMiddle = topLeft;
    rightMiddle = bottomRight;
    top = topRight;
  }
  _inputMap->UTM2Win(bottom.N, bottom.E, &_startRow, &tempInt);
  if(_startRow < minRow)
    _startRow = minRow;
  if(_startRow > maxRow)
    _startRow = maxRow;
  
  _inputMap->UTM2Win(top.N, top.E, &_endRow, &tempInt);
  if(_endRow < minRow)
    _endRow = minRow;
  if(_endRow > maxRow)
    _endRow = maxRow;
  
  findStartingIndices(bottom, leftMiddle, top,
		      minRow, maxRow, minCol, maxCol);
  findEndingIndices(bottom, rightMiddle, top,
		      minRow, maxRow, minCol, maxCol);
  double origSpeed;
  for(int row = _startRow; row <= _endRow; row++) {
    for(int col = _startingIndices[row]; col <= _endingIndices[row]; col++) {
      origSpeed = _inputMap->getDataWin<double>(_layerNum, row, col);
      if(speed > origSpeed) {
	if(_useDelta) {
	  _inputMap->setDataWin_Delta<double>(_layerNum, row, col, speed);
	} else {
	  _inputMap->setDataWin<double>(_layerNum, row, col, speed);
	}		 
      }
    }
  }
}


void CCorridorPainter::repaintAll() {
  NEcoord rowBox[4], colBox[4];

  _inputMap->getEntireMapBoxUTM(rowBox);
  for(int i=0; i<4; i++)
    colBox[i] = NEcoord(0.0, 0.0);

  paintChanges(rowBox, colBox);
	       //, RDDFscaling, maxSpeed);
}


void CCorridorPainter::setPainterOptions(CCorridorPainterOptions* painterOpts) {
  _painterOpts = *painterOpts;
}


void CCorridorPainter::paintTrackline(int i, double speed,
				      int minRow, int maxRow,
				      int minCol, int maxCol) {
  int tempInt;

  NEcoord bottom=_inputRDDF->getWaypoint(i);
  NEcoord top = _inputRDDF->getWaypoint(i+1);

  if(bottom.N > top.N) {
    NEcoord temp = top;
    top = bottom;
    bottom = temp;
  }

  _inputMap->UTM2Win(bottom.N, bottom.E, &_startRow, &tempInt);
  if(_startRow < minRow)
    _startRow = minRow;
  if(_startRow > maxRow)
    _startRow = maxRow;
  
  _inputMap->UTM2Win(top.N, top.E, &_endRow, &tempInt);
  if(_endRow < minRow)
    _endRow = minRow;
  if(_endRow > maxRow)
    _endRow = maxRow;
  
  traceRay(bottom, top, _tracklineStartingIndices,
	   minRow, maxRow, minCol, maxCol, 1);
  traceRay(top, bottom, _tracklineEndingIndices,
	   minRow, maxRow, minCol, maxCol, -1);  


  for(int row = _startRow; row <= _endRow; row++) {
    for(int col = _tracklineStartingIndices[row]; col <= _tracklineEndingIndices[row]; col++) {
      if(_useDelta) {
	_inputMap->setDataWin_Delta<double>(_layerNum, row, col, speed);
      } else {
	_inputMap->setDataWin<double>(_layerNum, row, col, speed);
      }		 
    }
  }
  


}
