//Class interface to the sparrow display!!!
//
//Include all your .h files defining sparrow interfaces for include sparrowhawk.h,
//then call the constructor with the main sparrow display page
//
//At the top of your dd file, include the following define to enable
//dynamic rebinding of variables:
//extern double sparrowhawk;
//#define D(x)   (sparrowhawk)
//
//Then when defining variables use the D() construct around the variable definitions
//int:    %lbcnt    D(LadarBumperCnt)   "%d";

//You should only create one instance of this class!!!

//Remove the define used to make dynamic variables
#ifdef D
#undef D
#endif

//Keycodes
#undef KEY_UP
#undef KEY_DOWN
#undef KEY_LEFT
#undef KEY_RIGHT
#undef KEY_HOME
#undef KEY_END
#undef KEY_BACKSPACE

#define KEY_UP        0x00415b1b
#define KEY_DOWN      0x00425b1b
#define KEY_LEFT      0x00445b1b
#define KEY_RIGHT     0x00435b1b

#define KEY_S_UP      0x41324f1b
#define KEY_S_DOWN    0x42324f1b
#define KEY_S_LEFT    0x44324f1b
#define KEY_S_RIGHT   0x43324f1b

#define KEY_C_UP      0x41355b1b
#define KEY_C_DOWN    0x42355b1b
#define KEY_C_LEFT    0x44355b1b
#define KEY_C_RIGHT   0x43355b1b

#define KEY_A_UP      0x41335b1b
#define KEY_A_DOWN    0x42335b1b
#define KEY_A_LEFT    0x44335b1b
#define KEY_A_RIGHT   0x43335b1b

#define KEY_CS_UP     0x41365b1b
#define KEY_CS_DOWN   0x42365b1b
#define KEY_CS_LEFT   0x44365b1b
#define KEY_CS_RIGHT  0x43365b1b

#define KEY_PGUP      0x7e355b1b
#define KEY_PGDN      0x7e365b1b
#define KEY_HOME      0x00000084
#define KEY_END       0x00464f1b

#define KEY_BACKSPACE 0x0000007f
#define KEY_DEL       0x7e335b1b
#define KEY_INS       0x7e325b1b
#define KEY_TAB       0x00000009

#define KEY_A(x)      (0x000001b + ((x)<<8))

//Main sparrowhawk class
#ifndef SPARROWHAWK_HH
#define SPARROWHAWK_HH

#include"sparrow/display.h"
#include"sparrow/dbglib.h"

#include<vector>
#include<string>
using namespace std;


//Include functors for callbacks, getters and setters
#include "sparrowhawk_functors.hh"

//Accessor function to singleton instance of CSparrowHawk
//Usage: SparrowHawk().run();
//       
//       CSparrowHawk &sh = SparrowHawk();
//       sh.run();
class CSparrowHawk;
CSparrowHawk &SparrowHawk();

/*
**
**Singleton implementation of sparrowhawk
**
*/
class CSparrowHawk
{
 private:
  /*
  **
  ** Singleton functionality
  ** Private constructors of CSparrowHawk prevents multiple instances
  **
  */
  CSparrowHawk();
  CSparrowHawk(const CSparrowHawk &sh) {}
 public:
  ~CSparrowHawk();

  //Get the singleton sparrow instance
  static CSparrowHawk *Instance();
 protected:
  static CSparrowHawk *pInstance;

 public:
  /*
  **
  ** Running, stopping and operations
  **
  */
  bool run();
  void stop();

  bool running();
  
  void set_update_frequency(double hz);

  //Change the value of the names string
  void set_string(const char *name, const char *str);


  /*
  **
  ** Log messages to sparrow display
  **
  */
  void log(const char *sz, ...);   //Log using same format as printf
  void log(const string &str);
  void clear_log();

 public:
  /*
  **
  ** Settings (too lazy to make accessors)
  **
  */
  struct Settings {
    bool use_tabs;//=true       Should tabs be used or not
    bool tabs_top;//=false      Should the tabs be on top (otherwise bottom)
    bool tab_seperator;//=false Should a seperator line be added between page and tabs
    bool log_tab;//=true        Should a log tab be added

    bool log_nr;//=false        Add row number to all log messages
    bool log_time;//=true       Add time stamp to all logs
  } setting;

  /*
  **
  ** Handle pages, this only has effect before sparrow is started
  **
  */
  //Add a page to the current display
  void add_page(DD_IDENT *sparrow_page, const char *name);

  //rebind variables
  void rebind(const char *name, bool *ptr);
  void rebind(const char *name, int *ptr);
  void rebind(const char *name, double *ptr);

  //Set properties
  void set_readonly(const char *name, bool readonly=true);
  void set_readonly(display_entry *de, bool readonly=true);

  //Returns entry from name
  display_entry *find(const char *name);

  /*
  **
  ** Dynamically create (/change) pages
  **
  */

  //Creates holders for items in a page
  display_entry *create_page(const char *name, int items);


  void make_label(display_entry *de, int row, int col, const char *str, int max_len=-1);
  void make_button(display_entry *de, int row, int col, const char *str, 
		   int (*callback)(long)=dd_nilcbk, long user_arg=0, int max_len=-1);

  void make_null(display_entry *de);

  //Make item to display variable
  void make_display(display_entry *de, int row, int col, bool *pbool, 
		    const char *format = "");
  void make_display(display_entry *de, int row, int col, int *pint, 
		    const char *format = "%d");
  void make_display(display_entry *de, int row, int col, double *pdouble, 
		    const char *format = "%f");

  //Make item to display and edit variable
  void make_edit(display_entry *de, int row, int col, bool *pbool, 
		    const char *format = "");
  void make_edit(display_entry *de, int row, int col, int *pint, 
		    const char *format = "%d");
  void make_edit(display_entry *de, int row, int col, double *pdouble, 
		    const char *format = "%f");

  //Change item properties
  void set_fg_color(display_entry *de, int col);   //Set color number (0-7 vga colors)
  void set_bg_color(display_entry *de, int col);   //Set color number (0-7 vga colors)
  //Change color (possible also at runtime)
  void set_fg_color(const char *name, int col);   //Set color number (0-7 vga colors)
  void set_bg_color(const char *name, int col);   //Set color number (0-7 vga colors)


 private:
  /*
  **
  ** Callback functions
  **
  */

  //Handles callbacks w CallbackData structure
  static int do_callback(long);

  //Functions to sit inbetween sparrow interface and sparrow handler functions to
  //insert code for getter/setter logic
  static int do_bool(DD_ACTION action, int id);
  static int do_int(DD_ACTION action, int id);
  static int do_double(DD_ACTION action, int id);

 private:
  /*
  **
  ** Set keyboard mapping
  **
  */
  void set_final_keymap(int keycode, CSparrowHawkCallback *pc);
  void set_final_keymap(const char *page, int keycode, CSparrowHawkCallback *pc);

 public:
  template<class T>
  void set_keymap(int keycode, T func) {
    set_final_keymap(keycode, new CSparrowHawkCallbackFnc<T>(func));
  }
  template<class T>
  void set_keymap(const char *page, int keycode, T func) {
    set_final_keymap(page, keycode, new CSparrowHawkCallbackFnc<T>(func));
  }
  template<class T>
  void set_keymap(int keycode, T *pClass, void (T::*fcn)()) {
    set_final_keymap(keycode, new CSparrowHawkCallbackT<T>(pClass, fcn));
  }
  template<class T>
  void set_keymap(const char *page, int keycode, T *pClass, void (T::*fcn)()) {
    set_final_keymap(page, keycode, new CSparrowHawkCallbackT<T>(pClass, fcn));
  }
  template<class T, class A1>
  void set_keymap(int keycode, T *pClass, void (T::*fcn)(A1), A1 arg1) {
    set_final_keymap(keycode, new CSparrowHawkCallbackT1<T, A1>(pClass, fcn, arg1));
  }
  template<class T, class A1>
  void set_keymap(const char *page, int keycode, T *pClass, void (T::*fcn)(A1), A1 arg1) {
    set_final_keymap(page, keycode, new CSparrowHawkCallbackT1<T, A1>(pClass, fcn, arg1));
  }

 private:
  /*
  **
  ** Set the notification function
  **
  */
  void set_final_notify(display_entry *de, CSparrowHawkCallback *pc);

 public:
  template<class T>
  void set_notify(display_entry *de, T func) {
    set_final_notify(de, new CSparrowHawkCallbackFnc<T>(func));
  }
  template<class T>
  void set_notify(const char *name, T func) {
    if( display_entry*de=find(name)) 
      set_final_notify(de, new CSparrowHawkCallbackFnc<T>(func));
  }
  template<class T>
  void set_notify(display_entry *de, T *pClass, void (T::*fcn)()) {
    set_final_notify(de, new CSparrowHawkCallbackT<T>(pClass, fcn));
  }
  template<class T>
  void set_notify(const char *name, T *pClass, void (T::*fcn)()) {
    if( display_entry * de=find(name)) 
      set_final_notify(de, new CSparrowHawkCallbackT<T>(pClass, fcn));
  }
  template<class T, class A1>
  void set_notify(display_entry *de, T *pClass, void (T::*fcn)(A1), A1 arg1) {
    set_final_notify(de, new CSparrowHawkCallbackT1<T, A1>(pClass, fcn, arg1));
  }
  template<class T, class A1>
  void set_notify(const char *name, T *pClass, void (T::*fcn)(A1), A1 arg1) {
    if( display_entry*de=find(name)) 
      set_final_notify(de, new CSparrowHawkCallbackT1<T, A1>(pClass, fcn, arg1));
  }

 private:
  /*
  **
  ** Set the setter function
  **
  */
  void set_final_setter(display_entry *de, CSparrowHawkSetter *ps);

  template<class T>
  void set_setter_t(display_entry *de, CSparrowHawkSetterBT<T> *ps)
  {
    //This only compiles when used
    log("Unsupported setter type!");
    //error_in_set_setter_t_not_a_supported_type();
  }

 public:
  template<class BT, class T>
  void set_setter(display_entry *de, T func)
  { set_setter_t(de, new CSparrowHawkSetterFnc<BT, T>(func)); }
  template<class BT, class T>
  void set_setter(const char *name, T func)
  { if( display_entry*de=find(name)) set_setter_t(de, new CSparrowHawkSetterFnc<BT, T>(func)); }
 
  template<class BT, class T>
  void set_setter(display_entry *de, T *pClass, void (T::*fcn)(BT *, BT))
  { set_setter_t(de, new CSparrowHawkSetterT<BT, T>(pClass, fcn)); }
  template<class BT, class T>
  void set_setter(const char *name, T *pClass, void (T::*fcn)(BT *, BT))
  { if( display_entry*de=find(name)) set_setter_t(de, new CSparrowHawkSetterT<BT, T>(pClass, fcn)); }

  template<class BT, class T, class A1>
  void set_setter(display_entry *de, T *pClass, void (T::*fcn)(BT *, BT, A1), A1 arg1)
  { set_setter_t(de, new CSparrowHawkSetterT1<BT, T, A1>(pClass, fcn, arg1)); }
  template<class BT, class T, class A1>
  void set_setter(const char *name, T *pClass, void (T::*fcn)(BT *, BT, A1), A1 arg1)
  { if( display_entry*de=find(name)) set_setter_t(de, new CSparrowHawkSetterT1<BT, T, A1>(pClass, fcn, arg1)); }

 private:
  /*
  **
  ** Set the getter function
  **
  */

  void set_final_getter(display_entry *de, CSparrowHawkGetter *ps);

  template<class T>
  void set_getter_t(display_entry *de, CSparrowHawkGetterBT<T> *ps)
  {
    //This only compiles when used
    log("Unsupported getter type!");
    //error_in_set_getter_t_not_a_supported_type();
  }

 public:
  template<class BT, class T>
  void set_getter(display_entry *de, T func)
  { set_getter_t(de, new CSparrowHawkGetterFnc<BT, T>(func)); }
  template<class BT, class T>
  void set_getter(const char *name, T func)
  { if( display_entry*de=find(name)) set_getter_t(de, new CSparrowHawkGetterFnc<BT, T>(func)); }
 
  template<class BT, class T>
  void set_getter(display_entry *de, T *pClass, BT (T::*fcn)())
  { set_getter_t<BT>(de, new CSparrowHawkGetterT<BT, T>(pClass, fcn)); }
  template<class BT, class T>
  void set_getter(const char *name, T *pClass, BT (T::*fcn)())
  { if( display_entry*de=find(name)) set_getter_t<BT>(de, new CSparrowHawkGetterT<BT, T>(pClass, fcn)); }

  template<class BT, class T, class A1>
  void set_getter(display_entry *de, T *pClass, BT (T::*fcn)(A1), A1 arg1)
  { set_getter_t<BT>(de, new CSparrowHawkGetterT1<BT, T, A1>(pClass, fcn, arg1)); }
  template<class BT, class T, class A1>
  void set_getter(const char *name, T *pClass, BT (T::*fcn)(A1), A1 arg1)
  { if( display_entry*de=find(name)) set_getter_t<BT>(de, new CSparrowHawkGetterT1<BT, T, A1>(pClass, fcn, arg1)); }

 protected:
  /*
  **
  ** Generic helper functions
  **
  */
  void create_log_page();
  int calc_tab_rows();      //Rows used by tabs and seperators
  void convert_pages();     //Moves pages into internal_pages and adds tabs on the fly

  bool verify_stopped();    //Makes sure we are not running

  char *get_buf(int size);  //Return buffert space

  display_entry *find_internal(const char *name);    //Returns entry from name

  struct internal_page;
  internal_page *get_cur_page();    //Get current internal page

  int page_index(const char *name);

  /*
  **
  ** Tab and page scrolling helpers
  **
  */
 public:
  void tab_select(const char *name);
  void tab_select_prev();

 protected:
  static int tab_select(long id);
  static int do_static_scroll_function(DD_ACTION action, int id);
  int do_scroll_function(DD_ACTION action, int id);

 public:
  //Scroll window
  void scroll_up(int rows=5);
  void scroll_down(int rows=5);

  //Move cursor
  void move_down(int steps=1);
  void move_left(int steps=1);
  void move_right(int steps=1);
  void move_up(int steps=1);

  void move_first();
  void move_last();

 protected:
  /*
  **
  ** Log helper functions
  **
  */
  void log_changed();       //Log has changed
  void log_redraw();        //Force a redraw of the log
  void log_pgup();
  void log_pgdn();
  void log_home();
  void log_end();

  /*
  **
  ** key bounds
  **
  */
 public:
  int kb_handle(long key);   //Needs to be public to handle be callable from static helpers
  int kb_unbound(long key);  //Handle unbound keys

 protected:   //Keybord handers
  void kb_back();

  /*
  **
  ** Sparrow thread and callbacks
  **
  */
  void sparrow_thread();

 protected:
  /*
  **
  ** Data members
  **
  */

  /*
  **
  ** Page (tab) data
  **
  */
  struct page {
    display_entry *de;
    const char *name;
    bool should_delete;     //Mark that this page should be deleted in the destructor
  };

  vector<page> m_pages;

  struct internal_page {
    display_entry *de;
    int rows;
  };
  vector<internal_page> m_internal_pages;    //The pages w tabs added that sparrow uses

  
  /*
  **
  ** Scroll lock
  **
  */
  int m_scroll_row;
  int m_scroll_lock;           //Don't force a scroll as long as this item is selected


  /*
  **
  ** log messages
  **
  */
  vector<string>   m_log;       //Log strings
  display_entry    *m_log_page; //The log page
  int              m_log_rows;  //The number of rows for log
  int              m_log_pos;   //The position in the log buf
  int              m_log_items; //Number of items in the log

  /*
  **
  **is running flag
  **
  */
  bool m_running;

  /*
  **
  ** Keymapping data
  **
  */
  struct KeyBind {
    KeyBind() : code(0), pc(0) {}
    KeyBind(int c, CSparrowHawkCallback *p, int pg=-1) : code(c), pc(p), page(pg) {}

    int code;
    CSparrowHawkCallback *pc;
    int page;     //Page number to map on (or -1 for all)
  };
  vector<KeyBind> m_keybinder[256];

  /*
  **
  ** Sparrow buffers needed when items are added, sparrow needs an extra pointer
  ** to a buffered the same size of every variable added to the display
  **
  */
  static const int sparrow_buf_grow=1024;  //Use increments of 1024
  vector<char *>   m_sparrow_bufs;    //A bunch of bufferts, only last one has empty space
  int              m_sparrow_used;    //Number of bytes used in last buffer


  /*
  **
  ** Allocated callback mappers
  **
  */
  struct CallbackData {
    CallbackData() : pcallback(0), psetter(0), pgetter(0), pfunction(0) {}
    ~CallbackData() {
      if(pcallback)
	delete pcallback;
      pcallback=0;
      if(psetter)
	delete psetter;
      psetter=0;
      if(pgetter)
	delete pgetter;
      pgetter=0;
    }

    CSparrowHawkCallback *pcallback;
    CSparrowHawkSetter   *psetter;
    CSparrowHawkGetter   *pgetter;
    
    int (*pfunction)(DD_ACTION, int);    //The "real" display function, overriden to handle scrolling
  };
  vector<CallbackData*>  m_callbacks;
};


/*
**
**  Template specialization must be defined in namespace scope
**
*/
template<>
void CSparrowHawk::set_setter_t<bool>(display_entry *de, CSparrowHawkSetterBT<bool> *ps);
template<>
void CSparrowHawk::set_setter_t<int>(display_entry *de, CSparrowHawkSetterBT<int> *ps);
template<>
void CSparrowHawk::set_setter_t<double>(display_entry *de, CSparrowHawkSetterBT<double> *ps);


template<>
void CSparrowHawk::set_getter_t<bool>(display_entry *de, CSparrowHawkGetterBT<bool> *ps);
template<>
void CSparrowHawk::set_getter_t<int>(display_entry *de, CSparrowHawkGetterBT<int> *ps);
template<>
void CSparrowHawk::set_getter_t<double>(display_entry *de, CSparrowHawkGetterBT<double> *ps);



#endif
