#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include "planner.h"
#include "CCorridorPainter.hh"
#include "CMapPlus.hh"

int main(int argc, char *argv[])
{
	CMapPlus costmap;
	int layer;
	CMapPlus costmap2;
	int layer2;
	int i,j;
	double n, e;
	VehicleState state;


	double nhigh=-1e20, nlow=1e20, ehigh=-1e20, elow=1e20;
// 	RDDF rddf("/home/dkogan/classes/caltech/dgc/bob/RDDF/waypoints_caltechlot.rddf");
	RDDF rddf("rddf.dat");

	for(i=0; i<rddf.getNumTargetPoints(); i++)
	{
		nhigh = fmax(rddf.getWaypointNorthing(i), nhigh);
		nlow  = fmin(rddf.getWaypointNorthing(i), nlow);
		ehigh = fmax(rddf.getWaypointEasting(i),  ehigh);
		elow  = fmin(rddf.getWaypointEasting(i),  elow);
	}
	nhigh += 10.0;
	nlow -= 10.0;
	ehigh += 10.0;
	elow -= 10.0;

	double res = 0.2;
	int nwidth = lround(floor((nhigh-nlow)/res));
	int ewidth = lround(floor((ehigh-elow)/res));
	CCorridorPainter painter;
// 	costmap.initMap(-1e6, -1e6, nwidth, ewidth, res, res, 0);
// 	int layer = costmap.addLayer<int>(CP_OUTSIDE_CORRIDOR, CP_OUTSIDE_MAP);
// 	painter.initPainter(&costmap, layer, &rddf);
// 	costmap.updateVehicleLoc((nhigh+nlow)/2.0, (ehigh+elow)/2.0);
// 	painter.paintChanges();

	costmap.initMap(-1e6, -1e6, 2500, 1000, res, res, 0);
	layer = costmap.addLayer<double>(-0.01, -0.001);
 	costmap.loadLayer<double>(layer, "cmap", true);



	ofstream all("all");
	all << setprecision(20);


	state.Northing = rddf.getWaypointNorthing(0) - 4.0;
	state.Easting  = rddf.getWaypointEasting(0);
	state.Vel_N = -0.5;
	state.Vel_E = -0.0;
	state.Acc_N = 0.0;
	state.Acc_E = 0.0;
	state.Yaw = atan2(state.Vel_E, state.Vel_N);
	state.YawRate = 0.0;
	state.Vel_D = 0.0;

#if 0
	double ststate[]=
{3834448.9642390487716, -4.9826554823364341829, -0.0056891951331319778537,
442583.89353444706649, 0.41610616955612611534, -0.068125159861045836429,
3.0582750571456007727, 0.013672460418455640793}
;
	state.Northing = ststate[0];
	state.Easting  = ststate[3];
	state.Vel_N = ststate[1];
	state.Vel_E = ststate[4];
	state.Acc_N = ststate[2];
	state.Acc_E = ststate[5];
	state.Yaw = atan2(state.Vel_E, state.Vel_N);
	state.YawRate = (state.Vel_N*state.Acc_E - state.Acc_N*state.Vel_E) / (state.Vel_N*state.Vel_N + state.Vel_E*state.Vel_E);
	state.Vel_D = 0.0;
#endif

	CPlanner planner(&costmap, layer, &rddf, true);

	while(!(state.Northing >  3.83472e+06 &&
					state.Easting > 442597.0))
// 	while(state.Northing >  3.83439e+06)
	{

		int ptstoleaveoff;
		int planres = planner.plan(&state);
		if(planres == 0 || planres == 9 || planres == 4)
		{
			cout << "succeeded: " << planres << endl;
			ptstoleaveoff = planner.getTraj()->getNumPoints() - 100;
		}
		else
		{
			cout << "failed: " << planres << endl;
			ptstoleaveoff = planner.getTraj()->getNumPoints() - 50;
		}

		planner.getTraj()->print(all,planner.getTraj()->getNumPoints() - ptstoleaveoff);

		ofstream outseed("seed");
		planner.getSeedTraj()->print(outseed);

		state.Northing = planner.getTraj()->getNorthingDiff( planner.getTraj()->getNumPoints()-ptstoleaveoff, 0 );
		state.Easting  = planner.getTraj()->getEastingDiff ( planner.getTraj()->getNumPoints()-ptstoleaveoff, 0 );
		state.Vel_N    = planner.getTraj()->getNorthingDiff( planner.getTraj()->getNumPoints()-ptstoleaveoff, 1 );
		state.Vel_E    = planner.getTraj()->getEastingDiff ( planner.getTraj()->getNumPoints()-ptstoleaveoff, 1 );
		state.Acc_N    = planner.getTraj()->getNorthingDiff( planner.getTraj()->getNumPoints()-ptstoleaveoff, 2 );
		state.Acc_E		 = planner.getTraj()->getEastingDiff ( planner.getTraj()->getNumPoints()-ptstoleaveoff, 2 );
		state.Yaw			 = atan2(state.Vel_E, state.Vel_N);
		state.YawRate	 = (state.Vel_N*state.Acc_E - state.Acc_N*state.Vel_E) / (state.Vel_N*state.Vel_N + state.Vel_E*state.Vel_E);
	}
}
