#ifndef _DGCUTILS_
#define _DGCUTILS_

// vim: syntax=cpp
#include <pthread.h>
#include <iostream>
using namespace std;

//#define USE_RTAI

#ifdef USE_RTAI
#include <rtai/task.h>
#include <rtai/queue.h>
#include <rtai/intr.h>
#define STACK_SIZE 0
#define TASK_PRIO  99
#endif

#define MAX_THREADS 1024

//! Calls gettimeofday and fills in reference with number of microseconds since the epoch
void DGCgettime(unsigned long long& DGCtime);

//! Converts an unsigned long long (microseconds) to a double value (seconds)
double DGCtimetosec(unsigned long long& DGCtime, bool lose_precision = false);

//! Converts an unsigned long long (microseconds) to a long long (seconds)
long long DGCtimetollsec(const unsigned long long& DGCtime);

template <class T>
struct DGCObjectAndFunction
{
	T* object;
	void (T::*function)();
};

//! Struct for storing a boolean, a condition, and a mutex
struct DGCcondition
{
	bool bCond;
	pthread_cond_t pCond;
	pthread_mutex_t pMutex;

	operator bool&() { return bCond; }
};

template <class T>
void* DGCthread_func(void *pArg)
{
// 	cout << "DGCthread_func called" << endl;
	(((DGCObjectAndFunction<T>*)pArg)->object->*((DGCObjectAndFunction<T>*)pArg)->function)();
	return NULL;
}

template <class T>
void DGCthread_func_realtime(void *pArg)
{
	DGCthread_func<T>(pArg);
}

template <class T>
void DGCstartMemberFunctionThread(T* object, void (T::*function)(), bool bRealtime = false)
{
	//cout << "starting thread" << endl;
	static DGCObjectAndFunction<T> objectAndFunction[MAX_THREADS];
	static int thread_index = 0;

	objectAndFunction[thread_index].object   = object;
	objectAndFunction[thread_index].function = function;

	if(bRealtime)
	{
#ifdef USE_RTAI
		char thread_name[50];
		sprintf(thread_name,"DGC_RT_THREAD[%d]",getpid());
		cout << "starting realtime thread  "<< thread_name << ". oh man!" << endl;
		static RT_TASK thread_id[MAX_THREADS];
		rt_timer_start(TM_ONESHOT);
		if(rt_task_spawn(&thread_id[thread_index], thread_name, STACK_SIZE, TASK_PRIO, 0, &DGCthread_func_realtime<T>, (void*)&objectAndFunction[thread_index]) != 0)
		{
			cerr << "DGCstartMemberFunctionThread: couldn't create rtai thread" << endl;
		}
#else
		cerr << "DGCutils: USE_RTAI not defined, but we're spawning a realtime thread. Figure out what you want to do." << endl;
		exit(1);
#endif
	}
	else
	{
		static pthread_t thread_id[MAX_THREADS];
		if(pthread_create(&thread_id[thread_index], NULL, &DGCthread_func<T>, (void*)&objectAndFunction[thread_index]) != 0)
		{
			cerr << "DGCstartMemberFunctionThread: couldn't create thread" << endl;
		}
	}

	thread_index++;
}

template <class T>
void DGCstartMemberFunctionThreadInRealtime(T* object, void (T::*function)())
{
#ifdef USE_RTAI
	DGCstartMemberFunctionThread<T>(object, function, true);
#else
	DGCstartMemberFunctionThread<T>(object, function);
#endif
}

template <class T>
struct DGCObjectAndFunctionAndThreadArg
{
	T* object;
	void (T::*function)(void*);
	void* pThreadArgs;
};

template <class T>
void* DGCthread_funcWithArg(void *pArg)
{
// 	cout << "DGCthread_func called" << endl;
	(((DGCObjectAndFunctionAndThreadArg<T>*)pArg)->object->*((DGCObjectAndFunctionAndThreadArg<T>*)pArg)->function)(((DGCObjectAndFunctionAndThreadArg<T>*)pArg)->pThreadArgs);
	return NULL;
}

template <class T>
void DGCstartMemberFunctionThreadWithArg(T* object, void (T::*function)(void*), void* pThreadArgs)
{
// 	cout << "DGCstartMemberFunctionThread called" << endl;

	static pthread_t thread_id[MAX_THREADS];
	static DGCObjectAndFunctionAndThreadArg<T> objectAndFunctionAndThreadArg[MAX_THREADS];
	static int thread_index = 0;

	objectAndFunctionAndThreadArg[thread_index].object      = object;
	objectAndFunctionAndThreadArg[thread_index].function    = function;
	objectAndFunctionAndThreadArg[thread_index].pThreadArgs = pThreadArgs;

	if(pthread_create(&thread_id[thread_index], NULL, &DGCthread_funcWithArg<T>, (void*)&objectAndFunctionAndThreadArg[thread_index]) != 0)
	{
		cerr << "DGCstartMemberFunctionThreadWithArg: couldn't create thread" << endl;
	}

	thread_index++;
}

bool DGCcreateMutex(pthread_mutex_t* pMutex);
bool DGCdeleteMutex(pthread_mutex_t* pMutex);
bool DGClockMutex(pthread_mutex_t* pMutex);
bool DGCunlockMutex(pthread_mutex_t* pMutex);
bool DGCcreateCondition(pthread_cond_t* pCondition);
bool DGCcreateCondition(DGCcondition* dCondition);
bool DGCdeleteCondition(pthread_cond_t* pCondition);
bool DGCdeleteCondition(DGCcondition* dCondition);
bool DGCWaitForConditionTrue(bool& bCond, pthread_cond_t& cond, pthread_mutex_t& mutex);
bool DGCWaitForConditionTrue(DGCcondition& dCondition);
bool DGCSetConditionTrue(bool& bCond, pthread_cond_t& cond, pthread_mutex_t& mutex);
bool DGCSetConditionTrue(DGCcondition& dCondition);
void DGCusleep(unsigned long microseconds);

#endif
