function [path, rads] = path_from_corridor(corridor)
% 
% function path = path_from_corridor(corridor)
%
% Changes: 
%   12-6-2004, Jason Yosinski, created
%
% Function:
%   This function generates a somewhat optimal (max speed for any given
%   segment is within a factor of sqrt(2) of the optimal max speed I
%   think...) path from a corridor
%
% Input:
%   corridor = [x1 y1 width1 sl1;
%               x2 y2 width2 sl2;
%               .. .. ...... ...
%               xN yN widthN slN]
%       where width1 is the width of the corridor between (x1, y1) and 
%       (x2, y2).  sl1 is the speed limit imposed by the rddf on the first
%       corridor.  widthN and slN are irrelevant.
%
% Output:
%   path = [x1, y1, vx1, vy1, ax1, ay1;
%           x2, y2, vx2, vy2, ax2, ay2;
%           ..  ..  ...  ...  ...  ...;
%           xN, yN, vxN, vyN, axN, ayN]
%       The last point in a path matrix is the endpoint of the path,
%       therefore vxN, vyN, axN, and ayN are irrelevant.  The current path
%       infrastructure assumes that the velocity vector and the
%       acceleration vector are orthogonal, and this function will only
%       return such vectors.
%
% Usage example:
%   corridor = [0 0 10; 0 100 10; 100 200 10; 200 200 10];
%   path = path_from_corridor(corridor);


% get number of points in corridor
N = size(corridor,1);
%###
%disp('N = '); disp(N);
rads = [];
%###


% figure out if some l00zer passed us a messed up corridor with zero or one
% points in it
if N == 0
    error('You fool - the corridor passed to path_from_corridor is empty!');
    
elseif N == 1    
    % corridor is just a single point, so i guess our path says we're
    % already there! (path_from_corridor is passive-agressive)
    path = [corridor(1,1:2), 0, 0, 0, 0];
    
else
    % *whew*... finally... a challenge - a corridor with at least 2 points
    % in it!

    % do first path segment (starting location to halfway down first segment)
    % direction
    direction = (corridor(2,1:2) - corridor(1,1:2)) ./ ...
        mag(corridor(2,1:2) - corridor(1,1:2));
    % store first path segment
    path = [corridor(1,1:2), direction.*corridor(1,4), 0, 0];

    % do rest of path segments
    for i = 1:N-2
        % our story starts with a particular i.  we now start along the
        % corridor at a position equal to the midpoint of the "ith" segment

        % some useful stuff...
        %###
        %disp(i);
        %###
        corridor_segment = corridor(i:i+2,1:3);   % no speed limit needed for
                                                  % the curve funtions

        % extract points and widths from corridor_segment
        point1 = corridor_segment(1,1:2);
        point2 = corridor_segment(2,1:2);
        point3 = corridor_segment(3,1:2);
        width12 = corridor_segment(1,3);
        width23 = corridor_segment(2,3);
        % rays from point1 to point2 and point2 to point3
        ray12 = point2 - point1;
        ray23 = point3 - point2;
        % directions (normalized rays)
        dir12 = ray12 ./ mag(ray12);
        dir23 = ray23 ./ mag(ray23);
        % speed limits imposed by the rddf
        sl12 = corridor(i,4);
        sl23 = corridor(i+1,4);


        % figure out whether the length of the segment or the width of the
        % corridor limits the curvature

        % get width limited data
        [radiusW thetaW start_xW start_yW] = ...
            width_limited_curve(corridor_segment);

        % get length limited data
        [radiusL thetaL start_xL start_yL] = ...
            length_limited_curve(corridor_segment);
        
        %###
        %radiusL = radiusL + 75;
        %###
        
        %###
        %disp('[i, radiusL, radiusW] =');
        %disp([i, radiusL, radiusW]);
        %###

        % we now have two curves, each limited by a different factor.  we want
        % to use the one with the stricter limitation, namely the one with the
        % smaller radius of curvature.
        %###
        rads = [rads min(radiusL, radiusW)];
        %###
        if radiusL < radiusW

            % the limiting factor is the LENGTH of one of the corridor
            % segments (the shorter one), so next we'll figure out which
            % segment it was...
            if mag(point2 - point1) < mag(point3 - point2)
                % the first segment limited the curve, so the path continues
                % here with a curve
                path = [path; start_xL, start_yL, dir12.*min(sl12, sl23), ...
                    curve2accel(corridor_segment, radiusL, min(sl12, sl23))];

                % once that curve is done, the path continues on in a straight
                % line to at least the next midpoint
                % end of curve point (which != next midpoint)
                end_of_curve_point = point2 + dir23 .* radiusL .* tan(thetaL);
                path = [path; end_of_curve_point, dir23.*min(sl12, sl23), 0, 0];
            else
                % the second segment limited the curve, so the path continues
                % here first with a straght segment
                path = [path; point1 + .5.*ray12,  dir12.*sl12, 0, 0];

                % then comes the curve, which ends at the next midpoint
                path = [path; start_xL, start_yL, dir12.*min(sl12, sl23), ...
                    curve2accel(corridor_segment, radiusL, min(sl12, sl23))];
            end

        else
            % the limiting factor is the WIDTH of the corridor, so we need a
            % straight segment, then a curve, then another straight piece.
            path = [path; point1 + .5.*ray12, dir12.*sl12, 0, 0];
            path = [path; start_xW, start_yW, dir12.*min(sl12, sl23), ...
                curve2accel(corridor_segment, radiusW, min(sl12, sl23))];
            path = [path; point2 + dir23.*radiusW.*tan(thetaW), dir23.*sl23, ...
                0, 0];
        end
    end

    % do last actual path segment (straight line from midpoint of last
    % corridor segment to the last point in the corridor)
    point1 = corridor(N-1,1:2);     % second last point
    point2 = corridor(N,1:2);       % last point
    ray12 = point2 - point1;        % ray from second last point to last point
    sl12 = corridor(N-1,4);         % speed limit of last stretch of corridor
    path = [path; .5.*(point1+point2), ray12 ./ mag(ray12).*sl12, 0, 0];

    % tack on a final point to the path (so everyone else knows how long the
    % last path segment should be
    path = [path; point2, 0, 0, 0, 0];
end





function m = mag(vec)
% vec must be a row vector
m = sqrt(vec*transpose(vec));





function [radius, theta, start_x, start_y] = length_limited_curve(corridor_segment)
%
% function [radius, theta, start_x, start_y] = length_limited_curve(corridor_segment)
%
% Changes:
%   12-6-2004, Benjamin Pickett, created
%
% Function:
%   This function takes a corridor segment consisting of 3 points
%       corridor_segment = [x1 y1 width1;
%                           x2 y2 width2;
%                           x3 y3 width3]
% and calculates the circular arc with a radius defined by creating a
% smooth curve (continuous first derivative) such that:
%       1.  The arc starts tangent to the centerline between point1 and
%           point2.
%       2.  The arc ends tangent to the centerline between point2 and
%           point3.
%       3.  The start and end points are equidistant from the corner of the
%           corridor.
%       4.  The distance from (3) is defined to be the minimum of the two
%           leg lengths (point1 to point2 and point2 to point3).
%
%   The function returns the radius of the arc in question, the x and y
%   locations of the start of the arc (start_x and start_y), and theta.
%   Theta is half of the exterior angle between the two line segments 
%   joining the three points, MEASURED IN RADIANS.
%
% Usage example:
%   [radius, theta, start_x, start_y] = width_limited_curve(corridor_segment)
%

% extract points and widths from corridor_segment
point1 = corridor_segment(1,1:2);
point2 = corridor_segment(2,1:2);
point3 = corridor_segment(3,1:2);
% vectors from point1 to point2 and point2 to point3
v12 = point2-point1;
v23 = point3-point2;
% take the half the shorter of the two distances to the corner
d = min(mag(v12), mag(v23))/2;
% using half angle formula and dot product to find cos
% and using cos = u*v/|u||v|

%theta = acos(sqrt(((1+(v23)*(v12)')/(mag(v23)*mag(v12)))/2));
theta = .5 .* acos((v12*v23.') ./ (mag(v12).*mag(v23)));

radius = d/tan(theta);
% distance d from corner in the direction of v1
start = point2-(d*v12)/mag(v12);
start_x = start(1);
start_y = start(2);





function [radius, theta, start_x, start_y] = width_limited_curve(corridor_segment)
% 
% function [radius theta start_x start_y] = width_limited_curve(corridor_segment)
%
% Changes: 
%   12-6-2004, Jason Yosinski, created
%
% Function:
%   This function takes a corridor segment consisting of 3 points
%       corridor_segment = [x1 y1 width1;
%                           x2 y2 width2;
%                           x3 y3 width3]
%   and calculates the circular arc with the largest possible radius such
%   that:
%       1. The arc starts along and tangent to the centerline between
%           point1 and point2
%       2. The arc ends along and tangent to the centerline between point2
%           and point3
%       3. The arc just touches the inside corner of the two corridors
%           (meaning if it were any larger, the path would go outside the
%           corridor)
%   Note that the widths are radii rather than diameters, that is, the
%   corridor is actually 20 meters wide from outside edge to outside edge
%   if width = 10 meters
%
%   The function returns the radius of the arc in question, the x and y
%   locations of the start of the arc (start_x and start_y), and theta.
%   Theta is half of the exterior angle between the two line segments 
%   joining the three points, MEASURED IN RADIANS.
%
% Usage example:
%   [radius theta start_x start_y] = width_limited_curve(corridor_segment)

% extract points and widths from corridor_segment
point1 = corridor_segment(1,1:2);
point2 = corridor_segment(2,1:2);
point3 = corridor_segment(3,1:2);
width12 = corridor_segment(1,3);
width23 = corridor_segment(2,3);

% rays from point1 to point2 and point2 to point3
ray12 = point2 - point1;
ray23 = point3 - point2;

% min_width is the limiting corridor width, namely the smaller of the first
% and second corridor widths
min_width = min(width12, width23);

% calculate theta
theta = .5 .* acos((ray12*ray23.') ./ (mag(ray12).*mag(ray23)));

% calculate the radius
radius = min_width ./ (1 - cos(theta));

% calculate b, which is the distance from the intersection of the two
% centerlines to the start of the curve
b = radius .* tan(theta);

% calculate the start of curve point
start = point2 - (ray12./mag(ray12)) .* b;
% return the start point
start_x = start(1);
start_y = start(2);





function accel = curve2accel( corridor_segment, r, v)
 % function accel = curve2accel( corridor_segment, radius, speed)
 %
 % Changes:
 %   12-7-2004, Benjamin Pickett, created
 %
 % Function:
 %   This function takes a corridor segment consisting of 3 points
 %       corridor_segment = [x1 y1 width1;
 %                           x2 y2 width2;
 %                           x3 y3 width3]
 % and a predefined radius of curvature for the desired path.  The function
 % returns the centripetal acceleration required to begin traveling in a
 % circular arc from a point on the first leg of path segment (x1,y1) to
 % (x2,y2).  The acceleration returned by this function is designed to be
 % one of the components needed for defining a segment of path to send to
 % path following.
 %
 % Usage example:
 %   accel = curve2accel( corridor_segment, radius, speed)
 
 % extract points and widths from corridor_segment
 point1 = corridor_segment(1,1:2);
 point2 = corridor_segment(2,1:2);
 point3 = corridor_segment(3,1:2);
 % vectors from point1 to point2 and point2 to point3
 v12 = point2-point1;
 v23 = point3-point2;
 %project v23 onto v12
 projv23v12 = v12*(v23*v12')/(mag(v12)^2);
 %normalize orthogonal component of v23 and scale by |a| = v^2/r
 accel = (v^2/r)*(v23-projv23v12)/mag(v23-projv23v12);


		   %%% Note: the following added by Ben Pickett 1-21-2005 %%%

%this script creates a dense path from a sparse path.  I use the
%accelerations and velocities specified in the given path to determine the
%course of the path between path points.  I assume the path will be
%contiguous using the above information. ie, the velocity at point 2 will
%be achieved by maintaining the acceleration specified at point 1 until I
%get to point 2.  Under these assumptions, the path has a continuous
%time derivative and the second derivative is piecewise continuous.  All I
%have to do is fill in the spaces between points with more points and fill
%out the already smooth path.  If the path sent to this function is not
%smooth then this function will fail (probably silently).

		   %step size (point density), measured in meters/point
		   step = 1;

		   file = 'corridor_ben.txt';
		   cor = file2corridor(file);
		   path = path_from_corridor(cor);
		   %disp('path after velocity profiler');
		   %path = velocity_profile(path);
		   [N,M] = size(path);
		   path(N,3:6) = [0,0,0,0]; %quick stop at end
		   %figure(1);
		   %hold on;
		   path_dense = [];
		   for i = 1:N-1 %ignore last point for now
		   %path_dense = [path_dense;path]; %add next point
		   pt0 = path(i,1:2);
    
		   %plot(pt0(1),pt0(2),'go'); %first point in segment
		   v0 = mag(velocity);
    if v0 ~= 0 %avoid division by zero
		   uv = path(i,3:4)/v0;
		   else
		   uv = [0,0];
		   disp('set uv to zero');
    end
    
		   a0 = mag(path(i,5:6));
    if a0 ~= 0  %avoid division by zero
		   ua = path(i,5:6)/a0; %unit vector in direction of acceleration
		   a_tan = a0*(uv*ua')*uv; %tangential component of acceleration  
        a_perp = ua*a0 - a_tan; %perpendicular component of acceleration
    else
        %disp('set ua to zero');
        ua = [0,0];
        a_tan = [0,0];
        a_perp = [0,0];
    end
    %swap = 0; %for graphing
    if mag(a_perp) < 1e-6 %straight path segment (with tolerance)
        length = mag(path(i+1,1:2)-path(i,1:2));
        sign = uv*ua'; %uv and ua are parallel and unit vectors, product = +1 or -1
			       %disp(sign); %for debugging
			       for ds = 0:step:length %will this cause duplicate point at end?
			       point = pt0 + uv*ds; %current point distance ds from initial point in direction of v
			       %sign: direction of accel. + means forward, - means backwards.
            %this works because uv and ua have unit magnitudes and are
            %parallel.  If a0 == 0 then either equation is valid.
			       if (sign == 1 | a0 == 0)
			       path_dense = [path_dense;point, uv*sqrt(v0^2+2*a0*ds), ua*a0];
			       else if sign == -1
			       path_dense = [path_dense;point, uv*sqrt(v0^2-2*a0*ds), ua*a0];
			       else
			       disp('wtf');
			       disp(sign);
                end
            end
			       %{
            %print stuff
	       plot(point(1),point(2),'k.');
            plot([point(1),point(1)+uv(1)*sqrt(v0^2+2*a0*ds*sign)],[point(2),point(2)+uv(2)*sqrt(v0^2+2*a0*ds*sign)],'b-');
            plot([point(1),point(1)+ua(1)*a0*sign],[point(2),point(2)+ua(2)*a0*sign],'r--');
            %end print stuff
	       %}
        end
			       else %curved path segment
			       r = v0^2/mag(a_perp); %radius of curvature
			       center = pt0 + a_perp*r/mag(a_perp); %center of curvature
        
			       ray01 = path(i,1:2) - center; %from center to initial point of arc
			       ray02 = path(i+1,1:2) - center; %from center to final point

			       %plot([center(1), center(1)+ray01(1)], [center(2), center(2)+ray01(2)],'g-');
			       %plot([center(1), center(1)+ray02(1)], [center(2), center(2)+ray02(2)],'y-');

			       theta_min = atan2(ray01(2),ray01(1));
			       theta_max = atan2(ray02(2),ray02(1));
			       theta_step = step/r; %step size for theta so points are spaced correctly
        
        %force sweep angle to be between -pi and pi
			       sweep = atan2(sin(theta_max-theta_min),cos(theta_max-theta_min));

        if sweep < 0 %handle decreasing theta
			       theta_step = -theta_step;
        end
			       swap = 0;
			       for t = theta_min:theta_step:theta_min+sweep
			       point = center + r*[cos(t),sin(t)];
            if sweep > 0
			       sign = 1; %left turn
			       else
			       sign = -1; %right turn
            end
			       %add circular points, acceleration towards center of circle
			       ua = (center-point)/mag(center-point);
			       path_dense = [path_dense; point, v0*[cos(t+sign*pi/2),sin(t+sign*pi/2)], ua*a0];
            
			       %{
            %print stuff
	       plot([point(1),point(1)+v0*cos(t+sign*pi/2)],[point(2),point(2)+v0*sin(t+sign*pi/2)],'b-');
            plot([point(1),point(1)+ua(1)*a0],[point(2),point(2)+ua(2)*a0],'r-');
            %end print stuff
	       %}
        end
    end
end
			       %hold off;
%don't even worry about end point... its dense, right?

%{
%make into traj path
traj_path = [path_dense(:,2),path_dense(:,4),path_dense(:,6),path_dense(:,1),path_dense(:,3),path_dense(:,5)];
save traj_path_dense.txt traj_path -ascii -double
%}

figure(1);
hold on;
%plot_path(path);
plot_path_dense(path_dense);

%% end %%
