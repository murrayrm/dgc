FRAMES_PATH = $(DGC)/util/frames

FRAMES_DEPEND = \
	$(FRAMES_PATH)/rot_matrix.hh \
	$(FRAMES_PATH)/CMatrix.hh \
	$(FRAMES_PATH)/frames.hh \
	$(FRAMES_PATH)/coords.hh \
	$(FRAMES_PATH)/frames.cc \
	$(FRAMES_PATH)/frames.hh \
	$(FRAMES_PATH)/rot_matrix.hh \
	$(FRAMES_PATH)/CMatrix.hh \
	$(FRAMES_PATH)/CMatrix.cc \
	$(FRAMES_PATH)/test_coords.cc \
	$(FRAMES_PATH)/Makefile
