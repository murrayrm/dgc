/**
 * DGCKeepAlive.cc
 * Revision History:
 * 10/09/2005 hbarnor created
 * $Id$
 * DGCKeepAlive - wrapper that starts up DGCModules and logs
 * information to enable processMonitoring. 
 */

#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <ctime>

#include <limits.h>
#include <sys/stat.h>
#include <dirent.h>

#include "pm.h"

#define DEBUG 0
#define MAX_BUF_SIZE 1024
#define LOG_EXT ".pid"
using namespace std;
char * cur_id;

// forward declare


/**
 * spawn - forks, then calls execv to execute the module with the
 * options passed. 
 * @param argv - the arguments to the command and the command itself. 
 */
void spawn(char *argv[]);
/**
 * logProcess - logs the pid, the command and its arguments to the
 * DIR_RUN directory. Also logs the start time, the user, the key to
 * the DIR_LOG directory. 
 * @param pid - process id of process being kept alive.
 * @param argc - the number of elements in argument array.
 * @param argv - command and arguments of process being kept alive.
 * @param id - unique id used to turn off keep alive.
 * @return 0 if everything is okay -1 otherwise.
 */
int logProcess(pid_t pid, int argc, char * argv[], char * id);
/**
 * logRun - helper function that logs the pid and command line
 * arguments.  
 * @param pid - process id of process being kept alive.
 * @param argc - the number of elements in argument array.
 * @param argv - command and arguments of process being kept alive.
 * @return 0 if everything is okay -1 otherwise.
 */
int logRun(pid_t pid, int argc, char * argv[], char * id);

/**
 * logLog - helper function that logs the time, the user, the key 
 * and the command line arguments.  
 * @param argc - the number of elements in argument array.
 * @param argv - command and arguments of process being kept alive.
 * @param id - unique id used to turn off keep alive.
 * @return 0 if everything is okay -1 otherwise.
 */
int logLog(int argc, char * argv[]);
/**
 * fileExists - returns true if a filename passed as arg exists. 
 * @param filename - the name of the file to be tests.
 * @return true if it exists and false otherwise.
 */
bool fileExists(const char * filename);
/**
 * parseArgs - checks to see if there are DGCKeepAlive specific
 * arguments and processes them before calling the log and spawn
 * functions.
 * @param pid - the process id of the process being kept alive.
 * @param argc - the nmber of elements in the argument array
 * @param argv - the command and its arguments.  
 *
 */
int parseArgs(pid_t pid, int argc, char * argv[]);
// used to remove files, we longer care to keep alive
void ParseDir();
//string command;

int main(int argc, char *argv[])
{  
  //pid_t pid = spawn(argv+1);  
  pid_t pid = getpid();
  //int idx = parseArgs(argv);
  //logProcess(pid,argc-1,argv+1);
#if DEBUG
  cout << "Pid is " << pid << endl; 
#endif
  return parseArgs(pid, argc, argv);
  //return spawn(argv+1);  
}


void spawn(char * argv[])
{
  int status = execv(argv[0],argv);
#if DEBUG
  cerr << "Forked Module  exiting ....." << flush << endl;
  cerr << endl << endl;
#endif
  exit(status);
  // should never get this far 
}

int logProcess(pid_t pid, int argc, char * argv[], char * id)
{
  if ( (logRun(pid, argc, argv, id)) == 0)
    {
      return logLog(argc, argv);
    }
  else
    {
      return -1;
    }
}

int logRun(pid_t pid, int argc, char * argv[], char * id)
{
  stringstream fileName;
  //create file name
  int idx = 0;
  do
    {
      fileName.str("");
      fileName.clear();
      fileName << RUN_DIR << "/" << argv[0] << "." << idx++ << ".pid";
    }
  while(fileExists(fileName.str().c_str()));
    
#if DEBUG
  cout << fileName.str() << endl;
#endif
  ofstream runFile(fileName.str().c_str());
  if (! runFile) // Always test file open
    {
      cerr << "Error opening output file" << endl;
      return -1;
    }
  runFile << pid << flush << endl;
  runFile << argv[0];
  int i;
  for(i = 1; i < argc; i++)
    {
      runFile << " " << argv[i];
    }
  char* pSkynetkey = getenv("SKYNET_KEY");
  char path[PATH_MAX];
  getcwd(path, PATH_MAX);
  runFile << endl << path << endl;;
  runFile << pSkynetkey  << endl;
  runFile << id << flush << endl;
  runFile.close();
  return 0;  
}

int logLog( int argc, char * argv[])
{
  stringstream fileName;
  //create file name
  fileName << LOG_DIR << "/DGCKeepAlive.log";
#if DEBUG
  cout << fileName.str() << endl;
#endif
  ofstream logFile(fileName.str().c_str(), ios::app);
  if (! logFile) // Always test file open
    {
      cerr << "Error opening output file" << endl;
      return -1;
    }
  //get local time
  time_t seconds;
  struct tm * ltime;
  char * dispTime;
  time(&seconds);
  ltime = localtime(&seconds);
  dispTime = asctime(ltime);
  dispTime[strlen(dispTime)-1] = '\0';
  char * user = getlogin();
  char* pSkynetkey = getenv("SKYNET_KEY");
#if DEBUG 
  cout << "Time is " << dispTime << endl;
  cout << "User is " << user << endl;
  cout << "Environment key is" << pSkynetkey << endl;
#endif
  logFile << "[ " << dispTime << " ] " << user << " executed: (";
  logFile << argv[0];
  int i;
  for(i = 1; i < argc; i++)
    {
      logFile << " " << argv[i];
    }
  logFile <<") with $SKYNET_KEY=" << pSkynetkey;
  logFile << flush << endl;
  logFile.close();
  return 0;
}

bool fileExists(const char * filename)
{
  bool flag = false;
  fstream fin;
  fin.open(filename,ios::in);
  if( fin.is_open() )
    {
#if DEBUG
      cout<<"file exists"<<endl;
#endif
      flag=true;
    }
  fin.close();
  return flag;
}


int parseArgs(pid_t pid, int argc, char * argv[])
{
  int retVal = -1;
  if(strcmp("-id", argv[1]) == 0)
  {
#if DEBUG 
    cout << "Got an id " << endl;
#endif //DEBUG
    char * idValue = argv[2];
    retVal = logProcess(pid, argc-3, argv+3, idValue);
    spawn(argv+3);
  }
  else
    {
      if(strcmp("-kill", argv[1]) == 0 )
	{
#if DEBUG 
	  cout << " Got a kill " << endl;
#endif //DEBUG
	  cur_id = argv[2];
	  ParseDir();
	}
      else
	{
#if DEBUG 
	  cout << "No extra args " << endl;
#endif //DEBUG
	  retVal = logProcess(pid, argc-1, argv+1, NULL);
	  spawn(argv+1);
	}
    }
  return retVal;
}

  //need to check that we have args

  //   stringstream tempStream;
  //   int i;
  //   command.clear();
  //   tempStream << argv[1];
  //   for(i = 2; i < argc; i++)
  //     {
  //       tempStream << " " << argv[i];
  //     }
  //   command = tempStream.str();
  //  cout << command << endl;

//   int k = 0;
//   do
//     {
//       cout << environ[k++] << endl;;      
//     }
//   while(environ[k] != NULL);
//   exit(1);

//---------------------------------------------------------------------------
//Parse a simple 2 line config file containing pid on first line and 
//executable with cmd line options on the second line 
//
void ParseConfig(char *fname) 
{
  char mon_strbuf[MAX_BUF_SIZE];
  FILE * log =fopen(fname, "r");
  fgets(mon_strbuf, MAX_BUF_SIZE, log); // read line 1
  fgets(mon_strbuf, MAX_BUF_SIZE, log); // read the command
  fgets(mon_strbuf, MAX_BUF_SIZE, log); // read the directory
  fgets(mon_strbuf, MAX_BUF_SIZE, log); // read the skynetkey
  fgets(mon_strbuf, MAX_BUF_SIZE, log); // read the id if it exists
  fclose(log);
  // delete  \n in all these line
  mon_strbuf[strlen(mon_strbuf)-1] = '\0';
  if (strcmp(mon_strbuf, cur_id) == 0)
    {
      if (remove(fname) == -1) 
	{
	  perror("remove_file");
	}		             
    }
}


//---------------------------------------------------------------------------
//check if a dir entry is a file ending on .pid and if so parse it 
//
int chfile(const struct dirent *entry) 
{
  char full_fname[MAX_BUF_SIZE];
  struct stat f_stat;
  sprintf(full_fname,"%s/%s\0",RUN_DIR.c_str(),entry->d_name); 
  lstat(full_fname, &f_stat);	
  if (S_ISDIR(f_stat.st_mode) == 0)
    {
      if ((strlen(full_fname)>strlen(LOG_EXT)) && (strcmp(full_fname+strlen(full_fname)-strlen(LOG_EXT),".pid") == 0))
	ParseConfig(full_fname);
    }
  return 0;
}

//---------------------------------------------------------------------------
//Parse the log dir, checking each .pid file for existing pid and restarting 
//dead processes as needed
//
void ParseDir()
{
  struct dirent **namelist;
  int n;
  n = scandir(RUN_DIR.c_str(), &namelist, chfile, alphasort);
  free(namelist);
}


