//SUPERCON STRATEGY TITLE: End of RDDF - ANYTIME STRATEGY
//SOURCE FILE (.cc)
//See strategy header file for detailed documentation

#include "StrategyEndOfRDDF.hh"
#include "StrategyHelpers.hh"

void CStrategyEndOfRDDF::stepForward( const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface ) {

  skipStageOps.reset(); //resets to FALSE (this MUST remain here)
  currentStageName = endofrddf_stage_names_asString( endofrddf_stage_names(stgItr.nextStage()) );

  switch( stgItr.nextStage() ) {
    
  case s_endofrddf::estop_pause: 
    //end of RDDF has been reached -> estop pause
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	skipStageOps = trans_GPSreAcq( (*m_pdiag), currentStageName.c_str() );
      }
      if( skipStageOps == false ) {
	skipStageOps = trans_OutsideRDDF( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      /* ALL conditional checks should be completed ABOVE this point */
      stgItr.incrCnt(); //increment the stage loop counter

      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	
	m_pStrategyInterface->stage_superConEstop(estp_pause);
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "EndOfRDDF - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;
    
  case s_endofrddf::wait_for_DARPA: 
    //wait in this stage until either the rule saying that we are
    //at the end of the RDDF is NOT reached (rule changes) OR we
    //time-out waiting for DARPA to pause us, note that this is a
    //VERY long timeout
    {
      /* CONDITIONS GENERIC TO *ALL STRATEGIES* AND ALL STAGES  SHOULD BE EVALUATED *BELOW* THIS POINT */      
      
      /* CONDITIONS GENERIC TO *ALL* STAGES IN *THIS* STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      if( skipStageOps == false ) {
	skipStageOps = trans_GPSreAcq( (*m_pdiag), currentStageName.c_str() );
      }
      if( skipStageOps == false ) {
	skipStageOps = trans_OutsideRDDF( (*m_pdiag), currentStageName.c_str() );
      }
      
      /* CONDITIONS SPECIFIC TO THIS STAGE OF THE STRATEGY SHOULD BE EVALUATED *BELOW* THIS POINT */
      
      skipStageOps = trans_EstopPausedNOTsuperCon( (*m_pdiag), strprintf( "EndOfRDDF: %s", currentStageName.c_str() ) );
      
      if( m_pdiag->endOfRDDF ) {
	
	skipStageOps = true;
	SuperConLog().log( STR, WARNING_MSG, "StrategyEndOfRDDF: Still at end of RDDF -> will poll" );
	checkForPersistLoop( this, persist_loop_waitforDARPA, stgItr.currentStageCount() );		
      }
      
      /* ALL conditional checks should be completed ABOVE this point */       	
      stgItr.incrCnt(); //increment the stage loop counter
      
      /* EXECUTE OPERATIONS FOR THIS STAGE *BELOW* THIS POINT */
      if( skipStageOps == false ) {

	transitionStrategy( StrategyNominal, "StrategyEndOfRDDF: Reached end of traj rule now NOT TRUE -> NOMINAL" );
	SuperConLog().log( STR, STRATEGY_STAGE_MSG, "EndOfRDDF - Stage - %s completed", currentStageName.c_str() );
	stgItr.incrStage();
      }
    }
    break;    

    /* END OF STRATEGY */

  default:
    {
      //This point should *never* be reached - the final declared stage
      //should be a termination/transition stage
      cerr<<"ERROR: CStrategyEndOfRDDF::stepForward - default case in stage list reached final declared stage MUST be a termination/transition stage"<< endl;
      SuperConLog().log( STR, ERROR_MSG, "ERROR: CStrategyEndOfRDDF::stepForward - default case in nextStep switch() reached");
      transitionStrategy(StrategyNominal, "ERROR: EndOfRDDF - default stage reached!");
    }
  }
}


void CStrategyEndOfRDDF::leave( CStrategyInterface *m_pStrategyInterface )
{
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "EndOfRDDF - Exiting");
}

void CStrategyEndOfRDDF::enter( CStrategyInterface *m_pStrategyInterface )
{
  stgItr.reset(); //reset the stage iterator
  SuperConLog().log( STR, STRATEGY_TRANSITION_MSG, "EndOfRDDF - Entering");
}


