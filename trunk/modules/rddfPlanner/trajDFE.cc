/*
 * trajDFE
 * Created 02-23-2005 Ben Pickett
 *
 * trajDFE is Dynamic Feasibility code that modifies accelerations
 * and velocities in traj files to make them easier to follow.
 *
 * 02-26-2005 Ben Pickett
 * Check for division by zero in Recurse Up/Down functions, debug
*/

#include "trajDFE.hh" //includes appropriate path and vector dependencies

/* ensures velocity varies continuously over dense traj
 * and assigns corresponding longitudinal accelerations */
void TrajDFE(pathstruct & path)
{
  vector<double> vCurrent(2);
  vector<double> vPrevious(2);
  if (path.numPoints > 1) //else we can't do anything substantial
    {
       
      for (unsigned int i=1; i < path.numPoints; ++i)
	{
	
	  //make velocities around corners dynamically feasible
	  lower_velocity(path, i);

	  //extract useful information
	  //vectors are in the form <northing,easting>
	  vPrevious[0] = path.vn[i-1];
	  vPrevious[1] = path.ve[i-1];
	  vCurrent[0] = path.vn[i];
	  vCurrent[1] = path.ve[i];
	  
	  if (vecmagsquared(vCurrent) != vecmagsquared(vPrevious))
	    {
	      if (vecmagsquared(vCurrent) > vecmagsquared(vPrevious))
		recurse_accel(path,i);
	      else
		recurse_decel(path,i);
	    }
	}
    }
  return;
}

//force velocity to a value where the lateral acceleration
//is not greater than RDDF_MAXLATERAL
void lower_velocity(pathstruct& path, unsigned int i)
{
  double v1[2];
  double a1[2];
  double a1radial[2];
  v1[0]=path.vn[i]; v1[1]=path.ve[i];
  a1[0]=path.an[i]; a1[1]=path.ae[i];
  //make sure we are only dealing with radial component of acceleration
  vec_copy(a1radial,v1,2);
  vec_project(a1radial,a1,2);  //project a0 onto v0 to get tangential component
  a1radial[0]=a1[0]-a1radial[0]; a1radial[1]=a1[1]-a1radial[1];
  //adjust lateral accel (if necessary)
  if(vec_dot(a1radial,a1radial,2))
    set_lat_accel(path,i,fmin(vec_mag(a1radial,2),RDDF_MAXLATERAL));
}


/* the following are helper functions for UpdateVelocity
 * Check to see if the step change in accceleration is possible
 * If it is possible, then assign the necessary acceleration to
 * the previous point (to obtain the desired velocity at the
 * current point.
 * If it is not possible, then assign the maximum acceleration
 * and recurse to the previous point to continue checking.
 */
void recurse_accel(pathstruct& path, unsigned int current_point)
{
  vector<double> xPrevious(2);
  vector<double> xCurrent(2);
  vector<double> vPrevious(2);
  vector<double> vCurrent(2);
  vector<double> aTangential(2);
  if (current_point)
    {
      //extract some useful info
      xPrevious[0] = path.n[current_point-1];
      xPrevious[1] = path.e[current_point-1];
      xCurrent[0] = path.n[current_point];
      xCurrent[1] = path.e[current_point];

      vPrevious[0] = path.vn[current_point-1];
      vPrevious[1] = path.ve[current_point-1]; 

      //distance between points
      double stepsize = vecmag(xCurrent-xPrevious);
      if (stepsize)
	{
	  //check to see if step change in velocity is feasible
	  vCurrent[0] = path.vn[current_point];
	  vCurrent[1] = path.ve[current_point];
	  if (vecmagsquared(vCurrent) > vecmagsquared(vPrevious) + 2*RDDF_MAXACCEL*stepsize)
	    {
	      //set velocity (and corresponding radial acceleration)
	      if (vecmag(vCurrent)) //check for division by zero
		vCurrent = sqrt(vecmagsquared(vPrevious)+2*RDDF_MAXACCEL*stepsize)* (vCurrent/vecmag(vCurrent));
	      SetVel(path,current_point, vecmag(vCurrent) );
	      //call function recursively
	      recurse_accel(path, current_point-1);
	    }
	  
	  aTangential = (vPrevious/vecmag(vPrevious)) * (vecmagsquared(vCurrent)-vecmagsquared(vPrevious))/(2*stepsize);

	  /*
	  //debug output **
	  cout << endl <<  "Point: " << current_point-1 << " accel recursion " << endl;	 
	  if (vecmag(aTangential) > RDDF_MAXACCEL*1.1 ) //this is a problem
	    {
	      cout << "V Previous: " << vPrevious[0]<<vPrevious[1]<< " V Current: " << vCurrent[0]<<vCurrent[1]<< "Step: " << stepsize << endl;
	      cout << "Acceleration Magnitude: " << (vecmagsquared(vCurrent)-vecmagsquared(vPrevious))/(2*stepsize) << endl << endl;
	    }
	  else
	      vecprint(aTangential);
	  //end debug output **
	  */

	  //I am assuming the tangential acceleration is zero to begin with (in the origional traj file).
	  //I should probably fix this to make it more robust.... at some point.
 	  path.an[current_point-1] = path.an[current_point-1] + aTangential[0];
	  path.ae[current_point-1] = path.ae[current_point-1] + aTangential[1];
	  //cout << path.an[current_point-1] << path.ae[current_point-1] << endl;
	 }
      else
	{
	  if (vecmagsquared(vPrevious))
	    {
	      //the two conescutive points are ontop of one another
	      //so we want them to be exactly the same, unless the first
	      //point has zero velocity, in which case we want to maintain
	      //the second point's values.
	      path.vn[current_point] = path.vn[current_point-1];
	      path.ve[current_point] = path.ve[current_point-1];
	      path.an[current_point] = path.an[current_point-1];
	      path.ae[current_point] = path.ae[current_point-1];
	    }
	}
    }
}

void recurse_decel (pathstruct& path, unsigned int current_point)
{
  vector<double> xPrevious(2);
  vector<double> xCurrent(2);
  vector<double> vPrevious(2);
  vector<double> vCurrent(2);
  vector<double> aTangential(2);
  if (current_point)
    {
      //extract some useful info
      xPrevious[0] = path.n[current_point-1];
      xPrevious[1] = path.e[current_point-1];
      xCurrent[0] = path.n[current_point];
      xCurrent[1] = path.e[current_point];

      //distance between points
      double stepsize = vecmag(xCurrent-xPrevious);
      if (stepsize)
	{
	  //check to see if step change in velocity is feasible
	  vPrevious[0] = path.vn[current_point-1];
	  vPrevious[1] = path.ve[current_point-1];
	  vCurrent[0] = path.vn[current_point];
	  vCurrent[1] = path.ve[current_point];
	  if (vecmagsquared(vPrevious) > vecmagsquared(vCurrent) + 2*RDDF_MAXDECEL*stepsize)
	    {
	      if (vecmagsquared(vPrevious)) //check for division by zero
		vPrevious = sqrt(vecmagsquared(vCurrent)+2*RDDF_MAXDECEL*stepsize) * vPrevious/vecmag(vPrevious);
	      SetVel(path,current_point-1, vecmag(vPrevious) );
	      //call function recursively
	      recurse_decel(path, current_point-1);
	    }
	  aTangential = (vPrevious/vecmag(vPrevious)) * (vecmagsquared(vCurrent)-vecmagsquared(vPrevious))/(2*stepsize);

	  /*
	  //debug output **
	  cout << endl <<  "Point: " << current_point-1 << " decel recursion " << endl;	 
	  if (vecmag(aTangential) > RDDF_MAXDECEL*1.1 ) //this is a problem
	    {
	      cout << "V Previous: " << vPrevious[0]<<vPrevious[1]<< " V Current: " << vCurrent[0]<<vCurrent[1]<< "Step: " << stepsize << endl;
	      cout << "Acceleration Magnitude: " << (vecmagsquared(vCurrent)-vecmagsquared(vPrevious))/(2*stepsize) << endl << endl;
	    }
	  else
	      vecprint(aTangential);
	  //end debug output **
	  */

	  //I am assuming the tangential acceleration is zero to begin with (in the origional traj file.
	  //I should probably fix this to make it more robust.... at some point.
	  path.an[current_point-1] = path.an[current_point-1] + aTangential[0];
	  path.ae[current_point-1] = path.ae[current_point-1] + aTangential[1];
	  //vecprint(aTangential);
	}
      else
	{
	  if (vecmagsquared(vPrevious))
	    {
	      //the two conescutive points are ontop of one another
	      //so we want them to be exactly the same, unless the first
	      //point has zero velocity, in which case we want to maintain
	      //the second point's values.
	      path.vn[current_point] = path.vn[current_point-1];
	      path.ve[current_point] = path.ve[current_point-1];
	      path.an[current_point] = path.an[current_point-1];
	      path.ae[current_point] = path.ae[current_point-1];
	    }
	}
    }
}



//some path manipulation functions to make my life easier up above

//copy rhs to lhs (must be same size arrays)
void vec_copy(double arr1[], double arr2[],unsigned int length)
{
  while (length--)
    arr1[length] = arr2[length];
}

//inner vector product (also used to calculate magnitude)
double vec_dot(double arr1[], double arr2[], unsigned int length)
{
  double dotproduct = 0;
  while (length--)
    dotproduct += arr1[length]*arr2[length];
  return dotproduct;
}

//vector magnitude
double vec_mag(double arr[], unsigned int length)
{
  return sqrt(vec_dot(arr,arr,length));
}


//project arr2 onto arr1, store in arr1
void vec_project(double arr1[], double arr2[], unsigned int length)
{
  double dotproduct12 = vec_dot(arr1,arr2,length);
  double magsquared1 = vec_dot(arr1,arr1,length);
  if(magsquared1) //check to avoid dividing by zero
    while (length--)
      arr1[length] = arr1[length]*dotproduct12/magsquared1;
  else
    while (length--)
      arr1[length]=0;
}

//set the lateral acceleration to an arbitrary value
//and assign the corresponding velocity so that the radius
//remains constant (maintain path integrith)
void set_lat_accel(pathstruct& path, unsigned int i, double accelmag_desired)
{
  double a[2]; //current accel (vector)
  double a_tangential[2]; //tangential component of acceleration
  double a_radial[2]; //radial component of acceleration
  double v[2]; //current velocity
  double r; //radius of curvature
 
  //extract some info
  v[0] = path.vn[i]; v[1] = path.ve[i]; 
  a[0] = path.an[i]; a[1] = path.ae[i];

  vec_copy(a_tangential,v,2);
  vec_project(a_tangential,a,2);
  a_radial[0]=a[0]-a_tangential[0]; a_radial[1]=a[1]-a_tangential[1];

  //if magnitude is zero, we don't need to change anything anyway
  if(vec_dot(v,v,2))
    {
      double vnew[2];
      //assign new velocity
      r = vec_dot(v,v,2)/vec_mag(a_radial,2);
      path.vn[i]=vnew[0]=sqrt(r*accelmag_desired)* v[0]/vec_mag(v,2);
      path.ve[i]=vnew[1]=sqrt(r*accelmag_desired)* v[1]/vec_mag(v,2);
    }
  //if magnitude is zero, we don't need to change anything anyway
  if(vec_dot(a_radial,a_radial,2))
    {
      //assign new acceleration
      //a_tangential + a_radial(vnew/vold)^2
      path.an[i]=a_tangential[0]+accelmag_desired*(a_radial[0]/vec_mag(a_radial,2));
      path.ae[i]=a_tangential[1]+accelmag_desired*(a_radial[1]/vec_mag(a_radial,2));
    }
}
