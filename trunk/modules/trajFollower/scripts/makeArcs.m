function makeArcs()

#this file creates a static trajectory of half circles, 
#at velocity specified by v, at angles specified by theta.
#to change the length of the segments, change what dist goes to,
#and to change the spacing of points, change the incrementing of dist.

#This function will mainly be useful for trajFollower to have trajectories
# that are specified exactly, with perfect FF around the corners and constant
# velocity (as opposed to planner and pathgen paths)

     v = 6;
     oldn = 3831723;
     olde = 449273;

     N = [];
     E = [];
     Ndot = [];
     Edot = [];
     Ndotdot = [];
     Edotdot = [];

     count = 0;

     for radius=35:-5:5 
          olde = olde-radius;
     
          a = -v*v/radius;

          for theta = 0:pi/(5*radius):pi

               sn = sin(theta);
               cs = cos(theta);

               count = count + 1;
               
               N = [N;radius*sn+oldn];
               E = [E;radius*cs+olde];
               Ndot = [Ndot; v*cs];
               Edot = [Edot; -v*sn];
               Ndotdot = [Ndotdot; a*sn];
               Edotdot = [Edotdot; a*cs];

          endfor
	  
          oldn = N(count);
          olde = E(count);

# now going straight...


          for dist = 0:.1:15

               count = count + 1;
               
               N = [N;-dist+oldn];
               E = [E;olde];
               Ndot = [Ndot; -v];
               Edot = [Edot; 0];
               Ndotdot = [Ndotdot; 0];
               Edotdot = [Edotdot; 0];

          endfor
	  
          oldn = N(count);
          olde = E(count);




          olde = olde-radius;

          for theta = 0:pi/(10*radius):pi
	 
               sn = sin(-theta);
               cs = cos(theta);

               count = count + 1;
               
               N = [N;radius*sn+oldn];
               E = [E;radius*cs+olde];
               Ndot = [Ndot; -v*cs];
               Edot = [Edot; v*sn];
               Ndotdot = [Ndotdot; a*sn];
               Edotdot = [Edotdot; a*cs];

          endfor
	  
          oldn = N(count);
          olde = E(count);

# now going straight...

          for dist = 0:.1:15

               count = count + 1;
               
               N = [N;dist+oldn];
               E = [E;olde];
               Ndot = [Ndot; v];
               Edot = [Edot; 0];
               Ndotdot = [Ndotdot; 0];
               Edotdot = [Edotdot; 0];

          endfor
	  
          oldn = N(count);
          olde = E(count);







     endfor


     traj = [N,Ndot,Ndotdot,E,Edot,Edotdot];

plot(E,N);

     save -ascii arc6_small.traj traj

%bob=[];
%for i=1:1:size(traj)(1)
%  if(mod(i,300)=1)
%    bob=[bob;i,traj(i,4), traj(i,1), v, 4];
%  endif
%endfor

%save -ascii arc2b.bob bob

