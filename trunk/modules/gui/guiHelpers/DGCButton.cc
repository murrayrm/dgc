/**
 * DGCButton.cc
 * Revision History:
 * 07/08/2005  hbarnor  Created
 * $Id: DGCButton.cc 8500 2005-07-09 10:45:45Z hbarnor $
 */

#include "DGCButton.hh"

DGCButton::DGCButton(const Glib::ustring& label)
  : DGCWidget(DGCBUTTON),
    Button(label)

{
  //
}
