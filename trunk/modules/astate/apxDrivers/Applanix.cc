/* \file Applanix.hh
 * \brief Applanix POS-LV drivers definition.
 * \author Stefano Di Cairano,
 * \date dec-06
 */
 
#include "apxTest.hh"

using namespace std;
#define MAX_MSG_SIZE 200
#define DEBUG_LEVEL 5



/* ****************************************************
 * ****************** CHECKSUM ************************
 * ****************************************************/

// *************COMPUTE CHECKSUM **********************

unsigned short apxCompChecksum(char* inBuffer,int size)
{
	
  char buffer[size];
  memcpy(buffer, inBuffer, size);	
  unsigned short checksum=0;
  unsigned short len=sizeof(buffer);
  if(len%2>0)
  {
    cout<<"There is an error here";	
  }
  else
  {
  	
    unsigned short nTerms=len/2;
    unsigned short total=0;
    unsigned short elem=0;  
    for(short i=0; i<nTerms; i++)
    {
      memcpy(&elem,&buffer[2*i],2);
      total+=elem; //here we implicitly get the modulo
      elem=0;
    }  
  checksum=65536-total;
  
  }
  return(checksum);
}


// *************VERIFY CHECKSUM **********************

unsigned short apxVerChecksum(char* inBuffer,int size)
{
	
  char buffer[size];
  memcpy(buffer, inBuffer, size);
  unsigned short len=sizeof(buffer);	
  unsigned short total=0;
  if(len%2>0)
  {
    cout<<"There is an error here";	
  }
  else
  {
  	
    unsigned short nTerms=len/2;
    unsigned short elem=0;  
    for(short i=0; i<nTerms; i++)
    {
      memcpy(&elem,&buffer[2*i],2);
      total+=elem; //here we implicitly get the modulo
      elem=0;
    }    
  }
  return(total); //this must be 0
}




/* ************************************
 * ********** APX_DATA STRUCT *********
 * ************************************/


void parseNavData(char* inBuffer, int size, apxNavData* msg)
{
  char buffer[size];
  memcpy(buffer, inBuffer, size);
  memset(&(msg->groupStart[0]),'/0',5);
  memcpy(&(msg->groupStart[0]),&buffer[0],2);  
  
  
  memcpy(&(msg->ID),&buffer[4],2);  
  memcpy(&(msg->byteCount),&buffer[6],2);
  memcpy(&(msg->time1),&buffer[8],8);
  memcpy(&(msg->time2),&buffer[16],8);
  memcpy(&(msg->distanceTag),&buffer[24],8);
  memcpy(&(msg->timeTypes),&buffer[32],1);
  memcpy(&(msg->distanceType),&buffer[33],1);
  memcpy(&(msg->latitude),&buffer[34],8);
  memcpy(&(msg->longitude),&buffer[42],8);
  memcpy(&(msg->altitude),&buffer[50],8);
  memcpy(&(msg->northVelocity),&buffer[58],4);
  memcpy(&(msg->eastVelocity),&buffer[62],4);
  memcpy(&(msg->downVelocity),&buffer[66],4);
  memcpy(&(msg->roll),&buffer[70],8);
  memcpy(&(msg->pitch),&buffer[78],8);
  memcpy(&(msg->heading),&buffer[86],8);
  memcpy(&(msg->wander),&buffer[94],8);
  memcpy(&(msg->trackAngle),&buffer[102],4);
  memcpy(&(msg->speed),&buffer[106],4);
  memcpy(&(msg->rollRate),&buffer[110],4);
  memcpy(&(msg->pitchRate),&buffer[114],4);
  memcpy(&(msg->yawRate),&buffer[118],4);
  memcpy(&(msg->longitudinalAcc),&buffer[122],4);
  memcpy(&(msg->transverseAcc),&buffer[126],4);
  memcpy(&(msg->downAcc),&buffer[130],4);
  memcpy(&(msg->alignStatus),&buffer[134],1);
  memcpy(&(msg->pad),&buffer[135],1);
  memcpy(&(msg->checksum),&buffer[136],2);
  memset(&(msg->groupEnd[0]),'\0',3);
  memcpy(&(msg->groupEnd[0]),&buffer[138],2);
 
}





/* *****************************************
 * ************* Navigation Mode ***********
 * *****************************************/




// ********* BUILD MESSAGE ***************

void buildNavModeMsg(apxNavModeMsg* msg, unsigned short transaction) //this will be made more flexible later on
{ 
  msg->transNumber=transaction;
}

// ******** SERIALIZE NAV MODE MESSAGE *******

void serialNavModeMsg(apxNavModeMsg* msg, char* inBuffer)
{ 
  char buffer[16];
  memcpy(&buffer[0],msg->msgStart,4);
  memcpy(&buffer[4],&(msg->ID),2);
  memcpy(&buffer[6],&(msg->byteCount),2);
  memcpy(&buffer[8],&(msg->transNumber),2);
  memcpy(&buffer[10],&(msg->navMode),1);\
  memcpy(&buffer[11],&(msg->pad),1);  	
  memcpy(&buffer[12],&(msg->checksum),2);  	 
  memcpy(&buffer[14],(msg->msgEnd),2);
  unsigned short checksum=apxCompChecksum(buffer,16);
  memcpy(&buffer[12],&checksum,2);  	   
  memcpy(inBuffer, buffer, 16);
}



/* *****************************************
 * ************** SELECT DATA GROUPS *******
 * *****************************************/




// ********* BUILD MESSAGE ***************

void buildSelGroupsMsg(apxSelGroupsMsg* msg, unsigned short transaction) //this will be made more flexible
{ 
  msg->transNumber=transaction;
  msg->numberOfGroups=1;
  msg->groupID=60;  //I set to 10 the max number of groups we can get. This can be changed in the future
  msg->dataRate=200;
  msg->pad=0;
  msg->byteCount=10+2*(msg->numberOfGroups);
}


// ******** SERIALIZE MESSAGE *******

void serialSelGroupsMsg(apxSelGroupsMsg* msg, char* inBuffer, int size)
{ 
  char buffer[size];
  memcpy(&buffer[0],msg->msgStart,4);
  memcpy(&buffer[4],&(msg->ID),2);
  memcpy(&buffer[6],&(msg->transNumber),2);
  memcpy(&buffer[8],&(msg->numberOfGroups),2);
  memcpy(&buffer[10],&(msg->groupID),2);
  memcpy(&buffer[12],&(msg->dataRate),2);
  memcpy(&buffer[14],&(msg->checksum),2);
  memcpy(&buffer[16],&(msg->pad),2);  	
  memcpy(&buffer[18],(msg->msgEnd),2);
  unsigned short checksum=apxCompChecksum(buffer,size);
  memcpy(&buffer[14],&checksum,2); 
  memcpy(inBuffer, buffer, 20);
}





/* *****************************************
 * ***** ACKNOWLEDGE MESSAGE ****************
 * *****************************************/
 
void parseAckMsg(char* inBuffer, int size, apxAckMsg* msg)
{
  char buffer[size];
  memcpy(&(buffer[0]), inBuffer, size);
  memset(&(msg->msgStart[0]),'/0',5);
  memcpy(&(msg->msgStart[0]),&buffer[0],2);  
  memcpy(&(msg->ID),&buffer[4],2);  
  memcpy(&(msg->byteCount),&buffer[6],2);
  memcpy(&(msg->transNumber),&buffer[8],2);
  memcpy(&(msg->recMsgID),&buffer[10],2);
  memcpy(&(msg->responseCode),&buffer[12],2);
  memcpy(&(msg->newParamStatus),&buffer[14],1);
  memcpy(&(msg->rejectedParamName),&buffer[15],32);
  memcpy(&(msg->pad),&buffer[47],1);
  memcpy(&(msg->checksum),&buffer[48],2);
  memset(&(msg->msgEnd[0]),'/0',3);
  memcpy(&(msg->msgEnd[0]),&buffer[50],2);  	
} 



/* *****************************************
 * *********** CONTROL MESSAGE  ************
 * *****************************************/






// ********* BUILD MESSAGE ***************

void buildControlMsg(apxControlMsg* msg, unsigned short transaction, unsigned short control) //this will be made more flexible later on
{ 
  msg->transNumber = transaction;
  msg->control = control;
}

// ******** SERIALIZE MESSAGE *******

void serialControlMsg(apxControlMsg* msg, char* inBuffer)
{ 
  char buffer[16];
  memcpy(&buffer[0],msg->msgStart,4);
  memcpy(&buffer[4],&(msg->ID),2);
  memcpy(&buffer[6],&(msg->byteCount),2);
  memcpy(&buffer[8],&(msg->transNumber),2);
  memcpy(&buffer[10],&(msg->control),2);
  memcpy(&buffer[12],&(msg->checksum),2);  	 
  memcpy(&buffer[14],(msg->msgEnd),2);
  unsigned short checksum=apxCompChecksum(buffer,16);
  memcpy(&buffer[12],&checksum,2);  	   
  memcpy(inBuffer, buffer, 16);
}




/* ****************************************************
*  ************* TCP-IP functions *********************
*  ****************************************************/

// **************** SEND ******************************

short apxTcpSend(int* socket, char* message, short sizeMsg)
{
  short totBytes = 0;            // total bytes we have sent
  short remBytes = sizeMsg;  // how many we have left to send
  short sentBytes = 0;           // how many bytes we have sent this round 
  short comStatus = 1;		   // status of the communication, -1 = failure
      
      //send the message: as a cycle in case of packet splitting
      
  while( (totBytes< remBytes) && (comStatus > 0) )
  {
    sentBytes = send(*socket, &message[totBytes], remBytes, 0);
    if (sentBytes < 1)  //0 = socket closed; -1 = transfer error;
    { 
      comStatus = 0; //error!!!
    } 
    else
    {
      totBytes += sentBytes;
      remBytes -= sentBytes;
    }
  }
  return(comStatus);
}


// **************** RECEIVE ******************


short apxTcpReceive(int* socket, char* message, short msgLength)
{       
  short comStatus = 1;                    //set to 0 if there are errors        
  short recBytes = 0;
  short totBytes = 0;
  short lengthKnown = 1;
  char swapBuffer[MAX_MSG_SIZE];  //put an appropriate constant here e.g. #define MAX_MSG_SIZE 140
  char buffer[APX_BUFFER_SIZE];

  if (msgLength<1)
  {
    msgLength=APX_BUFFER_SIZE;
    lengthKnown = 0;  
  }
    
  while ((recBytes < msgLength) && (comStatus > 0))
  {
    recBytes=recv(*socket, buffer, APX_BUFFER_SIZE, 0);
    
    if(recBytes < 1)
    {
      comStatus = 0;     
    }
    else
    {
      unsigned short realLength;       
      memcpy(&realLength,&buffer[LENGTH_BYTE],2);
     
      if (totBytes==0) //if we are at the beginning of the msg
      { 
        char isGroup[5]="$GRP";
        char isMessage[5]="$MSG";
        char isMine[5]="nnnn";       //initialization to have a null-terminated string...

        memcpy(&isMine[0],&buffer[0],4);
        
        if( ( strcmp(isMine,isGroup)!=0 )  && ( strcmp(isMine,isMessage)!=0  ) )
        {
          comStatus = 0;           //this packet is out of synchrony
          
          if(DEBUG_LEVEL > 4)
          { 
          	time_t badtime=time(NULL);
          }
         }
	     else
	     {
	       if(lengthKnown && (( realLength+LENGTH_OFFSET)!=msgLength))   //this msg is not what we want
	       {
             comStatus = 0;
	      
	         if(DEBUG_LEVEL>4)
	         { time_t badtime=time(NULL); 
		       for(int i=0;i<recBytes-3;i++)
		       { 
	             memcpy(&isMine,&buffer[i],4);
	          /* if(strcmp(isMine,isGroup)==0)
		         {	  
	               cout<<"group found at "<< badtime << "size: "<< recBytes << endl;
		         }
		         */
		       }
	         }

		     msgLength=realLength+LENGTH_OFFSET;   //update to the real msg length
	       }
        }	                  
        if( (!lengthKnown) && (comStatus > 0) )
        {
          msgLength = realLength+LENGTH_OFFSET;
          lengthKnown = 1;
        }
      }
      if((totBytes + recBytes <= msgLength) && (comStatus>0))
      {
        memcpy(&swapBuffer[totBytes], buffer, recBytes);                //copy the temp buffer into message buffer
        totBytes += recBytes;
      }
      else
      {
      	comStatus = 0;     	
      }        
    }
  }
  if(comStatus>0)
  {
    memcpy(message, swapBuffer, msgLength);                //copy the temp buffer into message buffer
  }
  return(comStatus);
}




/*
 * This is code for a simple drivers test. Not to be used in std operations but useful for testing purposes 
  
int main()
{   
	cout << " DONE!!!!" <<endl;
	return(0);
}
*/

/*
int main(int argc, char **argv)
{
  char buffer[NAV_GRP_LENGTH];
  int apxControlSocket=socket(PF_INET, SOCK_STREAM, 0);// create the data socket
  int apxDataSocket=socket(PF_INET, SOCK_STREAM, 0); // create the data socket
  time_t initTime;
  time_t currentTime;

  
  struct sockaddr_in apxControl_addr, apxData_addr;					// these structs contains the addresses 
 
  apxControl_addr.sin_family = AF_INET;								// IPv4 addressing
  apxControl_addr.sin_port = htons(APX_CONTROL_PORT);                   
  apxControl_addr.sin_addr.s_addr = inet_addr(APX_IP_ADDR);
  
  memset(&(apxControl_addr.sin_zero), '\0', 8); 				    //required by the structure	
  apxData_addr.sin_addr.s_addr = inet_addr(APX_IP_ADDR);  
  apxData_addr.sin_family = AF_INET;
  apxData_addr.sin_port = htons(APX_DATA_PORT);  					
  
  memset(&(apxControl_addr.sin_zero), '\0', 8);
  
  if (connect(apxDataSocket,(struct sockaddr *)&apxData_addr,sizeof(apxData_addr)) < 0)
  { 
//    cout << "ERROR connecting to Applanix Data Port" << endl;
 //   cout<< "Error: "<<strerror(errno)<<" , " << endl;
  //  sleep(1);
    exit(1);
  }
  else 
  { 
    cout<< "Connected to Data Port" << endl;
   // sleep(1); 
  }
  int count = 0;  
  int goodCount = 0;
  initTime=time(NULL);
  currentTime=time(NULL);
  while(currentTime-initTime< 600)
  {     
        apxNavData navMsg;
	count++;
  	short comStatus = apxTcpReceive(&apxDataSocket , buffer , sizeof(buffer));
  	if(comStatus > 0)
	{
	  goodCount++;
	  unsigned short v;
	         
          memcpy(&v,&buffer[4],2);
	  if(v!=1)		  
          {
            cout << "I have got a message here... this shall not be: "<<count << endl;
            exit(1);
	  }
	  parseNavData(buffer, sizeof(buffer), &navMsg);
  	  if (DEBUG_LEVEL>4)
  	  {	
  	   cout << "This should be the group ID: " << navMsg.byteCount << endl;
            cout << "Lat :" << navMsg.latitude << " Long: " << navMsg.longitude << " Hgt: " << navMsg.altitude << endl;
            cout << "velN :" << navMsg.northVelocity << " velE: " << navMsg.eastVelocity << " velD: " << navMsg.downVelocity << endl;
            cout << "R :" << navMsg.roll << " P: " << navMsg.pitch << " Y: " << navMsg.heading << endl;
//  	    usleep(10000);
  	  }
  	}
//    if(count%5000==0) {cout<<count<<endl;}
    currentTime = time(NULL);
  }
  cout << goodCount << " vs " << count;
}

*/
