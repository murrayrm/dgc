/* \file AState.cc 
 * \brief Astate class definition
 * \author Stefano Di Cairano,
 * \date dec-06
 */
 
#include "AState.hh"

/*! Astate Constructor. 
 * /param skynetKey: integer defining the skynet key to be used
 * /param options: astateOpts struct defining user options
 */
 
AState::AState(int skynetKey, astateOpts options):
CSkynetContainer(SNastate, skynetKey),
CTimberClient(timber_types::astate)
{
 /*! \param snKey: integer, the skynet key
  */
  snKey = skynetKey;
  _options = options;
  quitPressed = 0;


  apxCount = 0;
  apxDead = 1;
  apxStatus = NO_SOLUTION; // check the table
  timberEnabled = 0;
  apxEnabled = 1;
  apxActive = 1;
  
  
  //Log Files Setup
  
  if(_options.logRaw == 1 )
  {
    cout << "Activating logging" << endl;	  
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);
  	
    mkdir("/tmp/logs/astate_raw/",
          S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH
          | S_IXOTH);
    
    sprintf(apxLogFile,
            "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_APX.raw",
            local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
            local->tm_hour, local->tm_min, local->tm_sec);

    sprintf(stateLogFile,
            "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_State.raw",
            local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
            local->tm_hour, local->tm_min, local->tm_sec);
    
    apxLogStream.open(apxLogFile, fstream::out | fstream::binary);
    stateLogStream.open(stateLogFile, fstream::out | fstream::binary);
    
  }
  
  
  apxHeight = -1;
  apxNorth = -1;
  apxEast = -1;
  apxVelN = -1;
  apxVelE = -1;
  apxVelD = -1;
  apxAccN = -1;
  apxAccE = -1;
  apxAccD = -1;
  apxRoll = -1;
  apxPitch = -1;
  apxHeading = -1;
  apxRollRate = -1;
  apxPitchRate = -1;
  apxHeadingRate = -1;
  
  apxHeight_accuracy = 1;
  apxNorth_accuracy = -1;
  apxEast_accuracy = -1;
  apxVelN_accuracy = -1;
  apxVelE_accuracy = -1;
  apxVelD_accuracy = -1;
  apxAccN_accuracy = -1;
  apxAccE_accuracy = -1;
  apxAccD_accuracy = -1;
  apxRoll_accuracy = -1;
  apxPitch_accuracy = -1;
  apxHeading_accuracy = -1;
  apxRollRate_accuracy = -1;
  apxPitchRate_accuracy = -1;
  apxHeadingRate_accuracy = -1;
  
  
  //Initialize the sockets
  if(options.stateReplay == 1)
  {
  	// name is hard-coded for now
  	cout << "Ready for  state replay: wait 3 seconds..." << endl;
	sleep(3);
  	replayStateStream.open("/tmp/logs/astate_raw/stateLog.raw", fstream::in | fstream::binary);
    if(!replayStateStream)
    {
      cout << "error: replay file is missing" << endl;
      exit(1);
    }
  	apxActive = 0;
  }
  else
  {
    apxDataSocket=socket(PF_INET, SOCK_STREAM, 0); 	// create the data socket
    if(apxDataSocket<0)
    {
  	  cout << "ERROR while creating data socket" << endl;
	  exit(1);
    }
    struct sockaddr_in apxControl_addr, apxData_addr;					// these structs contains the addresses 
    apxData_addr.sin_addr.s_addr = inet_addr(APX_IP_ADDR);  
    apxData_addr.sin_family = AF_INET;
    apxData_addr.sin_port = htons(APX_DATA_PORT);  					
 	
    memset(&(apxControl_addr.sin_zero), '\0', 8);
    cout<<" Attempting a connection" << endl; 
    if (connect(apxDataSocket,(struct sockaddr *)&apxData_addr,sizeof(apxData_addr)) < 0)
    {  
      cout << ("ERROR connecting to Applanix Data Port") << endl;
      cout<< "Error: "<<strerror(errno) << endl;
      apxActive = 0;
    }
    else 
    { 
      apxActive = 1;
      cout<< "Connected to Data Port" << endl; 
    }
  
    stateMode = PREINIT;
  }
  
  //Get starting time;
  DGCgettime(startTime);

  //Initialize EVERYTHING to be zeroed for good measure.
  _vehicleState.Timestamp = startTime;
  _vehicleState.Northing = 0;
  _vehicleState.Easting = 0;
  _vehicleState.Altitude = 0;
  _vehicleState.Vel_N = 0;
  _vehicleState.Vel_E = 0;
  _vehicleState.Vel_D = 0;
  _vehicleState.Acc_N = 0;
  _vehicleState.Acc_E = 0;
  _vehicleState.Acc_D = 0;
  _vehicleState.Roll = 0;
  _vehicleState.Pitch = 0;
  _vehicleState.Yaw = 0;
  _vehicleState.RollAcc = 0;
  _vehicleState.PitchAcc = 0;
  _vehicleState.YawAcc = 0;
  _vehicleState.GPS_Northing = 0;
  _vehicleState.GPS_Easting = 0;
  _vehicleState.NorthConf = 0;
  _vehicleState.EastConf = 0;
  _vehicleState.HeightConf = 0;
  _vehicleState.RollConf = 0;
  _vehicleState.PitchConf = 0;
  _vehicleState.YawConf = 0;
    _vehicleState.localX = 0;
  _vehicleState.localY = 0;
  _vehicleState.localZ = 0;
  _vehicleState.localRoll = 0;
  _vehicleState.localPitch = 0;
  _vehicleState.localYaw = 0;
  _vehicleState.vehicleVelX = 0;
  _vehicleState.vehicleVelY = 0;
  _vehicleState.vehicleVelZ = 0;
  _vehicleState.vehicleVelRoll = 0;
  _vehicleState.vehicleVelPitch = 0;
  _vehicleState.vehicleVelYaw = 0;

  haveInitPose = false;
  

  //Initialize metastate struct 

  //Create mutexes:
  DGCcreateMutex(&m_VehicleStateMutex);
  DGCcreateMutex(&m_ApxDataMutex);
  
  DGCcreateCondition(&newData);
  DGCcreateCondition(&apxBufferFree);

  DGCSetConditionTrue(apxBufferFree);
  
  //Set up socket for skynet broadcasting:
  broadcastStateSock = m_skynet.get_send_sock(SNstate);
  if (broadcastStateSock < 0)
  {
    cerr << "AState::AState(): skynet get_send_sock returned error" << endl;
  }


  apxBufferLastInd = 0;
  apxBufferReadInd = 0;
  if(apxActive == 1)
  {
    DGCstartMemberFunctionThread(this, &AState::apxReadThread); //this will be the Thread to read from apx
  } 
  DGCstartMemberFunctionThread(this, &AState::updateStateThread); //this will be the Thread to read from apx

  //Start sparrow threads
  if (_options.useSparrow)
  {
    DGCstartMemberFunctionThread(this, &AState::sparrowThread);
  }
  
} // end AState::AState() constructor

/*! Astate destructor
 */
 
AState::~AState()
{
  if(_options.logRaw)
  {
    apxLogStream.close();
    stateLogStream.close();
  }
  if(_options.stateReplay);
  {
    replayStateStream.close();
  }
  if(apxActive)
  {
    close(apxDataSocket);
  }
} // end AState::~AState() destructor


/*! Function that broadcasts VehicleState.
 */
void AState::broadcast()
{
  pthread_mutex_t *pMutex = &m_VehicleStateMutex;
  int c=m_skynet.send_msg(broadcastStateSock,
                        &_vehicleState,
                        sizeof(VehicleState),
                        0, &pMutex);
  if (c!= sizeof(VehicleState))
  {
    cerr << "AState::Broadcast(): didn't send right size state message" << endl;
    sleep(1);
    exit(1);
  }
 
} // end AState::broadcast()



/*! Astate main loop
 */

void AState::active()
{
  while (!quitPressed)
  {
    sleep(1);
  }
}
