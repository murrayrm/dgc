#include <iostream>
#include <string>
#include "RNDF.hh"
#include "Graph.hh"
#include "LatLon2UTM.hh"
#include <vector>
#include <math.h>
using namespace std;

void addVertices(RNDF*, Graph*);
void addEdges(RNDF*, Graph*);
double weight(Waypoint*, Waypoint*);
double distance(Waypoint*, Waypoint*);
double distance(double, double, double, double);
double avgTravelTime(Waypoint*, Waypoint*);
double avgSpeed(int);
double avgSpeed(Segment*);
bool adjacentSameDirection(Lane*, Lane*);
vector<int> getAdjacentLanes(Lane*);
double square(double);
double min(double, double);

RNDF* rndf;

int main(int argc, char* argv[])
{ 
  rndf = new RNDF();
  
  if(!rndf->loadFile(argv[1]))
  {
    cout << "Error: Unable to load RNDF file, exiting program." << endl;
    exit(0);
  }
  
  if(!rndf->loadFile(argv[2]))
  {
    cout << "Error: Unable to load MDF file, exiting program." << endl;
    exit(0);
  }
  
  Graph* rndfGraph = new Graph();
  
  addVertices(rndf, rndfGraph);
  
  addEdges(rndf, rndfGraph);

  delete rndfGraph;  
  delete rndf;
  
  return 0;
}

void addVertices(RNDF* rndf, Graph* rndfGraph)
{
  int numOfSegments = rndf->getNumOfSegments();
  int numOfZones = rndf->getNumOfZones();
  
/* Adding all entry, exit, and checkpoint waypoints to the graph. */

  for(int i = 1; i <= numOfSegments; i++)
  {    
    for(int j = 1; j <= rndf->getSegment(i)->getNumOfLanes(); j++)
    {
      for(int k = 1; k <= rndf->getLane(i, j)->getNumOfWaypoints(); k++)
      {
        Waypoint* waypoint = rndf->getWaypoint(i, j, k);
        
        if(waypoint->isEntry() || waypoint->isExit() || waypoint->isCheckpoint())
          rndfGraph->addVertex(i, j, k);
      }
    }    
  }
  
/* Adding all entry and exit perimeter points and parking spot waypoints. */
  
  for(int i = numOfSegments + 1; i <= numOfSegments + numOfZones; i++)
  {
    
/* Adding all entry and exit perimeter points. */
    
    for(int j = 1; j <= rndf->getZone(i)->getNumOfPerimeterPoints(); j++)
    {
      PerimeterPoint* perimeterPoint = rndf->getPerimeterPoint(i, j);
      
      if(perimeterPoint->isEntry() || perimeterPoint->isExit())
        rndfGraph->addVertex(i, 0, j);
    }
    
/* Adding all parking spot checkpoints. */
    
    for(int j = 1; j <= rndf->getZone(i)->getNumOfSpots(); j++)
    {
        Waypoint* waypoint = rndf->getWaypoint(i, j, 1);
          
        if(waypoint->isCheckpoint())
          rndfGraph->addVertex(i, j, 1);
          
        waypoint = rndf->getWaypoint(i, j, 2);
          
        if(waypoint->isCheckpoint())
          rndfGraph->addVertex(i, j, 2);
    }
  }
  
  cout << "Vertices added to graph: " << endl;
  rndfGraph->print();
}

void addEdges(RNDF* rndf, Graph* rndfGraph)
{
  vector<Vertex*> vertices = rndfGraph->getVertices();
  
/* Adding all edges from exit waypoints to their entry waypoints. */
  
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    GPSPoint* exit;
    vector<GPSPoint*> entries;
    
    int exitSegmentID = vertex->getSegmentID();
    int exitLaneID = vertex->getLaneID();
    int exitWaypointID = vertex->getWaypointID();
    
    if(exitLaneID == 0)
      exit = rndf->getPerimeterPoint(exitSegmentID, exitWaypointID);
    else
      exit = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID);
        
    if(!exit->isExit())
      continue;
    else
      entries = exit->getEntryPoints();
           
    for(unsigned j = 0; j < entries.size(); j++)
    {
      GPSPoint* entry = entries[j];
      
      rndfGraph->addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
        entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID());
    }
  }
  
/* Adding all edges from entry waypoints. */

  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    GPSPoint* entry;
    
    int entrySegmentID = vertex->getSegmentID();
    int entryLaneID = vertex->getLaneID();
    int entryWaypointID = vertex->getWaypointID();
    
/* Adding all edges from entry perimeter points of zones. */

    if(entryLaneID == 0)
    {
      entry = rndf->getPerimeterPoint(entrySegmentID, entryWaypointID);
      
      if(!entry->isEntry())
        continue;
        
/* Adding edges from all entry perimeter points to all exit perimeter points. */
        
      for(int j = entryWaypointID + 1; j <= rndf->getZone(entrySegmentID)->getNumOfPerimeterPoints(); j++)
      {
        if(rndf->getPerimeterPoint(entrySegmentID, j)->isExit())
          rndfGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
            entrySegmentID, entryLaneID, j);
      }
            
/* Adding edges from all perimeter points to all parking spot checkpoints. */
            
      for(int j = 1; j <= rndf->getZone(entrySegmentID)->getNumOfSpots(); j++)
      {
        if(rndf->getWaypoint(entrySegmentID, j, 2)->isCheckpoint())
          rndfGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
            entrySegmentID, j, 2);
      }
    }
    else
    {
      entry = rndf->getWaypoint(entrySegmentID, entryLaneID, entryWaypointID);
            
      if(!entry->isEntry())
        continue;
      
      vector<int> adjacentLanes = getAdjacentLanes(rndf->getLane(entrySegmentID, entryLaneID));

/* Adding edges from all entry waypoints to exit waypoints in the same lane. */

      for(int j = entryWaypointID + 1; j <= rndf->getLane(entrySegmentID, entryLaneID)->getNumOfWaypoints(); j++)
      {
        if(rndf->getWaypoint(entrySegmentID, entryLaneID, j)->isExit())
          rndfGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
            entrySegmentID, entryLaneID, j);
      }

/* Adding edges from all entry waypoints to exit waypoints in adjacent, same direction lanes. */

      for(unsigned j = 0; j < adjacentLanes.size(); j++)
      {
        Lane* adjacentLane = rndf->getLane(entrySegmentID, adjacentLanes[j]);
        int numOfWaypoints = adjacentLane->getNumOfWaypoints();
        
        for(int k = 2; k <= numOfWaypoints; k++)
        {
          if(adjacentLane->getWaypoint(k)->isExit())
            rndfGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
              entrySegmentID, adjacentLane->getLaneID(), k);  
        }   
      }
    }
  }
}

double weight(Waypoint* waypoint1, Waypoint* waypoint2)
{
  return avgTravelTime(waypoint1, waypoint2);
}

double distance(Waypoint* waypoint1, Waypoint* waypoint2)
{
  return distance(waypoint1->getLatitude(), waypoint1->getLongitude(),
    waypoint2->getLatitude(), waypoint2->getLongitude());
}

double distance(double lat1, double lon1, double lat2, double lon2)
{
  double UTMNorthing1, UTMEasting1, UTMNorthing2, UTMEasting2;
  char UTMZone[4];
  int RefEllipsoid = 23;

  LLtoUTM(RefEllipsoid, lat1, lon1, UTMNorthing1, UTMEasting1, UTMZone);
  LLtoUTM(RefEllipsoid, lat2, lon2, UTMNorthing2, UTMEasting2, UTMZone);

  return  sqrt(square(UTMEasting2 - UTMEasting1) + square(UTMNorthing2 - UTMNorthing1));
}

double avgTravelTime(Waypoint* waypoint1, Waypoint* waypoint2)
{
  return distance(waypoint1, waypoint2) / avgSpeed(waypoint1->getSegmentID());
}

double avgSpeed(int segmentID)
{
  return avgSpeed(rndf->getSegment(segmentID));
}

double avgSpeed(Segment* segment)
{
  return (segment->getMinSpeed() + segment->getMaxSpeed()) / 2;
}

bool adjacentSameDirection(Lane* lane1, Lane* lane2)
{
  return (distance(lane1->getWaypoint(1), lane2->getWaypoint(1)) <= 6.096);
}

vector<int> getAdjacentLanes(Lane* lane)
{
  vector<int> adjacentLanes;
  int segmentID = lane->getSegmentID();
  int numOfLanes = rndf->getSegment(segmentID)->getNumOfLanes();
  
  for(int i = 1; i <= numOfLanes; i++)
    if(i != lane->getLaneID() && adjacentSameDirection(lane, rndf->getLane(segmentID, i)))
      adjacentLanes.push_back(i);
      
  return adjacentLanes;
}
  

double square(double x)
{
  return x * x;
}

double min(double x, double y)
{
  if(x < y)
    return x;
  else
    return y;
}
