#ifndef FUSIONMAPPER_HH
#define FUSIONMAPPER_HH

#include <unistd.h>
#include <pthread.h>
#include "MapdeltaTalker.h"
#include "DGCutils"
#include "CMapPlus.hh"
#include "rddf.hh"
#include "herman.hh"
#include "CCorridorPainter.hh"
#include "MapConstants.h"
#include "RoadPainter.hh"
#include "Road.hh"
#include "AliceConstants.h"
#include "TrajTalker.h"
#include "CTerrainCostPainter.hh"
#include "StateClient.h"
#include "interface_superCon_fmap.hh"
#include "SuperConClient.hh"
#include "../movingObstacle.hh"
#include "CDynCostFuser.hh"

#define MAX_DELTA_SIZE 999999
#define MAX_ROAD_SIZE 50
#define NUM_LADAR_LAYERS 5
#define NUM_STEREO_LAYERS 2
#define NUM_ELEV_LAYERS (NUM_LADAR_LAYERS + NUM_STEREO_LAYERS)

#define DEFAULT_WING_WIDTH 2.0

using namespace std;

//Sparrow Display Functions
int user_quit(long arg);
int user_pause(long arg);
int user_reset(long arg);
int user_change(long arg);
int user_repaint(long arg);
int user_reevaluate(long arg);
int user_save(long arg);
int user_clear(long arg);

struct FusionMapperOptions {
  int optRoad, optSupercon; //Tells the fusion mapper if we should use road and superCon
  int optSNKey;             //will contain the skynet key we are using
  char configFilename[256]; //Contains the name of the config file
  int sendRDDF;
  double processingRate;
  double deltaRate;
  double optWingWidth;
  int optKF;                //Tells the fusion mapper if we should use the kalman filer terrain fusion
  int optDA;                //if =1, the fusion mapper will use data association for correcting MO info
};


class FusionMapperStatusInfo {
private:
  unsigned long long startProcessTime;
  unsigned long long endProcessTime;
  unsigned long long endLockTime;
  unsigned long long endStage1Time;
  unsigned long long endStage2Time;
  
public:
  int numMsgs;
  int msgSize;
  double msgSizeAvg;
  double processTime;
  double processTimeAvg;
  double lockTime;
  double lockTimeAvg;
  double stage1Time;
  double stage1TimeAvg;
  double stage2Time;
  double stage2TimeAvg;
  double remainingTime;
  double remainingTimeAvg;

  FusionMapperStatusInfo() {reset();};
  ~FusionMapperStatusInfo() {};

  void startProcessTimer() {
    DGCgettime(startProcessTime);
  };
	
  void endLockTimer() {
    DGCgettime(endLockTime);
  };
  
  void endStage1Timer() {
    DGCgettime(endStage1Time);
  };
  
  void endStage2Timer() {
    DGCgettime(endStage2Time);
  };
  
  void endProcessTimer(int newMsgSize) {
    DGCgettime(endProcessTime);
    msgSize = newMsgSize;
    msgSizeAvg = (msgSize + numMsgs*msgSizeAvg)/(numMsgs+1);
    processTime = ((double)(endProcessTime-startProcessTime))/(1000.0);
    processTimeAvg = (processTime + processTimeAvg*numMsgs)/(numMsgs+1);
    lockTime = ((double)(endLockTime-startProcessTime))/(1000.0);
    lockTimeAvg = (lockTime + lockTimeAvg*numMsgs)/(numMsgs+1);
    stage1Time = ((double)(endStage1Time - endLockTime))/(1000.0);
    stage1TimeAvg = (stage1Time + stage1TimeAvg*numMsgs)/(numMsgs+1);
    stage2Time = ((double)(endStage2Time - endStage1Time))/(1000.0);
    stage2TimeAvg = (stage2Time + stage2TimeAvg*numMsgs)/(numMsgs+1);
    remainingTime = ((double)(endProcessTime - endStage2Time))/(1000.0);
    remainingTimeAvg = (remainingTime + remainingTimeAvg*numMsgs)/(numMsgs+1);
    numMsgs++;
  };

  void reset() {
    numMsgs=0;
    msgSize=0;
    msgSizeAvg=0.0;
    processTime=0.0;
    processTimeAvg=0.0;
    lockTime = 0.0;
    lockTimeAvg = 0.0;
    stage1Time = 0.0;
    stage1TimeAvg = 0.0;
    stage2Time = 0.0;
    stage2TimeAvg = 0.0;
    remainingTime = 0.0;
    remainingTimeAvg = 0.0;
    
    startProcessTime = 0;
    endProcessTime = 0;
    endLockTime = 0;
    endStage1Time = 0;
    endStage2Time = 0;
  };
};

/**
This struct is used for saving everything which has to do with the cost maps for the different ladars.
*/
struct FusionMapperElevationLayer {
  int elevLayerNum;
  int costLayerNum;
  int terrainPainterIndex;
  int costPainterIndex;
  int corridorLayerNum;
  CCorridorPainter* corridorPainter; 
  bool sendCost;
  sn_msg msgTypeCost;
  CMapPlus* map;
  CTerrainCostPainter* terrainPainter;
  sn_msg msgTypeElev;
  char name[32];
  char optionsFilename[128];
  FusionMapperStatusInfo statusInfo;
  bool enabled;
  CTerrainCostPainterOptions options;
  double relWeight;
  pthread_mutex_t shiftMutex;
  
};

class FusionMapper : public CStateClient, public CMapdeltaTalker,                    
		     public CTrajTalker, public CSuperConClient {
public:
  FusionMapper(int skynetKey, FusionMapperOptions mapperOptsArg, int waitForState);
  ~FusionMapper();
  
  /**
Updates the vehicle location in all maps, paints the corridor, and sends the fused speed limits across skynet. 
  */
  void ActiveLoop();
  
  /**
Receives data from the feeders. Data belonging to moving obstacles is ignored and all other data get fused.
   */
  void ReceiveDataThread_ElevFeeder(void* pArg);
  /**
     Listens for road data.
   */
  void ReceiveDataThread_Road();
  /**
     Listens for Supercon data.
  */
  void ReceiveDataThread_Supercon();
  /**
     SuperCon can tell the fusionMapper to broaden the RDDF or clear all maps, 
     this thread listens for such messages does what it's told. 
  */
  void ReceiveDataThread_SuperconActions();
  
  /**
     Listens for EStop messages, which can pause the fusionmapper.
  */

  void ReceiveDataThread_EStop();
  /**
     Listens for moving obstacle messages and saves them to obstacleList. It also calls the dynamic fuser 
     which updates the layer containing the moving obstacles. 
   */
  void ReceiveDataThread_MovingObstacles();

  void UpdateSparrowVariablesLoop();
  /**
     The plannerModule can request the entire fused speed map by sending a message over skynet, 
     this thread listens for such messages and sends the map when it's requested.
  */
  void PlannerListenerThread();
  
  void SparrowDisplayLoop();

  void ReadConfigFile();

  void WriteConfigFile();

  //This array contains all the cost maps for the different sensors
  FusionMapperElevationLayer layerArray[NUM_ELEV_LAYERS];

private:

  FusionMapperOptions mapperOpts;

  //Info Structs for each module we receive from
  FusionMapperStatusInfo _infoStaticPainter;  
  FusionMapperStatusInfo _infoStereoFeeder;
  FusionMapperStatusInfo _infoRoad;
  FusionMapperStatusInfo _infoSupercon;
  //And one for output info
  FusionMapperStatusInfo _infoFusionMapper;

  
  char* m_pStereoMapDelta;
  char* m_pSuperconMapDelta;
  double* m_pRoadInfo;
  

  //double* m_pRoadInfo;

  double* m_pRoadReceive;

  double* m_pRoadX;
  double* m_pRoadY;
  double* m_pRoadW;
  int m_pRoadPoints;

  pthread_rwlock_t allMapsRWLock;
  pthread_mutex_t roadMutex;
  pthread_mutex_t kfMutex;
  pthread_mutex_t movingMutex;

  movingObstacle *obstacleList; //The list where the moving obstacles get saved.

  CMapPlus fusionMap;
  CMapPlus roadMap;
  CMapPlus swapMapA, swapMapB;

  RDDF fusionRDDF;
  RDDF widerRDDF;
  herman* fusionHerman;

  CCorridorPainter* fusionCorridorPainter;
  CCorridorPainterOptions _corridorOptsFinalCost;
  CCorridorPainterOptions _corridorOptsReference;
  CCorridorPainterOptions _corridorOptsWings;
  CCorridorPainter *lowResCorridorPainter;
  
  CTerrainCostPainter* defaultTerrainPainter;
  CTerrainCostPainter* lowResTerrainPainter;
  
  //this is the object doing the actual fusion:
  CDynCostFuser* costFuser;
  //this struct contains some options for the costFuser
  CDynCostFuserOptions costFuserOpts;

  //All the layer IDs:  
  int layerID_road;
  int layerID_roadCounter;
  int layerID_costFinal;
  int layerID_corridor;
  int layerID_corridor_lowres;
  int layerID_superCon;
  int layerID_fusedKF;
  int layerID_fusedElev;
  int layerID_fusedVar;
  int layerID_movingObst;

  int _status;

  int _QUIT;
  int _PAUSE;
  int _PAINTWHILEPAUSED;
};

#endif
