#include "SpoofCom.hh"
using namespace std;

int statflag = 0;
SpoofCom::SpoofCom(int sn_key):CSkynetContainer(MODtrackMO, sn_key, &statflag){}

void SpoofCom::run(){
  int stat = 10;
  _QUIT = 0;
  int m_send_socket = m_skynet.get_send_sock(SNmovingObstacle);

  double dist=0;
  int k = 0;
  while(!_QUIT){
    
    movingObstacle obstacle;
    obstacle.ID = 0;
    obstacle.status = 2;

    for(int i = 0; i<10; i++){
      obstacle.cornerPos[i].N = 3780305 + dist - 0.3*(9-i);
      obstacle.cornerPos[i].E = 392392 + dist/3 - 0.1*(9-i);
      obstacle.side1.N = 0;
      obstacle.side1.E = -4;
      obstacle.side2.N = -3;
      obstacle.side2.E = 0;
    }
      DGCgettime(obstacle.timeStamp);
      int msgSize = sizeof(obstacle);
      //cout<< "Size of MovingObstacle struct: " << msgSize << endl;
      cout << "Sending new object located at: (" << obstacle.cornerPos[9].N << "," << obstacle.cornerPos[9].E << ")" <<   endl;
      cout << "Obstacle status:" << obstacle.status << endl;
      m_skynet.send_msg(m_send_socket, &obstacle, msgSize, 0);
     dist=dist+0.3;
      k++;
      clock_t goal = 600000 + clock();
      while(goal> clock()){}
      }
}

int main() {

  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
    {
      cerr << "SKYNET_KEY environment variable isn't set" << endl;
    }
  else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  cout << sn_key;

  SpoofCom spoof(sn_key);
  spoof.run();

  return 0;
}



