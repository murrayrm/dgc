#ifndef CBUFFER_HH
#define CBUFFER_HH

//Implements a circular buffer

template<class T, int nsize>
class cbuffer
{
public:
  cbuffer() : head(0), tail(-1) {}
  cbuffer(const cbuffer &cb) : head(cb.head), tail(cb.tail) {
    for(int i=0; i<size(); ++i)
      (*this)[i] = cb.tail[i];
  }
  ~cbuffer() {}

  const cbuffer &operator=(const cbuffer &cb) {
    if(this == &cb)
      return *this;
    head = cb.head;
    tail = cb.tail;
    for(int i=0; i<size(); ++i)
      (*this)[i] = cb.tail[i];
    return *this;
  } 

  //access operators count from the front (latest added item)
  T &operator[](int i) { return buf[(head+1+i)%nsize]; }
  const T&operator[](int i) const { return buf[(head+1+i)%nsize]; }
  
  T &last() { return buf[tail]; }
  const T&last() const { return buf[tail]; }

  T &first() { return (*this)[0]; }
  const T&first() const { return (*this)[0]; }
  
  int size() const {
    if(tail==-1)
      return 0;
    if(tail<head)
      return head-tail;
    return (nsize-tail) + head;
  }
  
  bool empty() const {
    return tail==-1;
  }

  bool full() const {
    return head==tail;
  }

  void add(const T& t) {
    if(head==tail) {  //Buffer is full, remove last item
      buf[head] = t;
      ++head;
      if(head>=nsize)
	head=0;
      tail=head;
    } else {             //Buffer isnt full, just append
      buf[head] = t;
      if(tail==-1)      //Buffer was previosly empty
	tail=head;
      ++head;
      if(head>=nsize)
	head=0;
    }
  }

private:
  T buf[nsize];
  int head, tail;
};



#endif
