#ifndef SUPERCONINTERFACES_HH
#define SUPERCONINTERFACES_HH

#include<stdlib.h>   //Needed for MAX_PATH
#include<string>    //Needed for serialization/deserialization of strings

using std::string;

/*! sc_criticality enum. Lists the different levels of errors allowed in a supercon throw
 * \brief The levels of criticality in a SuperCon throw
 */
enum sc_msg_type
  {
    /*! Just sends a message to supercon */
    scMessage=0,
    /*! Most critical error. The module can not continue on its own and requires a restart */
    scAssert=1,
    /*! Some serious error has accured that requires SuperCon to take action, however, the error
     * might be recoverable. The pre-conditions and post-conditions for this call are defined for
     * each module and throwID on the SuperCon WIKI page */
    scError=2,
    /*! Some minor thing has happened from which the module can recover gracefully, however, it
     * might be suitable for SuperCon to have information about it (such as trajFollower not getting
     * new trajectories at a high enough frequency) */
    scWarning=3
  };

/*! Maximal number of bytes in user supplied arguments */
#define SUPERCON_MAX_USER_BYTES 512
#define SUPERCON_PATH_MAX 32
#define SUPERCONCLIENT_TIMEOUT 1    //Number of seconds for a client to wait for reply from supercon before terminating
/*! supercon_msg_cmd. The struct send from SuperConClient to SuperCon in the case of a scMessage
*/
struct supercon_msg_cmd
{
  /*! 3 letter aberation of the module name */
  char module_id[3];
  /*! process_id of the module sending the throw */
  unsigned int process_id;
  /*! Criticality of the throw */
  sc_msg_type type;
  /*! msg_id uniquely identifying the message within the module */
  int msg_id;
  
  //The following members are for debug purpose
  /*! Subversion revision of current code */
  int revision;
  /*! Filename of file throwing */
  char file[SUPERCON_PATH_MAX];
  /*! Line # of the throw */
  int line;

  //User supplied data, passed by extending the struct
  /*! Number of bytes of user supplied data to be sent */
  int user_bytes;
  /*! The user supplied data, however, only the first user_bytes will be sent */

  unsigned char user_data[SUPERCON_MAX_USER_BYTES];
};

#define SUPERCON_MSG_CMD_CORE (sizeof(supercon_msg_cmd) - SUPERCON_MAX_USER_BYTES)

/*! supercon_resume_cmd. The struct send from SuperCon to SuperConClient after a supercon_throw_cmd
 * with criticality error was received. This cmd tells SuperConClient if SuperCon was able to repair
 * the errors and it is ok for SuperConClient resume execution, or if the module should assert. */
struct supercon_resume_cmd
{
  /*! process_id of the module the message is intended for */
  unsigned int process_id;
  /*! Should execution be resumed or not, contains either 'y' or 'n' */
  unsigned char resume;
};

////////////////////////////////////////////////////////////////////////////////////
//Serialization functions for supercon_throw_cmd
/*! Serialize n bytes from pBuf to the throw command */
void sc_serialize_raw(supercon_msg_cmd *pCmd, void *pBuf, int n);

//Standard template function handles most types
template<typename T>
void sc_serialize(supercon_msg_cmd *pCmd, T t)
{
  sc_serialize_raw(pCmd, (void*)&t, sizeof(T));
}

//Partial specialization to capture incorrect pointer use
//template<class T>
//void sc_serialize(supercon_msg_cmd *pCmd, T *t)
//{
  //Can't serialize pointers
  //  #warning Unable to serialize pointer for SuperConClient
//}

//Specializations for some common types
template<>
void sc_serialize<const char *>(supercon_msg_cmd *pCmd, const char *t);
template<>
void sc_serialize<char *>(supercon_msg_cmd *pCmd, char *t);

template<>
void sc_serialize<const unsigned char *>(supercon_msg_cmd *pCmd, const unsigned char *t);
template<>
void sc_serialize<unsigned char *>(supercon_msg_cmd *pCmd, unsigned char *t);

template<>
void sc_serialize<const string>(supercon_msg_cmd *pCmd, const string t);
template<>
void sc_serialize<string>(supercon_msg_cmd *pCmd, string t);

////////////////////////////////////////////////////////////////////////////////////
//Deserialization functions for supercon_throw_cmd
/*! Deserialize n bytes from the throw command at position npos to pBuf */

void sc_deserialize_raw(supercon_msg_cmd *pCmd, int &npos, void *pBuf, int n);

//Standard template function handles most types
template<class T>
void sc_deserialize(supercon_msg_cmd *pCmd, int &npos, T &t)
{
  sc_deserialize_raw(pCmd, npos, (void*)&t, sizeof(T));
}

//Partial specialization to capture incorrect pointer use
//template<class T>
//void sc_deserialize(supercon_msg_cmd *pCmd, int &npos, T *t)
//{
//  //Can't serialize pointers
//  //  #warning Unable to deserialize pointer for SuperCon
//}

//Specializations for some common types
template<>
void sc_deserialize<string>(supercon_msg_cmd *pCmd, int &npos, string &t);

#endif //SUPERCONINTERFACES_HH
