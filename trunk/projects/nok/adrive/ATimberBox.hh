#ifndef __ATIMBERBOX_HH__
#define __ATIMBERBOX_HH__

#include "SkynetContainer.h"
#include "CTimber.hh"

class AtimberBox : virtual public CSkynetContainer {
public:
  AtimberBox(int skynetKey, int * guistatus);
  ~AtimberBox();


  void timberStart();
  void timberStop();

private:
  int timberMsgSocket;
};

class AdriveEventLogger {

public:
  AdriveEventLogger(char * pathname);
  ~AdriveEventLogger();

  int operator<<(string input_string);
  int enable(void);
  int disable(void);

private:
  unsigned long long current_time;
  fstream logfile;
  int event_enabled;  

};

extern AdriveEventLogger event;
extern AdriveEventLogger estop_log;
#endif //__ATIMBERBOX_HH__
