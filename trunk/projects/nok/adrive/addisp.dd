/*
 * addisp.dd - adrive display table
 *
 * RMM, 31 Dec 04
 *
 */

%%
ADRIVE    %EXIT    GUI_STATUS: %guist
IF YOU HAVE NOT READ THE README IN THE LAST WEEK READ IT!  SHUTDOWN=%shtdwn 
---------------------------------------+---------------------------------------
STEER -1(L) to 1(R)           %st_ct   | BRAKE  0 to 1                %br_ct
 st delay: %st_spd cm_delay:  %st_cm   | st delay: %br_spd cm delay:  %br_cm
 status_e: %st_se  command_e: %st_ce   | status_e: %br_se  command_e: %br_ce
 status:   %st_st  command:   %st_cmd  | status:   %br_st  command:   %br_cmd
 position: %st_pos accel:     %st_ac   | position: %br_pos pressure:  %br_pre
 error:    %st_err velocity:  %st_vel  | error:    %br_err
---------------------------------------+---------------------------------------
GAS	0 to 1	              %th_ct   | TRANS  0=park,-1=rev,1=drive %tr_ct
 st delay: %th_spd cm delay:  %th_cm   | st delay: %tr_spd cm delay:  %tr_cm
 status_e: %th_se  command_e: %th_ce   | status_e: %tr_se  command_e: %tr_ce
 status:   %th_st  command:   %th_cmd  | status:   %tr_st   error:    %tr_err 
 position: %th_pos		       | t_pos:    %tr_pos   t_cmd:   %tr_cmd
                                       | i_pos:    %ig_pos   i_cmd:   %ig_cmd
                                       | l1_pso:   %l1_pos   l1_cmd:  %l1_cmd
                                       | l2_pso:   %l2_pos   l2_cmd:  %l2_cmd
 error:    %th_err                     | l3_pso:   %l3_pos   l3_cmd:  %l3_cmd
                                       | l4_pso:   %l4_pos   l4_cmd:  %l4_cmd
---------------------------------------+---------------------------------------
ESTOP 0=D,1=P,2=R             %es_ct   |SKYNET INTERFACE
 st delay: %es_spd about:     %es_ab   |
 status_e: %es_se  command_e: %es_ce   |Skynet Key:%snkey  
 status:   %es_st  superCon:  %es_cmd  |Gas cmds:  %ga_cct trans cmd: %tr_cct
 position: %es_pos darpa:     %d_stop  |Steer cmds:%st_cct
 error:    %es_err astop:     %a_stop  |Brake cmds:%br_cct
		   error:     %a_stop_err
---------------------------------------+---------------------------------------
OBDII                                                                  %ob_ct
Status_e: %ob_se
Status: %ob_st   st delay: %ob_spd  cm delay: %ob_cm
Engine : Speed: %ob_speed  Wheel Force: %ob_tor
SLOW UPDATES:: RPM: %ob_rpm  Temp: %ob_temp 
-------------------------------------------------------------------------------
SUPERVISORY  delay: %su_dly  interlocks on: %locks                     %su_ct
===============================================================================
Press to send comand|g:gas, s:steer, b:brake, c:acceleration, v:velocity, t:trans.
Press e to reset steering after a disable and re-enable.  

Error codes are: 0 is good,  >= 1 is bad
%%

tblname: addisp;
bufname: addisp_buf;

# Steering device 
int: %st_se my_vehicle.actuator[ACTUATOR_STEER].status_enabled "%6d" -ro;
int: %st_st my_vehicle.actuator[ACTUATOR_STEER].status "%6d" -ro;
int: %st_ce my_vehicle.actuator[ACTUATOR_STEER].command_enabled "%6d" -ro;
double: %st_cmd my_vehicle.actuator[ACTUATOR_STEER].command "%6g";
double: %st_pos my_vehicle.actuator[ACTUATOR_STEER].position "%6g" -ro;
double: %st_vel my_vehicle.actuator[ACTUATOR_STEER].velocity "%6g";
double: %st_ac my_vehicle.actuator[ACTUATOR_STEER].acceleration "%6g";
int: %st_err my_vehicle.actuator[ACTUATOR_STEER].error "%6d" -ro;
int:  %st_spd my_vehicle.actuator[ACTUATOR_STEER].status_sleep_length "%6d";
int:  %st_cm my_vehicle.actuator[ACTUATOR_STEER].command_sleep_length "%6d";
int:  %st_ct my_vehicle.actuator[ACTUATOR_STEER].status_loop_counter "%6d" -ro;

# Brake device 
int: %br_se my_vehicle.actuator[ACTUATOR_BRAKE].status_enabled "%6d" -ro;
int: %br_st my_vehicle.actuator[ACTUATOR_BRAKE].status "%6d" -ro;
int: %br_ce my_vehicle.actuator[ACTUATOR_BRAKE].command_enabled "%6d" -ro;
double: %br_cmd my_vehicle.actuator[ACTUATOR_BRAKE].command "%6g";
double: %br_pos my_vehicle.actuator[ACTUATOR_BRAKE].position "%6g" -ro;
double: %br_pre my_vehicle.actuator[ACTUATOR_BRAKE].pressure "%6g" -ro;
int: %br_err my_vehicle.actuator[ACTUATOR_BRAKE].error "%6d" -ro;
int:  %br_spd my_vehicle.actuator[ACTUATOR_BRAKE].status_sleep_length "%6d";
int:  %br_cm my_vehicle.actuator[ACTUATOR_BRAKE].command_sleep_length "%6d";
int:  %br_ct my_vehicle.actuator[ACTUATOR_BRAKE].status_loop_counter "%6d" -ro;

# Throttle device 
int: %th_se my_vehicle.actuator[ACTUATOR_GAS].status_enabled "%6d" -ro;
int: %th_st my_vehicle.actuator[ACTUATOR_GAS].status "%6d" -ro;
int: %th_ce my_vehicle.actuator[ACTUATOR_GAS].command_enabled "%6d" -ro;
double: %th_cmd my_vehicle.actuator[ACTUATOR_GAS].command "%6g";
double: %th_pos my_vehicle.actuator[ACTUATOR_GAS].position "%6g" -ro;
int: %th_err my_vehicle.actuator[ACTUATOR_GAS].error "%6d" -ro;
int:  %th_spd my_vehicle.actuator[ACTUATOR_GAS].status_sleep_length "%6d";
int:  %th_cm my_vehicle.actuator[ACTUATOR_GAS].command_sleep_length "%6d";
int:  %th_ct my_vehicle.actuator[ACTUATOR_GAS].status_loop_counter "%6d" -ro;

# Transmission device 
int: %tr_se my_vehicle.actuator_trans.status_enabled "%6d" -ro;
int: %tr_st my_vehicle.actuator_trans.status "%6d" -ro;
int: %tr_ce my_vehicle.actuator_trans.command_enabled "%6d" -ro;
int:  %tr_cmd my_vehicle.actuator_trans.command "%6d";
int:  %tr_pos my_vehicle.actuator_trans.position "%6d" -ro;
int:  %tr_err my_vehicle.actuator_trans.error "%6d" -ro;
int:  %tr_spd my_vehicle.actuator_trans.status_sleep_length "%6d";
int:  %tr_cm my_vehicle.actuator_trans.command_sleep_length "%6d";
int:  %tr_ct my_vehicle.actuator_trans.status_loop_counter "%6d" -ro;
int:  %tr_cct my_vehicle.actuator_trans.command_loop_counter "%6d" -ro;
int:  %ig_pos my_vehicle.actuator_trans.ignition_position "%6d" -ro;
int:  %ig_cmd my_vehicle.actuator_trans.ignition_command "%6d";
int:  %l1_cmd my_vehicle.actuator_trans.led1_command "%6d";
int:  %l2_cmd my_vehicle.actuator_trans.led2_command "%6d";
int:  %l3_cmd my_vehicle.actuator_trans.led3_command "%6d";
int:  %l4_cmd my_vehicle.actuator_trans.led4_command "%6d";
int:  %l1_pos my_vehicle.actuator_trans.led1_position "%6d" -ro;
int:  %l2_pos my_vehicle.actuator_trans.led2_position "%6d" -ro;
int:  %l3_pos my_vehicle.actuator_trans.led3_position "%6d" -ro;
int:  %l4_pos my_vehicle.actuator_trans.led4_position "%6d" -ro;



# EStop device 
int: %es_se my_vehicle.actuator_estop.status_enabled "%6d" -ro;
int: %es_st my_vehicle.actuator_estop.status "%6d" -ro;
int: %es_ce my_vehicle.actuator_estop.command_enabled "%6d" -ro;
int: %es_cmd my_vehicle.actuator_estop.command "%6d";
int: %es_pos my_vehicle.actuator_estop.position "%6d" -ro;
int: %es_err my_vehicle.actuator_estop.error "%6d" -ro;
int: %a_stop  my_vehicle.actuator_estop.astop "%6d";
int: %a_stop_err my_vehicle.actuator_estop.astop_err "%4x";
int: %d_stop  my_vehicle.actuator_estop.dstop "%6d";
int:  %es_spd my_vehicle.actuator_estop.status_sleep_length "%6d";
int: %es_ct my_vehicle.actuator_estop.status_loop_counter "%6d" -ro;
int:  %es_ct my_vehicle.actuator_estop.status_loop_counter "%6d" -ro;
int:  %es_ab my_vehicle.actuator_estop.about_to_unpause "%6d" -ro;

# Files for skynet feedback
int: %snkey my_vehicle.skynet_key "%6d" -ro;
int: %ga_cct my_vehicle.actuator[ACTUATOR_GAS].command_loop_counter "%6d" -ro;
int: %br_cct my_vehicle.actuator[ACTUATOR_BRAKE].command_loop_counter "%6d" -ro;
int: %st_cct my_vehicle.actuator[ACTUATOR_STEER].command_loop_counter "%6d" -ro;

# OBDII
int: %ob_se my_vehicle.actuator_obdii.status_enabled "%6d" -ro;
int: %ob_st my_vehicle.actuator_obdii.status "%6d" -ro;
double: %ob_rpm my_vehicle.actuator_obdii.engineRPM "%4.0f" -ro;
double: %ob_temp my_vehicle.actuator_obdii.EngineCoolantTemp "%5.1f" -ro;
double: %ob_speed my_vehicle.actuator_obdii.VehicleWheelSpeed "% 3.1f" -ro;
double: %ob_tor my_vehicle.actuator_obdii.WheelForce "% 7.1f" -ro;
#double: %ob_ratio my_vehicle.actuator_obdii.CurrentGearRatio "%7.3f" -ro;
#double: %ob_vol my_vehicle.actuator_obdii.BatteryVoltage "% 4.1f" -ro;
int:  %ob_spd my_vehicle.actuator_obdii.status_sleep_length "%6d" -ro;
int:  %ob_cm my_vehicle.actuator_obdii.command_sleep_length "%6d" -ro;
int:  %ob_ct my_vehicle.actuator_obdii.status_loop_counter "%6d" -ro;

# Supervisory
int: %su_dly my_vehicle.supervisory_delay "%6d";
int: %su_ct my_vehicle.super_count  "%6d" -ro;
int: %guist my_vehicle.gui_status "%6d" -ro;


button: %EXIT "EXIT" dd_exit_loop;
int: %locks my_vehicle.protective_interlocks "%6d";
int: %shtdwn my_vehicle.shutdown "%6d";
