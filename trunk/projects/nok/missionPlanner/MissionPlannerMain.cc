/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include <getopt.h>
#include <iostream>
#include "MissionPlanner.hh"
#include "DGCutils"
#include "sparrowhawk.hh"
#include "skynet.hh"
#include "cmdline_mplanner.h"

using namespace std;

int main(int argc, char **argv) 
{
  gengetopt_args_info cmdline;
  int sn_key;
  bool noSparrow = false;
  bool usingGloNavMapLib = true;
  
  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
  
  /* Figure out what skynet key to use */
  sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;

  if(cmdline.disable_sparrow_flag || cmdline.nosp_flag)
  {  
    noSparrow = true;
    cerr << "NO SPARROW DISPLAY" << endl;
  }
    
  if(cmdline.nomap_given || cmdline.rndf_given)
  {
    usingGloNavMapLib = false;
    cerr << "RNDF file: " << cmdline.rndf_arg << endl;
  }
  
  cout << "MDF file: " << cmdline.mdf_arg << endl;

  CMissionPlanner* pMissionPlanner = new CMissionPlanner(sn_key, !cmdline.nowait_given, 
      noSparrow, cmdline.rndf_arg, cmdline.mdf_arg, usingGloNavMapLib, cmdline.debug_arg,
      cmdline.verbose_given);

  DGCstartMemberFunctionThread(pMissionPlanner, &CMissionPlanner::getTPlannerStatusThread);
  DGCstartMemberFunctionThread(pMissionPlanner, &CMissionPlanner::sendSegGoalsThread);
  if (!noSparrow)
  {
    DGCstartMemberFunctionThread(pMissionPlanner, &CMissionPlanner::SparrowDisplayLoop);
    DGCstartMemberFunctionThread(pMissionPlanner, &CMissionPlanner::UpdateSparrowVariablesLoop);
  }
  pMissionPlanner->MPlanningLoop();

  return 0;
}
