/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

// Test protocol:
// Create a Segment.
// Create 2 Lanes.
// Populate those lanes w/ road objects.
// Print out the segments (which should cause everything else to print)

#include <iostream>
#include <vector>
#include "LocalMapTalker.hh"

using namespace std;
using namespace Mapper;

class CLocalMapTalkerTestSend : public CLocalMapTalker
{
public:
	CLocalMapTalkerTestSend(int sn_key)	: CSkynetContainer(MODmapping, sn_key)
	{
		// Create lane boundaries.
		Mapper::LaneBoundary lb0("Lane Boundary 0", 10.0, 10.0, LaneBoundary::SOLID_WHITE);
		lb0.addLocPoint(15.0, 10.0);
		lb0.addLocPoint(20.0, 10.0);
		lb0.addLocPoint(25.0, 10.0);
		lb0.addLocPoint(30.0, 10.0);
		LaneBoundary lb1("Lane Boundary 1", 10.0, 15.0, LaneBoundary::DOUBLE_YELLOW);
		lb1.addLocPoint(15.0, 15.0);
		lb1.addLocPoint(20.0, 15.0);
		lb1.addLocPoint(25.0, 15.0);
		lb1.addLocPoint(30.0, 15.0);
		LaneBoundary lb2("Lane Boundary 2", 10.0, 20.0, LaneBoundary::SOLID_WHITE);
		lb2.addLocPoint(15.0, 20.0);
		lb2.addLocPoint(20.0, 20.0);
		lb2.addLocPoint(25.0, 20.0);
		lb2.addLocPoint(30.0, 20.0);
		// Create two lanes.
    StopLine stopLine = StopLine("", 0, 0);
		Mapper::Lane lane0(0, lb0, lb1);
		Mapper::Lane lane1(1, lb1, lb2);
    lane1.addStopLine(stopLine);
		vector<Mapper::Lane> lanes;
		lanes.push_back(lane0);
		lanes.push_back(lane1);
		// Create a segment.
		Mapper::Segment seg0(1, lanes);
		
		// Create a map and print the segment out.
		seg0.print();
		vector<Mapper::Segment> segments;
		segments.push_back(seg0);
		Mapper::Map* map = new Map(segments);

		cout << "about to get_send_sock...";
		int mapSocket = m_skynet.get_send_sock(SNtrafficLocalMap);
		cout << " get_send_sock returned" << endl;
  	while(true)
		{
			cout << "about to send a map...";
			SendLocalMap(mapSocket, map);
			cout << " sent a map!" << endl;
 			sleep(2);
		}
	}
};

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;

	CLocalMapTalkerTestSend test(key);
}
