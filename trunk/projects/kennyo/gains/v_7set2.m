
V_0 = 7;   % speed of the vehicle- meters/sec

% loop gain
K = .0035;
% frequency at which the derivative term rolls off
T_r = 17;
% derivative gain
T_d = 8;
% integral gain
T_i = 30;

run run_gains
save v_7set2.mat
