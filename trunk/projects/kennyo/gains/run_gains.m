% gains defined in a separate file (gains for parameters in
% frequency space). this converts them into the appropriate state space
% controller for follow.

% variables needed
% V_0- speed tp linearize around- meters/sec
% K = loop gain
% T_r = frequency at which the derivative term rolls off
% T_d = derivative gain
% T_i = integral gain

close all;
%clear all;


% define the matricies for the system, so we can plot its response
L = 3;      % wheel base of the vehicle in meters
v = V_0;    % different varialble names used for the simulink model
l = L;

A_ = [ 0 V_0; 0 0 ];
B_ = [ 0; V_0/L ];        % I want to plot the frequency response of just the
% process, so there isn't going to be any control yet
C_ = [ 1 0 ];        % Y is the output
D_ = 0;

proc = ss(A_, B_, C_, D_);
process = tf(proc);

% controller

s = tf( 's' );

controller = K * (1 + T_d * s / ((s / T_r) + 1) );
%controller = K * (1 + T_d * s / ((s / T_r) + 1) + 1/(T_i * s));
L = process * controller;
closed_loop = L / (1 + L);

%figure;
%margin(L);
%figure;
%rlocus(closed_loop);
%figure;
%nyquist(L);
%figure;
%step(closed_loop);

[cont_num, cont_den] = tfdata(controller, 'v');

[Ac, Bc, Cc, Dc] = tf2ss(cont_num, cont_den);

% this was from an attempt to make the matricies the right dimension to
% work with an unmodified version of follow. it may or may not have
% worked...
%Bc2 = zeros(2,18);
%Bc2(1, 2) = Bc(1);
%Bc2(1, 11) = -1 * Bc(1);
%Dc2 = zeros(1,18);
%Dc2(1, 2) = Dc(1);
%Dc2(1, 11) = Dc(1);

% convert the continuous time system to a discrete time one
Ts = 0.1;       % follow runs at 10hz..
A = eye( size(Ac) ) + Ts * Ac;
B = Ts * Bc;
C = Cc;
D = Dc;

% do simulink stuff

cd ../model
sim alice
cd ../gains

figure;
plot(tout, yout, tout, rout.signals.values);



% transfer functions and ss systems will cause the mat lab file loader in
% follow to segfault
clear s proc process controller L closed_loop;
