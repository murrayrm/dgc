function probMoving()

%for good ones, w/ Alice stationary, car moving towards (works nicely)
%points = load('stat1_points.dat');
%segs = load('stat1_sentSegs.dat');

%for alice stationary, w/ car moving across...doesn't work as well
%points = load('stat2_points.dat');
%segs = load('stat2_sentSegs.dat');

points = load('points.dat');
segs = load('sentSegs.dat');


[scans, foo] = size(segs);
cars = foo/2; % each car gets two columns

for i=5:1:6 %cars %using 1st 7 for testing (only #6 is actually moving)
    segStart = segs(:,2*i-1);
    segEnd = segs(:,2*i);
    
    if max(segStart) > -1 %this car was actually seen at some point
        figure();
        seen = 0;
        
        for j=1:1:scans
            ss = segStart(j) +1; %matlab's indices are one off from c++
            se = segEnd(j) + 1;
            if ss>0 && seen == 0
              seen = seen+1;
%              model = buildModel(100*[points(2*j-1,ss), points(2*j-1,se); points(2*j,ss), points(2*j,se)]); %expects measures in cm
              model = buildModel(100*[points(2*j-1,ss:se); points(2*j,ss:se)]);
              
            elseif ss > 0 & seen>0 & seen<5 %this car was seen this scan; 
             seen = seen+1;
              pause(.05); %don't want to plot too fast
%              plot(points(2*j-1,ss:se), points(2*j,ss:se),'k.');
%              axis([398970,399040,3781281,3781320]); %for cars moving towards  
%              axis([399000,399030,3781280,3781300]); %for cars moving across
              drawnow;
              dx = points(2*j-1,ss:se) - 399007;
              dy = points(2*j,ss:se) - 3781281;

              
              angles = atan2(dy,dx);
              ranges = sqrt(dx.^2 + dy.^2);
             prob = probModel(angles,100*ranges, model);
             seg_scan_prob = [i,seen,sum(prob)/length(prob)]
              
           end
        end
        
    end
end
