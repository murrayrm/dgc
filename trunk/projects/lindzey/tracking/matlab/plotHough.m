function plotHough(index,file)
tic
%index is which frame of the raw file to plot
data = load(file);
[row,col]=size(data);
ranges = data(index, 3:col);
[row,col] = size(ranges);

for i=1:row
    for j=1:col
        if ranges(i,j)==8191 
            ranges(i,j)=0;
        end
    end
end

temp = [1:col];
angles = 2*pi*temp/360;
%figure(1);
%polar(angles,ranges);
x = cos(angles).*ranges;
y = sin(angles).*ranges;

figure(1);
plot(x,y,'.');

%figure(1);
%plot(ranges,'.');

num = length(x);
xmin = min(x);
xmax = max(x);
ymin = min(y);
ymax = max(y);
grr = zeros(num+1,num+1);

scalex = num/(xmax-xmin);
scaley = num/(ymax-ymin);

for i=1:1:num
    xpt = round(scalex*(x(i)-xmin))+1;
    ypt = round(scaley*(y(i)-ymin))+1;
    grr(xpt,ypt)=1;
end

%from help houghlines

       rotI = grr;
       %BW = edge(rotI,'canny');
       BW = grr;
       figure(3);
       [H,T,R] = hough(BW);
        P  = houghpeaks(H,15,'threshold',ceil(0.1*max(H(:))),'NHoodSize',[15,13]);
       lines = houghlines(BW,T,R,P,'FillGap',20,'MinLength',2);
       toc

     
       imshow(H,[],'XData',T,'YData',R,'InitialMagnification','fit');
       xlabel('\theta'), ylabel('\rho');
       axis on, axis normal, hold on;
       x = T(P(:,2)); y = R(P(:,1));
       plot(x,y,'s','color','white');
 
       % Find lines and plot them

       figure(4), imshow(grr), hold on
       max_len = 0;
       for k = 1:length(lines)
         xy = [lines(k).point1; lines(k).point2];
         plot(xy(:,1),xy(:,2),'LineWidth',1,'Color','green');
 
         % plot beginnings and ends of lines
         %plot(xy(1,1),xy(1,2),'x','LineWidth',1,'Color','yellow');
         %plot(xy(2,1),xy(2,2),'x','LineWidth',1,'Color','red');
 
         % determine the endpoints of the longest line segment 
         len = norm(lines(k).point1 - lines(k).point2);
         if ( len > max_len)
           max_len = len;
           xy_long = xy;
         end
       end
 
       % highlight the longest line segment
       %plot(xy_long(:,1),xy_long(:,2),'LineWidth',2,'Color','cyan');

%If I evern decide to try UTM coords again...       
       % [row,col] = size(UTM)
% coords = UTM(index, 3:col);
% [row,col] = size(coords)
% x=[];
% y=[];
% for i=1:1:col
%    if mod(i,3)==1
%        x=[x,coords(i)];
%    elseif mod(i,3)==2
%        y=[y,coords(i)];
%    end
% end
% %figure(2);
% %plot(x,-1*y,'.');

end