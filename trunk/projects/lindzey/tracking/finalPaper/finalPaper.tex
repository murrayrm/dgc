\documentclass[11pt]{article}

%the defines from jfr06.tex
\usepackage{fullpage}		% use full page width
\usepackage{graphicx}		% graphics support (.eps files)
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}		% format URLs properly
\usepackage{comment}		% define comment environment
\usepackage{afterpage}
\usepackage{natbib}
\bibpunct[, ]{(}{)}{;}{a}{,}{,}

% a todo macro
\newcommand{\todo}[1]{\vspace{3 mm}\par \noindent {\textsc{ToDo}}\framebox{\begin{minipage}[c]{1.0\hsize}\tt #1 \end{minipage}}\vspace{3mm}\par}

\begin{document}
%\twocolumn

\title{Using LADAR to Detect and Track Moving Obstacles from a Mobile Platform}
\author{Laura Lindzey \\ \emph{Mentor: Richard Murray}}
\maketitle



\begin{abstract}
A team of students is working to develop an autonomous vehicle capable of 
operating in an urban setting, including following traffic laws and 
interacting with other moving vehicles. This project deals with on the 
detection and tracking of moving obstacles, which is a baseline capability 
for operating in a dynamic environment. We observe that a human has no difficulty watching 
an animation of LADAR scans and picking out which pixels correspond to a moving object, 
even in the presence of noise and scarce data. Our goal is to match human 
performance at this task. The problem 
can be broken down into segmentation of a scan, initial data association, 
and updating the representation of the environment. The update step is of 
the most interest, because it includes an expectation of how the environment 
should change, and uses the most recent sensor data to refine this 
approximation via a kalman filter. An appropriate representation makes 
this computation more elegant.
\end{abstract}


\section{Introduction}
In 2007, DARPA is holding a third race for autonomous ground vehicles. The 
previous two races were staged in an off-road, desert environment, where the
emphasis was on safely traversing terrain. Team Caltech has developed a vehicle
(Alice) capable of operating in these conditions. For this year's race, the 
vehicles must operate in an urban environment, complete with obeying traffic laws 
and interacting with other moving vehicles. Our goal is to modify Alice to be able 
to complete in the DGC2007 race. 

As it stands now, Alice is capable of mapping static terrain in front of the 
vehicle. The main sensors are 4 laser detection and ranging (LADAR) units, pointed 
at the ground 2m, 20m, 35m, and 50m in front of the vehicle. A LADAR takes range 
measurements at one-degree increments across a 180-degree field of view. They
 are mounted to scan the ground as Alice moves forward, 
compiling elevation maps. This setup is ideal for static environments, any
moving obstacle appears as a wall in our map. This is becasue when we assume a static
environment, a car is mapped as being everywhere it has been seen, not only where it is
at any particular instant in time. Additionally, there is the possibility
that we would never see another car, if we were following it or it drove between our
lines of coverage. Finally, we only have sensor coverage in front of Alice, which is
insufficient for maneuvers such as changing lanes or parking.

Clearly, to operate in an urban environment, a robot needs some understanding of
dynamic obstacles. This includes the ability to identify and track them, as well
as predict their motion. This project deals with the identification and tracking
steps, using data from a horizontally mounted LADAR unit. The project should have
two results working towards the final solution Team Caltech will use in the race: 
an algorithm to track other cars, and an understanding of the limitations of a 
LADAR-based system. A separate module will deal with the long-term prediction step, because
that will require some knowledge of the road network, which yet another module is
using vision data to interpret.

The bulk of previous research has been  vision-based, with an emphasis either on surveillance
or driver assistance in seeing pedestrians, etc. Also, detection and tracking of moving obstacles (DATMO) from a moving platform hasn't been addressed in nearly as many papers: many of them explicitly use the assumption that their sensors are static. 

Kluge et al \citep{KKP01-ieeera} use the convex hull assumption to help in segmentation. I use the same basic ideas they do for segmentation, and hope to implement the convex requirement at some point. 

Lindstrom et al \citep{LE01-ieeersj} use occlusion ideas and a concept of free space to find motion; any change in free space is known to be caused by either a return from a moving obstacle, or something that the MO revealed as it traveled. I really like the idea of using geometric reasoning about the scene to find objects. However, this paper doesn't deal with the problem of tracking objects through multiple images. 

Zhao et al \citep{ZT-misc} represent cars as rectangles. However, it uses a Hough transform 
to find the line segments, which doesn't do so well at longer distances where the
cars are just a few dots. It uses constant vel/acc/turn models for motion, and classifies the car into one of those models. This comes closest to what I am trying to do, but it lacks the ability to handle long-range data or occlusions. 

\section{Approach}

\subsection{Data Input}
For this project I am using a SICK LMS-221 LADAR, mounted on Alice's bumper, with
its beam pointed horizontally. This sensor scans from right to left, returning
range readings with 1-degree spacing at 75Hz. I chose this sensor because its 
range readings make it very easy to locate an object precisely, and the small
amount of data (compared to vision data) makes processing much simpler. Since a 
LADAR effectively give a picture of a slice of the environment, mounting it pointed
parallel to the ground should allow it to detect anything sticking out of the ground
plane. A trained human looking at an animation of this data can easily pick out a car at about 35 meters, and track it through the whole data set. I also assume that accurate global state data will be provided by another model. 

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{successive_scans.eps}
  \caption{LADAR scans separated by 0.13 sec, showing a car moving towards Alice.
Different colors are separate scans, superimposed}
  \label{fig:successive_scans}
\end{figure}

\begin{figure}
  \includegraphics[width=0.9\textwidth]{OverallArchitecture.eps}
  \caption{Current architecture for the overall project}
  \label{fig:overall_architecture}
\end{figure}

\subsection{Expected Output}
The expected output for this module is a list of moving cars, with their associated positions, dimensions, velocities, and unique ID number. It is important that we output not only where moving objects currently are, but that they've been correctly associated with previously seen ones, because the prediction module needs this data. Also, we attach the object's history, so the mapper can correctly remove its whole representation from the static map. All of the necessary interfaces have been implemented. See figure \ref{fig:overall_architecture} to see how this module is expected to fit in with the rest of the software required to run Alice. 

\subsection{Performance Metric} 
Algorithms are judged on whether they identify all moving objects in the sensor 
data. Next in  importance is trying to minimize the frequency that an object gets
dropped and re-detected, associated with a different ID number. Finally, we'd like to 
minimize the occurrence of extraneous MO's identified. (spurious returns, or
segments actually belonging to the environment)

\subsection{Algorithm Design}

To lessen the impact of a mobile platform, I chose to use global coordinates
for all calculations. In doing this, I am assuming that our state data from
only GPS and IMU is good, and am choosing not to try to close the loop with
additional sensors (SLAM, visual odometry, etc). This also has the effect
that none of the tracking algorithm depend on whether Alice is stationary
or moving. Other guiding principles were that it needs to be able to track however few or many moving obstacles may be in a scene, and leaving the possibility open for easy generalization to multiple LADARs. 

I have divided the architecture into discrete steps: segmentation, initial
data association, classification, model update, and cleanup. Since only
segmentation, and to some extent, initial data association, are dependent on the actual sensor being used, this should allow the code to generalize to other sensor types, as well as multiple bumper LADARs. 

\paragraph{World Representation}
Over the course of the summer, I've come to realize that an appropriate model format makes all the difference in how elegant or messy the rest of the code will be.

For the initial tracking steps, before an object has been classified as a car,
we represent objects by their centroid. This was the most general representation
available, and has been shown to work better than trying to fit a more specific model
to an arbitrary object. For example, line segments work really well to represent walls, 
but when used to represent a bush, the best-fit line segment will change drastically
between one scan and the next. 

A horizontally mounted LADAR will see a car as either a single line of points 
or as a corner, and DARPA has said that the only moving objects on the course 
will be vehicles. Therefore, I've chosen to represent any moving obstacle as 
a rectangle. We track the back right corner, and keep track of the sides using
two sets of delta-coordinates. This is a fairly simple representation, but it
has the important quality of allowing us to track features of the car through frames, 
not just the the boundaries of what is currently visible.

This is necessary when a car passes behind something else. If you're tracking just 
endpoints or a centroid, it will look as if the velocity is changing and the object 
shrinking, when really you're just not seeing the whole car. By tracking a specific
feature it is possible to get an accurate measurement of change in position, even
when the whole object is not visible. 

\paragraph{Segmentation}
The first step in tracking objects is to segment the individual objects out of the 
LADAR range scan. This can be done by looking for discontinuities in range 
measurement. A distance-scaled threshold is defined for maximum allowable 
difference between consecutive range measurements, and this is used to break 
the image into objects.

Occasionally, this does segment the back of the car differently than the rest, if
the angle is small enough. It would be nice if I could find some way to group points 
based on similar motion rather than spatially, but the spatial method is nice 
because it only requires one scan. 

\paragraph{Initial Data Association}
Once objects have been segmented in individual scans, it is necessary to match 
them between successive scans for the initial data association. Since the LADAR 
scans at 75Hz and we know that the maximum relative speed that objects can be 
traveling in the Urban Challenge, it is possible to associate groups of points 
based on proximity. The maximum travel distance (40cm) is enough smaller than 
the objects we're interested in tracking that there shouldn't be a problem with 
incorrect matching.

For data association purposes, the tracked objects are represented by the 
centroid of the points observed. The velocities of the tracked objects are 
estimated using a very simple Kalman Filter, with state being represented 
by $N$, $E$, $\dot{N}$ and $\dot{E}$, and no correlation between northing and easting. 
We assume that at the time steps involved (1/75th of a second) any tracked object
can be represented as a point mass with constant velocity. Any accelerations are 
modeled as noise. 
At each time step, the KF is updated with the object's new position. 
Thus, the Kalman Filter matrices for Northing would be:

$
X = 
\left[
\begin{array}{ c c}
N \\
\dot{N}
\end{array}\right]
,    A =
\left[ 
\begin{array}{ c c}
1 & 0 \\
0 & dt 
\end{array}\right]
,    B = 0,    C = 
\left[
\begin{array}{ c c}
0 & 1
\end{array}\right]
$

$ 
R = 
\left[
\begin{array}{ c c}
0 & 0 \\
0 & .05
\end{array}\right]
, Q = 
\left[
\begin{array}{ c c}
.1
\end{array}\right]
$

In the simple case
of no occlusions, this method tracks objects very well on its own, without 
any fancier representation required. However, when an object passes behind 
something else, its center appears to slow down, causing a too-slow velocity 
estimate, and making it impossible to pick up on the other side. 

\paragraph{Classification}
The only classification that I care about at this point is moving versus stationary. Ideally, this classification would be made in as few scans as possible, to cut down on delays in the system and allow better reasoning about traffic. Finally, I implemented a kalman filter for the object positions, as mentioned above. If the estimated velocity is above a threshold after some number of updates, that object is considered a car. This works decently in practice, but it takes more scans that I'd like, and still gives false positives (not sure how much this has to do with tuning the uncertainties...I haven't spent much time on that)

Once a new scan has been segmented, each segment is classified as being a stationary object, a moving car, or a new object. This is done using the predicted positions of the environment objects and tracked cars, as estimated by their kalman filters. 

\paragraph{Update}
Once we've classified the new objects in a scan, we need to update the appropriate representations. Environment objects are simple -- just use the new observed position of their center to update their KF, and add new points/ranges/angles to their history (for purpose of initializing a car, if it turns out that this object is moving). Similarly for initializing a new object.

However, for cars, it is necessary to determine which features should be used for motion estimation. This turned out to be the key for correctly tracking an object past an occlusion. Using the raw range measurements, it is possible to determine whether the observed edge of an object is in the background or foreground. If it's in the background, then you can know that the actual edge is probably obscured, and you shouldn't trust this new measurement for update purposes. If it's in the foreground, then it is safe to use this edge's update, because you know you've seen to the end of the car (to the limit of sensing/segmentation errors). 

\paragraph{Cleanup}
For the sake of performance, it is necessary to remove objects that haven't 
been seen recently and to classify any objects that were initially identified
as static, but further observations have shown motion. Now that
there's an updated representation of the environment, a list of the currently tracked 
moving obstacles and their positions is broadcast to any other interested modules.


\section{Results}

\subsection{Current Status}
At this point, I've implemented all of the above-described steps. 
The geometric reasoning requires some more work, and I am pretty 
sure that there are more bugs that additional data sets will reveal. 
The most recent version of the code correctly tracked two cars crossing 
in front of Alice, including picking up a car after an occlusion. 
Figure \ref{fig:tracked_occlusion} shows three frames of this process. 

\begin{figure}[h]
\begin{tabular}{ccc}
\includegraphics[width=.33\textwidth]{scan1.eps}
&
\includegraphics[width=.33\textwidth]{scan2.eps}
&
\includegraphics[width=.33\textwidth]{scan3.eps}
\\ (a) & (b) & (c)
\end{tabular}
\caption{Two cars heading towards each other. Note that each each is represented by the four corners of a rectangle. The hollow circles are the predicted corner positions, and the colored dots are the most recently measured positions. The 'cars' in the background are erroneously classified. (a) shows the two cars before either is occluded. In (b), the second car is directly behind the first, so it doesn't appear in the ladar scan. However, it's predicted position still shows up. In (c), the second car is no longer behind the first, and it has been correctly associated with the predicted position}
\label{fig:tracked_occlusion}

\end{figure}


A previous version of the code, without any prediction capability, as tested in a more 
realistic situation, running real-time and with fusionMapper. For this test, we had 
Alice moving, with traffic approaching head-on. Surprisingly enough, on the first run, 
we detected the other car perfectly,and the new FusionMapper appropriately subtracted 
it from the map, as can be seen in figure \ref{fig:trackedCar_gui}. Plenty of code has 
changed since then, but this seems to demonstrate proof-of-concept that my algorithms 
work just as well on a mobile platform. 

\begin{figure}[h]
  \centering
  \includegraphics[width=0.95\textwidth]{Comparison-1.eps}
  \caption{Performance tracking a car approaching Alice head-on (Alice also moving)
First frame shows data with no tracking, second frame shows tracked points, 
third frame shows performance after fusionMapper has performed data association.
Note spurious reported obstacles due to bumper LADAR hitting the ground.}
  \label{fig:trackedCar_gui}
\end{figure}

\ref{fig:trackedCar_matlab} shows the performance of the initial association step on its own. In this case, with no occlusions, it correctly tracks an object coming towards Alice, from about 35m away to 5m. 

\begin{figure}[h]
\centering
  \includegraphics[width=0.4\textwidth]{trackedCar.eps}
  \caption{Performance tracking a single car with Alice stationary.
Each differently colored dot corresponds to a separate tracked vehicle, 
plotted at 75Hz. The black dots are the actual LADAR scans, plotted at ~4Hz}
  \label{fig:trackedCar_matlab}
\end{figure}

\subsection{Future Plans}

The decision to turn a tracked object into a car is important and tricky. If you turn too many things into cars, it will confuse fusionMapper and planner, as well as trying to describe objects with a representation unsuited to the task. For example, trying to represent a bush as a line segment leads to spurious velocity estimates, as the line segment jumps around, finding the best fit to each new set of points. However, it is important to use as few scans to make this decision as it is possible to, because any time spent in this processing step slows down Alice's ability to react to traffic. 

I tried to solve this problem by reasoning that a human can tell w/in about 5-10 scans if an object is moving or not. There should be a way to model the probability that a second scan matches the same object that the first scan did. This has the potential to work, and correctly classify motion in relatively few (5 or so) scans, as seen in. To some extent, I got this working, but the integration that it required slowed the code down a lot and it didn't handle all cases correctly. I'd like to look at this idea some more, as I think it could work really well with the right implementation.

 Additionally, I plan to implement a step requiring all 
objects to be convex. This is necessary to distinguish between nearby objects, 
and works because almost anything that we care about in an urban environment 
will be convex. In particular, this will help to segment a row of cars that's 
parked close enough together to look like a single zig-zag line, as seen in \ref{fig: needConvexHull}.

\begin{figure}[h]
\includegraphics[width=0.5\textwidth]{segmentation.eps}
\caption{Output of the segmentation step. Each 'segment' is represented by points of the same color. Note on the lower left, where there is a long, zig-zag line. This is really a row of parked cars, but there wasn't any large discontinuity}
\label{fig:needConvexHull}
\end{figure}

As it stands now, the environment representation is the same as that provided by the initial data association step. This only keeps track of the center point of objects, which doesn't use all the available data. In particular, it doesn't allow reasoning about free space, which could be a very powerful tool. I think that I want to try representing the environment as a list of objects, described by their convex hulls. In real-world applications, a linear assumption will not suffice, and I don't like the idea of a grid, because in urban situations you care more about discrete objects.

At some point, we will have bumper ladars mounted on all 4 sides of Alice. It 
will be necessary to adapt my code to take data from more than one sensor. 
Since they are of the same type, it should be fairly easy, except for the data
association problems caused by their overlapping areas of coverage. Also,
the algorithm needs a way to report if/where the LADAR beams are hitting the ground 
plane, so we will know that we can't rely on bumper LADAR data for tracking 
objects. 

\section{Conclusions} 

I've had enough success with my project this summer that I think we should definitely 
include bumper LADARs in our solution to the DATMO problem. They are particularly
well-suited to tracking obstacles on Alice's sides and rear, where we care about cars relatively close to us (in the next lane over). As part of this, we need to install LADARs on the sides and rear of Alice, and adapt the code to appropriately deal with the new data. This will be an interesting challenge, due to the overlap in sensor coverage they will provide. 

However, I do not think that LADARs are suitable for handling the DATMO task on their own. They have a tendency to intersect the ground plane if the road has any significant slope, or if 
Alice is pitching for any reason, which effectively blinds Alice. Also, their ~40m maximum effective range means that they will not pick up cars approaching head-on in the time currently required by our sensor specification calculations. It seems to me that cameras would be better suited to detecting motion farther away from the vehicle, and LADARs for tracking and
precisely locating the object. Thus, any final implementation should use both 
types of sensors.

\bibliographystyle{plain}
\bibliography{lindzeybib}




\end{document}