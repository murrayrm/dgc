#include "ggis.h"
#include <iostream>
#include <iomanip>
#include <stdlib.h>
using namespace std;

int main(int argc, char *argv[])
{
  if (argc != 3) {
    cout << "Converts from UTM to lat-long coordinates." << endl;
    cout << "Usage: utm2ll N E" << endl;
    return 0;
  }
  double N = atof(argv[1]);
  double E = atof(argv[2]);
  GisCoordLatLon latlon;
  GisCoordUTM utm = {11, 'S', N, E};
  if (gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL)) {
    cout << setprecision(20);
    cout << "Lat, Long = " << endl;
    cout << latlon.latitude << "," << latlon.longitude << endl;
    return 0;
  } else {
    cout << "Didn't work." << endl;
    return 1;
  }
}
