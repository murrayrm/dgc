#ifndef _MAP_ACCESS_H_
#define _MAP_ACCESS_H_

#include "CMap.hh"
#include "CDynMap.hh"

double getTimeMapValueGrownNoDiff(CMap* pMap, int mapLayer, bool bPassableNodata,
				  double initSpeed,
				  double Northing, double Easting,
				  double sinyaw, double cosyaw,
				  double len);

/**
   Computes map value and derivatives at the specified spacetime location.
 
   Extension for use with time dependent constraints:
   - fuses value from static map with time dependent value
   - computes derivative wrt time at specified spacetime location

   According to doxygen:
   Referenced by
   - trajSelector::assignSpeedProfile()
   - trajSelector::checkSpeed()
   - trajSelector::select().
   - trajChecker::check()
   - CPlanner::getMinSpeedOn()
   - CDynRefinementStage::makeCollocationData()
   - CDynRefinementStage::SeedSpeedFromState()
*/
void getContinuousMapValueDiffGrown(int kernelWidth,
				    CMap* pMap, int mapLayer, CDynMap* dynMap, bool bPassableNodata,
				    double initSpeed,
				    double Northing, double Easting, double yaw, double Time,
				    double *fval, double *dfdN, double *dfdE, double *dfdyaw, double *dfdT);

void getContinuousMapValueDiffGrown(int kernelWidth,
				    CMap* pMap, int mapLayer, bool bPassableNodata,
				    double initSpeed,
				    double Northing, double Easting, double yaw,
				    double *fval, double *dfdN, double *dfdE, double *dfdyaw);

void getContinuousMapValueDiff(CMap* pMap, int mapLayer,
			       double UTMNorthing, double UTMEasting,
			       double *f, double *dfdN, double *dfdE);

double approxMin(double* f, double* dmin_df);

#endif

