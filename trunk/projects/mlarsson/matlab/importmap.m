% *************************
% importmap.m:
% Imports a CMap (in pgm format) and a CTraj (saved by CTraj::print() ),
% makes the appropriate coordinate transformations, merges the two and
% draws the merged image.
%
% Input: path + filename of trajectory to be imported (e.g.:
% '../planner/outtraj').
% *************************
function importmap(trajname,mapname)


% Define necessary variables
%trajname = '../planner/outtraj';
%mapname = '../dplanner/cmap.pgm';
% resRows = 0.2000;
% resCols = 0.2000;
% blNRowMul = 19171625;
% blEColMul = 2212600;
resRows = 0.4000;
resCols = 0.4000;
blNRowMul = 9585813;
blEColMul = 1106300;

readBaseMap = 1;    % true or false

% Import traj and plot
traj = loadtraj(trajname);
% load rddfwpt;
% traj=[];
% traj(:,1)=rddfwpt(:,1);
% traj(:,4)=rddfwpt(:,2);

% Transform to pixel framework coordinates
rows = round( traj(:,1) / resRows - blNRowMul ) + 1;    % Northing
cols = round( traj(:,4) / resCols - blEColMul ) + 1;    % Easting
% plus one is because Matlab indices start from 1, not 0

% Read the basemap and merge trajectory
basemap = imread(mapname);
map = merge_into_map(rows,cols,50,basemap);
image(map);
title(trajname)