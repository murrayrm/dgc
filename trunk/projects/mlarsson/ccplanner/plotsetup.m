length = 1;
% load testlegalregion.txt;
load teststartfinish.txt;
% plot(testlegalregion(:,1),testlegalregion(:,2),'g-','LineWidth',2);
plotRDDF
hold on;
start = teststartfinish(1,1:2);
start(2,1) = start(1,1) + length*cos(teststartfinish(1,3));
start(2,2) = start(1,2) + length*sin(teststartfinish(1,3));
finish = teststartfinish(2,1:2);
finish(2,1) = finish(1,1) + length*cos(teststartfinish(2,3));
finish(2,2) = finish(1,2) + length*sin(teststartfinish(2,3));
% plot(start(:,1),start(:,2),'r-');
% plot(finish(:,1),finish(:,2),'r-');
set(gca,'DataAspectRatio',[1 1 1]);