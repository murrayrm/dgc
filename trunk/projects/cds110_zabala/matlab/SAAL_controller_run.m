% Runs stopping at a line controller simulation

clear all;

% calculate noisy vision and GPS data
run sensing_input_sim;

% run the SIMULINK model
sim SAAL_Controller;