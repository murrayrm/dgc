% Spoofs inputs from sensing

clear all;

% CONSTANTS
max_error = 50;

% Load the traj file
traj = dlmread('stluke_sine_wave.traj');

% compute longitudinal distance (add up segments of traj file)
for (i = 1:526)
    
    %time
    segments(i,1) = traj(i,1);
    % length of segment
    segments(i,2) = sqrt( (traj(i,4)-traj(i+1,4))^2 + ...
        (traj(i,5)-traj(i+1,5))^2 );
    
end


% compute total longitudinal distance at each timestep
for (i = (size(segments,1)-1):-1:1)
    segments(i,2) = segments(i,2) + segments(i+1,2);
end

%figure;
%plot(segments(:,1),segments(:,2));

% save segments to mat file for use in SIMULINK model
save('distance.mat','segments');

% store total time for running path
total_time = segments(size(segments,1),1);

% store initial error
init_pos_err = segments(1,2)

% store seed for random number generator
seed = random('Normal',0,10);

% run simulation
%sim sensing_inputs;

% NOTE: in the end, generate noisy data and save to a new traj file