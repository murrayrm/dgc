
/* 
 * Desc: Remote viewer utility
 * Date: 27 November 2006
 * Author: Andrew Howard
 * CVS: $Id: mtp_remote.cc,v 1.3 2007/01/02 20:56:22 abhoward Exp $
*/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <FL/Fl.H>
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <jplv/Fl_Glv_Window.H>
#include <jplv/jplv_image.h>

#include "sensnet.h"
#include "sensnet_types.h"
#include "sn_types.h"
#include "VehicleState.hh"

#if USE_RNDF
#include "rndf.hh"
#endif

#include "tsim.h"
#include "tsim_view_opt.h"


// Useful message macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Application class
class app_t
{
  public:

  // Default constructor
  app_t();

  // Destructor
  ~app_t();
  
  // Parse the command line
  int parse_cmdline(int argc, char **argv);

  // Initialize GUI
  int init(int cols, int rows);

  // Finalize GUI
  int fini();

  public:

  // Exit callback
  static void on_exit(Fl_Widget *w, int option);

  // Predraw callback
  static void on_predraw(Fl_Glv_Window *win, app_t *self);

  // Draw callback
  static void on_draw(Fl_Glv_Window *win, app_t *self);

  // Idle callback
  static void on_idle(app_t *self);

  public:

  // Draw a virtual camera view
  int draw_camera(double cam_pose[6], float cam2veh[4][4]);

#if USE_RNDF
  // Draw an RNDF
  int draw_rndf(RNDF *rndf);
#endif

  public:
    
  // Command-line options
  struct gengetopt_args_info options;

  // Sensnet settings
  char *spread_daemon;
  int skynet_key, module_id;
  
  // Sensnet
  sensnet_t *sensnet;
  
  // Simulator
  tsim_t *sim;

#if USE_RNDF
  // RNDF
  RNDF *rndf;
#endif

  // UTM coordinates of display (for better numerical conditioning)
  vec3_t map_offset;

  // Virtual camera texture
  GLuint image_texture;
  
  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;

  // Map window
  Fl_Glv_Window *mapwin;
  
  // Should we quit?
  bool quit;
};


// Commands
enum
{
  // TODO APP_ACTION_PAUSE = 0x1000,
};


// Set up the menu
static Fl_Menu_Item menuitems[] =
{
  {"&File", 0, 0, 0, FL_SUBMENU},    
  {"E&xit", FL_CTRL + 'q', (Fl_Callback*) app_t::on_exit},
  {0},
  {"&View", 0, 0, 0, FL_SUBMENU},    
  // TODO {"Pause", ' ', NULL, (void*) APP_ACTION_PAUSE, FL_MENU_TOGGLE},
  {0},
  {0},
};


// Default constructor
app_t::app_t()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
app_t::~app_t()
{
  return;
}


// Parse the command line
int app_t::parse_cmdline(int argc, char **argv)
{
  // Run parser
  if (cmdline_parser(argc, argv, &this->options) != 0)
  {
    cmdline_parser_print_help();
    return -1;
  }

  if (this->options.inputs_num < 1)
    return ERROR("no world file specified");

  if (!this->options.disable_sensnet_flag)
  {
    // Fill out the spread name
    if (this->options.spread_daemon_given)
      this->spread_daemon = this->options.spread_daemon_arg;
    else if (getenv("SPREAD_DAEMON"))
      this->spread_daemon = getenv("SPREAD_DAEMON");
    else
      return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
    // Fill out the skynet key
    if (this->options.skynet_key_given)
      this->skynet_key = this->options.skynet_key_arg;
    else if (getenv("SKYNET_KEY"))
      this->skynet_key = atoi(getenv("SKYNET_KEY"));
    else
      this->skynet_key = 0;
  }

  return 0;
}


// Initialize stuff
int app_t::init(int cols, int rows)
{
  int w, h;
  double ax, ay, az, bx, by, bz;
  
  // Create the simulator
  this->sim = tsim_alloc();
  if (tsim_open(this->sim, this->options.inputs[0]) != 0)
    return ERROR("unable to open %s", this->options.inputs[0]);

#if USE_RNDF
  if (true)
  {
    char filename[1024];
    snprintf(filename, sizeof(filename), "%s/rndf.txt", this->options.inputs[0]);

    MSG("loading rndf %s", filename);
    
    // Create RNDF   
    this->rndf = new RNDF();
    if (!this->rndf->loadFile(filename))
      ERROR("unable to load RNDF file; skipping");
  }
#endif  
  
  if (!this->options.disable_sensnet_flag)
  {
    // Create sensnet
    this->sensnet = sensnet_alloc();
    if (sensnet_connect(this->sensnet, this->spread_daemon, this->skynet_key, this->module_id) != 0)
      return -1;
    if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState), 5) != 0)
      return -1;
  }

  w = cols;
  h = rows;
  
  this->mainwin = new Fl_Window(w, h + 30, "DGC Sim Viewer");
  this->mainwin->user_data(this);
  this->mainwin->size_range(w,  h + 30);

  this->mainwin->begin();
    
  // Set up the main window
  this->menubar = new Fl_Menu_Bar(0, 0, w, 30);
  this->menubar->user_data(this);
  this->menubar->menu(menuitems); 
    
  // Create map window
  this->mapwin = new Fl_Glv_Window(0, 30, w, h, this,
                                   (Fl_Callback*) on_predraw, (Fl_Callback*) on_draw);

  this->mainwin->end();

  // Make main window resizable 
  this->mainwin->resizable(this->mapwin);

  // Get the map bounding box in global (UTM) frame
  tsim_get_bounds(this->sim, &ax, &ay, &az, &bx, &by, &bz);

  // Record the offset to use during drawing (everything is drawn in
  // the map frame).
  this->map_offset.x = ax;
  this->map_offset.y = ay;
  this->map_offset.z = az;
  
  // Set the initial viewpoint
  this->mapwin->set_lookat((bx - ax)/2, (by - ay)/2, -50,
                           (bx - ax)/2, (by - ay)/2, 0,
                           0.01, 0, -1);
  
  // TODO
  this->mapwin->set_clip(10, 300);
  
  /* TODO
  // Hook up menu options
  const Fl_Menu_Item *item;
  for (i = 0; i < this->menubar->menu()->size(); i++)
  {
  item = this->menubar->menu() + i;
  if (item->user_data() == (void*) APP_ACTION_PAUSE)
  this->action_pause = item;
  }
  */

  return 0;
}


// Finalize styff
int app_t::fini()
{
  // Clean up simulator
  if (this->sim)
  {
    tsim_close(this->sim);
    tsim_free(this->sim);
    this->sim = NULL;
  }

  // Clean up SensNet
  if (this->sensnet)
  {
    sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
    sensnet_disconnect(this->sensnet);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
 
  return 0;
}


// Handle menu callbacks
void app_t::on_exit(Fl_Widget *w, int option)
{
  app_t *self;

  self = (app_t*) w->user_data();
  self->quit = true;

  return;
}



// Predraw callback
void app_t::on_predraw(Fl_Glv_Window *win, app_t *self)
{
  double r;

  // TODO: orthographic projection

  // TODO: mouse handling
  
  // HACK
  // Make sure whatever we are looking at is inside the clipping
  // boundaries
  r = vec3_mag(vec3_sub(win->at, win->eye));
  win->set_clip(1.0, 2 * r);
  
  return;
}


// Draw callback
void app_t::on_draw(Fl_Glv_Window *win, app_t *self)
{
  if (true)
  {
    float size;
    size = 1.0;

    // TESTING
    // Show coordinate origin
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(size, 0, 0);
    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, size, 0);
    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, size);
    glEnd();
  }
  
  if (self->sim)
  {    
    // Draw the map
    tsim_render_map(self->sim);

    // Draw the vehicle
    if (self->sensnet)
    {
      VehicleState state;
      if (sensnet_read(self->sensnet, SENSNET_SKYNET_SENSOR,
                       SNstate, -1, sizeof(VehicleState), &state) == 0)
      {
        double veh_pose[6];
        veh_pose[0] = state.Northing;
        veh_pose[1] = state.Easting;
        veh_pose[2] = state.Altitude;
        veh_pose[3] = state.Roll;
        veh_pose[4] = state.Pitch;
        veh_pose[5] = state.Yaw;

        // Draw vehicle
        tsim_render_vehicle(self->sim, veh_pose);
      }
    }

    // TESTING
    if (!self->sensnet)
    {
      pose3_t pose;
      double veh_pose[6];
      float cam2veh[4][4];
  
      // Vehicle pose
      veh_pose[0] = self->map_offset.x + 3.3 * 256 / 8;
      veh_pose[1] = self->map_offset.y + 4.5 * 256 / 8;
      veh_pose[2] = 0;
      veh_pose[3] = 0;
      veh_pose[4] = 0;
      veh_pose[5] = 0;

      // Camera pose
      pose.pos = vec3_set(3, 0, -2);
      pose.rot = quat_from_rpy(0.899140, 0.000917, 1.588830);
      pose3_to_mat44f(pose, cam2veh);
      
      // Draw vehicle
      tsim_render_vehicle(self->sim, veh_pose);
            
      // Draw virtual camera
      self->draw_camera(veh_pose, cam2veh);
      
      // Render a SICK ladar
      pose.pos = vec3_set(3, 0, -2);
      pose.rot = quat_from_rpy(0, -10 * M_PI/180, -45 * M_PI/180);
      pose3_to_mat44f(pose, cam2veh);
      tsim_render_ranges(self->sim, veh_pose, cam2veh, 0.10, 30.0, M_PI/180, 181);
    }
  }

#if USE_RNDF
  // TESTING
  //if (self->rndf)
  //  self->draw_rndf(self->rndf);
#endif
  
  return;
}


// Draw a virtual camera view
int app_t::draw_camera(double veh_pose[6], float cam2veh[4][4])
{
  float cx, cy, sx, sy;
  int cols, rows, channels, size;
  uint8_t *data;

  if (!this->sim)
    return 0;
  
  // Set up default camera
  cols = 320;
  rows = 240;
  cx = cols / 2;
  cy = rows / 2;
  sx = 2 * cols;
  sy = 2 * cols;
  channels = 3;
  size = cols * rows * channels;

  data = (uint8_t*) malloc(size);

  // Generate an image
  tsim_gen_image(this->sim, veh_pose, cam2veh,
                 cx, cy, sx, sy, cols, rows, channels, size, data);

  // TESTING
  if (false)
  {
    jplv_image_t *image;
    image = jplv_image_alloc(cols, rows, channels, 8, 0, data);

    MSG("writing cam test");
    jplv_image_write_pnm(image, "cam-test.pnm", NULL);
    
    jplv_image_free(image);
  }

  // TODO
  if (this->image_texture == 0)
  {
    glGenTextures(1, &this->image_texture);
    assert(this->image_texture > 0);
  }

  // Update the texture
  glBindTexture(GL_TEXTURE_2D, this->image_texture);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  if (channels == 1)
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_INTENSITY, cols, rows,
                      GL_INTENSITY, GL_UNSIGNED_BYTE, data);
  else if (channels == 3)
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, cols, rows,
                      GL_RGB, GL_UNSIGNED_BYTE, data);
  
  free(data);

  // HACK
  // TODO move
  if (true)
  {
    int wx, wy;
    GLint vp[4];

    // Configure viewport for image display
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glGetIntegerv(GL_VIEWPORT, vp);
    glTranslatef(-1, 1, 0);
    glScalef(2.0 / vp[2], -2.0 / vp[3], 1.0);
    
    wx = 128;
    wy = wx * rows / cols;

    // Draw the image 
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, this->image_texture);
    glColor3f(1, 1, 1);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(wx, 0);
    glTexCoord2f(1, 1); glVertex2f(wx, wy);
    glTexCoord2f(0, 1); glVertex2f(0, wy);
    glEnd();
    glDisable(GL_TEXTURE_2D);

    // Restore viewport
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();    
    glMatrixMode(GL_MODELVIEW);  
  }
  
  return 0;
}


#if USE_RNDF
// Draw an RNDF
int app_t::draw_rndf(RNDF *rndf)
{
  int i, j, k;
  Segment* segment;
  Lane* lane;
  Waypoint *wp;
  float px, py, pz;
  
  for (i = 0; i < (int) rndf->getAllSegments().size(); i++)
  {
    segment = rndf->getAllSegments()[i];
    
    MSG("segment %d %d", i, segment->getSegmentID());

    for (j = 0; j < (int) segment->getAllLanes().size(); j++)
    {
      lane = segment->getAllLanes()[j];

      glPointSize(5);
      glBegin(GL_POINTS);
      for (k = 0; k < (int) lane->getAllWaypoints().size(); k++)
      {
        wp = lane->getAllWaypoints()[k];

        px = (float) (wp->getNorthing() - this->map_offset.x);
        py = (float) (wp->getEasting() - this->map_offset.y);
        pz = (float) (0 - this->map_offset.z);
        
        glVertex3f(px, py, pz);

        MSG("wp %.3f %.3f %.3f", px, py, pz);
      }
      glEnd();
    }
  }
  
  return 0;
}
#endif


// Handle idle callbacks
void app_t::on_idle(app_t *self)
{ 
  if (self->mapwin->mouse_state > 0)
    return;
    
  // TODO
  usleep(100000);
  self->mapwin->redraw();
  
  return;
}


// Main loop
int main(int argc, char *argv[])
{
  app_t *app;

  app = new app_t();

  if (app->parse_cmdline(argc, argv) != 0)
    return -1;
  
  // Initialize gui
  if (app->init(1024, 768) != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) app_t::on_idle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  // Clean up
  app->fini();
  delete app;

  MSG("exited cleanly");
  
  return 0;
}
