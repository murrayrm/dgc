/* 
 * Desc: Toy-world simulator for DGC
 * Date: 06 December 2006
 * Author: Andrew Howard
 * SVN: $Id$
*/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#if USE_GL
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

//#include <mat44.h>
#include <pose3.h>
#include <jplv/jplv_image.h>

#include "AliceConstants.h"
#include "ggis.h"

#include "tsim.h"
#include "tsim_opt.h"



// Map fragment data/
typedef struct
{
  // Color data file
  char *color_name;

  // Elevation data file
  char *elev_name;

  // UTM zone
  char zone[3];
  
  // Fragment bounding box, simulation coordinates
  float ax, ay, bx, by;

} tsim_frag_t;


// Map cell data
typedef struct
{
  // Color value
  uint8_t color[3];

  // Elevation value
  uint8_t elev;
  
} tsim_cell_t;


// Map tile description.
typedef struct
{
  // Tile bounding box, simulation coordinates
  float ax, ay, bx, by;
  
  // Position of lower-left corner of tile in map frame
  int ai, aj, bi, bj;
  
  // Cell data (if loaded)
  tsim_cell_t *cells;

#if USE_GL
  // Textures at varying pyramid levels
  int texture;
#endif

} tsim_tile_t;



// Tsim context.
struct tsim
{
  // World file options
  struct _tsim_opt_t options;

  // Map fragments data (pointers to map image files)
  int num_frags;
  tsim_frag_t frags[64];

  // Resolution (cells/m)
  int res;
  
  // Number of tiles in each map dimension
  int num_map_tiles;

  // Number of cells in each tile dimension
  int num_tile_cells;

  // Offset for UTM to simulation frame
  vec3_t utm_offset;
  
  // Map bounding box (simulation frame)
  float map_ax, map_ay, map_az, map_bx, map_by, map_bz;

  // Map tile data
  tsim_tile_t *tiles;
};


// Load data for the given tile
int tsim_tile_load(tsim_t *self, tsim_tile_t *tile);

// Discard a tile contents
int tsim_tile_unload(tsim_t *self, tsim_tile_t *tile);


// Useful message macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)



// Create context and allocate resources.
tsim_t *tsim_alloc()
{
  tsim_t *self;

  self = (tsim_t*) calloc(1, sizeof(tsim_t));
  
  return self;
}


// Destroy context and free all resources.
int tsim_free(tsim_t *self)
{
  free(self);
    
  return 0;
}


// Convert from simulation to map coordinates
__inline__ void tsim_w2m(tsim_t *self, float wx, float wy, int *mi, int *mj)
{
  *mi = (int) ((wx - self->map_ax) * self->res + 100000) - 100000;
  *mj = (int) ((wy - self->map_ay) * self->res + 100000) - 100000;  
  return;
}


// Convert from map to simulation coordinates
__inline__ void tsim_m2w(tsim_t *self, int mi, int mj, float *wx, float *wy)
{
  *wx = (float) mi / self->res + self->map_ax;
  *wy = (float) mj / self->res + self->map_ay;  
  return;
}


// Get the tile at the given tile index
__inline__ tsim_tile_t *tsim_get_tile(tsim_t *self, int pi, int pj)
{
  if (!self->tiles)
    return NULL;
  if (pi < 0 || pi >= self->num_map_tiles)
    return NULL;
  if (pj < 0 || pj >= self->num_map_tiles)
    return NULL;  
  return self->tiles + (pi + pj * self->num_map_tiles);
}


// Get the cell at the given tile index
__inline__ tsim_cell_t *tsim_get_tile_cell(tsim_t *self, tsim_tile_t *tile, int pi, int pj)
{
  if (!tile->cells)
    return NULL;
  if (pi < 0 || pi >= self->num_tile_cells)
    return NULL;
  if (pj < 0 || pj >= self->num_tile_cells)
    return NULL;  
  return tile->cells + (pi + pj * self->num_tile_cells);
}


// Get the cell at the given map index
__inline__ tsim_cell_t *tsim_get_map_cell(tsim_t *self, int mi, int mj)
{
  tsim_tile_t *tile;  
  tile = tsim_get_tile(self, mi / self->num_tile_cells, mj / self->num_tile_cells);
  if (!tile)
    return NULL;
  return tsim_get_tile_cell(self, tile, mi % self->num_tile_cells, mj % self->num_tile_cells);
}


// Initialize simulator.
int tsim_open(tsim_t *self, char *filename)
{
  int i, j;
  char nfilename[1024];
  tsim_frag_t *frag;
  tsim_tile_t *tile;
  
  tsim_opt_init(&self->options);

  // Parse world file
  snprintf(nfilename, sizeof(nfilename), "%s/config", filename);
  if (tsim_opt_configfile(nfilename, &self->options, false, true, true) != 0)
  {
    tsim_opt_print_help();
    return ERROR("unable to parse world file %s", nfilename);
  }

  // Set resolution and tile size
  self->res = self->options.res_arg;
  self->num_tile_cells = self->options.tile_arg;
  
  // TESTING
  // Suck out one fragment
  assert(self->num_frags < sizeof(self->frags) / sizeof(self->frags[0]));
  frag = self->frags + self->num_frags++;

  assert(self->options.color_image_arg);
  snprintf(nfilename, sizeof(nfilename), "%s/%s", filename, self->options.color_image_arg);  
  frag->color_name = strdup(nfilename);

  if (true)
  {
    GisCoordLatLon geo;
    GisCoordUTM utm;
    double ax, ay, bx, by;
    
    geo.longitude = self->options.alon_arg;
    geo.latitude = self->options.alat_arg;
    gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);
    bx = utm.n;
    ay = utm.e;
  
    geo.longitude = self->options.blon_arg;
    geo.latitude = self->options.blat_arg;
    gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);
    ax = utm.n;
    by = utm.e;

    // Set the utm offset 
    if (self->utm_offset.x == 0)
    {
      self->utm_offset.x = ax;
      self->utm_offset.y = ay;
      self->utm_offset.z = 0; // TODO
      MSG("using utm offset %.3f %.3f %.3f", self->utm_offset.x, self->utm_offset.y, self->utm_offset.z);

      // TODO: check sign convention
      self->map_az = -VEHICLE_TIRE_RADIUS;
      self->map_bz = -VEHICLE_TIRE_RADIUS;
    }

    // Convert from UTM to simulation frame
    frag->ax = (float) (ax - self->utm_offset.x);
    frag->ay = (float) (ay - self->utm_offset.y);
    frag->bx = (float) (bx - self->utm_offset.x);
    frag->by = (float) (by - self->utm_offset.y);  
  
    // Update map origin
    if (frag->ax < self->map_ax || self->map_ax == 0)
      self->map_ax = frag->ax;
    if (frag->ay < self->map_ay || self->map_ay == 0)
      self->map_ay = frag->ay;

    // Update map size
    if (frag->bx > self->map_bx || self->map_bx == 0)
      self->map_bx = frag->bx;
    if (frag->by > self->map_by || self->map_by == 0)
      self->map_by = frag->by;
  }

  self->num_map_tiles = (int) ((self->map_bx - self->map_ax) * self->res / self->num_tile_cells);
  self->num_map_tiles = (int) ((self->map_by - self->map_ay) * self->res / self->num_tile_cells);

  MSG("creating map at %.2f %.2f size %.2f %.2f %dx%d",
      self->map_ax, self->map_ay, self->map_bx - self->map_ax, self->map_by - self->map_ay,
      self->num_map_tiles, self->num_map_tiles);

  self->tiles = calloc(self->num_map_tiles * self->num_map_tiles, sizeof(self->tiles[0]));

  for (j = 0; j < self->num_map_tiles; j++)
  {
    for (i = 0; i < self->num_map_tiles; i++)
    {
      tile = tsim_get_tile(self, i, j);
      assert(tile);

      // Set tile bounds
      tile->ai = i * self->num_tile_cells;
      tile->aj = j * self->num_tile_cells;
      tile->bi = i * self->num_tile_cells + self->num_tile_cells;
      tile->bj = j * self->num_tile_cells + self->num_tile_cells;
      tsim_m2w(self, tile->ai, tile->aj, &tile->ax, &tile->ay);
      tsim_m2w(self, tile->bi, tile->bj, &tile->bx, &tile->by);      

      // Load tile to check that it works
      if (tsim_tile_load(self, tile) != 0)
        return -1;
      // TESTING tsim_tile_unload(self, tile);
    }
  }
  
  return 0;
}


// Finalize simulator.
int tsim_close(tsim_t *self)
{
  int k;
  tsim_frag_t *frag;
  
  if (self->tiles)
  {
    free(self->tiles);
    self->tiles = NULL;
  }
  
  for (k = 0; k < self->num_frags; k++)
  {
    frag = self->frags + k;
    free(frag->color_name);
    frag->color_name = NULL;
  }

  tsim_opt_free(&self->options);
  
  return 0;
}


// Load data for the given tile
int tsim_tile_load(tsim_t *self, tsim_tile_t *tile)
{
  int i, j, k;
  jplv_image_t *image;
  tsim_frag_t *frag;
  tsim_cell_t *cell;
  uint8_t *pix;
  float px, py;
  int qi, qj;
  
  // Allocate storage
  if (!tile->cells)
    tile->cells = calloc(self->num_tile_cells * self->num_tile_cells, sizeof(tile->cells[0]));

  for (k = 0; k < self->num_frags; k++)
  {
    frag = self->frags + k;

    // TODO check for overlap before loading image

    image = jplv_image_alloc_file(frag->color_name);
    if (!image)
      return ERROR("unable to load %s : %s", frag->color_name, jplv_error_str());
    
    for (j = 0; j < self->num_tile_cells; j++)
    {
      for (i = 0; i < self->num_tile_cells; i++)
      {
        cell = tsim_get_tile_cell(self, tile, i, j);

        // Compute position in simulation frame
        tsim_m2w(self, tile->ai + i, tile->aj + j, &px, &py);

        // Compute pixel in image
        qj = image->rows - 1 - (int) ((px - frag->ax) / (frag->bx - frag->ax) * image->rows);
        qi = (int) ((py - frag->ay) / (frag->by - frag->ay) * image->cols);

        pix = jplv_image_pixel(image, qi, qj);
        if (!pix)
          continue;

        cell->color[0] = pix[0];
        cell->color[1] = pix[1];
        cell->color[2] = pix[2];
      }
    }

    jplv_image_free(image);
  }
  
  
  return 0;
}


// Discard a tile contents
int tsim_tile_unload(tsim_t *self, tsim_tile_t *tile)
{
  if (tile->cells)
  {
    free(tile->cells);
    tile->cells = NULL;
  }
  return 0;
}

                             
// Generate an image from a simple camera.
int tsim_gen_image(tsim_t *self, double veh_pose[6], float cam2veh[4][4],
                   float cx, float cy, float sx, float sy, int cols, int rows, int channels,
                   int size, uint8_t *data)
{
  int i, j;
  float rx, ry, s;
  float px, py, pz;
  float wx, wy, wz;
  pose3_t pose;
  float m[4][4];
  int mi, mj;
  tsim_cell_t *cell;
  uint8_t *pix;

  assert(size >= cols * rows * channels);
  assert(channels == 1 || channels == 3);

  // Compute vehicle pose in simulation frame
  pose.pos = vec3_set(veh_pose[0], veh_pose[1], veh_pose[2]);
  pose.pos = vec3_sub(pose.pos, self->utm_offset);
  pose.rot = quat_from_rpy(veh_pose[3], veh_pose[4], veh_pose[5]);

  // Construct camera pose in simulation frame
  pose = pose3_mul(pose, pose3_from_mat44f(cam2veh));

  // Construct combined transform from camera to simulation frame.
  pose3_to_mat44f(pose, m);
  
  pix = data;
  
  for (j = 0; j < rows; j++)
  {
    for (i = 0; i < cols; i++)
    {
      // Compute ray coefficients
      rx = (i - cx) / sx;
      ry = (j - cy) / sy;

      wz = 0;
      
      // Compute ray/plane intersection for z = 0.  The plane is in
      // the simulatation frame; the intersection is computed in the
      // camera frame.
      s = m[2][0] * rx + m[2][1] * ry + m[2][2];
      pz = (wz - m[2][3]) / s;
      px = rx * pz;
      py = ry * pz;

      //printf("%d %d %f %f %f %f\n", i, j, s, px, py, pz);

      if (pz > 0)
      {
        //printf("%d %d %.3f %.3f %.3f\n", i, j, px, py, pz);
        
        // Convert to simulation frame
        wx = m[0][0] * px + m[0][1] * py + m[0][2] * pz + m[0][3];
        wy = m[1][0] * px + m[1][1] * py + m[1][2] * pz + m[1][3];

        // Convert to map index
        tsim_w2m(self, wx, wy, &mi, &mj);
      
        // Get the corresponding cell
        cell = tsim_get_map_cell(self, mi, mj);
        
        if (cell)
        {
          // Update the image
          if (channels == 1)
          {
            pix[0] = cell->color[1];
          }
          else if (channels == 3)
          {
            pix[0] = cell->color[0];
            pix[1] = cell->color[1];
            pix[2] = cell->color[2];
          }
        }
      }
      
      pix += channels;
    }
  }
  
  return 0;
}


// Generate a LADAR range scan
int tsim_gen_ranges(tsim_t *self, double veh_pose[6], float sens2veh[4][4],
                    float range_res, float max_range, float ang_res, 
                    int num_ranges, float *ranges)
{
  int i, j;
  float rx, ry, d;
  float px, py, pz;
  float wx, wy, wz;
  int num_steps;
  pose3_t pose;
  float m[4][4];
  int mi, mj;
  tsim_cell_t *cell;

  // Compute vehicle pose in simulation frame
  pose.pos = vec3_set(veh_pose[0], veh_pose[1], veh_pose[2]);
  pose.pos = vec3_sub(pose.pos, self->utm_offset);
  pose.rot = quat_from_rpy(veh_pose[3], veh_pose[4], veh_pose[5]);

  // Construct sensor pose in simulation frame
  pose = pose3_mul(pose, pose3_from_mat44f(sens2veh));

  // Construct combined transform from sensor to simulation frame.
  pose3_to_mat44f(pose, m);

  // Number of steps in the ray-tracing
  num_steps = (int) ceil(max_range / range_res);
  
  for (i = 0; i < num_ranges; i++)
  {
    // Compute ray coefficients
    rx = cos((i - num_ranges/2) * ang_res);
    ry = sin((i - num_ranges/2) * ang_res);

    ranges[i] = max_range;
    
    for (j = 0; j < num_steps; j++)
    {
      d = j * range_res;
      px = rx * d;
      py = ry * d;
      pz = 0;
          
      // Convert to simulation frame
      wx = m[0][0] * px + m[0][1] * py + m[0][2] * pz + m[0][3];
      wy = m[1][0] * px + m[1][1] * py + m[1][2] * pz + m[1][3];
      wz = m[2][0] * px + m[2][1] * py + m[2][2] * pz + m[2][3];      
      
      // Convert to map index
      tsim_w2m(self, wx, wy, &mi, &mj);
            
      // Get the corresponding cell
      cell = tsim_get_map_cell(self, mi, mj);
      if (cell && wz > cell->elev / 256.0)
      {
        ranges[i] = d;
        break;
      }
    }
  }
  
  return 0;
}



#if USE_GL

// Get the simulation boundaries in global coordinates
int tsim_get_bounds(tsim_t *self,
                    double *ax, double *ay, double *az, double *bx, double *by, double *bz)
{
  *ax = self->utm_offset.x + self->map_ax;
  *ay = self->utm_offset.y + self->map_ay;
  *az = self->utm_offset.z + self->map_az;
  *bx = self->utm_offset.x + self->map_bx;
  *by = self->utm_offset.y + self->map_by;
  *bz = self->utm_offset.z + self->map_bz;
  
  return 0;
}


// Switch to the given frame.
void tsim_render_push(tsim_t *self, float m[4][4])

{
  int i, j;
  float t[16];

  // Transpose to column-major order for GL
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);
  
  return;
}


// Revert from the current frame.
void tsim_render_pop(tsim_t *self)
{
  glPopMatrix();
  return;
}


// Render a tile
int tsim_render_tile(tsim_t *self, tsim_tile_t *tile)
{
  if (!tile->cells)
    return 0;

  if (tile->texture == 0)
  {
    glGenTextures(1, &tile->texture);
    assert(tile->texture > 0);
    glBindTexture(GL_TEXTURE_2D, tile->texture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    // This is a bit naughty: use the cell data directly as an RGBA
    // image (the alpha channel will correspond to elevation, and is
    // not used).
    assert(sizeof(tile->cells) == 4);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, self->num_tile_cells, self->num_tile_cells,
                      GL_RGBA, GL_UNSIGNED_BYTE, tile->cells);
  }

  // Dont us the alpha channel: it corresponds to elevation.
  glDisable(GL_BLEND);
  
  // Draw a simple quad representing the tile
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, tile->texture);
  glColor3f(1, 1, 1);
  glBegin(GL_QUADS);
  glTexCoord2f(0, 0); glVertex2f(tile->ax, tile->ay);
  glTexCoord2f(1, 0); glVertex2f(tile->bx, tile->ay);
  glTexCoord2f(1, 1); glVertex2f(tile->bx, tile->by);
  glTexCoord2f(0, 1); glVertex2f(tile->ax, tile->by);
  glEnd();
  glDisable(GL_TEXTURE_2D);

  if (false)
  {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin(GL_QUADS);
    glVertex2f(tile->ax, tile->ay);
    glVertex2f(tile->bx, tile->ay);
    glVertex2f(tile->bx, tile->by);
    glVertex2f(tile->ax, tile->by);
    glEnd();
  }

  return 0;
}


// Render the map
int tsim_render_map(tsim_t *self)
{
  int i, j;
  tsim_tile_t *tile;

  // Shift to map frame
  glPushMatrix();
  glTranslatef(-self->map_ax, -self->map_ay, -self->map_az);
  
  for (j = 0; j < self->num_map_tiles; j++)
  {
    for (i = 0; i < self->num_map_tiles; i++)
    {
      tile = tsim_get_tile(self, i, j);
      assert(tile);
      tsim_render_tile(self, tile);
    }
  }

  glPopMatrix();
  
  return 0;
}


// Render vehicle at the given pose.
int tsim_render_vehicle(tsim_t *self, double veh_pose[6])
{
  pose3_t pose;
  float m[4][4];
  
  // Construct transform matrix
  pose.pos = vec3_set(veh_pose[0], veh_pose[1], veh_pose[2]);
  pose.pos = vec3_sub(pose.pos, self->utm_offset);
  pose.rot = quat_from_rpy(veh_pose[3], veh_pose[4], veh_pose[5]);  
  pose3_to_mat44f(pose, m);

  // Push the vehicle frame
  tsim_render_push(self, m);
  
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Pop the vehicle frame
  tsim_render_pop(self);
  
  return 0;
}


// Render a laser scan.
int tsim_render_ranges(tsim_t *self, double veh_pose[6], float sens2veh[4][4],
                       float range_res, float max_range, float ang_res, int num_ranges)
{
  int i;
  float px, py;
  float ranges[num_ranges];
  pose3_t pose;
  float m[4][4];
  
  // Generate range data
  tsim_gen_ranges(self, veh_pose, sens2veh, range_res, max_range, ang_res, num_ranges, ranges);
  
  // Construct transform matrix for sensor
  pose.pos = vec3_set(veh_pose[0], veh_pose[1], veh_pose[2]);
  pose.pos = vec3_sub(pose.pos, self->utm_offset);
  pose.rot = quat_from_rpy(veh_pose[3], veh_pose[4], veh_pose[5]);
  pose = pose3_mul(pose, pose3_from_mat44f(sens2veh));
  pose3_to_mat44f(pose, m);

  // Push the sensor frame
  tsim_render_push(self, m);

  // TESTING
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0.2, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0.2, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 0.2);
  glEnd();

  glPointSize(2.0);
  glColor3f(0, 0, 1);
  glBegin(GL_POINTS);
  for (i = 0; i < num_ranges; i++)
  {
    px = ranges[i] * cos((i - num_ranges/2) * ang_res);
    py = ranges[i] * sin((i - num_ranges/2) * ang_res);
    glVertex2f(px, py);
  }
  glEnd();
  
  tsim_render_pop(self);
  
  return 0;
}


#endif



#if TSIM_TEST

// Test wait and cache read
int main(int argc, const char *argv[])
{
  tsim_t *Tsim;
  int i, id, len;
  int sensor_id;
  int blob_type;
  int blob_size, *blob;
  
  blob_size = 700000;
  blob = malloc(blob_size);

  Tsim = tsim_alloc();
  if (tsim_connect(Tsim, "4803", 1234, 1) < 0)
    return -1;

  sensor_id = DUMMY_SENSOR_ID;
  blob_type = DUMMY_BLOB_TYPE;
  
  if (tsim_join(Tsim, sensor_id, blob_type, blob_size, 5) < 0)
    return -1;
    
  MSG("waiting");
      
  for (i = 0; i < 2000; i++)
  {
    if (tsim_wait(Tsim) != 0)
      break;
    if (tsim_peek(Tsim, sensor_id, blob_type, &id, &len) != 0)
      break;
    assert(len <= blob_size);
    if (tsim_read(Tsim, sensor_id, blob_type, id, len, blob) != 0)
      break;
    MSG("recieved %d %d", id, blob[0]);
  }

  if (tsim_leave(Tsim, sensor_id, blob_type) < 0)
    return -1;

  tsim_disconnect(Tsim);
  tsim_free(Tsim);
  
  free(blob);

  MSG("exiting cleanly");
    
  return 0;
}

#endif
