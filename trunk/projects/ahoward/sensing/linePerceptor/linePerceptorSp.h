/* Object ID's (offset into table) */

/* Allocate space for buffer storage */
char buffer[48];

DD_IDENT display[] = {
{1, 1, (void *)("LinePerceptor Rev: 11975  Author: ahoward"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 1, (void *)("Spread:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 1, (void *)("Key   :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 1, (void *)("Module:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 1, (void *)("Frame :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 1, (void *)("Rate  :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 15, (void *)("Hz"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 24, (void *)("ms"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 1, (void *)("Latency"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 1, (void *)("Fakes :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 1, (void *)("["), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 8, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 16, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 23, (void *)("]"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 9, (void *)(spSpreadDaemon), dd_string, "%s", (char *)(sizeof(spSpreadDaemon)), 0, dd_nilcbk, (long)(long) 0, 0, 0, String, "spSpreadDaemon", -1},
{4, 9, (void *)(&(spSpreadKey)), dd_short, "%d", buffer+0, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "spSpreadKey", -1},
{5, 9, (void *)(spModuleName), dd_string, "%s", (char *)(sizeof(spModuleName)), 0, dd_nilcbk, (long)(long) 0, 0, 0, String, "spModuleName", -1},
{7, 9, (void *)(&(spFrameId)), dd_short, "%d", buffer+8, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "spFrameId", -1},
{9, 9, (void *)(&(spLatency)), dd_double, "%.3f", buffer+16, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "spLatency", -1},
{8, 9, (void *)(&(spRunFreq)), dd_double, "%.1f", buffer+24, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "spRunFreq", -1},
{8, 18, (void *)(&(spRunPeriod)), dd_double, "%.3f", buffer+32, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "spRunPeriod", -1},
{11, 9, (void *)(&(spNumFakes)), dd_short, "%d", buffer+40, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "spNumFakes", -1},
{13, 3, (void *)("QUIT"), dd_label, "NULL", (char *) NULL, 1, spUserQuit, (long)(long) 0, 0, 0, Button, "", -1},
{13, 10, (void *)("PAUSE"), dd_label, "NULL", (char *) NULL, 1, spUserPause, (long)(long) 0, 0, 0, Button, "", -1},
{13, 18, (void *)("FAKE"), dd_label, "NULL", (char *) NULL, 1, spUserFake, (long)(long) 0, 0, 0, Button, "", -1},
DD_End};
