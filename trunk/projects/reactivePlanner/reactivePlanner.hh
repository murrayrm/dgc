#include <math.h>
#include <sys/time.h>
#include "frames/coords.hh"
#include "trajMap.hh"
#include "rddf.hh"
#include "CMap.hh"
#include "CCorridorPainter.hh"
#include "AliceConstants.h"
using namespace std;

#ifndef _REACTIVEPLANNER_HH_
#define _REACTIVEPLANNER_HH_

/*All arcs will be produced with roughly this spacing.  Should be about 1/2 map
  resolution.*/
#define INTERNAL_TRAJ_SPACING 0.5 //  m
/*Specifies the amount of memory allocated for each arc.  In rare cases the
  point spacing of an arc may be increased so it fits under this limit.*/
#define POINTS_PER_ARC 256
/*Specifies the number of points per phase line.  Complexity is quadratic in
  this factor.*/
#define POINTS_WIDE 7
//Specifies the number of phase lines.  Complexity is linear in this factor.
#define LINES_LONG 6
/*Specifies whether to connect arcs only to the closest phase line or also to
  phase lines farther away.  Using larger depth should produce smoother trajs,
  especially around turns.  Complexity is linear in this factor.
  0 < LINES_DEEP <= LINES_LONG*/
#define LINES_DEEP 3
/*Specifies the distance between phase lines.
  LINES_LONG * DISTANCE_BETWEEN_LINES ~= the length of the output traj, and this
  needs to be shorter than 1/2 the map edge size.*/
#define DISTANCE_BETWEEN_LINES 14.  //  m/s
/*All points in the output traj will have at least this speed regardless of the
  map velocity limits.*/
#define MIN_SPEED 1.
/*As a temporary hack to deal with wide corridors the corRad is artificially
  capped at this value.*/
#define MAX_WIDTH 15. //  m
/*Map cells with velocity smaller than this value are considered to be
  intraversible.  This needs to be better coordinated with fusionMapper.*/
#define CUTOFF 0.1 //  m/s

//Temporary estimates of Alice's kinematic limits.
#define MAX_ACCEL 2.      //  m/s^2
#define MAX_DECEL (-3.)   //  m/s^2
#define MAX_LAT_ACCEL 3.  //  m/s^2
#define TURNING_RADIUS 8. //  m

/*Index macros are used to simplify the accessing of complicated
  multidimensional arrays.*/
#define INDEX2(i, j) (POINTS_WIDE * (i) + (j) + 1)
#define INDEX2a(i, j) (POINTS_WIDE * (i) + (j))
#define INDEX3(i, j, k) (4 * POINTS_PER_ARC * (i) + 4 * (j) + (k))
#define INDEX4(i, j, f, k) (POINTS_WIDE * POINTS_WIDE * (f) * (2 *\
    LINES_LONG - (f) - 1) / 2 + POINTS_WIDE * POINTS_WIDE * (i) + POINTS_WIDE *\
    (j) + (k) + LINES_DEEP * POINTS_WIDE)

#define NUM_ARCS (LINES_DEEP * POINTS_WIDE * (2 - POINTS_WIDE * (LINES_DEEP -\
        2 * LINES_LONG + 1)) / 2)

class reactivePlanner
{
  private:
        //Number of points in the first circular segment.
    int m_m[NUM_ARCS],
        //Number of points in the second circular segment.
        m_n[NUM_ARCS],
        //Number of points in the arc.  m_p = m_m + m_n
        m_p[NUM_ARCS],
        //Stores the indexes of the arcs picked to define the output traj.
        b[LINES_LONG],
        m_layer,  //CMap layer
        m_g;  //Counter
    //Stores all of the points that will be connected by arcs.  Includes origin.
    NEcoord points[LINES_LONG * POINTS_WIDE + 1];
    double m_speed0,  //Initial speed.
        theta[LINES_LONG + 1],  //Initial yaw.
        *m_arcs,  //Points to array of arcs.
        //Radius of curvature of the circular segments that make up the arc.
        m_r[NUM_ARCS],
        //The exact point spacing of the arc.
        m_d[NUM_ARCS];
    CTraj *traj;  //Points to output order 3 traj
    RDDF *m_rddf;
    CMap *m_map;

    /*! Produces lines of points perpendicular to the RDDF track line.
        Also gives the heading of points in each line*/
    void pickPoints();

    /*! Iterates over the points, calling the makeArc function to create arcs
        connecting the points.*/
    void makeBundles();

    /*! Returns a pointer to a traj made up of 2 circular arcs that smoothly
        connect two unit vectors.  Also, gives the radius of the arcs and the
        arc length between points.*/
    void makeArc(int index, NEcoord p0, double theta0, NEcoord p1,
        double theta1);

    /*! Returns a pointer to a traj made up of 2 circular arcs that smoothly
        connect two unit vectors.  Also, gives the radius of the arcs and the
        arc length between points.*/
    void makeArc(int index, double x0, double y0, double theta0, double x1,
        double y1, double theta1);

    /*! This is the function which selects the arcs which will be concatenated
        to produce the output traj.  The function selects the combination of
        arcs whose costs sum to the smallest amount.  The initial point is
        assigned a cost of zero.  For all other points the function evaluates
        the cost of each arc coming in to the point plus the cost of the point
        from which the arc came.  The index of the incoming arc with lowest cost
        is stored and the cost is assigned to the point.  This process is
        repeated until costs have been assigned to each point.  Then the end
        point with the lowest cost is selected to be the endpoint of the output
        traj.  Then, working backwards, the arc which led to the end point is
        selected, and then the arc which led to the origin of that arc is
        selected, continuing untill the process reaches the initial point.*/
    void pickArcs();

    /*! This function concatenates the selected arcs into a single traj.
        Additionally, it applies a speed profile to the traj which meets the
        constraints of the velocity may and vehicle kinematics.  To create the
        speed profile, the function iterates foreword through the traj
        evaluating each point in the CMap.  The speed profile is allowed to
        increase at the MAX_ACCEL and decrease instataniously, so that the speed
        for each point is always less than the speed limit for the CMap cell
        that point is in.  Then the process is repeated going backwards through
        the traj allowing the speed profile to increase at MAX_DECEL and
        decrease instataniously.  At each point the minimum of the two speed
        profiles is taken, and then the minimum of that speed and a rollover
        speed becomes the final speed for that traj point.*/
    void makeTrajFromArcs();
    
    /*! Evaluates traj through a CMap layer and returns 1 if any point in the
        traj lies in a cell with goodness below the cutoff level.  Currently is
        only defined for a layer of doubles.*/
    bool isTrajObstructed(int index, CMap * map, int layer);
  
    /*! Evaluates traj through a CMap layer and returns the time it would take
        to complete the traj if at every point in the traj the vehicle traveled
        at the speed specified by the map cell that point falls within.  Only
        defined for a layer of doubles.*/
    double evalCost(int index, CMap * map, int layer);
  public:
    reactivePlanner();

    ~reactivePlanner();
    
  /*! This is the main function of the reactivePlanner class.  It is called by
      the reactiveModule or any other module which wants to create a
      reactivePlanner traj.*/
    CTraj *plan(RDDF *rddf, CMap *map, int layer,
        NEcoord &initialPoint, double initialTheta, double initialSpeed);
};

#endif
