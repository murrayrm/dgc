#include <iostream>
#include <cassert>
#include "Environment.hh"
using namespace std;

  ///////////
  // Setup //
  ///////////

void InteractiveEnvironment::setup() {

  cout <<endl<< "Interactive Environment setup. " << endl;

  int input = 0;
 
  cout<<endl<<"** The following would come from astate. **"<<endl;
  
  // set our state
  double n,e,vn,ve;
  cout<<"Setting Alice's state. "<<endl;
  cout<<"  Position (northing easting): ";
  cin>>n>>e;
  cout<<"  Velocity (northing easting): ";
  cin>>vn>>ve;

  // Alice's vehicle ID is always 0
  ourState = new State(0, NEcoord(n,e), NEcoord(vn,ve));
  ourState->setMode(0); 

  // load the RNDF
  loadRNDF();

  // get the next goal
  setGoal();

  // get lane boundaries
  setLaneBoundaries();

  // determine which lane we're in
  calcLane(*ourState);

  // now that we have our state, figure out right/left lanes
  calcRightLanes();

  cout<<endl<<"** The following would come from sensing. **"<<endl;
  cout<<"Number of vehicles: ";
  cin>>input;

  for(int i=0; i<input; i++) {
    cout<<endl<<"Adding "<<i+1;
    if (i+1==1) cout<<"st ";
    else if (i+1==2) cout<<"nd ";
    else if (i+1==3) cout<<"rd ";
    else cout<<"th ";
    cout<<"vehicle."<<endl;
    addVehicle();
  }
  
  ourState->setMode(1);
  cout <<endl<< endl<<"Set to normal driving mode." <<endl;
  
  cout <<endl<< "Environment setup complete."<< endl;
}


void InteractiveEnvironment::loadRNDF()
{

  int input = 0;

  rndf.loadFile(rndf_filename);
  cout<<"Loaded "<<rndf_filename<<endl<<endl;

  cout << "Segment ID: ";
  cin>> input;
  
  assert(input>=0);
  currSegment = rndf.getSegment(input);

  // initialize the laneBoundaries array
  laneBoundaries = new vector<NEcoord>[currSegment->getNumOfLanes()];
}


void InteractiveEnvironment::setLaneBoundaries()
{
  int numLanes = currSegment->getNumOfLanes();
  int numPoints = 0;
  double n,e;

  cout<<endl<<"Current segment has "<<numLanes<<" lanes."<<endl;

  for (int i=0; i<numLanes; i++) {
    cout<<"Lane "<<i+1<<"."<<endl;;
    cout<<"  Number of points: ";
    cin>>numPoints;
    
    for (int j=0; j<numPoints; j++) {
      cout<<"  Point "<<j+1<<" (n e): ";
      cin>>n>>e;
      laneBoundaries[i].push_back(NEcoord(n,e));
    }
    
  }

  cout<<"Lane boundaries stored."<<endl;

}

void InteractiveEnvironment::update()
{

  // update the lanes
  setLaneBoundaries();

  // update our state
  updateState();

  // check the goal status
  if (atGoal())
    setGoal();

  // update other vehicles' states
  updateVehicles();

}


void InteractiveEnvironment::updateState() 
{
  double n,e,vn,ve;
  cout<<"Updating Alice's state. "<<endl;
  cout<<"  Position (northing easting): ";
  cin>>n>>e;
  cout<<"  Velocity (northing easting): ";
  cin>>vn>>ve;

  ourState->setPos(n,e);
  ourState->setVel(vn,ve);

  calcLane(*ourState);  
}

void InteractiveEnvironment::updateVehicles()
{

  char input;

  cout<<endl<<"Updating vehicles. ";

  vector<State>::iterator it = vehicles.begin();
  while (it != vehicles.end()) {
    cout<<"Vehicle "<< it->getID() <<".";
    
    while (true) {
      cout<<"(update, delete, skip) ";
      cin>>input;
      if (input=='d') {
	cout<<"  Deleting vehicle "<<it->getID()<<"."<<endl;
	vehicles.erase(it); /* it is incremented automatically here */
	break;
      }
      else if (input=='u') {
	double n, e, vn, ve;
	cout<<"  Vehicle position (northing easting): ";
	cin>>n>>e;
	cout<<"  Vehicle velocity (northing easting): ";
	cin>>vn>>ve;

	it->setPos(n,e);
	it->setVel(vn,ve);
	calcLane(*it);

	it++;
	break;
      }
      else if (input=='s') {
	cout<<"  Skipping vehicle "<<it->getID()<<"."<<endl;
	it++;
	break;
      }

      // if we get here, user entered something weird, start over
    }
  } 
    // add new vehicles
    int num;
    cout<<"Number of new vehicles: ";
    cin>>num;
    for (int i=0; i<num; i++) {
      cout<<endl<<"Adding "<<i+1;
      if (i+1==1) cout<<"st ";
      else if (i+1==2) cout<<"nd ";
      else if (i+1==3) cout<<"rd ";
      else cout<<"th ";
      cout<<"vehicle."<<endl;
      addVehicle();
    }
}


void InteractiveEnvironment::addVehicle()
{
  int id;
  double n, e, vn, ve;

  cout<< "  Vehicle ID (must be unique!): ";
  cin>>id;
  // make sure ID doesn't belong to Alice:
  while (id ==0) {
    cout<<"ID 0 is reserved for Alice!"<<endl;
    cout<<"Please choose another ID: ";
    cin>>id;
  }
  // make sure ID isn't already taken:
  vector<State>::iterator it = vehicles.begin();
  while (it != vehicles.end()) {
    if (it->getID() != id) 
      it++;
    else {
      cout<<"Vehicle "<<id<<" already exists!"<<endl;
      cout<<"Please choose another ID: ";
      cin>>id;
      // start at beginning again
      it = vehicles.begin();
    }
    
  }

  cout<< "  Position (northing easting): ";
  cin>>n>>e;
  cout<< "  Velocity (northing easting): ";
  cin>>vn>>ve;

  // store the state
  State temp(id, NEcoord(n,e), NEcoord(vn,ve));

  // determine which lane the vehicle is in
  if (calcLane(temp)) {

  vehicles.push_back(temp);

  cout<<"  Vehicle inserted ";
  if (vehicles.back().stopped())
    cout << "(vehicle is stopped)";
  }
  else {
    cout<<"Vehicle "<<id<<" not added.";
  }

}


void InteractiveEnvironment::setGoal()
{
  int a,b,c;
  cout<<"Next goal (segment lane ID): ";
  cin>>a>>b>>c;
  currGoal = rndf.getWaypoint(a,b,c);
  cout<<"Stored waypoint "<<a<<"."<<b<<"."<<c<<endl;
}
