#include <iostream>
#include <cassert>
#include "Environment.hh"
using namespace std;

  ///////////
  // Setup //
  ///////////

void AliceEnvironment::setup()
{

#warning "AliceEnvironment::setup(): using incomplete module!"

  cout <<endl<< "Alice Environment setup. " << endl;

  // update astate
  UpdateState();

  // access members from VehicleState
  NEcoord pos = m_state.ne_coord();
  double vn = m_state.Vel_N; 
  double ve = m_state.Vel_E;

  // Alice's vehicle ID is always 0
  ourState = new State(0, pos, NEcoord(vn,ve));
  ourState->setMode(0); 

  // load the RNDF
  loadRNDF();

  // get the next goal
  setGoal();

  // get lane boundaries
  setLaneBoundaries();

  // determine which lane we're in
  calcLane(*ourState);

  // now that we have our state, figure out right/left lanes
  calcRightLanes();

  // TODO: get vehicles from sensing
  int numVehicles;

  for(int i=0; i<numVehicles; i++) {
    cout<<endl<<"Adding "<<i+1;
    if (i+1==1) cout<<"st ";
    else if (i+1==2) cout<<"nd ";
    else if (i+1==3) cout<<"rd ";
    else cout<<"th ";
    cout<<"vehicle."<<endl;
    addVehicle();
  }
  
  ourState->setMode(1);
  cout <<endl<< endl<<"Set to normal driving mode." <<endl;
  
  cout <<endl<< "Environment setup complete."<< endl;
}


void AliceEnvironment::loadRNDF()
{

  rndf.loadFile(rndf_filename);
  cout<<"Loaded "<<rndf_filename<<endl<<endl;


  int segmentID;
  // TODO: get current segment from mission or route planner
  currSegment = rndf.getSegment(segmentID);

  // initialize the laneBoundaries array
  laneBoundaries = new vector<NEcoord>[currSegment->getNumOfLanes()];

}

void AliceEnvironment::setLaneBoundaries()
{
  // TODO: load the lane boundaries from road following module
}

void AliceEnvironment::update()
{

  // update the lane boundaries
  setLaneBoundaries();

  // update our state
  updateState();

  // check the goal status
  if (atGoal())
    setGoal();

  // update the other vehicles
  updateVehicles();

}


void AliceEnvironment::updateState() {
    
  UpdateState();
  
  // access members from VehicleState
  ourState->setPos(m_state.ne_coord());
  ourState->setVel(m_state.Vel_N, m_state.Vel_E);

  calcLane(*ourState);

}


void AliceEnvironment::updateVehicles()
{

#warning "AliceEnvironment::updateVehicles(): using incomplete module!"

  char input;

  cout<<endl<<"Updating vehicles. ";

  vector<State>::iterator it = vehicles.begin();
  while (it != vehicles.end()) {
    cout<<"Vehicle "<< it->getID() <<".";
    
    while (true) {
  
      // TODO: ask sensing what to do with this vehicle
      // update, delete, skip

      if (input=='d') {
	cout<<"  Deleting vehicle "<<it->getID()<<"."<<endl;
	vehicles.erase(it); /* it is incremented automatically here */
	break;
      }
      else if (input=='u') {

	// TODO: get new pos and vel from sensing

	//it->setPos(n,e);
	//it->setVel(vn,ve);
	calcLane(*it);

	it++;
	break;
      }
      else if (input=='s') {
	cout<<"  Skipping vehicle "<<it->getID()<<"."<<endl;
	it++;
	break;
      }

      // if we get here, user entered something weird, start over
    }
  } 
    // add new vehicles
    int numNewVehicles;
    // TODO: get number of new vehicles from sensing
    for (int i=0; i<numNewVehicles; i++) {
      cout<<endl<<"Adding "<<i+1;
      if (i+1==1) cout<<"st ";
      else if (i+1==2) cout<<"nd ";
      else if (i+1==3) cout<<"rd ";
      else cout<<"th ";
      cout<<"vehicle."<<endl;
      addVehicle();
    }

}

void AliceEnvironment::addVehicle()
{
  // TODO: get vehicles from sensing
}

void AliceEnvironment::setGoal()
{
  // TODO: get goal from route planner
}
