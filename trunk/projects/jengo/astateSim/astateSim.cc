#include "astateSim.hh"
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

CStateSimClient::CStateSimClient(int skynetKey) : 
  CSkynetContainer(SNstateSim, skynetKey)
{

  // get the separation distance
  cout<<endl<<"To avoid outputting sparse waypoints, the simulator will interpolate";
  cout<<endl<<"between waypoints given in the file. The separation distance ";
  cout<<endl<<"indicates how dense the coordinates on the path will be after";
  cout<<endl<<"interpolation. (Note: this can only increase the number of waypoints)";
  cout<<endl<<"Separation distance (meters): ";
  cin>>separation_dist;
  cout<<endl;

}


void CStateSimClient::loadSimFile(const char* filename)
{

  ///////////////////////
  // Load Raw GPS Data //
  ///////////////////////

  ifstream coord_file(filename);

  GisCoordLatLon waypoint_LL;
  GisCoordUTM waypoint_UTM;
  NEcoord waypoint;
  string line;

  cout<<setprecision(9);

  if (coord_file.is_open()) {
    cout<<"Using file: "<<filename<<endl;

    // check if it's a RDDF
    getline(coord_file,line,DELIMITER);
    if (strchr(line.c_str(),',')) {
      loadRDDF(filename);
      return;
    }
    
    cout<<"Loading waypoints file..."<<endl;

    // load first line
    if (! line.empty()) {
      waypoint_LL.latitude = atof(line.c_str());
      getline(coord_file,line);
      waypoint_LL.longitude = atof(line.c_str());
      cout<<waypoint_LL.latitude<<" "<<waypoint_LL.longitude<<" -> ";
      
      gis_coord_latlon_to_utm(&waypoint_LL,
			      &waypoint_UTM,
			      GIS_GEODETIC_MODEL_WGS_84);
      waypoint.N = waypoint_UTM.n;
      waypoint.E = waypoint_UTM.e;
      waypoint.display();
      
      waypoints.push_back(waypoint);
    }
    
    // load the rest of the file
    while (!coord_file.eof()) {  	

      getline(coord_file,line,DELIMITER);
      if (line.empty()) continue;
      waypoint_LL.latitude = atof(line.c_str());
      getline(coord_file,line);
      waypoint_LL.longitude = atof(line.c_str());
      
      gis_coord_latlon_to_utm(&waypoint_LL,
			      &waypoint_UTM,
			      GIS_GEODETIC_MODEL_WGS_84);
      waypoint.N = waypoint_UTM.n;
      waypoint.E = waypoint_UTM.e;
      waypoint.display();
      
      waypoints.push_back(waypoint);
    }

    coord_file.close();
  }
  
  else {
    cout<<"Unable to open file."<<endl;
    exit(0);
  }

  ///////////////////////////////////
  // Calculate intermediate points //
  ///////////////////////////////////
  
  calcCoords();
  
}

void CStateSimClient::loadRDDF(const char* filename)
{

  cout<<"RDDF format detected. Loading waypoints file..."<<endl;

  ifstream coord_file(filename);

  GisCoordLatLon waypoint_LL;
  GisCoordUTM waypoint_UTM;
  NEcoord waypoint;
  string line;

  cout<<setprecision(15);

  if (coord_file.is_open()) {

    while (!coord_file.eof()) {  	

      // get the waypoint number
      getline(coord_file,line,RDDF_DELIMITER);
      if (line.empty()) continue;

      // get the lat
      getline(coord_file,line,RDDF_DELIMITER);
      waypoint_LL.latitude = atof(line.c_str());

      // get the lon
      getline(coord_file,line,RDDF_DELIMITER);
      waypoint_LL.longitude = atof(line.c_str());

      // get the rest of it
      getline(coord_file,line);
      
      cout<<waypoint_LL.latitude<<" "<<waypoint_LL.longitude<<" -> ";

      gis_coord_latlon_to_utm(&waypoint_LL,
			      &waypoint_UTM,
			      GIS_GEODETIC_MODEL_WGS_84);
      waypoint.N = waypoint_UTM.n;
      waypoint.E = waypoint_UTM.e;
      waypoint.display();
      
      waypoints.push_back(waypoint);
    }
    
    coord_file.close();
  }

  else {
    cout<<"Error: Unable to load file."<<endl;
    exit(0);
  }

  //////////////////////////////////////
  // Calculate intermediate waypoints //
  //////////////////////////////////////
  
  calcCoords();
}

void CStateSimClient::calcCoords()
{

  cout<<endl<<"Calculating intermediate waypoints..."<<endl;

  vector<NEcoord>::iterator it = waypoints.begin();

  NEcoord currPoint = *it;
  it++;
  NEcoord nextPoint = *it;

  while (true) {

    if (currPoint.distanceTo(nextPoint) <= separation_dist) {
      
      coords.push_back(currPoint);
      currPoint.display();
      
      if (it != waypoints.end()) {
	currPoint = nextPoint;
	if (nextPoint.N == it->N &&
	    nextPoint.E == it->E) {
	  it++;
	  nextPoint = *it;
	}
	else
	  nextPoint = *it;
      }

      else {
	coords.push_back(nextPoint);
	nextPoint.display();
	break;
      }
    }
    
    else {
      nextPoint.N = 0.5 * (currPoint.N + nextPoint.N);
      nextPoint.E = 0.5 * (currPoint.E + nextPoint.E);
    }
  }

  cout<<coords.size()<<" waypoints total."<<endl<<endl;

  //////////////////////////////
  // Initialize the iterator! //
  //////////////////////////////

  currCoord = coords.begin();
  m_state_sim.Northing = currCoord->N;
  m_state_sim.Easting = currCoord->E;
  
}

bool CStateSimClient::UpdateStateSim()
{

  if (currCoord == coords.end())
    return false;

  // increment the coord
  m_state_sim.Northing = currCoord->N;
  m_state_sim.Easting = currCoord->E;
  currCoord++;

  return true;

}
