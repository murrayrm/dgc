
#include <iostream>
#include <math.h>
#include <assert.h>

using namespace std;

#include "cv.h"
#include "highgui.h"

#include "mcv.hh"

//for gnuplot interfacing
#include "gnuplot_i.h"


//some helper functions for debugging

//print the matrix passed to it
void SHOW_MAT(const CvMat *pmat, char str[])
{  
    cout << str << "\n";
    for(int i=0; i<pmat->rows; i++) 
    {
        for(int j=0; j<pmat->cols; j++) 
           cout << cvGetReal2D(pmat, i, j)  << " ";
        cout << "\n"; 
    }
}

void SHOT_MAT_TYPE(const CvMat *pmat)
{
    cout << CV_MAT_TYPE(pmat->type) << "\n";
}

void SHOW_IMAGE(const CvMat *pmat, char str[], int wait)
{
    //cout << "channels:" << CV_MAT_CN(pmat->type) << "\n";
    //scale it
    //CvMat *mat = cvCreateMat(pmat->height, pmat->width, pmat->type);
    //cvCopy(pmat, mat);
    CvMat *mat = cvCloneMat(pmat);
    assert(mat);
    mcvScaleMat(mat, mat);
    //show it
    //cout << "in\n";
    cvNamedWindow(str, 1); 
    cvShowImage(str, mat); 
    cvWaitKey(wait);
    //cvDestroyWindow(str);
    //clear
    cvReleaseMat(&mat);     
    //cout << "out\n";
}

void SHOW_IMAGE(const IplImage *pmat, char str[])
{
    //cout << "channels:" << CV_MAT_CN(pmat->type) << "\n";
    //scale it
    //CvMat *mat = cvCreateMat(pmat->height, pmat->width, pmat->type);
    //cvCopy(pmat, mat);
    //CvMat *mat = cvCloneMat(pmat);
    //assert(mat);
    //    mcvScaleMat(mat, mat);
    //show it
    //cout << "in\n";
    cvNamedWindow(str, 1); 
    cvShowImage(str, pmat); 
    cvWaitKey(0);
    //cvDestroyWindow(str);
    //clear
    //cvReleaseMat(&mat);     
    //cout << "out\n";
}

void SHOW_POINT(const FLOAT_POINT2D pt, char str[])
{
    
    cout << str << "(" << pt.x << "," << pt.y << ")\n";   
}



/**
 * This function scales the input image to have values 0->1
 * 
 * \param inImage the input image
 * \param outImage hte output iamge
 */
void mcvScaleMat(const CvMat *inMat, CvMat *outMat)
{
    //if (CV_MAT_DEPTH(inMat->type)
    //get the min and subtract it
    double min;
    cvMinMaxLoc(inMat, &min, 0, 0, 0, 0);
    cvSubS(inMat, cvRealScalar(min), outMat);
    
    //get max and divide by it
    double max;
    cvMinMaxLoc(outMat, 0, &max, 0, 0, 0);
    cvConvertScale(outMat, outMat, 1/max);
}







/**
 * This function plots a 1-D matrix
 * 
 * \param handle the handle to the gnuplot object to add to, or NULL to create
 * a new one
 * \param mat the input matrix
 * \param title the plot title
 * \return the passed handle or the created one
 *
 */
gnuplot_ctrl* mcvPlotMat1D(gnuplot_ctrl* handle, const CvMat * mat, char *title)
{
    //check its dimensions
    if( mat->rows != 1  &&  mat->cols != 1)
    {
        ERROR("Matrix not 1D\n");
	return handle;
    }

    //convert it to a double array
    int matLen = mat->rows==1 ?  mat->cols : mat->rows;
    double *x = new double[matLen];
    for (int i=0; i<matLen; i++)
	x[i] = (double) cvGetReal1D(mat, i);

    //now plot it
    //gnuplot_plot_once(title, (char*)0, (char*)0, (char*)0, x, (double*)0, matLen);      
    if (handle==0)
	handle = gnuplot_init();
    gnuplot_plot_x(handle, x, matLen, title);
    //sleep(2) ;
    //getchar();
    //gnuplot_close(h) ;

    //clean
    delete[] x;

    //return
    return handle;
}


/**
 * This function plots a 2-D matrix
 * 
 * \param handle the handle to the gnuplot object to add to, or NULL to create
 * a new one
 * \param matx the x-axis matrix
 * \param maty the y-axis matrix
 * \param title the plot title
 *
 * \return the passed handle or the created one
 *
 */
gnuplot_ctrl* mcvPlotMat2D(gnuplot_ctrl* handle, const CvMat * matx, 
			   const CvMat* maty, char *title)
{
    //check its dimensions
    if( (matx->rows != 1  &&  matx->cols != 1) || 
	(maty->rows != 1  &&  maty->cols != 1) )
    {
        ERROR("Matrix not 1D\n");
	return handle;
    }

    //check dimensionts
    int matLen = matx->rows==1 ?  matx->cols : matx->rows;
    int matLen2 = maty->rows==1 ? maty->cols : maty->rows;
    if(matLen != matLen2)
    {
        ERROR("Matrix not 1D\n");
	return handle;
    }
	

    //convert it to a double array
    double *x = new double[matLen];
    double *y = new double[matLen];
    for (int i=0; i<matLen; i++)
    {
	x[i] = (double) cvGetReal1D(matx, i);
	y[i] = (double) cvGetReal1D(maty, i);
    }

    //now plot it
    if (handle==0)
	handle = gnuplot_init();
    gnuplot_plot_xy(handle, x, y, matLen, title);
    //sleep(2) ;
    //getchar();
    //gnuplot_close(h) ;

    //clean
    delete[] x;
    delete[] y;

    //return
    return handle;    
}


/**
 * This function initializes the plots
 * 
 * \return the created handle
 *
 */
void* mcvPlotInit()
{
    return (void*) gnuplot_init();
}

/**
 * This function finishes the plots
 * 
 * \return the created handle
 *
 */
void mcvPlotFinish(void *handle)
{
    //delete the handle
    gnuplot_close((gnuplot_ctrl*)handle);
}

/**
 * This function creates a double matrix from an input vector
 * 
 * \param vec the input vector
 * \param mat the output matrix (column vector)
 *
 */
template <class T>
CvMat* mcvVector2Mat(const vector<T> &vec)
{
    CvMat *mat = 0; 

    if (vec.size()>0)
    {
	//create the matrix
	mat = cvCreateMat(vec.size(), 1, CV_64FC1);
	//loop and get values
	for (int i=0; i<(int)vec.size(); i++)
	    CV_MAT_ELEM(*mat, double, i, 0) =(double) vec[i];
    }

    //return
    return mat;
}

template CvMat* mcvVector2Mat(const vector<double> &vec);
template CvMat* mcvVector2Mat(const vector<int> &vec);
