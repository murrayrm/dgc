/** This is laura's code for learning
 * about how to talk to ladars over
 * serial. 
 * 
 * hodgepodge of functions/cmds from 
 * laserdevice.cc and sick_driver.c
 *
 * I'm not at all sure if my terminology
 * is correct...oh well
 **/

#include "debugLadar.h"

const unsigned char startString2[] = { 0x02, 0x80};

const unsigned char replyB2[] = { 0xb2};
const unsigned char replyB1[] = { 0xb1};
const unsigned char replyA0[] = { 0xa0};


// Will prompt for an integer 0-5 
// of which USB port to use. 
// Number may change if computer is power-cycled.
// To change ports, need to rerun program.

int main() {

  Setup();
  cout<<"finished setup"<<endl;
  
  int function = 0;
  while (function != 6) {
    cout << "\n \n \n Choose 1 of 4 functions (enter the number) \n"
	  << "1. test -- print error messages \n"
	  << "2. status -- print status of unit \n"
	  << "3. rate -- change the port rate of the computer and the ladar\n"
          << "4. scan -- print distances from a scan \n"
	  << "5. setup rate -- change the rate of the computer only \n"
	  << "6. quit" << endl;
 
    cin >> function;
    cout << endl;
    
    switch (function) {
      case 1: RequestTest();
	    continue;
      case 2: RequestStatus();
	    continue;
      case 3: ChangeRate();
	    continue;
      case 4: RequestScan();
            continue;
      case 5: ChangeComputerPortRate();
	    continue;
      case 6: break;
      default: cout << "invalid entry!" << endl;
	    break;
    }
  }
  
  //trying to close the port
  int temp;
  temp = ::close(laser_term);
 cout << endl << "attempted to close port, returned: " << temp << endl;

 return 0;

}

/**
 * This function changes the port rate of the computer side only.
 **/
int ChangeComputerPortRate() {
 struct termios term;
  int test1, test2, test3;
  
  //gets attributes of term; must be called before
  //any of the following commands to set attributes
  tcgetattr(laser_term, &term); 

  //sets attributes to some standard setup
  cfmakeraw(&term); 

  int choice;
  cout << "choose 1 of 4 rates to set baud rate to (enter the number) \n"
	  << "1. 9,600 Bd \n" << "2. 19,200 Bd \n" << "3. 38,400 Bd \n"
	  << "4. 500,000 Bd" << endl;
  cin >> choice;

  //sets input and output baud rates in struct
  switch (choice) {
	  case 1:
  test1 = cfsetispeed(&term, B9600);
  test2 = cfsetospeed(&term, B9600);
  break;
  
	  case 2:
  test1 = cfsetispeed(&term, B19200);
  test2 = cfsetospeed(&term, B19200);
  break;

	  case 3:
  test1 = cfsetispeed(&term, B38400);
  test2 = cfsetospeed(&term, B38400);
  break;	  
  
	  case 4:
  test1 = cfsetispeed(&term, B500000);
  test2 = cfsetospeed(&term, B500000);
  break;
  }
  
  //sends attribute struct to port
  test3 = tcsetattr(laser_term, TCSAFLUSH, &term);
  if((test1==0)&&(test2==0)&&(test3==0))
    cout<<"successfully set i/o rates to new baud"<<endl<<endl;
  else
    cout<<"test1, test2, test3: "<<test1<<' '<<test2<<' '<<test3<<' '
	    << "rate change failed" <<endl;
return 0;
}
    

/**
 * This function sends the request scan telegram
 * (when ladars first started, they're in mode 25h, 
 * which requires sending a request to receive a scan)
 * and prints out the range readings every 20 degrees
 **/
int RequestScan() {

  uint8_t ranges[2048];
  uint8_t msg[2];
  ssize_t len;
  int count;

  //  char *software;
  //  cout<<"trying to set first element"<<endl;
  //  software[0] = ;

  msg[0]='\x30'; //this requests measured values
  msg[1]='\x01'; //this wants all values from a scan
  len=2;

  WriteToLaser(msg,len);

  cout<<"Trying to read from ladar..."<<endl;
  len = ReadFromLaser();
  //  PrintTelegram(grrr, len);

  if (len == 0)
  {
    cout<<"no scan received"<<endl;
    return -1;
  }

  cout<<"received msg type: "<<hex<<(int)grrr[0]<<endl;

  // Process raw packets
  //
  if (grrr[0] == 0xB0)
  {
    cout<<"received expected msg type"<<endl;
		assert((grrr[2] & 0xF8) == 0   ); // make sure data is in cm (not mm) and that we're not getting partial scans

    // Determine the number of values returned
    //
    //int units = grrr[2] >> 6;
    count = (int) grrr[1] | ((int) (grrr[2] & 0x1F) << 8);
    //    assert((size_t) count <= datalen);
    cout<<"expecting "<<count<<" scan points"<<endl;

    // Strip the status info and shift everything down a few bytes
    // to remove packet header.
    //
    for (int i = 0; i < count; i++)
    {
      int src = 2 * i + 3;
      ranges[i] = grrr[src + 0] | (grrr[src + 1] << 8);
    }

    //output the ranges returned
    cout<<"received "<<count<<" scan points"<<endl;
    for(int i=0;i<count;i++) {
      if(i%20==0) {
        cout<<"scan point "<<i<<" has distance "<< dec << (int)ranges[i]
		<<" cm"<<endl;
      }
    } 
  }
  //  else if (grrr[0] == 0xB7)
  // {
     // Determine the number of values returned
  //   int count = (int) grrr[5] | ((int) (grrr[6] & '\x3F') << 8);
     //    assert((size_t) count <= datalen);

    // Strip the status info and shift everything down a few bytes
    // to remove packet header.
  //   for (int i = 0; i < count; i++)
  //  {
  //    int src = 2 * i + 7;
  //    ranges[i] = grrr[src + 0] | (grrr[src + 1] << 8);
  //  }
  // }
  else
    cout<<"unexpected packet type. expected: 0xb0, received: "<<hex<<(int)grrr[0];
    //  RETURN_ERROR(false, "unexpected packet type");

  return 0;
}


/**
 * This function sends the request status telegram
 * to the ladar, and prints relevant data from 
 * the return telegram
 **/
#warning: "FIXME: requestStatus needs parsing of reply"
int RequestStatus() {

  uint8_t *msg;
  ssize_t len;

  //  char *software;
  //  cout<<"trying to set first element"<<endl;
  //  software[0] = ;

  msg[0]='\x31'; //this requests a status 
  len=1;

  WriteToLaser(msg,len);

  cout<<"Trying to read from ladar..."<<endl;
  len = ReadFromLaser();
  PrintTelegram(grrr, len);

  //check that we received the expected message type
  if(strncmp((char *)grrr, (char *)replyB1 ,1) == 0) {
    //block A gives software type
    //    strncpy(software,(char *)grrr,6);
    //    cout<<"ladar is running version: "<<software<<endl;
 
    //block  B gives operating mode
    cout<< endl << "ladar is in operating mode "<<hex<<(int)grrr[8]<<
      "; see p. 51 of telegram manual for explanation"<<endl;

    //block C gives status
    if((int)grrr[9] > 0) {
      cout << "LADAR is defective, has error or fatal error" << endl;
    }
    else {
      cout << "LADAR is NOT defective, no error nor fatal error =)" << endl;
    }

    //block D gives manufacturer code as an 8 digit ascii
    uint8_t mc[8] = {grrr[10], grrr[11], grrr[12], grrr[13], grrr[14],
	    grrr[15], grrr[16], grrr[17]};
    cout << "Manufacturer code is " << hex << (int)mc[0] << " " << hex
	    << (int)mc[1] << " " << hex << (int)mc[2] << " " << hex 
	    << (int)mc[3] << " " << hex << (int)mc[4] << " " << hex
	    << (int)mc[5] << " " << hex << (int)mc[6] << " " << hex
	    << (int)mc[7] << endl;
    bool found = false;
    if ((int)mc[2] == 50 && (int)mc[3] == 49 && (int)mc[6] == 54) {
	    cout << ">>>>>this is the BUMPER ladar!<<<<<" << endl;
	    found = true;
    }
    if ((int)mc[2] == 52 && (int)mc[3] == 52 && (int)mc[6] == 51) {
		cout << ">>>>>this is the ROOF ladar!<<<<<" << endl;
		found = true;
    }
// add when we identify SMALL LADAR	
//  if () {
//              cout << "this is the SMALL ladar!" << endl;
//              found = true;
//  }
    if (found == false) {
	    cout << "unrecognized manufacturer code for this ladar unit =(" 
		    << endl;
    }
	  

    //block E gives variant type. MUST BE EITHER 00 or 01, corresponds to index #18
    
    //block F gives pollution values, 8 words, 16 bytes
    
    //block G reference pollution values, 4 words, 8 bytes

    //block H calibrate pollution channels, 8 words, 16 bytes

    //block I calibrate reference poluution channels, 4 words, 8 bytes
    
    //block J # of motor revolutions, 1 word, 2 bytes
    
    //block K reserved, 1 word, 2 bytes

    //block L reference scale, 1 word, 2 bytes
    
    //block M reserved, 1 word, 2 bytes

    //block N reference scale-2, 1 word, 2 bytes

    //block O, reference scale 66%, 1 word, 2 bytes
    
    //block P, reserved, 1 word, 2 bytes
    
    //block Q, reference scale-2 66%, 1 word, 2 bytes
    
    //block R, signal amplitude, 1 word, 2 bytes

    //block S, current angle, 1 word, 2 bytes

    //block T, peak threshold, 1 word, 2 bytes

    //block U, angle of measurement, 1 word, 2 bytes
    
    //block V, calibration value of signal amplitude, 1 word, 2 bytes

    //block W, target value of stop threshold, 1 word, 2 bytes
    //INDEX 93-94
    
    //block X, target value of peak threshold, 1 word, 2 bytes
    //INDEX 95-96
    
    //Block Y, actual value of stop threshold, 1 word, 2 bytes
    //INDEX 97-98

    //block Z, actual value of peak threshold, 1 word, 2 bytes
    //INDEX 99-100

    //block A1, reserved, 1 byte

    //block A2, Measuring mode, see page 94 for translation by bits, 1 byte
    
    //block A3, Reference target single measured value, 1 word, 2 bytes

    //block A4, reference target mean measured values, 1 word, 2 bytes

    //block A5, scanning angle, 1 word, 2 bytes
    uint8_t scanning_angle[2] = {grrr[107], grrr[108]};
    cout << "the bytes representing the scanning angle are "
	    << hex << (int)scanning_angle[0] << " and " 
	    << hex << (int)scanning_angle[1] << endl;
    cout << "NEED HELP PARSING THESE TWO BYTES" << endl;

    //block A6, angular resolution, 1 word, 2 bytes
    uint8_t angular_resolu[2] = {grrr[109], grrr[110]};
    cout << "the bytes representing the angular resolution are "
	    << hex << (int)angular_resolu[0] << " and " 
	    << hex << (int)angular_resolu[1] << endl;
    cout << "NEED HELP PARSING THESE TWO BYTES" << endl;

    
    //block A7, restart mode, see page 94, 1 byte

    //block A8, restart time, see page 94, 1 byte

    //block A9 and B1, reserved, 2 bytes

    //block B2, BAUD rate, 2 bytes.
     uint8_t baud_rate[2] = {grrr[116], grrr[117]};
    cout << "the bytes representing the baud rate are "
	    << hex << (int)baud_rate[0] << " and " 
	    << hex << (int)baud_rate[1] << endl;
    if ((int)baud_rate[1] != 128) {
      cout << "Error, reading wrong area of telegram for baud rate." << endl;
    }
    switch ((int)baud_rate[0]) {
	    case 103: cout << "Baud rate is set at 9,600" << endl; break;
	    case 51: cout << "Baud rate is set at 19,200" << endl; break;
	    case 25: cout << "Baud rate is set at 38,400" << endl; break;
	    case 1: cout << "Baud rate is set at 500,000" << endl; break;
            default: cout << "Error trying to read baud rate" << endl;
    }

    

  } else {
    cout<<"Ladar returned wrong msg type. expected 0xb1, received "<<hex<<(int)grrr[0];
  }

  return 0;
}


/**
 * This function sends the request test/error  telegram
 * to the ladar, and prints relevant data from 
 * the return telegram
 **/
int RequestTest() {

  uint8_t *msg;
  uint8_t *ans;
  ssize_t max_len=1024;
  ssize_t len;

  msg[0]='\x32'; //this requests a Test/Error list
  len=1;

  WriteToLaser(msg,len);
  //  ans[0] = '\44';
  cout<<"Trying to read from ladar..."<<endl;
  len = ReadFromLaser();
  PrintTelegram(grrr, len);

  //check that we received the expected message type
  if(strncmp((char *)grrr, (char *)replyB2 ,1) == 0) {
    cerr << "Got " << (len-1)/2 << " errors" << endl;

    //while there are errors left, print them out
    for(int i=0; i<(len-1)/2; i++) {
      cout<< "Error " << i << " of " << ((len-1)/2-1) << ": ";
      if(grrr[1+i*2] & 128) 
        cout << "No longer relevant ";
      switch(grrr[1 + i*2] & 7) {
        case 1:
          cout << "info ";
          break;
        case 2:
          cout << "warning ";
          break;
        case 3:
          cout << "error ";
          break;
        case 4:
          cout << "fatal error ";
          break;
        case 0:
        default:
          cout << "non-error ";
          break;
      }
      cout << ((int)grrr[2+i*2])<<endl;
    }

  } else {
    cout<<"Ladar returned wrong msg type. expected 0xb2, received "<<hex<<(int)grrr[0];
  }

  return 0;
}


// This function will change the baud rate
int ChangeRate(){

  uint8_t msg[2];
  uint8_t *ans;
  ssize_t len = 2;

  msg[0] = '\x20';

  int choice;
  cout << "choose 1 of 4 rates to set baud rate to (enter the number) \n"
	  << "1. 9,600 Bd \n" << "2. 19,200 Bd \n" << "3. 38,400 Bd \n"
	  << "4. 500,000 Bd" << endl;
  cin >> choice;
  switch (choice) {
	  case 1: msg[1] = '\x42'; break; 
	  case 2: msg[1] = '\x41'; break;
	  case 3: msg[1] = '\x40'; break;
	  case 4: msg[1] = '\x48'; break;
	  default: cout << "Invalid choice!" << endl;
  }
  
  WriteToLaser (msg, len);
  cout << "Trying to read from ladar..." << endl;
  len = ReadFromLaser();
  PrintTelegram(grrr, len);

   //check that we received the expected message type
  if (strncmp((char *)grrr, (char *)replyA0, 1) == 0) {
    if ((int)grrr[1] == 0) {
    cout << "mode changed successfully =)" << endl;
    
  struct termios term;
  int test1, test2, test3;
  
  //gets attributes of term; must be called before
  //any of the following commands to set attributes
  tcgetattr(laser_term, &term); 

  //sets attributes to some standard setup
  cfmakeraw(&term); 

  //sets input and output baud rates in struct
  switch (choice) {
	  case 1:
  test1 = cfsetispeed(&term, B9600);
  test2 = cfsetospeed(&term, B9600);
  break;
  
	  case 2:
  test1 = cfsetispeed(&term, B19200);
  test2 = cfsetospeed(&term, B19200);
  break;

	  case 3:
  test1 = cfsetispeed(&term, B38400);
  test2 = cfsetospeed(&term, B38400);
  break;	  
  
	  case 4:
  test1 = cfsetispeed(&term, B500000);
  test2 = cfsetospeed(&term, B500000);
  break;
  }
  
  //sends attribute struct to port
  test3 = tcsetattr(laser_term, TCSAFLUSH, &term);
  if((test1==0)&&(test2==0)&&(test3==0))
    cout<<"successfully set i/o rates to new baud"<<endl<<endl;
  else
    cout<<"test1, test2, test3: "<<test1<<' '<<test2<<' '<<test3<<' '<<endl;
    }
  
    
    if ((int)grrr[1] == 1) {
    cout << "mode change failed =(" << endl;
    }
  
  } else {
    cout << "Ladar returned wrong msg type. expected 0xa0, received "
	    << hex << (int)grrr[0];
    }
  return 0;
}	


int Setup() {
  int test1, test2, test3, temp;

  //can only get read/write to work for USB3 and USB5
  char port[] = "../../../../../../dev/ttyUSB";
  cout << "please enter a USB port number (0 - 5)" << endl;
  char number[2];
  cin >> number;
  strcat (port, number);
  cout << "port address is " << port << endl; 
  
  //trying to open connections w/ port
  //using this, can open USB0 - 3 and 5
  laser_term = ::open(port, O_RDWR|O_SYNC, S_IRUSR|S_IWUSR);
  cout<<"attempted to open port "<<port<<"  returned: "<<laser_term<<endl;

  struct termios term;

  //gets attributes of term; must be called before
  //any of the following commands to set attributes
  tcgetattr(laser_term, &term); 

  //sets attributes to some standard setup
  cfmakeraw(&term); 

#warning: "this code assumes that the ladar has just been powered on, and is thus running at 9600 baud"
  //sets input and output baud rates in struct
  test1 = cfsetispeed(&term, B9600);
  test2 = cfsetospeed(&term, B9600);

  //sends attribute struct to port
  test3 = tcsetattr(laser_term, TCSAFLUSH, &term);
  if((test1==0)&&(test2==0)&&(test3==0))
    cout<<"successfully set i/o rates to 9600 baud"<<endl<<endl;
  else
    cout<<"test1, test2, test3: "<<test1<<' '<<test2<<' '<<test3<<' '<<endl;
}

/**
 * The sick ladar expects commands to be in
 * a very specific format...using the
 * sick_driver.cc code w/ a few mods for this
 * (simpler, no error handling)
 **/

ssize_t WriteToLaser(uint8_t *data, ssize_t len)
{
  uint8_t buffer[4 + 1024 + 2];

  //create header
  buffer[0] = STX;
  buffer[1] = '\0';
  buffer[2] = LOBYTE(len);
  buffer[3] = HIBYTE(len);

  //copy data
  memcpy(buffer + 4, data, len);

  // Create footer (CRC)
  uint16_t crc = CreateCRC(buffer, 4 + len);
  buffer[4 + len + 0] = LOBYTE(crc);
  buffer[4 + len + 1] = HIBYTE(crc);

  cout<<"about to send the following telegram:"<<endl;
  PrintTelegram(buffer, len+6);

  // Make sure both input and output queues are empty
  tcflush(laser_term, TCIOFLUSH);

  ssize_t bytes = 0;

  //Assuming that we're not using high-speed 
  //serial (<38400), so we can just
  // write the data to the port
  tcflush(laser_term, TCIOFLUSH);
  bytes = ::write(laser_term, buffer, 4 + len + 2);
  cout<<"wrote "<<bytes<<" bytes to the laser"<<endl<<endl;

  // Return the actual number of bytes sent, including header and footer
  return bytes;
}

/**
 * again, using the sick_driver.cc code as
 * a base for this...
 * 
 * for now, this code just reads a telegram and
 * has it printed out. more parsing to follow.
 **/

ssize_t ReadFromLaser() {
  unsigned char data[1024];
  int n,c;
  int bytes = 0;
  unsigned short crc;
  ssize_t len;
 
  int gotStr = 0;

  memset(data, 0, 5);

  while(gotStr == 0) {
    // read the stream of bytes one by one until we see the message header
    // move the first 4 bytes in the data buffer one place left
    for( n=0; n<4; n++ ) {
      data[n] = data[n+1];
    }
   
    bytes += read(laser_term, &data[4], 1 );

    if( strncmp( (char *) data, (char *) startString2, 2 ) == 0 ) // ok we've got the start of the data 
      {
	gotStr = 1;
	cout<<"recognized startString...now reading values"<<endl;

	bytes = 1; //ln 115, we read in one byte... 

	//check how many bytes the header says should be included
        len = ((int)data[2] | ((int)data[3] << 8));
	cout<<"expecting "<<len<<" bytes."<<endl;

        while(bytes<len) {//reading in the msg body
	  bytes += read( laser_term, &data[bytes+4], len-bytes);
	}

	//reading in the footer
	bytes += read( laser_term, &data[len+4+0],1);
	bytes += read( laser_term, &data[len+4+1],1);

        uint16_t crc = CreateCRC(data, 4 + len);
	uint16_t recd = MAKEUINT16(data[4+len], data[4+len+1]);
        if (crc != recd) {
          cout<<"didn't get expected crc!"<<endl;
        }
        cout<<"CRC received: "<<hex<<recd<<"; CRC expected: "<<hex<<crc<<endl; 

	//	PrintTelegram(data,4+len+2);
	memcpy(grrr, data+4, len);
      } // if(strncmp...
  } //end while
  return len;
}


/**
 * a method that prints out the data telegram
 * that's being sent/received
 **/
int PrintTelegram(uint8_t *data, ssize_t len) {
  for(int i=0; i<len; i++) {
    cout<<"0x"<<hex<<(int)data[i]<<' ';
    if(i%10 == 9)
      cout<<endl;
  }
  cout<<endl;

  return 0;
}

/**
 * the ladar also expects a specific format
 * in the footer...using sick_driver.cc
 * to create this (copy&paste..no changes)
 **/
unsigned short CreateCRC(uint8_t* data, ssize_t len)
{
  uint16_t uCrc16;
  uint8_t abData[2];
  
  uCrc16 = 0;
  abData[0] = 0;
  
  while(len-- )
  {
    abData[1] = abData[0];
    abData[0] = *data++;
    
    if( uCrc16 & 0x8000 )
    {
      uCrc16 = (uCrc16 & 0x7fff) << 1;
      uCrc16 ^= CRC16_GEN_POL;
    }
    else
    {    
      uCrc16 <<= 1;
    }
    uCrc16 ^= MAKEUINT16(abData[0],abData[1]);
  }
  return (uCrc16); 
}

/**
 * needed this for timeout purposes,
 * but didn't want to have to include
 * any files from repository.
 */
void DGCgettime(unsigned long long& DGCtime)
{
	timeval tv;
	gettimeofday(&tv, NULL);

	DGCtime = (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;
}
