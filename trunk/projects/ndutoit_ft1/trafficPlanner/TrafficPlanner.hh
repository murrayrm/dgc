#ifndef TRAFFICPLANNER_HH
#define TRAFFICPLANNER_HH

#include "StateClient.h"
#include "CMapPlus.hh"
#include "MapdeltaTalker.h"
#include "MapConstants.h"
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include "rddf.hh"
#include "AliceConstants.h"
#include "trafficUtils.hh"
#include "../../../util/moduleHelpers/RDDFTalker.hh"
#include "../../nok/missionPlanner/LocalMapTalker.hh"
#include "RNDF.hh"
#include "SegGoalsTalker.hh"
#include "../../nok/missionPlanner/GloNavMapTalker.hh"
#include <vector>
#include <math.h>
#include "../Mapper/include/Map.hh"
//#include "Map.h"

#define EPS 0.05
#define BIGNUMBER 1000000000.0f 
#define PI 3.1416
#define LANEWIDTH 5.0f
#define ALICE_MAX_DECELERATION VEHICLE_MAX_DECEL/2
#define SEG_COMPLETE_DIST_TO_EXIT 1.0f
#define SEG_ALMOST_COMPLETE_DIST_TO_EXIT 10.0f
#define STOP_SPEED_SQR 1.0f

/****************************************************************************/
/* 						Temporary defs										*/
/****************************************************************************/
//enum Divider { DOUBLE_YELLOW=0, SOLID_WHITE=1, BROKEN_WHITE=2, ROAD_EDGE=3 };
enum PlanningHorizon {CURRENT_SEGMENT, NEXT_SEGMENT, NEXT_NEXT_SEGMENT};

struct Location
{
	Location(){}
	
	double getXLoc() {return x;}
	double getYLoc() {return y;}
	double x;
	double y;
};

struct LaneObject
{
	vector<Location> laneLBCoord;
	vector<Location> laneRBCoord;
	Divider laneLBType;
	Divider laneRBType;
};

struct StopLine
{
	vector<Location> coordinates;
};

struct CheckPoint
{
	Location coordinates;
};

struct SegmentObject
{
	vector<LaneObject> lane;
	StopLine stopLine;
	CheckPoint checkPoint;
};

struct LocalMapObject
{
	SegmentObject segment;
};


/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/


class CTrafficPlanner : public CStateClient, public CSegGoalsTalker, public CGloNavMapTalker, public CMapdeltaTalker, public CRDDFTalker, public CLocalMapTalker
{ 
  
  RNDF* m_rndf;
  int m_rndfRevNum;                // RNDF revision#
  pthread_mutex_t m_GloNavMapMutex;

  // basic constructor of local map
  vector<Mapper::Segment> localMapSegments;
  Mapper::Map m_localMap_test;
  Mapper::Map m_localMap;
  //Mapper::Map m_localMap;
  pthread_mutex_t m_LocalMapMutex;
  pthread_cond_t m_LocalMapRecvCond;
  pthread_mutex_t m_LocalMapRecvMutex;
  bool m_bReceivedAtLeastOneLocalMap;
  int localMapSocket;                // The skynet socket for receiving local map from mapping
  
  vector<Obstacle*> m_obstacle;
  pthread_mutex_t m_ObstacleMutex;

  CMapPlus m_costMap;
  CMapPlus m_elevationMap;
  pthread_mutex_t m_CostMapMutex;

  int m_fullMapRequestSocket;
  int m_GloNavMapRequestSocket;
  int m_GloNavMapSocket;

  DPlannerStatus* m_dplannerStatus;
  pthread_mutex_t m_DPlannerStatusMutex;

  list<SegGoals> m_segGoals;
  pthread_mutex_t m_SegGoalsMutex;
  int segGoalsSocket;                // The skynet socket for receiving segment goals from mplanner

  /*! Whether at least one delta was received */
  bool m_bReceivedAtLeastOneDelta;
  pthread_mutex_t m_deltaReceivedMutex;
  pthread_cond_t m_deltaReceivedCond;

public:
  /*! Contstructor */
  CTrafficPlanner(int skynetKey, bool bWaitForStateFill, char*);
  /*! Standard destructor */
  ~CTrafficPlanner();

  /*! this is the function that continually reads map deltas and updates the map */
  void getMapDeltasThread();

  void getLocalMapThread();

  /*! this is the function that continually reads dplanner status*/
  void getDPlannerStatusThread();

  /*! this is the function that continually receives segment goals from mplanner */
  void getSegGoalsThread();

  void TPlanningLoop(void);

 

private:
  /*! This function is used to request the full map from mapping */
  void requestFullElevationMap();

  void requestGloNavMap();

  void elevation2Cost(CMapPlus, CMapPlus&);
  
  vector<TrafficRules> extractRules(SegmentType segmentType);
  
  ConstraintSet laneFollowing(vector<Mapper::Location> laneLBCoord, vector<Mapper::Location> laneRBCoord, Mapper::LaneBoundary::Divider laneLBType, Mapper::LaneBoundary::Divider laneRBType, vector<TrafficRules> trafficRules, double dLimit, double dist2Exit, double x_A, double y_A); 
  
  ConstraintSet intersectionNav(vector<Mapper::Location> laneLBCoord, vector<Mapper::Location> laneRBCoord, vector<TrafficRules> trafficRules, double dLimit, double x_A, double y_A);

  ConstraintSet laneFollowing_local(vector<Location> laneLBCoord, vector<Location> laneRBCoord, Divider laneLBType, Divider laneRBType, vector<TrafficRules> trafficRules, double dLimit, double dist2Exit, double x_A, double y_A); 
  
  void lineFit(double x_toFit[],double y_toFit[],int sizeOfXToFit,double& slope,double& intersection);
  
  double closestBoundaryPt(vector<double> xB,vector<double> yB, double x, double y, int& ind_CP, int& ind_Insert, double& x_Pt, double& y_Pt);
  
  void insertPointOnBoundary(vector<double>& xB,vector<double>& yB,double x, double y,int index);
  
  void distanceAlongBoundary(vector<double>& xB, vector<double>& yB, int& index, double distance);
  
  vector<Constraint> boundaryConstraints(vector<double> xB1, vector<double> yB1, vector<double> xB2, vector<double> yB2, double x, double y, double distance, bool insert_flag);
  
  LocalMapObject spoofLocalMapData(int segmentID, int numLanes, int numTravelLanes, double laneWidth, int senseMin, int senseMax, int xSpacing);  
  
  SegGoals spoofSegmentGoal();
  
  bool convRDDF( vector<Constraint> line1, vector<Constraint> line2, SegGoals currentSegGoals, double x_A, double y_A, double laneWidth, RDDF* newRddf);
  
  
  point closept( double lnseg[4], double pt[2] );
  
  point midpt( double pt1[2], double pt2[2] );
  
  double radius( double mpt[2], double pt1[2], double pt2[2] );

  void generateMapFromRNDF(RNDF*, Mapper::Map&);
  };

#endif  // TRAFFICPLANNER_HH

