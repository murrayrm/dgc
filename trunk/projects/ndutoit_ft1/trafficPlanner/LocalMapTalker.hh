#ifndef LOCALMAPTALKER_HH
#define LOCALMAPTALKER_HH

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "SkynetContainer.h"
#include "DGCutils"
#include "trafficUtils.hh"
#include "rndf.hh"

class CLocalMapTalker : virtual public CSkynetContainer {
  pthread_mutex_t m_localMapDataBufferMutex;
  char* m_pLocalMapDataBuffer;

public:
  CLocalMapTalker();
  ~CLocalMapTalker();

  bool SendLocalMap(int localMapSocket, Map* localMap);
  bool RecvLocalMap(int localMapSocket, Map* localMap, int* pSize);
};

#endif //  LOCALMAPTALKER_HH
