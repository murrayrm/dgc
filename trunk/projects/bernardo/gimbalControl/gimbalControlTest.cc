#include "gimbalControl.hh"

char* TRAJFILENAME = "gps_20060824_122110_UTM_real.dat";

const char* INTERSECTIONFILENAME = "tintersect.dat";
const char* INTERSECTIONLAYERPREFIX = "tintersect_layer";


class gimbalTest: public CStateClient, public CTrajTalker
{
public:
  
  gimbalTest(int skynetKey): CSkynetContainer(MODgimbalControl, skynetKey){}
  
  NEcoord calculatePointAtStoppingDistanceInTraj(CTraj trajToPoint)
  {    
    UpdateState();
        
    //compute stopping distance based on current velocity
    double stoppingDistance, currentSpeed;
    currentSpeed = hypot(m_state.Vel_N, m_state.Vel_E);
    stoppingDistance = currentSpeed*currentSpeed / (2.0 * VEHICLE_MAX_DECEL);
    
    //search point in traj @ that distance
    int index;
    NEcoord stoppingPoint;
    index = trajToPoint.getPointAhead(0, stoppingDistance);
    stoppingPoint = trajToPoint.getNEcoord(index);

    return stoppingPoint;
  }

  void trajPointingLoop()
  {
    _QUIT = 0;
    int m_send_socket = m_skynet.get_send_sock(SNgimbalCommand);

    //like this, it inits from file; need to use trajtalker
    CTraj currentTraj(1, TRAJFILENAME);
    //currentTraj.print();
    int numPoints = currentTraj.getNumPoints(), i;
    gimbalCommand commandToSend;
    

    char answ = 'f';
    /*do
      {
	cout << "File(f) or TrajTalker(t)?" << endl;
	cin >> answ;
      }
      while(answ != 'f' || answ != 't');*/

#warning "need to add/complete timestamp definition through the states file"
    unsigned long long timestampIssued;
    do
      {
	cout << "Current point in Traj (1-30)? (0 to quit)" << endl;
	cin >> timestampIssued;
      }
    while(timestampIssued < 0  && timestampIssued > 30);	
    
    if(timestampIssued == 0)
      {
	return;
      }
    
    for(i=0;i<numPoints;i++)
      {
	switch(answ)
	  {
	  case 't':
	    /*(receive planned trajectory and) calculate stopping distance
	      
	    commandToSend.UTM = calculatePointAtStoppingDistanceInTraj(currentTraj);
	    */
	    break;
	  case 'f':
	    commandToSend.init();
	    commandToSend.UTM = NEDcoord(currentTraj.getNEcoord(i)); //D=0 by default
	    break;
	  }

	//send command

	//DGCgettime(timestampIssued);
	commandToSend.timestampIssued = timestampIssued;
	commandToSend.mode = TRACKING;
	
	m_skynet.send_msg(m_send_socket, &commandToSend, sizeof(gimbalCommand));
      }
  }
  
  //for a t intersection
  void intersectionMapPointingLoop()
  {
    _QUIT = 0;
    int m_send_socket = m_skynet.get_send_sock(SNgimbalCommand);

    CMapPlus gMap;
    gMap.initMap(0.0, 0.0, 300, 200, 0.8, 0.8, 0);
    //boundaries = 1 and corners = 2
    int intersectionLayerID = gMap.addLayer<double>(0.0, -1.0, FALSE, "Intersection");
    
    string stemp;
    unsigned long long ttemp;
    NEDcoord pos, corners[2];

    /*------------------------------this should go in a separate function createIntersectionInMap(------------------*/

    //create intersection in map
    //each road is supposed to be 100 m long and have the same width, with 2 lanes with equal widths = 0.5*road width
    //thus only need to read 4 points (corners); written clockwise, with the first point being the one we are closer to
    
    gMap.clearLayer(intersectionLayerID);
    
    ifstream intersectionFile(INTERSECTIONFILENAME);
    if(intersectionFile)
      {
	getline(intersectionFile, stemp);
		    
	istringstream istemp(stemp);
	istemp >> ttemp >> pos.N >> pos.E;
	pos.D = 0;

	gMap.updateVehicleLoc(pos.N, pos.E);

	//perfectly square intersection, thus only need to read 2 points; the first point being the one we are closer to and the second the diagonally opposite one
	for(int i = 0; i < 2 || !(intersectionFile.eof()); i++)
	  {
	    getline(intersectionFile, stemp);
	    
	    istringstream istemp(stemp);
	    istemp >> corners[i].N >> corners[i].E;
	    corners[i].D = 0;
	    
	    gMap.setDataUTM(intersectionLayerID, corners[i].N, corners[i].E, 2.0);
	  }
	intersectionFile.close();
      }
    else
      {
	cerr << "Error opening state from intersection file. Quitting..." << endl;
	_QUIT = 1;
	return;
      }
    
    //for a t intersection where North is where there's no road
    double j, k;
    

    /* this probably isn't correct
       
    double roadWidth, laneWidth;
    if(!(corners[0].E - corners[1].E) != (corners[1].N - corners[0].N))
      {
	//roadWidth = corners[0].E - corners[1].E;
	//laneWidth = roadWidth / 2;
      }
    else
      {
	cerr << "That's not a perfectly square intersection. Quitting..." << endl;
	_QUIT = 1;
	return;
      }

    */

    //boundaries
    for(j = gMap.getResCols(); j <= 100; j += gMap.getResCols())
      {
	gMap.setDataUTM(intersectionLayerID, corners[0].N - j, corners[0].E, 1.0);
	//gMap.setDataUTM(intersectionLayerID, corners[0].N - j, corners[1].E + laneWidth, 1);
	gMap.setDataUTM(intersectionLayerID, corners[0].N - j, corners[1].E, 1.0);
      }
    for(j = gMap.getResRows(); j <= 100; j += gMap.getResRows())
      {
	gMap.setDataUTM(intersectionLayerID, corners[0].N, corners[0].E + j, 1.0);
	//gMap.setDataUTM(intersectionLayerID, corners[0].N + laneWidth, corners[0].E + j, 1);
	gMap.setDataUTM(intersectionLayerID, corners[1].N, corners[0].E + j, 1.0);

	gMap.setDataUTM(intersectionLayerID, corners[0].N, corners[1].E - j, 1.0);
	//gMap.setDataUTM(intersectionLayerID, corners[0].N + laneWidth, corners[1].E -j, 1);
	gMap.setDataUTM(intersectionLayerID, corners[1].N, corners[1].E - j, 1.0);
      }
    for(j = corners[1].E + gMap.getResRows(); j < corners[0].E; j += gMap.getResRows())
      {
	gMap.setDataUTM(intersectionLayerID, corners[1].N, j, 1.0);
      }

    //cout.precision(15);
    //cout << gMap.getWindowBottomLeftUTMNorthing() << " " << gMap.getWindowBottomLeftUTMEasting() << endl;
    //cout << gMap.getWindowTopRightUTMNorthing() << " " << gMap.getWindowTopRightUTMEasting() << endl;

      for(j = corners[1].N + gMap.getResCols(); j <= gMap.getWindowTopRightUTMNorthing(); j += gMap.getResCols())
      {
	for(k = gMap.getWindowBottomLeftUTMEasting(); k <= gMap.getWindowTopRightUTMEasting(); k += gMap.getResRows())
	  {
	    gMap.setDataUTM(intersectionLayerID, j, k, -1.0);
	  }
      }
    
    for(j = corners[0].N - gMap.getResCols(); j <= gMap.getWindowBottomLeftUTMNorthing(); j -= gMap.getResCols())
      {
	for(k = gMap.getWindowBottomLeftUTMEasting(); k <= gMap.getWindowTopRightUTMEasting(); k += gMap.getResRows())
	  {
	    if(k >= corners[1].E && k <= corners[0].E)
	      {
		k = corners[0].E + gMap.getResRows();
	      }
	    else
	      {
	       gMap.setDataUTM(intersectionLayerID, j, k, -1.0);
	      }
	  }
      }

    gMap.saveLayer<double>(intersectionLayerID, INTERSECTIONLAYERPREFIX);
    
    /*----------------------------------------------------------------------------------------------------*/

    /*t intersection behavior:
      
    look left/right (decide somehow or assume 2 gimbals + time the looks)
    
    update information layer
    -look down lanes of interest thro, within certain distance
    -what about angles we shouldn't look? reverse calculations
                              
    find less information cells in map
    send command
    
    */
    
    //~~~
    double testMiddle = corners[0].N + (corners[1].N - corners[0].N) / 2;
    gimbalCommand commandToSend;
  
    for(j = 0; j <= 100; j += 10 * gMap.getResRows())
      {
	commandToSend.init();

	commandToSend.UTM.N = testMiddle;
	commandToSend.UTM.E = corners[1].E - j;
	//commandToSend.UTM.D = 0;
	
	commandToSend.UTM.display();

	commandToSend.mode = INTERSECTION;
	commandToSend.timestampIssued = ttemp;

	m_skynet.send_msg(m_send_socket, &commandToSend, sizeof(gimbalCommand));
      }    
  }

  void activeLoop()
  {
    _QUIT = 0;
    int m_send_socket = m_skynet.get_send_sock(SNgimbalCommand);
    while(!_QUIT)
      {
	
	gimbalCommand commandToSend;
	commandToSend.init();

	char mode, ans;
	unsigned long long timestampIssued;
	
	cout << endl << "Send comand? (y/n)" << endl;
	cin>>ans;
	if(ans!='n' && ans!='y')
	  {
	    ans = 'n';
	  }
	if(ans == 'n')
	  {
	    break;
	  }
	
	cout<<"UTM(u), XYZ(x), RPY (r)?"<<endl;
	cin>>ans;
	switch(ans)
	  {
	  case 'u':
	    cout << "N E D UTM?" << endl;
	    cin >> commandToSend.UTM.N
		>> commandToSend.UTM.E
		>> commandToSend.UTM.D;
	    break;
	  case 'x':
	    cout << "X Y Z relative to vehicle?" << endl;
	    cin >> commandToSend.XYZ_Vehicle.X
		>> commandToSend.XYZ_Vehicle.Y
		>> commandToSend.XYZ_Vehicle.Z;
	    break;
	  case 'r':
	    cout << "R P Y angle?" << endl;
	    cin >> commandToSend.RPYAngle_Gimbal.R
		>> commandToSend.RPYAngle_Gimbal.P
		>> commandToSend.RPYAngle_Gimbal.Y;
	    break;
	  }
	
	cout << "R P Y speed?" << endl;
	cin >> commandToSend.RPYSpeed_Gimbal.R
	    >> commandToSend.RPYSpeed_Gimbal.P
	    >> commandToSend.RPYSpeed_Gimbal.Y;
	
	cout << "Mode = OVERRIDE (o), TRACKING(t), INTERSECTION(i), UTURN(u), NONE(n)?" << endl;
	cin >> mode;
	switch(mode)
	  {
	  case 'o':
	    commandToSend.mode = OVERRIDE;
	    break;
	  case 't':
	    commandToSend.mode = TRACKING;
	    break;
	  case 'i':
	    commandToSend.mode = INTERSECTION;
	    break;
	  case 'u':
	    commandToSend.mode = UTURN;
	    break;
	  case 'n':
	  default:
	    commandToSend.mode = NONE;
	    break;
	  }
	
	int i,j;
	cout << "How many?" << endl;
	cin >> j;
	if(j<0)
	  {
	    j=1;
	  }
	cout<<"Issuing times:"<<endl;
	for(i=0;i<j;i++)
	  {
	    //DGCgettime(timestampIssued);
	    timestampIssued = 1;
	    commandToSend.timestampIssued = timestampIssued;
	    cout<<timestampIssued<<endl;
	    m_skynet.send_msg(m_send_socket, &commandToSend, sizeof(gimbalCommand));
	  }
      }
  }

private:
  
  int _QUIT;
 
};

int main(int argc, const char* argv[])
{
  int sn_key = -1;
  char* pSkynetkey = getenv("SKYNET_KEY");
  char ans;
  
  if( pSkynetkey == NULL )
    {
      cerr << "SKYNET_KEY environment variable isn't set" << endl;
    }
  else
  sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  
  gimbalTest test(sn_key);
  
  while(TRUE)
    {
      cout << "Active(a), Traj(t), Map(m), Quit(q)?" << endl;
      cin >> ans;
      switch(ans)
	{
	case 'a':
	  test.activeLoop();
	  break;
	case 't':
	  test.trajPointingLoop();
	  break;
	case 'm':
	  test.intersectionMapPointingLoop();
	  break;
	case 'q':
	  return 0;
	default:
	  break;
	}
    }
}

/*Changes to ladarSource:

before .UTMpoint (line 295):

setLadarFrame through:(scanning file with gimbal status logged) broadcasted gimbal state

*/
/* both --> check how gimbal motion affects measurements*/
/*Changes to stereoSource

before .loadPair or inside loadPair:

check gimbal state;
if(gimbal state has changed){
	get new frame;
	updateC2B(new frame);
}

int stereoSource::updateC2B(new frame){
update roll, pitch, yaw, position;
newC2B = Matrix(4,4);
maketransform(newC2B,...);
C2B=newC2B;
}
*/
