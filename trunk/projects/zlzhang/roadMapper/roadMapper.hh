#ifndef __ROADMAPPER_HH__
#define __ROADMAPPER_HH__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cv.h>

#include "DGCutils"
#include "stereovision/stereoSource.hh"
#include "stereovision/stereoProcess.hh"
#include "GlobalConstants.h"
#include "frames/frames.hh"
#include "StateClient.h"
#include "sn_msg.hh"

#include "roadFinding.hh"
#include "RNDF.hh"
#include "GPSPont.hh"

#define USE_FILES 1
#define USE_CAMERAS 0
 
using namespace std;

class roadMapper: public CStateClient {

public:

  //Constructor.
  /*It take the skynetkey the simulator is running and initializes the internal
   *variables and global run properties.
   */
  roadMapper(int skynetKey);

  //Destructor.
  //It cleans up the IplImage objects created by OpenCV.
  ~roadMapper();

  //This method calls a roadFinding process which assists this class to detect
  //lines on the stereo images.
  roadFinding roadFindingProcess (int skynetKey);

  //This will create the RNDF file for the local map. 
  RNDF rndfFile();

  //This will return the current state.  
  VehicleState iState ();

  //This will return the current GPS coordinate of Alice.
  GPSPoint iGPS ();
  
  //Member Functions.
  int laneLabel (roadFinding process) {
  
  }

 private:

  //Helper Functions:


}


int main () 
{
 
  return 0;
}
