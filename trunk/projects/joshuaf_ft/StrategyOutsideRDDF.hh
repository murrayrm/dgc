#ifndef STRATEGY_OUTSIDERDDF_HH
#define STRATEGY_OUTSIDERDDF_HH

//SUPERCON STRATEGY TITLE: Outside RDDF strategy
//HEADER FILE (.hh)
//DESCRIPTION: Strategy to handle the situation in which Alice is outside of the
//RDDF, and as such is unable to continue to make forward progress

#include "Strategy.hh"
#include "sc_specs.h"
#include "stage_iterator.hh"

namespace s_outsiderddf {

#define OUTSIDE_RDDF_STAGE_LIST(_) \
  _(estop_pause, = 1) /*first entry must = 1 (as this is referenced against NEXTstage */ \
  _(broaden_rddf, ) \
  _(trans_nominal, )
DEFINE_ENUM(outside_rddf_stage_names, OUTSIDE_RDDF_STAGE_LIST)

}

using namespace std;
using namespace s_outsiderddf;

class CStrategyOutsideRDDF : public CStrategy
{

/** METHODS **/
public:

  CStrategyOutsideRDDF() : CStrategy() {}  
  ~CStrategyOutsideRDDF() {}


  /** MUTATORS **/
  void stepForward(const SCdiagnostic *m_pdiag, CStrategyInterface *m_pStrategyInterface);
  void leave( CStrategyInterface *m_pStrategyInterface );
  void enter( CStrategyInterface *m_pStrategyInterface );

};

#endif
