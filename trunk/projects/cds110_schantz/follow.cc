/*
 * follow.cc - simple trajectory tracking algorithm for CDS 110b
 *
 * This file contains a simple trajectory tracking framework for use
 * on Alice.  The controller is defined in a file that is loaded up at
 * run-time, so that students can design controllers and see how they
 * work. 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <getopt.h>
using namespace std;

#include "FollowClient.hh"		// definition of common variables

// Global variables
double inp[2*NUMINP];		// controller inputs (cur + ref)
double err[NUMINP];		// error between current and reference
double out[NUMOUT];		// controller outputs (acc, steer)
double meas[NUMMEAS+NUMOUT]; //measurements (x,y, theta_dot)
double covar[NUMINP];
double correct[NUMINP];
double outGain[NUMOUT], outCtrl[NUMOUT], outFF[NUMOUT], outCmd[NUMOUT], outRef[NUMOUT], outState[NUMOUT];
int outOverride[NUMOUT];
double xorigin = 3833759;	// Northing and Easting for Caltech
double yorigin = 487579;
double overrideVel = 0.0;

int sn_key = 0;
int laneSight;
double controlRate;
double actualRate;
double currentTime;
double threshold;

int previousLaneCondition;
int currentLaneCondition;

double oldsigmaL;
double oldsigmaR;
double sigmaL;
double sigmaR;

int behaviorFlag;

double rightMaintainDistance;
double leftMaintainDistance;
double rightDefaultDistance;
double leftDefaultDistance;
double trajLeftVector[25];
double trajRightVector[25];





char ctrlFilename[256] = "default.mat";
char gainFilename[256] = " ";
char obsvFilename[256] = "default_obs.mat";
char trajFilename[256] = "default.traj";
char logFilename[256] = "default.log";
char loggingStatus[4] = "On";
char autoNamingStatus[4] = "Off";
char obsvStatus[4] = "Off";
char stateType[16] = "Value";

char statusEnabled[4] = "[ ]";
char statusDisabled[4] = "[X]";
char statusPaused[4] = "[ ]";
char obsvEnabled[4] = "[ ]";
char obsvDisabled[4] = "[X]";


char statusMessage[80] = "OK";

int USE_SPARROW = 1;
int NO_COMMANDS = 0;
int PROJ_ERR = 0;		// don't project error unless specified
int trajpoint = -1;		// current trajectory point
double trajerr[11];		// trajectory error (for sparrow)

FollowClient *client;		// skynet client for send/recv messages

//Sparrow functions
int home(long arg);
int toggleLogging(long arg);
int toggleAutoNaming(long arg);
int newLogFile(long arg);
int resumeControl(long arg);
int pauseControl(long arg);
int disableControl(long arg);
int resetFiles(long arg);
int toggleObserver(long arg);

// Sparrow displays - these should be included *after* global variables
#include "sparrow/display.h"
#include "maindisp.h"

enum {
	OPT_NONE,
	OPT_HELP,
	OPT_TRAJ,
	OPT_CTRL,
	OPT_OBSV,
	OPT_NOSPARROW,
	OPT_NOCOMMANDS,
	OPT_PROJERR,


	NUM_OPTS
};

// Command line options
static struct option long_options[] = {
  // first: long option (--option) string
  // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
  // third: if pointer, set variable to value of fourth argument
  //        if NULL, getopt_long returns fourth argument
	{"nosparrow",   no_argument, NULL, OPT_NOSPARROW},
  {"help",        no_argument, NULL,              OPT_HELP},
	{"nocommands",  no_argument, NULL, OPT_NOCOMMANDS},
	{"trajectory", required_argument, NULL, OPT_TRAJ},
	{"controller", required_argument, NULL, OPT_CTRL},
	{"observer", required_argument, NULL, OPT_OBSV},
	{"projerr", no_argument, NULL, OPT_PROJERR},
	{0,0,0,0}
};
char *usage_string = "\
Usage: follow [options]\n\
  -h, --help                  print usage information\n\
  -c, --controller FILENAME   load controller specified by FILENAME by default\n\
  -t, --trajectory FILENAME   load trajectory specified by FILENAME by default\n\
  -o, --observer   FILENAME   load observer specified by FILENAME by default\n\
      --nocommands            do not send commands to adrive\n\
  --projerr		      project error onto trajectory\n\
";

int main(int argc, char **argv) {
  int ch, errflg = 0;

  // Print a welcome message to let everyone know we are alive
  cout << "Welcome to follow!" << endl;

	int option_index = 0;

  // Parse command line options
  while (!errflg && (ch = getopt_long_only(argc, argv, "",
				      long_options, &option_index)) != -1) {
    switch (ch) {
		case '?':
    case OPT_HELP:			// print options
      fprintf(stderr, usage_string);
      ++errflg;
      break;
		case OPT_TRAJ:
			if(optarg!=NULL) {
				sprintf(trajFilename, "%s", optarg);
			}
			break;
		case OPT_CTRL:
			if(optarg!=NULL) {
				sprintf(ctrlFilename, "%s", optarg);
			}
			break;
		case OPT_OBSV:
			if(optarg!=NULL) {
				sprintf(obsvFilename, "%s", optarg);
			}
			break;
	        case OPT_PROJERR:
		  PROJ_ERR = 1;
		  break;
		case OPT_NOSPARROW:
			USE_SPARROW = 0;
			break;
		case OPT_NOCOMMANDS:
			NO_COMMANDS = 1;
			break;
    default:
      if(ch!=0) {
				printf("Unknown option %d!\n", ch);
				fprintf(stderr, usage_string);
				exit(1);
      }
    }
  }

  // Check to see if there were any errors
  if (errflg) { exit(1); }

  // Get the skynet key that we will be using
  char* pSkynetkey = getenv("SKYNET_KEY");
  if(pSkynetkey == NULL) {
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  } else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  /*
   * Start up any threads that are required
   *
   * In this section, we start up the various threads that feed data 
   * to the follower.  These threads are each defined in separate
   * files that define the data structures that are used.
   *
   */

  // Set up skynet module
  client = new FollowClient(sn_key);

  // Threads for reading state and reference
//   DGCstartMemberFunctionThread(client, &FollowClient::ReadState);
//   DGCstartMemberFunctionThread(client, &FollowClient::ReadTraj);
  DGCstartMemberFunctionThread(client, &FollowClient::ControlLoop);

	if(USE_SPARROW == 1) {
		// Startup the sparrow display
		if (dd_open() < 0) {
			cerr << "follow: can't initalize display\n";
			exit(1);
		}
		dd_usetbl(maindisp);		// start in the main display table

		dd_bindkey('Q', dd_exit_loop);
		dd_bindkey('q', dd_exit_loop);
		dd_bindkey('H', home);
		dd_bindkey('h', home);
		dd_bindkey('L', toggleLogging);
		dd_bindkey('l', toggleLogging);
		dd_bindkey('A', toggleAutoNaming);
		dd_bindkey('a', toggleAutoNaming);
		dd_bindkey('N', newLogFile);
		dd_bindkey('n', newLogFile);
		dd_bindkey('R', resumeControl);
		dd_bindkey('r', resumeControl);
		dd_bindkey('E', resumeControl);
		dd_bindkey('e', resumeControl);
		dd_bindkey('P', pauseControl);
		dd_bindkey('p', pauseControl);
		dd_bindkey('D', disableControl);
		dd_bindkey('d', disableControl);
		dd_bindkey('O', toggleObserver);
		dd_bindkey('o', toggleObserver);
		dd_bindkey(' ', disableControl);

		dd_loop();			// start the display manager
		dd_close();			// close up the screen		
	} else {
		while(1) {
			//spin our wheels until I restructure this
			sleep(10000);
		}
	}
  // Close off any threads that we have started

  return 0;
}


int home(long arg) {
	xorigin = inp[XPOS] + xorigin;
	yorigin = inp[YPOS] + yorigin;
	dd_redraw(0);
	
	return 0;
}


int toggleLogging(long arg) {
	sprintf(client->logFilename, "%s", logFilename);

	if(client->toggleLogging()) {
		sprintf(loggingStatus, "Off");
	} else {
		sprintf(loggingStatus, "On");
	}
	sprintf(logFilename, "%s", client->logFilename);
	dd_redraw(0);

	return 0;
}


int toggleAutoNaming(long arg) {
	if(client->useAutoLogNaming == true) {
		client->useAutoLogNaming = false;
		sprintf(autoNamingStatus, "On");
	} else {
		client->useAutoLogNaming = true;
		sprintf(autoNamingStatus, "Off");		
	}
	dd_redraw(0);

	return 0;
}


int newLogFile(long arg) {
	if(client->loggingEnabled) {
		toggleLogging(0);
	}
	toggleLogging(0);

	return 0;
}


int resumeControl(long arg) {
	if(client->setControlStatus(FollowClient::enabled)) {
		sprintf(statusEnabled, "[X]");
		sprintf(statusDisabled, "[ ]");
		sprintf(statusPaused, "[ ]");
		dd_redraw(0);
	}

	return 0;
}


int pauseControl(long arg) {
	if(client->setControlStatus(FollowClient::paused)) {
		sprintf(statusEnabled, "[ ]");
		sprintf(statusDisabled, "[ ]");
		sprintf(statusPaused, "[X]");
		dd_redraw(0);
	}

	return 0;
}


int disableControl(long arg) {
	if(client->setControlStatus(FollowClient::disabled)) {
		sprintf(statusEnabled, "[ ]");
		sprintf(statusDisabled, "[X]");
		sprintf(statusPaused, "[ ]");
		dd_redraw(0);
	}

	return 0;
}


int toggleObserver(long arg) {
	client->toggleObsvStatus();

	if(client->obsvEnabled == FollowClient::enabled) {
		sprintf(obsvEnabled, "[X]");
		sprintf(obsvDisabled, "[ ]");
		sprintf(obsvStatus, "Off");
		sprintf(stateType, "Estimate!");
	} else {
		sprintf(obsvEnabled, "[ ]");
		sprintf(obsvDisabled, "[X]");
		sprintf(obsvStatus, "On");
		sprintf(stateType, "Value");
	}
	dd_redraw(0);

	return 0;
}


int resetFiles(long arg) {
	disableControl(0);
	client->setupFiles();

	dd_redraw(0);

	return 0;
}
