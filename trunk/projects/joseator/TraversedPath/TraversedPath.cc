/*****************************************************************
 *  Project    : Learning and adaptation, SURF 2006               *
 *  File       : TravesedPath.cc                                  *
 *  Description: Program receives starting and destination        *
 *               checkpoints via message type SNrndfCheckpoint.   *
 *               The program then searches for a path in          *
 *               ../joseator/Learner/savedPaths file. If a path   *
 *               exists, it outputs the path, distance (m)        *
 *               and time it took to traverse the path (sec)       *
 *  Author     : Jose Torres                                      *
 *  Modified   : 7-18-06                                          *
 *****************************************************************/      

#include "TraversedPath.hh"

// Constructor
TraversedPath::TraversedPath(double northing, double easting, int skynet_key) :
  CSkynetContainer(SNastate, skynet_key)
{
    
} //end constructor



//destructor
TraversedPath::~TraversedPath()  
{
  
} //end destructor



void TraversedPath::getCheckpoint()
{
  //Set up socket for skynet broadcasting:
  int checkPointSock = m_skynet.listen(SNrndfCheckpoint, ALLMODULES);
  cout<< checkPointSock <<endl;
  if (checkPointSock < 0)
    {
      cerr << "TraversedPath::TraversedPath(): skynet listen returned error" << endl;
    }
  Checkpoint my_command;
  
  while(1)
    {
      //cout<<"about to attempt to get checkpoint" <<endl;
      if(m_skynet.get_msg(checkPointSock, &my_command, sizeof(my_command), 0) != sizeof(my_command))
	cout << "Didn't receive the right number of bytes in the state structure" << endl;
      //cout<< "received checkpoint " << my_command.x <<" " <<my_command.y <<endl;
      searchFile(my_command.startX, my_command.startY, my_command.endX, my_command.endY);
      DGCusleep(1000000);
    } //end while

} //end getCheckpoint



bool TraversedPath::searchFile(double startX, double startY, double destinationX, double destinationY)
{
  double fileX, fileY, dummyVariable;
  ifstream inData;
  inData.open("../Learner/pathFiles/path1");
  if(!inData)
    {
      cout<< "ERROR, unable to open file ../Learner/savedPaths " <<endl;
    }
  cout<<fixed <<showpoint <<setprecision(6);
  cout<< "Looking for (" <<startX <<", " <<startY <<")  (" <<destinationX <<", " <<destinationY <<")" <<endl;
  while(!inData.eof())
    {
      inData >>fileX;
      inData >>fileY;
      inData>> dummyVariable;
      //cout<< fileX <<"=" <<startX <<" " <<fileY <<"=" <<startY  <<endl;

      if((abs(fileX - startX)< 0.1) && (abs(fileY - startY) < 0.1)) 
	{
	  cout<<"found first checkpoint" <<endl;
	  while(!inData.eof())
	    {
	      inData >>fileX;
	      inData >>fileY;
	      inData>> dummyVariable;
	      //cout<< fileX <<"=" <<destinationX <<" " <<fileY <<"=" <<destinationY  <<endl;
	      if((abs(fileX - destinationX)< 0.1) && (abs(fileY - destinationY) < 0.1)) 
		{
		  cout<<"found path to checkpoint" <<endl;
		  pathToCheckPoint(startX, startY, destinationX, destinationY);
		  inData.close();
		  return true;
		}
	    }
	}
	
    }
  cout<< "Unable to find path" <<endl <<endl;
  inData.close();
  return false;
  
} //end searchFile



void TraversedPath::pathToCheckPoint(double startX, double startY, double destinationX, double destinationY)
{
  double fileX, fileY;
  double previousX = startX;
  double previousY = startY;
  double startTimeStamp, endTimeStamp;
  double distance = 0;
  ifstream inData;
  inData.open("../Learner/pathFiles/path1");
  while(!inData.eof())
    {
      inData >>fileX;
      inData >>fileY;
      inData >>startTimeStamp;
      if((abs(fileX - startX)< 0.1) && (abs(fileY - startY) < 0.1)) 
	{
	  cout<< fileX <<" " <<fileY <<endl;
	  while(!inData.eof())
	    {
	      inData >>fileX;
	      inData >>fileY;
	      inData >>endTimeStamp;
	      cout<< fileX <<" " <<fileY <<endl;
	      distance = distance + (sqrt(pow(previousX - fileX,2) + pow(previousY - fileY,2)));
	      if((abs(fileX - destinationX)< 0.1) && (abs(fileY - destinationY) < 0.1)) 
		{
		  cout<<"Distance = " << distance <<" m" <<endl;
		  cout<< "Time = " <<  (endTimeStamp - startTimeStamp)/1000000 <<" sec" <<endl <<endl;
		  inData.close();
		  return;
		  
		}
	      previousX = fileX;
	      previousY = fileY;
	    } //end inner while
	}
    } //end outer while
} //end pathToCheckPoint


//General function that is modified to test portion of code
void TraversedPath::testFunction()
{
 
}



int main() 
{
  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  cout<<"got skynet key"<<endl;
  if ( pSkynetkey == NULL)
    {
      cerr << "SKYNET_KEY environment variable isn't set." << endl;
    } else
      {
	sn_key = atoi(pSkynetkey);
      }
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  TraversedPath ast(0.0, 0.0, sn_key);
  ast.getCheckpoint();
  //ast.searchFile(640669.909134, 3941147.885637, 640667.463950, 3941147.260091);
  return 0;
}    




