/****************************************************************
 *  Project    : Learning and adaptation, SURF 2006              *
 *  File       : TraversedPath.hh                                *
 *  Author     : Jose Torres                                     *
 *  Modified   : 6-23-06                                         *
 *****************************************************************/      

#ifndef TRAVERSEDPATH_HH
#define TRAVERSEDPATH_HH

#include "StateClient.h"
#include "SkynetContainer.h"
#include <iostream>
#include <cstring>
#include <iomanip>
#include <cmath>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <stdio.h>
#include <time.h>
#define GPS_CAPTURE_FREQUENCY 0.5
using namespace std;

/*! TraversedPath class receives starting and destination        
 *  checkpoints via message type SNrndfCheckpoint.   
 *  The program then searches for a path in          
 *  ../joseator/Learner/savedPaths file. If a path   
 *  exists, it outputs the path, distance (m)        
 *  and time it took to travere the path (sec)
 */
class TraversedPath : public CStateClient 
{  

private:
 
  //struct used to receive message type SNrndfCheckpoint
  struct Checkpoint
  {
    double startX;
    double startY;
    double endX;
    double endY;
  };
  
  
public:

  /*! constructor */
  TraversedPath(double northing, double easting, int skynet_key);
  
  /*! destructor  */
  ~TraversedPath();                                   
 
  /*! General function used to test portions of code */
  void testFunction();

  /*! Function listens for message type SNrndfCheckpoint 
  *   calls searchfile with checkpoints when message is 
  *   received */
  void getCheckpoint();
 
  /*! Function opens ../joseator/Learner/savedPaths file
  *   and determines if there exists a path that takes you 
  *   to the destination checkpoint */
  bool TraversedPath::searchFile(double startX, double startY, double destinationX, double destinationY);
  
  /*! Function prints the path that takes you to the destination
  *   checkpoint */
  void TraversedPath::pathToCheckPoint(double startX, double startY, double destinationX, double destinationY);
  
};

#endif
