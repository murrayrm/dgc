/*!****************************************************************
 *  Project    : Learning and adaptation                          *
 *  File       : Node.hh                                          *
 *  Description: First version of program that stores Alice's     *
 *               traversed paths. This file contains struct that  *
 *               is used to store individual positions of ALICE   *
 *  Author     : Jose Torres                                      *
 *  Modified   : 6-23-06                                          *
 *****************************************************************/ 

#ifndef NODE_HH
#define NODE_HH

struct Node
{
  int pathListLength;
  double northing;
  double easting;
  Node* location;
  Node* path;   
  Node* pathEnd; 
};

#endif
