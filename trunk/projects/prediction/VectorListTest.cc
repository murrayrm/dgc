#include "Vector.h"
#include <list>
#include <iostream>

using namespace std;

list<Vector> methodTest (list<Vector> listy)
{
  list<Vector> temp = listy;
  for(int i = 1; i <=5; i++)
    {
      Vector vectus (3);

      for(int c = 1; c <= 3; c++)
	{
	  vectus.setElement(c, 5*c);
	}
      temp.push_back(vectus);
    }

  cout<<"Printing contents of list 'temp'"<<endl<<endl;

  list<Vector>::const_iterator iter = temp.begin();

  for(int i = 1; i<= temp.size(); i++)
    {
      (*iter).print();
      cout<<endl<<endl;
      iter++;
    }

  return temp;
}

int main(void)
{
  list<Vector> listy;

  Vector vector1(3);

  vector1.setElement(1, 1);
  vector1.setElement(2, 2);
  vector1.setElement(3, 3);

  listy.push_back(vector1);

  vector1.setElement(2, 5);

  listy.push_back(vector1);


  cout<<"List 1 contains: "<<endl<<endl;
  list<Vector>::const_iterator iter = listy.begin();

  for(int i = 1; i<= 2; i++)
    {
      (*iter).print();
      cout<<endl<<endl;
      iter++;
    }

  list<Vector> listy2 = listy;

  iter = listy2.begin();

  cout<<"Copying list1 into list2.  List2 = "<<endl<<endl;

  for(int i = 1; i<= 2; i++)
    {
      (*iter).print();
      cout<<endl<<endl;
      iter++;
    }

  cout<<"Adding some elements to list2."<<endl;

  for(int i = 1; i <= 5; i++)
    {
      Vector vectus(3);

      for(int c = 1; c <= 3; c++)
	{
	  vectus.setElement(c, c);
	}
      listy2.push_back(vectus);
    }

  cout<<"List2 now contains: "<<endl<<endl;

  iter = listy2.begin();

  for(int i = 1; i<= listy2.size(); i++)
    {
      (*iter).print();
      cout<<endl<<endl;
      iter++;
    }

  cout<<"Assigning list2 to variable list3 after adding some stuff."<<endl<<endl;


  list<Vector> list3 = methodTest(listy2);

  cout<<"Printing contents of list3."<<endl<<endl;
  iter = list3.begin();

  for(int i = 1; i<= list3.size(); i++)
    {
      (*iter).print();
      cout<<endl<<endl;
      iter++;
    }

}
