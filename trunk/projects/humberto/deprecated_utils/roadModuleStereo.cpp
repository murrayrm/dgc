#include <stdio.h>
#include <stdlib.h>

#include "stereovision/stereoSource.hh"
#include "stereovision/stereoProcess.hh"
#include "frames/frames.hh"
#include "../../util/cMap/CMapPlus.hh"

#include <iostream>
#include <fstream>

#include <Magick++.h>
#include <Magick++.h>


/*! Simple stereovision process, loader and 3D point evaluation.*/

// To compile drag to PARENT folder and run > make roadModuleStereo 
// or > make test

// SURF 06
// Humberto Pereira: tallberto @ gmail.com

using namespace std;
using namespace Magick;

int main(int argc, char *argv[]) {

	stereoSource mySource;

	stereoProcess myProcess;
	int debug=0;
	
	ofstream dumpFile("rawDump.txt",ofstream::binary);
	
//	mySource.init(0, 640, 480, "/tmp/logs/2006_07_20/21_24_21/stereoFeeder/stereo_short", "bmp");
	mySource.init(0, 640, 480, "/home/tallberto/dgc/projects/humberto/images/stereo", "bmp");

	
	myProcess.init(0, "../../drivers/stereovision/config/stereovision/SVSCal.ini.short",
								 "../../drivers/stereovision/config/stereovision/SVSParams.ini.short",
								 "../../drivers/stereovision/config/stereovision/CamCal.ini.short",
								 "temp",
								 "bmp",
								 0,
								 "../../drivers/stereovision/config/stereovision/SunParams.ini.short",
								 "../../drivers/stereovision/config/stereovision/PointCutoffs.ini.short");


	VehicleState myState;
	NEDcoord myCoord;
	
	int numValidPixels;
	int returner;
	

// Save changes to underlying image .
  // Save updated image to file.


	CMapPlus stereoMap;
	stereoMap.initMap(0, 0, 600, 400, 0.1, 0.1, 0);

	int validLayer = stereoMap.addLayer<double>(0.0, 0.0);  		//existing points are 1, others 0
	int realLayer = stereoMap.addLayer<double>(0.0, -32.0);		//height
	int smoothLayer = stereoMap.addLayer<double>(0.0, -32.0);		
	int edgeLayer = stereoMap.addLayer<double>(0.0, -32.0);		//detected edges
	
	while(mySource.pairIndex()<2) {
		if(mySource.grab()) {
			numValidPixels=0;
			myProcess.loadPair(mySource.pair(), myState);
			myProcess.calcRect();
			myProcess.calcDisp();

			//from the left camera
			for(int x=0; x<640; x++) {
			   for(int y=0; y<410; y++) { //410 to ignore alice from bottom of image. rewrite with poligon
					if(myProcess.SinglePoint(x,y,&myCoord)) {
						numValidPixels++;
						returner = stereoMap.setDataUTM<int>(validLayer, myCoord.N, myCoord.E, 1);
						dumpFile << mySource.pairIndex() - 1 << ",x:," << x << ",y:," << y << ",PointNED,"<< myCoord.N << "," << myCoord.E << "," << myCoord.D << endl;
						if (debug) cout << "FrameNum: " << mySource.pairIndex() - 1 << " " << x << ", " << y << " Point: " << myCoord.N << ", " << myCoord.E << ", " << myCoord.D << endl;
					}
				}
			}
			cout << "FrameNum: " << mySource.pairIndex() - 1 << " " << numValidPixels << " valid pixels"  << endl;
		} else {
			if(debug && mySource.pairIndex()%100 == 0) cout << "Frame: " << mySource.pairIndex() << endl;
		}
 	}
  dumpFile.close();
  return 0;
}

