#!/bin/bash

USER=henrik
BUILDSTRING="cd dgc && svn up && make install && cd bob && make && echo BUILD SUCCESSFUL!"

DEFAULT_COMPUTERS="cobalt titanium tantalum magnesium iridium osmium"

# if argument given, set the computers to that argument instead of the default
if [ $1 ]; then
  COMPUTERS=$1
else
  COMPUTERS=$DEFAULT_COMPUTERS
fi

echo "Build script for race computers"
echo "==============================="
echo "Currently configured to build for user: $USER (edit script to change)"
echo "Does: svn up and builds the tree on these computers by "
echo "default: $DEFAULT_COMPUTERS"
echo
echo "Optional switch:"
echo "computernames : build on space delimited computer names (must be surrounded by brackets)"
echo "                example: $0 \"osmium titanium\" "
echo
echo "Computers to build on this run: $COMPUTERS"
echo 
echo "IMPORTANT: Remember that the RDDF file will go back to the default (see dgc/bob/RDDF/Makefile)"
echo "           So if you used a custom one, be sure to update it again!"


for comp in $COMPUTERS
do
  xterm -sl 9999 -geometry 100x30-500-300 -e "echo $comp: building...; \
  ssh $USER@$comp '$BUILDSTRING'; \
  echo; echo; echo -n $comp: build finished. Press enter to exit; \
  read foo" &
done

exit 0
