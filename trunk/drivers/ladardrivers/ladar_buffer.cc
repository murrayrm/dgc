#include "ladar_buffer.hh"

CLadarDataBuffer::CLadarDataBuffer(){
	frame_size = 0;
	c_frame = 0;
	c_frame_offset = 0;
	last_frame_sent = -1;
	
	memset(frames, 0, LADAR_BUF_SIZE*sizeof(frames[0]));
}

CLadarDataBuffer::~CLadarDataBuffer() {
	DeInit();	
}

void CLadarDataBuffer::DeInit() {
	int i;

	frame_size = 0;
	c_frame = 0;
	c_frame_offset = 0;
	last_frame_sent = -1;
	
	for ( i = 0 ; i < LADAR_BUF_SIZE ; i++) {
		if (frames[i] != NULL)
		{
			delete[] frames[i];
			frames[i] = NULL;
		}
	}
}

void CLadarDataBuffer::Init(int new_fsize) {
	int i;
	frame_size = new_fsize;
	c_frame = 0;
	c_frame_offset = 0; 
	last_frame_sent = -1;
	for ( i = 0 ; i < LADAR_BUF_SIZE ; i++) {
		frames[i] = new char[new_fsize];
	}
}

int CLadarDataBuffer::_NextFrame() {
	if (IsFull()){
		printf("Cant add frame\n");
		return 0;
	}

	c_frame++;
	c_frame %= LADAR_BUF_SIZE;

	c_frame_offset = 0; 

	return 1;
}

int CLadarDataBuffer::FillMsg(char *msg, int size, FILE *log) { 
	int msg_offset = 0;
	if (size <= 0)
		return 0;

	if (log != NULL) 
		fwrite(msg, sizeof(msg[0]), size, log);		

	if (size <= (frame_size - c_frame_offset )) {
		if (IsFull())
		{
			printf("Cant add frame\n");
			return 0;
		}

		memcpy(frames[c_frame] + c_frame_offset, msg, size);
		c_frame_offset+=size;
		
		if (c_frame_offset == frame_size) 
		{
			_NextFrame();
			return LADAR_BUF_FILL_MSG;
		}
		return 1;
	}
	else {
		//fill the last part of the previous frame
		memcpy(frames[c_frame] + c_frame_offset, msg, frame_size - c_frame_offset);
		msg_offset = frame_size - c_frame_offset;
		//switch frames
		if (!_NextFrame())
			return 0; //cant switch frames 

		memcpy(frames[c_frame], msg + msg_offset, size - msg_offset);
		c_frame_offset+= (size - msg_offset);

		return LADAR_BUF_FILL_FRAME;
	}
		

	return 1;
}

char *CLadarDataBuffer::GetFrame() {
	if (!IsEmpty()) {
		last_frame_sent++;
		last_frame_sent %= LADAR_BUF_SIZE;
		return frames[last_frame_sent];
	}

	fprintf(stderr, "CLadarDataBuffer::GetFrame() called with an empty buffer. check your mutices %d %d %d\n", num_frames(), last_frame_sent, c_frame );
	return NULL;
}

int CLadarDataBuffer::IsFull() { 
  //Assure that we have 1 empty frame always, because othrewise recieved message from riegl might 
  //not be successfully processed and this can result in loss of data due to lack of space(riegl packets are arbitrary size)

  //Why it's actually minus 2: numframes are now compuited from
  //c_frame and last_frame_sent. This causes a full buffer to look
  //exactly the same as an empty buffer. An extra padding frame takes
  //care of the problem
	return (num_frames() == LADAR_BUF_SIZE -2);
}

int CLadarDataBuffer::IsEmpty() {
	return num_frames() == 0;
}

void CLadarDataBuffer::Empty() {
	int c_fsize = frame_size;
	DeInit();
	Init(c_fsize);
}

void CLadarDataBuffer::LogAllFrames(FILE *log) {
	int curr_fr = last_frame_sent;
	int i;

	for (i = 0; i < num_frames()+1; i++) {
		curr_fr = (curr_fr+1)%LADAR_BUF_SIZE;

		if (curr_fr != c_frame) {
			fwrite( frames[curr_fr], sizeof(frames[curr_fr][0]), frame_size, log);		
		}
		else {
			if (c_frame_offset != 0)
				fwrite( frames[curr_fr], sizeof(frames[curr_fr][0]), c_frame_offset, log);		
		}
	}
		
}
