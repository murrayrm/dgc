#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <iostream.h>
#include <rddf.hh>
#include <CMap/CMapPlus.hh>
#include "../../MTA/Kernel/Misc/Time/Timeval.hh"
#include "CCorridorPainter.hh"

int main (int argc,char* argv[]){
  //system("clear");
  int ind, total,ret, waypointNumber;
  RDDF rddf;
  RDDFVector targetPoints;
  char * bobfile;
  double Northing,Easting;
  CMapPlus myMap;
  //CMap myMap;
  CCorridorPainter myPainter;
  int numRows=700;
  int numCols = 700;
  double resRows =3;
  double resCols =3;
  Timeval before, after;

  myMap.initMap(0, 0, numRows, numCols, resRows, resCols, 0);
  //myMap.initMap(3846508.753+375, 499015.283-250, numRows, numCols, resRows, resCols, 0);
  //myMap.initMap(3846508.753, 499015.28, numRows, numCols, resRows, resCols, 0);

  printf("Game\n");

  int rddfLayerID = myMap.addLayer<int>(CP_OUTSIDE_CORRIDOR, CP_OUTSIDE_MAP);

  myPainter.initPainter(&myMap, rddfLayerID, &rddf);

  RDDFVector tempVector = rddf.getTargetPoints();

  //myMap.initLayerDisplay(rddfLayerID);
  myMap.initLayerDisplay(rddfLayerID, 0, 1, tempVector[0].Northing, tempVector[0].Easting, 20.0);
  myMap.startLayerDisplay(rddfLayerID);


  printf("There are %d points\n", rddf.getNumTargetPoints());

  printf("At %lf, %lf\n", tempVector[0].Northing, tempVector[0].Easting);

  //myMap.updateVehicleLoc(3846508.753+375, 499015.283-250);
  myMap.updateVehicleLoc(tempVector[0].Northing, tempVector[0].Easting);
  //myMap.updateVehicleLoc(3846508.753+0, 499015.283-0);
  before = TVNow();
  printf("Crashnburn\n");
  myPainter.paintChanges();
  printf("Crashnburn\n");
  after = TVNow();

  printf("Took %lf\n", (after-before).dbl());
  myMap.refreshLayerDisplay<int>(rddfLayerID);
  sleep(5);
  double change=10.0;
  double tempN, tempE;
  for(int i=1; i<10; i++) {
    myMap.updateVehicleLoc(tempVector[0].Northing+change*i, tempVector[0].Easting-change*0);
    rddf.getPointOnTrackLineDist(myMap.getVehLocUTMNorthing(), myMap.getVehLocUTMEasting(), 50,
				 tempN, tempE);
    printf("At %lf, %lf\n", tempN, tempE);
    //myMap.setDataUTM<int>(rddfLayerID, tempN, tempE, 0);
    //myMap.updateVehicleLoc(3846508.753+0+2.5*i, 499015.283+50-2.5*i);
    before = TVNow();
    myPainter.paintChanges();
    after = TVNow();
    for(int j=0; j<rddf.getNumTargetPoints(); j++) {
      printf("%d: %lf, %lf\n", j,
	     rddf.getWaypointNorthing(j)-rddf.getWaypointNorthing(0),
	     rddf.getWaypointEasting(j)-rddf.getWaypointEasting(0));
      myMap.setDataUTM<int>(rddfLayerID, rddf.getWaypointNorthing(j),
			    rddf.getWaypointEasting(j), 0);
    }
    myMap.refreshLayerDisplay<int>(rddfLayerID);
    printf("Took %lf\n", (after-before).dbl());
    sleep(7);
  }
  /*
  for(int row=0; row<numRows; row++) {
    for(int col=0; col<numCols; col++) {
      double tempVal = myMap.getDataWin<double>(rddfLayerID, row, col);
      myMap.Win2UTM(row, col, &Northing, &Easting);
      myMap.setDataWin<double>(rddfLayerID, row, col, rddf.isPointInCorridor(Northing, Easting));
      //tempVal = rddf.isPointInCorridor(Northing, Easting);
    }
  }
  */

  /*
  double blN, blE, trN, trE;
  double wblN, wblE, wtrN, wtrE;
  int blR, blC, trR, trC;
  int r, c;
  double tempa, tempb;
  int retval, fout, bout;
  Timeval before, after;

  wblN = myMap.getWindowBottomLeftUTMNorthing();
  wblE = myMap.getWindowBottomLeftUTMEasting();
  wtrN = myMap.getWindowTopRightUTMNorthing() - myMap.getResRows()/2;
  wtrE = myMap.getWindowTopRightUTMEasting() - myMap.getResCols()/2;

  before = TVNow();

  fout = rddf.getWaypointNumAheadOut(20, wblN, wblE, wtrN, wtrE);
  bout = rddf.getWaypointNumBehindOut(20, wblN, wblE, wtrN, wtrE);
  //printf("%d is next, %d is fout, %d is bout\n", retval, fout, bout);


  for(int i=bout; i<=fout; i++) {
    retval = rddf.getCorridorSegmentBoundingBoxUTM(i, wblN, wblE, wtrN, wtrE,
					  blN, blE, trN, trE);
    myMap.UTM2Win(blN, blE, &blR, &blC);
    myMap.UTM2Win(trN, trE, &trR, &trC);
    if(retval >= 0) {
      //printf("%d (is a %d) goes from (%d, %d) to (%d, %d)\n",
      //i, retval, blR, blC, trR, trC);
      for(int row=blR; row <= trR; row++) {
	for(int col=blC; col <= trC; col++) {
	  myMap.Win2UTM(row, col, &Northing, &Easting);
	  if(myMap.getDataWin<double>(rddfLayerID, row, col) != 1) {
	    //if(rddf.isPointInCorridor(i, Northing, Easting)) {
	      myMap.setDataWin<double>(rddfLayerID, row, col,
				       rddf.isPointInCorridor(i, Northing, Easting));
	      //}
	  }
	}
      }
      //tempa = rddf.getWaypointNorthing(i);
      //tempb = rddf.getWaypointEasting(i);
      //myMap.setDataUTM<double>(rddfLayerID, tempa, tempb, 1.0);
    }
    //printf("%d: (%lf, %lf) to (%lf, %lf)\n", i, blN, blE, trN, trE);
    //myMap.setDataUTM<double>(rddfLayerID, blN, blE, -1.0);
    //myMap.setDataUTM<double>(rddfLayerID, trN, trE, -1.0);
  }
  after = TVNow();
  printf("Took %lf\n", (after-before).dbl());

  before = TVNow();
  rddf.getCurrentWaypointNumber(0,0);
  after = TVNow();
  printf("Took %lf\n", (after-before).dbl());


  myMap.refreshLayerDisplay<double>(rddfLayerID);
  sleep(15);

  myMap.updateVehicleLoc(3846508.753-10, 499015.283+10);

  long int origBLRow, origBLCol, newBLRow, newBLCol;
  origBLRow = myMap.getWindowBottomLeftUTMNorthingRowResMultiple();
  origBLCol = myMap.getWindowBottomLeftUTMEastingColResMultiple();

  myMap.updateVehicleLoc(3846508.753, 499015.283);

  //myMap.refreshLayerDisplay<double>(rddfLayerID);
  //sleep(15);


  newBLRow = myMap.getWindowBottomLeftUTMNorthingRowResMultiple();
  newBLCol = myMap.getWindowBottomLeftUTMEastingColResMultiple();

  int deltaRow, deltaCol;
  deltaRow = newBLRow - origBLRow;
  deltaCol = newBLCol - origBLCol;
  printf("Delta %d, %d\n", deltaRow, deltaCol);

  int rbBLr, rbBLc, rbTRr, rbTRc;
  int cbBLr, cbBLc, cbTRr, cbTRc;
  int bbBLr, bbBLc, bbTRr, bbTRc;

  if(deltaRow>=0) {
    //we moved north, empty area is at top
    bbBLr = myMap.getNumRows()-deltaRow;
    bbTRr = myMap.getNumRows() - 1;

    rbBLr = myMap.getNumRows() - deltaRow;
    rbTRr = myMap.getNumRows() - 1;
    cbBLr = 0;
    cbTRr = myMap.getNumRows() - deltaRow;
  } else {
    //moved south, empty at bottom
    bbBLr = 0;
    bbBLr = -1*deltaRow;
    rbBLr = 0;
    rbTRr = -1*deltaRow;
    cbBLr = -1*deltaRow;
    cbTRr = myMap.getNumRows() -1;
  }

  if(deltaCol>=0) {
    //we moved east, empty area is at right
    bbBLc = myMap.getNumCols() - deltaCol;
    bbTRc = myMap.getNumCols() -1;

    cbBLc = myMap.getNumCols() - deltaCol;
    cbTRc = myMap.getNumCols() - 1;
    rbBLc = 0;
    rbTRc = myMap.getNumCols() - deltaCol;
  } else {
    //moved west, empty at left
    bbBLc = 0;
    bbTRc = -1*deltaCol;

    cbBLc = 0;
    cbTRc = -1*deltaCol;
    rbBLc = -1*deltaCol;
    rbTRc = myMap.getNumCols()-1;
  }

  printf("(%d, %d) to (%d, %d)\n", rbBLr, rbBLc, rbTRr, rbTRc);

  //for(int r=rbBLr; r <= rbTRr; r++) {
  //for(int c = rbBLc; c<=rbTRc; c++) {
      //myMap.setDataWin<double>(rddfLayerID, r, c, -0.25);

      //wblN = myMap.getWindowBottomLeftUTMNorthing();
      //wblE = myMap.getWindowBottomLeftUTMEasting();
      //wtrN = myMap.getWindowTopRightUTMNorthing() - myMap.getResRows()/2;
      //wtrE = myMap.getWindowTopRightUTMEasting() - myMap.getResCols()/2;

      myMap.Win2UTM(rbBLr, rbBLc, &wblN, &wblE);
      myMap.Win2UTM(rbTRr, rbTRc, &wtrN, &wtrE);
      
      fout = rddf.getWaypointNumAheadOut(20, wblN, wblE, wtrN, wtrE);
      bout = rddf.getWaypointNumBehindOut(20, wblN, wblE, wtrN, wtrE);
      printf("%d is next, %d is fout, %d is bout\n", retval, fout, bout);
      
      
      for(int i=bout; i<=fout; i++) {
	retval = rddf.getCorridorSegmentBoundingBoxUTM(i, wblN, wblE, wtrN, wtrE,
						       blN, blE, trN, trE);
	myMap.UTM2Win(blN, blE, &blR, &blC);
	myMap.UTM2Win(trN, trE, &trR, &trC);
	if(retval >= 0) {
	  printf("%d (is a %d) goes from (%d, %d) to (%d, %d)\n",
		 i, retval, blR, blC, trR, trC);
	  for(int row=blR; row <= trR; row++) {
	    for(int col=blC; col <= trC; col++) {
	      myMap.Win2UTM(row, col, &Northing, &Easting);
	      if(myMap.getDataWin<double>(rddfLayerID, row, col) != 1) {
		//if(rddf.isPointInCorridor(i, Northing, Easting)) {
		myMap.setDataWin<double>(rddfLayerID, row, col,
					 rddf.isPointInCorridor(i, Northing, Easting));
		//}
	      }
	    }
	  }
	  //tempa = rddf.getWaypointNorthing(i);
	  //tempb = rddf.getWaypointEasting(i);
	  //myMap.setDataUTM<double>(rddfLayerID, tempa, tempb, 1.0);
	}
	//printf("%d: (%lf, %lf) to (%lf, %lf)\n", i, blN, blE, trN, trE);
	//myMap.setDataUTM<double>(rddfLayerID, blN, blE, -1.0);
	//myMap.setDataUTM<double>(rddfLayerID, trN, trE, -1.0);
      }






      
      //}
      //}


  for(int r=cbBLr; r <= cbTRr; r++) {
    for(int c = cbBLc; c<=cbTRc; c++) {
      myMap.setDataWin<double>(rddfLayerID, r, c, -0.5);
    }
  }

  for(int r=bbBLr; r <= bbTRr; r++) {
    for(int c = bbBLc; c<=bbTRc; c++) {
      myMap.setDataWin<double>(rddfLayerID, r, c, -0.75);
    }
  }
  */

  printf("Set\n");
  myMap.refreshLayerDisplay<int>(rddfLayerID);
  sleep(30);

    int imageRow=0;
    int imageCol=0;
    int winRow, winCol;
    double minDouble, maxDouble, valueDouble;
    char valueChar;
    minDouble = 0;
    maxDouble = 0;
    double dptr;

    int _numRows = myMap.getNumRows();
    int _numCols = myMap.getNumCols();

    char *imageBuffer;

    imageBuffer = (char*)malloc(myMap.getNumRows()*myMap.getNumCols());

    for(winRow = 0; winRow < _numRows; winRow++) {
      for(winCol = 0; winCol < _numCols; winCol++) {
	dptr = myMap.getDataWin<double>(rddfLayerID, winRow, winCol);
	if(dptr != -2) {
	  valueDouble = dptr;
	  if(valueDouble < minDouble) minDouble = valueDouble;
	  if(valueDouble > maxDouble) maxDouble = valueDouble;
	}
      }
    }

    printf("MaxDouble: %d, MinDouble: %d\n", maxDouble, minDouble);
    double newValueDouble;
    imageRow = myMap.getNumRows()-1;
    for(winRow = 0; winRow < _numRows; winRow++) {
      imageCol = 0;
      for(winCol = 0; winCol < _numCols; winCol++) {
	valueDouble = 0;
	dptr = myMap.getDataWin<double>(rddfLayerID, winRow, winCol);
	if(dptr != -2) valueDouble = dptr;
	newValueDouble = (((valueDouble-minDouble)*255/(maxDouble-minDouble)));
	valueChar = (char)(int)newValueDouble;
	imageBuffer[imageRow*_numCols + imageCol] = valueChar;
	imageCol++;
      }
      imageRow--;
    }


    FILE* imagefile;
    imagefile=fopen("map.pgm", "w");
    if( imagefile == NULL) {
      fprintf( stderr, "Can't create '%s'", "map.pgm");
    } else {
      fprintf(imagefile,"P5\n%u %u 255\n", _numRows, _numCols);
      fwrite((const char *)imageBuffer, 1, _numRows*_numCols, imagefile);
      fclose(imagefile);
    }



 
  /*************************************************************************/
  /********** DECLARING VARIABLES ******************************************/
  /*************************************************************************/
  // Variales used to get parameters from command line
  int opt, outOfMemory;
  char *argMethod;
  int method,param;

  /*************************************************************************
   ********** GETTING THE PARAMETERS FROM COMMAND LINE			
   **********  CAUTION:						  
   **********    - optind,opterr:  extern variables, do not declare a
   **********	         local variable with any of these names	     
   **********	 - optind: indice of the argv where to find the argument 
   **********		  desired					    
   *************************************************************************/

  // Defining the characters used as delimiter of a command line argument  
  param = 0;
  outOfMemory = 0;
  while ((opt = getopt (argc, argv, "m:")) != -1){
    switch ((unsigned char)opt){
    case 'm':
      param = 1;
      if(optarg != NULL){
	argMethod = (char*) malloc(strlen(optarg)+1);
	if(argMethod == NULL)
	  outOfMemory = 1;
	else
	  strcpy(argMethod,optarg);
      }
      break;
    case '?':
    default:
      system("clear");
      cout << "usage: [-m method]" << endl << endl
           << "1 : load RDDF file test" << endl
           << "2 : create BOB file" << endl
           << "3 : getNextWaypoint" << endl << endl;
      return(-1);
    }    
  }
  
  // If any allocation failed
  if(outOfMemory){
    if(argMethod != NULL) free(argMethod);
    system("clear");
    cout << "Problems with memory management" << endl;
    return(-1);
  }
  
  // Showing the choosen test
  if(param)
    method = atoi(argMethod);
  else
    method = 4;

  switch(method){
  case 1:

    //**********************************************//
    // Testing constructor
    //**********************************************//

    targetPoints = rddf.getTargetPoints();
    total = rddf.getNumTargetPoints();
    for(ind=0; ind<total; ind++){
      targetPoints[ind].display();
    }

    break;

  case 2:

    //**********************************************//
    // create BOB file
    //**********************************************//

    targetPoints = rddf.getTargetPoints();
    total = rddf.getNumTargetPoints();
    ret = rddf.createBobFile("rddf.bob");

    break;

  case 3:

    //**********************************************//
    // getNextWaypoint
    //**********************************************//
    waypointNumber = 0;
    cout << "NEXT WAYPOINT = " << rddf.getNextWaypointNumber(Northing,Easting,waypointNumber) << endl;
    break;

  default:
    cout << "Wrong choice !!" << endl;
  }
  
  return(0);
}
