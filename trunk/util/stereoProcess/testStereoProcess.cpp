#include <stdio.h>
#include <stdlib.h>

#include "stereovision/stereoSource.hh"
#include "stereovision/stereoProcess.hh"
#include "frames/frames.hh"

#include <iostream>

int main(int argc, char *argv[]) {

	stereoSource mySource;
	stereoProcess myProcess;

	mySource.init(0, 640, 480, "/tmp/logs/2006_07_20/21_24_21/stereoFeeder/stereo_short", "bmp");

	myProcess.init(0, "../../drivers/stereovision/config/stereovision/SVSCal.ini.short",
								 "../../drivers/stereovision/config/stereovision/SVSParams.ini.short",
								 "../../drivers/stereovision/config/stereovision/CamCal.ini.short",
								 "temp",
								 "bmp",
								 0,
								 "../../drivers/stereovision/config/stereovision/SunParams.ini.short",
								 "../../drivers/stereovision/config/stereovision/PointCutoffs.ini.short");

	VehicleState myState;
	NEDcoord myCoord;

	int frameNum;

	while(mySource.pairIndex()<1500) {
		if(mySource.grab()) {
			myProcess.loadPair(mySource.pair(), myState);
			myProcess.calcRect();
			myProcess.calcDisp();
			for(int x=0; x<640; x++) {
				for(int y=0; y<480; y++) {
					if(myProcess.SinglePoint(x,y,&myCoord)) {
						cout << "FrameNum: " << mySource.pairIndex() << " " << x << ", " << y << " Point: " << myCoord.N << ", " << myCoord.E << ", " << myCoord.D << endl;
						x=641;
						y=481;
					}
				}
			}
		} else {
			if(mySource.pairIndex()%100 == 0) cout << "Frame: " << mySource.pairIndex() << endl;
		}
 	}
  
  return 0;
}

