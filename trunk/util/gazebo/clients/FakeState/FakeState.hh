/* FakeState.hh
 *
 * JML 31 Dec 04
 *
 */

#ifndef FAKESTATE_HH
#define FAKESTATE_HH

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "VehicleState.hh"
#include "SensorConstants.h"
#include "sn_msg.hh"
#include "sn_types.h"
#include "DGCutils"
#include "playerclient.h"

class FakeState
{
    skynet m_skynet;

    int lastIndex;

    // These structs used for messaging purposes
    struct VehicleState vehiclestate;

    unsigned long long starttime;
    unsigned long long dgctime;
    unsigned long long dgctimeold;

    //perpetual UpdateState variables?
    double timediff;

    int broadcast_statesock;

public:
  FakeState(int, int, int, int, int, int);
  
  void UpdateState();
  void printVehicleState();
  void Broadcast();
  
  // Keep track of our initial coordinates
  double Northing_Offset;
  double Easting_Offset;
  
private:
  PlayerClient* robot;
  GpsProxy* GPSpos;
  Position3DProxy* RobotPos;

};

// This is now defined in VehicleState.hh
/* New Clean struct for Vehicle State Messages */

#endif
