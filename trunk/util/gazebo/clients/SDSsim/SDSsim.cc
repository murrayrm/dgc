#include <sys/types.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <time.h>
#include <math.h>
#include "playerclient.h"
#include "constants.h"
#include "string.h"

#define ERROR 0
#define MAX_MSG 200

static double BrakePosition = 0;
static double ThrottlePosition = 0;
static double OldSteerPosition = 0;
static double GoalSteerPosition = 0;
static double SteerPosition = 0; //angle of wheels in degrees. 0 is oriented straight ahead, positive is to the right

struct timeval  StartedMovingSteer;
struct timezone tz;

static int GoCommanded = 0;
static double MaxTurnRate = 6000000 / 0.8; //motor counts per second was 60000 / 0.8
static double MaxSteerAngle = 30; //(degrees)

PlayerClient* robot = new PlayerClient("localhost");
PositionProxy* SDSpos = new PositionProxy(robot, 0, 'w');

void parse(char* message)
{
  int i,s;
  s = strlen(message);
  for(i=0;i<s;i++)
    if(message[i] == '\n')
      message[i] = '\0';
}

int execute_brake(char* message)
{
  //  printf("Brake Command Received: %s\n", message);
  parse(message);
  if(message[0] == 'C')
    {
      double position = atoi(message+1);
      position = position/DGC_ALICE_BRAKE_MAXPOS;
      BrakePosition = position;
      SDSpos->SetSpeed(ThrottlePosition, BrakePosition, SteerPosition);
      strcpy(message, "");
      //sprintf(message,"%f", position);
      //send to the player
    }
  else if(message[0]=='R')
    {
	//get position .. which should be a double from 0 to 1
      double position = BrakePosition;
      position = position * DGC_ALICE_BRAKE_MAXPOS;
      char brake_position_string[3];
      if(position < 10) {
	brake_position_string[0] = '0';
	sprintf(brake_position_string+1,"%d",(int)position);
      }
      else {
	sprintf(brake_position_string, "%d",(int)position);
      }
      strcpy(message, brake_position_string);
      message[2] = '\0';
      //send brake_position_string to the port
    }
  else if(message[0]=='P')
    {
      strcpy(message, "125");
      //return a pressure from 000 to 255
    }
  else 
    {
      strcpy(message,"INVALID\n");
      return 0;
    }
  return 1;
  
}

int execute_throttle(char *message)
{
  parse(message);

  //printf("parsed message0 %s\n",message);
  if((strlen(message) == 5)&&(message[0] == 'B')&&(message[4] == 'E'))
    {
      double apps1_read = (unsigned char) (APPS1_IDLE - (unsigned char) message[1]);
      apps1_read = apps1_read/APPS1_RANGE;
      double apps2_read = ((unsigned char) message[2] - APPS2_IDLE);
      apps2_read = apps2_read/APPS2_RANGE;
      double apps3_read = ((unsigned char) message[3] - APPS3_IDLE);
      apps3_read = apps3_read/APPS3_RANGE;
      double throttle_position_read  = (apps1_read + apps2_read + apps3_read) / 3;
      
      if (throttle_position_read < 0) throttle_position_read = 0;   //normalize to within 0-1 range
      if (throttle_position_read > 1) throttle_position_read = 1;
      //send position to player;
      ThrottlePosition = throttle_position_read;

      SDSpos->SetSpeed(ThrottlePosition, BrakePosition, SteerPosition);

      strcpy(message, "");
      //printf("parsed message1 %s\n",message);
    }
  else if(message[0]=='S')
    {
      double position = ThrottlePosition;
      //get position from player ..the position should be a double from 0 to 1
      
      char throttle_output[4];
      if(position < THROTTLE_IDLE) position = THROTTLE_IDLE;
      if(position > THROTTLE_MAX) position = THROTTLE_MAX;
      double throttle_position = position;
      
      unsigned char apps1_output = (unsigned char)(APPS1_IDLE - (int)(throttle_position * APPS1_RANGE));   
      unsigned char apps2_output = (unsigned char)(APPS2_IDLE + (throttle_position * APPS2_RANGE));
      unsigned char apps3_output = (unsigned char)(APPS3_IDLE + (throttle_position * APPS3_RANGE));

      throttle_output[0] =  apps1_output;
      throttle_output[1] =  apps2_output;
      throttle_output[2] =  apps3_output;
      throttle_output[3] = '\0';    
      
      strcpy(message, throttle_output);
      
      //send throttle_output to the serial port
    }else {
      strcpy(message,"INVALID\n");
      return 1;}
  return 1;
}

double get_Current_steer()
{
  double position;
  int sign;
  double timePassed; //in seconds

  struct timeval now;
  struct timezone zone;

  if (GoCommanded == 0)
    {
      position = OldSteerPosition;
    }
  else
    {
      if (SteerPosition > OldSteerPosition)
	{sign = 1;}
      else
	{sign = -1;}
      gettimeofday(&now, &zone);
      timePassed = (double)(now.tv_sec - StartedMovingSteer.tv_sec);
      timePassed += (double)(now.tv_usec - StartedMovingSteer.tv_usec) / 1000000;
      if (timePassed * MaxTurnRate >= sign * (SteerPosition - OldSteerPosition) * 30000 / MaxSteerAngle)
	{
	  //printf("max turn rate NOT exceeded\n");
	  position = SteerPosition;
	  OldSteerPosition = SteerPosition;
	  GoCommanded = 0;
	}
      else
	{
	  //printf("max turn rate exceeded\n");
	  position = sign * timePassed * MaxTurnRate / 30000 * MaxSteerAngle + OldSteerPosition;
	}
    }
  return position;
}

int execute_steer(char *message)
{
  //printf("Steer Command Received: %s\n", message);
  parse(message);
  if(strlen(message)>2 && message[0] == '!' && message[1] == 'D')
    {
      double position = atoi(message+2);
      printf("Set position: %d\n", (int)position);
      position = position / -30000;
      if (position < -1) position = -1;   //normalize to within 0-1 range
      if (position > 1) position = 1;

      GoalSteerPosition = position * MaxSteerAngle;
      strcpy(message, "");
      
      // added by chris
      OldSteerPosition = get_Current_steer();
      SteerPosition = GoalSteerPosition;      
      SDSpos->SetSpeed(ThrottlePosition, BrakePosition, SteerPosition);
      printf("Commanded SteerPosition: %d\n", (int)SteerPosition);
      //printf("Turn rate is: %d\n",SDSpos->TurnRate());
      GoCommanded = 1;
      strcpy(message, "");
      gettimeofday(&StartedMovingSteer, &tz);
    }
  else if(strlen(message) == 4 && message[0] == '!' && message[1] == 'T' && message[2] == 'P' && message[3] =='E')
    {
      double position;

      //hard right : -30000
      //hard left : 30000

      //position = get_Current_steer();
      //position = position * -30000 / MaxSteerAngle;
      //message[0] = '\0';
      sprintf(message, "TPE%d\n",(int)position);
    }
  else if(strlen(message) == 3 && message[0] == '!' && message[1] == 'G' && message[2] == 'O')
    {
      OldSteerPosition = get_Current_steer();
      SteerPosition = GoalSteerPosition;      
      //SDSpos->SetSpeed(ThrottlePosition, BrakePosition, SteerPosition);
      //printf("Commanded SteerPosition: %d\n", (int)SteerPosition);
      GoCommanded = 1;
      strcpy(message, "");
      gettimeofday(&StartedMovingSteer, &tz);
    }
  else if(strlen(message) == 2 && message[0] == 'G' && message[1] == 'O')
    {
      OldSteerPosition = get_Current_steer();
      SteerPosition = GoalSteerPosition;      
      SDSpos->SetSpeed(ThrottlePosition, BrakePosition, SteerPosition);
      GoCommanded = 1;
      strcpy(message, "");
      gettimeofday(&StartedMovingSteer, &tz);
    }
  else if(strlen(message) == 4 && message[0] == '!' && message[1] == 'T' && message[2] == 'A' && message[3] == 'S')
    {
      strcpy(message, "TAS0000_0000_0000_0000_0000_0000_0000_0000\n");
    }
  else if(strlen(message) == 5 && message[0] == 'D' && message[1] == 'R' && message[2] == 'I' && message[3] == 'V' && message[4] == 'E')
    {
      strcpy(message, "DRIVE1\n");
    }
  else {
      strcpy(message,"INVALID\n");
      return 0;
    }
  return 1;
}



int main (){
  
  int cliLen;
  int SteerSocket;               //All sockets
  int BrakeSocket;
  int ThrotSocket;
  int SteerConnectedSocket = -2;
  int BrakeConnectedSocket = -2;
  int ThrotConnectedSocket = -2;
  int select_value, bytes;       //keeps track of select return and message length
  int fd_max = 0;                //max socket number
  int i;
  int STEER_PORT = 8000;         //Port Number of Server
  int BRAKE_PORT = 8001;         //Port Number of Server
  int THROT_PORT = 8002;         //Port Number of Server

  fd_set fileDesc;               //File Descriptors
  fd_set copy_fileDesc;
  FD_ZERO(&fileDesc);            //Clear File Descriptors
  FD_ZERO(&copy_fileDesc);

  struct sockaddr_in cliAddr, steerAddr, brakeAddr, throtAddr;
  char line[MAX_MSG];

  //INITIALIZE STEERING PORT
  SteerSocket = socket(AF_INET, SOCK_STREAM, 0); /* create socket */
  if(SteerSocket<0) { perror("cannot open socket "); return ERROR; }
  steerAddr.sin_family = AF_INET; /* bind server port */
  steerAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  steerAddr.sin_port = htons(STEER_PORT);
  if(bind(SteerSocket, (struct sockaddr *) &steerAddr, sizeof(steerAddr))<0) { perror("cannot bind port "); return ERROR; }

  //INITIALIZE BRAKE PORT
  BrakeSocket = socket(AF_INET, SOCK_STREAM, 0); /* create socket */
  if(BrakeSocket<0) { perror("cannot open socket "); return ERROR; }
  brakeAddr.sin_family = AF_INET; /* bind server port */
  brakeAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  brakeAddr.sin_port = htons(BRAKE_PORT);
  if(bind(BrakeSocket, (struct sockaddr *) &brakeAddr, sizeof(brakeAddr))<0) { perror("cannot bind port "); return ERROR; }

  //INTITIALIZE THROTTLE PORT
  ThrotSocket = socket(AF_INET, SOCK_STREAM, 0); /* create socket */
  if(ThrotSocket<0) { perror("cannot open socket "); return ERROR; }
  throtAddr.sin_family = AF_INET; /* bind server port */
  throtAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  throtAddr.sin_port = htons(THROT_PORT);
  if(bind(ThrotSocket, (struct sockaddr *) &throtAddr, sizeof(throtAddr))<0) { perror("cannot bind port "); return ERROR; }

  //Listen for incoming connections and insert file descriptor
  listen(SteerSocket, 1);
  listen(BrakeSocket, 1);
  listen(ThrotSocket, 1);

  FD_SET(SteerSocket, &fileDesc);
  FD_SET(BrakeSocket, &fileDesc);
  FD_SET(ThrotSocket, &fileDesc);
  
  fd_max = SteerSocket;
  if (BrakeSocket > fd_max)
    {
      fd_max = BrakeSocket;
      }
  if (ThrotSocket > fd_max)
    {
      fd_max = ThrotSocket;
    }

  //notify which port listening on
  printf("Server Listening on Port %d (Steering)\n", STEER_PORT);
  printf("Server Listening on Port %d (Brake)\n", BRAKE_PORT);
  printf("Server Listening on Port %d (Throttle)\n", THROT_PORT);
  while(1) {
    // copy file descriptors so that nothing gets unintentionally set on select call
    usleep(150000);
    copy_fileDesc = fileDesc;
    //printf("Turnrate is: %d\n",SDSpos->TurnRate());
    //wait for incoming connection or message
    if((select_value = select(fd_max + 1, &copy_fileDesc, NULL, NULL, NULL)) == -1)
      {
	perror("Socket");
	return ERROR;
      }
    //testing loop to detect buffering by player/gazebo
    /*
    for(int i=0; i<10;i++)
      {
	usleep(10000000);
	printf("RIGHT\n");
	for(int j=0;j<50;j++)
	  {
	  //SDSpos->SetSpeed(0,0,30);
	  //SDSpos->SetSpeed(0,0,0);
	  //printf("right: %i\n",j);
	  }
        //printf("LEFT\n");
        //usleep(1000000);
        //SDSpos->SetSpeed(0,0,-30);
	printf("LEFT\n");
	//usleep(1000000);
      }
    */
    //printf("looping\n");
    
    //usleep(5000);
    for (i = 0; i <= fd_max; i++)
      //if incoming connection or message...
      if (FD_ISSET(i, &copy_fileDesc))
	{
	  //if new connection coming into ServerSocket, accept it
	  if (i == SteerSocket)
	    {
	      cliLen = sizeof(cliAddr);
	      if ((SteerConnectedSocket = accept(SteerSocket, (struct sockaddr *)&cliAddr,(socklen_t *) &cliLen)) == -1)
		{ 
		  perror("Could Not Accept Connection On Steering Port\n");
		  return ERROR;
		}
	      else
		{
		  //add to fd set
		  FD_SET(SteerConnectedSocket, &fileDesc);
		  if (SteerConnectedSocket > fd_max)
		    fd_max = SteerConnectedSocket;
		}
	      printf("Connected on Steer Port\n");
	    }
	  else if (i == SteerConnectedSocket)
	    {
	      //get incoming message from Steering
	      if ((bytes = recv(SteerConnectedSocket, line, sizeof(line), 0)) <= 0)
		{
		  if (bytes == 0)
		    printf("steering disconnected %d\n", SteerConnectedSocket);
		  else
		    perror("Connection error from steering");
		  close(SteerConnectedSocket);
		  if (SteerConnectedSocket == fd_max)
		    fd_max -= 1;
		  FD_CLR(SteerConnectedSocket, &fileDesc);
		  SteerConnectedSocket = -2;
		}
	      //otherwise parse message and do what's necessary
	      else
		{
		  line[bytes] = '\0';
		  execute_steer(line);

		  if (line[0] != '\0')
		    {
		      if (send(SteerConnectedSocket, line, sizeof(line), 0) == -1)
			{
			  perror("Cannot send to Client");
			  return ERROR;
			}
		      else
			{
			  //printf("Message Sent to Steering %d: %s\n", SteerConnectedSocket, line);
			}
		    }
		}
	    }


	  if (i == BrakeSocket)
	    {
	      cliLen = sizeof(cliAddr);
	      if ((BrakeConnectedSocket = accept(BrakeSocket, (struct sockaddr *)&cliAddr, (socklen_t *)&cliLen)) == -1)
		{ 
		  perror("Could Not Accept Connection On Brake Port\n");
		  return ERROR;
		}
	      else
		{
		  //add to fd set
		  FD_SET(BrakeConnectedSocket, &fileDesc);
		  if (BrakeConnectedSocket > fd_max)
		    fd_max = BrakeConnectedSocket;
		}
	      printf("Connected on Brake Port\n");
	    }
	  else if (i == BrakeConnectedSocket)
	    {

	      //get incoming message from Brake
	      if ((bytes = recv(BrakeConnectedSocket, line, sizeof(line), 0)) <= 0)
		{
		  if (bytes == 0)
		    printf("brake disconnected %d\n", BrakeConnectedSocket);
		  else
		    perror("Connection error from brake");
		  close(BrakeConnectedSocket);
		  if (BrakeConnectedSocket == fd_max)
		    fd_max -= 1;
		  FD_CLR(BrakeConnectedSocket, &fileDesc);
		  BrakeConnectedSocket = -2;
		}
	      //otherwise parse message and do what's necessary
	      else
		{
		  line[bytes] = '\0';
		  //		  printf("Message Received: %s", line);
		  execute_brake(line);

		  if (line[0] != '\0')
		    {		  
		      if (send(BrakeConnectedSocket, line, sizeof(line), 0) == -1)
			{
			  perror("Cannot send to Brake");
			  return ERROR;
			}
		      else
			{
			  //printf("Message Sent to Brake %d: %s\n", BrakeConnectedSocket, line);
			}
		    }
		}
	    }


	  if (i == ThrotSocket)
	    {
	      cliLen = sizeof(cliAddr);
	      if ((ThrotConnectedSocket = accept(ThrotSocket, (struct sockaddr *)&cliAddr, (socklen_t *)&cliLen)) == -1)
		{ 
		  perror("Could Not Accept Connection On Throttle Port\n");
		  return ERROR;
		}
	      else
		{
		  //add to fd set
		  FD_SET(ThrotConnectedSocket, &fileDesc);
		  if (ThrotConnectedSocket > fd_max)
		    fd_max = ThrotConnectedSocket;
		}
	      printf("Connected on Throttle Port\n");
	    }
	  else if (i == ThrotConnectedSocket)
	    {
	      //get incoming message from Steering
	      if ((bytes = recv(ThrotConnectedSocket, line, sizeof(line), 0)) <= 0)
		{
		  if (bytes == 0)
		    printf("throttle disconnected %d\n", ThrotConnectedSocket);
		  else
		    perror("Connection error from throttle");
		  close(ThrotConnectedSocket);
		  if (ThrotConnectedSocket == fd_max)
		    fd_max -= 1;
		  FD_CLR(ThrotConnectedSocket, &fileDesc);
		  ThrotConnectedSocket = -2; 
		}
	      //otherwise parse message and do what's necessary
	      else
		{
		  line[bytes] = '\0';
		  execute_throttle(line); 

		  if (line[0] != '\0')
		    {		
		      if (send(ThrotConnectedSocket, line, sizeof(line), 0) == -1)
			{
			  perror("Cannot send to Throttle");
			  return ERROR;
			}
		      else
			{
			  //printf("Message Sent to Throttle %d: %s\n", ThrotConnectedSocket, line);
			}
		    }
		}
	    }


	} 
  }
  delete &SDSpos;
  delete &robot;
  return 0;
}
