/* THis is a class to keep track of communication delays.
 * see write up in header file */


#include "Benchmark.hh"

//Constructor
Benchmark::Benchmark()
{
  lossless = true;
  first_delay = NULL;
  last_delay = NULL;
  first_start_time = NULL;
  last_start_time = NULL;
  //  number_of_measurements = 0;
}

//Destructor
Benchmark::~Benchmark()  
{
  destroy(first_delay,last_delay);
  destroy(first_start_time,last_start_time);
}

bool Benchmark::begin()
{
  if (lossless)
    return lossless_begin();
  else
    return latest_begin();
}


bool Benchmark::latest_begin()
{
  // This is actually the same
  return lossless_begin();
}

/* Start timing a new packet in lossless mode*/
bool Benchmark::lossless_begin()
{

  // cout << "BEGIN called" << endl;
  time_node * temp;

  // First get the time stamp
  unsigned long long current_time = ulltime();

  // Check the case that there is no list and add one element
  // Linking in from both ends
  if (first_start_time == NULL)
    {
      first_start_time = new time_node(current_time,NULL,NULL);
      last_start_time = first_start_time;
    }
  // There must be at least one so add the current node to the end linking
  // it back to the old end.  Then increment the last pointer.
  else
    {
      last_start_time->next = new time_node(current_time,NULL,last_start_time);
      temp = last_start_time->next;
      last_start_time = temp;
    }
  // cout << "Recording the current time of " << current_time << endl;

  return true;
}


bool Benchmark::end()
{
  if (lossless)
    return lossless_end();
  else
    return latest_end();
}


/* record the delay since the last message sent.  
 * LIFO instead of FIFO */
bool Benchmark::latest_end()
{
  //  cout << "END called" << endl;
  time_node * temp;
  unsigned long long difference = 0;

  if (last_start_time == NULL)
    {
      //      cout <<"No previous value" << endl;
      return false;
    }

  // Record the difference
  difference = ulltime() - last_start_time->value;
  //cout << "setting delay of " << difference << endl;
  // Remove the used start time
  temp = last_start_time;
  last_start_time = temp->previous;

  // if the list has not reached zero length make sure the last->next pointer is destroyed
  if (last_start_time != NULL)
    last_start_time->next = NULL;

  delete temp;
  
  // Check if the element removed was the last one and get rid of the first pointer
  if  (last_start_time == NULL)
    {
      first_start_time = NULL;
    }

  //  cout << "ending and returning 2" << endl;
  return append_delay(difference);

}


/* Record the reciept of a new packet in lossless mode
* FIFO not LIFO*/
bool Benchmark::lossless_end() 
{
  //  cout << "END called" << endl;
  time_node * temp;
  unsigned long long difference = 0;

  if (first_start_time == NULL)
    {
      cerr<<"No time stamp in the past" << endl;
      return false;
    }

  // Record the difference
  difference = ulltime() - first_start_time->value;
  //cout << "setting delay of " << difference << endl;
  // Remove the used start time
  temp = first_start_time;
  first_start_time = temp->next;
  delete temp;
  
  // Check if the element removed was the last one and get rid of the last pointer
  if  (first_start_time == NULL)
    {
      last_start_time = NULL;
    }

  //  cout << "ending and returning 2" << endl;
  return append_delay(difference);
}


  /* Append the difference */
bool Benchmark::append_delay(unsigned long long difference)
{
  time_node * temp;  // A temporary variable for reassignment

  // Check the case that there is no list and add one element
  // Linking in from both ends
  if (first_delay == NULL)
    {
      first_delay = new time_node(difference,NULL,NULL);
      last_delay = first_delay;
    }
  // There must be at least one so add the current node to the end linking
  // it back to the old end.  Then increment the last pointer.
  else
    {
      last_delay->next = new time_node(difference,NULL,last_delay);
      temp = last_delay->next;
      last_delay = temp;
    }


  return true;
}


/* Calculate the average of all the delays */
double Benchmark::average_delay()
{
  if (first_delay == NULL)
    return 0;

  double total = 0;
  int length = 0;

  time_node * first = first_delay;
  while (first != NULL)
    {
      total += first->value;
      length++;
      first = first->next;
    }
  //  cout << "The average is " << total/length << endl;
  return double(total / length);
}

// Calculate the standard deviation of all the delays
double Benchmark::std_dev()
{
  double average = average_delay();
  if (first_delay == NULL)
    return 0;

  double total = 0;
  int length = 0;

  time_node * first = first_delay;
  while (first != NULL)
    {
      total += (double(first->value)-average)*(double(first->value)-average);
      length++;
      first = first->next;
    }
  return sqrt(total/length);
}

// Count the number of packets waiting for a return
int Benchmark::lost_packets()
{
  time_node *temp = first_start_time;
  int start_count = 0;
  while (temp != NULL)
    {
      start_count++;
      temp = temp->next;
    }

  return start_count;
}

// A function to dump to file the data collected
void Benchmark::report()
{
  // An output file
  fstream logfile;
  string filename;
  filename = "Log_" + description + ".log";

  logfile.open(filename.c_str(), fstream::out|fstream::app);
  logfile << "**************REPORT****************" << endl;
  logfile << "Delay " << description << endl;
  logfile << "Printing start time list of "<< description  << endl;
  print_ll(first_start_time,logfile);
  logfile << "Lost packets " << lost_packets() << endl;
  logfile << "Printing delay list of "<< description << endl;
  print_ll(first_delay,logfile);
  logfile << "The mean delay is " << average_delay() << endl;
  logfile << "The standard deviation is " << std_dev() << endl;

  logfile << "***********END REPORT***************" << endl;
  logfile.close();
}

// Return the current time in unsigned long long (from DGCutils)
unsigned long long Benchmark::ulltime()
{
  timeval tv;
  gettimeofday(&tv, NULL);
  return  (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;
}


// Deallocate all memory properly
void Benchmark::destroy(time_node * first, time_node * last)
{
  time_node * ahead;
  time_node * moving_pointer;
  // Making sure that there actually is data.  
  if (first != NULL)
    {
      // Initialize the current pointer and the next pointer
      moving_pointer = first;
      ahead = first->next;
      while (moving_pointer != NULL)
	{
	    
	  delete moving_pointer;
	  moving_pointer = NULL;
	  if (ahead != NULL)
	    {
	      // Increment the current pointer
	      moving_pointer = ahead;
	      // Increment ahead pointer
	      ahead = moving_pointer->next;
	    }
	}
    }  
  // Get rid of the last pointer to protect against segfaults/memory leaks
  last = NULL;

}

// Printout a linked list
bool Benchmark::print_ll(time_node * first, fstream &logfile)
{
  //  logfile << "Printing out an array" << endl;
  while (first !=NULL)
    {
      logfile << first->value << endl;  // print
      first = first->next;  // increment
    }
  return true;
}


bool Benchmark::set_lossless()
{
  lossless = true;
  return true;
}

bool Benchmark::set_lossy()
{
  lossless = false;
  return true;
}

bool Benchmark::set_description(string desc)
{
  description = desc;
  return true;
}
