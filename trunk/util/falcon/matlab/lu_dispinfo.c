/*
 * lu_dispinfo - open a lookup table
 *
 * RMM, 14 May 96
 *
 */

#include <stdio.h>
#include "mex.h"
#include "falcon/lutable.h"

void mexFunction(nlhs, plhs, nrhs, prhs)
int nlhs, nrhs;
Matrix *plhs[], *prhs[];
{
    char address[16];
    LU_DATA *lut = NULL;

    if (nrhs != 1) {
	mexErrMsgTxt("lu_dispinfo: missing or invalid argument");
	return;
    }
    mxGetString(prhs[0], address, 1024);

    /* Get the address of the table */
    if (sscanf(address, "0x%x", &lut) != 1) {
	mexErrMsgTxt("lu_dispinfo: invalid address");
	return;
    }

    /* Print out information about the lookup table */
    lu_dispinfo(lut);
}
