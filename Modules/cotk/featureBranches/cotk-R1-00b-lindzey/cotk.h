
/* 
 * Desc: Simple ncurses-based console toolkit
 * Date: 14 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id: cotk.h,v 1.4 2006/12/17 23:00:35 abhoward Exp $
*/

#ifndef COTK_H
#define COTK_H

#if defined __cplusplus
extern "C"
{
#endif

/// @brief COTK display handle (opaque)  
typedef struct _cotk_t cotk_t;

/// @brief COTK callback prototype
typedef int (*cotk_callback_t) (cotk_t *self, void *data, const char *token);

/// @brief Allocate new display
cotk_t *cotk_alloc();

/// @brief Free display
void cotk_free(cotk_t *self);
  
/// @brief Bind the display template
int cotk_bind_template(cotk_t *self, const char *templ);

/// @brief Bind the default refresh function
int cotk_bind_refresh(cotk_t *self, cotk_callback_t fn, void *data);  

/// @brief Bind a button to a callback function
int cotk_bind_button(cotk_t *self, const char *token, const char *text,
                     const char *keys, cotk_callback_t fn, void *data);  

/// @brief Bind a toggle to a callback function
int cotk_bind_toggle(cotk_t *self, const char *token, const char *text,
                     const char *keys, cotk_callback_t fn, void *data);  

/// @brief Open and initialize the display
int cotk_open(cotk_t *self);  

/// @brief Finalize and close the display
int cotk_close(cotk_t *self);  

/// @brief Refresh the display and generate callbacks
int cotk_update(cotk_t *self);

/// @brief Print text to field
int cotk_printf(cotk_t *self, const char *token, int attr, const char *fmt, ...);

/// @brief Print an aalib image
int cotk_draw_image(cotk_t *self, int x, int y,
                    int width, int height, const unsigned char *text, const unsigned char *attr);

/// @brief Set the toggle state (on or off)
int cotk_toggle_set(cotk_t *self, const char *token, int state);

/// @brief Get the toggle state (on or off)
int cotk_toggle_get(cotk_t *self, const char *token);

#if defined __cplusplus
}
#endif

#endif
