
/* 
 * Desc: Test/example program for simple ncurses-based console toolkit
 * Date: 14 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id: ctk_test.c,v 1.6 2007/01/04 01:53:43 abhoward Exp $
*/

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <ncurses.h>

#include "cotk.h"


// Message macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

static bool quit = false;

static char screen[] =
"COTK Test Program $Revision: 1.6 $   \n"
"                        \n"
"Time: %time%            \n"
"\n"
"%stderr%\n"
"%stderr%\n"
"%stderr%\n"
"%stderr%\n"
"%stderr%\n"
"\n"
"[%T1%|%T2%]             \n"
"[%QUIT%|%HELLO%]        \n";


// Refresh the display
int on_refresh(cotk_t *cotk, void *data)
{
  struct timeval tv;

  gettimeofday(&tv, NULL);
  cotk_printf(cotk, "%time%", A_NORMAL, "%d.%06d", tv.tv_sec, tv.tv_usec);
  
  return 0;
}


// Handle buttons
int on_quit(cotk_t *cotk, void *data, const char *token)
{
  MSG("got quit");
  quit = true;
  return 0;
}


// Handle buttons
int on_hello(cotk_t *cotk, void *data, const char *token)
{
  MSG("hello world");
  return 0;
}


// Handle toggles
int on_toggle(cotk_t *cotk, void *data, const char *token)
{
  MSG("toggle");

  // Make toggles work like radio buttons
  if (strcmp(token, "%T1%") == 0)
    cotk_toggle_set(cotk, "%T2%", false);
  else if (strcmp(token, "%T2%") == 0)
    cotk_toggle_set(cotk, "%T1%", false);
  
  return 0;
}


int main(int argc, char **argv)
{
  cotk_t *cotk;
  int count, i;

  cotk = cotk_alloc();
  assert(cotk);

  cotk_bind_template(cotk, screen);
  cotk_bind_button(cotk, "%QUIT%", " QUIT ", "Qq", on_quit, NULL);
  cotk_bind_button(cotk, "%HELLO%", " HELLO ", "Hh", on_hello, NULL);
  cotk_bind_toggle(cotk, "%T1%", " T1 ", "1", on_toggle, NULL);
  cotk_bind_toggle(cotk, "%T2%", " T2 ", "2", on_toggle, NULL);
  
  if (cotk_open(cotk, "./cotk-test.msg") != 0)
    return ERROR("unable to open display");

  count = 0;
  while (!quit)
  {
    struct timeval tv;

    usleep(100000);
    
    gettimeofday(&tv, NULL);
    cotk_printf(cotk, "%time%", (tv.tv_sec % 4 == 0 ? A_NORMAL : A_UNDERLINE),
               "%d.%06d", tv.tv_sec, tv.tv_usec);
    
    cotk_update(cotk);

    for (i = 0; i < 10; i++)
      MSG("count %d %d", count++, rand());      
  }
  
  cotk_close(cotk);
  cotk_free(cotk);
  
  return 0;
}
