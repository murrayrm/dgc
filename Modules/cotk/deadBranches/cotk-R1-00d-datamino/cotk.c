
/* 
 * Desc: Simple ncurses-based console toolkit
 * Date: 14 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id: cotk.c,v 1.3 2007/04/09 01:22:39 abhoward Exp $
*/


/* TODO

- Use "window" version of functions.
- Have function for setting focus attrs.
- Fix so we dont need '\n\r' when printing to stdout.

 */

#if HAVE_CONFIG_H
  #include <config.h>
#endif

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <sys/poll.h>
#include <ncurses.h>

#include "cotk.h"


// COTK field descriptor
typedef struct
{
  // Token
  char token[64];
  
  // Location
  int row, col;

  // What type of field is this?
  enum {type_text, type_button, type_toggle} type;
  
  // Text (for buttons)
  char text[64];

  // Bound keys (for buttons)
  char keys[16];

  // Callback (for buttons)
  cotk_callback_t callback_fn;
  void *callback_data;

  // Is the toggle in the on state?
  bool toggle;

} cotk_field_t;


// COTK display data
struct _cotk_t
{
  // Window handle
  WINDOW *win;
  
  // Template
  const char *template;

  // Field list extracted from template
  int num_fields;
  cotk_field_t fields[512];

  // Region for for stderr (scrolling)
  int stderr_rows[2];

  // Re-directed file descriptors for stderr
  int stderr_fds[2];

  // Message file for stderr
  FILE *stderr_file;

  // Default cursor row
  int default_row;
  
  // Refresh callback
  cotk_callback_t refresh_fn;
  void *refresh_data;

  // Field that has focus
  cotk_field_t *focus;
};


// Redraw buttons and toggles
int cotk_redraw_fields(cotk_t *self);

// Process anything in the stderr pipe
int cotk_update_stderr(cotk_t *self);


// Allocate new display
cotk_t *cotk_alloc()
{
  cotk_t *self;

  self = calloc(1, sizeof(cotk_t));
 
  return self;
}


// Free display
void cotk_free(cotk_t *self)
{
  free(self);
  return;
}


// Bind the display template
int cotk_bind_template(cotk_t *self, const char *template)
{
  int i, row, col;
  cotk_field_t *field;
  
  self->template = template;

  row = 0;
  col = 0;
  field = NULL;
  self->stderr_rows[0] = +INT_MAX;
  self->stderr_rows[1] = -INT_MAX;
  
  // Extract fields from the template
  for (i = 0; i < strlen(template); i++)
  {
    if (field == NULL && template[i] == '%')
    {
      // Parse field start char
      assert(self->num_fields < (int) (sizeof(self->fields) / sizeof(self->fields[0])));
      field = self->fields + self->num_fields++;
      field->row = row;
      field->col = col;
      field->token[0] = 0;
      assert(strlen(field->token) + 1 < sizeof(field->token));
      field->token[strlen(field->token)] = template[i];
      field->text[0] = 0;
    }
    else if (field != NULL && template[i] != '%')
    {
      // Parse field token
      assert(strlen(field->token) + 1 < sizeof(field->token));
      field->token[strlen(field->token)] = template[i];
    }
    else if (field != NULL && template[i] == '%')
    {
      // Parse field end char
      assert(strlen(field->token) + 1 < sizeof(field->token));
      field->token[strlen(field->token)] = template[i];

      // Pick out reserved token for stderr region and find the first
      // and last rows.
      if (strcasecmp(field->token, "%stderr%") == 0)
      {
        if (row < self->stderr_rows[0])
          self->stderr_rows[0] = row;
        if (row > self->stderr_rows[1])
          self->stderr_rows[1] = row;
      }

      field = NULL;
    }

    // Parse EOL
    if (template[i] == '\n')
    {
      row += 1;
      col = 0;
    }
    else
    {
      col += 1;
    }
  }

  self->default_row = row;  
      
  return 0;
}


// Lookup the location of the given token
cotk_field_t *cotk_lookup(cotk_t *self, const char *token)
{
  int i;
  cotk_field_t *field;

  for (i = 0; i < self->num_fields; i++)
  {
    field = self->fields + i;
    if (strcmp(token, field->token) == 0)
      return field;
  }
  
  return NULL;
}


// Bind the default refresh function
int cotk_bind_refresh(cotk_t *self, cotk_callback_t fn, void *data)
{
  self->refresh_fn = fn;
  self->refresh_data = data;
  
  return 0;
}


// Bind a button to a callback function
int cotk_bind_button(cotk_t *self, const char *token, const char *text,
                    const char *keys, cotk_callback_t fn, void *data)
{
  cotk_field_t *field;

  field = cotk_lookup(self, token);
  if (!field)
    return -1;

  field->type = type_button;
  strncpy(field->text, text, sizeof(field->text) - 1);
  strncpy(field->keys, keys, sizeof(field->keys) - 1);
  field->callback_fn = fn;
  field->callback_data = data;

  // Set the default focus to the first button
  if (!self->focus)
    self->focus = field;
  
  return 0;
}


// Bind a toggle to a callback function
int cotk_bind_toggle(cotk_t *self, const char *token, const char *text,
                    const char *keys, cotk_callback_t fn, void *data)
{
  cotk_field_t *field;

  field = cotk_lookup(self, token);
  if (!field)
    return -1;

  field->type = type_toggle;
  strncpy(field->text, text, sizeof(field->text) - 1);
  strncpy(field->keys, keys, sizeof(field->keys) - 1);
  field->callback_fn = fn;
  field->callback_data = data;

  // Set the default focus to the first button
  if (!self->focus)
    self->focus = field;
  
  return 0;
}


// Open and initialize the display
int cotk_open(cotk_t *self, const char *filename)
{
  int i;
  cotk_field_t *field;

  // Create file for stderr messages
  if (filename)
  {
    self->stderr_file = fopen(filename, "a");
    if (!self->stderr_file)
      return fprintf(stderr, "unable to open stderr file %s: %s\n", filename, strerror(errno));
  }

  // Redirect stderr to a pipe
  pipe(self->stderr_fds);
  dup2(self->stderr_fds[1], fileno(stderr));
  
  // Create window
  self->win = initscr();

  // Set up default colors
  if (has_colors())
  {
    start_color();
    use_default_colors();
  }

  // Hide the cursor
  curs_set(0);
  
  // Draw the template
  mvwprintw(self->win, 0, 0, "%s", self->template);

  // Blank out the fields
  for (i = 0; i < self->num_fields; i++)
  {
    field = self->fields + i;
    if (field->type == type_text)
      mvprintw(field->row, field->col, "%*s", strlen(field->token), "");
  }
  
  // Draw buttons/toggles
  for (i = 0; i < self->num_fields; i++)
  {
    field = self->fields + i;
    if (field->type == type_button || field->type == type_toggle)
      mvprintw(field->row, field->col, field->text);
  }

  // Draw button states
  cotk_redraw_fields(self);

  // Set up a stderr scrolling region  
  if (self->stderr_rows[0] < +INT_MAX && self->stderr_rows[1] > -INT_MAX)
  {
    wsetscrreg(self->win, self->stderr_rows[0], self->stderr_rows[1]);
    idlok(self->win, true);
    scrollok(self->win, true);
  }

  // Use non-blocking input
  noecho();
  nodelay(self->win, true);
  keypad(self->win, true);

  // Leave cursor at a sensible location
  move(self->default_row, 0);
  wrefresh(self->win);

  return 0;
}


// Finalize and close the display
int cotk_close(cotk_t *self)
{
  // Flush stderr
  cotk_update_stderr(self);

  // Leave curses mode
  endwin();
 
  // Clean up stderr handling
  if (self->stderr_file)
    fclose(self->stderr_file);

  // Re-store the re-direction so that future stderr messages don't
  // get swallowed.  Is this correct?  Or a horrible HACK?
  dup2(fileno(stdout), fileno(stderr));

  return 0;
}


// Create a color pair.
int cotk_set_color_pair(cotk_t *self, int pair, int fg, int bg)
{
  if (!has_colors())
    return -1;

  init_pair(pair, fg, bg);

  return 0;
}


// Pick the focusable field that is nearest the given row, col.
// (nr, rc) define the metric used to determine proximity.
cotk_field_t *cotk_pick_focus(cotk_t *self, int row, int col, int nr, int nc)
{
  int i;
  int p, q, min;
  cotk_field_t *field, *min_field;

  p = nr * row + nc * col;

  min = 1000;
  min_field = NULL;
  
  for (i = 0; i < self->num_fields; i++)
  {
    field = self->fields + i;
    if (field->type == type_text)
      continue;

    q = nr * field->row + nc * field->col;

    if (q > p && abs(q - p) < min)
    {
      min = abs(q - p);
      min_field = field;
    }
  }

  return min_field;
}


// Process keyboard input
int cotk_update_key(cotk_t *self, int ch)
{
  int i;
  cotk_field_t *field;
  cotk_field_t *focus;
  
  // Handle focus movement
  if (self->focus)
  {
    // Pick the next field
    focus = self->focus;
    if (ch == KEY_RIGHT)
      focus = cotk_pick_focus(self, self->focus->row, self->focus->col, +100, +1);
    else if (ch == KEY_LEFT)
      focus = cotk_pick_focus(self, self->focus->row, self->focus->col, -100, -1);
    else if (ch == KEY_DOWN)
      focus = cotk_pick_focus(self, self->focus->row, self->focus->col, +1, +100);
    else if (ch == KEY_UP)
      focus = cotk_pick_focus(self, self->focus->row, self->focus->col, -1, -100);

    if (focus)
      self->focus = focus;
  }

  // Handle ENTER
  if (ch == '\n')
  {
    field = self->focus;
    if (field)
    {
      if (field->type == type_toggle)
        field->toggle = !field->toggle;      
      if (field->callback_fn)
      {
        move(self->default_row, 0);
        touchwin(self->win);
        wrefresh(self->win);
        (*field->callback_fn) (self, field->callback_data, field->token);
      }
    }
  }
      
  // Handle bound keys
  for (i = 0; i < self->num_fields; i++)
  {
    field = self->fields + i;
    if (strchr(field->keys, ch) != NULL)
    { 
      if (field->type == type_toggle)
        field->toggle = !field->toggle;      
      if (field->callback_fn)
      {
        move(self->default_row, 0);
        touchwin(self->win);
        wrefresh(self->win);
        (*field->callback_fn) (self, field->callback_data, field->token);
      }
    }
  }
  
  return 0;
}


// Redraw buttons and toggles
int cotk_redraw_fields(cotk_t *self)
{
  int i;
  int at;
  cotk_field_t *field;
  
  for (i = 0; i < self->num_fields; i++)
  {
    field = self->fields + i;
    if (field->type == type_text)
      continue;
    at = A_NORMAL;
    if (field == self->focus)
      at |= A_REVERSE;
    if (field->toggle)
      at |= A_UNDERLINE;      
    mvwchgat(self->win, field->row, field->col, strlen(field->text), at, 0, NULL);
  }

  touchwin(self->win);
  wrefresh(self->win);
  
  return 0;
}


// Process anything in the stderr pipe
int cotk_update_stderr(cotk_t *self)
{
  struct pollfd fds[1];
  int size;
  char data[4096];

  if (self->stderr_fds[0] == 0)
    return 0;

  fds[0].fd = self->stderr_fds[0];
  fds[0].events = POLLIN;
  fds[0].revents = 0;

  // TODO: fix this so it cannot overflow the buffer
  size = 0;
  while (poll(fds, 1, 0) > 0 && size < sizeof(data) - 1)
  {
    assert(size < sizeof(data) - 1);
    read(fds[0].fd, data + size++, 1);
  }

  assert(size < sizeof(data));
  data[size] = 0;

  // Write to the screen
  if (size > 0 && self->stderr_rows[0] < +INT_MAX)
    mvwprintw(self->win, self->stderr_rows[0], 0, "%s", data);

  // Write to the message file
  if (self->stderr_file)
  {
    fprintf(self->stderr_file, data);
    fflush(self->stderr_file);
  }
  
  return 0;
}


// Refresh the display and generate callbacks
int cotk_update(cotk_t *self)
{
  int ch;
  
  // Handle keyboard
  ch = getch();
  if (ch != ERR)       
    cotk_update_key(self, ch);

  // Redraw buttons/toggles
  cotk_redraw_fields(self);

  // Refresh display using callback
  if (self->refresh_fn)
    (*self->refresh_fn) (self, self->refresh_data, NULL);

  // Handle any stderr messages
  cotk_update_stderr(self);

  // Set cursor at reasonable location for stdout
  move(self->default_row, 0);
  wrefresh(self->win);
  
  return 0;
}


// Print text to field
int cotk_printf(cotk_t *self, const char *token, int attr, const char *fmt, ...)
{
  int row, col; 
  va_list ap;
  cotk_field_t *field;

  row = 1;
  col = 1;
  
  // Find the field
  field = cotk_lookup(self, token);
  if (!field)
    return -1;

  move(field->row, field->col);
    
  wattrset(self->win, attr);
  va_start(ap, fmt);  
  vwprintw(self->win, fmt, ap);
  va_end(ap);
  wattrset(self->win, A_NORMAL);
  
  return 0;
}


// Print an aalib image
int cotk_draw_image(cotk_t *self, int x, int y,
                    int width, int height, const unsigned char *text, const unsigned char *attr)
{
  int i, j;
  const unsigned char *tp, *ap;

  tp = text;
  ap = attr;

  for (j = 0; j < height; j++)
  {
    for (i = 0; i < width; i++)
    {
      mvwprintw(self->win, y + j, x + i, "%c", tp[0]);
      ap += 1;
      tp += 1;
    }
  }
  
  return 0;
}


// Set the toggle state (on or off)
int cotk_toggle_set(cotk_t *self, const char *token, int state)
{
  cotk_field_t *field;
  
  // Find the field
  field = cotk_lookup(self, token);
  if (!field)
    return -1;

  field->toggle = state;
  touchwin(self->win);

  return 0;
}


// Get the toggle state (on or off)
int cotk_toggle_get(cotk_t *self, const char *token)
{
  cotk_field_t *field;
  
  // Find the field
  field = cotk_lookup(self, token);
  if (!field)
    return false;

  return field->toggle;
}
