/*
 * Desc: Path planner based on RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */
//#include <stdlib.h>
// Dependencies
#include <temp-planner-interfaces/PlanGraph.hh>
#include <temp-planner-interfaces/PlanGraphPath.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include "Tree.hh"
#include <list>
#include <stdio.h>
#include "frames/point2_uncertain.hh"
#include "frames/point2.hh"
#include "map/Map.hh"
#include "map/MapPrior.hh"
#include "gcinterfaces/SegGoals.hh"


/// @brief Basic planner for searching the graph.
class RRTPlanner
{
  public:

  /// @brief Default constructor
  RRTPlanner(PlanGraph *graph);

  /// @brief Destructor
  ~RRTPlanner();

  public:

  /// @brief Main function that plans the path
  int planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart,
                PlanGraphNode *nodeGoal, PlanGraphPath *path, Map *map, float steerAngle);

  int planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart,
                PlanGraphNode *nodeGoal, PlanGraphPath *path);

  // Give the logic state to the path planner
  int setState(StateProblem_t *state_problem);

  // Test function to see if semi-goals can be implemented
  void setGoal(SegGoals segGoals);

  // Array used if you want to see the samples in the Planner Viewer
  PlanGraphNode* sample[20000];

  // Points in the tree to be drawn by the Planner Viewer
  point2 livePoints[10000];

  // Used if you want to see the samples in the Planner Viewer
  int sampleSize;

  // Number of points in the tree
  int pointSize;

  int pathsize;

  TreeNode nodes[100];

  private:

  // Build or extend a RRT
  void buildRRT(PlanGraphNode *nodeGoal);

  // Read config file
  void readConfig();

  // Generate the best plan
  void genPlan(PlanGraphNode *nodeGoal);

  // Generate a path from the plan
  void genPath(PlanGraphPath *path);

  // Extend the tree
  int extendRRT(float x, float y);

  // Simple and bad backing function
  void backUp(PlanGraphPath *path);

  // Used to build the path for the RRTPlannerViewer
  void buildPath(int steps);

  // Is this pose ok?
  bool checkConstraints(float x, float y, float rot);

  // Is this point in a road?
  bool onRoad(float x, float y);

  // Is this point free from collision
  bool obstacleFree(float x, float y);

  void addSample(float x, float y);

  void addPoint();

  // The graph we are using
  PlanGraph *graph;

  bool tryConnect(TreeNode *node, float x, float y, float v);

  // The tree we are using
  Tree *tree_ptr;

  std::list<TreeNode*> pathList;

  PlanGraphNode* startNode;

  PlanGraphNode* goalNode;

  // The map we are using
  Map *m_map;

  float steer;

  bool zone;

  bool pathFound;

  int pathSize;

  int prevPathSize;
  
  FSM_state_t currState;

  FSM_flag_t currFlag;

  float con_w;

  bool closeHeur;

  bool back;

  bool inIntersect(float x, float y);

  //Parameters
  static int NUM_SAMPLES;
  static int STEP_SIZE;
  static bool USE_BIAS;
  static bool USE_DYN;
  static float DYN_STEP;
  static float DELTA;
  static bool CON_STEER;
  static float CON_W;
  static bool USE_DUBIN;
  static bool KEEP_PATH;
  static bool KEEP_TREE;
  static float BIAS_SIG_THETA;
  static float BIAS_SIG_R;
  static float BIAS_R; 
  static float GOAL_BIAS;
  static bool TEST_NEW;
};
