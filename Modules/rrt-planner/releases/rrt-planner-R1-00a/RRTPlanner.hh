/*
 * Desc: Path planner based on RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */
//#include <stdlib.h>
// Dependencies
#include <temp-planner-interfaces/PlanGraph.hh>
#include <temp-planner-interfaces/PlanGraphPath.hh>
#include "Tree.hh"
#include <list>
#include <stdio.h>
#include "frames/point2_uncertain.hh"
#include "frames/point2.hh"
#include "map/Map.hh"
#include "map/MapPrior.hh"



/// @brief Basic planner for searching the graph.
class RRTPlanner
{
  public:

  /// @brief Default constructor
  RRTPlanner(PlanGraph *graph);

  /// @brief Destructor
  ~RRTPlanner();

  public:

  /// @brief Main function that plans the path
  int planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart,
                PlanGraphNode *nodeGoal, PlanGraphPath *path, Map *map);

  int planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart,
                PlanGraphNode *nodeGoal, PlanGraphPath *path);

  TreeNode nodes[100];

  TreeNode path[100];

  PlanGraphNode* sample[200];

  int sampleSize;

  int pathsize;

  private:

  void buildRRT(PlanGraphNode *nodeGoal);

  void readConfig();

  // Generate the best plan
  void genPlan(PlanGraphNode *nodeGoal);

  void genPath(PlanGraphPath *path);

  int extendRRT(float x, float y);

  void buildPath(int steps);

  bool onRoad(float x, float y);

  void addSample(float x, float y);

  // The graph we are using
  PlanGraph *graph;

  int tryConnect(TreeNode *node, float x, float y);

  Tree *treeP;

  std::list<TreeNode*> pathList;

  PlanGraphNode* startNode;

  PlanGraphNode* goalNode;

  Map *m_map;

  //Parameters
  static int NUM_SAMPLES;
  static int STEP_SIZE;
  static bool USE_BIAS;
  static bool USE_DYN;
  static float DYN_STEP;
  static float DELTA;
  static bool CON_STEER;
  static bool KEEP_PATH;
  static float BIAS_SIG_THETA;
  static float BIAS_SIG_R;
  static float BIAS_R; 
};
