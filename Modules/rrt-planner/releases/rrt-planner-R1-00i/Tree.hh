/*
 * Desc: Tree for the RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */

#ifndef TREE_HH
#define TREE_HH

#include <stdio.h>
#include <list>
#include <vector>
#include <cctype>
using namespace std;

// The node of the Rapidly-exploring Random Tree
struct TreeNode
{
  float x;

  float y;

  float rot;

  float steerAngle;

  // Parents id
  int parent;

  // Pointer to parent
  TreeNode* parent_ptr;

  // List of children
  list<TreeNode*> children;

  int id;

  // Is this node part of a path?
  bool path;

  // Is this node alive?
  int dead;

};

class Tree {
  public:
 
  Tree();

  ~Tree();

  // Make the input point the root and clear the tree (if there is one)
  void makeRoot(float x, float y);

  // Make the input point the root and clear the tree (if there is one)
  void makeRoot(float x, float y, float rot);

  // Make the input point the root and clear the tree (if there is one)
  void makeRoot(float x, float y, float rot, float steerAngle);

  // Extend the tree with the given point
  int extendTree(TreeNode* parent_ptr, float x, float y);

  // Extend the tree with the given point
  int extendTree(TreeNode* parent_ptr, float x, float y, float rot);

  // Extend the tree with the given point
  int extendTree(TreeNode* parent_ptr, float x, float y, float rot, float steerAngle);

  // Return the closest node in the tree to the given point
  TreeNode* closest(float x, float y);

  // Return the closest node in the tree to the given point with distance dist
  TreeNode* closest(float x, float y, float &dist);

  // Return a list of the closest nodes to the given point
  list<TreeNode*> closestArr(float x, float y);

  // Return the closest node to the given point using Dubins length (something is wrong with this function)
  TreeNode* closestDubin(float px, float py);

  // Return the closest node to the given point using Dubins length (something is wrong with this function)
  TreeNode* closestDubin(float px, float py, float &min_dist);

  // Make the id node the root of the tree
  int reRoot(int id);

  list<TreeNode*> nodes;

  private:

  // Check if the nodes in the tree are alive.
  void check(TreeNode *node_ptr, int id);

  TreeNode* root_ptr;

  int size;

};


#endif
