/*
 * Desc: Tree for the RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */

#include "Tree.hh"
#include <math.h>
#include <alice/AliceConstants.h>

#define PI 3.14159265

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

Tree::Tree() {

  rootP = NULL;
  size = 0;
}


Tree::~Tree() { 
}

//Make the root of the tree
void Tree::makeRoot(float px, float py) {

  nodes.clear();
  size = 0;

  rootP = new TreeNode;
  rootP->x = px;
  rootP->y = py;
  rootP->id=size;
  nodes.push_back(rootP);

  size = 1;


}

//Make the root of the tree
void Tree::makeRoot(float px, float py, float prot) {

  nodes.clear();
  size = 0;


  rootP = new TreeNode;
  rootP->x = px;
  rootP->y = py;
  rootP->rot = prot;
  rootP->id=size;
  nodes.push_back(rootP);

  size = 1;


}

//Make the root of the tree
void Tree::makeRoot(float px, float py, float prot, float steerAngle) {

  nodes.clear();
  size = 0;


  rootP = new TreeNode;
  rootP->x = px;
  rootP->y = py;
  rootP->rot = prot;
  rootP->steerAngle=steerAngle;
  rootP->id=size;
  nodes.push_back(rootP);

  size = 1;


}

//Extends the tree
TreeNode* Tree::extendTree(TreeNode* parP, float px, float py) {
  TreeNode *nn;

  nn = new TreeNode;
  nn->x=px;
  nn->y=py;
  nn->parent=parP->id;
  nn->parentP=parP;
  nn->id=size;
  nn->path=false;
  nodes.push_back(nn);

  size++;

  return nn;
}

//Extends the tree
TreeNode* Tree::extendTree(TreeNode* parP, float px, float py, float prot) {
  TreeNode *nn;

  nn = new TreeNode;
  nn->x=px;
  nn->y=py;
  nn->rot=prot;
  nn->parent=parP->id;
  nn->parentP=parP;
  nn->id=size;
  nn->path=false;
  nodes.push_back(nn);

  size++;

  return nn;
}

//Extends the tree
TreeNode* Tree::extendTree(TreeNode* parP, float px, float py, float prot, float steerAngle) {
  TreeNode *nn;

  nn = new TreeNode;
  nn->x=px;
  nn->y=py;
  nn->rot=prot;
  nn->steerAngle=steerAngle;
  nn->parent=parP->id;
  nn->parentP=parP;
  nn->id=size;
  nn->path=false;
  nodes.push_back(nn);

  size++;

  return nn;
}

//Finds the closest node to a given point with euclidian distance
TreeNode* Tree::closest(float px, float py){
  TreeNode* min_n, * nn;
  int i;  
  float min_dist, dist;
  min_dist = 10000;

  for(i=0;i<size;i++){
    nn=nodes.front();
    dist=sqrt( ((px - nn->x) * (px - nn->x)) + ((py - nn->y) * (py - nn->y)) );
    nodes.push_back(nn);
    nodes.pop_front();
    if(dist < min_dist) {
      min_dist = dist;
      min_n = nn;
    }
  }
  return min_n;
}

//Finds the closest node to a given point with euclidian distance
TreeNode* Tree::closestDubin(float px, float py){
  int i;
  float x, xn, y, yn, theta;
  float min_dist = 10000;  
  float rho, L, df, dc, thetac, alpha;  
  TreeNode* min_n, *nn;
  rho = VEHICLE_MIN_TURNING_RADIUS;

  for(i=0;i<size;i++){
    nn=nodes.front();

    xn = px - nn->x;
    yn = px - nn->y;
    theta = nn->rot;
    x = xn*cos(theta) + yn*sin(theta);
    y = yn*cos(theta) - xn*cos(theta);

    nodes.push_back(nn);
    nodes.pop_front();
    
    // Dubins length
    df = sqrtf(pow(x,2) + pow((fabs(y) + rho),2));
    dc = sqrtf(pow(x,2) + pow((fabs(y) - rho),2));
    thetac = atan2(x,(rho-fabs(y))) + PI;
    alpha = acos((5*pow(rho,2) - pow(df,2))/(4*pow(rho,2)));

    if(sqrtf(pow(x,2) + pow((y - rho),2)) < rho or sqrt(pow(x,2) + pow((y + rho),2)) < rho){
      L = rho*(2*PI - alpha + asin(dc*sin(thetac)/df) + asin(rho*sin(alpha)/df));
    }
    else{
      L = sqrtf(pow(dc,2) - pow(rho,2)) + rho*(thetac - acos(rho/dc));
    }
//    MSG("L: %d",(int) L );
    if(L < min_dist){
      min_dist = L;
      min_n = nn;
    }
  }
  return min_n;

}
