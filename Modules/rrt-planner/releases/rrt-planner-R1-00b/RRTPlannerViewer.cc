/* 
 * Desc: RRT planner viewer utility
 * Date: 30 June 2008
 * Author: Karl Murray
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <float.h>

#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>

//#include "PathUtils.hh"
#include "RRTPlannerViewer.hh"

 
// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "%s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Useful macros
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define PI 3.14159265

// Default constructor
RRTPlannerViewer::RRTPlannerViewer()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
RRTPlannerViewer::~RRTPlannerViewer()
{
  return;
}


// Initialize stuff
int RRTPlannerViewer::initGUI(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) RRTPlannerViewer::onExit},
      {0},

      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows + 30, "RRT Planner Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new Fl_Glv_Window(0, 30, cols, rows, this, (Fl_Callback*) onDraw);

  this->mainwin->end();

  // Make world window resizable  
  this->mainwin->resizable(this->worldwin);

  // Set the initial POV
  this->worldwin->set_hfov(40.0);  
  this->worldwin->set_clip(4, 2000);
  this->worldwin->set_lookat(-0.1, 0, -80, 0, 0, 0, 0, 0, -1);

  // Set consistent menu state
  this->viewLayers = (1 << CMD_VIEW_AERIAL) | (1 << CMD_VIEW_RNDF);
  this->viewLayers |= (1 << CMD_VIEW_GRAPH) | (1 << CMD_VIEW_OBS);
  this->graphProps = (1  << CMD_GRAPH_NODE_STATUS);

  this->rndfList = 0;
  
//  this->dirty = true;
  
  return 0;
}

void RRTPlannerViewer::onDraw(Fl_Glv_Window *win, RRTPlannerViewer *self)
{
  VehicleState *vehState;
  ActuatorState *actState;
  PlanGraph *graph;

  // Get the state.
  vehState = &self->vehState;
  actState = &self->actState;

  // Update the state based on the mouse motion
  if (win->mouse_button == 1 && win->mouse_shift)
  {
    vehState->siteNorthing = win->at_x - self->at_x + self->at_n;
    vehState->siteEasting = win->at_y - self->at_y + self->at_e;
    self->dirty = true;
  }
  else
  {
    self->at_x = win->at_x;
    self->at_y = win->at_y;
    self->at_n = vehState->siteNorthing;
    self->at_e = vehState->siteEasting;
  }

  // Get the graph so we know the global to graph offset
  graph = self->graph;
  assert(graph);

  // Draw the grid
  self->drawMisc.predrawGrid(vehState->siteNorthing, vehState->siteEasting, 10.0);

  // Predraw the static RNDF if necessary
  if (self->rndfList == 0)
    self->rndfList = self->graph->rndf.predraw();

  self->drawMisc.predrawGrid(0, 0, 10.0);
  // Draw the grid
  glCallList(self->drawMisc.gridList);

    // Draw the RNDF 
    if (self->viewLayers & (1 << CMD_VIEW_RNDF))
      glCallList(self->rndfList);  

    // Draw the graph
    if (self->viewLayers & (1 << CMD_VIEW_GRAPH))
    {
      glCallList(self->graphList);
      if (self->graphProps & (1  << CMD_GRAPH_NODE_STATUS))
        glCallList(self->statusList);
    }

  // Switch to vehicle frame
  self->pushFrameVehicle(vehState);

  // Draw Alice
  self->drawMisc.drawAxes(1.0);
  self->drawMisc.drawAlice(actState->m_steerpos * VEHICLE_MAX_AVG_STEER);

  self->drawRRT(self);

  MSG("Drawing RRT");
  MSG("Number of nodes: %d",self->planner->pathsize);

}

// Switch to the vehicle frame 
void RRTPlannerViewer::pushFrameVehicle(const VehicleState *state)
{
  PlanGraph *graph;
  pose3_t pose;
  float m[4][4];
  
  graph = this->graph;
  
  // Transform from vehicle to site frame
  pose.pos = vec3_set(state->siteNorthing, state->siteEasting, state->siteAltitude);
  pose.rot = quat_from_rpy(state->siteRoll, state->sitePitch, state->siteYaw);
  pose3_to_mat44f(pose, m);

  // Transpose to column-major order for GL
  mat44f_trans(m, m);
  
  glPushMatrix();
  glMultMatrixf((GLfloat*) m);
  
  return;
}

void RRTPlannerViewer::drawRRT(RRTPlannerViewer *self)
{  
  int i,j,k,h;
  PlanGraphNode *node;
  k = self->planner->pathsize;
  h = self->path.pathLen;
  MSG("length: %d", h);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  
  glColor3f(0, 0, 1);
  glPointSize(4);
  glBegin(GL_POINTS);
  glVertex2f(0, 0);  

  for(i=0;i<k;i++){   
//    glVertex2f(self->planner->nodes[i].x, self->planner->nodes[i].y);
    glVertex2f(self->planner->nodes[i].y, self->planner->nodes[i].x);
  }  
  glEnd();

  glColor3f(1, 0, 0);
  glPointSize(1);
  glBegin(GL_POINTS);
  glVertex2f(0, 0);  

  for(i=0;i<self->planner->sampleSize;i++){   
//    node = self->planner->samples.at(i);
    glVertex2f(self->planner->sample[i]->pose.pos.y, self->planner->sample[i]->pose.pos.x);
  }  
  glEnd();

  glColor3f(0, 1, 0);
  glLineWidth(3);
  glBegin(GL_LINES);

  if(self->cmdline.show_tree_given){
    for(i=0;i<k;i++){
      for(j=0;j<k;j++){
       if(self->planner->nodes[i].parent == self->planner->nodes[j].id){
            glVertex2f(self->planner->nodes[i].y, self->planner->nodes[i].x);
            glVertex2f(self->planner->nodes[j].y, self->planner->nodes[j].x);
        }
      }
    }  
  }

  glEnd();

  glColor3f(1, 0, 0);
  glLineWidth(3);
  glBegin(GL_LINES);

  for(i=0;i<h-1;i++){
//    glVertex2f(self->path.nodes[i]->pose.pos.x , self->path.nodes[i]->pose.pos.y );
//    glVertex2f(self->path.nodes[i+1]->pose.pos.x , self->path.nodes[i+1]->pose.pos.y );
    glVertex2f(self->path.nodes[i]->pose.pos.y, self->path.nodes[i]->pose.pos.x);
    glVertex2f(self->path.nodes[i+1]->pose.pos.y, self->path.nodes[i+1]->pose.pos.x);
  }  
//  glVertex2f(self->path.nodes[0]->pose.pos.x , self->path.nodes[0]->pose.pos.y );
//  glVertex2f(self->path.nodes[h-1]->pose.pos.x , self->path.nodes[h-1]->pose.pos.y );
//  glVertex2f(self->path.nodes[0]->pose.pos.y , self->path.nodes[0]->pose.pos.x );
//  glVertex2f(self->path.nodes[h-1]->pose.pos.y , self->path.nodes[h-1]->pose.pos.x );

  glEnd();

  return;
}

// Finalize stuff
int RRTPlannerViewer::finiGUI()
{  
  return 0;
}


// Handle menu callbacks
void RRTPlannerViewer::onExit(Fl_Widget *w, int option)
{
  RRTPlannerViewer *self;

  self = (RRTPlannerViewer*) w->user_data();
  self->quit = true;

  return;
}

// Handle idle callbacks
void RRTPlannerViewer::onIdle(RRTPlannerViewer *self)
{
  if (!self->pause){  
    if(self->update() != 0){
      self->quit = true;
    }
  }
  else
  {
    // Sleepy bye-bye
    usleep(0);
  }

  return;
}

int RRTPlannerViewer::update(){
  return 0;
}

// Initialize the planner
int RRTPlannerViewer::init()
{
  char filename [1024];

  //initialize graph
  this->graph = new PlanGraph();

  // Load the graph
  snprintf(filename, sizeof(filename), "%s.pg", this->cmdline.rndf_arg);  
  if (this->graph->load(filename) != 0)
    return ERROR("unable to load %s", filename);

  // Load up imagery
  this->drawAerial.load("/dgc/aerial-images/", this->graph->rndf.siteN, this->graph->rndf.siteE);

  //initialize planner
  this->planner = new RRTPlanner(this->graph);

  //Test for RRTPlanner
  PlanGraphNode* myStart,* myGoal;
  PlanGraphPathDirection myDir;
  myStart = new PlanGraphNode;
  myGoal = new PlanGraphNode;
  myDir = PLAN_GRAPH_PATH_FWD;
  myStart->pose.pos.x = 0;
  myStart->pose.pos.y = 0;
  myStart->pose.rot = PI/2;
  myGoal->pose.pos.x = 0;
  myGoal->pose.pos.y = 30;
  myGoal->pose.rot = 0;
  this->planner->planPath(myStart, myDir, myGoal, &this->path);

  return 0;
}


// Finalize the planner
int RRTPlannerViewer::fini()
{  
  return 0;  
}


int main(int argc, char *argv[])
{


  RRTPlannerViewer *app;
 
  // Initialize GLUT calls
  glutInit(&argc, argv);

  app = new RRTPlannerViewer();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  if (app->initGUI(1024, 768) != 0)
    return -1;

  // Initilize the app
  if (app->init() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) RRTPlannerViewer::onIdle, app);

 
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  app->finiGUI();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}

// Parse command-line options
int RRTPlannerViewer::parseCmdLine(int argc, char **argv)
{
  pose2f_t pose;
  rrt_planner_cmdline_init(&this->cmdline);
      
  // Run parser
  if (rrt_planner_cmdline(argc, argv, &this->cmdline) != 0)
  {
    rrt_planner_cmdline_print_help();
    return -1;
  }

  // Load configuration file, if given; settings in the config file do not
  // override the command line options.
  if (this->cmdline.inputs_num > 0)
  {
    if (rrt_planner_cmdline_configfile(this->cmdline.inputs[0], &this->cmdline,
                                        false, false, false) != 0)
    {
      rrt_planner_cmdline_print_help();
      return -1;
    }
  }

  this->vehState.siteYaw = - M_PI / 2;
  // Get the intial pose
  if (this->cmdline.start_given)
  {
    int numArgs;
    int segmentId, laneId, waypointId;
    float px, py, ph;
    PlanGraphNode *wp;

    // Load the waypoint; the pose fields are optional
    px = py = 0;
    ph = -90;
//    numArgs = sscanf(this->cmdline.start_arg, "%d.%d.%d %f%f%f", 
//                     &segmentId, &laneId, &waypointId, &px, &py, &ph);
//    if (numArgs < 3)
//      return ERROR("syntax error in [%s]", this->cmdline.start_arg);

    // Get the waypoint
//    wp = this->graph->getWaypoint(segmentId, laneId, waypointId);
//    if (!wp)
//      return MSG("unknow waypoint %d.%d.%d", segmentId, laneId, waypointId);

    // Convert from waypoint frame to site frame
    pose.pos = vec2f_set(px, py);
    pose.rot = ph * M_PI/180;
    pose = pose2f_mul(wp->pose, pose);

    // Set the initial state value
    this->vehState.siteNorthing = pose.pos.x;
    this->vehState.siteEasting = pose.pos.y;
    this->vehState.siteYaw = pose.rot;

    MSG("start %s", this->cmdline.start_arg);
  }
  // Do some checks
//  if (!this->cmdline.rndf_given)
//    return ERROR("rndf must be specified");
  
  return 0;
}
