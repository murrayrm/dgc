/*
 * Desc: Tree for the RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */

#include "Tree.hh"
#include <math.h>
#include <float.h>
#include <alice/AliceConstants.h>
#include <temp-planner-interfaces/Log.hh>


#define PI 3.14159265

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

Tree::Tree() {
  root_ptr = NULL;
  size = 0;
}


Tree::~Tree() { 
}

//Make the root of the tree
void Tree::makeRoot(float x, float y) {

  nodes.clear();
  size = 0;

  root_ptr = new TreeNode;
  root_ptr->x = x;
  root_ptr->y = y;
  root_ptr->id=size;
  nodes.push_back(root_ptr);

  size = 1;


}

//Make the root of the tree
void Tree::makeRoot(float x, float y, float rot) {

  nodes.clear();
  size = 0;


  root_ptr = new TreeNode;
  root_ptr->x = x;
  root_ptr->y = y;
  root_ptr->rot = rot;
  root_ptr->id=size;
  nodes.push_back(root_ptr);

  size = 1;


}

//Make the root of the tree
void Tree::makeRoot(float x, float y, float rot, float steerAngle) {

  nodes.clear();
  size = 0;


  root_ptr = new TreeNode;
  root_ptr->x = x;
  root_ptr->y = y;
  root_ptr->rot = rot;
  root_ptr->steerAngle=steerAngle;
  root_ptr->id=size;
  nodes.push_back(root_ptr);

  size = 1;


}

//Extends the tree
int Tree::extendTree(TreeNode* parent_ptr, float x, float y) {
  TreeNode *node_ptr;

  node_ptr = new TreeNode;
  node_ptr->x = x;
  node_ptr->y = y;
  node_ptr->parent = parent_ptr->id;
  node_ptr->parent_ptr = parent_ptr;
  node_ptr->id = size;
  node_ptr->path = false;
  nodes.push_back(node_ptr);

  size++;

  return 0;
}

//Extends the tree
int Tree::extendTree(TreeNode* parent_ptr, float x, float y, float rot) {
  TreeNode *node_ptr;

  node_ptr = new TreeNode;
  node_ptr->x=x;
  node_ptr->y=y;
  node_ptr->rot=rot;
  node_ptr->parent=parent_ptr->id;
  node_ptr->parent_ptr=parent_ptr;
  node_ptr->id=size;
  node_ptr->path=false;
  nodes.push_back(node_ptr);

  size++;

  return 0;
}

//Extends the tree
int Tree::extendTree(TreeNode* parent_ptr, float x, float y, float rot, float steerAngle) {
  TreeNode *node_ptr;

  // Put a cap of number of children to a node
  if(parent_ptr->children.size() > 4)
    return -1;

  node_ptr = new TreeNode;
  node_ptr->x=x;
  node_ptr->y=y;
  node_ptr->rot=rot;
  node_ptr->steerAngle=steerAngle;
  node_ptr->parent=parent_ptr->id;
  node_ptr->parent_ptr=parent_ptr;
  node_ptr->id=size;
  node_ptr->path=false;
  node_ptr->dead = 0;
  nodes.push_back(node_ptr);

  Log::getStream(10) << "Extending with node: " << size << ", " << x << ", " << y << ", " << parent_ptr->id << endl;

  // Add the new node as a child to the parent
  parent_ptr->children.push_back(node_ptr);

  size++;

  return 0;
}

//Finds the closest node to a given point with euclidian distance
TreeNode* Tree::closest(float x, float y){
  TreeNode* min_n, * nn;
  unsigned int i;  
  float min_dist, dist;
  min_dist = FLT_MAX;

  for(i = 0; i < nodes.size(); i++){
    nn=nodes.front();
    dist=sqrt( ((x - nn->x) * (x - nn->x)) + ((y - nn->y) * (y - nn->y)) );
    nodes.push_back(nn);
    nodes.pop_front();
    if(dist < min_dist) {
      min_dist = dist;
      min_n = nn;
    }
  }
  return min_n;
}

//Finds the closest node to a given point with euclidian distance
TreeNode* Tree::closest(float x, float y, float &min_dist){
  TreeNode* min_n, * nn;
  unsigned int i;  
  float dist;
  min_dist = FLT_MAX;

  for(i = 0; i < nodes.size(); i++){
    nn=nodes.front();
    dist=sqrt( ((x - nn->x) * (x - nn->x)) + ((y - nn->y) * (y - nn->y)) );
    nodes.push_back(nn);
    nodes.pop_front();
    if(dist < min_dist) {
      min_dist = dist;
      min_n = nn;
    }
  }
  return min_n;
}


// Return a list of the nodes closest to (x,y)
list<TreeNode*> Tree::closestArr(float x, float y){
  vector<TreeNode*> sorted;
  vector<TreeNode*>::iterator it;
  list<TreeNode*> sort;
  int i, j, k;
  TreeNode* node_ptr, *sortNode_ptr;
  float dist, sortDist, prevDist;

  for(i = 0; i < nodes.size(); i++){
    node_ptr = nodes.front();
    nodes.pop_front();
    nodes.push_back(node_ptr);
    if(sorted.empty())
      sorted.push_back(node_ptr);
    else{
      dist = sqrt( ((x - node_ptr->x) * (x - node_ptr->x)) + ((y - node_ptr->y) * (y - node_ptr->y)) );
      it = sorted.begin();
      k = sorted.size();
      for(j = 0; j < k; j++){
        sortNode_ptr = sorted.at(j);
        sortDist = sqrt( ((x - sortNode_ptr->x) * (x - sortNode_ptr->x)) + ((y - sortNode_ptr->y) * (y - sortNode_ptr->y)) );
        if(dist<sortDist){
          sorted.insert(it, node_ptr);
          break;
        }
        it++;
      }
      if(k == sorted.size()){
        sorted.push_back(node_ptr);
      }
    }
  }

  Log::getStream(5) << "nodes.size(), sorted.size(): " << nodes.size() << ", " << sorted.size() <<endl;

  if(sorted.size() < 100){
    for(i = 0; i < sorted.size(); i++){
      node_ptr = sorted.at(i);
      dist = sqrt( ((x - node_ptr->x) * (x - node_ptr->x)) + ((y - node_ptr->y) * (y - node_ptr->y)) );
      Log::getStream(5) << "Node, dist: " << node_ptr->id << ", " << dist <<endl;
      if(i > 0){
        Log::getStream(5) << "Dist diff: " << dist - prevDist <<endl;
        // Only add nodes to the list that have a difference in the distance over some constant
        if(dist - prevDist > 0.5){ 
          sort.push_back(node_ptr);
          prevDist = dist;
        }
      }
      else{
        sort.push_back(node_ptr);
        prevDist = dist;
      }
    }
  }
  else{
    for(i = 0; i < 100; i++){
      node_ptr = sorted.at(i);
      dist = sqrt( ((x - node_ptr->x) * (x - node_ptr->x)) + ((y - node_ptr->y) * (y - node_ptr->y)) );
      Log::getStream(5) << "Node, dist: " << node_ptr->id << ", " << dist <<endl;
      if(i > 0){
        Log::getStream(5) << "Dist diff: " << dist - prevDist <<endl;
        // Only add nodes to the list that have a difference in the distance over some constant
        if(dist - prevDist > 0.5){ 
          sort.push_back(node_ptr);
          prevDist = dist;
        }
      }
      else{
        sort.push_back(node_ptr);
        prevDist = dist;
      }
    }
  }

  Log::getStream(5) << "Sort.size(): " << sort.size() <<endl;

  return sort;
}



//Finds the closest node to a given point with euclidian distance
TreeNode* Tree::closestDubin(float px, float py){
  int i;
  float x, xn, y, yn, theta;
  float min_dist = FLT_MAX;  
  float rho, L, df, dc, thetac, alpha;  
  TreeNode* min_n, *nn;
  rho = VEHICLE_MIN_TURNING_RADIUS;

  for(i = 0; i < nodes.size(); i++){
    nn=nodes.front();

    // Moving coordinate frame
    xn = px - nn->x;
    yn = px - nn->y;
    theta = nn->rot;
    x = xn*cos(theta) + yn*sin(theta);
    y = yn*cos(theta) - xn*sin(theta);

    nodes.push_back(nn);
    nodes.pop_front();
    
    // Dubins length
    df = sqrtf(pow(x,2) + pow((fabs(y) + rho),2));
    dc = sqrtf(pow(x,2) + pow((fabs(y) - rho),2));
    thetac = atan2(x,(rho-fabs(y)));
    if(thetac < 0)
      thetac += 2*PI;
    alpha = acos((5*pow(rho,2) - pow(df,2))/(4*pow(rho,2)));

    if(sqrtf(pow(x,2) + pow((y - rho),2)) < rho or sqrtf(pow(x,2) + pow((y + rho),2)) < rho){
      L = rho*(2*PI - alpha + asin(dc*sin(thetac)/df) + asin(rho*sin(alpha)/df));
    }
    else{
      L = sqrtf(pow(dc,2) - pow(rho,2)) + rho*(thetac - acos(rho/dc));
    }
    if(L < min_dist){
      min_dist = L;
      min_n = nn;
    }
  }
  return min_n;
}

//Finds the closest node to a given point with euclidian distance
TreeNode* Tree::closestDubin(float px, float py, float &min_dist){
  int i;
  float x, xn, y, yn, theta;
  min_dist = FLT_MAX;  
  float rho, L, df, dc, thetac, alpha;  
  TreeNode* min_n, *nn;
  rho = VEHICLE_MIN_TURNING_RADIUS;

  for(i=0;i<size;i++){
    nn=nodes.front();

    // Moving coordinate frame
    xn = px - nn->x;
    yn = px - nn->y;
    theta = nn->rot;
    x = xn*cos(theta) + yn*sin(theta);
    y = yn*cos(theta) - xn*sin(theta);

    nodes.push_back(nn);
    nodes.pop_front();
    
    // Dubins length
    df = sqrtf(pow(x,2) + pow((fabs(y) + rho),2));
    dc = sqrtf(pow(x,2) + pow((fabs(y) - rho),2));
    thetac = atan2(x,(rho-fabs(y)));
    if(thetac < 0)
      thetac += 2*PI;
    alpha = acos((5*pow(rho,2) - pow(df,2))/(4*pow(rho,2)));

    if(sqrtf(pow(x,2) + pow((y - rho),2)) < rho or sqrtf(pow(x,2) + pow((y + rho),2)) < rho){
      L = rho*(2*PI - alpha + asin(dc*sin(thetac)/df) + asin(rho*sin(alpha)/df));
    }
    else{
      L = sqrtf(pow(dc,2) - pow(rho,2)) + rho*(thetac - acos(rho/dc));
    }
    if(L < min_dist){
      min_dist = L;
      min_n = nn;
    }
  }
  return min_n;
}

// Root the tree from the id node, removing dead nodes
int Tree::reRoot(int id){
  TreeNode *node_ptr;
  int i, k;
  Log::getStream(10) << "Rerooting from id: " << id << endl;
  node_ptr = nodes.front();
  check(node_ptr, id);

  k = nodes.size();

  for(i = 0; i < k; i++){
    node_ptr = nodes.front();
    nodes.pop_front();
    if(node_ptr->dead != 1)
      nodes.push_back(node_ptr);
    else{
      Log::getStream(10) << "Removing: " << node_ptr->id << ", " << node_ptr->dead <<endl;
      delete node_ptr;
      node_ptr = NULL;
    }
  }

  if(nodes.size() == 0)
    return -1;
  else
    return 0;
}

// Recursively check if the nodes are still reachable
void Tree::check(TreeNode *node_ptr, int id){
  int i, k;
  TreeNode* child_ptr;

  k = node_ptr->children.size();

  Log::getStream(10) << "Checking: " << node_ptr->id << ", " << k  << ", " <<endl;
  if(node_ptr->id != id){
    node_ptr->dead = 1;

    if(k == 0)
      return;

    for(i = 0; i < k; i++){
      child_ptr = node_ptr->children.front();
      node_ptr->children.pop_front();
      node_ptr->children.push_back(child_ptr);
      Log::getStream(10) << "Will Check: " << child_ptr->id << "(" << node_ptr->id << ")" << ", " << i << " < " << k <<endl;
      check(child_ptr, id);
    }
    return;
  }
  else{
    return;
  }

}
