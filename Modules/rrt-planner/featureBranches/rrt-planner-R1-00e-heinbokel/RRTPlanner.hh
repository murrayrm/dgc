/*
 * Desc: Path planner based on RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */
//#include <stdlib.h>
// Dependencies
#include <temp-planner-interfaces/PlanGraph.hh>
#include <temp-planner-interfaces/PlanGraphPath.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include "Tree.hh"
#include <list>
#include <stdio.h>
#include "frames/point2_uncertain.hh"
#include "frames/point2.hh"
#include "map/Map.hh"
#include "map/MapPrior.hh"



/// @brief Basic planner for searching the graph.
class RRTPlanner
{
  public:

  /// @brief Default constructor
  RRTPlanner(PlanGraph *graph);

  /// @brief Destructor
  ~RRTPlanner();

  public:

  /// @brief Main function that plans the path
  int planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart,
                PlanGraphNode *nodeGoal, PlanGraphPath *path, Map *map, float steerAngle);

  int planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart,
                PlanGraphNode *nodeGoal, PlanGraphPath *path);

  int setState(StateProblem_t *state_problem);

  TreeNode nodes[100];

  TreeNode path[100];

  PlanGraphNode* sample[20000];

  point2 treePoints[10000];

  int sampleSize;

  int pointSize;

  int pathsize;

  private:

  void buildRRT(PlanGraphNode *nodeGoal);

  void readConfig();

  // Generate the best plan
  void genPlan(PlanGraphNode *nodeGoal);

  void genPath(PlanGraphPath *path);

  int extendRRT(float x, float y);

  void buildPath(int steps);

  bool checkConstraints(float x, float y);

  bool onRoad(float x, float y);

  bool obstacleFree(float x, float y);

  void addSample(float x, float y);

  void addPoint(float x, float y);

  // The graph we are using
  PlanGraph *graph;

  bool tryConnect(TreeNode *node, float x, float y);

  Tree *treeP;

  std::list<TreeNode*> pathList;

  PlanGraphNode* startNode;

  PlanGraphNode* goalNode;

  Map *m_map;

  float steer;

  bool zone;

  bool pathFound;

  int pathSize;
  
  FSM_state_t currState;

  //Parameters
  static int NUM_SAMPLES;
  static int STEP_SIZE;
  static bool USE_BIAS;
  static bool USE_DYN;
  static float DYN_STEP;
  static float DELTA;
  static bool CON_STEER;
  static float CON_W;
  static bool USE_DUBIN;
  static bool KEEP_PATH;
  static bool KEEP_TREE;
  static float BIAS_SIG_THETA;
  static float BIAS_SIG_R;
  static float BIAS_R; 
  static float GOAL_BIAS;
};
