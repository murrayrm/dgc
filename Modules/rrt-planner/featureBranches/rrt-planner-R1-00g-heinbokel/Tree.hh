/*
 * Desc: Tree for the RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */

#ifndef TREE_HH
#define TREE_HH

#include <stdio.h>
#include <list>
#include <vector>
#include <cctype>
using namespace std;

struct TreeNode
{
  float x;

  float y;

  float rot;

  float steerAngle;

  int parent;

  TreeNode* parent_ptr;

  list<TreeNode*> children;

  int id;

  bool path;

  int dead;

};

class Tree {
  public:
 
  Tree();

  ~Tree();

  void makeRoot(float x, float y);

  void makeRoot(float x, float y, float rot);

  void makeRoot(float x, float y, float rot, float steerAngle);

  int extendTree(TreeNode* parent_ptr, float x, float y);

  int extendTree(TreeNode* parent_ptr, float x, float y, float rot);

  int extendTree(TreeNode* parent_ptr, float x, float y, float rot, float steerAngle);

  TreeNode* closest(float x, float y);

  TreeNode* closest(float x, float y, float &dist);

  list<TreeNode*> closestArr(float x, float y);

  TreeNode* closestDubin(float px, float py);

  TreeNode* closestDubin(float px, float py, float &min_dist);

  int reRoot(int id);

  list<TreeNode*> nodes;

  private:

  void check(TreeNode *node_ptr, int id);

  TreeNode* root_ptr;

  int size;

  float sx, sy;

};


#endif
