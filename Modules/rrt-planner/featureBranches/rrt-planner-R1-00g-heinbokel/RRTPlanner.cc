/*
 * Desc: Path planner based on RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */

#include <assert.h>
#include <errno.h>
#include <float.h>
#include <stdio.h>
#include <temp-planner-interfaces/ConfigFile.hh>
#include <temp-planner-interfaces/Log.hh>
#include "RRTPlanner.hh"
#include <dgcutils/cfgfile.h>
#include <stdlib.h>
#include <ctime>
#include <cmath>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <cstdlib>
#include <alice/AliceConstants.h>
#define PI 3.14159265

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Common macros
#ifndef MIN
#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif

// Euclidian distance between two points
#define DISTANCE(x1, y1, x2, y2) \
  sqrtf( (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) )

int RRTPlanner::NUM_SAMPLES = 50;
int RRTPlanner::STEP_SIZE = 1;
bool RRTPlanner::USE_BIAS = 0;
bool RRTPlanner::USE_DYN = 0;
float RRTPlanner::DYN_STEP = 1;
float RRTPlanner::DELTA = 0.1;
bool RRTPlanner::CON_STEER = 0;
float RRTPlanner::CON_W = 0.1;
bool RRTPlanner::USE_DUBIN = 0;
bool RRTPlanner::KEEP_PATH = 0;
bool RRTPlanner::KEEP_TREE = 0;
float RRTPlanner::BIAS_SIG_THETA = PI/4;
float RRTPlanner::BIAS_SIG_R = 20;
float RRTPlanner::BIAS_R = 10;
float RRTPlanner::GOAL_BIAS = 0;
bool RRTPlanner::TEST_NEW = 0;

// Constructor 
RRTPlanner::RRTPlanner(PlanGraph *graph)
{
  Log::getStream(1)<<"Starting RRT-planner."<<endl;
  
  readConfig();

  this->graph = graph;

  pathsize = 0; 

  pointSize = 0;

  tree_ptr = new Tree();

//  steer = 0;

  zone = false;

  pathFound = false;

  currState = STOP_INT;

  closeHeur = true;

  return;
}

// Destructor 
RRTPlanner::~RRTPlanner()
{
  // TODO Clean up
  return;
}

void RRTPlanner::readConfig()
{
  char *path;
  Log::getStream(1)<<"Reading RRT-planner config file."<<endl;
  path=dgcFindConfigFile("rrtplanner.conf","rrt-planner");
  FILE* configFile = fopen(path,"r");
  if (configFile == NULL) {
    Log::getStream(1)<<"Warning: Couldn't read RRT-Planner configuration file."<<endl;
  }
  else {
    ConfigFile config(path);
    config.readInto(NUM_SAMPLES, "NUM_SAMPLES");
    config.readInto(STEP_SIZE, "STEP_SIZE");
    config.readInto(USE_BIAS, "USE_BIAS");
    config.readInto(USE_DYN, "USE_DYN");
    config.readInto(DYN_STEP, "DYN_STEP");
    config.readInto(DELTA, "DELTA");
    config.readInto(CON_STEER, "CON_STEER");
    config.readInto(CON_W, "CON_W");
    config.readInto(USE_DUBIN, "USE_DUBIN");
    config.readInto(KEEP_PATH, "KEEP_PATH");
    config.readInto(KEEP_TREE, "KEEP_TREE");
    config.readInto(BIAS_SIG_THETA, "BIAS_SIG_THETA");
    config.readInto(BIAS_SIG_R, "BIAS_SIG_R");
    config.readInto(BIAS_R, "BIAS_R");
    config.readInto(GOAL_BIAS, "GOAL_BIAS");
    config.readInto(TEST_NEW, "TEST_NEW");

    Log::getStream(1)<<"Info: Read RRT-Planner configuration successfully."<<endl;
    fclose(configFile);

    // Log all the parameters
    Log::getStream(1) << "NUM_SAMPLES: " << NUM_SAMPLES << endl;
    Log::getStream(1) << "STEP_SIZE: " << STEP_SIZE << endl;
    Log::getStream(1) << "USE_BIAS: " << USE_BIAS << endl;
    Log::getStream(1) << "USE_DYN: " << USE_DYN << endl;
    Log::getStream(1) << "DYN_STEP: " << DYN_STEP << endl;
    Log::getStream(1) << "DELTA: " << DELTA << endl;
    Log::getStream(1) << "CON_STEER: " << CON_STEER << endl;
    Log::getStream(1) << "CON_W: " << CON_W << endl;
    Log::getStream(1) << "USE_DUBIN: " << USE_DUBIN << endl;
    Log::getStream(1) << "KEEP_PATH: " << KEEP_PATH << endl;
    Log::getStream(1) << "KEEP_TREE: " << KEEP_TREE << endl;
    Log::getStream(1) << "BIAS_SIG_THETA: " << BIAS_SIG_THETA << endl;
    Log::getStream(1) << "BIAS_SIG_R: " << BIAS_SIG_R << endl;
    Log::getStream(1) << "BIAS_R: " << BIAS_R << endl;
    Log::getStream(1) << "GOAL_BIAS: " << GOAL_BIAS << endl;
    Log::getStream(1) << "TEST_NEW: " << TEST_NEW << endl;
  }
}

int RRTPlanner::setState(StateProblem_t *state_problem){
  FSM_state_t oldState;

  oldState = currState;

  currState = state_problem->state;
  currFlag = state_problem->flag;
  if(TEST_NEW){
    if(currFlag == PASS_REV)
      zone = true;
  }
  if(oldState != currState and oldState == DRIVE  )
    pathList.clear();
  if(currState == DRIVE)
    closeHeur = true;
  else
    closeHeur = false;

  return 0;
}

// Main function that plans the path
int RRTPlanner::planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart,
                          PlanGraphNode *nodeGoal, PlanGraphPath *path, Map *map, float steerAngle){

  // Check for valid start/end conditions
  if (!nodeStart)
    return -1;
  if (!nodeGoal)
    return -1;

  prevPathSize = pathSize;

  Log::getStream(10) << "Planning rrt-path from (x,y,th,st) to (x,y): " << nodeStart->pose.pos.x << ", " << nodeStart->pose.pos.y << ", " << nodeStart->pose.rot << ", " << steerAngle << ", " << nodeGoal->pose.pos.x << ", " << nodeGoal->pose.pos.y << endl;

  steer = steerAngle;

  m_map = map;

  startNode = nodeStart;
  Log::getStream(5) << "Starting node ID, x, y: " << startNode->nodeId << ", " << startNode->pose.pos.x << ", " << startNode->pose.pos.y << endl;
  goalNode = nodeGoal;
  Log::getStream(5) << "Goal node x, y: " << goalNode->pose.pos.x << ", " << goalNode->pose.pos.y << endl;

  buildRRT(nodeGoal);
  genPlan(nodeGoal);
  genPath(path);

  addPoint();

  if( prevPathSize < 3 and pathSize < 3)
    return -1;

  return 0;
}

// Main function that plans the path for the viewer
int RRTPlanner::planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart,
                          PlanGraphNode *nodeGoal, PlanGraphPath *path){

  startNode = nodeStart;
  goalNode = nodeGoal;

  buildRRT(nodeGoal);
  genPlan(nodeGoal);
  genPath(path);


  return 0;
}

// This function starts to build the RRT graph
void RRTPlanner::buildRRT(PlanGraphNode *goalP){
  int i, k, id;
  float a,b;
  TreeNode* n,* pathn;
  // Biasing parameters
  float sigR, sigTh, r0, th0, x0, y0, nR, nTh, r, th;

  if(!KEEP_TREE)
    sampleSize = 0;

  // Random number generation
  const gsl_rng_type *T;
  gsl_rng *rand;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  rand = gsl_rng_alloc(T);
  gsl_rng_set(rand,time(NULL));

  if(currState == STOP_INT){
    sigR = 10;
    sigTh = PI/3;
    r0 = 3;
  }
  else{
    sigR = BIAS_SIG_R;
    sigTh = BIAS_SIG_THETA;
    r0 = BIAS_R;
  }  
  th0 = startNode->pose.rot;
  x0 = startNode->pose.pos.x; 
  y0 = startNode->pose.pos.y;   

  k = pathList.size();
  
  if(KEEP_TREE){
    if(tree_ptr->nodes.size() == 0)
      tree_ptr->makeRoot(x0, y0, th0, steer);
    else if(startNode->flags.isReserved == 1){
      if(tree_ptr->reRoot(startNode->nodeId) < 0)
        tree_ptr->makeRoot(x0, y0, th0, steer);        
    }
    else 
      tree_ptr->makeRoot(x0, y0, th0, steer);
  }
  else{
    if(KEEP_PATH){
      if(k > 0){
        if(USE_DYN){
          if(CON_STEER){
            pathn = tree_ptr->closest(x0, y0);
            id = pathn->id;
            tree_ptr->makeRoot(pathn->x, pathn->y, pathn->rot, pathn->steerAngle);
            for(i=0;i<k;i++){
              pathn = pathList.back();
              pathList.pop_back();
              if(pathn->id > id){ 
                n = tree_ptr->closest(pathn->x, pathn->y);
                tree_ptr->extendTree(n, pathn->x, pathn->y, pathn->rot, pathn->steerAngle);
              }
            }
            pathList.clear();
          }
          else{
            tree_ptr->makeRoot(x0, y0, th0);
            for(i=0;i<k;i++){
              pathn = pathList.back();
              pathList.pop_back();        
              n = tree_ptr->closest(pathn->x,pathn->y);
              tree_ptr->extendTree(n,pathn->x,pathn->y,pathn->rot);
            }
            pathList.clear();
          }
        }
        else{
          tree_ptr->makeRoot(x0, y0);
          for(i=0;i<k;i++){
            pathn = pathList.back();
            pathList.pop_back();        
            n = tree_ptr->closest(pathn->x,pathn->y);
            tree_ptr->extendTree(n,pathn->x,pathn->y);
          }
          pathList.clear();
        }
      }
      else{
        //Creates the root
        if(USE_DYN){
          if(CON_STEER){
            tree_ptr->makeRoot(x0, y0, th0, steer);
          }
          else{
            tree_ptr->makeRoot(x0, y0, th0);
          }
        }
        else{
          tree_ptr->makeRoot(x0, y0);
        }
      }
    }
    else{
      pathList.clear();
      if(USE_DYN)
        tree_ptr->makeRoot(x0, y0, th0);
      else
        tree_ptr->makeRoot(x0, y0);
    }
  }

  // Take random sample and try to extend the tree
  for(i=0; i < NUM_SAMPLES; i++){
    if(USE_BIAS){
      if(gsl_rng_uniform(rand) >= GOAL_BIAS){
        nR = gsl_ran_ugaussian(rand);
        nTh = gsl_ran_ugaussian(rand);
  
        r = sigR*fabs(nR) + r0;
        th = sigTh*nTh + th0;

        a = x0 + r*cos(th);
        b = y0 + r*sin(th);
      }
      else{
        a = goalP->pose.pos.x;
        b = goalP->pose.pos.y;
      }
    }
    else{
      a = gsl_rng_uniform(rand)*100;
      b = gsl_rng_uniform(rand)*100;
      if(gsl_rng_uniform(rand) > 0.5)
        a = -a;
      if(gsl_rng_uniform(rand) > 0.5)
        b = -b;
    }

    Log::getStream(10)<<"Sample: " << a << ", " << b << endl;

    if(m_map){
      if(onRoad(a,b)){
        if(extendRRT(a,b) < 0)
          Log::getStream(10)<<"Could not extend tree" << endl;
        else
          Log::getStream(10)<<"Extend tree OK" << endl;      
      }
    }
    else
      extendRRT(a,b);   
  }

  //Build a rrt for the rrt-viewer (funkar inte med KEEP_TREE)
  if(!KEEP_TREE){
    buildPath(NUM_SAMPLES);
  }

}

// Generate the best plan
void RRTPlanner::genPlan(PlanGraphNode *nodeGoal){
  TreeNode* n;
  float dist;

  // Clear the pathlist just to be sure
  pathList.clear();

  //Pick the node closest (euclidian) to the goal and backtrack
  n = tree_ptr->closest(goalNode->pose.pos.x, goalNode->pose.pos.y, dist);
  //TEST, if closest is root, go somewhere
  if(KEEP_TREE)
    if(n->id == startNode->nodeId)
      n = tree_ptr->nodes.front();

  if(dist < 2)
    pathFound = true;
  else
    pathFound = false;

  Log::getStream(4)<<"Path found? " << pathFound << endl; 

  if(KEEP_TREE){
    n->path=true;
    if(startNode->flags.isReserved == 1){
      while(n->id != startNode->nodeId){
        pathList.push_back(n);
        n=n->parent_ptr;
        n->path=true;
      }
      //Remember the last node
      pathList.push_back(n);
    }
    else{
      while(n->id != 0){
        pathList.push_back(n);
        n=n->parent_ptr;
        n->path=true;
      }
      //Remember the last node
      pathList.push_back(n);
    }
  }
  else{
    if(pathFound){  
      n->path=true;
      while(n->id != 0){
        pathList.push_back(n);
        n=n->parent_ptr;
        n->path=true;
      }
      //Remember the last node
      pathList.push_back(n);
    }
    else{
      if(n->id !=0){
        n->path=true;
        while(n->id != 0){
          pathList.push_back(n);
          n=n->parent_ptr;
          n->path=true;
        }
        //Remember the last node
        pathList.push_back(n);
      }
      else{
        n = tree_ptr->nodes.front();
        if(n->id !=0){
          n->path=true;
          while(n->id != 0){
            pathList.push_back(n);
            n=n->parent_ptr;
            n->path=true;
          }
          //Remember the last node
          pathList.push_back(n);
        }
      }
    }
  }

  pathSize = pathList.size();
//  if(pathList.size() <= 1){
//    zone = true;
//    GOAL_BIAS = 1;
//  }
//  else{
//    zone = false;
//    GOAL_BIAS = 0.1;
//  }
}


// Generate the path for the vel-planner
void RRTPlanner::genPath(PlanGraphPath *path){
  int i,k;
  PlanGraphNode* pnodeP,* prevpnodeP;
  TreeNode *tnodeP;
  float alpha;

  // Set up the path
  path->pathLen = 0;
  path->valid = true;
  path->collideObs = 0;
  path->collideCar = 0;
  path->dist = 0;

  k=pathList.size();

  //Populate the PlanGraphPath 
  for(i = 0; i < k; i++){
    // Get the previous node to calculate the distance
    if(i>0){
      prevpnodeP = pnodeP;
    }

    // Take out and remove the last node in the list
    tnodeP = pathList.back();
    pathList.pop_back();

    pathList.push_front(tnodeP);

    // Create a new node for the path
    pnodeP = this->graph->allocNode(tnodeP->x,tnodeP->y);

    if(!USE_DYN){
      //Calculate the rotation
      if(i>0){
        alpha = atan2((pnodeP->pose.pos.y - prevpnodeP->pose.pos.y),(pnodeP->pose.pos.x - prevpnodeP->pose.pos.x));
        prevpnodeP->pose.rot = alpha;
      }
    }
    else{
      pnodeP->pose.rot = tnodeP->rot;
    }  

    // Test for KEEP_PATH
    pnodeP->nodeId = tnodeP->id;
    Log::getStream(10) << "Node id given: " << pnodeP->nodeId << endl;

    //Test
    pnodeP->status.obsSideDist = 3;
    pnodeP->flags.isReserved = 1;
  
    // Add the PlanGraphNode to the path
    path->nodes[i]=pnodeP;


    // Calculate the total distance along the path
    if(i>0){
      path->dist += vec2f_mag(vec2f_sub(pnodeP->pose.pos, prevpnodeP->pose.pos));
    }
    Log::getStream(10) << "Dist " << path->dist << endl;
    // Add the distance at each node
    path->dists[path->pathLen] = path->dist;

    // Only go forward for now
    path->directions[path->pathLen] = PLAN_GRAPH_PATH_FWD;

    // Calculate the total length of the path
    path->pathLen++;
  }
}



//Extend the RRT by finding out what the closest node is
int RRTPlanner::extendRRT(float x, float y){
  float nx,ny;    
  float dist;
  TreeNode* n;
  list<TreeNode*> closeList;
  int i;
  int k;

  //Find the closest node in the tree
  if(USE_DUBIN)
    n = tree_ptr->closestDubin(x, y);
  else{
    if(closeHeur)
      closeList = tree_ptr->closestArr(x,y);
    else
      n = tree_ptr->closest(x, y, dist);
  }

  if(USE_DYN){
    if(closeHeur){
      k = closeList.size();
      Log::getStream(5)<<"closeList.size(): " << k <<endl;
      for(i = 0; i < k; i++){
        n = closeList.front();
        Log::getStream(10)<<"Trying to connect: " << n->id <<endl;
        tryConnect(n,x,y);
        closeList.pop_front();
      }
    }
    else{
     //Try to connect the nodes with respect to dynamics
      if(!tryConnect(n,x,y)){
        if(n->id > 0)
          return tryConnect(n->parent_ptr,x,y);
        else
          return false;
      }
      else
        return true;
    }
  }
  else{
    //Calculate the step taken in the direction of closest node
    nx = (STEP_SIZE*((x - n->x) / dist)) + n->x;
    ny = (STEP_SIZE*((y - n->y) / dist)) + n->y;

//    if(m_map)
//      if(!onRoad(nx,ny))
//        return -1;

    //Extend the tree with the new node
    tree_ptr->extendTree(n,nx,ny);

    return 0;
  }
}

// Try to find a feasible path from node to point
bool RRTPlanner::tryConnect(TreeNode *node, float x, float y){
  float L, alpha, phi, t, theta, xG, yG, w, phi0;
  L = VEHICLE_AXLE_DISTANCE;
  xG = node->x;
  yG = node->y;
  alpha = atan2((y - yG),(x - xG));
  theta = node->rot;
  phi = alpha - theta;
  phi0 = node->steerAngle;
  if(zone)
    con_w = 0.19;
  else if(currState == DRIVE)
    con_w = CON_W;
  else
    con_w = 0.19;    

  if(CON_STEER){
    if(phi < - PI)
      phi += 2*PI;
    if(phi > PI)
      phi -= 2*PI;

    if(phi < -PI/2)
      phi = -PI/2;
    if(phi > PI/2)
      phi = PI/2;
    w = con_w*sin(phi);

    phi = phi0;
  }


  if(CON_STEER){
    for(t = 0; t < DYN_STEP; t += DELTA){
      phi += w*DELTA;
      theta += tan(phi)*DELTA/L;
      xG += cos(theta)*DELTA;
      yG += sin(theta)*DELTA;
    }
  }
  else{
    for(t = 0; t < DYN_STEP; t += DELTA){
      theta += tan(phi)*DELTA/L;
      phi = alpha - theta;
      xG += cos(theta)*DELTA;
      yG += sin(theta)*DELTA;
    }
  }

  if(m_map){
//    if(onRoad(xG,yG) and obstacleFree(xG,yG)){
    if(checkConstraints(xG, yG, theta)){
      tree_ptr->extendTree(node, xG, yG, theta, phi);
      Log::getStream(5)<<"Good point: "<< xG << ", " << yG <<endl;  
    }
    else{
      Log::getStream(5)<<"Bad point: "<< xG << ", " << yG <<endl;      
      return false;
    }
  }
  else
    tree_ptr->extendTree(node, xG, yG, theta, phi);

  return true;
}



// Build the tree for the viewer
void RRTPlanner::buildPath(int steps){
  int i;
  TreeNode* nodeP;

  for(i=0; i < steps + 1; i++){
    nodeP = tree_ptr->nodes.front();
    nodes[i] = *nodeP;
    pathsize++;
    tree_ptr->nodes.push_back(nodeP);
    tree_ptr->nodes.pop_front();
  }

}

bool RRTPlanner::checkConstraints(float x, float y, float rot){
  static float front, side, back;
  front = DIST_REAR_AXLE_TO_FRONT;
  side = MAX_FRONT_BUMPER_WIDTH/2;
  back = DIST_REAR_TO_REAR_AXLE;
/*
  Log::getStream(10)<<"OnRoad 1?: "<< onRoad(x + front*cos(rot) - side*sin(rot), y + side*cos(rot) + front*sin(rot)) <<endl;
  Log::getStream(10)<<"OnRoad 2?: "<< onRoad(x + front*cos(rot) + side*sin(rot), y - side*cos(rot) + front*sin(rot)) <<endl; 
  Log::getStream(10)<<"OnRoad 3?: "<< onRoad(x - back*cos(rot) - side*sin(rot), y + side*cos(rot) - back*sin(rot)) <<endl; 
  Log::getStream(10)<<"OnRoad 4?: "<< onRoad(x - back*cos(rot) + side*sin(rot), y - side*cos(rot) - back*sin(rot)) <<endl; 

  Log::getStream(10)<<"ObsFree 1?: "<< obstacleFree(x + front*cos(rot) - side*sin(rot), y + side*cos(rot) + front*sin(rot)) <<endl;
  Log::getStream(10)<<"ObsFree 2?: "<< obstacleFree(x + front*cos(rot) + side*sin(rot), y - side*cos(rot) + front*sin(rot)) <<endl; 
  Log::getStream(10)<<"ObsFree 3?: "<< obstacleFree(x - back*cos(rot) - side*sin(rot), y + side*cos(rot) - back*sin(rot)) <<endl;
  Log::getStream(10)<<"ObsFree 4?: "<< obstacleFree(x - back*cos(rot) + side*sin(rot), y - side*cos(rot) - back*sin(rot)) <<endl;
*/
  if(onRoad(x,y) and
//     onRoad(x + front*cos(rot) - side*sin(rot), y + side*cos(rot) + front*sin(rot)) and
//     onRoad(x + front*cos(rot) + side*sin(rot), y - side*cos(rot) + front*sin(rot)) and
//     onRoad(x - back*cos(rot) - side*sin(rot), y + side*cos(rot) - back*sin(rot)) and
//     onRoad(x - back*cos(rot) + side*sin(rot), y - side*cos(rot) - back*sin(rot)) and
     obstacleFree(x + front*cos(rot) - side*sin(rot), y + side*cos(rot) + front*sin(rot)) and
     obstacleFree(x + front*cos(rot) + side*sin(rot), y - side*cos(rot) + front*sin(rot)) )
//     obstacleFree(x - back*cos(rot) - side*sin(rot), y + side*cos(rot) - back*sin(rot)) and
//     obstacleFree(x - back*cos(rot) + side*sin(rot), y - side*cos(rot) - back*sin(rot)) )
    return true;
  else 
    return false;
}

// Check if the point is on a road
bool RRTPlanner::onRoad(float x, float y){
  if(zone)
    return true;
  int i, closest, side;
  LaneLabel currentLane, otherLane, goalLane;
  point2_uncertain newPoint, calcPoint, calcPointG, safePoint, safePointG;
  point2 curPos, finPos;
  point2arr_uncertain leftCur, rightCur, leftGo, rightGo;
  float dist, min_dist = 1000, alpha;

  curPos.x = startNode->pose.pos.x;
  curPos.y = startNode->pose.pos.y;
  newPoint.x = x;
  newPoint.y = y;
  finPos.x = goalNode->pose.pos.x;
  finPos.y = goalNode->pose.pos.y;

  if(m_map->getLane(currentLane, curPos) < 0)
    return true;
  if(m_map->getLane(goalLane, finPos) < 0)
    return true;

  
  Log::getStream(10)<<"Current Lane:"<< currentLane.segment << ", " << currentLane.lane <<endl;
  Log::getStream(10)<<"Goal Lane:"<< goalLane.segment << ", " << goalLane.lane <<endl;


  // Point in the same lane as Alice?
  if(m_map->isPointInLane(newPoint, currentLane)){
    Log::getStream(10)<<"Good point x, y:"<< newPoint.x << ", " << newPoint.y <<endl;
    return true;
  }
  else if(m_map->isPointInLane(newPoint, goalLane)){
    Log::getStream(10)<<"Good point x, y:"<< newPoint.x << ", " << newPoint.y <<endl;
    return true;
  }
  else{
    // Get the closest lane to the point
    m_map->getLane(otherLane, newPoint);
    // DEBUG
    Log::getStream(10)<<"New Lane:"<< otherLane.lane << ", " << otherLane.segment  <<endl;
    // Is the point in the closest lane?
    if(m_map->isPointInLane(newPoint, otherLane)){
      // Does the lane has the same direction as the goal lane?
      if(m_map->isLaneSameDir(goalLane,otherLane)){
        // DEBUG
        return true;
      }
      else{
        Log::getStream(10)<<"Point x, y in opposite direction:"<< newPoint.x << ", " << newPoint.y <<endl;
        return false;
      }
    }
    else{
      // DEBUG
      Log::getStream(10)<<"Point x, y not in lane:"<< newPoint.x << ", " << newPoint.y <<endl;
    }
  }
  return inIntersect(x, y);
}

bool RRTPlanner::obstacleFree(float x, float y){
  int i;  
  int numEl;
  int collision;
  LaneLabel currentLane;
  point2 point;
  vector<MapElement> elarr;
  point2arr_uncertain polygon_uncertain;
  point2arr polygon;

  collision = 0;
  point.x = x;
  point.y = y;

  if(m_map->getLane(currentLane, point) < 0)
    return false;
  
  numEl = m_map->getObsInLane(elarr, currentLane);

  if(numEl < 1)
    return true;

  for(i = 0; i < numEl; i++){
    polygon_uncertain = elarr.at(i).geometry;
    polygon = point2arr(polygon_uncertain);

    if(polygon.is_inside_poly(point))
      collision++;
    
  }

  if(collision > 0)
    return false;
  else
    return true;
}

void RRTPlanner::addSample(float x, float y){
  PlanGraphNode *node;

  node = this->graph->allocNode(x,y);

  this->sample[sampleSize] = node;

  sampleSize++;

}

void RRTPlanner::addPoint(){
  int i;
  TreeNode* node_ptr;
  pointSize = 0;

 Log::getStream(5)<<"Tree size :"<< tree_ptr->nodes.size() <<endl;

  for(i = 0; i < tree_ptr->nodes.size(); i++){
    node_ptr = tree_ptr->nodes.front();
    tree_ptr->nodes.pop_front();
    livePoints[pointSize].x = node_ptr->x;
    livePoints[pointSize].y = node_ptr->y;
    Log::getStream(10)<<"Tree node id, parent: "<< node_ptr->id << ", " << node_ptr->parent <<endl;
    Log::getStream(10)<<"Point x, y :"<< livePoints[pointSize].x << ", " << livePoints[pointSize].x <<endl;
    pointSize++;
    tree_ptr->nodes.push_back(node_ptr);
  }
}

bool RRTPlanner::inIntersect(float x, float y){
  LaneLabel currentLane;
  point2 curPos, newPoint;
  point2arr leftbound;
  point2arr rightbound, interBound;
  int i, j;
  vector<PointLabel> exitLabels1, exitLabels2;

  curPos.x = startNode->pose.pos.x;
  curPos.y = startNode->pose.pos.y;
  newPoint.x = x;
  newPoint.y = y;

  // Get current lane
  if(m_map->getLane(currentLane, curPos) < 0){
    Log::getStream(10)<<"No lane found "<< endl;
    return false;
  }

  // Get array of exits
  if(m_map->getLaneExits(exitLabels1, exitLabels2, currentLane) < 0){
    Log::getStream(10)<<"No exits found "<< endl;
    return false;
  }
  Log::getStream(10)<<"Exitlabels size: "<< exitLabels1.size() << ", " << exitLabels2.size() << endl;
  for(i = 0; i < MIN(exitLabels1.size(), exitLabels2.size()); i++){
    if(m_map->getShortTransitionBounds(leftbound ,rightbound , exitLabels1.at(i), exitLabels2.at(i)) != -1){
      Log::getStream(10)<<"Bounds found! "<< endl;
      Log::getStream(10)<<"leftbound: "<< leftbound.size() << endl;
      for(j = 0; j < leftbound.size(); j++){
        Log::getStream(10)<<leftbound[j].x << ", " << leftbound[j].y << endl;
        interBound.push_back(leftbound[j]);
      }
      Log::getStream(10)<<"rightbound: "<< rightbound.size() << endl;
      for(j = rightbound.size() - 1; j >= 0 ; j--){
        Log::getStream(10)<<rightbound[j].x << ", " << rightbound[j].y << endl;
        interBound.push_back(rightbound[j]);      
      }
      Log::getStream(10)<<"interBound: "<< endl;
      for(j = 0; j < interBound.size(); j++){
        Log::getStream(10)<<interBound[j].x << ", " << interBound[j].y << endl;
      }
      if(interBound.is_inside_poly(newPoint)){
        Log::getStream(10)<<"Inside!" << endl;
        return true;
      }
      else
        Log::getStream(10)<<"Outside!" << endl;
      interBound.clear();
    }
  else
    Log::getStream(10)<<"No bounds: "<< endl;
  }

  return false;
}
