/*
 * Desc: Tree for the RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */

#include "Tree.hh"
#include <math.h>
#include <alice/AliceConstants.h>
#include <temp-planner-interfaces/Log.hh>

#define PI 3.14159265

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

Tree::Tree() {

  rootP = NULL;
  size = 0;
}


Tree::~Tree() { 
}

//Make the root of the tree
void Tree::makeRoot(float px, float py) {

  nodes.clear();
  size = 0;

  rootP = new TreeNode;
  rootP->x = px;
  rootP->y = py;
  rootP->id=size;
  nodes.push_back(rootP);

  size = 1;


}

//Make the root of the tree
void Tree::makeRoot(float px, float py, float prot) {

  nodes.clear();
  size = 0;


  rootP = new TreeNode;
  rootP->x = px;
  rootP->y = py;
  rootP->rot = prot;
  rootP->id=size;
  nodes.push_back(rootP);

  size = 1;


}

//Make the root of the tree
void Tree::makeRoot(float px, float py, float prot, float steerAngle) {

  nodes.clear();
  size = 0;


  rootP = new TreeNode;
  rootP->x = px;
  rootP->y = py;
  rootP->rot = prot;
  rootP->steerAngle=steerAngle;
  rootP->id=size;
  nodes.push_back(rootP);

  size = 1;


}

//Extends the tree
TreeNode* Tree::extendTree(TreeNode* parP, float px, float py) {
  TreeNode *nn;

  nn = new TreeNode;
  nn->x=px;
  nn->y=py;
  nn->parent=parP->id;
  nn->parentP=parP;
  nn->id=size;
  nn->path=false;
  nodes.push_back(nn);

  size++;

  return nn;
}

//Extends the tree
TreeNode* Tree::extendTree(TreeNode* parent_ptr, float px, float py, float prot) {
  TreeNode *node_ptr;

  node_ptr = new TreeNode;
  node_ptr->x=px;
  node_ptr->y=py;
  node_ptr->rot=prot;
  node_ptr->parent=parent_ptr->id;
  node_ptr->parentP=parent_ptr;
  node_ptr->id=size;
  node_ptr->path=false;
  nodes.push_back(node_ptr);

  size++;

  return node_ptr;
}

//Extends the tree
TreeNode* Tree::extendTree(TreeNode* parent_ptr, float px, float py, float prot, float steerAngle) {
  TreeNode *node_ptr;
//  TreeNode *nodeParent_ptr;

  node_ptr = new TreeNode;
  node_ptr->x=px;
  node_ptr->y=py;
  node_ptr->rot=prot;
  node_ptr->steerAngle=steerAngle;
  node_ptr->parent=parent_ptr->id;
  node_ptr->parentP=parent_ptr;
  node_ptr->id=size;
  node_ptr->path=false;
  nodes.push_back(node_ptr);

  // Add the new node as a child to the parent
  parent_ptr->children.push_back(node_ptr);

  size++;

  return node_ptr;
}

//Finds the closest node to a given point with euclidian distance
TreeNode* Tree::closest(float px, float py){
  TreeNode* min_n, * nn;
  int i;  
  float min_dist, dist;
  min_dist = 10000;

  for(i=0;i<size;i++){
    nn=nodes.front();
    dist=sqrt( ((px - nn->x) * (px - nn->x)) + ((py - nn->y) * (py - nn->y)) );
    nodes.push_back(nn);
    nodes.pop_front();
    if(dist < min_dist) {
      min_dist = dist;
      min_n = nn;
    }
  }
  return min_n;
}

//Finds the closest node to a given point with euclidian distance
TreeNode* Tree::closest(float px, float py, float &min_dist){
  TreeNode* min_n, * nn;
  int i;  
  float dist;
  min_dist = 10000;

  for(i=0;i<size;i++){
    nn=nodes.front();
    dist=sqrt( ((px - nn->x) * (px - nn->x)) + ((py - nn->y) * (py - nn->y)) );
    nodes.push_back(nn);
    nodes.pop_front();
    if(dist < min_dist) {
      min_dist = dist;
      min_n = nn;
    }
  }
  return min_n;
}

//Finds the closest node to a given point with euclidian distance
TreeNode* Tree::closestDubin(float px, float py){
  int i;
  float x, xn, y, yn, theta;
  float min_dist = 10000;  
  float rho, L, df, dc, thetac, alpha;  
  TreeNode* min_n, *nn;
  rho = VEHICLE_MIN_TURNING_RADIUS;

  for(i=0;i<size;i++){
    nn=nodes.front();

    // Moving coordinate frame
    xn = px - nn->x;
    yn = px - nn->y;
    theta = nn->rot;
    x = xn*cos(theta) + yn*sin(theta);
    y = yn*cos(theta) - xn*sin(theta);

    nodes.push_back(nn);
    nodes.pop_front();
    
    // Dubins length
    df = sqrtf(pow(x,2) + pow((fabs(y) + rho),2));
    dc = sqrtf(pow(x,2) + pow((fabs(y) - rho),2));
    thetac = atan2(x,(rho-fabs(y)));
    if(thetac < 0)
      thetac += 2*PI;
    alpha = acos((5*pow(rho,2) - pow(df,2))/(4*pow(rho,2)));

    if(sqrtf(pow(x,2) + pow((y - rho),2)) < rho or sqrtf(pow(x,2) + pow((y + rho),2)) < rho){
      L = rho*(2*PI - alpha + asin(dc*sin(thetac)/df) + asin(rho*sin(alpha)/df));
    }
    else{
      L = sqrtf(pow(dc,2) - pow(rho,2)) + rho*(thetac - acos(rho/dc));
    }
    if(L < min_dist){
      min_dist = L;
      min_n = nn;
    }
  }
  return min_n;
}

//Finds the closest node to a given point with euclidian distance
TreeNode* Tree::closestDubin(float px, float py, float &min_dist){
  int i;
  float x, xn, y, yn, theta;
  min_dist = 10000;  
  float rho, L, df, dc, thetac, alpha;  
  TreeNode* min_n, *nn;
  rho = VEHICLE_MIN_TURNING_RADIUS;

  for(i=0;i<size;i++){
    nn=nodes.front();

    // Moving coordinate frame
    xn = px - nn->x;
    yn = px - nn->y;
    theta = nn->rot;
    x = xn*cos(theta) + yn*sin(theta);
    y = yn*cos(theta) - xn*sin(theta);

    nodes.push_back(nn);
    nodes.pop_front();
    
    // Dubins length
    df = sqrtf(pow(x,2) + pow((fabs(y) + rho),2));
    dc = sqrtf(pow(x,2) + pow((fabs(y) - rho),2));
    thetac = atan2(x,(rho-fabs(y)));
    if(thetac < 0)
      thetac += 2*PI;
    alpha = acos((5*pow(rho,2) - pow(df,2))/(4*pow(rho,2)));

    if(sqrtf(pow(x,2) + pow((y - rho),2)) < rho or sqrtf(pow(x,2) + pow((y + rho),2)) < rho){
      L = rho*(2*PI - alpha + asin(dc*sin(thetac)/df) + asin(rho*sin(alpha)/df));
    }
    else{
      L = sqrtf(pow(dc,2) - pow(rho,2)) + rho*(thetac - acos(rho/dc));
    }
    if(L < min_dist){
      min_dist = L;
      min_n = nn;
    }
  }
  return min_n;
}

int Tree::reRoot(int id){
  TreeNode *node_ptr;
  int i;

  node_ptr = nodes.front();
  check(node_ptr, id);

  for(i = 0; i < nodes.size(); i++){
    node_ptr = nodes.front();
    nodes.pop_front();
    if(node_ptr != NULL)
      nodes.push_back(node_ptr);
  }
}

void Tree::check(TreeNode *node_ptr, int id){
  int i;

  if(node_ptr->id != id){
    for(i = 0; i < node_ptr->children.size(); i++){
      check(node_ptr->children.front(), id);
      node_ptr->children.pop_front();
    }
    delete node_ptr;
    node_ptr = NULL;
  }
  else{
    return;
  }

}
