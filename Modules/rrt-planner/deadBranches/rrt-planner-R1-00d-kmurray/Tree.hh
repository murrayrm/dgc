/*
 * Desc: Tree for the RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */

#ifndef TREE_HH
#define TREE_HH

#include <stdio.h>
#include <list>

struct TreeNode
{
  float x;

  float y;

  float rot;

  float steerAngle;

  int parent;

  TreeNode* parentP;

  std::list<TreeNode*> children;

  int id;

  bool path;

};

class Tree {
  public:
 
  Tree();

  ~Tree();

  void makeRoot(float px, float py);

  void makeRoot(float px, float py, float prot);

  void makeRoot(float px, float py, float prot, float steerAngle);

  TreeNode* extendTree(TreeNode* parP, float px, float py);

  TreeNode* extendTree(TreeNode* parP, float px, float py, float prot);

  TreeNode* extendTree(TreeNode* parP, float px, float py, float prot, float steerAngle);

  TreeNode* closest(float px, float py);

  TreeNode* closest(float px, float py, float &dist);

  TreeNode* closestDubin(float px, float py);

  TreeNode* closestDubin(float px, float py, float &min_dist);

  int reRoot(int id);

  std::list<TreeNode*> nodes;

  private:

  void check(TreeNode *node_ptr, int id);

  TreeNode* rootP;

  int size;

};


#endif
