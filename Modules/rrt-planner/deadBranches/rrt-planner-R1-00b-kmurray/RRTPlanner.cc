/*
 * Desc: Path planner based on RRT algorithm
 * Date: 25 June 2008
 * Author: Karl Murray
 * 
 */

#include <assert.h>
#include <errno.h>
#include <float.h>
#include <stdio.h>
#include <temp-planner-interfaces/ConfigFile.hh>
#include <temp-planner-interfaces/Log.hh>
#include "RRTPlanner.hh"
#include <dgcutils/cfgfile.h>
#include <stdlib.h>
#include <ctime>
#include <cmath>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <cstdlib>
#include <alice/AliceConstants.h>
#define PI 3.14159265

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Common macros
#ifndef MIN
#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif

// Euclidian distance between two points
#define DISTANCE(x1, y1, x2, y2) \
  sqrtf( (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) )

int RRTPlanner::NUM_SAMPLES = 50;
int RRTPlanner::STEP_SIZE = 1;
bool RRTPlanner::USE_BIAS = 0;
bool RRTPlanner::USE_DYN = 0;
float RRTPlanner::DYN_STEP = 1;
float RRTPlanner::DELTA = 0.1;
bool RRTPlanner::CON_STEER = 0;
float RRTPlanner::CON_W = 0.1;
bool RRTPlanner::KEEP_PATH = 0;
float RRTPlanner::BIAS_SIG_THETA = PI/4;
float RRTPlanner::BIAS_SIG_R = 20;
float RRTPlanner::BIAS_R = 10;
float RRTPlanner::BIAS_GOAL = 0;

// Constructor 
RRTPlanner::RRTPlanner(PlanGraph *graph)
{
  Log::getStream(1)<<"Starting RRT-planner."<<endl;
  
  readConfig();

  this->graph = graph;

  pathsize=0;

  treeP = new Tree();

  steer = 0;

  zone = false;

  return;
}

// Destructor 
RRTPlanner::~RRTPlanner()
{
  // TODO Clean up
  return;
}

void RRTPlanner::readConfig()
{
  char *path;
  Log::getStream(1)<<"Reading RRT-planner config file."<<endl;
  path=dgcFindConfigFile("rrtplanner.conf","rrt-planner");
  FILE* configFile = fopen(path,"r");
  if (configFile == NULL) {
    Log::getStream(1)<<"Warning: Couldn't read RRT-Planner configuration file."<<endl;
  }
  else {
    ConfigFile config(path);
    config.readInto(NUM_SAMPLES, "NUM_SAMPLES");
    config.readInto(STEP_SIZE, "STEP_SIZE");
    config.readInto(USE_BIAS, "USE_BIAS");
    config.readInto(USE_DYN, "USE_DYN");
    config.readInto(DYN_STEP, "DYN_STEP");
    config.readInto(DELTA, "DELTA");
    config.readInto(CON_STEER, "CON_STEER");
    config.readInto(CON_W, "CON_W");
    config.readInto(KEEP_PATH, "KEEP_PATH");
    config.readInto(BIAS_SIG_THETA, "BIAS_SIG_THETA");
    config.readInto(BIAS_SIG_R, "BIAS_SIG_R");
    config.readInto(BIAS_R, "BIAS_R");

    Log::getStream(1)<<"Info: Read RRT-Planner configuration successfully."<<endl;
    fclose(configFile);

    // Log all the parameters
    Log::getStream(1) << "NUM_SAMPLES: " << NUM_SAMPLES << endl;
    Log::getStream(1) << "STEP_SIZE: " << STEP_SIZE << endl;
    Log::getStream(1) << "USE_BIAS: " << USE_BIAS << endl;
    Log::getStream(1) << "USE_DYN: " << USE_DYN << endl;
    Log::getStream(1) << "DYN_STEP: " << DYN_STEP << endl;
    Log::getStream(1) << "DELTA: " << DELTA << endl;
    Log::getStream(1) << "CON_STEER: " << CON_STEER << endl;
    Log::getStream(1) << "CON_W: " << CON_W << endl;
    Log::getStream(1) << "KEEP_PATH: " << KEEP_PATH << endl;
    Log::getStream(1) << "BIAS_SIG_THETA: " << BIAS_SIG_THETA << endl;
    Log::getStream(1) << "BIAS_SIG_R: " << BIAS_SIG_R << endl;
    Log::getStream(1) << "BIAS_R: " << BIAS_R << endl;
  }
}

int RRTPlanner::setRegion(int i){
  if(i > 0)
    zone = true;
  else
    zone = false;
  return 0;
}

// Main function that plans the path
int RRTPlanner::planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart,
                          PlanGraphNode *nodeGoal, PlanGraphPath *path, Map *map){

  Log::getStream(1)<<"Start node steering angle: "<< nodeStart->steerAngle <<endl;

  m_map = map;

  startNode = nodeStart;
  goalNode = nodeGoal;

  if(USE_BIAS){
    buildRRT(nodeGoal);
  }
  else{
    buildRRT(nodeGoal);
  }
  genPlan(nodeGoal);
  genPath(path);

  return 0;
}

// Main function that plans the path for the viewer
int RRTPlanner::planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart,
                          PlanGraphNode *nodeGoal, PlanGraphPath *path){

  startNode = nodeStart;

  if(USE_BIAS){
    buildRRT(nodeGoal);
  }
  else{
    buildRRT(nodeGoal);
  }
  genPlan(nodeGoal);
  genPath(path);

  return 0;
}

// This function starts to build the RRT graph
void RRTPlanner::buildRRT(PlanGraphNode *goalP){
  int i,k;
  float a,b;
  TreeNode* n,* pathn;
  sampleSize = 0;

  // Random number generation
  const gsl_rng_type *T;
  gsl_rng *rand;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  rand = gsl_rng_alloc(T);
  gsl_rng_set(rand,time(NULL));

  // Biasing parameters
  float sigR, sigTh, r0, th0, x0, y0, nR, nTh, r, th;
  sigR = BIAS_SIG_R;
  sigTh = BIAS_SIG_THETA;
  r0 = BIAS_R;
  th0 = startNode->pose.rot;
  x0 = startNode->pose.pos.x; 
  y0 = startNode->pose.pos.y;   

  // If we are far from the path, re-plan
  if(DISTANCE(x0,y0,pathList.back()->x,pathList.back()->y) > 10)
    pathList.clear();    


  k = pathList.size();


  if(KEEP_PATH){
    if(k > 0){
      if(USE_DYN){
        if(CON_STEER){
          treeP->makeRoot(startNode->pose.pos.x,startNode->pose.pos.y,startNode->pose.rot, steer);
          for(i=0;i<k;i++){
            pathn = pathList.back();
            pathList.pop_back();        
            n = treeP->closest(pathn->x, pathn->y);
            treeP->extendTree(n, pathn->x, pathn->y, pathn->rot, pathn->steerAngle);
          }
          pathList.clear();
        }
        else{
          treeP->makeRoot(startNode->pose.pos.x,startNode->pose.pos.y,startNode->pose.rot);
          for(i=0;i<k;i++){
            pathn = pathList.back();
            pathList.pop_back();        
            n = treeP->closest(pathn->x,pathn->y);
            treeP->extendTree(n,pathn->x,pathn->y,pathn->rot);
          }
          pathList.clear();
        }
      }
      else{
        treeP->makeRoot(startNode->pose.pos.x,startNode->pose.pos.y);
        for(i=0;i<k;i++){
          pathn = pathList.back();
          pathList.pop_back();        
          n = treeP->closest(pathn->x,pathn->y);
          treeP->extendTree(n,pathn->x,pathn->y);
        }
        pathList.clear();
      }
    }
    else{
      //Creates the root
      if(USE_DYN){
        if(CON_STEER){
          treeP->makeRoot(startNode->pose.pos.x,startNode->pose.pos.y,startNode->pose.rot, 0);
        }
        else{
          treeP->makeRoot(startNode->pose.pos.x,startNode->pose.pos.y,startNode->pose.rot);
        }
      }
      else{
        treeP->makeRoot(startNode->pose.pos.x,startNode->pose.pos.y);
      }
    }
  }
  else{
    pathList.clear();
    if(USE_DYN)
      treeP->makeRoot(startNode->pose.pos.x,startNode->pose.pos.y,startNode->pose.rot);
    else
      treeP->makeRoot(startNode->pose.pos.x,startNode->pose.pos.y);
  }

  // Take random sample and try to extend the tree
  for(i=0; i < NUM_SAMPLES; i++){
    if(USE_BIAS){
      nR = gsl_ran_ugaussian(rand);
      nTh = gsl_ran_ugaussian(rand);

      r = sigR*abs(nR) + r0;
      th = sigTh*nTh + th0;

      a = x0 + r*cos(th);
      b = y0 + r*sin(th);

    }
    else{
      a = gsl_rng_uniform(rand)*100;
      b = gsl_rng_uniform(rand)*100;
      if(gsl_rng_uniform(rand) > 0.5)
        a = -a;
      if(gsl_rng_uniform(rand) > 0.5)
        b = -b;
    }

    addSample(a,b);
    extendRRT(a,b);
  }

  //Build a rrt for the rrt-viewer
  buildPath(NUM_SAMPLES);

}

// Generate the best plan
void RRTPlanner::genPlan(PlanGraphNode *nodeGoal){
  TreeNode* n;

  // Clear the pathlist just to be sure
  pathList.clear();

  //Pick the node closest to the goal and backtrack
  n = treeP->closest((float)nodeGoal->pose.pos.x,(float) nodeGoal->pose.pos.y);
  if(n->id == 0){
    RRTPlanner::BIAS_SIG_THETA = +0.1;
    RRTPlanner::BIAS_SIG_R += 0.1;

  }
      
  n->path=true;
  while(n->id != 0){
    pathList.push_back(n);
    n=n->parentP;
    n->path=true;
  }
  //Remember the last node
  pathList.push_back(n);
}


// Generate the path for the vel-planner
void RRTPlanner::genPath(PlanGraphPath *path){
  int i,k;
  PlanGraphNode* pnodeP,* prevpnodeP;
  TreeNode *tnodeP;
  float alpha;

  // Set up the path
  path->pathLen = 0;
  path->valid = true;
  path->collideObs = 0;
  path->collideCar = 0;
  path->dist = 0;

  k=pathList.size();

  //TEST -removing the root from the path
  steer = pathList.back()->steerAngle;
  pathList.pop_back();

  //Populate the PlanGraphPath 
  for(i = 0; i < k - 1; i++){
    // Get the previous node to calculate the distance
    if(i>0){
      prevpnodeP = pnodeP;

    }
    // Take out and remove the last node in the list
    tnodeP = pathList.back();
    pathList.pop_back();

    pathList.push_front(tnodeP);

    // Create a new node for the path
    pnodeP = this->graph->allocNode(tnodeP->x,tnodeP->y);

    if(!USE_DYN){
      //Calculate the rotation
      if(i>0){
        alpha = atan2((pnodeP->pose.pos.y - prevpnodeP->pose.pos.y),(pnodeP->pose.pos.x - prevpnodeP->pose.pos.x));
//        Log::getStream(1)<<"Setting path angle: " << alpha <<endl;
        prevpnodeP->pose.rot = alpha;
      }
    }
    else{
      pnodeP->pose.rot = tnodeP->rot;
//      Log::getStream(1)<<"Setting path angle: " << tnodeP->rot <<endl;
    }  
  
    // Add the PlanGraphNode to the path
    path->nodes[i]=pnodeP;


    // Calculate the total distance along the path
    if(i>0){
      path->dist += vec2f_mag(vec2f_sub(pnodeP->pose.pos, prevpnodeP->pose.pos));
    }

    // Add the distance at each node
    path->dists[path->pathLen] = path->dist;

    // Only go forward for now
    path->directions[path->pathLen] = PLAN_GRAPH_PATH_FWD;

    // Calculate the total length of the path
    path->pathLen++;
  }
}



//Extend the RRT by finding out what the closest node is
int RRTPlanner::extendRRT(float x, float y){
  float nx,ny;    
  float dist;
  TreeNode* n;

  //Find the closest node in the tree
  n = treeP->closest(x, y);

  if(USE_DYN){
//    if(m_map){
//      if(!onRoad(x,y))
//        return -1;
//      else tryConnect(n,x,y);
//    }
//    else
    //Try to connect the nodes with respect to dynamics
      return tryConnect(n,x,y);
  }
  else{
    //Calculate the step taken in the direction of closest node
    dist=sqrtf( ((x - n->x) * (x - n->x)) + ((y - n->y) * (y - n->y)) );
    nx = (STEP_SIZE*((x - n->x) / dist)) + n->x;
    ny = (STEP_SIZE*((y - n->y) / dist)) + n->y;

    if(m_map)
      if(!onRoad(nx,ny))
        return -1;

    //Extend the tree with the new node
    treeP->extendTree(n,nx,ny);

    return 0;
  }
}

// Try to find a feasible path from node to point
int RRTPlanner::tryConnect(TreeNode *node, float x, float y){
  float L, alpha, phi, t, theta, xG, yG, w, phi0;
  L = VEHICLE_AXLE_DISTANCE;
  xG = node->x;
  yG = node->y;
  alpha = atan2((y - yG),(x - xG));
  theta = node->rot;
  phi = alpha - theta;
  phi0 = node->steerAngle;
//  phi +=phi0;
//  Log::getStream(1)<<"Phi0: " << phi0  <<endl;

//  Log::getStream(1)<<"PhiA: " << phi  <<endl;

  if(CON_STEER){
    if(phi < - PI)
      phi += 2*PI;
    if(phi > PI)
      phi -= 2*PI;
//  Log::getStream(1)<<"PhiB: " << phi  <<endl;
/*
    if(phi < -PI/2)
      phi = -PI/2;
    if(phi > PI/2)
      phi = PI/2;
    w = 0.2*sin(phi);
*/
    if(phi < -PI/4)
      w = -CON_W;
    else if(phi > PI/4)
      w = CON_W;
    else
      w = 0;

    phi = phi0;
  }


  if(CON_STEER){
    for(t = 0; t < DYN_STEP; t += DELTA){
      phi += w*DELTA;
      theta += tan(phi)*DELTA/L;
      xG += cos(theta)*DELTA;
      yG += sin(theta)*DELTA;
    }
  }
  else{
    for(t = 0; t < DYN_STEP; t += DELTA){
      theta += tan(phi)*DELTA/L;
      phi = alpha - theta;
      xG += cos(theta)*DELTA;
      yG += sin(theta)*DELTA;
    }
  }
//  Log::getStream(1)<<"Phi: " << phi  <<endl;
  if(m_map){
//  Log::getStream(1)<<"Calculated theta: " << theta <<endl;
    if(onRoad(xG,yG))
      treeP->extendTree(node, xG, yG, theta, phi);
//  Log::getStream(1)<<"Calculated theta: " << theta <<endl;
    else if(onRoad((3*xG-2*node->x),(3*yG-2*node->y)))
      treeP->extendTree(node, xG, yG, theta, phi);
  }
  else
    treeP->extendTree(node, xG, yG, theta, phi);

  return 0;
}



// Build the tree for the viewer
void RRTPlanner::buildPath(int steps){
  int i;
  TreeNode* nodeP;

  for(i=0; i < steps + 1; i++){
    nodeP = treeP->nodes.front();
    nodes[i] = *nodeP;
    pathsize++;
    treeP->nodes.push_back(nodeP);
    treeP->nodes.pop_front();
  }

}

// Check if the point is on a road
bool RRTPlanner::onRoad(float x, float y){
  if(zone)
    return true;
  int i, closest, side;
  LaneLabel currentLane, otherLane, goalLane;
  point2_uncertain newPoint, calcPoint, calcPointG, safePoint, safePointG;
  point2 curPos, finPos;
  point2arr_uncertain leftCur, rightCur, leftGo, rightGo;
  float dist, min_dist = 1000, alpha;

  curPos.x = startNode->pose.pos.x;
  curPos.y = startNode->pose.pos.y;
  newPoint.x = x;
  newPoint.y = y;
  finPos.x = goalNode->pose.pos.x;
  finPos.y = goalNode->pose.pos.y;

  if(m_map->getLane(currentLane, curPos) < 0)
    return true;
  m_map->getLane(goalLane, finPos);

  Log::getStream(10)<<"Current Lane:"<< currentLane.lane << ", " << currentLane.segment <<endl;
  Log::getStream(10)<<"Goal Lane:"<< goalLane.lane << ", " << goalLane.segment <<endl;

  // Need to know if all of Alice fits on the road
  // 1. in lane
  // 2. lane boundary
  // 3. new point 0.5 meters in perpendicular step towards boundary
  // 4. in lane
  m_map->getLaneBounds(leftCur, rightCur, currentLane);
  m_map->getLaneBounds(leftGo, rightGo, goalLane);

  // Calc safePoint for current lane
  for(i=0; i<leftCur.arr.size(); i++){
    calcPoint = leftCur.arr.at(i);
    dist = DISTANCE(x,y,calcPoint.x,calcPoint.y);
    if(dist<min_dist){
      min_dist = dist;
      closest = i;
      side = -1;
    }
  }
  if(closest == leftCur.arr.size() - 1)
    closest = leftCur.arr.size() - 2;
  for(i=0; i<rightCur.arr.size(); i++){
    calcPoint = rightCur.arr.at(i);
    dist = DISTANCE(x,y,calcPoint.x,calcPoint.y);
    if(dist<min_dist){
      min_dist = dist;
      closest = i;
      side = 1;
    }
  }
  if(closest == rightCur.arr.size() - 1)
    closest = rightCur.arr.size() - 2;
  if(side = 1){
    alpha = atan2((leftCur.arr.at(closest + 1).y - leftCur.arr.at(closest).y), (leftCur.arr.at(closest + 1).x - leftCur.arr.at(closest).x));
    safePoint.x = x + 0.5*VEHICLE_WIDTH*cos(PI/2 + alpha);
    safePoint.y = y + 0.5*VEHICLE_WIDTH*sin(PI/2 + alpha);
  }
  if(side = -1){
    alpha = atan2((rightCur.arr.at(closest + 1).y - rightCur.arr.at(closest).y), (rightCur.arr.at(closest + 1).x - rightCur.arr.at(closest).x));
    safePoint.x = x + 0.5*VEHICLE_WIDTH*cos(PI/2 + alpha);
    safePoint.y = y + 0.5*VEHICLE_WIDTH*sin(PI/2 + alpha);
  }

  // Calc safePoint for goal lane
  for(i=0; i<leftGo.arr.size(); i++){
    calcPointG = leftGo.arr.at(i);
    dist = DISTANCE(x,y,calcPointG.x,calcPointG.y);
    if(dist<min_dist){
      min_dist = dist;
      closest = i;
      side = -1;
    }
  }
  if(closest == leftGo.arr.size() - 1)
    closest = leftGo.arr.size() - 2;
  for(i=0; i<rightGo.arr.size(); i++){
    calcPointG = rightGo.arr.at(i);
    dist = DISTANCE(x,y,calcPointG.x,calcPointG.y);
    if(dist<min_dist){
      min_dist = dist;
      closest = i;
      side = 1;
    }
  }
  if(closest == rightGo.arr.size() - 1)
    closest = rightGo.arr.size() - 2;
  if(side = 1){
    alpha = atan2((leftGo.arr.at(closest + 1).y - leftGo.arr.at(closest).y), (leftGo.arr.at(closest + 1).x - leftGo.arr.at(closest).x));
    safePointG.x = x + 0.5*VEHICLE_WIDTH*cos(PI/2 + alpha);
    safePointG.y = y + 0.5*VEHICLE_WIDTH*sin(PI/2 + alpha);
  }
  if(side = -1){
    alpha = atan2((rightGo.arr.at(closest + 1).y - rightGo.arr.at(closest).y), (rightGo.arr.at(closest + 1).x - rightGo.arr.at(closest).x));
    safePointG.x = x + 0.5*VEHICLE_WIDTH*cos(PI/2 + alpha);
    safePointG.y = y + 0.5*VEHICLE_WIDTH*sin(PI/2 + alpha);
  }



  // Point in the same lane as Alice?
  if(m_map->isPointInLane(safePoint, currentLane)){
    Log::getStream(10)<<"Good point x, y:"<< newPoint.x << ", " << newPoint.y <<endl;
    return true;
  }
  else if(m_map->isPointInLane(safePointG, goalLane)){
    Log::getStream(10)<<"Good point x, y:"<< newPoint.x << ", " << newPoint.y <<endl;
    return true;
  }
  else{
    // Get the closest lane to the point
    m_map->getLane(otherLane, newPoint);
    // DEBUG
    Log::getStream(10)<<"New Lane:"<< otherLane.lane <<endl;
    // Is the point in the closest lane?
    if(m_map->isPointInLane(newPoint, otherLane)){
      // Does the lane has the same direction as the current lane?
      if(m_map->isLaneSameDir(currentLane,otherLane)){
        // DEBUG
        Log::getStream(10)<<"Good point x, y:"<< newPoint.x << ", " << newPoint.y <<endl;
        return true;
      }
      else{
        return false;
      }
    }
    else{
      // DEBUG
      Log::getStream(10)<<"Bad point x, y:"<< newPoint.x << ", " << newPoint.y <<endl;
      return false;
    }
  }
}

void RRTPlanner::addSample(float x, float y){
  PlanGraphNode *node;

  node = this->graph->allocNode(x,y);

  this->sample[sampleSize] = node;

  sampleSize++;
}
