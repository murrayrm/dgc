Mon Sep 10 12:42:19 2007	Tamas Szalay (tamas)

	* version R2-01b
	BUGS:  
	FILES: SENSNET_LF_BUMPER_LADAR.CFG(38015),
		SENSNET_MF_BUMPER_LADAR.CFG(38015)
	Some manual tweaks to the config files to fit ground plane better.

Tue Sep  4  5:19:25 2007	Tamas Szalay (tamas)

	* version R2-01a
	BUGS:  
	FILES: SENSNET_LF_BUMPER_LADAR.CFG(37509),
		SENSNET_LF_ROOF_LADAR.CFG(37509),
		SENSNET_MF_BUMPER_LADAR.CFG(37509),
		SENSNET_PTU_LADAR.CFG(37509),
		SENSNET_REAR_BUMPER_LADAR.CFG(37509),
		SENSNET_RF_BUMPER_LADAR.CFG(37509),
		SENSNET_RF_ROOF_LADAR.CFG(37509)
	Removed stray characters in config files.

Tue Sep  4  4:02:20 2007	Tamas Szalay (tamas)

	* version R2-01
	BUGS:  
	FILES: SENSNET_LF_BUMPER_LADAR.CFG(37477),
		SENSNET_LF_ROOF_LADAR.CFG(37477),
		SENSNET_MF_BUMPER_LADAR.CFG(37477),
		SENSNET_PTU_LADAR.CFG(37477),
		SENSNET_REAR_BUMPER_LADAR.CFG(37477),
		SENSNET_RF_BUMPER_LADAR.CFG(37477),
		SENSNET_RF_ROOF_LADAR.CFG(37477)
	Used ladar-calib utility to align ladars to world (and PTU)

Fri Aug 31  8:53:24 2007	Tamas Szalay (tamas)

	* version R2-00z
	BUGS:  
	FILES: SENSNET_RF_ROOF_LADAR.CFG(36805)
	Modified config file to reflect new position and orientation.

Wed Aug 29 14:52:42 2007	Tamas Szalay (tamas)

	* version R2-00y
	BUGS:  
	FILES: LadarFeeder.cc(35978)
	For PTU ladar, sends PTU pan/tilt in reserved slot of ladar blob.

	FILES: sick_driver.c(34362)
	Some debug messages

	FILES: sick_driver.c(35556)
	Reverting to unmodified version.

	FILES: sick_driver.c(36011), sick_driver.h(36011)
	Hopefully fixed UDP communication for sick drivers. Also cleaned up
	TCP code to make it packet-oriented instead of a hack of serial
	comm.

	FILES: sick_driver.c(36108)
	More modifications (and a few deletions) for UDP functionality

	FILES: sick_driver.c(36167)
	Changed the way timestamps are reported, so that ladar scans appear
	evenly spaced (timed at reception of footer, not header)

Mon Aug  6 22:25:19 2007	Laura Lindzey (lindzey)

	* version R2-00x
	BUGS:  
	FILES: LadarFeeder.cc(32422)
	one-line change, making sensnet_connect adhere to general
	convention of using moduleId, rather than sensorId 

Sun Aug  5 12:18:49 2007	Jeremy Ma (jerma)

	* version R2-00w
	BUGS:  
	FILES: SENSNET_LF_ROOF_LADAR.CFG(32040),
		SENSNET_RF_ROOF_LADAR.CFG(32040)
	modified config files of roof ladars after moving them from
	obstructing view of bumblebees.

Fri Aug  3  1:43:47 2007	Jeremy Ma (jerma)

	* version R2-00v
	BUGS:  
	FILES: sick_driver.c(31687)
	modified sick_driver to turn off Nagle algorithm; according to
	Josh, this should improve the latencies (small) observed in the
	ladars reading off the DM500

Thu Aug  2 15:16:19 2007	Jeremy Ma (jerma)

	* version R2-00u
	BUGS:  
	FILES: SENSNET_LF_BUMPER_LADAR.CFG(31549),
		SENSNET_LF_ROOF_LADAR.CFG(31549),
		SENSNET_MF_BUMPER_LADAR.CFG(31549),
		SENSNET_PTU_LADAR.CFG(31549),
		SENSNET_REAR_BUMPER_LADAR.CFG(31549),
		SENSNET_RF_BUMPER_LADAR.CFG(31549),
		SENSNET_RF_ROOF_LADAR.CFG(31549), riegl_driver.hh(31549)
	modifying the IP address to use with the cPCI system.

Wed Aug  1 12:03:54 2007	Tamas Szalay (tamas)

	* version R2-00t
	BUGS:  
	FILES: LadarFeeder.cc(31342), riegl_driver.cc(31342)
	Changed so that riegl always returns 200 points (in correct order),
	with no-returns marked at distance of 200 m.

	FILES: sick_driver.c(30921)
	Trying to properly extract reflectivity data from SICK ladars.

	FILES: sick_driver.c(30957)
	More testing for driver

	FILES: sick_driver.c(31346)
	Attempted to get SICK ladars to output proper intensity data.
	Failed.

Thu Jul 19 18:29:23 2007	Jeremy Ma (jerma)

	* version R2-00s
	BUGS:  
	FILES: LadarFeeder.cc(29710)
	adding health status messages to the simulator for debugging
	purposes.

Wed Jul 18 23:28:41 2007	Jeremy Ma (jerma)

	* version R2-00r
	BUGS:  
	FILES: LadarFeeder.cc(29567), SENSNET_LF_ROOF_LADAR.CFG(29567),
		SENSNET_RF_ROOF_LADAR.CFG(29567)
	modified config files for new roof-ladar locations and added health
	monitor values to be added to the process message.

Thu Jun 14 16:03:02 2007	Jeremy Ma (jerma)

	* version R2-00q
	BUGS:  
	New files: SENSNET_PTU_LADAR.CFG sick_baudrate_test.c
		sick_baudrate_test.h
	FILES: LadarFeeder.cc(27969), Makefile.yam(27969),
		SENSNET_REAR_BUMPER_LADAR.CFG(27969), sick_driver.c(27969)
	adding files and modifications to complete the YAM merge.

	New files: SENSNET_PTU_LADAR.CFG sick_baudrate_test.c
		sick_baudrate_test.h
	FILES: LadarFeeder.cc(26400)
	made changes to account for the new PTU ladar. This required
	specific conditional case statements since the PTU-ladar's sens2veh
	transformations are continually changing.

	FILES: LadarFeeder.cc(27679), Makefile.yam(27679),
		SENSNET_REAR_BUMPER_LADAR.CFG(27679), sick_driver.c(27679)
	modifications for PTU ladar. 

Sun Jun 10 23:34:33 2007	 (ahoward)

	* version R2-00p
	BUGS:  
	FILES: LadarFeeder.cc(26199), Makefile.yam(26199)
	Modified build options for Darwin compatability

	FILES: LadarFeeder.cc(27776), SimLadar.cc(27776),
		SimLadar.hh(27776), cmdline.c(27776), cmdline.ggo(27776),
		cmdline.h(27776)
	Added fractional placement of obstacles (with correct orientation)

Sat Jun  2 14:25:34 2007	Andrew Howard (ahoward)

	* version R2-00o
	BUGS:  
	FILES: LadarFeeder.cc(26040), sick_driver.c(26040)
	Small tweaks

Thu May 31 11:03:40 2007	 (ahoward)

	* version R2-00n
	BUGS:  
	New files: calib.py
	FILES: LadarFeeder.cc(25503), cmdline.c(25503), cmdline.ggo(25503),
		cmdline.h(25503)
	Added export option

	FILES: LadarFeeder.cc(25519)
	Added support for intensity values

	FILES: SENSNET_LF_BUMPER_LADAR.CFG(25473),
		SENSNET_MF_BUMPER_LADAR.CFG(25473),
		SENSNET_RF_BUMPER_LADAR.CFG(25473)
	Re-calibrated bumper ladar height; needs to be re-calibrated

	FILES: SENSNET_LF_BUMPER_LADAR.CFG(25677),
		SENSNET_LF_ROOF_LADAR.CFG(25677),
		SENSNET_MF_BUMPER_LADAR.CFG(25677),
		SENSNET_RF_BUMPER_LADAR.CFG(25677),
		SENSNET_RF_ROOF_LADAR.CFG(25677), ladar_log_tweak.c(25677)
	Calibrated 5 front ladars; still not entirely happy

	FILES: SENSNET_LF_ROOF_LADAR.CFG(25447),
		SENSNET_RF_ROOF_LADAR.CFG(25447), ladar_log_tweak.c(25447)
	Calibrating; still broken

	FILES: SENSNET_LF_ROOF_LADAR.CFG(25467),
		SENSNET_RF_ROOF_LADAR.CFG(25467), ladar_log_tweak.c(25467)
	Updated calibration on roof ladars; still pretty crappy, so we
	should write an auto-cal program at some point

Thu May 24 15:53:37 2007	Jeremy Ma (jerma)

	* version R2-00m
	BUGS:  
	FILES: SENSNET_LF_BUMPER_LADAR.CFG(24866),
		SENSNET_REAR_BUMPER_LADAR.CFG(24866)
	switched the ports on the DM500 for these two ladars.

Thu May 17 22:23:03 2007	 (ahoward)

	* version R2-00l
	BUGS:  
	New files: SimLadar.cc SimLadar.hh
	FILES: LadarFeeder.cc(23402), Makefile.yam(23402),
		cmdline.c(23402), cmdline.ggo(23402), cmdline.h(23402),
		ladar_log_test.c(23402)
	Added basic ladar simulation

	FILES: LadarFeeder.cc(23619), SENSNET_LF_BUMPER_LADAR.CFG(23619),
		cmdline.c(23619), cmdline.ggo(23619), cmdline.h(23619)
	Added scan limits and fixed simulator z-calculation

	FILES: LadarFeeder.cc(21681)
	Tweaks

	FILES: LadarFeeder.cc(22251)
	Minor tweaks to process control messages

	FILES: ladar_log_test.c(22306)
	Some useful hacks to the log test

Wed May 16 12:06:05 2007	Jeremy Ma (jerma)

	* version R2-00k
	BUGS:  
	New files: SENSNET_REAR_BUMPER_LADAR.CFG
	FILES: LadarFeeder.cc(23173), cmdline.c(23173), cmdline.ggo(23173),
		cmdline.h(23173), sick_driver.c(23173),
		sick_driver.h(23173)
	added the udp connection type option to help debug the "burst" of
	laser scans phenomenon we're seeing with the DeviceMaster500

	FILES: Makefile.yam(23288)
	minor changes to account for the rear ladar now

Fri Apr 27 22:42:56 2007	Andrew Howard (ahoward)

	* version R2-00j
	BUGS:  
	FILES: LadarFeeder.cc(21179), sick_driver.c(21179)
	Fast

Thu Apr 26  0:29:58 2007	 (ahoward)

	* version R2-00i
	BUGS:  
	New files: ladar-log-tweak.sh ladar_log_tweak.c
	FILES: LadarFeeder.cc(20112)
	Improved log timing resolution; added default config path guess

	FILES: LadarFeeder.cc(20114)
	Added ladar log tweaker

	FILES: LadarFeeder.cc(20182), sick_driver.c(20182),
		sick_driver.h(20182)
	Added untested socket timestamp ioctl for sick driver

	FILES: LadarFeeder.cc(20398), sick_driver.c(20398)
	Tested new timestamp (still seeing bursty data).  Also added
	fast-initialization check for already-running ladar.

	FILES: LadarFeeder.cc(20776)
	Tweaks

	FILES: ladar_log_test.c(20190)
	Script for mass log tweaking

Sun Apr 15 23:44:00 2007	Andrew Howard (ahoward)

	* version R2-00h
	BUGS:  
	FILES: LadarFeeder.cc(19748)
	Added process status support

Sat Apr 14 12:34:18 2007	Andrew Howard (ahoward)

	* version R2-00g
	BUGS:  
	New files: ladar_log_dist.sh
	FILES: LadarFeeder.cc(19539), SENSNET_MF_BUMPER_LADAR.CFG(19539),
		cmdline.c(19539), cmdline.ggo(19539), cmdline.h(19539),
		riegl_driver.cc(19539), sick_driver.c(19539)
	Merged changes

	FILES: LadarFeeder.cc(19546)
	Field version; needs to be fixed

	FILES: LadarFeeder.cc(19551), ladar_log_test.c(19551)
	Merged changes with field branch

	New files: ladar_log_dist.sh
	FILES: LadarFeeder.cc(19026), SENSNET_MF_BUMPER_LADAR.CFG(19026)
	Tweaks for API changes

	FILES: riegl_driver.cc(18945)
	Fixed some bugs found by coverity

	FILES: sick_driver.c(19064)
	Added ladar distro script

Fri Apr 13 12:39:59 2007	Jeremy Ma (jerma)

	* version R2-00f
	BUGS: 
	FILES: LadarFeeder.cc(19526), SENSNET_LF_BUMPER_LADAR.CFG(19526),
		SENSNET_LF_ROOF_LADAR.CFG(19526),
		SENSNET_MF_BUMPER_LADAR.CFG(19526),
		SENSNET_RF_BUMPER_LADAR.CFG(19526),
		SENSNET_RF_ROOF_LADAR.CFG(19526), SENSNET_RIEGL.CFG(19526),
		sick_driver.c(19526), sick_driver.h(19526)
	modified ladarfeeder to be compatible with the devicemaster500

Sat Mar 24 17:15:46 2007	 (ahoward)

	* version R2-00e
	BUGS: 
	FILES: sick_driver.c(18913)
	Fixed bugs found by uno

Fri Mar 23 16:36:19 2007	Jeremy Ma (jerma)

	* version R2-00d
	BUGS: 
	FILES: SENSNET_LF_BUMPER_LADAR.CFG(18875),
		SENSNET_RF_BUMPER_LADAR.CFG(18875)
	corrected the USB port numbers 

	FILES: SENSNET_RIEGL.CFG(18876)
	minor modification; replaced port with IPaddress

Wed Mar 21 16:55:58 2007	Jeremy Ma (jerma)

	* version R2-00c
	BUGS: 
	New files: SENSNET_RIEGL.CFG riegl_driver.cc riegl_driver.hh
	FILES: LadarFeeder.cc(18794), Makefile.yam(18794)
	adding riegl driver support to ladarfeeder

	FILES: LadarFeeder.cc(18805)
	more modifications to get the riegl driver integrated 

Mon Mar 19 21:27:34 2007	 (ahoward)

	* version R2-00b
	BUGS: 
	New files: ladar_log_convert.c
	FILES: LadarFeeder.cc(18161), ladar_log_test.c(18161)
	Added log file converter

	FILES: LadarFeeder.cc(18226)
	Added DMA logging support

	FILES: SENSNET_LF_BUMPER_LADAR.CFG(18361),
		SENSNET_LF_ROOF_LADAR.CFG(18361),
		SENSNET_RF_BUMPER_LADAR.CFG(18361),
		SENSNET_RF_ROOF_LADAR.CFG(18361)
	Re-fixed port ids

Mon Mar 12 23:08:09 2007	 (ahoward)

	* version R2-00a
	BUGS: 
	New files: ladar_log_test.c
	FILES: LadarFeeder.cc(17185)
	Shifted to modified ladar blob

	FILES: Makefile.yam(17278)
	Added log test program

Mon Mar  5 16:37:23 2007	Andrew Howard (ahoward)

	* version R1-00l
	BUGS: 
	FILES: LadarFeeder.cc(16620)
	Fixed log message path

Sat Mar  3 15:34:37 2007	 (ahoward)

	* version R1-00k
	BUGS: 
	FILES: SENSNET_MF_BUMPER_LADAR.CFG(16385)
	Tweak ladar config file

Fri Mar  2 21:18:39 2007	 (ahoward)

	* version R1-00j
	BUGS: 
	Deleted files: ladar_log.c ladar_log.h
	FILES: LadarFeeder.cc(14967)
	Fixed segfault when no log directory is present

	FILES: SENSNET_LF_BUMPER_LADAR.CFG(14872),
		SENSNET_MF_BUMPER_LADAR.CFG(14872),
		SENSNET_RF_BUMPER_LADAR.CFG(14872)
	Fixed config files

	FILES: SENSNET_MF_BUMPER_LADAR.CFG(14982)
	Config tweaks

	FILES: sick_driver.c(14983)
	Fixed array overrun

Fri Mar  2 17:22:22 2007	Laura Lindzey (lindzey)

	* version R1-00i
	BUGS: 
Wed Feb 21 22:09:59 2007	Sam Pfister (sam)

	* version R1-00h
	BUGS: 
	FILES: LadarFeeder.cc(15386)
	Updated to work with new skynet release

Mon Feb  5  8:47:53 2007	Andrew Howard (ahoward)

	* version R1-00g
	BUGS: 
	FILES: LadarFeeder.cc(14599)
	Switched to built-in sensnet recording

Sun Feb  4 17:30:50 2007	Andrew Howard (ahoward)

	* version R1-00f
	BUGS: 
	FILES: LadarFeeder.cc(14504)
	Added stderr message logging and switched to sensnet_log

	FILES: LadarFeeder.cc(14524)
	Fixed minor memory leak

Sat Feb  3 22:34:45 2007	Andrew Howard (ahoward)

	* version R1-00e
	BUGS: 
	New files: SENSNET_RF_BUMPER_LADAR.CFG
	FILES: LadarFeeder.cc(14381)
	Field fixes (again)

	FILES: LadarFeeder.cc(14391), Makefile.yam(14391),
		SENSNET_LF_BUMPER_LADAR.CFG(14391),
		SENSNET_LF_ROOF_LADAR.CFG(14391),
		SENSNET_RF_ROOF_LADAR.CFG(14391)
	Field fixes

	FILES: LadarFeeder.cc(14403), Makefile.yam(14403),
		cmdline.c(14403), cmdline.ggo(14403), cmdline.h(14403)
	Added sensnet logging

	FILES: LadarFeeder.cc(14407)
	foo


Fri Feb  2 16:06:01 2007	Andrew Howard (ahoward)

	* version R1-00d
	BUGS: 
	
Fri Feb  2  9:44:15 2007	Andrew Howard (ahoward)

	* version R1-00c
	BUGS: 
	FILES: LadarFeeder.cc(14090)
	Refactored to remove replay support

	FILES: LadarFeeder.cc(14102)
	Display tweaks

Wed Jan 31 13:02:35 2007	Andrew Howard (ahoward)

	* version R1-00b
	BUGS: 
	FILES: LadarFeeder.cc(13677)
	Tweaks

	FILES: LadarFeeder.cc(13731)
	Fixed pause bug

Tue Jan 30 23:26:30 2007	Andrew Howard (ahoward)

	* version R1-00a
	BUGS: 
	New files: LadarFeeder.cc SENSNET_LF_BUMPER_LADAR.CFG
		SENSNET_LF_ROOF_LADAR.CFG SENSNET_MF_BUMPER_LADAR.CFG
		SENSNET_RF_ROOF_LADAR.CFG cmdline.c cmdline.ggo cmdline.h
		ladar_log.c ladar_log.h sick_driver.c sick_driver.h
	FILES: Makefile.yam(13661)
	Added initial file set

	FILES: Makefile.yam(13662)
	Fixed serialization of state

Tue Jan 30 22:09:29 2007	Andrew Howard (ahoward)

	* version R1-00
	Created ladarfeeder module.
















































