/* 
 * Desc: Simulated ladar driver
 * Date: 16 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef SIM_LADAR_H
#define SIM_LADAR_H


#include <frames/pose3.h>


// Forward declarations
namespace std
{
  class RNDF;
}


/// @brief Data for a single ladar measurement.
struct SimLadarMeas
{
  /// Range (m) and bearing (radians) in the sensor frame.
  float range, bearing;
};


/// @brief Simulated object data.
struct SimLadarObject
{
  // Transform from object to global frame
  float globalFromObject[4][4];
  
  // Dimensions
  float size_x, size_y, size_z;
};


/// @brief Simulated ladar driver.
class SimLadar
{
  public:

  /// @brief Default constructor.
  SimLadar();

  /// @brief Destructor
  virtual ~SimLadar();

  public:

  /// @brief Set the sensor-to-vehicle transform.
  int setSensToVeh(float sens2veh[4][4]);

  /// @brief Load RNDF data (for placing obstacles on RNDF waypoints).
  int loadRNDF(const char *filename);

  /// @brief Create a simulated car at the given waypoint
  int addCar(int segmentID, int laneID, int waypointID, double percent);

  public:
  
  /// @brief Generate a ladar scan.
  ///
  /// Read a complete scan, including range and retro-reflector data.
  ///
  /// @param[in] poseGlobal Vehicle pose in global frame.
  /// @param[in]  maxMeas Length of of measurement array.
  /// @param[out] numMeas Number of measurements in scan.
  /// @param[out] meas    Scan measurements.
  /// @returns Returns non-zero on error
  int getScan(pose3_t poseGlobal, int maxMeas, int *numMeas, SimLadarMeas *meas);  

  private:

  // Vehicle to sensor transform
  float sensFromVeh[4][4];
  
  // RNDF data
  std::RNDF *rndf;

  // Global offset (used to bring positions into a usable range)
  vec3_t globalOffset;

  // List of objects
  int numObjects;
  SimLadarObject objects[64];   
};


#endif
