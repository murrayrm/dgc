
# Desc: calibration script
# Author: Andrew Howard
# Date: 11 March 2007

from sys import *
from math import *
from numpy import *
from numpy.linalg import inv
from scipy.optimize.optimize import *


def mat44_set_euler((r, p, h, x, y, z)):
    """Create transform matrix from euler angles and translation"""

    # TODO check sign convention
    R = matrix([[1, 0, 0, 0],
                [0, cos(r), -sin(r), 0],
                [0, sin(r), +cos(r), 0],
                [0, 0, 0, 1]])
    P = matrix([[cos(p), 0, +sin(p), 0],
                [0, 1, 0, 0],
                [-sin(p), 0, +cos(p), 0],
                [0, 0, 0, 1]])
    H = matrix([[cos(h), -sin(h), 0, 0],
                [sin(h), +cos(h), 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1]])
    T = matrix([[1, 0, 0, x],
                [0, 1, 0, y],
                [0, 0, 1, z],
                [0, 0, 0, 1]])

    M = T * H * P * R
    
    return M


def mat41_set_xyz((x, y, z)):
    """Create a 4x1 homogeneous coordinate from XYZ values (right-handed)."""

    return transpose(matrix((x, y, z, 1)));


def mat41_set_neh((n, e, h)):
    """Create a 4x1 homogeneous coordinate from NEH values (left-handed)."""

    return transpose(matrix((e, n, h, 1)));


def mat31_set_xy((x, y)):
    """Create a 3x1 homogeneous coordinate from XY values (right-handed)."""

    return transpose(matrix((x, y, 1)));


def err3d(T, P, Q):
    """Error function for matching 3D point sets."""

    # Construct transform matrix
    M = mat44_set_euler(T)

    # Construct distance matrix
    E = M * P - Q

    # Construct error matrix
    E = multiply(E, E)

    # Compute sum of squared errors
    err = sum(sum(array(E)))
    
    return err


def calc_neh2veh(veh_neh, veh_veh):
    """Compute transform from NEH frame to vehicle frame."""

    # Points in NEH frame (left-handed).
    # Create matrix from the column vectors.
    P = hstack((mat41_set_neh(veh_neh[0]),
                mat41_set_neh(veh_neh[1]),
                mat41_set_neh(veh_neh[2])))

    # Points in vehicle frame.
    # Create matrix from the column vectors.
    Q = hstack((mat41_set_xyz(veh_veh[0]),
                mat41_set_xyz(veh_veh[1]),
                mat41_set_xyz(veh_veh[2])))

    # Initial transform
    T = (0, 0, 0, 0, 0, 0)
    
    # Optimize
    T = fmin_bfgs(err3d, T, None, (P, Q))

    #print 'P = \n', P
    #print 'Q = \n', Q
    #print 'T = ', T

    return mat44_set_euler(T)


def err3d_rot(T, pos, P, Q):
    """Error function for matching 3D point sets (rotation only)."""

    # Construct transform matrix
    M = mat44_set_euler((T[0], T[1], T[2], pos[0], pos[1], pos[2]))

    # Construct distance matrix
    E = M * P - Q

    # Construct error matrix
    E = multiply(E, E)

    # Compute sum of squared errors
    err = sum(sum(array(E)))
    
    return err



def calc_sens2veh(pos_veh, retro_veh, retro_sens):
    """Compute transform from sensor to vehicle frame."""

    pos = pos_veh
    P = retro_sens
    Q = retro_veh
    
    #print 'pos = \n', pos
    #print 'P = \n', P
    #print 'Q = \n', Q

    # HACK
    # Initial transform
    T = (pi, 0, +pi/2)
    
    # Optimize
    T = fmin_bfgs(err3d_rot, T, None, (pos, P, Q))

    #print 'T = ', T

    return T


def calc_ladar(neh2veh, ladar_neh, retro1_neh, retro2_neh, retro1_sens, retro2_sens):
    """Compute the sensor-to-vehicle transform for a ladar"""

    # Compute positions in vehicle frame
    ladar_neh = mat41_set_neh(ladar_neh)
    ladar_veh = neh2veh * ladar_neh
    print "ladar_veh = \n", ladar_veh

    retro_neh = hstack(map(mat41_set_neh, (retro1_neh, retro2_neh)))
    retro_veh = neh2veh * retro_neh
    print "retro_veh = \n", retro_veh

    # Compute retro positions in sensor frame
    retro1_sens = (retro1_scan[1] * cos(retro1_scan[0]),
                   retro1_scan[1] * sin(retro1_scan[0]), 0, 1)
    retro2_sens = (retro2_scan[1] * cos(retro2_scan[0]),
                   retro2_scan[1] * sin(retro2_scan[0]), 0, 1)
    retro_sens = hstack((transpose(matrix(retro1_sens)),
                         transpose(matrix(retro2_sens))))
    print "retro_sens = \n", retro_sens

    # Compute transform from between farmes
    T = calc_sens2veh(ladar_veh, retro_veh, retro_sens)

    #print mat44_set_euler((trans_vs[0], trans_vs[1], trans_vs[2],
    #                       ladar_veh[0], ladar_veh[1], ladar_veh[2]))    
    #print "trans_vs = \n", trans_vs

    return ((ladar_veh[0], ladar_veh[1], ladar_veh[2]),
            (T[0], T[1], T[2]))


if True:

    # Calibration 2007-05-30

    # Vehicle left side

    # Tire radius
    tire = 0.3937
    
    # Vehicle points in vehicle frame.  This comes from the Wiki, where
    # the coordinates are specified with respect to the ground.
    veh_veh = (
        (4.09, -0.89, -1.30 + tire), # Lower left 
        (3.00, -0.72, -2.19 + tire), # Upper left
        (3.00, +0.72, -2.19 + tire), # Upper right
        #(4.09, +0.89, -1.30 + tire), # Lower right
        )

    # Vehicle points in NEH frame (left-handed)
    veh_neh = (
        (8.334,4.899,0.176),
        (8.735,5.951,1.072),
        (10.094,5.655,1.091),
        )

    # Compute transform from NEH to vehicle frame
    neh2veh = calc_neh2veh(veh_neh, veh_veh)
    print "neh2veh = \n", neh2veh

    # Left roof ladar position, in NEH frame.
    pa = (8.634,6.028,1.264)
    pb = (8.742,5.857,1.255)
    ladar_neh = ((pa[0] + pb[0])/2,
                 (pa[1] + pb[1])/2,
                 (pa[2] + pb[2])/2)

    # Retro reflector positions, in NEH frame.
    retro1_neh = (7.817,-2.884,-0.508)
    retro2_neh = (0.608,4.690,-0.850) 

    # Retro reflector positions in scan
    retro1_scan = (-0.890118, 9.040000)
    retro2_scan = (0.383972, 8.440000)

    (pos, rot) = calc_ladar(neh2veh, ladar_neh, retro1_neh, retro2_neh, retro1_scan, retro2_scan)

    print 'LF_ROOF'
    print 'sens-pos = "%f, %f, %f"' % (pos[0], pos[1], pos[2])
    print 'sens-rot = "%f, %f, %f"' % (rot[0], rot[1], rot[2])

    # Left bumper ladar position, in NEH frame.
    pa = (8.124,4.804,-0.396)
    pb = (8.085,4.605,-0.399)
    ladar_neh = ((pa[0] + pb[0])/2,
                 (pa[1] + pb[1])/2,
                 (pa[2] + pb[2])/2)

    # Retro reflector positions, in NEH frame.
    retro1_neh = (-5.519,5.072,-0.492)
    retro2_neh = (3.314,-2.508,-0.501)

    # Retro reflector positions in scan
    retro1_scan = (-0.174533, 13.600000)
    retro2_scan = (-1.186824, 8.640000)

    (pos, rot) = calc_ladar(neh2veh, ladar_neh, retro1_neh, retro2_neh, retro1_scan, retro2_scan)

    print 'LF_BUMPER'
    print 'sens-pos = "%f, %f, %f"' % (pos[0], pos[1], pos[2])
    print 'sens-rot = "%f, %f, %f"' % (rot[0], rot[1], rot[2])

    # Middle bumper ladar position, in NEH frame.
    pa = (9.004,4.279,-0.438)
    pb = (9.215,4.230,-0.431)
    ladar_neh = ((pa[0] + pb[0])/2,
                 (pa[1] + pb[1])/2,
                 (pa[2] + pb[2])/2)

    # Retro reflector positions, in NEH frame.
    retro1_neh = (3.640,-2.444,-0.462)
    retro2_neh = (9.554,-3.438,-0.263)

    # Retro reflector positions in scan
    retro1_scan = (0.453786, 8.650000)
    retro2_scan = (-0.296706, 7.710000)

    (pos, rot) = calc_ladar(neh2veh, ladar_neh, retro1_neh, retro2_neh, retro1_scan, retro2_scan)

    print 'MF_BUMPER'
    print 'sens-pos = "%f, %f, %f"' % (pos[0], pos[1], pos[2])
    print 'sens-rot = "%f, %f, %f"' % (rot[0], rot[1], rot[2])



if True:

    # Calibration 2007-05-30

    # Vehicle right side

    # Tire radius
    tire = 0.3937
    
    # Vehicle points in vehicle frame.  This comes from the Wiki, where
    # the coordinates are specified with respect to the ground.
    veh_veh = (
        (4.09, +0.89, -1.30 + tire), # Lower right
        (3.00, +0.72, -2.19 + tire), # Upper right
        (3.00, -0.72, -2.19 + tire), # Upper left
        #(4.09, -0.89, -1.30 + tire), # Lower left         
        )

    # Vehicle points in NEH frame (left-handed)
    veh_neh = (
        (9.759,1.286,0.383),
        (10.720,0.661,1.278),
        (11.661,1.683,1.258),
        )

    # Compute transform from NEH to vehicle frame
    neh2veh = calc_neh2veh(veh_neh, veh_veh)
    print "neh2veh = \n", neh2veh

    # Ladar position, in NEH frame.
    pa = (10.662,0.550,1.467)
    pb = (10.651,0.752,1.456)
    ladar_neh = ((pa[0] + pb[0])/2,
                 (pa[1] + pb[1])/2,
                 (pa[2] + pb[2])/2)

    # Retro reflector positions, in NEH frame.
    retro1_neh = (3.033,-2.024,0.287)
    retro2_neh = (3.128,5.587,-0.061) 

    # Retro reflector positions in scan
    retro1_scan = (-0.279253, 8.180000)
    retro2_scan = (0.628319, 9.130000)

    (pos, rot) = calc_ladar(neh2veh, ladar_neh, retro1_neh, retro2_neh, retro1_scan, retro2_scan)

    print 'RF_ROOF'
    print 'sens-pos = "%f, %f, %f"' % (pos[0], pos[1], pos[2])
    print 'sens-rot = "%f, %f, %f"' % (rot[0], rot[1], rot[2])


    # Right bumper ladar position, in NEH frame.
    pa = (9.521,1.218,-0.166)
    pb = (9.373,1.356,-0.169)
    ladar_neh = ((pa[0] + pb[0])/2,
                 (pa[1] + pb[1])/2,
                 (pa[2] + pb[2])/2)

    # Retro reflector positions, in NEH frame.
    retro1_neh = (3.078,5.561,-0.154)
    retro2_neh = (-0.325,-3.232,-0.139)

    # Retro reflector positions in scan
    retro1_scan = (1.413717, 7.660000)
    retro2_scan = (0.383972, 10.740000)

    (pos, rot) = calc_ladar(neh2veh, ladar_neh, retro1_neh, retro2_neh, retro1_scan, retro2_scan)

    print 'RF_BUMPER'
    print 'sens-pos = "%f, %f, %f"' % (pos[0], pos[1], pos[2])
    print 'sens-rot = "%f, %f, %f"' % (rot[0], rot[1], rot[2])






