/* 
 * Desc: Simulated ladar driver
 * Date: 16 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>

#include <frames/quat.h>
#include <frames/mat44.h>
#include <rndf/RNDF.hh>

#include "SimLadar.hh"


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor.
SimLadar::SimLadar()
{
  this->numObjects = 0;
  
  return;
}


// Destructor
SimLadar::~SimLadar()
{
  if (this->rndf)
    delete this->rndf;
  
  return;
}


// Set the sensor-to-vehicle transform.
int SimLadar::setSensToVeh(float sens2veh[4][4])
{  
  mat44f_inv(this->sensFromVeh, sens2veh);
  
  return 0;
}


// Load RNDF data (for placing obstacles on RNDF waypoints).
int SimLadar::loadRNDF(const char *filename)
{
  // Create RNDF object
  this->rndf = new std::RNDF();
  assert(this->rndf);

  // Load RNDF from file
  if (!this->rndf->loadFile(filename))
    return ERROR("unable to load %s", filename);

  std::Waypoint *wp;
  
  // Get the first waypoint to use as an offset
  wp = this->rndf->getWaypoint(1, 1, 1);
  assert(wp);
  this->globalOffset.x = wp->getNorthing();
  this->globalOffset.y = wp->getEasting();
  this->globalOffset.z = 0;
  
  return 0;
}


// Create a simulated car at the given waypoint
int SimLadar::addCar(int segmentID, int laneID, int waypointID, double fraction)
{
  std::Waypoint *wpA, *wpB;
  SimLadarObject *object;
  double dx, dy;
  pose3_t pose;

  // Look up the waypoint in the RNDF
  wpA = this->rndf->getWaypoint(segmentID, laneID, waypointID);
  if (!wpA)
    return ERROR("waypoint %d.%d.%d not found", segmentID, laneID, waypointID);

  // Look up the next waypoint in the RNDF
  wpB = this->rndf->getWaypoint(segmentID, laneID, waypointID + 1);
  if (!wpB)
    return ERROR("waypoint %d.%d.%d not found", segmentID, laneID, waypointID);

  dx = wpB->getNorthing() - wpA->getNorthing();
  dy = wpB->getEasting() - wpA->getEasting();

  // Create new car object
  if ((size_t) this->numObjects >= sizeof(this->objects)/sizeof(this->objects[0]))
    return ERROR("too many objects");
  object = this->objects + this->numObjects++;

  pose.pos.x = (wpA->getNorthing() - this->globalOffset.x) + dx * fraction;
  pose.pos.y = (wpA->getEasting() - this->globalOffset.y) + dy * fraction;
  pose.pos.z = 0 - this->globalOffset.z;
  pose.rot = quat_from_rpy(0, 0, atan2(dy, dx));
  pose3_to_mat44f(pose, object->globalFromObject);

  object->size_x = 4.0;
  object->size_y = 2.0;
  object->size_z = 1.0;
  
  return 0;
}

  
// Generate a ladar scan.
int SimLadar::getScan(pose3_t poseGlobal, int maxMeas, int *numMeas, SimLadarMeas *meas)
{
  int i, j;
  int bin;
  float px, py, pz;
  float ax, ay, az, bx, by, bz;
  float a, qx, qy, qr, qb;
  float vehFromGlobal[4][4];
  float sensFromGlobal[4][4];
  float m[4][4];
  SimLadarObject *object;

  // Offset the global pose to produce a more tractable value
  poseGlobal.pos.x -= this->globalOffset.x;
  poseGlobal.pos.y -= this->globalOffset.y;
  poseGlobal.pos.z -= this->globalOffset.z;
  
  // Transform from global to vehicle frame
  pose3_to_mat44f(pose3_inv(poseGlobal), vehFromGlobal);

  // Transform from global to sensor frame
  mat44f_mul(sensFromGlobal, this->sensFromVeh, vehFromGlobal);
  
  // Initialize scan measurements
  for (i = 0; i < maxMeas; i++)
  {
    meas[i].bearing = (i - maxMeas/2) * M_PI/180;
    meas[i].range = 81.92;  // MAGIC
  }
  *numMeas = maxMeas;

  for (i = 0; i < this->numObjects; i++)
  {
    object = this->objects + i;

    // Transform from object to sensor frame
    mat44f_mul(m, sensFromGlobal, object->globalFromObject);
             
    for (j = 0; j < 100; j++) // MAGIC
    {
      // Select the lower point on the object
      px = (float) rand() / RAND_MAX * object->size_x - object->size_x / 2;
      py = (float) rand() / RAND_MAX * object->size_y - object->size_y / 2;
      pz = poseGlobal.pos.z;
      
      // Transform to sensor frame
      ax = m[0][0] * px + m[0][1] * py + m[0][2] * pz + m[0][3];
      ay = m[1][0] * px + m[1][1] * py + m[1][2] * pz + m[1][3];
      az = m[2][0] * px + m[2][1] * py + m[2][2] * pz + m[2][3];

      // Select the upper point on the object
      px = (float) rand() / RAND_MAX * object->size_x - object->size_x / 2;
      py = (float) rand() / RAND_MAX * object->size_y - object->size_y / 2;
      pz = poseGlobal.pos.z - object->size_z;
      
      // Transform to sensor frame
      bx = m[0][0] * px + m[0][1] * py + m[0][2] * pz + m[0][3];
      by = m[1][0] * px + m[1][1] * py + m[1][2] * pz + m[1][3];
      bz = m[2][0] * px + m[2][1] * py + m[2][2] * pz + m[2][3];

      // The two points must be on either side of the scan if we expect
      // to get a return
      if (az * bz > 0)
        continue;

      // Compute the linear interpolation at z = 0 in the sensor frame.
      a = (0 - az) / (bz - az);
      qx = (1 - a) * ax + a * bx;
      qy = (1 - a) * ay + a * by;
      
      qr = sqrt(qx * qx + qy * qy);
      qb = atan2(qy, qx);

      // Assume standard SICK configuration
      bin = (int) (qb * 180/M_PI + maxMeas/2 + 0.5);
      if (bin < 0)
        continue;
      if (bin > maxMeas)
        continue;

      if (qr < meas[bin].range)
        meas[bin].range = qr;
    }    
  }
  
  return 0;
}
