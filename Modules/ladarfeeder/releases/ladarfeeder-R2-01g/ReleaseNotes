              Release Notes for "ladarfeeder" module

Release R2-01g (Thu Oct  4  4:42:56 2007):
	new config files for the upper bumper ladars.

Release R2-01f (Sun Sep 30 14:17:31 2007):
	Attaches ptu state to the blob when ptu-ladar is being used.

Release R2-01e (Sat Sep 29 10:40:13 2007):
	Changed send to write command for compatibility wirth serial 
mode usage.

Release R2-01d (Sun Sep 16 21:17:57 2007):
	Config file updates.

Release R2-01c (Sat Sep 15  6:59:33 2007):
slight modification to SENSNET_LF_ROOF_LADAR.CFG file 
to account for new position; this is a temporary fix 
until Tamas uses the PTU calibration to get the value 
right. It seems accurate to within 2 or 3 cm.


Release R2-01b (Mon Sep 10 12:42:23 2007):
	Tweaked MF_ and LF_BUMPER_LADAR to fix roll and pitch (previous 
data was insufficient to account for these).

Release R2-01a (Tue Sep  4  5:19:34 2007):
	Removed stray characters in config files accidentally inserted by ladar-calib. That's what I get for 4AM releases.

Release R2-01 (Tue Sep  4  4:02:33 2007):
	Used ladar-calib to align all ladars to world using minimization algorithm + PTU ladar. Appears to have worked for all but rear bumper, which I aligned manually. Should fix the 
misalignment issues we have been seeing (if it worked).

Release R2-00z (Fri Aug 31  8:53:27 2007):
	Updated right roof ladar cfg.

Release R2-00y (Wed Aug 29 14:52:48 2007):
	Added support for UDP connection, which is not going to be 
used because the DM500 can't split packets correctly (as I later found 
out). Also changed timestamps to reflect the footer time of the 
packet instead of the header time of the packet, due to the way the 
DM500 is sending us data.
In addition, I just recently fixed the serial timeout on the 
two ladar DM500s so that it splits packets correctly. As a result 
there is no longer any packet clumping, though when all four ladars are 
running on a single unit the packet spacing is 14 +- 5 ms (which seems 
acceptable).

Release R2-00x (Mon Aug  6 22:25:23 2007):
	one-line change, making sensnet_connect adhere to general
	convention of using moduleId, rather than sensorId.
	
	Tested running this on Alice this evening in the shop.

Release R2-00w (Sun Aug  5 12:18:53 2007):
Changed the configuration files for the roof ladars after having moved 
the roof ladars out from obstructing the view of the bumblebees. They 
should line up with the other ladar scans now. 

Release R2-00v (Fri Aug  3  1:43:50 2007):
Modified the sick_driver.c file to not use the Nagle algorithm; 
according to Josh, this will help with reducing delays incurred by 
reading packets off the SICKs through the DM500.

Release R2-00u (Thu Aug  2 15:16:33 2007):
Changing the IP addresses in the cfg files to allow ladars to run on the 
cPCI system. With the new configuration, the ladars can only run on 
blade16 and the Riegl on blade15

Release R2-00t (Wed Aug  1 12:04:03 2007):
The Riegl will now send all 200 points (including no-returns), with no-returns
set to a distance of 200 m. In addition, it now properly sends intensity data
(whether it will be useful is a different question); I tried to get the SICKs
to do the same but they refuse to.

Release R2-00s (Thu Jul 19 18:29:27 2007):
adding health status messages to the simulator option in ladarfeeder. 
This is to help with debugging purposes. 

I also included my add'tl release notes for my last entry. 

Release R2-00r (Wed Jul 18 23:28:46 2007):
Two main changes:

1) Modified the config files for the roof ladars to reflect their 
current location on alice (i.e. above the bumper ladars). Checked the 
output through sensviewer and all seems kosher. I did not change the 
name of the roof ladars though so by an abuse of notation, the 
SENSNET_RF_ROOF_LADAR and SENSNET_LF_ROOF_LADAR refer to the 
right-front-upper-bumper-ladar and left-front-upper-bumper-ladar, 
respectively. One could use their imagination and say the roof ladars are 
technically "roof" ladars because they sit on top of the roof of the bumper 
ladars :)

2) The second major change is I added a health status message to be reported
along with the processState message. This is to be in accordance with the new
health monitor module. Current healthStatus values are:

2: good
1: temporarily bad
0: permanently bad

This change required a change to the processState.h file in interfaces. I'm
currently in the process of making new build releases of all affected modules. 

Release R2-00q (Thu Jun 14 15:35:07 2007):
  This version of ladarfeeder has modifications that take into account 
the special case of the PTU mounted ladar. Special care has to be taken 
for that ladar specifically because the sensor to vehicle 
transformations are continually changing. It should be the case that if 
the PTU is off, then you cannot subscribe to this ladar (i.e. scans are 
ignored until ptufeeder state is obtained). 

Release R2-00p (Sun Jun 10 23:35:16 2007):
  Added fractional placement of simulated obstacles and corrected obstacle
  orientation.

Release R2-00o (Sat Jun  2 14:25:40 2007):
  Very minor tweaks.

Release R2-00n (Thu May 31 11:05:21 2007):
  This release contains the updated calibration for the five front ladars.
  I'm still not happy with the results, however, as there is a noticable
  discrepancy between temporally fused point clouds.  Possibly a problem with
  ladar calibration method, code, or measurements, or possibly a problem
  with the Applanix/astate calibration.

Release R2-00m (Thu May 24 15:53:46 2007):
  This is a relatively major change I suppose. I had to swap the ports 
between the rear-ladar and the left-bumper ladar on the DM500 since the cable 
lengths were becoming an issue. The config files in this release are 
changed appropriately to reflect this change. For those of you that go 
out into the field and don't have this release of ladarfeeder, you'll be 
interpreting rear-ladar data for your bumper-ladar which would be bad. 

Release R2-00l (Wed May 16 18:00:57 2007):
  Minor fixes for process control message handling.
  Added a crude but effective ladar simulation capability using RNDF data;
  should probably be replaced with a decent version that uses TrafSim data.

Release R2-00k (Wed May 16 12:06:26 2007):
  Added config file for rear ladar. Calibration was done VERY crudely 
(i.e. eye-balling and a measuring tape).

Release R2-00j (Fri Apr 27 22:43:01 2007):
  Added timeout to sick driver, which now supports fast initialization (skips
  the configuration step if the ladar is already running).

Release R2-00i (Thu Apr 26  0:30:30 2007):
  Now searches default config path.

Release R2-00h (Sun Apr 15 23:44:04 2007):
  Added support for process status messages.

Release R2-00g (Fri Apr 13 12:57:39 2007):
  Includes support for SICK lasers operating over Ethernet (via DeviceMaster),
  and a fairly extensive re-factoring (simplification) of the driver.

Release R2-00f (Fri Apr 13 12:40:13 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R2-00e (Sat Mar 24 17:15:59 2007):
  Fixed unitialized data bugs found by uno; this could explain why the laser
  connection sequence sometimes fails.

Release R2-00d (Fri Mar 23 16:36:24 2007):
I changed the configuration files for the bumper level ladars to match the correct usb ports. The two roof ladar configuration 
files probably need to be corrected as well but I didn't change those; the roof ladars are temporarily being used to test with 
the DeviceMaster500 Serial-to-Ethernet gateway. With this release, you should be able to connect to any of the bumper ladars 
without any problem. 

Release R2-00c (Wed Mar 21 16:56:02 2007):
  Modified ladarfeeder to be compatible with the Riegl scanner. Tested on Alice and seems to work fine (i.e. no seg-faults). I have 
yet to test logging and replay but since it's using the LadarRangeBlob struct type through sensnet, there shouldn't be any problems. 
I should note that the simulation capabilities of the Riegl have not been modified so it simulates just like it would a regular SICK 
scanner. To test the riegl, simply type: ./ladarfeeder --sensor SENSNET_RIEGL

Release R2-00b (Mon Mar 19 21:28:19 2007):
  Updates to configuration files to match the current ladar cabling.
  Minor revision to support high-speed logging.

Release R2-00a (Mon Mar 12 23:08:30 2007):
  This release includes a relatively minor revision of the LadarRangeBlob;
  log files will be incompatible with those collected prior to 2007-04-11.

Release R1-00l (Mon Mar  5 16:37:26 2007):
  Minor release to fix message log path.

Release R1-00k (Sat Mar  3 15:34:55 2007):
  Minor release with modified ladar configuration file.

Release R1-00h (Wed Feb 21 22:10:03 2007):
	Updated to work with new skynet release.

Release R1-00g (Mon Feb  5  8:47:56 2007):
	Changed to sensnet built-in logging.
	
Release R1-00f (Sun Feb  4 17:30:53 2007):
	Changed to sensnet/sensnet_log API.

Release R1-00e (Sat Feb  3 22:34:57 2007):
	Working, field tested version.

Release R1-00c (Fri Feb  2  9:44:18 2007):
	Little bit'o refactoring.

Release R1-00b (Wed Jan 31 13:02:39 2007):
	Display tweaks.

Release R1-00a (Tue Jan 30 23:26:33 2007):
	Initial release.

Release R1-00 (Tue Jan 30 22:09:29 2007):
	Created.



















































