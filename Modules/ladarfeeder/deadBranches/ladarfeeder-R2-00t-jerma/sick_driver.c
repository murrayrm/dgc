
/*
 Desc: Driver for the SICK laser
 Author: Andrew Howard, adapted from the Player SickLMS200 driver.
 Date: 9 Oct 2006
 CVS: $Id: sicklms200.cc,v 1.60 2006/10/05 20:05:03 gerkey Exp $
*/

#include <assert.h>
#include <math.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <termios.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <linux/serial.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#include "sick_driver.h"



// Open the serial terminal
static int open_term_serial(sick_driver_t *self, const char *device_name);

// Open the tcp socket
static int open_term_tcp(sick_driver_t *self, const char *host, int port);

// Open the tcp socket
static int open_term_udp(sick_driver_t *self, const char *host, int port);

// Close the terminal
static int close_term(sick_driver_t *self);

// Set the terminal speed
static int set_term_speed(sick_driver_t *self, int speed);

// Put the laser into configuration mode
static int set_laser_mode(sick_driver_t *self);

// Set the laser data rate
static int set_laser_speed(sick_driver_t *self, int speed);

// Get the laser type
static int get_laser_type(sick_driver_t *self, char *buffer, size_t bufflen);

// Set the laser configuration
static int set_laser_config(sick_driver_t *self, int range_res, bool retro);

// Change the angular resolution of the laser
static int set_laser_res(sick_driver_t *self, int width, int res);

// Request range scan from the laser
static int request_laser_scan(sick_driver_t *self);

// Request range scan from the laser
static int request_laser_scan_full(sick_driver_t *self);

// Read range scan from laser
static int read_laser_scan(sick_driver_t *self, uint64_t *timestamp,
                           int max_rays, int *num_rays, uint16_t *rays, int timeout);

// Write a packet to the laser
static ssize_t write_to_laser(sick_driver_t *self, uint8_t *data, ssize_t len);

// Read a packet from the laser
static ssize_t read_from_laser(sick_driver_t *self, uint64_t *timestamp,
                               uint8_t *data, ssize_t maxlen, bool ack, int timeout);


// Operating modes
#define MODE_SOCKET 1
#define MODE_SERIAL 2

// Laser device codes
#define STX     0x02
#define ACK     0xA0
#define NACK    0x92
#define CRC16_GEN_POL 0x8005
#define DEFAULT_LASER_RETRIES 6


// Error handling 
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg  : %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)


// Allocate driver object.
sick_driver_t *sick_driver_alloc()
{
  sick_driver_t *self;

  self = calloc(1, sizeof(sick_driver_t));

  return self;
}


// Free driver object.
void sick_driver_free(sick_driver_t *self)
{
  free(self);
  return;
}


// Open comms and initialize laser.
int sick_driver_open_serial(sick_driver_t *self, const char *port, int rate,
                            int scan_res, int range_res)
{
  char type[64];

  self->mode = MODE_SERIAL;
    
  // Open the terminal
  if (open_term_serial(self, port) != 0)
    return -1;
  
  // Set the desired rate
  if (set_term_speed(self, rate) != 0)
  {
    close_term(self);
    return -1;
  }

  // Try connecting at the given rate
  MSG("connecting at %d", rate);
  if (set_laser_mode(self) != 0)
  {
    close_term(self);
    return -1;
  }
  
  // Display the laser type
  memset(type, 0, sizeof(type));
  if (get_laser_type(self, type, sizeof(type)))
    return -1;
  MSG("SICK laser type %s at %s %d", type, port, rate);

  // Configure the laser
  if (set_laser_res(self, 180, scan_res))
    return -1;
  if (set_laser_config(self, range_res, true))
    return -1;

  self->scan_res = scan_res;
  self->range_res = range_res;
  
  // Request data
  if (request_laser_scan(self) != 0)
    return -1;

  MSG("laser is running");

  return 0;
}


// Open comms and initialize laser.
int sick_driver_open_tcp(sick_driver_t *self, const char *host, int port,
                        int scan_res, int range_res)
{
  char type[64];

  self->mode = MODE_SOCKET;
  self->scan_res = scan_res;
  self->range_res = range_res;
    
  // Open the terminal
  if (open_term_tcp(self, host, port) != 0)
    return -1;

  // TESTING
  // See if laser is already up and running
  if (sick_driver_read(self, NULL, 0, NULL, NULL, NULL) == 0)
  {
    MSG("laser is already running");
    return 0;
  }

  MSG("configuring laser");
  
  // Try connecting
  if (set_laser_mode(self) != 0)
  {
    close_term(self);
    return -1;
  }

  // Display the laser type
  memset(type, 0, sizeof(type));
  if (get_laser_type(self, type, sizeof(type)))
    return -1;
  MSG("SICK laser type %s at %s:%d", type, host, port);

  // Configure the laser
  if (set_laser_res(self, 180, scan_res))
    return -1;
  if (set_laser_config(self, range_res, true))
    return -1;

  // Request data
  if (request_laser_scan(self) != 0)
    return -1;

  MSG("laser is running");

  return 0;
}



// Open comms and initialize laser.
int sick_driver_open_udp(sick_driver_t *self, const char *host, int port,
                        int scan_res, int range_res)
{
  char type[64];

  self->mode = MODE_SOCKET;
  self->scan_res = scan_res;
  self->range_res = range_res;
    
  // Open the terminal
  if (open_term_udp(self, host, port) != 0)
    return -1;

  // TESTING
  // See if laser is already up and running
  if (sick_driver_read(self, NULL, 0, NULL, NULL, NULL) == 0)
  {
    MSG("laser is already running");
    return 0;
  }

  MSG("configuring laser");
  
  // Try connecting
  if (set_laser_mode(self) != 0)
  {
    close_term(self);
    return -1;
  }

  // Display the laser type
  memset(type, 0, sizeof(type));
  if (get_laser_type(self, type, sizeof(type)))
    return -1;
  MSG("SICK laser type %s at %s:%d", type, host, port);

  // Configure the laser
  if (set_laser_res(self, 180, scan_res))
    return -1;
  if (set_laser_config(self, range_res, true))
    return -1;

  // Request data
  if (request_laser_scan(self) != 0)
    return -1;

  MSG("laser is running");

  return 0;
}


// Close everything down.
int sick_driver_close(sick_driver_t *self)
{
  // TODO: stop the scan data?
  
  // Close the connection
  close_term(self);

  MSG("laser is stopped");
  
  return 0;
}


// Read scan data
int sick_driver_read(sick_driver_t *self, uint64_t *timestamp,
                     int max_ranges, int *num_ranges, float *ranges, int *retros)
{
  int i;
  int num_rays;
  uint16_t rays[361];
  
  // Read a scan
  if (read_laser_scan(self, timestamp, sizeof(rays)/sizeof(rays[0]), &num_rays, rays, 1000) != 0)
    return -1;

  if (num_ranges)
    *num_ranges = num_rays;

  // Convert into ranges
  for (i = 0; i < num_rays && i < max_ranges; i++)
  {
    ranges[i] = (float) (rays[i] & 0x1FFF) * self->range_res / 1e3;
    if (retros)
      retros[i] = ((rays[i] >> 13) & 0x0007);
  }

  return 0;
}


// Open a serial connection
int open_term_serial(sick_driver_t *self, const char *port)
{
  struct termios term;

  self->fd = open(port, O_RDWR | O_SYNC , S_IRUSR | S_IWUSR );
  if (self->fd < 0)
    return ERROR("unable to open serial port %s: %s", port, strerror(errno));

  // set the serial port speed to 9600 to match the laser
  // later we can ramp the speed up 
  if ( tcgetattr( self->fd, &term ) < 0 )
    return ERROR("unable to get serial port attributes: %s", strerror(errno));
  
  cfmakeraw( &term );
  cfsetispeed( &term, B9600 );
  cfsetospeed( &term, B9600 );
  
  if (tcsetattr( self->fd, TCSAFLUSH, &term ) < 0)
    return ERROR("unable to set serial port attributes: %s", strerror(errno));

  // Make sure queue is empty
  tcflush(self->fd, TCIOFLUSH);
    
  return 0;
}


// Open a tcp connection
int open_term_tcp(sick_driver_t *self, const char *host, int port)
{  
  struct sockaddr_in serverAddr;
  struct hostent *serverHostInfo;

  serverHostInfo = gethostbyname(host);
  if (serverHostInfo == NULL) 
    return ERROR("unable to parse host %s", host);
  
  // Create address
  memset(&serverAddr,0,sizeof(serverAddr));
  serverAddr.sin_family = serverHostInfo->h_addrtype;
  memcpy((char *) &serverAddr.sin_addr.s_addr,
         serverHostInfo->h_addr_list[0], serverHostInfo->h_length);
  serverAddr.sin_port = htons(port);

  // Create socket; 
  self->fd = socket(AF_INET, SOCK_STREAM, 0);
  if (self->fd < 0) 
    return ERROR("socket failed: %s", strerror(errno));
  
  // Connect to server
  if (connect(self->fd, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
    return ERROR("unable to connect to server: %s", strerror(errno));
  
  return 0;  
}


// Open a tcp connection
int open_term_udp(sick_driver_t *self, const char *host, int port)
{  
  struct sockaddr_in serverAddr;
  struct hostent *serverHostInfo;

  serverHostInfo = gethostbyname(host);
  if (serverHostInfo == NULL) 
    return ERROR("unable to parse host %s", host);
  
  // Create address
  memset(&serverAddr,0,sizeof(serverAddr));
  serverAddr.sin_family = serverHostInfo->h_addrtype;
  memcpy((char *) &serverAddr.sin_addr.s_addr,
         serverHostInfo->h_addr_list[0], serverHostInfo->h_length);
  serverAddr.sin_port = htons(port);

  // Create socket; SOCK_DGRAM for UDP connection
  self->fd = socket(AF_INET, SOCK_DGRAM, 0);
  if (self->fd < 0) 
    return ERROR("socket failed: %s", strerror(errno));
  
  // No need to connect for UDP connection
  /*
  // Connect to server
  if (connect(self->fd, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0) 
    return ERROR("unable to connect to server: %s", strerror(errno));
  */
  return 0;  
}


// Close the terminal
int close_term(sick_driver_t *self)
{
  close(self->fd);
  return 0;
}


// Set the terminal speed
int set_term_speed(sick_driver_t *self, int speed)
{
  struct termios term;
  struct serial_struct serial, old_serial;

  // we should check and reset the AYSNC_SPD_CUST flag
  // since if it's set and we request 38400, we're likely
  // to get another baud rate instead (based on custom_divisor)
  // this way even if the previous player doesn't reset the
  // port correctly, we'll end up with the right speed we want
  if (ioctl(self->fd, TIOCGSERIAL, &serial) < 0) 
  {
    //RETURN_ERROR(1, "error on TIOCGSERIAL in beginning");
    MSG("ioctl() failed while trying to get serial port info");
  }
  else
  {
    serial.flags &= ~ASYNC_SPD_CUST;
    serial.custom_divisor = 0;
    if (ioctl(self->fd, TIOCSSERIAL, &serial) < 0) 
    {
      MSG("ioctl() failed while trying to set serial port info");
    }
  }

  switch(speed)
  {
    case 9600:
      if( tcgetattr( self->fd, &term ) < 0 )
        return ERROR("unable to get device attributes");        
      cfmakeraw( &term );
      cfsetispeed( &term, B9600 );
      cfsetospeed( &term, B9600 );        
      if( tcsetattr( self->fd, TCSAFLUSH, &term ) < 0 )
        return ERROR("unable to set device attributes");
      break;

    case 38400:
      if( tcgetattr( self->fd, &term ) < 0 )
        return ERROR("unable to get device attributes");
      cfmakeraw( &term );
      cfsetispeed( &term, B38400 );
      cfsetospeed( &term, B38400 );
      if( tcsetattr( self->fd, TCSAFLUSH, &term ) < 0 )
        return ERROR("unable to set device attributes");
      break;

    case 500000:
      if (ioctl(self->fd, TIOCGSERIAL, &old_serial) < 0) 
        return ERROR("error on TIOCGSERIAL ioctl");    
      serial = old_serial;    
      serial.flags |= ASYNC_SPD_CUST;
      serial.custom_divisor = 48; // for FTDI USB/serial converter divisor is 240/5    
      if (ioctl(self->fd, TIOCSSERIAL, &serial) < 0)
        return ERROR("error on TIOCSSERIAL ioctl");

      // even if we are doing 500kbps, we have to set the speed to 38400...
      // the driver will know we want 500000 instead.

      if( tcgetattr( self->fd, &term ) < 0 )
        return ERROR("unable to get device attributes");    
      cfmakeraw( &term );
      cfsetispeed( &term, B38400 );
      cfsetospeed( &term, B38400 );
      if( tcsetattr( self->fd, TCSAFLUSH, &term ) < 0 )
        return ERROR("unable to set device attributes");
      break;
      
    default:
      return ERROR("unknown speed %d", speed);
  }
  return 0;
}


// Put the laser into configuration mode
int set_laser_mode(sick_driver_t *self)
{
  int tries;
  ssize_t len;
  uint8_t packet[20];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    packet[0] = 0x20; /* mode change command */
    packet[1] = 0x00; /* configuration mode */
    packet[2] = 0x53; // S - the password 
    packet[3] = 0x49; // I
    packet[4] = 0x43; // C
    packet[5] = 0x4B; // K
    packet[6] = 0x5F; // _
    packet[7] = 0x4C; // L
    packet[8] = 0x4D; // M
    packet[9] = 0x53; // S
    len = 10;
  
    MSG("sending configuration mode request to laser");
    if (write_to_laser(self, packet, len) < 0)
      return -1;

    // Wait for laser to return ack
    // This could take a while...
    MSG("waiting for acknowledge");
    len = read_from_laser(self, NULL, packet, sizeof(packet), true, 2000);

    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != ACK)
      return ERROR("unexpected packet type");
    break;
  }
  return (tries >= DEFAULT_LASER_RETRIES);
}


// Set the laser data rate
int set_laser_speed(sick_driver_t *self, int speed)
{
  int tries;
  ssize_t len;
  uint8_t packet[20];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    packet[0] = 0x20;
    packet[1] = (speed == 9600 ? 0x42 : (speed == 38400 ? 0x40 : 0x48));
    len = 2;

    if (write_to_laser(self, packet, len) < 0)
      return -1;
            
    // Wait for laser to return ack
    len = read_from_laser(self, NULL, packet, sizeof(packet), true, 10000);
    if (len < 0)
      return -1;
    else if (len < 1)
      return ERROR("no reply from laser");
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != ACK)
      return ERROR("unexpected packet type");
    break;
  }
  return (tries >= DEFAULT_LASER_RETRIES);
}


// Get the laser type
int get_laser_type(sick_driver_t *self, char *buffer, size_t bufflen)
{
  int tries;
  ssize_t len;
  uint8_t packet[512];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    packet[0] = 0x3A;
    len = 1;

    if (write_to_laser(self, packet, len) < 0)
    {
      printf("returning -1 on write_to_laser\n");
      return -1;
    }

    // Wait for laser to return data
    len = read_from_laser(self, NULL, packet, sizeof(packet), false, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != 0xBA)
      return ERROR("unexpected packet type");

    // NULL terminate the return string
    assert((size_t) len < sizeof(packet));
    packet[len] = 0;

    // Copy to buffer
    assert(bufflen >= (size_t) len - 1);
    strcpy(buffer, (char*) (packet + 1));

    break;
  }

  return (tries >= DEFAULT_LASER_RETRIES);
}


// Set the laser configuration
int set_laser_config(sick_driver_t *self, int range_res, bool retro)
{
  int tries;
  ssize_t len;
  uint8_t npacket[512], packet[512];

  // Get current config
  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    npacket[0] = 0x74;
    len = 1;

    if (write_to_laser(self, npacket, len) < 0)
      return -1;

    // Wait for laser to return data
    len = read_from_laser(self, NULL, npacket, sizeof(npacket), false, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (npacket[0] == NACK)
      return ERROR("request denied by laser");
    else if (npacket[0] != 0xF4)
      return ERROR("unexpected packet type");
    break;
  }
  if (tries >= DEFAULT_LASER_RETRIES)
    return -1;
  
  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    memcpy(packet, npacket, sizeof(packet));

    // Modify the configuration and send it back
    packet[0] = 0x77;

    // Return intensity in top 3 data bits
    packet[6] = (retro ? 0x01 : 0x00); 

    // Set the units for the range reading
    if (range_res == 1)
      packet[7] = 0x01;
    else if (range_res == 10)
      packet[7] = 0x00;
    else if (range_res == 100)
      packet[7] = 0x02;
    else
      packet[7] = 0x01;

    len = 8;

    if (write_to_laser(self, packet, len) < 0)
      return -1;

    // Wait for the change to "take"
    len = read_from_laser(self, NULL, packet, sizeof(packet), false, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != 0xF7)
      return ERROR("unexpected packet type");
    break;
  }

  return (tries >= DEFAULT_LASER_RETRIES);
}


// Change the angular resolution of the laser
int set_laser_res(sick_driver_t *self, int width, int res)
{
  int tries;
  ssize_t len;
  uint8_t packet[512];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    len = 0;
    packet[len++] = 0x3B;
    packet[len++] = (width & 0xFF);
    packet[len++] = (width >> 8);
    packet[len++] = (res & 0xFF);
    packet[len++] = (res >> 8);

    if (write_to_laser(self, packet, len) < 0)
      return -1;

    // Wait for laser to return data
    len = read_from_laser(self, NULL, packet, sizeof(packet), false, 3000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != 0xBB)
      return ERROR("unexpected packet type");

    // See if the request was accepted
    if (packet[1] == 0)
      return ERROR("variant request ignored");
    break;
  }

  return (tries >= DEFAULT_LASER_RETRIES);
}

//#define USE_NEW_CMD

// Request data from the laser
int request_laser_scan(sick_driver_t *self)
{
#ifdef USE_NEW_CMD
  return request_laser_scan_full(self);
#endif
  int tries;
  ssize_t len;
  uint8_t packet[20];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    len = 0;
    packet[len++] = 0x20; /* mode change command */
    
    // Use this for raw scan data...
    packet[len++] = 0x24;
    if (write_to_laser(self, packet, len) < 0)
      return -1;

    // Wait for laser to return ack
    // This should be fairly prompt
    len = read_from_laser(self, NULL, packet, sizeof(packet), true, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != ACK)
      return ERROR("unexpected packet type");
    break;
  }

  return (tries >= DEFAULT_LASER_RETRIES);
}

// Request data + reflectivity from the laser
int request_laser_scan_full(sick_driver_t *self)
{
  int tries;
  ssize_t len;
  uint8_t packet[20];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    len = 0;
    packet[len++] = 0x20; /* mode change command */
    
    // Use this for raw scan + reflectivity data...
    packet[len++] = 0x50;
    //0x0001 is reflectivity start
    packet[len++] = 0x01;
    packet[len++] = 0x00;
    //0x00B5=181 is reflectivity end
    packet[len++] = 0x02;
    packet[len++] = 0x00;

    if (write_to_laser(self, packet, len) < 0)
      return -1;

    // Wait for laser to return ack
    // This should be fairly prompt
    len = read_from_laser(self, NULL, packet, sizeof(packet), true, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != ACK)
      return ERROR("unexpected packet type");
    MSG("%d",packet[1]);
    assert(false);
    break;
  }

  return (tries >= DEFAULT_LASER_RETRIES);
}

// Read range data from laser
int read_laser_scan(sick_driver_t *self, uint64_t *timestamp,
                    int max_rays, int *num_rays, uint16_t *rays, int timeout)
{
  int len;
  int i, count, src;
  uint8_t raw_data[1024];

  // Read a packet from the laser
  len = read_from_laser(self, timestamp, raw_data, sizeof(raw_data), false, timeout);
  if (len <= 0)
    return ERROR("empty packet");
  MSG("Got scan packet of size %d",len);

  // Process raw packets
  if (raw_data[0] == 0xB0)
  {
    // Determine the number of values returned
    //int units = raw_data[2] >> 6;
    count = (int) raw_data[1] | ((int) (raw_data[2] & 0x3F) << 8);
    if (count > max_rays)
      return ERROR("bogus count: %d > %d", count, max_rays);

    // Strip the status info and shift everything down a few bytes
    // to remove packet header.
    for (i = 0; i < count; i++)
    {
      src = 2 * i + 3;
      rays[i] = raw_data[src + 0] | (raw_data[src + 1] << 8);
    }
  }
  else {
    MSG("Packet type %x received", raw_data[0]);
  }
  /* REMOVE or FIX
  else if (raw_data[0] == 0xB7)
  {
    // Determine which values were returned
    //
    //int first = ((int) raw_data[1] | ((int) raw_data[2] << 8)) - 1;
    //int last =  ((int) raw_data[3] | ((int) raw_data[4] << 8)) - 1;
        
    // Determine the number of values returned
    //
    //int units = raw_data[6] >> 6;
    count = (int) raw_data[5] | ((int) (raw_data[6] & 0x3F) << 8);
    assert((size_t) count <= maxrays);

    // Strip the status info and shift everything down a few bytes
    // to remove packet header.
    for (i = 0; i < count; i++)
    {
      src = 2 * i + 7;
      data[i] = raw_data[src + 0] | (raw_data[src + 1] << 8);
    }
  }
  */
  //  else
  // return ERROR("unexpected packet type");

  // Returns the number of rays we got
  *num_rays = count;
  
  return 0;
}


// Bit-bashing macros
#define MAKEUINT16(lo, hi) ((((uint16_t) (hi)) << 8) | ((uint16_t) (lo)))
#define LOBYTE(w) ((uint8_t) (w & 0xFF))
#define HIBYTE(w) ((uint8_t) ((w >> 8) & 0xFF))


// Get the time (in ms)
int64_t get_time()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return (int64_t) tv.tv_sec * 1000000 + (int64_t) tv.tv_usec;
}


// Create a CRC for the given packet
unsigned short create_crc(uint8_t* data, ssize_t len)
{
  uint16_t uCrc16;
  uint8_t abData[2];
  
  uCrc16 = 0;
  abData[0] = 0;
  
  while (len--)
  {
    abData[1] = abData[0];
    abData[0] = *data++;
    
    if( uCrc16 & 0x8000 )
    {
      uCrc16 = (uCrc16 & 0x7fff) << 1;
      uCrc16 ^= CRC16_GEN_POL;
    }
    else
    {    
      uCrc16 <<= 1;
    }
    uCrc16 ^= MAKEUINT16(abData[0],abData[1]);
  }
  
  return (uCrc16); 
}


// Write a packet to the laser
ssize_t write_to_laser(sick_driver_t *self, uint8_t *data, ssize_t len)
{
  uint8_t buffer[4 + 1024 + 2];
  uint16_t crc;
  ssize_t bytes;
  
  assert(4 + len + 2 < (ssize_t) sizeof(buffer));

  // Create header
  buffer[0] = STX;
  buffer[1] = 0;
  buffer[2] = LOBYTE(len);
  buffer[3] = HIBYTE(len);

  // Copy body
  memcpy(buffer + 4, data, len);

  // Create footer (CRC)
  crc = create_crc(buffer, 4 + len);
  buffer[4 + len + 0] = LOBYTE(crc);
  buffer[4 + len + 1] = HIBYTE(crc);

  /* REMOVE
  // have to write one char at a time, because if we're
  // high speed, then must take no longer than 55 us between
  // chars
  tcflush(self->fd, TCIOFLUSH);  
  if (self->current_rate > 38400)
  {    
    bytes = 0;
    for (i = 0; i < 6 + len; i++)
    {
      do
      {
        gettimeofday(&start, NULL);
        ret = write(self->fd, buffer + i, 1);	  
      }
      while (!ret);

      if (ret > 0)
        bytes += ret;

      // need to do this sort of busy wait to ensure the right timing
      // although I've noticed you will get some anamolies that are
      // in the ms range; this could be a problem...
      do
      {
        gettimeofday(&end, NULL);
        usecs = (end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec);
      }
      while (usecs < 60);
    }

  }
  */

  // Write in
  bytes = write(self->fd, buffer, 4 + len + 2);
  
  // Make sure the queue is drained
  // Synchronous IO doesnt always work
  tcdrain(self->fd);

  //MSG("wrote %d bytes", bytes);
    
  // Return the actual number of bytes sent, including header and footer
  return bytes;
}


// Read a packet from the laser
// Set ack to true to ignore all packets except ack and nack
// Set timeout to -1 to make this blocking, otherwise it will return in timeout ms.
// Returns the packet length (0 if timeout occurs)
ssize_t read_from_laser(sick_driver_t *self, uint64_t *timestamp,
                        uint8_t *data, ssize_t maxlen, bool ack, int timeout)
{
  int bytes, nbytes;
  ssize_t len;
  uint16_t crc;
  int nevents;
  struct pollfd fds = {0};
  uint8_t header[5] = {0};
  uint8_t footer[3];
  uint8_t buffer[4 + 1024 + 1];
  
  // Set up the descriptor for poll
  fds.fd = self->fd;
  fds.events = POLLIN;
  
  // Read until we get a valid header or we timeout.  This creates a
  // little shift register that runs until we get something that looks
  // like a packet header.
  bytes = 0;
  while (true)
  {
    // Use poll to do a timed wait
    nevents = poll(&fds, 1, timeout);
    if (nevents < 0)
      return ERROR("poll error %s", strerror(errno));
    if (nevents == 0)
    {
      MSG("timeout reading header");
      return 0;
    }

    // Read one byte and put it at the end of our shift register.
    nbytes = read(self->fd, header + sizeof(header) - 1, 1);   
    if (nbytes < 0)
      return ERROR("read error %s", strerror(errno));
    if (nbytes == 0)
      continue;

    // See if our shift register has something that looks like a
    // packet header.
    if (header[0] == STX && header[1] == 0x80)
    {
      if (!ack)
        break;
      if (header[4] == ACK || header[4] == NACK)
        break;
    }

    // We may be getting scan data, but no ack, so give
    // up after a while.
    bytes += nbytes;
    if (bytes > 1024)
    {
      MSG("invalid respose waiting for header");
      return 0;
    }

    // So shift down and try again
    memmove(header, header + 1, sizeof(header) - 1);
  }

  //MSG("head %X %X %X %X %X",
  //    header[0], header[1], header[2], header[3], header[4]);

  // Get the timestamp as soon as we read the header.
  // This should give us a more precise estimate.
  if (timestamp)
      *timestamp = get_time();
  
  // Determine data length.  Includes status, but not CRC, so subtract
  // status to get data packet length.
  len = ((int) header[2] | ((int) header[3] << 8)) - 1;
    
  // Check for buffer overflows
  if (len > maxlen)
    return ERROR("buffer overflow (len > max_len)");

  // Read in the data
  // Note that we smooge the packet type from the header
  // onto the front of the data buffer.
  bytes = 0;
  data[bytes++] = header[4];
  while (bytes < len)
  {
    nbytes = read(self->fd, data + bytes, len - bytes);
    if (nbytes < 0)
      return ERROR("read error %s", strerror(errno));
    bytes += nbytes;
  }

  // Read in footer
  bytes = 0;
  while (bytes < 3)
  {
    nbytes = read(self->fd, footer + bytes, 3 - bytes);
    if (nbytes < 0)
      return ERROR("read error %s", strerror(errno));
    bytes += nbytes;
  }

  //MSG("%X %X %X", footer[0], footer[1], footer[2]);
  
  // Construct entire packet
  // And check the CRC
  assert(4 + len + 1 < (ssize_t) sizeof(buffer));
  memcpy(buffer, header, 4);
  memcpy(buffer + 4, data, len);
  memcpy(buffer + 4 + len, footer, 1);
  crc = create_crc(buffer, 4 + len + 1);
  if (crc != MAKEUINT16(footer[1], footer[2]))
    return ERROR("CRC error, ignoring packet");
    
  return len;
}


#if SICK_DRIVER_TEST

// Simple laser test program
int main(int argc, char *argv[])
{
  int i;
  sick_driver_t *driver;
  int64_t timestamp;

  if (argc < 4)
  {
    fprintf(stderr,
            "usage: sick-driver-test serial <DEVICE> <SPEED> (e.g., serial /dev/ttyS0 9600)\n");
    fprintf(stderr,
            "usage: sick-driver-test tcp <HOST> <PORT> (e.g., tcp 192.168.0.70 8000)\n");
    fprintf(stderr,
            "usage: sick-driver-test udp <HOST> <PORT> (e.g., tcp 192.168.0.70 8000)\n");
    return -1;
  }
  
  driver = sick_driver_alloc();

  // Open either tcp or serial connection
  if (strcmp(argv[1], "serial") == 0)
  {
    const char *dev;
    int speed;

    dev = argv[2];
    speed = atoi(argv[3]);
    MSG("opening serial connection to %s %d", dev, speed);

    if (sick_driver_open_serial(driver, dev, speed, 100, 10) != 0)
      return -1;
  }
  else if (strcmp(argv[1], "tcp") == 0)
  {
    const char *host;
    int port;

    host = argv[2];
    port = atoi(argv[3]);
    MSG("opening tcp connection to %s %d", host, port);

    if (sick_driver_open_tcp(driver, host, port, 100, 10) != 0)
      return -1;
  }
  else if (strcmp(argv[1], "udp") == 0)
  {
    const char *host;
    int port;

    host = argv[2];
    port = atoi(argv[3]);
    MSG("opening udp connection to %s %d", host, port);

    if (sick_driver_open_udp(driver, host, port, 100, 10) != 0)
      return -1;
  }

  for (i = 0; i < 100; i++)
  {
    int num_ranges;
    float ranges[361];
    int retros[361];
    sick_driver_read(driver, &timestamp, sizeof(ranges)/sizeof(ranges[0]), &num_ranges, ranges, retros);


    MSG("scan %d %d rays", i, num_ranges);
  }

  sick_driver_close(driver);
  sick_driver_free(driver);
  
  return 0;
}

#endif





