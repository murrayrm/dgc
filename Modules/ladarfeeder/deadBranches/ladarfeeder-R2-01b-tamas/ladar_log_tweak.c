
/* Desc: Tweak ladar logs
 * Author: Andrew Howard
 * Date: 12 Mar 2005
 * CVS: $Id$
 *
 * Build with: gcc -o ladar-log-tweak -I../../include ladar_log_tweak.c -L../../lib/i486-gentoo-linux-static -lsensnetlog -lm
 *
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <frames/mat44.h>
#include <frames/pose3.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/LadarRangeBlob.h>


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Simple log self-test.
int main(int argc, char **argv)
{
  char *srcfilename, *dstfilename;
  sensnet_log_t *srclog, *dstlog;
  sensnet_log_header_t header;
  uint64_t timestamp;
  LadarRangeBlob blob;

  pose3_t pose;
  float sens2veh[4][4];
  float veh2sens[4][4];
    
  if (argc < 3)
  {
    printf("usage: %s <SRC> <DST>\n", argv[0]);
    return -1;
  }
  srcfilename = argv[1];
  dstfilename = argv[2];

  // Create source log
  srclog = sensnet_log_alloc();
  assert(srclog);
  MSG("opening source %s", srcfilename);
  if (sensnet_log_open_read(srclog, srcfilename, &header) != 0)
    return -1;

  // Create destination log
  dstlog = sensnet_log_alloc();
  assert(dstlog);
  MSG("opening source %s", dstfilename);
  if (sensnet_log_open_write(dstlog, dstfilename, &header, false) != 0)
    return -1;

  // Create new transform (LF)
  //pose.pos = vec3_set(+3.095625, -0.672982, -1.976634);
  //pose.rot = quat_from_rpy(3.173, -0.2476, -0.796);
  //pose3_to_mat44f(pose, sens2veh);
  //mat44f_inv(veh2sens, sens2veh);

  /*
  {
    float m[4][4] = 
      {{ 0.66058386, -0.71758042,  0.22069734,  3.01156104},
       {-0.69959551, -0.69502982, -0.16583023, -0.73663904},
       { 0.27238775, -0.0448541, -0.96114152, -1.99101378},
       { 0.        ,  0.       ,  0.        ,  1.        }};

    mat44f_setf(sens2veh, m);
    mat44f_inv(veh2sens, sens2veh);
  }
  */
  
  // Create new transform (RF)
  //pose.pos = vec3_set(+3.102206, +0.673193, -1.986133);
  //pose.rot = quat_from_rpy(3.0880, -0.1321, 0.7957);
  //pose3_to_mat44f(pose, sens2veh);
  //mat44f_inv(veh2sens, sens2veh);

  {
    float m[4][4] = 
      {{ 0.67701575,  0.71651934,  0.16807649,  3.02848703},
       { 0.71382647, -0.69489126,  0.08705119,  0.74598796},
       { 0.17916875,  0.06104242, -0.9819228 , -1.98986953},
       { 0.        ,  0.        ,  0.        ,  1.        }};
    mat44f_setf(sens2veh, m);
    mat44f_inv(veh2sens, sens2veh);
  }
    
  while (true)
  {
    uint64_t timestamp;
    int sensor_id, blob_type, blob_id, blob_size;

    // Read blob
    if (sensnet_log_peek(srclog, NULL, NULL, NULL, NULL, &blob_size) != 0)
      return -1;
    if (sensnet_log_read(srclog, &timestamp, &sensor_id,
                         &blob_type, &blob_id, sizeof(blob), &blob) != 0)
      return -1;

    MSG("read blob %d %d bytes %d points", blob_id, blob_size, blob.numPoints);

    // Modify the transform
    mat44f_setf(blob.sens2veh, sens2veh);
    mat44f_setf(blob.veh2sens, veh2sens);
    
    // Write new blob with modified calibration
    if (sensnet_log_write(dstlog, timestamp, sensor_id,
                          blob_type, blob_id, blob_size, &blob) != 0)
      return -1;
  }

  // Clean up
  sensnet_log_close(dstlog);
  sensnet_log_free(dstlog);
  sensnet_log_close(srclog);
  sensnet_log_free(srclog);
  
  return 0;
}

