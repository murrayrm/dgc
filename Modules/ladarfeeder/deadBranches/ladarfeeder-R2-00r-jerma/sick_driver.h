/*
 Desc: Driver for the SICK laser
 Author: Andrew Howard, adapted from the Player SickLMS200 driver.
 Date: 9 Oct 2006
 CVS: $Id: sicklms200.cc,v 1.60 2006/10/05 20:05:03 gerkey Exp $
*/


#ifndef SICK_DRIVER_H
#define SICK_DRIVER_H

#if __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>


/// @brief Class data for SICK LMS series driver
typedef struct
{
  // Operating mode (socket or serial)
  int mode;
  
  /// Laser device file/socket descriptor
  int fd;           

  /// Scan resolution (TODO).
  int scan_res;

  /// Range resolution (1 = 1mm, 10 = 1cm, 100 = 10cm).
  int range_res;

} sick_driver_t;


/// @brief Allocate driver object.
/// @returns Returns a newly allocated object.
sick_driver_t *sick_driver_alloc();

/// @brief Free driver object.
/// @param[in] self Context.
void sick_driver_free(sick_driver_t *self);

/// @brief Open comms and initialize laser.
/// @param[in] self Context.
/// @param[in] port Serial port device (e.g., "/dev/ttyS1").
/// @param[in] rate Device baud rant; usually 9600 or 500000.  
/// @returns Returns non-zero on error.
int sick_driver_open_serial(sick_driver_t *self, const char *port, int rate,
                            int scan_res, int range_res);

/// @brief Open comms and initialize laser.
/// @param[in] self Context.
/// @param[in] host IP address of the adaptor (e.g., 192.168.0.70).
/// @param[in] port Port number of the laser (e.g., 8000).  
/// @returns Returns non-zero on error.
int sick_driver_open_tcp(sick_driver_t *self, const char *host, int port,
                         int scan_res, int range_res);

/// @brief Open comms and initialize laser.
/// @param[in] self Context.
/// @param[in] host IP address of the adaptor (e.g., 192.168.0.70).
/// @param[in] port Port number of the laser (e.g., 8000).  
/// @returns Returns non-zero on error.
int sick_driver_open_udp(sick_driver_t *self, const char *host, int port,
                         int scan_res, int range_res);

/// @brief Close everything down.
/// @param[in] self Context.
/// @returns Returns non-zero on error.
int sick_driver_close(sick_driver_t *self);

/// @brief Read data from the laser.
///
/// Read a complete scan, including range and retro-reflector data.
///
/// @param[in] self Context.
/// @param[out] timestamp Scan timestamp, in microseconds since epoch.
/// @param[in] max_ranges Length of range/retro arrays.
/// @param[out] num_ranges Number of rays in scan.
/// @param[out] ranges Detected range data.
/// @param[out] retros Detected retro-reflector data.
/// @returns Returns non-zero on error
int sick_driver_read(sick_driver_t *self, uint64_t *timestamp,
                     int max_ranges, int *num_ranges, float *ranges, int *retros);  

#if __cplusplus
}
#endif

#endif



