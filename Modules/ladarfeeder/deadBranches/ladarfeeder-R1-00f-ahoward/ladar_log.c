
/* Desc: DGC ladar data logging
 * Author: Andrew Howard
 * Date: 9 Nov 2005
 * CVS: $Id$
 */

#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <rpc/types.h>
#include <rpc/xdr.h>

#include "ladar_log.h"


// Logging context.  
struct ladar_log
{
  // Log directory
  char *logdir;

  // Metadata file
  FILE *file;

  // Metadata stream
  XDR xdrs;

  // Number of frames written/read
  int num_frames;
};


// Serialization functions
static bool xdr_header(XDR *xdrs, ladar_log_header_t *header);
static bool xdr_state(XDR *xdrs, VehicleState *state);
static bool xdr_scan(XDR *xdrs, ladar_log_scan_t *scan);


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stdout, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Allocate object
ladar_log_t *ladar_log_alloc()
{
  ladar_log_t *self;

  self = calloc(1, sizeof(ladar_log_t));

  return self;
}


// Free object
void ladar_log_free(ladar_log_t *self)
{
  if (self->logdir)
    free(self->logdir);
  free(self);

  return;
}


// Open file for writing
int ladar_log_open_write(ladar_log_t *self,
                         const char *logdir, ladar_log_header_t *header)
{
  char filename[1024];
  
  self->logdir = strdup(logdir);

  // Create directory
  if (mkdir(self->logdir, 0x1ED) != 0) 
    return ERROR("unable to create: %s %s", self->logdir, strerror(errno));

  // Create index file
  snprintf(filename, sizeof(filename), "%s/scan.xdr", self->logdir);  
  self->file = fopen(filename, "w");
  if (!self->file)
    return ERROR("unable to open: %s %s", filename, strerror(errno));

  // Overwrite version
  header->version = LADAR_LOG_VERSION;

  // Create the encoding stream
  xdrstdio_create(&self->xdrs, self->file, XDR_ENCODE);

  // Encode the header
  if (!xdr_header(&self->xdrs, header))
    return ERROR("unable to encode log header");
      
  return 0;
}


// Open file for reading
int ladar_log_open_read(ladar_log_t *self,
                        const char *logdir, ladar_log_header_t *header)
{
  char filename[1024];
  
  self->logdir = strdup(logdir);

  // Open index file
  snprintf(filename, sizeof(filename), "%s/scan.xdr", self->logdir);  
  self->file = fopen(filename, "r");
  if (!self->file)
    return ERROR("unable to open: %s %s", filename, strerror(errno));

  // Create the decoding stream
  xdrstdio_create(&self->xdrs, self->file, XDR_DECODE);

  // Decode the header
  if (!xdr_header(&self->xdrs, header))
    return ERROR("unable to decode log header");

  // Check the version
  if (header->version != LADAR_LOG_VERSION)
    return ERROR("log is version %d, code is version %d", header->version, LADAR_LOG_VERSION);
  
  return 0;
}


// Close the log
int ladar_log_close(ladar_log_t *self)
{
  xdr_destroy(&self->xdrs);
  fclose(self->file);
  return 0;
}


// Write an image to the log
int ladar_log_write(ladar_log_t *self, ladar_log_scan_t *scan)
{
  // Write scan data
  if (!xdr_scan(&self->xdrs, scan))
    return ERROR("unable to write scan");
  fflush(self->file);
  fsync(fileno(self->file));

  return 0;
}


// Read an image from the log
int ladar_log_read(ladar_log_t *self, ladar_log_scan_t *scan)
{
  if (feof(self->file))
    return ERROR("end-of-file");

  // Read scan data
  if (!xdr_scan(&self->xdrs, scan))
    return ERROR("unable to read scan");

  // Record number of frames read
  self->num_frames++;

  return 0;
}


// Serialize header
bool xdr_header(XDR *xdrs, ladar_log_header_t *header)
{
  if (!xdr_int(xdrs, &header->version))
    return false;
  if (!xdr_int(xdrs, &header->ladar_id))
    return false;
  
  return true;
}


// Serialize state 
bool xdr_state(XDR *xdrs, VehicleState *state)
{
  #warning "Serializing VehicleState as opaque blob"

  if (!xdr_opaque(xdrs, (void*) state, sizeof(*state)))
    return false;

  return true;
}


// Serialize scan
bool xdr_scan(XDR *xdrs, ladar_log_scan_t *scan)                
{
  int i, j;
  
  if (!xdr_int(xdrs, &scan->scanid))
    return false;
  if (!xdr_hyper(xdrs, &scan->timestamp))
    return false;
  if (!xdr_state(xdrs, &scan->state))
    return false;

  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      if (!xdr_float(xdrs, &scan->sens2veh[j][i]))
        return false;

  if (!xdr_int(xdrs, &scan->num_points))
    return false;

  for (i = 0; i < scan->num_points; i++)
  {
    if (!xdr_float(xdrs, &scan->points[i][0]))
      return false;
    if (!xdr_float(xdrs, &scan->points[i][1]))
      return false;
  }

  return true;
}


#ifdef LADAR_LOG_TEST

// Simple self-test.
//
// Build with:
// gcc -Wall -DLADAR_LOG_TEST ladar_log.c -o ladar-log-test
int main(int argc, char **argv)
{
  char *filename;
  ladar_log_t *log;
  ladar_log_header_t header;
  ladar_log_scan_t scan;

  if (argc < 2)
  {
    printf("usage: %s <FILENAME>\n", argv[0]);
    return -1;
  }
  filename = argv[1];

  log = ladar_log_alloc();
  assert(log);

  if (ladar_log_open_read(log, filename, &header) != 0)
    return -1;

  printf("version: %X\n", header.version);

  while (true)
  {
    if (ladar_log_read(log, &scan) != 0)
      break;

    printf("scan %d %.3f\n",
           scan.scanid, scan.timestamp);
  }
  
  ladar_log_free(log);
  
  return 0;
}

#endif
