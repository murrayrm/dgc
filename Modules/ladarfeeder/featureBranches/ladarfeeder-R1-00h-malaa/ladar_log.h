
/* Desc: DGC ladar data logging
 * Author: Andrew Howard
 * Date: 9 Nov 2005
 * CVS: $Id$
 */

#ifndef LADAR_LOG_H
#define LADAR_LOG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stdbool.h>
#include <interfaces/VehicleState.h>

  
/// @brief Log version number
#define LADAR_LOG_VERSION 0x02

/// @brief Maximum points in a range scan
#define LADAR_LOG_MAX_POINTS 181
  

/// @brief Log header data
typedef struct
{
  /// File version (read-only)
  int version;

  /// Ladar id REMOVE?
  int ladar_id;
  
  /// Image dimensions
  int cols, rows, channels;
  
} ladar_log_header_t;
   

/// @brief Scan data
typedef struct
{
  /// Scan id
  int scanid;

  /// Scan timestamp
  uint64_t timestamp;

  /// Vehicle state data
  VehicleState state;

  /// Sensor-to-vehicle transform
  float sens2veh[4][4];

  /// Number of scan points
  int num_points;
  
  /// Scan points (bearing and range) in sensor frame.
  float points[LADAR_LOG_MAX_POINTS][2];

} ladar_log_scan_t;


/// @brief Ladar log context (opaque).
typedef struct ladar_log ladar_log_t;

/// @brief Allocate object
ladar_log_t *ladar_log_alloc();

/// @brief Free object
void ladar_log_free(ladar_log_t *self);

/// @brief Open file for writing
int ladar_log_open_write(ladar_log_t *self,
                         const char *logdir, ladar_log_header_t *header);

/// @brief Open file for reading
int ladar_log_open_read(ladar_log_t *self,
                        const char *logdir, ladar_log_header_t *header);

/// @brief Close the log
int ladar_log_close(ladar_log_t *self);

/// @brief Write a scan to the log
///
/// @param[in] scan Scan data.
int ladar_log_write(ladar_log_t *self, ladar_log_scan_t *scan);

/// @brief Read an image from the log
///
/// @param[out] scan Scan data.
int ladar_log_read(ladar_log_t *self, ladar_log_scan_t *scan);


#ifdef __cplusplus
}
#endif

#endif
