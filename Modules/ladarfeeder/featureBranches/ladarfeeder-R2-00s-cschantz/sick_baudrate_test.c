/*
 Desc: Driver for the SICK laser
 Author: Andrew Howard, adapted from the Player SickLMS200 driver.
 Date: 9 Oct 2006
 CVS: $Id: sicklms200.cc,v 1.60 2006/10/05 20:05:03 gerkey Exp $
*/

#include <assert.h>
#include <math.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/ioctl.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#include "sick_baudrate_test.h"




// Open the serial terminal
static int open_term_serial(sick_driver_t *self, const char *device_name);

// Open the IP socket
static int open_term_IP(sick_driver_t *self, const char *IP, int port);

// Close the terminal
static int close_term(sick_driver_t *self);

// Set the terminal speed
static int set_term_speed(sick_driver_t *self, int speed);

// Put the laser into configuration mode
static int set_laser_mode(sick_driver_t *self);

// Set the laser data rate
static int set_laser_speed(sick_driver_t *self, int speed);

// Get the laser type
static int get_laser_type(sick_driver_t *self, char *buffer, size_t bufflen);

// Set the laser configuration
static int set_laser_config(sick_driver_t *self, int range_res, bool retro);

// Change the angular resolution of the laser
static int set_laser_res(sick_driver_t *self, int width, int res);

// Set the laser baudrate permanently
static int set_laser_baudrate(sick_driver_t *self, int baudrate);

// Request range scan from the laser
static int request_laser_scan(sick_driver_t *self);

// Read range scan from laser
static int read_laser_scan(sick_driver_t *self, int max_rays, int *num_rays, uint16_t *rays);

// Write a packet to the laser
static ssize_t write_to_laser(sick_driver_t *self, uint8_t *data, ssize_t len);

// Read a packet from the laser
static ssize_t read_from_laser(sick_driver_t *self, uint8_t *data, ssize_t maxlen,
                               bool ack, int timeout);


// Laser device codes
#define STX     0x02
#define ACK     0xA0
#define NACK    0x92
#define CRC16_GEN_POL 0x8005
#define DEFAULT_LASER_RETRIES 3

// Error handling 
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg  : %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Allocate driver object.
sick_driver_t *sick_driver_alloc()
{
  sick_driver_t *self;

  self = calloc(1, sizeof(sick_driver_t));
    
  // Set default configuration
  self->current_rate = 0;
  self->retry_limit = 1;

  return self;
}


// Free driver object.
void sick_driver_free(sick_driver_t *self)
{
  free(self);
  return;
}


// Open comms and initialize laser.
int sick_driver_open_serial(sick_driver_t *self, const char *port, 
                     int connect_rate, int transfer_rate,
                     int scan_res, int range_res)
{
  char type[64];
  int current_rate;
  self->connect_type = SERIAL;
        
  // Open the terminal
  if (open_term_serial(self, port) != 0)
    return -1;
  
  // Try connecting at the given rate
  MSG("connecting at %d", connect_rate);
  if (set_term_speed(self, connect_rate) != 0)
  {
    close_term(self);
    return -1;
  }
  if (set_laser_mode(self) != 0)
  {
    close_term(self);
    return -1;
  }

  current_rate = connect_rate;

  if (current_rate != transfer_rate && transfer_rate == 38400)
  {
    // Jump up to 38400 rate
    MSG("operating at %d; changing to %d", current_rate, transfer_rate);
    if (set_laser_speed(self, transfer_rate))
      return -1;
    sleep(1);
    if (set_term_speed(self, transfer_rate))
      return -1;
    sleep(1);
  }
  else if (current_rate != transfer_rate && transfer_rate == 500000)
  {
    // Jump up to 500000 rate
    MSG("operating at %d; changing to %d", current_rate, transfer_rate);
    if (set_laser_speed(self, transfer_rate))
      return -1;
    sleep(1);
    if (set_term_speed(self, transfer_rate))
      return -1;
    sleep(1);
  }
  else if (current_rate != transfer_rate)
  {
    // Dont know this rate
    return ERROR("unsupported transfer rate %d", transfer_rate);
  }

  self->connect_rate = connect_rate;
  self->current_rate = current_rate;
  
  // Display the laser type
  memset(type, 0, sizeof(type));
  if (get_laser_type(self, type, sizeof(type)))
    return -1;

  MSG("SICK laser type %s at %s %d", type, port, transfer_rate);

  // Configure the laser
  if (set_laser_res(self, 180, scan_res))
    return -1;
  if (set_laser_config(self, range_res, true))
    return -1;

  self->scan_res = scan_res;
  self->range_res = range_res;

  /*
  // Request data
  if (request_laser_scan(self) != 0)
    return -1;
  */

  MSG("laser is up at %d", self->current_rate);

  return 0;
}


// Open comms and initialize laser.
int sick_driver_open_IP(sick_driver_t *self, const char *sickIP, 
			int sickPort, int connect_rate, 
			int scan_res, int range_res)
{
  char type[64];
  int current_rate;
  self->connect_type = IP;
        
  // Open the terminal
  if (open_term_IP(self, sickIP, sickPort) != 0)
    return -1;
  
  if (set_laser_mode(self) != 0)
  {
    close_term(self);
    return -1;
  }

  current_rate = connect_rate; 

  self->connect_rate = connect_rate;
  self->current_rate = current_rate;
  
  // Display the laser type
  memset(type, 0, sizeof(type));
  if (get_laser_type(self, type, sizeof(type)))
      return -1;

  MSG("SICK laser type %s at %s:%d", type, sickIP, sickPort);

  // Configure the laser
  if (set_laser_res(self, 180, scan_res))
    return -1;
  if (set_laser_config(self, range_res, true))
    return -1;

  self->scan_res = scan_res;
  self->range_res = range_res;
  
  // Request data
  if (request_laser_scan(self) != 0)
    return -1;

  MSG("laser is up at %d", self->current_rate);

  return 0;
}


// Close everything down.
int sick_driver_close(sick_driver_t *self)
{
  // Put the laser back to the original connection speed
  if (self->connect_rate != self->current_rate)
  {
    if (set_laser_speed(self, self->connect_rate) != 0)
      MSG("unable to reset laser speed");
    else
      self->current_rate = self->connect_rate;
  }

  // Close serial
  close_term(self);

  MSG("laser is down at %d", self->current_rate);
  
  return 0;
}


// Read scan data
int sick_driver_read(sick_driver_t *self,
                     int max_rays, int *num_rays, float *ranges,  int *retros)
{
  int i;
  uint16_t rays[361];
  
  // Read a scan
  if (read_laser_scan(self, sizeof(rays)/sizeof(rays[0]), num_rays, rays) != 0)
    return -1;

  // Convert into ranges
  for (i = 0; i < *num_rays; i++)
  {
    if (i >= max_rays)
      continue;
    ranges[i] = (float) (rays[i] & 0x1FFF) * self->range_res / 1e3;
    if (retros)
      retros[i] = ((rays[i] >> 13) & 0x0007);
  }

  return 0;
}


// Open the terminal
int open_term_serial(sick_driver_t *self, const char *port)
{
  struct termios term;
  
  self->laser_fd = open(port, O_RDWR | O_SYNC , S_IRUSR | S_IWUSR );
  if (self->laser_fd < 0)
    return ERROR("unable to open serial port %s: %s", port, strerror(errno));

  // set the serial port speed to 9600 to match the laser
  // later we can ramp the speed up 
  if ( tcgetattr( self->laser_fd, &term ) < 0 )
    return ERROR("unable to get serial port attributes: %s", strerror(errno));
  
  cfmakeraw( &term );
  cfsetispeed( &term, B9600 );
  cfsetospeed( &term, B9600 );
  
  if( tcsetattr( self->laser_fd, TCSAFLUSH, &term ) < 0 )
    return ERROR("unable to set serial port attributes: %s", strerror(errno));

  // Make sure queue is empty
  tcflush(self->laser_fd, TCIOFLUSH);
    
  return 0;
}


// Open the terminal
int open_term_IP(sick_driver_t *self, const char *sickIP, int sickPort)
{
  
  struct sockaddr_in serverAddr;
  struct hostent *serverHostInfo;
  int listen_socket, tempInt; 
  char host[30];

  strcpy(host, sickIP);

  serverHostInfo = gethostbyname(host);
  if (serverHostInfo == NULL) 
    return 0;
  
  // Create socket
  memset(&serverAddr,0,sizeof(serverAddr));
  serverAddr.sin_family = serverHostInfo->h_addrtype;
  memcpy((char *) &serverAddr.sin_addr.s_addr, serverHostInfo->h_addr_list[0], serverHostInfo->h_length);
  serverAddr.sin_port = htons(sickPort);
  listen_socket = socket(AF_INET, SOCK_STREAM, 0);
  if (listen_socket < 0) 
    return 0;
  
  // connect to server
  tempInt = connect(listen_socket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));
  if (tempInt < 0) 
    {
      MSG("could not connect...abort.");
      return -1;
    }

  self->laser_fd = listen_socket;
  
  MSG("connected!!");
  
  return 0;
  
}


// Close the terminal
int close_term(sick_driver_t *self)
{
  close(self->laser_fd);
  return 0;
}


// Set the terminal speed
int set_term_speed(sick_driver_t *self, int speed)
{
  struct termios term;
  struct serial_struct serial;

  // we should check and reset the AYSNC_SPD_CUST flag
  // since if it's set and we request 38400, we're likely
  // to get another baud rate instead (based on custom_divisor)
  // this way even if the previous player doesn't reset the
  // port correctly, we'll end up with the right speed we want
  if (ioctl(self->laser_fd, TIOCGSERIAL, &serial) < 0) 
  {
    //RETURN_ERROR(1, "error on TIOCGSERIAL in beginning");
    MSG("ioctl() failed while trying to get serial port info");
  }
  else
  {
    serial.flags &= ~ASYNC_SPD_CUST;
    serial.custom_divisor = 0;
    if (ioctl(self->laser_fd, TIOCSSERIAL, &serial) < 0) 
    {
      MSG("ioctl() failed while trying to set serial port info");
    }
  }

  switch(speed)
  {
    case 9600:
      if( tcgetattr( self->laser_fd, &term ) < 0 )
        return ERROR("unable to get device attributes");        
      cfmakeraw( &term );
      cfsetispeed( &term, B9600 );
      cfsetospeed( &term, B9600 );        
      if( tcsetattr( self->laser_fd, TCSAFLUSH, &term ) < 0 )
        return ERROR("unable to set device attributes");
      break;

    case 38400:
      if( tcgetattr( self->laser_fd, &term ) < 0 )
        return ERROR("unable to get device attributes");
      cfmakeraw( &term );
      cfsetispeed( &term, B38400 );
      cfsetospeed( &term, B38400 );
      if( tcsetattr( self->laser_fd, TCSAFLUSH, &term ) < 0 )
        return ERROR("unable to set device attributes");
      break;

    case 500000:
      if (ioctl(self->laser_fd, TIOCGSERIAL, &self->old_serial) < 0) 
        return ERROR("error on TIOCGSERIAL ioctl");    
      serial = self->old_serial;    
      serial.flags |= ASYNC_SPD_CUST;
      serial.custom_divisor = 48; // for FTDI USB/serial converter divisor is 240/5    
      if (ioctl(self->laser_fd, TIOCSSERIAL, &serial) < 0)
        return ERROR("error on TIOCSSERIAL ioctl");

      // even if we are doing 500kbps, we have to set the speed to 38400...
      // the driver will know we want 500000 instead.

      if( tcgetattr( self->laser_fd, &term ) < 0 )
        return ERROR("unable to get device attributes");    
      cfmakeraw( &term );
      cfsetispeed( &term, B38400 );
      cfsetospeed( &term, B38400 );
      if( tcsetattr( self->laser_fd, TCSAFLUSH, &term ) < 0 )
        return ERROR("unable to set device attributes");
      break;
      
    default:
      return ERROR("unknown speed %d", speed);
  }
  return 0;
}


// Put the laser into configuration mode
int set_laser_mode(sick_driver_t *self)
{
  int tries;
  ssize_t len;
  uint8_t packet[20];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    packet[0] = 0x20; /* mode change command */
    packet[1] = 0x00; /* configuration mode */
    packet[2] = 0x53; // S - the password 
    packet[3] = 0x49; // I
    packet[4] = 0x43; // C
    packet[5] = 0x4B; // K
    packet[6] = 0x5F; // _
    packet[7] = 0x4C; // L
    packet[8] = 0x4D; // M
    packet[9] = 0x53; // S
    len = 10;
  
    MSG("sending configuration mode request to laser");
    if (write_to_laser(self, packet, len) < 0)
      return -1;

    // Wait for laser to return ack
    // This could take a while...
    MSG("waiting for acknowledge");
    len = read_from_laser(self, packet, sizeof(packet), true, 2000);

    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != ACK)
      return ERROR("unexpected packet type");
    break;
  }
  return (tries >= DEFAULT_LASER_RETRIES);
}


// Set the laser data rate
int set_laser_speed(sick_driver_t *self, int speed)
{
  int tries;
  ssize_t len;
  uint8_t packet[20];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    packet[0] = 0x20;
    packet[1] = (speed == 9600 ? 0x42 : (speed == 38400 ? 0x40 : 0x48));
    len = 2;

    //PLAYER_MSG0(2, "sending baud rate request to laser");
    if (write_to_laser(self, packet, len) < 0)
      return -1;
            
    // Wait for laser to return ack
    len = read_from_laser(self, packet, sizeof(packet), true, 10000);
    if (len < 0)
      return -1;
    else if (len < 1)
      return ERROR("no reply from laser");
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != ACK)
      return ERROR("unexpected packet type");
    break;
  }
  return (tries >= DEFAULT_LASER_RETRIES);
}


// Get the laser type
int get_laser_type(sick_driver_t *self, char *buffer, size_t bufflen)
{
  int tries;
  ssize_t len;
  uint8_t packet[512];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    packet[0] = 0x3A;
    len = 1;

    if (write_to_laser(self, packet, len) < 0)
      {
	printf("returning -1 on write_to_laser\n");
	return -1;
      }

    // Wait for laser to return data
    len = read_from_laser(self, packet, sizeof(packet), false, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != 0xBA)
      return ERROR("unexpected packet type");

    // NULL terminate the return string
    assert((size_t) len < sizeof(packet));
    packet[len] = 0;

    // Copy to buffer
    assert(bufflen >= (size_t) len - 1);
    strcpy(buffer, (char*) (packet + 1));

    break;
  }

  return (tries >= DEFAULT_LASER_RETRIES);
}


// Set the laser configuration
int set_laser_config(sick_driver_t *self, int range_res, bool retro)
{
  int tries;
  ssize_t len;
  uint8_t npacket[512], packet[512];

  // Get current config
  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    npacket[0] = 0x74;
    len = 1;

    if (write_to_laser(self, npacket, len) < 0)
      return -1;

    // Wait for laser to return data
    len = read_from_laser(self, npacket, sizeof(npacket), false, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (npacket[0] == NACK)
      return ERROR("request denied by laser");
    else if (npacket[0] != 0xF4)
      return ERROR("unexpected packet type");
    break;
  }
  if (tries >= DEFAULT_LASER_RETRIES)
    return -1;
  
  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    memcpy(packet, npacket, sizeof(packet));

    // Modify the configuration and send it back
    packet[0] = 0x77;

    // Return intensity in top 3 data bits
    packet[6] = (retro ? 0x01 : 0x00); 

    // Set the units for the range reading
    if (range_res == 1)
      packet[7] = 0x01;
    else if (range_res == 10)
      packet[7] = 0x00;
    else if (range_res == 100)
      packet[7] = 0x02;
    else
      packet[7] = 0x01;

    len = 8;

    if (write_to_laser(self, packet, len) < 0)
      return -1;

    // Wait for the change to "take"
    len = read_from_laser(self, packet, sizeof(packet), false, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != 0xF7)
      return ERROR("unexpected packet type");
    break;
  }

  return (tries >= DEFAULT_LASER_RETRIES);
}


// Change the angular resolution of the laser
int set_laser_res(sick_driver_t *self, int width, int res)
{
  int tries;
  ssize_t len;
  uint8_t packet[512];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    len = 0;
    packet[len++] = 0x3B;
    packet[len++] = (width & 0xFF);
    packet[len++] = (width >> 8);
    packet[len++] = (res & 0xFF);
    packet[len++] = (res >> 8);

    if (write_to_laser(self, packet, len) < 0)
      return -1;

    // Wait for laser to return data
    len = read_from_laser(self, packet, sizeof(packet), false, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != 0xBB)
      return ERROR("unexpected packet type");

    // See if the request was accepted
    if (packet[1] == 0)
      return ERROR("variant request ignored");
    break;
  }

  return (tries >= DEFAULT_LASER_RETRIES);
}


// Change the baudrate of the laser
int set_laser_baudrate(sick_driver_t *self, int baudrate)
{
  int tries;
  ssize_t len;
  uint8_t packet[512];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    len = 0;
    packet[len++] = 0x66;
    packet[len++] = 0x01;
    //    packet[len++] = (width >> 8);
    //    packet[len++] = (res & 0xFF);
    //    packet[len++] = (res >> 8);

    if (write_to_laser(self, packet, len) < 0)
      return -1;

    // Wait for laser to return data
    len = read_from_laser(self, packet, sizeof(packet), false, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != 0xE6)
      return ERROR("unexpected packet type %X", packet[0]);


    // See if the request was accepted
    if (packet[1] != 0x01)    
      return ERROR("set permanent baudrate failed.");     
    else
      MSG("new baudrate set successfully!");

    break;
  }

  return (tries >= DEFAULT_LASER_RETRIES);
}



// Request data from the laser
int request_laser_scan(sick_driver_t *self)
{
  int tries;
  ssize_t len;
  uint8_t packet[20];

  for (tries = 0; tries < DEFAULT_LASER_RETRIES; tries++)
  {
    len = 0;
    packet[len++] = 0x20; /* mode change command */
    
    // Use this for raw scan data...
    packet[len++] = 0x24;
    if (write_to_laser(self, packet, len) < 0)
      return -1;

    // Wait for laser to return ack
    // This should be fairly prompt
    len = read_from_laser(self, packet, sizeof(packet), true, 2000);
    if (len < 0)
      return -1;
    else if (len < 1)
    {
      MSG("timeout");
      continue;
    }
    else if (packet[0] == NACK)
      return ERROR("request denied by laser");
    else if (packet[0] != ACK)
      return ERROR("unexpected packet type");
    break;
  }

  return (tries >= DEFAULT_LASER_RETRIES);
}


// Read range data from laser
int read_laser_scan(sick_driver_t *self, int max_rays, int *num_rays, uint16_t *rays)
{
  int len;
  int i, count, src;
  uint8_t raw_data[1024];

  // Read a packet from the laser
  len = read_from_laser(self, raw_data, sizeof(raw_data), false, -1);
  if (len == 0)
    return ERROR("empty packet");

  // Process raw packets
  if (raw_data[0] == 0xB0)
  {
    // Determine the number of values returned
    //int units = raw_data[2] >> 6;
    count = (int) raw_data[1] | ((int) (raw_data[2] & 0x3F) << 8);
    if (count > max_rays)
      return ERROR("bogust count: %d > %d", count, max_rays);

    // Strip the status info and shift everything down a few bytes
    // to remove packet header.
    for (i = 0; i < count; i++)
    {
      src = 2 * i + 3;
      rays[i] = raw_data[src + 0] | (raw_data[src + 1] << 8);
    }
  }
  else
    return ERROR("unexpected packet type");

  // Returns the number of rays we got
  *num_rays = count;
  
  return 0;
}


// Bit-bashing macros
#define MAKEUINT16(lo, hi) ((((uint16_t) (hi)) << 8) | ((uint16_t) (lo)))
#define LOBYTE(w) ((uint8_t) (w & 0xFF))
#define HIBYTE(w) ((uint8_t) ((w >> 8) & 0xFF))


// Get the time (in ms)
int64_t get_time()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  //GlobalTime->GetTime(&tv);
  return (int64_t) tv.tv_sec * 1000 + (int64_t) tv.tv_usec / 1000;
}


// Create a CRC for the given packet
unsigned short create_crc(uint8_t* data, ssize_t len)
{
  uint16_t uCrc16;
  uint8_t abData[2];
  
  uCrc16 = 0;
  abData[0] = 0;
  
  while(len-- )
  {
    abData[1] = abData[0];
    abData[0] = *data++;
    
    if( uCrc16 & 0x8000 )
    {
      uCrc16 = (uCrc16 & 0x7fff) << 1;
      uCrc16 ^= CRC16_GEN_POL;
    }
    else
    {    
      uCrc16 <<= 1;
    }
    uCrc16 ^= MAKEUINT16(abData[0],abData[1]);
  }
  return (uCrc16); 
}


// Write a packet to the laser
ssize_t write_to_laser(sick_driver_t *self, uint8_t *data, ssize_t len)
{
  uint8_t buffer[4 + 1024 + 2];
  uint16_t crc;
  ssize_t bytes;
  int i, ret, usecs;
  struct timeval start, end;
  
  assert(4 + len + 2 < (ssize_t) sizeof(buffer));

  // Create header
  buffer[0] = STX;
  buffer[1] = 0;
  buffer[2] = LOBYTE(len);
  buffer[3] = HIBYTE(len);

  // Copy body
  memcpy(buffer + 4, data, len);

  // Create footer (CRC)
  crc = create_crc(buffer, 4 + len);
  buffer[4 + len + 0] = LOBYTE(crc);
  buffer[4 + len + 1] = HIBYTE(crc);

  // Make sure both input and output queues are empty
  if(self->connect_type==SERIAL)
    tcflush(self->laser_fd, TCIOFLUSH);

  // have to write one char at a time, because if we're
  // high speed, then must take no longer than 55 us between
  // chars
  if (self->current_rate > 38400)
  {
    printf("LASER: writing %d bytes\n", 6+len);
    bytes = 0;
    for (i = 0; i < 6 + len; i++)
    {
      do
      {
        gettimeofday(&start, NULL);
	if(self->connect_type==SERIAL)	  
	    ret = write(self->laser_fd, buffer + i, 1);	  
	else	  
	    ret = send(self->laser_fd, buffer+i,1,0);
	  
      }
      while (!ret);

      if (ret > 0)
        bytes += ret;

      // need to do this sort of busy wait to ensure the right timing
      // although I've noticed you will get some anamolies that are
      // in the ms range; this could be a problem...
      do
      {
        gettimeofday(&end, NULL);
        usecs= (end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec);
      }
      while (usecs < 60);
    }
  }
  else
  {
    if(self->connect_type==SERIAL)
      bytes = write( self->laser_fd, buffer, 4 + len + 2);
    else
      bytes = send(self->laser_fd, buffer, 4+ len + 2,0);
  }
    
  // Write the data to the port
  if(self->connect_type==SERIAL)
    {
      bytes = write( self->laser_fd, buffer, 4 + len + 2);

      // Make sure the queue is drained
      // Synchronous IO doesnt always work
      tcdrain(self->laser_fd);
    }
  else
    {
      bytes = send( self->laser_fd, buffer, 4 + len + 2,0);

      // Make sure the queue is drained
      // Synchronous IO doesnt always work
      fsync(self->laser_fd);  
    }

  printf("bytes: %d \n", bytes);
    
  // Return the actual number of bytes sent, including header and footer
  return bytes;
}


// Read a packet from the laser
// Set ack to true to ignore all packets except ack and nack
// Set timeout to -1 to make this blocking, otherwise it will return in timeout ms.
// Returns the packet length (0 if timeout occurs)
ssize_t read_from_laser(sick_driver_t *self, uint8_t *data, ssize_t maxlen, bool ack, int timeout)
{
  int flags;
  int64_t start_time, stop_time;
  int bytes;
  ssize_t len;
  uint16_t crc;
  uint8_t header[5] = {0};
  uint8_t footer[3];
  uint8_t buffer[4 + 1024 + 1];
  

  //sleepytime of 7000 so so
  //sleeptyime of 7500 works great
  //sleepytime of 8000 works great
  //sleepytime of 8500 works well
  //sleepytime of 9500 works ok
  //sleepytime of 9750 works ok
  //sleepytime of 9800 works so so
  //sleepytime of 10000 is bad
  unsigned long sleepytime = 10000;
  
  // If the timeout is infinite,
  // go to blocking io

  if (timeout < 0)
  {
    flags = fcntl(self->laser_fd, F_GETFL);
    if (flags < 0)
      return ERROR("unable to get device flags");
    if (fcntl(self->laser_fd, F_SETFL, flags & (~O_NONBLOCK)) < 0)
      return ERROR("unable to set device flags");
  }
  //
  // Otherwise, use non-blocking io
  //
  else
  {
    flags = fcntl(self->laser_fd, F_GETFL);
    if (flags < 0)
      return ERROR("unable to get device flags");
    if (fcntl(self->laser_fd, F_SETFL, flags | O_NONBLOCK) < 0)
      return ERROR("unable to set device flags");
  }

  start_time = get_time();
  stop_time = start_time + timeout;

  // Read until we get a valid header
  // or we timeout  
  while (true)
  {
    if (timeout >= 0)
      usleep(sleepytime);
    if(self->connect_type==SERIAL)
      bytes = read(self->laser_fd, header + sizeof(header) - 1, 1);
    else      
      bytes = recv(self->laser_fd, header + sizeof(header) - 1, 1,0);
    
    if (header[0] == STX && header[1] == 0x80)
    {
      if (!ack)
        break;
      if (header[4] == ACK || header[4] == NACK)
        break;
    }
    memmove(header, header + 1, sizeof(header) - 1);
    if (timeout >= 0 && get_time() >= stop_time)
    {
      //return 0;
      return ERROR("timeout on read (1)");
    }
  }

  // Determine data length.
  // Includes status, but not CRC, so subtract status to get data packet length.
  len = ((int) header[2] | ((int) header[3] << 8)) - 1;
    
  // Check for buffer overflows
  if (len > maxlen)
    return ERROR("buffer overflow (len > max_len)");

  // Read in the data
  // Note that we smooge the packet type from the header
  // onto the front of the data buffer.
  bytes = 0;
  data[bytes++] = header[4];
  while (bytes < len)
  {
    if (timeout >= 0)
      usleep(sleepytime);

    if(self->connect_type==SERIAL)
      bytes += read(self->laser_fd, data + bytes, len - bytes);
    else
      bytes += recv(self->laser_fd, data + bytes, len - bytes,0);

    if (timeout >= 0 && get_time() >= stop_time)
      return ERROR("timeout on read (2)");
  }


  // Read in footer
  //
  bytes = 0;
  while (bytes < 3)
  {
    if (timeout >= 0)
      usleep(sleepytime);

    if(self->connect_type==SERIAL)
      bytes += read(self->laser_fd, footer + bytes, 3 - bytes);
    else
      bytes += recv(self->laser_fd, footer + bytes, 3 - bytes,0);

    if (timeout >= 0 && get_time() >= stop_time)
      return ERROR("timeout on read (3)");
  }
    
  // Construct entire packet
  // And check the CRC

  assert(4 + len + 1 < (ssize_t) sizeof(buffer));
  memcpy(buffer, header, 4);
  memcpy(buffer + 4, data, len);
  memcpy(buffer + 4 + len, footer, 1);
  crc = create_crc(buffer, 4 + len + 1);
  if (crc != MAKEUINT16(footer[1], footer[2]))
    return ERROR("CRC error, ignoring packet");
    
  return len;
}


// Simple laser test program
int main(int argc, char *argv[])
{
  int i;
  const char *devPort;
  const char *sickIP;
  int sickPort;
  int conn_speed, scan_speed;
  sick_driver_t *driver;
  int baudrate=500000;

  if (argc < 4)
    {
      fprintf(stderr, "usage: sick_baudrate_test <PORT> <CONN_SPEED> <SCAN_SPEED> (e.g., /dev/ttyUSB0 9600 500000)\n");
      return -1;
    }
  
  devPort = argv[1];
  conn_speed = atoi(argv[2]);
  scan_speed = atoi(argv[3]);
  
  driver = sick_driver_alloc();
  if(sick_driver_open_serial(driver, devPort, conn_speed, scan_speed, 100, 10)!=0)
    return -1;
    
  printf("baudrate is going to be permanently set to %d\n", baudrate);
  printf("You have 10 seconds to abort ... \n");
  
  for(i=0;i<10;i++)
  {
    sleep(1);
    printf("%d \n", 10-(i+1));
  }

  set_laser_baudrate(driver,baudrate);  

  printf("closing connection now. Goodbye. \n");

  sick_driver_close(driver);
  sick_driver_free(driver);
  
  return 0;
}






