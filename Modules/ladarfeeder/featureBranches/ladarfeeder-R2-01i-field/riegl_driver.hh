#ifndef RIEGL_DRIVER_H
#define RIEGL_DRIVER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h> //ok
#include <arpa/inet.h>  //ok
#include <netdb.h> // ok
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <iostream>
#include <dgcutils/DGCutils.hh>

using namespace std;

#define LADAR_BUF_SIZE 100  /* in frames */
#define LADAR_BUF_FILL_FRAME 2
#define LADAR_BUF_FILL_MSG 1

#define RIEGL_MAX_CHAR 512
#define RIEGL_CTRL_PORT 20002
#define RIEGL_DATA_PORT 20001
#define RIEGL_DEFAULT_IP "192.168.2.234"
#define RIEGL_MSG_TIMEOUT 3

#define RIEGL_ERR_RESET -2
#define RIEGL_ERR_GEN -1
#define RIEGL_ERR_TIMO 0 

#define RIEGL_NO_MODE 0
#define RIEGL_PROG_MODE 1 
#define RIEGL_SCAN_MODE 2
#define RIEGL_PLAYBACK_MODE 4

#define RIEGL_RESP_ERR 0
#define RIEGL_RESP_OK 1
#define RIEGL_RESP_MSG 2


#define RIEGL_EOL \x00A\x00D

#define R(x) #x 
#define RIEGL_CMD(x) R(x)  R(\x00A\x00D)
#define RIEGL_CMD_LASER_ON RIEGL_CMD(\x00E)
#define RIEGL_CMD_PROG_ON "\x010"
#define RIEGL_CMD_SCAN_ON RIEGL_CMD(Q)
#define RIEGL_CMD_SAVE_PARAMS RIEGL_CMD(W)
#define RIEGL_CMD_LASER_ON RIEGL_CMD(\x00E)
#define RIEGL_CMD_LASER_OFF RIEGL_CMD(\x006)

#define RIEGL_TEST RIEGL_CMD(.HELP)

#define RIEGL_SCAN_RANGE 1
#define RIEGL_SCAN_INTENSITY 4
#define RIEGL_SCAN_ANGLE 8
#define RIEGL_SCAN_QUALITY 32
#define RIEGL_SCAN_TIME 64

#define ulong unsigned long
#define uchar unsigned char
#define ushort unsigned short int

struct SRieglDataHeader {
	ulong   header_size;
	ushort   dataset_size;
	uchar protocol_id;
	uchar header_id;
	ushort meas_offset;
	ushort meas_size;
	ushort meas_count;
	uchar leadin_main;
	ushort leadin_sub;
	uchar measid_main;
	ushort measid_sub;
	uchar trailerid_main;
	ushort trailerid_sub;
	uchar paramid_main;
	ushort paramid_sub;
	char serial_num[RIEGL_MAX_CHAR];
	float range_unit;
	float angle_unit;
	float timer_unit;
	uchar polar_angleid;
	uchar hw_res;
	uchar target_sel;
	ushort beam_aperture;
	ushort beam_divergence;
	ushort beam_focus; //FF FF is for infinity
	ushort beam_sep_lenght;
	ulong FactoryAdjustmentData[28]; //Padding
};


struct SRieglDataPoint {
	ulong range;
	uchar amplitude;
	ulong mirror_angle;
	uchar quality;
	ulong timestamp;
};



/// Riegl LADAR device interface
class CRieglLadar {
 private:

        //taken from CLadarBuffer Class
	char* frames[LADAR_BUF_SIZE];	
	int frame_size;
	int c_frame;
	int c_frame_offset;
	int last_frame_sent; 


	char host[RIEGL_MAX_CHAR]; //its always good to know who we talk to 
	char reply_buf[RIEGL_MAX_CHAR];		
	char header_buf[RIEGL_MAX_CHAR]; //contains the initial msg sent by the ladar, which has the header		
	int reply_size; 

	int ctrl_sock; 
	fd_set ctrl_fdset;
		
	int data_sock; 
	fd_set data_fdset; 

	int mode; 
	timeval tl; 
	
	unsigned long long start_time; 
	unsigned long long curr_time;
	ulong last_scan_time;

	SRieglDataHeader header;
	SRieglDataPoint pnt; //used for scanning 

	pthread_mutex_t frames_mux;	
	pthread_mutex_t cond_mux; 
 	pthread_cond_t cond;
	bool has_frame;

	bool m_bRieglHasMode;
	pthread_cond_t m_rieglHasModeCondition;
	pthread_mutex_t m_rieglHasModeConditionMutex;

	bool m_bIsNotFull;
	pthread_cond_t m_isNotFullCondition;
	pthread_mutex_t m_isNotFullConditionMutex;


        //taken from CLadarBuffer class
	int num_frames() {
	  return (c_frame - last_frame_sent - 1 + LADAR_BUF_SIZE)
	    % LADAR_BUF_SIZE; 
	}	
	int _NextFrame(); 

	//get messages from ctrl and data socks , stubs to _GetMsg
	int _GetMsg(char *msg, int size, int socket, fd_set sock_fdset); 
	int _GetCtrlMsg(char *msg, int size, int vfy=0, int ignm = 1);
	int _GetDataMsg(char *msg, int size, int vfy=0); 
	
	//send messages to control and data socks
	int _SendCtrlMsg(char *msg, int size); 
	int _SendDataMsg(char *msg, int size);
		
	int _MakeSocket(int port, int &sock, fd_set &sock_fd);
	void _CloseSocket(int &socket); 
	int _StartProgMode();
	int _StartScanMode();
	
	int _StartLaser();
	int _StopLaser();
		
	int _ResetDataSock();
	int _VerifyResponse(char *msg,int size);	
		
	void _ProcessHeader(char *msg, ulong size);
		
	int _SaveParamChanges();

	void _GetFramePoint(char *msg);
	void _UpdateCurrTime(ulong tn);	
	void _ReceiveDataFrames();
	int _ExtractFrame(char *c_frame, unsigned long long *times, double *ranges, double *angles, uchar *amps, uchar *quals);
 public:
	CRieglLadar();
	~CRieglLadar();
	int Connect( char *host=NULL);
	void Disconnect();
	int SetScanOpts(uchar opts);
	int StartScan();
	int StopScan();
	void SetMsgTimeOut( int sec, int usec=0);
	void ReportHeader();
	int GetScanFrame(unsigned long long *times, double *ranges,  double *angles, uchar *amps, uchar *quals, unsigned long long *frame_time);

        //taken from CLadarBuffer class
	void InitFrame(int new_fsize);
	void DeInitFrame();
	int FillMsg(char *msg, int size); 
	char *GetFrame();
	int IsFull();
	int IsEmpty();
	void Empty();
	
	//control messages
};


#endif
