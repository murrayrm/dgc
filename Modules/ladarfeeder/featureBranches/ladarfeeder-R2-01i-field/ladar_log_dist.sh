#!/bin/bash

# Desc: Make a mini-distribution with the files needed to read ladar logs.
# Author: Andrew H
# Date: 14 March 2007

# This script expects to be run from the stereofeeder directory.

USAGE="USAGE: ladar_log_dist.sh <version>"
if [ $# -ne 1 ]; then
  echo $USAGE
  exit
fi

DST=ladar-log
VER=$1

README="Simple ladar log test program.\n"
README=$README"Build with : gcc -o ladar-log-test ladar_log_test.c sensnet_log.c\n"
README=$README"Run with   : ./ladar-log-test <LOGFILE>\n"

mkdir $DST

# Do some replacements on the test to make paths work out
sed -e "s|<interfaces/|<|" -e "s|<sensnet/|<|" ladar_log_test.c > $DST/ladar_log_test.c

# Copy additional files
cp ../sensnet/sensnet_log.[ch] $DST
cp ../interfaces/LadarRangeBlob.h $DST
cp ../interfaces/VehicleState.h $DST

# Add a README
echo -e $README > $DST/README

# Make a tarball
tar cvzf ladar-log-dist-$VER.tar.gz ladar-log
