
/* Desc: Convert ladar logs
 * Author: Andrew Howard
 * Date: 12 Mar 2005
 * CVS: $Id$
 *
 * Build with: gcc -o ladar-log-convert -I../../include ladar_log_convert.c -L../../lib/i486-gentoo-linux-static -lsensnetlog
 *
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <frames/mat44.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/LadarRangeBlob.h>


/// @brief Maximum image dimensions
#define LADAR_BLOB_V3_MAX_POINTS 181
  
/// @brief Ladar scan data.
///
typedef struct _LadarRangeBlobV3
{  
  /// Blob type (must be SENSNET_LADAR_BLOB)
  int blob_type;
  
  /// Version number (must be LADAR_BLOB_VERSION)
  int32_t version;

  /// Sensor ID for originating sensor
  int sensor_id;

  /// Scan id
  int scanid;

  /// Image timestamp
  uint64_t timestamp;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  VehicleState state;

  /// Sensor-to-vehicle transformation (homogeneous matrix)
  float sens2veh[4][4];

  /// Vehicle-to-local transformation (homogeneous matrix)
  float veh2loc[4][4];
  
  /// Reserved for future use; must be all zero.
  uint32_t reserved[16];

  /// Number of points
  int num_points;
  
  /// Range data
  float points[LADAR_BLOB_V3_MAX_POINTS][2];
  
} __attribute__((packed)) LadarRangeBlobV3;


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Simple log self-test.
int main(int argc, char **argv)
{
  char *srcfilename, *dstfilename;
  sensnet_log_t *srclog, *dstlog;
  sensnet_log_header_t header;

  LadarRangeBlobV3 src;
  LadarRangeBlob dst;
    
  if (argc < 3)
  {
    printf("usage: %s <SRC> <DST>\n", argv[0]);
    return -1;
  }
  srcfilename = argv[1];
  dstfilename = argv[2];

  // Create source log
  srclog = sensnet_log_alloc();
  assert(srclog);
  MSG("opening source %s", srcfilename);
  if (sensnet_log_open_read(srclog, srcfilename, &header) != 0)
    return -1;

  // Create destination log
  dstlog = sensnet_log_alloc();
  assert(dstlog);
  MSG("opening source %s", dstfilename);
  if (sensnet_log_open_write(dstlog, dstfilename, &header) != 0)
    return -1;

  while (true)
  {
    uint64_t timestamp;
    int sensor_id, blob_type, blob_id, blob_len;

    // Read blob
    blob_len = sizeof(src);
    if (sensnet_log_read(srclog, &timestamp, &sensor_id,
                         &blob_type, &blob_id, &blob_len, &src) != 0)
      return -1;

    MSG("read blob %d %d bytes %d points", blob_id, blob_len, src.num_points);

    // Copy data
    dst.blobType = src.blob_type;
    dst.version = LADAR_BLOB_VERSION;
    dst.sensorId = src.sensor_id;
    dst.scanId = src.scanid;
    dst.timestamp = src.timestamp;
    dst.state = src.state;
    memcpy(dst.sens2veh, src.sens2veh, sizeof(dst.sens2veh));
    mat44f_inv(dst.veh2sens, dst.sens2veh);
    memcpy(dst.veh2loc, src.veh2loc, sizeof(dst.veh2loc));
    mat44f_inv(dst.loc2veh, dst.veh2loc);
    memset(dst.reserved, 0, sizeof(dst.reserved));
    dst.numPoints = src.num_points;
    memcpy(dst.points, src.points, sizeof(dst.points));

    // Write new blob
    blob_len = sizeof(dst);
    if (sensnet_log_write(dstlog, timestamp, sensor_id,
                          blob_type, blob_id, blob_len, &dst) != 0)
      return -1;
  }

  // Clean up
  sensnet_log_close(dstlog);
  sensnet_log_free(dstlog);
  sensnet_log_close(srclog);
  sensnet_log_free(srclog);
  
  return 0;
}

