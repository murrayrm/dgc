#include "riegl_driver.hh"

void dumpmsg(char *msg, int size ) { 
	int i;
	char c; 
	printf("(%d)", size);
	for ( i = 0 ; i < size; i++) {
		c = msg[i];
		printf("0x%hx", c); 
		if (i%2 == 0)
			printf("  ");
		if (i%16 == 0)
			printf("\n");
//		printf("0x%.2x %2d,",msg[i], msg[i]); 
	}
	printf("\n");
}

int hexget(char *msg_start, int size) {
	long long r_val = 0;
	int i;
		
	for (i = size-1; i>=0; i--){
		r_val = r_val*256 + (unsigned char)msg_start[i];
	}
	return r_val;
}
double hexdget(char *msg_start, int size) {
	unsigned int result; 
	float *dres;
	result = hexget(msg_start, size);
	dres = (float *)&result;

	return *dres;
	
}

//---------------------------------------------------------------------------
//
//
int CRieglLadar::_MakeSocket(int port, int &sock, fd_set &sock_fd) { 
	struct sockaddr_in localAddr, serverAddr;
	struct hostent *serverHostInfo;
	int listen_socket,tempInt; 

	if (port == 0) 
		return 0;

	serverHostInfo = gethostbyname(host);
	if (serverHostInfo == NULL) 
	    return 0;

	//close passed socket if opened
	_CloseSocket(sock);
  
	// Create socket
	serverAddr.sin_family = serverHostInfo->h_addrtype;
	memcpy((char *) &serverAddr.sin_addr.s_addr, serverHostInfo->h_addr_list[0], serverHostInfo->h_length);
	serverAddr.sin_port = htons(port);
	listen_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (listen_socket < 0) 
	    return 0;

	// Bind any port number
	localAddr.sin_family = AF_INET;
	localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	localAddr.sin_port = htons(0);
	memset(&(localAddr.sin_zero), '\0', 8);

	// connect to server
	tempInt = connect(listen_socket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));
	if (tempInt < 0) {
	    sock = 0;
	    return 0;
	}
	FD_ZERO(&sock_fd);  	 
	FD_SET(listen_socket, &sock_fd);
	sock = listen_socket; 
	return 1;
}

//---------------------------------------------------------------------------
//
//
void CRieglLadar::_CloseSocket(int &socket) { 
	if (socket!= 0)
		close(socket); 
	socket = 0; 
}

//---------------------------------------------------------------------------
//
//
int CRieglLadar::_SendDataMsg(char *msg, int size) {		
    return send(data_sock, msg, size, 0);
}

//---------------------------------------------------------------------------
//
//
int CRieglLadar::_SendCtrlMsg(char *msg, int size) {		
    return send(ctrl_sock, msg, size, 0);
}

//---------------------------------------------------------------------------
//
//
int CRieglLadar::_GetMsg(char *msg, int size,  int socket, fd_set sock_fdset) { 
	 int nbytes;
	 timeval tmp; 
	 
     tmp = tl; 
     if ((nbytes = select(socket +1, &sock_fdset, NULL, NULL, &tmp)) == -1)
			 return RIEGL_ERR_GEN;
	
	 if (nbytes == 0){
		 printf("Warning: Timeout Waithing for Message\n");
			 return RIEGL_ERR_TIMO;
	 }
	 
	 if ((nbytes = recv(socket, msg, size , 0)) == -1)
			return RIEGL_ERR_GEN;

	 if (nbytes ==0) 
	 		return RIEGL_ERR_RESET;
			
     return nbytes;
}


//---------------------------------------------------------------------------
//
//
int CRieglLadar::_GetDataMsg(char *msg, int size, int vfy) { 
	return _GetMsg(msg, size,  data_sock, data_fdset);
}


//---------------------------------------------------------------------------
//
//
int CRieglLadar::_GetCtrlMsg(char *msg, int size, int vfy, int ignm) {
	int nb, vres;
	 nb = _GetMsg(msg, size,  ctrl_sock, ctrl_fdset);

	if ((!vfy)||(nb <= 0))
		return nb; 

	vres = _VerifyResponse(msg, size);
	
	if ((!ignm)||(vres != RIEGL_RESP_MSG))
		return vres;
	
	printf("Watning: Ignoring Message Packet"); dumpmsg(msg,nb);
	return _GetCtrlMsg(msg,size,vfy, ignm); //if we get message and we have to ignore it, get next packet 
}


//---------------------------------------------------------------------------
//
//
int CRieglLadar::_VerifyResponse(char *msg,int size) {		
		if (msg == NULL) 
				return RIEGL_RESP_ERR;
		
		if (msg[0] == '?')
				return RIEGL_RESP_ERR;

		if (msg[0] == 'm')
				return RIEGL_RESP_MSG;
		return RIEGL_RESP_OK; 
}


//---------------------------------------------------------------------------
//
//
int CRieglLadar::_StartProgMode() {
		
		_SendCtrlMsg(RIEGL_CMD_PROG_ON, strlen(RIEGL_CMD_PROG_ON));
		if (! _GetCtrlMsg(reply_buf, sizeof(reply_buf),1))
				return 0;
		
		mode = RIEGL_PROG_MODE;
		DGCSetConditionTrue(m_bRieglHasMode, m_rieglHasModeCondition, m_rieglHasModeConditionMutex);
		printf("Started Programming mode...\n");
		return 1;
}

//---------------------------------------------------------------------------
//
//
int CRieglLadar::_StartScanMode() {
		_SendCtrlMsg(RIEGL_CMD_SCAN_ON, strlen(RIEGL_CMD_SCAN_ON));
		if (! _GetCtrlMsg(reply_buf, sizeof(reply_buf),1))
			return 0;
		
		if (! _GetCtrlMsg(reply_buf, sizeof(reply_buf)))//recieved mScanning, TODO:verify
			return 0;
		
	
		mode = RIEGL_SCAN_MODE;
		DGCSetConditionTrue(m_bRieglHasMode, m_rieglHasModeCondition, m_rieglHasModeConditionMutex);

		DGCgettime(start_time);	
		printf("Started Scanning mode..\n");
	 	_ResetDataSock();
		
		return 1;
}


//---------------------------------------------------------------------------
//
//
int CRieglLadar::_StartLaser() { 
	_SendCtrlMsg(RIEGL_CMD_LASER_ON, strlen(RIEGL_CMD_LASER_ON));
	if (! _GetCtrlMsg(reply_buf, sizeof(reply_buf),1))
		return 0;
	printf("Laser Started...\n");
	return 1;
}

//---------------------------------------------------------------------------
//
//
int CRieglLadar::_StopLaser() { 
	_SendCtrlMsg(RIEGL_CMD_LASER_ON, strlen(RIEGL_CMD_LASER_OFF));
	if (! _GetCtrlMsg(reply_buf, sizeof(reply_buf),1))
		return 0;
	
	printf("Laser Stopped...\n");
	return 1;
}

//---------------------------------------------------------------------------
//
//
int CRieglLadar::_ResetDataSock() {
	int nb; 
	
	_CloseSocket(data_sock);
	if (!_MakeSocket(RIEGL_DATA_PORT, data_sock, data_fdset))
		return 0;
	
	if  ((nb = _GetDataMsg(reply_buf, sizeof(reply_buf)-1)) > 0) {
		_ProcessHeader(reply_buf,nb);
	}
	else return 0;
	
	//recieve one full frame , otherwise something messess up 
	DGClockMutex(&frames_mux);
	while (IsEmpty()) {
		if  ((nb = _GetDataMsg(reply_buf, sizeof(reply_buf)-1)) <= 0) {
			DGCunlockMutex(&frames_mux);
			return 0;	
		}

		FillMsg(reply_buf, nb);
	}
	has_frame = true;
	DGCunlockMutex(&frames_mux);
	
	return 1;
	
}


//---------------------------------------------------------------------------
//
//
CRieglLadar::CRieglLadar() {
		SetMsgTimeOut(RIEGL_MSG_TIMEOUT);
		ctrl_sock = 0;
		data_sock = 0; 
		mode = RIEGL_NO_MODE;

		has_frame = false;
		
		pnt.timestamp = 0; 
		pnt.range =0; 
		pnt.mirror_angle = 0; 
		pnt.quality = 0;
	
		curr_time = 0; 
		start_time = 0; 
		last_scan_time = 0;
		DGCcreateMutex(&frames_mux);
    DGCcreateMutex(&cond_mux);

		DGCcreateMutex(&m_rieglHasModeConditionMutex);
		DGCcreateCondition(&m_rieglHasModeCondition);
		DGCcreateMutex(&m_isNotFullConditionMutex);
		DGCcreateCondition(&m_isNotFullCondition);

    DGCcreateMutex(&cond_mux);
 		DGCcreateCondition(&cond);

		m_bRieglHasMode = false;
		m_bIsNotFull    = true;

		//taken from CLadarBuffer Class
		frame_size = 0;
		c_frame = 0;
		c_frame_offset = 0;
		last_frame_sent = -1;		
		memset(frames, 0, LADAR_BUF_SIZE*sizeof(frames[0]));

}


//---------------------------------------------------------------------------
//
//
CRieglLadar::~CRieglLadar() {
		Disconnect();	
		DGCdeleteMutex(&frames_mux); 
		DGCdeleteMutex(&cond_mux); 
		DGCdeleteCondition(&cond);

		DGCdeleteMutex(&m_rieglHasModeConditionMutex);
		DGCdeleteCondition(&m_rieglHasModeCondition);
		DGCdeleteMutex(&m_isNotFullConditionMutex);
		DGCdeleteCondition(&m_isNotFullCondition);

		DeInitFrame();
}

//---------------------------------------------------------------------------
//
//
int CRieglLadar::Connect(char *new_host) {
	
	if (new_host !=NULL)
		strcpy(host,new_host);
	else
		strcpy(host,RIEGL_DEFAULT_IP);
		
	if (!_MakeSocket(RIEGL_CTRL_PORT, ctrl_sock, ctrl_fdset))
			return 0;

	if (!_MakeSocket(RIEGL_DATA_PORT, data_sock, data_fdset))
			return 0;
	
	return 1;	
}

//---------------------------------------------------------------------------
//
//
void CRieglLadar::Disconnect() {
	if (mode == RIEGL_NO_MODE)
		return; //not connected

	m_bRieglHasMode = false;
	mode = RIEGL_NO_MODE;

	_CloseSocket(data_sock);
	_CloseSocket(ctrl_sock);
	
	printf("Disconnected from riegl\n");
}


//---------------------------------------------------------------------------
//
//
void CRieglLadar::SetMsgTimeOut( int sec, int usec) { 
	tl.tv_sec = sec;
	tl.tv_usec = usec;
}

//---------------------------------------------------------------------------
//
//
void CRieglLadar::_ProcessHeader(char *msg, ulong size) {
	char *c_ptr = msg;
	
	memcpy(header_buf, msg, size);	//copy the header packet
	
	header.header_size = hexget(c_ptr,4); c_ptr+=4;
	header.dataset_size = hexget(c_ptr,2); c_ptr+=2;
	header.protocol_id = hexget(c_ptr,1); c_ptr++;
	header.header_id = hexget(c_ptr,1); c_ptr++;
	header.meas_offset = hexget(c_ptr,2); c_ptr+=2; 
	header.meas_size = hexget(c_ptr,2); c_ptr+=2; 
	header.meas_count = hexget(c_ptr,2); c_ptr+=2; 
	
	header.leadin_main = hexget(c_ptr,1); c_ptr++;
	header.leadin_sub = hexget(c_ptr,2); c_ptr+=2; 
	header.measid_main = hexget(c_ptr,1); c_ptr++;
	header.measid_sub = hexget(c_ptr,2); c_ptr+=2; 
	header.trailerid_main = hexget(c_ptr,1); c_ptr++;
	header.trailerid_sub = hexget(c_ptr,2); c_ptr+=2; 
	header.paramid_main = hexget(c_ptr,1); c_ptr++;
	header.paramid_sub = hexget(c_ptr,2); c_ptr+=2; 
	
	strcpy(header.serial_num, c_ptr); c_ptr+=strlen(header.serial_num)+1; 
	
	header.range_unit = (double)hexdget(c_ptr,4); c_ptr+=4;
	header.angle_unit = (double)hexdget(c_ptr,4); c_ptr+=4;
	header.timer_unit = (double)hexdget(c_ptr,4); c_ptr+=4;

	header.polar_angleid = hexget(c_ptr,1); c_ptr++;
	header.hw_res = hexget(c_ptr,1); c_ptr++;
	header.target_sel = hexget(c_ptr,1); c_ptr++;
	
	
	header.beam_aperture = hexget(c_ptr,2); c_ptr+=2; 
	header.beam_divergence = hexget(c_ptr,2); c_ptr+=2; 
	header.beam_focus = hexget(c_ptr,2); c_ptr+=2; 
	header.beam_sep_lenght = hexget(c_ptr,2); c_ptr+=2; 
	memcpy(header.FactoryAdjustmentData, c_ptr, header.header_size - (c_ptr-msg)); //copy the rest of the header in there
	
	ReportHeader();

	DGClockMutex(&frames_mux);
	DeInitFrame();
	InitFrame(header.dataset_size + sizeof(header.dataset_size));
	if (size > header.header_size)
		FillMsg(msg+header.header_size, size - header.header_size);
	DGCunlockMutex(&frames_mux);
}

//---------------------------------------------------------------------------
//
//
void CRieglLadar::ReportHeader() {
	printf("------------------------Riegl Data Header----------------------\n");
	printf("Serial Number     : %s\n", header.serial_num);
	printf("Header Size       : %lu\n", header.header_size);
	printf("Dataset(Line) Size: %d\n", header.dataset_size);
	printf("Measurement Size  : %d\n", header.meas_size);
	printf("Measurement Count : %d\n", header.meas_count);
	printf("Measurements      : %x\n", header.measid_sub);
	printf("Range Unit        : %f\n", header.range_unit);
	printf("Angle Unit        : %f\n", header.angle_unit);
	printf("Polar Angle ID    : %d\n", header.polar_angleid);
	printf("Timer Unit        : %f\n", header.timer_unit);
	printf("Target Selection  : %d\n", header.target_sel);
	printf("---------------------------------------------------------------\n");
}


#define RIEGL_PRE_PROG_CMD 	int cmode = mode;  	  \
				if ( mode == RIEGL_PLAYBACK_MODE) return 0; \
				if ((mode != RIEGL_PROG_MODE)&&(!_StartProgMode())) \
					return 0;

#define RIEGL_POST_PROG_CMD 	if ((cmode != mode)&&(!_StartScanMode())) \
					return 0;\
				return 1;


//---------------------------------------------------------------------------
//
//
int CRieglLadar::_SaveParamChanges() {
	RIEGL_PRE_PROG_CMD
		
	_SendCtrlMsg(RIEGL_CMD_SAVE_PARAMS, strlen(RIEGL_CMD_SAVE_PARAMS));
	if (! _GetCtrlMsg(reply_buf, sizeof(reply_buf),1))
		return 0;

	RIEGL_POST_PROG_CMD	
}


//---------------------------------------------------------------------------
//
//
int CRieglLadar::SetScanOpts(uchar opts) {
	RIEGL_PRE_PROG_CMD
	
	sprintf(reply_buf, RIEGL_CMD(F%d), opts);
			
	_SendCtrlMsg(reply_buf, strlen(reply_buf));
	if (! _GetCtrlMsg(reply_buf, sizeof(reply_buf),1))
		return 0;

	if (!_SaveParamChanges()) return 0;

	RIEGL_POST_PROG_CMD
}


//---------------------------------------------------------------------------
//
//
int CRieglLadar::StartScan() {
	RIEGL_PRE_PROG_CMD
		
	cmode=mode; // this is really to get rid of the nasty warning about cmode not being used
	if (!_StartLaser()) return 0;


	if (!_StartScanMode()) return 0;

	printf("Started scanning..\n");
	DGCstartMemberFunctionThread(this, &CRieglLadar::_ReceiveDataFrames); 
	return 1;
}

//---------------------------------------------------------------------------
//
//
int CRieglLadar::StopScan() {
	if (!_StartProgMode()) return 0; //prog mode puts the ladar to sleep

	return 1;
}

//---------------------------------------------------------------------------
//
//
void CRieglLadar::_GetFramePoint(char *msg) {
	char *c_ptr = msg;

	if (header.measid_sub & RIEGL_SCAN_RANGE) {pnt.range = hexget(c_ptr,3); c_ptr+=3;}
	if (header.measid_sub & RIEGL_SCAN_INTENSITY) {pnt.amplitude = hexget(c_ptr,1); c_ptr+=1;}
	if (header.measid_sub & RIEGL_SCAN_ANGLE) {pnt.mirror_angle = hexget(c_ptr,3); c_ptr+=3;}
	if (header.measid_sub & RIEGL_SCAN_QUALITY) {pnt.quality = hexget(c_ptr,1); c_ptr+=1;}
	if (header.measid_sub & RIEGL_SCAN_TIME) {pnt.timestamp = hexget(c_ptr,3); c_ptr+=3;}
	
}


//---------------------------------------------------------------------------
//
//
void CRieglLadar::_UpdateCurrTime(ulong tn) {
	ulong tmp; 
	//first scan point 
	if (curr_time == 0) {
		curr_time = start_time;
		last_scan_time = tn;
		return;
	}

	//take difference between consequitive scans and be careful about the overflow
	if (tn < last_scan_time)  {
		tmp = 0xFFFFFF - last_scan_time + tn;
	}
	else 
		tmp = tn - last_scan_time;
			
	curr_time += lround(((double)tmp*header.timer_unit) * 1000000ULL);
	last_scan_time = tn;
}

//---------------------------------------------------------------------------
//
//
void CRieglLadar::_ReceiveDataFrames() 
{
  char data_buf[RIEGL_MAX_CHAR];
  int nb;
  while (1) 
    {
      
      // wait until mode is set
      if (mode == RIEGL_NO_MODE)
	DGCWaitForConditionTrue(m_bRieglHasMode, m_rieglHasModeCondition, m_rieglHasModeConditionMutex);
      
      DGCWaitForConditionTrue(m_bIsNotFull, m_isNotFullCondition, m_isNotFullConditionMutex);
      
      if ( mode == RIEGL_SCAN_MODE)
	nb = _GetDataMsg(data_buf, sizeof(data_buf)-1); 

      DGClockMutex(&frames_mux);
      FillMsg(data_buf, nb);
      if (!IsEmpty())
	DGCSetConditionTrue(has_frame, cond, cond_mux);  
      m_bIsNotFull = !IsFull();
      
      DGCunlockMutex(&frames_mux);      
      
    }
 
  return;
}

int CRieglLadar::_ExtractFrame(char *c_frame, unsigned long long *times, double *ranges,  double *angles, uchar *amps, uchar *quals) {
	int i;
	long a_mult;	
	int bp;	
	
	if ( header.dataset_size != hexget(c_frame,2)) {
		printf("Error Syncing.. %xh != %xh\n", header.dataset_size, hexget(c_frame,2));
		return 0;
	}

	bp = 0;
	a_mult = lround((double)400.0/header.angle_unit/header.polar_angleid);
	for (i = 0 ; i < header.meas_count; i++) {
		_GetFramePoint(c_frame + i*header.meas_size+2);
		
		if (times != NULL) times[i-bp] = (unsigned long long)(pnt.timestamp*header.timer_unit);
		//update the scan time from the first successfully retrieved point
		if (i-bp == 0)	_UpdateCurrTime(pnt.timestamp);
			
		if (ranges != NULL) ranges[i-bp] =pnt.range*header.range_unit;
		if (angles != NULL) angles[i-bp] = (double)(pnt.mirror_angle % a_mult)*2.0*header.angle_unit*M_PI/180.0*0.9; 	
		if (amps != NULL) amps[i-bp] = pnt.amplitude;
		if (quals != NULL) quals[i-bp] = pnt.quality;
		if (ranges[i-bp] < 0.000001 ) bp++;//float epsilon compare
	}

	return header.meas_count -bp;
}

//---------------------------------------------------------------------------
//
//
int CRieglLadar::GetScanFrame(unsigned long long *times, double *ranges,  double *angles, uchar *amps, uchar *quals, unsigned long long *frame_time) {
	char *c_frame; 
	int is_full;
	int num_points;

	if (IsEmpty()) {
	        has_frame = false;
		DGCWaitForConditionTrue(has_frame, cond, cond_mux);  
	}

	DGClockMutex(&frames_mux);
  
	if (frame_time != NULL) *frame_time = 0; 
	
	is_full = IsFull();		
	if ((c_frame = GetFrame())==NULL) {
		DGCunlockMutex(&frames_mux);
		printf("Cant get frame\n");
		return 0;
	}

	if ((is_full)&&(!IsFull())) 	DGCSetConditionTrue(m_bIsNotFull, m_isNotFullCondition, m_isNotFullConditionMutex);

	//Done with mutexing , do work 	
	num_points = _ExtractFrame(c_frame, times, ranges, angles, amps, quals);


	DGCunlockMutex(&frames_mux);
	if (frame_time  != NULL) *frame_time= curr_time;
	return num_points;
}

void CRieglLadar::DeInitFrame() {
	int i;

	frame_size = 0;
	c_frame = 0;
	c_frame_offset = 0;
	last_frame_sent = -1;
	
	for ( i = 0 ; i < LADAR_BUF_SIZE ; i++) {
		if (frames[i] != NULL)
		{
			delete[] frames[i];
			frames[i] = NULL;
		}
	}
}

void CRieglLadar::InitFrame(int new_fsize) {
	int i;
	frame_size = new_fsize;
	c_frame = 0;
	c_frame_offset = 0; 
	last_frame_sent = -1;
	for ( i = 0 ; i < LADAR_BUF_SIZE ; i++) {
		frames[i] = new char[new_fsize];
	}
}

int CRieglLadar::_NextFrame() {
	if (IsFull()){
		printf("Cant add frame\n");
		return 0;
	}

	c_frame++;
	c_frame %= LADAR_BUF_SIZE;

	c_frame_offset = 0; 

	return 1;
}

int CRieglLadar::FillMsg(char *msg, int size) { 
	int msg_offset = 0;
	if (size <= 0)
		return 0;

	if (size <= (frame_size - c_frame_offset )) {
		if (IsFull())
		{
			printf("Cant add frame\n");
			return 0;
		}

		memcpy(frames[c_frame] + c_frame_offset, msg, size);
		c_frame_offset+=size;
		
		if (c_frame_offset == frame_size) 
		{
			_NextFrame();
			return LADAR_BUF_FILL_MSG;
		}
		return 1;
	}
	else {
		//fill the last part of the previous frame
		memcpy(frames[c_frame] + c_frame_offset, msg, frame_size - c_frame_offset);
		msg_offset = frame_size - c_frame_offset;
		//switch frames
		if (!_NextFrame())
			return 0; //cant switch frames 

		memcpy(frames[c_frame], msg + msg_offset, size - msg_offset);
		c_frame_offset+= (size - msg_offset);

		return LADAR_BUF_FILL_FRAME;
	}
		

	return 1;
}

char *CRieglLadar::GetFrame() {
	if (!IsEmpty()) {
		last_frame_sent++;
		last_frame_sent %= LADAR_BUF_SIZE;
		return frames[last_frame_sent];
	}

	fprintf(stderr, "CRieglLadar::GetFrame() called with an empty buffer. check your mutices %d %d %d\n", num_frames(), last_frame_sent, c_frame );
	return NULL;
}

int CRieglLadar::IsFull() { 
  //Assure that we have 1 empty frame always, because othrewise recieved message from riegl might 
  //not be successfully processed and this can result in loss of data due to lack of space(riegl packets are arbitrary size)

  //Why it's actually minus 2: numframes are now compuited from
  //c_frame and last_frame_sent. This causes a full buffer to look
  //exactly the same as an empty buffer. An extra padding frame takes
  //care of the problem
	return (num_frames() == LADAR_BUF_SIZE -2);
}

int CRieglLadar::IsEmpty() {
	return num_frames() == 0;
}

void CRieglLadar::Empty() {
	int c_fsize = frame_size;
	DeInitFrame();
	InitFrame(c_fsize);
}

