/* 
 * Desc: Simulated ladar driver
 * Date: 16 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <alice/AliceConstants.h>
#include <frames/quat.h>
#include <frames/mat44.h>
#include <rndf/RNDF.hh>

#include "SimLadar.hh"


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor.
SimLadar::SimLadar()
{
  this->numLines = 0;
  this->numObjects = 0;
  
  return;
}


// Destructor
SimLadar::~SimLadar()
{
  if (this->rndf)
    delete this->rndf;
  
  return;
}


// Set the sensor-to-vehicle transform.
int SimLadar::setSensToVeh(float sens2veh[4][4])
{
  mat44f_setf(this->sens2veh, sens2veh);
  mat44f_inv(this->veh2sens, sens2veh);
  
  return 0;
}


// Load a data from a KML file
int SimLadar::loadKml(const char *filename)
{
  const char *ch;
  double ax, ay, bx, by;
  Line *nline;
  xmlDocPtr doc;
  xmlNode *root, *folder, *place, *line, *coord;
   
  this->numLines = 0;

  // Load the file
  doc = xmlReadFile(filename, NULL, 0);
  if (doc == NULL)
    return ERROR("unable to open/parse %s", filename);

  // Get the root element node
  root = xmlDocGetRootElement(doc);

  // Get the document node
  for (root = root->children; root != NULL; root = root->next)
  {
    if (root->type == XML_ELEMENT_NODE &&
        strcmp((const char*) root->name, "Document") == 0)
      break;
  }
  assert(root);

  // Parse each folder node
  for (folder = root->children; folder != NULL; folder = folder->next)
  {
    if (folder->type != XML_ELEMENT_NODE)
      continue;
    if (strcmp((const char*) folder->name, "Folder") != 0)
      continue;

    // Parse each placemark
    for (place = folder->children; place != NULL; place = place->next)
    {
      if (place->type != XML_ELEMENT_NODE)
        continue;
      if (strcmp((const char*) place->name, "Placemark") != 0)
        continue;

      // Parse each line
      for (line = place->children; line != NULL; line = line->next)
      {
        if (line->type != XML_ELEMENT_NODE)
          continue;
        if (strcmp((const char*) line->name, "LineString") != 0)
          continue;

        // Parse each coordinate list
        for (coord = line->children; coord != NULL; coord = coord->next)
        {
          if (coord->type != XML_ELEMENT_NODE)
            continue;
          if (strcmp((const char*) coord->name, "coordinates") != 0)
            continue;
        
          // Parse out a set of lines.  HACK this is fragile.
          ch = (const char*) xmlNodeGetContent(coord);
          while (ch)
          {
            GisCoordUTM utm;
            GisCoordLatLon geo;

            // Read out two points
            if (sscanf(ch, "%lf,%lf,0 %lf,%lf,0", &ax, &ay, &bx, &by) < 4)
              break;

            //MSG("point %f,%f %f,%f", ax, ay, bx, by);

            // Convert to UTM
            geo.latitude = ay;
            geo.longitude = ax;
            gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);
            ax = utm.n;
            ay = utm.e;

            geo.latitude = by;
            geo.longitude = bx;
            gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);
            bx = utm.n;
            by = utm.e;

            MSG("point %f,%f %f,%f", ax, ay, bx, by);
            
            // Create line
            assert((size_t) this->numLines < sizeof(this->lines)/sizeof(this->lines[0]));
            nline = this->lines + this->numLines++;
            nline->ax = ax;
            nline->ay = ay;
            nline->bx = bx;
            nline->by = by;
            nline->len = sqrtf(pow(nline->bx - nline->ax, 2) + pow(nline->by - nline->ay, 2));

            // Advance one point in the coordinates list
            ch = strchr(ch + 1, ' ');
          }
        }
      }
    }
  }

  xmlFreeDoc(doc);
  
  return 0;
}


// Generate a ladar scan using the KML data
int SimLadar::getScanKML(pose3_t poseGlobal, int maxMeas, int *numMeas, SimLadarMeas *meas)
{
  int i;
  float a, b, d;
  float pt, pr;

  a = this->sens2veh[2][0];
  b = this->sens2veh[2][1];
  d = this->sens2veh[2][3] - VEHICLE_TIRE_RADIUS;
  
  // Compute intersection with nominal ground plane
  for (i = 0; i < 181; i++)
  {
    // Range and bearing of point on the ground (sensor frame)
    pt = (i - 90) * M_PI / 180;
    pr = -d / (a * cos(pt) + b * sin(pt));
    if (pr < 0 || pr > 80 || !finite(pr)) // MAGIC
      pr = 80;    
    meas[i].bearing = pt;
    meas[i].range = pr;
    meas[i].intensity = 0x80;
  }
  *numMeas = i;
  
  return 0;
}


// Load RNDF data (for placing obstacles on RNDF waypoints).
int SimLadar::loadRNDF(const char *filename)
{
  // Create RNDF object
  this->rndf = new std::RNDF();
  assert(this->rndf);

  // Load RNDF from file
  if (!this->rndf->loadFile(filename))
    return ERROR("unable to load %s", filename);

  std::Waypoint *wp;
  
  // Get the first waypoint to use as an offset
  wp = this->rndf->getWaypoint(1, 1, 1);
  assert(wp);
  this->globalOffset.x = wp->getNorthing();
  this->globalOffset.y = wp->getEasting();
  this->globalOffset.z = 0;
  
  return 0;
}


// Create a simulated car at the given waypoint
int SimLadar::addCar(int segmentID, int laneID, int waypointID, double fraction)
{
  std::Waypoint *wpA, *wpB;
  SimLadarObject *object;
  double dx, dy;
  pose3_t pose;

  // Look up the waypoint in the RNDF
  wpA = this->rndf->getWaypoint(segmentID, laneID, waypointID);
  if (!wpA)
    return ERROR("waypoint %d.%d.%d not found", segmentID, laneID, waypointID);

  // Look up the next waypoint in the RNDF
  wpB = this->rndf->getWaypoint(segmentID, laneID, waypointID + 1);
  if (!wpB)
    return ERROR("waypoint %d.%d.%d not found", segmentID, laneID, waypointID);

  dx = wpB->getNorthing() - wpA->getNorthing();
  dy = wpB->getEasting() - wpA->getEasting();

  // Create new car object
  if ((size_t) this->numObjects >= sizeof(this->objects)/sizeof(this->objects[0]))
    return ERROR("too many objects");
  object = this->objects + this->numObjects++;

  pose.pos.x = (wpA->getNorthing() - this->globalOffset.x) + dx * fraction;
  pose.pos.y = (wpA->getEasting() - this->globalOffset.y) + dy * fraction;
  pose.pos.z = 0 - this->globalOffset.z;
  pose.rot = quat_from_rpy(0, 0, atan2(dy, dx));
  pose3_to_mat44f(pose, object->globalFromObject);

  object->size_x = 4.0;
  object->size_y = 2.0;
  object->size_z = 1.0;
  
  return 0;
}

  
// Generate a ladar scan.
int SimLadar::getScan(pose3_t poseGlobal, int maxMeas, int *numMeas, SimLadarMeas *meas)
{
  int i, j;
  int bin;
  float px, py, pz;
  float ax, ay, az, bx, by, bz;
  float a, qx, qy, qr, qb;
  float vehFromGlobal[4][4];
  float sensFromGlobal[4][4];
  float m[4][4];
  SimLadarObject *object;

  // Offset the global pose to produce a more tractable value
  poseGlobal.pos.x -= this->globalOffset.x;
  poseGlobal.pos.y -= this->globalOffset.y;
  poseGlobal.pos.z -= this->globalOffset.z;
  
  // Transform from global to vehicle frame
  pose3_to_mat44f(pose3_inv(poseGlobal), vehFromGlobal);

  // Transform from global to sensor frame
  mat44f_mul(sensFromGlobal, this->veh2sens, vehFromGlobal);
  
  // Initialize scan measurements
  for (i = 0; i < maxMeas; i++)
  {
    meas[i].bearing = (i - maxMeas/2) * M_PI/180;
    meas[i].range = 81.92;  // MAGIC
  }
  *numMeas = maxMeas;

  for (i = 0; i < this->numObjects; i++)
  {
    object = this->objects + i;

    // Transform from object to sensor frame
    mat44f_mul(m, sensFromGlobal, object->globalFromObject);
             
    for (j = 0; j < 100; j++) // MAGIC
    {
      // Select the lower point on the object
      px = (float) rand() / RAND_MAX * object->size_x - object->size_x / 2;
      py = (float) rand() / RAND_MAX * object->size_y - object->size_y / 2;
      pz = poseGlobal.pos.z;
      
      // Transform to sensor frame
      ax = m[0][0] * px + m[0][1] * py + m[0][2] * pz + m[0][3];
      ay = m[1][0] * px + m[1][1] * py + m[1][2] * pz + m[1][3];
      az = m[2][0] * px + m[2][1] * py + m[2][2] * pz + m[2][3];

      // Select the upper point on the object
      px = (float) rand() / RAND_MAX * object->size_x - object->size_x / 2;
      py = (float) rand() / RAND_MAX * object->size_y - object->size_y / 2;
      pz = poseGlobal.pos.z - object->size_z;
      
      // Transform to sensor frame
      bx = m[0][0] * px + m[0][1] * py + m[0][2] * pz + m[0][3];
      by = m[1][0] * px + m[1][1] * py + m[1][2] * pz + m[1][3];
      bz = m[2][0] * px + m[2][1] * py + m[2][2] * pz + m[2][3];

      // The two points must be on either side of the scan if we expect
      // to get a return
      if (az * bz > 0)
        continue;

      // Compute the linear interpolation at z = 0 in the sensor frame.
      a = (0 - az) / (bz - az);
      qx = (1 - a) * ax + a * bx;
      qy = (1 - a) * ay + a * by;
      
      qr = sqrt(qx * qx + qy * qy);
      qb = atan2(qy, qx);

      // Assume standard SICK configuration
      bin = (int) (qb * 180/M_PI + maxMeas/2 + 0.5);
      if (bin < 0)
        continue;
      if (bin > maxMeas)
        continue;

      if (qr < meas[bin].range)
        meas[bin].range = qr;
    }    
  }
  
  return 0;
}
