/*
 Desc: Driver for the SICK laser
 Author: Andrew Howard, adapted from the Player SickLMS200 driver.
 Date: 9 Oct 2006
 CVS: $Id: sicklms200.cc,v 1.60 2006/10/05 20:05:03 gerkey Exp $
*/


#ifndef SICK_DRIVER_H
#define SICK_DRIVER_H

#if __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <linux/serial.h>


/// @brief Class data for SICK LMS series driver
typedef struct
{
  /// laser device file descriptor
  int laser_fd;           

  /// Number of times to try connecting
  int retry_limit;
  
  /// Scan resolution (
  int scan_res;

  /// Range resolution (1 = 1mm, 10 = 1cm, 100 = 10cm).
  int range_res;

  /// Turn intensity data on/off
  bool enable_retro;

  /// Is the laser upside-down? (if so, we'll reverse the ordering of the
  /// readings)
  bool enable_invert;

  /// The final connection rate that we settle on
  int connect_rate;

  /// Current rate
  int current_rate;  

  /// What for?
  struct serial_struct old_serial;
  
} sick_driver_t;


/// @brief Allocate driver object.
/// @returns Returns a newly allocated object.
sick_driver_t *sick_driver_alloc();

/// @brief Free driver object.
/// @param[in] self Context.
void sick_driver_free(sick_driver_t *self);

/// @brief Open comms and initialize laser.
/// @param[in] self Context.
/// @param[in] port Serial port device (e.g., "/dev/ttyS1").
/// TODO
/// @returns Returns non-zero on error.
int sick_driver_open(sick_driver_t *self, const char *port, 
                     int connect_rate, int transfer_rate,
                     int scan_res, int range_res);

/// @brief Close everything down.
/// @param[in] self Context.
/// @returns Returns non-zero on error.
int sick_driver_close(sick_driver_t *self);

/// @brief Read data from the laser.
///
/// Read a complete scan, including range and retro-reflector data.
///
/// @param[in] self Context.
/// @param[in] max_rays Length of range/retro arrays.
/// @param[out] num_rays Number of rays in scan.
/// @param[out] ranges Detected range data.
/// @param[out] ranges Detected retro-reflector data.
/// @returns Returns non-zero on error
int sick_driver_read(sick_driver_t *self,
                     int max_rays, int *num_rays, float *ranges,  int *retros);  

#if __cplusplus
}
#endif

#endif



