
/* 
 * Desc: Ladar feeder module using JPLV
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/LadarRangeBlob.h>
#include <cotk/cotk.h>

#include "sick_driver.h"
#include "riegl_driver.hh"
#include "cmdline.h"



/// @brief Ladar feeder class
class LadarFeeder
{
  public:   

  /// Default constructor
  LadarFeeder();

  /// Default destructor
  ~LadarFeeder();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  /// Parse the config file
  int parseConfigFile(const char *configPath);

  /// Initialize feeder for simulated capture
  int initSim(const char *configPath);
  
  /// Finalize feeder for simulated capture
  int finiSim();

  /// Capture a simulated scan 
  int captureSim();

  /// Initialize feeder for live capture
  int initLive(const char *configPath);
  
  /// Finalize feeder for live capture
  int finiLive();

  /// Capture a scan 
  int captureLive();

  /// Initialize sensnet
  int initSensnet(const char *configPath);

  /// Finalize sensnet
  int finiSensnet();
  
  /// Publish data over sensnet
  int writeSensnet();

  /// Process a scan
  int process();

  /// Get the predicted vehicle state
  int getState(uint64_t timestamp);

  public:

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, LadarFeeder *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, LadarFeeder *self, const char *token);
  
  /// Console button callback
  static int onUserLog(cotk_t *console, LadarFeeder *self, const char *token);

  public:

  // Program options
  gengetopt_args_info options;

  // Default configuration path
  char defaultConfigPath[256];

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;
  
  // What mode are we in?
  enum {modeLive, modeSim} mode;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Id of the ladar
  int ladarId;

  // Port we are talking to (this can be either a USB or an IP w/ port)
  const char *ladarPort;

  int sickPort;
  
  const char *sickIP;  

  // SICK driver
  sick_driver_t *sick;  

  // RIEGL driver
  CRieglLadar *riegl;

  // Sensor-to-vehicle transform
  float sens2veh[4][4];

  // Calibration stats (display only).
  // Sensor position (x,y,z) and rotation (roll,pitch,yaw).
  vec3_t sensPos, sensRot;
  
  // Log file name
  char logName[1024];
  
  // Is logging enabled?
  bool enableLog;

  // SensNet handle
  sensnet_t *sensnet;

  // SensNet log handle
  sensnet_log_t *sensnet_log;
  
  // Blob buffer
  LadarRangeBlob *blob;

  // Console text display
  cotk_t *console;

  // Current scan id
  int scanId;

  // Current scan time (microseconds)
  uint64_t scanTime;

  // Current vehicle state data
  VehicleState state;

  // Point data (bearing and range)
  int numPoints;
  float points[361][2];
  
  // Start time for computing stats
  uint64_t startTime;
  
  // Capture stats
  int capCount;
  uint64_t capTime;
  double capRate, capPeriod;

  // Logging stats
  int logCount, logSize;
};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
LadarFeeder::LadarFeeder()
{
  memset(this, 0, sizeof(*this));

  this->scanId = -1;

  return;
}


// Default destructor
LadarFeeder::~LadarFeeder()
{
  return;
}


// Parse the command line
int LadarFeeder::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
  
  // Fill out the default config path
  if (getenv("DGC_CONFIG_PATH"))
    snprintf(this->defaultConfigPath, sizeof(this->defaultConfigPath),
             "%s/ladarfeeder", getenv("DGC_CONFIG_PATH"));
  else
    return ERROR("unknown configuration path: please set DGC_CONFIG_PATH");    

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  return 0;
}


// Parse the config file
int LadarFeeder::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, sensnet_id_to_name(this->sensorId));
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  this->ladarPort = this->options.port_arg;
  
  // Parse transform
  float px, py, pz;
  float rx, ry, rz;
  if (sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz) < 3)
    return ERROR("syntax error in sensor pos argument");
  if (sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz) < 3)
    return ERROR("syntax error in sensor rot argument");
    
  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2veh);
  
  // Record euler angles for display only
  this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;

  return 0;
}


// Initialize feeder for simulated capture
int LadarFeeder::initSim(const char *configPath)
{
  //char filename[256];

  // Fill out the ladar id
  /* TODO
  this->ladarId = atoi(ladarId);
  if (this->ladarId <= 0)
    return ERROR("invalid ladar id: %s", ladarId);
  */
 
  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;

  this->ladarPort = "sim";

  this->mode = modeSim;
  
  return 0;
}


// Finalize feeder for simulated capture
int LadarFeeder::finiSim()
{    
  return 0;
}


// Capture a simulated scan 
int LadarFeeder::captureSim()
{
  uint64_t timestamp;
  int i;
  float a, b, d;
  float pt, pr;

  // Simulate 75Hz
  usleep(13000);
  
  timestamp = DGCgettime();

  // Get the current state data
  if (this->getState(timestamp) != 0)
  {
    MSG("unable to get state; using dummy values");
    memset(&this->state, 0, sizeof(this->state));
    this->state.timestamp = DGCgettime();
  }

  a = this->sens2veh[2][0];
  b = this->sens2veh[2][1];
  d = this->sens2veh[2][3] - VEHICLE_TIRE_RADIUS;
  
  // Compute intersection with nominal ground plane
  for (i = 0; i < 181; i++)
  {
    // Range and bearing of point on the ground (sensor frame)
    pt = (i - 90) * M_PI / 180;
    pr = -d / (a * cos(pt) + b * sin(pt));
    if (pr < 0 || pr > 80 || !finite(pr)) // MAGIC
      pr = 80;    
    this->points[i][0] = pt;
    this->points[i][1] = pr;
  }
  this->numPoints = i;
  
  this->scanId += 1;
  this->scanTime = timestamp;
  
  if (this->console)
  {
    cotk_printf(this->console, "%capid%", A_NORMAL, "%5d %9.3f",
                this->scanId, fmod((double) this->scanTime * 1e-6, 10000));
    if (this->state.timestamp > 0)
      cotk_printf(this->console, "%slat%", A_NORMAL,
                  "%+06dms", (int) (this->state.timestamp - this->scanTime) / 1000);
  }

  return 0;
}


// Initialize feeder for live capture
int LadarFeeder::initLive(const char *configPath)
{
  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;

  if (this->ladarPort == NULL && this->sensorId!=SENSNET_RIEGL)
    return ERROR("ladar port is not set");

  /* TODO
  // Fill out the ladar id
  this->ladarId = atoi(ladarId);
  if (this->ladarId <= 0)
  return ERROR("invalid ladar id: %s", this->ladarId);
  */

  if(this->sensorId==SENSNET_RIEGL)
    MSG("connecting to riegl at IP address %s\n", this->ladarPort);
  else
    MSG("connecting %s", this->ladarPort);

  // Initialize the sick ladar or Riegl
  if(this->sensorId==SENSNET_RIEGL)
  {
    this->riegl = new CRieglLadar();
    assert(this->riegl);
    this->riegl->Connect();
    this->riegl->StartScan();
  }
  else
  {
    this->sick = sick_driver_alloc();
    assert(this->sick);
      
    if(strncmp(this->ladarPort,"192",3)==0)
    {
      //need to do this because ladarPort is a const char
      char IPstr[30];
      strcpy(IPstr,this->ladarPort);

      char * p_token;
      p_token = strtok(IPstr,":");
      this->sickIP = p_token;

      p_token = strtok(NULL, " ");
      this->sickPort = atoi(p_token);
	  
      //Try connecting to the DeviceMaster500
      if(sick_driver_open_tcp(this->sick,this->sickIP,this->sickPort,100,10)!=0)
        return ERROR("unable to open socket connection.");	      	   	  
    }      
    else
    { 
      // Try connecting at 500000
      // each ladar has the EPROM permanently set to transfer data at 500K
      // so there's no need to try 9600
      if(sick_driver_open_serial(this->sick, this->ladarPort, 500000, 100, 10) != 0)
        return ERROR("unable to connect to ladar on this port: %s", this->ladarPort);
    }      
  }
    
  this->mode = modeLive;
  
  return 0;
}


// Finalize feeder for live capture
int LadarFeeder::finiLive()
{
  if(this->sensorId==SENSNET_RIEGL)
  {
    this->riegl->Disconnect();
  }
  else
  {
    sick_driver_close(this->sick);
    sick_driver_free(this->sick);
    this->sick = NULL;    
  } 
  return 0;
}


// Capture a scan 
int LadarFeeder::captureLive()
{
  if(this->sensorId==SENSNET_RIEGL)
  {
    //not too familiar with the Riegl resolution but the Riegl MAX_SCANS is set to be very large
    //but the output looks like only <200 pts are returned
    //TODO: investigate whether MAX_SCANS can be set to something smaller
    double ranges[201];
    unsigned long long times[201];
    double angles[201];
    uchar amps[201];
    uchar quals[201];
    unsigned long long fr_time;
      
    this->numPoints = this->riegl->GetScanFrame(times,ranges,angles,amps,quals, &fr_time);

    if(this->numPoints==0)
    {
      MSG("Can't get scan frame for riegl!\n");
      return 0;
    }
      
    // Unpack the data.
    // Assuming a sensor frame that is x-forward, y-left
    for (int i = 0; i < this->numPoints; i++)
    {
      this->points[i][0] = angles[i]-M_PI/2;
      this->points[i][1] = ranges[i];
    }

    this->scanId += 1;
    this->scanTime = DGCgettime();
      
    // Get the matching state data
    if (this->getState(this->scanTime) != 0)
      return MSG("unable to get state; ignoring scan");
  }
  else
  {
    int i, numRanges;
    float ranges[181];
      
    this->numPoints = 0;

    // Read data from sensor.  Note that this ignores errors on read
    // (e.g., CRC errors) to prevent the program from terminating.
    if (sick_driver_read(this->sick, 181, &numRanges, ranges, NULL) != 0)
      return 0;	      
      
    // Unpack the data.  Assumes standard format for SICK.
    for (i = 0; i < numRanges; i++)
    {
      this->points[i][0] = (i - numRanges/2) * M_PI/180;
      this->points[i][1] = ranges[i];
    }
    this->numPoints = i;
      
    this->scanId += 1;
    this->scanTime = DGCgettime();
      
    // Get the matching state data
    if (this->getState(this->scanTime) != 0)
      return MSG("unable to get state; ignoring scan");   
  }
  
  if (this->console)
  {
    cotk_printf(this->console, "%capid%", A_NORMAL, "%5d %9.3f",
                this->scanId, fmod((double) this->scanTime * 1e-6, 10000));
    if (this->state.timestamp > 0)
      cotk_printf(this->console, "%slat%", A_NORMAL,
                  "%+6dms", (int) (this->state.timestamp - this->scanTime) / 1000);
  }
  
  return 0;
}


// Initialize sensnet
int LadarFeeder::initSensnet(const char *configPath)
{    
  // Check that blob size is a multiple of [something].  This allows
  // for DMA transfers.
  if (sizeof(LadarRangeBlob) % 512 != 0)
    return ERROR("invalid blob size %d; needs padding of %d",
                 sizeof(LadarRangeBlob),
                 512 - sizeof(LadarRangeBlob) % 512);

  // Create page-aligned blob to enable DMA logging
  this->blob = (LadarRangeBlob*) valloc(sizeof(LadarRangeBlob));

  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, this->sensorId) != 0)
    return ERROR("unable to connect to sensnet");
    
  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");
  
  // Initialize logging
  if (this->options.enable_log_flag)
  {
    time_t t;
    char timestamp[64];
    char cmd[256];
    sensnet_log_header_t header;

    // Construct log name
    t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
    snprintf(this->logName, sizeof(this->logName), "%s/%s-%s",
             this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));

    MSG("opening log %s", this->logName);
        
    // Initialize sensnet logging
    this->sensnet_log = sensnet_log_alloc();
    assert(this->sensnet_log);
    memset(&header, 0, sizeof(header));
    if (sensnet_log_open_write(this->sensnet_log, this->logName, &header, true) != 0)
      return ERROR("unable to open log: %s", this->logName);

    // Copy configuration files
    snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
             configPath, sensnet_id_to_name(this->sensorId), this->logName);
    system(cmd);
  }

  return 0;
}


// Finalize sensnet
int LadarFeeder::finiSensnet()
{  
  // Clean up SensNet
  if (this->sensnet_log)
  {
    sensnet_log_close(this->sensnet_log);
    sensnet_log_free(this->sensnet_log);
    this->sensnet_log = NULL;
  }
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  free(this->blob);
  this->blob = NULL;
  
  return 0;
}


// Publish data
int LadarFeeder::writeSensnet()
{
  int i;
  pose3_t pose;
  LadarRangeBlob *blob;

  blob = this->blob;

  // Construct the blob header
  blob->blobType = SENSNET_LADAR_BLOB;
  blob->version = LADAR_BLOB_VERSION;
  blob->sensorId = this->sensorId;
  blob->scanId = this->scanId;
  blob->timestamp = this->scanTime;
  blob->state = this->state;

  // Sensor to vehicle transform
  memcpy(blob->sens2veh, this->sens2veh, sizeof(this->sens2veh));
  mat44f_inv(blob->veh2sens, blob->sens2veh);

  // Vehicle to local transform
  pose.pos = vec3_set(blob->state.localX,
                      blob->state.localY,
                      blob->state.localZ);
  pose.rot = quat_from_rpy(blob->state.localRoll,
                           blob->state.localPitch,
                           blob->state.localYaw);  
  pose3_to_mat44f(pose, blob->veh2loc);  
  mat44f_inv(blob->loc2veh, blob->veh2loc);
  
  // Reset reseved values
  memset(blob->reserved, 0, sizeof(blob->reserved));

  // Copy the scan data
  blob->numPoints = this->numPoints;
  for (i = 0; i < this->numPoints; i++)
  {
    assert(i < (int) (sizeof(blob->points) / sizeof(blob->points[0])));
    assert(i < (int) (sizeof(this->points) / sizeof(this->points[0])));
    blob->points[i][0] = this->points[i][0];
    blob->points[i][1] = this->points[i][1];
  }
  
  // Write blob
  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, this->sensorId, SENSNET_LADAR_BLOB,
                    this->scanId, sizeof(*blob), blob) != 0)
    return ERROR("unable to write blob");
  
  // Write to log
  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag))
  {
    if (sensnet_log_write(this->sensnet_log, DGCgettime(),
                          this->sensorId, SENSNET_LADAR_BLOB,
                          this->scanId, sizeof(*blob), blob) != 0)
      return ERROR("unable to write blob");
  }

  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag) && this->console)
  {    
    // Keep some stats on logging
    this->logCount += 1;
    this->logSize += sizeof(*blob);
    cotk_printf(this->console, "%log%", A_NORMAL, "%df %dMb",
                this->logCount, this->logSize / 1024 / 1024);
  }

  return 0;
}


// Get the predicted vehicle state
int LadarFeeder::getState(uint64_t timestamp)
{
  int blobId;
  
  // Default to all zeros in state
  memset(&this->state, 0, sizeof(this->state));
  
  // Get the current state value
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
                   &blobId, sizeof(this->state), &this->state) != 0)
    return ERROR("unable to read state data");
  if (blobId < 0)
    return ERROR("state is invalid");

  // TODO: do prediction

  // Update the display
  if (this->console)
  {
    cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
                fmod((double) this->state.timestamp * 1e-6, 10000));
    cotk_printf(this->console, "%spos%", A_NORMAL, "%+09.3f %+09.3f %+09.3f",
                this->state.localX, this->state.localY, this->state.localZ);
    cotk_printf(this->console, "%srot%", A_NORMAL, "%+06.1f %+06.1f %+06.1f",
                this->state.localRoll*180/M_PI,
                this->state.localPitch*180/M_PI,
                this->state.localYaw*180/M_PI);
  }
  
  return 0;
}


// Process a scan
int LadarFeeder::process()
{
  // Processing currently does nothing for a laser scan  
  return 0;
}


// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"LadarFeeder $Revision$                                                     \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"                                                                           \n"
"Capture                                State                               \n"
"Mode  : %mode%                         Time  : %stime%                     \n"
"Port  : %port%                         Pos   : %spos%                      \n"
"Scan  : %capid%                        Rot   : %srot%                      \n"
"Log   : %log%                          Latency: %slat%                     \n"
"      : %logname%                                                          \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%LOG%]                                                     \n";


// Initialize console display
int LadarFeeder::initConsole()
{
  char filename[1024];
    
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%LOG%", " LOG ", "Ll",
                   (cotk_callback_t) onUserLog, this);
    
  // Initialize the display
  snprintf(filename, sizeof(filename), "%s/%s.msg",
           this->options.log_path_arg, sensnet_id_to_name(this->sensorId));
  if (cotk_open(this->console, filename) != 0)
    return -1;
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, sensnet_id_to_name(this->sensorId));
  cotk_printf(this->console, "%port%", A_NORMAL, "%s", this->ladarPort);
    
  if (this->mode == modeSim)
    cotk_printf(this->console, "%mode%", A_NORMAL, "sim   ");
  else if (this->mode == modeLive)
    cotk_printf(this->console, "%mode%", A_NORMAL, "live  ");
    
  cotk_printf(this->console, "%logname%", A_NORMAL, this->logName);

  return 0;
}


// Finalize console display
int LadarFeeder::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int LadarFeeder::onUserQuit(cotk_t *console, LadarFeeder *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int LadarFeeder::onUserPause(cotk_t *console, LadarFeeder *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Handle user events; occurs in sparrow thread
int LadarFeeder::onUserLog(cotk_t *console, LadarFeeder *self, const char *token)
{
  assert(self);
  self->enableLog = !self->enableLog;
  MSG("log %s", (self->enableLog ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  int status;
  LadarFeeder *feeder;

  // Create feeder
  feeder = new LadarFeeder();
  assert(feeder);
 
  // Parse command line options
  if (feeder->parseCmdLine(argc, argv) != 0)
    return -1;

  if (feeder->options.sim_flag)
  {
    // Initialize for simulated capture
    if (feeder->initSim(feeder->defaultConfigPath) != 0)
      return -1;
  }
  else
  {
    // Initialize for live capture
    if (feeder->initLive(feeder->defaultConfigPath) != 0)
      return -1;
  }

  // Initialize sensnet
  if (feeder->initSensnet(feeder->defaultConfigPath) != 0)
    return -1;

  // Initialize console
  if (!feeder->options.disable_console_flag)
  {
    if (feeder->initConsole() != 0)
      return -1;
  }

  feeder->startTime = DGCgettime();
  
  // Start processing
  while (!feeder->quit)
  {
    // Capture incoming scan directly into the ladar buffers
    if (feeder->mode == LadarFeeder::modeSim)
    {
      if (feeder->captureSim() != 0)
        break;
    }
    else
    {
      if (feeder->captureLive() != 0)
        break;
    }

    // Compute some diagnostics
    feeder->capCount += 1;
    feeder->capTime = DGCgettime() - feeder->startTime;
    feeder->capRate = (float) feeder->capCount / feeder->capTime;
    feeder->capPeriod = 1000.0 / feeder->capRate;

    // Update the console
    if (feeder->console)
      cotk_update(feeder->console);
    
    // If paused, give up our time slice.
    if (feeder->pause)
    {
      usleep(0);
      continue;
    }
    
    // Process one scan
    status = feeder->process();
    if (status != 0)
      break;

    // Publish data
    if (feeder->writeSensnet() != 0)
      break;    
  }
  
  // Clean up
  feeder->finiConsole();
  feeder->finiSensnet();
  if (feeder->mode == LadarFeeder::modeSim)
    feeder->finiSim();
  else
    feeder->finiLive();
  cmdline_parser_free(&feeder->options);  
  delete feeder;

  MSG("program exited cleanly");
  
  return 0;
}
