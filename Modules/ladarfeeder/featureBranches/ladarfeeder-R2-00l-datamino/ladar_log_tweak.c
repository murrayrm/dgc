
/* Desc: Tweak ladar logs
 * Author: Andrew Howard
 * Date: 12 Mar 2005
 * CVS: $Id$
 *
 * Build with: gcc -o ladar-log-tweak -I../../include ladar_log_tweak.c -L../../lib/i486-gentoo-linux-static -lsensnetlog
 *
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <frames/mat44.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/LadarRangeBlob.h>


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Simple log self-test.
int main(int argc, char **argv)
{
  char *srcfilename, *dstfilename;
  sensnet_log_t *srclog, *dstlog;
  sensnet_log_header_t header;

  LadarRangeBlob blob;
    
  if (argc < 3)
  {
    printf("usage: %s <SRC> <DST>\n", argv[0]);
    return -1;
  }
  srcfilename = argv[1];
  dstfilename = argv[2];

  // Create source log
  srclog = sensnet_log_alloc();
  assert(srclog);
  MSG("opening source %s", srcfilename);
  if (sensnet_log_open_read(srclog, srcfilename, &header) != 0)
    return -1;

  // Create destination log
  dstlog = sensnet_log_alloc();
  assert(dstlog);
  MSG("opening source %s", dstfilename);
  if (sensnet_log_open_write(dstlog, dstfilename, &header, false) != 0)
    return -1;

  while (true)
  {
    uint64_t timestamp;
    int sensor_id, blob_type, blob_id, blob_size;

    // Read blob
    if (sensnet_log_peek(srclog, NULL, NULL, NULL, NULL, &blob_size) != 0)
      return -1;
    if (sensnet_log_read(srclog, NULL, &sensor_id,
                         &blob_type, &blob_id, sizeof(blob), &blob) != 0)
      return -1;

    MSG("read blob %d %d bytes %d points", blob_id, blob_size, blob.numPoints);

    // Write new blob with correct timestamp
    if (sensnet_log_write(dstlog, blob.timestamp, sensor_id,
                          blob_type, blob_id, blob_size, &blob) != 0)
      return -1;
  }

  // Clean up
  sensnet_log_close(dstlog);
  sensnet_log_free(dstlog);
  sensnet_log_close(srclog);
  sensnet_log_free(srclog);
  
  return 0;
}

