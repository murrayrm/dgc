#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <assert.h>
using namespace std;
#include "traj.hh"

#include <stdlib.h>

CTraj::CTraj(int order)
{
  m_trajHeader.m_order = order;
  m_trajHeader.m_numPoints = 0;

  m_pN = new double[TRAJ_MAX_LEN * m_trajHeader.m_order];
  m_pE = new double[TRAJ_MAX_LEN * m_trajHeader.m_order];
}

// Copy constructor
CTraj::CTraj(const CTraj & other)
{
  m_trajHeader.m_order = other.m_trajHeader.m_order;
  m_pN = new double[TRAJ_MAX_LEN * m_trajHeader.m_order];
  m_pE = new double[TRAJ_MAX_LEN * m_trajHeader.m_order];
  m_trajHeader.m_numPoints = 0;

  *this = other;
}

CTraj::CTraj(int order, istream& inputstream)
{
  m_trajHeader.m_order = order;
	load(inputstream);
}

CTraj::CTraj(int order, char* pFilename)
{
  m_trajHeader.m_order = order;
	ifstream inputstream(pFilename);
	load(inputstream);
}

void CTraj::load(istream& inputstream)
{
  string line;
  int d;
  
  m_trajHeader.m_numPoints = 0;
  
  // allocate memory for the data arrays
  m_pN = new double[TRAJ_MAX_LEN * m_trajHeader.m_order];
  m_pE = new double[TRAJ_MAX_LEN * m_trajHeader.m_order];
  
  
  if(!inputstream)
    {
      cerr << "CTraj couldn't read from a stream\n";
      return;
    }
  
  while(inputstream)
    {
      getline(inputstream, line, '\n');
      if(line.size() == 0)
	return;
      
      if(line[0] == '#' || line[0] == '%')
	continue;
      
      istringstream linestream(line);
      
      for(d=0; d<m_trajHeader.m_order; d++)
	linestream >> m_pN[INDEX(m_trajHeader.m_numPoints, d)];
      
      for(d=0; d<m_trajHeader.m_order; d++)
	linestream >> m_pE[INDEX(m_trajHeader.m_numPoints, d)];
      
      if(!linestream)
	return;
      
      m_trajHeader.m_numPoints++;
    }
}

CTraj &CTraj::operator=(const CTraj &other)
{
  if (&other != this) {       // avoid self-assignment
    // delete the contents of this if needed
    // copy all elements
    m_trajHeader.m_numPoints = other.m_trajHeader.m_numPoints;
    m_trajHeader.m_order = m_trajHeader.m_order;
    memcpy(m_pN, other.m_pN, sizeof(double)*m_trajHeader.m_order*TRAJ_MAX_LEN);
    memcpy(m_pE, other.m_pE, sizeof(double)*m_trajHeader.m_order*TRAJ_MAX_LEN);
  }
  
  return *this;
}

CTraj &CTraj::operator+=(const CTraj &other)
{
	if(other.m_trajHeader.m_order != m_trajHeader.m_order)
	{
		cerr << "CTraj::operator+=(): orders don't match" << endl;
		return *this;
	}
	if(other.m_trajHeader.m_numPoints + m_trajHeader.m_numPoints > TRAJ_MAX_LEN)
	{
		cerr << "CTraj::operator+=(): resulting traj would be too long" << endl;
		return *this;
	}

	for(int i=0; i<m_trajHeader.m_order; i++)
	{
		memcpy(m_pN+INDEX(m_trajHeader.m_numPoints,i), other.m_pN+INDEX(0,i), other.m_trajHeader.m_numPoints*sizeof(double));
		memcpy(m_pE+INDEX(m_trajHeader.m_numPoints,i), other.m_pE+INDEX(0,i), other.m_trajHeader.m_numPoints*sizeof(double));
	}
	m_trajHeader.m_numPoints += other.m_trajHeader.m_numPoints;

  return *this;
}


CTraj::~CTraj()
{
  delete [] m_pN;
  delete [] m_pE;
}

double CTraj::lastE()
{
  return m_pE[INDEX(m_trajHeader.m_numPoints-1, 0)];
}
double CTraj::lastN()
{
  return m_pN[INDEX(m_trajHeader.m_numPoints-1, 0)];
}

void CTraj::setSpeed(int index, double speed)
{
  double cur_speed = getSpeed(index);
  if (cur_speed == 0)
    {
      cerr << "CTraj::setSpeed called, but old speed was 0!  Ignoring..." << endl;
      return;
    }
  double scale = speed / cur_speed;
  NEcoord norm_vel = getNEcoord(index,1) / cur_speed;
  NEcoord accel = getNEcoord(index,2);
  NEcoord long_accel = norm_vel * (norm_vel * accel);
  NEcoord lat_accel = accel - long_accel;
  // scale lat_accel by the appropriate amount
  lat_accel *= (scale * scale);
  accel = lat_accel + long_accel; // the new total accel
  NEcoord new_vel = norm_vel * speed;
  m_pN[INDEX(index,1)] = new_vel.N;
  m_pE[INDEX(index,1)] = new_vel.E;
  m_pN[INDEX(index,2)] = accel.N;
  m_pE[INDEX(index,2)] = accel.E;
}

void CTraj::setLongAccel(int index, double long_accel_magnitude)
{
  double speed = getSpeed(index);
  if (speed != 0)
    {
      NEcoord norm_vel = getNEcoord(index, 1) / speed;
      NEcoord accel = getNEcoord(index, 2);
      NEcoord long_accel = norm_vel * (norm_vel * accel);
      NEcoord lat_accel = accel - long_accel;
      long_accel = norm_vel * long_accel_magnitude;
      accel = long_accel + lat_accel;
      m_pN[INDEX(index,2)] = accel.N;
      m_pE[INDEX(index,2)] = accel.E;
    }
}

void CTraj::setLatAccel(int index, double lat_accel_magnitude)
{
  double speed = getSpeed(index);
  if (speed != 0)
    {
      NEcoord norm_vel = getNEcoord(index, 1) / speed;
      NEcoord accel = getNEcoord(index, 2);
      NEcoord long_accel = norm_vel * (norm_vel * accel);
      NEcoord norm_vel_rot(-norm_vel.E, norm_vel.N); // rotated by 90 ccw
      NEcoord lat_accel = norm_vel_rot * lat_accel_magnitude;
      accel = long_accel + lat_accel;
      m_pN[INDEX(index,2)] = accel.N;
      m_pE[INDEX(index,2)] = accel.E;
    }
}

bool CTraj::inTraj(double northing, double easting)
{
  for(int i=0; i<m_trajHeader.m_numPoints; i++)
    {
      if( (northing - m_pN[INDEX(i, 0)])*(northing - m_pN[INDEX(i, 0)]) +
	  (easting  - m_pE[INDEX(i, 0)])*(easting  - m_pE[INDEX(i, 0)]) <
	  TRAJ_IN_PATH_RES * TRAJ_IN_PATH_RES)
	return true;
    }
  
  return false;
}

void CTraj::startDataInput(void)
{
  m_trajHeader.m_numPoints = 0;
}

void CTraj::inputNoDiffs(double northing, double easting)
{
#warning "maybe specify 'no data' for derivatives here"
  m_pN[INDEX(m_trajHeader.m_numPoints, 0)] = northing;
  m_pE[INDEX(m_trajHeader.m_numPoints, 0)] = easting;
  
  if(m_trajHeader.m_numPoints != TRAJ_MAX_LEN-1)
    m_trajHeader.m_numPoints++;
  else
    cerr << "CTraj: overflow\n";
}

void CTraj::inputWithDiffs(double* northing, double* easting)
{
  for(int i=0; i<m_trajHeader.m_order; i++)
  {
    m_pN[INDEX(m_trajHeader.m_numPoints, i)] = northing[i];
    m_pE[INDEX(m_trajHeader.m_numPoints, i)] = easting[i];
  }
  
  if(m_trajHeader.m_numPoints != TRAJ_MAX_LEN-1)
    m_trajHeader.m_numPoints++;
  else
    cerr << "CTraj: overflow\n";
}

void CTraj::setPoint(int index, double new_N, double new_Nd, double new_Ndd,
		     double new_E, double new_Ed, double new_Edd)
{
  if (m_trajHeader.m_order < 3)
    cerr << "CTraj::setPoint: Cant' use setPoint for traj's which are less than 3rd order!!!" << endl;
  if (index > TRAJ_MAX_LEN - 1)
    cerr << "CTraj::setPoint: traj overflow!" << endl;
  m_pN[INDEX(index, 0)] = new_N;
  m_pN[INDEX(index, 1)] = new_Nd;
  m_pN[INDEX(index, 2)] = new_Ndd;
  m_pE[INDEX(index, 0)] = new_E;
  m_pE[INDEX(index, 1)] = new_Ed;
  m_pE[INDEX(index, 2)] = new_Edd;
}

void CTraj::addPoint(double new_N, double new_Nd, double new_Ndd,
		     double new_E, double new_Ed, double new_Edd)
{
  int index = m_trajHeader.m_numPoints;
  if (m_trajHeader.m_order < 3)
    cerr << "CTraj::setPoint: Cant' use setPoint for traj's which are less than 3rd order!!!" << endl;
  if (index > TRAJ_MAX_LEN - 1)
    cerr << "CTraj::setPoint: traj overflow!" << endl;
  m_pN[INDEX(index, 0)] = new_N;
  m_pN[INDEX(index, 1)] = new_Nd;
  m_pN[INDEX(index, 2)] = new_Ndd;
  m_pE[INDEX(index, 0)] = new_E;
  m_pE[INDEX(index, 1)] = new_Ed;
  m_pE[INDEX(index, 2)] = new_Edd;
  m_trajHeader.m_numPoints++;
}


void CTraj::shiftNoDiffs(int ptsToToss)
{
	memmove((char*)m_pN, ((char*)m_pN) + ptsToToss*sizeof(m_pN[0]), (m_trajHeader.m_numPoints-ptsToToss)*sizeof(m_pN[0]));
	memmove((char*)m_pE, ((char*)m_pE) + ptsToToss*sizeof(m_pN[0]), (m_trajHeader.m_numPoints-ptsToToss)*sizeof(m_pN[0]));
	m_trajHeader.m_numPoints -= ptsToToss;
}

// This function cuts down the current trajectory to be of length desiredDist
// (if the traj is longer), or lengthens it to be dist long
void CTraj::truncate(double desiredDist)
{
  if(m_trajHeader.m_numPoints < 2)
    return;

  int i, j;
  double d = 0.0;
  double dN, dE;
  
  for(i=0; i<m_trajHeader.m_numPoints-1; i++)
	{
		dN = m_pN[INDEX(i+1, 0)] - m_pN[INDEX(i, 0)];
		dE = m_pE[INDEX(i+1, 0)] - m_pE[INDEX(i, 0)];
		d = sqrt( dN*dN + dE*dE );
		desiredDist -= d;
      
		// we exceeded than desiredDist
		if(desiredDist < 0.0)
		{
			// we want to truncate the last segment by -desiredDist long, but along
			// the same direction
			for(j=0; j<m_trajHeader.m_order; j++)
	    {
	      m_pN[INDEX(i+1, j)] = m_pN[INDEX(i, j)] + (m_pN[INDEX(i+1, j)] - m_pN[INDEX(i, j)]) * (d+desiredDist) / d;
	      m_pE[INDEX(i+1, j)] = m_pE[INDEX(i, j)] + (m_pE[INDEX(i+1, j)] - m_pE[INDEX(i, j)]) * (d+desiredDist) / d;
	    }
	  
			// Set new point count
			m_trajHeader.m_numPoints = i + 2;
	  
			return;
		}
	}
  
  // we get here if the total length was not enough. We want to grow the last
  // segment to meet the required length. desiredDist left, so the last segment
  // should be d+desiredDist long
  for(j=0; j<m_trajHeader.m_order; j++)
	{
		m_pN[INDEX(m_trajHeader.m_numPoints-1, j)] = m_pN[INDEX(m_trajHeader.m_numPoints-2, j)] + (m_pN[INDEX(m_trajHeader.m_numPoints-1, j)] - m_pN[INDEX(m_trajHeader.m_numPoints-2, j)]) * (d+desiredDist) / d;
		m_pE[INDEX(m_trajHeader.m_numPoints-1, j)] = m_pE[INDEX(m_trajHeader.m_numPoints-2, j)] + (m_pE[INDEX(m_trajHeader.m_numPoints-1, j)] - m_pE[INDEX(m_trajHeader.m_numPoints-2, j)]) * (d+desiredDist) / d;
	}
}

// This function returns a vector of length num of the orientations of the
// vehicle along this traj, dist meters ahead. output into thetavector. bias is
// subtracted from the result.
void CTraj::getThetaVector(double dist, int num, double* thetavector, double bias)
{
	double delta = dist / (double)(num - 1);

	int i;
	int pointIndex;
	double distInSegment;

	double distLeftInSegment;
	double n0, e0, n1, e1;
	double lenSegment;
	double distLeft;

	// this keeps track of overruns. overrunBias is always an integer multiple of 2pi
	double prevTheta = 0.0;
	double overrunBias = 0.0;

	pointIndex = 0;
	distInSegment = 0.0;

	n0 = m_pN[INDEX(pointIndex  , 0)];
	n1 = m_pN[INDEX(pointIndex+1, 0)];
	e0 = m_pE[INDEX(pointIndex  , 0)];
	e1 = m_pE[INDEX(pointIndex+1, 0)];
	for(i=0; i<num; i++)
	{
		// compute the angle and force it to be in [-pi..pi] + overrunBias. 
		double theta = atan2(e1-e0, n1-n0) - bias ;
		theta = atan2(sin(theta), cos(theta)) + overrunBias;

		// now check for and correct overruns
		if(fabs(theta-prevTheta) > M_PI)
		{
			if(theta-prevTheta > M_PI)
			{
				overrunBias -= 2.0*M_PI;
				theta -= 2.0*M_PI;
			}
			else
			{
				overrunBias += 2.0*M_PI;
				theta += 2.0*M_PI;
			}
		}
		prevTheta = theta;
		thetavector[i] = theta;

		distLeft = delta;

		while(distLeft > 0.0)
		{
			n0 = m_pN[INDEX(pointIndex  , 0)];
			n1 = m_pN[INDEX(pointIndex+1, 0)];
			e0 = m_pE[INDEX(pointIndex  , 0)];
			e1 = m_pE[INDEX(pointIndex+1, 0)];
			lenSegment = hypot(n1-n0, e1-e0);

			distLeftInSegment = lenSegment - distInSegment;
			if(distLeft < distLeftInSegment)
			{
				distInSegment += distLeft;
				break;
			}

			distLeft -= distLeftInSegment;

			// don't advance if we're at the end
			pointIndex = min(pointIndex+1, m_trajHeader.m_numPoints-1);
			distInSegment = 0.0;
		}
	}
}

// This function returns a vector of length num of the speeds of the
// vehicle along this traj, dist meters ahead. output into speedvector. bias is
// subtracted from the result.
void CTraj::getSpeedVector(double dist, int num, double* speedvector, double bias)
{
	double delta = dist / (double)(num - 1);

	int i;
	int pointIndex;
	double distInSegment;

	double distLeftInSegment;
	double v0, v1;
	double n0, e0, n1, e1;
	double lenSegment;
	double distLeft;

	pointIndex = 0;
	distInSegment = 0.0;

	n0 = m_pN[INDEX(pointIndex  , 0)];
	n1 = m_pN[INDEX(pointIndex+1, 0)];
	e0 = m_pE[INDEX(pointIndex  , 0)];
	e1 = m_pE[INDEX(pointIndex+1, 0)];
	lenSegment = hypot(n1-n0, e1-e0);
	v0 = hypot(m_pN[INDEX(pointIndex  , 1)], m_pE[INDEX(pointIndex  , 1)]);
	v1 = hypot(m_pN[INDEX(pointIndex+1, 1)], m_pE[INDEX(pointIndex+1, 1)]);
	for(i=0; i<num; i++)
	{
#ifdef RECIPROCAL_SPEED
		speedvector[i] = 1.0 / (
														v0*(1.0 - distInSegment/lenSegment) +
														v1*distInSegment / lenSegment
													 ) - 1.0/bias ;
#else
		speedvector[i] =
			v0*(1.0 - distInSegment/lenSegment) +
			v1*distInSegment / lenSegment
			- bias ;
#endif

		distLeft = delta;

		while(distLeft > 0.0)
		{
			n0 = m_pN[INDEX(pointIndex  , 0)];
			n1 = m_pN[INDEX(pointIndex+1, 0)];
			e0 = m_pE[INDEX(pointIndex  , 0)];
			e1 = m_pE[INDEX(pointIndex+1, 0)];
			v0 = hypot(m_pN[INDEX(pointIndex  , 1)], m_pE[INDEX(pointIndex  , 1)]);
			v1 = hypot(m_pN[INDEX(pointIndex+1, 1)], m_pE[INDEX(pointIndex+1, 1)]);
			lenSegment = hypot(n1-n0, e1-e0);

			distLeftInSegment = lenSegment - distInSegment;
			if(distLeft < distLeftInSegment)
			{
				distInSegment += distLeft;
				break;
			}

			distLeft -= distLeftInSegment;

			// don't advance if we're at the end
			pointIndex = min(pointIndex+1, m_trajHeader.m_numPoints-1);
			distInSegment = 0.0;
		}
	}
}

double CTraj::getLength(void)
{
	return dist(0, m_trajHeader.m_numPoints-1);
}

double CTraj::dist(int from, int to)
{
  double length = 0.0;

  // forwards
  if(to > from)
    {
      for(int i=from; i<to; i++)
	{
	  length += hypot(m_pN[INDEX(i+1, 0)] - m_pN[INDEX(i, 0)],
			  m_pE[INDEX(i+1, 0)] - m_pE[INDEX(i, 0)]);
	}
      return length;
    }

  // backwards
  for(int i=to; i<from; i++)
    {
      length -= hypot(m_pN[INDEX(i+1, 0)] - m_pN[INDEX(i, 0)],
		      m_pE[INDEX(i+1, 0)] - m_pE[INDEX(i, 0)]);
    }
  return length;
}

double CTraj::distToNext(int index)
{
  return hypot(m_pN[INDEX(index+1, 0)] - m_pN[INDEX(index, 0)],
	       m_pE[INDEX(index+1, 0)] - m_pE[INDEX(index, 0)]);
}

void CTraj::print(ostream& outstream, int numPoints)
{
  int i, d;
  
  if(numPoints == -1)
    numPoints = m_trajHeader.m_numPoints;
 
  // print a comment header at the top
  outstream << "# This is a trajectory file output by CTraj::print()." << endl;
  outstream << "# Column format is [n, nd, ndd, e, ed, edd], in SI units." << endl;

  outstream << setprecision(10);
  for(i=0; i<numPoints; i++)
    {
      for(d=0; d<m_trajHeader.m_order; d++)
	outstream << m_pN[INDEX(i, d)] << " ";
      for(d=0; d<m_trajHeader.m_order; d++)
	outstream << m_pE[INDEX(i, d)] << " ";
      outstream << endl;
    }
}

// This function prints the speed profile into the output stream specified. The
// main purpose of this function is to communicate with my (dima's) qt realtime
// plotting program. This program is a huge hack and is probably breakage-prone,
// so please tell me before changing this function
void CTraj::printSpeedProfile(ostream& outputstream)
{
  int i;

  outputstream << setprecision(5);
  for(i=0; i<m_trajHeader.m_numPoints; i+=20)
	{
		outputstream << hypot(m_pN[INDEX(i, 1)], m_pE[INDEX(i, 1)]) << '\n';
	}
	outputstream << endl;
}

//This funstion applies a speed limit to the path.
void CTraj::speed_adjust(double speedlimit,NEcoord* pState,double aspeed)
{
  const double DECEL_RATE=2;
  int nearestOnPathIndex, i;
  double nearestDistSq;
  double currentDistSq;
  //Compute the index of the point that is closest to where we are located.
  nearestDistSq = 1.0e20;
  for(i=0; i<m_trajHeader.m_numPoints; i++)
  {
    currentDistSq =
	    pow(pState->N - m_pN[INDEX(i, 0)], 2.0) +
	    pow(pState->E - m_pE[INDEX(i, 0)], 2.0);
      
    if(currentDistSq < nearestDistSq)
    {
	    nearestDistSq = currentDistSq;
	    nearestOnPathIndex = i;
    }
  }
  
  i=nearestOnPathIndex;
  double speed,t_aN,t_aE,n_aN,n_aE,distance;
  for(; i<m_trajHeader.m_numPoints; i++)
  {
    speed=
      sqrt(m_pN[INDEX(i,1)]*m_pN[INDEX(i,1)]+m_pE[INDEX(i,1)]*m_pE[INDEX(i,1)]);
    /*If the speed of the initial point is above the speed limit, then we
    need to decelerate to below the speed limit in a dynamically feasible
    way.*/
    if(aspeed>speedlimit)
    {
      //Check if the source traj is decelerating faster than our DECEL_RATE.
      if(aspeed<speed)
      {
        //Set the tangential acceleration to the decel rate.
        t_aN=-DECEL_RATE*m_pN[INDEX(i,1)]/speed;
        t_aE=-DECEL_RATE*m_pE[INDEX(i,1)]/speed;
        //Calculate acceleration that is normal to velocity.
        n_aN=
          -m_pE[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
          m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
        n_aE=
          m_pN[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
          m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
        //Reduce speed to match any deceleration from previous points.
        m_pN[INDEX(i,1)]=m_pN[INDEX(i,1)]*aspeed/speed;
        m_pE[INDEX(i,1)]=m_pE[INDEX(i,1)]*aspeed/speed;
        /*Normal acceleration is scaled down to match the new velocity and
        added to the tangential acceleration giving total acceleration*/
        m_pN[INDEX(i,2)]=t_aN+n_aN*aspeed*aspeed/(speed*speed);
        m_pE[INDEX(i,2)]=t_aE+n_aE*aspeed*aspeed/(speed*speed);
      }
      else aspeed=speed;
      //Calculate the new decelerated speed for the next point.
      if(i<m_trajHeader.m_numPoints-1)
      {
        distance=
          sqrt(pow(m_pN[INDEX(i+1,0)]-m_pN[INDEX(i,0)],2.0)+
		      pow(m_pE[INDEX(i+1,0)]-m_pE[INDEX(i,0)],2.0));
        aspeed=sqrt(max(aspeed*aspeed-2*DECEL_RATE*distance,0.));
      }
    }
    /*Once the speed is below the speed limit it is only necessary to make
    sure the path does not acellerate back above the speed limit.*/
    else if(speed>speedlimit)
    {
      //Calculate acceleration that is normal to velocity.
      n_aN=
        -m_pE[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
        m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
      n_aE=
        m_pN[INDEX(i,1)]*(m_pN[INDEX(i,1)]*m_pE[INDEX(i,2)]-
        m_pE[INDEX(i,1)]*m_pN[INDEX(i,2)])/(speed*speed);
      //The speed is reduced to the speed limit.
      m_pN[INDEX(i,1)]=m_pN[INDEX(i,1)]*speedlimit/speed;
      m_pE[INDEX(i,1)]=m_pE[INDEX(i,1)]*speedlimit/speed;
      /*Tangential acceleration is set to zero, and normal acceleration is
      scaled down to match the new velocity.*/
      m_pN[INDEX(i,2)]=n_aN*speedlimit*speedlimit/(speed*speed);
      m_pE[INDEX(i,2)]=n_aE*speedlimit*speedlimit/(speed*speed);
    }
    //Points that already have speed below the speed limit are not changed.
  }
}

// Returns the index of the closest point in this traj to the provided
// (N,E). Note: this function returns the closest (spatially) point. It doesn't
// look to see how close we actually are. So even if we're miles away from this
// traj or facing the wrong way, this function will still return just the
// spatially closest point.
int CTraj::getClosestPoint(double n, double e)
{
	int			i;

	// index of the point on the trajectory that's closest to the current system
	// point, and the squared distance from the system point to that point on the
	// trajectory
	int     nearestIndex = 0;
	double	nearestDistSq;
	double	currentDistSq;

	// Compute the nearest point on the trajectory to where we currently are
	nearestDistSq = 1.0e20;
	for(i=0; i<getNumPoints(); i++)
	{
		currentDistSq =
			pow(n - getNorthing(i),2.0) +
			pow(e	- getEasting(i) ,2.0);

		if(currentDistSq < nearestDistSq)
		{
			nearestDistSq = currentDistSq;
			nearestIndex = i;
		}
	}

	return nearestIndex;
}


// See header for documentation
int CTraj::getPointAhead(int currpoint, double dist)
{
  if( currpoint < 0 )
  {
    cerr << "CTraj::getPointAhead error: currpoint is negative" << endl;
    return 0;
  }
  if( currpoint > m_trajHeader.m_numPoints - 1 )
  {
    cerr << "CTraj::getPointAhead error: currpoint out of bounds" << endl;
    return m_trajHeader.m_numPoints-1;
  }
  
  // initialize the return index and distance remaining
  int retind = currpoint;
  double distrem = dist;

  if( dist >= 0 )
  {
     NEcoord here(getNorthing(currpoint), getEasting(currpoint));
     NEcoord there(getNorthing(currpoint+1), getEasting(currpoint+1));

    // While we haven't reached the desired lookahead distance and haven't 
    // exhausted the trajectory, step ahead by an index and reevaluate.
    while( distrem > 0 && retind <= getNumPoints()-2)
    {
      // subtract from the distance remaining
      distrem -= (here - there).norm();
      // step forward and reset the NEcoords to calculate distance
      retind++;
      here = there; 
      there.N = getNorthing(retind+1);
      there.E = getEasting(retind+1);
    }

    return (retind < m_trajHeader.m_numPoints) ? retind : -1 ;
  }
  else if (dist < 0)
  {
     NEcoord here(getNorthing(currpoint), getEasting(currpoint));
     NEcoord there(getNorthing(currpoint-1), getEasting(currpoint-1));

    // While we haven't reached the desired lookahead distance and haven't 
    // exhausted the trajectory, step ahead by an index and reevaluate.
    while( distrem < 0 && retind >= 1)
    {
      // subtract from the distance remaining
      distrem += (here - there).norm();
      // step forward and reset the NEcoords to calculate distance
      retind--;
      here = there; 
      there.N = getNorthing(retind-1);
      there.E = getEasting(retind-1);
    }

    return (retind >= 0) ? retind : -1 ;
  }  
}

void CTraj::prepend(CTraj* pFrom, int start, int end)
{
	int i;

	int numToPrepend = end - start + 1;
	int numToKeep = min(TRAJ_MAX_LEN - numToPrepend, m_trajHeader.m_numPoints);

	assert(numToKeep > 0);

	for(i = 0; i<m_trajHeader.m_order; i++)
	{
		memmove(&m_pN[INDEX(numToPrepend, i)], &m_pN[INDEX(0, i)], numToKeep * sizeof(double));
		memcpy(&m_pN[INDEX(0, i)],  pFrom->getNdiffarray(i)+start, numToPrepend * sizeof(double));

		memmove(&m_pE[INDEX(numToPrepend, i)], &m_pE[INDEX(0, i)], numToKeep * sizeof(double));
		memcpy(&m_pE[INDEX(0, i)],  pFrom->getEdiffarray(i)+start, numToPrepend * sizeof(double));
	}

	m_trajHeader.m_numPoints = numToKeep + numToPrepend;
}


void CTraj::append(CTraj* pFrom, int start, int end)
{
	int i;

	int numToAppend = min(TRAJ_MAX_LEN - m_trajHeader.m_numPoints, (end-start+1));

	for(i = 0; i<m_trajHeader.m_order; i++)
	{
		memcpy(&m_pN[INDEX(m_trajHeader.m_numPoints, i)], pFrom->getNdiffarray(i)+start, numToAppend * sizeof(double));
		memcpy(&m_pE[INDEX(m_trajHeader.m_numPoints, i)], pFrom->getEdiffarray(i)+start, numToAppend * sizeof(double));
	}

	m_trajHeader.m_numPoints += numToAppend;
}

TrajPoint CTraj::interpolate( NEcoord curPos )
{
  double nc,ec,nc1,ec1, nc2,ec2,dbclpo_cp, dtjnp, dtjpb,vp1,vp2,vp3,projA,projB,Nd,Ed,Ndd,Edd,A,B;
  
  int closestIndex = getClosestPoint(curPos.N, curPos.E);
 
  if (getNumPoints()==1) {  // only one point ??
    return TrajPoint(curPos.N, curPos.E, getNorthingDiff(0 ,1) , getEastingDiff(0,1) ,getNorthingDiff(0,2) , getEastingDiff(0,2));
  }


  if(closestIndex==0) {   //Closest point is the first point
    nc = getNorthing(closestIndex);
    //east cordniate closeset indes
    ec = getEasting(closestIndex);
    // north cordinate on curpos+1
    nc1 = getNorthing(closestIndex+1);
    // east cordinate on curpos+1
    ec1 = getEasting(closestIndex+1);
    
    vp2=(( ec1)- (ec))*( curPos.E-(ec))+((nc1)- (nc))*( curPos.N- (nc));
    if (vp2<=0){   // we are before the first point
      return TrajPoint(curPos.N, curPos.E, getNorthingDiff(closestIndex,1) , getEastingDiff(closestIndex,1) ,getNorthingDiff(closestIndex,2) , getEastingDiff(closestIndex,2));
    }
    else {         // we are between first and second point
      A=sqrt(pow(curPos.N - (nc),2.0) + pow(curPos.E - (ec),2.0));
      B = sqrt(pow(curPos.N - (nc1),2.0) + pow(curPos.E - (ec1),2.0));
      Nd= (A/(A+B))*getNorthingDiff(closestIndex,1) + (B/(A+B))*getNorthingDiff(closestIndex+1,1);
      Ed = (A/(A+B))*getEastingDiff(closestIndex,1) + (B/(A+B))*getEastingDiff(closestIndex+1,1);
      Ndd= (A/(A+B))*getNorthingDiff(closestIndex,2) + (B/(A+B))*getNorthingDiff(closestIndex+1,2);
      Edd = (A/(A+B))*getEastingDiff(closestIndex,2) + (B/(A+B))*getEastingDiff(closestIndex+1,2);
      return TrajPoint(curPos.N, curPos.E, Nd, Ed, Ndd, Edd);
    }
    
  }
  else if( closestIndex==getNumPoints()-1) {   // closestPoint is the lastPoint
    nc = getNorthing(closestIndex);
    //east cordniate closeset indes
    ec = getEasting(closestIndex);
    //north cordinate on curpos-1
    nc2 = getNorthing(closestIndex-1);
    //esat cordinate on curpos-1
    ec2 = getEasting(closestIndex-1);
    vp1= (( ec2)- (ec))*( curPos.E-(ec))+((nc2)- (nc))*( curPos.N- (nc));
    if (vp1<=0){    // we are after last point
      return TrajPoint(curPos.N, curPos.E, getNorthingDiff(closestIndex,1) , getEastingDiff(closestIndex,1) ,getNorthingDiff(closestIndex,2) , getEastingDiff(closestIndex,2));
    }
    else {          //we are between last and second last point
      A=sqrt(pow(curPos.N - (nc),2.0) + pow(curPos.E - (ec),2.0));
      B = sqrt(pow(curPos.N - (nc2),2.0) + pow(curPos.E - (ec2),2.0));
      Nd= (A/(A+B))*getNorthingDiff(closestIndex,1) + (B/(A+B))*getNorthingDiff(closestIndex-1,1);
      Ed = (A/(A+B))*getEastingDiff(closestIndex,1) + (B/(A+B))*getEastingDiff(closestIndex-1,1);
      Ndd= (A/(A+B))*getNorthingDiff(closestIndex,2) + (B/(A+B))*getNorthingDiff(closestIndex-1,2);
      Edd = (A/(A+B))*getEastingDiff(closestIndex,2) + (B/(A+B))*getEastingDiff(closestIndex-1,2);
      return TrajPoint(curPos.N, curPos.E, Nd, Ed, Ndd, Edd);
    }
  }
  
  
  // general case
  
  
    // north cordinate closestIndex
  nc = getNorthing(closestIndex);
  //east cordniate closeset indes
  ec = getEasting(closestIndex);
  // north cordinate on curpos+1
  nc1 = getNorthing(closestIndex+1);
  // east cordinate on curpos+1
  ec1 = getEasting(closestIndex+1);
  
  
  //north cordinate on curpos-1
  nc2 = getNorthing(closestIndex-1);
  //esat cordinate on curpos-1
  ec2 = getEasting(closestIndex-1);
  
  //distance between closest point and Curpos  
  dbclpo_cp = sqrt(pow(curPos.N - (nc),2.0) + pow(curPos.E - (ec),2.0));
  //distance between trajpoint in the middle and the index after.  
  dtjnp =  sqrt(pow((nc1) - (nc),2.0) + pow((ec1) - (nc),2.0));
  // distance between trajpoint in the middle and the index before.  
  dtjpb =  sqrt(pow((nc2) - (nc),2.0) + pow((nc2) - (nc),2.0));
  //vektor produkt between AB and AC
  vp1= (( ec2)- (ec))*( curPos.E-(ec))+((nc2)- (nc))*( curPos.N- (nc));
  //vector product BC*BD 
  vp2=(( ec1)- (ec))*( curPos.E-(ec))+((nc1)- (nc))*( curPos.N- (nc));
  //BD*BA
  vp3 = (( ec)- (ec1))*( (ec)-(ec2))+(( nc)- (nc1))*( (nc)-(nc2));
  projA = sqrt(pow(dbclpo_cp,2.0)-pow(vp1/dtjnp,2.0));
  projB = sqrt(pow(dbclpo_cp,2.0)-pow(vp2/dtjpb,2.0));
  A = dbclpo_cp;
  if (projA <= projB && vp2 >=0) {  
    
    //Give the right value on B for this circumstances, the proj os on the nextP
    B = sqrt(pow(curPos.N - (nc1),2.0) + pow(curPos.E - (ec1),2.0));
    
    Nd= (A/(A+B))*getNorthingDiff(closestIndex,1) + (B/(A+B))*getNorthingDiff(closestIndex+1,1);
    Ed = (A/(A+B))*getEastingDiff(closestIndex,1) + (B/(A+B))*getEastingDiff(closestIndex+1,1);
    Ndd= (A/(A+B))*getNorthingDiff(closestIndex,2) + (B/(A+B))*getNorthingDiff(closestIndex+1,2);
    Edd = (A/(A+B))*getEastingDiff(closestIndex,2) + (B/(A+B))*getEastingDiff(closestIndex+1,2);
  }
  
  else if (projA >= projB && vp1 >=0) {
    
    //Give the right value for b for this circumstances, the proj is on the Pbefore
    
    B = sqrt(pow(curPos.N - (nc2),2.0) + pow(curPos.E - (ec2),2.0));
        Nd= (A/(A+B))*getNorthingDiff(closestIndex,1) + (B/(A+B))*getNorthingDiff(closestIndex-1,1);
    Ed = (A/(A+B))*getEastingDiff(closestIndex,1) + (B/(A+B))*getEastingDiff(closestIndex-1,1);
    Ndd= (A/(A+B))*getNorthingDiff(closestIndex,2) + (B/(A+B))*getNorthingDiff(closestIndex-1,2);
    Edd = (A/(A+B))*getEastingDiff(closestIndex,2) + (B/(A+B))*getEastingDiff(closestIndex-1,2);
  }
  
  else {
      // return curpos om ej ngn projektion    
    
    Nd = getNorthingDiff(closestIndex,1);
    Ed = getEastingDiff(closestIndex,1);
    Ndd = getNorthingDiff(closestIndex,2);
    Edd = getEastingDiff(closestIndex,2);
    
  }
  return TrajPoint(curPos.N, curPos.E, Nd, Ed, Ndd, Edd);
  
}




  
TrajPoint::TrajPoint(double nt, double et, double ndt, double edt, double nddt, double eddt) {
  n=nt;
  e=et;
  nd=ndt;
  ed=edt;
  ndd=nddt;
  edd=eddt;
}

/** BEGIN: Alternative interpoilation functions*/
TrajPoint CTraj::interpolGetClosest(double n, double e, int* index, double* fractionToNext) {
  
  /* Take care of NULL pointers */
  int indexNULL;
  double fractionToNextNULL;
  if(index == NULL) {
    index = &indexNULL;
  }
  if(fractionToNext == NULL) {
    fractionToNext = &fractionToNextNULL;
  }

  /* (An,Ae) is the position of Alice */
  double An = n;
  double Ae = e;

  /* (Qn,Qe) is the position of the point closest to Alice (has index q) */
  int q = getClosestPoint(An,Ae);
  double Qn = m_pN[INDEX(q,0)];
  double Qe = m_pE[INDEX(q,0)];


  /*(Qvn,Qve) is the velocity vector associated with traj point Q */
  double Qvn = m_pN[INDEX(q,1)];
  double Qve = m_pE[INDEX(q,1)];  

  /* (Rn,Re) is the point after Q (has index r) */
  double Rn, Re;
  int r = q+1;

  /* (Pn,Pe) is the point before Q (has index r) */
  double Pn, Pe;
  int p = q-1;

  /* [QA, velocity vector of Q]. Scalar product used to chose between Q and P */ 
  if ((An-Qn)*Qvn + (Ae-Qe)*Qve > 0 ) {  
    if (q==getNumPoints()-1) { /* End of traj */
      *index = q;
      *fractionToNext = 0;
      return interpolGetPoint(q,*fractionToNext);      
    } else { /* Use R */
      Rn = m_pN[INDEX(r,0)];
      Re = m_pE[INDEX(r,0)];
      double absRQ = (Re-Qe)*(Re-Qe)+(Rn-Qn)*(Rn-Qn);
      if (absRQ == 0) { /* Avoid div by 0 */
        *fractionToNext = 0;
      } else {
        *fractionToNext = ((Ae-Qe)*(Re-Qe)+(An-Qn)*(Rn-Qn))/absRQ;
      }      
    
    if (*fractionToNext >= 1) { /* Projection out of bounds */
        *fractionToNext = 0;
        *index = r;
        return interpolGetPoint(r,*fractionToNext);
      } else if (*fractionToNext < 0) {
        *fractionToNext = 0;
        *index = q;
        return interpolGetPoint(q,*fractionToNext);
      } else { /* Projection in bounds */
        *index = q;
        return interpolGetPoint(q,*fractionToNext);
      }
    }
  } else { 
    if (q==0) { /* Beginning of traj */
      *index = q;
      *fractionToNext = 0;
      return interpolGetPoint(q,*fractionToNext);
    } else { /* Use P */
      Pn = m_pN[INDEX(p,0)];
      Pe = m_pE[INDEX(p,0)];
      double absQP = (Qn-Pn)*(Qn-Pn)+(Qe-Pe)*(Qe-Pe);
      if (absQP == 0) { /* Avoid div by 0 */
        *fractionToNext = 0;
      } else {
        *fractionToNext = 1 + ((An-Qn)*(Qn-Pn)+(Ae-Qe)*(Qe-Pe))/absQP;
      }
      if (*fractionToNext >= 1) { /* Projection out of bounds */
        *fractionToNext = 0;
        *index = q;
        return interpolGetPoint(q,*fractionToNext);
      } else if (*fractionToNext < 0) {
        *fractionToNext = 0;
        *index = p;
        return interpolGetPoint(p,*fractionToNext);
      } else { /* Projection in bounds */
        *index = p;
        return interpolGetPoint(p,*fractionToNext);
      }
    }
  }
}


TrajPoint CTraj::interpolGetPoint(int index, double fractionToNext) {

  /* fractionToNext has to be in interval [0,1[ */
  if (fractionToNext < 0 || fractionToNext >= 1) {
    cerr << "CTraj::interpolGetPoint error: fractionToNext not in [0,1[" << endl;
    fractionToNext = min(1.0, fractionToNext);
    fractionToNext = max(0.0, fractionToNext);
  }

  /* If fractionToNext == 1, choose the next point */
  if (fractionToNext == 1) {
    index++;
    fractionToNext = 0;
  }

  /* Make sure index is in the traj */
  if (index < 0 || index > getNumPoints()-1) {
    cerr << "CTraj::interpolGetPoint error: index out of bounds" << endl;
    index = min(getNumPoints()-1, index);
    index = max(0, index);
  }

  /* Make sure point is in the traj */
  if (index == getNumPoints()-1 && fractionToNext != 0) {
    cerr << "CTraj::interpolGetPoint error: point out of bounds" << endl;
    fractionToNext = 0;
  }
  
  /* Do the linear interpolation between two neighboring points of the traj */
  return TrajPoint(m_pN[INDEX(index,0)]*(1-fractionToNext) + m_pN[INDEX(index+1,0)]*fractionToNext,
                   m_pE[INDEX(index,0)]*(1-fractionToNext) + m_pE[INDEX(index+1,0)]*fractionToNext,
                   m_pN[INDEX(index,1)]*(1-fractionToNext) + m_pN[INDEX(index+1,1)]*fractionToNext,
                   m_pE[INDEX(index,1)]*(1-fractionToNext) + m_pE[INDEX(index+1,1)]*fractionToNext,
                   m_pN[INDEX(index,2)]*(1-fractionToNext) + m_pN[INDEX(index+1,2)]*fractionToNext,
                   m_pE[INDEX(index,2)]*(1-fractionToNext) + m_pE[INDEX(index+1,2)]*fractionToNext);
}


TrajPoint CTraj::interpolGetPointAhead(int index, double fractionToNext, double distance, int* indexAhead, double* fractionToNextAhead) {

  /* Take care of NULL pointers */
  int indexAheadNULL;
  double fractionToNextAheadNULL;
  if(indexAhead == NULL) {
    indexAhead = &indexAheadNULL;
  }
  if(fractionToNextAhead == NULL) {
    fractionToNextAhead = &fractionToNextAheadNULL;
  }

  /* fractionToNext has to be in interval [0,1[ */
  if (fractionToNext < 0 || fractionToNext >= 1) {
    cerr << "CTraj::interpolGetPointAhead error: fractionToNext not in [0,1[" << endl;
    fractionToNext = min(1.0, fractionToNext);
    fractionToNext = max(0.0, fractionToNext);
  }
 
  /* If fractionToNext == 1, choose the next point */
  if (fractionToNext == 1) {
    index++;
    fractionToNext = 0;
  }  
 
  /* interpolGetPointAhead only handles positive distances */
  if (distance < 0) {
    cerr << "CTraj::interpolGetPointAhead error: distance must be > 0" << endl;
    distance = 0;
  }

  /* Make sure index is in the traj */
  if (index < 0 || index > getNumPoints()-1) {
    cerr << "CTraj::interpolGetPointAhead error: index of start point out of bounds" << endl;  
    index = min(getNumPoints()-1, index);
    index = max(0, index);
  }
 
  /* Make sure index is in the traj */
  if (index == getNumPoints()-1) {
    if (fractionToNext > 0) {
      cerr << "CTraj::interpolGetPointAhead error: point out of bounds" << endl;
    }  
    *indexAhead = index;
    *fractionToNextAhead = 0;
    return interpolGetPoint(*indexAhead, *fractionToNextAhead);
  }
  
  /* Use distance from closest TrajPoint behind */
  distance += dist(index, index+1)*fractionToNext;

  /* Walk the traj until it ends, or until we get distance ahead */
  while (index < getNumPoints()-1 && distance > dist(index, index+1)) {
    distance -= dist(index, index+1);
    index++;
  }
  *indexAhead = index;
  
  /* Check if we end up beyond the last point of the traj */
  if (index == getNumPoints()-1) {
    if (distance > 0.0) {
      cerr << "CTraj::interpolGetPoitnAhead error: point beyond end of traj" << endl;
    }
    *fractionToNextAhead = 0;
    return interpolGetPoint(*indexAhead, *fractionToNextAhead); 
  }

  if (dist(index, index+1)>0) { /* Avoid div by 0 */
    *fractionToNextAhead = distance/dist(index, index+1);
  } else {
    *fractionToNextAhead = 0;
  }

  /* This is the return of the 'normal' case */
  return interpolGetPoint(*indexAhead, *fractionToNextAhead); 
}
/** END: Alternative interpolation functions */


void CTraj::feasiblize(double max_lat_accel, double max_long_accel, double max_long_decel,
		       double averaging_distance)
{
  /** Strategy:
   * 1.  Estimate curvature at every point.
   * 2.  Go through and speedcap stuff based on DF
   * 3a. Step through in forward direction, slowing points down if necessary.
   *     Assign longitudinal accels along the way.
   * 3b. Step through in backward direction, slowing points if necessary.
   *     (re)-Assign longitudinal accels along way.
   * 4.  Convert curvature estimate + speed to lat accel
   */
  
  if (m_trajHeader.m_numPoints < 2)
    {
      cerr << "What were you thinking, giving us a traj with only "
	   << m_trajHeader.m_numPoints << " point(s) in it!?!?" << endl;
      return;
    }

  /** New array to store curvature estimates in.
   * Positive curvature is to the right! */
  double* curv = new double[m_trajHeader.m_numPoints];


  /**********************************************
   *
   * 1.  Estimate curvature at every point.
   *
   **********************************************/
  bool done_with_this_point, back_enough, ahead_enough;
  double dist_back_so_far, dist_ahead_so_far, running_total, x, y;
  int cur_index_offset, measurements;
  NEcoord this_point;
  NEcoord other_point; /**< temporary point for calculations */
  NEcoord norm_vel;
  NEcoord x_proj; /**< projection of other point onto the 'x' direction,
		   * where x is tangent to the traj; */
  for (int i = 0; i < m_trajHeader.m_numPoints; i++)
    {
      this_point.setvals(m_pN[INDEX(i,0)], m_pE[INDEX(i,0)]);
      norm_vel.setvals(m_pN[INDEX(i,1)], m_pE[INDEX(i,1)]);
      if (norm_vel.norm() == 0)
	continue;
      norm_vel /= norm_vel.norm();
      done_with_this_point = false;
      back_enough = false;
      ahead_enough = false;
      dist_back_so_far = dist_ahead_so_far = 0;
      cur_index_offset = 1; /**< num of points before/after the current point
			     * we're using to find the curvature.  Starts at 1
			     * and ends when we've gone averaging_distance */
      measurements = 0;
      running_total = 0;
      /** estimate curvature of the i'th point in the traj */
      while (!back_enough || !ahead_enough)
	{
	  /** check if we've went past the beginning or end */
	  back_enough = (i - cur_index_offset < 0);
	  ahead_enough = (i + cur_index_offset >= m_trajHeader.m_numPoints);

	  /** check if we've went too far backwards or forwards (in distance) */
	  back_enough |= (dist_back_so_far > averaging_distance);
	  ahead_enough |= (dist_ahead_so_far > averaging_distance);

	  /** get the curvature estimate from this particular point (behind) */
	  if (!back_enough)
	    {
	      other_point.setvals(m_pN[INDEX(i-cur_index_offset,0)],
				  m_pE[INDEX(i-cur_index_offset,0)]);
	      other_point -= this_point; // make it a relative vector
	      x_proj = norm_vel * (other_point * norm_vel);
	      y = (NEDcoord(norm_vel) ^  NEDcoord((other_point - x_proj))).D;
	      x = x_proj.norm();
	      /** add to estimate for curvature */
	      if (x != 0)
		{
		  running_total += y / (x * x);
		  ++measurements;
		}
	      dist_back_so_far += other_point.norm();
	    }
	      
	  /** get the curvature estimate from this particular point (ahead) */
	  if (!ahead_enough)
	    {
	      other_point.setvals(m_pN[INDEX(i+cur_index_offset,0)],
				  m_pE[INDEX(i+cur_index_offset,0)]);
	      other_point -= this_point; // make it a relative vector
	      x_proj = norm_vel * (other_point * norm_vel);
	      y = (NEDcoord(norm_vel) ^  NEDcoord((other_point - x_proj))).D;
	      x = x_proj.norm();
	      /** add to estimate for curvature */
	      if (x != 0)
		{
		  running_total += y / (x * x);
		  ++measurements;
		}
	      dist_ahead_so_far += other_point.norm();
	    }
	  cur_index_offset++; // head on to the next point
	}

      /** assign the curvature for this point */
      if (measurements == 0)
	{
	  cerr << "Couldn't estimate curvature for point " << i << " of "
	       << m_trajHeader.m_numPoints << " in traj; setting to 0!";
	  curv[i] = 0;
	}
      else
	{
	  curv[i] = 2 * running_total / measurements;
	  //cout << "curvature estimated for point " << i << " with " << measurements
	  //     << " measurements, curv is " << curv[i] << endl;
	}
    }


  /**********************************************
   *
   * 2.  Go through and speedcap stuff based on DF
   *
   **********************************************/
  double cur_speed, max_speed;
  for (int i = 0; i < m_trajHeader.m_numPoints; i++)
    {
      cur_speed = getSpeed(i);
      if (curv[i] != 0) // else no max from here
	{
	  max_speed = sqrt(max_lat_accel / curv[i]);
	  //	  cout << "dfe max speed for point " << i << " is " << max_speed << endl;
	  if (cur_speed > max_speed)
	    setSpeed(i, max_speed);
	}
    }


  /**********************************************
   *
   * 3a. Step through in forward direction, slowing points down if necessary.
   *     This step enforces the max_long_accel constraint.
   *
   **********************************************/
  double distance;
  double last_speed;
  cur_speed = getSpeed(0);
  for (int i = 1; i < m_trajHeader.m_numPoints; i++)
    {
      distance = distToNext(i-1);
      last_speed = cur_speed;
      cur_speed = getSpeed(i);
      max_speed = last_speed + distance * max_long_accel;
      //      cout << "forward max speed for point " << i << " is " << max_speed 
      //	   << " (last speed is " << last_speed << ")" << endl;
      if (cur_speed > max_speed)
	{
	  setSpeed(i, max_speed);
	  cur_speed = max_speed;
	}

#ifdef UNUSED
	  /* 
	   * Computation removed (RMM, 15 Mar 07)
	   *
	   * This calculation was being overwritten and is now
	   * replaced with the vector calculation in step 4.
	   */
      if (distance == 0)
	cerr << "CTraj::feasiblize: traj point " << i << " is duplicate point!" << endl;
      else {
	    cout << "Setting accel: correct = " 
		 << (max_speed - last_speed) / (distance/cur_speed)
		 << ", incorrect = "
		 << (max_speed - last_speed) / (distance) << endl;
	setLongAccel(i, (cur_speed - last_speed) / (distance/cur_speed));
      }
#endif
    }


  /**********************************************
   *
   * 3b. Step through in backward direction, slowing points if necessary.
   *     This step enforces the max_long_decel constraint.
   *
   **********************************************/
  cur_speed = getSpeed(m_trajHeader.m_numPoints - 1);
  for (int i = m_trajHeader.m_numPoints - 2; i >= 0; i--)
    {
      distance = distToNext(i);
      last_speed = cur_speed;
      cur_speed = getSpeed(i);
      max_speed = last_speed + distance * max_long_decel;
      //      cout << "backward max speed for point " << i << " is " << max_speed 
      //	   << " (last speed is " << last_speed << ")" << endl;
      if (cur_speed > max_speed)
	{
	  setSpeed(i, max_speed);
#ifdef UNUSED
	  /* 
	   * Computation removed (RMM, 15 Mar 07)
	   *
	   * This calculation was being overwritten and is now
	   * replaced with the vector calculation in step 4.
	   */
	  if (distance == 0)
	    cerr << "CTraj::feasiblize: traj point " << i << " is duplicate point!" << endl;
	  else {
	    cout << "Resetting accel: correct = " 
		 << (max_speed - last_speed) / (distance/last_speed)
		 << ", incorrect = "
		 << (max_speed - last_speed) / (distance) << endl;
	    setLongAccel(i, -1.0 * (max_speed - last_speed) / (distance/last_speed));
	  }
#endif
	}
    }


  /**********************************************
   *
   * 4.  Compute the accelerations along the path
   *
   **********************************************/
  for (int i = 0; i < m_trajHeader.m_numPoints - 1; i++) {
    // setLatAccel(i, pow(getSpeed(i), 2) * curv[i]);

    /* Coarse approximation: delta between velocities */
    cur_speed = getSpeed(i);
    NEcoord cur_vel = getNEcoord(i, 1);
    NEcoord next_vel = getNEcoord(i+1, 1);
    distance = distToNext(i);

    NEcoord accel = (next_vel - cur_vel) / (distance / cur_speed);
    m_pN[INDEX(i, 2)] = accel.N;
    m_pE[INDEX(i, 2)] = accel.E;
  }  

  delete [] curv;
}



void CTraj::fillWithCubicSpline(double here_n, double here_n_dot,
				double here_e, double here_e_dot,
				double there_n, double there_n_dot,
				double there_e, double there_e_dot,
				double density)
{
  // erase current traj
  m_trajHeader.m_numPoints = 0;

  // see little bit of documentation in CPath.cc
  // in function splineFromHereToThere
  double cn_0, cn_1, cn_2, cn_3, ce_0, ce_1, ce_2, ce_3;

  cn_0 = here_n;
  cn_1 = here_n_dot;
  cn_2 = -3*here_n + -2*here_n_dot + 3*there_n + -1*there_n_dot;
  cn_3 = 2*here_n + 1*here_n_dot + -2*there_n + 1*there_n_dot;

  ce_0 = here_e;
  ce_1 = here_e_dot;
  ce_2 = -3*here_e + -2*here_e_dot + 3*there_e + -1*there_e_dot;
  ce_3 = 2*here_e + 1*here_e_dot + -2*there_e + 1*there_e_dot;

  double total_distance = hypot(there_n-here_n, there_e-here_e);
  unsigned int divisions = (int)(total_distance/density + 1.0); //+1 to round up
  double step_size = 1.0/divisions;

  double t, this_n, this_e;
  for (unsigned int i = 0; i < divisions; ++i) // never quite get to last point
    {
      t = i * step_size;
      this_n = cn_0 + cn_1*t + cn_2*t*t + cn_3*t*t*t;
      this_e = ce_0 + ce_1*t + ce_2*t*t + ce_3*t*t*t;
      setPoint(i, this_n, 1234.0, 1234.0, this_e, 1234.0, 1234.0);
      m_trajHeader.m_numPoints++;
    }
}
