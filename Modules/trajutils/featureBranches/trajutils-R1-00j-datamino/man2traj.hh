/*!
 * \file man2traj.hh
 * \brief Header file for man2traj function
 *
 * \author Richard Murray
 * \date 1 may 2005
 *
 */

#include "maneuver.h"
#include "traj.hh"

extern int maneuver_profile_generate(Vehicle *, int, Maneuver **, 
			      double *, int, CTraj *);
