/*!
 * \file TrajTalker.hh
 * \author Jason Yosinski?
 * \date 2004-05
 *
 * Header file for TrajTalker class
 *
 */

#ifndef _TRAJTALKER_H_
#define _TRAJTALKER_H_

#include <unistd.h>
#include <string>
#include <pthread.h>
using namespace std;

#include "skynet/SkynetContainer.hh"
#include "traj.hh"

class CTrajTalker : virtual public CSkynetContainer
{
	pthread_mutex_t m_dataBufferMutex;

	char* m_pDataBuffer;

public:
	CTrajTalker();
  CTrajTalker(int snname, int snkey, int* status=NULL);  
	~CTrajTalker();

	bool SendTraj(int trajSocket, CTraj* pTraj,
		      pthread_mutex_t* pMutex = NULL);
	bool RecvTraj(int trajSocket, CTraj* pTraj,
		      pthread_mutex_t* pMutex = NULL,
		      string* pOutstring = NULL);
	void WaitForTrajData(int trajSocket);
};

#endif // _TRAJTALKER_H_
