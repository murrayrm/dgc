% bezier.m - basis functions for planning
function val = basis(index, deriv, time)
switch index
  case 0,
    switch deriv
      case 0, val = time;
      case 1, val = ones(size(time));
      case 0, val = zeros(size(time));
    end

  case 1,
    switch deriv
      case 0, val = (time.^2);
      case 1, val =  2*time;
      case 2, val = ones(size(time));
    end

  case 2,
    switch deriv
    case 0, val = (time.^3);
    case 1, val =  3 * (time.^2);
    case 2, val = 6 * time;
    end

  case 3,
    switch deriv
    case 0, val = (1-time).^3;
    case 1, val =  -3 * (1-time).^2;
    case 2, val = 6 * (1-time);
    end

  case 4,
    switch deriv
    case 0, val = (1-time).^2;
    case 1, val =  -2 * (1-time);
    case 2, val = 2 * ones(size(time));
    end

  case 5,
    switch deriv
    case 0, val = (1-time);
    case 1, val =  -ones(size(time));
    case 2, val = zeros(size(time));
    end

  case 6,
    switch deriv
    case 0, val = 3 * time .* (1-time).^2;
    case 1, val =  3 * (1-time).^2 - 6 * time .* (1-time);
    case 2, val = -6 * (1-time) - 6 * (1-time) + 6 * time;
    end

  case 7,
    switch deriv
    case 0, val = 3 * (time.^2) .* (1-time);
    case 1, val =  6 * time .* (1-time) - 3 * (time.^2);
    case 2, val = 6 * (1-time) - 6 * time - 6 * time;
    end
end
