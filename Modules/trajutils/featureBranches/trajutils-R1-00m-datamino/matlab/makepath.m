% makepath.m - make a path given a set of coefficients
function path = makepath(alpha, time)
global nBasisFunctions;

path(1,:) = zeros(size(time));
path(2,:) = zeros(size(time));

for index=1:nBasisFunctions
  path(1,:) = path(1,:) + alpha(index) * basis(index-1, 0, time);
  path(2,:) = path(2,:) + ...
    alpha(index+nBasisFunctions) * basis(index-1, 0, time);
end
