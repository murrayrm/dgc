/*! 
 * \file UT_man2traj.c c
 * \brief Unit test for manuever to trajectory conversion
 *
 * \author Richard Murray
 * \date 4 May 2007
 *
 * This program does some basic checks on the manuever generation
 * library to make sure that things are working properly.  
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#include "maneuver.h"
#include "man2traj.hh"
#include "traj.hh"
#include "UT_cmdline.h"

/* Global variables used in various tests */
static Vehicle *vp;
static const double epsilon = 1e-5;	    /* accuracy for checking config */

int main(int argc, char **argv)
{
  int errcount = 0;

  /* Let everyone know we are here */
  printf("UT_maneuver unit test\n");

  // Parse command line options
  struct gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  /* Set level of verbosity */
  maneuver_verbose = cmdline.verbose_arg;

  /* Initialize the vehicle configuration */
  vp = maneuver_create_vehicle(3.0, 1.0);
  assert(vp != NULL);

  /* Test #1: Generate a sample trajectory between config and pose */
  VehicleConfiguration config1 = {1, 2, 1, 0.5};
  Pose2D pose2 = {10, 10, 2};

  /* Generate the the maneuver */
  Maneuver *man = maneuver_config2pose(vp, &config1, &pose2);
  assert(man != NULL);

  /* Convert this to a trajectory */
  double *vlist = (double *) calloc(2, sizeof(double)); 
  CTraj *ptraj = new CTraj(3);

  vlist[0] = 1; vlist[1] = 2;
  int status = maneuver_profile_generate(vp, 1, &man, vlist, 10, ptraj);

  /* Check that the trajectory matches the initial configuration */
  VehicleConfiguration config;
  NEcoord pos = ptraj->getNEcoord(0, 0);
  NEcoord vel = ptraj->getNEcoord(0, 1);
  NEcoord acc = ptraj->getNEcoord(0, 2);

  config.x = pos.N; config.y = pos.E;
  config.theta = atan2(vel.E, vel.N);
  config.phi = atan((acc.E * cos(config.theta) - acc.N * sin(config.theta)) * 
		    vp->wheelbase);

  if ((fabs(config.x - config1.x) > epsilon) ||
      (fabs(config.y - config1.y) > epsilon) ||
      (fabs(config.theta - config1.theta) > epsilon) ||
      (fabs(config.phi - config1.phi) > epsilon)) {
    fprintf(stderr, 
	    "Config error (%g, %g, %g, %g); initial = (%g, %g, %g, %g)\n", 
	    config1.x, config1.y, config1.theta, config1.phi, 
	    config.x, config.y, config.theta, config.phi);
    ++errcount;
  }

#ifdef UNUSED
  /* Check that the trajectory matches the final ppose */
# warning Traj doesn't include end point yet
  Pose2D pose;
  pos = ptraj->getNEcoord(9, 0);
  vel = ptraj->getNEcoord(9, 1);
  acc = ptraj->getNEcoord(9, 2);

  pose.x = pos.N; pose.y = pos.E;
  pose.theta = atan2(vel.E, vel.N);

  if ((fabs(pose.x - pose2.x) > epsilon) ||
      (fabs(pose.y - pose2.y) > epsilon) ||
      (fabs(pose.theta - pose2.theta) > epsilon)) {
    fprintf(stderr, "Pose error at (%g, %g, %g); got (%g, %g, %g)\n",
	    pose2.x, pose2.y, pose2.theta, pose.x, pose.y, pose.theta);
    ++errcount;
  }
#endif

  printf("Errors: %d\n", errcount);
  return errcount;
}

