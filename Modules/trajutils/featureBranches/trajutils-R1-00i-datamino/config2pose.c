/*!
 * \file config2pose.c
 * \brief manuever generation for going from a configuration to a pose
 *
 * \author Richard Murray
 * \date 30 April 2007
 *
 * This file has some additional routines for the maneuver generation
 * library.  These are in a separate file so that you don't
 * automatically link them (not everyone will need them).
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "maneuver.h"

/* Some constants that control our operations */
extern const int nBasisFunctions;	// number of basis functions
extern const int nFlatOutputs;	// number of flat outputs that we use
extern const int nFlatDerivs;	// number of derivatives of flat outputs used

/* LAPACK files */
extern void dgels_(char *, int *, int *, int *, double *, int *, double *,
	      int *, double *, int *, int *);

/* Functions declared in this file */
extern double _maneuver_basis(int index, int deriv, double time);
static int flat_linsolve_config(double, double, Pose2D *endp, double *alpha);

/* Redefine a few names to shorten the code a bit */
#define verbose maneuver_verbose
#define basis _maneuver_basis

/*!
 * The maneuver_config2point() function is used to generate a maneuver
 * between a configuration and a pose.  It returns a pointer to a maneuver
 * structure with the information required for representing the
 * maneuver.
 *
 */
Maneuver *maneuver_config2pose(Vehicle *vp, VehicleConfiguration *startp, 
				Pose2D *endp)
{
  Maneuver *mp;
  if ((mp = maneuver_alloc(vp)) == NULL) return NULL;

  if (maneuver_verbose >= 7) {
    fprintf(stderr, "manuever_config2point: planning path from ");
    fprintf(stderr, "start = (%8g, %g, %g, %g)  to ", startp->x, startp->y, 
	    startp->theta, startp->phi);
    fprintf(stderr, "stop = (%8g, %g, %g)\n", endp->x, endp->y, endp->theta);
  }

  /* Figure out the scaling factors for the problem */
  mp->base.x = startp->x; mp->base.y = startp->y; 
  mp->base.theta = startp->theta;
  mp->scale = sqrt(pow(startp->x - endp->x, 2) + pow(startp->y - endp->y, 2));
  if (maneuver_verbose >= 8) 
    fprintf(stderr, "scaling factor = %g, rotation = %g\n", 
	    mp->scale, mp->base.theta);

  /* Now normalize the problem that we solve */
  Pose2D norm_end;

  /* Shift the end point */
  double xend = endp->x - mp->base.x;
  double yend = endp->y - mp->base.y;

  /* Rotate and normalize */
  norm_end.x = 1/mp->scale *
    (xend * cos(-mp->base.theta) - yend * sin(-mp->base.theta));
  norm_end.y = 1/mp->scale * 
    (xend * sin(-mp->base.theta) + yend * cos(-mp->base.theta));
  norm_end.theta = endp->theta - mp->base.theta;
  
  /* Find the coefficients based on differential flatness */
  flat_linsolve_config(startp->phi, vp->wheelbase/mp->scale, &norm_end, 
		       mp->alpha);

  return mp;
}

int flat_linsolve_config(double phi, double wheelbase, Pose2D *endp,
			 double *alpha)
{
  Maneuver *mp;

  /* Allocate the memory we need */
  if ((mp = calloc(1, sizeof(Maneuver))) == NULL) return -1;

  /* Generate the linear problem that we need to solve */
  double zbar[2*nFlatOutputs*(1+nFlatDerivs) + nFlatOutputs];
  double M[2*nFlatOutputs*(1+nFlatDerivs) + nFlatOutputs]
          [nFlatOutputs*nBasisFunctions];
  int index;
  for (index = 0; index < nBasisFunctions; ++index) {
    int row = 0;

    /* Initial position in x (Northing) - assume the origin */
    M[row][index] = basis(index, 0, 0.0);
    M[row][index+nBasisFunctions] = 0;
    zbar[row++] = 0;

    /* Initial position in y (Easting) - assume the origin */
    M[row][index] = 0;
    M[row][index+nBasisFunctions] = basis(index, 0, 0.0); 
    zbar[row++] = 0;

    /* Initial angle and velocity */
    M[row][index] = basis(index, 1, 0.0);
    M[row][index+nBasisFunctions] = 0;
    zbar[row++] = 1;		// cos(startp->theta);

    M[row][index] = 0;
    M[row][index+nBasisFunctions] = basis(index, 1, 0.0);
    zbar[row++] = 0;		// sin(startp->theta);

    /* Initial curvature - get from steering angle */
    M[row][index] = basis(index, 2, 0.0);
    M[row][index+nBasisFunctions] = 0;
    zbar[row++] = 0;		// sin(startp->theta) * tan(phi) / wheelbase;

    M[row][index] = 0;
    M[row][index+nBasisFunctions] = basis(index, 2, 0.0);
    zbar[row++] = tan(phi) / wheelbase;	// * cos(startp->theta)

    /* Final position in x (Northing) */
    M[row][index] = basis(index, 0, 1.0);
    M[row][index+nBasisFunctions] = 0;
    zbar[row++] = endp->x;

    /* Final position in y (Easting) */
    M[row][index] = 0;
    M[row][index+nBasisFunctions] = basis(index, 0, 1.0); 
    zbar[row++] = endp->y;

    /* Final point and velocity */
    M[row][index] = basis(index, 1, 1.0);
    M[row][index+nBasisFunctions] = 0;
    zbar[row++] = cos(endp->theta);

    M[row][index] = 0;
    M[row][index+nBasisFunctions] = basis(index, 1, 1.0);
    zbar[row++] = sin(endp->theta);
  }

  /* Declare the variables that we need for lapack */
  char trans = 'N';		// solve for underdetermined system
  int nrows = 2*nFlatOutputs*(1+nFlatDerivs) + 2, lda = nrows;
  int ncols = nFlatOutputs*nBasisFunctions, ldb = ncols;
  int nrhs = 1;
  int lwork = 2*nrows*ncols;
  double A[ncols*nrows];
  double work[2*nrows*ncols];
  int info;

  /* Fill in the matrices with the information */
  int i, j;
  for (i = 0; i < nrows; ++i) {
    for (j = 0; j < ncols; ++j) {
      A[i + j*nrows] = M[i][j];
    }
    alpha[i] = zbar[i];
  }

  dgels_(&trans, &nrows, &ncols, &nrhs, A, &lda, 
	 alpha, &ldb, work, &lwork, &info);
  if (verbose >= 5) fprintf(stderr, "dgels: info = %d\n",  info);

  /* Check the answer to see if it is OK */
  double zchk[2*nFlatOutputs*(1+nFlatDerivs) + 2];
  for (i = 0; i < nrows; ++i) {
    zchk[i] = 0;
    for (j = 0; j < ncols; ++j) {
      zchk[i] += M[i][j] * alpha[j];
    }
  }

  if (verbose >= 8) {
    // Print the problems we are trying to solve
    unsigned int row;
    for (row = 0; row < 2*nFlatOutputs*(1+nFlatDerivs) + 2; ++row) {
      fprintf(stderr, "%g = M[%d] = [", zbar[row], row);
      unsigned int col;
      for (col = 0; col < nFlatOutputs*nBasisFunctions; ++col) {
	fprintf(stderr, "%g ", M[row][col]);
      }
      fprintf(stderr, "] --- chk: %g\n", zchk[row]);
    }

    fprintf(stderr, "alpha = ");
    for (row = 0; row < 2*nBasisFunctions; ++row) {
      fprintf(stderr, "%g ", alpha[row]);
    }
    fprintf(stderr, "\n");
  }

  return info;
}
