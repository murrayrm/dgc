/*! 
 * \file UT_maneuver.c 
 * \brief Unit test for manuever generation
 *
 * \author Richard Murray
 * \date 4 May 2007
 *
 * This program does some basic checks on the manuever generation
 * library to make sure that things are working properly.  
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#include "maneuver.h"
#include "UT_cmdline.h"

/* Global variables used in various tests */
static Vehicle *vp;
static const double epsilon = 0.01;	    /* accuracy for checking config */
int check_pose2pose(Maneuver *man, Pose2D pose1, Pose2D pose2) 
{
  int errcount = 0;

  /* Check that the maneuver matches the initial pose */
  Pose2D pose = maneuver_evaluate_pose(vp, man, 0.0);
  if ((fabs(pose.x - pose1.x) > epsilon) ||
      (fabs(pose.y - pose1.y) > epsilon) ||
      (fabs(pose.theta - pose1.theta) > epsilon)) {
    fprintf(stderr, "Pose error (%g, %g, %g); initial = (%g, %g, %g)\n",
	    pose1.x, pose1.theta, pose1.theta, pose.x, pose.y, pose.theta);
    ++errcount;
  }

  /* Check that the maneuver matches the final ppose */
  pose = maneuver_evaluate_pose(vp, man, 1.0);
  if ((fabs(pose.x - pose2.x) > epsilon) ||
      (fabs(pose.y - pose2.y) > epsilon) ||
      (fabs(pose.theta - pose2.theta) > epsilon)) {
    fprintf(stderr, "Pose error at (%g, %g, %g); got (%g, %g, %g)\n",
	    pose2.x, pose2.y, pose2.theta, pose.x, pose.y, pose.theta);
    ++errcount;
  }
  return errcount;
}

int check_config2pose(Maneuver *man, VehicleConfiguration config1, 
		      Pose2D pose2)
{
  int errcount = 0;

  /* Check that the maneuver matches the initial pose */
  VehicleConfiguration config = maneuver_evaluate_configuration(vp, man, 0.0);
  if ((fabs(config.x - config1.x) > epsilon) ||
      (fabs(config.y - config1.y) > epsilon) ||
      (fabs(config.theta - config1.theta) > epsilon) ||
      (fabs(config.phi - config1.phi) > epsilon)) {
    fprintf(stderr, 
	    "Config error (%g, %g, %g, %g); initial = (%g, %g, %g, %g)\n", 
	    config1.x, config1.y, config1.theta, config1.phi, 
	    config.x, config.y, config.theta, config.phi);
    ++errcount;
  }

  /* Check that the maneuver matches the final ppose */
  Pose2D pose = maneuver_evaluate_pose(vp, man, 1.0);
  if ((fabs(pose.x - pose2.x) > epsilon) ||
      (fabs(pose.y - pose2.y) > epsilon) ||
      (fabs(pose.theta - pose2.theta) > epsilon)) {
    fprintf(stderr, "Pose error at (%g, %g, %g); got (%g, %g, %g)\n",
	    pose2.x, pose2.y, pose2.theta, pose.x, pose.y, pose.theta);
    ++errcount;
  }
  return errcount;
}

int main(int argc, char **argv)
{
  int errcount = 0;

  /* Let everyone know we are here */
  printf("UT_maneuver unit test\n");

  // Parse command line options
  struct gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  /* Set level of verbosity */
  maneuver_verbose = cmdline.verbose_arg;

  /* Initialize the vehicle configuration */
  vp = maneuver_create_vehicle(3.0, 1.0);
  assert(vp != NULL);

  /* Test #0: Generate a sample trajectory between two poses */
  Pose2D pose1 = {1, 1, 1};
  Pose2D pose2 = {10, 10, 2};

  Maneuver *man = maneuver_pose2pose(vp, &pose1, &pose2);
  assert(man != NULL);

  errcount += check_pose2pose(man, pose1, pose2);
  free(man);

  /* Test #1: Generate a sample trajectory between config and pose */
  VehicleConfiguration config1 = {1, 2, 1, 0.5};

  /* Generate the the maneuver */
  man = maneuver_config2pose(vp, &config1, &pose2);
  assert(man != NULL);

  errcount += check_config2pose(man, config1, pose2);
  free(man);

  /* Stop here if we have a problem */
  assert(errcount == 0);

  /* 
   * Test #2: generate some trajectories between two poses
   */

  double xfinal, yfinal, thetafinal;

  for (xfinal = 10; xfinal >= 1; xfinal -= 1) {
    for (yfinal = 1; yfinal < 20; yfinal += M_PI) {
      for (thetafinal = -M_PI/2; thetafinal < 3*M_PI/4; thetafinal += 1) {

	/* Set up the condition to test */
	pose2.x = xfinal;
	pose2.y = yfinal;
	pose2.theta = thetafinal;

	/* Generate the poses and the maneuver */
	Maneuver *man = maneuver_twopoint(vp, &pose1, &pose2);
	assert(man != NULL);

	errcount += check_pose2pose(man, pose1, pose2);
	free(man);
      }
    }
  }

  /* 
   * Test #2: generate some trajectories between two poses
   */

  double phi;
  config1.x = 0; config1.y = 0;
  for (phi = -0.4; phi < 0.4; phi += 0.05) {
    for (thetafinal = -M_PI/2; thetafinal < 3*M_PI/4; thetafinal += 1) {

      /* Set up the condition to test */
      config1.phi = phi;
      pose2.x = xfinal;
      pose2.y = yfinal;
      pose2.theta = thetafinal;

      /* Generate the poses and the maneuver */
      Maneuver *man = maneuver_config2pose(vp, &config1, &pose2);
      assert(man != NULL);

      check_config2pose(man, config1, pose2);
      free(man);
    }
  }

  printf("Errors: %d\n", errcount);
  return errcount;
}

