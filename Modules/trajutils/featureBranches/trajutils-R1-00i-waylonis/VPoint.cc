#include "VPoint.hh"

VPoint::VPoint(double X, double Y, double P, double dP, double dS, double dM) {
  x = X;
  y = Y;
  p = P;
  dp = dP;
  ds = dS;
  dm = dM;
}

VPoint::~VPoint() {}

double VPoint::getdP() {

  return dp;
}

double VPoint::getdS() {

  return ds;
}

double VPoint::getdM() {

  return dm;
}

VPoint* VPoint::getNext() {

  return next;
}

VPoint* VPoint::getPrevious() {

  return previous;
}

void VPoint::setNext(VPoint* p) {

  next = p;
}

void VPoint::setPrevious(VPoint* p) {

  previous = p;
}

void VPoint::setVelocity(double v) {

  velocity = v;
}

double VPoint::getVelocity() {

  return velocity;
}

double VPoint::getX() {

  return x;
}

double VPoint::getY() {

  return y;
}

double VPoint::getP() {

  return p;
}
