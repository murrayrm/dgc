#IFNDEF VPOINT_HH
#DEFINE VPOINT_HH

class VPoint {
public:
  Point(double X, double Y, double P, double dP, double dS, double dM);
  ~Point();
  double getdP();
  double getdS();
  double getdM();
  VPoint* getNext();
  VPoint* getPrevious();
  void setNext(VPoint* p);
  void setPrevious(VPoint* p);
  void setVelocity(double v);
  double getVelocity();
  double getX();
  double getY();
  double getP();

private:
  double x;
  double y;
  double p;
  double dp;
  double ds;
  double dm;
  VPoint* next;
  VPoint* previous;
  double velocity;
};

#ENDIF
