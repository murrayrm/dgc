#IFNDEF VELGEN_HH
#DEFINE VELGEN_HH

class VelocityGen {
public:
  VelocityGen(CTraj* t);
  VelocityGen(VPoint* s, VPoint* e, double maxSpd);
  ~VelocityGen();
  void getVelocityProfile(double maxSpd);
  VPoint* solve();
  double getAccel(VPoint* p1, VPoint* p2);
  double getDecel(VPoint* p1, VPoint* p2);
  double getDistance(VPoint* p1, VPoint* p2);

private:
  VPoint* start;
  VPoint* end;
  double zoneMax;
};

#ENDIF
