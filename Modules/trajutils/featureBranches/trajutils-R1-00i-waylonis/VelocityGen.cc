#include "VelocityGen.hh"

VelocityGen::VelocityGen(CTraj* t) {
  traj = t;
}

VelocityGen::VelocityGen(VPoint* s, VPoint* e, double maxSpd) {
  start = s;
  end = e;
  zoneMax = maxSpd;
}

VelocityGen::~VelocityGen() {}

void VelocityGen::getVelocityProfile(double maxSpd) {

  zoneMax = maxSpd;
  int num = traj->getNumPoints();

  // Setup the start point:

  start = new VPoint(traj->getNorthing(0),
		     traj->getEasting(0),
		     0/*angle*/,
		     0/*angularAcc*/,
		     (traj->getSpeed(1) - traj->getSpeed(0)) / traj->distToNext(0),
		     sqrt(traj->getNorthingDiff(0,1) * traj->getNorthingDiff(0,1) + traj->getEastingDiff(0,1) * traj->getEastingDiff(0,1)));

  // Setup all intermediate points:

  VPoint prev = start;
  for (int i = 1; i < num - 1; i++) {
    VPoint vp = new VPoint(traj->getNorthing(i),
			   traj->getEasting(i),
			   0/*angle*/,
			   0/*angularAcc*/,
			   (traj->getSpeed(i + 1) - traj->getSpeed(i)) / traj->distToNext(i),
			   sqrt(traj->getNorthingDiff(i,1) * traj->getNorthingDiff(i,1) + traj->getEastingDiff(i,1) * traj->getEastingDiff(i,1)));
    vp->setPrevious(prev);
    prev->setNext(vp);
    prev = vp;
  }

  // Setup the end point:
  
  end = new VPoint(traj->getNorthing(num),
		   traj->getEasting(num),
		   0/*angle*/,
		   0/*angularAcc*/,
		   0,
		   0);
  end->setPrevious(prev);
  prev->setNext(end);
}

VPoint* VelocityGen::solve() {

  double turnConst = 100;
  double steerConst = 100;

  VPoint* cur = end->getPrevious();

  end->setVelocity(0);

  // Set velocity based on angular acceleration:

  while (cur != start) {
    cur->setVelocity(turnConst / (abs(cur->getdP()) + 0.01));
    cur = cur->getPrevious();
  }

  cur = end->getPrevious();

  // Cap velocity at the maximum zone velocity:

  while (cur != start) {
    if (cur->getVelocity() > zoneMax) {
      cur->setVelocity(zoneMax);
    }
    cur = cur->getPrevious();
  }
  cur = end->getPrevious();

  // Limit velocity so as to account for the maximum steering rate:

  while (cur != start) {
    if ((cur->getVelocity() / distance(cur, cur->getNext()) * (abs(cur->getdP()) + 0.01 - abs(cur->getNext()->getdP()) + 0.01) > steerConst)) {
      cur->setVelocity(steerConst * distance(cur, cur->getNext()) / (abs(cur->getdP()) + 0.01 - abs(cur->getNext()->getdP()) + 0.01));
    }
    cur = cur->getPrevious();
  }

  cur = end->getPrevious();

  // Smooth the curve going backwards to keep deceleration under maximum:

  while (cur != start) {
    if (cur->getNext()->getVelocity() < cur->getVelocity() - getDecel(cur, cur->getNext())) {
      cur->setVelocity(cur->getNext()->getVelocity() + getDecel(cur, cur->getNext()));
    }
    cur = cur->getPrevious();
  }

  cur = start->getNext();

  // Smooth the curve going forwards to keep acceleration under maximum:

  while (cur != end) {
    if (cur->getPrevious()->getVelocity() < cur->getVelocity() - getAccel(cur, cur->getPrevious())) {
      cur->setVelocity(cur->getPrevious()->getVelocity() + getAccel(cur, cur->getPrevious()));
    }
    cur = cur->getNext();
  }

  return start;
}

double VelocityGen::getAccel(VPoint* p1, VPoint* p2) {

  double accelConst = 0.2;

  double dist = distance(p1, p2);

  return accelConst * abs(p2->getVelocity() * p2->getVelocity() - p1->getVelocity() * p1->getVelocity()) / (2 * dist);
}

double VelocityGen::getDecel(VPoint* p1, VPoint* p2) {

  double decelConst = 0.2;

  double dist = distance(p1, p2);

  return decelConst * abs(p1->getVelocity() * p1->getVelocity() - p2->getVelocity() * p2->getVelocity()) / (2 * dist);
}

double VelocityGen::distance(VPoint* p1, VPoint* p2) {

  return sqrt((p1->getX() - p2->getX()) * (p1->getX() - p2->getX()) + (p1->getY() - p2->getY()) * (p1->getY() - p2->getY()));
}
