#ifndef CPATH_HH
#define CPATH_HH

#include <pthread.h>
#include <unistd.h>
#include <iomanip>
using namespace std;

/********************************************
 * Options available to the user
 */

/** Whether or not to actually send traj's over skynet */
//#define SEND_TRAJ 1

/** When we chop the path, this is how far back along the path
 * from our current location we go */
#define CHOPBEHIND 5.0 

/** when we chop the path, this is how far forward along the
 * path we go from our current location */
#define CHOPAHEAD 100.0 

#include "skynettalker/StateClient.hh"
#include "TrajTalker.hh"
#include "PathLib.hh"

#include "alice/AliceConstants.h" 
#include <dgcutils/DGCutils.hh>

/**
 * CPath class.
 */
class CPath
{
public:
  /** Contstructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  CPath(char * rddf_file);

  /** Standard destructor */
  ~CPath();

  void SeedFromLocation(double n, double e, double yaw, CTraj * destination_traj, 
			       double merge_length);

  void UpdateState(double n, double e);

  double GetChopBehind();
  double GetChopAhead();
  void SetChopBehind(double value);
  void SetChopAhead(double value);

private:
  // variables
  corridorstruct corridor_whole;
  pathstruct path_whole_sparse;
  double chop_behind;
  double chop_ahead;

  /** called when we need to know where we are (So we know about which point to
   * crop the traj */
  void GetLocation(vector<double> & location);

  VehicleState m_state;
};

#endif  // CPATH_HH
