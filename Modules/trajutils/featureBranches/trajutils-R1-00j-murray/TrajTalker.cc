/*!
 * \file TrajTalker.cc
 * \author Jason Yosinski?
 * \date 2004-05
 *
 * This file implements the functions required to send trajectories
 * across skynet.
 *
 */

#include "TrajTalker.hh"
#include "dgcutils/DGCutils.hh"

#include <iostream>
using namespace std;

CTrajTalker::CTrajTalker()
{
  m_pDataBuffer = new char[CTraj::getHeaderSize() + 
			   TRAJ_MAX_LEN*3*2*sizeof(double)];

  DGCcreateMutex(&m_dataBufferMutex);
}


CTrajTalker::CTrajTalker(int snname, int snkey, int* status)
    : CSkynetContainer(snname, snkey, status)  
{
  m_pDataBuffer = new char[CTraj::getHeaderSize() + 
			   TRAJ_MAX_LEN*3*2*sizeof(double)];

  DGCcreateMutex(&m_dataBufferMutex);
}

CTrajTalker::~CTrajTalker()
{
  delete [] m_pDataBuffer;

  DGCdeleteMutex(&m_dataBufferMutex);
}

bool CTrajTalker::SendTraj(int trajSocket, CTraj* pTraj, pthread_mutex_t* pMutex)
{
  int            bytesToSend;
  int            bytesSent;
  int           i;
  char* pBuffer = m_pDataBuffer;

  if(pTraj->getOrder() != 3)
  {
    cerr << "Error: CTrajTalker::SendTraj(): order != 3" << endl;
    return false;
  }

  DGClockMutex(&m_dataBufferMutex);
  if(pMutex != NULL)
    DGClockMutex(pMutex);

  memcpy(pBuffer, (char*)pTraj->getHeader(), pTraj->getHeaderSize());
  pBuffer += pTraj->getHeaderSize();

  for(i=0; i<pTraj->getOrder(); i++)
  {
    memcpy(pBuffer, pTraj->getNdiffarray(i), 
	   pTraj->getNumPoints() * sizeof(double));
    pBuffer += pTraj->getNumPoints() * sizeof(double);
    memcpy(pBuffer, pTraj->getEdiffarray(i), 
	   pTraj->getNumPoints() * sizeof(double));
    pBuffer += pTraj->getNumPoints() * sizeof(double);
  }

  bytesToSend = 2 * pTraj->getOrder() * pTraj->getNumPoints() * 
    sizeof(double) + pTraj->getHeaderSize();

// cerr << "TrajTalker: about to send " << bytesToSend << " bytes" << endl;

  bytesSent = m_skynet.send_msg(trajSocket, m_pDataBuffer, bytesToSend, 0);

  DGCunlockMutex(&m_dataBufferMutex);
  if(pMutex != NULL) DGCunlockMutex(pMutex);

  if(bytesSent != bytesToSend)
  {
    cerr << "CTrajTalker::SendTraj(): sent " << bytesSent
	 << " bytes while expected to send " << bytesToSend
	 << " bytes" << endl;
    return false;
  }

  return true;
}

bool CTrajTalker::RecvTraj(int trajSocket, CTraj* pTraj, pthread_mutex_t* pMutex, string* pOutstring)
{
  int            bytesToReceive;
  int            bytesReceived;
  int            i;
  char* pBuffer = m_pDataBuffer;

  if(pTraj->getOrder() != 3)
  {
    cerr << "Error: CTrajTalker::RecvTraj(): order != 3" << endl;
    return false;
  }

  // Build the mutex list. We want to protect the data buffer and, if
  // requested, the traj
  int numMutices = 1;
  pthread_mutex_t* ppMutices[2];
  ppMutices[0] = &m_dataBufferMutex;
  if(pMutex != NULL)
  {
    ppMutices[1] = pMutex;
    numMutices++;
  }

  // Get the traj data from skynet. We want to receive the whole traj, locking
  // the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  bytesToReceive = pTraj->getHeaderSize() + TRAJ_MAX_LEN*3*2*sizeof(double);
  bytesReceived = m_skynet.get_msg(trajSocket, m_pDataBuffer, bytesToReceive,
				   0, ppMutices, false, numMutices);
  if(bytesReceived <= 0)
  {
    cerr << "CTrajTalker::RecvTraj(): skynet error" << endl;
    DGCunlockMutex(&m_dataBufferMutex);
    if(pMutex != NULL)
      DGCunlockMutex(pMutex);
    return false;
  }

  if(pOutstring != NULL)
  {
    pOutstring->append((char*)&bytesReceived, sizeof(bytesReceived));
    pOutstring->append(m_pDataBuffer, bytesReceived);
  }

// cerr << "TrajTalker: received " << bytesReceived << " bytes" << endl;

  memcpy(pTraj->getHeader(), pBuffer, pTraj->getHeaderSize());
  pBuffer += pTraj->getHeaderSize();

  for(i=0; i<pTraj->getOrder(); i++)
  {
    memcpy(pTraj->getNdiffarray(i), pBuffer,
	   pTraj->getNumPoints() * sizeof(double));
    pBuffer += pTraj->getNumPoints() * sizeof(double);
    memcpy(pTraj->getEdiffarray(i), pBuffer,
	   pTraj->getNumPoints() * sizeof(double));
    pBuffer += pTraj->getNumPoints() * sizeof(double);
  }

  DGCunlockMutex(&m_dataBufferMutex);
  if(pMutex != NULL)
    DGCunlockMutex(pMutex);

  return true;
}

void CTrajTalker::WaitForTrajData(int trajSocket)
{
  m_skynet.sn_select(trajSocket);
}
