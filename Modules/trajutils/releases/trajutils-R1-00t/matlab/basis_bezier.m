% bezier.m - basis functions for planning
function val = basis(index, deriv, time)
switch index
  case 0,				% (1-time)^3
    switch deriv
      case 0, val = (1-time).^3;
      case 1, val = - 3 .* (1-time).^2;
      case 0, val = 6 * (1-time);
    end

  case 1,				% 3 * t * (1-t)^2
    switch deriv
    case 0, val = 3 * time .* (1-time).^2;
    case 1, val =  3 * (1-time).^2 - 6 * time .* (1-time);
    case 2, val = -6 * (1-time) - 6 * (1-time) + 6 * time;
    end

  case 2,				% 3 * t^2 * (1-t)
    switch deriv
    case 0, val = 3 * (time.^2) .* (1-time);
    case 1, val =  6 * time .* (1-time) - 3 * (time.^2);
    case 2, val = 6 * (1-time) - 6 * time - 6 * time;
    end

  case 3,				% t^3
    switch deriv
    case 0, val = (time.^3);
    case 1, val =  3 * (time.^2);
    case 2, val = 6 * time;
    end
end
