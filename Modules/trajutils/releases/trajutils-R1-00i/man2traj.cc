/*!
 * \file man2traj.cc
 * \brief Convert a manuever list into a CTraj
 * 
 * \author Richard M. Murray
 * \date 1 May 2007
 *
 * This function takes a list of maneuvers and a set of velocities for
 * the endpoints of the maneuvers and generates a CTraj.
 *
 */

#include <iostream>
#include <math.h>
#include "maneuver.h"
#include "traj.hh"

int maneuver_profile_generate(Vehicle *vp, int nseg, Maneuver **mlist, 
			      double *vlist, int nTrajPoints, CTraj *ptraj)
{
  int npoints = 0;
  /* Go through each segment */
  for (int seg = 0; seg < nseg; ++seg) {
    Maneuver *mp = mlist[seg];

    /* Determine the velocity profile for this segment */
    double initial_vel = vlist[seg];
    double final_vel = vlist[seg+1];

    /* Generate a CTraj point at each data point */
    for (int i = 0; i < nTrajPoints; ++i) {
      double s1 = ((double) i / nTrajPoints);
      double s2 = ((double) (i+1) / nTrajPoints);

      /*
       * Compute the trajectory parameters for this segment
       *
       * We now compute the position, velocity and acceleration for
       * this segment, using the kinematics of the system:
       * 
       * xdot = v cos(th)	-> xdd = vdot cos(th) - v sin(th) thdot
       * ydot = v sin(th)	-> ydd = vdot sin(th) + v cos(th) thdot
       * thdot = v/l tan(phi)	-> omega = v/l tan(phi)
       *
       */

      /* Compute the starting and end configurations for this segment */
      VehicleConfiguration vcfg1, vcfg2;
      vcfg1 = maneuver_evaluate_configuration(vp, mp, s1);
      vcfg2 = maneuver_evaluate_configuration(vp, mp, s2);

      /* Compute the length of this segment */
      double dist = sqrt(pow(vcfg1.x-vcfg2.x, 2) + pow(vcfg1.y-vcfg2.y, 2));

      /* Compute the desired velocity for this segment */
      double vel = (final_vel - initial_vel) * s1 + initial_vel;

      /* Use the velocity + distance to compute rough time on this segment */
      double dt = (vel == 0 ? 0 : dist/vel);

      /* Compute the acceleration along this segment */
      double vdot = (dt == 0 ? 0 : (final_vel - initial_vel) * (s2-s1) / dt);

#     ifdef DEBUG
      cout << "dist = " << dist 
	   << ", vel = " << vel
	   << ", dt = " << dt
	   << ", vdot = " << vdot << endl;
#     endif

      /* Now compute out the spatial position, velocity, accleration */
      double N, E, Nd, Ed, Ndd, Edd;

      N = vcfg1.x;
      E = vcfg1.y;

      Nd = vel * cos(vcfg1.theta);
      Ed = vel * sin(vcfg1.theta);

      /* For the acceleration, make sure we don't saturate the steering */
      if (fabs(vcfg1.phi) > vp->steerlimit) 
	vcfg1.phi = (vcfg1.phi / fabs(vcfg1.phi)) * vp->steerlimit;

      double omega = (vel / vp->wheelbase) * tan(vcfg1.phi);
      Ndd = vdot * cos(vcfg1.theta) - vel * sin(vcfg1.theta) * omega;
      Edd = vdot * sin(vcfg1.theta) + vel * cos(vcfg1.theta) * omega;

#     ifdef DEBUG
      cout << "  phi = " << vcfg1.phi << "  omega = " << omega 
	   << "  , Ndd = " << Ndd << ", Edd = " << Edd << endl;

      //      Ndd = Edd = 0;
#     endif

      /* Add the point to the trajectory, minus the last point */
      ptraj->addPoint(N, Nd, Ndd, E, Ed, Edd);
      ++npoints;
    }
  }

  /* Add on the very last point */
# warning missing final point on segment

  return npoints;
}
