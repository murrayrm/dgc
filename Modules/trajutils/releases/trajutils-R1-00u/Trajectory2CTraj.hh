///
/// \file Trajectory2CTrah.hh
/// \brief Converter from new Trajectory to old CTraj
///
/// \author Kristian Soltesz
/// \date 6 August 2007
#ifndef TRAJECTORY2CTRAJ_HH_HVKDSVHGVSGJSDG
#define TRAJECTORY2CTRAJ_HH_HVKDSVHGVSGJSDG

// Be careful with name spaces here
#include "Trajectory.hh"
#include "traj.hh"
#include <math.h>

/// This function takes an instance
/// of the new trajectory class Trajectory
/// and returns an instance of the 
/// old trajectory class CTraj, corresponding
/// to it.
void Trajectory2CTraj(Trajectory* trajectory, CTraj* ctraj);

#endif // TRAJECTORY2CTRAJ_HH_HVKDSVHGVSGJSDG
