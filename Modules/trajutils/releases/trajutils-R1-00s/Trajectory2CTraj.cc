///
/// \file Trajectory2CTrah.cc
/// \brief Converter from new Trajectory to old CTraj
///
/// \author Kristian Soltesz
/// \date 6 August 2007

#include "Trajectory2CTraj.hh"

void Trajectory2CTraj(Trajectory* trajectory, CTraj* ctraj) {

  // We need to construct the CTraj. 
  // Passing a pointer to a CTraj
  // to be populated, is not enough,
  // since we cannot set order.
  // FIXME: is this the way to do it,
  // or could it cause memory leak?
  *ctraj = CTraj(3);

  // Set the direction of the trajectory 
  ctraj->setDirection(trajectory->getDirection());

  // This loop copies the trajectory
  // into ctraj, one node at a time
  for (int node = 0; node < trajectory->getNumberOfPoints(); node++) {
    
    // Copying the coordinates is trivial
    double new_N = trajectory->getn(node);
    double new_E = trajectory->gete(node);
    
    // These variables are for internal use
    // and will not explicitely be written
    // to the ctraj.
    double v = trajectory->getv(node);
    double yaw = trajectory->getyaw(node);
    double a = trajectory->geta(node);
    
    // The projections are precomputed
    //FIXME: is this correct?
    double nProjection = cos(yaw);
    double eProjection = sin(yaw);
    
    double new_Nd = nProjection * v;
    double new_Ed = eProjection * v;

    double new_Ndd = nProjection * a;
    double new_Edd = eProjection * a;

    ctraj->addPoint(new_N, new_Nd, new_Ndd, new_E, new_Ed, new_Edd);
  }
}
