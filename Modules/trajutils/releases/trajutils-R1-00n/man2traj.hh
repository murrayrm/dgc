/*!
 * \file man2traj.hh
 * \brief Header file for man2traj function
 *
 * \author Richard Murray
 * \date 1 may 2005
 *
 */

#include "maneuver.h"
#include "traj.hh"


/// @brief Generate the trajectory for a single maneuver, with fine
/// control of the speed.
///
/// @param[in] vp Vehicle description.
/// @param[in] mp Maneuver to use for the trajectory generator.
/// @param[in] nSpeeds Number of speeds in the speed array.
/// @param[in] speeds Array of speeds (the first and last speeds
/// should match the first and last points in the maneuver).
/// @param[in] nSteps Number of steps to generate in the trajector.
/// @param[out] ptraj Trajectory object (points will be appended).
extern int maneuver_profile_single(Vehicle *vp, Maneuver *mp,
                                   int nSpeeds, const double *speeds,
                                   int nSteps, CTraj *ptraj);

extern int maneuver_profile_generate(Vehicle *, int, Maneuver **, 
                                     const double *, int, CTraj *);

