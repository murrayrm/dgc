/*!
 * \file maneuver.h
 * \brief struct and function definitions for maneuver library
 *
 * \author Richard Murray
 * \date 30 April 2007
 *
 * This manuever library contains some functions for generating
 * manuevers for a simple kinematic vehicle.  It exploits the fact
 * that a vehicle is differentially flat, so we can follow any curve
 * in the plane subject to curvature constraints.  Each manuever is
 * planned over the internal [0, 1], which can be rescaled to give a
 * trajectory with the appropriate velocity profile.
 *
 * Basic operation:
 *
 *   Vehicle *vp = maneuver_create_vehicle(wheelbase, steerlimit);
 *   Maneuver *mp = maneuver_twopoint(vp, &initialPose, &finalPose);
 *   Pose2D pose = maneuver_evaluate_pose(vp, mp, time);
 *
 * Note: curvature constraints are not currently implemented; coming soon.
 *
 */

#ifndef __MANEUVER_H__
#define __MANEUVER_H__

/*! Vehicle description */
typedef struct vehicle {
  double max_curvature;			/*!> Maximum allowable curvature  */
  double wheelbase;			/*!> Wheelbase (front to rear) */
  double steerlimit;			/*!> Maximum steering angle (rad)  */
} Vehicle;

/*! 2D pose information (position, orientation) */
typedef struct pose2d {
  double x, y, theta;
} Pose2D;

/*! 2D pose information (position, orientation) */
typedef struct vehicle_configuration {
  double x, y, theta, phi;
} VehicleConfiguration;

/*! Manuever data structure */
typedef struct maneuver {
  double *alpha;			/*!> Maneuver parameters */
  Pose2D base;				/*!> Initial pose for maneuver  */
  double scale;				/*!> Scaling factor  */
  double (*basis)(int, int, double);	/*!> Basis functions  */
} Maneuver;

/*! Flag to allow printing of information messages */
extern int maneuver_verbose;

/* 
 * Function declarations 
 */

#ifdef __cplusplus
extern "C" {
#endif

/*! Create a vehicle to establish maneuver limits */
Vehicle *maneuver_create_vehicle(double, double);

/*! Compute a maneuver from two pose points */
Maneuver *maneuver_twopoint(Vehicle *, Pose2D *, Pose2D *);
#define maneuver_pose2pose(vp, startp, endp) \
  maneuver_twopoint(vp, startp, endp)

/*! Compute a maneuver between a configuration and a pose */
Maneuver *maneuver_config2pose(Vehicle *, VehicleConfiguration *, Pose2D *);

/*! Determine the pose corresponding to a given maneuver at a given time */
Pose2D maneuver_evaluate_pose(Vehicle *, Maneuver *, double);

/*! Determine the vehicle configuration for a maneuver at a given time */
VehicleConfiguration maneuver_evaluate_configuration(Vehicle *, Maneuver *, 
						     double);

/*! Allocated the memory associated with a maneuver */
Maneuver *maneuver_alloc(Vehicle *);

/*! Fee the memory associated wth a maneuver */
void maneuver_free(Maneuver *);

#ifdef __cplusplus
}
#endif

#endif
