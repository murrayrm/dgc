#ifndef _TRAJ_HH_
#define _TRAJ_HH_

/** This is the trajectory container class. The trajectory stored is a
 * list of Northing values, their derivatives, Easting values and
 * their derivatives. The number of derivatives stored is given by the
 * m_trajHeader.m_order member (order = maxderivativenumber+1). The
 * number of points stored is hard-limited by TRAJ_MAX_LEN. Setting
 * this high will only affect local memory allocation and will not
 * slow down anything that sends trajectories over the network, since
 * only m_numPoints points would be sent. The actual data is stored as
 * two one-dimensional arrays in m_pN, m_pE. Those arrays are indexes
 * by the INDEX macro.
*/

#include <ostream>
#include <iostream>
using namespace std;

#include "frames/coords.hh"

#define TRAJ_MAX_LEN  50000

// How close to the path we have to be to be considered "in the path"
#define TRAJ_IN_PATH_RES  2.0

#define INDEX(i, d) ((i) + (d)*TRAJ_MAX_LEN)

struct TrajPoint {
  double n,e,nd,ed,ndd,edd;
  TrajPoint (double,double,double,double,double,double);
};


class CTraj {
protected:
  struct STrajHeader {
    int m_numPoints;		// number of points in this trajectory
    int m_order;		// max derivs of each point+1 (x,xdot -> 2)
    int m_dir;			// direction of the trajectory (-1 = rev)
  } __attribute__((packed)) m_trajHeader;
  
  // actual trajectory: northing, easting and their derivatives
  // each array holds at most TRAJ_MAX_LEN*m_trajHeader.m_order points
  double  *m_pN;
  double  *m_pE;

  // filled in by the planner to contain the minimum speed of this
  // trajectory. Note that this is NOT filled in automatically.
  double    m_minSpeed;

public:
  /** The constructors allocate memory for the data */
  CTraj(int order=3);
  
  /** If an input stream is specified, the trajectory is filled in from that
    stream. The trajectory file is a white-space-delimited text array of {n,
    ndot, nddot, ..., e, edot, eddot, ...} points (assumed to be SI units).
    Comments can appear in a line as long as the first character is '%' or '#'
  */
  CTraj(int order, istream& inputstream);

  /** If an filename is specified, the trajectory is filled in from that
    file. The trajectory file is a white-space-delimited text array of {n,
    ndot, nddot, ..., e, edot, eddot, ...} points (assumed to be SI units).
    Comments can appear in a line as long as the first character is '%' or '#'
  */
  CTraj(int order, char* pFilename);

  /** This function loads the current traj from a stream */
  void load(istream& inputstream);

  /** Destructor deallocates memory */
  ~CTraj();
  
  /* Assignment operator overload */
  CTraj &operator=(const CTraj &other);

	/* Append operator */
  CTraj &operator+=(const CTraj &other);
  
  CTraj(const CTraj & other);
  
  /** Returns the last Northing value */
  double  lastN();

  /** Returns the last Easting value */
  double  lastE();

  /** Returns the number of points in the structure */
  int     getNumPoints() { return m_trajHeader.m_numPoints; }

  /** Return the direction of the trajectory */
  int     getDirection() { return m_trajHeader.m_dir; }

  /** Returns the order of the structure (max derivative number + 1) */
  int     getOrder() { return m_trajHeader.m_order; }
  
  /** Returns a pointer to the Northing data of specified order */
  double* getNdiffarray(int order) { return m_pN+INDEX(0, order); }

  /** Returns a pointer to the Easting data of specified order */
  double* getEdiffarray(int order) { return m_pE+INDEX(0, order); }
  
  /** Returns the number of bytes stored in the header of the class. */
  static unsigned int getHeaderSize() { return sizeof(STrajHeader); }

  /** Returns the header */
	STrajHeader* getHeader() { return &m_trajHeader; }

  /** Returns the index-th Northing value */
  double  getNorthing(int index) { return m_pN[INDEX(index,0)]; }

  /** Returns the index-th Easting value */
  double  getEasting(int index) { return m_pE[INDEX(index,0)]; }

  /** Returns the der-th derivative of the index-th point as an NEcoord */
  NEcoord getNEcoord(int index, int der = 0) { return NEcoord(m_pN[INDEX(index,der)], m_pE[INDEX(index,der)]); }

  /** Sets the index-th Northing value */
  void  setNorthing(int index, double val) { m_pN[INDEX(index,0)] = val; }

  /** Sets the index-th Easting value */
  void  setEasting (int index, double val) { m_pE[INDEX(index,0)] = val; }

  /** Sets the index-th Northing,Easting value */
  void  setNorthingEasting (int index, double valN, double valE) { m_pN[INDEX(index,0)] = valN; m_pE[INDEX(index,0)] = valE; }


  /** Set the number of points */
  void  setNumPoints(int numPoints) { m_trajHeader.m_numPoints = numPoints; }

  /** Set the direction of the trajecotry */
  void setDirection(int dir) { m_trajHeader.m_dir = dir; }

	/** Set/get the min speed. This is the ONLY interface to this argument and is
			NOT set automatically */
	void   setMinSpeed(double minSpeed) { m_minSpeed = minSpeed; }
	double getMinSpeed(void) { return m_minSpeed; }

  /** Returns the diff-th value derivative of the index-th Northing value  */
  double  getNorthingDiff(int index, int diff) { return m_pN[INDEX(index,diff)]; }
  
  /** Returns the diff-th value derivative of the index-th Easting value  */
  double  getEastingDiff(int index, int diff) { return m_pE[INDEX(index,diff)]; }

  /** Returns the speed of the index-th point */
  double getSpeed(int index) { return hypot(m_pN[INDEX(index,1)], m_pE[INDEX(index,1)]); }

  /** Sets the speed of the index-th point to be `speed', and scales
   * the LATERAL accel appropriately */
  void setSpeed(int index, double speed);

  /** Sets the longitudinal accel of the index-th point */
  void setLongAccel(int index, double long_accel_magnitude);
  
  /** Sets the lateral accel of the index-th point (right is positive) */
  void setLatAccel(int index, double lat_accel_magnitude);
  
  /** Computes whether the given (N,E) pair is within TRAJ_IN_PATH_RES of the
    path */
  bool    inTraj(double northing, double easting);
  
  /** This function is called before starting to fill in the trajectory */
  void    startDataInput(void);
  
  /** Input the given (N,E) pair into the trajectory */
  void    inputNoDiffs(double northing, double easting);
  
  /** Input the given N, N derivatives, E, E derivatives data into the
    trajectory */
  void    inputWithDiffs(double* northing, double* easting);

  /** Set a given point in a 3rd order traj.  ONLY WORKS FOR 3RD ORDER! */
  void setPoint(int index, double new_N, double new_Nd, double new_Ndd,
		double new_E, double new_Ed, double new_Edd);

  /** Add a given point to the end of a 3rd order traj.  ONLY WORKS FOR 3RD ORDER! */
  void addPoint(double new_N, double new_Nd, double new_Ndd,
		double new_E, double new_Ed, double new_Edd);


  /** Shifts the spatial data ptsToToss points back to the beginning */
  void    shiftNoDiffs(int ptsToToss);
  
  /** Cuts down the current trajectory to be of length desiredDist (if the traj
    is longer), or lengthens it to be desiredDist long */
  void    truncate(double desiredDist);
  
  /** Prepends the pFrom traj (from start to end inclusively) into the current
      traj */
  void    prepend(CTraj* pFrom, int start, int end);
  
  /** Appends from point start to point end of pFrom onto the current traj */
  void    append (CTraj* pFrom, int start, int end);

  /** Fills in thetavector with theta(s) where s is the length along the traj
      that goes from 0 to dist with num total points. bias is subtracted from
      every element */
  void    getThetaVector(double dist, int num, double* thetavector, double bias = 0.0);

  /** Fills in speedvector with speed(s) where s is the length along the traj
      that goes from 0 to dist with num total points. bias is subtracted from
      every element */
  void    getSpeedVector(double dist, int num, double* speedvector, double bias = 0.0);

  /** Print out the current trajectory to the given ostream (cout if not
    specified). If less than every point should be printed, that can be
    specified as well.  Column format is [N, dN, ddN, E, dE, ddE], (SI). */
  void    print(ostream& outputstream = cout, int numPoints = -1);

  /** Prints out the speed profile into the ostream specified */
  void    printSpeedProfile(ostream& outputstream = cout);


  /** This funstion applies a speed limit to the path.*/
  void speed_adjust(double speedlimit,NEcoord* pState,double aspeed);

  /** Returns the index of the closest point in this traj to the provided (N,E) 
   * */
  int getClosestPoint(double n, double e);

  /** Returns the first index of the trajectory that is further forward on the 
   * trajectory by a minimum distance, assuming the trajectory to be piecewise 
   * linear.  If this minimum distance is greater than the distance to the end 
   * of the trajectory, then the index of the last point on the trajectory is 
   * returned. */
  int getPointAhead( int curpoint, double dist );
  
  /** Returns the length of the whole traj if it's assumed to be piecewise 
   * linear. */
  double getLength(void);

  /** Returns the distance between two points */
  double dist(int from, int to);

  /** Returns the distance from the index-th point to the next */
  double distToNext(int index);


  /**Enhance the CTraj class with trajectory interpolation. */
  TrajPoint interpolate (NEcoord);


  /** Given a point (n,e), interpolGetClosest finds the 'closest point' on the traj and returns it.
   *  The function generates a TrajPoint by linear interpolation. It returns this
   *  TrajPoint, and also writes the index of its closest preceding neighbor,
   *  and a fractionToNext describing where between its two neighbors the returned TrajPoint is.
   *  See the in-code documentation for handling of special cases.
   *
   * @param n northing of point (n,e)
   * @param e easting of point (n,e)
   * @param index pointer to which the index of the closest preceding neighbor of the returned TrajPoint is written
   * @param fractionToNext pointer to which the distance between the returned trajPoint and its closest preceding 
   *                       neigbors is written. The distance is expressed as a fraction of the distance between the 
   *                       neighbors of the returned TrajPoint
   */
  TrajPoint interpolGetClosest(double n, double e, int* index, double* fractionToNext);


  /** interpolGetPoint returns a TrajPoint a fractionToNext between points index and index+1 (measured from index). 
   *  Velocity and 
   *  acceleration references of the returned TrajPoint are linear interpolations of index and index+1.
   *  See the in-code documentation for handling of special cases.
   *
   * @param index index of point preceding the point to be returned
   * @param fractionToNext distance between index and point to be returned. Expressed as a fraction of the distance
   *                       between index and index+1
  */
  TrajPoint interpolGetPoint(int index, double fractionToNext);


  /** interpolGetPointAhead takes a TrajPoint given by index and a fractionToNext and calculates a 
   *  TrajPoint dist ahead, by linear interpolation. This TrajPoint is returned, and 
   *  the index if  its cloesest preceding neighbor is written together with a fractionToNextAhead.
   *  (See interpolGetClosest, interpolGetPoint for explanation of fractionToNext.)
   *  See the in-code documentation for handling of special cases.
   *
   *  @param index index of point preceding start point of distance
   *  @param fractionToNext defines the distance between index and actual start point
   *  @param distance distance between start point and point to be returned
   *  @param indexAhead index of point preceding end point of distance
   *  @param fractionToNextAhead defines the distance between indexAhead and actual end point
   */  
  TrajPoint interpolGetPointAhead(int index, double fractionToNext, 
                                  double distance, int* indexAhead, double* fractionToNextAhead);

	
  /** Makes the traj feasible.  feasiblize accomplishes this by:
   * 1. estimating the curvature at every point on the traj
   * 2. speedcapping every point based on its curvature and the vehicle's
   *    maximum lateral acceleration
   * 3. lowering speeds to make them compatible with the vehicle's
   *    maximum longitudinal acceleration
   *
   * Assumptions:
   * When speedcapping, feasiblize will always respect the speed limits already
   * in the traj (it will never increase the speed of a point).  If you'd like
   * your traj to respect the max speeds present in the map, first run it
   * through that.
   *
   * There aren't two traj points right on top of each other... don't be stupid.
   *
   * All the velocities in the traj point in the right direction (i.e. toward
   * the next point on the traj).
   *
   * @param max_lat_accel the maximum allowed lateral acceleration that the resulting
   *                      traj will have
   * @param max_long_accel the max allowed acceleration in the forward direction that
   *                       the resulting traj will have
   * @param max_long_decel the max allowed accel in the backward direction (deceleration
   *                       due to braking
   * @param averaging_distance distance ahead and behind of each point that will be
   *                           used to estimate the curvature of the path.  Actually
   *                           we'll always use one point before and/or after PLUS
   *                           this distance
   */
  void feasiblize(double max_lat_accel, double max_long_accel, double max_long_decel,
		  double averaging_distance);

  /** Erases traj and fills it with a spline from one location to another.  this method
   * uses a standard cubic spline 
   *
   * @param density nominal spacing of the points in the resulting traj. */
  void fillWithCubicSpline(double here_n, double here_n_dot,
			   double here_e, double here_e_dot,
			   double there_n, double there_n_dot,
			   double there_e, double there_e_dot,
			   double density);
};

#endif // _TRAJ_HH_
