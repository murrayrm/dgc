% testflat.m - function to test basis functions
% RMM, 1 May 07

global nBasisFunctions;
nBasisFunctions = 8;

time = 0:0.01:1;
figure(1); clf; 

% Plot the basis functions
for index = 0:3:12
  for height = -1:1:1

    angle = (index-6) / 3;
    pose1 = [0, 0, 0];
    pose2 = [1, height, angle];

    alpha = twopoint(pose1, pose2);
    path = makepath(alpha, time);

    figure(1); hold on;
    plot(index + path(1,:), path(2,:));
  end
end
axis equal

% Now try doing some rescaling
