/*!
 * \file maneuver.c
 * \brief library for generating vehicle maneuvers
 *
 * \author Richard Murray
 * \date 30 April 2007
 *
 * This file contains some utility functions for generating maneuvers
 * for a simple kinematic vehicle.  It exploits the fact that a
 * vehicle is differentially flat, so we can follow any curve in the
 * plane subject to curvature constraints.  Each maneuver is planned
 * over the internal [0, 1], which can be rescaled to give a
 * trajectory with the appropriate velocity profile.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "maneuver.h"

/* Some constants that control our operations */
const int nBasisFunctions = 8;	// number of basis functions
const int nBasisDerivs = 1;	// number of derivatives supported
const int nFlatOutputs = 2;	// number of flat outputs that we use
const int nFlatDerivs = 1;	// number of derivatives of flat outputs used

/* LAPACK files */
extern void dgels_(char *, int *, int *, int *, double *, int *, double *,
	      int *, double *, int *, int *);

/* Functions declared in this file */
static double basis(int index, int deriv, double time);
int flat_linsolve(Pose2D *startp, Pose2D *endp, double *alpha);

/* Allow printing messages for debugging */
int maneuver_verbose = 0;
#define verbose maneuver_verbose

/* Utility functions for allocating and free maneuver object memory */
Maneuver *maneuver_alloc(Vehicle *vp)
{
  Maneuver *mp;

  /* Allocate the memory that we need */
  if ((mp = calloc(1, sizeof(Maneuver))) == NULL) return NULL;
  if ((mp->alpha = calloc(nFlatOutputs*nBasisFunctions, 
			  sizeof(double))) == NULL) {
    free(mp);
    return NULL;
  }
  return mp;
}

void maneuver_free(Maneuver *mp)
{
  if (mp->alpha != NULL) free(mp->alpha);
  free(mp);
}

/*!
 * The maneuver_create_vehicle() function is used to create a vehicle element
 * that can be used for subsequent maneuvers.
 *
 * \param wheelbase
 * Vehicle wheelbase (in length units)
 *
 * \param steerlimit
 * Maximum steering angle (in radians)
 *
 */
Vehicle *maneuver_create_vehicle(double wheelbase, double steerlimit)
{
  Vehicle *vp;
  
  /* Allocate space for the structure */
  if ((vp = calloc(1, sizeof(Vehicle))) == NULL) return NULL;

  /* Compute the turning radius limit */
  vp->max_curvature = wheelbase * tan(steerlimit);
  vp->wheelbase = wheelbase;
  vp->steerlimit = steerlimit;

  return vp;
}

/*!
 * The maneuver_twopoint() function is used to generate a maneuver
 * between one pose and another.  It returns a pointer to a maneuver
 * structure with the information required for representing the
 * maneuver.
 */
Maneuver *maneuver_twopoint(Vehicle *vp, Pose2D *startp, Pose2D *endp)
{
  Maneuver *mp;
  if ((mp = maneuver_alloc(vp)) == NULL) return NULL;

  if (maneuver_verbose >= 7) {
    fprintf(stderr, "manuever_twopoint: planning path from");
    fprintf(stderr, "start = (%8g, %g, %g) to ", startp->x, startp->y, 
	    startp->theta);
    fprintf(stderr, "stop = (%8g, %g, %g)\n", endp->x, endp->y, endp->theta);
  }

  /* Figure out the scaling factors for the problem */
  mp->base = *startp;
  mp->scale = sqrt(pow(startp->x - endp->x, 2) + pow(startp->y - endp->y, 2));
  if (maneuver_verbose >= 8) 
    fprintf(stderr, "scaling factor = %g, rotation = %g\n", 
	    mp->scale, mp->base.theta);

  /* Now normalize the problem that we solve */
  Pose2D norm_start = {0, 0, 0}, norm_end;

  /* Shift the end point */
  double xend = endp->x - mp->base.x;
  double yend = endp->y - mp->base.y;

  /* Rotate and normalize */
  norm_end.x = 1/mp->scale *
    (xend * cos(-mp->base.theta) - yend * sin(-mp->base.theta));
  norm_end.y = 1/mp->scale * 
    (xend * sin(-mp->base.theta) + yend * cos(-mp->base.theta));
  norm_end.theta = endp->theta - mp->base.theta;
  
  /* Find the coefficients based on differential flatness */
  flat_linsolve(&norm_start, &norm_end, mp->alpha);

  return mp;
}

/*!
 * Evaluate a maneuver at a given point long the path and return the
 * pose corresponding to that point.
 */
Pose2D maneuver_evaluate_pose(Vehicle *vp, Maneuver *mp, double time)
{
  Pose2D pose;
  double x=0, y=0, xd=0, yd=0;

  /* Compute the flat variables and their derivatives */
  int index;
  for (index = 0; index < nBasisFunctions; ++index) {
    x += mp->alpha[index] * basis(index, 0, time);
    y += mp->alpha[index+nBasisFunctions] * basis(index, 0, time);
      
    xd += mp->alpha[index] * basis(index, 1, time);
    yd += mp->alpha[index+nBasisFunctions] * basis(index, 1, time);
  }

  /* Now convert these to the vehicle configuration */
  double theta = atan2(yd, xd);
  pose.x = mp->base.x + mp->scale * 
    (x * cos(mp->base.theta) - y * sin(mp->base.theta));
  pose.y = mp->base.y + mp->scale *
    (x * sin(mp->base.theta) + y * cos(mp->base.theta));
  pose.theta = mp->base.theta + theta;

  return pose;
}

VehicleConfiguration maneuver_evaluate_configuration(Vehicle *vp, Maneuver *mp, double time)
{
  VehicleConfiguration vcfg;
  double x=0, y=0, xd=0, yd=0, xdd=0, ydd=0;

  /* Compute the flat variables and their derivatives */
  int index;
  for (index = 0; index < nBasisFunctions; ++index) {
    x += mp->alpha[index] * basis(index, 0, time);
    y += mp->alpha[index+nBasisFunctions] * basis(index, 0, time);
      
    xd += mp->alpha[index] * basis(index, 1, time);
    yd += mp->alpha[index+nBasisFunctions] * basis(index, 1, time);
      
    xdd += mp->alpha[index] * basis(index, 2, time);
    ydd += mp->alpha[index+nBasisFunctions] * basis(index, 2, time);
  }

  /* Now convert these to the vehicle configuration */
  double theta = atan2(yd, xd);
  vcfg.x = mp->base.x + mp->scale * 
    (x * cos(mp->base.theta) - y * sin(mp->base.theta));
  vcfg.y = mp->base.y + mp->scale *
    (x * sin(mp->base.theta) + y * cos(mp->base.theta));
  vcfg.theta = mp->base.theta + theta;
  vcfg.phi = atan((ydd * cos(theta) - xdd * sin(theta)) * 
		  vp->wheelbase / mp->scale);

  /* Renormalize theta if necessary */
  if (vcfg.theta > M_PI) vcfg.theta -= 2 * M_PI; // TESTING
  if (vcfg.theta < -M_PI) vcfg.theta += 2 * M_PI;

  return vcfg;
}

/*
 * Static functions that implement the main functionality of the
 * library.  These functions are hidden from the user so that we can
 * change them if needed.
 */

/* Basis functions - these are used for planning trajectories */
static double basis(int index, int deriv, double time)
{
  switch (index) {
  case 0:			// phi(t) = t
    switch (deriv) {
    case 0:	return time;
    case 1:	return 1;
    case 2:	return 0;
    }

  case 1:			// phi(t) = t^2
    switch (deriv) {
    case 0:	return pow(time, 2);
    case 1:	return 2*time;
    case 2:	return 1;
    }

  case 2:			// phi(t) = t^3
    switch (deriv) {
    case 0:	return pow(time, 3);
    case 1:	return 3 * pow(time, 2);
    case 2:	return 6 * time;
    }

  case 3:			// phi(t) = (1-t)^3
    switch (deriv) {
    case 0:	return pow(1-time, 3);
    case 1:	return -3 * pow(1-time, 2);
    case 2:	return 6 * (1-time);
    }

  case 4:			// phi(t) = (1-t)^2
    switch (deriv) {
    case 0:	return pow(1-time, 2);
    case 1:	return -2 * (1-time);
    case 2:	return 2;
    }

  case 5:			// phi(t) = (1-t)
    switch (deriv) {
    case 0:	return (1-time);
    case 1:	return -1;
    case 2:	return 0;
    }

  case 6:			// phi(t) = 3 t (1-t)^2
    switch (deriv) {
    case 0:	return 3 * time * pow(1-time, 2);
    case 1:	return 3 * pow(1-time, 2) - 6 * time * (1-time);
    case 2:	return -6 * (1-time) - 6 * (1-time) + 6 * time;
    }

  case 7:			// phi(t) = 3 t^2 (1-t)
    switch (deriv) {
    case 0:	return 3 * pow(time, 2) * (1-time);
    case 1:	return 6 * time * (1-time) - 3 * pow(time, 2);
    case 2:	return 6 * (1-time) - 6 * time - 6 * time;
    }

  }
  return 0;
}

int flat_linsolve(Pose2D *startp, Pose2D *endp, double *alpha)
{
  Maneuver *mp;

  /* Allocate the memory we need */
  if ((mp = calloc(1, sizeof(Maneuver))) == NULL) return -1;

    /* Generate the linear problem that we need to solve */
    double zbar[2*nFlatOutputs*(1+nFlatDerivs)];
    double M[2*nFlatOutputs*(1+nFlatDerivs)][nFlatOutputs*nBasisFunctions];
    int index;
    for (index = 0; index < nBasisFunctions; ++index) {
      int row = 0;

      /* Initial position in x (Northing) */
      M[row][index] = basis(index, 0, 0.0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = startp->x - startp->x;

      /* Initial position in y (Easting) */
      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 0, 0.0); 
      zbar[row++] = startp->y - startp->y;

      /* Initial angle and velocity */
      M[row][index] = basis(index, 1, 0.0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = cos(startp->theta);

      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 1, 0.0);
      zbar[row++] = sin(startp->theta);

      /* Final position in x (Northing) */
      M[row][index] = basis(index, 0, 1.0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = endp->x - startp->x;

      /* Final position in y (Easting) */
      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 0, 1.0); 
      zbar[row++] = endp->y - startp->y;

      /* Final point and velocity */
      M[row][index] = basis(index, 1, 1.0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = cos(endp->theta);

      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 1, 1.0);
      zbar[row++] = sin(endp->theta);
    }

    /* Declare the variables that we need for lapack */
    char trans = 'N';		// solve for underdetermined system
    int nrows = 2*nFlatOutputs*(1+nFlatDerivs), lda = nrows;
    int ncols = nFlatOutputs*nBasisFunctions, ldb = ncols;
    int nrhs = 1;
    int lwork = 2*nrows*ncols;
    double A[ncols*nrows];
    double work[2*nrows*ncols];
    int info;

    /* Fill in the matrices with the information */
    int i, j;
    for (i = 0; i < nrows; ++i) {
      for (j = 0; j < ncols; ++j) {
	A[i + j*nrows] = M[i][j];
      }
      alpha[i] = zbar[i];
    }

    dgels_(&trans, &nrows, &ncols, &nrhs, A, &lda, 
	   alpha, &ldb, work, &lwork, &info);
    if (verbose >= 5) fprintf(stderr, "dgels: info = %d\n",  info);

    /* Check the answer to see if it is OK */
    double zchk[2*nFlatOutputs*(1+nFlatDerivs)];
    for (i = 0; i < nrows; ++i) {
      zchk[i] = 0;
      for (j = 0; j < ncols; ++j) {
	zchk[i] += M[i][j] * alpha[j];
      }
    }

    if (verbose >= 8) {
      // Print the problems we are trying to solve
      unsigned int row;
      for (row = 0; row < 2*nFlatOutputs*(1+nFlatDerivs); ++row) {
	fprintf(stderr, "%g = M[%d] = [", zbar[row], row);
	unsigned int col;
	for (col = 0; col < nFlatOutputs*nBasisFunctions; ++col) {
	  fprintf(stderr, "%g ", M[row][col]);
	}
	fprintf(stderr, "] --- chk: %g\n", zchk[row]);
      }

      fprintf(stderr, "alpha = ");
      for (row = 0; row < 2*nBasisFunctions; ++row) {
	fprintf(stderr, "%g ", alpha[row]);
      }
      fprintf(stderr, "\n");
    }

    return info;
}

