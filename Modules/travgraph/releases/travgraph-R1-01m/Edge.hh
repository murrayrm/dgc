/*!**
 * Morlan Lui and Nok Wongpiromsarn
 * December 8, 2006
 */

#ifndef EDGE_HH_
#define EDGE_HH_


// Default edge costs
#define COST_INTERSECTION 500.0f
#define COST_CHANGE_LANE 100.0f
#define COST_UTURN 2000.0f
#define COST_KTURN 3000.0f
#define COST_ZONE 200.0f
#define COST_FIRST_SEGMENT 0.0f
#define COST_STOP_SIGN 300.0f
#define COST_NOT_VISITED 0.0f
#define COST_OBSTRUCTED 600.0f
#define MAX_OBSTRUCTED_LEVEL 3
#define MIN_NUM_LANES_UTURN 3
#define MAX_EDGE_COST 1.0E6f
using namespace std;

class Vertex;

/*! Edge class. Represents an edge of a graph. An edge contains an edge weight
 *  and a vertex pointer to the vertex it is connected to.
 * \brief The Edge class used in the Graph class.
 */
class Edge
{
public:

  enum EdgeType{ INTERSECTION_LEFT_TURN, INTERSECTION_RIGHT_TURN,
		 INTERSECTION_STRAIGHT, ROAD_SEGMENT, 
		 UTURN, KTURN, ZONE, FIRST_SEGMENT, UNKNOWN };

/*! All edges have a vertex pointer to the vertex it is connected to. */
  Edge()
  {
    Edge(0, 0);
  }
  Edge(Vertex* previous, Vertex* next);
  virtual ~Edge();
  
/*! Returns the edge weight of THIS. */
  double getWeight();

/*! Returns the length of the segment corresponding to THIS. */
  double getLength();
  
/*! Returns a vertex pointer to the vertex THIS is connected from */
  Vertex* getPrevious();

/*! Returns a vertex pointer to the vertex THIS is connected to. */
  Vertex* getNext();

/*! Returns the type of THIS . */
  EdgeType getType();

/*! Returns the level of how much this edge is obstructed */
  int getObstructedLevel();

/*! Returns isVisited */
  bool getIsVisited();

/*! Returns the average speed of THIS */
  double getAvgSpeed();
  
/*! Sets the edge weight of THIS. */
  void setWeight(double);

/*! Sets the length of the segment corresponding to THIS. */
  void setLength(double);

/*! Sets the level of how much this edge is obstructed */
  void setObstructedLevel(int);
  
/*! Sets the vertex pointer to the vertex THIS is connected to. */
  void setNext(Vertex* next);

/*! Sets the type of THIS . */
  void setType(EdgeType);

/*! Set isVisited */
  void setIsVisited(bool);

/*! Sets the average speed of THIS */
  void setAvgSpeed(double);
  
/*! Prints the next vertex of THIS. */
  void print();

/*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)
  {
    ar & weight;
    ar & length;
    ar & type;
    ar & next;
    ar & previous;
    ar & obstructedLevel;
    ar & isVisited;
    ar & avgSpeed;
  }
  
private:
  double weight, length;
  EdgeType type;
  Vertex* next;
  Vertex* previous;
  int obstructedLevel;
  bool isVisited;
  double avgSpeed;

  Edge(const Edge& v);
};

#endif /*EDGE_HH_*/
