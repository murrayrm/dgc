/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#ifndef VERTEX_HH_
#define VERTEX_HH_
#define INFINITE_COST_TO_COME 1.0E9f
#include "Edge.hh"
#include <vector>
#include <iostream>
using namespace std;

/*! Vertex class. Represents a vertex of a graph. A vertex contains a
 *  segmentID, laneID, and waypointID. A vertex may contain one or more
 *  edges.
 * \brief The Vertex class used in the Graph class.
 */
class Vertex
{
public:
/*! All vertices have a segmentID, laneID, and waypointID. */
  Vertex()
  {
    Vertex(0, 0, 0, false, false, false, false, 0);
  }
  Vertex(int, int, int);
  Vertex(int, int, int, bool, bool, bool, bool, int);
  virtual ~Vertex();
  
/*! Returns the segmentID of THIS. */
  int getSegmentID();
  
/*! Returns the laneID of THIS. */
  int getLaneID();
  
/*! Returns the waypointID of THIS. */
  int getWaypointID();

/*! Returns whether THIS is an entry point */
  bool isEntry();

/*! Returns whether THIS is an exit point */
  bool isExit();

/*! Returns whether THIS is a checkpoint */
  bool isCheckpoint();

/*! Returns whether THIS is has a stop sign */
  bool isStopSign();

/*! Returns the checkpointID of THIS */
  int getCheckpointID();
  
/*! Returns the vector of edges contained in THIS. */
  vector<Edge*> getEdges();
  
/*! Adds a pointer to an edge to the vector of edges. */
  void addEdge(Edge* edge);

/*! Removes a pointer an edge from the vector of edges. */
  bool removeEdge(Edge* edge);

/*! Removes all edges */
  void removeAllEdges();

/*! For graph search, set the cost to come to THIS. */
  void setCostToCome(float cost);

/*! For graph search, get the cost to come to THIS. */ 
  float getCostToCome();

/*! For graph search, set the vertex that is to be visited before THIS. */
  void setPreviousVertex(Vertex* previous);

/*! For graph search, get the vertex that is to be visited before THIS. */
  Vertex* getPreviousVertex();

/*! Prints the segmentID, laneID, and waypointID of THIS. */
  void print();

/*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)
  {
    ar & segmentID;
    ar & laneID;
    ar & waypointID;
    ar & entry;
    ar & exit;
    ar & checkpoint;
    ar & stopSign;
    ar & checkpointID;
    ar & edges;
    ar & costToCome;
    ar & previousVertex;
  }
  

private:
  int segmentID, laneID, waypointID;
  bool entry, exit, checkpoint, stopSign;
  int checkpointID;
  vector<Edge*> edges;

  // The following variables are used for graph search
  float costToCome;
  Vertex* previousVertex;

  Vertex(const Vertex& v);
};

#endif /*VERTEX_HH_*/
