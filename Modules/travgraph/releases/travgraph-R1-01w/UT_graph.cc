#include <iostream>
#include "rndf/RNDF.hh"
#include "Graph.hh"
#include "dgcutils/ggis.h"

using namespace std;

int main(int argc, char **argv) 
{
  char* rndfFileName;
  if(argc >1)
    rndfFileName = argv[1];
  else {
    cerr << "ERROR: No rndf specified" << endl;
    exit(1);
  }

  RNDF* rndf = new RNDF();
  if (!rndf->loadFile(rndfFileName))
  {
    cerr << "ERROR:  Unable to load RNDF file " << endl;
    exit(1);
  }
  rndf->assignLaneDirection();
  Graph* graph = new Graph(rndf);
  graph->print();

  delete rndf;
  delete graph;
}
