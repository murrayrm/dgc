/*!**
 * Morlan Liu and Nok Wongpiromsarn
 * December 8, 2006
 */

#include "Graph.hh"
#include <math.h>
#include <algorithm>
#define DEBUG_LEVEL 0
#define PI 3.14159265
#define UTURN_DISTANCE 22.0f

struct IntersectionTurnAngle
{
  GPSPoint* point;
  double angle;
  IntersectionTurnAngle() {
    point = NULL;
    angle = 0;
  }
};

bool operator < (const IntersectionTurnAngle& a, const IntersectionTurnAngle& b)
{
  return a.angle < b.angle;
}

using namespace std;

/* Debugging code. Hopefully we don't need to use it again.
static const void* small_ptr = (void*) 0x8000000;
static void check_graph(Graph* g)
{
  const vector<Vertex*>& vertices = g->getVertices();
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    if (vertices[i] < (Vertex*)small_ptr)
      abort();
    const vector<Edge*>& edges = vertices[i]->getEdges();
    for(unsigned j = 0; j < edges.size(); j++)
    {
      if (edges[j] <  (Edge*)small_ptr)
	abort();
      if (edges[j]->getNext() < (Vertex*)small_ptr)
	abort();
    }
  }
}
*/

Graph::Graph()
{
  vertices.clear();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
Graph::Graph(RNDF* rndf)
{
  //addAllVertices(rndf);
  //addAllEdges(rndf);
  init(rndf);
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
/*
Graph& Graph::operator=(const Graph& g)
{ 
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    delete vertices[i];
  }
  
  vertices.resize(g.vertices.size());
  for (unsigned i = 0; i < g.vertices.size(); i++)
  {
    vertices[i] = new Vertex(*g.vertices[i]);
  }
  return *this;
}
*/

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
Graph::~Graph()
{
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    delete vertices[i];
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void Graph::init(RNDF* rndf)
{
  numOfSegments = rndf->getNumOfSegments();
  numOfZones = rndf->getNumOfZones();
  m_rndf = rndf;
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    delete vertices[i];
  }
  vertices.clear();
  addAllVertices(rndf);
  addAllEdges(rndf);  
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void Graph::setRNDF(RNDF* rndf)
{
  m_rndf = rndf;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
Vertex* Graph::getVertex(int segmentID, int laneID, int waypointID)
{
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* temp = vertices[i];
    
    if(temp->getSegmentID() == segmentID &&
        temp->getLaneID() == laneID &&
        temp->getWaypointID() == waypointID)
    {
      return temp;
    }
  }
  
  return NULL;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<Vertex*> Graph::getVertices()
{
  return vertices;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
int Graph::getNumOfVertices()
{
  return vertices.size();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
int Graph::getNumOfSegments()
{
  return numOfSegments;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
int Graph::getNumOfZones()
{
  return numOfZones;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool Graph::addVertex(int segmentID, int laneID, int waypointID)
{
  return addVertex(segmentID, laneID, waypointID, false, false, false, false, 0);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool Graph::addVertex(int segmentID, int laneID, int waypointID, bool entry, bool exit, 
	       bool checkpoint, bool stopSign, int checkpointID)
{
  //  check_graph(this); // REMOVE ME
  if(getVertex(segmentID, laneID, waypointID) != NULL)
  {
    return false;
  }
  
  vertices.push_back(new Vertex(segmentID, laneID, waypointID, entry, exit, 
				checkpoint, stopSign, checkpointID));
  
  // check_graph(this); // REMOVE ME
  return true;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add a vertex corresponding to current position (with segmentID = 0, laneID = 0
// and waypointID = 0) to the graph with appropriate edges
//-------------------------------------------------------------------------------------------
bool Graph::addCurrentPosition(double utmNorthing, double utmEasting, double utmYaw, RNDF* rndf,
			       double maxHeadingDiff, double turningRadius, double maxHeadingLaneDir, 
			       double maxDistance, double& headingDiff, double& headingLaneDir,
			       bool uturnAllowed)
{  
  double distance;
  bool currentPointAdded;
  GPSPoint* closestGPSPoint;
  closestGPSPoint = rndf->findClosestGPSPoint(utmNorthing, utmEasting,
					      0, 0, distance, true, utmYaw, 
					      maxHeadingDiff, turningRadius, maxHeadingLaneDir, 
					      headingDiff, headingLaneDir);
    
  while (distance > maxDistance && maxHeadingDiff < 2*PI)
  {
    closestGPSPoint = rndf->findClosestGPSPoint(utmNorthing, utmEasting,
						0, 0, distance, true, utmYaw, 
						maxHeadingDiff, turningRadius, maxHeadingLaneDir, 
						headingDiff, headingLaneDir);
    maxHeadingDiff += maxHeadingDiff/3;
    maxHeadingLaneDir += maxHeadingLaneDir/5;
  }
  
  removeVertex(0, 0, 0);
  
  addGPSPointToGraph(closestGPSPoint, rndf);
  currentPointAdded = addVertex(0, 0, 0);
  if (currentPointAdded)
  {
    double length = computeDistance(utmNorthing, utmEasting,
				closestGPSPoint->getNorthing(), closestGPSPoint->getEasting());
    addEdge(0, 0, 0, closestGPSPoint->getSegmentID(), closestGPSPoint->getLaneID(), 
	    closestGPSPoint->getWaypointID(), length, 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT), Edge::FIRST_SEGMENT);
  }
  
  if (!uturnAllowed || closestGPSPoint->getSegmentID() > rndf->getNumOfSegments())
    return currentPointAdded;

  else
  {
    if ( closestGPSPoint->getWaypointID() > 1 && closestGPSPoint->getWaypointID() < 
	 rndf->getLane(closestGPSPoint->getSegmentID(), closestGPSPoint->getLaneID())->getNumOfWaypoints() )
    {
      GPSPoint* prevWaypoint = rndf->getWaypoint(closestGPSPoint->getSegmentID(), closestGPSPoint->getLaneID(), 
						 closestGPSPoint->getWaypointID() - 1);
      if ( ( closestGPSPoint->getNorthing() - prevWaypoint->getNorthing()) * 
	   ( utmNorthing  - closestGPSPoint->getNorthing()) + 
	   ( closestGPSPoint->getEasting() - prevWaypoint->getEasting()) * 
	   ( utmEasting  - closestGPSPoint->getEasting()) < 0 )
      {
	closestGPSPoint = prevWaypoint;
      }
    }
    return addUturnEdges(utmNorthing, utmEasting, getVertex(0,0,0), closestGPSPoint, rndf);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add a vertex corresponding to the specified GPSPoint (can be waypoint or perimeter point)
// to the graph with appropriate edges
//-------------------------------------------------------------------------------------------
bool Graph::addGPSPointToGraph (GPSPoint* point, RNDF* rndf)
{
  int segmentID = point->getSegmentID();
  int laneID = point->getLaneID();
  int waypointID = point->getWaypointID();
  return addGPSPointToGraph(point, rndf, segmentID, laneID, waypointID);
}


bool Graph::addGPSPointToGraph (GPSPoint* point, RNDF* rndf, int newSegmentID, 
				int newLaneID, int newWaypointID)
{
  int segmentID = point->getSegmentID();
  int laneID = point->getLaneID();
  int waypointID = point->getWaypointID(); 
  bool entry = point->isEntry();
  bool exit = point->isExit();
  bool checkpoint = false;
  bool stopSign = false;
  int checkpointID = 0;

  if (laneID != 0)
  {
    Waypoint* waypoint = rndf->getWaypoint(segmentID, laneID, waypointID);
    checkpoint = waypoint->isCheckpoint();
    stopSign = waypoint->isStopSign();
    checkpointID = waypoint->getCheckpointID();
  }

  if (!addVertex(newSegmentID, newLaneID, newWaypointID, entry, exit, checkpoint, stopSign, checkpointID))
  {
    return false;
  }

  // In the case we're in a zone.
  if (segmentID > rndf->getNumOfSegments())
  {
    addEdgesFromEntryPerimeter(rndf, point, newSegmentID, newLaneID, newWaypointID );
  }

  // In the case we're on a segment
  else
  {
    // Add edges to all exit points both in the same lane and in adjacent,
    // same direction lanes
    addEdgesFromEntry(rndf, point, newSegmentID, newLaneID, newWaypointID);
    
    // Add edges to checkpoints
    int numOfWaypoints = rndf->getLane(segmentID, laneID)->getNumOfWaypoints();

    // Adding edges from this entry waypoint to its next checkpoint waypoint in the same lane.
    bool ckptFound = false;
    int j = waypointID + 1;
    GPSPoint* previousWaypoint = point;
    double length = 0;
    while(!ckptFound && j <= numOfWaypoints)
    {
      Waypoint* ckpt = rndf->getWaypoint(segmentID, laneID, j);
      length += computeDistance(previousWaypoint->getNorthing(), previousWaypoint->getEasting(),
				ckpt->getNorthing(), ckpt->getEasting());
      if(ckpt->isCheckpoint())
      {
	double weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT);
	if (!addEdge(newSegmentID, newLaneID, newWaypointID, segmentID, laneID, j, length, 
		     weight, Edge::ROAD_SEGMENT))
	{
	  if (DEBUG_LEVEL > 3)
	  {
	    cout << "Cannot add edge from ";	  
	    cout << newSegmentID << "." << newLaneID << "." << newWaypointID << "(";
	    point->print();
	    cout << ") to ";
	    ckpt->print();
	    cout << endl;
	  }
	}
	ckptFound = true;
      }
      else if (ckpt->isExit())
      {
	ckptFound = true; // Do not add edge if the closest exit point is closer than
	                  // the closest checkpoint	
      }
      j++;
      previousWaypoint = ckpt;
    }
    
    // Update obstructed level
    Vertex* addedVertex = getVertex(newSegmentID, newLaneID, newWaypointID);
    vector<Edge*> addedEdges = addedVertex->getEdges();
    vector<Vertex*> allVertices = getVertices();
    for (unsigned i=0; i < allVertices.size(); i++ ) {
      vector<Edge*> allEdges = allVertices[i]->getEdges();
      for (unsigned j=0; j < allEdges.size(); j++) {
	if (allEdges[j]->getObstructedLevel() > 0 &&
	    allEdges[j]->getNext()->getSegmentID() == newSegmentID &&
	    allEdges[j]->getNext()->getLaneID() == newLaneID &&
	    allEdges[j]->getNext()->getWaypointID() >= newWaypointID ) {
	  for (unsigned k=0; k < addedEdges.size(); k++) {
	    if (addedEdges[k]->getObstructedLevel() < allEdges[j]->getObstructedLevel()) {
	      addedEdges[k]->setObstructedLevel(allEdges[j]->getObstructedLevel());
	    }
	  }
	}
      }
    }
  }
  return true;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool Graph::removeVertex(int segmentID, int laneID, int waypointID)
{
  Vertex* vertex = getVertex(segmentID, laneID, waypointID);
  if(vertex == NULL)
  {
    return false;
  }
    
  bool vertexFound = false;
  unsigned i = 0;
  while (!vertexFound && i < vertices.size())
  {
    if(vertices[i] == vertex)
    {
      vertexFound = true;
      vertices.erase(vertices.begin()+i,vertices.begin()+i+1);
      delete vertex;
    }
    i++;
  }
  
  // check_graph(this); // REMOVE ME
  return vertexFound;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------  
Edge* Graph::getEdge(Vertex* vertex1, Vertex* vertex2)
{  
  if(vertex1 == NULL || vertex2 == NULL)
    return NULL;

  vector<Edge*> edges = vertex1->getEdges();
      
  unsigned i = 0;
  while (i < edges.size())
  {
    if((edges[i])->getNext() == vertex2)
    {
      return edges[i];
    }
    i++;
  }
  
  return NULL;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
Edge* Graph::getEdge(int segmentID1, int laneID1, int waypointID1,
  int segmentID2, int laneID2, int waypointID2)
{
  Vertex* vertex1 = getVertex(segmentID1, laneID1, waypointID1);    
  Vertex* vertex2 = getVertex(segmentID2, laneID2, waypointID2);
  return getEdge(vertex1, vertex2);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool Graph::addEdge(int segmentID1, int laneID1, int waypointID1,
  int segmentID2, int laneID2, int waypointID2)
{
  cerr << __FILE__ << ":" << __LINE__ << " warning: Edge cost not specified" << endl;
  return addEdge(segmentID1, laneID1, waypointID1,
		 segmentID2, laneID2, waypointID2, MAX_EDGE_COST, MAX_EDGE_COST);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool Graph::addEdge(int segmentID1, int laneID1, int waypointID1,
  int segmentID2, int laneID2, int waypointID2, double length, double weight)
{
  cerr << __FILE__ << ":" << __LINE__ << " warning: Edge type not specified" << endl;
  return addEdge(segmentID1, laneID1, waypointID1, segmentID2, laneID2, waypointID2, 
		 length, weight, Edge::UNKNOWN);
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool Graph::addEdge(int segmentID1, int laneID1, int waypointID1,
  int segmentID2, int laneID2, int waypointID2, double length, double weight, Edge::EdgeType type, 
  double avgSpeed, bool isVisited)
{
  // check_graph(this); // REMOVE ME
  Vertex* vertex1 = getVertex(segmentID1, laneID1, waypointID1);
  
  if(vertex1 == NULL)
  {
    return false;
  }
  
  Vertex* vertex2 = getVertex(segmentID2, laneID2, waypointID2);
  
  if(vertex2 == NULL)
  {
    return false;
  }

  if (getEdge(segmentID1, laneID1, waypointID1, segmentID2, laneID2, waypointID2) == NULL)
  {
    Edge* edge = new Edge(vertex1, vertex2);
    edge->setType(type);
    vertex1->addEdge(edge);
    edge->setLength(length);
    edge->setWeight(weight);
    edge->setAvgSpeed(avgSpeed);
    edge->setIsVisited(isVisited);

    if (DEBUG_LEVEL > 0)
    {
      cout << "Added edge from " << segmentID1 << "." << laneID1 << "." << waypointID1 
           << " to " << segmentID2 << "." << laneID2 << "." << waypointID2 << endl;
    }
          
    // check_graph(this); // REMOVE ME
    return true;
  }
  else
  {
    if (DEBUG_LEVEL > 0)
    {
      cout << "Cannot add edge from " << segmentID1 << "." << laneID1 << "." << waypointID1 
	         << " to " << segmentID2 << "." << laneID2 << "." << waypointID2
           << ". This edge already exists."<< endl;
    }
    // check_graph(this); // REMOVE ME
    return false;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool Graph::addEdge(Edge* edge)
{
  // check_graph(this); // REMOVE ME
  Vertex* vertex1 = edge->getPrevious();
  Vertex* vertex2 = edge->getNext();

  if (getEdge(vertex1, vertex2) == NULL)
  {
    vertex1->addEdge(edge);

    if (DEBUG_LEVEL > 0)
    {
      cout << "Added edge from " << vertex1->getSegmentID() << "."
           << vertex1->getLaneID() << "." << vertex1->getWaypointID() 
           << " to " << vertex2->getSegmentID() << "." << vertex2->getLaneID() << "."
           << vertex2->getWaypointID() << endl;
    }
          
    // check_graph(this); // REMOVE ME
    return true;
  }
  else
  {
    if (DEBUG_LEVEL > 0)
    {
      cout << "Cannot add edge from " << vertex1->getSegmentID() << "."
           << vertex1->getLaneID() << "." << vertex1->getWaypointID()
           << " to " << vertex2->getSegmentID() << "." << vertex2->getLaneID()
           << "." << vertex2->getWaypointID() << ". This edge already exists."<< endl;
    }
    // check_graph(this); // REMOVE ME
    return false;
  }  
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
/*! Adds all the edges for uturn starting from the given vertex */
bool Graph::addUturnEdges(double utmNorthing, double utmEasting, Vertex* vertex1, GPSPoint* closestGPSPoint, RNDF* rndf,
			  bool validUturnPointOnly)
{
  if (closestGPSPoint->getSegmentID() > rndf->getNumOfSegments() || closestGPSPoint->getSegmentID() <= 0)
    return false;

  vector<Lane*> nonAdjLanes = rndf->getNonAdjacentLanes(rndf->getLane(closestGPSPoint->getSegmentID(),
								      closestGPSPoint->getLaneID()));
  unsigned nonAdjLaneIndex = 0;
  while (nonAdjLaneIndex < nonAdjLanes.size())
  {
    double length, weight;
    GPSPoint* uturnGPSPoint = rndf->findClosestGPSPoint(closestGPSPoint->getNorthing(),
							closestGPSPoint->getEasting(), 
							closestGPSPoint->getSegmentID(), 
							nonAdjLanes[nonAdjLaneIndex]->getLaneID(), length);

    if (validUturnPointOnly) {
      int numOfWaypoints = nonAdjLanes[nonAdjLaneIndex]->getNumOfWaypoints();
      if (closestGPSPoint->getWaypointID() > 1)
      {
	closestGPSPoint = rndf->getWaypoint(closestGPSPoint->getSegmentID(), closestGPSPoint->getLaneID(),
					    closestGPSPoint->getWaypointID() - 1);
      }
      double normFact = computeDistance(closestGPSPoint->getNorthing(), closestGPSPoint->getEasting(),
					utmNorthing, utmEasting);
      if (normFact == 0)
      {
	return false;
      }
      
      while (uturnGPSPoint->getWaypointID() < numOfWaypoints && (1/normFact)*
	     ((closestGPSPoint->getNorthing() - utmNorthing)*(uturnGPSPoint->getNorthing() - utmNorthing) + 
	      (closestGPSPoint->getEasting() - utmEasting)*(uturnGPSPoint->getEasting() - utmEasting)) < UTURN_DISTANCE )
      {
      uturnGPSPoint = rndf->getWaypoint(uturnGPSPoint->getSegmentID(), uturnGPSPoint->getLaneID(), 
					uturnGPSPoint->getWaypointID() + 1);
      }
    }

    addGPSPointToGraph(uturnGPSPoint, rndf);

    bool edgeAdded;
    if (abs(nonAdjLanes[nonAdjLaneIndex]->getLaneID()-closestGPSPoint->getLaneID()) < MIN_NUM_LANES_UTURN)
    {
      weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT) + COST_KTURN;
      edgeAdded = addEdge(vertex1->getSegmentID(), vertex1->getLaneID(), vertex1->getWaypointID(), 
	      uturnGPSPoint->getSegmentID(), uturnGPSPoint->getLaneID(), 
	      uturnGPSPoint->getWaypointID(), length, weight, Edge::KTURN);
    }
    else
    {
      weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT) + COST_UTURN;
      edgeAdded = addEdge(vertex1->getSegmentID(), vertex1->getLaneID(), vertex1->getWaypointID(),
	      uturnGPSPoint->getSegmentID(), uturnGPSPoint->getLaneID(), 
	      uturnGPSPoint->getWaypointID(), length, weight, Edge::UTURN);
    }
    nonAdjLaneIndex++;
  }

  return true;
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool Graph::removeEdge(int segmentID1, int laneID1, int waypointID1,
  int segmentID2, int laneID2, int waypointID2)
{
  // check_graph(this); // REMOVE ME
  Vertex* vertex1 = getVertex(segmentID1, laneID1, waypointID1);
  
  if(vertex1 == NULL)
    return false;

  Edge* edge = getEdge(segmentID1, laneID1, waypointID1, segmentID2, laneID2, waypointID2);
  if(edge == NULL)
  {
    return false;
  }

  // check_graph(this); // REMOVE ME
  return vertex1->removeEdge(edge);
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool Graph::removeEdge(Edge* edge)
{
  Vertex* vertex1 = edge->getPrevious();
  
  if(vertex1 == NULL)
    return false;

  if(edge == NULL)
  {
    return false;
  }

  return vertex1->removeEdge(edge);  
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void Graph::addObstacle(int segmentID, int laneID, int waypointID)
{
  Vertex* vertex = getVertex(segmentID, laneID, waypointID);
  while (vertex == NULL && waypointID > 1)
  {
    waypointID--;
    vertex = getVertex(segmentID, laneID, waypointID);
  }
  
  if (vertex != NULL) {
    bool entryFound = vertex->isEntry();
    vector<Edge*> edges = vertex->getEdges();
    for (unsigned i = 0; i < edges.size(); i++)
      edges[i]->setObstructedLevel(MAX_OBSTRUCTED_LEVEL);
    
    while (!entryFound && waypointID > 1) {
      entryFound = vertex->isEntry();
      vector<Edge*> edges = vertex->getEdges();
      for (unsigned i = 0; i < edges.size(); i++)
	edges[i]->setObstructedLevel(MAX_OBSTRUCTED_LEVEL);
      waypointID--;
      vertex = getVertex(segmentID, laneID, waypointID);
      while(vertex == NULL && waypointID > 1) {
	waypointID--;
	vertex = getVertex(segmentID, laneID, waypointID);
      }
    }
  }
  else {
    Waypoint* waypoint = m_rndf->getWaypoint(segmentID, laneID, 1);
    if (waypoint == NULL) {
      cerr << "ERROR: Graph::addObstacle: Can't find waypoint " 
	   << segmentID << "." << laneID << ".1" << endl;
      return;
    }
    addVertex(segmentID, laneID, 1, waypoint->isEntry(), waypoint->isExit(), waypoint->isCheckpoint(), 
	      waypoint->isStopSign(), waypoint->getCheckpointID());
    addEdgesFromEntry(m_rndf, waypoint);
    Vertex* vertex = getVertex(segmentID, laneID, 1);
    vector<Edge*> edges = vertex->getEdges();
    for (unsigned i=0; i < edges.size(); i++)
      edges[i]->setObstructedLevel(MAX_OBSTRUCTED_LEVEL);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void Graph::updateEdgeCost(vector<double> cost, vector<double> avgSpeed)
{
  if (cost.size() != 11)
    {
      cerr << "ERROR: Graph::updateEdgeCost: cost.size() = " << cost.size() << endl;
      return;
    }

  double costIntersectionLeftTurn = cost[0]; 
  if (costIntersectionLeftTurn <= 0)
    costIntersectionLeftTurn = INFINITE_COST_TO_COME;
  double costIntersectionRightTurn = cost[1];
  if (costIntersectionRightTurn <= 0)
    costIntersectionRightTurn = INFINITE_COST_TO_COME;
  double costIntersectionStraight = cost[2]; 
  if (costIntersectionStraight <= 0)
    costIntersectionStraight = INFINITE_COST_TO_COME;
  double costChangeLane = cost[3];
  if (costChangeLane <= 0)
    costChangeLane = INFINITE_COST_TO_COME;
  double costUturn = cost[4]; 
  if (costUturn <= 0)
    costUturn = INFINITE_COST_TO_COME;
  double costKturn = cost[5]; 
  if (costKturn <= 0)
    costKturn = INFINITE_COST_TO_COME;
  double costZone = cost[6];
  if (costZone <= 0)
    costZone = INFINITE_COST_TO_COME;
  double costFirstSegment = cost[7];
  if (costFirstSegment < 0)
    costFirstSegment = INFINITE_COST_TO_COME;
  double costStopSign = cost[8];
  if (costStopSign <= 0)
    costStopSign = INFINITE_COST_TO_COME;
  double costNotVisited = cost[9];
  if (costNotVisited < 0)
    costNotVisited = INFINITE_COST_TO_COME;
  double costObstructed = cost[10]; 
  if (costObstructed <= 0)
    costObstructed = INFINITE_COST_TO_COME;
      
  for (unsigned i = 0; i < vertices.size(); i++)
    {
      vector<Edge*> edges = vertices[i]->getEdges();
      for (unsigned j = 0; j < edges.size(); j++)
	{
	  double weight;
	  double avgSpeed1, avgSpeed2;
	  int segmentID1 = edges[j]->getPrevious()->getSegmentID();
	  int segmentID2 = edges[j]->getNext()->getSegmentID();
	  if ((unsigned)segmentID1 < avgSpeed.size())
	    {
	      avgSpeed1 = avgSpeed[segmentID1];
	    }
	  else
	    {
	      cerr << "ERROR: Graph::updateEdgeCost: segmentID1 = " << segmentID1 << " avgSpeed.size() = "
		   << avgSpeed.size() << endl;
	      avgSpeed1 = avgSpeed[0];
	    }
	  if ((unsigned)segmentID2 < avgSpeed.size())
	    {
	      avgSpeed2 = avgSpeed[segmentID2]; 
	    } 
	  else 
	    {
	      cerr << "ERROR: Graph::updateEdgeCost: segmentID1 = " << segmentID1 << " avgSpeed.size() = "
		   << avgSpeed.size() << endl;
	      avgSpeed2 = avgSpeed[0];
	    } 
	  if (avgSpeed1 > 0 && avgSpeed2 > 0) 
	    { 
	      weight = 2*edges[j]->getLength()/(avgSpeed1 + avgSpeed2); 
	    } 
	  else 
	    { 
	      weight = MAX_EDGE_COST; 
	    } 

	  Edge::EdgeType edgeType = edges[j]->getType();
	  switch(edgeType)
	    {
	    case Edge::INTERSECTION_LEFT_TURN:
	      weight += costIntersectionLeftTurn;
	      break;
	    case Edge::INTERSECTION_RIGHT_TURN:
	      weight += costIntersectionRightTurn;
	      break;
	    case Edge::INTERSECTION_STRAIGHT:
	      weight += costIntersectionStraight;
	      break;
	    case Edge::ROAD_SEGMENT:
	      weight += costChangeLane * abs(edges[j]->getPrevious()->getLaneID() - 
					     edges[j]->getNext()->getLaneID());
	      break;
	    case Edge::UTURN:
	      weight += costUturn;
	      break;
	    case Edge::KTURN:
	      weight += costKturn;
	      break;
	    case Edge::ZONE:
	      weight += costZone;
	      break;
	    case Edge::FIRST_SEGMENT:
	      weight += costFirstSegment;
	      break;
	    default:
	      weight += MAX_EDGE_COST;
	      break;
	    }
	  if ( (edgeType == Edge::INTERSECTION_LEFT_TURN || edgeType == Edge::INTERSECTION_RIGHT_TURN
	      || edgeType == Edge::INTERSECTION_STRAIGHT) && edges[j]->getPrevious()->isStopSign())
	    {
	      weight += costStopSign;
	    }
	  if (!edges[j]->getIsVisited())
	    {
	      weight += costNotVisited;
	    }
	  weight += edges[j]->getObstructedLevel() * costObstructed;
	  edges[j]->setWeight(weight);
	}
    }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void Graph::print()
{
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    vertices[i]->print();
    vector<Edge*> edges = vertices[i]->getEdges();
    cout << "  -> ";
    for(unsigned j = 0; j < edges.size(); j++)
    {
      edges[j]->getNext()->print();
      edges[j]->print();
      printf(", \t");
    }
    cout << endl;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void Graph::log(FILE* file)
{
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    fprintf (file, "%d.%d.%d (", vertices[i]->getSegmentID(), vertices[i]->getLaneID(), vertices[i]->getWaypointID());
    vector<Edge*> edges = vertices[i]->getEdges();
    if (vertices[i]->isEntry())
    {
      fprintf(file, "entry ");
    }
    if (vertices[i]->isExit())
    {
      fprintf(file, "exit ");
    }
    if (vertices[i]->isCheckpoint())
    { 
      fprintf(file, "checkpoint ");
    }
    if (vertices[i]->isStopSign())
    {
      fprintf(file, "stop ");
    }
    fprintf (file, ")");

    fprintf (file, "  -> ");
    for(unsigned j = 0; j < edges.size(); j++)
    {
      fprintf(file, "%d.%d.%d", edges[j]->getNext()->getSegmentID(), edges[j]->getNext()->getLaneID(),
	      edges[j]->getNext()->getWaypointID());
      
      fprintf(file, "(");
      if (edges[j]->getType() == Edge::INTERSECTION_LEFT_TURN)
      {
	fprintf( file, "intersection left turn");
      }
      else if ( edges[j]->getType() == Edge::INTERSECTION_RIGHT_TURN )
      {
	fprintf( file, "intersection right turn");
      }
      else if ( edges[j]->getType() == Edge::INTERSECTION_STRAIGHT )
      {
	fprintf( file, "intersection straight");
      }
      else if (edges[j]->getType() == Edge::ROAD_SEGMENT)
      {
	fprintf( file, "road segment");
      }
      else if (edges[j]->getType() == Edge::UTURN || edges[j]->getType() == Edge::KTURN)
      {
	fprintf( file, "uturn" );
      }
      else if (edges[j]->getType() == Edge::ZONE)
      {
	fprintf( file, "zone" );
      }
      else if (edges[j]->getType() == Edge::FIRST_SEGMENT)
      {
	fprintf( file, "first segment" );
      }
      else
      {
	fprintf (file, "unknown");
      }
      fprintf (file, ", %f, %f, %d)", edges[j]->getLength(), edges[j]->getWeight(), edges[j]->getObstructedLevel());
      fprintf(file, ", \t");
    }
    fprintf(file, "\n");
  }  
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add the following vertices to this graph
// * exit points
// * entry points
// * checkpoints
// * entry perimeter points
// * exit perimeter points
// * parking spot waypoints
//-------------------------------------------------------------------------------------------
void Graph::addAllVertices(RNDF* rndf)
{
  int numOfSegments = rndf->getNumOfSegments();
  int numOfZones = rndf->getNumOfZones();
  
  // Adding all entry, exit, and checkpoint waypoints to the graph.

  for(int i = 1; i <= numOfSegments; i++)
  {    
    for(int j = 1; j <= rndf->getSegment(i)->getNumOfLanes(); j++)
    {
      for(int k = 1; k <= rndf->getLane(i, j)->getNumOfWaypoints(); k++)
      {
        Waypoint* waypoint = rndf->getWaypoint(i, j, k);
        
        if(waypoint->isEntry() || waypoint->isExit() || waypoint->isCheckpoint())
        {
          addVertex(i, j, k, waypoint->isEntry(), waypoint->isExit(), waypoint->isCheckpoint(), 
		    waypoint->isStopSign(), waypoint->getCheckpointID());
        }
      }
    }    
  }
  
  // Adding all entry and exit perimeter points and parking spot waypoints.
  
  for(int i = numOfSegments + 1; i <= numOfSegments + numOfZones; i++)
  {
    
    // Adding all entry and exit perimeter points.
    
    for(int j = 1; j <= rndf->getZone(i)->getNumOfPerimeterPoints(); j++)
    {
      PerimeterPoint* perimeterPoint = rndf->getPerimeterPoint(i, j);
      
      if(perimeterPoint->isEntry() || perimeterPoint->isExit())
      {
        addVertex(i, 0, j, perimeterPoint->isEntry(), perimeterPoint->isExit(), 
		  false, false, 0);
      }
    }
    
    // Adding all parking spot checkpoints.
    
    for(int j = 1; j <= rndf->getZone(i)->getNumOfSpots(); j++)
    {
      Waypoint* waypoint = rndf->getWaypoint(i, j, 1);
          
      if(waypoint->isCheckpoint())
      {
	addVertex(i, j, 1, waypoint->isEntry(), waypoint->isExit(), waypoint->isCheckpoint(), 
		  waypoint->isStopSign(), waypoint->getCheckpointID());
      }
        
      waypoint = rndf->getWaypoint(i, j, 2);
        
      if(waypoint->isCheckpoint())
      {
        addVertex(i, j, 2, waypoint->isEntry(), waypoint->isExit(), waypoint->isCheckpoint(), 
		  waypoint->isStopSign(), waypoint->getCheckpointID());
      }
    }
  }
  
  if (DEBUG_LEVEL > 0)
  {
    cout << "Vertices added to graph: " << endl;
    print();
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add the following edges to this graph
// * exit point -> entry points
// * entry point -> closest exit point
// * closest entry points -> checkpoint
// * checkpoint -> closest exit point
// * checkpoint -> other checkpoints before closest exit point
// * parking spot -> exit points
// * entry perimeter point -> exit perimeter points
// * entry perimenter point -> parking spots
//-------------------------------------------------------------------------------------------
void Graph::addAllEdges(RNDF* rndf)
{  
  /* Adding all edges from exit waypoints. */  
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    GPSPoint* exit;
    vector<GPSPoint*> entries;
    vector<IntersectionTurnAngle> intTurnAngles;
    
    int exitSegmentID = vertex->getSegmentID();
    int exitLaneID = vertex->getLaneID();
    int exitWaypointID = vertex->getWaypointID();
    
    if(exitLaneID == 0)
      exit = rndf->getPerimeterPoint(exitSegmentID, exitWaypointID);
    else
      exit = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID);
        
    if(!exit->isExit())
      continue;
    else
      entries = exit->getEntryPoints();

    for(unsigned j = 0; j < entries.size(); j++) {
      GPSPoint* entry = entries[j];
      IntersectionTurnAngle tmpIntTurnAng;
      tmpIntTurnAng.point = entry;
      double angle = atan2(entry->getEasting() - exit->getEasting(), entry->getNorthing() - exit->getNorthing());
      if (exit->getWaypointID() > 1) {
	GPSPoint* prevPoint;
	if (exitLaneID == 0)
	  prevPoint = rndf->getPerimeterPoint(exitSegmentID, exitWaypointID - 1);
	else
	  prevPoint = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID - 1);
	angle -= atan2(exit->getEasting() - prevPoint->getEasting(), exit->getNorthing() - prevPoint->getNorthing());
	while (angle < -PI) 
	  angle += 2*PI;
	while (angle > PI)
	  angle -= 2*PI;
      }
      tmpIntTurnAng.angle = angle;
      intTurnAngles.push_back(tmpIntTurnAng);
    }

    sort(intTurnAngles.begin(), intTurnAngles.end());

    // Adding all edges from exit waypoints to their entry waypoints.
    for(unsigned j = 0; j < entries.size(); j++)
    {
      GPSPoint* entry = entries[j];
      double length = computeDistance(entry->getNorthing(), entry->getEasting(),
          exit->getNorthing(), exit->getEasting());
      double weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT);
      double direction1, direction2;
      bool isUturn = false;

      if (exitSegmentID <= rndf->getNumOfSegments() &&
	  exitSegmentID == entry->getSegmentID() && rndf->getLane(exitSegmentID, exitLaneID)->getDirection()
	  != rndf->getLane(exitSegmentID, entry->getLaneID())->getDirection()) {
	if (exitWaypointID > 1) {
	  GPSPoint* prevPoint = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID - 1);
	  direction1 = atan2(exit->getEasting() - prevPoint->getEasting(), 
			     exit->getNorthing() - prevPoint->getNorthing());
	}
	else if (exitWaypointID < rndf->getLane(exitSegmentID, exitLaneID)->getNumOfWaypoints() ) {
	  GPSPoint* nextPoint = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID + 1);
	  direction1 = atan2(nextPoint->getEasting() - exit->getEasting(), 
			     nextPoint->getNorthing() - exit->getNorthing());
	}
	else
	  break;
	if (entry->getWaypointID() < rndf->getLane(entry->getSegmentID(), 
						   entry->getLaneID())->getNumOfWaypoints()) {
	  GPSPoint* nextPoint = rndf->getWaypoint(entry->getSegmentID(), entry->getLaneID(), 
						  entry->getWaypointID() + 1);
	  direction2 = atan2(nextPoint->getEasting() - entry->getEasting(), 
			     nextPoint->getNorthing() - entry->getNorthing());
	}
	else if (entry->getWaypointID() > 1) {
	  GPSPoint* prevPoint = rndf->getWaypoint(entry->getSegmentID(), entry->getLaneID(), 
						  entry->getWaypointID() - 1);
	  direction2 = atan2(entry->getEasting() - prevPoint->getEasting(), 
			     entry->getNorthing() - prevPoint->getNorthing());
	}
	else
	  break;

#       warning need to check logic for uturn more carefully
	double directionDiff = fmod(fabs(direction1 - direction2), 2*PI);
	if (directionDiff > PI)
	  directionDiff -= 2*PI;
	directionDiff = fabs(directionDiff);

	if (directionDiff > 2*PI/3) {
	  isUturn = true;
	}
      }
      
      if (isUturn)
      {
	bool edgeAdded;
	if (abs(entry->getLaneID()-exitLaneID) < MIN_NUM_LANES_UTURN)
	{
	  weight += COST_KTURN;
	  edgeAdded = addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
			      entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID(), 
			      length, weight, Edge::KTURN);
	}
	else
	{
	  weight += COST_UTURN;
	  edgeAdded = addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
			      entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID(), 
			      length, weight, Edge::UTURN);
	}
	if (!edgeAdded && DEBUG_LEVEL > 3)
	{
	  cout << "Cannot add edge from ";
	  exit->print();
	  cout << " to ";
	  entry->print();
	  cout << endl;
	}
      }
      else
      {
	if (exitLaneID != 0)
	{
	  weight += COST_INTERSECTION;
	  if (rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID)->isStopSign())
	  {
	    weight += COST_STOP_SIGN;
	  }
	}
	Edge::EdgeType edgeType = Edge::INTERSECTION_STRAIGHT;

	// Determine the type of intersection
	if (entries.size() >= 2 && (exit->getSegmentID() != entry->getSegmentID() ||
				    exit->getLaneID() != entry->getLaneID() ||
				    exit->getWaypointID() + 1 != entry->getWaypointID() ) ) {
	  unsigned intTurnAngInd = 0;
	  while (intTurnAngInd < intTurnAngles.size() && entry != intTurnAngles[intTurnAngInd].point) {
	    intTurnAngInd++;
	  }
	  if (intTurnAngInd >= intTurnAngles.size()) {
	    cerr << __FILE__ << ":" << __LINE__ << " ERROR: cannot find entry point "
		 << entry->getSegmentID() << "." << entry->getLaneID() << "." << entry->getWaypointID()
		 << endl;
	  }
	  else {
	    intTurnAngInd++;
	    if (fmod(intTurnAngles.size(), 2) == 0) {
	      if(intTurnAngInd > intTurnAngles.size()/2)
		edgeType = Edge::INTERSECTION_RIGHT_TURN;
	      else
		edgeType = Edge::INTERSECTION_LEFT_TURN;
	    }
	    else {
	      if(intTurnAngInd > (intTurnAngles.size() + 1)/2)
		edgeType = Edge::INTERSECTION_RIGHT_TURN;
	      else if(intTurnAngInd < (intTurnAngles.size() + 1)/2)
		edgeType = Edge::INTERSECTION_LEFT_TURN;
	    }
	  }
	}

	bool edgeAdded = addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
				 entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID(), 
				 length, weight, edgeType);
        if (!edgeAdded && DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";
          exit->print();
          cout << " to ";
          entry->print();
          cout << endl;
        }
      }
    }
            
    // Adding edges from all parking spot checkpoints to their exit waypoints.
    if(exitLaneID == 0)
    {
      for(int j = 1; j <= rndf->getZone(exitSegmentID)->getNumOfSpots(); j++)
      {
        Waypoint* spot = rndf->getWaypoint(exitSegmentID, j, 2);
        if(spot->isCheckpoint())
        {
          double length = computeDistance(exit->getNorthing(), exit->getEasting(), spot->getNorthing(), 
					  spot->getEasting());
	  // TODO: take into account the speed (rndf->getZone(exitSegmentID)->getMaxSpeed());
	  double weight = COST_ZONE + 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT);
          if (!addEdge(exitSegmentID, j, 2, 
		       exitSegmentID, exitLaneID, exitWaypointID, length, weight, Edge::ZONE))
          {
            if (DEBUG_LEVEL > 3)
            {
              cout << "Cannot add edge from ";
              exit->print();
              cout << " to ";
              spot->print();
              cout << endl;
            }
          }
        }
      }
    }

    /* Comment this out because now RNDF treats the next waypoint from an exit waypoints
       as an entry point.
    // Adding all edges from exit waypoints to their next waypoint on the same lane.
    else if(exitWaypointID < rndf->getLane(exitSegmentID, exitLaneID)->getNumOfWaypoints())
    {
      GPSPoint* entry = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID+1);
      double length = computeDistance(entry->getNorthing(), entry->getEasting(),
          exit->getNorthing(), exit->getEasting());
      double weight = length/(rndf->getSegment(exitSegmentID)->getMaxSpeed()) +
          COST_INTERSECTION;
      if (rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID)->isStopSign())
      {
        weight += COST_STOP_SIGN;
      }
      if (!addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
			    entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID(), length, weight))
      {
        addVertex(entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID());
        if (!addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
            entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID(), length, weight))
        {
  	      cout << "Cannot add edge from ";
          exit->print();
          cout << " to ";
          entry->print();
          cout << endl;
        }
        addEdgesFromEntry(rndf, entry);
      }
    }
    */
  }
  
  // Adding all edges from entry waypoints.
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    GPSPoint* entry;
    
    int entrySegmentID = vertex->getSegmentID();
    int entryLaneID = vertex->getLaneID();
    int entryWaypointID = vertex->getWaypointID();
    
    // Adding all edges from entry perimeter points of zones.

    if(entryLaneID == 0)
    {
      entry = rndf->getPerimeterPoint(entrySegmentID, entryWaypointID);
      
      if(!entry->isEntry())
      {
        continue;
      }
        
      addEdgesFromEntryPerimeter(rndf, entry);
    }
    else
    {
      entry = rndf->getWaypoint(entrySegmentID, entryLaneID, entryWaypointID);
            
      if(!entry->isEntry())
      {
        continue;
      }

      addEdgesFromEntry(rndf, entry);
    }
  }


  // Adding edges to/from all checkpoints.
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    Waypoint* checkpoint;
    
    int checkpointSegmentID = vertex->getSegmentID();
    int checkpointLaneID = vertex->getLaneID();
    int checkpointWaypointID = vertex->getWaypointID();
        
    if(checkpointLaneID == 0 || checkpointSegmentID > rndf->getNumOfSegments())
    {
      continue;
    }
    else
    {
      checkpoint = rndf->getWaypoint(checkpointSegmentID, checkpointLaneID,
          checkpointWaypointID);
    }

    if(!checkpoint->isCheckpoint() || checkpoint->isExit())
    {
      continue;
    }
        
    // Adding edges to all checkpoints from their closest entry waypoints.
    Waypoint* previousWaypoint = rndf->getWaypoint(checkpointSegmentID, checkpointLaneID,
        checkpointWaypointID);
    bool entryFound = previousWaypoint->isEntry();
    int j = checkpointWaypointID - 1;
    double length = 0;
    while(!entryFound && j >= 1)
    {
      Waypoint* entry = rndf->getWaypoint(checkpointSegmentID, checkpointLaneID, j);
      length += computeDistance(entry->getNorthing(), entry->getEasting(),
    		previousWaypoint->getNorthing(), previousWaypoint->getEasting());
      if(entry->isEntry())
      {
        double weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT);
        if (!addEdge(checkpointSegmentID, checkpointLaneID, j,
		     checkpointSegmentID, checkpointLaneID, checkpointWaypointID, 
		     length, weight, Edge::ROAD_SEGMENT ))
        {
          if (DEBUG_LEVEL > 3)
          {
            cout << "Cannot add edge from ";
            checkpoint->print();
            cout << " to ";
            entry->print();
            cout << endl;
          }
        }
        entryFound = true;
      }
      j--;
      previousWaypoint = entry;
    }
   
    // Adding edges from all checkpoints to their closest exit waypoints and other
    // checkpoints on the same segment before their closest exit waypoints.
    addEdgesFromEntry(rndf, checkpoint);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add edges from the specified entry point to its closest exit point
// (both on the same lane and adjacent lanes) and to all
// checkpoints before its closest exit point.
//-------------------------------------------------------------------------------------------
bool Graph::addEdgesToNewWaypoint(RNDF* rndf, Waypoint* newWaypoint)
{
  if (newWaypoint == NULL)
    return false;

  int newWaypointSegmentID = newWaypoint->getSegmentID();

  if (newWaypointSegmentID > getNumOfSegments())
    return false;

  int newWaypointLaneID = newWaypoint->getLaneID();
  int newWaypointWaypointID = newWaypoint->getWaypointID();

  // Adding edges to this waypoint from its closest checkpoint or entry point in the same lane.
  bool entryFound = (newWaypoint->isEntry()) || (newWaypoint->isCheckpoint());
  int j = newWaypointWaypointID - 1;
  GPSPoint* previousWaypoint = newWaypoint;
  double length = 0;
  while(!entryFound && j >= 1)
  {
    Waypoint* entry = rndf->getWaypoint(newWaypointSegmentID, newWaypointLaneID, j);
    length += computeDistance(previousWaypoint->getNorthing(), previousWaypoint->getEasting(), 
			      entry->getNorthing(), entry->getEasting());
    if(entry->isEntry() || entry->isCheckpoint())
    {
      double weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT);
      if (!addEdge(newWaypointSegmentID, newWaypointLaneID, j,
		   newWaypointSegmentID, newWaypointLaneID, newWaypointWaypointID,
		   length, weight, Edge::ROAD_SEGMENT))
      {
        if (DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";	  
	  cout << newWaypointSegmentID << "." << newWaypointLaneID << "." << j << "(";
	  entry->print();
	  cout << ")";
          cout << " to ";
          newWaypoint->print();
          cout << endl;
        }
      }
      else 
      {
	entryFound = true;
	return true;
      }
    }
    j--;
    previousWaypoint = entry;
  }
  return false;
}


void Graph::addEdgesFromEntry(RNDF* rndf, GPSPoint* entry)
{      
  int entrySegmentID = entry->getSegmentID();
  int entryLaneID = entry->getLaneID();
  int entryWaypointID = entry->getWaypointID();
  addEdgesFromEntry(rndf, entry, entrySegmentID, entryLaneID, entryWaypointID);
}


void Graph::addEdgesFromEntry(RNDF* rndf, GPSPoint* entry, int newEntrySegmentID, 
			      int newEntryLaneID, int newEntryWaypointID)
{
  if (entry == NULL)
    return;

  int entrySegmentID = entry->getSegmentID();
  int entryLaneID = entry->getLaneID();
  int entryWaypointID = entry->getWaypointID();
  int numOfWaypoints = rndf->getLane(entrySegmentID, entryLaneID)->getNumOfWaypoints();

  // Adding edges from this entry waypoint to its next exit waypoints in the same lane.
  bool exitFound = false;
  int j = entryWaypointID + 1;
  GPSPoint* previousWaypoint = entry;
  double length = 0;
  while(!exitFound && j <= numOfWaypoints)
  {
    Waypoint* exit = rndf->getWaypoint(entrySegmentID, entryLaneID, j);
    length += computeDistance(previousWaypoint->getNorthing(), previousWaypoint->getEasting(), 
        exit->getNorthing(), exit->getEasting());
    if(exit->isExit())
    {
      double weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT);
      if (!addEdge(newEntrySegmentID, newEntryLaneID, newEntryWaypointID,
          entrySegmentID, entryLaneID, j, length, weight, Edge::ROAD_SEGMENT))
      {
        if (DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";	  
	  cout << newEntrySegmentID << "." << newEntryLaneID << "." << newEntryWaypointID << "(";
	  entry->print();
	  cout << ")";
          cout << " to ";
          exit->print();
          cout << endl;
        }
      }
      exitFound = true;
    }
    else if (exit->isCheckpoint())
    {
      double weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT);
      if (!addEdge(newEntrySegmentID, newEntryLaneID, newEntryWaypointID,
          entrySegmentID, entryLaneID, j, length, weight, Edge::ROAD_SEGMENT))
      {
        if (DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";	  
	  cout << newEntrySegmentID << "." << newEntryLaneID << "." << newEntryWaypointID << "(";
	  entry->print();
	  cout << ")";
          cout << " to ";
          exit->print();
          cout << endl;
        }
      }
    }
    j++;
    previousWaypoint = exit;
  }


  // Adding edges from this entry waypoint to exit waypoints in adjacent,
  // same direction lanes.
  if (entryWaypointID < numOfWaypoints)
  {
    vector<Lane*> adjacentLanes =
        rndf->getAdjacentLanes(rndf->getLane(entrySegmentID, entryLaneID));
    GPSPoint* nextWaypoint =
        rndf->getWaypoint(entrySegmentID, entryLaneID, entryWaypointID+1);
    for(unsigned j = 0; j < adjacentLanes.size(); j++)
    {
      Lane* adjacentLane = adjacentLanes[j];
      int numOfWaypointsOnAdjLane = adjacentLane->getNumOfWaypoints();
      double distanceFromNextWaypoint;
      double distanceFromEntry;

      // Make sure that the exit point on the adjacent lane is further along the road
      // (we can't go backward!)
      double nextWaypointNorthing = nextWaypoint->getNorthing();
      double nextWaypointEasting = nextWaypoint->getEasting();
      GPSPoint* nextWaypointOnAdjLane =
          rndf->findClosestGPSPoint(nextWaypointNorthing, nextWaypointEasting, 
          entrySegmentID, adjacentLane->getLaneID(), distanceFromNextWaypoint);
      int nextWaypointOnAdjLaneID = nextWaypointOnAdjLane->getWaypointID();
      distanceFromEntry = computeDistance(entry->getNorthing(), entry->getEasting(),
          nextWaypointOnAdjLane->getNorthing(), nextWaypointOnAdjLane->getEasting());
      
      while (nextWaypointOnAdjLaneID < numOfWaypointsOnAdjLane &&
          distanceFromNextWaypoint > distanceFromEntry)
      {
        nextWaypointOnAdjLaneID++;
        nextWaypointOnAdjLane =
            rndf->getWaypoint(entrySegmentID, adjacentLane->getLaneID(), nextWaypointOnAdjLaneID);
        distanceFromNextWaypoint =
            computeDistance(nextWaypoint->getNorthing(), nextWaypoint->getEasting(), 
            nextWaypointOnAdjLane->getNorthing(), nextWaypointOnAdjLane->getEasting());
        distanceFromEntry = computeDistance(entry->getNorthing(), entry->getEasting(), 
            nextWaypointOnAdjLane->getNorthing(), nextWaypointOnAdjLane->getEasting());
      }

      if (distanceFromNextWaypoint <= distanceFromEntry)
      {	
        if (nextWaypointOnAdjLane->isExit())
        {
	  double weight = 2*distanceFromEntry/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT) +
	    abs(entryLaneID - adjacentLane->getLaneID())*COST_CHANGE_LANE;
	  if (!addEdge(newEntrySegmentID, newEntryLaneID, newEntryWaypointID,
		       entrySegmentID, adjacentLane->getLaneID(), nextWaypointOnAdjLaneID,
		       distanceFromEntry, weight, Edge::ROAD_SEGMENT))
          {
            if (DEBUG_LEVEL > 3)
            {
              cout << "Cannot add edge from ";	  
	      cout << newEntrySegmentID << "." << newEntryLaneID << "." << newEntryWaypointID << "(";
	      entry->print();
	      cout << ")";
              cout << " to ";
              nextWaypointOnAdjLane->print();
              cout << endl;
            }
          }
        }
        else
        {
          exitFound = false;
          int k = nextWaypointOnAdjLaneID + 1;
          previousWaypoint = nextWaypointOnAdjLane;
          length = distanceFromEntry;
          while(!exitFound && k <= numOfWaypointsOnAdjLane)
          {
            Waypoint* exit = rndf->getWaypoint(entrySegmentID, adjacentLane->getLaneID(), k);
            length += computeDistance(previousWaypoint->getNorthing(),
                previousWaypoint->getEasting(), exit->getNorthing(), exit->getEasting());
            if(exit->isExit())
            {
              double weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT) +
		abs(entryLaneID - adjacentLane->getLaneID())*COST_CHANGE_LANE;
	      if (!addEdge(newEntrySegmentID, newEntryLaneID, newEntryWaypointID,
			   entrySegmentID, adjacentLane->getLaneID(), k, length, 
			   weight, Edge::ROAD_SEGMENT))
              {
                if (DEBUG_LEVEL > 3)
                {
                  cout << "Cannot add edge from "; 
		  cout << newEntrySegmentID << "." << newEntryLaneID << "." << newEntryWaypointID << "(";
		  entry->print();
		  cout << ")";
                  cout << " to ";
                  exit->print();
                  cout << endl;
                }
              }
              exitFound = true;
            }
            else if (exit->isCheckpoint())
            {
              double weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT) +
                  abs(entryLaneID - adjacentLane->getLaneID())*COST_CHANGE_LANE;
              if (!addEdge(newEntrySegmentID, newEntryLaneID, newEntryWaypointID,
			   entrySegmentID, adjacentLane->getLaneID(), k, length, 
			   weight, Edge::ROAD_SEGMENT))
              {
                if (DEBUG_LEVEL > 3)
                {
                  cout << "Cannot add edge from ";
		  cout << newEntrySegmentID << "." << newEntryLaneID << "." << newEntryWaypointID << "(";
		  entry->print();
		  cout << ")";
                  cout << " to ";
                  exit->print();
                  cout << endl;
                }
              }
            }
            k++;
            previousWaypoint = exit;
          }
        }   
      }
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add edges from the specified entry perimeter point to all its exit perimeter points and
// to all the checkpoints in the zone.
//-------------------------------------------------------------------------------------------
void Graph::addEdgesFromEntryPerimeter(RNDF* rndf, GPSPoint* entry)
{
  int entrySegmentID = entry->getSegmentID();
  int entryLaneID = entry->getLaneID();
  int entryWaypointID = entry->getWaypointID();
  addEdgesFromEntryPerimeter(rndf, entry, entrySegmentID, entryLaneID, entryWaypointID);
}

void Graph::addEdgesFromEntryPerimeter(RNDF* rndf, GPSPoint* entry, int newEntrySegmentID, 
			      int newEntryLaneID, int newEntryWaypointID)
{
  int entrySegmentID = entry->getSegmentID();

  // Adding edges from this entry perimeter points to all exit perimeter points.
  for(int j = 1; j <= rndf->getZone(entrySegmentID)->getNumOfPerimeterPoints(); j++)
  {
    GPSPoint* exit = rndf->getPerimeterPoint(entrySegmentID, j);
    if(exit->isExit())
    {
      double length = computeDistance(entry->getNorthing(), entry->getEasting(),
          exit->getNorthing(), exit->getEasting());
      // TODO: Take into account the speed (rndf->getZone(entrySegmentID)->getMaxSpeed()) ;
      double weight = COST_ZONE + 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT);
      if (!addEdge(newEntrySegmentID, newEntryLaneID, newEntryWaypointID, entrySegmentID, 0,
          j, length, weight, Edge::ZONE))
      {
        if (DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";
	  cout << newEntrySegmentID << "." << newEntryLaneID << "." << newEntryWaypointID << "(";
	  entry->print();
	  cout << ")";
          exit->print();
          cout << endl;
        }
      }
    }
  }
            
  // Adding edges from this entry perimeter point to all parking spot checkpoints.
  for(int j = 1; j <= rndf->getZone(entrySegmentID)->getNumOfSpots(); j++)
  {
    Waypoint* spot = rndf->getWaypoint(entrySegmentID, j, 2);
    if(spot->isCheckpoint())
    {
      double length = computeDistance(entry->getNorthing(), entry->getEasting(),
          spot->getNorthing(), spot->getEasting());
      double weight = 2*length/(MIN_SPEED_LIMIT + MAX_SPEED_LIMIT) + COST_ZONE;
      if (!addEdge(newEntrySegmentID, newEntryLaneID, newEntryWaypointID, entrySegmentID, j,
          2, length, weight, Edge::ZONE))
      {
        if (DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";
	  cout << newEntrySegmentID << "." << newEntryLaneID << "." << newEntryWaypointID << "(";
	  entry->print();
	  cout << ")";
          spot->print();
          cout << endl;
        }
      }
    }
  }
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Compute the distance between two GPSPoints
//-------------------------------------------------------------------------------------------
double Graph::computeDistance(double northing1, double easting1, double northing2, double easting2)
{
  return  sqrt(pow(easting2 - easting1, 2) + pow(northing2 - northing1, 2));
}

