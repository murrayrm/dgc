/*!**
 * Daniele Tamino
 * February 6, 2007
 */


#include <getopt.h>
// C++ STL headers
#include <iostream>

// local headers
#include "skynettalker/SkynetTalker.hh"
//#include "skynettalker/SkynetTalker.cc" // hack to instantiate templates
#include "travgraph/Graph.hh"
#include "interfaces/sn_types.h"
#include <boost/serialization/vector.hpp>

class SkynetTalkerTestRecv
{
    SkynetTalker<Graph> talker;
    
public:
    SkynetTalkerTestRecv(int sn_key) :
        talker(sn_key, SNglobalGloNavMapFromGloNavMapLib, MODmissionplanner)
    {
        cout << " listening!" << endl;
        while(true)
	{
            if (talker.hasNewMessage())
            {
                Graph* graph = new Graph();
                cout << "about to receive a graph...\n";
                talker.receive(graph);
                cout << " received a graph! here it is:" << endl;
                graph->print();
                cout << " done!" << endl;
            } else {
                cout << "waiting ... " << endl;
                talker.waitForMessage();
            }
        }
    }
};

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;
        cout << "about to listen...";
	SkynetTalkerTestRecv test(key);
        return 0;
}
