              Release Notes for "travgraph" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "travgraph" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "travgraph" module can be found in
the ChangeLog file.

Release R1-02 (Tue Sep 18 19:54:58 2007):
	Fixed some bugs based on the coverity results. 
	(I did check for NULL pointer but somehow that was done after
	that pointer was used.)

Release R1-01z (Tue Sep 18 16:09:25 2007):
	Remove some couts that screw up mplanner display

Release R1-01y (Thu Sep 13 16:06:49 2007):
	Fixed intersection type so we only turn on the turn signal when we
	need to turn more than 15 deg

Release R1-01x (Fri Sep  7 17:32:22 2007):
	In the addCurrentPosition function, allow mplanner to make a guess
	of on which lane we're.

Release R1-01w (Mon Aug 27 18:33:36 2007):
	Only increase the cost of the edges when capability goes down to 0.
	Remove an intersection edge only if we have a stop sign.

Release R1-01v (Sun Aug  5  3:22:56 2007):
	Fixed bug that caused segfault in mapper in the mock field demo

Release R1-01u (Wed Aug  1 18:19:50 2007):
	Fixed bug when an exit point is also a checkpoint

Release R1-01t (Wed Jul 25 16:21:41 2007):
	Initialize edge cost properly

Release R1-01s (Fri Jul 20  3:56:29 2007):
	Minor changes in updateEdgeCost function

Release R1-01r (Fri Jun  8 23:09:51 2007):
	Added and fixed functions to support mplanner new logic for
	choosing between skipping checkpoint, crossing checkpoint from
	other lane, uturn and multiple uturns

Release R1-01q (Sun Jun  3 10:46:20 2007):
	Fixed minor bug in addObstacle

Release R1-01p (Mon May 28 10:00:52 2007):
	Removed debugging messages

Release R1-01o (Thu May 24 10:42:36 2007):
	Discovered anotehr segfault

Release R1-01n (Thu May 24  1:18:22 2007):
	Serialize prevVertex. Added setRNDF() function to Graph.

Release R1-01m (Tue May 22 22:53:52 2007):
	Determine the type of intersection (left turn, right turn, straight) so mplanner can send directive to tplanner whether we want to put turn signals on.

Release R1-01l (Sat May  5  9:39:05 2007):
	Update obstructed level of added edges

Release R1-01k (Sat May  5  0:15:07 2007):
	Fixed the addObstacle function so now the mission planner should see all the obstacles that the mapper sees.

Release R1-01j (Thu Apr 12 17:01:05 2007):
	Added more functions to the travgraph that are needed by 
revision R2-01h of mplanner

Release R1-01i (Tue Apr  3 18:40:02 2007):
	Updated Graph.{hh,cc} so it compiles against version R1-00l of rndf. Also fixed the edge type so if the exit point and entry point 
are in the same segment but the direction of exit lane is different from the direction of entry lane, the type is UTURN instead of 
INTERSECTION.

Release R1-01h (Sat Mar 17  2:29:25 2007):
	Added a function for inserting obstacle (Moved from mplanner)"

Release R1-01g (Fri Mar 16 17:34:43 2007):
	Fixed bugs in creating graph from RNDF (remove extra edges)"

Release R1-01f (Fri Mar 16  1:13:41 2007):
	Added log() function

Release R1-01e (Tue Mar 13 21:15:24 2007):
	Modified addCurrentPosition so it output more useful information"

Release R1-01d (Mon Mar 12 19:48:04 2007):
	Fixed possible memory leak in Graph. Valgrind is now happy.	

Release R1-01c (Wed Mar  7 20:50:03 2007):
	Added removeEdge(Edge*) function needed by mplanner-R2-00d

Release R1-01b (Tue Mar  6 21:07:50 2007):
	Updated the print function for Graph and Edge so it gives more useful information for debugging the mission planner.

Release R1-01a (Mon Mar  5 19:23:20 2007):
	Added fields numOfSegments and numOfZones to the Graph class and entry, exit, checkpoint, stopSign, and checkpointID to the Vertex class so 
the route planner can get all the information it needs from the graph instead of needing rndf."

Release R1-00d (Sat Mar  3 20:14:42 2007):
	Added functions for adding uturn edges and added member variable type to Edge so that mplanner can command uturn.

Release R1-00c (Sat Mar  3 12:32:57 2007):
	Added the copy constructor and assignment operator for Graph and Vertex as private function (just in case we need them in the
	future) and fixed the addCurrentPosition function in Graph.

Release R1-00b (Sat Mar  3  1:36:58 2007):
	Added serialize function for Graph, Vertex and Edge so we can send graph over skynet using skynettalker. Big thanks to Daniele for sitting 
with me for 12 hours to fix this.

Release R1-00a (Thu Mar  1 22:05:58 2007):
	Moved the structure for traversibility graph from the mplanner module to here. Basically, this is part of Nok's first attempt to split up the 
mission planner into 5 different things as suggested by the canonical structure.

Release R1-00 (Thu Mar  1 18:55:52 2007):
	Created.































