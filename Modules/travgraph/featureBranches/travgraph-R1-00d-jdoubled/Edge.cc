/*!**
 * Morlan Lui and Nok Wongpiromsarn
 * December 8, 2006
 */

#include "Edge.hh"
using namespace std;

Edge::Edge(Vertex* previous, Vertex* next)
{
  this->previous = previous;
  this->next = next;
  this->weight = 0;
  this->length = 0;
  this->type = UNKNOWN;
  obstructedLevel = 0;
}

Edge::~Edge()
{
}

double Edge::getWeight()
{
  return weight;
}

double Edge::getLength()
{
  return length;
}

Vertex* Edge::getPrevious()
{
  return previous;
}

Vertex* Edge::getNext()
{
  return next;
}

Edge::EdgeType Edge::getType()
{
  return type;
}

int Edge::getObstructedLevel()
{
  return obstructedLevel;
}

void Edge::setWeight(double weight)
{
  this->weight = weight;
}

void Edge::setLength(double length)
{
  this->length = length;
}

void Edge::setObstructedLevel(int obstructedLevel)
{
  this->obstructedLevel = obstructedLevel;
}

void Edge::setNext(Vertex* next)
{
  this->next = next;
}

void Edge::setType(EdgeType type)
{
  this->type = type;
}
