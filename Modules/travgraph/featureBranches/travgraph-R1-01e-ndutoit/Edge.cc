/*!**
 * Morlan Lui and Nok Wongpiromsarn
 * December 8, 2006
 */

#include "Edge.hh"
#include <iostream>
using namespace std;

Edge::Edge(Vertex* previous, Vertex* next)
{
  this->previous = previous;
  this->next = next;
  this->weight = 0;
  this->length = 0;
  this->type = UNKNOWN;
  obstructedLevel = 0;
}

Edge::~Edge()
{
}

double Edge::getWeight()
{
  return weight;
}

double Edge::getLength()
{
  return length;
}

Vertex* Edge::getPrevious()
{
  return previous;
}

Vertex* Edge::getNext()
{
  return next;
}

Edge::EdgeType Edge::getType()
{
  return type;
}

int Edge::getObstructedLevel()
{
  return obstructedLevel;
}

void Edge::setWeight(double weight)
{
  this->weight = weight;
}

void Edge::setLength(double length)
{
  this->length = length;
}

void Edge::setObstructedLevel(int obstructedLevel)
{
  this->obstructedLevel = obstructedLevel;
}

void Edge::setNext(Vertex* next)
{
  this->next = next;
}

void Edge::setType(EdgeType type)
{
  this->type = type;
}

void Edge::print()
{
  cout << "(";
  if (type == INTERSECTION_LEFT_TURN || type == INTERSECTION_LEFT_TURN_STOP
      || type == INTERSECTION_RIGHT_TURN || type == INTERSECTION_RIGHT_TURN_STOP
      || type == INTERSECTION_STRAIGHT || type == INTERSECTION_STRAIGHT_STOP)
  {
    cout << "intersection";
  }
  else if (type == ROAD_SEGMENT)
  {
    cout << "road segment";
  }
  else if (type == UTURN || type == KTURN)
  {
    cout << "uturn";
  }
  else if (type == ZONE)
  {
    cout << "zone";
  }
  else
  {
    cout << "unknown";
  }
  cout << ", " << length << ", " << weight << ")";
}
