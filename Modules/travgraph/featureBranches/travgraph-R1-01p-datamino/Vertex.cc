/*!**
 * Morlan Liu and Nok Wongpiromsarn
 * December 8, 2006
 */

#include "Vertex.hh"
using namespace std;


Vertex::Vertex(int segmentID, int laneID, int waypointID)
{
  Vertex(segmentID, laneID, waypointID, false, false, false, false, 0);
}


Vertex::Vertex(int segmentID, int laneID, int waypointID, bool entry, bool exit, 
	       bool checkpoint, bool stopSign, int checkpointID)
{
  this->segmentID = segmentID;
  this->laneID = laneID;
  this->waypointID = waypointID;
  this->entry = entry;
  this->exit = exit;
  this->checkpoint = checkpoint;
  this->stopSign = stopSign;
  this->checkpointID = checkpointID;
  previousVertex = NULL;
  costToCome = INFINITE_COST_TO_COME;
}

/*
Vertex::Vertex(const Vertex& v)
{
  segmentID = v.segmentID;
  laneID = v.laneID;
  waypointID = v.waypointID;
  costToCome = v.costToCome;
  edges.resize(v.edges.size());
  for (unsigned i = 0; i < v.edges.size(); i++)
  {
    edges[i] = new Edge(*v.edges[i]);
  }  
}
*/

Vertex::~Vertex()
{
  for(unsigned i = 0; i < edges.size(); i++)
    delete edges[i];
}

int Vertex::getSegmentID()
{
  return segmentID;
}

int Vertex::getLaneID()
{
  return laneID;
}

int Vertex::getWaypointID()
{
  return waypointID;
}

bool Vertex::isEntry()
{
  return entry;
}

bool Vertex::isExit()
{
  return exit;
}

bool Vertex::isCheckpoint()
{
  return checkpoint;
}

bool Vertex::isStopSign()
{
  return stopSign;
}

int Vertex::getCheckpointID()
{
  return checkpointID;
}

vector<Edge*> Vertex::getEdges()
{
  return edges;
}

void Vertex::addEdge(Edge* edge)
{
  if (edge == NULL)
  {
    cerr << "Cannnot add NULL edge" << endl;
  }
  else
  {
    if (edge->getPrevious()->getSegmentID() == segmentID &&
	edge->getPrevious()->getLaneID() == laneID &&
	edge->getPrevious()->getWaypointID() == waypointID)
    {
      edges.push_back(edge);
    }
    else
    {
      cout << "invalide edge" << endl;
    }
  }
}

bool Vertex::removeEdge(Edge* edge)
{
  if (edge == NULL)
    return false;
  unsigned i = 0;
  bool edgeRemoved = false;
  while( i < edges.size() )
  {
    if(edges[i] == edge)
    {
      edges.erase(edges.begin()+i,edges.begin()+i+1);
      edgeRemoved = true;
      delete edge;
    }
    else
      i++;
  }
  return edgeRemoved;
}

void Vertex::removeAllEdges()
{
  for(unsigned i = 0; i < edges.size(); i++)
    delete edges[i];
  edges.clear();
}

void Vertex::setCostToCome(float cost)
{
  costToCome = cost;
}

float Vertex::getCostToCome()
{
  return costToCome;
}

void Vertex::setPreviousVertex(Vertex* previous)
{
  previousVertex = previous;
}

Vertex* Vertex::getPreviousVertex()
{
  return previousVertex;
}

void Vertex::print()
{
  cout << segmentID << "." << laneID << "." << waypointID;
}
