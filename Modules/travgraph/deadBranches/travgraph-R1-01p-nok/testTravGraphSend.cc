/*!**
 * Daniele Tamino
 * February 6, 2007
 */

#include <getopt.h>
// C++ STL headers
#include <iostream>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>
// local headers
#include "skynettalker/SkynetTalker.hh"
#include "skynet/skynet.hh"
#include "travgraph/Graph.hh"
#include "rndf/RNDF.hh"
#include "interfaces/sn_types.h"

class SkynetTalkerTestSend
{
  SkynetTalker<Graph> talker;
public:
  SkynetTalkerTestSend(int sn_key, char *RNDFFileName)
    : talker(sn_key, SNglobalGloNavMapFromGloNavMapLib, MODmissionplanner)
  {
    RNDF* m_rndf = new RNDF();
    if (!m_rndf->loadFile(RNDFFileName))
    {
      cerr << "Error:  Unable to load RNDF file " << RNDFFileName
           << ", exiting program" << endl;
      exit(1);
    }
    m_rndf->assignLaneDirection();

    Graph* m_rndfGraph = new Graph(m_rndf);

    cout << "about to send a graph" << endl;
    m_rndfGraph->print();
    cout << "graph printed" << endl;
    talker.send(m_rndfGraph);
    cout << endl << "sent a graph!" << endl;
  }
};

// Command line options
enum {OPT_NONE, OPT_HELP, OPT_RNDF, NUM_OPTS};
static struct option long_options[] = {
  // first: long option (--option) string
  // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
  // third: if pointer, set variable to value of fourth argument
  //        if NULL, getopt_long returns fourth argument
  {"help", 0, NULL, OPT_HELP},
  {"rndf", 1, NULL, OPT_RNDF},
  {NULL, 0, NULL, 0}
};

char *usage_string = "\
Usage: follow [options]\n\
  -h, --help        print usage information\n\
  --rndf FILENAME   use FILENAME as the RDDF\n\
";

int main(int argc, char *argv[])
{
  int errflg = 0, ch, option_index;
  char *rndffile = "../../routes-darpa/darpa_sample.rndf";

  // Parse command line options
  while (!errflg &&
	 (ch = getopt_long_only(argc, argv, "",
				long_options, &option_index)) != -1) {
    switch (ch) {
    case '?':
      fprintf(stderr, usage_string);
      ++errflg;
      break;

    case OPT_RNDF:
      if (optarg != NULL) rndffile=optarg;
      break;

    default:
      if(ch!=0) {
	printf("Unknown option %d!\n", ch);
	fprintf(stderr, usage_string);
	exit(1);
      }
    }
  }

  // Check to see if there were any errors
  if (errflg) { exit(1); }


  extern int optind;
  int snkey = (argc > optind) ? atoi(argv[optind]) : 0;
  SkynetTalkerTestSend test(snkey, rndffile);
  return 0;
}
