/*!**
 * Morlan Lui and Nok Wongpiromsarn
 * December 8, 2006
 */

#include "Edge.hh"
#include <iostream>
using namespace std;

Edge::Edge(Vertex* previous, Vertex* next)
{
  this->previous = previous;
  this->next = next;
  this->weight = 0;
  this->length = 0;
  this->type = UNKNOWN;
  obstructedLevel = 0;
  isVisited = false;
}

Edge::~Edge()
{
}

double Edge::getWeight()
{
  return weight;
}

double Edge::getLength()
{
  return length;
}

Vertex* Edge::getPrevious()
{
  return previous;
}

Vertex* Edge::getNext()
{
  return next;
}

Edge::EdgeType Edge::getType()
{
  return type;
}

int Edge::getObstructedLevel()
{
  return obstructedLevel;
}

bool Edge::getIsVisited()
{
  return isVisited;
}

double Edge::getAvgSpeed()
{
  return avgSpeed;
}

void Edge::setWeight(double weight)
{
  this->weight = weight;
}

void Edge::setLength(double length)
{
  this->length = length;
}

void Edge::setObstructedLevel(int obstructedLevel)
{
  this->obstructedLevel = obstructedLevel;
}

void Edge::setNext(Vertex* next)
{
  this->next = next;
}

void Edge::setType(EdgeType type)
{
  this->type = type;
}

void Edge::setIsVisited(bool isVisited)
{
  this->isVisited = isVisited;
}

void Edge::setAvgSpeed(double avgSpeed)
{
  this->avgSpeed = avgSpeed;
}

void Edge::print()
{
  cout << "(";
  if (type == INTERSECTION_LEFT_TURN)
  {
    cout << "intersection left turn";
  }
  else if (type == INTERSECTION_RIGHT_TURN)
  {
    cout << "intersection right turn";
  }
  else if (type == INTERSECTION_STRAIGHT)
  {
    cout << "intersection straight";
  }
  else if (type == ROAD_SEGMENT)
  {
    cout << "road segment";
  }
  else if (type == UTURN || type == KTURN)
  {
    cout << "uturn";
  }
  else if (type == ZONE)
  {
    cout << "zone";
  }
  else if (type == FIRST_SEGMENT)
  {
    cout << "first segment";
  }
  else
  {
    cout << "unknown";
  }
  cout << ", " << length << ", " << weight << ")";
}
