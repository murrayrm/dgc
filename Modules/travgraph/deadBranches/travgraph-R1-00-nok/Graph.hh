/*!**
 * Morlan Lui and Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef GRAPH_HH_
#define GRAPH_HH_
#include "Vertex.hh"
#include <vector>
#include "rndf/RNDF.hh"
using namespace std;

/*! Graph class. Represents a graph of vertices and edges. A graph contains a
 *  vector of Vertex pointers.
 * \brief The Graph class used for the routePlanner.
 */
class Graph
{
public:
  Graph();
  Graph(RNDF*);
  virtual ~Graph();
  
/*! Returns the pointer of the vertex with segmentID, laneID, and waypointID
 *  contained in THIS. */
  Vertex* getVertex(int segmentID, int laneID, int waypointID);
  
/*! Returns the vector of vertex pointers contained in THIS. */
  vector<Vertex*> getVertices();

/* Returns the number of vertices contained in THIS. */
  int getNumVertices();
  
/*! Adds a vertex with segmentID, laneID, and waypointID to THIS. */
  bool addVertex(int segmentID, int laneID, int waypointID);

/*! Add a vertex corresponding to the specified GPSPoint (can be waypoint or perimeter point)
 * to the graph with appropriate edges */
  bool addGPSPointToGraph (GPSPoint*, RNDF*);

/*! Removes a vertex with segmentID, laneID, and waypointID to THIS. */
  bool removeVertex(int segmentID, int laneID, int waypointID);

/*! Returns the pointer of the edge from vertex with segmentID1, laneID1, waypointID1 to the
 *  vertex with segmentID2, laneID2, and waypointID2 contained in THIS. */
  Edge* getEdge(Vertex* vertex1, Vertex* vertex2);
  Edge* getEdge(int segmentID1, int laneID1, int waypointID1,
		int segmentID2, int laneID2, int waypointID2);

/*! Adds an edge from vertex with segmentID1, laneID1, waypointID1 to the
 *  vertex with segmentID2, laneID2, and waypointID2. */
  bool addEdge(int segmentID1, int laneID1, int waypointID1, 
	       int segmentID2, int laneID2, int waypointID2);
  bool addEdge(int segmentID1, int laneID1, int waypointID1,
	       int segmentID2, int laneID2, int waypointID2, double length, double weight);
  bool addEdge(Edge* edge);

/*! Removes an edge from vertex with segmentID1, laneID1, waypointID1 to the
 *  vertex with segmentID2, laneID2, and waypointID2. */
  bool removeEdge(int segmentID1, int laneID1, int waypointID1, 
  int segmentID2, int laneID2, int waypointID2);
  
/*! Prints the vertices contained in THIS. */
  void print();
  
private:
  vector<Vertex*> vertices;
  
/*! Add the following vertices to rndfGraph
 * exit points
 * entry points
 * checkpoints
 * entry perimeter points
 * exit perimeter points
 * parking spot waypoints */
void addAllVertices(RNDF*);


/*! Add the following edges to rndfGraph
 * exit point -> entry points
 * entry point -> closest exit points
 * closest entry points -> checkpoint
 * checkpoint -> closest exit points
 * parking spot -> exit points
 * entry perimeter point -> exit perimeter points
 * entry perimenter point -> parking spots */
void addAllEdges(RNDF*);


/*! Add edges from the specified entry point to its closest exit points
 * (both on the same lane and adjacent lanes). */
void addEdgesFromEntry(RNDF*, GPSPoint*);


/*! Add edges from the specified entry perimeter point to all its exit perimeter points and
 * to all the checkpoints in the zone. */
void addEdgesFromEntryPerimeter(RNDF*, GPSPoint*);

/*! Compute the distance between two GPSPoints */
double computeDistance(double, double, double, double);

};

#endif /*GRAPH_HH_*/
