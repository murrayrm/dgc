#include "CSpecs.hh"

double* CSpecs::getStartingState()
{
	double *tmp = new double[4];
	for (int i=0; i<4; i++)
		tmp[i] = startingState[i];
	return tmp;
}

double* CSpecs::getFinalState() 
{
	double *tmp = new double[4];
	for (int i=0; i<4; i++)
		tmp[i] = finalState[i];
	return tmp;
}
double* CSpecs::getStartingControls()	
{
	double *tmp = new double[2];
	for (int i=0; i<2; i++)
		tmp[i] = startingControls[i];
	return tmp;
}
double* CSpecs::getFinalControls()	
{
	double *tmp = new double[2];
	for (int i=0; i<2; i++)
		tmp[i] = finalControls[i];
	return tmp;
}
double* CSpecs::getRmatrix()	
{
	double *tmp = new double[4];
	for (int i=0; i<4; i++)
		tmp[i] = RmatrixRowByRow[i];
	return tmp;
}
double* CSpecs::getQmatrix()
{
	double *tmp = new double[16];
	for (int i=0; i<16; i++)
		tmp[i] = QmatrixRowByRow[i];
	return tmp;
}	



