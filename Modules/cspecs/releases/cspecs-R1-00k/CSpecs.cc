#include "CSpecs.hh"

/**	Accessors: getting data */

vector<double> CSpecs::getStartingState()
{
	return m_startingState;
}

vector<double> CSpecs::getFinalState() 
{
	return m_finalState;
}
vector<double> CSpecs::getStartingControls()	
{
	return m_startingControls;
}
vector<double> CSpecs::getFinalControls()	
{
	return m_finalControls;
}
vector<double> CSpecs::getRmatrix()	
{
	return m_QmatrixRowByRow;
}
vector<double> CSpecs::getQmatrix()
{
	return m_RmatrixRowByRow;
}	
double CSpecs::getMaxVelocity()
{
  return m_maxVelocity;
}

double CSpecs::getMaxAcc()
{
  return m_maxAcc;
}

double CSpecs::getMaxBraking()
{
  return m_maxBraking;
}

double CSpecs::getMaxSteeringAngle()
{
  return m_maxSteeringAngle;
}

double CSpecs::getMaxSteeringRate()
{
  return m_maxSteeringRate;
}

BitmapParams CSpecs::getBitmapParams()
{
  return m_bmParams;
}
CostMap *CSpecs::getCostMap()
{
  return &m_costMap;
}
point2arr CSpecs::getBoundingPolygon()
{
  return m_boundingPolygon;
}
vector<point2> CSpecs::getBoundingPolygon_Vector()
{
  return getBoundingPolygon().arr;
}
// TODO: taking the convex hull needs to be implemented here
point2arr CSpecs::getBoundingPolytope()
{
  return m_boundingPolygon;
}
vector<point2> CSpecs::getBoundingPolytope_Vector()
{
  return getBoundingPolytope().arr;
}
vector<Obstacle> CSpecs::getLinearObstacles()
{
  return m_myObstacles;
}

vector<double> CSpecs::getSafetyMargins()
{
  return m_safetyMargins;
}

/** accessors: setting information with vectors */
void CSpecs::setStartingState(vector<double> startingState_in)
{
	if ( startingState_in.size() != STATE_NUM_ELEMENTS )
	{
		cerr << "cspecs::setStartingState- ERROR- wrong number of elements in state" << endl;
		cerr << "num elements: " << startingState_in.size() << " expected " << STATE_NUM_ELEMENTS << endl;
		abort();
	}
	m_startingState = startingState_in;
}
void CSpecs::setFinalState(vector<double> finalState_in)
{
	if ( finalState_in.size() != STATE_NUM_ELEMENTS )
	{
		cerr << "cspecs::setFinalState- ERROR- wrong number of elements in state" << endl;
		cerr << "num elements: " << finalState_in.size() << " expected " << STATE_NUM_ELEMENTS << endl;
		abort();
	}
	m_finalState = finalState_in;
}
void CSpecs::setStartingControls(vector<double> startingControls_in)
{
	if ( startingControls_in.size() != CONTROLS_NUM_ELEMENTS )
	{
		cerr << "cspecs::setStartingControls- ERROR- wrong number of elements in controls" << endl;
		cerr << "num elements: " << startingControls_in.size() << " expected " << CONTROLS_NUM_ELEMENTS << endl;
		abort();
	}
	m_startingControls = startingControls_in;
}
void CSpecs::setFinalControls(vector<double> finalControls_in)
{
	if ( finalControls_in.size() != CONTROLS_NUM_ELEMENTS )
	{
		cerr << "cspecs::setFinalControls- ERROR- wrong number of elements in controls" << endl;
		cerr << "num elements: " << finalControls_in.size() << " expected " << CONTROLS_NUM_ELEMENTS << endl;
		abort();
	}
	m_finalControls = finalControls_in;
}
void CSpecs::setRmatrix(vector<double> Rmatrix_in)
{
	m_RmatrixRowByRow = Rmatrix_in;
}
void CSpecs::setQmatrix(vector<double> Qmatrix_in)
{
	m_QmatrixRowByRow = Qmatrix_in;
}
void CSpecs::setBoundingPolygon(point2arr poly_in)
{
	m_boundingPolygon = poly_in;
}


void CSpecs::setTrafficState(int tstate) {
  m_trafficState = tstate; 
}

void CSpecs::setSafetyMargins(vector<double> safetyMargins_in)
{
	m_safetyMargins = safetyMargins_in;
}
void CSpecs::setSafetyMargins(double safetyMargins_in)
{
	vector<double> margins;
	for (int i = 0; i < SAFETY_NUM_ELEMENTS; i++)
		margins.push_back(safetyMargins_in);
	setSafetyMargins(margins);
}


void CSpecs::setPolyObstacles(vector<point2arr> obstacles)
{
	m_polyObstacles = obstacles;
}

/** accessors: setting information with arrays */
void CSpecs::setStartingState(double* startingState_in)
{
  vector<double> startingState;
  for (int i=0; i<STATE_NUM_ELEMENTS; i++) {
    startingState.push_back(startingState_in[i]);
  }
  setStartingState(startingState);
  return;
}


void CSpecs::setFinalState(double* finalState_in)
{
  vector<double> finalState;
  for (int i=0; i<STATE_NUM_ELEMENTS; i++) {
    finalState.push_back(finalState_in[i]);
  }
  setFinalState(finalState);
  return;
}

void CSpecs::setStartingControls(double* startingControls_in)
{
  vector<double> startingControls;
  for (int i=0; i<CONTROLS_NUM_ELEMENTS; i++) {
    startingControls.push_back(startingControls_in[i]);
  }
  setStartingControls(startingControls);
  return;
}

void CSpecs::setFinalControls(double* finalControls_in)
{
  vector<double> finalControls;
  for (int i=0; i<CONTROLS_NUM_ELEMENTS; i++) {
    finalControls.push_back(finalControls_in[i]);
  }
  setFinalControls(finalControls);
  return;
}

void CSpecs::setRmatrix(double* Rmatrix_in)
{
  vector<double> Rmatrix;
  for (int i=0; i<4; i++) {
    Rmatrix.push_back(Rmatrix_in[i]);
  }
  setRmatrix(Rmatrix);
  return;
}

void CSpecs::setQmatrix(double* Qmatrix_in)
{
  vector<double> Qmatrix;
  for (int i=0; i<16; i++) {
    Qmatrix.push_back(Qmatrix_in[i]); 
  }
  setQmatrix(Qmatrix);
  return;
}

void CSpecs::setBoundingPolygon(vector<point2> poly_in)
{
  point2arr boundingPolygon;
  for (int i=0; i<(int)poly_in.size(); i++) {
    boundingPolygon.push_back(poly_in[i]);
  }
  setBoundingPolygon(boundingPolygon);
  return;
}

void CSpecs::setObstacles(vector<Obstacle> obst_in)
{
  for (int i=0; i<(int)obst_in.size(); i++) 
    m_myObstacles.push_back(obst_in[i]);
  return;
}

void CSpecs::setCostMap(BitmapParams bmParams_in)
{
  m_bmParams.centerX = bmParams_in.centerX;
  m_bmParams.centerY = bmParams_in.centerY;
  m_bmParams.resX = bmParams_in.resX;
  m_bmParams.resY = bmParams_in.resY;
  m_bmParams.width = bmParams_in.width;
  m_bmParams.height = bmParams_in.height;
  m_bmParams.baseVal = bmParams_in.baseVal;
  m_bmParams.outOfBounds = bmParams_in.outOfBounds;
  for (int i=0; i<(int)bmParams_in.polygons.size(); i++) {
    m_bmParams.polygons.push_back(bmParams_in.polygons[i]);
  }

  // generate a costmap from the bitmap params
  m_costMap.init(m_bmParams.width, m_bmParams.height, m_bmParams.resX, m_bmParams.resY, 
	    point2(m_bmParams.centerX, m_bmParams.centerY), m_bmParams.baseVal);
  m_costMap.setOutOfBounds(m_bmParams.outOfBounds);
  for (unsigned i=0; i<m_bmParams.polygons.size(); i++) {
    m_bmParams.polygons[i].draw(&m_costMap);
  }

  return;
}

void CSpecs::setMaxVelocity(double maxVel_in)
{
  m_maxVelocity = maxVel_in;
  return;
}

void CSpecs::setMaxAcc(double maxAcc_in)
{
  m_maxAcc = maxAcc_in;
  return;
}

void CSpecs::setMaxBraking(double maxBraking_in)
{
  m_maxBraking = maxBraking_in;
  return;
}

void CSpecs::setMaxSteeringAngle(double maxPhi_in)
{
  m_maxSteeringAngle = maxPhi_in;
  return;
}

void CSpecs::setMaxSteeringRate(double maxPhiDot_in)
{
  m_maxSteeringRate = maxPhiDot_in;
  return;
}

vector<point2arr> CSpecs::getPolyObstacles()
{
  return m_polyObstacles;
}

int CSpecs::getTrafficState()
{
  return m_trafficState;
}

