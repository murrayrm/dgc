#include "CSpecs.hh"

double* CSpecs::getStartingState()
{
	double *tmp = new double[4];
	for (int i=0; i<4; i++)
		tmp[i] = startingState[i];
	return tmp;
}

double* CSpecs::getFinalState() 
{
	double *tmp = new double[4];
	for (int i=0; i<4; i++)
		tmp[i] = finalState[i];
	return tmp;
}
double* CSpecs::getStartingControls()	
{
	double *tmp = new double[2];
	for (int i=0; i<2; i++)
		tmp[i] = startingControls[i];
	return tmp;
}
double* CSpecs::getFinalControls()	
{
	double *tmp = new double[2];
	for (int i=0; i<2; i++)
		tmp[i] = finalControls[i];
	return tmp;
}
double* CSpecs::getRmatrix()	
{
	double *tmp = new double[4];
	for (int i=0; i<4; i++)
		tmp[i] = RmatrixRowByRow[i];
	return tmp;
}
double* CSpecs::getQmatrix()
{
	double *tmp = new double[16];
	for (int i=0; i<16; i++)
		tmp[i] = QmatrixRowByRow[i];
	return tmp;
}	

double CSpecs::getMaxVelocity()
{
  return maxVelocity;
}

BitmapParams CSpecs::getCostMap()
{
  return bmParams;
}

vector<point2> CSpecs::getBoundingPolytope()
{
  return boundingPolytope;
}

void CSpecs::setStartingState(double* startingState_in)
{
  for (int i=0; i<4; i++) {
    startingState[i] = startingState_in[i];
  }
  return;
}


void CSpecs::setFinalState(double* finalState_in)
{
  for (int i=0; i<4; i++) {
    finalState[i] = finalState_in[i];
  }
  return;
}

void CSpecs::setStartingControls(double* startingControls_in)
{
  for (int i=0; i<2; i++) {
    startingControls[i] = startingControls_in[i];
  }
  return;
}

void CSpecs::setFinalControls(double* finalControls_in)
{
  for (int i=0; i<2; i++) {
    finalControls[i] = finalControls_in[i];
  }
  return;
}

void CSpecs::setRmatrix(double* Rmatrix_in)
{
  for (int i=0; i<4; i++) {
    RmatrixRowByRow[i] = Rmatrix_in[i];
  }
  return;
}

void CSpecs::setQmatrix(double* Qmatrix_in)
{
  for (int i=0; i<16; i++) {
    QmatrixRowByRow[i] = Qmatrix_in[i];
  }
  return;
}

void CSpecs::setBoundingPolytope(vector<point2> poly_in)
{
  for (int i=0; i<(int)poly_in.size(); i++) {
    boundingPolytope.push_back(poly_in[i]);
  }
  return;
}

void CSpecs::setObstacles(vector<Obstacle> obst_in)
{
  return;
}

void CSpecs::setCostMap(BitmapParams bmParams_in)
{
  bmParams.centerX = bmParams_in.centerX;
  bmParams.centerY = bmParams_in.centerY;
  bmParams.resX = bmParams_in.resX;
  bmParams.resY = bmParams_in.resY;
  bmParams.width = bmParams_in.width;
  bmParams.height = bmParams_in.height;
  bmParams.baseVal = bmParams_in.baseVal;
  bmParams.outOfBounds = bmParams_in.outOfBounds;
  for (int i=0; i<(int)bmParams_in.polygons.size(); i++) {
    bmParams.polygons.push_back(bmParams_in.polygons[i]);
  }
  return;
}

void CSpecs::setMaxVelocity(double maxVel_in)
{
  maxVelocity = maxVel_in;
  return;
}
