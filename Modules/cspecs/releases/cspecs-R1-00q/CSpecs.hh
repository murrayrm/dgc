/*
 *  CSpecs.h
 *  
 *
 *  Created by Northrop Grumman on 8/6/07.
 *  Copyright 2007 Northrop Grumman. All rights reserved.
 *
 */

#ifndef ZONEPLANNERINTERFACE_H
#define ZONEPLANNERINTERFACE_H

#include <vector>
#include <assert.h>
#include <stdlib.h>
#include <frames/point2.hh>
#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
//#include <bitmap/Bitmap.hh>
#include <temp-planner-interfaces/Log.hh>


/** some index definitions */
// start and and final states
#define STATE_IDX_X		0
#define STATE_IDX_Y		1
#define STATE_IDX_V		2
#define STATE_IDX_THETA		3
#define STATE_NUM_ELEMENTS	4
// Controls
#define CONTROLS_A		0
#define CONTROLS_PHI		1
#define CONTROLS_NUM_ELEMENTS	2
// safety zones
#define SAFETY_FRONT		0
#define SAFETY_RIGHT		1
#define SAFETY_BACK		2
#define SAFETY_LEFT		3
#define SAFETY_NUM_ELEMENTS	4
// traffic states
#define TRAFFIC_STATE_UNSPECIFIED	0
#define TRAFFIC_STATE_ENTER_PARK	1
#define TRAFFIC_STATE_EXIT_PARK		2
#define TRAFFIC_STATE_UTURN		3
#define TRAFFIC_STATE_TRAVERSE_ZONE	4
#define TRAFFIC_STATE_BACKUP	5
#define TRAFFIC_STATE_ROAD	6
//... can add more later as needed

/*! Obstacle struct.  This struct represents an obstacle, and it consists
 * of six fields: (x,y) position, (x,y) velocity, and (x,y) dimension.
 */
struct CSpecsObstacle
{
	double x;
	double y;
	double dx;
	double dy;
	double length;
	double width;
	double costFunctionWeight;  
};

struct CSpecsLinearConstraint {

  /* xC(x) + yC(y) <= b */

  double xC;  
  double yC; 
  double b; 

};

/*! CSpecs class.  This class is designed as an interface between
 * The traffic planner and the trajectory planners.  Specifically, this interface
 * should be used to pass information about the problem to be solved to the
 * trajectory planner.  It is intended to be used for both zone planner (OTG) and
 * clothoid planner.  It contains fields to give information about: initial and
 * final state and controls; maximum limits on acceleration, velocity, braking, 
 * steering angle, and steering rate; a bounding polytope; a set of obstacles; 
 * Q and R matrices; minimum and maximum time of trajectory; and an initial guess.
 * \brief Interface for zone planner & clothoid planner
 */

class CSpecs 
{
private:
	
  /*! Starting State (x,y,v,theta) */
  vector<double> m_startingState;
  /*! Final State (x,y,v,theta) */
  vector<double> m_finalState;
  /*! Starting Controls (a, phi) */
  vector<double> m_startingControls;
  /*! Final Controls (a, phi) */
  vector<double> m_finalControls;

  /* vector describing a safety margins around alice - its in the form of
	distances that we want to keep obstacles from alice
	to the front, right, back, and left (in that order) */
  vector<double> m_obstacleSafetyMargins;

  /* vector describing a safety margins around alice - its in the form of
	distances that we want to keep obstacles from alice
	to the front, right, back, and left (in that order) */
  vector<double> m_perimeterSafetyMargins;
	
  /*! Maximum acceleration constraint */
  double m_maxAcc;
  /*! Maximum breaking (minimum acceleration) constraint */
  double m_maxBraking;

  /*! Maximum velocity constraint */
  double m_maxVelocity;
  /*! Maximum steering angle constraint */
  double m_maxSteeringAngle;
  /*! Maximum steering rate constraint */
  double m_maxSteeringRate;
	
  /*! A set of points, representing the bounding polygon */
  point2arr m_boundingPolygon;
	
  /*! A set of points, representing the clockwise convex hull of 
   * the bounding polygon */
  point2arr m_boundingPolygonHull;
	
  /*! Container for all the obstacles in the polytope */
  std::vector<CSpecsObstacle> m_myObstacles;

  /*! Q matrix, delivered row-by-row in a 16 element vector. */
  vector<double> m_QmatrixRowByRow;
	
  /*! R matrix, delivered row-by-row in a 16 element vector. */
  vector<double> m_RmatrixRowByRow;
	
  /*! Number of points in the solution trajectory. */
  int m_numPointsInTrajectory;

  /*! Minimum and maximum times (i.e. durations) for the solution trajectory. */	
  double m_minTrajectoryTime;
  double m_maxTrajectoryTime;

  /*! Costmap object to be used with clothoid planner. */
  CostMap m_costMap;
  BitmapParams m_bmParams;
	
  /*! Initial Guess. */
  double* m_initialGuess;

  /*! traffic planner state */
  int m_trafficState;

  /*! the obstacles */
  vector<point2arr> m_polyObstacles;

  /*! The direction of the path, 1 is forwards, -1 is backwards. */
  int m_pathDirection;

  /*! A vector of linear constraints representing the bounding polygon */
  vector<CSpecsLinearConstraint> m_boundingPolyConstraints;

public:

  /*! CSpecs constructor. */
  CSpecs()
  {
    double zeroArray[4] = {0,0,0,0};
    double idmat2by2[4] = {1,0,1,0};
    double idmat4by4[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
    setStartingState(zeroArray);
    setFinalState(zeroArray);
    setQmatrix(idmat2by2);
    setRmatrix(idmat4by4);

    setObstacleSafetyMargins(2.0);
    setPerimeterSafetyMargins(1.0);

    m_maxAcc = 5;				//  m/s^2
    m_maxBraking = -2;		//  m/s^2
    m_maxVelocity = 5;		//  m/s
    m_maxSteeringAngle = 45;	//	degrees
    m_maxSteeringRate = 45;	//	deg/s
		
    m_trafficState = TRAFFIC_STATE_UNSPECIFIED; 
    m_pathDirection = 0;
  }

  /*! CSpecs constructor that takes a starting state and a
   * final state as its input.
   */
  CSpecs(double startingState_in[4], double finalState_in[4])
  {
    CSpecs();
    assert(startingState_in);
    assert(finalState_in);
    setStartingState(startingState_in);
    setFinalState(finalState_in);
  }

  /*! Various setting and getting utilities */

  /* accessors for setting data which use the vectors (like the internal structures) */
  void setStartingState(vector<double> startingState_in);
  void setFinalState(vector<double> finalState_in);
  void setStartingControls(vector<double> startingControls_in);	
  void setFinalControls(vector<double> finalControls_in);
  void setRmatrix(vector<double> Rmatrix_in);
  void setQmatrix(vector<double> Qmatrix_in);
  void setBoundingPolygon(point2arr poly_in);
  void makeBoundingPolygonHull();
  void setPolyObstacles(vector<point2arr> obstacles);
  void setTrafficState(int tstate);
  /* accessors that use regular arrays as arguments. there really ins't any disadvantage to
     using these over the above ones, or vis versa */
  void setStartingState(double* startingState_in);
  void setFinalState(double* finalState_in);
  void setStartingControls(double* startingControls_in);	
  void setFinalControls(double* finalControls_in);
  void setRmatrix(double* Rmatrix_in);
  void setQmatrix(double* Qmatrix_in);
  void setBoundingPolygon(vector<point2> poly_in);


  void setObstacles(vector<CSpecsObstacle> obst_in);
  void setCostMap(BitmapParams bmParams_in);
  void setMaxVelocity(double);
  void setMaxAcc(double);
  void setMaxBraking(double);
  void setMaxSteeringAngle(double);
  void setMaxSteeringRate(double);
  void setObstacleSafetyMargins(vector<double> safetyMargins_in);
  void setObstacleSafetyMargins(double safetyMargins_in);
  void setPerimeterSafetyMargins(vector<double> safetyMargins_in);
  void setPerimeterSafetyMargins(double safetyMargins_in);
  void setPathDirection(int dir);

  /* better methods for accessing data- since these return instances of the vector, not pointers
     to the vector, they copy the data (its a little implicit rather than explicit, but it isn't bad)
     it protects member variables, and does not require allocating/ freeing memory. */
  vector<double> getStartingState();
  vector<double> getFinalState();
  vector<double> getStartingControls();	
  vector<double> getFinalControls();	
  vector<double> getRmatrix();
  vector<double> getQmatrix();
  vector<double> getObstacleSafetyMargins();
  vector<double> getPerimeterSafetyMargins();
  vector<point2arr> getPolyObstacles();
  int getTrafficState();

  point2arr getBoundingPolygon();
  // takes the convex hull of the boundary and returns that...
  point2arr getBoundingPolytope();
  /* The idea here is that obstacles will be polygons inside this structure, which can be accessed as 
     such, or can be converted into a linear form and accessed that way. however, none of this
     is implemented yet */
//  vector<point2arr> getObstacles();
  vector<CSpecsObstacle> getLinearObstacles();

  /*! All of these get utilities make a copy of the original.  This is good because it protects the
   * private variables, but it's bad because it creates a possible memory leak.  Any array passed
   * out from these functions should be DELETED after getting used.
   */
  double getMaxVelocity();
  double getMaxAcc();
  double getMaxBraking();
  double getMaxSteeringAngle();
  double getMaxSteeringRate();

  BitmapParams getBitmapParams();
  CostMap *getCostMap();
  vector<point2> getBoundingPolygon_Vector();
  // takes the convex hull of the boundary and returns that...
  vector<point2> getBoundingPolytope_Vector();
	
  int getPathDirection();

  vector<CSpecsLinearConstraint> getPerimeterLinearConstraints();

  /*! CSpecs destructor. Releases all dynamically created objects.
   */
  ~CSpecs()
  {
    /* Where is initialGuess being set/used? */
    //delete initialGuess;

  }

};

#endif


