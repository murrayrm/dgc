/*
 *  ZonePlannerInterface.h
 *  
 *
 *  Created by Northrop Grumman on 8/6/07.
 *  Copyright 2007 Northrop Grumman. All rights reserved.
 *
 */

#ifndef ZONEPLANNERINTERFACE_H
#define ZONEPLANNERINTERFACE_H

#include <assert.h>
#include <stdlib.h>

/*! PointXY struct.  This struct simply contains an x and y position.
 */
struct PointXY
{
	double x;
	double y;
};

/*! Obstacle struct.  This struct represents an obstacle, and it consists
 * of six fields: (x,y) position, (x,y) velocity, and (x,y) dimension.
 */
struct Obstacle
{
	double x;
	double y;
	double dx;
	double dy;
	double length;
	double width;
};


/*! ZonePlannerInterface class.  This class is designed as an interface between
 * The traffic planner and the trajectory planners.  Specifically, this interface
 * should be used to pass information about the problem to be solved to the
 * trajectory planner.  It is intended to be used for both zone planner (OTG) and
 * clothoid planner.  It contains fields to give information about: initial and
 * final state and controls; maximum limits on acceleration, velocity, braking, 
 * steering angle, and steering rate; a value for modifying the roll constraint;
 * a bounding polytope; a set of obstacles; cost function weights; Q and R 
 * matrices; minimum and maximum time of trajectory; and an initial guess.
 * \brief North side interface for zone planner & clothoid planner
 */

class ZonePlannerInterface
{
	private:
	
   /*! Starting State (x,y,v,theta) */
	double startingState[4];
   /*! Final State (x,y,v,theta) */
	double finalState[4];
   /*! Starting Controls (a, phi) */
	double startingControls[2];
   /*! Final Controls (a, phi) */
	double finalControls[2];
	
   /*! Maximum acceleration constraint */
	double maxAcc;
   /*! Maximum breaking (minimum acceleration) constraint */
	double maxBreaking;

   /*! Maximum velocity constraint */
	double maxVelocity;
   /*! Maximum steering angle constraint */
	double maxSteeringAngle;
   /*! Maximum steering rate constraint */
	double maxSteeringRate;
	
   /*! Hcg, constant to modify roll constraint */
	double Hcg;
	
   /*! A set of points, representing the bounding polytope */
	PointXY* boundingPolytope;
   /*! number of points in the polynomial */
	int numPolyPts;
	
   /*! Container for all the obstacles in the polytope */
	Obstacle* myObstacles;

   /*! number of obstacles */
	int numObstacles;

   /*! cost function weights.  The first number is a weighting on the
    * distance from center, and the rest of the numbers are weights for
	* each obstacle.  Thus length(costFunctionWeights) should equal 
	* numObstacles + 1;
	*/
	double costFunctionWeights[3];
	
	/*! Q matrix, delivered row-by-row in a 16 element vector. */
	double QmatrixRowByRow[16];
	
	/*! R matrix, delivered row-by-row in a 16 element vector. */
	double RmatrixRowByRow[4];
	
	/*! Number of points in the solution trajectory. */
	int numPointsInTrajectory;

	/*! Minimum and maximum times (i.e. durations) for the solution trajectory. */	
	double minTrajectoryTime;
	double maxTrajectoryTime;
	
	/*! Initial Guess. */
	double* initialGuess;
	
	public:

  /*! ZonePlannerInterface constructor.
   */
	ZonePlannerInterface()
	{
		double zeroArray[4] = {0,0,0,0};
		double idmat2by2[4] = {1,0,1,0};
		double idmat4by4[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
		setStartingState(zeroArray);
		setFinalState(zeroArray);
		setCostFunctionWeights(zeroArray);
		setQmatrix(idmat2by2);
		setRmatrix(idmat4by4);

		boundingPolytope = NULL;
		numPolyPts = 0;
		maxAcc = 5;				//  m/s^2
		maxBreaking = -2;		//  m/s^2
		maxVelocity = 20;		//  m/s
		maxSteeringAngle = 45;	//	degrees
		maxSteeringRate = 45;	//	deg/s
		Hcg = 10; // FIND OUT THE RIGHT NUMBER TO PUT HERE
		
		myObstacles = NULL;
		numObstacles = 0;
	}

  /*! ZonePlannerInterface constructor that takes a starting state and a
   * final state as its input.
   */
	ZonePlannerInterface(double startingState_in[4], double finalState_in[4])
	{
		ZonePlannerInterface();
		assert(startingState_in);
		assert(finalState_in);
		setStartingState(startingState_in);
		setFinalState(finalState_in);
	}

  /*! Various setting and getting utilities
   */
	
	void setStartingState(double* startingState_in)	{
		assert(startingState_in);
		for (int i=0; i<4; i++)
			startingState[i] = startingState_in[i];
	}
	void setFinalState(double* finalState_in) {
		assert(finalState_in);
		for (int i=0; i<4; i++)
			finalState[i] = finalState_in[i];
	}
	void setStartingControls(double* startingControls_in)	{
		assert(startingControls_in);
		for (int i=0; i<2; i++)
			startingControls[i] = startingControls_in[i];
	}
	void setFinalControls(double* finalControls_in)	{
		assert(finalControls_in);
		for (int i=0; i<2; i++)
			finalControls[i] = finalControls_in[i];
	}
	void setCostFunctionWeights(double* weights_in)	{
		assert(weights_in);
		for (int i=0; i<3; i++)
			costFunctionWeights[i] = weights_in[i];
	}
	void setRmatrix(double* Rmatrix_in)	{
		assert(Rmatrix_in);
		for (int i=0; i<4; i++)
			RmatrixRowByRow[i] = Rmatrix_in[i];
	}
	void setQmatrix(double* Qmatrix_in)	{
		assert(Qmatrix_in);
		for (int i=0; i<16; i++)
			QmatrixRowByRow[i] = Qmatrix_in[i];
	}

	void setBoundingPolytope(PointXY* poly_in, int numPts) {
		assert(poly_in);
		boundingPolytope = poly_in;
		numPolyPts = numPts;
	}

	void setObstacles(Obstacle* obst_in, int numObst) {
		assert(obst_in);
		myObstacles = obst_in;
		numObstacles = numObst;
	}

 
  /*! All of these get utilities make a copy of the original.  This is good because it protects the
   * private variables, but it's bad because it creates a possible memory leak.  Any array passed
   * out from these functions should be DELETED after getting used.
   */
	double* getStartingState()	{
		double *tmp = new double[4];
		for (int i=0; i<4; i++)
			tmp[i] = startingState[i];
		return tmp;
	}
	double* getFinalState() {
		double *tmp = new double[4];
		for (int i=0; i<4; i++)
			tmp[i] = finalState[i];
		return tmp;
	}
	double* getStartingControls()	{
		double *tmp = new double[2];
		for (int i=0; i<2; i++)
			tmp[i] = startingControls[i];
		return tmp;
	}
	double* getFinalControls()	{
		double *tmp = new double[2];
		for (int i=0; i<2; i++)
			tmp[i] = finalControls[i];
		return tmp;
	}
	double* getCostFunctionWeights()	{
		double *tmp = new double[3];
		for (int i=0; i<3; i++)
			tmp[i] = costFunctionWeights[i];
		return tmp;
	}
	double* getRmatrix()	{
		double *tmp = new double[4];
		for (int i=0; i<4; i++)
			tmp[i] = RmatrixRowByRow[i];
		return tmp;
	}
	double* getQmatrix()	{
		double *tmp = new double[16];
		for (int i=0; i<16; i++)
			tmp[i] = QmatrixRowByRow[i];
		return tmp;
	}	
	
   /*! ZonePlannerInterface destructor. Releases all dynamically created objects.
	*/
  ~ZonePlannerInterface()
  {
  delete boundingPolytope;
  delete initialGuess;
  delete myObstacles;
  }
};

#endif


