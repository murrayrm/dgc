/*
 *  CSpecs.h
 *  
 *
 *  Created by Northrop Grumman on 8/6/07.
 *  Copyright 2007 Northrop Grumman. All rights reserved.
 *
 */

#ifndef ZONEPLANNERINTERFACE_H
#define ZONEPLANNERINTERFACE_H

#include <vector>
#include <assert.h>
#include <stdlib.h>
#include <frames/point2.hh>
#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
//#include <bitmap/Bitmap.hh>

/*! Obstacle struct.  This struct represents an obstacle, and it consists
 * of six fields: (x,y) position, (x,y) velocity, and (x,y) dimension.
 */
struct Obstacle
{
	double x;
	double y;
	double dx;
	double dy;
	double length;
	double width;
	double costFunctionWeight;  
};


/*! CSpecs class.  This class is designed as an interface between
 * The traffic planner and the trajectory planners.  Specifically, this interface
 * should be used to pass information about the problem to be solved to the
 * trajectory planner.  It is intended to be used for both zone planner (OTG) and
 * clothoid planner.  It contains fields to give information about: initial and
 * final state and controls; maximum limits on acceleration, velocity, braking, 
 * steering angle, and steering rate; a bounding polytope; a set of obstacles; 
 * Q and R matrices; minimum and maximum time of trajectory; and an initial guess.
 * \brief Interface for zone planner & clothoid planner
 */

class CSpecs 
{
private:
	
  /*! Starting State (x,y,v,theta) */
  double startingState[4];
  /*! Final State (x,y,v,theta) */
  double finalState[4];
  /*! Starting Controls (a, phi) */
  double startingControls[2];
  /*! Final Controls (a, phi) */
  double finalControls[2];
	
  /*! Maximum acceleration constraint */
  double maxAcc;
  /*! Maximum breaking (minimum acceleration) constraint */
  double maxBraking;

  /*! Maximum velocity constraint */
  double maxVelocity;
  /*! Maximum steering angle constraint */
  double maxSteeringAngle;
  /*! Maximum steering rate constraint */
  double maxSteeringRate;
	
  /*! A set of points, representing the bounding polytope */
  std::vector<point2> boundingPolytope;
	
  /*! Container for all the obstacles in the polytope */
  std::vector<Obstacle> myObstacles;

  /*! Q matrix, delivered row-by-row in a 16 element vector. */
  double QmatrixRowByRow[16];
	
  /*! R matrix, delivered row-by-row in a 16 element vector. */
  double RmatrixRowByRow[4];
	
  /*! Number of points in the solution trajectory. */
  int numPointsInTrajectory;

  /*! Minimum and maximum times (i.e. durations) for the solution trajectory. */	
  double minTrajectoryTime;
  double maxTrajectoryTime;

  /*! Costmap object to be used with clothoid planner. */
  //	CostMap myCostMap;
  BitmapParams bmParams;
	
  /*! Initial Guess. */
  double* initialGuess;
	
public:

  /*! CSpecs constructor. */
  CSpecs()
  {
    double zeroArray[4] = {0,0,0,0};
    double idmat2by2[4] = {1,0,1,0};
    double idmat4by4[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
    setStartingState(zeroArray);
    setFinalState(zeroArray);
    setQmatrix(idmat2by2);
    setRmatrix(idmat4by4);

    maxAcc = 5;				//  m/s^2
    maxBraking = -2;		//  m/s^2
    maxVelocity = 5;		//  m/s
    maxSteeringAngle = 45;	//	degrees
    maxSteeringRate = 45;	//	deg/s
		
  }

  /*! CSpecs constructor that takes a starting state and a
   * final state as its input.
   */
  CSpecs(double startingState_in[4], double finalState_in[4])
  {
    CSpecs();
    assert(startingState_in);
    assert(finalState_in);
    setStartingState(startingState_in);
    setFinalState(finalState_in);
  }

  /*! Various setting and getting utilities */
	
  void setStartingState(double* startingState_in);
  void setFinalState(double* finalState_in);
  void setStartingControls(double* startingControls_in);	
  void setFinalControls(double* finalControls_in);
  void setRmatrix(double* Rmatrix_in);
  void setQmatrix(double* Qmatrix_in);
  void setBoundingPolytope(vector<point2> poly_in);
  void setObstacles(vector<Obstacle> obst_in);
  void setCostMap(BitmapParams bmParams_in);
  void setMaxVelocity(double velMax);
  

  /*! All of these get utilities make a copy of the original.  This is good because it protects the
   * private variables, but it's bad because it creates a possible memory leak.  Any array passed
   * out from these functions should be DELETED after getting used.
   */
  double* getStartingState();
  double* getFinalState();
  double* getStartingControls();	
  double* getFinalControls();	
  double* getRmatrix();	
  double* getQmatrix();		
  double getMaxVelocity();
  BitmapParams getCostMap();
  vector<point2> getBoundingPolytope();
	
  /*! CSpecs destructor. Releases all dynamically created objects.
   */
  ~CSpecs()
  {
    /* Where is initialGuess being set/used? */
    //delete initialGuess;

  }

};

#endif


