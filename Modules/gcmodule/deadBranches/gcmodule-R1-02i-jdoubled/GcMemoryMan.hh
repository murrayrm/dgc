/*!
 *  \file  GcMemoryMan.hh
 *  \brief A basic memory manager to simulate dynamic allocations from a static store
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  July 24 2007
 *
 * To avoid the possible latency of asking hte OS for dynamic memory
 * in the loop, we will allocate a large chunk of memory up front,
 * and divy out from that as necessary.
 *
 */

#ifndef _GCMEMORYMAN_HH_0239TU02N4GU3J5RHNU___
#define _GCMEMORYMAN_HH_0239TU02N4GU3J5RHNU___

#include "gcmodule/GcModuleLogger.hh"

#include <deque>
#include <vector>
#include <list>
using std::list;
using std::deque;
using std::vector;

/// A little helper class for working with template classes.
/// gdb has problems setting a breakpoint in template code -- 
/// so you can instantiate this simple class at the desired
/// break area, set your debugger to break on the constructor
/// and step out of the funciton
class DebuggerBreakPoint {
public:
  DebuggerBreakPoint() {
    return;
  }
};

template< class T >
class GcMemoryMan
{

  struct unit{
    //T t;
    char  t[sizeof(T)];
    void* heap;
    int   i;
  } ;
  typedef vector<unit> HeapSpace;
  typedef deque<HeapSpace*> HeapList;
  HeapSpace*  m_heap;
  HeapList    m_oldHeaps;
  vector<int>  m_avail;
  int         m_used;
  IGcLogger*  m_logger;

  enum { nGrowthFactor = 4 } ;

  void grow( int nOldSize, int nNewSize ) {
    /// Create a new heap.  We will push the indices that do not correspond to those already allocated
    /// on the avail list.  As those old allocations are freed, they will be be used on this new heap
    /// for new allocations over time.  The old heap is stored away, eventually will empty, but we will
    /// not deallocate until destruction of the mem-manager as book-keeping of how each heap seems
    /// extraneous, and as long as we grow geometrically, the wasted memory remains a fraction of the 
    /// total consumed (instead of growing polynomially or something horrible).
    HeapSpace* t = new HeapSpace();
    t->reserve( nNewSize );
    m_avail.reserve( nNewSize );
    for( int i=nOldSize; i<nNewSize; i++ )
      m_avail.push_back(i);
    if( m_used == 0 ) {
      delete m_heap;
      m_heap = NULL;
    }
    else if( m_heap ) {
      m_oldHeaps.push_back(m_heap);
    }
    m_heap = t;
  }

public:

  /*! return the first available index in our array of preallocated objects */
  T* gcmmNew() 
  {
    /// if we dont have any left, do something...
    if( m_avail.empty() ) {
      GcTimebomb b( m_logger, 10, __LINE__ );
      int nOldSize = m_heap->capacity();
      int nNewSize = nGrowthFactor * nOldSize;
      if( m_logger ) {
        m_logger->gclog(1)  << "GcMemoryMan::gcmmNew : RAN OUT OF SPACE --- GROWING TO SAVE YOU!! --- INCREASE YOUR INITIAL SIZE. New size= " 
                            << nNewSize
                            << " : " << m_logger->usecTime() << endl;
      }
      DebuggerBreakPoint bp;
      grow( nOldSize, nNewSize );
    }
      

    /// get the first available index
    int i = m_avail.back();
    m_avail.pop_back();
    m_used++;

    /// call a "placement new" -- runs the constructor, but doesn't allocate, use
    /// the specified memory buffer
    unit* u = &(*m_heap)[i];
    u->i = i;
    u->heap = m_heap;
    T* r = new(&u->t) T; 
    if( m_logger ) {
      m_logger->gclog(10) << "GcMemoryMan::gcmmNew ok: " << i << ", " << r <<  endl;
    }
    return r;
  }
  
  /*! reclaim the memory slot ... */
  void gcmmDelete( T* m )
  {
    /// First try to locate it via the heap pointer and index stored after its location in mem
    unit* u = (unit*)m;
    HeapSpace* s = m_heap;
    if( s != u->heap ) {
      for( typename HeapList::iterator old=m_oldHeaps.begin();
           old!=m_oldHeaps.end() && *old!=u->heap;
           s=*(++old) )
      {}
    }
    /// heap has been located..
    if( s == u->heap ) {
      int j = u->i;
      if( j > 0 && (unsigned)j < s->capacity() &&
          &(*s)[j] == (void*)&u->t ) 
      {
        ((T*)u)->~T();
        m_used--;
        m_avail.push_back(j);
        m_logger && m_logger->gclog(10) << "GcMemoryMan::gcmmDelete ok: " 
                                        << j << endl;
        return;
      } else {
        m_logger && m_logger->gclog(1) << "GcMemoryMan::gcmmDelete: error: either index out of range: " << j << endl;
        m_logger && m_logger->gclog(1) << "                            or: address mismatch (alleged,actual): (" << &(*s)[j] << ", " << &u->t << ")" << endl;
      }
    }
    else {
      m_logger && m_logger->gclog(10) << "GcMemoryMan::gcmmDelete: error: allged heap address: " << (void*)&u->t << endl;
    }
     
    
    /// Ok our more intelligent method of deleting has failed, so we will resort to a 
    /// search across all of the allocated memory area to see if we have match
    m_logger && m_logger->gclog(1) << "GcMemoryMan::gcmmDelete: error: could not index, resorting to brute force." << endl;
    
    int size = m_heap->capacity();
    int i=0;
    for( ; i<size; i++ ) {
      if( m == (void*)&(*m_heap)[i] ) {
        /// Call the destructor manually since we allocated the memory
        m->~T();
        m_used--;
        m_avail.push_back(i);
        if( m_logger ) {
          m_logger->gclog(10) << "GcMemoryMan::gcmmDelete bruteforce ok: " 
                              << i << endl;
        }
        return;
      }
    }
    if( i >= size ) {
      /// Ok, could not find this in our current heap... lets try the old heaps
      i = 0;
      for( typename HeapList::iterator old=m_oldHeaps.begin(); old!=m_oldHeaps.end(); old++ ) {
        HeapSpace* s = *old;
        int size = s->capacity();
        /// old heaps will only have allocations at indices greater than the 
        /// capacity of the older heap..
        for( ; i<size; i++ ) {
          if( m == (void*)&(*s)[i] ) {
            /// Call the destructor manually since we allocated the memory
            m->~T();
            m_used--;
            m_avail.push_back(i);
            if( m_logger ) m_logger->gclog(10) << "GcMemoryMan::gcmmDelete ok from old heap: " 
                                               << i << endl;
            return;
          }
        }

      }

      /// if this was not found in an old heap, then
      /// need to handle this as an error
      if( m_logger ) { 
      m_logger->gclog(1) << "GcMemoryMan::gcmmDelete failed to deallocate addr "
                         << m << " from preallocation starting at: " 
                         << &(*m_heap)[0] << endl;
      }
    }
  }

  ~GcMemoryMan() {
    delete m_heap;
    for( typename HeapList::iterator i=m_oldHeaps.begin(); i!=m_oldHeaps.end(); i++ ) {
      delete *i;
    }
  }

  GcMemoryMan( int preAllocateSize, IGcLogger* logger ) 
    : m_heap( NULL ),
      m_used( 0 ),
      m_logger( logger )
  {
    /// reserve the desired number of spots in our array
    grow( 0, preAllocateSize );
  }

};



#endif // _GCMEMORYMAN_HH_0239TU02N4GU3J5RHNU___
