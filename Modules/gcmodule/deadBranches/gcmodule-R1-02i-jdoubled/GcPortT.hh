/*!
 *  \file  GcPortT.hh
 *  \brief The IGcPort interface and GcPortT default implementation
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *
 *  IGcPort provides a consistent layer through which GcInterfaces can
 *  communicate to their variable delivery mechanisms.
 *  It provides queing of messages and requires pumping of the queue, to 
 *  send and receive from the underlying delivery mech.  
 *
 *  This class has become nearly completely redundant and merely adds
 *  yet another layer of queing to the GcInterface communications.  It
 *  is scheduled to be removed.
 *
 */

#ifndef _GCPORTT_HH_S0FD9HSGH248T92HT048FH__
#define _GCPORTT_HH_S0FD9HSGH248T92HT048FH__

#include <deque>
#include <list>
#include <map>
#include <sstream>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include "interfaces/sn_types.h"
#include "gcmodule/AutoMutex.hh"
#include "gcmodule/GcTimebomb.hh"
#include "gcmodule/IGcLogger.hh"
#include "gcmodule/GcMemoryMan.hh"
using std::list;
using std::map;
using std::string;

/// IGcPort
///
/// GcPort is a virtual class.  It should be
/// inherited anew for each combination of
/// message type and module being used.
/// Each time it is inherited,
/// serialize and deserialize must be defined.
///
/// A port is a connection from _a given module_
/// ..with a given shared _skynet key_
/// ...for a certain kind of _message type_ 
/// (the module and message type options are
/// defined in sn_types.hh, the skynet key is
/// defined when the port starts running.
/// 
/// A GcPort contains a list of messages that
/// have been sent.
class IGcPort
{
  public:
    virtual ~IGcPort() {}
    //__message WRITING functions
    //		virtual bool addMsg(Message * msg, int msgSize = -1) = 0; //adds a message to the queue

    //__message HANDLING functions
    virtual bool handleMsgs() = 0; //clears all of the messages that are ready to die
    virtual bool sendMsgs() = 0; //sends all of the recently added messages
    virtual bool recvMsgs() = 0; //receives all of the new messages
    virtual sn_msg getMsgType() = 0;
    virtual void setSendFlag(int) = 0; // use to set the send flag
    virtual bool getSendFlag()  = 0 ;

    // in some delivery cases, when a message becomes ready 
    // on the port, this condition can be signaled, allowing tighter loops
    virtual void installMessageReadyCondition( DGCcondition* cond ) = 0;

};

/// IGcMailman
/// 
/// abstracts the actual delivery mechanism from IGcPort
template <class MsgT>
class IGcMailman
{
  public:
    virtual bool hasNewMessage() = 0;
    virtual bool receive(MsgT* msg) = 0;
    virtual bool send(MsgT* msg) = 0;
    enum OperationMode { SENDER, RECEIVER } ;
    virtual bool setMode( OperationMode m ) = 0;
    virtual void installMessageReadyCondition( DGCcondition* cond ) = 0;

    virtual ~IGcMailman() {}
};

#ifndef OMIT_TEMPLATE_DEFS
#include "gcmodule/GcInProcMailman.template.hh"
#include "gcmodule/GcSkynetMailman.template.hh"
#endif

template <class Message>
class GcPortT : public IGcPort 
{
  public:
    GcPortT(int sn_key, sn_msg sn_msgtype, modulename sn_modname, IGcLogger* logger, bool bStale=false, int nTimeToHold=10 ) ;
    virtual ~GcPortT();

    //for now, this defines whether a given message has come in
    //or is going out
    enum msgStatus { READYTOKILL, READYTOSEND, SENT, RECEIVED, SENDERROR };

    //__message READING functions
    //gets the newest message sent or received
    Message* getNewestMessage(); //gets a pointer to the newest message
    void     releaseMessage( Message* );

    time_t lastMsgTimeSent();

    //__message WRITING functions
    bool addMsg(Message * msg, bool bAsynchronous=false ); //adds a message to the queue

    //__message HANDLING functions
    bool handleMsgs(); //clears all of the messages that are ready to die

    sn_msg getMsgType() { return m_msgtype; }
    int getNumMsgs() { AutoMutex a(m_qMutex); return (int)msgQ.size(); };
    bool haveMsgs() { AutoMutex a(m_qMutex); return !msgQ.empty(); };

    virtual void setSendFlag(int set) { // use to set the sendflag
      sendFlag = set; 
      m_pMailman->setMode( set ? IGcMailman<Message>::SENDER : IGcMailman<Message>::RECEIVER ) ;
    } 
    virtual bool getSendFlag() {return sendFlag;}

    virtual void installMessageReadyCondition( DGCcondition* cond );
  protected:
    /// basic memory manager to preallocate space for our msgs...
    GcMemoryMan<Message> m_msgmm;


    int sendFlag; //is this a sending (1) or receiving (0) type of port

    int m_prevCount;

    //wraps each message up with relevant data
    //including arrival time (such that we know
    //when it should be cleared), status and so
    //on.
    template<class MWM>
    class MessageWrapperT
      {
        public:
          MWM * msg; //the message in its serial or "to be sent" format
          int msgSize;
          time_t timeSentRcvd; //default behavior is to be clear after 100 ms, say
          msgStatus status; //if status is READYTOKILL, this message is as good as dead
          pthread_mutex_t m_dataBufferMutex; //?
      };
    typedef MessageWrapperT<Message> MessageWrapper ;
    typedef deque< MessageWrapper > MsgQ;
    MsgQ msgQ;
    pthread_mutex_t m_qMutex; //?

    //__Message handling
    IGcMailman<Message>* m_pMailman;
    bool m_bStale;
    int maxTimeToHold;
    sn_msg m_msgtype;

  public:

    bool sendMsgs(); //sends all of the recently added messages
    bool recvMsgs(); //receives all of the new messages
    bool sendMessage(Message * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex);


    class IGcMessageFilter
    {
      public:
        virtual ~IGcMessageFilter() {}
        virtual bool filter( Message* m ) = 0;
    };

    void changeMsgStatus( IGcMessageFilter& filter, int toStat, bool bOnlyFirst=true ) ;
    void moveMsgsStatus( msgStatus fromStat, msgStatus toStat ) ;
    void flushMsgs( ) ;

  protected:
  IGcLogger* m_logger;
};
#ifndef OMIT_TEMPLATE_DEFS
#include "gcmodule/GcPortT.template.hh"
#endif


#endif
