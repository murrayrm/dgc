#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "dgcutils/DGCutils.hh"
#define OMIT_TEMPLATE_DEFS
#include "GcPortT.hh"
#include "GcModule.hh"



// ---------------- GcModule TODO warnings
//
//#warning " the inprocess mailman is not threadsafe.  insert automutex on add|remove.listener which is also synched wiht broadcast"

//--------------------GcModule code

bool GcModule::Start(void)
{ 
	//start the threads
	gcMainEndThreadFlag = false;
	DGCstartMemberFunctionThread(this, &GcModule::gcMainThread);

	//gcMessageEndThreadFlag = 0;
  GcSimplePortHandlerThread::ReStart();

	return true;
};

GcModule::GcModule( string sName, ControlStatus* cs, MergedDirective* md, 
    long nControlUSleepTime, long nMessageLoopUsleepTime ) :
  GcModuleLogger(sName),
  GcSimplePortHandlerThread( false, nMessageLoopUsleepTime ),
  m_nControlLoopUsleepTime(nControlUSleepTime),
  m_container(NULL),
  m_pMergedDirectiveBase(md),
  m_pControlStatusBase(cs)
{
  DGCcreateCondition( & m_condMainThreadDone );
} 

GcModule::~GcModule()
{
  DGCdeleteCondition( &m_condMainThreadDone );
}

GcPortHandler::~GcPortHandler()
{
	for (int ii=0;ii<(int)portQ.size();ii++)
	{ }
	portQ.clear();
}


void GcPortHandler::pumpPorts() {

  for (int ii=0;ii<(int)portQ.size();ii++)
  {
    if (portQ[ii]->getSendFlag())
    {
      portQ[ii]->sendMsgs();
    }
    else
    {
      portQ[ii]->recvMsgs();
    }
    portQ[ii]->handleMsgs();
  }
}

bool GcPortHandler::addPort(IGcPort* newport)
{
	portQ.push_front(newport);
	return true;
}

bool GcPortHandler::remPort(int num)
{
	if ((int)portQ.size() > num)
	{
		portQ.erase(portQ.begin()+num);
		return 1;
	}
	else
	{
		return 0;
	}
}


void GcModule::gcMainThread()
{
  DGCSetConditionFalse(m_condMainThreadDone);
	while (!gcMainEndThreadFlag)
	{
		arbitrate(m_pControlStatusBase, m_pMergedDirectiveBase);
    gclog(9) << " * control status at      " << time(0) << ": " << *m_pControlStatusBase << endl;
    gclog(9) << " * arbitratedDirective at " << time(0) << ": " << *m_pMergedDirectiveBase << endl;
		control(m_pControlStatusBase, m_pMergedDirectiveBase);  
    usleep( m_nControlLoopUsleepTime );
	}
  DGCSetConditionTrue(m_condMainThreadDone);
}

