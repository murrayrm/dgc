#ifndef _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__
#define _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__

#include <deque>
#include <list>
#include <map>
#include <sstream>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include "interfaces/sn_types.h"
#include "gcinterfaces/GcModuleInterfaces.hh"
#include "gcmodule/AutoMutex.hh"
#include "gcmodule/GcModuleLogger.hh"
#include "gcmodule/GcPortHandler.hh"

using std::list;
using std::map;
using std::string;


class MergedDirective : public OStreamable
{
  public:
    int id;
    virtual ~MergedDirective() {}
    virtual string toString() const { std::ostringstream s(""); s << id; return s.str(); }
};

class ControlStatus : public OStreamable
{
  public:
    enum statusTypes { FAILED, RUNNING, STOPPED } status;

    virtual ~ControlStatus() {}
    virtual string toString() const { std::ostringstream s(""); s << status; return s.str(); }

};

// forward declare the container class
class GcModuleContainer;


class GcModule : public GcModuleLogger, public GcSimplePortHandlerThread
{
  protected:
    long m_nControlLoopUsleepTime;
    //long m_nMessageLoopUsleepTime;
    string m_sName;
    GcModuleContainer* m_container;
    friend class GcModuleContainer;
    void setContainer( GcModuleContainer* c ) { m_container = c ;}
  public:
    GcModule( string sName, ControlStatus* cs, MergedDirective* md, 
        long nControlUSleepTime=10000, long nMessageLoopUsleepTime=10000 );
    virtual ~GcModule(void);

    string getName() { return m_sName; }

    // these are used to start and stop both the messaging thread and the main 
    // arbitrate/control thread
    DGCcondition m_condMainThreadDone;
    bool Start();
    bool Stop() { 
      bool bSuccess = true;
      bSuccess &= GcSimplePortHandlerThread::Stop();
      gcMainEndThreadFlag=true;
      DGCWaitForConditionTrue( m_condMainThreadDone );
      return bSuccess;
    }
    int IsStopped() { return (gcMessageEndThreadFlag && gcMainEndThreadFlag); }


    /// main functions of the module
    // each of the following arbitrate and control take two pointer parameters.  
    // Each pointer will be aimed at datastructures that should be created by the sub-class,
    // and which should have been provided to the constructor of this base class.
    // Arbitrate and control are called in the main thread created by this object -- the
    // subclass just needs to implement them -- they are worker-thread stubs.
    //
    //Arbitrate should receive external directives from the controlling module above,
    //process them against this modules current control status and generate a
    //mergedDirective to pass into control()
    virtual void arbitrate(ControlStatus* /*input*/controlStatus, MergedDirective* /*output*/ md) = 0;
    // control should process the mergedDirective from arbitrate, perform the main 
    // algorithms of this module (tactics), generate and send directives to a controlled 
    // module below, receive status from controlled modules and update the control-status
    virtual void control(ControlStatus* /*output?*/controlStatus, MergedDirective* /*input*/mergedDirective) = 0;

  protected:

    /*! This is the module's main thread. */
    bool gcMainEndThreadFlag;
    void gcMainThread();

    /*! This thread is supposed to handle all communication. */
    int gcMessageEndThreadFlag;

    // these pointers are set in the contructor to aim at the sub-classes instantiations
    // of MergedDirective and ControlStatus subclasses
    MergedDirective* m_pMergedDirectiveBase;
    ControlStatus* m_pControlStatusBase;

};




#endif // _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__
