#ifndef _IGCLOGGER_HH_7YU4HN87FBYC37BC8ECV78GC97GB__
#define _IGCLOGGER_HH_7YU4HN87FBYC37BC8ECV78GC97GB__

/// Defines an interface guaranteed to be implemented to log
/// messages (somehow - depends on subclass)
//  Calls to log at a particular level are inteded to be logged if
//  the logger is running at a level equal or higher than this level, only.
class IGcLogger
{
  public:
    virtual ~IGcLogger() {} 
    virtual int setLogLevel( int nLevel ) = 0; 
    virtual void log( int nLevel, const char* format, ... ) = 0;
    virtual ostream& log(int nLevel) = 0;
};

#endif // _IGCLOGGER_HH_7YU4HN87FBYC37BC8ECV78GC97GB__
