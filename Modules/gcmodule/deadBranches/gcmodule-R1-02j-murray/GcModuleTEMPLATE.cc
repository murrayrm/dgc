/*!
 *  \file  GcModuleTEMPLATE.cc
 *  \brief
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *  This file is used to generate the GcInterface template code 
 *  needed for the test applications.  
 *
 *  In your make file you can #define OMIT_TEMPLATE_DEFS which 
 *  will prevent the bulk of the template code from being
 *  included in those files, and then compile a single file
 *  (as is the case here) without that definition -- trick is
 *  to ensure that we force the template code to be included.
 *  We can achieve that by calling generateNorthface which
 *  will instantiate a GcInterface and hence all of the 
 *  supporting tempalte code behind it.  
 *
 */

#include "GcModuleTestDirective.hh"
BOOST_CLASS_EXPORT_GUID( test123, "test123_derived" )

void templatecodeInstantiation() {
 GcInterfaceTest::Northface* n1 = GcInterfaceTest::generateNorthface( -1, (GcModule*)NULL );
 GcInterfaceTest::releaseNorthface(n1);

 GcIntTestInProc::Northface* n2 = GcIntTestInProc::generateNorthface( -1, (GcModule*)NULL );
 GcIntTestInProc::releaseNorthface(n2);
}

