/*!
 *  \file  GcTimebomb.hh
 *  \brief A simple class to log the creation and destruction of object
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  7/23/2007
 *
 * GcTimebomb will log to an attached IgcLogger its creation and destruction
 * along with given tag id (intended to hold a line-number).
 * Its intended use is to serve as a temporary object on a line .. when the 
 * begins a log entry is made, when the line ends (after some long runnign 
 * statement hopefully) a log entry is made again.
 */

#ifndef _GCTIMEBOMB_HH_OIDS0IFG049NT8234HY__
#define _GCTIMEBOMB_HH_OIDS0IFG049NT8234HY__


#include "gcmodule/IGcLogger.hh"
#include <sys/time.h>
#include <sys/resource.h>

class GcTimebomb {
  IGcLogger*  m_logger;
  int         m_level;
  int         m_id;
  uint64_t    m_start;
  rusage      m_usage;
  public:
    GcTimebomb( IGcLogger* logger, int loglevel, int id ) :
      m_logger(logger), m_level(loglevel), m_id(id)
    {
      if( m_logger && m_logger->isLevel(m_level) ) {
  getrusage( RUSAGE_SELF, &m_usage );
  m_start = m_logger->usecTime();
  m_logger->gclog(m_level) << "GcTimebomb " << m_id<< ": " << "started at" << " " << m_start << endl;
      }
    }
    ~GcTimebomb()
    {
      if( m_logger && m_logger->isLevel(m_level) ) {
  rusage usageNow;
  getrusage( RUSAGE_SELF, &usageNow );
  int nVolSwitches =(usageNow.ru_nvcsw - m_usage.ru_nvcsw);
  int nInVolSwitches =  (usageNow.ru_nivcsw - m_usage.ru_nivcsw) ;
  uint64_t now = m_logger->usecTime();
  m_logger->gclog(m_level)  << "GcTimebomb " << m_id<< ": stopped at " << now 
                            << " spanning: " << now - m_start << " us" 
                            << "; with context switches (voluntary,invol): (" 
                            << nVolSwitches << ", " << nInVolSwitches << ")" <<endl;
      }
    }

};


#endif
