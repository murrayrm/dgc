#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"


//--------------------GcModule code

bool GcModule::Start(void)
{ 
	//start the threads
	gcMainEndThreadFlag = 0;
	DGCstartMemberFunctionThread(this, &GcModule::gcMainThread);

	gcMessageEndThreadFlag = 0;
	DGCstartMemberFunctionThread(this, &GcModule::gcMessageThread);

	return true;
};

GcModule::~GcModule()
{
	for (int ii=0;ii<(int)portQ.size();ii++)
	{
		delete portQ[ii];
	}
	portQ.clear();
}

bool GcModule::addPort(IGcPort* newport)
{
	portQ.push_front(newport);
	return true;
}

bool GcModule::remPort(int num)
{
	if ((int)(int)portQ.size() > num)
	{
		delete portQ[num]; //delete the pointer?
		portQ.erase(portQ.begin()+num);
		return 1;
	}
	else
	{
		return 0;
	}
}


void GcModule::gcMainThread()
{
	while (!gcMainEndThreadFlag)
	{
		arbitrate(m_controlStatus, m_mergedDirective);

		control(m_controlStatus, m_mergedDirective);  
    usleep( m_nControlLoopUsleepTime );
	}
}

void GcModule::gcMessageThread()
{
	int ii;

	while (!gcMessageEndThreadFlag)
	{

		for (ii=0;ii<(int)portQ.size();ii++)
		{
			if (portQ[ii]->getSendFlag())
			{
				portQ[ii]->sendMsgs();
			}
			else
			{
				portQ[ii]->recvMsgs();
			}
			portQ[ii]->handleMsgs();
		}

    usleep( m_nMessageLoopUsleepTime );
	}
}

IGcPort* GcModule::getPortofType(sn_msg sn_msgtype)
{
	for (int ii=0;ii<(int)portQ.size();ii++)
	{
		if (portQ[ii]->getMsgType() == sn_msgtype) { return portQ[ii]; }
	}
	return NULL;
}
