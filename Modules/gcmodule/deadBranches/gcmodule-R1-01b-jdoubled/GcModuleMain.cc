#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"
#include <assert.h>
#include "GcModuleTestDirective.hh"
#include "GcInterface.hh"
#include "GcModuleContainer.hh"




#define CP1REPTIME 5
class GcModuleA : public GcModule
{
  public:
    GcInterfaceTest::Southface* southface;
    GcInterfaceTest::Northface* verifyTest;

    GcIntTestInProc::Northface* verifyInProcInterfaceNorth;
    GcIntTestInProc::Southface* testInProcInterfaceSouth;


    class ControlStatusA : public ControlStatus {};
    class MergedDirectiveA : public MergedDirective {
      public:
        enum Status { NONE, STOP, RUN };
        Status directive;
    };
    ControlStatusA m_ctrlStatusA;
    MergedDirectiveA m_mgddA;

    GcModuleA( int sn_key ) : GcModule( "GcModuleTestSend", &m_ctrlStatusA, &m_mgddA )
  {
    southface = GcInterfaceTest::generateSouthface(sn_key, this);
    testInProcInterfaceSouth = GcIntTestInProc::generateSouthface(sn_key, this);
    verifyInProcInterfaceNorth = GcIntTestInProc::generateNorthface(sn_key, this);

    southface->setStaleThreshold(14);
  }

    ~GcModuleA() {
      GcInterfaceTest::releaseSouthface( southface );
      GcIntTestInProc::releaseSouthface( testInProcInterfaceSouth );
      GcIntTestInProc::releaseNorthface( verifyInProcInterfaceNorth );
    }

    void arbitrate(ControlStatus* cs, MergedDirective* md)
    {
      MergedDirectiveA& mergedDirective = *(MergedDirectiveA*)md;

      log(1) << "mergedDir set to run\n" << std::endl;
      mergedDirective.directive = MergedDirectiveA::RUN;
    }

    void control(ControlStatus* cs, MergedDirective* md)
    {
      assert( cs );
      assert( md );
      //ControlStatusA& controlStatus = *dynamic_cast<ControlStatusA*>(cs);
      MergedDirectiveA& mergedDirective = *(MergedDirectiveA*)md;

      if (mergedDirective.directive == MergedDirectiveA::RUN)
      {
        //just some junk
        directive1 d1;
        static int i = 60000;
        d1.id = i++;

        deque<unsigned> noAck = southface->orderedDirectivesWaitingForResponse();
        log(1) << "dirctives that have not been responeded too: ";
        copy( noAck.begin(), noAck.end(), ostream_iterator<unsigned>( log(1), ", " ) );
        log(1) << endl;


        //periodically sends new messages
        {
          southface->sendDirective(&d1);
          testInProcInterfaceSouth->sendDirective(&d1); // validate delivery, with a loopback
          log(1) << "directive added: " << d1.data << ":" << d1.getDirectiveId() << "\n";
        }

        log(1) << "validating directives....: " << endl;
        while( verifyInProcInterfaceNorth->haveNewDirective() )
        {
          directive1 v;
          if( !verifyInProcInterfaceNorth->getNewDirective( &v ) ) {
            log(1) << "ASSERT ERROR in GcModule: could not get directive when haveNewDirective indicated available" << endl;
            exit(0);
          }

          directive1 s = testInProcInterfaceSouth->getSentDirective( v.getDirectiveId() );

          if( !(s==v) ) {
            log(1) << "ASSERT ERROR in GcModuleMain:"
              << "received and sent directictives differ" << endl;
            log(1) << "sent directive " << s.getDirectiveId() << endl << s << endl;
            log(1) << "received directive " << v.getDirectiveId() << endl << v << endl;
            exit(0);
          }

          log(1) << "validated directive " << v.getDirectiveId() << endl;
        }


        if( southface->haveNewStatus() )
        {
          log(2) << "have new status" << endl;

          status1* stat = southface->getStatusOf( d1.id );
          if( stat )
          {
            log(1) << "received status on " << stat->getDirectiveId() ;
            log(1) << " data: " << stat->blah << endl;
          }

          status1* latest = southface->getLatestStatus();
          if( latest )
          {
            log(1) << " latest status was for id: " << latest->getDirectiveId() << endl;
          }
          else
            log(1) << " there was no latest status " << endl;
        }
        else
          log(2) << "no new status available" << endl;

        usleep(200000); //TEMPORARY

      }
      return;
    }

    void UT_1()
    {
       // make sure the message pump is running
      ReStart();

      directive1 d1;
      int i = 8000;
      d1.id = i; 

      for( int x=0; x<10; x++ )
      {
        d1.id =  i++;
        southface->sendDirective(&d1);
        //handler.pumpPorts();
        usleep(1100);
      }
      for( int x=0; x<10; x++ )
      {
        cout << "waiting for status... " << endl;
        //log(1) << "waiting for status... " << endl;
        while( !southface->haveNewStatus() ) {usleep(10000);}
        status1* s = southface->getLatestStatus();
        cout << "statusid: " << s->getDirectiveId();
        //log(1) << "statusid: " << s->getDirectiveId();
      }
      if( southface->haveNewStatus() )
        log(1) << "still have more status available!!!" << endl;
    }
};



int main(void) //main(int argc, char **argv)
{
  GcModuleContainer modules;
  GcModuleA moduleA( 321 );
  modules.addModule( &moduleA );
  modules.setLogLevel(15);

  //log(1) << "starting TestModule (northern/control'er)\n" << endl;


  //GcSimplePortHandlerThread handler;
  //GcIntTestInProc::Southface* testInProcInterfaceSouth2;
  //testInProcInterfaceSouth2 = GcIntTestInProc::generateSouthface( 0, &handler, NULL ) ;

  moduleA.UT_1();

  moduleA.Start();

  sleep( 10 );

  moduleA.Stop();



  //GcIntTestInProc::releaseSouthface( testInProcInterfaceSouth2 );

}
