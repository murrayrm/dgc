#ifndef _GCINPROCMAILMAN_TEMPLATE_HH_9283UHG87B34HJ8DHJ87HG982HRG072NFH7G284NHG__ 
#define _GCINPROCMAILMAN_TEMPLATE_HH_9283UHG87B34HJ8DHJ87HG982HRG072NFH7G284NHG__

template<class MsgT>
GcInProcMailman<MsgT>::GcInProcMailman( string msgtype, int modulename  ) :
  depot( Singleton< GcInProcMailDepot >::Instance() )
{
  AutoMutex::initMutex(&m_mtxQ);
  stringstream s; s << "_" << msgtype << ":" << typeid(MsgT).name() << modulename;
  m_mailtype = s.str();
}

  template<class MsgT>
GcInProcMailman<MsgT>::~GcInProcMailman( )
{
  DGCdeleteMutex(&m_mtxQ);
}


template<class MsgT>
void GcInProcMailman<MsgT>::push_back( MsgT& msg ) {
  AutoMutex a(m_mtxQ);
  msgs.push_back( msg );
}

template<class MsgT>
bool GcInProcMailman<MsgT>::send(MsgT* msg) {
  AutoMutex a(m_mtxQ);
  return depot.broadcast( m_mailtype, msg );
}

template<class MsgT>
bool GcInProcMailman<MsgT>::receive(MsgT* msg) {
  AutoMutex a(m_mtxQ);
  if( hasNewMessage() )
  {
    *msg = msgs.front();
    msgs.pop_front();
    return true;
  }
  else
    return false;
}

template<class MsgT>
bool GcInProcMailman<MsgT>::setMode( typename PMailman::OperationMode m ) {
  switch(m) {
    case PMailman::RECEIVER:   depot.registerListener( m_mailtype, this ); break;
    case PMailman::SENDER: depot.removeListener( m_mailtype, this ); break;
    default:
                           return false;
  }
  return true;
}

  template<class MsgT>
bool GcInProcMailman<MsgT>::hasNewMessage() 
{ 
  AutoMutex a(m_mtxQ); 
  return !msgs.empty(); 
}



template<class MsgT>
bool GcInProcMailman<MsgT>::GcInProcMailDepot::registerListener( string mailtype, GcInProcMailman<MsgT>* mailman ) {
  InProcMailList& l = map[mailtype];
  l.push_back( mailman );
  return true;
}

template<class MsgT>
bool GcInProcMailman<MsgT>::GcInProcMailDepot::removeListener( string mailtype, GcInProcMailman<MsgT>* mailman ) {
  InProcMailList& l = map[mailtype];
  l.remove( mailman );
  return true;
}

template<class MsgT>
bool GcInProcMailman<MsgT>::GcInProcMailDepot::broadcast( string mailtype, MsgT* msg ) {
  InProcMailList& l = map[mailtype];
  if( l.empty() ) 
    return false;
  for( typename InProcMailList::iterator iter=l.begin(); 
      iter!=l.end(); 
      iter++ )
    (*iter)->push_back( *msg );
  return true;
}

#endif
