/*!
 *  \file  GcModule.cc
 *  \brief Implementaiton of GcModule Functions
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 * Main implemenation of the GcModule class.
 *
 * Also implementation of the GcPortHandlers -- remnant from 
 * when they were actually part of gcmodule.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "dgcutils/DGCutils.hh"
#define OMIT_TEMPLATE_DEFS
#include "GcPortT.hh"
#include "GcModule.hh"


/// Start()
/// 
/// Starts the messaging thread, and if applicable the main processing
/// thread
bool GcModule::Start(void)
{ 
  if( m_bEventDriven ) {
    // callback with the messaging thread is installed by default, 
    // but controled by the above flag
  } else {
    /// Create a new thread to handle the arbitrate control loop
    gcMainEndThreadFlag = false;
    DGCstartMemberFunctionThread(this, &GcModule::gcMainThread);
  }

  /// Startup the portHandler thread
  GcSimplePortHandlerThread::ReStart();

  return true;
};

/// mainloopCallback()
///
/// default handler called by handleMessageReadyCallbacks()
void GcModule::mainloopCallback() 
{
  /// Repeatedly call arbitrate and control on callback, giving the loop
  /// a chance to reach a stable point before possibly waiting for the 
  /// next message
  for( int i=0; i<REPEAT_ARBITRATE_CONTROL_CALLBACK_COUNT; i++ ) {
    gclog(9) << " * entering arbitrate: " << usecTime() << ": " << *m_pControlStatusBase << endl;
    arbitrate(m_pControlStatusBase, m_pMergedDirectiveBase);
    gclog(9) << " * control status at      " << usecTime() << ": " << *m_pControlStatusBase << endl;
    gclog(9) << " * arbitratedDirective at " << usecTime() << ": " << *m_pMergedDirectiveBase << endl;
    control(m_pControlStatusBase, m_pMergedDirectiveBase);  
    gclog(9) << " * finished control: " << usecTime() << ": " << *m_pMergedDirectiveBase << endl;
  }
}

/// handleMessageReadyCallbacks()
/// 
/// the default callback on a new message ready.  This callback is always
/// called, so it needs to no-op if this is not an event driven module
void GcModule::handleMessageReadyCallbacks() {
  if( m_bEventDriven ) {
    m_bEventDriven = false;
    gclog(9) << "gcPumpThread: calling mainloopCallback on new message recevied" << endl;
    mainloopCallback();
    m_bEventDriven = true;
  }
}

/// GcModule()
/// Constructor
///
/// When constructing a gcmodule, you should provide a textual name (used for
/// logging), pointers to ControlStatus and MergedDirective instances (which
/// may be references to members of a derived GcModule), time to sleep between
/// iterations of the main thread, and the message pump, and whether the
/// module should behave event-driven based or not.
GcModule::GcModule( string sName, ControlStatus* cs, MergedDirective* md, 
                    long nControlUSleepTime, long nMessageLoopUsleepTime,
                    bool bEventDriven ) :
  GcModuleLogger(sName),
  GcSimplePortHandlerThread( false, nMessageLoopUsleepTime ),
  m_nControlLoopUsleepTime(nControlUSleepTime),
  m_container(NULL),
  m_bEventDriven( bEventDriven ),
  m_pMergedDirectiveBase(md),
  m_pControlStatusBase(cs)
{
  DGCcreateCondition( & m_condMainThreadDone );
  DGCcreateCondition( & m_msgReady );

  /// install a condition signal with the portHandler so that
  /// the main thread can be signaled when a new message is ready
  ///
  // * perhaps this should not be the default (or only) behavior
  ///  for those modules that really do need rate limiting
  if( !m_bEventDriven )
    GcSimplePortHandlerThread::installMsgReadySignal( &m_msgReady );
} 

/// Destructor
///
GcModule::~GcModule()
{
  DGCdeleteCondition( &m_condMainThreadDone );
  DGCdeleteCondition( &m_msgReady );
}

/// gcMainThread()
/// 
/// The thread function for non-event-based modules
/// Calls arbitrate, control, arbitrate... indefinitely until flagged
/// to stop.  A sleep (time specified in constructor) is called after
/// each control() -- interuptable by arrival of a new message, if the 
/// underlying message delivery mechanism supports signaling.  
void GcModule::gcMainThread()
{
  DGCSetConditionFalse(m_condMainThreadDone);
  while (!gcMainEndThreadFlag)
  {
    arbitrate(m_pControlStatusBase, m_pMergedDirectiveBase);
    gclog(9) << " * control status at      " << usecTime() << ": " << *m_pControlStatusBase << endl;
    gclog(9) << " * arbitratedDirective at " << usecTime() << ": " << *m_pMergedDirectiveBase << endl;
    control(m_pControlStatusBase, m_pMergedDirectiveBase);  
    bool bSignaled = 0 == GCpthread_cond_timedwait( m_msgReady.pCond, m_msgReady.pMutex, m_nControlLoopUsleepTime );
    if( bSignaled ) gclog(8) << "gcMainThread awoken by m_msgReadySignal" << endl;
  }
  DGCSetConditionTrue(m_condMainThreadDone);
}


/////////////////////////////////////////
/// GcPortHandler implementation

/// Destructor
///
/// remove all the ports from our list of ports to 
/// service
GcPortHandler::~GcPortHandler()
{
  for (int ii=0;ii<(int)portQ.size();ii++)
  { }
  portQ.clear();
}

/// pumpPorts()
///
/// For all of the registered ports, either send messages waiting
/// to go out (typically there are none as sending is usually 
/// done synchronously) or check for and receive incoming messages.
/// 
/// return true if any messages were received
bool GcPortHandler::pumpPorts() {
  bool bNewOne = false;
  vector<IGcPort*>::iterator pp = portQ.begin();
  for ( ; pp != portQ.end(); pp++ )
  {
    if ((*pp)->getSendFlag())
    {
      (*pp)->sendMsgs();
    } else {
      bNewOne = (*pp)->recvMsgs() || bNewOne;
    }
    (*pp)->handleMsgs();
  }
  return bNewOne;
}

/// addPort()
///
/// used by ports to register themselves with the 
/// handler.  If the portHandler has/supports a condition for 
/// signalling when a new message is ready, then install it
/// with the port
bool GcPortHandler::addPort(IGcPort* newport)
{
  portQ.push_back(newport);

  if( DGCcondition* cond = getMessageReadyCondition() ) {
    newport->installMessageReadyCondition(cond);
  }
  return true;
}

/// remPort()
///
/// remove a port from the portHandler.  Not used frequently,
/// should be DEPRICATED
bool GcPortHandler::remPort(int num)
{
  if ((int)portQ.size() > num)
  {
    portQ.erase(portQ.begin()+num);
    return 1;
  }
  else
  {
    return 0;
  }
}


int GCpthread_cond_timedwait( pthread_cond_t& got_request, pthread_mutex_t& request_mutex, long usecTimeout )
{
  struct timeval  now={0};            /* time when we started waiting        */
  struct timespec timeout={0};        /* timeout value for the wait function */
  int             done=0;           /* are we done waiting?                */

  /* first, lock the mutex */
  int rc = pthread_mutex_lock(&request_mutex);
  if (rc) { /* an error has occurred */
      perror("pthread_mutex_lock");
      pthread_exit(NULL);
  }
  /* mutex is now locked */

  /* get current time */ 
  gettimeofday(&now, 0 );
  /* prepare timeout value.              */
  /* Note that we need an absolute time. */
  timeout.tv_sec = now.tv_sec + (usecTimeout / 1000000 );
  timeout.tv_nsec = ( usecTimeout%1000000 + now.tv_usec ) * 1000; 
                                        /* timeval uses micro-seconds.         */
                                        /* timespec uses nano-seconds.         */
                                        /* 1 micro-second = 1000 nano-seconds. */

  /* wait on the condition variable. */
  /* we use a loop, since a Unix signal might stop the wait before the timeout */
  {
    /* remember that pthread_cond_timedwait() unlocks the mutex on entrance */
    rc = pthread_cond_timedwait(&got_request, &request_mutex, &timeout);
    switch(rc) {
      case 0:  /* we were awakened due to the cond. variable being signaled */
        /* the mutex was now locked again by pthread_cond_timedwait. */
      default:        /* some error occurred (e.g. we got a Unix signal) */
        if (errno == ETIMEDOUT) { /* our time is up */
          done = -1;
        }
    }
  }
  /* finally, unlock the mutex */
  pthread_mutex_unlock(&request_mutex);

  return rc;
}

