/*!
 *  \file  GcInterface.hh
 *  \brief Defines the API for interfaces between GcModules -- GcInterface
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *
 */

#ifndef _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__
#define _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__

// use this value as a substitue sn_msg type --> directives will only
// be delivered to GcPorts/GcInterfaces within the same process, no 
// skynet connection is created
#define GCM_IN_PROCESS_DIRECTIVE ((sn_msg)-42) 


#include "gcmodule/GcPortT.hh"
#include "GcModule.hh"
#include "gcinterfaces/GcModuleInterfaces.hh"
#include <map>
#include <set>
using std::set;
using std::map;
#include "AutoMutex.hh"

#include <ext/new_allocator.h>


/// IInterface
/// provides basic functionality common between the North and Southfaces
///
class IInterface {
public:
  /// free's all cached directives and status.  Pointers that have been returned
  /// will be invalidated!!!
  virtual void clearAll() = 0;

  /// Interfaces that implement caching of directives or status messages can
  /// automatically purge when a threshold is reached, keeping memory usage
  /// from running rampant
  virtual void setStaleThreshold( int n ) = 0;

  virtual ~IInterface() {}
};

/// This is the interface the arbiter will use to recv packets from another modules
/// controller and to send status responses back 
template<class YourDirectiveT, class YourStatusT>
class INorthfaceT : public IInterface
{
  public:
    /// returns false if no directives waiting, or invalid pointer
    /// if bNewest = false, returns true with oldest unread directive
    /// if bNewest = true, returns the latest unread directive and removes
    ///  all other directives (drops them)
    virtual bool getNewDirective( YourDirectiveT* dir, bool bNewest=false  ) = 0;

    /// sends a status response to a directive with id
    virtual bool sendResponse( YourStatusT* s, unsigned directiveId  ) = 0;
    /// Does the same as the above but does not ensure that you have the correct ID, use the above please!
    virtual bool sendResponse( YourStatusT* s ) = 0;

    /// checks for undread directives
    virtual bool haveNewDirective() = 0;

    virtual ~INorthfaceT() {}
};

/// This is the interface the controller will use to send directives down to the next
/// modules arbiter.
template<class YourDirectiveT, class YourStatusT> 
class ISouthfaceT : public IInterface
{
  public:
    /// push one directive out of hte port and cache its data to be paired with a status-response */
    virtual bool sendDirective( YourDirectiveT* dir ) = 0;

    /// returns a copy of a directive that was previously sent */
    virtual YourDirectiveT getSentDirective( unsigned id ) = 0;

    /// used to get the status of past or pending status responses of known id
    ///  * NOTE: this will purge pending status-responses, so that getLatestStatus returns NULL and haveNewStatus retruns false */
    virtual YourStatusT* getStatusOf( unsigned id ) = 0;
    /// shorthand to test the custom-status of a known id
    ///  * NOTE: uses getStatusOf to fetch status */
    virtual bool isStatus( int queryStatus, unsigned id ) = 0;
    /// pulls one status off of the incomming queue, null if haveNewStatus returns false */
    virtual YourStatusT* getLatestStatus( ) = 0;
    /// returns true if there are status msgs waiting to be processed */
    virtual bool haveNewStatus() = 0;

    /// remove all pending directives, past directives, past status-responses */
    virtual void flushAll( ) = 0;

    /// returns ids of all directives that have not yet received a response
    /// will be DEPRICATED soon
    virtual deque<unsigned> orderedDirectivesWaitingForResponse( ) = 0;


    virtual ~ISouthfaceT() {}
};


/// GcInterface
/// 
/// This is the main implementation class for communicating both "downwards"
/// and "upwards."  An implementor should specialize (typedef) this class with their
/// directive and response classes, corresponding skynet messagetypes for each,
/// and a skynet modulename.  If communication will be within a single process
/// boundary, the GCM_IN_PROCESS_DIRECTIVE value can be used instead of a true
/// skynet sn_msg, and teh sn_modname should be used to distinguish interfaces
/// with otherwise identical template parameters.
///
/// Instances of GcInterface are not constructed directly.  Instead they are 
/// obtained via static functions generateSouthface and generateNorthface. 
/// 
/// 
template< class YourDirectiveT, class YourStatusT, sn_msg sn_directive_msg, 
          sn_msg sn_status_msg, modulename sn_modname >
class GcInterface 
: public INorthfaceT<YourDirectiveT,YourStatusT>,
  public ISouthfaceT<YourDirectiveT,YourStatusT>
{
  public:
    typedef INorthfaceT<YourDirectiveT, YourStatusT> 		Northface;
    typedef ISouthfaceT<YourDirectiveT, YourStatusT> 		Southface;
    typedef GcPortT<YourDirectiveT>														GcDirectivePort;
    typedef GcPortT<YourStatusT>															GcStatusPort;

  protected:
    IGcLogger* m_logger;

    /// ctors are hidden -- instantiate via static generate functions
    ///
    /// 
    GcInterface( int sn_key, IGcPortHandler* portHandler, IGcLogger* logger = NULL )
      : directivePort( sn_key, sn_directive_msg, sn_modname, logger ),
        statusPort( sn_key, sn_status_msg, sn_modname, logger ) 
    {
      initialize( portHandler, logger );
    }

    GcInterface( int sn_key, GcModule* module ) 
      : directivePort( sn_key, sn_directive_msg, sn_modname, module ),
        statusPort( sn_key, sn_status_msg, sn_modname, module )
    {
      initialize( module, module );
    }

    void initialize( IGcPortHandler* portHandler, IGcLogger* logger )
    {
      m_logger = logger ;
      m_staleThreshold = 0;

      // register with the porthandler the two ports used
      // to communicate in and out
      portHandler->addPort( &directivePort );
      portHandler->addPort( &statusPort );

      AutoMutex::initMutex( &m_accessMutex );

      // ensure proper usage of in-process messaging
      if( (sn_directive_msg == GCM_IN_PROCESS_DIRECTIVE || sn_status_msg == GCM_IN_PROCESS_DIRECTIVE) &&
          sn_directive_msg != sn_status_msg )
      {
        std::cerr << " when using GCM_IN_PROCESS_DIRECTIVE for your GcInterfaces, you must use "
          "for both the directive and status ports ";
        exit(0);
      }
    }

    virtual ~GcInterface() {
      DGCdeleteMutex( &m_accessMutex );
    } 

  public:

    /// generateSouthface()
    /// 
    /// provide a skynetkey, an IGcPortHandler (usually a downcasted GcModule),
    /// and an optional IGcLogger (will be ignored if the porthandler is a gcmodule)
    ///
    /// The portHandler controls how/when messages are received on the port
    /// A GcSimpleThreadPortHandler will automatically pump the ports and recieve
    /// messages, while a simpler handler will require calling pumpPorts "manually"
    static Southface* generateSouthface( int sn_key, IGcPortHandler* portHandler, IGcLogger* logger = NULL )
    { 
      return (Southface*) generateFace( false, sn_key, portHandler, logger ); 
    }

    /// generateNorthface()
    //
    static Northface* generateNorthface( int sn_key, IGcPortHandler* portHandler, IGcLogger* logger = NULL )
    { 
      return (Northface*) generateFace( true, sn_key, portHandler, logger ); 
    }

    /// releaseSouthface()
    /// 
    /// checks the provided pointer for a true typed southface, and deallocates
    /// if appropriate
    /// referenced pointer will be assigned to null 
    static bool releaseSouthface( Southface*& face )
    {
      GcInterface* i = dynamic_cast<GcInterface*>(face);
      if( !i ) 
        return false;
      releaseFace( (GcInterface*&)face );
      return true;
    }
    /// releaseNorthface()
    //
    static bool releaseNorthface( Northface*& face )
    {
      GcInterface* i = dynamic_cast<GcInterface*>(face);
      if( !i ) 
        return false;
      releaseFace( (GcInterface*&)face );
      return true;
    }

  protected:
    /// generateFace()
    /// 
    /// Generalized function called by each of the generateNorth or Southface
    /// functions
    static GcInterface* generateFace( bool bNorth, int sn_key, IGcPortHandler* portHandler, IGcLogger* logger = NULL )
    {
      GcInterface* face = NULL;
      // find out if our porthandler is really a gcmodule, and call the appropriate
      // contructor
      GcModule* mod = dynamic_cast<GcModule*>(portHandler);
      if( mod )
        face = new GcInterface( sn_key, mod );
      else
        face = new GcInterface( sn_key, portHandler, logger );
      // set the ports to either send or receive depending on whether
      // we are a norhtface or a southface
      face->statusPort.setSendFlag(bNorth);
      face->directivePort.setSendFlag(!bNorth);
      return face;
    }
    
    /// releaseFace()
    /// 
    /// deletes a GcInterface
    static void releaseFace( GcInterface*& face )
    {
      delete face;
      face = NULL;
    }


  protected:
    GcDirectivePort  	directivePort;
    GcStatusPort			statusPort;

    pthread_mutex_t   m_accessMutex;

    /// The status of sent directives are tracked via a map of sets of 
    /// ids, each set represents all the directives of a given status.
    /// The number of sets can grow as new status states are encountered
    /// at run time.
    typedef set<unsigned> StatusSet;
    typedef map<int, StatusSet> StatusSetMapP;
    class StatusSetMap : public StatusSetMapP
    {
      public:
        /// move the status of given id to new status
        void mapStatus( unsigned id, int status )
        {
          StatusSetMap& me = *this;
          // remove old status
          for( typename StatusSetMapP::iterator i=me.begin(); i!=me.end(); i++ )
            (*i).second.erase( id );
          // add new status, creating new set for status if necessary
          me[status].insert(id);
        }
        /// remove status of an id altogether (used when clearing caches,etc.)
        void remove( unsigned id )
        {
          StatusSetMap& me = *this;
          for( iterator i=me.begin(); i!=me.end(); i++ )
            (*i).second.erase(id);
        }
    };

    //// Have seen problems with the map never deallocating its objects (destructor is not called) on gentoo linux builds
    //// with the default allocator , resulting in a massive memory 'leak' (it is properly released at program close
    //// Using the __gnu_cxx::new_allocator seems to remedy this, but this is not available on hte Mac's default stl, 
    //   nor for other versions of gnu stl.  A better ifdef needs to replace this one -- this jsut makes the mac users
    //   happy
    #ifdef MACOSX 
    typedef map<unsigned, YourStatusT> StatusMap;
    typedef map<unsigned, YourDirectiveT> DirectiveMap;
    #ifndef ACKNOWLEDGE_STL_MAP_ALLOCATOR_ISSUE_
    #warning "Memory leaking has been observed when using stl::map with the default allocator -- you are compiling with the default allocator because of your platform and unavailability of alternative allocators (__gnu_cxx::new_allocator).  Feedback on memory observations are welcome as well as any information leading to the cause of this issue (I am quite perplexed: j. doubleday); submit on the wiki: stl_map_allocator_issue.  To remove this warning you can define  ACKNOWLEDGE_STL_MAP_ALLOCATOR_ISSUE_  in your makefile or files that include GcInterface"
    #endif //     #ifndef ACKNOWLEDGE_STL_MAP_ALLOCATOR_ISSUE_
    #else
    typedef map<unsigned, YourStatusT, less<unsigned>, __gnu_cxx::new_allocator< pair<unsigned, YourStatusT> > > StatusMap;
    typedef map<unsigned, YourDirectiveT, less<unsigned>, __gnu_cxx::new_allocator< pair<unsigned,YourDirectiveT> > > DirectiveMap;
    #endif 
    typedef deque<unsigned>  IdQueue;

    /// pushs the id on the back, removes any other instances from the queue
    void push_unique_back( IdQueue& me, unsigned id )
    {
      IdQueue tmp;
      for(typename IdQueue::iterator i=me.begin(); i!=me.end(); i++ )
        if( *i != id ) tmp.push_back(*i);
      me = tmp;
      me.push_back(id);
    }


    /// GcPortMessageQueue
    ///
    /// utility class to collect ids in directiveport que
    class GcPortMessageQueue : public GcDirectivePort::IGcMessageFilter
    {
      private:
        IdQueue m_ids;
      public:
        // function is called on all items in que
        bool filter( YourDirectiveT* dir )
        {
          m_ids.push_back( dir->getDirectiveId() );
          return false;
        }

        IdQueue getIds() { return m_ids; }
    };

    /// GcPortMessageFilter
    /// 
    /// utility class used to find directive in que of a particular id
    class GcPortMessageFilter : public GcDirectivePort::IGcMessageFilter
    {
      public:
        unsigned m_id;
        GcPortMessageFilter( unsigned id ) : m_id(id) { }
        /*GcDirectivePort::MessageIdentifierFunc*/
        // function is called on items in que until it returns true
        bool filter( YourDirectiveT* dir )
        { 
          return ( dir->getDirectiveId() == m_id );
        }
    };

    /// clearFromDirectiveQueue()
    /// 
    /// utility function to remove a message from directivePort via its
    /// READYTOKILL / handleMsgs interface
    void clearFromDirectiveQueue( unsigned id )
    {
      GcPortMessageFilter filter(id);
      directivePort.changeMsgStatus( filter, (int)GcDirectivePort::READYTOKILL, true );
      directivePort.handleMsgs();
    }

    StatusSetMap  m_deliveryMap;
    StatusSetMap  m_customStatMap;
    StatusMap     statMap;
    DirectiveMap  m_directiveMap;
    IdQueue       m_unreadQ;
    protected:  // helpers for managing incoming status-responses
    YourStatusT* getFromStatmap( unsigned id )
    {
      // now look for the requested
      if( statMap.find(id) != statMap.end() )
        return &(statMap[id]);
      return NULL;
    }


    bool statusWaiting() 
    {
      return statusPort.haveMsgs();
    }

    IdQueue m_staleDirectives;
    int     m_staleThreshold;

    /// getStatusFromQ()
    ///
    /// This is an intermediate function, moving status messages from
    /// the incoming que directly above the delivery mechanism to the 
    /// to the cache at this level. 
    /// get one message from the incoming que and return it.  
    YourStatusT* getStatusFromQ( )
    {
      if( statusWaiting() )
      {

        YourStatusT* s = (statusPort.getNewestMessage());
        if( s ) 
        {
          unsigned id = s->getDirectiveId();

          // update the status of the originating directive
          m_deliveryMap.mapStatus( id, GcInterfaceDirectiveStatus::ACKED );
          m_customStatMap.mapStatus( id, s->getCustomStatus() );
          //clearFromDirectiveQueue( id );

          // this logic could be used to control whether only the latest 
          // (dropping old unread messages) status are retreived
          if( false ) // use a variable later to implement getOldestUnread vs getLatest -- currently hardwired for getOldestUnread
            m_unreadQ.clear();

          // put the id of this message on the unread message que
          m_unreadQ.push_back( id );

          if( m_logger ) m_logger->gclog(7) << "GcInterface::getStatusFromQ: pushing status/response onto unread q. id: "<< id << endl;

          // put id at end of que to remove the directive & status as they become old
          push_unique_back(m_staleDirectives, id);
        }

        return s;
      }
      return NULL;
    }

    /// cleanup
    // check for a size larger than some threshold and then clear down to half of that size with the stale/old list
    void cleanStale()
    {
      int size = m_staleDirectives.size() ;
      /// we multiply the threshold by the cardinality of the scustom status set to avoid flushing directives
      /// before they have had a terminal status sent, assuming each directive will normally not have 
      /// more than one of each of these status messages.
      if( m_staleThreshold && size > (int)(m_staleThreshold*(1+m_customStatMap.size())) )
      {
        if(m_logger) m_logger->gclog(7) << "Removing oldest messages: " << (size/2) << endl;
        for( int i=0; i<size/2; i++ )
        {
          unsigned stale = m_staleDirectives.front();
          m_staleDirectives.pop_front();
          if(m_logger) m_logger->gclog(8) << "GcInterface::cleanStale: removing id: " << stale << endl;
          m_customStatMap.remove(stale);
          m_deliveryMap.remove(stale);
          m_directiveMap.erase(stale);
          statMap.erase(stale);
        }
        /// need to cleanup the m_unreadQ, remove any ids that have been deleted
        {
          IdQueue tmp;
          for( IdQueue::iterator i=m_unreadQ.begin(); i!=m_unreadQ.end(); i++ )
            if( getFromStatmap(*i)!=NULL ) tmp.push_back(*i);
          m_unreadQ = tmp;
        }
        if(m_logger) m_logger->gclog(7) << "GcInterface::cleanStale: " << "m_directivemap.size: " << m_directiveMap.size() << endl;
        if(m_logger) m_logger->gclog(7) << "GcInterface::cleanStale: " << "statmap.size()" << statMap.size() << endl;
      }
    }

    void updateStatus()
    {
      cleanStale();

      //first get all the latest stati
      while( statusWaiting() )
      {
        YourStatusT* s = getStatusFromQ();
        if( s )
        {
          statMap.insert( typename StatusMap::value_type(s->getDirectiveId(), *s) );
          // Cleanup for allocation in the port -- ugly interface, should ask 
          // the port to deallocate
          delete s;
        }
      }
    }

    void flushAll( )
    {
      AutoMutex autoM( m_accessMutex );

      m_deliveryMap.clear();
      m_customStatMap.clear();
      directivePort.flushMsgs();
      statusPort.flushMsgs();
      m_unreadQ.clear();
      m_staleDirectives.clear();

      statMap.clear();
      m_directiveMap.clear();
    }



  public:
    // Implement the IInterface
    //
    /// Clears all cached directives and status
    void clearAll()
    {
      flushAll();
    }

    /// This sets the number of directives that the interface will cache.  Once this number is
    /// exceeded, the oldest half of directives and status responses will be released.  Pointers
    /// returned will be invalid!  Use them temporarilly.  
    void setStaleThreshold( int n )
    {
      m_staleThreshold = n;
    }

    /// implement the southinterface
  public:
    /// This returns the directives that have not been responsded too, in order that they were posted
    /// mostly for debugging purposes.
    IdQueue orderedDirectivesWaitingForResponse( )
    {
      AutoMutex autoM( m_accessMutex );

      /// This fucntion no longer returns ids in order (order of being sent)
      //GcPortMessageQueue queue;
      //directivePort.changeMsgStatus( queue, 0, false );
      //return queue.getIds();

      StatusSet& s = m_deliveryMap[GcInterfaceDirectiveStatus::SENT] ;
      IdQueue ids( s.begin(), s.end() );
      return ids;
    }


    /// This sends a directive.  You should ensure that your directive will return a 
    /// unique id when getDirectiveId() is called
    bool sendDirective( YourDirectiveT* dir )
    {
      AutoMutex autoM( m_accessMutex );

      /// add a timestamp for processing time computation 
      struct timeval tv;
      gettimeofday(&tv, NULL);
      dir->timeSent = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;

      /// cache the directive 
      m_directiveMap.insert( typename DirectiveMap::value_type(dir->getDirectiveId(), *dir) );
      //m_directiveMap[dir->getDirectiveId()] = *dir;

      /// send the directive
      directivePort.addMsg( dir );

      /// mark the status of the directive as SENT
      m_deliveryMap.mapStatus( dir->getDirectiveId(), GcInterfaceDirectiveStatus::SENT );

      /// This schedules the directive to be remvd evn f it never gets a response
      push_unique_back(m_staleDirectives, dir->getDirectiveId());

      return true;
    }

    /// You can recover recently sent directives -- they are cached here (and cleared with the clearAll() 
    /// or when the staleThreshold is exceeded (if set).
    ///
    /// This interface has the flaw in that if you ask for a directive
    /// that has not ever been sent, or that has been cleared from the
    /// cache, it will return a default constructed directive.
    YourDirectiveT getSentDirective( unsigned id )
    {
      AutoMutex autoM( m_accessMutex );

      /*
         if(0)
         {
         if( m_directiveMap.find(id) != m_directiveMap.end() )
         return &m_directiveMap[id];
         else
         return NULL;
         }
         */
      return m_directiveMap[id];
    }

    /// This returns the received status/response of a particular directive
    /// The pointer received is valid only temporarily -- do not store for later use.
    YourStatusT* getStatusOf( unsigned id )
    {
      AutoMutex autoM( m_accessMutex );

      updateStatus();

      return getFromStatmap( id );
    }


    /// Responses/status can be checked for sent directives.  Will return true if the most 
    /// recently recieved response/status had a queryStatus value returned by getCustomStatus
    bool isStatus( int queryStatus, unsigned id )
    {
      AutoMutex autoM( m_accessMutex );

      YourStatusT* s = getStatusOf( id );
      if( s )
      {
        return (s->getCustomStatus() == queryStatus);
      }
      return false;
    }

    /// returns the oldest unread status received
    /// The pointer received is valid only tempo -- do not store for later use.
    ///
    /// The potential dangling pointer needs to be fixed at the API level. 
    /// Return either a copy or a static allocated reference, or even some
    /// sort of release mechanism.
    YourStatusT* getLatestStatus( )
    {
      AutoMutex autoM( m_accessMutex );

      /// Check to see if we do not have any status
      if(m_unreadQ.empty() && !haveNewStatus()) 
      {
        if(m_logger) m_logger->gclog(7) << "ERROR: GcInterface::getLatestStatus() about to return null -- no available status" << endl;
        return NULL;
      }

      // retrive message from unread que
      YourStatusT* s = NULL;
      do {
        unsigned id = m_unreadQ.front();
        m_unreadQ.pop_front();
        s = getStatusOf( id );
      } while( s == NULL );

      /// compute time to process message from time it was sent
      if( m_logger && m_logger->isLevel(8) )
      {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
        if( t > s->timeSent )
          m_logger->gclog(0) << "GcInterface(" << this << ")::getLatestStatus: micro-seconds to deliver message(" << s->getDirectiveId() << "): " << (t - s->timeSent) << " us" << endl;
        else
          m_logger->gclog(0) << "GcInterface(" << this << ")::getLatestStatus: micro-seconds to deliver message(" << s->getDirectiveId() << "): -1 us ; sent time is earlier than deliverd! Check for clock skew between machines " << endl;
      }
      return s;
    }

    /// Returns true if there is status that is ready to be recieved/read.  Callingi getStatusof() or 
    /// isStatus(), etc., will cause this function to return false until new status is received.
    bool haveNewStatus()
    {
      AutoMutex autoM( m_accessMutex );

      updateStatus();

      return !m_unreadQ.empty();
    }



    /// implement the north interface
  public:
    /// new directive ready?
    bool haveNewDirective() 
    {
      AutoMutex autoM( m_accessMutex );

      return directivePort.haveMsgs();
    }

    /// retreive the latest directive
    /// dir must be a valid pointer 
    /// returns false if no directive ready
    bool getNewDirective( YourDirectiveT* dir, bool bNewest )
    {
      AutoMutex autoM( m_accessMutex );

      if( !dir )
        return false;
      if( !directivePort.haveMsgs() )
        return false;

      YourDirectiveT* iPack = NULL;
      if( bNewest )
      {
        do { 
          if(iPack) delete iPack; /// will try to delete NULL on the first iteration
          iPack = directivePort.getNewestMessage();
        } while( directivePort.haveMsgs() );
      }
      else
        iPack = directivePort.getNewestMessage( );
      if( iPack != NULL )
      {
        if( m_logger && m_logger->isLevel(8) ) {
          *dir = *(iPack);
          struct timeval tv;
          gettimeofday(&tv, NULL);
          uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
          if(t > dir->timeSent )
            m_logger->gclog(0) << "GcInterface(" << this << ")::getNewDirective: micro-seconds to deliver message(" << dir->getDirectiveId() << "): " << (t - dir->timeSent) << " us" << endl;
          else
            m_logger->gclog(0) << "GcInterface(" << this << ")::getNewDirective: micro-seconds to deliver message(" << dir->getDirectiveId() << "): -1 us ; sent time is earlier than deliverd! Check for clock skew between machines " << endl;
        }
        delete iPack;
        return true;
      }
      return false;
    }

    /// Sends a response for the received directiveId
    bool sendResponse( YourStatusT* s, unsigned directiveId )
    {
      AutoMutex autoM( m_accessMutex );

      // set the directive within the directive
      s->id = directiveId;

      return sendResponse( s );
    }

    bool sendResponse( YourStatusT* s )
    {
      AutoMutex autoM( m_accessMutex );

      // generate a timestamp to later compute time from send to read
      struct timeval tv;
      gettimeofday(&tv, NULL);
      s->timeSent = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;

      // send the directive
      return statusPort.addMsg( s );
    }


  public:
    /// The getNorthface and getSouthface functions have been depricated.
    /// Use the corresponding generateXXXXXface functions instead.
    void* DEPRICATED_getXXXXXface(int) {return NULL;} 
    #define getNorthface() DEPRICATED_getXXXXXface()// generate an error
    #define getSouthface() DEPRICATED_getXXXXXface() // generate an error
    #if 0 
    Northface* getNorthface() 
    { 
      directivePort.setSendFlag(false) ;
      statusPort.setSendFlag(true) ;
      return this; 
    }
    Southface* getSouthface() 
    {
      directivePort.setSendFlag(true) ;
      statusPort.setSendFlag(false) ;
      return this; 
    }
    #endif 
};

#endif // _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__

