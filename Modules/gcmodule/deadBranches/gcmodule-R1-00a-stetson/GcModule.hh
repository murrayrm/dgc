#include <deque>
#include <time.h>
#include <unistd.h>
#include "skynet/skynet.hh"
#include "skynet/sn_types.h"


// ______
// GcPort
//
// GcPort is a virtual class.  It should be
// inherited anew for each combination of
// message type and module being used.
// Each time it is inherited,
// serialize and deserialize must be defined.
//
// A port is a connection from _a given module_
// ..with a given shared _skynet key_
// ...for a certain kind of _message type_ 
// (the module and message type options are
// defined in sn_types.hh, the skynet key is
// defined when the port starts running.
// 
// A GcPort contains a list of messages that
// have been sent.
class GcPort
{
public:
  GcPort(int sn_key, sn_msg sn_msgtype, modulename sn_modname) 
    : m_sendsock(-1),
      m_recvsock(-1),
      m_modname(sn_modname),
      m_msgtype(sn_msgtype),
      maxTimeToHold(2) //hold messages for no more than 2 seconds
  {
    int status;
    p_skynet = new skynet(sn_modname, sn_key, &status);
    DGCcreateMutex(&m_qMutex);
    sendFlag = 0;
  }

  ~GcPort()
  {
    DGCdeleteMutex(&m_qMutex);
    delete p_skynet;
  }

  //for now, this defines whether a given message has come in
  //or is going out
  enum msgStatus { READYTOKILL, READYTOSEND, SENT, RECEIVED, SENDERROR };

  //serialize and deserialize must be defined
  //unfortunately, I can't define them here as virtual functions because this class
  //must be queued in GcModule
  //virtual bool serialize(void * indata, void * outdata, int insize, int outsize) = 0;

  //__message READING functions
  //gets the newest message sent or received
  void * getNewestMessage(int * size); //gets a pointer to the newest message
  time_t lastMsgTimeSent();
  //bool getMsg(id,void * msg);, bool getMsg(id);

  //__message WRITING functions
  bool addMsg(void * msg, int msgSize); //adds a message to the queue

  //__message HANDLING functions
  bool handleMsgs(); //clears all of the messages that are ready to die
  bool sendMsgs(); //sends all of the recently added messages
  bool recvMsgs(); //receives all of the new messages

  sn_msg getMsgType() { return m_msgtype; }
  int getNumMsgs() { return msgQ.size(); };

  int sendFlag; //is this a sending (1) or receiving (0) type of port

protected:
  //wraps each message up with relevant data
  //including arrival time (such that we know
  //when it should be cleared), status and so
  //on.
  class MessageWrapper
  {
  public:
    void * msg; //the message in its serial or "to be sent" format
    int msgSize;
    time_t timeSentRcvd; //default behavior is to be clear after 100 ms, say
    msgStatus status; //if status is READYTOKILL, this message is as good as dead
    pthread_mutex_t m_dataBufferMutex; //?
  };
  deque<MessageWrapper> msgQ;
  pthread_mutex_t m_qMutex; //?

  //__Skynet stuff__
  skynet * p_skynet;
  modulename m_modname;
  sn_msg m_msgtype;
  int m_sendsock;
  int m_recvsock;
  int getRecvSock();
  int getSendSock();
  bool sendMessage(void * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex);
  void * receiveMessage(int * bufSize);

  //__Message handling
  int maxTimeToHold;
};

class GcModule
{
public:
  GcModule(void) {}; //normally, ports should be added here
  ~GcModule(void);

  bool Start();
  bool Stop() { gcMessageEndThreadFlag = 1; gcMainEndThreadFlag = 1; }
  int IsStopped() { return (gcMessageEndThreadFlag && gcMainEndThreadFlag); }

  bool addPort(GcPort* newport);
  bool remPort(int num);

  class MergedDirective
  {
  public:
    enum directiveTypes { RUN, STOP } directive;
    void * rules;
    int rulesNumBytes;
  };
  class ControlStatus
  {
  public:
    enum statusTypes { FAILED, RUNNING, STOPPED } status;
    void * additionalData;
    int addDataNumBytes;
  };

  virtual MergedDirective Arbitrate(ControlStatus controlStatus)
  {
    MergedDirective mergedDirective;

    if (controlStatus.status == ControlStatus::FAILED)
    {
      mergedDirective.directive = MergedDirective::STOP;
    }
    else
    {
      mergedDirective.directive = MergedDirective::RUN;
    }
  }
  virtual ControlStatus Control(MergedDirective mergedDirective)
  {
    ControlStatus controlStatus;
    if (mergedDirective.directive == MergedDirective::RUN)
    {
      //std::cout << "the default directive \n";
    }
    return controlStatus;
  }

protected:

  /*! This is the module's main thread. */
  int gcMainEndThreadFlag;
  void gcMainThread();

  /*! This thread is supposed to handle all communication. */
  int gcMessageEndThreadFlag;
  void gcMessageThread();

  deque<GcPort*> portQ;
  GcPort* getPortofType(sn_msg sn_msgtype);
};

