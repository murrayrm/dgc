#include "GcModule.hh"
#include "GcInterface.hh"


class directive1 : public ITransmissive
{
	public:
		directive1() { memcpy(data,"do your job",12); }
		char data[12];
		template< class ArchiveT >
		void serialize(ArchiveT &ar, const unsigned int version)  
		{
			ar & data;
		}

		unsigned produceUniqId( )
		{
			return (time(NULL) << 8 | (char)((int)this) );
		}
};

class status1
{
public:
	template< class ArchT >
	void serialize( ArchT & ar, const unsigned int ver )
	{
	}
};

class GcPort : public GcPortT<directive1>
{
	public:
		GcPort(int sn_key, sn_msg sn_msgtype, modulename sn_modname) :
			GcPortT<directive1>(sn_key, sn_msgtype, sn_modname) 
	{}
		virtual ~GcPort() {}
};


