#ifndef _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__
#define _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__

#include <deque>
#include <time.h>
#include <unistd.h>
//#include "skynet/skynet.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/SkynetTalker.cc"

// ______
// GcPort
//
// GcPort is a virtual class.  It should be
// inherited anew for each combination of
// message type and module being used.
// Each time it is inherited,
// serialize and deserialize must be defined.
//
// A port is a connection from _a given module_
// ..with a given shared _skynet key_
// ...for a certain kind of _message type_ 
// (the module and message type options are
// defined in sn_types.hh, the skynet key is
// defined when the port starts running.
// 
// A GcPort contains a list of messages that
// have been sent.

class IGcPort
{
	public:
		virtual ~IGcPort() {}
		//__message WRITING functions
//		virtual bool addMsg(Message * msg, int msgSize = -1) = 0; //adds a message to the queue

		//__message HANDLING functions
		virtual bool handleMsgs() = 0; //clears all of the messages that are ready to die
		virtual bool sendMsgs() = 0; //sends all of the recently added messages
		virtual bool recvMsgs() = 0; //receives all of the new messages
		virtual sn_msg getMsgType() = 0;
		virtual void setSendFlag(int) = 0; // use to set the send flag
		virtual bool getSendFlag()  = 0 ;

};

template <class Message>
class GcPortT : public IGcPort, public SkynetTalker<Message>
{
	public:
		GcPortT(int sn_key, sn_msg sn_msgtype, modulename sn_modname) ;
		virtual ~GcPortT();

		//for now, this defines whether a given message has come in
		//or is going out
		enum msgStatus { READYTOKILL, READYTOSEND, SENT, RECEIVED, SENDERROR };

		//serialize and deserialize must be defined
		//unfortunately, I can't define them here as virtual functions because this class
		//must be queued in GcModule
		//virtual bool serialize(void * indata, void * outdata, int insize, int outsize) = 0;

		//__message READING functions
		//gets the newest message sent or received
		Message * getNewestMessage(int * size); //gets a pointer to the newest message
		time_t lastMsgTimeSent();
		//bool getMsg(id,void * msg);, bool getMsg(id);

		//__message WRITING functions
		bool addMsg(Message * msg, int msgSize = -1); //adds a message to the queue

		//__message HANDLING functions
		bool handleMsgs(); //clears all of the messages that are ready to die

		sn_msg getMsgType() { return m_msgtype; }
		int getNumMsgs() { return (int)msgQ.size(); };

		int sendFlag; //is this a sending (1) or receiving (0) type of port
		virtual void setSendFlag(int set) { sendFlag = set; } // use to set the send flag
		virtual bool getSendFlag() {return sendFlag;}

	protected:
		//wraps each message up with relevant data
		//including arrival time (such that we know
		//when it should be cleared), status and so
		//on.
		template<class MWM>
		class MessageWrapperT
		{
			public:
				MWM * msg; //the message in its serial or "to be sent" format
				int msgSize;
				time_t timeSentRcvd; //default behavior is to be clear after 100 ms, say
				msgStatus status; //if status is READYTOKILL, this message is as good as dead
				pthread_mutex_t m_dataBufferMutex; //?
		};
		typedef MessageWrapperT<Message> MessageWrapper ;
		deque< MessageWrapper > msgQ;
		pthread_mutex_t m_qMutex; //?

		//__Message handling
		int maxTimeToHold;
		sn_msg m_msgtype;

	public:

		bool sendMsgs(); //sends all of the recently added messages
		bool recvMsgs(); //receives all of the new messages
		bool sendMessage(Message * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex);
};


template <class Message>
bool GcPortT<Message>::addMsg(Message * msg, int msgSize) 
{ 

	bool success = DGClockMutex(&m_qMutex);

	MessageWrapper mwrap;
	mwrap.timeSentRcvd = (time_t) -1;
	mwrap.status = READYTOSEND;

	// create a copy -- copy constructor must be implemented
	mwrap.msg = new Message( *msg );
	mwrap.msgSize = msgSize;

	success = DGCcreateMutex(&(mwrap.m_dataBufferMutex));
	//std::cout << "successful mutex? " << success << "\n"; //TEMPORARY

	msgQ.push_front(mwrap); //I think this copies all the data

	success = DGCunlockMutex(&m_qMutex);

	return true;

}


class MergedDirective
{
	public:
	int id;
};

class ControlStatus
{
	public:
		enum statusTypes { FAILED, RUNNING, STOPPED } status;
};


class GcModule
{
	public:
	public:
		GcModule(ControlStatus* cs, MergedDirective* md) :
			m_mergedDirective(md),
			m_controlStatus(cs)
	{}; //normally, ports should be added here
		virtual ~GcModule(void);

		bool Start();
		bool Stop() { gcMessageEndThreadFlag = 1; gcMainEndThreadFlag = 1; return true; }
		int IsStopped() { return (gcMessageEndThreadFlag && gcMainEndThreadFlag); }

		bool addPort(IGcPort* newport);
		bool remPort(int num);

		virtual void arbitrate(ControlStatus* controlStatus, MergedDirective* md) = 0;

		virtual void control(ControlStatus* controlStatus, MergedDirective* mergedDirective) = 0;

	protected:

		/*! This is the module's main thread. */
		int gcMainEndThreadFlag;
		void gcMainThread();

		/*! This thread is supposed to handle all communication. */
		int gcMessageEndThreadFlag;
		void gcMessageThread();

		deque<IGcPort*> portQ;
#warning "dont think this(below) is ok returning an interface pointer"
		IGcPort* getPortofType(sn_msg sn_msgtype);

		MergedDirective* m_mergedDirective;
		ControlStatus* m_controlStatus;
};



/// GcPortT template implementations
//

template <class Message>
GcPortT<Message>::GcPortT(int sn_key, sn_msg sn_msgtype, modulename sn_modname)  :
	SkynetTalker<Message>(sn_key, sn_msgtype, sn_modname) ,
	maxTimeToHold(2), //hold messages for no more than 2 seconds
	m_msgtype(sn_msgtype)
{
	DGCcreateMutex(&m_qMutex);
	sendFlag = 0;
}

template <class Message>
GcPortT<Message>::~GcPortT()
{
	DGCdeleteMutex(&m_qMutex);
}

//gets the newest message sent or received
template <class Message>
Message * GcPortT<Message>::getNewestMessage(int * msgSize) //gets a pointer to the newest message
{
	if ((int)msgQ.size() > 0)
	{
		*msgSize = msgQ[0].msgSize;
		return static_cast<Message*>(msgQ[0].msg);
	}
	else
	{
		return NULL;
	}
}

//gets the time that the last message was sent out
template <class Message>
time_t GcPortT<Message>::lastMsgTimeSent()
{

	//std::cout << "doodles\n"; //TEMPORARY
	//std::cout << "message Q size " << msgQ.size() << "\n"; //TEMPORARY

	for (int ii=0;ii<(int)msgQ.size();ii++)
	{
		if (msgQ[ii].status == SENT)
		{
			return msgQ[ii].timeSentRcvd;
		}

	}

	return (time_t) 0;
}


template <class Message>
bool GcPortT<Message>::recvMsgs() //sends all of the recently added messages
{
#warning " must deallocate off of the queue "
	Message * newMsg = new Message;
	int newSize;

	receive(newMsg);
	if (NULL != newMsg)
	{
		if (newSize <= 0)
		{
//			std::cout << "recvMsgs, zero size non-null msg -- this should never happen";
		}
		MessageWrapper mwrap;
		mwrap.timeSentRcvd = time(NULL); //the time we got the message
		mwrap.status = RECEIVED;

		mwrap.msg = newMsg;
		mwrap.msgSize = newSize;
		DGCcreateMutex(&(mwrap.m_dataBufferMutex));

		msgQ.push_front(mwrap); //I think this copies all the data

		return true;
	}

	return false;
}

template <class Message>
bool GcPortT<Message>::sendMsgs() //sends all of the new messages
{    

	for(int ii=0;ii<(int)msgQ.size();ii++)
	{
		//std::cout << "s"; //TEMPORARY
		if (READYTOSEND == msgQ[ii].status)
		{
			//std::cout << "send?"; //TEMPORARY
			if (sendMessage(msgQ[ii].msg,msgQ[ii].msgSize,&(msgQ[ii].m_dataBufferMutex)))
			{
				//std::cout << "message successfully sent \n"; //TEMPORARY
				msgQ[ii].timeSentRcvd = time(NULL);
				msgQ[ii].status = SENT;
			}
			else
			{
				std::cerr << "error in GcPort::sendMsgs \n";
				msgQ[ii].status = SENDERROR;
			}
		}
	}

	return true;
}

template <class Message>
bool GcPortT<Message>::handleMsgs() //clears all of the messages that are ready to die
{
	MessageWrapper  mwrap;

	int ii;
	time_t timenow = time(NULL);


	//clear based on time
	for (ii=0;ii<(int)msgQ.size();ii++)
	{
		if ((difftime(timenow,msgQ[ii].timeSentRcvd) > maxTimeToHold) 
				&& (msgQ[ii].timeSentRcvd != (time_t) -1))
		{
			//std::cout << "here I am, ready to kill " << maxTimeToHold 
			//	  << " " << difftime(timenow,msgQ[ii].timeSentRcvd) 
			//	  << " sent? " << msgQ[ii].status << "\n"; //TEMPORARY
			msgQ[ii].status = READYTOKILL;
		}
	}

	DGClockMutex(&m_qMutex);

	ii=0;
	while (ii<(int)msgQ.size())
	{
		if (READYTOKILL == msgQ[ii].status)
		{
			//std::cout << "here I am, ready to kill " << maxTimeToHold 
			//	  << " " << difftime(timenow,msgQ[ii].timeSentRcvd) 
			//	  << " sent? " << msgQ[ii].status << "\n";
			mwrap = msgQ[ii];
			DGCdeleteMutex(&(mwrap.m_dataBufferMutex));
			free(mwrap.msg);
			msgQ.erase(msgQ.begin()+ii);
			//std::cout << "msg killed: size of q? " << msgQ.size() << "\n"; //TEMPORARY
		}
		else
		{
			ii++;
		}
	}

	DGCunlockMutex(&m_qMutex);

	return 0;
}

template <class Message>
bool GcPortT<Message>::sendMessage(Message * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex)
{
	return SkynetTalker<Message>::send( data );
}

/*
template <class Message>
int GcPortT::getSendSock()
{
	if (m_sendsock < 0) {
		m_sendsock = p_skynet->get_send_sock(m_msgtype);
	}
	return m_sendsock;
};
*/

/*
template <class Message>
int GcPortT::getRecvSock()
{  
	if (m_recvsock < 0) {
		m_recvsock = p_skynet->listen(m_msgtype, m_modname);
		if (m_recvsock < 0) {
			std::cout << "SkynetTalker::getRecvSock(): *** error opening rcv socket";      
		}
		//std::cout << "got receive socket " << m_recvsock << "\n"; //TEMPORARY
	}
	return m_recvsock;
}
*/


/*
template <class Message>
bool GcPortT::sendMessage(void * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex)
{
	int bytesSent;
	bool ret = true;

	DGClockMutex(p_dataBufferMutex);

	// Assume the message is already serialized
	bytesSent = p_skynet->send_msg(getSendSock(), data, bytesToSend, 0);

	if(bytesSent != bytesToSend)
	{
		cerr << "SkynetTalker::send() sent " << bytesSent << "\n";
		ret = false;
	}
	else
	{
		//cout << "successfully sent" << bytesSent << " bytes on socket " << m_sendsock << "\n";
	}

	DGCunlockMutex(p_dataBufferMutex);

	return ret;
}


//allocates memory.  returns null if unsuccessful
template <class Message>
void * GcPortT::receiveMessage(int * bufSize)
{
	void * newstuff = NULL;
	int bytesReceived;
	int requiredSize;
	pthread_mutex_t m_dataBufferMutex;
	int recvsock = getRecvSock();
	if (recvsock < 0)
	{
		return NULL;
	}

	//does not wait for messages
	if (!p_skynet->is_msg(recvsock))
	{
		//std::cout << "no message \n";
		return NULL;
	}
	else
	{
		//std:cout << "got message \n";
	}

	DGCcreateMutex(&m_dataBufferMutex);

	// Build the mutex list. We want to protect the data buffer.  I'm not positive we need this.
	int numMutices = 1;
	pthread_mutex_t* ppMutices[2];
	ppMutices[0] = &m_dataBufferMutex;
	//if(pMutex != NULL)
		//{
		//ppMutices[1] = pMutex;
		//numMutices++;
		//}

	requiredSize = p_skynet->get_msg_size(recvsock);
	//std::cout << "got message size: " << requiredSize << "\n";
	if (requiredSize <= 0)
	{
		std::cout << "This should not happen \n";
		return NULL;
	}
	newstuff = malloc(requiredSize); //requiredSize should be in bytes

	bytesReceived = p_skynet->get_msg(recvsock, newstuff, requiredSize,
			0, ppMutices, false, numMutices);

	DGCunlockMutex(&m_dataBufferMutex);

	if (bytesReceived != requiredSize)
	{
		std::cout << "recieveMsg -- There's a size problem here \n";
	}

	(*bufSize) = bytesReceived;
	return newstuff;

}

*/


#endif // _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__
