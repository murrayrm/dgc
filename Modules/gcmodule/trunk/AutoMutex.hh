/*!
 *  \file  AutoMutex.hh
 *  \brief Mutex utility class 
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *
 * The AutoMutex class provides a wrapper around a pthread mutex with
 * two advantages:
 * 1) A static function allows the initialization of pthread mutex's so that
 *    the mutex is initialized with recursion detection -- threads which
 *    already own the mutex will not block if they try to lock it again
 * 2) the AutoMutex instance wrapper's destructor releases the mutex, 
 *    allowing a function to syntactically return a value, then 
 *    release a mutex:
 *    int foo( ) {
 *      Automutex a( m_myRecursiveMutex );
 *      return incAndGetValue();
 *
 *
 */

#ifndef _AUTOMUTEX_HH_2398THY903270V98G2Y30TY_
#define _AUTOMUTEX_HH_2398THY903270V98G2Y30TY_

#include <pthread.h>
#include <errno.h>
#include <vector>
#include <iostream>
using std::cerr;
using std::vector;

#include "dgcutils/DGCutils.hh"
#include "Singleton.hh"
//#include "IGcLogger.hh"
#include <sys/time.h>     /* struct timeval definition           */
#include <unistd.h>       /* declaration of gettimeofday()       */


/// Simple class to keep track of when a mutex is blocked
struct MutexWaitRecord {
  unsigned long long starttime;
  unsigned long long endtime;
  int id;
};

/// Simple wrapper class to lock a mutex on construction
/// and release it on destruction (perhaps after a return statement)
///
/// To use, instantiate a pthread_mutex_t structure and initialize 
/// with the static function  initMutex(..)
/// When you wish to lock the mutex, instantiate this class with you mutex
/// instance.  To unlock, destoy this object. 
template< class childT, int m_line >
class AutoMutex_base
{
  public:
  typedef vector<MutexWaitRecord> MutexWaitLog;

  protected:
  pthread_mutex_t * m;
  bool AMlockMutex(pthread_mutex_t* pMutex)
  {
    int err = pthread_mutex_trylock(pMutex);
    if( err == EBUSY ) {
      starttime = DGCgettime();
      err = pthread_mutex_lock(pMutex);
      bhadToWait = true;
    }
    if( err != 0) {
      cerr << "Couldn't lock mutex" << endl;
      return false;
    }

    return true;
  }

  void lock() {
    if( m ) {
      AMlockMutex(m);
      //DGClockMutex(m);
    }
  }

  void unlock() {
    if( m ) {
      DGCunlockMutex(m);
      if( bhadToWait ) {
        MutexWaitRecord r = { starttime, DGCgettime(), m_line };
        Singleton<MutexWaitLog>::Instance().push_back(r);
      }
    }
  }

  public:
    bool bhadToWait;
    //static MutexWaitLog waitLog;
    unsigned long long starttime;

  public:
  AutoMutex_base( pthread_mutex_t& mutex ) : m(&mutex), bhadToWait(false), starttime(0)
  { static_cast<childT*>(this)->lock(); }
  AutoMutex_base( pthread_mutex_t* mutex ) : m(mutex), bhadToWait(false), starttime(0)
  { static_cast<childT*>(this)->lock(); }
  ~AutoMutex_base()
  { static_cast<childT*>(this)->unlock(); }

  /// This static function is used to initialize a pthread_mutex_t structure
  /// so that it is recursive enabled -- threads will not block if they already
  /// have the mutex locked
  static bool initMutex( pthread_mutex_t* m )
  {
    pthread_mutexattr_t   mta;
    pthread_mutexattr_init( &mta );
    pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE);
    int err = 0;
    if((err = pthread_mutex_init(m, &mta)) != 0)
    {
      // For reference:
      //
      // from http://www.opengroup.org/onlinepubs/007908775/xsh/pthread_mutex_init.html
      //[EAGAIN]
      //    The system lacked the necessary resources (other than memory) to initialise another mutex. 
      //[ENOMEM]
      //    Insufficient memory exists to initialise the mutex. 
      //[EPERM]
      //    The caller does not have the privilege to perform the operation. 
      //
      //The pthread_mutex_init() function may fail if:
      //
      //[EBUSY]
      //    The implementation has detected an attempt to re-initialise the object referenced by mutex, a previously initialised, but not yet destroyed, mutex. 
      //[EINVAL]
      //    The value specified by attr is invalid. 
      //
      cerr << "Couldn't create mutex. error = " << err << endl;
      switch( err ) {
        case EAGAIN:
          cerr << "Error EAGAIN: ";
          cerr << "The system lacked the necessary resources (other than memory) to initialise another mutex. " << endl; break;
        case ENOMEM:
          cerr << "Error ENOMEM: ";
          cerr << "Insufficient memory exists to initialise the mutex. " << endl; break;
        case EPERM:
          cerr << "Error EPERM: ";
          cerr << "The caller does not have the privilege to perform the operation. " << endl; break;
          //The pthread_mutex_init() function may fail if:
        case EBUSY:
          cerr << "Error EBUSY: ";
          cerr << "The implementation has detected an attempt to re-initialise the object referenced by mutex, a previously initialised, but not yet destroyed, mutex. " << endl; break;
        case EINVAL:
          cerr << "Error EINVAL: ";
          cerr << "The value specified by attr is invalid. " << endl; break;
      }
      return false;
    }
    return true;
  }
  static void deleteMutex( pthread_mutex_t* mtx ) {
    DGCdeleteMutex( mtx );
  }

  static void getWaitLog( ostream& o ) {
    MutexWaitLog& l = Singleton<MutexWaitLog>::Instance();
    if( l.empty() )
      o << "WaitLog is empty" << endl;

    for( MutexWaitLog::iterator i = l.begin(); i!=l.end(); i++ ) {
      MutexWaitRecord& r = *i;
      o << "AutoMutex " << r.id << " waited at: " << r.starttime << " duration: " << r.endtime - r.starttime << endl;
    }
  }

  static void clearWaitLog( ) {
    MutexWaitLog& l = Singleton<MutexWaitLog>::Instance();
    l.clear();
  }

};

/// This class will use the default lock and unlock functions of the base
/// which will store a record marking each time the lock had to wait.
template< int lineT >
class AutoMutex_log : public AutoMutex_base<AutoMutex_log<lineT>,lineT> 
{
  public:
  AutoMutex_log( pthread_mutex_t& mutex ) : AutoMutex_base<AutoMutex_log,lineT>( mutex ) 
  { }
  AutoMutex_log( pthread_mutex_t* mutex ) : AutoMutex_base<AutoMutex_log,lineT>( mutex ) 
  { }
  ~AutoMutex_log()
  { }
};

class AutoMutex : public AutoMutex_base<AutoMutex,0> 
{
  public:
  void lock() {
    if( m ) {
      DGClockMutex(m);
    }
  }

  void unlock() {
    if( m ) {
      DGCunlockMutex(m);
    }
  }

  public:
  AutoMutex( pthread_mutex_t& mutex ) : AutoMutex_base<AutoMutex,0>( mutex ) 
  { }
  AutoMutex( pthread_mutex_t* mutex ) : AutoMutex_base<AutoMutex,0>( mutex ) 
  { }
  ~AutoMutex()
  { }
};


/// If you wish to see loggging of mutex colisions, you can uncomment the
/// following #define, which will change the AutoMutex class used throughout
/// the code from the minimal/streamlined default to one which checks
/// system time at lock & unlock time and stores a record every time
/// it has to wait.  
/// The log can then be printed out (preferably at close of module) with
/// a call to getWaitLog().  For the default AutoMutex class no records
/// will be present, resulting in a nearly empty message.
//#define AutoMutex   AutoMutex_log<__LINE__>

#endif // _AUTOMUTEX_HH_2398THY903270V98G2Y30TY_
