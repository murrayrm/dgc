/*!
 *  \file  GcPeerPort.hh
 *  \brief The IGcPort interface and GcPeerPort default implementation
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *
 *  IGcPort provides a consistent layer through which GcInterfaces can
 *  communicate to their variable delivery mechanisms.
 *  It provides queing of messages and requires pumping of the queue, to 
 *  send and receive from the underlying delivery mech.  
 *
 *  This class has become nearly completely redundant and merely adds
 *  yet another layer of queing to the GcInterface communications.  It
 *  is scheduled to be removed.
 *
 */

#ifndef _GcPeerPort_HH_S0FD9HSGH248T92HT048FH__
#define _GcPeerPort_HH_S0FD9HSGH248T92HT048FH__

#include <deque>
#include <list>
#include <map>
#include <sstream>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include "interfaces/sn_types.h"
#include "gcmodule/AutoMutex.hh"
#include "gcmodule/GcTimebomb.hh"
#include "gcmodule/IGcLogger.hh"
#include "gcmodule/GcMemoryMan.hh"
//#include "gcmodule/GcInterface.hh" // do not include, instead forward declare to break 
//the circular reference
#include "gcmodule/GcPortT.hh"
using std::list;
using std::string;

// forward declaration
template< class, class, sn_msg, sn_msg, modulename > class GcInterface ;

template < class YourDirectiveT, class YourStatusT, sn_msg sn_directive_msg, 
          sn_msg sn_status_msg, modulename sn_modname >
class GcPeerPort : public GcPortT<YourStatusT> 
{
    typedef GcInterface< YourDirectiveT, YourStatusT, sn_directive_msg, sn_status_msg, sn_modname >  GcInterfacePeer;
    GcInterfacePeer* m_interface;
    typedef GcPortT<YourStatusT>   P;
  public:
    GcPeerPort(GcInterfacePeer* i, int sn_key, IGcLogger* logger, bool bStale=false, int nTimeToHold=10 ) ;
    virtual ~GcPeerPort();

    //__message READING functions
    //gets the newest message sent or received
    YourStatusT* getNewestMessage(); //gets a pointer to the newest message
    void     releaseMessage( YourStatusT* );

    //__message HANDLING functions
//    bool handleMsgs(); //clears all of the messages that are ready to die

    bool haveMsgs() ;

    bool recvMsgs(); //receives all of the new messages

};
#ifndef OMIT_TEMPLATE_DEFS
#include "gcmodule/GcPeerPort.template.hh"
#endif


#endif
