#ifndef _GCMODULE_LOGGER_HH__F89JN4T2FJMH4G8902Y3N5G4C2F908Y34G784Y5GJHSEF__
#define _GCMODULE_LOGGER_HH__F89JN4T2FJMH4G8902Y3N5G4C2F908Y34G784Y5GJHSEF__

#include "gcmodule/IGcLogger.hh"
#include "StringUtil.hh"
#include <stdarg.h>
#include <fstream>
#include <ostream>
#include <vector>
#include <sstream>
using std::ofstream;
using std::ostream;
using std::vector;
using std::string;


/// This class works as a no-op / sink for messages that we do not want 
/// streamed into anything useful (for loglevels higher than the runtime level)
class nullOStringStream : public std::ostringstream
{
  public:
    // create a no-op for our nullOStringstream
    template<typename T>
      nullOStringStream& operator<<( T t ) { return *this; }
    //nullOStringStream& operator<<( nullOStringStream& n, T t ) { return n; }

};



/// This class embodies the logging fascility of GcModule.  It can
/// also be instantiated stand-alone for "lightweight" apps that
/// utilize GcInterfaces without an actual GcModule instance
class GcModuleLogger : public IGcLogger
{
  // logging
  // GcModule provides utilities for logging that conform to the 
  // Unit Testing standards
  public:
    /// gclog(n) is available via a macro until a better way of protecting
    /// against multithreaded writes is developed
    //virtual ostream& gclog(int nLevel) = 0;
    //#define gclog(n)   gclogProtected(n).getOstream()
    int setLogLevel( int nLevel ) { int n = nLevel; m_nLogLevel = nLevel; return n; }
    void gclogProtected( int nLevel, const char* format, ... ) {
      va_list argList;
      va_start(argList, format);
      gclogProtected(nLevel).getOstream() <<  stringutil::vformat(format, argList);
      va_end(argList);
    }
    protectedostream gclogProtected(int nLevel) ;
    void addLogOstream( ostream& o );
    void addLogfile( string filename ) ;
  protected:
    std::ostream* m_plogStream;
    nullOStringStream m_nullStream;
    pthread_mutex_t   m_logMutex;
    typedef vector<ostream*> LogStreams;
    LogStreams m_logstreams;
    LogStreams m_ownedlogstreams;
    bool openLogFile( ) ;
    void logString( string s );
    int m_nLogLevel;

    class logstringbuf : public std::stringbuf 
    {
      public:
      logstringbuf() : stringbuf(ios_base::out), module(NULL)
      {}
      void setModule( GcModuleLogger* mod ) { module=mod; }

      protected:
      GcModuleLogger* module;
      virtual int sync( ) ;

    };
    friend class logstringbuf;
    logstringbuf  m_logbuf;

    string m_sName;

  public:
    GcModuleLogger( string sName ) ;
    virtual ~GcModuleLogger() ;

};
#endif // _GCMODULE_LOGGER_HH__F89JN4T2FJMH4G8902Y3N5G4C2F908Y34G784Y5GJHSEF__

