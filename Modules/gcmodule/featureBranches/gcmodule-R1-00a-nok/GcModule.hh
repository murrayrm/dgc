#include <deque>

class GcMessage //temporary - this will change
{
public:
  enum messageType { DIRECTIVE, STATUS };
  void * data;
  int dataNumBytes;
};

class MergedDirective
{
public:
  enum directiveTypes { DEFAULT, START, STOP }; //TEMPORARY 
  int directive;
private:
  void * rules;
  int rulesNumBytes;
};

class ControlStatus
{
public:
  enum statusTypes { FAILED, RUNNING, WAITING };
  int status;

  ControlStatus() { status = WAITING; }

private:
  void * additionalData;
  int adataNumBytes;
};

class GcModule
{
public:
  GcModule(void);
  
  //ControlStatus controlStatus; // the control status
  //MergedDirective mergedDirective;
  
  int readyToKill() { return (gcMessageEndThreadFlag && gcMainEndThreadFlag); }
  int kill() { gcMessageEndThreadFlag = 1; gcMainEndThreadFlag = 1; }

private:
  /*! This is the module's main thread. */
  int gcMainEndThreadFlag;
  void gcMainThread();

  /*! This thread is supposed to handle all communication. */
  int gcMessageEndThreadFlag;
  void gcMessageThread();

  MergedDirective Arbitrate(ControlStatus controlStatus);
  ControlStatus Control(MergedDirective mergedDirective);

  deque<GcMessage> arbDirectivesQueue;
};
