/*!
 *  \file  GcModuleTestDirective.hh
 *  \brief directive class used for testing gcmodule
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *  This file contains demonstration code on contructing a 
 *  complicated directive class for delivery on a GcInterface
 *
 *  It is not actively maintained
 *
 *
 */

#include "GcModule.hh"
#include "GcInterface.hh"
#include "gcinterfaces/GcModuleInterfaces.hh"
#include "boost/serialization/export.hpp"
#include <vector>
using std::vector;

struct testBase
{
  virtual ~testBase() {}
  virtual int getID() = 0;
  template< class ArT >
  void serialize( ArT &ar, const unsigned int v ) { }
  virtual testBase* newCopy() = 0;
};

struct test123 : public testBase
{
  int id;
  test123() : id(123) {}
  int getID() { return id; }

  template< class ArT >
  void serialize( ArT &ar, const unsigned int v )
  {
    ar & boost::serialization::base_object<testBase>(*this);
    ar & id;
  }
  testBase* newCopy() { return (testBase*) new test123(*this); } 
};
//BOOST_CLASS_EXPORT_GUID( test123, "test123_derived" )

class directive1 : public GcTransmissive
{

  public:
    #if 1
    directive1() { 
      memcpy(data,"do your job",12);
      test = 0; //new test123();
    }
    virtual ~directive1() {
      delete test;
    }
    directive1( const directive1& o ) {
      timeSent = o.timeSent;
      memcpy( data, o.data, sizeof(data)/sizeof(data[0]) );
      id = o.id;
      if( o.test )
        test = o.test->newCopy();
      else test = 0;
    }

    directive1& operator=( const directive1& o ) {
      if( &o != this )
      {
        timeSent = o.timeSent;
        memcpy( data, o.data, sizeof(data)/sizeof(data[0]) );
        id = o.id;
        if( o.test )
          test = o.test->newCopy();
        else test = 0;
      }
      return *this;
    }
    #endif
    char data[10*1024];
    int id;
    typedef vector<char*> Strings;
    Strings ss;
    testBase* test;
    
  bool operator==( const directive1& o ) const {
    bool eq = false;
    eq = ( 0 == memcmp( data, o.data, sizeof(data)/sizeof(data[0]) ) ) &&
         ( id == o.id ) &&
         ( ss == o.ss );

    return eq;
  }

  public:

    template< class ArchiveT >
      void serialize(ArchiveT &ar, const unsigned int version)  
      {
        ar & boost::serialization::base_object<GcTransmissive>(*this);
        ar & data;
        ar & id;
        // list all of the possible derived types here
        // or use the BOOST_CLASS_EXPORT_GUID macro
        //ar.template register_type<test123>();
        ar & test;
      }

    unsigned getDirectiveId( ) const
    {
      return id;
    }

    // to satisfy OStreamable... 
    // note, the const at the end -- very important! won't compile without
    std::string toString() const { 
      stringstream s("");
      s << "Directive1(" << id << ")" << "\n";
      s << "  - data=" << data << "\n";
      s << "        =" << std::hex << std::showbase; copy( data, &data[52], ostream_iterator<unsigned int>(s, " ") ); s << "\n";
      s << "    timeSent = " << std::dec << timeSent << "\n";
      if( test ) s << " test id=" << test->getID() << endl;
      else s << "test is NULL" << endl;
      copy( ss.begin(), ss.end(), ostream_iterator<string>(s, "\t\n") );

      //cout << " ---- in toString ---: " << s.str();
      return s.str();
    }
};

class directive2 : public directive1 {
public:
  directive2 () {
    instances++;
    //std::cout << "creating directive2. instances: " << instances<< endl;
  }
  directive2( const directive2& o ) : directive1(o) {
    instances++;
    //std::cout << "creating copy of directive2(" << o.getDirectiveId() << "). instances: " << instances<< endl;
  }
  virtual ~directive2() {
    directive2::instances--;
    //std::cout << "deleting a directive2(" << getDirectiveId() << ").  Instances left: " << instances << endl;
  }
  std::string toString() const { 
    stringstream s("");
    s << directive1::toString();
    s << "instances: " << instances << endl;
    return s.str();
  }
  directive2& operator=( const directive2& o ) { directive1::operator=(o); return *this; }
  static int instances;
  static int getcount() { return instances; }
};

class directive3 : public directive2 {
public:
};

class status1 : public GcInterfaceDirectiveStatus
{
  public:
    enum MyCustomStatus { NONE, STARTED, COMPLETED, FAILED };

  public:
    char blah[3*1024];
    //char blah[3];
    MyCustomStatus myCustomStatus;

  public:
    int getCustomStatus( ) { return myCustomStatus; }

    // for OStreamable.. 
    // note, the const at the end -- very important! won't compile without.
    std::string toString() const { 
      stringstream s("");
      s << "status1(" << id << ")" << "\n";
      s << "  - myCustomStatus=" << myCustomStatus;
      return s.str();
    }
  friend class boost::serialization::access;
  private:
     template< class ArchT >
      void serialize( ArchT & ar, const unsigned int ver )
      {
        ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
        ar & blah;
      }
};

class GcPort : public GcPortT<directive1>
{
  public:
    GcPort(int sn_key, sn_msg sn_msgtype, modulename sn_modname) :
      GcPortT<directive1>(sn_key, sn_msgtype, sn_modname, NULL)
  {}
    virtual ~GcPort() {}
};

#include "gcinterfaces/AdriveCommand.hh"
typedef GcInterface<directive3, AdriveResponse, SNsegGoals, SNtplannerStatus, MODrddftalkertestsend> GcInterfaceTest  ;
typedef GcInterface<directive3, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE, GCM_IN_PROCESS_DIRECTIVE, MODrddftalkertestsend> GcIntTestInProc  ;
//typedef GcInterface<directive2, status1, SNsegGoals, SNtplannerStatus, MODrddftalkertestsend> GcInterfaceTest  ;
//typedef GcInterface<directive2, status1, GCM_IN_PROCESS_DIRECTIVE, GCM_IN_PROCESS_DIRECTIVE, MODrddftalkertestsend> GcIntTestInProc  ;
//typedef GcInterface<directive1, status1, SNsegGoals, SNtplannerStatus, MODrddftalkertestsend> GcInterfaceTest  ;
//typedef GcInterface<directive1, status1, GCM_IN_PROCESS_DIRECTIVE, GCM_IN_PROCESS_DIRECTIVE, MODrddftalkertestsend> GcIntTestInProc  ;

