
#ifndef _STRING_UTIL_HH_JW89EVNRG8VNH94GWGH8945JYF928YNFCJHNIOWE4HG8__
#define _STRING_UTIL_HH_JW89EVNRG8VNH94GWGH8945JYF928YNFCJHNIOWE4HG8__

#include <string>


namespace stringutil {
std::string format(const char* fmt, ...) ;

std::string vformat(const char *fmt, va_list argPtr);

};


#endif // _STRING_UTIL_HH_JW89EVNRG8VNH94GWGH8945JYF928YNFCJHNIOWE4HG8__

