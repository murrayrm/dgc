/*!
 *  \file  IGcLogger.hh
 *  \brief Interface for conditional/multiplexed logging
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *
 * This interface defines utilities to log to several registered
 * ostreams via one common ostream, with built-in conditional logging: 
 * logLevel.  
 *
 * General Ostreams can be attached to the logger, or filenames can be 
 * provided for which the logger will maintain the ofstream internally.
 *
 * Additionally peer IGcLoggers can be attached, for which alternate
 * loglevels could be specified, along with alternate ostreams, allowing
 * simultaneous creation of sparse and verbose logs.
 *
 */

#ifndef _IGCLOGGER_HH_7YU4HN87FBYC37BC8ECV78GC97GB__
#define _IGCLOGGER_HH_7YU4HN87FBYC37BC8ECV78GC97GB__

#include <fstream>
#include <ostream>
#include <vector>
#include <string>
using std::string;
using std::ofstream;
using std::ostream;
using std::vector;

#include "gcmodule/AutoMutex.hh"


// forward declaration
class protectedostream;

/// Defines an interface guaranteed to be implemented to log
/// messages (somehow - depends on subclass)
//  Calls to log at a particular level are intended to be logged if
//  the logger is running at a level equal or higher than this level, only.
class IGcLogger
{
  public:
    virtual ~IGcLogger() {} 

  public:

    /// setLogLevel()
    ///
    /// set the threshold level at which a gclog calls will appear in ostreams
    virtual int setLogLevel( int nLevel ) = 0; 

    /// getLevel()
    ///
    /// return the current logLevel.  useful for creating if() statements
    /// where significant processing solely for a conditional log statement
    /// could otherwise be avoided if not logged
    virtual int getLevel() = 0;

    /// isLevel()
    ///
    /// similar to getLevel in intended use, returns a boolean if the specified
    /// level would be logged.
    /// Could be overriden in specialiezed loggers where the logger does not
    /// the traditional "log at this level or below" behavior, but instead
    /// have something like "log only at this level"
    virtual bool isLevel( int nLevel ) = 0;

    /// addLogOstream()
    ///
    /// attaches the handle of an ostream which should receive logging
    virtual void addLogOstream( ostream& o ) = 0;

    /// addLogfile()
    ///
    /// given a filename, will open the file and log to it, managing the 
    /// resources of that file internally
    virtual void addLogfile( string filename, bool bAppendTimestamp ) = 0;

    /// attachPeerLogger()
    ///
    /// Attach a separate IgcLogger instance to receive logging requests
    /// and process them in its own speciallized manner
    virtual void attachPeerLogger( IGcLogger* logger ) = 0;
    
    //////////////////////
    /// 
    /// gclog(n) is available via a macro until a better way of protecting
    /// against multithreaded writes is developed
    //virtual ostream& gclog(int nLevel) = 0;
    #define gclog(n)   gclogProtected(n).getOstream()

    /// gclogProtected()
    /// 
    /// main logging function, although generally gclog(n) should
    /// be used for shorthand
    /// main function to return a protectedostream which has
    /// both thread protection and returns a no-op ostream or
    /// the real multiplexing ostream based on the loglevel
    virtual protectedostream gclogProtected(int n) = 0;
    /// gclogProtected()
    ///
    /// overloaded version of the above function, allowing printf style
    /// formating
    virtual void gclogProtected( int nLevel, const char* format, ... ) = 0;


    /// Helper function, not intended for general use
    /// Should clean these in the interface; either remove or hide
    //
    /// logStringUnprotected()
    /// 
    /// low-level function that actually, unconditionally logs
    /// a string
    virtual void logStringUnprotected( string s ) = 0;

};

/// protectedostream class
///
/// wrapper class around a mutex and an ostream
/// Intended use is for this object to exist only as a temporary
/// variable, such that a protecting mutex is locked and released
/// for the time that the ostream is being written to.  A temporary
/// variable can be obtained by returning an instance (copy) from 
/// a function but not assigning it to any variable on the stack.
/// So the intended use looks like:
///   returnAprotectedostream().getOstream() << "string";
/// The constructor will be called, locking the mutex, the ostream
/// is referenced and operated on, and at the end of the statement,
/// the destructor of protectedostream is called releasing the 
/// mutex.
///
/// making a calls such as:
///   protectedostream p = returnAprotectedostream();
///   protectedostream& p = returnAprotectedostream();
/// will lead directly to deadlock or the mutex remained lock
/// and should just be avoided
/// 
/// making a call such as:
///   ostream& o = returnAprotectedostream().getOstream();
/// will entirely circumvent the protection that the mutex provides
/// and is not recommended
class protectedostream 
{
  pthread_mutex_t* m_pMutex;
  AutoMutex a;
  ostream* m_pStream;
  public:
    protectedostream(pthread_mutex_t* m, ostream* s) : m_pMutex(m), a(*m_pMutex), m_pStream(s)  { }
    ~protectedostream() { }

    ostream& getOstream() { return *m_pStream; };
};

#endif // _IGCLOGGER_HH_7YU4HN87FBYC37BC8ECV78GC97GB__
