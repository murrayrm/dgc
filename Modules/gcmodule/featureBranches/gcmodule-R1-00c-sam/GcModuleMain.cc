#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"
#include <assert.h>
#include "GcModuleTestDirective.hh"
#include "GcInterface.hh"


class GcInterfaceTest  : public GcInterface<directive1, status1, 321, SNrddf, MODrddftalkertestsend> {};

#define CP1REPTIME 5
class GcModuleA : public GcModule
{
	public:
		GcPort * ctrlPort1;
		//GcInterfaceTest * testInterface;


		class ControlStatusA : public ControlStatus {};
		class MergedDirectiveA : public MergedDirective {};
		ControlStatusA m_ctrlStatusA;
		MergedDirectiveA m_mgddA;

		GcModuleA(void) : GcModule( &m_ctrlStatusA, &m_mgddA )
		{
			int sn_key = 321;
			sn_msg sn_msgtype = SNrddf;
			modulename sn_modtype = MODrddftalkertestsend;

			GcPort * port1 = new GcPort(sn_key,sn_msgtype,sn_modtype);
			port1->sendFlag = 1; //TEMPORARY: for now, modules can only either send or recieve

			addPort(port1);
			ctrlPort1 = (GcPort*)getPortofType(SNrddf);

			//testInterface = new GcInterfaceTest();
			//addPort(testInterface);
		}

		void Control(ControlStatus* cs, MergedDirective* md)
		{
			assert( cs );
			assert( md );
			ControlStatus& controlStatus = *cs;
			MergedDirective& mergedDirective = *md;

			if (mergedDirective.directive == MergedDirective::RUN)
			{
				//just some junk
				directive1 d1;

				//periodically sends new messages
				if (ctrlPort1->getNumMsgs() == 0)
				{
					ctrlPort1->addMsg(&d1);
					std::cout << "directive added: " << d1.data << "\n";
					sleep(5); //TEMPORARY
				}

			}
			return;
		}
};


int main(void) //main(int argc, char **argv)
{
	GcModuleA moduleA;

	moduleA.Start();

	while (!moduleA.IsStopped())
	{
		sleep(1);
	}

}
