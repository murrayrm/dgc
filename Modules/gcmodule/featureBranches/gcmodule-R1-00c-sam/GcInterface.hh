#ifndef _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__
#define _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__

#include "GcModule.hh"



class ITransmissiveDirective {
	public:
		virtual ~ITransmissiveDirective() {} 
		virtual unsigned int produceUniqId() = 0;
		// Cannot have a virtual templated function... 
		// Developers, you must implment this function(remove 'virtual'), 
		// the linker will remind you...
		//template<class ArchiveT>
		//virtual void serialize(ArchiveT, const unsigned int version ) = 0;
};

template <class YourDirectiveT, class YourStatusT>
class TransactionPacket
{
	public:
		enum PacketType { NONE, DIRECTIVE, STATUS, NUMPACKETTYPES };
	private:
		void init( unsigned id, PacketType t, YourDirectiveT* d, YourStatusT* s )
		{	
			m_id = id;
			m_packetType = t;
			m_dir = d;
			m_stat = s;
		}
	public:
		TransactionPacket( YourDirectiveT* d ) 
		{ 
			if( d )
				init(d->produceUniqId(),DIRECTIVE,d,NULL);
			else
				init(-1,NONE, NULL, NULL );
		}
		TransactionPacket( unsigned id, YourStatusT* s ) { init(id,STATUS,NULL,s); }
		TransactionPacket( ) { init(-1,NONE,NULL,NULL); }

	public:
		unsigned int 							m_id;
		PacketType 								m_packetType;
		YourDirectiveT*			 			m_dir;
		YourStatusT*							m_stat;

	public:
		template<class ArchiveT> void serialize( ArchiveT ar, const unsigned int ver )
		{
			ar & m_id;
			ar & m_packetType;
			ar & m_dir;
			ar & m_stat;
		}

};

/// This is the interface the arbiter will use to recv packets from another modules
/// controller and to send status responses back on
template<class YourDirectiveT, class YourStatusT>
class INorthfaceT 
{
	public:
		// returns false if no directives waiting, or invalid pointer
		// returns true with oldest unread directive
		virtual bool getNewDirective( YourDirectiveT* dir ) = 0;

		// sends a status response to a directive with id
		virtual bool sendResponse( unsigned id, YourStatusT* s ) = 0;

		// checks for undread directives
		virtual bool haveNewDirective() = 0;

		virtual ~INorthfaceT() {}
};

/// This is the interface the controller will use to send directives down to the next
/// modules arbiter.
template<class YourDirectiveT, class YourStatusT> 
class ISouthfaceT
{
	public:
		virtual bool sendDirective( YourDirectiveT* dir ) = 0;

		virtual YourStatusT getStatusOf( unsigned id ) = 0;
		virtual bool isCompleted( unsigned id ) = 0;
		virtual YourStatusT getLatestStatus( ) = 0;
		virtual bool haveNewStatus() = 0;

		virtual ~ISouthfaceT() {}
};

template< class YourDirectiveT, class YourStatusT, int sn_key, sn_msg sn_msgtype, modulename sn_modname >
class GcInterface : public GcPortT< TransactionPacket< YourDirectiveT, YourStatusT > >,
	public INorthfaceT<YourDirectiveT,YourStatusT>, public ISouthfaceT<YourDirectiveT,YourStatusT>
{
	public:
		typedef INorthfaceT<YourDirectiveT, YourStatusT> 		Northface;
		typedef ISouthfaceT<YourDirectiveT, YourStatusT> 		Southface;
		typedef TransactionPacket< YourDirectiveT, YourStatusT > InterfacePacket ;

		GcInterface() : GcPortT<InterfacePacket>( sn_key, sn_msgtype, sn_modname ) {}
		virtual ~GcInterface() {} 

		Northface& getNorthface() { return *this; }
		Southface& getSouthface() { return *this; }


		/// implement the southinterface

		bool sendDirective( YourDirectiveT* dir )
		{
			InterfacePacket i(dir);
			addMsg( &i );
			return true;
		}

		YourStatusT getStatusOf( unsigned id )
		{
		#warning "not yet implmented"
			YourStatusT s;
			return s;
		}

		bool isCompleted( unsigned id )
		{
		#warning "not yet implemented ; nor cousins"
			return true;
		}

		YourStatusT getLatestStatus( )
		{
		#warning "not yet implmented"
			YourStatusT s;
			return s;
		}

		bool haveNewStatus() 
		{
			#warning "not yet implmented"
			return true;
		}


		/// implement the north interface
		//

		bool haveNewDirective() 
		{
			#warning "not yet impolmentd"
			return true;
		}

		bool getNewDirective( YourDirectiveT* dir )
		{
			if( !dir )
				return NULL;
			int nSize=0;
			InterfacePacket* iPack = GcPortT<InterfacePacket>::getNewestMessage( &nSize );
			if( iPack->m_packetType == InterfacePacket::DIRECTIVE )
			{
				*dir = *(iPack->m_dir);
				return true;
			}
			return false;
		}

		bool sendResponse( unsigned id, YourStatusT* s )
		{
			InterfacePacket i(id, s);
			addMsg( &i );
			return true;
		}


};

#endif // _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__

