/*!
 *  \file  GcModuleMain.cc
 *  \brief
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 * A test application for the GcModule library.
 *
 * This application is not actively maintained.
 *
 */


#define OMIT_TEMPLATE_DEFS

#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"
#include <assert.h>
#include "GcModuleTestDirective.hh"
#include "GcInterface.hh"
#include "GcModuleContainer.hh"


int directive2::instances = 0;

#define directive2 directive3
#define status1     AdriveResponse

#define CP1REPTIME 5

#define SOUTHFACELITE 0

class GcModuleA : public GcModule
{
  public:
      #if SOUTHFACELITE
    GcInterfaceTest::SouthfaceLite* southface;
    GcIntTestInProc::SouthfaceLite* testInProcInterfaceSouth;
      #else
    GcInterfaceTest::Southface* southface;
    GcIntTestInProc::Southface* testInProcInterfaceSouth;
      #endif
    GcInterfaceTest::Northface* verifyTest;

    GcIntTestInProc::Northface* verifyInProcInterfaceNorth;


    class ControlStatusA : public ControlStatus {};
    class MergedDirectiveA : public MergedDirective {
      public:
        enum Status { NONE, STOP, RUN };
        Status directive;
    };
    ControlStatusA m_ctrlStatusA;
    MergedDirectiveA m_mgddA;

    GcModuleA( int sn_key ) : GcModule( "GcModuleTestSend", &m_ctrlStatusA, &m_mgddA, 10000, 10000, true )
  {
      #if SOUTHFACELITE
    southface = GcInterfaceTest::generateSouthfaceLite(sn_key, this);
    testInProcInterfaceSouth = GcIntTestInProc::generateSouthfaceLite(sn_key, this);
      #else
    southface = GcInterfaceTest::generateSouthface(sn_key, this);
    testInProcInterfaceSouth = GcIntTestInProc::generateSouthface(sn_key, this);
    southface->setStaleThreshold(16);
    testInProcInterfaceSouth->setStaleThreshold(16);
      #endif
    verifyInProcInterfaceNorth = GcIntTestInProc::generateNorthface(sn_key, this);

  }

    ~GcModuleA() {
      #if SOUTHFACELITE
      GcInterfaceTest::releaseSouthfaceLite( southface );
      GcIntTestInProc::releaseSouthfaceLite( testInProcInterfaceSouth );
      #else
      GcInterfaceTest::releaseSouthface( southface );
      GcIntTestInProc::releaseSouthface( testInProcInterfaceSouth );
      #endif
      GcIntTestInProc::releaseNorthface( verifyInProcInterfaceNorth );
    }

    void arbitrate(ControlStatus* cs, MergedDirective* md)
    {
      MergedDirectiveA& mergedDirective = *(MergedDirectiveA*)md;

      gclog(1) << "mergedDir set to run\n" << std::endl;
      mergedDirective.directive = MergedDirectiveA::RUN;
    }

    void control(ControlStatus* cs, MergedDirective* md)
    {
      assert( cs );
      assert( md );
      //ControlStatusA& controlStatus = *dynamic_cast<ControlStatusA*>(cs);
      MergedDirectiveA& mergedDirective = *(MergedDirectiveA*)md;

      if (mergedDirective.directive == MergedDirectiveA::RUN)
      {
        //just some junk
        directive2 d1;
        static int i = 60000;
        d1.id = i++;
        d1.test = new test123();
        memcpy( d1.data, "123456879\0", 10 );

        #if ! SOUTHFACELITE
        deque<unsigned> noAck = southface->orderedDirectivesWaitingForResponse();
        gclog(1) << "dirctives that have not been responeded to: ";
        copy( noAck.begin(), noAck.end(), ostream_iterator<unsigned>( gclog(1), ", " ) );
        gclog(1) << endl;
        #endif


        //periodically sends new messages
        {
          southface->sendDirective(&d1);
          testInProcInterfaceSouth->sendDirective(&d1); // validate delivery, with a loopback
          gclog(1) << "directive added: " << d1.data << ":" << d1.getDirectiveId() << "\n";
        }

        #if ! SOUTHFACELITE
        gclog(1) << "validating directives....: " << endl;
        while( verifyInProcInterfaceNorth->haveNewDirective() )
        {
          directive2 v;
          if( !verifyInProcInterfaceNorth->getNewDirective( &v ) ) {
            gclog(1) << "ASSERT ERROR in GcModule: could not get directive when haveNewDirective indicated available" << endl;
            exit(0);
          }

          directive2 s = testInProcInterfaceSouth->getSentDirective( v.getDirectiveId() );

          if( !(s==v) ) {
            gclog(1) << "ASSERT ERROR in GcModuleMain:"
              << "received and sent directictives differ" << endl;
            gclog(1) << "sent directive " << s.getDirectiveId() << endl << s << endl;
            gclog(1) << "received directive " << v.getDirectiveId() << endl << v << endl;
            exit(0);
          }

          gclog(1) << "validated directive " << v.getDirectiveId() << endl;

          //# send a response... 
          {
            status1 s1;
            verifyInProcInterfaceNorth->sendResponse( &s1, v.getDirectiveId() );
          }
        }

        while( testInProcInterfaceSouth->haveNewStatus() ) {
          status1* latest = testInProcInterfaceSouth->getLatestStatus();
          latest = NULL;
        }
          
        #endif


        bool bNoStatus = true;
        while( southface->haveNewStatus() )
        {
          bNoStatus = false;
          gclog(2) << "have new status" << endl;

          #if ! SOUTHFACELITE
          status1* stat = southface->getStatusOf( d1.id );
          if( stat )
          {
            gclog(1) << "received status on " << stat->getDirectiveId() << endl;
            //gclog(1) << " data: " << stat->blah << endl;
          }
          #endif

          status1* latest = southface->getLatestStatus();
          if( latest )
          {
            gclog(1) << " latest status was for id: " << latest->getDirectiveId() << endl;
          }
          else {
            gclog(1) << " there was no latest status " << endl;
            exit(-1);
          }
        }
        if( bNoStatus )
          gclog(2) << "no new status available" << endl;
      }
      return;
    }

    /// in event based we need at least one message to get things going...
    void SnowballEventBased() {
      directive2 d1;
      d1.id = 777;
      southface->sendDirective(&d1);
    }

    void UT_1()
    {
       // make sure the message pump is running
      ReStart();

      directive2 d1;
      int i = 8000;
      d1.id = i; 
      d1.test = new test123();

      for( int x=0; x<10; x++ )
      {
        d1.id =  i++;
        southface->sendDirective(&d1);
        //handler.pumpPorts();
        usleep(1100);
      }
      for( int x=0; x<10; x++ )
      {
        gclog(1) << "waiting for status... " << endl;
        while( !southface->haveNewStatus() ) {usleep(10000);}
        status1* s = southface->getLatestStatus();
        gclog(1) << "statusid: " << s->getDirectiveId();
      }
      if( southface->haveNewStatus() )
        gclog(1) << "still have more status available!!!" << endl;
    }
};



int main(void) //main(int argc, char **argv)
{
  #if 1
  /// Test whether we can create and release a southface without
  /// seg-faulting...
  GcInterfaceTest::Southface* s;
  s = GcInterfaceTest::generateSouthface( 321, NULL, NULL ) ;
  GcInterfaceTest::releaseSouthface( s );

  /// test whether we can create a directive and destroy it -- debuggin
  directive2* testd = new directive2();
  delete testd;

  GcSimplePortHandlerThread handler;
  GcModuleLogger l( "simpleLogger" );
  l.setLogLevel(20);
  l.addLogfile( "logs/logger.out" );
  l.addLogOstream( std::cout );
  s = GcInterfaceTest::generateSouthface( 321, &handler, &l ) ;
  l.gclog(1) << "starting TestModule (northern/control'er)\n" << endl;

  handler.ReStart();
  l.gclog(1) << "starting TestModule (northern/control'er)\n" << endl;
  directive2 d1;
  l.gclog(1) << "starting TestModule (northern/control'er)\n" << endl;
  d1.id = 10;
  l.gclog(1) << "starting TestModule (northern/control'er)\n" << endl;
  s->sendDirective( &d1 );

  directive2 d2 = s->getSentDirective( d1.id );
  l.gclog(1) << "timestamp of sent directive: " << d2.timeSent << endl;
  if( d1.timeSent != d2.timeSent )
    l.gclog(1) << "times sent are not equal: " << d1.timeSent << " : " << d2.timeSent << endl;

  sleep( 1 );
  handler.Stop();
  GcInterfaceTest::releaseSouthface( s );
  #endif


  GcModuleLogger ll( "GcModuleTestSend" );
  ll.setLogLevel(15);
  ll.addLogfile( "logs/main.out.duplicate", false );
  GcModuleLogger low( "notverbose" );
  low.setLogLevel(2);
  low.addLogfile( "logs/notverbose.log", false );
  {
    GcModuleContainer modules;
    GcModuleA moduleA( 321 );
    moduleA.addLogOstream( std::cerr );
    moduleA.addLogfile( "logs/main.out" );
    modules.addModule( &moduleA );
    modules.setLogLevel(15);
    moduleA.attachPeerLogger( &ll );
    moduleA.attachPeerLogger( &low );


    moduleA.gclogProtected( 2, "testing printf style: %s %d times!\n", "yay", 3 );

    //moduleA.UT_1();

    moduleA.Start();
    moduleA.gclog(1) << "started. " << endl;
    //moduleA.SnowballEventBased();  /// should not have needed this -- arbitrate/control loop should
    //                                   be called periodically regardless of whether anything is 
    //                                   received so that messages can periodically be generated

    sleep( 19 );

    moduleA.gclog(1) << "stopping. " << endl;
    moduleA.Stop();
    moduleA.gclog(1) << "stopped. " << endl;
  }

  directive2 d3;
  ll.gclog(1) << endl << endl << "Counting directive instantiations..." << endl;
  ll.gclog(1) << d3 << endl;

}
