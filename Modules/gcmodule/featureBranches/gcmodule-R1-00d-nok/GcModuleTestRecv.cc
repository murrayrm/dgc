#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"
#include <assert.h>
#include "GcModuleTestDirective.hh"


#define CP1REPTIME 5
class GcModuleB : public GcModule
{
public:
  int printFlag;

	ControlStatus m_cs;
	MergedDirective	m_md;

  GcInterfaceTest * gcInterface;
  GcInterfaceTest::Northface * northface;

  GcModuleB(void) : GcModule( &m_cs, &m_md )
  {
    int sn_key = 321;
    printFlag = 0;

    gcInterface = new GcInterfaceTest( sn_key, this );
    if( gcInterface )
      northface = gcInterface->getNorthface();
  }

  virtual void control(ControlStatus* cs, MergedDirective* md )
  {
  }

  virtual void arbitrate(ControlStatus* cs, MergedDirective* md )
  {
		assert( md );
		assert( cs );

    //MergedDirectiveA& mergedDirective = *dynamic_cast<MergedDirectiveA*>md;
    directive1  d1;

    if ( northface->getNewDirective(&d1) ) // && printFlag)
    {
      std::cout << "got a directive: " << d1.data << "\t" << d1.getDirectiveId() << endl;
      printFlag = 0;


      if( d1.getDirectiveId() % 2 == 0 )
      {
        status1 status;
        status.id = d1.getDirectiveId();
        sprintf( status.blah, "yippy" );
        northface->sendResponse( &status );
      }
    }

  }
};


int main(void) //main(int argc, char **argv)
{
  GcModuleB moduleB;

  moduleB.Start();

  while (!moduleB.IsStopped())
  {
    sleep(1);
  }

}
