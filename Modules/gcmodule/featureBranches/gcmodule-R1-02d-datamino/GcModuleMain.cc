
#define OMIT_TEMPLATE_DEFS

#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"
#include <assert.h>
#include "GcModuleTestDirective.hh"
#include "GcInterface.hh"
#include "GcModuleContainer.hh"


int directive2::instances = 0;

#define directive2 directive1

#define CP1REPTIME 5
class GcModuleA : public GcModule
{
  public:
    GcInterfaceTest::Southface* southface;
    GcInterfaceTest::Northface* verifyTest;

    GcIntTestInProc::Northface* verifyInProcInterfaceNorth;
    GcIntTestInProc::Southface* testInProcInterfaceSouth;


    class ControlStatusA : public ControlStatus {};
    class MergedDirectiveA : public MergedDirective {
      public:
        enum Status { NONE, STOP, RUN };
        Status directive;
    };
    ControlStatusA m_ctrlStatusA;
    MergedDirectiveA m_mgddA;

    GcModuleA( int sn_key ) : GcModule( "GcModuleTestSend", &m_ctrlStatusA, &m_mgddA )
  {
    southface = GcInterfaceTest::generateSouthface(sn_key, this);
    testInProcInterfaceSouth = GcIntTestInProc::generateSouthface(sn_key, this);
    verifyInProcInterfaceNorth = GcIntTestInProc::generateNorthface(sn_key, this);

    southface->setStaleThreshold(14);
  }

    ~GcModuleA() {
      GcInterfaceTest::releaseSouthface( southface );
      GcIntTestInProc::releaseSouthface( testInProcInterfaceSouth );
      GcIntTestInProc::releaseNorthface( verifyInProcInterfaceNorth );
    }

    void arbitrate(ControlStatus* cs, MergedDirective* md)
    {
      MergedDirectiveA& mergedDirective = *(MergedDirectiveA*)md;

      gclog(1) << "mergedDir set to run\n" << std::endl;
      mergedDirective.directive = MergedDirectiveA::RUN;
    }

    void control(ControlStatus* cs, MergedDirective* md)
    {
      assert( cs );
      assert( md );
      //ControlStatusA& controlStatus = *dynamic_cast<ControlStatusA*>(cs);
      MergedDirectiveA& mergedDirective = *(MergedDirectiveA*)md;

      if (mergedDirective.directive == MergedDirectiveA::RUN)
      {
        //just some junk
        directive2 d1;
        static int i = 60000;
        d1.id = i++;
        d1.test = new test123();

        deque<unsigned> noAck = southface->orderedDirectivesWaitingForResponse();
        gclog(1) << "dirctives that have not been responeded too: ";
        copy( noAck.begin(), noAck.end(), ostream_iterator<unsigned>( gclog(1), ", " ) );
        gclog(1) << endl;


        //periodically sends new messages
        {
          southface->sendDirective(&d1);
          //testInProcInterfaceSouth->sendDirective(&d1); // validate delivery, with a loopback
          gclog(1) << "directive added: " << d1.data << ":" << d1.getDirectiveId() << "\n";
        }

        gclog(1) << "validating directives....: " << endl;
        while( false ) // verifyInProcInterfaceNorth->haveNewDirective() )
        {
          directive2 v;
          if( !verifyInProcInterfaceNorth->getNewDirective( &v ) ) {
            gclog(1) << "ASSERT ERROR in GcModule: could not get directive when haveNewDirective indicated available" << endl;
            exit(0);
          }

          directive2 s = testInProcInterfaceSouth->getSentDirective( v.getDirectiveId() );

          if( !(s==v) ) {
            gclog(1) << "ASSERT ERROR in GcModuleMain:"
              << "received and sent directictives differ" << endl;
            gclog(1) << "sent directive " << s.getDirectiveId() << endl << s << endl;
            gclog(1) << "received directive " << v.getDirectiveId() << endl << v << endl;
            exit(0);
          }

          gclog(1) << "validated directive " << v.getDirectiveId() << endl;
        }


        if( southface->haveNewStatus() )
        {
          gclog(2) << "have new status" << endl;

          status1* stat = southface->getStatusOf( d1.id );
          if( stat )
          {
            gclog(1) << "received status on " << stat->getDirectiveId() ;
            gclog(1) << " data: " << stat->blah << endl;
          }

          status1* latest = southface->getLatestStatus();
          if( latest )
          {
            gclog(1) << " latest status was for id: " << latest->getDirectiveId() << endl;
          }
          else
            gclog(1) << " there was no latest status " << endl;
        }
        else
          gclog(2) << "no new status available" << endl;

        usleep(200000); //TEMPORARY

      }
      return;
    }

    void UT_1()
    {
       // make sure the message pump is running
      ReStart();

      directive2 d1;
      int i = 8000;
      d1.id = i; 
      d1.test = new test123();

      for( int x=0; x<10; x++ )
      {
        d1.id =  i++;
        southface->sendDirective(&d1);
        //handler.pumpPorts();
        usleep(1100);
      }
      for( int x=0; x<10; x++ )
      {
        gclog(1) << "waiting for status... " << endl;
        while( !southface->haveNewStatus() ) {usleep(10000);}
        status1* s = southface->getLatestStatus();
        gclog(1) << "statusid: " << s->getDirectiveId();
      }
      if( southface->haveNewStatus() )
        gclog(1) << "still have more status available!!!" << endl;
    }
};



int main(void) //main(int argc, char **argv)
{
  #if 1
  GcSimplePortHandlerThread handler;
  GcInterfaceTest::Southface* s;
  GcModuleLogger l( "simpleLogger" );
  l.setLogLevel(10);
  l.addLogfile( "logger.out" );
  l.addLogOstream( std::cout );
  s = GcInterfaceTest::generateSouthface( 321, &handler, &l ) ;
  l.gclog(1) << "starting TestModule (northern/control'er)\n" << endl;

  handler.ReStart();
  l.gclog(1) << "starting TestModule (northern/control'er)\n" << endl;
  directive2 d1;
  l.gclog(1) << "starting TestModule (northern/control'er)\n" << endl;
  d1.id = 10;
  l.gclog(1) << "starting TestModule (northern/control'er)\n" << endl;
  s->sendDirective( &d1 );

  directive2 d2 = s->getSentDirective( d1.id );
  l.gclog(1) << "timestamp of sent directive: " << d2.timeSent << endl;
  if( d1.timeSent != d2.timeSent )
    l.gclog(1) << "times sent are not equal: " << d1.timeSent << " : " << d2.timeSent << endl;

  sleep( 1 );
  handler.Stop();
  GcInterfaceTest::releaseSouthface( s );
  #endif


  GcModuleContainer modules;
  GcModuleA moduleA( 321 );
  moduleA.addLogOstream( std::cerr );
  moduleA.addLogfile( "main.out" );
  modules.addModule( &moduleA );
  modules.setLogLevel(15);
  GcModuleLogger ll( "GcModuleTestSend" );
  ll.setLogLevel(15);
  ll.addLogfile( "main.out.duplicate", false );
  GcModuleLogger low( "notverbose" );
  low.setLogLevel(2);
  low.addLogfile( "notverbose.log", false );
  moduleA.attachPeerLogger( &ll );
  moduleA.attachPeerLogger( &low );


  moduleA.gclogProtected( 2, "testing prtinf style: %s %d times!\n", "yay", 3 );

  moduleA.UT_1();

  moduleA.Start();

  sleep( 130 );

  moduleA.Stop();


}
