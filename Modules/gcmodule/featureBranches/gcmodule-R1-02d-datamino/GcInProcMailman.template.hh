#ifndef _GCINPROCMAILMAN_TEMPLATE_HH_9283UHG87B34HJ8DHJ87HG982HRG072NFH7G284NHG__ 
#define _GCINPROCMAILMAN_TEMPLATE_HH_9283UHG87B34HJ8DHJ87HG982HRG072NFH7G284NHG__

#include "skynettalker/SkynetTalker.hh"

template <class MsgT>
class GcSkynetMailman : public IGcMailman<MsgT>
{
  protected:
  SkynetTalker<MsgT> talker;

  public:
    GcSkynetMailman( int sn_key, sn_msg sn_msgtype, modulename sn_modname ) : 
      talker(sn_key, sn_msgtype, sn_modname)
    {  }

  public:
    bool hasNewMessage() { return talker.hasNewMessage(); }
    bool receive(MsgT* msg) { return talker.receive(msg); }
    bool send(MsgT* msg) { return talker.send(msg); }
    bool setMode( typename IGcMailman<MsgT>::OperationMode m ) 
    { 
      if( m==IGcMailman<MsgT>::RECEIVER ) 
        talker.listen(); // initializes skynet
      return true; 
    }
};

template <class T>
class Singleton
{
public:
  static T& Instance() {
    static T _instance;
    return _instance;
  }
private:
  Singleton();          // ctor hidden
  ~Singleton();          // dtor hidden
  Singleton(Singleton const&);    // copy ctor hidden
  Singleton& operator=(Singleton const&);  // assign op hidden
};

template <class MsgT>
class GcInProcMailman : public IGcMailman<MsgT>
{
  protected:
  // helper class -- used as a singleton to register all the receivers for
  // a given messagetype
    class GcInProcMailDepot 
    {
      protected:
        typedef GcInProcMailman<MsgT> GcInProcMsgMailman;
        typedef list<GcInProcMsgMailman*>  InProcMailList;
        typedef map<string, InProcMailList > InProcMailMap;
        InProcMailMap map;

      public:
        bool registerListener( string mailtype, GcInProcMailman<MsgT>* mailman ) ;
        bool removeListener( string mailtype, GcInProcMailman<MsgT>* mailman ) ;
        bool broadcast( string mailtype, MsgT* msg ) ;

    };

  protected: 
    string m_mailtype;
    typedef deque<MsgT> MsgQ;
    MsgQ msgs;
    friend class GcInProcMailDepot;
    GcInProcMailDepot& depot;
    pthread_mutex_t m_mtxQ;
    void push_back( MsgT& msg ) ;

  public:
    GcInProcMailman( string msgtype, int modulename  );

    virtual ~GcInProcMailman( );

  public:
    typedef IGcMailman<MsgT> PMailman;

    bool hasNewMessage() ;
    bool receive(MsgT* msg) ;
    bool send(MsgT* msg) ;
    bool setMode( typename PMailman::OperationMode m ) ;

};
template<class MsgT>
GcInProcMailman<MsgT>::GcInProcMailman( string msgtype, int modulename  ) :
  depot( Singleton< GcInProcMailDepot >::Instance() )
{
  AutoMutex::initMutex(&m_mtxQ);
  stringstream s; s << "_" << msgtype << ":" << typeid(MsgT).name() << modulename;
  m_mailtype = s.str();
}

  template<class MsgT>
GcInProcMailman<MsgT>::~GcInProcMailman( )
{
  DGCdeleteMutex(&m_mtxQ);
}


template<class MsgT>
void GcInProcMailman<MsgT>::push_back( MsgT& msg ) {
  AutoMutex a(m_mtxQ);
  msgs.push_back( msg );
}

template<class MsgT>
bool GcInProcMailman<MsgT>::send(MsgT* msg) {
  AutoMutex a(m_mtxQ);
  return depot.broadcast( m_mailtype, msg );
}

template<class MsgT>
bool GcInProcMailman<MsgT>::receive(MsgT* msg) {
  AutoMutex a(m_mtxQ);
  if( hasNewMessage() )
  {
    *msg = msgs.front();
    msgs.pop_front();
    return true;
  }
  else
    return false;
}

template<class MsgT>
bool GcInProcMailman<MsgT>::setMode( typename PMailman::OperationMode m ) {
  switch(m) {
    case PMailman::RECEIVER:   depot.registerListener( m_mailtype, this ); break;
    case PMailman::SENDER: depot.removeListener( m_mailtype, this ); break;
    default:
                           return false;
  }
  return true;
}

  template<class MsgT>
bool GcInProcMailman<MsgT>::hasNewMessage() 
{ 
  AutoMutex a(m_mtxQ); 
  return !msgs.empty(); 
}



template<class MsgT>
bool GcInProcMailman<MsgT>::GcInProcMailDepot::registerListener( string mailtype, GcInProcMailman<MsgT>* mailman ) {
  InProcMailList& l = map[mailtype];
  l.push_back( mailman );
  return true;
}

template<class MsgT>
bool GcInProcMailman<MsgT>::GcInProcMailDepot::removeListener( string mailtype, GcInProcMailman<MsgT>* mailman ) {
  InProcMailList& l = map[mailtype];
  l.remove( mailman );
  return true;
}

template<class MsgT>
bool GcInProcMailman<MsgT>::GcInProcMailDepot::broadcast( string mailtype, MsgT* msg ) {
  InProcMailList& l = map[mailtype];
  if( l.empty() ) 
    return false;
  for( typename InProcMailList::iterator iter=l.begin(); 
      iter!=l.end(); 
      iter++ )
    (*iter)->push_back( *msg );
  return true;
}

#endif
