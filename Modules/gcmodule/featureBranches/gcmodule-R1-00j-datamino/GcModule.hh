#ifndef _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__
#define _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__

#include <deque>
#include <list>
#include <map>
#include <sstream>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include <stdarg.h>
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "StringUtil.hh"
#include "gcinterfaces/GcModuleInterfaces.hh"
#include "gcmodule/AutoMutex.hh"
#include "gcmodule/IGcLogger.hh"
using std::list;
using std::map;
using std::string;

// use this value as a substitue sn_msg type --> directives will only
// be delivered to GcPorts/GcInterfaces within the same process, no 
// skynet connection is created
#define GCM_IN_PROCESS_DIRECTIVE ((sn_msg)-42) 


// ______
// IGcPort
//
// GcPort is a virtual class.  It should be
// inherited anew for each combination of
// message type and module being used.
// Each time it is inherited,
// serialize and deserialize must be defined.
//
// A port is a connection from _a given module_
// ..with a given shared _skynet key_
// ...for a certain kind of _message type_ 
// (the module and message type options are
// defined in sn_types.hh, the skynet key is
// defined when the port starts running.
// 
// A GcPort contains a list of messages that
// have been sent.

class IGcPort
{
  public:
    virtual ~IGcPort() {}
    //__message WRITING functions
    //		virtual bool addMsg(Message * msg, int msgSize = -1) = 0; //adds a message to the queue

    //__message HANDLING functions
    virtual bool handleMsgs() = 0; //clears all of the messages that are ready to die
    virtual bool sendMsgs() = 0; //sends all of the recently added messages
    virtual bool recvMsgs() = 0; //receives all of the new messages
    virtual sn_msg getMsgType() = 0;
    virtual void setSendFlag(int) = 0; // use to set the send flag
    virtual bool getSendFlag()  = 0 ;

};

template <class MsgT>
class IGcMailman
{
  public:
    virtual bool hasNewMessage() = 0;
    virtual bool receive(MsgT* msg) = 0;
    virtual bool send(MsgT* msg) = 0;
    enum OperationMode { SENDER, RECEIVER } ;
    virtual bool setMode( OperationMode m ) = 0;

    virtual ~IGcMailman() {}
};

template <class MsgT>
class GcSkynetMailman : public IGcMailman<MsgT>
{
  protected:
  SkynetTalker<MsgT> talker;

  public:
    GcSkynetMailman( int sn_key, sn_msg sn_msgtype, modulename sn_modname ) : 
      talker(sn_key, sn_msgtype, sn_modname)
    {  }

  public:
    bool hasNewMessage() { return talker.hasNewMessage(); }
    bool receive(MsgT* msg) { return talker.receive(msg); }
    bool send(MsgT* msg) { return talker.send(msg); }
    bool setMode( typename IGcMailman<MsgT>::OperationMode m ) { return true; }
};

template <class T>
class Singleton
{
public:
  static T& Instance() {
    static T _instance;
    return _instance;
  }
private:
  Singleton();          // ctor hidden
  ~Singleton();          // dtor hidden
  Singleton(Singleton const&);    // copy ctor hidden
  Singleton& operator=(Singleton const&);  // assign op hidden
};

template <class MsgT>
class GcInProcMailman : public IGcMailman<MsgT>
{
  protected:
  // helper class -- used as a singleton to register all the receivers for
  // a given messagetype
    class GcInProcMailDepot 
    {
      protected:
        typedef GcInProcMailman<MsgT> GcInProcMsgMailman;
        typedef list<GcInProcMsgMailman*>  InProcMailList;
        typedef map<string, InProcMailList > InProcMailMap;
        InProcMailMap map;

      public:
        bool registerListener( string mailtype, GcInProcMailman<MsgT>* mailman ) {
          InProcMailList& l = map[mailtype];
          l.push_back( mailman );
          //cout << "sizeof listener list of type " << mailtype << ": " << l.size() << endl;
          return true;
        }

        bool removeListener( string mailtype, GcInProcMailman<MsgT>* mailman ) {
          InProcMailList& l = map[mailtype];
          l.remove( mailman );
          return true;
        }

        bool broadcast( string mailtype, MsgT* msg ) {
          InProcMailList& l = map[mailtype];
          //cout << "broadcasting msg to " << l.size() << " listeners" << endl;
          if( l.empty() ) 
            return false;
          for( typename InProcMailList::iterator iter=l.begin(); 
              iter!=l.end(); 
              iter++ )
            (*iter)->msgs.push_back( *msg );
          return true;
        }

    };

  protected: 
    string m_mailtype;
    typedef deque<MsgT> MsgQ;
    MsgQ msgs;
    friend class GcInProcMailDepot;
    GcInProcMailDepot& depot;

  public:
    GcInProcMailman( string msgtype ) :
      //depot( Singleton< GcInProcMailDepot<MsgT> >::Instance() )
      depot( Singleton< GcInProcMailDepot >::Instance() )
    {
      stringstream s; s << "_" << msgtype << ":" << typeid(MsgT).name() ;
      m_mailtype = s.str();
    }

  public:
    bool hasNewMessage() { return !msgs.empty(); }
    bool receive(MsgT* msg) {
      if( hasNewMessage() )
      {
        *msg = msgs.front();
        msgs.pop_front();
        return true;
      }
      else
        return false;
    }
    bool send(MsgT* msg) {
      return depot.broadcast( m_mailtype, msg );
    }
    typedef IGcMailman<MsgT> PMailman;
    bool setMode( typename PMailman::OperationMode m ) {
      switch(m) {
        case PMailman::RECEIVER:   depot.registerListener( m_mailtype, this ); break;
        case PMailman::SENDER: depot.removeListener( m_mailtype, this ); break;
        default:
          return false;
      }
      return true;
    }
};


template <class Message>
class GcPortT : public IGcPort 
{
  public:
    GcPortT(int sn_key, sn_msg sn_msgtype, modulename sn_modname, IGcLogger* logger, bool bStale=false, int nTimeToHold=10 ) ;
    virtual ~GcPortT();

    //for now, this defines whether a given message has come in
    //or is going out
    enum msgStatus { READYTOKILL, READYTOSEND, SENT, RECEIVED, SENDERROR };

    //__message READING functions
    //gets the newest message sent or received
    Message* getNewestMessage(); //gets a pointer to the newest message
    time_t lastMsgTimeSent();
    //bool getMsg(id,void * msg);, bool getMsg(id);

    //__message WRITING functions
    bool addMsg(Message * msg, bool bAsynchronous=false ); //adds a message to the queue

    //__message HANDLING functions
    bool handleMsgs(); //clears all of the messages that are ready to die

    sn_msg getMsgType() { return m_msgtype; }
    int getNumMsgs() { return (int)msgQ.size(); };
    bool haveMsgs() { return !msgQ.empty(); };

    virtual void setSendFlag(int set) { // use to set the sendflag
      sendFlag = set; 
      m_pMailman->setMode( set ? IGcMailman<Message>::SENDER : IGcMailman<Message>::RECEIVER ) ;
    } 
    virtual bool getSendFlag() {return sendFlag;}

  protected:
    int sendFlag; //is this a sending (1) or receiving (0) type of port

    //wraps each message up with relevant data
    //including arrival time (such that we know
    //when it should be cleared), status and so
    //on.
    template<class MWM>
      class MessageWrapperT
      {
        public:
          MWM * msg; //the message in its serial or "to be sent" format
          int msgSize;
          time_t timeSentRcvd; //default behavior is to be clear after 100 ms, say
          msgStatus status; //if status is READYTOKILL, this message is as good as dead
          pthread_mutex_t m_dataBufferMutex; //?
      };
    typedef MessageWrapperT<Message> MessageWrapper ;
    typedef deque< MessageWrapper > MsgQ;
    MsgQ msgQ;
    pthread_mutex_t m_qMutex; //?

    //__Message handling
    IGcMailman<Message>* m_pMailman;
    bool m_bStale;
    int maxTimeToHold;
    sn_msg m_msgtype;

  public:

    bool sendMsgs(); //sends all of the recently added messages
    bool recvMsgs(); //receives all of the new messages
    bool sendMessage(Message * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex);


    class IGcMessageFilter
    {
      public:
        virtual ~IGcMessageFilter() {}
        virtual bool filter( Message* m ) = 0;
    };

    void changeMsgStatus( IGcMessageFilter& filter, int toStat, bool bOnlyFirst=true )
    {
      for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
        if( filter.filter((*i).msg) ) {
          (*i).status = (msgStatus)toStat;
          if(bOnlyFirst) break;
        }
    }

    void moveMsgsStatus( msgStatus fromStat, msgStatus toStat )
    {
      for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
        if( (*i).status == fromStat ) (*i).status = toStat;
    }

    void flushMsgs( )
    {
      for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
        (*i).status = READYTOKILL;
      handleMsgs();
    }
  protected:
  IGcLogger* m_logger;
};


  template <class Message>
bool GcPortT<Message>::addMsg(Message * msg, bool bAsynchronous) 
{ 

  bool success = DGClockMutex(&m_qMutex);

  MessageWrapper mwrap;
  mwrap.timeSentRcvd = (time_t) -1;
  mwrap.status = READYTOSEND;

  // create a copy -- copy constructor must be implemented
  mwrap.msg = new Message( *msg );
  mwrap.msgSize = -1;

  success = DGCcreateMutex(&(mwrap.m_dataBufferMutex));
  //std::cout << "successful mutex? " << success << "\n"; //TEMPORARY

  msgQ.push_back(mwrap); //I think this copies all the data

  // empty queue now if we want things done now
  if( !bAsynchronous )
    sendMsgs();

  success = DGCunlockMutex(&m_qMutex);

  return true;
}

class MergedDirective : public OStreamable
{
  public:
    int id;
    virtual ~MergedDirective() {}
    virtual string toString() const { std::ostringstream s(""); s << id; return s.str(); }
};

class ControlStatus : public OStreamable
{
  public:
    enum statusTypes { FAILED, RUNNING, STOPPED } status;

    virtual ~ControlStatus() {}
    virtual string toString() const { std::ostringstream s(""); s << status; return s.str(); }

};

// forward declare the container class
class GcModuleContainer;

class IGcPortHandler
{
  public:
    virtual ~IGcPortHandler() {}

    virtual bool addPort( IGcPort* newport ) = 0;
    virtual bool remPort( int num ) = 0;
    virtual void pumpPorts() = 0;
};

class GcPortHandler : public IGcPortHandler 
{
  protected:
    deque<IGcPort*> portQ;

  public:
    virtual ~GcPortHandler() ;

    // attach a GcInterface::South|Northfaces (ports) to this module/handler to send/receive messages
    // in the message thread
    virtual bool addPort( IGcPort* newport ) ;
    virtual bool remPort( int num ) ;
    virtual void pumpPorts() ;
};

class GcSimplePortHandlerThread : public GcPortHandler
{
protected:
  pthread_mutex_t m_protectCollectionMutex;
  DGCcondition m_condStopped;
  bool gcMessageEndThreadFlag;
  long m_nMessageLoopUsleepTime;
  void initThread() {
    DGCSetConditionFalse(m_condStopped); 
    DGCstartMemberFunctionThread(this, &GcSimplePortHandlerThread::gcPumpThread);
  }
  void init (bool bStart) 
  {
    AutoMutex::initMutex( &m_protectCollectionMutex );
    DGCcreateCondition( &m_condStopped );
    DGCSetConditionTrue( m_condStopped );
    if( bStart )
      initThread();
  }
public:
  GcSimplePortHandlerThread(bool bStart=true, int sleepTime=(10*1000) ) : m_nMessageLoopUsleepTime(sleepTime) {init(bStart);}
  virtual ~GcSimplePortHandlerThread() {
    DGCdeleteMutex( &m_protectCollectionMutex );
    DGCdeleteCondition( &m_condStopped );
  }
  void ReStart() { 
    Stop();
    gcMessageEndThreadFlag = false;
    initThread();
  }
  bool Stop() { 
    gcMessageEndThreadFlag = true; 
    DGCWaitForConditionTrue( m_condStopped );
    return IsStopped();
  } 
  bool IsStopped() {return (bool)m_condStopped;}

  void gcPumpThread()
  {
    while (!gcMessageEndThreadFlag)
    {
      pumpPorts();
      usleep( m_nMessageLoopUsleepTime );
    }
    DGCSetConditionTrue( m_condStopped );
  }

  public:
    bool addPort( IGcPort* newport ) { AutoMutex a(m_protectCollectionMutex); return GcPortHandler::addPort(newport); }
    bool remPort( int num ) { AutoMutex a(m_protectCollectionMutex); return GcPortHandler::remPort(num); }
    void pumpPorts() { AutoMutex a(m_protectCollectionMutex); GcPortHandler::pumpPorts(); }

};

class GcModule : public IGcLogger, public GcSimplePortHandlerThread
{
  protected:
    long m_nControlLoopUsleepTime;
    //long m_nMessageLoopUsleepTime;
    string m_sName;
    GcModuleContainer* m_container;
    friend class GcModuleContainer;
    void setContainer( GcModuleContainer* c ) { m_container = c ;}
  public:
    GcModule( string sName, ControlStatus* cs, MergedDirective* md, 
        long nControlUSleepTime=10000, long nMessageLoopUsleepTime=10000 );
    virtual ~GcModule(void);

    string getName() { return m_sName; }

    // these are used to start and stop both the messaging thread and the main 
    // arbitrate/control thread
    bool Start();
    bool Stop() { gcMessageEndThreadFlag=true; return GcSimplePortHandlerThread::Stop(); }
    int IsStopped() { return (gcMessageEndThreadFlag && gcMainEndThreadFlag); }


    /// main functions of the module
    // each of the following arbitrate and control take two pointer parameters.  
    // Each pointer will be aimed at datastructures that should be created by the sub-class,
    // and which should have been provided to the constructor of this base class.
    // Arbitrate and control are called in the main thread created by this object -- the
    // subclass just needs to implement them -- they are worker-thread stubs.
    //
    //Arbitrate should receive external directives from the controlling module above,
    //process them against this modules current control status and generate a
    //mergedDirective to pass into control()
    virtual void arbitrate(ControlStatus* /*input*/controlStatus, MergedDirective* /*output*/ md) = 0;
    // control should process the mergedDirective from arbitrate, perform the main 
    // algorithms of this module (tactics), generate and send directives to a controlled 
    // module below, receive status from controlled modules and update the control-status
    virtual void control(ControlStatus* /*output?*/controlStatus, MergedDirective* /*input*/mergedDirective) = 0;

  protected:

    /*! This is the module's main thread. */
    int gcMainEndThreadFlag;
    void gcMainThread();

    /*! This thread is supposed to handle all communication. */
    int gcMessageEndThreadFlag;
    void gcMessageThread();

    // these pointers are set in the contructor to aim at the sub-classes instantiations
    // of MergedDirective and ControlStatus subclasses
    MergedDirective* m_mergedDirective;
    ControlStatus* m_controlStatus;

  // logging
  // GcModule provides utilities for logging that conform to the 
  // Unit Testing standards
  public:
    int setLogLevel( int nLevel ) { int n = nLevel; m_nLogLevel = nLevel; return n; }
    void log( int nLevel, const char* format, ... ) {
      va_list argList;
      va_start(argList, format);
      log(nLevel) <<  stringutil::vformat(format, argList);
      va_end(argList);
    }
    ostream& log(int nLevel) {
      if( nLevel <= m_nLogLevel )
        return *m_plogStream;
      else
      {
        m_nullStream.clear();
        return m_nullStream;
      }
    }
  protected:
    std::ostream* m_plogStream;
    std::stringstream m_nullStream;
    pthread_mutex_t   m_logMutex;
    bool openLogFile( ) { 
      m_logbuf.setModule( this );
      m_plogStream = new ostream( &m_logbuf );
      m_plogStream->rdbuf(&m_logbuf);
      return true; 
    }
    void log( string s )
    {
      std::cout << m_sName << ": " << s;
    }
    int m_nLogLevel;

    class logstringbuf : public std::stringbuf 
    {
      public:
      logstringbuf(GcModule* mod) : stringbuf(ios_base::out), module(mod)
      {}
      void setModule( GcModule* mod ) { module=mod; }

      protected:
      GcModule* module;
      virtual int sync( ) 
      {
        AutoMutex( module->m_logMutex );
        int ret = stringbuf::sync();
        module->log(str());
        str("");
        return ret;
      }

    };
    friend class logstringbuf;
    logstringbuf  m_logbuf;

};



/// GcPortT template implementations
//

template <class Message>
GcPortT<Message>::GcPortT(int sn_key, sn_msg sn_msgtype, modulename sn_modname,  IGcLogger* logger, bool bStale, int nTimeToHold  )
  :
  m_bStale(bStale),
  maxTimeToHold(nTimeToHold), //hold messages for no more than 2 seconds
  m_msgtype(sn_msgtype),
  m_logger(logger)
{
  if( sn_msgtype == GCM_IN_PROCESS_DIRECTIVE )
    m_pMailman = new GcInProcMailman<Message>("");
  else
    m_pMailman = new GcSkynetMailman<Message>(sn_key, sn_msgtype, sn_modname);
  DGCcreateMutex(&m_qMutex);
  sendFlag = 0;
}

  template <class Message>
GcPortT<Message>::~GcPortT()
{
  DGCdeleteMutex(&m_qMutex);
}

//gets the newest message sent or received
  template <class Message>
Message* GcPortT<Message>::getNewestMessage() //gets a pointer to the newest message
{
  DGClockMutex(&m_qMutex);
  if( !msgQ.empty() )
  {
    Message* retMessage = msgQ.front().msg;
    msgQ.pop_front();
    #warning " not well implemented - should allocate and return "
    DGCunlockMutex(&m_qMutex);
    return retMessage;
  }
  DGCunlockMutex(&m_qMutex);
  return NULL;
}

//gets the time that the last message was sent out
  template <class Message>
time_t GcPortT<Message>::lastMsgTimeSent()
{
  for (int ii=0;ii<(int)msgQ.size();ii++)
  {
    if (msgQ[ii].status == SENT)
    {
      return msgQ[ii].timeSentRcvd;
    }
  }

  return (time_t) 0;
}


  template <class Message>
bool GcPortT<Message>::recvMsgs() //sends all of the recently added messages
{
#warning " must deallocate off of the queue "

  bool bNew = m_pMailman->hasNewMessage();
  if( !bNew )
    return false;
  for( ; bNew; bNew = m_pMailman->hasNewMessage() )
  {
    Message * newMsg = new Message;
    bNew = m_pMailman->receive(newMsg);
    if (bNew && NULL != newMsg)
    {
      MessageWrapper mwrap;
      mwrap.timeSentRcvd = m_bStale?time(NULL):-1; //the time we got the message
      mwrap.status = RECEIVED;

      mwrap.msg = newMsg;
      mwrap.msgSize = 0;
      DGCcreateMutex(&(mwrap.m_dataBufferMutex));
      if(m_logger) m_logger->log(8) << " -- statusresponse recvd :  " << *newMsg << endl;

      msgQ.push_back(mwrap); //I think this copies all the data
      {
        cout << "received message.  Q size=" << msgQ.size() << endl;
      }
    }
  }

  return true;
}

  template <class Message>
bool GcPortT<Message>::sendMsgs() //sends all of the new messages
{    
  for(int ii=0;ii<(int)msgQ.size();ii++)
  {
    //std::cout << "s"; //TEMPORARY
    if (READYTOSEND == msgQ[ii].status)
    {
      //std::cout << "send?"; //TEMPORARY
      if (sendMessage(msgQ[ii].msg,msgQ[ii].msgSize,&(msgQ[ii].m_dataBufferMutex)))
      {
        //std::cout << "message successfully sent \n"; //TEMPORARY
        msgQ[ii].timeSentRcvd = m_bStale?time(NULL):-1;
        msgQ[ii].status = SENT;
        if(m_logger) m_logger->log(8) << " -- directive sent:  " << *msgQ[ii].msg << endl;
      }
      else
      {
        std::cerr << "error in GcPort::sendMsgs \n";
        msgQ[ii].status = SENDERROR;
      }
    }
  }

  return true;
}

  template <class Message>
bool GcPortT<Message>::handleMsgs() //clears all of the messages that are ready to die
{
  MessageWrapper  mwrap;

  int ii;
  time_t timenow = time(NULL);


  //clear based on time
  for (ii=0;ii<(int)msgQ.size();ii++)
  {
    double dTime =fabs(difftime(timenow,msgQ[ii].timeSentRcvd));
    if (dTime > maxTimeToHold 
        && (msgQ[ii].timeSentRcvd != (time_t) -1))
    {
      //std::cout << "here I am, ready to kill " << maxTimeToHold 
      //	  << " " << difftime(timenow,msgQ[ii].timeSentRcvd) 
      //	  << " sent? " << msgQ[ii].status << "\n"; //TEMPORARY
      msgQ[ii].status = READYTOKILL;
    }
  }

  DGClockMutex(&m_qMutex);

  ii=0;
  while (ii<(int)msgQ.size())
  {
    if (READYTOKILL == msgQ[ii].status)
    {
      //std::cout << "here I am, ready to kill " << maxTimeToHold 
      //	  << " " << difftime(timenow,msgQ[ii].timeSentRcvd) 
      //	  << " sent? " << msgQ[ii].status << "\n";
      mwrap = msgQ[ii];
      msgQ.erase(msgQ.begin()+ii);
      DGCdeleteMutex(&(mwrap.m_dataBufferMutex));
      delete (mwrap.msg);
    }
    else
    {
      ii++;
    }
  }

  DGCunlockMutex(&m_qMutex);

  return 0;
}

template <class Message>
bool GcPortT<Message>::sendMessage(Message * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex)
{
  return m_pMailman->send( data );
}




#endif // _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__
