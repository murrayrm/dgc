#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"

//TEMPORARY: void *hola(void *arg);

MergedDirective GcModule::Arbitrate(ControlStatus controlStatus)
{
  MergedDirective mergedDirective;

  if (arbDirectivesQueue.empty())
  {
    mergedDirective.directive = MergedDirective::DEFAULT;
  }

  return mergedDirective;
}

ControlStatus GcModule::Control(MergedDirective mergedDirective)
{
  ControlStatus controlStatus;

  if (mergedDirective.directive == MergedDirective::DEFAULT)
  {
    //std::cout << "the default directive\n"; //TEMPORARY
  }

  return controlStatus;
}

GcModule::GcModule(void)
{
  //TEMPORARY
  //controlStatus.status = 2;
  
  //start the threads
  gcMainEndThreadFlag = 0;
  DGCstartMemberFunctionThread(this, &GcModule::gcMainThread);

  gcMessageEndThreadFlag = 0;
  DGCstartMemberFunctionThread(this, &GcModule::gcMessageThread);

};

void GcModule::gcMainThread()
{
  //int temp = 0;

  ControlStatus controlStatus;
  MergedDirective mergedDirective;

  while (!gcMainEndThreadFlag)
  {
    mergedDirective = Arbitrate(controlStatus);

    controlStatus = Control(mergedDirective);

    //sleep(1);
    //std::cout << "da main thread: " << temp << "\n"; temp++;
    
  }
}

void GcModule::gcMessageThread()
{
  //int temp = 0;

  while (!gcMessageEndThreadFlag)
  {
    //sleep(3);
    //std::cout << "da message thread: " << temp << "\n"; temp++;
  }
}

//TEMPORARY
/*void *hola(void * arg)
{
  int myid = *(int *) arg;

  //printf("Hello, world, I'm %d\n",myid);
  std::cout << "this thread started\n";
  sleep(1);
  std::cout << "the thread spit out something else\n";

  return arg;
}*/

int main(void) //main(int argc, char **argv) 
{
  //GcModule gcmodule;

  //pthread_t thisthread;
  //int temp = 1;

  //char * doodles = "Hollow World";

  //std::cout << doodles << "\n";

  //pthread_create(&thisthread, NULL, hola, &temp);
  
  //std::cout << "sleep1\n";
  //sleep(1);
  //std::cout << "sleep2\n";
  //sleep(1);
  //std::cout << "sleep3\n";
  //sleep(1);

  int runtime = 0;

  GcModule gcmodule;
  std::cout << "ready to kill: " << gcmodule.readyToKill() << "\n";

  while (!gcmodule.readyToKill())
  {
    if (runtime > 8) 
    {
      gcmodule.kill();
    }
    runtime++;
    sleep(1);
  }
}
