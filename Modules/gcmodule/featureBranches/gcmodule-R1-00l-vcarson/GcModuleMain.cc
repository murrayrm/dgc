#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"
#include <assert.h>
#include "GcModuleTestDirective.hh"
#include "GcInterface.hh"
#include "GcModuleContainer.hh"




#define CP1REPTIME 5
class GcModuleA : public GcModule
{
  public:
    GcInterfaceTest * testInterface;
    GcInterfaceTest::Southface* southface;
    GcInterfaceTest* verifyTestInt;
    GcInterfaceTest::Northface* verifyTest;

    GcIntTestInProc* testInProcInterface;
    GcIntTestInProc* verifyInProcInterface;
    GcIntTestInProc::Northface* verifyInProcInterfaceNorth;
    GcIntTestInProc::Southface* testInProcInterfaceSouth;


    class ControlStatusA : public ControlStatus {};
    class MergedDirectiveA : public MergedDirective {
      public:
        enum Status { NONE, STOP, RUN };
        Status directive;
    };
    ControlStatusA m_ctrlStatusA;
    MergedDirectiveA m_mgddA;

    GcModuleA( int sn_key ) : GcModule( "GcModuleTestSend", &m_ctrlStatusA, &m_mgddA )
  {
    southface = NULL;
    testInterface = new GcInterfaceTest( sn_key, this );
    if( testInterface )
      southface = testInterface->getSouthface();

    testInProcInterface = new GcIntTestInProc( 0, this ) ;
    testInProcInterfaceSouth = testInProcInterface->getSouthface();

    verifyInProcInterface = new GcIntTestInProc( 0, this );
    verifyInProcInterfaceNorth = verifyInProcInterface->getNorthface();
  }

    void arbitrate(ControlStatus* cs, MergedDirective* md)
    {
      MergedDirectiveA& mergedDirective = *(MergedDirectiveA*)md;

      log(1) << "mergedDir set to run\n" << std::endl;
      mergedDirective.directive = MergedDirectiveA::RUN;
    }

    void control(ControlStatus* cs, MergedDirective* md)
    {
      assert( cs );
      assert( md );
      //ControlStatusA& controlStatus = *dynamic_cast<ControlStatusA*>(cs);
      MergedDirectiveA& mergedDirective = *(MergedDirectiveA*)md;

      if (mergedDirective.directive == MergedDirectiveA::RUN)
      {
        //just some junk
        directive1 d1;
        d1.id = time(NULL);

        deque<unsigned> noAck = southface->orderedDirectivesWaitingForResponse();
        log(1) << "dirctives that have not been responeded too: ";
        copy( noAck.begin(), noAck.end(), ostream_iterator<unsigned>( log(1), ", " ) );
        log(1) << endl;


        //periodically sends new messages
        {
          southface->sendDirective(&d1);
          testInProcInterfaceSouth->sendDirective(&d1); // validate delivery, with a loopback
          log(1) << "directive added: " << d1.data << ":" << d1.getDirectiveId() << "\n";
        }

        log(1) << "validating directives....: " << endl;
        while( verifyInProcInterfaceNorth->haveNewDirective() )
        {
          directive1 v;
          if( !verifyInProcInterfaceNorth->getNewDirective( &v ) ) {
            log(1) << "ASSERT ERROR in GcModule: could not get directive when haveNewDirective indicated available" << endl;
            exit(0);
          }

          directive1 s = testInProcInterfaceSouth->getSentDirective( v.getDirectiveId() );

          if( !(s==v) ) {
            log(1) << "ASSERT ERROR in GcModuleMain:"
              << "received and sent directictives differ" << endl;
            log(1) << "sent directive " << s.getDirectiveId() << endl << s << endl;
            log(1) << "received directive " << v.getDirectiveId() << endl << v << endl;
            exit(0);
          }

          log(1) << "validated directive " << v.getDirectiveId() << endl;
        }


        if( southface->haveNewStatus() )
        {
          log(2) << "have new status" << endl;

          status1* stat = southface->getStatusOf( d1.id );
          if( stat )
          {
            log(1) << "received status on " << stat->getDirectiveId() ;
            log(1) << " data: " << stat->blah << endl;
          }

          status1* latest = southface->getLatestStatus();
          if( latest )
          {
            log(1) << " latest status was for id: " << latest->getDirectiveId() << endl;
          }
          else
            log(1) << " there was no latest status " << endl;
        }
        else
          log(2) << "no new status available" << endl;

        sleep(2); //TEMPORARY

      }
      return;
    }
};



int main(void) //main(int argc, char **argv)
{
  GcModuleContainer modules;
  GcModuleA moduleA( 321 );
  modules.addModule( &moduleA );
  modules.setLogLevel(5);

  //log(1) << "starting TestModule (northern/control'er)\n" << endl;

  moduleA.Start();

  GcPortHandler handler;
  GcIntTestInProc* testInProcInterface2;
  GcIntTestInProc::Southface* testInProcInterfaceSouth2;
  testInProcInterface2 = new GcIntTestInProc( 0, &handler, NULL ) ;
  testInProcInterfaceSouth2 = testInProcInterface2->getSouthface();
  directive1 d1;
  d1.id = time(NULL);

  while (!moduleA.IsStopped())
  {
    d1.id = time(NULL);
    testInProcInterfaceSouth2->sendDirective(&d1);
    handler.pumpPorts();
    usleep(110);
  }

}
