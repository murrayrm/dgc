#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"

class directive1
{
public:
  directive1() { memcpy(data,"do your job",12); }
  char data[12];
};

#define CP1REPTIME 5
class GcModuleB : public GcModule
{
public:
  GcPort * arbPort1;
  int printFlag;

  GcModuleB(void)
  {
    int sn_key = 321;
    sn_msg sn_msgtype = SNrddf;
    modulename sn_modtype = MODrddftalkertestrecv;
    
    GcPort * port1 = new GcPort(sn_key,sn_msgtype,sn_modtype);
    port1->sendFlag = 0; //TEMPORARY: for now, this is only a "receive" module

    addPort(port1);
    arbPort1 = getPortofType(SNrddf);

    printFlag = 0;
  }

  virtual MergedDirective Arbitrate(ControlStatus controlStatus)
  {
    MergedDirective mergedDirective;
    directive1 * d1;
    int d1Size;

    if (((d1 = (directive1 *) arbPort1->getNewestMessage(&d1Size)) != NULL) && printFlag)
    {
      std::cout << "got a directive: " << d1->data << "\n";
      printFlag = 0;
    }
    if (arbPort1->getNumMsgs() == 0)
    {
      printFlag = 1;
    }
  
  }
};


int main(void) //main(int argc, char **argv)
{
  GcModuleB moduleB;

  moduleB.Start();

  while (!moduleB.IsStopped())
  {
    sleep(1);
  }

}
