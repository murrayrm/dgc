#ifndef __GCPORTT_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__
#define __GCPORTT_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__

/// GcPortT template implementations
//
//

  template <class Message>
GcPortT<Message>::GcPortT(int sn_key, sn_msg sn_msgtype, modulename sn_modname,  IGcLogger* logger, bool bStale, int nTimeToHold  )
  :
    m_bStale(bStale),
    maxTimeToHold(nTimeToHold), //hold messages for no more than 2 seconds
    m_msgtype(sn_msgtype),
    m_logger(logger)
{
  if( sn_msgtype == GCM_IN_PROCESS_DIRECTIVE )
    m_pMailman = new GcInProcMailman<Message>("", sn_modname);
  else
    m_pMailman = new GcSkynetMailman<Message>(sn_key, sn_msgtype, sn_modname);
  AutoMutex::initMutex(&m_qMutex);
  sendFlag = 0;
}

  template <class Message>
GcPortT<Message>::~GcPortT()
{
  delete m_pMailman; m_pMailman = NULL;
  while( Message* m=getNewestMessage() )
    delete m;
  DGCdeleteMutex(&m_qMutex);
}

//gets the newest message sent or received
  template <class Message>
Message* GcPortT<Message>::getNewestMessage() //gets a pointer to the newest message
{
  DGClockMutex(&m_qMutex);
  if( !msgQ.empty() )
  {
    Message* retMessage = msgQ.front().msg;
    msgQ.pop_front();
    //#warning " not well implemented - should allocate and return "
    DGCunlockMutex(&m_qMutex);
    return retMessage;
  }
  DGCunlockMutex(&m_qMutex);
  return NULL;
}

//gets the time that the last message was sent out
  template <class Message>
time_t GcPortT<Message>::lastMsgTimeSent()
{
  DGClockMutex(&m_qMutex);
  for (int ii=0;ii<(int)msgQ.size();ii++)
  {
    if (msgQ[ii].status == SENT)
    {
      return msgQ[ii].timeSentRcvd;
    }
  }
  DGCunlockMutex(&m_qMutex);

  return (time_t) 0;
}


  template <class Message>
bool GcPortT<Message>::recvMsgs() //sends all of the recently added messages
{
  //#warning " must deallocate off of the queue "

  bool bNew = m_pMailman->hasNewMessage();
  if( !bNew )
    return false;
  DGClockMutex(&m_qMutex);
  for( ; bNew; bNew = m_pMailman->hasNewMessage() )
  {
    Message * newMsg = new Message;
    bNew = m_pMailman->receive(newMsg);
    if (bNew && NULL != newMsg)
    {
      MessageWrapper mwrap;
      mwrap.timeSentRcvd = m_bStale?time(NULL):-1; //the time we got the message
      mwrap.status = RECEIVED;

      mwrap.msg = newMsg;
      mwrap.msgSize = 0;
      DGCcreateMutex(&(mwrap.m_dataBufferMutex));
      if(m_logger) m_logger->gclog(8) << " -- message recvd(" << this << "):  " << *newMsg << endl;

      msgQ.push_back(mwrap); //I think this copies all the data
    }
    else
      delete newMsg;
  }
  DGCunlockMutex(&m_qMutex);

  return true;
}

  template <class Message>
bool GcPortT<Message>::sendMsgs() //sends all of the new messages
{    
  DGClockMutex(&m_qMutex);
  for(int ii=0;ii<(int)msgQ.size();ii++)
  {
    if (READYTOSEND == msgQ[ii].status)
    {
      /// Check for null pointer
      if( !msgQ[ii].msg )
      {
        msgQ[ii].status = READYTOKILL;
        if(m_logger) m_logger->gclog(8) << "GcPort:: null message found in GcPort Queue" << endl;
      }
      else if (sendMessage(msgQ[ii].msg,msgQ[ii].msgSize,&(msgQ[ii].m_dataBufferMutex)))
      {
        msgQ[ii].timeSentRcvd = m_bStale?time(NULL):-1;
        msgQ[ii].status = READYTOKILL;
        if(m_logger) m_logger->gclog(8) << " -- message sent(" << this << "):  " << *msgQ[ii].msg << endl;
      }
      else
      {
        if(m_logger) m_logger->gclog(1) << "error in GcPort::sendMsgs \n";
        msgQ[ii].status = SENDERROR;
      }
    }
  }
  DGCunlockMutex(&m_qMutex);

  return true;
}

  template <class Message>
bool GcPortT<Message>::handleMsgs() //clears all of the messages that are ready to die
{
  MessageWrapper  mwrap;

  int ii;
  time_t timenow = time(NULL);

  DGClockMutex(&m_qMutex);

  //clear based on time
  for (ii=0;ii<(int)msgQ.size();ii++)
  {
    double dTime =fabs(difftime(timenow,msgQ[ii].timeSentRcvd));
    if (dTime > maxTimeToHold 
        && (msgQ[ii].timeSentRcvd != (time_t) -1))
    {
      msgQ[ii].status = READYTOKILL;
    }
  }


  ii=0;
  while (ii<(int)msgQ.size())
  {
    if (READYTOKILL == msgQ[ii].status)
    {
      mwrap = msgQ[ii];
      msgQ.erase(msgQ.begin()+ii);
      DGCdeleteMutex(&(mwrap.m_dataBufferMutex));
      delete (mwrap.msg);
    }
    else
    {
      ii++;
    }
  }


  DGCunlockMutex(&m_qMutex);

  if( m_logger )
  {
    int count= msgQ.size();
    if(m_prevCount!=count) m_logger->gclog(8) << "GcPort::handle(" << this << "): que size: " << msgQ.size() << " -- " << count << ":" << m_prevCount << endl;
    m_prevCount = count;
  }
  
  return 0;
}

  template <class Message>
bool GcPortT<Message>::sendMessage(Message * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex)
{
  return m_pMailman->send( data );
}



  template <class Message>
bool GcPortT<Message>::addMsg(Message * msg, bool bAsynchronous) 
{ 

  bool success = DGClockMutex(&m_qMutex);

  MessageWrapper mwrap;
  mwrap.timeSentRcvd = (time_t) -1;
  mwrap.status = READYTOSEND;

  // create a copy -- copy constructor must be implemented
  mwrap.msg = new Message( *msg );
  mwrap.msgSize = -1;

  success = DGCcreateMutex(&(mwrap.m_dataBufferMutex));

  msgQ.push_back(mwrap); //I think this copies all the data

  success = DGCunlockMutex(&m_qMutex);
  
  // empty queue now if we want things done now
  if( !bAsynchronous )
    sendMsgs();


  return true;
}

  template <class Message>
void GcPortT<Message>::changeMsgStatus( IGcMessageFilter& filter, int toStat, bool bOnlyFirst )
{
  DGClockMutex(&m_qMutex);
  for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
    if( filter.filter((*i).msg) ) {
      (*i).status = (msgStatus)toStat;
      if(bOnlyFirst) break;
    }
  DGCunlockMutex(&m_qMutex);
}

  template <class Message>
void GcPortT<Message>::moveMsgsStatus( msgStatus fromStat, msgStatus toStat )
{
  DGClockMutex(&m_qMutex);
  for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
    if( (*i).status == fromStat ) (*i).status = toStat;
  DGCunlockMutex(&m_qMutex);
}


  template <class Message>
void GcPortT<Message>::flushMsgs( )
{
  DGClockMutex(&m_qMutex);
  for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
    (*i).status = READYTOKILL;
  DGCunlockMutex(&m_qMutex);
  handleMsgs();
}



#endif // __GCPORTT_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__








