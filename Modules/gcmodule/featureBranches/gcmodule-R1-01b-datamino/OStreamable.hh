#ifndef _OSTREAMABLE_HH_SDJLKFJ08934VT0324UT304UTY9T49TYHTGYNHGVNSEIH__
#define _OSTREAMABLE_HH_SDJLKFJ08934VT0324UT304UTY9T49TYHTGYNHGVNSEIH__

#include <ostream>
using std::ostream;

class OStreamable {
public:
  virtual ~OStreamable() {}
  friend ostream& operator<< (ostream& out, const OStreamable& me);
  virtual std::string toString() const = 0;
};

#endif // _OSTREAMABLE_HH_SDJLKFJ08934VT0324UT304UTY9T49TYHTGYNHGVNSEIH__


