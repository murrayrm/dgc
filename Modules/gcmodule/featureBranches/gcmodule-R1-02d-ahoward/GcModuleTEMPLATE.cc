#include "GcModuleTestDirective.hh"
BOOST_CLASS_EXPORT_GUID( test123, "test123_derived" )

void templatecodeInstantiation() {
 GcInterfaceTest::Northface* n1 = GcInterfaceTest::generateNorthface( -1, (GcModule*)NULL );
 GcInterfaceTest::releaseNorthface(n1);

 GcIntTestInProc::Northface* n2 = GcIntTestInProc::generateNorthface( -1, (GcModule*)NULL );
 GcIntTestInProc::releaseNorthface(n2);
}

