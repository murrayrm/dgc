#include "GcModuleLogger.hh"

int GcModuleLogger::logstringbuf::sync() 
{
  module->logStringUnprotected(str());
  str("");
  int ret = stringbuf::sync();
  return ret;
}

bool GcModuleLogger::openLogFile( ) 
{ 
  m_logbuf.setModule( this );
  m_plogStream = new std::ostream( &m_logbuf );
  return true; 
}


protectedostream GcModuleLogger::gclogProtected(int nLevel) 
{
  m_nMessageLevel = nLevel;
  if( isLevel(nLevel) )
    //return *m_plogStream;
    return protectedostream(&m_logMutex, m_plogStream);
  else
  {
    m_nullStream.rdbuf()->str("");
    m_nullStream.flush();
    return protectedostream(&m_logMutex, &m_nullStream);
  }
}

void GcModuleLogger::logStringUnprotected( string s )
{
  int n = m_nMessageLevel;
  for( Loggers::iterator i=peers.begin(); i!=peers.end(); i++ )
    if( (*i) && (*i)->isLevel(n) ) (*i)->logStringUnprotected(s);

  LogStreams::iterator i; 
  for( i=m_logstreams.begin(); i!=m_logstreams.end(); i++ )
  {
    *(*i) << m_sName << ": " << s;
    (*i)->flush();
  }
}

void GcModuleLogger::addLogOstream( ostream& o )
{
  m_logstreams.push_back( &o );
}

void GcModuleLogger::addLogfile( string filename, bool bAppendTimestamp )
{
  if( bAppendTimestamp )
  {
    char timestamp[128];
    time_t t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
    filename += "." + string(timestamp) + ".log";
    string suf = "";
    for(int i=1; std::ifstream( (filename+suf).c_str()); i++ ) {
      std::ostringstream ss;
      ss << "." << i;
      suf = ss.str();
    }
    filename += suf;
  }
  ofstream* file = new ofstream( filename.c_str(), ios::out );
  m_ownedlogstreams.push_back( file );
  addLogOstream( *file );
}

GcModuleLogger::~GcModuleLogger() 
{
  for( LogStreams::iterator i=m_ownedlogstreams.begin(); i!=m_ownedlogstreams.end(); i++ )
  {
    delete *i;
  }
  delete m_plogStream; m_plogStream = NULL;
}


GcModuleLogger::GcModuleLogger( string sName ) : m_logbuf(), m_sName(sName)
{
  AutoMutex::initMutex( &m_logMutex );
  m_nLogLevel = 1;
  m_nMessageLevel = 1;
  openLogFile( );
}

void GcModuleLogger::attachPeerLogger( IGcLogger* logger ) 
{
  if( logger )
    peers.push_back( logger );
}

