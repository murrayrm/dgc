#ifndef _GCPORTHANDLER_HH_029TUNC2UT09N24UGN839HG489__
#define _GCPORTHANDLER_HH_029TUNC2UT09N24UGN839HG489__

#include <deque>
#include "dgcutils/DGCutils.hh"
#include "gcmodule/AutoMutex.hh"

using std::deque;


// forward declare the port..
class IGcPort;

class IGcPortHandler
{
  public:
    virtual ~IGcPortHandler() {}

    virtual bool addPort( IGcPort* newport ) = 0;
    virtual bool remPort( int num ) = 0;
    virtual void pumpPorts() = 0;
};

class GcPortHandler : public IGcPortHandler 
{
  protected:
    deque<IGcPort*> portQ;

  public:
    virtual ~GcPortHandler() ;

    // attach a GcInterface::South|Northfaces (ports) to this module/handler to send/receive messages
    // in the message thread
    virtual bool addPort( IGcPort* newport ) ;
    virtual bool remPort( int num ) ;
    virtual void pumpPorts() ;
};

class GcSimplePortHandlerThread : public GcPortHandler
{
protected:
  pthread_mutex_t m_protectCollectionMutex;
  DGCcondition m_condStopped;
  bool gcMessageEndThreadFlag;
  long m_nMessageLoopUsleepTime;
  void initThread() {
    DGCSetConditionFalse(m_condStopped); 
    DGCstartMemberFunctionThread(this, &GcSimplePortHandlerThread::gcPumpThread);
  }
  void init (bool bStart) 
  {
    AutoMutex::initMutex( &m_protectCollectionMutex );
    DGCcreateCondition( &m_condStopped );
    DGCSetConditionTrue( m_condStopped );
    if( bStart )
      initThread();
  }
public:
  GcSimplePortHandlerThread(bool bStart=true, int sleepTime=(10*1000) ) : m_nMessageLoopUsleepTime(sleepTime) {init(bStart);}
  virtual ~GcSimplePortHandlerThread() {
    DGCdeleteMutex( &m_protectCollectionMutex );
    DGCdeleteCondition( &m_condStopped );
  }
  void ReStart() { 
    Stop();
    gcMessageEndThreadFlag = false;
    initThread();
  }
  bool Stop() { 
    gcMessageEndThreadFlag = true; 
    DGCWaitForConditionTrue( m_condStopped );
    return IsStopped();
  } 
  bool IsStopped() {return (bool)m_condStopped;}

  void gcPumpThread()
  {
    while (!gcMessageEndThreadFlag)
    {
      pumpPorts();
      usleep( m_nMessageLoopUsleepTime );
    }
    DGCSetConditionTrue( m_condStopped );
  }

  public:
    bool addPort( IGcPort* newport ) { AutoMutex a(m_protectCollectionMutex); return GcPortHandler::addPort(newport); }
    bool remPort( int num ) { AutoMutex a(m_protectCollectionMutex); return GcPortHandler::remPort(num); }
    void pumpPorts() { AutoMutex a(m_protectCollectionMutex); GcPortHandler::pumpPorts(); }
};

#endif // _GCPORTHANDLER_HH_029TUNC2UT09N24UGN839HG489__
