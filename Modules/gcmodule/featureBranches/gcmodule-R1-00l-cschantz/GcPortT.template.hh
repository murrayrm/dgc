#ifndef __GCPORTT_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__
#define __GCPORTT_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__

/// GcPortT template implementations
//
//

template <class Message>
GcPortT<Message>::GcPortT(int sn_key, sn_msg sn_msgtype, modulename sn_modname,  IGcLogger* logger, bool bStale, int nTimeToHold  )
  :
  m_bStale(bStale),
  maxTimeToHold(nTimeToHold), //hold messages for no more than 2 seconds
  m_msgtype(sn_msgtype),
  m_logger(logger)
{
  if( sn_msgtype == GCM_IN_PROCESS_DIRECTIVE )
    m_pMailman = new GcInProcMailman<Message>("", sn_modname);
  else
    m_pMailman = new GcSkynetMailman<Message>(sn_key, sn_msgtype, sn_modname);
  DGCcreateMutex(&m_qMutex);
  sendFlag = 0;
}

  template <class Message>
GcPortT<Message>::~GcPortT()
{
  DGCdeleteMutex(&m_qMutex);
}

//gets the newest message sent or received
  template <class Message>
Message* GcPortT<Message>::getNewestMessage() //gets a pointer to the newest message
{
  DGClockMutex(&m_qMutex);
  if( !msgQ.empty() )
  {
    Message* retMessage = msgQ.front().msg;
    msgQ.pop_front();
    #warning " not well implemented - should allocate and return "
    DGCunlockMutex(&m_qMutex);
    return retMessage;
  }
  DGCunlockMutex(&m_qMutex);
  return NULL;
}

//gets the time that the last message was sent out
  template <class Message>
time_t GcPortT<Message>::lastMsgTimeSent()
{
  for (int ii=0;ii<(int)msgQ.size();ii++)
  {
    if (msgQ[ii].status == SENT)
    {
      return msgQ[ii].timeSentRcvd;
    }
  }

  return (time_t) 0;
}


  template <class Message>
bool GcPortT<Message>::recvMsgs() //sends all of the recently added messages
{
#warning " must deallocate off of the queue "

  bool bNew = m_pMailman->hasNewMessage();
  if( !bNew )
    return false;
  for( ; bNew; bNew = m_pMailman->hasNewMessage() )
  {
    Message * newMsg = new Message;
    bNew = m_pMailman->receive(newMsg);
    if (bNew && NULL != newMsg)
    {
      MessageWrapper mwrap;
      mwrap.timeSentRcvd = m_bStale?time(NULL):-1; //the time we got the message
      mwrap.status = RECEIVED;

      mwrap.msg = newMsg;
      mwrap.msgSize = 0;
      DGCcreateMutex(&(mwrap.m_dataBufferMutex));
      if(m_logger) m_logger->log(8) << " -- statusresponse recvd :  " << *newMsg << endl;

      msgQ.push_back(mwrap); //I think this copies all the data
    }
  }

  return true;
}

  template <class Message>
bool GcPortT<Message>::sendMsgs() //sends all of the new messages
{    
  for(int ii=0;ii<(int)msgQ.size();ii++)
  {
    if (READYTOSEND == msgQ[ii].status)
    {
      if (sendMessage(msgQ[ii].msg,msgQ[ii].msgSize,&(msgQ[ii].m_dataBufferMutex)))
      {
        msgQ[ii].timeSentRcvd = m_bStale?time(NULL):-1;
        msgQ[ii].status = SENT;
        if(m_logger) m_logger->log(8) << " -- directive sent:  " << *msgQ[ii].msg << endl;
      }
      else
      {
        std::cerr << "error in GcPort::sendMsgs \n";
        msgQ[ii].status = SENDERROR;
      }
    }
  }

  return true;
}

  template <class Message>
bool GcPortT<Message>::handleMsgs() //clears all of the messages that are ready to die
{
  MessageWrapper  mwrap;

  int ii;
  time_t timenow = time(NULL);


  //clear based on time
  for (ii=0;ii<(int)msgQ.size();ii++)
  {
    double dTime =fabs(difftime(timenow,msgQ[ii].timeSentRcvd));
    if (dTime > maxTimeToHold 
        && (msgQ[ii].timeSentRcvd != (time_t) -1))
    {
      //std::cout << "here I am, ready to kill " << maxTimeToHold 
      //	  << " " << difftime(timenow,msgQ[ii].timeSentRcvd) 
      //	  << " sent? " << msgQ[ii].status << "\n"; //TEMPORARY
      msgQ[ii].status = READYTOKILL;
    }
  }

  DGClockMutex(&m_qMutex);

  ii=0;
  while (ii<(int)msgQ.size())
  {
    if (READYTOKILL == msgQ[ii].status)
    {
      //std::cout << "here I am, ready to kill " << maxTimeToHold 
      //	  << " " << difftime(timenow,msgQ[ii].timeSentRcvd) 
      //	  << " sent? " << msgQ[ii].status << "\n";
      mwrap = msgQ[ii];
      msgQ.erase(msgQ.begin()+ii);
      DGCdeleteMutex(&(mwrap.m_dataBufferMutex));
      delete (mwrap.msg);
    }
    else
    {
      ii++;
    }
  }

  DGCunlockMutex(&m_qMutex);

  return 0;
}

template <class Message>
bool GcPortT<Message>::sendMessage(Message * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex)
{
  return m_pMailman->send( data );
}



  template <class Message>
bool GcPortT<Message>::addMsg(Message * msg, bool bAsynchronous) 
{ 

  bool success = DGClockMutex(&m_qMutex);

  MessageWrapper mwrap;
  mwrap.timeSentRcvd = (time_t) -1;
  mwrap.status = READYTOSEND;

  // create a copy -- copy constructor must be implemented
  mwrap.msg = new Message( *msg );
  mwrap.msgSize = -1;

  success = DGCcreateMutex(&(mwrap.m_dataBufferMutex));
  //std::cout << "successful mutex? " << success << "\n"; //TEMPORARY

  msgQ.push_back(mwrap); //I think this copies all the data

  // empty queue now if we want things done now
  if( !bAsynchronous )
    sendMsgs();

  success = DGCunlockMutex(&m_qMutex);

  return true;
}

  template <class Message>
void GcPortT<Message>::changeMsgStatus( IGcMessageFilter& filter, int toStat, bool bOnlyFirst )
    {
      for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
        if( filter.filter((*i).msg) ) {
          (*i).status = (msgStatus)toStat;
          if(bOnlyFirst) break;
        }
    }

  template <class Message>
void GcPortT<Message>::moveMsgsStatus( msgStatus fromStat, msgStatus toStat )
    {
      for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
        if( (*i).status == fromStat ) (*i).status = toStat;
    }


  template <class Message>
void GcPortT<Message>::flushMsgs( )
    {
      for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
        (*i).status = READYTOKILL;
      handleMsgs();
    }



#endif // __GCPORTT_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__








