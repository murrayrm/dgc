/*!
 *  \file  GcSkynetMailman.template.hh
 *  \brief Implementation of delivery method of In-Process Directives and Responses
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *  GcSkynetMailman.template.hh contains the declarations and template
 *  definitions of the delivery mechanism used when communicating 
 *  directives and responses over the network using skynet.
 *
 *  It is a very thin layer merely wrapping calls to skynettalker.
 *
 */


#ifndef _GCSKYNETMAILMAN_20938JYGC0234YJG50892F4JHYGR98YWR_
#include "skynettalker/SkynetTalker.hh"

template <class MsgT>
class GcSkynetMailman : public IGcMailman<MsgT>
{
  protected:
    SkynetTalker<MsgT> talker;

    DGCcondition* m_receivedMessageCond;

  public:
    GcSkynetMailman( int sn_key, sn_msg sn_msgtype, modulename sn_modname ) : 
      talker(sn_key, sn_msgtype, sn_modname),
      m_receivedMessageCond( NULL )
    {  }

  public:
    bool hasNewMessage() { return talker.hasNewMessage(); }
    bool receive(MsgT* msg) { return talker.receive(msg); }
    bool send(MsgT* msg) { 
      bool bRet = talker.send(msg); 
      if( m_receivedMessageCond ) {
        pthread_cond_broadcast( &m_receivedMessageCond->pCond );
      }
      return bRet;
    }
    bool setMode( typename IGcMailman<MsgT>::OperationMode m ) 
    { 
      if( m==IGcMailman<MsgT>::RECEIVER ) 
        talker.listen(); // initializes skynet
      return true; 
    }
    void installMessageReadyCondition( DGCcondition* cond ) { 
      m_receivedMessageCond = cond;
    }

};

#endif // _GCSKYNETMAILMAN_20938JYGC0234YJG50892F4JHYGR98YWR_

