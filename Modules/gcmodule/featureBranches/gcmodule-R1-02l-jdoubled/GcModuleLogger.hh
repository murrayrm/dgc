/*!
 *  \file  GcModuleLogger.hh
 *  \brief define a logger class for gcmodules and gcinterfaces
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *
 * GcModuleLogger is the base-line logging class implementing the 
 * IGcLogger interface.  It supports log-levels for conditional
 * logging of messages, generalized ostream logging for logging to 
 * files, in-memory streams or custom streams, and should be threadsafe
 * (note this does not mean process safe if attempting to log to a 
 * single file from several processes; perhaps ofsteam or the OS protects
 * this?).  
 *
 * The ostream presented by GcModuleLogger redirects all loggin output
 * to all of the registered ostreams (ofstream for file(s)) via a custom
 * stringbuf class attached to the presented ostream, 
 * which overloads the sync() function.
 *
 * To log, set the logging level (setLogLevel), add a destination ostream 
 * (i.e. addLogfile( filename )) and call
 *    gclog(n) << "your message" << endl;
 * where n is the minimum level you wish the message to appear. 
 *
 * Below the logging threshold gclog will return a nullOStringStream
 * which essentially no-ops.
 *
 * In order to fascilitate the no-op, and provide threading protection,
 * a helper function getOStream() is used, for which gclog is #defined to be 
 * replaced by.  
 * This should be improved, as currently gclog is not part of the interface, 
 * yet is the main API function of the class!
 *
 * Peer logging is currently limited to the lowest common denominator of the 
 * parent logger.  That is to say if a logger with logLevel 9 is attached to 
 * a logger with level 1, the attached logger will still only log level 1.  
 * To achieve the desired effect the base logger should have level 9, and 
 * the attached have level 1.
 */

#ifndef _GCMODULE_LOGGER_HH__F89JN4T2FJMH4G8902Y3N5G4C2F908Y34G784Y5GJHSEF__
#define _GCMODULE_LOGGER_HH__F89JN4T2FJMH4G8902Y3N5G4C2F908Y34G784Y5GJHSEF__

#include "gcmodule/IGcLogger.hh"
#include "StringUtil.hh"
#include <stdarg.h>
#include <fstream>
#include <ostream>
#include <vector>
#include <list>
#include <set>
#include <sstream>
using std::ofstream;
using std::ostream;
using std::vector;
using std::list;
using std::string;
using std::set;


/// This class works as a no-op / sink for messages that we do not want 
/// streamed into anything useful (for loglevels higher than the runtime level)
class nullOStringStream : public std::ostringstream
{
  public:
    // create a no-op for our nullOStringstream
    template<typename T>
      nullOStringStream& operator<<( const T& t ) { return *this; }
};



/// This class embodies the logging fascility of GcModule.  It can
/// also be instantiated stand-alone for "lightweight" apps that
/// utilize GcInterfaces without an actual GcModule instance
class GcModuleLogger : public IGcLogger
{
  // logging
  // GcModule provides utilities for logging that conform to the 
  // Unit Testing standards
  public:
    /// gclog(n) is available via a macro until a better way of protecting
    /// against multithreaded writes is developed
    //virtual ostream& gclog(int nLevel) = 0;

    /// setLogLevel()
    ///
    /// sets the threshold level at which logging is enabled
    int setLogLevel( int nLevel ) { 
      int n = nLevel;
      m_nLogLevel = nLevel;
      return n;
    }

    /// getLevel()
    ///
    int getLevel() {
      return m_nLogLevel;
    }

    /// isLevel()
    //
    bool isLevel( int nLevel ) {
      return nLevel <= m_nLogLevel;
    }

    /// gclogProtected()
    //
    void gclogProtected( int nLevel, const char* format, ... ) {
      va_list argList;
      va_start(argList, format);
      gclogProtected(nLevel).getOstream() <<  stringutil::vformat(format, argList);
      va_end(argList);
    }

    /// gclogProtected()
    //
    protectedostream gclogProtected(int nLevel) ;
    #if 0
    protectedostream gclog(int nLevel) {
      return gclogProtected(nLevel);
    }
    #endif
    

    /// addLogOstream()
    //
    void addLogOstream( ostream& o );

    /// addLogfile()
    ///
    /// will open and manage a file ostream given a filename.  If bAppend
    /// timestamp is true, filename will be appended with an ascii date/time,
    /// .log, and append .NNN starting at 1 if a duplicate file is already present
    void addLogfile( string filename, bool bAppendTimestamp=true );

    /// attachPeerLogger()
    //
    void attachPeerLogger( IGcLogger* logger ) ;

    /// logStringUnprotected()
    //
    void logStringUnprotected( const std::string& s );
    void logStringUnprotectedHelp( const std::string& s, bool bAddName );
    void logStringUnprotectedThread( ) ;
    static bool bThreadedLogging;
    enum { bThreadedLoggingDefault = 0 };
    void setThreadedLogging( bool bUseThreadedLogging ) {
      bThreadedLogging = bUseThreadedLogging;
    }

    /// This returns a timestamp in units of usec, however the true 
    /// time will be truncated to fit within 32 bits!  Do not use this
    /// for calculating time differences, etc.  Just for log reference
    uint64_t usecTime();
    uint64_t usecTick();
  protected:
    /// pointer to the ostream present from gclogProtected()
    std::ostream* m_plogStream;
    /// a no-op ostream for gclog calls not meeting the isLevel condition
    nullOStringStream m_nullStream;
    /// mutex for thread protection to the ostream
    pthread_mutex_t   m_logMutex;

    typedef vector<ostream*> LogStreams;
    /// streams to actually log to
    LogStreams m_logstreams;
    /// streams that we are managing internally (must deallocate)
    LogStreams m_ownedlogstreams;
    /// streams that are written to in memory, but need to be flushed to disk
    /// at destruction time
    vector< pair<stringstream*,ofstream*> > m_streampairs;
    typedef vector<IGcLogger*> Loggers;
    /// list of IgcLogger peers that we need to pass log requests to
    Loggers peers;

    /// prepare the presented m_plogStream
    bool openLogFile( ) ;

    int m_nLogLevel;
    int m_nMessageLevel;

    /// logstringbuf class
    /// serves as a multiplexor of messages received into the
    /// m_plogStream, to send to all the registered ostreams
    class logstringbuf : public std::stringbuf 
    {
      public:
      logstringbuf() : stringbuf(ios_base::out), module(NULL)
      {}

      /// setModule()
      ///
      /// logstringbuf needs a handle back to the GcModuleLogger
      /// so it can flush out all logging to all of the registered
      /// ostreams
      void setModule( GcModuleLogger* mod ) {
        module=mod;
      }

      protected:
      GcModuleLogger* module;
      /// sync
      /// 
      /// Override the default sync function so that we can pass all logging
      /// on to the other ostreams when this ostream is flushed.
      virtual int sync( ) ;
    };

    /// Instantiate a logstringbuf which will be attached to our presented
    /// ostream, and give it access to protected members
    friend class logstringbuf;
    logstringbuf  m_logbuf;

    enum { nInitialLogBufSize = 128 * 1024 };
    string m_ssBuf1;
    string m_ssBuf2;
    string* m_pssBuf;
    pthread_mutex_t m_mtxThreadedDump;

    /// This friend class is intended to be instantiated as a singleton
    /// such that it will serve all GcModuleLoggers in the process
    /// A que/set is maintained to store cumulative log-requests from 
    /// each logger.  The thread will service each logger by calling 
    /// its logStringUnprotectedThread() (essentially "flush") in the 
    /// que until empty, and then will wait on a condition, signalled 
    /// by a new log-request.
    friend class GcLogScrubber;
    class GcLogScrubber {
      protected:
        typedef set<GcModuleLogger*> LoggerSet;
        typedef list<GcModuleLogger*> LoggerQ;
        LoggerSet loggers;
        LoggerQ   loggerq;

        pthread_mutex_t m_mtxScrub;
        GcModuleLogger* m_curlog;
        bool m_bScrubbing;
        bool m_bStopScrub;
        pthread_t  m_thrScrub;
        DGCcondition    m_condNewLog;
        DGCObjectAndFunction<GcLogScrubber> m_paramsThread;

        /// put a logger into the queue if its not there already
        /// signal the thread if inserted
        void push( GcModuleLogger* log ) {
          AutoMutex a( m_mtxScrub );
          if( loggers.find( log ) == loggers.end() ) {
            loggers.insert( log );
            loggerq.push_back( log );
            DGCSetConditionTrue( m_condNewLog );
          }
        }

        /// get the first logger to service, or null if non
        /// available
        GcModuleLogger* pop( ) {
          AutoMutex a( m_mtxScrub );
          GcModuleLogger* ret = NULL;
          if( !loggerq.empty() ) {
            ret = loggerq.front();
            loggerq.pop_front();
            loggers.erase( ret );
          }
          return ret;
        }
        
        /// create a new thread if there is not one running already
        void startThread() {
          AutoMutex a( m_mtxScrub );
          if( !m_bScrubbing ) {
            m_bScrubbing = true;
            m_bStopScrub = false;
            pthread_t thread_id;
            if(pthread_create(&thread_id, NULL, &DGCthread_func<GcLogScrubber>, (void*)&m_paramsThread) != 0)
            {
              cerr << "Could not start logging thread \n" << endl;
              m_bScrubbing = false;
            } 
            else {
            }

          }
        }

      public:
        GcLogScrubber() : m_curlog(NULL), m_bScrubbing(false), m_bStopScrub(false), m_thrScrub(0)
        {
          AutoMutex::initMutex( &m_mtxScrub );
  
          DGCcreateCondition( &m_condNewLog );

          m_paramsThread.object = this;
          m_paramsThread.function = &GcLogScrubber::scrubFunc;
        }

        ~GcLogScrubber() {
          /// Try to wait for any remaining logs to be flushed, however
          /// GcModuleLoggers should have removed themselves at their
          /// destruction time -- if the scrub thread is still running
          /// with logs in the que at the time this destructor is called
          /// then it will most likely result in a seg-fault since this
          /// class should only be a singleton and should only happen 
          /// at process cleanup
          { AutoMutex a(m_mtxScrub); m_bStopScrub = true; }
          DGCSetConditionTrue( m_condNewLog );
          pthread_join( m_thrScrub, NULL );
          DGCdeleteCondition( &m_condNewLog );
          AutoMutex::deleteMutex( &m_mtxScrub );
        }


        /// the main thread function
        void scrubFunc() {
          m_thrScrub = pthread_self();
          while( true ) {
            {
              AutoMutex a( m_mtxScrub );
              m_curlog = pop();
              /// quit if the class is being destroyed and
              /// we are done
              if( m_curlog == NULL && m_bStopScrub) {
                m_bScrubbing = false;
                break;
              }
            }

            /// either do the logging for the logger, 
            if( m_curlog ) 
              m_curlog->logStringUnprotectedThread( );
            /// or sleep until a new logger is added
            else {
              pthread_mutex_lock( &m_condNewLog.pMutex );
              pthread_cond_wait( &m_condNewLog.pCond, &m_condNewLog.pMutex );
              pthread_mutex_unlock( &m_condNewLog.pMutex );
            }
          }
        }

        /// Try to remove a logger from the queue before logging
        void remove( GcModuleLogger* log ) {
          GcModuleLogger* cur = NULL;
          {
            AutoMutex a( m_mtxScrub );
            cur = m_curlog;
            if( loggers.find( log ) != loggers.end() ) {
              loggerq.remove( log );
              loggers.erase( log );
              return ;
            } 
          }

          /// the logger is in the middle of being serviced,
          /// so wait until its done
          while( cur == log ) {
            usleep(0);
            { AutoMutex a(m_mtxScrub); cur = m_curlog; }
          }
        }

        /// logger asks to be flushed by the thread
        void enque( GcModuleLogger* log ) {
          push( log );
          startThread();
        }
    };
    typedef Singleton<GcLogScrubber> theScrubber;

    string m_sName;

  public:
    /// Contructor
    ///
    /// by default will prepend logged output with the name of the logger
    /// (used by gcmodules for example)
    GcModuleLogger( string sName ) ;
    /// Destructor
    //
    virtual ~GcModuleLogger() ;

};
#endif // _GCMODULE_LOGGER_HH__F89JN4T2FJMH4G8902Y3N5G4C2F908Y34G784Y5GJHSEF__

