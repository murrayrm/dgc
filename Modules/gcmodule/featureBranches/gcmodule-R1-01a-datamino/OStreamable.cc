#include "gcinterfaces/GcModuleInterfaces.hh"
#include <ostream>
using std::ostream;
ostream& operator<<(ostream& out, const OStreamable& me )
{ out << me.toString(); return out; }
