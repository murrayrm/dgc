#include "gcinterfaces/GcModuleInterfaces.hh"
#include <ostream>
using std::ostream;
ostream& operator<<(ostream& out, const OStreamable& me )
{
  const OStreamable* t = dynamic_cast<const OStreamable*>(&me);
  t->toString();
  out << me.toString(); 
  return out; 
}
