#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"
#include <assert.h>
#include "GcModuleTestDirective.hh"

int DEBUG_LEVEL=0;

#define CP1REPTIME 5

class GcModuleB : public GcModule
{
public:
  int printFlag;

	ControlStatus m_cs;
	MergedDirective	m_md;

  GcInterfaceTest::Northface * northface;

  GcModuleB(void) : GcModule( "GcModuleTestRecv", &m_cs, &m_md )
  {
    int sn_key = 321;
    printFlag = 0;

    northface = GcInterfaceTest::generateNorthface( sn_key, this );
  }

  virtual void control(ControlStatus* cs, MergedDirective* md )
  {
  }

  virtual void arbitrate(ControlStatus* cs, MergedDirective* md )
  {
		assert( md );
		assert( cs );

    //MergedDirectiveA& mergedDirective = *dynamic_cast<MergedDirectiveA*>md;
    directive1  d1;

    if ( northface->getNewDirective(&d1) ) // && printFlag)
    {
      log(0) << "got a directive: " << d1.data << "\t" << d1.getDirectiveId() << endl;
      printFlag = 0;


      if( d1.getDirectiveId() % 2 == 0 )
      {
        status1 status;
        status.id = d1.getDirectiveId();
        sprintf( status.blah, "yippy" );
        northface->sendResponse( &status );
      }
    }

  }
};


int main(void) //main(int argc, char **argv)
{
  GcModuleB moduleB;

  moduleB.Start();

  while (!moduleB.IsStopped())
  {
    sleep(1);
  }

}


