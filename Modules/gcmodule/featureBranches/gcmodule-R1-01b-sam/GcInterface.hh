#ifndef _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__
#define _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__

#include "GcModule.hh"
#include "gcinterfaces/GcModuleInterfaces.hh"
#include <map>
#include <set>
using std::set;
using std::map;
#include "AutoMutex.hh"


/// IInterface
/// provides basic functionality common between the North and Southfaces
//
class IInterface {
public:
  /// free's all cached directives and status.  Pointers that have been returne
  /// will be invalid!!!
  virtual void clearAll() = 0;
  virtual void setStaleThreshold( int n ) = 0;

  virtual ~IInterface() {}
};

/// This is the interface the arbiter will use to recv packets from another modules
/// controller and to send status responses back on
template<class YourDirectiveT, class YourStatusT>
class INorthfaceT : public IInterface
{
  public:
    // returns false if no directives waiting, or invalid pointer
    // returns true with oldest unread directive
    virtual bool getNewDirective( YourDirectiveT* dir ) = 0;

    // sends a status response to a directive with id
    virtual bool sendResponse( YourStatusT* s, unsigned directiveId  ) = 0;
    // Does the same as the above but does not ensure that you have the correct ID, use the above please!
    virtual bool sendResponse( YourStatusT* s ) = 0;

    // checks for undread directives
    virtual bool haveNewDirective() = 0;

    virtual ~INorthfaceT() {}
};

/// This is the interface the controller will use to send directives down to the next
/// modules arbiter.
template<class YourDirectiveT, class YourStatusT> 
class ISouthfaceT : public IInterface
{
  public:
    /*! push one directive out of hte port and cache its data to be paired with a status-response */
    virtual bool sendDirective( YourDirectiveT* dir ) = 0;

    /*! returns a copy of a directive that was previously sent */
    virtual YourDirectiveT getSentDirective( unsigned id ) = 0;

    /*! used to get the status of past or pending status responses of known id
     * NOTE: this will purge pending status-responses, so that getLatestStatus returns NULL and haveNewStatus retruns false */
    virtual YourStatusT* getStatusOf( unsigned id ) = 0;
    /*! shorthand to test the custom-status of a known id
     * NOTE: uses getStatusOf to fetch status */
    virtual bool isStatus( int queryStatus, unsigned id ) = 0;
    /*! pulls one status off of the incomming queue, null if haveNewStatus returns false */
    virtual YourStatusT* getLatestStatus( ) = 0;
    /*! returns true if there are status msgs waiting to be processed */
    virtual bool haveNewStatus() = 0;

    /*! remove all pending directives, past directives, past status-responses */
    virtual void flushAll( ) = 0;

    /*! resend directives that have not received a status-response yet */
    // depricated
    //virtual void resendUnAcked( ) = 0;

    virtual deque<unsigned> orderedDirectivesWaitingForResponse( ) = 0;


    virtual ~ISouthfaceT() {}
};


template< class YourDirectiveT, class YourStatusT, sn_msg sn_directive_msg, sn_msg sn_status_msg, modulename sn_modname >
class GcInterface 
: public INorthfaceT<YourDirectiveT,YourStatusT>,
  public ISouthfaceT<YourDirectiveT,YourStatusT>
{
  public:
    typedef INorthfaceT<YourDirectiveT, YourStatusT> 		Northface;
    typedef ISouthfaceT<YourDirectiveT, YourStatusT> 		Southface;
    typedef GcPortT<YourDirectiveT>														GcDirectivePort;
    typedef GcPortT<YourStatusT>															GcStatusPort;

  protected:
  IGcLogger* m_logger;

    GcInterface( int sn_key, IGcPortHandler* portHandler, IGcLogger* logger = NULL )
      : directivePort( sn_key, sn_directive_msg, sn_modname, logger ),
      statusPort( sn_key, sn_status_msg, sn_modname, logger )
  {
    initialize( portHandler, logger );
  }

    GcInterface( int sn_key, GcModule* module ) 
      : directivePort( sn_key, sn_directive_msg, sn_modname, module ),
      statusPort( sn_key, sn_status_msg, sn_modname, module )
  {
    initialize( module, module );
  }

    void initialize( IGcPortHandler* portHandler, IGcLogger* logger )
    {
      m_logger = logger ;
      m_staleThreshold = 0;

      portHandler->addPort( &directivePort );
      portHandler->addPort( &statusPort );

      AutoMutex::initMutex( &m_accessMutex );

      if( (sn_directive_msg == GCM_IN_PROCESS_DIRECTIVE || sn_status_msg == GCM_IN_PROCESS_DIRECTIVE) &&
          sn_directive_msg != sn_status_msg )
      {
        std::cerr << " when using GCM_IN_PROCESS_DIRECTIVE for your GcInterfaces, you must use "
          "for both the directive and status ports ";
        exit(0);
      }
    }

    virtual ~GcInterface() {
      DGCdeleteMutex( &m_accessMutex );
    } 

  public:
    /// The getNorthface and getSouthface functions have been depricated.
    /// Use the corresponding generateXXXXXface functions instead.
    void* DEPRICATED_getXXXXXface(int) {return NULL;} 
    #define getNorthface() DEPRICATED_getXXXXXface()// generate an error
    #define getSouthface() DEPRICATED_getXXXXXface() // generate an error
    #if 0 
    Northface* getNorthface() 
    { 
      directivePort.setSendFlag(false) ;
      statusPort.setSendFlag(true) ;
      return this; 
    }
    Southface* getSouthface() 
    {
      directivePort.setSendFlag(true) ;
      statusPort.setSendFlag(false) ;
      return this; 
    }
    #endif 
  public:

    static Southface* generateSouthface( int sn_key, IGcPortHandler* portHandler, IGcLogger* logger = NULL )
    { return (Southface*) generateFace( false, sn_key, portHandler, logger ); }
    static Northface* generateNorthface( int sn_key, IGcPortHandler* portHandler, IGcLogger* logger = NULL )
    { return (Northface*) generateFace( true, sn_key, portHandler, logger ); }
    static bool releaseSouthface( Southface*& face )
    {
      GcInterface* i = dynamic_cast<GcInterface*>(face);
      if( !i ) 
        return false;
      releaseFace( (GcInterface*&)face );
      return true;
    }
    static bool releaseNorthface( Northface*& face )
    {
      GcInterface* i = dynamic_cast<GcInterface*>(face);
      if( !i ) 
        return false;
      releaseFace( (GcInterface*&)face );
      return true;
    }

  protected:
    static GcInterface* generateFace( bool bNorth, int sn_key, IGcPortHandler* portHandler, IGcLogger* logger = NULL )
    {
      GcInterface* face = NULL;
      GcModule* mod = dynamic_cast<GcModule*>(portHandler);
      if( mod )
        face = new GcInterface( sn_key, mod );
      else
        face = new GcInterface( sn_key, portHandler, logger );
      face->statusPort.setSendFlag(bNorth);
      face->directivePort.setSendFlag(!bNorth);
      return face;
    }
    static void releaseFace( GcInterface*& face )
    {
      delete face;
      face = NULL;
    }


  protected:
    GcDirectivePort  	directivePort;
    GcStatusPort			statusPort;

    pthread_mutex_t   m_accessMutex;

    typedef set<unsigned> StatusSet;
    typedef map<int, StatusSet> StatusSetMapP;
    class StatusSetMap : public StatusSetMapP
  {
    public:
      void mapStatus( unsigned id, int status )
      {
        StatusSetMap& me = *this;
        for( typename StatusSetMapP::iterator i=me.begin(); i!=me.end(); i++ )
          (*i).second.erase( id );
        me[status].insert(id);
      }
      void remove( unsigned id )
      {
        StatusSetMap& me = *this;
        for( iterator i=me.begin(); i!=me.end(); i++ )
          (*i).second.erase(id);
      }
  };
    typedef map<unsigned, YourStatusT> StatusMap;
    typedef map<unsigned, YourDirectiveT> DirectiveMap;
    typedef deque<unsigned>  IdQueue;

    class GcPortMessageQueue : public GcDirectivePort::IGcMessageFilter
  {
    private:
      IdQueue m_ids;
    public:
      bool filter( YourDirectiveT* dir )
      {
        m_ids.push_back( dir->getDirectiveId() );
        return false;
      }

      IdQueue getIds() { return m_ids; }
  };

    class GcPortMessageFilter : public GcDirectivePort::IGcMessageFilter
  {
    public:
      unsigned m_id;
      GcPortMessageFilter( unsigned id ) : m_id(id) { }
      /*GcDirectivePort::MessageIdentifierFunc*/
      bool filter( YourDirectiveT* dir )
      { 
        return ( dir->getDirectiveId() == m_id );
      }
  };

    void clearFromDirectiveQueue( unsigned id )
    {
      GcPortMessageFilter filter(id);
      directivePort.changeMsgStatus( filter, (int)GcDirectivePort::READYTOKILL, true );
      directivePort.handleMsgs();
    }

    StatusSetMap  m_deliveryMap;
    StatusSetMap  m_customStatMap;
    StatusMap     statMap;
    DirectiveMap  m_directiveMap;
    IdQueue       m_unreadQ;

  protected:  // helpers for managing incoming status-responses
    bool statusWaiting() 
    {
      return statusPort.haveMsgs();
    }

    IdQueue m_staleDirectives;
    int     m_staleThreshold;

    YourStatusT* getStatusFromQ( )
    {
      if( statusWaiting() )
      {
        YourStatusT* s = (statusPort.getNewestMessage());
        if( s ) 
        {
          unsigned id = s->getDirectiveId();
          m_deliveryMap.mapStatus( id, GcInterfaceDirectiveStatus::ACKED );
          m_customStatMap.mapStatus( id, s->getCustomStatus() );
          //clearFromDirectiveQueue( id );
          if( true ) // use a variable later to implement getOldestUnread vs getLatest -- currently hardwired for getLatest
            m_unreadQ.clear();
          m_unreadQ.push_back( id );
          m_staleDirectives.push_back(id);
        }

        return s;
      }
      return NULL;
    }

    void cleanStale()
    /// cleanup
    // check for a size larger than some threshold and then clear down to half of that size with the stale/old list
    {
      int size = m_staleDirectives.size() ;
      /// we multiply the threshold by the cardinality of the scustom status set to avoid flushing directives
      /// before they have had a terminal status sent, assuming each directive will normally not have 
      /// more than one of each of these status messages.
      if( m_staleThreshold && size > (int)(m_staleThreshold*m_customStatMap.size() ) )
      {
        if(m_logger) m_logger->log(7) << "Revoving oldest messages: " << (size/2) << endl;
        for( int i=0; i<size/2; i++ )
        {
          unsigned stale = m_staleDirectives.front();
          m_staleDirectives.pop_front();
          m_customStatMap.remove(stale);
          m_deliveryMap.remove(stale);
          m_directiveMap.erase(stale);
          statMap.erase(stale);
        }
      }
    }

    void updateStatus()
    {
      //first get all the latest stati
      while( statusWaiting() )
      {
        cleanStale();

        YourStatusT* s = getStatusFromQ();
        if( s )
        {
          statMap[s->getDirectiveId()] = *s;
          // possible fix for memory leak in GcPort::getNewestMessage
          delete s;
        }
      }
    }

    void flushAll( )
    {
      AutoMutex autoM( m_accessMutex );

      m_deliveryMap.clear();
      m_customStatMap.clear();
      directivePort.flushMsgs();
      statusPort.flushMsgs();
      m_unreadQ.clear();
      m_staleDirectives.clear();

      // these two are going to cause serious memory leaks as is...
      statMap.clear();
      m_directiveMap.clear();
    }



  public:
    // Implement the IInterface
    //
    /// Clears all cached directives and status
    void clearAll()
    {
      flushAll();
    }

    /// This sets the number of directives that the interface will cache.  Once this number is
    /// exceeded, the oldest half of directives and status responses will be released.  Pointers
    /// returned will be invalid!  Use them temporarilly.  
    void setStaleThreshold( int n )
    {
      m_staleThreshold = n;
    }

    /// implement the southinterface
  public:
    /// This returns the directives that have not been responsded too, in order that they were posted
    /// mostly for debugging purposes.
    IdQueue orderedDirectivesWaitingForResponse( )
    {
      AutoMutex autoM( m_accessMutex );

      /// This fucntion no longer returns ids in order (order of being sent)
      //GcPortMessageQueue queue;
      //directivePort.changeMsgStatus( queue, 0, false );
      //return queue.getIds();

      StatusSet& s = m_deliveryMap[GcInterfaceDirectiveStatus::SENT] ;
      IdQueue ids( s.begin(), s.end() );
      return ids;
    }


    /// This sends a directive.  You should ensure that your directive will return a 
    /// unique id when getDirectiveId() is called
    bool sendDirective( YourDirectiveT* dir )
    {
      AutoMutex autoM( m_accessMutex );

      m_directiveMap[dir->getDirectiveId()] = *dir;
      directivePort.addMsg( dir );
      m_deliveryMap.mapStatus( dir->getDirectiveId(), GcInterfaceDirectiveStatus::SENT );
      return true;
    }

    /// You can recover recently sent directives -- they are cached here (and cleared with the clearAll() 
    /// or when the staleThreshold is exceeded (if set).
    YourDirectiveT getSentDirective( unsigned id )
    {
      AutoMutex autoM( m_accessMutex );

      /*
         if(0)
         {
         if( m_directiveMap.find(id) != m_directiveMap.end() )
         return &m_directiveMap[id];
         else
         return NULL;
         }
         */
      return m_directiveMap[id];
    }

    /// This returns the received status/response of a particular directive
    /// The pointer received is valid only temporarily -- do not store for later use.
    YourStatusT* getStatusOf( unsigned id )
    {
      AutoMutex autoM( m_accessMutex );

      updateStatus();

      // now look for the requested
      if( statMap.find(id) != statMap.end() )
        return &(statMap[id]);

      return NULL;
    }

    /// Responses/status can be checked for sent directives.  Will return true if the most 
    /// recently recieved response/status had a queryStatus value returned by getCustomStatus
    bool isStatus( int queryStatus, unsigned id )
    {
      AutoMutex autoM( m_accessMutex );

      YourStatusT* s = getStatusOf( id );
      if( s )
      {
        return (s->getCustomStatus() == queryStatus);
      }
      return false;
    }
    
    /// returns the oldest unread status received
    /// The pointer received is valid only tempo -- do not store for later use.
    YourStatusT* getLatestStatus( )
    {
      AutoMutex autoM( m_accessMutex );

      if(!haveNewStatus()) 
        return NULL;
      else {
        unsigned id = m_unreadQ.front();
        m_unreadQ.pop_front();
        return getStatusOf( id );
      }

    }

    /// Returns true if there is status that is ready to be recieved/read.  Callingi getStatusof() or 
    /// isStatus(), etc., will cause this function to return false until new status is received.
    bool haveNewStatus()
    {
      AutoMutex autoM( m_accessMutex );

      updateStatus();

      return !m_unreadQ.empty();
    }


    /// Don't use
    #if 0
    void resendUnAcked( )
    {
      AutoMutex autoM( m_accessMutex );

      directivePort.moveMsgsStatus( GcDirectivePort::SENT, GcDirectivePort::READYTOSEND );
    }
    #endif


    /// implement the north interface
  public:
    /// new directive ready?
    bool haveNewDirective() 
    {
      AutoMutex autoM( m_accessMutex );

      return directivePort.haveMsgs();
    }

    /// retreive the latest directive
    /// dir must be a valid pointer 
    /// returns false if no directive ready
    bool getNewDirective( YourDirectiveT* dir )
    {
      AutoMutex autoM( m_accessMutex );

      if( !dir )
        return false;
      YourDirectiveT* iPack = directivePort.getNewestMessage( );
      if( iPack != NULL )
      {
        {
          *dir = *(iPack);
          delete iPack;
          return true;
        }
      }
      return false;
    }

    /// Sends a response for the received directiveId
    bool sendResponse( YourStatusT* s, unsigned directiveId )
    {
      AutoMutex autoM( m_accessMutex );
      
      s->id = directiveId;

      return statusPort.addMsg( s );
    }

    /// Don't use; use the above method with the directiveId parameter
    bool sendResponse( YourStatusT* s )
    {
      AutoMutex autoM( m_accessMutex );

      return statusPort.addMsg( s );
    }


};

#endif // _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__

