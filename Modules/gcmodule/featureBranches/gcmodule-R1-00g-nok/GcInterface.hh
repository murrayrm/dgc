#ifndef _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__
#define _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__

#include "GcModule.hh"
#include "interfaces/GcModuleInterfaces.hh"
#include <map>
#include <set>
using std::set;
using std::map;

/// This is the interface the arbiter will use to recv packets from another modules
/// controller and to send status responses back on
template<class YourDirectiveT, class YourStatusT>
class INorthfaceT 
{
  public:
    // returns false if no directives waiting, or invalid pointer
    // returns true with oldest unread directive
    virtual bool getNewDirective( YourDirectiveT* dir ) = 0;

    // sends a status response to a directive with id
    virtual bool sendResponse( YourStatusT* s ) = 0;

    // checks for undread directives
    virtual bool haveNewDirective() = 0;

    virtual ~INorthfaceT() {}
};

/// This is the interface the controller will use to send directives down to the next
/// modules arbiter.
template<class YourDirectiveT, class YourStatusT> 
class ISouthfaceT
{
  public:
    /*! push one directive out of hte port and cache its data to be paired with a status-response */
    virtual bool sendDirective( YourDirectiveT* dir ) = 0;

    /*! returns a copy of a directive that was previously sent */
    virtual YourDirectiveT getSentDirective( unsigned id ) = 0;

    /*! used to get the status of past or pending status responses of known id
     * NOTE: this will purge pending status-responses, so that getLatestStatus returns NULL and haveNewStatus retruns false */
    virtual YourStatusT* getStatusOf( unsigned id ) = 0;
    /*! shorthand to test the custom-status of a known id
     * NOTE: uses getStatusOf to fetch status */
    virtual bool isStatus( int queryStatus, unsigned id ) = 0;
    /*! pulls one status off of the incomming queue, null if haveNewStatus returns false */
    virtual YourStatusT* getLatestStatus( ) = 0;
    /*! returns true if there are status msgs waiting to be processed */
    virtual bool haveNewStatus() = 0;

    /*! remove all pending directives, past directives, past status-responses */
    virtual void flushAll( ) = 0;

    /*! resend directives that have not received a status-response yet */
    virtual void resendUnAcked( ) = 0;

    virtual deque<unsigned> orderedDirectivesWaitingForResponse( ) = 0;
    

    virtual ~ISouthfaceT() {}
};

class AutoMutex
{
  pthread_mutex_t & m;
  public:
  AutoMutex( pthread_mutex_t& mutex ) : m(mutex)
  { DGClockMutex(&m); }
  ~AutoMutex()
  { DGCunlockMutex(&m); }

  static bool initMutex( pthread_mutex_t* m )
  {
    pthread_mutexattr_t   mta;
    pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE);
    if(pthread_mutex_init(m, &mta) != 0)
    {
      cerr << "Couldn't create mutex" << endl;
      return false;
    }
    return true;
  }

};

template< class YourDirectiveT, class YourStatusT, sn_msg sn_directive_msg, sn_msg sn_status_msg, modulename sn_modname >
class GcInterface : 
  public INorthfaceT<YourDirectiveT,YourStatusT>,
  public ISouthfaceT<YourDirectiveT,YourStatusT>
{
  public:
    typedef INorthfaceT<YourDirectiveT, YourStatusT> 		Northface;
    typedef ISouthfaceT<YourDirectiveT, YourStatusT> 		Southface;
    typedef GcPortT<YourDirectiveT>														GcDirectivePort;
    typedef GcPortT<YourStatusT>															GcStatusPort;

    GcInterface( int sn_key, GcModule* module ) :
      directivePort( sn_key, sn_directive_msg, sn_modname ),
      statusPort( sn_key, sn_status_msg, sn_modname ) 
  {
    module->addPort( &directivePort );
    module->addPort( &statusPort );

    AutoMutex::initMutex( &m_accessMutex );
  }

    virtual ~GcInterface() {
      DGCdeleteMutex( &m_accessMutex );
    } 

    Northface* getNorthface() 
    { 
      directivePort.sendFlag = false;
      statusPort.sendFlag = true;
      return this; 
    }
    Southface* getSouthface() 
    {
      directivePort.sendFlag = true;
      statusPort.sendFlag = false;
      return this; 
    }


  protected:
    GcDirectivePort  	directivePort;
    GcStatusPort			statusPort;

    pthread_mutex_t   m_accessMutex;

    typedef set<unsigned> StatusSet;
    typedef map<int, StatusSet> StatusSetMapP;
    class StatusSetMap : public StatusSetMapP
    {
      public:
      void mapStatus( unsigned id, int status )
      {
        StatusSetMap& me = *this;
        for( typename StatusSetMapP::iterator i=me.begin(); i!=me.end(); i++ )
          (*i).second.erase( id );
        me[status].insert(id);
      }
    };
    typedef map<unsigned, YourStatusT> StatusMap;
    typedef map<unsigned, YourDirectiveT> DirectiveMap;
    typedef deque<unsigned>  IdQueue;

    class GcPortMessageQueue : public GcDirectivePort::IGcMessageFilter
    {
      private:
        IdQueue m_ids;
      public:
        bool filter( YourDirectiveT* dir )
        {
          m_ids.push_back( dir->getDirectiveId() );
          return false;
        }

        IdQueue getIds() { return m_ids; }
    };

    class GcPortMessageFilter : public GcDirectivePort::IGcMessageFilter
    {
      public:
      unsigned m_id;
      GcPortMessageFilter( unsigned id ) : m_id(id) { }
      /*GcDirectivePort::MessageIdentifierFunc*/
      bool filter( YourDirectiveT* dir )
      { 
        return ( dir->getDirectiveId() == m_id );
      }
    };

    void clearFromDirectiveQueue( unsigned id )
    {
      GcPortMessageFilter filter(id);
      directivePort.changeMsgStatus( filter, (int)GcDirectivePort::READYTOKILL, true );
      directivePort.handleMsgs();
    }

    StatusSetMap  m_deliveryMap;
    StatusSetMap  m_customStatMap;
    StatusMap     statMap;
    DirectiveMap  m_directiveMap;
    IdQueue       m_unreadQ;

    protected:  // helpers for managing incoming status-responses
    bool statusWaiting() 
    {
      return statusPort.haveMsgs();
    }

    YourStatusT* getStatusFromQ( )
    {
      if( statusWaiting() )
      {
        YourStatusT* s = (statusPort.getNewestMessage());
        if( s ) 
        {
          unsigned id = s->getDirectiveId();
          m_deliveryMap.mapStatus( id, GcInterfaceDirectiveStatus::ACKED );
          m_customStatMap.mapStatus( id, s->getCustomStatus() );
          clearFromDirectiveQueue( id );
          if( true ) // use a variable later to implement getOldestUnread vs getLatest -- currently hardwired for getLatest
            m_unreadQ.clear();
          m_unreadQ.push_back( id );
        }

        return s;
      }
      return NULL;
    }

    void updateStatus()
    {
      //first get all the latest stati
      while( statusWaiting() )
      {
        YourStatusT* s = getStatusFromQ();
        if( s )
        {
          statMap[s->getDirectiveId()] = *s;
        }
      }

    }

    /// implement the southinterface
  public:
    IdQueue orderedDirectivesWaitingForResponse( )
    {
      AutoMutex autoM( m_accessMutex );

      GcPortMessageQueue queue;
      directivePort.changeMsgStatus( queue, 0, false );
      return queue.getIds();
    }

    bool sendDirective( YourDirectiveT* dir )
    {
      AutoMutex autoM( m_accessMutex );

      m_directiveMap[dir->getDirectiveId()] = *dir;
      directivePort.addMsg( dir );
      m_deliveryMap.mapStatus( dir->getDirectiveId(), GcInterfaceDirectiveStatus::SENT );
      return true;
    }

    YourDirectiveT getSentDirective( unsigned id )
    {
      AutoMutex autoM( m_accessMutex );

      /*
      if(0)
      {
        if( m_directiveMap.find(id) != m_directiveMap.end() )
          return &m_directiveMap[id];
        else
          return NULL;
      }
      */
      return m_directiveMap[id];
    }

    YourStatusT* getStatusOf( unsigned id )
    {
      AutoMutex autoM( m_accessMutex );

      updateStatus();

      // now look for the requested
      if( statMap.find(id) != statMap.end() )
        return &(statMap[id]);

      return NULL;
    }

    bool isStatus( int queryStatus, unsigned id )
    {
      AutoMutex autoM( m_accessMutex );

      YourStatusT* s = getStatusOf( id );
      if( s )
      {
        return (s->getCustomStatus() == queryStatus);
      }
      return false;
    }

    YourStatusT* getLatestStatus( )
    {
      AutoMutex autoM( m_accessMutex );

      unsigned id = m_unreadQ.front();
      m_unreadQ.pop_front();

      return getStatusOf( id );
    }
  
    bool haveNewStatus()
    {
      AutoMutex autoM( m_accessMutex );

      return !m_unreadQ.empty();
    }

    
    void flushAll( )
    {
      AutoMutex autoM( m_accessMutex );

      m_deliveryMap.clear();
      m_customStatMap.clear();
      directivePort.flushMsgs();
      statusPort.flushMsgs();
      m_unreadQ.clear();

      // these two are going to cause serious memory leaks as is...
      statMap.clear();
      m_directiveMap.clear();
    }


    void resendUnAcked( )
    {
      AutoMutex autoM( m_accessMutex );

      directivePort.moveMsgsStatus( GcDirectivePort::SENT, GcDirectivePort::READYTOSEND );
    }


    /// implement the north interface
    //
  public:
    bool haveNewDirective() 
    {
      AutoMutex autoM( m_accessMutex );

      return directivePort.haveMsgs();
    }

    bool getNewDirective( YourDirectiveT* dir )
    {
      AutoMutex autoM( m_accessMutex );

      if( !dir )
        return false;
      YourDirectiveT* iPack = directivePort.getNewestMessage( );
      if( iPack != NULL )
      {
        {
          *dir = *(iPack);
          return true;
        }
      }
      return false;
    }

    bool sendResponse( YourStatusT* s )
    {
      AutoMutex autoM( m_accessMutex );

      return statusPort.addMsg( s );
    }


};

#endif // _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__

