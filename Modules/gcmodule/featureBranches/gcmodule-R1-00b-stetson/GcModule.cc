#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"

//gets the newest message sent or received
void * GcPort::getNewestMessage(int * msgSize) //gets a pointer to the newest message
{
    if (msgQ.size() > 0)
    {
      *msgSize = msgQ[0].msgSize;
      return msgQ[0].msg;
    }
    else
    {
      return NULL;
    }
}

//gets the time that the last message was sent out
time_t GcPort::lastMsgTimeSent()
{

  //std::cout << "doodles\n"; //TEMPORARY
  //std::cout << "message Q size " << msgQ.size() << "\n"; //TEMPORARY

  for (int ii=0;ii<msgQ.size();ii++)
  {
    if (msgQ[ii].status == SENT)
    {
      return msgQ[ii].timeSentRcvd;
    }
    
  }

  return (time_t) 0;

}

bool GcPort::addMsg(void * msg, int msgSize) //adds the template
{ //this really needs to find a way to copy the data

    bool success = DGClockMutex(&m_qMutex);

    //std::cout << "here i am \n"; //TEMPORARY
    MessageWrapper mwrap;
    mwrap.timeSentRcvd = (time_t) -1;
    mwrap.status = READYTOSEND;

    mwrap.msg = malloc(msgSize);
    memcpy(mwrap.msg,msg,msgSize);
    mwrap.msgSize = msgSize;

    success = DGCcreateMutex(&(mwrap.m_dataBufferMutex));
    //std::cout << "successful mutex? " << success << "\n"; //TEMPORARY

    msgQ.push_front(mwrap); //I think this copies all the data
  
    success = DGCunlockMutex(&m_qMutex);

    return true;
    
}

bool GcPort::recvMsgs() //sends all of the recently added messages
{
  void * newMsg;
  int newSize;

  newMsg = receiveMessage(&newSize);
  if (NULL != newMsg)
  {
    if (newSize <= 0)
    {
      std::cout << "recvMsgs, zero size non-null msg -- this should never happen";
    }
    MessageWrapper mwrap;
    mwrap.timeSentRcvd = time(NULL); //the time we got the message
    mwrap.status = RECEIVED;

    mwrap.msg = newMsg;
    mwrap.msgSize = newSize;
    DGCcreateMutex(&(mwrap.m_dataBufferMutex));

    msgQ.push_front(mwrap); //I think this copies all the data

    return true;
  }

  return true;
}

bool GcPort::sendMsgs() //sends all of the new messages
{    

  for(int ii=0;ii<msgQ.size();ii++)
  {
      //std::cout << "s"; //TEMPORARY
      if (READYTOSEND == msgQ[ii].status)
      {
        //std::cout << "send?"; //TEMPORARY
	if (sendMessage(msgQ[ii].msg,msgQ[ii].msgSize,&(msgQ[ii].m_dataBufferMutex)))
	{
	  //std::cout << "message successfully sent \n"; //TEMPORARY
	  msgQ[ii].timeSentRcvd = time(NULL);
	  msgQ[ii].status = SENT;
	}
	else
	{
	  std::cerr << "error in GcPort::sendMsgs \n";
	  msgQ[ii].status = SENDERROR;
	}
      }
  }
  
  return true;
}

bool GcPort::handleMsgs() //clears all of the messages that are ready to die
{
    MessageWrapper  mwrap;

    int ii;
    time_t timenow = time(NULL);


    //clear based on time
    for (ii=0;ii<msgQ.size();ii++)
    {
      if ((difftime(timenow,msgQ[ii].timeSentRcvd) > maxTimeToHold) 
	  && (msgQ[ii].timeSentRcvd != (time_t) -1))
      {
	//std::cout << "here I am, ready to kill " << maxTimeToHold 
	//	  << " " << difftime(timenow,msgQ[ii].timeSentRcvd) 
	//	  << " sent? " << msgQ[ii].status << "\n"; //TEMPORARY
	msgQ[ii].status = READYTOKILL;
      }
    }

    DGClockMutex(&m_qMutex);

    ii=0;
    while (ii<msgQ.size())
    {
      if (READYTOKILL == msgQ[ii].status)
      {
	//std::cout << "here I am, ready to kill " << maxTimeToHold 
	//	  << " " << difftime(timenow,msgQ[ii].timeSentRcvd) 
	//	  << " sent? " << msgQ[ii].status << "\n";
	mwrap = msgQ[ii];
	DGCdeleteMutex(&(mwrap.m_dataBufferMutex));
	free(mwrap.msg);
	msgQ.erase(msgQ.begin()+ii);
	//std::cout << "msg killed: size of q? " << msgQ.size() << "\n"; //TEMPORARY
      }
      else
      {
	ii++;
      }
    }

    DGCunlockMutex(&m_qMutex);

    return 0;
}

int GcPort::getSendSock()
{
  if (m_sendsock < 0) {
    m_sendsock = p_skynet->get_send_sock(m_msgtype);
  }
  return m_sendsock;
};

int GcPort::getRecvSock()
{  
  if (m_recvsock < 0) {
    m_recvsock = p_skynet->listen(m_msgtype, m_modname);
    if (m_recvsock < 0) {
	std::cout << "SkynetTalker::getRecvSock(): *** error opening rcv socket";      
    }
    //std::cout << "got receive socket " << m_recvsock << "\n"; //TEMPORARY
  }
  return m_recvsock;
}


bool GcPort::sendMessage(void * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex)
{
  int bytesSent;
  bool ret = true;

  DGClockMutex(p_dataBufferMutex);
 
  // Assume the message is already serialized
  bytesSent = p_skynet->send_msg(getSendSock(), data, bytesToSend, 0);

  if(bytesSent != bytesToSend)
  {
	cerr << "SkynetTalker::send() sent " << bytesSent << "\n";
        ret = false;
  }
  else
  {
    //cout << "successfully sent" << bytesSent << " bytes on socket " << m_sendsock << "\n";
  }

  DGCunlockMutex(p_dataBufferMutex);

  return ret;
}


//allocates memory.  returns null if unsuccessful
void * GcPort::receiveMessage(int * bufSize)
{
  void * newstuff = NULL;
  int bytesReceived;
  int requiredSize;
  pthread_mutex_t m_dataBufferMutex;
  int recvsock = getRecvSock();
  if (recvsock < 0)
  {
      return NULL;
  }

  //does not wait for messages
  if (!p_skynet->is_msg(recvsock))
  {
    //std::cout << "no message \n";
    return NULL;
  }
  else
  {
    //std:cout << "got message \n";
  }

  DGCcreateMutex(&m_dataBufferMutex);

  // Build the mutex list. We want to protect the data buffer.  I'm not positive we need this.
  int numMutices = 1;
  pthread_mutex_t* ppMutices[2];
  ppMutices[0] = &m_dataBufferMutex;
  /*if(pMutex != NULL)
  {
    ppMutices[1] = pMutex;
    numMutices++;
    }*/

  requiredSize = p_skynet->get_msg_size(recvsock);
  //std::cout << "got message size: " << requiredSize << "\n";
  if (requiredSize <= 0)
  {
    std::cout << "This should not happen \n";
    return NULL;
  }
  newstuff = malloc(requiredSize); //requiredSize should be in bytes

  bytesReceived = p_skynet->get_msg(recvsock, newstuff, requiredSize,
                                   0, ppMutices, false, numMutices);

  DGCunlockMutex(&m_dataBufferMutex);
  
  if (bytesReceived != requiredSize)
  {
    std::cout << "recieveMsg -- There's a size problem here \n";
  }

  (*bufSize) = bytesReceived;
  return newstuff;

}





//--------------------GcModule code

bool GcModule::Start(void)
{ 
  //start the threads
  gcMainEndThreadFlag = 0;
  DGCstartMemberFunctionThread(this, &GcModule::gcMainThread);

  gcMessageEndThreadFlag = 0;
  DGCstartMemberFunctionThread(this, &GcModule::gcMessageThread);

  return true;
};

GcModule::~GcModule()
{
  for (int ii=0;ii<portQ.size();ii++)
  {
    delete portQ[ii];
  }
  portQ.clear();
}

bool GcModule::addPort(GcPort* newport)
{
    portQ.push_front(newport);
}

bool GcModule::remPort(int num)
{
    if (portQ.size() > num)
    {
      delete portQ[num]; //delete the pointer?
      portQ.erase(portQ.begin()+num);
      return 1;
    }
    else
    {
      return 0;
    }
}

void GcModule::gcMainThread()
{
  ControlStatus controlStatus;
  MergedDirective mergedDirective;

  while (!gcMainEndThreadFlag)
  {
    mergedDirective = Arbitrate(controlStatus);

    controlStatus = Control(mergedDirective);  
  }
}

void GcModule::gcMessageThread()
{
  int ii;

  while (!gcMessageEndThreadFlag)
  {

    for (ii=0;ii<portQ.size();ii++)
    {
      if (portQ[ii]->sendFlag)
      {
	portQ[ii]->sendMsgs();
      }
      else
      {
	portQ[ii]->recvMsgs();
      }
      portQ[ii]->handleMsgs();
    }

  }
}

GcPort* GcModule::getPortofType(sn_msg sn_msgtype)
{
  for (int ii=0;ii<portQ.size();ii++)
  {
    if (portQ[ii]->getMsgType() == sn_msgtype) { return portQ[ii]; }
  }
  return NULL;
}
