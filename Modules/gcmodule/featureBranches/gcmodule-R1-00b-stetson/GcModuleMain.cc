#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"

class directive1
{
public:
  directive1() { memcpy(data,"do your job",12); }
  char data[12];
};

#define CP1REPTIME 5
class GcModuleA : public GcModule
{
public:
  GcPort * ctrlPort1;

  GcModuleA(void)
  {
    int sn_key = 321;
    sn_msg sn_msgtype = SNrddf;
    modulename sn_modtype = MODrddftalkertestsend;
    
    GcPort * port1 = new GcPort(sn_key,sn_msgtype,sn_modtype);
    port1->sendFlag = 1; //TEMPORARY: for now, modules can only either send or recieve

    addPort(port1);
    ctrlPort1 = getPortofType(SNrddf);
  }

  ControlStatus Control(MergedDirective mergedDirective)
  {
    
    ControlStatus controlStatus;
    if (mergedDirective.directive == MergedDirective::RUN)
    {
      //just some junk
      directive1 d1;

      //periodically sends new messages
      if (ctrlPort1->getNumMsgs() == 0)
      {
	ctrlPort1->addMsg((void *) &d1,sizeof(d1));
	std::cout << "directive added: " << d1.data << "\n";
	//sleep(5); //TEMPORARY
      }

    }
    return controlStatus;
  }
};


int main(void) //main(int argc, char **argv)
{
  GcModuleA moduleA;

  moduleA.Start();

  while (!moduleA.IsStopped())
  {
    sleep(1);
  }

}
