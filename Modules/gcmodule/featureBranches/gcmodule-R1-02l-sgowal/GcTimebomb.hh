/*!
 *  \file  GcTimebomb.hh
 *  \brief A simple class to log the creation and destruction of object
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  7/23/2007
 *
 * GcTimebomb will log to an attached IgcLogger its creation and destruction
 * along with given tag id (intended to hold a line-number).
 * Its intended use is to serve as a temporary object on a line .. when the 
 * begins a log entry is made, when the line ends (after some long runnign 
 * statement hopefully) a log entry is made again.
 *
 * Derivatives can make use of the preCondition and postCondition functions
 * to control the output, and perform custom processing
 *
 */

#ifndef _GCTIMEBOMB_HH_OIDS0IFG049NT8234HY__
#define _GCTIMEBOMB_HH_OIDS0IFG049NT8234HY__


#include "gcmodule/IGcLogger.hh"
#include <sys/time.h>
#include <pthread.h>
#include <sys/resource.h>
#include <string>
#include <map>
using std::string;
using std::map;

template<class derived>
class GcTimebomb_base {
  public:
  IGcLogger*  m_logger;
  int         m_level;
  string         m_id;
  uint64_t    m_start;
  uint64_t    m_span;
  rusage      m_usage;

    bool yes() { return (m_logger && m_logger->isLevel(m_level) ); }

    GcTimebomb_base( IGcLogger* logger, int loglevel, int id ) :
      m_logger(logger), m_level(loglevel), m_id("")
    { 
      if( yes() ) {
        std::stringstream ss;
        ss << id;
        ss >> m_id;
      }
      init();
    }

    GcTimebomb_base( IGcLogger* logger, int loglevel, string id ) :
      m_logger(logger), m_level(loglevel), m_id(id)
    { init(); }

    void init()
    {
      if( yes() ) {
  getrusage( RUSAGE_SELF, &m_usage );
  m_start = m_logger->usecTime();
  if( static_cast<derived*>(this)->preCondition() ) {
    m_logger->gclog(m_level) << "GcTimebomb " << m_id<< ": " << "started at" << " " << m_start << endl;
  }
      }
    }

    ~GcTimebomb_base()
    {
      if( yes() ) {
  rusage usageNow;
  getrusage( RUSAGE_SELF, &usageNow );
  int nVolSwitches =(usageNow.ru_nvcsw - m_usage.ru_nvcsw);
  int nInVolSwitches =  (usageNow.ru_nivcsw - m_usage.ru_nivcsw) ;
  uint64_t now = m_logger->usecTime();
  m_span = now - m_start;
  if( static_cast<derived*>(this)->postCondition() ) {
    m_logger->gclog(m_level)  << "GcTimebomb " << m_id<< ": stopped at " << now 
                              << " spanning: " << m_span << " us" 
                              << "; with context switches (voluntary,invol): (" 
                              << nVolSwitches << ", " << nInVolSwitches << ")" <<endl;
  }
      }
    }

    bool preCondition() {
      return true;
    }

    bool postCondition() {
      return true;
    }

};

class GcTimebomb : public GcTimebomb_base<GcTimebomb> 
{
  public:
    GcTimebomb( IGcLogger* logger, int loglevel, int id ) :
      GcTimebomb_base<GcTimebomb>( logger, loglevel, id )
    { }

    GcTimebomb( IGcLogger* logger, int loglevel, string id ) :
      GcTimebomb_base<GcTimebomb>( logger, loglevel, id )
    { }
};


//class GcAutoTimer;
//typedef void (*GcAutoTimerFunc)(GcAutoTimer*);

struct  LineRecord {
  unsigned long long time;
  int           lineNumber;
};

template< int lineT >
class GcAutoTimer : public GcTimebomb_base<GcAutoTimer<lineT> > {
  typedef GcTimebomb_base<GcAutoTimer<lineT> > Parent;
  private:
    typedef vector<LineRecord> LineLog;
    /// since this may be used in reentrant code, we will keep a logger for every 
    /// thread that enters
    LineLog& ll() {
#     ifdef MACOSX
      int self = (int) pthread_self();
#     else
      int self = pthread_self();
#     endif
      static map<int, LineLog> logs;
      return logs[self];
    }

  public:
    const unsigned long maxTime;
    // GcAutoTimerFunc m_func;
     void* m_func;
  public:
    GcAutoTimer( IGcLogger* l, int level, string name, unsigned long timeout_us ) : 
      GcTimebomb_base<GcAutoTimer<lineT> >( l, level, name ), maxTime(timeout_us), m_func(0)
    { }
    bool preCondition() { return false; }
    bool postCondition()
    {
      // if we exceed .1 ms, then we want to know about it...
      bool retVal = false;
      if( Parent::m_span > maxTime ) {
        if( Parent::m_logger && Parent::m_logger->isLevel(Parent::m_level) ) {
          stringstream ss;
          getlog(ss);
          Parent::m_logger->gclog(0) << "Maxtime exceeded for GcAutotimer_" << lineT <<
                " span: " << Parent::m_span << " at: " << Parent::m_logger->usecTime() << endl <<
                ss.str() << endl;
          retVal = true;
        }
      }
      ll().clear();
      return retVal;
    }

    void getlog( ostream& o ) {
      static unsigned long long lasttime = 0;
      for( typename LineLog::iterator i = ll().begin(); i!=ll().end(); i++ ) {
        LineRecord& r = *i;
        o << "line: " << r.lineNumber << " time: " << r.time << " us" << 
          " diff: " << r.time - lasttime << " us" << endl;
        lasttime = r.time;
      }
    }

    void markLine(int lineNumber) {
      LineRecord r = { Parent::m_logger->usecTime(), lineNumber };
      ll().push_back(r);
    }

};

#define GCAUTOTIMER_PROFILING

#ifdef GCAUTOTIMER_PROFILING
# define GCAUTOTIMER(logger,level,id,maxtime) GcAutoTimer<__LINE__> \
     gc__autotimer(logger,level,id,maxtime)
# define GCAUTOTIMERLINE() gc__autotimer.markLine(__LINE__)
#else
# define GCAUTOTIMER(logger,level,id,maxtime)
# define GCAUTOTIMERLINE()
#endif

#endif
