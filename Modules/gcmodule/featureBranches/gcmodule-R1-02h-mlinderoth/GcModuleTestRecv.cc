/*!
 *  \file  GcModuleTestRecv.cc
 *  \brief
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *
 *  This is a test application of the GcModule architecture
 *  particularly the Northface GcInterface
 *
 *  This is not actively maintained.
 *
 */


#define OMIT_TEMPLATE_DEFS

#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"
#include <assert.h>
#include "GcModuleTestDirective.hh"

int DEBUG_LEVEL=0;

#define CP1REPTIME 5

int directive2::instances = 0;


class GcModuleB : public GcModule
{
public:
  int printFlag;

	ControlStatus m_cs;
	MergedDirective	m_md;

  GcInterfaceTest::Northface * northface;

  GcModuleB(void) : GcModule( "GcModuleTestRecv", &m_cs, &m_md )
  {
    int sn_key = 321;
    printFlag = 0;

    northface = GcInterfaceTest::generateNorthface( sn_key, this );
  }

  virtual void control(ControlStatus* cs, MergedDirective* md )
  {
  }

  virtual void arbitrate(ControlStatus* cs, MergedDirective* md )
  {
		assert( md );
		assert( cs );

    //MergedDirectiveA& mergedDirective = *dynamic_cast<MergedDirectiveA*>md;
    directive2  d1;

    if ( northface->getNewDirective(&d1) ) // && printFlag)
    {
      gclog(0) << "got a directive: " << d1.data << "\t" << d1.getDirectiveId() << endl;
      printFlag = 0;


      //if( d1.getDirectiveId() % 2 == 0 )
      if( true )
      {
        status1 status;
        status.id = d1.getDirectiveId();
        sprintf( status.blah, "yippy" );
        northface->sendResponse( &status, d1.getDirectiveId() );
      }
    }

  }

  void UT_1( )
  {
    directive2 d1, d2;
    d1.timeSent = 234;
    d2 = d1;
    gclog(1) << "times sent: " << d1.timeSent << ", " << d2.timeSent << endl;
  }
};


int main(void) //main(int argc, char **argv)
{
  GcModuleB moduleB;

  moduleB.setLogLevel(8);
  moduleB.addLogOstream( std::cerr );
  moduleB.addLogfile( "recv.out" );

  moduleB.UT_1();

  moduleB.Start();

  while (!moduleB.IsStopped())
  {
    sleep(1);
  }

}


