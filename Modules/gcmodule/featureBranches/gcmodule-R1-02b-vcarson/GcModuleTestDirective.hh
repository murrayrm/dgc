#include "GcModule.hh"
#include "GcInterface.hh"
#include "gcinterfaces/GcModuleInterfaces.hh"
#include <vector>
using std::vector;

struct testBase
{
  virtual ~testBase() {}
  virtual int getID() = 0;
  template< class ArT >
  void serialize( ArT &ar, const unsigned int v ) { }
};

struct test123 : public testBase
{
  int id;
  test123() : id(123) {}
  int getID() { return id; }

  template< class ArT >
  void serialize( ArT &ar, const unsigned int v )
  {
    boost::serialization::base_object<testBase>(*this);
    ar & id;
  }
};

class directive1 : public ITransmissive
{
  public:
    directive1() { 
      memcpy(data,"do your job",12);
      test = new test123();
    }
    char data[16*1024];
    int id;
    typedef vector<char*> Strings;
    Strings ss;
    testBase* test;
    
  bool operator==( const directive1& o ) const {
    bool eq = false;
    eq = ( 0 == memcmp( data, o.data, sizeof(data)/sizeof(data[0]) ) ) &&
         ( id == o.id ) &&
         ( ss == o.ss );

    return eq;
  }

  public:

    template< class ArchiveT >
      void serialize(ArchiveT &ar, const unsigned int version)  
      {
        ar & data;
        ar & id;
        // list all of the possible derived types here
        ar.template register_type<test123>();
        ar & test;
      }

    unsigned getDirectiveId( )
    {
      return id;
    }

    // to satisfy OStreamable... 
    // note, the const at the end -- very important! won't compile without
    std::string toString() const { 
      stringstream s("");
      s << "Directive1(" << id << ")" << "\n";
      s << "  - data=" << data << "\n";
      s << "        =" << std::hex << std::showbase; copy( data, &data[52], ostream_iterator<unsigned int>(s, " ") ); s << "\n";
      s << " test id=" << std::dec << test->getID() << endl;
      copy( ss.begin(), ss.end(), ostream_iterator<string>(s, "\t\n") );

      //cout << " ---- in toString ---: " << s.str();
      return s.str();
    }
};

class status1 : public GcInterfaceDirectiveStatus
{
  public:
    enum MyCustomStatus { NONE, STARTED, COMPLETED, FAILED };

  public:
    char blah[3*1024];
    MyCustomStatus myCustomStatus;

  public:
    int getCustomStatus( ) { return myCustomStatus; }

    // for OStreamable.. 
    // note, the const at the end -- very important! won't compile without.
    std::string toString() const { 
      stringstream s("");
      s << "status1(" << id << ")" << "\n";
      s << "  - myCustomStatus=" << myCustomStatus;
      return s.str();
    }
  friend class boost::serialization::access;
  private:
     template< class ArchT >
      void serialize( ArchT & ar, const unsigned int ver )
      {
        ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
        ar & blah;
      }
};

class GcPort : public GcPortT<directive1>
{
  public:
    GcPort(int sn_key, sn_msg sn_msgtype, modulename sn_modname) :
      GcPortT<directive1>(sn_key, sn_msgtype, sn_modname, NULL)
  {}
    virtual ~GcPort() {}
};

typedef GcInterface<directive1, status1, SNsegGoals, SNtplannerStatus, MODrddftalkertestsend> GcInterfaceTest  ;
typedef GcInterface<directive1, status1, GCM_IN_PROCESS_DIRECTIVE, GCM_IN_PROCESS_DIRECTIVE, MODrddftalkertestsend> GcIntTestInProc  ;

