#ifndef _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__
#define _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__

#include "GcModule.hh"
#include <map>
#include <set>
using std::set;
using std::map;



class ITransmissive {
  public:
    virtual ~ITransmissive() {} 
    virtual unsigned int getDirectiveId() = 0;
    // Cannot have a virtual templated function... 
    // Developers, you must implment this function(remove 'virtual'), 
    // the linker will remind you...
    //template<class ArchiveT>
    //virtual void serialize(ArchiveT, const unsigned int version ) = 0;
};

#if 0
template <class YourDirectiveT, class YourStatusT>
class TransactionPacket
{
  public:
    enum PacketType { NONE, DIRECTIVE, STATUS, NUMPACKETTYPES };
  private:
    void init( unsigned id, PacketType t, YourDirectiveT* d, YourStatusT* s )
    {	
      m_id = id;
      m_packetType = t;
      m_dir = d;
      m_stat = s;
    }
  public:
    TransactionPacket( YourDirectiveT* d ) 
    { 
      if( d )
        init(d->getDirectiveId(),DIRECTIVE,d,NULL);
      else
        init(-1,NONE, NULL, NULL );
    }
    TransactionPacket( unsigned id, YourStatusT* s ) { init(id,STATUS,NULL,s); }
    TransactionPacket( ) { init(-1,NONE,NULL,NULL); }

  public:
    unsigned int 							m_id;
    PacketType 								m_packetType;
    YourDirectiveT*			 			m_dir;
    YourStatusT*							m_stat;

  public:
    template<class ArchiveT> void serialize( ArchiveT ar, const unsigned int ver )
    {
      ar & m_id;
      ar & m_packetType;
      ar & m_dir;
      ar & m_stat;
    }

};
#endif

/// This is the interface the arbiter will use to recv packets from another modules
/// controller and to send status responses back on
template<class YourDirectiveT, class YourStatusT>
class INorthfaceT 
{
  public:
    // returns false if no directives waiting, or invalid pointer
    // returns true with oldest unread directive
    virtual bool getNewDirective( YourDirectiveT* dir ) = 0;

    // sends a status response to a directive with id
    virtual bool sendResponse( YourStatusT* s ) = 0;

    // checks for undread directives
    virtual bool haveNewDirective() = 0;

    virtual ~INorthfaceT() {}
};

class GcInterfaceDirectiveStatus : public ITransmissive
{
  public:
    // must implement this so we can auto-capture and classify 
    // status respnoses
    virtual int          getCustomStatus() = 0;

  public:
    enum DeliveryStatus { NONE, QUEUED, SENT, RECEIVED, ACKED } ;
  public:
    unsigned id;
    DeliveryStatus deliveryStat;

  public:
    GcInterfaceDirectiveStatus() : id(0), deliveryStat(NONE)
    {}
    virtual ~GcInterfaceDirectiveStatus() {}
    unsigned getDirectiveId( )
    {
      return id;
    }

    friend class boost::serialization::access;
  private:
    template< class ArchT >
      void serialize( ArchT & ar, const unsigned version )
      {
        ar & id;
        ar & deliveryStat;
      }

};

/// This is the interface the controller will use to send directives down to the next
/// modules arbiter.
template<class YourDirectiveT, class YourStatusT> 
class ISouthfaceT
{
  public:
    /*! push one directive out of hte port and cache its data to be paired with a status-response */
    virtual bool sendDirective( YourDirectiveT* dir ) = 0;

    /*! used to get the status of past or pending status responses of known id
     * NOTE: this will purge pending status-responses, so that getLatestStatus returns NULL and haveNewStatus retruns false */
    virtual YourStatusT* getStatusOf( unsigned id ) = 0;
    /*! shorthand to test the custom-status of a known id
     * NOTE: uses getStatusOf to fetch status */
    virtual bool isStatus( int queryStatus, unsigned id ) = 0;
    /*! pulls one status off of the incomming queue, null if haveNewStatus returns false */
    virtual YourStatusT* getLatestStatus( ) = 0;
    /*! returns true if there are status msgs waiting to be processed */
    virtual bool haveNewStatus() = 0;

    /*! remove all pending directives, past directives, past status-responses */
    virtual void flushAll( ) = 0;

    /*! resend directives that have not received a status-response yet */
    virtual void resendUnAcked( ) = 0;

    virtual ~ISouthfaceT() {}
};

template< class YourDirectiveT, class YourStatusT, sn_msg sn_directive_msg, sn_msg sn_status_msg, modulename sn_modname >
class GcInterface : 
  public INorthfaceT<YourDirectiveT,YourStatusT>,
  public ISouthfaceT<YourDirectiveT,YourStatusT>
{
  public:
    typedef INorthfaceT<YourDirectiveT, YourStatusT> 		Northface;
    typedef ISouthfaceT<YourDirectiveT, YourStatusT> 		Southface;
    typedef GcPortT<YourDirectiveT>														GcDirectivePort;
    typedef GcPortT<YourStatusT>															GcStatusPort;

    GcInterface( int sn_key, GcModule* module ) : directivePort( sn_key, sn_directive_msg, sn_modname ), statusPort( sn_key, sn_status_msg, sn_modname ) 
  {
    module->addPort( &directivePort );
    module->addPort( &statusPort );
  }

    virtual ~GcInterface() {} 

    Northface* getNorthface() 
    { 
      directivePort.sendFlag = false;
      statusPort.sendFlag = true;
      return this; 
    }
    Southface* getSouthface() 
    {
      directivePort.sendFlag = true;
      statusPort.sendFlag = false;
      return this; 
    }


    GcDirectivePort  	directivePort;
    GcStatusPort			statusPort;

    /// implement the southinterface
  protected:
    typedef set<unsigned> StatusSet;
    typedef map<int, StatusSet> StatusSetMapP;
    class StatusSetMap : public StatusSetMapP
    {
      public:
      void mapStatus( unsigned id, int status )
      {
        StatusSetMap& me = *this;
        for( typename StatusSetMapP::iterator i=me.begin(); i!=me.end(); i++ )
          (*i).second.erase( id );
        me[status].insert(id);
      }
    };
    typedef map<unsigned, YourStatusT> StatusMap;
    typedef map<unsigned, YourDirectiveT> DirectiveMap;

    class GcPortMessageFilter
    {
      public:
      unsigned m_id;
      GcPortMessageFilter( unsigned id ) : m_id(id) { }
      /*GcDirectivePort::MessageIdentifierFunc*/
      bool filter( YourDirectiveT* dir )
      { 
        return ( dir->getDirectiveId() == m_id );
      }
    };

    void clearFromDirectiveQueue( unsigned id )
    {
      //GcPortMessageFilter filter(id);
      //directivePort.changeMsgStatus( filter.filter, (int)GcDirectivePort::READYTOKILL, true );
    }

    StatusSetMap  m_deliveryMap;
    StatusSetMap  m_customStatMap;
    StatusMap     statMap;
    DirectiveMap  m_directiveMap;


  public:
    bool sendDirective( YourDirectiveT* dir )
    {
      m_directiveMap[dir->getDirectiveId()] = *dir;
      directivePort.addMsg( dir );
      m_deliveryMap.mapStatus( dir->getDirectiveId(), GcInterfaceDirectiveStatus::SENT );
      return true;
    }

    YourStatusT* getStatusOf( unsigned id )
    {
      // first get all the latest stati
      while( haveNewStatus() )
      {
        YourStatusT* s = getLatestStatus();
        if( s )
        {
          statMap[s->getDirectiveId()] = *s;
        }
      }
      // now look for the requested
      if( statMap.find(id) != statMap.end() )
        return &(statMap[id]);

      //debug output

      return NULL;
    }

    bool isStatus( int queryStatus, unsigned id )
    {
      YourStatusT* s = getStatusOf( id );
      if( s )
      {
        return (s->getCustomStatus() == queryStatus);
      }
      return false;
    }

    

    YourStatusT* getLatestStatus( )
    {
      if( haveNewStatus() )
      {
        int nSize = -1;
        YourStatusT* s = (statusPort.getNewestMessage(&nSize));
        if( s ) 
        {
          unsigned id = s->getDirectiveId();
          m_deliveryMap.mapStatus( id, GcInterfaceDirectiveStatus::ACKED );
          m_customStatMap.mapStatus( id, s->getCustomStatus() );
          clearFromDirectiveQueue( id );
        }

        return s;
      }
      return NULL;
    }

    void flushAll( )
    {
      m_deliveryMap.clear();
      m_customStatMap.clear();
      directivePort.flushMsgs();
      statusPort.flushMsgs();

      // these two are going to cause serious memory leaks as is...
      statMap.clear();
      m_directiveMap.clear();
    }

    bool haveNewStatus() 
    {
      return statusPort.haveMsgs();
    }

    void resendUnAcked( )
    {
      directivePort.moveMsgsStatus( GcDirectivePort::SENT, GcDirectivePort::READYTOSEND );
    }


    /// implement the north interface
    //
  public:
    bool haveNewDirective() 
    {
      return directivePort.haveMsgs();
    }

    bool getNewDirective( YourDirectiveT* dir )
    {
      if( !dir )
        return false;
      int nSize=0;
      YourDirectiveT* iPack = directivePort.getNewestMessage( &nSize );
      if( iPack != NULL )
      {
        {
          *dir = *(iPack);
          return true;
        }
      }
      return false;
    }

    bool sendResponse( YourStatusT* s )
    {
      return statusPort.addMsg( s );
    }


};

#endif // _GC_INTERFACE_HH_908V2JY8C5GM029845YJG0982C45YT8C245T89YN459G__

