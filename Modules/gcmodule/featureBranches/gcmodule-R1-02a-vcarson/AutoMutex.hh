#ifndef _AUTOMUTEX_HH_2398THY903270V98G2Y30TY_
#define _AUTOMUTEX_HH_2398THY903270V98G2Y30TY_

#include <pthread.h>
#include <errno.h>


class AutoMutex
{
  pthread_mutex_t & m;
  public:
  AutoMutex( pthread_mutex_t& mutex ) : m(mutex)
  { DGClockMutex(&m); }
  ~AutoMutex()
  { DGCunlockMutex(&m); }

  static bool initMutex( pthread_mutex_t* m )
  {
    pthread_mutexattr_t   mta;
    pthread_mutexattr_init( &mta );
    pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE);
    int err = 0;
    if((err = pthread_mutex_init(m, &mta)) != 0)
    {
      // from http://www.opengroup.org/onlinepubs/007908775/xsh/pthread_mutex_init.html
      //[EAGAIN]
      //    The system lacked the necessary resources (other than memory) to initialise another mutex. 
      //[ENOMEM]
      //    Insufficient memory exists to initialise the mutex. 
      //[EPERM]
      //    The caller does not have the privilege to perform the operation. 
      //
      //The pthread_mutex_init() function may fail if:
      //
      //[EBUSY]
      //    The implementation has detected an attempt to re-initialise the object referenced by mutex, a previously initialised, but not yet destroyed, mutex. 
      //[EINVAL]
      //    The value specified by attr is invalid. 
      //
      cerr << "Couldn't create mutex. error = " << err << endl;
      switch( err ) {
        case EAGAIN:
          cerr << "Error EAGAIN: ";
          cerr << "The system lacked the necessary resources (other than memory) to initialise another mutex. " << endl; break;
        case ENOMEM:
          cerr << "Error ENOMEM: ";
          cerr << "Insufficient memory exists to initialise the mutex. " << endl; break;
        case EPERM:
          cerr << "Error EPERM: ";
          cerr << "The caller does not have the privilege to perform the operation. " << endl; break;
          //The pthread_mutex_init() function may fail if:
        case EBUSY:
          cerr << "Error EBUSY: ";
          cerr << "The implementation has detected an attempt to re-initialise the object referenced by mutex, a previously initialised, but not yet destroyed, mutex. " << endl; break;
        case EINVAL:
          cerr << "Error EINVAL: ";
          cerr << "The value specified by attr is invalid. " << endl; break;
      }
      return false;
    }
    return true;
  }

};



#endif // _AUTOMUTEX_HH_2398THY903270V98G2Y30TY_
