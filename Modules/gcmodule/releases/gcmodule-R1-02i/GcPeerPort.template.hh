/*!
 *  \file  GcPeerPort.template.hh
 *  \brief Template implementations of GcPeerPort
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  8/08/2007 2007
 *
 *
 *
 */

#ifndef __GcPeerPort_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__
#define __GcPeerPort_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__

/// GcPeerPort template implementations
//
//

/// Contructor
//

template < class YourDirectiveT, class YourStatusT, sn_msg sn_directive_msg, 
          sn_msg sn_status_msg, modulename sn_modname >
GcPeerPort< YourDirectiveT, YourStatusT, sn_directive_msg, sn_status_msg, sn_modname >::GcPeerPort(GcInterfacePeer* interface, int sn_key, IGcLogger* logger, bool bStale, int nTimeToHold  )
  :
    GcPortT<YourStatusT>(sn_key, sn_status_msg, sn_modname,  logger, bStale, nTimeToHold  ),
    m_interface( interface )
{
}

/// Destructor
//
template < class YourDirectiveT, class YourStatusT, sn_msg sn_directive_msg, 
          sn_msg sn_status_msg, modulename sn_modname >
GcPeerPort< YourDirectiveT, YourStatusT, sn_directive_msg, sn_status_msg, sn_modname >::~GcPeerPort()
{
}

template < class dT, class sT, sn_msg dsn, sn_msg ssn, modulename mod >
bool GcPeerPort< dT, sT, dsn, ssn, mod >::haveMsgs()  
{
  return P::m_pMailman->hasNewMessage();
}

/// getNewestMessage
///
///gets the newest message sent or received
template < class YourDirectiveT, class YourStatusT, sn_msg sn_directive_msg, 
          sn_msg sn_status_msg, modulename sn_modname >
YourStatusT* GcPeerPort< YourDirectiveT, YourStatusT, sn_directive_msg, sn_status_msg, sn_modname >::getNewestMessage() 
{
  DGClockMutex(&(this->m_qMutex));
  if( haveMsgs() ) {
    /// Gamble for now, put the message in the statMap at -5..., let
    /// updateStatus move it appropriately later
    YourStatusT* msg = &(m_interface->statMap[m_interface->mapid(-5)]);
    this->m_pMailman->receive(msg);
    DGCunlockMutex(&(this->m_qMutex));
    if(this->m_logger) 
      this->m_logger->gclog(8) << " -- message recvd(" << this << "):  " << *msg << endl;
    return msg;
  }
  DGCunlockMutex(&(this->m_qMutex));
  return NULL;
}

/// releaseMessage
///
/// deallocates a message that was allocated from the memory man..
template < class dT, class sT, sn_msg dsn, sn_msg ssn, modulename mod >
void     GcPeerPort< dT, sT, dsn, ssn, mod >::releaseMessage( sT* msg )
{
}


/// recvMsgs()
///
/// sends all of the recently added messages
template < class YourDirectiveT, class YourStatusT, sn_msg sn_directive_msg, 
          sn_msg sn_status_msg, modulename sn_modname >
bool GcPeerPort< YourDirectiveT, YourStatusT, sn_directive_msg, sn_status_msg, sn_modname >::recvMsgs() 
{
  bool bProcessedOne = false;
  // return immediately before lockign the mutex if there are no messages
  bool bNew = this->m_pMailman->hasNewMessage();
  if( !bNew )
    return false;

  // iterate until we have no new messages
  DGClockMutex((pthread_mutex_t*) &(this->m_qMutex));
  for( ; bNew; bNew = this->m_pMailman->hasNewMessage() )
  {
    bProcessedOne = true;
    m_interface->updateStatus();
    continue;
  }
  DGCunlockMutex(&(this->m_qMutex));

  return bProcessedOne;
}

/// handleMsgs()
///
/// clears all of the messages that are ready to die
//template < class dT, class sT, sn_msg dsn, sn_msg ssn, modulename mod >
//bool GcPeerPort< dT, sT, dsn, ssn, mod >::handleMsgs() 
//{ return 0; }

#endif // __GcPeerPort_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__








