#include "GcModuleLogger.hh"

int GcModuleLogger::logstringbuf::sync() 
{
  //AutoMutex( module->m_logMutex );
  module->logString(str());
  str("");
  int ret = stringbuf::sync();
  return ret;
}

bool GcModuleLogger::openLogFile( ) 
{ 
  m_logbuf.setModule( this );
  m_plogStream = new std::ostream( &m_logbuf );
  //m_plogStream->rdbuf(&m_logbuf);
  return true; 
}


protectedostream GcModuleLogger::gclogProtected(int nLevel) 
{
  if( isLevel(nLevel) )
    //return *m_plogStream;
    return protectedostream(&m_logMutex, m_plogStream);
  else
  {
    m_nullStream.rdbuf()->str("");
    m_nullStream.flush();
    return protectedostream(&m_logMutex, &m_nullStream);
  }
}

void GcModuleLogger::logString( string s )
{
  LogStreams::iterator i; 
  for( i=m_logstreams.begin(); i!=m_logstreams.end(); i++ )
  {
    *(*i) << m_sName << ": " << s;
    (*i)->flush();
  }
}

void GcModuleLogger::addLogOstream( ostream& o )
{
  m_logstreams.push_back( &o );
}

void GcModuleLogger::addLogfile( string filename )
{
  ofstream* file = new ofstream( filename.c_str(), ios::out );
  m_ownedlogstreams.push_back( file );
  addLogOstream( *file );
}

GcModuleLogger::~GcModuleLogger() 
{
  for( LogStreams::iterator i=m_ownedlogstreams.begin(); i!=m_ownedlogstreams.end(); i++ )
  {
    delete *i;
  }
  delete m_plogStream; m_plogStream = NULL;
}


GcModuleLogger::GcModuleLogger( string sName ) : m_logbuf(), m_sName(sName)
{
  AutoMutex::initMutex( &m_logMutex );
  m_nLogLevel = 1;
  openLogFile( );
}


