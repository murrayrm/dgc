#ifndef _IGCLOGGER_HH_7YU4HN87FBYC37BC8ECV78GC97GB__
#define _IGCLOGGER_HH_7YU4HN87FBYC37BC8ECV78GC97GB__

#include <fstream>
#include <ostream>
#include <vector>
#include <string>
using std::string;
using std::ofstream;
using std::ostream;
using std::vector;

#include "gcmodule/AutoMutex.hh"


// forward declaration
class protectedostream;

/// Defines an interface guaranteed to be implemented to log
/// messages (somehow - depends on subclass)
//  Calls to log at a particular level are inteded to be logged if
//  the logger is running at a level equal or higher than this level, only.

class IGcLogger
{
  public:
    virtual ~IGcLogger() {} 
    virtual int setLogLevel( int nLevel ) = 0; 
    virtual int getLevel() = 0;
    virtual bool isLevel( int nLevel ) = 0;
    virtual void addLogOstream( ostream& o ) = 0;
    virtual void addLogfile( string filename ) = 0;
    
    /// gclog(n) is available via a macro until a better way of protecting
    /// against multithreaded writes is developed
    //virtual ostream& gclog(int nLevel) = 0;
    #define gclog(n)   gclogProtected(n).getOstream()
    virtual protectedostream gclogProtected(int n) = 0;
    virtual void gclogProtected( int nLevel, const char* format, ... ) = 0;
};

class protectedostream 
{
  pthread_mutex_t* m_pMutex;
  AutoMutex a;
  ostream* m_pStream;
  public:
    protectedostream(pthread_mutex_t* m, ostream* s) : m_pMutex(m), a(*m_pMutex), m_pStream(s)  { }
    ~protectedostream() { }

    ostream& getOstream() { return *m_pStream; };
};

#endif // _IGCLOGGER_HH_7YU4HN87FBYC37BC8ECV78GC97GB__
