import os;
import sys;

print os.getcwd()


apps = [ \
('asim',  '--rndf=routes-stluke/tplanner_unittest.rndf', 2, 'localhost'), \
('mplanner', '--rndf=routes-stluke/tplanner_unittest.rndf --mdf=routes-stluke/tplanner_unittest_straight.mdf -l -p --nomap', 2 , 'localhost'), \
('tplannerNoCSS', '--rndf=../routes-stluke/tplanner_unittest.rndf --disable-console --debug=1', 2 , 'localhost'), \
('rddfplanner',  '--use-endpoints --use-flat --verbose=4', 2 , 'localhost'), \
('trajfollower',  '--use-local', 2 , 'localhost'), \
('planviewer',  '--use-local', 2 , 'berlin'), \
]


appdict = {}
for i in apps:
  (appname, params, sleeptime, host) = i
  appdict[appname] = [appname, '', '', 0]   #(appname, fullpath, pid)

bFoundAll = True
for root, dirs, files in os.walk('../../bin'):
  for name in files:
    if name in appdict.keys():
      #print 'found our target'
      #print os.path.join( root, name )
      lpath =  os.path.join( os.getcwd(), os.path.join(root,name) )
      if( os.path.islink(lpath) ):
        path =  os.readlink(lpath)  
        path = os.path.join(os.path.dirname(lpath), path)
        if( os.path.isfile(path) ):
          appdict[name][1] = lpath
        else:
          print 'found a broken link: %s -> %s ' % (lpath, path)
          bFoundAll = False
      else:
        appdict[name][1] = lpath
      print appdict[name]



print 
print 

for i in appdict.keys():
  if appdict[i][1] == '':
    print 'could not find path for %s' % i
    bFoundAll = False 
if not bFoundAll:
  print 'unavailable modules, exiting'
  sys.exit()


for i in apps:
  (appname, params, sleeptime, host) = i
  path = appdict[appname][1]
  # pid = os.spawnle( os.P_NOWAIT, 'xterm -e "%(appname)s" %(params)s' % locals(), os.environ)
  cmd = '''xterm -T %(appname)s -e ssh -X %(host)s "bash --login -c '{ "%(path)s" %(params)s; echo press enter to close; read; }' "  &  ''' % locals()
  print cmd
  pid = os.system( cmd );
  # pid = os.environ['last']
  # print pid
  os.system( 'sleep %(sleeptime)d' % locals() )



