/*!
 *  \file  GcPortT.template.hh
 *  \brief Template implementations of GcPortT
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *
 * To minimize compile time, the template implementations of the GcPortT
 * have been moved to this header so that they can normally be omitted
 * from pre-processing and compilation of a *.cc file that includes 
 * (indirectly) GcPortT.hh, and thus save compile-time.  At link time, at
 * least one object file should have had this included (see OMIT_TEMPLATE_DEFS 
 * in GcPortT.hh) and should force instantiation of this code (indirectly
 * instantiate a GcPortT; try calling generateNorthFace(..) )
 *
 */

#ifndef __GCPORTT_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__
#define __GCPORTT_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__

/// GcPortT template implementations
//
//

/// Contructor
//
template <class Message>
GcPortT<Message>::GcPortT(int sn_key, sn_msg sn_msgtype, modulename sn_modname,  IGcLogger* logger, bool bStale, int nTimeToHold  )
  :
    m_bStale(bStale),
    maxTimeToHold(nTimeToHold), //hold messages for no more than 2 seconds
    m_msgtype(sn_msgtype),
    m_logger(logger)
{
  // create the appropriate mailman depending on if we are in process or not
  if( sn_msgtype == GCM_IN_PROCESS_DIRECTIVE )
    m_pMailman = new GcInProcMailman<Message>("", sn_modname);
  else
    m_pMailman = new GcSkynetMailman<Message>(sn_key, sn_msgtype, sn_modname);
  AutoMutex::initMutex(&m_qMutex);
  // we are a receiving port be default
  sendFlag = 0;
}

/// Destructor
//
template <class Message>
GcPortT<Message>::~GcPortT()
{
  delete m_pMailman;
  m_pMailman = NULL;
  while( Message* m=getNewestMessage() )
    delete m;
  DGCdeleteMutex(&m_qMutex);
}

/// getNewestMessage
///
///gets the newest message sent or received
template <class Message>
Message* GcPortT<Message>::getNewestMessage() 
{
  DGClockMutex(&m_qMutex);
  if( !msgQ.empty() )
  {
    Message* retMessage = msgQ.front().msg;
    msgQ.pop_front();
    DGCunlockMutex(&m_qMutex);
    return retMessage;
  }
  DGCunlockMutex(&m_qMutex);
  return NULL;
}

/* DEAD CODE */
//gets the time that the last message was sent out
  template <class Message>
time_t GcPortT<Message>::lastMsgTimeSent()
{
  DGClockMutex(&m_qMutex);
  for (int ii=0;ii<(int)msgQ.size();ii++)
  {
    if (msgQ[ii].status == SENT)
    {
      return msgQ[ii].timeSentRcvd;
    }
  }
  DGCunlockMutex(&m_qMutex);

  return (time_t) 0;
}

/// recvMsgs()
///
/// sends all of the recently added messages
  template <class Message>
bool GcPortT<Message>::recvMsgs() 
{
  bool bProcessedOne = false;
  // return immediately before lockign the mutex if there are no messages
  bool bNew = m_pMailman->hasNewMessage();
  if( !bNew )
    return false;

  // iterate until we have no new messages
  DGClockMutex(&m_qMutex);
  for( ; bNew; bNew = m_pMailman->hasNewMessage() )
  {
    // allocate memory for the received message
    // * should replace this with pre-allocated memory 
    Message * newMsg = new Message;
    //unsigned long long recTime = m_logger->usecTime();
    bNew = m_pMailman->receive(newMsg);
    //recTime = m_logger->usecTime() - recTime;
    if (bNew && NULL != newMsg)
    {
      // create a wrapper to contain the new message, and its status, etc.
      MessageWrapper mwrap;
      // set a timestamp and status
      mwrap.timeSentRcvd = m_bStale?time(NULL):-1; //the time we got the message
      mwrap.status = RECEIVED;

      // set the data pointer to the received message buffer
      mwrap.msg = newMsg;
      mwrap.msgSize = 0;
      // create an access mutex
      DGCcreateMutex(&(mwrap.m_dataBufferMutex));
      if(m_logger) 
        m_logger->gclog(8) << " -- message recvd(" << this << "):  " << *newMsg << endl;

      // copy this wrapper into our incoming que
      msgQ.push_back(mwrap); 
      bProcessedOne = true;
    }
    else
      // deallocate un-needed message buffer
      delete newMsg;
  }
  DGCunlockMutex(&m_qMutex);

  return bProcessedOne;
}


#if 1
/// sendMsgs()
///
/// sends all of the new messages
  template <class Message>
bool GcPortT<Message>::sendMsgs() 
{    
  DGClockMutex(&m_qMutex);
  for( typename MsgQ::iterator ii=msgQ.begin(); ii!=msgQ.end(); ii++ )
  {
    MessageWrapper& mw = *ii;
    // send only messages of appropriate status
    if (READYTOSEND == mw.status)
    {
      // Check for null pointer
      if( !mw.msg )
      {
        mw.status = READYTOKILL;
        if(m_logger) m_logger->gclog(8) << "GcPort:: null message found in GcPort Queue" << endl;
      }

      // send the message and set its status to ready to be deleted 
      else if (sendMessage(mw.msg,mw.msgSize,&(mw.m_dataBufferMutex)))
      {
        mw.timeSentRcvd = m_bStale?time(NULL):-1;
        mw.status = READYTOKILL;
        if(m_logger) m_logger->gclog(8) << " -- message sent(" << this << "):  " << *mw.msg << endl;
      }
      
      // log errors and mark message
      // * the SENDERROR messages are never cleaned or handled?
      else
      {
        if(m_logger) m_logger->gclog(1) << "error in GcPort::sendMsgs \n";
        mw.status = SENDERROR;
      }
    }
  }
  DGCunlockMutex(&m_qMutex);

  return true;
}

/// handleMsgs()
///
/// clears all of the messages that are ready to die
  template <class Message>
bool GcPortT<Message>::handleMsgs() 
{
  time_t timenow = time(NULL);

  DGClockMutex(&m_qMutex);

  //clear based on time
  for( typename MsgQ::iterator ii=msgQ.begin(); ii!=msgQ.end(); ii++ )
  {
    // step over messages that are already marked
    if( ii->status == READYTOKILL )
      continue;

    // check time to-live, mark ready to delete if time is up
    // or marked with -1
    double dTime =fabs(difftime(timenow,ii->timeSentRcvd));
    if (dTime > maxTimeToHold 
        && (ii->timeSentRcvd != (time_t) -1))
    {
      ii->status = READYTOKILL;
    }
    else /// assume there will be no others in que ready to kill if this one isn't
    {
      break;
    }
  }


  // try to empty the entire que
  while ( !msgQ.empty() )
  {
    // get the first mesg and delete if its ready
    MessageWrapper& msgwrap = msgQ.front();
    if (READYTOKILL == msgwrap.status)
    {
      DGCdeleteMutex(&(msgwrap.m_dataBufferMutex));
      delete (msgwrap.msg);
      msgQ.pop_front();
    }

    // if this one is not ready to delete, assume the 
    // later messages aren't either
    else {  
      break;
    }
  }


  DGCunlockMutex(&m_qMutex);

  // do some logging if level is high enough
  if( m_logger && m_logger->isLevel(8) )
  {
    int count= msgQ.size();
    if(m_prevCount!=count) {
      m_logger->gclog(0) << "GcPort::handle(" << this << "): que size: " << count << " -- " << count << ":" << m_prevCount << endl;
    }
    m_prevCount = count;
  }
  
  return 0;
}

#else
#endif

/// sendMessage()
///
/// to send a message we just call our mailman instance
  template <class Message>
bool GcPortT<Message>::sendMessage(Message * data, int bytesToSend, pthread_mutex_t * p_dataBufferMutex)
{
  return m_pMailman->send( data );
}


/// addMsg()
///
/// entry API -- higher level calls to send a message to a receiving
/// port.
/// If bAsynchronous message does not leave until the port is pumped
  template <class Message>
bool GcPortT<Message>::addMsg(Message * msg, bool bAsynchronous) 
{ 

  bool success = DGClockMutex(&m_qMutex);

  MessageWrapper mwrap;
  mwrap.timeSentRcvd = (time_t) -1;
  mwrap.status = READYTOSEND;

  // create a copy -- copy constructor must be implemented
  mwrap.msg = new Message( *msg );
  mwrap.msgSize = -1;

  success = DGCcreateMutex(&(mwrap.m_dataBufferMutex));

  msgQ.push_back(mwrap); 

  // empty queue now if we want things done now
  if( !bAsynchronous )
    sendMsgs();

  success = DGCunlockMutex(&m_qMutex);
  
  return true;
}

/// changeMsgStatus
///
/// manually alter the status of a message in the pump que
  template <class Message>
void GcPortT<Message>::changeMsgStatus( IGcMessageFilter& filter, int toStat, bool bOnlyFirst )
{
  DGClockMutex(&m_qMutex);
  for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
    if( filter.filter((*i).msg) ) {
      (*i).status = (msgStatus)toStat;
      if(bOnlyFirst) break;
    }
  DGCunlockMutex(&m_qMutex);
}

  template <class Message>
void GcPortT<Message>::moveMsgsStatus( msgStatus fromStat, msgStatus toStat )
{
  DGClockMutex(&m_qMutex);
  for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
    if( (*i).status == fromStat ) (*i).status = toStat;
  DGCunlockMutex(&m_qMutex);
}

  template <class Message>
void GcPortT<Message>::installMessageReadyCondition( DGCcondition* cond ) {
  m_pMailman->installMessageReadyCondition( cond );
}

  template <class Message>
void GcPortT<Message>::flushMsgs( )
{
  DGClockMutex(&m_qMutex);
  for(typename MsgQ::iterator i=msgQ.begin(); i!=msgQ.end(); i++ )
    (*i).status = READYTOKILL;
  DGCunlockMutex(&m_qMutex);
  handleMsgs();
}



#endif // __GCPORTT_TEMPLATE_HH__SDJF892VNTUH83249GE9RHG879EYHRBGE8RG__








