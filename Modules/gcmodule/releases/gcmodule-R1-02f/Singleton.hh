/*!
 *  \file  Singleton.hh
 *  \brief Implementation of delivery method of In-Process Directives and Responses
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *
 * This is a helper class used to create one and only one instance of a given
 * class within the process space.  
 *
 * For example it is used by GcInProcMailman to create only one mail depot
 * to deliver/broadcast messages from one to interface to another.
 */

#ifndef _SINGLETON_HH_AJFOWIECNJFW098EJG983849TY9384H8FWF__

template <class T>
class Singleton
{
public:
  /// Calling this function will always return the same instance of the
  /// template parameter class.
  /// The first time this function is called, it will call the default 
  /// contructor on the template parameter class.  Destruction will
  /// occur at close of the process. 
  static T& Instance() {
    static T _instance;
    return _instance;
  }
  
private:
  /// This class should never be instantiated directly -- it is only a tool
  /// to provide an instance of another class.  Ctors and dtors are thus hidden
  Singleton();          // ctor hidden
  ~Singleton();          // dtor hidden
  Singleton(Singleton const&);    // copy ctor hidden
  Singleton& operator=(Singleton const&);  // assign op hidden
};


#endif // _SINGLETON_HH_AJFOWIECNJFW098EJG983849TY9384H8FWF__
