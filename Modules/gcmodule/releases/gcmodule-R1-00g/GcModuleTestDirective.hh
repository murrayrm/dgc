#include "GcModule.hh"
#include "GcInterface.hh"
#include "interfaces/GcModuleInterfaces.hh"
#include <vector>
using std::vector;



class directive1 : public ITransmissive
{
  public:
    directive1() { memcpy(data,"do your job",12); }
    char data[12];
    int id;
    typedef vector<char*> Strings;
    

  public:

    template< class ArchiveT >
      void serialize(ArchiveT &ar, const unsigned int version)  
      {
        ar & data;
        ar & id;
      }

    unsigned getDirectiveId( )
    {
      return id;
    }
};

class status1 : public GcInterfaceDirectiveStatus
{
  public:
    enum MyCustomStatus { NONE, STARTED, COMPLETED, FAILED };

  public:
    char blah[16];
    MyCustomStatus myCustomStatus;

  public:
    int getCustomStatus( ) { return myCustomStatus; }

  friend class boost::serialization::access;
  private:
     template< class ArchT >
      void serialize( ArchT & ar, const unsigned int ver )
      {
        ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
        ar & blah;
      }
};

class GcPort : public GcPortT<directive1>
{
  public:
    GcPort(int sn_key, sn_msg sn_msgtype, modulename sn_modname) :
      GcPortT<directive1>(sn_key, sn_msgtype, sn_modname) 
  {}
    virtual ~GcPort() {}
};

typedef GcInterface<directive1, status1, SNsegGoals, SNtplannerStatus, MODrddftalkertestsend> GcInterfaceTest  ;

