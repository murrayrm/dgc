/*!
 *  \file  OStreamable.cc
 *  \brief default << operator implemenation for Ostreamables
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 */

#include "gcinterfaces/GcModuleInterfaces.hh"
#include <ostream>
using std::ostream;

/// operator<<()
///
/// this function is a friend of sub-classes of OStreamable,
/// guaranteeing the abllitity to stream a class to an ostream
ostream& operator<<(ostream& out, const OStreamable& me )
{
  const OStreamable* t = dynamic_cast<const OStreamable*>(&me);
  t->toString(); // catch dynamic_cast == null?
  out << me.toString(); 
  return out; 
}
