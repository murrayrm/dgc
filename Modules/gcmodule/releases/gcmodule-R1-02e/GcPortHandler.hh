/*!
 *  \file  GcPortHandler.hh
 *  \brief Message delivery system managers for GcInterfaces
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *
 * GcInterface relies on several different underlying delivery
 * mechanisms in different circumstances to deliver and receive
 * messages.  Each of these ports currently require polling to 
 * move an inbound message into a local que.
 * IGcPortHandler specifies an abstract interface for handling 
 * that movement.
 *
 * GcPortHandler is the most trivial implementation of the 
 * IGcPortHandler interface.  It does not support signalling 
 * from the delivery mechanism of awaiting messages, and requires
 * the implementor to call pumpPorts to move the message in.
 *
 * GcSimplePortHandlerThread is a threaded handler which calls
 * pumpPorts periodically.  The sleep time between pumping is
 * configurable by a constructor parameter, and can be inter-
 * rupted by a signal from the underlying delivery mechanism (if
 * it supports signalling).  
 *
 */

#ifndef _GCPORTHANDLER_HH_029TUNC2UT09N24UGN839HG489__
#define _GCPORTHANDLER_HH_029TUNC2UT09N24UGN839HG489__

//#include <deque>
#include <vector>
#include "dgcutils/DGCutils.hh"
#include "gcmodule/AutoMutex.hh"

using std::vector;


// forward declare the port interface which we are servicing
class IGcPort;

/// IGcPortHandler
///
/// The interface which all of our instances must implement
class IGcPortHandler
{
  public:
    virtual ~IGcPortHandler() {}

    /// Add a port to be handled when pumpPorts is called
    virtual bool addPort( IGcPort* newport ) = 0;
    /// Remove a port from list of handled ports
    virtual bool remPort( int num ) = 0;
    /// Call each registered port's send or receive functions
    /// return true if a port has received a new message
    virtual bool pumpPorts() = 0;

    /// An implementation of IGcPortHandler may have a condition which 
    /// it will wait upon for a signal indicating a new message is 
    /// in its que.  This function returs the condition waited upon, or 
    /// null if it does not exist.
    virtual DGCcondition* getMessageReadyCondition() = 0;

    /// An IGcPortHandler may signal other threads when it has received
    /// a new message.  Call this function with the other threads condition
    /// waited upon.
    virtual void installMsgReadySignal( DGCcondition* cond ) = 0;
};

/// GCpthread_cond_timedwait()
//
/// a utility function to allow timedwaits with interuptions from signals
int GCpthread_cond_timedwait( pthread_cond_t& got_request, pthread_mutex_t& request_mutex, long usecTimeout );

class GcPortHandler : public IGcPortHandler 
{
  protected:
    /// list of ports to service
    vector<IGcPort*> portQ;
    /// condition to signal when new messages are recieved
    DGCcondition* m_msgReadySignal;

  public:
    GcPortHandler() : m_msgReadySignal(NULL) { }
    virtual ~GcPortHandler() ;

    // attach a GcInterface::South|Northfaces (ports) to this module/handler to send/receive messages
    // in the message thread
    virtual bool addPort( IGcPort* newport ) ;
    virtual bool remPort( int num ) ;
    virtual bool pumpPorts() ;
    /// This portHandler does not receive a signal from underlying ports when a
    /// new message is received -- instead it is known when pumpPorts is called
    /// manually and returns true
    virtual DGCcondition* getMessageReadyCondition() { return NULL; }
    virtual void installMsgReadySignal( DGCcondition* cond ) { m_msgReadySignal = cond; }
    /// This trival portHandler does not make callbacks when a new message is 
    /// received
    virtual void handleMessageReadyCallbacks() { }
};

/// GcSimplePortHandlerThread
///
/// Wraps the port handling into a contained thread to periodically pump the
/// registered ports, handle ports immedialy if the underlying delivery mechanism
/// signals its ready, notifies another thread if a new message has been received
/// and calls a callback function.
class GcSimplePortHandlerThread : public GcPortHandler
{
  protected:
    pthread_mutex_t m_protectCollectionMutex;
  // condition used to receive signal from port when new message is ready
  DGCcondition m_condMessageReady;
  // used to stop the thread
  DGCcondition m_condStopped;
  bool gcMessageEndThreadFlag;
  // interuptable sleep time between iterations of polling the ports
  long m_nMessageLoopUsleepTime;

  // initialize stop-condition and start thread
  void initThread() {
    DGCSetConditionFalse(m_condStopped); 
    DGCstartMemberFunctionThread(this, &GcSimplePortHandlerThread::gcPumpThreadWrapper);
  }

  // initialize conditions, mutex and start thread if specified
  void init (bool bStart) 
  {
    AutoMutex::initMutex( &m_protectCollectionMutex );
    DGCcreateCondition( &m_condStopped );
    DGCcreateCondition( &m_condMessageReady );
    DGCSetConditionTrue( m_condStopped );
    if( bStart )
      initThread();
  }

public:
  /// Constructor
  /// 
  /// default to start the thread and a sleep time of 10 ms
  GcSimplePortHandlerThread(bool bStart=true, int sleepTime=(10*1000) ) : m_nMessageLoopUsleepTime(sleepTime) 
  {
    init(bStart);
  }
  
  /// Destructor
  //
  virtual ~GcSimplePortHandlerThread() {
    DGCdeleteMutex( &m_protectCollectionMutex );
    DGCdeleteCondition( &m_condStopped );
    DGCdeleteCondition( &m_condMessageReady );
  }
  
  /// Restart()
  ///
  void ReStart() { 
    Stop();
    gcMessageEndThreadFlag = false;
    initThread();
  }
  
  /// Stop()
  //
  bool Stop() { 
    gcMessageEndThreadFlag = true; 
    DGCWaitForConditionTrue( m_condStopped );
    return IsStopped();
  } 

  /// IsStopped()
  //
  /// Is the thread running
  bool IsStopped() {
    return (bool)m_condStopped;
  }

  /// getMessageReadyCondition()
  ///
  /// return the signal our thread will wait upon in a timedsleep
  DGCcondition* getMessageReadyCondition() {
    return NULL;
    //return &m_condMessageReady;
  }

  /// gcPumpThreadWrapper()
  ///
  /// entry point for the thread
  void  gcPumpThreadWrapper() {
    gcPumpThread();
  }

  /// gcPumpThread()
  //
  /// virtual function to handle the thread
  virtual void gcPumpThread()
  {
    while (!gcMessageEndThreadFlag)
    {
      // wait the specified amount of time, or return early
      // if notified of a new message available in one of the ports
       GCpthread_cond_timedwait( m_condMessageReady.pCond,
                                 m_condMessageReady.pMutex, 
                                 m_nMessageLoopUsleepTime );
      /// pump the ports.  if a new message is received, signal awaiting
      /// thread, execute callbacks, and repeat until no messages are
      /// ready
      bool bNewMessage = false;
      while ( (bNewMessage = pumpPorts()!=0) == true ) {
        if( m_msgReadySignal ) {
          pthread_cond_signal( &m_msgReadySignal->pCond );
          handleMessageReadyCallbacks();
          // yeild our timeslice to a thread that may have been woken
          // by our signal
          //DGCusleep(0);
        }
        else {
          handleMessageReadyCallbacks();
        }
      }
    }
    /// signal that the thread has terminated
    DGCSetConditionTrue( m_condStopped );
  }

  public:
    /// implement the standard IGcPortHandler interface
    bool addPort( IGcPort* newport ) { 
      AutoMutex a(m_protectCollectionMutex);
      return GcPortHandler::addPort(newport);
    }
    bool remPort( int num ) { 
      AutoMutex a(m_protectCollectionMutex);
      return GcPortHandler::remPort(num);
    }
    bool pumpPorts() {
      AutoMutex a(m_protectCollectionMutex);
      return GcPortHandler::pumpPorts();
    }
};


#endif // _GCPORTHANDLER_HH_029TUNC2UT09N24UGN839HG489__
