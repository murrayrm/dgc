#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"



// ---------------- GcModule TODO warnings
//
//#warning " the inprocess mailman is not threadsafe.  insert automutex on add|remove.listener which is also synched wiht broadcast"

//--------------------GcModule code

bool GcModule::Start(void)
{ 
	//start the threads
	gcMainEndThreadFlag = 0;
	DGCstartMemberFunctionThread(this, &GcModule::gcMainThread);

	//gcMessageEndThreadFlag = 0;
	//DGCstartMemberFunctionThread(this, &GcModule::gcMessageThread);
  GcSimplePortHandlerThread::ReStart();

	return true;
};

GcModule::GcModule( string sName, ControlStatus* cs, MergedDirective* md, 
    long nControlUSleepTime, long nMessageLoopUsleepTime ) :
  GcSimplePortHandlerThread( false, nMessageLoopUsleepTime ),
  m_nControlLoopUsleepTime(nControlUSleepTime),
  m_container(NULL),
  m_mergedDirective(md),
  m_controlStatus(cs),
  m_logbuf( this )
{

  AutoMutex::initMutex( &m_logMutex );
  //m_logbuf.setModule(this);
  openLogFile() ;

  m_nLogLevel = 1;
  m_sName = sName;
} 

GcModule::~GcModule()
{
  DGCdeleteMutex( &m_logMutex );
}

GcPortHandler::~GcPortHandler()
{
	for (int ii=0;ii<(int)portQ.size();ii++)
	{
		delete portQ[ii];
	}
	portQ.clear();
}


void GcPortHandler::pumpPorts() {

  for (int ii=0;ii<(int)portQ.size();ii++)
  {
    if (portQ[ii]->getSendFlag())
    {
      portQ[ii]->sendMsgs();
    }
    else
    {
      portQ[ii]->recvMsgs();
    }
    portQ[ii]->handleMsgs();
  }
}

bool GcPortHandler::addPort(IGcPort* newport)
{
	portQ.push_front(newport);
	return true;
}

bool GcPortHandler::remPort(int num)
{
	if ((int)portQ.size() > num)
	{
		delete portQ[num]; //delete the pointer?
		portQ.erase(portQ.begin()+num);
		return 1;
	}
	else
	{
		return 0;
	}
}


void GcModule::gcMainThread()
{
	while (!gcMainEndThreadFlag)
	{
		arbitrate(m_controlStatus, m_mergedDirective);
    log(9) << " * control status at      " << time(0) << ": " << *m_controlStatus << endl;
    log(9) << " * arbitratedDirective at " << time(0) << ": " << *m_mergedDirective << endl;
		control(m_controlStatus, m_mergedDirective);  
    usleep( m_nControlLoopUsleepTime );
	}
}

void GcModule::gcMessageThread()
{
	while (!gcMessageEndThreadFlag)
	{
    pumpPorts();
    usleep( m_nMessageLoopUsleepTime );
	}
}

