#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"



// ---------------- GcModule TODO warnings
//
//#warning " the inprocess mailman is not threadsafe.  insert automutex on add|remove.listener which is also synched wiht broadcast"

//--------------------GcModule code

bool GcModule::Start(void)
{ 
	//start the threads
	gcMainEndThreadFlag = 0;
	DGCstartMemberFunctionThread(this, &GcModule::gcMainThread);

	//gcMessageEndThreadFlag = 0;
	//DGCstartMemberFunctionThread(this, &GcModule::gcMessageThread);
  GcSimplePortHandlerThread::ReStart();

	return true;
};

GcModule::GcModule( string sName, ControlStatus* cs, MergedDirective* md, 
    long nControlUSleepTime, long nMessageLoopUsleepTime ) :
  GcSimplePortHandlerThread( false, nMessageLoopUsleepTime ),
  m_nControlLoopUsleepTime(nControlUSleepTime),
  m_container(NULL),
  m_pMergedDirectiveBase(md),
  m_pControlStatusBase(cs),
  m_logbuf( this )
{

  AutoMutex::initMutex( &m_logMutex );
  //m_logbuf.setModule(this);
  openLogFile() ;

  m_nLogLevel = 1;
  m_sName = sName;
} 

GcModule::~GcModule()
{
  DGCdeleteMutex( &m_logMutex );
}

GcPortHandler::~GcPortHandler()
{
	for (int ii=0;ii<(int)portQ.size();ii++)
	{
		delete portQ[ii];
	}
	portQ.clear();
}


void GcPortHandler::pumpPorts() {

  for (int ii=0;ii<(int)portQ.size();ii++)
  {
    if (portQ[ii]->getSendFlag())
    {
      portQ[ii]->sendMsgs();
    }
    else
    {
      portQ[ii]->recvMsgs();
    }
    portQ[ii]->handleMsgs();
  }
}

bool GcPortHandler::addPort(IGcPort* newport)
{
	portQ.push_front(newport);
	return true;
}

bool GcPortHandler::remPort(int num)
{
	if ((int)portQ.size() > num)
	{
		delete portQ[num]; //delete the pointer?
		portQ.erase(portQ.begin()+num);
		return 1;
	}
	else
	{
		return 0;
	}
}


void GcModule::gcMainThread()
{
	while (!gcMainEndThreadFlag)
	{
		arbitrate(m_pControlStatusBase, m_pMergedDirectiveBase);
    log(9) << " * control status at      " << time(0) << ": " << *m_pControlStatusBase << endl;
    log(9) << " * arbitratedDirective at " << time(0) << ": " << *m_pMergedDirectiveBase << endl;
		control(m_pControlStatusBase, m_pMergedDirectiveBase);  
    usleep( m_nControlLoopUsleepTime );
	}
}

void GcModule::gcMessageThread()
{
	while (!gcMessageEndThreadFlag)
	{
    pumpPorts();
    usleep( m_nMessageLoopUsleepTime );
	}
}

int GcModule::logstringbuf::sync() 
{
  AutoMutex( module->m_logMutex );
  module->logString(str());
  str("");
  int ret = stringbuf::sync();
  return ret;
}

    bool GcModule::openLogFile( ) 
    { 
      m_logbuf.setModule( this );
      m_plogStream = new ostream( &m_logbuf );
      m_plogStream->rdbuf(&m_logbuf);
      return true; 
    }


    protectedostream GcModule::logProtected(int nLevel) 
    {
      if( nLevel <= m_nLogLevel )
        //return *m_plogStream;
        return protectedostream(&m_logMutex, m_plogStream);
      else
      {
        m_nullStream.rdbuf()->str("");
        m_nullStream.flush();
        return protectedostream(&m_logMutex, &m_nullStream);
      }
    }

