/*!
 *  \file  GcModuleLogger.cc
 *  \brief
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *  Implementation of the GcModuleLogger
 */

#include "GcModuleLogger.hh"
#include "gcmodule/GcTimebomb.hh"

/// logstringbuf::sync()
///
/// override of defult stringbuf::sync() such that
/// we get a callback into teh associated logger
int GcModuleLogger::logstringbuf::sync() 
{
  module->logStringUnprotected(str());
  str("");
  int ret = stringbuf::sync();
  return ret;
}

/// openLogfile()
///
/// initializes the logstringbuf member and creates the ostream object
/// that is exposed for logging.  
bool GcModuleLogger::openLogFile( ) 
{ 
  // initialize our logstringbuf 
  m_logbuf.setModule( this );
  // create the ostream that we will present for logging, and
  // attach our logstringbuf
  m_plogStream = new std::ostream( &m_logbuf );
  return true; 
}

/// usecTime()
///
/// return timestamp in usec
uint64_t GcModuleLogger::usecTime() {
  struct timeval  now={0};
  /* get current time */ 
  gettimeofday(&now, 0 );
  uint64_t nowusec = uint64_t(now.tv_sec) * uint64_t(1000*1000) + now.tv_usec;
  return nowusec;
}

/// gclogProtected()
/// 
/// main logging function, although generally gclog(n) should
/// be used for shorthand
/// main function to return a protectedostream which has
/// both thread protection and returns a no-op ostream or
/// the real multiplexing ostream based on the loglevel
protectedostream GcModuleLogger::gclogProtected(int nLevel) 
{
  //GcMLTimer timer( this, 0, "gclogProtected" );
  m_nMessageLevel = nLevel;
  if( isLevel(nLevel) )
    // return our multiplexing ostream
    return protectedostream(this, &m_logMutex, m_plogStream);
  else
  {
    // flush our no-op stream of any data it may have accumulated
    m_nullStream.rdbuf()->str("");
    m_nullStream.flush();
    // return the no-op ostream
    return protectedostream( this, &m_nullStream);
  }
}


/// logStringUnprotected()
/// 
/// low-level function that actually, unconditionally logs
/// a string to all the ostreams, and tries writing to peers
void GcModuleLogger::logStringUnprotected( string s )
{
  int n = m_nMessageLevel;
  // first log the string in each of our peer loggers
  for( Loggers::iterator i=peers.begin(); i!=peers.end(); i++ ) {
    if( (*i) && (*i)->isLevel(n) ) {
      (*i)->logStringUnprotected(s);
    }
  }

  // log the string to all of our registered ostreams
  LogStreams::iterator i; 
  for( i=m_logstreams.begin(); i!=m_logstreams.end(); i++ )
  {
    // prepend each log message with the name of the logger
    *(*i) << m_sName << ": " << s;
    (*i)->flush();
  }
}

/// addLogOstream()
//
void GcModuleLogger::addLogOstream( ostream& o )
{
  m_logstreams.push_back( &o );
}

/// addLogfile()
//
void GcModuleLogger::addLogfile( string filename, bool bAppendTimestamp )
{
  if( bAppendTimestamp )
  {
    // create a timestamp suffix
    char timestamp[128];
    time_t t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
    filename += "." + string(timestamp) + ".log";
    string suf = "";
    // check for pre-existence of named file, and append .NN until unique 
    // name is found
    for(int i=1; std::ifstream( (filename+suf).c_str()); i++ ) {
      std::ostringstream ss;
      ss << "." << i;
      suf = ss.str();
    }
    filename += suf;
  }

  #ifdef GC_USE_IN_MEMORY_LOGGING
  // open the file
  ofstream* file = new ofstream( filename.c_str(), ios::out );
  *file << "file opened.  Waiting for in memory log to dump on close()\n" << endl;
  stringstream* inmemlog = new stringstream( );
  m_ownedlogstreams.push_back( file );
  m_ownedlogstreams.push_back( inmemlog );
  m_streampairs.push_back( pair<stringstream*, ofstream*>(inmemlog, file) );
  addLogOstream( *inmemlog );
  #else // GC_USE_IN_MEMORY_LOGGING
  ofstream* file = new ofstream( filename.c_str(), ios::out );
  m_ownedlogstreams.push_back( file );
  addLogOstream( *file );
  #endif // GC_USE_IN_MEMORY_LOGGING
}

/// Destructor
///
/// clean up any resources we allocated, such as ofstreams
GcModuleLogger::~GcModuleLogger() 
{
  /// if we used in-memory logging to defer logging to disk, then
  /// we flush those streams out now
  while( !m_streampairs.empty() ) {
    pair<stringstream*,ofstream*> p = m_streampairs.back();
    m_streampairs.pop_back();

    ostream* file = p.second;
    stringstream* log = p.first;
    *file << "dumping in memory log...\n";
    *file << log->str();
    *file << "done with in memory dump \n";
  }

  /// Any streams that we created should be in the m_ownedlogstreams list,
  /// and will be deleted/closed here
  for( LogStreams::iterator i=m_ownedlogstreams.begin(); i!=m_ownedlogstreams.end(); i++ )
  {
    delete *i;
  }

  /// destroy our exposed ostream
  delete m_plogStream; m_plogStream = NULL;
}

/// Constructor
//
GcModuleLogger::GcModuleLogger( string sName ) : m_logbuf(), m_sName(sName)
{
  AutoMutex::initMutex( &m_logMutex );
  m_nLogLevel = 1;
  m_nMessageLevel = 1;
  /// initialize our exposed ostream
  openLogFile( );

  // to avoid any dereferning of the master logger (static of IGcLogger), 
  // all instances of GcModuleLogger set themselves as the master logger.
  // a seperate call to setMaster should be made in the program to set the 
  // appropriate one after the program is initialized
  IGcLogger::setMaster( this ) ;
}

/// attachPeerLogger
//
void GcModuleLogger::attachPeerLogger( IGcLogger* logger ) 
{
  if( logger )
    peers.push_back( logger );
}

