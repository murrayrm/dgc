/*!
 *  \file  GcInProcMailman.template.hh
 *  \brief Implementation of delivery method of In-Process Directives and Responses
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *  GcInProcMailman.template.hh contains the declarations and template
 *  definitions of the delivery mechanism used when communicating 
 *  directives and responses within the same process space.
 *
 */

#ifndef _GCINPROCMAILMAN_TEMPLATE_HH_9283UHG87B34HJ8DHJ87HG982HRG072NFH7G284NHG__ 
#define _GCINPROCMAILMAN_TEMPLATE_HH_9283UHG87B34HJ8DHJ87HG982HRG072NFH7G284NHG__

#include "gcmodule/Singleton.hh"
#include "gcmodule/GcMemoryMan.hh"

template <class MsgT>
class GcInProcMailman : public IGcMailman<MsgT>
{
  protected:
    /// This mailman relies on a "global" GcInProcMailDepot to deliver messages.  We 
    /// initialize our handle to it here with a Singleton wrapper.
    /// The maildepot broadcasts messages to different registered listeners based
    /// on messagetypes, which are merely string identifiers.
    class GcInProcMailDepot 
    {
      protected:
        typedef GcInProcMailman<MsgT> GcInProcMsgMailman;
        typedef list<GcInProcMsgMailman*>  InProcMailList; // list of listeners
        typedef map<string, InProcMailList > InProcMailMap; 
        InProcMailMap map;  // lists of listeners base on messagetype

      public:
        /// Add a mailman to the broadcast list for a given messagetype
        bool registerListener( string mailtype, GcInProcMailman<MsgT>* mailman ) ;
        /// Remove a mailman from the broadcast list for a given messagetype
        bool removeListener( string mailtype, GcInProcMailman<MsgT>* mailman ) ;
        /// Broadcast a message of a given type to all listeners
        bool broadcast( string mailtype, MsgT* msg ) ;

    };

  protected: 
    string m_mailtype;
    typedef deque<MsgT*> MsgQ;
    MsgQ msgs;
    friend class GcInProcMailDepot;
    GcInProcMailDepot& depot;
    pthread_mutex_t m_mtxQ;
    void push_back( MsgT& msg ) ;
    DGCcondition * m_receivedMessageCond;
    GcMemoryMan<MsgT> m_mem;

  public:
    GcInProcMailman( string msgtype, int modulename, DGCcondition* condMessageReady  );

    virtual ~GcInProcMailman( );

  public:
    typedef IGcMailman<MsgT> PMailman;

    /// Implement the IGcMailman interface
    bool hasNewMessage() ;
    bool receive(MsgT* msg) ;
    bool send(MsgT* msg) ;
    bool setMode( typename PMailman::OperationMode m ) ;
    void installMessageReadyCondition( DGCcondition* cond );

};

/// Contructor
/// 
/// Our maildepot variable is initialized with the singleton,
/// Our messagetype is constructed from run-time-type information of the 
/// template parameter class combined with a string parameter 
/// (to distinguish channels that may be delivering the same message 
/// types)
template<class MsgT>
GcInProcMailman<MsgT>::GcInProcMailman( string msgtype, int modulename, DGCcondition* cond=NULL ) :
  depot( Singleton< GcInProcMailDepot >::Instance() ),
  m_receivedMessageCond( cond ),
  m_mem(8, NULL)
{
  AutoMutex::initMutex(&m_mtxQ);

  /// construct our messagetype string
  stringstream s; 
  s << "_" << msgtype << ":" << typeid(MsgT).name() << modulename;
  m_mailtype = s.str();
}

/// Destructor
///
  template<class MsgT>
GcInProcMailman<MsgT>::~GcInProcMailman( )
{
  DGCdeleteMutex(&m_mtxQ);
}

template<class MsgT>
void GcInProcMailman<MsgT>::installMessageReadyCondition( DGCcondition* cond ) {
  m_receivedMessageCond = cond;
}

/// push_back
///
/// This function is called by the GcInProcMailDepot when it broadcasts a
/// a message -- this function needs to actually perform the transfer.
/// 
/// Also note this implies that this function is called in the context of
/// the sending thread, rather than the receiving.
///
/// After the message is transfered, we also signal any conditions
/// that may be waiting for new messages.
template<class MsgT>
void GcInProcMailman<MsgT>::push_back( MsgT& msg ) {
  /// Protect data while we insert the data
  {
    AutoMutex a(m_mtxQ);
    MsgT* pmsg = m_mem.gcmmNew();
    *pmsg = msg;
    msgs.push_back( pmsg );
  }

  /// Notify the waiting thread that there is a new message ready
  if( m_receivedMessageCond ) {
    pthread_cond_signal( &m_receivedMessageCond->pCond );
    // Sleeping may cause a context switch to a waiting thread, but it also
    // delays the callbackMechanism
    //DGCusleep(0);
  }
}

/// send
///
/// broadcast the message through the GcInProcMailDepot, essentially 
/// multiplexing the signal to all listeners
///
template<class MsgT>
bool GcInProcMailman<MsgT>::send(MsgT* msg) {
  AutoMutex a(m_mtxQ);
  return depot.broadcast( m_mailtype, msg );
}

/// receive
/// 
/// external function receive a message from the delivery mechanism
/// parameter msg is an output param and should not be null
/// 
template<class MsgT>
bool GcInProcMailman<MsgT>::receive(MsgT* msg) {
  if( !msg ) 
    return false;
  
  AutoMutex a(m_mtxQ);
  if( hasNewMessage() )
  {
    MsgT* temp = msgs.front();
    *msg = *temp;
    msgs.pop_front();
    m_mem.gcmmDelete(temp);
    return true;
  }
  else
    return false;
}

/// setMode
/// 
/// Configures an instance of GcInProcMailman as a sender or receiver by
/// registering or removing itself with teh GcInProcMailDepot.
/// 
template<class MsgT>
bool GcInProcMailman<MsgT>::setMode( typename PMailman::OperationMode m ) {
  switch(m) {
    case PMailman::RECEIVER:   depot.registerListener( m_mailtype, this ); break;
    case PMailman::SENDER: depot.removeListener( m_mailtype, this ); break;
    default:
                           return false;
  }
  return true;
}

/// hasNewMessage
///
  template<class MsgT>
bool GcInProcMailman<MsgT>::hasNewMessage() 
{ 
  AutoMutex a(m_mtxQ); 
  return !msgs.empty(); 
}


// GcInProcMailDepot functions

/// registerListener
/// 
template<class MsgT>
bool GcInProcMailman<MsgT>::
GcInProcMailDepot::registerListener( string mailtype, GcInProcMailman<MsgT>* mailman ) {
  InProcMailList& l = map[mailtype];
  l.push_back( mailman );
  return true;
}

/// removeListener
///
template<class MsgT>
bool GcInProcMailman<MsgT>::
GcInProcMailDepot::removeListener( string mailtype, GcInProcMailman<MsgT>* mailman ) {
  InProcMailList& l = map[mailtype];
  l.remove( mailman );
  return true;
}


/// broadcast
/// 
template<class MsgT>
bool GcInProcMailman<MsgT>::
GcInProcMailDepot::broadcast( string mailtype, MsgT* msg ) {
  InProcMailList& l = map[mailtype];
  if( l.empty() ) 
    return false;
  for( typename InProcMailList::iterator iter=l.begin(); 
      iter!=l.end(); 
      iter++ )
    (*iter)->push_back( *msg );
  return true;
}

#endif // _GCINPROCMAILMAN_TEMPLATE_HH_9283UHG87B34HJ8DHJ87HG982HRG072NFH7G284NHG__
