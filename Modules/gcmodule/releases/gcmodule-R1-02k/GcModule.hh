/*!
 *  \file  GcModule.hh
 *  \brief Definition of GcModule and supporting classes
 *
 *  \author Joshua Doubleday, jdoubled
 *  \date  Spring 2007
 *
 *  GcModule is the main wrapper class representing the Canonical Software
 *  Structure (CSS).  Its main features include arbitrate() and control()
 *  functions which by default are called sequentially, repeatedly by
 *  a dedicated thread.
 *
 *  These two functions share a pair of datastructures, a derivative of 
 *  MergedDirective and a derivative of ControlStatus (each of which
 *  should implement OStreamable for logging purposes).  
 *
 *  The implementor needs to define the arbitrate() and control() functions, 
 *  as well as the parameter classes (although the base classes could be
 *  used as is).  
 *
 *  GcModules communicate via GcInterfaces.  These are actually completely
 *  separated from GcModule itself, and can be used without implementing
 *  a GcModule entirely, however they require an IGcPortHandler, and 
 *  optionally an IGcModuleLogger.
 *
 *  GcModule is itself a IGcPortHandler (threaded), and also implements
 *  a GcModuleLogger interface, so when obtaining instances of gcInterfaces
 *  the instance of GcModule can be provided for both of these parameters.
 *
 */

#ifndef _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__
#define _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__

#include <deque>
#include <list>
#include <map>
#include <sstream>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include "interfaces/sn_types.h"
#include "gcinterfaces/GcModuleInterfaces.hh"
#include "gcmodule/AutoMutex.hh"
#include "gcmodule/GcModuleLogger.hh"
#include "gcmodule/GcPortHandler.hh"

using std::list;
using std::map;
using std::string;


/// MergedDirective
///
/// This base class serves as input to the control stage of a gcModule.  The
/// arbitrate stage should assemble all given directives to a module allong
/// with the ControlStatus of the module to generate a signle, consistent
/// mergedDirective for control ot operate on.
class MergedDirective : public OStreamable
{
  public:
    int id;
    virtual ~MergedDirective() {}
    virtual string toString() const { std::ostringstream s(""); s << id; return s.str(); }
};

/// ControlStatus
/// 
/// This base class serves as an input to the arbitrate stage of a module.
/// As control operates on its mergedDirectives and as other state information
/// is seen in the module, ControlStatus should reflect the state of the 
/// module as a whole, providing necessary information to the arbitrate stage
/// on how to handle incoming directives (reject immediately, or merge/process)
class ControlStatus : public OStreamable
{
  public:
    enum statusTypes { FAILED, RUNNING, STOPPED } status;

    virtual ~ControlStatus() {}
    virtual string toString() const { std::ostringstream s(""); s << status; return s.str(); }
};

// forward declare the container class
class GcModuleContainer;


/// GcModule
///
/// GcModule is mostly a control wrapper around implementing the main 
/// processing loop of a module (arbitrate<->control) and the delivery
/// of messages over GcInterfaces.  The class inherits from
/// GcSimpolePortHandlerThread to provide processing of messages.
/// 
/// The main processing loop by default is handled by a second thread
/// created by the class to continually loop between arbitrate and control.
/// Alternatively, the class can be instantiated as an "event based" module
/// in which arbitrate and control are called from within the context
/// of the messaging thread as a callback at the arival of a new message
/// (directive or status).  The callback function can be 
/// overridden by subclassing if a different behavior than the default is 
/// desired.
/// 
/// Each of these threads uses a pthread_cond_timedwait function to throttle
/// its loop time and yeild the CPU at times specified by the implementor.  
/// If the underlying delivery mechanism
/// provides a method of signaling arival of new messages, then the timedwait
/// can be interrupted early, for low-latency.
/// 
/// GcModule also implements a GcModuleLogger, allowing for convenient logging
/// utilities within arbitrate, control and other member function, as well
/// as serving as a logger for attached GcInterfaces which provide debug
/// information at higher levels (>=7).
class GcModule : public GcModuleLogger, public GcSimplePortHandlerThread
{
  protected:
    long m_nControlLoopUsleepTime;
    //long m_nMessageLoopUsleepTime;
    string m_sName;
    GcModuleContainer* m_container;
    friend class GcModuleContainer;
    void setContainer( GcModuleContainer* c ) { m_container = c ;}
    DGCcondition m_msgReady;
    bool m_bEventDriven;
    pthread_t m_thrMainthread;
  public:
    GcModule( string sName, ControlStatus* cs, MergedDirective* md, 
              long nControlUSleepTime=10000, long nMessageLoopUsleepTime=10000, 
              bool bEventDriven=false );
    virtual ~GcModule(void);

    string getName() { return m_sName; }

    /// these are used to start and stop both the messaging thread and the main 
    /// arbitrate/control thread
    DGCcondition m_condMainThreadDone;
    bool Start();
    bool Stop() { 
      bool bSuccess = true;
      bSuccess &= GcSimplePortHandlerThread::Stop();
      gcMainEndThreadFlag=true;
      if( !m_bEventDriven && m_thrMainthread && m_thrMainthread != pthread_self() ) {
        pthread_join( m_thrMainthread, NULL );
        //DGCWaitForConditionTrue( m_condMainThreadDone );
      }
      return bSuccess;
    }
    int IsStopped() { 
      return (gcMessageEndThreadFlag && gcMainEndThreadFlag); 
    }

    /// An event driven GcModule will have the following function called
    /// when a new directive or status message is received.
    /// The default mainloopCallback will call arbitrate, control 
    /// REPEAT_ARBITRATE_CONTROL_CALLBACK_COUNT times before
    /// returning.
    enum { REPEAT_ARBITRATE_CONTROL_CALLBACK_COUNT = 2 } ;
    virtual void handleMessageReadyCallbacks() ;
    void mainloopCallback() ;


    /// main functions of the module
    /// each of the following arbitrate and control take two pointer parameters.  
    /// Each pointer will be aimed at datastructures that should be created by the sub-class,
    /// and which should have been provided to the constructor of this base class.
    /// Arbitrate and control are called in the main thread created by this object -- the
    /// subclass just needs to implement them -- they are worker-thread stubs.
    //
    /// arbitrate()
    /// Arbitrate should receive external directives from the controlling module above,
    /// process them against this modules current control status and generate a
    /// mergedDirective to pass into control()
    virtual void arbitrate(ControlStatus* /*input*/controlStatus, MergedDirective* /*output*/ md) = 0;
    /// control()
    /// control should process the mergedDirective from arbitrate, perform the main 
    /// algorithms of this module (tactics), generate and send directives to a controlled 
    /// module below, receive status from controlled modules and update the control-status
    virtual void control(ControlStatus* /*output?*/controlStatus, MergedDirective* /*input*/mergedDirective) = 0;

  protected:

    /*! This is the module's main thread. */
    void gcMainThread();

    /// Flags used to control the termination of threads
    bool gcMainEndThreadFlag;
    int gcMessageEndThreadFlag;

    /// these pointers are set in the contructor to aim at the sub-classes instantiations
    /// of MergedDirective and ControlStatus subclasses
    MergedDirective* m_pMergedDirectiveBase;
    ControlStatus* m_pControlStatusBase;

};




#endif // _GCMODULE_HH_JLKSDJFNCJWE98T983HN9834N34__
