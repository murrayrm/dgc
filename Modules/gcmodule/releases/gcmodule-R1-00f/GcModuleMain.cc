#include <stdio.h>
#include <stdlib.h>
#include "dgcutils/DGCutils.hh"
#include "GcModule.hh"
#include <assert.h>
#include "GcModuleTestDirective.hh"
#include "GcInterface.hh"


int DEBUG_LEVEL=0;


#define CP1REPTIME 5
class GcModuleA : public GcModule
{
  public:
    GcInterfaceTest * testInterface;
    GcInterfaceTest::Southface* southface;


    class ControlStatusA : public ControlStatus {};
    class MergedDirectiveA : public MergedDirective {
      public:
        enum Status { NONE, STOP, RUN };
        Status directive;
    };
    ControlStatusA m_ctrlStatusA;
    MergedDirectiveA m_mgddA;

    GcModuleA( int sn_key ) : GcModule( &m_ctrlStatusA, &m_mgddA )
  {
    southface = NULL;
    testInterface = new GcInterfaceTest( sn_key, this );
    if( testInterface )
      southface = testInterface->getSouthface();
  }

    void arbitrate(ControlStatus* cs, MergedDirective* md)
    {
      MergedDirectiveA& mergedDirective = *(MergedDirectiveA*)md;

      std::cout << "mergedDir set to run\n" << std::endl;
      mergedDirective.directive = MergedDirectiveA::RUN;
    }

    void control(ControlStatus* cs, MergedDirective* md)
    {
      assert( cs );
      assert( md );
      ControlStatusA& controlStatus = *dynamic_cast<ControlStatusA*>(cs);
      MergedDirectiveA& mergedDirective = *(MergedDirectiveA*)md;

      if (mergedDirective.directive == MergedDirectiveA::RUN)
      {
        //just some junk
        directive1 d1;
        d1.id = time(NULL);


        deque<unsigned> noAck = southface->orderedDirectivesWaitingForResponse();
        cout << "dirctives that have not been responeded too: ";
        copy( noAck.begin(), noAck.end(), ostream_iterator<unsigned>( cout, ", " ) );
        cout << endl;


        //periodically sends new messages
        {
          southface->sendDirective(&d1);
          std::cout << "directive added: " << d1.data << ":" << d1.getDirectiveId() << "\n";
        }

        status1* stat = southface->getStatusOf( d1.id );
        if( stat )
        {
          cout << "received status on " << stat->getDirectiveId() ;
          cout << " data: " << stat->blah << endl;
        }

        sleep(5); //TEMPORARY

      }
      return;
    }
};


int main(void) //main(int argc, char **argv)
{
  GcModuleA moduleA( 321 );

  cout << "starting TestModule (northern/control'er)\n" << endl;

  moduleA.Start();

  while (!moduleA.IsStopped())
  {
    sleep(1);
  }

}
