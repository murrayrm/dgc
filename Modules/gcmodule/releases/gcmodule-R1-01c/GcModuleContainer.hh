#ifndef _GCMODULECONTAINER_HH_7584YTOIEHSD398HY4OGR380YT4HRI__
#define _GCMODULECONTAINER_HH_7584YTOIEHSD398HY4OGR380YT4HRI__

#include <map>
#include <string>
using std::map;
using std::string;


// forward declaration
class GcModule;

typedef map<string, GcModule*> GcModuleMap;

class GcModuleContainer
{
  public:
    GcModuleMap modules;

    // adds a module to the container
    void addModule( GcModule* m )
    {
      if( m )
        //modules.insert( m->getName(), m );
        modules[m->getName()] = m;
    }

    // remove a module from the container.  callers responsibility to 
    // destroy it
    GcModule* removeModule( string name )
    {
      GcModule* ret = NULL;
      if( modules.find(name) != modules.end() )
      {
        ret = modules[name];
        modules.erase(name);
      }
      return ret;
    }

    /// Sets the log level for all the contained modules
    struct setlogFunc : public unary_function<GcModuleMap::value_type, void> {
      setlogFunc(int nLevel) : n(nLevel) {}
      void operator() (argument_type i) { ((i).second)->setLogLevel(n); }
      int n;
    };
    void setLogLevel( int nLevel ) {
      std::for_each( modules.begin(), modules.end(), setlogFunc(nLevel) );
    }

};

#endif // _GCMODULECONTAINER_HH_7584YTOIEHSD398HY4OGR380YT4HRI__
