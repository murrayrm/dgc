#ifndef __MVCOMMANDPACKET_H__
#define __MVCOMMANDPACKET_H__

/* File:	MVCommandPacket.h
 * Description: This file describes the format of the UDP packets that are sent
 *		to the vehicles from the MVServer.
 * Author:	Rob Christy
 * CVS:		$Id: MVCommandPacket.h,v 1.8 2003/08/08 22:07:31 waydo Exp $
 */

#include <ieee754.h>

// Constants
#define	DEFAULT_COMMAND_PORT	    2020

// Command types. This is no longer implemented as a datatype for network
// portability issues. Since C++ refuses to implicitly cast enums to other
// types (e.g. uint16_t), we have to use #defines rather than an enum.
#define CMD_IDLE		    0
#define CMD_VELOCITY		    1
#define CMD_SHUTDOWN		    2

// Type definitions
typedef struct
{
    // At one point, this was an enumerated type. But since it's getting sent
    // over the network, its better to stick to a fixed-size datatype.
    uint16_t type;

    // Rather then send architecture-dependent floating point values over the
    // network, we're using the ieee754 standard
    union ieee754_float xvel;
    union ieee754_float yvel;
    union ieee754_float tvel;

} CommandPacket;

#endif
