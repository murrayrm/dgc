#ifndef __MVWT1VISIONPACKET_H__
#define __MVWT1VISIONPACKET_H__

/* File:	MVWT1VisionPacket.h
 * Description: This file describes the format of the UDP packets that are sent
 *		from the vision computer to the MVServer.
 * Author:	Rob Christy
 * CVS:		$Id: MVWT1VisionPacket.h,v 1.5 2003/08/30 03:22:46 waydo Exp $
 *		vim:ts=8:sts=4:sw=4
 */


// Constants
#define NUM_VEHICLES			16
#define MAX_FRAME_SKIP			20
#define DEFAULT_VISION_PORT		2001

// Type definition
typedef struct
{
    double timestamp;
    struct
    {
	double x;
	double y;
	double theta;
	double xdot;
	double ydot;
	double thetadot;
    } vehicle[NUM_VEHICLES];
} VisionPacket;

#endif
