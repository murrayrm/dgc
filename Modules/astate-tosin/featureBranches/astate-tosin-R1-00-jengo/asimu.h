/*
 * asdisp.dd - astate display table
 *
 * JML, EJC 
 *
 */

extern long long sparrowhawk;
#define D(x) (sparrowhawk)

/* Object ID's (offset into table) */


#ifdef __cplusplus
extern "C" {
#endif

/* Allocate space for buffer storage */
static char asimu_buf[304];

static DD_IDENT asimu[] = {
{1, 1, (void *)("IMU DEBUG INFO"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 1, (void *)("--------------------------------------------+----------------------------------"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 1, (void *)("Error Flags:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 36, (void *)("x gyro temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 1, (void *)("gyro Error:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 36, (void *)("y gyro temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 1, (void *)("accel Error:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 36, (void *)("z gyro temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 1, (void *)("gyro Temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 36, (void *)("x accl temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 1, (void *)("accel Temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 36, (void *)("y accl temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 1, (void *)("power Error:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 36, (void *)("z accl temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 6, (void *)("Meas    Avg     Off"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 36, (void *)("diode temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 1, (void *)("dvx:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 36, (void *)("receiv temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 1, (void *)("dvy:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 36, (void *)("bfs temp:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 1, (void *)("dvz:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 36, (void *)("-5V mon:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 36, (void *)("+5V mon:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 1, (void *)("dtx:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 36, (void *)("-15V mon:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 1, (void *)("dty:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 36, (void *)("+15V mon:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 1, (void *)("dtz:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 36, (void *)("analog gnd:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 8, (void *)("gain:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{18, 1, (void *)("-------------------------------------------------------------------------------"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 15, (void *)(&(D(gyroError))), dd_short, "%2d", asimu_buf+0, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(gyroError)", -1},
{5, 15, (void *)(&(D(accelError))), dd_short, "%2d", asimu_buf+8, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(accelError)", -1},
{6, 15, (void *)(&(D(gyroTmp))), dd_short, "%2d", asimu_buf+16, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(gyroTmp)", -1},
{7, 15, (void *)(&(D(accelTmp))), dd_short, "%2d", asimu_buf+24, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(accelTmp)", -1},
{8, 15, (void *)(&(D(powerError))), dd_short, "%2d", asimu_buf+32, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(powerError)", -1},
{3, 49, (void *)(&(D(xGyroTmp))), dd_double, "%6.4f", asimu_buf+40, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(xGyroTmp)", -1},
{4, 49, (void *)(&(D(yGyroTmp))), dd_double, "%6.4f", asimu_buf+48, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(yGyroTmp)", -1},
{5, 49, (void *)(&(D(zGyroTmp))), dd_double, "%6.4f", asimu_buf+56, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(zGyroTmp)", -1},
{6, 49, (void *)(&(D(xAccelTmp))), dd_double, "%6.4f", asimu_buf+64, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(xAccelTmp)", -1},
{7, 49, (void *)(&(D(yAccelTmp))), dd_double, "%6.4f", asimu_buf+72, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(yAccelTmp)", -1},
{8, 49, (void *)(&(D(zAccelTmp))), dd_double, "%6.4f", asimu_buf+80, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(zAccelTmp)", -1},
{9, 49, (void *)(&(D(diodeTmp))), dd_double, "%6.4f", asimu_buf+88, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(diodeTmp)", -1},
{10, 49, (void *)(&(D(receivTmp))), dd_double, "%6.4f", asimu_buf+96, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(receivTmp)", -1},
{11, 49, (void *)(&(D(bfsTmp))), dd_double, "%6.4f", asimu_buf+104, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(bfsTmp)", -1},
{12, 49, (void *)(&(D(minus5mon))), dd_double, "%6.4f", asimu_buf+112, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(minus5mon)", -1},
{13, 49, (void *)(&(D(plus5mon))), dd_double, "%6.4f", asimu_buf+120, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(plus5mon)", -1},
{14, 49, (void *)(&(D(minus15mon))), dd_double, "%6.4f", asimu_buf+128, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(minus15mon)", -1},
{15, 49, (void *)(&(D(plus15mon))), dd_double, "%6.4f", asimu_buf+136, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(plus15mon)", -1},
{16, 49, (void *)(&(D(analogGnd))), dd_double, "%6.4f", asimu_buf+144, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(analogGnd)", -1},
{10, 6, (void *)(&(D(imu_dvx))), dd_double, "%8.5f", asimu_buf+152, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_dvx)", -1},
{11, 6, (void *)(&(D(imu_dvy))), dd_double, "%8.5f", asimu_buf+160, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_dvy)", -1},
{12, 6, (void *)(&(D(imu_dvz))), dd_double, "%8.5f", asimu_buf+168, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_dvz)", -1},
{14, 6, (void *)(&(D(imu_dtx))), dd_double, "%8.5f", asimu_buf+176, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_dtx)", -1},
{15, 6, (void *)(&(D(imu_dty))), dd_double, "%8.5f", asimu_buf+184, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_dty)", -1},
{16, 6, (void *)(&(D(imu_dtz))), dd_double, "%8.5f", asimu_buf+192, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_dtz)", -1},
{10, 15, (void *)(&(D(imu_avx))), dd_double, "%8.5f", asimu_buf+200, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_avx)", -1},
{11, 15, (void *)(&(D(imu_avy))), dd_double, "%8.5f", asimu_buf+208, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_avy)", -1},
{12, 15, (void *)(&(D(imu_avz))), dd_double, "%8.5f", asimu_buf+216, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_avz)", -1},
{14, 15, (void *)(&(D(imu_atx))), dd_double, "%8.5f", asimu_buf+224, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_atx)", -1},
{15, 15, (void *)(&(D(imu_aty))), dd_double, "%8.5f", asimu_buf+232, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_aty)", -1},
{16, 15, (void *)(&(D(imu_atz))), dd_double, "%8.5f", asimu_buf+240, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_atz)", -1},
{10, 24, (void *)(&(D(imu_bvx))), dd_double, "%8.5f", asimu_buf+248, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_bvx)", -1},
{11, 24, (void *)(&(D(imu_bvy))), dd_double, "%8.5f", asimu_buf+256, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_bvy)", -1},
{12, 24, (void *)(&(D(imu_bvz))), dd_double, "%8.5f", asimu_buf+264, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_bvz)", -1},
{14, 24, (void *)(&(D(imu_btx))), dd_double, "%8.5f", asimu_buf+272, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_btx)", -1},
{15, 24, (void *)(&(D(imu_bty))), dd_double, "%8.5f", asimu_buf+280, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_bty)", -1},
{16, 24, (void *)(&(D(imu_btz))), dd_double, "%8.5f", asimu_buf+288, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_btz)", -1},
{17, 15, (void *)(&(D(imu_gain))), dd_double, "%6.5f", asimu_buf+296, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(imu_gain)", -1},
DD_End};

#ifdef __cplusplus
}
#endif

