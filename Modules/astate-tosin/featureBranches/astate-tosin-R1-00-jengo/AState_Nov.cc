/* AState_GPS.cc - Functions for dealing with the GPS
 *
 * JML 09 Jun 05
 *
 * */

#include "AState.hh"
#include "vehports.h"
void AState::novInit()
{

  unsigned long long rawNovTime;
  //If we are in replay mode, read in first nov reading.
  if (_astateOpts.useReplay == 1) {
    if (_metaState.imuEnabled == 1) {
      // Line up times on nov and imu logs.
      novLogStart = imuLogStart;
    } else {
      rawNov novIn;
      novReplayStream.read((char *)&novIn, sizeof(rawNov));
      
      rawNovTime = novIn.time;      
      novLogStart = rawNovTime;
    }
    DGClockMutex(&m_MetaStateMutex);
    _metaState.novEnabled = 1;
    DGCunlockMutex(&m_MetaStateMutex);
  } else {
    int tmp = nov_open(NOVPORT);

    DGClockMutex(&m_MetaStateMutex);
    _metaState.novEnabled = tmp;
    DGCunlockMutex(&m_MetaStateMutex);
    if(_metaState.novEnabled == -1)
      {
	cerr << "AState::novInit(): failed to open GPS" << endl;
      }
  }
}


void AState::novThread()
{

  int readStatus = 0;

  novDead = 0;

  /* Definition moved to AState.hh for Sparrow access
 
    int gpsValid; 

  */

  char *novReadBuffer;

  if ((novReadBuffer = (char *) malloc(2000)) == NULL)
    {
      cerr << "AState::novThread(): memory allocation error" << endl;
    }

  //Temporary time variable
  unsigned long long nowTime;
  unsigned long long rawNovTime;

  //Variables for reading in raw data
  rawNov novIn;
  rawNov novOut;
  novDataWrapper wrappedNovData;

  spGPSDisable = 0;
	
  while (!quitPressed) {
    if (_astateOpts.useReplay == 1) {
      if (!novReplayStream) {
	return;
      }
      //If in replay mode read in from stream then wait
      //until we've reached time we would have read point

      novReplayStream.read((char *)&novIn, sizeof(rawNov));
      rawNovTime = novIn.time - novLogStart + startTime;

      if (_astateOpts.fastMode == 0) {
	DGCgettime(nowTime);
	while (rawNovTime > nowTime) {
	  usleep(10);
	  DGCgettime(nowTime);
	}
      }
      memcpy(&(wrappedNovData.data), &(novIn.data), sizeof(novData));
    } else {
      //Otherwise do an actual read.
      readStatus = nov_read(novReadBuffer, 1000);  //Need to determine if this is blocking...
      if (readStatus == -1) {
	novDead = 1;
	free(novReadBuffer);
	return;
      }
      DGCgettime(rawNovTime); // Time stamp as soon as data read.

      if (wrappedNovData.update_nov_data(novReadBuffer) == -1) {
	//Ignore non position/velocity value
	continue;
      }

      if (_astateOpts.logRaw == 1) {
	novOut.time = rawNovTime;
	memcpy(&(novOut.data), &(wrappedNovData.data), sizeof(novData));
	novLogStream.write((char *)&novOut, sizeof(rawNov));
      }

    }

    
    if (wrappedNovData.data.sol_status == 0) {
      novValid = 1;
    } else {
      novValid = 0;
    }

    if (spGPSDisable == 1) {
      continue;
    }
    
    DGClockMutex(&m_HeartbeatMutex);
    _heartbeat.nov = true;

    if(novValid) {
      _heartbeat.novValid = true;
    }
    DGCunlockMutex(&m_HeartbeatMutex);
    
    if (novValid) {


      DGClockMutex(&m_NovDataMutex);

      if (((novBufferReadInd + 1) % NOV_BUFFER_SIZE) == (novBufferLastInd % NOV_BUFFER_SIZE)) {
	novBufferFree.bCond = false;

	DGCunlockMutex(&m_NovDataMutex);

	DGCWaitForConditionTrue(novBufferFree);
      

	DGClockMutex(&m_NovDataMutex);
      }

      ++novBufferReadInd;
      
      memcpy(&(novIn.data),&(wrappedNovData.data), sizeof(novData));
      novIn.time = rawNovTime;

      memcpy(&(novBuffer[novBufferReadInd % NOV_BUFFER_SIZE]), &novIn, sizeof(rawNov));
      
      DGCunlockMutex(&m_NovDataMutex);    

      DGCSetConditionTrue(newData);
    }
    
    // Occasionally flush the buffer
    if(novBufferReadInd % 100 == 1 && _astateOpts.logRaw) {
      novLogStream.flush();
    }
  }
  free(novReadBuffer);
}



