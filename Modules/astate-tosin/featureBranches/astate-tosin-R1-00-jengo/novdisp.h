/*
 * novdisp.dd - detailed information about the Novatel GPS
 *
 * RMM, 22 Aug 06
 *
 */

extern long long sparrowhawk;
#define D(x) (sparrowhawk)

/* Object ID's (offset into table) */


#ifdef __cplusplus
extern "C" {
#endif

/* Allocate space for buffer storage */
static char novatel_buf[16];

static DD_IDENT novatel_tbl[] = {
{1, 1, (void *)("Novatel GPS"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 1, (void *)("Northing:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 1, (void *)("Easting:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 11, (void *)(&(D(novNorth))), dd_double, "%11.3f", novatel_buf+0, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(novNorth)", -1},
{4, 11, (void *)(&(D(novEast))), dd_double, "%11.3f", novatel_buf+8, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(novEast)", -1},
{6, 1, (void *)("EXIT"), dd_label, "NULL", (char *) NULL, 1, dd_exit_loop, (long)(long) 0, 0, 0, Button, "", -1},
{6, 11, (void *)("RETURN"), dd_label, "NULL", (char *) NULL, 1, dd_prvtbl_cb, (long)(long) 0, 0, 0, Button, "", -1},
DD_End};

#ifdef __cplusplus
}
#endif

