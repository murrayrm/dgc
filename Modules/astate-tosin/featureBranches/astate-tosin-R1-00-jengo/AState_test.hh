#ifndef ASTATE_TEST_HH
#define ASTATE_TEST_HH

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "interfaces/VehicleState.h"
#include "skynet/sn_msg.hh"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"

class AState_test : public CStateClient {

	fstream testlogfile;
	fstream pathlogfile;
	fstream interplogfile;

	unsigned long long starttime;
	unsigned long long nowtime;
	double timecalc;

public:
	AState_test(int);
	void Active();
};

#endif
