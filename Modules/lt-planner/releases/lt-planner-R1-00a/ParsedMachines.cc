// ParsedMachines.cc
// Define a few environments and systems for ParsedSMs.
// By Nicholas Fette.
// see DGC07:User:Nfette/Summer 2008

#include "ParsedMachines.hh"
//#include <temp-planner-interfaces/PlannerInterfaces.h>

// start implementing TrafficStop.

TrafficSM* TrafficSM::getDefaultTrafficSM() {
  ParsedSM *sam = new ParsedSM("default.psm");
  sam->init();
  TrafficStop *ts = new TrafficStop(sam);
  ts->destroySamOnExit = true;
  return ts;
}

