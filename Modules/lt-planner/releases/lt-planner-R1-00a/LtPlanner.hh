// File: LtPlanner.hh
// Created by Nicholas Fette, July 2008,
// echoing mod-log-planner setup.
#ifndef _LT_PLANNER_HH_
#define _LT_PLANNER_HH_

 /********************************
 * Includes for things I may need
 *********************************/
//#include "interfaces/VehicleState.h"
#include "temp-planner-interfaces/PlannerInterfaces.h" // for StateProblem_t
//#include <temp-planner-interfaces/ConfigFile.hh>
//#include <temp-planner-interfaces/Console.hh>
//#include <temp-planner-interfaces/Log.hh>
//#include <dgcutils/cfgfile.h>
//#include "ParsedMachines.hh"    // The other white meat

 /********************************
 * Class definitions
 *********************************/

// Wrapper for traffic logic handlers, possibly based on linear temporal logic.
class LtPlanner
{
  
  public:

  /*! Constructor */
  LtPlanner();

  /*! Destructor */
  virtual ~LtPlanner();
  
  /*! Initialization, just for backward compatibleness */  
  void init();

  /*! Alternative destructor, just for backward compatibleness */
  void destroy();
  
  /*! Read input config file and store in class variables  */
  void readConfig();

  //! Planner might ask for this. Don't know what it does yet.
  void resetState();

  //! Planner might ask for this also.
  void updateDisplayInfo();
  
  /*! Do the actual logic planning!  */
  Err_t planLogic(vector<StateProblem_t> &problems, PlanGraph *graph,
    Err_t prevErr, VehicleState &vehState, Map *map, Logic_params_t &params,
    int& currentSegmentId, int& currentLaneId, bool& replanInZone);

  private:
  // Finite State Machine current status
  StateProblem_t current_status;

  // Alternative finite state machine.
  //TrafficSM *tsm;

  // This one is only needed for development.
  long cycle_counter;

  // Configuration parameters.
  // int SM_MODE;

};


#endif // _LT_PLANNER_HH_
// EOF '  LtPlanner.hh'
