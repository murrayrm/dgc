// ParsedMachines.hh
// Define a few environments and systems for ParsedSMs.
// By Nicholas Fette.
// see DGC07:User:Nfette/Summer 2008

#ifndef _PARSED_MACHINES_HH_
#define _PARSED_MACHINES_HH_
#include "ParsedSM.hh" // Defines ParsedSM, SMState, Environment, and System.
#include <cmath> // For pow(base, exp) in hash.
#include <map>   // For, well, you guessed it.

// Unfortunately, I can't compile standalone any longer.
#include <temp-planner-interfaces/PlannerInterfaces.h>

using namespace std;

// Helps make a map of the input functions.
struct strCmp {
  bool operator() (const string a, const string b) const {
    return (strcmp(a.c_str(), b.c_str()) > 0);
  }
};

typedef struct {
  vector<StateProblem_t*> *problems;
  PlanGraph *graph;
  Err_t prevErr;
  VehicleState *vehState;
  Map *map;
  Logic_params_t *params;
  int *currentSegmentId;
  int *currentLaneId;
  bool *replanInZone; // Not needed right now.
} TrafficData;

class TrafficSM {
  public:
    TrafficSM();
    virtual ~TrafficSM();
    virtual Err_t planLogic(TrafficData *tada) = 0; 
    StateProblem_t current_status;
    static TrafficSM* getDefaultTrafficSM();
};

// A basic implementation of a TrafficSM setup with a ParsedSM.
class TrafficStop : public TrafficSM, public Environment, public System {
  private:
    ParsedSM *sam;
    map<const string, bool (*)(TrafficData *td), strCmp> inputMap;
    TrafficData *tada;
  public:
    bool destroySamOnExit;
    TrafficStop(ParsedSM *psm);
    ~TrafficStop();
    // Gets called by LtPlanner.
    Err_t planLogic(TrafficData *tada); 
    // Override inherited methods for Environment.
    vector<string> getInputNames();
    vector<bool> getInputs();
    vector<bool> getInputs(vector<string> varnames);
    // Override inherited methods for System.
    void execute(vector<bool> outputVals, vector<string> outputNames);
    // Tests that will become state machine input.
    // Let's try to not make these static (later).
    static bool isJustBeforeStopSign(TrafficData *td);
    static bool isNowStopped(TrafficData *td);
};

// Example of an Environment and System (need not be in the same class)
// for running a counter or timer and displaying the result.
class timerGame : public Environment, public System {
  protected:
    int timevar;
    vector<bool> lastVals;
    vector<string> lastNames;
  public:
    timerGame() : Environment(), System() {
      timevar = 0;
    }
    ~timerGame() {}
    // for Environment
    vector<bool> getInputs(vector<string> varnames);
    // for System
    void execute(vector<bool> outputVals, vector<string> outputNames);
    ostream & displayLastCall(ostream &os) const;
};

// Allow a user to play the environment, for testing purposes.
class manualStepper: public Environment {
  private:
    istream *is;
  public:
    manualStepper(istream *input) : Environment(), is(input) { }
    ~manualStepper() { }

    // Wait for a user to enter inputs through the input stream.
    // Each character is treated as a bit.
    // '0' and ' ' are treated as zero.
    // Anything else is treated as one.
    vector<bool> getInputs() {
      vector<bool> result;
      string s;
      bool bit;
      if (!getline (*is, s)) {
        throw SMException("File is over");
      }
      for (int i = 0; i < (int)s.length(); i++) {
        bit = !(s[i] == '0' || s[i] == ' ');
        result.push_back(bit);
      }
      // User might want to know that these are correct. Or not, hey.
      //cout << "Your input was " << result << endl;
      return result;
    }
    vector<bool> getInputs(vector<string> varnames) {
      // Maybe I should prompt first? Not now, anyway.
      return getInputs();
    }
};

#endif // _PARSED_MACHINES_HH_
// EOF ParsedMachines.hh
