/*!
 * \file TrafficSM.hh
 * \brief Define the interface for TrafficSM.
 *
 * \author Nicholas Fette
 * \date July 2008.
 * 
 * \ingroup TrafficSM
 *
 * see DGC07:Lt-planner
 */

#ifndef _TRAFFIC_SM_HH_
#define _TRAFFIC_SM_HH_
#include "ParsedSM.hh" // Defines ParsedSM, SMState, Environment, and System.
// REMOVE #include <cmath> // For pow(base, exp) in hash.
#include <map>   // For, well, you guessed it.

// Unfortunately, I can't compile standalone any longer.
#include <temp-planner-interfaces/PlannerInterfaces.h>

using namespace std;

//! Helps make a map of the input functions.
struct strCmp {
  bool operator() (const string a, const string b) const {
    return (strcmp(a.c_str(), b.c_str()) > 0);
  }
};

//! Helps pass data from Planner to LtPlanner to TrafficSM.
typedef struct {
  //! A vector with usually one scenario for the path planner.
  vector<StateProblem_t*> *problems;
  //! The graph of something.
  PlanGraph *graph;
  //! The errors from the previous Planner cycle.
  Err_t prevErr;
  //! The state of the vehicle. (Maybe position, transmission, steering?)
  VehicleState *vehState;
  //! Map represents the conglomeration of sensing data which should
  //! represent the surrounding landscape.
  Map *map;
  //! Just some extra, but useful stuff added on by Planner.
  Logic_params_t *params;
  //! According to MissionPlanner, where we are driving, as in 1.2.1.
  int *currentSegmentId;
  //! According to MissionPlanner, which lane we drive in, as in 0, 1.
  int *currentLaneId;
  //! Whether to do something vague, according to Jeremy. (ignore that)
  bool *replanInZone; // Not needed right now.

  // "Derived spatial data" (Stefan)
  double currentVelocity;
  double stoplineDistance;
  double obstacleDistance;
  double exitDistance;
  double obstacleOnPath;
  double obstacleInLane;
  double stoppingDistance;
  double obsSide;
  double obsRear;
  double obsFront;
  LaneLabel currentLane;
  LaneLabel desiredLane;
  
  //! Based on Console input, should we freeze the state machine?
  bool freeze;

} TrafficData; // for inputs

//! Intermediate between state machine and me, holding output variables.
typedef struct {
  // Elements of a StateProblem_t.
  double         probability; // in [0, 1], such as 0.99
  FSM_state_t    state;       // DRIVE STOP_INT STOP_OBS BACKUP UTURN PAUSE
  FSM_region_t   region;      // ZONE_REGION ROAD_REGION (INTERSECTION)
  FSM_flag_t     flag;        // PASS NO_PASS OFFROAD PASS_REV
  FSM_planner_t  planner;     // RAIL_PLANNER S1~ D~ CIRCLE_~ RRT_~
  FSM_obstacle_t obstacle;    // OBSTACLE_SAFETY ~_AGGRESSIVE ~_BARE
  // Whether to generate a new graph on the road.
  bool planFromCurrPos;
  // Whether to generate a new graph in the zone.
  bool replanInZone;
  // Message to send to planner, who called planLogic.
  Err_t          error;       // Such as LP_OK

  // Set all the fields in the argument, like a memcpy.
  void toStateProblem_t(StateProblem_t* p) {
    p->probability = probability;
    p->state       = state;
    p->region      = region;
    p->flag        = flag;
    p->planner     = planner;
    p->obstacle    = obstacle;
  }
} TSMout;

// Usage, for example: "EnvFunctor a = TrafficData::getCharFromVoid;".
typedef char (TrafficData::*EnvFunctor) (void);

//! A generic representation of a state machine for planning traffic logic.
class TrafficSM {
  protected:
    //! Map from input names to boolean function pointers.
    map<string, EnvFunctor, strCmp> envMap;
    //! Default input value in case an input name is not recognized.
    EnvFunctor envDefaultInput;
    //! Fast access to ordered input functions.
    //! You must setInputNames before using this.
    vector<EnvFunctor> envInputFuncs;
    //! Map from output names to boolean values.
    map<string, char, strCmp> sysMap;
    //! Stores state machine's output variables in a more useful form.
    //TSMout to;
  public:
    //! Create a Traffic State Machine that can't do anything.
    TrafficSM();
    //! Destroy a Traffic State Machine (using high/noisy voltage).
    virtual ~TrafficSM();
    //! Make state transitions and prepare output. (Name is legacy.)
    virtual Err_t planLogic(TrafficData *tada) = 0; 
    //! The most recent, uh, status.
    StateProblem_t current_status;
    //! Expose inner workings.
    TSMout to;
    virtual string getStateString() = 0;
    static char *DEFAULT_PSM_FILE;
    static TrafficSM* getDefaultTrafficSM();
    string intersectionStringForConsole;
};

//! A basic implementation of a TrafficSM setup with a ParsedSM.
class TrafficStop : public TrafficSM, public Environment, public System {
  private:
    ParsedSM *sam;
    //! How the Environment evaluates its input variables.
    //! Each valid name is mapped to a functor.
    //! Valid names are assigned in the constructor.
    map<const string, char (*)(TrafficData *td), strCmp> inputMap;
    TrafficData *tada;
  public:
    bool destroySamOnExit;
    TrafficStop(ParsedSM *psm);
    ~TrafficStop();
    //! Gets the state machine which is supposed to be hidden.
    ParsedSM* getPSM();
    //! Gets a string which conveys information about state.
    string getStateString();

    //! Gets called by LtPlanner. In turn calls execute().
    Err_t planLogic(TrafficData *tada); 

    //! Override inherited methods for Environment.
    const vector<string> &getInputNames();
    const vector<char> &getInputs();
    const vector<char> &getInputs(const vector<string>& varnames);

    //! Override inherited methods for System.
    void execute(const vector<char>& outputVals,
      const vector<string>& outputNames);

    //! Tests that will become state machine input.
    // Let's try to not make these static (later).
    static char isJustBeforeStopSign(TrafficData *td);
    static char isNowStopped(TrafficData *td);

    //! Not very well-defined, is it?
    static char isObstacleInWay(TrafficData *td);

    //! Straight out of gcinterface/SegGoals/SegmentType.
    static char isSegmentRoad(TrafficData *td);
    static char isSegmentZone(TrafficData *td);
    static char isSegmentIntersection(TrafficData *td);
    static char isSegmentPreZone(TrafficData *td);
    static char isSegmentUTurn(TrafficData *td);
    static char isSegmentDriveAround(TrafficData *td);
    static char isSegmentBackUp(TrafficData *td);
    static char isSegmentEndOfMission(TrafficData *td);
    static char isSegmentEmergencyStop(TrafficData *td);
    static char isSegmentUnknown(TrafficData *td);
    static char isSegmentStartChute(TrafficData *td);

    //! Straight out of gcinterface/SegGoals/IntersectionType.
    static char isIntersectionStraight(TrafficData *td);
    static char isIntersectionLeft(TrafficData *td);
    static char isIntersectionRight(TrafficData *td);

    //! Deliver us from error. temp-planner-interfaces/PlannerInterfaces.h
    static char isErrorNone(TrafficData *td);
    static char isErrorNoPath(TrafficData *td);
    
    //! Let's use enum types, or something similar, everybody!
    //! These give only internal values, as given by synthesis.
    static char whichSegment(TrafficData *td);
    static char whichIntersection(TrafficData *td);

    //! Is there an obstacle close but not dangerously so?
    static char isObsCozy(TrafficData *td);
    //! Is there an obstacle dangerously close?
    static char isObsTooClose(TrafficData *td);
    //! Is the previous path going to collide with something?
    static char isErrorCollision(TrafficData *td);
    
};


#endif // _TRAFFIC_SM_HH_
// EOF TrafficSM.hh
