/*!
 * \file ParsedSM.cc
 * \brief Forms a generic state machine parsed from text description file.
 *
 * \author Nicholas Fette
 * \date 14 July 2008
 *
 * \ingroup ParsedSM
 *
 */


/*********************************
 * Include header
 *********************************/
#include "ParsedSM.hh"
#include "ParseUtils.hh"         // for ParseUtils static object.
#include "ParseUtilsInternal.hh" // for operator<<(ostream&, vector<T>&)

#define POUT cerr

/*********************************
 * Methods
 *********************************/

ParsedSM::ParsedSM(char *file) : SMfile(file), fin(SMfile), freeze(false) {
  env = NULL;
  sys = NULL;
}

ParsedSM::~ParsedSM() {
  // clean up memory
  // (nothing to do)
}

// Parse a state machine text file,
// as described in DGC07:User:Nfette/Summer 2008.
// Also create the data structure to represent it.
void ParsedSM::init() {
  PULOGF(1) << endl;
  // Check that file exists.
  if (!fin) {
    PULOGL(-1) << "Couldn't find file " << SMfile << endl;
    throw SMException("Couldn't find file \"" + string(SMfile) + "\"");
  }
  PULOGL(0) << "Opened an input file stream on " << SMfile << endl;

  // Find header
  string s;
  if (!getline(fin, s) || (s.find("State machine") == string::npos
      && s.find("state machine") == string::npos) ) {
    PULOGL(-2) << "Incorrect first line: should have \"State machine\"." << endl;
    throw SMException("Incorrect first line: should have \"State machine\".");
  }
  PULOGL(9) << "Found the correct header: " << s << endl;

  // Build a queue of all the tokens in the file.
  // For each statement, do something to store the data.
  int tokens;
  vector<string> args;
  vector<string>::iterator iter;
  vector<char>::iterator iterb;
  int ii;
  int argi;
  string whichStatement;
  bool gettingOutputs;

  // Just temporarily stick the values into these variables.
  // (Need to move these declarations closer to their usage, later)
  int error;
  int initial;
  vector<string> inputs;
  vector<string> outputs;
  int state;
  int errorOverride;
  bool isErrorOverride;
  int successorCount;
  vector<int> successors;
  string valuationString;
  vector<char> valuation;
  vector<char> valIn;
  vector<char> valOut;
  inputCount = 0;
  outputCount = 0;

  while ((tokens = readStatement(args)) >= 0) {
    PULOGL(10) << "Interepreting the statement: ";
    for (iter = args.begin(); iter!=args.end(); iter++) {
      PULOG(10) << *(iter) << "~";
    }
    PULOG(10) << endl;

    whichStatement = args.front();

    // Select the appropriate statement type.
    if (whichStatement == "Version") {
      version = args.at(2);
      PULOGL(2) << "Using the version \"" << args.at(2) << "\"" << endl;

    } else if (whichStatement == "Name") {
      name = args.at(2);
      // Display parsed info.
      PULOGL(2) << "Using the name \"" << args.at(2) << "\"" << endl;

    } else if (whichStatement == "Inputs") {
      // POUT << "Going to count input variables by name." << endl;
      for (iter = args.begin() + 3; iter != args.end(); iter++) {
        if (string(*iter) == "}") { break; }
        inputCount++;
        inputNames.push_back(*iter);
      }
      PULOGL(9) << "Counted input variables: " << inputCount << endl;
      // Display parsed info.
      PULOGL(2) << "Using the inputs ";
      for (ii = 0; ii < inputCount; ii++) {
        PULOG(2) << inputNames[ii] << (( ii == inputCount - 1)? "." : ", " );
      }
      PULOG(2) << endl;
      
    } else if (whichStatement == "Outputs") {
      for (iter = args.begin() + 3; iter != args.end() && *iter!="}"; iter++) {
        //if (string(*iter) == "}") { break; }
        outputCount++;
        outputNames.push_back(*iter);
      }
      // Display parsed info.
      PULOGL(2) << "Using the outputs ";
      for (ii = 0; ii < outputCount; ii++) {
        PULOG(2) << outputNames[ii] << (( ii == outputCount - 1 )? "." : ", " );
      }
      PULOG(2) << endl;

    } else if (whichStatement == "Error") {
      error = atoi(args.at(2).c_str());
      errorID = error;
      // Display parsed info.
      PULOGL(2) << "Setting the error state to " << error << "." <<  endl;

    } else if (whichStatement == "Initial") {
      initial = atoi(args.at(2).c_str());
      initialID = initial;
      // Display parsed info.
      PULOGL(2) << "Setting the initial state to " << initial << "." << endl;

    } else if (whichStatement == "State") {
      state = atoi(args.at(3).c_str());
      // Get override error successor.
      int offset = 0;
      isErrorOverride = false;
      errorOverride = errorID;
      if (args.at(5) != "]") {
        isErrorOverride = true;
        errorOverride = atoi(args.at(5).c_str());
        offset++;
      }
      // Get successors.
      successors.clear();
      successorCount = 0;
      for (argi = 7 + offset; argi < (int)args.size(); argi++) {
        if (args.at(argi) == "}") {
          break;
        }
        successors.push_back(atoi(args.at(argi).c_str()));
        successorCount++;
      }

      // Get valuation of state variables inside brackets, like <10/00 1>.
      valuationString = "";
      for (argi = argi + 2; args.at(argi).at(0) != '>'; argi++) {
        valuationString += args.at(argi);
      }
      PULOGL(9) <<"valuationString looks like \"" <<valuationString <<"\"" <<endl;
      valuation.clear();
      valIn.clear();
      valOut.clear();

      gettingOutputs = false;
      for (ii = 0; ii < (int)valuationString.length(); ii++) {
        char c = valuationString.at(ii);
        gettingOutputs |= (c == '/');
        if (isdigit(c)) {
          char valkyrie = atoi(&c);
          if (gettingOutputs) {
            valOut.push_back(valkyrie);
          } else {
            valIn.push_back(valkyrie);
          }
          valuation.push_back(valkyrie);
        }
      }

      // Create the data structure for the state and add it to astate.
      SMState newstate(this, state, errorOverride, successors, valIn, valOut);
      astate.push_back(newstate);

      // Display parsed info.
      PULOGL(5) << "Adding a state number " << args.at(3) << "." << endl;
      if (isErrorOverride) {
        PULOGD(6) <<"With error override state "<<errorOverride<<"."<<endl;
      }
      PULOGD(6) << "With successors ";
      for (ii = 0; ii < (int)successors.size(); ii++) {
        PULOG(6)<<successors.at(ii)<<((ii==(int)successors.size()-1)?".":", ");
      }
      PULOGL(6) << endl << "With valuation ";//<< valuationString << ", aka ";
      for (iterb = valIn.begin();
        iterb != valIn.end(); iterb++) {
        PULOG(6) << (int)*iterb;
      }
      PULOG(6) << "/";
      for (iterb = valOut.begin();
        iterb != valOut.end(); iterb++) {
        PULOG(6) << (int)*iterb;
      }
      PULOG(6) << "." << endl;

    } else if (whichStatement == "End") {
      // Display parsed info.
      PULOGL(1) << "End of state machine." << endl
        << "  Final state was #" << state << "." << endl;

    } else {
      // Display parsed info.
      string unknownCommandString = "";
      PULOGL(-2) << "Unknown command sequence:";
      for (iter = args.begin(); iter != args.end(); iter++) {
        PULOGL(-2) << *iter << " ";
        unknownCommandString += *iter + " ";
      }
      PULOGL(-2) << endl;
      unknownCommandString = "ParsedSM.init(): Unknown command sequence: \""
        + unknownCommandString + "\"";
      throw SMException(unknownCommandString);
    } // select command type
    
    // Make sure I didn't leave any tokens from this statement.
    args.clear();
  } // while reading tokens
  fin.close();

  // Should now have the data structure assembled.
  // Initialize the current state:
  trace.push_back(initialID);
} // init()

char * ParsedSM::getSMfile() {
  return SMfile;
}

int ParsedSM::readStatement(vector<string> &args) {
  
  int tokenCounter = -1;
  string s;
  string::size_type search;
  // Loop over lines and separate tokens
  while(getline(fin, s) || leftover.length() > 0) {
    // Remove comments
    search = s.find("#", 0);
    if (search != string::npos) {
      s.erase(search);
    }
    // Add to old string.
    // I have a feeling that getline chops the trailing newline, which I need.
    s = leftover + s + " ";
    //POUT << "Looking at the string \"" << s << "\"." << endl;
    while (s.erase(0, s.find_first_not_of(" \t\n")).length() > 0) {
      // match against possible tokens, store and go on.
      //POUT << "String is now \"" << s << "\"." << endl;
      if ((search = s.find_first_of(":;<>{}[]()")) == 0) {
        // Consider as a special character.
        //POUT << "Got special char: \"" << s.substr(0, 1) << "\"" << endl;
        args.push_back(s.substr(0, 1));
        s.erase(0, 1);
        tokenCounter++;
        if (args.back() == ";") {
          //POUT << "Reached the end of a statement." << endl;
          leftover = s;
          return tokenCounter;
        }
      } else if ((search = s.find_first_of(" \t\n\v\f\r:;<>{}[]()"))
        != string::npos && search > 0) {
        //POUT << "Found the token \"" << s.substr(0, search) << "\"." << endl;
        args.push_back(s.substr(0, search));
        s.erase(0, search);
        tokenCounter++;
      } else {
        // Didn't find a match this time around,
        // maybe there is a token without an ending.
        args.push_back(s);
        s.erase();
        tokenCounter++;
        break;
      }
    }
    leftover = s;
  }
  // What to do if file is over?
  return tokenCounter;
}

int ParsedSM::getInputCount() const {
  return inputCount;
}

int ParsedSM::getOutputCount() const {
  return outputCount;
}

const vector<string>& ParsedSM::getInputNames() const {
  return inputNames;
}

const vector<string>& ParsedSM::getOutputNames() const {
  return outputNames;
}

void ParsedSM::setEnvironment(Environment *arg) {
  env = arg;
}

void ParsedSM::setSystem(System *arg) {
  sys = arg;
}

// Cause the state machine to read inputs, transition, and call output.
void ParsedSM::step() {
  // 0. State should have been initialized by a call to init().
  //    However, no action was taken in the initial state.

  // 1. Evaluate input and find a matching successor.
  const vector<char> &newInputs = env->getInputs(getInputNames());
  // Let them know the truth.
  ParseUtils::getLogger() << "LTP->tsm->sam Inputs: ";
  ParseUtils::setVectorDelims("<", ">", " ");
  ParseUtils::displayVectorChar(ParseUtils::getLogger(), newInputs) << endl;

  // 2. Identify current state's successors.
  int oldState = trace.back();
  vector<int> newSuccs = astate[trace.back()].getSuccessors();
  int newState = astate[trace.back()].getErrorID();
  for (unsigned int i = 0; i < newSuccs.size(); i++) {
    if (astate.at(newSuccs[i]).getInputVector() == newInputs) {
      newState = newSuccs[i];
      break;
    }
  }

  // 3. Update state to it, or to the error state for this state.
  // Uh-oh, memory-oh!
  if (freeze) { newState = oldState; }
  trace.push_back(newState);

  // 4. Execute system on new output state.
  sys->execute(astate.at(newState).getOutputVector(), getOutputNames());

}

// Get the execution history, ie the most recent state id is pushed back.
vector<int> ParsedSM::getTrace() {
  return trace;
}

// Get the most recent state id.
int ParsedSM::getState() {
  return trace.back();
}

ostream& ParsedSM::display(ostream &os) const {
  // Decide whether to print input format or dot graph.
  if (1) {
    os << "State machine" << endl;
    os << "Name: " << name << ";" << endl;
    os << "Version: " << version << ";" << endl;
    ParseUtils::setVectorDelims("{", "}", " ");
#ifndef VECTOR_OFF
    os << "Inputs: " << inputNames << ";" << endl;
    os << "Outputs: " << outputNames << ";" << endl;
#else
    os << "Inputs: ";
    ParseUtils::displayVectorString(os, inputNames);
    os << ";" << endl;
    os << "Outputs: ";
    ParseUtils::displayVectorString(os, outputNames);
    os << ";" << endl;
#endif
    os << "Error: " << errorID << ";" << endl;
    os << "Initial: " << initialID << ";" << endl;
    for (int i = 0; i < (int)astate.size(); i++) {
      os << "State: " << astate[i] << ";" << endl;
    }
    os << "End" << endl;
  } else {
    os << "Not implemented, clearly." << endl;
  }
  return os;
}

ostream& ParsedSM::displayState(ostream &os) const {
  astate[trace.back()].display(os);
  return os;
}

ostream & SMState::display(ostream &os) const {
  os << "(" << myID << " [";
  if (parent->getErrorID() != myErrorID) {
    os << myErrorID;
  } else {
    os << " ";
  }
  os << "] ";

  ParseUtils::setVectorDelims("{", "}", " ");
#ifndef VECTOR_OFF
  os << succ;
  ParseUtils::setVectorDelims("", "", "");
  os << " <";
  os << ivars << "/" << ovars;
  os << "> )";
#else
  ParseUtils::displayVectorInt(os, succ);
  ParseUtils::setVectorDelims("", "", "");
  os << " <";
  ParseUtils::displayVectorChar(os, ivars); // note cast
  os << "/";
  ParseUtils::displayVectorChar(os, ovars); // note cast
  os << "> )";
#endif
  return os;
}

ostream & operator<<(ostream &os, const SMState &arg) {
  return arg.display(os);
}

ostream & operator<<(ostream &os, const ParsedSM &arg) {
  return arg.display(os);
}

// EOF ParsedSM.cc
