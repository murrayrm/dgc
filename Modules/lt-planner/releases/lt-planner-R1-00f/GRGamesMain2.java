import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigInteger;

import net.sf.javabdd.BDD;
import edu.wis.jtlv.env.Env;
import edu.wis.jtlv.env.module.ModuleWithWeakFairness;
import edu.wis.jtlv.lib.games.GameException;


/**
 * GR = general reactivity.
 * Using synthesis algorithm,
 * @author yaniv
 * has created a game.
 */
public class GRGamesMain2 {

  public static int LOAD_SMV = 1;
  public static int LOAD_FDS = 2;
  
  /**
   * main run for the game.
   * 
   * @param args
   */
  @SuppressWarnings("deprecation")
  public static void main(String[] args) {
    int loadMode = LOAD_SMV;
    
    // Parse command line arguments.
    boolean flagHelp = false;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("--help") || args[i].equals("-h")) {
        flagHelp = true;
      }
    }
    
    // For instance, these are reasonable.
    String input_file = "arbiter2_with_spec.smv"; // What? I never use that notation.
    String mainModule = "main";
    String envModule  = "main.e";
    String sysModule  = "main.s";
    int ninputs = 2;
    int noutputs = 2;
    String psmFile = "";
    String dotFile = "";

    // Show no tolerance for wrong number of arguments.
    if (args.length >= 6) {
      input_file = args[0];
      mainModule = args[1];
      envModule  = args[2];
      sysModule  = args[3];
      ninputs = Integer.parseInt(args[4]);
      noutputs = Integer.parseInt(args[5]);
      if (args.length >= 7) {
    	  psmFile = args[6];
      }
      if (args.length >= 8) {
    	  dotFile = args[7];
      }
    } else if (args.length != 0) {
      flagHelp = true;
    }

    if (flagHelp) {
      System.out.println(
        "Usage: java GRGamesMain2 [--help | file.smv main main.e main.s\n"
        + "                                 ninputs noutputs [file.psm file.dot]]\n"
        + "      Synthesize a state machine from a temporal logic spec.\n"
        + "      written in the SMV format. The output is various information\n"
        + "      and you find what you need. (Later, we can add output file\n"
        + "      arguments.) main, main.e, and main.s are respectively the\n"
        + "      full names of the main, environment, and system modules.\n"
        + "      The --help argument prints this information. No arguments\n"
        + "      runs the example \"arbiter2_with_spec.smv\".\n");
      return;
    }

    ModuleWithWeakFairness cx;
    ModuleWithWeakFairness player1;
    ModuleWithWeakFairness player2;
    
    try {
      
      if (loadMode == LOAD_SMV) {
        // 1. SMV 
        System.out.println("== Strating synthesis algorithm ==");
        System.out.println("Treating input file as SMV.");
        Env.loadModule(input_file);
        cx = (ModuleWithWeakFairness) Env.getModule(mainModule);
        player1 = (ModuleWithWeakFairness) Env.getModule(envModule);
        player2 = (ModuleWithWeakFairness) Env.getModule(sysModule);
        System.out.println("Module main looks like:");
        System.out.println(Env.toNiceString(cx, cx.trans()));
      } else if (loadMode == LOAD_FDS) {
        // 2. FDS example
        // input_file = "./testcases/arbiter2_with_spec.fds";
        System.out.println("== Starting synthesis algorithm ==");
        System.out.println("Treating input file as FDS.");
        Env.loadModule(input_file);
        cx = null;
        player1 = (ModuleWithWeakFairness) Env.getModule("env");
        player2 = (ModuleWithWeakFairness) Env.getModule("sys");
      } else {
        System.err.println("Error: Invalid loading mode! Shouldn't happen.");
        return;
      }

      System.out.println("== Loaded players ==");
      System.out.println("'''Player 1: Environment: " + envModule + "'''");
      System.out.println(player1);
      System.out.println("'''Player 2: System: " + sysModule + "'''");
      System.out.println(player2);

      System.out.println("== Constructing and playing the game ==");
      GRGame2 g = new GRGame2(player1, player2);
      System.out.println("Player sys winning states are:");
      System.out.println(Env.toNiceString(cx, g.sysWinningStates()));
      System.out.println("Player env winning states are:");
      System.out.println(Env.toNiceString(cx, g.envWinningStates()));

      BDD all_init = g.getSysPlayer().initial().and(
          g.getEnvPlayer().initial());
      BDD counter_exmple = g.envWinningStates().and(all_init);
      if (!counter_exmple.isZero()) {
        System.out.println("Specification is unrealizable!");
        System.out.println("The env player can win from states:");
        System.out.println("\t" + Env.toNiceString(cx, counter_exmple));
        return;
      }
      // Else proceed.
      System.out.println("Specification is realizable.");
      System.out.println("== Building an implementation ==");
      
      /* As of 26 June 2008, there is no way to get any information
       * out of this method, and it does not modify the players!
       * I need Yaniv to output the strategy into an SMV or 
       * something so that I can use it, at least. (User error:
       * user doesn't know details of program functionality!)
       * Refer jtlv-api1.1.0.jar!/edu/wis/jtlv/lib/games/GRGame.class,
       * then find source, then find lines 178 to 359.
       */
      /* Made a minor modification so I can at least get the string. */
      g.calculate_strategy();
      System.out.println(g.getStrategyString());
      // No output available, so added getStrategyArray.
      RawState[] srs = g.getStrategyArray();
      // Couldn't get variable names! Try other things.
      
      // Prints a multiline textual description of a digraph:
      //for (int tin = 0; tin < 1; tin++) {
      //  srs[tin].get_state().printDot();
      //}
      
      // Prints the state like <0:0, 2:0, 4:0, 6:0> where
      // {0, 2, 4, 6}:* must represent internal variable names and
      // *:{0,1} are the (boolean) values associated with them.
      //srs[0].get_state().printSet();
      
      // Bear with me on this one. Let's hack System...
      //java.io.PrintStream sysout = System.out;
      //java.io.PipedInputStream pis = new java.io.PipedInputStream();
      //java.io.BufferedReader newReader = new java.io.BufferedReader(
      //    new java.io.InputStreamReader(pis));
      //java.io.PipedOutputStream pos = new java.io.PipedOutputStream(pis);
      //System.setOut(new java.io.PrintStream(pos));
      //Env.JTLVBDDToString.print_as_range = true;
      //srs[0].get_state().printSetWithDomains();
      //System.setOut(oldSystemOut);
      //sysout.println("This should look like a state: "
      //    + newReader.readLine());
      //sysout.print("-----------------------------------------\n");
      //System.setOut(sysout);
      
      // Here's another way to get the name, maybe, but it's deprecated.
      //net.sf.javabdd.BDDDomain[] a_bdom = srs[0].get_state().support().getDomains();
      //for (int pb = 0; pb < a_bdom.length; pb++) {
      //  System.out.println("Your variable has the name: " +
      //    Env.___getVarForDomain(a_bdom[pb]));
      //}
      
      // OK, now I want their values of the variables, too. That should
      // be relatively easy, right?
      //BigInteger[] vals = srs[2].get_state().scanAllVar();
      //System.out.println(java.util.Arrays.toString(vals));
      //for (int w = 0; w < vals.length/2; w++) {
      //  System.out.println("Your variable has the value: " + vals[w*2]);
      //}
      
      // Print out all the states, and the values in each.
      // Loop over states in automaton.
      //for (int s = 0; s < srs.length; s++) { 
      //  net.sf.javabdd.BDDDomain[] nsjb =
      //    srs[s].get_state().support().getDomains();
      //  BigInteger[] bivals = srs[s].get_state().scanAllVar();
      //  System.out.print("State " + s + ":   <");
        // Loop over variables in state.
      //  for (int v = 0; v < nsjb.length; v++) { 
      //    System.out.print(((v != 0)?", ":"")
      //      + Env.___getVarForDomain(nsjb[v]) + ":" + bivals[v * 2]);
      //  }
      //  System.out.println(">");
      //}
      
      System.out.println("== Run this through '''dot''' ==");
      PrintStream dotout;
      FileOutputStream fos;
      if (dotFile != "") {
    	  fos = new FileOutputStream(dotFile);
    	  dotout = new PrintStream(fos);
    	  System.out.println("(I'm writing this stuff to " + dotFile + ".)");
      } else {
    	  dotout = System.out;
      }
      // Print an input format for dot graph generator.
      dotout.println("digraph G {");
      // loop over states in automaton
      for (int s = 0; s < srs.length; s++) {
    	
        // Print the number of the node and a label.
        net.sf.javabdd.BDDDomain[] nsjb =
          srs[s].get_state().support().getDomains();
        BigInteger[] bivals = srs[s].get_state().scanAllVar();
        
        // loop over variables in state

        dotout.print("" + srs[s].get_id());
        dotout.print(" [label=\"" + srs[s].get_id() + "<");
        for (int v = 0; v < nsjb.length; v++) {
        	if (v >= ninputs + noutputs) {
        		break;
        	}
          dotout.print(
            ((v == ninputs)?"/":((v != 0 && s == 0)?", ":""))
            + ((s == 0)?(Env.___getVarForDomain(nsjb[v]) + ":"):"")
            + bivals[v * 2]);
        }
        dotout.println(">\"];");
        // Print each outgoing transition.
        RawState[] nexts = srs[s].get_succ_raw();
        for (int n = 0; n < nexts.length; n++) {
          dotout.println("" + srs[s].get_id()
        		  + " -> " + nexts[n].get_id() + ";");
        }
        
      }
      dotout.println("}");
      
      System.out.println("== Run this through '''lt-planner''' ==");
      PrintStream psmout;
      FileOutputStream sil;
      // I should handle io errors better than this.
      if (psmFile != "") {
    	  sil = new FileOutputStream(psmFile);
    	  psmout = new PrintStream(sil);
    	  System.out.println("(I'm writing this stuff to " + psmFile + ".)");
      } else {
    	  psmout = System.out;
      }
      // Print an input format for state machine parser.
      psmout.println("State machine.");
      psmout.println("Version: 2;");
      psmout.println("Name: " + input_file + ";");
      psmout.println("Initial: 0;");
      psmout.println("Error: 0;");

      // loop over states in automaton
      for (int s = 0; s < srs.length; s++) {
        // Print the number of the node and a label.
        net.sf.javabdd.BDDDomain[] nsjb =
          srs[s].get_state().support().getDomains();
        BigInteger[] bivals = srs[s].get_state().scanAllVar();
        
        // loop over variables in state

        // Print the names of the input variables once.
        if (s == 0) {
          psmout.print("Inputs: {");
          int v = 0;
          for ( ; v < ninputs; v++) {
            psmout.print(Env.___getVarForDomain(nsjb[v]) + " ");
          }
          psmout.println("};");
          psmout.print("Outputs: {");
          for ( ; v < ninputs + noutputs; v++) {
        	psmout.print(Env.___getVarForDomain(nsjb[v]) + " ");
          }
          psmout.println("};");
        }

        // Print each state.
        // State Id.
        psmout.print("State: (" + srs[s].get_id());
        // Error successor left blank for now.
        psmout.print(" [ ]");
        // Print each outgoing transition.
        psmout.print(" {");
        RawState[] nexts = srs[s].get_succ_raw();
        for (int n = 0; n < nexts.length; n++) {
          psmout.print(nexts[n].get_id() + " ");
        }
        psmout.print("}");
        // State valuation.
        psmout.print(" <");
        for (int v = 0; v < ninputs; v++) {
          psmout.print(bivals[v * 2]);
        }
        psmout.print("/");
        for (int v = ninputs; v < ninputs + noutputs; v++) {
          psmout.print(bivals[v * 2]);
        }
        psmout.print(">");
        // Close up the line on this state.
        psmout.println(");");
      } // End the loop over states
      psmout.println("End");

      System.out.println("== Done ==");

    } catch (GameException e) {
      // error in the game...
      e.printStackTrace();
    } catch (IOException e) {
      // error with reading the file...
      e.printStackTrace();
    }
  }
}

