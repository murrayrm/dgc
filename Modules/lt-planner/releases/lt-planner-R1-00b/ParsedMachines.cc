// ParsedMachines.cc
// Define a few environments and systems for ParsedSMs.
// By Nicholas Fette.
// see DGC07:User:Nfette/Summer 2008

#include "ParsedMachines.hh"
#include "ParseUtils.hh"         // For ParseUtils static object.
#include "ParseUtilsInternal.hh" // for operator<< using ParseUtils.

#include "temp-planner-interfaces/Console.hh" // for Console
#include "temp-planner-interfaces/Utils.hh"   // for Utils
#include "temp-planner-interfaces/AliceStateHelper.hh" // For velocityMag.
#include <deque>

// This is supposed to be a virtual class!
TrafficSM::TrafficSM () {
  // Nothing to do here.
}

// This is supposed to be a virtual class!
TrafficSM::~TrafficSM () {
  // Nothing to do here.
}

// Use this to set up a "default" for 
TrafficSM* TrafficSM::getDefaultTrafficSM() {
  ParsedSM *sam = new ParsedSM("../lt-planner/default.psm");
  sam->init();
  TrafficStop *ts = new TrafficStop(sam);
  ts->destroySamOnExit = true;
  return ts;
}

TrafficStop::TrafficStop(ParsedSM *psm) : Environment(), System(), sam(psm)
{
  // Must do these things for object structure to work.
  sam->setEnvironment(this);
  sam->setSystem(this);
  destroySamOnExit = false;

  // Init current status, complete state representation
  current_status.state       = DRIVE;
  current_status.probability = 0.99;
  current_status.flag        = NO_PASS;
  current_status.region      = ROAD_REGION;
  current_status.planner     = RAIL_PLANNER;
  current_status.obstacle    = OBSTACLE_SAFETY;

  // Define the input map.
  inputMap["justBeforeStopSign"] = &TrafficStop::isJustBeforeStopSign;
  inputMap["nowStopped"]         = &TrafficStop::isNowStopped;

}

// This had better not
TrafficStop::~TrafficStop() {
  if (destroySamOnExit) {
    delete sam;
  }
}

Err_t TrafficStop::planLogic( TrafficData *arg )
{
  // I call other functions that need the traffic data.
  tada = arg;

  // Build a temporary output to fool the planner.
  static StateProblem_t p;
  p.state       = current_status.state;
  p.region      = current_status.region;
  p.obstacle    = current_status.obstacle;
  p.planner     = current_status.planner;
  p.flag        = current_status.flag;
  p.probability = current_status.probability;

  // Push new problem into queue. We can modify it there.
  tada->problems->push_back(p);

  // Step the state machine. See ParsedSM::step near ParsedSM.cc:310.
  sam->step();

  // Change the variables we use for printing.
  current_status.state = tada->problems->back().state;
  current_status.region = tada->problems->back().region;
  current_status.obstacle = tada->problems->back().obstacle;
  current_status.planner = tada->problems->back().planner;
  current_status.flag = tada->problems->back().flag;
  current_status.probability = tada->problems->back().probability;

  return LP_OK;
}

vector<string> TrafficStop::getInputNames() {
  return sam->getInputNames();
}

// Get all the input values in default order.
vector<bool> TrafficStop::getInputs() {
  return getInputs(getInputNames());
}

// Get the named inputs in their order, or false for an unkown name.
vector<bool> TrafficStop::getInputs(vector<string> names) {
  vector<bool> result;
  for (int i = 0; i < (int)names.size(); i++) {
    // Evaluate one of the functions in the input map, using my TrafficData.
    result.push_back(inputMap[names[i]](tada));
  }
  return result;
}

// Take any appropriate action after state change. (None required)
void TrafficStop::execute(vector<bool> outputVals,
  vector<string> outputNames) {
  static int myLastState = sam->getState();
  int myNewState = sam->getState();
  if (myLastState != myNewState) {
    string outputValsString = "<";
    for (int i = 0; i < (int)outputVals.size(); i++) {
      outputValsString += ((outputVals[i])?"1":"0");
    }
    outputValsString += ">";
    Console::addMessage("LTP: State transition: %d -> %d %s", myLastState,
      myNewState, outputValsString.c_str());
  }
  myLastState = myNewState;
  // Are we in the mood to drive?
  tada->problems->at(0).state = (outputVals[0])?DRIVE:STOP_INT;
}

vector<bool> timerGame::getInputs(vector<string> varnames) {
  // cout << "Tiger: giving away inputs at time " << time << endl;
  vector<bool> result;
  for (vector<string>::iterator iter = varnames.begin();
    iter != varnames.end(); iter++) {
    if (*iter == "a") {
      // high bit
      result.push_back((timevar / 2) % 2);
    } else if (*iter == "b") {
      // low bit
      result.push_back(timevar % 2);
    } else {
      // fake bit (not supposed to happen)
      result.push_back(false);
    }
  }
  timevar ++;
  return result;
}

void timerGame::execute(vector<bool> outputVals, vector<string> outputNames) {
  lastVals = outputVals;
  lastNames = outputNames;
}

ostream & timerGame::displayLastCall(ostream &os) const {
  ParseUtils::setVectorDelims("<",">","");
#ifndef VECTOR_OFF
  os << lastNames << " = " << lastVals << endl;
#else
  ParseUtils::displayVector(os, lastNames);
  os << " = ";
  ParseUtils::displayVector(os, lastVals);
  os << endl;
#endif
  return os;
}

bool TrafficStop::isJustBeforeStopSign(TrafficData *td) {
  double stopline_distance = Utils::getDistToStopline(td->params->m_path);
  return stopline_distance < 10;
}

bool TrafficStop::isNowStopped(TrafficData *td) {
  double currentVelocity = AliceStateHelper::getVelocityMag(*(td->vehState));
  // Warning: magic number
  return currentVelocity < 0.2;
}

// EOF ParsedMachines.cc
