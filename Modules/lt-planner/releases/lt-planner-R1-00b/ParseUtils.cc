// File ParseUtils.cc
// Extras for displaying output, mostly.

#include "ParseUtils.hh"

using namespace std;

string ParseUtils::PRETTY_VECTOR_BEGIN = ")";
string ParseUtils::PRETTY_VECTOR_END = "(";
string ParseUtils::PRETTY_VECTOR_SEP = ", ";

void ParseUtils::setVectorDelims(string begin, string end, string sep)
{
  PRETTY_VECTOR_BEGIN = begin;
  PRETTY_VECTOR_END = end;
  PRETTY_VECTOR_SEP = sep;
}

template<class T>
ostream & ParseUtils::displayVector(ostream &os, const vector<T> &arg) {
  int len = arg.size() - 1;
  os << PRETTY_VECTOR_BEGIN;
  for (int i = 0; i <= len; i++) {
    os << arg[i];
    if (i != len) { os << PRETTY_VECTOR_SEP; }
  }
  os << PRETTY_VECTOR_END;
  return os;
}

ostream & ParseUtils::displayVectorBool(ostream &os,
  const vector<bool> &arg) {
  return displayVector<bool>(os, arg);
}

ostream & ParseUtils::displayVectorInt(ostream &os,
  const vector<int> &arg) {
  return displayVector<int>(os, arg);
}

ostream & ParseUtils::displayVectorString(ostream &os,
  const vector<string> &arg) {
  return displayVector<string>(os, arg);
}

// Return a hash of a valuation. (Basically, binary to integer.)
// If valuation.size() == 2,
// hash = val[0] * 1 + val[1] * 2 + val[2] * 4
unsigned int ParseUtils::valuation2hash(vector<bool> valuation) {
  int result = 0;
  int mul = 1;
  for (int i = 0; i < (int) valuation.size(); i++) {
    result += mul * valuation[i];
    mul *= 2;
  }
  return result;
}

// Return a valuation of a hash. (Basically, integer to binary.)
// If size == 2, hash = val[0] * 1 + val[1] * 2 + val[2] * 4.
vector<bool> ParseUtils::hash2valuation(unsigned int hash, unsigned int size) {
  vector<bool> result;
  //int mul = (int)pow((double)2, (double)size);
  int rem = 0;
  for (unsigned int i = 0; i < size; i++) {
    rem = hash % 2;
    hash = (hash - rem) / 2;
    result.push_back(rem);
  }
  return result;
}

// EOF ParseUtils.cc
