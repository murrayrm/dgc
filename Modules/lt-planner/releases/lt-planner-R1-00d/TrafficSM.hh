/*!
 * \file TrafficSM.hh
 * \brief Define the interface for TrafficSM.
 *
 * \author Nicholas Fette
 * \date July 2008.
 * 
 * \ingroup TrafficSM
 *
 * see DGC07:Lt-planner
 */

#ifndef _TRAFFIC_SM_HH_
#define _TRAFFIC_SM_HH_
#include "ParsedSM.hh" // Defines ParsedSM, SMState, Environment, and System.
// REMOVE #include <cmath> // For pow(base, exp) in hash.
#include <map>   // For, well, you guessed it.

// Unfortunately, I can't compile standalone any longer.
#include <temp-planner-interfaces/PlannerInterfaces.h>

using namespace std;

//! Helps make a map of the input functions.
struct strCmp {
  bool operator() (const string a, const string b) const {
    return (strcmp(a.c_str(), b.c_str()) > 0);
  }
};

//! Helps pass data from Planner to LtPlanner to TrafficSM.
typedef struct {
  //! A vector with usually one scenario for the path planner.
  vector<StateProblem_t*> *problems;
  //! The graph of something.
  PlanGraph *graph;
  //! The errors from the previous Planner cycle.
  Err_t prevErr;
  //! The state of the vehicle. (Maybe position, transmission, steering?)
  VehicleState *vehState;
  //! Map represents the conglomeration of sensing data which should
  //! represent the surrounding landscape.
  Map *map;
  //! Just some extra, but useful stuff added on by Planner.
  Logic_params_t *params;
  //! According to MissionPlanner, where we are driving, as in 1.2.1.
  int *currentSegmentId;
  //! According to MissionPlanner, which lane we drive in, as in 0, 1.
  int *currentLaneId;
  //! Whether to do something vague, according to Jeremy. (ignore that)
  bool *replanInZone; // Not needed right now.
} TrafficData;

typedef bool (TrafficData::*EnvFunctor) (void);

//! A generic representation of a state machine for planning traffic logic.
class TrafficSM {
  protected:
    //! Map from input names to boolean function pointers.
    map<string, EnvFunctor, strCmp> envMap;
    //! Default input value in case an input name is not recognized.
    EnvFunctor envDefaultInput;
    //! Fast access to ordered input functions.
    //! You must setInputNames before using this.
    vector<EnvFunctor> envInputFuncs;
  public:
    TrafficSM();
    virtual ~TrafficSM();
    virtual Err_t planLogic(TrafficData *tada) = 0; 
    StateProblem_t current_status;
    virtual string getStateString() = 0;
    static char *DEFAULT_PSM_FILE;
    static TrafficSM* getDefaultTrafficSM();
};

//! A basic implementation of a TrafficSM setup with a ParsedSM.
class TrafficStop : public TrafficSM, public Environment, public System {
  private:
    ParsedSM *sam;
    //! How the Environment evaluates its input variables.
    //! Each valid name is mapped to a functor.
    //! Valid names are assigned in the constructor.
    map<const string, bool (*)(TrafficData *td), strCmp> inputMap;
    TrafficData *tada;
  public:
    bool destroySamOnExit;
    TrafficStop(ParsedSM *psm);
    ~TrafficStop();
    //! Gets the state machine which is supposed to be hidden.
    ParsedSM* getPSM();
    //! Gets a string which conveys information about state.
    string getStateString();
    //! Gets called by LtPlanner.
    Err_t planLogic(TrafficData *tada); 
    //! Override inherited methods for Environment.
    const vector<string> &getInputNames();
    const vector<bool> &getInputs();
    const vector<bool> &getInputs(const vector<string>& varnames);
    //! Override inherited methods for System.
    void execute(const vector<bool>& outputVals,
      const vector<string>& outputNames);
    //! Tests that will become state machine input.
    // Let's try to not make these static (later).
    static bool isJustBeforeStopSign(TrafficData *td);
    static bool isNowStopped(TrafficData *td);

    //! Not very well-defined, is it?
    static bool isObstacleInWay(TrafficData *td);

    //! Straight out of gcinterface/SegGoals/SegmentType.
    static bool isSegmentRoad(TrafficData *td);
    static bool isSegmentZone(TrafficData *td);
    static bool isSegmentIntersection(TrafficData *td);
    static bool isSegmentPreZone(TrafficData *td);
    static bool isSegmentUturn(TrafficData *td);
    static bool isSegmentDriveAround(TrafficData *td);
    static bool isSegmentBackUp(TrafficData *td);
    static bool isSegmentEndOfMission(TrafficData *td);
    static bool isSegmentEmergencyStop(TrafficData *td);
    static bool isSegmentUnknown(TrafficData *td);
    static bool isSegmentStartChute(TrafficData *td);

    //! Straight out of gcinterface/SegGoals/IntersectionType.
    static bool isIntersectionStraight(TrafficData *td);
    static bool isIntersectionLeft(TrafficData *td);
    static bool isIntersectionRight(TrafficData *td);

};


#endif // _TRAFFIC_SM_HH_
// EOF TrafficSM.hh
