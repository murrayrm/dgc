import java.io.IOException;

import net.sf.javabdd.BDD;
import edu.wis.jtlv.env.Env;
import edu.wis.jtlv.env.module.ModuleWithWeakFairness;
import edu.wis.jtlv.lib.games.GameException;


/**
 * Make and run an automaton from a SMV file.
 * This class copies Nfsm and adds a graphical representation.
 * 
 * @author nfette
 *
 */
public class Gfsm {

	/** The states and transitions of the resulting automaton. */
	private RawState[] srs;
	/** The state in which we now are. */
	private int state = -1;
	/** The allowable next states. */
	private int[] successorIds = null;

	/* Start here. */
	public static void main(String[] args) {
		
		String wd = "./src/testcases/";
		//		String[] files = (new java.io.File(wd)).list();
		//		if (files != null) {
		//			System.out.println("Dir \"" + wd + "\": found " + files.length + " files.");
		//			for (int i = 0; i < files.length; i++) {
		//				System.out.println(files[i]);
		//			}
		//			System.out.println();
		//		} else {
		//			System.out.println("Dir \"" + wd + "\": found no files!");
		//		}

		// 1. Load SMV from file
		System.out.print("Loading file: ");
		String input_file = wd + "arbiter2_with_spec.smv";
		if (args.length > 0) {
			input_file = args[0];
		}
		System.out.println(input_file);

		System.out.println("About to compute automaton.");
		RawState[] srs;
		try {
			//srs = computeAutomaton(input_file).clone();
			GRGame2 g = computeAutomaton(input_file);
			srs = g.getStrategyArray();
			if (srs == null) {
				System.out.println("Warning: RawState array was null/empty!");
			}
			
			// Now build a structure that is useful.
			System.out.println("Converting from raw states to finite state machine.");
			Gfsm msfn = new Gfsm(srs);
			int[] freq = new int[srs.length];
			
			// Now run the thing.
			System.out.println("Beginning to run FSM from state "
					+ msfn.getState() + ".");
			System.out.println("Initial frequency " +
					java.util.Arrays.toString(freq) + ".");
			for (int i = 0; i < 100; i++) {
				freq[msfn.getState()] ++;
				int[] success = msfn.getAllowableSteps().clone();
//				System.out.println("In state " + msfn.getState());
//				System.out.println("Valid successors: " +
//						java.util.Arrays.toString(msfn.getAllowableSteps()));
				//System.out.println(msfn.srs[msfn.state]);
				int m = (int)(Math.random() * success.length);
				msfn.step(success[m]);
			}
			System.out.println("Final frequency "
					+ java.util.Arrays.toString(freq) + ".");
		} catch (GameException e) {
			// error in the game...
			e.printStackTrace();
		} catch (IOException e) {
			// error with reading the file...
			e.printStackTrace();
		}

	} // end main

	public int getState() {
		return this.state;
	}

	public static GRGame2 computeAutomaton(String input_file)
	throws IOException, GameException {
		GRGame2 g;
		Env.loadModule(input_file);
		ModuleWithWeakFairness cx = (ModuleWithWeakFairness) Env
		.getModule("main");
		ModuleWithWeakFairness player1 = (ModuleWithWeakFairness) Env
		.getModule("main.e");
		ModuleWithWeakFairness player2 = (ModuleWithWeakFairness) Env
		.getModule("main.s");

		// 2. Determine if FSM exists to satisfy SMV spec.
		System.out.println("Constructing and playing the game.");
		g = new GRGame2(player1, player2);
		System.out.println("Player sys winning states are:");
		System.out.println(Env.toNiceString(cx, g.sysWinningStates()));
		System.out.println("Player env winning states are:");
		System.out.println(Env.toNiceString(cx, g.envWinningStates()));

		BDD all_init = g.getSysPlayer().initial().and(
				g.getEnvPlayer().initial());
		BDD counter_exmple = g.envWinningStates().and(all_init);
		if (!counter_exmple.isZero()) {
			System.out.println("Specification is unrealizable.");
			System.out.println("The env player can win from states:");
			System.out.print("\t" + Env.toNiceString(cx, counter_exmple));
		} else {
			System.out.println("Specification is realizable.");
			System.out.println("Building an implementation.");

			g.calculate_strategy();
			//return g.getStrategyArray();
		}
		System.out.println("Done with this part."); // hush
		//return g.getStrategyArray().clone();
		return g;
	}

	public Gfsm (RawState[] srs) {
		this.srs = srs;
		// initialize to some state. You guess....
		this.state = 0;
	}

	/** Returns whether the step was allowed, and if so executes it. */
	public boolean step(int next) {
		boolean validStep = false;
		for (int i = 0; i < successorIds.length; i++) {
			if (next == successorIds[i]) {
				validStep = true;
				this.state = next;
				break;
			}
		}
		return validStep;
	}
	
	public int[] getAllowableSteps() {
		RawState[] nextraws = this.srs[this.state].get_succ_raw().clone();
		successorIds = new int[nextraws.length];
		for (int i = 0; i < nextraws.length; i++) {
			successorIds[i] = nextraws[i].get_id();
		}
		return successorIds;
	}
}

