/*!
 * \file ParsedSM.hh
 * \brief // Defines some state machine data types and a parser.
 * \author Nicholas Fette
 * \date July 2008
 *
 * \ingroup ParsedSM
 * This file defines a class for parseable state machines.
 */

#ifndef _PARSED_SM_HH_
#define _PARSED_SM_HH_

#include <iostream>  // For std::cout to print info.
#include <fstream>   // For ifstream to read files.
#include <vector>    // For vector template. (char, int, and string)
#include <exception> // For SMException.
#include <map>       // For mapping input names to functions.

using namespace std;

class SMState;

/*!
 * \brief Interface for state machines to read inputs.
 *
 * Just an interface through which a state machine can read inputs.
 * For instance, ALICE's environment reads from sensor data.
 * Need to not be passing objects to copy! Think of the time and memory!
 */
class Environment {
  protected:
    //! A name for this environment. (not used)
    string envName;
    //! Remember the input names to use.
    vector<string> envInputNames;
    //! Keep the return value for getInputs here.
    vector<char> envInputVals;
  public:
    //! Create an Environment with the internal name "no-name".
    Environment() : envName("no-name") { }
    //! Create an Environment with the given internal name.
    Environment(string name) : envName(name) { }
    //! Clean up any memory used by the Environment.
    virtual ~Environment() {}
    //! Get the names of all the inputs variables.
    virtual const vector<string>& getInputNames() {
      // dummy implementation
      return envInputNames;
    }
    //! Get all the values of the input variables.
    virtual const vector<char>& getInputs() {
      return envInputVals;
    }
    /*!
     * Get only the values of the requested input variables.
     * If a name is unrecognized, we should notify somehow, but we don't.
     * In the meantime check getInputNames(),
     * and the return value should have the same size as varnames.
     */
    virtual const vector<char>& getInputs(const vector<string> &varnames) {
      envInputVals.clear();
      for (int i = 0; i < (int)varnames.size(); i++) {
        envInputNames.push_back(false);
      }
      return envInputVals;
    }
};

/*!
 * \brief Interface for state machines to act on outputs.
 *
 * Just an interface through which a state machine can affect its environment.
 * For instance, ALICE's system includes actuator states and path plans.
 */
class System {
  protected:
    string name;
  public:
    //! Create a System with the internal name "no-name".
    System() : name("no-name") { }
    //! Create a System with the given internal name.
    System(string sysname) : name(sysname) { }
    //! Clean up memory of the System.
    virtual ~System() {};
    //! Do whatever action the doing of which the system is capable.
    //! I know, this is so generalized it's disgusting.
    virtual void execute(const vector<char> &outputVals,
      const vector<string> &outputNames) {
      // Haha, let's let them all make brilliant mistakes.
      cout << "Warning: you're using a virtual method System::execute."<< endl;
    }
};

/*!
 * \brief State machine and parser from text files.
 *
 * Implementation of a state machine which is built from a text file.
 * For documentation on the .psm file format, see DGC07:LTL planner project.
 * State machine files are specified usually in a configuration file.
 */
class ParsedSM {
  protected:
    string version;
    string name;
    char *SMfile;
    bool SMfileExists;
    //! Input file stream for the input file (to be streamed).
    ifstream fin;
    //! Unparsed string leftover from previous lines.
    string leftover;
    //! Read tokens from fin into args up to next semicolon,
    //! and return the new size of that vector, or -1
    //! if reached the end of file and no more statements.
    int readStatement(vector<string> &args);
    //! How many input state variables are there?
    int inputCount;
    //! How many output state variables are there?
    int outputCount;
    //! What are the names of the state variables?
    vector<string> inputNames;
    vector<string> outputNames;
    //! How are we going to store the states?
    vector<SMState> astate;
    //! Integer identifier of initial state, not guaranteed to exist.
    int initialID;
    //! Integer identifier of error state, not guaranteed to exist.
    int errorID;
    //! The environment from which the inputs are evaluated.
    Environment *env;
    //! The system through which outputs are executed.
    System *sys;
    //! The execution history.
    vector<int> trace;
  public:
    //! Read and parse state machine.
    ParsedSM(char *file);
    //! Destructor. Clean memory used for state variable names.
    virtual ~ParsedSM();
    //! Return the file from which this state machine was formed.
    char *getSMfile();
    //! Call to parse and build data structure.
    void init();
    //! Get the name of this state machine (not the file name).
    string getName() const { return name; }
    //! Get the version of this state machine.
    string getVersion() const { return version; }
    //! Get the ID of the initial state of this state machine.
    int getInitialID() const { return initialID; }
    //! Get the ID of the defaul error state of this state machine.
    int getErrorID() const { return errorID; }
    //! How many input state variables are there?
    int getInputCount() const;
    //! How many output state variables are there?
    int getOutputCount() const;
    //! What are the names of the state variables?
    const vector<string>& getInputNames() const;
    const vector<string>& getOutputNames() const;
    //! What are those states? Just for a little exposure.
    const vector<SMState>& getStates() const { return astate; }
    //! Associate this state machine with an environment so that it
    //! can read in the input states.
    void setEnvironment(Environment *arg);
    //! Associate this state machine with a system
    //! so that it can control its output.
    void setSystem(System *arg);
    //! Cause the state machine to read inputs, transition, and call output.
    void step();
    //! Get the execution history, ie, the most recent state id is pushed back.
    vector<int> getTrace();
    //! Get the most recent state id.
    int getState();
    //! Display the state machine in input format to stream, et return stream.
    ostream & display(ostream &os) const;
    //! Display just the current state in input format to the stream.
    ostream & displayState(ostream &os) const;
};


// NOT USED
//! State machine with boolean variable states.
class SM {
  protected:
    //vector<SMstate> sms;
  public:
    /* Create a new state machine. */
    SM();
    /* Destroy a state machine. */
    virtual ~SM();
    /* How many states in this machine. */
    //int length();
    /* Add a state and tell me its number. */
    //int addSMState(SMState s);
    /* Where do we start? */
    //SMState getInitialState();
    /* Where do we end up? */
    //SMState getCurrentState();
    /* Let's make a transition. */
    //int transition(int next);
    /* Preview what should be the next state. */
    //int getSuccessor();
};

//! State for a state machine.
class SMState {
  protected:
    //! The state machine in which I am a state.
    //! Note that state variable names are stored in the parent.
    ParsedSM* parent;
    //! My unique identifier.
    int myID;
    //! The successor to this state iff we get a bad input.
    int myErrorID;
    //! Array of IDs of successors to this state.
    vector<int> succ;
    //! Vector of input state variable values.
    vector<char> ivars;
    //! Vector of output state variable values.
    vector<char> ovars;
  public:
    //! Construct a new state.
    //! parent is responsible for knowing the length of its
    //! variable arrays, and will be called for them.
    SMState(ParsedSM* argparent, int argid, int errorid, vector<int> argsucc,
      vector<char> argivars, vector<char> argovars) :
        parent(argparent), myID(argid), myErrorID(errorid)
    {
        ivars = argivars;
        ovars = argovars;
        succ = argsucc;
    }
    //! Free memory used to store state variable values.
    virtual ~SMState() { }
    ParsedSM* getParent() { return parent; }
    //! The integer identifier of this state.
    int getID() const { return myID; }
    //! The integer identifier of the error successor to this state.
    int getErrorID() const { return myErrorID; }
    //! The successors of this state.
    vector<int> getSuccessors() const { return succ; }
    //! The input valuation.
    vector<char> getInputVector() const { return ivars; }
    //! The output valuation.
    vector<char> getOutputVector() const { return ovars; }
    //! Display routine, formatted like an input file.
    ostream & display(ostream &os) const; 
};

/* Represent a boolean variable. 
class BoolVar {
  private:
    char * name;
    bool value;
  public:
    BoolVar(char *name, bool value);
    ~BoolVar();
    char * getName();
    bool getValue();
};
*/

//! Might be thrown while parsing state machines from file.
class SMException : public exception {
  protected:
    string mymsg;
  public:
    SMException() throw() : mymsg("Generic state machine exception.") { }
    SMException(string msg) throw() : mymsg(msg) { }
    ~SMException() throw() { }
    const char* what() const throw() { return mymsg.c_str(); }
};

ostream & operator<<(ostream &os, const SMState &arg);

ostream & operator<<(ostream &os, const ParsedSM &arg);

#endif // _PARSED_SM_HH_
//EOF ParsedSM.hh
