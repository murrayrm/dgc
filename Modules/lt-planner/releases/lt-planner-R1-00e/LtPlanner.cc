/*!
 * \file LtPlanner.cc
 * \brief Definitions LtPlanner
 *
 * \author Nicholas Fette (based on ModLogPlanner by Stefan Skoog)
 * \date July 2008
 *
 * \ingroup lt-planner
 *
 */



/*********************************
 * Include header
 *********************************/
#include "LtPlanner.hh"      // Declarations for this class (top level).
#include "temp-planner-interfaces/Console.hh" // for Console.
#include "temp-planner-interfaces/Utils.hh"   // for Utils.
#include "temp-planner-interfaces/ConfigFile.hh" // for ConfigFile.
#include "dgcutils/cfgfile.h" // for dgcFindConfigFile().
#include <sstream>
#include "ParseUtils.hh"

/*********************************
* Initiate (static) class variables
**********************************/


/*********************************
 * Methods
 *********************************/

LtPlanner::LtPlanner() 
{
  // In case we need a new constructor, call alternative init function.
  init();
}

/**
 * @brief   Lt PLanner initializer
 *      Reads config file, resets internal variables,
 *      creates LT State Machine if necessary.
 */
void LtPlanner::init() 
{
  // Code using ParseUtils will print to the right place, roughly.
  ParseUtils::setLogger(Log::getStream(5));
  // Read config file and save into class variables.
  readConfig();
  char path[128] = "../lt-planner/default.psm";
  strcpy(path, DEFAULT_PSM_FILE.c_str());
  TrafficSM::DEFAULT_PSM_FILE = 
  dgcFindConfigFile(path, "lt-planner");
  Log::getStream(5) << "TrafficSM::DEFAULT_PSM_FILE = " << 
    TrafficSM::DEFAULT_PSM_FILE << endl;

  
  // Count how many times planLogic gets called.
  cycle_counter = 0;

  // Init current status, complete state representation
  current_status.state       = DRIVE;
  current_status.probability = 0.99;
  current_status.flag        = NO_PASS;
  current_status.region      = ROAD_REGION;
  current_status.planner     = RAIL_PLANNER;
  current_status.obstacle    = OBSTACLE_SAFETY;
  
  // Once the dummy release is working, uncomment.
  // Create a state machine parsed from file.
  try {
    tsm = TrafficSM::getDefaultTrafficSM();
  } catch (exception &e) {
    cerr << e.what();
    throw;
  }
}

// Destructor
LtPlanner::~LtPlanner()
{
  // Get rid of all created objects and class references
  destroy();  // Call alternate destructor
  return;
}

// Alternative destructor
void LtPlanner::destroy() 
{
  // Deallocate all claimed objects...
  delete tsm;
}

/**
 * @brief   Modular Logic PLanner config file reader
 *      Reads config file and store data in internal variables.
 * @return  (int)0 if good
 */
void LtPlanner::readConfig() 
{
  // Once the dummy release is working, uncomment.
  //*
  // Read from file into workspace
  char *path;
  // include this function from dgcutils/cfgfile.h
  path=dgcFindConfigFile("LtPlanner.conf","lt-planner");
  FILE* configFile = fopen(path,"r");
  if (configFile == NULL) {
    Console::addMessage("LTP: Warning: Couldn't read LtPlanner.conf.");
    // FIXME: choose & use appropriate logging function.
    Log::getStream(1)<<"LTP: Warning: Couldn't read LtPlanner.conf."<<endl;
    //qprint("Warning: Couldn't read LtPlanner.conf.\n");
  }
  else {
    // include this class from temp-planner-interfaces/ConfigFile.hh
    ConfigFile config(path);
    // Example:
    config.readInto(LIFE, "LIFE");
    config.readInto(SM_MODE, "SM_MODE");
    config.readInto(DEFAULT_PSM_FILE, "DEFAULT_PSM_FILE");
    
    Log::getStream(1)<<"LTP: Read LtPlanner configuration successfully."<<endl;
    fclose(configFile);

    // Log all the parameters
    Log::getStream(5) << "LTP: LIFE: " << LIFE << endl;
    Log::getStream(5) << "LTP: SM_MODE: " << SM_MODE;
    switch (SM_MODE) {
      case 1: Log::getStream(5) << " (Parsed SM)" << endl; break;
      case 0:
      default: Log::getStream(5) << " (Quantum SM)" << endl;
    }
    Log::getStream(5) << "LTP: DEFAULT_PSM_FILE: "
      << DEFAULT_PSM_FILE << endl;
  }
  //*/

}

// Planner might ask for Console::queryResetStateFlag(), 
// which I should at least pretend to acknowledge.
void LtPlanner::resetState() {
}

//! Just a comparator for use in making maps from int to string et al.
struct intCmp {
  bool operator()(int a, int b) {
    return a > b;
  }
};

// Planner will ask every time.
void LtPlanner::updateDisplayInfo() {
    
    // Dummy messages
    string msgst = stateString[tsm->current_status.state];
    Console::updateFSMState(msgst);
    Log::getStream(1) << "Current State = " << msgst << endl;

    string msgfl = flagString[tsm->current_status.flag];
    Console::updateFSMFlag(msgfl);
    Log::getStream(1) << "Current Flag = " << msgfl << endl;
    
    string msgre = regionString[tsm->current_status.region];
    Console::updateFSMRegion(msgre);
    Log::getStream(1) << "Current Region = " << msgre << endl;
    
    string msgpl = plannerString[tsm->current_status.planner];
    Console::updateFSMPlanner(msgpl);
    Log::getStream(1) << "Current Planner = " << msgpl << endl;
    
    string msgob = obstacleString[tsm->current_status.obstacle];
    Console::updateFSMObstacle(msgob);
    Log::getStream(1) << "Current Obstacle Level = " << msgob << endl;

    double msgpr = current_status.probability;
    Log::getStream(1) << "Current Probability Level = " << msgpr << endl;

    // Send the planner status to the map viewer
    Utils::displayPlannerStatus(msgre, msgst, msgpl, msgfl);
    Utils::displayPlannerState(tsm->getStateString());
}

void LtPlanner::updateIntersectionConsole() {
  // A blank string here is supposed to indicate
  // that we aren't in an intersection.
  Utils::displayIntersectionState(tsm->intersectionStringForConsole);

  // Note:
  // map/MapPrior.hh/PointLabel
  // temp-planner-interfaces/PlannerInterfaces.h/precedenceList_t
  // map/MapElement.hh/MapElement

  // Console::updateInter(
  //   int CountPrecedence, // How many elements in the precedenceList
  //                        // actually have precedence?
  //   string Status,       // eg, Waiting for car on right, jammed, ....
  //   map/MapPrior.hh/PointLabel wp, // eg, PointLabel(
  //                                           currSegment.entrySegmentID,
  //                                           currSegment.entryLaneID,
  //                                           currSegment.entryWaypointID);
  //   double interTimer); // Time in seconds since some event.
  // Console::updateInterPage2(
  //   // Vehicles with precedence.
  //   vector<precedenceList_t> obstaclesPrecedence,
  //   // Vehicles that block intersection.
  //   vector<MapElement> obstaclesClearance,
  //   // Vehicles that are merging, or that we merge into, maybe.
  //   vector<MapElement> obstaclesMerging);
}

/**
 * @brief   Called by planner to handle traffic.
 * 
 * @return  An Err_t object (temp-planner-interfaces/PlannerInterfaces.h)
 *       Also, the main result is returned as a StateProblem_t
 *       which is pushed into the problem vector (first argument).
 */
Err_t LtPlanner::planLogic( vector<StateProblem_t*> &problems, PlanGraph *graph,
  Err_t prevErr, VehicleState &vehState, Map *map, Logic_params_t &params,
  int& currentSegmentId, int& currentLaneId, bool& replanInZone ) 
{
  static TrafficData *tada = new TrafficData;
  tada->problems = &problems;
  tada->graph    = graph;
  tada->prevErr  = prevErr;
  tada->vehState = &vehState;
  tada->map      = map;
  tada->params   = &params;
  tada->currentSegmentId = &currentSegmentId;
  tada->currentLaneId    = &currentLaneId;
  tada->replanInZone     = &replanInZone;

  SegGoals intersectionSegGoals;
  // Found this method used in the old LogicPlanner. Stefan uses the other.
  //static double stopline_distance = Utils::getDistToStopline(params.m_path,
  //  params.seg_goal_queue, intersectionSegGoals);
  double stopline_distance = Utils::getDistToStopline(params.m_path);
  Console::updateStopline(stopline_distance);
  double stopped_duration = Utils::monitorAliceSpeed(vehState, params.m_estop);
  Console::updateStoppedDuration(stopped_duration);

  // Find out in what lane and segment we're really.
  // And where we're going, preferably.
  // First guess from input arguments. Usually wrong.
  int segId = currentSegmentId;
  int laneId = currentLaneId;

  LaneLabel currentLaneLabel = Utils::getCurrentLane(vehState, map);
  string laneName = "blank";
  int junk = map->getLaneName(laneName, currentLaneLabel);
  segId = currentLaneLabel.segment;
  laneId = currentLaneLabel.lane;
  string segName = "blank";
  junk = map->getSegmentName(segName, segId);

  char whereAreWe[128];
  sprintf(whereAreWe, "LTP[%ld] Driving on %s. segId = %d, laneId = %d.",
    cycle_counter, segName.c_str(), segId, laneId);
  char whatWentWrong[256];
  sprintf(whatWentWrong, "LTP: Errors from last cycle: %s",
    plannerErrorsToString(prevErr).c_str());

  Log::getStream(4) << whereAreWe << endl << whatWentWrong << endl;

  // Give information about segment goals.
  {
    stringstream strstr;
    params.seg_goal.print(strstr);
    Log::getStream(3) << strstr.str() << endl;
  }

  // Let console users know I'm still running.
  cycle_counter++;
  if (!(cycle_counter % 100)) {
    Console::addMessage(whereAreWe);
    Console::addMessage(whatWentWrong);
  }

  // Actually cause the state machine to step and modify problems.
  Err_t result = tsm->planLogic(tada);

  return result;
}

// EOF 'LtPlanner.cc'
