/*!
 * \file ParseUtils.hh
 * \brief Extras for displaying output, mostly.
 * \author Nicholas Fette
 * \date July 2008
 * \ingroup ParsedSM
 */

#ifndef _PARSE_UTILS_HH_
#define _PARSE_UTILS_HH_

#include <ostream>
#include <vector> 
//#include <iostream>
//#include <ostream>
//#include <stdio.h>
//#include <string>
#include <sstream>
//#include <fstream>

using namespace std;

typedef unsigned short int sint; // repeated from ParsedSM.hh

class MyNullStream : public std::ostringstream {
  public:
    template<typename T> MyNullStream& operator<<(T t) {
      return *this;
    }
};

/*!
 * \brief Handles printing routines requested by lt-planner classes.
 *
 * Handles printing routines requested by lt-planner classes.
 */
class ParseUtils {
  // Suppose we want to print a vector like (1, 2, 3).
  // Begin with "(", separate with ", ", and end with ")".

  //! What does the beginning of the vector look like?
  static string PRETTY_VECTOR_BEGIN;
  //! What does the end of a vector look like?
  static string PRETTY_VECTOR_END;
  //! What do we see between elements of the vector?
  static string PRETTY_VECTOR_SEP;
  //! Where should we send logging information?
  static ostream *logger;
  //! Where should we send logging information that we don't want?
  static MyNullStream nully;
public:
  //! Set the delimiters for printing vectors.
  static void setVectorDelims(string begin, string end, string sep);
  //! Help define operators for vector printing.
  template<class T, class C>
  static ostream & displayVector(ostream &os, const vector<T> &arg);
  //! Help define operators for vector printing.
  static ostream & displayVectorBool(ostream &os, const vector<bool> &arg);
  //! Help define operators for vector printing.
  static ostream & displayVectorChar(ostream &os, const vector<char> &arg);
  //! Help define operators for vector printing.
  static ostream & displayVectorInt(ostream &os, const vector<int> &arg);
  //! Help define operators for vector printing.
  static ostream & displayVectorSint(ostream &os, const vector<sint> &arg);
  //! Help define operators for vector printing.
  static ostream & displayVectorString(ostream &os, const vector<string> &arg);
  //! Compute a hash of a vector<char> for use by examples like timerGame.
  static unsigned int valuation2hash(vector<char> valuation);
  //! Compute a vector<char> from a hash for use by examples like timerGame.
  static vector<char> hash2valuation(unsigned int hash, unsigned int len);
  //! Set the logger. Code using ParseUtils should log to this logger.
  static ostream & setLogger(ostream &os);
  //! Get the logger. Code using ParseUtils should log to this logger.
  static ostream & getLogger();
  
  //! On a scale from least to most of 0 to 9, how verbose to log internal junk?
  static int VERBOSITY;
  //! Returns cerr iff given level <= VERBOSITY, else a black hole.
  static ostream & getCerr(int level);
};

#define PULOG(a) ParseUtils::getCerr(a)
#define PULOGD(a) PULOG(a) << "  "
#define PULOGL(a) PULOG(a) << "LTP: "
#define PULOGF(a) PULOG(a) << "LTP[" << __FUNCTION__ << "] "


#endif // _PARSE_UTILS_HH_
// EOF ParseUtils.hh
