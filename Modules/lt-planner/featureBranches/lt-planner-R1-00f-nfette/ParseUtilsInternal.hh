/*!
 * \file ParseUtilsInternal.hh
 * \brief Extras for displaying output, mostly.
 * \author Nicholas Fette
 * \date July 2008
 *
 * "Internal" because it otherwise globally redefines operator<< on vectors.
 */

#ifndef _PARSE_UTILS_INTERNAL_HH_
#define _PARSE_UTILS_INTERNAL_HH_

#include <vector>  // for vector
#include <ostream> // for ostream

using namespace std; 

#ifndef VECTOR_OFF
ostream &operator<<(ostream &os, const vector<bool> &arg);
ostream &operator<<(ostream &os, const vector<char> &arg);
ostream &operator<<(ostream &os, const vector<int> &arg);
ostream &operator<<(ostream &os, const vector<string> &arg);
#else
extern ostream &operator<<(ostream &os, const vector<bool> &arg);
extern ostream &operator<<(ostream &os, const vector<char> &arg);
extern ostream &operator<<(ostream &os, const vector<int> &arg);
extern ostream &operator<<(ostream &os, const vector<string> &arg);
#endif

#endif // _PARSE_UTILS_INTERNAL_HH_
// EOF ParseUtilsInternal.hh
