/*!
 * \file TrafficSM.hh
 * \brief Define the interface for TrafficSM.
 *
 * \author Nicholas Fette
 * \date July 2008.
 * 
 * \ingroup TrafficSM
 *
 * see DGC07:Lt-planner
 */

#ifndef _TRAFFIC_SM_HH_
#define _TRAFFIC_SM_HH_
#include "ParsedSM.hh" // Defines ParsedSM, SMState, Environment, and System.
// REMOVE #include <cmath> // For pow(base, exp) in hash.
#include <map>   // For, well, you guessed it.

// Unfortunately, I can't compile standalone any longer.
// This includes StateProblem_t, Err_t, point2, VehicleState, Logic_params_t, ...
#include <temp-planner-interfaces/PlannerInterfaces.h>
// For DGCgettime(unsigned long long&), DGCgettime(), DGCtimetosec(.,.);
#include <dgcutils/DGCutils.hh>
// For AliceStateHelper::getPositionFrontBumper
#include <temp-planner-interfaces/AliceStateHelper.hh>

using namespace std;

//! Helps make a map of the input functions.
struct strCmp {
  bool operator() (const string a, const string b) const {
    return (strcmp(a.c_str(), b.c_str()) > 0);
  }
};

//! Helps pass data from Planner to LtPlanner to TrafficSM.
typedef struct {
  //! A vector with usually one scenario for the path planner.
  vector<StateProblem_t*> *problems;
  //! The graph of something.
  PlanGraph *graph;
  //! The errors from the previous Planner cycle.
  Err_t prevErr;
  //! The state of the vehicle. (Maybe position, transmission, steering?)
  VehicleState *vehState;
  //! Map represents the conglomeration of sensing data which should
  //! represent the surrounding landscape.
  Map *map;
  //! Just some extra, but useful stuff added on by Planner.
  Logic_params_t *params;
  //! According to MissionPlanner, where we are driving, as in 1.2.1.
  int *currentSegmentId;
  //! According to MissionPlanner, which lane we drive in, as in 0, 1.
  int *currentLaneId;
  //! Whether to do something vague, according to Jeremy. (ignore that)
  bool *replanInZone; // Not needed right now.

  // "Derived spatial data" (Stefan)
  double currentVelocity;
  double stoplineDistance;
  double obstacleDistance;
  double exitDistance;
  double obstacleOnPath;
  double obstacleInLane;
  double stoppingDistance;
  double obsSide;
  double obsRear;
  double obsFront;
  LaneLabel currentLane;
  LaneLabel desiredLane;
  
  //! Based on Console input, should we freeze the state machine?
  bool freeze;

  // Config parameters
  float TIMERLEN; // seconds
  float POSTIMERLEN; // meters

} TrafficData; // for inputs

//! Intermediate between state machine and me, holding output variables.
typedef struct {
  // Elements of a StateProblem_t.
  double         probability; // in [0, 1], such as 0.99
  FSM_state_t    state;       // DRIVE STOP_INT STOP_OBS BACKUP UTURN PAUSE
  FSM_region_t   region;      // ZONE_REGION ROAD_REGION (INTERSECTION)
  FSM_flag_t     flag;        // PASS NO_PASS OFFROAD PASS_REV
  FSM_planner_t  planner;     // RAIL_PLANNER S1~ D~ CIRCLE_~ RRT_~
  FSM_obstacle_t obstacle;    // OBSTACLE_SAFETY ~_AGGRESSIVE ~_BARE
  // Whether to generate a new graph on the road.
  bool planFromCurrPos;
  // Whether to generate a new graph in the zone.
  bool replanInZone;
  // Message to send to planner, who called planLogic.
  Err_t          error;       // Such as LP_OK

  // Set all the fields in the argument, like a memcpy.
  void toStateProblem_t(StateProblem_t* p) {
    p->probability = probability;
    p->state       = state;
    p->region      = region;
    p->flag        = flag;
    p->planner     = planner;
    p->obstacle    = obstacle;
  }
} TSMout;

//! Timer for use in limiting state transition rates and so on.
class TrafficTimer {
private:
	unsigned long long timeStamp;
	point2 posStamp;
	//float xPosStamp;
	//float yPosStamp;
public:
  //! Initialize to here and now.
  TrafficTimer() : timeStamp(0), posStamp() { }
  //! Set the time to now.
  void setTime() {DGCgettime(timeStamp);}
  //! Return the latest time stamp. Probably shouldn't use this.
  // float getTime() {return (float)(timeStamp - bornOn);}
  //! Return the difference between now and the latest time stamp [s].
  float getTimeElapsed() {
    return ((float)(DGCgettime() - timeStamp)) * 1e-6;
  }
  
  //! Set the position stamp to here.
  void setPos(TrafficData *td) {posStamp = getVehPos(td);}
  //! Return the latest position stamp (point).
  point2 getPos() {return posStamp;}
  //! Return the difference between here and the latest position stamp (vector).
  point2 getPosElapsed(TrafficData *td) {
  	return getVehPos(td) - posStamp;
  }
  float getDistance(TrafficData *td) {
  	return (float)getVehPos(td).dist(posStamp);
  }
  
  //! Helper function to return the position of Alice's front bumper,
  //! probably in the local frame.
  point2 getVehPos(TrafficData *td) {
    if (td == NULL || td->vehState == NULL) {
      // Make some kind of warning?
      return point2();
    } else {
      return AliceStateHelper::getPositionFrontBumper(*(td->vehState));
    }
  }
  
}; // TrafficTimer, which is unrelated to stop lights and other such timers :)

class TrafficSM;
class TrafficStop;

// Usage, for example: "EnvFunctor a = TrafficStop::isJustBeforeStopSign;".
typedef char (Environment::*EnvFunctor) (TrafficData *);

//! A generic representation of a state machine for planning traffic logic.
class TrafficSM {
	public:
	  //! The transition rules.
    ParsedSM *sam;

  protected:
    //! Map from input names to boolean function pointers.
    map<string, EnvFunctor, strCmp> envMap;
    //! Default input value in case an input name is not recognized.
    EnvFunctor envDefaultInput;
    //! Fast access to ordered input functions.
    //! You must setInputNames before using this.
    vector<EnvFunctor> envInputFuncs;
    //! Map from output names to boolean values.
    map<string, char, strCmp> sysMap;
    //! Stores state machine's output variables in a more useful form.
    //TSMout to;
    //! Keeps track of recent state positions and times (cf mod-log-planner).
    //TrafficTimer stateTimer;
  public:
    //! Create a Traffic State Machine that can't do anything.
    TrafficSM();
    //! Destroy a Traffic State Machine (using high/noisy voltage).
    virtual ~TrafficSM();
    //! Make state transitions and prepare output. (Name is legacy.)
    virtual Err_t planLogic(TrafficData *tada) = 0; 
    //! The most recent, uh, status.
    StateProblem_t current_status;
    //! Expose inner workings.
    TSMout to;
    virtual string getStateString() = 0;
    static char *DEFAULT_PSM_FILE;
    static TrafficSM* getDefaultTrafficSM();
    string intersectionStringForConsole;
};

// I changed my mind about it not being static; what if I change again?
#define VARTYPE char

//! A basic implementation of a TrafficSM setup with a ParsedSM.
class TrafficStop : public TrafficSM, public Environment, public System {
  private:
    //ParsedSM *sam;
    //! How the Environment evaluates its input variables.
    //! Each valid name is mapped to a functor.
    //! Valid names are assigned in the constructor.
    map<const string, EnvFunctor, strCmp> inputMap;
    TrafficData *tada;
    //! Track position and times as variables specifically requested by states. 
    TrafficTimer varTimer;
    
  public:
    bool destroySamOnExit;
    TrafficStop(ParsedSM *psm);
    ~TrafficStop();
    //! Gets the state machine which is supposed to be hidden.
    ParsedSM* getPSM();
    //! Gets a string which conveys information about state.
    string getStateString();

    //! Gets called by LtPlanner. In turn calls execute().
    Err_t planLogic(TrafficData *tada); 

    //! Override inherited methods for Environment.
    const vector<string> &getInputNames();
    const vector<char> &getInputs();
    const vector<char> &getInputs(const vector<string>& varnames);

    //! Override inherited methods for System.
    void execute(const vector<char>& outputVals,
      const vector<string>& outputNames);


    // Tests that will become state machine input.
    //! I did define this, didn't I?
    VARTYPE isJustBeforeStopSign(TrafficData *td);
    VARTYPE isNowStopped(TrafficData *td);

    //! Not very well-defined, is it?
    VARTYPE isObstacleInWay(TrafficData *td);

    //! Straight out of gcinterface/SegGoals/SegmentType, and not undefined.
    VARTYPE isSegmentRoad(TrafficData *td);
    VARTYPE isSegmentZone(TrafficData *td);
    VARTYPE isSegmentIntersection(TrafficData *td);
    VARTYPE isSegmentPreZone(TrafficData *td);
    VARTYPE isSegmentUTurn(TrafficData *td);
    VARTYPE isSegmentDriveAround(TrafficData *td);
    VARTYPE isSegmentBackUp(TrafficData *td);
    VARTYPE isSegmentEndOfMission(TrafficData *td);
    VARTYPE isSegmentEmergencyStop(TrafficData *td);
    VARTYPE isSegmentUnknown(TrafficData *td);
    VARTYPE isSegmentStartChute(TrafficData *td);

    //! Straight out of gcinterface/SegGoals/IntersectionType.
    VARTYPE isIntersectionStraight(TrafficData *td);
    VARTYPE isIntersectionLeft(TrafficData *td);
    VARTYPE isIntersectionRight(TrafficData *td);

    //! Deliver us from error. temp-planner-interfaces/PlannerInterfaces.h
    VARTYPE isErrorNone(TrafficData *td);
    VARTYPE isErrorNoPath(TrafficData *td);
    
    //! Let's use enum types, or something similar, everybody!
    //! These give only internal values, as given by synthesis.
    VARTYPE whichSegment(TrafficData *td);
    VARTYPE whichIntersection(TrafficData *td);

    //! Is there an obstacle close but not dangerously so?
    VARTYPE isObsCozy(TrafficData *td);
    //! Is there an obstacle dangerously close?
    VARTYPE isObsTooClose(TrafficData *td);
    //! Is the previous path going to collide with something?
    VARTYPE isErrorCollision(TrafficData *td);
    
    //! True when the simple timer times out.
    VARTYPE isTimerUp(TrafficData *td);
    //! True when Alice has moved since reseting the spacial timer.
    VARTYPE isSpacialTimerUp(TrafficData *td);
    
    
};


#endif // _TRAFFIC_SM_HH_
// EOF TrafficSM.hh
