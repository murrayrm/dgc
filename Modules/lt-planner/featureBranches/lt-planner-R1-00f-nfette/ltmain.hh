#ifndef _LTMAIN_HH_
#define _LTMAIN_HH_

#define MY_VERSION __DATE__ " " __TIME__

#define LLOG(a) Log::getStream(a)
#define LLOGL(a) LLOG(a) << "LTP: "
#define LTPLOG(a) LLOG(a) << "LTP[" << __FUNCTION__ << "] "
#define LLOGD(a) LLOG(a) << "  "

// Convenience trick for "name and value". Good for C++ only, really.
#define NAV(a) #a << " = " << a
// Value and name, for reading configs.
#define VAN(a) a, #a

#include "temp-planner-interfaces/PlannerInterfaces.h" // for Log, etc.

using namespace std;

/* Static variables for fun. */
static char liblt_version[100] = MY_VERSION;

#endif // _LTMAIN_HH
