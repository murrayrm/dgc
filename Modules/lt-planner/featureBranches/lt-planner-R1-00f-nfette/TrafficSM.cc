/*!
 * \file TrafficSM.cc
 * \brief Define the interface for TrafficSM.
 * \author Nicholas Fette
 * \date July 2008
 *
 * \ingroup TrafficSM
 *
 * see DGC07:Lt-planner
 */

#include <ltmain.hh> // for LLOG, LLOGL, LTPLOG, LLOGD
#include "TrafficSM.hh"
#include "ParseUtils.hh"         // For ParseUtils static object.
#include "ParseUtilsInternal.hh" // for operator<< using ParseUtils.

#include "temp-planner-interfaces/Console.hh" // for Console
#include "temp-planner-interfaces/Utils.hh"   // for Utils
#include "temp-planner-interfaces/AliceStateHelper.hh" // For velocityMag.
// REMOVE #include <deque>
#include <sstream>

#define ltlPause 0
#define ltlDrive 1
#define ltlStopInt 2
#define ltlStopObs 3
#define ltlBackUpMode 4
#define ltlUTurnMode 5

#define ltlRoadRegion 0
#define ltlZoneRegion 1

#define ltlNoPass 0
#define ltlPass 1
#define ltlPassRev 2
#define ltlOffRoad 3

#define ltlRails 0
#define ltlSTraj 1
#define ltlDTraj 2
#define ltlCTraj 3
#define ltlRTraj 4

#define ltlSafety 0
#define ltlAggressive 1
#define ltlBare 2

#define ltlRoad 0
#define ltlZone 1
#define ltlIntersection 2
#define ltlPreZone 3
#define ltlUTurn 4
#define ltlDriveAround 5
#define ltlBackUp 6
#define ltlEndOfMission 7
#define ltlEmergencyStop 8
#define ltlUnknown 9
#define ltlStartChute 10 // don't use until syntax changes!!!!!!

#define ltlStraight 0
#define ltlLeft 1
#define ltlRight 2

//! This is supposed to be a virtual class!
TrafficSM::TrafficSM () {
  // Nothing to do here.
}

char *TrafficSM::DEFAULT_PSM_FILE = NULL;

//! This is supposed to be a virtual class!
TrafficSM::~TrafficSM () {
  // Nothing to do here.
}

/*!
 * \brief Get a "default" traffic state machine.
 *
 * Use this to set up a "default" traffic state machine, presumably
 * from a single text file described in the configuration file, such as
 * with the parameter DEFAULT_PSM_FILE = "default.psm".
 */
TrafficSM* TrafficSM::getDefaultTrafficSM() {
	//cout << "LTP: Loading state machines." << endl;
	unsigned long long hourglass1, hourglass2;
  DGCgettime(hourglass1);
  
  ParsedSM *sam = new ParsedSM(DEFAULT_PSM_FILE);
  sam->init();
  TrafficStop *ts = new TrafficStop(sam);
  ts->destroySamOnExit = true;
  
  DGCgettime(hourglass2);
  float hourglass = (hourglass2 - hourglass1) * 1.0e-6;
  cout << "State machines loaded in " << hourglass << " seconds." << endl;
  LLOGD(7) << "State machines loaded in " << hourglass << " seconds." << endl;
  
  return ts;
}

TrafficStop::TrafficStop(ParsedSM *psm) : Environment(), System()
{
	sam = psm;
	LTPLOG(7) << endl;
  // Must do these things for object structure to work.
  sam->setEnvironment(this);
  sam->setSystem(this);
  destroySamOnExit = false;

  // Init current status, complete state representation
  current_status.state       = DRIVE;
  current_status.probability = 0.99;
  current_status.flag        = NO_PASS;
  current_status.region      = ROAD_REGION;
  current_status.planner     = RAIL_PLANNER;
  current_status.obstacle    = OBSTACLE_SAFETY;

  // Define the input map
  inputMap["isJustBeforeStopSign"]    = (EnvFunctor) &TrafficStop::isJustBeforeStopSign;
  inputMap["isNowStopped"]            = (EnvFunctor) &TrafficStop::isNowStopped;
  inputMap["isErrorNone"]             = (EnvFunctor) &TrafficStop::isErrorNone;
  inputMap["isErrorNoPath"]           = (EnvFunctor) &TrafficStop::isErrorNoPath;

  inputMap["whichSegment"]            = (EnvFunctor) &TrafficStop::whichSegment;
  inputMap["whichIntersection"]       = (EnvFunctor) &TrafficStop::whichIntersection;

  inputMap["isObsCozy"]               = (EnvFunctor) &TrafficStop::isObsCozy;
  inputMap["isObsTooClose"]           = (EnvFunctor) &TrafficStop::isObsTooClose;
  inputMap["isErrorCollision"]        = (EnvFunctor) &TrafficStop::isErrorCollision;
  inputMap["isTimerUp"]               = (EnvFunctor) &TrafficStop::isTimerUp;
  inputMap["isSpacialTimerUp"]        = (EnvFunctor) &TrafficStop::isSpacialTimerUp;

  // consider setting the time here.
}

// This had better not leak, or I'll add extra sealant!
TrafficStop::~TrafficStop() {
  if (destroySamOnExit) {
    delete sam;
  }
}

// Expose inner workings for useful information. Maybe should be const.
ParsedSM * TrafficStop::getPSM() {
  return sam;
}

string TrafficStop::getStateString() {
  char result[16];
  sprintf(result, "%d", sam->getState());
  return string(result);
}

// This is where I should put the console messages, logging, and anything
// that doesn't need to be in the state machine. 
Err_t TrafficStop::planLogic( TrafficData *arg )
{
  // Log some messages.
  {
  Log::getStream(7) << "LTP: In TrafficStop::planLogic()" << endl;
  char msg[80];
  if (isObsCozy(arg)) {
    sprintf(msg, "LTP: An obstacle is getting cozy: %d", isObsCozy(arg));
    //Console::addMessage(msg);
    Log::getStream(4) << msg << endl;
  }
  if (isObsTooClose(arg)) {
    sprintf(msg, "LTP: An obstacle is too close: %d", isObsTooClose(arg));
    //Console::addMessage(msg);
    Log::getStream(4) << msg << endl;
  }
  if (isErrorCollision(arg)) {
    char msg[] = "LTP: An obstacle collides on the path ahead.";
    //Console::addMessage(msg);
    Log::getStream(4) << msg << endl;
  }

  }
  
  // I call other functions that need the traffic data.
  tada = arg;
  sam->freeze = tada->freeze;

  // Step the state machine. See ParsedSM::step near ParsedSM.cc:310.
  // This calls execute, by the way.
  sam->step();

  // We're going to modify tada->problems.
  StateProblem_t *p = new StateProblem_t();
  // Give it some values based on the outputs.
  to.toStateProblem_t(p);
  // Push new problem into queue. We can modify it there.
  tada->problems->push_back(p);
  // Change the variables we use for printing.
  to.toStateProblem_t(&current_status);

  // Don't always replan from current position.
  tada->params->planFromCurrPos = to.planFromCurrPos;
  // Same thing, but when in a parking zone.
  *(tada->replanInZone) = to.replanInZone;

  return to.error;
}

const vector<string>& TrafficStop::getInputNames() {
  return sam->getInputNames();
}

// Get all the input values in default order.
const vector<char>& TrafficStop::getInputs() {
  return getInputs(getInputNames());
}

// Get the named inputs in their order, or false for an unkown name.
const vector<char>& TrafficStop::getInputs(const vector<string>& names) {
  envInputVals.clear();
  for (int i = 0; i < (int)names.size(); i++) {
    // Evaluate one of the functions in the input map, using my TrafficData.
    EnvFunctor myfunc = inputMap[names[i]];
    if (myfunc != NULL) {
      envInputVals.push_back((this->*myfunc)(tada));
    } else {
      envInputVals.push_back(0);
      // SHOULDN'T HAPPEN: Programmer made a mistake.
      // But, just in case, don't let's execute a null pointer (hehe).
      // Issue a warning and die gracefully. Redundancy makes things look better.
      Log::getStream(-1) << "LTP[" << __FUNCTION__
        << "]: Panic because inputMap[\""<< names[i] <<"\"] == NULL!" << endl;
      cerr << "LTP[" << __FUNCTION__
        << "]: Panic because inputMap[\""<< names[i] <<"\"] == NULL!" << endl;
      assert(myfunc != NULL);
    }
  }
  return envInputVals;
}

// Take any appropriate action after state change.
// Ie, set up output variables as needed.
// Avoid anything dependent on other modules.
void TrafficStop::execute(const vector<char>& outputVals,
  const vector<string>& outputNames) {
  Log::getStream(7) << "LTP: In TrafficStop::execute()" << endl;
  // Just to keep track of cycles.
  static int cycleCounter = -1;
  cycleCounter += 1;
  // At which cycle did we make the latest transition?
  static int latestTransitionCycle = -1;

  // Keep Console and Log informed about the state.
  // Actually, this is really annoying on the console.
  // And besides, mapviewer shows the "state" now.
  ostringstream oss;
  static int myLastState = sam->getState();
  int myNewState = sam->getState();
  oss << "LTP->tsm State transition: " << myLastState << " -> "
    << myNewState << " <";
  for (int i = 0; i < (int)outputVals.size(); i++) {
    oss << (int)outputVals[i];
  }
  oss << ">";
  // If we've just made a transition:
  if (myLastState != myNewState) {
    latestTransitionCycle = cycleCounter;
    //Console::addMessage("%s", oss.str().c_str());
    myLastState = myNewState;
  }
  Log::getStream(5) << oss.str() << endl;
  Log::getStream(10) << "LTP->tsm State: ";
  sam->displayState(Log::getStream(10)) << endl;

  // I keep changing my mind about how I want to store these values :)
  for (int i = 0; i < (int) outputVals.size(); i++) {
    sysMap[outputNames[i]] = outputVals[i];
  }

  // Load up the output variables. planLogic will do the rest.
  { // Do it using a mix of booleans and enums.
  // Note that, when asking a map for a key that doesn't exist yet,
  // the map will initialize a pair, and if the value type is bool,
  // the value gets initialized to 0 (false).
  // Likewise for char, but I'd rather initialize to undefined, like -1.

  // Set "probability", as in yet to be defined.
  // This will not be determined by the state machine, but by me,
  // probably depending on the probability of inputs.
  to.probability = 0.99;
  // Set "state", as in mode.
  // Unfortunately, I don't think JTLV can handle integers.
  // Maybe I just haven't tried it yet.
  // Oh, but it can handle enum types! Let's try it.
  if (sysMap["useMode"] == ltlDrive) {
    to.state = DRIVE;
  } else if (sysMap["useMode"] == ltlStopInt) {
    to.state = STOP_INT;
  } else if (sysMap["useMode"] == ltlStopObs) {
    to.state = STOP_OBS;
  } else if (sysMap["useMode"] == ltlBackUpMode) {
    to.state = BACKUP;
  } else if (sysMap["useMode"] == ltlUTurnMode) {
    to.state = UTURN;
  } else if (sysMap["useMode"] == ltlPause) {
    to.state = PAUSE;
  } else {
  	// Shouldn't happen. Log an runtime error. Maybe also give up.
    to.state = PAUSE;
  }
  
  // Set "region", as in segment.
  if (sysMap["useRegion"] == ltlZoneRegion) {
    to.region = ZONE_REGION;
  } else if (sysMap["useRegion"] == ltlRoadRegion) {
    to.region = ROAD_REGION;
  // I am told that this isn't even implemented by any trajectory planners.
  // } else if (sysMap["intersection"]) {
  //   to.region = INTERSECTION;
  } else {
  	// This shouldn't happen. Log a runtime error and give up.
    to.region = ROAD_REGION;
  }
  
  // Set "flag", as in allowed maneuvers.
  if (sysMap["useFlag"] == ltlPass) {
    to.flag = PASS;
  } else if (sysMap["useFlag"] == ltlNoPass) {
    to.flag = NO_PASS;
  } else if (sysMap["useFlag"] == ltlOffRoad) {
    to.flag = OFFROAD;
  } else if (sysMap["useFlag"] == ltlPassRev) {
    to.flag = PASS_REV;
  } else {
  	// Shouldn't happen; log runtime error, give up!
    to.flag = NO_PASS;
  }
  
  // Set "planner", as in trajectory generation tool.
  if (sysMap["useTraj"] == ltlRails) {
    to.planner = RAIL_PLANNER;
  } else if (sysMap["useTraj"] == ltlSTraj) {
    to.planner = S1PLANNER;
  } else if (sysMap["useTraj"] == ltlDTraj) {
    to.planner = DPLANNER;
  } else if (sysMap["useTraj"] == ltlCTraj) {
    to.planner = CIRCLE_PLANNER;
  } else if (sysMap["useTraj"] == ltlRTraj) {
    to.planner = RRT_PLANNER;
  } else {
  	// Shouldn't happen; log runtime error, give up! Eventually.
    to.planner = RAIL_PLANNER;
  }
  
  // Set "obstacle", as in how much safety margin to keep around us.
  if (sysMap["useMargin"] == ltlSafety) {
    to.obstacle = OBSTACLE_SAFETY;
  } else if (sysMap["useMargin"] == ltlAggressive) {
    to.obstacle = OBSTACLE_AGGRESSIVE;
  } else if (sysMap["useMargin"] == ltlBare) {
    to.obstacle = OBSTACLE_BARE;
  } else {
  	// Shouldn't happen! TODO: log runtime error, and give up.
    to.obstacle = OBSTACLE_SAFETY;
  }
  
  // Set "planFromCurrPos", a mystery boolean.
  to.planFromCurrPos = sysMap["doPlanFromCurrPos"];
  // Set "replanInZone", another mystery boolean.
  to.replanInZone = sysMap["doReplanInZone"];

  // TODO Set "error", as in what went wrong this planning cycle.
  to.error = LP_OK;
  // coming soon
  if (sysMap["doReportLaneBlocked"]) {
    to.error |= LP_FAIL_MPLANNER_LANE_BLOCKED;
  } 
  if (sysMap["doReportRoadBlocked"]) {
     to.error |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
  }
  if (sysMap["doReportMapIncomplete"]) {
    to.error |= LP_MAP_INCOMPLETE;
  }
  if (sysMap["doReportLost"]) {
    to.error |= LP_FAIL_MPLANNER_LOST;
  }

  if (sysMap["doSetTimer"]) {
    varTimer.setTime();
  }
  if (sysMap["doSetSpacialTimer"]) {
    varTimer.setPos(tada);
  }

  } // End loading up the output variables, bool and char.
  
  // All done!
  return;

} // execute 


// Input variable functions.

//! Are we close enough to a stop sign that we should slow down?
//! Don't filter anything, that's my job.
char TrafficStop::isJustBeforeStopSign(TrafficData *td) {
  double current_velocity = AliceStateHelper::getVelocityMag(*(td->vehState));
  double stopping_distance = pow(current_velocity,2)
    / (2.0 * 0.25); // DESIRED_DECELERATION
  stopping_distance = max ( stopping_distance, 5.0 ); // MAGIC
  double stopline_distance = Utils::getDistToStopline(td->params->m_path);
  return (stopline_distance > 0.0)
         && ((stopline_distance < stopping_distance)
            || (stopline_distance < 5));
}

//! Are we stopped?
char TrafficStop::isNowStopped(TrafficData *td) {
  double currentVelocity = AliceStateHelper::getVelocityMag(*(td->vehState));
  // Warning: magic number. Defined in, oh, every other module, SEPARATELY!
  return currentVelocity < 0.2;
}

//! Not very well-defined, is it?
char TrafficStop::isObstacleInWay(TrafficData *td) {
  return false;
}

//! Straight out of gcinterface/SegGoals/SegmentType.
char TrafficStop::isSegmentRoad(TrafficData *td) {
  return td->params->segment_type == SegGoals::ROAD_SEGMENT;
}

char TrafficStop::isSegmentZone(TrafficData *td) {
  return td->params->segment_type == SegGoals::PARKING_ZONE;
}

char TrafficStop::isSegmentIntersection(TrafficData *td) {
  return td->params->segment_type == SegGoals::INTERSECTION;
}

char TrafficStop::isSegmentPreZone(TrafficData *td) {
  return td->params->segment_type == SegGoals::PREZONE;
}

char TrafficStop::isSegmentUTurn(TrafficData *td) {
  return td->params->segment_type == SegGoals::UTURN;
}

char TrafficStop::isSegmentDriveAround(TrafficData *td) {
  return td->params->segment_type == SegGoals::DRIVE_AROUND;
}

char TrafficStop::isSegmentBackUp(TrafficData *td) {
  return td->params->segment_type == SegGoals::BACK_UP;
}

char TrafficStop::isSegmentEndOfMission(TrafficData *td) {
  return td->params->segment_type == SegGoals::END_OF_MISSION;
}

char TrafficStop::isSegmentEmergencyStop(TrafficData *td) {
  return td->params->segment_type == SegGoals::EMERGENCY_STOP;
}

char TrafficStop::isSegmentUnknown(TrafficData *td) {
  return td->params->segment_type == SegGoals::UNKNOWN;
}

char TrafficStop::isSegmentStartChute(TrafficData *td) {
  return td->params->segment_type == SegGoals::START_CHUTE;
}

//! Straight out of gcinterface/SegGoals/IntersectionType.
char TrafficStop::isIntersectionStraight(TrafficData *td) {
  return td->params->seg_goal.intersection_type
    == SegGoals::INTERSECTION_STRAIGHT;
}

char TrafficStop::isIntersectionLeft(TrafficData *td) {
  return td->params->seg_goal.intersection_type
    == SegGoals::INTERSECTION_LEFT;
}

char TrafficStop::isIntersectionRight(TrafficData *td) {
  return td->params->seg_goal.intersection_type
    == SegGoals::INTERSECTION_RIGHT;
}

//! Deliver us from error. temp-planner-interfaces/PlannerInterfaces.h
char TrafficStop::isErrorNone(TrafficData *td) {
  return (td->prevErr == LP_OK);
}

//! Are we lost? Usually this happens when rail planner goes out of the road,
//! or if another planner doesn't have enough time to find a path.
char TrafficStop::isErrorNoPath(TrafficData *td) {
  return ((td->prevErr & PP_NOPATH_LEN) != 0);
}

//! Not related to anything, only internal values from synthesis program.
char TrafficStop::whichSegment(TrafficData *td) {
  // This implementation is ugly.
  // This much really should be generated automatically.
  
  if (isSegmentRoad(td)) {
    return ltlRoad;
  } else if (isSegmentZone(td)) {
    return ltlZone;
  } else if (isSegmentIntersection(td)) {
    return ltlIntersection;
  } else if (isSegmentPreZone(td)) {
    return ltlPreZone;
  } else if (isSegmentUTurn(td)) {
    return ltlUTurn;
  } else if (isSegmentDriveAround(td)) {
    return ltlDriveAround;
  } else if (isSegmentBackUp(td)) {
    return ltlBackUp;
  } else if (isSegmentEndOfMission(td)) {
    return ltlEndOfMission;
  } else if (isSegmentEmergencyStop(td)) {
    return ltlEmergencyStop;
  } else if (isSegmentUnknown(td)) {
    return ltlUnknown;
  } else if (isSegmentStartChute(td)) {
    return ltlStartChute;
  } else {
    return -1;
  }
}

//! Not related to anything, only internal values from synthesis program.
char TrafficStop::whichIntersection(TrafficData *td) {
  // This should be generated automatically.
  if (isIntersectionStraight(td)) {
    return ltlStraight;
  } else if (isIntersectionLeft(td)) {
    return ltlLeft;
  } else if (isIntersectionRight(td)) {
    return ltlRight;
  } else {
    // This should never happen.
    return -1;
  }
}

// Various ways of detecting obstacles, from mod-log-planner/QHSM21_drive.cc
char TrafficStop::isObsCozy(TrafficData *td) {
  // TODO: replace magic numbers (with something less magical).
  //return (td->obstacleOnPath >= 16 && td->obstacleOnPath < 20);
  return (td->obstacleDistance >= 16 && td->obstacleDistance < 20);
}
char TrafficStop::isObsTooClose(TrafficData *td) {
  //return (td->obstacleOnPath < 16);
  return (td->obstacleDistance < 16);
}
char TrafficStop::isErrorCollision(TrafficData *td) {
  return ((td->prevErr & PP_COLLISION_SAFETY) != 0);
}

char TrafficStop::isTimerUp(TrafficData *td) {
  return 0;
  // timer is running in microseconds, ie 1e-6
  return (varTimer.getTimeElapsed() < td->TIMERLEN);
}

char TrafficStop::isSpacialTimerUp(TrafficData *td) {
  return 0;
  // timer is running in meters, ie 1e0
  return (varTimer.getDistance(td) < td->POSTIMERLEN);
}

// EOF TrafficSM.cc
