#include <iostream>
#include <ltmain.hh>

using namespace std;

int main(int argc, char **argv) {
  cout <<
"This should be liblt-planner, version \"" << liblt_version << "\",\n"
"the library for LTL-based state machines applied to traffic \"planning\",\n"
"ie for use with TeamCaltech's planner. By Nicholas Fette.\n";
  char var_char = 0;
  int var_int = 1;
  unsigned int var_uint = 2;
  short var_short = 3;
  unsigned short var_ushort = 4;
  long int var_lint = 5;
  long long int var_llint = 6; 
  
  cout << NAV(sizeof(var_char)) << endl
    << NAV(sizeof(var_int)) << endl
    << NAV(sizeof(var_uint)) << endl
    << NAV(sizeof(var_short)) << endl
    << NAV(sizeof(var_ushort)) << endl
    << NAV(sizeof(var_lint)) << endl
    << NAV(sizeof(var_llint)) << endl;
  return 0;
}
