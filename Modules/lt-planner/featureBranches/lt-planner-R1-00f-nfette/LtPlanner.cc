/*!
 * \file LtPlanner.cc
 * \brief Definitions LtPlanner
 *
 * \author Nicholas Fette (original based on ModLogPlanner by Stefan Skoog)
 * \date July 2008
 *
 * \ingroup lt-planner
 *
 */

/*********************************
 * Include header
 *********************************/
#include "LtPlanner.hh"      // Declarations for this class (top level).
#include "temp-planner-interfaces/Console.hh" // for Console.
#include "temp-planner-interfaces/Utils.hh"   // for Utils.
#include "temp-planner-interfaces/ConfigFile.hh" // for ConfigFile.
#include "dgcutils/cfgfile.h" // for dgcFindConfigFile().
#include <sstream>  // for stringstream
#include "ParseUtils.hh" // for ParseUtils::setLogger, log(), or something?
#include <temp-planner-interfaces/AliceStateHelper.hh> // for AliceStateHelper
#include "ltmain.hh" // for liblt_version and NAV macro
/*********************************
* Initiate (static) class variables
**********************************/

#ifdef FAIL_ON_RESET
//! Just for fun; hit 's' in planner sometime.
static int fail_on_reset(int a, char b) {
	assert(0);
}
#endif

/*********************************
 * Methods
 *********************************/

LtPlanner::LtPlanner() 
{
  // In case we need a new constructor, call alternative init function.
  init();
}

/**
 * @brief   Lt PLanner initializer
 *      Reads config file, resets internal variables,
 *      creates LT State Machine if necessary.
 */
void LtPlanner::init() 
{
	cerr << "LTP[init] Nominal library version: " << liblt_version << endl;
  // Code using ParseUtils will print to the right place, roughly.
  ParseUtils::setLogger(Log::getStream(5));
  LTPLOG(0) << " Nominal library version " << liblt_version << endl;
  // Read config file and save into class variables.
  readConfig();
  char path[128] = "../lt-planner/default.psm";
  strcpy(path, DEFAULT_PSM_FILE.c_str());
  TrafficSM::DEFAULT_PSM_FILE =  dgcFindConfigFile(path, "lt-planner");
  //LLOGD(5) << "TrafficSM::DEFAULT_PSM_FILE = " << 
  //  TrafficSM::DEFAULT_PSM_FILE << endl;
  LLOGD(5) << NAV(TrafficSM::DEFAULT_PSM_FILE) << endl;

  
  // Count how many times planLogic gets called.
  cycle_counter = 0;

  // Init current status, complete state representation
  current_status.state       = DRIVE;
  current_status.probability = 0.99;
  current_status.flag        = NO_PASS;
  current_status.region      = ROAD_REGION;
  current_status.planner     = RAIL_PLANNER;
  current_status.obstacle    = OBSTACLE_SAFETY;
  
  replanFromConsole = false;
  
  cout << "LTP[" << __FUNCTION__ << "] Loading state machines." << endl;
  LTPLOG(0) << "Any debug info will be sent to stderr." << endl;
  // Load all state machines into a map from name to TrafficSM.
  LTPLOG(0) << "Loading extra state machines, namely ";
  // Just for fun, eh?
  vector<string> psmfiles;
  istringstream psmfilelist(ALL_PSM_FILES);
  string psmfile;
  while (psmfilelist >> psmfile) {
    psmfiles.push_back(psmfile);
    LLOG(0) << psmfile << ", ";
  }
  LLOG(0) << "(" << psmfiles.size() << ")" << endl;
  for (int i = 0; i < (int)psmfiles.size(); i++) {
  	try {
    	psmfile = psmfiles.at(i);
    	strcpy(path, psmfile.c_str());
      char *psmfileabs =  dgcFindConfigFile(path, "lt-planner");
      LTPLOG(0) << " Loading file " << psmfile
        << " (" << psmfileabs << ")" << endl; 
      unsigned long long hourglass1, hourglass2;
      DGCgettime(hourglass1);

      ParsedSM *samurai = new ParsedSM(psmfileabs);
      samurai->init();
      TrafficStop *tseliot = new TrafficStop(samurai);
      tseliot->destroySamOnExit = true;
      allmachines[samurai->getName()] = samurai;

      DGCgettime(hourglass2);
      float hourglass = (hourglass2 - hourglass1) * 1.0e-6;
      LTPLOG(0) << "Loaded machine " << samurai->getName()
        << " in " << hourglass << " seconds." << endl;
        
      LLOGD(0) << "It was this big: " << sizeof(*samurai) << endl;
      LLOGD(0) << "And the TrafficStop holding it is this big: "
        << sizeof(*tseliot) << endl;
        
    } catch (SMException &e) {
    	LTPLOG(0) << " caught: " << e.what() << endl;
  	}
  } // all psm files
  
  // Once the dummy release is working, uncomment.
  // Create a state machine parsed from file.
  try {
    tsm = TrafficSM::getDefaultTrafficSM();
    allmachines[tsm->sam->getName()] = tsm->sam;
  } catch (exception &e) {
    cerr << "LTP[" << __FUNCTION__ << "] caught: " << e.what() << endl;
    throw;
  }
  
  // Go through the state machines and say what they look like.
  // Hm, loading and printing should have their own functions elsewhere.
  LTPLOG(3) << "All the nation engines:" << endl;
  for (map<string, ParsedSM*>::iterator iteroni = allmachines.begin();
      iteroni != allmachines.end(); iteroni++) {
    LLOGD(3) << (*iteroni).first << " -> "
      << (*iteroni).second->getName() << endl;
  }
}

// Destructor
LtPlanner::~LtPlanner()
{
  // Get rid of all created objects and class references
  destroy();  // Call alternate destructor
  return;
}

// Alternative destructor
void LtPlanner::destroy() 
{
  if (TrafficSM::DEFAULT_PSM_FILE) {
    free(TrafficSM::DEFAULT_PSM_FILE);
  }
  // Deallocate all claimed objects...
  delete tsm;
}

/**
 * @brief   Modular Logic PLanner config file reader
 *      Reads config file and store data in internal variables.
 * @return  (int)0 if good
 */
void LtPlanner::readConfig() 
{
	LTPLOG(5) << endl;
	// (Preferably to be abstracted into a struct)
  // Read configuration from file into workspace.
  char *path;
  // include this function from dgcutils/cfgfile.h
  path = dgcFindConfigFile("LtPlanner.conf","lt-planner");
  FILE* configFile = fopen(path,"r");
  if (configFile == NULL) {
    Console::addMessage("LTP: Warning: Couldn't read LtPlanner.conf.");
    // FIXME: choose & use appropriate logging function.
    LLOGL(1) << "Warning: Couldn't read LtPlanner.conf."<<endl;
    //qprint("Warning: Couldn't read LtPlanner.conf.\n");
  }
  else {
    // include this class from temp-planner-interfaces/ConfigFile.hh
    ConfigFile config(path);
    // Example:
    config.readInto(VAN(LIFE));
    config.readInto(VAN(SM_MODE));
    config.readInto(VAN(DEFAULT_PSM_FILE));
    config.readInto(ParseUtils::VERBOSITY, "VERBOSITY");
    config.readInto(VAN(TIMERLEN));
    config.readInto(VAN(POSTIMERLEN));
    config.readInto(VAN(ALL_PSM_FILES));
    
    LLOGL(1) << "Read LtPlanner configuration successfully." << endl;
    fclose(configFile);

    // Log all the parameters
    LLOGD(5) << NAV(LIFE) << endl;
    LLOGD(5) << NAV(SM_MODE);
    switch (SM_MODE) {
      case 1: LLOG(5) << " (Parsed SM)" << endl; break;
      case 0:
      default: LLOG(5) << " (Quantum SM)" << endl;
    }
    LLOGD(5) << NAV(DEFAULT_PSM_FILE) << endl;
    LLOGD(5) << NAV(ParseUtils::VERBOSITY) << endl;
    LLOGD(5) << NAV(TIMERLEN) << endl;
    LLOGD(5) << NAV(POSTIMERLEN) << endl;
    LLOGD(5) << NAV(ALL_PSM_FILES) << endl;
      
  }
  //*/

}

// Planner might ask for Console::queryResetStateFlag(), 
// which I should at least pretend to acknowledge.
void LtPlanner::resetState() {
	// This stuff is good in fast food, bad 4 u.
	char msg[] = "I'm supposed to reset the state, but I won't.";
	LTPLOG(4) << msg << endl;
	Console::addMessage(msg);
	char msg2[] = "Better idea: replanFromCurrPos!";
  LLOGD(4) << msg2 << endl;
  Console::addMessage(msg2);
	replanFromConsole = true;
	#ifdef FAIL_ON_RESET
	LLOGD(4) << "This is an intentional assert(0). "
	 "Please recompile without -DFAIL_ON_RESET, thank you." << endl;
	fail_on_reset(7, 42); // This is only a test. Please don't define FAIL_ON_RESET!
	#endif
}

//! Just a comparator for use in making maps from int to string et al.
struct intCmp {
  bool operator()(int a, int b) {
    return a > b;
  }
};

// Planner will ask every time.
void LtPlanner::updateDisplayInfo() {
    LTPLOG(9) << endl;
    // Dummy messages
    string msgst = stateString[tsm->current_status.state];
    Console::updateFSMState(msgst);
    LLOGD(2) << "Current State             = " << msgst << endl;

    string msgfl = flagString[tsm->current_status.flag];
    Console::updateFSMFlag(msgfl);
    LLOGD(2) << "Current Flag              = " << msgfl << endl;
    
    string msgre = regionString[tsm->current_status.region];
    Console::updateFSMRegion(msgre);
    LLOGD(2) << "Current Region            = " << msgre << endl;
    
    string msgpl = plannerString[tsm->current_status.planner];
    Console::updateFSMPlanner(msgpl);
    LLOGD(2) << "Current Planner           = " << msgpl << endl;
    
    string msgob = obstacleString[tsm->current_status.obstacle];
    Console::updateFSMObstacle(msgob);
    LLOGD(2) << "Current Obstacle Level    = " << msgob << endl;

    double msgpr = current_status.probability;
    LLOGD(2) << "Current Probability Level = " << msgpr << endl;

    LLOGD(2) << "planFromCurrPos           = "
      << tsm->to.planFromCurrPos << endl;
    LLOGD(2) << "replanInZone              = "
      << tsm->to.replanInZone << endl;

    // Send the planner status to the map viewer
    Utils::displayPlannerStatus(msgre, msgst, msgpl, msgfl);
    Utils::displayPlannerState(tsm->getStateString());
}

void LtPlanner::updateIntersectionConsole() {
  // A blank string here is supposed to indicate
  // that we aren't in an intersection.
  Utils::displayIntersectionState(tsm->intersectionStringForConsole);

  // Note:
  // map/MapPrior.hh/PointLabel
  // temp-planner-interfaces/PlannerInterfaces.h/precedenceList_t
  // map/MapElement.hh/MapElement

  // Console::updateInter(
  //   int CountPrecedence, // How many elements in the precedenceList
  //                        // actually have precedence?
  //   string Status,       // eg, Waiting for car on right, jammed, ....
  //   map/MapPrior.hh/PointLabel wp, // eg, PointLabel(
  //                                           currSegment.entrySegmentID,
  //                                           currSegment.entryLaneID,
  //                                           currSegment.entryWaypointID);
  //   double interTimer); // Time in seconds since some event.
  // Console::updateInterPage2(
  //   // Vehicles with precedence.
  //   vector<precedenceList_t> obstaclesPrecedence,
  //   // Vehicles that block intersection.
  //   vector<MapElement> obstaclesClearance,
  //   // Vehicles that are merging, or that we merge into, maybe.
  //   vector<MapElement> obstaclesMerging);
}

/**
 * @brief   Called in every cycle before state machine to condense information.
 *
 * Information is stored in hard-to-get places. We use something like
 * temp-planner-interfaces/Utils to aggregate the information.
 * Current implementation due to Stefan; see ModLogPlanner.cc.
 */
void LtPlanner::updateTrafficData(TrafficData *td) {
  LTPLOG(9) << endl; // just a greeting
  // How fast are we going? For use in progress checking and see if stopped.
  td->currentVelocity = AliceStateHelper::getVelocityMag(*(td->vehState));

  // As you can see, this does not filter stopline distance.
  td->stoplineDistance = Utils::getDistToStopline(td->params->m_path);

  // Get a couple of LaneLabels.
  td->currentLane = Utils::getCurrentLaneAnyDir(*(td->vehState), td->graph);
  td->desiredLane = Utils::getDesiredLane(*(td->vehState), td->graph);

  // Objects in mirror may appear closer than they are.
  td->obstacleDistance =
    Utils::getNearestObsDist(*(td->vehState), td->map, td->currentLane);

  td->obstacleOnPath = 
    Utils::getNearestObsOnPath(td->params->m_path, td->graph, *(td->vehState));

  td->obstacleInLane =
    Utils::getNearestObsInLane(*(td->vehState), td->map, td->currentLane);

  // Where is the nearest exit on this segment?
  td->exitDistance =
    Utils::getDistToExit(*(td->vehState), td->map, td->params->seg_goal);
  
  // How soon should we stop before a stop sign? Blatantly copied but not used.
  td->stoppingDistance =
    20;
  //(td->currentVelocity * td->currentVelocity) / (2.0 * td->DESIRED_DECELERATION);

  // "Clean up two of them", but why?
  if (td->obstacleDistance == -1) {td->obstacleDistance = INFINITY;}
  if (td->obstacleInLane == -1) {td->obstacleInLane = INFINITY;}

  // "Prevenir pointeros malos from messing up este coleccion datal."
  // Find out how close obstacles are in various directions.
  LLOG(10) << "  " << NAV(td->params->m_path)    << endl
    << "  " << NAV(td->params->m_path->pathLen)  << endl
    << "  " << NAV(td->params->m_path->nodes)    << endl
    << "  " << NAV(td->params->m_path->nodes[0]) << endl;
  
  if (td->params->m_path == NULL
     || td->params->m_path->pathLen < 1
     || td->params->m_path->nodes[0] == NULL) {
    LTPLOG(9) << "PlanGraphPath not established." << endl;
    td->obsSide = INFINITY;
    td->obsRear = INFINITY;
    td->obsFront = INFINITY;
  } else {
    LTPLOG(9) << "PlanGraphPath established." << endl;
    td->obsSide = td->params->m_path->nodes[0]->status.obsSideDist;
    td->obsRear = td->params->m_path->nodes[0]->status.obsRearDist;
    td->obsFront = td->params->m_path->nodes[0]->status.obsFrontDist;
  }
  
  // From user input (temp-planner-interfaces)
  td->freeze = Console::queryToggleFailureHandling();

  // From configuration file. (Probably not the best way to handle)
  td->TIMERLEN = this->TIMERLEN;
  td->POSTIMERLEN = this->POSTIMERLEN;
} // updateTrafficData

/**
 * @brief One for the record and one for the road.
 *
 * Huh?
 */
void LtPlanner::logTrafficData(TrafficData *td) {
  LTPLOG(7) "TrafficData look like:" << endl
    << "  currentVelocity = " << td->currentVelocity << " [m/s]" << endl
    << "  stoplineDistance = " << td->stoplineDistance << " [m]" << endl
    << "  currentLane      = " << td->currentLane << endl
    << "  desiredLane      = " << td->desiredLane << endl
    << "  obstacleDistance = " << td->obstacleDistance << " [m]" << endl
    << "  obstacleOnPath   = " << td->obstacleOnPath << " [m]" << endl
    << "  obstacleInLane   = " << td->obstacleInLane << " [m]" << endl
    << "  exitDistance     = " << td->exitDistance << " [m]" << endl
    << "  stoppingDistance = " << td->stoppingDistance << " [m]" << endl
    << "  obsSide          = " << td->obsSide << " [m]" << endl
    << "  obsRear          = " << td->obsRear << " [m]" << endl
    << "  obsFront         = " << td->obsFront << " [m]" << endl
    << "  freeze           = " << td->freeze << " in [0,1]." << endl;

} // logTrafficData

/**
 * @brief   Called by planner to handle traffic.
 * 
 * @return  An Err_t object (temp-planner-interfaces/PlannerInterfaces.h)
 *       Also, the main result is returned as a StateProblem_t
 *       which is pushed into the problem vector (first argument).
 */
Err_t LtPlanner::planLogic( vector<StateProblem_t*> &problems, PlanGraph *graph,
  Err_t prevErr, VehicleState &vehState, Map *map, Logic_params_t &params,
  int& currentSegmentId, int& currentLaneId, bool& replanInZone ) 
{
  static bool isMissionEnd = false;
  static int cycleWhenDone = -1;
  Err_t result; // TBD

  if (isMissionEnd)
  {
    if (cycle_counter == cycleWhenDone + 1) {
      Console::addMessage("LTP[%ld] Reached the end of our mission.",
        cycle_counter);
      Log::getStream(3) << "LTP[" << cycle_counter
        << "] Reached the end of our mission." << endl;
    } else {
      Log::getStream(3) << "LTP[" << cycle_counter
        << "] Finished the mission in cycle " << cycleWhenDone << "." << endl;
    }

    // Just to appease the "planner".
    StateProblem_t *p = new StateProblem_t();
    p->state = PAUSE;
    p->planner = RAIL_PLANNER;
    p->region = ROAD_REGION;
    p->obstacle = OBSTACLE_SAFETY;
    p->flag = NO_PASS;
    problems.push_back(p);

    result = LP_OK; // To be returned...
  }
  else
  {

  // It is an unwritten assumption that this happens before the rest
  // of your planning cycle. Since it is now written, don't assume.
  Utils::updateStoppedMapElements(map);

  // Wow, this looks just like it does in the mod log planner!
  static TrafficData *tada = new TrafficData;
  tada->problems = &problems;
  tada->graph    = graph;
  tada->prevErr  = prevErr;
  tada->vehState = &vehState;
  tada->map      = map;
  tada->params   = &params;
  tada->currentSegmentId = &currentSegmentId;
  tada->currentLaneId    = &currentLaneId;
  tada->replanInZone     = &replanInZone;

  SegGoals intersectionSegGoals;
  // Found this method used in the old LogicPlanner. Stefan uses the other.
  //static double stopline_distance = Utils::getDistToStopline(params.m_path,
  //  params.seg_goal_queue, intersectionSegGoals);
  double stopline_distance = Utils::getDistToStopline(params.m_path);
  Console::updateStopline(stopline_distance);
  double stopped_duration = Utils::monitorAliceSpeed(vehState, params.m_estop);
  Console::updateStoppedDuration(stopped_duration);

  // Find out in what lane and segment we're really.
  // And where we're going, preferably.
  // First guess from input arguments. Usually wrong.
  int segId = currentSegmentId;
  int laneId = currentLaneId;

  LaneLabel currentLaneLabel = Utils::getCurrentLane(vehState, map);
  string laneName = "blank";
  int junk = map->getLaneName(laneName, currentLaneLabel);
  segId = currentLaneLabel.segment;
  laneId = currentLaneLabel.lane;
  string segName = "blank";
  junk = map->getSegmentName(segName, segId);

  // Get information about current segment.
  char whereAreWe[128];
  sprintf(whereAreWe, "LTP[%ld] Driving on %s. segId = %d, laneId = %d.",
    cycle_counter, segName.c_str(), segId, laneId);
  // Get information about segment goals.
  stringstream whichGoal;
  params.seg_goal.print(whichGoal);
  // Get information about planning errors.
  char whatWentWrong[256];
  sprintf(whatWentWrong, "LTP: Errors from last cycle: %s",
    plannerErrorsToString(prevErr).c_str());

  isMissionEnd |= (params.segment_type == SegGoals::END_OF_MISSION);
  if (isMissionEnd) {
    cycleWhenDone = cycle_counter;
  }

  Log::getStream(4) << whereAreWe << endl; 
  Log::getStream(4) << whichGoal.str() << endl;
  Log::getStream(4) << whatWentWrong << endl;
  // Let console users know I'm still running.
  if (!(cycle_counter % 100)) {
    Console::addMessage(whereAreWe);
    Console::addMessage(whatWentWrong);
  }
  
  if (params.readConfig) {
  	char msg[] = "LTP: I'm not supposed to \"read config\", am I?";
  	// No, I think this was intended for the path planners only.
  	Log::getStream(4) << msg << endl;
  	//Console::addMessage(msg);
  }

  // most of that was clearly unnecessary. Let's condense!
  updateTrafficData(tada);
  logTrafficData(tada);

  // Actually cause the state machine to step and modify problems.
  result = tsm->planLogic(tada);
  
  // Causes weird behavior when you hit the 's' key on console.
  if (replanFromConsole) {
  	params.planFromCurrPos = true;
  	replanFromConsole = false;
  }
  
  } // end if not done with mission

  cycle_counter++;
  return result;
}

// EOF 'LtPlanner.cc'
