/*!
 * \file ParsedMachines.hh
 * \brief Define a few environments and systems for ParsedSMs.
 * \author Nicholas Fette
 * \date July 2008
 * \ingroup ParsedSM
 *
 * see DGC07:Lt-planner
 */

#ifndef _PARSED_MACHINES_HH_
#define _PARSED_MACHINES_HH_
#include "ParsedSM.hh" // Defines ParsedSM, SMState, Environment, and System.

using namespace std;

/*!
 * \brief Example Environment and System for a timer.
 *
 * Example of an Environment and System (need not be in the same class)
 * for running a counter or timer and displaying the result.
 */
class timerGame : public Environment, public System {
  protected:
    int timevar;
    vector<char> lastVals;
    vector<string> lastNames;
  public:
    timerGame() : Environment(), System() {
      timevar = 0;
    }
    ~timerGame() {}
    // for Environment
    const vector<char>& getInputs(const vector<string>& varnames);
    // for System
    void execute(const vector<char>& outputVals,
      const vector<string>& outputNames);
    ostream & displayLastCall(ostream &os) const;
};

/*!
 * \brief Example Environment for manual input.
 *
 * Example implementation of Environment which allows a user to manually
 * enter the inputs, usually for testing purposes.
 */
class manualStepper: public Environment {
  private:
    istream *is;
  public:
    manualStepper(istream *input) : Environment(), is(input) { }
    ~manualStepper() { }

    // Wait for a user to enter inputs through the input stream.
    // Each character is treated as a bit.
    // '0' and ' ' are treated as zero.
    // Anything else is treated as one.
    const vector<char>& getInputs() {
      envInputVals.clear();
      string s;
      char bit;
      if (!getline (*is, s)) {
        throw SMException("File is over");
      }
      for (int i = 0; i < (int)s.length(); i++) {
        bit = !(s[i] == '0' || s[i] == ' ');
        envInputVals.push_back(bit);
      }
      // User might want to know that these are correct. Or not, hey.
      //cout << "Your input was " << envInputVals << endl;
      return envInputVals;
    }
    const vector<char>& getInputs(const vector<string>& varnames) {
      // Maybe I should prompt first? Not now, anyway.
      return getInputs();
    }
};

#endif // _PARSED_MACHINES_HH_
// EOF ParsedMachines.hh
