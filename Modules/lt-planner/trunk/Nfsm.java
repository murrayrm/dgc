import java.io.IOException;

import net.sf.javabdd.BDD;
import edu.wis.jtlv.env.Env;
import edu.wis.jtlv.env.module.Module;
import edu.wis.jtlv.env.module.ModuleWithWeakFairness;
import edu.wis.jtlv.lib.games.GameException;


/**
 * Make and run an automaton from a SMV file.
 * 
 * @author nfette
 *
 */
public class Nfsm {

	/** The states and transitions of the represented automaton. */
	private RawState[] srs;
	/** The state in which we now are. */
	private int state = -1;
	/** The allowable next states. */
	private int[] successorIds = null;
	/** {@link Nfsm#debug(String)} prints iff <b>isDebug</b> is true. **/
	public static boolean isDebug;
	/** The (PrintStream) printer to use for debugging.
	 * Set to {@link System#out} by default. */
	public static java.io.PrintStream printer = System.out;

	/* Start here. */
	public static void main(String[] args) {

		String wd = "./src/testcases/";
		//		String[] files = (new java.io.File(wd)).list();
		//		if (files != null) {
		//			System.out.println("Dir \"" + wd + "\": found " + files.length + " files.");
		//			for (int i = 0; i < files.length; i++) {
		//				System.out.println(files[i]);
		//			}
		//			System.out.println();
		//		} else {
		//			System.out.println("Dir \"" + wd + "\": found no files!");
		//		}

		// 1. Load SMV from file
		System.out.print("Loading file: ");
		String input_file = wd + "arbiter2_with_spec.smv";
		if (args.length > 0) {
			input_file = args[0];
		}
		System.out.println(input_file);

		System.out.println("About to compute automaton.");
		RawState[] srs;
		try {
			GRGame2 g = computeAutomaton(input_file);
			srs = g.getStrategyArray();
			if (srs == null) {
				System.out.println("Warning: RawState array was null/empty!");
			}
			
			// Now build a structure that is useful.
			System.out.println("Converting from raw states to finite state machine.");
			Nfsm msfn = new Nfsm(srs);
			int[] freq = new int[srs.length];
			
			// Now run the thing.
			System.out.println("Beginning to run FSM from state "
					+ msfn.getState() + ".");
			System.out.println("Initial frequency " +
					java.util.Arrays.toString(freq) + ".");
			// Initialize environment.
			//boolean r1 = false,
			//		r2 = false;
			for (int i = 0; i < 1000; i++) {
				freq[msfn.getState()] ++;
				int[] success = msfn.getAllowableSteps();
				// Scan for possible inputs and pick one.
				int[][] inputs = new int[success.length][2];
				int[] hashes = new int[success.length];
				for (int n = 0; n < success.length; n++) {
					// example has just two boolean inputs. TODO: generalize.
					inputs[n][0] = (msfn.srs[success[n]]).getVarValues()[0];
					inputs[n][1] = (msfn.srs[success[n]]).getVarValues()[2];
					hashes[n] = inputs[n][0] + 2 * inputs[n][1];
				}
				// Compare hashed inputs for uniqueness.
				boolean areInputsUnique = true;
				for (int m = 0; m < success.length; m++) {
					for (int n = m + 1; n < success.length; n++) {
						areInputsUnique &= (hashes[m] != hashes[n]);
					}					
				}
				Nfsm.debug("From state " + msfn.getState() + ", inputs are "
						+ java.util.Arrays.toString(hashes) + " which are "
						+ ((areInputsUnique)?"":"not ") + "unique.");
				if (!areInputsUnique) {
					System.err.println("Possible inputs not unique!");
				}
//				System.out.println("In state " + msfn.getState());
//				System.out.println("Valid successors: " +
//						java.util.Arrays.toString(msfn.getAllowableSteps()));
				// Display some information about the current state.
				//System.out.println(msfn.srs[msfn.state]);
				// Display some information about the possible transitions.
				//for (int q = 0; q < success.length; q++) {
				//	System.out.println(msfn.srs[success[q]]);
				//}
				//System.out.println("-- End of successors for state " + msfn.state + "--");
				//System.out.println(
					//	msfn.srs[msfn.state].get_state().support().getDomains().toString());
				int m = (int)(Math.random() * success.length);
				msfn.step(success[m]);
			}
			System.out.println("Final frequency "
					+ java.util.Arrays.toString(freq) + ".");
		} catch (GameException e) {
			// error in the game...
			e.printStackTrace();
		} catch (IOException e) {
			// error with reading the file...
			e.printStackTrace();
		}

	} // end main

	public int getState() {
		return this.state;
	}

	public static GRGame2 computeAutomaton(String input_file)
	throws IOException, GameException {
		GRGame2 g;
		Env.loadModule(input_file);
		ModuleWithWeakFairness cx = (ModuleWithWeakFairness) Env
		.getModule("main");
		ModuleWithWeakFairness player1 = (ModuleWithWeakFairness) Env
		.getModule("main.e");
		ModuleWithWeakFairness player2 = (ModuleWithWeakFairness) Env
		.getModule("main.s");

		// 2. Determine if FSM exists to satisfy SMV spec.
		System.out.println("Constructing and playing the game.");
		g = new GRGame2(player1, player2);
		System.out.println("Player sys winning states are:");
		System.out.println(Env.toNiceString(cx, g.sysWinningStates()));
		System.out.println("Player env winning states are:");
		System.out.println(Env.toNiceString(cx, g.envWinningStates()));

		BDD all_init = g.getSysPlayer().initial().and(
				g.getEnvPlayer().initial());
		BDD counter_exmple = g.envWinningStates().and(all_init);
		if (!counter_exmple.isZero()) {
			System.out.println("Specification is unrealizable.");
			System.out.println("The env player can win from states:");
			System.out.print("\t" + Env.toNiceString(cx, counter_exmple));
		} else {
			System.out.println("Specification is realizable.");
			System.out.println("Building an implementation.");

			g.calculate_strategy();
			//return g.getStrategyArray();
		}
		System.out.println("Done with this part."); // hush
		//return g.getStrategyArray().clone();
		return g;
	}

	public Nfsm (RawState[] srs) {
		this.srs = srs;
		// initialize to some state. You guess....
		this.state = 0;
	}

	/** Returns whether the step was allowed, and if so executes it. */
	public boolean step(int next) {
		boolean validStep = false;
		for (int i = 0; i < successorIds.length; i++) {
			if (next == successorIds[i]) {
				validStep = true;
				this.state = next;
				break;
			}
		}
		return validStep;
	}
	
	public int[] getAllowableSteps() {
		RawState[] nextraws = this.srs[this.state].get_succ_raw().clone();
		successorIds = new int[nextraws.length];
		for (int i = 0; i < nextraws.length; i++) {
			successorIds[i] = nextraws[i].get_id();
		}
		return successorIds;
	}
	
	/** Prints <b>str</b> plus a newline to printer (System.out, by default)
	 * if and only if {@link Nfsm#isDebug} is true. */
	public static void debug(String str) {
		if (Nfsm.isDebug) {
			Nfsm.printer.println(str);
		}
	}
}
