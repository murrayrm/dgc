Description of "lt-planner" module:
Another new version of logic planner using temporal logic.
Or at least, that's the goal. My intention is to support generic state
machines, which could be produced automatically from LTL specifications
using TLV or the newer JTLV. My preference for implementation is to use
text files to describe the state machine, and my code only needs to parse
the text file and execute the machine. My goal is to have a robust state
machine to handle all traffic scenarios and guarantee forward progress,
and my objectives are to keep the code easily understandable and adaptable.
(It would be nice to eventually let ALICE generate her own logic,
and this is also one possible step in that direction.)

As of July 2008, the code is organized thus:
This code builds a library lt-planner, which is linked into planner (or
planner-viewer). Planner handles lower level navigation-related reasoning,
taking mission directives and outputting a trajectory (a path with velocity).
The highest level of this particular stack is traffic reasoning.

The LtPlanner class is mostly an interface through which Planner can access
my implementation of traffic reasoning with similar signatures to other
previous traffic logic modules (logic-planner and mod-log-planner). It
instantiates a ParsedSM object, which builds the state machine from a file
such as 'default.psm', then matches it with the appropriate TrafficSM, which
provides an implementation for evaluating the input variables needed to choose
state transitions.

The "LT" can stand for "linear temporal" because there is nothing really unique
about a state machine parsed from file (other than it's being useful). If I
can write a feasible linear temporal logic specification for my state machine,
then I can use JTLV to synthesize the actual set of states and transitions that
satisfy the specification, and entirely avoid this messy business of post facto
verification that the system does what I want. But, should I want to understand
my state machine or adjust it slightly, the "parseable state machine" file
format allows it without the need for the excesses of a full programming
language such as C++.

For a more thorough description, see Lt-planner on the wiki.


Other modules that "lt-planner" depends on:
planner

