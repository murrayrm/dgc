-- See http://gc.caltech.edu/wiki07/index.php?title=Lt-planner for usage.
-- See varlist.txt for usabe variables.
-- Run to build something like this:
-- ./synth trafficstop.05.smv main main.e main.s 5 5 trafficstop.05.psm trafficstop.05.dot
-- java -jar Xmx1000m ltsynth.jar trafficstop.05.smv main main.e main.s 6 6 trafficstop.05.psm trafficstop.05.dot
-- This is an LTL specification for a primitive traffic state machine.
-- It should incorporate the following.
-- * Stopping for stop lines:
--   RAIL_PLANNER, ROAD_REGION, STOP_INT, OBSTACLE_*
-- * Parking in a zone:
--   S1PLANNER, ZONE_REGION, OBSTACLE_BARE | AGGRESSIVE | SAFETY, PASS_REV
--   maybe not replanInZone and not planFromCurrPos.
-- * Backing up and making uturns if requested by segment goals:
--   S1_PLANNER, ROAD_REGION, DRIVE | UTURN | BACKUP.
-- * Driving offroad if no path is found for a little while.
--   DRIVE, OBSTACLE_BARE, OFFROAD,
--   RAIL_PLANNER at first, then switch to S1 if still lost.
-- Note: this specification attempts to use enum types.
-- However, the synthesis algorithm tends to output arbitrary values
-- instead of the symbolic names, so for instance:
-- spec says: setVelMode = STOP_INT // STOP_INT is arbitrary
-- code says: state = STOP_INT // STOP_INT defined in temp-planner-interfaces
-- Do not do: state = setVelMode; // "Arbitrary" is not defined in t-p-i.faces.

-- The model contains an environment e (sensors) and a system s (actuators).
MODULE main 
	VAR
		e : env();
		s : sys(
			e.isJustBeforeStopSign
			,e.isNowStopped
			,e.whichSegment
			,e.isErrorNoPath
--			,e.isObsCozy
			,e.isObsTooClose
			,e.isErrorCollision
			);

-- The environment knows whether the car is approaching a stop sign
-- and whether it (the car) is stopped.
MODULE env()
	VAR
		-- My convention for naming input variables is this:
		-- isName indicates Name takes on binary values,
		-- whichName indicates Name takes on enum values.
		-- About 5 bits, since whichSegment takes only 4 values usually.
		isJustBeforeStopSign : boolean; 	-- isJustBeforeStopSign
		isNowStopped : boolean; 	-- isStopped
		-- Don't use StartChute until I change printing format a little.
		whichSegment : {Road, Zone, Intersection, PreZone, UTurn,
		  DriveAround, BackUp, EndOfMission, EmergencyStop, Unknown, StartChute};
		-- Doesn't matter for anything that I do until I do precedence.
		--whichIntersection : {Straight, Left, Right};
		-- Can't ask which error because it might not be distinct, unless
		-- SMV can do "bitwise and" and let me declare constants as such.
		--isErrorNone : boolean;  -- don't know
		isErrorNoPath : boolean;  -- don't know
		-- Is an obstacle approaching too close but not yet?
		-- More precisely, is an obstacle in front at distance 16 to 20 meters?
--		isObsCozy : boolean;
		-- Is an obstacle in front at distance 16 or fewer meters?
		isObsTooClose : boolean;
		-- Does the path collide with an alleged obstacle?
		isErrorCollision : boolean;
		-- Input functions specific to TrafficSM (eventually).
		--isTimerUp;
		--isSpacialTimerUp;
		

	ASSIGN
		init(isJustBeforeStopSign) := FALSE;
		init(isNowStopped) := TRUE;
		init(whichSegment) := Unknown;
		--init(whichIntersection) := Straight;
		--init(isErrorNone) := TRUE;
		init(isErrorNoPath) := FALSE;
--		init(isObsCozy) := FALSE;
		init(isObsTooClose) := FALSE;
		init(isErrorCollision) := FALSE;
	
	-- You can make separate TRANS statements; use no ";".
	-- Try: make no assumptions for now and see if automaton blows up.
	TRANS
		-- I can't say much about this environment, really.
		--isNowStopped -> (isJustBeforeStopSign = next(isJustBeforeStopSign));
		--(isErrorNone = !(isErrorNoPath)); -- OR other errors
		((next(whichSegment) = Road)
		|(next(whichSegment) = Zone)
		|(next(whichSegment) = Intersection)
		|(next(whichSegment) = UTurn)
		|(next(whichSegment) = BackUp)
		)&(next(whichSegment) != Unknown)

	JUSTICE
		-- Can't say much here, either.
		--isJustBeforeStopSign;
		--!isJustBeforeStopSign;
		--isNowStopped;
		--!isNowStopped;
		isJustBeforeStopSign & isNowStopped;
		!isJustBeforeStopSign & !isNowStopped;
		isJustBeforeStopSign & !isNowStopped;
		!isJustBeforeStopSign & isNowStopped;
		--whichSegment = Road;
		--whichSegment = Zone;
		--whichSegment = Intersection;
		--whichSegment = PreZone;
		--whichSegment = UTurn;
		--whichSegment = DriveAround;
		--whichSegment = BackUp;
		--whichSegment = EndOfMission;
		--whichSegment = EmergencyStop;
		--whichSegment = Unknown;
		--whichSegment = StartChute;
		--whichIntersection = Straight;
		--whichIntersection = Left;
		--whichIntersection = Right;
		--isErrorNone;
		--isErrorNoPath;
		
	--This is not implemented by JTLV.
--	SPEC
--		(useMode = StopInt) -> AF isNowStopped;

-- The system tells the car whether to drive or slow to a stop.
-- Also, any "helper" parameters can be added here.
MODULE sys(isJustBeforeStopSign
	, isNowStopped
	, whichSegment
	, isErrorNoPath
--	, isObsCozy
	, isObsTooClose
	, isErrorCollision)
  
	VAR
		-- Note that enum types seem to start at zero and increment by 1.
		-- My convention for output variable names is this:
		-- useName indicates name takes on enum values,
		-- doName indicates name takes on binary values.
		-- state, as in mode. 0 1 2 3 4 5
		useMode : {Pause, Drive, StopInt, StopObs, BackUpMode, UTurnMode};
		-- region, as if this wasn't clear enough. 0 1
		useRegion : {RoadRegion, ZoneRegion};
		-- "Intersection" not implemented by planner; don't use. 0 1 2 3
		-- flag, as in allowed maneuvers. sp?
		useFlag : {NoPass, Pass, PassRev, OffRoad};
		-- trajectory generation, aka path planner 0 1 2 3 4
		useTraj : {Rails, STraj, DTraj, CTraj, RTraj};
		-- obstacle safety margins 0 1 2
		useMargin : {Safety, Aggressive, Bare};
		-- Error return codes. With all false, return LP_OK.
--		doReportLaneBlocked : boolean;
--		doReportRoadBlocked : boolean;
--		doReportMapIncomplete : boolean;
--		doReportLost : boolean;
		-- Miscallaneous functions causing replanning, occasionally good.
		doPlanFromCurrPos : boolean;
--		doReplanInZone : boolean;
		
		-- Outputs local to TrafficSM.
--		doSetTimer : Boolean;
--		doSetSpacialTimer : Boolean;

		-- helper variables: put these last.
--		oldUseMode : {Pause, Drive, StopInt, StopObs, BackUpMode, UTurnMode};
		wasJustBeforeStopSign : boolean; 	-- (s2) wasJustBeforeStopSign 0 1
		hadStopped : boolean; 	-- (s3) hadStopped 0 1
		
	ASSIGN
		init(wasJustBeforeStopSign) := FALSE;
		next(wasJustBeforeStopSign) := isJustBeforeStopSign;
		init(hadStopped) := FALSE;
		next(hadStopped) := !(isJustBeforeStopSign & !wasJustBeforeStopSign) &
		                    (isNowStopped | hadStopped);
		-- state, as in mode.
		init(useMode) := Pause;
		next(useMode) :=
			case -- "Switch" to the expression after the first true condition.
				(next(whichSegment) = Road
				|next(whichSegment) = Intersection) :
					case
						next(!hadStopped)	: StopInt;
						1			: Drive;
					esac;
				next(whichSegment) = Zone : Drive;
				next(whichSegment) = BackUp : BackUpMode;
				next(whichSegment) = UTurn : UTurnMode;
				1 : Pause; -- This is your "else"/"default".
			esac;
--		init(oldUseMode) := Drive;
--		next(oldUseMode) := 
--			case
--				(whichSegment = Road
--				| whichSegment = Intersection
--				| whichSegment = Zone)		: Drive;
--				whichSegment = UTurn		: UTurnMode;
--				whichSegment = BackUp		: BackUpMode;
--				1							: Pause;
--			esac;
		-- region, as if this wasn't clear enough.
		init(useRegion) := RoadRegion;
		-- Please reconsider this; especially, when merging from road to zone.
		next(useRegion) :=
			case
				whichSegment = Zone & next(whichSegment) = Zone : ZoneRegion;
				1 : RoadRegion;
			esac;
		-- flag, as in allowed maneuvers. sp?
		init(useFlag) := NoPass;
		next(useFlag) := 
			case
				(isErrorNoPath
				| next(isErrorNoPath)
				| (useFlag = OffRoad & isNowStopped)) : OffRoad;
				(next(whichSegment) = Zone) : PassRev;
				(isErrorCollision
				| next(isErrorCollision)
				| isObsTooClose
				| next(isObsTooClose)) : Pass;
--				( isObsOuch
--				| next(isObsOuch)
--				) : PassRev;
				1 : NoPass;
			esac;
		-- trajectory generation, aka path planner
		init(useTraj) := Rails;
		next(useTraj) :=
			case
				(next(whichSegment) = Road
				| next(whichSegment) = Intersection)	: Rails;
				1 : STraj;
			esac;
		-- obstacle safety margins
		init(useMargin) := Safety;
		next(useMargin) :=
			case
				next(whichSegment) = Zone : Aggressive;
				1 : Safety;
			esac;
		-- Error return codes
--		init(doReportLaneBlocked) := FALSE;
--		next(doReportLaneBlocked) := FALSE;
--		init(doReportRoadBlocked) := FALSE;
--		next(doReportRoadBlocked) := FALSE;
--		init(doReportMapIncomplete) := FALSE;
--		next(doReportMapIncomplete) := FALSE;
--		init(doReportLost) := FALSE;
--		next(doReportLost) := FALSE;
		-- Miscallaneous functions.
		-- Replan when an obstacle appears or disappears, or the road moves,
		-- or we stand still for too long.
		init(doPlanFromCurrPos) := FALSE;
		next(doPlanFromCurrPos) := 
			( (useFlag != next(useFlag))
			| (useMargin != next(useMargin))
			| (useRegion != next(useRegion))
			| (useTraj != next(useTraj))
			);

--	TRANS
--		(
--		 ((whichSegment = Road) | (whichSegment = Intersection))->
--		 (
--		  useMode =
--		  case
--		   hadStopped : oldUseMode;
--		   !hadStopped : StopInt;
--		  esac
--		 )
--		) &
--		
--		(
--		 (whichSegment = Zone) ->
--		 (oldUseMode = useMode)
--		) &
--		
--		(Pause != next(useMode)) &
--		(StopObs != next(useMode)) &
--		(BackUpMode != next(useMode)) &
--		
--		(
--		 (whichSegment = UTurn) ->
--		 (UTurnMode = next(oldUseMode))
--		 (UTurnMode = useMode)
--		) &
--		
--		(
--		 (whichSegment = BackUp) ->
--		 (BackUpMode = next(oldUseMode))
--		 (BackUpMode = useMode)
--		) &
--		
--		(
--		 (
--		  (whichSegment = Road) |
--		  (whichSegment = Intersection) |
--		  (whichSegment = Zone)
--		 ) ->
--		 (Drive = next(oldUseMode))
--		)
--	TRANS
--		(
--		 (useMode = Drive) ->
--		 (
--		  ((whichSegment = Zone) -> (useTraj = STraj)) &
--		  ((whichSegment != Zone) -> (useTraj = Rails))
--		 )
--		) &
--		
--		((useMode = UTurnMode) -> (useTraj = STraj)) &
--		
--		(
--		 (whichSegment = Zone) ->
--		 (
--		  (next(useRegion) = ZoneRegion) &
--		  (next(useFlag) = PassRev) &
--		  (useMargin = Aggressive)
--		 )
--		) &
--		(
--		 (whichSegment != Zone) -> 
--		 (
--		  (next(useRegion) = RoadRegion) &
--		  (
--		   (
--		    isErrorNoPath |
--		    next(isErrorNoPath) |
--		    ((useFlag = OffRoad) & isNowStopped)
--		   ) ->
--		   (next(useFlag) = OffRoad)
--		  ) &
--		  (useMargin = Safety)
--		 )
--		)
	JUSTICE
	-- Each proposition is always eventually true, or always, or something.
	-- More like, undefined meaning. Not mentioned in the SMV manual.
		--(useMode = Drive);
		--(useMode = StopInt);
		(useMode = StopInt) |
		(useMode = Drive) |
		(useMode = UTurnMode) |
		(useMode = BackUpMode) |
		(useMode = Pause);
		--(useMode = StopInt) & isNowStopped;
		--wasJustBeforeStopSign;
		--!wasJustBeforeStopSign;
		--hadStopped;
		--!hadStopped;
		--wasJustBeforeStopSign & hadStopped;
		--wasJustBeforeStopSign & !hadStopped;
		--!wasJustBeforeStopSign & hadStopped;
		--!wasJustBeforeStopSign & !hadStopped;
