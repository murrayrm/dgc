#ifndef _LTMAIN_HH_
#define _LTMAIN_HH_

#define MY_VERSION __DATE__ " " __TIME__

using namespace std;

/* Static variables for fun. */
static char liblt_version[100] = MY_VERSION;

#endif // _LTMAIN_HH
