/*!
 * \file ParseUtilsInternal.cc
 * \brief Extras for displaying output, mostly.
 * \author Nicholas Fette
 * \date July 2008
 *
 * "Internal" because this attempts to avoid globally redefining operator<<
 * on vectors, mostly due to a conflict with the frames library.
 */

#include "ParseUtils.hh"
#include "ParseUtilsInternal.hh"

using namespace std;

// The frames library also redefines these. Use -DVECTOR_OFF as a compiler op.
#ifndef VECTOR_OFF

//! Print a vector<bool> to the given ostream.
ostream &operator<<(ostream &os, const vector<bool> &arg) {
  return ParseUtils::displayVectorBool(os, arg);
}

//! Print a vector<char> to the given ostream.
ostream &operator<<(ostream &os, const vector<char> &arg) {
  return ParseUtils::displayVectorChar(os, arg);
}

//! Print a vector<int> to the given ostream.
ostream &operator<<(ostream &os, const vector<int> &arg) {
  return ParseUtils::displayVectorInt(os, arg);
}

//! Print a vector<string> to the given ostream.
ostream &operator<<(ostream &os, const vector<string> &arg) {
  return ParseUtils::displayVectorString(os, arg);
}

#endif

// EOF ParseUtilsInternal.cc
