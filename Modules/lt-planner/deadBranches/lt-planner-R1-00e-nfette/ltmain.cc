#include <iostream>
#include <ltmain.hh>

using namespace std;

int main(int argc, char **argv) {
  cout <<
"This should be liblt-planner, version \"" << liblt_version << "\",\n"
"the library for LTL-based state machines applied to traffic \"planning\",\n"
"ie for use with TeamCaltech's planner. By Nicholas Fette.\n";
  return 0;
}
