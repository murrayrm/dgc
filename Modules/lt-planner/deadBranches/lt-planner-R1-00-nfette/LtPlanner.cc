/*!
 * \file LtPlanner.cc
 * \brief Definitions LtPlanner
 *
 * \author Nicholas Fette (based on ModLogPlanner by Stefan Skoog)
 * \date July 2008
 *
 * \ingroup lt-planner
 *
 */



/*********************************
 * Include header
 *********************************/
#include "LtPlanner.hh"


/*********************************
* Initiate (static) class variables
**********************************/


/*********************************
 * Methods
 *********************************/

LtPlanner::LtPlanner() 
{
  // In case we need a new constructor, call alternative init function.
  init();
}

/**
 * @brief   Lt PLanner initializer
 *      Reads config file, resets internal variables,
 *      creates LT State Machine if necessary.
 */
void LtPlanner::init() 
{
  
  // Read config file and save into class variables
  readConfig();
  
  // Cycle counter, begin at zero
  cycle_counter = 0;

  // Init current status, complete state representation
  current_status.state       = DRIVE;
  current_status.probability = 0.99;
  current_status.flag        = PASS;
  current_status.region      = ROAD_REGION;
  current_status.planner     = RAIL_PLANNER;
  current_status.obstacle    = OBSTACLE_SAFETY;
  
  // Once the dummy release is working, uncomment.
  // Create a state machine parsed from file.
  // tsm = TrafficSM::getDefaultTrafficSM();
}

// Destructor
LtPlanner::~LtPlanner()
{
  // Get rid of all created objects and class references
  destroy();  // Call alternate destructor
  return;
}

// Alternative destructor
void LtPlanner::destroy() 
{
  // Deallocate all claimed objects...
  //delete tsm;
}

/**
 * @brief   Modular Logic PLanner config file reader
 *      Reads config file and store data in internal variables.
 * @return  (int)0 if good
 */
void LtPlanner::readConfig() 
{
  // Once the dummy release is working, uncomment.
  /*
  // Read from file into workspace
  char *path;
  // include this function from dgcutils/cfgfile.h
  path=dgcFindConfigFile("LtPlanner.conf","lt-planner");
  FILE* configFile = fopen(path,"r");
  if (configFile == NULL) {
    Console::addMessage("Warning: Couldn't read LtPlanner.conf.");
    // FIXME: choose & use appropriate logging function.
    Log::getStream(1)<<"Warning: Couldn't read LtPlanner.conf."<<endl;
    //qprint("Warning: Couldn't read LtPlanner.conf.\n");
  }
  else {
    // include this class from temp-planner-interfaces/ConfigFile.hh
    ConfigFile config(path);
    // Example:
    config.readInto(LIFE, "LIFE");
    config.readInto(SM_MODE, "SM_MODE");
    
    Log::getStream(1)<<"Info: Read LtPlanner configuration successfully."<<endl;
    fclose(configFile);

    // Log all the parameters
    Log::getStream(5) << "LIFE: " << LIFE << endl;
    Log::getStream(5) << "SM_MODE: " << SM_MODE;
    switch (SM_MODE) {
      case 1: Log::getStream(5) << " (Parsed SM)" << endl; break;
      case 0:
      default: Log::getStream(5) << " (Quantum SM)" << endl;
    }
  }
  */

}

// Planner might ask for Console::queryResetStateFlag(), 
// which I should at least pretend to acknowledge.
void LtPlanner::resetState() {
}

// Planner will ask every time.
void LtPlanner::updateDisplayInfo() {
}

/**
 * @brief   Called by planner to handle traffic.
 * 
 * @return  An Err_t object (temp-planner-interfaces/PlannerInterfaces.h)
 *       Also, the main result is returned as a StateProblem_t
 *       which is pushed into the problem vector (first argument).
 */
Err_t LtPlanner::planLogic( vector<StateProblem_t> &problems, PlanGraph *graph,
  Err_t prevErr, VehicleState &vehState, Map *map, Logic_params_t &params,
  int& currentSegmentId, int& currentLaneId, bool& replanInZone ) 
{
  // Once the dummy release is working, uncomment.
  
//  // Working with the parsed state machine
//  return tsm->planLogic(problems, graph, prevErr, 
//                         vehState, map, 
//                         params, currentSegmentId, currentLaneId,
//                         replanInZone); 

  // new() makes it non-local, not on the stack, that's probably what we want.
  StateProblem_t* p = new StateProblem_t;
  p->state       = current_status.state;
  p->region      = current_status.region;
  p->obstacle    = current_status.obstacle;
  p->planner     = current_status.planner;
  p->flag        = current_status.flag;
  p->probability = current_status.probability;

  // Push new problem into queue
  problems.push_back(*p);
  
  // Change return data when code is starting to work
  return LP_OK;
}

// EOF 'LtPlanner.cc'
