// Run a test on the generic ParsedSM file reading operations.
// by Nicholas Fette
// see DGC07:User:Nfette/Summer 2008

#include <iostream> // for cout

#include "ParseUtils.hh"      // Include first for printing vectors.
#include "ParsedSM.hh"        // Include next, define interfaces and parser.
#include "ParsedMachines.hh"  // Include last, define implementations.

using namespace std;

int main(int argc, char **argv) {
  char *prog = "UT_ParsedSM";
  prog = (argc)?argv[0]:prog;
  if (argc < 2) {
    cout << "Usage: " << argv[0]
      << " --example | --manual | file1.psm [file2.psm ...]" << endl;
    cout << "The options are not treated as flags, but rather let you" << endl
      << "run timer.psm with the example or manual environment." << endl;
    cout << "Otherwise, the state machine is only loaded and printed." << endl;
    return 1;
  }
  for (int i = 1; i < argc; i++) {
    try {
      char *file = argv[i];
      bool example = !strcmp(file, "--example");
      bool manual = !strcmp(file, "--manual");
      if (example || manual) { file = "timer.psm"; }
      cout << prog << ": Loading state machine in " << file << "." << endl;
      ParsedSM parsha(file);
      // Don't read it until I say so.
      parsha.init();
      cout << endl;
      cout << prog << ": Got this from " << file << ":\n" << parsha << endl;
      if (example) { 
      // Run the timer example.
        cout << prog << ": Now going to run the timer example." << endl;
        timerGame *tiger = new timerGame();
        parsha.setSystem(tiger);
        parsha.setEnvironment(tiger);
        for (int i = 0; i < 10; i++) {
          parsha.step();
          cout << i << ": Output is ";
          tiger->displayLastCall(cout);
        }
        delete tiger; // growl!
      } else if (manual) {
        // Run the timer example, with manual inputs for you.
        cout << prog << ": Going to run the timer. Enter like \"01\"." << endl;
        timerGame *tiger = new timerGame();
        // Try reading commands in from file?
        //ifstream commander("commander");
        //manualStepper *step = new manualStepper(&commander);
        manualStepper *step = new manualStepper(&cin);
        parsha.setSystem(tiger);
        parsha.setEnvironment(step);
        for (int i = 0; i < 100; i++) {
          cout << "Tic toc ... what o'clock? ";
          parsha.step();
          cout << i << ": Output is ";
          tiger->displayLastCall(cout);
        }
        delete tiger; // growl!
        delete step;  // eep!
     }
    } catch (SMException &e) {
      cout << "Caught an SMException:" << endl
        << "  what(): " <<  e.what() << endl;
    }
    cout << endl;
  }
  
  return 0;

}

// EOF UT_ParsedSM.cc
