/*!
 * \file ParsedMachines.cc
 * \brief Define a few environments and systems for ParsedSMs.
 * \author Nicholas Fette.
 * \date July 2008
 * \ingroup ParsedSM
 *
 * see DGC07:Lt-planner
 */

#include "ParsedMachines.hh"
#include "ParseUtils.hh"         // For ParseUtils static object.
#include "ParseUtilsInternal.hh" // for operator<< using ParseUtils.

//! Counts up like a clock with every call.
const vector<char>& timerGame::getInputs(const vector<string>& varnames) {
  // cout << "Tiger: giving away inputs at time " << time << endl;
  envInputNames = varnames;
  envInputVals.clear();
  for (vector<string>::iterator iter = envInputNames.begin();
    iter != envInputNames.end(); iter++) {
    if (*iter == "a") {
      // high bit
      envInputVals.push_back((timevar / 2) % 2);
    } else if (*iter == "b") {
      // low bit
      envInputVals.push_back(timevar % 2);
    } else {
      // fake bit (not supposed to happen)
      envInputVals.push_back(false);
    }
  }
  timevar ++;
  return envInputVals;
}

void timerGame::execute(const vector<char>& outputVals,
  const vector<string>& outputNames)
{
  lastVals = outputVals;
  lastNames = outputNames;
}

ostream & timerGame::displayLastCall(ostream &os) const {
  ParseUtils::setVectorDelims("<",">","");
#ifndef VECTOR_OFF
  os << lastNames << " = " << lastVals << endl;
#else
  ParseUtils::displayVectorString(os, lastNames);
  os << " = ";
  ParseUtils::displayVectorChar(os, lastVals);
  os << endl;
#endif
  return os;
}

// EOF ParsedMachines.cc
