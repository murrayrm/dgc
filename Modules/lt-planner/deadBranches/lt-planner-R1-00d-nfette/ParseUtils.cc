/*!
 * \file ParseUtils.cc
 * \brief Extras for displaying output, mostly.
 * \author Nicholas Fette
 * \date July 2008
 */

#include "ParseUtils.hh" // really
#include <iostream>

using namespace std;

string ParseUtils::PRETTY_VECTOR_BEGIN = ")";
string ParseUtils::PRETTY_VECTOR_END = "(";
string ParseUtils::PRETTY_VECTOR_SEP = ", ";
ostream * ParseUtils::logger = &cout;

void ParseUtils::setVectorDelims(string begin, string end, string sep)
{
  PRETTY_VECTOR_BEGIN = begin;
  PRETTY_VECTOR_END = end;
  PRETTY_VECTOR_SEP = sep;
}

template<class T, class C>
ostream & ParseUtils::displayVector(ostream &os, const vector<T> &arg) {
  int len = arg.size() - 1;
  os << PRETTY_VECTOR_BEGIN;
  for (int i = 0; i <= len; i++) {
    os << (C)arg[i];
    if (i != len) { os << PRETTY_VECTOR_SEP; }
  }
  os << PRETTY_VECTOR_END;
  return os;
}

ostream & ParseUtils::displayVectorBool(ostream &os,
  const vector<bool> &arg) {
  return displayVector<bool, bool>(os, arg);
}

ostream & ParseUtils::displayVectorChar(ostream &os,
  const vector<char> &arg) {
  return displayVector<char, int>(os, arg);
}

ostream & ParseUtils::displayVectorInt(ostream &os,
  const vector<int> &arg) {
  return displayVector<int, int>(os, arg);
}

ostream & ParseUtils::displayVectorString(ostream &os,
  const vector<string> &arg) {
  return displayVector<string, string>(os, arg);
}

// Return a hash of a valuation, assuming boolean values.
// (Basically, binary to integer.)
// If valuation.size() == 2,
// hash = val[0] * 1 + val[1] * 2 + val[2] * 4
unsigned int ParseUtils::valuation2hash(vector<char> valuation) {
  int result = 0;
  int mul = 1;
  for (int i = 0; i < (int) valuation.size(); i++) {
    result += mul * valuation[i];
    mul *= 2;
  }
  return result;
}

// Return a valuation of a hash. (Basically, integer to binary.)
// If size == 2, hash = val[0] * 1 + val[1] * 2 + val[2] * 4.
vector<char> ParseUtils::hash2valuation(unsigned int hash, unsigned int size) {
  vector<char> result;
  //int mul = (int)pow((double)2, (double)size);
  int rem = 0;
  for (unsigned int i = 0; i < size; i++) {
    rem = hash % 2;
    hash = (hash - rem) / 2;
    result.push_back(rem);
  }
  return result;
}

// Set the logger.
ostream& ParseUtils::setLogger(ostream &os) {
  logger = &os;
  return os;
}

// Get the logger.
ostream& ParseUtils::getLogger() {
  return *(logger);
}

// EOF ParseUtils.cc
