/*!
 * \file ParsedSM.cc
 * \brief Forms a generic state machine parsed from text description file.
 *
 * \author Nicholas Fette
 * \date 14 July 2008
 *
 * \ingroup mod-log-planner
 *
 */


/*********************************
 * Include header
 *********************************/
#include "ParsedSM.hh"
#include "ParseUtils.hh"         // for ParseUtils static object.
#include "ParseUtilsInternal.hh" // for operator<<(ostream&, vector<T>&)

#define POUT cerr

/*********************************
 * Methods
 *********************************/

ParsedSM::ParsedSM(char *file) : SMfile(file), fin(SMfile) {
  env = NULL;
  sys = NULL;
  // nothing to do here
}

ParsedSM::~ParsedSM() {
  // clean up memory
  // (nothing to do)
}

// Parse a state machine text file,
// as described in DGC07:User:Nfette/Summer 2008.
// Also create the data structure to represent it.
void ParsedSM::init() {
  
  // Check that file exists.
  if (!fin) {
    POUT << "Couldn't find file " << SMfile << endl;
    throw SMException("Couldn't find file \"" + string(SMfile) + "\"");
  }
  POUT << "Opened an input file stream on " << SMfile << endl;

  // Find header
  string s;
  if (!getline(fin, s) || (s.find("State machine") == string::npos
      && s.find("state machine") == string::npos) ) {
    // POUT << "Incorrect first line: should have \"State machine\"." << endl;
    throw SMException("Incorrect first line: should have \"State machine\".");
  }
  // POUT << "Found the correct header: " << s << endl;

  // Build a queue of all the tokens in the file.
  // For each statement, do something to store the data.
  int tokens;
  vector<string> args;
  vector<string>::iterator iter;
  vector<bool>::iterator iterb;
  int ii;
  int argi;
  string whichStatement;
  bool gettingOutputs;

  // Just temporarily stick the values into these variables.
  // (Need to move these declarations closer to their usage, later)
  int error;
  int initial;
  vector<string> inputs;
  vector<string> outputs;
  int state;
  int errorOverride;
  bool isErrorOverride;
  int successorCount;
  vector<int> successors;
  string valuationString;
  vector<bool> valuation;
  vector<bool> valIn;
  vector<bool> valOut;
  inputCount = 0;
  outputCount = 0;

  while ((tokens = readStatement(args)) >= 0) {
    whichStatement = args.front();

    // Select the appropriate statement type.
    if (whichStatement == "Version") {
      version = args.at(2);
      POUT << "Using the version \"" << args.at(2) << "\"" << endl;

    } else if (whichStatement == "Name") {
      name = args.at(2);
      // Display parsed info.
      POUT << "Using the name \"" << args.at(2) << "\"" << endl;

    } else if (whichStatement == "Inputs") {
      // POUT << "Going to count input variables by name." << endl;
      for (iter = args.begin() + 3; iter != args.end(); iter++) {
        if (string(*iter) == "}") { break; }
        inputCount++;
        inputNames.push_back(*iter);
      }
      // POUT << "Counted input variables: " << inputCount << endl;
      // Display parsed info.
      POUT << "Using the inputs ";
      for (ii = 0; ii < inputCount; ii++) {
        POUT << inputNames[ii] << (( ii == inputCount - 1)? "." : ", " );
      }
      POUT << endl;
      
    } else if (whichStatement == "Outputs") {
      for (iter = args.begin() + 3; iter != args.end() && *iter!="}"; iter++) {
        //if (string(*iter) == "}") { break; }
        outputCount++;
        outputNames.push_back(*iter);
      }
      // Display parsed info.
      POUT << "Using the outputs ";
      for (ii = 0; ii < outputCount; ii++) {
        POUT << outputNames[ii] << (( ii == outputCount - 1 )? "." : ", " );
      }
      POUT << endl;

    } else if (whichStatement == "Error") {
      error = atoi(args.at(2).c_str());
      errorID = error;
      // Display parsed info.
      POUT << "Setting the error state to " << error << "." <<  endl;

    } else if (whichStatement == "Initial") {
      initial = atoi(args.at(2).c_str());
      initialID = initial;
      // Display parsed info.
      POUT << "Setting the initial state to " << initial << "." << endl;

    } else if (whichStatement == "State") {
      state = atoi(args.at(3).c_str());
      // Get override error successor.
      int offset = 0;
      isErrorOverride = false;
      errorOverride = errorID;
      if (args.at(5) != "]") {
        isErrorOverride = true;
        errorOverride = atoi(args.at(5).c_str());
        offset++;
      }
      // Get successors.
      successors.clear();
      successorCount = 0;
      for (argi = 7 + offset; argi < (int)args.size(); argi++) {
        if (args.at(argi) == "}") {
          break;
        }
        successors.push_back(atoi(args.at(argi).c_str()));
        successorCount++;
      }

      // Get valuation of state variables.
      valuationString = args.at(argi + 2);
      valuation.clear();
      valIn.clear();
      valOut.clear();

      gettingOutputs = false;
      for (ii = 0; ii < (int)valuationString.length(); ii++) {
        char c = valuationString.at(ii);
        gettingOutputs |= (c == '/');
        if (isdigit(c)) {
          if (gettingOutputs) {
            valOut.push_back(atoi(&c));
          } else {
            valIn.push_back(atoi(&c));
          }
          valuation.push_back(atoi(&c));
        }
      }

      // Create the data structure for the state and add it to astate.
      SMState newstate(this, state, errorOverride, successors, valIn, valOut);
      astate.push_back(newstate);

      // Display parsed info.
      POUT << "Adding a state number " << args.at(3) << "." << endl;
      if (isErrorOverride) {
        POUT<<"  With error override state "<<errorOverride<<"."<<endl;
      }
      POUT << "  With successors ";
      for (ii = 0; ii < (int)successors.size(); ii++) {
        POUT<<successors.at(ii)<<((ii==(int)successors.size()-1)?".":", ");
      }
      POUT << endl << "  With valuation ";//<< valuationString << ", aka ";
      for (iterb = valIn.begin();
        iterb != valIn.end(); iterb++) {
        POUT << *iterb;
      }
      POUT << "/";
      for (iterb = valOut.begin();
        iterb != valOut.end(); iterb++) {
        POUT << *iterb;
      }
      POUT << "." << endl;

    } else if (whichStatement == "End") {
      // Display parsed info.
      POUT << "End of state machine." << endl;

    } else {
      // Display parsed info.
      POUT << "Unknown command sequence:";
      for (iter = args.begin(); iter != args.end(); iter++) {
        POUT << *iter << " ";
      }
      POUT << endl;
    } // select command type
    
    // Just to make sure I didn't leave any tokens from this statement.
    args.clear();
  } // while reading tokens
  fin.close();

  // Should now have the data structure assembled.
  // Initialize the current state:
  trace.push_back(initialID);
} // init()

char * ParsedSM::getSMfile() {
  return SMfile;
}

int ParsedSM::readStatement(vector<string> &args) {
  
  int tokenCounter = -1;
  string s;
  string::size_type search;
  // Loop over lines and separate tokens
  while(getline(fin, s) || leftover.length() > 0) {
    // Remove comments
    search = s.find("#", 0);
    if (search != string::npos) {
      s.erase(search);
    }
    // Add to old string.
    // I have a feeling that getline chops the trailing newline, which I need.
    s = leftover + s + " ";
    //POUT << "Looking at the string \"" << s << "\"." << endl;
    while (s.erase(0, s.find_first_not_of(" \t\n")).length() > 0) {
      // match against possible tokens, store and go on.
      //POUT << "String is now \"" << s << "\"." << endl;
      if ((search = s.find_first_of(":;<>{}[]()")) == 0) {
        // Consider as a special character.
        //POUT << "Got special char: \"" << s.substr(0, 1) << "\"" << endl;
        args.push_back(s.substr(0, 1));
        s.erase(0, 1);
        tokenCounter++;
        if (args.back() == ";") {
          //POUT << "Reached the end of a statement." << endl;
          leftover = s;
          return tokenCounter;
        }
      } else if ((search = s.find_first_of(" \t\n\v\f\r:;<>{}[]()")) != string::npos
        && search > 0) {
        //POUT << "Found the token \"" << s.substr(0, search) << "\"." << endl;
        args.push_back(s.substr(0, search));
        s.erase(0, search);
        tokenCounter++;
      } else {
        // Didn't find a match this time around,
        // maybe there is a token without an ending.
        args.push_back(s);
        s.erase();
        tokenCounter++;
        break;
      }
    }
    leftover = s;
  }
  // What to do if file is over?
  return tokenCounter;
}

int ParsedSM::getInputCount() const {
  return inputCount;
}

int ParsedSM::getOutputCount() const {
  return outputCount;
}

vector<string> ParsedSM::getInputNames() const {
  return inputNames;
}

vector<string> ParsedSM::getOutputNames() const {
  return outputNames;
}

void ParsedSM::setEnvironment(Environment *arg) {
  env = arg;
}

void ParsedSM::setSystem(System *arg) {
  sys = arg;
}

// Cause the state machine to read inputs, transition, and call output.
void ParsedSM::step() {
  // 0. State should have been initialized by a call to init().
  //    However, no action was taken in the initial state.

  // 1. Evaluate input and find a matching successor.
  vector<bool> newInputs = env->getInputs(getInputNames());

  // 2. Identify current state's successors.
  vector<int> newSuccs = astate[trace.back()].getSuccessors();
  int newState = astate[trace.back()].getErrorID();
  for (unsigned int i = 0; i < newSuccs.size(); i++) {
    if (astate.at(newSuccs[i]).getInputVector() == newInputs) {
      newState = newSuccs[i];
      break;
    }
  }

  // 3. Update state to it, or to the error state for this state.
  // Uh-oh, memory-oh!
  trace.push_back(newState);

  // 4. Execute system on new output state.
  sys->execute(astate.at(newState).getOutputVector(), getOutputNames());

}

// Get the execution history, ie the most recent state id is pushed back.
vector<int> ParsedSM::getTrace() {
  return trace;
}

// Get the most recent state id.
int ParsedSM::getState() {
  return trace.back();
}

ostream& ParsedSM::display(ostream &os) const {
  // Decide whether to print input format or dot graph.
  if (1) {
    os << "State machine" << endl;
    os << "Name: " << name << ";" << endl;
    os << "Version: " << version << ";" << endl;
    ParseUtils::setVectorDelims("{", "}", " ");
#ifndef VECTOR_OFF
    os << "Inputs: " << inputNames << ";" << endl;
    os << "Outputs: " << outputNames << ";" << endl;
#else
    os << "Inputs: ";
    ParseUtils::displayVector<string>(os, inputNames);
    os << ";" << endl;
    os << "Outputs: ";
    ParseUtils::displayVector<string>(os, outputNames);
    os << ";" << endl;
#endif
    os << "Error: " << errorID << ";" << endl;
    os << "Initial: " << initialID << ";" << endl;
    for (int i = 0; i < (int)astate.size(); i++) {
      os << "State: " << astate[i] << ";" << endl;
    }
    os << "End" << endl;
  } else {
    os << "Not implemented, clearly." << endl;
  }
  return os;
}

/*
Err_t ParsedSM::planLogic(vector<StateProblem_t> &problems, PlanGraph *graph, Err_t prevErr, 
  VehicleState &vehState, Map *map, 
  Logic_params_t &params, int& currentSegmentId, int& currentLaneId,
  bool& replanInZone) {
  
  problems.push_back(current_state);
  return LP_OK;
}
*/

ostream & SMState::display(ostream &os) const {
  os << "(" << myID << " [";
  if (parent->getErrorID() != myErrorID) {
    os << myErrorID;
  } else {
    os << " ";
  }
  os << "] ";

  ParseUtils::setVectorDelims("{", "}", " ");
#ifndef VECTOR_OFF
  os << succ;
  ParseUtils::setVectorDelims("", "", "");
  os << " <";
  os << ivars << "/" << ovars;
  os << "> )";
#else
  ParseUtils::displayVector<int>(os, succ);
  ParseUtils::setVectorDelims("", "", "");
  os << " <";
  ParseUtils::displayVector<bool>(os, ivars);
  os << "/";
  ParseUtils::displayVector<bool>(os, ovars);
  os << "> )";
#endif
  return os;
}

ostream & operator<<(ostream &os, const SMState &arg) {
  return arg.display(os);
}

ostream & operator<<(ostream &os, const ParsedSM &arg) {
  return arg.display(os);
}

// EOF ParsedSM.cc
