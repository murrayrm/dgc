// File: ParseUtils.hh
// Extras for displaying output, mostly.

#ifndef _PARSE_UTILS_HH_
#define _PARSE_UTILS_HH_

#include <ostream>
#include <vector>

using namespace std; 

class ParseUtils {
  // Suppose we want to print a vector like (1, 2, 3).
  // Begin with "(", separate with ", ", and end with ")".
  // What does the beginning of the vector look like?
  static string PRETTY_VECTOR_BEGIN;
  // What does the end of a vector look like?
  static string PRETTY_VECTOR_END;
  // What do we see between elements of the vector?
  static string PRETTY_VECTOR_SEP;
public:
  // Set the delimiters for printing vectors.
  static void setVectorDelims(string begin, string end, string sep);
  // Help define operators for vector printing.
  template<class T>
  static ostream & displayVector(ostream &os, const vector<T> &arg);
  static ostream & displayVectorBool(ostream &os, const vector<bool> &arg);
  static ostream & displayVectorInt(ostream &os, const vector<int> &arg);
  static ostream & displayVectorString(ostream &os, const vector<string> &arg);
  static unsigned int valuation2hash(vector<bool> valuation);
  static vector<bool> hash2valuation(unsigned int hash, unsigned int len);
};

#endif // _PARSE_UTILS_HH_
// EOF ParseUtils.hh
