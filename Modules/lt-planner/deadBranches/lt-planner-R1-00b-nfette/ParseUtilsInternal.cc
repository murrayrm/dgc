// File ParseUtilsInternal.cc
// Extras for displaying output, mostly. "Internal" because this
// attempts to avoid globally redefining operator<< on vectors.

#include "ParseUtils.hh"
#include "ParseUtilsInternal.hh"

using namespace std;

// The frames library also redefines these.
#ifndef VECTOR_OFF
ostream &operator<<(ostream &os, const vector<int> &arg) {
  return ParseUtils::displayVector<int>(os, arg);
}

ostream &operator<<(ostream &os, const vector<bool> &arg) {
  return ParseUtils::displayVector<bool>(os, arg);
}

ostream &operator<<(ostream &os, const vector<string> &arg) {
  return ParseUtils::displayVector<string>(os, arg);
}
#endif

// EOF ParseUtilsInternal.cc
