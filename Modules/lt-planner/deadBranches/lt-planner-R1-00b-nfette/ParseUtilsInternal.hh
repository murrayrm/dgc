// File: ParseUtilsInternal.hh
// Extras for displaying output, mostly. "Internal" because it
// otherwise globally redefines the operator<< for vectors.

#ifndef _PARSE_UTILS_INTERNAL_HH_
#define _PARSE_UTILS_INTERNAL_HH_

#include <vector>  // for vector
#include <ostream> // for ostream

using namespace std; 

#ifndef VECTOR_OFF
ostream &operator<<(ostream &os, const vector<int> &arg);
ostream &operator<<(ostream &os, const vector<bool> &arg);
ostream &operator<<(ostream &os, const vector<string> &arg);
#else
extern ostream &operator<<(ostream &os, const vector<int> &arg);
extern ostream &operator<<(ostream &os, const vector<bool> &arg);
extern ostream &operator<<(ostream &os, const vector<string> &arg);
#endif

#endif // _PARSE_UTILS_INTERNAL_HH_
// EOF ParseUtilsInternal.hh
