/*!
 * \file TrafficSM.cc
 * \brief Define the interface for TrafficSM.
 * \author Nicholas Fette
 * \date July 2008
 *
 * \ingroup TrafficSM
 *
 * see DGC07:Lt-planner
 */

#include "TrafficSM.hh"
#include "ParseUtils.hh"         // For ParseUtils static object.
#include "ParseUtilsInternal.hh" // for operator<< using ParseUtils.

#include "temp-planner-interfaces/Console.hh" // for Console
#include "temp-planner-interfaces/Utils.hh"   // for Utils
#include "temp-planner-interfaces/AliceStateHelper.hh" // For velocityMag.
// REMOVE #include <deque>


//! This is supposed to be a virtual class!
TrafficSM::TrafficSM () {
  // Nothing to do here.
}

char *TrafficSM::DEFAULT_PSM_FILE = "../lt-planner/default.psm";

//! This is supposed to be a virtual class!
TrafficSM::~TrafficSM () {
  // Nothing to do here.
}

/*!
 * \brief Get a "default" traffic state machine.
 *
 * Use this to set up a "default" traffic state machine, presumably
 * from a single text file described in the configuration file, such as
 * with the parameter DEFAULT_PSM_FILE = "default.psm".
 */
TrafficSM* TrafficSM::getDefaultTrafficSM() {
  ParsedSM *sam = new ParsedSM(DEFAULT_PSM_FILE);
  sam->init();
  TrafficStop *ts = new TrafficStop(sam);
  ts->destroySamOnExit = true;
  return ts;
}

TrafficStop::TrafficStop(ParsedSM *psm) : Environment(), System(), sam(psm)
{
  // Must do these things for object structure to work.
  sam->setEnvironment(this);
  sam->setSystem(this);
  destroySamOnExit = false;

  // Init current status, complete state representation
  current_status.state       = DRIVE;
  current_status.probability = 0.99;
  current_status.flag        = NO_PASS;
  current_status.region      = ROAD_REGION;
  current_status.planner     = RAIL_PLANNER;
  current_status.obstacle    = OBSTACLE_SAFETY;

  // Define the input map->
  inputMap["justBeforeStopSign"] = &TrafficStop::isJustBeforeStopSign;
  inputMap["nowStopped"]         = &TrafficStop::isNowStopped;

}

// This had better not
TrafficStop::~TrafficStop() {
  if (destroySamOnExit) {
    delete sam;
  }
}

// Expose inner workings for useful information. Maybe should be const.
ParsedSM * TrafficStop::getPSM() {
  return sam;
}

string TrafficStop::getStateString() {
  char result[16];
  sprintf(result, "%d", sam->getState());
  return string(result);
}

Err_t TrafficStop::planLogic( TrafficData *arg )
{
  // I call other functions that need the traffic data.
  tada = arg;

  // Build a temporary output to fool the planner.
  StateProblem_t *p = new StateProblem_t();
  p->state       = current_status.state;
  p->region      = current_status.region;
  p->obstacle    = current_status.obstacle;
  p->planner     = current_status.planner;
  p->flag        = current_status.flag;
  p->probability = current_status.probability;

  // Push new problem into queue. We can modify it there.
  tada->problems->push_back(p);

  // Step the state machine. See ParsedSM::step near ParsedSM.cc:310.
  sam->step();

  // Change the variables we use for printing.
  current_status.state = tada->problems->back()->state;
  current_status.region = tada->problems->back()->region;
  current_status.obstacle = tada->problems->back()->obstacle;
  current_status.planner = tada->problems->back()->planner;
  current_status.flag = tada->problems->back()->flag;
  current_status.probability = tada->problems->back()->probability;

  // Don't replan from current position.
  tada->params->planFromCurrPos = false;

  return LP_OK;
}

const vector<string>& TrafficStop::getInputNames() {
  return sam->getInputNames();
}

// Get all the input values in default order.
const vector<bool>& TrafficStop::getInputs() {
  return getInputs(getInputNames());
}

// Get the named inputs in their order, or false for an unkown name.
const vector<bool>& TrafficStop::getInputs(const vector<string>& names) {
  envInputVals.clear();
  for (int i = 0; i < (int)names.size(); i++) {
    // Evaluate one of the functions in the input map, using my TrafficData.
    envInputVals.push_back(inputMap[names[i]](tada));
  }
  return envInputVals;
}

// Take any appropriate action after state change. (None required)
void TrafficStop::execute(const vector<bool>& outputVals,
  const vector<string>& outputNames) {
  // Keep Console informed about the state.
  static int myLastState = sam->getState();
  int myNewState = sam->getState();
  if (myLastState != myNewState) {
    string outputValsString = "<";
    for (int i = 0; i < (int)outputVals.size(); i++) {
      outputValsString += ((outputVals[i])?"1":"0");
    }
    outputValsString += ">";
    Console::addMessage("LTP: State transition: %d -> %d %s", myLastState,
      myNewState, outputValsString.c_str());
    // Log.getStream(5) << "LTP: State transition: " << myLastState << " -> "
    // << myNewState << " " << outputValsString;
    myLastState = myNewState;
  }

  // Act on output "drive"
  tada->problems->at(0)->state = (outputVals[0])?DRIVE:STOP_INT;

  // Keep Console informed about backing up.
  if (TrafficStop::isSegmentUturn(tada)) {
    Console::addMessage("LTP: Segment is a UTURN: panic");
  }
}

bool TrafficStop::isJustBeforeStopSign(TrafficData *td) {
  double current_velocity = AliceStateHelper::getVelocityMag(*(td->vehState));
  double stopping_distance = pow(current_velocity,2)
    / (2.0 * 0.25); // DESIRED_DECELERATION
  stopping_distance = max ( stopping_distance, 5.0 ); // MAGIC
  double stopline_distance = Utils::getDistToStopline(td->params->m_path);
  return (stopline_distance > 0.0)
         && ((stopline_distance < stopping_distance)
            || (stopline_distance < 5));
}

bool TrafficStop::isNowStopped(TrafficData *td) {
  double currentVelocity = AliceStateHelper::getVelocityMag(*(td->vehState));
  // Warning: magic number. Defined in, oh, every other module, SEPARATELY!
  return currentVelocity < 0.2;
}

//! Not very well-defined, is it?
bool TrafficStop::isObstacleInWay(TrafficData *td) {
  return false;
}

//! Straight out of gcinterface/SegGoals/SegmentType.
bool TrafficStop::isSegmentRoad(TrafficData *td) {
  return td->params->segment_type == SegGoals::ROAD_SEGMENT;
}

bool TrafficStop::isSegmentZone(TrafficData *td) {
  return td->params->segment_type == SegGoals::PARKING_ZONE;
}

bool TrafficStop::isSegmentIntersection(TrafficData *td) {
  return td->params->segment_type == SegGoals::INTERSECTION;
}

bool TrafficStop::isSegmentPreZone(TrafficData *td) {
  return td->params->segment_type == SegGoals::PREZONE;
}

bool TrafficStop::isSegmentUturn(TrafficData *td) {
  return td->params->segment_type == SegGoals::UTURN;
}

bool TrafficStop::isSegmentDriveAround(TrafficData *td) {
  return td->params->segment_type == SegGoals::DRIVE_AROUND;
}

bool TrafficStop::isSegmentBackUp(TrafficData *td) {
  return td->params->segment_type == SegGoals::BACK_UP;
}

bool TrafficStop::isSegmentEndOfMission(TrafficData *td) {
  return td->params->segment_type == SegGoals::END_OF_MISSION;
}

bool TrafficStop::isSegmentEmergencyStop(TrafficData *td) {
  return td->params->segment_type == SegGoals::EMERGENCY_STOP;
}

bool TrafficStop::isSegmentUnknown(TrafficData *td) {
  return td->params->segment_type == SegGoals::UNKNOWN;
}

bool TrafficStop::isSegmentStartChute(TrafficData *td) {
  return td->params->segment_type == SegGoals::START_CHUTE;
}

//! Straight out of gcinterface/SegGoals/IntersectionType.
bool TrafficStop::isIntersectionStraight(TrafficData *td) {
  return td->params->seg_goal.intersection_type
    == SegGoals::INTERSECTION_STRAIGHT;
}

bool TrafficStop::isIntersectionLeft(TrafficData *td) {
  return td->params->seg_goal.intersection_type
    == SegGoals::INTERSECTION_LEFT;
}

bool TrafficStop::isIntersectionRight(TrafficData *td) {
  return td->params->seg_goal.intersection_type
    == SegGoals::INTERSECTION_RIGHT;
}

// EOF TrafficSM.cc
