/*!
 * \file LtPlanner.hh
 * \brief Main include file for lt-planner library
 * \author Nicholas Fette
 * \date July 2008
 *
 * This file includes the types needed for Planner's calls and the internal
 * implementation.
 */

/*!
 * \mainpage Lt-Planner Documentation
 * \section intro_sec Introduction
 *
 * Lt-Planner is an internal module of Team Caltech, providing generic state
 * machine support and a few implementations of state machines for use in
 * traffic planning logic by the Planner module.
 *
 * Further documentation may be available at:
 * http://gc.caltech.edu/wiki/Lt-planner
 *
 * \defgroup ParsedSM	Parsed state machines
 * \defgroup TrafficSM	Traffic state machines
 *
 */

#ifndef _LT_PLANNER_HH_
#define _LT_PLANNER_HH_

 /********************************
 * Includes for things I may need
 *********************************/
#include "temp-planner-interfaces/PlannerInterfaces.h" // for StateProblem_t
#include "TrafficSM.hh"    // The other white meat

 /********************************
 * Class definitions
 *********************************/

//! Wrapper for traffic logic state machines of class TrafficSM, to be used by
//! Planner in place of LogicPlanner.
class LtPlanner
{
  
  public:

  /*! Constructor */
  LtPlanner();

  /*! Destructor */
  virtual ~LtPlanner();
  
  /*! Initialization, just for backward compatibleness */  
  void init();

  /*! Alternative destructor, just for backward compatibleness */
  void destroy();
  
  /*! Read input config file and store in class variables  */
  void readConfig();

  //! Planner might ask for this. Don't know what it does yet.
  void resetState();

  //! Planner might ask for this also.
  void updateDisplayInfo();
  
  /*! Do the actual logic planning.
   * Probably just forwards the arguments to a state machine.
   */
  Err_t planLogic(vector<StateProblem_t*> &problems, PlanGraph *graph,
    Err_t prevErr, VehicleState &vehState, Map *map, Logic_params_t &params,
    int& currentSegmentId, int& currentLaneId, bool& replanInZone);

  /*! Planner might call this function later. */
  void updateIntersectionConsole() { ; }

  private:
  //! Finite State Machine current status
  StateProblem_t current_status;

  //! Alternative finite state machine.
  TrafficSM *tsm;

  //! This one is only needed for development.
  long cycle_counter;

  //! Configuration parameters.
  int LIFE;
  int SM_MODE;
  string DEFAULT_PSM_FILE;

};


#endif // _LT_PLANNER_HH_
// EOF '  LtPlanner.hh'
