/* 
 * Desc: VR Simulator Class
 * Date: 13 July 2008
 * Author: Bjorn Heinbokel
 * CVS: $Id$
*/

#ifndef VR_SIM
#define VR_SIM

#include <iostream>
#include <cstring>
#include <string>
#include <gazebo/gazebo.h>
#include <skynet/skynet.hh>
#include <interfaces/VehicleState.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <pthread.h>
#include <dgcutils/DGCutils.hh>
#include <cmdline.h>


#include <ncurses.h>
#include <cotk/cotk.h>

#include "vrStereo.hh"
#include "vrLadar.hh"

class vrSim
{
  /********************/
  /* Public Functions */
  /********************/
  public: 
  /// Default Contructor
  vrSim();
  
  /// Default destructor
  ~vrSim();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);

  /// Parse the config file
  //int parseConfigFile(const char *configPath);
  
  /// Should we quit?
  bool quit;

  /// Should we pause?
  bool pause;
  
  /// Intitialize Gazebo Client
  int initGzClient();

  /// Initialize Simulation Interface
  int initGzSimIface();

  /// Intialize Stereo Cameras
  int initStereo(int argc, char **argv);
  
  /// Initialize Ladar Sensors
  int initLadar(int argc, char **argv, modulename moduleId, sensnet_id_t sensorId);
  
  /// Initialize console display
  int initConsole();

  /// Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, vrSim *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, vrSim *self, const char *token);
  
  /// Set Model Position
  int gzSetModelPos(string strModelName, double x, double y, double z, 
                                         double roll, double pitch, double yaw);
  
  
  /********************/
  /* Public Variables */
  /********************/
  public:
  /// Program options
  gengetopt_args_info options;
  
  /// Gazebo Client
  gazebo::Client *gzClient;
  
  /// One of the Stereo Cameras
  vrStereoCamera *StereoCam1;

  //Left Front Bumper SICK Ladar
  VRLadar *ladarLFBumper;

  //Middle Front Bumper SICK Ladar
  VRLadar *ladarMFBumper;

  //Right Front Bumper SICK Ladar
  VRLadar *ladarRFBumper;

  //Rear Bumper SICK Ladar
  VRLadar *ladarRearBumper;
  
  /// gz Server ID
  int gzServerID;
  
  /// Simulation Interface
  gazebo::SimulationIface *gzSimIface;
  
  /// Skynet Vehicle State
  VehicleState vehState;
  
  // Console text display
  cotk_t *console;
  
  
};

#endif

