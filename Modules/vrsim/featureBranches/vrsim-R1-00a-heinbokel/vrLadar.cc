/*
 *  \file vrLadar.cc
 *  \brief Used to send ladar data to sensnet
 *  
 *  \author Manuel Segura
 *  \date 2008
 *   
 */

#include "vrLadar.hh"

VRLadar::VRLadar(modulename moduleId, sensnet_id_t sensorId)
{
  memset(this, 0, sizeof(*this));
  this->ladarIface = new gazebo::LaserIface();
  this->moduleId = moduleId;
  this->sensorId = sensorId;
  this->scanId = -1;
  return;
}

VRLadar::~VRLadar()
{
  delete &ladarIface;
  return;
}

// Parse the command line
int VRLadar::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the default config path
  this->defaultConfigPath = dgcFindConfigDir("ladarfeeder");
 
  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  this->skynetKey = atoi(getenv("SKYNET_KEY"));
  
  return 0;
}

//Parse the config file
int VRLadar::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, sensnet_id_to_name(this->sensorId));
  MSG("loading %s", filename);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);
/*
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  this->ladarPort = this->options.port_arg;
  
  // Parse transform
  float px, py, pz;
  float rx, ry, rz;

  if(this->sensorId==SENSNET_PTU_LADAR)
    {
      if (sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz) < 3)
	return ERROR("syntax error in sensor pos argument");
      if (sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz) < 3)
	return ERROR("syntax error in sensor rot argument");
      
      pose3_t pose;
      pose.pos = vec3_set(px, py, pz);
      pose.rot = quat_from_rpy(rx, ry, rz);
      pose3_to_mat44f(pose, this->sens2tool);

      // Record euler angles for display only
      this->sensPos = vec3_set(px, py, pz);
      this->sensRot.x = rx * 180/M_PI;
      this->sensRot.y = ry * 180/M_PI;
      this->sensRot.z = rz * 180/M_PI;

    }
  else
    {
      if (sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz) < 3)
	return ERROR("syntax error in sensor pos argument");
      if (sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz) < 3)
	return ERROR("syntax error in sensor rot argument");
      
      pose3_t pose;
      pose.pos = vec3_set(px, py, pz);
      pose.rot = quat_from_rpy(rx, ry, rz);
      pose3_to_mat44f(pose, this->sens2veh);
      
      // Record euler angles for display only
      this->sensPos = vec3_set(px, py, pz);
      this->sensRot.x = rx * 180/M_PI;
      this->sensRot.y = ry * 180/M_PI;
      this->sensRot.z = rz * 180/M_PI;
    }
*/
  return 0;
}

int VRLadar::initSensnet(const char *configPath)
{
  if (sizeof(LadarRangeBlob) % 512 != 0)
     return ERROR("invalid blob size %d; needs padding of %d", 
                  sizeof(LadarRangeBlob), 512 - sizeof(LadarRangeBlob) % 512);
   
  // Create page-aligned blob to enable DMA logging
  this->blob = (LadarRangeBlob*) valloc(sizeof(LadarRangeBlob));
  
  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
     return ERROR("unable to connect to sensnet");
 
  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");

  // Subscribe to vehicle state messages
  if (!this->options.disable_state_flag)
  {
    if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
      return ERROR("unable to join state group");
  }

  // Subscribe to the PTU state messages is applicable
  if(this->sensorId==SENSNET_PTU_LADAR)
  {
    if (sensnet_join(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB, sizeof(this->ptublob)) != 0)
      return ERROR("unable to join PTU state group");
  } 

  return 0;
}

int VRLadar::finiSensnet()
{
  sensnet_disconnect(this->sensnet);
  this->blob = NULL;

  return 0;
}

//Capture Ladar data from gazebo
int VRLadar::captureScan()
{ 
  if(this->sensorId==SENSNET_RIEGL)
  {
    //double ranges[201];
    //unsigned long long times[201];
    double angles[201];
    //uchar amps[201];
    //uchar quals[201];
    //unsigned long long fr_time;

    this->numPoints = 201; //this->riegl->GetScanFrame(times,ranges,angles,amps,quals, &fr_time);
    //TODO: scan information from gazebo right here

    this->scanId += 1;
    this->scanTime = DGCgettime();

    //Unpack the data.
    //Assuming a sensor frame that is x-forward, y-left
    for (int i=0; i < this->numPoints; i++)
    {
       this->points[i][0] = angles[i]-M_PI/2;
       this->points[i][1] = this->ladarIface->data->ranges[i];//ranges[i];
       this->points[i][2] = this->ladarIface->data->intensity[i];//amps[i];
    }

    //Get the matching state data
    if(this->getState(this->scanTime) != 0)
    {
       return MSG("unable to get state; ignoring scan");
    }
  }
  else
  {
    int i, numRanges;
    float pt;//, ranges[181];
    //int values[181];

    //Read data from sensor. Note that this ignores errors on read
    //(e.g., CRC errors) to prevent the program from terminating.
/*
    if(sick_driver_read(this->sick, &this->scanTime, 181, &numRanges, ranges, values) != 0)
    {
       return 0;
    }
*/
    this->scanId += 1;
    
    numRanges = this->ladarIface->data->range_count;
    // Unpack the data.  Assumes standard format for SICK.
    // This also checks against a min/max scan angle to exclude points
    // that lie of the vehicle itself.
    this->numPoints = 0;
    for (i = 0; i < numRanges; i++)
    {
      pt = -(i - numRanges/2) * M_PI/180;

      if (pt < this->ladarIface->data->min_angle * M_PI/180)
            continue;
      if (pt > this->ladarIface->data->max_angle * M_PI/180)
            continue;

      this->points[this->numPoints][0] = pt;
      this->points[this->numPoints][1] = this->ladarIface->data->ranges[i];//ranges[i];
      this->points[this->numPoints][2] = this->ladarIface->data->intensity[i];//values[i];
      this->numPoints++;
    }

    //Get the matching state data
    if (this->getState(this->scanTime) != 0)
    {
       return MSG("unable to get state; ignoring scan");
    }
    
  }
  return 0;
}

int VRLadar::writeSensnet()
{
  int i;
  pose3_t pose;
  LadarRangeBlob *blob;

  blob = this->blob;

  //Construct the blob header
  blob->blobType = SENSNET_LADAR_BLOB;
  blob->version = LADAR_BLOB_VERSION;
  blob->sensorId = this->sensorId;
  blob->scanId = this->scanId;
  blob->timestamp = this->scanTime;
  blob->state = this->state;

  if(this->sensorId==SENSNET_PTU_LADAR)
  {
    // For the ptu ladar, we'll need to be careful about transforms
    float sens2ptu[4][4];

    //Sensor to vehicle transform
    mat44f_mul(sens2ptu,this->ptublob.tool2ptu, this->sens2tool);
    mat44f_mul(this->sens2veh, this->ptublob.ptu2veh, sens2ptu);

    memcpy(blob->sens2veh, this->sens2veh, sizeof(this->sens2veh));
    mat44f_inv(blob->veh2sens, blob->sens2veh);
  }
  else
  {
    //Sensor to vehicle transform
    memcpy(blob->sens2veh, this->sens2veh, sizeof(this->sens2veh));
    mat44f_inv(blob->veh2sens, blob->sens2veh);

  }
  //Vehicle to local transform
  pose.pos = vec3_set(blob->state.localX,
  		      blob->state.localY,
		      blob->state.localZ);
  pose.rot = quat_from_rpy(blob->state.localRoll,
			   blob->state.localPitch,
			   blob->state.localYaw);
			   pose3_to_mat44f(pose, blob->veh2loc);  
  mat44f_inv(blob->loc2veh, blob->veh2loc);

  //Reset reserved values
  memset(blob->reserved, 0, sizeof(blob->reserved));
  blob->ptuPan = 0;
  blob->ptuTilt = 0;
  blob->ptuPanSpeed = 0;
  blob->ptuTiltSpeed = 0;

  // fill ptu fields if ptu-ladar
  if (this->sensorId==SENSNET_PTU_LADAR)
  {     
    blob->ptuPan = this->ptublob.currpan;
    blob->ptuTilt = this->ptublob.currtilt;
    blob->ptuPanSpeed = this->ptublob.currpanspeed;
    blob->ptuTiltSpeed = this->ptublob.currtiltspeed;
  }

  // Copy the scan data
  blob->numPoints = this->numPoints;
  for (i = 0; i < this->numPoints; i++)
  {
    assert(i < (int) (sizeof(blob->points) / sizeof(blob->points[0])));
    assert(i < (int) (sizeof(this->points) / sizeof(this->points[0])));
    blob->points[i][0] = this->points[i][0];
    blob->points[i][1] = this->points[i][1];
    blob->intensities[i] = (uint8_t) this->points[i][2];
  }
  
  // Write blob
  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, this->sensorId, SENSNET_LADAR_BLOB,
                    this->scanId, sizeof(*blob), blob) != 0)
    return ERROR("unable to write blob");

  return 0;
}

int VRLadar::getState(uint64_t timestamp)
{
  int blobId;
  int ptublobId;
  
  // Default to all zeros in state
  memset(&this->state, 0, sizeof(this->state));
  
  // Get the current state value
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
                   &blobId, sizeof(this->state), &this->state) != 0)
    return ERROR("unable to read state data");
  if (blobId < 0)
    return ERROR("state is invalid");

  // Get the current PTU state if applicable
  if (this->sensorId==SENSNET_PTU_LADAR)
  {      
    //Default to all zeros in PTU state
    memset(&this->ptublob, 0, sizeof(this->ptublob));

    if (sensnet_read(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB,
		     &ptublobId, sizeof(this->ptublob), &this->ptublob) != 0)
	return ERROR("unable to read ptu-state data");
    if(ptublobId < 0)
       return ERROR("ptu state is invalid");
  }

  return 0;
}
