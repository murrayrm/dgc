/*!
 *  \file vrMain.cc
 *  \brief Main Thread for Gazebo VR simulator
 *
 *  \author Bjorn Heinbokel, Manuel Segura
 *  \date 2008
 *
 *  This file implements the main function of the gazebo client side functionality.
 */

#include <math.h>

#include <iostream>
#include <cstring>
#include <string>
#include <gazebo/gazebo.h>
#include <skynet/skynet.hh>
#include <interfaces/VehicleState.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <pthread.h>
#include <dgcutils/DGCutils.hh>
#include <cmdline.h>

#include "vrStereo.hh"
#include "vrLadar.hh"
#include "vrSim.hh"

// This is OpenCV visualization
//#include <//usr/include/opencv/cv.h>
//#include <//usr/include/opencv/highgui.h>
//#include "mcv.hh"

using namespace std;

int main (int argc, char **argv)
{
  // Create vr simulation object
  vrSim *myVrSim = new vrSim();
  assert(myVrSim);

  // Parse command line options
  if (myVrSim->parseCmdLine(argc, argv) != 0)
    return -1;
  
  if (myVrSim->initConsole() != 0)
    return -1;
  
  //wait for gazebo to finish starting
  usleep(100000);

  if (myVrSim->initGzClient() != 0)
    return -1;

  if (myVrSim->initGzSimIface() != 0)
    return -1;

  myVrSim->initLadar(argc, argv, MODladarFeederLFBumper, SENSNET_LF_BUMPER_LADAR);
  myVrSim->initLadar(argc, argv, MODladarFeederMFBumper, SENSNET_MF_BUMPER_LADAR);
  myVrSim->initLadar(argc, argv, MODladarFeederRFBumper, SENSNET_RF_BUMPER_LADAR);
  //myVrSim->initLadar(argc, argv, MODladarFeederRearBumper, SENSNET_REAR_BUMPER_LADAR);
  
  // Connect to Skynet
  int snkey = skynet_findkey(argc, argv);
  cout << "The Skynet Key is : " << snkey << '\n';
  SkynetServer skynet_server = SkynetServer(snkey, 150);

  //Start a thread to listen to messages
  pthread_t skynet_thread;
  skynet_thread = skynet_server.start();
  VehicleState vehstate;  

  pthread_mutex_t vehstate_mutex;
  DGCcreateMutex(&vehstate_mutex);
  SkynetGroup sngroup = SkynetGroup(skynet_server.getServer(), SNstate, (char *) &vehstate, 
                          sizeof(VehicleState), NULL, &vehstate_mutex, NULL, NULL);
/* 
  gazebo::Client *gzClient;
  gzClient = new gazebo::Client(); // Allocate memory for new object

  gzClient->ConnectWait(6, 7);

  gazebo::SimulationIface *gzSimIface;

  gzSimIface = new gazebo::SimulationIface();
  gzSimIface->Open(gzClient,"default");

  gazebo::LaserIface ladarIface;
  ladarIface.Open(gzClient, "ladarLFBumperLaserIface");

  char setModelPose[32] = "set_pose3d";

  string strModelName = "aliceBox_model";
*/
  /* Main Loop */
  while(!myVrSim->quit)
  {
    // Update the console
    if (myVrSim->console)
    cotk_update(myVrSim->console);

    pthread_mutex_lock(&vehstate_mutex);
    myVrSim->gzSetModelPos("aliceBox_model",vehstate.siteNorthing,vehstate.siteEasting,1.5,0,0,vehstate.siteYaw);
    pthread_mutex_unlock(&vehstate_mutex);
    
    //Trying to simulate 75Hz
    usleep (13000);

    //ladarIface.Lock(1);
    //cout << ladarIface.data->ranges[0] << " " << ladarIface.data->intensity[0] << "\n";
    //ladarIface.Unlock();

    //TODO: have Ladars write to sensnet here
    myVrSim->ladarLFBumper->captureScan();
    myVrSim->ladarMFBumper->captureScan();
    myVrSim->ladarRFBumper->captureScan();
    //myVrSim->ladarRearBumper->captureScan();

    myVrSim->ladarLFBumper->writeSensnet();
    myVrSim->ladarMFBumper->writeSensnet();
    myVrSim->ladarRFBumper->writeSensnet();
    //myVrSim->ladarRearBumper->writeSensnet();
  }
  
  // Clean up
  //myVrSim->finiConsole();
  //delete &skynet_server;
  //delete &sngroup;

  return 0;
}

