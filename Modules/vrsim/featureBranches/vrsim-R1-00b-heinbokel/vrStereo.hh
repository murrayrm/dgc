/* 
 * Desc: Stereo Camera Sensor Class for Gazebo Simulator vrsim
 * Date: 10 July 2008
 * Author: Bjorn Heinbokel
*/

#ifndef VR_STEREO_CAMERA
#define VR_STEREO_CAMERA


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

//#include <alice/AliceConstants.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <skynet/sn_msg.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <interfaces/StereoImageBlob.h>

#include <gazebo/gazebo.h>

//#include <ncurses.h>
//#include <cotk/cotk.h>

#include "cmdline.h"


/// @brief Stereo Camera Sensor Class for Gazebo VR Simulator
class vrStereoCamera
{
  public:   
  
  /// Default constructor
  vrStereoCamera(int argc, char **argv, _sensnet_id_t sensorId, modulename moduleId);

  /// Default destructor
  virtual ~vrStereoCamera();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv, _sensnet_id_t sensorId, modulename moduleId);

  /// Parse Config File
  int parseConfigFile(const char *configPath);

  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;

  // Is logging enabled?
  // bool enableLog;

  public: gazebo::CameraIface leftCamIface;
  //public: gazebo::CameraIface rightCamIface;
  
  public: gazebo::StereoCameraIface stereoCamIface;
  
  public:

  /// Initialize sensnet
  int initSensnet();

  /// Finalize sensnet
  int finiSensnet();
  
  /// Publish data
  int writeSensnet(VehicleState vehstate);

  // SensNet handle
  sensnet_t *sensnet;

  // SensNet log file
  //sensnet_log_t *sensnet_log;
  
  // Sensnet log file name
  char logName[1024];

  // Sensnet logging stats
  int logCount, logSize;

  // Workspace for stereo blob
  StereoImageBlob *blob;
  StereoImageBlob *test_blob;

  // Current Frame ID
  int32_t frameId;
  
  // Sensor-to-vehicle transform
  float sens2veh[4][4];

};

#endif
