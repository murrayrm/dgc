/*!
 *  \file vrsim.cc
 *  \brief VR Simulator Class 
 *
 *  \author Bjorn Heinbokel
 *  \date 2008
 *
 *  This class implements the main functionality of a gazebo client interface.
 */

#include "vrSim.hh"
#include "vrStereo.hh"
#include "vrLadar.hh"

#include <interfaces/SensnetTypes.h>
#include <interfaces/sn_types.h>


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
vrSim::vrSim()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
vrSim::~vrSim()
{  
  return;
}

// Parse the command line
int vrSim::parseCmdLine(int argc, char **argv)
{  
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
    
  this->snkey = skynet_findkey(argc, argv); 
  this->gzServerID = this->snkey;
  cout << snkey << endl;
/* // THese are remains of the stereofeeder:
  ********************************************
  // Use raw image logging or sensnet logging, but not both
  if (this->options.enable_log_flag && this->options.enable_imagelog_flag)
    return ERROR("sensnet logging and image logging cannot be enabled at the same time");
  
  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
    
  // Fill out sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // Fill out the default config path from the environment if not
  // given on the command line.
  if (!this->options.config_path_given)
    this->options.config_path_arg = dgcFindConfigDir("stereofeeder");
  
  // Load configuration file
  if (this->parseConfigFile(this->options.config_path_arg) != 0)
    return -1;
*/
  return 0;
}

// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"vrSim                                                                      \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"[%QUIT%|%PAUSE%]                                                           \n";
 
// Initialize console display
int vrSim::initConsole()
{
  char filename[1024];
  
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);

  // Initialize the display
  snprintf(filename, sizeof(filename), "test.msg");
  if (cotk_open(this->console, filename) != 0)
    return -1;

  // Display some fixed values
  //cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
  //            "Test Deamon", 523, "Module Name");
                    
  return 0;
}

// Finalize console display
int vrSim::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }  
  return 0;
}

int vrSim::initGzClient()
{
  gzClient = new gazebo::Client();
  gzClient->ConnectWait(this->gzServerID,GZ_CLIENT_ID_USER_FIRST); //connect Client in Wait mode (blocking) 
  return 0;
}

int vrSim::initGzSimIface()
{
  this->gzSimIface = new gazebo::SimulationIface(); // Allocate memory for new object
    
  this->gzSimIface->Open(gzClient,"default");; //connect Simulation Interface
  return 0;
}


int vrSim::initStereo(int argc, char **argv)
{
  /**************************************/
  /*TEST CAMERA MF                      */
  /**************************************/
  // Setup the Stereo Camera Sensors
  this->vrCam_MF_MEDIUM_STEREO = new vrStereoCamera(argc,argv,SENSNET_MF_MEDIUM_STEREO, MODstereoFeederMFMedium);
  
  // Open Interface to gazebo
  this->vrCam_MF_MEDIUM_STEREO->leftCamIface.Open(this->gzClient, "camera_iface_0");
  //this->vrCam_MF_MEDIUM_STEREO->rightCamIface.Open(this->gzClient, "camera_iface_1");
  this->vrCam_MF_MEDIUM_STEREO->stereoCamIface.Open(this->gzClient, "stereo_iface_0");

  // Initialize Sensnet
  this->vrCam_MF_MEDIUM_STEREO->initSensnet();
  
  return 0;
}

int vrSim::processStereo()
{
  int blobId;  

  if(this->vrCam_MF_MEDIUM_STEREO->writeSensnet(this->vehstate) != 0)
    return -1;
}

int vrSim::finiStereo()
{
  this->vrCam_MF_MEDIUM_STEREO->finiSensnet();
}


//TODO: Fix problem with ladarIface (causes program to crash)
//Connects to sensnet properly though
int vrSim::initLadar(int argc, char **argv)
{
        ladarLFRoof = new VRLadar(MODladarFeederLFRoof, SENSNET_LF_ROOF_LADAR);
        ladarLFRoof->parseCmdLine(argc,argv);
        ladarLFRoof->parseConfigFile(ladarLFRoof->defaultConfigPath);
        ladarLFRoof->initSensnet(ladarLFRoof->defaultConfigPath);
        ladarLFRoof->ladarIface->Open(this->gzClient, "ladarLFRoofLaserIface");

        ladarRFRoof = new VRLadar(MODladarFeederRFRoof, SENSNET_RF_ROOF_LADAR);
        ladarRFRoof->parseCmdLine(argc,argv);
        ladarRFRoof->parseConfigFile(ladarRFRoof->defaultConfigPath);
        ladarRFRoof->initSensnet(ladarRFRoof->defaultConfigPath);
        ladarRFRoof->ladarIface->Open(this->gzClient, "ladarRFRoofLaserIface");

        ladarLFBumper = new VRLadar(MODladarFeederLFBumper, SENSNET_LF_BUMPER_LADAR);
        ladarLFBumper->parseCmdLine(argc, argv);
        ladarLFBumper->parseConfigFile(ladarLFBumper->defaultConfigPath);
        ladarLFBumper->initSensnet(ladarLFBumper->defaultConfigPath);
        ladarLFBumper->ladarIface->Open(this->gzClient, "ladarLFBumperLaserIface");

        ladarMFBumper = new VRLadar(MODladarFeederMFBumper, SENSNET_MF_BUMPER_LADAR);
        ladarMFBumper->parseCmdLine(argc, argv);
        ladarMFBumper->parseConfigFile(ladarMFBumper->defaultConfigPath);
        ladarMFBumper->initSensnet(ladarMFBumper->defaultConfigPath);
        ladarMFBumper->ladarIface->Open(this->gzClient, "ladarMFBumperLaserIface");

        ladarRFBumper = new VRLadar(MODladarFeederRFBumper, SENSNET_RF_BUMPER_LADAR);
        ladarRFBumper->parseCmdLine(argc, argv);
        ladarRFBumper->parseConfigFile(ladarRFBumper->defaultConfigPath);
        ladarRFBumper->initSensnet(ladarRFBumper->defaultConfigPath);
        ladarRFBumper->ladarIface->Open(this->gzClient, "ladarRFBumperLaserIface");

        ladarRearBumper = new VRLadar(MODladarFeederRearBumper, SENSNET_REAR_BUMPER_LADAR);
        ladarRearBumper->parseCmdLine(argc, argv);
        ladarRearBumper->parseConfigFile(ladarRearBumper->defaultConfigPath);
        ladarRearBumper->initSensnet(ladarRearBumper->defaultConfigPath);
        ladarRearBumper->ladarIface->Open(this->gzClient, "ladarRearBumperLaserIface");

   return 0;
}

// Handle button callbacks
int vrSim::onUserQuit(cotk_t *console, vrSim *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}

// Handle button callbacks
int vrSim::onUserPause(cotk_t *console, vrSim *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

int vrSim::gzSetModelPos(string strModelName, double x, double y, double z, 
                                              double roll, double pitch, double yaw, int requestNr)
{
  // Setting command name for imposing 3d position
  gazebo::SimulationRequestData *request = &(gzSimIface->data->requests[requestNr]);
  
  request->type = gazebo::SimulationRequestData::SET_POSE3D;

  //memcpy(this->gzSimIface->data->model_req,setModelPose,32);

  // Setting model name for inposing new Pose in Sim Iface
  int i = (int)strModelName.length();
  for(int k=0;k<25;k++)
  {
    request->modelName[k] = 0;
  };
  for(int k=0;k<i;k++)
  {
    request->modelName[k] = strModelName[k];
  };

  this->gzSimIface->Lock(1);


  request->modelPose.pos.x = x;
  request->modelPose.pos.y = -y;
  request->modelPose.pos.z = z;
  request->modelPose.roll = roll;
  request->modelPose.pitch = pitch;
  request->modelPose.yaw = yaw;

  this->gzSimIface->Unlock();

  cout << this->gzSimIface->data->requestCount << endl;

  return 0;

}

int vrSim::processLadars()
{
  this->ladarLFBumper->captureScan();
  this->ladarMFBumper->captureScan();
  this->ladarRFBumper->captureScan();
  this->ladarRearBumper->captureScan();
  this->ladarLFRoof->captureScan();
  this->ladarRFRoof->captureScan();

  this->ladarLFBumper->writeSensnet();
  this->ladarMFBumper->writeSensnet();
  this->ladarRFBumper->writeSensnet();
  this->ladarRearBumper->writeSensnet();
  this->ladarLFRoof->writeSensnet();
  this->ladarRFRoof->writeSensnet();

}


/// Initializes Skynet to receive state signals
int vrSim::initSkynet()
{
  this->skynet_server = new SkynetServer(snkey, 150); // TODO: change 150 to appropriate module enum
  this->skynet_thread = this->skynet_server->start();

  DGCcreateMutex(&this->vehstate_mutex);
  this->sngroup = new SkynetGroup(this->skynet_server->getServer(), SNstate, (char *) &this->vehstate, 
                          sizeof(VehicleState), NULL, &this->vehstate_mutex, NULL, NULL);
                          
  return 0;
}


int vrSim::setVehstate2Alice()
{
  this->sngroup->getMessage((char *) &(this->vehstate), sizeof(VehicleState));
  pthread_mutex_lock(&(this->vehstate_mutex));
  MSG("Northing: %f, Easting: %f",this->vehstate.siteNorthing,this->vehstate.siteEasting);
  int status = this->gzSetModelPos("myAlice", this->vehstate.siteNorthing, this->vehstate.siteEasting, 1.1, 0.0, 0.0, -this->vehstate.siteYaw - 1.5707963,0);

  pthread_mutex_unlock(&(this->vehstate_mutex));
  return 0;

}












