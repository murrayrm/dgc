/*!
 *  \file vrMain.cc
 *  \brief Main Thread for Gazebo VR simulator
 *
 *  \author Bjorn Heinbokel, Manuel Segura
 *  \date 2008
 *
 *  This file implements the main function of the gazebo client side functionality.
 */


#include <math.h>  // used for debugging, simple circular movement of objects 

// include the headder files which declare the main sensor and simulation classes
#include "vrStereo.hh"
#include "vrLadar.hh"
#include "vrSim.hh"

using namespace std;

int main (int argc, char **argv)
{
  uint64_t lastTime;
  long cnt = 0;
  
  // Create vr simulation object
  vrSim *myVrSim = new vrSim();
  assert(myVrSim);

  // Parse command line options
  if (myVrSim->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // initialize console and bring up in terminal 
  if (myVrSim->initConsole() != 0)
    return -1;
  
  // connect gazebo client to gazebo server through libgazebo
  if (myVrSim->initGzClient() != 0)
    return -1;

  // initialize simIface (takes care of placing objects)
  if (myVrSim->initGzSimIface() != 0)
    return -1;


  // Initialize all ladar sensor objects and connect them to gazebo
  /*if (myVrSim->initLadar(argc, argv) !=0)
    return -1;*/

  // init skynet to acquire skynet vehstate information
  if (myVrSim->initSkynet() != 0)
    return -1;
  
  // init the stereo camera sensors and connect them to gazebo
  if (myVrSim->initStereo(argc,argv) !=0)
    return -1;


  /* Main Loop */
  while(!myVrSim->quit)
  {
    // Update the console
    if (myVrSim->console)
      cotk_update(myVrSim->console);

  
    cnt++;
    myVrSim->gzSimIface->data->requestCount=1;
    /*if(myVrSim->gzSetModelPos("myAlice", 0*cos(0.005*6.28*(double)cnt), 
                      -87 + 10*sin(0.001*6.28*(double)cnt), 1, 0.0, 0.0, 0*0.002*(double)cnt, 0) != 0)
      return -1;
    */
    /*myVrSim->gzSimIface->data->requestCount=2;
    status = myVrSim->gzSetModelPos("theBox", 0, 
                      -74.0 + 1*sin(0.01*6.28*(double)cnt) , 1.1, 0.0, 0.0, 90.0, 1);*/


    myVrSim->setVehstate2Alice();


    //Trying to simulate 75Hz
    usleep (13000);

   
    // If paused, or if we are running too fast, give up our time
    // slice.
    if ((DGCgettime() - lastTime > (uint64_t) 1e1)) // 7.5 Hz = 1/133,333 us
    {
      lastTime = DGCgettime();
      cnt++;

      if(myVrSim->processStereo() != 0)
        return  -1;
    }
;


    //Ladars write to sensnet here 
    //myVrSim->processLadars();
  }
  
  // Clean up TODO : write finiSkynet() function
  myVrSim->finiConsole();
  myVrSim->finiStereo();
  delete myVrSim;
  
  return 0;
}

