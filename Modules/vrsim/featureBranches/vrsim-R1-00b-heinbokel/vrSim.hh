/* 
 * Desc: VR Simulator Class
 * Date: 13 July 2008
 * Author: Bjorn Heinbokel
 * CVS: $Id$
*/

#ifndef VR_SIM
#define VR_SIM

#include <iostream>
#include <cstring>
#include <string>
#include <gazebo/gazebo.h>
#include <skynet/skynet.hh>
#include <interfaces/VehicleState.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <pthread.h>
#include <dgcutils/DGCutils.hh>
#include <cmdline.h>
#include <sensnet/sensnet.h>

#include <ncurses.h>
#include <cotk/cotk.h>

#include "vrStereo.hh"
#include "vrLadar.hh"

class vrSim
{
  /********************/
  /* Public Functions */
  /********************/
  public: 
  /// Default Contructor
  vrSim();
  
  /// Default destructor
  ~vrSim();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);

  /// Parse the config file
  //int parseConfigFile(const char *configPath);
  
  /// Should we quit?
  bool quit;

  /// Should we pause?
  bool pause;
  
  /// Intitialize Gazebo Client
  int initGzClient();

  /// Initialize Simulation Interface
  int initGzSimIface();

  /// Intialize Stereo Cameras
  int initStereo(int argc, char **argv);
    
  /// Finish Stereo Cameras
  int processStereo();
  
  /// Finish Stereo Cameras
  int finiStereo();

  //Process Ladar sensors
  int processLadars();

  /// Initialize Skynet Connection
  int initSkynet();
  
  /// Initialize Ladar Sensors
  int initLadar(int argc, char **argv);
  
  /// Initialize console display
  int initConsole();

  /// Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, vrSim *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, vrSim *self, const char *token);
  
  /// Set Model Position
  int gzSetModelPos(string strModelName, double x, double y, double z, 
                                         double roll, double pitch, double yaw, int requestNr);
  /// Set Skynet vehstate data to model position in gazebo
  int setVehstate2Alice();
    
  /********************/
  /* Public Variables */
  /********************/
  public:
  /// Program options
  gengetopt_args_info options;
  
  /// Gazebo Client
  gazebo::Client *gzClient;
  
  /// One of the Stereo Cameras
  vrStereoCamera *vrCam_MF_MEDIUM_STEREO;

  //Left Front Bumper SICK Ladar
  VRLadar *ladarLFBumper;

  //Middle Front Bumper SICK Ladar
  VRLadar *ladarMFBumper;

  //Right Front Bumper SICK Ladar
  VRLadar *ladarRFBumper;

  //Rear Bumper SICK Ladar
  VRLadar *ladarRearBumper;
 
  //Left Front Roof SICK Ladar
  VRLadar *ladarLFRoof;

 //Right Front Roof SICK Ladar
  VRLadar *ladarRFRoof;

  /// gz Server ID
  int gzServerID;
  
  /// Simulation Interface
  gazebo::SimulationIface *gzSimIface;
  
  /// Skynet Vehicle State
  VehicleState vehstate;
  
  // Console text display
  cotk_t *console;
  
  /// Skynet Key
  int snkey;
  
  /// Skynet Group
  SkynetGroup *sngroup;
  
  /// Skynet Server
  SkynetServer *skynet_server;
  
  /// Skynet Thread
  pthread_t skynet_thread;
  
  /// Skynet Vehicle State Mutex
  pthread_mutex_t vehstate_mutex;
  
};

#endif

