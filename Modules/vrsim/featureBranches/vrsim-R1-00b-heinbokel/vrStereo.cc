/* 
 * Desc: Stereo Camera Sensor Class for Gazebo Simulator vrsim
 * Date: 10 July 2008
 * Author: Bjorn Heinbokel
*/

#include "vrStereo.hh"

// This is OpenCV stuff
#include <//usr/include/opencv/cv.h>
#include <//usr/include/opencv/highgui.h>
#include "mcv.hh"

// Default constructor
vrStereoCamera::vrStereoCamera(int argc, char **argv, _sensnet_id_t sensorId, modulename moduleId)
{
  this->parseCmdLine(argc,argv, sensorId, moduleId);
  return;
}

// Default destructor
vrStereoCamera::~vrStereoCamera()
{  
  return;
}

// Parse the command line
int vrStereoCamera::parseCmdLine(int argc, char **argv, _sensnet_id_t sensorId, modulename moduleId)
{  
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
    
  // Fill out sensor id
  //this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  this->sensorId = sensorId;
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  //Fill out module Id
  this->moduleId = moduleId;

  // Fill out the default config path from the environment if not
  // given on the command line.
  /*if (!this->options.config_path_given)
      this->options.config_path_arg = dgcFindConfigDir("stereofeeder");*/
  
  // FOR DEBUG ONLY!!!    
  this->options.config_path_arg = "/home/users/heinbokel/surf08-hardware-heinbokel01/src/vrsim";
  
  // Load configuration file
  
  if (this->parseConfigFile(this->options.config_path_arg) != 0)
    return -1;
    MSG("Stereo Camera intialized and command line parsed");

  return 0;
}

// Parse the config file
int vrStereoCamera::parseConfigFile(const char *configPath)
{

  // Load options from the configuration file
  MSG("%s",configPath);
  /*
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, sensnet_id_to_name(this->sensorId));
  MSG("loading %s", filename);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);
  */

  // Fill out module id (may be in config file) DOES NOT EXIST YET!
  /*
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
*/

  // Parse transform
  float px, py, pz;
  float rx, ry, rz;
  //sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz);
  //sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz);

  // DEBUG!!! Write sensor pose directly drom CFG file
  px = 3.0311;
  py = -0.304300;
  pz = -1.7861;

  rx = +1.35;
  ry = -0.02;
  rz =  1.56;
  
  // Record pose (for display only)
  /*this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;*/

  // Compute sensor-to-vehicle transform
  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2veh);
  
  return 0;
}



// Initialize sensnet
int vrStereoCamera::initSensnet()
{
  // Check that blob size is a multiple of [something].  This allows
  // for DMA transfers.
  if (sizeof(StereoImageBlob) % 512 != 0)
    return ERROR("invalid blob size %d; needs padding of %d",
                 sizeof(StereoImageBlob), 512 - sizeof(StereoImageBlob) % 512);

  // Create page-aligned blob to enable DMA logging
  this->blob = (StereoImageBlob*) valloc(sizeof(StereoImageBlob));
  this->test_blob = (StereoImageBlob*) valloc(sizeof(StereoImageBlob));

  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    {
      cerr << "Something went wrong!!!";      
      return ERROR("unable to connect to sensnet");
    }
  MSG("SENSNET successfully initiated");
  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");
  
  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");
  



  // Initialize sensnet logging
  /*
  if (this->options.enable_log_flag)
  {
    time_t t;
    char timestamp[64];
    char cmd[256];
    sensnet_log_header_t header;

    // Construct log name
    t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
    snprintf(this->logName, sizeof(this->logName), "%s/%s-%s",
             this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));

    MSG("opening log %s", this->logName);
    
    
    // Initialize sensnet logging
    this->sensnet_log = sensnet_log_alloc();
    assert(this->sensnet_log);
    memset(&header, 0, sizeof(header));
    if (sensnet_log_open_write(this->sensnet_log, this->logName, &header, true) != 0)
      return ERROR("unable to open log: %s", this->logName);
    */
    
    /*
    // Copy configuration files
    snprintf(cmd, sizeof(cmd), "cp %s/%s-left.cahvor %s",
             this->options.config_path_arg, this->options.left_camera_arg, this->logName);
    system(cmd);
    snprintf(cmd, sizeof(cmd), "cp %s/%s-right.cahvor %s",
             this->options.config_path_arg, this->options.right_camera_arg, this->logName);
    system(cmd);
    snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
             this->options.config_path_arg, sensnet_id_to_name(this->sensorId), this->logName);
    system(cmd);
    
  }
 */
  return 0;
}


// Finalize sensnet
int vrStereoCamera::finiSensnet()
{  
  /*if (this->sensnet_log)
  {
    sensnet_log_close(this->sensnet_log);
    sensnet_log_free(this->sensnet_log);
    this->sensnet_log = NULL;
  }*/

  //sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
  //sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  free(this->blob);
  this->blob = NULL;
  
  return 0;
}


// Publish image data
int vrStereoCamera::writeSensnet(VehicleState vehstate)
{ 
  // Define the openCv images matrix structure variable to display in X11 and write to stereoBlob  
  CvMat cvStereoRawImLeft, cvDepthLeft;
  // Pose for veh2local and local2veh transformations
  pose3_t pose;
  // Image width and heigth (640x480 for SENSNET_MF_MEDIUM_STEREO)
  unsigned int width, height;
  
  // blob buffer varaible to reduce clutter
  StereoImageBlob *blob;
  // some variables the blob requires
  int imageSize(0), blobSize;

  // copy over the pointer
  blob = this->blob;

  // some information the blob needs (not in the order as in stereofeeder)
  blob->blobType = SENSNET_STEREO_IMAGE_BLOB;
  blob->version = STEREO_IMAGE_BLOB_VERSION;
  blob->sensorId = this->sensorId;
  blob->frameId = this->frameId;
  blob->timestamp = DGCgettime();
  
  // Compute left camera transform (and inverse)
  blob->leftCamera.sens2veh = this->sens2veh;
  mat44f_inv(blob->leftCamera.veh2sens, blob->leftCamera.sens2veh);


  // increment frame id
  this->frameId++;

  // reset blob information, set to 0
  memset(blob, 0, sizeof(*blob));  

  // get height and width information from stereo camera data structure
  width  = this->stereoCamIface.data->width;
  height = this->stereoCamIface.data->height;

  // Lock Camera Interface to prevent data corruption
  //this->stereoCamIface.Lock(1);
  // Copy left image data
  cvStereoRawImLeft = cvMat(height,width,CV_8UC3,&leftCamIface.data->image[0]);
  // Copy left disparity
  cvDepthLeft = cvMat(height,width,CV_32FC1, &this->stereoCamIface.data->left_depth[0]);  
  // Unlock stereo camera interface
  //this->stereoCamIface.Unlock();

  // Display the images using a openCv macro
  SHOW_IMAGE(&cvDepthLeft, "1. left depth map", 10); 
  SHOW_IMAGE(&cvStereoRawImLeft, "2. RGB camera image", 11);
  
  // Prepare and write left image to blob
  blob->leftOffset = imageSize;
  blob->leftSize = width*height*3;
  imageSize += width*height*3;
  memcpy(blob->imageData + blob->leftOffset, (uint8_t*)cvStereoRawImLeft.data.ptr, width*height*3);
  
  // Convert disparity data, write disparity parameters and write to blob
  // Find maximum float number of the opengl z-buffer (which is used as depth information)
  double max = 0;
  uint16_t temp[width*height];
  // Go through entire image array
  for (unsigned int i=0; i<height*width; i++)
  {
    double v = this->stereoCamIface.data->left_depth[i];
    if (v > max)
      max = v;
  }  
  // 
  for (unsigned int i = 0; i<height; i++)
  {
    for (unsigned int j =0; j<width; j++)
    {
      double v = this->stereoCamIface.data->left_depth[i*width+j];
      temp[i*width+j] = (uint16_t)((v/max) * 32767);
    }
  }
  MSG("Mas Disparity value: %f",max);
  

  blob->dispScale = 1; // TODO: What is the correct value for: JPLV_STEREO_DISP_SCALE;
  blob->dispOffset = imageSize;
  blob->dispSize = width*height;
  imageSize += width*height;
  memcpy(blob->imageData + blob->dispOffset, temp, width*height*2); // times 2 since the disp array is 2 byte per pixel

  // Compute the size of the blob that was actually used
  blobSize = sizeof(*blob) - sizeof(blob->imageData) + imageSize;

  // set some important blob variables
  blob->rows = 480;
  blob->cols = 640;
  blob->channels = 3;
  blob->state = vehstate;

  // Vehicle to local transform
  pose.pos = vec3_set(blob->state.localX,
                      blob->state.localY,
                      blob->state.localZ);
  pose.rot = quat_from_rpy(blob->state.localRoll,
                           blob->state.localPitch,
                           blob->state.localYaw);  
  pose3_to_mat44f(pose, blob->veh2loc);
  mat44f_inv(blob->loc2veh, blob->veh2loc);  

  // Reset reseved values
  memset(blob->reserved, 0, sizeof(blob->reserved));

  // Send blob, but over the shmem channel only
  if (sensnet_write(this->sensnet, SENSNET_METHOD_SHMEM, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                    this->frameId, blobSize, blob) != 0)
    return ERROR("unable to write blob");
  //MSG("Function done!");
  //MSG("Function done! Frame ID: %u", this->frameId);
  return 0;



  

  //pose3_t pose;
  /*
  float mf[4][4];
  double md[4][4];
  StereoImageBlob *blob;
  int imageSize, blobSize;

  blob = this->blob;

  blob->blobType = SENSNET_STEREO_IMAGE_BLOB;
  blob->version = STEREO_IMAGE_BLOB_VERSION;
  blob->sensorId = this->sensorId;
  blob->frameId = this->frameId;
  blob->timestamp = this->frameTime;

  // Compute left camera transform (and inverse)
  jplv_cmod_get_transform(&model, md);
  mat44f_setd(mf, md);
  mat44f_mul(blob->leftCamera.sens2veh, this->sens2veh, mf);
  mat44f_inv(blob->leftCamera.veh2sens, blob->leftCamera.sens2veh);

  // Get the rectified model and copy to blob
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_RIGHT, &model);    
  blob->rightCamera.cx = model.ext.cahv.hc;
  blob->rightCamera.cy = model.ext.cahv.vc;
  blob->rightCamera.sx = model.ext.cahv.hs;
  blob->rightCamera.sy = model.ext.cahv.vs;    

  // Compute right camera transform (and inverse)
  jplv_cmod_get_transform(&model, md);
  mat44f_setd(mf, md);
  mat44f_mul(blob->rightCamera.sens2veh, this->sens2veh, mf);
  mat44f_inv(blob->rightCamera.veh2sens, blob->rightCamera.sens2veh);
    
  // Camera baseline
  blob->baseline = jplv_stereo_get_baseline(this->stereo);
  assert(blob->baseline > 0);

  // Copy camera settings
  blob->leftCamera.gain = this->camera->gain[0];
  blob->leftCamera.shutter = this->camera->shutter[0];
  blob->rightCamera.gain = this->camera->gain[1];
  blob->rightCamera.shutter = this->camera->shutter[1];

  // Copy vehicle state
  blob->state = this->state;

  // Vehicle to local transform
  pose.pos = vec3_set(blob->state.localX,
                      blob->state.localY,
                      blob->state.localZ);
  pose.rot = quat_from_rpy(blob->state.localRoll,
                           blob->state.localPitch,
                           blob->state.localYaw);  
  pose3_to_mat44f(pose, blob->veh2loc);
  mat44f_inv(blob->loc2veh, blob->veh2loc);  

  // Reset reseved values
  memset(blob->reserved, 0, sizeof(blob->reserved));

  // Set image data
  jplv_stereo_get_dims(this->stereo, &blob->cols, &blob->rows, &blob->channels);

  // Keep track of total image data stored
  imageSize = 0;
  
  
  // Copy left rectified data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
  blob->leftOffset = imageSize;
  blob->leftSize = image->data_size;
  imageSize += image->data_size;  
  memcpy(blob->imageData + blob->leftOffset, image->data, image->data_size);
  
  // Copy right rectified data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_RIGHT);
  blob->rightOffset = imageSize;
  blob->rightSize = image->data_size;
  imageSize += image->data_size;
  memcpy(blob->imageData + blob->rightOffset, image->data, image->data_size);
  
  
  // Copy disparity data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);
  blob->dispScale = JPLV_STEREO_DISP_SCALE;
  blob->dispOffset = imageSize;
  blob->dispSize = image->data_size;
  imageSize += image->data_size;
  memcpy(blob->imageData + blob->dispOffset, image->data, image->data_size);

  // Compute the size of the blob that was actually used
  blobSize = sizeof(*blob) - sizeof(blob->imageData) + imageSize;
    
  // Send blob, but over the shmem channel only
  if (sensnet_write(this->sensnet, SENSNET_METHOD_SHMEM, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                    this->frameId, blobSize, blob) != 0)
    return ERROR("unable to write blob");

  // Write to log
  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag))
  {
    if (sensnet_log_write(this->sensnet_log, blob->timestamp,
                          this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                          this->frameId, blobSize, blob) != 0)
      return ERROR("unable to write blob");
  }
  
  // Keep some stats
  if (this->console && this->sensnet_log && (this->enableLog || this->options.always_log_flag))
  {
    this->logCount += 1;
    this->logSize += blobSize / 1024;
    cotk_printf(this->console, "%log%", A_NORMAL,
                "%df %dMb", this->logCount, this->logSize / 1024);
  }

  return 0;*/
}
