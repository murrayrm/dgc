/*!
 *  \file vrsim.cc
 *  \brief Gazebo (Virtual Reality) Client Program
 *
 *  \author Bjorn Heinbokel, Manuel Segura
 *  \date 2008
 *
 *  This file implements the main function of the gazebo client side functionality.
 */

#include <iostream>
#include <cstring>
#include <string>
#include <gazebo/gazebo.h>

#include "cmdline.h"


using namespace std;

int main (int argc, char **argv)
{
    int i = 0;
    long k = 0;
    char* temp = NULL;
    
    char getModelPose[32] = "get_pose";
    char setModelPose[32] = "set_pose3d";
    char aliceModelName[512] = "aliceBox_model";
    
    string strPosIface ("position_iface_0");
    string strAliceLaser ("laser_iface_0");
    string strSim ("default");
    
    double xx,yy,zz;
    
    /* Process command line options */
    gengetopt_args_info cmdline;
    if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

    /* Retrieve the skynet key */
//    int optSNKey = skynet_findkey(argc, argv);
//    cerr << "Constructing skynet with KEY = " << optSNKey << endl;

    // Instantiate Gazebo Objects
    gazebo::Client my_client;
    gazebo::PositionIface posAlice;
    gazebo::SimulationIface ALICE_Iface;
    gazebo::LaserIface aliceLaser;
    
    //Setup Server Connection
    my_client.clientId = 1;
    my_client.serverId = 5;
    
    // Connect to the libgazebo server
    //my_client.ConnectWait(5,1);
    my_client.Connect(5);
    
    // Open the Simulation Interface
    ALICE_Iface.Open(&my_client,strSim);
    
    // Open Laser Interface
    aliceLaser.Open(&my_client,strAliceLaser);
    
    /** Pausing simulation... (maybe not needed!!) **/
    //ALICE_Iface.Lock(1);
    //ALICE_Iface.data->pause = 1; 

    // Test cmdline
    if(cmdline.test_me_flag) { cout<< "test_me_flag was successfully set in command line" << '\n';};

    for(i=1;i<=5000;i++)
    {
      for(k=1;k<=100000;k++) {k=k;}
        
        // Setting command name for getting 3d position
        memcpy(ALICE_Iface.data->model_req,getModelPose,32);
        
        // Setting model name for getting alice's pose
        memcpy(ALICE_Iface.data->model_name, aliceModelName,512);
        
        xx = ALICE_Iface.data->model_pose.pos.x;
        yy = ALICE_Iface.data->model_pose.pos.y;
        zz = ALICE_Iface.data->model_pose.pos.z;
        
        // Setting command name for imposing 3d position
        memcpy(ALICE_Iface.data->model_req,setModelPose,32);  

        // Setting model name for inposing new Pose in Sim Iface
        memcpy(ALICE_Iface.data->model_name, aliceModelName,512);
      
        /** Unlock simulation interface ... **/
        //ALICE_Iface.Unlock(); 
        //ALICE_Iface.data->pause = 0; 
      
          ALICE_Iface.data->model_pose.pos.y = 0.001 * (double)i;
          ALICE_Iface.data->model_pose.pos.x = 0;
          ALICE_Iface.data->model_pose.pos.z = 1.6;
          
          ALICE_Iface.data->model_pose.yaw = 0*0.001* (double)i;
           
          /** Lock simulation interface ... **/
        //ALICE_Iface.Lock(1); 
        //ALICE_Iface.data->pause = 1; 
	    
	    temp = &ALICE_Iface.data->model_name[0];
	    cout << temp  << '\n';
	    
    cout << i << '\n';
  }

    my_client.Disconnect();
    return 0;
}

