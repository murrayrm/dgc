/* 
 * Desc: Stereo Camera Sensor Class for Gazebo Simulator vrsim
 * Date: 10 July 2008
 * Author: Bjorn Heinbokel
*/

#include "vrStereo.hh"

// This is OpenCV stuff
#include <//usr/include/opencv/cv.h>
#include <//usr/include/opencv/highgui.h>
#include "mcv.hh"

// Default constructor
vrStereoCamera::vrStereoCamera(int argc, char **argv, _sensnet_id_t sensorId, modulename moduleId)
{
  this->parseCmdLine(argc,argv, sensorId, moduleId);
  return;
}

// Default destructor
vrStereoCamera::~vrStereoCamera()
{  
  return;
}

// Parse the command line
int vrStereoCamera::parseCmdLine(int argc, char **argv, _sensnet_id_t sensorId, modulename moduleId)
{  
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
    
  // Fill out sensor id
  //this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  this->sensorId = sensorId;
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  //Fill out module Id
  this->moduleId = moduleId;

  // Fill out the default config path from the environment if not
  // given on the command line.
  //if (!this->options.config_path_given)
  //    this->options.config_path_arg = dgcFindConfigDir("stereofeeder");
  
  // FOR DEBUG ONLY!!!    
  this->options.config_path_arg = "/home/users/heinbokel/navigation-heinbokel03/src/vrsim";
  
  // Load configuration file
  if (this->parseConfigFile(this->options.config_path_arg) != 0)
    return -1;
  ERROR("Stereo Camera intialized and command line parsed");
  return 0;
}

// Parse the config file
int vrStereoCamera::parseConfigFile(const char *configPath)
{

  // Load options from the configuration file
  /*
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, sensnet_id_to_name(this->sensorId));
  MSG("loading %s", filename);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);
*/

  // Fill out module id (may be in config file) DOES NOT EXIST YET!
  /*
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
*/

  // Parse transform
  float px, py, pz;
  float rx, ry, rz;
  sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz);
  sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz);

  // Record pose (for display only)
  /*this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;*/

  // Compute sensor-to-vehicle transform
  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2veh);
  
  return 0;
}



// Initialize sensnet
int vrStereoCamera::initSensnet()
{
  // Check that blob size is a multiple of [something].  This allows
  // for DMA transfers.
  if (sizeof(StereoImageBlob) % 512 != 0)
    return ERROR("invalid blob size %d; needs padding of %d",
                 sizeof(StereoImageBlob), 512 - sizeof(StereoImageBlob) % 512);

  // Create page-aligned blob to enable DMA logging
  this->blob = (StereoImageBlob*) valloc(sizeof(StereoImageBlob));
 
  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return ERROR("unable to connect to sensnet");

 /* // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");
  
  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");*/

  // Initialize sensnet logging
  /*
  if (this->options.enable_log_flag)
  {
    time_t t;
    char timestamp[64];
    char cmd[256];
    sensnet_log_header_t header;

    // Construct log name
    t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
    snprintf(this->logName, sizeof(this->logName), "%s/%s-%s",
             this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));

    MSG("opening log %s", this->logName);
    
    
    // Initialize sensnet logging
    this->sensnet_log = sensnet_log_alloc();
    assert(this->sensnet_log);
    memset(&header, 0, sizeof(header));
    if (sensnet_log_open_write(this->sensnet_log, this->logName, &header, true) != 0)
      return ERROR("unable to open log: %s", this->logName);
    */
    
    /*
    // Copy configuration files
    snprintf(cmd, sizeof(cmd), "cp %s/%s-left.cahvor %s",
             this->options.config_path_arg, this->options.left_camera_arg, this->logName);
    system(cmd);
    snprintf(cmd, sizeof(cmd), "cp %s/%s-right.cahvor %s",
             this->options.config_path_arg, this->options.right_camera_arg, this->logName);
    system(cmd);
    snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
             this->options.config_path_arg, sensnet_id_to_name(this->sensorId), this->logName);
    system(cmd);
    
  }
 */
  return 0;
}


// Finalize sensnet
int vrStereoCamera::finiSensnet()
{  
  /*if (this->sensnet_log)
  {
    sensnet_log_close(this->sensnet_log);
    sensnet_log_free(this->sensnet_log);
    this->sensnet_log = NULL;
  }*/

  //sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
  //sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  free(this->blob);
  this->blob = NULL;
  
  return 0;
}


// Publish image data
int vrStereoCamera::writeSensnet()
{
  CvMat rawim;  
  CvMat stRawimLeft, stRawimRight, depth_left, depth_right, depth_diff;
  
  unsigned int width, height;
  
  StereoImageBlob *blob;
  int imageSize(0), blobSize;

  //this->frameId++;
  blob = this->blob;
  
  // Lock Camera Interface to prevent data corruption
  //this->camIface.Lock(1);
  this->stereoCamIface.Lock(1);
  
  // Swap the R and B channel
  /* unsigned char temp[640*480*3];
  unsigned char temp3;
  for (unsigned int i=0; i < (width*height); i++)
  {
    MSG("%u",i);
    temp3 = this->leftCamIface.data->image[3*i];
    temp[3*i] = this->leftCamIface.data->image[3*i+2];
    temp[3*i+1] = this->leftCamIface.data->image[3*i+1];
    temp[3*i+2] = temp3;
  }*/
  // Copy left image data
  rawim = cvMat(480,640,CV_8UC3,&leftCamIface.data->image[0]);
  
  width  = this->stereoCamIface.data->width;
  height = this->stereoCamIface.data->height;
  
  MSG("Width: %u, Height: %u, left RGB size: %u, right RGB size: %u", width, height, this->stereoCamIface.data->left_rgb_size, this->stereoCamIface.data->right_rgb_size);
  
  stRawimLeft = cvMat(height,width,CV_8UC3,&this->stereoCamIface.data->left_rgb[0]);   
  
  float temp2[640*480];
  for (unsigned int i=0; i < width*height; i++)
  {
    temp2[i] = fabs(this->stereoCamIface.data->left_depth[i] - this->stereoCamIface.data->right_depth[i]);
  }
  
  depth_left = cvMat(height,width,CV_32FC1, &this->stereoCamIface.data->left_depth[0]); 
  depth_right = cvMat(height,width,CV_32FC1,&this->stereoCamIface.data->right_depth[0]); 
  depth_diff = cvMat(height,width,CV_32FC1,&temp2[0]); 

  SHOW_IMAGE(&depth_left, "1. left depth map", 10);
  SHOW_IMAGE(&depth_right, "2. right depth map", 11);
  SHOW_IMAGE(&depth_diff, "3. difference between l/r depth map", 12);
  SHOW_IMAGE(&rawim, "2. RGB camera image", 13);
  
  
  blob->leftOffset = imageSize;
  blob->leftSize = 640*480*3;
  imageSize += 640*480*3;
  memcpy(blob->imageData + blob->leftOffset, (uint8_t*)stRawimLeft.data.ptr, 640*480*3);

/*
  // Copy right rectified data (RIGHT NOW THIS IS THE LEFT IMAGE!!!)
  rawim = cvMat(480,640,CV_8UC3,&this->camIface.data->image[0]);
  
  blob->rightOffset = imageSize;
  blob->rightSize = 640*480*3;
  imageSize += 640*480*3;  
  memcpy(blob->imageData + blob->rightOffset, rawim->data, image->data_size);
*/

  //this->camIface.Unlock();
  this->stereoCamIface.Unlock();
  // Compute the size of the blob that was actually used
  blobSize = sizeof(*blob) - sizeof(blob->imageData) + imageSize;
    
  // Send blob, but over the shmem channel only
  if (sensnet_write(this->sensnet, SENSNET_METHOD_SHMEM, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                    this->frameId, blobSize, blob) != 0)
    return ERROR("unable to write blob");

  //pose3_t pose;
  /*
  float mf[4][4];
  double md[4][4];
  StereoImageBlob *blob;
  int imageSize, blobSize;

  blob = this->blob;

  blob->blobType = SENSNET_STEREO_IMAGE_BLOB;
  blob->version = STEREO_IMAGE_BLOB_VERSION;
  blob->sensorId = this->sensorId;
  blob->frameId = this->frameId;
  blob->timestamp = this->frameTime;

  // Compute left camera transform (and inverse)
  jplv_cmod_get_transform(&model, md);
  mat44f_setd(mf, md);
  mat44f_mul(blob->leftCamera.sens2veh, this->sens2veh, mf);
  mat44f_inv(blob->leftCamera.veh2sens, blob->leftCamera.sens2veh);

  // Get the rectified model and copy to blob
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_RIGHT, &model);    
  blob->rightCamera.cx = model.ext.cahv.hc;
  blob->rightCamera.cy = model.ext.cahv.vc;
  blob->rightCamera.sx = model.ext.cahv.hs;
  blob->rightCamera.sy = model.ext.cahv.vs;    

  // Compute right camera transform (and inverse)
  jplv_cmod_get_transform(&model, md);
  mat44f_setd(mf, md);
  mat44f_mul(blob->rightCamera.sens2veh, this->sens2veh, mf);
  mat44f_inv(blob->rightCamera.veh2sens, blob->rightCamera.sens2veh);
    
  // Camera baseline
  blob->baseline = jplv_stereo_get_baseline(this->stereo);
  assert(blob->baseline > 0);

  // Copy camera settings
  blob->leftCamera.gain = this->camera->gain[0];
  blob->leftCamera.shutter = this->camera->shutter[0];
  blob->rightCamera.gain = this->camera->gain[1];
  blob->rightCamera.shutter = this->camera->shutter[1];

  // Copy vehicle state
  blob->state = this->state;

  // Vehicle to local transform
  pose.pos = vec3_set(blob->state.localX,
                      blob->state.localY,
                      blob->state.localZ);
  pose.rot = quat_from_rpy(blob->state.localRoll,
                           blob->state.localPitch,
                           blob->state.localYaw);  
  pose3_to_mat44f(pose, blob->veh2loc);
  mat44f_inv(blob->loc2veh, blob->veh2loc);  

  // Reset reseved values
  memset(blob->reserved, 0, sizeof(blob->reserved));

  // Set image data
  jplv_stereo_get_dims(this->stereo, &blob->cols, &blob->rows, &blob->channels);

  // Keep track of total image data stored
  imageSize = 0;
  
  
  // Copy left rectified data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
  blob->leftOffset = imageSize;
  blob->leftSize = image->data_size;
  imageSize += image->data_size;  
  memcpy(blob->imageData + blob->leftOffset, image->data, image->data_size);
  
  // Copy right rectified data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_RIGHT);
  blob->rightOffset = imageSize;
  blob->rightSize = image->data_size;
  imageSize += image->data_size;
  memcpy(blob->imageData + blob->rightOffset, image->data, image->data_size);
  
  
  // Copy disparity data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);
  blob->dispScale = JPLV_STEREO_DISP_SCALE;
  blob->dispOffset = imageSize;
  blob->dispSize = image->data_size;
  imageSize += image->data_size;
  memcpy(blob->imageData + blob->dispOffset, image->data, image->data_size);

  // Compute the size of the blob that was actually used
  blobSize = sizeof(*blob) - sizeof(blob->imageData) + imageSize;
    
  // Send blob, but over the shmem channel only
  if (sensnet_write(this->sensnet, SENSNET_METHOD_SHMEM, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                    this->frameId, blobSize, blob) != 0)
    return ERROR("unable to write blob");

  // Write to log
  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag))
  {
    if (sensnet_log_write(this->sensnet_log, blob->timestamp,
                          this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                          this->frameId, blobSize, blob) != 0)
      return ERROR("unable to write blob");
  }
  
  // Keep some stats
  if (this->console && this->sensnet_log && (this->enableLog || this->options.always_log_flag))
  {
    this->logCount += 1;
    this->logSize += blobSize / 1024;
    cotk_printf(this->console, "%log%", A_NORMAL,
                "%df %dMb", this->logCount, this->logSize / 1024);
  }

  return 0;*/
}
