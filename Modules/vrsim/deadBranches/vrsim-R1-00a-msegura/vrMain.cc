/*!
 *  \file vrMain.cc
 *  \brief Main Thread for Gazebo VR simulator
 *
 *  \author Bjorn Heinbokel, Manuel Segura
 *  \date 2008
 *
 *  This file implements the main function of the gazebo client side functionality.
 */

#include <math.h>

#include "vrStereo.hh"
#include "vrLadar.hh"
#include "vrSim.hh"

using namespace std;

int main (int argc, char **argv)
{
  uint64_t lastTime;
  long cnt = 0;
  
  // Create vr simulation object
  vrSim *myVrSim = new vrSim();
  assert(myVrSim);

  // Parse command line options
  if (myVrSim->parseCmdLine(argc, argv) != 0)
    return -1;
  
  if (myVrSim->initConsole() != 0)
    return -1;

  if (myVrSim->initGzClient() != 0)
    return -1;

  if (myVrSim->initGzSimIface() != 0)
    return -1;


//Put specific calls into one initLadar Function
  myVrSim->initLadar(argc, argv);

  
  if (myVrSim->initSkynet() != 0)
    return -1;

  if (myVrSim->initStereo(argc,argv) !=0)
    return -1;

/*
  // Connect to Skynet
  int snkey = skynet_findkey(argc, argv);
  cout << "The Skynet Key is : " << snkey << '\n';
  SkynetServer skynet_server = SkynetServer(snkey, 150);

  //Start a thread to listen to messages
  pthread_t skynet_thread;
  skynet_thread = skynet_server.start();
  VehicleState vehstate;  

  pthread_mutex_t vehstate_mutex;
  DGCcreateMutex(&vehstate_mutex);
  SkynetGroup sngroup = SkynetGroup(skynet_server.getServer(), SNstate, (char *) &vehstate, 
                          sizeof(VehicleState), NULL, &vehstate_mutex, NULL, NULL);
*/


  /* Main Loop */
  while(!myVrSim->quit)
  {
    // Update the console
    if (myVrSim->console)
      cotk_update(myVrSim->console);

/*
    pthread_mutex_lock(&vehstate_mutex);
    myVrSim->gzSetModelPos("aliceBox_model",vehstate.siteNorthing,vehstate.siteEasting,1.5,0,0,vehstate.siteYaw);
    pthread_mutex_unlock(&vehstate_mutex);*/
  
  cnt++;
     // int status = myVrSim->gzSetModelPos("theBox", 5*cos(0.01*6.28*(double)cnt), 
      //                5*sin(0.01*6.28*(double)cnt), 1.5, 0.0, 0.0, 0.0);

 
    //myVrSim->setVehstate2Alice();

    //Trying to simulate 75Hz
    //usleep (13000);

   
    // If paused, or if we are running too fast, give up our time
    // slice.
    if ((DGCgettime() - lastTime > (uint64_t) 1e6)) // 7.5 Hz = 1/133,333 us
    {
      lastTime = DGCgettime();
      cnt++;
      myVrSim->processStereo();
    }
;


    //Ladars write to sensnet here 
    myVrSim->processLadars();
  }
  
  // Clean up TODO : write finiSkynet() function
  myVrSim->finiConsole();
  myVrSim->finiStereo();
  delete myVrSim;
  
  return 0;
}

