/*
 *  \file vrLadar.hh
 *  \brief Used to send ladar data to sensnet
 *  
 *  \author Manuel Segura
 *  \date 2008
 *   
 */
#ifndef VR_LADAR
#define VR_LADAR

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include <alice/AliceConstants.h>
#include <gazebo/gazebo.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <cmdline.h>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_log.h>
#include <skynet/sn_msg.hh>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <interfaces/LadarRangeBlob.h>
#include <cotk/cotk.h>
#include <interfaces/PTUStateBlob.h>

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

#define uchar unsigned char

using namespace std;

//Note to self: 
//Need to eventually implement stats for verification
class VRLadar
{
  public:

  VRLadar(modulename moduleId, sensnet_id_t sensorId);

  ~VRLadar();

  int parseCmdLine(int argc, char **argv);

  int parseConfigFile(const char *configPath);

  int initSensnet(const char *configPath);

  int finiSensnet();

  int writeSensnet();

  int captureScan();

  int getState(uint64_t timestamp);

  public:
  gazebo::LaserIface *ladarIface;

  public:
  
  // Program options
  gengetopt_args_info options;

  // Default configuration path
  char *defaultConfigPath;

  //Spread settings
  char *spreadDaemon;
  int skynetKey;

  //Our module id (Skynet)
  modulename moduleId;

  //Our sensor id (SensNet)
  sensnet_id_t sensorId;

  //Id of the ladar
  int ladarId;

  // Sensor-to-vehicle transform
  float sens2veh[4][4];

  // Sensor-to-tool frame transform for PTU ladar
  float sens2tool[4][4];

  //SensNet handle
  sensnet_t *sensnet;

  //SensNet log handle
  sensnet_log_t *sensnet_log;

  //Blob buffer
  LadarRangeBlob *blob;

  //PTU blob
  PTUStateBlob ptublob;

  //Current vehicle state data
  VehicleState state;

  //Current scan id
  int scanId;

  //Current scan time (microseconds)
  uint64_t scanTime;

  //Point data (bearing, range, intensity)
  int numPoints;
  float points[361][3];

  //Start time for computing stats
  uint64_t startTime;

};

#endif
