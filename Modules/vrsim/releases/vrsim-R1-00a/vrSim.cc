/*!
 *  \file vrsim.cc
 *  \brief VR Simulator Class 
 *
 *  \author Bjorn Heinbokel
 *  \date 2008
 *
 *  This class implements the main functionality of a gazebo client interface.
 */

#include "vrSim.hh"
#include "vrStereo.hh"
#include "vrLadar.hh"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
vrSim::vrSim()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
vrSim::~vrSim()
{  
  return;
}

// Parse the command line
int vrSim::parseCmdLine(int argc, char **argv)
{  
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
    
  this->gzServerID = this->options.gzServerID_arg;
  
/*
  // Use raw image logging or sensnet logging, but not both
  if (this->options.enable_log_flag && this->options.enable_imagelog_flag)
    return ERROR("sensnet logging and image logging cannot be enabled at the same time");
  
  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
    
  // Fill out sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // Fill out the default config path from the environment if not
  // given on the command line.
  if (!this->options.config_path_given)
    this->options.config_path_arg = dgcFindConfigDir("stereofeeder");
  
  // Load configuration file
  if (this->parseConfigFile(this->options.config_path_arg) != 0)
    return -1;
*/
  return 0;
}

// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"vrSim                                                                      \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"[%QUIT%|%PAUSE%]                                                           \n";
 
// Initialize console display
int vrSim::initConsole()
{
  char filename[1024];
  
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);

  // Initialize the display
  snprintf(filename, sizeof(filename), "test.msg");
  if (cotk_open(this->console, filename) != 0)
    return -1;

  // Display some fixed values
  //cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
  //            "Test Deamon", 523, "Module Name");
                    
  return 0;
}

// Finalize console display
int vrSim::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }  
  return 0;
}

int vrSim::initGzClient()
{
  gzClient = new gazebo::Client();
  gzClient->ConnectWait(this->gzServerID,GZ_CLIENT_ID_USER_FIRST); //connect Client in Wait mode (blocking) 
  //DO WE NEED BLOCKING MODE (BEH)???
  
  return 0;
}

int vrSim::initGzSimIface()
{
  this->gzSimIface = new gazebo::SimulationIface(); // Allocate memory for new object
    
  this->gzSimIface->Open(gzClient,"default");; //connect Simulation Interface
  return 0;
}


int vrSim::initStereo(int argc, char **argv)
{
  // Setup the Stereo Camera Sensors
  StereoCam1 = new vrStereoCamera();
  
  // Open Interface to gazebo
  StereoCam1->camIface.Open(gzClient, "cam_iface_0");
  
  return 0;
}

//TODO: Fix problem with ladarIface (causes program to crash)
//Connects to sensnet properly though
int vrSim::initLadar(int argc, char **argv, modulename moduleId, sensnet_id_t sensorId)
{
   switch(moduleId){
      case 62:
        ladarLFBumper = new VRLadar(moduleId,sensorId);
	ladarLFBumper->parseCmdLine(argc, argv);
	ladarLFBumper->initSensnet(ladarLFBumper->defaultConfigPath);
        ladarLFBumper->ladarIface->Open(this->gzClient, "ladarLFBumperLaserIface");
        break;
      case 64:
        ladarMFBumper = new VRLadar(moduleId,sensorId);
	ladarMFBumper->parseCmdLine(argc, argv);
	ladarMFBumper->initSensnet(ladarMFBumper->defaultConfigPath);
        ladarMFBumper->ladarIface->Open(this->gzClient, "ladarMFBumperLaserIface");
        break;
      case 63:
        ladarRFBumper = new VRLadar(moduleId,sensorId);
	ladarRFBumper->parseCmdLine(argc, argv);
	ladarRFBumper->initSensnet(ladarRFBumper->defaultConfigPath);
        ladarRFBumper->ladarIface->Open(this->gzClient, "ladarRFBumperLaserIface");
        break;
      case 72:
        ladarRearBumper = new VRLadar(moduleId,sensorId);
	ladarRearBumper->parseCmdLine(argc, argv);
	ladarRearBumper->initSensnet(ladarRearBumper->defaultConfigPath);
        ladarRearBumper->ladarIface->Open(this->gzClient, "ladarRearBumperLaserIface");
        break; 
      default:
        cout << "moduleId error\n";
        break;     
   }
   return 0;
}

// Handle button callbacks
int vrSim::onUserQuit(cotk_t *console, vrSim *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}

// Handle button callbacks
int vrSim::onUserPause(cotk_t *console, vrSim *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

int vrSim::gzSetModelPos(string strModelName, double x, double y, double z, 
                                              double roll, double pitch, double yaw)
{
  char setModelPose[32] = "set_pose3d";
  char temp[512] ;
  
  // Setting command name for imposing 3d position
  memcpy(this->gzSimIface->data->model_req,setModelPose,32);
  
  // Setting model name for inposing new Pose in Sim Iface
  int i = (int)strModelName.length();
  for(int k=0;k<i;k++)
  {
    this->gzSimIface->data->model_name[k] = strModelName[k];
  };
  
  this->gzSimIface->Lock(1);
  
  this->gzSimIface->data->model_pose.pos.x = x;
  this->gzSimIface->data->model_pose.pos.y = -y;
  this->gzSimIface->data->model_pose.pos.z = z;
  this->gzSimIface->data->model_pose.roll  = roll;
  this->gzSimIface->data->model_pose.pitch = pitch;
  this->gzSimIface->data->model_pose.yaw   = yaw;
  
  this->gzSimIface->Unlock();

  return 0;
}

/*
int vrSim::gzSetModelPos(string strModelName, VehicleState vehState)
{
  char setModelPose[32] = "set_pose3d";

  // Setting command name for imposing 3d position
  memcpy(this->gzSimIface->data->model_req,setModelPose,32);
  
  // Setting model name for inposing new Pose in Sim Iface
  memcpy(ALICE_Iface->data->model_name, strModelName,strModelName.size);
  
  this->gzSimIface->Lock(1);
  
  this->gzSimIface->data->model_pose.pos.x = vehstate.siteEasting;
  this->gzSimIface->data->model_pose.pos.y = vehstate.siteNorthing;
  this->gzSimIface->data->model_pose.pos.z = 0.5;
  this->gzSimIface->data->model_pose.roll  = 0;
  this->gzSimIface->data->model_pose.pitch = 0;
  this->gzSimIface->data->model_pose.yaw   = vehstate.yaw;
  
  this->gzSimIface->Unlock();
}
*/

/*   This is Manuels old Skynet connection code:
        //sngroup.getMessage((char *) &vehstate, sizeof(VehicleState));
        pthread_mutex_lock(&vehstate_mutex);
        ALICE_Iface->data->model_pose.pos.y = vehstate.siteEasting;
        ALICE_Iface->data->model_pose.pos.x = vehstate.siteNorthing;
        
        ALICE_Iface->data->model_pose.yaw = vehstate.siteYaw;
        
        pthread_mutex_unlock(&vehstate_mutex);   
*/









