#include <iostream>
#include <math.h>
#include "dgcutils/ggis.h"

using namespace std;

#define PI 3.14159265
#define LANE_WIDTH 2.286

int main(int argc, char **argv) 
{ 
  int cornerNum = 0;

  while (cornerNum < 1 || cornerNum > 3) {
    cout << "corner: ";
    cin >> cornerNum;
    if (cornerNum < 1 || cornerNum > 3)
      cerr << "The number needs to be between 1 and 3" << endl;
  }

  double dist;
  if (cornerNum == 1) {
    dist = 16; // CHANGE THIS
  }
  else if (cornerNum == 2) {
    dist = 15; // CHANGE THIS
  }
  else {
    dist = 18; // CHANGE THIS
  }

  GisCoordLatLon latlon;
  GisCoordUTM utm;
  utm.zone = 11;
  utm.letter = 'S';
  double northing11, easting11, northing12, easting12, northing31, easting31, northing32, easting32;
  double lat11, lon11, lat12, lon12, lat2, lon2, lat31, lon31, lat32, lon32;
  double lbn1, lbe1, lbn2, lbe2, lbn3, lbe3, lbn4, lbe4, lbn5, lbe5;
  double n1, e1, n2, e2, lat, lon;
  double n41, e41, n42, e42, n51, e51, n52, e52;

  if (cornerNum == 1) {
    lat = 34.142986; // CHANGE THIS
    lon = -118.042333; // CHANGE THIS 
  }
  else if (cornerNum == 2) {
    lat = 34.143644; // CHANGE THIS
    lon = -118.042586; // CHANGE THIS
  }
  else {
    lat = 34.143836; // CHANGE THIS
    lon = -118.041786; // CHANGE THIS
  }
  latlon.latitude = lat;
  latlon.longitude = lon;
  gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
  n1 = utm.n;
  e1 = utm.e;

  if (cornerNum == 1) {
    lat = 34.143047; // CHANGE THIS
    lon = -118.042538; // CHANGE THIS
  }
  else if (cornerNum == 2) {
    lat = 34.143808; // CHANGE THIS
    lon = -118.042508; // CHANGE THIS
  }
  else {
    lat = 34.143772; // CHANGE THIS
    lon = -118.041586; // CHANGE THIS
  }
  latlon.latitude = lat;
  latlon.longitude = lon;
  gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
  n2 = utm.n;
  e2 = utm.e;

  if (cornerNum == 1) { 
  lat = 34.143361; // CHANGE THIS
  lon = -118.042716; // CHANGE THIS
  }
  else if (cornerNum == 2) {
    lat = 34.143947; // CHANGE THIS
    lon = -118.042138; // CHANGE THIS
  }
  else {
    lat = 34.143450; // CHANGE THIS
    lon = -118.041391; // CHANGE THIS
  }
  latlon.latitude = lat;
  latlon.longitude = lon;
  gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
  n41 = utm.n;
  e41 = utm.e;

  if (cornerNum == 1) {
    lat = 34.143375; // CHANGE THIS
    lon = -118.042763; // CHANGE THIS 
  }
  else if (cornerNum == 2) {
    lat = 34.143983; // CHANGE THIS
    lon = -118.042125; // CHANGE THIS
  }
  else {
    lat = 34.143436; // CHANGE THIS
    lon = -118.041347; // CHANGE THIS 
  }
  latlon.latitude = lat;
  latlon.longitude = lon;
  gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
  n42 = utm.n;
  e42 = utm.e;

  lbn4 = (n41 + n42)/2;
  lbe4 = (e41 + e42)/2;
 
  if (cornerNum == 1) {
    lat = 34.143513; // CHANGE THIS
    lon = -118.042644; // CHANGE THIS
  }
  else if (cornerNum == 2) {
    lat = 34.143883; // CHANGE THIS
    lon = -118.041938; // CHANGE THIS
  }
  else {
    lat = 34.143280; // CHANGE THIS
    lon = -118.041469; // CHANGE THIS
  }
  latlon.latitude = lat;
  latlon.longitude = lon;
  gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
  n51 = utm.n;
  e51 = utm.e;

  if (cornerNum == 1) {
    lat = 34.143530; // CHANGE THIS
    lon = -118.042691; // CHANGE THIS
  }
  else if (cornerNum == 2) {
    lat = 34.143922; // CHANGE THIS
    lon = -118.041919; // CHANGE THIS
  }
  else {
    lat = 34.143263; // CHANGE THIS
    lon = -118.041419; // CHANGE THIS
  }
  latlon.latitude = lat;
  latlon.longitude = lon;
  gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
  n52 = utm.n;
  e52 = utm.e;

  lbn5 = (n51 + n52)/2;
  lbe5 = (e51 + e52)/2;

  double thetaDes = atan2(lbn5 - lbn4, lbe5 - lbe4);

  double ang = atan2(n2 - n1, e2 - e1);

  cout << "angle = " << fabs(thetaDes - ang) * 180/PI << endl;

  northing11 = n2 + dist*sin(ang);
  easting11 = e2 + dist*cos(ang);

  utm.n = northing11;
  utm.e = easting11;
  gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
  lat11 = latlon.latitude;
  lon11 = latlon.longitude;
  cout << "Waypoint: ";
  if (cornerNum == 1)
    cout << "2.2.10: ";
  else if (cornerNum == 2)
    cout << "2.2.16: ";
  else
    cout << "2.2.22: ";
  printf( "lat = %9f\t", lat11 );
  printf( "lon = %9f\n", lon11 );

  ang = ang - 3*PI/2;
  lbn1 = northing11 + LANE_WIDTH*sin(ang);
  lbe1 = easting11 + LANE_WIDTH*cos(ang);

  northing12 = northing11 + 2*LANE_WIDTH*sin(ang);
  easting12 = easting11 + 2*LANE_WIDTH*cos(ang);
  
  utm.n = northing12;
  utm.e = easting12;
  gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
  lat12 = latlon.latitude;
  lon12 = latlon.longitude;  
  cout << "Waypoint: ";
  if (cornerNum == 1)
    cout << "2.1.19: ";
  else if (cornerNum == 2)
    cout << "2.1.13: ";
  else
    cout << "2.1.7: ";
  printf( "lat = %9f\t", lat12 );
  printf( "lon = %9f\n", lon12 );

  double alpha = acos(9.5/11);
  double theta;
  double thetaTmp;
  double thetaMax;
  if (cornerNum == 1) {
    theta = 0;
    thetaTmp = 0;
    thetaMax = PI;
  }
  else if (cornerNum == 2) {
    theta = 0;
    thetaTmp = 0;
    thetaMax = PI/2;
  }
  else {
    theta = -PI/2;
    thetaTmp = -PI/2;
    thetaMax = 0;
  }
      
  double thetaInc = 0.001;
  double smallestAngDiff = 2*PI;  

  while (thetaTmp < thetaMax) {
    lbn3 = lbn1 + 19*sin(thetaTmp);
    lbe3 = lbe1 + 19*cos(thetaTmp);
    double angDiff = fabs(atan2(lbn4 - lbn3, lbe4 - lbe3) - thetaDes);
    if (angDiff < smallestAngDiff) {
      smallestAngDiff = angDiff;
      theta = thetaTmp;
    }      
    thetaTmp += thetaInc;
  }
  lbn3 = lbn1 + 19*sin(theta);
  lbe3 = lbe1 + 19*cos(theta);
  lbn2 = lbn1 + 11*sin(theta + alpha);
  lbe2 = lbe1 + 11*cos(theta + alpha);
  utm.n = lbn2;
  utm.e = lbe2;
  gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
  lat2 = latlon.latitude;
  lon2 = latlon.longitude;

  cout << endl;
  cout << "theta = " << theta * 180/PI << " angDiff = " << smallestAngDiff * 180/PI << endl;
  cout << "Apex point: ";
  printf("%9f\t%9f\n\n", lat2, lon2);

  ang = atan2(lbn4 - lbn3, lbe4 - lbe3);

  northing31 = lbn3 + LANE_WIDTH*sin(ang - PI/2);
  easting31 = lbe3 + LANE_WIDTH*cos(ang - PI/2);
  northing32 = lbn3 + LANE_WIDTH*sin(ang + PI/2);
  easting32 = lbe3 + LANE_WIDTH*cos(ang + PI/2);
  
  utm.n = northing31;
  utm.e = easting31;
  gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
  lat31 = latlon.latitude;
  lon31 = latlon.longitude;
  cout << "Waypoint: ";
  if (cornerNum == 1)
    cout << "2.2.11: ";
  else if (cornerNum == 2)
    cout << "2.2.17: ";
  else
    cout << "2.2.23: ";
  printf( "lat = %9f\t", lat31 );
  printf( "lon = %9f\n", lon31 );
  
  utm.n = northing32;
  utm.e = easting32;
  gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
  lat32 = latlon.latitude;
  lon32 = latlon.longitude;
  cout << "Waypoint: ";
  if (cornerNum == 1)
    cout << "2.1.18: ";
  else if (cornerNum == 2)
    cout << "2.1.12: ";
  else
    cout << "2.1.6: ";
  printf( "lat = %9f\t", lat32 );
  printf( "lon = %9f\n", lon32 );

  cout << "distance = " 
       << sqrt(pow(northing11 - n2,2) + pow(easting11 - e2, 2)) << ", "
       << sqrt(pow(northing11 - northing31,2) + pow(easting11 - easting31, 2)) << ", "
       << sqrt(pow(n41 - northing31,2) + pow(e41 - easting31, 2)) << endl;

}
