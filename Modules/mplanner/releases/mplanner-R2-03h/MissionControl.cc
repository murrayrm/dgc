/*!**
 * Nok Wongpiromsarn
 * February 21, 2007
 */

#include <assert.h>
#include <sys/time.h>
#include <signal.h>
#include <dgcutils/GlobalConstants.h>
#include <interfaces/StatePrecision.hh>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>
#include "MissionControl.hh"
#include "mcontrolDisplay.h"
#include "CmdArgs.hh"

#define VEHICLE_CAP_CHANGE 0.2
#define TIME_BETWEEN_PAUSE 3000000.0

using namespace std;

volatile sig_atomic_t quit = 0;

MissionControl::MissionControl(int skynetKey) :
  GcModule( "MissionControl", &m_contrStatus, &m_mergedDirective ),
  astateHealthTalker(skynetKey, SNastateHealth, MODmissionplanner), 
  healthTalker(skynetKey, SNvehicleCapability, MODmissionplanner)
{
  m_snKey = skynetKey;

  DGCcreateMutex(&m_contrDirectiveQMutex);
  m_nosparrow = true;
  m_logData = false;
  m_logFile = NULL;
  m_missionFile = NULL;
  m_rndf = NULL;
  m_RNDFFileName = NULL;
  m_MDFFileName = NULL;
  m_nextCheckpointIndex = 0;
  m_lastAddedCheckpointIndex = 0;
  m_lastPauseID = 0;
  m_lastCompletedPauseID = 0;
  m_missionCompleted = false;
  m_eomIssued = false;
  m_mergedDirective.id = 0;
  m_response.goalID = 0;
  m_missionCompleted = false;
  m_restart = false;
  m_reset = false;
  m_isInit = false;
  m_currentMergedDirectiveID = 0;
  m_lastGoalID = 0;
  mrInterfaceSF = MRInterface::generateSouthface(skynetKey, this);
  mrInterfaceSF->setStaleThreshold(20);
  astateHealthTalker.listen();
  if (CmdArgs::getSystemHealth)
    healthTalker.listen();

  m_apxCount = 0;
  m_apxStatus = (int)NOT_CONNECTED;
  APXTIMEOUT = 500;

  m_healthCount = 0;

  m_lastPauseT = 0;

  // Initialize sparrow variables
  sparrowResponseGoalID = 0;
  for (int i=0; i < NUM_GOALS_DISPLAYED; i++)
  {
    sparrowNextGoalIDs[i] = 0;
    sparrowNextSegments[i] = 0;
    sparrowNextLanes[i] = 0;
    sparrowNextWaypoints[i] = 0;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MissionControl::~MissionControl() 
{
  // m_rndf is deleted by the RoutePlanner
  // m_logFile is closed by MissionPlannerMain
  // m_shp is deleted by MissionPlannerMain
  DGCdeleteMutex(&m_contrDirectiveQMutex);
  MRInterface::releaseSouthface(mrInterfaceSF);
  sensnet_leave(m_sensnet, SENSNET_SKYNET_SENSOR, SNprocessRequest);
  sensnet_disconnect(m_sensnet);
  sensnet_free(m_sensnet);
  m_sensnet = NULL;
  fclose(m_missionFile);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::init(CSparrowHawk* shp, pthread_mutex_t* shpMutex)
{
  m_shp = shp;
  m_shpMutex = shpMutex;
  m_rndf = CmdArgs::rndf;
  m_RNDFFileName = CmdArgs::RNDFFileName;
  m_MDFFileName = CmdArgs::MDFFileName;
  m_nosparrow = CmdArgs::nosparrow;
  m_logData = CmdArgs::logData;
  m_logFile = CmdArgs::logFile;
  m_getApxStatus = CmdArgs::getApxStatus;
  DEBUG_LEVEL = CmdArgs::debugLevel;

  if (CmdArgs::verbose) {
    VERBOSITY_LEVEL = 1;
  }

  if (CmdArgs::logLevel > 0) {
    stringstream ssfilename("");
    stringstream ssMDF;
    ssMDF << "./" << m_MDFFileName;
    string tmpMDFname;
    string MDFFileNameStr = ssMDF.str();
    tmpMDFname.assign(MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("/")+1, 
		      MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("."));
    ssfilename << CmdArgs::logPath  << "/mplanner-missioncontrol-" << tmpMDFname;
    this->addLogfile(ssfilename.str());
    this->setLogLevel(CmdArgs::logLevel);
  }

  m_isInit = false;
  m_restart = false;
  m_reset = false;
  m_missionCompleted = false;
  m_eomIssued = false;

  m_apxReceivedTime = getTime() + 5000000;

  if (!m_nosparrow) {
    SparrowDisplayLoop();
  }
  initSensnet();
  initMission();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::initMission()
{
  stringstream s("");
  s << "mission file: " << CmdArgs::missionFileName << "\n";
  print(s.str(), -1);

  if (!CmdArgs::continueMission)
  {
    m_nextCheckpointIndex = 0;
    m_missionFile = fopen(CmdArgs::missionFileName, "w");
    if (m_missionFile == NULL) {
      cerr << "Cannot open mission file: " << CmdArgs::missionFileName << endl;
      exit(1);
    }
    return;
  }

  m_missionFile = fopen(CmdArgs::missionFileName, "a+");
  if (m_missionFile == NULL) {
    cerr << "Cannot open mission file: " << CmdArgs::missionFileName << endl;
    exit(1);
  }
  rewind(m_missionFile);
  int lastCheckpoint = 0;
  int readLine = fscanf(m_missionFile, "%u", &lastCheckpoint);
  while (readLine != EOF) {
    m_nextCheckpointIndex = lastCheckpoint + 1;
    readLine = fscanf(m_missionFile, "%u", &lastCheckpoint);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::initSensnet()
{
  m_sensnet = sensnet_alloc();
  
  if (sensnet_connect(m_sensnet, CmdArgs::spreadDaemon, m_snKey, MODmissionplanner) != 0) {
    stringstream s("");
    s << "Unable to connect to sensnet, exiting program\n";
    printError("initSensnet", s.str());
    exit(1);
  }

  // Subscribe to process state messages
  if (sensnet_join(m_sensnet, SENSNET_SKYNET_SENSOR, SNprocessRequest, sizeof(ProcessRequest)) != 0) {
    stringstream s("");
    s << "Unable to join process group, exiting program\n";
    printError("initSensnet", s.str());
    exit(1);
  }
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  //  MissionControlStatus* contrStatus = dynamic_cast<MissionControlStatus*>(cs);
  MissionControlMergedDirective* mergedDirective = dynamic_cast<MissionControlMergedDirective*>(md);
  stringstream s("");

  if (!m_isInit) {
    if (!loadMDF(m_MDFFileName, m_rndf, mergedDirective)) {
      s.str("");
      s << "Unable to load MDF file " << m_MDFFileName << ", sending RESET command\n";
      printError("arbitrate", s.str());
      mergedDirective->id = mergedDirective->id + 1;
      mergedDirective->name = MissionControlMergedDirective::PAUSE;
    }
    else {
      mergedDirective->id = mergedDirective->id + 1;
      mergedDirective->name = MissionControlMergedDirective::MISSION;
    }
    m_isInit = true;
    m_restart = false;
    m_reset = false;
  }
  else if (m_restart) { 
    s.str("");
    s << "MissionControl::arbitrate: Restart\n";
    print(s.str(), 0);
    mergedDirective->id = mergedDirective->id + 1; 
    mergedDirective->name = MissionControlMergedDirective::MISSION;
    m_nextCheckpointIndex = 0;
    m_lastAddedCheckpointIndex = 0;
    m_restart = false;  
    m_reset = false;
    m_missionCompleted = false;
    m_eomIssued = false;
  }
  else if (m_reset) {
    s.str("");
    s << "MissionControl::arbitrate: Reset\n";
    print(s.str(), 0);
    mergedDirective->id = mergedDirective->id + 1;
    mergedDirective->name = MissionControlMergedDirective::MISSION;
    m_reset = false;
    m_missionCompleted = false;
    m_eomIssued = false;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::control(ControlStatus* cs, MergedDirective* md)
{
  // MissionControlStatus* contrStatus = dynamic_cast<MissionControlStatus*> (cs);
  MissionControlMergedDirective* mergedDirective = dynamic_cast<MissionControlMergedDirective*>(md);
  stringstream s("");

  // update process state
  updateProcessState();

  int replanMode = 0;  // replanMode = 0 => no replan
                       // replanMode = 1 => replan with reset command
                       // replanMode = 2 => replan with emergency stop command
  sparrowNextCkpt = m_nextCheckpointIndex + 1;

  // Get astate health
  if (astateHealthTalker.hasNewMessage()) {
    bool astateModeReceived = astateHealthTalker.receive(&m_apxStatus);
    if (astateModeReceived) {
      m_apxCount++;
      m_apxReceivedTime = getTime();
      if (m_apxStatus == (int)NOT_CONNECTED  && m_getApxStatus) {
	s.str("");
	s << "astate health: NOT_CONNECTED\n";
	printError("control", s.str(), false);
	replanMode = 2;
      }
      else if (m_apxStatus == (int)NO_MESSAGES && m_getApxStatus) {
	s.str("");
	s << "astate health: NO_MESSAGE\n";
	printError("control", s.str(), false);
	replanMode = 2;
      }
      else if (m_apxStatus == (int)NO_SOLUTION && m_getApxStatus) {
	s.str("");
	s << "astate health: NO_SOLUTION\n";
	printError("control", s.str(), false);
	replanMode = 2;
      }    
      else if (m_apxStatus == (int)INITIAL && m_getApxStatus) {
	s.str("");
	s << "astate health: INITIAL\n";
	printError("control", s.str(), false);
	replanMode = 2;
      }
    }
  }
  else if (m_getApxStatus && (int)(getTime() - m_apxReceivedTime) > APXTIMEOUT*1000) {
    s.str("");
    s << "No applanix health message for " 
      << (getTime() - m_apxReceivedTime)/100 << " ms"
      << " (timeout = " << APXTIMEOUT << " ms, m_getApxStatus = "
      << m_getApxStatus << ")\n";
    printError("control", s.str(), false);
    replanMode = 2;
  }

  // Get vehicle capability
  if (CmdArgs::getSystemHealth) {
    if (healthTalker.hasNewMessage()) {
      VehicleCapability recvdVehicleCap;
      bool healthReceived = healthTalker.receive(&recvdVehicleCap);
      if (healthReceived)
	m_healthCount++;

      if (recvdVehicleCap.intersectionRightTurn >= m_vehicleCap.intersectionRightTurn + VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.intersectionRightTurn <= m_vehicleCap.intersectionRightTurn - VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.intersectionLeftTurn >= m_vehicleCap.intersectionLeftTurn + VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.intersectionLeftTurn <= m_vehicleCap.intersectionLeftTurn - VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.intersectionStraightTurn >= m_vehicleCap.intersectionStraightTurn + VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.intersectionStraightTurn <= m_vehicleCap.intersectionStraightTurn - VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.uturn >= m_vehicleCap.uturn + VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.uturn <= m_vehicleCap.uturn - VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.nominalDriving >= m_vehicleCap.nominalDriving + VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.nominalDriving <= m_vehicleCap.nominalDriving - VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.nominalStopping >= m_vehicleCap.nominalStopping + VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.nominalStopping <= m_vehicleCap.nominalStopping - VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.nominalZoneRegionDriving >= m_vehicleCap.nominalZoneRegionDriving + VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.nominalZoneRegionDriving <= m_vehicleCap.nominalZoneRegionDriving - VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.nominalNewRegionDriving >= m_vehicleCap.nominalNewRegionDriving + VEHICLE_CAP_CHANGE ||
	  recvdVehicleCap.nominalNewRegionDriving <= m_vehicleCap.nominalNewRegionDriving - VEHICLE_CAP_CHANGE) {
	s.str("");
	s << "\n" << "MissionControl::control: Vehicle Capability:\n " 
	  << recvdVehicleCap.toString() << "\n";
	print(s.str(), 3);
	if (replanMode < 1)
	  replanMode = 1;
      }
      m_vehicleCap = recvdVehicleCap;
    }
    if (m_vehicleCap.nominalDriving <= 0.1) {
      s.str("");
      s << "VehicleCapability::nominalDriving = " << m_vehicleCap.nominalDriving << "\n";
      print(s.str(), 1);
      replanMode = 2;
    }
  }

  // Check if we are restarting the mission
  if (mergedDirective->id != m_currentMergedDirectiveID) {
    s.str("");
    s << "MissionControl::control: Restart mission\n";
    print(s.str(), 0);

    m_currentMergedDirectiveID = mergedDirective->id;
    
    s.str("");
    s << "MissionControl::control mergedDirectiveID = " << mergedDirective->id 
      << " m_currentMergedDirectiveID = " << m_currentMergedDirectiveID << "\n";
    print(s.str(), 0);
    if (replanMode < 1)
      replanMode = 1;
  }

  if (mrInterfaceSF->haveNewStatus()) {
    m_response = *(mrInterfaceSF->getLatestStatus());
    sparrowResponseGoalID = (int)m_response.goalID;
    if (!m_nosparrow) {
      UpdateSparrowVariablesLoop();
      DGClockMutex(m_shpMutex);
      if (m_response.status == MControlDirectiveResponse::COMPLETED)
	m_shp->set_string("rplanner_status", "COMPLETED");
      else if (m_response.status == MControlDirectiveResponse::FAILED)
	m_shp->set_string("rplanner_status", "FAILED   ");
      else if (m_response.status == MControlDirectiveResponse::REJECTED)
	m_shp->set_string("rplanner_status", "REJECTED ");
      else if (m_response.status == MControlDirectiveResponse::ACCEPTED)
	m_shp->set_string("rplanner_status", "ACCEPTED ");
      else
	m_shp->set_string("rplanner_status", "UNKNOWN  ");
      DGCunlockMutex(m_shpMutex);
    }

    s.str();
    s << "MissionControl::control: Received response for goal " << m_response.goalID << "\n";
    print(s.str(), 2);

    if (m_response.goalID == 0) {
      replanMode = 2;
      s.str("");
      s << "\n MissionControl: Route planner just starts listening\n";
      print(s.str(), -1);
    }

    if (m_response.status == MControlDirectiveResponse::FAILED) {
      if (m_response.reason != MControlDirectiveResponse::PREEMPTED_BY_PAUSE &&
	  m_response.reason != MControlDirectiveResponse::PREVIOUS_DIR_FAILURE) {
	if (replanMode < 1)
	  replanMode = 1;
	s.str("");
	s << "MissionControl: Goal " << m_response.goalID << " failed" << "\n";
	print(s.str(), -1);
	if (m_response.reason == MControlDirectiveResponse::NO_ROUTE) {
	  s.str("");
	  s << m_nextCheckpointIndex << "\n";
	  fprintf(m_missionFile, s.str().c_str());
	  fflush(m_missionFile);
	  if (m_vehicleCap.intersectionRightTurn >= 0 &&
	      m_vehicleCap.intersectionLeftTurn >= 0 &&
	      m_vehicleCap.intersectionStraightTurn >= 0 &&
	      m_vehicleCap.uturn >= 0 &&
	      m_vehicleCap.nominalZoneRegionDriving >= 0)
	    m_nextCheckpointIndex++;
	  s.str("");
	  s << "Warning: MissionControl: skip checkpoint " << m_nextCheckpointIndex << "\n";
	  print(s.str(), -1);
	}
      }
    }
    else if (m_response.status == MControlDirectiveResponse::COMPLETED) {
      if (m_response.goalID == m_lastPauseID)
	m_lastCompletedPauseID = m_lastPauseID;

      MControlDirective completedDirective = mrInterfaceSF->getSentDirective(m_response.goalID);

      if ( completedDirective.type == MControlDirective::NEXT_CHECKPOINT ) {
	if (completedDirective.nextCheckpointIndex != m_nextCheckpointIndex) {
	  s.str("");
	  s << "received response for goal " 
	    << m_response.goalID << " : checkpoint " 
	    << completedDirective.nextCheckpointIndex +1 << " is crossed before checkpoint "
	    << m_nextCheckpointIndex +1 << ". Go back to checkpoint " << m_nextCheckpointIndex + 1 
	    << "\n";
	  printError("control", s.str());
	  if (replanMode < 1)
	    replanMode = 1;
	}
	else if (m_nextCheckpointIndex >= mergedDirective->checkpointSequence.size()) {
	  s.str("");
	  s << "checkpoint " << m_nextCheckpointIndex + 1
	    << " doesn't exist\n";
	  printError("control", s.str());
	}
	else {
	  s.str("");
	  s << "\ncheckpoint " << m_nextCheckpointIndex + 1 
	    << " (Waypoint " << completedDirective.segmentID << "."
	    << completedDirective.laneID << "." << completedDirective.waypointID
	    << ") is crossed\n\n";
	  print(s.str(), -1);
	  s.str("");
	  s << m_nextCheckpointIndex << "\n";
	  fprintf(m_missionFile, s.str().c_str());
	  fflush(m_missionFile);
	  m_nextCheckpointIndex++;
	}
      }
      else if ( completedDirective.type == MControlDirective::RESET || 
		completedDirective.type == MControlDirective::EMERGENCY_STOP ) {
	// I don't think we need to do anything here
      }
      else if ( completedDirective.type == MControlDirective::END_OF_MISSION ){
#warning Misson Control does not handle the end of mission yet.
	if (!m_missionCompleted) {
	  m_missionCompleted = true;
	  s.str("");
	  s << "\nMISSION COMPLETED !!!\n\n";
	  print(s.str(), -1);
	}
      }
      else {
#warning Need to handle REJECTED directive
	s.str("");
	s << "unknown directive " << completedDirective.type << endl;
	printError("control", s.str());
      }
    }
    else {
      s.str("");
      s << "cannot handle status = " << m_response.status << "\n";
      printError("control", s.str());
      if (replanMode < 1)
	replanMode = 1;
    } 
  } 

  sparrowNextCkpt = m_nextCheckpointIndex + 1;

  if (replanMode == 0) {
    DGClockMutex(&m_contrDirectiveQMutex);
    while( m_contrDirectiveQ.size() > 0 && m_response.goalID + LEAST_NUM_MISSION_RPLANNER_STORED >= 
	   (m_contrDirectiveQ.front()).goalID ) {
      sendDirective();
    }
    DGCunlockMutex(&m_contrDirectiveQMutex);
  }

  if (replanMode != 0) {
    if (getTime() - m_lastPauseT > TIME_BETWEEN_PAUSE) {
      resetGoals();
      
      MControlDirective pause;
      pause.goalID = ++m_lastGoalID;
      if (replanMode == 1)
	pause.type = MControlDirective::RESET;
      else
	pause.type = MControlDirective::EMERGENCY_STOP;
      DGClockMutex(&m_contrDirectiveQMutex);
      m_contrDirectiveQ.push_back(pause);
      m_lastPauseT = getTime();
      m_lastPauseID = pause.goalID;
      s.str("");
      s << "MissionControl: " << m_contrDirectiveQ.front().toString() << "\n";
      print(s.str(), 0);
      sendDirective();
      DGCunlockMutex(&m_contrDirectiveQMutex);
      
      m_lastAddedCheckpointIndex = m_nextCheckpointIndex;
      //updateControlDirectiveQ(mergedDirective->checkpointSequence);
      /*
      DGClockMutex(&m_contrDirectiveQMutex);
      while (!m_eomIssued && m_contrDirectiveQ.size() < LEAST_NUM_MISSION_STORED) {
	MControlDirective directive = 
	  findNextCheckpointDirective(mergedDirective->checkpointSequence,
				      m_lastAddedCheckpointIndex);
	if (directive.type == MControlDirective::END_OF_MISSION)
	  m_eomIssued = true;
	m_contrDirectiveQ.push_back(directive);
	s.str("");
	s << "MissionControl: " << m_contrDirectiveQ.back().toString() << "\n";
	print(s.str(), 0);
	m_lastAddedCheckpointIndex++;
      }
      DGCunlockMutex(&m_contrDirectiveQMutex);
      */
    }
  }
  else if (m_lastCompletedPauseID == m_lastPauseID) {
    updateControlDirectiveQ(mergedDirective->checkpointSequence);
    /*
    DGClockMutex(&m_contrDirectiveQMutex);
    while (!m_eomIssued && m_contrDirectiveQ.size() < LEAST_NUM_MISSION_STORED) {
      MControlDirective directive = 
	findNextCheckpointDirective(mergedDirective->checkpointSequence,
				    m_lastAddedCheckpointIndex);
      if (directive.type == MControlDirective::END_OF_MISSION)
	m_eomIssued = true;
      m_contrDirectiveQ.push_back(directive);
      s.str("");
      s << "MissionControl: " << m_contrDirectiveQ.back().toString() << "\n";
      print(s.str(), 0);
      m_lastAddedCheckpointIndex++;
    }
    DGCunlockMutex(&m_contrDirectiveQMutex);
    */
  }

  if (!m_nosparrow)
    UpdateSparrowVariablesLoop();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::updateControlDirectiveQ(const vector<Waypoint*>& checkpointSequence)
{
  stringstream s("");
  DGClockMutex(&m_contrDirectiveQMutex);
  while (!m_eomIssued && m_contrDirectiveQ.size() < LEAST_NUM_MISSION_STORED) {
    MControlDirective directive = 
      findNextCheckpointDirective(checkpointSequence, m_lastAddedCheckpointIndex);
    if (directive.type == MControlDirective::END_OF_MISSION)
      m_eomIssued = true;
    m_contrDirectiveQ.push_back(directive);
    s.str("");
    s << "MissionControl: " << m_contrDirectiveQ.back().toString() << "\n";
    print(s.str(), 0);
    m_lastAddedCheckpointIndex++;
  }
  DGCunlockMutex(&m_contrDirectiveQMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MControlDirective MissionControl::findNextCheckpointDirective(vector<Waypoint*> checkpointSequence,
							      unsigned nextCheckpointIndex)
{
  MControlDirective directive;
  directive.goalID = m_lastGoalID + 1;
  stringstream s("");

  if(nextCheckpointIndex < checkpointSequence.size()) {
    m_lastGoalID = directive.goalID;
    directive.type = MControlDirective::NEXT_CHECKPOINT;
    if ( nextCheckpointIndex >= checkpointSequence.size() ) {
      s.str("");
      s << "invalid nextCheckpointIndex = "
	<< nextCheckpointIndex << "\n";
      printError("findNextCheckpointDirective", s.str());
      directive.nextCheckpointIndex = checkpointSequence.size() - 1;
    }
    else {
      directive.nextCheckpointIndex = nextCheckpointIndex;
    }
    directive.vehicleCap = m_vehicleCap;
    directive.segmentID = checkpointSequence[directive.nextCheckpointIndex]->getSegmentID();
    directive.laneID = checkpointSequence[directive.nextCheckpointIndex]->getLaneID();
    directive.waypointID = checkpointSequence[directive.nextCheckpointIndex]->getWaypointID();
    directive.maxSpeed = 30 * MPS_PER_MPH;
    if ( CmdArgs::getSystemHealth ) {
      if ( m_vehicleCap.nominalDriving < 0.9 )
	directive.maxSpeed = 10;
      if ( m_vehicleCap.nominalDriving < 0.75 )
	directive.maxSpeed = 5;
      if ( m_vehicleCap.nominalDriving < 0.5 )
	directive.maxSpeed = 3;
      if ( directive.maxSpeed > m_vehicleCap.nominalStopping*30*MPS_PER_MPH )
	directive.maxSpeed = m_vehicleCap.nominalStopping*30*MPS_PER_MPH;
    }	  
    if (directive.maxSpeed > CmdArgs::maxSpeed*MPS_PER_MPH) {
      directive.maxSpeed = CmdArgs::maxSpeed*MPS_PER_MPH;
    }
  }
  // We complete the mission    
  else {
    m_lastGoalID = directive.goalID;
    directive.type = MControlDirective::END_OF_MISSION;
  }

  return directive;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::resetGoals()
{
  DGClockMutex(&m_contrDirectiveQMutex);
  m_contrDirectiveQ.clear();
  DGCunlockMutex(&m_contrDirectiveQMutex);
  m_eomIssued = false;
  stringstream s("");
  s << "MissionControl: Clear contrGcPort\n";
  print(s.str(), 0);

  mrInterfaceSF->flushAll();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::sendDirective()
{
  stringstream s("");
  if( m_contrDirectiveQ.size() == 0) {
    s.str("");
    s << "ERROR: MissionControl::sendDirective: m_contrDirectiveQ.size() == 0\n";
    printError("sendDirective", s.str());
    return;
  }

  s.str("");
  s << "MissionControl: Adding goal " << (m_contrDirectiveQ.front()).goalID 
    << " to contrGcPortMsgQ.\n";
  print(s.str(), 0);
  mrInterfaceSF->sendDirective(&m_contrDirectiveQ.front());
  m_contrDirectiveQ.pop_front();
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
int MissionControl::updateProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = MODmissionplanner;
  response.timestamp = DGCgettime();
  response.logSize = 0;
  response.healthStatus = 1;
  sensnet_write(m_sensnet, SENSNET_METHOD_CHUNK,
                MODmissionplanner, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(m_sensnet, SENSNET_SKYNET_SENSOR, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  if (request.quit) {
    sparrowQuit();
    print("remote quit request", -1);
  }
  
  return 0;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool MissionControl::loadMDF(char* fileName, RNDF* rndf,
			     MissionControlMergedDirective* mergedDirective)
{
  ifstream file;
  string line;
  string word = "";

  file.open(fileName, ios::in);

  if(!file)
  {
    stringstream s("");
    s << fileName << " file not found.\n";
    printError("loadMDF", s.str());
    return false;
  } 
  
  while (word != "MDF_name") {
    getline(file, line);    
    istringstream lineStream(line, ios::in);  
    lineStream >> word;
  }

  if(word == "MDF_name")
  {
    parseCheckpoint(&file, rndf, mergedDirective);
    file.close();
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::parseCheckpoint(ifstream* file, RNDF* rndf, 
				     MissionControlMergedDirective* mergedDirective)
{
  string line, word;
  char letter;
  int ckptID;  
  
  while(word != "end_checkpoints")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      ckptID = atoi(word.c_str());
      mergedDirective->checkpointSequence.push_back(rndf->getCheckpoint(ckptID));
    }
    else
    {
      continue;
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::print(string mess, int d)
{
  stringstream s("");
  s << getTime() << "\t" << mess;
  if (DEBUG_LEVEL > d) {
    if (m_nosparrow) {
      cout << mess;
    }
    else {
      SparrowHawk().log(mess);
    }
  }
  if (m_logData) {
    fprintf(m_logFile, s.str().c_str());
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::printError(string funcName, string errorMessage, bool error)
{
  stringstream s1("");
  if (error)
    s1 << "\nERROR: MissionControl::" << funcName << " : ";
  s1 << errorMessage << "\n";
  cerr << s1.str();

  stringstream s2("");
  s2 << getTime() << "\t" << s1.str();
  if (m_logData) {
    fprintf(m_logFile, s2.str().c_str());
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
uint64_t MissionControl::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Sparrow-related functions */
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::SparrowDisplayLoop()
{
  DGClockMutex(m_shpMutex);
  m_shp->add_page(mcontroltable, "MissionControl");

  m_shp->rebind("mcsnkey", &m_snKey);
  m_shp->set_readonly("mcsnkey");

  m_shp->set_string("mcRNDFFileName", m_RNDFFileName);
  m_shp->set_readonly("mcRNDFFileName");

  m_shp->set_string("mcMDFFileName", m_MDFFileName);
  m_shp->set_readonly("mcMDFFileName");

  m_shp->rebind("apxCount", &m_apxCount);
  m_shp->rebind("apxStatus", &m_apxStatus);
  m_shp->rebind("recvApx", &m_getApxStatus);
  m_shp->rebind("apxTimeout", &APXTIMEOUT);
  m_shp->set_readonly("apxCount");
  m_shp->set_readonly("apxStatus");

  m_shp->rebind("recvHealth", &CmdArgs::getSystemHealth);
  m_shp->rebind("healthCount", &m_healthCount);
  m_shp->rebind("nominalCap", &m_vehicleCap.nominalDriving);
  m_shp->rebind("rightCap", &m_vehicleCap.intersectionRightTurn);
  m_shp->rebind("leftCap", &m_vehicleCap.intersectionLeftTurn);
  m_shp->rebind("straightCap", &m_vehicleCap.intersectionStraightTurn);
  m_shp->rebind("stopCap", &m_vehicleCap.nominalStopping);
  m_shp->rebind("zoneCap", &m_vehicleCap.nominalZoneRegionDriving);
  m_shp->rebind("newRegionCap", &m_vehicleCap.nominalNewRegionDriving);
  m_shp->set_readonly("healthCount");
  m_shp->set_readonly("nominalCap");
  m_shp->set_readonly("rightCap");
  m_shp->set_readonly("leftCap");
  m_shp->set_readonly("straightCap");
  m_shp->set_readonly("stopCap");
  m_shp->set_readonly("zoneCap");
  m_shp->set_readonly("newRegionCap");

  m_shp->rebind("rplanner_goalID", &(sparrowResponseGoalID));
  m_shp->set_string("rplanner_status", "UNKNOWN");
  m_shp->set_readonly("rplanner_goalID");
  m_shp->set_readonly("rplanner_status");

  m_shp->rebind("mcnextGoal1ID", &(sparrowNextGoalIDs[0]));
  m_shp->rebind("mcnextGoal1_segment", &(sparrowNextSegments[0]));
  m_shp->rebind("mcnextGoal1_lane", &(sparrowNextLanes[0]));
  m_shp->rebind("mcnextGoal1_waypoint", &(sparrowNextWaypoints[0]));
  m_shp->set_string("mcnextGoal1_type", "UNKNOWN");
  m_shp->rebind("mcnextCkpt", &sparrowNextCkpt);
  m_shp->set_readonly("mcnextGoal1ID");
  m_shp->set_readonly("mcnextGoal1_segment");
  m_shp->set_readonly("mcnextGoal1_lane");
  m_shp->set_readonly("mcnextGoal1_waypoint");
  m_shp->set_readonly("mcnextGoal1_type");
  m_shp->set_readonly("mcnextCkpt");

  m_shp->rebind("mcnextGoal2ID", &(sparrowNextGoalIDs[1]));
  m_shp->set_string("mcnextGoal2_type", "UNKNOWN");
  m_shp->set_readonly("mcnextGoal2ID");
  m_shp->set_readonly("mcnextGoal2_type");

  m_shp->rebind("mcnextGoal3ID", &(sparrowNextGoalIDs[2]));
  m_shp->set_string("mcnextGoal3_type", "UNKNOWN");
  m_shp->set_readonly("mcnextGoal3ID");
  m_shp->set_readonly("mcnextGoal3_type");

  m_shp->rebind("mcnextGoal4ID", &(sparrowNextGoalIDs[3]));
  m_shp->set_string("mcnextGoal4_type", "UNKNOWN");
  m_shp->set_readonly("mcnextGoal4ID");
  m_shp->set_readonly("mcnextGoal4_type");

  m_shp->set_notify("sparrowRestartMission", this, &MissionControl::sparrowRestartMission);
  m_shp->set_notify("sparrowResetMission", this, &MissionControl::sparrowResetMission);
  m_shp->set_notify("sparrowQuit", this, &MissionControl::sparrowQuit);
  DGCunlockMutex(m_shpMutex);
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::UpdateSparrowVariablesLoop()
{
  deque<unsigned> execGoalIDs = mrInterfaceSF->orderedDirectivesWaitingForResponse();
  deque<MControlDirective> executingGoals;

  for (unsigned i=0; i<execGoalIDs.size(); i++) {
    executingGoals.push_back(mrInterfaceSF->getSentDirective(execGoalIDs[i]));
  }

  int i = 0;
  deque<MControlDirective> mission;	
  DGClockMutex(&m_contrDirectiveQMutex);
  if (m_contrDirectiveQ.size() > 0) {
    mission.assign(m_contrDirectiveQ.begin(), m_contrDirectiveQ.end());
  }
  DGCunlockMutex(&m_contrDirectiveQMutex);

  char* nextGoalType;

  while (i < NUM_GOALS_DISPLAYED && executingGoals.size() > 0) {
    sparrowNextGoalIDs[i] = (executingGoals.front()).goalID;
    sparrowNextSegments[i] = (executingGoals.front()).segmentID;
    sparrowNextLanes[i] = (executingGoals.front()).laneID;
    sparrowNextWaypoints[i] = (executingGoals.front()).waypointID;

    switch (i) {
    case 0:
      nextGoalType = "mcnextGoal1_type";
      break;
    case 1:
      nextGoalType = "mcnextGoal2_type";
      break;
    case 2:
      nextGoalType = "mcnextGoal3_type";
      break;
    default:
      nextGoalType = "mcnextGoal4_type";
      break;
    }

    DGClockMutex(m_shpMutex);
    if ((executingGoals.front()).type == MControlDirective::NEXT_CHECKPOINT) {
      m_shp->set_string(nextGoalType, "NEXT_CHECKPOINT");
    }
    else if ((executingGoals.front()).type == MControlDirective::END_OF_MISSION) {
      m_shp->set_string(nextGoalType, "END_OF_MISSION ");
    }
    else if ((executingGoals.front()).type == MControlDirective::RESET) {
      m_shp->set_string(nextGoalType, "RESET          ");
    }
    else if ((executingGoals.front()).type == MControlDirective::DRIVE_AROUND) {
      m_shp->set_string(nextGoalType, "DRIVE_AROUND   ");
    }
    else if ((executingGoals.front()).type == MControlDirective::BACK_UP) {
      m_shp->set_string(nextGoalType, "BACK_UP        ");
    }
    else if ((executingGoals.front()).type == MControlDirective::EMERGENCY_STOP) {
      m_shp->set_string(nextGoalType, "EMERGENCY_STOP ");
    }
    else {
      m_shp->set_string(nextGoalType, "UNKNOWN");
    }
    DGCunlockMutex(m_shpMutex);

    executingGoals.pop_front();
    i++;
  }

  while (i < NUM_GOALS_DISPLAYED && mission.size() > 0) {
    sparrowNextGoalIDs[i] = (mission.front()).goalID;
    sparrowNextSegments[i] = (mission.front()).segmentID;
    sparrowNextLanes[i] = (mission.front()).laneID;
    sparrowNextWaypoints[i] = (mission.front()).waypointID;

    switch (i) {
    case 0:
      nextGoalType = "mcnextGoal1_type";
      break;
    case 1:
      nextGoalType = "mcnextGoal2_type";
      break;
    case 2:
      nextGoalType = "mcnextGoal3_type";
      break;
    default:
      nextGoalType = "mcnextGoal4_type";
      break;
    }

    DGClockMutex(m_shpMutex);
    if ((mission.front()).type == MControlDirective::NEXT_CHECKPOINT) {
      m_shp->set_string(nextGoalType, "NEXT_CHECKPOINT");
    }
    else if ((mission.front()).type == MControlDirective::END_OF_MISSION) {
      m_shp->set_string(nextGoalType, "END_OF_MISSION ");
    }
    else if ((mission.front()).type == MControlDirective::RESET) {
      m_shp->set_string(nextGoalType, "RESET          ");
    }
    else if ((mission.front()).type == MControlDirective::DRIVE_AROUND) {
      m_shp->set_string(nextGoalType, "DRIVE_AROUND   ");
    }
    else if ((mission.front()).type == MControlDirective::BACK_UP) {
      m_shp->set_string(nextGoalType, "BACK_UP        ");
    }
    else if ((mission.front()).type == MControlDirective::EMERGENCY_STOP) {
      m_shp->set_string(nextGoalType, "EMERGENCY_STOP ");
    }
    else {
      m_shp->set_string(nextGoalType, "UNKNOWN");
    }
    DGCunlockMutex(m_shpMutex);

    mission.pop_front();
    i++;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::sparrowRestartMission()
{
  string s = "Restart\n";
  printError("sparrowRestartMission", s, false);
  m_restart = true;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::sparrowResetMission()
{
  string s = "Restart\n";
  printError("sparrowResetMission", s, false);
  m_reset = true;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::sparrowQuit()
{
  string quitMess = "Quit\n";
  printError("sparrowQuit", quitMess, false);

  MControlDirective pause;
  pause.goalID = ++m_lastGoalID;
  pause.type = MControlDirective::RESET;
  DGClockMutex(&m_contrDirectiveQMutex);
  m_contrDirectiveQ.clear();

  stringstream s("");
  s << "MissionControl: Clear contrGcPort\n";
  print(s.str(), 0);

  mrInterfaceSF->flushAll();

  m_contrDirectiveQ.push_back(pause);
  s.str("");
  s << "MissionControl: " << m_contrDirectiveQ.front().toString() << "\n";
  print(s.str(), -1);
  sendDirective();
  DGCunlockMutex(&m_contrDirectiveQMutex);

  usleep(500000);
  quit = true;
}

