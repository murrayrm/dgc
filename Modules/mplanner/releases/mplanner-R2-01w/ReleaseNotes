              Release Notes for "mplanner" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "mplanner" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "mplanner" module can be found in
the ChangeLog file.

Release R2-01w (Thu May 24 17:25:16 2007):
	Speed up compilation time. Added command line argument --log-level
	for gcmodule logging. All log files go to ./logs directory.

Release R2-01v (Thu May 24  1:26:11 2007):	
	Fixed another segfault when an obstacle is added from sparrow
	display when mplanner receives map from mapper

Release R2-01u (Wed May 23  1:29:51 2007):
	* Fixed segfault problem
	* Added directive for turn signal
	* mplanner should be bright green for the site visit

Release R2-01t (Sat May 19 12:55:03 2007):
	Used dgcFindRouteFile

Release R2-01s (Sun May 13 10:22:21 2007):
	Added more scenario for unit testing. No change in functionality.

Release R2-01r (Thu May 10 11:58:28 2007):
	* Fixed the MPlannerCommand so it works with the new release of
	gcinterfaces and added scripts for unit tests.
	* Added automated unit tests

Release R2-01q (Sat May  5  9:42:18 2007):
	Fixed bug when illegal passing is allowed

Release R2-01p (Thu May  3 23:37:40 2007):
	* Added a button to remove all the manually inserted obstacles on the
	sparrow display.
	* Added some missing mutices.

Release R2-01o (Thu May  3 18:14:27 2007):
	Fixed the directive class so mplanner compile against version
	R1-00k of gcinterfaces

Release R2-01n (Sat Apr 28 18:43:25 2007):
	Allow illegal uturn.
	Added commandline option for not allowing illegal uturn

Release R2-01m (Sat Apr 28  8:44:48 2007):
	Put more info in the sparrow display

Release R2-01l (Thu Apr 26 23:42:56 2007):
	* This version works with R1-01b of gcmodule
	* Set stale threshold to 20
	* Added apx timeout to teh sparrow display
	* Fixed logging ingraphEstimator
	* Added QUIT to the sparrow display. It'll send down a PAUSE directive before it actually quits.

Release R2-01k (Wed Apr 25 23:39:39 2007):
	* Mission Control now talks to Route Planner using GcInterface (instead of function call)
	* Added stop before uturn
	* 2 tabs in sparrow display, one for Mission Control and the other for Route Planner
	* Handle astate failure
	* Can restart from the console
	* NOTE: This version doesn't compile against R1-01a of gcmodule. This will be fixed
	in the next release. It also segfaults on my mac (gcc 4.0.1) but works fine on gentoo. (Tried running
	it in valgrind and it's pretty happy so I'm not sure what the problem is.)

Release R2-01j (Fri Apr 20 11:49:54 2007):
	* Minor fix to make mplanner compile with version R1-00e of gcinterfaces
	* Modified DummyTPlanner so it uses GcInterface
	* Removed global variable DEBUG_LEVEL

Release R2-01i (Sat Apr 14 13:44:17 2007):
	Fixed the route planner so 3 segment goals are always sent to 
the traffic planner.

Release R2-01h (Wed Apr 11 22:40:35 2007):
	Added missing files. (Note the previous version doesn't compile.)

Release R2-01g (Wed Apr 11 21:43:03 2007):
	Increase the cost of the edge that is blocked instead of removing it. An edge is removed only if it isblocked and tplanner fails on that 
edge. Also fixed the include statements so it compiles against version R4-00q of the interfaces module."

Release R2-01f (Tue Apr  3 18:49:11 2007):
	Updated TraversibilityGraphEstimation so it compiles against version R1-01l of travgraph. Yaw is now in (-pi,pi]. Also added 
	commandline option --disable-console for no sparrow display.

Release R2-01e (Sat Mar 17 13:39:20 2007):
	Fixed bug in uturn when map is received from mapper.

Release R2-01d (Sat Mar 17  2:38:37 2007):
	Moved the function for inserting obstacle to Graph. Recursively remove all the edges in the segment that is blocked and find a uturn point 
that is at least 22m from current position.

Release R2-01c (Fri Mar 16 13:10:06 2007):
	Fixed include statement due to the changes in gcmodule"

Release R2-01b (Fri Mar 16 12:17:33 2007):
	Added more information to log file.

Release R2-01a (Fri Mar 16  1:06:27 2007):
	Mission planner that uses GcInterface for message handling (sending directive, receiving status and paring up directive	and status), so it 
uses SkynetTalker instead of SegGoalsTalker to communicate with traffic planner. It can also log data (mission plan, graph, etc) and search for 
rndf/mdf in the DGC_CONFIG_PATH directory. (Un)fixed the yaw angle so it ranges from 0 to 2pi (which is what astate is using right now).

Release R2-00h (Wed Mar 14 18:05:50 2007):
	Reduced the number of threads in RoutePlanner by combining the	sending and receiving message threads into one thread. Made the dummyTPlanner 
a little smarter. This is the last release of the mission planner that uses SegGoalsTalker to communicate with the traffic planner. 
The next release will use the generic talker.

Release R2-00g (Tue Mar 13 21:18:05 2007):
	Mission planner that is completely in canonical structure. All the modules are derived from GcModule. Still waiting for automatic message 
handling in GcModule. Nok (, Daniele and Josh) are happy.

Release R2-00f (Mon Mar 12 19:30:25 2007):
	The RoutePlanner in the canonical structure but not yet derived from gcmodule. Fixed some bugs in MissionControl.

Release R2-00e (Fri Mar  9 16:37:48 2007):
	Fixed the heading angle computation. Added the default constructor for MissionControl. Fixed Makefile. Combined the functions 
planFromCurrentPosition and planNextMission in RoutePlanner.
	This release of mplanner is only compatible with release R3 (or newer) of the interfaces module.

Release R2-00d (Wed Mar  7 20:56:13 2007):
	All the rndf dependencies were moved to the graph estimation class (for unit test only). This version of mplanner only commands uturn if 
there are obstacles that block the road."

Release R2-00c (Wed Mar  7 11:30:23 2007):
	Fixed segfault when DEBUG_LEVEL>4 and removed that mutex that protects the map because only one thread in RoutePlanner needs it now.

Release R2-00b (Tue Mar  6 21:20:29 2007):
	Fixed bug in the findRoute function. Getting closer to remove the rndf dependency. The route planner now only needs RNDF when it 
determines which edges the manually inserted obstacles block. This function should belong to the map class but I'll leave it here for now for unit 
test purpose.

Release R2-00a (Mon Mar  5 19:47:20 2007):
	Mission planner that plans route without using rndf information. Got the stupid segfault problem figured out. (Thanks to Daniele again.) 
Changed the release number because as in the Release R1-00g, this mission planner uses different interfaces to communicate with the mapper.

Release R1-00g (Sat Mar  3 20:29:19 2007):
	Moved all the graph functionality (including for doing uturn) from the MissionPlanner class to the TraversibilityGraphEstimator class. This 
version of mplanner receives a graph from mapper instead of an rndf object. Need to run the whole loop to verify that the uturn is working correctly. 
(This version of mplanner determines the current lane and segment from state instead of getting it from tplanner so it's harder to run a unit test 
where tplanner fails such that a uturn is needed because the vehicle does not move from the initial position unless we actually run the whole 
planning loop.)

Release R1-00f (Thu Mar  1 22:31:53 2007):
	Nok's first attempt to untangle the mission planner. Moved the graph structure to the travgraph module and moved relevant functions to the 
travgraph and rndf modules.

Release R1-00e (Wed Feb 21 22:39:00 2007):
	Updated to work with the new Status.hh in revision R2-00b of interfaces

Release R1-00d (Tue Feb 20 17:17:42 2007):
	Modified mission planner so it compiles against revision R2-00a of interfaces and skynet

Release R1-00c (Thu Feb  1 20:34:35 2007):
	Modified mission planner so it takes into account hte 
orientation of the vehicle when it computes the closest 
waypoint.

Release R1-00b (Wed Jan 31 11:36:28 2007):
	Changed variables name so they are compatible with the changes 
in VehicleState.

Release R1-00a (Wed Jan 24 23:58:24 2007):
	Added files needed by mission planner.

Release R1-00 (Tue Jan 23 20:03:35 2007):
	Created.






































