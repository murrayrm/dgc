/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */


#ifndef ROUTEPLANNER_HH
#define ROUTEPLANNER_HH

#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <deque>
#include <math.h>
//#include "alice/estop.h"
#include "dgcutils/DGCutils.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include "rndf/RNDF.hh"
#include "travgraph/Graph.hh"
#include "skynettalker/SegGoalsTalker.hh"
#include "dgcutils/GlobalConstants.h"
#include "Obstacle.hh"
#include "MissionUtils.hh"
#include "MissionControl.hh"
#include "TraversibilityGraphEstimation.hh"

#define LEAST_NUM_SEGGOALS_STORED 8
#define LEAST_NUM_SEGGOALS_TPLANNER_STORED 3
#define NUM_SEGGOALS_DISPLAYED 6


/*! The structure for merged directive that is "sent" from arbiter to control. 
 * Since the arbiter just passes through the directive MergedDirective has
 * the same structure as MContrDirective. */
typedef MContrDirective RPlannerMergedDirective;

/*! The structure for status of merged directive. This status is "sent"
 *  from control back to arbiter. The structure for this is the same
 *  as MContrDirectiveStatus. */
typedef MContrDirectiveStatus RPlannerControlStatus;

/*! The structure for response from tplanner */
struct RPlannerContrMsgResponse
{
  enum Status{ QUEUED, SENT, ACCEPTED, COMPLETED, FAILED };
  RPlannerContrMsgResponse()
  {
    // initialize the segment goals to zeros to appease the memory profilers
      memset(this, 0, sizeof(*this));
      this->status = QUEUED;
  }
  Status status;
  SegGoalsStatus::ReasonForFailure reason;
};

/*! The structure for pairing up the directives sent to the
 *  traffic planner (but in the language of the control) and the response
 *  to this directive. */
struct RPlannerContrMsgWrapper
{
  RPlannerContrMsgWrapper()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
    response.status = RPlannerContrMsgResponse::QUEUED;
  }
  SegGoals directive;  // goalID is in here
  RPlannerContrMsgResponse response;
};


/**
 * RoutePlanner class.
 * This is a main class for the mission planner where information from 
 * gloNavMap (mapping) and MDF file is used to compute segment goals
 * and send to the traffic planner.
 * \brief Main class for mission planner function
 */
class RoutePlanner : public CSegGoalsTalker
{
  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;

  
  int DEBUG_LEVEL, VERBOSITY_LEVEL;

  /*!\param m_rndf is a pointer to the RNDF object. */
  RNDF* m_rndf;

  /*!\param m_graphEstimator is the TraversibilityGraphEstimation object. */
  TraversibilityGraphEstimation m_graphEstimator;

  /*!\param m_travGraph is the corresponding graph of the current RNDF
   * that mission planner uses to compute segment goals. */
  Graph* m_travGraph;

  /*!\param m_minSpeedLimits is the vector of min speed limits in m/s*/
  vector<double> m_minSpeedLimits;

  /*!\param m_maxSpeedLimits is the vector of max speed limits in m/s*/
  vector<double> m_maxSpeedLimits;

  /*!\param m_missionControl is the Mission Control object which keeps
   * track of where we are in the mission. */
  MissionControl m_missionControl;

  /*!\param CkptcurrentSegmentID, currentCkptLaneID, currentCkptWaypointID is the
   * waypoint ID of the current checkpoint */
  int m_currentCkptSegmentID, m_currentCkptLaneID, m_currentCkptWaypointID;

  /*!\param m_checkpointGoalID is an array of goalID's which correspond
   * to the goals for crossing checkpoints. m_checkpointGoalID[i] is the 
   * goalID for crossing m_checkpointSequence[i] */
  int* m_checkpointGoalID;       

  /*!\param m_missionID tells us where we are in MDF */
  int m_missionID;

  /*!\param m_segGoalsSeq */
  deque<SegGoals> m_segGoalsSeq;

  /*!\param m_executingGoals is a list of goals that are currently stored in
   * the traffic planner. This is basically the goals mission planner
   * sent to the traffic planner that have not yet completed. The first
   * element of this list is the goal that mission planner believes the
   * traffic planner is executing. */
  deque<SegGoals> m_executingGoals;

  /*!\param m_nextGoalID is the ID of the next goal to be added 
   * to m_segGoalsSeq. */
  int m_nextGoalID; 

  /*!\param m_segGoalsStatus is the status of the traffic planner which is
   * used in the planning loop to determine if we need to replan. */
  SegGoalsStatus m_segGoalsStatus;

  /*!\param m_receivedSegGoalsStatus is the actual status of the traffic
   * planner. */
  SegGoalsStatus* m_receivedSegGoalsStatus;

  /*!\param m_numSegGoalsRequests is the numer of requests for segment goals
   * received from the traffic planner. */
  int m_numSegGoalsRequests;

  /*!\param Varibales which keep track of whether traffic planner 
   * starts listening. */
  bool m_bReceivedAtLeastOneSegGoalsStatus;
  pthread_mutex_t m_FirstSegGoalsStatusReceivedMutex;
  pthread_cond_t m_FirstSegGoalsStatusReceivedCond;

  /*!\param The mutex to protect the status of segGoals */
  pthread_mutex_t m_SegGoalsStatusMutex;

  /*!\param The skynet socket for sending segment-level goals to tplanner */
  int m_segGoalsSocket;
  
  /*!\param SparrowHawk display */
  CSparrowHawk *shp;

  /*!\param Sparrow variables */
  int sparrowNextGoalIDs[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints2[NUM_SEGGOALS_DISPLAYED];
  double sparrowMinSpeed, sparrowMaxSpeed;
  bool sparrowIllegalPassingAllowed, sparrowStopAtExit, sparrowIsExitCheckpoint;

  /*!\param Obstacle inserted by sparrow */
  int obstacleID;
  int obstacleSegmentID;
  int obstacleLaneID;
  int obstacleWaypointID;

  /*!\param merged directive sent from arbiter to control */
  RPlannerMergedDirective m_mergedDirective;

  /*!\param goalID of control directive that corresponds to the completion of
   * m_mergedDirective */
  int m_mergedDirectiveGoalID;

  /*!\param goalID of the last directive that is planned */
  int m_lastPlannedMergedDirectiveID;

  /*!\param the previous merged directive that needs to be completed before m_mergedDirective.
   * When m_mergedDirective is near completion, it will be updated
   * by the arbiter but control still needs to keep it to make sure that
   * it is completed. */
  RPlannerMergedDirective m_prevMergedDirective;

  /*!\param goalID of control directive that corresponds to the completion of
   * m_prevMergedDirective */
  int m_prevMergedDirectiveGoalID;

  /*!\param control status sent from control to arbiter */
  RPlannerControlStatus m_contrStatus;

  /*!\param directives currently stored in control waiting to be added to contrGcPort */
  deque<SegGoals> m_contrDirectiveQ;

  /*!\param the mutex to protect m_contrDirectiveQ */
  pthread_mutex_t m_contrDirectiveQMutex;

  /*!\param message queue in gcPort for control. These are directives that will be sent to tplanner
   * right away by the messaging thread*/
  deque<RPlannerContrMsgWrapper> m_contrGcPortMsgQ;

  /*!\param the index of the oldest directive in m_contrGcPortMsgQ that has not been completed or failed */
  int  m_firstQueuedMsgIndex;

  /*!\param the mutex to protect m_contrGcPortMsgQ */
  pthread_mutex_t m_contrGcPortMutex;

  /*!\param whether a new status is received from tplanner */
  bool m_newStatus;

  /*!\param the mutex to protect m_newStatus */
  pthread_mutex_t m_newStatusMutex;


public:
  /*! Constructor */
  RoutePlanner(int skynetKey, bool bWaitForStateFill, bool nosparrow, char* RNDFFileName,
      char* MDFFileName, bool usingGloNavMapLib, int debugLevel, bool verbose);
      
  /*! Standard destructor */
  ~RoutePlanner();

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface. */
  void UpdateSparrowVariablesLoop();

  /*! This is the function that continually runs the planner in a loop */
  void MPlanningLoop(void);

  /*! This is the function that continually reads tplanner status and update the
   *  executing goalID. */
  void getTPlannerStatusThread();

  /*! This is the function that continually transmit segment goals to tplanner and
   *  update the executing goalID. */
  void sendSegGoalsThread();

private:
  /*! Arbitration for the route planner module. ControlStatus is the input and
   *  MergedDirective is the output of this function. */
  void arbitrate(RPlannerControlStatus*, RPlannerMergedDirective*);

  /*! Control for the route planner module. ControlStatus is the output and
   *  MergedDirective is the input of this function. */
  void control(RPlannerControlStatus*, RPlannerMergedDirective*);

  /*! Whether the given directive is completed */
  bool isCompleted(int);

  /*! Get the response of tplanner to the given directive */
  RPlannerContrMsgResponse getResponse(int);

  /*! Whether a new status is received from tplanner after the last time getStatus is called */
  bool gotNewStatus();

  /*! Get the latest tplanner status change */
  RPlannerContrMsgWrapper getLatestStatusChange();

  /*! Add the manually inserted obstacle to the map */
  void sparrowInsertObstacle();

  /*! Request map */
  void requestGloNavMap();
  vector<SegGoals> planSegGoals(int, int, int, int, int, int, int, bool);
  //vector<SegGoals> planFromCurrentPosition(int, int, int, int);
  //vector<SegGoals> planNextMission(int);
  vector<SegGoals> addUturn(Vertex*, Vertex*);
  void addEndOfMission(vector<SegGoals>&);
  void resetGoals();
  bool loadMDFFile(char*, RNDF*);
  void parseSpeedLimit(ifstream*, RNDF*);
  void setSpeedLimits(int, double, double, RNDF*);
  void printMissionOnSparrow(deque<SegGoals> segGoals, int startIndex);
  void addDirectiveToGcPort(SegGoals directive);
};

#endif  // ROUTEPLANNER_HH
