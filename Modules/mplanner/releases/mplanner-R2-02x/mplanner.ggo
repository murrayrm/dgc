#
# This is the source file for managing mission planner command-line options.  
# It uses gengetopt (http://www.gnu.org/software/gengetopt/gengetopt.html)
#
# Nok Wongpiromsarn, 2007-01-05
#
# Specification:
#   (1) no command-line options required if default settings are ok
#   (2) Command-line options will override default settings

package "mplanner"
purpose "mplanner computes the mission plan, which specifies how Alice
will satisfy the mission specified in the MDF."
version "1.0"

# Options for rndf and mdf
option "nomap" - "do not listen to mapper" flag off
option "rndf" - "rndf file"
  string default="darpa_sample.rndf" no
option "mdf" - "mdf file"
  string default="darpa_sample_simpleloop.mdf" no

# Options for logging
option "enable-logging" l "enable data logging" flag off
option "log-level" - "set the gcmodule log level" int default="0" no argoptional
option "log-path" - "set the log path"
  string default="./logs" no

# Options to allow compatibility with tplanner
option "nopause" p "do not issue PAUSE directive" flag off
option "noillegalpassing" i "do not allow illegal passing" flag off

# Options for getting health info
option "enable-apx-status" a "get Applanix status" flag off
option "enable-system-health" - "get system health from health monitor" flag off

# Options for continuing the mission (in case mplanner dies)
option "continue" - "Read the file specified by the mission-file option to figure out the first	checkpoint we need to cross"
  flag off
option "mission-file" - "The file that keeps track of the checkpoints we have crossed"
  string default="mission.log" no

# Standard options
option "disable-console" D "turn off sparrow display" flag  off
option "nosp" - "turn off sparrow display" flag  off
option "nowait" - "do not wait for state to fill, plan from vehicle state" flag off
option "spread-daemon" - "spread daemon" string no
option "skynet-key" S "skynet key" int default="0" no
option "debug" d "specify the amount of debug messages" int default="0" no argoptional
option "verbose" v "turn on verbose error messages" flag off
