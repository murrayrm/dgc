/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */
 
#include <iostream>
#include <skynet/skynet.hh>
#include <skynettalker/SkynetTalker.hh>
#include <interfaces/VehicleCapability.h>

class DummyHealthMonitor
{
  SkynetTalker<VehicleCapability> healthTalker;
  
public:
  /*! Contstructor */
  DummyHealthMonitor(int skynetKey) : healthTalker(skynetKey, SNvehicleCapability, MODhealthMonitor)
  {
    sendMessage();
  }

  void sendMessage()
  {
    VehicleCapability vehicleCap;
    int numLoop = 0;
    while(true) {
      numLoop++;
      vehicleCap.timestamp = 0;
      vehicleCap.intersectionRightTurn = 1;
      vehicleCap.nominalDriving = 1;
      if (numLoop > 200 && numLoop < 400)
	vehicleCap.intersectionRightTurn = 0.5;
      if (numLoop > 300 && numLoop < 500)
	vehicleCap.nominalDriving = 0.5;
      vehicleCap.intersectionLeftTurn = 1;
      vehicleCap.intersectionStraightTurn = 1;
      vehicleCap.uturn = 1;
      vehicleCap.nominalStopping = 1;
      vehicleCap.nominalZoneRegionDriving = 1;
      vehicleCap.nominalNewRegionDriving = 1;
      healthTalker.send(&vehicleCap);
      usleep(100000);
    }
  }
};

int main(int argc, char **argv)
{
  int sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;
  DummyHealthMonitor healthMonitor(sn_key);
  return 0;
}
