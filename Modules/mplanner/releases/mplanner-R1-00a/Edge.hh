/*!**
 * Morlan Lui and Nok Wongpiromsarn
 * December 8, 2006
 */

#ifndef EDGE_HH_
#define EDGE_HH_
#define COST_CHANGE_LANE 100.0f
#define COST_ZONE 200.0f
#define COST_STOP_SIGN 300.0f
#define COST_INTERSECTION 500.0f
#define COST_OBSTRUCTED 500.0f
#define COST_UTURN 2000.0f
#define COST_KTURN 3000.0f
#define INTERSECTION_SPEED 20.0f
#define MAX_OBSTRUCTED_LEVEL 3
using namespace std;

class Vertex;

/*! Edge class. Represents an edge of a graph. An edge contains an edge weight
 *  and a vertex pointer to the vertex it is connected to.
 * \brief The Edge class used in the Graph class.
 */
class Edge
{
public:
/*! All edges have a vertex pointer to the vertex it is connected to. */
  Edge(Vertex* previous, Vertex* next);
  virtual ~Edge();
  
/*! Returns the edge weight of THIS. */
  double getWeight();

/*! Returns the length of the segment corresponding to THIS. */
  double getLength();
  
/*! Returns a vertex pointer to the vertex THIS is connected from */
  Vertex* getPrevious();

/*! Returns a vertex pointer to the vertex THIS is connected to. */
  Vertex* getNext();

/*! Returns the level of how much this edge is obstructed */
  int getObstructedLevel();
  
/*! Sets the edge weight of THIS. */
  void setWeight(double);

/*! Sets the length of the segment corresponding to THIS. */
  void setLength(double);

/*! Sets the level of how much this edge is obstructed */
  void setObstructedLevel(int);
  
/*! Sets the vertex pointer to the vertex THIS is connected to. */
  void setNext(Vertex* next);
  
private:
  double weight, length;
  Vertex* next;
  Vertex* previous;
  int obstructedLevel;
};

#endif /*EDGE_HH_*/
