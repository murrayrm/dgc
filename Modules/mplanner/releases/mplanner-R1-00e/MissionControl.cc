/*!**
 * Nok Wongpiromsarn
 * February 21, 2007
 */


#include "MissionControl.hh"
using namespace std;

MissionControl::MissionControl(RNDF* rndf, char* MDFFileName, bool nosparrow, 
			       int debugLevel, bool verbose)
{
  m_rndf = rndf;
  m_nosparrow = nosparrow;
  DEBUG_LEVEL = debugLevel; 
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }
  arbitrate(MDFFileName, m_rndf);
  control();
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MContrDirective MissionControl::getNextDirective()
{
  return m_nextContrDirective;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl:: updateDirectiveStatus(MContrDirectiveStatus directiveStatus)
{
  control(directiveStatus);
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::arbitrate(char* MDFFileName, RNDF* rndf)
{
  if (!loadMDF(MDFFileName, m_rndf))
  {
    cerr << "Error: Unable to load MDF file " << MDFFileName << ", exiting program"
         << endl;
    #warning "mplanner will quit if MDF is not valid. Should instead put vehicle in pause."
    exit(1);
  }
  m_nextCheckpointIndex = 0;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::control()
{
  if(unsigned(m_nextCheckpointIndex) < m_checkpointSequence.size())
    {
      m_nextContrDirective.goalID = m_nextContrDirective.goalID + 1;
      m_nextContrDirective.name = MContrDirective::NEXT_CHECKPOINT;
      m_nextContrDirective.nextCheckpoint = m_checkpointSequence[m_nextCheckpointIndex];
    }
  else // We complete the mission
    {
      m_nextContrDirective.goalID = m_nextContrDirective.goalID + 1;
      m_nextContrDirective.name = MContrDirective::PAUSE;
      if (m_nosparrow)
	{
	  cout << endl << "MISSION COMPLETED !!!" << endl << endl;
	}
      else
	{
	  SparrowHawk().log("MISSION COMPLETED !!!\n");
	}
    }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::control(MContrDirectiveStatus directiveStatus)
{
  if (m_nextContrDirective.goalID != directiveStatus.goalID)
    {
      cerr << "ERROR: MissionControl::control : Status goalID = " 
	   << directiveStatus.goalID << " Control directive goalID = "
	   << m_nextContrDirective.goalID << endl;
    }

  if (directiveStatus.status == MContrDirectiveStatus::COMPLETED)
    {
      if(unsigned(m_nextCheckpointIndex) + 1 < m_checkpointSequence.size())
	{
	  m_nextCheckpointIndex++;
	  m_nextContrDirective.goalID = m_nextContrDirective.goalID + 1;
	  m_nextContrDirective.name = MContrDirective::NEXT_CHECKPOINT;
	  m_nextContrDirective.nextCheckpoint = m_checkpointSequence[m_nextCheckpointIndex];
	}
      else // We complete the mission
	{
	  m_nextContrDirective.goalID = m_nextContrDirective.goalID + 1;
	  m_nextContrDirective.name = MContrDirective::PAUSE;
	  if (m_nosparrow)
            {
              cout << endl << "MISSION COMPLETED !!!" << endl << endl;
            }
	  else
            {
              SparrowHawk().log("MISSION COMPLETED !!!\n");
            }
	}
    }
  else if (directiveStatus.status == MContrDirectiveStatus::FAILED)
    {
      // TODO: Determine the new minimum vehicle capability required for Alice to run.
      m_failedContrDirectiveQueue.push_back(m_nextContrDirective);
      m_nextContrDirective.goalID = m_nextContrDirective.goalID + 1;
      m_nextContrDirective.name = MContrDirective::PAUSE;      
    }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool MissionControl::loadMDF(char* fileName, RNDF* rndf)
{
  ifstream file;
  string line;
  string word;

  file.open(fileName, ios::in);

  if(!file)
  {
    cerr << "Error: " << fileName << " file not found." << endl;
    return false;
  } 
  
  getline(file, line);
    
  istringstream lineStream(line, ios::in);
  
  lineStream >> word;

  if(word == "MDF_name")
  {
    parseCheckpoint(&file, rndf);
    file.close();
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::parseCheckpoint(ifstream* file, RNDF* rndf)
{
  string line, word;
  char letter;
  int ckptID;  
  
  while(word != "end_checkpoints")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      ckptID = atoi(word.c_str());
      m_checkpointSequence.push_back(rndf->getCheckpoint(ckptID));
    }
    else
    {
      continue;
    }
  }
}
