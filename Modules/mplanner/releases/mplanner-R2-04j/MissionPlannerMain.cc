/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include <getopt.h>
#include <iostream>
#include <dgcutils/DGCutils.hh>
#include <dgcutils/findroute.h>
#include <sparrowhawk/SparrowHawk.hh>
#include <skynet/skynet.hh>
#include <sys/time.h>
#include <sys/stat.h>
#include <signal.h>
#include "cmdline.h"
#include "CmdArgs.hh"
#include "RoutePlanner.hh"

using namespace std;

extern volatile sig_atomic_t quit;
void sigintHandler(int /*sig*/)
{
  // if the user presses CTRL-C three or more times, and the program still
  // doesn't terminate, abort
  if (quit > 1) {
    abort();
  }
  quit++;
}


int main(int argc, char **argv) 
{
  // catch CTRL-C and exit cleanly, if possible
  signal(SIGINT, sigintHandler);
  // and SIGTERM, i.e. when someone types "kill <pid>" or the like
  signal(SIGTERM, sigintHandler);

  CSparrowHawk *shp = NULL;
  pthread_mutex_t shpMutex;
  DGCcreateMutex(&shpMutex);

  gengetopt_args_info cmdline;
  int sn_key;
  bool noSparrow = false;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }

  // Fill out the spread name
  if (cmdline.spread_daemon_given)
    CmdArgs::spreadDaemon = cmdline.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    CmdArgs::spreadDaemon = getenv("SPREAD_DAEMON");
  else {
    cerr << "unknown Spread daemon: please set SPREAD_DAEMON" << endl;
    exit(1);
  }
  
  /* Figure out what skynet key to use */
  sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;

  /* Figure out if we want to use sparrow display */
  if(cmdline.disable_console_flag || cmdline.nosp_flag)
  {  
    noSparrow = true;
    cout << "NO SPARROW DISPLAY" << endl;
  }

  /* Figure out what RNDF and MDF we want to use */
  char *RNDFFileName = dgcFindRouteFile(cmdline.rndf_arg);
  char *MDFFileName = dgcFindRouteFile(cmdline.mdf_arg);

  cout << "RNDF file: " << RNDFFileName << endl;
  cout << "MDF file: " << MDFFileName << endl;

  /* Figure out where logged data go */
  string logFileName;
  FILE* logFile = NULL;
  if(cmdline.enable_logging_given)
  {
    /* Check if the logs directory exists */
    struct stat st;
    int ret = stat(cmdline.log_path_arg, &st);
    if (errno == ENOENT) {
      if(mkdir(cmdline.log_path_arg, 0755) != 0)
	cerr << __FILE__ << ":" << __LINE__ << "cannot create log dir.";
    }

    string MDFFileNameStr;
    string tmpMDFname;
    ostringstream ossMDF;
    ossMDF << "./" << cmdline.mdf_arg;
    MDFFileNameStr = ossMDF.str();
    tmpMDFname.assign(MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("/")+1, 
		      MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("."));
    ostringstream oss;
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
    oss  << cmdline.log_path_arg << "/mplanner-" << tmpMDFname << "." << timestr << ".log";
    logFileName = oss.str();
    string suffix = "";
    
    // if it exists already, append .1, .2, .3 ... 
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
    logFile = fopen(logFileName.c_str(), "w");
    if (logFile == NULL)
    {
      cerr << "Cannot open log file: " << logFile << endl;
      exit(1);
    }
    cout << "log file: " << logFileName << endl;
    fprintf (logFile, "RNDF file: %s\n", RNDFFileName);
    fprintf (logFile, "MDF file: %s\n\n", MDFFileName);
  }

  /* Get the RNDF object */
  RNDF* rndf = new RNDF();
  if (!rndf->loadFile(RNDFFileName))
  {
    cerr << "ERROR:  Unable to load RNDF file " << RNDFFileName
	 << ", exiting program" << endl;
    if (cmdline.enable_logging_given)
    {
      fprintf( logFile, "ERROR:  Unable to load RNDF file %s, exiting program\n", RNDFFileName);
    }
    exit(1);
  }
  rndf->assignLaneDirection();
    
  if (cmdline.debug_arg > 2)
  {
    rndf->print();
    cout << "Finished rndf" << endl;
  }
  else if (cmdline.debug_arg > 1)
  {
    for (int i=1; i <= rndf->getNumOfSegments(); i++)
    {
      for (int j=1; j <= rndf->getSegment(i)->getNumOfLanes(); j++)
      {
        cout << "Lane " << i << "." << j << ":\t\t"
             << rndf->getLane(i,j)->getDirection() << endl;
      }
    } 
  }
  if (cmdline.enable_logging_given)
  {
    fprintf ( logFile, "\n" );
    for (int i=1; i <= rndf->getNumOfSegments(); i++)
    {
      for (int j=1; j <= rndf->getSegment(i)->getNumOfLanes(); j++)
      {
	fprintf ( logFile, "Lane %d.%d: \t %d\n", i, j, rndf->getLane(i,j)->getDirection() );
      }
    }
    fprintf( logFile, "Finished rndf\n" );
  }
 

  /* Figure out where to start */
  cout << "Start waypoint: " << cmdline.rndf_start_arg << endl;

  if (sscanf(cmdline.rndf_start_arg, "%d.%d.%d", &CmdArgs::startSegmentID, 
	     &CmdArgs::startLaneID, &CmdArgs::startWaypointID) < 3) {
    CmdArgs::startSegmentID = 0;
    CmdArgs::startLaneID = 0;
    CmdArgs::startWaypointID = 0;
    cerr << "Invalid waypoint specification '" << cmdline.rndf_start_arg << "'" 
	 << endl;
  }
  else {
    if (CmdArgs::startSegmentID == 0) {
      cout << "No start waypoint given" << endl;
      CmdArgs::startLaneID = 0;
      CmdArgs::startWaypointID = 0;
      if (cmdline.enable_logging_given)
	fprintf ( logFile, "\n No start waypoint given \n" );
    }
    else if (CmdArgs::startLaneID == 0 ||
	     CmdArgs::startSegmentID > rndf->getNumOfSegments()) {
      CmdArgs::startLaneID = 0;
      CmdArgs::startWaypointID = 0;
      if (CmdArgs::startSegmentID > rndf->getNumOfSegments()) {
	Zone* startZone = rndf->getZone(CmdArgs::startSegmentID);
	if (startZone == NULL) {
	  CmdArgs::startSegmentID = 0;
	  CmdArgs::startLaneID = 0;
	  CmdArgs::startWaypointID = 0;
	  cerr << "Invalid start waypoint" << endl;
	  if (cmdline.enable_logging_given)
	    fprintf ( logFile, "\n Invalid start waypoint \n" );
	}
      }
      else {
	Segment* startSegment = rndf->getSegment(CmdArgs::startSegmentID);
	if (startSegment == NULL) {
	  CmdArgs::startSegmentID = 0;
	  CmdArgs::startLaneID = 0;
	  CmdArgs::startWaypointID = 0;
	  cerr << "Invalid start waypoint" << endl;
	  if (cmdline.enable_logging_given)
	    fprintf ( logFile, "\n Invalid start waypoint \n" );
	}
      }
    }
    else if (CmdArgs::startWaypointID == 0) {
      Lane* startLane = rndf->getLane(CmdArgs::startSegmentID, CmdArgs::startLaneID);
      if (startLane == NULL) {
	CmdArgs::startSegmentID = 0;
	CmdArgs::startLaneID = 0;
	CmdArgs::startWaypointID = 0;
	cerr << "Invalid start waypoint" << endl;
	if (cmdline.enable_logging_given)
	  fprintf ( logFile, "\n Invalid start waypoint \n" );
      }
    }
    else {
      Waypoint* startWaypoint = rndf->getWaypoint(CmdArgs::startSegmentID, CmdArgs::startLaneID,
						  CmdArgs::startWaypointID);
      if (startWaypoint == NULL) {
	CmdArgs::startSegmentID = 0;
	CmdArgs::startLaneID = 0;
	CmdArgs::startWaypointID = 0;
	cerr << "Invalid start waypoint" << endl;
	if (cmdline.enable_logging_given)
	  fprintf ( logFile, "\n Invalid start waypoint \n" );
      }
    }
  }

  cout << "Start waypoint: " << CmdArgs::startSegmentID << "." << CmdArgs::startLaneID 
       << "." << CmdArgs::startWaypointID << endl;


  /* SparrowHawk object */

  if (!noSparrow) {
    CSparrowHawk &sh = SparrowHawk();  
    shp = &sh;
  }

  CmdArgs::sn_key = sn_key;
  CmdArgs::waitForStateFill = !cmdline.nowait_given;
  CmdArgs::listenToMapper = !cmdline.nomap_given;
  CmdArgs::nopause = cmdline.nopause_given;
  CmdArgs::nobackup = cmdline.nobackup_given;
  CmdArgs::noillegalPassing = cmdline.noillegalpassing_given;
  CmdArgs::rndf = rndf;
  CmdArgs::RNDFFileName = RNDFFileName;
  CmdArgs::MDFFileName = MDFFileName;
  CmdArgs::startChute = cmdline.start_chute_given;
  if (cmdline.no_start_chute_given)
    CmdArgs::startChute = false;
  CmdArgs::getApxStatus = cmdline.enable_apx_status_given;
  CmdArgs::getSystemHealth = cmdline.enable_system_health_given;
  CmdArgs::continueMission = cmdline.continue_given;
  CmdArgs::missionFileName = cmdline.mission_file_arg;
  CmdArgs::listenToEstop = cmdline.listen_to_estop_given;
  CmdArgs::keepForwardProgress = cmdline.keep_forward_progress_given;
  CmdArgs::stopTimeout = cmdline.stop_timeout_arg;
  CmdArgs::nosparrow = noSparrow;
  CmdArgs::logData = cmdline.enable_logging_given;
  CmdArgs::logPath = cmdline.log_path_arg;
  CmdArgs::logFile = logFile;
  CmdArgs::debugLevel = cmdline.debug_arg;
  CmdArgs::verbose = cmdline.verbose_given;
  CmdArgs::logLevel = cmdline.log_level_arg;
  CmdArgs::maxSpeed = cmdline.max_speed_arg;

  MissionControl* mcontrol = new MissionControl(CmdArgs::sn_key);
  mcontrol->init(shp, &shpMutex);

  RoutePlanner* rplanner = new RoutePlanner(shp, &shpMutex);

  mcontrol->Start();
  rplanner->Start();

  while (!quit)
  {
    /*
    if (!noSparrow) {
      mcontrol->UpdateSparrowVariablesLoop();
      rplanner->UpdateSparrowVariablesLoop();
    } 
    */ 
    usleep(100000);
  }

  mcontrol->Stop();
  rplanner->Stop();

  if (!noSparrow)
    shp->stop();

  if(cmdline.enable_logging_given)
  {
    fclose(logFile);
  }
  delete rndf;
  DGCdeleteMutex(&shpMutex);

  return 0;
}

