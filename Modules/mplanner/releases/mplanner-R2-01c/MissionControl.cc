/*!**
 * Nok Wongpiromsarn
 * February 21, 2007
 */


#include "MissionControl.hh"
using namespace std;

MissionControl::MissionControl() :
  GcModule( &m_contrStatus, &m_mergedDirective )
{
  m_rndf = NULL;
  m_missionCompleted = false;
}
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MissionControl::~MissionControl() 
{
  // Do nothing
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::init(RNDF* rndf, char* MDFFileName, bool nosparrow, bool logData, FILE* logFile,
			  int debugLevel, bool verbose)
{
  m_rndf = rndf;
  m_MDFFileName = MDFFileName;
  m_nosparrow = nosparrow;
  m_logData = logData;
  m_logFile = logFile;
  DEBUG_LEVEL = debugLevel; 
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }
  arbitrate(&m_contrStatus, &m_mergedDirective);
  control(m_mergedDirective.checkpointSequence);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MContrDirective MissionControl::getNextDirective()
{
  if (m_contrDirectiveQueue.size() == 0)
    {
      control(m_mergedDirective.checkpointSequence);
    }
  m_contrDirectiveQueue.back().status = Strategy::ACCEPTED;
  return strategy2ContrDirective(m_contrDirectiveQueue.back(), m_mergedDirective.checkpointSequence);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
int MissionControl::getNextCheckpointIndex()
{
  return m_nextCheckpointIndex;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl:: updateDirectiveStatus(MControlDirectiveResponse directiveStatus)
{
  m_directiveStatus = directiveStatus;
  // control((ControlStatus*) &m_contrStatus, (MergedDirective*) &m_mergedDirective);
  control(&m_contrStatus, &m_mergedDirective);
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  //  MissionControlStatus* contrStatus = dynamic_cast<MissionControlStatus*>(cs);
  MissionControlMergedDirective* mergedDirective = dynamic_cast<MissionControlMergedDirective*>(md);

  if (!loadMDF(m_MDFFileName, m_rndf, mergedDirective))
  {
    cerr << "Error: Unable to load MDF file " << m_MDFFileName << ", exiting program"
         << endl;
    #warning "mplanner will quit if MDF is not valid. Should instead put vehicle in pause."
    if ( m_logData )
    {
      fprintf (m_logFile, "Error: Unable to load MDF file %s, exiting program\n", m_MDFFileName );
    }
    exit(1);
  }
  m_nextCheckpointIndex = 0;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::control(vector<Waypoint*> checkpointSequence)
{
  Strategy nextStrategy;
  if(unsigned(m_nextCheckpointIndex) < checkpointSequence.size())
    {
      if (m_contrDirectiveQueue.size() > 0)
	{
	  nextStrategy.goalID = m_contrDirectiveQueue.back().goalID + 1;
	}
      else
	{
	  nextStrategy.goalID = 1;
	}
      nextStrategy.name = NEXT_CHECKPOINT;
      nextStrategy.nextCheckpointIndex = m_nextCheckpointIndex;
      nextStrategy.vehicleCap = MAX_CAP;
      nextStrategy.status = Strategy::SENT;
      m_contrDirectiveQueue.push_back(nextStrategy);
    }
  else // We complete the mission
    {
      if (m_contrDirectiveQueue.size() > 0)
	{
	  nextStrategy.goalID = m_contrDirectiveQueue.back().goalID + 1;
	}
      else
	{
	  nextStrategy.goalID = 1;
	}
      nextStrategy.name = END_OF_MISSION;
      nextStrategy.status = Strategy::SENT;
      m_contrDirectiveQueue.push_back(nextStrategy);
    }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::control(ControlStatus* cs, MergedDirective* md)
{
  // MissionControlStatus* contrStatus = dynamic_cast<MissionControlStatus*> (cs);
  MissionControlMergedDirective* mergedDirective = dynamic_cast<MissionControlMergedDirective*>(md);

  if (m_contrDirectiveQueue.size() == 0 ||
      m_directiveStatus.status == MControlDirectiveResponse::EXECUTING)
    {
      return;
    }

  unsigned directiveIndex = 0;
  if (m_directiveStatus.goalID < m_contrDirectiveQueue.front().goalID ||
      m_directiveStatus.goalID > m_contrDirectiveQueue.back().goalID )
    {
      cerr << "ERROR: MissionControl::control : Status goalID = " 
	   << m_directiveStatus.goalID 
	   << " Stored control directive goalID are between "
	   << m_contrDirectiveQueue.front().goalID << " and "
	   << m_contrDirectiveQueue.back().goalID << endl;
      if (m_logData)
      {
	fprintf (m_logFile, "ERROR: MissionControl::control : Status goalID = %d",
		 m_directiveStatus.goalID );
	fprintf (m_logFile,   " Stored control directive goalID are between %d and %d\n",
		 m_contrDirectiveQueue.front().goalID,  m_contrDirectiveQueue.back().goalID);
      }
    }
  else
    {
      while(directiveIndex < m_contrDirectiveQueue.size() && 
	    m_directiveStatus.goalID > m_contrDirectiveQueue[directiveIndex].goalID)
	{
	  directiveIndex++;
	}
    }

  if (m_directiveStatus.status == MControlDirectiveResponse::COMPLETED)
    {
      if (m_contrDirectiveQueue[directiveIndex].name == NEXT_CHECKPOINT )
	{
          if (m_nosparrow)
	    {
	      cout << endl << "checkpoint " << m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1 
		   << " (Waypoint " ;
	      mergedDirective->checkpointSequence[m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex]->print();
	      cout << ") is crossed" << endl << endl;
	    }
          else
	    {
	      SparrowHawk().log("checkpoint %d (Waypoint %d.%d.%d) is crossed\n",
	          m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1,
		  mergedDirective->checkpointSequence[m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex]->getSegmentID(), 
		  mergedDirective->checkpointSequence[m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex]->getLaneID(), 
		  mergedDirective->checkpointSequence[m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex]->getWaypointID());
	    }
	  if (m_logData)
	  {
	      fprintf (m_logFile, "checkpoint %d (Waypoint %d.%d.%d) is crossed\n",
	          m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1,
		  mergedDirective->checkpointSequence[m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex]->getSegmentID(), 
		  mergedDirective->checkpointSequence[m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex]->getLaneID(), 
		  mergedDirective->checkpointSequence[m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex]->getWaypointID());
	  }

	  if (m_nextCheckpointIndex != m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1)
	  {
	    m_nextCheckpointIndex = m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1;
	    control(mergedDirective->checkpointSequence);
	  }

	  while(m_contrDirectiveQueue.front().goalID <= m_directiveStatus.goalID)
	    {
	      if (m_contrDirectiveQueue[0].name == NEXT_CHECKPOINT && 
		  m_contrDirectiveQueue[0].nextCheckpointIndex != 
		  m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex )
	      {
		cerr << "ERROR: MissionControl::control : checkpoint "
		     << m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1 << " is crossed before "
		     << m_contrDirectiveQueue[0].nextCheckpointIndex + 1
		     << ". We will go back to checkpoint " 
		     << m_contrDirectiveQueue[0].nextCheckpointIndex + 1 << endl;
		if (m_logData)
		{
		  fprintf (m_logFile, "ERROR: MissionControl::control : checkpoint %d", 
			   m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1);
		  fprintf (m_logFile, " is crossed before %d. We will go back to checkpoint %d\n", 
			   m_contrDirectiveQueue[0].nextCheckpointIndex + 1,
			   m_contrDirectiveQueue[0].nextCheckpointIndex + 1);
		}
		m_nextCheckpointIndex = m_contrDirectiveQueue[0].nextCheckpointIndex;
		m_contrDirectiveQueue.clear();
		control(mergedDirective->checkpointSequence);
		break;
	      }
	      m_contrDirectiveQueue.pop_front();
	    }
	}
      else if (m_contrDirectiveQueue[directiveIndex].name == PAUSE ) 
	{
	  unsigned i = 0;
	  VehicleCap failedVehicleCap = MAX_CAP;
	  while (i <= directiveIndex)
	    {
	      if (m_contrDirectiveQueue[directiveIndex].name == NEXT_CHECKPOINT)
		{
		  failedVehicleCap = m_contrDirectiveQueue[i].vehicleCap;
		  m_nextCheckpointIndex = m_contrDirectiveQueue[i].nextCheckpointIndex;
		  break;
		}
	      i++;
	    }
	  control(mergedDirective->checkpointSequence);
	  if (failedVehicleCap == MAX_CAP)
	    {
	      m_contrDirectiveQueue.back().vehicleCap = MEDIUM_CAP;
	    }
	  else
	    {
	      m_contrDirectiveQueue.back().vehicleCap = MIN_CAP;
	    }
	}
      else // END_OF_MISSION
	{
	  #warning Misson Control does not handle the end of mission yet.
	  if (!m_missionCompleted)
	    {
	      m_missionCompleted = true;
	      if (m_nosparrow)
		{
		  cout << endl << "MISSION COMPLETED !!!" << endl << endl;
		}
	      else
		{
		  SparrowHawk().log("MISSION COMPLETED !!!\n");
		}
	      if (m_logData)
	      {
		fprintf(m_logFile, "MISSION COMPLETED !!!\n");
	      }
	    }
	}
    }
  else if (m_directiveStatus.status == MControlDirectiveResponse::FAILED ||
	   m_directiveStatus.status == MControlDirectiveResponse::REJECTED )
    {
      if (m_contrDirectiveQueue[directiveIndex].name == NEXT_CHECKPOINT )
	{
	  Strategy nextStrategy;
	  if (m_contrDirectiveQueue.size() > 0)
	    {
	      nextStrategy.goalID = m_contrDirectiveQueue.back().goalID + 1;
	    }
	  else
	    {
	      nextStrategy.goalID = 1;
	    }
	  nextStrategy.name = PAUSE;
	  nextStrategy.vehicleCap = MIN_CAP;
	  nextStrategy.status = Strategy::SENT;
	  m_contrDirectiveQueue.push_back(nextStrategy);
	}
      else
	{
	  //TODO: Command estop
	}
    }
  else // READY_FOR_NEXT
    {
      if (m_contrDirectiveQueue[directiveIndex].name == NEXT_CHECKPOINT &&
	  m_nextCheckpointIndex != m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1)
	{
	  m_nextCheckpointIndex = m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1;
	  control(mergedDirective->checkpointSequence);
	}
    }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool MissionControl::loadMDF(char* fileName, RNDF* rndf, MissionControlMergedDirective* mergedDirective)
{
  ifstream file;
  string line;
  string word;

  file.open(fileName, ios::in);

  if(!file)
  {
    cerr << "Error: " << fileName << " file not found." << endl;
    return false;
  } 
  
  getline(file, line);
    
  istringstream lineStream(line, ios::in);
  
  lineStream >> word;

  if(word == "MDF_name")
  {
    parseCheckpoint(&file, rndf, mergedDirective);
    file.close();
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::parseCheckpoint(ifstream* file, RNDF* rndf, MissionControlMergedDirective* mergedDirective)
{
  string line, word;
  char letter;
  int ckptID;  
  
  while(word != "end_checkpoints")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      ckptID = atoi(word.c_str());
      mergedDirective->checkpointSequence.push_back(rndf->getCheckpoint(ckptID));
    }
    else
    {
      continue;
    }
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MContrDirective MissionControl::strategy2ContrDirective(Strategy strategy, vector<Waypoint*> checkpointSequence)
{
  MContrDirective directive;
  directive.goalID = strategy.goalID;
  directive.name = strategy.name;
  if (strategy.nextCheckpointIndex < 0 || 
      unsigned(strategy.nextCheckpointIndex) >= checkpointSequence.size() )
    {
      cerr << "ERROR: MissionControl::strategy2ContrDirective : invalid nextCheckpointIndex = "
	   << strategy.nextCheckpointIndex << endl;
      if (m_logData)
      {
	fprintf (m_logFile, "ERROR: MissionControl::strategy2ContrDirective : invalid nextCheckpointIndex = %d\n",
		 strategy.nextCheckpointIndex);
      }
      strategy.nextCheckpointIndex = checkpointSequence.size() - 1;
    }
  directive.segmentID = checkpointSequence[strategy.nextCheckpointIndex]->getSegmentID();
  directive.laneID = checkpointSequence[strategy.nextCheckpointIndex]->getLaneID();
  directive.waypointID = checkpointSequence[strategy.nextCheckpointIndex]->getWaypointID();
  return directive;
}
