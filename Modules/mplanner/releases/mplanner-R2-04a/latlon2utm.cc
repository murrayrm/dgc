#include <iostream>
#include "dgcutils/ggis.h"

using namespace std;

int main(int argc, char **argv) 
{  
  GisCoordLatLon latlon;
  GisCoordUTM utm;
  utm.zone = 5;
  utm.letter = 'S';
  double northing, easting, lat, lon, x, y;
  double xoffset = 0;
  double yoffset = 0;

  while (true) {
    cout << "Latitude: ";
    cin >> lat;
    cout << "Longitude: ";
    cin >> lon;
    latlon.latitude = lat;
    latlon.longitude = lon;
    gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
    northing = utm.n;
    easting = utm.e;
    cout << "Northing: " << northing << "\tEasting: " << easting << endl;
    cout << "x: " << northing - xoffset << "\ty: " << easting - yoffset << endl;
    cout << "zone = " << utm.zone << endl;
  }
}
