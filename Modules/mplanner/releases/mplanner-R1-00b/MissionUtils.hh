/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef MISSIONUTILS
#define MISSIONUTILS

#include <iostream>
#include <string>
#include "rndf/RNDF.hh"
#include "Graph.hh"
#include <vector>
#include <list>
#include <math.h>
#include "interfaces/SegGoals.hh"
using namespace std;

/*! missionUtils contains functions which are useful for mission planner.
 * \brief missionUtils contains functions which are useful for mission planner.
 */

/*! Assign direction to all the lanes */
void assignLaneDirection(RNDF*);


/*! Add the following vertices to rndfGraph
 * exit points
 * entry points
 * checkpoints
 * entry perimeter points
 * exit perimeter points
 * parking spot waypoints */
void addAllVertices(RNDF*, Graph*);


/*! Add the following edges to rndfGraph
 * exit point -> entry points
 * entry point -> closest exit points
 * closest entry points -> checkpoint
 * checkpoint -> closest exit points
 * parking spot -> exit points
 * entry perimeter point -> exit perimeter points
 * entry perimenter point -> parking spots */
void addAllEdges(RNDF*, Graph*);


/*! Add edges from the specified entry point to its closest exit points
 * (both on the same lane and adjacent lanes). */
void addEdgesFromEntry(RNDF*, Graph*, GPSPoint*);


/*! Add edges from the specified entry perimeter point to all its exit perimeter points and
 * to all the checkpoints in the zone. */
void addEdgesFromEntryPerimeter(RNDF*, Graph*, GPSPoint*);


/*! Compute the optimal route from vertex1 to vertex2 in rndfGraph using Dijkstra's
 * algorithm */
bool findRoute(Vertex*, Vertex*, RNDF*, Graph*, vector<Vertex*>&, double&);


/*! Determine the segment-level goals from route. */
vector<SegGoals> findSegGoals(vector<Vertex*>, RNDF*, Graph*);
vector<SegGoals> findSegGoals(vector<Vertex*>, RNDF*, Graph*, int);


/*! Determine the type of a segment from vertex1 to vertex2. possible types are
 * ROAD_SEGMENT
 * PARKING_ZONE
 * INTERSECTION
 * PREZONE */
SegmentType findType(Vertex*, Vertex*, RNDF*);


/*! Find the closest waypoint or perimeter point on the specified segment and lane.
 * If segmentID = 0, then find the closest waypoint on any segment. If laneID = 0,
 * then find the closest waypoint on the specified segment on any lane. */
GPSPoint* findClosestGPSPoint(double, double, int, int, double&, RNDF*);


/*! Add a vertex corresponding to the specified GPSPoint (can be waypoint or perimeter point)
 * to the graph with appropriate edges */
bool addGPSPointToGraph (GPSPoint*, Graph*, RNDF*);


/*! Find all the exit points that have entry as one of their associated entry points. */
vector<Vertex*> findAllExits(Vertex*, RNDF*, Graph*);


/*! Print out the sequence of segment-level goals. */
void printMission(vector<SegGoals>);
void printMission(list<SegGoals>, int);


/*! Remove vertex from vertices */
bool removeVertexFromVertices(vector<Vertex*>&, Vertex*);


/*! Compute the distance between two GPSPoints */
double computeDistance(double, double, double, double);


/*! Other useful helper functions */
double avgSpeed(int, RNDF*);
double avgSpeed(Segment*);
bool adjacentSameDirection(Lane*, Lane*);
vector<Lane*> getAdjacentLanes(Lane*, RNDF*);
vector<Lane*> getNonAdjacentLanes(Lane*, RNDF*);
double square(double);
double min(double, double);


#endif // MISSIONUTILS
