/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include "MissionUtils.hh"
#define DEBUG_LEVEL 0
#define DEFAULT_MAX_SPEED 10.0f
#define INTERSECTION_NO_STOP_MAX_SPEED 8.0f
#define INTERSECTION_STOP_MAX_SPEED 10.0f


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Compute the optimal route from vertex1 to vertex2 in travGraph using Dijkstra's algorithm
//-------------------------------------------------------------------------------------------
bool findRoute(Vertex* vertex1, Vertex* vertex2, Graph* travGraph,
	       vector<Vertex*> &route, double &cost, bool removeIntWaypoints)
{
  vector<Vertex*> unvisitedVertices = travGraph->getVertices();
  vector<Vertex*> visitedVertices;
  Vertex* cheapestVertex;
  int cheapestVertexIndex = -1; // for debugging
  vector<Edge*> edgesFromCheapestVertex;
  float minCostToCome = INFINITE_COST_TO_COME;

  route.clear();

  for(unsigned i = 0; i < unvisitedVertices.size(); i++)
  {
    unvisitedVertices[i]->setCostToCome(INFINITE_COST_TO_COME);
    unvisitedVertices[i]->setPreviousVertex(NULL);
  }

  vertex1->setCostToCome(0);

  bool routeFound = false;

  while(!routeFound && unvisitedVertices.size() > 0)
  {
    // Find the vertex with minimum cost to come
    cheapestVertex = unvisitedVertices[0];
    minCostToCome = cheapestVertex->getCostToCome();
    for(unsigned i = 1; i < unvisitedVertices.size(); i++)
    {
      if (unvisitedVertices[i]->getCostToCome() < minCostToCome)
      {
        cheapestVertex = unvisitedVertices[i];
	cheapestVertexIndex = i;
        minCostToCome = cheapestVertex->getCostToCome();
      }
    }

    // Add the vertex with minimum cost to come to visitedVertices.
    visitedVertices.push_back(cheapestVertex);
    removeVertexFromVertices(unvisitedVertices, cheapestVertex);
    if (cheapestVertex == vertex2)
    {
      routeFound = true;
    }
    else
    {
      // Update cost to come
      edgesFromCheapestVertex = cheapestVertex->getEdges();
      for(unsigned i = 0; i < edgesFromCheapestVertex.size(); i++)
      {
	float currentCost = edgesFromCheapestVertex[i]->getNext()->getCostToCome();
        float newCost = cheapestVertex->getCostToCome() +
            edgesFromCheapestVertex[i]->getWeight();
        if (currentCost > newCost)
        {
          edgesFromCheapestVertex[i]->getNext()->setCostToCome(newCost);
          edgesFromCheapestVertex[i]->getNext()->setPreviousVertex(cheapestVertex);
        }
      }
    }
  }

  cost = vertex2->getCostToCome();

  if(vertex2->getCostToCome() >= INFINITE_COST_TO_COME)
  {
    routeFound = false;
  }
  else
  {
    Vertex* currentVertex = vertex2;
    Edge* nextEdge = NULL;
    Edge* currentEdge = NULL;
    vector<Vertex*> reverseRoute;
    bool vertex1Found = false;
    while (!vertex1Found)
    {
      /*
      int currentSegmentID = currentVertex->getSegmentID();
      int currentLaneID = currentVertex->getLaneID();
      int currentWaypointID = currentVertex->getWaypointID();

      GPSPoint* current;
      Waypoint* currentWaypoint;
      if(currentLaneID == 0)
      {
        current = rndf->getPerimeterPoint(currentSegmentID, currentWaypointID);
      }
      else
      {
        current = rndf->getWaypoint(currentSegmentID, currentLaneID, currentWaypointID);
        currentWaypoint = rndf->getWaypoint(currentSegmentID, currentLaneID, currentWaypointID);
      }
      */
      
      Vertex* previousVertex = currentVertex->getPreviousVertex();
      nextEdge = currentEdge;
      currentEdge = travGraph->getEdge(previousVertex, currentVertex);

      if (!removeIntWaypoints || currentVertex == vertex1 || currentVertex == vertex2 || currentVertex->isEntry() || 
          currentVertex->isExit() || currentVertex->getPreviousVertex()->getSegmentID() == 0 ||
	  (currentEdge != NULL && (currentEdge->getType() == Edge::UTURN || currentEdge->getType() == Edge::KTURN)) ||
	  (nextEdge != NULL && (nextEdge->getType() == Edge::UTURN || nextEdge->getType() == Edge::KTURN)) )
      {
        reverseRoute.push_back(currentVertex);
      }

      if (currentVertex == vertex1 || (currentVertex->getSegmentID() == vertex1->getSegmentID() &&
				       currentVertex->getLaneID() == vertex1->getLaneID() && 
				       currentVertex->getWaypointID() == vertex1->getWaypointID()) )
      {
        vertex1Found = true;
	if (currentVertex != vertex1)
	  cerr << "Multiple vertex " << currentVertex->getSegmentID() << "." << currentVertex->getLaneID()
	       << "." << currentVertex->getWaypointID() << " in the graph" << endl;
      }
      currentVertex = currentVertex->getPreviousVertex();
    }

    for(int i = reverseRoute.size()-1; i >= 0; i--)
    {
      route.push_back(reverseRoute[i]);
    }
  }

  return routeFound;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Determine the segment-level goals from route.
//-------------------------------------------------------------------------------------------
vector<SegGoals> findSegGoals(vector<Vertex*> route, Graph* travGraph,
    vector<double> minSpeedLimits, vector<double> maxSpeedLimits, int firstSegGoalsID,
    double maxSpeed)
{
  vector<SegGoals> segGoals;
  double minSpeedLimit1, minSpeedLimit2, maxSpeedLimit1, maxSpeedLimit2;
  minSpeedLimit1 = 0;
  maxSpeedLimit1 = DEFAULT_MAX_SPEED;
  minSpeedLimit2 = 0;
  maxSpeedLimit2 = DEFAULT_MAX_SPEED;

  if (route.size() <= 1)
    return segGoals;

  for(unsigned i = 0; i < route.size()-1; i++)
  {
    int entrySegmentID = route[i]->getSegmentID();
    int entryLaneID = route[i]->getLaneID();
    int entryWaypointID = route[i]->getWaypointID();
    int exitSegmentID = route[i+1]->getSegmentID();
    int exitLaneID = route[i+1]->getLaneID();
    int exitWaypointID = route[i+1]->getWaypointID();
    SegGoals currentSegGoals;

    /*
      GPSPoint* entryPoint;
      GPSPoint* exitPoint;
      if(entryLaneID == 0)
      entryPoint = rndf->getPerimeterPoint(entrySegmentID, entryWaypointID);
      else
      entryPoint = rndf->getWaypoint(entrySegmentID, entryLaneID, entryWaypointID);
      if(exitLaneID == 0)
      exitPoint = rndf->getPerimeterPoint(exitSegmentID, exitWaypointID);
      else
      exitPoint = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID);
    */
      
    currentSegGoals.goalID = firstSegGoalsID + i;
    currentSegGoals.entrySegmentID = entrySegmentID;
    currentSegGoals.entryLaneID = entryLaneID;
    currentSegGoals.entryWaypointID = entryWaypointID;
    currentSegGoals.exitSegmentID = exitSegmentID;
    currentSegGoals.exitLaneID = exitLaneID;
    currentSegGoals.exitWaypointID = exitWaypointID;

    Edge* edge = travGraph->getEdge(route[i], route[i+1]);

    // First get the type
    if (edge != NULL)
    {
      if (edge->getType() == Edge::INTERSECTION_LEFT_TURN ||
	  edge->getType() == Edge::INTERSECTION_RIGHT_TURN ||
	  edge->getType() == Edge::INTERSECTION_STRAIGHT)
      {
	currentSegGoals.segment_type = SegGoals::INTERSECTION;
	if (edge->getType() == Edge::INTERSECTION_LEFT_TURN)
	  currentSegGoals.intersection_type = SegGoals::INTERSECTION_LEFT;
	else if (edge->getType() == Edge::INTERSECTION_RIGHT_TURN)
	  currentSegGoals.intersection_type = SegGoals::INTERSECTION_RIGHT;
	else
	  currentSegGoals.intersection_type = SegGoals::INTERSECTION_STRAIGHT;
      }
      else if (edge->getType() == Edge::ROAD_SEGMENT)	 
      {
	currentSegGoals.segment_type = SegGoals::ROAD_SEGMENT;
      }
      else if (edge->getType() == Edge::UTURN ||
	       edge->getType() == Edge::KTURN)
      {
	currentSegGoals.segment_type = SegGoals::SegGoals::UTURN;
	if (segGoals.size() > 0) {
	  segGoals.back().stopAtExit = true;
	}
      }
      else if (edge->getType() == Edge::ZONE)
      {
	currentSegGoals.segment_type = SegGoals::PARKING_ZONE;
      }
      else if (edge->getType() == Edge::FIRST_SEGMENT)
      {
	currentSegGoals.segment_type = findType(route[i], route[i+1], travGraph);
      }
      else
      {
	currentSegGoals.segment_type = SegGoals::UNKNOWN;
      }
    }
    else
    {
      currentSegGoals.segment_type = findType(route[i], route[i+1], travGraph);
      currentSegGoals.intersection_type = SegGoals::INTERSECTION_STRAIGHT;
    }

    // Check whether it's sparse
    if (currentSegGoals.segment_type == SegGoals::ROAD_SEGMENT) {
      if (entrySegmentID != 0)
	currentSegGoals.isSparse = travGraph->isSparse(route[i], route[i+1]);
      else if (exitWaypointID > 1) {
	Vertex entryVertex(exitSegmentID, exitLaneID, exitWaypointID - 1);
	currentSegGoals.isSparse = travGraph->isSparse(&entryVertex, route[i+1]);
      }
    }
    else if (currentSegGoals.segment_type == SegGoals::PARKING_ZONE &&
	     entryLaneID == 0 && exitLaneID == 0) {
      Vertex entryVertex(exitSegmentID, exitLaneID, 1);
      Vertex exitVertex(exitSegmentID, exitLaneID, 3);
      currentSegGoals.isSparse = travGraph->isSparse(&entryVertex, &exitVertex);
    }

    // Get the speed limits
    if (entrySegmentID > 0 && (unsigned)entrySegmentID < minSpeedLimits.size())
    {
      minSpeedLimit1 = minSpeedLimits[entrySegmentID];
      maxSpeedLimit1 = maxSpeedLimits[entrySegmentID];
    }
    else
    {
      minSpeedLimit1 = 0;
      maxSpeedLimit1 = DEFAULT_MAX_SPEED;
    }

    if ((unsigned)exitSegmentID < minSpeedLimits.size())
    {
      minSpeedLimit2 = minSpeedLimits[exitSegmentID];
      maxSpeedLimit2 = maxSpeedLimits[exitSegmentID];
    }
    else
    {
      minSpeedLimit1 = 0;
      maxSpeedLimit1 = DEFAULT_MAX_SPEED;
    }

    if (entrySegmentID == 0)
    {
      minSpeedLimit1 = minSpeedLimit2;
      maxSpeedLimit1 = maxSpeedLimit2;
    }

    if (currentSegGoals.segment_type == SegGoals::ROAD_SEGMENT ||
	currentSegGoals.segment_type == SegGoals::PARKING_ZONE)
    {
      currentSegGoals.minSpeedLimit = min(minSpeedLimit1, minSpeedLimit2);
      currentSegGoals.maxSpeedLimit = min(maxSpeedLimit1, maxSpeedLimit2);  
    }
    else if (currentSegGoals.segment_type == SegGoals::INTERSECTION)
    {
      currentSegGoals.minSpeedLimit = 0;
      currentSegGoals.maxSpeedLimit = INTERSECTION_NO_STOP_MAX_SPEED;
      if (route[i]->isStopSign())
	currentSegGoals.maxSpeedLimit = INTERSECTION_STOP_MAX_SPEED;
    }
    else
    {
      currentSegGoals.minSpeedLimit = minSpeedLimits[0];
      currentSegGoals.maxSpeedLimit = maxSpeedLimits[0];
    }    

    if (currentSegGoals.maxSpeedLimit > maxSpeed)
      currentSegGoals.maxSpeedLimit = maxSpeed;

    if (currentSegGoals.minSpeedLimit > currentSegGoals.maxSpeedLimit)
      currentSegGoals.minSpeedLimit = currentSegGoals.maxSpeedLimit;

    // Determine whether illegal passing is allowed
    currentSegGoals.illegalPassingAllowed = false;
    if (edge != NULL && edge->getObstructedLevel() != 0)
    {
      currentSegGoals.illegalPassingAllowed = true;
    }
    else if (edge == NULL)
    {
      vector<Vertex*> tmpRoute;
      double cost;
      bool tmpRouteFound = findRoute(route[i], route[i+1], travGraph, tmpRoute, cost, false);
      if (!tmpRouteFound) {
	cerr << "Can't find edges between " << entrySegmentID << "." << entryLaneID << "."
	     << "." << entryWaypointID << " and " << exitSegmentID << "."
	     << "." << exitLaneID << "." << exitWaypointID << endl;
      }
      else {
	for (unsigned j = 0; j < tmpRoute.size() - 1; j++) {
	  Edge* tmpEdge = travGraph->getEdge(tmpRoute[j], tmpRoute[j+1]);
	  if (tmpEdge == NULL) {
	    cerr << "No edge between " << tmpRoute[j]->getSegmentID()
		 << "." << tmpRoute[j]->getLaneID() << "." 
		 << tmpRoute[j]->getWaypointID() << " and "
		 << tmpRoute[j+1]->getSegmentID() << "."
		 << tmpRoute[j+1]->getLaneID() << tmpRoute[j+1]->getWaypointID() << endl;
	  }
	  else if (tmpEdge->getObstructedLevel() != 0 && currentSegGoals.segment_type == SegGoals::ROAD_SEGMENT) {
	    currentSegGoals.illegalPassingAllowed = true;
	  }
	}
      }
    }

    // Determine whether we have to stop at exit or have to cross a checkpoint at the end
    currentSegGoals.stopAtExit = false;
    currentSegGoals.isExitCheckpoint = false;
    if (currentSegGoals.segment_type == SegGoals::ROAD_SEGMENT || 
	currentSegGoals.segment_type == SegGoals::PARKING_ZONE)
    {
      if (route[i+1]->isStopSign())
      {
	currentSegGoals.stopAtExit = true;
      }
    }
    segGoals.push_back(currentSegGoals);
  }

  return segGoals;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Determine the type of a segment from vertex1 to vertex2. possible types are
// * ROAD_SEGMENT
// * PARKING_ZONE
// * INTERSECTION
// * PREZONE
//-------------------------------------------------------------------------------------------
SegGoals::SegmentType findType(Vertex* vertex1, Vertex* vertex2, Graph* travGraph)
{
  int vertex1SegmentID = vertex1->getSegmentID();
  int vertex1LaneID = vertex1->getLaneID();
  int vertex1WaypointID = vertex1->getWaypointID();
  int vertex2SegmentID = vertex2->getSegmentID();
  int vertex2LaneID = vertex2->getLaneID();
  int numofSegments = travGraph->getNumOfSegments();
  
  /* The case where vertex1 represents the current position */
  if(vertex1SegmentID == 0 && vertex1LaneID == 0 && vertex1WaypointID == 0)
  {
    if (vertex2SegmentID > numofSegments)
      return SegGoals::PARKING_ZONE;
    else
      return SegGoals::ROAD_SEGMENT;
  }

  /* The case where vertex1 represents a perimeter point .
     The only possible types are
     * PARKING_ZONE (entry perimeter point -> parking spot checkpoint OR
                     entry perimeter point -> exit perimeter point)
     * ROAD_SEGMENT (exit perimeter point -> entry point) */
  if(vertex1LaneID == 0)
  {
    if (vertex1->isEntry())
    {
      if (vertex2LaneID == 0 && vertex2->isExit())
      {
	return SegGoals::PARKING_ZONE;
      }
      else if (vertex2SegmentID > numofSegments && vertex2->isCheckpoint())
      {
	return SegGoals::PARKING_ZONE;
      }
    }
    else if (vertex1->isExit() && vertex2SegmentID <= numofSegments && vertex2->isEntry())
    {
      return SegGoals::ROAD_SEGMENT;
    }
  }

  /* The case where vertex1 represents a parking spot
     The only posible type is PARKING_ZONE
     (parking spot checkpoint -> exit perimeter point) */
  else if(vertex1SegmentID > numofSegments && vertex1->isCheckpoint() && vertex2LaneID == 0 &&
	  vertex2->isExit())
  {
    return SegGoals::PARKING_ZONE;
  }

  /* The case where vertex1 represents an entry, exit, checkpoint waypoint,
   * or a regular waypoint. The only possible types are
   * ROAD_SEGMENT
   *  (entry -> exit OR entry -> checkpoint OR
   *  checkpoint -> exit OR regular waypoint -> exit OR regular waypoint -> checkpoint)
   * PREZONE (exit -> entry perimeter point)
   * INTERSECTION (exit -> entry OR exit -> regular waypoint)
   * Note that the order of the if-else statement here is important because a checkpoint
   * can also be an entry or exit point */
  else
  {
    if (vertex1->isEntry())
    {
      if ((vertex2SegmentID <= numofSegments && (vertex2->isExit() || vertex2->isCheckpoint())) ||
	  (vertex1SegmentID == vertex2SegmentID && vertex1LaneID == vertex2LaneID))
      {
	return SegGoals::ROAD_SEGMENT;
      }
    }
    else if (vertex1->isExit())
    {
      if (vertex2LaneID == 0 && vertex2->isEntry())
      {
	return SegGoals::PREZONE;
      }
      else if (vertex2SegmentID <= numofSegments)
      {
        // Waypoint* segmentExit =
        //     rndf->getWaypoint(vertex2SegmentID, vertex2LaneID, vertex2WaypointID);
        // if(segmentExit->isEntry())
        return SegGoals::INTERSECTION;
      }
    }
    else // if (vertex1->isCheckpoint())
    {
      if ((vertex2SegmentID <= numofSegments && vertex2->isExit() || vertex2->isCheckpoint()) ||
	  (vertex1SegmentID == vertex1SegmentID && vertex1LaneID == vertex2LaneID))
      {
	return SegGoals::ROAD_SEGMENT;
      }
    }
  }
  return SegGoals::UNKNOWN;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Find all the exit points that have entry as one of their associated entry points.
//-------------------------------------------------------------------------------------------
vector<Vertex*> findAllExits(Vertex* entryVertex, RNDF* rndf, Graph* travGraph)
{
  vector<Vertex*> vertices = travGraph->getVertices();
  vector<Vertex*> exitVertices;
  int entrySegmentID = entryVertex->getSegmentID();
  int entryLaneID = entryVertex->getLaneID();
  int entryWaypointID = entryVertex->getWaypointID();
  GPSPoint* entry;
  if(entryLaneID == 0)
  {
    entry = rndf->getPerimeterPoint(entrySegmentID, entryWaypointID);
  }
  else
  {
    entry = rndf->getWaypoint(entrySegmentID, entryLaneID, entryWaypointID);
  }
  
  if(!entry->isEntry())
  {
    return exitVertices;
  }

  int exitSegmentID;
  int exitLaneID;
  int exitWaypointID;
  GPSPoint* exit;
  for(unsigned i=0; i<vertices.size(); i++)
  {
    Vertex* exitVertex = vertices[i];
    exitSegmentID = exitVertex->getSegmentID();
    exitLaneID = exitVertex->getLaneID();
    exitWaypointID = exitVertex->getWaypointID();
    if(exitLaneID == 0)
    {
      exit = rndf->getPerimeterPoint(exitSegmentID, exitWaypointID);
    }
    else
    {
      exit = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID);
    }
    vector<GPSPoint*> allEntryPoints = exit->getEntryPoints();
    for(unsigned j=0; j<allEntryPoints.size(); j++)
    {
      if(allEntryPoints[j] == entry)
      {
        exitVertices.push_back(exitVertex);
      }
    }
  }
  return exitVertices;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Print out the sequence of segment-level goals.
//-------------------------------------------------------------------------------------------
void printMission(vector<SegGoals> segGoals)
{
  for (unsigned i = 0; i < segGoals.size(); i++)
  {
    segGoals[i].print();
    cout<<endl;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Print out the sequence of segment-level goals.
//-------------------------------------------------------------------------------------------
void printMission(deque<SegGoals> segGoals, int startIndex)
{
  int i = 0;
  while (segGoals.size() > 0 && i < startIndex)
  {
    segGoals.pop_front();
    i++;
  }

  while (segGoals.size() > 0)
  {
    (segGoals.front()).print();
    segGoals.pop_front();
    cout<<endl;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Remove vertex from vertices
//-------------------------------------------------------------------------------------------
bool removeVertexFromVertices(vector<Vertex*>& vertices, Vertex* vertex)
{
  unsigned i = 0;
  bool vertexFound = false;
  while(i < vertices.size())
  {
    if(vertices[i] != vertex)
      i++;
    else
    {
      vertices.erase(vertices.begin()+i,vertices.begin()+i+1);
      vertexFound = true;
    }
  }
  return vertexFound;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Compute the distance between two GPSPoints
//-------------------------------------------------------------------------------------------
double computeDistance(double northing1, double easting1, double northing2, double easting2)
{
  return  sqrt(square(easting2 - easting1) + square(northing2 - northing1));
}


double avgSpeed(int segmentID, RNDF* rndf)
{
  return avgSpeed(rndf->getSegment(segmentID));
}

double avgSpeed(Segment* segment)
{
  return (segment->getMinSpeed() + segment->getMaxSpeed()) / 2;
}

bool adjacentSameDirection(Lane* lane1, Lane* lane2)
{
  return (lane1->getDirection() == lane2->getDirection());
}

vector<Lane*> getAdjacentLanes(Lane* lane, RNDF* rndf)
{
  vector<Lane*> adjacentLanes;
  int segmentID = lane->getSegmentID();
  int numOfLanes = rndf->getSegment(segmentID)->getNumOfLanes();

  for(int i = 1; i <= numOfLanes; i++)
  {
    Lane* adjLane = rndf->getLane(segmentID, i);
    if(i != lane->getLaneID() && adjacentSameDirection(lane, adjLane))
      adjacentLanes.push_back(adjLane);
  }
      
  return adjacentLanes;
}


vector<Lane*> getNonAdjacentLanes(Lane* lane, RNDF* rndf)
{
  vector<Lane*> nonAdjacentLanes;
  int segmentID = lane->getSegmentID();
  int numOfLanes = rndf->getSegment(segmentID)->getNumOfLanes();

  for(int i = 1; i <= numOfLanes; i++)
  {
    Lane* nonAdjLane = rndf->getLane(segmentID, i);
    if(i != lane->getLaneID() && !adjacentSameDirection(lane, nonAdjLane))
      nonAdjacentLanes.push_back(nonAdjLane);
  }
      
  return nonAdjacentLanes;
}

double square(double x)
{
  return x*x;	
}

double min(double x, double y)
{
  if (x < y)
    return x;
  else
    return y;
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add the following vertices to travGraph
// * exit points
// * entry points
// * checkpoints
// * entry perimeter points
// * exit perimeter points
// * parking spot waypoints
//-------------------------------------------------------------------------------------------
/*
void addAllVertices(RNDF* rndf, Graph* travGraph)
{
  int numOfSegments = rndf->getNumOfSegments();
  int numOfZones = rndf->getNumOfZones();
  
  // Adding all entry, exit, and checkpoint waypoints to the graph.

  for(int i = 1; i <= numOfSegments; i++)
  {    
    for(int j = 1; j <= rndf->getSegment(i)->getNumOfLanes(); j++)
    {
      for(int k = 1; k <= rndf->getLane(i, j)->getNumOfWaypoints(); k++)
      {
        Waypoint* waypoint = rndf->getWaypoint(i, j, k);
        
        if(waypoint->isEntry() || waypoint->isExit() || waypoint->isCheckpoint())
        {
          travGraph->addVertex(i, j, k);
        }
      }
    }    
  }
  
  // Adding all entry and exit perimeter points and parking spot waypoints.
  
  for(int i = numOfSegments + 1; i <= numOfSegments + numOfZones; i++)
  {
    
    // Adding all entry and exit perimeter points.
    
    for(int j = 1; j <= rndf->getZone(i)->getNumOfPerimeterPoints(); j++)
    {
      PerimeterPoint* perimeterPoint = rndf->getPerimeterPoint(i, j);
      
      if(perimeterPoint->isEntry() || perimeterPoint->isExit())
      {
        travGraph->addVertex(i, 0, j);
      }
    }
    
    // Adding all parking spot checkpoints.
    
    for(int j = 1; j <= rndf->getZone(i)->getNumOfSpots(); j++)
    {
      Waypoint* waypoint = rndf->getWaypoint(i, j, 1);
          
      if(waypoint->isCheckpoint())
      {
      travGraph->addVertex(i, j, 1);
      }
        
      waypoint = rndf->getWaypoint(i, j, 2);
        
      if(waypoint->isCheckpoint())
      {
        travGraph->addVertex(i, j, 2);
      }
    }
  }
  
  if (DEBUG_LEVEL > 0)
  {
    cout << "Vertices added to graph: " << endl;
    travGraph->print();
  }
}
*/

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add the following edges to travGraph
// * exit point -> entry points
// * entry point -> closest exit point
// * closest entry points -> checkpoint
// * checkpoint -> closest exit point
// * checkpoint -> other checkpoints before closest exit point
// * parking spot -> exit points
// * entry perimeter point -> exit perimeter points
// * entry perimenter point -> parking spots
//-------------------------------------------------------------------------------------------
/*
void addAllEdges(RNDF* rndf, Graph* travGraph)
{
  vector<Vertex*> vertices = travGraph->getVertices();
  
  // Adding all edges from exit waypoints.
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    GPSPoint* exit;
    vector<GPSPoint*> entries;
    
    int exitSegmentID = vertex->getSegmentID();
    int exitLaneID = vertex->getLaneID();
    int exitWaypointID = vertex->getWaypointID();
    
    if(exitLaneID == 0)
      exit = rndf->getPerimeterPoint(exitSegmentID, exitWaypointID);
    else
      exit = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID);
        
    if(!exit->isExit())
      continue;
    else
      entries = exit->getEntryPoints();

    // Adding all edges from exit waypoints to their entry waypoints.
    for(unsigned j = 0; j < entries.size(); j++)
    {
      GPSPoint* entry = entries[j];
      double length = computeDistance(entry->getNorthing(), entry->getEasting(),
          exit->getNorthing(), exit->getEasting());
      double weight = length/INTERSECTION_SPEED;
      if (exitLaneID != 0)
      {
        weight += COST_INTERSECTION;
        if (rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID)->isStopSign())
        {
          weight += COST_STOP_SIGN;
        }
      }
      
      if (!travGraph->addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
			   entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID(), length, weight))
      {
        if (DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";
          exit->print();
          cout << " to ";
          entry->print();
          cout << endl;
        }
      }
    }
            
    // Adding edges from all parking spot checkpoints to their exit waypoints.
    if(exitLaneID == 0)
    {
      for(int j = 1; j <= rndf->getZone(exitSegmentID)->getNumOfSpots(); j++)
      {
        Waypoint* spot = rndf->getWaypoint(exitSegmentID, j, 2);
        if(spot->isCheckpoint())
        {
          double length = computeDistance(exit->getNorthing(), exit->getEasting(), spot->getNorthing(), spot->getEasting());
          double weight = length/(rndf->getZone(exitSegmentID)->getMaxSpeed()) + COST_ZONE;
          if (! travGraph->addEdge(exitSegmentID, j, 2, 
              exitSegmentID, exitLaneID, exitWaypointID, length, weight))
          {
            if (DEBUG_LEVEL > 3)
            {
              cout << "Cannot add edge from ";
              exit->print();
              cout << " to ";
              spot->print();
              cout << endl;
            }
          }
        }
      }
    }

    
    // Adding all edges from exit waypoints to their next waypoint on the same lane.
    else if(exitWaypointID < rndf->getLane(exitSegmentID, exitLaneID)->getNumOfWaypoints())
    {
      GPSPoint* entry = rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID+1);
      double length = computeDistance(entry->getNorthing(), entry->getEasting(),
          exit->getNorthing(), exit->getEasting());
      double weight = length/(rndf->getSegment(exitSegmentID)->getMaxSpeed()) +
          COST_INTERSECTION;
      if (rndf->getWaypoint(exitSegmentID, exitLaneID, exitWaypointID)->isStopSign())
      {
        weight += COST_STOP_SIGN;
      }
      if (!travGraph->addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
			    entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID(), length, weight))
      {
        travGraph->addVertex(entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID());
        if (!travGraph->addEdge(exitSegmentID, exitLaneID, exitWaypointID, 
            entry->getSegmentID(), entry->getLaneID(), entry->getWaypointID(), length, weight))
        {
  	      cout << "Cannot add edge from ";
          exit->print();
          cout << " to ";
          entry->print();
          cout << endl;
        }
        addEdgesFromEntry(rndf, travGraph, entry);
      }
    }
  }
  
  // Adding all edges from entry waypoints.
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    GPSPoint* entry;
    
    int entrySegmentID = vertex->getSegmentID();
    int entryLaneID = vertex->getLaneID();
    int entryWaypointID = vertex->getWaypointID();
    
    // Adding all edges from entry perimeter points of zones.

    if(entryLaneID == 0)
    {
      entry = rndf->getPerimeterPoint(entrySegmentID, entryWaypointID);
      
      if(!entry->isEntry())
      {
        continue;
      }
        
      addEdgesFromEntryPerimeter(rndf, travGraph, entry);
    }
    else
    {
      entry = rndf->getWaypoint(entrySegmentID, entryLaneID, entryWaypointID);
            
      if(!entry->isEntry())
      {
        continue;
      }

      addEdgesFromEntry(rndf, travGraph, entry);
    }
  }


  // Adding edges to/from all checkpoints.
  for(unsigned i = 0; i < vertices.size(); i++)
  {
    Vertex* vertex = vertices[i];
    Waypoint* checkpoint;
    
    int checkpointSegmentID = vertex->getSegmentID();
    int checkpointLaneID = vertex->getLaneID();
    int checkpointWaypointID = vertex->getWaypointID();
        
    if(checkpointLaneID == 0 || checkpointSegmentID > rndf->getNumOfSegments())
    {
      continue;
    }
    else
    {
      checkpoint = rndf->getWaypoint(checkpointSegmentID, checkpointLaneID,
          checkpointWaypointID);
    }

    if(!checkpoint->isCheckpoint())
    {
      continue;
    }
        
    // Adding edges to all checkpoints from their closest entry waypoints.
    Waypoint* previousWaypoint = rndf->getWaypoint(checkpointSegmentID, checkpointLaneID,
        checkpointWaypointID);
    bool entryFound = false;
    int j = checkpointWaypointID - 1;
    double length = 0;
    while(!entryFound && j >= 1)
    {
      Waypoint* entry = rndf->getWaypoint(checkpointSegmentID, checkpointLaneID, j);
      length += computeDistance(entry->getNorthing(), entry->getEasting(),
    		previousWaypoint->getNorthing(), previousWaypoint->getEasting());
      if(entry->isEntry())
      {
        double weight = length;
        if (!travGraph->addEdge(checkpointSegmentID, checkpointLaneID, j,
            checkpointSegmentID, checkpointLaneID, checkpointWaypointID, length, weight))
        {
          if (DEBUG_LEVEL > 3)
          {
            cout << "Cannot add edge from ";
            checkpoint->print();
            cout << " to ";
            entry->print();
            cout << endl;
          }
        }
        entryFound = true;
      }
      j--;
      previousWaypoint = entry;
    }
   
    // Adding edges from all checkpoints to their closest exit waypoints and other
    // checkpoints on the same segment before their closest exit waypoints.
    addEdgesFromEntry(rndf, travGraph, checkpoint);
  }
}
*/

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add edges from the specified entry point to its closest exit point
// (both on the same lane and adjacent lanes) and to all
// checkpoints before its closest exit point.
//-------------------------------------------------------------------------------------------
/*
void addEdgesFromEntry(RNDF* rndf, Graph* travGraph, GPSPoint* entry)
{      
  int entrySegmentID = entry->getSegmentID();
  int entryLaneID = entry->getLaneID();
  int entryWaypointID = entry->getWaypointID();
  int numOfWaypoints = rndf->getLane(entrySegmentID, entryLaneID)->getNumOfWaypoints();

  // Adding edges from this entry waypoint to its next exit waypoints in the same lane.
  bool exitFound = false;
  int j = entryWaypointID + 1;
  GPSPoint* previousWaypoint = entry;
  double length = 0;
  while(!exitFound && j <= numOfWaypoints)
  {
    Waypoint* exit = rndf->getWaypoint(entrySegmentID, entryLaneID, j);
    length += computeDistance(previousWaypoint->getNorthing(), previousWaypoint->getEasting(), 
        exit->getNorthing(), exit->getEasting());
    if(exit->isExit())
    {
      double weight = length;
      if (!travGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
          entrySegmentID, entryLaneID, j, length, weight))
      {
        if (DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";
          entry->print();
          cout << " to ";
          exit->print();
          cout << endl;
        }
      }
      exitFound = true;
    }
    else if (exit->isCheckpoint())
    {
      double weight = length;
      if (!travGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
          entrySegmentID, entryLaneID, j, length, weight))
      {
        if (DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";
          entry->print();
          cout << " to ";
          exit->print();
          cout << endl;
        }
      }
    }
    j++;
    previousWaypoint = exit;
  }


  // Adding edges from this entry waypoint to exit waypoints in adjacent,
  // same direction lanes.
  if (entryWaypointID < numOfWaypoints)
  {
    vector<Lane*> adjacentLanes =
        getAdjacentLanes(rndf->getLane(entrySegmentID, entryLaneID), rndf);
    GPSPoint* nextWaypoint =
        rndf->getWaypoint(entrySegmentID, entryLaneID, entryWaypointID+1);
    for(unsigned j = 0; j < adjacentLanes.size(); j++)
    {
      Lane* adjacentLane = adjacentLanes[j];
      int numOfWaypointsOnAdjLane = adjacentLane->getNumOfWaypoints();
      double distanceFromNextWaypoint;
      double distanceFromEntry;

      // Make sure that the exit point on the adjacent lane is further along the road
      // (we can't go backward!)
      double nextWaypointNorthing = nextWaypoint->getNorthing();
      double nextWaypointEasting = nextWaypoint->getEasting();
      GPSPoint* nextWaypointOnAdjLane =
          findClosestGPSPoint(nextWaypointNorthing, nextWaypointEasting, 
          entrySegmentID, adjacentLane->getLaneID(), distanceFromNextWaypoint, rndf);
      int nextWaypointOnAdjLaneID = nextWaypointOnAdjLane->getWaypointID();
      distanceFromEntry = computeDistance(entry->getNorthing(), entry->getEasting(),
          nextWaypointOnAdjLane->getNorthing(), nextWaypointOnAdjLane->getEasting());
      
      while (nextWaypointOnAdjLaneID < numOfWaypointsOnAdjLane &&
          distanceFromNextWaypoint > distanceFromEntry)
      {
        nextWaypointOnAdjLaneID++;
        nextWaypointOnAdjLane =
            rndf->getWaypoint(entrySegmentID, entryLaneID, nextWaypointOnAdjLaneID);
        distanceFromNextWaypoint =
            computeDistance(nextWaypoint->getNorthing(), nextWaypoint->getEasting(), 
            nextWaypointOnAdjLane->getNorthing(), nextWaypointOnAdjLane->getEasting());
        distanceFromEntry = computeDistance(entry->getNorthing(), entry->getEasting(), 
            nextWaypointOnAdjLane->getNorthing(), nextWaypointOnAdjLane->getEasting());
      }

      if (distanceFromNextWaypoint <= distanceFromEntry)
      {	
        if (nextWaypointOnAdjLane->isExit())
        {
  	  		double weight = distanceFromEntry +
              abs(entryLaneID - adjacentLane->getLaneID())*COST_CHANGE_LANE;
  	  		if (!travGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
  				  entrySegmentID, adjacentLane->getLaneID(), nextWaypointOnAdjLaneID,
            distanceFromEntry, weight))
          {
            if (DEBUG_LEVEL > 3)
            {
              cout << "Cannot add edge from ";
              entry->print();
              cout << " to ";
              nextWaypointOnAdjLane->print();
              cout << endl;
            }
          }
        }
        else
        {
          exitFound = false;
          int k = nextWaypointOnAdjLaneID + 1;
          previousWaypoint = nextWaypointOnAdjLane;
          length = distanceFromEntry;
          while(!exitFound && k <= numOfWaypointsOnAdjLane)
          {
            Waypoint* exit = rndf->getWaypoint(entrySegmentID, adjacentLane->getLaneID(), k);
            length += computeDistance(previousWaypoint->getNorthing(),
                previousWaypoint->getEasting(), exit->getNorthing(), exit->getEasting());
            if(exit->isExit())
            {
              double weight = length +
                  abs(entryLaneID - adjacentLane->getLaneID())*COST_CHANGE_LANE;
            	if (!travGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
                  entrySegmentID, adjacentLane->getLaneID(), k, length, weight))
              {
                if (DEBUG_LEVEL > 3)
                {
                  cout << "Cannot add edge from ";
                  entry->print();
                  cout << " to ";
                  exit->print();
                  cout << endl;
                }
              }
              exitFound = true;
            }
            else if (exit->isCheckpoint())
            {
              double weight = length +
                  abs(entryLaneID - adjacentLane->getLaneID())*COST_CHANGE_LANE;
              if (!travGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID,
                  entrySegmentID, adjacentLane->getLaneID(), k, length, weight))
              {
                if (DEBUG_LEVEL > 3)
                {
                  cout << "Cannot add edge from ";
                  entry->print();
                  cout << " to ";
                  exit->print();
                  cout << endl;
                }
              }
            }
            k++;
            previousWaypoint = exit;
          }
        }   
      }
    }
  }
}
*/

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add edges from the specified entry perimeter point to all its exit perimeter points and
// to all the checkpoints in the zone.
//-------------------------------------------------------------------------------------------
/*
void addEdgesFromEntryPerimeter(RNDF* rndf, Graph* travGraph, GPSPoint* entry)
{
  int entrySegmentID = entry->getSegmentID();
  int entryLaneID = entry->getLaneID();
  int entryWaypointID = entry->getWaypointID();

  // Adding edges from this entry perimeter points to all exit perimeter points.
  for(int j = 1; j <= rndf->getZone(entrySegmentID)->getNumOfPerimeterPoints(); j++)
  {
    GPSPoint* exit = rndf->getPerimeterPoint(entrySegmentID, j);
    if(exit->isExit())
    {
      double length = computeDistance(entry->getNorthing(), entry->getEasting(),
          exit->getNorthing(), exit->getEasting());
      double weight = length/(rndf->getZone(entrySegmentID)->getMaxSpeed()) + COST_ZONE;
      if (!travGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID, entrySegmentID, 0,
          j, length, weight))
      {
        if (DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";
          entry->print();
          cout << " to ";
          exit->print();
          cout << endl;
        }
      }
    }
  }
            
  // Adding edges from this entry perimeter point to all parking spot checkpoints.
  for(int j = 1; j <= rndf->getZone(entrySegmentID)->getNumOfSpots(); j++)
  {
    Waypoint* spot = rndf->getWaypoint(entrySegmentID, j, 2);
    if(spot->isCheckpoint())
    {
      double length = computeDistance(entry->getNorthing(), entry->getEasting(),
          spot->getNorthing(), spot->getEasting());
      double weight = length + COST_ZONE;
      if (!travGraph->addEdge(entrySegmentID, entryLaneID, entryWaypointID, entrySegmentID, j,
          2, length, weight))
      {
        if (DEBUG_LEVEL > 3)
        {
          cout << "Cannot add edge from ";
          entry->print();
          cout << " to ";
          spot->print();
          cout << endl;
        }
      }
    }
  }
}
*/



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Find the closest waypoint or perimeter point on the specified segment and lane.
// If segmentID = 0, then find the closest waypoint on any segment.
// If laneID = 0, then find the closest waypoint on the specified segment on any lane.
//-------------------------------------------------------------------------------------------
/*
GPSPoint* findClosestGPSPoint(double northing, double easting, int segmentID, int laneID,
    double& distance, RNDF* rndf)
{
  double headingDiff;
  return findClosestGPSPoint(northing, easting, segmentID, laneID, distance, rndf, 
			     false, 0, 0, headingDiff);
}

GPSPoint* findClosestGPSPoint(double northing, double easting, int segmentID, int laneID,
    double& distance, RNDF* rndf, bool useHeading, double yaw, double maxHeadingDiff,
    double& headingDiff)
{
  int numOfSegments = rndf->getNumOfSegments();
  int numOfZones = rndf->getNumOfZones();
  double closestDistance;
  GPSPoint* closestGPSPoint;
  double GPSPointNorthing, GPSPointEasting;

  // The case where we're on a segment
  if (segmentID != 0 && laneID != 0 && segmentID <= numOfSegments &&
      laneID <= rndf->getSegment(segmentID)->getNumOfLanes())
  {
    closestGPSPoint = rndf->getWaypoint(segmentID, laneID, 1);
    GPSPointNorthing = closestGPSPoint->getNorthing();
    GPSPointEasting = closestGPSPoint->getEasting();
    closestDistance = sqrt(square(northing - GPSPointNorthing) +
        square(easting - GPSPointEasting));
    headingDiff = fmod(atan2(northing - GPSPointNorthing, easting - GPSPointEasting)
		       +(3 * PI/2), 2*PI) - yaw;
    headingDiff = fabs(headingDiff);
    if (useHeading && headingDiff > maxHeadingDiff)
    {
      closestDistance = MAX_DISTANCE;
    }

    for (int k = 1; k <= rndf->getLane(segmentID, laneID)->getNumOfWaypoints(); k++)
    {
      GPSPoint* waypoint = rndf->getWaypoint(segmentID, laneID, k);
      GPSPointNorthing = waypoint->getNorthing();
      GPSPointEasting = waypoint->getEasting();
      double newDistance = sqrt(square(northing - GPSPointNorthing) +
          square(easting - GPSPointEasting));
      double newHeadingDiff = fmod(atan2(northing - GPSPointNorthing, easting - GPSPointEasting)
	  +(3 * PI/2), 2*PI) - yaw;
      newHeadingDiff = fabs(newHeadingDiff);
      if (newDistance < closestDistance)
      {
	if(!useHeading || (newHeadingDiff < maxHeadingDiff))
	{
	  closestGPSPoint = waypoint;
	  closestDistance = newDistance;
	  headingDiff = newHeadingDiff;
	}
      }    
    }
  }

  else if (segmentID != 0 && segmentID <= numOfSegments)
  {
    closestGPSPoint = findClosestGPSPoint(northing, easting, segmentID, 1, closestDistance, 
					  rndf, useHeading, yaw, maxHeadingDiff, headingDiff);
    for (int j = 1; j <= rndf->getSegment(segmentID)->getNumOfLanes(); j++)
    {
      double newDistance;
      double newHeadingDiff;
      GPSPoint* newClosestGPSPoint = findClosestGPSPoint(northing, easting, segmentID, j, 
							 newDistance, rndf, useHeading, yaw, 
							 maxHeadingDiff, newHeadingDiff);
      if (newDistance < closestDistance)
      {
	if(!useHeading || (newHeadingDiff < maxHeadingDiff))
	{
	  closestGPSPoint = newClosestGPSPoint;
	  closestDistance = newDistance;
	  headingDiff = newHeadingDiff;
	}
      }    
    }
  }

  // The case where we're in a zone
  else if (segmentID != 0 && segmentID <= numOfSegments + numOfZones)
  {
    closestGPSPoint = rndf->getPerimeterPoint(segmentID, 1);
    GPSPointNorthing = closestGPSPoint->getNorthing();
    GPSPointEasting = closestGPSPoint->getEasting();
    closestDistance = sqrt(square(northing - GPSPointNorthing) +
        square(easting - GPSPointEasting));
    headingDiff = fmod(atan2(northing - GPSPointNorthing, easting - GPSPointEasting)
		       +(3 * PI/2), 2*PI) - yaw;
    headingDiff = fabs(headingDiff);
    if (useHeading && headingDiff > maxHeadingDiff)
    {
      closestDistance = MAX_DISTANCE;
    }

    // Check all the perimeter points
    for(int j = 1; j <= rndf->getZone(segmentID)->getNumOfPerimeterPoints(); j++)
    {
      GPSPoint* perimeterPoint = rndf->getPerimeterPoint(segmentID, j);
      GPSPointNorthing = perimeterPoint->getNorthing();
      GPSPointEasting = perimeterPoint->getEasting();
      double newDistance = sqrt(square(northing - GPSPointNorthing) +
          square(easting - GPSPointEasting));
      double newHeadingDiff = fmod(atan2(northing - GPSPointNorthing, easting - GPSPointEasting)
				   +(3 * PI/2), 2*PI) - yaw;
      newHeadingDiff = fabs(newHeadingDiff);
      if (newDistance < closestDistance)
      {
	if(!useHeading || (newHeadingDiff < maxHeadingDiff))
	{
	  closestGPSPoint = perimeterPoint;
	  closestDistance = newDistance;
	  headingDiff = newHeadingDiff;
	}
      }
    }
    
    // Check all the parking spot waypoints
    for(int j = 1; j <= rndf->getZone(segmentID)->getNumOfSpots(); j++)
    {
      for(int k = 1; k <= 2; k++)
      {
        GPSPoint* waypoint = rndf->getWaypoint(segmentID, j, k);
        GPSPointNorthing = waypoint->getNorthing();
        GPSPointEasting = waypoint->getEasting();
        double newDistance = sqrt(square(northing - GPSPointNorthing) +
            square(easting - GPSPointEasting));
	double newHeadingDiff = fmod(atan2(northing - GPSPointNorthing, easting - GPSPointEasting)
				     +(3 * PI/2), 2*PI) - yaw;
	newHeadingDiff = fabs(newHeadingDiff);
        if (newDistance < closestDistance)
        {
	  if(!useHeading || (newHeadingDiff < maxHeadingDiff))
	  {
	    closestGPSPoint = waypoint;
	    closestDistance = newDistance;
	    headingDiff = newHeadingDiff;
	  }
        }    
      }
    }
  }

  else
  {
    closestGPSPoint = findClosestGPSPoint(northing, easting, 1, 0, closestDistance, rndf,
					  useHeading, yaw, maxHeadingDiff, headingDiff);
    
    for (int i = 2; i <= numOfSegments + numOfZones; i++)
    {
      double newDistance;
      double newHeadingDiff;
      GPSPoint* newClosestGPSPoint = findClosestGPSPoint(northing, easting, i, 0, newDistance, 
							 rndf, useHeading, yaw, 
							 maxHeadingDiff, newHeadingDiff);
      if (newDistance < closestDistance)
      {
	if(!useHeading || (newHeadingDiff < maxHeadingDiff))
	{
	  closestGPSPoint = newClosestGPSPoint;
	  closestDistance = newDistance;
	  headingDiff = newHeadingDiff;
	}
      }    
    }
  }

  distance = closestDistance;
  return closestGPSPoint;
}
*/


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Add a vertex corresponding to the specified GPSPoint (can be waypoint or perimeter point)
// to the graph with appropriate edges
//-------------------------------------------------------------------------------------------
/*
bool addGPSPointToGraph (GPSPoint* point, Graph* travGraph, RNDF* rndf)
{
  int segmentID = point->getSegmentID();
  int laneID = point->getLaneID();
  int waypointID = point->getWaypointID();

  if (!travGraph->addVertex(segmentID, laneID, waypointID))
  {
    return false;
  }

  // In the case we're in a zone.
  if (segmentID > rndf->getNumOfSegments())
  {
    travGraph->addEdgesFromEntryPerimeter(rndf, point);
  }

  // In the case we're on a segment
  else
  {
    // Add edges to all exit points both in the same lane and in adjacent,
    // same direction lanes
    addEdgesFromEntry(rndf, travGraph, point);
    
    // Add edges to checkpoints
  	int numOfWaypoints = rndf->getLane(segmentID, laneID)->getNumOfWaypoints();

  	// Adding edges from this entry waypoint to its next checkpoint waypoint in the same lane.
  	bool ckptFound = false;
  	int j = waypointID + 1;
  	GPSPoint* previousWaypoint = point;
  	double length = 0;
  	while(!ckptFound && j <= numOfWaypoints)
  	{
    	Waypoint* ckpt = rndf->getWaypoint(segmentID, laneID, j);
    	length += computeDistance(previousWaypoint->getNorthing(), 
          previousWaypoint->getEasting(), ckpt->getNorthing(), ckpt->getEasting());
    	if(ckpt->isCheckpoint())
    	{
        double weight = length;
      	if (!travGraph->addEdge(segmentID, laneID, waypointID, 
            segmentID, laneID, j, length, weight))
        {
          if (DEBUG_LEVEL > 3)
          {
            cout << "Cannot add edge from ";
            point->print();
            cout << " to ";
            ckpt->print();
            cout << endl;
          }
        }
        ckptFound = true;
    	}
  		else if (ckpt->isExit())
  		{
  			ckptFound = true; // Do not add edge if the closest exit point is closer than
                          // the closest checkpoint	
  		}
    	j++;
    	previousWaypoint = ckpt;
  	}
  }
  return true;
}
*/
