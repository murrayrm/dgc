/*!**
 * Nok Wongpiromsarn
 * February 23, 2007
 */


#ifndef ROUTEPLANNER_HH
#define ROUTEPLANNER_HH

#include <pthread.h>
#include <string>
#include <vector>
#include <list>
#include <math.h>
//#include "alice/estop.h"
#include "dgcutils/DGCutils.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include "rndf/RNDF.hh"
#include "Obstacle.hh"
#include "Graph.hh"
#include  "MissionUtils.hh"
#include "skynettalker/SegGoalsTalker.hh"
#include "dgcutils/GlobalConstants.h"

#define LEAST_NUM_SEGGOALS_STORED 8
#define LEAST_NUM_SEGGOALS_TPLANNER_STORED 3
#define NUM_SEGGOALS_DISPLAYED 6



/**
 * RoutePlanner class.
 * This is a main class for the route planner part of the mission planner.
 * It is responsible for planning route (taking into account vehicle
 * capability) to the next checkpoint as a series of segments,
 * traffic rules that are to be enforced on each segment and performance
 * criteria for each segment, an issue segment goals between waypoints,
 * as previous ones are completed or failed.
 * \brief Main class for route planner part of mission planner
 */

class RoutePlanner : public CSegGoalsTalker
{
  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;
  
  int DEBUG_LEVEL, VERBOSITY_LEVEL;

  /*!\param m_rndf is a pointer to the RNDF object. 
   * There is also a revision number associated with this map*/
  RNDF* m_rndf;  
  
  /*!\param m_rndfGraph is the corresponding graph of the current RNDF
   * that mission planner uses to compute segment goals. */
  Graph* m_rndfGraph;

  /*!\param m_edgesRemoved is the list of all the edges that got removed
   * from the RNDF graph because mission planner thinks that the road is 
   * blocked. */
  list<Edge*> m_edgesRemoved;

  /*!\param m_checkpointGoalID is an array of goalID's which correspond
   * to the goals for crossing checkpoints. m_checkpointGoalID[i] is the 
   * goalID for crossing m_checkpointSequence[i] */
  int* m_checkpointGoalID;     

  /*!\param m_segGoalsSeq */
  list<SegGoals> m_segGoalsSeq;

  /*!\param m_executingGoals is a list of goals that are currently stored in
   * the traffic planner. This is basically the goals mission planner
   * sent to the traffic planner that have not yet completed. The first
   * element of this list is the goal that mission planner believes the
   * traffic planner is executing. */
  list<SegGoals> m_executingGoals;

  /*!\param m_nextGoalID is the ID of the next goal to be added 
   * to m_segGoalsSeq. */
  int m_nextGoalID; 

  /*!\param m_segGoalsStatus is the status of the traffic planner which is
   * used in the planning loop to determine if we need to replan. */
  SegGoalsStatus m_segGoalsStatus;

  /*!\param m_receivedSegGoalsStatus is the actual status of the traffic
   * planner. */
  SegGoalsStatus* m_receivedSegGoalsStatus;

  /*!\param m_numSegGoalsRequests is the numer of requests for segment goals
   * received from the traffic planner. */
  int m_numSegGoalsRequests;

  /*!\param m_obstacles is the list of all the obstacles. */
  vector<Obstacle*> m_obstacles;


  /*!\param Varibales which keep track of whether traffic planner 
   * starts listening. */
  bool m_bReceivedAtLeastOneSegGoalsStatus;
  pthread_mutex_t m_FirstSegGoalsStatusReceivedMutex;
  pthread_cond_t m_FirstSegGoalsStatusReceivedCond;

  /*!\param The mutex to protect the RNDF we're planning on */
  pthread_mutex_t m_GloNavMapMutex;

  /*!\param The mutex to protect the segGoalsSeq */
  pthread_mutex_t m_SegGoalsSeqMutex;

  /*!\param The mutex to protect the status of segGoals */
  pthread_mutex_t m_SegGoalsStatusMutex;

  /*!\param The mutex to protect the index of the next checkpoint */
  pthread_mutex_t m_NextCheckpointIndexMutex;

  /*!\param The mutex to protect the list of executing goals */
  pthread_mutex_t m_ExecGoalsMutex;

  /*!\param The mutex to protect the list of obstacles */
  pthread_mutex_t m_ObstaclesMutex;

  /*!\param The skynet socket for sending segment-level goals to tplanner */
  int m_segGoalsSocket;
  
  /*!\param SparrowHawk display */
  CSparrowHawk *shp;

  /*!\param Sparrow variables */
  int sparrowNextGoalIDs[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints2[NUM_SEGGOALS_DISPLAYED];
  double sparrowMinSpeed, sparrowMaxSpeed;
  bool sparrowIllegalPassingAllowed, sparrowStopAtExit, sparrowIsExitCheckpoint;
  int sparrowNextCheckpointID, sparrowNextCheckpointGoalID;
  int sparrowNextCheckpointSegmentID, sparrowNextCheckpointLaneID;
  int sparrowNextCheckpointWaypointID;

  /*!\param Obstacle inserted by sparrow */
  int obstacleID;
  int obstacleSegmentID;
  int obstacleLaneID;
  int obstacleWaypointID;


public:
  /*! Constructor */
  RoutePlanner(int skynetKey, bool nosparrow, int debugLevel, bool verbose);
      
  /*! Standard destructor */
  ~RoutePlanner();

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface. */
  void UpdateSparrowVariablesLoop();

  /*! This is the function that continually runs the planner in a loop */
  void MPlanningLoop(void);

  /*! This is the function that continually reads tplanner status and update the
   *  executing goalID. */
  void getTPlannerStatusThread();

  /*! This is the function that continually transmit segment goals to tplanner and
   *  update the executing goalID. */
  void sendSegGoalsThread();

private:
  void arbiter();
  void control();
  void control(SegGoalsStatus status);
  void sparrowInsertObstacle();
  void requestTraversibilityGraph();
  void initializeSegGoals();
  vector<SegGoals> planFromCurrentPosition(GPSPoint*, int);
  vector<SegGoals> planNextMission(int);
  vector<SegGoals> addUturn(GPSPoint*, GPSPoint*, Vertex*);
  void addEndOfMission(vector<SegGoals>&);
  void resetGoals();
  int getObstacleIndex(int);
  vector<Obstacle*> getAllObstaclesOnSegment(int);
  bool loadMDFFile(char*, RNDF*);
  void parseCheckpoint(ifstream*, RNDF*);
  void parseSpeedLimit(ifstream*, RNDF*);
  void setSpeedLimits(int, double, double, RNDF*);
  void printMissionOnSparrow(list<SegGoals> segGoals, int startIndex);
};

#endif  // ROUTEPLANNER_HH
