/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include <getopt.h>
#include <iostream>
#include <rndf/RNDF.hh>
#include <rndf/MDF.hh>
#include <dgcutils/ggis.h>
#include <dgcutils/GlobalConstants.h>
#include <travgraph/Graph.hh>
#include <gcinterfaces/SegGoals.hh>
#include <dgcutils/DGCutils.hh>
#include <dgcutils/findroute.h>
#include <sparrowhawk/SparrowHawk.hh>
#include <skynet/skynet.hh>
#include <sys/time.h>
#include <sys/stat.h>
#include <signal.h>
#include "findMinMissionTimeCmdline.h"
#include "CmdArgs.hh"
#include "MissionUtils.hh"


using namespace std;

double findDistanceOnRoad(GPSPoint* waypoint1, GPSPoint* waypoint2, RNDF* rndf) ;
double findShortestDistance(GPSPoint* waypoint1, GPSPoint* waypoint2) ;
bool loadMDFFile(char* fileName, RNDF* rndf, vector<double> &minSpeedLimits, 
		 vector<double> &maxSpeedLimits);
void parseSpeedLimit(ifstream* file, RNDF* rndf, vector<double> &minSpeedLimits, 
		     vector<double> &maxSpeedLimits);

int main(int argc, char **argv) 
{
  gengetopt_args_info cmdline;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    return -1;
  }

  /* Figure out what RNDF and MDF we want to use */
  char *RNDFFileName = dgcFindRouteFile(cmdline.rndf_arg);
  char *MDFFileName = dgcFindRouteFile(cmdline.mdf_arg);

  cout << "RNDF file: " << RNDFFileName << endl;
  cout << "MDF file: " << MDFFileName << endl;

  /* Get the log analysis file */
  FILE* logFile = NULL;
  if(cmdline.enable_logging_given) {
    logFile = fopen(cmdline.logfile_arg, "a");
  }

  /* Get the RNDF object */
  RNDF* rndf = new RNDF();
  if (!rndf->loadFile(RNDFFileName))
  {
    cerr << "ERROR:  Unable to load RNDF file " << RNDFFileName
	 << ", exiting program" << endl;
    return -1;
  }
  rndf->assignLaneDirection();

  MDF mdf;
  if (mdf.loadFile(MDFFileName) != 0) {
    cerr << "ERROR: Unable to load MDF file " << MDFFileName 
	 << ", exiting program" << endl;
    return -1;
  }

  Graph* graph = new Graph(rndf);
  vector<double> minSpeedLimits, maxSpeedLimits;
  minSpeedLimits.resize(graph->getNumOfSegments() + graph->getNumOfZones() + 1);
  maxSpeedLimits.resize(graph->getNumOfSegments() + graph->getNumOfZones() + 1);

  for (unsigned i=0; i < minSpeedLimits.size(); i++) {
    minSpeedLimits[i] = 0;
    maxSpeedLimits[i] = 15;
  }

  if (!loadMDFFile(MDFFileName, rndf, minSpeedLimits, maxSpeedLimits)) {
    cerr << "Unable to load speed limits in " << MDFFileName << ", exiting program\n";
    return -1;
  }

  int startSegmentID = 0;
  int startLaneID = 0;
  int startWaypointID = 0;
  if (sscanf(cmdline.rndf_start_arg, "%d.%d.%d", &startSegmentID, &startLaneID, &startWaypointID) < 3) {
    cerr << "Invalid waypoint specification '" << cmdline.rndf_start_arg << "'" 
	 << endl;
    return -1;
  }

  cout << "Start waypoint: " << startSegmentID << "." << startLaneID << "." << startWaypointID << endl;

  int numCheckpoints = mdf.getNumCheckpoints();
  double minMissionTime = 0;
  double minMissionDist = 0;

  for (int i = -1; i < numCheckpoints - 1; i++) {
    GPSPoint* currentCheckpoint;
    if (i == -1) {
      if (startSegmentID == 0)
	continue;
      else if (startLaneID == 0)
	currentCheckpoint = rndf->getPerimeterPoint(startSegmentID, startWaypointID);
      else
	currentCheckpoint = rndf->getWaypoint(startSegmentID, startLaneID, startWaypointID);
    }
    else
      currentCheckpoint = rndf->getCheckpoint(mdf.getCheckpoint(i));

    GPSPoint* nextCheckpoint = rndf->getCheckpoint(mdf.getCheckpoint(i+1));
    Vertex* vertex1 = graph->getVertex(currentCheckpoint->getSegmentID(), 
					     currentCheckpoint->getLaneID(), 
					     currentCheckpoint->getWaypointID()); 
    Vertex* vertex2 = graph->getVertex(nextCheckpoint->getSegmentID(), 
					     nextCheckpoint->getLaneID(), 
					     nextCheckpoint->getWaypointID());
    vector<Vertex*> route; 
    double cost;
    bool routeFound = findRoute(vertex1, vertex2, graph, route, cost);
    if (!routeFound) {
      cerr << "Cannot find route from " << currentCheckpoint->getSegmentID()
	   << "." << currentCheckpoint->getLaneID() << "." 
	   << currentCheckpoint->getWaypointID() << " to "
	   << nextCheckpoint->getSegmentID()
	   << "." << nextCheckpoint->getLaneID() << "." 
	   << nextCheckpoint->getWaypointID();
      continue;
    }
    else {
      vector<SegGoals> segGoalsSeq = findSegGoals(route, graph, minSpeedLimits, maxSpeedLimits, i, 
						  cmdline.max_speed_arg*MPS_PER_MPH); 
      for (unsigned j = 0; j < segGoalsSeq.size(); j++) {
	GPSPoint *waypoint1, *waypoint2;
	if (route[j]->getSegmentID() == 0 || route[j+1]->getSegmentID() == 0)
	  continue;

	if (route[j]->getLaneID() == 0)
	  waypoint1 = rndf->getPerimeterPoint(route[j]->getSegmentID(), route[j]->getWaypointID());
	else
	  waypoint1 = rndf->getWaypoint(route[j]->getSegmentID(), route[j]->getLaneID(),
					route[j]->getWaypointID());

	if (route[j+1]->getLaneID() == 0)
	  waypoint2 = rndf->getPerimeterPoint(route[j+1]->getSegmentID(), route[j+1]->getWaypointID());
	else
	  waypoint2 = rndf->getWaypoint(route[j+1]->getSegmentID(), route[j+1]->getLaneID(),
					route[j+1]->getWaypointID());

	if (route[j]->isStopSign()) {
	  minMissionTime += cmdline.time_stopline_arg;
	}

	if (segGoalsSeq[j].segment_type == SegGoals::INTERSECTION ||
	    segGoalsSeq[j].segment_type == SegGoals::PREZONE) {
	  minMissionTime += cmdline.time_intersection_arg;
	  double currentDist = findShortestDistance(waypoint1, waypoint2);
	  minMissionDist += currentDist;
	  minMissionTime += currentDist/segGoalsSeq[j].maxSpeedLimit;
	}
	else if (segGoalsSeq[j].segment_type == SegGoals::ROAD_SEGMENT) {
	  double currentDist = findDistanceOnRoad(waypoint1, waypoint2, rndf);
	  minMissionDist += currentDist;
	  minMissionTime += currentDist/segGoalsSeq[j].maxSpeedLimit;
	}
	else if (segGoalsSeq[j].segment_type == SegGoals::PARKING_ZONE) {
	  minMissionTime += cmdline.time_zone_arg;
	  double currentDist = findShortestDistance(waypoint1, waypoint2);
	  minMissionDist += currentDist;
	  minMissionTime += currentDist/segGoalsSeq[j].maxSpeedLimit;
	}
	else if (segGoalsSeq[j].segment_type == SegGoals::UTURN) {
	  minMissionTime += cmdline.time_uturn_arg;
	  double currentDist = findShortestDistance(waypoint1, waypoint2);
	  minMissionDist += currentDist;
	  minMissionTime += currentDist/segGoalsSeq[j].maxSpeedLimit;
	}
      }
    }
  }

  stringstream s("");
  s << endl << "Minimum possible time: " << minMissionTime << " s" << endl
    << "Maximum possible average speed: " << 0.000621371*3600*minMissionDist/minMissionTime << " mph"
    << endl << endl;
  cout << s.str();
  if (logFile)
    fprintf(logFile, s.str().c_str());

  delete rndf;

  if (logFile)
    fclose(logFile);

  return 0;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double findDistanceOnRoad(GPSPoint* waypoint1, GPSPoint* waypoint2, RNDF* rndf) 
{
  if (waypoint1->getSegmentID() != waypoint2->getSegmentID()) {
    cerr << "segment_type = ROAD_SEGMENT but entry and exit are on different segment" << endl;
    return 0;
  }

  double distance = 0;

  if (waypoint1->getLaneID() != waypoint2->getLaneID()) {
    GPSPoint* nextWaypointOnAdjLane =
      rndf->findClosestGPSPoint(waypoint1->getNorthing(), waypoint1->getEasting(), 
				waypoint2->getSegmentID(), waypoint2->getLaneID(), distance);
    return distance + findDistanceOnRoad(nextWaypointOnAdjLane, waypoint2, rndf);
  }

  distance = 0;
  for (int i = waypoint1->getWaypointID(); i < waypoint2->getWaypointID() - 1; i++) {
    distance += findShortestDistance(rndf->getWaypoint(waypoint1->getSegmentID(),
						       waypoint1->getLaneID(),
						       i), 
				     rndf->getWaypoint(waypoint1->getSegmentID(),
						       waypoint1->getLaneID(),
						       i+1));
  }
  return distance;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double findShortestDistance(GPSPoint* waypoint1, GPSPoint* waypoint2) 
{
  return sqrt(pow(waypoint2->getEasting() - waypoint1->getEasting(), 2) + 
	      pow(waypoint2->getNorthing() - waypoint1->getNorthing(), 2));
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool loadMDFFile(char* fileName, RNDF* rndf, vector<double> &minSpeedLimits, 
			       vector<double> &maxSpeedLimits)
{
  ifstream file;
  string line;
  string word;
  
  file.open(fileName, ios::in);

  if(!file) {
    cerr << "Error: " << fileName << " file not found.\n";
    return false;
  } 

  while (word != "MDF_name") {
    getline(file, line);    
    istringstream lineStream(line, ios::in);  
    lineStream >> word;
  }

  if(word == "MDF_name") {
    parseSpeedLimit(&file, rndf, minSpeedLimits, maxSpeedLimits);
    file.close();
    return true;
  }
  else {
    file.close();
    return false;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void parseSpeedLimit(ifstream* file, RNDF* rndf, vector<double> &minSpeedLimits, 
				   vector<double> &maxSpeedLimits)
{
  int segmentID;
  double minSpeed, maxSpeed;
  string line, word;
  char letter;

  while(word != "speed_limits") {    
    getline(*file, line);
    istringstream lineStream(line, ios::in);
    lineStream >> word;
  }

  
  while(word != "end_speed_limits") {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    if(letter >= '0' && letter <= '9') {
      lineStream >> segmentID;
      lineStream >> minSpeed;
      lineStream >> maxSpeed;

      minSpeed = minSpeed * MPS_PER_MPH;
      maxSpeed = maxSpeed * MPS_PER_MPH;
      
      // setSpeedLimits(segmentID, minSpeed, maxSpeed, rndf);
      if ((unsigned)segmentID <= minSpeedLimits.size()) {
	minSpeedLimits[segmentID] = minSpeed;
	maxSpeedLimits[segmentID] = maxSpeed;
      }
      else {
	cerr << "got speed limit for segment " << segmentID
	     << " while number of segments and zones = " << minSpeedLimits.size() << "\n";
	int numOfSegments = minSpeedLimits.size();
	minSpeedLimits.resize(segmentID);
	maxSpeedLimits.resize(segmentID);
	for (unsigned i = numOfSegments; i <= minSpeedLimits.size(); i++)
	{
	  minSpeedLimits[i] = 0;
	  maxSpeedLimits[i] = 0;
	}
	minSpeedLimits[segmentID] = minSpeed;
	maxSpeedLimits[segmentID] = maxSpeed;
      }
    }
    else {
      lineStream >> word;
      continue;      
    }
  }
}
