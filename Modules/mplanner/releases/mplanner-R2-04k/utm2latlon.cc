#include <iostream>
#include "dgcutils/ggis.h"

using namespace std;

int main(int argc, char **argv) 
{
  GisCoordLatLon latlon;
  GisCoordUTM utm;
  utm.zone = 5;
  utm.letter = 'S';
  double northing, easting, lat, lon, x, y;
  double xoffset = 0;
  double yoffset = 0;

  while (true) {
    cout << "x: ";
    cin >> x;
    cout << "y: ";
    cin >> y;
    northing = x + xoffset;
    easting = y + yoffset;
    utm.n = northing;
    utm.e = easting;
    gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
    cout << "Northing: " << utm.n << "\tEasting: " << utm.e << endl;
    cout << "Latitude: " << latlon.latitude << "\tLongitude: " 
	 << latlon.longitude << endl << endl;
  }
}
