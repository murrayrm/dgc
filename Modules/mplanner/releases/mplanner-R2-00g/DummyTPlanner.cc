/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include "DummyTPlanner.hh"
#include "dgcutils/DGCutils.hh"
#include <pthread.h>
#include "interfaces/sn_types.h"
#include "interfaces/Status.hh"

using namespace std;

CTrafficPlanner::CTrafficPlanner(int sn_key, bool bWaitForStateFill)
	: CSkynetContainer(MODtrafficplanner, sn_key),
	  CStateClient(bWaitForStateFill)
{
  segGoalsStatus = new SegGoalsStatus();
  segGoalsStatus->goalID = 0;
  segGoalsStatus->status = SegGoalsStatus::COMPLETED;
}

void CTrafficPlanner::TPlanningLoop(void)
{
  int segGoalsStatusSocket = m_skynet.get_send_sock(SNtplannerStatus);
  int segGoalsSocket = m_skynet.listen(SNsegGoals, MODmissionplanner);
  if(segGoalsSocket < 0)
    cerr << "TrafficPlanner: skynet listen returned error" << endl;

  if(segGoalsStatusSocket < 0)
    cerr << "TrafficPlanner: skynet get_send_sock returned error" << endl;

  // Notify the mission planner that I'm joinning the group
  bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
  if (!statusSent)
    cout << "Error sending notification" << endl;
  else
    cout << "Successfully notified mission planner that I'm joining the group" <<  endl;
  
  SegGoals* segGoals = new SegGoals();
  int lastFailedGoalID = 0;
  // int numFailed = 0;
  // int goalIDtoFail = 12;
  // int numGoalFailedSent = 0;
  while(true)
  {
    bool segGoalsReceived = RecvSegGoals(segGoalsSocket, segGoals);

    if (segGoalsReceived)
    {
      cout << "segGoals " << segGoals->goalID << " received" << endl;
      segGoals->print();
      cout << endl;
      segGoalsStatus->goalID = segGoals->goalID;
      /*
      if (segGoals->goalID == goalIDtoFail && numFailed < 1)
      {
	segGoalsStatus->status = FAILED;	
	segGoalsStatus->currentSegmentID = 4;
	segGoalsStatus->currentLaneID = 1;
	segGoalsStatus->lastWaypointID = 6;
	numFailed++;
	//numGoalFailedSent++;
      }
      */
      if (segGoals->entrySegmentID == 4 && segGoals->entryLaneID == 1 && segGoals->entryWaypointID == 6 && 
	  segGoals->goalID != lastFailedGoalID)
      {
	segGoalsStatus->status = SegGoalsStatus::FAILED;	
	segGoalsStatus->currentSegmentID = 4;
	segGoalsStatus->currentLaneID = 1;
	segGoalsStatus->lastWaypointID = 6;
	lastFailedGoalID = segGoals->goalID;
      }
      else
	segGoalsStatus->status = SegGoalsStatus::COMPLETED;
	
      /*
      if (!(numGoalFailedSent == 1 && segGoals->goalID != goalIDtoFail)) {
	if (segGoals->goalID == goalIDtoFail)
	  numGoalFailedSent++;
      */

      sleep(3);
      cout << "Sending goal " << segGoalsStatus->goalID << " status: " << segGoalsStatus->status << endl;
      bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
      if (!statusSent)
	cout << "Error sending status" << endl;
      else
	cout << "Successfully sent status for goal " << segGoalsStatus->goalID << endl;
      // }
    }
  }
}
