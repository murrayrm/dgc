/*!**
 * Nok Wongpiromsarn
 * February 21, 2007
 */

#include <assert.h>
#include <sys/time.h>
#include <interfaces/StatePrecision.hh>
#include "MissionControl.hh"
#include "mcontrolDisplay.h"

using namespace std;

MissionControl::MissionControl(int skynetKey) :
  GcModule( "MissionControl", &m_contrStatus, &m_mergedDirective ),
  astateHealthTalker(skynetKey, SNastateHealth, MODmissionplanner)
{
  DGCcreateMutex(&m_contrDirectiveQMutex);
  m_snKey = skynetKey;
  m_nosparrow = true;
  m_logData = false;
  m_rndf = NULL;
  m_nextCheckpointIndex = 0;
  m_lastAddedCheckpointIndex = 0;
  m_missionCompleted = false;
  m_eomIssued = false;
  m_mergedDirective.id = 0;
  m_response.goalID = 0;
  m_missionCompleted = false;
  m_restart = false;
  m_isInit = false;
  m_currentMergedDirectiveID = 0;
  m_lastGoalID = 0;
  mrInterfaceSF = MRInterface::generateSouthface(skynetKey, this);
  mrInterfaceSF->setStaleThreshold(20);
  astateHealthTalker.listen();

  m_apxCount = 0;
  m_apxStatus = (int)NOT_CONNECTED;
  APXTIMEOUT = 500;

  m_lastPauseT = 0;

  // Initialize sparrow variables
  sparrowResponseGoalID = 0;
  for (int i=0; i < NUM_GOALS_DISPLAYED; i++)
  {
    sparrowNextGoalIDs[i] = 0;
    sparrowNextSegments[i] = 0;
    sparrowNextLanes[i] = 0;
    sparrowNextWaypoints[i] = 0;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MissionControl::~MissionControl() 
{
  // m_rndf is deleted by the RoutePlanner
  // m_logFile is closed by MissionPlannerMain
  // m_shp is deleted by MissionPlannerMain
  DGCdeleteMutex(&m_contrDirectiveQMutex);
  MRInterface::releaseSouthface(mrInterfaceSF);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::init(RNDF* rndf, char* RNDFFileName, char* MDFFileName, bool getApxStatus,
			  bool nosparrow, bool logData, FILE* logFile,
			  int debugLevel, bool verbose, CSparrowHawk* shp, int logLevel)
{
  m_shp = shp;
  m_rndf = rndf;
  m_RNDFFileName = RNDFFileName;
  m_MDFFileName = MDFFileName;
  m_MDFFileName = MDFFileName;
  m_nosparrow = nosparrow;
  m_logData = logData;
  m_logFile = logFile;
  m_getApxStatus = getApxStatus;
  DEBUG_LEVEL = debugLevel;

  if (verbose) {
    VERBOSITY_LEVEL = 1;
  }

  if (logLevel > 0) {
    stringstream ssfilename("");
    stringstream ssMDF;
    ssMDF << "./" << MDFFileName;
    string tmpMDFname;
    string MDFFileNameStr = ssMDF.str();
    tmpMDFname.assign(MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("/")+1, 
		      MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("."));
    ssfilename  << "./logs/mplanner-missioncontrol-" << tmpMDFname;
    this->addLogfile(ssfilename.str());
    this->setLogLevel(logLevel);
  }

  m_isInit = false;
  m_restart = false;
  m_missionCompleted = false;
  m_eomIssued = false;

  m_apxReceivedTime = getTime() + 5000000;

  if (!m_nosparrow)
    SparrowDisplayLoop();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  //  MissionControlStatus* contrStatus = dynamic_cast<MissionControlStatus*>(cs);
  MissionControlMergedDirective* mergedDirective = dynamic_cast<MissionControlMergedDirective*>(md);
  stringstream s("");

  if (!m_isInit) {
    if (!loadMDF(m_MDFFileName, m_rndf, mergedDirective)) {
      s.str("");
      s << "Unable to load MDF file " << m_MDFFileName << ", sending PAUSE command\n";
      printError("arbitrate", s.str());
      mergedDirective->id = mergedDirective->id + 1;
      mergedDirective->name = MissionControlMergedDirective::PAUSE;
    }
    else {
      mergedDirective->id = mergedDirective->id + 1;
      mergedDirective->name = MissionControlMergedDirective::MISSION;
    }
    m_isInit = true;
    m_restart = false;
  }
  else if (m_restart) { 
    s.str("");
    s << "MissionControl::arbitrate: Restart\n";
    print(s.str(), 0);
    mergedDirective->id = mergedDirective->id + 1;
    mergedDirective->name = MissionControlMergedDirective::MISSION;
    m_restart = false;  
    m_missionCompleted = false;
    m_eomIssued = false;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::control(ControlStatus* cs, MergedDirective* md)
{
  // MissionControlStatus* contrStatus = dynamic_cast<MissionControlStatus*> (cs);
  MissionControlMergedDirective* mergedDirective = dynamic_cast<MissionControlMergedDirective*>(md);
  stringstream s("");

  bool replan = false;
  sparrowNextCkpt = m_nextCheckpointIndex + 1;

  // Get astate health
  if (astateHealthTalker.hasNewMessage()) {
    bool astateModeReceived = astateHealthTalker.receive(&m_apxStatus);
    if (astateModeReceived) {
      m_apxCount++;
      m_apxReceivedTime = getTime();
      if (m_apxStatus == (int)NOT_CONNECTED  && m_getApxStatus) {
	s.str("");
	s << "astate health: NOT_CONNECTED\n";
	printError("control", s.str(), false);
	replan = true;
      }
      else if (m_apxStatus == (int)NO_MESSAGES && m_getApxStatus) {
	s.str("");
	s << "astate health: NO_MESSAGE\n";
	printError("control", s.str(), false);
	replan = true;
      }
      else if (m_apxStatus == (int)NO_SOLUTION && m_getApxStatus) {
	s.str("");
	s << "astate health: NO_SOLUTION\n";
	printError("control", s.str(), false);
	replan = true;
      }    
      else if (m_apxStatus == (int)INITIAL && m_getApxStatus) {
	s.str("");
	s << "astate health: INITIAL\n";
	printError("control", s.str(), false);
	replan = true;
      }
    }
  }
  else if (m_getApxStatus && (int)(getTime() - m_apxReceivedTime) > APXTIMEOUT*1000) {
    s.str("");
    s << "No applanix health message for " 
      << (getTime() - m_apxReceivedTime)/100 << " ms"
      << " (timeout = " << APXTIMEOUT << " ms, m_getApxStatus = "
      << m_getApxStatus << ")\n";
    printError("control", s.str(), false);
    replan = true;
  }
  
  if (mergedDirective->id != m_currentMergedDirectiveID) {
    s.str("");
    s << "MissionControl::control: Restart mission\n";
    print(s.str(), 0);

    m_nextCheckpointIndex = 0;
    m_lastAddedCheckpointIndex = 0;
    m_currentMergedDirectiveID = mergedDirective->id;
    
    s.str("");
    s << "MissionControl::control mergedDirectiveID = " << mergedDirective->id 
      << " m_currentMergedDirectiveID = " << m_currentMergedDirectiveID << "\n";
    print(s.str(), 0);
    replan = true;
  }

  if (mrInterfaceSF->haveNewStatus()) {
    m_response = *(mrInterfaceSF->getLatestStatus());
    sparrowResponseGoalID = (int)m_response.goalID;
    if (!m_nosparrow) {
      if (m_response.status == MControlDirectiveResponse::COMPLETED)
	m_shp->set_string("rplanner_status", "COMPLETED");
      else if (m_response.status == MControlDirectiveResponse::FAILED)
	m_shp->set_string("rplanner_status", "FAILED   ");
      else if (m_response.status == MControlDirectiveResponse::REJECTED)
	m_shp->set_string("rplanner_status", "REJECTED ");
      else if (m_response.status == MControlDirectiveResponse::ACCEPTED)
	m_shp->set_string("rplanner_status", "ACCEPTED ");
      else
	m_shp->set_string("rplanner_status", "UNKNOWN  ");
    }

    s.str();
    s << "MissionControl::control: Received response for goal " << m_response.goalID << "\n";
    print(s.str(), 2);

    if (m_response.goalID == 0) {
      replan = true;
      s.str("");
      s << "\n MissionControl: Route planner just starts listening\n";
      print(s.str(), -1);
    }

    if (m_response.status == MControlDirectiveResponse::FAILED) {
      if (m_response.reason != MControlDirectiveResponse::PREEMPTED_BY_PAUSE &&
	  m_response.reason != MControlDirectiveResponse::PREVIOUS_DIR_FAILURE) {
	replan = true;
	s.str("");
	s << "MissionControl: Goal " << m_response.goalID << " failed" << "\n";
	print(s.str(), -1);
      }
    }
    else if (m_response.status == MControlDirectiveResponse::COMPLETED) {
      MControlDirective completedDirective = mrInterfaceSF->getSentDirective(m_response.goalID);

      if ( completedDirective.name == MControlDirective::NEXT_CHECKPOINT ) {
	if (completedDirective.nextCheckpointIndex != m_nextCheckpointIndex) {
	  s.str("");
	  s << "received response for goal " 
	    << m_response.goalID << " : checkpoint " 
	    << completedDirective.nextCheckpointIndex +1 << " is crossed before checkpoint "
	    << m_nextCheckpointIndex +1 << ". Go back to checkpoint " << m_nextCheckpointIndex + 1 
	    << "\n";
	  printError("control", s.str());
	  replan = true;
	}
	else if (m_nextCheckpointIndex >= mergedDirective->checkpointSequence.size()) {
	  s.str("");
	  s << "checkpoint " << m_nextCheckpointIndex + 1
	    << " doesn't exist\n";
	  printError("control", s.str());
	}
	else {
	  s.str("");
	  s << "\ncheckpoint " << m_nextCheckpointIndex + 1 
	    << " (Waypoint " << completedDirective.segmentID << "."
	    << completedDirective.laneID << "." << completedDirective.waypointID
	    << ") is crossed\n\n";
	  print(s.str(), -1);
	  m_nextCheckpointIndex++;
	}
      }
      else if ( completedDirective.name == MControlDirective::PAUSE ) {
	// TODO
      }
      else if ( completedDirective.name == MControlDirective::END_OF_MISSION ){
#warning Misson Control does not handle the end of mission yet.
	if (!m_missionCompleted) {
	  m_missionCompleted = true;
	  s.str("");
	  s << "\nMISSION COMPLETED !!!\n\n";
	  print(s.str(), -1);
	}
      }
      else {
	s.str("");
	s << "unknown directive " << completedDirective.name << endl;
	printError("control", s.str());
      }
    }
    else {
      s.str("");
      s << "cannot handle status = " << m_response.status << "\n";
      printError("control", s.str());
      replan = true;
    } 
  } 

  sparrowNextCkpt = m_nextCheckpointIndex + 1;

  if (!replan) {
    DGClockMutex(&m_contrDirectiveQMutex);
    while( m_contrDirectiveQ.size() > 0 && m_response.goalID + LEAST_NUM_MISSION_RPLANNER_STORED >= 
	   (m_contrDirectiveQ.front()).goalID ) {
      sendDirective();
    }
    DGCunlockMutex(&m_contrDirectiveQMutex);
  }

  if (replan) {
    if (getTime() - m_lastPauseT > 2000000) {
      resetGoals();
      
      MControlDirective pause;
      pause.goalID = ++m_lastGoalID;
      pause.name = MControlDirective::PAUSE;
      DGClockMutex(&m_contrDirectiveQMutex);
      m_contrDirectiveQ.push_back(pause);
      m_lastPauseT = getTime();
      s.str("");
      s << "MissionControl: " << m_contrDirectiveQ.front().toString() << "\n";
      print(s.str(), 0);
      sendDirective();
      DGCunlockMutex(&m_contrDirectiveQMutex);
      
      m_lastAddedCheckpointIndex = m_nextCheckpointIndex;
      DGClockMutex(&m_contrDirectiveQMutex);
      while (!m_eomIssued && m_contrDirectiveQ.size() < LEAST_NUM_MISSION_STORED) {
	MControlDirective directive = 
	  findNextCheckpointDirective(mergedDirective->checkpointSequence,
				      m_lastAddedCheckpointIndex);
	if (directive.name == MControlDirective::END_OF_MISSION)
	  m_eomIssued = true;
	m_contrDirectiveQ.push_back(directive);
	s.str("");
	s << "MissionControl: " << m_contrDirectiveQ.back().toString() << "\n";
	print(s.str(), 0);
	m_lastAddedCheckpointIndex++;
      }
      DGCunlockMutex(&m_contrDirectiveQMutex);
    }
  }
  else {
    DGClockMutex(&m_contrDirectiveQMutex);
    while (!m_eomIssued && m_contrDirectiveQ.size() < LEAST_NUM_MISSION_STORED) {
      MControlDirective directive = 
	findNextCheckpointDirective(mergedDirective->checkpointSequence,
				    m_lastAddedCheckpointIndex);
      if (directive.name == MControlDirective::END_OF_MISSION)
	m_eomIssued = true;
      m_contrDirectiveQ.push_back(directive);
      s.str("");
      s << "MissionControl: " << m_contrDirectiveQ.back().toString() << "\n";
      print(s.str(), 0);
      m_lastAddedCheckpointIndex++;
    }
    DGCunlockMutex(&m_contrDirectiveQMutex);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MControlDirective MissionControl::findNextCheckpointDirective(vector<Waypoint*> checkpointSequence,
							      unsigned nextCheckpointIndex)
{
  MControlDirective directive;
  directive.goalID = m_lastGoalID + 1;
  stringstream s("");

  if(nextCheckpointIndex < checkpointSequence.size()) {
    m_lastGoalID = directive.goalID;
    directive.name = MControlDirective::NEXT_CHECKPOINT;
    if ( nextCheckpointIndex >= checkpointSequence.size() ) {
      s.str("");
      s << "invalid nextCheckpointIndex = "
	<< nextCheckpointIndex << "\n";
      printError("findNextCheckpointDirective", s.str());
      directive.nextCheckpointIndex = checkpointSequence.size() - 1;
    }
    else {
      directive.nextCheckpointIndex = nextCheckpointIndex;
    }
    directive.vehicleCap = MAX_CAP;
    directive.segmentID = checkpointSequence[directive.nextCheckpointIndex]->getSegmentID();
    directive.laneID = checkpointSequence[directive.nextCheckpointIndex]->getLaneID();
    directive.waypointID = checkpointSequence[directive.nextCheckpointIndex]->getWaypointID();
  }
  // We complete the mission    
  else {
    m_lastGoalID = directive.goalID;
    directive.name = MControlDirective::END_OF_MISSION;
  }

  return directive;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::resetGoals()
{
  DGClockMutex(&m_contrDirectiveQMutex);
  m_contrDirectiveQ.clear();
  DGCunlockMutex(&m_contrDirectiveQMutex);
  m_eomIssued = false;
  stringstream s("");
  s << "MissionControl: Clear contrGcPort\n";
  print(s.str(), 0);

  mrInterfaceSF->flushAll();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::sendDirective()
{
  stringstream s("");
  if( m_contrDirectiveQ.size() == 0) {
    s.str("");
    s << "ERROR: MissionControl::sendDirective: m_contrDirectiveQ.size() == 0\n";
    printError("sendDirective", s.str());
    return;
  }

  s.str("");
  s << "MissionControl: Adding goal " << (m_contrDirectiveQ.front()).goalID 
    << " to contrGcPortMsgQ.\n";
  print(s.str(), 0);
  mrInterfaceSF->sendDirective(&m_contrDirectiveQ.front());
  m_contrDirectiveQ.pop_front();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool MissionControl::loadMDF(char* fileName, RNDF* rndf,
			     MissionControlMergedDirective* mergedDirective)
{
  ifstream file;
  string line;
  string word;

  file.open(fileName, ios::in);

  if(!file)
  {
    stringstream s("");
    s << fileName << " file not found.\n";
    printError("loadMDF", s.str());
    return false;
  } 
  
  getline(file, line);
    
  istringstream lineStream(line, ios::in);
  
  lineStream >> word;

  if(word == "MDF_name")
  {
    parseCheckpoint(&file, rndf, mergedDirective);
    file.close();
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::parseCheckpoint(ifstream* file, RNDF* rndf, 
				     MissionControlMergedDirective* mergedDirective)
{
  string line, word;
  char letter;
  int ckptID;  
  
  while(word != "end_checkpoints")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      ckptID = atoi(word.c_str());
      mergedDirective->checkpointSequence.push_back(rndf->getCheckpoint(ckptID));
    }
    else
    {
      continue;
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::print(string mess, int d)
{
  stringstream s("");
  s << getTime() << "\t" << mess;
  if (DEBUG_LEVEL > d) {
    if (m_nosparrow) {
      cout << mess;
    }
    else {
      SparrowHawk().log(mess);
    }
  }
  if (m_logData) {
    fprintf(m_logFile, s.str().c_str());
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::printError(string funcName, string errorMessage, bool error)
{
  stringstream s1("");
  if (error)
    s1 << "\nERROR: MissionControl::" << funcName << " : ";
  s1 << errorMessage << "\n";
  cerr << s1.str();

  stringstream s2("");
  s2 << getTime() << "\t" << s1.str();
  if (m_logData) {
    fprintf(m_logFile, s2.str().c_str());
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
uint64_t MissionControl::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Sparrow-related functions */
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::SparrowDisplayLoop()
{
  m_shp->add_page(mcontroltable, "MissionControl");

  m_shp->rebind("mcsnkey", &m_snKey);
  m_shp->set_readonly("mcsnkey");

  m_shp->set_string("mcRNDFFileName", m_RNDFFileName);
  m_shp->set_readonly("mcRNDFFileName");

  m_shp->set_string("mcMDFFileName", m_MDFFileName);
  m_shp->set_readonly("mcMDFFileName");

  m_shp->rebind("apxCount", &m_apxCount);
  m_shp->rebind("apxStatus", &m_apxStatus);
  m_shp->rebind("recvApx", &m_getApxStatus);
  m_shp->rebind("apxTimeout", &APXTIMEOUT);
  m_shp->set_readonly("apxCount");
  m_shp->set_readonly("apxStatus");

  m_shp->rebind("rplanner_goalID", &(sparrowResponseGoalID));
  m_shp->set_string("rplanner_status", "UNKNOWN");
  m_shp->set_readonly("rplanner_goalID");
  m_shp->set_readonly("rplanner_status");

  m_shp->rebind("mcnextGoal1ID", &(sparrowNextGoalIDs[0]));
  m_shp->rebind("mcnextGoal1_segment", &(sparrowNextSegments[0]));
  m_shp->rebind("mcnextGoal1_lane", &(sparrowNextLanes[0]));
  m_shp->rebind("mcnextGoal1_waypoint", &(sparrowNextWaypoints[0]));
  m_shp->set_string("mcnextGoal1_type", "UNKNOWN");
  m_shp->rebind("mcnextCkpt", &sparrowNextCkpt);
  m_shp->set_readonly("mcnextGoal1ID");
  m_shp->set_readonly("mcnextGoal1_segment");
  m_shp->set_readonly("mcnextGoal1_lane");
  m_shp->set_readonly("mcnextGoal1_waypoint");
  m_shp->set_readonly("mcnextGoal1_type");
  m_shp->set_readonly("mcnextCkpt");

  m_shp->rebind("mcnextGoal2ID", &(sparrowNextGoalIDs[1]));
  m_shp->set_string("mcnextGoal2_type", "UNKNOWN");
  m_shp->set_readonly("mcnextGoal2ID");
  m_shp->set_readonly("mcnextGoal2_type");

  m_shp->rebind("mcnextGoal3ID", &(sparrowNextGoalIDs[2]));
  m_shp->set_string("mcnextGoal3_type", "UNKNOWN");
  m_shp->set_readonly("mcnextGoal3ID");
  m_shp->set_readonly("mcnextGoal3_type");

  m_shp->rebind("mcnextGoal4ID", &(sparrowNextGoalIDs[3]));
  m_shp->set_string("mcnextGoal4_type", "UNKNOWN");
  m_shp->set_readonly("mcnextGoal4ID");
  m_shp->set_readonly("mcnextGoal4_type");

  m_shp->set_notify("sparrowRestartMission", this, &MissionControl::sparrowRestartMission);
  m_shp->set_notify("sparrowQuit", this, &MissionControl::sparrowQuit);
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::UpdateSparrowVariablesLoop()
{
  while(true) {
    deque<unsigned> execGoalIDs = mrInterfaceSF->orderedDirectivesWaitingForResponse();
    deque<MControlDirective> executingGoals;

    for (unsigned i=0; i<execGoalIDs.size(); i++) {
      executingGoals.push_back(mrInterfaceSF->getSentDirective(execGoalIDs[i]));
    }

    int i = 0;
    deque<MControlDirective> mission;	
    DGClockMutex(&m_contrDirectiveQMutex);
    if (m_contrDirectiveQ.size() > 0) {
      mission.assign(m_contrDirectiveQ.begin(), m_contrDirectiveQ.end());
    }
    DGCunlockMutex(&m_contrDirectiveQMutex);

    char* nextGoalType;

    while (i < NUM_GOALS_DISPLAYED && executingGoals.size() > 0) {
      sparrowNextGoalIDs[i] = (executingGoals.front()).goalID;
      sparrowNextSegments[i] = (executingGoals.front()).segmentID;
      sparrowNextLanes[i] = (executingGoals.front()).laneID;
      sparrowNextWaypoints[i] = (executingGoals.front()).waypointID;

      switch (i) {
      case 0:
	nextGoalType = "mcnextGoal1_type";
	break;
      case 1:
	nextGoalType = "mcnextGoal2_type";
          break;
      case 2:
	nextGoalType = "mcnextGoal3_type";
	break;
      default:
	nextGoalType = "mcnextGoal4_type";
	break;
      }

      if ((executingGoals.front()).name == MControlDirective::NEXT_CHECKPOINT) {
        m_shp->set_string(nextGoalType, "NEXT_CHECKPOINT");
      }
      else if ((executingGoals.front()).name == MControlDirective::END_OF_MISSION) {
        m_shp->set_string(nextGoalType, "END_OF_MISSION ");
      }
      else if ((executingGoals.front()).name == MControlDirective::PAUSE) {
        m_shp->set_string(nextGoalType, "PAUSE          ");
      }
      else if ((executingGoals.front()).name == MControlDirective::DRIVE_FORWARD) {
        m_shp->set_string(nextGoalType, "DRIVE_FORWARD  ");
      }
      else if ((executingGoals.front()).name == MControlDirective::BACK_UP) {
        m_shp->set_string(nextGoalType, "BACK_UP        ");
      }
      else {
        m_shp->set_string(nextGoalType, "UNKNOWN");
      }

      executingGoals.pop_front();
      i++;
    }

    while (i < NUM_GOALS_DISPLAYED && mission.size() > 0) {
      sparrowNextGoalIDs[i] = (mission.front()).goalID;
      sparrowNextSegments[i] = (mission.front()).segmentID;
      sparrowNextLanes[i] = (mission.front()).laneID;
      sparrowNextWaypoints[i] = (mission.front()).waypointID;

      switch (i) {
      case 0:
	nextGoalType = "mcnextGoal1_type";
	break;
      case 1:
	nextGoalType = "mcnextGoal2_type";
          break;
      case 2:
	nextGoalType = "mcnextGoal3_type";
	break;
      default:
	nextGoalType = "mcnextGoal4_type";
	break;
      }

      if ((mission.front()).name == MControlDirective::NEXT_CHECKPOINT) {
        m_shp->set_string(nextGoalType, "NEXT_CHECKPOINT");
      }
      else if ((mission.front()).name == MControlDirective::END_OF_MISSION) {
        m_shp->set_string(nextGoalType, "END_OF_MISSION ");
      }
      else if ((mission.front()).name == MControlDirective::PAUSE) {
        m_shp->set_string(nextGoalType, "PAUSE          ");
      }
      else if ((mission.front()).name == MControlDirective::DRIVE_FORWARD) {
        m_shp->set_string(nextGoalType, "DRIVE_FORWARD  ");
      }
      else if ((mission.front()).name == MControlDirective::BACK_UP) {
        m_shp->set_string(nextGoalType, "BACK_UP        ");
      }
      else {
        m_shp->set_string(nextGoalType, "UNKNOWN");
      }

      mission.pop_front();
      i++;
    }

    usleep(500000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::sparrowRestartMission()
{
  string s = "Restart\n";
  printError("sparrowRestartMission", s, false);
  m_restart = true;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::sparrowQuit()
{
  string quitMess = "Quit\n";
  printError("sparrowQuit", quitMess, false);

  MControlDirective pause;
  pause.goalID = ++m_lastGoalID;
  pause.name = MControlDirective::PAUSE;
  DGClockMutex(&m_contrDirectiveQMutex);
  m_contrDirectiveQ.clear();

  stringstream s("");
  s << "MissionControl: Clear contrGcPort\n";
  print(s.str(), 0);

  mrInterfaceSF->flushAll();

  m_contrDirectiveQ.push_back(pause);
  s.str("");
  s << "MissionControl: " << m_contrDirectiveQ.front().toString() << "\n";
  print(s.str(), -1);
  sendDirective();
  DGCunlockMutex(&m_contrDirectiveQMutex);

  usleep(500000);
  exit(0);
}

