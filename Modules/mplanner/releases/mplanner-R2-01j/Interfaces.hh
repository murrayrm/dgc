#ifndef MISSIONPLANNERINTERNALINTERFACES_HH
#define MISSIONPLANNERINTERNALINTERFACES_HH

#include "gcmodule/GcInterface.hh"

enum VehicleCap{ MAX_CAP, MEDIUM_CAP, MIN_CAP };

// Directive from Mission Control
struct MControlDirective
{
  enum DirectiveName{ NEXT_CHECKPOINT, END_OF_MISSION, PAUSE };
  MControlDirective()
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int goalID;
  DirectiveName name;
  // Parameters for NEXT_CHECKPOINT
  // The waypoint id of the next checkpoint
  int segmentID;
  int laneID;
  int waypointID;
  int nextCheckpointIndex;
  VehicleCap vehicleCap;
};


struct MControlDirectiveResponse
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ NO_ROUTE, VEHICLE_CAP };
  MControlDirectiveResponse()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int goalID;
  Status status;
  ReasonForFailure reason;
};

#define SNmission SNtrajfollowerTabOutput
#define SNrplannerStatus SNtrajfollowerTabInput

typedef GcInterface<MControlDirective, MControlDirectiveResponse, 
		    SNmission, SNrplannerStatus, MODmissionplanner> MRInterface ;

#endif //MISSIONPLANNERINTERNALINTERFACES_HH

