
/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */


#include "RoutePlanner.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "mplannerDisplay.h"

using namespace std;

RoutePlanner::RoutePlanner(int skynetKey, bool waitForStateFill, bool nosparrow,
    char* RNDFFileName, char* MDFFileName, bool listenToMapper, bool logData, 
    FILE* logFile, int debugLevel, bool verbose)
  : GcModule( "RoutePlanner", &m_controlStatus, &m_mergedDirective, 100000, 100000 ),
    m_snKey(skynetKey), m_nosparrow(nosparrow), 
    m_graphEstimator(skynetKey,  waitForStateFill)
{  
  DEBUG_LEVEL = debugLevel;
  //this->setLogLevel(9);
  
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }
  m_logData = logData;
  m_logFile = logFile;

  DGCcreateMutex(&m_contrDirectiveQMutex);

  m_RNDFFileName = RNDFFileName;
  m_MDFFileName = MDFFileName;

  m_rndf = new RNDF();
  if (!m_rndf->loadFile(RNDFFileName))
  {
    cerr << "ERROR:  Unable to load RNDF file " << RNDFFileName
	 << ", exiting program" << endl;
    if (m_logData)
    {
      fprintf( m_logFile, "ERROR:  Unable to load RNDF file %s, exiting program\n", RNDFFileName);
    }
    exit(1);
  }
  m_rndf->assignLaneDirection();
    
  if (DEBUG_LEVEL > 2)
  {
    m_rndf->print();
    cout << "Finished rndf" << endl;
  }
  else if (DEBUG_LEVEL > 0)
  {
    for (int i=1; i <= m_rndf->getNumOfSegments(); i++)
    {
      for (int j=1; j <= m_rndf->getSegment(i)->getNumOfLanes(); j++)
      {
        cout << "Lane " << i << "." << j << ":\t\t"
             << m_rndf->getLane(i,j)->getDirection() << endl;
      }
    } 
  }
  if (m_logData)
  {
    fprintf ( m_logFile, "\n" );
    for (int i=1; i <= m_rndf->getNumOfSegments(); i++)
    {
      for (int j=1; j <= m_rndf->getSegment(i)->getNumOfLanes(); j++)
      {
	fprintf ( m_logFile, "Lane %d.%d: \t %d\n", i, j, m_rndf->getLane(i,j)->getDirection() );
      }
    }
    fprintf( m_logFile, "Finished rndf\n" );
  }
 
  rtInterfaceSF = NULL;
  rtInterface = new RTInterface( skynetKey, this );
  if( rtInterface )
    rtInterfaceSF = rtInterface->getSouthface();

  m_missionControl.init(m_rndf, MDFFileName, nosparrow, m_logData, m_logFile, debugLevel, verbose);
  m_controlStatus.rpcs.goalID = 0;
  m_controlStatus.rpcs.status = MControlDirectiveResponse::READY_FOR_NEXT;
  //  arbitrate(&m_controlStatus, &m_mergedDirective);

  m_graphEstimator.init(m_rndf, listenToMapper, nosparrow, m_logData, m_logFile, debugLevel, verbose);
  m_travGraph = &m_graphEstimator.getGraph();

  m_minSpeedLimits.resize(m_travGraph->getNumOfSegments() + m_travGraph->getNumOfZones() + 1);
  m_maxSpeedLimits.resize(m_travGraph->getNumOfSegments() + m_travGraph->getNumOfZones() + 1);

  for (unsigned i=0; i < m_minSpeedLimits.size(); i++)
  {
    m_minSpeedLimits[i] = MIN_SPEED_LIMIT;
    m_maxSpeedLimits[i] = MAX_SPEED_LIMIT;
  }

  if (!loadMDFFile(MDFFileName, m_rndf))
  {
    cerr << "Error: Unable to load MDF file " << MDFFileName << ", exiting program"
         << endl;
    exit(1);
  }

  if (DEBUG_LEVEL > 1)
  {
    cout << endl << endl << "RNDFGRAPH:" << endl;
    m_travGraph->print();
    cout << "Finished RNDFGRAPH" << endl;
  }
  if (m_logData)
  {
    fprintf ( m_logFile, "\nGraph:\n" );
    m_travGraph->log(m_logFile);
    fprintf ( m_logFile, "Finished RNDFGRAPH\n" );
    m_travGraph->log(m_logFile);
  }

  m_currentCkptSegmentID = 0;
  m_currentCkptLaneID = 0;
  m_currentCkptWaypointID = 0;
  m_missionID = 0;
  m_nextGoalID = 1;
  m_mergedDirectiveGoalID = 0;
  m_lastPlannedMergedDirectiveID = -1;
  m_prevMergedDirectiveGoalID = 0;

  // Initialize sparrow variables
  for (int i=0; i < NUM_SEGGOALS_DISPLAYED; i++)
  {
    sparrowNextGoalIDs[i] = 0;
    sparrowNextSegments1[i] = 0;
    sparrowNextLanes1[i] = 0;
    sparrowNextWaypoints1[i] = 0;
    sparrowNextSegments2[i] = 0;
    sparrowNextLanes2[i] = 0;
    sparrowNextWaypoints2[i] = 0;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
RoutePlanner::~RoutePlanner() 
{
  cerr << "I'm the destructor...Don't call me" << endl;
  delete m_rndf;
  delete m_travGraph;
  delete rtInterface;
  delete [] sparrowNextGoalIDs;
  delete [] sparrowNextSegments1;
  delete [] sparrowNextLanes1;
  delete [] sparrowNextWaypoints1;
  delete [] sparrowNextSegments2;
  delete [] sparrowNextLanes2;
  delete [] sparrowNextWaypoints2;

  DGCdeleteMutex(&m_contrDirectiveQMutex);
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Sparrow-related functions */
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::SparrowDisplayLoop()
{
  // SparrowHawk display
  CSparrowHawk &sh = SparrowHawk();  
  shp = &sh;
  
  shp->add_page(mplannertable, "Main");
  shp->rebind("snkey", &m_snKey);
  shp->rebind("tplanner_goalID", &(m_segGoalsStatus.goalID));
  shp->set_string("tplanner_status", "UNKNOWN");
  shp->set_readonly("snkey");
  shp->set_readonly("tplanner_goalID");
  shp->set_readonly("tplanner_status");

  shp->set_string("RNDFFileName", m_RNDFFileName);
  shp->set_string("MDFFileName", m_MDFFileName);
  shp->set_readonly("RNDFFileName");
  shp->set_readonly("MDFFileName");

  shp->rebind("nextGoal1ID", &(sparrowNextGoalIDs[0]));
  shp->rebind("nextGoal1_segment1", &(sparrowNextSegments1[0]));
  shp->rebind("nextGoal1_lane1", &(sparrowNextLanes1[0]));
  shp->rebind("nextGoal1_waypoint1", &(sparrowNextWaypoints1[0]));
  shp->rebind("nextGoal1_segment2", &(sparrowNextSegments2[0]));
  shp->rebind("nextGoal1_lane2", &(sparrowNextLanes2[0]));
  shp->rebind("nextGoal1_waypoint2", &(sparrowNextWaypoints2[0]));
  shp->set_string("nextGoal1_type", "UNKNOWN");
  shp->rebind("minSpeed", &sparrowMinSpeed);
  shp->rebind("maxSpeed", &sparrowMaxSpeed);
  shp->rebind("illegalPassingAllowed", &sparrowIllegalPassingAllowed);
  shp->rebind("stopAtExit", &sparrowStopAtExit);
  shp->rebind("isExitCheckpoint", &sparrowIsExitCheckpoint);
  shp->set_readonly("nextGoal1ID");
  shp->set_readonly("nextGoal1_segment1");
  shp->set_readonly("nextGoal1_lane1");
  shp->set_readonly("nextGoal1_waypoint1");
  shp->set_readonly("nextGoal1_segment2");
  shp->set_readonly("nextGoal1_lane2");
  shp->set_readonly("nextGoal1_waypoint2");
  shp->set_readonly("nextGoal1_type");
  shp->set_readonly("minSpeed");
  shp->set_readonly("maxSpeed");
  shp->set_readonly("illegalPassingAllowed");
  shp->set_readonly("stopAtExit");
  shp->set_readonly("isExitCheckpoint");

  shp->rebind("nextGoal2ID", &(sparrowNextGoalIDs[1]));
  shp->rebind("nextGoal2_segment1", &(sparrowNextSegments1[1]));
  shp->rebind("nextGoal2_lane1", &(sparrowNextLanes1[1]));
  shp->rebind("nextGoal2_waypoint1", &(sparrowNextWaypoints1[1]));
  shp->rebind("nextGoal2_segment2", &(sparrowNextSegments2[1]));
  shp->rebind("nextGoal2_lane2", &(sparrowNextLanes2[1]));
  shp->rebind("nextGoal2_waypoint2", &(sparrowNextWaypoints2[1]));
  shp->set_string("nextGoal2_type", "UNKNOWN");
  shp->set_readonly("nextGoal2ID");
  shp->set_readonly("nextGoal2_segment1");
  shp->set_readonly("nextGoal2_lane1");
  shp->set_readonly("nextGoal2_waypoint1");
  shp->set_readonly("nextGoal2_segment2");
  shp->set_readonly("nextGoal2_lane2");
  shp->set_readonly("nextGoal2_waypoint2");
  shp->set_readonly("nextGoal2_type");

  shp->rebind("nextGoal3ID", &(sparrowNextGoalIDs[2]));
  shp->rebind("nextGoal3_segment1", &(sparrowNextSegments1[2]));
  shp->rebind("nextGoal3_lane1", &(sparrowNextLanes1[2]));
  shp->rebind("nextGoal3_waypoint1", &(sparrowNextWaypoints1[2]));
  shp->rebind("nextGoal3_segment2", &(sparrowNextSegments2[2]));
  shp->rebind("nextGoal3_lane2", &(sparrowNextLanes2[2]));
  shp->rebind("nextGoal3_waypoint2", &(sparrowNextWaypoints2[2]));
  shp->set_string("nextGoal3_type", "UNKNOWN");
  shp->set_readonly("nextGoal3ID");
  shp->set_readonly("nextGoal3_segment1");
  shp->set_readonly("nextGoal3_lane1");
  shp->set_readonly("nextGoal3_waypoint1");
  shp->set_readonly("nextGoal3_segment2");
  shp->set_readonly("nextGoal3_lane2");
  shp->set_readonly("nextGoal3_waypoint2");
  shp->set_readonly("nextGoal3_type");

  shp->rebind("nextGoal4ID", &(sparrowNextGoalIDs[3]));
  shp->rebind("nextGoal4_segment1", &(sparrowNextSegments1[3]));
  shp->rebind("nextGoal4_lane1", &(sparrowNextLanes1[3]));
  shp->rebind("nextGoal4_waypoint1", &(sparrowNextWaypoints1[3]));
  shp->rebind("nextGoal4_segment2", &(sparrowNextSegments2[3]));
  shp->rebind("nextGoal4_lane2", &(sparrowNextLanes2[3]));
  shp->rebind("nextGoal4_waypoint2", &(sparrowNextWaypoints2[3]));
  shp->set_string("nextGoal4_type", "UNKNOWN");
  shp->set_readonly("nextGoal4ID");
  shp->set_readonly("nextGoal4_segment1");
  shp->set_readonly("nextGoal4_lane1");
  shp->set_readonly("nextGoal4_waypoint1");
  shp->set_readonly("nextGoal4_segment2");
  shp->set_readonly("nextGoal4_lane2");
  shp->set_readonly("nextGoal4_waypoint2");
  shp->set_readonly("nextGoal4_type");

  shp->rebind("nextGoal5ID", &(sparrowNextGoalIDs[4]));
  shp->rebind("nextGoal5_segment1", &(sparrowNextSegments1[4]));
  shp->rebind("nextGoal5_lane1", &(sparrowNextLanes1[4]));
  shp->rebind("nextGoal5_waypoint1", &(sparrowNextWaypoints1[4]));
  shp->rebind("nextGoal5_segment2", &(sparrowNextSegments2[4]));
  shp->rebind("nextGoal5_lane2", &(sparrowNextLanes2[4]));
  shp->rebind("nextGoal5_waypoint2", &(sparrowNextWaypoints2[4]));
  shp->set_string("nextGoal5_type", "UNKNOWN");
  shp->set_readonly("nextGoal5ID");
  shp->set_readonly("nextGoal5_segment1");
  shp->set_readonly("nextGoal5_lane1");
  shp->set_readonly("nextGoal5_waypoint1");
  shp->set_readonly("nextGoal5_segment2");
  shp->set_readonly("nextGoal5_lane2");
  shp->set_readonly("nextGoal5_waypoint2");
  shp->set_readonly("nextGoal5_type");

  shp->rebind("nextGoal6ID", &(sparrowNextGoalIDs[5]));
  shp->rebind("nextGoal6_segment1", &(sparrowNextSegments1[5]));
  shp->rebind("nextGoal6_lane1", &(sparrowNextLanes1[5]));
  shp->rebind("nextGoal6_waypoint1", &(sparrowNextWaypoints1[5]));
  shp->rebind("nextGoal6_segment2", &(sparrowNextSegments2[5]));
  shp->rebind("nextGoal6_lane2", &(sparrowNextLanes2[5]));
  shp->rebind("nextGoal6_waypoint2", &(sparrowNextWaypoints2[5]));
  shp->set_string("nextGoal6_type", "UNKNOWN");
  shp->set_readonly("nextGoal6ID");
  shp->set_readonly("nextGoal6_segment1");
  shp->set_readonly("nextGoal6_lane1");
  shp->set_readonly("nextGoal6_waypoint1");
  shp->set_readonly("nextGoal6_segment2");
  shp->set_readonly("nextGoal6_lane2");
  shp->set_readonly("nextGoal6_waypoint2");
  shp->set_readonly("nextGoal6_type");

  shp->rebind("nextCheckpointIndex", &m_missionID);
  shp->rebind("nextCheckpointSegmentID", &m_currentCkptSegmentID);
  shp->rebind("nextCheckpointLaneID", &m_currentCkptLaneID);
  shp->rebind("nextCheckpointWaypointID", &m_currentCkptWaypointID);
  shp->rebind("nextCheckpointGoalID", &m_prevMergedDirectiveGoalID);
  shp->set_readonly("nextCheckpointIndex");
  shp->set_readonly("nextCheckpointSegmentID");
  shp->set_readonly("nextCheckpointLaneID");
  shp->set_readonly("nextCheckpointWaypointID");
  shp->set_readonly("nextCheckpointGoalID");
  

  shp->rebind("obstacleID", &obstacleID);
  shp->rebind("obstacleSegmentID", &obstacleSegmentID);
  shp->rebind("obstacleLaneID", &obstacleLaneID);
  shp->rebind("obstacleWaypointID", &obstacleWaypointID);

  shp->set_notify("sparrowInsertObstacle", this, &RoutePlanner::sparrowInsertObstacle);
  shp->set_notify("sparrowRestartMission", this, &RoutePlanner::sparrowRestartMission);
  shp->run();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::UpdateSparrowVariablesLoop()
{

  while(true)
  {
    deque<unsigned> execGoalIDs = rtInterfaceSF->orderedDirectivesWaitingForResponse();
    deque<SegGoals> executingGoals;

    for (unsigned i=0; i<execGoalIDs.size(); i++)
    {
      executingGoals.push_back(rtInterfaceSF->getSentDirective(execGoalIDs[i]));
    }
    if (executingGoals.size() > 0)
    {
      sparrowMinSpeed = executingGoals.front().minSpeedLimit;
      sparrowMaxSpeed = executingGoals.front().maxSpeedLimit;
      sparrowIllegalPassingAllowed = executingGoals.front().illegalPassingAllowed;
      sparrowStopAtExit = executingGoals.front().stopAtExit;
      sparrowIsExitCheckpoint = executingGoals.front().isExitCheckpoint;
    }

    /*
    deque<RPlannerContrMsgWrapper> executingGoals;
    DGClockMutex(&m_contrGcPortMutex);	
    if (m_contrGcPortMsgQ.size() > (unsigned)m_firstQueuedMsgIndex)
    {
      executingGoals.assign(m_contrGcPortMsgQ.begin() + m_firstQueuedMsgIndex, m_contrGcPortMsgQ.end());
      sparrowMinSpeed = executingGoals.front().directive.minSpeedLimit;
      sparrowMaxSpeed = executingGoals.front().directive.maxSpeedLimit;
      sparrowIllegalPassingAllowed = executingGoals.front().directive.illegalPassingAllowed;
      sparrowStopAtExit = executingGoals.front().directive.stopAtExit;
      sparrowIsExitCheckpoint = executingGoals.front().directive.isExitCheckpoint;
    }
    DGCunlockMutex(&m_contrGcPortMutex);	
    */

    if (m_segGoalsStatus.status == SegGoalsStatus::ACCEPTED)
    {
      shp->set_string("tplanner_status", "ACCEPT   ");
    }
    else if (m_segGoalsStatus.status == SegGoalsStatus::REJECTED)
    {
      shp->set_string("tplanner_status", "REJECT   ");
    }
    else if (m_segGoalsStatus.status == SegGoalsStatus::COMPLETED)
    {
      shp->set_string("tplanner_status", "COMPLETED");
    }
    else if (m_segGoalsStatus.status == SegGoalsStatus::FAILED)
    {
      shp->set_string("tplanner_status", "FAILED   ");
    }
    else
    {
      shp->set_string("tplanner_status", "UNKNOWN  ");
    }

    int i = 0;
    deque<SegGoals> segGoalsSeq;	
    DGClockMutex(&m_contrDirectiveQMutex);
    if (m_contrDirectiveQ.size() > 0)
    {
      segGoalsSeq.assign(m_contrDirectiveQ.begin(), m_contrDirectiveQ.end());
    }
    DGCunlockMutex(&m_contrDirectiveQMutex);

    char* nextGoalType;

    while (i < NUM_SEGGOALS_DISPLAYED && executingGoals.size() > 0)
    {
      sparrowNextGoalIDs[i] = (executingGoals.front()).goalID;
      sparrowNextSegments1[i] = (executingGoals.front()).entrySegmentID;
      sparrowNextLanes1[i] = (executingGoals.front()).entryLaneID;
      sparrowNextWaypoints1[i] = (executingGoals.front()).entryWaypointID;
      sparrowNextSegments2[i] = (executingGoals.front()).exitSegmentID;
      sparrowNextLanes2[i] = (executingGoals.front()).exitLaneID;
      sparrowNextWaypoints2[i] = (executingGoals.front()).exitWaypointID;

      switch (i)
      {
        case 0:
          nextGoalType = "nextGoal1_type";
          break;
        case 1:
          nextGoalType = "nextGoal2_type";
          break;
        case 2:
          nextGoalType = "nextGoal3_type";
          break;
        case 3:
          nextGoalType = "nextGoal4_type";
          break;
        case 4:
          nextGoalType = "nextGoal5_type";
          break;
        default:
          nextGoalType = "nextGoal6_type";
      }

      if ((executingGoals.front()).segment_type == SegGoals::ROAD_SEGMENT)
      {
        shp->set_string(nextGoalType, "ROAD_SEGMENT");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::PARKING_ZONE)
      {
        shp->set_string(nextGoalType, "PARKING_ZONE");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::INTERSECTION)
      {
        shp->set_string(nextGoalType, "INTERSECTION");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::PREZONE)
      {
        shp->set_string(nextGoalType, "PREZONE     ");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::UTURN)
      {
        shp->set_string(nextGoalType, "UTURN       ");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::PAUSE)
      {
        shp->set_string(nextGoalType, "PAUSE       ");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::END_OF_MISSION)
      {
        shp->set_string(nextGoalType, "END_OF_MISSION");
      }
      else
      {
        shp->set_string(nextGoalType, "UNKNOWN");
      }

      executingGoals.pop_front();
      i++;
    }

    while (i < NUM_SEGGOALS_DISPLAYED && segGoalsSeq.size() > 0)
    {
      sparrowNextGoalIDs[i] = (segGoalsSeq.front()).goalID;
      sparrowNextSegments1[i] = (segGoalsSeq.front()).entrySegmentID;
      sparrowNextLanes1[i] = (segGoalsSeq.front()).entryLaneID;
      sparrowNextWaypoints1[i] = (segGoalsSeq.front()).entryWaypointID;
      sparrowNextSegments2[i] = (segGoalsSeq.front()).exitSegmentID;
      sparrowNextLanes2[i] = (segGoalsSeq.front()).exitLaneID;
      sparrowNextWaypoints2[i] = (segGoalsSeq.front()).exitWaypointID;

      switch (i)
      {
        case 0:
          nextGoalType = "nextGoal1_type";
          break;
        case 1:
          nextGoalType = "nextGoal2_type";
          break;
        case 2:
          nextGoalType = "nextGoal3_type";
          break;
        case 3:
          nextGoalType = "nextGoal4_type";
          break;
        case 4:
          nextGoalType = "nextGoal5_type";
          break;
        default:
          nextGoalType = "nextGoal6_type";
      }

      if ((segGoalsSeq.front()).segment_type == SegGoals::ROAD_SEGMENT)
      {
        shp->set_string(nextGoalType, "ROAD_SEGMENT");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::PARKING_ZONE)
      {
        shp->set_string(nextGoalType, "PARKING_ZONE");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::INTERSECTION)
      {
        shp->set_string(nextGoalType, "INTERSECTION");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::PREZONE)
      {
        shp->set_string(nextGoalType, "PREZONE     ");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::UTURN)
      {
        shp->set_string(nextGoalType, "UTURN       ");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::PAUSE)
      {
        shp->set_string(nextGoalType, "PAUSE       ");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::END_OF_MISSION)
      {
        shp->set_string(nextGoalType, "END_OF_MISSION");
      }
      else
      {
        shp->set_string(nextGoalType, "UNKNOWN");
      }

      segGoalsSeq.pop_front();
      i++;
    }

    usleep(500000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sparrowInsertObstacle()
{  
  m_graphEstimator.insertObstacle(obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
}





//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sparrowRestartMission()
{

}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// GCmodule functions */
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::MPlanningLoop(void)
{
  while(true)
  {
    arbitrate(&m_controlStatus, &m_mergedDirective );
    control(&m_controlStatus, &m_mergedDirective );
    usleep(100000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  if (DEBUG_LEVEL > 3 && m_nosparrow)
  {
    cout << "I'm in the arbiter now" << endl;
  }
  if (m_logData)
  {
    fprintf ( m_logFile, "\nIn arbiter:\n" );
  }

  RoutePlannerMergedDirective * rcmd = dynamic_cast<RoutePlannerMergedDirective*>(md);
  MControlDirective* mergedDir = &(rcmd->directive);
  RoutePlannerControlStatus*  rpcontrs = dynamic_cast<RoutePlannerControlStatus*>(cs);
  MControlDirectiveResponse* contrStatus = &(rpcontrs->rpcs);

  if (contrStatus->status == MControlDirectiveResponse::EXECUTING)
  {
    if (m_logData)
    {
      fprintf( m_logFile, "  Control status: goalID = %d\t status = %d\n", contrStatus->goalID, contrStatus->status );
    }
    return;
  }

  if (DEBUG_LEVEL > 1)
  {
    if (m_nosparrow)
    {
      cout << endl << "Control status: goalID = " << contrStatus->goalID << "  status = " 
	   << contrStatus->status << endl;
    }
    else
    {
      SparrowHawk().log("Control status: goalID = %d   status = %d\n", contrStatus->goalID, contrStatus->status);
    }
  }
  if (m_logData)
  {
    fprintf ( m_logFile, "  Control status: goalID = %d\t status = %d\n", contrStatus->goalID, contrStatus->status );
  }

  if (contrStatus->goalID != 0)
  {
    m_missionControl.updateDirectiveStatus(*contrStatus);
  }
	   
  *mergedDir = m_missionControl.getNextDirective();
  if (contrStatus->goalID == 0)
  {
    m_prevMergedDirective = *mergedDir;
  }

  if (DEBUG_LEVEL > 1)
  {
    if (m_nosparrow)
    {
      cout << "Merged directive: goal ID = " << mergedDir->goalID; 
      if (mergedDir->name == MControlDirective::NEXT_CHECKPOINT)
      {
	cout << " NEXT_CHECKPOINT = " << mergedDir->segmentID << "." << mergedDir->laneID << "."
	     << mergedDir->waypointID << endl;
      }
      else if (mergedDir->name == MControlDirective::END_OF_MISSION)
      {
	cout << " END_OF_MISSION" << endl;
      }
      else
      {
	cout << " PAUSE" << endl;
      }
    }
    else
    {
      SparrowHawk().log("Merged directive: goal ID = %d", mergedDir->goalID);
      if (mergedDir->name == MControlDirective::NEXT_CHECKPOINT)
      {
	SparrowHawk().log(" NEXT_CHECKPOINT = %d.%d.%d\n", mergedDir->segmentID, mergedDir->laneID,
			  mergedDir->waypointID);
      }
      else if (mergedDir->name == MControlDirective::END_OF_MISSION)
      {
	SparrowHawk().log(" END_OF_MISSION\n");
      }
      else
      {
	SparrowHawk().log(" PAUSE\n");
      }
    }
  }

  if (m_logData)
  {
    fprintf(m_logFile, "  Merged directive: goal ID = %d", mergedDir->goalID);
    if (mergedDir->name == MControlDirective::NEXT_CHECKPOINT)
    {
      fprintf(m_logFile, "    NEXT_CHECKPOINT = %d.%d.%d\n", mergedDir->segmentID, mergedDir->laneID,
	      mergedDir->waypointID);
    }
    else if (mergedDir->name == MControlDirective::END_OF_MISSION)
    {
      fprintf(m_logFile, "    END_OF_MISSION\n");
    }
    else
    {
      fprintf(m_logFile, "    PAUSE\n");
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::control(ControlStatus* cs, MergedDirective* md)
{
  if (DEBUG_LEVEL > 3 && m_nosparrow)
  {
    cout << "I'm in the control now" << endl;
  }
  if (m_logData)
  {
    fprintf( m_logFile, "\nIn control:\n" );
  }

  RoutePlannerMergedDirective* rcmd = dynamic_cast<RoutePlannerMergedDirective*>(md);
  MControlDirective* mergedDir = &(rcmd->directive);
  RoutePlannerControlStatus* rpcontrStatus = dynamic_cast<RoutePlannerControlStatus*>(cs);
  MControlDirectiveResponse* contrStatus = &(rpcontrStatus->rpcs);

  #warning RoutePlanner cannot handle PAUSE command yet.
  bool replan = false;
  bool goalFailed = false;

  m_currentCkptSegmentID = m_prevMergedDirective.segmentID;
  m_currentCkptLaneID = m_prevMergedDirective.laneID;
  m_currentCkptWaypointID = m_prevMergedDirective.waypointID;  
  if (m_prevMergedDirective.goalID != mergedDir->goalID)
  {
    m_missionID = m_missionControl.getNextCheckpointIndex();
  }
  else
  {
    m_missionID = m_missionControl.getNextCheckpointIndex() + 1;
  }


  // First see if the previous merged directive is completed.
  if (rtInterfaceSF->isStatus(SegGoalsStatus::COMPLETED, m_prevMergedDirectiveGoalID))
  {
    if (m_logData)
    {
      fprintf (m_logFile, "  Merged directive %d (waypoint %d.%d.%d) is completed\n", 
	       contrStatus->goalID, m_prevMergedDirective.segmentID, 
	       m_prevMergedDirective.laneID, m_prevMergedDirective.waypointID );
    }

    contrStatus->goalID = m_prevMergedDirective.goalID;
    contrStatus->status = MControlDirectiveResponse::COMPLETED;

    m_prevMergedDirective = *mergedDir;
    m_prevMergedDirectiveGoalID = m_mergedDirectiveGoalID;

    m_currentCkptSegmentID = m_prevMergedDirective.segmentID;
    m_currentCkptLaneID = m_prevMergedDirective.laneID;
    m_currentCkptWaypointID = m_prevMergedDirective.waypointID;

    return;
  }

  // plan from current position if we're not in run
  // plan from previous plan if we're in run and at least one plan came through

  // if we're not in RUN, plan from veh state
  /*
  if(m_actuatorState.m_estoppos != RUN)
  {
    replan = true;
    if (m_nosparrow)
      cout << "I'm not in RUN" <<endl;
    else
      SparrowHawk().log("I'm not in RUN\n");
  }
  else
*/

  if (DEBUG_LEVEL > 6 && m_nosparrow)
  {
    cout << "Did I just get new status... : " ;
    cout.flush();
    cout << rtInterfaceSF->haveNewStatus() << endl;
  }
  if (m_logData)
  {
    fprintf( m_logFile, "  Did I just get new status...: %d\n", rtInterfaceSF->haveNewStatus() );
  }

  if (rtInterfaceSF->haveNewStatus())
  {
    m_segGoalsStatus = *(rtInterfaceSF->getLatestStatus());
    if (m_segGoalsStatus.goalID == 0)
    {
      replan = true;
      if (m_nosparrow)
      {
	cout << endl << "tplanner just starts listening...I'll replan" << endl;
      }
      else
      {
	SparrowHawk().log("tplanner just starts listening...I'll replan\n");
      }
      if (m_logData)
      {
	fprintf(m_logFile, "  tplanner just starts listening...I'll replan\n");
      }

      m_nextGoalID = 1;
    }
    else if(m_segGoalsStatus.status == SegGoalsStatus::FAILED)
    {
      replan = true;
      goalFailed = true;
      if (m_nosparrow)
      {
	cout << endl << "Goal " << m_segGoalsStatus.goalID << " failed : Replanning" << endl;
      }
      else
      {
	SparrowHawk().log("Goal %d failed : Replanning\n", m_segGoalsStatus.goalID);
      }  
      if (m_logData)
      {
	fprintf(m_logFile, "  Goal %d failed : Replanning\n", m_segGoalsStatus.goalID);
      }  

      m_nextGoalID = m_segGoalsStatus.goalID;
      if (m_nextGoalID == 0)
      {
	m_nextGoalID = 1;
      }
    }
    else if (m_segGoalsStatus.status == SegGoalsStatus::COMPLETED)
    {
      replan = false;

      /*
      DGClockMutex(&m_contrDirectiveQMutex);

      // Add directives to contrGcPort
      while( m_contrDirectiveQ.size() > 0 && m_segGoalsStatus.goalID + LEAST_NUM_SEGGOALS_TPLANNER_STORED >= 
	(m_contrDirectiveQ.front()).goalID )
      {
	sendSegGoals();

	if (DEBUG_LEVEL > 0)
        {
	  if (m_nosparrow)
	  {
	    cout << "Adding goal " << (m_contrDirectiveQ.front()).goalID << " to contrGcPortMsgQ." << endl;
	  }
	  else
	  {
	    SparrowHawk().log("Adding goal %d to GcInterface\n", (m_contrDirectiveQ.front()).goalID);
	  }
	}
	if (m_logData)
	{
	  fprintf(m_logFile, "  Adding goal %d to GcInterface\n", (m_contrDirectiveQ.front()).goalID);
	}
	rtInterfaceSF->sendDirective(&m_contrDirectiveQ.front());
	m_contrDirectiveQ.pop_front();
      }
      DGCunlockMutex(&m_contrDirectiveQMutex);
      */
    }
  }
  else if ( m_prevMergedDirective.goalID > m_lastPlannedMergedDirectiveID)
  {
    replan = true;
    m_nextGoalID = 1;
    if (m_logData)
    {
      fprintf(m_logFile, "  m_prevMergedDirective.goalID > m_lastPlannedMergedDirectiveID...I'll replan\n");
    }
  }

  if (!replan)
  {
    DGClockMutex(&m_contrDirectiveQMutex);
    while( m_contrDirectiveQ.size() > 0 && m_segGoalsStatus.goalID + LEAST_NUM_SEGGOALS_TPLANNER_STORED >= 
	   (m_contrDirectiveQ.front()).goalID )
    {
      sendSegGoals();
    }
    DGCunlockMutex(&m_contrDirectiveQMutex);
  }
  
  DGClockMutex(&m_contrDirectiveQMutex);
  int numSegGoalsStored = m_contrDirectiveQ.size();
  DGCunlockMutex(&m_contrDirectiveQMutex);
    
  // Compute new goals starting from current position or from last waypoint
  // (given by the traffic planner) if failure occurs.    
  if (replan && m_prevMergedDirective.name == MControlDirective::NEXT_CHECKPOINT)
  {
    
    if (DEBUG_LEVEL > 3 && m_nosparrow)
    {
      cout << "I'm about to replan" << endl;
    }
    // First, get an updated graph
    getNewGraph();

    if (goalFailed)
    {
      SegGoals failedGoal = rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID);
      if (failedGoal.segment_type == SegGoals::ROAD_SEGMENT || failedGoal.entrySegmentID == 0)
      {
	m_graphEstimator.removeFailedGoalEdges(failedGoal.entrySegmentID, failedGoal.entryLaneID, 
					       failedGoal.entryWaypointID, failedGoal.exitSegmentID, 
					       failedGoal.exitLaneID, failedGoal.exitWaypointID, true);
      }
      else if  (failedGoal.segment_type != SegGoals::UTURN)
      {
	m_graphEstimator.removeFailedGoalEdges(failedGoal.entrySegmentID, failedGoal.entryLaneID, 
					       failedGoal.entryWaypointID, failedGoal.exitSegmentID, 
					       failedGoal.exitLaneID, failedGoal.exitWaypointID, false);
      }
    }

    if (m_logData)
    {
      fprintf (m_logFile, "Graph: \n" );
      m_travGraph->log(m_logFile);
      fprintf (m_logFile, "\n");
    }
          
    resetGoals();
        
    int missionID = m_missionControl.getNextCheckpointIndex() + 1;

    if (m_prevMergedDirective.goalID != mergedDir->goalID)
    {
      missionID--;
    }
    
    // Plan from current position
    vector<SegGoals> segGoalsSeq = planSegGoals(0, 0, 0, m_prevMergedDirective.segmentID,
						m_prevMergedDirective.laneID, m_prevMergedDirective.waypointID,
						missionID);
    
    DGClockMutex(&m_contrDirectiveQMutex);
    int startPrintingIndex = (int)m_contrDirectiveQ.size();

    // Add directives to control queue
    for(unsigned i = 0; i < segGoalsSeq.size(); i++)
    {
      m_contrDirectiveQ.push_back(segGoalsSeq[i]);
    }  
    
    // Update goalID that corresponds to completion of previous merged directive.
    if (m_contrDirectiveQ.size() > 0)
    {
      m_prevMergedDirectiveGoalID = (m_contrDirectiveQ.back()).goalID;
      m_nextGoalID = (m_contrDirectiveQ.back()).goalID + 1;
      m_lastPlannedMergedDirectiveID = m_prevMergedDirective.goalID;
    }
    
    // Print out the mission
    if (m_nosparrow)
    {
      printMission(m_contrDirectiveQ, startPrintingIndex);
      cout<<endl;  
    }
    else
    {
      printMissionOnSparrow(m_contrDirectiveQ, startPrintingIndex);
    }
    if (m_logData)
    {
      logMission(m_contrDirectiveQ, startPrintingIndex);
    }

    // Add directives to contrGcPort
    int numDirectivesAdded = 0;
    while( m_contrDirectiveQ.size() > 0 && numDirectivesAdded < LEAST_NUM_SEGGOALS_TPLANNER_STORED )
    {
      sendSegGoals();
      numDirectivesAdded++;
      /*
      if (DEBUG_LEVEL > 0)
      {
	if (m_nosparrow)
	{
	  cout << "Adding goal " << (m_contrDirectiveQ.front()).goalID << " to GcInterface." << endl;
	}
	else
	{
	  SparrowHawk().log("Adding goal %d to GcInterface\n", (m_contrDirectiveQ.front()).goalID);
	}
      }
      if (m_logData)
      {
	fprintf(m_logFile, "  Adding goal %d to GcInterface\n", (m_contrDirectiveQ.front()).goalID);
      }

      rtInterfaceSF->sendDirective(&m_contrDirectiveQ.front());
      m_contrDirectiveQ.pop_front();
      numDirectivesAdded++;
      */
    }
    
    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirective.goalID;
    contrStatus->status = MControlDirectiveResponse::EXECUTING;

    DGCunlockMutex(&m_contrDirectiveQMutex);
    return;
  }

  // Notify arbiter that we're ready for the next directive from Mission Control
  else if (numSegGoalsStored < LEAST_NUM_SEGGOALS_STORED && 
	   m_prevMergedDirective.goalID == mergedDir->goalID)
  {
    if (DEBUG_LEVEL > 3 && m_nosparrow)
    {
      cout << "I'm ready for next goal" << endl;
    }
    if (m_logData)
    {
      fprintf (m_logFile, "  I'm ready for next goal\n");
    }
    // Update controlStatus
    contrStatus->goalID = mergedDir->goalID;
    contrStatus->status = MControlDirectiveResponse::READY_FOR_NEXT;
    return;
  }
  
  else if (numSegGoalsStored < LEAST_NUM_SEGGOALS_STORED  && 
	   mergedDir->name == MControlDirective::NEXT_CHECKPOINT && mergedDir->goalID > m_lastPlannedMergedDirectiveID )
  {
    if (DEBUG_LEVEL > 3 && m_nosparrow)
    {
      cout << "I'm planning ahead" << endl;
    }
    if (m_logData)
    {
      fprintf( m_logFile, "  I'm planning ahead\n" );
    }
    int missionID = m_missionControl.getNextCheckpointIndex() + 1;
    vector<SegGoals> segGoalsSeq = planSegGoals(m_prevMergedDirective.segmentID,
						m_prevMergedDirective.laneID,
						m_prevMergedDirective.waypointID,
						mergedDir->segmentID,
						mergedDir->laneID,
						mergedDir->waypointID,
						missionID);

    DGClockMutex(&m_contrDirectiveQMutex);
    int startPrintingIndex = (int)m_contrDirectiveQ.size(); 

    // Add directives to control queue     
    for(unsigned i = 0; i < segGoalsSeq.size(); i++)
    {
      m_contrDirectiveQ.push_back(segGoalsSeq[i]);
    }      
    
    // Update goalID that corresponds to completion of current merged directive.
    if (m_contrDirectiveQ.size() > 0)
    {
      m_mergedDirectiveGoalID = (m_contrDirectiveQ.back()).goalID;
      m_nextGoalID = (m_contrDirectiveQ.back()).goalID + 1;
      m_lastPlannedMergedDirectiveID = mergedDir->goalID;
    }
    
    // Print out the mission
    if (m_nosparrow)
    {
      printMission(m_contrDirectiveQ, startPrintingIndex);
      cout << endl;  
    }
    else
    {
      printMissionOnSparrow(m_contrDirectiveQ, startPrintingIndex);
    }
    if (m_logData)
    {
      logMission(m_contrDirectiveQ, startPrintingIndex);
    }

    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirective.goalID;
    contrStatus->status = MControlDirectiveResponse::EXECUTING;

    DGCunlockMutex(&m_contrDirectiveQMutex);
    return;
  }
  else if (numSegGoalsStored < LEAST_NUM_SEGGOALS_STORED 
	   && mergedDir->name == MControlDirective::END_OF_MISSION && mergedDir->goalID > m_lastPlannedMergedDirectiveID )
  {
    if (DEBUG_LEVEL > 3 && m_nosparrow)
    {
      cout << "I got END_OF_MISSION directive. Yay!!!" << endl;
    }
    if (m_logData)
    {
      fprintf( m_logFile, "  I got END_OF_MISSION directive. Yay!!!\n" );
    }
    SegGoals endOfMission;
    endOfMission.goalID = m_nextGoalID;
    endOfMission.entrySegmentID = 0;
    endOfMission.entryLaneID = 0;
    endOfMission.entryWaypointID = 0;
    endOfMission.exitSegmentID = 0;
    endOfMission.exitLaneID = 0;
    endOfMission.exitWaypointID = 0;
    endOfMission.minSpeedLimit = 0;
    endOfMission.maxSpeedLimit = 0;
    endOfMission.segment_type = SegGoals::END_OF_MISSION;
    endOfMission.illegalPassingAllowed = false;
    endOfMission.stopAtExit = true;
    endOfMission.isExitCheckpoint = false;

    DGClockMutex(&m_contrDirectiveQMutex);
    int startPrintingIndex = (int)m_contrDirectiveQ.size(); 

    // Add directives to control queue     
    m_contrDirectiveQ.push_back(endOfMission);
    m_mergedDirectiveGoalID = (m_contrDirectiveQ.back()).goalID;
    
    // Update goalID that corresponds to completion of current merged directive.
    if (m_contrDirectiveQ.size() > 0)
    {
      m_mergedDirectiveGoalID = (m_contrDirectiveQ.back()).goalID;
      m_nextGoalID = (m_contrDirectiveQ.back()).goalID + 1;
      m_lastPlannedMergedDirectiveID = mergedDir->goalID;
    }
    
    // Print out the mission
    if (m_nosparrow)
    {
      printMission(m_contrDirectiveQ, startPrintingIndex);
      cout << endl;  
    }
    else
    {
      printMissionOnSparrow(m_contrDirectiveQ, startPrintingIndex);
    }
    if (m_logData)
    {
      logMission(m_contrDirectiveQ, startPrintingIndex);
    }

    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirective.goalID;
    contrStatus->status = MControlDirectiveResponse::EXECUTING;

    DGCunlockMutex(&m_contrDirectiveQMutex);
    return;
  }
  else
  {
    if (DEBUG_LEVEL > 3 && m_nosparrow)
    {
      cout << "I'm being lazy and not doing anything" << endl;
    }
    if (m_logData)
    {
      fprintf( m_logFile, "  I'm being lazy and not doing anything\n" );
    }
    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirective.goalID;
    contrStatus->status = MControlDirectiveResponse::EXECUTING;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sendSegGoals()
{
  if (DEBUG_LEVEL > 0)
  {
    if (m_nosparrow)
    {
      cout << "Adding goal " << (m_contrDirectiveQ.front()).goalID << " to contrGcPortMsgQ." << endl;
    }
    else
    {
      SparrowHawk().log("Adding goal %d to GcInterface\n", (m_contrDirectiveQ.front()).goalID);
    }
  }
  if (m_logData)
  {
    fprintf(m_logFile, "  Adding goal %d to GcInterface\n", (m_contrDirectiveQ.front()).goalID);
  }
  rtInterfaceSF->sendDirective(&m_contrDirectiveQ.front());
  m_contrDirectiveQ.pop_front();
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::getNewGraph()
{
  m_graphEstimator.updateGraph(true);
  m_travGraph = &m_graphEstimator.getGraph();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> RoutePlanner::planSegGoals(int segmentID1, int laneID1,  
    int waypointID1, int segmentID2, int laneID2, int waypointID2, int missionNumber) 
{
  double cost; 
  vector<SegGoals> segGoalsSeq; 
  vector<Vertex*> route; 

  Vertex* vertex1 = m_travGraph->getVertex(segmentID1, laneID1, waypointID1); 
  if (vertex1 == NULL) 
  {
    m_graphEstimator.addVertex(segmentID1, laneID1, waypointID1); 
    vertex1 = m_travGraph->getVertex(segmentID1, laneID1, waypointID1);
    if (vertex1 == NULL)
    {
      if (m_nosparrow)
      {
	cerr << "ERROR: planSegGoals: Cannot find waypoint " << segmentID1
	     << "." << laneID1 << "." << waypointID1 << " in graph" << endl;
      }
      else
      {
	SparrowHawk().log("ERROR: planSegGoals: Cannot find waypoint");
	SparrowHawk().log("%d.%d.%d in graph!\n", segmentID1, laneID1, waypointID1);
      }
      if (m_logData)
      {
	fprintf(m_logFile, "\nERROR: planSegGoals: Cannot find waypoint");
	fprintf(m_logFile, "%d.%d.%d in graph!\n", segmentID1, laneID1, waypointID1);
      }
    }
  }

  Vertex* vertex2 = m_travGraph->getVertex(segmentID2, laneID2, waypointID2); 
  if (vertex2 == NULL)
  {
    m_graphEstimator.addVertex(segmentID2, laneID2, waypointID2); 
    vertex2 = m_travGraph->getVertex(segmentID2, laneID2, waypointID2);
    if (vertex2 == NULL)
    {
      if (m_nosparrow)
      {
	cerr << "ERROR: planSegGoals: Cannot find waypoint " << segmentID2
	     << "." << laneID2 << "." << waypointID2 << " in graph" << endl;
      }
      else
      {
	SparrowHawk().log("ERROR: planSegGoals: Cannot find waypoint");
	SparrowHawk().log("%d.%d.%d in graph!\n", segmentID2, laneID2, waypointID2);
      }
      if (m_logData)
      {
	fprintf( m_logFile, "\nERROR: planSegGoals: Cannot find waypoint");
	fprintf( m_logFile, "%d.%d.%d in graph!\n", segmentID2, laneID2, waypointID2);
      }
    }
  }

  bool routeFound = findRoute(vertex1, vertex2, m_travGraph, route, cost);
  if (!routeFound)
  {
    // Add uturn to segGoalsSeq
    segGoalsSeq = addUturn(vertex1, vertex2);


    if (segGoalsSeq.size() == 0)
    {
      if (m_graphEstimator.addRemovedEdge())
      {
        if (m_nosparrow)
        {
          cout << "Cannot find route after allowing u-turn" << endl;
	}
	else
	{
          SparrowHawk().log("Cannot find route after allowing u-turn");
	}
	if (m_logData)
	{
	  fprintf (m_logFile, "\nCannot find route from %d.%d.%d to %d.%d.%d after allowing u-turn\n", 
		   segmentID1, laneID1, waypointID1, segmentID2, laneID2, waypointID2);
	  m_travGraph->log(m_logFile);
	  fprintf(m_logFile, "\n");
	}
        segGoalsSeq = planSegGoals(segmentID1, laneID1, waypointID1, 
				   segmentID2, laneID2, waypointID2, 
				   missionNumber);
      }
      else
      {
        cerr << "Cannot find route after adding all the removed edges back to the graph" 
             << endl;
	cout << "Here is the graph" << endl;
	if (m_logData)
	{
	  fprintf (m_logFile, "\nCannot find route after adding all the removed edges back to the graph\n" );
	  m_travGraph->log(m_logFile);
	}
	m_travGraph->print();
	#warning routeplanner gives up when route cannot be found
        exit(1);
      }
    }
  }
  else 
  { 
    segGoalsSeq = findSegGoals(route, m_travGraph, m_minSpeedLimits, m_maxSpeedLimits, m_nextGoalID); 
  } 

  if (segGoalsSeq.size() > 0) 
  { 
    // The exit point of the last segment goal is a checkpoint 
    segGoalsSeq[segGoalsSeq.size()-1].isExitCheckpoint = true; 
  } 

  if (m_nosparrow && route.size() > 1)
  {
    cout << endl << "Mission " << missionNumber;
    cout << ": from waypoint "<<  vertex1->getSegmentID() << "."
         << vertex1->getLaneID() << "." << vertex1->getWaypointID();
    cout << " to waypoint " << vertex2->getSegmentID() << "."
         << vertex2->getLaneID() << "."
         << vertex2->getWaypointID() << endl;
  }
  else if (route.size() > 1)
  {
    SparrowHawk().log("Mission %d: from waypoint %d.%d.%d to waypoint %d.%d.%d",
        missionNumber, vertex1->getSegmentID(),
        vertex1->getLaneID(), vertex1->getWaypointID(),
        vertex2->getSegmentID(), vertex2->getLaneID(),
        vertex2->getWaypointID());
  }
  if (m_logData)
  {
    fprintf( m_logFile, "\n  Mission %d: from waypoint %d.%d.%d to waypoint %d.%d.%d\n",
        missionNumber, vertex1->getSegmentID(),
        vertex1->getLaneID(), vertex1->getWaypointID(),
        vertex2->getSegmentID(), vertex2->getLaneID(),
        vertex2->getWaypointID());
  }


  return segGoalsSeq;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> RoutePlanner::addUturn(Vertex* vertex1, Vertex* vertex2)
{
  if (m_nosparrow)
  {
    cout << "no route from ";
    vertex1->print();
    cout << " to ";
    vertex2->print();
    cout << " without making u-turn" << endl;
  }
  else
  {
    SparrowHawk().log("no route from %d.%d.%d to %d.%d.%d without making u-turn\n",
        vertex1->getSegmentID(), vertex1->getLaneID(), vertex1->getWaypointID(),
        vertex2->getSegmentID(), vertex2->getLaneID(), vertex2->getWaypointID());
  }
  if (m_logData)
  {
    fprintf( m_logFile, "\nNo route from %d.%d.%d to %d.%d.%d without making u-turn\n",
        vertex1->getSegmentID(), vertex1->getLaneID(), vertex1->getWaypointID(),
        vertex2->getSegmentID(), vertex2->getLaneID(), vertex2->getWaypointID());
  }

  if (vertex1->getSegmentID() > m_travGraph->getNumOfSegments())
  {
    cerr << "Cannot make uturn in a zone" << endl;
    cout << "Here is the graph" << endl;
    m_travGraph->print();
    if (m_logData)
    {
      fprintf (m_logFile, "\nERROR: Cannot make uturn in a zone\n" );
      fprintf (m_logFile, "Here is the graph\n" );
      m_travGraph->log(m_logFile);
    }
    exit(1);
  }

  vector<Vertex*> route;
  vector<SegGoals> segGoalsSeq;
  double cost;
  bool routeFound = false;

  m_graphEstimator.addUturnEdges(vertex1);
  if (m_logData)
  {
    fprintf( m_logFile, "After adding uturn edges: \n");
    m_travGraph->log(m_logFile);
    fprintf( m_logFile, "\n");
  }
  
  routeFound = findRoute(vertex1, vertex2, m_travGraph, route, cost);
  if (!routeFound)
  {
    return segGoalsSeq;
  }
  
  segGoalsSeq = findSegGoals(route, m_travGraph, m_minSpeedLimits, m_maxSpeedLimits, m_nextGoalID);

  return segGoalsSeq;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::resetGoals()
{
  m_contrDirectiveQ.clear();

  if (DEBUG_LEVEL > 0)
  {
    if (m_nosparrow)
    {
      cout << "Clear contrGcPort" << endl;
    }
    else
    {
      SparrowHawk().log("Clear contrGcPort\n");
    }
  }
  if (m_logData)
  {
    fprintf(m_logFile, "  Clear contrGcPort\n");
  }

  rtInterfaceSF->flushAll();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool RoutePlanner::loadMDFFile(char* fileName, RNDF* rndf)
{
  ifstream file;
  string line;
  string word;
  
  file.open(fileName, ios::in);

  if(!file)
  {
    cerr << "Error: " << fileName << " file not found." << endl;
    return false;
  } 
  
  getline(file, line);
  istringstream lineStream(line, ios::in);
  lineStream >> word;

  if(word == "MDF_name")
  {
    if (DEBUG_LEVEL > 1)
    {
      cout << "Parsing speed limits" << endl;
    }
    parseSpeedLimit(&file, rndf);
    file.close();
    if (DEBUG_LEVEL > 1)
    {
      cout << "Finish loading MDF file" << endl;
    }
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::parseSpeedLimit(ifstream* file, RNDF* rndf)
{
  int segmentID;
  double minSpeed, maxSpeed;
  string line, word;
  char letter;

  while(word != "speed_limits")
  {    
    getline(*file, line);
    istringstream lineStream(line, ios::in);
    lineStream >> word;
  }

  
  while(word != "end_speed_limits")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    if(letter >= '0' && letter <= '9')
    {
      lineStream >> segmentID;
      lineStream >> minSpeed;
      lineStream >> maxSpeed;

      minSpeed = minSpeed * MPS_PER_MPH;
      maxSpeed = maxSpeed * MPS_PER_MPH;
      
      // setSpeedLimits(segmentID, minSpeed, maxSpeed, rndf);
      if ((unsigned)segmentID <= m_minSpeedLimits.size())
      {
	m_minSpeedLimits[segmentID] = minSpeed;
	m_maxSpeedLimits[segmentID] = maxSpeed;
      }
      else
      {
	cerr << "ERROR: parseSpeedLimit: got speed limit for segment " << segmentID
	     << " while number of segments and zones = " << m_minSpeedLimits.size() << endl;
	if (m_logData)
	{
	  fprintf (m_logFile, "\nERROR: parseSpeedLimit: got speed limit for segment %d", segmentID);
	  fprintf (m_logFile, " while number of segments and zones = %d\n", m_minSpeedLimits.size());
	}
	int numOfSegments = m_minSpeedLimits.size();
	m_minSpeedLimits.resize(segmentID);
	m_maxSpeedLimits.resize(segmentID);
	for (unsigned i = numOfSegments; i <= m_minSpeedLimits.size(); i++)
	{
	  m_minSpeedLimits[i] = 0;
	  m_maxSpeedLimits[i] = 0;
	}
	m_minSpeedLimits[segmentID] = minSpeed;
	m_maxSpeedLimits[segmentID] = maxSpeed;
      }
    }
    else
    {
      lineStream >> word;
      continue;      
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::printMissionOnSparrow(deque<SegGoals> segGoals, int startIndex)
{
  int i = 0;
  while (segGoals.size() > 0 && i < startIndex)
  {
    segGoals.pop_front();
    i++;
  }

  int goalID;
  int entrySegmentID, entryLaneID, entryWaypointID;
  int exitSegmentID, exitLaneID, exitWaypointID;
  double minSpeedLimit, maxSpeedLimit;
  bool illegalPassingAllowed, stopAtExit, isExitCheckpoint;
  SegGoals::SegmentType segment_type;
  
  while (segGoals.size() > 0)
  {
    SegGoals currentGoal = segGoals.front();
    goalID = currentGoal.goalID;
    entrySegmentID = currentGoal.entrySegmentID;
    entryLaneID = currentGoal.entryLaneID;
    entryWaypointID = currentGoal.entryWaypointID;
    exitSegmentID = currentGoal.exitSegmentID;
    exitLaneID = currentGoal.exitLaneID;
    exitWaypointID = currentGoal.exitWaypointID;
    minSpeedLimit = currentGoal.minSpeedLimit;
    maxSpeedLimit = currentGoal.maxSpeedLimit;
    illegalPassingAllowed = currentGoal.illegalPassingAllowed;
    stopAtExit = currentGoal.stopAtExit;
    isExitCheckpoint = currentGoal.isExitCheckpoint;
    segment_type = currentGoal.segment_type;
    SparrowHawk().log("GOAL %d: %d.%d.%d -> %d.%d.%d",  goalID, entrySegmentID, entryLaneID,
        entryWaypointID, exitSegmentID, exitLaneID, exitWaypointID);
    switch(segment_type)
    {
      case SegGoals::ROAD_SEGMENT:
        SparrowHawk().log("  ROAD_SEGMENT");
        break;
      case SegGoals::PARKING_ZONE:
        SparrowHawk().log("  PARKING_ZONE");
        break;
      case SegGoals::INTERSECTION:
        SparrowHawk().log("  INTERSECTION");
        break;
      case SegGoals::PREZONE:
        SparrowHawk().log("  PREZONE");
        break;
      case SegGoals::UTURN:
        SparrowHawk().log("  UTURN");
        break;
      case SegGoals::PAUSE:
        SparrowHawk().log("  PAUSE");
        break;
      case SegGoals::END_OF_MISSION:
        SparrowHawk().log("  END_OF_MISSION");
        break;
      default:
        SparrowHawk().log("  UNKNOWN");
    }
    SparrowHawk().log("  Min Speed: %4.1f, Max Speed: %4.1f", minSpeedLimit, maxSpeedLimit);
    if (illegalPassingAllowed)
    {
      SparrowHawk().log("  Illegal passing allowed");
    }
    segGoals.pop_front();
  }	
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::logMission(deque<SegGoals> segGoals, int startIndex)
{
  int i = 0;
  while (segGoals.size() > 0 && i < startIndex)
  {
    segGoals.pop_front();
    i++;
  }

  int goalID;
  int entrySegmentID, entryLaneID, entryWaypointID;
  int exitSegmentID, exitLaneID, exitWaypointID;
  double minSpeedLimit, maxSpeedLimit;
  bool illegalPassingAllowed, stopAtExit, isExitCheckpoint;
  SegGoals::SegmentType segment_type;
  
  while (segGoals.size() > 0)
  {
    SegGoals currentGoal = segGoals.front();
    goalID = currentGoal.goalID;
    entrySegmentID = currentGoal.entrySegmentID;
    entryLaneID = currentGoal.entryLaneID;
    entryWaypointID = currentGoal.entryWaypointID;
    exitSegmentID = currentGoal.exitSegmentID;
    exitLaneID = currentGoal.exitLaneID;
    exitWaypointID = currentGoal.exitWaypointID;
    minSpeedLimit = currentGoal.minSpeedLimit;
    maxSpeedLimit = currentGoal.maxSpeedLimit;
    illegalPassingAllowed = currentGoal.illegalPassingAllowed;
    stopAtExit = currentGoal.stopAtExit;
    isExitCheckpoint = currentGoal.isExitCheckpoint;
    segment_type = currentGoal.segment_type;
    fprintf( m_logFile, "    GOAL %d: %d.%d.%d -> %d.%d.%d",  goalID, entrySegmentID, entryLaneID,
        entryWaypointID, exitSegmentID, exitLaneID, exitWaypointID);
    switch(segment_type)
    {
      case SegGoals::ROAD_SEGMENT:
        fprintf( m_logFile, "  ROAD_SEGMENT");
        break;
      case SegGoals::PARKING_ZONE:
        fprintf( m_logFile, "  PARKING_ZONE");
        break;
      case SegGoals::INTERSECTION:
        fprintf( m_logFile, "  INTERSECTION");
        break;
      case SegGoals::PREZONE:
        fprintf( m_logFile, "  PREZONE");
        break;
      case SegGoals::UTURN:
        fprintf( m_logFile, "  UTURN");
        break;
      case SegGoals::PAUSE:
        fprintf( m_logFile, "  PAUSE");
        break;
      case SegGoals::END_OF_MISSION:
        fprintf( m_logFile, "  END_OF_MISSION");
        break;
      default:
        fprintf( m_logFile, "  UNKNOWN");
    }
    fprintf( m_logFile, "  Min Speed: %4.1f, Max Speed: %4.1f", minSpeedLimit, maxSpeedLimit);
    if (illegalPassingAllowed)
    {
      fprintf( m_logFile, "  Illegal passing   ");
    }
    if (stopAtExit)
    {
      fprintf( m_logFile, "  Stop  " );
    }
    if (isExitCheckpoint)
    {
      fprintf( m_logFile, "  Checkpoint  " );
    }
    fprintf (m_logFile, "\n");
    segGoals.pop_front();
  }	
  fprintf (m_logFile, "\n");
}


/*
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// GCinterface functions 
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::addDirectiveToGcPort(SegGoals directive)
{
  RPlannerContrMsgWrapper msgWrapper;
  msgWrapper.directive = directive;
  msgWrapper.response.status = RPlannerContrMsgResponse::QUEUED;
  DGClockMutex(&m_contrGcPortMutex);
  m_contrGcPortMsgQ.push_back(msgWrapper);
  DGCunlockMutex(&m_contrGcPortMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool RoutePlanner::isCompleted(int goalID)
{
  if (goalID <= 0)
  {
    return false;
  }
  DGClockMutex(&m_contrGcPortMutex);
  for (unsigned i=0; i<m_contrGcPortMsgQ.size(); i++)
  {
    if(m_contrGcPortMsgQ[i].directive.goalID == goalID)
    {
      if (m_contrGcPortMsgQ[i].response.status == RPlannerContrMsgResponse::COMPLETED)
      {
	DGCunlockMutex(&m_contrGcPortMutex);
	return true;
      }
      else
      {
	DGCunlockMutex(&m_contrGcPortMutex);
	return false;
      }
    }
  }

  DGCunlockMutex(&m_contrGcPortMutex);
  return false;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
RPlannerContrMsgResponse RoutePlanner::getResponse(int goalID)
{
  RPlannerContrMsgResponse response;
  response.status = RPlannerContrMsgResponse::QUEUED;

  DGClockMutex(&m_contrGcPortMutex);
  for (unsigned i=0; i<m_contrGcPortMsgQ.size(); i++)
  {
    if(m_contrGcPortMsgQ[i].directive.goalID == goalID)
    {
      response = m_contrGcPortMsgQ[i].response;
      DGCunlockMutex(&m_contrGcPortMutex);
      return response;
    }
  }
  DGCunlockMutex(&m_contrGcPortMutex);

  
  cerr << "ERROR: getResponse: Cannot find goalID " << goalID << " in contrGcPort" << endl;
  return response;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool RoutePlanner::gotNewStatus()
{
  DGClockMutex(&m_newStatusMutex);
  bool newStatusReceived = m_newStatus;
  DGCunlockMutex(&m_newStatusMutex);
  return newStatusReceived;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
SegGoalsStatus RoutePlanner::getLatestStatusChange()
{
  SegGoalsStatus status;
  status.goalID = 0;

  DGClockMutex(&m_SegGoalsStatusMutex);
  status = m_segGoalsStatus;
  DGCunlockMutex(&m_SegGoalsStatusMutex);

  DGClockMutex(&m_newStatusMutex);
  m_newStatus = false;
  DGCunlockMutex(&m_newStatusMutex);
  return status;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::messageThread()
{
  // The skynet socket for receiving tplanner status

  SkynetTalker<SegGoalsStatus> statusTalker(m_snKey, SNsegGoals, MODmissionplanner);
  SkynetTalker<SegGoals> segGoalsTalker(m_snKey,  SNtplannerStatus, MODmissionplanner);
  
  int numSegGoalsRequests = 0;
  int numRespondedRequests = 0;

  if (m_nosparrow)
  {
    cout << "messageThread: Waiting for traffic planner" << endl;
  }
  else
  {
    SparrowHawk().log("messageThread: Waiting for traffic planner\n");
  }

  while(true)
  {

    // Receive status
    if (statusTalker.hasNewMessage())
    {
      SegGoalsStatus receivedSegGoalsStatus;
    
      bool tPlannerStatusReceived =
          statusTalker.receive(&receivedSegGoalsStatus);
        
      if (tPlannerStatusReceived)
      {
	if (m_nosparrow)
	{
	  cout << "Goal " << receivedSegGoalsStatus.goalID << " status: "
	       << receivedSegGoalsStatus.status << endl;
	}
	else
	{
	  SparrowHawk().log("Goal %d status %d\n", receivedSegGoalsStatus.goalID,
			    (int) receivedSegGoalsStatus.status);
	}
	
	if (receivedSegGoalsStatus.goalID == 0)
	{
	  numSegGoalsRequests++;
	}
	else
	{
	  unsigned i = 0;
	  bool directiveFound = false;
	  DGClockMutex(&m_contrGcPortMutex);
	  while( i < m_contrGcPortMsgQ.size() && !directiveFound)
	  {
	    if(m_contrGcPortMsgQ[i].directive.goalID == receivedSegGoalsStatus.goalID)
	    {
	      directiveFound = true;
	      if (receivedSegGoalsStatus.status == SegGoalsStatus::ACCEPT)
	      {
		m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::ACCEPTED;
	      }
	      else if (receivedSegGoalsStatus.status == SegGoalsStatus::REJECT ||
		       receivedSegGoalsStatus.status == SegGoalsStatus::FAILED)
	      {
		m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::FAILED;
		m_contrGcPortMsgQ[i].response.reason = receivedSegGoalsStatus.reason;
	      }
	      else if (receivedSegGoalsStatus.status == SegGoalsStatus::COMPLETED)
	      {
		m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::COMPLETED;
	      }
	      else
	      {
		cerr << "ERROR: getTPlannerStatusThread: Received unknown status "
		     << receivedSegGoalsStatus.status << endl;
	      }
	      m_firstQueuedMsgIndex = i+1;
	    }
	    i++;
	  }
	  DGCunlockMutex(&m_contrGcPortMutex);
	  
	  if (!directiveFound)
	  {
	    cerr << "ERROR: getTPlannerStatusThread: Cannot find goalID " 
		 << receivedSegGoalsStatus.goalID << " in contrGcPort" << endl;
	  }
	}
      
	DGClockMutex(&m_SegGoalsStatusMutex);
	m_segGoalsStatus = receivedSegGoalsStatus;
	DGCunlockMutex(&m_SegGoalsStatusMutex);

	DGClockMutex(&m_newStatusMutex);
	m_newStatus = true;
	DGCunlockMutex(&m_newStatusMutex);
      }
    }

    // Start sending goals when tplanner starts listening
    if (numSegGoalsRequests > 0)
    {
      DGClockMutex(&m_SegGoalsStatusMutex);
      int lastGoalID = m_segGoalsStatus.goalID;
      DGCunlockMutex(&m_SegGoalsStatusMutex);
      
      DGClockMutex(&m_contrGcPortMutex);
      // If the tplanner just restarts, reset the status for goals previously sent to QUEUED.
      if (lastGoalID == 0 && numRespondedRequests < numSegGoalsRequests)
      {
	for (unsigned i=0; i < m_contrGcPortMsgQ.size(); i++)
	{
	  if (m_contrGcPortMsgQ[i].response.status == RPlannerContrMsgResponse::SENT)
	  {
	    m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::QUEUED;
	  }
	}
	numRespondedRequests++;
      }
      DGCunlockMutex(&m_contrGcPortMutex);

      // Find the first messages with status QUEUED.
      unsigned firstQueuedMsgIndex = 0;
      bool firstQueuedMsgFound = false;
      bool m_firstQueuedMsgFound = false;
      DGClockMutex(&m_contrGcPortMutex);
      while (firstQueuedMsgIndex < m_contrGcPortMsgQ.size() && !firstQueuedMsgFound)
      {
	if (m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::QUEUED ||
	    m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::SENT ||
	    m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::ACCEPTED)
	{
	  if (!m_firstQueuedMsgFound)
	  {
	    m_firstQueuedMsgFound = true;
	    m_firstQueuedMsgIndex = firstQueuedMsgIndex;
	  }
	  if (m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::QUEUED)
	  {
	    firstQueuedMsgFound = true;
	  }
	  else
	  {
	    firstQueuedMsgIndex++;
	  }
	}
	else
	{
	  firstQueuedMsgIndex++;
	}
      }

      DGCunlockMutex(&m_contrGcPortMutex);

      // Send all the messages with status QUEUED
      DGClockMutex(&m_contrGcPortMutex);
      if (firstQueuedMsgFound)
      {
	for (unsigned i = firstQueuedMsgIndex; i < m_contrGcPortMsgQ.size(); i++)
	{
	  if (m_nosparrow)
	  {
	    cout << "Sending goal " << (m_contrGcPortMsgQ[i]).directive.goalID << " to tplanner." << endl;
	  }
	  else
	  {
	    SparrowHawk().log("Sending goal %d to tplanner\n", (m_contrGcPortMsgQ[i]).directive.goalID);
	  }
	  segGoalsTalker.send(&(m_contrGcPortMsgQ[i].directive));
	  m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::SENT;
	}
      }
      DGCunlockMutex(&m_contrGcPortMutex);	
    }
    usleep(100000);
  }
}
*/
