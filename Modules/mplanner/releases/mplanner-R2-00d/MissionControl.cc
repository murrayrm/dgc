/*!**
 * Nok Wongpiromsarn
 * February 21, 2007
 */


#include "MissionControl.hh"
using namespace std;

MissionControl::MissionControl(RNDF* rndf, char* MDFFileName, bool nosparrow, 
			       int debugLevel, bool verbose)
{
  m_rndf = rndf;
  m_nosparrow = nosparrow;
  DEBUG_LEVEL = debugLevel; 
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }
  arbitrate(MDFFileName, m_rndf);
  control();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MissionControl::~MissionControl() 
{
  delete m_rndf;
  for(unsigned i=0; i<m_checkpointSequence.size(); i++)
    delete m_checkpointSequence[i];
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MContrDirective MissionControl::getNextDirective()
{
  if (m_contrDirectiveQueue.size() == 0)
    {
      control();
    }
  m_contrDirectiveQueue.back().status = Strategy::ACCEPTED;
  return strategy2ContrDirective(m_contrDirectiveQueue.back());
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
int MissionControl::getNextCheckpointIndex()
{
  return m_nextCheckpointIndex;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl:: updateDirectiveStatus(MContrDirectiveStatus directiveStatus)
{
  control(directiveStatus);
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::arbitrate(char* MDFFileName, RNDF* rndf)
{
  if (!loadMDF(MDFFileName, m_rndf))
  {
    cerr << "Error: Unable to load MDF file " << MDFFileName << ", exiting program"
         << endl;
    #warning "mplanner will quit if MDF is not valid. Should instead put vehicle in pause."
    exit(1);
  }
  m_nextCheckpointIndex = 0;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::control()
{
  Strategy nextStrategy;
  if(unsigned(m_nextCheckpointIndex) < m_checkpointSequence.size())
    {
      if (m_contrDirectiveQueue.size() > 0)
	{
	  nextStrategy.goalID = m_contrDirectiveQueue.back().goalID + 1;
	}
      else
	{
	  nextStrategy.goalID = 1;
	}
      nextStrategy.name = NEXT_CHECKPOINT;
      nextStrategy.nextCheckpointIndex = m_nextCheckpointIndex;
      nextStrategy.vehicleCap = MAX_CAP;
      nextStrategy.status = Strategy::SENT;
      m_contrDirectiveQueue.push_back(nextStrategy);
    }
  else // We complete the mission
    {
      if (m_contrDirectiveQueue.size() > 0)
	{
	  nextStrategy.goalID = m_contrDirectiveQueue.back().goalID + 1;
	}
      else
	{
	  nextStrategy.goalID = 1;
	}
      nextStrategy.name = END_OF_MISSION;
      nextStrategy.status = Strategy::SENT;
      m_contrDirectiveQueue.push_back(nextStrategy);
      if (m_nosparrow)
	{
	  cout << endl << "MISSION COMPLETED !!!" << endl << endl;
	}
      else
	{
	  SparrowHawk().log("MISSION COMPLETED !!!\n");
	}
    }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::control(MContrDirectiveStatus directiveStatus)
{
  if (m_contrDirectiveQueue.size() == 0)
    {
      return;
    }

  unsigned directiveIndex = 0;
  if (directiveStatus.goalID < m_contrDirectiveQueue.front().goalID ||
      directiveStatus.goalID > m_contrDirectiveQueue.back().goalID )
    {
      cerr << "ERROR: MissionControl::control : Status goalID = " 
	   << directiveStatus.goalID 
	   << " Stored control directive goalID are between "
	   << m_contrDirectiveQueue.front().goalID << " and "
	   << m_contrDirectiveQueue.back().goalID << endl;
    }
  else
    {
      while(directiveIndex < m_contrDirectiveQueue.size() && 
	    directiveStatus.goalID < m_contrDirectiveQueue[directiveIndex].goalID)
	{
	  directiveIndex++;
	}
    }

  if (directiveStatus.status == MContrDirectiveStatus::COMPLETED)
    {
      if (m_contrDirectiveQueue[directiveIndex].name == NEXT_CHECKPOINT )
	{
	  m_nextCheckpointIndex = m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1;
	  control();
	  while(m_contrDirectiveQueue.size() > 1)
	    {
	      m_contrDirectiveQueue.pop_front();
	    }
	}
      else if (m_contrDirectiveQueue[directiveIndex].name == PAUSE ) 
	{
	  int i = directiveIndex;
	  VehicleCap failedVehicleCap = MAX_CAP;
	  while (i >= 0)
	    {
	      if (m_contrDirectiveQueue[directiveIndex].name == NEXT_CHECKPOINT)
		{
		  failedVehicleCap = m_contrDirectiveQueue[i].vehicleCap;
		  m_nextCheckpointIndex = m_contrDirectiveQueue[i].nextCheckpointIndex;
		  break;
		}
	      i--;
	    }
	  control();
	  if (failedVehicleCap == MAX_CAP)
	    {
	      m_contrDirectiveQueue.back().vehicleCap = MEDIUM_CAP;
	    }
	  else
	    {
	      m_contrDirectiveQueue.back().vehicleCap = MIN_CAP;
	    }
	}
      else
	{
	  control();
	  while(m_contrDirectiveQueue.size() > 1)
	    {
	      m_contrDirectiveQueue.pop_front();
	    }
	}
    }
  else if (directiveStatus.status == MContrDirectiveStatus::FAILED ||
	   directiveStatus.status == MContrDirectiveStatus::REJECTED )
    {
      if (m_contrDirectiveQueue[directiveIndex].name == NEXT_CHECKPOINT )
	{
	  Strategy nextStrategy;
	  if (m_contrDirectiveQueue.size() > 0)
	    {
	      nextStrategy.goalID = m_contrDirectiveQueue.back().goalID + 1;
	    }
	  else
	    {
	      nextStrategy.goalID = 1;
	    }
	  nextStrategy.name = PAUSE;
	  nextStrategy.vehicleCap = MIN_CAP;
	  nextStrategy.status = Strategy::SENT;
	  m_contrDirectiveQueue.push_back(nextStrategy);
	}
      else
	{
	  //TODO: Command estop
	}
    }
  else // READY_FOR_NEXT
    {
      if (m_contrDirectiveQueue[directiveIndex].name == NEXT_CHECKPOINT )
	{
	  m_nextCheckpointIndex = m_contrDirectiveQueue[directiveIndex].nextCheckpointIndex + 1;
	  control();
	}
    }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool MissionControl::loadMDF(char* fileName, RNDF* rndf)
{
  ifstream file;
  string line;
  string word;

  file.open(fileName, ios::in);

  if(!file)
  {
    cerr << "Error: " << fileName << " file not found." << endl;
    return false;
  } 
  
  getline(file, line);
    
  istringstream lineStream(line, ios::in);
  
  lineStream >> word;

  if(word == "MDF_name")
  {
    parseCheckpoint(&file, rndf);
    file.close();
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void MissionControl::parseCheckpoint(ifstream* file, RNDF* rndf)
{
  string line, word;
  char letter;
  int ckptID;  
  
  while(word != "end_checkpoints")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      ckptID = atoi(word.c_str());
      m_checkpointSequence.push_back(rndf->getCheckpoint(ckptID));
    }
    else
    {
      continue;
    }
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
MContrDirective MissionControl::strategy2ContrDirective(Strategy strategy)
{
  MContrDirective directive;
  directive.goalID = strategy.goalID;
  directive.name = strategy.name;
  if (strategy.nextCheckpointIndex < 0 || 
      unsigned(strategy.nextCheckpointIndex) >= m_checkpointSequence.size() )
    {
      cerr << "ERROR: MissionControl::strategy2ContrDirective : invalid nextCheckpointIndex = "
	   << strategy.nextCheckpointIndex << endl;
      strategy.nextCheckpointIndex = m_checkpointSequence.size() - 1;
    }
  directive.segmentID = m_checkpointSequence[strategy.nextCheckpointIndex]->getSegmentID();
  directive.laneID = m_checkpointSequence[strategy.nextCheckpointIndex]->getLaneID();
  directive.waypointID = m_checkpointSequence[strategy.nextCheckpointIndex]->getWaypointID();
  return directive;
}
