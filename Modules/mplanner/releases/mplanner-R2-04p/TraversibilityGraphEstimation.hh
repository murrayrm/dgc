/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */


#ifndef TRAVGRAPHEST_HH
#define TRAVGRAPHEST_HH

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <deque>
#include "rndf/RNDF.hh"
#include "travgraph/Graph.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"
#include "Obstacle.hh"

struct EdgeID
{
  EdgeID(int psID, int plID, int pwID, int nsID, int nlID, int nwID, double l, double w, Edge::EdgeType t,
	 double speed, bool visited)
  {
    prevSegmentID = psID;
    prevLaneID = plID;
    prevWaypointID = pwID;
    nextSegmentID = nsID;
    nextLaneID = nlID;
    nextWaypointID = nwID;
    length = l;
    weight = w;
    type = t;
    avgSpeed = speed;
    isVisited = visited;
  }
  int prevSegmentID;
  int prevLaneID;
  int prevWaypointID;
  int nextSegmentID;
  int nextLaneID;
  int nextWaypointID;
  double length;
  double weight;
  Edge::EdgeType type;
  double avgSpeed;
  bool isVisited;
};

/**
 * TraversibilityGraphEstimation class
 * This is a class that communicates with the mapper. It requests
 * the updated traversibility graph and updates this graph according
 * to the current vehicle capability. The route planner part
 * of the mission planner can access this graph through a function call.
 * \brief class that communicates with the mapper.
 */
class TraversibilityGraphEstimation : public CStateClient
{ 
private:
  int DEBUG_LEVEL, VERBOSITY_LEVEL;

  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param whether we want to listen to mapper */
  bool m_listenToMapper;

  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;

  bool m_logData;
  FILE* m_logFile;

  /*!\param m_rndf is a pointer to the RNDF object. */
  RNDF* m_rndf;

  /*!\param m_graph is the corresponding graph of the current RNDF
   * that route planner uses to compute segment goals. */
  Graph* m_graph;

  /*!\param m_receivedGraph is the local copy of Graph that gets
   * updated when any is received from the networkthat  */
  Graph* m_receivedGraph;

  /*!\param m_currentSegmentID and m_currentLaneID is the lane we're currently in. 
   * If currentSegmentID is 0, that means mplanner doesn't know where we're and 
   * will compute the initial position by itself. */
  int m_currentSegmentID;
  int m_currentLaneID;
  int m_currentWaypointID;

  /*!\param m_obstacles is the list of all the obstacles. */
  vector<Obstacle> m_obstacles;

  /*!\param m_obstacles is the list of all the obstacles on a segment. */
  vector<Obstacle> m_obstaclesOnSegment;

  /*!\param m_removedEdges is the list of all the edges that got removed
   * because manually inserted obstacles block the road */
  deque<EdgeID> m_removedEdges;

  /*!\param m_edgeCost is the vector of cost of each edge type. See Graph.hh for the order. */
  vector<double> m_edgeCost;

  /*!\param m_avgSpeeds is the vector of averaged speed for each segment */
  vector<double> m_avgSpeeds;

  /*!\param The mutex to protect the list of obstacles */
  pthread_mutex_t m_ObstaclesMutex;

  /*!\param m_conNewGraph tells whether a new graph is received. */
  pthread_mutex_t m_condNewGraphMutex;
  DGCcondition m_condNewGraph;

  /*!\param The mutex for m_receivedGraph. */
  pthread_mutex_t m_receivedGraphMutex;

  /*!\param The mutex for m_removedEdges */
  pthread_mutex_t m_removedEdgesMutex;

  /*! The thread which continually updates the local copy of Graph when any is received from
    the network */
  void getGraphThread();

  /*! The function that adds a vertex that corresponds to our current position to graph */
  bool addCurrentPositionToGraph();

  /*! Get the index of obstacle in the vector */
  int getObstacleIndex(int obstacleID);

  /*! Update the list of obstacles based on decay time and the time they're created */
  void updateObstacles();

  /*! Update the cost of all the egdegs based on all the obstacles that I know of */
  void updateObstructedEdges();

  /*! Remove edges specified in m_removedEdges from graph */
  void removeEdges();

  /*! Update all the edges from current position based on edges that have been removed */
  void updateCurrentPositionEdges();

  /*! Add edges from the specified vertex corresponding to uturn */
  void addExtraUturnEdges(Vertex*);

  /*! A function to log data and print out a message on the screen
   *  if DEBUG_LEVEL is greater than d*/
  void print(string s, int d);
  void printError(string funcName, string errorMessage, bool error = true);

  /*! Get time in microsecond */
  uint64_t getTime();

public:
  /*! Constructor */
  TraversibilityGraphEstimation(int skynetKey, bool waitForStateFill);

  /*! Standard destructor */
  ~TraversibilityGraphEstimation();


  /*! A function to initialize this object */
  void init(RNDF* rndf, bool listenToMapper, bool nosparrow, bool logData, FILE* logFile, int debugLevel, bool verbose);

  /*! A function that updates m_edgeCost */
  void updateEdgeCostParam(vector<double> edgeCost);

  /*! A function that updates m_avgSpeed */
  void updateSpeed(vector<double> avgSpeeds);

  /*! Update current lane */
  void setCurrentLane(int segmentID, int laneID);

  /*! Update current waypoint */
  void setCurrentWaypoint(int segmentID, int laneID, int waypointID);

  /*! Check for new Graph */
  bool newGraph();
  
  /*! Give me the latest Graph (a copy) and reset new flag */
  void updateGraph(bool requestMapper);

  /*! use conditional mutexes to wait until Graph is received */
  void waitForGraph();

  /*! Get the graph */
  Graph& getGraph();
  /*! Get the graph */
  const Graph& getGraph() const;

  /*! Get the vertex closest to current position */
  Vertex* getCurrentPositionVertex();

  /*! Add a vertex and all the edges from this vertex.*/
  bool addVertex(int, int, int);

  /*! Add edges from the specified vertex corresponding to uturn */
  bool addUturnEdges(Vertex*);

  /*! Add the first removed edge back to the graph */
  bool addRemovedEdge();
  
  /*! Get all obstacles on the specified segment */
  vector<Obstacle>& getAllObstaclesOnSegment(int segmentID);

  /*! Insert an obstacle into the map*/
  bool insertObstacle(int obstacleID, int obstacleSegmentID, int obstacleLaneID, int obstacleWaypointID,
		      int decayT = -1);

  /*! Remove all the obstacles */
  void removeAllObstacles();

  /*! Remove all the edges corresponding to the specified failed goal */
  void removeFailedGoalEdges(int segmentID1, int laneID1, int waypointID1, 
			     int segmentID2, int laneID2, int waypointID2, bool removeOtherEdges);

  /*! Remove all the edges which have max obstructed level */
  void removeAllObstructedEdges();

  /*! Clear m_removedEdges */
  void clearRemovedEdges();

  /*! Find the closest exit point */
  bool findExit(int segmentID, int laneID, int& waypointID);

  /*! Get the current position */
  double getNorthing(bool updateState = false);
  double getEasting(bool updateState = false);

  /*! Get the current speed */
  double getSpeed(bool updateState = false);

  /*! Get Estop status */
  int getEstopStatus();
  int getDstopStatus();
};

#endif  //TRAVGRAPHEST_HH
