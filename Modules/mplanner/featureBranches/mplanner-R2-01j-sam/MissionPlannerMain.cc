/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include <getopt.h>
#include <iostream>
#include "RoutePlanner.hh"
#include "dgcutils/DGCutils.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include "skynet/skynet.hh"
#include "cmdline.h"
#include <sys/time.h>
#include <sys/stat.h>
#include "mplannerDisplay.h"


using namespace std;

void SparrowDisplayLoop();
CSparrowHawk *shp;
  char RNDFFileName[500];
  char MDFFileName[500];

int main(int argc, char **argv) 
{
  gengetopt_args_info cmdline;
  int sn_key;
  bool noSparrow = false;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
  
  /* Figure out what skynet key to use */
  sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;

  /* Figure out if we want to use sparrow display */
  if(cmdline.disable_console_flag || cmdline.nosp_flag)
  {  
    noSparrow = true;
    cout << "NO SPARROW DISPLAY" << endl;
  }

  /* Figure out what RNDF and MDF we want to use */
  string RNDFFileNameStr, MDFFileNameStr;
  //char RNDFFileName[500];
  //char MDFFileName[500];
  struct stat buf;			/* status buffer for fstat */
  /* First see if the file exists in the current directory */
  ostringstream ossRNDF, ossMDF;
  ossRNDF << "./" << cmdline.rndf_arg;
  ossMDF << "./" << cmdline.mdf_arg;
  RNDFFileNameStr = ossRNDF.str();
  MDFFileNameStr = ossMDF.str();
  if (stat(RNDFFileNameStr.c_str(), &buf) == 0)
  {
    snprintf( RNDFFileName, sizeof(RNDFFileName), "%s", RNDFFileNameStr.c_str() );    
  }
  else if (getenv("DGC_CONFIG_PATH"))
  {
    ostringstream oss1;
    oss1 << getenv("DGC_CONFIG_PATH") << "/" << cmdline.rndf_arg;
    RNDFFileNameStr = oss1.str();
    snprintf( RNDFFileName, sizeof(RNDFFileName), "%s", RNDFFileNameStr.c_str() );
  }
  else
  {
    snprintf( RNDFFileName, sizeof(RNDFFileName), "%s", cmdline.rndf_arg );
    cerr << "unknown configuration path: please set DGC_CONFIG_PATH" << endl;
    cerr << "ERROR: cannot find RNDF file: " << RNDFFileName << endl;
    exit(1);
  }
  if (stat(MDFFileNameStr.c_str(), &buf) == 0)
  {
    snprintf( MDFFileName, sizeof(MDFFileName), "%s", MDFFileNameStr.c_str() );    
  }
  else if (getenv("DGC_CONFIG_PATH"))
  {
    ostringstream oss2;
    oss2 << getenv("DGC_CONFIG_PATH") << "/" << cmdline.mdf_arg;
    MDFFileNameStr = oss2.str();
    snprintf( MDFFileName, sizeof(MDFFileName), "%s", MDFFileNameStr.c_str() );
  }
  else
  {
    snprintf( MDFFileName, sizeof(MDFFileName), "%s", cmdline.mdf_arg );
    cerr << "unknown configuration path: please set DGC_CONFIG_PATH" << endl;  
    cerr << "ERROR: cannot find MDF file: " << MDFFileName << endl;
    exit(1);
  }

  cout << "RNDF file: " << RNDFFileName << endl;
  cout << "MDF file: " << MDFFileName << endl;

  /* Figure out where logged data go */
  string logFileName;
  FILE* logFile;
  if(cmdline.enable_logging_given)
  {
    string tmpMDFname;
    tmpMDFname.assign(MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("/")+1, 
		      MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("."));
    ostringstream oss;
    struct stat st;
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
    oss << timestr << "-mplanner-" << tmpMDFname << ".log";
    logFileName = oss.str();
    string suffix = "";
    
    // if it exists already, append .1, .2, .3 ... 
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
    logFile = fopen(logFileName.c_str(), "w");
    if (logFile == NULL)
    {
      cerr << "Cannot open log file: " << logFile << endl;
      exit(1);
    }
    cout << "log file: " << logFileName << endl;
    fprintf (logFile, "RNDF file: %s\n", RNDFFileName);
    fprintf (logFile, "MDF file: %s\n\n", MDFFileName);
  }

  CSparrowHawk &sh = SparrowHawk();  
  shp = &sh;

  RoutePlanner* mplanner = new RoutePlanner(sn_key, !cmdline.nowait_given, 
      noSparrow, RNDFFileName, MDFFileName, !cmdline.nomap_given, cmdline.enable_logging_given,
      logFile, cmdline.debug_arg, cmdline.verbose_given);

  mplanner->Start();

  if (!noSparrow)
  {
    DGCstartMemberFunctionThread(mplanner, &RoutePlanner::SparrowDisplayLoop);
    DGCstartMemberFunctionThread(mplanner, &RoutePlanner::UpdateSparrowVariablesLoop);
  }


  while (!mplanner->IsStopped())
  {
    sleep(1);
  }

  if(cmdline.enable_logging_given)
  {
    pclose(logFile);
  }
  return 0;
}

