
platform="*linux"
#platform="*darwin"
declare -a apps
apps+=("asim --rndf=routes-stluke/stluke_singleroad.rndf --rndf-start=1.1.1")
apps+=("UT_illegalpassinguturnsmallgraph")

for i in "${apps[@]}"; do
  appname=`echo $i | cut -f1 -d" "`
  echo appname = $appname
  appparams=`echo $i | cut -f1 -d" " --complement`
  app=`find ../../bin -name "$appname"`
  if [ -z app ]; then app=`find .. -name "$appname"`; fi
  echo $app $appparams
  { xterm -e "$app $appparams" & } 
  sleep 3
done


