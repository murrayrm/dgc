#ifndef MISSIONPLANNERINTERNALINTERFACES_HH
#define MISSIONPLANNERINTERNALINTERFACES_HH

enum DirectiveName{ NEXT_CHECKPOINT, END_OF_MISSION, PAUSE };
enum VehicleCap{ MAX_CAP, MEDIUM_CAP, MIN_CAP };

// Directive from Mission Control
struct MContrDirective
{
  MContrDirective()
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int goalID;
  DirectiveName name;
  // Parameters
  // The waypoint id of the next checkpoint
  int segmentID;
  int laneID;
  int waypointID;
  VehicleCap vehicleCap;
};


struct MControlDirectiveResponse
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ NO_ROUTE, VEHICLE_CAP };
  MControlDirectiveResponse()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int goalID;
  Status status;
  ReasonForFailure reason;
};

#endif //MISSIONPLANNERINTERNALINTERFACES_HH

