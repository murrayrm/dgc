/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */
 
 #ifndef TRAFFICPLANNER_HH
#define TRAFFICPLANNER_HH

#include "skynettalker/StateClient.hh"
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <string>
#include "MissionUtils.hh"
#include "skynettalker/SegGoalsTalker.hh"

class CTrafficPlanner : public CStateClient, public CSegGoalsTalker
{
  SegGoalsStatus* segGoalsStatus;

public:
  /*! Contstructor */
  CTrafficPlanner(int skynetKey, bool bWaitForStateFill);
  /*! Standard destructor */
  ~CTrafficPlanner();
  void TPlanningLoop(void);
};

#endif  // TRAFFICPLANNER_HH
