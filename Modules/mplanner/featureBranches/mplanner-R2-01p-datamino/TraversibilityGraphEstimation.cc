/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */

#include "TraversibilityGraphEstimation.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "alice/AliceConstants.h"
#include <boost/serialization/vector.hpp>
#include <math.h>
#include <sys/time.h>

#define PI 3.14159265
#define MAX_NUM_OF_WAYPOINTS 1000
using namespace std;


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Basic constructor: no threads
TraversibilityGraphEstimation::TraversibilityGraphEstimation
(int skynetKey, bool waitForStateFill) :
  CSkynetContainer(MODmissionplanner, skynetKey), CStateClient(waitForStateFill),
  m_snKey(skynetKey)
{  
  DGCcreateMutex(&m_ObstaclesMutex);
  DGCcreateMutex(&m_removedEdgesMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
TraversibilityGraphEstimation::~TraversibilityGraphEstimation()
{
  DGCdeleteMutex(&m_receivedGraphMutex);
  DGCdeleteMutex(&m_condNewGraphMutex);
  DGCdeleteMutex(&m_ObstaclesMutex);
  DGCdeleteCondition(&m_condNewGraph);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::init
    (RNDF* rndf, bool listenToMapper, bool nosparrow, bool logData, FILE* logFile, int debugLevel, bool verbose)
{
  m_listenToMapper = listenToMapper; 
  m_nosparrow = nosparrow; 
  m_logData = logData;
  m_logFile = logFile;
  m_rndf = rndf;
  m_graph = new Graph(rndf);
  m_receivedGraph = NULL;
  DEBUG_LEVEL = debugLevel; 
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }
  m_edgeCost.push_back(COST_INTERSECTION);
  m_edgeCost.push_back(COST_INTERSECTION);
  m_edgeCost.push_back(COST_INTERSECTION);
  m_edgeCost.push_back(COST_CHANGE_LANE);
  m_edgeCost.push_back(COST_UTURN);
  m_edgeCost.push_back(COST_KTURN);
  m_edgeCost.push_back(COST_ZONE);
  m_edgeCost.push_back(COST_FIRST_SEGMENT);
  m_edgeCost.push_back(COST_STOP_SIGN);
  m_edgeCost.push_back(COST_NOT_VISITED);
  m_edgeCost.push_back(COST_OBSTRUCTED);

  double avgSpeed = (MIN_SPEED_LIMIT + MAX_SPEED_LIMIT)/2;
  m_avgSpeeds.resize(m_graph->getNumOfSegments() + m_graph->getNumOfZones() + 1);
  for (unsigned i=0; i < m_avgSpeeds.size(); i++) {
    m_avgSpeeds[i] = avgSpeed;
  }

  DGCcreateMutex(&m_receivedGraphMutex);
  DGCcreateMutex(&m_condNewGraphMutex);
  DGCcreateCondition(&m_condNewGraph);
  m_condNewGraph.bCond = false;
  m_condNewGraph.pMutex = m_condNewGraphMutex;
  if (listenToMapper) {
    DGCstartMemberFunctionThread(this, &TraversibilityGraphEstimation::getGraphThread);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateEdgeCostParam(vector<double> edgeCost)
{
  m_edgeCost = edgeCost;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateSpeed(vector<double> avgSpeeds)
{
  m_avgSpeeds = avgSpeeds;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::newGraph()
{
  DGClockMutex(&m_condNewGraph.pMutex);
  bool condNewGraph = m_condNewGraph.bCond;
  stringstream s("");
  s << "TraversibilityGraphEstimation::newGraph: See if we get a new graph ... " << condNewGraph << "\n";
  print(s.str(), 0);
  DGCunlockMutex(&m_condNewGraph.pMutex);
  return condNewGraph;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateGraph(bool requestMapper)
{
  #warning updateGraph deletes the graph that is accessible from other classes through the getGraph() function

  int graphRequestSocket = m_skynet.get_send_sock(SNglobalGloNavMapRequest);
  stringstream s("");
  if (requestMapper && m_listenToMapper) {  
    bool requestGraph = true;
    s << "TraversibilityGraphEstimation::updateGraph: Waiting for mapper\n";
    print(s.str(), -1);

    while (!newGraph()) {
      s.str("");
      s << "TraversibilityGraphEstimation::Sending request to mapper\n";
      print(s.str(),0);
      m_skynet.send_msg(graphRequestSocket, &requestGraph, sizeof(bool) , 0);
      sleep(5);
    }
  }

  DGClockMutex(&m_receivedGraphMutex);
  // Swap pointers between m_graph and m_receivedGraph
  if (m_receivedGraph != NULL) {
    delete m_graph;
    m_graph = m_receivedGraph;
    m_receivedGraph = NULL;
  }
  else if (!m_listenToMapper) {
    m_graph->init(m_rndf);
  }
  DGCunlockMutex(&m_receivedGraphMutex);

  if (!m_listenToMapper || m_graph->getVertex(0,0,0) == NULL) {
    addCurrentPositionToGraph();
  }

  // Update the weight of edges based on inserted obstacles
  updateObstructedEdges();

  m_graph->updateEdgeCost(m_edgeCost, m_avgSpeeds);
  removeEdges();
  updateCurrentPositionEdges();

  DGClockMutex(&m_condNewGraph.pMutex);
  m_condNewGraph.bCond = false;
  DGCunlockMutex(&m_condNewGraph.pMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::waitForGraph()
{
  if (m_listenToMapper) {
    stringstream s("");
    s << "TraversibilityGraphEstimation::waitForGraph: Waiting for graph...\n";
    print(s.str(), -1);

    DGCWaitForConditionTrue(m_condNewGraph);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
Graph& TraversibilityGraphEstimation::getGraph()
{
  if (m_nosparrow && DEBUG_LEVEL > 5) {
    cout << "getGraph()" << endl;
    DGClockMutex(&m_receivedGraphMutex);
    if (m_receivedGraph != NULL) {
      cout << "m_receivedGraph: " << endl;
      m_receivedGraph->print();
    }
    cout << "m_graph: " << endl;
    m_graph->print();
    DGCunlockMutex(&m_receivedGraphMutex);
    cout << "finished m_graph" << endl;
  }

  return *m_graph;
}

const Graph& TraversibilityGraphEstimation::getGraph() const
{
  return *m_graph;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addVertex(int segmentID, int laneID, int waypointID)
{
  GPSPoint* point = NULL;
  if (segmentID == 0)
    return addCurrentPositionToGraph();
  else if (laneID != 0)
    point = m_rndf->getWaypoint(segmentID, laneID, waypointID);
  else
    point = m_rndf->getPerimeterPoint(segmentID, waypointID);

  if (point == NULL)
    return false;

  return m_graph->addGPSPointToGraph (point, m_rndf);
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addUturnEdges(Vertex* vertex1)
{
  if (vertex1->getSegmentID() == 0)
  {
    double northingFront = m_state.utmNorthing + DIST_REAR_AXLE_TO_FRONT*cos(m_state.utmYaw);
    double eastingFront = m_state.utmEasting + DIST_REAR_AXLE_TO_FRONT*sin(m_state.utmYaw);
    double yaw = m_state.utmYaw;
    if (yaw > PI)
      yaw = yaw - 2*PI;

    double headingDiff, headingLaneDiff;
    
    bool edgeAdded = m_graph->addCurrentPosition(northingFront, eastingFront, yaw, 
						 m_rndf, 50*PI/180, VEHICLE_MIN_TURNING_RADIUS + 0.1, 
						 45*PI/180, 5000, headingDiff,
						 headingLaneDiff, true);
    if (m_logData)  {
      fprintf (m_logFile, "After adding uturn edges before updating obstructing edges\n");
      fprintf (m_logFile, "Graph:\n");
      m_graph->log(m_logFile);
      fprintf (m_logFile, "\n");
    }
    updateObstructedEdges();
    updateCurrentPositionEdges();
    return edgeAdded;
  }
  else
  {
    if (vertex1->getSegmentID() > m_rndf->getNumOfSegments())
      return false;
    
    GPSPoint* point = m_rndf->getWaypoint(vertex1->getSegmentID(), vertex1->getLaneID(),
					  vertex1->getWaypointID());  
    if (point == NULL)
      return false;
    
    bool edgeAdded = m_graph->addUturnEdges(point->getNorthing(), point->getEasting(), vertex1, point, m_rndf);
    if (m_logData) {
      fprintf (m_logFile, "After adding uturn edges before updating obstructing edges\n");
      fprintf (m_logFile, "Graph:\n");
      m_graph->log(m_logFile);
      fprintf (m_logFile, "\n");
    }
    updateObstructedEdges();
    return edgeAdded;
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addRemovedEdge()
{
  stringstream s("");
  bool ret = false;

  DGClockMutex(&m_removedEdgesMutex);
  if (m_removedEdges.size() > 0) {
    EdgeID edgeToAdd = m_removedEdges.front();
    s << "TraversibilityGraphEstimation::addRemovedEdge: Adding the removed edge from "
      << edgeToAdd.prevSegmentID
      << "." << edgeToAdd.prevLaneID << "."
      << edgeToAdd.prevWaypointID << " to "
      << edgeToAdd.nextSegmentID << "."
      << edgeToAdd.nextLaneID << "."
      << edgeToAdd.nextWaypointID << " to the graph\n";
    print(s.str(), -1);

    bool edgeAdded = m_graph->addEdge(edgeToAdd.prevSegmentID, edgeToAdd.prevLaneID, edgeToAdd.prevWaypointID,
		     edgeToAdd.nextSegmentID, edgeToAdd.nextLaneID, edgeToAdd.nextWaypointID,
		     edgeToAdd.length, edgeToAdd.weight, edgeToAdd.type, edgeToAdd.avgSpeed, edgeToAdd.isVisited);
    Edge* tmpEdge = m_graph->getEdge(edgeToAdd.prevSegmentID, edgeToAdd.prevLaneID, edgeToAdd.prevWaypointID,
				     edgeToAdd.nextSegmentID, edgeToAdd.nextLaneID, edgeToAdd.nextWaypointID);
    if (edgeAdded && tmpEdge != NULL)
      tmpEdge->setObstructedLevel(MAX_OBSTRUCTED_LEVEL);

    m_removedEdges.pop_front();
    ret = true;
  }
  DGCunlockMutex(&m_removedEdgesMutex);
  return ret;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<Obstacle>& TraversibilityGraphEstimation::getAllObstaclesOnSegment(int segmentID)
{  
  m_obstaclesOnSegment.clear();
  DGClockMutex(&m_ObstaclesMutex);
  for (unsigned i = 0; i < m_obstacles.size(); i++) {
    if (m_obstacles[i].getSegmentID() == segmentID) {
      m_obstaclesOnSegment.push_back(m_obstacles[i]);
    }
  }
  DGCunlockMutex(&m_ObstaclesMutex);
  
  return m_obstaclesOnSegment;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::insertObstacle(int obstacleID, int obstacleSegmentID,
						   int obstacleLaneID, int obstacleWaypointID)
{
  // Check that the segment ID, lane ID and waypoint ID are valid
  int numOfSegments = m_rndf->getNumOfSegments();
  int numOfZones = m_rndf->getNumOfZones();
  int numOfSegmentsAndZones = numOfSegments + numOfZones;
  stringstream s("");

  if (obstacleSegmentID < 1 || obstacleSegmentID > numOfSegmentsAndZones) {
    s.str("");
    s << "Cannot insert obstacle at segment " << obstacleSegmentID
      << " lane " << obstacleLaneID << " waypoint " << obstacleWaypointID << "\n";
    s << "Segment ID must be between 1 and " <<  numOfSegmentsAndZones << "\n";
    printError("insertObstacle", s.str());
    return false;
  }

  if (obstacleSegmentID <= numOfSegments) {
    int numOfLanes = m_rndf->getSegment(obstacleSegmentID)->getNumOfLanes();
    if (obstacleLaneID < 1 || obstacleLaneID > numOfLanes) {
      s.str("");
      s << "Cannot insert obstacle at segment " << obstacleSegmentID
	<< " lane " << obstacleLaneID << " waypoint " << obstacleWaypointID << "\n";
      s << "Lane ID must be between 1 and " << numOfLanes << "for segment "
	<< obstacleSegmentID << "\n";
      printError("insertObstacle", s.str());
      return false;
    }
  }
  else if (obstacleLaneID != 0) {
    int numOfSpots = m_rndf->getZone(obstacleSegmentID)->getNumOfSpots();
    if (obstacleLaneID < 1 || obstacleLaneID > numOfSpots) {
      s.str("");
      s << "Cannot insert obstacle at segment " << obstacleSegmentID
	<< " lane " << obstacleLaneID << " waypoint " << obstacleWaypointID << "\n";
      s << "Lane ID must be between 0 and " << numOfSpots << " for segment "
	<< obstacleSegmentID << "\n";
      printError("insertObstacle", s.str());
      return false;
    }
  }

  if (obstacleSegmentID <= numOfSegments) {
    int numOfWaypoints =
        m_rndf->getLane(obstacleSegmentID, obstacleLaneID)->getNumOfWaypoints();
    if (obstacleWaypointID < 1 || obstacleWaypointID > numOfWaypoints) {
      s.str("");
      s << "Cannot insert obstacle at segment " << obstacleSegmentID
	<< " lane " << obstacleLaneID << " waypoint " << obstacleWaypointID << "\n";
      s << "Waypoint ID must be between 1 and " << numOfWaypoints << " for segment "
	<< obstacleSegmentID << " lane " << obstacleLaneID << "\n";
      printError("insertObstacle", s.str());
      return false;
    }
  }
  else if (obstacleLaneID == 0) {
    int numOfPerimeterPoints =
        m_rndf->getZone(obstacleSegmentID)->getNumOfPerimeterPoints();
    if (obstacleWaypointID < 1 || obstacleWaypointID > numOfPerimeterPoints) {
      s.str("");
      s << "Cannot insert obstacle at segment " << obstacleSegmentID
	<< " lane " << obstacleLaneID << " waypoint " << obstacleWaypointID << "\n";
      s << "Waypoint ID must be between 1 and " << numOfPerimeterPoints << " for segment "
	<< obstacleSegmentID << " lane " << obstacleLaneID << "\n";
      printError("insertObstacle", s.str());
      return false;
    }
  }
  else if (obstacleWaypointID < 1 || obstacleWaypointID > 2) {
    s.str("");
    s << "Cannot insert obstacle at segment " << obstacleSegmentID
      << " lane " << obstacleLaneID << " waypoint " << obstacleWaypointID << "\n";
    s << "Waypoint ID must be between 1 and 2 for segment " << obstacleSegmentID
      << " lane " << obstacleLaneID << "\n";
    printError("insertObstacle", s.str());
    return false;
  }
  
  DGClockMutex(&m_ObstaclesMutex);
  int obstacleIndex = getObstacleIndex(obstacleID);
  if ((unsigned)obstacleIndex < m_obstacles.size()) {
    if (m_obstacles[obstacleIndex].getSegmentID() != obstacleSegmentID) {
      int newObstacleID = obstacleID + 1;
      while((unsigned)getObstacleIndex(newObstacleID) < m_obstacles.size())
        newObstacleID++;

      s.str("");
      s << "Obstacle ID " << obstacleID << " already exists on segment "
	<< m_obstacles[obstacleIndex].getSegmentID() << "." << " Changed obstacle ID to "
	<< newObstacleID << "\n";
      printError("insertObstacle", s.str());
      Obstacle obstacle(newObstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      m_obstacles.push_back(obstacle);

      s.str("");
      s << "TraversibilityGraphEstimation::insertObstacle: Added obstacle "
	<< newObstacleID << " at segment "
	<< obstacleSegmentID << " lane " << obstacleLaneID << " waypoint "
	<< obstacleWaypointID << " to the obstacle list\n";
      print(s.str(), -1);
    }
    else {
      bool obstructedLaneAdded =
          m_obstacles[obstacleIndex].addObstructedLane(obstacleLaneID, obstacleWaypointID);
      if (obstructedLaneAdded) {
	s.str("");
        s << "TraversibilityGraphEstimation::insertObstacle: Obstacle " 
	  << obstacleID << " on segment "
	  << obstacleSegmentID << ": Added obstructed lane "
	  << obstacleLaneID << " waypoint " << obstacleWaypointID << "\n";
	print(s.str(), -1);
      }
      else {
	s.str("");
	s << "Obstacle " << obstacleID <<" on segment " << obstacleSegmentID
	  << ": Cannot add obstructed lane " << obstacleLaneID << " waypoint "
	  << obstacleWaypointID << ".\n"
	  << "This obstructed lane already exists.\n"
	  << "Change obstacle ID if you still want to add this obstacle\n";
	printError("insertObstacle", s.str());
      }
    }
  }
  else {
    Obstacle obstacle(obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    m_obstacles.push_back(obstacle);
    s.str("");
    s << "TraversibilityGraphEstimation::insertObstacle: Added obstacle " 
      << obstacleID << " at segment " << obstacleSegmentID 
      << " lane " << obstacleLaneID << " waypoint " << obstacleWaypointID << " to the obstacle list\n";
    print(s.str(), -1);
  }
  DGCunlockMutex(&m_ObstaclesMutex);
  return true;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::removeAllObstacles()
{
  clearRemovedEdges();
  DGClockMutex(&m_ObstaclesMutex);
  m_obstacles.clear();
  DGCunlockMutex(&m_ObstaclesMutex);
  stringstream s("");
  s << "TraversibilityGraphEstimation::removeAllObstacles\n";
  print(s.str(), -1);
  cerr << "Removed all obstacles" << endl;
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
int TraversibilityGraphEstimation::getObstacleIndex(int obstacleID)
{
  int i = 0;
  while ((unsigned)i < m_obstacles.size() && m_obstacles[i].getObstacleID() != obstacleID)
    i++;
  
  return i;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateObstructedEdges()
{
  DGClockMutex(&m_ObstaclesMutex);
  for(unsigned i = 0; i < m_obstacles.size(); i++) {
    int obsSegmentID = m_obstacles[i].getSegmentID();
    vector<int> obsLaneIDs = m_obstacles[i].getLaneIDs();
    vector<int> obsWaypointIDs = m_obstacles[i].getWaypointIDs();
    for (unsigned j = 0; j < obsLaneIDs.size(); j++) {
      m_graph->addObstacle(obsSegmentID, obsLaneIDs[j], obsWaypointIDs[j]);
      stringstream s("");
      s << "TraversibilityGraphEstimation::updateObstructedEdges: "
	<< "Obstacle at " << obsSegmentID << "." << obsLaneIDs[j] << "."
	<< obsWaypointIDs[j] << " is addded.\n";
      print(s.str(), 0);
    }
  }
  DGCunlockMutex(&m_ObstaclesMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::removeFailedGoalEdges(int segmentID1, int laneID1, int waypointID1,
							  int segmentID2, int laneID2, int waypointID2,
							  bool removeOtherEdges)
{
  stringstream s("");

  int entryWaypointID = 1;
  int exitWaypointID = MAX_NUM_OF_WAYPOINTS;

  if (removeOtherEdges) {
    bool entryFound = false;
    int waypointID = waypointID2;
    Vertex* vertex;
    while (!entryFound && waypointID >= 1) {
      vertex = m_graph->getVertex(segmentID2, laneID2, waypointID);
      if (vertex != NULL)
	entryFound = vertex->isEntry();
      waypointID--;
    }
    if (entryFound)
      entryWaypointID = waypointID + 1;
    
    bool exitFound = false;
    int numOfWaypoints = MAX_NUM_OF_WAYPOINTS;
    if (segmentID1 == 0) {
      segmentID1 = segmentID2;
      laneID1 = laneID2;
      waypointID1 = waypointID2;
    }
    waypointID = waypointID1;
    Lane* failedLane = m_rndf->getLane(segmentID1, laneID1);
    if (failedLane != NULL) {
      numOfWaypoints = failedLane->getNumOfWaypoints();
    }
    else {
      s.str("");
      s << "Cannot find lane "
	<< segmentID1 << "." << laneID1 << ". Other edges will not be removed.\n";
      printError("removeFailedGoalEdges", s.str());
 
      removeOtherEdges = false; 
    }
    exitWaypointID = numOfWaypoints; 
    while (!exitFound && waypointID <= numOfWaypoints) { 
      vertex = m_graph->getVertex(segmentID1, laneID1, waypointID); 
      if (vertex != NULL) 
	exitFound = vertex->isExit(); 
      waypointID++;
    } 
    if (exitFound) 
      exitWaypointID = waypointID - 1;
  }


  // Search the graph to find edges that need to be removed
  vector<Vertex*> vertices = m_graph->getVertices();
  for (unsigned i=0; i < vertices.size(); i++) {
    vector<Edge*> edges = vertices[i]->getEdges();
    for (unsigned j=0; j < edges.size(); j++) {
      if ( edges[j]->getObstructedLevel() >= MAX_OBSTRUCTED_LEVEL && 
	   segmentID1 == edges[j]->getPrevious()->getSegmentID() &&
	   laneID1 == edges[j]->getPrevious()->getLaneID() &&
	   (waypointID1 == edges[j]->getPrevious()->getWaypointID() ||
	    (removeOtherEdges && exitWaypointID >= edges[j]->getPrevious()->getWaypointID())) &&
	   segmentID2 == edges[j]->getNext()->getSegmentID() &&
	   laneID2 == edges[j]->getNext()->getLaneID() &&
	   (waypointID2 == edges[j]->getNext()->getWaypointID() ||
	    (removeOtherEdges && entryWaypointID <= edges[j]->getNext()->getWaypointID())) )
      {
	s.str("");
	s << "TraversibilityGraphEstimation::removeFailedGoalEdges: "
	  << "Setting the edge from waypoint " 
	  << (edges[j]->getPrevious())->getSegmentID()
	  << "." << (edges[j]->getPrevious())->getLaneID() << "."
	  << (edges[j]->getPrevious())->getWaypointID() << " to " 
	  << (edges[j]->getNext())->getSegmentID()
	  << "." << (edges[j]->getNext())->getLaneID() << "." 
	  << (edges[j]->getNext())->getWaypointID()
	  << " to be fully blocked\n";
	print(s.str(), 0);
	// Remove the edge corresponding to the failed segment goals from the graph
	EdgeID remEdge(edges[j]->getPrevious()->getSegmentID(), edges[j]->getPrevious()->getLaneID(),
		       edges[j]->getPrevious()->getWaypointID(), edges[j]->getNext()->getSegmentID(),
		       edges[j]->getNext()->getLaneID(), edges[j]->getNext()->getWaypointID(), 
		       edges[j]->getLength(), edges[j]->getWeight(), edges[j]->getType(), edges[j]->getAvgSpeed(),
		       edges[j]->getIsVisited());
	m_graph->removeEdge(edges[j]);
	DGClockMutex(&m_removedEdgesMutex);
	m_removedEdges.push_back(remEdge);
	DGCunlockMutex(&m_removedEdgesMutex);
      }
    }
  }

  updateCurrentPositionEdges();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::removeAllObstructedEdges()
{
  stringstream s("");
  s << "TraversibilityGraphEstimation::removeAllObstructedEdges: \n";
  // Search the graph to find edges that need to be removed
  vector<Vertex*> vertices = m_graph->getVertices();
  for (unsigned i=0; i < vertices.size(); i++) {
    vector<Edge*> edges = vertices[i]->getEdges();
    for (unsigned j=0; j < edges.size(); j++) {
      if (edges[j]->getType() == Edge::ROAD_SEGMENT && edges[j]->getObstructedLevel() >= 
	      MAX_OBSTRUCTED_LEVEL) {
	s << "Setting the edge from waypoint " << (edges[j]->getPrevious())->getSegmentID()
	  << "." << (edges[j]->getPrevious())->getLaneID() << "."
	  << (edges[j]->getPrevious())->getWaypointID() << " to " 
	  << (edges[j]->getNext())->getSegmentID()
	  << "." << (edges[j]->getNext())->getLaneID() << "." 
	  << (edges[j]->getNext())->getWaypointID()
	  << " to be fully blocked\n";
	
	// Remove the edge corresponding to the failed segment goals from the graph
	EdgeID remEdge(edges[j]->getPrevious()->getSegmentID(), edges[j]->getPrevious()->getLaneID(),
		       edges[j]->getPrevious()->getWaypointID(), edges[j]->getNext()->getSegmentID(),
		       edges[j]->getNext()->getLaneID(), edges[j]->getNext()->getWaypointID(), 
		       edges[j]->getLength(), edges[j]->getWeight(), edges[j]->getType(), edges[j]->getAvgSpeed(),
		       edges[j]->getIsVisited());
	m_graph->removeEdge(edges[j]);
	DGClockMutex(&m_removedEdgesMutex);
	m_removedEdges.push_back(remEdge);
	DGCunlockMutex(&m_removedEdgesMutex);
      }
    }
  }
  print(s.str(), 0);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::removeEdges()
{
  // Search the graph to find edges that need to be removed
  vector<Vertex*> vertices = m_graph->getVertices();
  for (unsigned i=0; i < vertices.size(); i++) {
    vector<Edge*> edges = vertices[i]->getEdges();
    for (unsigned j=0; j < edges.size(); j++) {
      DGClockMutex(&m_removedEdgesMutex);
      for (unsigned k = 0; k < m_removedEdges.size(); k++) {
	Vertex* prevVertex = edges[j]->getPrevious();
	Vertex* nextVertex = edges[j]->getNext();
	if (m_removedEdges[k].prevSegmentID == prevVertex->getSegmentID() &&
	    m_removedEdges[k].prevLaneID == prevVertex->getLaneID() &&
	    m_removedEdges[k].prevWaypointID == prevVertex->getWaypointID() &&
	    m_removedEdges[k].nextSegmentID == nextVertex->getSegmentID() &&
	    m_removedEdges[k].nextLaneID == nextVertex->getLaneID() &&
	    m_removedEdges[k].nextWaypointID == nextVertex->getWaypointID())
	{
	  stringstream s("");
	  s << "TraversibilityGraphEstimation::removeEdges:  removed edge from "
	    << prevVertex->getSegmentID() << "." << prevVertex->getLaneID()
	    << "." << prevVertex->getWaypointID() << " to "
	    << nextVertex->getSegmentID() << "." << nextVertex->getLaneID()
	    << "." << nextVertex->getWaypointID() << "\n";
	  print(s.str(), 0);
	  m_graph->removeEdge(edges[j]);
	}
      }
      DGCunlockMutex(&m_removedEdgesMutex);
    }
  }  
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateCurrentPositionEdges()
{
  // Update the edge from current position
  Vertex* currentPos = m_graph->getVertex(0,0,0);
  if (currentPos == NULL)
    return;
  vector<Edge*> edges = currentPos->getEdges();
  stringstream s("");
  s << "TraversibilityGraphEstimation::updateCurrentPositionEdges: Removed edges: \n";
  DGClockMutex(&m_removedEdgesMutex);
  for (unsigned i = 0; i < m_removedEdges.size(); i++) {
    s << "    " << m_removedEdges[i].prevSegmentID << "."
      << m_removedEdges[i].prevLaneID << "." << m_removedEdges[i].prevWaypointID
      << " -> " << m_removedEdges[i].nextSegmentID << "." << m_removedEdges[i].nextLaneID
      << "." << m_removedEdges[i].nextWaypointID << "\t";
  }

  for (unsigned i = 0; i < edges.size(); i++) {
    Vertex* nextVertex = edges[i]->getNext();
    s << "  closest vertex is " << nextVertex->getSegmentID() << "." 
      << nextVertex->getLaneID() << "." << nextVertex->getWaypointID() << "\n";
    if (edges[i]->getType() != Edge::UTURN && edges[i]->getType() != Edge::KTURN) {
      s << "   See if edge 0.0.0 -> " << nextVertex->getSegmentID() << "."
	<< nextVertex->getLaneID() << "." << nextVertex->getWaypointID() << " is removed\n";
      bool removeCurrentEdge = false;
      for (unsigned j = 0; j < m_removedEdges.size(); j++) {
	s << "    edgeRemoved = " << m_removedEdges[j].prevSegmentID << "."
	  << m_removedEdges[j].prevLaneID << "." << m_removedEdges[j].prevWaypointID 
	  << " -> " << m_removedEdges[j].nextSegmentID << "."
	  << m_removedEdges[j].nextLaneID << "." << m_removedEdges[j].nextWaypointID
	  << " \t nextVertex = " << nextVertex->getSegmentID() << "."
	  << nextVertex->getLaneID() << "." << nextVertex->getWaypointID() << "\n";
	if (m_removedEdges[j].prevSegmentID == nextVertex->getSegmentID() &&
	    m_removedEdges[j].prevLaneID == nextVertex->getLaneID() &&
	    m_removedEdges[j].prevWaypointID <= nextVertex->getWaypointID() &&
	    m_removedEdges[j].nextSegmentID == nextVertex->getSegmentID() &&
	    m_removedEdges[j].nextLaneID == nextVertex->getLaneID() &&
	    m_removedEdges[j].nextWaypointID >= nextVertex->getWaypointID())
	{
	  removeCurrentEdge = true;
	  break;
	}
      }
      if (removeCurrentEdge) {
	s << "  removed edge from " << edges[i]->getPrevious()->getSegmentID() << "."
	  << edges[i]->getPrevious()->getLaneID() << "." 
	  << edges[i]->getPrevious()->getWaypointID() << " to "
	  << edges[i]->getNext()->getSegmentID() << "." << edges[i]->getNext()->getLaneID()
	  << "." << edges[i]->getNext()->getWaypointID() <<"\n";
	m_graph->removeEdge(edges[i]);
      }
    }
  }
  DGCunlockMutex(&m_removedEdgesMutex);
  print(s.str(), 2);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::clearRemovedEdges()
{
  DGClockMutex(&m_removedEdgesMutex);
  m_removedEdges.clear();
  DGCunlockMutex(&m_removedEdgesMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::getGraphThread()
{
  stringstream s("");
  s << "TraversibilityGraphEstimation::getGraphThread: Start listening to mapper\n";
  print(s.str(), -1);
  SkynetTalker<Graph> graphTalker(m_snKey, SNglobalGloNavMapFromGloNavMapLib, MODmapping);
  while(true) {
    //graphTalker.waitForMessage();
    //m_receivedGraph = new Graph();
    Graph* graph = new Graph();
    bool graphReceived = graphTalker.receive(graph);
    DGClockMutex(&m_receivedGraphMutex); 
    if (graphReceived) { 
      if (m_receivedGraph != NULL)
	delete m_receivedGraph;
      m_receivedGraph = graph;
      DGCSetConditionTrue(m_condNewGraph); 
      s.str("");
      s << "TraversibilityGraphEstimation::getGraphThread: Received a new graph\n";
      print(s.str(), -1);
      if (m_nosparrow && DEBUG_LEVEL > 4) {
	cout << "received graph: " << endl;
	m_receivedGraph->print();
      }
    } 
    else {
      s.str("");
      s << "Error receiving graph\n";
      printError("getGraphThread", s.str());
      delete m_receivedGraph;
      m_receivedGraph = NULL;
    }
    DGCunlockMutex(&m_receivedGraphMutex); 
    usleep(100000); 
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addCurrentPositionToGraph()
{ 
  UpdateState();
  double northingFront = m_state.utmNorthing + DIST_REAR_AXLE_TO_FRONT*cos(m_state.utmYaw);
  double eastingFront = m_state.utmEasting + DIST_REAR_AXLE_TO_FRONT*sin(m_state.utmYaw);
  double yaw = m_state.utmYaw;
  if (yaw >= PI) {
    yaw = yaw - 2*PI;
  }
  double headingDiff, headingLaneDiff;

  bool currentPosAdded = m_graph->addCurrentPosition(northingFront, eastingFront, yaw, 
						     m_rndf, 50*PI/180, VEHICLE_MIN_TURNING_RADIUS + 0.1, 
						     45*PI/180, 5000, headingDiff,
						     headingLaneDiff, false);
  stringstream s("");
  s << "TraversibilityGraphEstimation::addCurrentPositionToGraph: "
    << "Current state is Northing = " << northingFront 
    << " Easting = " << eastingFront <<  " Yaw = " << yaw << "\n";
  s << "Closest waypoints are ";
  if (m_graph->getVertex(0,0,0) != NULL) {
    vector<Edge*> edgesFromCurrPos = m_graph->getVertex(0,0,0)->getEdges();
    for (unsigned i = 0; i < edgesFromCurrPos.size(); i++) { 
      s << edgesFromCurrPos[i]->getNext()->getSegmentID() << "."
	<< edgesFromCurrPos[i]->getNext()->getLaneID() << "."
	<< edgesFromCurrPos[i]->getNext()->getWaypointID();
      GPSPoint* closestPoint;
      if (edgesFromCurrPos[i]->getNext()->getLaneID() != 0) {
	closestPoint = m_rndf->getWaypoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					   edgesFromCurrPos[i]->getNext()->getLaneID(),
					   edgesFromCurrPos[i]->getNext()->getWaypointID());
      }
      else {
	closestPoint = m_rndf->getPerimeterPoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
						 edgesFromCurrPos[i]->getNext()->getWaypointID());
      }
      if (closestPoint != NULL) {
	s << "(" << closestPoint->getNorthing() - northingFront
	  << ", " << closestPoint->getEasting() - eastingFront
	  << ", " << 180/PI * ( atan2(closestPoint->getEasting() - eastingFront, 
				      closestPoint->getNorthing() - northingFront))
	  << ", " << 180/PI * headingDiff << ", " << 180/PI * headingLaneDiff
	  << ")   ";
      }
    }
    s << "\n";
  }
  print(s.str(), -1);
  return currentPosAdded;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double TraversibilityGraphEstimation::getNorthing()
{
  return m_state.utmNorthing;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double TraversibilityGraphEstimation::getEasting()
{
  return m_state.utmEasting;
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::print(string mess, int d)
{
  stringstream s("");
  s << getTime() << "\t" << mess;
  if (DEBUG_LEVEL > d) {
    if (m_nosparrow) {
      cout << mess;
    }
    else {
      SparrowHawk().log(mess);
    }
  }
  if (m_logData) {
    fprintf(m_logFile, s.str().c_str());
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::printError(string funcName, string errorMessage, bool error)
{
  stringstream s1("");
  if (error)
    s1 << "\nERROR: TravGraphEstimation::" << funcName << " : ";
  s1 << errorMessage << "\n";
  cerr << s1.str();

  stringstream s2("");
  s2 << getTime() << "\t" << s1.str();
  if (m_logData) {
    fprintf(m_logFile, s2.str().c_str());
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
uint64_t TraversibilityGraphEstimation::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

