/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */


#include "RoutePlanner.hh"
#include "interfaces/sn_types.h"
#include "mplannerDisplay.h"

using namespace std;

RoutePlanner::RoutePlanner(int skynetKey, bool waitForStateFill, bool nosparrow,
    char* RNDFFileName, char* MDFFileName, bool listenToMapper, int debugLevel,
    bool verbose)
  : CSkynetContainer(MODmissionplanner, skynetKey), m_snKey(skynetKey), m_nosparrow(nosparrow), 
    m_graphEstimator(skynetKey,  waitForStateFill)
{  
  DEBUG_LEVEL = debugLevel;
  
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }

  m_segGoalsSocket = m_skynet.get_send_sock(SNsegGoals);

  DGCcreateMutex(&m_FirstSegGoalsStatusReceivedMutex);
  DGCcreateMutex(&m_SegGoalsSeqMutex);
  DGCcreateMutex(&m_SegGoalsStatusMutex);
  DGCcreateMutex(&m_NextCheckpointIndexMutex);
  DGCcreateMutex(&m_ExecGoalsMutex);
  DGCcreateCondition(&m_FirstSegGoalsStatusReceivedCond);

  
  m_rndf = new RNDF();
  if (!m_rndf->loadFile(RNDFFileName))
  {
    cerr << "Error:  Unable to load RNDF file " << RNDFFileName
	 << ", exiting program" << endl;
    exit(1);
  }
  m_rndf->assignLaneDirection();
    
  if (DEBUG_LEVEL > 2)
  {
    m_rndf->print();
    cout << "Finished rndf" << endl;
  }
  else if (DEBUG_LEVEL > 0)
  {
    for (int i=1; i <= m_rndf->getNumOfSegments(); i++)
    {
      for (int j=1; j <= m_rndf->getSegment(i)->getNumOfLanes(); j++)
      {
        cout << "Lane " << i << "." << j << ":\t\t"
             << m_rndf->getLane(i,j)->getDirection() << endl;
      }
    }
  }

  m_missionControl.init(m_rndf, MDFFileName, nosparrow, debugLevel, verbose);

  m_graphEstimator.init(m_rndf, listenToMapper, nosparrow, debugLevel, verbose);
  m_travGraph = &m_graphEstimator.getGraph();

  m_minSpeedLimits.resize(m_travGraph->getNumOfSegments() + m_travGraph->getNumOfZones() + 1);
  m_maxSpeedLimits.resize(m_travGraph->getNumOfSegments() + m_travGraph->getNumOfZones() + 1);

  for (unsigned i=0; i < m_minSpeedLimits.size(); i++)
  {
    m_minSpeedLimits[i] = 0;
    m_maxSpeedLimits[i] = 0;
  }

  if (!loadMDFFile(MDFFileName, m_rndf))
  {
    cerr << "Error: Unable to load MDF file " << MDFFileName << ", exiting program"
         << endl;
    exit(1);
  }

  if (DEBUG_LEVEL > 1)
  {
    cout << endl << endl << "RNDFGRAPH:" << endl;
    m_travGraph->print();
    cout << "Finished RNDFGRAPH" << endl;
  }

  m_checkpointGoalID = new int[m_checkpointSequence.size()];
  m_nextCheckpointIndex = 0;
  
  m_nextGoalID = 1;

  // Initialize goals
  initializeSegGoals();

  m_receivedSegGoalsStatus = new SegGoalsStatus();
  m_numSegGoalsRequests = 0;

  // Initialize sparrow variables
  for (int i=0; i < NUM_SEGGOALS_DISPLAYED; i++)
  {
    sparrowNextGoalIDs[i] = 0;
    sparrowNextSegments1[i] = 0;
    sparrowNextLanes1[i] = 0;
    sparrowNextWaypoints1[i] = 0;
    sparrowNextSegments2[i] = 0;
    sparrowNextLanes2[i] = 0;
    sparrowNextWaypoints2[i] = 0;
  }

  m_bReceivedAtLeastOneSegGoalsStatus = false;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
RoutePlanner::~RoutePlanner() 
{
  delete m_rndf;
  delete m_travGraph;
  for (unsigned i=0; i < m_checkpointSequence.size(); i++)
  {
    delete m_checkpointSequence[i];
  }
  delete m_receivedSegGoalsStatus;
  delete shp;
  delete [] m_checkpointGoalID;
  delete [] sparrowNextGoalIDs;
  delete [] sparrowNextSegments1;
  delete [] sparrowNextLanes1;
  delete [] sparrowNextWaypoints1;
  delete [] sparrowNextSegments2;
  delete [] sparrowNextLanes2;
  delete [] sparrowNextWaypoints2;

  DGCdeleteMutex(&m_FirstSegGoalsStatusReceivedMutex);
  DGCdeleteMutex(&m_SegGoalsSeqMutex);
  DGCdeleteMutex(&m_SegGoalsStatusMutex);
  DGCdeleteMutex(&m_NextCheckpointIndexMutex);
  DGCdeleteMutex(&m_ExecGoalsMutex);
  DGCdeleteCondition(&m_FirstSegGoalsStatusReceivedCond);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::SparrowDisplayLoop()
{
  // SparrowHawk display
  CSparrowHawk &sh = SparrowHawk();  
  shp = &sh;
  
  sh.add_page(mplannertable, "Main");

  sh.rebind("snkey", &m_snKey);
  sh.rebind("tplanner_goalID", &(m_segGoalsStatus.goalID));
  sh.set_string("tplanner_status", "UNKNOWN");
  sh.set_readonly("snkey");
  sh.set_readonly("tplanner_goalID");
  sh.set_readonly("tplanner_status");

  sh.rebind("nextGoal1ID", &(sparrowNextGoalIDs[0]));
  sh.rebind("nextGoal1_segment1", &(sparrowNextSegments1[0]));
  sh.rebind("nextGoal1_lane1", &(sparrowNextLanes1[0]));
  sh.rebind("nextGoal1_waypoint1", &(sparrowNextWaypoints1[0]));
  sh.rebind("nextGoal1_segment2", &(sparrowNextSegments2[0]));
  sh.rebind("nextGoal1_lane2", &(sparrowNextLanes2[0]));
  sh.rebind("nextGoal1_waypoint2", &(sparrowNextWaypoints2[0]));
  sh.set_string("nextGoal1_type", "UNKNOWN");
  sh.rebind("minSpeed", &sparrowMinSpeed);
  sh.rebind("maxSpeed", &sparrowMaxSpeed);
  sh.rebind("illegalPassingAllowed", &sparrowIllegalPassingAllowed);
  sh.rebind("stopAtExit", &sparrowStopAtExit);
  sh.rebind("isExitCheckpoint", &sparrowIsExitCheckpoint);
  sh.set_readonly("nextGoal1ID");
  sh.set_readonly("nextGoal1_segment1");
  sh.set_readonly("nextGoal1_lane1");
  sh.set_readonly("nextGoal1_waypoint1");
  sh.set_readonly("nextGoal1_segment2");
  sh.set_readonly("nextGoal1_lane2");
  sh.set_readonly("nextGoal1_waypoint2");
  sh.set_readonly("nextGoal1_type");
  sh.set_readonly("minSpeed");
  sh.set_readonly("maxSpeed");
  sh.set_readonly("illegalPassingAllowed");
  sh.set_readonly("stopAtExit");
  sh.set_readonly("isExitCheckpoint");

  sh.rebind("nextGoal2ID", &(sparrowNextGoalIDs[1]));
  sh.rebind("nextGoal2_segment1", &(sparrowNextSegments1[1]));
  sh.rebind("nextGoal2_lane1", &(sparrowNextLanes1[1]));
  sh.rebind("nextGoal2_waypoint1", &(sparrowNextWaypoints1[1]));
  sh.rebind("nextGoal2_segment2", &(sparrowNextSegments2[1]));
  sh.rebind("nextGoal2_lane2", &(sparrowNextLanes2[1]));
  sh.rebind("nextGoal2_waypoint2", &(sparrowNextWaypoints2[1]));
  sh.set_string("nextGoal2_type", "UNKNOWN");
  sh.set_readonly("nextGoal2ID");
  sh.set_readonly("nextGoal2_segment1");
  sh.set_readonly("nextGoal2_lane1");
  sh.set_readonly("nextGoal2_waypoint1");
  sh.set_readonly("nextGoal2_segment2");
  sh.set_readonly("nextGoal2_lane2");
  sh.set_readonly("nextGoal2_waypoint2");
  sh.set_readonly("nextGoal2_type");

  sh.rebind("nextGoal3ID", &(sparrowNextGoalIDs[2]));
  sh.rebind("nextGoal3_segment1", &(sparrowNextSegments1[2]));
  sh.rebind("nextGoal3_lane1", &(sparrowNextLanes1[2]));
  sh.rebind("nextGoal3_waypoint1", &(sparrowNextWaypoints1[2]));
  sh.rebind("nextGoal3_segment2", &(sparrowNextSegments2[2]));
  sh.rebind("nextGoal3_lane2", &(sparrowNextLanes2[2]));
  sh.rebind("nextGoal3_waypoint2", &(sparrowNextWaypoints2[2]));
  sh.set_string("nextGoal3_type", "UNKNOWN");
  sh.set_readonly("nextGoal3ID");
  sh.set_readonly("nextGoal3_segment1");
  sh.set_readonly("nextGoal3_lane1");
  sh.set_readonly("nextGoal3_waypoint1");
  sh.set_readonly("nextGoal3_segment2");
  sh.set_readonly("nextGoal3_lane2");
  sh.set_readonly("nextGoal3_waypoint2");
  sh.set_readonly("nextGoal3_type");

  sh.rebind("nextGoal4ID", &(sparrowNextGoalIDs[3]));
  sh.rebind("nextGoal4_segment1", &(sparrowNextSegments1[3]));
  sh.rebind("nextGoal4_lane1", &(sparrowNextLanes1[3]));
  sh.rebind("nextGoal4_waypoint1", &(sparrowNextWaypoints1[3]));
  sh.rebind("nextGoal4_segment2", &(sparrowNextSegments2[3]));
  sh.rebind("nextGoal4_lane2", &(sparrowNextLanes2[3]));
  sh.rebind("nextGoal4_waypoint2", &(sparrowNextWaypoints2[3]));
  sh.set_string("nextGoal4_type", "UNKNOWN");
  sh.set_readonly("nextGoal4ID");
  sh.set_readonly("nextGoal4_segment1");
  sh.set_readonly("nextGoal4_lane1");
  sh.set_readonly("nextGoal4_waypoint1");
  sh.set_readonly("nextGoal4_segment2");
  sh.set_readonly("nextGoal4_lane2");
  sh.set_readonly("nextGoal4_waypoint2");
  sh.set_readonly("nextGoal4_type");

  sh.rebind("nextGoal5ID", &(sparrowNextGoalIDs[4]));
  sh.rebind("nextGoal5_segment1", &(sparrowNextSegments1[4]));
  sh.rebind("nextGoal5_lane1", &(sparrowNextLanes1[4]));
  sh.rebind("nextGoal5_waypoint1", &(sparrowNextWaypoints1[4]));
  sh.rebind("nextGoal5_segment2", &(sparrowNextSegments2[4]));
  sh.rebind("nextGoal5_lane2", &(sparrowNextLanes2[4]));
  sh.rebind("nextGoal5_waypoint2", &(sparrowNextWaypoints2[4]));
  sh.set_string("nextGoal5_type", "UNKNOWN");
  sh.set_readonly("nextGoal5ID");
  sh.set_readonly("nextGoal5_segment1");
  sh.set_readonly("nextGoal5_lane1");
  sh.set_readonly("nextGoal5_waypoint1");
  sh.set_readonly("nextGoal5_segment2");
  sh.set_readonly("nextGoal5_lane2");
  sh.set_readonly("nextGoal5_waypoint2");
  sh.set_readonly("nextGoal5_type");

  sh.rebind("nextGoal6ID", &(sparrowNextGoalIDs[5]));
  sh.rebind("nextGoal6_segment1", &(sparrowNextSegments1[5]));
  sh.rebind("nextGoal6_lane1", &(sparrowNextLanes1[5]));
  sh.rebind("nextGoal6_waypoint1", &(sparrowNextWaypoints1[5]));
  sh.rebind("nextGoal6_segment2", &(sparrowNextSegments2[5]));
  sh.rebind("nextGoal6_lane2", &(sparrowNextLanes2[5]));
  sh.rebind("nextGoal6_waypoint2", &(sparrowNextWaypoints2[5]));
  sh.set_string("nextGoal6_type", "UNKNOWN");
  sh.set_readonly("nextGoal6ID");
  sh.set_readonly("nextGoal6_segment1");
  sh.set_readonly("nextGoal6_lane1");
  sh.set_readonly("nextGoal6_waypoint1");
  sh.set_readonly("nextGoal6_segment2");
  sh.set_readonly("nextGoal6_lane2");
  sh.set_readonly("nextGoal6_waypoint2");
  sh.set_readonly("nextGoal6_type");

  sh.rebind("nextCheckpointIndex", &sparrowNextCheckpointID);
  sh.rebind("nextCheckpointSegmentID", &sparrowNextCheckpointSegmentID);
  sh.rebind("nextCheckpointLaneID", &sparrowNextCheckpointLaneID);
  sh.rebind("nextCheckpointWaypointID", &sparrowNextCheckpointWaypointID);
  sh.rebind("nextCheckpointGoalID", &sparrowNextCheckpointGoalID);
  sh.set_readonly("nextCheckpointIndex");
  sh.set_readonly("nextCheckpointSegmentID");
  sh.set_readonly("nextCheckpointLaneID");
  sh.set_readonly("nextCheckpointWaypointID");
  sh.set_readonly("nextCheckpointGoalID");
  

  sh.rebind("obstacleID", &obstacleID);
  sh.rebind("obstacleSegmentID", &obstacleSegmentID);
  sh.rebind("obstacleLaneID", &obstacleLaneID);
  sh.rebind("obstacleWaypointID", &obstacleWaypointID);

  sh.set_notify("sparrowInsertObstacle", this, &RoutePlanner::sparrowInsertObstacle);
  sh.run();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::UpdateSparrowVariablesLoop()
{
  while(true)
  {
    deque<SegGoals> executingGoals;
    DGClockMutex(&m_ExecGoalsMutex);	
    if (m_executingGoals.size() > 0)
    {
      executingGoals.assign(m_executingGoals.begin(), m_executingGoals.end());
      sparrowMinSpeed = executingGoals.front().minSpeedLimit;
      sparrowMaxSpeed = executingGoals.front().maxSpeedLimit;
      sparrowIllegalPassingAllowed = executingGoals.front().illegalPassingAllowed;
      sparrowStopAtExit = executingGoals.front().stopAtExit;
      sparrowIsExitCheckpoint = executingGoals.front().isExitCheckpoint;
    }
    DGCunlockMutex(&m_ExecGoalsMutex);	

    if (m_receivedSegGoalsStatus->status == SegGoalsStatus::ACCEPT)
    {
      shp->set_string("tplanner_status", "ACCEPT   ");
    }
    else if (m_receivedSegGoalsStatus->status == SegGoalsStatus::REJECT)
    {
      shp->set_string("tplanner_status", "REJECT   ");
    }
    else if (m_receivedSegGoalsStatus->status == SegGoalsStatus::COMPLETED)
    {
      shp->set_string("tplanner_status", "COMPLETED");
    }
    else if (m_receivedSegGoalsStatus->status == SegGoalsStatus::FAILED)
    {
      shp->set_string("tplanner_status", "FAILED   ");
    }
    else
    {
      shp->set_string("tplanner_status", "UNKNOWN  ");
    }

    DGClockMutex(&m_NextCheckpointIndexMutex);
    sparrowNextCheckpointID = m_nextCheckpointIndex + 1;
    sparrowNextCheckpointSegmentID =
        m_checkpointSequence[m_nextCheckpointIndex]->getSegmentID();         
    sparrowNextCheckpointLaneID =
        m_checkpointSequence[m_nextCheckpointIndex]->getLaneID(); 
    sparrowNextCheckpointWaypointID =
        m_checkpointSequence[m_nextCheckpointIndex]->getWaypointID(); 
    sparrowNextCheckpointGoalID = m_checkpointGoalID[m_nextCheckpointIndex];
    DGCunlockMutex(&m_NextCheckpointIndexMutex);

    int i = 0;
    deque<SegGoals> segGoalsSeq;	
    DGClockMutex(&m_SegGoalsSeqMutex);	
    if (m_segGoalsSeq.size() > 0)
    {
      segGoalsSeq.assign(m_segGoalsSeq.begin(), m_segGoalsSeq.end());
    }
    DGCunlockMutex(&m_SegGoalsSeqMutex);

    char* nextGoalType;

    while (i < NUM_SEGGOALS_DISPLAYED && executingGoals.size() > 0)
    {
      sparrowNextGoalIDs[i] = (executingGoals.front()).goalID;
      sparrowNextSegments1[i] = (executingGoals.front()).entrySegmentID;
      sparrowNextLanes1[i] = (executingGoals.front()).entryLaneID;
      sparrowNextWaypoints1[i] = (executingGoals.front()).entryWaypointID;
      sparrowNextSegments2[i] = (executingGoals.front()).exitSegmentID;
      sparrowNextLanes2[i] = (executingGoals.front()).exitLaneID;
      sparrowNextWaypoints2[i] = (executingGoals.front()).exitWaypointID;

      switch (i)
      {
        case 0:
          nextGoalType = "nextGoal1_type";
          break;
        case 1:
          nextGoalType = "nextGoal2_type";
          break;
        case 2:
          nextGoalType = "nextGoal3_type";
          break;
        case 3:
          nextGoalType = "nextGoal4_type";
          break;
        case 4:
          nextGoalType = "nextGoal5_type";
          break;
        default:
          nextGoalType = "nextGoal6_type";
      }

      if ((executingGoals.front()).segment_type == SegGoals::ROAD_SEGMENT)
      {
        shp->set_string(nextGoalType, "ROAD_SEGMENT");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::PARKING_ZONE)
      {
        shp->set_string(nextGoalType, "PARKING_ZONE");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::INTERSECTION)
      {
        shp->set_string(nextGoalType, "INTERSECTION");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::PREZONE)
      {
        shp->set_string(nextGoalType, "PREZONE     ");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::UTURN)
      {
        shp->set_string(nextGoalType, "UTURN       ");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::PAUSE)
      {
        shp->set_string(nextGoalType, "PAUSE       ");
      }
      else if ((executingGoals.front()).segment_type == SegGoals::END_OF_MISSION)
      {
        shp->set_string(nextGoalType, "END_OF_MISSION");
      }
      else
      {
        shp->set_string(nextGoalType, "UNKNOWN");
      }

      executingGoals.pop_front();
      i++;
    }
    
    while (i < NUM_SEGGOALS_DISPLAYED && segGoalsSeq.size() > 0)
    {
      sparrowNextGoalIDs[i] = (segGoalsSeq.front()).goalID;
      sparrowNextSegments1[i] = (segGoalsSeq.front()).entrySegmentID;
      sparrowNextLanes1[i] = (segGoalsSeq.front()).entryLaneID;
      sparrowNextWaypoints1[i] = (segGoalsSeq.front()).entryWaypointID;
      sparrowNextSegments2[i] = (segGoalsSeq.front()).exitSegmentID;
      sparrowNextLanes2[i] = (segGoalsSeq.front()).exitLaneID;
      sparrowNextWaypoints2[i] = (segGoalsSeq.front()).exitWaypointID;

      switch (i)
      {
        case 0:
          nextGoalType = "nextGoal1_type";
          break;
        case 1:
          nextGoalType = "nextGoal2_type";
          break;
        case 2:
          nextGoalType = "nextGoal3_type";
          break;
        case 3:
          nextGoalType = "nextGoal4_type";
          break;
        case 4:
          nextGoalType = "nextGoal5_type";
          break;
        default:
          nextGoalType = "nextGoal6_type";
      }

      if ((segGoalsSeq.front()).segment_type == SegGoals::ROAD_SEGMENT)
      {
        shp->set_string(nextGoalType, "ROAD_SEGMENT");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::PARKING_ZONE)
      {
        shp->set_string(nextGoalType, "PARKING_ZONE");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::INTERSECTION)
      {
        shp->set_string(nextGoalType, "INTERSECTION");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::PREZONE)
      {
        shp->set_string(nextGoalType, "PREZONE     ");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::UTURN)
      {
        shp->set_string(nextGoalType, "UTURN       ");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::PAUSE)
      {
        shp->set_string(nextGoalType, "PAUSE       ");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::END_OF_MISSION)
      {
        shp->set_string(nextGoalType, "END_OF_MISSION");
      }
      else
      {
        shp->set_string(nextGoalType, "UNKNOWN");
      }

      segGoalsSeq.pop_front();
      i++;
    }
    usleep(500000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sparrowInsertObstacle()
{  
  m_graphEstimator.insertObstacle(obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::getTPlannerStatusThread()
{
  // The skynet socket for receiving tplanner status
  int tplannerStatusSocket = m_skynet.listen(SNtplannerStatus, MODtrafficplanner);
  if(tplannerStatusSocket < 0)
  {
    cerr << "MissionPlanner::getTPlannerStatusThread(): skynet listen returned error"
         << endl;
    exit(1);
  }

  while(true)
  {
    bool tPlannerStatusReceived =
        RecvSegGoalsStatus(tplannerStatusSocket, m_receivedSegGoalsStatus);
        
    if (tPlannerStatusReceived)
    {
      if (m_nosparrow)
      {
        cout << "Goal " << m_receivedSegGoalsStatus->goalID << " status: "
             << m_receivedSegGoalsStatus->status << endl;
      }
      else
      {
        SparrowHawk().log("Goal %d status %d\n", m_receivedSegGoalsStatus->goalID,
            (int) m_receivedSegGoalsStatus->status);
      }

      if (m_receivedSegGoalsStatus->goalID == 0)
      {
        m_numSegGoalsRequests++;
      }
      else if (m_receivedSegGoalsStatus->status == SegGoalsStatus::COMPLETED)
      {
        // Removed the completed goal from m_executingGoals
        DGClockMutex(&m_ExecGoalsMutex);	
        if (m_executingGoals.size() > 0)
        {
          while (m_executingGoals.size() > 0 && 
              m_receivedSegGoalsStatus->goalID != (m_executingGoals.front()).goalID)
          {
            m_executingGoals.pop_front();
          }
          if (m_executingGoals.size() == 0)
          {
            cerr << "m_executingGoals.size() == 0" << endl;
          }
          else
          {
            m_executingGoals.pop_front();
          }
        }
        else  // if (m_receivedSegGoalsStatus->goalID != 0)
        {
          cerr << "Goal " << m_receivedSegGoalsStatus->goalID
               << " was not sent to traffic planner" << "." << endl;
        }
        DGCunlockMutex(&m_ExecGoalsMutex);

        // Determine if the next checkpoint is crossed.
        DGClockMutex(&m_NextCheckpointIndexMutex);
        if (m_receivedSegGoalsStatus->goalID == m_checkpointGoalID[m_nextCheckpointIndex])
        {
          if (m_nosparrow)
          {
            cout << endl << "checkpoint " << m_nextCheckpointIndex+1 << " (Waypoint " ;
            m_checkpointSequence[m_nextCheckpointIndex]->print();
            cout << ") is crossed" << endl << endl;
          }
          else
          {
            SparrowHawk().log("checkpoint %d (Waypoint %d.%d.%d) is crossed\n",
                m_nextCheckpointIndex + 1, 
                m_checkpointSequence[m_nextCheckpointIndex]->getSegmentID(), 
                m_checkpointSequence[m_nextCheckpointIndex]->getLaneID(), 
                m_checkpointSequence[m_nextCheckpointIndex]->getWaypointID());
          }
          
          if ((unsigned)m_nextCheckpointIndex + 1 < m_checkpointSequence.size())
          {
            m_nextCheckpointIndex++;
          }
          else
          {
            if (m_nosparrow)
            {
              cout << endl << "MISSION COMPLETED !!!" << endl << endl;
            }
            else
            {
              SparrowHawk().log("MISSION COMPLETED !!!\n");
            }
          }
        }
      }
      DGCunlockMutex(&m_NextCheckpointIndexMutex);
      
      DGClockMutex(&m_SegGoalsStatusMutex);
      m_segGoalsStatus.goalID = m_receivedSegGoalsStatus->goalID;
      m_segGoalsStatus.status = m_receivedSegGoalsStatus->status;
      m_segGoalsStatus.currentSegmentID = m_receivedSegGoalsStatus->currentSegmentID;
      m_segGoalsStatus.currentLaneID = m_receivedSegGoalsStatus->currentLaneID;
      m_segGoalsStatus.lastWaypointID = m_receivedSegGoalsStatus->lastWaypointID;
      // The following line causes the mission planner to be killed. Not sure why.
      //m_segGoalsStatus = *m_receivedSegGoalsStatus;
      DGCunlockMutex(&m_SegGoalsStatusMutex);

      if (!m_bReceivedAtLeastOneSegGoalsStatus)
      {
        DGCSetConditionTrue(m_bReceivedAtLeastOneSegGoalsStatus,
            m_FirstSegGoalsStatusReceivedCond, m_FirstSegGoalsStatusReceivedMutex);
      }
    }
    usleep(100000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sendSegGoalsThread()
{
  int numSegGoalsRequests = 0;
  if (m_nosparrow)
  {
    cout << "sendSegGoalsThread(): Waiting for traffic planner" << endl;
  }
  else
  {
    SparrowHawk().log("sendSegGoalsThread(): Waiting for traffic planner\n");
  }

  // don't send goals until traffic planner starts listening
  DGCWaitForConditionTrue(m_bReceivedAtLeastOneSegGoalsStatus,
      m_FirstSegGoalsStatusReceivedCond, m_FirstSegGoalsStatusReceivedMutex);

  if (m_nosparrow)
  {
    cout << "Starting sendSegGoalsThread" << endl;
  }
  else
  {
    SparrowHawk().log("Starting sendSegGoalsThread\n");
  }

  while(true)
  {   
    DGClockMutex(&m_SegGoalsStatusMutex);
    int lastGoalID = m_segGoalsStatus.goalID;
    SegGoalsStatus::Status lastGoalStatus = m_segGoalsStatus.status;
    DGCunlockMutex(&m_SegGoalsStatusMutex);
    
    DGClockMutex(&m_SegGoalsSeqMutex);
    DGClockMutex(&m_ExecGoalsMutex);

    // If the tplanner just restarts, resend the goals previously sent but not completed
    if (lastGoalID == 0 && numSegGoalsRequests < m_numSegGoalsRequests)
    {
      while (m_executingGoals.size() > 0)
      {
        m_segGoalsSeq.push_front(m_executingGoals.back());
        m_executingGoals.pop_back();
      }
      if (m_segGoalsSeq.size() > 0)
      {
        lastGoalID = (m_segGoalsSeq.front()).goalID - 1;
      }
      else
      {
        lastGoalID = 0;
      }
      numSegGoalsRequests++;
    }

    if (lastGoalStatus == SegGoalsStatus::COMPLETED && 
	m_segGoalsSeq.size() > 0 && 
	lastGoalID + LEAST_NUM_SEGGOALS_TPLANNER_STORED >= 
	(m_segGoalsSeq.front()).goalID)
    {
      if (m_nosparrow)
      {
        cout << "Sending goal " << (m_segGoalsSeq.front()).goalID << " to tplanner." << endl;
      }
      else
      {
        SparrowHawk().log("Sending goal %d to tplanner\n", (m_segGoalsSeq.front()).goalID);
      }

      m_executingGoals.push_back(m_segGoalsSeq.front());
      SendSegGoals(m_segGoalsSocket, &(m_segGoalsSeq.front()));
      m_segGoalsSeq.pop_front();
    }
    DGCunlockMutex(&m_ExecGoalsMutex);	
    DGCunlockMutex(&m_SegGoalsSeqMutex);
    usleep(100000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::MPlanningLoop(void)
{
  vector<Vertex*> route;
  bool replan;
  DGClockMutex(&m_NextCheckpointIndexMutex);
  int lastPlanCheckpointIndex =  m_nextCheckpointIndex;
  DGCunlockMutex(&m_NextCheckpointIndexMutex);

  SegGoalsStatus segGoalsStatus;

  while(true)
  {
    // plan from current position if we're not in run
    // plan from previous plan if we're in run and at least one plan came through

    // if we're not in RUN, plan from veh state
/*
    if(m_actuatorState.m_estoppos != RUN)
    {
      replan = true;
      if (m_nosparrow)
        cout << "I'm not in RUN" <<endl;
      else
        SparrowHawk().log("I'm not in RUN\n");
    }
    else
*/
    replan = false;

    DGClockMutex(&m_SegGoalsStatusMutex);
    segGoalsStatus = m_segGoalsStatus;

    // Update traffic planner status to COMPLETED. If the traffic planner cannot execute
    // the new goals, the getTPlannerStatusThread will set this to false.
    m_segGoalsStatus.status = SegGoalsStatus::COMPLETED;  
    DGCunlockMutex(&m_SegGoalsStatusMutex);

    if(m_bReceivedAtLeastOneSegGoalsStatus && 
       segGoalsStatus.status != SegGoalsStatus::COMPLETED &&
       segGoalsStatus.status != SegGoalsStatus::ACCEPT)
    {
      replan = true;
    }

    DGClockMutex(&m_SegGoalsSeqMutex);
    int numSegGoalsStored = m_segGoalsSeq.size();
    DGCunlockMutex(&m_SegGoalsSeqMutex);
    
    // Compute new goals starting from current position or from last waypoint
    // (given by the traffic planner) if failure occurs.    
    if (replan)
    {
      // First, get an updated graph
      m_graphEstimator.updateGraph(true); 
      m_travGraph = &m_graphEstimator.getGraph();
      
      if (m_nosparrow)
      {
        cout << endl << "Goal " << segGoalsStatus.goalID << " failed : Replanning" << endl;
      }
      else
      {
        SparrowHawk().log("Goal %d failed : Replanning\n", segGoalsStatus.goalID);
      }
      
      m_nextGoalID = segGoalsStatus.goalID;
      SegGoals failedGoal;
     
      resetGoals();
      DGClockMutex(&m_NextCheckpointIndexMutex);
      lastPlanCheckpointIndex =  m_nextCheckpointIndex;
      DGCunlockMutex(&m_NextCheckpointIndexMutex);

      int lastWaypointSegmentID = segGoalsStatus.currentSegmentID;
      int lastWaypointLaneID = segGoalsStatus.currentLaneID;
      int lastWaypointWaypointID = segGoalsStatus.lastWaypointID;

      Waypoint* nextCheckpoint = m_checkpointSequence[lastPlanCheckpointIndex];
      bool lastMission = (unsigned)lastPlanCheckpointIndex + 1 == m_checkpointSequence.size();
    
      // Plan from current position
      vector<SegGoals> segGoalsSeq = planSegGoals(lastWaypointSegmentID, lastWaypointLaneID,
						  lastWaypointWaypointID, nextCheckpoint->getSegmentID(),
						  nextCheckpoint->getLaneID(), nextCheckpoint->getWaypointID(),
						  lastPlanCheckpointIndex + 1, lastMission);

      DGClockMutex(&m_SegGoalsSeqMutex);
      int startPrintingIndex = (int)m_segGoalsSeq.size();
      addDirectives(segGoalsSeq);
    
      if (m_segGoalsSeq.size() > 0)
      {
        m_checkpointGoalID[lastPlanCheckpointIndex] = (m_segGoalsSeq.back()).goalID;
        m_nextGoalID = (m_segGoalsSeq.back()).goalID + 1;
      }
      else if (lastPlanCheckpointIndex > 1)
      {
        m_checkpointGoalID[lastPlanCheckpointIndex] =
            m_checkpointGoalID[lastPlanCheckpointIndex - 1];
      }
      else
      {
        m_checkpointGoalID[lastPlanCheckpointIndex] = 0;
      }
    
      // Print out the mission
      if (m_nosparrow)
      {
        printMission(m_segGoalsSeq, startPrintingIndex);
        cout<<endl;  
      }
      else
      {
        printMissionOnSparrow(m_segGoalsSeq, startPrintingIndex);
      }
    
      DGCunlockMutex(&m_SegGoalsSeqMutex);
    }

    // Update current goals and next goals when the traffic planner finish the current goals.
    else if (numSegGoalsStored < LEAST_NUM_SEGGOALS_STORED &&
        unsigned(lastPlanCheckpointIndex) + 1 < m_checkpointSequence.size())
    {
      Waypoint* currentCheckpoint = m_checkpointSequence[lastPlanCheckpointIndex];
      Waypoint* nextCheckpoint = m_checkpointSequence[lastPlanCheckpointIndex + 1];
      bool lastMission = (unsigned)lastPlanCheckpointIndex + 2 == m_checkpointSequence.size();
      vector<SegGoals> segGoalsSeq = planSegGoals(currentCheckpoint->getSegmentID(),
						  currentCheckpoint->getLaneID(),
						  currentCheckpoint->getWaypointID(),
						  nextCheckpoint->getSegmentID(),
						  nextCheckpoint->getLaneID(),
						  nextCheckpoint->getWaypointID(),
						  lastPlanCheckpointIndex + 2, lastMission);


      DGClockMutex(&m_SegGoalsSeqMutex);
      int startPrintingIndex = (int)m_segGoalsSeq.size();      
      addDirectives(segGoalsSeq);

      if (m_segGoalsSeq.size() > 0)
      {
      	m_checkpointGoalID[lastPlanCheckpointIndex+1] = (m_segGoalsSeq.back()).goalID;
      	m_nextGoalID = (m_segGoalsSeq.back()).goalID + 1;
      }
      else
      {
      	m_checkpointGoalID[lastPlanCheckpointIndex+1] =
            m_checkpointGoalID[lastPlanCheckpointIndex];
      }
      
      // Print out the mission
      if (m_nosparrow)
      {
        printMission(m_segGoalsSeq, startPrintingIndex);
        cout << endl;  
      }
      else
      {
      	printMissionOnSparrow(m_segGoalsSeq, startPrintingIndex);
      }

      DGCunlockMutex(&m_SegGoalsSeqMutex);
      lastPlanCheckpointIndex++;
    }
    usleep(100000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::requestGloNavMap()
{
  m_graphEstimator.updateGraph(true);
  m_travGraph = &m_graphEstimator.getGraph();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::initializeSegGoals()
{
  m_graphEstimator.updateGraph(true); 
  m_travGraph = &m_graphEstimator.getGraph();


  Waypoint* nextCheckpoint = m_checkpointSequence[m_nextCheckpointIndex];
  bool lastMission = (unsigned)m_nextCheckpointIndex + 1 == m_checkpointSequence.size();
  
  // Plan from current position
  vector<SegGoals> currentMission = planSegGoals(0, 0, 0, nextCheckpoint->getSegmentID(),
					      nextCheckpoint->getLaneID(), nextCheckpoint->getWaypointID(),
					      m_nextCheckpointIndex + 1, lastMission);
  DGClockMutex(&m_SegGoalsSeqMutex);
  for(unsigned i = 0; i < currentMission.size(); i++)
  {
    m_segGoalsSeq.push_back(currentMission[i]);
  }

  DGClockMutex(&m_NextCheckpointIndexMutex);
  m_checkpointGoalID[m_nextCheckpointIndex] = (m_segGoalsSeq.back()).goalID;
  m_nextGoalID = (m_segGoalsSeq.back()).goalID + 1;
  DGCunlockMutex(&m_NextCheckpointIndexMutex);

  // Print out the mission
  if (m_nosparrow)
  {
    printMission(m_segGoalsSeq, 0);
    cout<<endl;  
  }
  else
  {
    printMissionOnSparrow(m_segGoalsSeq, 0);
  }
  DGCunlockMutex(&m_SegGoalsSeqMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> RoutePlanner::planSegGoals(int segmentID1, int laneID1,  
    int waypointID1, int segmentID2, int laneID2, int waypointID2, int missionNumber,
    bool lastMission) 
{
  double cost; 
  vector<SegGoals> segGoalsSeq; 
  vector<Vertex*> route; 

  Vertex* vertex1 = m_travGraph->getVertex(segmentID1, laneID1, waypointID1); 
  if (vertex1 == NULL) 
  {
    m_graphEstimator.addVertex(segmentID1, laneID1, waypointID1); 
    vertex1 = m_travGraph->getVertex(segmentID1, laneID1, waypointID1);
    if (vertex1 == NULL)
    {
      if (m_nosparrow)
      {
	cerr << "ERROR: planSegGoals: Cannot find waypoint " << segmentID1
	     << "." << laneID1 << "." << waypointID1 << " in graph" << endl;
      }
      else
      {
	SparrowHawk().log("ERROR: planSegGoals: Cannot find waypoint");
	SparrowHawk().log("%d.%d.%d in graph!\n", segmentID1, laneID1, waypointID1);
      }
    }
  }

  Vertex* vertex2 = m_travGraph->getVertex(segmentID2, laneID2, waypointID2); 
  if (vertex2 == NULL)
  {
    m_graphEstimator.addVertex(segmentID2, laneID2, waypointID2); 
    vertex2 = m_travGraph->getVertex(segmentID2, laneID2, waypointID2);
    if (vertex2 == NULL)
    {
      if (m_nosparrow)
      {
	cerr << "ERROR: planSegGoals: Cannot find waypoint " << segmentID2
	     << "." << laneID2 << "." << waypointID2 << " in graph" << endl;
      }
      else
      {
	SparrowHawk().log("ERROR: planSegGoals: Cannot find waypoint");
	SparrowHawk().log("%d.%d.%d in graph!\n", segmentID2, laneID2, waypointID2);
      }
    }
  }

  bool routeFound = findRoute(vertex1, vertex2, m_travGraph, route, cost);
  if (!routeFound)
  {
    // Add uturn to segGoalsSeq
    segGoalsSeq = addUturn(vertex1, vertex2);


    if (segGoalsSeq.size() == 0)
    {
      if (m_graphEstimator.addRemovedEdge())
      {
        if (m_nosparrow)
        {
          cout << "Cannot find route after allowing u-turn" << endl;
	}
	else
	{
          SparrowHawk().log("Cannot find route after allowing u-turn");
	}
        segGoalsSeq = planSegGoals(segmentID1, laneID1, waypointID1, 
				   segmentID2, laneID2, waypointID2, 
				   missionNumber, lastMission);
      }
      else
      {
        cerr << "Cannot find route after adding all the removed edges back to the graph" 
             << endl;
	cout << "Here is the graph" << endl;
	m_travGraph->print();
	#warning routeplanner gives up when route cannot be found
        exit(1);
      }
    }
  }
  else 
  { 
    segGoalsSeq = findSegGoals(route, m_travGraph, m_minSpeedLimits, m_maxSpeedLimits, m_nextGoalID); 
  } 

  if (segGoalsSeq.size() > 0) 
  { 
    // The exit point of the last segment goal is a checkpoint 
    segGoalsSeq[segGoalsSeq.size()-1].isExitCheckpoint = true; 
  } 

  // If this is the last checkpoint, add END_OF_MISSION goal.
  if (lastMission) 
  { 
    addEndOfMission(segGoalsSeq); 
  } 


  if (m_nosparrow && route.size() > 1)
  {
    cout << endl << "Mission " << missionNumber;
    if (lastMission)
    {
      cout << " (last mission)";
    }
    cout << ": from waypoint "<<  vertex1->getSegmentID() << "."
         << vertex1->getLaneID() << "." << vertex1->getWaypointID();
    cout << " to waypoint " << vertex2->getSegmentID() << "."
         << vertex2->getLaneID() << "."
         << vertex2->getWaypointID() << endl;
  }
  else if (route.size() > 1)
  {
    SparrowHawk().log("Mission %d: from waypoint %d.%d.%d to waypoint %d.%d.%d",
        missionNumber, vertex1->getSegmentID(),
        vertex1->getLaneID(), vertex1->getWaypointID(),
        vertex2->getSegmentID(), vertex2->getLaneID(),
        vertex2->getWaypointID());
  }


  return segGoalsSeq;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> RoutePlanner::addUturn(Vertex* vertex1, Vertex* vertex2)
{
  if (m_nosparrow)
  {
    cout << "no route from ";
    vertex1->print();
    cout << " to ";
    vertex2->print();
    cout << " without making u-turn" << endl;
  }
  else
  {
    SparrowHawk().log("no route from %d.%d.%d to %d.%d.%d without making u-turn\n",
        vertex1->getSegmentID(), vertex1->getLaneID(), vertex1->getWaypointID(),
        vertex2->getSegmentID(), vertex2->getLaneID(), vertex2->getWaypointID());
  }

  if (vertex1->getSegmentID() > m_travGraph->getNumOfSegments())
  {
    cerr << "Cannot make uturn in a zone" << endl;
    cout << "Here is the graph" << endl;
    m_travGraph->print();
    exit(1);
  }

  vector<Vertex*> route;
  vector<SegGoals> segGoalsSeq;
  double cost;
  bool routeFound = false;

  m_graphEstimator.addUturnEdges(vertex1);
  
  routeFound = findRoute(vertex1, vertex2, m_travGraph, route, cost);
  if (!routeFound)
  {
    return segGoalsSeq;
  }
  
  segGoalsSeq = findSegGoals(route, m_travGraph, m_minSpeedLimits, m_maxSpeedLimits, m_nextGoalID);

  return segGoalsSeq;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::addEndOfMission(vector<SegGoals>& segGoalsSeq)
{
  SegGoals lastSeg;
  lastSeg.goalID = segGoalsSeq[segGoalsSeq.size()-1].goalID + 1;;
  lastSeg.entrySegmentID = 0;
  lastSeg.entryLaneID = 0;
  lastSeg.entryWaypointID = 0;
  lastSeg.exitSegmentID = 0;
  lastSeg.exitLaneID = 0;
  lastSeg.exitWaypointID = 0;
  lastSeg.minSpeedLimit = 0;
  lastSeg.maxSpeedLimit = 0;
  lastSeg.segment_type = SegGoals::END_OF_MISSION;
  lastSeg.illegalPassingAllowed = false;
  lastSeg.stopAtExit = true;
  lastSeg.isExitCheckpoint = false;
  segGoalsSeq.push_back(lastSeg);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::resetGoals()
{
  DGClockMutex(&m_SegGoalsSeqMutex);
  m_segGoalsSeq.clear();
  DGCunlockMutex(&m_SegGoalsSeqMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool RoutePlanner::loadMDFFile(char* fileName, RNDF* rndf)
{
  ifstream file;
  string line;
  string word;
  
  file.open(fileName, ios::in);

  if(!file)
  {
    cerr << "Error: " << fileName << " file not found." << endl;
    return false;
  } 
  
  getline(file, line);
  istringstream lineStream(line, ios::in);
  lineStream >> word;

  if(word == "MDF_name")
  {
    if (DEBUG_LEVEL > 1)
    {
      cout << "Parsing checkpoints" << endl;
    }
    parseCheckpoint(&file, rndf);
    if (DEBUG_LEVEL > 1)
    {
      cout << "Parsing speed limits" << endl;
    }
    parseSpeedLimit(&file, rndf);
    file.close();
    if (DEBUG_LEVEL > 1)
    {
      cout << "Finish loading MDF file" << endl;
    }
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::parseCheckpoint(ifstream* file, RNDF* rndf)
{
  string line, word;
  char letter;
  int ckptID;  
  
  while(word != "end_checkpoints")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      ckptID = atoi(word.c_str());
      m_checkpointSequence.push_back(rndf->getCheckpoint(ckptID));
    }
    else
    {
      continue;
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::parseSpeedLimit(ifstream* file, RNDF* rndf)
{
  int segmentID;
  double minSpeed, maxSpeed;
  string line, word;
  char letter;

  while(word != "speed_limits")
  {    
    getline(*file, line);
    istringstream lineStream(line, ios::in);
    lineStream >> word;
  }

  
  while(word != "end_speed_limits")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    if(letter >= '0' && letter <= '9')
    {
      lineStream >> segmentID;
      lineStream >> minSpeed;
      lineStream >> maxSpeed;

      minSpeed = minSpeed * MPS_PER_MPH;
      maxSpeed = maxSpeed * MPS_PER_MPH;
      
      // setSpeedLimits(segmentID, minSpeed, maxSpeed, rndf);
      if ((unsigned)segmentID <= m_minSpeedLimits.size())
      {
	m_minSpeedLimits[segmentID] = minSpeed;
	m_maxSpeedLimits[segmentID] = maxSpeed;
      }
      else
      {
	cerr << "ERROR: parseSpeedLimit: got speed limit for segment " << segmentID
	     << " while number of segments and zones = " << m_minSpeedLimits.size() << endl;
	int numOfSegments = m_minSpeedLimits.size();
	m_minSpeedLimits.resize(segmentID);
	m_maxSpeedLimits.resize(segmentID);
	for (unsigned i = numOfSegments; i <= m_minSpeedLimits.size(); i++)
	{
	  m_minSpeedLimits[i] = 0;
	  m_maxSpeedLimits[i] = 0;
	}
	m_minSpeedLimits[segmentID] = minSpeed;
	m_maxSpeedLimits[segmentID] = maxSpeed;
      }
    }
    else
    {
      lineStream >> word;
      continue;      
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::setSpeedLimits(int segOrZoneID, double minSpeed, double maxSpeed,
    RNDF* rndf)
{
  if(segOrZoneID <= rndf->getNumOfSegments() + rndf->getNumOfZones() &&
      segOrZoneID > rndf->getNumOfSegments())
  {
    Zone* zone = rndf->getZone(segOrZoneID);
    zone->setSpeedLimits(minSpeed, maxSpeed);

    if (DEBUG_LEVEL > 2 && m_nosparrow)
    {
      cout << "The minimum speed of zone " << segOrZoneID << " is " << minSpeed
           << " mph, the maximum speed is " << maxSpeed << " mph." << endl;
    }
  }
  else
  {
    Segment* segment = rndf->getSegment(segOrZoneID);
    segment->setSpeedLimits(minSpeed, maxSpeed);
    
    
    if (DEBUG_LEVEL > 2 && m_nosparrow)
    {
      cout << "The minimum speed of segment " << segOrZoneID << " is " << minSpeed
           << " mph, the maximum speed is " << maxSpeed << " mph." << endl;
    }
  }  
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::printMissionOnSparrow(deque<SegGoals> segGoals, int startIndex)
{
  int i = 0;
  while (segGoals.size() > 0 && i < startIndex)
  {
    segGoals.pop_front();
    i++;
  }

  int goalID;
  int entrySegmentID, entryLaneID, entryWaypointID;
  int exitSegmentID, exitLaneID, exitWaypointID;
  double minSpeedLimit, maxSpeedLimit;
  bool illegalPassingAllowed, stopAtExit, isExitCheckpoint;
  SegGoals::SegmentType segment_type;
  
  while (segGoals.size() > 0)
  {
    SegGoals currentGoal = segGoals.front();
    goalID = currentGoal.goalID;
    entrySegmentID = currentGoal.entrySegmentID;
    entryLaneID = currentGoal.entryLaneID;
    entryWaypointID = currentGoal.entryWaypointID;
    exitSegmentID = currentGoal.exitSegmentID;
    exitLaneID = currentGoal.exitLaneID;
    exitWaypointID = currentGoal.exitWaypointID;
    minSpeedLimit = currentGoal.minSpeedLimit;
    maxSpeedLimit = currentGoal.maxSpeedLimit;
    illegalPassingAllowed = currentGoal.illegalPassingAllowed;
    stopAtExit = currentGoal.stopAtExit;
    isExitCheckpoint = currentGoal.isExitCheckpoint;
    segment_type = currentGoal.segment_type;
    SparrowHawk().log("GOAL %d: %d.%d.%d -> %d.%d.%d",  goalID, entrySegmentID, entryLaneID,
        entryWaypointID, exitSegmentID, exitLaneID, exitWaypointID);
    switch(segment_type)
    {
      case SegGoals::ROAD_SEGMENT:
        SparrowHawk().log("  ROAD_SEGMENT");
        break;
      case SegGoals::PARKING_ZONE:
        SparrowHawk().log("  PARKING_ZONE");
        break;
      case SegGoals::INTERSECTION:
        SparrowHawk().log("  INTERSECTION");
        break;
      case SegGoals::PREZONE:
        SparrowHawk().log("  PREZONE");
        break;
      case SegGoals::UTURN:
        SparrowHawk().log("  UTURN");
        break;
      case SegGoals::PAUSE:
        SparrowHawk().log("  PAUSE");
        break;
      case SegGoals::END_OF_MISSION:
        SparrowHawk().log("  END_OF_MISSION");
        break;
      default:
        SparrowHawk().log("  UNKNOWN");
    }
    SparrowHawk().log("  Min Speed: %4.1f, Max Speed: %4.1f", minSpeedLimit, maxSpeedLimit);
    if (illegalPassingAllowed)
    {
      SparrowHawk().log("  Illegal passing allowed");
    }
    segGoals.pop_front();
  }	
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::addDirectives(vector<SegGoals> segGoalsSeq)
{
  for(unsigned i = 0; i < segGoalsSeq.size(); i++)
  {
    m_segGoalsSeq.push_back(segGoalsSeq[i]);
  }  
}
