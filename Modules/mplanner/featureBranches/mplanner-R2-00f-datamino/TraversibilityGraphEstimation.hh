/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */


#ifndef TRAVGRAPHEST_HH
#define TRAVGRAPHEST_HH

#include <unistd.h>
#include <iostream>
#include <vector>
#include <list>
#include "rndf/RNDF.hh"
#include "travgraph/Graph.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"
#include "Obstacle.hh"

/**
 * TraversibilityGraphEstimation class
 * This is a class that communicates with the mapper. It requests
 * the updated traversibility graph and updates this graph according
 * to the current vehicle capability. The route planner part
 * of the mission planner can access this graph through a function call.
 * \brief class that communicates with the mapper.
 */
class TraversibilityGraphEstimation : public CStateClient
{ 
private:
  int DEBUG_LEVEL, VERBOSITY_LEVEL;

  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param whether we want to listen to mapper */
  bool m_listenToMapper;

  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;

  /*!\param m_rndf is a pointer to the RNDF object. */
  RNDF* m_rndf;

  /*!\param m_graph is the corresponding graph of the current RNDF
   * that route planner uses to compute segment goals. */
  Graph* m_graph;

  /*!\param m_receivedGraph is the local copy of Graph that gets
   * updated when any is received from the networkthat  */
  Graph* m_receivedGraph;

  /*!\param m_obstacles is the list of all the obstacles. */
  vector<Obstacle> m_obstacles;

  /*!\param m_edgesRemoved is the list of all the edges that got removed
   * because manually inserted obstacles block the road */
  list<Edge*> m_edgesRemoved;

  /*!\param m_obstacles is the list of all the obstacles on a segment. */
  vector<Obstacle> m_obstaclesOnSegment;

  /*!\param The mutex to protect the list of obstacles */
  pthread_mutex_t m_ObstaclesMutex;

  /*! \param m_conNewGraph tells whether a new graph is received. */
  pthread_mutex_t m_condNewGraphMutex;
  DGCcondition m_condNewGraph;

  /*! \param The mutex for m_receivedGraph. */
  pthread_mutex_t m_receivedGraphMutex;

  /*! The thread which continually updates the local copy of Graph when any is received from
    the network */
  void getGraphThread();

  /*! The function that adds a vertex that corresponds to our current position to graph */
  bool addCurrentPositionToGraph();

  /*! Get the index of obstacle in the vector */
  int getObstacleIndex(int obstacleID);
  
  /*! Update the cost of all the egdegs based on all the obstacles that I know of */
  void updateObstructedEdges();


public:
  /*! Constructor */
  TraversibilityGraphEstimation(int skynetKey, bool waitForStateFill);

  /*! Standard destructor */
  ~TraversibilityGraphEstimation();


  /*! A function to initialize this object */
  void init(RNDF* rndf, bool listenToMapper, bool nosparrow, 
	    int debugLevel, bool verbose);

  /*! Check for new Graph */
  bool newGraph();
  
  /*! Give me the latest Graph (a copy) and reset new flag */
  void updateGraph(bool requestMapper);

  /*! use conditional mutexes to wait until Graph is received */
  void waitForGraph();

  /*! Get the graph */
  Graph& getGraph();
  /*! Get the graph */
  const Graph& getGraph() const;

  /*! Add a vertex and all the edges from this vertex.*/
  bool addVertex(int, int, int);

  /*! Add edges from the specified vertex corresponding to uturn */
  bool addUturnEdges(Vertex*);

  /*! Add the first removed edge back to the graph */
  bool addRemovedEdge();
  
  /*! Get all obstacles on the specified segment */
  vector<Obstacle>& getAllObstaclesOnSegment(int segmentID);

  /*! Insert an obstacle into the map*/
  bool insertObstacle(int obstacleID, int obstacleSegmentID, int obstacleLaneID, int obstacleWaypointID);

  /*! Get the current position */
  double getNorthing();
  double getEasting();
};

#endif  //TRAVGRAPHEST_HH
