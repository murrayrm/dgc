/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */


#ifndef ROUTEPLANNER_HH
#define ROUTEPLANNER_HH

#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <deque>
#include <math.h>
//#include "alice/estop.h"
#include "dgcutils/DGCutils.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include "rndf/RNDF.hh"
#include "travgraph/Graph.hh"
#include "skynettalker/SegGoalsTalker.hh"
#include "dgcutils/GlobalConstants.h"
#include "Obstacle.hh"
#include "MissionUtils.hh"
#include "MissionControl.hh"
#include "TraversibilityGraphEstimation.hh"

#define LEAST_NUM_SEGGOALS_STORED 8
#define LEAST_NUM_SEGGOALS_TPLANNER_STORED 3
#define NUM_SEGGOALS_DISPLAYED 6


/**
 * RoutePlanner class.
 * This is a main class for the mission planner where information from 
 * gloNavMap (mapping) and MDF file is used to compute segment goals
 * and send to the traffic planner.
 * \brief Main class for mission planner function
 */
class RoutePlanner : public CSegGoalsTalker
{
  /*! The structure for pairing up the directives sent to the
   *  traffic planner (but in the language of the control) and the status
   *  of this directive. */
  struct ContrMsgWrapper
  {
    enum Status{ QUEUED, SENT, ACCEPTED, COMPLETED, FAILED };
    ContrMsgWrapper()
    {
      // initialize the segment goals to zeros to appease the memory profilers
      memset(this, 0, sizeof(*this));
      this->status = QUEUED;
    }
    int goalID;
    SegGoals directive;
    Status status;
    SegGoalsStatus::ReasonForFailure reason;
  };

  /*! The structure for merged directive that is "sent" from arbiter to control. */
  struct MergedDirective
  {
    MergedDirective()
    {
      // initialize the segment goals to zeros to appease the memory profilers
      memset(this, 0, sizeof(*this));
    }
    int goalID;
    DirectiveName name;
    // The waypoint id of the next checkpoint
    int segmentID;
    int laneID;
    int waypointID;
    VehicleCap vehicleCap;
  };

  /*! The structure for status of merged directive. This status is "sent"
   *  from control back to arbiter. */
  struct ControlStatus
  {
    enum Status{ COMPLETED, FAILED, READY_FOR_NEXT };
    enum ReasonForFailure{ NO_ROUTE, VEHICLE_CAP };
    ControlStatus()
    {
      // initialize the segment goals to zeros to appease the memory profilers
      memset(this, 0, sizeof(*this));
    }
    int goalID;
    Status status;
    ReasonForFailure reason;    
  };

  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;

  
  int DEBUG_LEVEL, VERBOSITY_LEVEL;

  /*!\param m_rndf is a pointer to the RNDF object. */
  RNDF* m_rndf;

  /*!\param m_graphEstimator is the TraversibilityGraphEstimation object. */
  TraversibilityGraphEstimation m_graphEstimator;

  /*!\param m_travGraph is the corresponding graph of the current RNDF
   * that mission planner uses to compute segment goals. */
  Graph* m_travGraph;

  /*!\param m_minSpeedLimits is the vector of min speed limits in m/s*/
  vector<double> m_minSpeedLimits;

  /*!\param m_maxSpeedLimits is the vector of max speed limits in m/s*/
  vector<double> m_maxSpeedLimits;

  /*!\param m_missionControl is the Mission Control object which keeps
   * track of where we are in the mission. */
  MissionControl m_missionControl;

  /*!\param m_checkpointSequence is the vector of waypoints which are
   * the checkpoints that we have to cross in order as specified by MDF */
  vector<Waypoint*> m_checkpointSequence;
  
  /*!\param m_nextCheckpointIndex is the index of the next checkpoint
   * in m_checkpointSequence */
  int m_nextCheckpointIndex;

  /*!\param m_checkpointGoalID is an array of goalID's which correspond
   * to the goals for crossing checkpoints. m_checkpointGoalID[i] is the 
   * goalID for crossing m_checkpointSequence[i] */
  int* m_checkpointGoalID;       

  /*!\param m_segGoalsSeq */
  deque<SegGoals> m_segGoalsSeq;

  /*!\param m_executingGoals is a list of goals that are currently stored in
   * the traffic planner. This is basically the goals mission planner
   * sent to the traffic planner that have not yet completed. The first
   * element of this list is the goal that mission planner believes the
   * traffic planner is executing. */
  deque<SegGoals> m_executingGoals;

  /*!\param m_nextGoalID is the ID of the next goal to be added 
   * to m_segGoalsSeq. */
  int m_nextGoalID; 

  /*!\param m_segGoalsStatus is the status of the traffic planner which is
   * used in the planning loop to determine if we need to replan. */
  SegGoalsStatus m_segGoalsStatus;

  /*!\param m_receivedSegGoalsStatus is the actual status of the traffic
   * planner. */
  SegGoalsStatus* m_receivedSegGoalsStatus;

  /*!\param m_numSegGoalsRequests is the numer of requests for segment goals
   * received from the traffic planner. */
  int m_numSegGoalsRequests;

  /*!\param Varibales which keep track of whether traffic planner 
   * starts listening. */
  bool m_bReceivedAtLeastOneSegGoalsStatus;
  pthread_mutex_t m_FirstSegGoalsStatusReceivedMutex;
  pthread_cond_t m_FirstSegGoalsStatusReceivedCond;

  /*!\param The mutex to protect the segGoalsSeq */
  pthread_mutex_t m_SegGoalsSeqMutex;

  /*!\param The mutex to protect the status of segGoals */
  pthread_mutex_t m_SegGoalsStatusMutex;

  /*!\param The mutex to protect the index of the next checkpoint */
  pthread_mutex_t m_NextCheckpointIndexMutex;

  /*!\param The mutex to protect the list of executing goals */
  pthread_mutex_t m_ExecGoalsMutex;

  /*!\param The skynet socket for sending segment-level goals to tplanner */
  int m_segGoalsSocket;
  
  /*!\param SparrowHawk display */
  CSparrowHawk *shp;

  /*!\param Sparrow variables */
  int sparrowNextGoalIDs[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints2[NUM_SEGGOALS_DISPLAYED];
  double sparrowMinSpeed, sparrowMaxSpeed;
  bool sparrowIllegalPassingAllowed, sparrowStopAtExit, sparrowIsExitCheckpoint;
  int sparrowNextCheckpointID, sparrowNextCheckpointGoalID;
  int sparrowNextCheckpointSegmentID, sparrowNextCheckpointLaneID;
  int sparrowNextCheckpointWaypointID;

  /*!\param Obstacle inserted by sparrow */
  int obstacleID;
  int obstacleSegmentID;
  int obstacleLaneID;
  int obstacleWaypointID;


public:
  /*! Constructor */
  RoutePlanner(int skynetKey, bool bWaitForStateFill, bool nosparrow, char* RNDFFileName,
      char* MDFFileName, bool usingGloNavMapLib, int debugLevel, bool verbose);
      
  /*! Standard destructor */
  ~RoutePlanner();

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface. */
  void UpdateSparrowVariablesLoop();

  /*! This is the function that continually runs the planner in a loop */
  void MPlanningLoop(void);

  /*! This is the function that continually reads tplanner status and update the
   *  executing goalID. */
  void getTPlannerStatusThread();

  /*! This is the function that continually transmit segment goals to tplanner and
   *  update the executing goalID. */
  void sendSegGoalsThread();

private:
  /*! Arbitration for the route planner module. ControlStatus is the input and
   *  MergedDirective is the output of this function. */
  void arbitrate(ControlStatus*, MergedDirective*);

  /*! Control for the route planner module. ControlStatus is the output and
   *  MergedDirective is the input of this function. */
  void control(ControlStatus*, MergedDirective*);

  /*! Add the manually inserted obstacle to the map */
  void sparrowInsertObstacle();

  /*! Request map */
  void requestGloNavMap();
  void initializeSegGoals();
  vector<SegGoals> planSegGoals(int, int, int, int, int, int, int, bool);
  //vector<SegGoals> planFromCurrentPosition(int, int, int, int);
  //vector<SegGoals> planNextMission(int);
  vector<SegGoals> addUturn(Vertex*, Vertex*);
  void addEndOfMission(vector<SegGoals>&);
  void resetGoals();
  bool loadMDFFile(char*, RNDF*);
  void parseCheckpoint(ifstream*, RNDF*);
  void parseSpeedLimit(ifstream*, RNDF*);
  void setSpeedLimits(int, double, double, RNDF*);
  void printMissionOnSparrow(deque<SegGoals> segGoals, int startIndex);
  void addDirectives(vector<SegGoals> segGoalsSeq);
};

#endif  // ROUTEPLANNER_HH
