/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */
 
#include <iostream>
#include <assert.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <signal.h>
#include "dgcutils/DGCutils.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include "gcinterfaces/SegGoals.hh"
#include "gcinterfaces/SegGoalsStatus.hh"
#include "skynet/skynet.hh"
#include "gcmodule/GcModule.hh"
#include "gcmodule/GcInterface.hh"
#include "RoutePlanner.hh"
#include "CmdArgs.hh"

using namespace std;

extern volatile sig_atomic_t quit;
void sigintHandler(int /*sig*/)
{
  // if the user presses CTRL-C three or more times, and the program still
  // doesn't terminate, abort
  if (quit > 1) {
    abort();
  }
  quit++;
}

class CTrafficPlanner : public GcModule
{
  
  ControlStatus m_controlStatus;
  MergedDirective m_mergedDirective;
  RTInterface::Northface* rtInterfaceNF;
  bool isInit;
  int lastFailedGoalID;
  bool lastFailedGoalResolved;
  int numPassed;
  int numChecks;

  //SkynetTalker<SegGoalsStatus> statusTalker;
  //SkynetTalker<SegGoals> segGoalsTalker;

public:
  /*! Contstructor */
  CTrafficPlanner(int skynetKey) : 
    GcModule( "TrafficPlanner", &m_controlStatus, &m_mergedDirective, 100000, 100000 )
  {
    rtInterfaceNF = RTInterface::generateNorthface(skynetKey, this);
    isInit = false;
    lastFailedGoalID = 0;
    lastFailedGoalResolved = true;
    numPassed = 0;
    numChecks = 10;
  }

  void arbitrate(ControlStatus* cs, MergedDirective* md) 
  {
    if (!isInit) {
      SegGoalsStatus segGoalsStatus;
      segGoalsStatus.goalID = 0;
      segGoalsStatus.status = SegGoalsStatus::COMPLETED;

      rtInterfaceNF->sendResponse(&segGoalsStatus);
      sleep(2);
      
      while (!rtInterfaceNF->haveNewDirective()) {    
	rtInterfaceNF->sendResponse(&segGoalsStatus);
	sleep(2);
      }

      isInit = true;
    }

    if (rtInterfaceNF->haveNewDirective())
    {
      SegGoals segGoals;
      SegGoalsStatus segGoalsStatus;
      rtInterfaceNF->getNewDirective(&segGoals);
      //segGoals.print();
      //cout << endl;

      //assert(!segGoals.illegalPassingAllowed);
      numPassed++;

      if (segGoals.goalID == 1) {
	assert(segGoals.segment_type == SegGoals::RESET);
	numPassed++;
      }
      if (segGoals.goalID == 2) {
	assert(segGoals.exitSegmentID == 1 && segGoals.exitLaneID == 1 &&
	       segGoals.exitWaypointID == 2 && 
	       segGoals.segment_type == SegGoals::PARKING_ZONE);
	numPassed++;
      }

      segGoalsStatus.goalID = segGoals.goalID;

      if (!lastFailedGoalResolved && lastFailedGoalID == segGoals.goalID)
      {
	lastFailedGoalResolved = true;
	assert(segGoals.segment_type == SegGoals::UTURN);
	assert(segGoals.entrySegmentID == 0 &&
	       segGoals.entryLaneID == 0 &&
	       segGoals.entryWaypointID == 0 &&
	       segGoals.exitSegmentID == 1 &&
	       segGoals.exitLaneID == 2 &&
	       segGoals.exitWaypointID == 8);
	numPassed++;
      }

      if (segGoals.entrySegmentID == 1 && segGoals.entryLaneID == 1 && 
	  segGoals.entryWaypointID == 2 && segGoals.goalID != lastFailedGoalID)
      {
	segGoalsStatus.status = SegGoalsStatus::FAILED;	
	segGoalsStatus.currentSegmentID = 0;
	segGoalsStatus.currentLaneID = 0;
	segGoalsStatus.lastWaypointID = 0;
	lastFailedGoalID = segGoals.goalID;
	lastFailedGoalResolved = false;
      }
      else
      {
	segGoalsStatus.status = SegGoalsStatus::COMPLETED;
      }
	
      if (lastFailedGoalResolved || segGoalsStatus.status == SegGoalsStatus::FAILED)
      {
	sleep(5);
	cout << "Sending response:  goal " << segGoalsStatus.goalID << " status: " << segGoalsStatus.status << endl;
	rtInterfaceNF->sendResponse(&segGoalsStatus);
      }

      if (segGoals.segment_type == SegGoals::END_OF_MISSION) {
	sleep(2);
	if (numPassed == numChecks)
	  cout << "Test completed successfully" << endl;
	else {
	  cout << "There is something wrong." << endl;
	  cout << "Only pass " << numPassed
	       << " out of " << numChecks << " tests" << endl;
	}
      }
    } 
  }
  void control(ControlStatus* cs, MergedDirective* md) {}
};

int main(int argc, char **argv)
{
  if (getenv("SPREAD_DAEMON"))
    CmdArgs::spreadDaemon = getenv("SPREAD_DAEMON");
  else {
    cerr << "unknown Spread daemon: please set SPREAD_DAEMON" << endl;
    exit(1);
  }

  int sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;
  CTrafficPlanner* tplanner = new CTrafficPlanner(sn_key);
  tplanner->Start();

  CSparrowHawk *shp = NULL;
  char RNDFFileName[500];
  char MDFFileName[500];
  char* tmpRNDFFileName = "routes-stluke/stluke_singleroad.rndf";
  char* tmpMDFFileName = "routes-stluke/stluke_singleroad_testobs.mdf";

  /* Figure out what RNDF and MDF we want to use */
  string RNDFFileNameStr, MDFFileNameStr;
  struct stat buf;			/* status buffer for fstat */
  /* First see if the file exists in the current directory */
  ostringstream ossRNDF, ossMDF;
  ossRNDF << "./" << tmpRNDFFileName;
  ossMDF << "./" << tmpMDFFileName;
  RNDFFileNameStr = ossRNDF.str();
  MDFFileNameStr = ossMDF.str();
  if (stat(RNDFFileNameStr.c_str(), &buf) == 0)
  {
    snprintf( RNDFFileName, sizeof(RNDFFileName), "%s", RNDFFileNameStr.c_str() );    
  }
  else if (getenv("DGC_CONFIG_PATH"))
  {
    ostringstream oss1;
    oss1 << getenv("DGC_CONFIG_PATH") << "/" << tmpRNDFFileName;
    RNDFFileNameStr = oss1.str();
    snprintf( RNDFFileName, sizeof(RNDFFileName), "%s", RNDFFileNameStr.c_str() );
  }
  else
  {
    snprintf( RNDFFileName, sizeof(RNDFFileName), "%s", tmpRNDFFileName );
    cerr << "unknown configuration path: please set DGC_CONFIG_PATH" << endl;
    cerr << "ERROR: cannot find RNDF file: " << RNDFFileName << endl;
    exit(1);
  }
  if (stat(MDFFileNameStr.c_str(), &buf) == 0)
  {
    snprintf( MDFFileName, sizeof(MDFFileName), "%s", MDFFileNameStr.c_str() );    
  }
  else if (getenv("DGC_CONFIG_PATH"))
  {
    ostringstream oss2;
    oss2 << getenv("DGC_CONFIG_PATH") << "/" << tmpMDFFileName;
    MDFFileNameStr = oss2.str();
    snprintf( MDFFileName, sizeof(MDFFileName), "%s", MDFFileNameStr.c_str() );
  }
  else
  {
    snprintf( MDFFileName, sizeof(MDFFileName), "%s", tmpMDFFileName );
    cerr << "unknown configuration path: please set DGC_CONFIG_PATH" << endl;  
    cerr << "ERROR: cannot find MDF file: " << MDFFileName << endl;
    exit(1);
  }

  cout << "RNDF file: " << RNDFFileName << endl;
  cout << "MDF file: " << MDFFileName << endl;

  /* Figure out where logged data go */
  string logFileName;
  FILE* logFile;
  string tmpMDFname;
  tmpMDFname.assign(MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("/")+1, 
		    MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("."));
  ostringstream oss;
  struct stat st;
  char timestr[64];
  time_t t = time(NULL);
  strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
  oss << timestr << "-mplanner-" << tmpMDFname << ".log";
  logFileName = oss.str();
  string suffix = "";
    
  // if it exists already, append .1, .2, .3 ... 
  for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
    ostringstream tmp;
    tmp << '.' << i;
    suffix = tmp.str();
  }
  logFileName += suffix;
  logFile = fopen(logFileName.c_str(), "w");
  assert(logFile != NULL);
  cout << "log file: " << logFileName << endl;
  fprintf (logFile, "RNDF file: %s\n", RNDFFileName);
  fprintf (logFile, "MDF file: %s\n\n", MDFFileName);

  /* Get the RNDF object */
  RNDF* rndf = new RNDF();
  assert(rndf->loadFile(RNDFFileName));
  rndf->assignLaneDirection();
 
  /* SparrowHawk object */
  CSparrowHawk &sh = SparrowHawk();  
  shp = &sh;
  pthread_mutex_t shpMutex;
  DGCcreateMutex(&shpMutex);

  CmdArgs::sn_key = sn_key;
  CmdArgs::waitForStateFill = true;
  CmdArgs::listenToMapper = false;
  CmdArgs::nopause = false;
  CmdArgs::noillegalPassing = true;
  CmdArgs::rndf = rndf;
  CmdArgs::RNDFFileName = RNDFFileName;
  CmdArgs::MDFFileName = MDFFileName;
  CmdArgs::getApxStatus = false;
  CmdArgs::getSystemHealth = false;
  CmdArgs::continueMission = false;
  CmdArgs::missionFileName = "mission.log";
  CmdArgs::nosparrow = true;
  CmdArgs::logData = true;
  CmdArgs::logPath = "./logs";
  CmdArgs::logFile = logFile;
  CmdArgs::debugLevel = 0;
  CmdArgs::verbose = false;
  CmdArgs::logLevel = 0;

  MissionControl* mcontrol = new MissionControl(sn_key);
  mcontrol->init(shp, &shpMutex);

  RoutePlanner* rplanner = new RoutePlanner(shp, &shpMutex);

  mcontrol->Start();
  rplanner->Start();
  
  sleep(5);
  rplanner->insertObstacle(0, 1, 1, 2);

  while (!quit)
  {
    sleep(1);
  }
  delete rndf;
  return 0;
}
