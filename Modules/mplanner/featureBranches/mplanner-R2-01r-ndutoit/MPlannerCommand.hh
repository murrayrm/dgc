#ifndef MISSIONPLANNERINTERNALINTERFACES_HH
#define MISSIONPLANNERINTERNALINTERFACES_HH

#include "gcmodule/GcInterface.hh"
#include "interfaces/sn_types.h"
using std::string;
using std::stringstream;

#define LEAST_NUM_MISSION_RPLANNER_STORED 2

enum VehicleCap{ MAX_CAP, MEDIUM_CAP, MIN_CAP };

// Directive from Mission Control
struct MControlDirective : public GcTransmissive
{
  enum DirectiveName{ NEXT_CHECKPOINT, END_OF_MISSION, PAUSE, DRIVE_FORWARD, BACK_UP };

  unsigned goalID;
  DirectiveName name;

  // Parameters for NEXT_CHECKPOINT
  // The waypoint id of the next checkpoint
  int segmentID;
  int laneID;
  int waypointID;
  unsigned nextCheckpointIndex;
  VehicleCap vehicleCap;

  MControlDirective()
  {
    goalID = 0;
    name = PAUSE;
    segmentID = 0;
    laneID = 0;
    waypointID = 0;
    nextCheckpointIndex = 0;
    vehicleCap = MAX_CAP;
  }

  std::string toString() const {
    stringstream s("");
    s << "GoalId: " << goalID << ": ";
    if (name == NEXT_CHECKPOINT)
      s << "NEXT_CHECKPOINT (" << nextCheckpointIndex+1
	<< " = " << segmentID << "." << laneID
	<< "." << waypointID << ")";
    else if (name == END_OF_MISSION)
      s << "END_OF_MISSION";
    else if (name == PAUSE)
      s << "PAUSE";
    else if (name == DRIVE_FORWARD)
      s << "DRIVE_FORWARD";
    else if (name == BACK_UP)
      s << "BACK_UP";
    else
      s << "UNKNOWN";
    s << endl;
    return s.str();
  }

  unsigned getDirectiveId() const
  {
    return goalID;
  }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar & goalID;
    ar & name;
    ar & segmentID;
    ar & laneID;
    ar & waypointID;
    ar & nextCheckpointIndex;
    ar & vehicleCap;
  }
};


struct MControlDirectiveResponse : public GcInterfaceDirectiveStatus
{
  //  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ NO_ROUTE, VEHICLE_CAP, PREEMPTED_BY_PAUSE,
                         PREVIOUS_DIR_FAILURE};

  unsigned goalID;
  //Status status;
  ReasonForFailure reason;

  MControlDirectiveResponse()
  {
    goalID = 0;
    status = ACCEPTED;
  }
  virtual unsigned getDirectiveId() const
  {
    return goalID;
  }

  /*! Serialize function */
  friend class boost::serialization::access;

private:
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar & goalID;
    ar & status;
    ar & reason;
  }

  std::string toString() const {
    stringstream s("");
    s << "GoalId: " << goalID << " Status: "  << status 
      << " Reason: " << reason << endl;
    return s.str();
  }
};

#define SNmission SNmark1
#define SNmissionStatus SNmark2

typedef GcInterface<MControlDirective, MControlDirectiveResponse, 
		    GCM_IN_PROCESS_DIRECTIVE, GCM_IN_PROCESS_DIRECTIVE, 
		    MODmissionplanner> MRInterface ;

/*
typedef GcInterface<MControlDirective, MControlDirectiveResponse, 
		    SNmission, SNmissionStatus, 
		    MODmissionplanner> MRInterface ;
*/
#endif //MISSIONPLANNERINTERNALINTERFACES_HH

