/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include <getopt.h>
#include <iostream>
#include "RoutePlanner.hh"
#include "dgcutils/DGCutils.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include "skynet/skynet.hh"
#include "cmdline.h"

using namespace std;

int main(int argc, char **argv) 
{
  gengetopt_args_info cmdline;
  int sn_key;
  bool noSparrow = false;
  
  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
  
  /* Figure out what skynet key to use */
  sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;

  if(cmdline.disable_sparrow_flag || cmdline.nosp_flag)
  {  
    noSparrow = true;
    cerr << "NO SPARROW DISPLAY" << endl;
  }
    
  if(cmdline.rndf_given)
  {
    cerr << "RNDF file: " << cmdline.rndf_arg << endl;
  }
  
  cout << "MDF file: " << cmdline.mdf_arg << endl;

  RoutePlanner* pMissionPlanner = new RoutePlanner(sn_key, !cmdline.nowait_given, 
      noSparrow, cmdline.rndf_arg, cmdline.mdf_arg, !cmdline.nomap_given, cmdline.debug_arg,
      cmdline.verbose_given);

  DGCstartMemberFunctionThread(pMissionPlanner, &RoutePlanner::getTPlannerStatusThread);
  DGCstartMemberFunctionThread(pMissionPlanner, &RoutePlanner::sendSegGoalsThread);
  if (!noSparrow)
  {
    DGCstartMemberFunctionThread(pMissionPlanner, &RoutePlanner::SparrowDisplayLoop);
    DGCstartMemberFunctionThread(pMissionPlanner, &RoutePlanner::UpdateSparrowVariablesLoop);
  }
  pMissionPlanner->MPlanningLoop();

  return 0;
}
