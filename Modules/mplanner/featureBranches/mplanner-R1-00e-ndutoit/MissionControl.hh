/*!**
 * Nok Wongpiromsarn
 * February 21, 2007
 */


#ifndef MISSIONCONTROL_HH
#define MISSIONCONTROL_HH

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include "rndf/RNDF.hh"
#include "sparrowhawk/SparrowHawk.hh"


struct MContrDirectiveStatus
{
  enum Status{ FAILED, COMPLETED };
  enum ReasonForFailure{ VEHICLE_CAP, UNKNOWN };
  MContrDirectiveStatus()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int goalID;
  Status status;
  ReasonForFailure reason;
};

struct MContrDirective
{
  enum Name{ NEXT_CHECKPOINT, PAUSE };
  MContrDirective()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int goalID;
  Name name;
  Waypoint* nextCheckpoint;
};


/**
 * MissionControl class.
 * This is a main class for the mission control part of the mission planner.
 * It is responsible for initialzing checkpoint series and keeping track of 
 * where we are in the mission. It also determines the minimum vehicle
 * capability for Alice to run.
 * \brief Main class for mission control part of mission planner
 */
class MissionControl
{
  int DEBUG_LEVEL, VERBOSITY_LEVEL;

  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;

  /*!\param m_rndf is a pointer to the RNDF object. */
  RNDF* m_rndf;

  /*!\param m_checkpointSequence is the vector of waypoints which are
   * the checkpoints that we have to cross in order as specified by MDF */
  vector<Waypoint*> m_checkpointSequence;

  /*!\param m_nextCheckpointIndex is the index of the next checkpoint
   * in m_checkpointSequence */
  int m_nextCheckpointIndex;

  /*! \param m_nextContrDirective is the next mission control directive. */
  MContrDirective m_nextContrDirective;

  /*!\param m_contrDirectiveQueue stores all the directives that fail */
  vector<MContrDirective> m_failedContrDirectiveQueue;

public:
  /*! Constructor */
  MissionControl(RNDF* rndf, char* MDFFileName, bool nosparrow, int debugLevel, bool verbose);
      
  /*! Standard destructor */
  ~MissionControl();

  /*! Get the next mission control directive */
  MContrDirective getNextDirective();

  /*! Update the status of mission control directive */
  void updateDirectiveStatus(MContrDirectiveStatus directiveStatus);

private:
  /*! Arbitration for the mission control module. It basically loads MDF file
   *  and parses checkpoint series. It will put the vehicle in pause if
   *  the given MDF file is not valid. */
  void arbitrate(char* MDFFileName, RNDF* rndf);

  /*! Control for the mission control module. It is responsible for
   *  updating m_nextDirective, as the previous one is completed
   *  or failed. This includes determining the next checkpoint and 
   *  minimum vehicle capability for Alice to run. */
  void control();
  void control(MContrDirectiveStatus directiveStatus);

  /*! A function that parses checkpoint series in MDF. */
  bool loadMDF(char* MDFFileName, RNDF* rndf);

  /*! A helper function for loadMDF. */
  void parseCheckpoint(ifstream*, RNDF*);
};

#endif  //MISSIONCONTROL_HH
