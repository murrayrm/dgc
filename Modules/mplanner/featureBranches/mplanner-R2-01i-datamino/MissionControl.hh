/*!**
 * Nok Wongpiromsarn
 * February 21, 2007
 */


#ifndef MISSIONCONTROL_HH
#define MISSIONCONTROL_HH

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <deque>
#include "rndf/RNDF.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include "gcmodule/GcModule.hh"
#include "Interfaces.hh"

#define BUFFER_SIZE 1000
//#define PRINTBUFFER

class MissionControlStatus : public ControlStatus
{

};

class MissionControlMergedDirective : public MergedDirective
{
public:
  /* param checkpointSequence is the vector of waypoints which are
   * the checkpoints that we have to cross in order as specified by MDF */
  vector<Waypoint*> checkpointSequence;
};


/**
 * MissionControl class.
 * This is a main class for the mission control part of the mission planner.
 * It is responsible for initialzing checkpoint series and keeping track of 
 * where we are in the mission. It also determines the minimum vehicle
 * capability for Alice to run.
 * \brief Main class for mission control part of mission planner
 */
class MissionControl :  public GcModule
{
  /*! The structure for keeping information about directives sent to the
   *  route planner */
  struct Strategy
  {
    enum Status{ SENT, ACCEPTED, COMPLETED, FAILED };
    enum ReasonForFailure{ UNKNOWN, VEHICLE_CAP };
    Strategy()
    {
      // initialize the segment goals to zeros to appease the memory profilers
      memset(this, 0, sizeof(*this));
      this->status = SENT;
    }
    int goalID;
    DirectiveName name;
    int nextCheckpointIndex;
    VehicleCap vehicleCap;
    Status status;
    ReasonForFailure reason;
  };
 
  int DEBUG_LEVEL, VERBOSITY_LEVEL;

  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;

  bool m_logData;
  FILE* m_logFile;

  /*!\param m_rndf is a pointer to the RNDF object. */
  RNDF* m_rndf;

  /*!\param the name of the MDF file. This is the directive to the Mission Control module */
  char* m_MDFFileName;

  /*!\param m_nextCheckpointIndex is the index of the next checkpoint
   * in m_checkpointSequence. This is basically the current arbitrated 
   * directive. */
  int m_nextCheckpointIndex;

  /*!\param m_contrDirectiveQueue stores all the directives sent to the route planner. */
  deque<Strategy> m_contrDirectiveQueue;

  /*!\param merged directive sent from arbiter to control */
  MissionControlMergedDirective m_mergedDirective;

  /*!\param control status sent from control to arbiter */
  MissionControlStatus m_contrStatus;

  /*!\param the latest status received from the route planner */
  MControlDirectiveResponse m_directiveStatus;

  /*!\param Whether the mission is completed */
  bool m_missionCompleted;

public:
  /*! Constructor */
  MissionControl();
      
  /*! Standard destructor */
  virtual ~MissionControl();

  /*! Initilize THIS */
  void init(RNDF* rndf, char* MDFFileName, bool nosparrow, bool logData, FILE* logFile, int debugLevel, bool verbose);

  /*! Get the next mission control directive */
  MContrDirective getNextDirective();

  /*! Get the index of the next checkpoint */
  int getNextCheckpointIndex();

  /*! Update the status of mission control directive */
  void updateDirectiveStatus(MControlDirectiveResponse directiveResponse);

private:
  /*! Arbitration for the mission control module. It basically loads MDF file
   *  and parses checkpoint series. It will put the vehicle in pause if
   *  the given MDF file is not valid. */
  void arbitrate(ControlStatus*, MergedDirective*);

  /*! Control for the mission control module. It is responsible for
   *  updating m_nextDirective, as the previous one is completed
   *  or failed. This includes determining the next checkpoint and 
   *  minimum vehicle capability for Alice to run. */

  // The control that computes directive based on the current arbitrated
  // directive only.
  void control(vector<Waypoint*> checkpointSequence);

  // The control that computes directive based on both the arbitrated
  // directive as well as status from the module below.
  void control(ControlStatus*, MergedDirective*);

  /*! A function that parses checkpoint series in MDF. */
  bool loadMDF(char* MDFFileName, RNDF* rndf, MissionControlMergedDirective* mergedDirective);

  /*! A helper function for loadMDF. */
  void parseCheckpoint(ifstream*, RNDF*, MissionControlMergedDirective* mergedDirective);

  /*! A function that converts Strategy to MContrDirective */
  MContrDirective strategy2ContrDirective(Strategy strategy, vector<Waypoint*> checkpointSequence);
};

#endif  //MISSIONCONTROL_HH
