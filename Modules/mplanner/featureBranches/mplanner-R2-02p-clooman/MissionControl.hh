/*!**
 * Nok Wongpiromsarn
 * February 21, 2007
 */


#ifndef MISSIONCONTROL_HH
#define MISSIONCONTROL_HH

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <deque>
#include <stdint.h>
#include <rndf/RNDF.hh>
#include <sensnet/sensnet.h>
#include <skynettalker/SkynetTalker.hh>
#include <sparrowhawk/SparrowHawk.hh>
#include <interfaces/VehicleCapability.h>
#include <gcmodule/GcModule.hh>
#include "MPlannerCommand.hh"

#define LEAST_NUM_MISSION_STORED 5
#define NUM_GOALS_DISPLAYED 4

class MissionControlStatus : public ControlStatus
{

};

class MissionControlMergedDirective : public MergedDirective
{
public:
  enum DirectiveName{ MISSION, PAUSE };
  DirectiveName name;
  unsigned id;
  /* param checkpointSequence is the vector of waypoints which are
   * the checkpoints that we have to cross in order as specified by MDF */
  vector<Waypoint*> checkpointSequence;
};


/**
 * MissionControl class.
 * This is a main class for the mission control part of the mission planner.
 * It is responsible for initialzing checkpoint series and keeping track of 
 * where we are in the mission. It also determines the minimum vehicle
 * capability for Alice to run.
 * \brief Main class for mission control part of mission planner
 */
class MissionControl :  public GcModule
{
  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param astateHealthTalker is the talker for receiving
   * astate health information */
  SkynetTalker<int> astateHealthTalker;

  /*!\param healthTalker is the talker for receiving
   * system health information from health-monitor */
  SkynetTalker<VehicleCapability> healthTalker;
 
  int DEBUG_LEVEL, VERBOSITY_LEVEL;

  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;

  /*!\param m_logData specifies whether data will be logged */
  bool m_logData;

  /*!\param m_logFile is the log file */
  FILE* m_logFile;

  /*!\param m_rndf is a pointer to the RNDF object. */
  RNDF* m_rndf;

  /*!\param m_RNDFFileName is the name of the RNDF file */
  char* m_RNDFFileName;

  /*!\param the name of the MDF file. This is the directive to the Mission Control module */
  char* m_MDFFileName;

  /*!\param m_nextCheckpointIndex is the index of the next checkpoint
   * in m_checkpointSequence. */
  unsigned m_nextCheckpointIndex;

  /*!\param m_lastAddedCheckpointIndex is the index of the last checkpoint
   * added to m_contrDirectiveQ */
  unsigned m_lastAddedCheckpointIndex;

  /*!\param merged directive sent from arbiter to control */
  MissionControlMergedDirective m_mergedDirective;

  /*!\param control status sent from control to arbiter */
  MissionControlStatus m_contrStatus;

  /*!\param m_response is the latest response from the route planner */
  MControlDirectiveResponse m_response;

  /*!\param directives currently stored in control waiting to be added to contrGcPort */
  deque<MControlDirective> m_contrDirectiveQ;

  /*!\param the mutex to protect m_contrDirectiveQ */
  pthread_mutex_t m_contrDirectiveQMutex;

  /*!\param Whether the mission is completed */
  bool m_missionCompleted;

  /*!\param Whether END_OF_MISSION directive is issued */
  bool m_eomIssued;

  /*!\param Whether the system is initialized */
  bool m_isInit;

  /*!\param whether we want to restart the mission */
  bool m_restart;

  /*!\param id for currently executing merged directive */
  unsigned m_currentMergedDirectiveID;

  /*!\param id of the last goal */
  unsigned m_lastGoalID;

  /*!\param Applanix status */
  int m_apxCount;
  int m_apxStatus;
  uint64_t m_apxReceivedTime;
  int APXTIMEOUT;  // Apx timeout in ms

  /*!\param variable that keeps track of the last time that PAUSE is sent*/
  bool m_getApxStatus;
  uint64_t m_lastPauseT;

  /*!\param m_vehicleCap is the latest system health from health monitor */
  VehicleCapability m_vehicleCap;

  /*\param the number of times vehicle capability is received from health monitor*/
  int m_healthCount;

  /*!\param SparrowHawk display */
  CSparrowHawk *m_shp;

  /*!\param Mutex for m_shp */
  pthread_mutex_t* m_shpMutex;

  /*!\param GcInterface variable */
  MRInterface::Southface* mrInterfaceSF;

  /*!\param Sparrow variables */
  int sparrowNextGoalIDs[NUM_GOALS_DISPLAYED];
  int sparrowNextSegments[NUM_GOALS_DISPLAYED];
  int sparrowNextLanes[NUM_GOALS_DISPLAYED];
  int sparrowNextWaypoints[NUM_GOALS_DISPLAYED];
  int sparrowResponseGoalID;
  int sparrowNextCkpt;

  /*!\param Spread setting */
  char* spreadDaemon;

  /*!\param SensNet handle */
  sensnet_t* m_sensnet;

public:
  /*! Constructor */
  MissionControl(int skynetKey);
      
  /*! Standard destructor */
  virtual ~MissionControl();

  /*! Initilize THIS */
  void init(CSparrowHawk* shp, pthread_mutex_t* shpMutex);

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

private:
  /*! Arbitration for the mission control module. It basically loads MDF file
   *  and parses checkpoint series. It will put the vehicle in pause if
   *  the given MDF file is not valid. */
  void arbitrate(ControlStatus*, MergedDirective*);

  /*! Control for the mission control module. It is responsible for
   *  updating m_nextDirective, as the previous one is completed
   *  or failed. This includes determining the next checkpoint and 
   *  minimum vehicle capability for Alice to run. */
  void control(ControlStatus*, MergedDirective*);

  /*! Initialize sensnet */
  void initSensnet();

  /* Add NEXT_CHECKPOINT directives to m_contrDirectiveQ */
  void updateControlDirectiveQ(const vector<Waypoint*>& checkpointSequence);

  /* Compute the NEXT_CHECKPOINT or END_OF_MISSION directive */
  MControlDirective findNextCheckpointDirective(vector<Waypoint*> checkpointSequence, 
			    unsigned nextCheckpointIndex);

  /*! Flush m_contrGcPortMsgQ and m_contrDirectiveQ */
  void resetGoals();

  /*! Send directive */
  void sendDirective();

  /*! Update the process state */
  int updateProcessState();

  /*! A function that parses checkpoint series in MDF. */
  bool loadMDF(char* MDFFileName, RNDF* rndf, MissionControlMergedDirective* mergedDirective);

  /*! A helper function for loadMDF. */
  void parseCheckpoint(ifstream*, RNDF*, MissionControlMergedDirective* mergedDirective);

  /*! A function to log data and print out a message on the screen
   *  if DEBUG_LEVEL is greater than d*/
  void print(string s, int d);
  void printError(string funcName, string errorMessage, bool error = true);

  /*! Get time in microsecond */
  uint64_t getTime();

  /*! Restart the mission */
  void sparrowRestartMission();

  /*! Quit */
  void sparrowQuit();
};

#endif  //MISSIONCONTROL_HH
