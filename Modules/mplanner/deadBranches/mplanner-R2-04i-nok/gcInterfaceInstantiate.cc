#ifdef OMIT_TEMPLATE_DEFS
#undef OMIT_TEMPLATE_DEFS
#endif // #ifdef OMIT_TEMPLATE_DEFS

#include "gcinterfaces/SegGoals.hh"
#include "gcinterfaces/SegGoalsStatus.hh"
#include "MPlannerCommand.hh"
#include "RoutePlanner.hh"
#include "MissionControl.hh"

void instantiateCode() {
  MRInterface::Southface* mrInterfaceSF = MRInterface::generateSouthface(-1, (GcModule*)NULL);
  MRInterface::releaseSouthface(mrInterfaceSF);

  MRInterface::Northface* mrInterfaceNF = MRInterface::generateNorthface(-1, (GcModule*)NULL);
  MRInterface::releaseNorthface(mrInterfaceNF);

  RTInterface::Southface* rtInterfaceSF = RTInterface::generateSouthface(-1,(GcModule*)NULL);
  RTInterface::releaseSouthface( rtInterfaceSF );

  RTInterface::Northface* rtInterfaceNF = RTInterface::generateNorthface(-1,(GcModule*)NULL);
  RTInterface::releaseNorthface( rtInterfaceNF );
}
