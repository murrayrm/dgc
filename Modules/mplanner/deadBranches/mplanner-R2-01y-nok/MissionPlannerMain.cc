/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include <getopt.h>
#include <iostream>
#include "RoutePlanner.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/findroute.h"
#include "sparrowhawk/SparrowHawk.hh"
#include "skynet/skynet.hh"
#include "cmdline.h"
#include <sys/time.h>
#include <sys/stat.h>
#include <signal.h>


using namespace std;

volatile sig_atomic_t quit = 0;
void sigintHandler(int /*sig*/)
{
  // if the user presses CTRL-C three or more times, and the program still
  // doesn't terminate, abort
  if (quit > 2) {
    abort();
  }
  quit++;
}


int main(int argc, char **argv) 
{
  // catch CTRL-C and exit cleanly, if possible
  signal(SIGINT, sigintHandler);
  // and SIGTERM, i.e. when someone types "kill <pid>" or the like
  signal(SIGTERM, sigintHandler);

  CSparrowHawk *shp = NULL;

  gengetopt_args_info cmdline;
  int sn_key;
  bool noSparrow = false;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
  
  /* Figure out what skynet key to use */
  sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;

  /* Figure out if we want to use sparrow display */
  if(cmdline.disable_console_flag || cmdline.nosp_flag)
  {  
    noSparrow = true;
    cout << "NO SPARROW DISPLAY" << endl;
  }

  /* Figure out what RNDF and MDF we want to use */
  char *RNDFFileName = dgcFindRouteFile(cmdline.rndf_arg);
  char *MDFFileName = dgcFindRouteFile(cmdline.mdf_arg);

  cout << "RNDF file: " << RNDFFileName << endl;
  cout << "MDF file: " << MDFFileName << endl;

  /* Figure out where logged data go */
  string logFileName;
  FILE* logFile;
  if(cmdline.enable_logging_given)
  {
    /* Check if the logs directory exists */
    struct stat st;
    int ret = stat("./logs", &st);
    if (errno == ENOENT) {
      if(mkdir("./logs", 0755) != 0)
	cerr << __FILE__ << ":" << __LINE__ << "cannot create log dir.";
    }

    string MDFFileNameStr;
    string tmpMDFname;
    ostringstream ossMDF;
    ossMDF << "./" << cmdline.mdf_arg;
    MDFFileNameStr = ossMDF.str();
    tmpMDFname.assign(MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("/")+1, 
		      MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("."));
    ostringstream oss;
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
    oss  << "./logs/mplanner-" << tmpMDFname << "." << timestr << ".log";
    logFileName = oss.str();
    string suffix = "";
    
    // if it exists already, append .1, .2, .3 ... 
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
    logFile = fopen(logFileName.c_str(), "w");
    if (logFile == NULL)
    {
      cerr << "Cannot open log file: " << logFile << endl;
      exit(1);
    }
    cout << "log file: " << logFileName << endl;
    fprintf (logFile, "RNDF file: %s\n", RNDFFileName);
    fprintf (logFile, "MDF file: %s\n\n", MDFFileName);
  }

  /* Get the RNDF object */
  RNDF* rndf = new RNDF();
  if (!rndf->loadFile(RNDFFileName))
  {
    cerr << "ERROR:  Unable to load RNDF file " << RNDFFileName
	 << ", exiting program" << endl;
    if (cmdline.enable_logging_given)
    {
      fprintf( logFile, "ERROR:  Unable to load RNDF file %s, exiting program\n", RNDFFileName);
    }
    exit(1);
  }
  rndf->assignLaneDirection();
    
  if (cmdline.debug_arg > 2)
  {
    rndf->print();
    cout << "Finished rndf" << endl;
  }
  else if (cmdline.debug_arg > 1)
  {
    for (int i=1; i <= rndf->getNumOfSegments(); i++)
    {
      for (int j=1; j <= rndf->getSegment(i)->getNumOfLanes(); j++)
      {
        cout << "Lane " << i << "." << j << ":\t\t"
             << rndf->getLane(i,j)->getDirection() << endl;
      }
    } 
  }
  if (cmdline.enable_logging_given)
  {
    fprintf ( logFile, "\n" );
    for (int i=1; i <= rndf->getNumOfSegments(); i++)
    {
      for (int j=1; j <= rndf->getSegment(i)->getNumOfLanes(); j++)
      {
	fprintf ( logFile, "Lane %d.%d: \t %d\n", i, j, rndf->getLane(i,j)->getDirection() );
      }
    }
    fprintf( logFile, "Finished rndf\n" );
  }
 
  /* SparrowHawk object */

  if (!noSparrow) {
    CSparrowHawk &sh = SparrowHawk();  
    shp = &sh;
  }

  MissionControl* mcontrol = new MissionControl(sn_key);
  mcontrol->init(rndf, RNDFFileName, MDFFileName, 
		 cmdline.enable_apx_status_given, noSparrow,
		 cmdline.enable_logging_given,
		 logFile, cmdline.debug_arg, 
		 cmdline.verbose_given, shp, cmdline.log_level_arg);

  RoutePlanner* rplanner = new RoutePlanner(sn_key, !cmdline.nowait_given, 
      noSparrow, rndf, RNDFFileName, MDFFileName, !cmdline.nomap_given,
      cmdline.nopause_given, cmdline.noillegalpassing_given,
      cmdline.enable_logging_given, logFile, cmdline.debug_arg, 
      cmdline.verbose_given, shp, cmdline.log_level_arg);

  mcontrol->Start();
  rplanner->Start();

  if (!noSparrow) {
    DGCstartMemberFunctionThread(mcontrol, &MissionControl::UpdateSparrowVariablesLoop);
    DGCstartMemberFunctionThread(rplanner, &RoutePlanner::SparrowDisplayLoop);
    DGCstartMemberFunctionThread(rplanner, &RoutePlanner::UpdateSparrowVariablesLoop);
  }


  while (!quit)
  {
    usleep(100000);
  }

  if(cmdline.enable_logging_given)
  {
    fclose(logFile);
  }
  delete rndf;
  return 0;
}

