Sat Mar 17  2:38:32 2007	Nok Wongpiromsarn (nok)

	* version R2-01c-nok
	BUGS: 
	FILES: RoutePlanner.cc(18104)
	Added more information about segment goals in the logged data

	FILES: RoutePlanner.cc(18196),
		TraversibilityGraphEstimation.cc(18196),
		TraversibilityGraphEstimation.hh(18196)
	Fixed bugs in uturn

Fri Mar 16 13:10:03 2007	Nok Wongpiromsarn (nok)

	* version R2-01c
	BUGS: 
	FILES: RoutePlanner.hh(18055)
	Fixed include statement due to the changes in gcmodule

Fri Mar 16 12:17:26 2007	Nok Wongpiromsarn (nok)

	* version R2-01b
	BUGS: 
	FILES: MissionPlannerMain.cc(18020), RoutePlanner.cc(18020),
		RoutePlanner.hh(18020),
		TraversibilityGraphEstimation.cc(18020)
	Added more information to log file.

Fri Mar 16  1:06:13 2007	Nok Wongpiromsarn (nok)

	* version R2-01a
	BUGS: 
	Deleted files: DummyTPlanner.hh
	FILES: DummyTPlanner.cc(17897), Makefile.yam(17897),
		MissionControl.cc(17897), MissionPlannerMain.cc(17897),
		RoutePlanner.cc(17897), RoutePlanner.hh(17897),
		TraversibilityGraphEstimation.cc(17897),
		mplanner.ggo(17897)
	Message handling (sending directives, receiving status and matching
	them) is now done automatically by GcInterface. Also look for RNDF
	and MDF in DGC_CONFIG_PATH.

	FILES: MissionControl.cc(17915), MissionControl.hh(17915),
		MissionPlannerMain.cc(17915), RoutePlanner.cc(17915),
		RoutePlanner.hh(17915),
		TraversibilityGraphEstimation.cc(17915),
		TraversibilityGraphEstimation.hh(17915),
		mplanner.ggo(17915)
	Added option for enabling logging and fixed the yaw angle so it
	ranges between 0 and 2pi.

Wed Mar 14 18:05:42 2007	Nok Wongpiromsarn (nok)

	* version R2-00h
	BUGS: 
	FILES: DummyTPlanner.cc(17656), MissionPlannerMain.cc(17656),
		RoutePlanner.cc(17656), RoutePlanner.hh(17656),
		TraversibilityGraphEstimation.cc(17656)
	Reduced the number of threads in RoutePlanner by combining the
	sending and receiving message threads into one thread. Made the
	dummyTPlanner a little smarter.

Tue Mar 13 21:17:54 2007	Nok Wongpiromsarn (nok)

	* version R2-00g
	BUGS: 
	FILES: Makefile.yam(17502), MissionControl.cc(17502),
		MissionControl.hh(17502), RoutePlanner.hh(17502)
	MissionControl derived from GcModule and WORKS !!!

	FILES: MissionControl.cc(17514), MissionControl.hh(17514),
		RoutePlanner.cc(17514), RoutePlanner.hh(17514)
	RoutePlanner is now derived from GcModule also.

	FILES: MissionUtils.cc(17540), RoutePlanner.cc(17540),
		RoutePlanner.hh(17540),
		TraversibilityGraphEstimation.cc(17540)
	Fixed the startup so it takes into account lane direction. Still
	need to fix some weird behavior in sparrow display

Mon Mar 12 19:30:17 2007	Nok Wongpiromsarn (nok)

	* version R2-00f
	BUGS: 
	FILES: DummyTPlanner.cc(17289), MissionControl.cc(17289),
		MissionControl.hh(17289), RoutePlanner.cc(17289),
		RoutePlanner.hh(17289),
		TraversibilityGraphEstimation.cc(17289)
	The RoutePlanner in the canonical structure but not yet derived
	from gcmodule. Fixed some bugs in MissionControl.

Fri Mar  9 16:37:36 2007	Nok Wongpiromsarn (nok)

	* version R2-00e
	BUGS: 
	FILES: Makefile.yam(16909), MissionControl.cc(16909),
		MissionControl.hh(16909), MissionUtils.cc(16909),
		MissionUtils.hh(16909), README(16909),
		RoutePlanner.cc(16909), RoutePlanner.hh(16909),
		TraversibilityGraphEstimation.cc(16909)
	Fixed the heading angle computation. Added the default constructor
	for MissionControl. Fixed Makefile. Combined the functions
	planFromCurrentPosition and planNextMission in RoutePlanner.

Wed Mar  7 20:56:05 2007	Nok Wongpiromsarn (nok)

	* version R2-00d
	BUGS: 
	FILES: MissionUtils.cc(16761), MissionUtils.hh(16761),
		RoutePlanner.cc(16761), RoutePlanner.hh(16761),
		TraversibilityGraphEstimation.cc(16761),
		TraversibilityGraphEstimation.hh(16761)
	All the rndf dependencies were moved to the graph estimation class
	(for unit test only). This version of mplanner only commands uturn
	if there are obstacles that block the road.

Wed Mar  7 11:30:20 2007	Nok Wongpiromsarn (nok)

	* version R2-00c
	BUGS: 
	FILES: RoutePlanner.cc(16732)
	Fixed segfault when DEBUG_LEVEL>4 and removed that mutex that
	protects the map because only one thread in RoutePlanner needs it
	now.

Tue Mar  6 21:20:23 2007	Nok Wongpiromsarn (nok)

	* version R2-00b
	BUGS: 
	FILES: DummyTPlanner.cc(16694), MissionUtils.cc(16694),
		RoutePlanner.cc(16694),
		TraversibilityGraphEstimation.cc(16694)
	Fixed bug in the findRoute function. Getting closer to remove the
	rndf dependency. The route planner now only needs RNDF when it
	determines which edges the manually inserted obstacles block. This
	function should belong to the map class but I'll leave it here for
	now for unit test purpose.

Mon Mar  5 19:47:10 2007	Nok Wongpiromsarn (nok)

	* version R2-00a
	BUGS: 
	FILES: DummyTPlanner.cc(16642), Makefile.yam(16642),
		MissionUtils.cc(16642), MissionUtils.hh(16642),
		RoutePlanner.cc(16642), RoutePlanner.hh(16642),
		TraversibilityGraphEstimation.cc(16642),
		TraversibilityGraphEstimation.hh(16642)
	Mission planner that plans route without using rndf information.
	Got the stupid segfault problem figured out. (Thanks to Daniele
	again.)

Sat Mar  3 20:29:05 2007	Nok Wongpiromsarn (nok)

	* version R1-00g
	BUGS: 
	New files: DummyTPlanner.cc DummyTPlanner.hh DummyTPlannerMain.cc
		TraversibilityGraphEstimation.cc
		TraversibilityGraphEstimation.hh
	FILES: Makefile.yam(16407), MissionControl.cc(16407),
		MissionControl.hh(16407), MissionPlanner.cc(16407),
		MissionPlanner.hh(16407), MissionPlannerMain.cc(16407),
		MissionUtils.cc(16407), MissionUtils.hh(16407),
		RoutePlanner.cc(16407), RoutePlanner.hh(16407),
		mplanner.ggo(16407)
	The mission planner that communicates with mapper through graph
	instead of rndf.

	FILES: MissionUtils.cc(16497), RoutePlanner.cc(16497),
		RoutePlanner.hh(16497)
	Moved all the graph functionality from the MissionPlanner class to
	the TraversibilityGraphEstimator class. This version of mplanner
	works with the new interfaces between mapper and mplanner.

Thu Mar  1 22:31:42 2007	Nok Wongpiromsarn (nok)

	* version R1-00f
	BUGS: 
	New files: RoutePlanner.cc RoutePlanner.hh
	Deleted files: Edge.cc Edge.hh Graph.cc Graph.hh Vertex.cc
		Vertex.hh
	FILES: Makefile.yam(16111), MissionPlanner.cc(16111),
		MissionPlanner.hh(16111), MissionUtils.cc(16111),
		MissionUtils.hh(16111)
	moved the graph structure to the travgraph module, including all
	the relevant functions.

	FILES: MissionControl.cc(16021), MissionControl.hh(16021)
	Added route planning module and modified mission control module so
	it handles some simple failure.

Wed Feb 21 22:38:57 2007	Nok Wongpiromsarn (nok)

	* version R1-00e
	BUGS: 
	New files: MissionControl.cc MissionControl.hh
	FILES: Makefile.yam(15405), MissionPlanner.cc(15405)
	Updated to work with the new Status.hh in revision R2-00b of
	interfaces

Tue Feb 20 17:17:37 2007	Nok Wongpiromsarn (nok)

	* version R1-00d
	BUGS: 
	FILES: Makefile.yam(15204), MissionPlanner.cc(15204),
		MissionPlanner.hh(15204)
	fixed mission planner so it compiles against revision R2-00a of
	interfaces and skynet

Thu Feb  1 20:34:28 2007	Nok Wongpiromsarn (nok)

	* version R1-00c
	BUGS: 
	FILES: MissionPlanner.cc(14000), MissionUtils.cc(14000),
		MissionUtils.hh(14000)
	Modified mission planner so it takes into account the orientation
	of the vehicle when planning from current position

Wed Jan 31 11:36:23 2007	Nok Wongpiromsarn (nok)

	* version R1-00b
	BUGS: 
	New files: MissionUtils.cc MissionUtils.hh mplanner.ggo
	Deleted files: missionPlanner.ggo missionUtils.cc missionUtils.hh
	FILES: Makefile.yam(13711), MissionPlanner.cc(13711),
		MissionPlanner.hh(13711)
	Modified the variable names due to the changes in VehicleState.

Wed Jan 24 23:58:21 2007	Nok Wongpiromsarn (nok)

	* version R1-00a
	BUGS: 
	New files: Edge.cc Edge.hh Graph.cc Graph.hh MissionPlanner.cc
		MissionPlanner.hh MissionPlannerMain.cc Obstacle.cc
		Obstacle.hh Vertex.cc Vertex.hh missionPlanner.ggo
		missionUtils.cc missionUtils.hh mplannerDisplay.dd
	FILES: Makefile.yam(13137)
	Added all the files needed to by mplanner

Tue Jan 23 20:03:35 2007	Nok Wongpiromsarn (nok)

	* version R1-00
	Created mplanner module.


















