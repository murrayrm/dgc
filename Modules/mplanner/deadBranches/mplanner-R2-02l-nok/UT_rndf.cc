#include <iostream>
#include "rndf/RNDF.hh"
#include "dgcutils/ggis.h"
#include "travgraph/Graph.hh"
using namespace std;

int main(int argc, char **argv) 
{
  
  RNDF* m_rndf = new RNDF();
  if (!m_rndf->loadFile("/home/users/nok/navigation-nok01/src/routes-stluke/stluke_small.rndf"))
  // if (!m_rndf->loadFile("/home/users/nok/setup-nok02/src/routes-darpa/darpa_sample.rndf"))
  {
    cerr << "ERROR:  Unable to load RNDF file " << endl;
    exit(1);
  }

  m_rndf->assignLaneDirection();
  m_rndf->print();

  vector<Segment*> segments = m_rndf->getAllSegments();
  for (unsigned i=0; i < segments.size(); i++)
    cout << i << endl;

  Graph* graph = new Graph(m_rndf);
  graph->print();
  delete m_rndf;
  delete graph;
}
