              Release Notes for "mplanner" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "mplanner" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "mplanner" module can be found in
the ChangeLog file.

Release R2-04e-nok (Sat Oct 13 11:24:32 2007):
	Fixed a really stupid bug that caused us not to be able to make a
	nominal uturn when running with health monitor. (This caused mplanner
	to skip couple checkpoints yesterday.)

Release R2-04e (Thu Oct 11 23:47:25 2007):
	Keep sending PAUSE directive every 3 seconds until planner
	responds. So we don't have to restart the planner when mplanner
	needs to be restarted in the middle of the run.

Release R2-04d (Thu Oct 11 15:37:22 2007):
	* Fixed the setCurrentLane function so it can handle the case where
	planner only reports the current segment and not the current lane.
	This is needed when planner's graph is broken and can't follow
	the route that mplanner gives.

Release R2-04c (Thu Oct 11 12:38:45 2007):
	Added the cmdline option --rndf-start so we can specify the start
	waypoint.

Release R2-04b (Wed Oct 10 23:35:45 2007):
	Send BACKUP directive before deciding to skip a checkpoint so we
	can replan and choose different route (in case we're on a one lane
	road and can't uturn). The latest release of planner and logic-planner
	can't handle this directive yet. But the ones in my branch can.
	(I'm just waiting for merging with Andrew's changes.) You can run
	mplanner with --nobackup option so this directive will not be sent.

Release R2-04a (Mon Oct  8 23:26:47 2007):
	Added a program to compute the minimum possible time to complete
	the mission and the maximum possible average speed.

Release R2-03n (Mon Oct  8 16:47:35 2007):
	Increased the max. velocity to 15 m/s within intersections.

Release R2-03m (Sat Oct  6 15:21:58 2007):
	Fixed bugs that kept mplanner from sending a uturn directive when
	the first seggoal fails

Release R2-03l (Sat Oct  6 10:33:47 2007):
	Replaced dynamic_cast with static_cast so coverity stops
	complaining.

Release R2-03k (Fri Oct  5 19:34:21 2007):
	* Fixed some bug so we do the right thing when obstacle is right in
	front of us when we start.

Release R2-03j (Tue Oct  2 22:59:42 2007):
	Check that current lane reported by planner makes sense. Otherwise,
	trust the current seggoals. This should fix the problem where
	mplanner commanded a uturn back to the same lane at El Toro on
	Sunday.

Release R2-03i (Sat Sep 29 16:01:13 2007):
	Added type of failure on the sparrow display for debugging.

Release R2-03h (Sat Sep 29 10:56:28 2007):
	* Now take into account the type of failure when replan 
	  so we don't always make a uturn.

Release R2-03g (Sat Sep 22  5:28:23 2007):
	Handle blocked intersection. Will first try to find other route. If
	none exists or all the other routes also fail, make a uturn. If
	route cannot be found after the u-turn, we'll skip that checkpoint.
	Once planner is ready to receive a BACK_UP goal, we'll try backing
	up before skipping checkpoint.

Release R2-03f (Tue Sep 18 19:54:47 2007):
	* Change the type of the second goal to ROAD_SEGMENT instead of
	PARKING_ZONE. If ROAD_SEGMENT fails, mplanner will give
	PARKING_ZONE. If PARKING_ZONE still fails, it'll tell the planner
	to make a u-turn. Tested with the rest of the navigation stack
	and seems to work ok.

	* Fixed some bugs based on the coverity results. (Speed limits will be
	some uninitialized values if they're not specified in the mdf. Fixed
	some BAD_FREE and FORWARD_NULL.)


Release R2-03e (Tue Sep 18 16:09:20 2007):
	Uturn should work now even when mapper is not running.

Release R2-03d (Thu Sep 13 14:42:11 2007):
	Fixed bug found in the field where mplanner sent reset command too
	often. Added a command line option to limit the speed.

Release R2-03c (Sun Sep  9  9:53:16 2007):
	Added command line options to ensure that we're making forward
	progress. If not, mplanner will reset the stack and replan from where 
	we are

Release R2-03b (Sat Sep  8  0:52:23 2007):
	Added a command line option for listening to estop. If we're
	relocated during PAUSE, mplanner will replan from current position.

Release R2-03a (Fri Sep  7 17:30:21 2007):
	Make a guess of where we currently are based on seggoals.

Release R2-03 (Thu Sep  6 22:15:35 2007):
	Added intersection type to the display

Release R2-02z (Mon Aug 27 18:33:31 2007):
	Handle capability that's in the range of 0-1.

Release R2-02y (Wed Aug 22  0:17:02 2007):
	Fixed some of the logic so mplanner can now handle the situation
	where it gets RESET command from the sparrow display when it's
	in the UTURN mode at the same time as the timeout for
	RESET/EMERGENCY_STOP command is reached, tplanner
	sends a response with wrong ID (probably when it restarts or gets
	reset) and Alice is in PAUSE at the same time.

	This should fix what happened in the field test on Aug 21 where
	mplanner sent down 5000+ goals to the planner.

	Right now, the logic in mplanner is quite complicated. I'm not sure
	that it will be able to handle all the different combinations of 
	all the weird things that can happen correctly. (It can handle each
	of them separately for sure.)

Release R2-02x (Thu Aug 16 23:38:23 2007):
	* Added RESET to teh sparrow display
	* Added timeout (8s) for RESET and EMERGENCY_STOP

Release R2-02w (Mon Aug 13 19:42:23 2007):
	* Send eitehr RESET or EMERGENCY_STOP instead of PAUSE
	* Use module ID when call sensnet_connect()

Release R2-02v (Wed Aug  8 21:53:06 2007):
	Removed the previous hack that allows tplanner to reset the illegal
	passing flag without notifying mplanner. Sven is fixing the new
	planner so we don't need this anymore.

Release R2-02u (Mon Aug  6 13:41:28 2007):
	Fixed the speed limit when health monitor is not running

Release R2-02t (Thu Aug  2 18:38:19 2007):
	Keep track of all the checkpoints that have been crossed so if
	mplanner dies and it's restarted with the --continue flag, we don't
	have to go back to the first checkpoint in the mission.

Release R2-02s (Thu Aug  2 12:20:36 2007):
	Lower the speed limit when system health is bad. Adjust the cost
	for u-turn and zone based on system health.

Release R2-02r (Thu Aug  2  0:34:50 2007):
	Pause when vehicle capability changes and replan right away based
	on the new capability. (The previous version doesn't pause so it
	won't replan based on the new capability immediately.)

Release R2-02q (Sat Jul 28  6:12:18 2007):
	Use SENSNET_SKYNET_SENSOR for both sensnet_connect() and
	sensnet_join(). Just a hack for this field test so mplanner can be
	run with ladarfeeder. But I think the right way to do it (based on
	what sensnet.h says) is to use module ID (MODmissionplanner) for
	sensnet_connect() as in the previous release but becasue
	ladarfeeder is using sensor ID for sensnet_connect() and the sensor
	ID for left front bumper ladar is enumed to be the same as
	MODmissionplanner, one of us will have to change and I think it's
	easier to do it in mplanner for now.

Release R2-02p (Tue Jul 24 16:02:01 2007):
	Fixed sensor id. (I was a little confused about when to use
	MODmissionplanner and when to use SENSNET_SKYNET_SENSOR.)

Release R2-02o (Tue Jul 24 15:15:19 2007):
	Quit most of the threads (except ones from CStateClient) before
	exiting mplanner

Release R2-02n (Fri Jul 20 23:34:04 2007):
	Fixed bug found in the field where mplanner used the wrong sensnet
	id which happened to be the same as left front bumper ladar and
	this caused ladarfeeder to die. Note I didn't see this bug in
	simulation because we don't run ladarfeeder in simulation. Maybe we
	should run all the feeders in simulation too although they don't
	have to do anything but just so we can find this kind of bug
	earlier.

Release R2-02m (Fri Jul 20  3:56:18 2007):
	* Added command line option for listening to health-monitor and
	adjust the plan based on the health info. 
	* Added interface to process-control so it sends heartbeat 
	messages to process-control and you can quit mplanner from the 
	process-control console.

Release R2-02l (Mon Jul  2 17:39:29 2007):
	* Removed two sparrow threads
	* Handle comments in mdf

Release R2-02k (Sat Jun 16 10:35:50 2007):
	* Fixed bug when roadblock is right on the top of checkpoint
	* Decreased the sleep before doing uturn when a checkpoint is blocked
	from 10s to 5 s


Release R2-02j (Sat Jun  9 23:33:28 2007):
	Fixed minor bug when we have to cross a checkpoint from a different
	lane

Release R2-02i (Sat Jun  9 11:49:32 2007):
	Added more debugging messages to logs. Added more info to the
	sparrow display.

Release R2-02h (Fri Jun  8 23:19:04 2007):
	* Found and fixed bugs in previous logic for choosing between
	skipping checkpoint, crossing checkpoint from other lane, uturn and
	multiple uturns. The new logic is totally different from before.
	(For the previous logic, the order of adding and deleting things
	is crucially important and I found that it's really hard to add
	something new in there without breaking something else.)
	Tested for pretty much all scenarios and seems to be working ok.
	* Added a quick hack for fixing the problem where mplanner and
	tplanner get out of sync because tplanner secretly modifies
	mplanner goals. We should fix this (in a right way) after the site visit.

Release R2-02g (Thu Jun  7  9:39:09 2007):
	Increased the threshold for difference between our yaw and lane
	direction at startup

Release R2-02f (Wed Jun  6 23:19:29 2007):
	Tweak the startup so we do something somewhat more reasonable when
	we start perpendicular to the lane direction.

Release R2-02e (Tue Jun  5 22:59:33 2007):
	Better logic for determining whether we should uturn or change lane
	when a checkpoint is blocked

Release R2-02d (Tue Jun  5 14:20:34 2007):
	oops. the previous mplanner segfaulted when an obstacle is on a
	checkpoint.

Release R2-02c (Mon Jun  4 15:54:19 2007):
	Keep computing route when no route exists because
	there is an obstacle that blocks a one lane road. (Currently give
	up after 10 seconds before reporting failure back to mission control.
	Mission control will decide to skip that problematic checkpoint.)
	Tell tplanner to stop right before the end of mission. Sparrow 
	quit now exits cleanly.


Release R2-02b (Sun Jun  3 14:08:19 2007):
	Handle the case where an obstacle is on the checkpoint we need to
	cross.

Release R2-02a (Thu May 31 22:50:23 2007):
	Added command line argument for specifying path for log file

Release R2-02 (Thu May 31 13:53:42 2007):
	Lock mutex around sparrow object which is used by both the mission
	control and route planner. Note sure if this is the cause of
	segfault that Noel saw. (It only segfaulted there once in the past
	6 months so it's quite hard to get the same segfault again and I'm
	not sure how to test this.)

Release R2-01z (Thu May 31  0:49:40 2007):
	Minor fix in the sparrow display. Goal completion that corresponds
	to crossing a checkpoint should now be shown correctly on the
	sparrow display.

Release R2-01y (Wed May 30 11:39:42 2007):
	Mission planner now generates 3 log files, one for mission control,
	one for route planner, and one that keeps track of pretty much
	everything.

Release R2-01x (Mon May 28 10:15:10 2007):
	* Do not send directive until tplanner starts listening. This confused tplanenrCSS but it was there so mplanner and tplannerNoCSS can be started in any order. So from now, if you want to run tplannerNoCSS, you need to start mplanner long enough before tplannerNoCSS.

Release R2-01w (Thu May 24 17:25:16 2007):
	Speed up compilation time. Added command line argument --log-level
	for gcmodule logging. All log files go to ./logs directory.

Release R2-01v (Thu May 24  1:26:11 2007):	
	Fixed another segfault when an obstacle is added from sparrow
	display when mplanner receives map from mapper

Release R2-01u (Wed May 23  1:29:51 2007):
	* Fixed segfault problem
	* Added directive for turn signal
	* mplanner should be bright green for the site visit

Release R2-01t (Sat May 19 12:55:03 2007):
	Used dgcFindRouteFile

Release R2-01s (Sun May 13 10:22:21 2007):
	Added more scenario for unit testing. No change in functionality.

Release R2-01r (Thu May 10 11:58:28 2007):
	* Fixed the MPlannerCommand so it works with the new release of
	gcinterfaces and added scripts for unit tests.
	* Added automated unit tests

Release R2-01q (Sat May  5  9:42:18 2007):
	Fixed bug when illegal passing is allowed

Release R2-01p (Thu May  3 23:37:40 2007):
	* Added a button to remove all the manually inserted obstacles on the
	sparrow display.
	* Added some missing mutices.

Release R2-01o (Thu May  3 18:14:27 2007):
	Fixed the directive class so mplanner compile against version
	R1-00k of gcinterfaces

Release R2-01n (Sat Apr 28 18:43:25 2007):
	Allow illegal uturn.
	Added commandline option for not allowing illegal uturn

Release R2-01m (Sat Apr 28  8:44:48 2007):
	Put more info in the sparrow display

Release R2-01l (Thu Apr 26 23:42:56 2007):
	* This version works with R1-01b of gcmodule
	* Set stale threshold to 20
	* Added apx timeout to teh sparrow display
	* Fixed logging ingraphEstimator
	* Added QUIT to the sparrow display. It'll send down a PAUSE directive before it actually quits.

Release R2-01k (Wed Apr 25 23:39:39 2007):
	* Mission Control now talks to Route Planner using GcInterface (instead of function call)
	* Added stop before uturn
	* 2 tabs in sparrow display, one for Mission Control and the other for Route Planner
	* Handle astate failure
	* Can restart from the console
	* NOTE: This version doesn't compile against R1-01a of gcmodule. This will be fixed
	in the next release. It also segfaults on my mac (gcc 4.0.1) but works fine on gentoo. (Tried running
	it in valgrind and it's pretty happy so I'm not sure what the problem is.)

Release R2-01j (Fri Apr 20 11:49:54 2007):
	* Minor fix to make mplanner compile with version R1-00e of gcinterfaces
	* Modified DummyTPlanner so it uses GcInterface
	* Removed global variable DEBUG_LEVEL

Release R2-01i (Sat Apr 14 13:44:17 2007):
	Fixed the route planner so 3 segment goals are always sent to 
the traffic planner.

Release R2-01h (Wed Apr 11 22:40:35 2007):
	Added missing files. (Note the previous version doesn't compile.)

Release R2-01g (Wed Apr 11 21:43:03 2007):
	Increase the cost of the edge that is blocked instead of removing it. An edge is removed only if it isblocked and tplanner fails on that 
edge. Also fixed the include statements so it compiles against version R4-00q of the interfaces module."

Release R2-01f (Tue Apr  3 18:49:11 2007):
	Updated TraversibilityGraphEstimation so it compiles against version R1-01l of travgraph. Yaw is now in (-pi,pi]. Also added 
	commandline option --disable-console for no sparrow display.

Release R2-01e (Sat Mar 17 13:39:20 2007):
	Fixed bug in uturn when map is received from mapper.

Release R2-01d (Sat Mar 17  2:38:37 2007):
	Moved the function for inserting obstacle to Graph. Recursively remove all the edges in the segment that is blocked and find a uturn point 
that is at least 22m from current position.

Release R2-01c (Fri Mar 16 13:10:06 2007):
	Fixed include statement due to the changes in gcmodule"

Release R2-01b (Fri Mar 16 12:17:33 2007):
	Added more information to log file.

Release R2-01a (Fri Mar 16  1:06:27 2007):
	Mission planner that uses GcInterface for message handling (sending directive, receiving status and paring up directive	and status), so it 
uses SkynetTalker instead of SegGoalsTalker to communicate with traffic planner. It can also log data (mission plan, graph, etc) and search for 
rndf/mdf in the DGC_CONFIG_PATH directory. (Un)fixed the yaw angle so it ranges from 0 to 2pi (which is what astate is using right now).

Release R2-00h (Wed Mar 14 18:05:50 2007):
	Reduced the number of threads in RoutePlanner by combining the	sending and receiving message threads into one thread. Made the dummyTPlanner 
a little smarter. This is the last release of the mission planner that uses SegGoalsTalker to communicate with the traffic planner. 
The next release will use the generic talker.

Release R2-00g (Tue Mar 13 21:18:05 2007):
	Mission planner that is completely in canonical structure. All the modules are derived from GcModule. Still waiting for automatic message 
handling in GcModule. Nok (, Daniele and Josh) are happy.

Release R2-00f (Mon Mar 12 19:30:25 2007):
	The RoutePlanner in the canonical structure but not yet derived from gcmodule. Fixed some bugs in MissionControl.

Release R2-00e (Fri Mar  9 16:37:48 2007):
	Fixed the heading angle computation. Added the default constructor for MissionControl. Fixed Makefile. Combined the functions 
planFromCurrentPosition and planNextMission in RoutePlanner.
	This release of mplanner is only compatible with release R3 (or newer) of the interfaces module.

Release R2-00d (Wed Mar  7 20:56:13 2007):
	All the rndf dependencies were moved to the graph estimation class (for unit test only). This version of mplanner only commands uturn if 
there are obstacles that block the road."

Release R2-00c (Wed Mar  7 11:30:23 2007):
	Fixed segfault when DEBUG_LEVEL>4 and removed that mutex that protects the map because only one thread in RoutePlanner needs it now.

Release R2-00b (Tue Mar  6 21:20:29 2007):
	Fixed bug in the findRoute function. Getting closer to remove the rndf dependency. The route planner now only needs RNDF when it 
determines which edges the manually inserted obstacles block. This function should belong to the map class but I'll leave it here for now for unit 
test purpose.

Release R2-00a (Mon Mar  5 19:47:20 2007):
	Mission planner that plans route without using rndf information. Got the stupid segfault problem figured out. (Thanks to Daniele again.) 
Changed the release number because as in the Release R1-00g, this mission planner uses different interfaces to communicate with the mapper.

Release R1-00g (Sat Mar  3 20:29:19 2007):
	Moved all the graph functionality (including for doing uturn) from the MissionPlanner class to the TraversibilityGraphEstimator class. This 
version of mplanner receives a graph from mapper instead of an rndf object. Need to run the whole loop to verify that the uturn is working correctly. 
(This version of mplanner determines the current lane and segment from state instead of getting it from tplanner so it's harder to run a unit test 
where tplanner fails such that a uturn is needed because the vehicle does not move from the initial position unless we actually run the whole 
planning loop.)

Release R1-00f (Thu Mar  1 22:31:53 2007):
	Nok's first attempt to untangle the mission planner. Moved the graph structure to the travgraph module and moved relevant functions to the 
travgraph and rndf modules.

Release R1-00e (Wed Feb 21 22:39:00 2007):
	Updated to work with the new Status.hh in revision R2-00b of interfaces

Release R1-00d (Tue Feb 20 17:17:42 2007):
	Modified mission planner so it compiles against revision R2-00a of interfaces and skynet

Release R1-00c (Thu Feb  1 20:34:35 2007):
	Modified mission planner so it takes into account hte 
orientation of the vehicle when it computes the closest 
waypoint.

Release R1-00b (Wed Jan 31 11:36:28 2007):
	Changed variables name so they are compatible with the changes 
in VehicleState.

Release R1-00a (Wed Jan 24 23:58:24 2007):
	Added files needed by mission planner.

Release R1-00 (Tue Jan 23 20:03:35 2007):
	Created.

























































































