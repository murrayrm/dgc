#ifndef MISSIONPLANNERINTERNALINTERFACES_HH
#define MISSIONPLANNERINTERNALINTERFACES_HH

#include "gcmodule/GcInterface.hh"
#include "interfaces/VehicleCapability.h"
#include "interfaces/sn_types.h"
using std::string;
using std::stringstream;

#define LEAST_NUM_MISSION_RPLANNER_STORED 3

// Directive from Mission Control
struct MControlDirective : public GcTransmissive
{
  enum DirectiveType{ NEXT_CHECKPOINT, END_OF_MISSION, PAUSE, DRIVE_AROUND, BACK_UP };

  unsigned goalID;
  DirectiveType type;

  // Parameters for NEXT_CHECKPOINT
  // The waypoint id of the next checkpoint
  int segmentID;
  int laneID;
  int waypointID;
  unsigned nextCheckpointIndex;

  // Parameters for DRIVE_AROUND and BACK_UP
  double distance;

  // Other parameters
  double maxSpeed;
  VehicleCapability vehicleCap;

  MControlDirective()
  {
    goalID = 0;
    type = PAUSE;
    segmentID = 0;
    laneID = 0;
    waypointID = 0;
    nextCheckpointIndex = 0;
    distance = 0;
    maxSpeed = 30;
    vehicleCap.intersectionRightTurn = 0;
    vehicleCap.intersectionLeftTurn = 0;
    vehicleCap.intersectionStraightTurn = 0;
    vehicleCap.uturn = 0;
    vehicleCap.nominalDriving = 0;
    vehicleCap.nominalStopping = 0;
    vehicleCap.nominalZoneRegionDriving = 0;
    vehicleCap.nominalNewRegionDriving = 0;
  }

  std::string toString() const {
    stringstream s("");
    s << "GoalId: " << goalID << ": ";
    if (type == NEXT_CHECKPOINT)
      s << "NEXT_CHECKPOINT (" << nextCheckpointIndex+1
	<< " = " << segmentID << "." << laneID
	<< "." << waypointID << ")";
    else if (type == END_OF_MISSION)
      s << "END_OF_MISSION";
    else if (type == PAUSE)
      s << "PAUSE";
    else if (type == DRIVE_AROUND)
      s << "DRIVE_AROUND " << distance << " m";
    else if (type == BACK_UP)
      s << "BACK_UP " << distance << " m";
    else
      s << "UNKNOWN";
    s << endl;
    return s.str();
  }

  unsigned getDirectiveId() const
  {
    return goalID;
  }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar & goalID;
    ar & type;
    ar & segmentID;
    ar & laneID;
    ar & waypointID;
    ar & distance;
    ar & maxSpeed;
    ar & nextCheckpointIndex;
    ar & vehicleCap;
  }
};


struct MControlDirectiveResponse : public GcInterfaceDirectiveStatus
{
  //  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ NO_ROUTE, VEHICLE_CAP, PREEMPTED_BY_PAUSE,
                         PREVIOUS_DIR_FAILURE};

  unsigned goalID;
  //Status status;
  ReasonForFailure reason;

  MControlDirectiveResponse()
  {
    goalID = 0;
    status = ACCEPTED;
  }
  virtual unsigned getDirectiveId() const
  {
    return goalID;
  }

  /*! Serialize function */
  friend class boost::serialization::access;

private:
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar & goalID;
    ar & status;
    ar & reason;
  }

  std::string toString() const {
    stringstream s("");
    s << "GoalId: " << goalID << " Status: "  << status 
      << " Reason: " << reason << endl;
    return s.str();
  }
};

typedef GcInterface<MControlDirective, MControlDirectiveResponse, 
		    GCM_IN_PROCESS_DIRECTIVE, GCM_IN_PROCESS_DIRECTIVE, 
		    MODmissionplanner> MRInterface ;


#endif //MISSIONPLANNERINTERNALINTERFACES_HH

