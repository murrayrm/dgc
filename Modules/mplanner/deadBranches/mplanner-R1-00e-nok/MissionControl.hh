/*!**
 * Nok Wongpiromsarn
 * February 21, 2007
 */


#ifndef MISSIONCONTROL_HH
#define MISSIONCONTROL_HH

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <deque>
#include "rndf/RNDF.hh"
#include "sparrowhawk/SparrowHawk.hh"

enum DirectiveName{ NEXT_CHECKPOINT, END_OF_MISSION, PAUSE };
enum VehicleCap{ MAX_CAP, MEDIUM_CAP, MIN_CAP };

struct MContrDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED };
  enum ReasonForFailure{ UNKNOWN, VEHICLE_CAP };
  MContrDirectiveStatus()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int goalID;
  Status status;
  ReasonForFailure reason;
};

struct MContrDirective
{
  MContrDirective()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int goalID;
  DirectiveName name;
  // The waypoint id of the next checkpoint
  int segmentID;
  int laneID;
  int waypointID;
  VehicleCap vehicleCap;
};


/**
 * MissionControl class.
 * This is a main class for the mission control part of the mission planner.
 * It is responsible for initialzing checkpoint series and keeping track of 
 * where we are in the mission. It also determines the minimum vehicle
 * capability for Alice to run.
 * \brief Main class for mission control part of mission planner
 */
class MissionControl
{
  /*! The structure for keeping information about directives sent to the
   *  route planner */
  struct Strategy
  {
    enum Status{ SENT, ACCEPTED, COMPLETED, FAILED };
    enum ReasonForFailure{ UNKNOWN, VEHICLE_CAP };
    Strategy()
    {
      // initialize the segment goals to zeros to appease the memory profilers
      memset(this, 0, sizeof(*this));
      this->status = SENT;
    }
    int goalID;
    DirectiveName name;
    int nextCheckpointIndex;
    VehicleCap vehicleCap;
    Status status;
    ReasonForFailure reason;
  };
 
  int DEBUG_LEVEL, VERBOSITY_LEVEL;

  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;

  /*!\param m_rndf is a pointer to the RNDF object. */
  RNDF* m_rndf;

  /*!\param m_checkpointSequence is the vector of waypoints which are
   * the checkpoints that we have to cross in order as specified by MDF */
  vector<Waypoint*> m_checkpointSequence;

  /*!\param m_nextCheckpointIndex is the index of the next checkpoint
   * in m_checkpointSequence. This is basically the current arbitrated 
   * directive. */
  int m_nextCheckpointIndex;

  /*! \param m_nextContrDirective is the next mission control directive. */
  MContrDirective m_nextContrDirective;

  /*!\param m_contrDirectiveQueue stores all the directives sent to the route planner. */
  deque<Strategy> m_contrDirectiveQueue;

public:
  /*! Constructor */
  MissionControl(RNDF* rndf, char* MDFFileName, bool nosparrow, int debugLevel, bool verbose);
      
  /*! Standard destructor */
  ~MissionControl();

  /*! Get the next mission control directive */
  MContrDirective getNextDirective();

  /*! Update the status of mission control directive */
  void updateDirectiveStatus(MContrDirectiveStatus directiveStatus);

private:
  /*! Arbitration for the mission control module. It basically loads MDF file
   *  and parses checkpoint series. It will put the vehicle in pause if
   *  the given MDF file is not valid. */
  void arbitrate(char* MDFFileName, RNDF* rndf);

  /*! Control for the mission control module. It is responsible for
   *  updating m_nextDirective, as the previous one is completed
   *  or failed. This includes determining the next checkpoint and 
   *  minimum vehicle capability for Alice to run. */

  // The control that computes directive based on the current arbitrated
  // directive only.
  void control();

  // The control that computes directive based on both the arbitrated
  // directive as well as status from the module below.
  void control(MContrDirectiveStatus directiveStatus);

  /*! A function that parses checkpoint series in MDF. */
  bool loadMDF(char* MDFFileName, RNDF* rndf);

  /*! A helper function for loadMDF. */
  void parseCheckpoint(ifstream*, RNDF*);

  /*! A function that converts Strategy to MContrDirective */
  MContrDirective strategy2ContrDirective(Strategy strategy);
};

#endif  //MISSIONCONTROL_HH
