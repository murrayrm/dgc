/*!
 * \file CmdArgs.cc
 * \brief Enables commandline arguments to be read by all parts of mplanner
 *
 * \author Nok Wongpiromsarn
 * \date 10 July 2007
 *
 */

#include "CmdArgs.hh"

/* shared ommand line arguments */
char* CmdArgs::spreadDaemon = NULL;
int CmdArgs::sn_key = 0;
bool CmdArgs::waitForStateFill = true;
bool CmdArgs::listenToMapper = true;
bool CmdArgs::nopause = false;
bool CmdArgs::noillegalPassing = false;
RNDF* CmdArgs::rndf = NULL;
char* CmdArgs::RNDFFileName = NULL;
char* CmdArgs::MDFFileName = NULL;
bool CmdArgs::getApxStatus = false;
bool CmdArgs::getSystemHealth = false;
bool CmdArgs::continueMission = false;
char* CmdArgs::missionFileName = NULL;
bool CmdArgs::listenToEstop = false;
bool CmdArgs::nosparrow = false;
bool CmdArgs::logData = false;
char* CmdArgs::logPath = NULL;
FILE* CmdArgs::logFile = NULL;
int CmdArgs::debugLevel = 0;
bool CmdArgs::verbose = false;
int CmdArgs::logLevel = 0;

