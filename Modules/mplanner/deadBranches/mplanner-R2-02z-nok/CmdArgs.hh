/*!
 * \file CmdArgs.hh
 * \brief Enables commandline arguments to be read by all parts of mplanner.
 *
 * \author Nok Wongpiromsarn
 * \date 10 July 2007
 *
 */

#ifndef CMDARGS_HH_36245789549870321762143
#define CMDARGS_HH_36245789549870321762143

#include <string>
#include <rndf/RNDF.hh>

using namespace std;
/*
 * \class CmdArgs
 *
 * This class holds shared command line arguments
 *
 */
class CmdArgs {
public:
  static char* spreadDaemon;
  static int sn_key;
  static bool waitForStateFill;
  static bool listenToMapper;
  static bool nopause;
  static bool noillegalPassing;
  static RNDF* rndf;
  static char* RNDFFileName;
  static char* MDFFileName;
  static bool getApxStatus;
  static bool getSystemHealth;
  static bool continueMission;
  static char* missionFileName;
  static bool nosparrow;
  static bool logData;
  static char* logPath;
  static FILE* logFile;
  static int debugLevel;
  static bool verbose;
  static int logLevel;
};

#endif
