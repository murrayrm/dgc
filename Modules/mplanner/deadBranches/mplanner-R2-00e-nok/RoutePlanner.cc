/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */


#include "RoutePlanner.hh"
#include "interfaces/sn_types.h"
#include "mplannerDisplay.h"

using namespace std;

RoutePlanner::RoutePlanner(int skynetKey, bool waitForStateFill, bool nosparrow,
    char* RNDFFileName, char* MDFFileName, bool listenToMapper, int debugLevel,
    bool verbose)
  : CSkynetContainer(MODmissionplanner, skynetKey), m_snKey(skynetKey), m_nosparrow(nosparrow), 
    m_graphEstimator(skynetKey,  waitForStateFill)
{  
  DEBUG_LEVEL = debugLevel;
  
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }

  m_segGoalsSocket = m_skynet.get_send_sock(SNsegGoals);

  DGCcreateMutex(&m_FirstSegGoalsStatusReceivedMutex);
  DGCcreateMutex(&m_SegGoalsStatusMutex);
  DGCcreateMutex(&m_contrDirectiveQMutex);
  DGCcreateMutex(&m_contrGcPortMutex);
  DGCcreateMutex(&m_newStatusMutex);
  DGCcreateCondition(&m_FirstSegGoalsStatusReceivedCond);

  m_rndf = new RNDF();
  if (!m_rndf->loadFile(RNDFFileName))
  {
    cerr << "Error:  Unable to load RNDF file " << RNDFFileName
	 << ", exiting program" << endl;
    exit(1);
  }
  m_rndf->assignLaneDirection();
    
  if (DEBUG_LEVEL > 2)
  {
    m_rndf->print();
    cout << "Finished rndf" << endl;
  }
  else if (DEBUG_LEVEL > 0)
  {
    for (int i=1; i <= m_rndf->getNumOfSegments(); i++)
    {
      for (int j=1; j <= m_rndf->getSegment(i)->getNumOfLanes(); j++)
      {
        cout << "Lane " << i << "." << j << ":\t\t"
             << m_rndf->getLane(i,j)->getDirection() << endl;
      }
    }
  }

  m_missionControl.init(m_rndf, MDFFileName, nosparrow, debugLevel, verbose);
  m_contrStatus.goalID = 0;
  m_contrStatus.status = RPlannerControlStatus::READY_FOR_NEXT;
  arbitrate(&m_contrStatus, &m_mergedDirective);

  m_graphEstimator.init(m_rndf, listenToMapper, nosparrow, debugLevel, verbose);
  m_travGraph = &m_graphEstimator.getGraph();

  m_minSpeedLimits.resize(m_travGraph->getNumOfSegments() + m_travGraph->getNumOfZones() + 1);
  m_maxSpeedLimits.resize(m_travGraph->getNumOfSegments() + m_travGraph->getNumOfZones() + 1);

  for (unsigned i=0; i < m_minSpeedLimits.size(); i++)
  {
    m_minSpeedLimits[i] = 0;
    m_maxSpeedLimits[i] = 0;
  }

  if (!loadMDFFile(MDFFileName, m_rndf))
  {
    cerr << "Error: Unable to load MDF file " << MDFFileName << ", exiting program"
         << endl;
    exit(1);
  }

  if (DEBUG_LEVEL > 1)
  {
    cout << endl << endl << "RNDFGRAPH:" << endl;
    m_travGraph->print();
    cout << "Finished RNDFGRAPH" << endl;
  }

  m_nextGoalID = 1;
  m_lastPlannedMergedDirectiveID = 0;

  m_receivedSegGoalsStatus = new SegGoalsStatus();
  m_numSegGoalsRequests = 0;

  // Initialize sparrow variables
  for (int i=0; i < NUM_SEGGOALS_DISPLAYED; i++)
  {
    sparrowNextGoalIDs[i] = 0;
    sparrowNextSegments1[i] = 0;
    sparrowNextLanes1[i] = 0;
    sparrowNextWaypoints1[i] = 0;
    sparrowNextSegments2[i] = 0;
    sparrowNextLanes2[i] = 0;
    sparrowNextWaypoints2[i] = 0;
  }

  m_bReceivedAtLeastOneSegGoalsStatus = false;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
RoutePlanner::~RoutePlanner() 
{
  delete m_rndf;
  delete m_travGraph;
  delete m_receivedSegGoalsStatus;
  delete shp;
  delete [] m_checkpointGoalID;
  delete [] sparrowNextGoalIDs;
  delete [] sparrowNextSegments1;
  delete [] sparrowNextLanes1;
  delete [] sparrowNextWaypoints1;
  delete [] sparrowNextSegments2;
  delete [] sparrowNextLanes2;
  delete [] sparrowNextWaypoints2;

  DGCdeleteMutex(&m_FirstSegGoalsStatusReceivedMutex);
  DGCdeleteMutex(&m_SegGoalsStatusMutex);
  DGCdeleteMutex(&m_contrDirectiveQMutex);
  DGCdeleteMutex(&m_contrGcPortMutex);
  DGCdeleteMutex(&m_newStatusMutex);
  DGCdeleteCondition(&m_FirstSegGoalsStatusReceivedCond);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::SparrowDisplayLoop()
{
  // SparrowHawk display
  CSparrowHawk &sh = SparrowHawk();  
  shp = &sh;
  
  sh.add_page(mplannertable, "Main");

  sh.rebind("snkey", &m_snKey);
  sh.rebind("tplanner_goalID", &(m_segGoalsStatus.goalID));
  sh.set_string("tplanner_status", "UNKNOWN");
  sh.set_readonly("snkey");
  sh.set_readonly("tplanner_goalID");
  sh.set_readonly("tplanner_status");

  sh.rebind("nextGoal1ID", &(sparrowNextGoalIDs[0]));
  sh.rebind("nextGoal1_segment1", &(sparrowNextSegments1[0]));
  sh.rebind("nextGoal1_lane1", &(sparrowNextLanes1[0]));
  sh.rebind("nextGoal1_waypoint1", &(sparrowNextWaypoints1[0]));
  sh.rebind("nextGoal1_segment2", &(sparrowNextSegments2[0]));
  sh.rebind("nextGoal1_lane2", &(sparrowNextLanes2[0]));
  sh.rebind("nextGoal1_waypoint2", &(sparrowNextWaypoints2[0]));
  sh.set_string("nextGoal1_type", "UNKNOWN");
  sh.rebind("minSpeed", &sparrowMinSpeed);
  sh.rebind("maxSpeed", &sparrowMaxSpeed);
  sh.rebind("illegalPassingAllowed", &sparrowIllegalPassingAllowed);
  sh.rebind("stopAtExit", &sparrowStopAtExit);
  sh.rebind("isExitCheckpoint", &sparrowIsExitCheckpoint);
  sh.set_readonly("nextGoal1ID");
  sh.set_readonly("nextGoal1_segment1");
  sh.set_readonly("nextGoal1_lane1");
  sh.set_readonly("nextGoal1_waypoint1");
  sh.set_readonly("nextGoal1_segment2");
  sh.set_readonly("nextGoal1_lane2");
  sh.set_readonly("nextGoal1_waypoint2");
  sh.set_readonly("nextGoal1_type");
  sh.set_readonly("minSpeed");
  sh.set_readonly("maxSpeed");
  sh.set_readonly("illegalPassingAllowed");
  sh.set_readonly("stopAtExit");
  sh.set_readonly("isExitCheckpoint");

  sh.rebind("nextGoal2ID", &(sparrowNextGoalIDs[1]));
  sh.rebind("nextGoal2_segment1", &(sparrowNextSegments1[1]));
  sh.rebind("nextGoal2_lane1", &(sparrowNextLanes1[1]));
  sh.rebind("nextGoal2_waypoint1", &(sparrowNextWaypoints1[1]));
  sh.rebind("nextGoal2_segment2", &(sparrowNextSegments2[1]));
  sh.rebind("nextGoal2_lane2", &(sparrowNextLanes2[1]));
  sh.rebind("nextGoal2_waypoint2", &(sparrowNextWaypoints2[1]));
  sh.set_string("nextGoal2_type", "UNKNOWN");
  sh.set_readonly("nextGoal2ID");
  sh.set_readonly("nextGoal2_segment1");
  sh.set_readonly("nextGoal2_lane1");
  sh.set_readonly("nextGoal2_waypoint1");
  sh.set_readonly("nextGoal2_segment2");
  sh.set_readonly("nextGoal2_lane2");
  sh.set_readonly("nextGoal2_waypoint2");
  sh.set_readonly("nextGoal2_type");

  sh.rebind("nextGoal3ID", &(sparrowNextGoalIDs[2]));
  sh.rebind("nextGoal3_segment1", &(sparrowNextSegments1[2]));
  sh.rebind("nextGoal3_lane1", &(sparrowNextLanes1[2]));
  sh.rebind("nextGoal3_waypoint1", &(sparrowNextWaypoints1[2]));
  sh.rebind("nextGoal3_segment2", &(sparrowNextSegments2[2]));
  sh.rebind("nextGoal3_lane2", &(sparrowNextLanes2[2]));
  sh.rebind("nextGoal3_waypoint2", &(sparrowNextWaypoints2[2]));
  sh.set_string("nextGoal3_type", "UNKNOWN");
  sh.set_readonly("nextGoal3ID");
  sh.set_readonly("nextGoal3_segment1");
  sh.set_readonly("nextGoal3_lane1");
  sh.set_readonly("nextGoal3_waypoint1");
  sh.set_readonly("nextGoal3_segment2");
  sh.set_readonly("nextGoal3_lane2");
  sh.set_readonly("nextGoal3_waypoint2");
  sh.set_readonly("nextGoal3_type");

  sh.rebind("nextGoal4ID", &(sparrowNextGoalIDs[3]));
  sh.rebind("nextGoal4_segment1", &(sparrowNextSegments1[3]));
  sh.rebind("nextGoal4_lane1", &(sparrowNextLanes1[3]));
  sh.rebind("nextGoal4_waypoint1", &(sparrowNextWaypoints1[3]));
  sh.rebind("nextGoal4_segment2", &(sparrowNextSegments2[3]));
  sh.rebind("nextGoal4_lane2", &(sparrowNextLanes2[3]));
  sh.rebind("nextGoal4_waypoint2", &(sparrowNextWaypoints2[3]));
  sh.set_string("nextGoal4_type", "UNKNOWN");
  sh.set_readonly("nextGoal4ID");
  sh.set_readonly("nextGoal4_segment1");
  sh.set_readonly("nextGoal4_lane1");
  sh.set_readonly("nextGoal4_waypoint1");
  sh.set_readonly("nextGoal4_segment2");
  sh.set_readonly("nextGoal4_lane2");
  sh.set_readonly("nextGoal4_waypoint2");
  sh.set_readonly("nextGoal4_type");

  sh.rebind("nextGoal5ID", &(sparrowNextGoalIDs[4]));
  sh.rebind("nextGoal5_segment1", &(sparrowNextSegments1[4]));
  sh.rebind("nextGoal5_lane1", &(sparrowNextLanes1[4]));
  sh.rebind("nextGoal5_waypoint1", &(sparrowNextWaypoints1[4]));
  sh.rebind("nextGoal5_segment2", &(sparrowNextSegments2[4]));
  sh.rebind("nextGoal5_lane2", &(sparrowNextLanes2[4]));
  sh.rebind("nextGoal5_waypoint2", &(sparrowNextWaypoints2[4]));
  sh.set_string("nextGoal5_type", "UNKNOWN");
  sh.set_readonly("nextGoal5ID");
  sh.set_readonly("nextGoal5_segment1");
  sh.set_readonly("nextGoal5_lane1");
  sh.set_readonly("nextGoal5_waypoint1");
  sh.set_readonly("nextGoal5_segment2");
  sh.set_readonly("nextGoal5_lane2");
  sh.set_readonly("nextGoal5_waypoint2");
  sh.set_readonly("nextGoal5_type");

  sh.rebind("nextGoal6ID", &(sparrowNextGoalIDs[5]));
  sh.rebind("nextGoal6_segment1", &(sparrowNextSegments1[5]));
  sh.rebind("nextGoal6_lane1", &(sparrowNextLanes1[5]));
  sh.rebind("nextGoal6_waypoint1", &(sparrowNextWaypoints1[5]));
  sh.rebind("nextGoal6_segment2", &(sparrowNextSegments2[5]));
  sh.rebind("nextGoal6_lane2", &(sparrowNextLanes2[5]));
  sh.rebind("nextGoal6_waypoint2", &(sparrowNextWaypoints2[5]));
  sh.set_string("nextGoal6_type", "UNKNOWN");
  sh.set_readonly("nextGoal6ID");
  sh.set_readonly("nextGoal6_segment1");
  sh.set_readonly("nextGoal6_lane1");
  sh.set_readonly("nextGoal6_waypoint1");
  sh.set_readonly("nextGoal6_segment2");
  sh.set_readonly("nextGoal6_lane2");
  sh.set_readonly("nextGoal6_waypoint2");
  sh.set_readonly("nextGoal6_type");

  sh.rebind("nextCheckpointIndex", &m_missionID);
  sh.rebind("nextCheckpointSegmentID", &m_currentCkptSegmentID);
  sh.rebind("nextCheckpointLaneID", &m_currentCkptLaneID);
  sh.rebind("nextCheckpointWaypointID", &m_currentCkptWaypointID);
  sh.rebind("nextCheckpointGoalID", &m_prevMergedDirectiveGoalID);
  sh.set_readonly("nextCheckpointIndex");
  sh.set_readonly("nextCheckpointSegmentID");
  sh.set_readonly("nextCheckpointLaneID");
  sh.set_readonly("nextCheckpointWaypointID");
  sh.set_readonly("nextCheckpointGoalID");
  

  sh.rebind("obstacleID", &obstacleID);
  sh.rebind("obstacleSegmentID", &obstacleSegmentID);
  sh.rebind("obstacleLaneID", &obstacleLaneID);
  sh.rebind("obstacleWaypointID", &obstacleWaypointID);

  sh.set_notify("sparrowInsertObstacle", this, &RoutePlanner::sparrowInsertObstacle);
  sh.run();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::UpdateSparrowVariablesLoop()
{
  while(true)
  {
    deque<RPlannerContrMsgWrapper> executingGoals;
    DGClockMutex(&m_contrGcPortMutex);	
    if (m_contrGcPortMsgQ.size() > (unsigned)m_firstQueuedMsgIndex)
    {
      executingGoals.assign(m_contrGcPortMsgQ.begin() + m_firstQueuedMsgIndex, m_contrGcPortMsgQ.end());
      sparrowMinSpeed = executingGoals.front().directive.minSpeedLimit;
      sparrowMaxSpeed = executingGoals.front().directive.maxSpeedLimit;
      sparrowIllegalPassingAllowed = executingGoals.front().directive.illegalPassingAllowed;
      sparrowStopAtExit = executingGoals.front().directive.stopAtExit;
      sparrowIsExitCheckpoint = executingGoals.front().directive.isExitCheckpoint;
    }
    DGCunlockMutex(&m_contrGcPortMutex);	

    if (m_segGoalsStatus.status == SegGoalsStatus::ACCEPT)
    {
      shp->set_string("tplanner_status", "ACCEPT   ");
    }
    else if (m_segGoalsStatus.status == SegGoalsStatus::REJECT)
    {
      shp->set_string("tplanner_status", "REJECT   ");
    }
    else if (m_segGoalsStatus.status == SegGoalsStatus::COMPLETED)
    {
      shp->set_string("tplanner_status", "COMPLETED");
    }
    else if (m_segGoalsStatus.status == SegGoalsStatus::FAILED)
    {
      shp->set_string("tplanner_status", "FAILED   ");
    }
    else
    {
      shp->set_string("tplanner_status", "UNKNOWN  ");
    }

    int i = 0;
    deque<SegGoals> segGoalsSeq;	
    DGClockMutex(&m_contrDirectiveQMutex);
    if (m_contrDirectiveQ.size() > 0)
    {
      segGoalsSeq.assign(m_contrDirectiveQ.begin(), m_contrDirectiveQ.end());
    }
    DGCunlockMutex(&m_contrDirectiveQMutex);

    char* nextGoalType;

    while (i < NUM_SEGGOALS_DISPLAYED && executingGoals.size() > 0)
    {
      sparrowNextGoalIDs[i] = (executingGoals.front()).directive.goalID;
      sparrowNextSegments1[i] = (executingGoals.front()).directive.entrySegmentID;
      sparrowNextLanes1[i] = (executingGoals.front()).directive.entryLaneID;
      sparrowNextWaypoints1[i] = (executingGoals.front()).directive.entryWaypointID;
      sparrowNextSegments2[i] = (executingGoals.front()).directive.exitSegmentID;
      sparrowNextLanes2[i] = (executingGoals.front()).directive.exitLaneID;
      sparrowNextWaypoints2[i] = (executingGoals.front()).directive.exitWaypointID;

      switch (i)
      {
        case 0:
          nextGoalType = "nextGoal1_type";
          break;
        case 1:
          nextGoalType = "nextGoal2_type";
          break;
        case 2:
          nextGoalType = "nextGoal3_type";
          break;
        case 3:
          nextGoalType = "nextGoal4_type";
          break;
        case 4:
          nextGoalType = "nextGoal5_type";
          break;
        default:
          nextGoalType = "nextGoal6_type";
      }

      if ((executingGoals.front()).directive.segment_type == SegGoals::ROAD_SEGMENT)
      {
        shp->set_string(nextGoalType, "ROAD_SEGMENT");
      }
      else if ((executingGoals.front()).directive.segment_type == SegGoals::PARKING_ZONE)
      {
        shp->set_string(nextGoalType, "PARKING_ZONE");
      }
      else if ((executingGoals.front()).directive.segment_type == SegGoals::INTERSECTION)
      {
        shp->set_string(nextGoalType, "INTERSECTION");
      }
      else if ((executingGoals.front()).directive.segment_type == SegGoals::PREZONE)
      {
        shp->set_string(nextGoalType, "PREZONE     ");
      }
      else if ((executingGoals.front()).directive.segment_type == SegGoals::UTURN)
      {
        shp->set_string(nextGoalType, "UTURN       ");
      }
      else if ((executingGoals.front()).directive.segment_type == SegGoals::PAUSE)
      {
        shp->set_string(nextGoalType, "PAUSE       ");
      }
      else if ((executingGoals.front()).directive.segment_type == SegGoals::END_OF_MISSION)
      {
        shp->set_string(nextGoalType, "END_OF_MISSION");
      }
      else
      {
        shp->set_string(nextGoalType, "UNKNOWN");
      }

      executingGoals.pop_front();
      i++;
    }
    
    while (i < NUM_SEGGOALS_DISPLAYED && segGoalsSeq.size() > 0)
    {
      sparrowNextGoalIDs[i] = (segGoalsSeq.front()).goalID;
      sparrowNextSegments1[i] = (segGoalsSeq.front()).entrySegmentID;
      sparrowNextLanes1[i] = (segGoalsSeq.front()).entryLaneID;
      sparrowNextWaypoints1[i] = (segGoalsSeq.front()).entryWaypointID;
      sparrowNextSegments2[i] = (segGoalsSeq.front()).exitSegmentID;
      sparrowNextLanes2[i] = (segGoalsSeq.front()).exitLaneID;
      sparrowNextWaypoints2[i] = (segGoalsSeq.front()).exitWaypointID;

      switch (i)
      {
        case 0:
          nextGoalType = "nextGoal1_type";
          break;
        case 1:
          nextGoalType = "nextGoal2_type";
          break;
        case 2:
          nextGoalType = "nextGoal3_type";
          break;
        case 3:
          nextGoalType = "nextGoal4_type";
          break;
        case 4:
          nextGoalType = "nextGoal5_type";
          break;
        default:
          nextGoalType = "nextGoal6_type";
      }

      if ((segGoalsSeq.front()).segment_type == SegGoals::ROAD_SEGMENT)
      {
        shp->set_string(nextGoalType, "ROAD_SEGMENT");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::PARKING_ZONE)
      {
        shp->set_string(nextGoalType, "PARKING_ZONE");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::INTERSECTION)
      {
        shp->set_string(nextGoalType, "INTERSECTION");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::PREZONE)
      {
        shp->set_string(nextGoalType, "PREZONE     ");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::UTURN)
      {
        shp->set_string(nextGoalType, "UTURN       ");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::PAUSE)
      {
        shp->set_string(nextGoalType, "PAUSE       ");
      }
      else if ((segGoalsSeq.front()).segment_type == SegGoals::END_OF_MISSION)
      {
        shp->set_string(nextGoalType, "END_OF_MISSION");
      }
      else
      {
        shp->set_string(nextGoalType, "UNKNOWN");
      }

      segGoalsSeq.pop_front();
      i++;
    }
    usleep(500000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sparrowInsertObstacle()
{  
  m_graphEstimator.insertObstacle(obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::getTPlannerStatusThread()
{
  // The skynet socket for receiving tplanner status
  int tplannerStatusSocket = m_skynet.listen(SNtplannerStatus, MODtrafficplanner);
  if(tplannerStatusSocket < 0)
  {
    cerr << "MissionPlanner::getTPlannerStatusThread(): skynet listen returned error"
         << endl;
    exit(1);
  }

  while(true)
  {
    bool tPlannerStatusReceived =
        RecvSegGoalsStatus(tplannerStatusSocket, m_receivedSegGoalsStatus);
        
    if (tPlannerStatusReceived)
    {
      if (m_nosparrow)
      {
        cout << "Goal " << m_receivedSegGoalsStatus->goalID << " status: "
             << m_receivedSegGoalsStatus->status << endl;
      }
      else
      {
        SparrowHawk().log("Goal %d status %d\n", m_receivedSegGoalsStatus->goalID,
            (int) m_receivedSegGoalsStatus->status);
      }

      if (m_receivedSegGoalsStatus->goalID == 0)
      {
        m_numSegGoalsRequests++;
      }
      else
      {
	unsigned i = 0;
	bool directiveFound = false;
	DGClockMutex(&m_contrGcPortMutex);
	while( i < m_contrGcPortMsgQ.size() && !directiveFound)
	{
	  if(m_contrGcPortMsgQ[i].directive.goalID == m_receivedSegGoalsStatus->goalID)
	  {
	    directiveFound = true;
	    if (m_receivedSegGoalsStatus->status == SegGoalsStatus::ACCEPT)
	    {
	      m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::ACCEPTED;
	    }
	    else if (m_receivedSegGoalsStatus->status == SegGoalsStatus::REJECT ||
		     m_receivedSegGoalsStatus->status == SegGoalsStatus::FAILED)
	    {
	      m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::FAILED;
	      m_contrGcPortMsgQ[i].response.reason = m_receivedSegGoalsStatus->reason;
	    }
	    else if (m_receivedSegGoalsStatus->status == SegGoalsStatus::COMPLETED)
	    {
	      m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::COMPLETED;
	    }
	    else
	    {
	      cerr << "ERROR: getTPlannerStatusThread: Received unknown status "
		   << m_receivedSegGoalsStatus->status << endl;
	    }
	  }
	  i++;
	}
	DGCunlockMutex(&m_contrGcPortMutex);
	
	if (!directiveFound)
	{
	  cerr << "ERROR: getTPlannerStatusThread: Cannot find goalID " 
	       << m_receivedSegGoalsStatus->goalID << " in contrGcPort" << endl;
	}
      }
      
      DGClockMutex(&m_SegGoalsStatusMutex);
      m_segGoalsStatus = *m_receivedSegGoalsStatus;
      DGCunlockMutex(&m_SegGoalsStatusMutex);

      if (!m_bReceivedAtLeastOneSegGoalsStatus)
      {
        DGCSetConditionTrue(m_bReceivedAtLeastOneSegGoalsStatus,
            m_FirstSegGoalsStatusReceivedCond, m_FirstSegGoalsStatusReceivedMutex);
      }

      DGClockMutex(&m_newStatusMutex);
      m_newStatus = true;
      DGCunlockMutex(&m_newStatusMutex);
    }
    usleep(100000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sendSegGoalsThread()
{
  int numSegGoalsRequests = 0;
  if (m_nosparrow)
  {
    cout << "sendSegGoalsThread(): Waiting for traffic planner" << endl;
  }
  else
  {
    SparrowHawk().log("sendSegGoalsThread(): Waiting for traffic planner\n");
  }

  // don't send goals until traffic planner starts listening
  DGCWaitForConditionTrue(m_bReceivedAtLeastOneSegGoalsStatus,
      m_FirstSegGoalsStatusReceivedCond, m_FirstSegGoalsStatusReceivedMutex);

  if (m_nosparrow)
  {
    cout << "Starting sendSegGoalsThread" << endl;
  }
  else
  {
    SparrowHawk().log("Starting sendSegGoalsThread\n");
  }

  while(true)
  {   
    DGClockMutex(&m_SegGoalsStatusMutex);
    int lastGoalID = m_segGoalsStatus.goalID;
    DGCunlockMutex(&m_SegGoalsStatusMutex);
    
    DGClockMutex(&m_contrGcPortMutex);

    // If the tplanner just restarts, reset the status for goals previously sent to QUEUED.
    if (lastGoalID == 0 && numSegGoalsRequests < m_numSegGoalsRequests)
    {
      for (unsigned i=0; i < m_contrGcPortMsgQ.size(); i++)
      {
	if (m_contrGcPortMsgQ[i].response.status == RPlannerContrMsgResponse::SENT)
	{
	  m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::QUEUED;
	}
      }
      numSegGoalsRequests++;
    }

    // Send all the messages with status QUEUED
    unsigned firstQueuedMsgIndex = 0;
    bool firstQueuedMsgFound = false;
    bool m_firstQueuedMsgFound = false;
    while (firstQueuedMsgIndex < m_contrGcPortMsgQ.size() && !firstQueuedMsgFound)
    {
      if (m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::QUEUED ||
	  m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::SENT ||
	  m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::ACCEPTED)
      {
	if (!m_firstQueuedMsgFound)
	{
	  m_firstQueuedMsgFound = true;
	  m_firstQueuedMsgIndex = firstQueuedMsgIndex;
	}
	if (m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::QUEUED)
	{
	  firstQueuedMsgFound = true;
	}
	else
	{
	  firstQueuedMsgIndex++;
	}
      }
      else
      {
	firstQueuedMsgIndex++;
      }
    }

    if (firstQueuedMsgFound)
    {
      for (unsigned i = firstQueuedMsgIndex; i < m_contrGcPortMsgQ.size(); i++)
      {
	if (m_nosparrow)
	{
	  cout << "Sending goal " << (m_contrGcPortMsgQ[i]).directive.goalID << " to tplanner." << endl;
	}
	else
	{
	  SparrowHawk().log("Sending goal %d to tplanner\n", (m_contrGcPortMsgQ[i]).directive.goalID);
	}
	SendSegGoals(m_segGoalsSocket, &(m_contrGcPortMsgQ[i].directive));
	m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::SENT;
      }
    }
    DGCunlockMutex(&m_contrGcPortMutex);	
    usleep(100000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::MPlanningLoop(void)
{
  while(true)
  {
    arbitrate(&m_contrStatus, &m_mergedDirective);
    control(&m_contrStatus, &m_mergedDirective);
    usleep(100000);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::arbitrate(RPlannerControlStatus* contrStatus, RPlannerMergedDirective* mergedDir)
{
  if (contrStatus->status == RPlannerControlStatus::EXECUTING)
  {
    return;
  }

  if (DEBUG_LEVEL > 1)
  {
    if (m_nosparrow)
    {
      cout << endl << "Control status: goalID = " << contrStatus->goalID << "  status = " 
	   << contrStatus->status << endl;
    }
    else
    {
      SparrowHawk().log("Control status: goalID = %d   status = %d\n", contrStatus->goalID, contrStatus->status);
    }
  }

  if (contrStatus->goalID != 0)
  {
    m_missionControl.updateDirectiveStatus(*contrStatus);
  }
	   
  *mergedDir = m_missionControl.getNextDirective();

  if (DEBUG_LEVEL > 1)
  {
    if (m_nosparrow)
    {
      cout << "Merged directive: goal ID = " << mergedDir->goalID; 
      if (mergedDir->name == NEXT_CHECKPOINT)
      {
	cout << " NEXT_CHECKPOINT = " << mergedDir->segmentID << "." << mergedDir->laneID << "."
	     << mergedDir->waypointID << endl;
      }
      else if (mergedDir->name == END_OF_MISSION)
      {
	cout << " END_OF_MISSION" << endl;
      }
      else
      {
	cout << " PAUSE" << endl;
      }
    }
    else
    {
      SparrowHawk().log("Merged directive: goal ID = %d", mergedDir->goalID);
      if (mergedDir->name == NEXT_CHECKPOINT)
      {
	SparrowHawk().log(" NEXT_CHECKPOINT = %d.%d.%d\n", mergedDir->segmentID, mergedDir->laneID,
			  mergedDir->waypointID);
      }
      else if (mergedDir->name == END_OF_MISSION)
      {
	SparrowHawk().log("END_OF_MISSION\n");
      }
      else
      {
	SparrowHawk().log(" PAUSE\n");
      }
    }
  }

  if (contrStatus->goalID == 0)
  {
    m_prevMergedDirective = *mergedDir;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::control(RPlannerControlStatus* contrStatus, RPlannerMergedDirective* mergedDir)
{
  #warning RoutePlanner cannot handle PAUSE command yet.
  bool replan = false;

  m_currentCkptSegmentID = m_prevMergedDirective.segmentID;
  m_currentCkptLaneID = m_prevMergedDirective.laneID;
  m_currentCkptWaypointID = m_prevMergedDirective.waypointID;  
  if (m_prevMergedDirective.goalID != mergedDir->goalID)
  {
    m_missionID = m_missionControl.getNextCheckpointIndex();
  }
  else
  {
    m_missionID = m_missionControl.getNextCheckpointIndex() + 1;
  }


  // First see if the previous merged directive is completed.
  if (isCompleted(m_prevMergedDirectiveGoalID))
  {
    contrStatus->goalID = m_prevMergedDirective.goalID;
    contrStatus->status = RPlannerControlStatus::COMPLETED;

    m_prevMergedDirective = *mergedDir;
    m_prevMergedDirectiveGoalID = m_mergedDirectiveGoalID;

    m_currentCkptSegmentID = m_prevMergedDirective.segmentID;
    m_currentCkptLaneID = m_prevMergedDirective.laneID;
    m_currentCkptWaypointID = m_prevMergedDirective.waypointID;

    return;
  }

  // plan from current position if we're not in run
  // plan from previous plan if we're in run and at least one plan came through

  // if we're not in RUN, plan from veh state
  /*
  if(m_actuatorState.m_estoppos != RUN)
  {
    replan = true;
    if (m_nosparrow)
      cout << "I'm not in RUN" <<endl;
    else
      SparrowHawk().log("I'm not in RUN\n");
  }
  else
*/

  RPlannerContrMsgWrapper segGoalsStatus;

  if (gotNewStatus())
  {
    segGoalsStatus = getLatestStatusChange();
    if(m_bReceivedAtLeastOneSegGoalsStatus && 
       segGoalsStatus.response.status == RPlannerContrMsgResponse::FAILED)
    {
      replan = true;
      if (m_nosparrow)
      {
	cout << endl << "Goal " << segGoalsStatus.directive.goalID << " failed : Replanning" << endl;
      }
      else
      {
	SparrowHawk().log("Goal %d failed : Replanning\n", segGoalsStatus.directive.goalID);
      }
    }
    else if (m_bReceivedAtLeastOneSegGoalsStatus && 
	     segGoalsStatus.response.status == RPlannerContrMsgResponse::COMPLETED)
    {
      replan = false;

      DGClockMutex(&m_contrDirectiveQMutex);
      // Add directives to contrGcPort
      while( m_contrDirectiveQ.size() > 0 && segGoalsStatus.directive.goalID + LEAST_NUM_SEGGOALS_TPLANNER_STORED >= 
	(m_contrDirectiveQ.front()).goalID )
      {
	if (DEBUG_LEVEL > 0)
        {
	  if (m_nosparrow)
	  {
	    cout << "Adding goal " << (m_contrDirectiveQ.front()).goalID << " to contrGcPortMsgQ." << endl;
	  }
	  else
	  {
	    SparrowHawk().log("Sending goal %d to contrGcPortMsgQ\n", (m_contrDirectiveQ.front()).goalID);
	  }
	}
	addDirectiveToGcPort(m_contrDirectiveQ.front());
	m_contrDirectiveQ.pop_front();
      }
      DGCunlockMutex(&m_contrDirectiveQMutex);
    }
  }
  else if ( m_prevMergedDirective.goalID > m_lastPlannedMergedDirectiveID)
  {
    segGoalsStatus.directive.goalID = 0;
    replan = true;
  }

  DGClockMutex(&m_contrDirectiveQMutex);
  int numSegGoalsStored = m_contrDirectiveQ.size();
  DGCunlockMutex(&m_contrDirectiveQMutex);
    
  // Compute new goals starting from current position or from last waypoint
  // (given by the traffic planner) if failure occurs.    
  if (replan && m_prevMergedDirective.name == NEXT_CHECKPOINT)
  {
    // First, get an updated graph
    m_graphEstimator.updateGraph(true); 
    m_travGraph = &m_graphEstimator.getGraph();
          
    m_nextGoalID = segGoalsStatus.directive.goalID;
    if (m_nextGoalID == 0)
    {
      m_nextGoalID = 1;
    }

    resetGoals();
        
    int missionID = m_missionControl.getNextCheckpointIndex() + 1;

    if (m_prevMergedDirective.goalID != mergedDir->goalID)
    {
      missionID--;
    }
    
    // Plan from current position
    vector<SegGoals> segGoalsSeq = planSegGoals(0, 0, 0, m_prevMergedDirective.segmentID,
						m_prevMergedDirective.laneID, m_prevMergedDirective.waypointID,
						missionID, false);
    
    DGClockMutex(&m_contrDirectiveQMutex);
    int startPrintingIndex = (int)m_contrDirectiveQ.size();

    // Add directives to control queue
    for(unsigned i = 0; i < segGoalsSeq.size(); i++)
    {
      m_contrDirectiveQ.push_back(segGoalsSeq[i]);
    }  
    
    // Update goalID that corresponds to completion of previous merged directive.
    if (m_contrDirectiveQ.size() > 0)
    {
      m_prevMergedDirectiveGoalID = (m_contrDirectiveQ.back()).goalID;
      m_nextGoalID = (m_contrDirectiveQ.back()).goalID + 1;
      m_lastPlannedMergedDirectiveID = m_prevMergedDirective.goalID;
    }
    
    // Print out the mission
    if (m_nosparrow)
    {
      printMission(m_contrDirectiveQ, startPrintingIndex);
      cout<<endl;  
    }
    else
    {
      printMissionOnSparrow(m_contrDirectiveQ, startPrintingIndex);
    }

    // Add directives to contrGcPort
    int numDirectivesAdded = 0;
    while( m_contrDirectiveQ.size() > 0 && numDirectivesAdded < LEAST_NUM_SEGGOALS_TPLANNER_STORED )
    {
      if (DEBUG_LEVEL > 0)
      {
	if (m_nosparrow)
	{
	  cout << "Adding goal " << (m_contrDirectiveQ.front()).goalID << " to contrGcPortMsgQ." << endl;
	}
	else
	{
	  SparrowHawk().log("Sending goal %d to contrGcPortMsgQ\n", (m_contrDirectiveQ.front()).goalID);
	}
      }
      addDirectiveToGcPort(m_contrDirectiveQ.front());
      m_contrDirectiveQ.pop_front();
      numDirectivesAdded++;
    }
    
    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirective.goalID;
    contrStatus->status = RPlannerControlStatus::EXECUTING;

    DGCunlockMutex(&m_contrDirectiveQMutex);
    return;
  }

  // Notify arbiter that we're ready for the next directive from Mission Control
  else if (numSegGoalsStored < LEAST_NUM_SEGGOALS_STORED && 
	   m_prevMergedDirective.goalID == mergedDir->goalID)
  {
    // Update controlStatus
    contrStatus->goalID = mergedDir->goalID;
    contrStatus->status = RPlannerControlStatus::READY_FOR_NEXT;
    return;
  }
  
  else if (numSegGoalsStored < LEAST_NUM_SEGGOALS_STORED  && 
	   mergedDir->name == NEXT_CHECKPOINT && mergedDir->goalID > m_lastPlannedMergedDirectiveID )
  {
    int missionID = m_missionControl.getNextCheckpointIndex() + 1;
    vector<SegGoals> segGoalsSeq = planSegGoals(m_prevMergedDirective.segmentID,
						m_prevMergedDirective.laneID,
						m_prevMergedDirective.waypointID,
						mergedDir->segmentID,
						mergedDir->laneID,
						mergedDir->waypointID,
						missionID, false);

    DGClockMutex(&m_contrDirectiveQMutex);
    int startPrintingIndex = (int)m_contrDirectiveQ.size(); 

    // Add directives to control queue     
    for(unsigned i = 0; i < segGoalsSeq.size(); i++)
    {
      m_contrDirectiveQ.push_back(segGoalsSeq[i]);
    }      
    
    // Update goalID that corresponds to completion of current merged directive.
    if (m_contrDirectiveQ.size() > 0)
    {
      m_mergedDirectiveGoalID = (m_contrDirectiveQ.back()).goalID;
      m_nextGoalID = (m_contrDirectiveQ.back()).goalID + 1;
      m_lastPlannedMergedDirectiveID = mergedDir->goalID;
    }
    
    // Print out the mission
    if (m_nosparrow)
    {
      printMission(m_contrDirectiveQ, startPrintingIndex);
      cout << endl;  
    }
    else
    {
      printMissionOnSparrow(m_contrDirectiveQ, startPrintingIndex);
    }

    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirective.goalID;
    contrStatus->status = RPlannerControlStatus::EXECUTING;

    DGCunlockMutex(&m_contrDirectiveQMutex);
    return;
  }
  else if (numSegGoalsStored < LEAST_NUM_SEGGOALS_STORED 
	   && mergedDir->name == END_OF_MISSION && mergedDir->goalID > m_lastPlannedMergedDirectiveID )
  {
    SegGoals endOfMission;
    endOfMission.goalID = m_nextGoalID;
    endOfMission.entrySegmentID = 0;
    endOfMission.entryLaneID = 0;
    endOfMission.entryWaypointID = 0;
    endOfMission.exitSegmentID = 0;
    endOfMission.exitLaneID = 0;
    endOfMission.exitWaypointID = 0;
    endOfMission.minSpeedLimit = 0;
    endOfMission.maxSpeedLimit = 0;
    endOfMission.segment_type = SegGoals::END_OF_MISSION;
    endOfMission.illegalPassingAllowed = false;
    endOfMission.stopAtExit = true;
    endOfMission.isExitCheckpoint = false;

    DGClockMutex(&m_contrDirectiveQMutex);
    int startPrintingIndex = (int)m_contrDirectiveQ.size(); 

    // Add directives to control queue     
    m_contrDirectiveQ.push_back(endOfMission);
    m_mergedDirectiveGoalID = (m_contrDirectiveQ.back()).goalID;
    
    // Update goalID that corresponds to completion of current merged directive.
    if (m_contrDirectiveQ.size() > 0)
    {
      m_mergedDirectiveGoalID = (m_contrDirectiveQ.back()).goalID;
      m_nextGoalID = (m_contrDirectiveQ.back()).goalID + 1;
      m_lastPlannedMergedDirectiveID = mergedDir->goalID;
    }
    
    // Print out the mission
    if (m_nosparrow)
    {
      printMission(m_contrDirectiveQ, startPrintingIndex);
      cout << endl;  
    }
    else
    {
      printMissionOnSparrow(m_contrDirectiveQ, startPrintingIndex);
    }

    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirective.goalID;
    contrStatus->status = RPlannerControlStatus::EXECUTING;

    DGCunlockMutex(&m_contrDirectiveQMutex);
    return;
  }
  else
  {
    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirective.goalID;
    contrStatus->status = RPlannerControlStatus::EXECUTING;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool RoutePlanner::isCompleted(int goalID)
{
  if (goalID == 0)
  {
    return false;
  }
  DGClockMutex(&m_contrGcPortMutex);
  for (unsigned i=0; i<m_contrGcPortMsgQ.size(); i++)
  {
    if(m_contrGcPortMsgQ[i].directive.goalID == goalID)
    {
      if (m_contrGcPortMsgQ[i].response.status == RPlannerContrMsgResponse::COMPLETED)
      {
	DGCunlockMutex(&m_contrGcPortMutex);
	return true;
      }
      else
      {
	DGCunlockMutex(&m_contrGcPortMutex);
	return false;
      }
    }
  }

  DGCunlockMutex(&m_contrGcPortMutex);
  return false;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
RPlannerContrMsgResponse RoutePlanner::getResponse(int goalID)
{
  RPlannerContrMsgResponse response;
  response.status = RPlannerContrMsgResponse::QUEUED;

  DGClockMutex(&m_contrGcPortMutex);
  for (unsigned i=0; i<m_contrGcPortMsgQ.size(); i++)
  {
    if(m_contrGcPortMsgQ[i].directive.goalID == goalID)
    {
      response = m_contrGcPortMsgQ[i].response;
      DGCunlockMutex(&m_contrGcPortMutex);
      return response;
    }
  }
  DGCunlockMutex(&m_contrGcPortMutex);

  
  cerr << "ERROR: getResponse: Cannot find goalID " << goalID << " in contrGcPort" << endl;
  return response;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool RoutePlanner::gotNewStatus()
{
  DGClockMutex(&m_newStatusMutex);
  bool newStatusReceived = m_newStatus;
  DGCunlockMutex(&m_newStatusMutex);
  return newStatusReceived;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
RPlannerContrMsgWrapper RoutePlanner::getLatestStatusChange()
{
  RPlannerContrMsgWrapper status;
  status.directive.goalID = 0;

  DGClockMutex(&m_SegGoalsStatusMutex);
  DGClockMutex(&m_contrGcPortMutex);	
  for (unsigned i=0; i<m_contrGcPortMsgQ.size(); i++)
  {
    if (m_segGoalsStatus.goalID == m_contrGcPortMsgQ[i].directive.goalID)
    {
      status = m_contrGcPortMsgQ[i];
      DGCunlockMutex(&m_contrGcPortMutex);
      DGCunlockMutex(&m_SegGoalsStatusMutex);  
      return status;
    }
  }

  if (m_segGoalsStatus.goalID != 0)
  {
    cerr << "ERROR: getLatestStatusChange: Cannot find goalID" << m_segGoalsStatus.goalID
	 << " in contrGcPort." << endl;
  }
  DGCunlockMutex(&m_contrGcPortMutex);
  DGCunlockMutex(&m_SegGoalsStatusMutex);

  DGClockMutex(&m_newStatusMutex);
  m_newStatus = false;
  DGCunlockMutex(&m_newStatusMutex);
  return status;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::requestGloNavMap()
{
  m_graphEstimator.updateGraph(true);
  m_travGraph = &m_graphEstimator.getGraph();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> RoutePlanner::planSegGoals(int segmentID1, int laneID1,  
    int waypointID1, int segmentID2, int laneID2, int waypointID2, int missionNumber,
    bool lastMission) 
{
  double cost; 
  vector<SegGoals> segGoalsSeq; 
  vector<Vertex*> route; 

  Vertex* vertex1 = m_travGraph->getVertex(segmentID1, laneID1, waypointID1); 
  if (vertex1 == NULL) 
  {
    m_graphEstimator.addVertex(segmentID1, laneID1, waypointID1); 
    vertex1 = m_travGraph->getVertex(segmentID1, laneID1, waypointID1);
    if (vertex1 == NULL)
    {
      if (m_nosparrow)
      {
	cerr << "ERROR: planSegGoals: Cannot find waypoint " << segmentID1
	     << "." << laneID1 << "." << waypointID1 << " in graph" << endl;
      }
      else
      {
	SparrowHawk().log("ERROR: planSegGoals: Cannot find waypoint");
	SparrowHawk().log("%d.%d.%d in graph!\n", segmentID1, laneID1, waypointID1);
      }
    }
  }

  Vertex* vertex2 = m_travGraph->getVertex(segmentID2, laneID2, waypointID2); 
  if (vertex2 == NULL)
  {
    m_graphEstimator.addVertex(segmentID2, laneID2, waypointID2); 
    vertex2 = m_travGraph->getVertex(segmentID2, laneID2, waypointID2);
    if (vertex2 == NULL)
    {
      if (m_nosparrow)
      {
	cerr << "ERROR: planSegGoals: Cannot find waypoint " << segmentID2
	     << "." << laneID2 << "." << waypointID2 << " in graph" << endl;
      }
      else
      {
	SparrowHawk().log("ERROR: planSegGoals: Cannot find waypoint");
	SparrowHawk().log("%d.%d.%d in graph!\n", segmentID2, laneID2, waypointID2);
      }
    }
  }

  bool routeFound = findRoute(vertex1, vertex2, m_travGraph, route, cost);
  if (!routeFound)
  {
    // Add uturn to segGoalsSeq
    segGoalsSeq = addUturn(vertex1, vertex2);


    if (segGoalsSeq.size() == 0)
    {
      if (m_graphEstimator.addRemovedEdge())
      {
        if (m_nosparrow)
        {
          cout << "Cannot find route after allowing u-turn" << endl;
	}
	else
	{
          SparrowHawk().log("Cannot find route after allowing u-turn");
	}
        segGoalsSeq = planSegGoals(segmentID1, laneID1, waypointID1, 
				   segmentID2, laneID2, waypointID2, 
				   missionNumber, lastMission);
      }
      else
      {
        cerr << "Cannot find route after adding all the removed edges back to the graph" 
             << endl;
	cout << "Here is the graph" << endl;
	m_travGraph->print();
	#warning routeplanner gives up when route cannot be found
        exit(1);
      }
    }
  }
  else 
  { 
    segGoalsSeq = findSegGoals(route, m_travGraph, m_minSpeedLimits, m_maxSpeedLimits, m_nextGoalID); 
  } 

  if (segGoalsSeq.size() > 0) 
  { 
    // The exit point of the last segment goal is a checkpoint 
    segGoalsSeq[segGoalsSeq.size()-1].isExitCheckpoint = true; 
  } 

  // If this is the last checkpoint, add END_OF_MISSION goal.
  if (lastMission) 
  { 
    addEndOfMission(segGoalsSeq); 
  } 


  if (m_nosparrow && route.size() > 1)
  {
    cout << endl << "Mission " << missionNumber;
    if (lastMission)
    {
      cout << " (last mission)";
    }
    cout << ": from waypoint "<<  vertex1->getSegmentID() << "."
         << vertex1->getLaneID() << "." << vertex1->getWaypointID();
    cout << " to waypoint " << vertex2->getSegmentID() << "."
         << vertex2->getLaneID() << "."
         << vertex2->getWaypointID() << endl;
  }
  else if (route.size() > 1)
  {
    SparrowHawk().log("Mission %d: from waypoint %d.%d.%d to waypoint %d.%d.%d",
        missionNumber, vertex1->getSegmentID(),
        vertex1->getLaneID(), vertex1->getWaypointID(),
        vertex2->getSegmentID(), vertex2->getLaneID(),
        vertex2->getWaypointID());
  }


  return segGoalsSeq;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> RoutePlanner::addUturn(Vertex* vertex1, Vertex* vertex2)
{
  if (m_nosparrow)
  {
    cout << "no route from ";
    vertex1->print();
    cout << " to ";
    vertex2->print();
    cout << " without making u-turn" << endl;
  }
  else
  {
    SparrowHawk().log("no route from %d.%d.%d to %d.%d.%d without making u-turn\n",
        vertex1->getSegmentID(), vertex1->getLaneID(), vertex1->getWaypointID(),
        vertex2->getSegmentID(), vertex2->getLaneID(), vertex2->getWaypointID());
  }

  if (vertex1->getSegmentID() > m_travGraph->getNumOfSegments())
  {
    cerr << "Cannot make uturn in a zone" << endl;
    cout << "Here is the graph" << endl;
    m_travGraph->print();
    exit(1);
  }

  vector<Vertex*> route;
  vector<SegGoals> segGoalsSeq;
  double cost;
  bool routeFound = false;

  m_graphEstimator.addUturnEdges(vertex1);
  
  routeFound = findRoute(vertex1, vertex2, m_travGraph, route, cost);
  if (!routeFound)
  {
    return segGoalsSeq;
  }
  
  segGoalsSeq = findSegGoals(route, m_travGraph, m_minSpeedLimits, m_maxSpeedLimits, m_nextGoalID);

  return segGoalsSeq;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::addEndOfMission(vector<SegGoals>& segGoalsSeq)
{
  SegGoals lastSeg;
  lastSeg.goalID = segGoalsSeq[segGoalsSeq.size()-1].goalID + 1;;
  lastSeg.entrySegmentID = 0;
  lastSeg.entryLaneID = 0;
  lastSeg.entryWaypointID = 0;
  lastSeg.exitSegmentID = 0;
  lastSeg.exitLaneID = 0;
  lastSeg.exitWaypointID = 0;
  lastSeg.minSpeedLimit = 0;
  lastSeg.maxSpeedLimit = 0;
  lastSeg.segment_type = SegGoals::END_OF_MISSION;
  lastSeg.illegalPassingAllowed = false;
  lastSeg.stopAtExit = true;
  lastSeg.isExitCheckpoint = false;
  segGoalsSeq.push_back(lastSeg);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::resetGoals()
{
  m_contrDirectiveQ.clear();

  if (DEBUG_LEVEL > 0)
  {
    if (m_nosparrow)
    {
      cout << "Clear contrGcPort" << endl;
    }
    else
    {
      SparrowHawk().log("Clear contrGcPort\n");
    }
  }
  DGClockMutex(&m_contrGcPortMutex);
  m_contrGcPortMsgQ.clear();
  DGCunlockMutex(&m_contrGcPortMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool RoutePlanner::loadMDFFile(char* fileName, RNDF* rndf)
{
  ifstream file;
  string line;
  string word;
  
  file.open(fileName, ios::in);

  if(!file)
  {
    cerr << "Error: " << fileName << " file not found." << endl;
    return false;
  } 
  
  getline(file, line);
  istringstream lineStream(line, ios::in);
  lineStream >> word;

  if(word == "MDF_name")
  {
    if (DEBUG_LEVEL > 1)
    {
      cout << "Parsing speed limits" << endl;
    }
    parseSpeedLimit(&file, rndf);
    file.close();
    if (DEBUG_LEVEL > 1)
    {
      cout << "Finish loading MDF file" << endl;
    }
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::parseSpeedLimit(ifstream* file, RNDF* rndf)
{
  int segmentID;
  double minSpeed, maxSpeed;
  string line, word;
  char letter;

  while(word != "speed_limits")
  {    
    getline(*file, line);
    istringstream lineStream(line, ios::in);
    lineStream >> word;
  }

  
  while(word != "end_speed_limits")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    if(letter >= '0' && letter <= '9')
    {
      lineStream >> segmentID;
      lineStream >> minSpeed;
      lineStream >> maxSpeed;

      minSpeed = minSpeed * MPS_PER_MPH;
      maxSpeed = maxSpeed * MPS_PER_MPH;
      
      // setSpeedLimits(segmentID, minSpeed, maxSpeed, rndf);
      if ((unsigned)segmentID <= m_minSpeedLimits.size())
      {
	m_minSpeedLimits[segmentID] = minSpeed;
	m_maxSpeedLimits[segmentID] = maxSpeed;
      }
      else
      {
	cerr << "ERROR: parseSpeedLimit: got speed limit for segment " << segmentID
	     << " while number of segments and zones = " << m_minSpeedLimits.size() << endl;
	int numOfSegments = m_minSpeedLimits.size();
	m_minSpeedLimits.resize(segmentID);
	m_maxSpeedLimits.resize(segmentID);
	for (unsigned i = numOfSegments; i <= m_minSpeedLimits.size(); i++)
	{
	  m_minSpeedLimits[i] = 0;
	  m_maxSpeedLimits[i] = 0;
	}
	m_minSpeedLimits[segmentID] = minSpeed;
	m_maxSpeedLimits[segmentID] = maxSpeed;
      }
    }
    else
    {
      lineStream >> word;
      continue;      
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::setSpeedLimits(int segOrZoneID, double minSpeed, double maxSpeed,
    RNDF* rndf)
{
  if(segOrZoneID <= rndf->getNumOfSegments() + rndf->getNumOfZones() &&
      segOrZoneID > rndf->getNumOfSegments())
  {
    Zone* zone = rndf->getZone(segOrZoneID);
    zone->setSpeedLimits(minSpeed, maxSpeed);

    if (DEBUG_LEVEL > 2 && m_nosparrow)
    {
      cout << "The minimum speed of zone " << segOrZoneID << " is " << minSpeed
           << " mph, the maximum speed is " << maxSpeed << " mph." << endl;
    }
  }
  else
  {
    Segment* segment = rndf->getSegment(segOrZoneID);
    segment->setSpeedLimits(minSpeed, maxSpeed);
    
    
    if (DEBUG_LEVEL > 2 && m_nosparrow)
    {
      cout << "The minimum speed of segment " << segOrZoneID << " is " << minSpeed
           << " mph, the maximum speed is " << maxSpeed << " mph." << endl;
    }
  }  
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::printMissionOnSparrow(deque<SegGoals> segGoals, int startIndex)
{
  int i = 0;
  while (segGoals.size() > 0 && i < startIndex)
  {
    segGoals.pop_front();
    i++;
  }

  int goalID;
  int entrySegmentID, entryLaneID, entryWaypointID;
  int exitSegmentID, exitLaneID, exitWaypointID;
  double minSpeedLimit, maxSpeedLimit;
  bool illegalPassingAllowed, stopAtExit, isExitCheckpoint;
  SegGoals::SegmentType segment_type;
  
  while (segGoals.size() > 0)
  {
    SegGoals currentGoal = segGoals.front();
    goalID = currentGoal.goalID;
    entrySegmentID = currentGoal.entrySegmentID;
    entryLaneID = currentGoal.entryLaneID;
    entryWaypointID = currentGoal.entryWaypointID;
    exitSegmentID = currentGoal.exitSegmentID;
    exitLaneID = currentGoal.exitLaneID;
    exitWaypointID = currentGoal.exitWaypointID;
    minSpeedLimit = currentGoal.minSpeedLimit;
    maxSpeedLimit = currentGoal.maxSpeedLimit;
    illegalPassingAllowed = currentGoal.illegalPassingAllowed;
    stopAtExit = currentGoal.stopAtExit;
    isExitCheckpoint = currentGoal.isExitCheckpoint;
    segment_type = currentGoal.segment_type;
    SparrowHawk().log("GOAL %d: %d.%d.%d -> %d.%d.%d",  goalID, entrySegmentID, entryLaneID,
        entryWaypointID, exitSegmentID, exitLaneID, exitWaypointID);
    switch(segment_type)
    {
      case SegGoals::ROAD_SEGMENT:
        SparrowHawk().log("  ROAD_SEGMENT");
        break;
      case SegGoals::PARKING_ZONE:
        SparrowHawk().log("  PARKING_ZONE");
        break;
      case SegGoals::INTERSECTION:
        SparrowHawk().log("  INTERSECTION");
        break;
      case SegGoals::PREZONE:
        SparrowHawk().log("  PREZONE");
        break;
      case SegGoals::UTURN:
        SparrowHawk().log("  UTURN");
        break;
      case SegGoals::PAUSE:
        SparrowHawk().log("  PAUSE");
        break;
      case SegGoals::END_OF_MISSION:
        SparrowHawk().log("  END_OF_MISSION");
        break;
      default:
        SparrowHawk().log("  UNKNOWN");
    }
    SparrowHawk().log("  Min Speed: %4.1f, Max Speed: %4.1f", minSpeedLimit, maxSpeedLimit);
    if (illegalPassingAllowed)
    {
      SparrowHawk().log("  Illegal passing allowed");
    }
    segGoals.pop_front();
  }	
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::addDirectiveToGcPort(SegGoals directive)
{
  RPlannerContrMsgWrapper msgWrapper;
  msgWrapper.directive = directive;
  msgWrapper.response.status = RPlannerContrMsgResponse::QUEUED;
  DGClockMutex(&m_contrGcPortMutex);
  m_contrGcPortMsgQ.push_back(msgWrapper);
  DGCunlockMutex(&m_contrGcPortMutex);
}
