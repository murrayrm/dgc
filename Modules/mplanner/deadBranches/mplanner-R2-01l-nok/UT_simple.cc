#include <iostream>
#include "rndf/RNDF.hh"
#include "dgcutils/ggis.h"

using namespace std;

int main(int argc, char **argv) 
{
  
  RNDF* m_rndf = new RNDF();
  if (!m_rndf->loadFile("/home/users/nok/setup-nok02/src/routes-santaanita/santaanita_sitevisit_mod.rndf"))
  // if (!m_rndf->loadFile("/home/users/nok/setup-nok02/src/routes-darpa/darpa_sample.rndf"))
  {
    cerr << "ERROR:  Unable to load RNDF file " << endl;
    exit(1);
  }

  m_rndf->print();

  vector<Segment*> segments = m_rndf->getAllSegments();
  for (unsigned i=0; i < segments.size(); i++)
    cout << i << endl;

  delete m_rndf;
}
