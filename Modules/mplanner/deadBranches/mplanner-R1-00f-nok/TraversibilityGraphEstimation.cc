/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */

#include "TraversibilityGraphEstimation.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/SkynetTalker.cc"
#include <boost/serialization/vector.hpp>

#define PI 3.14159265
using namespace std;

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Basic constructor: no threads
TraversibilityGraphEstimation::TraversibilityGraphEstimation
(int skynetKey, bool waitForStateFill) :
  CSkynetContainer(MODmissionplanner, skynetKey), CStateClient(waitForStateFill),
  m_snKey(skynetKey)
{
  // Do nothing
}

void TraversibilityGraphEstimation::init
    (RNDF* rndf, bool listenToMapper, bool nosparrow, int debugLevel, bool verbose)
{
  m_listenToMapper = listenToMapper;
  m_nosparrow = nosparrow; 
  m_rndf = rndf;
  m_graph = new Graph(rndf);
  m_receivedGraph = NULL;
  DEBUG_LEVEL = debugLevel; 
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }
  DGCcreateMutex(&m_receivedGraphMutex);
  DGCcreateMutex(&m_condNewGraphMutex);
  DGCcreateCondition(&m_condNewGraph);
  m_condNewGraph.bCond = false;
  m_condNewGraph.pMutex = m_condNewGraphMutex;
  if (listenToMapper)
  {
    DGCstartMemberFunctionThread(this, &TraversibilityGraphEstimation::getGraphThread);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
TraversibilityGraphEstimation::~TraversibilityGraphEstimation()
{
  delete m_rndf;
  DGCdeleteMutex(&m_receivedGraphMutex);
  DGCdeleteMutex(&m_condNewGraphMutex);
  DGCdeleteCondition(&m_condNewGraph);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::newGraph()
{
  DGClockMutex(&m_condNewGraph.pMutex);
  bool condNewGraph = m_condNewGraph.bCond;
  if (DEBUG_LEVEL > 0)
  {
    if (m_nosparrow)
    {
      cout << "See if we get a new graph ... " << condNewGraph << endl;
    }
    else
    {
      SparrowHawk().log( "See if we get a new graph %d\n", condNewGraph);
    }
  }
  DGCunlockMutex(&m_condNewGraph.pMutex);
  return condNewGraph;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateGraph(bool requestMapper)
{
  int graphRequestSocket = m_skynet.get_send_sock(SNglobalGloNavMapRequest);
  if (requestMapper && m_listenToMapper)
  {  
    bool requestGraph = true;
    if (m_nosparrow)
    {
      cout << "Waiting for mapper" << endl;
    }
    else
    {
      SparrowHawk().log( "Waiting for mapper\n");
    }
    while (!newGraph())
    {
      if (m_nosparrow)
      {
	cout << "Sending request to mapper" << endl;
      }
      else
      {
	SparrowHawk().log( "Sending request to mapper\n");
      }
      m_skynet.send_msg(graphRequestSocket, &requestGraph, sizeof(bool) , 0);
      sleep(5);
    }
  }

  DGClockMutex(&m_receivedGraphMutex);
  if (m_receivedGraph != NULL)
  {
    delete m_graph;
    m_graph = m_receivedGraph;
    m_receivedGraph = NULL;
  }
  if (!m_listenToMapper || m_graph->getVertex(0,0,0) == NULL)
  {
    addCurrentPositionToGraph();
  }
  DGCunlockMutex(&m_receivedGraphMutex);
  DGClockMutex(&m_condNewGraph.pMutex);
  m_condNewGraph.bCond = false;
  DGCunlockMutex(&m_condNewGraph.pMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::waitForGraph()
{
  if (m_listenToMapper)
  {
    if (m_nosparrow)
    {
      cout << "Waiting for graph..." << endl;
    }
    else
    {
      SparrowHawk().log( "Waiting for graph...\n");
    }
    DGCWaitForConditionTrue(m_condNewGraph);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
Graph& TraversibilityGraphEstimation::getGraph()
{
  if (m_nosparrow)
  {
    if (DEBUG_LEVEL > 3)
    {
      cout << "getGraph()" << endl;
      DGClockMutex(&m_receivedGraphMutex);
      if (m_receivedGraph != NULL)
      {
	cout << "m_receivedGraph: " << endl;
	m_receivedGraph->print();
      }
      cout << "m_graph: " << endl;
      m_graph->print();
      DGCunlockMutex(&m_receivedGraphMutex);
    }
  }
  return *m_graph;
}

const Graph& TraversibilityGraphEstimation::getGraph() const
{
  return *m_graph;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addUturnEdges(Vertex* vertex1, Graph* graph)
{
  if (vertex1->getSegmentID() == 0)
  {
    return m_graph->addCurrentPosition(m_state.utmNorthing, m_state.utmEasting, m_state.utmYaw, 
				       m_rndf, 15*PI/180, 5, true);
  }
  else
  {
    if (vertex1->getSegmentID() > m_rndf->getNumOfSegments())
      return false;
    
    GPSPoint* point = m_rndf->getWaypoint(vertex1->getSegmentID(), vertex1->getLaneID(),
					  vertex1->getWaypointID());  
    if (point == NULL)
      return false;
    
    return m_graph->addUturnEdges(vertex1, point, m_rndf);
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::getGraphThread()
{
  if (m_nosparrow)
  {
    cout << "Start listening to mapper" << endl;
  }
  else
  {
    SparrowHawk().log( "Start listening to mapper\n");
  }
  SkynetTalker<Graph> graphTalker(m_snKey, SNglobalGloNavMapFromGloNavMapLib, MODmapping);
  while(true)
  {
    //graphTalker.waitForMessage();
    //m_receivedGraph = new Graph();
    Graph* graph = new Graph();
    bool graphReceived = graphTalker.receive(graph);
    DGClockMutex(&m_receivedGraphMutex); 
    if (graphReceived) 
    { 
      if (m_receivedGraph != NULL)
	delete m_receivedGraph;
      m_receivedGraph = graph;
      DGCSetConditionTrue(m_condNewGraph); 
      if (m_nosparrow)
      {
	cout << "Received a new graph" << endl;
	if (DEBUG_LEVEL > 4)
	{
	  cout << "received graph: " << endl;
	  m_receivedGraph->print();
	}
      }
      else
      {
	SparrowHawk().log( "Received a new graph\n");
      }
    } 
    else
    {
      cerr << "Error receiving graph" << endl;
      delete m_receivedGraph;
      m_receivedGraph = NULL;
    }
    DGCunlockMutex(&m_receivedGraphMutex); 
    usleep(100000); 
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addCurrentPositionToGraph()
{ 
  UpdateState();
  return m_graph->addCurrentPosition(m_state.utmNorthing, m_state.utmEasting, m_state.utmYaw, 
				     m_rndf, 15*PI/180, 5, false);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double TraversibilityGraphEstimation::getNorthing()
{
  return m_state.utmNorthing;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double TraversibilityGraphEstimation::getEasting()
{
  return m_state.utmEasting;
}
