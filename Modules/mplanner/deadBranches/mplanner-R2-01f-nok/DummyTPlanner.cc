/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */
 
#include <iostream>
#include "skynettalker/SkynetTalker.hh"
#include "gcinterfaces/SegGoals.hh"
#include "gcinterfaces/Status.hh"
#include "skynet/skynet.hh"

class CTrafficPlanner
{
  SkynetTalker<SegGoalsStatus> statusTalker;
  SkynetTalker<SegGoals> segGoalsTalker;

public:
  /*! Contstructor */
  CTrafficPlanner(int skynetKey) : 
    statusTalker(skynetKey, SNtplannerStatus, MODtrafficplanner), 
    segGoalsTalker(skynetKey, SNsegGoals, MODtrafficplanner)
  {
    SegGoalsStatus segGoalsStatus;
    segGoalsStatus.goalID = 0;
    segGoalsStatus.status = SegGoalsStatus::COMPLETED;
    statusTalker.send(&segGoalsStatus);
    cout << "Successfully notified mission planner that I'm joining the group" <<  endl;

    int lastFailedGoalID = 0;
    bool lastFailedGoalResolved = true;

    while (true)
    {
      SegGoals segGoals;
      segGoalsTalker.receive(&segGoals);

      cout << "segGoals " << segGoals.goalID << " received" << endl;
      segGoals.print();
      cout << endl;
      segGoalsStatus.goalID = segGoals.goalID;

      if (!lastFailedGoalResolved && lastFailedGoalID == segGoals.goalID)
      {
	lastFailedGoalResolved = true;
      }

      if (segGoals.entrySegmentID == 4 && segGoals.entryLaneID == 1 && segGoals.entryWaypointID == 6 && 
	  segGoals.goalID != lastFailedGoalID)
      {
	segGoalsStatus.status = SegGoalsStatus::FAILED;	
	segGoalsStatus.currentSegmentID = 4;
	segGoalsStatus.currentLaneID = 1;
	segGoalsStatus.lastWaypointID = 6;
	lastFailedGoalID = segGoals.goalID;
	lastFailedGoalResolved = false;
      }
      else
      {
	segGoalsStatus.status = SegGoalsStatus::COMPLETED;
      }
	
      if (lastFailedGoalResolved || segGoalsStatus.status == SegGoalsStatus::FAILED)
      {
	sleep(3);
	cout << "Sending goal " << segGoalsStatus.goalID << " status: " << segGoalsStatus.status << endl;
	statusTalker.send(&segGoalsStatus);
	cout << "Successfully sent status for goal " << segGoalsStatus.goalID << endl;
      }
    }    
  }
};

int main(int argc, char **argv)
{
  int sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;
  CTrafficPlanner pTrafficPlanner(sn_key);
  return 0;
}
