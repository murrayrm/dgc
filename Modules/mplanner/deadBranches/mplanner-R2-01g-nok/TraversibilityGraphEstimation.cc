/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */

#include "TraversibilityGraphEstimation.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "alice/AliceConstants.h"
#include <boost/serialization/vector.hpp>
#include <math.h>

#define PI 3.14159265
#define MAX_NUM_OF_WAYPOINTS 1000
using namespace std;

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Basic constructor: no threads
TraversibilityGraphEstimation::TraversibilityGraphEstimation
(int skynetKey, bool waitForStateFill) :
  CSkynetContainer(MODmissionplanner, skynetKey), CStateClient(waitForStateFill),
  m_snKey(skynetKey)
{  
  DGCcreateMutex(&m_ObstaclesMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
TraversibilityGraphEstimation::~TraversibilityGraphEstimation()
{
  DGCdeleteMutex(&m_receivedGraphMutex);
  DGCdeleteMutex(&m_condNewGraphMutex);
  DGCdeleteMutex(&m_ObstaclesMutex);
  DGCdeleteCondition(&m_condNewGraph);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::init
    (RNDF* rndf, bool listenToMapper, bool nosparrow, bool logData, FILE* logFile, int debugLevel, bool verbose)
{
  m_listenToMapper = listenToMapper; 
  m_nosparrow = nosparrow; 
  m_logData = logData;
  m_logFile = logFile;
  m_rndf = rndf;
  m_graph = new Graph(rndf);
  m_receivedGraph = NULL;
  DEBUG_LEVEL = debugLevel; 
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }
  m_edgeCost.push_back(COST_INTERSECTION);
  m_edgeCost.push_back(COST_INTERSECTION);
  m_edgeCost.push_back(COST_INTERSECTION);
  m_edgeCost.push_back(COST_CHANGE_LANE);
  m_edgeCost.push_back(COST_UTURN);
  m_edgeCost.push_back(COST_KTURN);
  m_edgeCost.push_back(COST_ZONE);
  m_edgeCost.push_back(COST_FIRST_SEGMENT);
  m_edgeCost.push_back(COST_STOP_SIGN);
  m_edgeCost.push_back(COST_NOT_VISITED);
  m_edgeCost.push_back(COST_OBSTRUCTED);

  double avgSpeed = (MIN_SPEED_LIMIT + MAX_SPEED_LIMIT)/2;
  m_avgSpeeds.resize(m_graph->getNumOfSegments() + m_graph->getNumOfZones() + 1);
  for (unsigned i=0; i < m_avgSpeeds.size(); i++)
  {
    m_avgSpeeds[i] = avgSpeed;
  }

  DGCcreateMutex(&m_receivedGraphMutex);
  DGCcreateMutex(&m_condNewGraphMutex);
  DGCcreateCondition(&m_condNewGraph);
  m_condNewGraph.bCond = false;
  m_condNewGraph.pMutex = m_condNewGraphMutex;
  if (listenToMapper)
  {
    DGCstartMemberFunctionThread(this, &TraversibilityGraphEstimation::getGraphThread);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateEdgeCostParam(vector<double> edgeCost)
{
  m_edgeCost = edgeCost;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateSpeed(vector<double> avgSpeeds)
{
  m_avgSpeeds = avgSpeeds;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::newGraph()
{
  DGClockMutex(&m_condNewGraph.pMutex);
  bool condNewGraph = m_condNewGraph.bCond;
  if (DEBUG_LEVEL > 0)
  {
    if (m_nosparrow)
    {
      cout << "See if we get a new graph ... " << condNewGraph << endl;
    }
    else
    {
      SparrowHawk().log( "See if we get a new graph %d\n", condNewGraph);
    }
  }
  if (m_logData)
  {
    fprintf(m_logFile, "See if we get a new graph %d...\n", condNewGraph);
  }
  DGCunlockMutex(&m_condNewGraph.pMutex);
  return condNewGraph;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateGraph(bool requestMapper)
{
  #warning updateGraph deletes the graph that is accessible from other classes through the getGraph() function

  int graphRequestSocket = m_skynet.get_send_sock(SNglobalGloNavMapRequest);
  if (requestMapper && m_listenToMapper)
  {  
    bool requestGraph = true;
    if (m_nosparrow)
    {
      cout << "Waiting for mapper" << endl;
    }
    else
    {
      SparrowHawk().log( "Waiting for mapper\n");
    }
    if (m_logData)
    {
      fprintf (m_logFile, "Waiting for mapper\n");
    }

    while (!newGraph())
    {
      if (m_nosparrow)
      {
	cout << "Sending request to mapper" << endl;
      }
      else
      {
	SparrowHawk().log( "Sending request to mapper\n");
      }
      if (m_logData)
      {
	fprintf( m_logFile, "Sending request to mapper\n");
      }
      m_skynet.send_msg(graphRequestSocket, &requestGraph, sizeof(bool) , 0);
      sleep(5);
    }
  }

  DGClockMutex(&m_receivedGraphMutex);
  // Swap pointers between m_graph and m_receivedGraph
  if (m_receivedGraph != NULL)
  {
    delete m_graph;
    m_graph = m_receivedGraph;
    m_receivedGraph = NULL;
  }
  else if (!m_listenToMapper)
  {
    m_graph->init(m_rndf);
  }
  DGCunlockMutex(&m_receivedGraphMutex);

  if (!m_listenToMapper || m_graph->getVertex(0,0,0) == NULL)
  {
    addCurrentPositionToGraph();
  }

  // Update the weight of edges based on inserted obstacles
  DGClockMutex(&m_ObstaclesMutex);
  updateObstructedEdges();
  DGCunlockMutex(&m_ObstaclesMutex);

  m_graph->updateEdgeCost(m_edgeCost, m_avgSpeeds);
  removeEdges();
  updateCurrentPositionEdges();

  DGClockMutex(&m_condNewGraph.pMutex);
  m_condNewGraph.bCond = false;
  DGCunlockMutex(&m_condNewGraph.pMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::waitForGraph()
{
  if (m_listenToMapper)
  {
    if (m_nosparrow)
    {
      cout << "Waiting for graph..." << endl;
    }
    else
    {
      SparrowHawk().log( "Waiting for graph...\n");
    }
    if (m_logData)
    {
      fprintf(m_logFile, "Waiting for graph...\n");
    }

    DGCWaitForConditionTrue(m_condNewGraph);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
Graph& TraversibilityGraphEstimation::getGraph()
{
  if (m_nosparrow)
  {
    if (DEBUG_LEVEL > 4)
    {
      cout << "getGraph()" << endl;
      DGClockMutex(&m_receivedGraphMutex);
      if (m_receivedGraph != NULL)
      {
	cout << "m_receivedGraph: " << endl;
	m_receivedGraph->print();
      }
      cout << "m_graph: " << endl;
      m_graph->print();
      DGCunlockMutex(&m_receivedGraphMutex);
      cout << "finished m_graph" << endl;
    }
  }

  return *m_graph;
}

const Graph& TraversibilityGraphEstimation::getGraph() const
{
  return *m_graph;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addVertex(int segmentID, int laneID, int waypointID)
{
  GPSPoint* point = NULL;
  if (segmentID == 0)
  {
    return addCurrentPositionToGraph();
  }
  else if (laneID != 0)
  {
    point = m_rndf->getWaypoint(segmentID, laneID, waypointID);
  }
  else
  {
    point = m_rndf->getPerimeterPoint(segmentID, waypointID);
  }
  if (point == NULL)
    return false;

  return m_graph->addGPSPointToGraph (point, m_rndf);
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addUturnEdges(Vertex* vertex1)
{
  if (vertex1->getSegmentID() == 0)
  {
    double northingFront = m_state.utmNorthing + DIST_REAR_AXLE_TO_FRONT*cos(m_state.utmYaw);
    double eastingFront = m_state.utmEasting + DIST_REAR_AXLE_TO_FRONT*sin(m_state.utmYaw);
    double yaw = m_state.utmYaw;
    if (yaw > PI)
    {
      yaw = yaw - 2*PI;
    }
    double headingDiff, headingLaneDiff;
    
    bool edgeAdded = m_graph->addCurrentPosition(northingFront, eastingFront, yaw, 
						 m_rndf, 50*PI/180, VEHICLE_MIN_TURNING_RADIUS + 0.1, 
						 45*PI/180, 5000, headingDiff,
						 headingLaneDiff, true);
    if (m_logData)
    {
      fprintf (m_logFile, "After adding uturn edges before updating obstructing edges\n");
      fprintf (m_logFile, "Graph:\n");
      m_graph->log(m_logFile);
      fprintf (m_logFile, "\n");
    }
    updateObstructedEdges();
    return edgeAdded;
  }
  else
  {
    if (vertex1->getSegmentID() > m_rndf->getNumOfSegments())
      return false;
    
    GPSPoint* point = m_rndf->getWaypoint(vertex1->getSegmentID(), vertex1->getLaneID(),
					  vertex1->getWaypointID());  
    if (point == NULL)
      return false;
    
    bool edgeAdded = m_graph->addUturnEdges(point->getNorthing(), point->getEasting(), vertex1, point, m_rndf);
    if (m_logData)
    {
      fprintf (m_logFile, "After adding uturn edges before updating obstructing edges\n");
      fprintf (m_logFile, "Graph:\n");
      m_graph->log(m_logFile);
      fprintf (m_logFile, "\n");
    }
    updateObstructedEdges();
    return edgeAdded;
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addRemovedEdge()
{
  if (m_removedEdges.size() > 0)
  {
    EdgeID edgeToAdd = m_removedEdges.front();
    if (m_nosparrow)
    {
      cout << "Adding the removed edge from "
	   << edgeToAdd.prevSegmentID
	   << "." << edgeToAdd.prevLaneID << "."
	   << edgeToAdd.prevWaypointID << " to "
	   << edgeToAdd.nextSegmentID << "."
	   << edgeToAdd.nextLaneID << "."
	   << edgeToAdd.nextWaypointID << " to the graph" << endl;
    }
    else
    {
      SparrowHawk().log("Adding the removed edge from %d.%d.%d to %d.%d.%d to the graph",
			edgeToAdd.prevSegmentID,
			edgeToAdd.prevLaneID,
			edgeToAdd.prevWaypointID,
			edgeToAdd.nextSegmentID,
			edgeToAdd.nextLaneID, edgeToAdd.nextWaypointID);
    }
    if (m_logData)
    {
      fprintf (m_logFile, "\nAdding the removed edge from %d.%d.%d to %d.%d.%d to the graph",
			edgeToAdd.prevSegmentID,
			edgeToAdd.prevLaneID,
			edgeToAdd.prevWaypointID,
			edgeToAdd.nextSegmentID,
			edgeToAdd.nextLaneID, edgeToAdd.nextWaypointID);
    }

    bool edgeAdded = m_graph->addEdge(edgeToAdd.prevSegmentID, edgeToAdd.prevLaneID, edgeToAdd.prevWaypointID,
		     edgeToAdd.nextSegmentID, edgeToAdd.nextLaneID, edgeToAdd.nextWaypointID,
		     edgeToAdd.length, edgeToAdd.weight, edgeToAdd.type, edgeToAdd.avgSpeed, edgeToAdd.isVisited);
    Edge* tmpEdge = m_graph->getEdge(edgeToAdd.prevSegmentID, edgeToAdd.prevLaneID, edgeToAdd.prevWaypointID,
				     edgeToAdd.nextSegmentID, edgeToAdd.nextLaneID, edgeToAdd.nextWaypointID);
    if (edgeAdded && tmpEdge != NULL)
    {
      tmpEdge->setObstructedLevel(MAX_OBSTRUCTED_LEVEL);
    }
    m_removedEdges.pop_front();
    return true;
  }
  else
    return false;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<Obstacle>& TraversibilityGraphEstimation::getAllObstaclesOnSegment(int segmentID)
{  
  m_obstaclesOnSegment.clear();
  DGClockMutex(&m_ObstaclesMutex);
  for (unsigned i = 0; i < m_obstacles.size(); i++)
  {
    if (m_obstacles[i].getSegmentID() == segmentID)
    {
      m_obstaclesOnSegment.push_back(m_obstacles[i]);
    }
  }
  DGCunlockMutex(&m_ObstaclesMutex);
  
  return m_obstaclesOnSegment;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::insertObstacle(int obstacleID, int obstacleSegmentID,
						   int obstacleLaneID, int obstacleWaypointID)
{
  // Check that the segment ID, lane ID and waypoint ID are valid
  int numOfSegments = m_rndf->getNumOfSegments();
  int numOfZones = m_rndf->getNumOfZones();
  int numOfSegmentsAndZones = numOfSegments + numOfZones;

  if (obstacleSegmentID < 1 || obstacleSegmentID > numOfSegmentsAndZones)
  {
    SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
        obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    SparrowHawk().log("Segment ID must be between 1 and %d\n", numOfSegmentsAndZones);
    if (m_logData)
    {
      fprintf(m_logFile,"\nERROR: Cannot insert obstacle at segment %d, lane %d, waypoint %d\n",
			obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      fprintf(m_logFile,"Segment ID must be between 1 and %d\n", numOfSegmentsAndZones);
    }
    return false;
  }

  if (obstacleSegmentID <= numOfSegments)
  {
    int numOfLanes = m_rndf->getSegment(obstacleSegmentID)->getNumOfLanes();
    if (obstacleLaneID < 1 || obstacleLaneID > numOfLanes)
    {
      SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      SparrowHawk().log("Lane ID must be between 1 and %d for segment %d\n",
          numOfLanes, obstacleSegmentID);
      if (m_logData)
      {
	fprintf(m_logFile, "\nERROR: Cannot insert obstacle at segment %d, lane %d, waypoint %d\n",
			  obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	fprintf(m_logFile, "Lane ID must be between 1 and %d for segment %d\n",
			  numOfLanes, obstacleSegmentID);
      }
      return false;
    }
  }
  else if (obstacleLaneID != 0)
  {
    int numOfSpots = m_rndf->getZone(obstacleSegmentID)->getNumOfSpots();
    if (obstacleLaneID < 1 || obstacleLaneID > numOfSpots)
    {
      SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      SparrowHawk().log("Lane ID must be between 0 and %d for segment %d\n",
          numOfSpots, obstacleSegmentID);
      if (m_logData)
      {
	fprintf(m_logFile, "\nERROR: Cannot insert obstacle at segment %d, lane %d, waypoint %d\n",
			  obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	fprintf(m_logFile, "Lane ID must be between 0 and %d for segment %d\n",
			  numOfSpots, obstacleSegmentID);
      }
      return false;
    }
  }

  if (obstacleSegmentID <= numOfSegments)
  {
    int numOfWaypoints =
        m_rndf->getLane(obstacleSegmentID, obstacleLaneID)->getNumOfWaypoints();
    if (obstacleWaypointID < 1 || obstacleWaypointID > numOfWaypoints)
    {
      SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      SparrowHawk().log("Waypoint ID must be between 1 and %d for segment %d lane %d\n",
          numOfWaypoints, obstacleSegmentID, obstacleLaneID);
      if (m_logData)
      {
	fprintf(m_logFile, "\nERROR: Cannot insert obstacle at segment %d, lane %d, waypoint %d\n",
			  obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	fprintf(m_logFile, "Waypoint ID must be between 1 and %d for segment %d lane %d\n",
			  numOfWaypoints, obstacleSegmentID, obstacleLaneID);
      }
      return false;
    }
  }
  else if (obstacleLaneID == 0)
  {
    int numOfPerimeterPoints =
        m_rndf->getZone(obstacleSegmentID)->getNumOfPerimeterPoints();
    if (obstacleWaypointID < 1 || obstacleWaypointID > numOfPerimeterPoints)
    {
      SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      SparrowHawk().log("Waypoint ID must be between 1 and %d for segment %d lane %d\n",
          numOfPerimeterPoints, obstacleSegmentID, obstacleLaneID);
      if (m_logData)
      {
	fprintf(m_logFile, "\nERROR: Cannot insert obstacle at segment %d, lane %d, waypoint %d\n",
			  obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	fprintf(m_logFile, "Waypoint ID must be between 1 and %d for segment %d lane %d\n",
			  numOfPerimeterPoints, obstacleSegmentID, obstacleLaneID);
      }
      return false;
    }
  }
  else if (obstacleWaypointID < 1 || obstacleWaypointID > 2)
  {
    SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
        obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    SparrowHawk().log("Waypoint ID must be between 1 and 2 for segment %d lane %d\n",
        obstacleSegmentID, obstacleLaneID);
    if (m_logData)
    {
      fprintf( m_logFile, "\nERROR: Cannot insert obstacle at segment %d, lane %d, waypoint %d\n",
			obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      fprintf( m_logFile, "Waypoint ID must be between 1 and 2 for segment %d lane %d\n",
			obstacleSegmentID, obstacleLaneID);
    }
    return false;
  }
  
  DGClockMutex(&m_ObstaclesMutex);
  int obstacleIndex = getObstacleIndex(obstacleID);
  if ((unsigned)obstacleIndex < m_obstacles.size())
  {
    if (m_obstacles[obstacleIndex].getSegmentID() != obstacleSegmentID)
    {
      int newObstacleID = obstacleID + 1;
      while((unsigned)getObstacleIndex(newObstacleID) < m_obstacles.size())
      {
        newObstacleID++;
      }
      SparrowHawk().log("Obstacle ID %d already exists on segment %d. Changed obstacle ID to %d\n",
          obstacleID, m_obstacles[obstacleIndex].getSegmentID(), newObstacleID);
      if (m_logData)
	{
	  fprintf(m_logFile,"\nERROR: Obstacle ID %d already exists on segment %d. Changed obstacle ID to %d\n",
			    obstacleID, m_obstacles[obstacleIndex].getSegmentID(), newObstacleID);
	}
      Obstacle obstacle(newObstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      m_obstacles.push_back(obstacle);
      SparrowHawk().log("Added obstacle %d at segment %d lane %d waypoint %d to the obstacle list\n",
          newObstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      if (m_logData)
	{
	  fprintf(m_logFile,"\nAdded obstacle %d at segment %d lane %d waypoint %d to the obstacle list\n",
			    newObstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	}
    }
    else
    {
      bool obstructedLaneAdded =
          m_obstacles[obstacleIndex].addObstructedLane(obstacleLaneID, obstacleWaypointID);
      if (obstructedLaneAdded)
      {
        SparrowHawk().log("Obstacle %d on segment %d: Added obstructed lane %d waypoint %d\n",
            obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	if (m_logData)
	  {
	    fprintf(m_logFile, "\nObstacle %d on segment %d: Added obstructed lane %d waypoint %d\n",
			      obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	  }
      }
      else
      {
        SparrowHawk().log("Obstacle %d on segment %d: Cannot add obstructed lane %d waypoint %d.\n",
		        obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
        SparrowHawk().log("This obstructed lane already exists.\n");
        SparrowHawk().log("Change obstacle ID if you still want to add this obstacle");
	if (m_logData)
	  {
	    fprintf(m_logFile, "\nERROR: Obstacle %d on segment %d: Cannot add obstructed lane %d waypoint %d.\n",
			      obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	    fprintf(m_logFile, "This obstructed lane already exists.\n");
	    fprintf(m_logFile, "Change obstacle ID if you still want to add this obstacle\n");
	  }
      }
    }
  }
  else
  {
    Obstacle obstacle(obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    m_obstacles.push_back(obstacle);
    SparrowHawk().log("Added obstacle %d at segment %d lane %d waypoint %d to the obstacle list\n",
        obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    if (m_logData)
      {
	fprintf(m_logFile, "\nAdded obstacle %d at segment %d lane %d waypoint %d to the obstacle list\n",
			  obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      }
  }
  DGCunlockMutex(&m_ObstaclesMutex);
  return true;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
int TraversibilityGraphEstimation::getObstacleIndex(int obstacleID)
{
  int i = 0;
  while ((unsigned)i < m_obstacles.size() && m_obstacles[i].getObstacleID() != obstacleID)
  {
    i++;
  }
  
  return i;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateObstructedEdges()
{
  if (m_logData)
  {
    fprintf(m_logFile, "UpdateObstructedEdges():\n");
  }
  for(unsigned i = 0; i < m_obstacles.size(); i++)
  {
    int obsSegmentID = m_obstacles[i].getSegmentID();
    vector<int> obsLaneIDs = m_obstacles[i].getLaneIDs();
    vector<int> obsWaypointIDs = m_obstacles[i].getWaypointIDs();
    for (unsigned j = 0; j < obsLaneIDs.size(); j++)
    {
      m_graph->addObstacle(obsSegmentID, obsLaneIDs[j], obsWaypointIDs[j]);
      if (m_logData)
      {
	fprintf(m_logFile, "  Obstacle at %d.%d.%d is addded.\n", obsSegmentID, obsLaneIDs[j], obsWaypointIDs[j]);
      }
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::removeFailedGoalEdges(int segmentID1, int laneID1, int waypointID1,
							  int segmentID2, int laneID2, int waypointID2,
							  bool removeOtherEdges)
{
  if (m_logData)
  {
    fprintf(m_logFile, "removeFailedGoalEdges:\n");
  }
  int entryWaypointID = 1;
  int exitWaypointID = MAX_NUM_OF_WAYPOINTS;

  if (removeOtherEdges)
  {
    bool entryFound = false;
    int waypointID = waypointID2;
    Vertex* vertex;
    while (!entryFound && waypointID >= 1)
    {
      vertex = m_graph->getVertex(segmentID2, laneID2, waypointID);
      if (vertex != NULL)
	entryFound = vertex->isEntry();
      waypointID--;
    }
    if (entryFound)
      entryWaypointID = waypointID + 1;
    
    bool exitFound = false;
    int numOfWaypoints = MAX_NUM_OF_WAYPOINTS;
    if (segmentID1 == 0)
    {
      segmentID1 = segmentID2;
      laneID1 = laneID2;
      waypointID1 = waypointID2;
    }
    waypointID = waypointID1;
    Lane* failedLane = m_rndf->getLane(segmentID1, laneID1);
    if (failedLane != NULL)
    {
      numOfWaypoints = failedLane->getNumOfWaypoints();
    }
    else
    {
      cerr << "ERROR: TraversibilityGraphEstimation::removeFailedGoalEdges: Cannot find lane "
	   << segmentID1 << "." << laneID1 << ". Other edges will not be removed." << endl;
      if (m_logData)
      {
	fprintf(m_logFile, "ERROR: TraversibilityGraphEstimation::removeFailedGoalEdges(%d, %d, %d, %d, %d, %d, true)",
		segmentID1, laneID1, waypointID1, segmentID2, laneID2, waypointID2);
	fprintf(m_logFile, ": Cannot find lane %d.%d. Other edges will not be removed.", segmentID1, laneID1);
      }
      removeOtherEdges = false; 
    }
    exitWaypointID = numOfWaypoints; 
    while (!exitFound && waypointID <= numOfWaypoints) 
    { 
      vertex = m_graph->getVertex(segmentID1, laneID1, waypointID); 
      if (vertex != NULL) 
	exitFound = vertex->isExit(); 
      waypointID++;
    } 
    if (exitFound) 
      exitWaypointID = waypointID - 1;
  }


  // Search the graph to find edges that need to be removed
  vector<Vertex*> vertices = m_graph->getVertices();
  for (unsigned i=0; i < vertices.size(); i++)
  {
    vector<Edge*> edges = vertices[i]->getEdges();
    for (unsigned j=0; j < edges.size(); j++)
    {
      if ( edges[j]->getObstructedLevel() >= MAX_OBSTRUCTED_LEVEL && 
	   segmentID1 == edges[j]->getPrevious()->getSegmentID() &&
	   laneID1 == edges[j]->getPrevious()->getLaneID() &&
	   (waypointID1 == edges[j]->getPrevious()->getWaypointID() ||
	    (removeOtherEdges && exitWaypointID >= edges[j]->getPrevious()->getWaypointID())) &&
	   segmentID2 == edges[j]->getNext()->getSegmentID() &&
	   laneID2 == edges[j]->getNext()->getLaneID() &&
	   (waypointID2 == edges[j]->getNext()->getWaypointID() ||
	    (removeOtherEdges && entryWaypointID <= edges[j]->getNext()->getWaypointID())) )
      {
	if (m_nosparrow)
	{
	  cout << "Setting the edge from waypoint " << (edges[j]->getPrevious())->getSegmentID()
	       << "." << (edges[j]->getPrevious())->getLaneID() << "."
	       << (edges[j]->getPrevious())->getWaypointID() << " to " 
	       << (edges[j]->getNext())->getSegmentID()
	       << "." << (edges[j]->getNext())->getLaneID() << "." 
	       << (edges[j]->getNext())->getWaypointID()
	       << " to be fully blocked" << endl;
	}
	else
	{
	  SparrowHawk().
	    log("Setting the edge from waypoint %d.%d.%d to %d.%d.%d",
		(edges[j]->getPrevious())->getSegmentID(),
		(edges[j]->getPrevious())->getLaneID(),
		(edges[j]->getPrevious())->getWaypointID(),
		(edges[j]->getNext())->getSegmentID(),
		(edges[j]->getNext())->getLaneID(),
		(edges[j]->getNext())->getWaypointID());
	  SparrowHawk().log("to be fully blocked");
	}
	if (m_logData)
	{
	  fprintf(m_logFile, "  Setting the edge from waypoint %d.%d.%d to %d.%d.%d",
		  (edges[j]->getPrevious())->getSegmentID(),
		  (edges[j]->getPrevious())->getLaneID(),
		  (edges[j]->getPrevious())->getWaypointID(),
		  (edges[j]->getNext())->getSegmentID(),
		  (edges[j]->getNext())->getLaneID(),
		  (edges[j]->getNext())->getWaypointID());
	  fprintf(m_logFile, "to be fully blocked\n");
	}
	// Remove the edge corresponding to the failed segment goals from the graph
	EdgeID remEdge(edges[j]->getPrevious()->getSegmentID(), edges[j]->getPrevious()->getLaneID(),
		       edges[j]->getPrevious()->getWaypointID(), edges[j]->getNext()->getSegmentID(),
		       edges[j]->getNext()->getLaneID(), edges[j]->getNext()->getWaypointID(), 
		       edges[j]->getLength(), edges[j]->getWeight(), edges[j]->getType(), edges[j]->getAvgSpeed(),
		       edges[j]->getIsVisited());
	m_graph->removeEdge(edges[j]);
	m_removedEdges.push_back(remEdge);
      }
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::removeAllObstructedEdges()
{
  if (m_logData)
  {
    fprintf(m_logFile, "removeAllObstructedEdges:\n");
  }
  // Search the graph to find edges that need to be removed
  vector<Vertex*> vertices = m_graph->getVertices();
  for (unsigned i=0; i < vertices.size(); i++)
  {
    vector<Edge*> edges = vertices[i]->getEdges();
    for (unsigned j=0; j < edges.size(); j++)
    {
      if (edges[j]->getType() == Edge::ROAD_SEGMENT && edges[j]->getObstructedLevel() >= 
	      MAX_OBSTRUCTED_LEVEL)
      {
	if (m_nosparrow)
	{
	  cout << "Setting the edge from waypoint " << (edges[j]->getPrevious())->getSegmentID()
	       << "." << (edges[j]->getPrevious())->getLaneID() << "."
	       << (edges[j]->getPrevious())->getWaypointID() << " to " 
	       << (edges[j]->getNext())->getSegmentID()
	       << "." << (edges[j]->getNext())->getLaneID() << "." 
	       << (edges[j]->getNext())->getWaypointID()
	       << " to be fully blocked" << endl;
	}
	else
	{
	  SparrowHawk().
	    log("Setting the edge from waypoint %d.%d.%d to %d.%d.%d",
		(edges[j]->getPrevious())->getSegmentID(),
		(edges[j]->getPrevious())->getLaneID(),
		(edges[j]->getPrevious())->getWaypointID(),
		(edges[j]->getNext())->getSegmentID(),
		(edges[j]->getNext())->getLaneID(),
		(edges[j]->getNext())->getWaypointID());
	  SparrowHawk().log("to be fully blocked");
	}
	if (m_logData)
	{
	  fprintf(m_logFile, "  Setting the edge from waypoint %d.%d.%d to %d.%d.%d",
		  (edges[j]->getPrevious())->getSegmentID(),
		  (edges[j]->getPrevious())->getLaneID(),
		  (edges[j]->getPrevious())->getWaypointID(),
		  (edges[j]->getNext())->getSegmentID(),
		  (edges[j]->getNext())->getLaneID(),
		  (edges[j]->getNext())->getWaypointID());
	  fprintf(m_logFile, "to be fully blocked\n");
	}
	// Remove the edge corresponding to the failed segment goals from the graph
	EdgeID remEdge(edges[j]->getPrevious()->getSegmentID(), edges[j]->getPrevious()->getLaneID(),
		       edges[j]->getPrevious()->getWaypointID(), edges[j]->getNext()->getSegmentID(),
		       edges[j]->getNext()->getLaneID(), edges[j]->getNext()->getWaypointID(), 
		       edges[j]->getLength(), edges[j]->getWeight(), edges[j]->getType(), edges[j]->getAvgSpeed(),
		       edges[j]->getIsVisited());
	m_graph->removeEdge(edges[j]);
	m_removedEdges.push_back(remEdge);
      }
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::removeEdges()
{
  // Search the graph to find edges that need to be removed
  vector<Vertex*> vertices = m_graph->getVertices();
  for (unsigned i=0; i < vertices.size(); i++)
  {
    vector<Edge*> edges = vertices[i]->getEdges();
    for (unsigned j=0; j < edges.size(); j++)
    {
      for (unsigned k = 0; k < m_removedEdges.size(); k++)
      {
	Vertex* prevVertex = edges[j]->getPrevious();
	Vertex* nextVertex = edges[j]->getNext();
	if (m_removedEdges[k].prevSegmentID == prevVertex->getSegmentID() &&
	    m_removedEdges[k].prevLaneID == prevVertex->getLaneID() &&
	    m_removedEdges[k].prevWaypointID == prevVertex->getWaypointID() &&
	    m_removedEdges[k].nextSegmentID == nextVertex->getSegmentID() &&
	    m_removedEdges[k].nextLaneID == nextVertex->getLaneID() &&
	    m_removedEdges[k].nextWaypointID == nextVertex->getWaypointID())
	{
	  if (m_logData)
	  {
	    fprintf(m_logFile, "  removed edge from %d.%d.%d to %d.%d.%d\n", prevVertex->getSegmentID(),
		    prevVertex->getLaneID(), prevVertex->getWaypointID(),
		    nextVertex->getSegmentID(), nextVertex->getLaneID(), nextVertex->getWaypointID());
	  }
	  m_graph->removeEdge(edges[j]);
	}
      }
    }
  }  
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateCurrentPositionEdges()
{
  // Update the edge from current position
  Vertex* currentPos = m_graph->getVertex(0,0,0);
  if (currentPos == NULL)
    return;
  vector<Edge*> edges = currentPos->getEdges();
  if (m_logData)
  {
    fprintf (m_logFile, "  Removed edges:\n" );
    for (unsigned i = 0; i < m_removedEdges.size(); i++)
    {
      fprintf (m_logFile, "    %d.%d.%d -> %d.%d.%d \t", m_removedEdges[i].prevSegmentID,
	       m_removedEdges[i].prevLaneID, m_removedEdges[i].prevWaypointID,
	       m_removedEdges[i].nextSegmentID, m_removedEdges[i].nextLaneID,
	       m_removedEdges[i].nextWaypointID);
    }
  }
  for (unsigned i = 0; i < edges.size(); i++)
  {
    Vertex* nextVertex = edges[i]->getNext();
    if (m_logData)
    {
      fprintf(m_logFile, "  closest vertex is %d.%d.%d\n", nextVertex->getSegmentID(), nextVertex->getLaneID(),
	       nextVertex->getWaypointID());
    }
    if (edges[i]->getType() != Edge::UTURN && edges[i]->getType() != Edge::KTURN)
    {
      if (m_logData)
      {
	fprintf( m_logFile, "   See if edge 0.0.0 -> %d.%d.%d is removed\n", nextVertex->getSegmentID(),
		 nextVertex->getLaneID(), nextVertex->getWaypointID());
      }
      bool removeCurrentEdge = false;
      for (unsigned j = 0; j < m_removedEdges.size(); j++)
      {
	if (m_logData)
	{
	  fprintf( m_logFile, "    edgeRemoved = %d.%d.%d -> %d.%d.%d, \t nextVertex = %d.%d.%d\n",
		   m_removedEdges[j].prevSegmentID, m_removedEdges[j].prevLaneID,
		   m_removedEdges[j].prevWaypointID, m_removedEdges[j].nextSegmentID,
		   m_removedEdges[j].nextLaneID, m_removedEdges[j].nextWaypointID,
		   nextVertex->getSegmentID(), nextVertex->getLaneID(), nextVertex->getWaypointID());
	}
	if (m_removedEdges[j].prevSegmentID == nextVertex->getSegmentID() &&
	    m_removedEdges[j].prevLaneID == nextVertex->getLaneID() &&
	    m_removedEdges[j].prevWaypointID <= nextVertex->getWaypointID() &&
	    m_removedEdges[j].nextSegmentID == nextVertex->getSegmentID() &&
	    m_removedEdges[j].nextLaneID == nextVertex->getLaneID() &&
	    m_removedEdges[j].nextWaypointID >= nextVertex->getWaypointID())
	{
	  removeCurrentEdge = true;
	  break;
	}
      }
      if (removeCurrentEdge)
      {
	if (m_logData)
	{
	  fprintf(m_logFile, "  removed edge from %d.%d.%d to %d.%d.%d\n", edges[i]->getPrevious()->getSegmentID(),
		  edges[i]->getPrevious()->getLaneID(), edges[i]->getPrevious()->getWaypointID(),
		  edges[i]->getNext()->getSegmentID(), edges[i]->getNext()->getLaneID(),
		  edges[i]->getNext()->getWaypointID());
	}
	m_graph->removeEdge(edges[i]);
      }
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::clearRemovedEdges()
{
  m_removedEdges.clear();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::getGraphThread()
{
  if (m_nosparrow)
  {
    cout << "Start listening to mapper" << endl;
  }
  else
  {
    SparrowHawk().log( "Start listening to mapper\n");
  }
  if (m_logData)
  {
    fprintf (m_logFile, "Start listening to mapper\n");
  }
  SkynetTalker<Graph> graphTalker(m_snKey, SNglobalGloNavMapFromGloNavMapLib, MODmapping);
  while(true)
  {
    //graphTalker.waitForMessage();
    //m_receivedGraph = new Graph();
    Graph* graph = new Graph();
    bool graphReceived = graphTalker.receive(graph);
    DGClockMutex(&m_receivedGraphMutex); 
    if (graphReceived) 
    { 
      if (m_receivedGraph != NULL)
	delete m_receivedGraph;
      m_receivedGraph = graph;
      DGCSetConditionTrue(m_condNewGraph); 
      if (m_nosparrow)
      {
	cout << "Received a new graph" << endl;
	if (DEBUG_LEVEL > 4)
	{
	  cout << "received graph: " << endl;
	  m_receivedGraph->print();
	}
      }
      else
      {
	SparrowHawk().log( "Received a new graph\n");
      }
      if (m_logData)
      {
	fprintf(m_logFile, "Received a new graph\n");
      }
    } 
    else
    {
      cerr << "Error receiving graph" << endl;
      delete m_receivedGraph;
      m_receivedGraph = NULL;
    }
    DGCunlockMutex(&m_receivedGraphMutex); 
    usleep(100000); 
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addCurrentPositionToGraph()
{ 
  UpdateState();
  double northingFront = m_state.utmNorthing + DIST_REAR_AXLE_TO_FRONT*cos(m_state.utmYaw);
  double eastingFront = m_state.utmEasting + DIST_REAR_AXLE_TO_FRONT*sin(m_state.utmYaw);
  double yaw = m_state.utmYaw;
  if (yaw >= PI)
  {
    yaw = yaw - 2*PI;
  }
  double headingDiff, headingLaneDiff;

  bool currentPosAdded = m_graph->addCurrentPosition(northingFront, eastingFront, yaw, 
						     m_rndf, 50*PI/180, VEHICLE_MIN_TURNING_RADIUS + 0.1, 
						     45*PI/180, 5000, headingDiff,
						     headingLaneDiff, false);
  if (DEBUG_LEVEL > 0)
  {
    if (m_nosparrow)
    {
      cout << "Current state is Northing = " << northingFront << " Easting = " << eastingFront
	   << " Yaw = " << yaw << endl;
      cout << "Closest waypoints are ";
      if (m_graph->getVertex(0,0,0) != NULL)
      {
	vector<Edge*> edgesFromCurrPos = m_graph->getVertex(0,0,0)->getEdges();
	for (unsigned i = 0; i < edgesFromCurrPos.size(); i++)
	{
	  edgesFromCurrPos[i]->getNext()->print();
	  GPSPoint* closestPoint;
	  if (edgesFromCurrPos[i]->getNext()->getLaneID() != 0)
	  {
	    closestPoint = m_rndf->getWaypoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getLaneID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  else
	  {
	    closestPoint = m_rndf->getPerimeterPoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  if (closestPoint != NULL)
	  {
	    cout << "(" << closestPoint->getNorthing() - northingFront
		 << ", " << closestPoint->getEasting() - eastingFront
		 << ", " << 180/PI * ( atan2(closestPoint->getEasting() - eastingFront, 
					     closestPoint->getNorthing() - northingFront))
		 << ", " << 180/PI * headingDiff << ", " << 180/PI * headingLaneDiff
		 << ")   ";
	  }
	}
      }	
      cout << endl;
    }
    else
    {
      SparrowHawk().log("Current state is Northing = %f, Easting = %f,  Yaw = %f\n",  
			northingFront, eastingFront, yaw);
      SparrowHawk().log( "Closest waypoints are " );
      if (m_graph->getVertex(0,0,0) != NULL)
      {
	vector<Edge*> edgesFromCurrPos = m_graph->getVertex(0,0,0)->getEdges();
	for (unsigned i = 0; i < edgesFromCurrPos.size(); i++)
	{
	  SparrowHawk().log("%d.%d.%d", edgesFromCurrPos[i]->getNext()->getSegmentID(),
			   edgesFromCurrPos[i]->getNext()->getLaneID(), edgesFromCurrPos[i]->getNext()->getWaypointID());
	  GPSPoint* closestPoint;
	  if (edgesFromCurrPos[i]->getNext()->getLaneID() != 0)
	  {
	    closestPoint = m_rndf->getWaypoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getLaneID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  else
	  {
	    closestPoint = m_rndf->getPerimeterPoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  if (closestPoint != NULL)
	  {
	    SparrowHawk().log("(%f, %f, %f, %f, %f)\n", closestPoint->getNorthing() - northingFront,
			      closestPoint->getEasting() - eastingFront,
			      180/PI * ( atan2(closestPoint->getEasting() - eastingFront, 
					       closestPoint->getNorthing() - northingFront)),
			      180/PI * headingDiff, 180/PI * headingLaneDiff);
	  }
	}
      }	
    }	   
    if (m_logData)
    {
      fprintf(m_logFile, "  Current state is Northing = %f, Easting = %f,  Yaw = %f\n",  
			m_state.utmNorthing, m_state.utmEasting, m_state.utmYaw);
      fprintf(m_logFile, "  Closest waypoints are ");
      if (m_graph->getVertex(0,0,0) != NULL)
      {
	vector<Edge*> edgesFromCurrPos = m_graph->getVertex(0,0,0)->getEdges();
	for (unsigned i = 0; i < edgesFromCurrPos.size(); i++)
	{
	  fprintf( m_logFile, "%d.%d.%d", edgesFromCurrPos[i]->getNext()->getSegmentID(),
			   edgesFromCurrPos[i]->getNext()->getLaneID(), edgesFromCurrPos[i]->getNext()->getWaypointID());
	  GPSPoint* closestPoint;
	  if (edgesFromCurrPos[i]->getNext()->getLaneID() != 0)
	  {
	    closestPoint = m_rndf->getWaypoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getLaneID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  else
	  {
	    closestPoint = m_rndf->getPerimeterPoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  if (closestPoint != NULL)
	  {
	    fprintf(m_logFile,"(%f, %f, %f, %f, %f)\n", closestPoint->getNorthing() - northingFront,
			      closestPoint->getEasting() - eastingFront,
			      180/PI * ( atan2(closestPoint->getEasting() - eastingFront, 
					       closestPoint->getNorthing() - northingFront)),
			      180/PI * headingDiff, 180/PI * headingLaneDiff);
	  }
	}
      }	
    }
  }  
  return currentPosAdded;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double TraversibilityGraphEstimation::getNorthing()
{
  return m_state.utmNorthing;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double TraversibilityGraphEstimation::getEasting()
{
  return m_state.utmEasting;
}
