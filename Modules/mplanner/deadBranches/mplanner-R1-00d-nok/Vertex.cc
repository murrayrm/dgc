/*!**
 * Morlan Liu and Nok Wongpiromsarn
 * December 8, 2006
 */

#include "Vertex.hh"
using namespace std;

Vertex::Vertex(int segmentID, int laneID, int waypointID)
{
  this->segmentID = segmentID;
  this->laneID = laneID;
  this->waypointID = waypointID;
}

Vertex::~Vertex()
{
  for(unsigned i = 0; i < edges.size(); i++)
    delete edges[i];
  delete previousVertex;
}

int Vertex::getSegmentID()
{
  return segmentID;
}

int Vertex::getLaneID()
{
  return laneID;
}

int Vertex::getWaypointID()
{
  return waypointID;
}

vector<Edge*> Vertex::getEdges()
{
  return edges;
}

void Vertex::addEdge(Edge* edge)
{
  edges.push_back(edge);
}

bool Vertex::removeEdge(Edge* edge)
{
  unsigned i = 0;
  bool edgeRemoved = false;
  while( i < edges.size() )
  {
    if(edges[i] == edge)
    {
      edges.erase(edges.begin()+i,edges.begin()+i+1);
      edgeRemoved = true;
    }
    else
      i++;
  }
  return edgeRemoved;
}

void Vertex::setCostToCome(float cost)
{
  costToCome = cost;
}

float Vertex::getCostToCome()
{
  return costToCome;
}

void Vertex::setPreviousVertex(Vertex* previous)
{
  previousVertex = previous;
}

Vertex* Vertex::getPreviousVertex()
{
  return previousVertex;
}

void Vertex::print()
{
  cout << segmentID << "." << laneID << "." << waypointID;
}
