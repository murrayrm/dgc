/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */

#include <assert.h>
#include <signal.h>
#include <sys/time.h>
#include "RoutePlanner.hh"
#include "interfaces/ActuatorState.h"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "rplannerDisplay.h"
#include "CmdArgs.hh"

#define DISTANCE_BETWEEN_FAILURE 5.0
#define PAUSE_TIMEOUT 8000000.0
#define TIME_BETWEEN_PAUSE 3000000.0
#define STOP_SPEED 0.2

using namespace std;

void sigintHandler(int /*sig*/);

RoutePlanner::RoutePlanner(CSparrowHawk* shp, pthread_mutex_t* shpMutex)
  : GcModule( "RoutePlanner", &m_controlStatus, &m_mergedDirective, 100000, 100000 ),
    m_snKey(CmdArgs::sn_key), 
    //    healthTalker(CmdArgs::sn_key, SNvehicleCapability, MODmissionplanner),
    m_nosparrow(CmdArgs::nosparrow), m_nopause(CmdArgs::nopause),
    m_graphEstimator(CmdArgs::sn_key,  CmdArgs::waitForStateFill)
{  
  /*
  if (CmdArgs::getSystemHealth)
    healthTalker.listen();
  */

  DGCcreateMutex(&m_contrDirectiveQMutex);
  m_RNDFFileName = CmdArgs::RNDFFileName;
  m_MDFFileName = CmdArgs::MDFFileName;

  if (CmdArgs::noillegalPassing) {
    NUM_FAILS_BEFORE_REM_EDGE = 1;
    NUM_CKPT_FAILS_BEFORE_REM_EDGE = 1;
  }
  else {
    NUM_FAILS_BEFORE_REM_EDGE = 2;
    NUM_CKPT_FAILS_BEFORE_REM_EDGE = 3;
  }

  if (CmdArgs::nobackup) {
    NUM_FAILS_BEFORE_BACKUP = 5;
    NUM_FAILS_BEFORE_SKIPPING_CHECKPOINT = 5;
  }
  else {
    NUM_FAILS_BEFORE_BACKUP = 5;
    NUM_FAILS_BEFORE_SKIPPING_CHECKPOINT = 8;
  }

  m_shp = shp;
  m_shpMutex = shpMutex;

  DEBUG_LEVEL = CmdArgs::debugLevel;

  if (DEBUG_LEVEL > 4 && m_nosparrow) {
    this->setLogLevel(9);
  }

  
  if (CmdArgs::verbose) {
    VERBOSITY_LEVEL = 1;
  }

  if (CmdArgs::logLevel > 0) {
    stringstream ssfilename("");
    stringstream ssMDF;
    ssMDF << "./" << m_MDFFileName;
    string tmpMDFname;
    string MDFFileNameStr = ssMDF.str();
    tmpMDFname.assign(MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("/")+1, 
		      MDFFileNameStr.begin()+MDFFileNameStr.find_last_of("."));
    ssfilename << CmdArgs::logPath  << "/mplanner-routeplanner-" << tmpMDFname;
    this->addLogfile(ssfilename.str());
    this->setLogLevel(CmdArgs::logLevel);
  }

  m_logData = CmdArgs::logData;
  m_logFile = CmdArgs::logFile;

  rtInterfaceSF = RTInterface::generateSouthface(CmdArgs::sn_key, this);
  mrInterfaceNF = MRInterface::generateNorthface(CmdArgs::sn_key, this);
  rtInterfaceSF->setStaleThreshold(20);
  mrInterfaceNF->setStaleThreshold(20);

  m_controlStatus.goalID = 0;
  m_controlStatus.status = RoutePlannerControlStatus::PENDING;

  m_rndf = CmdArgs::rndf;

  m_graphEstimator.init(m_rndf, CmdArgs::listenToMapper, m_nosparrow, m_logData, 
			m_logFile, DEBUG_LEVEL, CmdArgs::verbose);
  m_travGraph = &m_graphEstimator.getGraph();

  // m_graphEstimator.setCurrentLane(0,0);
  m_graphEstimator.setCurrentWaypoint(CmdArgs::startSegmentID, CmdArgs::startLaneID,
				      CmdArgs::startWaypointID);

  m_minSpeedLimits.resize(m_travGraph->getNumOfSegments() + m_travGraph->getNumOfZones() + 1);
  m_maxSpeedLimits.resize(m_travGraph->getNumOfSegments() + m_travGraph->getNumOfZones() + 1);

  for (unsigned i=0; i < m_minSpeedLimits.size(); i++) {
    m_minSpeedLimits[i] = MIN_SPEED_LIMIT;
    m_maxSpeedLimits[i] = MAX_SPEED_LIMIT;
  }

  if (!loadMDFFile(m_MDFFileName, m_rndf)) {
    stringstream s("");
    s << "Unable to load MDF file " << m_MDFFileName << ", exiting program\n";
    printError("constructor", s.str());
    exit(1);
  }

  if (DEBUG_LEVEL > 1) {
    cout << endl << endl << "RNDFGRAPH:" << endl;
    m_travGraph->print();
    cout << "Finished RNDFGRAPH" << endl;
  }
  if (m_logData) {
    fprintf ( m_logFile, "\nGraph:\n" );
    m_travGraph->log(m_logFile);
    fprintf ( m_logFile, "Finished RNDFGRAPH\n" );
    fflush(m_logFile);
  }

  m_currentCkptSegmentID = 0;
  m_currentCkptLaneID = 0;
  m_currentCkptWaypointID = 0;
  m_currentCkptGoalID = 0;
  m_missionID = 0;
  m_nextGoalID = 1;
  m_nextGoalStartSegmentID = 0;
  m_nextGoalStartLaneID = 0;
  m_nextGoalStartWaypointID = 0;

  m_gotEstop = true;
  m_northingBeforeEstop = m_graphEstimator.getNorthing(true);
  m_eastingBeforeEstop = m_graphEstimator.getEasting(false);
  m_distMovedDuringPause = 0;

  m_tplannerStarted = false;
  m_eomCompleted = false;
  m_lastFailedGoalID = 0;
  m_lastFailedEasting = 0;
  m_lastFailedNorthing = 0;
  m_numFails = 0;
  m_lastPauseID = 0;
  m_lastCompletedPauseID = 0;
  m_lastPauseT = 0;
  m_lastBackupID = 0;
  m_lastCompletedBackupID = 0;

  m_obstacleID = 0;

  DGCgettime(m_lastVehMovedT);
  m_timeStopped = 0;

  // Initialize sparrow variables
  for (int i=0; i < NUM_SEGGOALS_DISPLAYED; i++) {
    sparrowNextGoalIDs[i] = 0;
    sparrowNextSegments1[i] = 0;
    sparrowNextLanes1[i] = 0;
    sparrowNextWaypoints1[i] = 0;
    sparrowNextSegments2[i] = 0;
    sparrowNextLanes2[i] = 0;
    sparrowNextWaypoints2[i] = 0;
  }

  if (!m_nosparrow)
    SparrowDisplayLoop();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
RoutePlanner::~RoutePlanner() 
{
  delete m_travGraph;
  RTInterface::releaseSouthface( rtInterfaceSF );
  MRInterface::releaseNorthface( mrInterfaceNF );

  DGCdeleteMutex(&m_contrDirectiveQMutex);
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Sparrow-related functions */
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::SparrowDisplayLoop()
{
  // SparrowHawk display    
  DGClockMutex(m_shpMutex);
  /*
  CSparrowHawk &sh = SparrowHawk();  
  m_shp = &sh;
  */

  m_shp->add_page(rplannertable, "RoutePlanner");
  m_shp->rebind("rpsnkey", &m_snKey);
  m_shp->rebind("tplanner_goalID", &(m_segGoalsStatus.goalID));
  m_shp->rebind("numFails", &(m_numFails));
  m_shp->set_string("tplanner_status", "UNKNOWN");
  m_shp->set_string("reasonForFailure", "UNKNOWN");
  m_shp->set_readonly("rpsnkey");
  m_shp->set_readonly("tplanner_goalID");
  m_shp->set_readonly("tplanner_status");
  m_shp->set_readonly("numFails");
  m_shp->set_readonly("reasonForFailure");

  m_shp->set_string("rpRNDFFileName", m_RNDFFileName);
  m_shp->set_readonly("rpRNDFFileName");

  m_shp->set_string("rpMDFFileName", m_MDFFileName);
  m_shp->set_readonly("rpMDFFileName");

  m_shp->rebind("listenToEstop", &(CmdArgs::listenToEstop));
  m_shp->rebind("distMovedDuringPause", &m_distMovedDuringPause);
  m_shp->rebind("keepForwardProgress", &(CmdArgs::keepForwardProgress));
  m_shp->rebind("stopTimeout", &(CmdArgs::stopTimeout));
  m_shp->rebind("timeStopped", &m_timeStopped);
  m_shp->set_readonly("distMovedDuringPause");
  m_shp->set_readonly("timeStopped");

  m_shp->rebind("rpnextGoal1ID", &(sparrowNextGoalIDs[0]));
  m_shp->rebind("rpnextGoal1_segment1", &(sparrowNextSegments1[0]));
  m_shp->rebind("rpnextGoal1_lane1", &(sparrowNextLanes1[0]));
  m_shp->rebind("rpnextGoal1_waypoint1", &(sparrowNextWaypoints1[0]));
  m_shp->rebind("rpnextGoal1_segment2", &(sparrowNextSegments2[0]));
  m_shp->rebind("rpnextGoal1_lane2", &(sparrowNextLanes2[0]));
  m_shp->rebind("rpnextGoal1_waypoint2", &(sparrowNextWaypoints2[0]));
  m_shp->set_string("rpnextGoal1_type", "UNKNOWN");
  m_shp->rebind("rpminSpeed", &sparrowMinSpeed);
  m_shp->rebind("rpmaxSpeed", &sparrowMaxSpeed);
  m_shp->rebind("rpillegalPassingAllowed", &sparrowIllegalPassingAllowed);
  m_shp->rebind("rpstopAtExit", &sparrowStopAtExit);
  m_shp->rebind("rpisExitCheckpoint", &sparrowIsExitCheckpoint);
  m_shp->set_readonly("rpnextGoal1ID");
  m_shp->set_readonly("rpnextGoal1_segment1");
  m_shp->set_readonly("rpnextGoal1_lane1");
  m_shp->set_readonly("rpnextGoal1_waypoint1");
  m_shp->set_readonly("rpnextGoal1_segment2");
  m_shp->set_readonly("rpnextGoal1_lane2");
  m_shp->set_readonly("rpnextGoal1_waypoint2");
  m_shp->set_readonly("rpnextGoal1_type");
  m_shp->set_readonly("rpminSpeed");
  m_shp->set_readonly("rpmaxSpeed");
  m_shp->set_readonly("rpillegalPassingAllowed");
  m_shp->set_readonly("rpstopAtExit");
  m_shp->set_readonly("rpisExitCheckpoint");

  m_shp->rebind("rpnextGoal2ID", &(sparrowNextGoalIDs[1]));
  m_shp->rebind("rpnextGoal2_segment1", &(sparrowNextSegments1[1]));
  m_shp->rebind("rpnextGoal2_lane1", &(sparrowNextLanes1[1]));
  m_shp->rebind("rpnextGoal2_waypoint1", &(sparrowNextWaypoints1[1]));
  m_shp->rebind("rpnextGoal2_segment2", &(sparrowNextSegments2[1]));
  m_shp->rebind("rpnextGoal2_lane2", &(sparrowNextLanes2[1]));
  m_shp->rebind("rpnextGoal2_waypoint2", &(sparrowNextWaypoints2[1]));
  m_shp->set_string("rpnextGoal2_type", "UNKNOWN");
  m_shp->set_readonly("rpnextGoal2ID");
  m_shp->set_readonly("rpnextGoal2_segment1");
  m_shp->set_readonly("rpnextGoal2_lane1");
  m_shp->set_readonly("rpnextGoal2_waypoint1");
  m_shp->set_readonly("rpnextGoal2_segment2");
  m_shp->set_readonly("rpnextGoal2_lane2");
  m_shp->set_readonly("rpnextGoal2_waypoint2");
  m_shp->set_readonly("rpnextGoal2_type");

  m_shp->rebind("rpnextGoal3ID", &(sparrowNextGoalIDs[2]));
  m_shp->rebind("rpnextGoal3_segment1", &(sparrowNextSegments1[2]));
  m_shp->rebind("rpnextGoal3_lane1", &(sparrowNextLanes1[2]));
  m_shp->rebind("rpnextGoal3_waypoint1", &(sparrowNextWaypoints1[2]));
  m_shp->rebind("rpnextGoal3_segment2", &(sparrowNextSegments2[2]));
  m_shp->rebind("rpnextGoal3_lane2", &(sparrowNextLanes2[2]));
  m_shp->rebind("rpnextGoal3_waypoint2", &(sparrowNextWaypoints2[2]));
  m_shp->set_string("rpnextGoal3_type", "UNKNOWN");
  m_shp->set_readonly("rpnextGoal3ID");
  m_shp->set_readonly("rpnextGoal3_segment1");
  m_shp->set_readonly("rpnextGoal3_lane1");
  m_shp->set_readonly("rpnextGoal3_waypoint1");
  m_shp->set_readonly("rpnextGoal3_segment2");
  m_shp->set_readonly("rpnextGoal3_lane2");
  m_shp->set_readonly("rpnextGoal3_waypoint2");
  m_shp->set_readonly("rpnextGoal3_type");

  m_shp->rebind("rpnextGoal4ID", &(sparrowNextGoalIDs[3]));
  m_shp->rebind("rpnextGoal4_segment1", &(sparrowNextSegments1[3]));
  m_shp->rebind("rpnextGoal4_lane1", &(sparrowNextLanes1[3]));
  m_shp->rebind("rpnextGoal4_waypoint1", &(sparrowNextWaypoints1[3]));
  m_shp->rebind("rpnextGoal4_segment2", &(sparrowNextSegments2[3]));
  m_shp->rebind("rpnextGoal4_lane2", &(sparrowNextLanes2[3]));
  m_shp->rebind("rpnextGoal4_waypoint2", &(sparrowNextWaypoints2[3]));
  m_shp->set_string("rpnextGoal4_type", "UNKNOWN");
  m_shp->set_readonly("rpnextGoal4ID");
  m_shp->set_readonly("rpnextGoal4_segment1");
  m_shp->set_readonly("rpnextGoal4_lane1");
  m_shp->set_readonly("rpnextGoal4_waypoint1");
  m_shp->set_readonly("rpnextGoal4_segment2");
  m_shp->set_readonly("rpnextGoal4_lane2");
  m_shp->set_readonly("rpnextGoal4_waypoint2");
  m_shp->set_readonly("rpnextGoal4_type");

  m_shp->rebind("rpnextGoal5ID", &(sparrowNextGoalIDs[4]));
  m_shp->rebind("rpnextGoal5_segment1", &(sparrowNextSegments1[4]));
  m_shp->rebind("rpnextGoal5_lane1", &(sparrowNextLanes1[4]));
  m_shp->rebind("rpnextGoal5_waypoint1", &(sparrowNextWaypoints1[4]));
  m_shp->rebind("rpnextGoal5_segment2", &(sparrowNextSegments2[4]));
  m_shp->rebind("rpnextGoal5_lane2", &(sparrowNextLanes2[4]));
  m_shp->rebind("rpnextGoal5_waypoint2", &(sparrowNextWaypoints2[4]));
  m_shp->set_string("rpnextGoal5_type", "UNKNOWN");
  m_shp->set_readonly("rpnextGoal5ID");
  m_shp->set_readonly("rpnextGoal5_segment1");
  m_shp->set_readonly("rpnextGoal5_lane1");
  m_shp->set_readonly("rpnextGoal5_waypoint1");
  m_shp->set_readonly("rpnextGoal5_segment2");
  m_shp->set_readonly("rpnextGoal5_lane2");
  m_shp->set_readonly("rpnextGoal5_waypoint2");
  m_shp->set_readonly("rpnextGoal5_type");

  m_shp->rebind("rpnextGoal6ID", &(sparrowNextGoalIDs[5]));
  m_shp->rebind("rpnextGoal6_segment1", &(sparrowNextSegments1[5]));
  m_shp->rebind("rpnextGoal6_lane1", &(sparrowNextLanes1[5]));
  m_shp->rebind("rpnextGoal6_waypoint1", &(sparrowNextWaypoints1[5]));
  m_shp->rebind("rpnextGoal6_segment2", &(sparrowNextSegments2[5]));
  m_shp->rebind("rpnextGoal6_lane2", &(sparrowNextLanes2[5]));
  m_shp->rebind("rpnextGoal6_waypoint2", &(sparrowNextWaypoints2[5]));
  m_shp->set_string("rpnextGoal6_type", "UNKNOWN");
  m_shp->set_readonly("rpnextGoal6ID");
  m_shp->set_readonly("rpnextGoal6_segment1");
  m_shp->set_readonly("rpnextGoal6_lane1");
  m_shp->set_readonly("rpnextGoal6_waypoint1");
  m_shp->set_readonly("rpnextGoal6_segment2");
  m_shp->set_readonly("rpnextGoal6_lane2");
  m_shp->set_readonly("rpnextGoal6_waypoint2");
  m_shp->set_readonly("rpnextGoal6_type");

  m_shp->rebind("rpnextCheckpointIndex", &m_missionID);
  m_shp->rebind("rpnextCheckpointSegmentID", &m_currentCkptSegmentID);
  m_shp->rebind("rpnextCheckpointLaneID", &m_currentCkptLaneID);
  m_shp->rebind("rpnextCheckpointWaypointID", &m_currentCkptWaypointID);
  m_shp->rebind("rpnextCheckpointGoalID", &m_currentCkptGoalID);
  m_shp->set_readonly("rpnextCheckpointIndex");
  m_shp->set_readonly("rpnextCheckpointSegmentID");
  m_shp->set_readonly("rpnextCheckpointLaneID");
  m_shp->set_readonly("rpnextCheckpointWaypointID");
  m_shp->set_readonly("rpnextCheckpointGoalID");
  

  m_shp->rebind("obstacleID", &obstacleID);
  m_shp->rebind("obstacleSegmentID", &obstacleSegmentID);
  m_shp->rebind("obstacleLaneID", &obstacleLaneID);
  m_shp->rebind("obstacleWaypointID", &obstacleWaypointID);

  m_shp->set_notify("sparrowInsertObstacle", this, &RoutePlanner::sparrowInsertObstacle);
  m_shp->set_notify("sparrowRemoveObstacles", this, &RoutePlanner::sparrowRemoveObstacles);
  DGCunlockMutex(m_shpMutex);
  m_shp->run();
  signal(SIGINT, sigintHandler);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::UpdateSparrowVariablesLoop()
{
  deque<unsigned> execGoalIDs = rtInterfaceSF->orderedDirectivesWaitingForResponse();
  deque<SegGoals> executingGoals;

  for (unsigned i=0; i<execGoalIDs.size(); i++) {
    executingGoals.push_back(rtInterfaceSF->getSentDirective(execGoalIDs[i]));
  }
  if (executingGoals.size() > 0) {
    sparrowMinSpeed = executingGoals.front().minSpeedLimit;
    sparrowMaxSpeed = executingGoals.front().maxSpeedLimit;
    sparrowIllegalPassingAllowed = executingGoals.front().illegalPassingAllowed;
    sparrowStopAtExit = executingGoals.front().stopAtExit;
    sparrowIsExitCheckpoint = executingGoals.front().isExitCheckpoint;
  }

  DGClockMutex(m_shpMutex);
  if (m_segGoalsStatus.status == SegGoalsStatus::ACCEPTED)
    m_shp->set_string("tplanner_status", "ACCEPT   ");
  else if (m_segGoalsStatus.status == SegGoalsStatus::REJECTED)
    m_shp->set_string("tplanner_status", "REJECT   ");
  else if (m_segGoalsStatus.status == SegGoalsStatus::COMPLETED)
    m_shp->set_string("tplanner_status", "COMPLETED");
  else if (m_segGoalsStatus.status == SegGoalsStatus::FAILED)
    m_shp->set_string("tplanner_status", "FAILED   ");
  else
    m_shp->set_string("tplanner_status", "UNKNOWN  ");

  if (m_segGoalsStatus.status == SegGoalsStatus::FAILED) {
    if (m_segGoalsStatus.reason == SegGoalsStatus::BLOCKED_LANE)
      m_shp->set_string("reasonForFailure", "BL     ");
    else if (m_segGoalsStatus.reason == SegGoalsStatus::BLOCKED_ROAD)
      m_shp->set_string("reasonForFailure", "BR     ");
    else if (m_segGoalsStatus.reason == SegGoalsStatus::KNOWLEDGE)
      m_shp->set_string("reasonForFailure", "K      ");
    else if (m_segGoalsStatus.reason == SegGoalsStatus::ACTUATOR_FAILURE)
      m_shp->set_string("reasonForFailure", "AF     ");
    else if (m_segGoalsStatus.reason == SegGoalsStatus::LOST)
      m_shp->set_string("reasonForFailure", "L      ");
    else if (m_segGoalsStatus.reason == SegGoalsStatus::PREEMPTED_BY_PAUSE)
      m_shp->set_string("reasonForFailure", "PP     ");
    else
      m_shp->set_string("reasonForFailure", "UNKNOWN");
  }
  else {
    m_shp->set_string("reasonForFailure", "UNKNOWN");
  }

  DGCunlockMutex(m_shpMutex);

  int i = 0;
  deque<SegGoals> segGoalsSeq;	
  DGClockMutex(&m_contrDirectiveQMutex);
  if (m_contrDirectiveQ.size() > 0)
    segGoalsSeq.assign(m_contrDirectiveQ.begin(), m_contrDirectiveQ.end());
  DGCunlockMutex(&m_contrDirectiveQMutex);

  char* nextGoalType;

  while (i < NUM_SEGGOALS_DISPLAYED && executingGoals.size() > 0) {
    sparrowNextGoalIDs[i] = (executingGoals.front()).goalID;
    sparrowNextSegments1[i] = (executingGoals.front()).entrySegmentID;
    sparrowNextLanes1[i] = (executingGoals.front()).entryLaneID;
    sparrowNextWaypoints1[i] = (executingGoals.front()).entryWaypointID;
    sparrowNextSegments2[i] = (executingGoals.front()).exitSegmentID;
    sparrowNextLanes2[i] = (executingGoals.front()).exitLaneID;
    sparrowNextWaypoints2[i] = (executingGoals.front()).exitWaypointID;

    switch (i) {
    case 0:
      nextGoalType = "rpnextGoal1_type";
      break;
    case 1:
      nextGoalType = "rpnextGoal2_type";
      break;
    case 2:
      nextGoalType = "rpnextGoal3_type";
      break;
    case 3:
      nextGoalType = "rpnextGoal4_type";
      break;
    case 4:
      nextGoalType = "rpnextGoal5_type";
      break;
    default:
      nextGoalType = "rpnextGoal6_type";
    }

    DGClockMutex(m_shpMutex);
    if ((executingGoals.front()).segment_type == SegGoals::ROAD_SEGMENT)
      m_shp->set_string(nextGoalType, "ROAD_SEGMENT         ");
    else if ((executingGoals.front()).segment_type == SegGoals::PARKING_ZONE)
      m_shp->set_string(nextGoalType, "PARKING_ZONE         ");
    else if ((executingGoals.front()).segment_type == SegGoals::INTERSECTION) {
      if ((executingGoals.front()).intersection_type == SegGoals::INTERSECTION_LEFT)
	m_shp->set_string(nextGoalType, "INTERSECTION LEFT    ");
      else if ((executingGoals.front()).intersection_type == SegGoals::INTERSECTION_RIGHT)
	m_shp->set_string(nextGoalType, "INTERSECTION RIGHT   ");
      else
	m_shp->set_string(nextGoalType, "INTERSECTION STRAIGHT");
    }
    else if ((executingGoals.front()).segment_type == SegGoals::PREZONE)
      m_shp->set_string(nextGoalType, "PREZONE              ");
    else if ((executingGoals.front()).segment_type == SegGoals::UTURN)
      m_shp->set_string(nextGoalType, "UTURN                ");
    else if ((executingGoals.front()).segment_type == SegGoals::RESET)
      m_shp->set_string(nextGoalType, "RESET                ");
    else if ((executingGoals.front()).segment_type == SegGoals::DRIVE_AROUND)
      m_shp->set_string(nextGoalType, "DRIVE_AROUND         ");
    else if ((executingGoals.front()).segment_type == SegGoals::BACK_UP)
      m_shp->set_string(nextGoalType, "BACK_UP              ");
    else if ((executingGoals.front()).segment_type == SegGoals::END_OF_MISSION)
      m_shp->set_string(nextGoalType, "END_OF_MISSION       ");
    else if ((executingGoals.front()).segment_type == SegGoals::EMERGENCY_STOP)
      m_shp->set_string(nextGoalType, "EMERGENCY_STOP       ");
    else
      m_shp->set_string(nextGoalType, "UNKNOWN              ");
    DGCunlockMutex(m_shpMutex);

    executingGoals.pop_front();
    i++;
  }

  while (i < NUM_SEGGOALS_DISPLAYED && segGoalsSeq.size() > 0) {
    sparrowNextGoalIDs[i] = (segGoalsSeq.front()).goalID;
    sparrowNextSegments1[i] = (segGoalsSeq.front()).entrySegmentID;
    sparrowNextLanes1[i] = (segGoalsSeq.front()).entryLaneID;
    sparrowNextWaypoints1[i] = (segGoalsSeq.front()).entryWaypointID;
    sparrowNextSegments2[i] = (segGoalsSeq.front()).exitSegmentID;
    sparrowNextLanes2[i] = (segGoalsSeq.front()).exitLaneID;
    sparrowNextWaypoints2[i] = (segGoalsSeq.front()).exitWaypointID;

    switch (i) {
    case 0:
      nextGoalType = "rpnextGoal1_type";
      break;
    case 1:
      nextGoalType = "rpnextGoal2_type";
      break;
    case 2:
      nextGoalType = "rpnextGoal3_type";
      break;
    case 3:
      nextGoalType = "rpnextGoal4_type";
      break;
    case 4:
      nextGoalType = "rpnextGoal5_type";
      break;
    default:
      nextGoalType = "rpnextGoal6_type";
    }

    DGClockMutex(m_shpMutex);
    if ((segGoalsSeq.front()).segment_type == SegGoals::ROAD_SEGMENT)
      m_shp->set_string(nextGoalType, "ROAD_SEGMENT         ");
    else if ((segGoalsSeq.front()).segment_type == SegGoals::PARKING_ZONE)
      m_shp->set_string(nextGoalType, "PARKING_ZONE         ");
    else if ((segGoalsSeq.front()).segment_type == SegGoals::INTERSECTION)  {
      if ((segGoalsSeq.front()).intersection_type == SegGoals::INTERSECTION_LEFT)
	m_shp->set_string(nextGoalType, "INTERSECTION LEFT    ");
      else if ((segGoalsSeq.front()).intersection_type == SegGoals::INTERSECTION_RIGHT)
	m_shp->set_string(nextGoalType, "INTERSECTION RIGHT   ");
      else
	m_shp->set_string(nextGoalType, "INTERSECTION STRAIGHT");
    }
    else if ((segGoalsSeq.front()).segment_type == SegGoals::PREZONE)
      m_shp->set_string(nextGoalType, "PREZONE              ");
    else if ((segGoalsSeq.front()).segment_type == SegGoals::UTURN)
      m_shp->set_string(nextGoalType, "UTURN                ");
    else if ((segGoalsSeq.front()).segment_type == SegGoals::RESET)
      m_shp->set_string(nextGoalType, "RESET                ");
    else if ((segGoalsSeq.front()).segment_type == SegGoals::DRIVE_AROUND)
      m_shp->set_string(nextGoalType, "DRIVE_AROUND         ");
    else if ((segGoalsSeq.front()).segment_type == SegGoals::BACK_UP)
      m_shp->set_string(nextGoalType, "BACK_UP              ");
    else if ((segGoalsSeq.front()).segment_type == SegGoals::END_OF_MISSION)
      m_shp->set_string(nextGoalType, "END_OF_MISSION       ");
    else if ((segGoalsSeq.front()).segment_type == SegGoals::EMERGENCY_STOP)
      m_shp->set_string(nextGoalType, "EMERGENCY_STOP       ");
    else
      m_shp->set_string(nextGoalType, "UNKNOWN              ");
    DGCunlockMutex(m_shpMutex);

    segGoalsSeq.pop_front();
    i++;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sparrowInsertObstacle()
{  
  m_graphEstimator.insertObstacle(obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::insertObstacle(int obstacleID, int obstacleSegmentID, 
				  int obstacleLaneID, int obstacleWaypointID)
{  
  m_graphEstimator.insertObstacle(obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sparrowRemoveObstacles()
{  
  m_graphEstimator.removeAllObstacles();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// GCmodule functions */
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

void RoutePlanner::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  RoutePlannerMergedDirective * rcmd = static_cast<RoutePlannerMergedDirective*>(md);
  deque<MControlDirective>* mergedDir = &(rcmd->directives);
  RoutePlannerControlStatus*  contrStatus = static_cast<RoutePlannerControlStatus*>(cs);

  stringstream s("");
  s << "RoutePlanner::arbitrate: Control status: goalID = " << contrStatus->goalID << "  status = " 
    << contrStatus->status << "\n";
  print(s.str(), 3);

  // Determine if any directive is completed or failed.
  if (contrStatus->goalID != 0 && contrStatus->status != RoutePlannerControlStatus::PENDING) {
    MControlDirectiveResponse rpcs;

    if (contrStatus->status == RoutePlannerControlStatus::COMPLETED) {
      rpcs.goalID = contrStatus->goalID;
      rpcs.status = MControlDirectiveResponse::COMPLETED;
      if (mergedDir->size() == 0 && !m_eomCompleted) {
	s.str("");
	s << "contrStatus goalID = " << contrStatus->goalID << " mergedDir is empty\n";
	printError("arbitrate", s.str());
      }
      else if (mergedDir->size() > 0 && contrStatus->goalID == mergedDir->front().goalID) {
	mergedDir->pop_front();
      }
      else if (mergedDir->size() > 0) {
	s.str("");
	s << "contrStatus goalID = " << contrStatus->goalID
	  << " mergedDir goalID = " << mergedDir->front().goalID << "\n";
	printError("arbitrate", s.str());
      }
      mrInterfaceNF->sendResponse(&rpcs, rpcs.goalID);
    }
    else if (contrStatus->status == RoutePlannerControlStatus::FAILED) { 
      // clear all the received directive
      if (mergedDir->size() == 0 && !m_eomCompleted) {
	s.str("");
	s << "contrStatus goalID = " << contrStatus->goalID << " mergedDir is empty" << "\n";
	printError("arbitrate", s.str());
      }
      else if (mergedDir->size() > 0 && contrStatus->goalID == mergedDir->front().goalID) {
	mergedDir->pop_front();
      }
      else if (mergedDir->size() > 0) {
	s.str("");
	s << "contrStatus goalID = " << contrStatus->goalID
	  << " mergedDir goalID = " << mergedDir->front().goalID << "\n";
	printError("arbitrate", s.str());
      }

      while (mrInterfaceNF->haveNewDirective()) {
	MControlDirective newDirective;
	mrInterfaceNF->getNewDirective(&newDirective);	
	rpcs.goalID = newDirective.goalID;
	rpcs.status = MControlDirectiveResponse::FAILED;
	rpcs.reason = MControlDirectiveResponse::PREVIOUS_DIR_FAILURE;
	mrInterfaceNF->sendResponse(&rpcs, rpcs.goalID);
      }

      rpcs.goalID = contrStatus->goalID;
      rpcs.status = MControlDirectiveResponse::FAILED;
      rpcs.reason = contrStatus->reason; 
      mrInterfaceNF->sendResponse(&rpcs, rpcs.goalID);

      while ( mergedDir->size() > 0) {
	rpcs.goalID = mergedDir->front().goalID;
	rpcs.status = MControlDirectiveResponse::FAILED;
	rpcs.reason = MControlDirectiveResponse::PREVIOUS_DIR_FAILURE;
	mergedDir->pop_front();
	mrInterfaceNF->sendResponse(&rpcs, rpcs.goalID);
      }
    }
    else {
      s.str("");
      s << "Unknown control status\n";
      printError("arbitrate", s.str());
    }
  }

  // Wait for Mission Control to send new directive
  usleep(500000);

  // Get all the new directives and put it in the queue
  while (mrInterfaceNF->haveNewDirective()) {
    if (m_eomCompleted) {
      m_eomCompleted = false;
    }
    MControlDirective newDirective;
    mrInterfaceNF->getNewDirective(&newDirective);
    if (newDirective.type == MControlDirective::RESET || 
	newDirective.type == MControlDirective::EMERGENCY_STOP) {
      MControlDirectiveResponse rpcs;
      while ( mergedDir->size() > 0) {
	rpcs.goalID = mergedDir->front().goalID;
	rpcs.status = MControlDirectiveResponse::FAILED;
	rpcs.reason = MControlDirectiveResponse::PREEMPTED_BY_PAUSE;
	mergedDir->pop_front();
	mrInterfaceNF->sendResponse(&rpcs, rpcs.goalID);
      }
      mergedDir->push_back(newDirective);
      break;
    }
    mergedDir->push_back(newDirective);
  }

  // Reject directives if the queue is too large
  while (mergedDir->size() > LEAST_NUM_MISSION_RPLANNER_STORED) {
    MControlDirectiveResponse rejectResponse;
    MControlDirective rejectedDir = mergedDir->back();
    mergedDir->pop_back();
    rejectResponse.goalID = rejectedDir.goalID;
    rejectResponse.status = MControlDirectiveResponse::REJECTED;
    s.str("");
    s << "Reject directive " << rejectedDir.goalID << "\n";
    printError("arbitrate", s.str());
    mrInterfaceNF->sendResponse(&rejectResponse, rejectResponse.goalID);    
  }

  s.str("");
  s << "RoutePlanner: Merged directive: \n";
  for (unsigned i=0; i<mergedDir->size(); i++) {
    s << "    RoutePlanner: " << mergedDir->at(i).toString(); 
  } 
  s << "\n";
  print(s.str(), 2);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::control(ControlStatus* cs, MergedDirective* md)
{
  if (!m_nosparrow) {
    UpdateSparrowVariablesLoop();
  }

  RoutePlannerMergedDirective* rcmd = static_cast<RoutePlannerMergedDirective*>(md);
  deque<MControlDirective>* mergedDir = &(rcmd->directives);
  RoutePlannerControlStatus* contrStatus = static_cast<RoutePlannerControlStatus*>(cs);
  stringstream s("");

  // Check that the size of m_prevMergedDirectiveControlGoalIDs is less than
  // the size of m_prevMergedDirectives
  if (m_prevMergedDirectives.size() < m_prevMergedDirectiveControlGoalIDs.size()) {
    s.str("");
    s << "m_prevMergedDirectives.size() = "
      << m_prevMergedDirectives.size() << " < m_prevMergedDirectiveControlGoalIDs.size() = "
      << m_prevMergedDirectiveControlGoalIDs.size() << "\n";
    printError("control", s.str());
    m_prevMergedDirectives.clear();
    m_prevMergedDirectiveControlGoalIDs.clear();
  }

  // First see if the previous merged directive is completed.
  if (m_prevMergedDirectiveControlGoalIDs.size() > 0 &&
      rtInterfaceSF->isStatus(SegGoalsStatus::COMPLETED, m_prevMergedDirectiveControlGoalIDs.front()))
  {
    // Reset the start waypoint.
    if (m_prevMergedDirectives.front().type == MControlDirective::NEXT_CHECKPOINT ||
	m_prevMergedDirectives.front().type == MControlDirective::DRIVE_AROUND ||
	m_prevMergedDirectives.front().type == MControlDirective::BACK_UP) {
      CmdArgs::startSegmentID = 0;
      CmdArgs::startLaneID = 0;
      CmdArgs::startWaypointID = 0;
    }

    if ((unsigned)m_segGoalsStatus.goalID <= m_prevMergedDirectiveControlGoalIDs.front()) {
      // Update the latest status
      m_segGoalsStatus.goalID = m_prevMergedDirectiveControlGoalIDs.front();
      m_segGoalsStatus.status = SegGoalsStatus::COMPLETED;
      s.str("");
      s << "RoutePlanner::control: Received response for " << m_segGoalsStatus.toString() << "\n"; 
      print(s.str(), 1);

      // Update current lane
      if (m_nextGoalID > m_segGoalsStatus.goalID + 1 &&
	  (rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID)).segment_type != SegGoals::RESET &&
	  (rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID)).segment_type != SegGoals::EMERGENCY_STOP ) {
	SegGoals nextGoal;
	DGClockMutex(&m_contrDirectiveQMutex);
	if (m_contrDirectiveQ.size() > 0 && m_contrDirectiveQ.front().goalID == m_segGoalsStatus.goalID + 1) 
	  nextGoal  = m_contrDirectiveQ.front();
	else
	  nextGoal  = rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID + 1);
	DGCunlockMutex(&m_contrDirectiveQMutex);

	if (nextGoal.segment_type == SegGoals::ROAD_SEGMENT || 
	    nextGoal.segment_type == SegGoals::PARKING_ZONE ||
	    nextGoal.segment_type == SegGoals::INTERSECTION ||
	    nextGoal.segment_type == SegGoals::PREZONE ||
	    nextGoal.segment_type == SegGoals::UTURN ) {
	  s.str("");
	  s << "RoutePlanner::control: goal " << m_segGoalsStatus.goalID 
	    << " is completed. Setting current lane to "
	    << nextGoal.exitSegmentID << "." << nextGoal.exitLaneID << "\n";
	  print(s.str(), 2);
	  m_graphEstimator.setCurrentLane(nextGoal.exitSegmentID, nextGoal.exitLaneID);
	}
	SegGoals completedGoal = rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID);
	if (completedGoal.segment_type == SegGoals::DRIVE_AROUND || 
	    completedGoal.segment_type == SegGoals::BACK_UP) {
	  s.str("");
	  s << "RoutePlanner::control: goal " << m_segGoalsStatus.goalID 
	    << " (DRIVE_AROUND or BACK_UP) is completed. Setting current lane to 0.0\n";
	  print(s.str(), 2);
	  m_graphEstimator.setCurrentLane(0,0);
	}
      }
    }

    s.str("");
    s << "RoutePlanner::control: Merged directive " << m_prevMergedDirectives.front().goalID
      << "(waypoint " << m_prevMergedDirectives.front().segmentID << "."
      << m_prevMergedDirectives.front().laneID << "."
      << m_prevMergedDirectives.front().waypointID << ") is completed\n";
    print(s.str(), 1);
    
    if (m_prevMergedDirectives.front().type == MControlDirective::END_OF_MISSION) {
      m_eomCompleted = true;
    }
    
    if (m_prevMergedDirectiveControlGoalIDs.front() == m_lastPauseID)
      m_lastCompletedPauseID = m_lastPauseID;
    
    if (mergedDir->size() > 0 && 
	m_prevMergedDirectives.front().goalID == mergedDir->front().goalID) {
      contrStatus->goalID = m_prevMergedDirectives.front().goalID;
      contrStatus->status = RoutePlannerControlStatus::COMPLETED;
	
      m_prevMergedDirectives.pop_front();
      m_prevMergedDirectiveControlGoalIDs.pop_front();
      
      return;
    }
  }

  // If not, continue planning
  bool replan = false;
  bool goalFailed = false;
  bool startFromCurrent = false;
  bool sendPause = false;

  if (mergedDir->size() == 0) {
    s.str("");
    s << "RoutePlanner::control: no merged directive \n";
    print(s.str(), 3);
    return;
  }

  if (mergedDir->front().type == MControlDirective::RESET ||
      mergedDir->front().type == MControlDirective::EMERGENCY_STOP) {
    if (m_nopause) {
      s.str("");
      s << "RoutePlanner::control: completed ";
      if (mergedDir->front().type == MControlDirective::RESET)
	s << "RESET";
      else
	s << "EMERGENCY_STOP";
      s << " (goalID = " << mergedDir->front().goalID << ")\n";
      print(s.str(),1);
      contrStatus->goalID = mergedDir->front().goalID;
      contrStatus->status = RoutePlannerControlStatus::COMPLETED;
      m_prevMergedDirectives.clear();
      s.str("");
      s << "RoutePlanner::control: Got RESET or EMERGENCY_STOP merged directive. "
	<< "Setting current lane to 0.0\n";
      print(s.str(), 2);
      // m_graphEstimator.setCurrentLane(0,0);
      m_graphEstimator.setCurrentWaypoint(CmdArgs::startSegmentID,
					  CmdArgs::startLaneID,
					  CmdArgs::startWaypointID);
      resetGoals();
      return;
    }
    else {
      if (m_prevMergedDirectives.size() == 0 || 
	  mergedDir->front().goalID != m_prevMergedDirectives.front().goalID) {
	m_prevMergedDirectives.clear();
	m_prevMergedDirectiveControlGoalIDs.clear();
	m_prevMergedDirectives.push_back(mergedDir->front());
	// m_graphEstimator.setCurrentLane(0,0);
	m_graphEstimator.setCurrentWaypoint(CmdArgs::startSegmentID,
					    CmdArgs::startLaneID,
					    CmdArgs::startWaypointID);
	s.str("");
	s << "RoutePlanner::control: Got RESET or EMERGENCY_STOP merged directive. "
	  << "Setting current lane to 0.0\n";
	print(s.str(), 2);
      }
    }
  }


  if (mergedDir->front().type != MControlDirective::NEXT_CHECKPOINT &&
      mergedDir->front().type != MControlDirective::END_OF_MISSION &&
      mergedDir->front().type != MControlDirective::RESET &&
      mergedDir->front().type != MControlDirective::EMERGENCY_STOP) {
    s.str("");
    s << "Got " << mergedDir->front().type << "\n";
    printError("control", s.str());
    return;
  }

  // Now the first merged directive should be either NEXT_CHECKPOINT, END_OF_MISSION,
  // RESET or EMERGENCY_STOP
  // Check that the new merged directive is valid
  for (unsigned i=0; i<m_prevMergedDirectives.size(); i++) {
    if (m_prevMergedDirectives.at(i).goalID != mergedDir->at(i).goalID) {
      s.str("");
      s << "m_prevMergedDirectives[" 
	<< i << "].goalID = " << m_prevMergedDirectives.at(i).goalID << "mergedDir[" 
	<< i << "].goalID = " << mergedDir->at(i).goalID << "\n";
      printError("control", s.str());
      m_prevMergedDirectives.clear();
      m_prevMergedDirectiveControlGoalIDs.clear();
      return;
    }
  }
  for (unsigned i=m_prevMergedDirectives.size(); i<mergedDir->size(); i++) {
    m_prevMergedDirectives.push_back(mergedDir->at(i));
  }

  // At this point m_prevMergedDirectives should be exactly the same as mergedDir.
  m_currentCkptSegmentID = mergedDir->front().segmentID;
  m_currentCkptLaneID = mergedDir->front().laneID;
  m_currentCkptWaypointID = mergedDir->front().waypointID;
  if (m_prevMergedDirectiveControlGoalIDs.size() > 0)
    m_currentCkptGoalID = m_prevMergedDirectiveControlGoalIDs.front();
  m_missionID = mergedDir->front().nextCheckpointIndex + 1;


  // Plan from current position if we just switch to RUN and we've moved more than
  // 3 meters during PAUSE.
  if (CmdArgs::listenToEstop) {
    if (m_graphEstimator.getEstopStatus() != EstopRun)
      DGCgettime(m_lastPauseT);

    if(m_graphEstimator.getDstopStatus() != EstopRun) {
      m_gotEstop = true;
      s.str("");
      s << "RoutePlanner::control: I'm not in RUN \n";
      print(s.str(), 2);
    }
    else {
      if (!m_gotEstop) {
	m_northingBeforeEstop = m_graphEstimator.getNorthing(true);
	m_eastingBeforeEstop = m_graphEstimator.getEasting(false);
      }
      else {
	m_gotEstop = false;
	m_distMovedDuringPause = sqrt( pow(m_graphEstimator.getNorthing(true) - m_northingBeforeEstop, 2) + 
				       pow(m_graphEstimator.getEasting(false) - m_eastingBeforeEstop, 2) );
	s.str("");
	s << "RoutePlanner::control: Switch to RUN. Moved " << m_distMovedDuringPause << " during PAUSE \n";
	print(s.str(), 2);
	if (m_distMovedDuringPause > 3) {
	  replan = true;
	  sendPause = true;
	  s.str("");
	  s << "RoutePlanner::control: Moved more than 3 meters during PAUSE. Replan from current position.\n";
	  print(s.str(), 1);
	  if (m_distMovedDuringPause > 5) {
	    s.str("");
	    s << "RoutePlanner::control: Moved more than 10 meters during PAUSE. Reset current lane.\n";
	    print(s.str(), 1);
	    //m_graphEstimator.setCurrentLane(0,0);
	    m_graphEstimator.setCurrentWaypoint(CmdArgs::startSegmentID, CmdArgs::startLaneID,
						CmdArgs::startWaypointID);
	  }
	}
      }
    }
  }

  s.str("");
  s << "RoutePlanner::control: Did I just get new status... : " << rtInterfaceSF->haveNewStatus() << "\n";
  print(s.str(), 4);

  if (rtInterfaceSF->haveNewStatus()) {
    m_segGoalsStatus = *(rtInterfaceSF->getLatestStatus());
    s.str("");
    s << "RoutePlanner::control: Received response for " << m_segGoalsStatus.toString() << "\n"; 
    print(s.str(), 1);
    m_tplannerStarted = true;

    // reset m_lastCompletedBackupID so we can move on
    if (m_segGoalsStatus.goalID == (int)m_lastBackupID) {
      m_lastCompletedBackupID = m_lastBackupID;
    }

    if (m_segGoalsStatus.goalID == 0) {
      replan = true;
      sendPause = true;
      m_graphEstimator.clearRemovedEdges();
      m_nextGoalStartSegmentID = 0;
      m_nextGoalStartLaneID = 0;
      m_nextGoalStartWaypointID = 0;
      s.str("");
      s << "RoutePlanner::control: tplanner just starts listening...I'll replan\n";
      print(s.str(), -1);
      m_nextGoalID = 1;
    }
    else if(m_segGoalsStatus.status == SegGoalsStatus::FAILED &&
	    m_segGoalsStatus.reason != SegGoalsStatus::PREEMPTED_BY_PAUSE) {
      replan = true;
      SegGoals failedGoal = rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID);

      // If we can't back up, increase m_numFails so we skip the checkpoint
      if (m_segGoalsStatus.goalID == (int)m_lastBackupID) {
	m_numFails = NUM_FAILS_BEFORE_SKIPPING_CHECKPOINT;
      }

      // Insert obstacle to the graph
      if (m_segGoalsStatus.reason == SegGoalsStatus::BLOCKED_LANE ||
	  m_segGoalsStatus.reason == SegGoalsStatus::BLOCKED_ROAD) {

	goalFailed = true;
	if (failedGoal.entrySegmentID != 0) {
	  s.str("");
	  s << "RoutePlanner::control: inserting obstacle at " << failedGoal.entrySegmentID
	    << "." << failedGoal.entryLaneID << "." << failedGoal.entryWaypointID << "\n";
	  print(s.str(), 0);
	  m_graphEstimator.insertObstacle(m_obstacleID, failedGoal.entrySegmentID, 
					  failedGoal.entryLaneID, failedGoal.entryWaypointID,
					  60*3);
	  m_obstacleID++;
	}
	else {
	  s.str("");
	  s << "RoutePlanner::control: inserting obstacle at " << failedGoal.exitSegmentID
	    << "." << failedGoal.exitLaneID << "." << failedGoal.exitWaypointID << "\n";
	  print(s.str(), 0);
	  m_graphEstimator.insertObstacle(m_obstacleID, failedGoal.exitSegmentID, 
					  failedGoal.exitLaneID, failedGoal.exitWaypointID,
					  60*3);
	  m_obstacleID++;
	
	  if (failedGoal.exitWaypointID > 1)
	    m_graphEstimator.insertObstacle(m_obstacleID, failedGoal.exitSegmentID, 
					    failedGoal.exitLaneID, failedGoal.exitWaypointID - 1,
					    60*3);
	  m_obstacleID++;
	}
      }
      s.str("");
      s << "RoutePlanner::control: Goal " << m_segGoalsStatus.goalID << " failed : Replanning" << "\n";
      print(s.str(), -1);

      double currentEasting = m_graphEstimator.getEasting(true);
      double currentNorthing = m_graphEstimator.getNorthing(false);

      m_nextGoalID = m_segGoalsStatus.goalID;
      double distFromPrevFailure = sqrt( pow(currentEasting - m_lastFailedEasting, 2) + 
					 pow(currentNorthing - m_lastFailedNorthing, 2) );

      if ((unsigned)m_segGoalsStatus.goalID == m_lastFailedGoalID ||
	  failedGoal.segment_type == SegGoals::BACK_UP) {
	m_numFails++;
	s.str("");
	s << "RoutePlanner::control: incrementing m_numFails to " << m_numFails << " for goal "
	  << m_segGoalsStatus.goalID << " -- distance from previous failure = "
	  << distFromPrevFailure << " Setting m_lastFailedGoalID to " << m_segGoalsStatus.goalID << "\n";
	print(s.str(), -1);
	m_lastFailedGoalID = m_segGoalsStatus.goalID;
      }
      else {
	s.str("");
	s << "RoutePlanner::control: resetting m_numFails to " << 1 << " for goal "
	  << m_segGoalsStatus.goalID << " (m_lastFailedGoalID = "
	  << m_lastFailedGoalID << ")\n";
	print(s.str(), -1);
	m_lastFailedGoalID = m_segGoalsStatus.goalID;
	m_numFails = 1;
	m_lastFailedEasting = currentEasting;
	m_lastFailedNorthing = currentNorthing;
      }

      if (m_nextGoalID == 0) {
	m_nextGoalID = 1;
      }
    }
    else if(m_segGoalsStatus.status == SegGoalsStatus::COMPLETED) {
      SegGoals completedGoal = rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID);

      if (completedGoal.segment_type != SegGoals::BACK_UP)
	m_numFails = 0;

      // Reset the start waypoint.
      if (completedGoal.segment_type != SegGoals::RESET &&
	  completedGoal.segment_type != SegGoals::END_OF_MISSION &&
	  completedGoal.segment_type != SegGoals::EMERGENCY_STOP) {
	CmdArgs::startSegmentID = 0;
	CmdArgs::startLaneID = 0;
	CmdArgs::startWaypointID = 0;
      }

      if (m_segGoalsStatus.goalID == (int)m_lastPauseID)
	m_lastCompletedPauseID = m_lastPauseID;

      // Update current lane
      if (m_nextGoalID > m_segGoalsStatus.goalID + 1 && 
	  (rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID)).segment_type != SegGoals::RESET &&
	  (rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID)).segment_type != SegGoals::EMERGENCY_STOP ) {
	SegGoals nextGoal;
	DGClockMutex(&m_contrDirectiveQMutex);
	if (m_contrDirectiveQ.size() > 0 && m_contrDirectiveQ.front().goalID == m_segGoalsStatus.goalID + 1) 
	  nextGoal  = m_contrDirectiveQ.front();
	else
	  nextGoal  = rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID + 1);
	DGCunlockMutex(&m_contrDirectiveQMutex);

	if (nextGoal.segment_type == SegGoals::ROAD_SEGMENT || 
	    nextGoal.segment_type == SegGoals::PARKING_ZONE ||
	    nextGoal.segment_type == SegGoals::INTERSECTION ||
	    nextGoal.segment_type == SegGoals::PREZONE ||
	    nextGoal.segment_type == SegGoals::UTURN ) {
	  s.str("");
	  s << "RoutePlanner::control: goal " << m_segGoalsStatus.goalID 
	    << " is completed. Setting current lane to "
	    << nextGoal.exitSegmentID << "." << nextGoal.exitLaneID << "\n";
	  print(s.str(), 2);
	  m_graphEstimator.setCurrentLane(nextGoal.exitSegmentID, nextGoal.exitLaneID);
	}
	if (completedGoal.segment_type == SegGoals::DRIVE_AROUND || 
	    completedGoal.segment_type == SegGoals::BACK_UP) {
	  s.str("");
	  s << "RoutePlanner::control: goal " << m_segGoalsStatus.goalID 
	    << " (DRIVE_AROUND or BACK_UP) is completed. Setting current lane to 0.0\n";
	  print(s.str(), 2);
	  m_graphEstimator.setCurrentLane(0,0);
	}
      }
    }
  }

  unsigned long long currentTime;
  DGCgettime(currentTime);

  if ( m_prevMergedDirectiveControlGoalIDs.size() == 0) {
    replan = true;
    s.str("");
    s << "RoutePlanner::control:  m_prevMergedDirectiveControlGoalIDs.size() == 0...I'll replan\n";
    print(s.str(), 2);
  }
  
  // Determine whether PAUSE_TIMEOUT is reached.
  if (m_lastCompletedPauseID != m_lastPauseID && currentTime - m_lastPauseT > PAUSE_TIMEOUT &&
      m_tplannerStarted) {
    replan = true;
    sendPause = true;
    s.str("");
    s << "RoutePlanner::control: Timeout for PAUSE (last completed pause = "
      << m_lastCompletedPauseID << ", last pause directive = "
      << m_lastPauseID << ", time from last pause = " << currentTime - m_lastPauseT
      << ") I'll replan\n";
    print(s.str(), 1);
  }

  // See if we get stuck
  if (m_graphEstimator.getEstopStatus() != EstopRun || m_graphEstimator.getSpeed(true) > STOP_SPEED) {
    m_lastVehMovedT = currentTime;
  }

  m_timeStopped = (currentTime - m_lastVehMovedT)/1000000.0;
  if (CmdArgs::keepForwardProgress && m_timeStopped > CmdArgs::stopTimeout) {
    replan = true;
    sendPause = true;
    s.str("");
    s << "RoutePlanner::control: We have been stuck for " << m_timeStopped
      << " seconds. I'll replan\n";
    print(s.str(), 1);
    m_lastVehMovedT = currentTime;
  }
  
  // If everything is ok, then send directive from contrDirectiveQ to the planner
  if (m_tplannerStarted && !replan && m_lastCompletedPauseID == m_lastPauseID &&
      m_lastCompletedBackupID == m_lastBackupID) {
    DGClockMutex(&m_contrDirectiveQMutex);
    while( m_contrDirectiveQ.size() > 0 && m_segGoalsStatus.goalID + LEAST_NUM_SEGGOALS_TPLANNER_STORED >= 
	   (m_contrDirectiveQ.front()).goalID ) {
      sendSegGoals();
    }
    DGCunlockMutex(&m_contrDirectiveQMutex);
  }
  
  DGClockMutex(&m_contrDirectiveQMutex);
  int numSegGoalsStored = m_contrDirectiveQ.size();
  DGCunlockMutex(&m_contrDirectiveQMutex);

  if (sendPause) {
    if (currentTime - m_lastPauseT > TIME_BETWEEN_PAUSE) {
      resetGoals();
      SegGoals pause;
      pause.segment_type = SegGoals::RESET;
      if (mergedDir->front().type == MControlDirective::EMERGENCY_STOP)
	pause.segment_type = SegGoals::EMERGENCY_STOP;
      pause.goalID = m_nextGoalID++;
      pause.stopAtExit = true;
      
      DGCgettime(m_lastPauseT);
      m_lastPauseID = pause.goalID;

      DGClockMutex(&m_contrDirectiveQMutex);
      
      // Add directives to control queue     
      m_contrDirectiveQ.push_back(pause);
      
      if ( mergedDir->front().type == MControlDirective::RESET ||
	   mergedDir->front().type == MControlDirective::EMERGENCY_STOP) {
	// Update goalID that corresponds to completion of current merged directive.
	m_prevMergedDirectiveControlGoalIDs.push_back(pause.goalID);
      }
      
      // Print out the mission
      s.str("");
      s << "RoutePlanner: " << pause.toString() << "\n";
      print(s.str(), 0);
      
      // Add directives to contrGcPort
      if (m_tplannerStarted)      
	sendSegGoals();

      DGCunlockMutex(&m_contrDirectiveQMutex);
    }
    else {
      s.str("");
      s << "RoutePlanner::control: Want to send PAUSE but it was just sent\n";
      print(s.str(), 3);
    }
    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirectives.front().goalID;
    contrStatus->status = RoutePlannerControlStatus::PENDING;
      
    return;
  }


  // Wait for completion of pause
  if (m_lastCompletedPauseID != m_lastPauseID) {
    s.str("");
    s << "RoutePlanner::control: Waiting for completion of PAUSE ID = " << m_lastPauseID
      << " (last completed PAUSE is " << m_lastCompletedPauseID << ")\n";
    print(s.str(), 3);
    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirectives.front().goalID;
    contrStatus->status = RoutePlannerControlStatus::PENDING;
    return;
  }
    

  // Wait for completion of backup
  if (m_lastCompletedBackupID != m_lastBackupID) {
    s.str("");
    s << "RoutePlanner::control: Waiting for completion of BACKUP ID = " << m_lastBackupID
      << " (last completed BACKUP is " << m_lastCompletedBackupID << ")\n";
    print(s.str(), 3);
    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirectives.front().goalID;
    contrStatus->status = RoutePlannerControlStatus::PENDING;
    return;
  }

  // Compute new goals starting from current position or from last waypoint
  // (given by the traffic planner) if failure occurs.    
  if (replan) {
    if ( m_prevMergedDirectives.front().type == MControlDirective::NEXT_CHECKPOINT) {
      s.str("");
      s << "RoutePlanner::control: I'm about to replan\n";
      print(s.str(), 3);

      if (m_logData) {
	fprintf ( m_logFile, "\nGraph:\n" );
	m_travGraph->log(m_logFile);
	fprintf ( m_logFile, "Finished RNDFGRAPH\n" );
	fflush(m_logFile);
      }

      SegGoals failedGoal  = rtInterfaceSF->getSentDirective(m_segGoalsStatus.goalID);
      if (m_segGoalsStatus.status == SegGoalsStatus::FAILED && m_segGoalsStatus.currentSegmentID != 0) {
	s.str("");
	s << "RoutePlanner::control: planner told me that the current lane is " 
	  << m_segGoalsStatus.currentSegmentID << "." << m_segGoalsStatus.currentLaneID << "\n";
	print(s.str(), 2);
	if (failedGoal.entrySegmentID == 0) {
	  s.str("");
	  s << "RoutePlanner::control: goal " << m_segGoalsStatus.goalID 
	    << " failed. failedGoal.entrySegmentID == 0. Setting current lane to "
	    << m_segGoalsStatus.currentSegmentID << "." 
	    << m_segGoalsStatus.currentLaneID << "\n";
	  print(s.str(), 2);
	  m_graphEstimator.setCurrentLane(m_segGoalsStatus.currentSegmentID, m_segGoalsStatus.currentLaneID);
	}
	else if (m_segGoalsStatus.currentSegmentID == failedGoal.entrySegmentID ||
		 m_segGoalsStatus.currentSegmentID == failedGoal.exitSegmentID) {
	  s.str("");
	  s << "RoutePlanner::control: goal " << m_segGoalsStatus.goalID 
	    << " failed. Planner told me the right segment. Setting current lane to "
	    << failedGoal.exitSegmentID << "." << failedGoal.exitLaneID << "\n";
	  print(s.str(), 2);
	  m_graphEstimator.setCurrentLane(failedGoal.exitSegmentID, failedGoal.exitLaneID);
	}
	else {
	  s.str("");
	  s << "RoutePlanner::control: goal " << m_segGoalsStatus.goalID 
	    << " failed. Planner gave me the wrong segment. Setting current lane to "
	    << m_segGoalsStatus.currentSegmentID << "." << m_segGoalsStatus.currentLaneID << "\n";
	  print(s.str(), 2);
	  m_graphEstimator.setCurrentLane(m_segGoalsStatus.currentSegmentID, m_segGoalsStatus.currentLaneID);
	}
      }

      // First, get an updated graph
      updateEdgeCostParam(m_prevMergedDirectives.front().vehicleCap);
      getNewGraph();

      Vertex* currentPositionVertex = m_graphEstimator.getCurrentPositionVertex();
      s.str("");
      if (currentPositionVertex != NULL) {
	s << "Current position is " << currentPositionVertex->getSegmentID() << "."
	  << currentPositionVertex->getLaneID() << "."
	  << currentPositionVertex->getWaypointID() << "\n";
	print(s.str(), 2);
      }
      else {
	s << "Can't find current position\n";
	printError("control", s.str());
      }
	    
      // Do not remove edges that lead to the checkpoint
      if (goalFailed  && m_numFails >= NUM_FAILS_BEFORE_REM_EDGE &&
	  m_segGoalsStatus.reason == SegGoalsStatus::BLOCKED_ROAD &&
	  (failedGoal.exitSegmentID != m_prevMergedDirectives.front().segmentID ||
	   failedGoal.exitLaneID != m_prevMergedDirectives.front().laneID ||
	   failedGoal.exitWaypointID != m_prevMergedDirectives.front().waypointID ||
	   m_numFails >= NUM_CKPT_FAILS_BEFORE_REM_EDGE || currentPositionVertex == NULL ||
	   currentPositionVertex->getSegmentID() != m_prevMergedDirectives.front().segmentID ||
	   currentPositionVertex->getLaneID() != m_prevMergedDirectives.front().laneID ||
	   currentPositionVertex->getWaypointID() < m_prevMergedDirectives.front().waypointID - 1) ) {
	s.str("");
	s << "RoutePlanner::control: m_numFails = " << m_numFails << " -- removing edges\n";
	print(s.str(), -1);
	if ( (failedGoal.segment_type == SegGoals::ROAD_SEGMENT && failedGoal.entrySegmentID != 0)
	     || (failedGoal.segment_type == SegGoals::PARKING_ZONE && failedGoal.entrySegmentID == 0 )) {
	  m_graphEstimator.removeFailedGoalEdges(failedGoal.entrySegmentID, failedGoal.entryLaneID, 
						 failedGoal.entryWaypointID, failedGoal.exitSegmentID, 
						 failedGoal.exitLaneID, failedGoal.exitWaypointID, true);
	  startFromCurrent = true;
	}
	else if  (failedGoal.segment_type != SegGoals::UTURN &&
		  failedGoal.segment_type != SegGoals::ROAD_SEGMENT &&
		  failedGoal.segment_type != SegGoals::RESET &&
		  failedGoal.segment_type != SegGoals::DRIVE_AROUND && 
		  failedGoal.segment_type != SegGoals::BACK_UP &&
		  failedGoal.segment_type != SegGoals::END_OF_MISSION &&
		  failedGoal.segment_type != SegGoals::EMERGENCY_STOP &&
		  failedGoal.entrySegmentID != 0) {
	  m_graphEstimator.removeFailedGoalEdges(failedGoal.entrySegmentID, failedGoal.entryLaneID, 
						 failedGoal.entryWaypointID, failedGoal.exitSegmentID, 
						 failedGoal.exitLaneID, failedGoal.exitWaypointID, false);
	}
      }

      if (m_logData) {
	fprintf (m_logFile, "Graph: \n" );
	m_travGraph->log(m_logFile);
	fprintf (m_logFile, "\n");
	fflush(m_logFile);
      }

      resetGoals();

      int missionID = m_prevMergedDirectives.front().nextCheckpointIndex + 1;
    
      // Plan from current position
      vector<SegGoals> segGoalsSeq;
      if (!goalFailed || startFromCurrent) {
	// If route cannot be found, wait (and hope that the map will get clear.)
	segGoalsSeq = planSegGoals(0, 0, 0, m_prevMergedDirectives.front().segmentID,
				   m_prevMergedDirectives.front().laneID, 
				   m_prevMergedDirectives.front().waypointID,
				   missionID, m_prevMergedDirectives.front().maxSpeed);

	if (segGoalsSeq.size() == 0 && m_numFails < NUM_FAILS_BEFORE_SKIPPING_CHECKPOINT &&
	    m_numFails < NUM_FAILS_BEFORE_BACKUP)  {
	  sleep(1);
	  s.str("");
	  s << "RoutePlanner::control: clear removed edges (m_numFails = " << m_numFails << ")\n";
	  print(s.str(), 1);
	  m_graphEstimator.clearRemovedEdges();
	  getNewGraph();
	  segGoalsSeq = planSegGoals(0, 0, 0, m_prevMergedDirectives.front().segmentID,
				     m_prevMergedDirectives.front().laneID, 
				     m_prevMergedDirectives.front().waypointID,
				     missionID, m_prevMergedDirectives.front().maxSpeed);
	}
	else if (!CmdArgs::nobackup && segGoalsSeq.size() == 0 && 
		 m_numFails < NUM_FAILS_BEFORE_SKIPPING_CHECKPOINT) {
	  sendBackup();
	  contrStatus->goalID = m_prevMergedDirectives.front().goalID;
	  contrStatus->status = RoutePlannerControlStatus::PENDING;
	  return;
	}
      }
      else {
	if ( m_numFails < NUM_CKPT_FAILS_BEFORE_REM_EDGE &&
	     failedGoal.exitSegmentID == m_prevMergedDirectives.front().segmentID &&
	     failedGoal.exitLaneID == m_prevMergedDirectives.front().laneID &&
	     failedGoal.exitWaypointID == m_prevMergedDirectives.front().waypointID &&
	     (failedGoal.segment_type == SegGoals::ROAD_SEGMENT || failedGoal.entrySegmentID == 0) ) {
	  s.str("");
	  s << "RoutePlanner::control: Checkpoint " << m_prevMergedDirectives.front().segmentID
	    << "." << m_prevMergedDirectives.front().laneID << "." 
	    << m_prevMergedDirectives.front().waypointID << " is blocked\n";
	  print(s.str(), 1);
	    
	  if (currentPositionVertex != NULL && 
	      currentPositionVertex->getSegmentID() == m_prevMergedDirectives.front().segmentID &&
	      currentPositionVertex->getLaneID() == m_prevMergedDirectives.front().laneID &&
	      currentPositionVertex->getWaypointID() >= m_prevMergedDirectives.front().waypointID - 1) {
	    int replanWaypointID = m_prevMergedDirectives.front().waypointID;
	    m_graphEstimator.findExit(m_prevMergedDirectives.front().segmentID, 
				      m_prevMergedDirectives.front().laneID,
				      replanWaypointID);
	    if (replanWaypointID > m_prevMergedDirectives.front().waypointID + 2)
	      replanWaypointID = m_prevMergedDirectives.front().waypointID + 2;
	    SegGoals currentSegGoals;
	    currentSegGoals.goalID = m_nextGoalID;
	    currentSegGoals.entrySegmentID = failedGoal.entrySegmentID;
	    currentSegGoals.entryLaneID = failedGoal.entryLaneID;
	    currentSegGoals.entryWaypointID = failedGoal.entryWaypointID;
	    currentSegGoals.exitSegmentID = m_prevMergedDirectives.front().segmentID;
	    currentSegGoals.exitLaneID = m_prevMergedDirectives.front().laneID;
	    currentSegGoals.exitWaypointID = replanWaypointID;
	    if (failedGoal.entrySegmentID == 0)
	      currentSegGoals.segment_type = SegGoals::PARKING_ZONE;
	    else
	      currentSegGoals.segment_type = SegGoals::ROAD_SEGMENT;
	    currentSegGoals.minSpeedLimit = m_minSpeedLimits[failedGoal.exitSegmentID];
	    currentSegGoals.maxSpeedLimit = m_maxSpeedLimits[failedGoal.exitSegmentID];
	    currentSegGoals.illegalPassingAllowed = true;
	    currentSegGoals.stopAtExit = false;
	    Vertex* replanVertex = m_travGraph->getVertex(m_prevMergedDirectives.front().segmentID, 
							  m_prevMergedDirectives.front().laneID,
							  replanWaypointID);
	    if (replanVertex != NULL)
	      currentSegGoals.stopAtExit = replanVertex->isStopSign();
	    currentSegGoals.isExitCheckpoint = false;
	    segGoalsSeq.push_back(currentSegGoals);
	  }
	  else {
	    segGoalsSeq = planSegGoals(failedGoal.entrySegmentID, failedGoal.entryLaneID, 
				       failedGoal.entryWaypointID, 
				       m_prevMergedDirectives.front().segmentID,
				       m_prevMergedDirectives.front().laneID, 
				       m_prevMergedDirectives.front().waypointID, 
				       missionID, m_prevMergedDirectives.front().maxSpeed);
	    if (segGoalsSeq.size() == 0 && m_numFails < NUM_FAILS_BEFORE_SKIPPING_CHECKPOINT &&
		m_numFails < NUM_FAILS_BEFORE_BACKUP) {
	      sleep(1);
	      s.str("");
	      s << "RoutePlanner::control: clear removed edges (m_numFails = " << m_numFails << ")\n";
	      print(s.str(), 1);
	      m_graphEstimator.clearRemovedEdges();
	      getNewGraph();
	      segGoalsSeq = planSegGoals(failedGoal.entrySegmentID, failedGoal.entryLaneID, 
					 failedGoal.entryWaypointID, 
					 m_prevMergedDirectives.front().segmentID,
					 m_prevMergedDirectives.front().laneID, 
					 m_prevMergedDirectives.front().waypointID, 
					 missionID, m_prevMergedDirectives.front().maxSpeed);
	    }
	    else if (!CmdArgs::nobackup && segGoalsSeq.size() == 0 && 
		     m_numFails < NUM_FAILS_BEFORE_SKIPPING_CHECKPOINT) {
	      sendBackup();
	      contrStatus->goalID = m_prevMergedDirectives.front().goalID;
	      contrStatus->status = RoutePlannerControlStatus::PENDING;
	      return;
	    }
	  }
	}
	else {
	  segGoalsSeq = planSegGoals(failedGoal.entrySegmentID, failedGoal.entryLaneID, 
				     failedGoal.entryWaypointID, 
				     m_prevMergedDirectives.front().segmentID,
				     m_prevMergedDirectives.front().laneID, 
				     m_prevMergedDirectives.front().waypointID, 
				     missionID, m_prevMergedDirectives.front().maxSpeed);
	  if (segGoalsSeq.size() == 0 && m_numFails < NUM_FAILS_BEFORE_SKIPPING_CHECKPOINT &&
	      m_numFails < NUM_FAILS_BEFORE_BACKUP) {
	    sleep(1);
	    s.str("");
	    s << "RoutePlanner::control: clear removed edges (m_numFails = " << m_numFails << ")\n";
	    print(s.str(), 1);
	    m_graphEstimator.clearRemovedEdges();
	    getNewGraph();
	    segGoalsSeq = planSegGoals(failedGoal.entrySegmentID, failedGoal.entryLaneID, 
				       failedGoal.entryWaypointID, 
				       m_prevMergedDirectives.front().segmentID,
				       m_prevMergedDirectives.front().laneID, 
				       m_prevMergedDirectives.front().waypointID, 
				       missionID, m_prevMergedDirectives.front().maxSpeed);
	  }
	  else if (!CmdArgs::nobackup && segGoalsSeq.size() == 0 && 
		   m_numFails < NUM_FAILS_BEFORE_SKIPPING_CHECKPOINT) {
	    sendBackup();
	    contrStatus->goalID = m_prevMergedDirectives.front().goalID;
	    contrStatus->status = RoutePlannerControlStatus::PENDING;
	    return;
	  }
	}

	if (segGoalsSeq.size() > 0 ) {
	  if (failedGoal.entrySegmentID == 0) {
	    segGoalsSeq.front().segment_type = SegGoals::PARKING_ZONE;
	  }

	  if (!segGoalsSeq.front().illegalPassingAllowed) {
	    s.str("");
	    s << "Goal " << failedGoal.goalID << "(" << m_segGoalsStatus.goalID << ")" << " failed but there is not obstacle in this lane.\n";
	    printError("control", s.str());
	    segGoalsSeq.front().illegalPassingAllowed = true;
	  }
	}
      }

      // If route cannot be found, report failure.
      if (segGoalsSeq.size() == 0) {
	// Update controlStatus
	contrStatus->goalID = m_prevMergedDirectives.front().goalID;
	contrStatus->status = RoutePlannerControlStatus::FAILED;
	contrStatus->reason = MControlDirectiveResponse::NO_ROUTE;
	return;
      }

      // Set the current lane
      s.str("");
      s << "RoutePlanner::control: replan. Setting current lane to "
	<< segGoalsSeq.front().exitSegmentID << "." 
	<< segGoalsSeq.front().exitLaneID << "\n";
      print(s.str(), 2);
      m_graphEstimator.setCurrentLane(segGoalsSeq.front().exitSegmentID, segGoalsSeq.front().exitLaneID);

      m_nextGoalStartSegmentID = segGoalsSeq.back().exitSegmentID;
      m_nextGoalStartLaneID = segGoalsSeq.back().exitLaneID;
      m_nextGoalStartWaypointID = segGoalsSeq.back().exitWaypointID;

      // Add stop at exit if the next merged directive is END_OF_MISSION
      if (m_prevMergedDirectives.size() > 1 && 
	  m_prevMergedDirectives.at(1).type == MControlDirective::END_OF_MISSION) {
	segGoalsSeq.back().stopAtExit = true;
      }

      DGClockMutex(&m_contrDirectiveQMutex);
      int startPrintingIndex = (int)m_contrDirectiveQ.size();
      
      // Add directives to control queue
      for(unsigned i = 0; i < segGoalsSeq.size(); i++) {
	m_contrDirectiveQ.push_back(segGoalsSeq[i]);
      }  
      
      // Update goalID that corresponds to completion of previous merged directive.
      if (m_contrDirectiveQ.size() > 0) {
	m_prevMergedDirectiveControlGoalIDs.push_back(m_contrDirectiveQ.back().goalID);
	m_nextGoalID = (m_contrDirectiveQ.back()).goalID + 1;
      }
      
      // Print out the mission
      if (DEBUG_LEVEL > 0) {
	if (m_nosparrow) {
	  printMission(m_contrDirectiveQ, startPrintingIndex);
	  cout<<endl;  
	}
	else {
	  printMissionOnSparrow(m_contrDirectiveQ, startPrintingIndex);
	}
      }
      if (m_logData) {
	logMission(m_contrDirectiveQ, startPrintingIndex);
      }

      // Add directives to contrGcPort
      if (m_tplannerStarted) {
	int numDirectivesAdded = 0;
	while( m_contrDirectiveQ.size() > 0 && numDirectivesAdded < LEAST_NUM_SEGGOALS_TPLANNER_STORED ) {
	  sendSegGoals();
	  numDirectivesAdded++;
	}
      }
      
      // Update controlStatus
      contrStatus->goalID = m_prevMergedDirectives.front().goalID;
      contrStatus->status = RoutePlannerControlStatus::PENDING;

      DGCunlockMutex(&m_contrDirectiveQMutex);
      return;
    }
    else if (m_prevMergedDirectives.front().type == MControlDirective::END_OF_MISSION ) {
      resetGoals();
      addEndOfMission();
      // Add directives to contrGcPort
      if (m_tplannerStarted) {
	int numDirectivesAdded = 0;
	while( m_contrDirectiveQ.size() > 0 && numDirectivesAdded < LEAST_NUM_SEGGOALS_TPLANNER_STORED ) {
	  sendSegGoals();
	  numDirectivesAdded++;
	}
      }
      
      // Update controlStatus
      contrStatus->goalID = m_prevMergedDirectives.front().goalID;
      contrStatus->status = RoutePlannerControlStatus::PENDING;

      DGCunlockMutex(&m_contrDirectiveQMutex);
      return;
    }
    else if (mergedDir->front().type == MControlDirective::RESET ||
	     mergedDir->front().type == MControlDirective::EMERGENCY_STOP) {
      resetGoals();
      m_graphEstimator.clearRemovedEdges();
      SegGoals pause;
      if (mergedDir->front().type == MControlDirective::RESET)
	pause.segment_type = SegGoals::RESET;
      else
	pause.segment_type = SegGoals::EMERGENCY_STOP;
	
      pause.goalID = m_nextGoalID++;
      pause.stopAtExit = true;

      DGCgettime(m_lastPauseT);
      m_lastPauseID = pause.goalID;

      DGClockMutex(&m_contrDirectiveQMutex);
      
      // Add directives to control queue     
      m_contrDirectiveQ.push_back(pause);
      
      // Update goalID that corresponds to completion of current merged directive.
      m_prevMergedDirectiveControlGoalIDs.push_back(pause.goalID);
      
      // Print out the mission
      s.str("");
      s << "RoutePlanner: " << pause.toString() << "\n";
      print(s.str(), 0);
      
      // Add directives to contrGcPort
      if (m_tplannerStarted)      
	sendSegGoals();
      
      // Update controlStatus
      contrStatus->goalID = m_prevMergedDirectives.front().goalID;
      contrStatus->status = RoutePlannerControlStatus::PENDING;
      
      DGCunlockMutex(&m_contrDirectiveQMutex);
      return;
    }
    // If the first merged directive is not NEXT_CHECKPOINT, END_OF_MISSION, RESET or EMERGENCY_STOP
    else {
      s.str("");
      s << "merged directive = " 
	<< m_prevMergedDirectives.front().type << ". Can't replan" << "\n";
      printError("control", s.str());
    }
  }
  else if (numSegGoalsStored < LEAST_NUM_SEGGOALS_STORED  && 
	   m_prevMergedDirectives.size() > m_prevMergedDirectiveControlGoalIDs.size() ) {
    s.str("");
    s << "RoutePlanner::control: I'm planning ahead\n";
    print(s.str(), 3);

    if (m_prevMergedDirectiveControlGoalIDs.size() == 0) {
      s.str("");
      s << "Planning ahead while m_prevMergedDirectiveControlGoalIDs.size() == 0\n";
      printError("control", s.str());
      return;
    }

    MControlDirective prevDirective = m_prevMergedDirectives.at(m_prevMergedDirectiveControlGoalIDs.size() - 1);
    MControlDirective currentDirective = m_prevMergedDirectives.at(m_prevMergedDirectiveControlGoalIDs.size());
    updateEdgeCostParam(currentDirective.vehicleCap);
    getNewGraph();

    if (m_logData) {
      fprintf ( m_logFile, "\nGraph:\n" );
      m_travGraph->log(m_logFile);
      fprintf ( m_logFile, "Finished RNDFGRAPH\n" );
      fflush(m_logFile);
    }


    if (currentDirective.type == MControlDirective::NEXT_CHECKPOINT) {
      int missionID = currentDirective.nextCheckpointIndex + 1;
      /*
      vector<SegGoals> segGoalsSeq = planSegGoals(prevDirective.segmentID,
						  prevDirective.laneID,
						  prevDirective.waypointID,
						  currentDirective.segmentID,
						  currentDirective.laneID,
						  currentDirective.waypointID,
						  missionID);
      */
      vector<SegGoals> segGoalsSeq = planSegGoals(m_nextGoalStartSegmentID,
						  m_nextGoalStartLaneID,
						  m_nextGoalStartWaypointID,
						  currentDirective.segmentID,
						  currentDirective.laneID,
						  currentDirective.waypointID,
						  missionID,
						  currentDirective.maxSpeed);
      if (segGoalsSeq.size() == 0) {
	sleep(1);
	m_graphEstimator.clearRemovedEdges();
	getNewGraph();
	segGoalsSeq = planSegGoals(m_nextGoalStartSegmentID,
				   m_nextGoalStartLaneID,
				   m_nextGoalStartWaypointID,
				   currentDirective.segmentID,
				   currentDirective.laneID,
				   currentDirective.waypointID,
				   missionID,
				   currentDirective.maxSpeed);
      }

      // If route cannot be found, report failure.
      if (segGoalsSeq.size() == 0) {
	// Update controlStatus
	contrStatus->goalID = m_prevMergedDirectives.front().goalID;
	contrStatus->status = RoutePlannerControlStatus::FAILED;
	contrStatus->reason = MControlDirectiveResponse::NO_ROUTE;
	return;
      }

      m_nextGoalStartSegmentID = segGoalsSeq.back().exitSegmentID;
      m_nextGoalStartLaneID = segGoalsSeq.back().exitLaneID;
      m_nextGoalStartWaypointID = segGoalsSeq.back().exitWaypointID;

      // Add stop at exit if the next merged directive is END_OF_MISSION
      if (m_prevMergedDirectives.size() > m_prevMergedDirectiveControlGoalIDs.size() + 1 && 
	  m_prevMergedDirectives.at(m_prevMergedDirectiveControlGoalIDs.size() + 1).type == 
	  MControlDirective::END_OF_MISSION) {
	segGoalsSeq.back().stopAtExit = true;
      }
      
      DGClockMutex(&m_contrDirectiveQMutex);
      int startPrintingIndex = (int)m_contrDirectiveQ.size(); 
      
      // Add directives to control queue     
      for(unsigned i = 0; i < segGoalsSeq.size(); i++) {
	m_contrDirectiveQ.push_back(segGoalsSeq[i]);
      }   
   
      
      // Update goalID that corresponds to completion of current merged directive.
      if (m_contrDirectiveQ.size() > 0) {
	m_prevMergedDirectiveControlGoalIDs.push_back(m_contrDirectiveQ.back().goalID);
	m_nextGoalID = (m_contrDirectiveQ.back()).goalID + 1;
      }
      
      // Print out the mission
      if (DEBUG_LEVEL > 0) {
	if (m_nosparrow) {
	  printMission(m_contrDirectiveQ, startPrintingIndex);
	  cout << endl;  
	}
	else {
	  printMissionOnSparrow(m_contrDirectiveQ, startPrintingIndex);
	}
      }
      if (m_logData) {
	logMission(m_contrDirectiveQ, startPrintingIndex);
      }
      
      // Update controlStatus
      contrStatus->goalID = m_prevMergedDirectives.front().goalID;
      contrStatus->status = RoutePlannerControlStatus::PENDING;
      
      DGCunlockMutex(&m_contrDirectiveQMutex);
      return;
    }
    else if (currentDirective.type == MControlDirective::END_OF_MISSION ) {
      addEndOfMission();

      // Update controlStatus
      contrStatus->goalID = m_prevMergedDirectives.front().goalID;
      contrStatus->status = RoutePlannerControlStatus::PENDING;

      return;
    }
    // If this merged directive is not NEXT_CHECKPOINT or END_OF_MISSION
    else {
      s.str("");
      s << "merged directive = " 
	<< m_prevMergedDirectives.front().type << ". Can't plan ahead" << "\n";
      printError("control", s.str());
    }    
  }
  else
  {
    s.str("");
    s << "RoutePlanner::control: I'm being lazy and not doing anything" 
      << " (numSegGoalsStored = " << numSegGoalsStored << ", m_prevMergedDirectives.size() = "
      << m_prevMergedDirectives.size() << ", m_prevMergedDirectiveControlGoalIDs.size() = "
      << m_prevMergedDirectiveControlGoalIDs.size() << "\n";
    print(s.str(), 3);
    // Update controlStatus
    contrStatus->goalID = m_prevMergedDirectives.front().goalID;
    contrStatus->status = RoutePlannerControlStatus::PENDING;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sendBackup()
{
  resetGoals();
  SegGoals backup;
  backup.segment_type = SegGoals::BACK_UP;
  backup.distance = 5;
  backup.minSpeedLimit = 0;
  backup.maxSpeedLimit = 5;
  backup.goalID = m_nextGoalID++;
  backup.stopAtExit = true;
      
  m_lastBackupID = backup.goalID;

  DGClockMutex(&m_contrDirectiveQMutex);
      
  // Add directives to control queue     
  m_contrDirectiveQ.push_back(backup);
      
  // Print out the mission
  stringstream s("");
  s << "RoutePlanner: " << backup.toString() << "\n";
  print(s.str(), 0);
      
  // Add directives to contrGcPort
  if (m_tplannerStarted)      
    sendSegGoals();

  DGCunlockMutex(&m_contrDirectiveQMutex);

  m_lastFailedGoalID = m_nextGoalID;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::sendSegGoals()
{
  if( m_contrDirectiveQ.size() == 0) {
    stringstream s("");
    s << "m_contrDirectiveQ.size() == 0" << endl;
    printError("sendSegGoals", s.str());
    return;
  }
  stringstream s("");
  s << "RoutePlanner::sendSegGoals: Adding goal " << (m_contrDirectiveQ.front()).goalID << " to contrGcPortMsgQ.\n";
  print(s.str(), 0);
  rtInterfaceSF->sendDirective(&m_contrDirectiveQ.front());
  m_contrDirectiveQ.pop_front();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::getNewGraph()
{
  m_graphEstimator.updateGraph(true);
  m_travGraph = &m_graphEstimator.getGraph();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::addEndOfMission()
{
  stringstream s("");
  s << "RoutePlanner::control: I got END_OF_MISSION directive. Yay!!!\n";
  print(s.str(), 3);

  SegGoals endOfMission;
  endOfMission.goalID = m_nextGoalID;
  endOfMission.entrySegmentID = 0;
  endOfMission.entryLaneID = 0;
  endOfMission.entryWaypointID = 0;
  endOfMission.exitSegmentID = 0;
  endOfMission.exitLaneID = 0;
  endOfMission.exitWaypointID = 0;
  endOfMission.minSpeedLimit = 0;
  endOfMission.maxSpeedLimit = 0;
  endOfMission.segment_type = SegGoals::END_OF_MISSION;
  endOfMission.illegalPassingAllowed = false;
  endOfMission.stopAtExit = true;
  endOfMission.isExitCheckpoint = false;
      
  DGClockMutex(&m_contrDirectiveQMutex);

  // Set the stopAtExit of the last goal
  if (m_contrDirectiveQ.size() > 0) {
    (m_contrDirectiveQ.back()).stopAtExit = true;
  }

  int startPrintingIndex = (int)m_contrDirectiveQ.size(); 
  if (m_contrDirectiveQ.size() > 0)
    startPrintingIndex--; 
      
  // Add directives to control queue     
  m_contrDirectiveQ.push_back(endOfMission);
      
  // Update goalID that corresponds to completion of current merged directive.
  if (m_contrDirectiveQ.size() > 0) {
    m_prevMergedDirectiveControlGoalIDs.push_back(m_contrDirectiveQ.back().goalID);
    m_nextGoalID = (m_contrDirectiveQ.back()).goalID + 1;
  }
      
  // Print out the mission
  if (DEBUG_LEVEL > 0) {
    if (m_nosparrow) {
      printMission(m_contrDirectiveQ, startPrintingIndex);
      cout << endl;  
    }
    else {
      printMissionOnSparrow(m_contrDirectiveQ, startPrintingIndex);
    }
  }
  if (m_logData) {
    logMission(m_contrDirectiveQ, startPrintingIndex);
  }

  DGCunlockMutex(&m_contrDirectiveQMutex);
}  


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::updateEdgeCostParam(VehicleCapability vehicleCap)
{
  if (!CmdArgs::getSystemHealth)
    return;

  stringstream s("");
  s << "\n" << "RoutePlanner::updateEdgeCostParam: Vehicle Capability:\n " 
    << vehicleCap.toString() << "\n";
  print(s.str(), 3);
    
  double costIntersectionLeftTurn = COST_INTERSECTION;
  double costIntersectionRightTurn = COST_INTERSECTION;
  double costIntersectionStraight = COST_INTERSECTION;
  double costUturn = COST_UTURN;
  double costKturn = COST_KTURN;
  double costZone = COST_ZONE;

  if (vehicleCap.intersectionLeftTurn <= 0 || 
      vehicleCap.intersectionLeftTurn > 1)
    costIntersectionLeftTurn = 0;
  else if (vehicleCap.intersectionLeftTurn < 1)
    costIntersectionLeftTurn += 10000*(1 - vehicleCap.intersectionLeftTurn);

  if (vehicleCap.intersectionRightTurn <= 0 || 
      vehicleCap.intersectionRightTurn > 1)
    costIntersectionRightTurn = 0;
  else if (vehicleCap.intersectionRightTurn < 1)
    costIntersectionRightTurn += 10000*(1 - vehicleCap.intersectionRightTurn);
 
  if (vehicleCap.intersectionStraightTurn <= 0 || 
      vehicleCap.intersectionStraightTurn > 1)
    costIntersectionStraight = 0;
  else if (vehicleCap.intersectionStraightTurn < 1)
    costIntersectionStraight += 10000*(1 - vehicleCap.intersectionStraightTurn);

  if (vehicleCap.uturn <= 1 || 
      vehicleCap.uturn > 1) {
    costUturn = 0;
    costKturn = 0;
  }
  else if (vehicleCap.uturn < 1) {
    costUturn += 10000*(1 - vehicleCap.uturn);
    costKturn += 10000*(1 - vehicleCap.uturn);
  }

  if (vehicleCap.nominalZoneRegionDriving <= 0 || 
      vehicleCap.nominalZoneRegionDriving > 1)
    costZone = 0;
  else if (vehicleCap.nominalZoneRegionDriving < 1)
    costZone += 10000*(1 - vehicleCap.nominalZoneRegionDriving);

  vector<double> edgeCost;
  edgeCost.push_back(costIntersectionLeftTurn);
  edgeCost.push_back(costIntersectionRightTurn);
  edgeCost.push_back(costIntersectionStraight);
  edgeCost.push_back(COST_CHANGE_LANE);
  edgeCost.push_back(costUturn);
  edgeCost.push_back(costKturn);
  edgeCost.push_back(costZone);
  edgeCost.push_back(COST_FIRST_SEGMENT);
  edgeCost.push_back(COST_STOP_SIGN);
  edgeCost.push_back(COST_NOT_VISITED);
  edgeCost.push_back(COST_OBSTRUCTED);

  m_graphEstimator.updateEdgeCostParam(edgeCost);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> RoutePlanner::planSegGoals(int segmentID1, int laneID1,  
    int waypointID1, int segmentID2, int laneID2, int waypointID2, 
    int missionNumber, double maxSpeed)
{
  double cost; 
  vector<SegGoals> segGoalsSeq; 
  vector<Vertex*> route; 
  stringstream s("");

  Vertex* vertex1 = m_travGraph->getVertex(segmentID1, laneID1, waypointID1); 
  if (vertex1 == NULL) {
    m_graphEstimator.addVertex(segmentID1, laneID1, waypointID1); 
    vertex1 = m_travGraph->getVertex(segmentID1, laneID1, waypointID1);
    if (vertex1 == NULL) {
      s.str("");
      s << "Cannot find waypoint " << segmentID1
	<< "." << laneID1 << "." << waypointID1 << " in graph\n";
      printError("planSegGoals", s.str());
      return segGoalsSeq;
    }
  }

  Vertex* vertex2 = m_travGraph->getVertex(segmentID2, laneID2, waypointID2); 
  if (vertex2 == NULL) {
    m_graphEstimator.addVertex(segmentID2, laneID2, waypointID2); 
    vertex2 = m_travGraph->getVertex(segmentID2, laneID2, waypointID2);
    if (vertex2 == NULL) {
      s.str("");
      s << "Cannot find waypoint " << segmentID2
	<< "." << laneID2 << "." << waypointID2 << " in graph\n";
      printError("planSegGoals", s.str());
      return segGoalsSeq;
    }
  }

  bool routeFound = findRoute(vertex1, vertex2, m_travGraph, route, cost);
  if (!routeFound) {
    // Add uturn to segGoalsSeq
    segGoalsSeq = addUturn(vertex1, vertex2, maxSpeed);


    if (segGoalsSeq.size() == 0) {
      if (m_numFails < NUM_FAILS_BEFORE_SKIPPING_CHECKPOINT &&
	  m_numFails < NUM_FAILS_BEFORE_BACKUP && 
	  m_graphEstimator.addRemovedEdge()) {
	stringstream s("");
	s << "Cannot find route from " << segmentID1 << "." << laneID1 << "." << waypointID1 
	  << " to " << segmentID2 << "." << laneID2 << "." << waypointID2 << " after allowing u-turn\n";
	printError("planSegGoals", s.str());
	if (m_logData) {
	  m_travGraph->log(m_logFile);
	  fprintf(m_logFile, "\n");
	  fflush(m_logFile);
	}
        segGoalsSeq = planSegGoals(segmentID1, laneID1, waypointID1, 
				   segmentID2, laneID2, waypointID2, 
				   missionNumber, maxSpeed);
      }
      else {
	s.str("");
        s << "Cannot find route after adding all the removed edges back to the graph\n" ;
	printError("planSegGoals", s.str());
	if (m_nosparrow) {
	  m_travGraph->print();
	}
	if (m_logData) {
	  m_travGraph->log(m_logFile);
	}
	return segGoalsSeq;
      }
    }
  }
  else 
    segGoalsSeq = findSegGoals(route, m_travGraph, m_minSpeedLimits, m_maxSpeedLimits, m_nextGoalID, maxSpeed); 

  if (segGoalsSeq.size() > 0) { 
    // The exit point of the last segment goal is a checkpoint 
    segGoalsSeq[segGoalsSeq.size()-1].isExitCheckpoint = true; 
  } 

  if (route.size() > 1 && DEBUG_LEVEL > 0) {
    s.str("");
    s << "\nRoutePlanner: Mission " << missionNumber
      << ": from waypoint "<<  vertex1->getSegmentID() << "."
      << vertex1->getLaneID() << "." << vertex1->getWaypointID()
      << " to waypoint " << vertex2->getSegmentID() << "."
      << vertex2->getLaneID() << "."
      << vertex2->getWaypointID() << "\n";
    print(s.str(), -1);
  }

  return segGoalsSeq;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<SegGoals> RoutePlanner::addUturn(Vertex* vertex1, Vertex* vertex2, double maxSpeed)
{
  stringstream s("");
  s << "RoutePlanner: No route from " << vertex1->getSegmentID() << "."
    << vertex1->getLaneID() << "." << vertex1->getWaypointID()
    << " to " << vertex2->getSegmentID() << "." << vertex2->getLaneID()
    << "." << vertex2->getWaypointID() << " without making u-turn\n";
  print(s.str(), -1);

  if (vertex1->getSegmentID() > m_travGraph->getNumOfSegments()) {
    s.str("");
    s << "Cannot make uturn in a zone\n";
    s << "Here is the graph" << endl;
    printError("addUturn", s.str());
    m_travGraph->print();
    if (m_logData) {
      m_travGraph->log(m_logFile);
    }
    exit(1);
  }

  vector<Vertex*> route;
  vector<SegGoals> segGoalsSeq;
  double cost;
  bool routeFound = false;

  m_graphEstimator.addUturnEdges(vertex1);
  if (m_logData) {
    fprintf( m_logFile, "After adding uturn edges: \n");
    m_travGraph->log(m_logFile);
    fprintf( m_logFile, "\n");
    fflush(m_logFile);
  }
  
  routeFound = findRoute(vertex1, vertex2, m_travGraph, route, cost);
  if (!routeFound)
    return segGoalsSeq;
  
  segGoalsSeq = findSegGoals(route, m_travGraph, m_minSpeedLimits, m_maxSpeedLimits, m_nextGoalID, maxSpeed);

  return segGoalsSeq;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::resetGoals()
{
  DGClockMutex(&m_contrDirectiveQMutex);

  if (m_contrDirectiveQ.size() > 0) {
    m_contrDirectiveQ.clear();
  }
  DGCunlockMutex(&m_contrDirectiveQMutex);
  m_prevMergedDirectiveControlGoalIDs.clear();
  stringstream s("");
  s << "RoutePlanner: Clear contrGcPort\n";
  print(s.str(), 0);

  rtInterfaceSF->flushAll();
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool RoutePlanner::loadMDFFile(char* fileName, RNDF* rndf)
{
  ifstream file;
  string line;
  string word;
  
  file.open(fileName, ios::in);

  if(!file) {
    stringstream s("");
    s << "Error: " << fileName << " file not found.\n";
    printError("loadMDFFile", s.str());
    return false;
  } 

  while (word != "MDF_name") {
    getline(file, line);    
    istringstream lineStream(line, ios::in);  
    lineStream >> word;
  }

  if(word == "MDF_name") {
    if (DEBUG_LEVEL > 3)
      cout << "RoutePlanner: Parsing speed limits" << endl;
    parseSpeedLimit(&file, rndf);
    file.close();
    if (DEBUG_LEVEL > 3)
      cout << "RoutePlanner: Finish loading MDF file" << endl;
    return true;
  }
  else {
    file.close();
    return false;
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::parseSpeedLimit(ifstream* file, RNDF* rndf)
{
  int segmentID;
  double minSpeed, maxSpeed;
  string line, word;
  char letter;

  while(word != "speed_limits") {    
    getline(*file, line);
    istringstream lineStream(line, ios::in);
    lineStream >> word;
  }

  
  while(word != "end_speed_limits") {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    if(letter >= '0' && letter <= '9') {
      lineStream >> segmentID;
      lineStream >> minSpeed;
      lineStream >> maxSpeed;

      minSpeed = minSpeed * MPS_PER_MPH;
      maxSpeed = maxSpeed * MPS_PER_MPH;
      
      // setSpeedLimits(segmentID, minSpeed, maxSpeed, rndf);
      if ((unsigned)segmentID <= m_minSpeedLimits.size()) {
	m_minSpeedLimits[segmentID] = minSpeed;
	m_maxSpeedLimits[segmentID] = maxSpeed;
      }
      else {
	stringstream s("");
	s << "got speed limit for segment " << segmentID
	  << " while number of segments and zones = " << m_minSpeedLimits.size() << "\n";
	printError("parseSpeedLimit", s.str());
	int numOfSegments = m_minSpeedLimits.size();
	m_minSpeedLimits.resize(segmentID);
	m_maxSpeedLimits.resize(segmentID);
	for (unsigned i = numOfSegments; i <= m_minSpeedLimits.size(); i++)
	{
	  m_minSpeedLimits[i] = 0;
	  m_maxSpeedLimits[i] = 0;
	}
	m_minSpeedLimits[segmentID] = minSpeed;
	m_maxSpeedLimits[segmentID] = maxSpeed;
      }
    }
    else {
      lineStream >> word;
      continue;      
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
uint64_t RoutePlanner::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::print(string mess, int d)
{
  stringstream s("");
  s << getTime() << "\t" << mess;
  if (DEBUG_LEVEL > d) {
    if (m_nosparrow) {
      cout << mess;
    }
    else {
      SparrowHawk().log(mess);
    }
  }
  if (m_logData) {
    fprintf(m_logFile, s.str().c_str());
    fflush(m_logFile);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::printError(string funcName, string errorMessage, bool error)
{
  stringstream s1("");
  if (error)
    s1 << "\nERROR: RoutePlanner::" << funcName << " : ";
  s1 << errorMessage << "\n";
  cerr << s1.str();

  stringstream s2("");
  s2 << getTime() << "\t" << s1.str();
  if (m_logData) {
    fprintf(m_logFile, s2.str().c_str());
    fflush(m_logFile);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::printMissionOnSparrow(deque<SegGoals> segGoals, int startIndex)
{
  int i = 0;
  while (segGoals.size() > 0 && i < startIndex) {
    segGoals.pop_front();
    i++;
  }

  while (segGoals.size() > 0) {
    SegGoals currentGoal = segGoals.front();
    SparrowHawk().log(currentGoal.toString().c_str());
    segGoals.pop_front();
  }	
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::logMission(deque<SegGoals> segGoals, int startIndex)
{
  int i = 0;
  while (segGoals.size() > 0 && i < startIndex) {
    segGoals.pop_front();
    i++;
  }

  while (segGoals.size() > 0) {
    SegGoals currentGoal = segGoals.front();
    fprintf( m_logFile, "  ");
    fprintf( m_logFile, currentGoal.toString().c_str());
    fprintf( m_logFile, "\n");
    fflush(m_logFile);
    segGoals.pop_front();
  }	
  fprintf (m_logFile, "\n");
  fflush(m_logFile);
}


/*
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// GCinterface functions 
//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::addDirectiveToGcPort(SegGoals directive)
{
  RPlannerContrMsgWrapper msgWrapper;
  msgWrapper.directive = directive;
  msgWrapper.response.status = RPlannerContrMsgResponse::QUEUED;
  DGClockMutex(&m_contrGcPortMutex);
  m_contrGcPortMsgQ.push_back(msgWrapper);
  DGCunlockMutex(&m_contrGcPortMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool RoutePlanner::isCompleted(int goalID)
{
  if (goalID <= 0)
  {
    return false;
  }
  DGClockMutex(&m_contrGcPortMutex);
  for (unsigned i=0; i<m_contrGcPortMsgQ.size(); i++)
  {
    if(m_contrGcPortMsgQ[i].directive.goalID == goalID)
    {
      if (m_contrGcPortMsgQ[i].response.status == RPlannerContrMsgResponse::COMPLETED)
      {
	DGCunlockMutex(&m_contrGcPortMutex);
	return true;
      }
      else
      {
	DGCunlockMutex(&m_contrGcPortMutex);
	return false;
      }
    }
  }

  DGCunlockMutex(&m_contrGcPortMutex);
  return false;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
RPlannerContrMsgResponse RoutePlanner::getResponse(int goalID)
{
  RPlannerContrMsgResponse response;
  response.status = RPlannerContrMsgResponse::QUEUED;

  DGClockMutex(&m_contrGcPortMutex);
  for (unsigned i=0; i<m_contrGcPortMsgQ.size(); i++)
  {
    if(m_contrGcPortMsgQ[i].directive.goalID == goalID)
    {
      response = m_contrGcPortMsgQ[i].response;
      DGCunlockMutex(&m_contrGcPortMutex);
      return response;
    }
  }
  DGCunlockMutex(&m_contrGcPortMutex);

  
  cerr << "ERROR: getResponse: Cannot find goalID " << goalID << " in contrGcPort" << endl;
  return response;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool RoutePlanner::gotNewStatus()
{
  DGClockMutex(&m_newStatusMutex);
  bool newStatusReceived = m_newStatus;
  DGCunlockMutex(&m_newStatusMutex);
  return newStatusReceived;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
SegGoalsStatus RoutePlanner::getLatestStatusChange()
{
  SegGoalsStatus status;
  status.goalID = 0;

  DGClockMutex(&m_SegGoalsStatusMutex);
  status = m_segGoalsStatus;
  DGCunlockMutex(&m_SegGoalsStatusMutex);

  DGClockMutex(&m_newStatusMutex);
  m_newStatus = false;
  DGCunlockMutex(&m_newStatusMutex);
  return status;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void RoutePlanner::messageThread()
{
  // The skynet socket for receiving tplanner status

  SkynetTalker<SegGoalsStatus> statusTalker(m_snKey, SNsegGoals, MODmissionplanner);
  SkynetTalker<SegGoals> segGoalsTalker(m_snKey,  SNtplannerStatus, MODmissionplanner);
  
  int numSegGoalsRequests = 0;
  int numRespondedRequests = 0;

  if (m_nosparrow)
  {
    cout << "messageThread: Waiting for traffic planner" << endl;
  }
  else
  {
    SparrowHawk().log("messageThread: Waiting for traffic planner\n");
  }

  while(true)
  {

    // Receive status
    if (statusTalker.hasNewMessage())
    {
      SegGoalsStatus receivedSegGoalsStatus;
    
      bool tPlannerStatusReceived =
          statusTalker.receive(&receivedSegGoalsStatus);
        
      if (tPlannerStatusReceived)
      {
	if (m_nosparrow)
	{
	  cout << "Goal " << receivedSegGoalsStatus.goalID << " status: "
	       << receivedSegGoalsStatus.status << endl;
	}
	else
	{
	  SparrowHawk().log("Goal %d status %d\n", receivedSegGoalsStatus.goalID,
			    (int) receivedSegGoalsStatus.status);
	}
	
	if (receivedSegGoalsStatus.goalID == 0)
	{
	  numSegGoalsRequests++;
	}
	else
	{
	  unsigned i = 0;
	  bool directiveFound = false;
	  DGClockMutex(&m_contrGcPortMutex);
	  while( i < m_contrGcPortMsgQ.size() && !directiveFound)
	  {
	    if(m_contrGcPortMsgQ[i].directive.goalID == receivedSegGoalsStatus.goalID)
	    {
	      directiveFound = true;
	      if (receivedSegGoalsStatus.status == SegGoalsStatus::ACCEPT)
	      {
		m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::ACCEPTED;
	      }
	      else if (receivedSegGoalsStatus.status == SegGoalsStatus::REJECT ||
		       receivedSegGoalsStatus.status == SegGoalsStatus::FAILED)
	      {
		m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::FAILED;
		m_contrGcPortMsgQ[i].response.reason = receivedSegGoalsStatus.reason;
	      }
	      else if (receivedSegGoalsStatus.status == SegGoalsStatus::COMPLETED)
	      {
		m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::COMPLETED;
	      }
	      else
	      {
		cerr << "ERROR: getTPlannerStatusThread: Received unknown status "
		     << receivedSegGoalsStatus.status << endl;
	      }
	      m_firstQueuedMsgIndex = i+1;
	    }
	    i++;
	  }
	  DGCunlockMutex(&m_contrGcPortMutex);
	  
	  if (!directiveFound)
	  {
	    cerr << "ERROR: getTPlannerStatusThread: Cannot find goalID " 
		 << receivedSegGoalsStatus.goalID << " in contrGcPort" << endl;
	  }
	}
      
	DGClockMutex(&m_SegGoalsStatusMutex);
	m_segGoalsStatus = receivedSegGoalsStatus;
	DGCunlockMutex(&m_SegGoalsStatusMutex);

	DGClockMutex(&m_newStatusMutex);
	m_newStatus = true;
	DGCunlockMutex(&m_newStatusMutex);
      }
    }

    // Start sending goals when tplanner starts listening
    if (numSegGoalsRequests > 0)
    {
      DGClockMutex(&m_SegGoalsStatusMutex);
      int lastGoalID = m_segGoalsStatus.goalID;
      DGCunlockMutex(&m_SegGoalsStatusMutex);
      
      DGClockMutex(&m_contrGcPortMutex);
      // If the tplanner just restarts, reset the status for goals previously sent to QUEUED.
      if (lastGoalID == 0 && numRespondedRequests < numSegGoalsRequests)
      {
	for (unsigned i=0; i < m_contrGcPortMsgQ.size(); i++)
	{
	  if (m_contrGcPortMsgQ[i].response.status == RPlannerContrMsgResponse::SENT)
	  {
	    m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::QUEUED;
	  }
	}
	numRespondedRequests++;
      }
      DGCunlockMutex(&m_contrGcPortMutex);

      // Find the first messages with status QUEUED.
      unsigned firstQueuedMsgIndex = 0;
      bool firstQueuedMsgFound = false;
      bool m_firstQueuedMsgFound = false;
      DGClockMutex(&m_contrGcPortMutex);
      while (firstQueuedMsgIndex < m_contrGcPortMsgQ.size() && !firstQueuedMsgFound)
      {
	if (m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::QUEUED ||
	    m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::SENT ||
	    m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::ACCEPTED)
	{
	  if (!m_firstQueuedMsgFound)
	  {
	    m_firstQueuedMsgFound = true;
	    m_firstQueuedMsgIndex = firstQueuedMsgIndex;
	  }
	  if (m_contrGcPortMsgQ[firstQueuedMsgIndex].response.status == RPlannerContrMsgResponse::QUEUED)
	  {
	    firstQueuedMsgFound = true;
	  }
	  else
	  {
	    firstQueuedMsgIndex++;
	  }
	}
	else
	{
	  firstQueuedMsgIndex++;
	}
      }

      DGCunlockMutex(&m_contrGcPortMutex);

      // Send all the messages with status QUEUED
      DGClockMutex(&m_contrGcPortMutex);
      if (firstQueuedMsgFound)
      {
	for (unsigned i = firstQueuedMsgIndex; i < m_contrGcPortMsgQ.size(); i++)
	{
	  if (m_nosparrow)
	  {
	    cout << "Sending goal " << (m_contrGcPortMsgQ[i]).directive.goalID << " to tplanner." << endl;
	  }
	  else
	  {
	    SparrowHawk().log("Sending goal %d to tplanner\n", (m_contrGcPortMsgQ[i]).directive.goalID);
	  }
	  segGoalsTalker.send(&(m_contrGcPortMsgQ[i].directive));
	  m_contrGcPortMsgQ[i].response.status = RPlannerContrMsgResponse::SENT;
	}
      }
      DGCunlockMutex(&m_contrGcPortMutex);	
    }
    usleep(100000);
  }
}
*/
