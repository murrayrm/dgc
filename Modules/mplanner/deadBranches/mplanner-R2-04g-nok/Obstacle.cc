/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include <string>
#include <iostream>
#include <dgcutils/DGCutils.hh>
#include "Obstacle.hh"
using namespace std;

Obstacle::Obstacle(int id, int m, vector<int> n, vector<int> p, int d)
{
  obstacleID = id;
  segmentID = m;
  laneIDs = n;
  waypointIDs = p;
  decayT = d;
  DGCgettime(timeCreated);
}

Obstacle::Obstacle(int id, int m, int n, int p, int d)
{
  obstacleID = id;
  segmentID = m;
  laneIDs.push_back(n);
  decayT = d;
  DGCgettime(timeCreated);
  waypointIDs.push_back(p);
}

Obstacle::~Obstacle()
{
  // Nothing needs to be done here
}

int Obstacle::getObstacleID()
{
  return obstacleID;
}

int Obstacle::getSegmentID()
{
  return segmentID;
}

vector<int> Obstacle::getLaneIDs()
{
  return laneIDs;
}

vector<int> Obstacle::getWaypointIDs()
{
  return waypointIDs;
}

int Obstacle::getNumOfLanesBlocked()
{
  return (int)laneIDs.size();
}

double Obstacle::getObstacleWidth()
{
  return width;
}

void Obstacle::setObstacleWidth(int width)
{
  this->width = width;
}

bool Obstacle::addObstructedLane(int laneID, int waypointID)
{
  bool obsFound = false;
  unsigned i = 0;
  while (!obsFound && i < laneIDs.size())
  {
    if (laneIDs[i] == laneID)
      obsFound = true;
    i++;
  }
  if (!obsFound)
  {
    laneIDs.push_back(laneID);
    waypointIDs.push_back(waypointID);
  }
  return !obsFound;
}
