/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */


#ifndef ROUTEPLANNER_HH
#define ROUTEPLANNER_HH

#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <string>
#include <vector>
#include <deque>
#include <math.h>
//#include "alice/estop.h"
#include <dgcutils/DGCutils.hh>
#include <sparrowhawk/SparrowHawk.hh>
#include <rndf/RNDF.hh>
#include <travgraph/Graph.hh>
#include <gcinterfaces/SegGoals.hh>
#include <gcinterfaces/SegGoalsStatus.hh>
#include <dgcutils/GlobalConstants.h>
#include <gcmodule/GcModule.hh>
#include <gcmodule/GcInterface.hh>
#include "MissionUtils.hh"
#include "MissionControl.hh"
#include "TraversibilityGraphEstimation.hh"
#include "MPlannerCommand.hh"
#include "Obstacle.hh"

#define LEAST_NUM_SEGGOALS_STORED 8
#define LEAST_NUM_SEGGOALS_TPLANNER_STORED 3
#define NUM_SEGGOALS_DISPLAYED 6

typedef GcInterface<SegGoals, SegGoalsStatus, SNsegGoals, SNtplannerStatus, MODmissionplanner> RTInterface ;
typedef MControlDirective RPlannerMergedDirective;

typedef MControlDirectiveResponse RPlannerControlStatus;


/*! The structure for status of merged directive. This status is "sent"
 *  from control back to arbiter. The structure for this is the same
 *  as MContrDirectiveStatus. */
class RoutePlannerControlStatus : public ControlStatus
{
public:
  enum Status { FAILED, COMPLETED, PENDING };
  unsigned goalID;
  Status status;
  MControlDirectiveResponse::ReasonForFailure reason;
  
  RoutePlannerControlStatus()
  {
    goalID = 0;
    status = PENDING;
  }

  std::string toString() const {
    stringstream s("");
    s << "GoalId: " << goalID << "Status: "  << status << endl;
    return s.str();
  }

  // MControlDirectiveResponse rpcs;
};

/*! The structure for merged directive that is "sent" from arbiter to control. 
 * Since the arbiter just passes through the directive MergedDirective has
 * the same structure as MControlDirective. */
class RoutePlannerMergedDirective : public MergedDirective
{
public:
  deque<MControlDirective> directives;

  std::string toString() const {
    std::string s;
    for (unsigned i=0; i<directives.size(); i++) {
      s += "  " + directives[i].toString() + "\n";
    }
    return s;
  }
};

/*! The structure for response from tplanner */
struct RPlannerContrMsgResponse
{
  enum Status{ QUEUED, SENT, ACCEPTED, COMPLETED, FAILED };
  RPlannerContrMsgResponse()
  {
    // initialize the segment goals to zeros to appease the memory profilers
      memset(this, 0, sizeof(*this));
      this->status = QUEUED;
  }
  Status status;
  SegGoalsStatus::ReasonForFailure reason;
};

/*! The structure for pairing up the directives sent to the
 *  traffic planner (but in the language of the control) and the response
 *  to this directive. */
struct RPlannerContrMsgWrapper
{
  RPlannerContrMsgWrapper()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
    response.status = RPlannerContrMsgResponse::QUEUED;
  }
  SegGoals directive;  // goalID is in here
  RPlannerContrMsgResponse response;
};


/**
 * RoutePlanner class.
 * This is a main class for the mission planner where information from 
 * gloNavMap (mapping) and MDF file is used to compute segment goals
 * and send to the traffic planner.
 * \brief Main class for mission planner function
 */
class RoutePlanner : public GcModule
{
  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param m_RNDFFileName is the name of the RNDF file */
  char* m_RNDFFileName;

  /*!\param m_MDFFileName is the name of the MDF file */
  char* m_MDFFileName;
  
  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;

  /*!\param m_nopause is true when PAUSE will be done manually. (i.e.
   * route planner will not send PAUSE down to traffic planner)*/
  bool m_nopause;

  /*!\param m_logData is true when data is to be logged */
  bool m_logData;

  /*!\param m_logFile is the name of the log file */
  FILE* m_logFile;

  int VERBOSITY_LEVEL;
  int DEBUG_LEVEL;
  
  /*!\param m_rndf is a pointer to the RNDF object. */
  RNDF* m_rndf;

  /*!\param m_graphEstimator is the TraversibilityGraphEstimation object. */
  TraversibilityGraphEstimation m_graphEstimator;

  /*!\param m_travGraph is the corresponding graph of the current RNDF
   * that mission planner uses to compute segment goals. */
  Graph* m_travGraph;

  /*!\param m_minSpeedLimits is the vector of min speed limits in m/s*/
  vector<double> m_minSpeedLimits;

  /*!\param m_maxSpeedLimits is the vector of max speed limits in m/s*/
  vector<double> m_maxSpeedLimits;

  /*!\param m_currentCkptSegmentID, m_currentCkptLaneID, m_currentCkptWaypointID is the
   * waypoint ID of the current checkpoint. m_currentCkptGoalID is 
   * the segment goal ID for crossing the current checkpoint*/
  int m_currentCkptSegmentID, m_currentCkptLaneID, m_currentCkptWaypointID;
  int m_currentCkptGoalID;

  /*!\param m_missionID tells us where we are in MDF */
  int m_missionID;

  /*!\param m_nextGoalID is the ID of the next goal to be added 
   * to m_segGoalsSeq. */
  int m_nextGoalID; 

  /*!\param m_eomIssued is whether END_OF_MISSION is completed */
  bool m_eomCompleted;

  /*!\param m_lastFailedGoalID is the id of the last goal that fails */
  unsigned m_lastFailedGoalID;

  /*!\param m_numFails is the number of times that the goal
   * m_lastFailedGoalID fails */
  unsigned m_numFails;

  /*!\param NUM_FAILS_BEFORE_REM_EDGE is the number of times that
   * tplanner fails a goal before mplanner removes the
   * corresponding edge */
  unsigned NUM_FAILS_BEFORE_REM_EDGE;

  /*!\param m_segGoalsStatus is the status of the traffic planner which is
   * used in the planning loop to determine if we need to replan. */
  SegGoalsStatus m_segGoalsStatus;

  /*!\param SparrowHawk display */
  CSparrowHawk *m_shp;

  /*!\param Sparrow variables */
  int sparrowNextGoalIDs[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints2[NUM_SEGGOALS_DISPLAYED];
  double sparrowMinSpeed, sparrowMaxSpeed;
  bool sparrowIllegalPassingAllowed, sparrowStopAtExit, sparrowIsExitCheckpoint;

  /*!\param Obstacle inserted by sparrow */
  int obstacleID;
  int obstacleSegmentID;
  int obstacleLaneID;
  int obstacleWaypointID;

  /*!\param control status sent from control to arbiter */
  RoutePlannerControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  RoutePlannerMergedDirective m_mergedDirective;

  /*!\param goalIDs of control directive that corresponds to the completion of
   * directives in m_prevMergedDirectives */
  deque<unsigned> m_prevMergedDirectiveControlGoalIDs;

  /*!\param previous merged directives */
  deque<MControlDirective> m_prevMergedDirectives;

  /*!\param directives currently stored in control waiting to be added to contrGcPort */
  deque<SegGoals> m_contrDirectiveQ;

  /*!\param the mutex to protect m_contrDirectiveQ */
  pthread_mutex_t m_contrDirectiveQMutex;

  /*!\param GcInterface variable for talking to tplanner*/
  RTInterface::Southface* rtInterfaceSF;

  /*!\param GcInterface variable for talking to mission control*/
  MRInterface::Northface* mrInterfaceNF;

public:
  /*! Constructor */
  RoutePlanner(int skynetKey, bool bWaitForStateFill, bool nosparrow, 
	       RNDF* rndf, char* RNDFFileName, char* MDFFileName, 
	       bool listenToMapper, bool nopause, bool noillegalPassing,
	       bool logData, FILE* logFile, int debugLevel, 
	       bool verbose, CSparrowHawk* shp, int logLevel = 0);
      
  /*! Standard destructor */
  virtual ~RoutePlanner();

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface. */
  void UpdateSparrowVariablesLoop();

  /*! Add the manually inserted obstacle to the map */
  void insertObstacle(int, int, int, int);

  /*! This is the function that continually runs the planner in a loop
  void MPlanningLoop(void); */

  /*! This is the function that continually reads tplanner status, transmits
   *  segment goals to tplanner and update status of the messages in the m_contrGcPortMsgQ.
   *  We don't need this anymore because GcInterface already handles this
   void messageThread(); */

private:
  /*! Arbitration for the route planner module. ControlStatus is the input and
   *  MergedDirective is the output of this function. */
  void arbitrate(ControlStatus*, MergedDirective*);

  /*! Control for the route planner module. ControlStatus is the output and
   *  MergedDirective is the input of this function. */
  void control(ControlStatus*, MergedDirective*);

  /*! Add the manually inserted obstacle to the map */
  void sparrowInsertObstacle();

  /*! Remove all the manually inserted obstacle */
  void sparrowRemoveObstacles();

  /*! Send segment goals */
  void sendSegGoals();

  /*! Request graph */
  void getNewGraph();

  /*! Plan segment goals from (segment1,lane1,waypoint1) to (segment2, lane2, waypoint2)
   *  if (segment1 = 0), plan from current position */
  vector<SegGoals> planSegGoals(int, int, int, int, int, int, int);

  /*! Compute segment goals with uturn allowed */
  vector<SegGoals> addUturn(Vertex*, Vertex*);

  /*! Flush m_contrGcPortMsgQ and m_contrDirectiveQ */
  void resetGoals();

  /*! Parse speed limit from MDF */
  bool loadMDFFile(char*, RNDF*);
  void parseSpeedLimit(ifstream*, RNDF*);

  /*! Get time in microsecond */
  uint64_t getTime();

  /*! A function to log data and print out a message on the screen
   *  if DEBUG_LEVEL is greater than d*/
  void print(string s, int d);
  void printError(string funcName, string errorMessage, bool error = true);

  /*! A function to print mission on sparrow display */
  void printMissionOnSparrow(deque<SegGoals> segGoals, int startIndex);

  /*! A function to log mission to the log file */
  void logMission(deque<SegGoals> segGoals, int startIndex);

};

#endif  // ROUTEPLANNER_HH
