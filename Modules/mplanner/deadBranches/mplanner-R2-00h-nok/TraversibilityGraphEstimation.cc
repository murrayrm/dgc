/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */

#include "TraversibilityGraphEstimation.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "alice/AliceConstants.h"
#include <boost/serialization/vector.hpp>
#include <math.h>

#define PI 3.14159265
using namespace std;

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// Basic constructor: no threads
TraversibilityGraphEstimation::TraversibilityGraphEstimation
(int skynetKey, bool waitForStateFill) :
  CSkynetContainer(MODmissionplanner, skynetKey), CStateClient(waitForStateFill),
  m_snKey(skynetKey)
{  
  DGCcreateMutex(&m_ObstaclesMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
TraversibilityGraphEstimation::~TraversibilityGraphEstimation()
{
  DGCdeleteMutex(&m_receivedGraphMutex);
  DGCdeleteMutex(&m_condNewGraphMutex);
  DGCdeleteMutex(&m_ObstaclesMutex);
  DGCdeleteCondition(&m_condNewGraph);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::init
    (RNDF* rndf, bool listenToMapper, bool nosparrow, bool logData, FILE* logFile, int debugLevel, bool verbose)
{
  m_listenToMapper = listenToMapper;
  m_nosparrow = nosparrow; 
  m_logData = logData;
  m_logFile = logFile;
  m_rndf = rndf;
  m_graph = new Graph(rndf);
  m_receivedGraph = NULL;
  DEBUG_LEVEL = debugLevel; 
  if (verbose)
  {
    VERBOSITY_LEVEL = 1;
  }
  DGCcreateMutex(&m_receivedGraphMutex);
  DGCcreateMutex(&m_condNewGraphMutex);
  DGCcreateCondition(&m_condNewGraph);
  m_condNewGraph.bCond = false;
  m_condNewGraph.pMutex = m_condNewGraphMutex;
  if (listenToMapper)
  {
    DGCstartMemberFunctionThread(this, &TraversibilityGraphEstimation::getGraphThread);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::newGraph()
{
  DGClockMutex(&m_condNewGraph.pMutex);
  bool condNewGraph = m_condNewGraph.bCond;
  if (DEBUG_LEVEL > 0)
  {
    if (m_nosparrow)
    {
      cout << "See if we get a new graph ... " << condNewGraph << endl;
    }
    else
    {
      SparrowHawk().log( "See if we get a new graph %d\n", condNewGraph);
    }
  }
  if (m_logData)
  {
    fprintf(m_logFile, "See if we get a new graph %d\n", condNewGraph);
  }
  DGCunlockMutex(&m_condNewGraph.pMutex);
  return condNewGraph;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateGraph(bool requestMapper)
{
  #warning updateGraph deletes the graph that is accessible from other classes through the getGraph() function

  int graphRequestSocket = m_skynet.get_send_sock(SNglobalGloNavMapRequest);
  if (requestMapper && m_listenToMapper)
  {  
    bool requestGraph = true;
    if (m_nosparrow)
    {
      cout << "Waiting for mapper" << endl;
    }
    else
    {
      SparrowHawk().log( "Waiting for mapper\n");
    }
    if (m_logData)
    {
      fprintf (m_logFile, "Waiting for mapper\n");
    }

    while (!newGraph())
    {
      if (m_nosparrow)
      {
	cout << "Sending request to mapper" << endl;
      }
      else
      {
	SparrowHawk().log( "Sending request to mapper\n");
      }
      if (m_logData)
      {
	fprintf( m_logFile, "Sending request to mapper\n");
      }
      m_skynet.send_msg(graphRequestSocket, &requestGraph, sizeof(bool) , 0);
      sleep(5);
    }
  }

  DGClockMutex(&m_receivedGraphMutex);
  // Swap pointers between m_graph and m_receivedGraph
  if (m_receivedGraph != NULL)
  {
    delete m_graph;
    m_graph = m_receivedGraph;
    m_receivedGraph = NULL;
  }
  else if (!m_listenToMapper)
  {
    // m_graph->init(m_rndf);
  }
  DGCunlockMutex(&m_receivedGraphMutex);

  if (!m_listenToMapper || m_graph->getVertex(0,0,0) == NULL)
  {
    addCurrentPositionToGraph();
  }

  // Update the weight of edges based on inserted obstacles
  DGClockMutex(&m_ObstaclesMutex);
  updateObstructedEdges();
  DGCunlockMutex(&m_ObstaclesMutex);

  DGClockMutex(&m_condNewGraph.pMutex);
  m_condNewGraph.bCond = false;
  DGCunlockMutex(&m_condNewGraph.pMutex);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::waitForGraph()
{
  if (m_listenToMapper)
  {
    if (m_nosparrow)
    {
      cout << "Waiting for graph..." << endl;
    }
    else
    {
      SparrowHawk().log( "Waiting for graph...\n");
    }
    if (m_logData)
    {
      fprintf(m_logFile, "Waiting for graph...\n");
    }

    DGCWaitForConditionTrue(m_condNewGraph);
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
Graph& TraversibilityGraphEstimation::getGraph()
{
  if (m_nosparrow)
  {
    if (DEBUG_LEVEL > 4)
    {
      cout << "getGraph()" << endl;
      DGClockMutex(&m_receivedGraphMutex);
      if (m_receivedGraph != NULL)
      {
	cout << "m_receivedGraph: " << endl;
	m_receivedGraph->print();
      }
      cout << "m_graph: " << endl;
      m_graph->print();
      DGCunlockMutex(&m_receivedGraphMutex);
      cout << "finished m_graph" << endl;
    }
  }
  if (m_logData)
  {
    fprintf (m_logFile, "TraversibilityGraphEstimation::getGraph(): \n");
    m_graph->log(m_logFile);
  }
  return *m_graph;
}

const Graph& TraversibilityGraphEstimation::getGraph() const
{
  return *m_graph;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addVertex(int segmentID, int laneID, int waypointID)
{
  GPSPoint* point = NULL;
  if (segmentID == 0)
  {
    return addCurrentPositionToGraph();
  }
  else if (laneID != 0)
  {
    point = m_rndf->getWaypoint(segmentID, laneID, waypointID);
  }
  else
  {
    point = m_rndf->getPerimeterPoint(segmentID, waypointID);
  }
  if (point == NULL)
    return false;

  return m_graph->addGPSPointToGraph (point, m_rndf);
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addUturnEdges(Vertex* vertex1)
{
  if (vertex1->getSegmentID() == 0)
  {
    double northingFront = m_state.utmNorthing + DIST_REAR_AXLE_TO_FRONT*cos(m_state.utmYaw);
    double eastingFront = m_state.utmEasting + DIST_REAR_AXLE_TO_FRONT*sin(m_state.utmYaw);
    double yaw = m_state.utmYaw;
    if (yaw >= PI)
    {
      yaw = yaw - 2*PI;
    }
    double headingDiff, headingLaneDiff;
    
    return m_graph->addCurrentPosition(northingFront, eastingFront, yaw, 
					 m_rndf, 15*PI/180, 45*PI/180, 500, headingDiff,
					 headingLaneDiff, true);
  }
  else
  {
    if (vertex1->getSegmentID() > m_rndf->getNumOfSegments())
      return false;
    
    GPSPoint* point = m_rndf->getWaypoint(vertex1->getSegmentID(), vertex1->getLaneID(),
					  vertex1->getWaypointID());  
    if (point == NULL)
      return false;
    
    return m_graph->addUturnEdges(vertex1, point, m_rndf);
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addRemovedEdge()
{
  if (m_edgesRemoved.size() > 0)
  {
    Edge* edgeToAdd = m_edgesRemoved.front();
    if (m_nosparrow)
    {
      cout << "Adding the removed edge from "
	   << edgeToAdd->getPrevious()->getSegmentID()
	   << "." << edgeToAdd->getPrevious()->getLaneID() << "."
	   << edgeToAdd->getPrevious()->getWaypointID() << " to "
	   << edgeToAdd->getNext()->getSegmentID() << "."
	   << edgeToAdd->getNext()->getLaneID() << "."
	   << edgeToAdd->getNext()->getWaypointID() << " to the graph" << endl;
    }
    else
    {
      SparrowHawk().log("Adding the removed edge from %d.%d.%d to %d.%d.%d to the graph",
			edgeToAdd->getPrevious()->getSegmentID(),
			edgeToAdd->getPrevious()->getLaneID(),
			edgeToAdd->getPrevious()->getWaypointID(),
			edgeToAdd->getNext()->getSegmentID(),
			edgeToAdd->getNext()->getLaneID(), edgeToAdd->getNext()->getWaypointID());
    }
    if (m_logData)
    {
      fprintf (m_logFile, "Adding the removed edge from %d.%d.%d to %d.%d.%d to the graph",
			edgeToAdd->getPrevious()->getSegmentID(),
			edgeToAdd->getPrevious()->getLaneID(),
			edgeToAdd->getPrevious()->getWaypointID(),
			edgeToAdd->getNext()->getSegmentID(),
			edgeToAdd->getNext()->getLaneID(), edgeToAdd->getNext()->getWaypointID());
    }
    edgeToAdd->setObstructedLevel(MAX_OBSTRUCTED_LEVEL);
    m_graph->addEdge(edgeToAdd);
    m_edgesRemoved.pop_front();
    return true;
  }
  else
    return false;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
vector<Obstacle>& TraversibilityGraphEstimation::getAllObstaclesOnSegment(int segmentID)
{
  m_obstaclesOnSegment.clear();
  DGClockMutex(&m_ObstaclesMutex);
  for (unsigned i = 0; i < m_obstacles.size(); i++)
  {
    if (m_obstacles[i].getSegmentID() == segmentID)
    {
      m_obstaclesOnSegment.push_back(m_obstacles[i]);
    }
  }
  DGCunlockMutex(&m_ObstaclesMutex);
  
  return m_obstaclesOnSegment;
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::insertObstacle(int obstacleID, int obstacleSegmentID,
						   int obstacleLaneID, int obstacleWaypointID)
{
  // Check that the segment ID, lane ID and waypoint ID are valid
  int numOfSegments = m_rndf->getNumOfSegments();
  int numOfZones = m_rndf->getNumOfZones();
  int numOfSegmentsAndZones = numOfSegments + numOfZones;

  if (obstacleSegmentID < 1 || obstacleSegmentID > numOfSegmentsAndZones)
  {
    SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
        obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    SparrowHawk().log("Segment ID must be between 1 and %d\n", numOfSegmentsAndZones);
    if (m_logData)
    {
      fprintf(m_logFile,"Cannot insert obstacle at segment %d, lane %d, waypoint %d",
			obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      fprintf(m_logFile,"Segment ID must be between 1 and %d\n", numOfSegmentsAndZones);
    }
    return false;
  }

  if (obstacleSegmentID <= numOfSegments)
  {
    int numOfLanes = m_rndf->getSegment(obstacleSegmentID)->getNumOfLanes();
    if (obstacleLaneID < 1 || obstacleLaneID > numOfLanes)
    {
      SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      SparrowHawk().log("Lane ID must be between 1 and %d for segment %d\n",
          numOfLanes, obstacleSegmentID);
      if (m_logData)
      {
	fprintf(m_logFile, "Cannot insert obstacle at segment %d, lane %d, waypoint %d",
			  obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	fprintf(m_logFile, "Lane ID must be between 1 and %d for segment %d\n",
			  numOfLanes, obstacleSegmentID);
      }
      return false;
    }
  }
  else if (obstacleLaneID != 0)
  {
    int numOfSpots = m_rndf->getZone(obstacleSegmentID)->getNumOfSpots();
    if (obstacleLaneID < 1 || obstacleLaneID > numOfSpots)
    {
      SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      SparrowHawk().log("Lane ID must be between 0 and %d for segment %d\n",
          numOfSpots, obstacleSegmentID);
      if (m_logData)
      {
	fprintf(m_logFile, "Cannot insert obstacle at segment %d, lane %d, waypoint %d",
			  obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	fprintf(m_logFile, "Lane ID must be between 0 and %d for segment %d\n",
			  numOfSpots, obstacleSegmentID);
      }
      return false;
    }
  }

  if (obstacleSegmentID <= numOfSegments)
  {
    int numOfWaypoints =
        m_rndf->getLane(obstacleSegmentID, obstacleLaneID)->getNumOfWaypoints();
    if (obstacleWaypointID < 1 || obstacleWaypointID > numOfWaypoints)
    {
      SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      SparrowHawk().log("Waypoint ID must be between 1 and %d for segment %d lane %d\n",
          numOfWaypoints, obstacleSegmentID, obstacleLaneID);
      if (m_logData)
      {
	fprintf(m_logFile, "Cannot insert obstacle at segment %d, lane %d, waypoint %d",
			  obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	fprintf(m_logFile, "Waypoint ID must be between 1 and %d for segment %d lane %d\n",
			  numOfWaypoints, obstacleSegmentID, obstacleLaneID);
      }
      return false;
    }
  }
  else if (obstacleLaneID == 0)
  {
    int numOfPerimeterPoints =
        m_rndf->getZone(obstacleSegmentID)->getNumOfPerimeterPoints();
    if (obstacleWaypointID < 1 || obstacleWaypointID > numOfPerimeterPoints)
    {
      SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
          obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      SparrowHawk().log("Waypoint ID must be between 1 and %d for segment %d lane %d\n",
          numOfPerimeterPoints, obstacleSegmentID, obstacleLaneID);
      if (m_logData)
      {
	fprintf(m_logFile, "Cannot insert obstacle at segment %d, lane %d, waypoint %d",
			  obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	fprintf(m_logFile, "Waypoint ID must be between 1 and %d for segment %d lane %d\n",
			  numOfPerimeterPoints, obstacleSegmentID, obstacleLaneID);
      }
      return false;
    }
  }
  else if (obstacleWaypointID < 1 || obstacleWaypointID > 2)
  {
    SparrowHawk().log("Cannot insert obstacle at segment %d, lane %d, waypoint %d",
        obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    SparrowHawk().log("Waypoint ID must be between 1 and 2 for segment %d lane %d\n",
        obstacleSegmentID, obstacleLaneID);
    if (m_logData)
    {
      fprintf( m_logFile, "Cannot insert obstacle at segment %d, lane %d, waypoint %d",
			obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      fprintf( m_logFile, "Waypoint ID must be between 1 and 2 for segment %d lane %d\n",
			obstacleSegmentID, obstacleLaneID);
    }
    return false;
  }
  
  DGClockMutex(&m_ObstaclesMutex);
  int obstacleIndex = getObstacleIndex(obstacleID);
  if ((unsigned)obstacleIndex < m_obstacles.size())
  {
    if (m_obstacles[obstacleIndex].getSegmentID() != obstacleSegmentID)
    {
      int newObstacleID = obstacleID + 1;
      while((unsigned)getObstacleIndex(newObstacleID) < m_obstacles.size())
      {
        newObstacleID++;
      }
      SparrowHawk().log("Obstacle ID %d already exists on segment %d. Changed obstacle ID to %d\n",
          obstacleID, m_obstacles[obstacleIndex].getSegmentID(), newObstacleID);
      if (m_logData)
	{
	  fprintf(m_logFile,"Obstacle ID %d already exists on segment %d. Changed obstacle ID to %d\n",
			    obstacleID, m_obstacles[obstacleIndex].getSegmentID(), newObstacleID);
	}
      Obstacle obstacle(newObstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      m_obstacles.push_back(obstacle);
      SparrowHawk().log("Added obstacle %d at segment %d lane %d waypoint %d to the obstacle list\n",
          newObstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      if (m_logData)
	{
	  fprintf(m_logFile,"Added obstacle %d at segment %d lane %d waypoint %d to the obstacle list\n",
			    newObstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	}
    }
    else
    {
      bool obstructedLaneAdded =
          m_obstacles[obstacleIndex].addObstructedLane(obstacleLaneID, obstacleWaypointID);
      if (obstructedLaneAdded)
      {
        SparrowHawk().log("Obstacle %d on segment %d: Added obstructed lane %d waypoint %d\n",
            obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	if (m_logData)
	  {
	    fprintf(m_logFile, "Obstacle %d on segment %d: Added obstructed lane %d waypoint %d\n",
			      obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	  }
      }
      else
      {
        SparrowHawk().log("Obstacle %d on segment %d: Cannot add obstructed lane %d waypoint %d.\n",
		        obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
        SparrowHawk().log("This obstructed lane already exists.\n");
        SparrowHawk().log("Change obstacle ID if you still want to add this obstacle");
	if (m_logData)
	  {
	    fprintf(m_logFile, "Obstacle %d on segment %d: Cannot add obstructed lane %d waypoint %d.\n",
			      obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
	    fprintf(m_logFile, "This obstructed lane already exists.\n");
	    fprintf(m_logFile, "Change obstacle ID if you still want to add this obstacle");
	  }
      }
    }
  }
  else
  {
    Obstacle obstacle(obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    m_obstacles.push_back(obstacle);
    SparrowHawk().log("Added obstacle %d at segment %d lane %d waypoint %d to the obstacle list\n",
        obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
    if (m_logData)
      {
	fprintf(m_logFile, "Added obstacle %d at segment %d lane %d waypoint %d to the obstacle list\n",
			  obstacleID, obstacleSegmentID, obstacleLaneID, obstacleWaypointID);
      }
  }
  DGCunlockMutex(&m_ObstaclesMutex);
  return true;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
int TraversibilityGraphEstimation::getObstacleIndex(int obstacleID)
{
  int i = 0;
  while ((unsigned)i < m_obstacles.size() && m_obstacles[i].getObstacleID() != obstacleID)
  {
    i++;
  }
  
  return i;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::updateObstructedEdges()
{
  for(unsigned i = 0; i < m_obstacles.size(); i++)
  {
    int obsSegmentID = m_obstacles[i].getSegmentID();
    vector<int> obsLaneIDs = m_obstacles[i].getLaneIDs();
    vector<int> obsWaypointIDs = m_obstacles[i].getWaypointIDs();
    int numOfLanesBlocked = m_obstacles[i].getNumOfLanesBlocked();
    for (unsigned j = 0; j < obsLaneIDs.size(); j++)
    {
      Vertex* blockedVertex = m_graph->getVertex(obsSegmentID, obsLaneIDs[j], obsWaypointIDs[j]);
      if (blockedVertex != NULL)
      {
	vector<Edge*> blockedEdges = blockedVertex->getEdges();
	for (unsigned l = 0; l < blockedEdges.size(); l++)
	{
	  blockedEdges[l]->setObstructedLevel(blockedEdges[l]->getObstructedLevel() + numOfLanesBlocked);
	  
	  // Remove this edge from graph if the obstacle fully obstructs the road.
	  // ASSUME THAT THERE IS NO OBSTACLE THAT CAN FULLY BLOCK A ZONE OR AN
	  // INTERSECTION
	  if (blockedEdges[l]->getType() == Edge::ROAD_SEGMENT && 
	      blockedEdges[l]->getObstructedLevel() >= m_rndf->getSegment(obsSegmentID)->getNumOfLanes())
	  {
	    if (m_nosparrow)
	    {
	      cout << "Setting the edge from waypoint " << (blockedEdges[l]->getPrevious())->getSegmentID()
		   << "." << (blockedEdges[l]->getPrevious())->getLaneID() << "."
		   << (blockedEdges[l]->getPrevious())->getWaypointID() << " to " 
		   << (blockedEdges[l]->getNext())->getSegmentID()
		   << "." << (blockedEdges[l]->getNext())->getLaneID() << "." 
		   << (blockedEdges[l]->getNext())->getWaypointID()
		   << " to be fully blocked" << endl;
	    }
	    else
	    {
	      SparrowHawk().
		log("Setting the edge from waypoint %d.%d.%d to %d.%d.%d",
		    (blockedEdges[l]->getPrevious())->getSegmentID(),
		    (blockedEdges[l]->getPrevious())->getLaneID(),
		    (blockedEdges[l]->getPrevious())->getWaypointID(),
		    (blockedEdges[l]->getNext())->getSegmentID(),
		    (blockedEdges[l]->getNext())->getLaneID(),
		    (blockedEdges[l]->getNext())->getWaypointID());
	      SparrowHawk().log("to be fully blocked");
	    }
	    if (m_logData)
	      {
		fprintf(m_logFile, "Setting the edge from waypoint %d.%d.%d to %d.%d.%d",
		    (blockedEdges[l]->getPrevious())->getSegmentID(),
		    (blockedEdges[l]->getPrevious())->getLaneID(),
		    (blockedEdges[l]->getPrevious())->getWaypointID(),
		    (blockedEdges[l]->getNext())->getSegmentID(),
		    (blockedEdges[l]->getNext())->getLaneID(),
		    (blockedEdges[l]->getNext())->getWaypointID());
		fprintf(m_logFile, "to be fully blocked");
	      }
	    // Remove the edge corresponding to the failed segment goals from the graph
	    m_graph->removeEdge(blockedEdges[l]);
	    m_edgesRemoved.push_back(blockedEdges[l]);
	  }
	}
      }
    }
  }
}



//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void TraversibilityGraphEstimation::getGraphThread()
{
  if (m_nosparrow)
  {
    cout << "Start listening to mapper" << endl;
  }
  else
  {
    SparrowHawk().log( "Start listening to mapper\n");
  }
  if (m_logData)
  {
    fprintf (m_logFile, "Start listening to mapper\n");
  }
  SkynetTalker<Graph> graphTalker(m_snKey, SNglobalGloNavMapFromGloNavMapLib, MODmapping);
  while(true)
  {
    //graphTalker.waitForMessage();
    //m_receivedGraph = new Graph();
    Graph* graph = new Graph();
    bool graphReceived = graphTalker.receive(graph);
    DGClockMutex(&m_receivedGraphMutex); 
    if (graphReceived) 
    { 
      if (m_receivedGraph != NULL)
	delete m_receivedGraph;
      m_receivedGraph = graph;
      DGCSetConditionTrue(m_condNewGraph); 
      if (m_nosparrow)
      {
	cout << "Received a new graph" << endl;
	if (DEBUG_LEVEL > 4)
	{
	  cout << "received graph: " << endl;
	  m_receivedGraph->print();
	}
      }
      else
      {
	SparrowHawk().log( "Received a new graph\n");
      }
      if (m_logData)
      {
	fprintf(m_logFile, "Received a new graph\n");
      }
    } 
    else
    {
      cerr << "Error receiving graph" << endl;
      delete m_receivedGraph;
      m_receivedGraph = NULL;
    }
    DGCunlockMutex(&m_receivedGraphMutex); 
    usleep(100000); 
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool TraversibilityGraphEstimation::addCurrentPositionToGraph()
{ 
  UpdateState();
  double northingFront = m_state.utmNorthing + DIST_REAR_AXLE_TO_FRONT*cos(m_state.utmYaw);
  double eastingFront = m_state.utmEasting + DIST_REAR_AXLE_TO_FRONT*sin(m_state.utmYaw);
  double yaw = m_state.utmYaw;
  if (yaw >= PI)
  {
    yaw = yaw - 2*PI;
  }
  double headingDiff, headingLaneDiff;

  bool currentPosAdded = m_graph->addCurrentPosition(northingFront, eastingFront, yaw, 
						     m_rndf, 15*PI/180, 45*PI/180, 500, headingDiff,
						     headingLaneDiff, false);
  if (DEBUG_LEVEL > 0)
  {
    if (m_nosparrow)
    {
      cout << "Current state is Northing = " << northingFront << " Easting = " << eastingFront
	   << " Yaw = " << yaw << endl;
      cout << "Closest waypoints are ";
      if (m_graph->getVertex(0,0,0) != NULL)
      {
	vector<Edge*> edgesFromCurrPos = m_graph->getVertex(0,0,0)->getEdges();
	for (unsigned i = 0; i < edgesFromCurrPos.size(); i++)
	{
	  edgesFromCurrPos[i]->getNext()->print();
	  GPSPoint* closestPoint;
	  if (edgesFromCurrPos[i]->getNext()->getLaneID() != 0)
	  {
	    closestPoint = m_rndf->getWaypoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getLaneID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  else
	  {
	    closestPoint = m_rndf->getPerimeterPoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  if (closestPoint != NULL)
	  {
	    cout << "(" << closestPoint->getNorthing() - northingFront
		 << ", " << closestPoint->getEasting() - eastingFront
		 << ", " << 180/PI * ( atan2(closestPoint->getEasting() - eastingFront, 
					     closestPoint->getNorthing() - northingFront))
		 << ", " << 180/PI * headingDiff << ", " << 180/PI * headingLaneDiff
		 << ")   ";
	  }
	}
      }	
      cout << endl;
    }
    else
    {
      SparrowHawk().log("Current state is Northing = %f, Easting = %f,  Yaw = %f\n",  
			northingFront, eastingFront, yaw);
      SparrowHawk().log( "Closest waypoints are ");
      if (m_graph->getVertex(0,0,0) != NULL)
      {
	vector<Edge*> edgesFromCurrPos = m_graph->getVertex(0,0,0)->getEdges();
	for (unsigned i = 0; i < edgesFromCurrPos.size(); i++)
	{
	  edgesFromCurrPos[i]->getNext()->print();
	  GPSPoint* closestPoint;
	  if (edgesFromCurrPos[i]->getNext()->getLaneID() != 0)
	  {
	    closestPoint = m_rndf->getWaypoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getLaneID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  else
	  {
	    closestPoint = m_rndf->getPerimeterPoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  if (closestPoint != NULL)
	  {
	    SparrowHawk().log("(%f, %f, %f, %f, %f)\n", closestPoint->getNorthing() - northingFront,
			      closestPoint->getEasting() - eastingFront,
			      180/PI * ( atan2(closestPoint->getEasting() - eastingFront, 
					       closestPoint->getNorthing() - northingFront)),
			      180/PI * headingDiff, 180/PI * headingLaneDiff);
	  }
	}
      }	
    }	   
    if (m_logData)
    {
      fprintf(m_logFile, "Current state is Northing = %f, Easting = %f,  Yaw = %f\n",  
			m_state.utmNorthing, m_state.utmEasting, m_state.utmYaw);
      fprintf(m_logFile, "Closest waypoints are ");
      if (m_graph->getVertex(0,0,0) != NULL)
      {
	vector<Edge*> edgesFromCurrPos = m_graph->getVertex(0,0,0)->getEdges();
	for (unsigned i = 0; i < edgesFromCurrPos.size(); i++)
	{
	  edgesFromCurrPos[i]->getNext()->print();
	  GPSPoint* closestPoint;
	  if (edgesFromCurrPos[i]->getNext()->getLaneID() != 0)
	  {
	    closestPoint = m_rndf->getWaypoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getLaneID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  else
	  {
	    closestPoint = m_rndf->getPerimeterPoint(edgesFromCurrPos[i]->getNext()->getSegmentID(),
					       edgesFromCurrPos[i]->getNext()->getWaypointID());
	  }
	  if (closestPoint != NULL)
	  {
	    fprintf(m_logFile,"(%f, %f, %f, %f, %f)\n", closestPoint->getNorthing() - northingFront,
			      closestPoint->getEasting() - eastingFront,
			      180/PI * ( atan2(closestPoint->getEasting() - eastingFront, 
					       closestPoint->getNorthing() - northingFront)),
			      180/PI * headingDiff, 180/PI * headingLaneDiff);
	  }
	}
      }	
    }
  }  
  return currentPosAdded;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double TraversibilityGraphEstimation::getNorthing()
{
  return m_state.utmNorthing;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double TraversibilityGraphEstimation::getEasting()
{
  return m_state.utmEasting;
}
