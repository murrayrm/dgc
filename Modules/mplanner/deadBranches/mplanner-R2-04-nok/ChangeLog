Mon Oct  8 23:29:48 2007	Nok Wongpiromsarn (nok)

	* version R2-04-nok
	BUGS:  
	New files: findMinMissionTime.cc findMinMissionTime.ggo
	FILES: Makefile.yam(43701), mplanner.ggo(43701)
	Merged

	New files: findMinMissionTime.cc findMinMissionTime.ggo
	FILES: Makefile.yam(43663), mplanner.ggo(43663)
	Added a program to compute the minimum possible time to complete
	the mission and the maximum possible average speed.

	FILES: Makefile.yam(43672), mplanner.ggo(43672)
	Added a ggo file for findMinMissionTime

Mon Oct  8 18:07:58 2007	Christian Looman (clooman)

	* version R2-03n
	BUGS:  
	FILES: MissionUtils.cc(43584)
	Increased max. velocity in intersections to 15 m/s

	FILES: MissionUtils.cc(43601)
	Changed max. velocity in intersections again: 5 m/s within
	intersections with stop lines, 8 m/s at intersections without stop
	lines

	FILES: MissionUtils.cc(43584)
	Increased max. velocity in intersections to 15 m/s

Sat Oct  6 15:21:51 2007	Nok Wongpiromsarn (nok)

	* version R2-03m
	BUGS:  
	FILES: DummyTPlanner.cc(43256), MissionUtils.cc(43256),
		RoutePlanner.cc(43256),
		TraversibilityGraphEstimation.cc(43256)
	Fixed bugs that kept mplanner from sending a uturn directive when
	the first seggoal fails

Sat Oct  6 10:33:40 2007	Nok Wongpiromsarn (nok)

	* version R2-03l
	BUGS:  
	FILES: MissionControl.cc(43183), RoutePlanner.cc(43183)
	Replaced dynamic_cast with static_cast so coverity stops
	complaining.

Fri Oct  5 19:34:14 2007	Nok Wongpiromsarn (nok)

	* version R2-03k
	BUGS:  
	FILES: MissionControl.cc(43095), MissionUtils.cc(43095),
		RoutePlanner.cc(43095),
		TraversibilityGraphEstimation.cc(43095)
	Fixed some bug so we do the right thing when obstacle is right in
	front of us when we start.

Tue Oct  2 22:59:39 2007	Nok Wongpiromsarn (nok)

	* version R2-03j
	BUGS:  
	FILES: RoutePlanner.cc(42370)
	Check that current lane reported by planner makes sense. Otherwise,
	trust the current seggoals. This should fix the problem where
	mplanner commanded a uturn back to the same lane at El Toro on
	Sunday.

Sat Sep 29 16:01:09 2007	Nok Wongpiromsarn (nok)

	* version R2-03i
	BUGS:  
	FILES: RoutePlanner.cc(41563), rplannerDisplay.dd(41563)
	Added type of failure on the sparrow display for debugging.

Sat Sep 29 10:56:20 2007	Nok Wongpiromsarn (nok)

	* version R2-03h
	BUGS:  
	FILES: DummyTPlanner.cc(41304), Makefile.yam(41304),
		RoutePlanner.cc(41304), latlon2utm.cc(41304),
		utm2latlon.cc(41304)
	Handle different types of failure

	FILES: Makefile.yam(41415)
	Don't compile the unit tests by default

Sat Sep 22  5:28:14 2007	Nok Wongpiromsarn (nok)

	* version R2-03g
	BUGS:  
	FILES: DummyTPlanner.cc(40000), MissionUtils.cc(40000),
		Obstacle.cc(40000), Obstacle.hh(40000),
		RoutePlanner.cc(40000),
		TraversibilityGraphEstimation.cc(40000),
		TraversibilityGraphEstimation.hh(40000)
	Handle blocked intersection. Will first try to find other route. If
	none exists or all the other routes also fail, make a uturn. If
	route cannot be found after the u-turn, we'll skip that checkpoint.
	Once planner is ready to receive a BACK_UP goal, we'll try backing
	up before skipping checkpoint.

Tue Sep 18 19:54:43 2007	Nok Wongpiromsarn (nok)

	* version R2-03f
	BUGS:  
	FILES: MissionUtils.cc(39369), RoutePlanner.cc(39369)
	Change the type of the second goal to ROAD_SEGMENT instead of
	PARKING_ZONE. If ROAD_SEGMENT fails, mplanner will give
	PARKING_ZONE. If PARKING_ZONE still fails, it'll tell the planner
	to make a u-turn.

	FILES: MissionUtils.cc(39413), RoutePlanner.cc(39413)
	Fixed some bugs based on the coverity results

Tue Sep 18 16:09:15 2007	Nok Wongpiromsarn (nok)

	* version R2-03e
	BUGS:  
	FILES: RoutePlanner.cc(39312), RoutePlanner.hh(39312)
	Uturn should work now even when mapper is not running.

Thu Sep 13 14:41:57 2007	Nok Wongpiromsarn (nok)

	* version R2-03d
	BUGS:  
	FILES: CmdArgs.cc(38587), CmdArgs.hh(38587),
		MissionControl.cc(38587), MissionPlannerMain.cc(38587),
		MissionUtils.cc(38587), RoutePlanner.cc(38587),
		RoutePlanner.hh(38587),
		TraversibilityGraphEstimation.cc(38587),
		TraversibilityGraphEstimation.hh(38587),
		mplanner.ggo(38587), rplannerDisplay.dd(38587)
	Fixed bug found in the field where mplanner sent reset command too
	often. Added a command line option to limit the speed.

Sun Sep  9  9:53:04 2007	Nok Wongpiromsarn (nok)

	* version R2-03c
	BUGS:  
	FILES: CmdArgs.cc(37962), CmdArgs.hh(37962),
		MissionControl.cc(37962), MissionPlannerMain.cc(37962),
		RoutePlanner.cc(37962), RoutePlanner.hh(37962),
		TraversibilityGraphEstimation.cc(37962),
		TraversibilityGraphEstimation.hh(37962),
		mplanner.ggo(37962), rplannerDisplay.dd(37962)
	Added command line options to ensure that we're making forward
	progress. If not, mplanner will reset the stack and replan from where 
	we are

Sat Sep  8  0:52:12 2007	Nok Wongpiromsarn (nok)

	* version R2-03b
	BUGS:  
	FILES: CmdArgs.cc(37934), CmdArgs.hh(37934),
		MissionPlannerMain.cc(37934), RoutePlanner.cc(37934),
		RoutePlanner.hh(37934),
		TraversibilityGraphEstimation.cc(37934),
		TraversibilityGraphEstimation.hh(37934),
		mplanner.ggo(37934)
	Added a command line option for listening to estop. If we're
	relocated during PAUSE, mplanner will replan from current position.

	FILES: RoutePlanner.cc(37935)
	Added more message to log

Fri Sep  7 17:32:05 2007	Nok Wongpiromsarn (nok)

	* version R2-03a
	BUGS:  
	FILES: RoutePlanner.cc(37856),
		TraversibilityGraphEstimation.cc(37856),
		TraversibilityGraphEstimation.hh(37856)
	Make a guess of where we currently are based on seggoals.

	FILES: RoutePlanner.cc(37866),
		TraversibilityGraphEstimation.cc(37866)
	Also update current lane when we replan

	FILES: RoutePlanner.cc(37856),
		TraversibilityGraphEstimation.cc(37856),
		TraversibilityGraphEstimation.hh(37856)
	Make a guess of where we currently are based on seggoals.

	FILES: RoutePlanner.cc(37866),
		TraversibilityGraphEstimation.cc(37866)
	Also update current lane when we replan

Thu Sep  6 22:15:32 2007	Nok Wongpiromsarn (nok)

	* version R2-03
	BUGS:  
	FILES: RoutePlanner.cc(37767)
	Added intersection type to the display

Mon Aug 27 18:33:25 2007	Nok Wongpiromsarn (nok)

	* version R2-02z
	BUGS:  
	FILES: DummyHealthMonitor.cc(35782), MissionControl.cc(35782),
		RoutePlanner.cc(35782), mcontrolDisplay.dd(35782)
	Handle capability that's in the range of 0-1.

Wed Aug 22  0:16:56 2007	Nok Wongpiromsarn (nok)

	* version R2-02y
	BUGS:  
	FILES: RoutePlanner.cc(34833),
		UT_illegalPassingSmallGraph.cc(34833),
		UT_illegalPassingUturnSmallGraph.cc(34833),
		UT_uturnSmallGraph.cc(34833)
	Fixed some of the logic so mplanner can now handle the situation
	where it gets RESET command from the sparrow display when it's
	in the UTURN mode, tplanner
	sends a response with wrong ID (probably when it restarts or gets
	reset) and Alice is in PAUSE at the same time.

Thu Aug 16 23:38:11 2007	Nok Wongpiromsarn (nok)

	* version R2-02x
	BUGS:  
	FILES: MissionControl.cc(33955), RoutePlanner.cc(33955),
		mcontrolDisplay.dd(33955)
	Handle reset in mission control

	FILES: MissionControl.hh(33950), RoutePlanner.cc(33950),
		mcontrolDisplay.dd(33950)
	Added a reset button to the sparrow display

	FILES: RoutePlanner.cc(34002), RoutePlanner.hh(34002)
	Added timeout for pause

	FILES: RoutePlanner.cc(34010), UT_blockedCheckpoint.cc(34010),
		UT_illegalPassingSmallGraph.cc(34010),
		UT_illegalPassingUturnSmallGraph.cc(34010),
		UT_uturnSmallGraph.cc(34010)
	Clear all obstacles when RoutePlanner receives RESET directive

Mon Aug 13 19:42:15 2007	Nok Wongpiromsarn (nok)

	* version R2-02w
	BUGS:  
	FILES: DummyTPlanner.cc(32882), MPlannerCommand.hh(32882),
		RoutePlanner.cc(32882)
	PAUSE => RESET or EMERGENCY_STOP

	FILES: MPlannerCommand.hh(32887), RoutePlanner.cc(32887)
	PAUSE => RESET

	FILES: MissionControl.cc(32940)
	Changed from PAUSE to RESET in MissionControl

	FILES: MissionControl.cc(33018), RoutePlanner.cc(33018)
	Can now send both RESET and EMERGENCY_STOP

	FILES: MissionControl.cc(33181)
	Corrected sensnet_connect to use module ID instead of sensor ID.

Wed Aug  8 21:53:02 2007	Nok Wongpiromsarn (nok)

	* version R2-02v
	BUGS:  
	FILES: MissionControl.cc(32695), RoutePlanner.cc(32695)
	Removed the previous hack that allows tplanner to reset the illegal
	passing flag without notifying mplanner. Sven is fixing the new
	planner so we don't need this anymore.

Mon Aug  6 13:41:25 2007	Nok Wongpiromsarn (nok)

	* version R2-02u
	BUGS:  
	FILES: MissionControl.cc(32292)
	Fixed the speed limit when health monitor is not running

Thu Aug  2 18:38:07 2007	Nok Wongpiromsarn (nok)

	* version R2-02t
	BUGS:  
	FILES: CmdArgs.cc(31581), CmdArgs.hh(31581),
		MissionControl.cc(31581), MissionControl.hh(31581),
		MissionPlannerMain.cc(31581), RoutePlanner.cc(31581),
		mplanner.ggo(31581)
	Keep track of all the checkpoints that have been crossed so if
	mplanner dies and it's restarted with the --continue flag, we don't
	have to go back to the first checkpoint in the mission.

Thu Aug  2 12:20:25 2007	Nok Wongpiromsarn (nok)

	* version R2-02s
	BUGS:  
	FILES: DummyHealthMonitor.cc(31527), MPlannerCommand.hh(31527),
		Makefile.yam(31527), MissionControl.cc(31527),
		MissionUtils.cc(31527), MissionUtils.hh(31527),
		RoutePlanner.cc(31527), RoutePlanner.hh(31527)
	Lower the speed limit when system health is bad. Adjust the cost
	for u-turn and zone based on system health.

Thu Aug  2  0:34:37 2007	Nok Wongpiromsarn (nok)

	* version R2-02r
	BUGS:  
	New files: DummyHealthMonitor.cc UT_blockedCheckpoint.sh
	FILES: DummyTPlanner.cc(31452), MPlannerCommand.hh(31452),
		MissionControl.cc(31452), RoutePlanner.cc(31452),
		RoutePlanner.hh(31452)
	Pause when vehicle capability changes and replan right away based
	on the new capability. (The previous version doesn't pause so it
	won't replan based on the new capability immediately.)

	FILES: Makefile.yam(30917), UT_blockedCheckpoint.cc(30917),
		UT_illegalPassingSmallGraph.cc(30917),
		UT_illegalPassingUturnSmallGraph.cc(30917),
		UT_uturnSmallGraph.cc(30917)
	Added a dummy health monitor

Sat Jul 28  6:12:15 2007	Nok Wongpiromsarn (nok)

	* version R2-02q
	BUGS:  
	FILES: MissionControl.cc(30783)
	Use SENSNET_SKYNET_SENSOR for both sensnet_connect() and
	sensnet_join(). Just a hack for this field test so mplanner can be
	run with ladarfeeder. But I think the right way to do it (based on
	what sensnet.h says) is to use module ID (MODmissionplanner) for
	sensnet_connect() as in the previous release but becasue
	ladarfeeder is using sensor ID for sensnet_connect() and the sensor
	ID for left front bumper ladar is enumed to be the same as
	MODmissionplanner, one of us will have to change and I think it's
	easier to do it in mplanner for now.

Tue Jul 24 16:01:59 2007	Nok Wongpiromsarn (nok)

	* version R2-02p
	BUGS:  
	FILES: MissionControl.cc(30190)
	Fixed sensor id (I was a little confused about when to use
	MODmissionplanner and when to use SENSNET_SKYNET_SENSOR.)

Tue Jul 24 15:15:12 2007	Nok Wongpiromsarn (nok)

	* version R2-02o
	BUGS:  
	FILES: MissionControl.cc(30174), MissionPlannerMain.cc(30174),
		RoutePlanner.cc(30174),
		TraversibilityGraphEstimation.cc(30174)
	Quit most of the threads (except ones from CStateClient) before
	exiting mplanner

Fri Jul 20 23:33:59 2007	Nok Wongpiromsarn (nok)

	* version R2-02n
	BUGS:  
	FILES: MissionControl.cc(29886), RoutePlanner.cc(29886),
		mcontrolDisplay.dd(29886)
	Fixed bug found in the field where mplanner used the wrong sensnet
	id which happened to be the same as left front bumper ladar and
	this caused ladarfeeder to die. Note I didn't see this bug in
	simulation because we don't run ladarfeeder in simulation. Maybe we
	should run all the feeders in simulation too although they don't
	have to do anything but just so we can find this kind of bug
	earlier.

Fri Jul 20  3:56:05 2007	Nok Wongpiromsarn (nok)

	* version R2-02m
	BUGS:  
	New files: CmdArgs.cc CmdArgs.hh
	FILES: MPlannerCommand.hh(29828), Makefile.yam(29828),
		MissionControl.cc(29828), MissionControl.hh(29828),
		MissionPlannerMain.cc(29828), MissionUtils.cc(29828),
		RoutePlanner.cc(29828), RoutePlanner.hh(29828),
		mcontrolDisplay.dd(29828), mplanner.ggo(29828)
	Added command line option for listening to health-monitor and
	adjust the plan based on the health info. Also added interface to
	process-control so it sends heartbeat messages to process-control
	and you can quit mplanner from the process-control console.

	FILES: MissionPlannerMain.cc(29664)
	Call dd_close before quitting the program

Mon Jul  2 17:39:21 2007	Nok Wongpiromsarn (nok)

	* version R2-02l
	BUGS:  
	FILES: Makefile.yam(28460), MissionControl.cc(28460),
		MissionPlannerMain.cc(28460), RoutePlanner.cc(28460),
		UT_rndf.cc(28460)
	Removed two sparrow threads

	FILES: MissionControl.cc(28461), RoutePlanner.cc(28461)
	Handle comments in mdf

Sat Jun 16 10:35:44 2007	Nok Wongpiromsarn (nok)

	* version R2-02k
	BUGS:  
	FILES: DummyTPlanner.cc(27716), RoutePlanner.cc(27716)
	Replan from scratch if tplanner fails because we're outside the
	corridor.

	FILES: RoutePlanner.cc(27804), RoutePlanner.hh(27804),
		TraversibilityGraphEstimation.cc(27804)
	Decreased the sleep before doing uturn when a checkpoint is blocked
	from 10s to 5 s

	FILES: TraversibilityGraphEstimation.cc(27880)
	Fixed getCurrentPos function

	FILES: TraversibilityGraphEstimation.cc(28012)
	Fixed the startup again

Sat Jun  9 23:33:26 2007	Nok Wongpiromsarn (nok)

	* version R2-02j
	BUGS:  
	FILES: TraversibilityGraphEstimation.cc(27597)
	Fixed minor bug when we have to cross a checkpoint from a different
	lane

Sat Jun  9 11:49:27 2007	Nok Wongpiromsarn (nok)

	* version R2-02i
	BUGS:  
	FILES: RoutePlanner.cc(27451), RoutePlanner.hh(27451),
		rplannerDisplay.dd(27451)
	Added more debugging messages to logs. Added more info to the
	sparrow display.

Fri Jun  8 23:18:56 2007	Nok Wongpiromsarn (nok)

	* version R2-02h
	BUGS:  
	FILES: MissionUtils.cc(27356), RoutePlanner.cc(27356),
		RoutePlanner.hh(27356),
		TraversibilityGraphEstimation.cc(27356),
		TraversibilityGraphEstimation.hh(27356)
	Found and fixed bugs in previous logic for choosing between
	skipping checkpoint, crossing checkpoint from other lane, uturn and
	multiple uturns. The new logic is totally different from before.
	Tested for pretty much all scenarios and seems to be working ok.
	Also, added a quick hack for fixing the problem where mplanner and
	tplanner get out of sync because tplanner secretly modifies
	mplanner goals.

Thu Jun  7  9:39:06 2007	Nok Wongpiromsarn (nok)

	* version R2-02g
	BUGS:  
	FILES: TraversibilityGraphEstimation.cc(27046)
	Increased the threshold for difference between our yaw and lane
	direction at startup

Wed Jun  6 23:19:26 2007	Nok Wongpiromsarn (nok)

	* version R2-02f
	BUGS:  
	FILES: TraversibilityGraphEstimation.cc(26984)
	Tweak the startup so we do something somewhat more reasonable when
	we start perpendicular to the lane direction.

Tue Jun  5 22:59:30 2007	Nok Wongpiromsarn (nok)

	* version R2-02e
	BUGS:  
	FILES: RoutePlanner.cc(26762), RoutePlanner.hh(26762)
	Better logic for determining whether we should uturn or change lane
	when a checkpoint is blocked

Tue Jun  5 14:20:29 2007	Nok Wongpiromsarn (nok)

	* version R2-02d
	BUGS:  
	FILES: RoutePlanner.cc(26569)
	oops. the previous mplanner segfaulted when an obstacle is on a
	checkpoint.

Mon Jun  4 15:54:10 2007	Nok Wongpiromsarn (nok)

	* version R2-02c
	BUGS:  
	FILES: MissionControl.cc(26352), MissionPlannerMain.cc(26352),
		RoutePlanner.cc(26352), RoutePlanner.hh(26352)
	Clear map and keep computing route when no route exists because
	there is an obstacle that blocks a one lane road. Tell tplanner to
	stop right before the end of mission. Sparrow quit now exits
	cleanly.

Sun Jun  3 14:07:54 2007	Nok Wongpiromsarn (nok)

	* version R2-02b
	BUGS:  
	New files: UT_blockedCheckpoint.cc
	FILES: DummyTPlanner.cc(26247), RoutePlanner.cc(26247)
	Removed debugging messages.

	FILES: MPlannerCommand.hh(26234), Makefile.yam(26234),
		RoutePlanner.cc(26234),
		TraversibilityGraphEstimation.cc(26234),
		TraversibilityGraphEstimation.hh(26234)
	Added unit test for blocked checkpoint. Fixed couple bugs.

	FILES: MissionControl.cc(26213), RoutePlanner.cc(26213),
		TraversibilityGraphEstimation.cc(26213),
		TraversibilityGraphEstimation.hh(26213)
	Handle the case where an obstacle is on the checkpoint we need to
	cross.

	FILES: MissionControl.cc(26226), RoutePlanner.cc(26226),
		UT_illegalPassingSmallGraph.cc(26226),
		UT_illegalPassingUturnSmallGraph.cc(26226),
		UT_uturnSmallGraph.cc(26226)
	Fixed bugs when sparrow display is not used

Thu May 31 22:50:09 2007	Nok Wongpiromsarn (nok)

	* version R2-02a
	BUGS:  
	FILES: MissionControl.cc(25895), MissionControl.hh(25895),
		MissionPlannerMain.cc(25895), RoutePlanner.cc(25895),
		RoutePlanner.hh(25895),
		UT_illegalPassingSmallGraph.cc(25895),
		UT_illegalPassingUturnSmallGraph.cc(25895),
		UT_uturnSmallGraph.cc(25895), mplanner.ggo(25895)
	Added command line argument for specifying path for log file

Thu May 31 13:53:24 2007	Nok Wongpiromsarn (nok)

	* version R2-02
	BUGS:  
	FILES: MissionControl.cc(25718), MissionControl.hh(25718),
		MissionPlannerMain.cc(25718), RoutePlanner.cc(25718),
		RoutePlanner.hh(25718),
		UT_illegalPassingSmallGraph.cc(25718),
		UT_illegalPassingUturnSmallGraph.cc(25718),
		UT_uturnSmallGraph.cc(25718)
	Lock mutex around sparrow object which is used by both the mission
	control and route planner. Note sure if this is the cause of
	segfault that Noel saw. (It only segfaulted there once in the past
	6 months so it's quite hard to get the same segfault again and I'm
	not sure how to test this.)

Thu May 31  0:49:37 2007	Nok Wongpiromsarn (nok)

	* version R2-01z
	BUGS:  
	FILES: RoutePlanner.cc(25625)
	Minor fix in the sparrow display. Goal completion that corresponds
	to crossing a checkpoint should now be shown correctly on the
	sparrow display.

Wed May 30 11:39:36 2007	Nok Wongpiromsarn (nok)

	* version R2-01y
	BUGS:  
	FILES: MissionControl.cc(25510), MissionPlannerMain.cc(25510),
		RoutePlanner.cc(25510)
	Mission planner now generates 3 log files, one for mission control,
	one for route planner, and one that keeps track of pretty much
	everything.

Mon May 28 10:15:00 2007	Nok Wongpiromsarn (nok)

	* version R2-01x
	BUGS:  
	FILES: DummyTPlanner.cc(24998), RoutePlanner.cc(24998),
		RoutePlanner.hh(24998)
	Do not send directive until tplanner starts listening

	FILES: Makefile.yam(25206), UT_illegalPassingSmallGraph.sh(25206),
		UT_illegalPassingUturnSmallGraph.sh(25206),
		UT_uturnSmallGraph.sh(25206)
	Fixed the unit test files due to the changes in asim

Thu May 24 17:25:01 2007	Nok Wongpiromsarn (nok)

	* version R2-01w
	BUGS:  
	New files: gcInterfaceInstantiate.cc
	FILES: DummyTPlanner.cc(24884), MPlannerCommand.hh(24884),
		Makefile.yam(24884), MissionControl.cc(24884),
		MissionControl.hh(24884), MissionPlannerMain.cc(24884),
		RoutePlanner.cc(24884), RoutePlanner.hh(24884),
		UT_illegalPassingSmallGraph.cc(24884),
		UT_illegalPassingUturnSmallGraph.cc(24884),
		UT_uturnSmallGraph.cc(24884), mplanner.ggo(24884)
	Speed up compilation time. Added command line argument --log-level
	for gcmodule logging. All log files go to ./logs directory.

Thu May 24  1:26:08 2007	Nok Wongpiromsarn (nok)

	* version R2-01v
	BUGS:  
	FILES: TraversibilityGraphEstimation.cc(24751)
	Fixed another segfault when an obstacle is added from sparrow
	display when mplanner receives map from mapper

Wed May 23  1:29:46 2007	Nok Wongpiromsarn (nok)

	* version R2-01u
	BUGS:  
	FILES: DummyTPlanner.cc(24584), MissionUtils.cc(24584),
		RoutePlanner.cc(24584)
	Tell tplanner whether we want to put turn signals on

Sat May 19 12:54:57 2007	Nok Wongpiromsarn (nok)

	* version R2-01t
	BUGS:  
	FILES: MissionPlannerMain.cc(23826), findBoundaries.cc(23826)
	Catch CRTL-C and exit cleanly

	FILES: MissionPlannerMain.cc(23983), RoutePlanner.cc(23983)
	Used dgcFindRouteFile

Sun May 13 10:22:18 2007	Nok Wongpiromsarn (nok)

	* version R2-01s
	BUGS:  
	New files: UT_illegalPassingUturnSmallGraph.cc
		UT_illegalPassingUturnSmallGraph.sh
	FILES: Makefile.yam(22723)
	Added more test scenario

Thu May 10 11:58:18 2007	Nok Wongpiromsarn (nok)

	* version R2-01r
	BUGS:  
	New files: UT_illegalPassingSmallGraph.cc
		UT_illegalPassingSmallGraph.sh UT_rndf.cc
		UT_uturnSmallGraph.cc UT_uturnSmallGraph.sh
	Deleted files: UT_simple.cc
	FILES: MPlannerCommand.hh(22624)
	Fixed the MPlannerCommand so it works with the new release of
	gcinterfaces and added scripts for unit tests.

	FILES: Makefile.yam(22454)
	Added more unit test

	FILES: Makefile.yam(22468), MissionControl.cc(22468),
		RoutePlanner.cc(22468), RoutePlanner.hh(22468),
		TraversibilityGraphEstimation.cc(22468)
	Added two module tests

Sat May  5  9:42:12 2007	Nok Wongpiromsarn (nok)

	* version R2-01q
	BUGS:  
	FILES: DummyTPlanner.cc(22141),
		TraversibilityGraphEstimation.cc(22141)
	Added more message to log

	FILES: DummyTPlanner.cc(22273), MissionUtils.cc(22273),
		MissionUtils.hh(22273)
	Fixed bug when illegal passing is allowed

Thu May  3 23:37:33 2007	Nok Wongpiromsarn (nok)

	* version R2-01p
	BUGS:  
	FILES: RoutePlanner.cc(22061), RoutePlanner.hh(22061),
		TraversibilityGraphEstimation.cc(22061),
		TraversibilityGraphEstimation.hh(22061),
		rplannerDisplay.dd(22061)
	Added a button to remove all the manually inserted obstacles on the
	sparrow display

	FILES: TraversibilityGraphEstimation.cc(22056),
		TraversibilityGraphEstimation.hh(22056)
	Added a function for removing obstacles inserted from sparrow
	display

Thu May  3 18:14:18 2007	Nok Wongpiromsarn (nok)

	* version R2-01o
	BUGS:  
	FILES: MPlannerCommand.hh(22038), MissionControl.cc(22038)
	Fixed the directive class so mplanner compile against version
	R1-00k of gcinterfaces

Sat Apr 28 18:43:19 2007	Nok Wongpiromsarn (nok)

	* version R2-01n
	BUGS:  
	FILES: MissionPlannerMain.cc(21449), RoutePlanner.cc(21449),
		RoutePlanner.hh(21449), mplanner.ggo(21449)
	Added commandline option for not allowing illegal uturn

Sat Apr 28  8:44:41 2007	Nok Wongpiromsarn (nok)

	* version R2-01m
	BUGS:  
	FILES: MissionControl.cc(21287), MissionControl.hh(21287),
		findBoundaries.cc(21287), mcontrolDisplay.dd(21287)
	Put more info in the sparrow display

Thu Apr 26 23:42:43 2007	Nok Wongpiromsarn (nok)

	* version R2-01l
	BUGS:  
	New files: findBoundaries.cc
	FILES: DummyTPlanner.cc(20844), MissionControl.cc(20844),
		MissionControl.hh(20844), RoutePlanner.cc(20844),
		RoutePlanner.hh(20844), mcontrolDisplay.dd(20844),
		rplannerDisplay.dd(20844)
	mplanner that compiles against R1-01a of gcmodule

	FILES: Makefile.yam(21049), MissionControl.cc(21049),
		MissionControl.hh(21049), RoutePlanner.cc(21049)
	Fixed segfault problem when not using sparrow display. Send
	END_OF_MISSION only once.

	FILES: MissionControl.cc(20876), MissionControl.hh(20876),
		mcontrolDisplay.dd(20876)
	Added apx timeout to teh sparrow display

	FILES: MissionControl.cc(20968), RoutePlanner.cc(20968),
		TraversibilityGraphEstimation.cc(20968),
		TraversibilityGraphEstimation.hh(20968)
	Fixed logging in graphEstimator

	FILES: mcontrolDisplay.dd(20846)
	Added QUIT to the sparrow display

Wed Apr 25 23:39:24 2007	Nok Wongpiromsarn (nok)

	* version R2-01k
	BUGS:  
	New files: MPlannerCommand.hh findCornerPoints.cc
		mcontrolDisplay.dd rplannerDisplay.dd
	Deleted files: Interfaces.hh MissionPlanner.cc MissionPlanner.hh
		mplannerDisplay.dd
	FILES: DummyTPlanner.cc(20324), MissionControl.cc(20324),
		MissionControl.hh(20324), MissionPlannerMain.cc(20324),
		RoutePlanner.cc(20324), RoutePlanner.hh(20324)
	Broken one

	FILES: Makefile.yam(20137)
	Added uturn scenario to Makefile.yam

	FILES: Makefile.yam(20410), RoutePlanner.cc(20410),
		RoutePlanner.hh(20410)
	Fixed bug when PAUSE directive is received

	FILES: Makefile.yam(20534), MissionControl.cc(20534),
		MissionControl.hh(20534), MissionPlannerMain.cc(20534),
		RoutePlanner.cc(20534), RoutePlanner.hh(20534)
	Added a tab in sparrow display for mission control

	FILES: MissionControl.cc(20079), MissionControl.hh(20079),
		MissionPlannerMain.cc(20079), RoutePlanner.cc(20079),
		RoutePlanner.hh(20079)
	Added sparrow to MissionControl

	FILES: MissionControl.cc(20328)
	Removed useless conditional statements

	FILES: MissionControl.cc(20412), MissionControl.hh(20412),
		RoutePlanner.cc(20412)
	Listen to astate health

	FILES: MissionControl.cc(20414), RoutePlanner.cc(20414)
	Most of the bugs should be fixed now except sparrow display

	FILES: MissionControl.cc(20473), MissionPlannerMain.cc(20473),
		RoutePlanner.cc(20473)
	Fixed minor bug when PAUSE is received

	FILES: MissionControl.cc(20537), MissionControl.hh(20537),
		RoutePlanner.cc(20537)
	Fixed bugs in sparrow display

	FILES: MissionControl.cc(20556), MissionPlannerMain.cc(20556)
	Sparrow display is not working

	FILES: MissionControl.cc(20595), MissionControl.hh(20595),
		RoutePlanner.cc(20595), RoutePlanner.hh(20595)
	Added a function for logging ERROR message

	FILES: MissionControl.cc(20700), MissionControl.hh(20700),
		MissionPlannerMain.cc(20700), RoutePlanner.cc(20700),
		RoutePlanner.hh(20700),
		TraversibilityGraphEstimation.cc(20700),
		mplanner.ggo(20700)
	Fixed uturn

	FILES: MissionControl.cc(20735), MissionUtils.cc(20735),
		RoutePlanner.cc(20735),
		TraversibilityGraphEstimation.cc(20735)
	Implemented restart mission. Fixed logic when PAUSE is received.
	Everything is cool now.

	FILES: MissionControl.cc(20755), MissionControl.hh(20755),
		MissionPlannerMain.cc(20755), mplanner.ggo(20755)
	Added timeout for apx health

	FILES: MissionControl.cc(20757)
	Fixed minor bug in sparrow display

	FILES: RoutePlanner.cc(20330)
	Removed cout

Fri Apr 20 11:49:34 2007	Nok Wongpiromsarn (nok)

	* version R2-01j
	BUGS:  
	New files: latlon2utm.cc utm2latlon.cc
	FILES: DummyTPlanner.cc(19701), Interfaces.hh(19701),
		Makefile.yam(19701), MissionControl.cc(19701),
		MissionControl.hh(19701), MissionPlannerMain.cc(19701),
		RoutePlanner.cc(19701), RoutePlanner.hh(19701),
		UT_simple.cc(19701), mplannerDisplay.dd(19701)
	Modified DummyTPlanner so it uses GcInterface

	FILES: DummyTPlanner.cc(19807), Makefile.yam(19807)
	DummyTPlanner that uses GcModule.

	FILES: DummyTPlanner.cc(19981), MissionControl.cc(19981),
		MissionControl.hh(19981), RoutePlanner.cc(19981),
		RoutePlanner.hh(19981)
	Removed global variable DEBUG_LEVEL

	FILES: Interfaces.hh(19898), MissionPlannerMain.cc(19898)
	properly closed the log file

	FILES: Interfaces.hh(19974), MissionControl.cc(19974),
		UT_simple.cc(19974)
	Removed things that shouldn't be in UT_simple.cc

	FILES: Makefile.yam(19900), UT_simple.cc(19900)
	Added a script for converting from latlon to UTM

	FILES: RoutePlanner.cc(20054)
	Fixed status so it matches what is defined in the base class

Sat Apr 14 13:44:13 2007	Nok Wongpiromsarn (nok)

	* version R2-01i
	BUGS:  
	FILES: RoutePlanner.cc(19592), RoutePlanner.hh(19592)
	Fixed the route planner so 3 segment goals are always sent to the
	traffic planner.

Wed Apr 11 22:40:31 2007	Nok Wongpiromsarn (nok)

	* version R2-01h
	BUGS: 
	New files: Interfaces.hh UT_simple.cc
	FILES: DummyTPlanner.cc(19429), RoutePlanner.hh(19429)
	Added missing files

Wed Apr 11 21:42:51 2007	Nok Wongpiromsarn (nok)

	* version R2-01g
	BUGS: 
	FILES: DummyTPlanner.cc(19414), Makefile.yam(19414),
		MissionControl.cc(19414), MissionControl.hh(19414),
		MissionUtils.cc(19414), MissionUtils.hh(19414),
		RoutePlanner.cc(19414), RoutePlanner.hh(19414),
		TraversibilityGraphEstimation.cc(19414),
		TraversibilityGraphEstimation.hh(19414)
	Increase the cost of the edge that is blocked instead of removing
	it. An edge is removed only if it isblocked and tplanner fails on
	that edge. Also fixed the include statements so it compiles against
	version R4-00q of the interfaces module.

Tue Apr  3 18:49:05 2007	Nok Wongpiromsarn (nok)

	* version R2-01f
	BUGS: 3199
	FILES: MissionPlannerMain.cc(19105), RoutePlanner.cc(19105),
		TraversibilityGraphEstimation.cc(19105),
		mplanner.ggo(19105)
	Updated TraversibilityGraphEstimation so it compiles against
	version R1-01l of travgraph. Yaw is now in (-pi,pi].  
	Also added commandline option --disable-console for no 
	sparrow display.

Sat Mar 17 13:38:42 2007	Nok Wongpiromsarn (nok)

	* version R2-01e
	BUGS: 
	FILES: RoutePlanner.cc(18283),
		TraversibilityGraphEstimation.cc(18283),
		TraversibilityGraphEstimation.hh(18283)
	Changed the list of edges that are removed from pointer to object
	to fix the problem when map is received from mapper.

Sat Mar 17  2:38:32 2007	Nok Wongpiromsarn (nok)

	* version R2-01d
	BUGS: 
	FILES: RoutePlanner.cc(18104)
	Added more information about segment goals in the logged data

	FILES: RoutePlanner.cc(18196),
		TraversibilityGraphEstimation.cc(18196),
		TraversibilityGraphEstimation.hh(18196)
	Fixed bugs in uturn

Fri Mar 16 13:10:03 2007	Nok Wongpiromsarn (nok)

	* version R2-01c
	BUGS: 
	FILES: RoutePlanner.hh(18055)
	Fixed include statement due to the changes in gcmodule

Fri Mar 16 12:17:26 2007	Nok Wongpiromsarn (nok)

	* version R2-01b
	BUGS: 
	FILES: MissionPlannerMain.cc(18020), RoutePlanner.cc(18020),
		RoutePlanner.hh(18020),
		TraversibilityGraphEstimation.cc(18020)
	Added more information to log file.

Fri Mar 16  1:06:13 2007	Nok Wongpiromsarn (nok)

	* version R2-01a
	BUGS: 
	Deleted files: DummyTPlanner.hh
	FILES: DummyTPlanner.cc(17897), Makefile.yam(17897),
		MissionControl.cc(17897), MissionPlannerMain.cc(17897),
		RoutePlanner.cc(17897), RoutePlanner.hh(17897),
		TraversibilityGraphEstimation.cc(17897),
		mplanner.ggo(17897)
	Message handling (sending directives, receiving status and matching
	them) is now done automatically by GcInterface. Also look for RNDF
	and MDF in DGC_CONFIG_PATH.

	FILES: MissionControl.cc(17915), MissionControl.hh(17915),
		MissionPlannerMain.cc(17915), RoutePlanner.cc(17915),
		RoutePlanner.hh(17915),
		TraversibilityGraphEstimation.cc(17915),
		TraversibilityGraphEstimation.hh(17915),
		mplanner.ggo(17915)
	Added option for enabling logging and fixed the yaw angle so it
	ranges between 0 and 2pi.

Wed Mar 14 18:05:42 2007	Nok Wongpiromsarn (nok)

	* version R2-00h
	BUGS: 
	FILES: DummyTPlanner.cc(17656), MissionPlannerMain.cc(17656),
		RoutePlanner.cc(17656), RoutePlanner.hh(17656),
		TraversibilityGraphEstimation.cc(17656)
	Reduced the number of threads in RoutePlanner by combining the
	sending and receiving message threads into one thread. Made the
	dummyTPlanner a little smarter.

Tue Mar 13 21:17:54 2007	Nok Wongpiromsarn (nok)

	* version R2-00g
	BUGS: 
	FILES: Makefile.yam(17502), MissionControl.cc(17502),
		MissionControl.hh(17502), RoutePlanner.hh(17502)
	MissionControl derived from GcModule and WORKS !!!

	FILES: MissionControl.cc(17514), MissionControl.hh(17514),
		RoutePlanner.cc(17514), RoutePlanner.hh(17514)
	RoutePlanner is now derived from GcModule also.

	FILES: MissionUtils.cc(17540), RoutePlanner.cc(17540),
		RoutePlanner.hh(17540),
		TraversibilityGraphEstimation.cc(17540)
	Fixed the startup so it takes into account lane direction. Still
	need to fix some weird behavior in sparrow display

Mon Mar 12 19:30:17 2007	Nok Wongpiromsarn (nok)

	* version R2-00f
	BUGS: 
	FILES: DummyTPlanner.cc(17289), MissionControl.cc(17289),
		MissionControl.hh(17289), RoutePlanner.cc(17289),
		RoutePlanner.hh(17289),
		TraversibilityGraphEstimation.cc(17289)
	The RoutePlanner in the canonical structure but not yet derived
	from gcmodule. Fixed some bugs in MissionControl.

Fri Mar  9 16:37:36 2007	Nok Wongpiromsarn (nok)

	* version R2-00e
	BUGS: 
	FILES: Makefile.yam(16909), MissionControl.cc(16909),
		MissionControl.hh(16909), MissionUtils.cc(16909),
		MissionUtils.hh(16909), README(16909),
		RoutePlanner.cc(16909), RoutePlanner.hh(16909),
		TraversibilityGraphEstimation.cc(16909)
	Fixed the heading angle computation. Added the default constructor
	for MissionControl. Fixed Makefile. Combined the functions
	planFromCurrentPosition and planNextMission in RoutePlanner.

Wed Mar  7 20:56:05 2007	Nok Wongpiromsarn (nok)

	* version R2-00d
	BUGS: 
	FILES: MissionUtils.cc(16761), MissionUtils.hh(16761),
		RoutePlanner.cc(16761), RoutePlanner.hh(16761),
		TraversibilityGraphEstimation.cc(16761),
		TraversibilityGraphEstimation.hh(16761)
	All the rndf dependencies were moved to the graph estimation class
	(for unit test only). This version of mplanner only commands uturn
	if there are obstacles that block the road.

Wed Mar  7 11:30:20 2007	Nok Wongpiromsarn (nok)

	* version R2-00c
	BUGS: 
	FILES: RoutePlanner.cc(16732)
	Fixed segfault when DEBUG_LEVEL>4 and removed that mutex that
	protects the map because only one thread in RoutePlanner needs it
	now.

Tue Mar  6 21:20:23 2007	Nok Wongpiromsarn (nok)

	* version R2-00b
	BUGS: 
	FILES: DummyTPlanner.cc(16694), MissionUtils.cc(16694),
		RoutePlanner.cc(16694),
		TraversibilityGraphEstimation.cc(16694)
	Fixed bug in the findRoute function. Getting closer to remove the
	rndf dependency. The route planner now only needs RNDF when it
	determines which edges the manually inserted obstacles block. This
	function should belong to the map class but I'll leave it here for
	now for unit test purpose.

Mon Mar  5 19:47:10 2007	Nok Wongpiromsarn (nok)

	* version R2-00a
	BUGS: 
	FILES: DummyTPlanner.cc(16642), Makefile.yam(16642),
		MissionUtils.cc(16642), MissionUtils.hh(16642),
		RoutePlanner.cc(16642), RoutePlanner.hh(16642),
		TraversibilityGraphEstimation.cc(16642),
		TraversibilityGraphEstimation.hh(16642)
	Mission planner that plans route without using rndf information.
	Got the stupid segfault problem figured out. (Thanks to Daniele
	again.)

Sat Mar  3 20:29:05 2007	Nok Wongpiromsarn (nok)

	* version R1-00g
	BUGS: 
	New files: DummyTPlanner.cc DummyTPlanner.hh DummyTPlannerMain.cc
		TraversibilityGraphEstimation.cc
		TraversibilityGraphEstimation.hh
	FILES: Makefile.yam(16407), MissionControl.cc(16407),
		MissionControl.hh(16407), MissionPlanner.cc(16407),
		MissionPlanner.hh(16407), MissionPlannerMain.cc(16407),
		MissionUtils.cc(16407), MissionUtils.hh(16407),
		RoutePlanner.cc(16407), RoutePlanner.hh(16407),
		mplanner.ggo(16407)
	The mission planner that communicates with mapper through graph
	instead of rndf.

	FILES: MissionUtils.cc(16497), RoutePlanner.cc(16497),
		RoutePlanner.hh(16497)
	Moved all the graph functionality from the MissionPlanner class to
	the TraversibilityGraphEstimator class. This version of mplanner
	works with the new interfaces between mapper and mplanner.

Thu Mar  1 22:31:42 2007	Nok Wongpiromsarn (nok)

	* version R1-00f
	BUGS: 
	New files: RoutePlanner.cc RoutePlanner.hh
	Deleted files: Edge.cc Edge.hh Graph.cc Graph.hh Vertex.cc
		Vertex.hh
	FILES: Makefile.yam(16111), MissionPlanner.cc(16111),
		MissionPlanner.hh(16111), MissionUtils.cc(16111),
		MissionUtils.hh(16111)
	moved the graph structure to the travgraph module, including all
	the relevant functions.

	FILES: MissionControl.cc(16021), MissionControl.hh(16021)
	Added route planning module and modified mission control module so
	it handles some simple failure.

Wed Feb 21 22:38:57 2007	Nok Wongpiromsarn (nok)

	* version R1-00e
	BUGS: 
	New files: MissionControl.cc MissionControl.hh
	FILES: Makefile.yam(15405), MissionPlanner.cc(15405)
	Updated to work with the new Status.hh in revision R2-00b of
	interfaces

Tue Feb 20 17:17:37 2007	Nok Wongpiromsarn (nok)

	* version R1-00d
	BUGS: 
	FILES: Makefile.yam(15204), MissionPlanner.cc(15204),
		MissionPlanner.hh(15204)
	fixed mission planner so it compiles against revision R2-00a of
	interfaces and skynet

Thu Feb  1 20:34:28 2007	Nok Wongpiromsarn (nok)

	* version R1-00c
	BUGS: 
	FILES: MissionPlanner.cc(14000), MissionUtils.cc(14000),
		MissionUtils.hh(14000)
	Modified mission planner so it takes into account the orientation
	of the vehicle when planning from current position

Wed Jan 31 11:36:23 2007	Nok Wongpiromsarn (nok)

	* version R1-00b
	BUGS: 
	New files: MissionUtils.cc MissionUtils.hh mplanner.ggo
	Deleted files: missionPlanner.ggo missionUtils.cc missionUtils.hh
	FILES: Makefile.yam(13711), MissionPlanner.cc(13711),
		MissionPlanner.hh(13711)
	Modified the variable names due to the changes in VehicleState.

Wed Jan 24 23:58:21 2007	Nok Wongpiromsarn (nok)

	* version R1-00a
	BUGS: 
	New files: Edge.cc Edge.hh Graph.cc Graph.hh MissionPlanner.cc
		MissionPlanner.hh MissionPlannerMain.cc Obstacle.cc
		Obstacle.hh Vertex.cc Vertex.hh missionPlanner.ggo
		missionUtils.cc missionUtils.hh mplannerDisplay.dd
	FILES: Makefile.yam(13137)
	Added all the files needed to by mplanner

Tue Jan 23 20:03:35 2007	Nok Wongpiromsarn (nok)

	* version R1-00
	Created mplanner module.























































































