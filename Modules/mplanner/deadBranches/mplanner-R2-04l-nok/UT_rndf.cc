#include <iostream>
#include "rndf/RNDF.hh"
#include "dgcutils/ggis.h"
#include "travgraph/Graph.hh"
using namespace std;

int main(int argc, char **argv) 
{
  
  RNDF* m_rndf = new RNDF();
  //if (!m_rndf->loadFile("/home/users/nok/navigation-nok01/src/routes-stluke/stluke_small.rndf"))
  if (!m_rndf->loadFile("/home/users/nok/alice-hardware-nok02/etc/routes-eltoro/eltoro_offroad.rndf"))
  {
    cerr << "ERROR:  Unable to load RNDF file " << endl;
    exit(1);
  }

  m_rndf->assignLaneDirection();
  //m_rndf->print();

  /*
  vector<Segment*> segments = m_rndf->getAllSegments();
  for (unsigned i=0; i < segments.size(); i++)
    cout << i << endl;
  */

  Graph* graph = new Graph(m_rndf);
  //graph->print();
  Vertex entryVertex(23,1,3);
  Vertex exitVertex(23,1,4);
  if (graph->getVertex(24,1,5) == NULL)
    cout <<" 24.1.5 is NULL" << endl;
  if (graph->getVertex(24,1,6) == NULL)
    cout << " 24.1.6 is NULL" << endl;
  /*
  cout << "isSparse = " << graph->isSparse(graph->getVertex(24,1,1), 
					   graph->getVertex(24,1,5)) << endl;
  */
  cout << "isSparse = " << graph->isSparse(&entryVertex, 
					   &exitVertex) << endl;

  delete m_rndf;
  delete graph;
}
