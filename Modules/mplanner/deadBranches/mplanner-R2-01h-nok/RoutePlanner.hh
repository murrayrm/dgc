/*!**
 * Nok Wongpiromsarn
 * March 1, 2007
 */


#ifndef ROUTEPLANNER_HH
#define ROUTEPLANNER_HH

#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <string>
#include <vector>
#include <deque>
#include <math.h>
//#include "alice/estop.h"
#include "dgcutils/DGCutils.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include "rndf/RNDF.hh"
#include "travgraph/Graph.hh"
#include "gcinterfaces/SegGoals.hh"
#include "gcinterfaces/SegGoalsStatus.hh"
#include "dgcutils/GlobalConstants.h"
#include "Obstacle.hh"
#include "MissionUtils.hh"
#include "MissionControl.hh"
#include "TraversibilityGraphEstimation.hh"
#include "gcmodule/GcModule.hh"
#include "gcmodule/GcInterface.hh"
#include "Interfaces.hh"

#define LEAST_NUM_SEGGOALS_STORED 8
#define LEAST_NUM_SEGGOALS_TPLANNER_STORED 3
#define NUM_SEGGOALS_DISPLAYED 6

extern int DEBUG_LEVEL;

typedef GcInterface<SegGoals, SegGoalsStatus, SNsegGoals, SNtplannerStatus, MODmissionplanner> MTInterface ;

/*! The structure for merged directive that is "sent" from arbiter to control. 
 * Since the arbiter just passes through the directive MergedDirective has
 * the same structure as MContrDirective. */
typedef MContrDirective RPlannerMergedDirective;

/*! The structure for status of merged directive. This status is "sent"
 *  from control back to arbiter. The structure for this is the same
 *  as MContrDirectiveStatus. */
typedef MControlDirectiveResponse RPlannerControlStatus;


class RoutePlannerControlStatus : public ControlStatus
{
public:
  RPlannerControlStatus rpcs;
};

class RoutePlannerMergedDirective : public MergedDirective
{
public:
  RPlannerMergedDirective rpmd;
};

/*! The structure for response from tplanner */
struct RPlannerContrMsgResponse
{
  enum Status{ QUEUED, SENT, ACCEPTED, COMPLETED, FAILED };
  RPlannerContrMsgResponse()
  {
    // initialize the segment goals to zeros to appease the memory profilers
      memset(this, 0, sizeof(*this));
      this->status = QUEUED;
  }
  Status status;
  SegGoalsStatus::ReasonForFailure reason;
};

/*! The structure for pairing up the directives sent to the
 *  traffic planner (but in the language of the control) and the response
 *  to this directive. */
struct RPlannerContrMsgWrapper
{
  RPlannerContrMsgWrapper()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
    response.status = RPlannerContrMsgResponse::QUEUED;
  }
  SegGoals directive;  // goalID is in here
  RPlannerContrMsgResponse response;
};


/**
 * RoutePlanner class.
 * This is a main class for the mission planner where information from 
 * gloNavMap (mapping) and MDF file is used to compute segment goals
 * and send to the traffic planner.
 * \brief Main class for mission planner function
 */
class RoutePlanner : public GcModule
{
  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param m_nosparrow is true when all the threads associated with
   * sparrow are not running. This parameter is set in MissionPlannerMain
   * and never changed. */
  bool m_nosparrow;

  bool m_logData;
  FILE* m_logFile;
  int VERBOSITY_LEVEL;

  /*!\param m_rndf is a pointer to the RNDF object. */
  RNDF* m_rndf;

  /*!\param m_graphEstimator is the TraversibilityGraphEstimation object. */
  TraversibilityGraphEstimation m_graphEstimator;

  /*!\param m_travGraph is the corresponding graph of the current RNDF
   * that mission planner uses to compute segment goals. */
  Graph* m_travGraph;

  /*!\param m_minSpeedLimits is the vector of min speed limits in m/s*/
  vector<double> m_minSpeedLimits;

  /*!\param m_maxSpeedLimits is the vector of max speed limits in m/s*/
  vector<double> m_maxSpeedLimits;

  /*!\param m_missionControl is the Mission Control object which keeps
   * track of where we are in the mission. */
  MissionControl m_missionControl;

  /*!\param CkptcurrentSegmentID, currentCkptLaneID, currentCkptWaypointID is the
   * waypoint ID of the current checkpoint */
  int m_currentCkptSegmentID, m_currentCkptLaneID, m_currentCkptWaypointID;

  /*!\param m_missionID tells us where we are in MDF */
  int m_missionID;

  /*!\param m_nextGoalID is the ID of the next goal to be added 
   * to m_segGoalsSeq. */
  int m_nextGoalID; 

  /*!\param m_segGoalsStatus is the status of the traffic planner which is
   * used in the planning loop to determine if we need to replan. */
  SegGoalsStatus m_segGoalsStatus;

  /*!\param SparrowHawk display */
  CSparrowHawk *shp;

  /*!\param Sparrow variables */
  int sparrowNextGoalIDs[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints1[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextSegments2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextLanes2[NUM_SEGGOALS_DISPLAYED];
  int sparrowNextWaypoints2[NUM_SEGGOALS_DISPLAYED];
  double sparrowMinSpeed, sparrowMaxSpeed;
  bool sparrowIllegalPassingAllowed, sparrowStopAtExit, sparrowIsExitCheckpoint;

  /*!\param Obstacle inserted by sparrow */
  int obstacleID;
  int obstacleSegmentID;
  int obstacleLaneID;
  int obstacleWaypointID;

  /*!\param control status sent from control to arbiter */
  RoutePlannerControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  RoutePlannerMergedDirective m_mergedDirective;

  /*!\param goalID of control directive that corresponds to the completion of
   * m_mergedDirective */
  int m_mergedDirectiveGoalID;

  /*!\param goalID of the last directive that is planned */
  int m_lastPlannedMergedDirectiveID;

  /*!\param the previous merged directive that needs to be completed before m_mergedDirective.
   * When m_mergedDirective is near completion, it will be updated
   * by the arbiter but control still needs to keep it to make sure that
   * it is completed. */
  RPlannerMergedDirective m_prevMergedDirective;

  /*!\param goalID of control directive that corresponds to the completion of
   * m_prevMergedDirective */
  int m_prevMergedDirectiveGoalID;

  /*!\param directives currently stored in control waiting to be added to contrGcPort */
  deque<SegGoals> m_contrDirectiveQ;

  /*!\param the mutex to protect m_contrDirectiveQ */
  pthread_mutex_t m_contrDirectiveQMutex;

  /*!\param GcInterface variable */
  MTInterface* mtInterface;
  MTInterface::Southface* mtInterfaceSF;

public:
  /*! Constructor */
  RoutePlanner(int skynetKey, bool bWaitForStateFill, bool nosparrow, char* RNDFFileName,
      char* MDFFileName, bool usingGloNavMapLib, bool logData, FILE* logFile, int debugLevel, bool verbose);
      
  /*! Standard destructor */
  virtual ~RoutePlanner();

  /*! Method to run in separate thread.  This function runs the dd_loop() 
   *  Sparrow method to update the screen and take keyboard commands. */
  void SparrowDisplayLoop();

  /*! Method to update the variables that are to be displayed in the Sparrow
   *  interface. */
  void UpdateSparrowVariablesLoop();

  /*! This is the function that continually runs the planner in a loop */
  void MPlanningLoop(void);

  /*! This is the function that continually reads tplanner status, transmits
   *  segment goals to tplanner and update status of the messages in the m_contrGcPortMsgQ.
   *  We don't need this anymore because GcInterface already handles this
   void messageThread(); */

private:
  /*! Arbitration for the route planner module. ControlStatus is the input and
   *  MergedDirective is the output of this function. */
  void arbitrate(ControlStatus*, MergedDirective*);

  /*! Control for the route planner module. ControlStatus is the output and
   *  MergedDirective is the input of this function. */
  void control(ControlStatus*, MergedDirective*);

  /*! Add the manually inserted obstacle to the map */
  void sparrowInsertObstacle();

  /*! Send segment goals */
  void sendSegGoals();

  /*! Request graph */
  void getNewGraph();

  /*! Plan segment goals from (segment1,lane1,waypoint1) to (segment2, lane2, waypoint2)
   *  if (segment1 = 0), plan from current position */
  vector<SegGoals> planSegGoals(int, int, int, int, int, int, int);

  /*! Compute segment goals with uturn allowed */
  vector<SegGoals> addUturn(Vertex*, Vertex*);

  /*! Flush m_contrGcPortMsgQ and m_contrDirectiveQ */
  void resetGoals();

  /*! Parse speed limit from MDF */
  bool loadMDFFile(char*, RNDF*);
  void parseSpeedLimit(ifstream*, RNDF*);

  /*! A function to print mission on sparrow display */
  void printMissionOnSparrow(deque<SegGoals> segGoals, int startIndex);

  /*! A function to log mission to the log file */
  void logMission(deque<SegGoals> segGoals, int startIndex);

};

#endif  // ROUTEPLANNER_HH
