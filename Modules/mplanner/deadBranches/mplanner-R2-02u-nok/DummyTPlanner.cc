/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */
 
#include <iostream>
#include "skynettalker/SkynetTalker.hh"
#include "gcinterfaces/SegGoals.hh"
#include "gcinterfaces/SegGoalsStatus.hh"
#include "skynet/skynet.hh"
#include "gcmodule/GcModule.hh"
#include "gcmodule/GcInterface.hh"
#include "RoutePlanner.hh"

class CTrafficPlanner : public GcModule
{
  
  ControlStatus m_controlStatus;
  MergedDirective m_mergedDirective;
  RTInterface::Northface* rtInterfaceNF;
  bool isInit;

  //SkynetTalker<SegGoalsStatus> statusTalker;
  //SkynetTalker<SegGoals> segGoalsTalker;

public:
  /*! Contstructor */
  CTrafficPlanner(int skynetKey) : 
    GcModule( "TrafficPlanner", &m_controlStatus, &m_mergedDirective, 100000, 100000 )
    //statusTalker(skynetKey, SNtplannerStatus, MODtrafficplanner) 
    //segGoalsTalker(skynetKey, SNsegGoals, MODtrafficplanner)
  {
    //this->setLogLevel(9);
    rtInterfaceNF = RTInterface::generateNorthface(skynetKey, this);
    isInit = false;

  }

  void arbitrate(ControlStatus* cs, MergedDirective* md) 
  {
    if (!isInit)
    {
      SegGoalsStatus segGoalsStatus;
      segGoalsStatus.goalID = 0;
      segGoalsStatus.status = SegGoalsStatus::COMPLETED;

      cout << "Sending status goal " << segGoalsStatus.goalID << endl;
      rtInterfaceNF->sendResponse(&segGoalsStatus);
      sleep(5);
      
      while (!rtInterfaceNF->haveNewDirective())
      {    
	cout << "Sending status goal " << segGoalsStatus.goalID << endl;
	rtInterfaceNF->sendResponse(&segGoalsStatus);
	sleep(5);
      }

      //statusTalker.send(&segGoalsStatus);
      
      //cout << "Receiving status" << endl;
      //statusTalker.receive(&segGoalsStatus);
      //cout << "status received" << endl;
      
      //statusTalker.send(&segGoalsStatus);
      cout << "Successfully notified mission planner that I'm joining the group" <<  endl;
      isInit = true;
    }

    int lastFailedGoalID = 0;
    bool lastFailedGoalResolved = true;

    if (rtInterfaceNF->haveNewDirective())
      //if (false)
      {
	SegGoals segGoals;
	SegGoalsStatus segGoalsStatus;
	//segGoalsTalker.receive(&segGoals);
	rtInterfaceNF->getNewDirective(&segGoals);

	cout << "segGoals " << segGoals.goalID << " received" << endl;
	segGoals.print();
	cout << endl;
	segGoalsStatus.goalID = segGoals.goalID;
	
	if (!lastFailedGoalResolved && lastFailedGoalID == segGoals.goalID)
	{
	  lastFailedGoalResolved = true;
	}

	if (segGoals.entrySegmentID == 9 && segGoals.entryLaneID == 2 && segGoals.entryWaypointID == 3 && 
	    segGoals.goalID != lastFailedGoalID)
	{
	  segGoalsStatus.status = SegGoalsStatus::FAILED;	
	  segGoalsStatus.reason = SegGoalsStatus::OUTSIDE_CORRIDOR;
	  segGoalsStatus.currentSegmentID = 0;
	  segGoalsStatus.currentLaneID = 0;
	  segGoalsStatus.lastWaypointID = 0;
	  lastFailedGoalID = segGoals.goalID;
	  lastFailedGoalResolved = false;
	}
	else
	{
	  segGoalsStatus.status = SegGoalsStatus::COMPLETED;
	}
	
	if (lastFailedGoalResolved || segGoalsStatus.status == SegGoalsStatus::FAILED)
	{
	  sleep(1);
	  cout << "Sending goal " << segGoalsStatus.goalID << " status: " << segGoalsStatus.status << endl;
	  rtInterfaceNF->sendResponse(&segGoalsStatus);
	  //statusTalker.send(&segGoalsStatus);
	  cout << "Successfully sent status for goal " << segGoalsStatus.goalID << endl;
	}
      } 
  }
  void control(ControlStatus* cs, MergedDirective* md) {}
};

int main(int argc, char **argv)
{
  int sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;
  CTrafficPlanner* pTrafficPlanner = new CTrafficPlanner(sn_key);
  pTrafficPlanner->Start();
  while (!pTrafficPlanner->IsStopped())
  {
    sleep(1);
  }
  return 0;
}
