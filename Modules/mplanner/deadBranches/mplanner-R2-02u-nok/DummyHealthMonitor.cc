/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */
 
#include <iostream>
#include <skynet/skynet.hh>
#include <skynettalker/SkynetTalker.hh>
#include <interfaces/VehicleCapability.h>

class DummyHealthMonitor
{
  SkynetTalker<VehicleCapability> healthTalker;
  
public:
  /*! Contstructor */
  DummyHealthMonitor(int skynetKey) : healthTalker(skynetKey, SNvehicleCapability, MODhealthMonitor)
  {
    sendMessage();
  }

  void sendMessage()
  {
    VehicleCapability vehicleCap;
    int numLoop = 0;
    while(true) {
      numLoop++;
      vehicleCap.timestamp = 0;
      vehicleCap.intersectionRightTurn = MAX_VEHICLE_CAPABILITY;
      vehicleCap.nominalDriving = MAX_VEHICLE_CAPABILITY;
      if (numLoop > 200 && numLoop < 400)
	vehicleCap.intersectionRightTurn = MAX_VEHICLE_CAPABILITY - 1;
      if (numLoop > 300 && numLoop < 500)
	vehicleCap.nominalDriving = MAX_VEHICLE_CAPABILITY - 1;
      vehicleCap.intersectionLeftTurn = MAX_VEHICLE_CAPABILITY;
      vehicleCap.intersectionStraightTurn = MAX_VEHICLE_CAPABILITY;
      vehicleCap.uturn = MAX_VEHICLE_CAPABILITY;
      vehicleCap.nominalStopping = MAX_VEHICLE_CAPABILITY;
      vehicleCap.nominalZoneRegionDriving = MAX_VEHICLE_CAPABILITY;
      vehicleCap.nominalNewRegionDriving = MAX_VEHICLE_CAPABILITY;
      healthTalker.send(&vehicleCap);
      usleep(100000);
    }
  }
};

int main(int argc, char **argv)
{
  int sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;
  DummyHealthMonitor healthMonitor(sn_key);
  return 0;
}
