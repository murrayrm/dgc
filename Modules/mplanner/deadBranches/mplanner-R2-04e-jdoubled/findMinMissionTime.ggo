#
# This is the source file for managing mission planner command-line options.  
# It uses gengetopt (http://www.gnu.org/software/gengetopt/gengetopt.html)
#
# Nok Wongpiromsarn, 2007-01-05
#
# Specification:
#   (1) no command-line options required if default settings are ok
#   (2) Command-line options will override default settings

package "findMinMissionTime"
purpose "findMinMissionTime computes the minimum possible time to complete the mission 
	 and the maximum possible average speed."
version "1.0"

# Options for rndf and mdf
option "rndf" - "rndf file"
  string default="darpa_sample.rndf" no
option "mdf" - "mdf file"
  string default="darpa_sample_simpleloop.mdf" no
option "rndf-start" - "set the start point as a waypoint from the RNDF"
  string default="0.0.0" no

# Options for logging
option "enable-logging" l "enable data logging" flag off
option "logfile" - "the name of the log file"
  string default="minMissionTime.log" no

# Option for speed limit
option "max-speed" - "the maximum speed in MPH" 
  float default="30.0" no
option "time-stopline" - "Time added to account for stopping at a stop line"
  float default="3.0" no
option "time-uturn" - "Time added to account for making a uturn."
  float default="5.0" no
option "time-intersection" - "Time added to account for going through an intersection."
  float default="3.0" no
option "time-zone" - "Time added to account for going in and out of a parking zone."
  float default="10.0" no