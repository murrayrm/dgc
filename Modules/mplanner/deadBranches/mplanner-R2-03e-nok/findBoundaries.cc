#include <iostream>
#include <math.h>
#include "dgcutils/ggis.h"
#include "rndf/RNDF.hh"

using namespace std;

int main(int argc, char **argv) 
{
  RNDF rndf;
  rndf.loadFile("../routes-santaanita/santaanita_sitevisit.rndf");
  int segID = 0;

  while (segID < 1 || segID > 5) {
  cout << "Segment: ";
  cin >> segID;
  }

  Lane* lane1 = rndf.getSegment(segID)->getLane(1);
  Lane* lane2 = rndf.getSegment(segID)->getLane(2);

  if (lane1->getNumOfWaypoints() != lane2->getNumOfWaypoints()) {
    cerr << "num of waypoints on lane 1 != num of waypoints on lane 2" << endl;
    exit(1);
  }

  cout << "Segment " << segID << endl;

  int numOfWaypoints = lane1->getNumOfWaypoints();
  double northing1, northing2, easting1, easting2;
  GisCoordLatLon latlon;
  GisCoordUTM utm;
  utm.zone = 11;
  utm.letter = 'S';

  cout << "Lane1 waypoints:" << endl;
  for (int i = 0; i < numOfWaypoints; i++) {
    Waypoint* waypoint1 = lane1->getWaypoint(i+1);
    northing1 = waypoint1->getNorthing();
    easting1 = waypoint1->getEasting();
    utm.n = northing1;
    utm.e = easting1;
    gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
    printf ("%d\t%9f\t%9f\t%9f\t%9f\n", i+1, latlon.latitude, latlon.longitude, utm.n, utm.e);
  }

  cout << "Lane2 waypoints:" << endl;
  for (int i = 0; i < numOfWaypoints; i++) {
    Waypoint* waypoint2 = lane2->getWaypoint(i+1);
    northing2 = waypoint2->getNorthing();
    easting2 = waypoint2->getEasting();
    utm.n = northing2;
    utm.e = easting2;
    gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
    printf ("%d\t%9f\t%9f\t%9f\t%9f\n", i+1, latlon.latitude, latlon.longitude, utm.n, utm.e);
  }
  
  cout << "Lane boundary:" << endl;
  for (int i = 0; i < numOfWaypoints; i++) {
    Waypoint* waypoint1 = lane1->getWaypoint(i+1);
    Waypoint* waypoint2 = lane2->getWaypoint(numOfWaypoints - i);
    northing1 = waypoint1->getNorthing();
    easting1 = waypoint1->getEasting();
    northing2 = waypoint2->getNorthing();
    easting2 = waypoint2->getEasting();
    double n = (northing1 + northing2)/2;
    double e = (easting1 + easting2)/2;
    utm.n = n;
    utm.e = e;
    gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
    printf ("%d\t%9f\t%9f\t%9f\t%9f\n", i+1, latlon.latitude, latlon.longitude, utm.n, utm.e);
  }

  cout << "Edge 1:" << endl;  
  for (int i = 0; i < numOfWaypoints; i++) {
    Waypoint* waypoint1 = lane1->getWaypoint(i+1);
    Waypoint* waypoint2 = lane2->getWaypoint(numOfWaypoints - i);
    northing1 = waypoint1->getNorthing();
    easting1 = waypoint1->getEasting();
    northing2 = waypoint2->getNorthing();
    easting2 = waypoint2->getEasting();
    double width = sqrt(pow(northing1 - northing2, 2) + 
			pow(easting1 - easting2, 2));
    double ang = atan2(northing1 - northing2, easting1 - easting2);
    double n = northing1 + width*sin(ang)/2;
    double e = easting1 + width*cos(ang)/2;
    utm.n = n;
    utm.e = e;
    gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
    printf ("%d\t%9f\t%9f\t%9f\t%9f\n", i+1, latlon.latitude, latlon.longitude, utm.n, utm.e);
  }

  cout << "Edge 2:" << endl;  
  for (int i = 0; i < numOfWaypoints; i++) {
    Waypoint* waypoint1 = lane1->getWaypoint(i+1);
    Waypoint* waypoint2 = lane2->getWaypoint(numOfWaypoints - i);
    northing1 = waypoint1->getNorthing();
    easting1 = waypoint1->getEasting();
    northing2 = waypoint2->getNorthing();
    easting2 = waypoint2->getEasting();
    double width = sqrt(pow(northing1 - northing2, 2) +
			pow(easting1 - easting2, 2));
    double ang = atan2(northing2 - northing1, easting2 - easting1);
    double n = northing2 + width*sin(ang)/2;
    double e = easting2 + width*cos(ang)/2;
    utm.n = n;
    utm.e = e;
    gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
    printf ("%d\t%9f\t%9f\t%9f\t%9f\n", i+1, latlon.latitude, latlon.longitude, utm.n, utm.e);
  }

}
