/* 
** AliceConstants.h
**
**NOTE TO ANYONE CONCERNED: On H's go-ahead, I cleaned up a superfluous reference to the incorrect (Bob's) vehicle mass. -- George Hines
** 
** This header is for any needed inherent physical constants of the vehicle.
** ******** This file contains shop measurements for shop-measurable statistics, taken before sensors were mounted and befor the new bumper
** ******** was installed.  More accurate measurements will be taken after sensor installation and pending characterization by STI
**
** ******** NOTE: everything below H's comment below ("Constant definitions for the model of Alice") is considered not shop-measurable.
** ******** They are still Bob constants until STI does their thing.
**
**
*/
#ifndef VEHICLECONSTANTS_H
#define VEHICLECONSTANTS_H

#define  VEHICLE_MASS     4133.6    // [kg] as reported by STI

#define  VEHICLE_LENGTH   5.4356  // [m] length of the vehicle from the end of the hitch to the front of the front bumper.

#define MAX_VEHICLE_LENGTH 6.30 // [m] from front of front ladar to rear of rear ladar

#define  MAX_FRONT_BUMPER_WIDTH     2.37 // [m] widest width of front bumper
#define  VEHICLE_WIDTH    2.1336  // [m] width of the vehicle, measured between the sidewalls of the front tires.
#define  MAX_REAR_AXLE_WIDTH 2.2  // [m] width of vehicle, measured at rear wheels (including DMI and equal amount of free space on right side)
#define  VEHICLE_HEIGHT   2.2352  // [m] height of the vehicle from ground to crown of roof.  NOTE: this does NOT include Yakima rack or sensors!!!

#define  DIST_REAR_TO_REAR_AXLE    1.16  // [m] distance between the rear-most point of Alice & the center of the rear axle along the center line of the vehicle
#define MAX_DIST_REAR_TO_REAR_AXLE 1.65  // [m] distance from rear axle to rear end of rear bumper ladar.

#define  DIST_REAR_AXLE_TO_FRONT   ( VEHICLE_LENGTH - DIST_REAR_TO_REAR_AXLE ) // [m] distance from the center of the rear axle to the frontmost point on Alice

#define MAX_DIST_REAR_AXLE_TO_FRONT (MAX_VEHICLE_LENGTH - MAX_DIST_REAR_TO_REAR_AXLE ) // [m] 

#define  DIST_FRONT_AXLE_TO_FRONT  1.04  // [m] distance between the center of the front axle and the front most point of Alice along the center line of the vehicle

//all max_steer constants are now consistent with H and Dima's experimental data fitting
#define  VEHICLE_MAX_LEFT_STEER     0.45 // [rad] maximum turning angle to the left
#define  VEHICLE_MAX_RIGHT_STEER    0.45 // [rad] maximum turning angle to the right
#define  VEHICLE_MAX_AVG_STEER      0.45 // [rad] average max turning angle
#define  VEHICLE_MAX_TAN_AVG_STEER  0.48306 // tan(average max turning angle)

// minimum turning radius of the rear axle (bicycle model)
#define  VEHICLE_MIN_TURNING_RADIUS ( (VEHICLE_AXLE_DISTANCE) / (VEHICLE_MAX_TAN_AVG_STEER) )

#define  VEHICLE_H   1.0668  // [m], height of Center of Mass from ground
                                 // TODO: NOTE: this is the worst case scenario, 
                                 // since cannot know from spec sheet
                                    
#define  VEHICLE_L   (1.759/2.0) // [m], distance of left wheel to projection of CM down on axle
#define  VEHICLE_R   (1.759/2.0) // [m], distance of right wheel to projection of CM down on axle


#define  DIFFERENTIAL_RATIO   3.73   // The ratio of the driveshaft to the axle.  
#define  VEHICLE_TIRE_RADIUS   0.3937 // [m]  
#define  TORQUE_CONVERTER_EFFICIENCY  .8 // This is Tully and Jeffs best estimate from back of the hand

#define  VEHICLE_AXLE_DISTANCE   3.55 // [m] distance between front and rear axles
#define  VEHICLE_WHEELBASE       (VEHICLE_AXLE_DISTANCE)

#define  VEHICLE_REAR_TRACK      1.7145  // [m] distance between centerline of rear wheels
#define  VEHICLE_FRONT_TRACK     1.8034  // [m] distance between centerline of front wheels
#define  VEHICLE_AVERAGE_TRACK   ((VEHICLE_REAR_TRACK+VEHICLE_FRONT_TRACK)/2.0)

// These constants define the /nominal/ operating limits for Alice, under
// the current design of the trajectory generators and follower to be tested.  
// All of them are units of [m/s/s]
#define  VEHICLE_MAX_ACCEL          (9.81*0.1)
#define  VEHICLE_MAX_DECEL          (3.0) //measured on flat road, with full brake. note that this will not always be achievable.
#define  VEHICLE_MAX_LATERAL_ACCEL  (9.81*0.1)

//
// Trajectory follower definitions
//
#define DFE_DELTA_SLOPE   0.0
#define DFE_DELTA_OFFSET  2.0

//
// Constant definitions for the model of Alice
// Haomiao Huang, 1/20/2005
//

// lengths from C.G. to front and rear axles, resp. [m]
// NOTE: The fraction of the wheelbase of the CG from the rear axle is GUESSED 
// to be 0.5 here.  This should be updated when we have a better estimate.
#define  L_r  (VEHICLE_WHEELBASE*1.82/3.55)
#define  L_f  (VEHICLE_WHEELBASE-L_r)

// NOTE: These constants are currently actually bob
// constants, cause we don't have anything for Alice yet

// steering lag constant
#define TAU_PHI 1.0

// Weight of vehicle in newtons
#define WEIGHT ((VEHICLE_MASS) * 9.8)

// moment of inertia in kg m^2
// this is guesstimated by taking alice as a rectangular block (4.8m long,
// 1.95 m tall, the numbers are smaller than actual cause she's empty)
// and finding the moment of inertia that way.  The formula
// approximately got a close answer for Bob's size, so I'm using this fudge
// for now. 
//#define I_VEH 4951.0
#define I_VEH 10342.0

// engine gearing and force constants
#define K_F1 57.32058
#define K_F2 2.289722
#define K_F3 (-.00368453)
#define K_F4 8.314834
#define K_F5 (-.1950403)
#define K_F6 .0001505491
#define K_T1 6.0
#define K_T2 2.0
#define FUDGE 2.0
#define LBFT2NM 1.35

#define R_TC 1.0 // torque converter
#define R_TRANS1 3.114 // transmission
#define R_TRANS2 2.216
#define R_TRANS3 1.545
#define R_TRANS4 1.000
#define R_TRANS5 0.712
#define R_DIFF 4.10 // differential
#define R_TRANSFER 1.08 // transfer case
#define R_WHEEL VEHICLE_TIRE_RADIUS // wheel radius
//#define R_WE R_TC * R_TRANS * R_DIFF * R_TRANSFER

// Idle engine speed (somewhat arbitrary)
#define W_IDLE (200 * 2 * M_PI / 60.0)

// Pacejka Tire Constants
#define TIRE_By 7.685
#define TIRE_Cy 1.1344 
#define TIRE_Ey -1.552
#define MU 1.0
// #define Dy 10675.0 
#define TIRE_Dy (WEIGHT * MU / 2.0)

// brake constants 
//#warning "This warning about H's massively fudged brake constants will be deleted by 8/10/2005 unless somebody complains loudly"
// NOTE: THESE ARE MASSIVELY FUDGED, AND ARE JUST THERE SO WE HAVE BRAKES
// OF SOME SORT


#define BF_MAX (-13645.0)
#define K_B1 5.0
#define K_B2 4.0
//
// end constants for vehicle dynamics model
//

//
// constants needed for rddfPathGen
//
#define RDDF_MAXACCEL    VEHICLE_MAX_ACCEL           // maximum forward acceleration
#define RDDF_MAXDECEL    VEHICLE_MAX_DECEL           // maximum braking acceleration
#define RDDF_MAXLATERAL  VEHICLE_MAX_LATERAL_ACCEL   // maximum sideways acceleration

// This width is subtracted from either side of the corridor to make sure the 
// paths we generate keep the entire vehicle within the corridor.
// width/2.0 plus a margin of error 
#define RDDF_VEHWIDTH_EFF  ((VEHICLE_WIDTH+0.40)/2.0) 
//
// end constants needed for RddfPathGen
//

#endif
