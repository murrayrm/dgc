/*!
 * \file TrajFollowermain.cc
 * \brief Main program for trajfollower
 *
 * \author Alex Stewart?, Laura Lindzey?
 * \date 29 April 2007
 *
 */

#include "TrajFollower.hh"

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

#include "dgcutils/DGCutils.hh"
#include "skynet/skynet.hh"

#include "cmdline.h"

//defines for switch statement involving command line options
#define HACK_STEER         5

//do we use supercon?
#define USING_SUPERCON 1

unsigned long SLEEP_TIME = 10000;    // How long in us to sleep in active fn.

int           LATERAL_FF_OFF = 0;    // Turn *OFF* the *LATERAL FEED FORWARD* term
                                     // (i.e. ignore its contribution) IF this is
                                     // SET (i.e. = 1)
int           A_HEADING_REAR = 0;    // Calculate the *ANGULAR ERROR* from the
                                     // heading of the *REAR* axle.
int           A_HEADING_FRONT = 0;   // Calculate the *ANGULAR ERROR* from the
                                     // heading of the *FRONT* axle.
int           A_YAW = 1;             // Calculate the *ANGULAR ERROR* from the
                                     // yaw
int           Y_REAR = 0;            // Calculate the *Y-ERROR* from the
                                     // position of the *REAR* axle
int           Y_FRONT = 1;           // Calculate the *Y-ERROR* from the
                                     // position of the *FRONT* axle
bool          USE_HACK_STEER = false;// whether to use the input steer command
double        HACK_STEER_COMMAND = 0;// aforementioned steer command.(normalized -1 to 1)
int           USE_DBS = 1;           // accept DBS speed caps


/* The name of this program. */
const char * program_name;
char* logNamePrefix = NULL;

int main(int argc, char **argv) 
{
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  if (cmdline.argfile_given &&
      cmdline_parser_configfile(cmdline.argfile_arg, &cmdline, 0, 0, 1) != 0)
    exit(1);

  /* Process command line arguments */
  program_name = argv[0];
  char *pTrajName = cmdline.traj_arg;
  char *pLindzey = cmdline.file_arg;
  logNamePrefix = cmdline.log_arg;
  SLEEP_TIME = cmdline.sleep_arg;
  if (cmdline.hacksteer_given) {
    HACK_STEER_COMMAND = (double) atof(optarg);
    USE_HACK_STEER = true;
  }

  /* Find the skynet key */
  int sn_key = skynet_findkey(argc, argv);

  /* Instantiate the trajectory follower object */
  TrajFollower trajFollower(sn_key, pTrajName, pLindzey, cmdline.log_level_arg, 
			    cmdline.use_local_flag, cmdline.gcdrive_flag,
			    cmdline.wait_for_state_flag);
  cerr << "Done constructing TrajFollower" << endl;

  DGCstartMemberFunctionThread(&trajFollower, &TrajFollower::roaComm);
  

  if( !cmdline.disable_console_flag )
    {
      cout<<"Starting sparrow display loops"<<endl;
      trajFollower.TrajFollower::Init_SparrowDisplayTable();
      DGCstartMemberFunctionThread(&trajFollower,&TrajFollower::SparrowDisplayLoop);      
      DGCstartMemberFunctionThread(&trajFollower,&TrajFollower::UpdateSparrowVariablesLoop);      
    }

  // if we're using a static trajectory, don't run the traj communication thread
  if(pTrajName[0] == '\0')
    DGCstartMemberFunctionThread(&trajFollower, &TrajFollower::Comm);

//#ifdef SUPERCON
//  we want supercon functionality to listen for when to drive in reverse.    This is our temporary hack.  Eventually thses things won't be called Supercon anymore, they will be normal.  I am fairly certain that the speed cap stuff is obselete.  but I should ask someone about that.  SO for now I am commenting it out

  if(USING_SUPERCON)  //USING_SUPERCON is defined above.
    {
      DGCstartMemberFunctionThread(&trajFollower, &TrajFollower::superconComm);
    }
//#endif
  trajFollower.Active();

  return 0;
} // end main()
