/*!
 * \file TrajFollower.cc 
 * \brief TrajFollower class code
 *
 * This code was written by some combination of Alex Stewart, Laura
 * Lindzey and perhaps others in 2004-05.  It needs some major work to
 * be useful for DGC07, mainly due to the different structure that we
 * use.
 */

using namespace std;

#include "TrajFollower.hh"
#include <string>
#include <interfaces/sn_types.h>
#include <skynettalker/SkynetTalker.hh>
#include "interfaces/LeadingVehicleInfo.hh"

#define STOPEDSPEED = 0.1

int QUIT_PRESSED = 0;

// These extern's relate to the '--' command line options for TrajFollower (see TrajFollowerMain.cc for set-up details)
extern int           NOSTATE;
extern int           NODISPLAY;
extern int           SIM;
extern unsigned long SLEEP_TIME;
extern int           A_HEADING_REAR;
extern int           A_HEADING_FRONT;
extern int           A_YAW;
extern int           Y_REAR;
extern int           Y_FRONT;
extern double        HACK_STEER_COMMAND;
extern bool          USE_HACK_STEER;
extern int           USE_DBS;

double g_LVvelocity = 5.0 ;
double g_LVdistance = LV_OUT_OF_RANGE;

double g_LVseparation = 10.0;
//speed below which we manually set speeds (so as to not have divide-by-zero errors)
#define MIN_SPEED 0.01

// Function for computing velocity vector in the ground plane
double Speed2(VehicleState &s) 
{
  return hypot(s.utmNorthVel, s.utmEastVel);
}

/******************************************************************************/
/******************************************************************************/
TrajFollower::TrajFollower(int sn_key, char* pTrajfile, char* pLindzey, int logLevel,
			   bool useLocal, int GCDRIVE, bool useNew, bool ffOnly, 
			   bool waitForStateFill) 
  : CSkynetContainer(SNtrajfollower, sn_key, &status_flag), CStateClient(waitForStateFill),
    m_snKey(sn_key), m_useLocal(useLocal), m_logLevel(logLevel)
{

  // getting start time
  DGCgettime(m_timeBegin);

  cout<<"Starting TrajFollower(...)"<<endl;

  DGCcreateMutex(&m_trajMutex);
  DGCcreateMutex(&m_speedCapMutex);
  DGCcreateMutex(&m_reverseMutex);
  DGCcreateMutex(&m_stopMutex);
  DGCcreateMutex(&m_roaMutex);
  DGCcreateMutex(&m_messageIDMutex);

  //initializing variables to make valgrind happy =)
  status_flag = 0;
  m_trajCount = 0;
  m_nActiveCount = 0;
  m_steer_cmd = 0.0;
  m_accel_cmd = 0.0;
  m_speedCapMax = 100.0;
  m_speedCapMin = -1.0;
  m_drivesocket = m_skynet.get_send_sock(SNdrivecmd);
  reverse = false;
  tryingtostop = false;
  logs_enabled = false;
  logs_location = "";
  logs_newDir = false;
  valid_state = true;
  old_valid_state = true;
  lindzey_logs_location = string(pLindzey);
  we_are_stopped = 0.0;
  m_roaFlag = false;
  messageID = 0;
  if (GCDRIVE == 0)
    {
      using_gcDrive = false;    // Lets us know what adrive we are using.
    } else {
      using_gcDrive = true;
    }

  //determining where we calculate errors from. default is YERR_FRONT and AERR_YAW.
  EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
  EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : ( A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));

  //creating the pidControllers
  m_pPIDcontroller = new CPID_Controller(yerrorside, aerrorside, lindzey_logs_location, false, useLocal, useNew, ffOnly);
  reversecontroller = new CPID_Controller(YERR_BACK, aerrorside, lindzey_logs_location, true, useLocal);

  cout<<"FINISHED CONSTRUCTING CONTROLLERS"<<endl;

  // 3rd order traj. If we've been passed a static path, use it
  if(pTrajfile[0] == '\0')
    {
      m_pTraj   = new CTraj(3);
      cerr<<"no static path"<<endl;
    }
  else
    {
      m_pTraj   = new CTraj(3, pTrajfile);
      //need to set path, cuz in main function only reset when new traj is received.  		
      m_pPIDcontroller->SetNewPath(m_pTraj, &m_state);
      reversecontroller->SetNewPath(m_pTraj, &m_state);
    }

  m_trajLogger = NULL;

  // Create a logger if log level > 0
  if (m_logLevel > 0) {
    m_trajLogger = new GcModuleLogger("Trajfollower");
    ostringstream oss;
    char logFileName[256];
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
    oss << timestr << "-trajfollower.log";
    snprintf( logFileName, sizeof(logFileName), "%s", oss.str().c_str() );

    m_trajLogger->addLogfile(logFileName);
    m_trajLogger->setLogLevel(m_logLevel);
  }

  if (using_gcDrive) {
    my_portHandler = new GcPortHandler();
    m_adriveCommandSF = AdriveCommand::generateSouthface(sn_key, my_portHandler, m_trajLogger);
    m_adriveCommandSF->setStaleThreshold(10);

  }
}

TrajFollower::~TrajFollower() 
{
  delete m_pTraj;
  delete m_pPIDcontroller;
  delete m_trajLogger;
  delete reversecontroller;
  if (using_gcDrive)
    AdriveCommand::releaseSouthface(m_adriveCommandSF);
  if (m_trajLogger != NULL)
    delete m_trajLogger;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollower::Active() 
{

  // timers to make sure our control frequency is what we set (not
  // affected by how long it takes to compute each cycle)
  unsigned long long timestamp1, timestamp2;
  cout<< "Starting Active Function" <<endl;

  drivecmd_t my_command;
  // init the cmd to 0 to appease the memprofiler gods
  memset(&my_command, 0, sizeof(my_command)); 
  
  AdriveDirective my_directive;
   
  UpdateState();
 
  while(!QUIT_PRESSED) 
    {
      // time checkpoint at the start of the cycle
      DGCgettime(timestamp1);

      // increment module counter
      m_nActiveCount++;

      // Get state from the simulator or astate; this sets m_state 
      UpdateActuatorState();

      //calculations for the sparrowHawk display
      DGCgettime(grr_timestamp);
      old_valid_state = valid_state;
      valid_state = UpdateState(grr_timestamp,true);
      //cout<<"called update state. valid? "<<valid_state<<endl;

      //Calculate the required vehicle speed
      m_Speed2 = Speed2(m_state);

      //Calculate angular values in degrees
      m_PitchDeg = ( ( m_state.utmPitch / M_PI ) * 180 );
      m_RollDeg = ( ( m_state.utmRoll / M_PI ) * 180 );
      m_YawDeg = ( ( m_state.utmYaw / M_PI ) * 180 );
      m_PitchRateDeg = ( ( m_state.utmPitchRate / M_PI ) * 180 );
      m_RollRateDeg = ( ( m_state.utmRollRate / M_PI ) * 180 );
      m_YawRateDeg = ( ( m_state.utmYawRate / M_PI ) * 180 );

      /** if our speed was too low, set it as if we're moving in the same direction
       * at the minimum speed WITH THE WHEELS POINTED STRAIGHT */
      if(  Speed2(m_state) < MIN_SPEED )
	{
	  m_state.utmNorthVel = MIN_SPEED * cos(m_state.utmYaw);
	  m_state.utmEastVel = MIN_SPEED * sin(m_state.utmYaw);
	}

      /* Compute the control inputs. */
      if(reverse == true) {   
	DGClockMutex(&m_trajMutex);
	at_end = reversecontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, &m_out_yerr, logs_enabled, logs_location, logs_newDir);
	DGCunlockMutex(&m_trajMutex);
	
	  if (m_logLevel > 0)
	    m_trajLogger->gclog(1) << "reversecontroller: accel cmd = " << m_accel_cmd << " steer cmd = " << m_steer_cmd << endl;
	}   
      else {
	DGClockMutex(&m_trajMutex);
	DGClockMutex(&m_speedCapMutex);
	m_pPIDcontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, &m_out_yerr, logs_enabled, logs_location, logs_newDir,m_speedCapMin,m_speedCapMax);
	DGCunlockMutex(&m_speedCapMutex);
	DGCunlockMutex(&m_trajMutex);
	if (m_logLevel > 0)
	  m_trajLogger->gclog(1) << "PIDcontroller: accel cmd = " << m_accel_cmd << " steer cmd = " << m_steer_cmd << endl;
      }

      /**apply DFE to proposed steering commands before shipping them off to adrive 
       * please note that pidController returns steering command in radians */
      //compute normalized steering commands from cmd in radians		
      double proposed_steer_Norm, steer_Norm, accel_Norm;
      proposed_steer_Norm = m_steer_cmd/VEHICLE_MAX_AVG_STEER;
      
      //data from driving in circes.
#warning "magic numbers here! need to tune this!"
      using namespace sc_interface;
#ifdef USE_DFE
      steer_Norm = DFE(proposed_steer_Norm, Speed2(m_state));
#else 
      steer_Norm = proposed_steer_Norm;
#endif

      //apply bound to the data
      steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);
      accel_Norm = fmax(-1.0, fmin(m_accel_cmd, 1.0));

      //Check m_roaFlag and activate if needed...
      DGClockMutex(&m_roaMutex);
      if(m_roaFlag == true)
	{
	  accel_Norm = -1.0;
	  if (m_logLevel > 0)
	    m_trajLogger->gclog(1) << "m_roaFlag = true" << endl;
	}
      DGCunlockMutex(&m_roaMutex);
 
      DGClockMutex(&m_stopMutex);
      if(tryingtostop == true)
	{
	  accel_Norm = -1.0;
	  if (m_logLevel > 0)
	    m_trajLogger->gclog(1) << "tryingtostop = true" << endl;
	}
      DGCunlockMutex(&m_stopMutex);

      //for data collection purposes...
      if(USE_HACK_STEER == true)
	{
	  steer_Norm = HACK_STEER_COMMAND;
	  accel_Norm = 0;
	  if (m_logLevel > 0)
	    m_trajLogger->gclog(1) << "USE_HACK_STEER = true" << endl;
	}

      if(valid_state == false) 
	{
	  cerr<<"bad state, setting steer = 0, brake = -1"<<endl;
	  steer_Norm = 0.0;
	  accel_Norm = -1.0;
	  if (m_logLevel > 0)
	    m_trajLogger->gclog(1) << "bad state, setting steer = 0, brake = -1" << endl;
	} 

      //      cout<<"computed accel command: "<<m_accel_cmd<<"accel_norm = "<<accel_Norm<<" trying to stop = "<<tryingtostop<<endl;
      if (!using_gcDrive)
	{
	  //shipping commands off to adrive
	  my_command.my_command_type = set_position;
	  my_command.my_actuator = steer;
	  my_command.number_arg = steer_Norm;
	  m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
	  if (m_logLevel > 0) {
	    m_trajLogger->gclog(8) << " -- message sent " << getTime() 
				   << "\tAdriveActuatorCommand: actuator = "
				   << my_command.my_actuator << " command = "
				   << my_command.my_command_type << " arg = "
				   << my_command.number_arg << endl;
	  }
	  my_command.my_actuator = accel;
	  my_command.number_arg = accel_Norm;
	  m_skynet.send_msg(m_drivesocket, &my_command, sizeof(my_command), 0);
	  if (m_logLevel > 0) {
	    m_trajLogger->gclog(8) << " -- message sent " << getTime() 
				   << "\tAdriveActuatorCommand: actuator = "
				   << my_command.my_actuator << " command = "
				   << my_command.my_command_type << " arg = "
				   << my_command.number_arg << endl;
	  }
	} 
      else 
	{   
	  /* here we send to gcDrive  */
	  my_directive.command = SetPosition;
	  my_directive.actuator = Steering;
	  my_directive.arg = steer_Norm;
      
	  DGClockMutex(&m_messageIDMutex);
        
	  my_directive.id = messageID;
	  messageID++;
	  m_adriveCommandSF->sendDirective(&my_directive);
	  my_portHandler->pumpPorts();
	  m_adriveCommandSF->haveNewStatus();
	  my_directive.id = messageID;
	  messageID++;

	  DGCunlockMutex(&m_messageIDMutex);

	  my_directive.actuator = Acceleration;
	  my_directive.arg = accel_Norm;
	  m_adriveCommandSF->sendDirective(&my_directive);
	  my_portHandler->pumpPorts();
	  m_adriveCommandSF->haveNewStatus();
	}

      //to be in nominal, we need to be receiving trajectories that
      //are close enough to be followed
      if(m_out_yerr < 1.5 && m_trajCount > 0) {
	status_flag = 0;
      } 
      else {
	status_flag = 10;
	if (m_logLevel > 0)
	  m_trajLogger->gclog(1) << "ERROR: m_out_yerr = " << m_out_yerr
			     << " m_trajCount = " << m_trajCount << endl;
      }

      // time checkpoint at the end of the cycle
      DGCgettime(timestamp2);

      /** now sleep for SLEEP_TIME usec less the amount of time the
       * computation took */
      int delaytime = SLEEP_TIME - (timestamp2 - timestamp1);
      if(delaytime > 0)
	usleep(delaytime);


    } // end while(!QUIT_PRESSED) 

  cout<< "Finished Active state" <<endl;
}







/**  
 *function: DFE
 *inputs: desired steering command (on a scale of -1 to 1)
 *action: compares desired steering angle to curr angle and speed, and
 *   places hard boundaries on the angle and angle's rate of change
 *   as a function of speed
 *outputs: new, bounded steering command
 **/
#warning "the DFE function in TrajFollower needs attention!"

double TrajFollower::DFE(double proposed_cmd, double speed)
{
  double max_safe_cmd, new_cmd;

  //setting boundary conditions. below 4m/s any command is safe.
  //Likewise, a cmd of .1 should be safe at any speed.
  if(proposed_cmd < 0.1 || speed < 4.0) return proposed_cmd;

  max_safe_cmd = -.05 + 25.05/pow(speed,2);

  //  cout<<"max safe, proposed: "<<max_safe_cmd<<' '<<proposed_cmd<<endl;
  if(fabs(max_safe_cmd) - fabs(proposed_cmd) < 0) 
    {
      cerr<<"DFE applied!!"<<endl;
    } 

  new_cmd =  fmax(fmin(proposed_cmd, max_safe_cmd), -max_safe_cmd);

  return new_cmd;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollower::Comm()
{
  int trajSocket;

  trajSocket = m_skynet.listen(SNtraj, SNplanner);

  
  while(!QUIT_PRESSED)
    {
      RecvTraj(trajSocket, m_pTraj);
	
      /** commenting this out as we're logging plans WAY too quickly.
       ** will make this an option soon, this is just a quick fix
       m_pTraj->print(m_outputPlans);
       m_outputPlans << endl;
       m_outputPlanStarts << m_pTraj->getNorthing(0) << ' '
       << m_pTraj->getEasting(0) << endl;
      */

      DGClockMutex(&m_trajMutex);
      if (reverse == 0) {
	m_pPIDcontroller->SetNewPath(m_pTraj, &m_state);
      }
      else if (reverse == 1) {
	reversecontroller->SetNewPath(m_pTraj, &m_state);
      }

      DGCunlockMutex(&m_trajMutex);

      m_trajCount++;
    }
}

/******************************************************************************/
/******************************************************************************/
void TrajFollower::roaComm()
{
  SkynetTalker<bool> roaTalker(m_snKey, SNroaFlag, SNtrajfollower);
  bool roaFlag = false;

  while (true) {
    bool roaFlagReceived = roaTalker.receive(&roaFlag);
    if (roaFlagReceived) {
      DGClockMutex(&m_roaMutex);
      m_roaFlag = roaFlag;
      cerr << "roaFlag = " << m_roaFlag << endl;
      DGCunlockMutex(&m_roaMutex);
    }
    usleep(100000);
  }
}


void TrajFollower::superconComm()
{
  int bytesReceived;
  int bytesToReceive;
  char* m_pDataBuffer;
  //coppied form pseudocon
  int adrivesock;
  drivecmd_t my_command;
  AdriveDirective my_directive;
  adrivesock = m_skynet.get_send_sock(SNdrivecmd);
  //end copping

  superConTrajFcmd* command;
  int reverseSocket; 
  reverseSocket = m_skynet.listen(SNsuperconTrajfCmd, MODsupercon);
  bytesToReceive = sizeof(superConTrajFcmd);
  m_pDataBuffer = new char[bytesToReceive];



  while(!QUIT_PRESSED)
    {
      bytesReceived = m_skynet.get_msg(reverseSocket, m_pDataBuffer, bytesToReceive, 0);

      command = (superConTrajFcmd*)m_pDataBuffer;

      if(bytesReceived != bytesToReceive)
	{
	  cerr << "Trajfollower::superconComm(): skynet error" << endl;
	}
  
      else //good message by this point, at least size wise...
	{
	  cerr <<"Good message by size" << endl;
	  if (((*command).commandType == tf_forwards))
            { cerr << "tf_forwards message received" << endl; }
	  else if (((*command).commandType == tf_reverse))
            { cerr << "tf_reverse message received" << endl; }
	  else {cerr << "Trajfollower::superconComm(): invalid supercon message" << endl; }
	}

      switch(tryingtostop)
	{
        case false:
          
	  if ((((*command).commandType == tf_forwards) && reverse == true) || 
	      (((*command).commandType == tf_reverse) && reverse == false))
            {
              DGClockMutex(&m_stopMutex);
              tryingtostop = true;
              DGCunlockMutex(&m_stopMutex);
              cerr <<"Mode change request now trying to stop"<<endl;
            }
	  //only other case is do nothing stay in same mode
	  break;
                    
        case true:
          //we are trying to stop  tryingtostop will be true and cause the brakes to be applied where we send out the commands
          reversecontroller->cleanPIDs();
          m_pPIDcontroller->cleanPIDs();
          we_are_stopped = fmax(m_actuatorState.m_VehicleWheelSpeed,Speed2(m_state));
          if (we_are_stopped <= 0.1)
	    {// then we are adequately stoped
	      cerr << "Speed low enough to be stopped. Consider ourselves stopped" << endl;
	      DGClockMutex(&m_stopMutex);
	      tryingtostop = false;
	      DGCunlockMutex(&m_stopMutex);
	      if (((*command).commandType == tf_forwards))
		{
		  //these lines coppied from pseudocon- they tell aDrive (or gcDrive) to shift
		  if (!using_gcDrive)
		    {
		      /* Here we are talking to the old aDrive */
		      memset(&my_command, 0, sizeof(my_command));
	              my_command.my_actuator = estop;
	              my_command.my_command_type = set_position;
	              my_command.number_arg = ESTP_PAUSE;
	              m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	              cerr << "sent estop pause" << endl;
		      if (m_logLevel > 0) {
			m_trajLogger->gclog(1) << "sent estop pause" << endl;
			m_trajLogger->gclog(8) << " -- message sent " << getTime() 
					       << "\tAdriveActuatorCommand: actuator = "
					       << my_command.my_actuator << " command = "
					       << my_command.my_command_type << " arg = "
					       << my_command.number_arg << endl;
		      }
		      sleep(1);
                  
		      memset(&my_command, 0, sizeof(my_command));
	              my_command.my_actuator = trans;
	              my_command.my_command_type = set_position;
	              my_command.number_arg = GEAR_DRIVE;
	              m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	              cerr << "sent trans --> Drive" << endl; 
		      if (m_logLevel > 0) {
			m_trajLogger->gclog(1) << "sent trans --> Drive" << endl;
			m_trajLogger->gclog(8) << " -- message sent " << getTime() 
					       << "\tAdriveActuatorCommand: actuator = "
					       << my_command.my_actuator << " command = "
					       << my_command.my_command_type << " arg = "
					       << my_command.number_arg << endl;
		      }
		      sleep(8);
                  
		      memset(&my_command, 0, sizeof(my_command));
	              my_command.my_actuator = estop;
	              my_command.my_command_type = set_position;
	              my_command.number_arg = ESTP_RUN;
	              m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	              cerr << "sent estop run" << endl;
		      if (m_logLevel > 0) {
			m_trajLogger->gclog(1) << "sent estop run" << endl;
			m_trajLogger->gclog(8) << " -- message sent " << getTime() 
					       << "\tAdriveActuatorCommand: actuator = "
					       << my_command.my_actuator << " command = "
					       << my_command.my_command_type << " arg = "
					       << my_command.number_arg << endl;
		      }
		    }
		  else
		    {
		      /* here we send to gcDrive  */
		      my_directive.command = SetPosition;
		      my_directive.actuator = Transmission;
		      my_directive.arg = GEAR_DRIVE;
                  
		      DGClockMutex(&m_messageIDMutex);
        
		      my_directive.id = messageID;
		      messageID++;
                                      
		      DGCunlockMutex(&m_messageIDMutex);
                  
		      m_adriveCommandSF->sendDirective(&my_directive);
		    }
                 
		  //end coppied code
		  DGClockMutex(&m_reverseMutex);
		  reverse = false;
		  DGCunlockMutex(&m_reverseMutex);
		  cerr << "going into forwards mode now" << endl;
		  if (m_logLevel > 0)
		    m_trajLogger->gclog(1) << "going into forwards mode now" << endl;
		}
	      else if (((*command).commandType == tf_reverse))
		{
		  if (!using_gcDrive)
		    {
		      /* Here we are talking to the old aDrive */
		      memset(&my_command, 0, sizeof(my_command));
	              my_command.my_actuator = estop;
	              my_command.my_command_type = set_position;
	              my_command.number_arg = ESTP_PAUSE;
	              m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	              cerr << "sent estop pause" << endl;
		      if (m_logLevel > 0) {
			m_trajLogger->gclog(1) << "sent estop pause" << endl;
			m_trajLogger->gclog(8) << " -- message sent " << getTime() 
					       << "\tAdriveActuatorCommand: actuator = "
					       << my_command.my_actuator << " command = "
					       << my_command.my_command_type << " arg = "
					       << my_command.number_arg << endl;
		      }
		      sleep(1);
                  
		      memset(&my_command, 0, sizeof(my_command));
	              my_command.my_actuator = trans;
	              my_command.my_command_type = set_position;
	              my_command.number_arg = GEAR_REVERSE;
	              m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	              cerr << "sent trans --> REVERSE" << endl; 
		      if (m_logLevel > 0) {
			m_trajLogger->gclog(1) << "sent trans --> REVERSE" << endl;
			m_trajLogger->gclog(8) << " -- message sent " << getTime() 
					       << "\tAdriveActuatorCommand: actuator = "
					       << my_command.my_actuator << " command = "
					       << my_command.my_command_type << " arg = "
					       << my_command.number_arg << endl;
		      }
		      sleep(8);
                  
		      memset(&my_command, 0, sizeof(my_command));
	              my_command.my_actuator = estop;
	              my_command.my_command_type = set_position;
	              my_command.number_arg = ESTP_RUN;
	              m_skynet.send_msg(adrivesock, &my_command, sizeof(my_command), 0);
	              cerr << "sent estop run" << endl;
		      if (m_logLevel > 0) {
			m_trajLogger->gclog(1) << "sent estop run" << endl;
			m_trajLogger->gclog(8) << " -- message sent " << getTime() 
					       << "\tAdriveActuatorCommand: actuator = "
					       << my_command.my_actuator << " command = "
					       << my_command.my_command_type << " arg = "
					       << my_command.number_arg << endl;
		      }
		    }
		  else
		    {
		      /* here we send to gcDrive  */
		      my_directive.command = SetPosition;
		      my_directive.actuator = Transmission;
		      my_directive.arg = GEAR_REVERSE;

		      DGClockMutex(&m_messageIDMutex);
        
		      my_directive.id = messageID;
		      messageID++;
                                      
		      DGCunlockMutex(&m_messageIDMutex);

		      m_adriveCommandSF->sendDirective(&my_directive);
		    }
                
                
		  //end coppied lines
                
                
		  DGClockMutex(&m_reverseMutex);
		  reverse = true;
		  DGCunlockMutex(&m_reverseMutex);
		  cerr << "going into reverse mode now" << endl;
		  if (m_logLevel > 0)
		    m_trajLogger->gclog(1) << "going into reverse mode now" << endl;
		}
	      else
		{
		  cerr << "Trajfollower::superconComm(): invalid supercon message" << endl;
		  if (m_logLevel > 0)
		    m_trajLogger->gclog(1) << "Trajfollower::superconComm(): invalid supercon message" << endl;
		}
	    }
	}
    }
  delete m_pDataBuffer;        
    
}
    

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
uint64_t TrajFollower::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}





