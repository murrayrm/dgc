#include "GcFollowerArbiter.hh"


trajfollowerArbiter::~trajfollowerArbiter ()
{
  //nothing yet
}

//! Tries to update each queue sequentially. If it succeeds on any one,
//! the function returns 0 for success. If it can't add to any of them, it
//! returns 1 for failure.
int trajfollowerArbiter::addMessage (tfControlMessage* NewMess)
{
  if (trajQ.tryUpdate(NewMess) == 0)
    return 0;
  if (directionQ.tryUpdate(NewMess) == 0)
    return 0;
  if (reactiveQ.tryUpdate(NewMess) == 0)
    return 0;
  if (estopQ.tryUpdate(NewMess) == 0)
    return 0;  
  return 1;
}


//! Because our current trajQ represents the last known safe path we measured,
//! we always use that to set our mergedDirective's path. However, we need
//! to figure out if we need to be stopping. This is based on our signaled
//! driving direction (FORWARD, STOP, or REVERSE) combined with our estop and
//! ROA status, each of which can override the direction into STOP mode.
//! Note: We currently don't actually get estop status. So that part's
//! commented out, since that decision may change.
void trajfollowerArbiter::mergeQueues (TrajDirective* md)
{
  if (trajQ.getTraj() != lastTraj) {
    md->setTraj(trajQ.getTraj());
    lastTraj = trajQ.getTraj();
  }

  md->setDirection(directionQ.getDirection());

  if ((estopQ.getMode() != EstopRun) || (reactiveQ.getReaction() == BRAKE) 
         || (md->getTraj() == 0))
    md->setDirection(STOP);
}

