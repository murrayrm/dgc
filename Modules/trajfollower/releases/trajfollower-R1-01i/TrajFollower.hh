#ifndef TRAJFOLLOWER_HH
#define TRAJFOLLOWER_HH

#include <pthread.h>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <iomanip>
#include <math.h>


#include "skynet/sn_msg.hh"
#include "interfaces/ActuatorCommand.h"
#include "interfaces/enumstring.h"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"
#include "trajutils/TrajTalker.hh"
#include "dgcutils/RDDF.hh"
#include "trajutils/PathLib.hh"
#include "trajutils/CPath.hh"
#include "alice/AliceConstants.h"
#include "trajF_status_struct.hh"
#include "interface_superCon_trajF.hh"
#include "PID_Controller.hh"


//The new way of talking to gcdrive
#include "gcinterfaces/AdriveCommand.hh"
#include "gcmodule/GcInterface.hh"
#include "gcmodule/GcModule.hh"


//includes from pseudocon not already in trajfollower- allows trajfollower to tell adriive to change gears
#include "interfaces/sn_types.h"
#include <stdio.h>
#include <string.h>
//pseudocon also included trajF_speedCap_cmd.hh, but we don't need that here


/***** DEFINE STATEMENTS TO CHANGE FUNCTION OF CONTROLLER ******/

// Whether to limit the steering angle based on maximum lateral acceleration
#define USE_DFE

//whether to use linear scheduling. default is inversely proportional to speed.
//#define USE_LINEAR_SCHEDULING

//These defines are for use in letting trajfollower command adrive to switch gears
//they are coppied from pseudocon
#define GEAR_DRIVE 1
#define GEAR_REVERSE -1

#define ESTP_RUN 2
#define ESTP_PAUSE 1



//estop defines required (used to check whether Alice is paused, which
//is a requirement in order to change trajFollower's mode (Forwards/Reverse)
#define ESTP_PAUSE 1
#define ESTP_RUN 2

using namespace std;
using namespace sc_interface;

class TrajFollower : public CStateClient, public CTrajTalker
{
  /*! Skynet key */
  int m_snKey;

  /*! use local coords */
  bool m_useLocal;

  /*! ROA flag */
  bool m_roaFlag;

  /*! The steer command to send to adrive (in SI units). */
  double m_steer_cmd;

  /*! The accel command to send to adrive (in SI units). */
  double m_accel_cmd;

  /* the curr yerr from pid_controller */
  double m_out_yerr;

  unsigned long long grr_timestamp;

  /*! The trajectory controller. */
  CPID_Controller* m_pPIDcontroller;
  CPID_Controller* reversecontroller;

  /*! how many times the while loop in the Active function has run.  */
  int m_nActiveCount;

  /*! The number of trajectories we have received. */
  int m_trajCount;

  /*! The current trajectory to follow. */
  CTraj* m_pTraj;

  /** status flag to be sent to gui */
  int status_flag;

  /*! Time that module started. */
  unsigned long long m_timeBegin; 

  /*! min and max speed caps */
  double m_speedCapMin, m_speedCapMax;

  /*! Speed - 2D [m/s], (for sparrowHawk display)  */
  double m_Speed2;

  bool at_end;
  //this will be set to what UpdateState returns, allowing me to check whether to send commands
  bool valid_state, old_valid_state;
  
  /*! Angular state variables in DEGREES (converted from radians) */
  double m_PitchRateDeg, m_RollRateDeg, m_YawRateDeg, m_PitchDeg, m_RollDeg, m_YawDeg;

  bool reverse; // true if we are reversing
  bool tryingtostop; //true if we are trying to stop the vehicle- triggered if our mode is asked to change.
  
  bool using_gcDrive;  //true makes us talk to gcDrive.

  GcSimplePortHandlerThread* my_portHandlerThread;  /*This will be unnnecessary after we make trajfollower a proper GCModule */
  
  /* message ID */
  unsigned messageID;

  double we_are_stopped; // similar use as in PID controller- to tell if we are stoped
 
  // vars related to timber
  bool logs_enabled;
  string logs_location;
  string lindzey_logs_location;
  bool logs_newDir;

  /*! Logs to output the plans and the state */
  ofstream m_outputPlans, m_outputTest;

  /*! The skynet socket for the communication with drive */
  int m_drivesocket;

  /* The new stuff for talking with gcdrive */
  AdriveCommand::Southface *m_adriveCommandSF;
  AdriveDirective m_adriveDirective;


  
  /*! The mutex to protect the traj we're following, and the speedCap calculations */
  pthread_mutex_t	m_trajMutex, m_speedCapMutex, m_reverseMutex, m_stopMutex, m_roaMutex, m_messageIDMutex;

  /*! The skynet socket for receiving trajectories */
  int m_trajSocket;
 
  
public:
  TrajFollower(int sn_key, char* pTrajfile, char* pLindzey, bool useLocal, int GCDRIVE, bool waitForState = true);
  ~TrajFollower();

  void Active();

  void Comm();

  void roaComm();

  void superconComm();

  void Init_SparrowDisplayTable();
  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

  double DFE(double proposed_cmd, double speed);

 
  double access_PitchRateDeg() { return m_PitchRateDeg; }
  double access_RollRateDeg() { return m_RollRateDeg; }
  double access_YawRateDeg() { return m_YawRateDeg; }
  double access_PitchDeg() { return m_PitchDeg; }
  double access_RollDeg() { return m_RollDeg; }
  double access_YawDeg() { return m_YawDeg; }


};

#endif
