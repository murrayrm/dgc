#ifndef TF_ARBITER
#define TF_ARBITER

#include "interfaces/VehicleState.h"
#include "TrajDirective.hh"
#include "TrajStatus.hh"

#include "GcFollowerQueues.hh"

class trajfollowerArbiter {
  private:
    eStopQueue estopQ;
    TrajectoryQueue trajQ;
    DrivingDirectionQueue directionQ;
    ReactiveStopQueue reactiveQ;
    CTraj* lastTraj; //!< Stores the location of the last CTraj we took in

  public:
    //! Constructor - all queues start out empty
    trajfollowerArbiter (): estopQ(), trajQ(), directionQ(), reactiveQ(), 
      lastTraj(0)  {}
    
    //! Destructor - probably does nothing
    ~trajfollowerArbiter ();

    //! Mutator - clears out the currently stored Traj from the Queue. Probably
    //! should not be used outside of testing.
    void dumpTraj () {trajQ.dump();}

    //! Mutator - clears out the currently stored Directon.
    void dumpDirection () {directionQ.dump();}

    //! Mutator - clears out the reactive state. Note: this will let us drive.
    void dumpReaction () {reactiveQ.dump();}

    //! Mutator - Tries to add a message to the appropriate Queue.
    //! 0 = success, 1 = failure.
    int addMessage (tfControlMessage* NewMess);

    //Instead of having the below thing run the interfaces, instead I should
    //just have a function that receives the listed parameters and a message
    //(and maybe not trajStatus). This will then sort the message into the
    //correct Queue, maybe after building it into a proper "Message"

// This function moved into the GcModule for convenience of interaction with
// Southfaces.
    // Mutator - updates the queues based on the result of the last 
    // control operation and the current vehicle state, in conjunction with
    // the exit and entry conditions of the messages in the queues
//    void updateQueues (TrajStatus* cs, &VehicleState m_state);

    //! Accessor - figues out what the proper directive for control will be.
    //! Assumes that Queues have just been updated.
    void mergeQueues (TrajDirective* md);
};

#endif
