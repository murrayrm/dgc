#define TABNAME trajfollower

// everything is TAB-RELATIVE. Thus input is FROM the client INTO the tab       
// fields: type, name, default value, widgetType, xLabel, yLabel, xVal, yVal
#define TABINPUTS(_) \
  _(double, Time, 0, Label, 0, 0, 1, 0)  \
  _(double, Northing, 0, Label, 0, 1, 1, 1) \
  _(double, Easting, 0, Label, 0, 2, 1, 2) \
  _(double, Altitude, 0, Label, 0, 3, 1, 3) \
  _(double, Vel_E, 0, Label, 0, 4, 1, 4) \
  _(double, Vel_N, 0, Label, 0, 5, 1, 5) \
  _(double, Vel_U, 0, Label, 0, 6, 1, 6) \
  _(double, Speed, 0, Label, 0, 7, 1, 7) \
  _(double, SteerCommand, 0, Label, 2, 0, 3, 0) \
  _(double, AccelCommand, 0, Label, 2, 1, 3, 1) \
  _(double, PitchRad, 0, Label, 2, 2, 3, 2) \
  _(double, RollRad, 0, Label, 2, 3, 3, 3) \
  _(double, YawRad, 0, Label, 2, 4, 3, 4) \
  _(double, PitchDeg, 0, Label, 4, 2, 5, 2) \
  _(double, RollDeg, 0, Label, 4, 3, 5, 3) \
  _(double, YawDeg, 0, Label, 4, 4, 5, 4) \
  _(double, PitchRateRad, 0, Label, 2, 5, 3, 5) \
  _(double, RollRateRad, 0, Label, 2, 6, 3, 6) \
  _(double, YawRateRad, 0, Label, 2, 7, 3, 7) \
  _(double, PitchRateDeg, 0, Label, 4, 5, 5, 5) \
  _(double, RollRateDeg, 0, Label, 4, 6, 5, 6) \
  _(double, YawRateDeg, 0, Label, 4, 7, 5, 7) \
  _(int, TrajCount, 0, Label, 0, 8, 1, 8) \
  _(int, RefIndex, 0, Label, 0, 9, 1, 9) \
  _(double, yError, 0, Label, 0, 10, 1, 10) \
  _(double, yErrorRef, 0, Label, 2, 10, 3, 10) \
  _(double, aError, 0, Label, 0, 11, 1, 11) \
  _(double, aErrorRef, 0, Label, 2, 11, 3, 11) \
  _(double, cError, 0, Label, 0, 12, 1, 12) \
  _(double, AccelFF, 0, Label, 0, 13, 1, 13) \
  _(double, pitchFF, 0, Label, 0, 13, 1, 13) \
  _(double, speedFF, 0, Label, 0, 13, 1, 13) \
  _(double, AccelFB, 0, Label, 0, 14, 1, 14) \
  _(double, AccelCmd, 0, Label, 0, 15, 1, 15) \
  _(double, SteerFF, 0, Label, 0, 16, 1, 16) \
  _(double, SteerFB, 0, Label, 0, 17, 1, 17) \
  _(double, SteerCmd, 0, Label, 0, 18, 1, 18) \
  _(double, LatGainP, 0, Label, 2, 13, 3, 13) \
  _(double, LatGainI, 0, Label, 2, 14, 3, 14) \
  _(double, LatGainD, 0, Label, 2, 15, 3, 15) \
  _(double, LongGainP, 0, Label, 4, 13, 5, 13) \
  _(double, LongGainI, 0, Label, 4, 14, 5, 14) \
  _(double, LongGainD, 0, Label, 4, 15, 5, 15) \
  _(double, LatErrP, 0, Label, 2, 13, 3, 13) \
  _(double, LatErrI, 0, Label, 2, 14, 3, 14) \
  _(double, LatErrD, 0, Label, 2, 15, 3, 15) \
  _(double, LongErrP, 0, Label, 4, 13, 5, 13) \
  _(double, LongErrI, 0, Label, 4, 14, 5, 14) \
  _(double, LongErrD, 0, Label, 4, 15, 5, 15) \
  _(double, phi, 0, Label, 4, 15, 5, 15) \
  _(double, phiss, 0, Label, 4, 15, 5, 15) \
  _(double, newyerr, 0, Label, 4, 15, 5, 15) \
  _(double, term1, 0, Label, 4, 15, 5, 15) \
  _(double, term2, 0, Label, 4, 15, 5, 15) \
  _(double, term3, 0, Label, 4, 15, 5, 15) \
  _(double, term4, 0, Label, 4, 15, 5, 15) \
  _(double, SteerFFRef, 0, Label, 2, 16, 3, 16) \
  _(double, VRef, 0, Label, 2, 17, 3, 17) \
  _(double, VTraj, 0, Label, 2, 18, 3, 18)

#define TABOUTPUTS(_) \
/*  _(unsigned,SAMPLE_INPUT_UNSIGNED,   0, Entry, 0,0, 1,0 ) \
  _(double  ,SAMPLE_INPUT_DOUBLE  , 1.0, Entry, 0,1, 1,1 )*/
