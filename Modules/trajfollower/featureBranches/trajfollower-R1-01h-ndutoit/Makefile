DGC=../..
include $(DGC)/global.mk

ADDLIBS=$(SPREAD) -lpthread -lncurses
ADLIBS2=$(SPREAD) -lpthread

EXECNAME=trajFollower
LIBNAME=trajFollower.a

all: $(TRAJFOLLOWER_DEPEND) $(EXECNAME) tags

tags:
	etags --declarations --defines --globals -l c++ --members *.h *.hh *.cc

stateforh: StateForH.o $(MODULEHELPERSLIB) $(SKYNETLIB)
	$(CPP) $(LDFLAGS) $^ $(ADLIBS2) -o $@

gpsCap: gpscap

gpscap: gpsCap.o $(MODULEHELPERSLIB) $(SKYNETLIB) $(RDDFLIB) $(GGISLIB)
	$(CPP) $(LDFLAGS) $^ $(ADLIBS2) -o $@

gpsCap_creatingRNDF: gpsCap_creatingRNDF.o $(MODULEHELPERSLIB) $(SKYNETLIB) $(RDDFLIB) $(GGISLIB)
	$(CPP) $(LDFLAGS) $^ $(ADLIBS2) -o $@

# Special rule for creating version of GPS using raw data
gpsCap-raw.o: gpsCap.cc 
	$(CPP) $(CPPFLAGS) -c gpsCap.cc -DRAW_GPS -o gpsCap-raw.o

gpscap-raw: gpsCap-raw.o $(MODULEHELPERSLIB) $(SKYNETLIB) $(RDDFLIB) $(GGISLIB)
	$(CPP) $(LDFLAGS)  $^ $(ADLIBS2) -o $@

gpsautocap: gpsAutoCap.o $(MODULEHELPERSLIB) $(SKYNETLIB) $(RDDFLIB) $(GGISLIB) 
	$(CPP) $(LDFLAGS) $^ $(ADLIBS2) -o $@

install: all $(LIBNAME) folders
	$(INSTALL_DATA) $(LIBNAME) $(LIBDIR)
	$(INSTALL_PROGRAM) $(EXECNAME) $(BINDIR)
	ln -sf ../modules/trajFollower/LongGains.dat $(BINDIR)/LongGains.dat
	ln -sf ../modules/trajFollower/LatGains.dat $(BINDIR)/LatGains.dat
	ln -sf ../modules/trajFollower/RevLongGains.dat $(BINDIR)/RevLongGains.dat
	ln -sf ../modules/trajFollower/RevLatGains.dat $(BINDIR)/RevLatGains.dat
	mkdir -p $(BINDIR)/logs
	cp scripts/plot.sc  $(BINDIR)/logs

$(LIBNAME): TrajFollower.o sparrow.o
	ar rs $@ $?

$(EXECNAME): TrajFollowerMain.o TrajFollower.o sparrow.o $(TRAJFOLLOWER_DEPEND_LIBS)
	$(CPP) $(LDFLAGS) $(INCLUDE) $^ $(ADDLIBS) -o $(EXECNAME) 

TrajFollower.o: TrajFollower.cc TrajFollower.hh trajFollowerTabSpecs.hh
	$(CPP) $(CPPFLAGS) -c TrajFollower.cc

vddtable.h: sparrow.dd $(CDD)
	$(CDD) -o $@ $<

sparrow.o: sparrow.cc vddtable.h trajFollowerTabSpecs.hh
	$(CPP) $(CPPFLAGS) -c sparrow.cc

TFblock.png: TFblock.fig
	fig2dev -L png TFblock.fig TFblock.png

StateForH.o: StateForH.cc StateForH.hh
	$(CPP) $(CPPFLAGS) -c $< -o $@

gpsCap.o: gpsCap.cc gpsCap.hh
	$(CPP) $(CPPFLAGS) -c $< -o $@

gpsCap_creatingRNDF.o: gpsCap_creatingRNDF.cc gpsCap.hh
	$(CPP) $(CPPFLAGS) -c $< -o $@

gpsAutoCap.o: $(RDDFLIB) $(GGISLIB) gpsAutoCap.cc gpsAutoCap.hh
	$(CPP) $(CPPFLAGS) -c gpsAutoCap.cc -o $@

clean:
	rm -f *.o *.a $(EXECNAME) gpscap gpsautocap *~ vddtable.h default.traj 
	rm -f vddtable.h \#* TAGS
