#ifndef TRAJFOLLOWERCONTROLLER_HH_1234567898578438943903
#define TRAJFOLLOWERCONTROLLER_HH_1234567898578438943903

#include <pthread.h>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <iomanip>
#include <math.h>


#include "skynet/sn_msg.hh"
#include "interfaces/ActuatorCommand.h"
#include "interfaces/enumstring.h"
#include "interfaces/ActuatorState.h"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"
#include "trajutils/TrajTalker.hh"
#include "dgcutils/RDDF.hh"
#include "trajutils/PathLib.hh"
#include "trajutils/CPath.hh"
#include "alice/AliceConstants.h"
#include "trajF_status_struct.hh"
#include "interface_superCon_trajF.hh"
#include "PID_Controller.hh"

#include "TrajDirective.hh"
#include "TrajStatus.hh"

//The new way of talking to gcdrive
#include "gcinterfaces/AdriveCommand.hh"
#include "gcmodule/GcInterface.hh"
#include "gcmodule/GcModule.hh"


//includes from pseudocon not already in trajfollower- allows trajfollower to tell adriive to change gears
#include "interfaces/sn_types.h"
#include <stdio.h>
#include <string.h>
//pseudocon also included trajF_speedCap_cmd.hh, but we don't need that here


/***** DEFINE STATEMENTS TO CHANGE FUNCTION OF CONTROLLER ******/

// Whether to limit the steering angle based on maximum lateral acceleration
#define USE_DFE

//These defines are for use in letting trajfollower command adrive to switch gears
//they are coppied from pseudocon
#define GEAR_DRIVE 1
#define GEAR_REVERSE -1


class TrajFollowerController
{
  /*! The steer command to send to adrive (in SI units). */
  double m_steer_cmd;

  /*! The accel command to send to adrive (in SI units). */
  double m_accel_cmd;

  /* the curr yerr from pid_controller */
  double m_out_yerr;

  unsigned long long grr_timestamp;

  /*! The trajectory controller. */
  CPID_Controller* m_pPIDcontroller;
  CPID_Controller* reversecontroller;

  /*! The number of trajectories we have received. */
  int m_trajCount;

  /*! The current trajectory to follow. */
  CTraj* m_pTraj;

  /** status flag to be sent to gui */
  int status_flag;

  /*! Time that module started. */
  unsigned long long m_timeBegin; 

  /*! min and max speed caps */
  double m_speedCapMin, m_speedCapMax;

  /*! Speed - 2D [m/s], (for sparrowHawk display)  */
  double m_Speed2;

  /*! true if we are reversing - needed for Sparrow display */
  bool m_reverse;

  /*! holds a copy of the latest vehicle state - needed for Sparrow display */
  VehicleState m_state;

  //! Tells if we are using global or local coordinates
  bool m_useLocal;
  
  // vars related to timber
  bool logs_enabled;
  string logs_location;
  string lindzey_logs_location;
  bool logs_newDir;

  AdriveDirective m_adriveDirective;
    
public:
  TrajFollowerController(TrajDirective* mergedDirective, char* pLindzey,
  				bool localCoords);
  ~TrajFollowerController();

  void ControllCycle(ControlStatus* controlStatus, 
  		TrajDirective* mergedDirective, double &steer_Norm, 
		double &accel_Norm, bool reverse, VehicleState &state, 
		ActuatorState m_actuatorState);

  void Init_SparrowDisplayTable();
  void SparrowDisplayLoop();
  void UpdateSparrowVariablesLoop();

  double DFE(double proposed_cmd, double speed);
 
};

#endif
