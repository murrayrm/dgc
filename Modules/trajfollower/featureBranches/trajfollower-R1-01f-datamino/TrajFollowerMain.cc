#include "TrajFollower.hh"

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

#include "dgcutils/DGCutils.hh"
#include "skynet/sn_msg.hh"

//defines for switch statement involving command line options
#define SLEEP_OPT          1
#define LOG_OPT            2
#define TRAJ_OPT           3
#define FILE_OPT           4
#define HACK_STEER         5

//do we use supercon?
#warning "this should be an external var, not placed here"
#define USING_SUPERCON 1

int           NOSTATE     = 0;       // Don't update state if = 1.
int           NODISPLAY   = 0;       // Don't start Sparrow display interface.
int           SIM  = 0;              // Don't send commands to adrive.
unsigned long SLEEP_TIME = 10000;    // How long in us to sleep in active fn.

int           LATERAL_FF_OFF = 0;    // Turn *OFF* the *LATERAL FEED FORWARD* term
                                     // (i.e. ignore its contribution) IF this is
                                     // SET (i.e. = 1)
int           A_HEADING_REAR = 0;    // Calculate the *ANGULAR ERROR* from the
                                     // heading of the *REAR* axle.
int           A_HEADING_FRONT = 0;   // Calculate the *ANGULAR ERROR* from the
                                     // heading of the *FRONT* axle.
int           A_YAW = 1;             // Calculate the *ANGULAR ERROR* from the
                                     // yaw
int           Y_REAR = 0;            // Calculate the *Y-ERROR* from the
                                     // position of the *REAR* axle
int           Y_FRONT = 1;           // Calculate the *Y-ERROR* from the
                                     // position of the *FRONT* axle
bool          USE_HACK_STEER = false;// whether to use the input steer command
double        HACK_STEER_COMMAND = 0;// aforementioned steer command.(normalized -1 to 1)
int           USE_DBS = 1;           // accept DBS speed caps
                                     

/* The name of this program. */
const char * program_name;
char* logNamePrefix = NULL;

/* Prints usage information for this program to STREAM (typically stdout
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
           "  --nostate         Continue even if state data is not received.\n"
           "  --nodisp          Don't start the Sparrow display interface.\n"
           "  --sim             Don't send commands to VDrive.\n"
           "  --sleep us        Specifies us sleep time in Active function (default 10000).\n"
           "  --traj filename   Use filename as the trajectory to follow.\n"
           "  --file filename   Use filename as where to save lindzey logs. \n"
	   "  --hacksteer cmd   Use cmd as steering command (normalized). \n"
           "  --noDBS           Don't listen to DBS speed caps\n"
           "  --help, -h        Display this message.\n" );
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  /* Set the default arguments that won't need external access here. */

  /* Temporary character. */
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
  static struct option long_options[] = 
    {
      // first: long option (--option) string
      // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
      // third: if pointer, set variable to value of fourth argument
      //        if NULL, getopt_long returns fourth argument
      {"nostate",     0, &NOSTATE,           1},
      {"nodisp",      0, &NODISPLAY,         1},
      {"sim",         0, &SIM,               1},
      {"sleep",       1, NULL,               SLEEP_OPT},
      {"traj",        1, NULL,               TRAJ_OPT},
      {"file",        1, NULL,               FILE_OPT},
      {"help",        0, NULL,               'h'},
      {"log",         1, NULL,               LOG_OPT},
      {"hacksteer",   1, NULL,               HACK_STEER},
      {"noDBS",       0, &USE_DBS,           0},
      {NULL,          0, NULL,               0}
    };


  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");

  // initialize pTrajName to a NULL string
  char pTrajName[256];
  pTrajName[0] = '\0';
  char pLindzey[256];
  pLindzey[0] = '\0';

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
    {
      switch(ch)
	{
	case TRAJ_OPT:
	  strcpy(pTrajName, optarg);
	  //	  SparrowHawk().log( "reading traj from file" );
	  cout<<"reading traj from file"<<endl;
	  break;

	case FILE_OPT:
	  strcpy(pLindzey, optarg);
	  //	  SparrowHawk().log( "got a file name!!" );
	  cout<<"got a file name!!"<<endl;
	  break;

	case LOG_OPT:
	  logNamePrefix = strdup(optarg);
	  break;

	case SLEEP_OPT:
	  SLEEP_TIME = (unsigned long) atol(optarg);
	  break;

	case HACK_STEER:
	  HACK_STEER_COMMAND = (double) atof(optarg);
	  USE_HACK_STEER = true;
	  break;
      
	case 'h':
	  /* User has requested usage information. Print it to standard
	     output, and exit with exit code zero (normal 
	     termination). */
	  print_usage(stdout, 0);

	case '?': /* The user specified an invalid option. */
	  /* Print usage information to standard error, and exit with exit
	     code one (indicating abnormal termination). */
	  print_usage(stderr, 1);

	case -1: /* Done with options. */
	  break;
	}
    }


  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
    {
      cerr << "SKYNET_KEY environment variable isn't set" << endl;
    }
  else
  sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  TrajFollower trajFollower(sn_key, pTrajName, pLindzey);
  cerr << "Done constructing TrajFollower" << endl;

  DGCstartMemberFunctionThread(&trajFollower, &TrajFollower::roaComm);

  if( !NODISPLAY )
    {
      cout<<"Starting sparrow display loops"<<endl;
      trajFollower.TrajFollower::Init_SparrowDisplayTable();
      DGCstartMemberFunctionThread(&trajFollower,&TrajFollower::SparrowDisplayLoop);      
      DGCstartMemberFunctionThread(&trajFollower,&TrajFollower::UpdateSparrowVariablesLoop);      
    }

  // if we're using a static trajectory, don't run the traj communication thread
  if(pTrajName[0] == '\0')
    DGCstartMemberFunctionThread(&trajFollower, &TrajFollower::Comm);

//#ifdef SUPERCON
//  we want supercon functionality to listen for when to drive in reverse.    This is our temporary hack.  Eventually thses things won't be called Supercon anymore, they will be normal.  I am fairly certain that the speed cap stuff is obselete.  but I should ask someone about that.  SO for now I am commenting it out

  if(USING_SUPERCON)  //USING_SUPERCON is defined above.
    {
      DGCstartMemberFunctionThread(&trajFollower, &TrajFollower::superconComm);
    //  DGCstartMemberFunctionThread(&trajFollower, &TrajFollower::speedCapComm);
    }
//#endif
  trajFollower.Active();

  return 0;
} // end main()
