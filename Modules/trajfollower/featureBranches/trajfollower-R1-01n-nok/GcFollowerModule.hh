#ifndef TRAJFOLLOWER_MODULE
#define TRAJFOLLOWER_MODULE

#include "GcFollowerController.hh"
#include "gcmodule/GcModule.hh"
#include "gcmodule/GcInterface.hh"
#include "interfaces/VehicleState.h"
#include "gcinterfaces/AdriveCommand.hh"

#include "TrajDirective.hh"
#include "TrajStatus.hh"
#include "interfaces/sn_types.h"
#include "trajutils/TrajTalker.hh"
#include "trajF_status_struct.hh"
#include "interface_superCon_trajF.hh"

#include "tfArbiter.hh"
#include <deque>
#include <pthread.h>


#include "gcinterfaces/AdriveCommand.hh"

//! Defines the maximum number of cycles a "reactive stop" can last
const int MAX_ROA_DOWNTIME = 15;

//! This is the GCmodule wrapper class that works with the trajfollower 
//! algorithm.
class trajfollowerModule : public CStateClient, public CTrajTalker, public GcModule
{
  private:
    TrajStatus tStatus;
    TrajDirective tDirective;
    trajfollowerArbiter arbiter;

    int roaDowntime;

    deque<tfControlMessage*> ReceivedQ;

    //! Keeps multiple sources from reading/writing ReceivedQ simlutaneously
    pthread_mutex_t ReceivedMutex;
    
    int skynetKey;
    
    /*! Time that module started. */
    unsigned long long m_timeBegin; 

    /* The stuff for talking with gcdrive */
    AdriveCommand::Southface *m_adriveCommandSF;
    AdriveDirective m_adriveDirective;    

    unsigned adrive_messageID;

    /*The controller object*/
    TrajFollowerController* my_tfController;

    bool reverse;
    bool tryingtostop;
    
    double we_are_stopped; // similar use as in PID controller- to tell if we are stoped
     //! Helper function - updates the state at the start of the processing loop

    //! Helper function - updates the Queues at the start of the loop
    void updateQueues (TrajStatus* cs);

  public:
    //! Constructor
    trajfollowerModule (int snKey, char* pLindsey, bool localCoords);

    //! Destructor
    ~trajfollowerModule ();

    /*! Runs the arbitrations step at the beginning of each processing cycle.
     *! This creates a merged directive from the current set of driving 
     *! direction and trajectory currently at the front of the queues. A
     *! stop command will override other commands, whether it comes from a 
     *! pause order or from a queue being empty. */
    void arbitrate (ControlStatus* controlStatus, 
    			MergedDirective* mergedDirective);

    /*! Runs the control step for each processing cycle. This part of the
     *! module is a big mystery to me. Hopefully Chris and Tom will fill in
     *! more about what happens here. It should, at high level, recieve a merged
     *! directive, look at some state, then update the ControlStatus and issue
     *! actuation directives down to Adrive. */
     void control (ControlStatus* controlStatus, 
     			MergedDirective* mergedDirective); 

    //! Handles the interface sending us our Traj
    void TrajComm ();

    //! Handles the interface sending us our Driving Direction
    void DirectionComm ();

    //! Handles the interface with Reactive Obstacle Avoidance
    void ReactiveComm ();

};

#endif
