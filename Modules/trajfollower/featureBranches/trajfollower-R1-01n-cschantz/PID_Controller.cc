/*
 *CPID_Controller: PID Lateral & Longitudinal Trajectory Follower 
 *implemented using the wrapper classes (HISTORIC:PathFollower (MTA)),
 * (HISTORIC:sn_TrajFollower (Skynet))TrajFollower (CURRENT: Skynet)
 */
#include "PID_Controller.hh"
#include "dgcutils/cfgfile.h"

double inline Speed2(VehicleState &s) 
{
  return hypot(s.utmNorthVel, s.utmEastVel);
}

using namespace std;

/**
 * FUNCTION:  Constructor for the PID_Controller class
 * ACTIONS: Initializes variables, creates the pid objects. 
 *  m_YerrorIndex indicates where (front=1, rear=0) yerror 
 * to be calculated from, and m_AerrorIndex does the same
 * for angle error.
 */
CPID_Controller::CPID_Controller(EYerrorside yerrorside, EAerrorside aerrorside, string lindzey_logs_location, bool rev, bool useLocal)
{
  reverse = rev;
  m_useLocal = useLocal;

  /** if a filename was input, log to that */
  if(lindzey_logs_location!="") 
    {
      m_outputLindzey.open(lindzey_logs_location.c_str());
      m_outputLindzey<<setprecision(20);

      char testFileName2[256];
      string temp;
      char testFilePath2[256];
      sprintf(testFileName2, "test");

      temp = lindzey_logs_location + testFileName2;
      strcpy(testFilePath2, temp.c_str());

      m_outputTest.open(testFilePath2);
    }


  /** initializing variable that indicates
   * whether we're too close to the end of the
   * trajectory to continue at our current speed
   */
  too_close_to_end = false;

  readspecs();

  /** 
   * checking if we're using the gain schedulin   
   * option. if so, setup the controllers with the 
   * proper initial gains, and if not, do it the
   * old way 
   */
  if(reverse==true)
    {
      char *LongGainRevFilenamePath = dgcFindConfigFile(LongGainRevFilename, "trajfollower");
      cout << LongGainRevFilename << endl;
      cout << LongGainRevFilenamePath << endl;
      m_pSpeedPID   = new Cpid(LongGainRevFilenamePath);
      char *LatGainRevFilenamePath = dgcFindConfigFile(LatGainRevFilename, "trajfollower");
      m_pLateralPID = new Cpid(LatGainRevFilenamePath);
    }
  else
    {

      initializeGainScheduling();

      double p,i,d;
      p = min(LAT_GAIN_CAP_P,getPgain());
      i = min(LAT_GAIN_CAP_I,getIgain());
      d = getDgain();

      cout<<" iniital gains: p,i,d: "<<p<<' '<<i<<' '<<d<<endl;

      cout << LongGainFilename << endl;
      char * LongGainFilenamePath = dgcFindConfigFile(LongGainFilename, "trajfollower");
      cout << LongGainFilenamePath << endl;
      m_pSpeedPID   = new Cpid(LongGainFilenamePath);

      double saturation;
      if(i > .01)
	{      
	  saturation = .5/i;
	} else
	  {
	    saturation = 50;
	  }
      m_pLateralPID = new Cpid(p,i,d,.2);

      cout<<"lateral pid constructed with above gains"<<endl;

    }

  /** 
   * initializes the variables that indicate where errors
   * should be calculated from 
   */
  m_YerrorIndex = (yerrorside==YERR_FRONT ? FRONT : REAR);
  m_AerrorIndex = (aerrorside==AERR_FRONT ? FRONT : REAR); 
  // NOTE that aerror==yaw does NOT get its own index
  m_AerrorYaw = aerrorside==AERR_YAW;


  /** reset all values to be stored. this makes memory 
   * profilers not complain about uninitialized data */
  m_Aerror[m_AerrorIndex] = 0.0;
  m_Yerror[m_YerrorIndex] = 0.0;
  m_Cerror                = 0.0;
  m_Verror                = 0.0;
  m_AccelFF               = 0.0;
  m_pitchFF               = 0.0;
  m_speedFF               = 0.0;
  m_AccelFB               = 0.0;
  m_accelCmd              = 0.0;
  m_SteerFF               = 0.0;
  m_SteerFB               = 0.0;
  m_steerCmd              = 0.0;
  m_trajSpeed             = 0.0;
  m_trajSpeedLookahead    = 0.0;
  m_oldSteerCmd           = 0.0;
  m_refSpeed              = 0.0;
  m_actSpeed              = 0.0;
  m_minSpeed              = NULL_MIN_SPEEDCAP;
  m_maxSpeed              = NULL_MAX_SPEEDCAP;
  trajF_current_state_Timestamp = 0.0;
  trajf_status_string[0] = '\0';
  we_are_paused           = 1;
  we_are_stopped          = 0.0;


  //for information & confirmation
  cout <<"You selected:"<<' '<<m_YerrorIndex<<"for y-error reference position"<<endl;
  cout <<"You selected:"<<' '<<m_AerrorIndex<<"for a-error reference position"<<endl;
  cout<<"0 indicates rear axle, 1 indicates front axle"<<endl;

  cout << "CPID_Controller constructor exiting" << endl;
}



/*
 *FUNCTION: destructor
 *ACTION: frees memory initialized in the two pid objects
 *INPUTS: none
 *OUTPUTS: none
 */

CPID_Controller::~CPID_Controller()
{
  delete m_pSpeedPID;
  delete m_pLateralPID;
}



/* 
 * FUNCTION: getControlVariables
 * ACTION: this is the main interface to the controller. This function is
 * called by trajFollower to get the desired accel and phi commands.  The
 * architecture it is based on can be seen in Alex's "trajfollower_flow.jpg"
 *
 * INPUTS:  current vehicle state
 * OUTPUTS: accel and phi, passed back through the provided pointers
 */

bool CPID_Controller::getControlVariables(VehicleState *pState, ActuatorState *pActuator, double *accel, double *phi, double *out_yerr, bool logs_enabled, string logs_location, bool logs_newDir, double speedCapMin, double speedCapMax)
{
  /** sets the error flag to false at beginning of every loop */
  exception_error_flag = false;

  /** If a new directory has been given to us, re-setup logs */
  if(logs_newDir) 
    {
      setup_Output(logs_location);
    }

  /** If our reference is empty (if we haven't yet gotten 
   * a reference), set 0 inputs and return */
  if(m_traj.getNumPoints() == 0)
    {
      *accel = -1.0;   //changed to negative one here because asim has us drifting all over the place slowly when no traj is loaded.
      *phi   = 0.0;
      //if we don't have a traj, by definition we are at the end of it =)
      return true;
    }

  m_minSpeed = speedCapMin;
  m_maxSpeed = speedCapMax;

  /** check if gain scheduling is supposed to be in use, 
   * and if so, update the gains */
  updateGainScheduling();

  /** compute the actual speed */
  //computing reference depends on current speed, due to lookahead terms.
  m_actSpeed = hypot(m_vehDN[FRONT], m_vehDE[FRONT]);
 
  /** compute the state of the two axles */
  compute_FrontAndRearState_veh(pState);

  /** computes the traj index to be used for error computations */
  oldIndex = m_refIndex;
  m_refIndex = getClosestIndex(LOOKAHEAD_FF_LAT_REFPOS);

  /** computes the new distance to the end of the trajectory */
  calculateDistToEnd();

  /** compute the desired state of the two axles */
  compute_FrontAndRearState_ref();

  
  /******* LATERAL CONTROL SETUP AND IMPLEMENTATION *******/
  
  
  /** Now compute our deviations from the reference state: */
  compute_YError(); // The spatial error (distance to reference point)
  compute_AError(); // The angular error (error in the angle)
  compute_CError(); // The combined error (linear combination of the y and angle errors)
  
  /** compute the commanded steering value */
  compute_SteerCmd();




  /******* LONGITUDINAL CONTROL SETUP AND IMPLEMENTATION *******/

  /** Calculate the reference speed */
  compute_refSpeed();

  /** Calculate velocity error */
  m_Verror = m_refSpeed - m_actSpeed;

  /**calculate various feedForward terms */
  compute_pitchFF(pState->utmPitch);
  compute_AccFF();

  if(fabs(m_Verror/m_refSpeed)<.25) m_pSpeedPID->reset_integral();

  /** Calculate the feed-back acceleration */
  m_AccelFB = m_pSpeedPID->step_forward(m_Verror);

  /** add FF and FB to get command */
  m_accelCmd = m_AccelFB + m_pitchFF;

  /** takes care of the known dead bands in throttle and brake actuators */
  if(m_accelCmd > 0)
    m_accelCmd = m_accelCmd + THROTTLE_DEAD_BAND;
  else if(m_accelCmd < 0)
    m_accelCmd = m_accelCmd - BRAKE_DEAD_BAND;

  
  /** TESTS FOR EXCEPTIONS - AND HANDLING OF ANY EXCEPTIONS FOUND **/

  /**
   * SPARROW STATUS FIELD TRY/(THROW) - used to test for exceptions that 
   * require that the STATUS field in the sparrow display is updated to
   * reflect that the exception has occurred
   */
  try {

    /**checking if we're getting too close to the end of the trajectory. 
     * if we're withing the minimum stopping distance, slam on the brakes 
     * if we were previously too close and haven't received a new traj, we
     * should also stop */    
#warning "this is a magic number, and needs to be fixed. Also, this number needs to be tested (calculated from braking distances)"
    stopping_dist = m_actSpeed*m_actSpeed*.1344;
    if(dist_to_end <= stopping_dist || too_close_to_end)
      //    if(dist_to_end <= 3 || too_close_to_end)
      {
	/** too close to end of curr traj. will be reset when receive a new one */
	too_close_to_end = true; 

	//we need to stomp on the brakes
	m_accelCmd = ERROR_ACCEL_CMD;

	/** Register than an exception has occurred and update the status string */
	conditionalStringThrow( true, "Too close to end of trj STOPPING");
      }


#warning "need to work out with dima and alex the exact conditions here, as this is redundant. send Joel an email about this one, for IPT intervention"
    /** Since the planner never commands zero speed, if it's asking for a vel
     * below MIN_TRACKABLE_SPEED and decelerating, we set vref to 0, and if its
     * accelerating, set vref to MIN_TRACKABLE_SPEED.  (this is done above, w/ no exception thrown*/
    
    /** IF the TrajFollower is sent a reference speed command with NEGATIVE ACCELERATION,
     * where'very small' is defined as being < MAX_SPEED_TAKEN_TO_BE_STATIONARY
     * (this is a defined variable in the header file), then the TrajFollower
     * will assume that it is being commanded to STOP (or remain stationary)
     * but due to complications with dealing with ZERO SPEED, when the planner
     * wants to STOP, it often commmands VERY SMALL SPEEDS.  In this situation
     * the trajfollower will set the accel command to ERROR_ACCEL_CMD
     * (defined in the header file) - this should bring Alice to an abrupt STOP
     */
    if(m_refSpeed < MAX_SPEED_TAKEN_TO_BE_STATIONARY && m_AccelFF <= 0.0) {
      m_accelCmd = ERROR_ACCEL_CMD;

      /** register than an exception has occurred and update the status string */
      conditionalStringThrow( true, "Trj spd<zero val &decel STOPPING");
          
    }

  } // END of SPARROW STATUS FIELD TRY

  /** 
   * SPARROW STATUS FIELD CATCH - used to catch all strings that need to be
   * displayed in the STATUS field of the sparrow display AFTER AN EXCEPTION
   * HAS OCCURRED
   */
  catch (std::string caught_status_string) {

    int caught_string_length;

    /** Determine the length of the caught string */
    caught_string_length = caught_status_string.length();
    
    if ( caught_string_length < MAX_SPARROW_DISPLAY_STATUS_STRING_LENGTH ) {
    
      /** string is short enough to display, so copy it to sparrow display */
      strcpy ( trajf_status_string, caught_status_string.c_str() );

    } else {
    
      strcpy ( trajf_status_string, "exception msg string too long!" );    
      cout<<trajf_status_string<<endl;
    }

  } //END of catch

  
  /**
   * NOTE - the exception_error_flag is updated to false at the start of the active loop 
   * NOTE - the conditionalStringThrow(...) method updates the exception_error_flag 
   */
  if ( exception_error_flag == false ) {

    strcpy ( trajf_status_string, "Everything is awesome! :)");

  } 




  /******* SETTING THE OUTPUT VARIABLE POINTERS *******/
  *accel = m_accelCmd;
  *phi   = m_steerCmd;
  *out_yerr = m_trajYerr;

  /** takes care of logging variables and outputting debug messages */
  output_Vars(logs_enabled, pState, pActuator);

  we_are_paused = pActuator->m_estoppos;
#warning "we need a better way of telling if we are actually stopped"
  we_are_stopped = fmax(pActuator->m_VehicleWheelSpeed,Speed2(*pState));


  /** if we're paused, we want to reset the integral and derivative errors 
   * because they become irrelevant. (steer commands are still passed through
   * in pause, so we only want to reset if we're also stopped 
   * */
#warning "check with Tully to check this number! bad magic numbers here"
  if(we_are_stopped <= 0.25 && we_are_paused!=2) 
    {
      //cout<<"We're paused/disabled. Resetting controller"<<endl;
      m_pLateralPID->reset();
      m_pSpeedPID->reset();
      *accel = 0.0;
      *phi   = 0.0;
      return false;
    }

  return too_close_to_end;
}





/**
 *FUNCTION: CleanPIDs
 *ACTIN: the method by which TrajFollower can wipe the Integral and derivative errors of the PID controller objects on change of mode (forward to revrse, etc).
 *inputs: nothing
 *outputs: none; it changes internal variables in CPID_Controller
 */
void CPID_Controller::cleanPIDs()
{
  m_pLateralPID->reset();
  m_pSpeedPID->reset();
}





/**
 *FUNCTION: SetNewPath
 *ACTIN: the method by which PathFollower can inform CPID_Controller of new plans.
 *inputs: pointer to new path
 *outputs: none; it changes internal variables in CPID_Controller
 */
void CPID_Controller::SetNewPath(CTraj* pTraj, VehicleState *state)
{

  /** Set the member variable */
  m_traj = *pTraj;

  /** 
   * reset variable that keeps track of whether
   * we are too close to the end of the path to 
   * continue at our current speed
   */
  too_close_to_end = false;

  /** making valgrind happy by calling this before getclosestindex */
  compute_FrontAndRearState_veh(state);

  /** receiving a new path resets the distance to the end of traj 
   * but first, gotta calculate closest point (cuz until we have 
   * this value, the initializing function doesnt know where to 
   * start from */
  m_refIndex = getClosestIndex(LOOKAHEAD_FF_LAT_REFPOS);

  initializeDistToEnd();

}


/** 
 *Function: initializeDistToEnd
 *Action: computes the distance in meters from current 
 * traj point to end of trajectory. Somewhat crude
 * calculation, as it uses everybody's favorite theorem,
 * thus assuming no curvature to the path. This won't 
 * matter because traj points are fairly closesly spaced, 
 * and this distance will only matter in some failure state.
 *Inputs: none (accesses global variables m_traj and 
 * m_refIndex
 *Outputs: none (changes global variable dist_to_end
 */
void CPID_Controller::initializeDistToEnd()
{
  int curr_point = m_refIndex;
  int total_points = m_traj.getNumPoints();
  double total_dist = 0;
  double temp_dist, n_diff, e_diff;
  for(int i = curr_point; i < total_points - 1; i++)
    {
      n_diff = m_traj.getNorthingDiff(i, 0) - m_traj.getNorthingDiff(i+1, 0);
      e_diff = m_traj.getEastingDiff(i, 0) - m_traj.getEastingDiff(i+1, 0);

      temp_dist = hypot(n_diff, e_diff);    
      total_dist += temp_dist;
    }

  dist_to_end = total_dist;

}

/** Function: calculateDistToEnd
 * Action: calculates how much we've traveled since the 
 *   last call of calculateDistToEnd, and subtract that
 *   from the total distance
 * Inputs: none (uses oldIndex, m_refIndexand m_traj)
 * Outputs: none (changes dist_to_end)
 */
void CPID_Controller::calculateDistToEnd() 
{
  int curr_point = oldIndex;
  int total_points = m_refIndex;
  double total_dist = 0;
  double temp_dist, n_diff, e_diff;
  for(int i = curr_point; i < total_points; i++)
    {
      n_diff = m_traj.getNorthingDiff(i, 0) - m_traj.getNorthingDiff(i+1, 0);
      e_diff = m_traj.getEastingDiff(i, 0) - m_traj.getEastingDiff(i+1, 0);
      temp_dist = hypot(n_diff,e_diff);
      total_dist += temp_dist;
    }

  dist_to_end = dist_to_end - total_dist;

}



/*
 *Function: getClosestIndex
 *Action: computes the closest index on the reference trajectory
 *   to the point on the vehicle specified by 'frac'
 *Inputs: double frac. where on vehicle to compute from
 *Outputs: integer index to trajectory
 */

int CPID_Controller::getClosestIndex(double frac)
{

  NEcoord look;
  NEcoord rear(m_vehN[REAR], m_vehE[REAR]);
  NEcoord front(m_vehN[FRONT], m_vehE[FRONT]);

  /** Calculate the point that is "frac" distance from 
   * the rear axle to the front axle location.*/

  look = rear * (1.0 - frac) + front * frac;
 
  return m_traj.getClosestPoint(look.N, look.E);
}




/*! 
 *Function: compute_FrontAndRearState_veh
 *Action: uses the state struct to initialize member variables containing
 *  information about both axles
 *Inputs: state of vehicle
 *Outputs: none
 */

void CPID_Controller::compute_FrontAndRearState_veh(VehicleState* pState)
{
  /** get the various elements of state for the two axles. 
   *If we're moving very slowly, assume that the wheels are straight */

  if (m_useLocal) {
    m_vehN      [REAR] = pState->localX;
    m_vehE      [REAR] = pState->localY;
  }
  else {
    m_vehN      [REAR] = pState->utmNorthing;
    m_vehE      [REAR] = pState->utmEasting;
  }
  
  m_vehDN     [REAR] = pState->utmNorthVel;
  m_vehDE     [REAR] = pState->utmEastVel;
  m_vehAngle  [REAR] = m_AerrorYaw ? pState->utmYaw :
    (Speed2(*pState) > MINSPEED ? atan2(m_vehDE[REAR], m_vehDN[REAR]) : pState->utmYaw);

  /*
   * Note: this is the same code as was used in trunk.  However, the
   * position returned in the state struct is actually the rear of the
   * vehicle, so this code is (and presumably has been) wrong.
   */
  if (m_useLocal) {
    m_vehN      [FRONT] = pState->localX;
    m_vehE      [FRONT] = pState->localY;
  }
  else {
    m_vehN      [FRONT] = pState->utmNorthing;
    m_vehE      [FRONT] = pState->utmEasting;
  }
  m_vehDN     [FRONT] = pState->utmNorthVel;
  m_vehDE     [FRONT] = pState->utmEastVel;
  m_vehAngle[FRONT] = (Speed2(*pState) > MINSPEED ?
                       atan2(m_vehDE[FRONT], m_vehDN[FRONT]) : pState->utmYaw);

  m_vehYaw            = pState->utmYaw;
  m_yawdot            = pState->utmYawRate;
 
  trajF_current_state_Timestamp = pState->timestamp;
}


/*! 
 *Function: compute_FrontAndRearState_ref
 *Action: This function computes the pertinent information about the reference state
 *   of both axles into the data structures in the class. reference state is 
 *   LATERAL_TIME_LOOKAHEAD ahead of current position
 *Inputs: none
 *Outputs: none (initializes variables)
 */
void CPID_Controller::compute_FrontAndRearState_ref()
{
  m_trajN = m_traj.getNorthingDiff(m_refIndex, 0);
  m_trajE = m_traj.getEastingDiff(m_refIndex, 0);
  m_trajDE = m_traj.getNorthingDiff(m_refIndex, 1);
  m_trajDN =  m_traj.getEastingDiff(m_refIndex, 1);

  m_trajSpeed = hypot(m_trajDE, m_trajDN);

  //the state computed by this is only used by the lateral controller, so we'll use that lookahead term here
  int steerLookahead = m_traj.getPointAhead(m_refIndex,LATERAL_TIME_LOOKAHEAD*m_actSpeed);

  m_refN      [REAR] = m_traj.getNorthingDiff(steerLookahead, 0);
  m_refE      [REAR] = m_traj.getEastingDiff (steerLookahead, 0);
  m_refDN     [REAR] = m_traj.getNorthingDiff(steerLookahead, 1);
  m_refDE     [REAR] = m_traj.getEastingDiff (steerLookahead, 1);
  m_refDDN_REAR      = m_traj.getNorthingDiff(steerLookahead, 2);
  m_refDDE_REAR      = m_traj.getEastingDiff (steerLookahead, 2);
  m_refAngle[REAR] = atan2(m_refDE[REAR], m_refDN[REAR]);

  m_refN      [FRONT] = m_refN[REAR] + VEHICLE_WHEELBASE*cos(m_refAngle[REAR]);
  m_refE      [FRONT] = m_refE[REAR] + VEHICLE_WHEELBASE*sin(m_refAngle[REAR]);

  double speedRear = hypot(m_refDN[REAR], m_refDE[REAR]);

  m_refDN     [FRONT] = m_refDN[REAR] - VEHICLE_WHEELBASE*sin(m_refAngle[REAR]) * (m_refDN[REAR]*m_refDDE_REAR - m_refDDN_REAR*m_refDE[REAR]) / speedRear/speedRear;
  m_refDE     [FRONT] = m_refDE[REAR] + VEHICLE_WHEELBASE*cos(m_refAngle[REAR]) * (m_refDN[REAR]*m_refDDE_REAR - m_refDDN_REAR*m_refDE[REAR]) / speedRear/speedRear;
  m_refAngle[FRONT]   = atan2(m_refDE[FRONT], m_refDN[FRONT]);

}

/*
 *Function: compute_AccFF
 *Action: computes desired acceleration based on the current trajectory
 *Inputs: none
 *Outputs: none (changes member variable)
 */
void CPID_Controller::compute_AccFF() 
{
  /** this is now used to test cases, but not in actual calculation of control */
  m_AccelFF = (m_refDN[REAR]*m_refDDN_REAR + m_refDE[REAR]*m_refDDE_REAR) 
    / hypot(m_refDN[REAR], m_refDE[REAR]);

}


/**
 * Function: compute_pitchFF
 * Action: computes necessary additions to the accel command based upon force upon 
 *     vehicle due to gravity. Note that we don't take the dead band into account here
 * Inputs: current vehicle pitch
 * Outputs: none. changes m_PitchFF
 */
void CPID_Controller::compute_pitchFF(double pitch)
{

  double force = 2*sin(pitch);
  double cmd;
  if(force > 0) 
    {
      cmd = force/1.55;
    } else if(force == 0) {
      cmd = 0;
    }
  else {
    cmd = force/2.00;
  }
  m_pitchFF = cmd;
}

/**
 *Function: compute_SteerCmd
 *Action: computes steering command (in radians) by individually
 *   calculating FF and FB, then adding
 *Inputs: none
 *Outputs: none (changes internal variable)
 **/

void CPID_Controller::compute_SteerCmd()
{
  /** calculate the feed-forward (lateral) phi term. */
  compute_SteerFF();

  /** calculate the feedback (lateral) phi term.
   * If we're close enough to the path, reset the integral */
  if(fabs(m_Yerror[FRONT]) < 0.1) {
    m_pLateralPID->reset_integral();
  }
  m_SteerFB = m_pLateralPID->step_forward(m_Cerror);


  /** calculate the steering command output */
  if(reverse==true)
    {
      m_steerCmd = m_SteerFB - m_SteerFF;
    }
  else
    {
      m_steerCmd = m_SteerFB + m_SteerFF;
    }


  /* Check the commanded steering angle is < +/- saturation, 
   * if not saturate steering command */
  //note that PID_Controller deals with throttle/brake in absolute setting, and steercmd in radians
  m_steerCmd = fmax(fmin(m_steerCmd, VEHICLE_MAX_RIGHT_STEER), -VEHICLE_MAX_LEFT_STEER);

  /** stores command for future reference */
  m_oldSteerCmd = m_steerCmd;

}



/**
 *function: compute_SteerFF
 *action: This feed-forward term uses the first and second order 
 *   northing & easting derivatives included in the traj file to 
 *   determine the nominal steering angle that would be required
 *   to follow the path - (calculations based upon the closest 
 *   point in the traj file to the vehicles current position).  
 *   The nominal steering angle is calculated based upon
 *   the nominal angle (commanded by the point in the traj file) 
 *   using the REAR AXLE centred model, i.e. this uses function 
 *   uses the CURRENT REAR TRAJ-POINT for the spatialn variables & 
 *   their derivatives used. This is important, and CANNOT be 
 *   changed to the FRONT axle system, as in the equations for the 
 *   FRONT axled model, there is an additional UNKNOWN term which is 
 *   very tricky to evaluate, and hence the Planner uses the REAR 
 *   axled model and the Controller MUST be exactly consistent with 
 *   the planner for lateral feed-forwared, as it uses the second 
 *   derivatives (whose values are dependent upon the model used)
 *input: none
 *output: none (changes internal var for feed-forward steering angle (radians)
 */

void CPID_Controller::compute_SteerFF()
{

  double speedRef = hypot(m_refDN[REAR], m_refDE[REAR]);
  
  m_SteerFF = atan(VEHICLE_WHEELBASE *
                   (m_refDN[REAR]*m_refDDE_REAR - m_refDDN_REAR*m_refDE[REAR])
                   / pow(speedRef, 3.0));
}


/**
 *function: compute_YError
 *action: Function to calculate the y-error for any given position of Alice, y-error (m) is
 *defined as the PERPENDICULAR distance of Alice's reference point from the CURRENT 
 *path-segment (this is a straight line segment drawn between the CURRENT and NEXT traj-points)
 */
void CPID_Controller::compute_YError()
{
  // the deviations in northing and easting
  double errN, errE;

  if(reverse==true)
    {
      errN = m_refN[FRONT] - m_vehN[FRONT];
      errE = m_refE[FRONT] - m_vehE[FRONT];
      m_Yerror[FRONT] = -(-sin(m_refAngle[FRONT])*errN +
			  cos(m_refAngle[FRONT])*errE);
      
      errN = m_refN[REAR] - m_vehN[REAR];
      errE = m_refE[REAR] - m_vehE[REAR];
      m_Yerror[REAR]  = -(-sin(m_refAngle[REAR])*errN +
			  cos(m_refAngle[REAR])*errE);
    }
  else
    {
      errN = m_refN[FRONT] - m_vehN[FRONT];
      errE = m_refE[FRONT] - m_vehE[FRONT];
      m_Yerror[FRONT] = (-sin(m_refAngle[FRONT])*errN +
			 cos(m_refAngle[FRONT])*errE);
      
      errN = m_refN[REAR] - m_vehN[REAR];
      errE = m_refE[REAR] - m_vehE[REAR];
      m_Yerror[REAR]  = (-sin(m_refAngle[REAR])*errN +
			 cos(m_refAngle[REAR])*errE);

      errN = m_trajN - m_vehN[REAR];
      errE = m_trajE - m_vehE[REAR];
      m_trajYerr = (-sin(m_refAngle[REAR])*errN +
		    cos(m_refAngle[REAR])*errE);
    }
}



/**
 *Function: compute_Aerror
 *Action: This function calculates the angle error, defined as 
 *    [DESIRED angle (rad) - ACTUAL angle (rad)], where the 
 *    DESIRED angle is calculated from the first derivatives 
 *    of northing & easting at the CURRENT traj-point, and the 
 *    ACTUAL angle is taken as the YAW stored in astate.
 *    Note that it uses the N/E convention, as it uses the *GLOBAL 
 *    reference frame (NOT the vehicle reference frame)
 *Inputs: none
 *Output: angle in radians returned as type double
 **/
void CPID_Controller::compute_AError() 
{
  if(reverse)
    {
      m_Aerror[FRONT] = -(m_refAngle[FRONT] - m_vehAngle[FRONT] - M_PI);
      m_Aerror[REAR]  = -(m_refAngle[REAR]  - m_vehAngle[REAR] - M_PI);
    }
  else
    {
      m_Aerror[FRONT] = m_refAngle[FRONT] - m_vehAngle[FRONT];
      m_Aerror[REAR]  = m_refAngle[REAR]  - m_vehAngle[REAR];
    }

  //Constrain the value of h_error to be within the range -pi -> pi    
  m_Aerror[FRONT] = atan2(sin(m_Aerror[FRONT]), cos(m_Aerror[FRONT]));
  m_Aerror[REAR]  = atan2(sin(m_Aerror[REAR]),  cos(m_Aerror[REAR]));
}




/**
 *Function: compute_Cerror
 *Action: Function to calculate the combination error (m_Cerror) from the y_error and h_error
 *   using the values of alpha & beta (gains) specified in the header file associated
 *   with this source file.  Alpha represents the differential weighting between the
 *   y-error and h-error in the combination error equation, and Beta is used to magnify
 *   the h-error so that it has a magnitude approximately equal to the y-error for 
 *   'equal' badness
 *
 *    Uses y-error_tilda, which is y-error after consideration of the perpendicular
 *    theta distance, this is the y-error, greater than which the
 *    controller commands a phi such that Alice heads directly
 *    towards the path (angle perpendicular to that of the path)
 *    y_error_tilda is m_Yerror, saturated so that 
 *    -PERP_THETA_DIST <= y_error_tilda <= PERP_THETA_DIST
 *
 *Input: y-error and h-error values output from compute_YError and compute_Aerror
 *Output: perpendicular distance from the traj-line, in meters
 */

void CPID_Controller::compute_CError()
{
  double y_error_tilda;

  double selectedYError = m_Yerror[m_YerrorIndex];
  double selectedAerror = m_Aerror[m_AerrorIndex];

  y_error_tilda  = fmax(fmin(selectedYError, PERP_THETA_DIST), -PERP_THETA_DIST);
  if(reverse)
    {
#warning "magic numbers!!!"
      m_Cerror = ((.6*y_error_tilda) + ((1.0-.6)*8.49*selectedAerror));//BETA_GAIN = 8.49, ALPHA_GAIN = .4
    }
  else
    {
      m_Cerror = ((ALPHA_GAIN*y_error_tilda) + ((1.0-ALPHA_GAIN)*BETA_GAIN*selectedAerror));
    }
}





/*
 *Function: compute_trajSpeed
 *Action: calculates the speed that the trajectory actually
 *   indicates that we should be going, based on Edot and Ndot.
 *   please note that this uses a lookahead term to improve tracking.
 */

void CPID_Controller::compute_trajSpeed() 
{
  //taking the desired velocity to be the sqrt of E' and N' squared
  //due to an observed delay in velocity tracking, we are trying a lookahead term

  int speedLookahead = m_traj.getPointAhead(m_refIndex,LONGITUDINAL_TIME_LOOKAHEAD*m_actSpeed);
  double dElookahead = m_traj.getEastingDiff(speedLookahead,1);
  double dNlookahead = m_traj.getNorthingDiff(speedLookahead,1);
  m_trajSpeedLookahead = hypot(dElookahead, dNlookahead);
 
}



/*
 *Function: compute_refSpeed
 *Action: calculates the reference speed that we will actually control
 *    around. First, if the speed is really slow, we use accelerations
 *    to decode planner intentions (depending on accel/decel we are 
 *    stopping/starting). Next, we check for speedCap conditions.

*/

void CPID_Controller::compute_refSpeed() 
{


  /**Retrieve the desired speed (this is where the lookahead occurs)
   * it will change m_trajSpeedLookahead. m_trajSpeed is now computed
   * in compute_frontandreadstate_ref   */
  compute_trajSpeed();
  m_refSpeed = m_trajSpeedLookahead;

#ifdef SUPERCON
  /** this now checks if were meant to be speeding up/slowing down
   * This should eliminate the problem of taking forever to get up to speed */
  if(m_trajSpeedLookahead < MINIMUM_TRACKABLE_SPEED && m_AccelFF > 0) {
    m_refSpeed = MINIMUM_TRACKABLE_SPEED;
  }
  else if(m_trajSpeedLookahead < MINIMUM_TRACKABLE_SPEED && m_AccelFF < 0){
    m_refSpeed = 0.0;
  }
  else {
    m_refSpeed = m_trajSpeedLookahead;
  }

  /** this performs the speedCap bounding */
  if(m_maxSpeed < NULL_MAX_SPEEDCAP && m_maxSpeed < m_refSpeed)
    {
      m_refSpeed = m_maxSpeed;
    }

  if(m_minSpeed > NULL_MIN_SPEEDCAP && m_minSpeed > m_refSpeed)
    {
      m_refSpeed = m_minSpeed;
    }
#endif
  return;
}



/******** CODE FOR GAIN SCHEDULING ******/

void CPID_Controller::initializeGainScheduling()
{
  /** trajFollower should never be started excep
   * when Alice is stationary, so it's ok to 
   * initialize to 0.0 */
  scheduling_region = 0.0;
  return;
}

void CPID_Controller::updateGainScheduling()
{
  if(changeGains())
    {
      double p,i,d;
      p = min(LAT_GAIN_CAP_P,getPgain());
      i = min(LAT_GAIN_CAP_I,getIgain());
      d = getDgain();

      bumplessTransfer(p,i,d);

      //cout<<"gains after running updateGainScheduling"<<endl;

      m_pLateralPID->set_Kp(p);
      m_pLateralPID->set_Ki(i);
      m_pLateralPID->set_Kd(d);
    }

}


/** this function resets the integral error to prevent the transfer
 * between different gain scheduling regions causing an abrupt
 * jump in the control signal */
void CPID_Controller::bumplessTransfer(double p, double i, double d)
{

  double oldP,oldI,oldD,errP,errD,errI,diff,newI;
  oldP = m_pLateralPID->get_Kp();
  oldI = m_pLateralPID->get_Ki();
  oldD = m_pLateralPID->get_Kd();
  errP = m_pLateralPID->getP();
  errI = m_pLateralPID->getI();
  errD = m_pLateralPID->getD();

  diff = (oldP-p)*errP + (oldI)*errI + (oldD-d)*errD;
  if(i!=0)
    newI = diff/i;
  else
    newI = 0;

  m_pLateralPID->setI(newI);

}

bool CPID_Controller::changeGains()
{
  double max = scheduling_region + REGION_WIDTH + REGION_OVERLAP;
  double min = scheduling_region - REGION_OVERLAP;

  if(m_actSpeed < min | m_actSpeed > max)
    { 
      scheduling_region = REGION_WIDTH * floor(m_actSpeed/REGION_WIDTH);
      return true;
    } else {
      return false;
    }

}

/** 
 * calculates and returns the proportional gain
 * using the assumption that gains should be 
 * inversely proportional to speed
 */
double CPID_Controller::getPgain()
{
  double tempGainP;
  /** since the regions start at 0, want to 
   * control based on the median velocity. */
  //cout<<"from getPgain: "<<PGAIN<<" "<<STANDARD_SPEED<<" "<<REGION_WIDTH<<endl;


#ifdef USE_LINEAR_SCHEDULING

  /** now, for linearly proportional. max will be double the input gain, slope will be xxx for now */
  tempGainP = 2 * PGAIN  -  PGAIN * (scheduling_region + (.5 * REGION_WIDTH)) / STANDARD_SPEED;
#else

  /** This one is if i do inversely proportional to speed */
  tempGainP = (PGAIN * STANDARD_SPEED) / (scheduling_region + (.5 * REGION_WIDTH));
#endif

  return tempGainP;
}

/** 
 * calculates and returns the integral gain
 * using the assumption that gains should be 
 * inversely proportional to speed
 */
double CPID_Controller::getIgain()
{
  double tempGainI;
  /** since the regions start at 0, want to 
   * control based on the median velocity. */

#ifdef USE_LINEAR_SCHEDULING
  /** This one is for linearly proportional */
  tempGainI = 2 * IGAIN  -  IGAIN * (scheduling_region + (.5 * REGION_WIDTH)) / STANDARD_SPEED;

#else

  /** This one is for inversely proportional. */
  tempGainI = (IGAIN * STANDARD_SPEED) / (scheduling_region + .5 * REGION_WIDTH);

#endif

  return tempGainI;
}

/** 
 * calculates and returns the derivative gain
 * using the assumption that gains should be 
 * inversely proportional to speed
 */
double CPID_Controller::getDgain()
{
  double tempGainD;
  /** since the regions start at 0, want to 
   * control based on the median velocity. */


#ifdef USE_LINEAR_SCHEDULING 
  /** this one is for linearly proportional */
  tempGainD = 2 * DGAIN  -  DGAIN * (scheduling_region + (.5 * REGION_WIDTH)) / STANDARD_SPEED;

#else

  /** This one if for inversely proportional */
  tempGainD = (DGAIN * STANDARD_SPEED) / (scheduling_region + .5 * REGION_WIDTH);

#endif

  return tempGainD;
}
 

/************* END CODE FOR GAIN SCHEDULING *******/


/*
 *Function: output_Vars
 *Action: condenses the printing of internal variables to one
 *    place, to make the code more readable. since each function
 *    tends to be called once, linearly, this works just fine.
 */
void CPID_Controller::output_Vars(bool logs_enabled, VehicleState* m_state, ActuatorState* m_actuator)
{
  double p,i,d,pv,iv,dv; 
  p=m_pLateralPID->get_Kp();
  i=m_pLateralPID->get_Ki();
  d=m_pLateralPID->get_Kd();

  pv=m_pLateralPID->getP();
  iv=m_pLateralPID->getI();
  dv=m_pLateralPID->getD();

  if(logs_enabled) {
    //  cout<<"trying to output to m_outputLogs"<<endl;
    m_outputLogs <<
      /**internal variables */
      m_refN[REAR]<<' '<<
      m_refE[REAR]<<' '<<
      m_Yerror[FRONT]<<' '<<
      m_Aerror[REAR] <<' '<<
      m_Cerror<<' '<<
      m_SteerFF<<' '<<
      m_SteerFB<<' '<<
      p<<' '<<
      i<<' '<<
      d<<' '<<
      m_refSpeed<<' '<<
      m_actSpeed<<' '<<
      m_Verror<<' '<<
      m_steerCmd<<' '<<
      m_accelCmd<<' ';

    /** the state varaibles ... */
    if (m_useLocal) {
      m_outputLogs << m_state->localX << ' ' <<
	m_state->localY << ' ';
    }
    else {
      m_outputLogs << m_state->utmNorthing << ' ' <<
	m_state->utmEasting << ' ';
    }

    m_outputLogs << 
      m_state->utmAltitude << ' ' <<
      m_state->utmNorthVel << ' ' <<
      m_state->utmEastVel << ' ' <<
      m_state->utmAltitudeVel << ' ' <<
      m_state->utmRoll << ' ' <<
      m_state->utmPitch << ' ' <<
      m_state->utmYaw << ' ' <<
      m_state->utmRollRate << ' ' <<
      m_state->utmPitchRate << ' ' <<
      m_state->utmYawRate << ' ' <<

      /** and now for the adrive variables **/

      m_state->timestamp << ' ' <<
      m_actuator->m_steerstatus << ' ' <<
      m_actuator->m_steerpos << ' ' <<
      m_actuator->m_steercmd << ' ' <<
      m_actuator->m_gasstatus << ' ' << 
      m_actuator->m_gaspos << ' ' << 
      m_actuator->m_gascmd << ' ' <<
      m_actuator->m_brakestatus << ' ' << 
      m_actuator->m_brakepos << ' ' << 
      m_actuator->m_brakecmd << ' ' << 
      m_actuator->m_brakepressure <<' ' <<
      m_actuator->m_estopstatus << ' ' << 
      m_actuator->m_estoppos << ' ' <<
      m_actuator->m_transstatus << ' ' << 
      m_actuator->m_transpos << ' ' << 
      m_actuator->m_transcmd << ' ' <<
      m_actuator->m_obdiistatus << ' ' << 
      m_actuator->m_engineRPM << ' ' << 
      m_actuator->m_TimeSinceEngineStart << ' ' << 
      m_actuator->m_VehicleWheelSpeed <<' ' << 
      m_actuator->m_EngineCoolantTemp << ' ' << 
      m_actuator->m_WheelForce << ' ' << 
      m_actuator->m_GlowPlugLampTime << ' ' << 
      m_actuator->m_CurrentGearRatio << ' ' <<

      /** current traj point **/

      m_refIndex << ' ' <<
      m_traj.getEastingDiff(m_refIndex,0)<<' '<<
      m_traj.getEastingDiff(m_refIndex,1) << ' ' <<
      m_traj.getEastingDiff(m_refIndex,2)<< ' '<<
      m_traj.getNorthingDiff(m_refIndex,0)<< ' ' <<
      m_traj.getNorthingDiff(m_refIndex,1)<< ' ' <<
      m_traj.getNorthingDiff(m_refIndex,2)<< ' ' <<

      /** current values of pid (not gains, but value in the pid util) **/

      pv<<' '<<
      iv<<' '<<
      dv<<endl;
  }

  if(m_outputLindzey.is_open()) {

    /** outputs in format (all coordinates are rear-axle)
	ref N
	ref E
	yerr
	aerr
	cerr
	steering ff
	steering fb
	p
	i
	d
	vehicle speed
	ref speed
	error speed
	accel Cmd
	steer Cmd
	full state struct
	full actuator struct 

    */

    m_outputTest<<m_trajN<< ' ' <<m_trajE<<' '<<m_trajDE<<' '<<m_trajDN<<' '<<m_trajYerr<<endl;

    m_outputLindzey << 

      /**internal variables */

      m_trajN<<' '<<
      m_trajE<<' '<<
      m_trajYerr <<' '<<
      m_Aerror[REAR] <<' '<<
      m_Cerror<<' ' <<
      m_SteerFF<<' '<<
      m_SteerFB<<' ' <<
      p<<' '<<
      i<<' '<<
      d<<' '<<
      m_trajSpeed<<' '<<
      m_actSpeed<<' '<<
      m_Verror<<' '<<
      m_steerCmd<<' '<<
      m_accelCmd<<' ';

    /** the state varaibles ... */

    if (m_useLocal) {
      m_outputLindzey << 
	m_state->localX << ' ' <<
	m_state->localY << ' ';
    }
    else {
      m_outputLindzey <<
	m_state->utmNorthing << ' ' <<
	m_state->utmEasting << ' ';
    }
    m_outputLindzey <<
      m_state->utmAltitude << ' ' <<
      m_state->utmNorthVel << ' ' <<
      m_state->utmEastVel << ' ' <<
      m_state->utmAltitudeVel << ' ' <<
      m_state->Acc_N_deprecated<< ' ' <<
      m_state->Acc_E_deprecated<< ' ' <<
      m_state->Acc_D_deprecated<< ' ' <<
      m_state->utmRoll << ' ' <<
      m_state->utmPitch << ' ' <<
      m_state->utmYaw << ' ' <<
      m_state->utmRollRate << ' ' <<
      m_state->utmPitchRate << ' ' <<
      m_state->utmYawRate << ' ' << 

      /** and now for the adrive variables **/

      m_state->timestamp << ' '<<
      m_actuator->m_steerstatus << ' ' << 
      m_actuator->m_steerpos << ' ' << 
      m_actuator->m_steercmd << ' ' <<
      m_actuator->m_gasstatus << ' ' << 
      m_actuator->m_gaspos << ' ' << 
      m_actuator->m_gascmd << ' ' <<
      m_actuator->m_brakestatus << ' ' << 
      m_actuator->m_brakepos << ' ' << 
      m_actuator->m_brakecmd << ' ' << 
      m_actuator->m_brakepressure <<' ' <<
      m_actuator->m_estopstatus << ' ' << 
      m_actuator->m_estoppos << ' ' <<
      m_actuator->m_transstatus << ' ' << 
      m_actuator->m_transpos << ' ' << 
      m_actuator->m_transcmd << ' ' <<
      m_actuator->m_obdiistatus << ' ' << 
      m_actuator->m_engineRPM << ' ' << 
      m_actuator->m_TimeSinceEngineStart << ' ' << 
      m_actuator->m_VehicleWheelSpeed <<' ' <<
      m_actuator->m_EngineCoolantTemp << ' ' << 
      m_actuator->m_WheelForce << ' ' << 
      m_actuator->m_GlowPlugLampTime << ' ' << 
      m_actuator->m_CurrentGearRatio << ' ' << 
    
      /** current traj point **/

      m_refIndex << ' ' <<
      m_traj.getEastingDiff(m_refIndex,0)<<' '<<
      m_traj.getEastingDiff(m_refIndex,1) << ' ' << 
      m_traj.getEastingDiff(m_refIndex,2)<< ' '<<
      m_traj.getNorthingDiff(m_refIndex,0)<< ' ' <<
      m_traj.getNorthingDiff(m_refIndex,1)<< ' ' <<
      m_traj.getNorthingDiff(m_refIndex,2)<< ' ' << 

      /**more ff terms **/
      m_pitchFF << ' ' << 
      m_speedFF << endl;
  }

}


/*
 *THROW exception method for STRINGS 
 */
void CPID_Controller::conditionalStringThrow( const bool exception_condition, const string string_to_throw ) {

  if ( exception_condition == true ) {

    //Update the exception_error_flag to TRUE to indicate that an
    //exception has occurred
    exception_error_flag = true;

    //The exception condition was TRUE - hence an exception has occurred
    //therefore hrow the string passed in the method (called string_to_throw)
    throw std::string(string_to_throw);
    
    //REMEMBER - NO CODE AFTER THE THROW WILL BE EXCECUTED - EVER!

  }

}



/*
 *Function: setup_Output
 *Action: initializes all the streams necessary to log data
 */

void CPID_Controller::setup_Output(string logs_location)
{
  //logs_location = getLogDir();

  time_t t = time(NULL);
  tm *local;
  local = localtime(&t);


  char logsFileName[FILE_NAME_LENGTH];
  char testFileName[FILE_NAME_LENGTH];

  char logsFilePath[FILE_NAME_LENGTH];
  char testFilePath[FILE_NAME_LENGTH];


  char lnCmd[FILE_NAME_LENGTH];

  //this creates the output file names as a function of time called  
  sprintf(logsFileName, "logs.dat");




  string temp;

  temp = logs_location + logsFileName;
  strcpy(logsFilePath, temp.c_str());

  temp = logs_location + testFileName;
  strcpy(testFilePath, temp.c_str());

  /** Closes the streams if necessary */

  if(m_outputLogs.is_open()) 
    {
      m_outputLogs.close();
    }
  
  if(m_outputTest.is_open()) 
    {
      m_outputTest.close();
    }


  /** opens the files so that functions can write to them */
  m_outputLogs.open(logsFilePath);


  // make state.dat, angle.dat,yerror.dat,waypoint.dat link to newet files
  //I'm not sure how/why it works, but I copied it from Dima, and it seems to work perfectly
  lnCmd[0] = '\0';
  strcat(lnCmd, "cd logs; ln -fs ");
  strcat(lnCmd, testFileName);
  strcat(lnCmd, " _test; cd ..");
  system(lnCmd);

  m_outputTest <<setprecision(20);
  m_outputLogs << setprecision(20);
}
