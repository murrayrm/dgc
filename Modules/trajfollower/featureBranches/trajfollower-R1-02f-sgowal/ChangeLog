Sun Jun 10 10:47:21 2007	Chris Schantz (cschantz)

	* version R1-02f
	BUGS:  
	FILES: PID_Controller.hh(27667)
	Just a few changes to make velocity traking more sensible and
	smooth.

Sat Jun  9 21:21:58 2007	Laura Lindzey (lindzey)

	* version R1-02e
	BUGS:  
	New files: newsparrow.dd
	FILES: GcFollowerController.hh(27571), GcFollowerModule.cc(27571),
		Makefile.yam(27571), PID_Controller.cc(27571),
		PID_Controller.hh(27571), gcsparrow.cc(27571),
		trajFollowerTabSpecs.hh(27571)
	merging new sparrow display changes with Daniele's makefile change

	New files: newsparrow.dd
	FILES: GcFollowerController.hh(27564), GcFollowerModule.cc(27564),
		Makefile.yam(27564), PID_Controller.cc(27564),
		PID_Controller.hh(27564), gcsparrow.cc(27564),
		trajFollowerTabSpecs.hh(27564)
	If --use-new flag is specified, a different sparrow display is
	used, which has the relevant variables. Probably still needs some
	changes/different variables, but committing to Chris can take a
	look

	FILES: Makefile.yam(27558), gcsparrow.cc(27558)
	fixing Makefile to actually compile sparrow w/o trajfollower

Sat Jun  9 19:27:48 2007	datamino (datamino)

	* version R1-02d
	BUGS:  
	FILES: Makefile.yam(27545)
	Fixed Makefile.yam, so gcfollower will compile (vddtable.h wasn't
	generated anymore since trajfollower compilation is disabled by
	default).

Sat Jun  9 14:10:47 2007	Chris Schantz (cschantz)

	* version R1-02c
	BUGS: 3311 3145 
	FILES: Makefile.yam(27482), PID_Controller.cc(27482),
		gcsparrow.cc(27482)
	Lots of bug fixes, hopefully no bunny hop anymore, and it *should*
	not have crazy steering at the end of a traj anymore either.  made
	the makefile not compile the old trajfollower by default.  removed
	some extrenious stuff from the sparrow display- will be rewriting
	this soon to show the new controller's innards.

Fri Jun  8 21:56:01 2007	Chris Schantz (cschantz)

	* version R1-02b
	BUGS:  
	FILES: PID_Controller.cc(27313), PID_Controller.hh(27313)
	These changes improved the logging so that the --file filename
	option will now log the internal variables of the new controller if
	that is the controller being used.  When the --use-new flag is on,
	the logger won't record the internal variables of the speed
	controller anymore- I will fix this soon.  Also I cleaned up the
	code a bit and stoped calling unnecessary functions when the
	--use-new flag is in place- this should make the inner loop
	marginally faster.

Fri Jun  8 13:24:20 2007	Stefano Di Cairano (stefano)

	* version R1-02a
	BUGS:  
	FILES: GcFollowerMain.cc(27235), GcFollowerModule.cc(27235),
		GcFollowerModule.hh(27235), PID_Controller.cc(27235),
		PID_Controller.hh(27235), TrajFollower.cc(27235),
		trajfollower.ggo(27235)
	more tuning on the velocity reduction.

	FILES: GcFollowerMain.cc(27102), GcFollowerModule.cc(27102),
		GcFollowerModule.hh(27102), PID_Controller.cc(27102),
		PID_Controller.hh(27102), trajfollower.ggo(27102)
	modified trajfollower for queuing. Added a flag --use-lead to strat
	the lead vehicle information reception and usage. 

	FILES: GcFollowerModule.cc(26673), GcFollowerModule.hh(26673),
		PID_Controller.cc(26673), PID_Controller.hh(26673)
	Added functions and variables to use leading vehicle information
	for queuing and following

	FILES: GcFollowerModule.cc(26683), PID_Controller.cc(26683),
		TrajFollower.cc(26683)
	tested velocity reference governor without any other car. In this
	case it never acts (as we want).

	FILES: PID_Controller.cc(27190)
	Tuned velocity reduction for following to be smoother

	FILES: TrajFollower.cc(27118)
	added global vars into trajFollower

Tue Jun  5 18:47:26 2007	Chris Schantz (cschantz)

	* version R1-02
	BUGS:  
	FILES: GcFollowerMain.cc(26686), GcFollowerModule.cc(26686)
	These changes remove some old safety code that checked on our
	velocity before ordering a transmission shift.	The velocity it was
	checking relied on a noisy estimate- and the noise is what caused
	it to take so long to shift during U-turns.  The old code was made
	unnesecary by gcdrive which does the safety checking using the
	correct speed from the wheel sensor.  We shift a whole lot faster
	now.

Tue Jun  5 16:52:51 2007	Chris Schantz (cschantz)

	* version R1-01z
	BUGS:  
	FILES: PID_Controller.cc(26661)
	synching wiht latest module version

	FILES: PID_Controller.cc(26654)
	Better alpha and beta gains for reversing, reads correct traj point
	now.

Tue Jun  5 16:11:45 2007	Laura Lindzey (lindzey)

	* version R1-01y
	BUGS:  
	FILES: GcFollowerController.cc(26639), PID_Controller.cc(26639)
	adding reverse to --use-new option

Mon Jun  4 22:39:52 2007	 (ahoward)

	* version R1-01x
	BUGS:  
	FILES: Makefile.yam(26439)
	Minor build tweak

Mon Jun  4 13:52:48 2007	Laura Lindzey (lindzey)

	* version R1-01w
	BUGS:  
	FILES: PID_Controller.cc(26341), TrajFollower.cc(26341),
		TrajFollower.hh(26341), TrajFollowerMain.cc(26341),
		trajfollower.ggo(26341)
	* removing non-functional nodisp cmdline argument to trajfollower *
	tryingtostop now initializes to false * --use-new and --ff-only now
	work w/ trajfollower, in the same was as with gcfollower. Note,
	that these options only apply when going forwards.

Fri Jun  1 16:54:47 2007	Joshua Feingold (joshuaf)

	* version R1-01v
	BUGS:  
	FILES: DegenerateTFQueue.hh(26062), GcFollowerMessages.hh(26062),
		GcFollowerModule.cc(26062), GcFollowerQueues.hh(26062)
	Hopefully, this will concurrently fix the segfault and memory leak
	issues we continue to have, despite me best efforts. This undoes
	the bulk of dynamic allocation which could have been causing the
	problem.

	FILES: GcFollowerMain.cc(26065)
	Contains the last bits needed to fix the memory issues.

Tue May 29 18:04:10 2007	Joshua Feingold (joshuaf)

	* version R1-01u
	BUGS:  
	FILES: DegenerateTFQueue.hh(25348), GcFollowerMessages.hh(25348),
		GcFollowerModule.cc(25348), GcFollowerQueues.hh(25348),
		Message.hh(25348)
	This should fix the seg-fault problems we've been having with
	gcfollower, since I think I actually found the root of the problem
	this time: I'm dumb.

	FILES: GcFollowerController.hh(25408), GcFollowerModule.cc(25408),
		GcFollowerModule.hh(25408), TrajDirective.hh(25408)
	I removed one instance of copying a CTraj, and now everything works
	like it should. Fortunately, this copy was not necessary for seg
	fault protection; it was just added late last night when I was
	trying to prevent memory errors. Also, the logs generated by the
	--logictrack option are easier to read and contain more
	information. The sparrow display now correctly updates the
	TrajCount, instead of staying 0 forever.

	FILES: GcFollowerMain.cc(25406), GcFollowerMessages.hh(25406),
		GcFollowerModule.cc(25406), TrajDirective.hh(25406)
	For some reason the previous modifications were causing the vehicle
	not to move. I'm not sure why. Right now, the vehicle will start
	moving and then stop a little while later. At least I haven't seen
	any seg-faults. I think this error is probably due to increased
	copying of CTrajs, since this is the major change between the old
	version and this one. However, I have no idea why that extra
	copying would cause a problem.

Mon May 28 18:30:56 2007	Laura Lindzey (lindzey)

	* version R1-01t
	BUGS:  
	FILES: GcFollowerController.cc(25287),
		GcFollowerController.hh(25287), GcFollowerMain.cc(25287),
		GcFollowerModule.cc(25287), GcFollowerModule.hh(25287),
		PID_Controller.cc(25287), PID_Controller.hh(25287),
		trajfollower.ggo(25287)
	trying, yet again to make commit work


Mon May 28 14:41:32 2007	Laura Lindzey (lindzey)

	* version R1-01s
	BUGS:  
	FILES: PID_Controller.cc(25260)
	Changing the stopping logic	*now requres that we're actually at
	the last traj point, not w/in some distane of it    *now requires
	identically zero velocity, rather than arbitrarily small velocity 
	Fixing a line in the old controller that I accidentally broke with
	my last release  Now, --use-new correctly handles steering
	feed-forward when velocity is zero

Sun May 27  8:18:06 2007	Laura Lindzey (lindzey)

	* version R1-01r
	BUGS:  

Tue May 22 22:07:29 2007	Joshua Feingold (joshuaf)

	* version R1-01q
	BUGS:  
	New files: GcFollowerArbiter.cc GcFollowerArbiter.hh
		GcFollowerMessages.hh GcFollowerQueues.hh
	Deleted files: TrajQueues.hh tfArbiter.cc tfArbiter.hh
		tfMessages.hh
	FILES: DegenerateTFQueue.hh(24466), GCTrajUnitTest.cc(24466),
		GcFollowerController.cc(24466), GcFollowerModule.hh(24466),
		Makefile.yam(24466)
	Changed some names to make naming more consistent among
	gcfollower's files. Also made a change to eliminate an unnecessary
	big memcopy that we were doing each control cycle. (We now only
	load the CTraj into the PID controller we are actually going to be
	using for this control cycle, instead of both as we did before.)

	FILES: GcFollowerModule.cc(24512), GcFollowerModule.hh(24512),
		TrajDirective.hh(24512)
	This should fix the segfault problem gcfollower has been
	experiencing. We now actually copy the Traj we are currently
	following instead of just passing a pointer. This is what I meant
	to do all along, but it turns out I'm dumb. Also, I think this
	might include the update to have correct logic to handle estop
	commands. The estop functionality should be removed from the PID
	Controller, but has not been yet.

Sun May 20 14:14:43 2007	murray (murray)

	* version R1-01p
	BUGS:  
	New files: UT_fwdbwd_adrive+oldtf.py UT_fwdbwd_sinewave.py
	FILES: TrajFollower.cc(24026)
	formatting tweaks

	New files: UT_fwdbwd_adrive+oldtf.py UT_fwdbwd_sinewave.py
	FILES: TrajFollower.cc(24026)
	formatting tweaks

Sat May 19 11:53:58 2007	murray (murray)

	* version R1-01o
	BUGS:  
	Deleted files: Makefile gcsparrow.dd
	FILES: PID_Controller.hh(23965), gcsparrow.cc(23965),
		sparrow.cc(23965), sparrow.dd(23965)
	added Northing, Easting to display

Wed May 16 21:32:22 2007	Joshua Feingold (joshuaf)

	* version R1-01n
	BUGS:  
	Deleted files: launcher.sh
	FILES: GcFollowerController.cc(23344), GcFollowerModule.cc(23344)
	removed high volume cerr messages to prevent them from bogging down
	the sparrow display. Should improve performance and hopefully
	prevent intermitant locking we experienced from gcfollower. Note:
	This revision did not remove such messages from trajfollower, only
	gcfollower.

Sat May 12 10:51:07 2007	murray (murray)

	* version R1-01m
	BUGS:  
	New files: UT_sinewave.traj
	FILES: GcFollowerController.cc(22828), TrajFollowerMain.cc(22828),
		pid.hh(22828)
	fixed documentation errors

	FILES: TrajFollower.hh(22826), TrajFollowerMain.cc(22826),
		trajfollower.ggo(22826)
	added disable-trans flag

	FILES: GcFollowerController.cc(22828), TrajFollowerMain.cc(22828),
		pid.hh(22828)
	fixed documentation errors

	FILES: TrajFollower.hh(22826), TrajFollowerMain.cc(22826),
		trajfollower.ggo(22826)
	added disable-trans flag

2007-05-12  Richard Murray  <murray@kona.local>

	* TrajFollowerMain.cc (main): added --disable-trans flag that puts
	causes trajfollower to run without gear changing commands being sent

Wed May  9 18:25:24 2007	Chris Schantz (cschantz)

	* version R1-01l
	BUGS:  
	New files: launcher.sh
	FILES: GcFollowerController.cc(22598),
		GcFollowerController.hh(22598), GcFollowerMain.cc(22598),
		GcFollowerModule.cc(22598), GcFollowerModule.hh(22598),
		TrajStatus.hh(22598), gcsparrow.cc(22598)
	Includes modifications to gcfollower that let it log its own errors
	and run in a local coordinate frame.

	FILES: GcFollowerMain.cc(22611), TrajDirective.hh(22611)
	added nok's startup script to the folder. Doesn't work for this
	yet, but is convenient to modify for testing. Also added logic
	logging command line functionality that records what directives are
	being sent into control. Would probably be able to merge these logs
	with our error logs. Should be easy. Will probably be in my next
	commit.

	FILES: GcFollowerModule.cc(22612)
	some changes josh made- don't know what they were, also commented
	out sopme cerr out lines that were filling up our screen too much

Thu May  3 17:23:46 2007	Joshua Feingold (joshuaf)

	* version R1-01k
	BUGS:  
	New files: DegenerateTFQueue.hh GCTrajUnitTest.cc
		GcFollowerController.cc GcFollowerController.hh
		GcFollowerMain.cc GcFollowerModule.cc GcFollowerModule.hh
		Message.hh TrajDirective.hh TrajHelper.hh TrajQueues.hh
		TrajStatus.hh gcsparrow.cc gcsparrow.dd tfArbiter.cc
		tfArbiter.hh tfMessages.hh
	FILES: Makefile.yam(22026)
	Added all the files we need to run gcfollower.

Thu May  3 14:56:04 2007	Nok Wongpiromsarn (nok)

	* version R1-01j
	BUGS:  
	FILES: PID_Controller.cc(21997), TrajFollower.cc(21997),
		TrajFollower.hh(21997), TrajFollowerMain.cc(21997),
		trajfollower.ggo(21997)
	Added GcModule logging

	FILES: TrajFollower.cc(21946)
	Removed the cerr

	FILES: TrajFollower.cc(21960), TrajFollower.hh(21960)
	Added pumpPorts

Wed May  2 11:34:49 2007	murray (murray)

	* version R1-01i
	BUGS:  
	FILES: PID_Controller.cc(21892)
	got rid of supercon code to reset reference speed

Sun Apr 29 19:45:38 2007	Nok Wongpiromsarn (nok)

	* version R1-01h
	BUGS:  
	FILES: Makefile.yam(21636), TrajFollower.cc(21636),
		TrajFollower.hh(21636), TrajFollowerMain.cc(21636),
		trajfollower.ggo(21636)
	Added command line option for running with gcdrive. (Merged with
	Chris's changes.)

Sun Apr 29 17:14:16 2007	Nok Wongpiromsarn (nok)

	* version R1-01g
	BUGS:  
	New files: trajfollower.ggo
	FILES: Makefile.yam(21575), TrajFollowerMain.cc(21575)
	added command line parsing via gengetopt

	FILES: PID_Controller.cc(21576), PID_Controller.hh(21576),
		TrajFollower.cc(21576), TrajFollower.hh(21576)
	Added argument for using local coordinates

	FILES: PID_Controller.cc(21579), sparrow.cc(21579)
	if-else statement for using local coords

	FILES: TrajFollowerMain.cc(21577)
	--use-local and --wait-for-state options

	FILES: TrajFollowerMain.cc(21580)
	converted command line options to gengetopt

	FILES: sparrow.cc(21594), sparrow.dd(21594)
	Fixed the sparrow display

Tue Apr 24 14:52:58 2007	Noel duToit (ndutoit)

	* version R1-01f
	BUGS:  
	FILES: Makefile.yam(20503), PID_Controller.cc(20503),
		PID_Controller.hh(20503), TrajFollower.cc(20503),
		TrajFollower.hh(20503), sparrow.cc(20503),
		sparrow.dd(20503)
	made the code leaner. roa flag now works.

Sun Apr 22 15:48:24 2007	Ghyrn Loveness (ghyrn)

	* version R1-01e
	BUGS:  
	FILES: Makefile.yam(20193), TrajFollower.cc(20193),
		TrajFollower.hh(20193), TrajFollowerMain.cc(20193)
	added skynet talker to listen for ROA flag from RoaLadarPerceptor

Sun Mar 18 20:23:13 2007	murray (murray)

	* version R1-01d
	BUGS: 
	FILES: TrajFollower.cc(18562)
	added sleeps in changing gears + error messages

	FILES: sparrow.cc(18561)
	added sparrow error log

2007-03-18  murray  <murray@gcfield.dgc.caltech.edu>

	* TrajFollower.cc (superconComm): Changed some cout's to cerr's
	* sparrow.cc (SparrowDisplayLoop): added sparrow error logging.

	* TrajFollower.cc (superconComm): added some sleep statements to
	the reverse logic, so that we wait for adrive to do its thing.

Sat Mar 17  1:21:14 2007	Chris Schantz (cschantz)

	* version R1-01c
	BUGS: 
	FILES: TrajFollower.cc(18119), TrajFollower.hh(18119)
	Now Trajfollower send the Estop Pause and Resume commands that
	Adrive needs to hear in order to shift.

Thu Mar 15 13:33:25 2007	Noel duToit (ndutoit)

	* version R1-01b
	BUGS: 
	FILES: PID_Controller.hh(17759), TrajFollower.cc(17759)
	fixed a little mode switching big

Mon Mar 12 15:12:34 2007	Chris Schantz (cschantz)

	* version R1-01a
	BUGS: 
	FILES: PID_Controller.cc(17191), PID_Controller.hh(17191),
		TrajFollower.cc(17191), TrajFollower.hh(17191)
	Added a simple finite state machine to trajfollower to manaje mode
	switching. It now has a stop mode it goes into when its asked to
	change modes and it will only leave this stop mode if it determines
	that its come to a stop (ie if its velocity is below .15 m/s-- a
	magic number that could be discussed, especially with the new
	applanix stfuu avaliaboe- we ought to know much better when we are
	stoped.)  Still lacking- telling adrive to change gears, but the
	code is ripe and the spot is known where I can add this if needed. 
	Also another change- when the PID_Controller object determins we
	have no trajectory to follow it puts on the brakes.  I did this
	because asmi was having us drift around aimlessley while
	trajfollower waited to hear its next trajectory.   

	FILES: TrajFollower.cc(17237), TrajFollower.hh(17237)
	THis new version has the functionality to let trajfollower tell
	adrive to change gears after checking that we are stopped (i.e.
	below a certain velocity- currently .15 m/s, but this is just a
	magic number and should be revised)

Wed Mar  7 22:21:29 2007	Chris Schantz (cschantz)

	* version R1-00e
	BUGS: 
	New files: interface_superCon_trajF.hh
	FILES: PID_Controller.cc(16770), PID_Controller.hh(16770),
		TrajFollower.cc(16770), TrajFollower.hh(16770),
		TrajFollowerMain.cc(16770), pid.cc(16770),
		sparrow.cc(16770)
	Implementation of reverse driving abilities in TrajFollower. 
	Actually a more accurate description is that I just made it simpler
	and more straightforward.  Most of my modifications were to
	resurect the supercon functionality to let TrajFollower use its old
	reversing capabilities, and I commented out a tone of code to get
	rid of extraneous past history recording and what not.	Now Driving
	backwards mirrors driving forwards.

Sun Feb 25  8:22:55 2007	murray (murray)

	* version R1-00d
	BUGS: 
	New files: trajF_speedCap_cmd.hh trajF_status_struct.hh
	FILES: Makefile.yam(15669)
	added files used by supercon for pseudocon testing

2007-02-25  murray  <murray@gclab.dgc.caltech.edu>

	* Makefile.yam: added deprecated supercon include files for
	testing with pseudocon

Tue Feb 20 17:31:20 2007	Nok Wongpiromsarn (nok)

	* version R1-00c
	BUGS: 
	FILES: Makefile.yam(15226), TrajFollower.cc(15226),
		TrajFollower.hh(15226)
	Fixed trajfollower so it compiles against revision R2-00a of
	interfaces and skynet

Thu Feb  1  2:16:46 2007	Nok Wongpiromsarn (nok)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(13883), PID_Controller.cc(13883),
		TrajFollower.cc(13883), sparrow.cc(13883),
		sparrow.dd(13883)
	Fixed the linking problems and changed the variable names for the
	new VehicleState

Wed Jan 31 13:33:28 2007	murray (murray)

	* version R1-00a
	BUGS: 
	New files: LatGains.dat LongGains.dat Makefile PID_Controller.cc
		PID_Controller.hh RevLatGains.dat RevLongGains.dat
		TabStruct.h TrajFollower.cc TrajFollower.hh
		TrajFollowerMain.cc pid.cc pid.hh sparrow.cc sparrow.dd
		tf_specs.cc tf_specs.h tf_specsfile trajFollowerTabSpecs.hh
	FILES: Makefile.yam(13601)
	first cut at trajfollower

	FILES: Makefile.yam(13672)
	updates for YaM

2007-01-30  murray  <murray@gclab.dgc.caltech.edu>

	* Copied over LatGains.dat, LongGains.dat, RevLatGains.dat,
	RevLongGains.dat from trunk.

	* TrajFollower.cc: commented out superCon code
	* TrajFollowerMain.cc: commented out superCon code

	* PID_Controller.cc: modified code that tries to compute the state
	of the rear of the vehicle to just grab the data from the state
	struct.  This is essentially equivalent to the previous behavior
	since the state has been represented at the rear wheels for a
	while.  See comments (and warnings) in code for more info.
	* PID_Controller.cc: got rid of prints of deprecated accelerations

	* TrajFollower.cc (Speed2): created new inline function; used to
	be part of VehicleState class (now a struct)

	* PID_Controller.cc, PID_Controller.hh: copied from trunk/utils
	* pid.cc, pid.hh: copies from trunk/utils

Sun Jan 28 17:08:09 2007	murray (murray)

	* version R1-00
	Created trajfollower module.













































