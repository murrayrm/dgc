/*!
 * \file GcFollowerController.cc 
 * \brief TrajFollower Controller class code
 *
 * This code was written by some combination of Alex Stewart, Laura
 * Lindzey and perhaps others in 2004-05.  It needs some major work to
 * be useful for DGC07, mainly due to the different structure that we
 * use.
 * This code was practically scrapped and rewritten be Chris Schantz
 * for GC07.
 */

using namespace std;

#include "GcFollowerController.hh"
#include "TrajHelper.hh"

// These extern's relate to the '--' command line options for TrajFollower (see TrajFollowerMain.cc for set-up details)
extern int           A_HEADING_REAR;
extern int           A_HEADING_FRONT;
extern int           A_YAW;
extern int           Y_REAR;
extern int           Y_FRONT;
extern double       HACK_STEER_COMMAND;
extern bool          USE_HACK_STEER;
extern 

//speed below which we manually set speeds (so as to not have divide-by-zero errors)
#define MIN_SPEED 0.01

/******************************************************************************/
/******************************************************************************/
TrajFollowerController::TrajFollowerController(TrajDirective* mergedDirective, 
				string log_location, string log_name, bool localCoords, bool useNew, bool FFonly)   
{
  // getting start time
  DGCgettime(m_timeBegin);

  cout<<"Initializing TrajFollower Controller(...)"<<endl;
  
  //initializing variables to make valgrind happy =)
  status_flag = 0;
  m_trajCount = 0;
  m_steer_cmd = 0.0;
  m_accel_cmd = 0.0;
  m_speedCapMax = 100.0;
  m_speedCapMin = -1.0;
  m_useLocal = localCoords;
  m_useNew = useNew;
  m_FFonly = FFonly;
  
  if (log_name != "no")
  {
      //initalizing the logging stream
      //getting time information:
      char logsFilePath[FILE_NAME_LENGTH];
      char logsFileName[FILE_NAME_LENGTH];

      timeval tv;
      gettimeofday(&tv,NULL);
      time_t thetime = (time_t)tv.tv_sec;
      tm* timestruct = localtime(&thetime);

      char logtime[50];

      sprintf(logtime, "-%d_%d_%d_%d:%d:%d", timestruct->tm_year+1900,timestruct->tm_mon+1, timestruct->tm_mday, timestruct->tm_hour, timestruct->tm_min, timestruct->tm_sec);
      strcpy(logsFileName, log_name.c_str());
      strcat(logsFileName, logtime);

  
      //creating name
      strcpy(logsFilePath, log_location.c_str());
      strcat(logsFilePath, logsFileName);

      //opening stream in correct place (log_location)
  

      if(m_log_stream.is_open()) 
      {
        m_log_stream.close();
      }

        m_log_stream.open(logsFilePath);
  }
  
  //determining where we calculate errors from. default is YERR_FRONT and AERR_YAW.
  EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
  EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : ( A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));

  //creating the pidControllers
  cout<<"constructing controllers: usenew = "<<m_useNew<<" and ffonly = "<<m_FFonly<<endl;
  m_pPIDcontroller = new CPID_Controller(m_log_stream, yerrorside, aerrorside, 
  			false, m_useLocal, m_useNew, m_FFonly);
  reversecontroller = new CPID_Controller(m_log_stream, YERR_BACK, aerrorside,  
  			true, m_useLocal, m_useNew, m_FFonly);

  cout<<"FINISHED CONSTRUCTING CONTROLLERS"<<endl;

  cout<<"TrajFollowerController (...) Finished"<<endl;
}

TrajFollowerController::~TrajFollowerController() 
{
  delete m_pTraj;
  delete m_pPIDcontroller;
  delete reversecontroller;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollowerController::ControllCycle(ControlStatus* controlStatus, 
     			TrajDirective* mergedDirective, double &steer_Norm, 
			double &accel_Norm, bool reverse, VehicleState& state,
			ActuatorState m_actuatorState) 
{
  /* update internal copies of variables. Used by Sparrow display */
  m_reverse = reverse;
  m_state = state;

  //calculations for the sparrowHawk display
  DGCgettime(grr_timestamp);
  
  //Calculate the required vehicle speed
  m_Speed2 = trajSpeed2(m_state);
  
  /** if our speed was too low, set it as if we're moving in the same direction
  * at the minimum speed WITH THE WHEELS POINTED STRAIGHT */
  if(  trajSpeed2(m_state) < MIN_SPEED )
    {
      m_state.utmNorthVel = MIN_SPEED * cos(m_state.utmYaw);
      m_state.utmEastVel = MIN_SPEED * sin(m_state.utmYaw);
    }

  CPID_Controller* currentcontroller;

  // Determine which controller we should use (forward vs. reverse)
  if(reverse == true)
    currentcontroller = reversecontroller;
  else
    currentcontroller = m_pPIDcontroller;

  //update the trajectory we are following
  m_pTraj = mergedDirective->getTraj();
  if (m_pTraj != 0)
    currentcontroller->SetNewPath(m_pTraj, &m_state);

  //Compute the control inputs
  currentcontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, &m_out_yerr);


  //Set the error variables used in the controlStatus
  ((TrajStatus*)controlStatus)->PerpError = currentcontroller->access_YError();
  ((TrajStatus*)controlStatus)->HeadingError = 
  					currentcontroller->access_AError();

  /**apply DFE to proposed steering commands before shipping them off to adrive 
  * please note that pidController returns steering command in radians */
  //compute normalized steering commands from cmd in radians		
   double proposed_steer_Norm;
   proposed_steer_Norm = m_steer_cmd/VEHICLE_MAX_AVG_STEER;
      
   using namespace sc_interface;
   #ifdef USE_DFE
   steer_Norm = DFE(proposed_steer_Norm, trajSpeed2(m_state));
   #else 
   steer_Norm = proposed_steer_Norm;
   #endif

   //apply bound to the data
   steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);

   accel_Norm = fmax(-1.0, fmin(m_accel_cmd, 1.0));
//   cerr << "ControlCycle: accel_Norm = " << accel_Norm << endl;

}



/**  
 *function: DFE
 *inputs: desired steering command (on a scale of -1 to 1)
 *action: compares desired steering angle to curr angle and speed, and
 *   places hard boundaries on the angle and angle's rate of change
 *   as a function of speed
 *outputs: new, bounded steering command
 **/
#warning "the DFE function in TrajFollower needs attention!"

double TrajFollowerController::DFE(double proposed_cmd, double speed)
{
  double max_safe_cmd, new_cmd;

  //setting boundary conditions. below 4m/s any command is safe.
  //Likewise, a cmd of .1 should be safe at any speed.
  if(proposed_cmd < 0.1 || speed < 4.0) return proposed_cmd;

  max_safe_cmd = -.05 + 25.05/pow(speed,2);

  //  cout<<"max safe, proposed: "<<max_safe_cmd<<' '<<proposed_cmd<<endl;
  if(fabs(max_safe_cmd) - fabs(proposed_cmd) < 0) 
    {
      cerr<<"DFE applied!!"<<endl;
    } 

  new_cmd =  fmax(fmin(proposed_cmd, max_safe_cmd), -max_safe_cmd);

  return new_cmd;
}



