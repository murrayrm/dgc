/*
 * Changes:
 * 
 * Lars Cremean, 7 Sep 04, created (based on Arbiter2/navdisplay.dd)
 *
 */

#include "TrajFollower.hh"

static int user_quit(long);

%%
    TrajFollower
00 +--------------------------------+------------------------------------------+
01 | Time     [s]: %tim             | SteerCommand [-1 to 1]:  %steer_cmd      |
02 | Northing [m]: %nor             | AccelCommand [-1 to 1]:  %accel_cmd      |
03 | Easting  [m]: %eas             | Pitch [rad]:%pit   [deg] %pitd           |
04 | Altitude [m]: %alt             | Roll  [rad]:%rol   [deg] %rold           |
05 | Vel_E  [m/s]: %v_eas           | Yaw   [rad]:%yaw   [deg] %yawd           |
06 | Vel_N  [m/s]: %v_nor           | PitchRate [rad/s]:%v_pit  [deg/s] %v_pitd|
07 | Vel_U  [m/s]: %v_alt           | RollRate  [rad/s]:%v_rol  [deg/s] %v_rold|
08 | Speed  [m/s]: %speed           | YawRate   [rad/s]:%v_yaw  [deg/s] %v_yawd|
09 +--------------------------------+------------------------------------------+
10 | Traj count:  %trct              Status: %statstr                          |
11 | Ref. index:  %refi       	     Dist Left: %d_left               	       |
15 +---------------------------------------------------------------------------+
16 | newYerr:    %newyerr                                                      |
17 |                                                                           |
18 | Phi:     %phi           Term1:  %t1        Term3:   %t3                   |
19 | PhiSS:   %phiss         Term2:  %t2        Term4:   %t4                   |
20 |                                                                           |
21 | Vtraj: %vtr       VRef: %vre                                              |
22 |                                                                           |
23 | AccelFB:  %accfb        LongGainP: %long_gainp    LongErrP: %long_errp    |
24 | AccelFF:  %accff        LongGainI: %long_gaini    LongErrI: %long_erri    |
25 |                         LongGainD: %long_gaind    LongErrD: %long_errd    |
26 | AccelCmd:  %acccmd                                                        |
27 |                                                                           |
28 |                                                                           |
29 |                                                                           |
   +---------------------------------------------------------------------------+
    Display Update Count: %uc
    CTRL-C to exit
%% 
tblname: newvddtable;
bufname: newvddbuf;

int:    %trct RecvTrajCount                       "%d";
int:    %refi TrajIndex                           "%d";
double: %vre  vRef                               "%.3f";
double: %vtr  vTraj                              "%.3f";

double: %accff  accelFF                          "% .3f";
double: %accfb  accelFB                          "% .3f";
double: %acccmd accelCmd                         "% .3f";

double: %strcmd steerCmd                         "% .3f";

double: %steer_cmd      steerCommand             "% .3f";
double: %accel_cmd      accelCommand             "% .3f";

double: %long_gainp	LongGainp	           "% .3f";
double: %long_gaini	LongGaini	           "% .3f";
double: %long_gaind	LongGaind	           "% .3f";
  
double: %long_errp	LongErrp	           "% .3f";
double: %long_erri	LongErri	           "% .3f";
double: %long_errd	LongErrd	           "% .3f";

double: %tim    d_timestamp                      "%.0f";
double:  %speed  d_speed                         "% .3f";

double:  %nor    sparrowX              "% 10.3f";
double:  %eas    sparrowY               "% 10.3f";
double:  %alt    dispSS.utmAltitude              "% .3f";
double:  %v_nor  dispSS.utmNorthVel              "% .3f";
double:  %v_eas  dispSS.utmEastVel               "% .3f";
double:  %v_alt  dispSS.utmAltitudeVel           "% .3f";
double:  %pit    dispSS.utmPitch                 "% .3f";
double:  %rol    dispSS.utmRoll                  "% .3f";
double:  %yaw    dispSS.utmYaw                   "% .4f";
double:  %v_pit  dispSS.utmPitchRate             "% .3f";
double:  %v_rol  dispSS.utmRollRate              "% .3f";
double:  %v_yaw  dispSS.utmYawRate               "% .3f";

double:  %pitd   PitchDeg                        "% .3f";
double:  %rold   RollDeg                         "% .3f";
double:  %yawd   YawDeg                          "% .3f";
double:  %v_pitd PitchRateDeg                    "% .3f";
double:  %v_rold RollRateDeg                     "% .3f";
double:  %v_yawd YawRateDeg                      "% .3f";

double:  %d_left distleft			   "% .3f";

double:  %phi    phi                             "% .3f";
double:  %phiss  phiss                           "% .3f";
double:  %newyerr  newyerr                       "% .3f";

double: %t1   term1                       "% .3f";
double: %t2   term2                       "% .3f";
double: %t3   term3                       "% .3f";
double: %t4   term4                       "% .3f";

short:  %uc SparrowUpdateCount                    "%5d";

string: %statstr trajf_stat_str    "%s" -idname=STATUSSTRING;  
