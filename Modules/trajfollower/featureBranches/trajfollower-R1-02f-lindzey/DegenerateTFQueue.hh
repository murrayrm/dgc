#ifndef DEGENERATE_TF_QUEUE
#define DEGENERATE_TF_QUEUE

#include "GcFollowerMessages.hh"

//! This is the base class for Queues used by the trajfollower arbiter.
//! It turns out all the Queues are just single element replacement setups,
//! so this strips away the more cumbersome features need by real Queues.
//! IMPORTANT USAGE NOTE : if you put a message with ID = 0 onto a queue, that
//! queue will think it is empty.
template <class MyMessageType>
class DegenerateTFQueue
{
  protected:
  MyMessageType CurrMess;

  //! Helper Mutator - Updates the DegenerateQueue so that it contains current 
  //! information. Cleans up the old Message data so that it doesn't 
  //! accumulate over the course of the race.
  virtual void update (tfControlMessage* NewMess)
  {
    CurrMess = *((MyMessageType*)NewMess);
  }

  public:
  //! Constructor - starts up a queue in the empty condition
  explicit DegenerateTFQueue(tfControlMessage* Mess=0) : CurrMess () {}

  //! Destructor - pure virtual, since this class should never be used directly
  virtual ~DegenerateTFQueue() {}

  //! Accessor - returns the id of the current message. Returns 0 if there is
  //! no message in the Queue.
  int currID () 
  { 
    if (CurrMess != 0) 
      return CurrMess.getID();
    else
      return 0;
  }

  //! Accessor - returns the message type of the current message. Not really
  //! sure why this would be needed, but why not have it?
  label_type currLabel () 
  { 
    if (CurrMess != 0) 
      return CurrMess.getLabel();
    else
      return NO_MESS;
  }

  //! Accessor - Calls the rules function on the Queue's contents. If there is
  //! no message, we call the DefaultRules for the specific Queue.
  int checkRules (VehicleState* m_state, TrajStatus* cs) {
    if (CurrMess != 0)
      return CurrMess.Rules(m_state, cs);
    else
      return DefaultRules(m_state, cs);
  }

  //! Accessor - Checks current vehicle state against the default rules.
  //! pure virtual, since it depends on the particular queue.
  virtual int DefaultRules (VehicleState* m_state, TrajStatus* cs) = 0;

  //! Accessor/Mutator - Checks current Queue state to see if the new message 
  //! could logically follow up the current one. Since all of our entry
  //! conditions have to be checked at the time we accept a message, given that
  //! the Queue is only one element, 
  //! pure virtual, since it depends on the particular queue.
  virtual int tryUpdate (tfControlMessage* NewMess) = 0;

  //! Mutator - Empties the Queue
  void dump () 
  { 
    MyMessageType DefaultMess;
    CurrMess = DefaultMess;
  }

  //! Accessor - tells if the Queue is empty
  bool empty () {return (CurrMess.getID() == 0);}

};

#endif
