/*!
 * \file TrajFollwerController.cc 
 * \brief TrajFollower Controller class code
 *
 * This code was written by some combination of Alex Stewart, Laura
 * Lindzey and perhaps others in 2004-05.  It needs some major work to
 * be useful for DGC07, mainly due to the different structure that we
 * use.
 * This code was practically scrapped and rewritten be Chris Schantz
 * for GC07.
 */

using namespace std;

#include "GcFollowerController.hh"
#include "TrajHelper.hh"

// These extern's relate to the '--' command line options for TrajFollower (see TrajFollowerMain.cc for set-up details)
extern int           A_HEADING_REAR;
extern int           A_HEADING_FRONT;
extern int           A_YAW;
extern int           Y_REAR;
extern int           Y_FRONT;
extern double       HACK_STEER_COMMAND;
extern bool          USE_HACK_STEER;
extern 

//speed below which we manually set speeds (so as to not have divide-by-zero errors)
#define MIN_SPEED 0.01

/******************************************************************************/
/******************************************************************************/
TrajFollowerController::TrajFollowerController(TrajDirective* mergedDirective, char* pLindzey)   
{
  // getting start time
  DGCgettime(m_timeBegin);

  cout<<"Initializing TrajFollower Controller(...)"<<endl;
  
  //initializing variables to make valgrind happy =)
  status_flag = 0;
  m_trajCount = 0;
  m_steer_cmd = 0.0;
  m_accel_cmd = 0.0;
  m_speedCapMax = 100.0;
  m_speedCapMin = -1.0;
  logs_enabled = false;
  logs_location = "";
  logs_newDir = false;
  lindzey_logs_location = string(pLindzey);
  
  
  //determining where we calculate errors from. default is YERR_FRONT and AERR_YAW.
  EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
  EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : ( A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));

  //creating the pidControllers
  m_pPIDcontroller = new CPID_Controller(yerrorside, aerrorside, lindzey_logs_location, false);
  reversecontroller = new CPID_Controller(YERR_BACK, aerrorside, lindzey_logs_location, true);

  cout<<"FINISHED CONSTRUCTING CONTROLLERS"<<endl;

  //give our traj to the controllers
  //m_pTraj = mergedDirective->getTraj();
  //m_pPIDcontroller->SetNewPath(m_pTraj, &m_state);
  //reversecontroller->SetNewPath(m_pTraj, &m_state);

  cout<<"TrajFollowerController (...) Finished"<<endl;
}

TrajFollowerController::~TrajFollowerController() 
{
  delete m_pTraj;
  delete m_pPIDcontroller;
  delete reversecontroller;
}

/******************************************************************************/
/******************************************************************************/
void TrajFollowerController::ControllCycle(ControlStatus* controlStatus, 
     			TrajDirective* mergedDirective, double &steer_Norm, 
			double &accel_Norm, bool reverse, VehicleState& state,
			ActuatorState m_actuatorState) 
{
  /* update internal copies of variables. Used by Sparrow display */
  m_reverse = reverse;
  m_state = state;

  /*update the trajectory we are following */
  m_pTraj = mergedDirective->getTraj();
  if (m_pTraj != 0)
  {
    cerr << "Got traj" << endl;
    m_pPIDcontroller->SetNewPath(m_pTraj, &m_state);
    reversecontroller->SetNewPath(m_pTraj, &m_state);
    cerr << "Traj loaded" << endl;
  }

       
  //calculations for the sparrowHawk display
  DGCgettime(grr_timestamp);
  
  //Calculate the required vehicle speed
  m_Speed2 = trajSpeed2(m_state);
  
  /** if our speed was too low, set it as if we're moving in the same direction
  * at the minimum speed WITH THE WHEELS POINTED STRAIGHT */
  if(  trajSpeed2(m_state) < MIN_SPEED )
    {
      m_state.utmNorthVel = MIN_SPEED * cos(m_state.utmYaw);
      m_state.utmEastVel = MIN_SPEED * sin(m_state.utmYaw);
    }

  /* Compute the control inputs. */
  if(reverse == true)
  {   
    reversecontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, &m_out_yerr ,logs_enabled, logs_location, logs_newDir);
  }   
  else
  {
    m_pPIDcontroller->getControlVariables(&m_state, &m_actuatorState, &m_accel_cmd, &m_steer_cmd, &m_out_yerr, logs_enabled, logs_location, logs_newDir,m_speedCapMin,m_speedCapMax);
  }


  /**apply DFE to proposed steering commands before shipping them off to adrive 
  * please note that pidController returns steering command in radians */
  //compute normalized steering commands from cmd in radians		
   double proposed_steer_Norm;
   proposed_steer_Norm = m_steer_cmd/VEHICLE_MAX_AVG_STEER;
      
   using namespace sc_interface;
   #ifdef USE_DFE
   steer_Norm = DFE(proposed_steer_Norm, trajSpeed2(m_state));
   #else 
   steer_Norm = proposed_steer_Norm;
   #endif

   //apply bound to the data
   steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);

   accel_Norm = fmax(-1.0, fmin(m_accel_cmd, 1.0));
   cerr << "ControlCycle: accel_Norm = " << accel_Norm << endl;
  
}



/**  
 *function: DFE
 *inputs: desired steering command (on a scale of -1 to 1)
 *action: compares desired steering angle to curr angle and speed, and
 *   places hard boundaries on the angle and angle's rate of change
 *   as a function of speed
 *outputs: new, bounded steering command
 **/
#warning "the DFE function in TrajFollower needs attention!"

double TrajFollowerController::DFE(double proposed_cmd, double speed)
{
  double max_safe_cmd, new_cmd;

  //setting boundary conditions. below 4m/s any command is safe.
  //Likewise, a cmd of .1 should be safe at any speed.
  if(proposed_cmd < 0.1 || speed < 4.0) return proposed_cmd;

  max_safe_cmd = -.05 + 25.05/pow(speed,2);

  //  cout<<"max safe, proposed: "<<max_safe_cmd<<' '<<proposed_cmd<<endl;
  if(fabs(max_safe_cmd) - fabs(proposed_cmd) < 0) 
    {
      cerr<<"DFE applied!!"<<endl;
    } 

  new_cmd =  fmax(fmin(proposed_cmd, max_safe_cmd), -max_safe_cmd);

  return new_cmd;
}



