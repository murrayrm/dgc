#ifndef TRAJ_STATUS
#define TRAJ_STATUS

#include "gcmodule/GcModule.hh"
#include <string>

class TrajFailureType
{
  public:
  bool perpDist, paraDist, direction;

  explicit TrajFailureType (bool ped = false, bool pad = false, 
  				bool dir = false): perpDist (ped),
    				paraDist (pad), direction (dir) {}  
};

//! This defines TrajStatus, the ControlStatus type used by trajFollower
//! Because trajFollower should only be recieving directives from a 
//! single source, we don't need a lot of detail in the failure message
//! type.
class TrajStatus : public ControlStatus 
{
  public:
   //  inherits
   //  enum statusTypes {FAILED, RUNNING, STOPPED} status;

  /*! failure types:
   *! PERP_DIST : Perpendicular distance from traj is greater than the
   *! allowable margin for error in this situation.
   *! PARA_DIST : Our speed is wrong enough that we have violated the
   *! parallel distance error.
   *! DIRECTION : We could not get the transmission to move to the correct 
   *! position.
   */
  TrajFailureType failure;

  //! Constructor - Initializes values to RUNNING, NONE. This will set up
  //! the first arbitrate step to operate in nominal empty Queue conditions.
  TrajStatus (): ControlStatus(), failure () { status = RUNNING;}

  //! Destructor - does nothing
  virtual ~TrajStatus () {}

  //! Accessor - prints out what's happening for logging purposes
  virtual std::string toString() const {
    std::string failures("");
    if (failure.perpDist)
      failures += "Perpendicular Distance, ";
    if (failure.paraDist)
      failures += "Parallel Distance, ";
    if (failure.direction)
      failures += "Driving Direction ";
   
    if (failures == "")
      failures = "No failures";

    std::ostringstream s("");
    s << status <<" - "<< failures;
    return s.str(); 
  } 
};

#endif
