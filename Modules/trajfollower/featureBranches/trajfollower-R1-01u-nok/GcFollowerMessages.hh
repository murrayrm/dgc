#ifndef TF_MESSAGES
#define TF_MESSAGES

#include "Message.hh"
#include "trajutils/traj.hh" //has CTraj in it
#include "interfaces/VehicleState.h"
#include "TrajStatus.hh"


#include "interfaces/ActuatorState.h"
/* Gives us
 * enum EstopStatus { EstopDisable = 0, EstopPause = 1, EstopRun = 2}; */


class tfControlMessage: public Message {
  public:
  tfControlMessage (label_type lab, unsigned long id): Message (lab, id) {}

  virtual int Rules (VehicleState* m_state, TrajStatus* cs)=0;
};


class eStopMessage: public tfControlMessage {
  private:
  EstopStatus mode;

  public:
  eStopMessage (unsigned long id, EstopStatus esmode): 
    tfControlMessage(ESTOP, id), mode(esmode) {}

  EstopStatus getMode () {return mode;}

  //success = 0, failure = 1
  //If we fail here that means something very, very bad. As in, DARPA told us 
  //to stop and we are not doing it for some reason.
  int Rules (VehicleState* m_state, TrajStatus* cs) 
  {
    if ((mode != EstopRun) && (cs->failure.direction))
      return 1;
    return 0;
  }
};


class TrajMessage: public tfControlMessage {
  private:
  //Need to find out what form the data filtered off skynet into gcInterface
  //is in. Do I need to make a local copy of the CTraj? Should I use a pointer
  //or reference? Assuming for now that it will be dynamically allocated and
  //when the message expires, that memory should be cleaned up.
  CTraj* traj;

  //It might be reasonable to have the margins of parallel and perpendicular 
  //error sendable as part of a traj. For now, however, we don't need that.
  //If that happens, we can stick them in right here.

  public:
  TrajMessage (unsigned long id, CTraj* ct): tfControlMessage (TRAJECTORY, id),
    		traj(ct)    {}

  // Copies from an object
  TrajMessage (TrajMessage &CopyMe) : tfControlMessage (TRAJECTORY, 
    CopyMe.getID()), traj(0)
  {
    if (CopyMe.traj != 0)
      traj = new CTraj (*(CopyMe.traj));
  }

  // Copies from a pointer
  TrajMessage ( TrajMessage *CopyMe) : tfControlMessage (TRAJECTORY,
    CopyMe->getID()), traj (0)
  {
    if (CopyMe->traj != 0)
      traj = new CTraj (*(CopyMe->traj));
  }

  ~TrajMessage () {if (traj != 0) delete traj;}

  CTraj* getTraj () {return traj;}

  //0 = success, 1 = perpendicular failure, 2 = parallel failure, 3 = both
  int Rules (VehicleState* m_state, TrajStatus* cs) {
    if (!(cs->failure.perpDist) && !(cs->failure.paraDist))
      return 0;
    if (cs->failure.perpDist && !(cs->failure.paraDist))
      return 1;
    if (!(cs->failure.perpDist) && cs->failure.paraDist)
      return 2;
    //if (cs->failure.perpDist && cs->failure.paraDist) <actually happens here
      return 3;
  }

};

// Note : this same enum is used by the TrajDirective type
#ifndef TRAJ_F_DIRECTIVE_TYPE
#define TRAJ_F_DIRECTIVE_TYPE
enum DirectionType {FORWARD, REVERSE, STOP};
#endif

class DirectionMessage: public tfControlMessage {
  private:
  DirectionType dir;

  public:
  DirectionMessage (unsigned long id, DirectionType dt): 
    tfControlMessage (DIRECTION, id), dir (dt)    {}

  ~DirectionMessage () {}

  DirectionType getDirection() {return dir;}

  // 0 = success, 1 = failure
  int Rules (VehicleState* m_state, TrajStatus* cs)
  {
    if (cs->failure.direction)
      return 1;
    else
      return 0;
  }

};

enum ReactiveState {DRIVE, BRAKE};

class ReactiveMessage: public tfControlMessage {
  private:
  ReactiveState reaction;

  public:
  ReactiveMessage (unsigned long id, ReactiveState rs): 
    tfControlMessage (REACTIVE, id),  reaction (rs) {}

  ReactiveState getReaction () {return reaction;}

  // 0 is success, 1 is failure.
  int Rules (VehicleState* m_state, TrajStatus* cs)
  {
    if (reaction == DRIVE)
      return 0;
    else
      if (cs->failure.direction)
        return 1;
      else 
        return 0;
  }

};

#endif
