#ifndef TRAJ_F_QUEUES
#define TRAJ_F_QUEUES

#include "DegenerateTFQueue.hh"
#include "GcFollowerMessages.hh"

class eStopQueue : public DegenerateTFQueue
{
protected:

#if 0
  virtual void update (tfControlMessage* NewMess)
  {
    delete CurrMess;
    //CurrMess = (tfControlMessage*) (new eStopMessage (*(eStopMessage*)NewMess));
    CurrMess = NewMess;
  }
#endif

  public:
  eStopQueue (): DegenerateTFQueue() {}

  ~eStopQueue () {}

  //! By default, assume we are meeting estop requirements. Without this, 
  //! we end up a strange situation. This is the result of "pause" being our
  //! default estop mode.
  int DefaultRules (VehicleState* m_state, TrajStatus* cs) 
  {
    return 0;
  }

  //! For now, we only check that the message is of the appropriate type.
  //! 0 = success ; 1 = failure
  int tryUpdate (tfControlMessage* NewMess)
  {
    if (NewMess->getLabel() == ESTOP) {
      update(NewMess);
      return 0;
      }
    else
      return 1;
  }


  //! By default, we assume that we are in pause mode. Need to be told to RUN
  //! before we will get off the brakes.
  EstopStatus getMode () 
  { 
    if (CurrMess == 0)
      return EstopPause;
    else
      return ((eStopMessage*)CurrMess)->getMode(); 
  }
};


class TrajectoryQueue : public DegenerateTFQueue
{
  public:
  TrajectoryQueue (): DegenerateTFQueue() {}

  ~TrajectoryQueue () {}

  //! By default, assume we are meeting traj requirements. Without this, 
  //! we end up a strange situation. This is the result of "0" being our
  //! default mode, which just tells the vehicle to brake.
  int DefaultRules (VehicleState* m_state, TrajStatus* cs) 
  {
    return 0;
  }
  
protected:

#if 0
  //! Mutator - makes a copy of whatever data it needs. Note: this function
  //! should never be called with a 0 pointer, since all access is through
  //! tryUpdate.
  virtual void update (tfControlMessage* NewMess)
  {
    delete CurrMess;
    //CurrMess = (tfControlMessage*) (new TrajMessage((TrajMessage*)NewMess));
    CurrMess = NewMess;
  }
#endif

public:

  //! For now, we only check that the message is of the appropriate type.
  //! 0 = success ; 1 = failure
  int tryUpdate (tfControlMessage* NewMess)
  {
    if (NewMess->getLabel() == TRAJECTORY) {
      update(NewMess);
      return 0;
      }
    else
      return 1;
  }

  //! By default, we have no known safe path. Thus, the only thing we can do is
  //! send a 0, which tells the controller to hit the brakes.
  CTraj* getTraj () 
  {
    if (CurrMess == 0)
      return 0;
    else
      return ((TrajMessage*)CurrMess)->getTraj();
  }
};

class DrivingDirectionQueue : public DegenerateTFQueue
{
protected:

#if 0
  virtual void update (tfControlMessage *NewMess)
  {
    delete CurrMess;
    //CurrMess = (tfControlMessage*) (new DirectionMessage(*(DirectionMessage*) NewMess));
    CurrMess = NewMess;
  }
#endif

  public:
  DrivingDirectionQueue (): DegenerateTFQueue () {}

  ~DrivingDirectionQueue () {}

  //! By default, we assume that cs accurately reflects our success, since
  //! the default direction is STOP, which will not get overridden by a "go"
  //! command from anywhere else. If we fail to meet the STOP objective,
  //! something is very wrong, and we need to tell the higher-ups.
  int DefaultRules (VehicleState* m_state, TrajStatus* cs) 
  {
    if (cs->failure.direction) 
      return 1;
    else
      return 0;
  }

  //! For now, we only check that the message is of the appropriate type.
  //! 0 = success ; 1 = failure
  int tryUpdate (tfControlMessage* NewMess)
  {
    if (NewMess->getLabel() == DIRECTION) {
      update(NewMess);
      return 0;
      }
    else
      return 1;
  }

  //! By defulat, we assume that we should be in stop mode, because that's 
  //! the safest option.
  DirectionType getDirection ()
  {
    if (CurrMess == 0)
      return FORWARD;
    else
      return ((DirectionMessage*)CurrMess)->getDirection();
  }

};

class ReactiveStopQueue : public DegenerateTFQueue
{
protected:

#if 0
  virtual void update (tfControlMessage* NewMess)
  {
    delete CurrMess;
    //CurrMess = (tfControlMessage*) (new ReactiveMessage (*(ReactiveMessage*) NewMess));
    CurrMess = NewMess;
  }
#endif

  public:
  ReactiveStopQueue (): DegenerateTFQueue () {}

  ~ReactiveStopQueue () {}

  //! By default, we assume that we are not about to run into a surprising
  //! obstacle. This means that if reactive is not online, we can function
  //! normally anyway. Thus, we are doing what we are supposed to be (whatever
  //! we otherwise would) no matter what our behavior is.
  int DefaultRules (VehicleState* m_state, TrajStatus* cs)
  {
    return 0;
  }
  
  //! For now, we only check that the message is of the appropriate type.
  //! 0 = success ; 1 = failure
  int tryUpdate (tfControlMessage* NewMess)
  {
    if (NewMess->getLabel() == REACTIVE) {
      update(NewMess);
      return 0;
      }
    else
      return 1;
  }

  //! We do not assume that an obstacle has popped up in front of us out of
  //! nowhere by default.
  int getReaction ()
  {
    if (CurrMess == 0)
      return DRIVE;
    else
      return ((ReactiveMessage*)CurrMess)->getReaction();
  }
};

#endif
