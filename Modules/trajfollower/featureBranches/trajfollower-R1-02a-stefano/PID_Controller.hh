/*
 *CPID_Controller: PID Lateral & Longitudinal 
 *Trajectory Follower implemented using
 *the wrapper classes PathFollower (MTA) or 
 *sn_TrajFollower (Skynet)
 */

/** note that most of the defines have been moved to tf_specs.h */

#ifndef PID_CONTROLLER_HH
#define PID_CONTROLLER_HH

/***** GENERAL INCLUDES ******/

#include "interfaces/VehicleState.h"  // for VehicleState
#include "interfaces/ActuatorState.h"
#include "tf_specs.h"
#include "alice/AliceConstants.h" // for vehicle constants

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include <unistd.h>
#include <iostream> 
#include <iomanip>
#include <time.h>
#include <string.h>



/** 
 * The CTraj trajectory container class and the
 * CPid pid controller wrapper class
 */
#include "trajutils/traj.hh"
#include "pid.hh"


/** 
 * files from which the gains are read in 
 * continuously while PID_Controller is running
 */
#define LatGainFilename				"LatGains.dat"
#define LongGainFilename			"LongGains.dat"
#define LatGainRevFilename			"RevLatGains.dat"
#define LongGainRevFilename			"RevLongGains.dat"

/**
The MAXIMUM size (starting at 1,2,3... etc, i.e. the first
character is NOT character #0) of the field on the sparrow
display used to display the status message for the module
currently the .dd file which defines the size and arrangement
of the sparrow display is dgc/modules/trajFollower/sparrow.dd */
#define MAX_SPARROW_DISPLAY_STATUS_STRING_LENGTH 34

#define SPEED_TO_STOPPING_DIST_MAPPING 2
#define FILE_NAME_LENGTH 256
#define RAD_TO_DEGREE 57.2958

#define MINIMUM_TRACKABLE_SPEED 1.0

//how big the actuator dead zones are
#define THROTTLE_DEAD_BAND .11
#define BRAKE_DEAD_BAND .3

//Time, in seconds, to lookahead on the trajectory before computing control
#define LONGITUDINAL_TIME_LOOKAHEAD 0.8
#define LATERAL_TIME_LOOKAHEAD 0.3

//Caps to the lateral gains (above this, experience shows, we tend to go unstable)
#define LAT_GAIN_CAP_P .18
#define LAT_GAIN_CAP_I .1

//The speeds above/below which speed caps will be ignored
#define NULL_MAX_SPEEDCAP 100
#define NULL_MIN_SPEEDCAP -1

//A cheap hack to pay more attention to the Steering FF.
#define STEERFFGAIN 2



extern double g_LVdistance ; //external variables that carry the info about the leading vehicle
extern double g_LVvelocity  ; // they are filled by the getLeadVehicleInfoThread into GcFollowerModule.cc
extern double g_LVseparation ; 

enum EYerrorside {YERR_FRONT, YERR_BACK};
enum EAerrorside {AERR_FRONT, AERR_BACK, AERR_YAW};

class CPID_Controller
{
  // Use local coordinate */
  bool m_useLocal;
  // use new algorithm
  bool m_useNew;
  //if using new algorithm, use FF only
  bool m_FFonly;

	// the PID controllers
	Cpid* m_pSpeedPID;
        Cpid* m_pLateralPID;
 


        // the reference trajectory we're following
        CTraj m_traj;

 	// these are the indices into the desired Yerror and Aerror arrays
	int m_YerrorIndex, m_AerrorIndex;

        /** whether we're feeding back on yaw error */
	bool m_AerrorYaw;

        /** whether we are the reverse controller */
        bool reverse;

        /** how far to reverse straight, and the starting N,E points */
  //double distanceToReverse;
  double reverseN;
  double reverseE;       

         /** m_refIndex stores the index of the closest 
	  * (spatially) point on the reference traj (reference 
	  * traj is REAR-based) to the rear axle of our vehicle.
          */
        int m_refIndex;
        int oldIndex;

  
  /** keeps track of which gain region we're
   * currently using. For now, it will be specified
   * using the lower speed
   */
  double scheduling_region;
 

       /**
         These contain the state of the front and rear axles of the VEHICLE when
	 asked for control inputs. Angle is either the direction the point in
	 question is moving (if front or rear) or the yaw. Thus arg(angle) at the
	 back is yaw and arg(angle) at the front is yaw+steeringangle */
	double m_vehN[2];
	double m_vehE[2];
	double m_vehDN[2];
	double m_vehDE[2];
	double m_vehAngle[2];
	double m_vehYaw;
	double m_vehPhi;
	double m_yawdot;
        
        //The timestamp for the current state-struct being used by the follower
        double trajF_current_state_Timestamp;

        /**
	 These contain the state of the front and rear axles of the REFERENCE when
	 asked for control inputs. Angle is either the direction the point in
	 question is moving (if front or rear) or the yaw. Thus arg(angle) at the
	 back is yaw and arg(angle) at the front is yaw+steeringangle */
	double m_refN[2];
	double m_refE[2];
	double m_refDN[2];
	double m_refDE[2];
	double m_refDDN_REAR;
	double m_refDDE_REAR;
	double m_refAngle[2];

  //since ref variables are now ref from a point ahead on traj, we also want to log 
  //the points closest to us on traj for performance checking
  double m_trajN, m_trajE, m_trajDE, m_trajDN, m_trajYerr;

  double m_Yerror[2];   // y-error (meters)
  double m_Aerror[2];   // heading-error (rad)
  double m_Cerror;      // combined-error
  double m_Verror;      // speed-error (m/s)

  double m_steerCmd;    // steering command (rad)
  double m_accelCmd;    // acceleration command (m/s^2)
  double m_oldSteerCmd; // previous steering command (rad)

  double prev_measured_steering_angle, curr_measured_steering_angle;

  /***** Feedback and feedforward terms for lateral and speed control ****/

  /** steering control signal from feedback component of controller (rad) */
  double m_SteerFB; 

  /** steering control signal from feedforward component of controller (rad) */
  double m_SteerFF; 
  
  /** acceleration control signal from feedback component of controller (m/s^2) */
  double m_AccelFB; 
  
  /** acceleration control signal from feedforward component of controller (m/s^2) */
  double m_AccelFF;
  double m_speedFF;
  double m_pitchFF;
 
  /** the reference and actual speeds */
  double m_refSpeed;
  double m_trajSpeed;
  double m_trajSpeedLookahead;
  double m_actSpeed;
  double m_minSpeed;
  double m_maxSpeed;
 
  /****** sets up output files *******/

  /**outputs file in format: yerror, aerror, cerror 
   * (adds line every time compute_CError is called) */

  ofstream m_outputLindzey;
  ofstream m_outputLogs;

  /** output file meant for testing purposes. 
   * its contents will change without warning */
  ofstream m_outputTest;

  /** used to keep track of how far until the end of the traj */
  double dist_to_end;
  double stopping_dist;

  /**** VARIABLES USED IN EXCEPTION HANDLING ****/

  /**
   *String used to pass a literal string that declares the 'status'
   *or health of TrajFollower to the Sparrow display for TrajFollower
   *so that it is easier to determine why TrajFollower may be responding
   *in a particular way - for exmaple why is its accel command '-5' or
   *'-6'?
   *NOTE - the size of the string is dictated by the size
   *of the sparrow display and defined by the constant:
   *MAX_SPARROW_DISPLAY_STATUS_STRING_LENGTH given in THIS HEADER FILE */  
  char trajf_status_string [MAX_SPARROW_DISPLAY_STATUS_STRING_LENGTH];


  /** 
   *boolean used to indicate whether an exception 
   * or error has occurred TRUE = exception HAS 
   * occurred, FALSE = NO exceptions (in the last
   * cycle of the active loop of the controller) 
   */
  bool exception_error_flag;

  /**used to indicate whether the adrive actuator struct
   * says we're paused or not */
  int we_are_paused;
  /** the vehicle speed from adrive */
  double we_are_stopped;

  /** used to tell if we've reached the end of the current traj */
  bool too_close_to_end;

  /** Calculates the distance from current traj point to 
   * the end of the trajectory. This is used for error
   * checking, so we stop at the end of a trajectory, 
   * rather than just trying to follow the last point
   * in it. */
  void calculateDistToEnd();

  /** Initializes the distance to the end of the 
   * trajecory fr a newly-received trajectory
   */
  void initializeDistToEnd();

  /** sets up the output streams to send the logs to
   * the proper directory, as directed by timber
   */
  void setup_Output(string logs_location);
 
  /**
   * this one function takes care of all the logging
   * and most of the diagnostic output
   */
  void output_Vars(bool logs_enabled, VehicleState* m_state, ActuatorState* m_actuator);

  /** Calculate the index of the point on the trajectory that is closest to 
   * some NEcoord.  This NEcoord is "frac" fraction away from the NEcoord of 
   * the rear of the vehicle to the NEcoord of the front of the vehicle, where 
   * frac can be any real value (negative and > 1 values are OK). This function 
   * returns the integer index of the closest point on the trajectory. This 
   * function formerly set the member variable m_refIndex but no longer does 
   * so.  */
  int getClosestIndex(double frac);


  /**
   * does exactly what it sounds like, calculating 
   * actual vehicle state for the vehicle
   */
  void compute_FrontAndRearState_veh(VehicleState* pState);
  
  /**
   * does exactly what it sounds like, calculating
   * reference/desired vehicle state for the vehicle
   */
  void compute_FrontAndRearState_ref();

  /** 
   * calculates perpendicular distance from the current
   * trajectory, for both the front and rear axles
   */
  void compute_YError();

  /**
   * calculates heading error from the current trajectory
   * for both front and rear axles.
   */
  void compute_AError();
  
  /** 
   * computes the combined error, using the axle selected
   * for y_error and a_error, as specified by m_YerrorIndex
   * and m_AerrorIndex
   */
  void compute_CError();

  void compute_SteerCmd();
  void compute_newSteerCmd(bool ffonly);


  void compute_SteerFF();
  
  void compute_trajSpeed();
  void compute_actSpeed();
  void compute_refSpeed();

  void compute_AccFF();
  void compute_pitchFF(double pitch);

  /**** METHODS ASSOCIATED WITH EXCEPTION HANDLING ****/
  //See the PID_Controller.cc file for detailed documentation
  void conditionalStringThrow( const bool exception_condition, const string string_to_throw );
  

  /************* FUNCTIONS RELATED TO GAIN SCHEDULING ********/


  /** initializs variables internal to the
   * gain-scheduling functionality */
  void initializeGainScheduling();

  /** calculates whether or not it will be necessary
   * to change the gains */
  bool changeGains();


  /** Resets the integral term to avoid sudden jumps in control signal  */
  void bumplessTransfer(double p, double i, double d);


  /** These functions calculate and return the 
   * gains, assuming gains to be inversely 
   * proportional to speed
   */
  double getPgain();
  double getIgain();
  double getDgain();

  /** function that PID_Controller calls to 
   * perform all the necessary updates.
   * this and initializeGainScheduling should
   * be the only fxns used by PID_Controller 
   */
  void updateGainScheduling();
  
  /** function that modify the reference velocity to account
   * for a leading vehicle in case of queuing/following
   */

  double getVrefLeadVehicle(double vTraj, double distLV, double velLV, double sepLV);

 
public:
  CPID_Controller(EYerrorside yerrorside = YERR_FRONT,EAerrorside aerrorside = AERR_YAW, string lindzey_logs_location="", bool reverse = false, bool useLocal = false, bool useNew = false, bool FFonly = false);

  ~CPID_Controller();
 
  /** set control values */
  bool getControlVariables(VehicleState *state, ActuatorState *actuator, double *accel, double *phi, double *out_yerr, bool logs_enabled, string logs_location, bool logs_newDir, double speedCapMin = -1.0, double speedCapMax = 100.0);

  /** sets new path */
  void SetNewPath(CTraj* path, VehicleState *state);

  /**cleans the underling PID controllers on change of mode**/
  void cleanPIDs();

  /** sets reversing distance for going straight back */
 // void setReverseDistance(double reverseDist, VehicleState *state);

  double access_pitchFF() { return m_pitchFF; }
  double access_speedFF() { return m_speedFF; }


  /** Use this to check the internal variable corresponding to error in heading */
  double access_AError()    { return m_Aerror[m_AerrorIndex]; }

  /** Use this to check the internal variable corresponding to perpendicular error from path */
  double access_YError()    { return m_trajYerr; }

  /** These functions used to access the current gains in the PID controllers */
  double access_LatGainp() { return m_pLateralPID->get_Kp(); }
  double access_LatGaini() { return m_pLateralPID->get_Ki(); }
  double access_LatGaind() { return m_pLateralPID->get_Kd(); }
  double access_LongGainp() { return m_pSpeedPID->get_Kp(); }
  double access_LongGaini() { return m_pSpeedPID->get_Ki(); }
  double access_LongGaind() { return m_pSpeedPID->get_Kd(); }


  /** These functions used to access the current errors in the PID controllers */
  double access_LatErrp() { return m_pLateralPID->getP(); }
  double access_LatErri() { return m_pLateralPID->getI(); }
  double access_LatErrd() { return m_pLateralPID->getD(); }
  double access_LongErrp() { return m_pSpeedPID->getP(); }
  double access_LongErri() { return m_pSpeedPID->getI(); }
  double access_LongErrd() { return m_pSpeedPID->getD(); }

  double access_refDN_REAR() { return m_refDN[1]; }
  double access_refDE_REAR() { return m_refDE[1]; }
  double access_refDDN_REAR() { return m_refDDN_REAR; }
  double access_refDDE_REAR() { return m_refDDE_REAR; }


  /**
   * NOTE: This code is not ideal, as the function 
   * declaration is actually in this file, which is 
   * the header file, however there are issues with 
   * having these functions accessible to sparrow 
   * (which uses C) - hence this appears to be the 
   * best option. 
   *
   * Use this to check the internal variable corresponding 
   * to a double which represents whether the FRONT or 
   * the REAR axle (or some other reference position) 
   * are being used to calculate the Y-ERROR in the lateral 
   * feed-back component of the controller.
   *
   * OUTPUT: double corresponding to the factor multiplying 
   * the wheelbase (which is then added to the center of 
   * the REAR axle (distance always taken along the line 
   * joining the center of the two axles together; HENCE 
   * 0 = REAR AXLE, 1 = FRONT AXLE & 0.5 = HALF-WAY between 
   * the axles 
   */

  double access_YErrorRefPos() {

    /**
     * m_YerrorIndex variable - inverted before being 
     * output so that the y-error reference positions 
     * are: 0 = REAR & 1 = FRONT (i.e. the SAME as the 
     * feed-forward lookahead in that they represesent 
     *a distance in units of wheelbase along the line 
     * of the vehicle FROM the REAR AXLE 
     */
    if (m_YerrorIndex==1) {
    
    /**
    * in the OLD representation this corresponds to the
     * REAR, in the NEW representation it correponds to the
     * FRONT, hence invert, i.e. make = ZERO before returning 
     */
      return double(m_YerrorIndex - 1);

    }
   if(m_YerrorIndex==0) {
 
      /**
       * in the OLD representation this corresponds to the 
       * FRONT, in the NEW representation it corresponds to 
       * to the REAR, hence invert, i.e. make = ONE before returning
       */  
      return double(m_YerrorIndex + 1);
    }
		cerr << "access_YErrorRefPos: We shouldn't be here. Error!!!!!" << endl;
		return 0.0;

  }

  /**
   * Use this to check the internal variable corresponding 
   * to a double which represents whether the FRONT or the 
   * REAR axle (or some other reference position) are being 
   * used to calculate the A-ERROR in the lateral feed-back 
   * component of the controller.
   *   
   * OUTPUT: double corresponding to the factor multiplying 
   *   the wheelbase (which is then added to the center of the 
   *   REAR axle (distance always taken along the line joining 
   *   the center of the two axles together; HENCE 0 = REAR AXLE, 
   *   1 = FRONT AXLE & 0.5 = HALF-WAY between the axles
   */
  double access_AErrorRefPos() { 

    /**
     * m_AerrorIndex variable - inverted before being output 
     * so that the a-error reference positions are: 0 = REAR 
     * & 1 = FRONT (i.e. the SAME as the feed-forward 
     * lookahead in that they represesent a distance in units 
     * of wheelbase along the line of the vehicle FROM the REAR AXLE
     */
    if (m_AerrorIndex==1) {
    
      /** 
       * in the OLD representation this corresponds to the REAR, 
       * in the NEW representation it correponds to the FRONT, 
       * hence invert, i.e. make = ZERO before returning
       */
      return double(m_AerrorIndex - 1);

    }
     if (m_AerrorIndex==0) {
 
      /**
       * in the OLD representation this corresponds to the 
       * FRONT, in the NEW representation it corresponds
       * to the REAR, hence invert, i.e. make = ONE before 
       * returning 
       */
      return double(m_AerrorIndex + 1);
    }

		cerr << "access_AErrorRefPos: We shouldn't be here. Error!!!!!" << endl;
		return 0.0;

  }

  /** 
   * Use this to check the internal variable corresponding 
   * to a double which represents whether the FRONT or the 
   * REAR axle (or some other reference position) are being 
   * used to calculate the lateral FEED-FORWARD component 
   * of the controller
   *
   * OUTPUT: double corresponding to the factor multiplying 
   *   the wheelbase (which is then added to the center of 
   *   the REAR axle (distance always taken along the line 
   *   joining the center of the two axles together; HENCE 
   *   0 = REAR AXLE, 1 = FRONT AXLE & 0.5 = HALF-WAY 
   *   between the axles
  **/
  double access_AFFRefPos() { return double(LOOKAHEAD_FF_LAT_REFPOS); }


  /** Use this to check the internal variable corresponding to combined error.  */
  double access_CError()    { return m_Cerror; }

  /** Use this to check the internal variable corresponding to speed error. */
  double access_VError()    { return m_Verror; }

  double access_AccelFF()   { return m_AccelFF;}
  double access_AccelFB()   { return m_AccelFB;}
  double access_AccelCmd()  { return m_accelCmd;}
  
  double access_SteerFF()   { return m_SteerFF;}
  double access_SteerFB()   { return m_SteerFB;}
  double access_SteerCmd()  { return m_steerCmd;}

  double access_VRef()      { return m_refSpeed; }
  double access_VTraj()     { return m_trajSpeed; }

  int access_refIndex() { return m_refIndex; }
  double access_distToEnd() { return dist_to_end; }
  double access_Timestamp() { return trajF_current_state_Timestamp; }

  char* access_trajf_status_string() { return trajf_status_string; }

};


#endif
