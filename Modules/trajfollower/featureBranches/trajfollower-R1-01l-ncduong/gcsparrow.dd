/*
 * Changes:
 * 
 * Lars Cremean, 7 Sep 04, created (based on Arbiter2/navdisplay.dd)
 *
 */

#include "TrajFollowerController.hh"

static int user_quit(long);

%%
    TrajFollower
00 +--------------------------------+------------------------------------------+
01 | Time     [s]: %tim             | SteerCommand [-1 to 1]:  %steer_cmd      |
02 | Northing [m]: %nor             | AccelCommand [-1 to 1]:  %accel_cmd      |
03 | Easting  [m]: %eas             | Pitch [rad]:%pit   [deg] %pitd           |
04 | Altitude [m]: %alt             | Roll  [rad]:%rol   [deg] %rold           |
05 | Vel_E  [m/s]: %v_eas           | Yaw   [rad]:%yaw   [deg] %yawd           |
06 | Vel_N  [m/s]: %v_nor           | PitchRate [rad/s]:%v_pit  [deg/s] %v_pitd|
07 | Vel_U  [m/s]: %v_alt           | RollRate  [rad/s]:%v_rol  [deg/s] %v_rold|
08 | Speed  [m/s]: %speed           | YawRate   [rad/s]:%v_yaw  [deg/s] %v_yawd|
09 +--------------------------------+------------------------------------------+
10 | Traj count:  %trct              Status: %statstr                          |
11 | Ref. index:  %refi       	     Dist Left: %d_left               	       |
15 +---------------------------------------------------------------------------+
16 |                                                                           |
17 | yError: %yerr           VTraj: %vtr                                       |
18 | aError: %aerr           VRef:  %vre                                       |
19 | cError: %cerr                                                             |
20 |                                                                           |
21 | AccelFB:  %accfb        LongGainP: %long_gainp    LongErrP: %long_errp    |
22 | PitchFF:  %pitchff      LongGainI: %long_gaini    LongErrI: %long_erri    |
23 | SpeedFF:  %speedff      LongGainD: %long_gaind    LongErrD: %long_errd    |
24 | AccelFF:  %accff                                                          |
25 | AccelCmd: %acccmd                                                         |
26 |                                                                           |
27 | SteerFF:  %strff        LatGainP: %lat_gainp      LatErrP: %lat_errp      |
28 | SteerFB:  %strfb        LatGainI: %lat_gaini      LatErrI: %lat_erri      |
29 | SteerCmd: %strcmd       LatGainD: %lat_gaind      LatErrD: %lat_errd      |
30 |                                                                           |
31 | Ref Position W-Base (0=REAR, 1=FRONT): %aFRef                             |
32 | Ref Position W-Base (0=REAR, 1=FRONT): %yERef                             |
33 | Ref Position W-Base (0=REAR, 1=FRONT): %aERef                             |
   +---------------------------------------------------------------------------+
    Display Update Count: %uc
    CTRL-C to exit
%% 
tblname: vddtable;
bufname: vddbuf;

int:    %trct RecvTrajCount                       "%d";
int:    %refi TrajIndex                           "%d";
double: %vre  vRef                               "%.3f";
double: %vtr  vTraj                              "%.3f";

double: %accff  accelFF                          "% .3f";
double: %accfb  accelFB                          "% .3f";
double: %pitchff  pitchFF                        "% .3f";
double: %speedff  speedFF                        "% .3f";
double: %acccmd accelCmd                         "% .3f";

double: %strff  steerFF                          "% .3f";
double: %strfb  steerFB                          "% .3f";
double: %strcmd steerCmd                         "% .3f";

double: %aerr aError                             "% .3f";
double: %yerr yError                             "% .3f";
double: %cerr cError                             "% .3f";

double: %yERef   yErrRef                         "%.3f";
double: %aERef   aErrRef                         "%.3f";
double: %aFRef   aFFref                          "%.3f";

double: %steer_cmd      steerCommand             "% .3f";
double: %accel_cmd      accelCommand             "% .3f";


double: %lat_gainp	LatGainp	           "% .3f";
double: %lat_gaini	LatGaini	           "% .3f";
double: %lat_gaind	LatGaind	           "% .3f";
double: %long_gainp	LongGainp	           "% .3f";
double: %long_gaini	LongGaini	           "% .3f";
double: %long_gaind	LongGaind	           "% .3f";
  
double: %lat_errp	LatErrp		   "% .3f";
double: %lat_erri	LatErri		   "% .3f";
double: %lat_errd	LatErrd		   "% .3f";
double: %long_errp	LongErrp	           "% .3f";
double: %long_erri	LongErri	           "% .3f";
double: %long_errd	LongErrd	           "% .3f";

double: %tim    d_timestamp                      "%.0f";
double:  %speed  d_speed                         "% .3f";

double:  %nor    dispSS.utmNorthing              "% 10.3f";
double:  %eas    dispSS.utmEasting               "% 10.3f";
double:  %alt    dispSS.utmAltitude              "% .3f";
double:  %v_nor  dispSS.utmNorthVel              "% .3f";
double:  %v_eas  dispSS.utmEastVel               "% .3f";
double:  %v_alt  dispSS.utmAltitudeVel           "% .3f";
double:  %pit    dispSS.utmPitch                 "% .3f";
double:  %rol    dispSS.utmRoll                  "% .3f";
double:  %yaw    dispSS.utmYaw                   "% .4f";
double:  %v_pit  dispSS.utmPitchRate             "% .3f";
double:  %v_rol  dispSS.utmRollRate              "% .3f";
double:  %v_yaw  dispSS.utmYawRate               "% .3f";

double:  %pitd   PitchDeg                        "% .3f";
double:  %rold   RollDeg                         "% .3f";
double:  %yawd   YawDeg                          "% .3f";
double:  %v_pitd PitchRateDeg                    "% .3f";
double:  %v_rold RollRateDeg                     "% .3f";
double:  %v_yawd YawRateDeg                      "% .3f";

double:  %d_left distleft			   "% .3f";

short:  %uc SparrowUpdateCount                    "%5d";

string: %statstr trajf_stat_str    "%s" -idname=STATUSSTRING;  
