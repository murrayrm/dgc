/*!
 * \file UT_sendtraj.cc
 * \brief Send command line trajectories to trajfollower
 *
 * \author Richard Murray
 * \date 13 May 2007
 *
 * This program sends trajectories to to Alice from the command line 
 *
 */
