#ifndef TRAJ_DIRECTIVE
#define TRAJ_DIRECTIVE

#include "gcmodule/GcModule.hh"
#include "trajutils/traj.hh"

//Notes for updating trajFollower: (assumes MergedDirective called md 
//instances of m_pTraj should be replaced with md->Traj
//md->direction should be used to check commanded driving direction

// Note : this same enum is used by the DirectionMessage type
#ifndef TRAJ_F_DIRECTIVE_TYPE
#define TRAJ_F_DIRECTIVE_TYPE
enum DirectionType {FORWARD, REVERSE, STOP};
#endif

/*! This class is the MergedDirective used by trajFollower. It contains the
 *! current Traj and the currently commanded driving direction. It should be
 *! noted that "success" in regard to driving direction is measured by 
 *! whether we are commanding aDrive the correct position in the case of
 *! FORWARD and REVERSE, and that our commanded brake position is "on" in
 *! the case of STOP.
 */
class TrajDirective : public MergedDirective
{
  private:
  CTraj* Traj; //!< The current trajectory to follow
  
  //! direction tells which direction we should be going
  DirectionType direction;
  
  public:
  //should there be some sort of max and min speed caps in here?

  //! Constructor - initializes Traj pointer to 0, direction to STOP
  explicit TrajDirective (int ident = 0): MergedDirective (), Traj(0), 
  	direction (STOP) 
   { id = ident; }  

  //! Destructor - does nothing. The CTraj should be in pointer form
  virtual ~TrajDirective () {}

  //! Accessor - prints out stuff for logging
  virtual std::string toString() const
  {
    std::ostringstream s(""); 
    s << id <<" : "<< direction <<" - ";
    if (Traj == 0)
      s <<"Zero Traj";
    else
      s <<"Non-zero Traj";
    return s.str(); 
  }

  //! Mutator - sets the Traj pointer to look at a new thing.
  void setTraj (CTraj* NewTraj) {Traj =  NewTraj;}

  //! Mutator - sets the direction to the appropriate value.
  void setDirection (DirectionType dir) {direction = dir;}

  //! Accessor - returns Traj
  CTraj* getTraj () {return Traj;}

  //! Accessor - returns direction
  DirectionType getDirection () {return direction;}

};

#endif
