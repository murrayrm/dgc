#ifndef TRAJ_F_QUEUES
#define TRAJ_F_QUEUES

#include "DegenerateTFQueue.hh"
#include "GcFollowerMessages.hh"

class eStopQueue : public DegenerateTFQueue <eStopMessage>
{

  public:
  eStopQueue (): DegenerateTFQueue<eStopMessage>() {}

  ~eStopQueue () {}

  //! By default, assume we are meeting estop requirements. Without this, 
  //! we end up a strange situation. This is the result of "pause" being our
  //! default estop mode.
  int DefaultRules (VehicleState* m_state, TrajStatus* cs) 
  {
    return 0;
  }

  //! For now, we only check that the message is of the appropriate type.
  //! 0 = success ; 1 = failure
  int tryUpdate (tfControlMessage* NewMess)
  {
    if (NewMess->getLabel() == ESTOP) {
      update(NewMess);
      return 0;
      }
    else
      return 1;
  }


  //! By default, we assume that we are in pause mode. Need to be told to RUN
  //! before we will get off the brakes.
  EstopStatus getMode () 
  { 
    if (empty())
      return EstopPause;
    else
      return CurrMess.getMode(); 
  }
};


class TrajectoryQueue : public DegenerateTFQueue <TrajMessage>
{
  private:
  //! Stores whatever traj we are currently following. This keeps the traj
  //! from getting eaten and causing a problem once it's on the Queue.
  CTraj CurrentTraj; 
  
  public:
  TrajectoryQueue (): DegenerateTFQueue<TrajMessage>(), CurrentTraj (3) {}

  ~TrajectoryQueue () {}

  //! By default, assume we are meeting traj requirements. Without this, 
  //! we end up a strange situation. This is the result of "0" being our
  //! default mode, which just tells the vehicle to brake.
  int DefaultRules (VehicleState* m_state, TrajStatus* cs) 
  {
    return 0;
  }
  
  //! For now, we only check that the message is of the appropriate type.
  //! 0 = success ; 1 = failure
  //! Trajectory is copied if message is accepted
  int tryUpdate (tfControlMessage* NewMess)
  {
    if (NewMess->getLabel() == TRAJECTORY) {
      update(NewMess);
      if ( ((TrajMessage*)NewMess)->getTraj() != 0)
        CurrentTraj = *(((TrajMessage*)NewMess)->getTraj());
      else
      	CurrentTraj.setNumPoints(0);        
      return 0;
      }
    else
      return 1;
  }

  //! By default, we have no known safe path. Thus, the only thing we can do is
  //! send a 0, which tells the controller to hit the brakes.
  CTraj* getTraj () 
  {
    if (CurrentTraj.getNumPoints() == 0)
      return 0;
    else
      return &CurrentTraj;
  }
};

class DrivingDirectionQueue : public DegenerateTFQueue<DirectionMessage>
{

  public:
  DrivingDirectionQueue (): DegenerateTFQueue<DirectionMessage> () {}

  ~DrivingDirectionQueue () {}

  //! By default, we assume that cs accurately reflects our success, since
  //! the default direction is STOP, which will not get overridden by a "go"
  //! command from anywhere else. If we fail to meet the STOP objective,
  //! something is very wrong, and we need to tell the higher-ups.
  int DefaultRules (VehicleState* m_state, TrajStatus* cs) 
  {
    if (cs->failure.direction) 
      return 1;
    else
      return 0;
  }

  //! For now, we only check that the message is of the appropriate type.
  //! 0 = success ; 1 = failure
  int tryUpdate (tfControlMessage* NewMess)
  {
    if (NewMess->getLabel() == DIRECTION) {
      update(NewMess);
      return 0;
      }
    else
      return 1;
  }

  //! By defulat, we assume that we should be in stop mode, because that's 
  //! the safest option.
  DirectionType getDirection ()
  {
    if (empty())
      return FORWARD;
    else
      return CurrMess.getDirection();
  }

};

class ReactiveStopQueue : public DegenerateTFQueue <ReactiveMessage>
{
  public:
  ReactiveStopQueue (): DegenerateTFQueue<ReactiveMessage> () {}

  ~ReactiveStopQueue () {}

  //! By default, we assume that we are not about to run into a surprising
  //! obstacle. This means that if reactive is not online, we can function
  //! normally anyway. Thus, we are doing what we are supposed to be (whatever
  //! we otherwise would) no matter what our behavior is.
  int DefaultRules (VehicleState* m_state, TrajStatus* cs)
  {
    return 0;
  }
  
  //! For now, we only check that the message is of the appropriate type.
  //! 0 = success ; 1 = failure
  int tryUpdate (tfControlMessage* NewMess)
  {
    if (NewMess->getLabel() == REACTIVE) {
      update(NewMess);
      return 0;
      }
    else
      return 1;
  }

  //! We do not assume that an obstacle has popped up in front of us out of
  //! nowhere by default.
  int getReaction ()
  {
    if (empty())
      return DRIVE;
    else
      return CurrMess.getReaction();
  }
};

#endif
