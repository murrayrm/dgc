
#include <math.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
using namespace std;
#include "pid.hh"
#include "dgcutils/DGCutils.hh"

// Default constructor
Cpid::Cpid(double Kp, double Ki, double Kd, double sat)
  : m_Kp(Kp), m_Ki(Ki), m_Kd(Kd), m_integralLimit(sat)
{
	m_pParamFile = NULL;
	m_integralError = 0.0;
	m_derivativeError = 0.0;
	reset();
}

Cpid::Cpid(char* pParamFile)
{
  /** trying to make valgrind happy*/
  m_Kp = 0.0;
  m_Ki = 0.0;
  m_Kd = 0.0;
  m_integralError = 0.0;
  m_derivativeError = 0.0;
  m_pParamFile = pParamFile;
  m_paramReadCounter = 0;
  reset();

  ifstream paramFile(m_pParamFile);
  if(!paramFile)
  {
    cerr << "COULDN'T READ PID PARAMETER FILE" << endl;
    exit(1);
  }
}

void Cpid::reset(void)
{
  for( int i=0; i < ERROR_SIGNAL_LENGTH; i++ )
  {
    m_errors[i] = 0.0;
    m_stamps[i] = 0;
  }
  m_integralError = 0.0;
  m_derivativeError = 0.0;
}

double Cpid::getP() {
  return m_errors[0];
}
double Cpid::getI() {
	return m_integralError;
}
double Cpid::getD() {
	return m_derivativeError;
}

double Cpid::step_forward(double error)
{
	// read in from a parameter file if we need to
	if(m_pParamFile!=NULL && m_paramReadCounter-- == 0)
	{
		m_paramReadCounter = PARAM_READ_INTERVAL;
		ifstream paramFile(m_pParamFile);
		if(!paramFile)
		{
			cerr << "COULDN'T READ PID PARAMETER FILE" << endl;
			exit(1);
		}

		paramFile >> m_Kp >> m_Ki >> m_Kd >> m_integralLimit;
	}


  // First shift the error signal history
	memmove(&m_errors[1], &m_errors[0], (ERROR_SIGNAL_LENGTH-1)*sizeof(m_errors[0]));
	memmove(&m_stamps[1], &m_stamps[0], (ERROR_SIGNAL_LENGTH-1)*sizeof(m_stamps[0]));

  // Fill in the latest signal
  m_errors[0] = error;

  // Get the time stamp for this
  DGCgettime(m_stamps[0]);

  // First cycle: not enough data to compute derivatives and integrals properly
  if( m_stamps[1] == 0ull )
		return 0.0;

	unsigned long long dtimestamp = m_stamps[0] - m_stamps[1];
	double dtime = DGCtimetosec(dtimestamp);

	// Calculate the integral (forward difference)
	m_integralError += m_errors[0] * dtime;

  // DEBUG
  //printf("m_errors[0] = %f, m_integralError = %f\n", m_errors[0], m_integralError);
  //printf("m_stamps[0] = %f, m_stamps[1] = %f\n", m_stamps[0], m_stamps[1]);
  //printf("m_stamps[1] - m_stamps[0] = %f\n", m_stamps[1] - m_stamps[1]);

  // Saturate the integral to assure that -m_integralLimit <= m_integralError <= m_integralLimit
  m_integralError = fmin(m_integralLimit, fmax(m_integralError, -m_integralLimit));

	int i;

  // Calculate the derivative (initialize to zero)
  m_derivativeError = 0.0;
  // Assume that the bandwidth we are interested in is on the timescale of
  // about 10 Hz.  We care about the derivative over ~100 ms.
  for( i = 0; i < ERROR_SIGNAL_LENGTH; i++ )
  {
      //printf("m_stamps[i] = %f\n",m_stamps[i]);
		if(m_stamps[i] == 0ULL)
		{
			return 0.0;
		}
		dtimestamp = m_stamps[0] - m_stamps[i];
		dtime = DGCtimetosec(dtimestamp);

     // Look far enough back to get a decent sample time
    if( dtime > 0.100 && m_stamps[i] != 0ULL )
    {
      //printf("error diff = %f\n",m_errors[i] - m_errors[0]);
      //printf("time diff = %f\n",m_stamps[i] - m_stamps[0]);

      m_derivativeError = (m_errors[0] - m_errors[i])/dtime;
      break;
    }
  }

  // Return the sum of the three terms
  //printf("m_errors[0] = %f, m_integralError = %f, m_derivativeError = %f\n", 
  //        m_errors[0], m_integralError, m_derivativeError );

  //cout<<"errors calculated (p,i,d): "<<m_errors[0]<<' '<<m_integralError<<' '<<m_derivativeError<<endl;
  //cout<<"gains used (p,i,d): "<<m_Kp<<' '<<m_Ki<<' '<<m_Kd<<endl;
  //cout<<"CPID OUTPUT: "<< m_Kp * m_errors[0] + m_Ki * m_integralError + m_Kd * m_derivativeError<<endl;

	return m_Kp * m_errors[0] + m_Ki * m_integralError + m_Kd * m_derivativeError;
}

// Deconstructor
Cpid::~Cpid()
{
}
