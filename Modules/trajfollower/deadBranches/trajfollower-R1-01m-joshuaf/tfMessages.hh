#ifndef TF_MESSAGES
#define TF_MESSAGES

#include "Message.hh"
#include "trajutils/traj.hh" //has CTraj in it
#include "interfaces/VehicleState.h" //this might be old
#include "TrajStatus.hh"

class tfControlMessage: public Message {
  public:
  tfControlMessage (label_type lab, unsigned long id): Message (lab, id) {}

  virtual int Rules (VehicleState* m_state, TrajStatus* cs)=0;
};

/*
enum eStopMode {RUN, PAUSE, STOP};

class eStopMessage: public tfControlMessage {
  private:
  eStopMode mode;

  public:
  eStopMessage (unsigned long id, eStopMode esmode): 
    tfControlMessage(ESTOP, id), mode(esmode) {}

  eStopMode getMode () {return mode;}

  //success = 0, failure = 1
  int Rules (VehicleState* m_state, TrajStatus* cs) 
  {
    if ((mode != RUN) && (cs->failure.direction))
      return 1;
    return 0;
  }
};
*/

class TrajMessage: public tfControlMessage {
  private:
  //Need to find out what form the data filtered off skynet into gcInterface
  //is in. Do I need to make a local copy of the CTraj? Should I use a pointer
  //or reference? Assuming for now that it will be dynamically allocated and
  //when the message expires, that memory should be cleaned up.
  CTraj* traj;

  //It might be reasonable to have the margins of parallel and perpendicular 
  //error sendable as part of a traj. For now, however, we don't need that.
  //If that happens, we can stick them in right here.

  public:
  TrajMessage (unsigned long id, CTraj* ct): tfControlMessage (TRAJECTORY, id),
    		traj(ct)    {}

  ~TrajMessage () {if (traj != 0) delete traj;}

  CTraj* getTraj () {return traj;}

  //0 = success, 1 = perpendicular failure, 2 = parallel failure, 3 = both
  int Rules (VehicleState* m_state, TrajStatus* cs) {
    if (!(cs->failure.perpDist) && !(cs->failure.paraDist))
      return 0;
    if (cs->failure.perpDist && !(cs->failure.paraDist))
      return 1;
    if (!(cs->failure.perpDist) && cs->failure.paraDist)
      return 2;
    //if (cs->failure.perpDist && cs->failure.paraDist) <actually happens here
      return 3;
  }

};

// Note : this same enum is used by the TrajDirective type
#ifndef TRAJ_F_DIRECTIVE_TYPE
#define TRAJ_F_DIRECTIVE_TYPE
enum DirectionType {FORWARD, REVERSE, STOP};
#endif

class DirectionMessage: public tfControlMessage {
  private:
  DirectionType dir;

  public:
  DirectionMessage (unsigned long id, DirectionType dt): 
    tfControlMessage (DIRECTION, id), dir (dt)    {}

  ~DirectionMessage () {}

  DirectionType getDirection() {return dir;}

  // 0 = success, 1 = failure
  int Rules (VehicleState* m_state, TrajStatus* cs)
  {
    if (cs->failure.direction)
      return 1;
    else
      return 0;
  }

};

enum ReactiveState {DRIVE, BRAKE};

class ReactiveMessage: public tfControlMessage {
  private:
  ReactiveState reaction;

  public:
  ReactiveMessage (unsigned long id, ReactiveState rs): 
    tfControlMessage (REACTIVE, id),  reaction (rs) {}

  ReactiveState getReaction () {return reaction;}

  // 0 is success, 1 is failure.
  int Rules (VehicleState* m_state, TrajStatus* cs)
  {
    if (reaction == DRIVE)
      return 0;
    else
      if (cs->failure.direction)
        return 1;
      else 
        return 0;
  }

};

#endif
