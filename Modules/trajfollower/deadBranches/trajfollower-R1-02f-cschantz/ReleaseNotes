              Release Notes for "trajfollower" module

Release R1-02f-cschantz (Mon Jun 11  9:42:52 2007):
	Merging with latest version, this version has auto-logging abilities

Release R1-02f (Sun Jun 10 10:47:25 2007):
	A few constant changes in the velocity controller to make the 
	ride a lot smoother (vehicle following still has problems 
	though)

Release R1-02e (Sat Jun  9 20:45:01 2007):
	sparrow display now actually matches controller being used (added different .dd file for --use-new flag)

Release R1-02d (Sat Jun  9 19:27:51 2007):
	Fixed Makefile.yam, so gcfollower will always compile (vddtable.h
        wasn't generated anymore since old trajfollower is not compiled
        by default).

Release R1-02c (Sat Jun  9 14:10:53 2007):
	Lots of bug fixes, hopefully no bunny hop anymore, and it *should* not have crazy 
	steering at the end of a traj anymore either.  made the makefile not compile the 
	old trajfollower by default.  removed some extrenious stuff from the sparrow 
	display- will be rewriting this soon to show the new controller's innards.

Release R1-02b (Fri Jun  8 21:56:05 2007):
	Improved logging abilities, it can now log the internal 
	variables of the correct controller when --use-new is used.  
	Also some code clean up, removed unnecesary function calls if 
	--use-new is applied- should result in less inner loop time.

Release R1-02a (Fri Jun  8 11:48:29 2007):
  Inserting velocity reduction in GcFollower when following a vehicle. Apply the flag --use-lead to enable this functionality. If tPlanner is not active or the flag is not applied, this functionality is not active and the module behavior is unchanged. Last note: this module requires to be rewritten accordingly to the coding standards. Now it is a big big mess (and GcFollower is supposed to be a new module...).
ADDED COMMENTS: merged correctly and tested in simulation

Release R1-02 (Tue Jun  5 18:47:30 2007):
        These changes remove some old safety code that checked on our velocity before ordering a 
	transmission shift.  The velocity it was checking relied on a noisy estimate- and the noise is 
	what caused it to take so long to shift during U-turns.  The old code was made unnesecary by 
	gcdrive which does the safety checking using the correct speed from the wheel sensor.     

Release R1-01z (Tue Jun  5 16:48:52 2007):
	Better gains for reverse driving, selects correct traj point in reverse- these improvements are for the PID 
	controller in reverse.

Release R1-01y (Tue Jun  5 16:11:49 2007):
	--use-new now also applies to reversing. Note that this hasn't been tested on Alice yet, but seemed OK in simulation

Release R1-01x (Mon Jun  4 22:40:05 2007):
  Minor build tweak.

Release R1-01w (Mon Jun  4 13:52:56 2007):
	adding --ff-only option to trajfollower, as per Andrew's request

Release R1-01v (Fri Jun  1 16:54:54 2007):
	This version removes the memory leak I had put into the last 
	release of gcfollower. On Andrew's suggestion, I removed almost
	all of the pointers and replaced them with objects by switching 
	the DegeneratetfQueue into a template.

Release R1-01u (Tue May 29 18:04:22 2007):
	This version of gcfollower gets rid of the seg fault bug we had 
	been experiencing (for real this time). It also improves the 
	logs genereated by --logictrack and corrects the problem with 
	TrajCount always reading 0 in the sparrow display. Victory for 
	the forces of Democratic Freedom!

Release R1-01t (Mon May 28 18:31:08 2007):
	aarrrrrgh.
	attempting merge, yet again. I'll be in the lab for the next few hours, so you know where to find me if it still doesn't work :-\

Release R1-01s (Mon May 28 14:41:39 2007):
	This is an update to the trajfollower stopping logic, taking out rules that were peculiar to the old optimization planner interface:
	* Rather than stopping a distance from the end of the traj, we will stop if we're at the last point on the traj, regardless of commanded velocity, but not before
	* Now, require a commanded velocity identical to zero, rather than less than some constant, to command a stop

	Fixing bug in old algorithm that I accidentally introduced in a previous release
	
	--use-new now correctly handles steering feed-forwards calculation when velocity is zero

Release R1-01r (Sun May 27  8:18:20 2007):
	Adding two new command-line options to gcfollower
	--use-new
		use this option for testing in simulation, in particular
		for planners that send spatially continuous trajectories.
		The feedback gains in this option work great with asim and
		gcdrive (as tested using graphplanner running @ .1 Hz)
		but are unstable on Alice. 
	--ff-only
		use this option for planners that replan quickly from 
		Alice's current state. Tested on Alice, with graphplanner
		running @ 5Hz. 

	neither of these changes affect longitudinal control.

	Also, please note that when I was looking through the current (default)
	control algorithm, it looks like when the state struct was changed
	and the old trajFollower code was cleaned up, the distinction
	between front and rear axles was lost. I suspect that this is what
	caused some of the poor performance we've been seeing, as the PID 
	controller is using the error as calculated at the REAR axle. 


Release R1-01q (Tue May 22 22:07:39 2007):
	This release fixes a couple in gcfollower. Most importantly, it 
	fixes a major segmentation faulting problem that we had not seen 
	until running it on a multi-core machine. This release also 
	incorporates logic to handle estop status in the arbiter portion
	of gcfollower so that it can be removed from the hidden logic of 
	PID_Controller. (Note: the logic is currently redundant in the 
	two places.) Also contains some minor runtime optimization 
	changes.

Release R1-01p (Sun May 20 13:28:44 2007):
	Added some unit tests: UT_fwdbwd_sineway.py uses rddfplanner (and
	one of the unit tests there) to generate a forward trajectory
	followed by a reverse one.  Runs with gcfollower.  For comparison,
	UT_fwdbwd_adrive+oldtf.py uses the old trajfollower/adrive.  Both of
	these require that the system-tests module be installed.

Release R1-01o (Sat May 19 11:54:04 2007):
	Added display of northing and easting velocity and acceleration to
	main display.  This is useful for seeing what is going on when
	trajfollower commands large steering angles (and making sure you are
	getting a reasonable trajectory).

Release R1-01n (Wed May 16 21:32:27 2007):
	Modified the outputs of gcfollower such that the sparrow display 
	is no longer bogged down by a bunch of cerr messages. gcfollower 
	has now been tested in forward mode with graphplanner for in 
	excess of 30000 cycles with no apparent errors. It has also been 
	tested with rddfplanner in both forward and reverse. It has no 
	issues with direction changes. It handles low curvitures in 
	either direction (as in rddfplanner/UT_straight.rddf) as well as 
	forward with high curvitures (as in rddfplanner/UT_sinewave.rddf).
	It has trouble reversing along high curviture trajectories, but 
	I think this may have to do with the reverse modeling for the 
	PID controller. Repeated trials, with and without restarting 
	gcfollower consistently yielded the same incorrect reverse 
	following behaviors in this case. gcfollower should be ready for 
	testing on Alice.
	

Release R1-01m (Sat May 12 10:48:26 2007):
	Added --disable-trans flag to allow operation when direction is not
	sent via supercon interface.  This mode is required for operation
	with the current release of graph-planner.  

	Also added a simple unit test trajectory, UT_sinewave.traj.  To use
	this file, run

	  trajfollower --traj=UT_sinewave.traj --disable-trans

Release R1-01l (Wed May  9 18:25:38 2007):
	Commented out some lines that filled our error buffer too fast, also
	some changes to reversing.  Maybe they will help.

Release R1-01k (Thu May  3 17:23:49 2007):
	Added the gcfollower code and build rules. It is currently 
	working in both forward and reverse, although backing up is 
	somewhat quirky. 

Release R1-01j (Thu May  3 14:56:11 2007):
	Added GcModule logging

Release R1-01i (Wed May  2 11:34:52 2007):
	Commented out some old supercon code that was causing problems when
	tplanner commands a stop.

Release R1-01h (Sun Apr 29 19:45:50 2007):
	Added command line option for running with gcdrive. (Merged with
	Chris's changes.)

Release R1-01g (Sun Apr 29 17:14:29 2007):
	Added command line options for not waiting for state and for using local coords.

Release R1-01f (Tue Apr 24 14:53:08 2007):
	Debugged the RoA comm thread, cleaned up the code removing obselete functions, and made it possible to start in the /bin directory.

Release R1-01e (Sun Apr 22 15:48:30 2007):
	Added a new thread for roaComm. A check added within the active loop
	for the roa flag sent from the roaladarperceptor. Currently a positive
	check sets the accelcmd to -1 to slow the vehicle while more checking occurs.

Release R1-01d (Sun Mar 18 20:23:17 2007):
	Added sleeps to gear changing code and ability to check past error
	messages (via sparrow errlog function).  This release has been field
	tested (FT2) and worked with tplanner, rddfplanner and adrive as
	desired.  For future, we need to improve the shifting logic
	(currently based on hand tuned timeouts rather than cananonical
	structure) and also work on improving tracking performance.

Release R1-01c (Sat Mar 17  1:21:27 2007):
	Trajfollower now sends the Estop pause and resume commands when it 
	tells adrive to shift.  Adrive needed these to work correctly.

Release R1-01b (Thu Mar 15 13:33:30 2007):
	Fixed something related to switching between fwd and reverse 
(ask Chris and Tom for details).	

Release R1-01a (Mon Mar 12 15:12:41 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-00e (Wed Mar  7 22:21:40 2007):
	Trajfollower changed to listen to old SuperCon commands with 
	respect to changing state (forward vs reverse) and I removed the 
	past history buffer overhead and made it listen for a reverse 
	trajectories just like it listens for forward trajectories.

Release R1-00d (Sun Feb 25  8:22:58 2007):
	Copied over some of the (deprecated) superCon include files for use
	in debugging (and eventually replacing) this interface.  The files
	are not currently used, but are put into the include directory so
	that pseudocon can access them.  No change to trajfollower
	functionality. 

Release R1-00c (Tue Feb 20 17:31:26 2007):
	Fixed include statements so this version trajfollower compiles
	against revision R2-00a of interfaces and skynet

Release R1-00b (Thu Feb  1  2:16:54 2007):
	Fixed the linking problems and changed variable names for the new
	VehicleState.

Release R1-00a (Wed Jan 31 13:33:32 2007):
	This is the first release of the trajfollower program, which is just
	trajFollower in YaM.  This version will not compile against
	trajutils R1-00a due to a problem with the standard vector library.
	Other than that, it should implement the same functionality as the
	old trajFollower.  Will probably need to be modified to use the new
	VehicleState struct before we do the package release.

Release R1-00 (Sun Jan 28 17:08:09 2007):
	Created.








































