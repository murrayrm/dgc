#include <assert.h>
#include "GcFollowerModule.hh"
#include "TrajHelper.hh"
#include "interfaces/LeadingVehicleInfo.hh"

extern bool NODISPLAY;
double g_LVvelocity = 5.0 ;                  //global variables used for leading vehicle related velocity adjustment 
double g_LVdistance = LV_OUT_OF_RANGE ;
double g_LVseparation = 10.0;

using namespace sc_interface;

//! Constructor - initializes everything
trajfollowerModule::trajfollowerModule (int snKey, char* pLindsey, bool localCoords, bool useNew, bool FFonly, bool useLead):
	CSkynetContainer(SNtrajfollower, snKey),
	CStateClient(true),
	GcModule("trajfollower", &tStatus, &tDirective),
	tStatus(), tDirective(),
	arbiter(),
	roaDowntime(0),
	ReceivedQ(),
	ReceivedMutex(),
	skynetKey(snKey),
	estopID(1)
{
  m_useLead = useLead ;
  m_adriveCommandSF = AdriveCommand::generateSouthface(snKey, this);
  m_adriveCommandSF->setStaleThreshold(100);
  ReceivedQ.clear();
  DGCcreateMutex (&ReceivedMutex);
  my_tfController =  new TrajFollowerController(&tDirective, pLindsey, 
  					localCoords, useNew, FFonly);
  adrive_messageID = 0;
  we_are_stopped = 0.0;
  reverse = false;
  tryingtostop = false;
  if( !NODISPLAY )
    {
      cout<<"Starting sparrow display loops"<<endl;
      my_tfController->TrajFollowerController::Init_SparrowDisplayTable();
      DGCstartMemberFunctionThread(my_tfController,&TrajFollowerController::SparrowDisplayLoop);      
      DGCstartMemberFunctionThread(my_tfController,&TrajFollowerController::UpdateSparrowVariablesLoop);      
    }
    if(m_useLead)
        DGCstartMemberFunctionThread(this, &trajfollowerModule::getLeadingVehicleInfoThread); //starts the thread that updates the leading vehicle information
}

//! Destructor - probably does nothing. Arbiter should clean itself up, and
//! state is probably not dynamically allocated.
trajfollowerModule::~trajfollowerModule () 
{
  delete my_tfController;
}

//! This is the definition for 
void trajfollowerModule::arbitrate (ControlStatus* controlStatus,
					MergedDirective* MergedDirective)
{
  //Look at the messages recieved to get state information to be used for the
  //upcoming processing cycle
  UpdateState ();
  UpdateActuatorState ();

  // Performs any dumps prompted by the ControlStatus, then
  // Updates Queues with messages recieved during last processing cycle, 
  // and checks entry conditions, which in this case are trivial.
  updateQueues ((TrajStatus*)controlStatus);
  //This looks at the current Queue states, and merges them into a directive
  //composed of a traj and driving direction or stop command
  arbiter.mergeQueues((TrajDirective*)MergedDirective);
}

//! Note: trajfollowerModule::control is found in ...?

//dummies for compile-check only
void trajfollowerModule::control (ControlStatus* controlStatus,
					MergedDirective* mergedDirective) 
{
  double steer_Norm = 0.0;
  double accel_Norm = 0.0;
  UpdateState();
  UpdateActuatorState();
  my_tfController->ControllCycle(controlStatus, (TrajDirective*)mergedDirective, steer_Norm, accel_Norm, reverse, m_state, m_actuatorState);

  if (m_actuatorState.m_transpos == GEAR_DRIVE) 
     reverse = false;
  if (m_actuatorState.m_transpos == GEAR_REVERSE)
     reverse = true;

  switch(((TrajDirective*)mergedDirective)->getDirection())
  {
    case STOP:
           tryingtostop = true;
	 break;
                    
    case FORWARD:
           tryingtostop = reverse;
           if (reverse) 
           { 
              m_adriveDirective.command = SetPosition;
              m_adriveDirective.actuator = Transmission;
              m_adriveDirective.arg = GEAR_DRIVE;
              m_adriveDirective.id = adrive_messageID++;
              cerr<<"Control - Stopped. Drive transmission command issued"<<endl;
              m_adriveCommandSF->sendDirective(&m_adriveDirective);
	       }
	 break;

    case REVERSE:
           tryingtostop = !(reverse);
           if (!(reverse)) 
            {
               m_adriveDirective.command = SetPosition;
               m_adriveDirective.actuator = Transmission;
               m_adriveDirective.arg = GEAR_REVERSE;
               m_adriveDirective.id = adrive_messageID++;
               cerr<<"Control - Stopped. Reverse tarnsmission command issued"<<endl;
               m_adriveCommandSF->sendDirective(&m_adriveDirective);
            }
	 break;

    default:
           cerr << "Control : Unkown Direction Directive Received" << endl;
	   tryingtostop = true;
	 break;
  }
  if (tryingtostop)
  {
    accel_Norm = -1.0;
  }


  /* here we send to gcDrive  */
  m_adriveDirective.command = SetPosition;
  
  m_adriveDirective.actuator = Steering;
  m_adriveDirective.arg = steer_Norm;
  m_adriveDirective.id = adrive_messageID;
  adrive_messageID++;
  m_adriveCommandSF->sendDirective(&m_adriveDirective);
  
  m_adriveDirective.id = adrive_messageID;
  adrive_messageID++;
  m_adriveDirective.actuator = Acceleration;
  m_adriveDirective.arg = accel_Norm;
  m_adriveCommandSF->sendDirective(&m_adriveDirective);
  
  AdriveResponse my_Response;

}

//!This function updates arbiter's specialized queues from the general
//!"received" queue that stores messages while control functions are 
//!performed.
void trajfollowerModule::updateQueues (TrajStatus* cs) 
{

  if (cs->failure.direction)
    arbiter.dumpDirection();

  //This keeps us from getting stopped indefinitely by an ROA message
  if (roaDowntime > 0)
    roaDowntime++;
  if (roaDowntime > 10) {
    arbiter.dumpReaction();
    roaDowntime = 0;
  }    
  tfControlMessage* NewMess(0);

  //we assume that we receive an actuator state update that tells us about
  //our new estop status. So, we always add a new message with our current
  //estop status at the start of each control cycle. That's here.
  eStopMessage CurrEStopMess(estopID++,(EstopStatus)m_actuatorState.m_estoppos);
  arbiter.addMessage( (tfControlMessage*) &CurrEStopMess );

  //unload all of our received messages
  DGClockMutex(&ReceivedMutex);
    while (!(ReceivedQ.empty())) {
      NewMess = ReceivedQ.at(0);

      //check 
      if (NewMess->getLabel() == REACTIVE) {
        roaDowntime = 1;
      }

      //we would send an accept/reject message depending on result here in the
      //full CSS version. For now, ignoring return value.
      arbiter.addMessage(NewMess);

      delete ReceivedQ.at(0);

      ReceivedQ.pop_front();

      //Need to figure out how to accept new Traj messages
      //Need to figure out now to accept new Direction messages
    }
  DGCunlockMutex(&ReceivedMutex);
}

void trajfollowerModule::TrajComm ()
{
  int trajSocket;
  trajSocket = m_skynet.listen(SNtraj, SNplanner);
  //Not sure if RecvTraj needs a blank to act as a buffer. Probably best to
  //give it one it won't use. It only costs one CTraj of memory to keep us from
  //having errors we would probably never trace back to this.
  CTraj* NewTraj (0);
  TrajMessage* NewTrajMess(0);
  int trajCount (0);

  while (true) {
    NewTraj = new CTraj(3);
    RecvTraj (trajSocket, NewTraj);
    trajCount++;
    if (NewTraj->getNumPoints() > 0) {
      NewTrajMess = new TrajMessage (trajCount, NewTraj);

      my_tfController->updateTrajCount(trajCount);
    
      //Note : This function does not distinguish between forward and reverse
      //trajectories.
    
      DGClockMutex(&ReceivedMutex);
        ReceivedQ.push_back((tfControlMessage*)NewTrajMess);
      DGCunlockMutex(&ReceivedMutex);
    }
  }
}

void trajfollowerModule::DirectionComm ()
{
  int bytesReceived(0),bytesToReceive(0);
  char* m_pDataBuffer(0);
  superConTrajFcmd* command;
  int reverseSocket;
  reverseSocket = m_skynet.listen(SNsuperconTrajfCmd, MODsupercon);
  bytesToReceive = sizeof(superConTrajFcmd);
  m_pDataBuffer = new char[bytesToReceive];
  
  DirectionMessage* NewDirectionMess(0);
  unsigned Directions(0);

  while(true)
  {
    bytesReceived = m_skynet.get_msg(reverseSocket, m_pDataBuffer, 
    					bytesToReceive, 0);
    command = (superConTrajFcmd*)m_pDataBuffer;
    NewDirectionMess = 0;
    if (bytesReceived != bytesToReceive)
    {
      //std::cerr<< "Trajfollower::superconComm(): skyneterror" << endl;
    }
    else //the message is valid
    {
      //std::cerr <<"Good message by size" << endl;
       if (((*command).commandType == tf_forwards)){ 
        // std::cerr << "tf_forwards message received" << endl;
	 Directions++;
	 NewDirectionMess = new DirectionMessage (Directions, FORWARD);
       }
       else if (((*command).commandType == tf_reverse)){ 
         //std::cerr << "tf_reverse message received" << endl; 
	 Directions++;
	 NewDirectionMess = new DirectionMessage (Directions, REVERSE);
       }
       else {
         //cerr << "Trajfollower::superconComm(): invalid supercon message" 
	 //<< endl; 
       }
    }

    if (NewDirectionMess != 0) {
      DGClockMutex(&ReceivedMutex);
        ReceivedQ.push_back((tfControlMessage*)NewDirectionMess);
      DGCunlockMutex(&ReceivedMutex);
    }
  }
}

void trajfollowerModule::ReactiveComm ()
{
   SkynetTalker<bool> roaTalker(skynetKey, SNroaFlag, SNtrajfollower);
   bool roaFlag = false;
   ReactiveMessage* NewReact(0);
   int reacts(0);

   while (true) {
     bool roaFlagReceived = roaTalker.receive(&roaFlag);
     if (roaFlagReceived) {
       reacts++;
       if (roaFlag)
         NewReact = new ReactiveMessage (reacts, BRAKE);
       else
         NewReact = new ReactiveMessage (reacts, DRIVE);
       
       DGClockMutex(&ReceivedMutex);
         ReceivedQ.push_back((tfControlMessage*)NewReact);
       DGCunlockMutex(&ReceivedMutex);
     }
     usleep(50000);
   }
}


/* This thread reads from skynet the information relative to the leading vehicle
 * and it stores it into two global variables (velocity and distance).
 * This is a TEMPORARY HACK due to very little time left before the site visit.
 *
 * S. Di Cairano jun-07
 */

void trajfollowerModule::getLeadingVehicleInfoThread()
{
    SkynetTalker<leadVehInfo> lvInfoTalker = SkynetTalker<leadVehInfo>(skynetKey, SNleadVehicleInfo, MODtrafficplanner); //change the params
    bool lvInfoCorrect  = false;
    leadVehInfo lvInfo ; 
	cerr << " starting LeadVehicleInfo Thread on skynet key : " << skynetKey << endl; 
	while(true)
	{	
        lvInfoCorrect = false;
        while(lvInfoTalker.hasNewMessage())
		{
		    lvInfoCorrect = lvInfoTalker.receive(&lvInfo);
		}
		if( lvInfoCorrect)
		{
			g_LVvelocity  = lvInfo.velocity ;
			g_LVdistance = lvInfo.distance ;
            g_LVseparation = lvInfo.separationDistance ; 
		}
		//else
		//    cerr << "Bad leadVehicleInfo message" << endl;
		usleep(50000);     //cycle is about 10Hz, this will update up to 20Hz  
	}
}
