#ifndef TRAJHELPER_23456789345678
#define TRAJHELPER_23456789345678

#include "interfaces/VehicleState.h"

double inline trajSpeed2(VehicleState &s)
{
  return hypot(s.utmNorthVel, s.utmEastVel);
}

#endif
