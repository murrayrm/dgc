/*! \file Message.hh
 *  This file contains the definition for the Message class and label_type. */ 

#ifndef MESSAGE
#define MESSAGE

/*! This type is used by Message objects to compactly store what type of 
 *  information they contain. Labels should include information on sender,
 *  intended recipient, and data passed. */
enum label_type { TRAJECTORY, ESTOP, DIRECTION, REACTIVE, NO_MESS };

/*! Messages contain all necessary information and instructions for identifying
 *  control hierarchy messages. All messages should be sent as objects of 
 *  classes derived from the Message class. These should contain as member 
 *  variables all the arguments necessary for the specific message being sent. 
 *  */
class Message {
  private:
  //! tells what sort of message this is	  
  label_type label; 
  //! ID number. ID is unique for each message with a given label
  unsigned long ID; 

  public:
  //! Constructor - takes a label and ID
  Message (label_type l, unsigned long id) : label (l), ID (id) {}

  //! Destructor - nothing to do
  virtual ~Message () {}

  //! Accessor - returns label
  label_type getLabel () { return label; }

  //! Accessor - returns ID
  unsigned long getID () { return ID; }

};

#endif
