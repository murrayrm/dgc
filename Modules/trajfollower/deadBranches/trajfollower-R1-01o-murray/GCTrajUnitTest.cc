#include "Message.hh"
#include "TrajStatus.hh"
#include "tfMessages.hh"
#include <iostream>
#include "DegenerateTFQueue.hh"
#include "TrajQueues.hh"
#include "TrajDirective.hh"
#include "tfArbiter.hh"

using namespace std;

void failhelper (bool failed, int& test, int& pass) 
{
  test++;
  if (failed)
    cout<<"Test "<<test<<" failed."<<endl;
  else
    pass++;
}

int main ()
{
  int Tests(0);
  int Passes(0);
  bool failure(false);

  //Starts with Test #1
  //****************************************************************
  //This section tests the proper creation of and access to Messages
  //****************************************************************
  Message Mess1(TRAJECTORY, 1);
  
  //Label initialization and access
  failure = (Mess1.getLabel() != TRAJECTORY);
  failhelper (failure, Tests, Passes);
  
  //ID initialization and access
  failure = (Mess1.getID() != 1);
  failhelper (failure, Tests, Passes);

  //Starts with Test #3
  //*********************************************************
  //This section tests the TrajStatus class and its elements.
  //*********************************************************
  TrajStatus Status;

  //perpDist initialization 
  failure = (Status.failure.perpDist);
  failhelper (failure, Tests, Passes);

  //paraDist initialization 
  failure = (Status.failure.paraDist);
  failhelper (failure, Tests, Passes);

  //direction initialization 
  failure = (Status.failure.direction);
  failhelper (failure, Tests, Passes);

  //overall initialization
  failure = (Status.toString() != "1 - No failures");
  failhelper (failure, Tests, Passes);
  
  //Starts with Test #7
  //***********************************************
  //This section tests the creation of TrajMessages
  //***********************************************
  TrajMessage trajM(2,0);

  //ID initialization
  failure = (trajM.getID() != 2);
  failhelper (failure, Tests, Passes);

  //Label initialization
  failure = (trajM.getLabel() != TRAJECTORY);
  failhelper (failure, Tests, Passes);

  //Traj initialization (empty)
  failure = (trajM.getTraj() != 0);
  failhelper (failure, Tests, Passes);

  //Starts with Test#10
  //*****************************************************
  //This section tests the rules function of TrajMessages
  //*****************************************************
  
  //All pass scenario
  failure = (trajM.Rules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Perpendicular failure scenario
  Status.failure.perpDist = true;
  failure = (trajM.Rules(0, &Status) != 1);
  failhelper (failure, Tests, Passes);

  //Both failure scenario
  Status.failure.paraDist = true;
  failure = (trajM.Rules(0, &Status) != 3);
  failhelper (failure, Tests, Passes);
 
  //Parallel failure scenario
  Status.failure.perpDist = false;
  failure = (trajM.Rules(0, &Status) != 2);
  failhelper (failure, Tests, Passes);

  //Starts with Test #14
  //***********************************************
  //This section tests the creation of TrajMessages
  //***********************************************
  DirectionMessage directMf(3, FORWARD);

  //ID initialization
  failure = (directMf.getID() != 3);
  failhelper (failure, Tests, Passes);

  //Label initialization
  failure = (directMf.getLabel() != DIRECTION);
  failhelper (failure, Tests, Passes);

  //Forward Direction Initialization
  failure = (directMf.getDirection() != FORWARD);
  failhelper (failure, Tests, Passes);

  //Stop Direction Initialization
  DirectionMessage directMs(4, STOP);
  failure = (directMs.getDirection() != STOP);
  failhelper (failure, Tests, Passes);

  //Reverse Direction Initialization
  DirectionMessage directMr(5, REVERSE);
  failure = (directMr.getDirection() != REVERSE);
  failhelper (failure, Tests, Passes);

  //Starts with Test #19
  //**********************************************************
  //This section tests the rules function of Direction Message
  //**********************************************************

  //No direction failure condition
  failure = (directMf.Rules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Direction failure condition
  Status.failure.direction = true;
  failure = (directMf.Rules(0, &Status) != 1);
  failhelper (failure, Tests, Passes);

  //Starts with Test #21
  //***************************************************
  //This section tests the creation of ReactiveMessages
  //***************************************************
  ReactiveMessage reactiveMd(6, DRIVE);

  //ID initialization
  failure = (reactiveMd.getID() != 6);
  failhelper (failure, Tests, Passes);

  //Label initialization
  failure = (reactiveMd.getLabel() != REACTIVE);
  failhelper (failure, Tests, Passes);

  //Reaction initialization (Drive)
  failure = (reactiveMd.getReaction() != DRIVE);
  failhelper (failure, Tests, Passes);

  //Reaction initialization (Brake)
  ReactiveMessage reactiveMb(7, BRAKE);
  failure = (reactiveMb.getReaction() != BRAKE);
  failhelper (failure, Tests, Passes);

  //Starts with Test #25
  //**********************************************************
  //This section tests the rules function of Reactive Messages
  //**********************************************************

  //Drive with direction failure
  failure = (reactiveMd.Rules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Brake with direction failure
  failure = (reactiveMb.Rules(0, &Status) != 1);
  failhelper (failure, Tests, Passes);

  //Drive without direction failure
  Status.failure.direction = false;
  failure = (reactiveMd.Rules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);
  
  //Brake without direction failure
  failure = (reactiveMb.Rules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Starts with Test #29
  //*****************************************************************
  //This section tests initials that are common to DegenerateTFQueues
  //*****************************************************************
  //Using a default DirectionQueue as my sample
  DrivingDirectionQueue DirectQ;

  //Initialzation to empty
  failure = !(DirectQ.empty());
  failhelper (failure, Tests, Passes);

  //currID behaves properly when empty
  failure = (DirectQ.currID() != 0);
  failhelper (failure, Tests, Passes);

  //currLabel behaves properly when empty
  failure = (DirectQ.currLabel() != NO_MESS);
  failhelper (failure, Tests, Passes);

  //Starts with Test #32
  //**********************************************
  //This section tests bad additions on the Queues 
  //**********************************************
  //Creating CTraj*, Queues, and dynamically allocated Messages of each type
  CTraj* EmptyTraj = new CTraj (3);
  TrajectoryQueue TrajQ;
  ReactiveStopQueue ReactiveQ;
  TrajMessage* trajM_p = new TrajMessage(8, EmptyTraj);
  DirectionMessage* directionMf_p = new DirectionMessage(9, FORWARD);
  ReactiveMessage* reactiveMb_p = new ReactiveMessage(10, BRAKE);

  //tryUpdate returns properly on a wrong Message type - Direction
  failure = (DirectQ.tryUpdate(trajM_p) != 1);
  failhelper (failure, Tests, Passes);

  //tryUpdate returns properly on a wrong Message type - Direction
  failure = (DirectQ.tryUpdate(reactiveMb_p) != 1);
  failhelper (failure, Tests, Passes);

  //make sure the Queue is still empty - Direction
  failure = !(DirectQ.empty());
  failhelper (failure, Tests, Passes);

  //tryUpdate returns properly on a wrong Message type - Trajectory
  failure = (TrajQ.tryUpdate(directionMf_p) != 1);
  failhelper (failure, Tests, Passes);

  //tryUpdate returns properly on a wrong Message type - Trajectory
  failure = (TrajQ.tryUpdate(reactiveMb_p) != 1);
  failhelper (failure, Tests, Passes);

  //make sure the Queue is still empty - Trajectory
  failure = !(TrajQ.empty());
  failhelper (failure, Tests, Passes);

  //tryUpdate returns properly on a wrong Message type - Reactive
  failure = (ReactiveQ.tryUpdate(directionMf_p) != 1);
  failhelper (failure, Tests, Passes);

  //tryUpdate returns properly on a wrong Message type - Reactive
  failure = (ReactiveQ.tryUpdate(trajM_p) != 1);
  failhelper (failure, Tests, Passes);

  //make sure the Queue is still empty - Reactive
  failure = !(ReactiveQ.empty());
  failhelper (failure, Tests, Passes);

  //Starts with Test #41
  //************************************************
  //This section tests valid additions on the Queues
  //************************************************

  //tryUpdate returns properly on a valid Message - Direction
  failure = (DirectQ.tryUpdate(directionMf_p) != 0);
  failhelper (failure, Tests, Passes);

  //Queue is no longer empty -Direction
  failure = (DirectQ.empty());
  failhelper (failure, Tests, Passes);
  
  //tryUpdate returns properly on a valid Message - Trajectory
  failure = (TrajQ.tryUpdate(trajM_p) != 0);
  failhelper (failure, Tests, Passes);

  //Queue is no longer empty - Trajectory
  failure = (TrajQ.empty());
  failhelper (failure, Tests, Passes);

  //tryUpdate returns properly on a valid Message - Reactive
  failure = (ReactiveQ.tryUpdate(reactiveMb_p) != 0);
  failhelper (failure, Tests, Passes);

  //Queue is no longer empty - Reactive
  failure = (ReactiveQ.empty());
  failhelper (failure, Tests, Passes);

  //Starts with Test #47
  //************************************************************
  //This section tests non-default responses to common accessors 
  //************************************************************
  
  //currID works with a message
  failure = (ReactiveQ.currID() != 10);
  failhelper (failure, Tests, Passes);

  //currLabel works with a message
  failure = (ReactiveQ.currLabel() != REACTIVE);
  failhelper (failure, Tests, Passes);

  //Starts with Test #49
  //************************************************************
  //This section tests non-default responses to unique accessors
  //************************************************************

  //Checks getTraj
  failure = (TrajQ.getTraj() != EmptyTraj);
  failhelper (failure, Tests, Passes);

  //Checks getDirection
  failure = (DirectQ.getDirection() != FORWARD);
  failhelper (failure, Tests, Passes);

  //Checks getReaction
  failure = (ReactiveQ.getReaction() != BRAKE);
  failhelper (failure, Tests, Passes);

  //Starts with Test #52
  //****************************************
  //This section tests the non-default rules
  //****************************************

  //Check for a failure from TrajQ's rules
  Status.failure.perpDist = true;
  Status.failure.paraDist = true;
  failure = (TrajQ.checkRules(0, &Status) != 3);
  failhelper (failure, Tests, Passes);

  //Check for a success from TrajQ's rules
  Status.failure.perpDist = false;
  Status.failure.paraDist = false;
  failure = (TrajQ.checkRules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Check for a failure from DirectQ
  Status.failure.direction = true;
  failure = (DirectQ.checkRules(0, &Status) != 1);
  failhelper (failure, Tests, Passes);

  //Check for a success from DirectQ
  Status.failure.direction = false;
  failure = (DirectQ.checkRules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Check for a failure from ReactiveQ
  Status.failure.direction = true;
  failure = (ReactiveQ.checkRules(0, &Status) != 1);
  failhelper (failure, Tests, Passes);

  //Check for a success from ReactiveQ
  Status.failure.direction = false;
  failure = (ReactiveQ.checkRules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Starts at Test #58
  //*****************************************
  //This section tests the dump functionality
  //*****************************************
  
  //Checks that dump works
  ReactiveQ.dump();
  failure = !(ReactiveQ.empty());
  failhelper (failure, Tests, Passes);

  TrajQ.dump();
  DirectQ.dump();

  // NOTE: AFTER THIS, THE QUEUES ARE EMPTY


  //Starts at Test #59
  //********************************************************
  //This section tests default responses to unique accessors
  //********************************************************

  //Checks getTraj
  failure = (TrajQ.getTraj() != 0);
  failhelper (failure, Tests, Passes);

  //Checks getDirection
  failure = (DirectQ.getDirection() != STOP);
  failhelper (failure, Tests, Passes);

  //Checks getReaction
  failure = (ReactiveQ.getReaction() != DRIVE);
  failhelper (failure, Tests, Passes);

  //Starts at Test #62
  //************************************
  //This section tests the default rules
  //************************************
  
  //Check for a success from TrajQ's rules - an empty TrajectoryQueue never
  //returns a fail, since a fail results in STOP, which is the commanded action
  Status.failure.perpDist = true;
  Status.failure.paraDist = true;
  failure = (TrajQ.checkRules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Check for a success from TrajQ's rules
  Status.failure.perpDist = false;
  Status.failure.paraDist = false;
  failure = (TrajQ.checkRules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Check for a failure from DirectQ. If we fail here, that means we can't STOP.
  //In this situation, bad stuff is happening.
  Status.failure.direction = true;
  failure = (DirectQ.checkRules(0, &Status) != 1);
  failhelper (failure, Tests, Passes);

  //Check for a success from DirectQ
  Status.failure.direction = false;
  failure = (DirectQ.checkRules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Check for a success from ReactiveQ - an empty ReactiveQueue assumes DRIVE
  //state, which is non-interactive. Thus, it cannot meaningfully fail.
  Status.failure.direction = true;
  failure = (ReactiveQ.checkRules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Check for a success from ReactiveQ
  Status.failure.direction = false;
  failure = (ReactiveQ.checkRules(0, &Status) != 0);
  failhelper (failure, Tests, Passes);

  //Starts at Test #68
  //*******************************************************
  //Tests the creation and initialization of TrajDirectives
  //*******************************************************
  TrajDirective tDirective(1);

  //Checks initial Traj value
  failure = (tDirective.getTraj() != 0);
  failhelper (failure, Tests, Passes);

  //Checks initial direction value
  failure = (tDirective.getDirection() != STOP);
  failhelper (failure, Tests, Passes);

  //Checks initial logging output - 2 below denotes STOP
  failure = (tDirective.toString() != "1 : 2 - Zero Traj");
  failhelper (failure, Tests, Passes);

  //Starts at Test #71
  //*************************************************************
  //Tests the modification and access of non-empty TrajDirectives
  //*************************************************************
  //Create a new empty dummy. Last one destroyed when TrajQ dumped.
  EmptyTraj = new CTraj(3);

  //Checks setting and accessing Traj
  tDirective.setTraj(EmptyTraj);
  failure = (tDirective.getTraj() != EmptyTraj);
  failhelper (failure, Tests, Passes);

  //Checks setting and accessing Direction
  tDirective.setDirection(REVERSE);
  failure = (tDirective.getDirection() != REVERSE);
  failhelper (failure, Tests, Passes);

  //Checks modified logging output - 1 below denotes REVERSE
  failure = (tDirective.toString() != "1 : 1 - Non-zero Traj");
  failhelper (failure, Tests, Passes);

  //Starts with Test #74
  //****************************************************
  //Tests the Creation and default behavior of tfArbiter
  //****************************************************
  trajfollowerArbiter arbiter;
  
  //Checks that in the empty condition, mergeQueues returns a 0 CTraj*
  arbiter.mergeQueues(&tDirective);
  failure = (tDirective.getTraj() != 0);
  failhelper (failure, Tests, Passes);

  //Checks that in the empty condition, mergeQueues returns a STOP
  failure = (tDirective.getDirection() != STOP);
  failhelper (failure, Tests, Passes);

  //Starts with Test #76
  //*********************************************
  //Tests the addition of Messages to the Arbiter
  //*********************************************
  //make a set of new Messages and Traj
  EmptyTraj = new CTraj (3);
  trajM_p = new TrajMessage(11, EmptyTraj);
  directionMf_p = new DirectionMessage(12, FORWARD);
  DirectionMessage* directionMs_p = new DirectionMessage(13, STOP);
  DirectionMessage* directionMr_p = new DirectionMessage(14, REVERSE);
  reactiveMb_p = new ReactiveMessage(15, BRAKE);
  ReactiveMessage* reactiveMd_p = new ReactiveMessage(16, DRIVE);

  //Check the addition of a TrajMessage
  failure = (arbiter.addMessage(trajM_p) != 0);
  failhelper (failure, Tests, Passes);

  //Check the addition of a DirectionMessage
  failure = (arbiter.addMessage(directionMf_p) != 0);
  failhelper (failure, Tests, Passes);

  //Check the addition of a ReactiveMessage
  failure = (arbiter.addMessage(reactiveMd_p) != 0);
  failhelper (failure, Tests, Passes);

  //Starts with Test #79
  //*****************************************************
  //Tests the mergeQueues function's non-default behavior
  //*****************************************************
  //Listing arbiter state in (Traj, Direction, Reaction) form

  //(Non-0, FORWARD, DRIVE) - Traj
  arbiter.mergeQueues(&tDirective);
  failure = (tDirective.getTraj() != EmptyTraj);
  failhelper (failure, Tests, Passes);

  //(Non-0, FORWARD, DRIVE) - Direction
  failure = (tDirective.getDirection() != FORWARD);
  failhelper (failure, Tests, Passes);

  //(Non-0, FORWARD, BRAKE) - Traj
  arbiter.addMessage(reactiveMb_p);
  arbiter.mergeQueues(&tDirective);
  failure = (tDirective.getTraj() != EmptyTraj);
  failhelper (failure, Tests, Passes);

  //(Non-0, FORWARD, BRAKE) - Direction
  failure = (tDirective.getDirection() != STOP);
  failhelper (failure, Tests, Passes);

  //(Non-0, FORWARD, 0) - Traj
  arbiter.dumpReaction();
  arbiter.mergeQueues(&tDirective);
  failure = (tDirective.getTraj() != EmptyTraj);
  failhelper (failure, Tests, Passes);

  //(Non-0, FORWARD, 0) - Direction - Test #85
  failure = (tDirective.getDirection() != FORWARD);
  failhelper (failure, Tests, Passes);

  //(0, FORWARD, 0) - Traj
  arbiter.dumpTraj();
  arbiter.mergeQueues(&tDirective);
  failure = (tDirective.getTraj() != 0);
  failhelper (failure, Tests, Passes);

  //(0, FORWARD, 0) - Direction
  failure = (tDirective.getDirection() != STOP);
  failhelper (failure, Tests, Passes);

  //(Non-0, STOP, 0) - Traj
  EmptyTraj = new CTraj(3);
  trajM_p = new TrajMessage (17, EmptyTraj);
  arbiter.addMessage(trajM_p);
  arbiter.addMessage(directionMs_p);
  arbiter.mergeQueues(&tDirective);
  failure = (tDirective.getTraj() != EmptyTraj);
  failhelper (failure, Tests, Passes);

  //(Non-0, STOP, 0) - Direction
  failure = (tDirective.getDirection() != STOP);
  failhelper (failure, Tests, Passes);

  //(Non-0, REVERSE, 0) - Traj - Test #90
  arbiter.addMessage(directionMr_p);
  arbiter.mergeQueues(&tDirective);
  failure = (tDirective.getTraj() != EmptyTraj);
  failhelper (failure, Tests, Passes);

  //(Non-0, REVERSE, 0) - Direction
  failure = (tDirective.getDirection() != REVERSE);
  failhelper (failure, Tests, Passes);

  //(Non-0, 0, 0) - Traj
  arbiter.dumpDirection();
  arbiter.mergeQueues(&tDirective);
  failure = (tDirective.getTraj() != EmptyTraj);
  failhelper (failure, Tests, Passes);

  //(Non-0, 0, 0) - Direction
  failure = (tDirective.getDirection() != STOP);
  failhelper (failure, Tests, Passes);

  cout<<"Passes / Tests: "<<Passes<<" / "<<Tests<<endl;
  return 0;

}

