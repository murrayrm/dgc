#ifndef TF_MESSAGES
#define TF_MESSAGES

#include "Message.hh"
#include "trajutils/traj.hh" //has CTraj in it
#include "interfaces/VehicleState.h"
#include "TrajStatus.hh"


#include "interfaces/ActuatorState.h"
/* Gives us
 * enum EstopStatus { EstopDisable = 0, EstopPause = 1, EstopRun = 2}; */


class tfControlMessage: public Message {
  public:
  tfControlMessage (label_type lab, unsigned long id): Message (lab, id) {}

  virtual int Rules (VehicleState* m_state, TrajStatus* cs)=0;
};


class eStopMessage: public tfControlMessage {
  private:
  EstopStatus mode;

  public:
  explicit eStopMessage (unsigned long id = 0, EstopStatus esmode = EstopPause):
    tfControlMessage(ESTOP, id), mode(esmode) {}

  EstopStatus getMode () {return mode;}

  //success = 0, failure = 1
  //If we fail here that means something very, very bad. As in, DARPA told us 
  //to stop and we are not doing it for some reason.
  int Rules (VehicleState* m_state, TrajStatus* cs) 
  {
    if ((mode != EstopRun) && (cs->failure.direction))
      return 1;
    return 0;
  }

  eStopMessage& operator= (const eStopMessage& CopyMe)
  {
    ID = CopyMe.ID;
    label = CopyMe.label;
    mode = CopyMe.mode;
    return *this;
  }
};


class TrajMessage: public tfControlMessage {
  private:
    CTraj* traj;

  //It might be reasonable to have the margins of parallel and perpendicular 
  //error sendable as part of a traj. For now, however, we don't need that.
  //If that happens, we can stick them in right here.

  public:
  explicit TrajMessage (unsigned long id = 0, CTraj* ct = 0): 
  	tfControlMessage (TRAJECTORY, id), traj(ct)    {}

  // Copies from an object
  TrajMessage (TrajMessage &CopyMe) : tfControlMessage (TRAJECTORY, 
    CopyMe.getID()), traj(0)
  {
    if (CopyMe.traj != 0)
      traj = new CTraj (*(CopyMe.traj));
  }

  // Copies from a pointer
  TrajMessage ( TrajMessage *CopyMe) : tfControlMessage (TRAJECTORY,
    CopyMe->getID()), traj (0)
  {
    if (CopyMe->traj != 0)
      traj = new CTraj (*(CopyMe->traj));
  }

  ~TrajMessage () {if (traj != 0) delete traj;}

  CTraj* getTraj () {return traj;}

  //0 = success, 1 = perpendicular failure, 2 = parallel failure, 3 = both
  int Rules (VehicleState* m_state, TrajStatus* cs) {
    if (!(cs->failure.perpDist) && !(cs->failure.paraDist))
      return 0;
    if (cs->failure.perpDist && !(cs->failure.paraDist))
      return 1;
    if (!(cs->failure.perpDist) && cs->failure.paraDist)
      return 2;
    //if (cs->failure.perpDist && cs->failure.paraDist) <actually happens here
      return 3;
  }

  TrajMessage& operator= (const TrajMessage& CopyMe)
  {
    ID = CopyMe.ID;
    label = CopyMe.label;
    traj = CopyMe.traj;
    return *this;
  }

};

// Note : this same enum is used by the TrajDirective type
#ifndef TRAJ_F_DIRECTIVE_TYPE
#define TRAJ_F_DIRECTIVE_TYPE
enum DirectionType {FORWARD, REVERSE, STOP};
#endif

class DirectionMessage: public tfControlMessage {
  private:
  DirectionType dir;

  public:
  DirectionMessage (unsigned long id = 0, DirectionType dt = STOP): 
    tfControlMessage (DIRECTION, id), dir (dt)    {}

  ~DirectionMessage () {}

  DirectionType getDirection() {return dir;}

  // 0 = success, 1 = failure
  int Rules (VehicleState* m_state, TrajStatus* cs)
  {
    if (cs->failure.direction)
      return 1;
    else
      return 0;
  }

  DirectionMessage& operator= (const DirectionMessage& CopyMe)
  {
    ID = CopyMe.ID;
    label = CopyMe.label;
    dir = CopyMe.dir;
    return *this;
  }

};

enum ReactiveState {DRIVE, BRAKE};

class ReactiveMessage: public tfControlMessage {
  private:
  ReactiveState reaction;

  public:
  explicit ReactiveMessage (unsigned long id = 0, ReactiveState rs = DRIVE): 
    tfControlMessage (REACTIVE, id),  reaction (rs) {}

  ReactiveState getReaction () {return reaction;}

  // 0 is success, 1 is failure.
  int Rules (VehicleState* m_state, TrajStatus* cs)
  {
    if (reaction == DRIVE)
      return 0;
    else
      if (cs->failure.direction)
        return 1;
      else 
        return 0;
  }

  ReactiveMessage& operator= (const ReactiveMessage& CopyMe)
  {
    ID = CopyMe.ID;
    label = CopyMe.label;
    reaction = CopyMe.reaction;
    return *this;
  }

};

#endif
