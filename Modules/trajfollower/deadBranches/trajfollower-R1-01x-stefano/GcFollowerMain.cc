#include "GcFollowerModule.hh"

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

#include "dgcutils/DGCutils.hh"
#include "skynet/sn_msg.hh"

//defines for switch statement involving command line options
#define SLEEP_OPT          1
#define LOG_OPT            2
#define TRAJ_OPT           3
#define FILE_OPT           4
#define HACK_STEER         5

//do we use supercon?
#warning "this should be an external var, not placed here"
#define USING_SUPERCON 1

int           NOSTATE     = 0;       // Don't update state if = 1.
int           NODISPLAY   = 0;       // Don't start Sparrow display interface.
int           SIM  = 0;              // Don't send commands to adrive.
unsigned long SLEEP_TIME = 10000;    // How long in us to sleep in active fn.

int           LATERAL_FF_OFF = 0;    // Turn *OFF* the *LATERAL FEED FORWARD* term
                                     // (i.e. ignore its contribution) IF this is
                                     // SET (i.e. = 1)
int           A_HEADING_REAR = 0;    // Calculate the *ANGULAR ERROR* from the
                                     // heading of the *REAR* axle.
int           A_HEADING_FRONT = 0;   // Calculate the *ANGULAR ERROR* from the
                                     // heading of the *FRONT* axle.
int           A_YAW = 1;             // Calculate the *ANGULAR ERROR* from the
                                     // yaw
int           Y_REAR = 0;            // Calculate the *Y-ERROR* from the
                                     // position of the *REAR* axle
int           Y_FRONT = 1;           // Calculate the *Y-ERROR* from the
                                     // position of the *FRONT* axle
int           USE_HACK_STEER = 0;    // whether to use the input steer command
double        HACK_STEER_COMMAND = 0;// aforementioned steer command.(normalized -1 to 1)
int           USE_DBS = 1;           // accept DBS speed caps
int           ERROR_TRACKING = 0;    // do not track errors, by default
int           LOGIC_TRACKING = 0;    // do not track logic, by default
int	      LOCAL_FRAME = 0;       // do not work in local frame, by default
int	      USE_NEW = 0;       //By default, use old algorithm
int	      FF_ONLY = 0;       //by default, use ff+fb                                   
int       USE_LEAD = 0;      //by default do not enable the leading vehicle following routine
/* The name of this program. */
const char * program_name;
char* logNamePrefix = NULL;

/* Prints usage information for this program to STREAM (typically stdout
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
           "  --nostate         Continue even if state data is not received.\n"
           "  --nodisp          Don't start the Sparrow display interface.\n"
           "  --sim             Don't send commands to VDrive.\n"
           "  --sleep us        Specifies us sleep time in Active function (default 10000).\n"
           "  --traj filename   Use filename as the trajectory to follow.\n"
           "  --file filename   Use filename as where to save lindzey logs. \n"
	   "  --hacksteer cmd   Use cmd as steering command (normalized). \n"
           "  --noDBS           Don't listen to DBS speed caps\n"
	   "  --errortrack	Record perpendicular & heading erros.\n"
	   "  --logictrack	Record the logic & directives.\n"
           "  --help, -h        Display this message.\n" 
	   "  --use-local	use local coordinates (deault=off)\n"
	   "  --use-new 	use new control algorithm (deault=off)\n"
	   "  --ff-only 	only use FF control (only has effect w/ --use-new) (default=off_)\n"
	   "  --use-lead 	use information about the leading vehicle to follow (default=off_)\n");
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  /* Set the default arguments that won't need external access here. */

  /* Temporary character. */
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
  static struct option long_options[] = 
    {
      // first: long option (--option) string
      // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
      // third: if pointer, set variable to value of fourth argument
      //        if NULL, getopt_long returns fourth argument
      {"nostate",     0, &NOSTATE,           1},
      {"nodisp",      0, &NODISPLAY,         1},
      {"sim",         0, &SIM,               1},
      {"sleep",       1, NULL,               SLEEP_OPT},
      {"traj",        1, NULL,               TRAJ_OPT},
      {"file",        1, NULL,               FILE_OPT},
      {"help",        0, NULL,               'h'},
      {"log",         1, NULL,               LOG_OPT},
      {"hacksteer",   1, NULL,               HACK_STEER},
      {"noDBS",       0, &USE_DBS,           0},
      {"errortrack",  0, &ERROR_TRACKING,    1},
      {"logictrack",  0, &LOGIC_TRACKING,    1},
      {"use-local",   0, &LOCAL_FRAME,       1},
      {"use-new",   0, &USE_NEW,       1},
      {"ff-only",   0, &FF_ONLY,       1},
      {"use-lead",   0, &USE_LEAD,       1},
      {NULL,          0, NULL,               0}
    };


  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");

  // initialize pTrajName to a NULL string
  char pTrajName[256];
  pTrajName[0] = '\0';
  char pLindzey[256];
  pLindzey[0] = '\0';

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
    {
      switch(ch)
	{
	case TRAJ_OPT:
	  strcpy(pTrajName, optarg);
	  //	  SparrowHawk().log( "reading traj from file" );
	  cout<<"reading traj from file"<<endl;
	  break;

	case FILE_OPT:
	  strcpy(pLindzey, optarg);
	  //	  SparrowHawk().log( "got a file name!!" );
	  cout<<"got a file name!!"<<endl;
	  break;

	case LOG_OPT:
	  logNamePrefix = strdup(optarg);
	  break;

	case SLEEP_OPT:
	  SLEEP_TIME = (unsigned long) atol(optarg);
	  break;

	case HACK_STEER:
	  HACK_STEER_COMMAND = (double) atof(optarg);
	  USE_HACK_STEER = true;
	  break;
      
	case 'h':
	  /* User has requested usage information. Print it to standard
	     output, and exit with exit code zero (normal 
	     termination). */
	  print_usage(stdout, 0);

	case '?': /* The user specified an invalid option. */
	  /* Print usage information to standard error, and exit with exit
	     code one (indicating abnormal termination). */
	  print_usage(stderr, 1);

	case -1: /* Done with options. */
	  break;
	}
    }

  // if we are in error tracking mode, create the error tracking file
  // if we are in logic tracking mode, create the logic tracking file
  ofstream errortrackinglog;
  ofstream logictrackinglog;
  unsigned long long timelabel;
  DGCgettime(timelabel);
  timelabel = timelabel % 10000;

  if (ERROR_TRACKING) { 
    ostringstream errlogname("");
    errlogname << "testlogs/ErrorTrack-" << timelabel << ".log";
    errortrackinglog.open((errlogname.str()).c_str());
    if (!(errortrackinglog.is_open())) {
      cerr << "Could not create an error tracking log";
      ERROR_TRACKING = false;
    }
    else {
      cerr << "Creating error tracking log at "<< errlogname.str();
    }  
  }
  if (LOGIC_TRACKING) {
    ostringstream logiclogname("");
    logiclogname << "testlogs/LogicTrack-" << timelabel << ".log";
    logictrackinglog.open((logiclogname.str()).c_str());
    if (!(logictrackinglog.is_open())) {
      cerr << "Could not create a logic tracking log";
      LOGIC_TRACKING = false;
    }
    else {
      cerr<< "Creating logic tracking log at "<< logiclogname.str();
    }
  }

  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == 0 )
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  cout<<"command line options: use-new = "<<USE_NEW<<" ff-only = "<<FF_ONLY<< " use-lead =  " << USE_LEAD << endl;
  trajfollowerModule my_tfModule (sn_key, pLindzey, LOCAL_FRAME, USE_NEW, FF_ONLY, USE_LEAD);
  TrajStatus* my_trajStatus = new TrajStatus;
  TrajDirective* my_trajDirective = new TrajDirective;
  unsigned long long timestamp1, timestamp2;
  cerr << "Done constructing TrajFollower" << endl;
  
  const long SLEEP_TIME = 100000;  

  DGCstartMemberFunctionThread(&my_tfModule, &trajfollowerModule::TrajComm);
  DGCstartMemberFunctionThread(&my_tfModule,&trajfollowerModule::DirectionComm);
  DGCstartMemberFunctionThread(&my_tfModule, &trajfollowerModule::ReactiveComm);

  while (true)
  {
     
     // timers to make sure our control frequency is what we set (not
     // affected by how long it takes to compute each cycle)
     

     // time checkpoint at the start of the cycle
     DGCgettime(timestamp1);

     //perform operations necessary to figure out the TrajDirective for this
     //control cycle
     my_tfModule.arbitrate(my_trajStatus, my_trajDirective);

     //if we are in logic tracking mode, log the Directive
     if (LOGIC_TRACKING)
       logictrackinglog << timestamp1 <<" : \t"
       			<< my_trajDirective->toString() << " \n";

     //perform operations to make the vehicle comply with the TrajDirective
     my_tfModule.control(my_trajStatus, my_trajDirective);
  
     //if we are in error tracking mode, log the errors from this control cycle
     if (ERROR_TRACKING)
     	errortrackinglog << timestamp1 <<" : \t"
			<<my_trajStatus->errorsToString() << " \t";

     // time checkpoint at the end of the cycle
      DGCgettime(timestamp2);

      /** now sleep for SLEEP_TIME usec less the amount of time the
       * computation took */
      int delaytime = SLEEP_TIME - (timestamp2 - timestamp1);

      //if we are in error tracking mode, log the delay time. The other part
      //of logging is done above to get the most accurate delay time we can.
      //Most likely, it makes almost no difference.
      if (ERROR_TRACKING)
        errortrackinglog << "Delay Time : "<<delaytime<<endl;

      if(delaytime > 0)
	    usleep(delaytime);
  }
  
  return 0;
} // end main()
