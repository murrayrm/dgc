/*! 
 * $Id$
 */
#include "trajFollowerTabSpecs.hh"
#include "TabStruct.h"

#include "GcFollowerController.hh"
#include <unistd.h>
#include <string.h>
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/errlog.h"	 /* error logging utility */
#include "TrajHelper.hh"

// Declare a location to store variables
StrajfollowerTabInput  m_input;

// Inline function for computing velocity vector in the ground plane
//extern double Speed2(VehicleState &s);

VehicleState dispSS;
double sparrowX;
double sparrowY;
double d_timestamp;
double d_speed;

double PitchDeg, RollDeg, YawDeg;
double PitchRateDeg, RollRateDeg, YawRateDeg;

double yError, aError, cError;
double yErrRef, aErrRef, aFFref;

double vRef, vTraj;
double accelFF, accelFB, accelCmd;
double steerFF, steerFB, steerCmd;
double pitchFF, speedFF;

double dphiCmd;
double daccelCmd;

/* gains added as per bug 1881 */
double LatGainp, LatGaini, LatGaind;
double LongGainp, LongGaini, LongGaind;
double LatErrp, LatErri, LatErrd;
double LongErrp, LongErri, LongErrd;

double steerCommand;
double accelCommand;

double revDist, histDist, revLeft;
int sIndex, pIndex;

double MinSC, MaxSC;

int SparrowUpdateCount = 0;
int RecvTrajCount = 0;
int TrajIndex = 0;

double distleft = 0; // distance left on current traj

/* Reference trajectory */
double Nd, Ndd, Ed, Edd;

//String used to display a literal indicating the health of the module
char trajf_stat_str[32];

#include "vddtable.h"
int user_quit(long arg);

void TrajFollowerController::UpdateSparrowVariablesLoop() 
{
  while(true) 
  {
    dispSS = m_state;
    m_input.Northing = m_state.utmNorthing;
    m_input.Easting = m_state.utmEasting;

//   Need to set up a command line argument to change between local and global
//   coordinates.
    if (m_useLocal) {
      sparrowX = m_state.localX;
      sparrowY = m_state.localY;
    }
    else {
      sparrowX = m_state.utmNorthing;
      sparrowY = m_state.utmEasting;
    }


    m_input.Altitude = m_state.utmAltitude;
    m_input.Vel_N = m_state.utmNorthVel;
    m_input.Vel_E = m_state.utmEastVel;
    m_input.Vel_U = m_state.utmAltitudeVel;
    m_input.PitchRad = m_state.utmPitch;
    m_input.RollRad = m_state.utmRoll;
    m_input.YawRad = m_state.utmYaw;
    m_input.PitchRateRad = m_state.utmPitchRate;
    m_input.RollRateRad = m_state.utmRollRate;
    m_input.YawRateRad = m_state.utmYawRate;

    m_input.Time = d_timestamp = m_pPIDcontroller->access_Timestamp();
    m_input.Speed = d_speed = trajSpeed2(m_state);

    m_input.PitchDeg     = PitchDeg     = m_state.utmPitch     * 180.0 /M_PI;
    m_input.RollDeg      = RollDeg      = m_state.utmRoll      * 180.0 /M_PI;
    m_input.YawDeg       = YawDeg       = m_state.utmYaw       * 180.0 /M_PI;
    m_input.PitchRateDeg = PitchRateDeg = m_state.utmPitchRate * 180.0 /M_PI;
    m_input.RollRateDeg  = RollRateDeg  = m_state.utmRollRate  * 180.0 /M_PI;
    m_input.YawRateDeg   = YawRateDeg   = m_state.utmYawRate   * 180.0 /M_PI;

    Nd = m_pPIDcontroller->access_refDN_REAR();
    Ed = m_pPIDcontroller->access_refDE_REAR();

    Ndd = m_pPIDcontroller->access_refDDN_REAR();
    Edd = m_pPIDcontroller->access_refDDE_REAR();


   if(m_reverse == false)
     {
       m_input.SteerCommand = dphiCmd   = m_steer_cmd;
       m_input.AccelCommand = daccelCmd = m_accel_cmd;
       
       m_input.yError = yError = m_pPIDcontroller->access_YError();
       m_input.aError = aError = m_pPIDcontroller->access_AError();
       m_input.cError = cError = m_pPIDcontroller->access_CError();
       
       m_input.yErrorRef = yErrRef = m_pPIDcontroller->access_YErrorRefPos();
       m_input.aErrorRef = aErrRef = m_pPIDcontroller->access_AErrorRefPos();
       m_input.SteerFFRef = aFFref = m_pPIDcontroller->access_AFFRefPos();
       
       m_input.VRef = vRef = m_pPIDcontroller->access_VRef();
       m_input.VTraj = vTraj = m_pPIDcontroller->access_VTraj();
       
       m_input.AccelFF = accelFF = m_pPIDcontroller->access_AccelFF();
       m_input.AccelFB = accelFB = m_pPIDcontroller->access_AccelFB();
       m_input.pitchFF = pitchFF = m_pPIDcontroller->access_pitchFF();
       m_input.speedFF = speedFF = m_pPIDcontroller->access_speedFF();
       m_input.AccelCmd = accelCmd = m_pPIDcontroller->access_AccelCmd();
       
       m_input.SteerFF = steerFF = m_pPIDcontroller->access_SteerFF();
       m_input.SteerFB = steerFB = m_pPIDcontroller->access_SteerFB();
       m_input.SteerCmd = steerCmd = m_pPIDcontroller->access_SteerCmd();
       
       m_input.LatGainP = LatGainp = m_pPIDcontroller->access_LatGainp() ;
       m_input.LatGainI = LatGaini = m_pPIDcontroller->access_LatGaini() ;
       m_input.LatGainD = LatGaind = m_pPIDcontroller->access_LatGaind() ;
       m_input.LongGainP = LongGainp = m_pPIDcontroller->access_LongGainp() ;
       m_input.LongGainI = LongGaini = m_pPIDcontroller->access_LongGaini() ;
       m_input.LongGainD = LongGaind = m_pPIDcontroller->access_LongGaind() ;
       
       m_input.LatErrP = LatErrp = m_pPIDcontroller->access_LatErrp() ;
       m_input.LatErrI = LatErri = m_pPIDcontroller->access_LatErri() ;
       m_input.LatErrD = LatErrd = m_pPIDcontroller->access_LatErrd() ;
       m_input.LongErrP = LongErrp = m_pPIDcontroller->access_LongErrp() ;
       m_input.LongErrI = LongErri = m_pPIDcontroller->access_LongErri() ;
       m_input.LongErrD = LongErrd = m_pPIDcontroller->access_LongErrd() ;
       
       TrajIndex = m_pPIDcontroller->access_refIndex();
       distleft = m_pPIDcontroller->access_distToEnd();
       
       //Update the status string for the TrajFollower module displayed
       //on the Sparrow display

       //m_input.Status = trajf_stat_str = m_pPIDcontroller->access_trajf_status_string();
       strcpy(trajf_stat_str, m_pPIDcontroller->access_trajf_status_string());
       //       cout<<trajf_stat_str<<endl;
	//	m_input.Status = trajf_stat_str;
     }
   else
     {
       m_input.SteerCommand = dphiCmd   = m_steer_cmd;
       m_input.AccelCommand = daccelCmd = m_accel_cmd;
       
       m_input.yError = yError = reversecontroller->access_YError();
       m_input.aError = aError = reversecontroller->access_AError();
       m_input.cError = cError = reversecontroller->access_CError();
       
       m_input.yErrorRef = yErrRef = reversecontroller->access_YErrorRefPos();
       m_input.aErrorRef = aErrRef = reversecontroller->access_AErrorRefPos();
       m_input.SteerFFRef = aFFref = reversecontroller->access_AFFRefPos();
       
       m_input.VRef = vRef = reversecontroller->access_VRef();
       m_input.VTraj = vTraj = reversecontroller->access_VTraj();
       
       m_input.AccelFF = accelFF = reversecontroller->access_AccelFF();
       m_input.AccelFB = accelFB = reversecontroller->access_AccelFB();
       m_input.pitchFF = pitchFF = reversecontroller->access_pitchFF();
       m_input.speedFF = speedFF = reversecontroller->access_speedFF();
       m_input.AccelCmd = accelCmd = reversecontroller->access_AccelCmd();
       
       m_input.SteerFF = steerFF = reversecontroller->access_SteerFF();
       m_input.SteerFB = steerFB = reversecontroller->access_SteerFB();
       m_input.SteerCmd = steerCmd = reversecontroller->access_SteerCmd();
       
       m_input.LatGainP = LatGainp = reversecontroller->access_LatGainp() ;
       m_input.LatGainI = LatGaini = reversecontroller->access_LatGaini() ;
       m_input.LatGainD = LatGaind = reversecontroller->access_LatGaind() ;
       m_input.LongGainP = LongGainp = reversecontroller->access_LongGainp() ;
       m_input.LongGainI = LongGaini = reversecontroller->access_LongGaini() ;
       m_input.LongGainD = LongGaind = reversecontroller->access_LongGaind() ;

       m_input.LatErrP = LatErrp = reversecontroller->access_LatErrp() ;
       m_input.LatErrI = LatErri = reversecontroller->access_LatErri() ;
       m_input.LatErrD = LatErrd = reversecontroller->access_LatErrd() ;
       m_input.LongErrP = LongErrp = reversecontroller->access_LongErrp() ;
       m_input.LongErrI = LongErri = reversecontroller->access_LongErri() ;
       m_input.LongErrD = LongErrd = reversecontroller->access_LongErrd() ;

        
       //Update the status string for the TrajFollower module displayed
       //on the Sparrow display
       //strcpy(trajf_stat_str, reversecontroller->access_trajf_status_string());
       strcpy(trajf_stat_str, "backing up now           \n");
     }
	
   //	dd_refresh(STATUSSTRING);
    
    m_input.TrajCount = RecvTrajCount = m_trajCount;

    steerCommand = m_steer_cmd; 
    accelCommand = m_accel_cmd;

    
    SparrowUpdateCount ++;

    // Wait a bit, because other threads will print some stuff out
    usleep(500000); 

  }
}

void TrajFollowerController::Init_SparrowDisplayTable() {

  //Initialise the vddtable used by the Sparrow display for the TrajFollower
  //module - note that the initialisation of the table occurs here so that the
  //table can be initialised before the sparrow related threads are started
  //as otherwise a race condition can occur (see comment below in the
  //SparrowDisplayLoop() method for more information)
  if (dd_open() < 0) exit(1);
  dd_usetbl(vddtable);

}

void TrajFollowerController::SparrowDisplayLoop() 
{
  dbg_all = 0;

  if (dd_open() < 0) exit(1);

  //Removed the table initialisation of vddtable here, as it allows a race condition
  //to exist between the two threads spawned by TrajFollower (SparrowDisplayLoop
  // & UpdateSparrowVariablesLoop), essentially there are methods in the later
  //thread (UpdateSparrowVariablesLoop) which require the table to be initialised
  //*before* they are called - however as the threads run in parallel there is no
  //check to verify that this is the case (other than that the first string is
  //started first, which is what leads to a potential race condition).  Hence
  //the initialisation of the vddtable (sparrow table) has been moved to the
  //method Init_SparrowDisplayTable, which is called *before* either of the threads
  //are started - hence the table *must* exist prior to the threads being called.
  dd_bindkey('q', user_quit);

  usleep(250000); // Wait a bit, because other threads will print some stuff out

  /* Set up error logging */
  dd_errlog_init(50);
  dd_errlog_bindkey();

  dd_loop();
  dd_close();

}

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}
