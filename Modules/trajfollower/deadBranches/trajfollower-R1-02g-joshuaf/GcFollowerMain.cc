#include "GcFollowerModule.hh"

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

#include "dgcutils/DGCutils.hh"
#include "skynet/sn_msg.hh"

#include "gccmdline.h"

//do we use supercon?
#define USING_SUPERCON 1

int           NOSTATE     = 0;       // Don't update state if = 1.
int           NODISPLAY   = 0;       // Don't start Sparrow display interface.
int           SIM  = 0;              // Don't send commands to adrive.
unsigned long SLEEP_TIME = 10000;    // How long in us to sleep in active fn.

int           LATERAL_FF_OFF = 0;    // Turn *OFF* the *LATERAL FEED FORWARD* term
                                     // (i.e. ignore its contribution) IF this is
                                     // SET (i.e. = 1)
int           A_HEADING_REAR = 0;    // Calculate the *ANGULAR ERROR* from the
                                     // heading of the *REAR* axle.
int           A_HEADING_FRONT = 0;   // Calculate the *ANGULAR ERROR* from the
                                     // heading of the *FRONT* axle.
int           A_YAW = 1;             // Calculate the *ANGULAR ERROR* from the
                                     // yaw
int           Y_REAR = 0;            // Calculate the *Y-ERROR* from the
                                     // position of the *REAR* axle
int           Y_FRONT = 1;           // Calculate the *Y-ERROR* from the
                                     // position of the *FRONT* axle
int           USE_HACK_STEER = 0;    // whether to use the input steer command
double        HACK_STEER_COMMAND = 0;// aforementioned steer command.(normalized -1 to 1)
int           USE_DBS = 1;           // accept DBS speed caps
int           ERROR_TRACKING = 0;    // do not track errors, by default
int           LOGIC_TRACKING = 0;    // do not track logic, by default

/* The name of this program. */
const char * program_name;
char* logNamePrefix = NULL;

int main(int argc, char **argv) 
{
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) {
    exit (1);}
  if (cmdline.argfile_given &&
      cmdline_parser_configfile(cmdline.argfile_arg, &cmdline, 0, 0, 1) != 0){
    exit (1);}

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");

  char* pLindzey;
  pLindzey = cmdline.file_arg;


  // Initializes the trajfollowerModule object
  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == 0 )
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;
  cout<<"command line options: use-new = "<<cmdline.use_new_flag
  	<<" ff-only = "<<cmdline.ff_only_flag<< " use-lead =  " 
	<< cmdline.use_lead_flag << endl;
  trajfollowerModule my_tfModule (sn_key, pLindzey, cmdline.use_local_flag, 
  	cmdline.use_new_flag, cmdline.ff_only_flag, cmdline.use_lead_flag);
  TrajStatus* my_trajStatus = new TrajStatus;
  TrajDirective* my_trajDirective = new TrajDirective;
  unsigned long long timestamp1, timestamp2;
  cerr << "Done constructing TrajFollower" << endl;
  
  // initialize pTrajName to a NULL string
  char pTrajName[256];
  pTrajName[0] = '\0';

  if (cmdline.traj_given) {
    my_tfModule.SendSelfTraj(cmdline.traj_arg);
  }

  logNamePrefix = cmdline.log_arg;
  SLEEP_TIME = cmdline.sleep_arg;
  if (cmdline.hacksteer_given) {
    HACK_STEER_COMMAND = (double) atof(optarg);
    USE_HACK_STEER = true;
  }      

  // if we are in error tracking mode, create the error tracking file
  // if we are in logic tracking mode, create the logic tracking file
  ofstream errorandlogiclog;
  unsigned long long timelabel;
  DGCgettime(timelabel);
  timelabel = (timelabel - (timelabel % 10000)) / 10000;

  if (cmdline.log_errors_flag || cmdline.log_logic_flag) {
    ostringstream errlogname("");
    errlogname << "testlogs/ErrorsAndLogic-" << timelabel << ".log";
    errorandlogiclog.open((errlogname.str()).c_str());
    if (!(errorandlogiclog.is_open())) {
      cerr << "Could not create an error tracking log";
      cmdline.log_errors_flag = false;
      cmdline.log_logic_flag = false;
    }
    else {
      cerr << "Creating error tracking log at "<< errlogname.str();
    } 
  }

  DGCstartMemberFunctionThread(&my_tfModule,&trajfollowerModule::DirectionComm);
  DGCstartMemberFunctionThread(&my_tfModule, &trajfollowerModule::ReactiveComm);

  //If we are running from a traj from a file, we "send" it to ourselves here.
  //This lets everything run normally except that our trajcount will stay zero.
  if (cmdline.traj_arg != "") {
    my_tfModule.SendSelfTraj(cmdline.traj_arg);
  } else {
    DGCstartMemberFunctionThread(&my_tfModule, &trajfollowerModule::TrajComm);
  }

  while (true)
  {
     
     // timers to make sure our control frequency is what we set (not
     // affected by how long it takes to compute each cycle)
     

     // time checkpoint at the start of the cycle
     DGCgettime(timestamp1);

     //perform operations necessary to figure out the TrajDirective for this
     //control cycle
     my_tfModule.arbitrate(my_trajStatus, my_trajDirective);

     //if we are in logic tracking mode, log the Directive
     if (cmdline.log_logic_flag)
       errorandlogiclog << timestamp1 <<" : \t"
       			<< my_trajDirective->toString() << " \n";

     //perform operations to make the vehicle comply with the TrajDirective
     my_tfModule.control(my_trajStatus, my_trajDirective);
  
     //if we are in error tracking mode, log the errors from this control cycle
     if (cmdline.log_errors_flag)
     	errorandlogiclog << timestamp1 <<" : \t"
			<<my_trajStatus->errorsToString() << " \t";

     // time checkpoint at the end of the cycle
      DGCgettime(timestamp2);

      /** now sleep for SLEEP_TIME usec less the amount of time the
       * computation took */
      int delaytime = SLEEP_TIME - (timestamp2 - timestamp1);

      //if we are in error tracking mode, log the delay time. The other part
      //of logging is done above to get the most accurate delay time we can.
      //Most likely, it makes almost no difference.
      if (cmdline.log_errors_flag)
        errorandlogiclog << "Delay Time : "<<delaytime<<endl;

      if(delaytime > 0)
	    usleep(delaytime);
  }
  
  return 0;
} // end main()
