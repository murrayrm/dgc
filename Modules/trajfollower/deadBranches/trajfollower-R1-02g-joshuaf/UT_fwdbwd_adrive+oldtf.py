#! /usr/bin/python -i
#
# \file UT_fwdbwd_adrive+oldtf.py 
# \brief Test forward and reverse driving using old code
#
# \author Richard Murray
# \date 19 May 2007
#
# This unit test generates a feasible, but reasonably aggressive
# trajectory in both forward and reverse.  It uses the old version of
# trajfollower and adrive so that we can compare performance.
#

import os;
import sys;

# Load the library that starts up modules
sys.path.append('../../etc/system-tests')
import sysTestLaunch

# Specify the situation we want to run
scenario = 'UT_fwdbwd_sinewave'
rddf = 'stluke_sinewave.rddf'
rndf = ''
mdf = ''
obsFile = 'noObs.log'
sceneFunc = ''

# Define the applications that we want to run
apps = [ \
 ('asim',  '--no-pause --rddf=%(rddf)s' % locals(), 1, 'localhost'), \
 ('rddfplanner',  '--verbose=2', 1, 'localhost'), \
 ('trajfollower',  '--disable-trans', 1, 'localhost'), \
 ('planviewer',  '', 5, 'localhost'), \
 ('UT_fwdbwd',  'stluke_sinewave.rddf', 2, 'localhost'), \
]

# Launch everything
sysTestLaunch.runApps(apps, scenario, obsFile, rndf, mdf, sceneFunc, False)
