/* gccmdline.h */

/* File autogenerated by gengetopt version 2.16  */

#ifndef GCCMDLINE_H
#define GCCMDLINE_H

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef CMDLINE_PARSER_PACKAGE
#define CMDLINE_PARSER_PACKAGE "gcfollower"
#endif

#ifndef CMDLINE_PARSER_VERSION
#define CMDLINE_PARSER_VERSION "1.0"
#endif

struct gengetopt_args_info
{
  int nostate_flag;	/* continue even if state data is not received (default=off).  */
  int nodisp_flag;	/* do not run the sparrow display (default=off).  */
  int sim_flag;	/* don't send commands to VDrive (default=off).  */
  int sleep_arg;	/* specifies us sleep time in Active function (default 10000) (default='10000').  */
  char * sleep_orig;	/* specifies us sleep time in Active function (default 10000) original value given at command line.  */
  char * traj_arg;	/* use filename as the trajectory to follow (default='').  */
  char * traj_orig;	/* use filename as the trajectory to follow original value given at command line.  */
  char * file_arg;	/* use filename as where to save lindzey logs (default='').  */
  char * file_orig;	/* use filename as where to save lindzey logs original value given at command line.  */
  int log_level_arg;	/* set the log level for logging trajfollower directives and the output from the PID controller. logs will be saved in yyyy-mm-dd-ddd-hh-mm-trajfollower.log (default='0').  */
  char * log_level_orig;	/* set the log level for logging trajfollower directives and the output from the PID controller. logs will be saved in yyyy-mm-dd-ddd-hh-mm-trajfollower.log original value given at command line.  */
  double hacksteer_arg;	/* use cmd as steering command (normalized).  */
  char * hacksteer_orig;	/* use cmd as steering command (normalized) original value given at command line.  */
  int noDBS_flag;	/* Don't listen to DBS speed cap (default=off).  */
  char * log_arg;	/* log file prefix (default='').  */
  char * log_orig;	/* log file prefix original value given at command line.  */
  int use_local_flag;	/* use local coordinates (default=off).  */
  int wait_for_state_flag;	/* wait for state before starting (default=on).  */
  int disable_trans_flag;	/* disable transmission (default=off).  */
  int use_new_flag;	/* uses new algorithm, tested w/ graph-planner (default=off).  */
  int ff_only_flag;	/* only has an effect with --use-new (default=off).  */
  int use_lead_flag;	/* use info about leading vehicle (default=off).  */
  int log_logic_flag;	/* create a log containing arbiter logic states (needs a testlogs folder) (default=off).  */
  int log_errors_flag;	/* create a log containing perpendicular and heading errors (needs a test logs folder (default=off).  */
  char * argfile_arg;	/* file containing command line arguments.  */
  char * argfile_orig;	/* file containing command line arguments original value given at command line.  */
  int skynet_key_arg;	/* skynet key (default='0').  */
  char * skynet_key_orig;	/* skynet key original value given at command line.  */
  int disable_console_flag;	/* turn off sparrow display (default=off).  */
  int verbose_arg;	/* turn on verbose messages (default='1').  */
  char * verbose_orig;	/* turn on verbose messages original value given at command line.  */
  
  int help_given ;	/* Whether help was given.  */
  int version_given ;	/* Whether version was given.  */
  int nostate_given ;	/* Whether nostate was given.  */
  int nodisp_given ;	/* Whether nodisp was given.  */
  int sim_given ;	/* Whether sim was given.  */
  int sleep_given ;	/* Whether sleep was given.  */
  int traj_given ;	/* Whether traj was given.  */
  int file_given ;	/* Whether file was given.  */
  int log_level_given ;	/* Whether log-level was given.  */
  int hacksteer_given ;	/* Whether hacksteer was given.  */
  int noDBS_given ;	/* Whether noDBS was given.  */
  int log_given ;	/* Whether log was given.  */
  int use_local_given ;	/* Whether use-local was given.  */
  int wait_for_state_given ;	/* Whether wait-for-state was given.  */
  int disable_trans_given ;	/* Whether disable-trans was given.  */
  int use_new_given ;	/* Whether use-new was given.  */
  int ff_only_given ;	/* Whether ff-only was given.  */
  int use_lead_given ;	/* Whether use-lead was given.  */
  int log_logic_given ;	/* Whether log-logic was given.  */
  int log_errors_given ;	/* Whether log-errors was given.  */
  int argfile_given ;	/* Whether argfile was given.  */
  int skynet_key_given ;	/* Whether skynet-key was given.  */
  int disable_console_given ;	/* Whether disable-console was given.  */
  int verbose_given ;	/* Whether verbose was given.  */

} ;

int cmdline_parser (int argc, char * const *argv, struct gengetopt_args_info *args_info);
int cmdline_parser2 (int argc, char * const *argv, struct gengetopt_args_info *args_info, int override, int initialize, int check_required);
int cmdline_parser_file_save(const char *filename, struct gengetopt_args_info *args_info);

void cmdline_parser_print_help(void);
void cmdline_parser_print_version(void);

void cmdline_parser_init (struct gengetopt_args_info *args_info);
void cmdline_parser_free (struct gengetopt_args_info *args_info);

int cmdline_parser_configfile (char * const filename, struct gengetopt_args_info *args_info, int override, int initialize, int check_required);

int cmdline_parser_required (struct gengetopt_args_info *args_info, const char *prog_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* GCCMDLINE_H */
