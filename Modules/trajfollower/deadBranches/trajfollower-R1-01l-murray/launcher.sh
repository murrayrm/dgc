
platform="*linux"
#platform="*darwin"
declare -a apps
apps+=("asim --rddf=rddfplanner/UT_sinewave.rddf")
apps+=("rddfplanner --use-flat --use-endpoints --verbose=2")
#apps+=("trajfollower --gcdrive --log-level=8")
#apps+=("trajfollower --gcdrive")
#apps+=("trajfollower");
#apps+=("gcdrive --simulate --log-level=8")
#apps+=("gcdrive --simulate")
apps+=("planviewer")
apps+=("UT_fwdbwd rddfplanner/UT_sinewave.rddf")


for i in "${apps[@]}"; do
  appname=`echo $i | cut -f1 -d" "`
  echo appname = $appname
  appparams=`echo $i | cut -f1 -d" " --complement`
  app=`find . -wholename "$platform*$appname"`
  echo $app $appparams
  { xterm -e "$app $appparams" & } 
  sleep 5
done
