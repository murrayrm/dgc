#ifndef PSEUDOCON_HH
#define PSEUDOCON_HH

//#include "StateClient.h"
#include "skynet/sn_msg.hh"
#include "interfaces/sn_types.h"
#include "interface_superCon_trajF.hh"
#include "interfaces/ActuatorCommand.h"
#include "trajfollower/trajF_speedCap_cmd.hh"
#include <stdio.h>
#include <iostream>
#include <string.h>

#define ESTP_RUN 2
#define ESTP_PAUSE 1

#define GEAR_DRIVE 1
#define GEAR_REVERSE -1

using namespace std;

class StatePrinter 
{
public:
  /** Contstructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  StatePrinter(int skynetKey);

  /** Standard destructor */
  ~StatePrinter();

  /** The main loop where execution is trapped */
  void ActiveLoop();

private:

  skynet m_skynet;

  int receiveAdrivecmdSock;

};

#endif  // STATEPRINTER_HH
