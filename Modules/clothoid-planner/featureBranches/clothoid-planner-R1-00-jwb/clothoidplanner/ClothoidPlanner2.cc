/*
 * ClothoidPlanner2.cc
 * Updated Version of ClothoidPlanner that uses A* searching
 * Based on Code by Kenny Oslund, Rewritten by Joel Burdick and Noel duToit
 */


#include "ClothoidPlanner.hh"

/* -------------------------------------------------------------------------------------- */
/*                              Constructors and Initialization Funcions                  */
/* -------------------------------------------------------------------------------------- */
/* Main Constructor for ClothoidPlanner. */
CClothoidPlanner::CClothoidPlanner(CEnvironment *env, VehicleState *state)
{
        m_aliceEnv = env;             // get copy of Alice's environment variables
      	m_aliceState = state;         // Note that m_aliceState is a pointer in the
                                      // ClothoidPlanner class, and a variable in the
                                      //  CElementaryPath class
	m_searchTreeBase = NULL;      // set search tree initially to zero
	initialize();                 // initialize stuff, such as setting search tree to Null
         	/* build the clothoid database to use in future graph constructions */
	precomputeClothoids();        // Precompute a set of Clothoids
	cycle_count = 0;              // zero the number of planning cycles that have occured

	return;

}

/* Default Constructor for ClothoidPlanner */
CClothoidPlanner::~CClothoidPlanner()
{       /*  delete search tree if it already exits */
	if ( m_searchTreeBase != NULL)        
		delete m_searchTreeBase;
	for (int j = 0; j < CLOTHOID_DB_NUM_LENGTHS; j++)  % delete any existing clothoid database
		for (int i = 0; i < CLOTHOID_DB_INIT_CURV_IDX_BOUND; i++)
			for (int h = 0; h < CLOTHOID_DB_FINAL_CURV_IDX_BOUND; h++)
				if ( m_pathDB[j][i][h] != NULL )
					delete m_pathDB[j][i][h];
	
}

// initialize everything to an empty search tree- this is used on startup and
// if the actual position gets too far from the previously generated trajectory
void CClothoidPlanner::initialize()
{
	if ( m_searchTreeBase != NULL )         // delete any pre-existing search trees to free up memory
	  {
           cout << "initialize: deleting pre-existing clothoid tree and planning from scratch" << endl;
     	   delete m_searchTreeBase;
          }
	m_searchTreeBase = new CElementaryPath(m_aliceEnv);  // How does this use m_AliceEnv? ---JWB

	// discard whatever was previously found as the best path,
	// so that setStartPoint will use tplanner's initial conidtions
	updateStartCount = 0;
	cur_resolution = 0;
	// initialize the Clothoid Paths Graph
	setStartPoint();

}


/* -------------------------------------------------------------------------------------- */
/*                              Constructors and Initialization Funcions                  */
/* -------------------------------------------------------------------------------------- */

/* create a new start node and set it up correctly */
void CClothoidPlanner::setStartPoint()
{
	m_searchTreeBase->clearClothoid();                 % clear data in existing clothoid

	// if there was a previous solution, project tplanner's initial condition 
	// onto that and use that as the initial condition
        // CHECK THIS ---JWB
	int clothoidInd;
	CElementaryPath *curClothoid = getCurrentClothoid(clothoidInd);
	if (curClothoid != NULL)
	{
		double curv;
		pose2 closestPoint = curClothoid->getFirstPoint(&curv);
		m_searchTreeBase->setCurvLength(curv, curv, 0);
		m_searchTreeBase->appendPoint(closestPoint);
	}
	else   // if no clothoid exists, then reset the search tree
	{
		m_searchTreeBase->appendPoint(m_aliceEnv->getStartPoint());
		m_searchTreeBase->setCurvLength(0, 0, 0);
	}
		
	m_searchTreeBase->setCost(0);	// no previous nodes
}


/* function to determine which clothoid in the sequence of our clothoids we are currently
   along (after alice has progressed down the traj) */
CElementaryPath *CClothoidPlanner::getCurrentClothoid(int& index)
{
	// stub for now- say we're in the first clothoid...
	vector<CElementaryPath *> bestClothoids = getBestClothoidSequence();
	CElementaryPath *closestClothoid = NULL;
	int closestIndex, ind;
	double newdToClosest, dToClosest = 1000000;
	pose2 startPt = m_aliceEnv->getStartPoint();

	if (bestClothoids.empty())
	{
		closestIndex = 0;
		closestClothoid = NULL;
	}
	else
	{
		// iterate along the previous solution to find the point that we are closest to
		// (clothoid that we are in, and the index to that clothoid)
		for ( vector<CElementaryPath *>::iterator i = bestClothoids.begin();
			i != bestClothoids.end(); i++ )
		{
			// iterate over the point in the clothoid
			vector<pose2> clothoidPts = (*i)->getClothoidPoints();
			ind = 0;
			for ( vector<pose2>::iterator j = clothoidPts.begin();
				j != clothoidPts.end(); j++, ind++ )
			{
				newdToClosest = ((*j) - startPt).magnitude();
				if ( newdToClosest < dToClosest)
				{
					closestIndex = ind;
					dToClosest = newdToClosest;
					closestClothoid = (*i);
				}
			}
		}
	}

	index = closestIndex;
	return closestClothoid;
}

vector<CElementaryPath *> CClothoidPlanner::getBestClothoidSequence()
{
	CElementaryPath *m_endNode;
	vector<CElementaryPath *> bestPath, bestPathRev;

	// if we reached the goal, use the lowest cost goal node. otherwise
	// just plan to the node with the lowest cost estimate
	if ( m_bestFinish != NULL )	// solution to goal found
		m_endNode = m_bestFinish;
	else
		return bestPath;	// and empty path indicates failure

	// start at the endpoint that we've already determined is the best,
	// then just iterate from the end back to the beginning
	for ( CElementaryPath *node = m_endNode; node != NULL; 
			node = node->getSearchParent() )
		bestPathRev.push_back(node);

	// the last node in bestPath is our starting node, and the only point in its 
	// clothoid is also in the preceding node, so just delete it since its redundant
	bestPathRev.pop_back();

	// now the clothoids are in the opposite order they need to be put 
	// into the traj, so reverse the order
        if ( !bestPathRev.empty() )
                for (vector<CElementaryPath *>::reverse_iterator i = bestPathRev.rbegin(); 
                        i != bestPathRev.rend(); i++)
                {
			bestPath.push_back(*i);
                }
	return bestPath;
}
