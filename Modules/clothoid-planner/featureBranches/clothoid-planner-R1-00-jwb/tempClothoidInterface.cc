/*
 * tempClothoidInterface.cc
 * temporary interface functions for 
 * the new planner stack to run the clothoid planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 26 July 07- Kenny Oslund
	- Created file
 *
 */

#include "s1planner/tempClothoidInterface.hh"

/* Do everything needed to generate a single trajectory and then clean up. All needed memory
   is allocated and freed in this function */
Err_t GenerateTrajOneShot(int sn_key, bool disp_costmap, OCPparams ocpParams, 
	CPolytope* polyCorridor, BitmapParams bmparams, Path_t *path, double runtime)
{
	CEnvironment *problem = new CEnvironment(sn_key, disp_costmap, ocpParams, polyCorridor, bmparams);

	CClothoidPlanner *planner = new CClothoidPlanner(problem, NULL);

	planner->generatePaths(runtime);
	populatePath(planner, path);

	delete planner;

	return LP_OK;
}





void populatePath(CClothoidPlanner *planner, Path_t *path)
{
        // I'm guessing blindly about how to fill in this graphPath herehere....
	double pathLength;
	bool reaches_end;
        vector<pose2> solution = planner->getBestPath(&pathLength, &reaches_end);

        path->valid = true;
        path->collideObs = 0;
        path->collideCar = 0;
	path->goalDist = pathLength;
	path->pathLen = solution.size();
	
	int i = 0;
	for (vector<pose2>::iterator j = solution.begin(); 
		j != solution.end(); j++, i++)
	{
		// isn't this kind of memory allocation a horrible idea b/c of memory leaks?
		// however, I don't know of any other way to do it...
		path->path[i] = new GraphNode();
		(((path->path[i])->pose).pos).x = j->x;
		(((path->path[i])->pose).pos).y = j->y;
		(((path->path[i])->pose).pos).z = 0;
		// dude, wtf is a quaternion and why are we using it here?!?!
		//((path->path[i])->pose).rot = j->ang;
		// I'm just going to pretend the other 30 fields in GraphNode aren't 
		// actually that important...
	}
	return;

}




