/*
 *  Stage1Planner_Main.cpp
 *  Stage1Planner
 *
 *  Created by Noel duToit
 *  2/28/07
 */



#include "Stage1Planner.hh"

using namespace std;


// signal handling- taken from mplanner- I really have no clue how this actually works
volatile sig_atomic_t quit;
void sigintHandler(int /*sig*/)
{
  // if the user presses CTRL-C three or more times, and the program still
  // doesn't terminate, abort
  if (quit > 2) {
    abort();
  }
  quit++;
}

int main(int argc, char** const argv)
{ 
  // catch CTRL-C and exit cleanly, if possible
  signal(SIGINT, sigintHandler);
  // and SIGTERM, i.e. when someone types "kill <pid>" or the like
  signal(SIGTERM, sigintHandler);


  cout << "Stage 1 Planner is starting..." << endl;
  cout << "Parsing command line..." << endl;

  gengetopt_args_info cmdline;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
    exit (1);


  int SkynetKey   = skynet_findkey(argc, argv);
  bool WAIT_STATE =  true;
  
  
  /*! Create a instance of dplanner */

    CStage1Planner* Stage1Planner = new CStage1Planner(SkynetKey, WAIT_STATE, cmdline);

  

  cout << "entering activeloop" << endl;
  Stage1Planner->ActiveLoop();

  cout << "Stage 1 Planner is ending." << endl;
  delete Stage1Planner;
  
  /* Restore memory resources */
//  delete Stage1Planner;
//  delete ClothoidPlanner;
  
  return (int) 0;
} 
