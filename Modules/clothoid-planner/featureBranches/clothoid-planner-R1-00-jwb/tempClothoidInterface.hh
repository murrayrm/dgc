/*
 * tempClothoidInterface.hh
 * header file for the temporary interface functions for 
 * the new planner stack to run the clothoid planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 26 July 07- Kenny Oslund
	- Created file
 *
 */

#ifndef TEMP_PLANNNER_INTERFACES_HH
#define TEMP_PLANNNER_INTERFACES_HH

#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "s1planner/ClothoidPlanner.hh"
#include "s1planner/Environment.hh"


/* Do everything needed to generate a single trajectory and then clean up. All needed memory
   is allocated and freed in this function */
Err_t GenerateTrajOneShot(int sn_key, bool disp_costmap, OCPparams ocpParams,
        CPolytope* polyCorridor, BitmapParams bmparam, Path_t *path, double runtime);

/* convert the path generated by CClothoidPlanner to a form that can be used by the planner */
void populatePath(CClothoidPlanner *planner, Path_t *path);

#endif
