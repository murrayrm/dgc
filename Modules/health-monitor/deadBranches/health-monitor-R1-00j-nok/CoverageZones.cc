/*!
 * File: CoverageZones.cc
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be an take care of caculating the 
 *      coverage levels of the zones around Alice.
 *
 */

#include <iostream>
#include <string>
#include <assert.h>
#include <set>
#include <vector>

#include "CoverageZones.hh"
#include "AliceSensor.hh"

using namespace std;

CoverageZones::CoverageZones(vector<AliceSensor> &sensorList)
{
  // iterate through the list of sensors, loading their pointers into the map with their ids (or names) as keys
  for(int i = 0; i < (int)sensorList.size(); i++)  // int cast to avoid the warning about it being unsigned
  {
    string name = sensorList[i].getComponentName();
    AliceSensor* address = &(sensorList[i]);
    sensors[name] = address;
  }
  assert(sensors.size() == sensorList.size());
  
  LeftCoverageLevel = 0.01;  // 0.01 to avoid divide by zero errors
  RightCoverageLevel = 0.01;
  CenterNearCoverageLevel = 0.01;
  CenterFarCoverageLevel = 0.01;
  RearCoverageLevel = 0.01;
}

void CoverageZones::updateCoverageLevels() 
{
  updateRearCoverageLevel();
  updateLeftCoverageLevel();
  updateRightCoverageLevel();
  updateCenterNearCoverageLevel();
  updateCenterFarCoverageLevel();
}

void CoverageZones::updateRearCoverageLevel() // levels possible: 1, 0.65, 0.6, 0.1, 0
{
  // one time gets so sensors status doesn't change due to time outs
  // while this function is running
  int LFB = sensors["MODladarFeederLFBumper"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorLeft"]->getComponentHealthLevel() < LFB)
    LFB = sensors["MODladarCarPerceptorLeft"]->getComponentHealthLevel();

  int RFB = sensors["MODladarFeederRFBumper"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorRight"]->getComponentHealthLevel() < RFB)
    RFB = sensors["MODladarCarPerceptorRight"]->getComponentHealthLevel();

  int LFR = sensors["MODladarFeederLFRoof"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorRoofLeft"]->getComponentHealthLevel() < LFR)
    LFR = sensors["MODladarCarPerceptorRoofLeft"]->getComponentHealthLevel();

  int RFR = sensors["MODladarFeederRFRoof"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorRoofRight"]->getComponentHealthLevel() < RFR)
    RFR = sensors["MODladarCarPerceptorRoofRight"]->getComponentHealthLevel();

  int MRB = sensors["MODladarFeederMRBumper"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorRear"]->getComponentHealthLevel() < MRB)
    MRB = sensors["MODladarCarPerceptorRear"]->getComponentHealthLevel();
  
  // If rear ladar is good that is all we need:
  if (MRB == GOOD) 
  {
    RearCoverageLevel = 1;
    return;
  }

  // All side ladars good (leaves only a thin vehicle width strip 
  // uncovered directly behind Alice):
  if (((LFB == GOOD) && (LFR == GOOD)) && ((RFB == GOOD) && (RFR == GOOD)))
  {
    RearCoverageLevel = 0.65;
    return;
  }

  // At least one ladar good on each side on:
  if (((LFB == GOOD) || (LFR == GOOD)) && ((RFB == GOOD) || (RFR == GOOD)))
  {
    RearCoverageLevel = 0.60;
    return;
  }

  // At least one of the side ladars good: (roughly half of rear zone is covered)
  if (((LFB == GOOD) || (LFR == GOOD)) || ((RFB == GOOD) || (RFR == GOOD)))
  {
    RearCoverageLevel = 0.1;
    return;
  }

  // Nothing is being sensed behind Alice at all:
  RearCoverageLevel = 0.01;
}

void CoverageZones::updateLeftCoverageLevel() // levels possible: 1, 0.95, 0.85, 0.5, 0
{
  // one time gets so sensors status doesn't change due to time outs
  // while this function is running
  int LFB = sensors["MODladarFeederLFBumper"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorLeft"]->getComponentHealthLevel() < LFB)
    LFB = sensors["MODladarCarPerceptorLeft"]->getComponentHealthLevel();

  int LFR = sensors["MODladarFeederLFRoof"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorRoofLeft"]->getComponentHealthLevel() < LFR)
    LFR = sensors["MODladarCarPerceptorRoofLeft"]->getComponentHealthLevel();

  int MFB = sensors["MODladarFeederMFBumper"]->getComponentHealthLevel();   
  if (sensors["MODladarCarPerceptorCenter"]->getComponentHealthLevel() < MFB)
    MFB = sensors["MODladarCarPerceptorCenter"]->getComponentHealthLevel();
  
  // If both of the Left facing ladars is good thats all we need:
  if ((LFB == GOOD) && (LFR == GOOD)) 
  {
    LeftCoverageLevel = 1;
    return;
  }

  // If one of the Left facing ladars is good and the middle front is good:
  if (((LFB == GOOD) || (LFR == GOOD)) && (MFB == GOOD)) 
  {
    LeftCoverageLevel = 0.95;
    return;
  }

  // If only of the Left facing ladars is good and the middle front is good:
  if ((LFB == GOOD) || (LFR == GOOD)) 
  {
    LeftCoverageLevel = 0.85;
    return;
  }
  
  // If only forward facing bumper Ladar is good it adds a bit of Left coverage
  if (MFB == GOOD)
  {
    LeftCoverageLevel = 0.50;
    return;
  }

  // Nothing is being sensed in this zone:
  LeftCoverageLevel = 0.01;
}

void CoverageZones::updateRightCoverageLevel() // levels possible: 1, 0.95, 0.85, 0.5, 0.01
{

  // one time gets so sensors status doesn't change due to time outs
  // while this function is running
  int RFB = sensors["MODladarFeederRFBumper"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorRight"]->getComponentHealthLevel() < RFB)
    RFB = sensors["MODladarCarPerceptorRight"]->getComponentHealthLevel();

  int RFR = sensors["MODladarFeederRFRoof"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorRoofRight"]->getComponentHealthLevel() < RFR)
    RFR = sensors["MODladarCarPerceptorRoofRight"]->getComponentHealthLevel();

  int MFB = sensors["MODladarFeederMFBumper"]->getComponentHealthLevel();   
  if (sensors["MODladarCarPerceptorCenter"]->getComponentHealthLevel() < MFB)
    MFB = sensors["MODladarCarPerceptorCenter"]->getComponentHealthLevel();
  
  // If both of the Right facing ladars is good thats all we need:
  if ((RFB == GOOD) && (RFR == GOOD)) 
  {
    RightCoverageLevel = 1;
    return;
  }

  // If one of the Right facing ladars is good and the middle front is good:
  if (((RFB == GOOD) || (RFR == GOOD)) && (MFB == GOOD)) 
  {
    RightCoverageLevel = 0.95;
    return;
  }

  // If only of the Right facing ladars is good and the middle front is good:
  if ((RFB == GOOD) || (RFR == GOOD)) 
  {
    RightCoverageLevel = 0.85;
    return;
  }
  
  // If only forward facing bumper Ladar is good it adds a bit of Right coverage
  if (MFB == GOOD)
  {
    RightCoverageLevel = 0.50;
    return;
  }

  // Nothing is being sensed in this zone:
  RightCoverageLevel = 0.01;
}

void CoverageZones::updateCenterNearCoverageLevel()  // levels possible: 1, 0.80, 0.70, 0.60, 0.50, 0.40, 0.35, 0.30, 0.25, 0.2, 0.15, 0.10, 0.01
{
  // one time gets so sensors status doesn't change due to time outs
  // while this function is running
  int LFB = sensors["MODladarFeederLFBumper"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorLeft"]->getComponentHealthLevel() < LFB)
    LFB = sensors["MODladarCarPerceptorLeft"]->getComponentHealthLevel();

  int RFB = sensors["MODladarFeederRFBumper"]->getComponentHealthLevel(); 
  if (sensors["MODladarCarPerceptorRight"]->getComponentHealthLevel() < RFB)
    RFB = sensors["MODladarCarPerceptorRight"]->getComponentHealthLevel();

  int LFR = sensors["MODladarFeederLFRoof"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorRoofLeft"]->getComponentHealthLevel() < LFR)
    LFR = sensors["MODladarCarPerceptorRoofLeft"]->getComponentHealthLevel();

  int RFR = sensors["MODladarFeederRFRoof"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorRoofRight"]->getComponentHealthLevel() < RFR)
    RFR = sensors["MODladarCarPerceptorRoofRight"]->getComponentHealthLevel();

  int MFB = sensors["MODladarFeederMFBumper"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorCenter"]->getComponentHealthLevel() < MFB)
    MFB = sensors["MODladarCarPerceptorCenter"]->getComponentHealthLevel();

  int SLFS = sensors["MODstereoFeederLFShort"]->getComponentHealthLevel();

  int SRFS = sensors["MODstereoFeederRFShort"]->getComponentHealthLevel();
  
  // If the main 3 for the front are ok:
  if ((MFB == GOOD) && ((SLFS == GOOD) && (SRFS == GOOD))) 
  {
    CenterNearCoverageLevel = 1;
    return;
  }

  // At least one ladar good on each side and cameras ok:
  if ((((LFB == GOOD) || (LFR == GOOD)) && ((RFB == GOOD) || (RFR == GOOD))) && ((SLFS == GOOD) && (SRFS == GOOD)))
  {
    CenterNearCoverageLevel = 0.80;
    return;
  }

  // MFB good and left camera good:
  if ((MFB == GOOD) && (SLFS == GOOD))
  {
    CenterNearCoverageLevel = 0.70;
    return;
  }

  // MFB good and right camera good:
  if ((MFB == GOOD) && (SRFS == GOOD))
  {
    CenterNearCoverageLevel = 0.60;
    return;
  }

  // At least one ladar good on each side left camera ok:
  if ((((LFB == GOOD) || (LFR == GOOD)) && ((RFB == GOOD) || (RFR == GOOD))) && (SLFS == GOOD))
  {
    CenterNearCoverageLevel = 0.60;
    return;
  }

  // At least one ladar good on each side right camera ok:
  if ((((LFB == GOOD) || (LFR == GOOD)) && ((RFB == GOOD) || (RFR == GOOD))) && (SRFS == GOOD))
  {
    CenterNearCoverageLevel = 0.50;
    return;
  }    

  // At least one side ladar good and cameras good:
  if ((((LFB == GOOD) || (LFR == GOOD)) || ((RFB == GOOD) || (RFR == GOOD))) && ((SLFS == GOOD) && (SRFS == GOOD)))
  {
    CenterNearCoverageLevel = 0.40;
    return;
  }

  // At least one side ladar good and left camera good:
  if ((((LFB == GOOD) || (LFR == GOOD)) || ((RFB == GOOD) || (RFR == GOOD))) && (SLFS == GOOD))
  {
    CenterNearCoverageLevel = 0.35;
    return;
  }

  // At least one side ladar good and right camera good:
  if ((((LFB == GOOD) || (LFR == GOOD)) || ((RFB == GOOD) || (RFR == GOOD))) && (SRFS == GOOD))
  {
    CenterNearCoverageLevel = 0.30;
    return;
  }
  
  // Both cameras out ladar good on each side and mfb ok:
  if (((LFB == GOOD) || (LFR == GOOD)) && ((RFB == GOOD) || (RFR == GOOD)) && (MFB == GOOD))
  {
    CenterNearCoverageLevel = 0.25;
    return;
  }

  // MFB is ok:
  if (MFB == GOOD)
  {
    CenterNearCoverageLevel = 0.25;
    return;
  }

  // only the cameras are good
  if ((SLFS == GOOD) && (SRFS == GOOD))
  {
    CenterNearCoverageLevel = 0.20;
    return;
  }

  // At least one ladar on each side good, cameras out, MFB out:
  if (((LFB == GOOD) || (LFR == GOOD)) && ((RFB == GOOD) || (RFR == GOOD)))
  {
    CenterNearCoverageLevel = 0.15;
    return;
  }

  // only the left camera good
  if (SLFS == GOOD)
  {
    CenterNearCoverageLevel = 0.15;
    return;
  }

  // only the right camera good
  if (SRFS == GOOD)
  {
    CenterNearCoverageLevel = 0.10;
    return;
  }

  // At least one side ladar good all others out:
  if (((LFB == GOOD) || (LFR == GOOD)) || ((RFB == GOOD) || (RFR == GOOD)))
  {
    CenterNearCoverageLevel = 0.05;
    return;
  }
 
  // Nothing is covering this zone:
  CenterNearCoverageLevel = 0.01;
}

void CoverageZones::updateCenterFarCoverageLevel()  // levels possible: 1, 0.90, 0.75, 0.70, 0.65, 0.60, 0.50, 0.45, 0.40, 0.35, 0.30, 0.25, 0.20, 0.1, 0.01
{
  // one time gets so sensors status doesn't change due to time outs
  // while this function is running
  int MFB = sensors["MODladarFeederMFBumper"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorCenter"]->getComponentHealthLevel() < MFB)
    MFB = sensors["MODladarCarPerceptorCenter"]->getComponentHealthLevel();

  int SMFM = sensors["MODstereoFeederMFMedium"]->getComponentHealthLevel();
  if (sensors["MODstereoObsPerceptorMedium"]->getComponentHealthLevel() < SMFM)
    SMFM = sensors["MODstereoObsPerceptorMedium"]->getComponentHealthLevel();

  int SMFL = sensors["MODstereoFeederMFLong"]->getComponentHealthLevel();
  if (sensors["MODstereoObsPerceptorLong"]->getComponentHealthLevel() < SMFL)
    SMFL = sensors["MODstereoObsPerceptorLong"]->getComponentHealthLevel();

  int RIEGL = sensors["MODladarFeederRiegl"]->getComponentHealthLevel();
  if (sensors["MODladarCarPerceptorRiegl"]->getComponentHealthLevel() < RIEGL)
    RIEGL = sensors["MODladarCarPerceptorRiegl"]->getComponentHealthLevel();

  int RADAR = sensors["MODradarFeeder"]->getComponentHealthLevel();
  if (sensors["MODradarObsPerceptor"]->getComponentHealthLevel() < RADAR)
    RADAR = sensors["MODradarObsPerceptor"]->getComponentHealthLevel();
  
  // If the main 3 for the front are ok:
  if ((RADAR == GOOD) && ((RIEGL == GOOD) && (SMFL == GOOD))) 
  {
    CenterFarCoverageLevel = 1;
    return;
  }

  // Only Long range sterio out:
  if ((RADAR == GOOD) && (RIEGL == GOOD) && (SMFM == GOOD))
  {
    CenterFarCoverageLevel = 0.90;
    return;
  }

  // Only Long range and medium range sterio out:
  if ((RADAR == GOOD) && (RIEGL == GOOD))
  {
    CenterFarCoverageLevel = 0.75;
    return;
  }

  // Only Riegl out:
  if ((RADAR == GOOD) && (SMFL == GOOD) && (MFB == GOOD))
  {
    CenterFarCoverageLevel = 0.70;
    return;
  }
  
  // Only Radar out:
  if ((SMFL == GOOD) && (RIEGL == GOOD))
  {
    CenterFarCoverageLevel = 0.65;
    return;
  }
  
  // Only Riegl and long range out  out:
  if ((RADAR == GOOD) && (SMFM == GOOD) && (MFB == GOOD))
  {
    CenterFarCoverageLevel = 0.60;
    return;
  }

  // Only Radar and long range out:
  if ((SMFM == GOOD) && (RIEGL == GOOD))
  {
    CenterFarCoverageLevel = 0.60;
    return;
  }

  // Only Radar and long range sterio out:
  if ((SMFM == GOOD) && (RIEGL == GOOD))
  {
    CenterFarCoverageLevel = 0.6;
    return;
  }

  // Only Riegl and MFB out:
  if ((RADAR == GOOD) && (SMFL == GOOD))
  {
    CenterFarCoverageLevel = 0.5;
    return;
  }

  // Only Radar and long range sterio out:
  if (RIEGL == GOOD)
  {
    CenterFarCoverageLevel = 0.5;
    return;
  }

  // Riegl, MFB, and long range out:
  if ((RADAR == GOOD) && (SMFM == GOOD))
  {
    CenterFarCoverageLevel = 0.45;
    return;
  }

  // Only MFB and Radar good:
  if ((RADAR == GOOD) && (MFB == GOOD))
  {
    CenterFarCoverageLevel = 0.4;
    return;
  }

  // Only long range and MFB good:
  if ((SMFL == GOOD) && (MFB == GOOD))
  {
    CenterFarCoverageLevel = 0.35;
    return;
  }

  // Radar regil and MFB out:
  if ((SMFM == GOOD) && (SMFL == GOOD))
  {
    CenterFarCoverageLevel = 0.3;
    return;
  }


  // Only med range and MFB good:
  if ((SMFM == GOOD) && (MFB == GOOD))
  {
    CenterFarCoverageLevel = 0.3;
    return;
  }

  // Only Radar good:
  if (RADAR == GOOD)
  {
    CenterFarCoverageLevel = 0.3;
    return;
  }

  // Only Radar and regil out:
  if (SMFL == GOOD)
  {
    CenterFarCoverageLevel = 0.25;
    return;
  }

  // Only Radar and regil out:
  if (SMFM == GOOD)
  {
    CenterFarCoverageLevel = 0.2;
    return;
  }

  // Only MFB good:
  if (MFB == GOOD)
  {
    CenterFarCoverageLevel = 0.1;
    return;
  }

  // Nothing is covering this zone:
  CenterFarCoverageLevel = 0.01;
}

CoverageZones::~CoverageZones()
{
  sensors.clear();
}



