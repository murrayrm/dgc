/*!
 * File: AliceCapability.hh
 * 
 * Author: Chris Schantz
 * Date: August 2nd 2007
 *
 * Description: This class is ment ttake care of the determination logic
 *              and broadcasting using the skynet talker.
 *
 */

#ifndef AliceCapability_HH
#define AliceCapability_HH

#include <skynettalker/SkynetTalker.hh>
#include <interfaces/sn_types.h>
#include <interfaces/ActuatorState.h>
#include <interfaces/VehicleCapability.h>

#include "AliceActuator.hh"
#include "CoverageZones.hh"
#include "AliceApplanix.hh"

class AliceCapability {

  // Actuator information
  AliceActuator* actuator;
  
  // Sensor information
  CoverageZones* zones;

  //Applanix Information
  AliceApplanix* applanix;

  // Used to broadcast
  SkynetTalker<VehicleCapability>* talker;

  // The message to send
  VehicleCapability* results;

  
public:
  
  // messageTimeOut in micro seconds
  AliceCapability(AliceActuator* actuatorPtr, CoverageZones* zonesPtr, AliceApplanix* gps, SkynetTalker<VehicleCapability>* talkerPtr);

  ~AliceCapability();

  void update();
  void broadcast() const;

  void setNominalDrivingCap(double nominalDrivingCap) {results->nominalDriving = nominalDrivingCap;}

  //Accessors
  float getLeftTurnCap() {return results->intersectionLeftTurn; }
  float getRightTurnCap() {return results->intersectionRightTurn; }
  float getStraightTurnCap() {return results->intersectionStraightTurn; }
  float getUTurnCap() {return results->uturn;}
  float getNominalDrivingCap() {return results->nominalDriving; }
  float getNominalStoppingCap() {return results->nominalStopping; }
  float getNominalZoneCap() {return results->nominalZoneRegionDriving; }
  float getNominalNewCap() {return results->nominalNewRegionDriving; }
};

#endif //AliceCapability_HH
