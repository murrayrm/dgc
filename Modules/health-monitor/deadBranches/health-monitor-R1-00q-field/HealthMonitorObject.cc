/* 
 * Object responsible for Health Monitor's communications inputs
 * Date: 20 July 2007
 * Author: Chris Schantz
 * 
 */


#include "HealthMonitorObject.hh"
#include <interfaces/sn_types.h>

bool quit = false;

// Default constructor
HealthMonitorObject::HealthMonitorObject(char *spreadDaemon, int skynetKey, gengetopt_args_info &options)
    : CSkynetContainer(MODhealthMonitor, skynetKey),
      CStateClient(true)
{
  MSG("In HealthMonitorObject constructor, CStateClient connection estalished");
  
  console = 0;

  if (initSensnet(spreadDaemon, skynetKey) != 0)
  {
    MSG("initalizing sensnet failed");
    return;
  }

  MSG("Sensnet connection established");

  //Skynet Talker 
  outTalker = new SkynetTalker<VehicleCapability>(skynetKey, SNvehicleCapability, MODhealthMonitor);

  astateHealthTalker = new SkynetTalker<int>(skynetKey, SNastateHealth, MODmissionplanner);

  cout << "outTalker address: " << outTalker << endl;

  cout << "skynetKey: " << skynetKey << endl;

  MSG("Skynettalker connection established");

  // populate our sensors  
  initSensors(options);  

  if (options.listen_to_mapper_given)
    listenToMapper = true;
  else
    listenToMapper = false;

  safeMode = options.safe_mode_given;

  MSG("Sensors Initialized");

  /*
  for (int i = 0; i < 12; i ++)
  {
    cout << "Sensor number " << i << " consideration value: " << sensors[i].getConsiderationValue() << endl;
  }
  */

  // initalize our CoverageZones object
  zones = new CoverageZones(sensors);

  MSG("CoverageZones Initialized");

  actuators = new AliceActuator(&m_actuatorState);

  MSG("Actuator Object Initalized");

  applanix = new AliceApplanix(astateHealthTalker);

  MSG("Applanix Object Initalized");

  capabilities = new AliceCapability(actuators, zones, applanix, outTalker);

  MSG("Capabilities Object Initalized");

  if (!options.disable_console_flag)
  {
    initConsole(skynetKey);
  }

  MSG("Console Initalized");

  return;
}

// Default destructor - close out our sensnet subscriptions
HealthMonitorObject::~HealthMonitorObject()
{
  if (console)
  {
    cotk_close(console);
    cotk_free(console);
    console = NULL;
  }
  finiSensnet();
  return;
}


// Initialize sensnet
int HealthMonitorObject::initSensnet(char *spreadDaemon, int skynetKey)
{
  MSG("in initalizing sensnet function");
  // Initialize SensNet
  sensnet = sensnet_alloc();
  MSG("sensnet handle allocated");
  if (sensnet_connect(sensnet, spreadDaemon, skynetKey, MODhealthMonitor) != 0)
  {
    return ERROR("unable to connect to sensnet");
  }
  MSG("initalizing sensnet done");
  return 0;    
}

void HealthMonitorObject::initSensors(gengetopt_args_info &options) //Hard Coded sensor oder, represents full suite
{
  MSG("Initializing Sensors");
  sensors.clear();

  sensors.push_back(AliceSensor("MODladarFeederLFBumper", modulenamefromString("MODladarFeederLFBumper"), sensnet, sensorPresentInList("MODladarFeederLFBumper", options)));
  sensors.push_back(AliceSensor("MODladarFeederMFBumper", modulenamefromString("MODladarFeederMFBumper"), sensnet, sensorPresentInList("MODladarFeederMFBumper", options)));
  sensors.push_back(AliceSensor("MODladarFeederRFBumper", modulenamefromString("MODladarFeederRFBumper"), sensnet, sensorPresentInList("MODladarFeederRFBumper", options))); 
  sensors.push_back(AliceSensor("MODladarFeederRearBumper", modulenamefromString("MODladarFeederRearBumper"), sensnet, sensorPresentInList("MODladarFeederRearBumper", options)));
  sensors.push_back(AliceSensor("MODladarFeederLFRoof", modulenamefromString("MODladarFeederLFRoof"), sensnet, sensorPresentInList("MODladarFeederLFRoof", options)));
  sensors.push_back(AliceSensor("MODladarFeederRFRoof", modulenamefromString("MODladarFeederRFRoof"), sensnet, sensorPresentInList("MODladarFeederRFRoof", options)));
  sensors.push_back(AliceSensor("MODladarFeederRiegl", modulenamefromString("MODladarFeederRiegl"), sensnet, sensorPresentInList("MODladarFeederRiegl", options)));
  sensors.push_back(AliceSensor("MODstereoFeederMFLong", modulenamefromString("MODstereoFeederMFLong"), sensnet, sensorPresentInList("MODstereoFeederMFLong", options)));
  sensors.push_back(AliceSensor("MODstereoFeederMFMedium", modulenamefromString("MODstereoFeederMFMedium"), sensnet, sensorPresentInList("MODstereoFeederMFMedium", options)));
  sensors.push_back(AliceSensor("MODstereoFeederLFShort", modulenamefromString("MODstereoFeederLFShort"), sensnet, sensorPresentInList("MODstereoFeederLFShort", options)));
  sensors.push_back(AliceSensor("MODstereoFeederRFShort", modulenamefromString("MODstereoFeederRFShort"), sensnet, sensorPresentInList("MODstereoFeederRFShort", options)));
  sensors.push_back(AliceSensor("MODradarFeeder", modulenamefromString("MODradarFeeder"), sensnet, sensorPresentInList("MODradarFeeder", options)));
  
  // Adding perceptors to health monitor here.
  sensors.push_back(AliceSensor("MODladarCarPerceptorLeft", modulenamefromString("MODladarCarPerceptorLeft"), sensnet, sensorPresentInList("MODladarCarPerceptorLeft", options)));
  sensors.push_back(AliceSensor("MODladarCarPerceptorCenter", modulenamefromString("MODladarCarPerceptorCenter"), sensnet, sensorPresentInList("MODladarCarPerceptorCenter", options)));
  sensors.push_back(AliceSensor("MODladarCarPerceptorRight", modulenamefromString("MODladarCarPerceptorRight"), sensnet, sensorPresentInList("MODladarCarPerceptorRight", options)));
  sensors.push_back(AliceSensor("MODladarCarPerceptorRear", modulenamefromString("MODladarCarPerceptorRear"), sensnet, sensorPresentInList("MODladarCarPerceptorRear", options)));
  sensors.push_back(AliceSensor("MODladarCarPerceptorRoofLeft", modulenamefromString("MODladarCarPerceptorRoofLeft"), sensnet, sensorPresentInList("MODladarCarPerceptorRoofLeft", options)));
  sensors.push_back(AliceSensor("MODladarCarPerceptorRoofRight", modulenamefromString("MODladarCarPerceptorRoofRight"), sensnet, sensorPresentInList("MODladarCarPerceptorRoofRight", options)));
  sensors.push_back(AliceSensor("MODladarCarPerceptorRiegl", modulenamefromString("MODladarCarPerceptorRiegl"), sensnet, sensorPresentInList("MODladarCarPerceptorRiegl", options)));
  sensors.push_back(AliceSensor("MODladarCurbPerceptor", modulenamefromString("MODladarCurbPerceptor"), sensnet, sensorPresentInList("MODladarCurbPerceptor", options)));
  sensors.push_back(AliceSensor("MODradarObsPerceptor", modulenamefromString("MODradarObsPerceptor"), sensnet, sensorPresentInList("MODradarObsPerceptor", options)));
  sensors.push_back(AliceSensor("MODstereoObsPerceptorMedium", modulenamefromString("MODstereoObsPerceptorMedium"), sensnet, sensorPresentInList("MODstereoObsPerceptorMedium", options)));
  sensors.push_back(AliceSensor("MODstereoObsPerceptorLong", modulenamefromString("MODstereoObsPerceptorLong"), sensnet, sensorPresentInList("MODstereoObsPerceptorLong", options)));
  sensors.push_back(AliceSensor("MODlinePerceptorRFShort", modulenamefromString("MODlinePerceptorRFShort"), sensnet, sensorPresentInList("MODlinePerceptorRFShort", options)));
  sensors.push_back(AliceSensor("MODlinePerceptorLFShort", modulenamefromString("MODlinePerceptorLFShort"), sensnet, sensorPresentInList("MODlinePerceptorLFShort", options)));
  sensors.push_back(AliceSensor("MODlinePerceptorMFMedium", modulenamefromString("MODlinePerceptorMFMedium"), sensnet, sensorPresentInList("MODlinePerceptorMFMedium", options)));
  sensors.push_back(AliceSensor("MODlinePerceptorMFLong", modulenamefromString("MODlinePerceptorMFLong"), sensnet, sensorPresentInList("MODlinePerceptorMFLong", options)));
  sensors.push_back(AliceSensor("MODroadPerceptor", modulenamefromString("MODroadPerceptor"), sensnet, sensorPresentInList("MODroadPerceptor", options)));

  // ladarObsPerceptor
  sensors.push_back(AliceSensor("MODladarObsPerceptor", modulenamefromString("MODladarObsPerceptor"), sensnet, sensorPresentInList("MODladarObsPerceptor", options)));

  // Mapper
  sensors.push_back(AliceSensor("MODmapping", MODmapping, sensnet, options.listen_to_mapper_given));
}

bool HealthMonitorObject::sensorPresentInList(char* sensorName, gengetopt_args_info &options)
{
  string temp = sensorName;
  for (int i = 0; i < (int)options.sensor_id_given; i++)
  {
    if (temp == (string)options.sensor_id_arg[i])
    {
      cout << temp << " is in the list****************" << endl;
      return true;
      
    }
  }
  cout << temp << " is not in the list=================" << endl;
  return false;
  
}    

// Finalize sensnet
int HealthMonitorObject::finiSensnet()
{  
  sensnet_disconnect(sensnet);
  sensnet_free(sensnet);
  sensnet = NULL;
  return 0;
}


// Update hardware components
int HealthMonitorObject::updateComponents()
{
  
  int status = sensnet_wait(sensnet, 1000);  //miliseconds- will wait for up to .1 seconds to get a message here
  if (status != 0 && status != ETIMEDOUT)
    return ERROR("wait failed %s", strerror(status));
  
  cout << "updating components" << endl;
  
  UpdateActuatorState();

  applanix->update();
  
  for (int i = 0; i < (int)sensors.size(); i++)
  {
    sensors[i].update();
  }

  return 0;
}



void HealthMonitorObject::initConsole(int skynetKey)
{
  MSG("In initializing Console Function");
  const char *layout =
    "-------------------------------------------------------------------------------\n"
    "|HealthMonitor                                 Skynet Key:    %_skynet_key_%  |\n"
    "------------------------------------------------------------------------------|\n"
    "|Sensors:         Health Status     #Msgs            Health Status     #Msgs  |\n"
    "|   %LFBumper_L%    %LFB%                 %Riegl_L%    %Rgl%                  |\n"
    "|   %MFBumper_L%    %MFB%                 %MFLong_c%   %MFL%                  |\n"
    "|   %RFBumper_L%    %RFB%                 %MFMed_c%    %MFM%                  |\n"
    "|   %MRBumper_L%    %MRB%                 %LFShort_c%  %LFS%                  |\n"
    "|   %LFRoof_L%      %LFR%                 %RFShort_c%  %RFS%                  |\n"
    "|   %RFRoof_L%      %RFR%                 %Rad%        %rad%                  |\n"
    "|Coverage Levels:                     Actuators:                              |\n"
    "|   Center Near: %_centerNear_%           Throttle:    %_throttle_%           |\n"
    "|   Center Far:  %_centerFar_%            Brake:       %_brake_%              |\n"
    "|   Left Zone:   %_leftZone_%             Steering:    %_steering_%           |\n"
    "|   Right Zone:  %_rightZone_%            Transmiss:   %_transmission_%       |\n"  
    "|   Rear Zone:   %_rearZone_%             Applanix:    %_applanix_%           |\n"
    "|Vehicle Capabilities:                                                        |\n"
    "|   Left-turn:     %_capLeft_%         Nominal Driving:  %_capDriving_%       |\n"
    "|   Right-turn:    %_capRight_%        Nominal Stopping: %_capStopping_%      |\n"
    "|   Straight-turn: %_capStraight_%     Nominal Zone:     %_capZone_%          |\n"
    "|   U-turn:        %_capUturn_%        Nominal New:      %_capNew_%           |\n"
    "-----------------------------------[%QUIT%]------------------------------------\n";
                                                 

  char temp[8192];
    
  // Initialize the console template
  memset(temp, 0, sizeof(temp)); 
  strcpy(temp, layout);

  // Initialize console
  console = cotk_alloc();
  assert(console);

  MSG("Console allocated");

  // Set the console template
  cotk_bind_template(console, temp);

  MSG("Console template bound");

  // Bind Concideration toggles
  cotk_bind_toggle(console, "%LFBumper_L%", "LFBumper_Ladar:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_LFBtoggle, this);
  
  cotk_bind_toggle(console, "%MFBumper_L%", "MFBumper_Ladar:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_MFBtoggle, this);
  
  cotk_bind_toggle(console, "%RFBumper_L%", "RFBumper_Ladar:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_RFBtoggle, this);
  
  cotk_bind_toggle(console, "%MRBumper_L%", "MRBumper_Ladar:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_MRBtoggle, this);
  
  cotk_bind_toggle(console, "%LFRoof_L%", "LFRoof_Ladar:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_LFRtoggle, this);
  
  cotk_bind_toggle(console, "%RFRoof_L%", "RFRoof_Ladar:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_RFRtoggle, this);

  cotk_bind_toggle(console, "%Riegl_L%", "Riegl_Ladar:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_Rgltoggle, this);

  cotk_bind_toggle(console, "%MFLong_c%", "MFLong_cam:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_MFLtoggle, this);

  cotk_bind_toggle(console, "%MFMed_c%", "MFMed_cam:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_MFMtoggle, this);

  cotk_bind_toggle(console, "%LFShort_c%", "LFShort_cam:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_LFStoggle, this);

  cotk_bind_toggle(console, "%RFShort_c%", "RFShort_cam:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_RFStoggle, this);

  cotk_bind_toggle(console, "%Rad%", "Radar:", "", 
                   (cotk_callback_t) &HealthMonitorObject::on_radtoggle, this);

  // Bind Quit
  cotk_bind_button(console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) &HealthMonitorObject::onUserQuit, this);

  // Set Toggles to correct initial values

  cotk_toggle_set(console, "%LFBumper_L%", sensors[0].getConsiderationValue());
  cotk_toggle_set(console, "%MFBumper_L%", sensors[1].getConsiderationValue());
  cotk_toggle_set(console, "%RFBumper_L%", sensors[2].getConsiderationValue());
  cotk_toggle_set(console, "%MRBumper_L%", sensors[3].getConsiderationValue());
  cotk_toggle_set(console, "%LFRoof_L%", sensors[4].getConsiderationValue());
  cotk_toggle_set(console, "%RFRoof_L%", sensors[5].getConsiderationValue());
  cotk_toggle_set(console, "%Riegl_L%", sensors[6].getConsiderationValue());
  cotk_toggle_set(console, "%MFLong_c%", sensors[7].getConsiderationValue());
  cotk_toggle_set(console, "%MFMed_c%", sensors[8].getConsiderationValue());
  cotk_toggle_set(console, "%LFShort_c%", sensors[9].getConsiderationValue());
  cotk_toggle_set(console, "%RFShort_c%", sensors[10].getConsiderationValue());
  cotk_toggle_set(console, "%Rad%", sensors[11].getConsiderationValue());

  MSG("Console Quit button bound");

  //Initialize the display
  if (cotk_open(console, NULL) != 0)
  {
    cout << "error on initalizing consle display";
  }

  cout << "Console opened" << endl;
 
  cotk_printf(console,"%_skynet_key_%", A_NORMAL, "%05d", skynetKey); 
  MSG("SkynetKey Printed");

}

void HealthMonitorObject::updateConsole()
{
  string name;
  string token;
  int status;
  int numMessages;
  string report;

  // take care of sensors
  for (int i = 0; i < (int)sensors.size(); i++)
  {
    report = "";
    status = sensors[i].getComponentHealthLevel(report);
    numMessages = sensors[i].getNumMessages();
    name = sensors[i].getComponentNameShort();
    token = "%" + name + "%";
    cotk_printf(console, token.c_str(), A_NORMAL, "%2d   %s %8d", status, report.c_str(), numMessages);
  }

  // take care of Coverage zones
  //cout << "Center near coverage level: " << zones->getCenterNearCoverageLevel() << endl;

  cotk_printf(console, "%_centerNear_%", A_NORMAL, "%3.2f", zones->getCenterNearCoverageLevel());
  cotk_printf(console, "%_centerFar_%", A_NORMAL, "%3.2f", zones->getCenterFarCoverageLevel());
  cotk_printf(console, "%_leftZone_%", A_NORMAL, "%3.2f", zones->getLeftCoverageLevel());
  cotk_printf(console, "%_rightZone_%", A_NORMAL, "%3.2f", zones->getRightCoverageLevel());
  cotk_printf(console, "%_rearZone_%", A_NORMAL, "%3.2f", zones->getRearCoverageLevel());

  // take care of actuators
  //cout << "throttle: " << actuators->getThrottleHealthLevel() << endl;
  
  actuators->getThrottleHealthLevel(report);
  cotk_printf(console, "%_throttle_%", A_NORMAL, report.c_str());
  actuators->getBrakeHealthLevel(report);
  cotk_printf(console, "%_brake_%", A_NORMAL, report.c_str());
  actuators->getSteeringHealthLevel(report);
  cotk_printf(console, "%_steering_%", A_NORMAL, report.c_str());
  actuators->getTransmissionHealthLevel(report);
  cotk_printf(console, "%_transmission_%", A_NORMAL, report.c_str());

  applanix->getApplanixHealthLevel(report);
  cotk_printf(console, "%_applanix_%", A_NORMAL, report.c_str());
  
  // take care of capabilities
  cotk_printf(console, "%_capLeft_%", A_NORMAL, "%1.2f", capabilities->getLeftTurnCap());
  cotk_printf(console, "%_capRight_%", A_NORMAL, "%1.2f", capabilities->getRightTurnCap()); 
  cotk_printf(console, "%_capStraight_%", A_NORMAL, "%1.2f", capabilities->getStraightTurnCap()); 
  cotk_printf(console, "%_capUturn_%", A_NORMAL, "%1.2f", capabilities->getUTurnCap());
  cotk_printf(console, "%_capDriving_%", A_NORMAL, "%1.2f", capabilities->getNominalDrivingCap());   
  cotk_printf(console, "%_capStopping_%", A_NORMAL, "%1.2f", capabilities->getNominalStoppingCap()); 
  cotk_printf(console, "%_capZone_%", A_NORMAL, "%1.2f", capabilities->getNominalZoneCap()); 
  cotk_printf(console, "%_capNew_%", A_NORMAL, "%1.2f", capabilities->getNominalNewCap()); 

  //cout << "calling cotk update: " << cotk_update(console) << endl;
  cotk_update(console);
}
  

void HealthMonitorObject::mainLoop()
{
  int counter = 0;
  MSG("Entering Main Loop");
  while (!quit)
  {
    usleep(50000);  //by my testing this makes the program run at a little slower than 5 Hz
    if (updateComponents() != 0)
    {
      MSG("update call failed");
      break;
    }

    for (int i = 0; i < (int)sensors.size(); i++)
    {
      int status = sensors[i].getComponentHealthLevel();
      string name = sensors[i].getComponentNameShort();
      if (!console)
	cout << name << ": " << status << endl;
    }

    zones->updateCoverageLevels();

    capabilities->update();
    if (listenToMapper) {
      if (sensors[sensors.size()-1].getComponentHealthLevel() != GOOD) {
	capabilities->setNominalDrivingCap(0);
	if (!console)
	  cout << "Mapper (health = " << sensors[sensors.size()-1].getComponentHealthLevel() 
	       << ") is not running. Setting nominal capability to 0" << endl;
      }
    }

    if (safeMode) {
      if (sensors[sensors.size()-2].getComponentHealthLevel() != GOOD) {
	capabilities->setNominalDrivingCap(0);
      }
    }

    if (!console) {
      cout << "Center Near: " <<  zones->getCenterNearCoverageLevel() << endl;
      cout << "Center Far: " <<  zones->getCenterFarCoverageLevel() << endl;
      cout << "Left: " <<  zones->getLeftCoverageLevel() << endl;
      cout << "Right: " <<  zones->getRightCoverageLevel() << endl;
      cout << "Rear: " <<  zones->getRearCoverageLevel() << endl;
      
      cout << "throttle: " << actuators->getThrottleHealthLevel() << endl;
      cout << "brake: " << actuators->getBrakeHealthLevel() << endl;
      cout << "steering: " << actuators->getSteeringHealthLevel() << endl;
      cout << "transmission: " << actuators->getTransmissionHealthLevel() << endl;  
    }

    MSG("broadcasting");
    capabilities->broadcast();
 
    if(console)
    {
      updateConsole();
    }
    if (!console)
      cout << "looping: " << counter++ << endl;
  }
}
    
// Handle quit button press
int HealthMonitorObject::onUserQuit(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  MSG("Quit button pressed");
  quit = true;
  return 0;
}


int HealthMonitorObject::on_LFBtoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[0].setConsiderationValue(cotk_toggle_get(console, "%LFBumper_L%"));
  return 0;
}

int HealthMonitorObject::on_MFBtoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[1].setConsiderationValue(cotk_toggle_get(console, "%MFBumper_L%"));
  return 0;
}

int HealthMonitorObject::on_RFBtoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[2].setConsiderationValue(cotk_toggle_get(console, "%RFBumper_L%"));
  return 0;
}

int HealthMonitorObject::on_MRBtoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[3].setConsiderationValue(cotk_toggle_get(console, "%MRBumper_L%"));
  return 0;
}

int HealthMonitorObject::on_LFRtoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[4].setConsiderationValue(cotk_toggle_get(console, "%LFRoof_L%"));
  return 0;
}

int HealthMonitorObject::on_RFRtoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[5].setConsiderationValue(cotk_toggle_get(console, "%RFRoof_L%"));
  return 0;
}

int HealthMonitorObject::on_Rgltoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[6].setConsiderationValue(cotk_toggle_get(console, "%Riegl_L%"));
  return 0;
}

int HealthMonitorObject::on_MFLtoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[7].setConsiderationValue(cotk_toggle_get(console, "%MFLong_c%"));
  return 0;
}

int HealthMonitorObject::on_MFMtoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[8].setConsiderationValue(cotk_toggle_get(console, "%MFMed_c%"));
  return 0;
}

int HealthMonitorObject::on_LFStoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[9].setConsiderationValue(cotk_toggle_get(console, "%LFShort_c%"));
  return 0;
}

int HealthMonitorObject::on_RFStoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[10].setConsiderationValue(cotk_toggle_get(console, "%RFShort_c%"));
  return 0;
}

int HealthMonitorObject::on_radtoggle(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  self->sensors[11].setConsiderationValue(cotk_toggle_get(console, "%Rad%"));
  return 0;
}




