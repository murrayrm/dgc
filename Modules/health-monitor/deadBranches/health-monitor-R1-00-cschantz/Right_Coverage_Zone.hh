/*!
 * File: Right_Coverage_Zone.cc
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be take care of the coverage zones
 *       around Alice
 *
 */

#ifndef Right_Coverage_Zone_812ye3hwq823uiw
#define Right_Coverage_Zone_812ye3hwq823uiw

#include <iostream>
#include <string>
#include "Coverage_Zone.hh"
#include "Alice_Lidar.hh"
using namespace std;

class Right_Coverage_Zone : public Coverage_Zone {

  Alice_Lidar *Right_Bumper_Ladar;
  //Alice_Sensor *Center_Bumper_Ladar;

  void updateCoverageLevel();

public:
  Right_Coverage_Zone(Alice_Lidar* RB_ladar);
    
};

#endif Right_Coverage_Zone_812ye3hwq823uiw
