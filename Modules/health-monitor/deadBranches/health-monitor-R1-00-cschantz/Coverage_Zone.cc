/*!
 * File: Coverage_Zone.cc
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     coverage zones around Alice
 *
 */

#include "Coverage_Zone.hh"
#include <iostream>
#include <string>
using namespace std;

string Coverage_Zone::getName() const {
  return zoneName;
}

int Coverage_Zone::getCoverageLevel() {
  updateCoverageLevel();
  return coverageLevel;
}




