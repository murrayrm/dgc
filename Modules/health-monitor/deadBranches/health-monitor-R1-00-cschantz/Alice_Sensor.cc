/*!
 * File: Alice_Sensor.cc
 * 
 * Author: Chris Schantz
 * Date: July 19th 2007
 *
 * Description: Implementation for the abstract base class for the 
 *     various sensor components in Alice.
 *
 */

#include "Alice_Sensor.hh"

void Alice_Sensor::setComponentHealthLevel(int level) {
  if ((level == UNKNOWN )||(level == PERMANETLY_BAD)||(level == TEMPORARILY_BAD)||(level == GOOD))
  {
    updateTimeStats(level); // valid level, so update our time variables
    componentHealth = level;
  }
  else
  {
    cout << "Bad sensor health level (not valid):" << level << endl;
    cout << "Defaulting to bad" << endl;
    componentHealth = PERMANETLY_BAD;
  }
}
