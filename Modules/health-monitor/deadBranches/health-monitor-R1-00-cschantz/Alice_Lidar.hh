/*!
 * File: Alice_Lidar.hh
 * 
 * Author: Chris Schantz
 * Date: July 19th 2007
 *
 * Description: This class is ment to be the class for the 
 *     various lidars on Alice.
 *
 */

#include "Alice_Sensor.hh"
#include <string>
using namespace std;


#define DEFAULT_LIDAR_TIME_LIMIT = 10000   //TODO: figure out the time stuff

#ifndef Alice_Lidar_56ytrf45e
#define Alice_Lidar_56ytrf45e

class Alice_Lidar: public Alice_Sensor {

  double timeOut;  //if update time is longer than this switch to unknown (or Bad)

  bool checkTimeOut();

public:
  Alice_Lidar(string name, double timeLimit);

  int getComponentHealthLevel();
};

#endif Alice_Lidar_56ytrf45e
