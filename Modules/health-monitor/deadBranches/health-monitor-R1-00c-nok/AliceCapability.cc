/*!
 * File: AliceCapability.cc
 * 
 * Author: Chris Schantz
 * Date: August 2nd 2007
 *
 * Description: This class is ment ttake care of the determination logic
 *              and broadcasting using the skynet talker.
 *
 */

#include <dgcutils/DGCutils.hh>

#include "AliceCapability.hh"

using namespace std;

AliceCapability::AliceCapability(AliceActuator* actuatorPtr, CoverageZones* zonesPtr, SkynetTalker<VehicleCapability>* talkerPtr)
{
  actuator = actuatorPtr;
  zones = zonesPtr;
  talker = talkerPtr;
  cout << "talker address: " << talker << endl;

  results = new VehicleCapability();
}

AliceCapability::~AliceCapability()
{
  delete results;
}

void AliceCapability::update()
{
  // Only grab there once so they don't change due to timeouts
  float left = zones->getLeftCoverageLevel();
  float right = zones->getRightCoverageLevel();
  float rear = zones->getRearCoverageLevel();
  float far = zones->getCenterFarCoverageLevel();
  float near = zones->getCenterNearCoverageLevel();

  int trans = actuator->getTransmissionHealthLevel();
  int brake = actuator->getBrakeHealthLevel();
  int steer = actuator->getSteeringHealthLevel();
  int throttle = actuator->getThrottleHealthLevel();
  
  assert(left <= 1.0);
  assert(right <= 1.0);
  assert(rear <= 1.0);
  assert(far <= 1.0);
  assert(near <= 1.0);  

  // Right turn determination
  float rightTurnCap = 0.0;
  float leftTurnCap = 0.0;
  float straightTurnCap = 0.0;
  float uTurnCap = 0.0;
  float nominalDrivingCap = 0.0;
  float nominalStoppingCap = 0.0;
  float nominalZoneCap = 0.0;
  //float nominalNewZoneCap = 0.0;

  rightTurnCap = ((0.4 * far + 0.6 * left + 0.1 * right)/ (0.4 + 0.6 + 0.1));
  assert(rightTurnCap <= 1.0);

  leftTurnCap = ((0.4 * far + 0.3 * left + 0.3 * right)/ (0.4 + 0.3 + 0.3));
  assert(rightTurnCap <= 1.0);

  straightTurnCap = ((0.2 * far + 0.4 * left + 0.4 * right)/ (0.2 + 0.4 + 0.4));
  assert(straightTurnCap <= 1.0);

  if (trans == 1)
  {
    uTurnCap = rear;
  }
  else uTurnCap = 0.0;

  nominalZoneCap = near;

  nominalStoppingCap = far;

  nominalDrivingCap = 1.0; 
  if (trans != 1)
  {
    nominalDrivingCap = 0.66;
  }
  if (throttle != 1)
  {
    nominalDrivingCap = 0.33;
  }
  if ((steer != 1) || (brake != 1))
  {
    nominalDrivingCap = 0.0;
  }

  // now convert to 1-4 scale and update results struct
  results->intersectionRightTurn = (int)(1 + (3 * rightTurnCap));
  results->intersectionLeftTurn = (int)(1 + (3 * leftTurnCap));
  results->intersectionStraightTurn = (int)(1 + (3 * straightTurnCap));
  results->uturn = (int)(1 + (3 * uTurnCap));
  results->nominalDriving = (int)(1 + (3 * nominalDrivingCap));
  results->nominalStopping = (int)(1 + (3 * nominalStoppingCap));
  results->nominalZoneRegionDriving = (int)(1 + (3 * nominalZoneCap));
}

void AliceCapability::broadcast() const
{
  talker->send(results);
}







  
