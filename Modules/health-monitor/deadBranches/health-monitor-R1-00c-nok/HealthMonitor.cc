/*!
 * File: Health_Monitor.cc
 * 
 * Author: Chris Schantz
 * Date: July 19th 2007
 *
 * Description: Monitors the health of the vehicle hardware and 
 *              determines what the vehicle is capable of doing
 *
 */

// Generic level includes
#include <iostream>
#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <ctype.h>
#include <time.h>
#include <list>
#include <vector>

// DGC level includes
#include <cotk/cotk.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>
#include <interfaces/VehicleCapability.h>
#include <skynettalker/SkynetTalker.hh>
#include <skynettalker/StateClient.hh>

// Module level includes
#include "cmdline.h"
#include "HealthMonitorObject.hh"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


using namespace std;

int main(int argc, char **argv)
{  
   
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Contains all of the sensor definitions
  char *configFile;  

  /* Do command line stuff */
      
  // Load options
  if (cmdline_parser(argc, argv, &options) < 0)
    return -1;

  // Get config file from the command line, default is Sensor_Feeder.CFG
  configFile = dgcFindConfigFile(options.cfg_arg, "health-monitor"); 

  // Fill out the spread name
  if (options.spread_daemon_given)
    spreadDaemon = options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  MSG("Spread Daemon Found");

  // Fill out the skynet key
  if (options.skynet_key_given)
    skynetKey = options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    skynetKey = 0;
  
  MSG("Skynet Key Found");

  // Load options from the configuration file
  if (cmdline_parser_configfile(configFile, &options, false, false, false) != 0)
    MSG("unable to process configuration file %s", configFile);
  
  MSG("Configuration File Processed");
    
  // Initalize the sensor communications object
  HealthMonitorObject healthMonitor = HealthMonitorObject(spreadDaemon, skynetKey, options);
  
  MSG("HealthMonitorObject created************");

  healthMonitor.mainLoop();

  cmdline_parser_free(&options);   
  return 0;
}

 
  

