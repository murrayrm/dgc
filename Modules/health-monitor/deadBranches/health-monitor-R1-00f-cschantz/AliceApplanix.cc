/*!
 * File: AliceApplanix.cc
 * 
 * Author: Chris Schantz
 * Date: August 13th 2007
 *
 * Description: Encapsulates Applanix unit health monitoring.
 *
 */

#include <dgcutils/DGCutils.hh>
#include <interfaces/StatePrecision.hh>

#include "AliceApplanix.hh"


using namespace std;

AliceApplanix::AliceApplanix(SkynetTalker<int>* astateHealthSource) 
{
  astateHealthTalker = astateHealthSource;
  timeOut = 100000;  // tenth a second, update time is longer than this switch to unknown/off
  
  numMessages = 0;
  
  lastTimeStamp = 0;

  applanixHealth = 9;
}

int AliceApplanix::update()
{
  // Get astate health
  if (astateHealthTalker->hasNewMessage()) 
  {
    bool astateModeReceived = astateHealthTalker->receive(&applanixHealth);
    if (astateModeReceived) 
    {
      numMessages++;
      lastTimeStamp = DGCgettime();
    }
  }
  return 0;
}


int AliceApplanix::getApplanixHealthLevel(string &report)
{
  char s[128];
  
  if ((DGCgettime() - lastTimeStamp) < timeOut) 
  {
    if (applanixHealth == (int)NOT_CONNECTED) 
    {
	  sprintf(s, "%2d   %s %8d", applanixHealth, "NoConn ", numMessages);
    }
    else if (applanixHealth == (int)NO_MESSAGES) 
    {
	  sprintf(s, "%2d   %s %8d", applanixHealth, "NoMess ", numMessages); 
    }
    else if (applanixHealth == (int)NO_SOLUTION) 
    {
	  sprintf(s, "%2d   %s %8d", applanixHealth, "NoSolu ", numMessages);
    }    
    else if (applanixHealth == (int)INITIAL) 
    {
	  sprintf(s, "%2d   %s %8d", applanixHealth, "Init   ", numMessages); 
    }
    else if ((applanixHealth == (int)NOT_ALIGN_GPS) || (applanixHealth == (int)NOT_ALIGN_NOGPS) || (applanixHealth == (int)COARSE_LEVELING)) 
    {
	  sprintf(s, "%2d   %s %8d", applanixHealth, "Degrade", numMessages); 
    }
    else if ((applanixHealth == (int)FINE_ALIGN) || (applanixHealth == (int)ALIGN_GPS) || (applanixHealth == (int)ALIGN_NOGPS)) 
    {
	  sprintf(s, "%2d   %s %8d", applanixHealth, "Good   ", numMessages); 
    }
    else if (applanixHealth == (int)FULL_NAV) 
    {
	  sprintf(s, "%2d   %s %8d", applanixHealth, "FullNav", numMessages); 
    }
  }
  else
  {
    applanixHealth = 11;
    sprintf(s, "%2d   %s %8d", applanixHealth, "TimeOut", numMessages); 
  }
  report = s;
  return applanixHealth;
}

int AliceApplanix::getApplanixHealthLevel()
{
  string temp = "";
  return getApplanixHealthLevel(temp);
}
