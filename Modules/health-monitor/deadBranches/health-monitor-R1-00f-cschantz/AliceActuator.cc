/*!
 * File: AliceActuator.cc
 * 
 * Author: Chris Schantz
 * Date: July 24th 2007
 *
 * Description: Implementation for the  
 *     various Actuator components in Alice.
 *
 */

#include <math.h>

#include <dgcutils/DGCutils.hh>

#include "AliceActuator.hh"

using namespace std;

AliceActuator::AliceActuator(ActuatorState* statusPtr) 
{
  status = statusPtr;
  timeOutDefault = 1000000;  // half a second, update time is longer than this switch to unknown/off
  timeOutTransmission = 10000000;  // 5 seconds because transmission is only updated at .25 hertz

  numMessagesThrottle = 0;
  numMessagesBrake = 0;
  numMessagesSteering = 0;
  numMessagesTransmission = 0;

  lastThrottleTimeStamp = 0;
  lastBrakeTimeStamp = 0;
  lastSteeringTimeStamp = 0;
  lastTransmissionTimeStamp = 0;
}

int AliceActuator::getThrottleHealthLevel(string &report)
{
  char s[128];
  //cout << "in throttle update: current time: " << DGCgettime() << " update_time stamp: " << status->m_gas_update_time << endl;
  //cout << "status of throttle (raw from struct): " << status->m_gasstatus << endl;
  if (status->m_gas_update_time > lastThrottleTimeStamp)
  {
    lastThrottleTimeStamp = status->m_gas_update_time;
    numMessagesThrottle++;
  }
  if ((DGCgettime() - status->m_brake_update_time) < timeOutDefault)
  {
    sprintf(s, "%2d   %s %8d", status->m_gasstatus, "Nominal", numMessagesThrottle);
    report = s;
    return status->m_gasstatus;
  }
  else 
  {
    sprintf(s, "%2d   %s %8d", -1, "TimeOut", numMessagesThrottle);
    report = s;    
    return -1;
  }
}

int AliceActuator::getBrakeHealthLevel(string &report){
  char s[128];
  if (status->m_brake_update_time > lastBrakeTimeStamp)
  {
    lastBrakeTimeStamp = status->m_brake_update_time;
    numMessagesBrake++;
  }
  if ((DGCgettime() - status->m_brake_update_time) < timeOutDefault)
  {
    sprintf(s, "%2d   %s %8d", status->m_brakestatus, "Nominal", numMessagesBrake);
    report = s;
    return status->m_brakestatus;
  }
  else 
  {
    sprintf(s, "%2d   %s %8d", -1, "TimeOut", numMessagesBrake);
    report = s;
    return -1;
  }
}

int AliceActuator::getSteeringHealthLevel(string &report){
  char s[128];
  if (status->m_steer_update_time > lastSteeringTimeStamp)
  {
    lastSteeringTimeStamp = status->m_steer_update_time;
    numMessagesSteering++;
  }
  if ((DGCgettime() - status->m_brake_update_time) < timeOutDefault)
  {
    sprintf(s, "%2d   %s %8d", status->m_steerstatus, "Nominal", numMessagesSteering);
    report = s;
    return status->m_steerstatus;
  }
  else 
  {
    sprintf(s, "%2d   %s %8d", -1, "TimeOut", numMessagesSteering);
    report = s;
    return -1;
  }
}

int AliceActuator::getTransmissionHealthLevel(string &report){
  char s[128];
  if (status->m_trans_update_time > lastTransmissionTimeStamp)
  {
    lastTransmissionTimeStamp = status->m_trans_update_time;
    numMessagesTransmission++;
  }
  if ((DGCgettime() - status->m_brake_update_time) < timeOutDefault)
  {
    sprintf(s, "%2d   %s %8d", status->m_transstatus, "Nominal", numMessagesTransmission);
    report = s;
    return status->m_transstatus;
  }
  else 
  {
    sprintf(s, "%2d   %s %8d", -1, "TimeOut", numMessagesTransmission);
    report = s;
    return -1;
  }
}

int AliceActuator::getThrottleHealthLevel()
{
  string temp = "";
  return getThrottleHealthLevel(temp);
}

int AliceActuator::getBrakeHealthLevel()
{
  string temp = "";
  return getBrakeHealthLevel(temp);
}

int AliceActuator::getSteeringHealthLevel()
{
  string temp = "";
  return getSteeringHealthLevel(temp);
}

int AliceActuator::getTransmissionHealthLevel()
{
  string temp = "";
  return getTransmissionHealthLevel(temp);
}




