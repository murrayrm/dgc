/*!
 * File: CoverageZones.hh
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to handle the coverage zones around Alice
 *
 */

#ifndef CoverageZones_HH
#define CoverageZones_HH

/*
 *    This area defines all the sensors and their associated keys for lookup in the map
 */

#include <interfaces/sn_types.h>

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "AliceSensor.hh"

class CoverageZones {
  
  /// Coverage Levels [0, 1]
  float LeftCoverageLevel;
  float RightCoverageLevel;
  float CenterNearCoverageLevel;
  float CenterFarCoverageLevel;
  float RearCoverageLevel;

  /// Pointer to set of all sensors (set means no duplicates)
  map<string, AliceSensor*> sensors;

  void updateRearCoverageLevel();
  void updateLeftCoverageLevel();
  void updateRightCoverageLevel();
  void updateCenterNearCoverageLevel();
  void updateCenterFarCoverageLevel();
  
public:

  /// Construct with the full list of sensors
  CoverageZones(vector<AliceSensor> &sensorList);
  
  ~CoverageZones();
  
  /// Updates all the coverage level floats above
  void updateCoverageLevels();

  /// Accessors
  float getLeftCoverageLevel() {return LeftCoverageLevel;}
  float getRightCoverageLevel() {return RightCoverageLevel;}
  float getCenterNearCoverageLevel() {return CenterNearCoverageLevel;}
  float getCenterFarCoverageLevel() {return CenterFarCoverageLevel;}
  float getRearCoverageLevel() {return RearCoverageLevel;}

};
#endif //CoverageZones_HH
