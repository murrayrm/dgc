/*!
 * File: AliceApplanix.hh
 * 
 * Author: Chris Schantz
 * Date: August 13th 2007
 *
 * Description: Encapsulates Applanix unit health monitoring.
 *
 */

#include <skynettalker/SkynetTalker.hh>

#ifndef AliceApplanix_HH
#define AliceApplanix_HH

class AliceApplanix {

  unsigned long long timeOut;

  int applanixHealth;

  int numMessages;

  unsigned long long startTimeInCurrentStatus;

  int prevApplanixHealth;
  int lastStableApplanixHealth;

  unsigned long long lastTimeStamp;
    
  SkynetTalker<int>* astateHealthTalker; 
  
public:
  
  // messageTimeOut in micro seconds
  AliceApplanix(SkynetTalker<int>* astateHealthSource);

  // Modifier
  int update();

  //Accessor
  int getApplanixHealthLevel();
  
  int getApplanixHealthLevel(string &report);
};

#endif //AliceApplanix_HH
