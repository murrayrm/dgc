/* 
 * Object responsible for Health Monitor's communications inputs
 * Date: 20 July 2007
 * Author: Chris Schantz
 * 
 */

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <iostream>

#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>
#include <skynettalker/SkynetTalker.hh>

#include "cmdline.h"
#include "CommunicationsObject.hh"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

// Default constructor
CommunicationsObject::CommunicationsObject(char *spreadDaemon, int skynetKey)
    : CSkynetContainer(MODhealthMonitor, skynetKey),  
      CStateClient(true)
{
  MSG("in commObject constructor");
  
  numSensorSubscribers = 0;
  numActuatorSubscribers = 0;
  
  MSG("everything zeroed");
  if (initSensnet(spreadDaemon, skynetKey) != 0)
  {
    MSG("initalizing sensnet failed");
    return;
  }
  return;
}

// Default destructor - close out or sensnet subscriptions
CommunicationsObject::~CommunicationsObject()
{
  finiSensnet();
  return;
}


// Initialize sensnet
int CommunicationsObject::initSensnet(char *spreadDaemon, int skynetKey)
{
  MSG("in initalizing sensnet function");
  // Initialize SensNet
  sensnet = sensnet_alloc();
  MSG("sensnet handle allocated");
  if (sensnet_connect(sensnet, spreadDaemon, skynetKey, MODhealthMonitor) != 0)
  {
    return ERROR("unable to connect to sensnet");
  }
  MSG("initalizing sensnet done");
  return 0;    
}


// Finalize sensnet
int CommunicationsObject::finiSensnet()
{  
  AliceSensor* tempSensor;

  // Clean up SensNet
  for (int i = 0; i < numSensorSubscribers; i++)
  {
    tempSensor = sensorSubscribers[i];    
    closeSensorConnection(tempSensor);
  }

  sensnet_disconnect(sensnet);
  sensnet_free(sensnet);
  sensnet = NULL;
  return 0;
}

// closes the sensnet group for this sensor
void CommunicationsObject::closeSensorConnection(AliceSensor* sensor)
{
  sensnet_leave(sensnet, sensor->getSensorId(), SNprocessResponse);
}
  

void CommunicationsObject::closeActuatorConnection(AliceActuator* actuator)
{
  return;  //no action needed here to close an actuator connection
}


// Update components of type actuator
void CommunicationsObject::updateActuatorComponent(AliceActuator* actuator)
{
  switch (actuator->getActuatorId())
  {
    case THROTTLE :
          actuator->setComponentHealthLevel(m_actuatorState.m_gasstatus, m_actuatorState.m_gas_update_time);
          break;
    case BRAKE : 
          actuator->setComponentHealthLevel(m_actuatorState.m_brakestatus, m_actuatorState.m_brake_update_time);
          break;
    case STEERING :
          actuator->setComponentHealthLevel(m_actuatorState.m_steerstatus, m_actuatorState.m_steer_update_time);
          break;
    case TRANSMISSION :
          actuator->setComponentHealthLevel(m_actuatorState.m_transstatus, m_actuatorState.m_trans_update_time);
          break;
    default :
          int doesNothing = 0;
          
  }
}

// Update components of type sensor reads off its own message from sensnet
void CommunicationsObject::updateSensorComponent(AliceSensor* sensor)
{
  int blobId;
  ProcessResponse response;

  memset(&response, 0, sizeof(response));
  
  if (sensnet_read(sensnet, sensor->getSensorId(), SNprocessResponse,
                     &blobId, sizeof(response), &response) != 0)
      return;  //error or no message do not update
  if (sensor->getTimeSinceLastUpdate() == response.timestamp)
      return;  //end this function, already have this message.

  sensor->setComponentHealthLevel(response.healthStatus, response.timestamp);
}  

// Update component of type Applanix
//void CommunicationsObject::updateComponent(AliceApplanix* applanix) {}

// Update componen of type Processes
//void CommunicationsObject::updateComponent(AliceProcesses* process) {}

// Subscribe a new sensor component to the list of subscribers
int CommunicationsObject::subscribe(AliceSensor* member)
{
  sensorSubscribers[numSensorSubscribers++] = member;
  if (sensnet_join(sensnet, member->getSensorId(), SNprocessResponse, sizeof(ProcessResponse)) != 0)
  {
        return ERROR("unable to join process response");
  }
  return 0;  
}

// Subscribe a new actuator component to the list of subscribers
int CommunicationsObject::subscribe(AliceActuator* member)
{
  actuatorSubscribers[numActuatorSubscribers++] = member;
  return 0;
}
  
// Check for changes to the sensor state
int CommunicationsObject::update()
{
  //MSG("In update call");

  AliceSensor *tempSensor;
  
  //MSG("got temp sensor");
  AliceActuator *tempActuator;
  //MSG("got temp actuator, going to sleep for 100000 microsecs");  
  // Don't run too fast
  usleep(100000); //about 10 Hz
  
  //MSG("done with sleep, calling sensnet_wait");
  
  // Wait for new updates (with timeout)
  int status = sensnet_wait(sensnet, 100);  //miliseconds- will wait for up to .1 seconds to get a message here
  if (status != 0 && status != ETIMEDOUT)
    return ERROR("wait failed %s", strerror(status));  

  //MSG("know we have new messages here");

  // Update all sensorSubscribers
  for (int i = 0; i < numSensorSubscribers; i++)
  {
    tempSensor = sensorSubscribers[i];    
    updateSensorComponent(tempSensor);
  }

  // Update the Actuator State
  UpdateActuatorState();
  
  for (int i = 0; i < numActuatorSubscribers; i++)
  {
    tempActuator = actuatorSubscribers[i];    
    updateActuatorComponent(tempActuator);
  }

  return 0;
}




