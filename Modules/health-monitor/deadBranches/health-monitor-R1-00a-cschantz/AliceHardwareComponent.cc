/*!
 * File: AliceHardwareComponent.cpp
 * 
 * Author: Chris Schantz
 * Date: July 18th 2007
 *
 * Description: Implementation for the abstract base class for the 
 *     various hardware components in Alice.
 *
 */

using namespace std;

#include <string>
#include <dgcutils/DGCutils.hh>
#include <sstream>

#include "AliceHardwareComponent.hh"

string AliceHardwareComponent::toString() const{
  stringstream s("");
  s << componentName << ": " << getComponentHealthLevel() << "\n";
  return s.str();
}

string AliceHardwareComponent::getComponentName() const {
  return componentName;
}

unsigned long long AliceHardwareComponent::getTimeSinceLastUpdate() {
  return (DGCgettime() - timeOfLastUpdate);
}

unsigned long long AliceHardwareComponent::getTimeSinceLastStatusChange() {
  return (DGCgettime() - timeOfLastStatusChange);
}

unsigned long long AliceHardwareComponent::getTimeSinceLastConsiderationToggle() {
  return (DGCgettime() - timeOfLastConsiderationToggle);
}

void AliceHardwareComponent::updateTimeStats(int level, unsigned long long timeStamp) {
  timeOfLastUpdate = timeStamp;
  if (level != componentHealth)
    timeOfLastStatusChange = timeStamp;
}

void AliceHardwareComponent::setConsiderationValue(bool flag) {
  if (flag != underConsideration)
    timeOfLastConsiderationToggle = DGCgettime();
  underConsideration = flag;
}

int AliceHardwareComponent::getComponentType() const
{
  return componentType;
}

AliceHardwareComponent::~AliceHardwareComponent() {}







