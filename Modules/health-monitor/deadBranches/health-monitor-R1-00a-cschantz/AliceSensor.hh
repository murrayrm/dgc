/*!
 * File: AliceSensor.hh
 * 
 * Author: Chris Schantz
 * Date: July 19th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     various sensors on Alice.
 *
 */

#ifndef AliceSensor_67tyfgfgtfr5
#define AliceSensor_67tyfgfgtfr5

#include "AliceHardwareComponent.hh"

#define DEFAULT_SENSOR_TIME_LIMIT 1000000   
#define MINIMUM_SENSOR_TIME_LIMIT 10000   // This Magic number should be greater than one loop time through the program- else everything will always be too old.

 enum SensorHealthLevels{   //checking on health values
    UNKNOWN = -1,
    PERMANETLY_BAD = 0,
    TEMPORARILY_BAD = 1,
    GOOD = 2
  };

class AliceSensor: public AliceHardwareComponent {

  unsigned long long timeOut;  //if update time is longer than this switch to unknown

  // Sensor id unique for each sensor
  int id;

  bool checkTimeOut() const;

public:
  
  // messageTimeOut in micro seconds
  AliceSensor(string name, int sensorId, unsigned long long newMessageTimeOut = DEFAULT_SENSOR_TIME_LIMIT, bool considered = true);
  
  AliceSensor();

  //Accessor
  int getComponentHealthLevel() const;

  int getSensorId() const;

  //Mutator
  int setComponentHealthLevel(int level, unsigned long long timeStamp);
  
};

#endif //AliceSensor_67tyfgfgtfr5
