/*!
 *  Author: Chris Schantz
 *  Date: July 25th 2007
 * 
 *  Description: Object to encapsulate communications
 *
 */

#ifndef CommunicationsObject_gyu87tuygt87
#define CommunicationsObject_gyu87tuygt87

#include <stdint.h>
#include <interfaces/sn_types.h>
#include <skynettalker/StateClient.hh>
#include "AliceSensor.hh"
#include "AliceActuator.hh"



class CommunicationsObject : public CStateClient {

  // Initialize sensnet
  int initSensnet(char *spreadDaemon, int skynetKey);

  // Parse the config file
  int parseConfigFile(char *configFile, gengetopt_args_info options);

  // Finalize sensnet
  int finiSensnet();

  // Functions to close down connections
  void closeSensorConnection(AliceSensor* sensor);

  void closeActuatorConnection(AliceActuator* actuator);  //doesn nothing right now

  // Update components of type actuator
  void updateSensorComponent(AliceSensor* sensor);

  // Update components of type sensor
  void updateActuatorComponent(AliceActuator* actuator);

  // Update component of type Applanix
  //void updateComponent(AliceApplanix* applanix);

  // Update componen of type Processes
  //void updateComponent(AliceProcesses* process);

  // SensNet handle
  sensnet_t *sensnet;

  //Pointers to all of our subscriber objects
  AliceSensor* sensorSubscribers[16];  //just a guess that we wont have any more than this number here.
  int numSensorSubscribers;  //actual number of subscribers

  //Pointers to all of our subscriber objects
  AliceActuator* actuatorSubscribers[8];  //just a guess that we wont have any more than this number here.
  int numActuatorSubscribers;  //actual number of subscribers

  public:   

  /// Default constructor
  CommunicationsObject (char *spreadDaemon, int skynetKey);

  /// Default destructor
  ~CommunicationsObject();

  /// Function to subscribe hardware components to the CommunicationsObject
  int subscribe(AliceActuator* member);

  int subscribe(AliceSensor* member);

  /// Update the processState messages for each sensor
  int update();

};

#endif //CommunicationsObject_gyu87tuygt87
