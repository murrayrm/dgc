/*!
 * File: AliceSensor.cc
 * 
 * Author: Chris Schantz
 * Date: July 19th 2007
 *
 * Description: Implementation for the abstract base class for the 
 *     various sensor components in Alice.
 *
 */

#include <string>

#include <dgcutils/DGCutils.hh>

#include "AliceSensor.hh"

using namespace std;

AliceSensor::AliceSensor(string name, int sensorId, sensnet_t *sensnetPtr, bool considered,  unsigned long long newMessageTimeOut)
{
  if (newMessageTimeOut >= MINIMUM_SENSOR_TIME_LIMIT)
  { 
    timeOut = newMessageTimeOut;
  }
  
  componentName = name;
  componentHealth = UNKNOWN;
  underConsideration = considered;
  id = sensorId;
  componentType = SENSOR;
  numMessages = -1;  //-1 not 0 because sensnet will return with a blank ProcessResponce message if nothing is present and incriment this to 0 
                     // before the timestamp checking can tell if it is a useless message or not.

  sensnet = sensnetPtr;

  if (sensnet_join(sensnet, id, SNprocessResponse, sizeof(ProcessResponse)) != 0)
  {
    ERROR("unable to join process response");
  }
  
  MSG("Sensnet Join Succussful");

  unsigned long long temp = DGCgettime();

  timeOfLastUpdate = temp;
  timeOfLastStatusChange = temp;
  timeOfLastConsiderationToggle = temp;
}

AliceSensor::~AliceSensor()
{
  //sensnet_leave(sensnet, id, SNprocessResponse);
}

int AliceSensor::setComponentHealthLevel(int level, unsigned long long timeStamp) {
  if ((level == UNKNOWN )||(level == PERMANETLY_BAD)||(level == TEMPORARILY_BAD)||(level == GOOD))
  {
    updateTimeStats(level, timeStamp); // valid level, so update our time variables
    componentHealth = level;
    return 0;               //no error return code
  }
  else
  {
    updateTimeStats(UNKNOWN, DGCgettime());
    componentHealth = UNKNOWN;
    return 1;               //error return code
  }
}

int AliceSensor::update()
{
  int blobId;
  
  memset(&response, 0, sizeof(response));
  
  if (sensnet_read(sensnet, id, SNprocessResponse,
                     &blobId, sizeof(response), &response) != 0)
      return ERROR("Could not update sensor");  //error or no message do not update
  
  if (timeOfLastUpdate == response.timestamp)
      return 0;  //end this function, already have this message.

  numMessages++;
  setComponentHealthLevel(response.healthStatus, response.timestamp);
  return 0;
}

int AliceSensor::getNumMessages() const
{
  return numMessages;
}

bool AliceSensor::checkTimeOut() const {  //returns true if timeout hasn't passed
  return ((DGCgettime()-timeOfLastUpdate) <= timeOut);
}

int AliceSensor::getComponentHealthLevel() const {
  string temp = "";
  return getComponentHealthLevel(temp);
}

int AliceSensor::getComponentHealthLevel(string &status) const {
  if (!underConsideration)
  {
    status = "Ignored";
    return GOOD;
  }
  if (checkTimeOut()) 
  {
    status = "Nominal";
    return componentHealth;
  }
  else 
  {
    status = "TimeOut";
    return UNKNOWN;
  }
}


int AliceSensor::getSensorId() const {
  return id;
}
