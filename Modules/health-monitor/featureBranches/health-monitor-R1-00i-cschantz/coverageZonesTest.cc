// Test program for the coverage zones object


#include <string>
#include <iostream>
#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <ctype.h>
#include <time.h>
#include <list>
#include <vector>

#include <dgcutils/DGCutils.hh>

#include "CoverageZones.hh"
#include "AliceSensor.hh"

enum zonetypes
{
  Right = 1,
  Left = 2,
  Rear = 3,
  CenterNear = 4,
  CenterFar = 5
};

map<float, int> results;


void print(vector<AliceSensor> &sensors, CoverageZones* zones, int type);
int recurse(int index, vector<AliceSensor> &sensors, CoverageZones* zones, int type);
void printResults();

int recurse(int index, vector<AliceSensor> &sensors, CoverageZones* zones, int type)
{
  if (index == 0)
  {
    sensors[index].setComponentHealthLevel(1, DGCgettime()); //set to bad
    print(sensors, zones, type);
    return 1;
  }
  else if (recurse(index-1, sensors , zones, type) == 1)
  {
    sensors[index].setComponentHealthLevel(1, DGCgettime());  //set me to bad
    for (int i = index-1; i >= 0; i--)  //return all others to good
    {
      sensors[i].setComponentHealthLevel(GOOD, DGCgettime());
    }
    print(sensors, zones, type);
    recurse(index-1, sensors, zones, type);
    return 1;
  }
  cout << "done with recursing" << endl;
  return 0;
}

void print(vector<AliceSensor> &sensors, CoverageZones* zones, int type)
{
  for (int i = 0; i < sensors.size(); i++)
  {
    cout << sensors[i].toString() << " ";
  }

  float temp;

  switch (type)
  {
    case Right:
       zones->updateRightCoverageLevel();
       temp = zones->getRightCoverageLevel();
       cout << "Right Coverage Zone: " << temp << endl;
       results[temp] = results[temp]++;
       break;
    case Left:
       zones->updateLeftCoverageLevel();
       temp = zones->getLeftCoverageLevel();
       cout << "Left Coverage Zone: " << temp << endl;
       results[temp] = results[temp]++;
       break;
    case Rear:
       zones->updateRearCoverageLevel();
       temp = zones->getRearCoverageLevel();
       cout << "Rear Coverage Zone: " << temp << endl;
       results[temp] = results[temp]++;
       break;
    case CenterNear:
       zones->updateCenterNearCoverageLevel();
       temp = zones->getCenterNearCoverageLevel();
       cout << "CenterNear Coverage Zone: " << temp << endl;
       results[temp] = results[temp]++;
       break;
    case CenterFar:
       zones->updateCenterFarCoverageLevel();
       temp = zones->getCenterFarCoverageLevel();
       cout << "CenterFar Coverage Zone: " << temp << endl;
       results[temp] = results[temp]++;
       break;
    default:
       cout << "error this is not a zone type" << endl;
  }

}

void testZone(int size, string names[], int type)
{
  vector<AliceSensor> sensorList;

  // create sensors and put them in the vector
  for (int i = 0; i < size; i++)
  {
    cout << names[i] << endl;
    sensorList.push_back(AliceSensor(names[i], i));
  }

  // set all health to good
  for (int i = 0; i < sensorList.size(); i++)
  {
    cout << sensorList[i].setComponentHealthLevel(GOOD, DGCgettime());
  }

  // create coveragezone object
  CoverageZones* zone = new CoverageZones(sensorList);

  cout << endl;
  print(sensorList, zone, type);
  recurse(sensorList.size()-1, sensorList, zone, type);
  printResults();
  results.clear();
  int pause;
  cin >> pause;
}

void printResults()
{
  map<float, int>::iterator i;
  for (i = results.begin(); i != results.end(); i++)
  {
    cout << "Level: " << (*i).first << " occured: " << (*i).second << endl;
  }
}

int main()
{


results.clear();

int numSensorsRZ = 3;
string RZnames[3] = {"MODladarFeederRFBumper", "MODladarFeederRFRoof", "MODladarFeederMFBumper"};
int numSensorsLZ = 3;
string LZnames[3] = {"MODladarFeederLFBumper", "MODladarFeederLFRoof", "MODladarFeederMFBumper"};
int numSensorsRearZ = 5;
string RearZnames[5] = {"MODladarFeederRFBumper", "MODladarFeederRFRoof", 
                        "MODladarFeederLFBumper", "MODladarFeederLFRoof", "MODladarFeederMRBumper"};
int numSensorsCNZ = 7;
string CNZnames[7] = { "MODladarFeederRFBumper", "MODladarFeederRFRoof", 
                      "MODladarFeederLFBumper", "MODladarFeederLFRoof", "MODladarFeederMFBumper", "MODstereoFeederLFShort",
                      "MODstereoFeederRFShort" };
int numSensorsCFZ = 5;
string CFZnames[5] = {"MODladarFeederMFBumper",  
                       "MODstereoFeederMFMedium", "MODstereoFeederMFLong", "MODladarFeederRiegl", "MODradarFeeder"};

testZone(numSensorsLZ, LZnames, Left);
testZone(numSensorsRZ, RZnames, Right);

testZone(numSensorsRearZ, RearZnames, Rear);
testZone(numSensorsCNZ, CNZnames, CenterNear);
testZone(numSensorsCFZ, CFZnames, CenterFar);

return 1;
}
// create coveragezone object
//CoverageZones* zones = new CoverageZones(sensorList);






