/*!
 * File: AliceActuator.cc
 * 
 * Author: Chris Schantz
 * Date: July 24th 2007
 *
 * Description: Implementation for the  
 *     various Actuator components in Alice.
 *
 */

#include <math.h>

#include <dgcutils/DGCutils.hh>

#include "AliceActuator.hh"

using namespace std;

AliceActuator::AliceActuator(ActuatorState* statusPtr) 
{
  status = statusPtr;
  timeOutDefault = 1000000;  // half a second, update time is longer than this switch to unknown/off
  timeOutTransmission = 10000000;  // 5 seconds because transmission is only updated at .25 hertz
}

int AliceActuator::getThrottleHealthLevel() const{
  if ((DGCgettime() - status->m_gas_update_time) > timeOutDefault)
  {
    return -1;
  }
  else 
  {
    return status->m_gasstatus;
  }
}

int AliceActuator::getBrakeHealthLevel() const {
  if ((DGCgettime() - status->m_brake_update_time) > timeOutDefault)
  {
    return -1;
  }
  else 
  {
    return status->m_brakestatus;
  }
}

int AliceActuator::getSteeringHealthLevel() const {
  if ((DGCgettime() - status->m_steer_update_time) > timeOutDefault)
  {
    return -1;
  }
  else 
  {
    return status->m_steerstatus;
  }
}

int AliceActuator::getTransmissionHealthLevel() const {
  if ((DGCgettime() - status->m_trans_update_time) > timeOutTransmission)
  {
    return -1;
  }
  else 
  {
    return status->m_transstatus;
  }
}






