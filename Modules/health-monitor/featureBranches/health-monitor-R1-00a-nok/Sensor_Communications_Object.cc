/* 
 * Process Control Module for starting/stoping/logging modules remotely. 
 * Date: 15 March, 2007
 * Author: David Trotz
 * CVS: $Id$
 *
 * Modified: Robbie Paolini 7/12/07
*/

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <ctype.h>
#include <time.h>

#include <cotk/cotk.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>

#include "cmdline.h"

#include "Sensor_Communications_Object.hh"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

// Default constructor
Sensor_Communications_Object::Sensor_Communications_Object()
{
  memset(this, 0, sizeof(*this)); //I assume this zeroes out all the member variables in the object???
  return;
}

// Default destructor
Sensor_Communications_Object::~Sensor_Communications_Object()
{
  return;
}

// Parse the command line
int Sensor_Communications_Object::parseCmdLine(int argc, char **argv)
{
  char *configFile;
      
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Get config file from the command line, default is Sensor_Feeder.CFG
  configFile = dgcFindConfigFile(this->options.cfg_arg, "health-monitor"); 

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;

  // Load the configuration file
  if (parseConfigFile(configFile) != 0)
    return -1;
  
  free(configFile);

  return 0;
}

// Parse the config file
int Sensor_Communications_Object::parseConfigFile(char *configFile)
{
  int i;
  SensorData *proc;
    
  // Load options from the configuration file
  if (cmdline_parser_configfile(configFile, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", configFile);

  // Loop through the modules in the configuration file and
  // create corresponding entries in the internal process list.
  for (i = 0; i < (int) this->options.module_id_given; i++)
  {
    MSG("reading option");
    // increment number of processes, assign host and module id
    proc = &this->procs[this->numProcs++];
    proc->id = modulenamefromString(this->options.module_id_arg[i]);
    proc->blobType = SNprocessResponse;
  }
  
  return 0;
}

// Initialize sensnet
int Sensor_Communications_Object::initSensnet(/*const char *configPath*/)
{
  int i;
  SensorData *proc;
  
  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, MODhealthMonitor) != 0)
    return ERROR("unable to connect to sensnet");
      
  // Subscribe to groups (sensnet gives each process has a unique group)
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];

    if (proc->blobType == SNprocessResponse)
    {
      if (sensnet_join(this->sensnet,
                       proc->id, SNprocessResponse, sizeof(ProcessResponse)) != 0)
        return ERROR("unable to join process response");
    }
    else
    {
      if (sensnet_join(this->sensnet,
                       SENSNET_SKYNET_SENSOR, proc->blobType, 0x15000) != 0)
	return ERROR("unable to join astate");
    }
  }
  
  return 0;
}


// Finalize sensnet
int Sensor_Communications_Object::finiSensnet()
{  
  int i;
  SensorData *proc;

  // Clean up SensNet
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];
    if (proc->blobType == SNprocessResponse)
      sensnet_leave(this->sensnet, proc->id, SNprocessResponse);
    else
      sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, proc->blobType);
  }
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}


// Check for changes to the process state
int Sensor_Communications_Object::updateSensorStatus()
{
  int i;
  int status;
  int blobId;
  SensorData *proc;
  ProcessResponse response;
  
  // Don't run too fast
  usleep(100000);
  
  // Wait for new updates (with timeout)
  status = sensnet_wait(sensnet, 100);
  if (status != 0 && status != ETIMEDOUT)
    return ERROR("wait failed %s", strerror(status));

  // Update all entries to show time since last heartbeat
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];    

    memset(&response, 0, sizeof(response));

    if (proc->blobType == SNprocessResponse)
    {
      // Read process response message
      if (sensnet_read(sensnet, proc->id, SNprocessResponse,
                       &blobId, sizeof(response), &response) != 0)
        return -1;
    }
    
    double time;
    
    // Process timestamp
    time = fmod((double) response.timestamp / 1e6, 1000);
    
    proc->healthStatus = response.healthStatus;
    proc->timestamp = time;
  }    
  return 0;
}


