/*!
 * File: Right_Coverage_Zone.cc
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be take care of the coverage zones
 *       around Alice
 *
 */

#include <iostream>
#include <string>
#include "Right_Coverage_Zone.hh"
#include "Alice_Lidar.hh"
using namespace std;


Right_Coverage_Zone::Right_Coverage_Zone(Alice_Lidar* RB_ladar) {
  zoneName = "Right Coverage Zone";
  Right_Bumper_Ladar = RB_ladar;
  coverageLevel = 0;
}


void Right_Coverage_Zone::updateCoverageLevel() {
  switch (Right_Bumper_Ladar->getComponentHealthLevel()) {
    case 2:
      coverageLevel = 4;
      break;
    case 1:
      coverageLevel = 2;
      break;
    case 0:
      coverageLevel = 0;
      break;
    case -1:
      coverageLevel = 0;
      break;
    default:
      cout<< "Error: shouldn't be here" << endl;
  }
}

