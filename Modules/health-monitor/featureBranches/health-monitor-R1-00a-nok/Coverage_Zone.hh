/*!
 * File: Coverage_Zone.hh
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     coverage zones around Alice
 *
 */


#ifndef Coverage_Zone_812ye3hwq823uiw
#define Coverage_Zone_812ye3hwq823uiw

#include <iostream>
#include <string>
using namespace std;


class Coverage_Zone {
  
protected:
  string zoneName;
  int coverageLevel;
  
  virtual void updateCoverageLevel() = 0;

public:
    
  int getCoverageLevel();
  string getName() const;
  
};
#endif Coverage_Zone_812ye3hwq823uiw