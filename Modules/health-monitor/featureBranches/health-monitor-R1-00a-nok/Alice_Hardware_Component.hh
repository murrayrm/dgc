/*!
 * File: Alice_Hardware_Component.h
 * 
 * Author: Chris Schantz
 * Date: July 18th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     various hardware components in Alice.
 *
 */

#include <iostream>
#include <string>
using namespace std;


#ifndef Alice_Hardware_Component_aiusfdhasufidsh
#define Alice_Hardware_Component_aiusfdhasufidsh

class Alice_Hardware_Component {

protected:  
  string componentName; 
  unsigned long long timeOfLastUpdate;
  unsigned long long timeOfLastStatusChange;
  int componentHealth;       //scale will vary based on component type

  void updateTimeStats(int level);    //ment to update the time variables above
    
public:
//  virtual Alice_Hardware_Component() = 0;
  
  //accessors
  string getComponentName() const;
  virtual int getComponentHealthLevel() = 0;
  double getTimeSinceLastUpdate();
  double getTimeSinceLastStatusChange();

  //modifiers
  virtual void setComponentHealthLevel(int level) = 0;
};

#endif Alice_Hardware_Component_aiusfdhasufidsh
