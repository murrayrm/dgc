/*!
 * File: Alice_Lidar.cc
 * 
 * Author: Chris Schantz
 * Date: July 19th 2007
 *
 * Description: Implementation for the abstract base class for the 
 *     various sensor components in Alice.
 *
 */

#include "Alice_Lidar.hh"
#include <dgcutils/DGCutils.hh>

using namespace std;

Alice_Lidar::Alice_Lidar(string name, double timeLimit) {
  if (timeLimit >= 0)
    timeOut = timeLimit;
  componentName = name;
  timeOfLastUpdate = 0;
  timeOfLastStatusChange = 0;
  componentHealth = UNKNOWN;
}

bool Alice_Lidar::checkTimeOut() {  //returns true if timeout hasn't passed
  return ((DGCgettime()-timeOfLastUpdate) <= timeOut);
}

int Alice_Lidar::getComponentHealthLevel() {
  if (checkTimeOut())
    return componentHealth;
  else
    return UNKNOWN;
}
