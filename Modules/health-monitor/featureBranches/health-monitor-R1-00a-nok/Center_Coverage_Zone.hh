/*!
 * File: Center_Coverage_Zone.cc
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be take care of the coverage zones
 *       around Alice
 *
 */

#ifndef Center_Coverage_Zone_812ye3hwq823uiw
#define Center_Coverage_Zone_812ye3hwq823uiw

#include <iostream>
#include <string>
#include "Coverage_Zone.hh"
#include "Alice_Lidar.hh"
using namespace std;

class Center_Coverage_Zone : public Coverage_Zone {

  Alice_Lidar *Center_Bumper_Ladar;
  //Alice_Sensor *Center_Bumper_Ladar;

  void updateCoverageLevel();

public:
  Center_Coverage_Zone(Alice_Lidar* CB_ladar);
    
};

#endif Center_Coverage_Zone_812ye3hwq823uiw
