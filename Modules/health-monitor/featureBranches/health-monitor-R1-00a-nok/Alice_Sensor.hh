/*!
 * File: Alice_Sensor.hh
 * 
 * Author: Chris Schantz
 * Date: July 19th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     various sensors on Alice.
 *
 */

#include "Alice_Hardware_Component.hh"

#ifndef Alice_Sensor_234ew243ew
#define Alice_Sensor_234ew243ew

 enum Sensor_Health_Levels{   //checking on health values
    UNKNOWN = -1,
    PERMANETLY_BAD = 0,
    TEMPORARILY_BAD = 1,
    GOOD = 2
  };

class Alice_Sensor: public Alice_Hardware_Component {
 
public:

  //Accessor
  virtual int getComponentHealthLevel() = 0;

  //Mutator
  void setComponentHealthLevel(int l);
};

#endif Alice_Sensor_234ew243ew
