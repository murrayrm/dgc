/* 
 * Object responsible for Health Monitor's communications inputs
 * Date: 20 July 2007
 * Author: Chris Schantz
 * 
 */


#include "HealthMonitorObject.hh"

bool quit = false;

// Default constructor
HealthMonitorObject::HealthMonitorObject(char *spreadDaemon, int skynetKey, gengetopt_args_info &options)
    : CSkynetContainer(MODhealthMonitor, skynetKey),  
      CStateClient(true)
{
  MSG("In HealthMonitorObject constructor, CStateClient connection estalished");
  
  console = 0;

  if (initSensnet(spreadDaemon, skynetKey) != 0)
  {
    MSG("initalizing sensnet failed");
    return;
  }

  MSG("Sensnet connection established");

  //Skynet Talker 
  outTalker = new SkynetTalker<VehicleCapability>(skynetKey, SNvehicleCapability, MODhealthMonitor);

  cout << "outTalker address: " << outTalker << endl;

  cout << "skynetKey: " << skynetKey << endl;

  MSG("Skynettalker connection established");

  // populate our sensors  
  initSensors(options);

  MSG("Sensors Initialized");

  // initalize our CoverageZones object
  zones = new CoverageZones(sensors);

  MSG("CoverageZones Initialized");

  actuators = new AliceActuator(&m_actuatorState);

  MSG("Actuator Object Initalized");

  capabilities = new AliceCapability(actuators, zones, outTalker);

  MSG("Capabilities Object Initalized");

  if (!options.disable_console_flag)
  {
    initConsole(skynetKey);
  }

  MSG("Console Initalized");

  return;
}

// Default destructor - close out or sensnet subscriptions
HealthMonitorObject::~HealthMonitorObject()
{
  if (console)
  {
    cotk_close(console);
    cotk_free(console);
    console = NULL;
  }
  finiSensnet();
  return;
}


// Initialize sensnet
int HealthMonitorObject::initSensnet(char *spreadDaemon, int skynetKey)
{
  MSG("in initalizing sensnet function");
  // Initialize SensNet
  sensnet = sensnet_alloc();
  MSG("sensnet handle allocated");
  if (sensnet_connect(sensnet, spreadDaemon, skynetKey, MODhealthMonitor) != 0)
  {
    return ERROR("unable to connect to sensnet");
  }
  MSG("initalizing sensnet done");
  return 0;    
}

void HealthMonitorObject::initSensors(gengetopt_args_info &options)
{
  MSG("Initializing Sensors");
  sensors.clear();
  for (int i = 0; i < (int)options.sensor_id_given; i++)
  {
    sensors.push_back(AliceSensor(options.sensor_id_arg[i], modulenamefromString(options.sensor_id_arg[i]), sensnet));
  }
  assert((int)sensors.size() == (int)options.sensor_id_given);
}  

// Finalize sensnet
int HealthMonitorObject::finiSensnet()
{  
  sensnet_disconnect(sensnet);
  sensnet_free(sensnet);
  sensnet = NULL;
  return 0;
}


// Update hardware components
int HealthMonitorObject::updateComponents()
{
  int status = sensnet_wait(sensnet, 100);  //miliseconds- will wait for up to .1 seconds to get a message here
  if (status != 0 && status != ETIMEDOUT)
    return ERROR("wait failed %s", strerror(status));
  
  UpdateActuatorState();
  
  for (int i = 0; i < (int)sensors.size(); i++)
  {
    sensors[i].update();
  }

  return 0;
}



void HealthMonitorObject::initConsole(int skynetKey)
{
  MSG("In initializinc Console Function");
  const char *layout =
    "-------------------------------------------------------------------------------\n"
    "|HealthMonitor                                 Skynet Key:    %_skynet_key_%  |\n"
    "------------------------------------------------------------------------------|\n"
    "|Sensor Health Levels:                                                        |\n"
    "|   LFBumper_Ladar: %_ladarLFBumper_%  Riegl_Ladar: %_ladarRiegl_%            |\n"
    "|   MFBumper_Ladar: %_ladarMFBumper_%  MFLong_cam:  %_stereoMFLong_%          |\n"
    "|   RFBumper_Ladar: %_ladarRFBumper_%  MFMed_cam:   %_stereoMFMedium_%        |\n"
    "|   MRBumper_Ladar: %_ladarMRBumper_%  LFShort_cam: %_stereoLFShort_%         |\n"
    "|   LFRoof_Ladar:   %_ladarLFRoof_%    RFShotr_cam: %_stereoRFShort_%         |\n"
    "|   RFRoof_Ladar:   %_ladarRFRoof_%    Radar:       %_radar_%                 |\n"
    "|Coverage Levels:                     Actuator Status:                        |\n"
    "|   Center Near: %_centerNear_%        Throttle:     %_throttle_%             |\n"
    "|   Center Far:  %_centerFar_%         Brake:        %_brake_%                |\n"
    "|   Left Zone:   %_leftZone_%          Steering:     %_steering_%             |\n"
    "|   Right Zone:  %_rightZone_%         Transmission: %_transmission_%         |\n"  
    "|   Rear Zone:   %_rearZone_%                                                 |\n"
    "|Vehicle Capabilities:                                                        |\n"
    "|   Left-turn:     %_capLeft_%         Nominal Driving:  %_capDriving_%       |\n"
    "|   Right-turn:    %_capRight_%        Nominal Stopping: %_capStopping_%      |\n"
    "|   Straight-turn: %_capStraight_%     Nominal Zone:     %_capZone_%          |\n"
    "|   U-turn:        %_capUturn_%        Nominal New:      %_capNew_%           |\n"
    "-----------------------------------[%QUIT%]------------------------------------\n";
                                                 

  char temp[8192];
    
  // Initialize the console template
  memset(temp, 0, sizeof(temp)); 
  strcpy(temp, layout);

  // Initialize console
  console = cotk_alloc();
  assert(console);

  MSG("Console allocated");

  // Set the console template
  cotk_bind_template(console, temp);

  MSG("Console template bound");

  // Bind Quit
  cotk_bind_button(console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) &HealthMonitorObject::onUserQuit, this);

  MSG("Console Quit button bound");
  
  //Initialize the display
  if (cotk_open(console, NULL) != 0)
  {
    cout << "error on initalizing consle display32yrweouyr2398yrw4638i";
  }

  cout << "Console opened" << endl;
 
  cotk_printf(console,"%_skynet_key_%", A_NORMAL, "%05d", skynetKey); 
  MSG("SkynetKey Printed");

}

void HealthMonitorObject::updateConsole()
{
  string name;
  string token;
  int status;

  // take care of sensors
  for (int i = 0; i < (int)sensors.size(); i++)
  {
    status = sensors[i].getComponentHealthLevel();
    name = sensors[i].getComponentNameShort();
    token = "%_" + name + "_%";
    cotk_printf(console, token.c_str(), A_NORMAL, "%2d", status);
  }

  // take care of Coverage zones
  //cout << "Center near coverage level: " << zones->getCenterNearCoverageLevel() << endl;

  cotk_printf(console, "%_centerNear_%", A_NORMAL, "%3.2f", zones->getCenterNearCoverageLevel());
  cotk_printf(console, "%_centerFar_%", A_NORMAL, "%3.2f", zones->getCenterFarCoverageLevel());
  cotk_printf(console, "%_leftZone_%", A_NORMAL, "%3.2f", zones->getLeftCoverageLevel());
  cotk_printf(console, "%_rightZone_%", A_NORMAL, "%3.2f", zones->getRightCoverageLevel());
  cotk_printf(console, "%_rearZone_%", A_NORMAL, "%3.2f", zones->getRearCoverageLevel());

  // take care of actuators
  //cout << "throttle: " << actuators->getThrottleHealthLevel() << endl;
  
  cotk_printf(console, "%_throttle_%", A_NORMAL, "%2d", actuators->getThrottleHealthLevel());
  cotk_printf(console, "%_brake_%", A_NORMAL, "%2d", actuators->getBrakeHealthLevel());
  cotk_printf(console, "%_steering_%", A_NORMAL, "%2d", actuators->getSteeringHealthLevel());
  cotk_printf(console, "%_transmission_%", A_NORMAL, "%2d", actuators->getTransmissionHealthLevel());
  
  // take care of capabilities
  cotk_printf(console, "%_capLeft_%", A_NORMAL, "%2d", capabilities->getLeftTurnCap());
  cotk_printf(console, "%_capRight_%", A_NORMAL, "%2d", capabilities->getRightTurnCap()); 
  cotk_printf(console, "%_capStraight_%", A_NORMAL, "%2d", capabilities->getStraightTurnCap()); 
  cotk_printf(console, "%_capUturn_%", A_NORMAL, "%2d", capabilities->getUTurnCap());
  cotk_printf(console, "%_capDriving_%", A_NORMAL, "%2d", capabilities->getNominalDrivingCap());   
  cotk_printf(console, "%_capStopping_%", A_NORMAL, "%2d", capabilities->getNominalStoppingCap()); 
  cotk_printf(console, "%_capZone_%", A_NORMAL, "%2d", capabilities->getNominalZoneCap()); 
  cotk_printf(console, "%_capNew_%", A_NORMAL, "%2d", capabilities->getNominalNewCap()); 

  //cout << "calling cotk update: " << cotk_update(console) << endl;
  cotk_update(console);
}
  

void HealthMonitorObject::mainLoop()
{
  int counter = 0;
  MSG("Entering Main Loop");
  while (!quit)
  {
    usleep(100000);
    if (updateComponents() != 0)
    {
      MSG("update call failed");
      break;
    }

    for (int i = 0; i < (int)sensors.size(); i++)
    {
      int status = sensors[i].getComponentHealthLevel();
      string name = sensors[i].getComponentNameShort();
      if (!console)
	cout << name << ": " << status << endl;
    }

    zones->updateCoverageLevels();

    capabilities->update();

    if (!console) {
      cout << "Center Near: " <<  zones->getCenterNearCoverageLevel() << endl;
      cout << "Center Far: " <<  zones->getCenterFarCoverageLevel() << endl;
      cout << "Left: " <<  zones->getLeftCoverageLevel() << endl;
      cout << "Right: " <<  zones->getRightCoverageLevel() << endl;
      cout << "Rear: " <<  zones->getRearCoverageLevel() << endl;
      
      cout << "throttle: " << actuators->getThrottleHealthLevel() << endl;
      cout << "brake: " << actuators->getBrakeHealthLevel() << endl;
      cout << "steering: " << actuators->getSteeringHealthLevel() << endl;
      cout << "transmission: " << actuators->getTransmissionHealthLevel() << endl;  
    }

    MSG("broadcasting");
    capabilities->broadcast();
 
    if(console)
    {
      updateConsole();
    }
    if (!console)
      cout << "looping: " << counter++ << endl;
  }
}
    
// Handle quit button press
int HealthMonitorObject::onUserQuit(cotk_t *console, HealthMonitorObject *self, const char *token)
{
  MSG("Quit button pressed");
  quit = true;
  return 0;
}




