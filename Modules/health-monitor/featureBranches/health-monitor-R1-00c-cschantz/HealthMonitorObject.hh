/*!
 *  Author: Chris Schantz
 *  Date: July 25th 2007
 * 
 *  Description: Main health Monitor functionality
 *
 */

#ifndef HealthMonitorObject_HH
#define HealthMonitorObject_HH

#include <ncurses.h>
#include <iostream>
#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <ctype.h>
#include <time.h>
#include <list>
#include <vector>
#include <pthread.h>


#include <cotk/cotk.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>
#include <interfaces/VehicleCapability.h>
#include <skynettalker/SkynetTalker.hh>
#include <skynettalker/StateClient.hh>

#include "cmdline.h"
#include "AliceSensor.hh"
#include "AliceActuator.hh"
#include "CoverageZones.hh"
#include "AliceCapability.hh"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

// Global quit variable
extern bool quit;

class HealthMonitorObject : public CStateClient {

  // Initialize sensnet
  int initSensnet(char *spreadDaemon, int skynetKey);
  
  // Finalize sensnet
  int finiSensnet();
  
  // Initialize console display
  void initConsole(int skynetKey);

  // Callback for quit button
  static int onUserQuit(cotk_t *console, HealthMonitorObject *self, const char *token);

  // Create and bind Sensors
  void initSensors(gengetopt_args_info &options);
  
  // Update components of type actuator
  int updateComponents();

  // Update components of type sensor
  void updateConsole();

  // Skynet Talker
  SkynetTalker<VehicleCapability>* outTalker;

  // Console text display
  cotk_t *console;

  // SensNet handle
  sensnet_t *sensnet;

  //Collection of our Sensor Objects
  vector<AliceSensor> sensors;
  
  AliceActuator *actuators;

  CoverageZones *zones;

  AliceCapability *capabilities;

  public:   

  /// Default constructor
  HealthMonitorObject (char* spreadDaemon, int skynetKey, gengetopt_args_info &options);

  /// Default destructor
  ~HealthMonitorObject();

  /// Main Program Loop
  void mainLoop();
};

#endif //HealthMonitorObject_HH
