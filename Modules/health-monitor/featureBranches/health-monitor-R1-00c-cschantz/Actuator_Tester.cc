/*!
 * Author: Chris Schantz
 * Date: July 25th 2007
 * 
 * tool to test actuator tracking performance and logg actuator state information
 */

#include "Actuator_Tester.hh"
#include <iostream>
#include <string>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <dgcutils/DGCutils.hh>
#include <skynettalker/SkynetTalker.hh>


using namespace std;

ActuatorTester::ActuatorTester(bool throt, bool decel, bool steer, bool transmis) :
  CSkynetContainer(MODhealthMonitor, 1001),  //hard coded because I am lazy and this is my sn_key
  CStateClient(true) {

displayThrottle = throt;
displayBrake = decel;
displaySteering = steer;
displayTransmission = transmis;
timeStampThrottle = 0;
timeStampBrake = 0;
timeStampSteering = 0;
timeStampTransmission = 0;

}

void ActuatorTester::mainLoop() {

while (true)
{
  UpdateActuatorState();
  usleep(10000);
  if (displayThrottle){
    //if (m_actuatorState.m_gas_update_time != timeStampThrottle) {
      cout << "Throttle commanded: " << m_actuatorState.m_gascmd
           << " position: " << m_actuatorState.m_gaspos 
           << " status: " << m_actuatorState.m_gasstatus 
           << " time stamp: " << m_actuatorState.m_gas_update_time << endl << endl;
      //timeStampThrottle = m_actuatorState.m_gas_update_time;
    //}
  }
  if (displayBrake){
    if (m_actuatorState.m_brake_update_time != timeStampBrake) {
      cout << "Brake commanded: " << m_actuatorState.m_brakecmd
           << " position: " << m_actuatorState.m_brakepos 
           << " status: " << m_actuatorState.m_brakestatus << endl << endl;
      timeStampBrake = m_actuatorState.m_brake_update_time;
    }
  }
  if (displaySteering){
    if (m_actuatorState.m_steer_update_time != timeStampSteering) {
      cout << "Steering commanded: " << m_actuatorState.m_steercmd
           << " position: " << m_actuatorState.m_steerpos 
           << " status: " << m_actuatorState.m_steerstatus << endl << endl;
      timeStampSteering = m_actuatorState.m_steer_update_time;
    }
  }
  if (displayTransmission){
    if (m_actuatorState.m_trans_update_time != timeStampTransmission) {
      cout << "Transmission commanded: " << m_actuatorState.m_transcmd
           << " position: " << m_actuatorState.m_transpos 
           << " status: " << m_actuatorState.m_transstatus
           << " time stamp: " << m_actuatorState.m_trans_update_time << endl << endl;
      timeStampTransmission = m_actuatorState.m_trans_update_time;
    }
  }
}
}


         
