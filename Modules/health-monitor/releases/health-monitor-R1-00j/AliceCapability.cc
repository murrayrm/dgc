/*!
 * File: AliceCapability.cc
 * 
 * Author: Chris Schantz
 * Date: August 2nd 2007
 *
 * Description: This class is ment ttake care of the determination logic
 *              and broadcasting using the skynet talker.
 *
 */

#include <dgcutils/DGCutils.hh>

#include "AliceCapability.hh"

using namespace std;

AliceCapability::AliceCapability(AliceActuator* actuatorPtr, CoverageZones* zonesPtr, AliceApplanix* applanixPtr, SkynetTalker<VehicleCapability>* talkerPtr)
{
  actuator = actuatorPtr;
  zones = zonesPtr;
  applanix = applanixPtr;
  talker = talkerPtr;
  cout << "talker address: " << talker << endl;

  results = new VehicleCapability();
}

AliceCapability::~AliceCapability()
{
  delete results;
}

void AliceCapability::update()
{
  // Only grab there once so they don't change due to timeouts
  float left = zones->getLeftCoverageLevel();
  float right = zones->getRightCoverageLevel();
  float rear = zones->getRearCoverageLevel();
  float far = zones->getCenterFarCoverageLevel();
  float near = zones->getCenterNearCoverageLevel();

  int trans = actuator->getTransmissionHealthLevel();

  int gps = applanix->getApplanixHealthLevel();
    
  assert(left <= 1.0);
  assert(right <= 1.0);
  assert(rear <= 1.0);
  assert(far <= 1.0);
  assert(near <= 1.0);  

  //Our capabilities at these various tasks
  float rightTurnCap = 0.0;
  float leftTurnCap = 0.0;
  float straightTurnCap = 0.0;
  float uTurnCap = 0.0;
  float nominalDrivingCap = 0.0;
  float nominalStoppingCap = 0.0;
  float nominalZoneCap = 0.0;
  

  rightTurnCap = 1.0 - (0.33 * (1 - far) + 0.33 * (1 - left));
  
  leftTurnCap = 1.0 - (1.0 * (1 - far) + 0.67 * (1 - left) + 0.67 * (1 - right));
  
  straightTurnCap = 1.0 - (0.33 * (1 - far) + 0.67 * (1 - left) + 1.0 * (1 - right));
  
  if (trans == 1)
  {
    uTurnCap = rear;
  }
  else
  {
     uTurnCap = 0.0;
  }

  nominalZoneCap = near;

  nominalStoppingCap = far;

  nominalDrivingCap = 1.0;
  if ((gps == 2) || (gps ==3))
  {
    nominalDrivingCap = 0.75;
  } 
  if ((left <= .6) && (right <= .6) && (rear <= .5) && (far <= .4) && (near <= .4))
  {
    nominalDrivingCap = 0.67;
  }
  if ((gps >=4) && (gps <=6))
  {
    nominalDrivingCap = 0.40;
  }
  if ((left == 0.01) && (right == 0.01) && (rear == 0.01) && (far == 0.01) && (near == 0.01))
  {
    nominalDrivingCap = 0.20;
  }
  if (gps >= 7)
  {
    nominalDrivingCap = 0;
  }
  
  // now convert to 1-4 scale and update results struct
  results->intersectionRightTurn = rightTurnCap;
  results->intersectionLeftTurn = leftTurnCap;
  results->intersectionStraightTurn = straightTurnCap;
  results->uturn = uTurnCap;
  results->nominalDriving = nominalDrivingCap;
  results->nominalStopping = nominalStoppingCap;
  results->nominalZoneRegionDriving = nominalZoneCap;
}

void AliceCapability::broadcast() const
{
  talker->send(results);
}







  
