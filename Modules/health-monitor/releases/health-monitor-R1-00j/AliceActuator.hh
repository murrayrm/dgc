/*!
 * File: AliceActuator.hh
 * 
 * Author: Chris Schantz
 * Date: July 24th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     various Actuators on Alice.
 *
 */

#ifndef AliceActuator_HH
#define AliceActuator_HH

#include <interfaces/ActuatorState.h>

class AliceActuator {

  unsigned long long timeOutDefault;
  unsigned long long timeOutTransmission;

  int numMessagesThrottle;
  int numMessagesBrake;
  int numMessagesSteering;
  int numMessagesTransmission;

  unsigned long long lastThrottleTimeStamp;
  unsigned long long lastBrakeTimeStamp;
  unsigned long long lastSteeringTimeStamp;
  unsigned long long lastTransmissionTimeStamp;


  
  ActuatorState* status; // Note the struct this points to will have to be updated by the host program every cycle
  
public:
  
  // messageTimeOut in micro seconds
  AliceActuator(ActuatorState* statusPtr);

  //Accessor
  int getThrottleHealthLevel();
  int getBrakeHealthLevel();
  int getSteeringHealthLevel();
  int getTransmissionHealthLevel();

  int getThrottleHealthLevel(string &report);
  int getBrakeHealthLevel(string &report);
  int getSteeringHealthLevel(string &report);
  int getTransmissionHealthLevel(string &report);
};

#endif //AliceActuator_HH
