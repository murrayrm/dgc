
#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <ctype.h>
#include <time.h>

#include <cotk/cotk.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>
#include <interfaces/VehicleCapability.h>
#include <skynettalker/SkynetTalker.hh>

#include "cmdline.h"

#include "Sensor_Communications_Object.hh"

// Main program thread
int main(int argc, char **argv)
{
  Sensor_Communications_Object *proctl; 
  
  //Vehicle Capability Struct
  VehicleCapability *results;
 
  results = new VehicleCapability();
  
  //just test code
  results->intersectionRightTurn = 3;
 
  // Create module
  proctl = new Sensor_Communications_Object();
  assert(proctl);

  // Parse command line options
  if (proctl->parseCmdLine(argc, argv) != 0)
    return -1;
  
  if (proctl->initSensnet() != 0)
    return -1;

  //Skynet Talker 
  SkynetTalker<VehicleCapability> outTalker(proctl->skynetKey, SNvehicleCapability, MODhealthMonitor);
  
  while (true)
  {
    if (proctl->updateSensorStatus() != 0)
      break;
    for (int i = 0; i < proctl->numProcs; i++)
       cout << "snesor num " << i << " health: " << proctl->procs[i].healthStatus <<endl;
    outTalker.send(results);
  }

  // Clean up
  proctl->finiSensnet();

  cmdline_parser_free(&proctl->options);  
  delete proctl;
    
  return 0;
}
