#include <stdint.h>

/// @brief ProcessControl class
class Sensor_Communications_Object
{
  public:   

  /// Default constructor
  Sensor_Communications_Object();

  /// Default destructor
  ~Sensor_Communications_Object();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  /// Parse the config file
  int parseConfigFile(char *configFile);

  /// Initialize sensnet
  int initSensnet();

  /// Finalize sensnet
  int finiSensnet();

  // Update the display with the current process response state
  int updateSensorStatus();

  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // SensNet handle
  sensnet_t *sensnet;

  // Data for each process
  struct SensorData
  {
    // Process id 
    int id;

    // Expected blob type
    int blobType;

    // Last blob id
    int blobId;

    // Command-line
    char cmd[1024];    
    
    // Current process request
    ProcessRequest request;

    // Sensor Health
    int healthStatus;

    //timeStamp of most recent message
    double timestamp;
  };

  // Process list
  int numProcs;
  SensorData procs[16];
  
};
