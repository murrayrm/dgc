/*!
 * File: Alice_Hardware_Component.cpp
 * 
 * Author: Chris Schantz
 * Date: July 18th 2007
 *
 * Description: Implementation for the abstract base class for the 
 *     various hardware components in Alice.
 *
 */

#include "Alice_Hardware_Component.hh"
#include <string>
#include <dgcutils/DGCutils.hh>

using namespace std;

string Alice_Hardware_Component::getComponentName() const{
  return componentName;
}

double Alice_Hardware_Component::getTimeSinceLastUpdate() {
  return (DGCgettime() - timeOfLastUpdate);
}

double Alice_Hardware_Component::getTimeSinceLastStatusChange() {
  return (DGCgettime() - timeOfLastStatusChange);
}

void Alice_Hardware_Component::updateTimeStats(int level) {
  unsigned long long temp = DGCgettime();
  timeOfLastUpdate = temp;
  if (level != componentHealth)
    timeOfLastStatusChange = temp;
}




