/*!
 * File: AliceCapability.hh
 * 
 * Author: Chris Schantz
 * Date: August 2nd 2007
 *
 * Description: This class is ment ttake care of the determination logic
 *              and broadcasting using the skynet talker.
 *
 */

#ifndef AliceCapability_HH
#define AliceCapability_HH

#include <skynettalker/SkynetTalker.hh>
#include <interfaces/sn_types.h>
#include <interfaces/ActuatorState.h>
#include <interfaces/VehicleCapability.h>

#include "AliceActuator.hh"
#include "CoverageZones.hh"

class AliceCapability {

  // Actuator information
  AliceActuator* actuator;
  
  // Sensor information
  CoverageZones* zones;

  // Used to broadcast
  SkynetTalker<VehicleCapability>* talker;

  // The message to send
  VehicleCapability* results;

  
public:
  
  // messageTimeOut in micro seconds
  AliceCapability(AliceActuator* actuatorPtr, CoverageZones* zonesPtr, SkynetTalker<VehicleCapability>* talkerPtr);

  ~AliceCapability();

  void update();
  void broadcast() const;

  //Accessors
  int getLeftTurnCap() {return results->intersectionLeftTurn; }
  int getRightTurnCap() {return results->intersectionRightTurn; }
  int getStraightTurnCap() {return results->intersectionStraightTurn; }
  int getUTurnCap() {return results->uturn;}
  int getNominalDrivingCap() {return results->nominalDriving; }
  int getNominalStoppingCap() {return results->nominalStopping; }
  int getNominalZoneCap() {return results->nominalZoneRegionDriving; }
  int getNominalNewCap() {return results->nominalNewRegionDriving; }
};

#endif //AliceCapability_HH
