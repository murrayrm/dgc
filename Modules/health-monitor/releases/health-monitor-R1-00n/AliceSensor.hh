/*!
 * File: AliceSensor.hh
 * 
 * Author: Chris Schantz
 * Date: July 19th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     various sensors on Alice.
 *
 */

#ifndef AliceSensor_HH
#define AliceSensor_HH

#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>
#include <interfaces/sn_types.h>

#include "AliceHardwareComponent.hh"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

#define DEFAULT_SENSOR_TIME_LIMIT 2000000   
#define MINIMUM_SENSOR_TIME_LIMIT 10000   // This Magic number should be greater than one loop time through the program- else everything will always be too old.

 enum SensorHealthLevels{   //checking on health values
    UNKNOWN = -1,
    PERMANETLY_BAD = 0,
    TEMPORARILY_BAD = 1,
    GOOD = 2
  };

class AliceSensor: public AliceHardwareComponent {

  unsigned long long timeOut;  //if update time is longer than this switch to unknown

  // Sensor id unique for each sensor
  int id;

  int numMessages;

  // Space for sensnet message update 
  ProcessResponse response;

  bool checkTimeOut() const;

  sensnet_t *sensnet;

public:
  
  // messageTimeOut in micro seconds
  AliceSensor(string name, int sensorId, sensnet_t *sensnet, bool considered = true, unsigned long long newMessageTimeOut = DEFAULT_SENSOR_TIME_LIMIT);
  
  ~AliceSensor();

  //Accessor
  int getComponentHealthLevel() const;
  int getComponentHealthLevel(string &status) const;

  int getNumMessages() const;

  int getSensorId() const;

  //Mutator
  int setComponentHealthLevel(int level, unsigned long long timeStamp);
  int update();
  
};

#endif //AliceSensor_HH
