/*!
 * Author: Chris Schantz
 * Date: July 25th 2007
 * 
 * tool to test actuator tracking performance and logg actuator state information
 */

#include <interfaces/sn_types.h>
#include <skynettalker/StateClient.hh>

#ifndef Actuator_Tester
#define Actuator_Tester

class ActuatorTester : public CStateClient {

  bool displayThrottle;
  bool displayBrake;
  bool displaySteering;
  bool displayTransmission;
  unsigned long long timeStampThrottle;
  unsigned long long timeStampBrake;
  unsigned long long timeStampSteering;
  unsigned long long timeStampTransmission;
  

public:

  // Construstor- tells object which actuators to pay attention too
  ActuatorTester(bool throt, bool decel, bool steer, bool transmis);

  ~ActuatorTester();

  void mainLoop();

};
#endif //Actuator_Tester
