/*!
 * File: Right_Coverage_Zone.cc
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be take care of the coverage zones
 *       around Alice
 *
 */

#ifndef RightCoverageZone_812ye3hwq823uiw
#define RightCoverageZone_812ye3hwq823uiw

#include <iostream>
#include <string>
#include "CoverageZone.hh"
#include "AliceSensor.hh"


class RightCoverageZone : public CoverageZone {

  AliceSensor* Right_Bumper_Ladar;
  

  void updateCoverageLevel();

public:
  RightCoverageZone(AliceSensor* LB_ladar);
    
};

#endif //RightCoverageZone_812ye3hwq823uiw

