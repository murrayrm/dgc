/*!
 * File: Coverage_Zone.cc
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     coverage zones around Alice
 *
 */

#include "CoverageZone.hh"
#include <iostream>
#include <string>
using namespace std;

string CoverageZone::getName() const {
  return zoneName;
}

int CoverageZone::getCoverageLevel() {
  updateCoverageLevel();
  return coverageLevel;
}

CoverageZone::~CoverageZone() {}



