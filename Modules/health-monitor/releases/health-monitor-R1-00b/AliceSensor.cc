/*!
 * File: AliceSensor.cc
 * 
 * Author: Chris Schantz
 * Date: July 19th 2007
 *
 * Description: Implementation for the abstract base class for the 
 *     various sensor components in Alice.
 *
 */

using namespace std;

#include "AliceSensor.hh"
#include <string>
#include <dgcutils/DGCutils.hh>


AliceSensor::AliceSensor() //so I can make a matrix of these things
{}

AliceSensor::AliceSensor(string name, int sensorId, unsigned long long newMessageTimeOut, bool considered)
{
  if (newMessageTimeOut >= MINIMUM_SENSOR_TIME_LIMIT)
  { 
    timeOut = newMessageTimeOut;
  }
  
  componentName = name;
  componentHealth = UNKNOWN;
  underConsideration = considered;
  id = sensorId;
  componentType = SENSOR;

  unsigned long long temp = DGCgettime();

  timeOfLastUpdate = temp;
  timeOfLastStatusChange = temp;
  timeOfLastConsiderationToggle = temp;
}

int AliceSensor::setComponentHealthLevel(int level, unsigned long long timeStamp) {
  if ((level == UNKNOWN )||(level == PERMANETLY_BAD)||(level == TEMPORARILY_BAD)||(level == GOOD))
  {
    updateTimeStats(level, timeStamp); // valid level, so update our time variables
    componentHealth = level;
    return 0;               //no error return code
  }
  else
  {
    updateTimeStats(UNKNOWN, DGCgettime());
    componentHealth = UNKNOWN;
    return 1;               //error return code
  }
}

bool AliceSensor::checkTimeOut() const {  //returns true if timeout hasn't passed
  return ((DGCgettime()-timeOfLastUpdate) <= timeOut);
}

int AliceSensor::getComponentHealthLevel() const {
  if (checkTimeOut()) 
  {
    return componentHealth;
  }
  else 
  {
    return UNKNOWN;
  }
}

int AliceSensor::getSensorId() const {
  return id;
}
