/*!
 * File: Center_Coverage_Zone.cc
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be take care of the coverage zones
 *       around Alice
 *
 */

#ifndef CenterCoverageZone_812ye3hwq823uiw
#define CenterCoverageZone_812ye3hwq823uiw

#include <iostream>
#include <string>
#include "CoverageZone.hh"
#include "AliceSensor.hh"

class CenterCoverageZone : public CoverageZone {

  AliceSensor* Center_Bumper_Ladar;
  
  void updateCoverageLevel();

public:
  CenterCoverageZone(AliceSensor* CB_ladar);
    
};

#endif //CenterCoverageZone_812ye3hwq823uiw
