/*!
 * File: AliceActuator.hh
 * 
 * Author: Chris Schantz
 * Date: July 24th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     various Actuators on Alice.
 *
 */

#ifndef AliceActuator_234wes54er
#define AliceActuator_234wes54er

#include "AliceHardwareComponent.hh"

#define DEFAULT_ACTUATOR_TIME_LIMIT 1000000   
#define MINIMUM_ACTUATOR_TIME_LIMIT 10000   // This Magic number should be greater than one loop time through the program- else everything will always be too old.

enum ActuatorHealthLevels {   //checking on health values
    OFF = 0,
    ON = 1,
};

enum ActuatorIds {     //to know which kind of actuator we are.
    THROTTLE = 0,
    BRAKE = 1,
    STEERING = 2,
    TRANSMISSION = 3,
    ESTOP = 4
};


class AliceActuator: public AliceHardwareComponent {

  double timeOut;  //if update time is longer than this switch to unknown

  int actuatorId;

  bool checkTimeOut() const;

public:
  
  // messageTimeOut in micro seconds
  AliceActuator(string name, int actId, unsigned long long newMessageTimeOut = DEFAULT_ACTUATOR_TIME_LIMIT, bool considered = true);

  //Accessor
  int getComponentHealthLevel() const;
  int getActuatorId() const;

  //Mutator
  int setComponentHealthLevel(int level, unsigned long long timeStamp);
};

#endif //AliceActuator_234wes54er
