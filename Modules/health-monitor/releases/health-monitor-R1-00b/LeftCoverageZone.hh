/*!
 * File: Left_Coverage_Zone.cc
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be take care of the coverage zones
 *       around Alice
 *
 */

#ifndef LeftCoverageZone_812ye3hwq823uiw
#define LeftCoverageZone_812ye3hwq823uiw

#include <iostream>
#include <string>
#include "CoverageZone.hh"
#include "AliceSensor.hh"


class LeftCoverageZone : public CoverageZone {

  AliceSensor* Left_Bumper_Ladar;
  

  void updateCoverageLevel();

public:
  LeftCoverageZone(AliceSensor* LB_ladar);
    
};

#endif //LeftCoverageZone_812ye3hwq823uiw

