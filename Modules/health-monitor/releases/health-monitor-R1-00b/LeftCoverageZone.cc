/*!
 * File: Left_Coverage_Zone.cc
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be take care of the coverage zones
 *       around Alice
 *
 */

#include <iostream>
#include <string>
#include "LeftCoverageZone.hh"
#include "AliceSensor.hh"
using namespace std;


LeftCoverageZone::LeftCoverageZone(AliceSensor* LB_ladar) {
  zoneName = "Left Coverage Zone";
  Left_Bumper_Ladar = LB_ladar;
  coverageLevel = 0;
}


void LeftCoverageZone::updateCoverageLevel() {
  switch (Left_Bumper_Ladar->getComponentHealthLevel()) {
    case 2:
      coverageLevel = 4;
      break;
    case 1:
      coverageLevel = 2;
      break;
    case 0:
      coverageLevel = 0;
      break;
    case -1:
      coverageLevel = 0;
      break;
    default:
      cout<< "Error: shouldn't be here" << endl;
  }
}







