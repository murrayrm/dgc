/*!
 * File: Coverage_Zone.hh
 * 
 * Author: Chris Schantz
 * Date: July 20th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     coverage zones around Alice
 *
 */


#ifndef CoverageZone_812ye3hwq823uiw
#define CoverageZone_812ye3hwq823uiw

#include <iostream>
#include <string>
using namespace std;


class CoverageZone {
  
protected:
  string zoneName;
  int coverageLevel;
  
  virtual void updateCoverageLevel() = 0;

public:
    
  int getCoverageLevel();
  string getName() const;

  virtual ~CoverageZone() = 0;
  
};
#endif //CoverageZone_812ye3hwq823uiw
