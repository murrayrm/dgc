/*!
 * File: AliceActuator.cc
 * 
 * Author: Chris Schantz
 * Date: July 24th 2007
 *
 * Description: Implementation for the abstract base class for the 
 *     various Actuator components in Alice.
 *
 */

using namespace std;

#include "AliceActuator.hh"
#include <string>
#include <dgcutils/DGCutils.hh>

AliceActuator::AliceActuator(string name, int actId, unsigned long long newMessageTimeOut, bool considered) {
  if (newMessageTimeOut >= MINIMUM_ACTUATOR_TIME_LIMIT) 
    timeOut = newMessageTimeOut;
  
  componentName = name;
  componentHealth = -1;
  underConsideration = considered;
  actuatorId = actId;
  componentType = ACTUATOR;

  unsigned long long temp = DGCgettime();

  timeOfLastUpdate = temp;
  timeOfLastStatusChange = temp;
  timeOfLastConsiderationToggle = temp;
}



int AliceActuator::setComponentHealthLevel(int level,  unsigned long long timeStamp) {
  if ((level == -1 )||(level == OFF)||(level == ON))
  {
    updateTimeStats(level, timeStamp); // valid level, so update our time variables
    componentHealth = level;
    return 0;               //no error return code
  }
  else
  {
    updateTimeStats(-1, DGCgettime());
    componentHealth = -1;
    return 1;               //error return code
  }
}

bool AliceActuator::checkTimeOut() const {  //returns true if timeout hasn't passed
  return ((DGCgettime()-timeOfLastUpdate) <= timeOut);
}

int AliceActuator::getComponentHealthLevel() const {
  if (checkTimeOut()) 
  {
    return componentHealth;
  }
  else 
  {
    return -1;
  }
}

int AliceActuator::getActuatorId() const {
  return actuatorId;
}
