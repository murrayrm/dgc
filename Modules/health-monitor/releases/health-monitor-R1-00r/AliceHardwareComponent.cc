/*!
 * File: AliceHardwareComponent.cpp
 * 
 * Author: Chris Schantz
 * Date: July 18th 2007
 *
 * Description: Implementation for the abstract base class for the 
 *     various hardware components in Alice.
 *
 */

using namespace std;

#include <string>
#include <dgcutils/DGCutils.hh>
#include <sstream>

#include "AliceHardwareComponent.hh"

string AliceHardwareComponent::toString() const{
  stringstream s("");
  s << getComponentNameShort() << ": " << getComponentHealthLevel();
  return s.str();
}

string AliceHardwareComponent::getComponentName() const {
  return componentName;
}

string AliceHardwareComponent::getComponentNameShort() const {
  string temp = componentName;
  //strip these words out of the name.  allows for shortened token names
  string removeMe[11] = {"MOD", "Feeder", "ladar", "umper", "oof", "stereo", "hort", "ong", "edium", "ar", "ie"};  
  
  int position;

  for (int i = 0; i < 11; i++)
  {
    position = temp.find(removeMe[i]);
    
    if(position != (int)string::npos)
    {
      temp.erase(position, removeMe[i].length());
    }
  }
 
  return temp;
}

bool AliceHardwareComponent::getConsiderationValue() const {
  return underConsideration;
}

unsigned long long AliceHardwareComponent::getTimeSinceLastUpdate() {
  return (DGCgettime() - timeOfLastUpdate);
}

unsigned long long AliceHardwareComponent::getTimeSinceLastStatusChange() {
  return (DGCgettime() - timeOfLastStatusChange);
}

unsigned long long AliceHardwareComponent::getTimeSinceLastConsiderationToggle() {
  return (DGCgettime() - timeOfLastConsiderationToggle);
}

void AliceHardwareComponent::updateTimeStats(int level, unsigned long long timeStamp) {
  timeOfLastUpdate = timeStamp;
  if (level != componentHealth)
    timeOfLastStatusChange = timeStamp;
}

void AliceHardwareComponent::setConsiderationValue(bool flag) {
  if (flag != underConsideration)
    timeOfLastConsiderationToggle = DGCgettime();
  underConsideration = flag;
}

int AliceHardwareComponent::getComponentType() const
{
  return componentType;
}

AliceHardwareComponent::~AliceHardwareComponent() {}







