/*!
 * File: AliceHardwareComponent.h
 * 
 * Author: Chris Schantz
 * Date: July 18th 2007
 *
 * Description: This class is ment to be an abstract base class for the 
 *     various hardware components in Alice.
 *
 */

#ifndef AliceHardwareComponent_34er34ew
#define AliceHardwareComponent_34er34ew

using namespace std;

#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum ComponentTypes{   //To keep track of what kind of input updates this object
    SENSOR = 1,
    ACTUATOR = 2,
    APPLANIX = 3,
    PROCESSES = 4,
};

class AliceHardwareComponent {

protected: 
 
  string componentName; 
  
  int componentHealth;       //scale will vary based on component type
  
  int componentType;
  
  bool underConsideration;   //If this component is being listened too or "counted" when determining capability levels.
  
  //Time variables for various time out functions.
  unsigned long long timeOfLastUpdate;
  unsigned long long timeOfLastStatusChange;
  unsigned long long timeOfLastConsiderationToggle;
  
  void updateTimeStats(int level, unsigned long long timeStamp);    //ment to update the time variables above
    
public:
  string toString() const;
  
  //accessors
  string getComponentName() const;
  string getComponentNameShort() const;
  int getComponentType() const;
  bool getConsiderationValue() const;
  virtual int getComponentHealthLevel() const = 0;
  virtual int getComponentHealthLevel(string &status) const = 0;
  unsigned long long getTimeSinceLastUpdate();
  unsigned long long getTimeSinceLastStatusChange();
  unsigned long long getTimeSinceLastConsiderationToggle();

  //modifiers
  virtual int setComponentHealthLevel(int level, unsigned long long timeStamp) = 0;
  virtual int update() = 0;
  void setConsiderationValue(bool flag);

  virtual ~AliceHardwareComponent() = 0;
};

#endif //AliceHardwareComponent_34er34ew

