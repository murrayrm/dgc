/*!
 * File: Health_Monitor.cc
 * 
 * Author: Chris Schantz
 * Date: July 19th 2007
 *
 * Description: For now a test program for my class structure
 *
 */

#include "Alice_Lidar.hh"
#include "Coverage_Zone_Includes.hh"
#include <iostream>
#include <string>
using namespace std;

#define L(x) cout << x->getComponentName() << ": " << x->getComponentHealthLevel() << endl;
#define Z(x) cout << x->getName() << ": " << x->getCoverageLevel() << endl;

int main() {
  Alice_Lidar* Llidar = new Alice_Lidar("Left Bumper Lidar", 5000);
  Alice_Lidar* Clidar = new Alice_Lidar("Center Bumper Lidar", 5000);
  Alice_Lidar* Rlidar = new Alice_Lidar("Right Bumper Lidar", 5000);

  Left_Coverage_Zone * lcv = new Left_Coverage_Zone(Llidar);
  Center_Coverage_Zone * ccv = new Center_Coverage_Zone(Clidar);
  Right_Coverage_Zone * rcv = new Right_Coverage_Zone(Rlidar);


  cout << "Starting test program, initalizing Lidars and Zones" << endl;
  //cout << lidar->getComponentName() << ": " << Clidar->getComponentHealthLevel() << endl;
  L(Llidar);
  L(Clidar);
  L(Rlidar);
  Z(lcv);
  Z(ccv);
  Z(rcv);

  int a, b, c;
  while (true)
  {
    cout << "enter 3 whitespace seperated integer health levels (Left Center Right):" << endl;
    cin >> a >> b >> c;
    cout << endl;
    Llidar->setComponentHealthLevel(a);
    Clidar->setComponentHealthLevel(b);
    Rlidar->setComponentHealthLevel(c);
    L(Llidar);
    L(Clidar);
    L(Rlidar);
    Z(lcv);
    Z(ccv);
    Z(rcv);
  }
  
  int pause;
  
  cin >> pause;

  return 1;
}

