/*!**
 * Morlan Liu and Nok Wongpiromsarn
 * December 8, 2006
 */

#include "RNDF.hh"
#include "dgcutils/ggis.h"
#include <math.h>
// using namespace std;


#define PI 3.14159265
#define MAX_DISTANCE 100000

namespace std
{

/*
#include "LatLon2UTM.hh"
char UTMZone[4];
int RefEllipsoid = 23;
*/

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
/* GPSPoint Class */
GPSPoint::GPSPoint()
{
  GPSPoint(0, 0, 0, 0, 0);	
}

GPSPoint::GPSPoint(int m, int n, int p, double northing, double easting)
{
  segmentID = m;
  laneID = n;
  waypointID = p;
  //this->latitude = latitude;
  //this->longitude = longitude;
  this->northing = northing;
  this->easting = easting;
  myIndex = -1;
  entry = exit = false;
}

GPSPoint::~GPSPoint()
{
  // Do nothing. entryPoints are already deleted in ~Lane, ~ParkingSpot and ~Zone
}

int GPSPoint::getSegmentID()
{
  return segmentID;
}

int GPSPoint::getLaneID()
{
  return laneID;
}

int GPSPoint::getWaypointID()
{
  return waypointID;
}

/*
double GPSPoint::getLatitude()
{
  return this->latitude;
}

double GPSPoint::getLongitude()
{
  return this->longitude;
}
*/

double GPSPoint::getNorthing()
{
  return northing;
}

double GPSPoint::getEasting()
{
  return easting;
}

vector<GPSPoint*> GPSPoint::getEntryPoints()
{
  return entryPoints;
}

int GPSPoint::getMyIndex()
{
  return myIndex;
}

void GPSPoint::setMyIndex(int index)
{
  myIndex = index;
}

void GPSPoint::setIDs(int m, int n, int p)
{
  segmentID = m;
  laneID = n;
  waypointID = p;	
}
  
void GPSPoint::setNorthingEasting(double northing, double easting)
{
  this->northing = northing;
  this->easting = easting;
}

void GPSPoint::setEntry()
{
  entry = true;
}

void GPSPoint::setExit(GPSPoint* entryGPSPoint)
{
  exit = true;
  bool entryGPSPointExist = false;
  for (unsigned i=0; i<entryPoints.size(); i++)
  {
    if (entryGPSPoint == entryPoints[i])
      entryGPSPointExist = true;
  }
  if (!entryGPSPointExist)
    entryPoints.push_back(entryGPSPoint);
}
  
void GPSPoint::setWaypointID(int p)
{
  waypointID = p;
}

bool GPSPoint::isEntry()
{
  return entry;
}

bool GPSPoint::isExit()
{
  return exit;
}

void GPSPoint::print()
{
  cout << segmentID << "." << laneID << "." << waypointID;
}

void GPSPoint::printAll()
{
  cout << segmentID << "." << laneID << "." << waypointID << endl;
  cout << "Entry points: ";
  for (unsigned i=0; i<entryPoints.size(); i++)
  {
  	cout << entryPoints[i]->getSegmentID() << "." << entryPoints[i]->getLaneID() << "." << entryPoints[i]->getWaypointID();
  	cout << "  ";
  }
}



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
/* Waypoint Class */
Waypoint::Waypoint(int m, int n, int p, double northing, double easting) : GPSPoint(m, n, p, northing, easting)
{
  checkpoint = false;
  stopSign = false;
  checkpointID = 0;
}

Waypoint::~Waypoint()
{
}

int Waypoint::getCheckpointID()
{
  if(isCheckpoint())
    return checkpointID;
  else
    return 0;
}

void Waypoint::setCheckpointID(int checkpointID)
{
  checkpoint = true;
  this->checkpointID = checkpointID;
}

void Waypoint::setStopSign()
{
  stopSign = true;
}

bool Waypoint::isCheckpoint()
{
  return checkpoint;
}

bool Waypoint::isStopSign()
{
  return stopSign;
}

void Waypoint::printAll()
{
  cout << segmentID << "." << laneID << "." << waypointID << endl;
  cout << "Entry points: ";
  for (unsigned i=0; i<entryPoints.size(); i++)
  {
  	cout << entryPoints[i]->getSegmentID() << "." << entryPoints[i]->getLaneID() << "." << entryPoints[i]->getWaypointID();
  	cout << "  ";
  }
  cout << endl;
  cout << "checkpoint ID: " << checkpointID << endl;
  cout << "Is checkpoint: " << checkpoint << endl;
  cout << "Is stop sign: " << stopSign << endl;
}

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
/* PerimeterPoint Class */
PerimeterPoint::PerimeterPoint(int m, int n, int p, double northing, double easting) : GPSPoint(m, n, p, northing, easting)
{
}

PerimeterPoint::~PerimeterPoint()
{
}

int PerimeterPoint::getZoneID()
{
  return segmentID;
}

int PerimeterPoint::getPerimeterPointID()
{
  return waypointID;
}



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
/* ParkingSpot Class */
ParkingSpot::ParkingSpot()
{
  ParkingSpot(0, 0);	
}

ParkingSpot::ParkingSpot(int zoneID, int spotID)
{
  this->zoneID = zoneID;
  this->spotID = spotID;
  spotWidth = 0;
  myIndex = -1;
  waypoint1 = NULL;
  waypoint2 = NULL;
}

ParkingSpot::~ParkingSpot()
{
  delete waypoint1;
  delete waypoint2;
}

void ParkingSpot::setSpotWidth(int spotWidth)
{
  this->spotWidth = spotWidth;
}

void ParkingSpot::setWaypoint(int waypointID, Waypoint* waypoint)
{
  if(waypointID == 1)
	waypoint1 = waypoint;
  else if(waypointID == 2)
    waypoint2 = waypoint;
  else
    cerr << "Not a valid waypoint ID. Waypoint ID must be 1 or 2." << endl;
}

int ParkingSpot::getZoneID()
{
  return zoneID;
}

int ParkingSpot::getSpotID()
{
  return spotID;
}

int ParkingSpot::getSpotWidth()
{
  return spotWidth;
}

Waypoint* ParkingSpot::getWaypoint(int waypointID)
{
  if(waypointID == 1)
    return waypoint1;
  else if(waypointID == 2)
    return waypoint2;
  else
    return NULL;
}

int ParkingSpot::getMyIndex()
{
  return myIndex;
}

void ParkingSpot::setMyIndex(int index)
{
  myIndex = index;
}

void ParkingSpot::print()
{
	cout << "Spot " << spotID << ":" << endl;
	cout << "Width: " << spotWidth << endl;
	waypoint1->printAll();
	cout << endl;
	waypoint2->printAll();
	cout << endl;
}


// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
/* Lane Class */
Lane::Lane()
{
	Lane(0, 0, 0);
}

Lane::Lane(int m, int n, int numOfWaypoints)
{
  segmentID = m;
  laneID = n;
  this->numOfWaypoints = numOfWaypoints;
  laneWidth = 0;
  direction = 0;
  myIndex = -1;
  leftBoundary = ROAD_EDGE;
  rightBoundary = ROAD_EDGE;
}

Lane::~Lane()
{
  for(unsigned i = 0; i < waypoints.size(); i++)
    delete waypoints[i];
}

int Lane::getSegmentID()
{
  return segmentID;
}

int Lane::getLaneID()
{
  return laneID;
}

int Lane::getNumOfWaypoints()
{
  return numOfWaypoints;
}

int Lane::getLaneWidth()
{
  return laneWidth;
}

Divider Lane::getLeftBoundary()
{
  return leftBoundary;
}

Divider Lane::getRightBoundary()
{
  return rightBoundary;
}

int Lane::getDirection()
{
  return direction;
}

int Lane::getMyIndex()
{
  return myIndex;
}

void Lane::setMyIndex(int index)
{
  myIndex = index;
}

void Lane::setLaneWidth(int laneWidth)
{
  this->laneWidth = laneWidth;
}

void Lane::setLeftBoundary(string leftBoundary)
{
  if (leftBoundary == "double_yellow")
  	this->leftBoundary = DOUBLE_YELLOW;
  else if (leftBoundary == "broken_white")
  	this->leftBoundary = BROKEN_WHITE;
  else if (leftBoundary == "solid_white")
  	this->leftBoundary = SOLID_WHITE;
  else
  	this->leftBoundary = ROAD_EDGE;
}

void Lane::setLeftBoundary(Divider leftBoundary)
{
  this->leftBoundary = leftBoundary;
}

void Lane::setRightBoundary(string rightBoundary)
{
  if (rightBoundary == "double_yellow")
  	this->rightBoundary = DOUBLE_YELLOW;
  else if (rightBoundary == "broken_white")
  	this->rightBoundary = BROKEN_WHITE;
  else if (rightBoundary == "solid_white")
	this->rightBoundary = SOLID_WHITE;
  else
  	this->rightBoundary = ROAD_EDGE;
}

void Lane::setRightBoundary(Divider rightBoundary)
{
  this->rightBoundary = rightBoundary;
}

void Lane::setNumOfWaypoints(int numOfWaypoints)
{
  this->numOfWaypoints = numOfWaypoints;
}

void Lane::setDirection(int dir)
{
  direction = dir;
}

Waypoint* Lane::getWaypoint(int waypointID)
{
  if(waypointID > 0 && waypointID <= numOfWaypoints)
  {
  	for(unsigned i = 0; i < waypoints.size(); i++)
  	{
   		if (waypoints[i]->getWaypointID() == waypointID)
      		return waypoints[i];
  	}
  }
  return NULL;
}

vector<Waypoint*> Lane::getAllWaypoints()
{
	return waypoints;	
}

bool Lane::addWaypoint(Waypoint* waypoint)
{
  if(waypoint->getSegmentID() == segmentID
    && waypoint->getLaneID() == laneID
    && waypoint->getWaypointID() > 0 
    && waypoint->getWaypointID() <= numOfWaypoints)
    {
    	if (getWaypoint(waypoint->getWaypointID()) == NULL)
    	{
      		waypoints.push_back(waypoint);
      		return true;
    	}
      	else
      	{
      		cerr << "Cannot add waypoint " << waypoint->getWaypointID() << ". This waypoint already exists." << endl;
      		return false;
      	}
    }
  else
  {
    cerr << "Invalid waypoint " << waypoint->getSegmentID() << "." << waypoint->getLaneID() << "." << waypoint->getWaypointID()
	 << " on segment " << segmentID << " lane " << laneID << endl;
  	return false;  
  }
}

void Lane::print()
{
	cout << "Lane " << laneID << ":" << endl;
	cout << "Width: " << laneWidth << endl;
	cout << "Direction: " << direction << endl;
	cout << "Left boundary: " << leftBoundary << endl;
	cout << "Right boundary: " << rightBoundary << endl;
	for (int i=0; i<numOfWaypoints; i++)
	{
		waypoints[i]->printAll();
		cout << endl;	
	}
}


// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
/* Segment Class */
Segment::Segment()
{
  Segment(0, 0);	
}

Segment::Segment(int m, int numOfLanes)
{
  segmentID = m;
  this->numOfLanes = numOfLanes;
  minSpeed = maxSpeed = 0;
}

Segment::~Segment()
{
  for(unsigned i = 0; i < lanes.size(); i++)
    delete lanes[i];
}

int Segment::getSegmentID()
{
  return segmentID;
}

int Segment::getNumOfLanes()
{
  return numOfLanes;
}

string Segment::getSegmentName()
{
  return segmentName;
}

void Segment::setSegmentName(string segmentName)
{
  this->segmentName = segmentName;
}

double Segment::getMinSpeed()
{
  return minSpeed;
}

double Segment::getMaxSpeed()
{
  return maxSpeed;
}

void Segment::setSpeedLimits(double minSpeed, double maxSpeed)
{
  this->minSpeed = minSpeed;
  this->maxSpeed = maxSpeed;
}

Lane* Segment::getLane(int laneID)
{
  if(laneID > 0 && laneID <= numOfLanes)
  {
	  for(unsigned i = 0; i < lanes.size(); i++)
	  {
	    if (lanes[i]->getLaneID() == laneID)
	      return lanes[i];
	  }
  }
  return NULL;
}

vector<Lane*> Segment::getAllLanes()
{
  return lanes;	
}

bool Segment::addLane(Lane* lane)
{  
  if(lane->getSegmentID() == segmentID && lane->getLaneID() > 0 && lane->getLaneID() <= numOfLanes)
  {
  	  if (getLane(lane->getLaneID()) == NULL)
  	  {
      	this->lanes.push_back(lane);
      	return true;
  	  }
  	  else
  	  {
  	  	cerr << "Cannot add lane " << lane->getLaneID() << ". This lane already exists." << endl;
  	  	return false;
  	  }
  }
  else
  {
  	cerr << "Invalid lane." << endl;
  	return false;
  }
}


void Segment::print()
{
	cout << "Segment " << segmentID << ":" << endl;
	cout << "Min Speed: " << minSpeed << endl;
	cout << "Max Speed: " << maxSpeed << endl;
	for (int i=0; i<numOfLanes; i++)
	{
		lanes[i]->print();
		cout << endl;	
	}
}


// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
/* Zone Class */
Zone::Zone()
{
	Zone(0, 0);
}

Zone::Zone(int zoneID, int numOfSpots)
{
  this->zoneID = zoneID;
  this->numOfSpots = numOfSpots;
  zoneName.clear();
  minSpeed = maxSpeed = 0;
}

Zone::~Zone()
{
  unsigned i = 0;
  
  for(i = 0; i < perimeter.size(); i++)
    delete perimeter[i];
    
  for(i = 0; i < spots.size(); i++)
    delete spots[i];
}

void Zone::setZoneName(string zoneName)
{
  this->zoneName = zoneName;
}

void Zone::setSpeedLimits(double minSpeed, double maxSpeed)
{
  this->minSpeed = minSpeed;
  this->maxSpeed = maxSpeed;
}

bool Zone::addPerimeterPoint(PerimeterPoint* perimeterPoint)
{
  if (perimeterPoint->getPerimeterPointID() > 0)
  {
    if (getPerimeterPoint(perimeterPoint->getPerimeterPointID()) == NULL)
    {
      perimeter.push_back(perimeterPoint);
      return true;
    }
    else
    {
      cerr << "Cannot add perimeter point " << perimeterPoint->getPerimeterPointID() 
	   << ". This perimeter point already exists." << endl;
      return false;
    }
  }
  else
  {
    cerr << "Invalid perimeter point" << perimeterPoint->getPerimeterPointID() << endl;
    return false;  
  }
}

bool Zone::addParkingSpot(ParkingSpot* parkingSpot)
{
  if (parkingSpot->getSpotID() > 0 && parkingSpot->getSpotID() <= numOfSpots)
  {
    if (getParkingSpot(parkingSpot->getSpotID()) == NULL)
    {
      spots.push_back(parkingSpot);
      return true;
    }
    else
    {
      cerr << "Cannot add parking spot " << parkingSpot->getSpotID() << ". This parking spot already exists." << endl;
      return false;
    }
  }
  else
  {
    cerr << "Invalid parking spot" << endl;
    return false;  
  }
}

int Zone::getZoneID()
{
  return zoneID;
}

int Zone::getNumOfSpots()
{
  return numOfSpots;
}

double Zone::getMinSpeed()
{
  return minSpeed;
}

double Zone::getMaxSpeed()
{
  return maxSpeed;
}

int Zone::getNumOfPerimeterPoints()
{
  return perimeter.size();
}

string Zone::getZoneName()
{
  return zoneName;
}

PerimeterPoint* Zone::getPerimeterPoint(int perimeterPointID)
{
  if(perimeterPointID > 0)
  {
  	for(unsigned i = 0; i < perimeter.size(); i++)
  	{
    	if (perimeter[i]->getPerimeterPointID() == perimeterPointID)
      		return perimeter[i];
  	}
  }
  return NULL;
}

vector<PerimeterPoint*> Zone::getAllPerimeterPoints()
{
  return perimeter;	
}

ParkingSpot* Zone::getParkingSpot(int spotID)
{
  if(spotID > 0 && spotID <= numOfSpots)
  {
  	for(unsigned i = 0; i < spots.size(); i++)
  	{
    	if (spots[i]->getSpotID() == spotID)
      		return spots[i];
  	}
  }
  return NULL;
}

vector<ParkingSpot*> Zone::getAllSpots()
{
  return spots;	
}

void Zone::print()
{
  cout << "Zone " << zoneID << ":" << endl;
  cout << "Min Speed: " << minSpeed << endl;
  cout << "Max Speed: " << maxSpeed << endl;
  cout << "Parking Spot: "<< endl;
  for (int i=0; i<numOfSpots; i++)
  {
    spots[i]->print();
    cout << endl;	
  }
  cout << "Perimeter Points: " << endl;
  for (int i=0; i<getNumOfPerimeterPoints(); i++)
  {
    perimeter[i]->printAll();
    cout << endl;	
  }
}



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/* RNDF Class */

RNDF::RNDF()
{
  numOfSegments = numOfZones = 0;
  utm.zone = 11;
  utm.letter = 'S';
}

RNDF::RNDF(int numOfSegments, int numOfZones)
{
  this->numOfSegments = numOfSegments;
  this->numOfZones = numOfZones;	
}

RNDF::~RNDF()
{
  for (unsigned i=0; i<segments.size(); i++)
  {
    delete segments[i];
  }
  for (unsigned i=0; i<zones.size(); i++)
  {
    delete zones[i];
  }
}

Segment* RNDF::getSegment(int segmentID)
{
  if(segmentID > 0 && segmentID <= numOfSegments)
  {
  	for(unsigned i = 0; i < segments.size(); i++)
  	{
    	if (segments[i]->getSegmentID() == segmentID)
      		return segments[i];
  	}
  }
  return NULL;
}

vector<Segment*> RNDF::getAllSegments()
{
  return segments;
}

Lane* RNDF::getLane(int segmentID, int laneID)
{
  Segment* tmpSegment = getSegment(segmentID);
  if(tmpSegment != NULL)
    return tmpSegment->getLane(laneID);
  else
    return NULL;
}

vector<Lane*> RNDF::getAdjacentLanes(Lane* lane)
{
  vector<Lane*> adjacentLanes;
  int segmentID = lane->getSegmentID();
  int numOfLanes = getSegment(segmentID)->getNumOfLanes();

  for(int i = 1; i <= numOfLanes; i++)
  {
    Lane* adjLane = getLane(segmentID, i);
    if(i != lane->getLaneID() && lane->getDirection() == adjLane->getDirection())
      adjacentLanes.push_back(adjLane);
  }
      
  return adjacentLanes;
}

vector<Lane*> RNDF::getNonAdjacentLanes(Lane* lane)
{
  vector<Lane*> nonAdjacentLanes;
  int segmentID = lane->getSegmentID();
  int numOfLanes = getSegment(segmentID)->getNumOfLanes();

  for(int i = 1; i <= numOfLanes; i++)
  {
    Lane* nonAdjLane = getLane(segmentID, i);
    if(i != lane->getLaneID() && lane->getDirection() != nonAdjLane->getDirection())
      nonAdjacentLanes.push_back(nonAdjLane);
  }
      
  return nonAdjacentLanes;  
}

Waypoint* RNDF::getWaypoint(int segmentOrZoneID, int laneID, int waypointID)
{
  if(segmentOrZoneID <= numOfSegments && segmentOrZoneID > 0)
  {
  	Lane* tmpLane = getLane(segmentOrZoneID, laneID);
  	if (tmpLane != NULL)
    	return tmpLane->getWaypoint(waypointID);
    else
    	return NULL;
  }
  else if(segmentOrZoneID <= numOfSegments + numOfZones 
    && segmentOrZoneID > numOfSegments && laneID != 0)
  {
  	ParkingSpot* tmpSpot = getSpot(segmentOrZoneID, laneID);
  	if (tmpSpot != NULL)
    	return tmpSpot->getWaypoint(waypointID);
    else
    	return NULL;
  }
  else
    return NULL;
}

Zone* RNDF::getZone(int zoneID)
{
  if(zoneID <= numOfSegments + numOfZones && zoneID > numOfSegments)
  {
  	for(unsigned i = 0; i < zones.size(); i++)
  	{
    	if (zones[i]->getZoneID() == zoneID)
      		return zones[i];
  	}
  }
  return NULL;
}

vector<Zone*> RNDF::getAllZones()
{
  return zones;	
}


PerimeterPoint* RNDF::getPerimeterPoint(int zoneID, int perimeterPointID)
{
  Zone* tmpZone = getZone(zoneID);
  if(tmpZone != NULL)
    return tmpZone->getPerimeterPoint(perimeterPointID);
  else
    return NULL;
}


ParkingSpot* RNDF::getSpot(int zoneID, int spotID)
{
  Zone* tmpZone = getZone(zoneID);
  if(tmpZone != NULL)
    return tmpZone->getParkingSpot(spotID);
  else
    return NULL;
}


int RNDF::getNumOfSegments()
{
  return numOfSegments;
}


int RNDF::getNumOfZones()
{
  return numOfZones;
}


Waypoint* RNDF::getCheckpoint(int checkpointID)
{
  for(unsigned i=0; i<ckpts.size(); i++)
  {
    if(ckpts[i]->getCheckpointID() == checkpointID)
      return ckpts[i];
  }
  return NULL;
}

vector<Waypoint*> RNDF::getAllCheckpoints()
{
  return ckpts;	
}

void RNDF::setNumOfSegments(int numOfSegments)
{
	this->numOfSegments = numOfSegments;
}

void RNDF::setNumOfZones(int numOfZones)
{
	this->numOfZones = numOfZones;
}

bool RNDF::addSegment(Segment* segment)
{
  if (segment == NULL)
  {
  	cerr << "addSegment: Error segment = NULL" << endl;
  	return false;
  }
  if (segment->getSegmentID() > 0 && segment->getSegmentID() <= numOfSegments)
  {
  	if (getSegment(segment->getSegmentID()) == NULL)
  	{
  		segments.push_back(segment);
  		return true;
  	}
  	else
  	{
  		cerr << "Cannot add segment " << segment->getSegmentID() << ". This segment already exists." << endl;
      	return false;
    }
  }
  else
  {
  	cerr << "Invalid segment" << endl;
  	return false;  
  }	
}


bool RNDF::addZone(Zone* zone)
{
  if (zone == NULL)
  {
  	cerr << "addZone: Error zone = NULL" << endl;
  	return false;
  }
  if (zone->getZoneID() > numOfSegments && zone->getZoneID() <= numOfSegments + numOfZones)
  {
  	if (getZone(zone->getZoneID()) == NULL)
  	{
  		zones.push_back(zone);
  		return true;
  	}
  	else
  	{
  		cerr << "Cannot add zone " << zone->getZoneID() << ". This zone already exists." << endl;
      	return false;
    }
  }
  else
  {
  	cerr << "Invalid zone" << endl;
  	return false;  
  }		
}

bool RNDF::addCheckpoint(Waypoint* waypoint)
{
  if (waypoint == NULL)
  {
  	cerr << "addCheckpoint: Error waypoint = NULL" << endl;
  	return false;
  }
  ckpts.push_back(waypoint);
  return false;
}


void RNDF::addExtraWaypoint(int segmentID, int laneID, int waypointID, double northing, double easting)
{
  Lane* parentLane = getLane(segmentID, laneID);
  
  if(parentLane != NULL)
  {
    int numOfWaypoints = parentLane->getNumOfWaypoints();
    parentLane->setNumOfWaypoints(numOfWaypoints + 1);
    for (int i = numOfWaypoints; i >= waypointID; i--)
      parentLane->getWaypoint(i)->setWaypointID(i+1);
    parentLane->addWaypoint(new Waypoint(segmentID, laneID, waypointID, northing, easting));
  }  
}


bool RNDF::loadFile(char* fileName)
{
  int i;
  ifstream file;
  string line;
  string word;
  
  file.open(fileName, ios::in);

  if(!file)
  {
    cerr << "Error: " << fileName << " file not found." << endl;
    return false;
  } 
  
  getline(file, line);
  
  istringstream lineStream(line, ios::in);
  
  lineStream >> word;
  
  if(word == "RNDF_name")
  {
    setSegmentsAndZones(&file);
    
    for(i = 0; i < this->numOfSegments; i++)
      parseSegment(&file);
      
    for(i = 0; i < this->numOfZones; i++)
      parseZone(&file);
      
    file.close();
    file.open(fileName, ios::in);  
    parseExit(&file);
    file.close();
    
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}


void RNDF::assignLaneDirection()
{
  for(int i = 1; i <= numOfSegments; i++)
  {
    int numOfLanes = getSegment(i)->getNumOfLanes();
    bool switchDirection = false;
    for (int j=1; j <= numOfLanes; j++)
    {
      if (!switchDirection)
	getLane(i,j)->setDirection(0);
      else
	getLane(i,j)->setDirection(1);
      if (j < numOfLanes)
      {
	Divider leftBoundary1 = getLane(i,j)->getLeftBoundary();
	Divider leftBoundary2 = getLane(i,j+1)->getLeftBoundary();
	if ((leftBoundary1 == DOUBLE_YELLOW && leftBoundary2 == DOUBLE_YELLOW) ||
	    (leftBoundary1 == ROAD_EDGE && leftBoundary2 == ROAD_EDGE))
	  switchDirection = true;
      }
    }   
  }
}


GPSPoint* RNDF::findClosestGPSPoint(double northing, double easting, int segmentID, int laneID,
    double& distance)
{
  double headingDiff, headingLaneDir;
  return findClosestGPSPoint(northing, easting, segmentID, laneID, distance, false, 0, 0, 0, 
			     headingDiff, headingLaneDir);
}

GPSPoint* RNDF::findClosestGPSPoint(double northing, double easting, int segmentID, int laneID,
    double& distance, bool useHeading, double yaw, double maxHeadingDiff, double maxHeadingLaneDir,
    double& headingDiff, double& headingLaneDir)
{
  double closestDistance;
  GPSPoint* closestGPSPoint;
  double GPSPointNorthing, GPSPointEasting;

  // The case where we're on a segment
  if (segmentID != 0 && laneID != 0 && segmentID <= numOfSegments &&
      laneID <= getSegment(segmentID)->getNumOfLanes())
  {
    closestGPSPoint = getWaypoint(segmentID, laneID, 1);
    GPSPointNorthing = closestGPSPoint->getNorthing();
    GPSPointEasting = closestGPSPoint->getEasting();
    closestDistance = sqrt(pow(northing - GPSPointNorthing, 2) +
        pow(easting - GPSPointEasting, 2));
    headingDiff = atan2(GPSPointEasting - easting, GPSPointNorthing - northing) - yaw;

    int numOfWaypoints = getLane(segmentID, laneID)->getNumOfWaypoints();
    if (numOfWaypoints > 1)
    {
      GPSPoint* tmpPoint = getWaypoint(segmentID, laneID, 2);
      headingLaneDir = atan2(tmpPoint->getEasting() - GPSPointEasting, 
			     tmpPoint->getNorthing() - GPSPointNorthing) - yaw;
    }
    else
    {
      headingLaneDir = 0;
    }

    headingDiff = fabs(headingDiff);
    headingLaneDir = fabs(headingLaneDir);
    if (headingDiff > PI)
    {
      headingDiff = fabs(2*PI - headingDiff);
    }
    if (headingLaneDir > PI)
    {
      headingLaneDir = fabs(2*PI - headingLaneDir);
    }


    if (useHeading && (headingDiff > maxHeadingDiff || headingLaneDir > maxHeadingLaneDir))
    {
      closestDistance = MAX_DISTANCE;
    }

    for (int k = 2; k <= numOfWaypoints; k++)
    {
      GPSPoint* waypoint = getWaypoint(segmentID, laneID, k);
      GPSPointNorthing = waypoint->getNorthing();
      GPSPointEasting = waypoint->getEasting();
      double newDistance = sqrt(pow(northing - GPSPointNorthing, 2) +
          pow(easting - GPSPointEasting, 2));
      double newHeadingDiff = atan2(GPSPointEasting - easting, GPSPointNorthing - northing) - yaw;
      double newHeadingLaneDir = 0;
      if (numOfWaypoints > k)
      {
	GPSPoint* tmpPoint = getWaypoint(segmentID, laneID, k+1);
	newHeadingLaneDir = atan2(tmpPoint->getEasting() - GPSPointEasting, 
				  tmpPoint->getNorthing() - GPSPointNorthing) - yaw;
      }
      else
      {
	GPSPoint* tmpPoint = getWaypoint(segmentID, laneID, k-1);
	newHeadingLaneDir = atan2(GPSPointEasting - tmpPoint->getEasting(), 
				  GPSPointNorthing - tmpPoint->getNorthing()) - yaw;
      }

      newHeadingDiff = fabs(newHeadingDiff);
      newHeadingLaneDir = fabs(newHeadingLaneDir);
      if (newHeadingDiff > PI)
      {
	newHeadingDiff = fabs(2*PI - newHeadingDiff);
      }
      if (newHeadingLaneDir > PI)
      {
	newHeadingLaneDir = fabs(2*PI - newHeadingLaneDir);
      }

      if (newDistance < closestDistance)
      {
	if(!useHeading || (newHeadingDiff < maxHeadingDiff && newHeadingLaneDir < maxHeadingLaneDir ))
	{
	  closestGPSPoint = waypoint;
	  closestDistance = newDistance;
	  headingDiff = newHeadingDiff;
	  headingLaneDir = newHeadingLaneDir;
	}
      }    
    }
  }

  else if (segmentID != 0 && segmentID <= numOfSegments)
  {
    closestGPSPoint = findClosestGPSPoint(northing, easting, segmentID, 1, closestDistance, 
					  useHeading, yaw, maxHeadingDiff, maxHeadingLaneDir,
					  headingDiff, headingLaneDir);
    for (int j = 1; j <= getSegment(segmentID)->getNumOfLanes(); j++)
    {
      double newDistance;
      double newHeadingDiff;
      double newHeadingLaneDir;
      GPSPoint* newClosestGPSPoint = findClosestGPSPoint(northing, easting, segmentID, j, 
							 newDistance, useHeading, yaw, 
							 maxHeadingDiff, maxHeadingLaneDir, 
							 newHeadingDiff, newHeadingLaneDir);
      if (newDistance < closestDistance)
      {
	if(!useHeading || (newHeadingDiff < maxHeadingDiff && newHeadingLaneDir < maxHeadingLaneDir))
	{
	  closestGPSPoint = newClosestGPSPoint;
	  closestDistance = newDistance;
	  headingDiff = newHeadingDiff;
	  headingLaneDir = newHeadingLaneDir;
	}
      }    
    }
  }

  // The case where we're in a zone
  else if (segmentID != 0 && segmentID <= numOfSegments + numOfZones)
  {
    closestGPSPoint = getPerimeterPoint(segmentID, 1);
    GPSPointNorthing = closestGPSPoint->getNorthing();
    GPSPointEasting = closestGPSPoint->getEasting();
    closestDistance = sqrt(pow(northing - GPSPointNorthing, 2) +
        pow(easting - GPSPointEasting, 2));
    headingDiff = atan2(GPSPointEasting - easting, GPSPointNorthing - northing) - yaw;
    headingDiff = fabs(headingDiff);
    if (headingDiff > PI)
    {
      headingDiff = fabs(2*PI - headingDiff);
    }

    headingLaneDir = 0;
    if (useHeading && (headingDiff > maxHeadingDiff))
    {
      closestDistance = MAX_DISTANCE;
    }

    // Check all the perimeter points
    for(int j = 1; j <= getZone(segmentID)->getNumOfPerimeterPoints(); j++)
    {
      GPSPoint* perimeterPoint = getPerimeterPoint(segmentID, j);
      GPSPointNorthing = perimeterPoint->getNorthing();
      GPSPointEasting = perimeterPoint->getEasting();
      double newDistance = sqrt(pow(northing - GPSPointNorthing, 2) +
          pow(easting - GPSPointEasting, 2));
      double newHeadingDiff = atan2(GPSPointEasting - easting, GPSPointNorthing - northing) - yaw;
      newHeadingDiff = fabs(newHeadingDiff);
      if (newHeadingDiff > PI)
      {
	newHeadingDiff = fabs(2*PI - newHeadingDiff);
      }

      if (newDistance < closestDistance)
      {
	if(!useHeading || (newHeadingDiff < maxHeadingDiff))
	{
	  closestGPSPoint = perimeterPoint;
	  closestDistance = newDistance;
	  headingDiff = newHeadingDiff;
	}
      }
    }
    
    // Check all the parking spot waypoints
    for(int j = 1; j <= getZone(segmentID)->getNumOfSpots(); j++)
    {
      for(int k = 1; k <= 2; k++)
      {
        GPSPoint* waypoint = getWaypoint(segmentID, j, k);
        GPSPointNorthing = waypoint->getNorthing();
        GPSPointEasting = waypoint->getEasting();
        double newDistance = sqrt(pow(northing - GPSPointNorthing, 2) +
            pow(easting - GPSPointEasting, 2));
	double newHeadingDiff = atan2(GPSPointEasting - easting, GPSPointNorthing - northing) - yaw;
	newHeadingDiff = fabs(newHeadingDiff);
	if (newHeadingDiff > PI)
	{
	  newHeadingDiff = fabs(2*PI - newHeadingDiff);
	}
        if (newDistance < closestDistance)
        {
	  if(!useHeading || (newHeadingDiff < maxHeadingDiff))
	  {
	    closestGPSPoint = waypoint;
	    closestDistance = newDistance;
	    headingDiff = newHeadingDiff;
	  }
        }    
      }
    }
  }

  else
  {
    closestGPSPoint = findClosestGPSPoint(northing, easting, 1, 0, closestDistance,
					  useHeading, yaw, maxHeadingDiff, maxHeadingLaneDir,
					  headingDiff, headingLaneDir);
    
    for (int i = 2; i <= numOfSegments + numOfZones; i++)
    {
      double newDistance;
      double newHeadingDiff;
      double newHeadingLaneDir;
      GPSPoint* newClosestGPSPoint = findClosestGPSPoint(northing, easting, i, 0, newDistance, 
							 useHeading, yaw, 
							 maxHeadingDiff, maxHeadingLaneDir,
							 newHeadingDiff, newHeadingLaneDir);
      if (newDistance < closestDistance)
      {
	if(!useHeading || (newHeadingDiff < maxHeadingDiff && newHeadingLaneDir < maxHeadingLaneDir))
	{
	  closestGPSPoint = newClosestGPSPoint;
	  closestDistance = newDistance;
	  headingDiff = newHeadingDiff;
	  headingLaneDir = newHeadingLaneDir;
	}
      }    
    }
  }

  distance = closestDistance;
  return closestGPSPoint;
}


void RNDF::print()
{
	for (int i=0; i<numOfSegments; i++)
	{
		segments[i]->print();
		cout << endl;
	}
//	for (int i=0; i<numOfZones; i++)
//	{
//		zones[i]->print();
//		cout << endl;	
//	}
}

//-----------------------------------------------------------------------------------------------------------------------------------
// Private functions
//-----------------------------------------------------------------------------------------------------------------------------------

void RNDF::addSegment(int segmentID, int numOfLanes)
{
  if(segmentID <= this->numOfSegments && segmentID > 0)
  {
    this->segments.push_back(new Segment(segmentID, numOfLanes));
    
    // cout << "Segment " << segmentID << " with " << numOfLanes << " lanes." << endl;
  }
}

void RNDF::setSegmentName(int segmentID, string segmentName)
{
  getSegment(segmentID)->setSegmentName(segmentName);
}

void RNDF::setSpeedLimits(int segOrZoneID, int minSpeed, int maxSpeed)
{
  if(segOrZoneID <= this->numOfSegments + this->numOfZones && segOrZoneID > this->numOfSegments)
  {
    Zone* zone = getZone(segOrZoneID);
    zone->setSpeedLimits(minSpeed, maxSpeed);
    
    // cout << "The minimum speed of zone " << segOrZoneID << " is " << minSpeed << " mph, the maximum speed is " << maxSpeed << " mph." << endl;
  }
  else
  {
    Segment* segment = getSegment(segOrZoneID);
    segment->setSpeedLimits(minSpeed, maxSpeed);

    // cout << "The minimum speed of segment " << segOrZoneID << " is " << minSpeed << " mph, the maximum speed is " << maxSpeed << " mph." << endl;
  }  
}

void RNDF::addLane(int segmentID, int laneID, int numOfWaypoints)
{
  Segment* parentSegment = getSegment(segmentID);
  
  if(parentSegment != NULL)
  {
    parentSegment->addLane(new Lane(segmentID, laneID, numOfWaypoints));
    
    // cout << "  Lane " << segmentID << "." << laneID << " with " << numOfWaypoints << " waypoints." << endl;
  }
}

void RNDF::addWaypoint(int segmentID, int laneID, int waypointID, double lat, double lon)
{
  /*
  double northing, easting;
  LLtoUTM(RefEllipsoid, lat, lon, northing, easting, UTMZone);
  */

  double northing, easting;
  latlon.latitude = lat;
  latlon.longitude = lon;
  gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
  northing = utm.n;
  easting = utm.e;

  Lane* parentLane = getLane(segmentID, laneID);
  
  if(parentLane != NULL)
  {
    parentLane->addWaypoint(new Waypoint(segmentID, laneID, waypointID, northing, easting));
    
    // cout << setprecision(9);
    
    // cout << "    Waypoint " << segmentID << "." << laneID << "." <<  waypointID << "@" << "(" << lat << "," << lon << ")" << endl;
  }
}

void RNDF::setLaneWidth(int segmentID, int laneID, int laneWidth)
{
  Lane* parentLane = getLane(segmentID, laneID);
  
  if(parentLane != NULL)
  {
    parentLane->setLaneWidth(laneWidth);
    // cout << "  Lane width is " << laneWidth << " ft." << endl;
  }
}

void RNDF::setLeftBoundary(int segmentID, int laneID, string leftBoundary)
{
  Lane* parentLane = getLane(segmentID, laneID);
  
  if(parentLane != NULL)
  {
    parentLane->setLeftBoundary(leftBoundary);
    // cout << "  Left boundary is a " << leftBoundary << "." << endl;
  }
}

void RNDF::setRightBoundary(int segmentID, int laneID, string rightBoundary)
{
  Lane* parentLane = getLane(segmentID, laneID);
  
  if(parentLane != NULL)
  {
    parentLane->setRightBoundary(rightBoundary);
    // cout << "  Right boundary is a " << rightBoundary << "." << endl;
  }
}

void RNDF::setCheckpoint(int segmentID, int laneID, int waypointID, int checkpointID)
{
  Waypoint* waypoint = getWaypoint(segmentID, laneID, waypointID);
  
  if(waypoint != NULL)
  {
    waypoint->setCheckpointID(checkpointID);
    ckpts.push_back(waypoint);
    // cout << "    Waypoint " << segmentID << "." << laneID << "." << waypointID << " is checkpoint " << checkpointID << "." << endl;
  }
}

void RNDF::setStopSign(int segmentID, int laneID, int waypointID)
{
  Waypoint* waypoint = getWaypoint(segmentID, laneID, waypointID);
  
  if(waypoint != NULL)
  {
    waypoint->setStopSign();
    // cout << "    Waypoint " << segmentID << "." << laneID << "." << waypointID << " has a stop sign." << endl;
  }
}

void RNDF::setExit(int entrySegmentID, int entryLaneID, int entryWaypointID, int exitSegmentID, int exitLaneID, int exitWaypointID)
{
  GPSPoint* entryWaypoint;
  GPSPoint* exitWaypoint;
  
  if(entryLaneID == 0)
    entryWaypoint = getPerimeterPoint(entrySegmentID, entryWaypointID);
  else
    entryWaypoint = getWaypoint(entrySegmentID, entryLaneID, entryWaypointID);
    
  if(exitLaneID == 0)
    exitWaypoint = getPerimeterPoint(exitSegmentID, exitWaypointID);
  else
    exitWaypoint = getWaypoint(exitSegmentID, exitLaneID, exitWaypointID);
    
  if(entryWaypoint != NULL && exitWaypoint != NULL)
  {
    entryWaypoint->setEntry();
    exitWaypoint->setExit(entryWaypoint);
    
    // cout << "Waypoint " << exitSegmentID << "." << exitLaneID << "." << exitWaypointID << " is an exit waypoint and has entry waypoint " << entrySegmentID << "." << entryLaneID << "." << entryWaypointID << endl;
  }

  Lane* exitLane = getLane(exitSegmentID, exitLaneID);

  if(exitSegmentID <= numOfSegments && exitWaypoint != NULL && 
     exitWaypointID < exitLane->getNumOfWaypoints())
  {
    entryWaypoint = getWaypoint(exitSegmentID, exitLaneID, exitWaypointID+1);
    entryWaypoint->setEntry();
    exitWaypoint->setExit(entryWaypoint);
  }
}

void RNDF::addZone(int zoneID, int numOfSpots)
{
  if(zoneID <= this->numOfSegments + this->numOfZones && zoneID > this->numOfSegments)
  {
    this->zones.push_back(new Zone(zoneID, numOfSpots));
    
    // cout << "Zone " << zoneID << " with " << numOfSpots << " parking spots." << endl;
  }
}

void RNDF::setZoneName(int zoneID, string zoneName)
{
  getZone(zoneID)->setZoneName(zoneName);
}

void RNDF::addPerimeterPoint(int zoneID, int perimeterPointID, double lat, double lon)
{
  /*
  double northing, easting;
  LLtoUTM(RefEllipsoid, lat, lon, northing, easting, UTMZone);
  */

  double northing, easting;
  latlon.latitude = lat;
  latlon.longitude = lon;
  gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
  northing = utm.n;
  easting = utm.e;

  Zone* parentZone = getZone(zoneID);
  
  if(parentZone != NULL)
  {
    parentZone->addPerimeterPoint(new PerimeterPoint(zoneID, 0, perimeterPointID, northing, easting));
    
    // cout << "  Perimeterpoint " << zoneID << ".0." << perimeterPointID <<  "@(" << lat << "," << lon << ")." << endl;
  }
}

void RNDF::addSpot(int zoneID, int spotID)
{
  Zone* parentZone = getZone(zoneID);
  
  if(parentZone != NULL)
  {
    parentZone->addParkingSpot(new ParkingSpot(zoneID, spotID));
    
    // cout << "  Parking spot " << zoneID << "." << spotID << endl;
  }
}  

void RNDF::setSpotWidth(int zoneID, int spotID, int spotWidth)
{
  ParkingSpot* parentSpot = getSpot(zoneID, spotID);
  
  if(parentSpot != NULL)
  {
    parentSpot->setSpotWidth(spotWidth);
    // cout << "  Parking spot width is " << spotWidth << " ft." << endl;
  }
}

void RNDF::addSpotWaypoint(int zoneID, int spotID, int waypointID, double lat, double lon)
{
  /*
  double northing, easting;
  LLtoUTM(RefEllipsoid, lat, lon, northing, easting, UTMZone);
  */

  double northing, easting;
  latlon.latitude = lat;
  latlon.longitude = lon;
  gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
  northing = utm.n;
  easting = utm.e;

  ParkingSpot* parentSpot = getSpot(zoneID, spotID);
  
  if(parentSpot != NULL)
  {
    parentSpot->setWaypoint(waypointID, new Waypoint(zoneID, spotID, waypointID, northing, easting));
    
    // cout << "    Parking spot waypoint " << zoneID << "." << spotID << "." << waypointID << "@(" << lat << "," << lon << ")" << endl;
  }
}

void RNDF::parseSegment(ifstream* file)
{
  int segmentID, numOfLanes;
  string segmentName, line, word;
  char letter;
  
  while(word != "end_segment")
  {
    letter = file->peek();
    
    if(letter == 'l')
    {
        parseLane(file, segmentID);
        
        continue;
    }
  
    getline(*file, line);
      
    istringstream lineStream(line, ios::in);

    lineStream >> word;
          
    if(word != "segment" && word != "num_lanes" && word != "segment_name")
      continue;
    else if(word == "segment")
      lineStream >> segmentID;
    else if(word == "num_lanes")
    {
      lineStream >> numOfLanes;
      
      addSegment(segmentID, numOfLanes);
    }
    else if(word == "segment_name")
    {
      string segmentName;
      
      lineStream >> segmentName;
      
      setSegmentName(segmentID, segmentName);
    }
    else
      continue;
  }
}

void RNDF::parseLane(ifstream* file, int segmentID)
{
  int numOfWaypoints, laneWidth, laneID;
  unsigned i;
  string leftBoundary, rightBoundary, line, word;
  vector<string> checkpoints;
  vector<string> stops;
  char letter;
  
  laneWidth = 0;
  
  while(word != "end_lane")
  {
    letter = file->peek();
    
    if(letter >= '0' && letter <= '9')
      parseWaypoint(file, segmentID, laneID);
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(word != "lane" && word != "num_waypoints" && word != "lane_width" &&
      word != "left_boundary" && word != "right_boundary" && word != "exit"
      && word != "checkpoint" && word != "stop")
      continue;
    else if(word == "lane")
    {
      lineStream >> word;
      
      int periodPosition = word.find(".", 0);
      
      istringstream charStream(word.substr(periodPosition + 1, word.size()), ios::in);
      
      charStream >> laneID;
    }
    else if(word == "num_waypoints")
    {
      lineStream >> numOfWaypoints;

      addLane(segmentID, laneID, numOfWaypoints);
    }
    else if(word == "lane_width")
    {
      string temp;
      
      lineStream >> temp;
      
      istringstream laneWidthStream(temp, ios::in);
      
      laneWidthStream >> laneWidth;
      
      setLaneWidth(segmentID, laneID, laneWidth);
    }
    else if(word == "left_boundary")
    {
      lineStream >> leftBoundary;
      
      setLeftBoundary(segmentID, laneID, leftBoundary);
    }
    else if(word == "right_boundary")
    {
      lineStream >> rightBoundary;
      
      setRightBoundary(segmentID, laneID, rightBoundary);
    }
    else if(word == "checkpoint")
    {
      string waypoint, checkpoint;
     
      lineStream >> waypoint;
      lineStream >> checkpoint;
      
      checkpoints.push_back(waypoint + "." + checkpoint);
    }
    else if(word == "stop")
    {
      lineStream >> word;
      
      stops.push_back(word);
    }
    else
      continue;
  }
  
  if(!checkpoints.empty())
  {
    for(i = 0; i < checkpoints.size(); i++)
    {
      int checkpointID, waypointID;
      string temp;
      
      temp = checkpoints[i];
      
      int firstPeriod = temp.find(".", 0);
      int secondPeriod = temp.find(".", firstPeriod + 1);
      int thirdPeriod = temp.find(".", secondPeriod + 1);

      istringstream waypointStream(temp.substr(secondPeriod + 1, thirdPeriod), ios::in);
      istringstream checkpointStream(temp.substr(thirdPeriod + 1, temp.size()), ios::in);
      
      waypointStream >> waypointID;
      checkpointStream >> checkpointID;
      
      setCheckpoint(segmentID, laneID, waypointID, checkpointID);
    } 
  }
  
  if(!stops.empty())
  {
    for(i = 0; i < stops.size(); i++)
    {
      int waypointID;
      string temp;
      
      temp = stops[i];
      
      int firstPeriod = temp.find(".", 0);
      int secondPeriod = temp.find(".", firstPeriod + 1);

      istringstream waypointStream(temp.substr(secondPeriod + 1, temp.size()), ios::in);
      
      waypointStream >> waypointID;
      
      setStopSign(segmentID, laneID, waypointID);
    } 
  }
}

void RNDF::parseWaypoint(ifstream* file, int segmentID, int laneID)
{
  int waypointID;
  double longitude, latitude;
  string line, word; 

  char letter = 'a';
  
  while(letter != 'e')
  {
    letter = file->peek();
    
    if(letter == 'e')
      continue;
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
      
    int firstPeriod = word.find(".", 0);
    int secondPeriod = word.find(".", firstPeriod + 1);
    
    istringstream charStream(word.substr(secondPeriod + 1, word.size()), ios::in);
    
    charStream >> waypointID;
    
    lineStream >> latitude;
    
    lineStream >> longitude;
    
    addWaypoint(segmentID, laneID, waypointID, latitude, longitude);
  }
}

void RNDF::parseZone(ifstream* file)
{
  int zoneID, numOfSpots;
  string zoneName, line, word;
  char letter;
  
  while(word != "end_zone")
  {
    letter = file->peek();
    
    if(letter == 'p')
    {
      parsePerimeter(file, zoneID);
        
      continue;
    }
    
    if(letter == 's')
    {
      parseSpot(file, zoneID);
      
      continue;
    }
  
    getline(*file, line);
      
    istringstream lineStream(line, ios::in);

    lineStream >> word;
          
    if(word != "zone" && word != "num_spots" && word != "zone_name")
      continue;
    else if(word == "zone")
      lineStream >> zoneID;
    else if(word == "num_spots")
    {
      lineStream >> numOfSpots;
      
      addZone(zoneID, numOfSpots);
    }
    else if(word == "zone_name")
    {
      string zoneName;
      
      lineStream >> zoneName;
      
      setZoneName(zoneID, zoneName);
    }
    else
      continue;
  }
}

void RNDF::parsePerimeter(ifstream* file, int zoneID)
{
  int perimeterPointID;
  double longitude, latitude;
  string line, word;
  char letter;
  
  while(word != "end_perimeter")
  {      
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      int firstPeriod = word.find(".", 0);
      int secondPeriod = word.find(".", firstPeriod + 1);
      
      istringstream charStream(word.substr(secondPeriod + 1, word.size()), ios::in);
      
      charStream >> perimeterPointID;
      
      lineStream >> latitude;
      
      lineStream >> longitude;
      
      addPerimeterPoint(zoneID, perimeterPointID, latitude, longitude);
    }
  }
}

void RNDF::parseSpot(ifstream* file, int zoneID)
{
  string line, word;
  char letter;
  int spotID, spotWidth, waypointID;
  double latitude, longitude; 
  vector<string> checkpoints;
  
  while(word != "end_spot")
  {
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
      
    if(word == "spot")
    {
      lineStream >> word;
      
      int period = word.find(".", 0);
      
      istringstream charStream(word.substr(period + 1, word.size()), ios::in);
      
      charStream >> spotID;
      
      addSpot(zoneID, spotID);
    }
    else if(word == "spot_width")
    {
      lineStream >> spotWidth;
      
      setSpotWidth(zoneID, spotID, spotWidth);
    }
    else if(word == "checkpoint")
    {
      string waypoint, checkpoint;
     
      lineStream >> waypoint;
      lineStream >> checkpoint;
      
      checkpoints.push_back(waypoint + "." + checkpoint);
    }
    else if(letter >= '0' && letter <= '9')
    {      
      int firstPeriod = word.find(".", 0);
      int secondPeriod = word.find(".", firstPeriod + 1);
      
      istringstream charStream(word.substr(secondPeriod + 1, word.size()), ios::in);
      
      charStream >> waypointID;
      
      lineStream >> latitude;
      
      lineStream >> longitude;
      
      addSpotWaypoint(zoneID, spotID, waypointID, latitude, longitude);
    }
    else
      continue;
  }
  
  if(!checkpoints.empty())
  {
    for(unsigned i = 0; i < checkpoints.size(); i++)
    {
      int checkpointID, waypointID;
      string temp;
      
      temp = checkpoints[i];
      
      int firstPeriod = temp.find(".", 0);
      int secondPeriod = temp.find(".", firstPeriod + 1);
      int thirdPeriod = temp.find(".", secondPeriod + 1);

      istringstream waypointStream(temp.substr(secondPeriod + 1, thirdPeriod), ios::in);
      istringstream checkpointStream(temp.substr(thirdPeriod + 1, temp.size()), ios::in);
      
      waypointStream >> waypointID;
      checkpointStream >> checkpointID;
      
      setCheckpoint(zoneID, spotID, waypointID, checkpointID);
    } 
  }
}

void RNDF::parseExit(ifstream* file)
{
  string line, word;
  int entrySegmentID, entryLaneID, entryWaypointID, exitSegmentID, exitLaneID, exitWaypointID;
  
  while(word != "end_file")
  {
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(word == "exit")
    {
      lineStream >> word;
      
      int firstPeriod = word.find(".", 0);
      int secondPeriod = word.find(".", firstPeriod + 1);
      
      istringstream exitSID(word.substr(0, firstPeriod), ios::in);
      istringstream exitLID(word.substr(firstPeriod + 1, secondPeriod), ios::in);
      istringstream exitWPID(word.substr(secondPeriod + 1, word.size()), ios::in);
      
      exitSID >> exitSegmentID;
      
      exitLID >> exitLaneID;
      
      exitWPID >> exitWaypointID;
      
      lineStream >> word;
      
      firstPeriod = word.find(".", 0);
      secondPeriod = word.find(".", firstPeriod + 1);
      
      istringstream entrySID(word.substr(0, firstPeriod), ios::in);
      istringstream entryLID(word.substr(firstPeriod + 1, secondPeriod), ios::in);
      istringstream entryWPID(word.substr(secondPeriod + 1, word.size()), ios::in);
      
      entrySID >> entrySegmentID;
      
      entryLID >> entryLaneID;
      
      entryWPID >> entryWaypointID;
      
      setExit(entrySegmentID, entryLaneID, entryWaypointID, exitSegmentID, exitLaneID, exitWaypointID);
    }    
  }
}


void RNDF::setSegmentsAndZones(ifstream* file)
{
  bool set = false;
  
  while(!set)
  {
    string word, line;
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(word != "num_segments" && word != "num_zones")
      continue;
    
    if(word == "num_segments")
      lineStream >> this->numOfSegments;
    
    if(word == "num_zones")
    {
      lineStream >> this->numOfZones;
        
      set = true;
    }
  }
}

} // namespace std
