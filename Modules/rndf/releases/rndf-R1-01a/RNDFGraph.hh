
/* 
 * Desc: Loads RNDF data into a convenient graph structure.
 * Date: 3 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef RNDF_GRAPH_HH
#define RNDF_GRAPH_HH

/** @file

@brief RNDF data expressed in a convenient graph form.

*/

#include <stdint.h>
#include <vector>


// Forward declarations from the RNDF class
namespace std
{
  class RNDF;
  class GPSPoint;
}


/// @brief Waypoint from the RNDF.
struct RNDFGraphWaypoint
{
  /// Position in site frame
  float px, py;
  
  /// Checkpoint id from the RNDF (zero at non-checkpoints).
  int checkpointId;

  /// RNDF ID information.
  int segmentId, laneId, waypointId;

  /// Bitfield with flags
  struct
  {
    /// Is this waypoint on a stop line?
    uint8_t isStop : 1;

    /// Is this waypoint an exit point?
    uint8_t isExit : 1;

    /// Is this waypoint an entry point?
    uint8_t isEntry : 1;

    /// Is this waypoint in a zone perimeter?
    uint8_t isZonePerimeter : 1;

    /// Is this waypoint on a zone parking spot?
    uint8_t isZoneParking : 1;
    
  } flags;

  /// Nominal lane width from the RNDF
  float laneWidth;
  
  /// Pointer for user data storage
  void *data;

  /// Plan cost (used for recording the distance to a goal).
  float planCost;
  
  /// Next waypoint(s)
  int numNext;
  RNDFGraphWaypoint *next[128];

  /// Previous waypoint(s)
  int numPrev;
  RNDFGraphWaypoint *prev[128];  
};


/// @brief Useful list for RNDF waypoints
typedef std::vector<RNDFGraphWaypoint*> RNDFGraphWaypointList;


/// @brief Convenient representation of the RNDF as a graph.
///
/// The graph is defined in the site frame (identical to UTM apart
/// from a translation).
///
class RNDFGraph
{
  public:

  /// @brief Constructor
  RNDFGraph();

  /// @brief Destructor
  virtual ~RNDFGraph();
  
  private:

  // Hide the copy constructor
  RNDFGraph(const RNDFGraph &that);
  
  public:

  /// @brief Load RNDF file.
  ///
  /// @param[in] filename RNDF file name.  
  int load(char *filename);

  /// @brief Write in binary format to a file handle.
  int writeFile(FILE *file);

  /// @brief Read in binary format from a file handle.
  int readFile(FILE *file);

  /// @brief Get the node corresponding to the given waypoint ID
  RNDFGraphWaypoint *getWaypoint(int segmentId, int laneId, int waypointId);

  /// @brief Get the node corresponding to the given checkpoint ID
  RNDFGraphWaypoint *getCheckpoint(int checkpointId);

  private:

  /// Generate the RNDF
  int genRndf(std::RNDF *rndf);

  /// Generate the waypoints for a segment/lane
  int genRndfLane(std::RNDF *rndf, int segmentId, int laneId);

  /// Generate the waypoints for a zone
  int genRndfZone(std::RNDF *rndf, int zoneId);

  /// Generate the RNDF intersections
  int genRndfIntersections(std::RNDF *rndf);

  /// Insert an arc between waypoints
  int insertArc(RNDFGraphWaypoint *wpA, RNDFGraphWaypoint *wpB);

  public:

  // Site frame origin in UTM frame.
  double siteN, siteE;

  // Zone number and letter for the UTM frame.
  int siteZone, siteLetter;
    
  // RNDF graph, stored as a list of waypoints.  
  int numWaypoints, maxWaypoints;
  RNDFGraphWaypoint *waypoints;

  public:

  /// Predraw the RNDF.
  /// @return On success, returns the display list.
  int predraw(void);

  // Draw a text box
  void drawText(float size, const char *text);

  // GL display list
  int rndfList;
};

#endif

