
/* 
 * Desc: Draw aerial images using OpenGL
 * Date: 22 October 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <glob.h>
#include <float.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glut.h>


#include <dgcutils/dgc_image.h>
#include <dgcutils/ggis.h>

#include "DrawAerial.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
DrawAerial::DrawAerial()
{
  this->numTiles = 0;
  this->maxTiles = 0;
  this->tiles = NULL;
  this->tile = NULL;
  this->tileTex = 0;
  this->siteNorthing = 0;
  this->siteEasting = 0;
  
  return;
}


// Desctructor
DrawAerial::~DrawAerial()
{
  free(this->tiles);
  this->tiles = NULL;
  
  return;
}


// Load map files from a directory
int DrawAerial::load(const char *path, double utmNorthing, double utmEasting)
{
  int i;
  glob_t gb;
  char pattern[1024];
  int lat, lon, scale;
  GisCoordUTM utm;
  GisCoordLatLon geo;
  Tile *tile;

  // Construct pattern for map files
  snprintf(pattern, sizeof(pattern), "%s/tile_*_*_*.ppm", path);

  // Glob the files
  if (glob(pattern, 0, NULL, &gb) != 0)
    return ERROR("unable to glob %s", pattern);

  // Construct pattern for scanf
  snprintf(pattern, sizeof(pattern), "%s/tile_%%d_%%d_%%d.ppm", path);

  // Copy available files into tile storage
  for (i = 0; i < (int) gb.gl_pathc; i++)
  {
    if (this->numTiles >= this->maxTiles)
    {
      this->maxTiles += 32;
      this->tiles = (Tile*) realloc(this->tiles, this->maxTiles * sizeof(this->tiles[0]));
      assert(this->tiles);
    }    
    tile = this->tiles + this->numTiles++;

    // Lat/lon are in micro-degrees; scale in is mm/pixel.    
    if (sscanf(gb.gl_pathv[i], pattern, &lat, &lon, &scale) < 3)
      MSG("syntax error in tile %s", gb.gl_pathv[i]);

    // Convert to UTM coordinates
    geo.latitude = (double) lat * 1e-6;
    geo.longitude = (double) lon * 1e-6;
    gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);

    // Construct tile
    tile->siteNorthing = utm.n - utmNorthing; 
    tile->siteEasting = utm.e - utmEasting;
    tile->scale = (float) scale * 1e-3;
    tile->filename = strdup(gb.gl_pathv[i]);
    
    //MSG("found map tile %.6f %.6f %.3f", lat*1e-6, lon*1e-6, tile->scale);
  }
  MSG("loaded %d map tiles", gb.gl_pathc);
  
  globfree(&gb);
  
  return 0;
}


// Draw the map at a particular location
void DrawAerial::draw(float siteNorthing, float siteEasting)
{
  Tile *tile;
  float px, py;

  // Check for new tile every few meters only
  if (this->tile == NULL ||
      fabs(siteNorthing - this->siteNorthing) > 2.0 || fabs(siteEasting - this->siteEasting) > 2.0)
  {
    this->siteNorthing = siteNorthing;
    this->siteEasting = siteEasting;
        
    // Find the nearest tile
    tile = this->lookupTile(siteNorthing, siteEasting);
    if (tile == NULL)
      return;

    // If the nearest tile has changed, load the new one
    if (tile != this->tile && this->loadTile(tile) != 0)
      return;

    this->tile = tile;
  }
  if (!this->tile)
    return;  

  
  glDisable(GL_BLEND);
  glEnable(GL_TEXTURE_2D);
  glColor3f(1, 1, 1);
  glPolygonMode(GL_FRONT, GL_LINE);
  glPolygonMode(GL_BACK, GL_FILL);
  glBindTexture(GL_TEXTURE_2D, this->tileTex);
  glBegin(GL_QUADS);

  // North-west corner
  px = this->tile->siteNorthing + this->tile->scale*this->tile->rows/2;
  py = this->tile->siteEasting - this->tile->scale*this->tile->cols/2;
  //MSG("tile %f %f", px, py);
  glTexCoord2f(0, 0);
  glVertex2f(px, py);
  
  // North-east corner
  px = this->tile->siteNorthing + this->tile->scale*this->tile->rows/2;
  py = this->tile->siteEasting + this->tile->scale*this->tile->cols/2;
  //MSG("tile %f %f", px, py);
  glTexCoord2d(1, 0);
  glVertex2f(px, py);

  // South-east corner
  px = this->tile->siteNorthing - this->tile->scale*this->tile->rows/2;
  py = this->tile->siteEasting + this->tile->scale*this->tile->cols/2;
  //MSG("tile %f %f", px, py);
  glTexCoord2d(1, 1);
  glVertex2f(px, py);

  // South-west corner
  px = this->tile->siteNorthing - this->tile->scale*this->tile->rows/2;
  py = this->tile->siteEasting - this->tile->scale*this->tile->cols/2;
  //MSG("tile %f %f", px, py);
  glTexCoord2d(0, 1);
  glVertex2f(px, py);  
 
  glEnd();
  glDisable(GL_TEXTURE_2D);

  return;
}


// Find the map closest to the target lat/lon
DrawAerial::Tile *DrawAerial::lookupTile(float siteNorthing, float siteEasting)
{
  int i;
  double dx, dy, dr;
  double mr;
  Tile *tile, *minTile;

  minTile = NULL;
  mr = DBL_MAX;
  
  for (i = 0; i < this->numTiles; i++)
  {
    tile = this->tiles + i;

    dx = siteNorthing - tile->siteNorthing;
    dy = siteEasting - tile->siteEasting;
    dr = dx * dx + dy * dy;
    if (dr < mr)
    {
      mr = dr;
      minTile = tile;
    }
  }
  
  return minTile;
}


// Load the data for a tile
int DrawAerial::loadTile(Tile *tile)
{
  dgc_image_t *image;
  
  // Read image header
  image = dgc_image_alloc_file(tile->filename);
  if (!image)
    return ERROR("unable to load %s", tile->filename);

  tile->rows = image->rows;
  tile->cols = image->cols;

  // Create GL texture
  if (this->tileTex == 0)
    glGenTextures(1, &this->tileTex);
 
  // Copy image into texture
  glBindTexture(GL_TEXTURE_2D, this->tileTex);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  if (image->channels == 1)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4,
                      image->cols, image->rows, GL_LUMINANCE, GL_UNSIGNED_BYTE, image->data);
  else if (image->channels == 3)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4,
                      image->cols, image->rows, GL_RGB, GL_UNSIGNED_BYTE, image->data);

  dgc_image_free(image);
  
  return 0;
}

