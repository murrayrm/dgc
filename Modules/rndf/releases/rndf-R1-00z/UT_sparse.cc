
/* 
 * Desc: Test RNDF graph loader
 * Date: 16 Oct 2007
 * Author: Joshua Doubleday
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include "RNDF.hh"
using namespace std;

// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


int main(int argc, char **argv)
{
  RNDF rndf;

  if (argc < 3)
    return -1;

  // Load the graph from an RNDF
  MSG("loading %s", argv[1]);
  if ( !rndf.loadFile(argv[1]) ) {
    cout << "could not successfully load file" << endl;
    return -1;
  }

  istringstream ss;
  
  Waypoint w1(0,0,0,0.f,0.f,0,0);
  Waypoint w2(w1);

  ss.str(argv[2]);
  rndf.parseWaypointId(&ss, w1);

  ss.str(argv[3]);
  rndf.parseWaypointId(&ss, w2);
  
  bool bSparse = rndf.isSparse(w1,w2);

  cout << "sparse: " << boolalpha << bSparse << endl;
  
  return 0;
}

