/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */

#include "GloNavMapLib.hh"
#include "interfaces/sn_types.h"

using namespace std;

CGloNavMapLib::CGloNavMapLib(int sn_key, char* RNDFFileName)
	: CSkynetContainer(MODgloNavMapLib, sn_key)
{
  m_rndf = new RNDF();
  m_rndf->loadFile(RNDFFileName);
  m_rndf->assignLaneDirection();

  m_rndfRevNum = 0;

  DGCcreateMutex(&m_GloNavMapMutex);
}

CGloNavMapLib::~CGloNavMapLib() 
{
  delete m_rndf;
  DGCdeleteMutex(&m_GloNavMapMutex);
}

void CGloNavMapLib::sendGlobalGloNavMapThread()
{

  // The skynet socket for receiving Global Navigation Map request
  int gloNavMapRequestSocket = m_skynet.listen(SNglobalGloNavMapRequest, MODmissionplanner);
  if(gloNavMapRequestSocket < 0)
    cerr << "GloNavMapLib::sendGlobalGloNavMapThread(): skynet listen returned error" << endl;

  int GloNavMapSocket = m_skynet.get_send_sock(SNglobalGloNavMapFromGloNavMapLib);

  bool bRequestMap;
  
  while(true)
  {
    int numreceived =  m_skynet.get_msg(gloNavMapRequestSocket, &bRequestMap, sizeof(bool), 0);
    cout << "Accepted request from mission planner " << endl;
    if(numreceived > 0 && bRequestMap == true)
    {
      DGClockMutex(&m_GloNavMapMutex);
      SendGloNavMap(GloNavMapSocket, m_rndf);
      DGCunlockMutex(&m_GloNavMapMutex);
      cout << "Sent GloNavMap to mission planner" << endl;
    }
    usleep(100000);
  }
}

void CGloNavMapLib::sendLocalGloNavMapThread()
{
  // The skynet socket for receiving Global Navigation Map request
  int gloNavMapRequestSocket = m_skynet.listen(SNlocalGloNavMapRequest, MODtrafficplanner);
  if(gloNavMapRequestSocket < 0)
    cerr << "GloNavMapLib::sendLocalGloNavMapThread(): skynet listen returned error" << endl;

  int GloNavMapSocket = m_skynet.get_send_sock(SNlocalGloNavMap);
  bool bRequestMap;
  
  while(true)
  {
    int numreceived =  m_skynet.get_msg(gloNavMapRequestSocket, &bRequestMap, sizeof(bool), 0);
    cout << "Accepted request from traffic planner " << endl;
    if(numreceived > 0 && bRequestMap == true)
    {
      DGClockMutex(&m_GloNavMapMutex);
      SendGloNavMap(GloNavMapSocket, m_rndf);
      DGCunlockMutex(&m_GloNavMapMutex);
      cout << "Sent GloNavMap to traffic planner" << endl;
    }
    usleep(100000);
  }
}

void CGloNavMapLib::getGlobalGloNavMapThread()
{
  // The skynet socket for receiving Global Navigation Map
  int gloNavMapSocket = m_skynet.listen(SNglobalGloNavMapFromMission, MODmissionplanner);
  int pSize;
  RNDF* receivedRndf = new RNDF();
  if(gloNavMapSocket < 0)
    cerr << "GloNavMapLib::getGlobalGloNavMapThread(): skynet listen returned error" << endl;
  
  while(true)
  {
    bool gloNavMapReceived = RecvGloNavMap(gloNavMapSocket, receivedRndf, &pSize);
    if (gloNavMapReceived)
    {
      cout << "Received a new GloNav map from mission planner" << endl;
      DGClockMutex(&m_GloNavMapMutex);
      m_rndf = receivedRndf;
      DGCunlockMutex(&m_GloNavMapMutex);
    }
    usleep(100000);
  }
}
