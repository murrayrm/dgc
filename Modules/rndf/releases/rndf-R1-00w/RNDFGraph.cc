
/* 
 * Desc: Loads RNDF data into a convenient graph structure.
 * Date: 3 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#if USE_GL
#include <GL/glut.h>
#endif

#include "RNDF.hh"
#include "RNDFGraph.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
RNDFGraph::RNDFGraph()
{
  this->numWaypoints = this->maxWaypoints = 0;
  this->waypoints = NULL;
  this->rndfList = 0;
  
  return;
}


// Destructor
RNDFGraph::~RNDFGraph()
{  
  if (this->waypoints)
    free(this->waypoints);
  this->waypoints = NULL;
  
  return;
}


// Load RNDF file
int RNDFGraph::load(char *filename, double siteN, double siteE)
{
  int sn, ln;
  std::RNDF rndf;
  std::Segment *segment;
  std::Lane *lane;
  std::Zone *zone;
  std::Waypoint *wp;
  
  // Load RNDF from file
  if (filename)
  {
    MSG("loading RNDF %s", filename);
    if (!rndf.loadFile(filename))
      return ERROR("unable to load %s", filename);
  }

  // Take a first pass to count waypoints 
  this->maxWaypoints = 0;
  for (sn = 0; sn < (int) rndf.getAllSegments().size(); sn++)
  {
    segment = rndf.getAllSegments()[sn];    
    for (ln = 0; ln < (int) segment->getAllLanes().size(); ln++)
    {
      lane = segment->getAllLanes()[ln];
      this->maxWaypoints += lane->getAllWaypoints().size();
    }
  }
  for (sn = 0; sn < (int) rndf.getAllZones().size(); sn++)
  {
    zone = rndf.getAllZones()[sn];
    this->maxWaypoints += zone->getAllPerimeterPoints().size();
    this->maxWaypoints += zone->getAllSpots().size() * 2;
  }
  if (this->maxWaypoints < 1)
    return ERROR("no waypoints found");

  // Initialize the site offset
  if (siteN == 0 && siteE == 0)
  {
    wp = rndf.getWaypoint(1, 1, 1);
    assert(wp);    
    this->siteN = wp->getNorthing();
    this->siteE = wp->getEasting();
  }
  else
  {
    this->siteN = siteN;
    this->siteE = siteE;
  }
  
  // Pre-allocate space for waypoints 
  this->waypoints = (RNDFGraphWaypoint*) calloc(this->maxWaypoints, sizeof(this->waypoints[0]));

  // Generate the RNDF graph
  this->genRndf(&rndf);

  MSG("loaded %d waypoints", this->numWaypoints);
    
  return 0;
}


// Macros for reading and writing
#define FWRITE(file, var) fwrite(var, sizeof(*var), 1, file)
#define FREAD(file, var) fread(var, sizeof(*var), 1, file)


// Write in binary format to a file handle.
int RNDFGraph::writeFile(FILE *file)
{
  int i, j;
  uint32_t index;
  RNDFGraphWaypoint *wp;

  // Write header info
  FWRITE(file, &this->siteN);
  FWRITE(file, &this->siteE);
  
  // First pass: write the waypoints
  FWRITE(file, &this->numWaypoints);
  for (i = 0; i < this->numWaypoints; i++)
  {
    wp = this->waypoints + i;
    FWRITE(file, &wp->px);
    FWRITE(file, &wp->py);
    FWRITE(file, &wp->segmentId);
    FWRITE(file, &wp->laneId);
    FWRITE(file, &wp->waypointId);
    FWRITE(file, &wp->checkpointId);
    FWRITE(file, &wp->flags);
  }

  // Second pass: write the arcs.  We simply store only index, since
  // this is sufficient to rebuild them later.
  for (i = 0; i < this->numWaypoints; i++)
  {
    wp = this->waypoints + i;

    FWRITE(file, &wp->numNext);
    for (j = 0; j < wp->numNext; j++)
    {
      index = wp->next[j] - this->waypoints;
      FWRITE(file, &index);
    }

    FWRITE(file, &wp->numPrev);
    for (j = 0; j < wp->numPrev; j++)
    {
      index = wp->prev[j] - this->waypoints;
      FWRITE(file, &index);
    }
  }  
  
  return 0;
}


// Read in binary format from a file handle.
int RNDFGraph::readFile(FILE *file)
{
  int i, j;
  uint32_t index;
  RNDFGraphWaypoint *wp;

  // Read header info
  FREAD(file, &this->siteN);
  FREAD(file, &this->siteE);

  // First pass: read the waypoints
  FREAD(file, &this->numWaypoints);
  this->maxWaypoints = this->numWaypoints;
  this->waypoints = (RNDFGraphWaypoint*) calloc(this->maxWaypoints, sizeof(this->waypoints[0]));
  for (i = 0; i < this->numWaypoints; i++)
  {
    wp = this->waypoints + i;
    FREAD(file, &wp->px);
    FREAD(file, &wp->py);
    FREAD(file, &wp->segmentId);
    FREAD(file, &wp->laneId);
    FREAD(file, &wp->waypointId);
    FREAD(file, &wp->checkpointId);
    FREAD(file, &wp->flags);
  }

  // Second pass: read the arcs
  for (i = 0; i < this->numWaypoints; i++)
  {
    wp = this->waypoints + i;

    FREAD(file, &wp->numNext);
    for (j = 0; j < wp->numNext; j++)
    {
      FREAD(file, &index);
      wp->next[j] = this->waypoints + index;
    }

    FREAD(file, &wp->numPrev);
    for (j = 0; j < wp->numPrev; j++)
    {
      FREAD(file, &index);
      wp->prev[j] = this->waypoints + index;
    }
  }  
  
  return 0;
}


// Get the waypoint corresponding to the given waypoint ID
RNDFGraphWaypoint *RNDFGraph::getWaypoint(int segmentId, int laneId, int waypointId)
{
  int i;
  RNDFGraphWaypoint *point;

  for (i = 0; i < this->numWaypoints; i++)
  {
    point = this->waypoints + i;
    if (point->segmentId == segmentId &&
        point->laneId == laneId &&
        point->waypointId == waypointId)
      return point;
  }
  
  return NULL;
}


// Get the waypoint corresponding to the given checkpoint ID
RNDFGraphWaypoint *RNDFGraph::getCheckpoint(int checkpointId)
{
  int i;
  RNDFGraphWaypoint *point;

  for (i = 0; i < this->numWaypoints; i++)
  {
    point = this->waypoints + i;
    if (point->checkpointId == checkpointId)
      return point;
  }
  
  return NULL;
}


// Generate the RNDF
int RNDFGraph::genRndf(std::RNDF *rndf)
{
  std::Segment *segment;
  std::Lane *lane;
  std::Zone *zone;
  int sn, ln;

  // Parse out lanes
  for (sn = 0; sn < (int) rndf->getAllSegments().size(); sn++)
  {
    segment = rndf->getAllSegments()[sn];    
    for (ln = 0; ln < (int) segment->getAllLanes().size(); ln++)
    {
      lane = segment->getAllLanes()[ln];      
      this->genRndfLane(rndf, lane->getSegmentID(), lane->getLaneID());
    }
  }

  // Parse out zones
  for (sn = 0; sn < (int) rndf->getAllZones().size(); sn++)
  {
    zone = rndf->getAllZones()[sn];
    this->genRndfZone(rndf, zone->getZoneID());
  }

  // Create the intersections
  this->genRndfIntersections(rndf);

  return 0;
}


// Help macros
#define SITE_X(wp) ((float) ((wp)->getNorthing() - this->siteN))
#define SITE_Y(wp) ((float) ((wp)->getEasting() - this->siteE))


// Generate the waypoints for a segment/lane
int RNDFGraph::genRndfLane(std::RNDF *rndf, int segmentId, int laneId)
{
  int i;
  std::Lane *lane;
  std::vector<std::Waypoint*> waypoints;
  std::Waypoint *wp;
  RNDFGraphWaypoint *waypointA, *waypointB;

  lane = rndf->getLane(segmentId, laneId);
  assert(lane);
  waypoints = lane->getAllWaypoints();

  // Step through all the waypoints in the lane.  
  for (i = 0; i < (int) waypoints.size(); i++)
  {
    wp = waypoints[i];

    // Add new waypoint
    assert(this->numWaypoints < this->maxWaypoints);
    waypointB = this->waypoints + this->numWaypoints++;
    memset(waypointB, 0, sizeof(*waypointB));
    waypointB->segmentId = wp->getSegmentID();
    waypointB->laneId = wp->getLaneID();
    waypointB->waypointId = wp->getWaypointID();
    waypointB->checkpointId = (wp->isCheckpoint() ? wp->getCheckpointID() : 0);
    waypointB->flags.isStop = wp->isStopSign();
    waypointB->flags.isExit = wp->isExit();
    waypointB->flags.isEntry = wp->isEntry();
    waypointB->px = SITE_X(wp);
    waypointB->py = SITE_Y(wp);
    waypointB->laneWidth = lane->getLaneWidth();

    if (i == 0)
      continue;
    if (this->numWaypoints < 2)
      continue;

    // Set up links with previous waypoint (the last one we added must
    // be a waypoint in the same lane).
    waypointA = this->waypoints + this->numWaypoints - 2;
    this->insertArc(waypointA, waypointB);    
  }

  return 0;
}


// Generate the waypoints for a zone
int RNDFGraph::genRndfZone(std::RNDF *rndf, int zoneId)
{
  int i, j;
  std::Zone *zone;
  std::vector<std::PerimeterPoint*> points;
  std::PerimeterPoint *point;
  std::vector<std::ParkingSpot*> spots;
  std::ParkingSpot *spot;
  std::Waypoint *wa, *wb;
  RNDFGraphWaypoint *waypointA, *waypointB;    

  zone = rndf->getZone(zoneId);

  // Step through all the perimeter points in the zone
  points = zone->getAllPerimeterPoints();
  for (i = 0; i < (int) points.size(); i++)
  {
    point = points[i];
    
    // Add new perimeter point
    assert(this->numWaypoints < this->maxWaypoints);
    waypointA = this->waypoints + this->numWaypoints++;
    memset(waypointA, 0, sizeof(*waypointA));
    waypointA->segmentId = point->getSegmentID();
    waypointA->laneId = point->getLaneID();
    waypointA->waypointId = point->getWaypointID();
    // TODO waypointA->checkpointId = (point->isCheckpoint() ? point->getCheckpointID() : 0);
    waypointA->px = SITE_X(point);
    waypointA->py = SITE_Y(point);
    waypointA->flags.isExit = point->isExit();
    waypointA->flags.isEntry = point->isEntry();
    waypointA->flags.isZonePerimeter = true;
  }

  // Join the perimeter in a closed polygon.
  for (i = 0; i < (int) points.size(); i++)
  {
    waypointA = this->waypoints + this->numWaypoints - points.size() + i;
    waypointB = this->waypoints + this->numWaypoints - points.size() + (i + 1) % points.size();
    
    // Add arc
    this->insertArc(waypointA, waypointB);
  }

  // Step through all the spots in the zone.
  spots = zone->getAllSpots();
  for (i = 0; i < (int) spots.size(); i++)
  {
    spot = spots[i];
    
    wa = spot->getWaypoint(1);
    wb = spot->getWaypoint(2);

    // Add new waypoint
    assert(this->numWaypoints < this->maxWaypoints);
    waypointA = this->waypoints + this->numWaypoints++;
    memset(waypointA, 0, sizeof(*waypointA));
    waypointA->segmentId = wa->getSegmentID();
    waypointA->laneId = wa->getLaneID(); 
    waypointA->waypointId = wa->getWaypointID();
    waypointA->checkpointId = (wa->isCheckpoint() ? wa->getCheckpointID() : 0);
    waypointA->px = SITE_X(wa);
    waypointA->py = SITE_Y(wa);
    waypointA->flags.isZoneParking = true;
    
    // Add new waypoint
    assert(this->numWaypoints < this->maxWaypoints);
    waypointB = this->waypoints + this->numWaypoints++;
    memset(waypointB, 0, sizeof(*waypointB));
    waypointB->segmentId = wb->getSegmentID();
    waypointB->laneId = wb->getLaneID();
    waypointB->waypointId = wb->getWaypointID();
    waypointB->checkpointId = (wb->isCheckpoint() ? wb->getCheckpointID() : 0);
    waypointB->px = SITE_X(wb);
    waypointB->py = SITE_Y(wb);
    waypointB->flags.isZoneParking = true;
    
    // Add arc
    this->insertArc(waypointA, waypointB);
  }

  // Hook up the perimeter exit entry points to the parking spots, and
  // each other.
  for (i = 0; i < this->numWaypoints; i++)
  {
    waypointA = this->waypoints + i;
    for (j = 0; j < this->numWaypoints; j++)
    {
      waypointB = this->waypoints + j;
      if (waypointA == waypointB)
        continue;
      if (waypointA->segmentId != waypointB->segmentId)
        continue;
      if (!(waypointA->flags.isZonePerimeter || waypointB->flags.isZonePerimeter))
        continue;

      // Add arc
      if (waypointA->flags.isEntry && waypointB->flags.isZoneParking && waypointB->waypointId == 1)
        this->insertArc(waypointA, waypointB);
      if (waypointA->flags.isZoneParking && waypointA->waypointId == 1 && waypointB->flags.isExit)
        this->insertArc(waypointA, waypointB);
      //if (waypointA->flags.isEntry && waypointB->flags.isExit)
      //  this->insertArc(waypointA, waypointB);      
    }
  }
  
  return 0;
}


// Generate the RNDF intersections
int RNDFGraph::genRndfIntersections(std::RNDF *rndf)
{
  int i, j;
  std::vector<std::GPSPoint*> waypoints;
  std::GPSPoint *wayA, *wayB;
  RNDFGraphWaypoint *waypointA, *waypointB;

  // Step through all the waypoints in our list.
  for (i = 0; i < this->numWaypoints; i++)
  {
    waypointA = this->waypoints + i;

    // Get the list of RNDF waypoints that we can go to
    if (waypointA->laneId > 0)
      wayA = rndf->getWaypoint(waypointA->segmentId, waypointA->laneId, waypointA->waypointId);
    else
      wayA = rndf->getPerimeterPoint(waypointA->segmentId, waypointA->waypointId);
    assert(wayA);
    waypoints = wayA->getEntryPoints();

    for (j = 0; j < (int) waypoints.size(); j++)
    {
      wayB = waypoints[j];
      waypointB = this->getWaypoint(wayB->getSegmentID(), wayB->getLaneID(), wayB->getWaypointID());
      assert(waypointB);

      // Add arc
      this->insertArc(waypointA, waypointB);
    }
  }

  return 0;
}


// Insert an arc between waypoints
int RNDFGraph::insertArc(RNDFGraphWaypoint *wpA, RNDFGraphWaypoint *wpB)
{
  int i;
  RNDFGraphWaypoint *wp;

  // Check for links to self
  if (wpA->segmentId == wpB->segmentId && wpA->laneId == wpB->laneId &&
      wpA->waypointId == wpB->waypointId)
    return ERROR("self arc: %d.%d.%d to %d.%d.%d",
                 wpA->segmentId, wpA->laneId, wpA->waypointId,
                 wpB->segmentId, wpB->laneId, wpB->waypointId);

  // Check for dups.  Seems unlikely, but still...
  for (i = 0; i < wpA->numNext; i++)
  {
    wp = wpA->next[i];
    if (wp->segmentId == wpB->segmentId && wp->laneId == wpB->laneId &&
        wp->waypointId == wpB->waypointId)
      return ERROR("duplicate next arc: %d.%d.%d to %d.%d.%d",
                   wpA->segmentId, wpA->laneId, wpA->waypointId,
                   wpB->segmentId, wpB->laneId, wpB->waypointId);
  }
  for (i = 0; i < wpB->numPrev; i++)
  {
    wp = wpB->prev[i];
    if (wp->segmentId == wpA->segmentId && wp->laneId == wpA->laneId &&
        wp->waypointId == wpA->waypointId)
      return ERROR("duplicate prev arc: %d.%d.%d to %d.%d.%d",
                   wpA->segmentId, wpA->laneId, wpA->waypointId,
                   wpB->segmentId, wpB->laneId, wpB->waypointId);
  }

  // Insert
  assert((size_t) wpA->numNext < sizeof(wpA->next)/sizeof(wpA->next[0]));
  wpA->next[wpA->numNext++] = wpB;
  assert((size_t) wpB->numPrev < sizeof(wpB->prev)/sizeof(wpB->prev[0]));
  wpB->prev[wpB->numPrev++] = wpA;

  return 0;
}


#if USE_GL


// Predraw the RNDF
int RNDFGraph::predraw(void)
{
  int i, j;
  char text[256];
  char flags[256];
  GLUquadric *quad;
  RNDFGraphWaypoint *waypointA, *waypointB;
  
  // Create display list
  if (this->rndfList == 0)
    this->rndfList = glGenLists(1);
  glNewList(this->rndfList, GL_COMPILE);
  quad = gluNewQuadric();
    
  for (i = 0; i < this->numWaypoints; i++)
  {
    waypointA = this->waypoints + i;

    // Set the flag string
    flags[0] = 0;
    strcat(flags, (waypointA->flags.isEntry ? ":en" : ":"));    
    strcat(flags, (waypointA->flags.isExit ? ":ex" : ":"));
    strcat(flags, (waypointA->flags.isZonePerimeter ? ":zp" : ":"));
    strcat(flags, (waypointA->flags.isZoneParking ? ":zs" : ":"));
      
    if (waypointA->checkpointId > 0)
    {
      if (waypointA->flags.isStop)
        glColor3f(1, 0, 0);
      else
        glColor3f(1, 1, 0);
      
      snprintf(text, sizeof(text), "%d.%d.%d\n%s\nCP %d", 
               waypointA->segmentId, waypointA->laneId, waypointA->waypointId, flags,
               waypointA->checkpointId);

      // Draw a ciricle with text
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glLineWidth(2.0);
      glPushMatrix();
      glTranslatef(waypointA->px, waypointA->py, 0);
      glRotatef(180, 0, 1, 0);
      gluDisk(quad, 0.50, 0.50, 16, 1);
      glTranslatef(-0.40, 0, 0);
      glLineWidth(1.0);
      this->drawText(0.1, text);
      glPopMatrix();
    }
    else
    {
      if (waypointA->laneId == 0)
        glColor3f(0, 0, 1);
      else if (waypointA->flags.isStop)
        glColor3f(1, 0, 0);
      else
        glColor3f(1, 0, 1);
      snprintf(text, sizeof(text), "%d.%d.%d\n%s",
               waypointA->segmentId, waypointA->laneId, waypointA->waypointId, flags);

      // Draw a rectnagle with text
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glLineWidth(1.0);
      glPushMatrix();      
      glTranslatef(waypointA->px, waypointA->py, 0);
      glRotatef(180, 0, 1, 0);
      glBegin(GL_QUADS);
      glVertex2f(-0.5, -0.5);
      glVertex2f(+0.5, -0.5);
      glVertex2f(+0.5, +0.5);
      glVertex2f(-0.5, +0.5);
      glEnd();
      glTranslatef(-0.40, 0, 0);
      this->drawText(0.1, text);
      glPopMatrix();
    }
        
    // Draw arcs to next waypoints.
    // Zone perimeters are draw in a different color
    glLineWidth(1.0);
    glBegin(GL_LINES);
    for (j = 0; j < waypointA->numNext; j++)
    {
      waypointB = waypointA->next[j];
      if (waypointA->flags.isZonePerimeter && waypointB->flags.isZonePerimeter)
        glColor3f(0.5, 0.5, 0.5);
      else if (waypointA->flags.isExit)
        glColor3f(0, 0, 1);
      else
        glColor3f(1.0, 0, 1.0);
      glVertex2f(waypointA->px, waypointA->py);
      glVertex2f(waypointB->px, waypointB->py);
    }
    glEnd();

    // Draw arcs to previous waypoints.  These should be identical, so
    // this is a test of graph integrity.
    if (false)
    {
      glColor3f(1, 0, 0);
      glBegin(GL_LINES);
      for (j = 0; j < waypointA->numPrev; j++)
      {
        waypointB = waypointA->prev[j];    
        glVertex3f(waypointA->px, waypointA->py, 1);
        glVertex3f(waypointB->px, waypointB->py, 1);
      }
      glEnd();
    }
  }

  gluDeleteQuadric(quad);
  glEndList();
    
  return this->rndfList;
}


// Draw a text box
void RNDFGraph::drawText(float size, const char *text)
{
  int i, count;
  float sx, sy;

  // Compute the scale factors in x/y directions, based on the 'M'
  // character.
  sx = glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, 'M');
  sy = sx * 1.3;
    
  glPushMatrix();
  glScalef(size / sx, size / sy, 1);
  glTranslatef(0, -sy, 0);

  glPushMatrix();
    
  for (i = 0, count = 0; i < (int) strlen(text); i++, count++)
  {
    if (text[i] == '\n')
    {
      glPopMatrix();
      glTranslatef(0, -sy, 0);
      glPushMatrix();
      count = 0;      
    }
    else
    {
      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[i]);
    }
  }

  glPopMatrix();
  glPopMatrix();

  return;
}


#endif
