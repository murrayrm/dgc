
/* 
 * Desc: Draw misc stuff using OpenGL
 * Date: 19 October 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef DRAW_MISC_HH
#define DRAW_MISC_HH



class DrawMisc
{
  public:

  // Constructor
  DrawMisc();

  public:
  
  /// @brief Draw a set of axes
  void drawAxes(float size);

  /// @brief Draw text.
  /// @param[in] px,py,pz Position.
  /// @param[in] size Text size (m).
  /// @param[in] text Text to draw.
  void drawText(float px, float py, float pz, float size, const char *text);

  /// @brief Draw Alice (vehicle frame)
  void drawAlice(float steerAngle, int signalState = 0);

  /// @brief Predraw a grid and return the display list
  /// @param[in] px,py Coordinates of the grid center.
  /// @param[in] spacing Grid spacing.
  /// @returns Returns a display list.
  int predrawGrid(float px, float py, float spacing);

  public:

  // Display lists
  int gridList;

  private:
  
  // Grid workspace
  float gridPx, gridPy;
};


#endif
