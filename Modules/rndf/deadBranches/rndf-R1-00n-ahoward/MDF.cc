
/* 
 * Desc: MDF parser
 * Date: 24 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "MDF.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
MDF::MDF()
{
  this->numTokens = 0;
  memset(this->tokens, 0, sizeof(this->tokens));  
  
  this->numCheckpoints = 0;
  memset(this->checkpoints, 0, sizeof(this->checkpoints));
 
  return;
}


// Destructor
MDF::~MDF()
{
  return;
}


// Load MDF from a file
int MDF::loadFile(const char *filename)
{
  FILE *file;
  bool checkpoint;
  int numCheckpoints;

  file = fopen(filename, "r");
  if (!file)
    return ERROR("unable to open %s: %s", filename, strerror(errno));

  checkpoint = false;
  numCheckpoints = 0;
  
  while (!feof(file))
  {
    // Read a line into the token list
    if (this->readLine(file) != 0)
      return -1;

    // Parse begin_checkpoint
    if (this->numTokens == 1 && strcmp(this->tokens[0], "checkpoints") == 0)
    {
      checkpoint = true;
    }

    // Parse end_checkpoint
    else if (this->numTokens == 1 && strcmp(this->tokens[0], "end_checkpoints") == 0)
    {
      checkpoint = false;
    }

    // If in the checkpoint block...
    else if (checkpoint)
    {
      // Get the number of checkpoints we are expecting...
      if (this->numTokens == 2 && strcmp(this->tokens[0], "num_checkpoints") == 0)
      {
        numCheckpoints = atoi(this->tokens[1]);
      }
      // Get a checkpoint and add it to the list
      else if (this->numTokens == 1 && atoi(this->tokens[0]) > 0)
      {
        assert((size_t) this->numCheckpoints <
               sizeof(this->checkpoints)/sizeof(this->checkpoints[0]));
        this->checkpoints[this->numCheckpoints] = atoi(this->tokens[0]);
        this->numCheckpoints += 1;
      }
    }

    // Clear the token list
    this->clearLine();
  }
  
  fclose(file);

  // Do a sanity check
  if (numCheckpoints != this->numCheckpoints)
    return ERROR("read %d checkpoints; expecting %d",
                 this->numCheckpoints, numCheckpoints);
  
  return 0;
}


// Get the number of checkpoints
int MDF::getNumCheckpoints()  
{
  return this->numCheckpoints;
}


// Get a particular checkpoint
int MDF::getCheckpoint(int n)
{
  if (n < 0 || n >= this->numCheckpoints)
    return ERROR("invalid checkpoint index");
  return this->checkpoints[n];  
}


// Parse and parse a line from the file
int MDF::readLine(FILE *file)
{
  char line[1024];
  int len;
  int i, j;
  bool endToken, endLine;
  
  // Read the line
  if (fgets(line, sizeof(line), file) == NULL)
  {
    if (feof(file))
      return 0;
    else
      return ERROR("unable to read line: %s", strerror(errno));
  }

  //MSG("[%s]", line);
  
  // Parse the line into tokens
  len = (int) strlen(line);
  this->numTokens = 0;
  for (i = 0, j = 0; i < len; i++)
  {
    endToken = false;
    endLine = false;
    
    // If we get whitespace, we are done with this token
    if (isspace(line[i]))
    {
      endToken = true;
    }

    // If we are at the end of the line, we are done with this token
    else if (i == len - 1)
    {
      endToken = true;
      i += 1;
    }
    
    // If we get an start comment, we are done with this token and this line.
    else if (i + 1 < len && line[i] == '/' && line[i + 1] == '*')
    {
      endToken = true;
      endLine = true;
    }

    // Get the token
    if (endToken)
    {
      if (i - j > 0)
      {
        line[i] = 0;
        //MSG("token %d %d [%s]", j, i, line + j);
        assert((size_t) this->numTokens < sizeof(this->tokens)/sizeof(this->tokens[0]));
        this->tokens[this->numTokens] = strdup(line + j);
        this->numTokens += 1;
      }
      j = i + 1;
    }

    if (endLine)
      break;
  }
  
  return 0;
}


// Clear the token list
int MDF::clearLine()
{
  while (this->numTokens > 0)
  {
    this->numTokens -= 1;
    free(this->tokens[this->numTokens]);
    this->tokens[this->numTokens] = NULL;
  }
  
  return 0;
}
