
/* 
 * Desc: MDF parser test program
 * Date: 24 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <stdio.h>
#include "MDF.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


int main(int argc, const char **argv)
{
  const char *filename;
  int i;
  MDF *mdf;
  
  // Parse command line
  if (argc < 2)
    return ERROR("usage: %s <FILENAME>", argv[0]);
  filename = argv[1];

  mdf = new MDF();

  // Load file
  MSG("loading %s", filename);
  if (mdf->loadFile(filename) != 0)
    return ERROR("unable to load %s", filename);

  // Print the checkpoints
  printf("checkpoints:\n");
  for (i = 0; i < mdf->getNumCheckpoints(); i++)
  {
    printf("%d : %d\n", i, mdf->getCheckpoint(i));
  }
  printf("\n");

  delete mdf;

  return 0;  
}
