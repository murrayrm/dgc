
/* 
 * Desc: Loads RNDF data into a convenient graph structure.
 * Date: 3 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef RNDF_GRAPH_HH
#define RNDF_GRAPH_HH

/** @file

@brief RNDF data expressed in a convenient graph form.

*/

#include <stdint.h>

// Forward declarations from the RNDF class
namespace std
{
  class RNDF;
  class GPSPoint;
}


/// @brief Waypoint from the RNDF.
struct RNDFGraphWaypoint
{
  /// Position in site frame
  float px, py;

  /// RNDF information.  Note:
  /// - laneId is zero for zone perimeters.
  /// - checkpointId is zero at non-checkpoints.
  int segmentId, laneId, waypointId, checkpointId;

  /// Bitfield with flags
  struct
  {
    /// Is this waypoint on a stop line?
    uint8_t isStop : 1;

    /// Is this waypoint an exit point?
    uint8_t isExit : 1;

    /// Is this waypoint an entry point?
    uint8_t isEntry : 1;
    
  } flags;
  
  /// Pointer for user data storage
  void *data;

  /// Plan cost (used for recording the distance to a goal).
  float planCost;
  
  /// Next waypoint(s)
  int numNext;
  RNDFGraphWaypoint *next[16];

  /// Previous waypoint(s)
  int numPrev;
  RNDFGraphWaypoint *prev[16];  
};


/// @brief Convenient representation of the RNDF as a graph.
///
/// The graph is defined in the site frame (identical to UTM apart
/// from a translation).
///
class RNDFGraph
{
  public:

  /// @brief Constructor
  RNDFGraph();

  /// @brief Destructor
  virtual ~RNDFGraph();
  
  private:

  // Hide the copy constructor
  RNDFGraph(const RNDFGraph &that);
  
  public:

  /// @brief Load RNDF file.
  ///
  /// @param[in] filename RNDF file name.  
  /// @param[in] siteN,siteE Site frame origin in UTM coordinates
  /// (norhting, easting).  If not set, the first waypoint in the RNDF
  /// will be used as the origin of the site frame.
  int load(char *filename, double siteN = 0, double siteE = 0);

  /// @brief Write in binary format to a file handle.
  int writeFile(FILE *file);

  /// @brief Read in binary format from a file handle.
  int readFile(FILE *file);

  /// @brief Get the node corresponding to the given waypoint ID
  RNDFGraphWaypoint *getWaypoint(int segmentId, int laneId, int waypointId);

  /// @brief Get the node corresponding to the given checkpoint ID
  RNDFGraphWaypoint *getCheckpoint(int checkpointId);

  private:

  /// Generate the RNDF
  int genRndf(std::RNDF *rndf);

  /// Generate the waypoints for a segment/lane
  int genRndfLane(std::RNDF *rndf, int segmentId, int laneId);

  /// Generate the waypoints for a zone
  int genRndfZone(std::RNDF *rndf, int zoneId);

  /// Generate the RNDF intersections
  int genRndfIntersections(std::RNDF *rndf);

  /// Insert an arc between waypoints
  int insertArc(RNDFGraphWaypoint *wpA, RNDFGraphWaypoint *wpB);

  public:

  // Site frame origin in UTM frame.
  double siteN, siteE;
    
  // RNDF graph, stored as a list of waypoints.  
  int numWaypoints, maxWaypoints;
  RNDFGraphWaypoint *waypoints;

#if USE_GL
  public:

  /// Predraw the RNDF.
  /// @return On success, returns the display list.
  int predraw(void);

  // Draw a text box
  void drawText(float size, const char *text);

  private:
  
  // GL display list
  int rndfList;
#endif
};

#endif

