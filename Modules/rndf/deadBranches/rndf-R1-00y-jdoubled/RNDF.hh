/*!**
 * Morlan Liu and Nok Wongpiromsarn
 * December 8, 2006
 */

#ifndef RNDF_HH_
#define RNDF_HH_
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#include <set>
#include "dgcutils/ggis.h"
//using namespace std;
using std::set;


namespace std
{

enum Divider { DOUBLE_YELLOW, SOLID_WHITE, BROKEN_WHITE, ROAD_EDGE };

/*! GPSPoint class. The GPS point class represents a generic GPS point.
 *  Waypoints and perimeter points are subclasses of GPS points.
 *  All GPS points have a longitude and latitude. GPS points can be designated
 *  as entry entry points and/or exit points.
 *  All perimeter points has laneID = 0
 *  \brief The waypoint class represents a waypoint provided by the RNDF file.
 */
class GPSPoint
{
public:
/*! All GPS points have a longitude and latitude. GPS points can be designated
 *  as entry points and/or exit points. */
  GPSPoint();
  GPSPoint(int segmentID, int laneID, int waypointID, double northing, double easting, int zone, char letter);
  virtual ~GPSPoint();
  
/*! Returns the segment ID of THIS. */
  int getSegmentID();
  
/*! Returns the lane ID of THIS. */
  int getLaneID();

/*! Returns the waypoint ID of THIS. */
  int getWaypointID();

/*! Returns the latitude of THIS.
  double getLatitude();
  
  ! Returns the longitude of THIS.
  double getLongitude();
*/
  
/*! Returns the northing of THIS. */
  double getNorthing();
  
/*! Returns the easting of THIS. */
  double getEasting();

/*! Returns the UTM zone number of THIS. */
  int getUtmZone() {return this->zone;}

/*! Returns the UTM zone letter of THIS. */
  char getUtmLetter() {return this->letter;}
  
/*! Returns the vector of pointers to entry GPS points. */
  vector<GPSPoint*> getEntryPoints();

/*! For serialization */
  int getMyIndex();
  void setMyIndex(int);
  
/*! Sets the waypointID of THIS. */
  void setWaypointID(int);
  
/*! Sets segmentID, laneID and waypointID of THIS */
  void setIDs(int, int, int);
  
  void setNorthingEasting(double, double);
  
/*! Sets THIS as an entry GPS point. */
  void setEntry();
  
/*! Sets THIS as an exit GPS point and adds entryGPSPoint to the array
 *  entryPoints. */
  void setExit(GPSPoint* entryGPSPoint);
  
/*! Returns TRUE iff THIS is an entry GPS point. */
  bool isEntry();
  
/*! Returns TRUE iff THIS is an exit GPS point. */
  bool isExit();

/*! Prints the segmentID, laneID, and waypointID of THIS. */
  void print();
  void printAll();
  
protected:
/*  Segments are identified using the form 'M'. The Nth lane of segment M has
 *  identifier 'M.N'. The waypoints of each lane are similarly identified 
 *  such that the Pth waypoint of lane M.N is 'M.N.P'.*/
  int segmentID, laneID, waypointID;
  
/* A variable used for serialization */
  int myIndex;

/*  GPS latitude and logitude fields are floats with six decimal places 
 *  and represent points in the WGS84 coordinate system.
  double latitude, longitude;
*/

  /*! UTM coordinates (in meters) using the WGS84 spheroid. */
  double northing, easting;

  /*! UTM zone number (e.g., 17). */
  int zone;

  /*! UTM letter (e.g., 'N') */
  char letter;

/*  GPS points can be designated as an entry and/or  an exit point. */
  bool entry, exit;
  
/*  Exit GPS points have related entry GPS points. */
  vector<GPSPoint*> entryPoints;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! Waypoint class. The Waypoint class represents a waypoint provided by the
 * RNDF file. All waypoints have a segment ID, lane ID, waypoint ID, longitude,
 * and latitude. Waypoints are designated as a checkpoint, entry waypoint,
 * exit waypoint, or a stop sign.
 * \brief The waypoint class represents a waypoint provided by the RNDF file.
 */
class Waypoint : public GPSPoint
{
public:
/*! All waypoints have a segment ID, lane ID, waypoint ID, longitude, and
 * latitude. Initially, each waypoint is not designated as a checkpoint,
 * entry waypoint, exit waypoint, nor a stop sign. */
	Waypoint(int segmentID, int laneID, int waypointID,
           double northing, double easting, int zone, char letter);
//  Waypoint( const Waypoint& other ) { copy(other); }
//  Waypoint& operator=( const Waypoint& other ) { copy(other); return *this; }
  void copy( const Waypoint& other ) {
    segmentID = other.segmentID;
    laneID = other.laneID;
    waypointID = other.waypointID;
  }
	virtual ~Waypoint();

/*! If this waypoint is also a checkpoint, returns the checkpoint ID. */
  int getCheckpointID();
  
/*! Sets THIS as a checkpoint and sets the checkpoint ID. */
  void setCheckpointID(int checkpointID);
  
/*! Sets THIS as a stop sign. */
  void setStopSign();
  
/*! Returns TRUE iff THIS is a checkpoint. */
  bool isCheckpoint();
  
/*! Returns TRUE iff THIS is a stop sign. */
  bool isStopSign();

  void printAll();
  void printID( ostream& out ) const { out << segmentID << "." << laneID << "." << waypointID; }

  /*! returns true if the waypoint is less than the other based on segment, lane, waypoint id */
  bool less( const Waypoint& other ) const ;
  bool operator< (const Waypoint& o ) const;
  
protected:
  int checkpointID;
  
/*! Waypoints can be designated as a checkpoint, stop sign, entry waypoint, 
 *  or exit waypoint. */  
  bool checkpoint, stopSign;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! PerimeterPoint class. The PerimeterPoint class represents a perimeterpoint
 * provided by the RNDF file. All perimeterpoints have a zone ID perimeterpoint
 * ID, longitude, and latitude. Perimeterpoints are designated as an entry 
 * perimeterpoint, exit perimeterpoint, or neither.
 * \brief The perimeterpoint class represents a perimeterpoint provided by the RNDF.
 */
class PerimeterPoint: public GPSPoint
{
public:
/*! All perimeterpoints have a zone ID, perimeterpoint ID, longitude, and
 *  latitude. Initially, each perimeterpoint is not designated as a entry
 *  perimeterpoint nor as an exit perimeterpoint. All perimeterpoints have
 *  a "laneID" equal to 0. */
	PerimeterPoint(int zoneID, int zero, int perimeterPointID, 
                 double northing, double easting, int zone, char letter);
	virtual ~PerimeterPoint();

/*! Returns the zone ID of THIS. */
  int getZoneID();

/*! Returns the perimeterpoint ID of THIS. */
  int getPerimeterPointID();
  
private:
/*  Zones are identified using the form 'M'. The perimeterpoints of each zone
 *  are identified such that the Pth perimeterpoint of zone M is 'M.0.P'.*/
  // int zoneID, perimeterPointID;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! ParkingSpot class. The ParkingSpot class represents a parking spot provided
 *  by the RNDF file. All parking spots have a zone ID, spotID, and two waypoints.
 *  Parking spots may have an optional spot width.
 * \brief The ParkingSpot class represents a parking spot provided by the RNDF.
 */
class ParkingSpot
{
public:
/*! All parking spots have a zoneID or spotID. */
	ParkingSpot();
	ParkingSpot(int zoneID, int spotID);
	virtual ~ParkingSpot();
  
/*! Sets the spot width of THIS. */
  void setSpotWidth(int spotWidth);
  
/*! Sets waypointID of THIS to WAYPOINT. */
  void setWaypoint(int waypointID, Waypoint* waypoint);
  
/*! Returns the zoneID of THIS. */
  int getZoneID();
  
/*! Returns the spotID of THIS. */
  int getSpotID();
  
/*! Returns the spot width of THIS. */
  int getSpotWidth();
  
/*! Returns a pointer to a Waypoint with the waypointID passed in as an argument. */
  Waypoint* getWaypoint(int waypointID);

/*! For serialization */
  int getMyIndex();
  void setMyIndex(int);
  
/*! Print out THIS */
    void print();
  
private:
  int zoneID, spotID, spotWidth;
  int myIndex;
  Waypoint* waypoint1;
  Waypoint* waypoint2;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! Lane class. The Lane class represents a lane provided by the RNDF file.
 * All lanes have a segment ID, lane ID, and number of waypoints. Lanes may
 * have optional information including lane width and boundaries.
 * \brief The Lane class represents a lane provided by the RNDF file.
 */
class Lane
{
public:
/*! All lanes have a segment ID, lane ID, and number of waypoints. */
	Lane();
	Lane(int, int, int);
	virtual ~Lane();

/*! Returns the segment ID of THIS. */
  int getSegmentID();
  
/*! Returns the lane ID of THIS. */
  int getLaneID();
  
/*! Returns the number of waypoints contained in THIS. */
  int getNumOfWaypoints();
  
/*! Returns the lane width of THIS. */
  int getLaneWidth();
  
/*! Returns the left boundary of THIS. */
  Divider getLeftBoundary();
  
/*! Returns the right boundary of THIS. */
  Divider getRightBoundary();

/*! Returns the direction of THIS */
  int getDirection();

/*! For serialization */
  int getMyIndex();
  void setMyIndex(int);
  
/*! Sets the lane width of THIS. */
  void setLaneWidth(int);
  
/*! Sets the left boundary of THIS. */
  void setLeftBoundary(string);
  void setLeftBoundary(Divider);
  
/*! Sets the right boundary of THIS. */
  void setRightBoundary(string);
  void setRightBoundary(Divider);
  
/*! Sets the number of waypoints of THIS. */
  void setNumOfWaypoints(int);

/*! Sets the direction of THIS */
  void setDirection(int);
  
/*! Returns a pointer to a Waypoint with the waypoint ID passed in as an
 *  argument. */
  Waypoint* getWaypoint(int);
  
/*! Returns a vector of pointers to all the waypoints */
  vector<Waypoint*> getAllWaypoints();
  
/*! Adds a waypoint to the array of waypoints contained in THIS. */
  bool addWaypoint(Waypoint*);
  
/*! Print out THIS */
    void print();
  
private:
  int segmentID, laneID, numOfWaypoints, laneWidth;
  int myIndex;
  int direction;
  Divider leftBoundary, rightBoundary;
  
/*! Each lane contains an array of waypoints of size numOfWaypoints. */
  vector<Waypoint*> waypoints;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! Segment class. The Segment class represents a segment provided by the RNDF
 *  file. Each segment is characterized by its ID, the number of lanes in it,
 *  and optionally a segment name.
 * \brief The segment class represents a segment provided by the RNDF file.
 */
class Segment
{
public:
/*! Each segment is characterized by its ID and the number of lanes in it. */
	Segment();
	Segment(int, int);
  virtual ~Segment();
  
/*! Returns the segment ID of THIS. */
  int getSegmentID();
  
/*! Returns the number of lanes that THIS contains. */
  int getNumOfLanes();

/*! Returns the segment name of THIS. */
  string getSegmentName();
  
/*! Sets the segment name of THIS. */
  void setSegmentName(string);
  
/*! Returns the minimum speed of THIS. */
  double getMinSpeed();
  
/*! Returns the maximum speed of THIS. */
  double getMaxSpeed();
  
/*! Returns the minimum and maximum speed limit of THIS. */
  void setSpeedLimits(double, double);
  
/*! Returns a pointer to a Lane with the lane ID passed in as an argument. */
  Lane* getLane(int);
  
/*! Returns vector of pointers to all the lanes. */
  vector<Lane*> getAllLanes();
  
/*! Adds a lane to the array of lanes contained in THIS. */
  bool addLane(Lane*);
      
/*! Print out THIS */
    void print();
  
private:
/* Segments are identified using the form 'M'. All segments have a finite 
 * number of lanes in them. */
  int segmentID, numOfLanes;
  
/* Optionally, a segment may also have a segment name or street name. */
  string segmentName;
  
/* Each segment contains an array of lanes of size numOfLanes. */
  vector<Lane*> lanes;
  
/* Each segment has a minimum and maximum speed limit. */
  double minSpeed, maxSpeed;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

/*! Zone class. The Zone class represents a zone provided by the RNDF file.
 * All zones have a zone ID, a fixed number of parking spots, and a minimum
 * and maximum speed limit. Zones have a fixed perimeter characterized by
 * a fixed number of perimeter points. Zones may have an optional name.
 * \brief The Zone class represents a zone provided by the RNDF file.
 */
class Zone
{
public:
/*! All Zones have a zone ID and a fixed number of parking spots. */
	Zone();
	Zone(int zoneID, int numOfSpots);
	virtual ~Zone();

/*! Sets the zone name of THIS. */
  void setZoneName(string zoneName);
  
/*! Sets the minimum and maximum speed limit of THIS. */
  void setSpeedLimits(double minSpeed, double maxSpeed);
  
/*! Adds a perimeter point to the array of perimeter points contained in THIS. */
  bool addPerimeterPoint(PerimeterPoint* perimeterPoint);
  
/*! Adds a parking spot to the array of parking spots contained in THIS. */
  bool addParkingSpot(ParkingSpot* parkingSpot);

/*! Returns the zone ID of THIS. */
  int getZoneID();
  
/*! Returns the number of parking spots contained in THIS. */
  int getNumOfSpots();
  
/*! Returns the number of perimeter points contained in THIS. */
  int getNumOfPerimeterPoints();
  
/*! Returns the zone name of THIS. */
  string getZoneName();
  
/*! Returns the minimum speed of THIS. */
  double getMinSpeed();
  
/*! Returns the maximum speed of THIS. */
  double getMaxSpeed();
 
/*! Returns a pointer to a perimeter point with the perimeterPointID passed in
 *  as an argument. */
  PerimeterPoint* getPerimeterPoint(int perimeterPointID);
  
/*! Returns vector of pointers to all the perimeter points. */
  vector<PerimeterPoint*> getAllPerimeterPoints();
  
/*! Returns a pointer to a parking spot with the spotID passed in as an argument. */
  ParkingSpot* getParkingSpot(int spotID);
  
/*! Returns vector of pointers to all the parking spots. */
  vector<ParkingSpot*> getAllSpots();

/*! Print out THIS */
    void print();
  
private:
/* Zones are identified with an integer > 0. All zones have a finite 
 * number of parkings spots in them. Zones also have a minimum and
 * maximum speed allowed. */
  int zoneID, numOfSpots;
  double minSpeed, maxSpeed;
  
/* Optionally, a zone may also have a zone name. */
  string zoneName;
  
/* Each zone contains a zone perimeter. */
  vector<PerimeterPoint*> perimeter;
  
/* Each zone contains an array of parking spots of size numOfSpots. */
  vector<ParkingSpot*> spots;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
/*! Intersection class. The Intersection class represents an intersection 
 * provided by the RNDF file (needed to be added to the original RNDF from
 * DARPA). All intersections have an intersection ID, a number of road 
 * segments (arms) that come together at the intersection, the waypoints
 * that come to the intersection, and the waypoints that leave the intersection.
 * \brief The Intersection class represents an intersection in the RNDF.
 */
class Intersection
{
public:
  Intersection(int, int, vector<Waypoint*>, vector<Waypoint*>);

  /*! Returns the intersection ID of THIS */
  int getIntersectionID();

  /*! Returns the number of road segments (arms) that come
   * together at the intersection */
  int getNumOfArms();
  
  /*! Returns the waypoints that come to the intersection */
  vector<Waypoint*> getInWaypoints();

  /*! Returns the waypoints that leave the intersection */
  vector<Waypoint*> getOutWaypoints();

  /*! Print out THIS */
  void print();
  
private:
  int intersectionID;
  int numOfArms;
  vector<Waypoint*> inWaypoints;
  vector<Waypoint*> outWaypoints;
};



// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
/*! RNDF class. The RNDF class represents an RNDF, route network definition file 
 * provided by DARPA. RNDF file are loaded using the loadFile(char* fileName) 
 * method. Segments and Zones are contained in the RNDF class. 
 * \brief The RNDF class represents a RNDF provided by DARPA.
 */
class RNDF
{
  public:
/*! All RNDFs initially contain no information. */
    RNDF();
    RNDF(int, int);
  	virtual ~RNDF();

/*! Returns a pointer to a Segment with the segmentID passed in as an argument. */
    Segment* getSegment(int segmentID);
    
/*! Returns a vector of pointers to all the segments */
    vector<Segment*> getAllSegments();
    
/*! Returns a pointer to a Lane with the segmentID and laneID passed in as 
 * arguments. */    
    Lane* getLane(int segmentID, int laneID);

/*! Returns all the lanes that have the same direction as the given lane */
    vector<Lane*> getAdjacentLanes(Lane*);

/*! Returns all the lanes that have the opposite direction from the given lane */
    vector<Lane*> getNonAdjacentLanes(Lane*);
    
/*! Returns a pointer to a Waypoint with the segmentOrZoneID, laneID and
 *  waypointID passed in as arguments. */  
    Waypoint* getWaypoint(int segmentOrZoneID, int laneID, int waypointID);
    
/*! Returns a pointer to a Zone with the zoneID passed in as an argument. */
    Zone* getZone(int zoneID);
    
/*! Returns a vector of pointers to all the zones */
    vector<Zone*> getAllZones();
    
/*! Returns a pointer to a PerimeterPoint with the zoneID and perimeterPointID
 *  passed in as arguments. */
    PerimeterPoint* getPerimeterPoint(int zoneID, int perimeterPointID);
    
/*! Returns a pointer to a ParkingSpot with the zoneID and spotID passed in as 
 *  arguments. */
    ParkingSpot* getSpot(int zoneID, int spotID);
    
/*! Returns the number of segments contained in THIS. */
    int getNumOfSegments();
    
/*! Returns the number of zones contained in THIS. */
    int getNumOfZones();

/*! Get the waypoint with the specified checkpointID */
    Waypoint* getCheckpoint(int);
    
/*! Returns a vector of pointers to all the checkpoints */
    vector<Waypoint*> getAllCheckpoints();

/*! Returns the intersection with the specified intersectionID */
    Intersection* getIntersection(int);
/*! Returns a reference to a vector of all the intersections */
    vector<Intersection>& getAllIntersections();
  
/*! Sets the number of segments contained in THIS. */
    void setNumOfSegments(int);
    
/*! Sets the number of zones contained in THIS. */
    void setNumOfZones(int);
	
/*! Inserts a segment */
    bool addSegment(Segment*);

/*! Inserts a zone */
    bool addZone(Zone*);
	
/*! Inserts a checkpoint */
    bool addCheckpoint(Waypoint*);
        
/*! Inserts a waypoint right before the given waypoint (waypointID). */
    void addExtraWaypoint(int, int, int, double, double, int zone, char letter);

/*! Loads a RNDF or MDF. */
    bool loadFile(const char* fileName);

/*! Assigns lane direction */
    void assignLaneDirection();

/*! Find the closest waypoint or perimeter point on the specified segment and lane.
 * If segmentID = 0, then find the closest waypoint on any segment. If laneID = 0,
 * then find the closest waypoint on the specified segment on any lane. */
    GPSPoint* findClosestGPSPoint(double, double, int, int, double&);
    GPSPoint* findClosestGPSPoint(double, double, int, int, double&, 
				  bool, double, double, double, double, double&, double&);
    /*! parses the segment, lane and waypoint ids out of a stream and
     *  stores them in the reference paramenter.  returns true if
     *  the stream could be parsed */
    bool parseWaypointId(istream* file, Waypoint& w) ;
    
    /*! checks to see if any waypoint between these two (inclusive) are
     *  marked as sparse by the *.rndf.sparse file */
    bool isSparse( Waypoint& w1, Waypoint& w2 );
    void printSparse( ostream& out );

/*! Print out THIS */
    void print();
    
  private:
    int numOfSegments, numOfZones;
    vector<Segment*> segments;
    vector<Zone*> zones;
    vector<Waypoint*> ckpts;           // The vector of waypoints that are checkpoints
    vector<Intersection> intersections;
    set<Waypoint> sparsepoints;   // waypoints that are part of a sparse span

    /* For utm-latlon conversion */
  	GisCoordLatLon latlon;
  	GisCoordUTM utm;
    
    void parseSegment(ifstream*);
    void parseLane(ifstream*, int);
    void parseWaypoint(ifstream*, int, int);
    void parseZone(ifstream*);
    void parsePerimeter(ifstream*, int);
    void parseSpot(ifstream*, int);
    void parseExit(ifstream*);
    void parseIntersection(ifstream*);
    void parseSparse(istream*);
    void setSegmentsAndZones(ifstream*);
    
    void addSegment(int, int);
    void setSegmentName(int, string);
    void setSpeedLimits(int, int, int);
    
    void addLane(int, int, int);
    void setLaneWidth(int, int, int);
    void setLeftBoundary(int, int, string);
    void setRightBoundary(int, int, string);
    
    void addWaypoint(int, int, int, double, double);
    void setCheckpoint(int, int, int, int);
    void setStopSign(int, int, int);
    void setExit(int, int, int, int, int, int);
    
    void addZone(int, int);
    void setZoneName(int, string);
    void addPerimeterPoint(int, int, double, double);
    
    void addSpot(int, int);
    void setSpotWidth(int, int, int);
    void addSpotWaypoint(int, int, int, double, double);
};

} // namespace std

#endif /*RNDF_HH_*/


