              Release Notes for "rndf" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "rndf" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "rndf" module can be found in
the ChangeLog file.

Release R1-00y-nav (Sat Oct 20 12:11:25 2007):
	Moved some commonly-used drawing functions here; not a good 
place, but I dont want to create new modules at this stage.

Release R1-00y (Tue Oct 16 23:01:35 2007):
	* added a primitive "isSparse(..)" function.  Given two 
     waypoints each with seg,lane and waypoint id's filled 
     properly, isSparse will return true if there are any
     waypoints marked as sparse.  This is determined via
     a *.rndf.sparse file (".sparse concatentated to name
     of rndf loaded) which has a pair of  segid.laneid.waypointid's
     per line.  Pairs must be of the same segment and lane, 
     both in the .sparse file and in the isSparse function.
     Both syntaxes operate inclusive.

Release R1-00x (Mon Oct 15 23:29:51 2007):
	* Added zone and letter to RNDF site info
	* modified rndf interpolation to give straighter paths.

Release R1-00w (Mon Oct 15  7:29:31 2007):
	* Do not search the perimeter points to find the closest waypoint unless the zone has no 
parking spots.
	* Added additional flags for handling zones
	* Fixed orientation bug in zone exit
	* set checkpoint if on parking spaces
	* increased arc count for big zones.

Release R1-00v (Thu Oct 11  0:20:31 2007):
  Minor tweaks.

Release R1-00u (Tue Oct  9 16:21:00 2007):
  Added new classes RNDFGraph and RoadGraph.

Release R1-00t (Mon Sep 24 15:31:05 2007):
	Added more cerr messages so we can easily figure out the problems
	with rndf. Properly initialized all the variables (so coverity 
	doesn't complain anymore).

Release R1-00s (Sun Aug  5 10:25:48 2007):
	Fixed bug found in the mock field demo when we started at 5.1.1 and
	mplanner wanted to go to 8.2.3

Release R1-00r (Sat Aug  4  1:18:52 2007):
	Corrected the math for finding the closest waypoint.

Release R1-00q (Thu Jun  7  9:09:06 2007):
	Take into account the turning radius to determine which lane we
	want to go to at startup.

Release R1-00p (Thu May 31 14:27:27 2007):
  Added speed limits to MDF parser.

Release R1-00o (Thu May 24 23:00:45 2007):
  Created a stand-alone MDF parser.  It currently reads checkpoints only.

Release R1-00n (Sun May  6 12:36:51 2007):
  Added support for UTM zone numbers and letters.

Release R1-00m (Wed Apr 11 16:53:01 2007):
	Fixed the RNDF parser so it can handle spaces at the beginning of the line. Also added Intersection class which is needed by the mission planner later."

Release R1-00l (Tue Apr  3 18:36:55 2007):
	Modified the findClosestGPSPoint function so it uses the minimum
	turning radius to determine the waypoint that is closest to us and
	is reachable from our current position.

Release R1-00k (Sat Mar 17 12:53:22 2007):
	Added solid_yellow to lane boundary type

Release R1-00j (Fri Mar 16 14:17:47 2007):
	Take into account the discontinuity of atan2

Release R1-00i (Tue Mar 13 21:12:05 2007):
	Fixed findClosestGPSPoint function so it takes into account the direction of the lane

Release R1-00h (Sat Mar 10 13:48:04 2007):
	Resolved the memory issues. (Valgrind does not complain about
	possible memory leak in RNDF anymore.) The core dump problem should
	be fixed as well.

Release R1-00g (Sat Mar 10 11:32:41 2007):
	Fixed a problem in the deconstructor for RNDFs.  Memory was getting
	freed (delete'd) twice and this was causing core dumps in asim,
	where we create a temporary RNDF to set the initial condition.  For
	now I commented out the delete's, which I think were all
	duplicates.  I left a warning for someone to go and check to make
	sure we don't have a memory leak (and created a bug).

Release R1-00f (Fri Mar  9 15:35:44 2007):
	Fixed the heading angle computation.

Release R1-00e (Sat Mar  3 20:21:41 2007):
	Added a function for getting all the lanes in opposite direction.

Release R1-00d (Thu Mar  1 22:16:55 2007):
	Added functions needed by the travgraph module but were previously internal to the mission planner.

Release R1-00c (Sun Feb 25 21:05:32 2007):
	Moved all the RNDF and MDF files to the appropriate routes-* modules

Release R1-00b (Tue Feb 20 17:11:38 2007):
	Fixed gloNavMap so it compiles against revision R2-00a of interfaces and skynet

Release R1-00a (Wed Jan 24 23:05:43 2007):
	Added RNDF class, gloNavMapLib, gloNavMapTalker and RNDF files 
used for testing.

Release R1-00 (Tue Jan 23 19:26:59 2007):
	Created.


























