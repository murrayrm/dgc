RNDF_name	Rose Bowl	
num_segments	4		
num_zones	0	
format_version	1.0	
creation_date	11/27/2006

segment	1	
num_lanes	1
segment_name	Waypoint_Road	
lane	1.1	
num_waypoints	3	
lane_width	12
checkpoint	1.2.3	1	
stop	1.1.3	
exit	1.1.3	2.1.1
1.1.1	34.157456902837139978	-118.16813206128824731
1.1.2	34.157829463614959309	-118.16825882096806311
1.1.3	34.158276343936833541	-118.16840786130899232
end_lane		
end_segment		

segment	2	
num_lanes	1
segment_name	Murray_Road	
lane	2.1	
num_waypoints	2	
lane_width	12	
exit	2.1.2	3.1.1
2.1.1	34.158423630812286831	-118.16834129384233165
2.1.2	34.158464499923475444	-118.16815789234591705
end_lane		
end_segment	

segment	3	
num_lanes	1	
segment_name	North_Street	
lane	3.1	
num_waypoints	3
lane_width	12	
stop	3.1.3	
exit	3.1.3	4.1.1
3.1.1	34.15839959339459142	-118.16803629205406878
3.1.2	34.158002100812588253	-118.16790433232019097
3.1.3	34.157593430371939291	-118.16777849020058966
end_lane		
end_segment	

segment	4	
num_lanes	1
segment_name	St_Luke_Lane	
lane	4.1	
num_waypoints	2	
lane_width	12	
checkpoint	4.1.2	2
stop	4.1.2
exit	4.1.2	1.1.1
4.1.1	34.157481874510416731	-118.1678517810352389
4.1.2	34.157439230714786049	-118.16799486366565475
end_lane
end_segment

end_file