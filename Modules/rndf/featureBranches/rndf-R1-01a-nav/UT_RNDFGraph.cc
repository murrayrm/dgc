
/* 
 * Desc: Test RNDF graph loader
 * Date: 3 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "RNDFGraph.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// Test reading/writing to file
int testSerialization(RNDFGraph *src)
{
  char *filename;
  FILE *file;
  RNDFGraph dstA;

  filename = "./rndf_graph_test_a.rg";
    
  // Write the graph to a file
  MSG("saving graph to %s", filename);
  file = fopen(filename, "w");
  assert(file);  
  if (src->writeFile(file) != 0)
    return ERROR("unable to save graph");
  fclose(file);

  // Load a new graph from the file
  MSG("loading graph from %s", filename);
  file = fopen(filename, "r");
  assert(file);  
  if (dstA.readFile(file) != 0)
    return ERROR("unable to load destination graph");
  fclose(file);
  
  filename = "./rndf_graph_test_b.rg";
    
  // Save the graph another file
  MSG("saving graph to %s", filename);
  file = fopen(filename, "w");
  if (dstA.writeFile(file) != 0)
    return ERROR("unable to save destination graph");
  fclose(file);

  // These two files should be identical.  A necessary but not
  // sufficient condition for validity.
  system("diff --binary ./rndf_graph_test_a.rg ./rndf_graph_test_b.rg");
  
  return 0;
}


int main(int argc, char **argv)
{
  RNDFGraph graph;

  if (argc < 2)
    return -1;

  // Load the graph from an RNDF
  MSG("loading %s", argv[1]);
  if (graph.load(argv[1]) != 0)
    return -1;

  // Test graph serialization
  testSerialization(&graph);
  
  return 0;
}
