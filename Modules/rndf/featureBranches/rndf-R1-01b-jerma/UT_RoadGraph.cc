
/* 
 * Desc: Test road graph loader
 * Date: 3 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <stdio.h>
#include <stdlib.h>
#include <dgcutils/DGCutils.hh>
#include "RoadGraph.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


int main(int argc, char **argv)
{
  char *filename;
  RoadGraph graph;
  int i;
  uint64_t time;

  filename = NULL;
  
  if (argc > 1)
    filename = argv[1];

  if (!filename)
    return fprintf(stderr, "usage: %s <FILE.RNDF>\n", argv[0]);

  // Load the file
  MSG("loading %s", filename);
  if (graph.load(filename, NULL) != 0)
    return -1;

  // Run some timing tests.  We choose a waypoint we know does not
  // exist to explore worst-case behavior.
  time = DGCgettime();
  for (i = 0; i < 10000; i++)
  {
    graph.getCheckpoint(10000);
  }
  time = DGCgettime() - time;
  MSG("get waypoint: %.3f ms/call", (double) time * 1e-3 / i);

  // Run some timing tests.  Pick some random points for
  // nearest-neighbor testing.
  time = DGCgettime();
  for (i = 0; i < 10000; i++)
  {
    float px, py;
    RoadGraphNode *node;
    
    px = (float) rand() / RAND_MAX * 8000 - 4000;
    py = (float) rand() / RAND_MAX * 8000 - 4000;
    node = graph.getNearestPos(px, py);

    //if (node)
    //  MSG("node nearest %f,%f is %d.%d.%d",
    //      px, py, node->segmentId, node->laneId, node->waypointId);
  }
  time = DGCgettime() - time;
  MSG("get nearest pos: %.3f ms/call", (double) time * 1e-3 / i);

  return 0;
}
