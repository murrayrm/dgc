
/* 
 * Desc: Draw aerial images using OpenGL
 * Date: 22 October 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef DRAW_AERIAL_HH
#define DRAW_AERIAL_HH



/// @brief Draw aerial images
class DrawAerial
{
  public:

  // Constructor
  DrawAerial();

  // Destructor
  ~DrawAerial();

  public:

  /// @brief Load map files from a directory
  int load(const char *path, double utmNorthing, double utmEasting);

  /// @brief Draw the map at a particular location in the UTM frame
  void draw(float siteNorthing, float siteEasting);

  private:
  
  // Data for each map tile
  struct Tile
  {
    // Northing/easting of the north-east corner (site frame)
    float siteNorthing, siteEasting;

    // Rotation of the tile in the site
    float siteRotation;

    // Map scale (m/pixel)
    float scale;

    // Map dimensions (pixels)
    int cols, rows;

    // Filename of tile data
    char *filename;    
  };

  private:
  
  // Find the tile closest to the target 
  Tile *lookupTile(float siteNorthing, float siteEasting);

  // Load the data for a tile
  int loadTile(Tile *tile);

  private:

  // Array of available tiles
  int numTiles, maxTiles;
  Tile *tiles;

  // Currently loaded tile
  Tile *tile;
  
  // Position when we last checked the tile
  float siteNorthing, siteEasting;

  // Current tile texture
  GLuint tileTex;  
};


#endif
