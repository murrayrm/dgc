RNDF_name	Sample_RNDF_Rev_1.2	
num_segments	1	
num_zones	0
format_version	1.0	
creation_date	5/18/2006	
segment	1	
num_lanes	1	
segment_name	Michigan_Ave	
lane	1.1
num_waypoints	6
lane_width	15	
left_boundary	double_yellow	
right_boundary	road_edge
checkpoint	1.1.6	1
1.1.1	34.139080179	-118.123414655
1.1.2	34.139081171	-118.123306213
1.1.3	34.139082164	-118.123197770
1.1.4	34.139083156	-118.123089328
1.1.5	34.139084148	-118.122980885
1.1.6	34.139085139	-118.122872443
end_lane		
end_segment		
end_file		
