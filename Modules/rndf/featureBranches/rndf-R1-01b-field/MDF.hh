
/* 
 * Desc: MDF parser
 * Date: 24 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef MDF_HH
#define MDF_HH

#include <stdio.h>


// Maximum number of checkpoints.
#define MDF_MAX_CHECKPOINTS 256

// Maximum number of speed limits.  This should be at greater than
// maximum number of segments in the RNDF.
#define MDF_MAX_SPEEDS 1024

// Maximum tokens on a line.
#define MDF_MAX_TOKENS 16


/// @brief MDF parser
class MDF
{
  public:

  /// @brief Default constructor
  MDF();

  /// @brief Destructor
  virtual ~MDF();

  public:

  /// @brief Load MDF from a file
  ///
  /// @param[in] filename Path/filename of MDF file
  /// @returns Returns 0 on success, non-zero on error.
  int loadFile(const char *filename);

  public:

  /// @brief Get the number of checkpoints
  int getNumCheckpoints();

  /// @brief Get a particular checkpoint
  /// @param[in] n Index of the checkpoint in the MDF.
  /// @returns Returns the checkpoint id, or -1 on error.
  int getCheckpoint(int n);

  /// @brief Get the speed limit in a segment.
  /// @param[in] segmentID Segment ID.
  //  @param[out] minSpeed Minimum speed in this segment (m/s).
  //  @param[out] maxSpeed Maximum speed in this segment (m/s).
  /// A value of zero means that the speed limit is undefined.
  /// @returns Non-zero on error.
  int getSpeedLimits(int segmentID, double *minSpeed, double *maxSpeed);

  private:

  // Read a line from the file; the results are placed in the token list
  int readLine(FILE *file);

  // Clear the token list
  int clearLine();

  private:

  // Checkpoint list
  int numCheckpoints;
  int checkpoints[MDF_MAX_CHECKPOINTS];

  // Speed limit dictionary (maps segmentID to min/max speed).
  // The number of speeds is included for sanity checking only.
  int numSpeeds;
  double speeds[MDF_MAX_SPEEDS][2];

  // Workspace with the current token list
  int numTokens;
  char *tokens[MDF_MAX_TOKENS];
};


#endif
