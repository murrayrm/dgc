
/* 
 * Desc: Draw misc stuff using OpenGL
 * Date: 19 October 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <GL/gl.h>
#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>

#include "DrawMisc.hh"


// Constructor
DrawMisc::DrawMisc()
{
  this->gridList = 0;
  
  return;
}


// Draw a set of axes
void DrawMisc::drawAxes(float size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Draw a text box
void DrawMisc::drawText(float px, float py, float pz, float size, const char *text)
{
  int i, count;
  float sx, sy;

  glPushMatrix();
  glTranslatef(px, py, pz);
  glRotatef(180, 0, 1, 0);
  glRotatef(90, 0, 0, 1);

  // Compute the scale factors in x/y directions, based on the 'M'
  // character.
  sx = glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, 'M');
  sy = sx * 1.3;
    
  glPushMatrix();
  glScalef(size / sx, size / sy, 1);
  glTranslatef(0, -sy, 0);

  glPushMatrix();
    
  for (i = 0, count = 0; i < (int) strlen(text); i++, count++)
  {
    if (text[i] == '\n')
    {
      glPopMatrix();
      glTranslatef(0, -sy, 0);
      glPushMatrix();
      count = 0;      
    }
    else
    {
      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[i]);
    }
  }

  glPopMatrix();
  glPopMatrix();
  glPopMatrix();

  return;
}


// Draw Alice (vehicle frame)
void DrawMisc::drawAlice(float steerAngle, int signalState)
{
  int onoff;

  glLineWidth(1);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(steerAngle * 180/M_PI, 0, 0, 1);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(steerAngle * 180/M_PI, 0, 0, 1);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Draw the left turn signal
  onoff = (signalState == -1) && ((DGCgettime() / 300000) % 2);  
  glPushMatrix();
  glTranslatef(-DIST_REAR_TO_REAR_AXLE, -VEHICLE_WIDTH/2, -VEHICLE_HEIGHT/2);
  glColor3f(0.5 * (onoff + 1), 0.25 * (onoff + 1), 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glutSolidCube(0.30);
  glPopMatrix();
  
  // Draw the right turn signal
  onoff = (signalState == +1) && ((DGCgettime() / 300000) % 2);  
  glPushMatrix();
  glTranslatef(-DIST_REAR_TO_REAR_AXLE, +VEHICLE_WIDTH/2, -VEHICLE_HEIGHT/2);
  glColor3f(0.5 * (onoff + 1), 0.25 * (onoff + 1), 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glutSolidCube(0.30);
  glPopMatrix();
    
  return;
}



// Predraw a grid
int DrawMisc::predrawGrid(float px, float py, float spacing)
{
  int i, j, n;
  float qx, qy;
  char text[256];

  n = (int) (100 / spacing);

  // Pick an offset that will line us up with the grid spacing
  qx = -fmodf(px + 1e6, spacing);
  qy = -fmodf(py + 1e6, spacing);

  // Dont re-draw the grid unless we move
  if (this->gridList != 0 && qx == this->gridPx && qy == this->gridPy)
    return this->gridList;
  this->gridPx = qx;
  this->gridPy = qy;
      
  // Create display list
  if (this->gridList == 0)
    this->gridList = glGenLists(1);
  glNewList(this->gridList, GL_COMPILE);

  glLineWidth(1.0);
  glColor3f(0.5, 0.5, 0.5);

  glPushMatrix();
  glTranslatef(qx, qy, 0);
  
  // Draw horizontal lines
  for (j = -n; j <= +n; j++)
  {
    glBegin(GL_LINE_STRIP);
    for (i = -n; i <= +n; i++)
      glVertex2f(i * spacing, j * spacing);
    glEnd();
  }  
  
  // Draw vertical lines
  for (j = -n; j <= +n; j++)
  {
    glBegin(GL_LINE_STRIP);
    for (i = -n; i <= +n; i++)
      glVertex2f(j * spacing, i * spacing);    
    glEnd();
  }  

  // Draw vertex coordinates
  for (j = -n; j < +n; j++)
  {
    for (i = -n; i < +n; i++)
    {
      snprintf(text, sizeof(text), "%+.0f %+.0f", px + qx + i * spacing, py + qy + j * spacing);
      this->drawText(i * spacing, j * spacing, 0, 0.5, text);
    }
  }
  
  glPopMatrix();

  glEndList();
  
	return this->gridList;
}
