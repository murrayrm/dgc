/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef GLONAVMAPTALKER_HH
#define GLONAVMAPTALKER_HH

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "interfaces/SkynetContainer.hh"
#include "dgcutils/DGCutils.hh"
#include "RNDF.hh"

#define GLONAVMAP_MAX_BUFFER_SIZE 500000

struct SRNDFHeader
{
  SRNDFHeader()
  {
    memset(this, 0, sizeof(*this));
  }
  int numOfSegments;
  int numOfZones;
  int numOfCheckpoints;
  int numOfLanes;
  int numOfPerimeterPoints;
  int numOfSpots;
  int numOfWaypoints;
};

class CGloNavMapTalker : virtual public CSkynetContainer
{
  pthread_mutex_t m_gloNavMapDataBufferMutex;
  char* m_pGloNavMapDataBuffer;

public:
  CGloNavMapTalker();
  ~CGloNavMapTalker();

  bool SendGloNavMap(int GloNavMapSocket, RNDF* receivedRndf);
  bool RecvGloNavMap(int GloNavMapSocket, RNDF* rndf, int* pSize);
  bool NewGloNavMap(int GloNavMapSocket);
  
private:
  int m_bufferSize;
};

#endif // GLONAVMAPTALKER_HH
