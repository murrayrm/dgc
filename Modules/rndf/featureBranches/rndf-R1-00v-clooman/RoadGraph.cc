
/* 
 * Desc: Generates a dense graph of interpolated vehicle configurations from an RNDF file.
 * Date: 3 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

#if USE_GL
#include <GL/glut.h>
#endif

#include <rndf/RNDF.hh>

#include "RoadGraph.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
RoadGraph::RoadGraph()
{
  this->spacing = 0.50;
  
  this->root = NULL;
  this->numQuads = 0;
  this->numQuadBytes = 0;
  this->numNodeBytes = 0;

  this->numNodes = 0;
  this->maxNodes = 0;
  this->nodes = NULL;
  
  this->roadList = 0;
  
  return;
}


// Destructor
RoadGraph::~RoadGraph()
{
  // Clean out the quad-tree; this also deletes the nodes
  if (this->root)
    this->freeQuad(this->root);
  this->root = NULL;

  // Clean out the node heap
  if (this->nodes)
    free(this->nodes);
  this->numNodes = 0;
  this->maxNodes = 0;
  this->nodes = NULL;
   
  return;
}


// Load RNDF file
int RoadGraph::load(char *filename)
{
  // Load up the RNDF graph
  if (this->rndf.load(filename) != 0)
    return -1;

  // Create the root node
  this->root = (RoadGraphQuad*) calloc(1, sizeof(RoadGraphQuad));
  this->root->px = 0;
  this->root->py = 0;
  this->root->size = 8192; // MAGIC
  
  // Generate the road graph
  if (this->genRoadGraph() != 0)
    return -1;

  //MSG("created: quads %d nodes %d Mb %.3f",
  //    this->numQuads, this->numNodes, this->numBytes * 1e-6);
  
  return 0;
}


// Get the canonical node corresponding to the given checkpoint ID
RoadGraphNode *RoadGraph::getCheckpoint(int checkpointId)
{
  RNDFGraphWaypoint* wp;
  wp = this->rndf.getCheckpoint(checkpointId);
  if (!wp)
    return NULL;  
  return (RoadGraphNode*) wp->data;
}


// Get the canonical node corresponding to the given RNDF ID
RoadGraphNode *RoadGraph::getWaypoint(int segmentId, int laneId, int waypointId)
{
  RNDFGraphWaypoint* wp;
  wp = this->rndf.getWaypoint(segmentId, laneId, waypointId);
  if (!wp)
    return NULL;  
  return (RoadGraphNode*) wp->data;
}


// Get the node nearest the given position
RoadGraphNode *RoadGraph::getNearestPos(float px, float py)
{
  int i;
  RoadGraphQuad *quad;
  RoadGraphNode *node, *minNode;
  float dist, minDist;

  quad = this->getNearestQuad(this->root, px, py);
  if (!quad)
    return NULL;

  // Check the nodes in the quad
  minDist = FLT_MAX;
  minNode = NULL;
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];
    dist = vec2f_mag(vec2f_sub(node->pose.pos, vec2f_set(px, py)));
    if (dist < minDist)
    {
      minDist = dist;
      minNode = node;
    }
  }
  
  return minNode;
}




// Generate the interpolated graph
int RoadGraph::genRoadGraph()
{
  int maxNodes;
  
  MSG("min quad size %d, min node size %d", sizeof(RoadGraphQuad), sizeof(RoadGraphNode));

  // Count the number of nodes we expect so we can pre-allocate storage
  maxNodes = this->predictNodeCount();
  MSG("pass 0: nodes %d", maxNodes);
    
  // Pre-allocate storage
  this->maxNodes = maxNodes;
  this->nodes = (RoadGraphNode*) calloc(maxNodes, sizeof(this->nodes[0]));
  
  // Generate the nodes for each RNDF waypoint
  this->genWaypoints();
  MSG("pass 1: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->numQuads, this->numQuadBytes * 1e-6,
      this->numNodes, this->numNodeBytes * 1e-6);

  // TODO refine orientations

  // Generate intermediate nodes
  this->genNonWaypoints();
  MSG("pass 2: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->numQuads, this->numQuadBytes * 1e-6,
      this->numNodes, this->numNodeBytes * 1e-6);

  return 0;
}


// Count the number of nodes we expect so we can pre-allocate
// storage.
int RoadGraph::predictNodeCount()
{
  int i, j;
  RNDFGraphWaypoint *wpA, *wpB;
  RoadGraphNode nodeA, nodeB;
  int count;

  count = 0;

  // Consider all waypoints in the RNDF graph, look at their outgoing
  // arcs, and figure out the number of points we would interpolate on
  // each arc.
  for (i = 0; i < this->rndf.numWaypoints; i++)
  {
    wpA = this->rndf.waypoints + i;
    nodeA.pose.pos.x = wpA->px;
    nodeA.pose.pos.y = wpA->py;
    count += 1;    
    for (j = 0; j < wpA->numNext; j++)
    {
      wpB = wpA->next[j];
      nodeB.pose.pos.x = wpB->px;
      nodeB.pose.pos.y = wpB->py;
      count += this->countInterpolated(&nodeA, &nodeB) - 1;
    }
  }

  return count;
}


// Generate road nodes for each RNDF waypoint,
// using an initial guess at the orientation
int RoadGraph::genWaypoints()
{
  int i;
  RNDFGraphWaypoint *wp;
  RoadGraphNode *node;
  
  for (i = 0; i < this->rndf.numWaypoints; i++)
  {
    wp = this->rndf.waypoints + i;

    node = this->allocNode(wp->px, wp->py);
    node->pose.rot = this->guessRotation(wp);

    // Copy RNDF cues
    node->segmentId = wp->segmentId;
    node->laneId = wp->laneId;
    node->waypointId = wp->waypointId;
    node->flags.isStop = wp->flags.isStop;
    node->flags.isExit = wp->flags.isExit;
    node->flags.isEntry = wp->flags.isEntry;
    node->prevWaypoint = wp;
    node->nextWaypoint = wp;

    // Store a pointer to this node in the RNDF graph so we can use
    // the RNDF for fast look-ups.  
    wp->data = (void*) node;
  }
  
  return 0;
}


// Generate intermediate nodes
int RoadGraph::genNonWaypoints()
{
  int i, j;
  RNDFGraphWaypoint *wpA, *wpB;
  RoadGraphNode *nodeA, *nodeB;

  // Generate interpolated nodes, using the RNDF to find the waypoint
  // nodes we just generated.
  for (i = 0; i < this->rndf.numWaypoints; i++)
  {
    wpA = this->rndf.waypoints + i;
    nodeA = (RoadGraphNode*) wpA->data;
    assert(nodeA);
    for (j = 0; j < wpA->numNext; j++)
    {
      wpB = wpA->next[j];
      nodeB = (RoadGraphNode*) wpB->data;
      assert(nodeB);
      if (this->genInterpolated(nodeA, nodeB) != 0)
        return -1;
    }
  }
  
  return 0;
}


// Guess the default rotation for the given RNDF node
float RoadGraph::guessRotation(const RNDFGraphWaypoint *wp)
{
  int i;  
  RNDFGraphWaypoint *wa, *wb;

  // If we are on a zone perimeter, look for an entry or exit point
  // and average the angles.  The angles on non-entry/exit perimeter
  // points are undefined.
  if (wp->laneId == 0)
  {
    float dx, dy;
    if (wp->numPrev > 1)
    {
      dx = dy = 0;
      for (i = 0; i < wp->numPrev; i++)
      {
        wa = wp->prev[i];
        if (wa->laneId == 0)
          continue;
        dx += wp->px - wa->px;
        dy += wp->py - wa->py;
      }
      return atan2(dy, dx);
    }
    if (wp->numNext > 1)
    {
      dx = dy = 0;
      for (i = 0; i < wp->numNext; i++)
      {
        wb = wp->next[i];
        if (wb->laneId == 0)
          continue;
        dx += wb->px - wp->px;
        dy += wb->py - wp->py;
      }
      return atan2(dy, dx);
    }    
  }
  
  // If there is exactly one node on either side of us, take the
  // average tangent.
  if (wp->numPrev == 1 && wp->numNext == 1)
  {
    wa = wp->prev[0];
    wb = wp->next[0];          
    return atan2((wb->py - wa->py), (wb->px - wa->px));
  }

  // If there is one node behind, and none ahead, take the direction
  // to the one behind.
  if (wp->numPrev == 1 && wp->numNext != 1)
  {
    wa = wp->prev[0];
    return atan2((wp->py - wa->py), (wp->px - wa->px));
  }

  // If there is one node ahead, and none behind, take the direction
  // to the one behind.
  if (wp->numPrev != 1 && wp->numNext == 1)
  {
    wb = wp->next[0];
    return atan2((wb->py - wp->py), (wb->px - wp->px));
  }

  // Fall-back case: ambiguous next/prev, so see if we are in a lane.
  if (wp->numPrev > 1 && wp->numNext > 1)
  {
    wa = wb = NULL;
    for (i = 0; i < wp->numPrev; i++)
    {
      if (wp->prev[i]->laneId == wp->laneId)
      {
        wa = wp->prev[i];
        break;
      }
    }
    for (i = 0; i < wp->numNext; i++)
    {
      if (wp->next[i]->laneId == wp->laneId)
      {
        wb = wp->next[i];
        break;
      }
    }
    if (wa && wb)
      return atan2((wb->py - wa->py), (wb->px - wa->px));
    if (wa)
      return atan2((wp->py - wa->py), (wp->px - wa->px));
    if (wb)
      return atan2((wb->py - wp->py), (wb->px - wp->px));
  }
  
  // Should not get here, so lets see why
  MSG("missing case for waypoint %d.%d.%d %d %d",
      wp->segmentId, wp->laneId, wp->waypointId, wp->numPrev, wp->numNext);
  for (i = 0; i < wp->numPrev; i++)
    MSG("prev %d  %d.%d.%d", i,
        wp->prev[i]->segmentId, wp->prev[i]->laneId, wp->prev[i]->waypointId);
  for (i = 0; i < wp->numNext; i++)
    MSG("next %d  %d.%d.%d", i,
        wp->next[i]->segmentId, wp->next[i]->laneId, wp->next[i]->waypointId);

  return 0;
}


// Generate the interpolated value for a cubic spline.
void cubicSpline(float *x, float *dx, float t, float p1, float m1, float p2, float m2)
{
  float ha, hb, hc, hd;
  float ga, gb, gc, gd;

  // Cubic interpolation between end-points (p1, p2) with tangents (m1, m2).
  ha =  2*t*t*t - 3*t*t + 1;
  hb =    t*t*t - 2*t*t + t;
  hc = -2*t*t*t + 3*t*t;
  hd =    t*t*t -   t*t;
  *x =  ha*p1 + hb*m1 + hc*p2 + hd*m2;

  // Cubic interpolation of tangents
  ga = 6*t*t - 6*t;
  gb = 3*t*t - 4*t + 1;
  gc = -6*t*t + 6*t;
  gd = 3*t*t - 2*t;
  *dx = ga*p1 + gb*m1 + gc*p2 + gd*m2;
  
  return;
}


// Count the number of interpolated nodes we would generate between
// these two nodes
int RoadGraph::countInterpolated(RoadGraphNode *nodeA, RoadGraphNode *nodeB)
{
  float dist;
  int steps;
  
  dist = vec2f_mag(vec2f_sub(nodeB->pose.pos, nodeA->pose.pos));
  steps = (int) (dist / this->spacing);

  return steps;
}


// Generate interpolated nodes
int RoadGraph::genInterpolated(RoadGraphNode *nodeA, RoadGraphNode *nodeB)
{
  int i, steps;
  float dist, ma, mb;
  float t, px, py, dx, dy;
  RoadGraphNode *next, *prev;

  // Number of interpolated nodes to create
  steps = this->countInterpolated(nodeA, nodeB);  

  // Distance between end nodes
  dist = vec2f_mag(vec2f_sub(nodeB->pose.pos, nodeA->pose.pos));

  //MSG("%f %f to %f %f dist %f steps %d",
  //    nodeA->pose.pos.x, nodeA->pose.pos.y,
  //    nodeB->pose.pos.x, nodeB->pose.pos.y, dist, steps);      

  // Generate intermediate nodes and hook them up with arcs
  prev = nodeA;
  for (i = 1; i < steps; i++)
  {
    t = (float) i / steps;

    if (nodeA->laneId == 0 && nodeB->laneId == 0)
    {
      // Do a linear interpolation, with the node orientation pointing
      // to the normal of the zone perimeter.
      dx = (nodeB->pose.pos.x - nodeA->pose.pos.x);
      dy = (nodeB->pose.pos.y - nodeA->pose.pos.y);
      px = nodeA->pose.pos.x + t * dx;
      py = nodeA->pose.pos.y + t * dy;
    }
    else
    {
      // Do a cubic spline interpolation on the position and orientation
      // of the end-nodes
      ma = cos(nodeA->pose.rot) * dist;
      mb = cos(nodeB->pose.rot) * dist;
      cubicSpline(&px, &dx, t, nodeA->pose.pos.x, ma, nodeB->pose.pos.x, mb);
      ma = sin(nodeA->pose.rot) * dist;
      mb = sin(nodeB->pose.rot) * dist;
      cubicSpline(&py, &dy, t, nodeA->pose.pos.y, ma, nodeB->pose.pos.y, mb);
    }

    //MSG("%d %f : %f %f : %f %f", i, t, px, py, dx, dy);

    // Create the node
    next = this->allocNode(px, py);
    if (!next)
      return ERROR("out of node memory; increase the max nodes beyond %d", this->maxNodes);
    next->pose.rot = atan2(dy, dx);

    // Copy RNDF cues so that every node has some meaningful
    // identifier.  We use the information from the node we are coming
    // from.
    next->segmentId = nodeA->segmentId;
    next->laneId = nodeA->laneId;
    next->waypointId = nodeA->waypointId;
    next->interId = i;

    // Is this interpolated node in an intersection?
    if (nodeA->flags.isExit)
      next->flags.isTurn = true;

    // Keep track our our relationship with the RNDF
    next->prevWaypoint = nodeA->prevWaypoint;
    next->nextWaypoint = nodeB->nextWaypoint;

    // Insert an arc between these nodes
    this->insertArc(prev, next);
    prev = next;
  }
  this->insertArc(prev, nodeB);

  return 0;
}


// Allocate a new node
RoadGraphNode *RoadGraph::allocNode(float px, float py)
{
  RoadGraphNode *node;
  RoadGraphQuad *quad;

  // Make sure there is room in the internal node list to store the new node.
  if (this->numNodes >= this->maxNodes)
    return NULL;
  
  // Initialize node
  node = this->nodes + this->numNodes++;
  memset(node, 0, sizeof(*node));
  node->pose.pos.x = px;
  node->pose.pos.y = py;
  node->pose.rot = 0;

  // Keep stats of memory actually used
  this->numNodeBytes += sizeof(this->nodes[0]);

  // Get a quad to put the node in
  quad = this->allocQuad(this->root, px, py);
  assert(quad);
  
  // Make sure there is space in the terminal leaf to store the new
  // node pointer.
  if (quad->numNodes >= quad->maxNodes)
  {
    this->numQuadBytes -= quad->maxNodes * sizeof(quad->nodes[0]);
    quad->maxNodes = quad->numNodes + 1;
    quad->nodes = (RoadGraphNode**) realloc(quad->nodes, quad->maxNodes*sizeof(quad->nodes[0]));
    this->numQuadBytes += quad->maxNodes * sizeof(quad->nodes[0]);
  }

  // Add to the terminal leaf
  quad->nodes[quad->numNodes++] = node;

  return node;
}


// Insert a new arc
int RoadGraph::insertArc(RoadGraphNode *nodeA, RoadGraphNode *nodeB)
{
  // Make room in node A
  if (nodeA->maxNext >= nodeA->maxNext)
  {
    this->numNodeBytes -= sizeof(nodeA->maxNext*sizeof(nodeA->next[0]));
    nodeA->maxNext += 1;
    nodeA->next = (RoadGraphNode**) realloc(nodeA->next, nodeA->maxNext*sizeof(nodeA->next[0]));
    this->numNodeBytes += sizeof(nodeA->maxNext*sizeof(nodeA->next[0]));
  }

  // Make room in node B
  if (nodeB->maxPrev >= nodeB->maxPrev)
  {
    this->numNodeBytes -= sizeof(nodeB->maxPrev*sizeof(nodeB->prev[0]));
    nodeB->maxPrev += 1;
    nodeB->prev = (RoadGraphNode**) realloc(nodeB->prev, nodeB->maxPrev*sizeof(nodeB->prev[0]));
    this->numNodeBytes += sizeof(nodeB->maxPrev*sizeof(nodeB->prev[0]));
  }

  // Insert arcs
  nodeA->next[nodeA->numNext++] = nodeB;
  nodeB->prev[nodeB->numPrev++] = nodeA;

  return 0;
}


// Allocate a new quad
RoadGraphQuad *RoadGraph::allocQuad(RoadGraphQuad *quad, float px, float py)
{
  int li;
  float qx, qy, size;
  RoadGraphQuad *leaf;

  // See if the point falls within this quad; if not, give up.
  if (px > quad->px + quad->size/2)
    return NULL;
  if (px < quad->px - quad->size/2)
    return NULL;
  if (py > quad->py + quad->size/2)
    return NULL;
  if (py < quad->py - quad->size/2)
    return NULL;

  // If we have not arrived at a terminal leaf...
  if (quad->size > 4.0) // MAGIC
  {
    // See which leaf this point belongs to
    li = 2 * (py < quad->py) + (px < quad->px);    
    leaf = quad->leaves[li];

    // If we need to recurse, but there is no leaf, create one now
    if (!leaf)
    {
      size = quad->size / 2;
      if (px < quad->px)
        qx = quad->px - size/2;
      else
        qx = quad->px + size/2;
      if (py < quad->py)
        qy = quad->py - size/2;
      else
        qy = quad->py + size/2;

      leaf = (RoadGraphQuad*) calloc(1, sizeof(RoadGraphQuad));
      leaf->px = qx;
      leaf->py = qy;
      leaf->size = size;
      quad->leaves[li] = leaf;
      
      // Update stats
      this->numQuads += 1;
      this->numQuadBytes += sizeof(*leaf);
    }

    // Recurse
    return this->allocQuad(leaf, px, py);
  }

  // If we have arrived at a terminal leaf, then we are done
  return quad;
}


// Free a quad and all the nodes in it
void RoadGraph::freeQuad(RoadGraphQuad *quad)
{
  int i;
  RoadGraphNode *node;
  RoadGraphQuad *leaf;
  
  // Free any of our nodes
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];
    free(node->next);
    node->numNext = 0;
    node->maxNext = 0;
    node->next = NULL;
    free(node->prev);
    node->numPrev = 0;
    node->maxPrev = 0;
    node->prev = NULL;
  }

  // Free node storage in the quad
  free(quad->nodes);
  quad->nodes = NULL;
  quad->numNodes = 0;
  quad->maxNodes = 0;

  // Recurse
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->freeQuad(leaf);
    quad->leaves[i] = NULL;
  }

  // Finally, free ourselves
  free(quad);
  
  return;
}


// Get the quad nearest the given position
RoadGraphQuad *RoadGraph::getNearestQuad(RoadGraphQuad *quad, float px, float py)
{
  int i;
  RoadGraphQuad *leaf;
  
  // See if the point falls within this quad; if not, give up.
  if (px > quad->px + quad->size/2)
    return NULL;
  if (px < quad->px - quad->size/2)
    return NULL;
  if (py > quad->py + quad->size/2)
    return NULL;
  if (py < quad->py - quad->size/2)
    return NULL;

  // Check our leaves
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (!leaf)
      continue;
    leaf = this->getNearestQuad(leaf, px, py);
    if (leaf)
      return leaf;
  }

  // We dont have leaves, so it must be us
  return quad;
}


#if USE_GL

// Predraw the road graph centered around some point
int RoadGraph::predraw(float px, float py, float size)
{
  if (this->root == NULL)
    return 0;
  
  // Dont do anything if we have already drawn a similar region
  if (this->roadList > 0 && fabs(size - this->roadListSize) < 1 &&
      fabs(px - this->roadListPx) < 1 && fabs(py - this->roadListPy) < 1)
    return this->roadList;

  this->roadListPx = px;
  this->roadListPy = py;
  this->roadListSize = size;
  
  // Create display list
  if (this->roadList == 0)
    this->roadList = glGenLists(1);
  glNewList(this->roadList, GL_COMPILE);

  // Recursively draw the road graph
  this->predrawQuad(this->root, px, py, size);
  
  glEndList();
    
  return this->roadList;
}


// Recursively predraw the given quad
int RoadGraph::predrawQuad(RoadGraphQuad *quad, float px, float py, float size)
{
  int i, j;
  RoadGraphQuad *leaf;
  RoadGraphNode *node, *next;

  // Check for overlap between this quad and the requested region.  If
  // there is no overlay, stop now.
  if (px - size/2 > quad->px + quad->size/2)
    return 0;
  if (px + size/2 < quad->px - quad->size/2)
    return 0;
  if (py - size/2 > quad->py + quad->size/2)
    return 0;
  if (py + size/2 < quad->py - quad->size/2)
    return 0;

  // Draw a bounding rectangle for this quad
  if (false)
  {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glColor3f(0, 1, 0);
    glBegin(GL_QUADS);
    glVertex2f(quad->px - quad->size/2, quad->py - quad->size/2);
    glVertex2f(quad->px + quad->size/2, quad->py - quad->size/2);
    glVertex2f(quad->px + quad->size/2, quad->py + quad->size/2);
    glVertex2f(quad->px - quad->size/2, quad->py + quad->size/2);
    glEnd();
  }

  if (quad->numNodes > 0)
  {
    // Draw our own nodes
    if (false)
    {
      glBegin(GL_LINES);
      for (i = 0; i < quad->numNodes; i++)
      {
        node = quad->nodes[i];
        glColor3f(0, 0, 1);
        glVertex2f(node->pose.pos.x, node->pose.pos.y);
        glColor3f(0, 1, 0);
        glVertex2f(node->pose.pos.x + 0.20 * cos(node->pose.rot),
                   node->pose.pos.y + 0.20 * sin(node->pose.rot));
      }
      glEnd();
    }

    // Draw arcs
    glBegin(GL_LINES);
    for (i = 0; i < quad->numNodes; i++)
    {
      node = quad->nodes[i];
      for (j = 0; j < node->numNext; j++)
      {
        next = node->next[j];
        if (node->laneId == 0 && next->laneId == 0)
          glColor3f(0, 0, 1);        
        else if (node->flags.isTurn)
          glColor3f(0, 0, 1);
        else
          glColor3f(1, 0, 1);
        glVertex2f(node->pose.pos.x, node->pose.pos.y);
        glVertex2f(next->pose.pos.x, next->pose.pos.y);
      }
    }
    glEnd();
  }
  else
  {
    // Recurse through the quad tree
    for (i = 0; i < 4; i++)
    {
      leaf = quad->leaves[i];
      if (leaf)
        this->predrawQuad(leaf, px, py, size);
    }
  }

  return 0;
}


#endif
