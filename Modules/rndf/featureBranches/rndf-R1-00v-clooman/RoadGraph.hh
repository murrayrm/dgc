
/* 
 * Desc: Generates a dense graph of interpolated vehicle configurations from an RNDF file.
 * Date: 3 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef ROAD_GRAPH_HH
#define ROAD_GRAPH_HH

/** @file

@brief RoadGraph maintains a dense graph of interpolated vehicle
configurations (poses) based on the RNDF data.  This graph is "best-guess"
at where the road is likely to go.

*/

#include <stdint.h>
#include "frames/pose2.h"
#include "RNDFGraph.hh"


/// @brief Data for each node in the graph (a vehicle configuration).
struct RoadGraphNode
{  
  /// Pose in site frame
  pose2f_t pose;

  /// RNDF identifier.  Note that:
  /// - laneId is zero for zone perimeters.
  /// - interId is zero for waypoints.
  uint16_t segmentId, laneId, waypointId, interId;

  /// Bitfield containing flags
  struct
  {
    /// Is this node at a stop line?
    uint8_t isStop : 1;
  
    /// Is this waypoint an exit point?
    uint8_t isExit : 1;

    /// Is this waypoint an entry point?
    uint8_t isEntry : 1;

    /// Is this node in an intersection (interpolated nodes)?
    uint8_t isTurn : 1;
    
  } flags;

  // Corresponding waypoints in the RNDF.  The next and previous will
  // be the same if the node corresponds exactly to an RNDF waypoint.
  RNDFGraphWaypoint *nextWaypoint, *prevWaypoint;

  /// Pointer for user data storage
  void *data;

  // Next nodes(s)
  uint16_t numNext, maxNext;
  RoadGraphNode **next;

  // Previous nodes(s)
  uint16_t numPrev, maxPrev;
  RoadGraphNode **prev;
};


/// @brief Quad-tree node
struct RoadGraphQuad
{
  // Quad center
  float px, py;

  // Quad size
  float size;

  // Leaves from this quad
  RoadGraphQuad *leaves[4];

  // Nodes in this quad
  int numNodes, maxNodes;
  RoadGraphNode **nodes;
};


/// @brief RoadGraph maintains a dense graph of interpolated vehicle
/// configurations (poses) based on the RNDF data.  This graph is
/// "best-guess" at where the road is likely to go.
///
/// The graph is defined in the site frame (identical to UTM apart
/// from a translation).
///
class RoadGraph
{
  public:

  /// @brief Constructor
  RoadGraph();

  /// @brief Destructor
  virtual ~RoadGraph();
  
  private:

  // Hide the copy constructor
  RoadGraph(const RoadGraph &that);
  
  public:

  /// @brief Load RNDF file.
  ///
  /// @param[in] filename RNDF file name.  
  int load(char *filename);

  /// @brief Get the canonical node corresponding to the given checkpoint ID
  RoadGraphNode *getCheckpoint(int checkpointId);

  /// @brief Get the canonical node corresponding to the given RNDF ID
  RoadGraphNode *getWaypoint(int segmentId, int laneId, int waypointId);

  /// @brief Get the node nearest the given position
  RoadGraphNode *getNearestPos(float px, float py);

  private:
  
  // Generate the road graph
  int genRoadGraph();

  // Count the number of nodes we expect so we can pre-allocate
  // storage.
  int predictNodeCount();
  
  // Generate road nodes for each RNDF waypoint,
  // using an initial guess at the orientation.
  int genWaypoints();

  // Generate intermediate nodes
  int genNonWaypoints();

  // Guess the default rotation for the given RNDF waypoint
  float guessRotation(const RNDFGraphWaypoint *waypoint);

  // Count the number of interpolated nodes we would generate between
  // these two nodes
  int countInterpolated(RoadGraphNode *nodeA, RoadGraphNode *nodeB);

  // Generate interpolated nodes
  int genInterpolated(RoadGraphNode *nodeA, RoadGraphNode *nodeB);

  /// Allocate a new node
  RoadGraphNode *allocNode(float px, float py);

  /// Insert a new arc
  int insertArc(RoadGraphNode *nodeA, RoadGraphNode *nodeB);

  // Allocate a new quad
  RoadGraphQuad *allocQuad(RoadGraphQuad *quad, float px, float py);

  /// Free a quad and all the nodes in it
  void freeQuad(RoadGraphQuad *quad);

  /// Get the quad nearest the given position
  RoadGraphQuad *getNearestQuad(RoadGraphQuad *quad, float px, float py);

  public:

  // Underlying RNDF graph
  RNDFGraph rndf;

  // Root of the quad-tree
  RoadGraphQuad *root;

  // Stats on memory usage
  int numQuads, numQuadBytes, numNodeBytes;

  public:

  // Interpolation spacing (m)
  float spacing;

  // Internal node storage for RNDF nodes (this is more efficient than
  // allocating them individually on the heap).
  int numNodes, maxNodes;
  RoadGraphNode *nodes;
   
  public:

  /// Predraw the road graph centered around some point
  /// @param[in] px,py Center point of region of interest (site frame).
  /// @param[in] size Size of region of interest (m).
  /// @return On success, returns the display list.
  int predraw(float px, float py, float size);

  private:

  // Recursively predraw the given quad
  int predrawQuad(RoadGraphQuad *quad, float px, float py, float size);
    
  // GL display list
  int roadList;

  // Last configuration drawn
  float roadListPx, roadListPy, roadListSize;
};

#endif

