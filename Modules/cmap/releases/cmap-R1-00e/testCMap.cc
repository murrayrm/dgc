#include "CMapPlus.hh"
//#include "CMapDelta.hh"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

void fillMapRandom_Double(CMap* theMap, int layerNum, int numPoints) {
  int numRows = theMap->getNumRows();
  int numCols = theMap->getNumCols();
  int row, col;
  double tempVal;

  for(int i=0; i<numPoints; i++) {
    row = (int)(((double)rand())/((double)RAND_MAX)*((double)numRows));
    col = (int)(((double)rand())/((double)RAND_MAX)*((double)numCols));
    tempVal = ((double)rand())/((double)RAND_MAX)*1.0;
    theMap->setDataWin<double>(layerNum, row, col, tempVal);
  }
}

int main() {
  int numRows, numCols;
  double resRows, resCols;
  double Northing, Easting;
  
  numRows = 100;
  numCols = 100;

  resRows=0.2;
  resCols=0.2;

  Northing = 0.0;
  Easting = 0.0;

  int layerA, layerB;

  printf("Testing CMap/CMapPlus functionality...\n");

  CMapPlus myMap;
  CMapPlus otherMap;
  myMap.initMap(Northing, Easting, numRows, numCols, resRows, resCols, 0);
  otherMap.initMap(Northing, Easting, numRows, numCols, resRows, resCols, 0);

  layerA = myMap.addLayer<double>(0.0, -32.0);
  layerB = otherMap.addLayer<double>(0.0, -32.0);

  fprintf(stderr, "%s [%d]: Filling randomly...\n", __FILE__, __LINE__);
  myMap.updateVehicleLoc(1,1);
  fillMapRandom_Double(&myMap, layerA, numRows*numCols);

  fprintf(stderr, "%s [%d]: Saving before...\n", __FILE__, __LINE__);
  myMap.saveLayer<double>(layerA, "before");

  fprintf(stderr, "%s [%d]: Copying...\n", __FILE__, __LINE__);
  otherMap.copyLayer<double>(&myMap, layerA, layerB, 0);

  fprintf(stderr, "%s [%d]: Saving after...\n", __FILE__, __LINE__);
  otherMap.saveLayer<double>(layerB, "after");

  double gr[2];
  double gNorth;
  double gEast;
  cout << "Insert coordinates to compute gradient,  N: ";
  cin >> gNorth;
  cout << endl;
  cout << "Insert coordinates to compute gradient,  E: ";
  cin >> gEast;
  cout << endl;
  myMap.getUTMGradient(1,gNorth,gEast,gr);
  cout << gr[0] << " " << gr [1]<< endl;
  return 0;
}
