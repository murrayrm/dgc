#ifndef _MAPDELTATALKER_HH_
#define _MAPDELTATALKER_HH_

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "skynet/SkynetContainer.hh"
#include "dgcutils/DGCutils.hh"
#include "CMap.hh"

class CMapdeltaTalker : virtual public CSkynetContainer {
public:
	CMapdeltaTalker();
	~CMapdeltaTalker();

	bool SendMapdelta(int mapdeltaSocket, CDeltaList* deltaList);
	bool RecvMapdelta(int mapdeltaSocket, char* pMapdelta, int* pSize);
};

#endif // _MAPDELTATALKER_H_
