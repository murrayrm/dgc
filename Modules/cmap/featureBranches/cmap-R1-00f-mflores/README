The CMap class defines a general map object for sending elevation
and/or cost data using a grid-based map.  This is the class that was
defined for DGC05 and some parts of it may be used in DGC07.

The cmap module also includes the CMapPlus class, which has logging
and perhaps some other functionalities.

CMap was created by Jeremy Gillula.  The current owner is either Sam
Pfister or Dominic Rizzo, whoever is last to edit the README file.

-- Old README --

This may be better organized at a later date, but for now, this is
notes on how the new CMap works.

BASIC HOWTO:
===========

To make a new CMap object, first declare it as you would any variable, for 
example:

  CMap myMap;

Then, initialize it as follows, where the number of rows/columns are ints, and 
the resolutions are doubles:

  myMap.initMap(double vehLocUTMNorthing, double vehLocUTMEasting, int numRows, int numCols, double resRows, double resCols,
 int verboseLevel)

(resRows specifies the height of each row in m, and resCols specifies the width 
 of each row in m.)

Then, to actually put data in the map, you must first create a layer, and keep 
track of the layer ID number:

  int sensorElevLayerID = myMap.addLayer(CM_DOUBLE);
  int costLayerID = myMap.addLayer(CM_INT);

Then, to update where the vehicle is:

  myMap.updateVehicleLoc(double vehLocUTMNorthing, double vehLocUTMEasting;

To get data from the map, use:

  double *dataPtr;
  dataPtr = (double*)myMap.getDataUTM(sensorElevLayerID, UTMNorthing, UTMEasting);

where the UTM coordinates are doubles.  Then the data is located at the address 
stored in dataPtr, so the actual value is:

  double myVal = *dataPtr;

To store the data, you can either use the reference, like:

  double newVal;
  *dataPtr = newVal;

Or you can use:

  setDataUTM(sensorElevLayerID, UTMNorthing, UTMEasting, (void*)&newVal);


FRAMES
======

There are four "frames" or coordinate systems in the map.  They are, in order 
from most abstracted (and easiest to understand) to most nitty gritty (and 
tougher to picture):

1.  UTM - the UTM frame, also known as the global frame.  A coordinate in UTM 
is denoted by its Northing, Easting, .  (It should be noted that UTM is defined 
into separate zones - but since the race occurs in only one zone, we can ignore 
that fact and assume all coordinates refer to the same zone.)

2.  Vehicle - the vehicle frame, sometimes confused with the local frame.  In 
this frame, the origin is on the ground below the center of the rear axle of 
the vehicle.  A coordinate in the vehicle frame is denoted by its XYZ 
coordinates, where X is the distance the point is in front of the vehicle, Y is 
the distance the point is to the right of the vehicle, and Z is the distance 
the point is below the vehicle.  (This gives rise to the commonly heard phrase 
"X forward, Y right, Z down.")  THIS COORDINATE SYSTEM IS NOT USED IN THE MAP - 
we just mention it here for completeness.

3.  Window - the window frame, which IS the local frame.  In this frame, the 
vehicle is always at the center of the map, and the map is always oriented so 
that its edges line up with North and East.  (This gives rise to the name 
"window" - this frame is essentially a window that scrolls along a larger UTM 
map, where the vehicle is always at the center.)  Unlike UTM coordinates, this 
frame does NOT have infinite resolution, but is divided up into cells, of which 
there are a finite number.  The resolution of this map is set when you 
initialize a map object, as is the number of rows and columns - this defines 
the size of the map, both in meters and in cells.  SOME IMPORTANT NOTES: If 
there is an odd number of rows/columns, the vehicle will ALWAYS be located in 
the center row/column in the window frame.  (That doesn't mean the vehicle is 
at the exact center of the map - it just means that the vehicle is somewhere 
within the center cell.)  If there are an even number of rows and columns, the 
vehicle is always defined to be in the cell that is adjacent to the center of 
the map and to the North and East.

4.  Memory - the memory frame is also known as the "jigsaw" frame, and is the 
way the map is stored in memory.  The vehicle moves around the 2D array, but 
once a point is placed into the map it stays there until the vehicle moves far 
enough away from the point that the point should no longer be in the Window 
frame.

