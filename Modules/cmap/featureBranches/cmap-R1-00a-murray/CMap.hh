#ifndef __CMAP_HH__
#define __CMAP_HH__
/*
  This is the new generic map class intended for use by Team Caltech for the
  DGC To understand the assumptions, definitions, and other fun stuff behind
  how this map works, READ THE README
*/


#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <limits.h>
#include <math.h>
#include <float.h>
#include <fstream>
#include "CMapLayer.hh"
#include "CMapDelta.hh"

#include "frames/coords.hh"
#include "dgcutils/DGCutils.hh"

using namespace std;

//These are the no data values associated with each data type
#define CM_INT_NO_DATA INT_MIN
#define CM_DOUBLE_NO_DATA -DBL_MAX
#define CM_DOUBLE_OUTSIDE_MAP DBL_MAX

#define MAX_MSG_SIZE 50000

/*---------------------------------------------------------------------------*/
/**
  @brief A general map class with shifting and layer capabilities

  CMap is the replacement to genMap.  It's designed to hold a 2.5D elevation
  map of any given data type, with multiple layers.  In other words, you can
  create a map and arbitrarily add any number and type of layer you like, such
  as an integer layer for determining if a given cell is in the RDDF corridor,
  a double layer for storing the elevation of a given cell, or a layer of some
  struct for some other purpose.  It's designed to be pretty flexible.

  CMap makes use of three different coordinate frames in the map.  They are, in
  order from most abstracted (and easiest to understand) to most nitty gritty
  (and hardest to understand):

  - UTM - The UTM frame, also known as the global frame.  A coordinate in UTM
  is defined by its Northing and Easting.  The map is oriented along this
  coordinate frame, such that the edges of the map are always aligned with the
  Northing and Easting directions.  (It should be noted that UTM is defined
  into separate zones - but since the race occurs in only one zone, we can
  ignore that fact and assume all coordinates refer to the same zone.)

  - Vehicle - the vehicle frame, sometimes confused with the local frame.  In
  this frame, the origin is on the ground below the center of the rear axle of
  the vehicle.  A coordinate in the vehicle frame is denoted by its XYZ
  coordinates, where X is the distance the point is in front of the vehicle, Y
  is the distance the point is to the right of the vehicle, and Z is the
  distance the point is below the vehicle.  (This gives rise to the commonly
  heard phrase "X forward, Y right, Z down.")  THIS COORDINATE SYSTEM IS NOT
  USED IN THE MAP - we just mention it here for completeness.

  - Window - the window frame, which IS the local frame.  In this frame, the
  vehicle is always at the center of the map, and the map is always oriented so
  that its edges line up with North and East.  (This gives rise to the name
  "window" - this frame is essentially a window that scrolls along a larger UTM
  map, where the vehicle is always at the center.)  Unlike UTM coordinates,
  this frame does NOT have infinite resolution, but is divided up into cells,
  of which there are a finite number.  The resolution of this map is set when
  you initialize a map object, as is the number of rows and columns - this
  defines the size of the map, both in meters and in cells.  SOME IMPORTANT
  NOTES: If there is an odd number of rows/columns, the vehicle will ALWAYS be
  located in the center row/column in the window frame.  (That doesn't mean the
  vehicle is at the exact center of the map - it just means that the vehicle is
  somewhere within the center cell.)  If there are an even number of rows and
  columns, the vehicle is always defined to be in the cell that is adjacent to
  the center of the map and to the North and East.  Window coordinates are
  defined on the interval [0, numRows) and [0, numCols), where (0, 0) is the
  bottom left cell in the window.

  - Memory - the memory frame is also known as the "jigsaw" frame, and
  is the way the map is stored in memory.  The vehicle moves around
  the 2D array, but once a point is placed into the map it stays there
  until the vehicle moves far enough away from the point that the
  point should no longer be in the Window frame.  A normal user should
  not need to pay attention to this frame.

  As mentioned before, CMap uses layers to allow you to store multiple
  types of data.  All the layers of a single CMap object are always
  aligned to the same window frame, with the same resolution and
  number of rows or columns.  (The only difference between any two
  layers is the type of data they can store, and what's actually
  stored in them.)  Virtually any type of struct or basic data type
  can be stored in a layer.  To achieve this flexibility, CMap makes
  use of C++ templates.  Be careful when using CMap's templated
  functions - make sure that you always use the right type when
  accessing a given layer!  CMap will NOT convert between types for
  you - so if you add a double layer and try to put an int value into
  it, bad things will happen.

*/
/*---------------------------------------------------------------------------*/
class CMap {
public:
  //! A basic constructor, initializes internal memory management stuff
  CMap();
  //! A basic destructor - doesn't do any clean up yet!
  ~CMap();

  //Not yet implemented
  //CMap(const CMap &other);
  //CMap &operator=(const CMap &other);


  /*------------------------------------------------------------------------*/
  /**
     @brief    Initialize the map
     @param    UTMNorthing     The UTM northing of the center cell of the map
     @param    UTMEasting      The UTM easting of the center cell of the map
     @param    numRows         The numbers of rows in the map
     @param    numCols         The number of columns in the map	
     @param    resRows         The height of a row (in m)
     @param    resCols         The width of a column (in m)
     @param    verboseLevel    Unused at present
     @return   Standard CMap status code
     
     This initializes a CMap object with the given parameters.  For example, to
     create a map centered at a UTM of 300 Northing, 100 Easting, with 200 rows
     and 400 columns, with a row height of 2 and a column width of 1 (which
     ends up making the map square in the UTM frame) you would do:
     
     @code
     myMap.initMap(300.0, 100.0, 200, 400, 2.0, 1.0, 0);
     @endcode

     initMap should only be called once per object.  If it is called more than
     once on a CMap object, further behavior of that object is undefined.

  */
  /*-------------------------------------------------------------------------*/
  int initMap(double UTMNorthing, double UTMEasting, int numRows, int numCols, double resRows, double resCols, int verboseLevel);

  int initMap(char* filename);

  template <class T>
  int copyLayer(CMap* otherMap, int sourceLayerNum, int targetLayerNum, int options);


  //! Returns the number of rows in a CMap object
  int getNumRows() {return _numRows;}
  //! Returns the number of columns in a CMap object
  int getNumCols() {return _numCols;}
  //! Returns the height (in m) of rows in a CMap object
  double getResRows() {return _resRows;}
  //! Returns the width (in m) of columns in a CMap object
  double getResCols() {return _resCols;}

  //! Returns the current number of layers in a CMap object
  int getNumLayers() {return _numLayers;};

  //! Returns the UTM Northing of the bottom left corner of the window frame
  double getWindowBottomLeftUTMNorthing() {return _windowBottomLeftUTMNorthing;}
  //! Returns the UTM Easting of the bottom left corner of the window frame
  double getWindowBottomLeftUTMEasting() {return _windowBottomLeftUTMEasting;}
  //! Returns the UTM Northing of the top right corner of the window frame
  double getWindowTopRightUTMNorthing() {return _windowTopRightUTMNorthing;}
  //! Returns the UTM Easting of the top right corner of the window frame
  double getWindowTopRightUTMEasting() {return _windowTopRightUTMEasting;}

  long int getWindowBottomLeftUTMNorthingRowResMultiple() {
    return _windowBottomLeftUTMNorthingRowResMultiple;
  };
  long int getWindowBottomLeftUTMEastingColResMultiple() {
    return _windowBottomLeftUTMEastingColResMultiple;
  };

  //! Returns the UTM northing at which the vehicle is located
  double getVehLocUTMNorthing() {return _vehLocUTMNorthing;};
  //! Returns the UTM Easting at which the vehicle is located
  double getVehLocUTMEasting() {return _vehLocUTMEasting;};

  //! Returns the window row coordinate at which the vehicle is located
  int getVehLocWinRow() {return _vehLocWinRow;};
  //! Returns the window column coordinate at which the vehicle is located
  int getVehLocWinCol() {return _vehLocWinCol;};

  /*------------------------------------------------------------------------*/
  /**
     @brief    Add a layer to the map
     @param    noDataVal     The default data value for cells of this layer
     @param    outsideMapVal The value to return for cells outside the map
     @return   ID number for the layer created
     
     This adds a layer of type T to a CMap object.  To store any data, a layer
     must be added.  (CMap does not have any default layers on initialization.)
     All layers have the same size (both in number of rows and columns, and
     width and height in m).  Sample code to add an integer layer would be:

     @code
     myMap.addLayer<int>(0, -1);
     @endcode

     addLayer returns an integer ID number for the layer that was just created
     - all future interaction with that layer will require that ID number.

  */
  /*-------------------------------------------------------------------------*/
  template <class T>
  int addLayer(T noDataVal, T outsideMapVal, bool useDeltas=false);

  template <class T>
  int addLayer(char* configFilename, bool useDeltas=false);

  /*------------------------------------------------------------------------*/
  /**
     @brief    Shifts the map while doing minimal data copying
     @param    UTMNorthing  The UTM Northing at which the map should re-center
     @param    UTMEasting   The UTM Easting at which the map should re-center
     @return   Standard CMap status code
     
     Since the map is a finite window, it is helpful to be able to shift it.
     This method does precisely that.  The window is shifted to be centered
     around the new Northing and Easting coordinates, while doing minimal data
     copying.

  */
  /*-------------------------------------------------------------------------*/
  int updateVehicleLoc(double UTMNorthing, double UTMEasting);

  int checkBoundsUTM(double UTMNorthing, double UTMEasting);

  int roundUTMToCellBottomLeft(double UTMNorthing, double UTMEasting, double* RoundedUTMNorthing, double* RoundedUTMEasting);

  /*------------------------------------------------------------------------*/
  /**
     @brief    Returns the value of the cell at the specified UTM coordinates
     @param    layerNum     The ID number of the desired layer to access
     @param    UTMNorthing  The UTM Northing coordinate of the desired cell
     @param    UTMEasting   The UTM Easting coordinate of the desired cell
     @return   Reference to the proper cell value
     
     One of the two basic accessor functions for data in a CMap.  The layerNum
     specifies which layer to access, and then returns a reference to the value
     at the given Northing and Easting.  If you use the return value of this
     method as a reference, then it is possible to directly change the value of
     the data at that cell, which is faster than using the setData mutator.
     For example:

     @code
     double& myValueReference = myMap.getDataUTM<double>(doubleLayerID, myNorthing, myEasting);
     myValueReference = 10.0;
     @endcode

     The above code will change the value of the cell at (myNorthing,
     myEasting) to 10.0 regardless of what it was before.  On the other hand,
     the below code will not:

     @code
     double myValue = myMap.getDataUTM<double>(doubleLayerID, myNorthing, myEasting);
     myValue = 10.0;
     @endcode

     If you ever seem to have erroneous data in a layer of your CMap, you
     should first check and make sure you aren't accidentally changing any
     cells by reference when you don't mean to be.

     If you seem to be getting erroneous data back from your CMap, make sure
     that you're using the right type when making the call to getDataUTM.  (You
     should use the same type as the one you used to add the layer you're
     accessing.)  CMap will NOT convert data values of one type to another upon
     returning, so the following code will not work properly:

     @code
     myLayerID = myMap.addLayer<double>(0.0, 0.0, 0);
     ... code to fill in the map with values here ...
     int myValue = myMap.getDataUTM<int>(myLayerID, someNorthing, someEasting)
     @endcode

     God only knows what value will be stored in myValue (in other words,
     behavior in this situation is undefined).  So don't do it.

     IMPORTANT WARNINGS: If you set data in a CMap by reference, check to make
     sure that the value you receive is not the outside map value before you
     set the data!  The lengthy explanation: When you add a layer to the map,
     you spceify what value to return when the coordinates given are outside
     the map's window, like so:

     @code
     someLayerID = myMap.addLayer<some_type>(noDataVal, outsideMapVal);
     @endcode;

     When you get a value from getDataUTM(), if the UTM coordinates you specify
     are outside the map, it will return this value.  Thus:

     @code
     some_type& myVal = myMap.getDataUTM<some_type>(someLayerID, northing_thats_outside, easting_thats_outside);
     myVal == outsideMapVal; // <---- Will return TRUE
     @endcode

     Then if you change myVal, then all further times you pass coordinates that
     are outside the map, you will get the new value you specified.  I.e. if
     you do this:

     @code
     myVal = someOtherVal;
     @endcode

     Then try to access some value outside the map, it will equal someOtherVal.
     If all of this confused you, just use the following code as your example:

     @code
     ... initialization code ...
     someLayerID = myMap.addLayer<some_type>(noDataVal, outsideMapVal)
     ... any other code here ...
     for(...some loop code...) {
        some_type& myVal = myMap.getDataUTM<some_type>(someLayerID, someNorthing, someEasting);
	if(some_type != outsideMapVal) {
	  ... code here ...
	  myVal = someNewVal;  //It's safe to do this
	} else {
	  //NOT SAFE TO CHANGE myVal!!!
	  ... do something because the value was outside the map ...
	  ... but don't change myVal ...
	}
     }
     @endcode

  */
  /*-------------------------------------------------------------------------*/
  template <class T>
  T& getDataUTM(int layerNum, double UTMNorthing, double UTMEasting);

  /*------------------------------------------------------------------------*/
  /**
     @brief    Returns the value of the cell at the specified window coords
     @param    layerNum     The ID number of the desired layer to access
     @param    winRow       The row number of the desired cell
     @param    winCol       The column number of the desired cell
     @return   Reference to the proper cell value
     
     This function works exactly like getDataUTM, only using rows and columns
     in window coordinates instead.  For more details read on.

     The second basic accessor function for data in a CMap.  The layerNum
     specifies which layer to access, and then returns a reference to the value
     at the given window frame row (equivalent to northing) and column
     (equivalent to easting).  If you use the return value of this method as a
     reference, then it is possible to directly change the value of the data at
     that cell, which is faster than using the setData mutator.  For more
     information on how to do this properly, read the documentation for the
     getDataUTM method.

     If you seem to be getting erroneous data back from your CMap, make sure
     that you're using the right type when making the call to getDataWin.  (You
     should use the same type as the one you used to add the layer you're
     accessing.)  For more information on this, read the documentation for the
     getDataUTM method.

     IMPORTANT WARNINGS: If you set data in a CMap by reference, check to make
     sure that the value you receive is not the outside map value before you
     set the data!  For the lengthy explanation, see the documentation for the
     getDataUTM method.

  */
  /*-------------------------------------------------------------------------*/
  template <class T>
  T& getDataWin(int layerNum, int winRow, int winCol);

  /*------------------------------------------------------------------------*/
  /**
     @brief    Returns the value of the cell at the specified UTM coordinates
     @param    layerNum     The ID number of the desired layer to access
     @param    UTMNorthing  The UTM Northing coordinate of the desired cell
     @param    UTMEasting   The UTM Easting coordinate of the desired cell
     @param    cellValue    The value to set the desired cell to
     @return   Standard CMap status code
     
     If for some reason you don't want to set things by reference (either
     because it's slightly more dangerous, or speed isn't as important) you can
     use the setDataUTM method.  This will find the cell at the given UTM
     coordinates, and set its value to cellValue.  If the coordinates are
     outside the map window, nothing happens.

     If you seem to be getting erroneous data back from your CMap, make sure
     that you're using the right type when making the call to getDataUTM.  (You
     should use the same type as the one you used to add the layer you're
     accessing.)  CMap will NOT convert data values of one type to another upon
     setting, so the following code will not work properly:

     @code
     myLayerID = myMap.addLayer<double>(0.0, 0.0);
     ... code to fill in the map with values here ...
     int myValue = something;
     returnStatus = myMap.setDataUTM<int>(myLayerID, someNorthing, someEasting, myValue)
     @endcode

     God only knows what value will be stored in the cell (in other words,
     behavior in this situation is undefined).  So don't do it.

  */
  /*-------------------------------------------------------------------------*/
  template <class T>
  int setDataUTM(int layerNum, double UTMNorthing, double UTMEasting, T cellValue);

  template <class T>
  int setDataUTM_Delta(int layerNum, double UTMNorthing, double UTMEasting, T cellValue, pthread_rwlock_t* expandDeltaLock = NULL);

  void sendDelta(int layerNum, int socket);

  template <class T>
  int resetDelta(int layerNum);

  template <class T>
  CDeltaList* serializeDelta(int layerNum, unsigned long long timestamp = 0);

  template <class T>
  CDeltaList* serializeFullMapDelta(int layerNum, unsigned long long timestamp = 0);

  template <class T>
  int applyDelta(int layerNum, void* serializedDelta, int byteSize, int shift=1);

  int deltaSize(int layerNum);

  
  /*------------------------------------------------------------------------*/
  /**
     @brief    Parses a mapDelta for its size in cells and returns it
     @param    serializedData   A pointer to the mapDelta
     @return   Size of the delta in number of cells
     
     If you want to apply a mapDelta to a map manually, use this function to
     first see how many cells the mapDelta contains.  Then, you can use the
     getDeltaVal<>() method to extract each cell one-by-one.  For example
     usage, see the documentation for getDeltaVal<>().

  */
  /*-------------------------------------------------------------------------*/
  int getDeltaSize(void* serializedData);

  /*------------------------------------------------------------------------*/
  /**
     @brief    Returns the ith value in a map delta
     @param    i               The index of the cell you want to get
     @param    layerNum        The layer the cell will belong to
     @param    serializedData  A pointer to the mapDelta
     @param    UTMNorthing     A pointer to where to store the UTM Northing
     @param    UTMEasting      A pointer to where to store the UTM Easting
     @return   The value of the given cell
     
     If you want to apply a mapDelta to a map manually, use this function to
     extract individual cells from the map delta so that you can apply them.
     Note that the layer whose ID is given in layerNum must be of the same
     type as the delta itself - e.g. only give the layerID of a double layer
     if the data in the delta is of type double.

     Example usage:

     @code
     int numCells = myMap.getDeltaSize(myDelta);
     double myNorthing, myEasting;
     for(int i=0; i<numCells; i++) {
       Type myVal = getDeltaVal<Type>(i, layerID, myDelta, &myNorthing, &myEasting);
       ...do something with myVal here, such as changing a different layer in
	  a certain way based on what myVal was, like blurring around it, or
	  painting cost into a map...
      some_processing_function(myVal, myMap);
     }
     @endcode

  */
  /*-------------------------------------------------------------------------*/
  template <class T>
  T getDeltaVal(int i, int layerNum, void* serializedData, double* UTMNorthing, double* UTMEasting);

  int getCurrentDeltaSize(int layerNum);

  template <class T>
  T getCurrentDeltaVal(int i, int layerNum, double* UTMNorthing, double* UTMEasting);


  /*------------------------------------------------------------------------*/
  /**
     @brief    Returns the value of the cell at the specified UTM coordinates
     @param    layerNum     The ID number of the desired layer to access
     @param    winRow       The row number of the desired cell
     @param    winCol       The column number of the desired cell
     @param    cellValue    The value to set the desired cell to
     @return   Standard CMap status code
     
     If for some reason you don't want to set things by reference (either
     because it's slightly more dangerous, or speed isn't as important) you can
     use the setDataWin method.  This will find the cell at the given window
     coordinates, and set its value to cellValue.  If the coordinates are
     outside the map window, nothing happens.

     For more information and warnings, look at the documenation for setDataUTM
     (which has the same behavior, except works on UTM coordinates instead of
     window coordinates).

  */
  /*-------------------------------------------------------------------------*/
  template <class T>
  int setDataWin(int layerNum, int winRow, int winCol, T cellvalue);


  template <class T>
  int setDataWin_Delta(int layerNum, int winRow, int winCol, T cellvalue, pthread_rwlock_t* expandDeltaLock = NULL);

  /*------------------------------------------------------------------------*/
  /**
     @brief    Clears every layer in the map
     @return   Standard CMap status code
     
     Simply sets every cell in every layer in the map to the no data value you
     passed into each layer when you add the layer.

  */
  /*-------------------------------------------------------------------------*/
  int clearMap();

  /*------------------------------------------------------------------------*/
  /**
     @brief    Clears the given layer
     @param    layerNum   The ID number of the layer to clear
     @return   Standard CMap status code
     
     Simply sets every cell in the given layer in the map to the no
     data value you passed into that layer when you added the layer.

  */
  /*-------------------------------------------------------------------------*/
  int clearLayer(int layerNum);

  int Win2UTM(int winRow, int winCol, double *UTMNorthing, double *UTMEasting);
  int UTM2Win(double UTMNorthing, double UTMEasting, int *winRow, int *winCol);

  /*------------------------------------------------------------------------*/
  /**
     @brief    Returns the no data value for a given layer
     @param    layerNum   The ID number of the layer
     @return   The no data value for a given layer
     
     Returns the no data value you passed into that layer when you
     added the layer.

  */
  template <class T>
  inline T getLayerNoDataVal(int layerNum) {return *(T*)_layerAddrs[layerNum]->getNoDataVal();};

  /*------------------------------------------------------------------------*/
  /**
     @brief    Returns the outside map value for a given layer
     @param    layerNum   The ID number of the layer
     @return   The outside mapvalue for a given layer
     
     Returns the outside map value you passed into that layer when you
     added the layer.

  */
  template <class T>
  T getLayerOutsideMapVal(int layerNum) {return *(T*)_layerAddrs[layerNum]->getOutsideMapVal();};

  int getExposedRowBoxUTM(NEcoord vertices[]);
  int getExposedColBoxUTM(NEcoord vertices[]);
  int getEntireMapBoxUTM(NEcoord vertices[]);

/** This function returns map data that lies in a rectangle centered at
		(UTMNorthing, UTMEasting), with dimensions 2radlat x 2radlong. Returned data
		are the cell values in pF and the coords of the cell centers in a coordinate
		system that's centered at (UTMNorthing, UTMEasting), rotated to have X in
		the theta direction, scaled to meters
 */
  template <class T>
  void getDataRectangle(int layerNum, double UTMNorthing, double UTMEasting,
			double cos_theta, double sin_theta, double radlat, double radlong, 
			T* pF, double* pX, double* pY, int* numCells);
	
  //! This function is purposely undocumented so nobody else uses it but me until it is fully tested
  template <class T>
  int interpolateFromOtherMap(int destinationLayer, CMap* sourceMap, int sourceLayer);


  void constrainRows(int currentRow, int numRows, int& minRow, int& maxRow);
  void constrainCols(int currentCol, int numCols, int& minCol, int& maxCol);

	//These are the return values CMap methods return to tell you whether they
  //succeeded or encountered an error
  enum {
    CM_OK,
    CM_OUT_OF_BOUNDS,
    CM_UNKNOWN_ERROR,
    
    CM_STATUS_COUNT
  };


protected:
  int _numRows;
  int _numCols;
  double _resRows;
  double _resCols;

  BaseLayer** _layerAddrs;
  int _numLayers;

  int _verboseLevel;

  //All sorts of coordinates in various systems
  //multiples
  long int _windowBottomLeftUTMNorthingRowResMultiple;
  long int _windowBottomLeftUTMEastingColResMultiple;

  //UTM!
  double _windowBottomLeftUTMNorthing;
  double _windowBottomLeftUTMEasting;
  double _windowTopRightUTMNorthing;
  double _windowTopRightUTMEasting;
  double _vehLocUTMNorthing;
  double _vehLocUTMEasting;
  NEcoord _exposedRowBoxUTM[4];
  NEcoord _exposedColBoxUTM[4];


  //Window!
  //These values are constant
  int _windowBottomLeftWinRow;
  int _windowBottomLeftWinCol;
  int _windowTopRightWinRow;
  int _windowTopRightWinCol;
  int _vehLocWinRow;
  int _vehLocWinCol;
  int _exposedRowBoxWin[4][2];
  int _exposedColBoxWin[4][2];

  //Mem!
  //The vehicle is located within this cell
  int _windowBottomLeftMemCol;
  int _windowBottomLeftMemRow;
  int _windowTopRightMemCol;
  int _windowTopRightMemRow;
  int _vehLocMemRow;
  int _vehLocMemCol;

  unsigned long long _latestTimestamp;

  int clearMapWinRow(int winRow);
  int clearMapWinCol(int winCol);

  int clearMapMemRow(int memRow);
  int clearMapMemCol(int memCol);

  int clearLayerWinRow(int layerNum, int winRow);
  int clearLayerWinCol(int layerNum, int winCol);

  int clearLayerMemRow(int layerNum, int row);
  int clearLayerMemCol(int layerNum, int row);

  int Mem2Win(int memRow, int memCol, int *winRow, int *winCol);
  //Returns the UTM coordinate of the center of the cell at memory coordinates memRow, memCol
  int Mem2UTM(int memRow, int memCol, double *UTMNorthing, double *UTMEasting);

  int Win2Mem(int winRow, int winCol, int *memRow, int *memCol);
  //Returns the UTM coordinate of the center of the cell at window coordinates winRow, winCol

  int UTM2Mem(double UTMNorthing, double UTMEasting, int *memRow, int *memCol);

  //Check to make sure a value is in the map
  int checkBoundsWin(int winRow, int winCol);
  int checkBoundsMem(int memRow, int memCol);

  void constrainRange(int start, int numDiff, int min, int max, int& resultMin, int& resultMax);

  template <class T>
  T& getDataMem(int layerNum, int memRow, int memCol);
  void** _deltaAddrs;
  CDeltaList** _deltaListAddrs;
  int* _maxCells;
  int* _numCells;
  int* _cellSize;
  int* _deltaLayerID;
};

template <class T>
int CMap::copyLayer(CMap* otherMap, int sourceLayerNum, int targetLayerNum, int options) {
  double newNorthing, newEasting;
  newNorthing = otherMap->getVehLocUTMNorthing();
  newEasting = otherMap->getVehLocUTMEasting();

  _windowBottomLeftUTMNorthingRowResMultiple=(long int)floor(newNorthing/_resRows - _numRows/2)-(_numRows+1);
  _windowBottomLeftUTMEastingColResMultiple=(long int)floor(newEasting/_resCols - _numCols/2)-(_numCols+1);

  updateVehicleLoc(newNorthing, newEasting);

  int otherMapColOffset, otherMapRowLength, otherMapRowOffset;
  otherMapRowLength = sizeof(T)*_numCols;
  otherMapColOffset = sizeof(T)*otherMap->_windowBottomLeftMemCol;
  otherMapRowOffset = otherMap->_windowBottomLeftMemRow;

  char* dest;
  char* src;

  char* dest_base;
  char* src_base;

  dest_base = (char*)_layerAddrs[targetLayerNum]->getDataAddr();
  src_base = (char*)otherMap->_layerAddrs[sourceLayerNum]->getDataAddr();

  int size_chunk_one, size_chunk_two, otherRow;


  //chunk_one is the chunk whose left edge is a map border
  //chunk_two is the chunk whose right edge is a map border
  size_chunk_one = otherMapRowLength - otherMapColOffset;
  size_chunk_two = otherMapColOffset;


  for(int row=0; row<_numRows; row++) {
    dest = dest_base+otherMapRowLength*row;
    otherRow = otherMapRowOffset + row;
    if(otherRow>=_numRows) otherRow-=_numRows;
    src = src_base+otherMapRowLength*otherRow+otherMapColOffset;
    memcpy(dest, src, size_chunk_one);
    dest+=size_chunk_one;
    src-=size_chunk_two;
    memcpy(dest, src, size_chunk_two);
  }

  return 0;
}


template <class T>
T CMap::getDeltaVal(int i, int layerNum, void* serializedDelta, double* UTMNorthing, double* UTMEasting) {
  //account for the stuff infront of data
 
  if(i<getDeltaSize(serializedDelta)) {
    //    *UTMNorthing = *(double*)((char*)serializedDelta + sizeof(int) + 2*sizeof(double) + _cellSize[layerNum]*i);
    // *UTMEasting = *(double*)((char*)serializedDelta + sizeof(int) + 2*sizeof(double) + _cellSize[layerNum]*i + sizeof(double));
    // cellValue = *(T*)((char*)serializedDelta + sizeof(int) + 2*sizeof(double) + _cellSize[layerNum]*i + 2*sizeof(double));
    *UTMNorthing =  (*((deltaCell<T>*)((char*)(serializedDelta)+sizeof(deltaHeader) + i*sizeof(deltaCell<T>)))).UTMNorthing;
    *UTMEasting =   (*((deltaCell<T>*)((char*)(serializedDelta)+sizeof(deltaHeader) + i*sizeof(deltaCell<T>)))).UTMEasting;

    return (*((deltaCell<T>*)((char*)(serializedDelta)+sizeof(deltaHeader) + i*sizeof(deltaCell<T>)))).cellValue;
  }

  return getLayerOutsideMapVal<T>(layerNum);
}


template <class T>
T CMap::getCurrentDeltaVal(int i, int layerNum, double* UTMNorthing, double* UTMEasting) {
	*UTMNorthing =  (*((deltaCell<T>*)((char*)_deltaAddrs[layerNum]+ i*sizeof(deltaCell<T>)))).UTMNorthing;
	*UTMEasting =   (*((deltaCell<T>*)((char*)_deltaAddrs[layerNum]+ i*sizeof(deltaCell<T>)))).UTMEasting;

	return (*((deltaCell<T>*)((char*)(_deltaAddrs[layerNum])+i*sizeof(deltaCell<T>)))).cellValue;
}


template <class T>
int CMap::applyDelta(int layerNum, void* serializedDelta, int byteSize, int shift) {
  T cellValue;
  double UTMNorthing, UTMEasting;

  deltaHeader tempHeader;
  memcpy(&tempHeader, serializedDelta, sizeof(deltaHeader));

  if(byteSize != (int)(sizeof(deltaHeader) + sizeof(deltaCell<T>)*tempHeader.numCells)) {
    printf("Corrupted mapDelta! on layer %d\n", layerNum);
    cout << "bytesize was " << byteSize << " and numcells was " << tempHeader.numCells
				 << " and  total size shoulda been " << sizeof(deltaHeader) + sizeof(deltaCell<T>)*tempHeader.numCells << endl;
    return 0;
  }

  if(tempHeader.numRows!=getNumRows() ||
     tempHeader.numCols!=getNumCols() ||
     tempHeader.resRows!=getResRows() ||
     tempHeader.resCols!=getResCols()) {
    printf("%s [%d]: Mismatch in map delta resolution or size on layer %d (Map: %d numRows, %d numCols, %lf resRows, %lf resCols; Delta: %d numRows, %d numCols, %lf resRows, %lf resCols)! - Ignoring delta\n",
					 __FILE__, __LINE__, layerNum,
					 getNumRows(), getNumCols(), getResRows(), getResCols(),
					 tempHeader.numRows, tempHeader.numCols, tempHeader.resRows, tempHeader.resCols);
    printf("%s [%d]: The difference in resolution is %lf (rows), %lf (cols)\n", __FILE__, __LINE__,
					 getResRows()-tempHeader.resRows, getResCols()-tempHeader.resCols);
    return 0;
  }

  //if(shift==1 && tempHeader.timestamp >= _latestTimestamp) {
  if(shift==1) {
    updateVehicleLoc(tempHeader.vehicleLocUTMNorthing,
		     tempHeader.vehicleLocUTMEasting);
  }

  _latestTimestamp = tempHeader.timestamp;

  //printf("%d cells!\n", tempHeader.numCells);
  for(int i=0; i<tempHeader.numCells; i++) {
    cellValue = getDeltaVal<T>(i, layerNum, serializedDelta,& UTMNorthing,& UTMEasting);
    setDataUTM<T>(layerNum, UTMNorthing, UTMEasting, cellValue);
  }

  return CM_OK;
}


template <class T>
CDeltaList* CMap::serializeFullMapDelta(int layerNum, unsigned long long timestamp)
{
	int i,j;
	for(i=0; i < _numRows ; i++){
		for(j=0; j < _numCols ; j++){
			setDataWin_Delta<T>(layerNum, i,j,getDataWin<T>(layerNum,i, j));
		}				
	}
	return serializeDelta<T>(layerNum, timestamp); 
}

template <class T>
CDeltaList* CMap::serializeDelta(int layerNum, unsigned long long timestamp) {
  deltaHeader tempHeader;

  tempHeader.vehicleLocUTMNorthing = getVehLocUTMNorthing();
  tempHeader.vehicleLocUTMEasting = getVehLocUTMEasting();
  tempHeader.resRows = getResRows();
  tempHeader.resCols = getResCols();
  tempHeader.numRows = getNumRows();
  tempHeader.numCols = getNumCols();
  tempHeader.timestamp = timestamp;

  int numCellsLeft = _numCells[layerNum];
  int numCellsSent = 0;
  int numCellsPerDelta;

  if(!_deltaListAddrs[layerNum]->isEmpty()) {
    _deltaListAddrs[layerNum]->clearList();
  }

  do {
    numCellsPerDelta = min((int)((MAX_MSG_SIZE - sizeof(deltaHeader))/sizeof(deltaCell<T>)),
			   numCellsLeft);
    tempHeader.numCells = numCellsPerDelta;
    _deltaListAddrs[layerNum]->addDelta(tempHeader,
					(char*)_deltaAddrs[layerNum]+numCellsSent*sizeof(deltaCell<T>),
					numCellsPerDelta*sizeof(deltaCell<T>));
    numCellsLeft-=numCellsPerDelta;
    numCellsSent+=numCellsPerDelta;
  } while(numCellsLeft!=0);

  return _deltaListAddrs[layerNum];
}


template <class T>
int CMap::resetDelta(int layerNum) {
  double UTMNorthing, UTMEasting;

  for(int i=0; i<_numCells[layerNum]; i++) {
    UTMNorthing = (*((deltaCell<T>*)((char*) _deltaAddrs[layerNum]+ i*sizeof(deltaCell<T>)))).UTMNorthing;
    UTMEasting = (*(deltaCell<T>*)((char*) _deltaAddrs[layerNum] + i*sizeof(deltaCell<T>))).UTMEasting;
    setDataUTM<int>(_deltaLayerID[layerNum], UTMNorthing, UTMEasting, -1);
  }
  _deltaListAddrs[layerNum]->clearList();
  _numCells[layerNum]=0;
  //clearLayer(_deltaLayerID[layerNum]);
  
  return CM_OK;
}


template <class T>
int CMap::addLayer(T noDataVal, T outsideMapVal, bool useDeltas) {
  int layerNum;

  layerNum = _numLayers;
  _numLayers++;

  _layerAddrs = (BaseLayer**)realloc(_layerAddrs, _numLayers*sizeof(BaseLayer*));
  _deltaAddrs = (void**)realloc(_deltaAddrs, _numLayers*sizeof(void*));
  _deltaListAddrs = (CDeltaList**)realloc(_deltaListAddrs, _numLayers*sizeof(CDeltaList*));
  _maxCells = (int*)realloc(_maxCells, _numLayers*sizeof(int));
  _numCells = (int*)realloc(_numCells, _numLayers*sizeof(int));
  _cellSize = (int*)realloc(_cellSize, _numLayers*sizeof(int));
  _deltaLayerID = (int*)realloc(_deltaLayerID, _numLayers*sizeof(int));
  //Check to make sure realloc was successful
  if(_layerAddrs == NULL || _deltaAddrs==NULL || _maxCells == NULL || _numCells == NULL || _cellSize == NULL || _deltaListAddrs==NULL) {
    printf("%s [%d]: Fatal error while attempting to allocate memory for layer %d\n", __FILE__, __LINE__, layerNum);
    printf("%s [%d]: The program cannot continue!  Quitting!\n", __FILE__, __LINE__);
    exit(1);
  } else {
    _layerAddrs[layerNum] = new CLayer<T>(_numRows, _numCols, noDataVal, outsideMapVal);
    _deltaListAddrs[layerNum] = new CDeltaList();
    if ( useDeltas == true){
      //_deltaAddrs[layerNum] = new deltaCell<T>[1];
      //_deltaAddrs[layerNum] = deltaCell<T>;
      _deltaAddrs[layerNum] = malloc(_numRows*_numCols*sizeof(deltaCell<T>));
      _deltaLayerID[layerNum] = addLayer<int>(-1,-2,0);
    } else{
      _deltaAddrs[layerNum] = NULL;
      _deltaLayerID[layerNum] = -1;
    }
    _maxCells[layerNum] = _numRows*_numCols;
    _numCells[layerNum] = 0;
    _cellSize[layerNum] = 2*sizeof(double)+sizeof(T);

    clearLayer(layerNum);
    return layerNum;
  }
}


template <class T>
T& CMap::getDataUTM(int layerNum, double UTMNorthing, double UTMEasting) {
  int memRow, memCol;
  UTM2Mem(UTMNorthing, UTMEasting, &memRow, &memCol);

  return getDataMem<T>(layerNum, memRow, memCol);
}


template <class T>
T& CMap::getDataWin(int layerNum, int winRow, int winCol) {
  int memRow, memCol;

  Win2Mem(winRow, winCol, &memRow, &memCol);

  return getDataMem<T>(layerNum, memRow, memCol);
}


template <class T>
T& CMap::getDataMem(int layerNum, int memRow, int memCol)
{
  return *(T*)_layerAddrs[layerNum]->getDataMem(memRow, memCol);
}


template <class T>
int CMap::setDataUTM(int layerNum, double UTMNorthing, double UTMEasting, T cellValue)
{
  T& tempRef = getDataUTM<T>(layerNum, UTMNorthing, UTMEasting);
  if(tempRef != getLayerOutsideMapVal<T>(layerNum)) {
    tempRef = cellValue;
  }

  return CM_OK;
}


template <class T>
int CMap::setDataWin_Delta(int layerNum, int winRow, int winCol, T cellValue, pthread_rwlock_t* expandDeltaLock) {
  double UTMNorthing, UTMEasting;

  Win2UTM(winRow, winCol, &UTMNorthing, &UTMEasting);
  //printf("%s [%d]: %lf, %lf\n", __FILE__, __LINE__, UTMNorthing, UTMEasting);
  

  return setDataUTM_Delta(layerNum, UTMNorthing, UTMEasting, cellValue, expandDeltaLock);
}

template <class T>
int CMap::setDataUTM_Delta(int layerNum, double UTMNorthing, double UTMEasting, T cellValue, pthread_rwlock_t* expandDeltaLock) {
  if(checkBoundsUTM(UTMNorthing, UTMEasting)==CM_OK && _deltaAddrs[layerNum] != NULL ) {
    if(_numCells[layerNum]==_maxCells[layerNum]) {
// 			if(expandDeltaLock != NULL)
// 				DGCwritelockRWLock(expandDeltaLock);
      _deltaAddrs[layerNum] = realloc(_deltaAddrs[layerNum],( _maxCells[layerNum]+1)*sizeof(deltaCell<T>));
      if(_deltaAddrs[layerNum] == NULL) {
				printf("%s [%d]: ERRROR MEMEORY DIE ACK KJAFHKLoxford english dictionaryJSDF\n", __FILE__, __LINE__);
				exit(0);
      } else {
				_maxCells[layerNum]++;
      }
    } else {
// 			if(expandDeltaLock != NULL)
// 				DGCreadlockRWLock(expandDeltaLock);
		}

    //check if delta Layer ID is okay
    int cellModified = getDataUTM<int>(_deltaLayerID[layerNum], UTMNorthing, UTMEasting);

    if(cellModified < 0) {
			(*(deltaCell<T>*)((char*) _deltaAddrs[layerNum] + _numCells[layerNum]*sizeof(deltaCell<T>))).UTMNorthing = UTMNorthing;
			(*(deltaCell<T>*)((char*) _deltaAddrs[layerNum] + _numCells[layerNum]*sizeof(deltaCell<T>))).UTMEasting = UTMEasting;
			(*(deltaCell<T>*)((char*) _deltaAddrs[layerNum] + _numCells[layerNum]*sizeof(deltaCell<T>))).cellValue = cellValue;
      
      setDataUTM<int>(_deltaLayerID[layerNum], UTMNorthing, UTMEasting, _numCells[layerNum]);
      
      _numCells[layerNum]++;     

    } else {
      (*(deltaCell<T>*)((char*) _deltaAddrs[layerNum] + cellModified*sizeof(deltaCell<T>))).UTMNorthing = UTMNorthing;
      (*(deltaCell<T>*)((char*) _deltaAddrs[layerNum] + cellModified*sizeof(deltaCell<T>))).UTMEasting = UTMEasting;
      (*(deltaCell<T>*)((char*) _deltaAddrs[layerNum] + cellModified*sizeof(deltaCell<T>))).cellValue = cellValue;
    } 

// 		if(expandDeltaLock!=NULL)
// 			DGCunlockRWLock(expandDeltaLock);
  }
  return setDataUTM(layerNum, UTMNorthing, UTMEasting, cellValue);
}


template <class T>
int CMap::setDataWin(int layerNum, int winRow, int winCol, T cellValue) {
  getDataWin<T>(layerNum, winRow, winCol) = cellValue;

  return CM_OK;
}

template <class T>
void CMap::getDataRectangle(int layerNum, double UTMNorthing, double UTMEasting,
			    double cos_theta, double sin_theta, double radlat, double radlong, 
			    T* pF, double* pX, double* pY, int* numCells)
{
  NEcoord C1(cos_theta*radlong - sin_theta*radlat,
	     sin_theta*radlong + cos_theta*radlat);
  NEcoord C2(cos_theta*radlong + sin_theta*radlat,
	     sin_theta*radlong - cos_theta*radlat);

  NEcoord* pCornerS;
  NEcoord* pCornerW;

  if(fabs(C1.N) < fabs(C2.N))
    {
      if(C2.N > 0.0)
	C2 *= -1.0;

      if(C1.E > 0.0)
	C1 *= -1.0;

      pCornerS = &C2;
      pCornerW = &C1;
    }
  else
    {
      if(C1.N > 0.0)
	C1 *= -1.0;

      if(C2.E > 0.0)
	C2 *= -1.0;

      pCornerS = &C1;
      pCornerW = &C2;
    }

  int winrowS, winrowN, wincolE, wincolW;  // window coords of the cells at the corners of our rectangle
  int winrowM, wincolM;                    // window coords of the start of the memory

  Mem2Win(0, 0, &winrowM, &wincolM);

	UTM2Win(UTMNorthing + pCornerS->N, UTMEasting + pCornerW->E,
					&winrowS, &wincolW);
	UTM2Win(UTMNorthing - pCornerS->N, UTMEasting - pCornerW->E,
					&winrowN, &wincolE);

	int row, col;
	T* data;
	T* dataStartRow;
	double midCellN, midCellE;
	double midCellEStartRow;
	double x,y;
	int firstRow, firstCol;
	int lastRow,  lastCol;
	*numCells = 0;


	firstCol = wincolW;
	lastRow  = min(winrowM, winrowN+1);
	lastCol  = min(wincolM, wincolE+1);
	if(firstCol < lastCol)
	{
		firstRow = winrowS;
		if(firstRow < lastRow)
		{
			dataStartRow = &getDataWin<double>(layerNum, firstRow, firstCol);
			Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
			midCellN         += 0.5*_resRows - UTMNorthing;
			midCellEStartRow += 0.5*_resCols - UTMEasting;
			for(row = firstRow; row < lastRow; row++)
			{
				data     = dataStartRow;
				midCellE = midCellEStartRow;
				for(col = firstCol; col < lastCol; col++)
				{
					x = midCellN*cos_theta + midCellE*sin_theta;
					y = midCellN*sin_theta - midCellE*cos_theta;
					if(fabs(x) < radlong && fabs(y) < radlat)
					{
						pF[*numCells] = *data;
						pX[*numCells] = x;
						pY[*numCells] = y;
						(*numCells)++;
					}
					data++;
					midCellE += _resCols;
				}
				dataStartRow += _numCols;
				midCellN += _resRows;
			}
		}

		firstRow = max(winrowM, winrowS);
		if(firstRow <= winrowN)
		{
			dataStartRow = &getDataWin<double>(layerNum, firstRow, firstCol);
			Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
			midCellN         += 0.5*_resRows - UTMNorthing;
			midCellEStartRow += 0.5*_resCols - UTMEasting;
			for(row = firstRow; row <= winrowN; row++)
			{
				data = dataStartRow;
				midCellE = midCellEStartRow;
				for(col = firstCol; col < lastCol; col++)
				{
					x = midCellN*cos_theta + midCellE*sin_theta;
					y = midCellN*sin_theta - midCellE*cos_theta;
					if(fabs(x) < radlong && fabs(y) < radlat)
					{
						pF[*numCells] = *data;
						pX[*numCells] = x;
						pY[*numCells] = y;
						(*numCells)++;
					}
					data++;
					midCellE += _resCols;
				}
				dataStartRow += _numCols;
				midCellN += _resRows;
			}
		}
	}

	firstCol = max(wincolM, wincolW);
	if(firstCol <= wincolE)
	{
		firstRow = winrowS;
		if(firstRow < min(winrowM, winrowN+1))
		{
			dataStartRow = &getDataWin<double>(layerNum, firstRow, firstCol);
			Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
			midCellN         += 0.5*_resRows - UTMNorthing;
			midCellEStartRow += 0.5*_resCols - UTMEasting;
			for(row = firstRow; row < lastRow; row++)
			{
				data = dataStartRow;
				midCellE = midCellEStartRow;
				for(col = firstCol; col <= wincolE; col++)
				{
					x = midCellN*cos_theta + midCellE*sin_theta;
					y = midCellN*sin_theta - midCellE*cos_theta;
					if(fabs(x) < radlong && fabs(y) < radlat)
					{
						pF[*numCells] = *data;
						pX[*numCells] = x;
						pY[*numCells] = y;
						(*numCells)++;
					}
					data++;
					midCellE += _resCols;
				}
				dataStartRow += _numCols;
				midCellN += _resRows;
			}
		}

		firstRow = max(winrowM, winrowS);
		if(firstRow <= winrowN)
		{
			dataStartRow = &getDataWin<double>(layerNum, firstRow, firstCol);
			Win2UTM(firstRow, firstCol, &midCellN, &midCellEStartRow);
			midCellN         += 0.5*_resRows - UTMNorthing;
			midCellEStartRow += 0.5*_resCols - UTMEasting;
			for(row = firstRow; row <= winrowN; row++)
			{
				data = dataStartRow;
				midCellE = midCellEStartRow;
				for(col = firstCol; col <= wincolE; col++)
				{
					x = midCellN*cos_theta + midCellE*sin_theta;
					y = midCellN*sin_theta - midCellE*cos_theta;
					if(fabs(x) < radlong && fabs(y) < radlat)
					{
						pF[*numCells] = *data;
						pX[*numCells] = x;
						pY[*numCells] = y;
						(*numCells)++;
					}
					data++;
					midCellE += _resCols;
				}
				dataStartRow += _numCols;
				midCellN += _resRows;
			}
		}
	}
}

template <class T>
int CMap::addLayer(char* configFilename, bool useDeltas) {
  ifstream configFile;
  string fieldName;
  T noDataVal;
  T outsideMapVal;
  
  configFile.open(configFilename,ios::in);
  
  if(!configFile) {
    cout << __FILE__ <<"[" << __LINE__ << "]: " << "Unable to open file '"
	 << configFilename << "' - please check that it exists" << endl;
    exit(1);
    }
  
  while(getline(configFile, fieldName, ':')) {
    if(fieldName == "NoDataVal") {
      configFile >> noDataVal;
    } else if(fieldName == "OutsideMapVal") {
      configFile >> outsideMapVal;
    } else if(fieldName == "Label") {
      //Do nothing
    } else {
      cout << __FILE__ << "[" << __LINE__ << "]: " 
	   << "While parsing config file " << configFilename 
	   << ", unknown parameter '" << fieldName << "'" << endl;
    }
    getline(configFile, fieldName);
  }  

  configFile.close();

  return addLayer<T>(noDataVal, outsideMapVal, useDeltas);
}

template <class T>
int CMap::interpolateFromOtherMap(int destinationLayer, CMap* sourceMap, int sourceLayer) {
	//cout << "We are running it" << endl;

	double sourceResRows = sourceMap->getResRows();
	double sourceResCols = sourceMap->getResCols();

	if(sourceResRows > getResRows() ||
		 sourceResCols > getResCols()) {
		return CM_UNKNOWN_ERROR;
	}
	double sourceUTMNorthing;
	double sourceUTMEasting;
	int destRow;
	int destCol;
	T sourceVal;

	double destUTMNorthing, destUTMEasting;

	int sourceRowStart, sourceRowEnd;
	int sourceColStart, sourceColEnd;


	//cout << "We got " << sourceMap->_numCells[sourceLayer] << " cells yo" << endl;
	//For every cell that's changed in the source map
	for(int i=0; i<sourceMap->_numCells[sourceLayer]; i++) {
		sourceUTMNorthing =  (*(deltaCell<T>*)((char*) sourceMap->_deltaAddrs[sourceLayer] + i*sizeof(deltaCell<T>))).UTMNorthing;
		sourceUTMEasting = (*(deltaCell<T>*)((char*) sourceMap->_deltaAddrs[sourceLayer] + i*sizeof(deltaCell<T>))).UTMEasting;
		sourceVal = (*(deltaCell<T>*)((char*) sourceMap->_deltaAddrs[sourceLayer] + i*sizeof(deltaCell<T>))).cellValue;
		
		//Find the appropriate cell in the destination (current) map
		UTM2Win(sourceUTMNorthing, sourceUTMEasting, &destRow, &destCol);
		Win2UTM(destRow, destCol, &destUTMNorthing, &destUTMEasting);

		sourceMap->UTM2Win(destUTMNorthing, destUTMEasting, &sourceRowStart, &sourceColStart);
		sourceMap->UTM2Win(destUTMNorthing+getResRows(), destUTMEasting + getResCols(),
											 &sourceRowEnd, &sourceColEnd);

		/*
		testDestRow = destRow;
		testDestCol = destCol;
		sourceRowEnd = sourceRowStart;
		sourceColEnd = sourceColStart;

		sourceMap->UTM2Win(destUTMNorthing, destUTMEasting, &sourceRowStart, &sourceColStart);
		while(destRow==testDestRow) {
			//while the destrow is the testrow we got from convering the sourcerow to the testrow
			sourceRowEnd++;
			//convert the sourceRowEnd to the testDestRow
			sourceMap->Win2UTM(sourceRowEnd, sourceColEnd, &tempNorthing, &tempEasting);
			UTM2Win(tempNorthing, tempEasting, &testDestRow, &testDestCol);
		}

		while(destCol==testDestCol) {
			//while the destrow is the testrow we got from convering the sourcerow to the testrow
			sourceColEnd++;
			//convert the sourceRowEnd to the testDestRow
			sourceMap->Win2UTM(sourceRowEnd, sourceColEnd, &tempNorthing, &tempEasting);
			UTM2Win(tempNorthing, tempEasting, &testDestRow, &testDestCol);
		}
		*/
		double sourceNorthingStart, sourceNorthingEnd;
		double sourceEastingStart, sourceEastingEnd;
		sourceMap->Win2UTM(sourceRowStart, sourceColStart,
											 &sourceNorthingStart, &sourceEastingStart);
		sourceMap->Win2UTM(sourceRowEnd, sourceColEnd,
											 &sourceNorthingEnd, &sourceEastingEnd);

		T totalValue = 0;
		int numCells = 0;

		for(int r=sourceRowStart; r<sourceRowEnd; r++) {
			for(int c=sourceColStart; c<sourceColEnd; c++) {
				totalValue+=sourceMap->getDataWin<T>(sourceLayer, r, c);
				numCells++;
			}
		}
		
		setDataWin_Delta<T>(destinationLayer, destRow, destCol,
												totalValue/((double)numCells));

				/*		
		cout << "our dest row is " << setprecision(10)
				 << destRow << " (" << destUTMNorthing << "), maps from rows "
				 << sourceRowStart << " (" << sourceNorthingStart
				 << ") to " << sourceRowEnd << " ("
				 << sourceNorthingEnd << ")" << endl;
				*/

	}


	return CM_OK;
}


#endif //__CMAP_HH__
