#ifndef __CMAP_DRAW_POLYGON_HH__
#define __CMAP_DRAW_POLYGON_HH__

#include <stdint.h>
#include <iostream>
#include <cassert>
#include <vector>
#include "CMap.hh"

using namespace std;

/* Ok guys, this is some very old code ('99 I think) that used to run under dos
 * in the times when there were no accelerated graphic cards, to draw shaded
 * polygons on the screen.
 * Now, I know this code is ugly, but it works and is fast, and rather well tested.
 * If you ever need to hack it, and can't understand what it does, send me an email,
 * to datamino@caltech.edu or asloxx@gmail.com.
 * -- Daniele Tamino
 */


template <class T>
struct gridbuffer
{
    T * buf;
    int width;
    int height;

    gridbuffer() : buf(NULL), width(0), height(0)
    { };

    T* operator[](int y) { return buf + width * y; }
};

// TODO: this code used to run on old computers, so it uses fixed point math to
// speed up the calculus. Modern CPUs don't have this bottleneck on floating
// point anymore, so may be worth changing this to use floating point!
static const int FRAC = 10;
typedef int32_t fixed_t;

template<class T>
struct internal_t
{
    typedef T type; // default, just use T
};

// for integers, do internal math using float (I think nobody uses this at the moment)
template<> struct internal_t<int8_t>   { typedef float type; };
template<> struct internal_t<uint8_t>  { typedef float type; };
template<> struct internal_t<int16_t>  { typedef float type; };
template<> struct internal_t<uint16_t> { typedef float type; };
template<> struct internal_t<int32_t>  { typedef double type; };
template<> struct internal_t<uint32_t> { typedef double type; };
template<> struct internal_t<int64_t>  { typedef long double type; };
template<> struct internal_t<uint64_t> { typedef long double type; };

/** Draws a scanline (horizontal straight line). The length of the scanline once
 * drawn will be (x2 - x1) cells, including the starting poing and excluding the
 * ending one. If x1 > x2, the endpoints will be exchanged.
 * Note: this works well when the T type is a floating point number.
 * For integers, it works using a float internally.
 * @param b Buffer to draw on.
 * @param x1 starting point on the row. It's NOT required that x1 < x2.
 * @param x2 ending point on the row.
 * @param val1 Desired value of cell at x1.
 * @param val2 Desired value of cell at x2.
 * @param row The row where all the drawing should happen.
 */
template <class T>
static void draw_scanline(gridbuffer<T>* b, int x1, int x2, T val1, T val2, int row)
{
    typedef typename internal_t<T>::type InternT;
    InternT curVal;
    InternT incVal;
  
    // we want x2 > x1
    if (x2 < x1) {
        swap(x2, x1);
        swap(val1, val2);
    }
    if (x1 > (b->width - 1) || x2 < 0) // completely out of bounds
        return;
    if (x2 == x1)
        return;

    curVal = InternT(val1);
    incVal = InternT(val2 - val1) / (x2 - x1);

    if (x1 < 0) {
        curVal += incVal*(-x1);
        x1 = 0;
    }
    if (x2 > (b->width - 1))
        x2 = b->width - 1;
    if(x2 == x1)
        return;

    T* adr = (*b)[row] + x1;

    for(int x = x1; x < x2; x++) {
        *adr = T(curVal);
        adr++;
        curVal += incVal;
    }

    // TODO: add the region drawn in the delta list here
}

/** Given a line specified by two verticies, and a mininum and maximum value
 * of y, updates the verticies coordinates to represent the portion of the line
 * within the specified boundaries.
 * @param max Mamimum value of y
 * @param min Minimum value of y
 * @param x1 Pointer to the x coordinate of the first vertex
 * @param x2 Pointer to the x coordinate of the second vertex
 * @param y1 Pointer to the y coordinate of the first vertex
 * @param y2 Pointer to the y coordinate of the second vertex
 * @return the distance of the top vertex with the 'y = max' line, if
 * the line was clipped, or 0 otherwise. This is useful in draw_polygon.
 */
int clip_line_y(int max, int min, int *x1, int *y1, int *x2, int *y2);

struct gvertex
{
    int x, y; // cell coordinates
    int c;
};

struct gpoly
{
    int n_vert;
    gvertex *v;
};


/**
 * Draw a polygon
 */
template <class T>
static void draw_polygon(gridbuffer<T>* b,
                         const vector<int>& vertX,
                         const vector<int>& vertY,
                         const vector<T>& vertVal)
{
    int nVert = int(vertX.size());
    assert(int(vertY.size()) == nVert);
    assert(int(vertVal.size()) == nVert);

    int miny=INT_MAX, maxy=INT_MIN, cv1=0, cv2=0;
    int dist=0;
    int x, y, x1, y1, x2, y2, xx, yy; // don't ask me what all these means, I don't remember ;-) Daniele
    T c, c1, c2; // int c, c1, c2;
    //float len1, len2;
    fixed_t len1, len2;
    int v_prev, v_next; //tmp vars

    //float cur_x_1, cur_x_2;
    fixed_t cur_x_1, cur_x_2;
    T     cur_c_1, cur_c_2;
    //float inc_x_1, inc_x_2;
    fixed_t inc_x_1, inc_x_2;
    T     inc_c_1, inc_c_2;

    // search for the vertex with the highest y value
    for (int i = 0; i < nVert; i++) {
        if (vertY[i] > maxy) {
            cv1 = i;
            maxy = vertY[i];
        }
        if (vertY[i] < miny) {
            miny = vertY[i];
        }
    }
    cv2 = cv1;
    if(maxy < 0) return; // completely out of boundary
    //if(miny < 0) miny = 0; //bugfix (chance of infinite loop otherwise)

    x = vertX[cv1];
    y = vertY[cv1];
    c = vertVal[cv1];
    xx = x;
    yy = y;
    v_prev = (cv1-1 < 0) ? (nVert - 1) : (cv1 - 1);
    v_next = (cv2 + 1 >= nVert) ? 0 : (cv2 + 1);
    x1 = vertX[v_prev];
    y1 = vertY[v_prev];
    x2 = vertX[v_next];
    y2 = vertY[v_next];
    c1 = vertVal[v_prev];
    c2 = vertVal[v_next];

    len1 = (y - y1 == 0) ? 1 : (y - y1);
    len2 = (y - y2 == 0) ? 1 : (y - y2);

    inc_x_1 = /* Dx / Dy */ ((fixed_t)(x1 - x)<<FRAC) / len1;
    inc_x_2 = /* Dx / Dy */ ((fixed_t)(x2 -xx)<<FRAC) / len2;
    //inc_c_1 = /* Dc / Dy */ ((fixed_t)(c1 - c)<<FRAC) / len1;
    //inc_c_2 = /* Dc / Dy */ ((fixed_t)(c2 - c)<<FRAC) / len2;
    inc_c_1 = /* Dc / Dy */ (c1 - c) / len1;
    inc_c_2 = /* Dc / Dy */ (c2 - c) / len2;

/*
    // cut initial edges
    dist = clip_line_y(b->height-1, 0, &x1, &y1, &x, &y);
    //cur_c_1 = ((fixed_t)c<<FRAC) + inc_c_1*dist;
    cur_c_1 = c + inc_c_1 * dist;

    dist = clip_line_y(b->height-1, 0, &x2, &y2, &xx, &yy);
    //cur_c_2 = ((fixed_t)c<<FRAC) + inc_c_2*dist;
    cur_c_2 = c + inc_c_2 * dist;
*/
    cur_c_1 = c;
    cur_c_2 = c;

    cur_x_1 = ((fixed_t)x<<FRAC);
    cur_x_2 = ((fixed_t)xx<<FRAC);

    while(y >= miny && y < b->height) {
        if(y < y1) { // new right edge
            cv1 = (cv1 - 1 < 0) ? (nVert - 1) : (cv1 - 1);
            x = vertX[cv1];
            c = vertVal[cv1];
            v_prev = (cv1 - 1 < 0) ? (nVert - 1) : (cv1 - 1);
            x1 = vertX[v_prev];
            y1 = vertY[v_prev];
            c1 = vertVal[v_prev];
            len1 = (y - y1 == 0) ? 1 : (y - y1);
            inc_x_1 = /* Dx / Dy */ ((fixed_t)(x1 - x)<<FRAC) / len1;
            //inc_c_1 = /* Dc / Dy */ ((fixed_t)(c1 - c)<<FRAC) / len1;
            //cur_c_1 = ((fixed_t)c<<FRAC);
            inc_c_1 = /* Dc / Dy */ (c1 - c) / len1;
            cur_c_1 = c;
            cur_x_1 = ((fixed_t)x<<FRAC);
        }
        if(y < y2) { // new left edge
            cv2 = (cv2 + 1 >=nVert) ? 0 : (cv2 + 1);
            xx = vertX[cv2];
            c = vertVal[cv2];
            v_next = (cv2 + 1 >= nVert) ? 0 : (cv2 + 1);
            x2 = vertX[v_next];
            y2 = vertY[v_next];
            c2 = vertVal[v_next];
            len2 = y - y2 == 0 ? 1 : y - y2;
            inc_x_2 = /* Dx / Dy */ ((fixed_t)(x2 - xx)<<FRAC) / len2;
            //inc_c_2 = /* Dc / Dy */ ((fixed_t)(c2 - c)<<FRAC) / len2;
            //cur_c_2 = ((fixed_t)c<<FRAC);
            inc_c_2 = /* Dc / Dy */ (c2 - c) / len2;
            cur_c_2 = c;
            cur_x_2 = ((fixed_t)xx<<FRAC);
        }

        if(y < b->height) {
            draw_scanline(b, cur_x_1>>FRAC, cur_x_2>>FRAC, cur_c_1, cur_c_2, y);
        }
        cur_x_1 += inc_x_1;
        cur_c_1 += inc_c_1;
        cur_x_2 += inc_x_2;
        cur_c_2 += inc_c_2;
        y--;
    } //  end of for (each value of y)
}


template<class T>
static inline void split_polygon_add_vert(int x, int y, T v, int yThres, int part,
                                          vector<int>& xv, vector<int>& yv, vector<T>& val)
{
    if (part == 0)
        assert(y > yThres);
    else
        assert(y <= yThres);
        
    if (xv.size() == 0 || !(x == xv.back() && y == yv.back()))
    {
        xv.push_back(x);
        yv.push_back(y);
        val.push_back(v);
    }
}

template<class T>
static inline void split_polygon_y(const vector<int>& xvIn, const vector<int>& yvIn, const vector<T>& valIn,
                                   vector<int> xv[2], vector<int> yv[2], vector<T> val[2], int yThres)
{
    unsigned int i;
    xv[0].clear();
    yv[0].clear();
    val[0].clear();
    xv[1].clear();
    yv[1].clear();
    val[1].clear();
    int part = yvIn.back() > yThres ? 0 : 1;
    for (i = 0; i < xvIn.size(); i++)
    {
        int p = yvIn[i] > yThres ? 0 : 1;
        if (p != part)
        {
            int prev = (i != 0) ? (i - 1) : (xvIn.size()-1);
            // split segment in two
            int q = yThres - yvIn[prev];
            int len = yvIn[i] - yvIn[prev];
            int xs1 = xvIn[prev] + (q - 1) * (xvIn[i] - xvIn[prev]) / len;
            int xs2 = xvIn[prev] + q * (xvIn[i] - xvIn[prev]) / len;
            int ys1 = yThres;
            int ys2 = yThres + 1;
            T vs1 = valIn[prev] + (q - 1) * (valIn[i] - valIn[prev]) / len;
            T vs2 = valIn[prev] + q * (valIn[i] - valIn[prev]) / len;
            if (part == 0) {
                swap(xs1, xs2);
                swap(ys1, ys2);
                swap(vs1, vs2);
            }
            split_polygon_add_vert(xs1, ys1, vs1, yThres, part, xv[part], yv[part], val[part]);
            split_polygon_add_vert(xs2, ys2, vs2, yThres, p, xv[p], yv[p], val[p]);
            part = p;
        }
        split_polygon_add_vert(xvIn[i], yvIn[i], valIn[i], yThres, part, xv[part], yv[part], val[part]);
    }
}

template<class T>
static inline void split_polygon_x(const vector<int>& xvIn, const vector<int>& yvIn, const vector<T>& valIn,
                                   vector<int> xv[2], vector<int> yv[2], vector<T> val[2], int xThres)
{
    // invert x and y
    split_polygon_y(yvIn, xvIn, valIn, yv, xv, val, xThres);
}


/** Draws a convex polygon with the specified verticies (fills all the cells within and on the
 * edges of the polygon).
 * @param layerNum The layer where to draw the polygon.
 * @param x vector of the x coordinates of the verticies (clockwise or ccw, doesn't matter as long
 * as they are in some order).
 * @param y vector of the y coordinates of the verticies (cw or ccw, same as x).
 * @param v value of each vertex. This values will be interpolated to fill the polygon linearly
 * (like Gouraud shading).
 */
template<class T>
void CMap::drawPolygonWin(int layerNum, const vector<int>& xOrig,
                          const vector<int>& yOrig,
                          const vector<T>& valOrig, bool useDelta)
{
    if (xOrig.size() == 0)
        return;

    vector<int> xv[4];
    vector<int> yv[4];
    vector<T> val[4];
    for (int i = 0; i < 4; i++)
    {
        xv[i].reserve(xOrig.size());
        yv[i].reserve(yOrig.size());
        val[i].reserve(valOrig.size());
    }

    // get the buffer
    gridbuffer<T> buf;
    buf.width = _numCols;
    buf.height = _numRows;
    buf.buf = reinterpret_cast<T*>(_layerAddrs[layerNum]->getDataAddr());

#if !defined(NDEBUG) && 0
    cerr << __FILE__ << ":" << __LINE__ << ": drawing a polygon:" << endl;
    for (unsigned int i = 0; i < xOrig.size(); i++)
    {
        cerr << "(x=" << xOrig[i] << ", y=" << yOrig[i] << ", value=" << val[i] << ")" << endl;
    }
#endif

    cerr << "mem bottom left ROW = " << _windowBottomLeftMemRow << endl;
    cerr << "mem bottom left COL = " << _windowBottomLeftMemCol << endl;
    cerr << "Splitting polygon along y axis at " << _numRows - _windowBottomLeftMemRow - 1<< endl;
    split_polygon_y(xOrig, yOrig, valOrig, xv, yv, val, _numRows - _windowBottomLeftMemRow - 1);

    for (int k = 0; k < 2; k++)
    {
        if (xv[k].size() > 0) {
            cerr << "Splitting polygon " << k << " along x axis at " << _numCols - _windowBottomLeftMemCol - 1 << endl;
            split_polygon_x(xv[k], yv[k], val[k], xv + 2, yv + 2, val + 2, _numCols - _windowBottomLeftMemCol - 1);
            
            for (int i = 2; i < 4; i++) {
                if (xv[i].size() > 0) {
                    cerr << "Drawing polygon " << k << ", " << i << " with " << xv[i].size() << " points" << endl;
                    for (unsigned int j = 0; j < xv[i].size(); j++) {
                        int winRow, winCol;
                        cerr << "(" << xv[i][j] << ", " << yv[i][j] << ")->";
                        Win2Mem(yv[i][j], xv[i][j], &winRow, &winCol);
                        xv[i][j] = winCol;
                        yv[i][j] = winRow;
                        cerr << "(" << xv[i][j] << ", " << yv[i][j] << ") ";
                    }
                    cerr << endl;
                    draw_polygon(&buf, xv[i], yv[i], val[i]);
                }
            }
        }
    }

    // add deltas: just add all the cells within the bounding box, for now ... ;-)
    if (useDelta && _deltaAddrs[layerNum] != NULL) {
        int minX = INT_MAX, maxX = INT_MIN;
        int minY = INT_MAX, maxY = INT_MIN;
        for (unsigned int i = 0; i < xOrig.size(); i++) {
            if (xOrig[i] < minX)
                minX = xOrig[i];
            if (xOrig[i] > maxX)
                maxX = xOrig[i];
            if (yOrig[i] < minY)
                minX = xOrig[i];
            if (yOrig[i] > maxY)
                maxX = xOrig[i];
        }
	int r, c;
	for(r = minY; r < maxY ; r++){
            for(c = minX; c < maxX ; c++){
                setDataWin_Delta<T>(layerNum, r, c, getDataWin<T>(layerNum, r, c));
            }
	}
    } // end of delta stuff
}

template<class T>
void CMap::drawPolygonUTM(int layerNum, const vector<NEcoord>& p, const vector<T>& v, bool useDelta)
{
    vector<int> x(p.size());
    vector<int> y(p.size());
    int winRow, winCol;
	
    for (unsigned int i = 0; i < p.size(); i++)
    {
        //printf("drawPolygon -- p[%d].N: %f  p[%d].E: %f\n", i, p[i].N, i, p[i].E);	
	winRow = (int)floor(p[i].N/_resRows+1e-6) - _windowBottomLeftUTMNorthingRowResMultiple;
	winCol = (int)floor(p[i].E/_resCols+1e-6) - _windowBottomLeftUTMEastingColResMultiple;
	//Win2Mem(winRow, winCol, &y[i], &x[i]);
	y[i] = winRow;
	x[i] = winCol;

    	//printf("drawPolygon -- x[%d]: %d  y[%d]: %d\n", i, x[i], i, y[i]);
    }
    drawPolygonWin(layerNum, x, y, v, useDelta);
}

template<class T>
void CMap::drawPolygonUTM(int layerNum, const vector<NEcoord>& p, T v, bool useDelta)
{
    vector<T> values(p.size(), v);
    drawPolygonUTM(layerNum, p, values, useDelta);
}

#endif
