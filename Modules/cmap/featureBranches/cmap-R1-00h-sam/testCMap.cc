/*!
 * \file testCMap.cc
 * \brief Simple test program for CMap/CMapPlus
 *
 * \author Unkown
 *
 * This program is used to test out the CMap and CMapPlus classes.
 * The tests that are run can be specified via the command line.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

#include "skynet/skynet.hh"
#include "dgcutils/cfgfile.h"
#include "cmap/MapdeltaTalker.hh"
#include "interfaces/sn_types.h"

#include "CMapPlus.hh"
//#include "CMapDelta.hh"
#include "cmdline.h"

/* Function to fill a map with random data */
void fillMapRandom_Double(CMap* theMap, int layerNum, int numPoints) {
  int numRows = theMap->getNumRows();
  int numCols = theMap->getNumCols();
  int row, col;
  double tempVal;

  for(int i=0; i<numPoints; i++) {
    row = (int)(((double)rand())/((double)RAND_MAX)*((double)numRows));
    col = (int)(((double)rand())/((double)RAND_MAX)*((double)numCols));
    tempVal = ((double)rand())/((double)RAND_MAX)*1.0;
    theMap->setDataWin_Delta<double>(layerNum, row, col, tempVal);
  }
}

void fillRandomPolygon(CMap* m, int layerNum, int numPoly = 1)
{
    double minN = m->getWindowBottomLeftUTMNorthing();
    double minE = m->getWindowBottomLeftUTMEasting();
    double maxN = m->getWindowTopRightUTMNorthing();
    double maxE = m->getWindowTopRightUTMEasting();

    // generate a triangle, because it's always convex
    // TODO: try with more complex convex polygons!
    vector<NEcoord> p(3);
    vector<double> values(p.size());
    for (int k = 0; k < numPoly; k++)
    {
        cout << "Generating random polygon " << k << ": " << endl;
        for (unsigned int i = 0; i < p.size(); i++)
        {
            p[i].N = (rand()/double(RAND_MAX)) * (maxN - minN) + minN;
            p[i].E = (rand()/double(RAND_MAX)) * (maxE - minE) + minE;
            values[i] = rand()/double(RAND_MAX);
            cout << "(N=" << p[i].N << ", E=" << p[i].E << ", value=" << values[i]
                 << ")" <<endl;
        }
        m->drawPolygonUTM<double>(layerNum, p, values);
    }
}

/* Class for testing out talker */
class TestTalker : public CMapdeltaTalker
{
public:
  TestTalker(int snkey) : CSkynetContainer(MODtrafficplanner, snkey) {}
  ~TestTalker() {};

  void sendDelta(CMapPlus &map, int layer)
  {
    cerr << "testCMap: sending delta of type " << SNtplannerStaticCostMap 
	 << endl;
    int mapdeltaSocket = m_skynet.get_send_sock(SNtplannerStaticCostMap);

    unsigned long long timestamp; DGCgettime(timestamp);
    CDeltaList *deltaList = map.serializeDelta<double>(layer, timestamp);
    cerr << "testCMap: sending " << deltaList->numDeltas << " deltas" << endl;
    SendMapdelta(mapdeltaSocket, deltaList);
  };
};

int main(int argc, char **argv) {
  /* Set the defaults that we use if not specified */
  int numRows = 500;	  int numCols = 500;
  double resRows=0.4;	  double  resCols=0.4;
  double Northing = 0.0;  double Easting = 0.0;
  
  /* Process command line options */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  /* Retrieve the skynet key */
  int snkey = skynet_findkey(argc, argv);
  cerr << "Constructing skynet with KEY = " << snkey << endl;

  /*
   * Create some maps that are used for the remaining tests
   *
   */
  cerr << "Creating maps" << endl;

  // initialize rand() with an arbitrary number
  int seed = time(0);
  srand(seed);
  // to repeat the result of a particular run, replace time(0)
  // with the number printed on screen by the line below
  cerr << "Random number generator seed: " << seed << endl;

  CMapPlus myMap;
  if (cmdline.cmap_config_given) {
    char *filename = dgcFindConfigFile(cmdline.cmap_config_arg, "cmap");
    cerr << "Creating my map from config file " << filename << endl;
    myMap.initMap(filename);
  } else {
    myMap.initMap(Northing, Easting, numRows, numCols, resRows, resCols, 0);
  }
  int layerA = myMap.addLayer<double>(0.0, -32.0, 1);

  CMapPlus otherMap;
  otherMap.initMap(Northing, Easting, numRows, numCols, resRows, resCols, 0);
  int layerB = otherMap.addLayer<double>(0.0, -32.0);

  if (cmdline.run_all_flag || cmdline.run_basic_flag) {
    cerr << "Testing basic CMap/CMapPlus functionality" << endl;

    fprintf(stderr, "%s [%d]: Filling randomly...\n", __FILE__, __LINE__);
    myMap.updateVehicleLoc(1,1);
    fillMapRandom_Double(&myMap, layerA, numRows*numCols);

    fprintf(stderr, "%s [%d]: Saving before...\n", __FILE__, __LINE__);
    myMap.saveLayer<double>(layerA, "before");

    fprintf(stderr, "%s [%d]: Copying...\n", __FILE__, __LINE__);
    otherMap.copyLayer<double>(&myMap, layerA, layerB, 0);

    fprintf(stderr, "%s [%d]: Saving after...\n", __FILE__, __LINE__);
    otherMap.saveLayer<double>(layerB, "after");
  }

  if (cmdline.run_all_flag || cmdline.run_sender_flag) {
    cerr << "Sending map deltas" << endl;

    // Set up a talker and send out map deltas
    TestTalker sender(snkey);

    /* Generate a pattern in the gradient */
    for (int i = 0; i < myMap.getNumRows(); ++i) {
      for (int j = 0; j < myMap.getNumCols(); ++j) {
	double cost = fabs(sin(((double) i)/100) * cos(2.0*((double) j)/100));
	myMap.setDataWin_Delta<double>(layerA, i, j, cost);
      }
    }
    sender.sendDelta(myMap, layerA);
  }

  if (cmdline.run_all_flag || cmdline.run_gradient_flag) {
    cerr << "Testing gradient functionality" << endl;

    double gr[2], gNorth, gEast;
    cout << "Insert coordinates to compute gradient,  N: ";
    cin >> gNorth;

    cout <<"Insert coordinates to compute gradient,  E: ";
    cin >> gEast;

    myMap.getUTMGradient(layerA,gNorth,gEast,gr);
    cout << "Gradient: " << gr[0] << " " << gr [1]<< endl;
  }

  if (cmdline.run_all_flag || cmdline.run_polygon_flag) {
      // Set up a talker and send out map deltas
      TestTalker sender(snkey);

      //myMap.clearLayer(layerA);
      fillRandomPolygon(&myMap, layerA);
      sender.sendDelta(myMap, layerA);
      cout << "waiting 3 sec, then sending another delta" << endl;
      sleep(3);
      fillRandomPolygon(&myMap, layerA);
      sender.sendDelta(myMap, layerA);
  }

  return 0;
}
