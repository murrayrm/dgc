/*
	DGC_AlgebraicGeometry.cc
    
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
    
	Created: 12/11/2006
*/

#include <math.h>
#include "DGC_AlgebraicGeometry.hh"

CDGCAlgebraicGeometry::CDGCAlgebraicGeometry(void)
{
    m_DebugFlag     = false;
    m_NormalizeFlag = true;
    m_TranslateFlag = true;
}

CDGCAlgebraicGeometry::~CDGCAlgebraicGeometry(void)
{

}

DGCErrorType CDGCAlgebraicGeometry::VertexEnumeration(CDGCPolytope* Polytope)
{
    /* Initialize cdd before performing any computations */  
    dd_set_global_constants(); 

    dd_ErrorType ErrorFlag;
    DGCErrorType ErrorCode; 

    dd_rowrange i;
    dd_colrange j;
    
    dd_rowrange k;
    dd_colrange ell;

    mytype value;
    dd_init(value);
    
    dd_rowrange NofRows = Polytope->m_NofRows;
    dd_colrange NofCols = Polytope->m_NofDimensions + 1;
    
    dd_MatrixPtr Matrix    = dd_CreateMatrix(NofRows, NofCols); 
    Matrix->representation = dd_Inequality; /* Input given in facet form */
    Matrix->numbtype       = dd_Real;
    
    double* LargestValue = new double[NofRows];
    double  Max = 0.0; 
    double* btrans = NULL;
    
    if(m_TranslateFlag == true)
    {
        btrans = new double[NofRows];
        for(i=0; i<NofRows; i++)
        {
            btrans[i] = 0.0; 
            for(j=1; j<NofCols; j++)
            {
                btrans[i] = btrans[i] + Polytope->m_A[i][j-1]*Polytope->m_Points[j-1][0];
            }                        
            btrans[i] = Polytope->m_b[i] - btrans[i];
        }      
    }
    
    if(m_NormalizeFlag == true)
    {
        if(m_TranslateFlag == true)
        {
            for(k = 0; k < NofRows; k++)
            {
                Max = fabs(btrans[k]);
                for(ell = 1; ell< NofCols; ell++)
                { 
                    if(fabs(Polytope->m_A[k][ell-1]) > Max)
                    {
                        Max = fabs(Polytope->m_A[k][ell-1]); 
                    }
                }
                LargestValue[k] = Max;
            }        
        }
        else
        { 
            for(k = 0; k < NofRows; k++)
            {
                Max = fabs(Polytope->m_b[k]);
                for(ell = 1; ell< NofCols; ell++)
                { 
                    if(fabs(Polytope->m_A[k][ell-1]) > Max)
                    {
                        Max = fabs(Polytope->m_A[k][ell-1]); 
                    }
                }
                LargestValue[k] = Max;
            }
        }
    }
    else
    {
        for(k = 0; k < NofRows; k++)
        {
            LargestValue[k] = 1.0;
        }
    }
    
    for(k = 0; k < NofRows; k++)
    {
        for(ell = 0; ell< NofCols; ell++)
        { 
            if(ell == 0)
            {
                if(m_TranslateFlag == true)
                {
                    dd_set_d(value, btrans[k]/LargestValue[k]);                
                }
                else
                {
                    dd_set_d(value, Polytope->m_b[k]/LargestValue[k]);
                }
            }
            else
            {
                dd_set_d(value, -Polytope->m_A[k][ell-1]/LargestValue[k]);
            }
            dd_set(Matrix->matrix[k][ell], value);
        }
    }
 
    if(m_DebugFlag == true)
    {
       dd_WriteMatrix(stdout, Matrix);  
    }   
             
    /* Compute the vertex form */
    dd_PolyhedraPtr Polyhedra = dd_DDMatrix2Poly(Matrix, &ErrorFlag); 
 
    if(m_TranslateFlag == true)
    {
        delete[] btrans;
    }
    
    if(ErrorFlag == dd_NoError)
    {
        dd_MatrixPtr G = dd_CopyGenerators(Polyhedra);
        if(G == NULL)
        {
           exit(1); 
        }
        
        if(m_DebugFlag == true)
        {
           dd_WriteMatrix(stdout, G);  
        }  
        
        NofRows = G->rowsize;
        NofCols = G->colsize;
        
        int NofDimensions = (int)(NofCols-1);    
        int NofVertices   = (int) NofRows;

        ErrorCode = dgc_NoError;
        for(k = 0; k < NofRows; k++)
        {
            if(dd_get_d(G->matrix[k][0]) == 0)
            {
               ErrorCode = dgc_HasRays; /* Rays have been found */
               break; 
            }
        } 
        
        if(ErrorCode == dgc_NoError)
        {
            int n = 0;
            double** Vertices = new double*[NofDimensions];
            for(n = 0; n < NofDimensions; n++)
            {
                Vertices[n] = new double[NofVertices];
            } 
            
            if(m_TranslateFlag == true)
            {
                for(k = 0; k < NofRows; k++)
                {
                    for(ell = 1; ell< NofCols; ell++)
                    {
                        Vertices[ell-1][k]  = dd_get_d(G->matrix[k][ell]) + Polytope->m_Points[ell-1][0];
                    }
                }
            }
            else
            {
                for(k = 0; k < NofRows; k++)
                {
                    for(ell = 1; ell< NofCols; ell++)
                    {
                        Vertices[ell-1][k]  = dd_get_d(G->matrix[k][ell]);
                    }
                }
            }        
            
            Polytope->setVertices(&NofDimensions, &NofVertices, (const double** const) Vertices);  
            
            dd_bigrange i, j;
            long elem;	
                
            dd_SetFamilyPtr GA = dd_CopyAdjacency(Polyhedra);
            if(GA == NULL)
            {
               exit(1);
            }
            
            int*  NofAdjVertices  = new int [GA->famsize];
            for(i = 0; i < GA->famsize; i++) 
            {
                NofAdjVertices[i]  = set_card(GA->set[i]);   
                if(NofAdjVertices[i] != NofDimensions)
                {
                   ErrorCode = dgc_ImproperAdjacency;
                   break; 
                }                         
            }            
            
            if(ErrorCode == dgc_NoError)
            {
                int*  VerticesIndeces = new int [GA->famsize];
                int** AdjVertices     = new int*[GA->famsize];
                for(i = 0; i < GA->famsize; i++) 
                {
                    VerticesIndeces[i] = i;
                    AdjVertices[i]     = new int[NofAdjVertices[i]];
                    
                    j = 0;  
                    for(elem = 1; elem <= GA->set[i][0]; elem++)
                    {
                        if(set_member(elem, GA->set[i]))
                        {
                            AdjVertices[i][j] = elem-1;
                            j = j + 1;
                        }
                    }
                }
                
                DGCDirectionType directionType;
                
                int* CounterClockwiseIndeces = new int[GA->famsize];
                CounterClockwiseIndeces[0] = VerticesIndeces[0];
                CounterClockwiseIndeces[1] = AdjVertices[0][1]; 
                for(i = 1; i < GA->famsize-1; i++) 
                {
                    for(j = 0; j < NofAdjVertices[CounterClockwiseIndeces[i]]; j++)
                    {
                        if(AdjVertices[CounterClockwiseIndeces[i]][j] != CounterClockwiseIndeces[i-1])
                        {
                            CounterClockwiseIndeces[i+1] = AdjVertices[CounterClockwiseIndeces[i]][j];
                        } 
                    }
                }    
                directionType = dgc_CounterClockwise;
                Polytope->setIndeces(&directionType, &NofVertices, CounterClockwiseIndeces);
                
                int* ClockwiseIndeces = new int[GA->famsize];
                ClockwiseIndeces[0] = VerticesIndeces[0];
                ClockwiseIndeces[1] = AdjVertices[0][0]; 
                for(i = 1; i < GA->famsize-1; i++) 
                {
                    for(j = 0; j < NofAdjVertices[ClockwiseIndeces[i]]; j++)
                    {
                        if(AdjVertices[ClockwiseIndeces[i]][j] != ClockwiseIndeces[i-1])
                        {
                            ClockwiseIndeces[i+1] = AdjVertices[ClockwiseIndeces[i]][j];
                        } 
                    }
                }
                directionType = dgc_Clockwise;    
                Polytope->setIndeces(&directionType, &NofVertices, ClockwiseIndeces);
                
                /* Free Resources */    
                delete[] VerticesIndeces;
                for(i = 0; i < GA->famsize; i++){delete[] AdjVertices[i];}
                delete[] AdjVertices;
                            
                delete[] CounterClockwiseIndeces;
                delete[] ClockwiseIndeces;               
            }
            
            for(n = 0; n < NofDimensions; n++){delete[] Vertices[n];}   
            delete[] Vertices;
            delete[] NofAdjVertices;
            
            dd_FreeSetFamily(GA);
        }
        dd_FreeMatrix(G); 
    }
    
    /* Terminate cdd and restore resources */      
    dd_free_global_constants();  

    dd_FreePolyhedra(Polyhedra);
    dd_FreeMatrix(Matrix);

    dd_clear(value);
    
    if(ErrorFlag != dd_NoError)
    {
       return decodeErrorType(&ErrorFlag);
    }
    else
    {
       return ErrorCode; 
    }   
}

void CDGCAlgebraicGeometry::setDebugFlag(const bool* const DebugFlag)
{
    m_DebugFlag = *DebugFlag;
}

void CDGCAlgebraicGeometry::setNormalizeFlag(const bool* const NormalizeFlag)
{
    m_NormalizeFlag = *NormalizeFlag;
}

void CDGCAlgebraicGeometry::setTranslateFlag(const bool* const TranslateFlag)
{
    m_TranslateFlag = *TranslateFlag;
}

DGCErrorType CDGCAlgebraicGeometry::FacetEnumeration(CDGCPolytope* Polytope)
{
    /* Initialize cdd before performing any computations */  
    dd_set_global_constants(); 

    dd_rowrange i;
    dd_colrange j;
    
    dd_rowrange k;
    dd_colrange ell;
    
    mytype value;
    dd_init(value);

    dd_ErrorType  ErrorFlag; 
    DGCErrorType ErrorCode;
    
    dd_rowrange NofRows = Polytope->m_NofPoints; 
    dd_colrange NofCols = Polytope->m_NofDimensions + 1; 

    dd_MatrixPtr Matrix    = dd_CreateMatrix(NofRows, NofCols); 
    Matrix->representation = dd_Generator;  /* Input given in vertex form */
    Matrix->numbtype       = dd_Real;

    for(k = 0; k < NofRows; k++)
    {
        for(ell = 0; ell< NofCols; ell++)
        { 
            if(ell == 0)
            {
                dd_set_d(value, (double) 1);
            }
            else
            {
                if(m_TranslateFlag == true)
                {
                    /* Affine transformation: {P0,P1,...Pn} |-> {P0-P0,P1-P0,...Pn-P0} */
                    dd_set_d(value, Polytope->m_Points[ell-1][k]-Polytope->m_Points[ell-1][0]);
                }
                else
                {
                    /* Pass the points as they are {P0,P1,...Pn} */
                    dd_set_d(value, Polytope->m_Points[ell-1][k]); 
                }
            }
            dd_set(Matrix->matrix[k][ell], value);
        }
    }
    
    /* compute the facet form */
    dd_PolyhedraPtr Polyhedra = dd_DDMatrix2Poly(Matrix, &ErrorFlag);     
    if(ErrorFlag == dd_NoError)
    {
        ErrorCode = dgc_NoError;
        dd_MatrixPtr A = dd_CopyInequalities(Polyhedra);
        if(A == NULL)
        {
           ErrorCode = dgc_NoInequalities;
        } 
        
        if(ErrorCode == dgc_NoError)
        {
           NofRows = A->rowsize;
           NofCols = A->colsize;

           if(NofRows == 0 || NofCols == 0)
           {
              ErrorCode = dgc_NoInequalities; 
           }            
        }

        if(ErrorCode == dgc_NoError)
        {
            int n = 0;
            int NofRowsOfA = (int)NofRows;
            int NofColsOfA = (int)(NofCols-1);
            
            double** Amat = new double*[NofRowsOfA];
            if(Amat == NULL)
            {
                exit(1);
            }
            
            double*  bmat = new double[NofRowsOfA];
            for(n = 0; n < NofRowsOfA; n++)
            {
                Amat[n] = new double[NofColsOfA];
                if(Amat[n] == NULL)
                {
                    exit(1);
                }
            }
            
            for(k = 0; k < NofRows; k++)
            {
                for(ell = 0; ell< NofCols; ell++)
                {
                    if(ell == 0)
                    {
                        bmat[k] = dd_get_d(A->matrix[k][ell]);
                    }
                    else
                    { 
                        Amat[k][ell-1] = -dd_get_d(A->matrix[k][ell]);
                    }
                }
            }
            
            if(m_TranslateFlag == true)
            {
                double* btrans = new double[NofRowsOfA];
                for(i=0; i<NofRowsOfA; i++)
                {
                    btrans[i] = 0.0; 
                    for(j=0; j<NofColsOfA; j++)
                    {
                        btrans[i] = btrans[i] + Amat[i][j]*Polytope->m_Points[j][0];
                    }                        
                    btrans[i] = btrans[i] + bmat[i];
                }              
                Polytope->setHalfspaces((const int* const) &NofRowsOfA, 
                                        (const int* const) &NofColsOfA, 
                                        (const double** const) Amat, 
                                        btrans);   
                delete[] btrans;                           
            }
            else
            {
                Polytope->setHalfspaces((const int* const) &NofRowsOfA, 
                                        (const int* const) &NofColsOfA, 
                                        (const double** const) Amat, 
                                        bmat);                  
            }
            
            /* Free resources */      
            for(n = 0; n < NofRowsOfA; n++){delete[] Amat[n];}
            delete[] Amat;
            delete[] bmat;
        }
        dd_FreeMatrix(A);
    }
    
    /* Terminate cdd and restore resources */      
    dd_free_global_constants();  
    
    dd_FreePolyhedra(Polyhedra);
    dd_FreeMatrix(Matrix);
    dd_clear(value);
       
    if(ErrorFlag != dd_NoError)
    {   
       return decodeErrorType(&ErrorFlag);
    }
    else
    {
       return ErrorCode; 
    }
}

DGCErrorType CDGCAlgebraicGeometry::VertexAndFacetEnumeration(CDGCPolytope* Polytope)
{   
    DGCErrorType ErrorCode = FacetEnumeration(Polytope);
    if(ErrorCode == dgc_NoError)
    {
        ErrorCode = VertexEnumeration(Polytope);
    }
    return ErrorCode;
}

int CDGCAlgebraicGeometry::ComputeCentroid(CDGCPolytope* Polytope)
{
    int i = 0;
    int j = 0;
    int k = 0;
    
    int NofDimensions = Polytope->m_NofDimensions;
    int NofVertices   = Polytope->m_NofVertices;
    
    double* Centroid;
    if(NofDimensions >0 && NofVertices >0 && Polytope->m_Vertices != NULL)
    {
        Centroid = new double[NofDimensions];
        for(j = 0; j < NofDimensions; j++){Centroid[j] = 0;}
        
        k = 0;
        for(i = 0; i < NofVertices; i++)
        {
            for(j = 0; j < NofDimensions; j++)
            {
                Centroid[j] = Centroid[j] + Polytope->m_Vertices[j][i];
            }
        }
        
        for(j = 0; j < NofDimensions; j++)
        {
            Centroid[j] = (1.0/((double)NofVertices))*Centroid[j];
        }
        
        Polytope->setCentroid(&NofDimensions, Centroid);
        
        delete[] Centroid;
        return 0;
    }
    else
    {
        return -1;
    }
}

int CDGCAlgebraicGeometry::ComputeBoundingBox(CDGCPolytope* Polytope)
{
    int i = 0;
    int j = 0;
    
    int NofDimensions = Polytope->m_NofDimensions;
    int NofVertices   = Polytope->m_NofVertices;
    
    double* mins;
    double* maxs;
    
    if(NofDimensions > 0 && NofVertices > 0 && Polytope->m_Vertices != NULL)
    {
        mins = new double[NofDimensions];
        maxs = new double[NofDimensions];

        for(j = 0; j < NofDimensions; j++)
        {
            mins[j] = Polytope->m_Vertices[j][0];
            for(i=1; i < NofVertices; i++)
            {
                if(Polytope->m_Vertices[j][i] < mins[j])
                {
                    mins[j] = Polytope->m_Vertices[j][i];
                }
            }
            
            maxs[j] = Polytope->m_Vertices[j][0];
            for(i=1; i<NofVertices; i++)
            {
                if(Polytope->m_Vertices[j][i] > maxs[j])
                {
                    maxs[j] = Polytope->m_Vertices[j][i];
                }           
            }        
        }

        Polytope->setBoundingBox(&NofDimensions, mins, maxs);
        
        delete[] mins;
        delete[] maxs;
        
        return 0;
    }
    else
    {
        return -1;
    }
}

int CDGCAlgebraicGeometry::ComputeVolume(CDGCPolytope* Polytope)
{
    getVertices(Polytope);
    getHyperplanes(Polytope);
    
    rational volume;
    volume_ortho_data(&volume);
    Polytope->setVolume((double)volume);
    
    return 0;
}

void CDGCAlgebraicGeometry::getVertices(CDGCPolytope* Polytope)
{
   T_Vertex* v;
   char* data_type_string = "real";
   int   data_type;
   
   int i = 0;
   int j = 0;
   
   int NofRows = Polytope->m_NofVertices; 
   int NofCols = Polytope->m_NofDimensions + 1; 
   
   G_Vertices = create_empty_set ();
   
   G_n = NofRows; 
   G_d = NofCols - 1;
   
   data_type = determine_data_type(data_type_string);

   for(i = 0; i < G_n; i++)
   {  
      v = create_vertex ();
      v -> no = i;               /* this assures v to be added at the end of the list */
      if (data_type == REAL_T)
      {
         for(j = 0; j < G_d; j++)
         {
             v->coords[j] = Polytope->m_Vertices[j][i];
         }
      }
      add_element (&G_Vertices, v);
   }
   
}

void CDGCAlgebraicGeometry::getHyperplanes(CDGCPolytope* Polytope)
{
    char* data_type_string = "real";
    int   data_type;
    
    int i = 0;
    int j = 0;

    int NofRows = Polytope->m_NofRows;
    int NofCols = Polytope->m_NofDimensions + 1;

   G_m = NofRows; 
   G_d = NofCols - 1;
   data_type = determine_data_type(data_type_string);
   
   create_hyperplanes ();
   if (data_type == REAL_T)
   {
      for (i = 0; i < G_m; i++)
      {  
         for (j = 0; j < G_d; j++)
         {  
            G_Hyperplanes[i][j] = Polytope->m_A[i][j];             
         }
         G_Hyperplanes[i][G_d] = Polytope->m_b[i];
      }
    }
}

DGCErrorType CDGCAlgebraicGeometry::VertexEnumeration(const int* const NofRowsOfA,
                                                      const int* const NofColsOfA,
                                                      const double** const A,
                                                      const double* const b,
                                                      int* const NofDimensions,
                                                      int* const NofPoints,
                                                      double** const Points)
{
    /* Initialize cdd before performing any computations */  
    dd_set_global_constants(); 
    
    mytype value; 
    dd_init(value);
    
    dd_rowrange NofRows = *NofRowsOfA;
    dd_colrange NofCols = *NofColsOfA + 1;
    
    dd_ErrorType ErrorFlag;
    DGCErrorType ErrorCode;
    
    dd_rowrange k;
    dd_colrange ell;

    dd_MatrixPtr Matrix    = dd_CreateMatrix(NofRows, NofCols); 
    Matrix->representation = dd_Inequality;   /* Input given in facet form */
    Matrix->numbtype       = dd_Real;
    
    for(k = 0; k < NofRows; k++)
    {
        for(ell = 0; ell< NofCols; ell++)
        { 
            if(ell == 0)
            {
                dd_set_d(value, b[k]);
            }
            else
            {
                dd_set_d(value, -A[k][ell-1]);
            }
            dd_set(Matrix->matrix[k][ell], value);
        }
    }
    
    /* compute the vertex form */
    dd_PolyhedraPtr Polyhedra = dd_DDMatrix2Poly(Matrix, &ErrorFlag);      
    if(ErrorFlag == dd_NoError)
    {
        dd_MatrixPtr G = dd_CopyGenerators(Polyhedra);
        if(G == NULL)
        {
           exit(1);
        }
        NofRows = G->rowsize;
        NofCols = G->colsize;
        
        if(*NofDimensions == NofCols-1 &&  *NofPoints == NofRows)
        {
            for(k = 0; k < NofRows; k++)
            {
                for(ell = 1; ell< NofCols; ell++)
                {
                    Points[ell-1][k]  = dd_get_d(G->matrix[k][ell]);
                }
            }
            ErrorCode = dgc_NoError;
        }
        else
        {
             ErrorCode     = dgc_Resize;
            *NofDimensions = NofCols-1;
            *NofPoints     = NofRows;
        }
        dd_FreeMatrix(G); 
    }
    
    /* Terminate cdd and restore resources */      
    dd_free_global_constants();    
    dd_FreePolyhedra(Polyhedra);
    dd_FreeMatrix(Matrix);
    
    dd_clear(value);

    if(ErrorFlag != dd_NoError)
    {
        return decodeErrorType(&ErrorFlag);
    }
    else
    {
        return ErrorCode;
    }
}

DGCErrorType CDGCAlgebraicGeometry::decodeErrorType(dd_ErrorType* ErrorFlag)
{
    DGCErrorType ReturnCode;
    
    switch (*ErrorFlag)
    {
        case dd_DimensionTooLarge:
            ReturnCode = dgc_DimensionTooLarge;
            break;
        case dd_ImproperInputFormat:
            ReturnCode = dgc_ImproperInputFormat;
            break;        
        case dd_NegativeMatrixSize: 
            ReturnCode = dgc_NegativeMatrixSize;
            break;                
        case dd_EmptyVrepresentation:
            ReturnCode = dgc_EmptyVrepresentation;
            break;        
         
        case dd_EmptyHrepresentation: 
            ReturnCode = dgc_EmptyHrepresentation;
            break;        

        case dd_EmptyRepresentation:
            ReturnCode = dgc_EmptyRepresentation;
            break;        

        case dd_IFileNotFound: 
            ReturnCode = dgc_IFileNotFound;
            break;        

        case dd_OFileNotOpen: 
            ReturnCode = dgc_OFileNotOpen;
            break;        

        case dd_NoLPObjective: 
            ReturnCode = dgc_NoLPObjective;
            break;        

        case dd_NoRealNumberSupport:
            ReturnCode = dgc_NoRealNumberSupport;
            break;        

        case dd_NotAvailForH: 
            ReturnCode = dgc_NotAvailForH;
            break;        

        case dd_NotAvailForV: 
            ReturnCode = dgc_NotAvailForV;
            break;        

        case dd_CannotHandleLinearity:
            ReturnCode = dgc_CannotHandleLinearity;
            break;        

        case dd_RowIndexOutOfRange: 
            ReturnCode = dgc_RowIndexOutOfRange;
            break;        

        case dd_ColIndexOutOfRange:
            ReturnCode = dgc_ColIndexOutOfRange;
            break;        

        case dd_LPCycling: 
            ReturnCode = dgc_LPCycling;
            break;        

        case dd_NumericallyInconsistent:
            ReturnCode = dgc_NumericallyInconsistent;
            break;        

        case dd_NoError:
            ReturnCode = dgc_NoError;
            break;        
    }
    
    return ReturnCode;
}
