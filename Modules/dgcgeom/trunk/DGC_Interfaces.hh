/*
    DGC_Interfaces.hh
 
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology

	Created: Apr. 12, 2007
 
 */

#ifndef DGC_INTERFACES_HH
#define DGC_INTERFACES_HH

enum DGCDirectionType{dgc_Clockwise, dgc_CounterClockwise};

enum DGCErrorType{dgc_DimensionTooLarge, 
                    dgc_ImproperInputFormat, 
                    dgc_NegativeMatrixSize, 
                    dgc_EmptyVrepresentation, 
                    dgc_EmptyHrepresentation, 
                    dgc_EmptyRepresentation,
                    dgc_IFileNotFound, 
                    dgc_OFileNotOpen, 
                    dgc_NoLPObjective, 
                    dgc_NoRealNumberSupport,
                    dgc_NotAvailForH, 
                    dgc_NotAvailForV, 
                    dgc_CannotHandleLinearity,
                    dgc_RowIndexOutOfRange, 
                    dgc_ColIndexOutOfRange,
                    dgc_LPCycling, 
                    dgc_NumericallyInconsistent,
                    dgc_NoError,
                    dgc_Resize,
                    dgc_HasRays,
                    dgc_ImproperAdjacency,
                    dgc_NoInequalities};

enum DGCBoundsType{dgc_LowerBound, dgc_UpperBound};

#endif  /* DGC_INTERFACES_HH */

