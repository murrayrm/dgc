/*
	DGC_AlgebraicGeometry.hh
	
    Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	
    Created: 12/11/2006
*/

#ifndef DGC_ALGEBRAICGEOMETRY_HH
#define DGC_ALGEBRAICGEOMETRY_HH

#include "DGC_Interfaces.hh"
#include "DGC_Polytope.hh"

#if defined(__cplusplus)
extern "C"
{
#endif

#include "setoper.h"
#include "cdd.h"
#include "vinci.h"

#if defined(__cplusplus)
}
#endif

class CDGCAlgebraicGeometry
{
	public:
		CDGCAlgebraicGeometry(void);
	   ~CDGCAlgebraicGeometry(void);

		DGCErrorType VertexEnumeration(CDGCPolytope* Polytope);
		DGCErrorType FacetEnumeration(CDGCPolytope* Polytope);
	    DGCErrorType VertexAndFacetEnumeration(CDGCPolytope* Polytope);
        
        int ComputeVolume(CDGCPolytope* Polytope);
        int ComputeCentroid(CDGCPolytope* Polytope);
        int ComputeBoundingBox(CDGCPolytope* Polytope);
        
        DGCErrorType VertexEnumeration(const int* const NofRowsOfA,
                                       const int* const NofColsOfA,
                                       const double** const A,
                                       const double* const b,
                                       int* const NofDimensions,
                                       int* const NofPoints,
                                       double** const Points);
                              
        void setDebugFlag(const bool* const DebugFlag);        
        void setNormalizeFlag(const bool* const NormalizeFlag); 
        void setTranslateFlag(const bool* const TranslateFlag); 
                        
	private:
        void getVertices(CDGCPolytope* Polytope);
        void getHyperplanes(CDGCPolytope* Polytope);
        DGCErrorType decodeErrorType(dd_ErrorType* ErrorType);
        
        bool m_DebugFlag;
        bool m_NormalizeFlag;
        bool m_TranslateFlag;
 };

#endif  /* DGC_ALGEBRAICGEOMETRY_HH */
