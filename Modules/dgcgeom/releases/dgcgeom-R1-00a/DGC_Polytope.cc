/*
	DGC_Polytope.cc
    
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
    
	Created: Dec. 11, 2006
*/

#include <iostream>
#include <iomanip>

#include "DGC_Polytope.hh"

CDGCPolytope::CDGCPolytope(void)
{
    this->m_NofDimensions = 0;
    this->m_NofPoints     = 0;
    this->m_Points        = NULL;
    
    this->m_NofVertices = 0;
    this->m_Vertices    = NULL;
    
    this->m_ClockwiseIndeces        = NULL;
    this->m_CounterClockwiseIndeces = NULL;
    
    this->m_NofRows    = 0;
    this->m_NofColumns = 0;
    
    this->m_A = NULL;
    this->m_b = NULL;
    
    this->m_Centroid = NULL;
    
    this->m_mins = NULL;
    this->m_maxs = NULL;
    
    this->m_Volume = 0.0;           
}

/* Construct a Polytope shell with the points only. */
CDGCPolytope::CDGCPolytope(const int* const NofDimensions, const int* const NofPoints, const double** const Points)
{
    int i = 0;
    int j = 0;
    
    if(*NofDimensions > 0 && *NofPoints > 0 && Points != NULL)
    {
   	   this->m_NofDimensions = *NofDimensions;
	   this->m_NofPoints     = *NofPoints;
       
	   this->m_Points = new double*[this->m_NofDimensions];
       for(i = 0; i< this->m_NofDimensions; i++)
       {
           this->m_Points[i] = new double[this->m_NofPoints];
	       for(j = 0; j < this->m_NofPoints; j++)
	       {
			   this->m_Points[i][j] = Points[i][j]; 	
		   }           
       }
    
       this->m_NofVertices = 0;
       this->m_Vertices    = NULL;
       
       this->m_ClockwiseIndeces        = NULL;
       this->m_CounterClockwiseIndeces = NULL;
       
       this->m_NofRows    = 0;
       this->m_NofColumns = 0;
       
       this->m_A = NULL;
       this->m_b = NULL;
       
       this->m_Centroid = NULL;
       
       this->m_mins = NULL;
       this->m_maxs = NULL;
       
       this->m_Volume = 0.0;              
    }
    else
    {
        this->m_NofDimensions = 0;
        this->m_NofPoints     = 0;
        this->m_Points        = NULL;
        
        this->m_NofVertices = 0;
        this->m_Vertices    = NULL;
        
        this->m_ClockwiseIndeces        = NULL;
        this->m_CounterClockwiseIndeces = NULL;
        
        this->m_NofRows    = 0;
        this->m_NofColumns = 0;
        
        this->m_A = NULL;
        this->m_b = NULL;
        
        this->m_Centroid = NULL;
        
        this->m_mins = NULL;
        this->m_maxs = NULL;
        
        this->m_Volume = 0.0;        
    }
}

CDGCPolytope::CDGCPolytope(const int* const NofDimensions, const int* const NofPoints, const double** const Points, const bool* const SwitchCoordinates)
{
    int i = 0;
    int j = 0;
    
    if(*NofDimensions == 2 && *NofPoints > 0 && Points != NULL)
    {
   	   this->m_NofDimensions = *NofDimensions;
	   this->m_NofPoints     = *NofPoints;
       
	   this->m_Points = new double*[this->m_NofDimensions];
       for(i = 0; i< this->m_NofDimensions; i++){this->m_Points[i] = new double[this->m_NofPoints];}

       if(*SwitchCoordinates == false)
       { 
           for(i = 0; i< this->m_NofDimensions; i++)
           {
               for(j = 0; j < this->m_NofPoints; j++)
               {
                   this->m_Points[i][j] = Points[i][j]; 	
               }           
           }
       }
       else
       {
          for(j = 0; j < this->m_NofPoints; j++)
          {
              this->m_Points[0][j] = Points[1][j]; 	
              this->m_Points[1][j] = Points[0][j]; 	
          }           
       }

       this->m_NofVertices = 0;
       this->m_Vertices    = NULL;
       
       this->m_ClockwiseIndeces        = NULL;
       this->m_CounterClockwiseIndeces = NULL;
       
       this->m_NofRows    = 0;
       this->m_NofColumns = 0;
       
       this->m_A = NULL;
       this->m_b = NULL;
       
       this->m_Centroid = NULL;
       
       this->m_mins = NULL;
       this->m_maxs = NULL;
       
       this->m_Volume = 0.0;              
    }
    else
    {
        this->m_NofDimensions = 0;
        this->m_NofPoints     = 0;
        this->m_Points        = NULL;
        
        this->m_NofVertices = 0;
        this->m_Vertices    = NULL;
        
        this->m_ClockwiseIndeces        = NULL;
        this->m_CounterClockwiseIndeces = NULL;
        
        this->m_NofRows    = 0;
        this->m_NofColumns = 0;
        
        this->m_A = NULL;
        this->m_b = NULL;
        
        this->m_Centroid = NULL;
        
        this->m_mins = NULL;
        this->m_maxs = NULL;
        
        this->m_Volume = 0.0;        
    }
}

CDGCPolytope::CDGCPolytope(const CDGCPolytope* const Polytope)
{
    if(Polytope != NULL)
    {      
        int i = 0;
        int j = 0;
        
        this->m_NofDimensions = Polytope->m_NofDimensions;
        this->m_NofPoints     = Polytope->m_NofPoints;     
        
        if(this->m_NofDimensions > 0 && this->m_NofPoints > 0 && Polytope->m_Points != NULL)
        { 
            this->m_Points = new double*[this->m_NofDimensions];
            for(i = 0; i< this->m_NofDimensions; i++)
            {
                this->m_Points[i] = new double[this->m_NofPoints];
                for(j = 0; j < this->m_NofPoints; j++)
                {
                    this->m_Points[i][j] = Polytope->m_Points[i][j]; 	
                }                
            }
        }
        else
        {
           this->m_NofDimensions = 0;
           this->m_NofPoints     = 0;
           this->m_Points = NULL;
        }
        
        if(Polytope->m_NofVertices > 0 && Polytope->m_Vertices != NULL)
        {
            this->m_NofVertices = Polytope->m_NofVertices;
            this->m_Vertices    = new double*[this->m_NofDimensions];
            for(i=0; i<this->m_NofDimensions; i++)
            {
                this->m_Vertices[i] = new double[this->m_NofVertices];
                for(j = 0; j<this->m_NofVertices; j++)
                {
                    this->m_Vertices[i][j] = Polytope->m_Vertices[i][j];
                }
            }
        }
        else
        {
            this->m_NofVertices = 0; 
            this->m_Vertices    = NULL;
        }
        
        if(Polytope->m_NofVertices > 0 && Polytope->m_CounterClockwiseIndeces != NULL)
        {
            this->m_CounterClockwiseIndeces = new int[this->m_NofVertices];
            for(i=0; i<this->m_NofVertices; i++)
            {
                this->m_CounterClockwiseIndeces[i] = Polytope->m_CounterClockwiseIndeces[i];
            } 
        }
        else
        {
            this->m_CounterClockwiseIndeces = NULL;
        }
        
        if(Polytope->m_NofVertices > 0 && Polytope->m_ClockwiseIndeces != NULL)
        {
            this->m_ClockwiseIndeces = new int[this->m_NofVertices];
            for(i=0; i<this->m_NofVertices; i++)
            {
                this->m_ClockwiseIndeces[i] = Polytope->m_ClockwiseIndeces[i];
            } 
        }
        else
        {
            this->m_ClockwiseIndeces = NULL;
        }
        
        if(Polytope->m_NofRows > 0 && Polytope->m_NofColumns > 0 && Polytope->m_A != NULL && Polytope->m_b != NULL)
        {
            this->m_NofRows    = Polytope->m_NofRows;
            this->m_NofColumns = Polytope->m_NofColumns;
            
            this->m_A = new double*[this->m_NofRows];
            this->m_b = new double[this->m_NofRows];
            for(i=0; i<this->m_NofRows; i++)
            {
                this->m_b[i] = Polytope->m_b[i];
                this->m_A[i] = new double[this->m_NofColumns];
                for(j=0; j<this->m_NofColumns; j++)
                {
                    this->m_A[i][j] = Polytope->m_A[i][j];
                }
            }
        }
        else
        {
            this->m_NofRows    = 0;
            this->m_NofColumns = 0; 
            this->m_A = NULL;
            this->m_b = NULL;
        }
        
        if(Polytope->m_Centroid != NULL)
        {
            this->m_Centroid = new double[this->m_NofDimensions];
            for(i=0; i<this->m_NofDimensions; i++)
            {
                this->m_Centroid[i] = Polytope->m_Centroid[i]; 
            }
        }
        else
        {
            this->m_Centroid = NULL; 
        }
        
        if(Polytope->m_mins != NULL && Polytope->m_maxs != NULL)
        {
            this->m_mins = new double[this->m_NofDimensions];
            this->m_maxs = new double[this->m_NofDimensions];
            for(i=0; i<this->m_NofDimensions; i++)
            {
                this->m_mins[i] = Polytope->m_mins[i];
                this->m_maxs[i] = Polytope->m_maxs[i];
            }
        }
        else
        {
            this->m_mins = NULL;
           this-> m_maxs = NULL;
        }
        
        this->m_Volume = Polytope->m_Volume;                
    }
    else
    {
        this->m_NofDimensions = 0;
        this->m_NofPoints     = 0;
        this->m_Points        = NULL;
        
        this->m_NofVertices = 0;
        this->m_Vertices    = NULL;
        
        this->m_ClockwiseIndeces        = NULL;
        this->m_CounterClockwiseIndeces = NULL;
        
        this->m_NofRows    = 0;
        this-> m_NofColumns = 0;
        
        this->m_A = NULL;
        this->m_b = NULL;
        
        this->m_Centroid = NULL;
        
        this->m_mins = NULL;
        this->m_maxs = NULL;
        
        this->m_Volume = 0.0;   
    }
}
 
CDGCPolytope::CDGCPolytope(const CDGCPolytope& Polytope)
{
    if(&Polytope != NULL)
    {      
        int i = 0;
        int j = 0;
        
        this->m_NofDimensions = Polytope.m_NofDimensions;
        this->m_NofPoints     = Polytope.m_NofPoints;     
        
        if(this->m_NofDimensions > 0 && this->m_NofPoints > 0 && Polytope.m_Points != NULL)
        { 
            this->m_Points = new double*[this->m_NofDimensions];
            for(i = 0; i< this->m_NofDimensions; i++)
            {
                this->m_Points[i] = new double[this->m_NofPoints];
                for(j = 0; j < this->m_NofPoints; j++)
                {
                    this->m_Points[i][j] = Polytope.m_Points[i][j]; 	
                }                
            }
        }
        else
        {
           this->m_NofDimensions = 0;
           this->m_NofPoints     = 0;
           this->m_Points = NULL;
        }
        
        if(Polytope.m_NofVertices > 0 && Polytope.m_Vertices != NULL)
        {
            this->m_NofVertices = Polytope.m_NofVertices;
            this->m_Vertices    = new double*[this->m_NofDimensions];
            for(i=0; i<this->m_NofDimensions; i++)
            {
                this->m_Vertices[i] = new double[this->m_NofVertices];
                for(j = 0; j<this->m_NofVertices; j++)
                {
                    this->m_Vertices[i][j] = Polytope.m_Vertices[i][j];
                }
            }
        }
        else
        {
            this->m_NofVertices = 0; 
            this->m_Vertices    = NULL;
        }
        
        if(Polytope.m_NofVertices > 0 && Polytope.m_CounterClockwiseIndeces != NULL)
        {
            this->m_CounterClockwiseIndeces = new int[this->m_NofVertices];
            for(i=0; i<this->m_NofVertices; i++)
            {
                this->m_CounterClockwiseIndeces[i] = Polytope.m_CounterClockwiseIndeces[i];
            } 
        }
        else
        {
            this->m_CounterClockwiseIndeces = NULL;
        }
        
        if(Polytope.m_NofVertices > 0 && Polytope.m_ClockwiseIndeces != NULL)
        {
            this->m_ClockwiseIndeces = new int[this->m_NofVertices];
            for(i=0; i<this->m_NofVertices; i++)
            {
                this->m_ClockwiseIndeces[i] = Polytope.m_ClockwiseIndeces[i];
            } 
        }
        else
        {
            this->m_ClockwiseIndeces = NULL;
        }
        
        if(Polytope.m_NofRows > 0 && Polytope.m_NofColumns > 0 && Polytope.m_A != NULL && Polytope.m_b != NULL)
        {
            this->m_NofRows    = Polytope.m_NofRows;
            this->m_NofColumns = Polytope.m_NofColumns;
            
            this->m_A = new double*[this->m_NofRows];
            this->m_b = new double[this->m_NofRows];
            for(i=0; i<this->m_NofRows; i++)
            {
                this->m_b[i] = Polytope.m_b[i];
                this->m_A[i] = new double[this->m_NofColumns];
                for(j=0; j<this->m_NofColumns; j++)
                {
                    this->m_A[i][j] = Polytope.m_A[i][j];
                }
            }
        }
        else
        {
            this->m_NofRows    = 0;
            this->m_NofColumns = 0; 
            this->m_A = NULL;
            this->m_b = NULL;
        }
        
        if(Polytope.m_Centroid != NULL)
        {
            this->m_Centroid = new double[this->m_NofDimensions];
            for(i=0; i<this->m_NofDimensions; i++)
            {
                this->m_Centroid[i] = Polytope.m_Centroid[i]; 
            }
        }
        else
        {
            this->m_Centroid = NULL; 
        }
        
        if(Polytope.m_mins != NULL && Polytope.m_maxs != NULL)
        {
            this->m_mins = new double[this->m_NofDimensions];
            this->m_maxs = new double[this->m_NofDimensions];
            for(i=0; i<this->m_NofDimensions; i++)
            {
                this->m_mins[i] = Polytope.m_mins[i];
                this->m_maxs[i] = Polytope.m_maxs[i];
            }
        }
        else
        {
            this->m_mins = NULL;
            this-> m_maxs = NULL;
        }
        
        this->m_Volume = Polytope.m_Volume;                
    }
    else
    {
        this->m_NofDimensions = 0;
        this->m_NofPoints     = 0;
        this->m_Points        = NULL;
        
        this->m_NofVertices = 0;
        this->m_Vertices    = NULL;
        
        this->m_ClockwiseIndeces        = NULL;
        this->m_CounterClockwiseIndeces = NULL;
        
        this->m_NofRows    = 0;
        this-> m_NofColumns = 0;
        
        this->m_A = NULL;
        this->m_b = NULL;
        
        this->m_Centroid = NULL;
        
        this->m_mins = NULL;
        this->m_maxs = NULL;
        
        this->m_Volume = 0.0;   
    }
} 
 
CDGCPolytope::~CDGCPolytope(void)
{
    int i = 0;
    if(this->m_Points != NULL)
    {
        for(i = 0; i< this->m_NofDimensions; i++){delete[] this->m_Points[i];}
        delete[] this->m_Points;
    }
    
    if(this->m_Vertices != NULL)
    {
        for(i=0; i<this->m_NofDimensions; i++){delete[] this->m_Vertices[i];}
        delete[] this->m_Vertices;
    }      
    
    if(this->m_CounterClockwiseIndeces != NULL)
    {
        delete[] this->m_CounterClockwiseIndeces;
    }
    
    if(this->m_ClockwiseIndeces != NULL)
    {
        delete[] this->m_ClockwiseIndeces;
    }       
    
    if(this->m_A != NULL)
    {
        for(i=0; i<this->m_NofRows; i++){delete[] this->m_A[i];}
        delete[] this->m_A;
    }
    
    if(this->m_b != NULL)
    {
        delete[] this->m_b;
    }      
    
    if(this->m_Centroid != NULL)
    {
        delete[] this->m_Centroid; 
    }
    
    if(this->m_mins != NULL)
    {
        delete[] this->m_mins;
    }
    
    if(this->m_maxs != NULL)
    {
        delete[] this->m_maxs;
    }      
}

CDGCPolytope* const CDGCPolytope::operator=(const CDGCPolytope* const Polytope)
{
    if(this != Polytope)
    {
        int i = 0;
        int j = 0;
        
        if(this->m_Points != NULL)
        {
            for(i = 0; i< this->m_NofDimensions; i++){delete[] this->m_Points[i];}
            delete[] this->m_Points;
            this->m_Points = NULL;
        }
        
        if(this->m_Vertices != NULL)
        {
            for(i=0; i<this->m_NofDimensions; i++){delete[] this->m_Vertices[i];}
            delete[] this->m_Vertices;
            this->m_Vertices = NULL;
        }   
        
        if(this->m_CounterClockwiseIndeces != NULL)
        {
            delete[] this->m_CounterClockwiseIndeces;
            this->m_CounterClockwiseIndeces = NULL;
        }
        
        if(this->m_ClockwiseIndeces != NULL)
        {
            delete[] this->m_ClockwiseIndeces;    
            this->m_ClockwiseIndeces = NULL;             
        }       
        
        if(this->m_A != NULL)
        {
            for(i=0; i<this->m_NofRows; i++){delete[] this->m_A[i];}
            delete[] this->m_A;
            this->m_A = NULL;
        }
        
        if(this->m_b != NULL)
        {
            delete[] this->m_b;
            this->m_b = NULL;
        }      
        
        if(this->m_Centroid != NULL)
        {
            delete[] this->m_Centroid; 
            this->m_Centroid = NULL;
        }
        
        if(this->m_mins != NULL)
        {
            delete[] this->m_mins;
            this->m_mins = NULL;
        }
        
        if(this->m_maxs != NULL)
        {
            delete[] this->m_maxs;    
            this->m_maxs = NULL;           
        }      
        
        this->m_NofDimensions = 0;
        this->m_NofPoints     = 0;
        
        this->m_NofVertices = 0;
        this->m_NofRows     = 0;
        this->m_NofColumns  = 0;
        
        this->m_Volume = 0.0;             
        
        if(Polytope != NULL)
        {      
            if(Polytope->m_Points != NULL)
            {
                this->m_NofDimensions = Polytope->m_NofDimensions;
                this->m_NofPoints     = Polytope->m_NofPoints;
                this->m_Points        = new double*[m_NofDimensions];
                for(i=0; i<this->m_NofDimensions; i++)
                {
                    this->m_Points[i] = new double[this->m_NofPoints];
                    for(j=0; j<this->m_NofPoints; j++)
                    {
                        this->m_Points[i][j] = Polytope->m_Points[i][j];
                    }
                }
            }
            
            if(Polytope->m_Vertices !=NULL)
            {
                this->m_NofVertices = Polytope->m_NofVertices;
                this->m_Vertices = new double*[this->m_NofDimensions];
                for(i=0; i<this->m_NofDimensions; i++)
                {
                    this->m_Vertices[i] = new double[this->m_NofVertices];
                    for(j=0; j<this->m_NofVertices; j++)
                    {
                        this->m_Vertices[i][j] = Polytope->m_Vertices[i][j];
                    }
                } 
            }
            
            if(Polytope->m_CounterClockwiseIndeces != NULL)
            {
                this->m_CounterClockwiseIndeces = new int[this->m_NofVertices];
                for(i=0; i<this->m_NofVertices; i++)
                {
                    this->m_CounterClockwiseIndeces[i] = Polytope->m_CounterClockwiseIndeces[i];
                } 
            }
            
            if(Polytope->m_ClockwiseIndeces != NULL)
            {
                this->m_ClockwiseIndeces = new int[this->m_NofVertices];
                for(i=0; i<this->m_NofVertices; i++)
                {
                    this->m_ClockwiseIndeces[i] = Polytope->m_ClockwiseIndeces[i];
                } 
            }                    
            
            if(Polytope->m_A != NULL)
            {
                this->m_NofRows    = Polytope->m_NofRows;
                this->m_NofColumns = Polytope->m_NofColumns; 
                this->m_A          = new double*[this->m_NofRows];
                for(i=0; i<this->m_NofRows; i++)
                {
                    this->m_A[i] = new double[this->m_NofColumns];
                    for(j=0; j<this->m_NofColumns; j++)
                    {
                        this->m_A[i][j] = Polytope->m_A[i][j];
                    } 
                }
            }
            
            if(Polytope->m_b != NULL)
            {
                this->m_b = new double[this->m_NofRows];
                for(i=0; i<this->m_NofRows; i++)
                {
                    this->m_b[i] = Polytope->m_b[i];
                }     
            }
            
            if(Polytope->m_Centroid != NULL)
            {
                this->m_Centroid = new double[this->m_NofDimensions];
                for(i=0; i<this->m_NofDimensions; i++)
                {
                    this->m_Centroid[i] = Polytope->m_Centroid[i];
                } 
            }
            
            if(Polytope->m_mins != NULL)
            {
                this->m_mins = new double[this->m_NofDimensions];
                for(i=0; i<this->m_NofDimensions; i++)
                {
                    this->m_mins[i] = Polytope->m_mins[i];
                }                     
            }
            
            if(Polytope->m_maxs != NULL)
            {
                this->m_maxs = new double[this->m_NofDimensions];
                for(i=0; i<this->m_NofDimensions; i++)
                {
                    this->m_maxs[i] = Polytope->m_maxs[i];
                }                     
            }                    
            this->m_Volume = Polytope->m_Volume; 
        }
    }
    
    return this;
}

CDGCPolytope& CDGCPolytope::operator=(const CDGCPolytope& Polytope)
{
    if(this != &Polytope)
    {
        int i = 0;
        int j = 0;
        
        if(this->m_Points != NULL)
        {
            for(i = 0; i< this->m_NofDimensions; i++){delete[] this->m_Points[i];}
            delete[] this->m_Points;
            this->m_Points = NULL;
        }
        
        if(this->m_Vertices != NULL)
        {
            for(i=0; i<this->m_NofDimensions; i++){delete[] this->m_Vertices[i];}
            delete[] this->m_Vertices;
            this->m_Vertices = NULL;
        }   
        
        if(this->m_CounterClockwiseIndeces != NULL)
        {
            delete[] this->m_CounterClockwiseIndeces;
            this->m_CounterClockwiseIndeces = NULL;
        }
        
        if(this->m_ClockwiseIndeces != NULL)
        {
            delete[] this->m_ClockwiseIndeces;    
            this->m_ClockwiseIndeces = NULL;             
        }       
        
        if(this->m_A != NULL)
        {
            for(i=0; i<this->m_NofRows; i++){delete[] this->m_A[i];}
            delete[] this->m_A;
            this->m_A = NULL;
        }
        
        if(this->m_b != NULL)
        {
            delete[] this->m_b;
            this->m_b = NULL;
        }      
        
        if(this->m_Centroid != NULL)
        {
            delete[] this->m_Centroid; 
            this->m_Centroid = NULL;
        }
        
        if(this->m_mins != NULL)
        {
            delete[] this->m_mins;
            this->m_mins = NULL;
        }
        
        if(this->m_maxs != NULL)
        {
            delete[] this->m_maxs;    
            this->m_maxs = NULL;           
        }      
        
        this->m_NofDimensions = 0;
        this->m_NofPoints     = 0;
        
        this->m_NofVertices = 0;
        this->m_NofRows     = 0;
        this->m_NofColumns  = 0;
        
        this->m_Volume = 0.0;             
        
        if(&Polytope != NULL)
        {      
            if(Polytope.m_Points != NULL)
            {
                this->m_NofDimensions = Polytope.m_NofDimensions;
                this->m_NofPoints     = Polytope.m_NofPoints;
                this->m_Points        = new double*[m_NofDimensions];
                for(i=0; i<this->m_NofDimensions; i++)
                {
                    this->m_Points[i] = new double[this->m_NofPoints];
                    for(j=0; j<this->m_NofPoints; j++)
                    {
                        this->m_Points[i][j] = Polytope.m_Points[i][j];
                    }
                }
            }
            
            if(Polytope.m_Vertices !=NULL)
            {
                this->m_NofVertices = Polytope.m_NofVertices;
                this->m_Vertices = new double*[this->m_NofDimensions];
                for(i=0; i<this->m_NofDimensions; i++)
                {
                    this->m_Vertices[i] = new double[this->m_NofVertices];
                    for(j=0; j<this->m_NofVertices; j++)
                    {
                        this->m_Vertices[i][j] = Polytope.m_Vertices[i][j];
                    }
                } 
            }
            
            if(Polytope.m_CounterClockwiseIndeces != NULL)
            {
                this->m_CounterClockwiseIndeces = new int[this->m_NofVertices];
                for(i=0; i<this->m_NofVertices; i++)
                {
                    this->m_CounterClockwiseIndeces[i] = Polytope.m_CounterClockwiseIndeces[i];
                } 
            }
            
            if(Polytope.m_ClockwiseIndeces != NULL)
            {
                this->m_ClockwiseIndeces = new int[this->m_NofVertices];
                for(i=0; i<this->m_NofVertices; i++)
                {
                    this->m_ClockwiseIndeces[i] = Polytope.m_ClockwiseIndeces[i];
                } 
            }                    
            
            if(Polytope.m_A != NULL)
            {
                this->m_NofRows    = Polytope.m_NofRows;
                this->m_NofColumns = Polytope.m_NofColumns; 
                this->m_A          = new double*[this->m_NofRows];
                for(i=0; i<this->m_NofRows; i++)
                {
                    this->m_A[i] = new double[this->m_NofColumns];
                    for(j=0; j<this->m_NofColumns; j++)
                    {
                        this->m_A[i][j] = Polytope.m_A[i][j];
                    } 
                }
            }
            
            if(Polytope.m_b != NULL)
            {
                this->m_b = new double[this->m_NofRows];
                for(i=0; i<this->m_NofRows; i++)
                {
                    this->m_b[i] = Polytope.m_b[i];
                }     
            }
            
            if(Polytope.m_Centroid != NULL)
            {
                this->m_Centroid = new double[this->m_NofDimensions];
                for(i=0; i<this->m_NofDimensions; i++)
                {
                    this->m_Centroid[i] = Polytope.m_Centroid[i];
                } 
            }
            
            if(Polytope.m_mins != NULL)
            {
                this->m_mins = new double[this->m_NofDimensions];
                for(i=0; i<this->m_NofDimensions; i++)
                {
                    this->m_mins[i] = Polytope.m_mins[i];
                }                     
            }
            
            if(Polytope.m_maxs != NULL)
            {
                this->m_maxs = new double[this->m_NofDimensions];
                for(i=0; i<this->m_NofDimensions; i++)
                {
                    this->m_maxs[i] = Polytope.m_maxs[i];
                }                     
            }                    
            this->m_Volume = Polytope.m_Volume; 
        }
    }
    
    return *this;
}

double CDGCPolytope::getVolume(void) const
{
	return this->m_Volume;
}

int CDGCPolytope::getNofDimensions(void) const
{
	return this->m_NofDimensions;
}

int CDGCPolytope::getNofPoints(void) const
{
	return this->m_NofPoints;
}

int CDGCPolytope::getPoints(const int* const NofDimensions, const int* const NofPoints, double** const Points) const
{	
	if(*NofDimensions == this->m_NofDimensions && *NofPoints == this->m_NofPoints && Points != NULL)
    {
        if(this->m_Points != NULL)
        {
            for(int i = 0; i < this->m_NofDimensions; i++)
            {
                for(int j = 0; j < this->m_NofPoints; j++)
                {
                    Points[i][j] = this->m_Points[i][j]; 	
                }
            }    
            return 0;
        }
        else
        {
            return -2; /* Nothing to return */
        } 
    }
    else
    {
	    return -1; /* Wrong input -- cannot return Points */
    }
}

int CDGCPolytope::getNofVertices(void) const
{
	return this->m_NofVertices;
}

int CDGCPolytope::getVertices(const int* const NofDimensions, const int* const NofVertices, double** const Vertices) const
{
    if(*NofDimensions == this->m_NofDimensions && *NofVertices == this->m_NofVertices && Vertices != NULL)
    {
        if(this->m_Vertices != NULL)
        {     
            for(int i=0; i<this->m_NofDimensions; i++)
            {
                for(int j=0; j<this->m_NofVertices; j++)
                {
                    Vertices[i][j] = this->m_Vertices[i][j];
                }
            }
		    return 0;            
        }
        else
        {
            return -2;
        }   
    }
    else
    {
        return -1;
    }
}

int CDGCPolytope::getIndeces(const DGCDirectionType* const directionType, const int* const NofVertices, int* const Indeces) const
{
    if(*NofVertices == this->m_NofVertices && Indeces != NULL)
    {
        if(*directionType == dgc_CounterClockwise)
        {    
            if(this->m_CounterClockwiseIndeces != NULL)
            {
                for(int j=0; j<this->m_NofVertices; j++)
                {
                    Indeces[j] = this->m_CounterClockwiseIndeces[j];
                }      
                return 0;    
            } 
            else
            {
                return -2;
            }
        }
        else /*if(*directionType == alg_CounterClockwise)*/
        {
            if(this->m_ClockwiseIndeces != NULL)
            {
                for(int j=0; j<this->m_NofVertices; j++)
                {
                    Indeces[j] = this->m_ClockwiseIndeces[j];
                }      
                return 0;    
            } 
            else
            {
                return -2;
            }                    
        }
    }
    else
    {
        return -1; 
    }
}

int CDGCPolytope::setVertices(const int* const NofDimensions, const int* const NofVertices, const double** const Vertices) 
{
    if((*NofDimensions > 0) && (*NofVertices > 0) && (Vertices != NULL))
    {
        int i = 0;
        int j = 0;    
        
        if(this->m_Vertices != NULL)
        {
			for(i=0; i<this->m_NofDimensions; i++)
			{
                delete[] this->m_Vertices[i];
            }
            delete[] this->m_Vertices;
            this->m_Vertices = NULL;
        }
        
        if(this->m_CounterClockwiseIndeces != NULL)
        {
           delete[] this->m_CounterClockwiseIndeces;
           this->m_CounterClockwiseIndeces = NULL;
        }         
        
        if(this->m_ClockwiseIndeces != NULL)
        {
           delete[] this->m_ClockwiseIndeces;
           this->m_ClockwiseIndeces = NULL;
        }
                
        this->m_NofVertices = *NofVertices;
        this->m_Vertices    = new double*[this->m_NofDimensions];
        for(i=0; i<this->m_NofDimensions; i++)
        {
            this->m_Vertices[i] = new double[this->m_NofVertices];
            for(j=0; j<this->m_NofVertices; j++)
            {
                this->m_Vertices[i][j] = Vertices[i][j];
            }                
        }        
        this->m_CounterClockwiseIndeces = new int[this->m_NofVertices];
        this->m_ClockwiseIndeces        = new int[this->m_NofVertices];
        return 0;
    }
    else
    {
        return -1;
    }
}

int CDGCPolytope::setIndeces(const DGCDirectionType* const directionType, const int* const NofVertices, const int* const Indeces)
{
    int i = 0;
    if(*NofVertices == this->m_NofVertices && Indeces != NULL)
    {           
        if(*directionType == dgc_CounterClockwise)
        {    
            for(i=0; i<this->m_NofVertices; i++)
            {
                this->m_CounterClockwiseIndeces[i] = Indeces[i];
            }      
            return 0;    
        }
        else /*if(*directionType == alg_CounterClockwise)*/
        {
            for(int i=0; i<this->m_NofVertices; i++)
            {
                this->m_ClockwiseIndeces[i] = Indeces[i];
            }      
            return 0;            
        }        
    } 
    else
    {
        return -1;
    }      
}

int CDGCPolytope::setHalfspaces(const int* const NofRows, const int* const NofCols, const double** const A, const double* const b)
{
    if((*NofRows > 0) && (*NofCols > 0) && (A != NULL) && (b != NULL))
    {
        int i = 0;
        int j = 0;
    
        if(this->m_A != NULL)
        {
 			for(i=0; i<this->m_NofRows; i++)
			{
                delete[] this->m_A[i];
            }           
            delete[] this->m_A;
            this->m_A = NULL;
        }
        
        if(this->m_b != NULL)
        {
           delete[] this->m_b;
           this->m_b = NULL; 
        }
    
        this->m_NofRows    = *NofRows;
        this->m_NofColumns = *NofCols;
        
        this->m_A = new double*[this->m_NofRows];
        this->m_b = new double[this->m_NofRows];
        for(i=0; i<this->m_NofRows; i++)
        {
            this->m_b[i] = b[i];
            this->m_A[i] =  new double[this->m_NofColumns];
            for(j=0; j<this->m_NofColumns; j++)
            {
                this->m_A[i][j] = A[i][j];
            }            
            
        }
        return 0;            
    }
    else
    {
        return -1;
    }
}

int CDGCPolytope::setVolume(double Volume)
{
    this->m_Volume = Volume;
    return 0;
}

int CDGCPolytope::getNofRows(void) const
{
	return this->m_NofRows;
}

int CDGCPolytope::getNofColumns(void) const
{
	return this->m_NofColumns;
}

int CDGCPolytope::getA(const int* const NofRows, const int* const NofCols, double** const A) const
{
    if(*NofRows == this->m_NofRows && *NofCols == this->m_NofColumns && A != NULL)
    {
        if(this->m_A != NULL)
        {
	        for(int i=0; i<this->m_NofRows; i++)
	        {
		        for(int j=0; j<this->m_NofColumns; j++)
		        {
			        A[i][j] = this->m_A[i][j];
		        }
	        }
            return 0;        
        }        
        else
        {
            return -2;  /* nothing to return */
        }
    }
    else
    {
        return -1;  /* wrong input -- cannot return A*/
    }
}

int CDGCPolytope::getb(const int* const NofRows, double* const b) const
{
    if(*NofRows == this->m_NofRows && b != NULL)
    {
        if(m_b != NULL)
        {
	       for(int i=0; i<this->m_NofRows; i++)
	       {
		       b[i] =this->m_b[i];
	       }
           return 0;
        }
        else
        {
           return -2; /* nothing to return */
        }        
    }
    else
    {
        return -1;  /* wrong input -- cannot return b*/
    }
}

int CDGCPolytope::getCentroid(const int* const NofDimensions, double* const Centroid) const
{
    if(*NofDimensions == this->m_NofDimensions && Centroid != NULL)
    {
        if(Centroid != NULL)
        {
            for(int i=0; i<this->m_NofDimensions; i++)
            {
                Centroid[i] = this->m_Centroid[i];
            }
            return 0;            
        }
        else
        {
            return -2; /* No centroid to return */
        }
    }
    else
    {
        return -1; /* Wrong input - cannot return centroid */
    } 
}

int CDGCPolytope::setCentroid(const int* const NofDimensions, const double* const Centroid)
{
    if(*NofDimensions == this->m_NofDimensions && Centroid != NULL)
    {
        if(this->m_Centroid == NULL)
        {
           this->m_Centroid = new double[this->m_NofDimensions]; 
        }
        
        for(int i=0; i<this->m_NofDimensions; i++)
        {
            this->m_Centroid[i] = Centroid[i];
        }        
        
        return 0;
    }
    else
    {
        return -1; /* Wrong input - cannot set centroid */
    }
}

int CDGCPolytope::getBoundingBox(const int* const NofDimensions, double* const mins, double* const maxs) const
{
    if(*NofDimensions == this->m_NofDimensions && mins != NULL && maxs != NULL)
    {
        if(this->m_mins !=  NULL && this->m_maxs != NULL)
        {
            for(int i=0; i<this->m_NofDimensions; i++)
            {
                mins[i] = this->m_mins[i];
                maxs[i] = this->m_maxs[i];
            }
            return 0;        
        }
        else
        {
            return -2; /* Not bounding box to return */
        }
    }
    else
    {
        return -1;  /* Wrong input -  cannot return bounding box*/
    }
}

int CDGCPolytope::setBoundingBox(const int* const NofDimensions, const double* const mins, const double* const maxs)
{
    if(*NofDimensions == this->m_NofDimensions && mins != NULL && maxs != NULL)
    {
        if(this->m_mins != NULL)
        {
           delete[] this->m_mins; 
        }
        
        if(this->m_maxs != NULL)
        {
           delete[] this->m_maxs; 
        }
        
        this->m_mins = new double[this->m_NofDimensions];
        this->m_maxs = new double[this->m_NofDimensions];
        for(int i=0; i<this->m_NofDimensions; i++)
        {
           this->m_mins[i] = mins[i];  
           this->m_maxs[i] = maxs[i];  
        }          
        return 0;        
    }   
    else
    {
        return -1; /* Wrong input - cannot set bounding box */    
    } 
}

int CDGCPolytope::IsPointInPolytope(const int* const NofDimensions, const double* const Point) const
{
    int i=0;
    int j=0;
    
    if(*NofDimensions == this->m_NofDimensions && this->m_A != NULL && this->m_b != NULL)
    {
        double* bs = new double[this->m_NofRows];
        for(i=0; i<this->m_NofRows; i++)
        {
            bs[i] = 0.0; 
            for(j=0; j<this->m_NofColumns; j++)
            {
                bs[i] = bs[i] + this->m_A[i][j]*Point[j];
            }                        
        }  
        
        bool IsInPolytope = true;
        for(i=0; i<this->m_NofRows; i++)
        {
            if(bs[i] > this->m_b[i])
            {
                IsInPolytope = false;
                break;      
            } 
        }
        
        if(IsInPolytope == true)
        {
           return 1;  // true
        }
        else
        {
           return 0;  // false
        }
        
        delete[] bs;
    }
    else
    {
        return -1;    // error
    }
}

ostream& operator<<(ostream& os, const CDGCPolytope* const Polytope) 
{
    int i = 0; int j = 0;
    
    os << "NofDimensions = " << Polytope->m_NofDimensions << endl;
    os << "NofPoints     = " << Polytope->m_NofPoints     << endl;
    os << "NofVertices   = " << Polytope->m_NofVertices   << endl;
    os << "NofRows       = " << Polytope->m_NofRows       << endl;
    os << "NofColumns    = " << Polytope->m_NofColumns    << endl;
    os << "Volume        = " << fixed << setprecision(12) << Polytope->m_Volume << endl;
    os << endl;
        
    if(Polytope->m_Points != NULL)
    {
        os << "Points" << endl;
        for(i = 0; i < Polytope->m_NofDimensions; i++)
        {
            for(j = 0; j < Polytope->m_NofPoints; j++)
            {
                os << fixed << setprecision(12) << Polytope->m_Points[i][j] << '\t'; 	
            }
            os << endl;
        }
        os << endl;      
    }
    
    if(Polytope->m_CounterClockwiseIndeces != NULL)
    {
        os << "Indeces" << endl;
        for(j=0; j < Polytope->m_NofVertices; j++)
        {
            os << fixed << setprecision(12) << Polytope->m_CounterClockwiseIndeces[j] << '\t';
        }
        os << endl;
        os << endl;
    }  
        
    if(Polytope->m_Vertices != NULL)
    {    
        os << "Vertices" << endl;
        for(i=0; i < Polytope->m_NofDimensions; i++)
        {   
            for(j=0; j < Polytope->m_NofVertices; j++)
            {
                os << fixed << setprecision(12) << Polytope->m_Vertices[i][j] << '\t';
            }
            os << endl;
        }
        os << endl;    
    }
   
    if(Polytope->m_A != NULL)
    {
        os << "A" << endl;
        for(i=0; i < Polytope->m_NofRows; i++)
        {
            for(j=0; j < Polytope->m_NofColumns; j++)
            {
                os << fixed << setprecision(12) << Polytope->m_A[i][j] << '\t';
            }
            os << endl;
        }
        os << endl;    
    }
    
    if(Polytope->m_b != NULL)
    {   
        os << "b" << endl;
        for(i=0; i < Polytope->m_NofRows; i++)
        {
            os << fixed << setprecision(12) << Polytope->m_b[i] << endl;
        }
        os << endl;
    }
    
    if(Polytope->m_Centroid != NULL)
    {
       os << "Centroid" << endl;
       for(i=0; i < Polytope->m_NofDimensions; i++)
       {   
           os << fixed << setprecision(12) << Polytope->m_Centroid[i] << '\t';
       }
       os << endl;
    }
        
    return os;
}

ostream& operator<<(ostream& os, const CDGCPolytope& Polytope) 
{
    int i = 0; int j = 0;
    
    os << "NofDimensions = " << Polytope.m_NofDimensions << endl;
    os << "NofPoints     = " << Polytope.m_NofPoints     << endl;
    os << "NofVertices   = " << Polytope.m_NofVertices   << endl;
    os << "NofRows       = " << Polytope.m_NofRows       << endl;
    os << "NofColumns    = " << Polytope.m_NofColumns    << endl;
    os << "Volume        = " << fixed << setprecision(12) << Polytope.m_Volume << endl;
    os << endl;
        
    if(Polytope.m_Points != NULL)
    {
        os << "Points" << endl;
        for(i = 0; i < Polytope.m_NofDimensions; i++)
        {
            for(j = 0; j < Polytope.m_NofPoints; j++)
            {
                os << fixed << setprecision(12) << Polytope.m_Points[i][j] << '\t'; 	
            }
            os << endl;
        }
        os << endl;      
    }
    
    if(Polytope.m_CounterClockwiseIndeces != NULL)
    {
        os << "Indeces" << endl;
        for(j=0; j < Polytope.m_NofVertices; j++)
        {
            os << fixed << setprecision(12) << Polytope.m_CounterClockwiseIndeces[j] << '\t';
        }
        os << endl;
        os << endl;
    }  
        
    if(Polytope.m_Vertices != NULL)
    {    
        os << "Vertices" << endl;
        for(i=0; i < Polytope.m_NofDimensions; i++)
        {   
            for(j=0; j < Polytope.m_NofVertices; j++)
            {
                os << fixed << setprecision(12) << Polytope.m_Vertices[i][j] << '\t';
            }
            os << endl;
        }
        os << endl;    
    }
   
    if(Polytope.m_A != NULL)
    {
        os << "A" << endl;
        for(i=0; i < Polytope.m_NofRows; i++)
        {
            for(j=0; j < Polytope.m_NofColumns; j++)
            {
                os << fixed << setprecision(12) << Polytope.m_A[i][j] << '\t';
            }
            os << endl;
        }
        os << endl;    
    }
    
    if(Polytope.m_b != NULL)
    {   
        os << "b" << endl;
        for(i=0; i < Polytope.m_NofRows; i++)
        {
            os << fixed << setprecision(12) << Polytope.m_b[i] << endl;
        }
        os << endl;
    }
    
    if(Polytope.m_Centroid != NULL)
    {
       os << "Centroid" << endl;
       for(i=0; i < Polytope.m_NofDimensions; i++)
       {   
           os << fixed << setprecision(12) << Polytope.m_Centroid[i] << '\t';
       }
       os << endl;
    }
        
    return os;
}
