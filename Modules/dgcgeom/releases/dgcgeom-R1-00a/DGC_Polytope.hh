/*
	DGC_Polytope.hh
    
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	
    Created: Dec. 11, 2006
*/

#ifndef DGC_POLYTOPE_HH
#define DGC_POLYTOPE_HH

#include <iostream>
#include "DGC_Interfaces.hh"

using namespace std;
class CDGCPolytope
{
	friend class CDGCAlgebraicGeometry;
    
	public:
        CDGCPolytope(void);
        CDGCPolytope(const int* const NofDimensions, const int* const NofPoints, const double** const Points);
        CDGCPolytope(const int* const NofDimensions, const int* const NofPoints, const double** const Points, const bool* const SwitchCoordinates);

        CDGCPolytope(const CDGCPolytope& Polytope);
        CDGCPolytope(const CDGCPolytope* const Polytope);
	   ~CDGCPolytope(void);

        CDGCPolytope& operator=(const CDGCPolytope& Polytope);
        CDGCPolytope* const operator=(const CDGCPolytope* const Polytope);
        
	    int getNofDimensions(void) const;
		int getNofPoints(void) const;		
        int getPoints(const int* const NofDimensions, const int* const NofPoints, double** const Points) const;

		int getNofVertices(void) const;
		int getVertices(const int* const NofDimensions, const int* const NofVertices, double** const Vertices) const;
        
        int getIndeces(const DGCDirectionType* const directionType, const int* const NofVertices, int* const Indeces) const;

		int getNofRows(void) const;
		int getNofColumns(void) const;

		int getA(const int* const NofRows, const int* const NofCols, double** const A) const;
		int getb(const int* const NofRows, double* const b) const;

        int getCentroid(const int* const NofDimensions, double* const Centroid) const;
        int getBoundingBox(const int* const NofDimensions, double* const mins, double* const maxs) const;

        double getVolume(void) const;
        int IsPointInPolytope(const int* const NofDimensions, const double* const Point) const;        
        
        friend ostream& operator<<(ostream& os, const CDGCPolytope& Polytope); 
        friend ostream& operator<<(ostream& os, const CDGCPolytope* const Polytope);  
        		
	private:
		int setVertices(const int* const NofDimensions, const int* const NofVertices, const double** const Vertices);
        int setIndeces(const DGCDirectionType* const directionType, const int* const NofVertices, const int* const Indeces);
		int setHalfspaces(const int* const NofRows, const int* const NofCols, const double** const A, const double* const b);
        int setCentroid(const int* const NofDimensions, const double* const Centroid);
        int setBoundingBox(const int* const NofDimensions, const double* const mins, const double* const maxs);
        int setVolume(double Volume);

		int m_NofDimensions;
        
		int m_NofPoints;
        double** m_Points;

		int  m_NofVertices;
        double** m_Vertices;
        
        int* m_ClockwiseIndeces;
        int* m_CounterClockwiseIndeces;
        
		int m_NofRows;
		int m_NofColumns;

		double** m_A;
		double*  m_b;

        double* m_Centroid;

        double* m_mins;
        double* m_maxs;

        double m_Volume;

};
#endif  /* DGC_POLYTOPE_HH */
