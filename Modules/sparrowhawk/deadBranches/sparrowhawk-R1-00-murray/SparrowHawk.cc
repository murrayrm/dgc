#include<signal.h>
#include<curses.h>
#include<iostream>
#include<vector>
#include<string.h>    //for strcasecmp
#include<stdarg.h>
#include<time.h>
#include"dgcutils/DGCutils.hh"

//Include last so we get the correct KEY_xx
#include"SparrowHawk.hh"


using namespace std;

//Create all the keymaps (because sparrow doesn't give us the first key)
#define KEYS(_)							  \
  _(0) _(1) _(2) _(3) _(4) _(5) _(6) _(7) _(8) _(9)		  \
    _(10) _(11) _(12) _(13) _(14) _(15) _(16) _(17) _(18) _(19)	  \
    _(20) _(21) _(22) _(23) _(24) _(25) _(26) _(27) _(28) _(29)	  \
    _(30) _(31) _(32) _(33) _(34) _(35) _(36) _(37) _(38) _(39)	  \
    _(40) _(41) _(42) _(43) _(44) _(45) _(46) _(47) _(48) _(49)	  \
    _(50) _(51) _(52) _(53) _(54) _(55) _(56) _(57) _(58) _(59)	  \
    _(60) _(61) _(62) _(63) _(64) _(65) _(66) _(67) _(68) _(69)	  \
    _(70) _(71) _(72) _(73) _(74) _(75) _(76) _(77) _(78) _(79)	  \
    _(80) _(81) _(82) _(83) _(84) _(85) _(86) _(87) _(88) _(89)	  \
    _(90) _(91) _(92) _(93) _(94) _(95) _(96) _(97) _(98) _(99)	  \
    _(100) _(101) _(102) _(103) _(104) _(105) _(106) _(107) _(108) _(109)	  \
    _(110) _(111) _(112) _(113) _(114) _(115) _(116) _(117) _(118) _(119)	  \
    _(120) _(121) _(122) _(123) _(124) _(125) _(126) _(127) _(128) _(129)	  \
    _(130) _(131) _(132) _(133) _(134) _(135) _(136) _(137) _(138) _(139)	  \
    _(140) _(141) _(142) _(143) _(144) _(145) _(146) _(147) _(148) _(149)	  \
    _(150) _(151) _(152) _(153) _(154) _(155) _(156) _(157) _(158) _(159)	  \
    _(160) _(161) _(162) _(163) _(164) _(165) _(166) _(167) _(168) _(169)	  \
    _(170) _(171) _(172) _(173) _(174) _(175) _(176) _(177) _(178) _(179)	  \
    _(180) _(181) _(182) _(183) _(184) _(185) _(186) _(187) _(188) _(189)	  \
    _(190) _(191) _(192) _(193) _(194) _(195) _(196) _(197) _(198) _(199)	  \
    _(200) _(201) _(202) _(203) _(204) _(205) _(206) _(207) _(208) _(209)	  \
    _(210) _(211) _(212) _(213) _(214) _(215) _(216) _(217) _(218) _(219)	  \
    _(220) _(221) _(222) _(223) _(224) _(225) _(226) _(227) _(228) _(229)	  \
    _(230) _(231) _(232) _(233) _(234) _(235) _(236) _(237) _(238) _(239)	  \
    _(240) _(241) _(242) _(243) _(244) _(245) _(246) _(247) _(248) _(249)	  \
    _(250) _(251) _(252) _(253) _(254) _(255)


#define DO_KEY(x) \
  static int kb_map##x(long) {			\
    SparrowHawk().kb_handle(x);			\
    return 0;					\
  }

#define SET_KEYMAP(x) \
  &kb_map##x,

typedef int (*key_callback_fnc)(long);

#define CREATE_KEYS(keys)				\
  keys(DO_KEY)						\
    static key_callback_fnc key_callback[257]={		\
    keys(SET_KEYMAP)					\
    NULL};
    
CREATE_KEYS(KEYS)


//Dummy variable used for binding in the dd files
long long sparrowhawk=1;

//Comment out to remove cerr outputs
#define LOG_ERRORS

//Extension to sparrow to handle bools
int dd_bool(DD_ACTION action, int id);
int dd_int(DD_ACTION action, int id) { return dd_short(action, id); }

/*
**
** Singleton functionality
** Private constructors of CSparrowHawk prevents multiple instances
**
*/

class forceSparrowHawkInit
{
 public:
  forceSparrowHawkInit() {
    CSparrowHawk::Instance();
  }
};

static forceSparrowHawkInit  my_init;


CSparrowHawk *CSparrowHawk::pInstance=0;

CSparrowHawk &SparrowHawk()
{
  return *CSparrowHawk::Instance();
}

//Get the singleton sparrow instance
CSparrowHawk *CSparrowHawk::Instance()
{
  if(!pInstance) {
    pInstance = new CSparrowHawk();
  }
  return pInstance;
}

static void sign_ctrl_c(int signr)
{
  signal(SIGINT, SIG_DFL);   //Reset handler

  nocbreak();
  echo();
  endwin();
}

CSparrowHawk::CSparrowHawk() : m_running(false)
{
  DGCcreateMutex(&m_log_mutex);

  //Set default settings
  setting.use_tabs=true;
  setting.tabs_top=false;
  setting.tab_seperator=false;
  setting.log_tab=true;
  
  setting.log_nr=false;
  setting.log_time=true;

  m_log_page=0;
  m_log_pos=0;
  m_log_items=0;

  m_scroll_row=0;
  m_scroll_lock=-1;
}

CSparrowHawk::~CSparrowHawk()
{
  stop();

  //Delete owner pages
  for(vector<page>::iterator itr = m_pages.begin(); itr!=m_pages.end(); ++itr) {
    if(itr->should_delete)
      delete []itr->de;
  }
  m_pages.clear();

  //Delete internal pages
  for(vector<internal_page>::iterator itr = m_internal_pages.begin(); itr!=m_internal_pages.end(); ++itr)
    delete [](itr->de);
  m_internal_pages.clear();
  
  //Clear buffert memory
  for(vector<char*>::iterator itr = m_sparrow_bufs.begin(); itr!=m_sparrow_bufs.end(); ++itr) {
    delete [](*itr);
  }
  m_sparrow_bufs.clear();

  //Clear callback function allocations
  for(vector<CallbackData*>::iterator itr = m_callbacks.begin(); itr!=m_callbacks.end(); ++itr)
    delete *itr;
  m_callbacks.clear();

  //Clear all keybind allocations
  for(int i=0; i<256; ++i) {
    for(vector<KeyBind>::iterator itr=m_keybinder[i].begin(); itr!=m_keybinder[i].end(); ++itr)
      delete itr->pc;
    m_keybinder[i].clear();
  }

  DGCdeleteMutex(&m_log_mutex);
}

/*
**
** Running, stopping and operations
**
*/

//Sparrow key binding internals
typedef int (*KM_BINDING)(long);
extern int km_unbound(long);
extern KM_BINDING *dd_keymap;

int kb_quit(long) {
  return DD_EXIT_LOOP;
}

extern int dd_delay;

//Starts the sparrow display (with all needed threads etc.) and returns
bool CSparrowHawk::run()
{
  //Start running sparrow
  if(dd_open()<0) {
    cerr<<"Error intializing sparrow hawk"<<endl;
    exit(-1);
  }

  signal(SIGINT, sign_ctrl_c);

  //Create log tab
  if(setting.log_tab)
    create_log_page();

  //Recreate all fields and add the tabs at the top
  convert_pages();

  if(setting.log_tab) {  //We created a log page
    m_log_page = m_internal_pages.back().de;   //Last page must me log page
  }

  tab_select((long int)0);   //Use the first tab page (might not contain tabs though)

  //Turn of debuging
  dbg_all = 0;

  //Key bindings
  dd_bindkey('q', kb_quit);
  dd_bindkey('Q', kb_quit);


  set_keymap(KEY_S_UP, this, &CSparrowHawk::move_up, 5);
  set_keymap(KEY_S_DOWN, this, &CSparrowHawk::move_down, 5);
  set_keymap(KEY_S_LEFT, this, &CSparrowHawk::move_left, 5);
  set_keymap(KEY_S_RIGHT, this, &CSparrowHawk::move_right, 5);

  set_keymap(KEY_PGUP, this, &CSparrowHawk::scroll_up, 5);
  set_keymap(KEY_PGDN, this, &CSparrowHawk::scroll_down, 5);

  set_keymap(KEY_HOME, this, &CSparrowHawk::move_first);
  set_keymap(KEY_END, this, &CSparrowHawk::move_last);

  set_keymap(KEY_BACKSPACE, this, &CSparrowHawk::kb_back);


  //Map all bound and unbound keys...
  for(int i=0; i<256; ++i) {
    if(!m_keybinder[i].empty() || 
       dd_keymap[i]==&dd_unbound || dd_keymap[i]==&km_unbound) {
      //log("Rebound unbound key %i (0x%x  '%c')", i,i, i>32 ? (char)i : ' ');
      dd_bindkey(i, key_callback[i] );
    } else {
      //log("Key %i (0x%x  '%c') bound to %p", i,i, i>32 ? (char)i : ' ', dd_keymap[i]);
    }
  }

  m_running=true;

  //Decrease the update frequency, to not take to much cpu
  set_update_frequency(10.0);

  log_changed();   //Make sure logging made before sparrowhawk started is correctly displayed


  DGCstartMemberFunctionThread(this, &CSparrowHawk::sparrow_thread);

  return true;
}

void CSparrowHawk::stop()
{
  //Try to stop for 1s
  for(int i=0; i<100 && running(); ++i) {
    DD_EXIT_LOOP;
    if(!running())
      return;
    usleep(10000);
  }
}

bool CSparrowHawk::running()
{
  return m_running;
}

//Change the value of the names string
void CSparrowHawk::set_string(const char *name, const char *str)
{
  if(!running()) {
    display_entry *de = find(name);
    if(!de)
      return;
    de->value = (void*)str;
    return;
  }

  //Find the name in the internal_pages and set initiated to false
  display_entry *de = find_internal(name);

  if(!de)
    return;

  //thread safely copy the string content (always make sure a terminating 0 exists)
  int l = strlen(str);
  if(l>80)
    l=80;
  char *s = (char *) de->value;
  s[l]=0;
  for(int i=l-1; i>=0; --i)
    s[i] = str[i];
  
  de->initialized=0;   //Mark for redrawing
}

/*
**
** Log messages to sparrow display
**
*/

//Log using same format as printf
void CSparrowHawk::log(const char *sz, ...)
{
  static char buf[81];
  va_list lst;

  DGClockMutex(&m_log_mutex);

  buf[0]=0;
  //Print nr/time
  if(setting.log_nr) {
    sprintf(buf, "%i. ", (int)m_log.size()+1);
  }
  if(setting.log_time) {
    time_t t;
    time(&t);
    strftime(&buf[strlen(buf)], 15, "[%H:%M.%S] ", localtime(&t));
  }

  va_start(lst, sz);
  vsnprintf(&buf[strlen(buf)], 81-strlen(buf), sz, lst);
  va_end(lst);

  m_log.push_back( string(buf) );
  if((int)m_log.size() > 10000) {  //Size limit on log items
    if(m_log_pos > 5000)
      m_log_pos=5000;
    m_log_items=5000;

    vector<string> tmp;
    tmp.insert(tmp.begin(), m_log.end()-5000, m_log.end());
    swap(m_log, tmp);
  }

  DGCunlockMutex(&m_log_mutex);

  log_changed();
}

void CSparrowHawk::log(const string &str)
{
  static char buf[81];

  DGClockMutex(&m_log_mutex);

  buf[0]=0;
  //Print nr/time
  if(setting.log_nr) {
    sprintf(buf, "%i. ", (int)m_log.size()+1);
  }
  if(setting.log_time) {
    time_t t;
    time(&t);
    strftime(&buf[strlen(buf)], 15, "[%H:%M.%S] ", localtime(&t));
  }
  
  strncpy(&buf[strlen(buf)], str.c_str(), 81-strlen(buf));
  buf[80]=0;

  m_log.push_back( string(buf) );
  if((int)m_log.size() > 10000) {  //Size limit on log items
    if(m_log_pos > 5000)
      m_log_pos=5000;
    m_log_items=5000;

    vector<string> tmp;
    tmp.insert(tmp.begin(), m_log.end()-5000, m_log.end());
    swap(m_log, tmp);
  }

  DGCunlockMutex(&m_log_mutex);

  log_changed();
}

void CSparrowHawk::clear_log()
{
  m_log.clear();
  log_changed();
}

/*
**
** Handle pages, this only has effect before sparrow is started
**
*/

//Add a page to the current display
void CSparrowHawk::add_page(DD_IDENT *sparrow_page, const char *name)
{
  if(!verify_stopped())
    return;

  page p;
  p.name = name;
  p.de = sparrow_page;
  p.should_delete = false;

  m_pages.push_back(p);
}


//rebind variables
void CSparrowHawk::rebind(const char *name, bool *ptr)
{
  if(!verify_stopped())
    return;

  display_entry *de = find(name);
  if(!de)
    return;

  //Set the pointer (and other stuff) correctly
  de->value = ptr;
  de->function = dd_bool;  
}

void CSparrowHawk::rebind(const char *name, int *ptr)
{
#ifdef ORIG
  if(!verify_stopped())
    return;
#endif

  display_entry *de = find(name);
  if(!de)
    return;

  //Set the pointer (and other stuff) correctly
  de->value = ptr;
}

void CSparrowHawk::rebind(const char *name, double *ptr)
{
  if(!verify_stopped())
    return;

  display_entry *de = find(name);
  if(!de)
    return;

  //Set the pointer (and other stuff) correctly
  de->value = ptr;
}

//Set properties
void CSparrowHawk::set_readonly(const char *name, bool readonly)
{
  set_readonly( find(name), readonly );
}

void CSparrowHawk::set_readonly(display_entry *de, bool readonly)
{
  if(!de)
    return;
  de->selectable = (readonly ? 0 : 1);
}


//Returns entry from name
display_entry *CSparrowHawk::find(const char *name)
{
  //Go through the table used by sparrow and return index
  string marked_name = string("D(") + name + ")";   //The marked up name actually used

  for(vector<page>::iterator itr = m_pages.begin(); itr!=m_pages.end(); ++itr) {
    //Go through all entries in this page
    display_entry *item = itr->de;
    for(;item->value!=NULL; ++item) {
      //Normal item
      if(marked_name == item->varname || strcmp(name,item->varname)==0)
	return item;
      //Button
      if(item->callback==NULL) { // && item->function==dd_label) {
	if(marked_name==(char*)item->value || strcmp(name, (char*)item->value)==0)
	  return item;
      }
    }
  }
  
#ifdef LOF_ERRORS
  cerr<<"Unable to find variable '"<<name<<"' in sparrow display"<<endl;
#endif
  log("Unable to find variable '%s' in sparrow display", name);
  return NULL;
}


/*
**
** Dynamically create (/change) pages
**
*/

//Creates holders for items in a page
display_entry *CSparrowHawk::create_page(const char *name, int items)
{
  if(!verify_stopped())
    return NULL;

  page p;
  p.name = name;
  p.de = new display_entry[items+1];
  make_null(p.de + items);
  p.should_delete = true;

  m_pages.push_back(p);

  return p.de;
}

void CSparrowHawk::make_label(display_entry *de, int row, int col, const char *str, int max_len)
{
  if(!verify_stopped())
    return;

  de->row=row;
  de->col=col;
  de->value = (void*)str;
  de->length=max_len;

  de->function = dd_label; de->format="NULL";        de->current=(char*)NULL;
  de->selectable = 0;      de->callback = dd_nilcbk; de->userarg=0;
  de->foreground = 0;      de->background = 0;       de->type = Label;
  de->varname[0]=0;
}

void CSparrowHawk::make_button(display_entry *de, int row, int col, const char *str, int (*callback)(long), long user_arg, int max_len)
{
  if(!verify_stopped())
    return;

  de->row=row;
  de->col=col;
  de->value = (void*)str;
  de->selectable = 1;
  de->callback = callback;
  de->userarg=user_arg;
  de->type = Button;
  de->length=max_len;

  de->function = dd_label; de->format="NULL";        de->current=(char*)NULL;
  de->foreground = 0;      de->background = 0;
  de->varname[0]=0;
}

void CSparrowHawk::make_null(display_entry *de)
{
  if(!verify_stopped())
    return;

  de->row=0;         de->col=0;             de->value=0;
  de->length=0;      de->function=0;        de->format=0;
  de->current=0;     de->selectable=0;      de->callback=0;
  de->userarg=0;     de->foreground=0;      de->background=0;
  de->type=(display_type)0;                 de->varname[0]=0;
}

/*
**
** Make item to display variable
**
*/

//Function to create a display of a variable, needs one function for every type
//  wherefore macros are used to generate the functions
#define DO_MAKE_DISPLAY(x)						\
  void CSparrowHawk::make_display(display_entry *de, int row, int col, x *pval, const char *format) { \
    if(!verify_stopped())						\
      return;								\
									\
    de->row=row;							\
    de->col=col;							\
    de->value = (void*)pval;						\
    de->function = dd_##x;						\
    de->format=(char*)format;						\
    de->current=(char*)get_buf(sizeof(x));				\
    de->selectable = 0;      de->callback = dd_nilcbk; de->userarg=0;	\
    de->foreground = 0;      de->background = 0;       de->type = Data;	\
    de->varname[0]=0; de->length=-1;					\
  }

DO_MAKE_DISPLAY(bool);
DO_MAKE_DISPLAY(int);
DO_MAKE_DISPLAY(double);

/*
**
** Make item to display and edit variable
**
*/

//Function to create a display of a variable, needs one function for every type
//  wherefore macros are used to generate the functions
#define DO_MAKE_EDIT(x)							\
  void CSparrowHawk::make_edit(display_entry *de, int row, int col, x *pval, const char *format) { \
    if(!verify_stopped())						\
      return;								\
									\
    de->row=row;							\
    de->col=col;							\
    de->value = (void*)pval;						\
    de->function = dd_##x;						\
    de->format=(char*)format;						\
    de->current=(char*)get_buf(sizeof(x));				\
    de->selectable = 1;							\
    de->callback = dd_nilcbk; de->userarg=0;				\
    de->foreground = 0;      de->background = 0;       de->type = Data;	\
    de->varname[0]=0; de->length=-1;					\
  }

DO_MAKE_EDIT(bool);
DO_MAKE_EDIT(int);
DO_MAKE_EDIT(double);


//Set color number (0-7 vga colors)
void CSparrowHawk::set_fg_color(display_entry *de, int col)
{
  if(!verify_stopped())
    return;

  de->foreground = col;
}

//Set color number (0-7 vga colors)
void CSparrowHawk::set_bg_color(display_entry *de, int col)
{
  if(!verify_stopped())
    return;

  de->background = col;
}


//Change color (possible also at runtime)
void CSparrowHawk::set_fg_color(const char *name, int col)
{
  if(!running()) {
    display_entry *de = find(name);
    if(de)
      set_fg_color(de, col);
    return;
  }

  display_entry *de = find_internal(name);
  if(!de)
    return;

  de->foreground = col;
  de->initialized = 0;
}

void CSparrowHawk::set_bg_color(const char *name, int col)
{
  if(!running()) {
    display_entry *de = find(name);
    if(de)
      set_fg_color(de, col);
    return;
  }

  display_entry *de = find_internal(name);
  if(!de)
    return;

  de->foreground = col;
  de->initialized = 0;
}


/*
**
** Callback functions
**
*/

//Handles callbacks w callback data
int CSparrowHawk::do_callback(long arg)
{
  CallbackData *pData = (CallbackData *)arg;

  if(pData->pcallback)
    (*pData->pcallback)();

  return 0;
}

/*
**
** Functions to sit inbetween sparrow interface and sparrow handler functions to
** insert code for getter/setter logic
**
*/

//General functions to handle the getter/setter interface (calles the native
//  sparrow functions eventually), needs one function for every type
//  wherefore macros are used to generate the functions
#define DO_FNC(x) \
  int CSparrowHawk::do_##x(DD_ACTION action, int id) {			\
    DD_IDENT *dd = ddtbl + id;						\
									\
    if(dd->callback!=do_callback)     /*No hooks in place*/		\
      return ::dd_##x(action, id);					\
    									\
    CallbackData * cd = (CallbackData*)dd->userarg;			\
    									\
    /*Swap variables out */						\
    x *org_value = (x *)dd->value;					\
    x tmp;								\
    									\
    if((action==Update || action==Refresh) && cd->pgetter) {   /*Retreive new value*/ \
      (*cd->pgetter)( (void*)&tmp );					\
      dd->value = (x*)&tmp;						\
    }									\
    									\
    if(action==Input && cd->psetter) {   /*We have a setter function, use it */ \
      /*Swap variables */						\
      dd->value = (void*)&tmp;						\
    }									\
    									\
    int ret = ::dd_##x(action, id);     /*Run real sparrow function */	\
    									\
    if((action==Update || action==Refresh) && cd->pgetter) {   /*Rest pointer*/ \
      dd->value = org_value;						\
    }									\
    									\
    if(action==Input && cd->psetter) {   /*We have a setter function, use it*/ \
      /*Swap variables back */						\
      dd->value = org_value;						\
      (*cd->psetter)((void*)dd->value, (void*)&tmp);			\
    }									\
    									\
    return ret;								\
  }

DO_FNC(bool);
DO_FNC(int);
DO_FNC(double);


/*
**
** Set keyboard mapping
**
*/

void CSparrowHawk::set_final_keymap(int keycode, CSparrowHawkCallback *pc)
{
  //Make sure we capture the first char in the bind
  if(running())
    dd_bindkey(keycode&0xff, key_callback[keycode&0xff] );

  m_keybinder[keycode&0xff].push_back( KeyBind(keycode, pc) );
}

void CSparrowHawk::set_final_keymap(const char *page, int keycode, CSparrowHawkCallback *pc)
{
  //Find the page
  int pagenr = page_index(page);

  if(pagenr==-1) {
    log("Unable to find page %s to bind key to", page);
    return;
  }
    
  //Make sure we capture the first char in the bind
  dd_bindkey(keycode&0xff, key_callback[keycode&0xff] );

  m_keybinder[keycode&0xff].push_back( KeyBind(keycode, pc, pagenr) );
}


/*
**
** Set the notification function
**
*/

void CSparrowHawk::set_final_notify(display_entry *de, CSparrowHawkCallback *pc)
{
  if(!verify_stopped())
    return;

  if(de->callback != CSparrowHawk::do_callback) {  //Create new callback hook
    CallbackData *cd = new CallbackData();
    cd->pcallback = pc;
    cd->psetter = 0;
    cd->pgetter = 0;
    m_callbacks.push_back(cd);
    de->callback = CSparrowHawk::do_callback;
    de->userarg = (long)cd;
  } else {
    CallbackData *cd = (CallbackData*)de->userarg;
    if(cd->pcallback)
      delete cd->pcallback;
    cd->pcallback = pc;
  }
}


/*
**
** Set the setter function
**
*/
void CSparrowHawk::set_final_setter(display_entry *de, CSparrowHawkSetter *ps)
{
  if(de->callback != CSparrowHawk::do_callback) {  //Create new callback hook
    CallbackData *cd = new CallbackData();
    cd->pcallback = new CSparrowHawkCallbackFnc1<int (*)(long), long>( *de->callback, de->userarg );
    cd->psetter = ps;
    cd->pgetter = 0;
    m_callbacks.push_back(cd);
    de->callback = CSparrowHawk::do_callback;
    de->userarg = (long)cd;
  } else {
    CallbackData *cd = (CallbackData*)de->userarg;
    if(cd->psetter)
      delete cd->psetter;
    cd->psetter = ps;
  }
}

//General functions to set the setters, needs one function for every type
//  wherefore macros are used to generate the functions
#define DO_SETTER(x)							\
  template<>								\
  void CSparrowHawk::set_setter_t<x>(display_entry *de, CSparrowHawkSetterBT<x> *ps) \
  {									\
    if(!verify_stopped())						\
      return;								\
									\
    set_final_setter(de, ps);						\
									\
    /*Change parsing function */					\
    if(de->function != CSparrowHawk::do_##x) {				\
      de->function = CSparrowHawk::do_##x;				\
      if(((CallbackData*)de->userarg)->pgetter) {			\
	/*If there existed a getter, it was of the wrong type*/		\
	delete ((CallbackData*)de->userarg)->pgetter;			\
	cerr<<"Different type of setter and getter, removing getter"<<endl; \
      }									\
      ((CallbackData*)de->userarg)->pgetter = 0;			\
    }									\
  }

DO_SETTER(bool);
DO_SETTER(int);
DO_SETTER(double);


/*
**
** Set the getter function
**
*/
void CSparrowHawk::set_final_getter(display_entry *de, CSparrowHawkGetter *pg)
{
  if(de->callback != CSparrowHawk::do_callback) {  //Create new callback hook
    CallbackData *cd = new CallbackData();
    cd->pcallback = new CSparrowHawkCallbackFnc1<int (*)(long),long>( de->callback, de->userarg );
    cd->psetter = 0;
    cd->pgetter = pg;
    m_callbacks.push_back(cd);
    de->callback = CSparrowHawk::do_callback;
    de->userarg = (long)cd;
  } else {
    CallbackData *cd = (CallbackData*)de->userarg;
    if(cd->pgetter)
      delete cd->pgetter;
    cd->pgetter = pg;
  }
}

//General functions to set the setters, needs one function for every type
//  wherefore macros are used to generate the functions
#define DO_GETTER(x)							\
  template<>								\
  void CSparrowHawk::set_getter_t<x>(display_entry *de, CSparrowHawkGetterBT<x> *pg) \
  {									\
    if(!verify_stopped())						\
      return;								\
									\
    set_final_getter(de, pg);						\
									\
    /*Change parsing function*/						\
    if(de->function != CSparrowHawk::do_##x) {				\
      de->function = CSparrowHawk::do_##x;				\
      if(((CallbackData*)de->userarg)->psetter) {			\
	/*If there existed a setter, it was of the wrong type */	\
	delete ((CallbackData*)de->userarg)->psetter;			\
	cerr<<"Different type of getter and setter, removing setter"<<endl; \
      }									\
      ((CallbackData*)de->userarg)->psetter = 0;			\
    }									\
  }

DO_GETTER(bool);
DO_GETTER(int);
DO_GETTER(double);


/*
**
** Generic helper functions
**
*/

void CSparrowHawk::create_log_page()
{
  display_entry *dp;

  //Allocate memory for the page
  m_log_rows = (dd_rows-1) - calc_tab_rows();
  dp = new display_entry[m_log_rows+5];  //Need rows + 2 counters + 2 texts + term null

  //Create the page
  page p;
  p.name = "Status";
  p.de = dp;
  p.should_delete = true;
  m_pages.push_back(p);   //Add page her to get tab rows calculations correctly
  
  //Decrease m_status_rows with none usable rows such as headers
  m_log_rows-=2;       //Need top and bottom seperators
  if(setting.use_tabs) {
    if(setting.tab_seperator)  //Can reuse this seperator
      ++m_log_rows;
  }
  
  display_entry *de = dp;

  //Fill the data, the first m_status_rows items are the "texts"
  int addrow=2;
  if(setting.use_tabs && setting.tabs_top && setting.tab_seperator)
    --addrow;    //Don't need the top seperator
  for(int i=0; i<m_log_rows; ++i) {
    make_label(de, addrow, 1, get_buf(dd_cols+1), dd_cols);
    ++de;
    ++addrow;
  }

  //Make seperators and counters
  if(setting.use_tabs && !setting.tabs_top && setting.tab_seperator) {
    //Can't put it in bottom seperator, use top one
    addrow=1;
  }
  make_label(de, addrow, 1, "-------------------------------------------------------------");
  ++de;
  make_display(de, addrow, 63, &m_log_pos, "%d ");
  ++de;
  make_label(de, addrow, 70, "/");
  ++de;
  make_display(de, addrow, 72, &m_log_items);
  ++de;
  make_label(de, addrow, 79, "--");
  ++de;

  if(!setting.use_tabs || !setting.tab_seperator) {
    //Add other seperator
    make_label(de, 1, 1, "------------------------------------ Log ---------------------------------------");
    ++de;
  }
  
  make_null(de);

  //Set keymapping
  set_keymap("Status", KEY_PGDN, this, &CSparrowHawk::log_pgdn);
  set_keymap("Status", KEY_PGUP, this, &CSparrowHawk::log_pgup);

  set_keymap("Status", KEY_HOME, this, &CSparrowHawk::log_home);
  set_keymap("Status", KEY_END, this, &CSparrowHawk::log_end);
}

//Rows used by tabs and seperators
int CSparrowHawk::calc_tab_rows()
{
  //Calculate rows needed for tabs
  int rows=0;

  if(setting.use_tabs) {
    rows=1;
    if(setting.tab_seperator)
      rows=2;

    int col=3 + 15;   //initial spacing + space for "Update: xx hz"
    for(vector<page>::iterator itr = m_pages.begin(); itr!=m_pages.end(); ++itr) {
      int l = strlen(itr->name);
      if(l>30)
	l=30;

      if(col + l >= 79) {
	++rows;
	col = 3;
      }
      col += l+2;
    }
  }

  return rows;
}

void CSparrowHawk::convert_pages()
{
  int tab_rows = calc_tab_rows();

  //Create copies of all pages with the right number of elements
  for(vector<page>::iterator itr = m_pages.begin(); itr!=m_pages.end(); ++itr) {
    int cnt=0;
    display_entry *de_old = itr->de;
    while(de_old[cnt].value!=NULL)
      ++cnt;
    ++cnt;     //Have to count the terminating null

    //Create a new table with cnt+tab_rows*2+1 +2 fields
    internal_page p;
    p.de = new display_entry[cnt+m_pages.size()+2*tab_rows+1 +2];
    p.rows=0;

    //Add this page to internal pages
    m_internal_pages.push_back(p);
  }

  //Add tabs and copy data (has to be 2 step due to links having to be to new pages)
  for(int p=0; p<(int)m_internal_pages.size(); ++p) {
    display_entry *de_old = m_pages[p].de;
    display_entry *de = m_internal_pages[p].de;

    //Add normal items
    int j;
    for(j=0; de_old[j].value!=NULL; ++j, ++de) {
      *de = de_old[j];
      if(setting.tabs_top)
	de->row += tab_rows;
    }
    display_entry *de_null = de_old+j;  //Get pointer to null element
    
    if(!setting.use_tabs) {   //Not using tabs, process next page
      *de = *de_null;   //Set null terminator
      continue;
    }
    
    //Calculate where to start adding rows
    int add_row=1;
    if(!setting.tabs_top) {
      for(int j=0; de_old[j].value!=NULL; ++j) {
	if(de_old[j].row>add_row)
	  add_row = de_old[j].row;
      }
      ++add_row;
    }

  //Add the tabs
    
    //Add seperator
    if(setting.tab_seperator) {
      int row=0;
      if(setting.tabs_top) {
	row=add_row+tab_rows-1;
      } else {
	row=add_row;
      }
      make_label(de, row, 1, "+------------------------------------------------------------------------------+");
      ++de;
    }

    int col;
    if(setting.tabs_top) {
      col = 3;
    } else {
      col = 1000;     //Force a new line straight away
      if(!setting.tab_seperator)
	--add_row;   //Want to print the new line | markers, but dont want to change row
    }
    for(int p2=0; p2<(int)m_internal_pages.size(); ++p2) {
      int l = strlen(m_pages[p2].name);
      if(l>30)  //Max allowed tab length
	l=30;
      
      if(col + l >= 79) {   //Need to add a new line
	++add_row;
	col = 3;

	make_label(de, add_row, 1, "|");
	++de;
    
	make_label(de, add_row, 80, "|");
	++de;
      }

      if(p==p2) {  //this is the seleted page
	make_label(de, add_row, col, m_pages[p2].name, l);
	set_fg_color(de, BLUE);
	++de;
      } else {
	make_button(de, add_row, col, m_pages[p2].name, CSparrowHawk::tab_select, p2, l);
	++de;
      }

      col += l+2;
    }

    //Add update frequency option
    if(col + 15 >= 79) {   //Need to add a new line
      ++add_row;
      col = 3;
    }

    //Right justify
    make_label(de++, add_row, 79-7-8, "Update: ");
    make_edit(de, add_row, 79-7, (double*)&sparrowhawk, "%4.2f Hz");
    set_setter(de, this, &CSparrowHawk::setter_frequency);
    set_getter(de, this, &CSparrowHawk::get_update_frequency);
    ++de;

    //Set null terminator
    *de = *de_null;
  }

  //Update the redraw functions to support scrolling and count rows + dynamic string
  for(int p=0; p<(int)m_internal_pages.size(); ++p) {
    display_entry *de = m_internal_pages[p].de;
    int max_row=0;

    for(;de->value!=NULL; ++de) {
      if(de->function == dd_string && de->varname[0]=='D' && de->varname[1]=='(') {
	//Dynamic variable, allocate memory
	char *old = (char*)de->value;
	de->value = (void*)get_buf(81);   //Never more than this
	//Copy old value
	if((int)old != (int)sparrowhawk) {   //It's not the default dynamic build construct
	  strncpy((char*)de->value, old, 80);
	  ((char *)de->value)[80] = 0;
	} else
	  ((char *)de->value)[0] = 0;
	de->current = (char*)81;
      }
      if(de->row>max_row)
	max_row=de->row;
      CallbackData *cd;
      if(de->callback != CSparrowHawk::do_callback) {
	//Create new callback hook
	cd = new CallbackData();
	cd->pcallback = new CSparrowHawkCallbackFnc1<int (*)(long), long>( *de->callback, de->userarg );
	m_callbacks.push_back(cd);
	de->callback = CSparrowHawk::do_callback;
	de->userarg = (long)cd;
      } else {
	//Use old hook
	cd = (CallbackData*)de->userarg;
      }

      cd->pfunction = de->function;
      de->function = do_static_scroll_function;
    }

    m_internal_pages[p].rows = max_row;
  }
}

//Makes sure we are not running
bool CSparrowHawk::verify_stopped()
{
  if(!running())
    return true;

#ifdef LOG_ERRORS
  cerr<<"Page binding or other modification is not allowed while running"<<endl;
#endif

  log("Page binding or other modification is not allowed while running");

  return false;
}

//Return buffert space
char *CSparrowHawk::get_buf(int size)
{
  if(m_sparrow_bufs.empty() || m_sparrow_used+size>sparrow_buf_grow) {
    //Need to alloc new buffer
    char *p;
    if(size>sparrow_buf_grow)
      p = new char[size];
    else
      p = new char[sparrow_buf_grow];
    m_sparrow_bufs.push_back(p);
    m_sparrow_used=size;
    return p;
  }

  //Can use current buffer
  m_sparrow_used += size;

  return m_sparrow_bufs.back() + (m_sparrow_used-size);
}

//Returns entry from name
display_entry *CSparrowHawk::find_internal(const char *name)
{
  //Find the name in the internal_pages

  string marked_name = string("D(") + name + ")";   //The marked up name actually used

  for(vector<internal_page>::iterator itr = m_internal_pages.begin(); itr!=m_internal_pages.end(); ++itr) {
    //Go through all entries in this page
    display_entry *item = itr->de;
    for(;item->value!=NULL; ++item) {
      if(marked_name == item->varname || strcmp(name, item->varname)==0) {
	return item;
      }
    }
  }
  
  log("find_internal: Unable to find variable '%s' in sparrow display at runtime", name);
  return NULL;
}

//Get current internal page
CSparrowHawk::internal_page *CSparrowHawk::get_cur_page()
{
  vector<internal_page>::iterator itr;
  for(itr=m_internal_pages.begin(); itr!=m_internal_pages.end(); ++itr) {
    if(itr->de == ddtbl)
      return &(*itr);
  }
  return NULL;
}


int CSparrowHawk::page_index(const char *name)
{
  //Find the page
  for(int pagenr=0; pagenr<(int)m_pages.size(); ++pagenr) {
    if(strcmp(m_pages[pagenr].name,name)==0)
      return pagenr;;
  }
  return -1;
}



/*
**
** Tab and page scrolling helpers
**
*/
void CSparrowHawk::tab_select(const char *name)
{
  int idx = page_index(name);
  if(idx==-1) {
    log("tab_select: can't find page '%s'", name);
    return;
  }
  tab_select(idx);
}

int CSparrowHawk::tab_select(long id)
{
  Instance()->m_scroll_row = 0;  //Reset scrolling
  Instance()->m_scroll_lock = -1;

  //Fool dd_usetbl to generate movement commands for all cells
  int true_rows = dd_rows;
  dd_rows = SparrowHawk().m_internal_pages[id].rows+1;
  int ret = dd_usetbl( SparrowHawk().m_internal_pages[id].de );
  dd_rows = true_rows;

  return ret;
}

void CSparrowHawk::tab_select_prev()
{
  //Find the page
  for(int i=0; i<(int)SparrowHawk().m_internal_pages.size(); ++i) {
    if(SparrowHawk().m_internal_pages[i].de == ddprv) {
      SparrowHawk().tab_select( i );
      return;
    }
  }

  return;
}

int CSparrowHawk::do_static_scroll_function(DD_ACTION action, int id)
{
  return Instance()->do_scroll_function(action, id);
}

int CSparrowHawk::do_scroll_function(DD_ACTION action, int id)
{
  DD_IDENT *dd = ddtbl + id;
  int ret=0;

  if(dd_cur==id && dd_cur!=m_scroll_lock) {
    //Drawing selection, scroll it visible
    if(dd->row <= m_scroll_row) {
      //Scroll up to visible
      scroll_up(m_scroll_row - dd->row +5);
    } else if(dd->row >= m_scroll_row + dd_rows) {
      //Scroll down to visible
      scroll_down(dd->row - (m_scroll_row + dd_rows) +5);
    }
  }

  dd->row -= m_scroll_row;
  
  if(((CallbackData*)dd->userarg)->pfunction) {
    //Run real update function
    if(action==Update || action==Refresh) {
      //Screen update only if cell is visible
      if(dd->row>=1 && dd->row<dd_rows)  //Bottom row is input
	ret = (((CallbackData*)dd->userarg)->pfunction)(action, id);
    } else
      ret = (((CallbackData*)dd->userarg)->pfunction)(action, id);
  }
  dd->row += m_scroll_row;

  return ret;
}

//Scroll window
void CSparrowHawk::scroll_up(int rows)
{
  internal_page *p = get_cur_page();
  if(!p)
    return;

  if(m_scroll_row==0)  //Nothing to scroll
    return;

  m_scroll_row-=rows;
  if(m_scroll_row<0)
    m_scroll_row=0;

  //Reset scroll lock
  m_scroll_lock = -1;

  //Change current selection to the scrolled window
  if(dd_cur!=-1 && p->de[dd_cur].row >= m_scroll_row+dd_rows) {
    //Move down from this till we have a selection
    int id = dd_cur;
    while(p->de[id].row <= m_scroll_row && id!=-1)
      id = p->de[id].up;

    if(id!=-1 && p->de[id].row <= m_scroll_row)
      id=-1;
    if(id!=-1 && p->de[id].row >= m_scroll_row+dd_rows)
      id=-1;

    if(id==-1) {
      //Went to far... select first valid on screen
      int best_id=-1;
      for(int i=0; p->de[i].value!=NULL; ++i) {
	if(!p->de[i].selectable)
	  continue;
	if(p->de[i].row>=m_scroll_row+dd_rows)
	  continue;

	if(best_id==-1)
	  best_id = i;
	if(p->de[i].row>p->de[best_id].row)
	  best_id = i;
	if(p->de[i].row==p->de[best_id].row && p->de[i].col<p->de[best_id].col)
	  best_id = i;
      }

      if(best_id!=-1 && p->de[best_id].row > m_scroll_row)
	id=best_id;    //best_id exist and on screen
    }

    if(id!=-1) {
      dd_cur=id;
    } else {
      //Couldn't find a control in the new display, so dont force a scroll
      m_scroll_lock = dd_cur;
    }
  }

  dd_redraw(0);
}

void CSparrowHawk::scroll_down(int rows)
{
  internal_page *p = get_cur_page();
  if(!p)
    return;

  if(m_scroll_row==p->rows-dd_rows+1)
    return;
  
  if(m_scroll_row==0 && p->rows<dd_rows-1)  //Cant scroll
    return;

  m_scroll_row+=rows;
  if(m_scroll_row>p->rows-dd_rows+1)
    m_scroll_row=p->rows-dd_rows+1;
  if(m_scroll_row<0)
    m_scroll_row=0;

  //Reset scroll lock
  m_scroll_lock = -1;

  //Change current selection to the scrolled window
  if(dd_cur!=-1 && p->de[dd_cur].row <= m_scroll_row) {
    //Move down from this till we have a selection
    int id = dd_cur;
    while(p->de[id].row <= m_scroll_row && id!=-1)
      id = p->de[id].down;

    if(id!=-1 && p->de[id].row <= m_scroll_row)
      id=-1;
    if(id!=-1 && p->de[id].row >= m_scroll_row+dd_rows)
      id=-1;

    if(id==-1) {
      //Went to far... select first valid on screen
      int best_id=-1;
      for(int i=0; p->de[i].value!=NULL; ++i) {
	if(!p->de[i].selectable)
	  continue;
	if(p->de[i].row<=m_scroll_row)
	  continue;

	if(best_id==-1)
	  best_id = i;
	if(p->de[i].row<p->de[best_id].row)
	  best_id = i;
	if(p->de[i].row==p->de[best_id].row && p->de[i].col<p->de[best_id].col)
	  best_id = i;
      }

      if(best_id!=-1 && p->de[best_id].row < m_scroll_row+dd_rows)
	id=best_id;    //best_id exist and on screen
      else
	id=-1;
    }

    if(id!=-1) {
      dd_cur=id;
    } else {
      //Couldn't find a control in the new display, so dont force a scroll
      m_scroll_lock = dd_cur;
    }
  }

  dd_redraw(0);
}

//Move cursor
void CSparrowHawk::move_down(int steps)
{
  while(dd_down(0) && steps>1)
    --steps;
}

void CSparrowHawk::move_left(int steps)
{
  while(dd_left(0) && steps>1)
    --steps;
}

void CSparrowHawk::move_right(int steps)
{
  while(dd_right(0) && steps>1)
    --steps;
}

void CSparrowHawk::move_up(int steps)
{
  while(dd_up(0) && steps>1)
    --steps;
}

void CSparrowHawk::move_first()
{
  int cur = dd_cur;

  while(dd_up(0) && cur!=dd_cur)
    cur = dd_cur;
}

void CSparrowHawk::move_last()
{
  int cur = dd_cur;

  while(dd_down(0) && cur!=dd_cur)
    cur = dd_cur;
}


/*
**
** Log helper functions
**
*/

//Log has changed
void CSparrowHawk::log_changed()
{
  if(!m_log_page) {
    //cerr<<"No log page!!!"<<endl;
    return;
  }

  DGClockMutex(&m_log_mutex);

  if(m_log_items != (int)m_log.size()) {   //Items have been added
    if(m_log_pos==m_log_items || m_log_items<m_log_rows) {  //Keep tracking the bottom
      m_log_pos = (int)m_log.size();

      m_log_items = (int)m_log.size();
      DGCunlockMutex(&m_log_mutex);
      log_redraw();
    } else {
      m_log_items = (int)m_log.size();
      DGCunlockMutex(&m_log_mutex);
    }

  } else
    DGCunlockMutex(&m_log_mutex);
  
}

//Force a redraw of the log
void CSparrowHawk::log_redraw()
{
  if(!running())   //Have to be running to redraw
    return;

  if(!m_log_page)
    return;

  DGClockMutex(&m_log_mutex);

  if(m_log_pos>m_log_items)
    m_log_pos = m_log_items;

  if(m_log_pos<m_log_rows)
    m_log_pos=m_log_rows;

  //Update all display entries for the logs
  for(int i=0; i<m_log_rows; ++i) {
    int idx = m_log_pos+i-m_log_rows;

    if(idx>=0 && idx<(int)m_log.size()) {
      strcpy((char*)m_log_page[i].value, m_log[idx].c_str());
    } else {
      strcpy((char*)m_log_page[i].value, "");
    }
    
    //Fill w empty space to ensure erasing
    for(int j=strlen((char*)m_log_page[i].value); j<dd_cols; ++j)
      ((char*)m_log_page[i].value)[j] = ' ';

    m_log_page[i].initialized=0;
  }

  DGCunlockMutex(&m_log_mutex);
}

//Page up
void CSparrowHawk::log_pgup()
{
  if(!m_log_page)
    return;

  m_log_pos -= m_log_rows/2;

  log_redraw();
}

//Page down
void CSparrowHawk::log_pgdn()
{
  if(!m_log_page)
    return;

  m_log_pos += m_log_rows/2;

  log_redraw();
}

//Home
void CSparrowHawk::log_home()
{
  if(!m_log_page)
    return;

  m_log_pos = 0;

  log_redraw();
}

//End
void CSparrowHawk::log_end()
{
  if(!m_log_page)
    return;

  m_log_pos = m_log_items;

  log_redraw();
}

/*
**
** Update frequency counter
**
*/
void CSparrowHawk::set_update_frequency(double hz)
{
  if(hz<0.01)
    hz=.01;
  if(hz>40)
    hz=40;

  dd_delay = (int)(1000000/hz);
}

double CSparrowHawk::get_update_frequency()
{
  if(dd_delay==0)      //Shouldn't happen.... makes us fall straight through all delays
    return 10000000.0;

  return 1000000.0/dd_delay;
}

void CSparrowHawk::setter_frequency(double *p, double val)
{
  set_update_frequency(val);
}

/*
**
** key bounds
**
*/

int CSparrowHawk::kb_handle(long k)
{
  unsigned long key = (unsigned long)k;

  //Get current page nr
  int page;
  for(int i=0; i<(int)SparrowHawk().m_internal_pages.size(); ++i) {
    if(SparrowHawk().m_internal_pages[i].de == ddtbl) {
      page = i;
      break;
    }
  }

  //Check the keymap
  vector<KeyBind> &binds = m_keybinder[key];
  vector<KeyBind>::iterator itr;
  vector<KeyBind>::iterator match_itr;

  if(binds.empty())
    return kb_unbound(key);

  //Get current position in keycode
  int pos=0;
  while(pos<4 && ((key>>(8*pos))&0xff))
    ++pos;

  while(1) {
    int matches=0;
    bool final_match=false;
    unsigned int mask = 0xffffffff >> (8*(4-pos));
    unsigned int antimask = 0xffffffff << (8*pos);

    if(pos==4)   //shift doesnt seem to work here
      antimask=0;

    //log("Searching binds  %x   mask:%x  anti:%x  page:%i",key, mask, antimask, page);

    match_itr=binds.end();

    //Check if we need more keycodes
    for(itr = binds.begin(); itr!=binds.end(); ++itr) {
      //log("  Checking(%i) %x", itr->page, itr->code);
      if((itr->page==-1 || itr->page==page) 
	 && (itr->code & mask) == key) {
	//This key is terminated and matches
	if(match_itr==binds.end() || match_itr->code!=itr->code) {
	  if((itr->code & antimask) == 0)
	    final_match=true;
	  //new match
	  ++matches;
	  match_itr = itr;
	}
      }
    }

    if(matches==0) {  //Nothing matches anymore
      log("Unbound key 0x%x (%i)", key, key);
      return 0;
    }

    if(final_match) {
      //Run first match
      for(itr = binds.begin(); itr!=binds.end(); ++itr) {
	if((itr->page==-1 || itr->page==page) && (itr->code & mask) == key && (itr->code & antimask) == 0) {
	  //This key is terminated and matches
	  (*itr->pc)();
	  return 0;
	}
      }
      return 0;
    }

    if(pos==4)    //Cant add more keys
      break;

    //Add more keystrokes
    int i = dd_getc();
    key |= (i&0xff) << (8*pos);
    ++pos;
  }

  return 0;
}

//Handle unbound keys
int CSparrowHawk::kb_unbound(long key)
{
  log("Unbound key 0x%x", key);
  return 0;
}

void CSparrowHawk::kb_back()
{
  if(setting.use_tabs) {
    tab_select_prev();
    return;
  }

  if(ddprv)
    dd_prvtbl();   //Go back to previos table
  return;
}


/*
**
** Sparrow thread and callbacks
**
*/

void CSparrowHawk::sparrow_thread()
{
  m_running=true;
  dd_loop();
  dd_close();
  m_running=false;
}




/*
**
** Extension to sparrow to handle bools
** Check and display doubles (double precision)
**
*/

int dd_bool(DD_ACTION action, int id)
{
  DD_IDENT *dd = ddtbl + id;
  char ibuf[32];
  bool *value = (bool *)dd->value, *current = (bool *)dd->current;
  
  switch (action) {
  case Update:
    if (dd->current != NULL && (!dd->initialized || *value != *current)) {
      dd_bool(Refresh, id);    //Do a refresh
    }
    break;
    
  case Refresh:
    *current = *value;
    if(*current)
      dd_puts(dd, "True ");
    else
      dd_puts(dd, "False");
    dd->initialized = 1;
    break;
    
  case Input:
    if (DD_SCANF("Bool: ", "%s", ibuf) == 1) {
      if(ibuf[0]=='0' && ibuf[1]==0)
	*value = false;
      else if(ibuf[0]=='1' && ibuf[1]==0)
	*value = true;
      else if((ibuf[0]=='f' || ibuf[0]=='F') && ibuf[1]==0)
	*value=false;
      else if((ibuf[0]=='t' || ibuf[0]=='T') && ibuf[1]==0)
	*value=true;
      else if(strcasecmp(ibuf, "false")==0)
	*value=false;
      else if(strcasecmp(ibuf, "true")==0)
	*value=true;
    }
    break;
    
  case Save:
    sprintf(dd_save_string, "%c", *value ? 'T' : 'F');
    break;
    
  case Load:
    if(sscanf(dd_save_string, "%c", &ibuf[0])==1)
      *value = (ibuf[0]=='T' ? true : false);
    break;
    
  }
  return 0;
}
