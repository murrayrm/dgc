/*!
 * \file AttentionPlannerStates.hh
 *
 * \author Jeremy Ma
 * \date August 16, 2007
 *
 * This header file defines the Planner states that get sent to the attention module
 */

#ifndef __ATTENTIONPLANNERSTATE__
#define __ATTENTIONPLANNERSTATE__

#include <stdint.h>

#include "gcinterfaces/GcModuleInterfaces.hh"
#include "gcmodule/GcInterface.hh"


namespace PlanningState { 

  enum Mode {
    
    // planning states
    NOMINAL = 0,
    INTERSECT_LEFT = 1 << 0,
    INTERSECT_RIGHT = 1 << 1,
    INTERSECT_STRAIGHT = 1 << 2,
    BACKUP = 1 << 3,
    DRIVE = 1 << 4,
    STOP_OBS = 1 << 5,
    
    // planning flags
    ZONE = 1 << 6
  };
}

/*! Directive for sending planning modes to Attention module */
struct PlannerState : public GcTransmissive {

  unsigned id;			///! Identifier for this directive  
  PlanningState::Mode mode; 

  /* Accessor functions */
  unsigned getDirectiveId() const { return id; }  


  /*! Planner state default constructor */
  PlannerState()
  : id(0)
  , mode(PlanningState::NOMINAL) {
  }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar &id;
    ar &mode;
  }

  /*! Logging function */
  string toString() const { 
    stringstream s("");
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    s << t << "\tPlannerState: id = " << id << " mode = " << mode;
    return s.str();
  }
};


/*! Response from Attention */
struct AttentionResponse : public GcInterfaceDirectiveStatus
{

  int reason;		///! Reason for failure (if status = FAILED) 

  /* Accessor functions */
  virtual unsigned getDirectiveId() const { return id; }
  virtual int getCustomStatus( ) { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar &boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &reason;
  }

  /*! Logging function */
  string toString() const {
    stringstream s("");
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    s << t << "\tAttentionResponse: id = " << id << " status = "
      << status << " reason = " << reason ;
    return s.str();
  }
};

typedef GcInterface<PlannerState, AttentionResponse, SNplannerState, SNattentionResponse, MODattention> PlannerStateInterface;

#endif
