/*!
 * \file AttentionPlannerStates.hh
 *
 * \author Jeremy Ma
 * \date August 16, 2007
 *
 * This header file defines the Planner states that get sent to the attention module
 */

#ifndef __ATTENTIONPLANNERSTATE__
#define __ATTENTIONPLANNERSTATE__

#include <stdint.h>

#include "gcinterfaces/GcModuleInterfaces.hh"
#include "gcmodule/GcInterface.hh"

enum PlanningMode {

  // planning states
  INTERSECT_LEFT = 1,
  INTERSECT_RIGHT = 1 << 1,
  INTERSECT_STRAIGHT = 1 << 2,
  BACKUP = 1 << 3,
  DRIVE = 1 << 4,
  STOP_OBS = 1 << 5,

  // planning flags
  ZONE = 1 << 6

};

#endif
