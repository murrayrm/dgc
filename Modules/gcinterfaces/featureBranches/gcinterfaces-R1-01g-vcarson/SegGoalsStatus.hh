/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef STATUS_HH_
#define STATUS_HH_

#include "gcinterfaces/GcModuleInterfaces.hh"
#include <sstream>
#include <string>
using std::string;
using std::stringstream;


class SegGoalsStatus : public GcInterfaceDirectiveStatus
{
public:
  /* Status is already defined in GcInterfaceDirectiveStatus in
     GcModuleInterfaces.hh
  enum Status{ ACCEPTED, REJECTED, COMPLETED, FAILED, PENDING };
  */
  enum ReasonForFailure{ OBSTACLES, KNOWLEDGE, OUTSIDE_CORRIDOR };

  int goalID;
  //Status status;
  int currentSegmentID;
  int currentLaneID;
  int lastWaypointID;
  ReasonForFailure reason;

  SegGoalsStatus()
  {
    // initialize the status
    goalID = 0;
    status = FAILED;
    currentSegmentID = 0;
    currentLaneID = 0;
    lastWaypointID = 0;
  }

  virtual unsigned getDirectiveId() const
  {
    return (unsigned) goalID;
  }

  std::string toString() const {
    stringstream s("");
    s << "GoalId: " << goalID << " Status: "  << status
      << " reason: " << reason << "\n";
    return s.str();
  }
  
  /* The following function is already defined in GcInterfaceDirectiveStatus
     in GcModuleInterfaces.hh 
  virtual int getCustomStatus( ) 
  { 
    return status;
  }
  */

  /*! Serialize function */
  friend class boost::serialization::access;
  private:
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar & goalID;
    ar & currentSegmentID;
    ar & currentLaneID;
    ar & lastWaypointID;
    ar & reason;
  }

};
#endif //STATUS_HH_
