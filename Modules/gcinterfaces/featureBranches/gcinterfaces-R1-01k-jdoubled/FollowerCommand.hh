/*!
 * \file FollowerCommand.hh
 * \brief Public interfaces for follower
 *
 * \author Nok Wongpiromsarn
 * \date June 2007
 *
 * This header file defines the GcInterfaces to follower.
 */

#ifndef _FOLLOWERCOMMAND_HH_457823197082539
#define _FOLLOWERCOMMAND_HH_457823197082539
#include <sys/time.h>
#include <string>
#include "gcinterfaces/GcModuleInterfaces.hh"
#include "gcmodule/GcInterface.hh"
using std::string;
using std::stringstream;


enum FollowerCommandType {
  PauseCmd,	   ///! Pause command 
  ReleasePauseCmd    ///! Reset the Pause command
};

/*! Directive for sending commands to follower */
struct FollowerDirective : public GcTransmissive
{
  /* Define the components of the directive */
  unsigned id;			///! Identifier for this directive
  FollowerCommandType command;	///! Command to execute

  /* Accessor functions */
  unsigned getDirectiveId() const { return id; }  

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar &id;
    ar &command;
  }

  /*! Logging function */
  string toString() const { 
    stringstream s("");
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    s << t << "\tFollowerCommand: id = " << id << " command = " << command;
    return s.str();
  }
};

/*! Response from Follower */
struct FollowerResponse : public GcInterfaceDirectiveStatus
{
  /*! Reasons for failure from follower */
  enum FollowerReason {  
    NoReason = 0,
    Paused = 1,
    EmptyTraj = 2,
    LatMaxErrorExceeded = 4,
    LongMaxErrorExceeded = 8,
    TrajReceiveTimeout = 16,
    ActuatorFailure = 32, 
    OutOfRange = 64,
    EndOfTraj = 128
  };

  int reason;		///! Reason for failure (if status = FAILED) 

  /* Accessor functions */
  virtual unsigned getDirectiveId() const { return id; }
  virtual int getCustomStatus( ) { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar &boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &reason;
  }

  /*! Logging function */
  string toString() const {
    stringstream s("");
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    s << t << "\tFollowerResponse: id = " << id << " status = "
      << status << " reason = " << reason ;
    return s.str();
  }
};

// This is for sending responses from the follower
typedef GcInterface<FollowerDirective, FollowerResponse, SNfollowDir, SNfollowResponse, MODfollow> FollowerCommand;

#endif
