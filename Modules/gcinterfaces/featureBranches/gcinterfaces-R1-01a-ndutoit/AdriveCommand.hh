/*!
 * \file AdriveCommand.hh
 * \brief Public interfaces for adrive
 *
 * \author Nok Wongpiromsarn and Richard Murray
 * \date April 2007
 *
 * This header file defines the GcInterfaces to adrive.  The main
 * interface is AdriveCommand, with input AdriveDirective and output
 * AdriveResponse.  Several enums are also used for passing
 * information back and forth to adrive.
 */

#ifndef __ADRIVECOMMAND_HH__
#define __ADRIVECOMMAND_HH__
#include <sys/time.h>
#include <string>
#include "gcinterfaces/GcModuleInterfaces.hh"
#include "gcmodule/GcInterface.hh"
using std::string;
using std::stringstream;

/*! Adrive actuators */
enum AdriveActuator { 
  Steering, Acceleration, Transmission, Ignition, TurnSignal
};

/*! Adrive actuation command type */
enum AdriveCommandType {
  SetPosition,			///! Set the position of the actuator 
  Reset,			///! Reset the actuator (mainly for test)
  Pause,			///! Pause the vehicle (for acceleration) 
  ReleasePause,			///! Release the pause condition
  Disable			///! Internal usage: e-stop disable
};

/*! Reasons for failure from adrive */
enum AdriveFailure { 
  NotInitialized,		// actuation subsystem not initialized
  OutOfRange,			// argument is out of range
  VehiclePaused,		// adrive is currently paused (Accel only)

  /* The following reasons for failure are mainly for internal use */
  CommunicationError,		// can't communicate with controller
  InitializationError,		// actuator didn't initialize properly
};

/*! Directive for sending commands to adrive */
struct AdriveDirective : public GcTransmissive
{
  /* Define the components of the directive */
  unsigned id;			///! Identifier for this directive
  AdriveActuator actuator;	///! Actuator to command
  AdriveCommandType command;	///! Command to execute
  double arg;			///! Value for the command 

  /* Accessor functions */
  unsigned getDirectiveId() const { return id; }  

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar &id;
    ar &actuator;
    ar &command;
    ar &arg;
  }

  /*! Logging function */
  string toString() const { 
    stringstream s("");
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    s << t << "\tAdriveActuatorCommand: id = " << id << " actuator = "
      << actuator << " command = " << command << " arg = " << arg;
    return s.str();
  }
};

/*! Response from adrive */
struct AdriveResponse : public GcInterfaceDirectiveStatus
{
  /* Define the elements of the response */

  /* We do not need id here because it's already defined in the base class
   * (GcInterfaceDirectiveStatusBase)
   unsigned id;			///! Identifier for this response */

  AdriveFailure reason;		///! Reason for failure (if status = FAILED) 

  /* Accessor functions */
  virtual unsigned getDirectiveId() const { return id; }
  virtual int getCustomStatus( ) { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar &boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &id;
    ar &reason;
  }

  /*! Logging function */
  string toString() const {
    stringstream s("");
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    s << t << "\tAdriveResponse: id = " << id << " status = "
      << status << " reason = " << reason;
    return s.str();
  }
};

/*!
 * GcInterface for sending command to adrive
 * 
 * TODO: We use two unused skynet IDs for sending information back and
 * forth.  When this file is moved to gcinterfaces, we should define
 * the skynet types properly.
 */
#warning Define skynet types for adrive directives and responses

typedef GcInterface<AdriveDirective, AdriveResponse, SNadrive_command, 
		    SNadrive_response, SNadrive> AdriveCommand;

#endif
