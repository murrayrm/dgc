/*!
 * \file AttentionPlannerStates.hh
 *
 * \author Jeremy Ma
 * \date August 16, 2007
 *
 * This header file defines the Planner states that get sent to the attention module
 */

#ifndef __ATTENTIONPLANNERSTATE__
#define __ATTENTIONPLANNERSTATE__

#include <stdint.h>

#include "gcinterfaces/GcModuleInterfaces.hh"
#include "gcmodule/GcInterface.hh"


namespace PlanningState { 

  enum Mode {
    
    // planning states
    NOMINAL = 0,
    INTERSECT_LEFT = 1 << 0,
    INTERSECT_RIGHT = 1 << 1,
    INTERSECT_STRAIGHT = 1 << 2,
    BACKUP = 1 << 3,
    DRIVE = 1 << 4,
    STOP_OBS = 1 << 5,
    PASS_LEFT = 1 << 6,
    PASS_RIGHT = 1 << 7,
    UTURN = 1 << 8,
    // planning flags
    ZONE = 1 << 9
  };


  struct ExitPoint {
    int segment; 
    int lane; 
    int waypoint;


    ExitPoint(int seg, int ln, int pt) 
      : segment(seg) 
      , lane(ln)
      , waypoint(pt)
    {
    }

    /*! Default constructor */
    ExitPoint() 
      : segment(0) 
      , lane(0)
      , waypoint(0)
    {
    }

    /*! Copy constructor */
    ExitPoint(const ExitPoint &exit) 
    {
      segment = exit.segment;
      lane = exit.lane;
      waypoint = exit.waypoint;
    }

    /*! Equals operator */
    ExitPoint &operator=(const ExitPoint &point){
      if (this!= &point){
	segment = point.segment;
	lane = point.lane;
	waypoint= point.waypoint;
      }
      return *this;
    }

    /*! Output operator */
    friend ostream &operator<<(ostream &os, const ExitPoint &exitPoint)
    {
      os << "(" << exitPoint.segment <<", " << exitPoint.lane << ", " << exitPoint.waypoint <<")" ;
      return os;
    }

    /*! Serialize function */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
      ar &segment;
      ar &lane;
      ar &waypoint;
    }


  };
}

/*! Directive for sending planning modes to Attention module */
struct PlannerState : public GcTransmissive {

  unsigned id;			/*!  Identifier for this directive  */
  PlanningState::Mode mode;   /*! The planning state the Planner is in */ 
  PlanningState::ExitPoint exitPoint; /*! The exit point corresponding to the goal from which mode was derived from */

  /* Accessor functions */
  unsigned getDirectiveId() const { return id; }  

  /*! Planner state default constructor */
  PlannerState()
  : id(0)
  , mode(PlanningState::NOMINAL)
  , exitPoint(0,0,0) {
  }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar &id;
    ar &mode;
    ar &exitPoint;
  }

  /*! Logging function */
  string toString() const { 
    stringstream s("");
    struct timeval tv;
    gettimeofday(&tv, NULL);
    stringstream state(""); 

    switch (mode) {
    case PlanningState::NOMINAL:
      state<<"NOMINAL";
      break;
    case PlanningState::INTERSECT_LEFT:
      state<<"INTERSECT_LEFT,exit point = "<<exitPoint;
      break;
    case PlanningState::INTERSECT_RIGHT:
      state<<"INTERSECT_RIGHT,exit point = "<<exitPoint;
      break;
    case PlanningState::INTERSECT_STRAIGHT:
      state<<"INTERSECT_STRAIGHT,exit point = "<<exitPoint;
      break;
    case PlanningState::BACKUP:
      state<<"BACKUP";
      break;
    case PlanningState::DRIVE:
      state<<"DRIVE";
      break;
    case PlanningState::STOP_OBS:
      state<<"STOP_OBS";
      break;
    case PlanningState::PASS_LEFT:
      state<<"PASS_LEFT";
      break;
    case PlanningState::PASS_RIGHT:
      state<<"PASS_RIGHT";
      break;
    case PlanningState::UTURN:
      state<<"UTURN";
      break;
    case PlanningState::ZONE:
      state<<"ZONE";
      break;
    default:
      state<<"DEFAULT";
      break;
    }


    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    s << t << "\tPlannerState: id = " << id << " mode = " << state.str();

    return s.str();
  }
};


/*! Response from Attention */
struct AttentionResponse : public GcInterfaceDirectiveStatus
{

  int reason;		///! Reason for failure (if status = FAILED) 

  /* Accessor functions */
  virtual unsigned getDirectiveId() const { return id; }
  virtual int getCustomStatus( ) { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar &boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &reason;
  }

  /*! Logging function */
  string toString() const {
    stringstream s("");
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    s << t << "\tAttentionResponse: id = " << id << " status = "
      << status << " reason = " << reason ;
    return s.str();
  }
};

typedef GcInterface<PlannerState, AttentionResponse, SNplannerState, SNattentionResponse, MODattention> PlannerStateInterface;

#endif
