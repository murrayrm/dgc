
#ifndef __GC_MODULE_INTERFACES_HH__FFIJ87GY4HFTO734JDGH9327H4FRGH__
#define __GC_MODULE_INTERFACES_HH__FFIJ87GY4HFTO734JDGH9327H4FRGH__

/*!  Dirctives to be tranmitted from GcModule controller to another 
 * controllers arbiter should derive from this class.
 * They must implement the getDirectiveId(), and the templated serialize
 * function.
 */
#include <boost/archive/binary_oarchive.hpp>
#include <ostream>
#include <iostream>
#include <sys/time.h>
using std::ostream;

class OStreamable {
public:
  virtual ~OStreamable() {}
  friend ostream& operator<< (ostream& out, const OStreamable& me);
  virtual std::string toString() const = 0;
};

class IGcTransmissive : public OStreamable {
  public:
    virtual ~IGcTransmissive() {} 
    virtual unsigned int getDirectiveId() = 0;
    // Cannot have a virtual templated function... 
    // Developers, you must implment this function(remove 'virtual'), 
    // the linker will remind you...
    //template<class ArchiveT>
    //virtual void serialize(ArchiveT, const unsigned int version ) = 0;
};

class GcTransmissive : public IGcTransmissive {
  public:
    virtual ~GcTransmissive() {}
    
    uint64_t timeSent;

    template< class ArT >
    void serialize( ArT & ar, const unsigned ver ) {
      ar & timeSent;
    }
};

/*!
 * Status sent back from a GcModule's arbiter to the controlling controler
 * should derive from this class.  They should override the getDirectiveId 
 * (or properly set the id field) and must implement the serialize function
 * to transmit their data.  The serialize function must serialize the base 
 * as instructed below.
 */
class GcInterfaceDirectiveStatusBase : public GcTransmissive
{
  public:
    // must implement this so we can auto-capture and classify 
    // status respnoses
    virtual int          getCustomStatus() = 0;

  public:
    enum DeliveryStatus { NONE, QUEUED, SENT, RECEIVED, ACKED } ;
  public:
    unsigned id;
    DeliveryStatus  deliveryStat;

  public:
    GcInterfaceDirectiveStatusBase() : id(0), deliveryStat(NONE)
    {}
    virtual ~GcInterfaceDirectiveStatusBase() {}
    unsigned getDirectiveId( )
    {
      return id;
    }

  friend class boost::serialization::access;
  private:
    /*! When overriding the serialize function you should make it private, 
     * and allow the boost:serialization::access class to be a friend.
     * You must also include the following line to serialize the parent
     * class:
     *   ar & boost::serialization::base_object<GcInterfaceDireciveStatus>(*this);
     * where GcInterfaceDirectiveStatus is the parent of your class (replace if 
     * subclass futher).
     */
    template< class ArchT >
      void serialize( ArchT & ar, const unsigned version )
      {
        /// serialize the base class because it carries the timestamp
        ar & boost::serialization::base_object<GcTransmissive>(*this);
        
        ar & id;
        ar & deliveryStat;
      }

};

class GcInterfaceDirectiveStatus : public GcInterfaceDirectiveStatusBase
{
  public: 
    enum Status { COMPLETED, FAILED, ACCEPTED, REJECTED };
    Status          status;
    int          getCustomStatus() { return status; }

    template< class ArchT >
    void serialize( ArchT & ar, const unsigned veresion )
    {
      ar & boost::serialization::base_object<GcInterfaceDirectiveStatusBase>(*this);
      ar & status;
    }
};


#endif // __GC_MODULE_INTERFACES_HH__FFIJ87GY4HFTO734JDGH9327H4FRGH__


