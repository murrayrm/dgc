/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef SEGGOALS_HH_
#define SEGGOALS_HH_

#include <iostream>
#include <string>
#include "gcinterfaces/GcModuleInterfaces.hh"
#include <sstream>
using std::cout;
using std::stringstream;
using std::string;


struct SegGoals : public GcTransmissive
{
  enum SegmentType{ ROAD_SEGMENT, PARKING_ZONE, INTERSECTION, PREZONE, UTURN, PAUSE,
		    DRIVE_AROUND, BACK_UP, END_OF_MISSION, UNKNOWN};
  SegGoals()
  {
    segment_type = UNKNOWN;
    goalID = 0;
    entrySegmentID = 0;
    entryLaneID = 0;
    entryWaypointID = 0;
    exitSegmentID = 0;
    exitLaneID = 0;
    exitWaypointID = 0;
    minSpeedLimit = 0;
    maxSpeedLimit = 0;
    illegalPassingAllowed = false;
    stopAtExit = false;
    isExitCheckpoint = false;
    distance = 0;
    perf_level = 0;
  }
  void print()
  { 
    stringstream strout("");
    print( strout );
    cout << strout.str();
  }

  string toString() const 
  {
    stringstream strout("");
    print(strout);
    return strout.str();
  }

  void print(stringstream& strout) const
  {
    strout << "GOAL " << goalID << ":\t";
    if ( segment_type == ROAD_SEGMENT || segment_type == PARKING_ZONE || 
	 segment_type == INTERSECTION || segment_type == PREZONE ||
	 segment_type == UTURN ) {
      strout << entrySegmentID << "." << entryLaneID << "." << entryWaypointID;
      strout << "\t -> \t";
      strout << exitSegmentID << "." << exitLaneID << "." << exitWaypointID;
      strout << "\t\t";
      switch(segment_type) {
      case ROAD_SEGMENT:
	strout << "ROAD_SEGMENT";
	break;
      case PARKING_ZONE:
	strout << "PARKING_ZONE";
	break;
      case INTERSECTION:
	strout << "INTERSECTION";
	break;
      case PREZONE:
	strout << "PREZONE";
	break;
      case UTURN:
	strout << "UTURN";
	break;
      case PAUSE:
	strout << "PAUSE";
	break;
      case END_OF_MISSION:
	strout << "END_OF_MISSION";
	break;
      default:
	strout << "UNKNOWN";
      }
      strout << "\tMin Speed: " << minSpeedLimit << " Max Speed: " << maxSpeedLimit
	     << "\tPerformance: " << perf_level;
      if (illegalPassingAllowed)
	strout << "\tIllegal passing allowed";
      if (stopAtExit)
	strout << "\tStop";
      if (isExitCheckpoint)
	strout << "\tCheckpoint";
    }
    else if ( segment_type == PAUSE )
      strout << "PAUSE";
    else if ( segment_type == DRIVE_AROUND )
      strout << "DRIVE_AROUND " << distance << " m" << "\tMin Speed: " << minSpeedLimit
	     << " Mix Speed: " << maxSpeedLimit;
    else if ( segment_type == BACK_UP )
      strout << "BACK_UP " << distance << " m"<< "\tMin Speed: " << minSpeedLimit
	     << " Mix Speed: " << maxSpeedLimit;
    else if ( segment_type == END_OF_MISSION )
      strout << "END_OF_MISSION";
    else
      strout << "UNKNOWN";
  }

  unsigned getDirectiveId()
  {
    return (unsigned) goalID;
  }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar & segment_type;
    ar & goalID;
    ar & entrySegmentID;
    ar & entryLaneID;
    ar & entryWaypointID;
    ar & exitSegmentID;
    ar & exitLaneID;
    ar & exitWaypointID;
    ar & minSpeedLimit;
    ar & maxSpeedLimit;
    ar & illegalPassingAllowed;
    ar & stopAtExit;
    ar & isExitCheckpoint;
    ar & perf_level;
    ar & distance;
  }

  SegmentType segment_type;
  int goalID;

  /*! parameters for segment goals */
  int entrySegmentID;
  int entryLaneID;
  int entryWaypointID;
  int exitSegmentID;
  int exitLaneID;
  int exitWaypointID;
  double minSpeedLimit;
  double maxSpeedLimit;
  bool illegalPassingAllowed;
  bool stopAtExit;
  bool isExitCheckpoint;
  int perf_level;

  /*! parameter for DRIVE_AROUND and BACK_UP */
  double distance;
};

#endif //SEGGOALS_HH_
