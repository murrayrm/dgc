/*!
 * \file FollowerCommand.hh
 * \brief Public interfaces for follower
 *
 * \author Nok Wongpiromsarn
 * \date June 2007
 *
 * This header file defines the GcInterfaces to follower.
 */

#ifndef _FOLLOWERCOMMAND_HH_457823197082539
#define _FOLLOWERCOMMAND_HH_457823197082539
#include <sys/time.h>
#include <string>
#include "gcinterfaces/GcModuleInterfaces.hh"
#include "gcmodule/GcInterface.hh"
using std::string;
using std::stringstream;

/*! Response from adrive */
struct FollowerResponse : public GcInterfaceDirectiveStatus
{
  /*! Reasons for failure from follower */
  enum FollowerFailure { 
    Paused = 1,
    LateralMaxErrorExceeded = 4,
    LongitudinalMaxErrorExceeded = 8,
    trajReceiveTimeout = 16,
    ActuatorFailure = 32
  };
  int reason;		///! Reason for failure (if status = FAILED) 

  /* Accessor functions */
  virtual unsigned getDirectiveId() const { return id; }
  virtual int getCustomStatus( ) { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar &boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &reason;
  }

  /*! Logging function */
  string toString() const {
    stringstream s("");
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    s << t << "\tFollowerResponse: id = " << id << " status = "
      << status << " reason = " << reason;
    return s.str();
  }
};

#endif
