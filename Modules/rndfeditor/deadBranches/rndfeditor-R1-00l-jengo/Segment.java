import java.io.BufferedReader;
import java.io.IOException;
import java.io.*;
import java.util.Arrays;

/**
 * a Segment class
 * @author Xiang Jerry He
 *
 */

public class Segment implements Comparable{
    /** the id of this segment */
    protected int id;
    /** number of lanes */
    protected int num_lanes;
    /** name of this segment */
    protected String segment_name;
    /** list of all lanes in this segment */
    private Lane[] lanes; 
    /** Holds whether this segement is selected */
    private boolean selected = false;
		  
    public Segment(){
                      
    }
                  
    public Segment(int id){
	this.id = id;
	num_lanes = 0;
	segment_name = null;
	lanes = new Lane[0];
    }
                  
    /**
     * Read in and parse a Segment
     * @author Xiang Jerry He
     * @param in the buffered reader for the RNDF
     * @throws IOException if we fail to read in the file
     * @throws MapFormatException if the file is incorrectly formatted
     */
    void readSegment(BufferedReader in) throws IOException, MapFormatException {
	System.out.println("Entering segment");
	String line = in.readLine();
	System.out.println(line);
	String[] tokens = line.split("\\s");
	while(!line.startsWith("lane")) {
	    System.out.println(line);
	    //if it is not an empty line, then process it
	    if(line.matches("\\w.*")) {
		tokens = line.split("\\s");
		if(line.startsWith("num_lanes")) {
		    System.out.println("numlanetoken: " + tokens[1]);
		    num_lanes = Integer.parseInt(tokens[1]);
		} 
		else if(line.startsWith("segment_name")) {
                                           
		    segment_name = line.substring( line.indexOf( tokens[1] ) );
		} else if(line.startsWith("segment")){
		    // Doesn't have to worry about getting confused segment_name
		    // as that has already been checked.
		    //Read in the segment ID(will not occur on the first
		    // call from readMap, as the needed line has already been
		    // read before the readSegment call
		    System.out.println(tokens[1]);
		    id = Integer.parseInt(tokens[1]);
		}
	    }
	    line = in.readLine();
	}
	System.out.println("number of lanes is " + num_lanes);
	System.out.println("segment name is "+ segment_name);
	System.out.println(line);
	lanes = new Lane[num_lanes];
	for(int i=0; i < num_lanes; i++) {
	    lanes[i] = new Lane();
	    lanes[i].readLane(in);
	}
	assert(in.readLine().equals("end_segment"));
    }
                 
    /**
     * Writes the segment to file using hte given PrintWriter in darpa specified RNDF
     * format
     *
     *@param file The PrintWriter to use to write the segment
     */              
    public void printSegment(PrintWriter file){
        file.println("segment\t" + id);
        file.println("num_lanes\t" + num_lanes);
        if(segment_name != null)
            file.println("segment_name\t" + segment_name);
        for(Lane l: lanes){
            l.printLane(file);
        }
        file.println("end_segment");
        file.println("");
    }
    
    /** Compares to another segment based on the id of the two segments 
     *
     *@param other The other segment to compare to.
     *@throws ClassCastException if other is not a Segment
     */
    public int compareTo(Object other){
        if(!(other instanceof Segment)){
            throw new ClassCastException("Tried to compare to a non-Segment object");
        }
        return id - ((Segment) other).getId();
    }

    /**
     * Adds a lane to the segment. Updates num_lanes
     *
     */
    public void addLane(Lane l){
        num_lanes++;
        Lane[] temp = lanes;
        lanes = new Lane[num_lanes];
        lanes[0] = l; // Add the new lane
        for(int i = 0; i < num_lanes-1; i++){
            // Copy over the old elements of the lane
            lanes[i+1] = temp[i];
        }
        if(lanes.length > 1){
            for(Lane lane : lanes){
                System.out.println(lane);
            }
            Arrays.sort(lanes); // only need to sort if there are >1 element
        }
    }
    
    /**
     * Removes the lane with the specified lane index.  
     * Returns -1 if the specified lane cannot be found.
     * The map containing the segment is needed so that this method can update the exit/entrance pairs of all
     * lanes to take into account the changed lane information
     *
     * @param l The lane component of the id of the lane to be removed (second number)
     * @param m The map containing the lane
     */
    public int removeLane(int l, Map m){
        if(l > num_lanes){
            return -1;
        }
        // Convert from id to array index
        l--;
        return removeLaneIndex( l, m );
    }

    public int removeLaneIndex( int l, Map m ) {
        System.out.println(num_lanes);
        Lane[] temp = new Lane[num_lanes - 1];
        int workingIndex = 0; // The index to copy elements into
        int[] laneid = new int[2];
        for(int i = 0; i < num_lanes; i++){
            if(i != l){
                // If this is not the element to remove copy into lanes, and increment
                // working index
                temp[workingIndex] = lanes[i];
                if(i > l){
                    // If this is a higher index than that to be removed, decrement its id value
                    temp[workingIndex].getId()[1] = temp[workingIndex].getId()[1] - 1;
                    // need to update lane exit/entrance pairs
                    for(Segment s : m.getSegs()){
                        for(Lane lane : s.lanes){
                            lane.laneUpdateEntrances(id, i+1, temp[workingIndex].getId()[1], -1, -1);
                        }
                    }
                }
                laneid[0] = id;
                laneid[1] = workingIndex + 1;
                temp[workingIndex].setId( laneid );

                workingIndex++;
            }else{
                // Do not increment working index to skip this element
                // need to update exit/entrance pairs to take into account the deleted lane
                for(Segment s : m.getSegs()){
                    for(Lane lane : s.lanes){
                        lane.laneUpdateEntrances(id, i+1, -1, -1, -1); // -1 to indicate deletion.  The other
                        // -1 are place holders for waypoint values that don't matter
                    }
                }
            }
        }
        lanes = temp;
        num_lanes --;
        return 0;
    }
    
    public Lane[] getLanes() {
        return lanes;
    }

    /**
     * Enforces sorting
     */
    public void setLanes(Lane[] lanes) {
        this.lanes = lanes;
        Arrays.sort(this.lanes);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getId() {
        return id;
    }

    /**
     * Propogates this change to its lanes and waypoints
     */
    public void setId(int id) {
        for(Lane l: lanes){
            // Reset the lane's segment id, which will propogate down to the waypoints
            l.setId(0, id);
        }
        this.id = id;
    }
    
    public String toString(){
        return "Segment " + id;
    }
	  
    public void transferLane( Lane l, Segment other, Map m) {
	if( other == null ) {
	    System.out.println( "transferLane: other segment was null" );
	    return ;
	}

	int[] oldLaneId = Ids.copy( l.getId() );
	int[] nid = { other.getId(), other.num_lanes+1 } ;
	// apply the transfer to the lane
	l.changeSegment( nid );
	// add the selected lane to the other segment
	other.addLane( l );
	// now go through all lanes and update their entrances
	for( Segment seg: m.getSegs() ) {
	    for( Lane ll: seg.getLanes() ) {
		ll.changeEntranceSegments( this.getId(), oldLaneId[1], nid[0], nid[1] );
	    }
	}
	// find the lane locally, remove it, and apply the changes to the remaining lanes
	int i=0;
	for( ; i<lanes.length; i++ ) {
	    if( lanes[i] == l ) break;
	}
	if( removeLaneIndex( i, m ) < 0 )
	    System.out.println( "Could not remove lane -- lane not found" );

    }
}
	
