
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Vector;
import java.io.*;

/**
 * Used to represent and read in a zone from an RNDF file.  A zone consists of
 * a perimeter and a list of spots.
 */
public class Zone {

    int zone_id;
    int num_spots;
    String zone_name;
    Perimeter perimeter;
    private Spot[] spots;
    /** Holds whether this zone is currently selected by the user */
    private boolean selected = false;

    public Zone() {

    }

    public Zone(int id) {
	this.zone_id = id;
	num_spots = 0;
	zone_name = null;
	spots = new Spot[0];
    }

    public int getId() {
	return zone_id;
    }

    /** 
     * Just calls changeZoneId
     */
    public void setId(int id) {
	changeZoneId(id);
    }
    /** 
     * Propogates this change to spots and perim pts 
     */
    void changeZoneId( int newId ) {
	zone_id = newId;
	for( Spot s: spots ) {
	    s.changeZoneId( newId );
	}
	perimeter.changeZoneId(newId);
    }

    /**
     * Returns the periim pts and spot pts 
     */
    public Vector<WayPoint> getWayPoints() {
	Vector<WayPoint> vect = new Vector<WayPoint>();

	for(WayPoint w: perimeter.getPoints()){
	    vect.add(w);
	}
	
	for(Spot s: spots){
	    WayPoint w = s.getCheckPoint();
	    vect.add(w);
	    w = s.getInPoint();
	    vect.add(w);
	}
	
	return vect;
    }

    public int getNumPerimPts() {
	return perimeter.getPoints().size();
    }

    public Vector<WayPoint> getPerimPts() {
	return perimeter.getPoints();
    }

    public void addPerimPt(WayPoint w) {
	perimeter.getPoints().add(w);
    } 

    public String toString() {
	return "Zone " + zone_id;
    }

    /** In the event that a segment or zone is deleted, 
     * this function will shift the exit/entrance pairs
     * note: this just calls perimeter::shiftBackExitEntrance
     */
    public void shiftBackExitEntrance(int seg) {
	perimeter.shiftBackExitEntrance(seg);
    }

    /** 
     * Removes all exit/entry pts pointing to a certain segment
     */
    public void removeExitsToSeg(int seg) {
	perimeter.removeExitsToSeg(seg);
    }

    /**
     * Read in and parse a zone.  Find the perimeter and spots corresponding
     * to the zone.
     * 
     * @param br the buffered reader for the RNDF
     * @throws IOException if we fail to read in the file
     * @throws MapFormatException if the file is incorrectly formatted
     */
    void readZone(BufferedReader br) throws IOException, MapFormatException {
		
	System.out.println("Entering zone");
	String line = br.readLine();
	System.out.println(line);
	String[] tokens = line.split("\\s");
	/*assert(tokens[0].equals("segment"));
	  System.out.println("token0 is "+ tokens[0]);
	  id = Integer.parseInt(tokens[1]);
	  System.out.println("segment ID is " + id); */
	while(!line.startsWith("spot")) {
			  
	    System.out.println(line);
	    tokens = line.split("\\s");
	    if(line.startsWith("num_spots")) {
		System.out.println("numspottoken: " + tokens[1]);
		num_spots = Integer.parseInt(tokens[1]);
	    }
	    else if(tokens[0].equals("zone_name")) {
		zone_name = tokens[1];
	    }
	    else if(tokens[0].equals("zone")){
		zone_id = Integer.parseInt(tokens[1]);
	    }
	    else if(tokens[0].equals("perimeter")) {
		perimeter = new Perimeter(WayPoint.toIntRep(tokens[1]));
		perimeter.readPerimeter(br);
	    }
	    line = br.readLine();
	}
	System.out.println("number of spots is " + num_spots);
	System.out.println("zone name is " + zone_name);
	System.out.println(line);
	tokens = line.split("\\s");
	spots = new Spot[num_spots];
	for(int i=0; i < num_spots; i++) {
	    spots[i] = new Spot(tokens[1]);
	    spots[i].readSpot(br);
	    line = br.readLine();
	    tokens = line.split("\\s");
	}
	assert(br.readLine().equals("end_zone"));
    }

    public Spot[] getSpots() {
        return spots;
    }

    public void setSpots(Spot[] spots) {
        this.spots = spots;
    }
    
    public Perimeter getPerimeter(){
        return perimeter;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    /** 
     * Prints the zone to file in DARPA RNDF format
     *
     *@param file The PrintWriter to use in printing the zone
     */
    public void printZone(PrintWriter file){
        file.println("zone\t" + zone_id);
        file.println("num_spots\t" + num_spots);
        if(zone_name != null){
            file.println("zone_name\t" + zone_name);
        }
        perimeter.printPerimeter(file);
        for(Spot s: spots){
            s.printSpot(file);
        }
        file.println("end_zone");
        file.println("");
    }
}
