
import java.io.*;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.regex.*;

/**
 * A map class
 * @author Xiang Jerry He
 */


/* 	currently cannot handle multi-line comments */
public class Map
{
	/** number of segments */
	private int num_segments;
	/** number of zones */
	private int num_zones;
	/** collection of all segments in this map */
	private Segment[] segs;
	/** collection of all zones in this map */
	private Zone[] zones;
	/** miscellaneous information */
	protected Hashtable<String, String> info;

	
	Map() {
		info = new Hashtable<String, String>();
	}
	

	/**
	 * Read in and parse a map.  
	 * 
	 * @param in the buffered reader for the RNDF
	 * @throws IOException if we fail to read in the file
	 * @throws MapFormatException if the file is incorrectly formatted
	 */
	void readMap(BufferedReader in) throws IOException, MapFormatException {
            String line = in.readLine();
		String[] tokens;
		while(!line.startsWith("segment")) {
			if(line.matches("\\w+.*")) {
                            //Splits the line into two parts, around the string (printed value)
                            // '\s'
				tokens = line.split("\\s");
				System.out.println(line);
                                // Anything before the trigger string is stored as a key
                                // Anything after the trigger string is stored as data
				info.put(tokens[0], tokens[1]);
			}
			line = in.readLine();
		}
                // Look up the number of segments from the hash table
		num_segments = Integer.parseInt(info.get("num_segments"));
		setSegs(new Segment[num_segments]);
		System.out.println("the number of lanes is");
		System.out.println("number of segments is "+num_segments);
		num_zones = Integer.parseInt(info.get("num_zones"));
		setZones(new Zone[num_zones]);
		System.out.println("number of zones is " + num_zones);
                
                for(int i=0; i < num_segments; i++) {
			getSegs()[i]=new Segment();
		    (getSegs()[i]).readSegment(in);  
		}
                //The first segment didn't read in its id properly, as that line was read by the
                // buffered reader prior to readSegment being called, as part of the detection method
                // for when to start calling readSegment.  Need to find the missing id, and assign that
                // as the id for the first segment in segs
                int[] posID = new int[num_segments];
                for(int i= 0 ; i < num_segments; i++){
                    posID[i] = i+1;
                }
                // The id of segs[0] will be zero do the to read in issue
                for(int i = 1; i<num_segments; i++){
                    posID[segs[i].getId()-1] = 0;
                }
                for(int i : posID){
                    if(i!=0)
                        segs[0].setId(i);
                }
		/**
		 * reading in all zones
		 * @author David Waylonis
		 */
		for(int i=0; i < num_zones; i++) {
			getZones()[i]=new Zone();
		    (getZones()[i]).readZone(in);  
		}
		/** testing end of file */
		System.out.println(in.readLine());
		System.out.println(in.readLine());
		System.out.println(in.readLine());
		//assert(in.readLine().equals("end_file"));
	}
        
        /**
         * Writes the map into an RNDF file, according to the DARPA specifications
         *
         */
        public void writeMap(String filename){
            /** Create a PrintWriter to write the RNDF */
            try{
                PrintWriter outFile = new PrintWriter(new BufferedWriter( new FileWriter(filename)));
                //Print the header
                outFile.println("RNDF_name\t" + filename);
                outFile.println("num_segments\t" + num_segments);
                outFile.println("num_zones\t" + num_zones);
                outFile.println("creation_date\t" + 
                        Calendar.getInstance().getTime().toString().replace(" ", ""));
                
                // Print all the segments
                for(Segment s: segs){
                    s.printSegment(outFile);
                }
                // Print all the zones
                for(Zone z: zones){
                    z.printZone(outFile);
                }
                outFile.println("end_file");
                outFile.flush();
                outFile.close();
            }catch(IOException e){
                System.err.println("Error writing RNDF");
            }
        }

        /**
         * Adds a new segment to the map.  Id is set to be next largest in the series
         */
        public void addSegment(){
            num_segments ++;
            Segment[] temp = segs;
            segs = new Segment[num_segments];
            for(int i = 0; i < num_segments - 1; i++){
                segs[i] = temp[i];
            }
            segs[num_segments - 1] = new Segment(num_segments);
        }
        
     /**
     * Removes the segment with the specified id.
     * 
     * 
     * @param id The id of the segment to remove
     * @throws ArrayIndexOutOfBoundsException if the id doesn't within the list
     */
        public void removeSegment (int id) throws ArrayIndexOutOfBoundsException {
            id --; // convert from segment id to array index
            Segment[] temp = new Segment[num_segments - 1];
            int workingIndex = 0;
            for(int i = 0; i < num_segments; i++){
                if(i != id){
                    temp[workingIndex] = segs[i];
                    //If the current segment is after the segment to be removed, decrement its id value
                    if(i > id){
                        temp[workingIndex].setId(temp[workingIndex].getId() - 1);
                    }
                    workingIndex++;
                }else{
                    
                }
            }
            segs = temp;
            System.err.println("a;slkdfja;slkdjf;alskdjfa;lskdjf;alskdfj;alskdjf;aslkdfj " + segs.length);
            // Do the updating afterwards, incase an OutOfBoundsException needs to be thrown
            num_segments--;
            segs = temp;
        }
        
    public Segment[] getSegs() {
        return segs;
    }

    public void setSegs(Segment[] segs) {
        this.segs = segs;
    }

    public Zone[] getZones() {
        return zones;
    }

    public void setZones(Zone[] zones) {
        this.zones = zones;
    }

    public int getNum_segments() {
        return num_segments;
    }

    public int getNum_zones() {
        return num_zones;
    }
}