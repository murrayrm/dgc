
import java.awt.peer.MouseInfoPeer;
import java.io.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.util.Vector;
import java.util.Iterator;
import java.awt.Point;
import java.awt.MouseInfo;
import java.awt.event.KeyEvent;
import java.util.Collections;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.GridLayout;

/**
 * A class to represent the controls of the Maze
 * @author Xiang Jerry He
 */
public class MapViewer {

  /** default size of the MapViewer */
  public static final int DEFAULTSIZE = 80;
  /** the Frame of our display */
  private JFrame myWindow;
  /** the top-most Panel used to put buttons */
  protected JPanel buttonPanel;
  /** x-coordinate of the current mouse position measured either in LatLon or UTM*/
  protected final JTextField xcoord = new JTextField("0.0", 8);
  /** y coordinate of the current mouse position measured either in LatLon or UTM*/
  protected final JTextField ycoord= new JTextField("0.0", 8);
  /** Displays the easting of the mouse location */
  protected final JTextField eastingField = new JTextField("0.0", 8);
  /** Displays the northing of the mouse location */
  protected final JTextField northingField = new JTextField("0.0", 8);
  /** Displays the UTM zone of the mouse location */
  protected final JTextField zoneField = new JTextField("0", 8);
  /** Holds the index for the reference ellipse to use in UTM calculations */
  protected final int ellipseIndex = 23;
  /** Holds the scale factor of the perspective view */
  protected final JTextField perScale = new JTextField("1.0", 8);
  /** Holds the degree offset in latitude to use */
  protected final JTextField xOffset = new JTextField("0.0", 8);
  /** Holds the degree offset in longitude */
  protected final JTextField yOffset = new JTextField("0.0", 8);
  /** The map viewing component */
  private MapView MapPanel;



  /** Panel to hold the control items */
  protected JPanel controlPanel;
  /** A menu to select the waypoints to work with */
  protected JList wayPointList;
  /** A menu to select the segment (eventually should include zones) to work with */
  protected JList segList;
  /** A menu to select the lane (Should eventually include periemters/spots) to work with */
  protected JList laneList;
  /** Holds all the waypoints on the map, for use with user interaction */
  protected final Vector<WayPoint> wayVector = new Vector<WayPoint>();
  /** Button group so that only one action will occur on a mouse click */
  protected ButtonGroup clickGroup = new ButtonGroup();
  // no longer used
  //        /** Button that when selected, nothing happens on left click. */
  //        protected JRadioButton clickNothingButton = new JRadioButton("Nothing");
  //        /** Button that causes an unassigned waypoint to be added when
  //         * the mouse is clicked */
  //        protected JRadioButton clickAddWaypointButton = new JRadioButton("Add Waypoint");
  //        /** Sets to select the waypoint closest to a left button click */
  //        protected JRadioButton clickSelectButton = new JRadioButton("Select Waypoint");
  //        /** Sets whether to drag a waypoint */
  //        protected JRadioButton dragWayPointButton = new JRadioButton("Move Waypoint");
  /** Shifts the waypoint up in the lane's list of vectors */
  protected final JButton wayPointUpButton = new JButton("Shift Up");
  /** Shifts the waypoint down in the lane's list of waypoints */
  protected final JButton wayPointDownButton = new JButton("Shift Down");

  /** the map object to be drawn */
  private Map m;

  // Actions work as combined JMenuItem with their action listener when added to a JMenu
  // Makes for cleaner code when assembling the menu
  private Action fileOpenAction = new AbstractAction("Open RNDF...") {
    public void actionPerformed(ActionEvent e) {
      doOpen();
    }
  };
  private Action fileExitAction = new AbstractAction("Exit") {
    public void actionPerformed(ActionEvent e) {
      System.exit(0);
    }
  };

  private Action modeUTMAction = new AbstractAction("UTM") {
    public void actionPerformed(ActionEvent e) {
      JOptionPane.showMessageDialog(myWindow, "not yet implemented");
    }
  };

  private Action modeLatLonAction = new AbstractAction("LatLon") {
    public void actionPerformed(ActionEvent e) {
      JOptionPane.showMessageDialog(myWindow, "not yet implemented " +
          "but LatLon is the default behavior");
    }
  };

  private Action stopSignAction = new AbstractAction("StopSigns") {
    public void actionPerformed(ActionEvent e) {
      MapPanel.toggleStopSignVisibility();
      MapPanel.repaint();
    }
  };

  private Action toggleBackground = new AbstractAction("show/hide background") {
    public void actionPerformed(ActionEvent e) {
      MapPanel.toggleBackgroundVisibility();
      MapPanel.repaint();
    }
  };

  private Action addSegmentAction = new AbstractAction("Add Segment"){
    public void actionPerformed(ActionEvent e){
      m.addSegment();
      // code to update segList
      segList.clearSelection();
      segList.setListData(m.getSegs());
      segList.validate();
      //                MapPanel.repaint(); // Don't need to repaint, as nothing should change visually
    }
  };

  private Action removeSegmentAction = new AbstractAction("Remove Selected Segment"){
    public void actionPerformed(ActionEvent e){
      Segment selectedSegment = (Segment) segList.getSelectedValue();
      if(selectedSegment == null){
        JOptionPane.showMessageDialog(MapPanel, "Please select a segment and try again");
        return;
      }
      //                 remove all associate waypoints from wayVector
      for(Lane l: selectedSegment.getLanes()){
        for(WayPoint w: l.getWayPoints()){
          wayVector.remove(w);
        }
      }
      try{
        Thread.sleep(0);
      }catch (InterruptedException f){

      }
      // Globally delete any exit-entrance pair that features an entrance from the removed lane
      for(Segment s : m.getSegs()){
        for(Lane l: s.getLanes()){
          l.laneUpdateEntrances(selectedSegment.getId(), -1, -1, -1, -1);
          // This call indicates that the segment is getting deleted, so all effected entrances should
          // get changed.  The first -1 indicates segment deletion, the rest are place holders
        }
      }
      segList.clearSelection();
      // remove the segment
      m.removeSegment(selectedSegment.getId());
      segList.setListData(m.getSegs());
      segList.validate();
      MapPanel.repaint();
    }
  };

  private Action addLaneAction = new AbstractAction("Add a Lane"){
    public void actionPerformed(ActionEvent e){
      Segment selectedSegment;
      try{
        selectedSegment = (Segment) segList.getSelectedValue();
        int[] id = new int[2];
        id[0] = selectedSegment.getId();
        id[1] = selectedSegment.num_lanes + 1;
        Lane l = new Lane(id);
        selectedSegment.addLane(l);
        laneList.clearSelection();
        laneList.setListData(selectedSegment.getLanes());
        laneList.validate();
        MapPanel.repaint();
      }catch (NullPointerException f){
        JOptionPane.showMessageDialog(MapPanel, "Error adding lane, null pointer");
        return;
      }
    }
  };

  private Action removeLaneAction = new AbstractAction("Remove Selected Lane"){
    public void actionPerformed(ActionEvent e){
      Segment selectedSegment;
      try{
        selectedSegment = (Segment) segList.getSelectedValue();
        Lane selectedLane = (Lane) laneList.getSelectedValue();
        selectedSegment.removeLane(selectedLane.getId()[1], m);
        //removeLane takes care of updating the exit/entrance pairs
        // Update wayVector to not include the waypoints from the selected lane
        for(WayPoint w: selectedLane.getWayPoints()){
          wayVector.remove(w);
        }
        // Update the GUI
        laneList.clearSelection();
        laneList.setListData(selectedSegment.getLanes());
        laneList.validate();
        MapPanel.repaint();
      } catch (NullPointerException f){
        JOptionPane.showMessageDialog(MapPanel, "Error in selection, Null Pointer Exception\n please try again");
        return;
      }
    }
  };

  /**
   * Calls up a dialog to edit entrance/exit pairs
   */
  private Action editExitAction = new AbstractAction("Edit entrance/exit pairs"){
    Lane l; // The lane to be edited.
    JFrame editFrame;  // Where the editting dialogs are displayed
    JList selectionList; // Where the user selects which exit pair to edit.
    Vector<String> exitName;

    public void actionPerformed(ActionEvent e){
      // Edit the exits of the lane selected in the lane list
      l = (Lane) laneList.getSelectedValue();
      if(l == null){
        JOptionPane.showMessageDialog(MapPanel, "Please slecet a lane and try again"); // Display an error message, and exit
        return;
      }
      editFrame = new JFrame("Edit exits of lane " + l.getId()[0] + "." + l.getId()[2]);
      editFrame.setLayout(new BorderLayout());
      editFrame.setSize(300, 400);
      editFrame.setVisible(true);
      // Point exits at the exit vector of th elane to be processed, so that exit/entrance
      // pairs can be added to it
      // Need to create a vector of strings to allow for a reasonable list of the entrance
      // and exit pairs to be displayed
      exitName = new Vector();
      for(int[][] i:l.getExits()){
        String name = new String();
        for(int j = 0; j <=1; j++){
          // Construct the name of the waypoints
          name = name + i[j][0] + "." + i[j][1] + "." + i[j][2] + " ";
        }
        //                    System.out.println("Adding name al;skfdja;sldkfja;lskdfja;lskdjf;alskdjf;alskdfj " + name);
        exitName.add(name);
      }
      //                System.out.println(exitName.size());
      selectionList = new JList(exitName);
      //                selectionList.setListData(exitName);
      selectionList.setSize(200, 300);
      JScrollPane scroll = new JScrollPane(selectionList);
      editFrame.add(scroll, BorderLayout.CENTER);
      // Create buttons to edit the exit pair
      JButton addExitButton = new JButton("Add");
      JButton removeExitButton = new JButton("Remove");
      //Create a panel to hold the buttons
      JPanel buttonPane = new JPanel();
      addExitButton.addActionListener(new AddExitListener());
      buttonPane.add(addExitButton);
      removeExitButton.addActionListener(new RemoveListener());
      buttonPane.add(removeExitButton);
      editFrame.add(buttonPane, BorderLayout.SOUTH);

      //                editFrame.pack();
      //                editFrame.setVisible(true);
      editFrame.validate();
    }
    /** Removes an exit pair */
    class RemoveListener implements ActionListener{
      public void actionPerformed(ActionEvent e){
        int index = selectionList.getSelectedIndex(); // Get he index of the pair to remove
        if(index == -1){
          JOptionPane.showMessageDialog(editFrame, "Please select an exit pair to remove");
          return;
        }
        // Remove the pair from the lane list
        l.getExits().remove(index);
        // Remove the name from the selection list
        exitName.remove(index);
        // Update the UI
        selectionList.setListData(exitName);
        editFrame.validate();
      }
    }

    /** Adds an exit pair */
    class AddExitListener implements ActionListener {
      JFrame addFrame;
      JList addExitList;
      JList addEntranceList;

      public void actionPerformed(ActionEvent e){
        // Create a panel for adding an entrance-exit pair
        addFrame = new JFrame();
        addFrame.setLayout(new BorderLayout());
        JPanel exitPanel = new JPanel();
        exitPanel.setLayout(new BoxLayout(exitPanel, BoxLayout.Y_AXIS));
        JPanel entrancePanel = new JPanel();
        entrancePanel.setLayout(new BoxLayout(entrancePanel, BoxLayout.Y_AXIS));
        JPanel buttonPanel = new JPanel();
        JButton OKButton = new JButton("OK");
        OKButton.addActionListener(new OKListener());
        JButton CancelButton = new JButton("CancelButton");
        CancelButton.addActionListener(new CancelListener());
        buttonPanel.add(OKButton);
        buttonPanel.add(CancelButton);
        // Add a list of all waypoints to the two lists, and assemble the frame
        addExitList = new JList(l.getWayPoints());
        JScrollPane exitScroll = new JScrollPane(addExitList);
        exitScroll.setSize(200, 300);
        addEntranceList = new JList(wayVector);
        JScrollPane entranceScroll = new JScrollPane(addEntranceList);
        exitScroll.setSize(200, 300);


        exitPanel.add(new JLabel("Select the exit waypoint"));
        exitPanel.add(exitScroll);
        addFrame.add(exitPanel, BorderLayout.EAST);
        entrancePanel.add(new JLabel("Select the entrance waypoint"));
        entrancePanel.add(entranceScroll);
        addFrame.add(entrancePanel, BorderLayout.EAST);
        addFrame.add(exitPanel, BorderLayout.WEST);

        addFrame.add(buttonPanel, BorderLayout.SOUTH);
        addFrame.pack();
        addFrame.setVisible(true);
      }
      /** Exit without doing anything */
      class CancelListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
          addFrame.setVisible(false);
          return;
        }
      }

      /** Add the exit pair to the lane */
      class OKListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
          WayPoint exit = (WayPoint) addExitList.getSelectedValue();
          WayPoint entrance = (WayPoint) addEntranceList.getSelectedValue();
          if(exit == null || entrance == null){
            JOptionPane.showMessageDialog(addFrame, "Need to select an entrance and exit");
            return;
          }
          int[][] newExitPair = new int[2][3];
          newExitPair[0][0] = exit.getId()[0];
          newExitPair[0][1] = exit.getId()[1];
          newExitPair[0][2] = exit.getId()[2];
          newExitPair[1][0] = entrance.getId()[0];
          newExitPair[1][1] = entrance.getId()[1];
          newExitPair[1][2] = entrance.getId()[2];
          l.getExits().add(newExitPair); 
          exitName.add(exit.getId()[0] + "." + exit.getId()[1] + "." + exit.getId()[2] + 
              " " + entrance.getId()[0] + "." + entrance.getId()[1] + "." + entrance.getId()[2]);
          addFrame.setVisible(false); // Close the add frame
          selectionList.setListData(exitName);
          selectionList.validate(); // update the selection list

          editFrame.validate();
        }
      }
    }
  };

  /**
   * Causes the map to be exported into RNDF format under the filename "Output.rndf"
   */
  private Action exportRNDFAction = new AbstractAction("Export RNDF") {
    public void actionPerformed(ActionEvent e) {
      // Creates a file chooser to choose the location to save the rndf
      JFileChooser chooser = new JFileChooser();
      int returnVal = chooser.showSaveDialog(myWindow);
      if(returnVal == JFileChooser.APPROVE_OPTION){
        m.writeMap(chooser.getName(chooser.getSelectedFile()));
      }
    }
  };



  /**
   * Provides panning about the map when the user drags the mouse.  This is
   * accomplished by listening to mouseMoved events to keep track of where the 
   * mouse has been (only need to watch the last position of the mouse)  THis can
   * be used to determine the motion as a result of a drag.
   */
  class MapMouseMotionListener extends MouseMotionAdapter{
    /** Keeps track of where the mouse was last cycle */
    Point mouseLocation;
    /** The MapView the MapMouseMotionListener is associated with */
    MapView view;
    /** The keyListener used to determine if it should zoom when the mouse is moved */
    MapKeyListener keyListener;

    public MapMouseMotionListener(MapView view){
      this.view = view;
    }

    public MapMouseMotionListener(MapView view, MapKeyListener keyListener){
      this.view = view;
      this.keyListener = keyListener;
    }

    public void mouseMoved(MouseEvent e){
      if(keyListener != null && keyListener.isDepressed()){
        double zoomRate = 0.05; // Determines rate of zoom
        int dy = MouseInfo.getPointerInfo().getLocation().y - mouseLocation.y;
        double zoomFactor = Math.pow(0.8, -1*dy*zoomRate);
        view.getP().setScale(view.getP().getScale()*zoomFactor);
        perScale.setText("" + (float) view.getP().getScale());
        view.repaint();
      }
      mouseLocation = MouseInfo.getPointerInfo().getLocation();
    }

    public void mouseDragged(MouseEvent e){
      Point newLocation = MouseInfo.getPointerInfo().getLocation();
      double dX = view.getP().pixel2Lat(newLocation.x)-view.getP().pixel2Lat(mouseLocation.x);
      double dY = view.getP().pixel2Lon(newLocation.y)-view.getP().pixel2Lon(mouseLocation.y);
      double newXOffset = view.getP().getXOffset() + dX;
      double newYOffset = view.getP().getYOffset() + dY;
      // Update the perspective used by the map with the new offsets
      view.getP().setXOffset(newXOffset);
      view.getP().setYOffset(newYOffset); 
      // Update the offset textfields with the new cooridnates 
      xOffset.setText("" + (float) newXOffset);
      yOffset.setText("" + (float) newYOffset);
      view.repaint();
      mouseLocation = newLocation;
      view.requestFocusInWindow();
    }

  }

  /**
   * Allows for the zooming of the map by use of the scroll wheel
   */
  class MapZoomWheelListener implements MouseWheelListener{
    private MapView view;

    public MapZoomWheelListener(MapView view){
      this.view = view;

    }

    public void mouseWheelMoved(MouseWheelEvent e){
      double zoomFactor = Math.pow(0.8, -1*e.getWheelRotation());
      view.getP().setScale(view.getP().getScale()*zoomFactor);
      perScale.setText("" + (float) view.getP().getScale());
      view.repaint();
    }
  }

  /**
   * Keeps track of whether a specific key is depressed, for use in press button and move
   * mouse controls.  The user must first drag the mouse to get the keyboard focus onto
   * the desired view.
   */
  class MapKeyListener extends KeyAdapter{
    private boolean depressed = false;

    public void keyPressed(KeyEvent e){
      if(e.getKeyCode() == KeyEvent.VK_Z){
        depressed = true;
      }
    }

    public void keyReleased(KeyEvent e){
      if(e.getKeyCode() == KeyEvent.VK_Z){
        depressed = false;
      }
    }

    public boolean isDepressed() {
      return depressed;
    }
  }

  /**
   * Interpretes mouse clicks
   */
  class ClickWayPointListener extends MouseAdapter {

    public void mouseReleased(MouseEvent e){
      // Check if this should bring up a popup menu
      if(e.isPopupTrigger()){
        JPopupMenu pop = new JPopupMenu();
        // Create a menu item for adding waypoints
        JMenuItem addItem = new JMenuItem("Add Waypoint");
        addItem.addActionListener(new MenuWaypointAddListener(e));
        pop.add(addItem);

        //Creates a menu item to remove the selected waypoint
        JMenuItem removeWaypoint = new JMenuItem("Remove the selected waypoint");
        removeWaypoint.addActionListener(new RemoveWaypointListener());
        pop.add(removeWaypoint);

        // create a menu item for moving the selected waypoint
        JMenuItem moveItem = new JMenuItem("Move Waypoint");
        moveItem.addActionListener(new MenuWaypointMoveListener(e));
        pop.add(moveItem);

        // creates a menu item to textualy edit the selected waypoint
        JMenuItem editWaypoint = new JMenuItem("Edit Selected Waypoint");
        editWaypoint.addActionListener(new MenuWaypointEditListener());
        pop.add(editWaypoint);

        // creates a menu item to textually edit the selected waypoint in utm coordinates
        JMenuItem editUTMWaypoint = new JMenuItem("Edite Selected Waypoint using UTM");
        editUTMWaypoint.addActionListener(new MenuUTMWaypointEditListener());
        pop.add(editUTMWaypoint);

        // create a menu to split a lane into two (as part of splitting a segment)
        JMenuItem splitLane = new JMenuItem( "Split lane at this waypoint" );
        splitLane.addActionListener( new SplitLaneListener() );
        pop.add( splitLane );

        // create a menu to split a lane into two (as part of splitting a segment)
        JMenuItem moveLane = new JMenuItem( "Move selected Lane to Segment..." );
        moveLane.addActionListener( new MoveLaneToSegmentListener() );
        pop.add( moveLane );



        pop.show(e.getComponent(), e.getX(), e.getY());
        MapPanel.repaint();
      }
    }

    /**
     * Note: Large chunks of the code has been replaced by a right click menu.  The only
     * left click it needs to worry about is for selecting waypoints.
     * 
     * Takes various action when the mouse is clicked in the MapView, depending on
     * the setting of certain buttons in the control Panel
     */
    public void mouseClicked(MouseEvent e){


      // If it wasn't the left mouse button, or the clickSelectButton or if the 
      // dragWayPointButton isn't selected don't do anything
      if((e.getButton() != MouseEvent.BUTTON1)) return; // || clickNothingButton.isSelected()){


      // If there aren't any waypoints in wayVector, exit
      if(wayVector.size() == 0){
        return;
      }
      //Find the closest waypoint to the click
      double x = MapPanel.getP().pixel2Lat(e.getX());
      double y = MapPanel.getP().pixel2Lon(e.getY());
      WayPoint closest = wayVector.elementAt(0);
      double distance = Math.sqrt(Math.pow(x-closest.getX(), 2)+
          Math.pow(y - closest.getY(), 2));
      for(WayPoint w: wayVector){
        double newDistance = Math.sqrt(Math.pow(x-w.getX(), 2)+
            Math.pow(y - w.getY(), 2));
        if(newDistance < distance){
          distance = newDistance;
          closest = w;
        }
      }
      // Check that the waypoint that will be selected is relatively close (pixel wise)
      // to the point of interest
      double pixelDist = 10;
      x = e.getX();
      y = e.getY();
      double targX = MapPanel.getP().cX(closest.getX());
      double targY = MapPanel.getP().cY(closest.getY());
      distance = Math.sqrt(Math.pow(x-targX, 2)+
          Math.pow(y - targY, 2));
      if(distance <= pixelDist){
        //Set the selection of closest to be the opposite of the present value
        // Set the selected segment and lane to corrospond to that of the waypoint
        // Assume that the lists are kept sorted

        if(closest.isSelected()){
          int index = wayVector.indexOf(closest);
          wayPointList.getSelectionModel().removeSelectionInterval(index, index);
          closest.setSelected(false);
          //closest.setSelected(false);
        }else{
          int index = wayVector.indexOf(closest);
          wayPointList.getSelectionModel().addSelectionInterval(index, index);
          closest.setSelected(true);
          //closest.setSelected(true);
        }
      }
    }

    // check under mouse pressing for a menu triggering event for platform compatibillity
    public void mousePressed(MouseEvent e) {
      // Check if this should bring up a popup menu
      if(e.isPopupTrigger()){
        JPopupMenu pop = new JPopupMenu();
        // Create a menu item for adding waypoints
        JMenuItem addItem = new JMenuItem("Add Waypoint");
        addItem.addActionListener(new MenuWaypointAddListener(e));
        pop.add(addItem);

        //Creates a menu item to remove the selected waypoint
        JMenuItem removeWaypoint = new JMenuItem("Remove the selected waypoint");
        removeWaypoint.addActionListener(new RemoveWaypointListener());
        pop.add(removeWaypoint);

        // create a menu item for moving the selected waypoint
        JMenuItem moveItem = new JMenuItem("Move Waypoint");
        moveItem.addActionListener(new MenuWaypointMoveListener(e));
        pop.add(moveItem);

        // creates a menu item to textualy edit the selected waypoint
        JMenuItem editWaypoint = new JMenuItem("Edit Selected Waypoint");
        editWaypoint.addActionListener(new MenuWaypointEditListener());
        pop.add(editWaypoint);

        // creates a menu item to textually edit the selected waypoint in utm coordinates
        JMenuItem editUTMWaypoint = new JMenuItem("Edite Selected Waypoint using UTM");
        editUTMWaypoint.addActionListener(new MenuUTMWaypointEditListener());
        pop.add(editUTMWaypoint);

        // create a menu to split a lane into two (as part of splitting a segment)
        JMenuItem splitLane = new JMenuItem( "Split lane at this waypoint" );
        splitLane.addActionListener( new SplitLaneListener() );
        pop.add( splitLane );

        // create a menu to split a lane into two (as part of splitting a segment)
        JMenuItem moveLane = new JMenuItem( "Move selected Lane to Segment..." );
        moveLane.addActionListener( new MoveLaneToSegmentListener() );
        pop.add( moveLane );

        // add items to set the corner poitns of the background image
        JMenuItem backgroundLL = new JMenuItem( "Set backgrounds lower-left point" );
        backgroundLL.addActionListener( new MenuBackgroundListener(MapPanel, true) );
        pop.add( backgroundLL );
        JMenuItem backgroundUR = new JMenuItem( "Set backgrounds upper-right point" );
        backgroundUR.addActionListener( new MenuBackgroundListener(MapPanel, false) );
        pop.add( backgroundUR );


        pop.show(e.getComponent(), e.getX(), e.getY());
        MapPanel.repaint();
      }
    }

    }

    /**
     * Moves the selected waypoint to the point where the menu was pulled up when
     * the corrosponding menu item is selected.
     */
    class MenuWaypointMoveListener implements ActionListener{
      /** The mouse event that created the menu this listens to */
      MouseEvent e;

      public MenuWaypointMoveListener(MouseEvent e){
        this.e = e;
      }

      public void actionPerformed(ActionEvent f){
        // search wayList for the selected wayPoint
        WayPoint w = null;
        for(WayPoint wa: wayVector){
          if(wa.isSelected())
            w = wa;
        }
        try{
          w.setX(MapPanel.getP().pixel2Lat(e.getX()));
          w.setY(MapPanel.getP().pixel2Lon(e.getY()));
          MapPanel.repaint();
        }catch (NullPointerException g){

        }
      }
    }

    class MenuBackgroundListener implements ActionListener {
      MapView view;
      boolean left;

      public MenuBackgroundListener( MapView v, boolean leftCorner ) {
        this.view = v;
        this.left = leftCorner;
      }

      public void actionPerformed( ActionEvent e) {
        if( view == null ) {
          System.out.println( "no view for the backgroundlistener" );
          return;
        }
        if( left )
          view.setBackgroundLowerLeft( view.lat, view.lon );
        else
          view.setBackgroundUpperRight( view.lat, view.lon );
        view.repaint();
      }
    }

    /**
     * Adds a waypoint when a menu item is selected.
     */
    class MenuWaypointAddListener implements ActionListener{
      MouseEvent me; //The mouse even that created the menu this listens for

      public MenuWaypointAddListener(MouseEvent e){
        me = e;
      } 

      public void actionPerformed(ActionEvent e) {
        Lane selectedLane;
        try{
          selectedLane = (Lane) laneList.getSelectedValue();
          int[] id = new int[3];
          id[0] = selectedLane.getId()[0];
          id[1] = selectedLane.getId()[1];
          id[2] = selectedLane.getNum_waypoints()+1;
          double x = MapPanel.getP().pixel2Lat(me.getX());
          double y = MapPanel.getP().pixel2Lon(me.getY());
          WayPoint w = new WayPoint((int[]) id, x, y);
          selectedLane.getWayPoints().add(w);
          // add w to the vector holding all waypoints for UI purposes
          wayVector.add(w);
          wayPointList.setListData(selectedLane.getWayPoints());
          // Increment the number of waypoints
          selectedLane.setNum_waypoints(selectedLane.getNum_waypoints() + 1);
        } catch (NullPointerException f){
          System.err.println("Error in lane selection");
        }
      }
    }

    //        /**
    //         * Adds a lane
    //         */
    //        class LaneAddListener implements ActionListener {
    //            
    //            public void actionPerformed(ActionEvent e){
    //                Segment selectedSegment;
    //                try{
    //                    selectedSegment = (Segment) segList.getSelectedValue();
    //                    int[] id = new int[2];
    //                    id[0] = selectedSegment.getId();
    //                    id[1] = selectedSegment.num_lanes + 1;
    //                    Lane l = new Lane(id);
    //                    selectedSegment.addLane(l);
    //                    laneList.clearSelection();
    //                    laneList.setListData(selectedSegment.getLanes());
    //                    laneList.validate();
    //                    MapPanel.repaint();
    //                }catch (NullPointerException f){
    //                    JOptionPane.showMessageDialog(MapPanel, "Error adding lane, null pointer");
    //                    return;
    //                }
    //            }
    //        }
    //        
    //        /**
    //         * Removes a lane from the currently selected segment
    //         */
    //        class LaneRemoveListener implements ActionListener {
    //            
    //            public void actionPerformed(ActionEvent e){
    //                Segment selectedSegment;
    //                try{
    //                    selectedSegment = (Segment) segList.getSelectedValue();
    //                    Lane selectedLane = (Lane) laneList.getSelectedValue();
    //                    selectedSegment.removeLane(selectedLane.getId()[1]);
    //                    // Update wayVector to not include the waypoints from the selected lane
    //                    for(WayPoint w: selectedLane.getWayPoints()){
    //                        wayVector.remove(w);
    //                    }
    //                    // Update the GUI
    //                    laneList.clearSelection();
    //                    laneList.setListData(selectedSegment.getLanes());
    //                    laneList.validate();
    //                    MapPanel.repaint();
    //                } catch (NullPointerException f){
    //                    JOptionPane.showMessageDialog(MapPanel, "Error in selection, Null Pointer Exception\n please try again");
    //                    return;
    //                }
    //            }
    //        }

    //        /**
    //         * Removes the selected lane
    //         */
    //        class LaneRemoveListener implements ActionListener {
    //            public void actionPerformed(ActionEvent e){
    //                try{
    //                    Segment selectedSegment = (Segment) segList.getSelectedValue();
    //                    Lane l = (Lane) laneList.getSelectedValue();
    //                    boolean elementOf = false;
    //                    for(int i = 0; i < selectedSegment.getLanes().length; i++){
    //                        if(selectedSegment.getLanes()[i] == l){
    //                            elementOf = true;
    //                        }
    //                    }
    //                    // Indicate error if the lane is not an element of the selected segment
    //                    if(!elementOf){
    //                        JOptionPane.showMessageDialog(MapPanel, "Sync error, Lane not an element of the segment\n Please try again");
    //                        return;
    //                    }
    //                    int returnValue = selectedSegment.removeLane(l.getId()[1]);
    //                    if(returnValue == -1){
    //                        JOptionPane.showMessageDialog(MapPanel, "Error removing lane");
    //                        return;
    //                    }
    //                } catch (NullPointerException f){
    //                    JOptionPane.showMessageDialog(MapPanel, "Error with selection lists");
    //                    return;
    //                }
    //            }
    //        }

    ///////////////////////////////////////////////////////////////////////////////////
    ////
    ///   Replaced by Actions
    ///
    /////////////////////////////////////////////////////////////////////////

    //        class SegmentAddListener implements ActionListener{
    //            public void actionPerformed(ActionEvent e){
    //                m.addSegment();
    //                segList.clearSelection();
    //                segList.setListData(m.getSegs());
    //                segList.validate();
    ////                MapPanel.repaint(); // Don't need to repaint, as nothing should change visually
    //            }
    //        }
    //        
    //        class SegmentRemoveListener implements ActionListener {
    //            public void actionPerformed(ActionEvent e){
    //                Segment selectedSegment = (Segment) segList.getSelectedValue();
    ////                 remove all associate waypoints from wayVector
    //                for(Lane l: selectedSegment.getLanes()){
    //                    for(WayPoint w: l.getWayPoints()){
    //                        wayVector.remove(w);
    //                    }
    //                }
    //                try{
    //                    Thread.sleep(0);
    //                }catch (InterruptedException f){
    //                    
    //                }
    //                // Globally delete any exit-entrance pair that features an entrance from the removed lane
    //                for(Segment s : m.getSegs()){
    //                    for(Lane l: s.getLanes()){
    //                        l.laneUpdateEntrances(selectedSegment.getId(), 0, 0);
    //                        // This call indicates that the segment is getting deleted, so all effected entrances should
    //                        // get changed
    //                    }
    //                }
    //                segList.clearSelection();
    //                // remove the segment
    //                m.removeSegment(selectedSegment.getId());
    //                segList.setListData(m.getSegs());
    //                segList.validate();
    //                MapPanel.repaint();
    //            }
    //        }

    /**
     *  Splits a lane into two halves, useful in splitting a segment into
     *  pieces.
     *
     */
    class SplitLaneListener implements ActionListener{

      public void actionPerformed(ActionEvent e){
        System.out.println( "starting lane split" );

        /// find the selected waypoint
        WayPoint w = null;
        for(WayPoint y: wayVector){
          if(y.isSelected()) {
            w = y;
            break;
          }
        }
        if(w == null){
          System.out.println("No selected waypoint");
          return;
        }

        System.out.println( "using waypoint: " + w.toString() );

        /// find the encompassing segment, lane and process..
        if(w.getId()[0] <= m.getNum_segments()) {
          int segidx = w.getId()[0] - 1;
          int laneidx = w.getId()[1] - 1;
          Segment s = m.getSegs()[segidx];
          Lane   ol = s.getLanes()[laneidx];
          int[] oid = Ids.copy( ol.getId() );

          // debugging 
          System.out.println( "Old lane id: " + ol.toString() + "  :" );
          for( int i: oid )
            System.out.print( i + "." );
          System.out.println( "" );

          // get the id
          int[] id = new int[2];
          id[0] = oid[0];
          id[1] = s.getLanes().length + 1;
          
          // get a new lane
          Lane nl = new Lane(id);
          s.addLane( nl );

          // leave initial waypoints in original lane, as the selected waypoint is crossed,
          // begin moving them to the new lane
          boolean bMove = false;
          Iterator<WayPoint> ii = ol.getWayPoints().iterator(); 
          for( ; ii.hasNext(); ) {
            WayPoint wp = ii.next();
            if( wp == w ) {
              bMove = !bMove;
            }
            if( bMove ) {
              ol.transferWayPoint( wp, nl, m );
              ii.remove();
            } else {
            }
          }
          /// adjust the waypoint counts
          nl.setNum_waypoints( nl.getWayPoints().size() );
          ol.setNum_waypoints( ol.getWayPoints().size() );


          // add an exit from the old to the new
          int[][] exit = new int[2][3];
          final int ENTRANCE = Lane.ENTRANCE;
          final int EXIT = Lane.EXIT;
          exit[EXIT][0] = exit[1][0] = id[0];
          exit[EXIT][1] = oid[1];
          exit[EXIT][2] = ol.getWayPoints().size();
          exit[ENTRANCE][1] = id[1];
          exit[ENTRANCE][2] = 1;
          Vector<int[][]> exits = ol.getExits();
          exits.add(exit);
          ol.setExits( exits );

        } else {
          System.out.println( "Waypoint does not belong to a segment" );
          return;
        }

        MapPanel.repaint();
      }
    }

    class MoveLaneToSegmentListener implements ActionListener {
      JTextField segId = new JTextField( "" );
      JButton acceptButton = new JButton("Accept");
      JButton cancelButton = new JButton( "Cancel" );
      JTextField segfield = new JTextField( "" );
      JFrame editFrame;
      Lane l = null;

      public void actionPerformed( ActionEvent e ) {
        l = (Lane) laneList.getSelectedValue();
        if( l == null ) {
          System.out.println( "No lane selected" );
          return;
        }

        editFrame = new JFrame("Transfer Lane " + l);
        editFrame.setLayout(new GridLayout(0,2));
        editFrame.add(new JLabel("Segement id (only most signif):"));
        segfield.setText("1");
        editFrame.add(segfield);

        // Creates a panel to hold the accept or cancel buttons
        JPanel finishPanel = new JPanel();
        // add the needed listeners
        acceptButton.addActionListener(new AcceptButtonListener());
        cancelButton.addActionListener(new CancelButtonListener());
        finishPanel.add(acceptButton);
        finishPanel.add(cancelButton);
        editFrame.add(finishPanel);

        editFrame.pack();
        editFrame.setVisible(true);
         
        MapPanel.repaint();
      }

      class CancelButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
          // not sure how to totally kill a frame
          editFrame.setEnabled(false);
          editFrame.setVisible(false);
        }
      }

      class AcceptButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
          // field based on whether or not the entered id is valid
          int checkId = Integer.parseInt(segfield.getText());
          if( checkId < 1 || checkId > m.getSegs().length ) {
            System.out.println( "Invalid segment: " + checkId );
            return ;
          }

          /// get the associated segments
          Segment starget = m.getSegs()[checkId-1];
          Segment ssource = m.getSegs()[ l.getId()[0]-1 ];

          /// make the transfer
          ssource.transferLane( l, starget, m );
          
          /// close the frame
          editFrame.setEnabled(false);
          editFrame.setVisible(false);
          MapPanel.repaint();
        }
      }
    }

    /**
     * Creates a fram where you can edit the selected waypoint
     * 
     * Needs x, y, checkpoint, stop
     * 
     */
    class MenuWaypointEditListener implements ActionListener{
      /** Holds the field to edit the latitude of the waypoint. */
      JTextField latField = new JTextField("");
      /** Field to edi the longitude of the waypoint */
      JTextField lonField = new JTextField("");
      /** Holds the field to determine whether it is a checkpoint */
      JTextField checkField = new JTextField("");
      /** Will determine if the waypoint is a stop sign */
      JRadioButton stopButton = new JRadioButton();
      JFrame editFrame;
      /** Accepts changes */
      JButton acceptButton = new JButton("Accept");
      /** Cancels changes */
      JButton cancelButton = new JButton("Cancel");
      /** Whether the selected waypoint is in a segment */
      boolean isSegment = true;

      public void actionPerformed(ActionEvent e){
        WayPoint w = null;
        for(WayPoint y: wayVector){
          if(y.isSelected())
            w = y;
        }
        if(w == null){
          System.out.println("No selected waypoint");
          return;
        }
        editFrame = new JFrame("Edit " + w);
        editFrame.setLayout(new GridLayout(0,2));
        editFrame.add(new JLabel("Latitude"));
        latField.setText("" + w.getX());
        editFrame.add(latField);
        editFrame.add(new JLabel("Longitude"));
        lonField.setText("" + w.getY());
        editFrame.add(lonField);
        // Create a panel to hold a multi line lable
        JPanel labelPan = new JPanel();
        labelPan.setLayout(new BoxLayout(labelPan, BoxLayout.Y_AXIS));
        labelPan.add(new JLabel("Checkpoint ID"));
        labelPan.add(new JLabel("<1 implies not a checkpoint"));
        editFrame.add(labelPan);
        checkField.setText("" + w.getCheckpointId());
        editFrame.add(checkField);
        // Need to use a named JLabel to allow for its removal if need be
        JLabel stopLabel = new JLabel("Is a stop sign");
        editFrame.add(stopLabel);
        editFrame.add(stopButton);
        if(w.getId()[0] <= m.getNum_segments()){
          stopButton.setSelected(m.getSegs()[w.getId()[0]-1].getLanes()[w.getId()[1]-1].getStopSign().contains(w));
        }else{
          editFrame.remove(stopLabel);
          editFrame.remove(stopButton);
          isSegment = false;
        }
        // Creates a panel to hold the accept or cancel buttons
        JPanel finishPanel = new JPanel();
        // add the needed listeners
        acceptButton.addActionListener(new AcceptButtonListener(w));
        cancelButton.addActionListener(new CancelButtonListener());
        finishPanel.add(acceptButton);
        finishPanel.add(cancelButton);
        editFrame.add(finishPanel);

        editFrame.pack();
        editFrame.setVisible(true);

      }

      class CancelButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
          // not sure how to totally kill a frame
          editFrame.setEnabled(false);
          editFrame.setVisible(false);
        }
      }

      class AcceptButtonListener implements ActionListener{
        WayPoint w; // The waypoint to edit

        public AcceptButtonListener(WayPoint w){
          this.w = w;
        }

        public void actionPerformed(ActionEvent e){
          // Sets whether this is a checkpoint
          // The setCheckpointId will automatically update the checkpoint
          // field based on whether or not the entered id is valid
          int checkId = Integer.parseInt(checkField.getText());
          // Check that there isn't another waypoint currently assigned to that id value
          for(WayPoint way: wayVector){
            if((checkId == way.getCheckpointId()) && checkId != 0  && way != w){
              // Already a waypoint with that checkpoint Id, so display a dialog box to
              // inform the user of the fact, and don't do anything else
              JOptionPane.showMessageDialog(MapPanel, "The specified checkpoint ID is already in use.\n  Please try " +
                  "a different value");
              checkField.setText("" + w.getCheckpointId());
              editFrame.toFront();
              return;
            }
          }
          w.setCheckpointId(Integer.parseInt(checkField.getText()));
          // Updates the coordinates of the waypoint
          w.setX(Double.parseDouble(latField.getText()));
          w.setY(Double.parseDouble(lonField.getText()));
          if(isSegment){
            // Updates the stop sign field
            Lane l = m.getSegs()[w.getId()[0]-1].getLanes()[w.getId()[1]-1];
            if(stopButton.isSelected()){
              if(!l.getStopSign().contains(w)){
                l.getStopSign().add(w);
              }else{
                System.err.println("Already a stop sign");
              }
            }else{
              // IF the stop sign button isn't selected, and the waypoint is currently listed as a stop
              // sign, remove it from the stop sign list.'
              if(l.getStopSign().contains(w)){
                l.getStopSign().removeElement(w);
              }else{
                System.err.println("Already not a stop sign");
              }
            }
          }
          editFrame.setEnabled(false);
          editFrame.setVisible(false);
          MapPanel.repaint();
        }
      }
    }

    class MapLatLonTransformListener implements ActionListener{
      JFrame editFrame;
      JTextField latCoord;
      JTextField lonCoord;
      JTextField angle;
      WayPoint w = null;

      public void actionPerformed(ActionEvent e){
        for(WayPoint way: wayVector){
          if(way.isSelected())
            w = way;
        }
        if(w == null){
          JOptionPane.showMessageDialog(MapPanel, "Please select a waypoint and try again");
          return;
        }
        editFrame = new JFrame("Transform Map Based on " + w);
        editFrame.setLayout(new GridLayout(0, 2));
        editFrame.add(new JLabel("Lattitude Coordinate"));
        latCoord = new JTextField("" + w.getX(), 20);
        editFrame.add(latCoord);
        editFrame.add(new JLabel("Longitude Coordinate"));
        lonCoord = new JTextField("" + w.getY(), 20);
        editFrame.add(lonCoord);
        editFrame.add(new JLabel("Angle to rotate"));    
        angle = new JTextField("" + 0, 20);
        editFrame.add(angle);
        JPanel buttonPanel = new JPanel();
        //                buttonPanel.setLayout(new GridLayout(1,2));
        JButton OKButton = new JButton("OK");
        OKButton.addActionListener(new OKButtonListener());
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new cancelButtonListener());
        buttonPanel.add(OKButton);
        buttonPanel.add(cancelButton);
        editFrame.add(buttonPanel);
        editFrame.pack();
        editFrame.setVisible(true);
      }
      class cancelButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
          editFrame.setVisible(false);
        }
      }
      class OKButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
          // Get the lat, lon of the selected waypoint
          double wX = w.getX();
          double wY = w.getY();
          for(WayPoint way: wayVector){
            //Translate the waypoints so the selected waypoint is at (0,0)
            double transX = way.getX() - wX;
            double transY = way.getY() - wY;
            // Calculate the rotate values
            double rot = 0;
            try{
              rot = Double.parseDouble(angle.getText())*Math.PI/180; // get angle, convert to radians
            } catch (NumberFormatException f){
              JOptionPane.showMessageDialog(editFrame, "Angle field must be formated as a number");
              System.err.println(e);
              return;
            }
            // Multiply the angle by -1 to conform to standard angle sign conventions
            double rotX = Math.cos(-1*rot)*transX - Math.sin(-1*rot)*transY;
            double rotY = Math.cos(-1*rot)*transY + Math.sin(-1*rot)*transX;
            try{
              way.setX(rotX+Double.parseDouble(latCoord.getText()));
              way.setY(rotY+Double.parseDouble(lonCoord.getText()));
            } catch (NumberFormatException f){
              JOptionPane.showMessageDialog(editFrame, "Coordinate fields must be in numeral format");
              System.out.println(e);
              return;
            }
          }
          MapPanel.setP(new Perspective(m, MapPanel.getWidth(), MapPanel.getHeight()));
          MapPanel.repaint();
          angle.setText(0 + "");
          editFrame.setVisible(false);
        }
      }
    }

    class MapUTMTransformListener implements ActionListener{
      JFrame editFrame;
      JTextField zone;
      JTextField easting;
      JTextField northing;
      JTextField angle;
      WayPoint w = null;

      public void actionPerformed(ActionEvent e){
        for(WayPoint way: wayVector){
          if(way.isSelected())
            w = way;
        }
        if(w == null){
          JOptionPane.showMessageDialog(MapPanel, "Please select a waypoint and try again");
          return;
        }
        editFrame = new JFrame("Transform Map Based on " + w);
        editFrame.setLayout(new GridLayout(0, 2));
        UTM utm = LatLongUTMConversion.LLtoUTM(ellipseIndex, w.getX(), w.getY());
        editFrame.add(new JLabel("Zone"));
        zone = new JTextField(utm.getZone());
        zone.setText(utm.getZone());
        editFrame.add(zone);
        editFrame.add(new JLabel("Easting Coordinate"));
        easting = new JTextField("" + utm.getEasting());
        editFrame.add(easting);
        editFrame.add(new JLabel("Northing Coordinate"));
        northing = new JTextField("" + utm.getNorthing(), 20);
        editFrame.add(northing);
        editFrame.add(new JLabel("Angle to rotate"));    
        angle = new JTextField("" + 0, 20);
        editFrame.add(angle);
        JPanel buttonPanel = new JPanel();
        //                buttonPanel.setLayout(new GridLayout(1,2));
        JButton OKButton = new JButton("OK");
        OKButton.addActionListener(new OKButtonListener());
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new cancelButtonListener());
        buttonPanel.add(OKButton);
        buttonPanel.add(cancelButton);
        editFrame.add(buttonPanel);
        editFrame.pack();
        editFrame.setVisible(true);
      }

      class cancelButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
          editFrame.setVisible(false);
        }
      }

      class OKButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
          // Get the lat, lon of the selected waypoint
          double wX = w.getX();
          double wY = w.getY();
          for(WayPoint way: wayVector){
            //Translate the waypoints so the selected waypoint is at (0,0)
            double transX = way.getX() - wX;
            double transY = way.getY() - wY;
            // Calculate the rotate values
            double rot = 0;
            try{
              rot = Double.parseDouble(angle.getText())*Math.PI/180; // get angle, convert to radians
            } catch (NumberFormatException f){
              JOptionPane.showMessageDialog(editFrame, "Angle field must be formated as a number");
              System.err.println(e);
              return;
            }
            double rotX = Math.cos(-1*rot)*transX - Math.sin(-1*rot)*transY;
            double rotY = Math.cos(-1*rot)*transY + Math.sin(-1*rot)*transX;
            try{
              LatLon translation = LatLongUTMConversion.UTMtoLL(ellipseIndex, Double.parseDouble(northing.getText()),
                  Double.parseDouble(easting.getText()), zone.getText());
              way.setX(rotX+translation.getLat());
              way.setY(rotY+translation.getLong());
            } catch (NumberFormatException f){
              JOptionPane.showMessageDialog(editFrame, "Coordinate fields must be in numeral format");
              System.out.println(e);
              return;
            }
          }
          MapPanel.setP(new Perspective(m, MapPanel.getWidth(), MapPanel.getHeight()));
          MapPanel.repaint();
          angle.setText(0 + "");
          editFrame.setVisible(false);
        }
      }
    }

    /**
     * Creates a fram where you can edit the selected waypoint using UTM waypoints
     * 
     * Needs zone, easting, northing, checkpoint, stop
     * 
     */
    class MenuUTMWaypointEditListener implements ActionListener{
      /** Holds the zone of the waypoint */
      JTextField zoneField = new JTextField("", 8);
      /** Holds the field to edit the latitude of the waypoint. */
      JTextField eastingField = new JTextField("", 8);
      /** Field to edi the longitude of the waypoint */
      JTextField northingField = new JTextField("", 8);
      /** Holds the field to determine whether it is a checkpoint */
      JTextField checkField = new JTextField("");
      /** Will determine if the waypoint is a stop sign */
      JRadioButton stopButton = new JRadioButton();
      JFrame editFrame;
      /** Accepts changes */
      JButton acceptButton = new JButton("Accept");
      /** Cancels changes */
      JButton cancelButton = new JButton("Cancel");
      /** Whether working on a segment or zone */
      boolean isSegment = true;

      public void actionPerformed(ActionEvent e){
        WayPoint w = null;
        for(WayPoint y: wayVector){
          if(y.isSelected())
            w = y;
        }
        if(w == null){
          System.out.println("No selected waypoint");
          return;
        }
        doSegmentEdit(w);
      }
      // performs textual editing for waypoints that are elements of segments
      private void doSegmentEdit(WayPoint w){
        editFrame = new JFrame("Edit " + w);
        editFrame.setLayout(new GridLayout(0,2));
        editFrame.add(new JLabel("Zone"));
        // Edit the fields to display the present UTM cooridnates
        UTM u = LatLongUTMConversion.LLtoUTM(ellipseIndex, w.getX(), w.getY());
        zoneField.setText(u.getZone());
        editFrame.add(zoneField);
        editFrame.add(new JLabel("Easting"));
        eastingField.setText("" + u.getEasting());
        editFrame.add(eastingField);
        editFrame.add(new JLabel("Northing"));
        northingField.setText("" + u.getNorthing());
        editFrame.add(northingField);
        // Create a panel to hold a multi line lable
        JPanel labelPan = new JPanel();
        labelPan.setLayout(new BoxLayout(labelPan, BoxLayout.Y_AXIS));
        labelPan.add(new JLabel("Checkpoint ID"));
        labelPan.add(new JLabel("<1 implies not a checkpoint"));
        editFrame.add(labelPan);
        checkField.setText("" + w.getCheckpointId());
        editFrame.add(checkField);
        JLabel stopLabel = new JLabel("Is a stop sign"); // use a named variable so it can be removed
        editFrame.add(stopLabel);
        editFrame.add(stopButton);
        // Need to check if this is part of a segment, and therefore may be a stop sign or not
        if(w.getId()[0] <= m.getNum_segments()){
          stopButton.setSelected(m.getSegs()[w.getId()[0]-1].getLanes()[w.getId()[1]-1].getStopSign().contains(w));
        }else{
          editFrame.remove(stopButton);
          editFrame.remove(stopLabel);
          isSegment = false;
        }
        // Creates a panel to hold the accept or cancel buttons
        JPanel finishPanel = new JPanel();
        // add the needed listeners
        acceptButton.addActionListener(new AcceptButtonListener(w));
        cancelButton.addActionListener(new CancelButtonListener());
        finishPanel.add(acceptButton);
        finishPanel.add(cancelButton);
        editFrame.add(finishPanel);

        editFrame.pack();
        editFrame.setVisible(true);
      }

      class CancelButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
          // not sure how to totally kill a frame
          editFrame.setEnabled(false);
          editFrame.setVisible(false);
        }
      }
      class AcceptButtonListener implements ActionListener{
        WayPoint w; // The waypoint to edit

        public AcceptButtonListener(WayPoint w){
          this.w = w;
        }

        public void actionPerformed(ActionEvent e){
          LatLon latlon = LatLongUTMConversion.UTMtoLL(ellipseIndex, Double.parseDouble(northingField.getText()),
              Double.parseDouble(eastingField.getText()), zoneField.getText());
          w.setX(latlon.getLat());
          w.setY(latlon.getLong());
          // Sets whether this is a checkpoint
          // The setCheckpointId will automatically update the checkpoint
          // field based on whether or not the entered id is valid
          int checkId = Integer.parseInt((checkField.getText()));
          // check that the checkpoint value isn't already in use
          for(WayPoint way: wayVector){
            if((checkId == way.getCheckpointId()) && checkId != 0  && way != w){
              // Already a waypoint with that checkpoint Id, so display a dialog box to
              // inform the user of the fact, and don't do anything else
              JOptionPane.showMessageDialog(MapPanel, "The specified checkpoint ID is already in use.\n  Please try " +
                  "a different value");
              checkField.setText("" + w.getCheckpointId());
              editFrame.toFront();
              return;
            }
          }
          w.setCheckpointId(checkId);
          if(isSegment){
            // Updates the stop sign field
            Lane l = m.getSegs()[w.getId()[0]-1].getLanes()[w.getId()[1]-1];
            if(stopButton.isSelected()){
              if(!l.getStopSign().contains(w)){
                l.getStopSign().add(w);
              }else{
                System.err.println("Already a stop sign");
              }
            }else{
              // IF the stop sign button isn't selected, and the waypoint is currently listed as a stop
              // sign, remove it from the stop sign list.'
              if(l.getStopSign().contains(w)){
                l.getStopSign().removeElement(w);
              }else{
                System.err.println("Already not a stop sign");
              }
            }
          }
          editFrame.setEnabled(false);
          editFrame.setVisible(false);
          MapPanel.repaint();
        }
      }
    }

    //        /**
    //         * Allows for changing the exit-entrance pairs in a lane
    //         */
    //        class ExitListener implements ActionListener {
    //            Lane l; // The lane to be edited.
    //            JFrame editFrame;  // Where the editting dialogs are displayed
    //            JList selectionList; // Where the user selects which exit pair to edit.
    //            Vector<String> exitName;
    //            
    //            public void actionPerformed(ActionEvent e){
    //                // Edit the exits of the lane selected in the lane list
    //                l = (Lane) laneList.getSelectedValue();
    //                if(l == null){
    //                    JOptionPane.showMessageDialog(MapPanel, "Please slecet a lane and try again"); // Display an error message, and exit
    //                    return;
    //                }
    //                editFrame = new JFrame("Edit exits of lane " + l.getId()[0] + "." + l.getId()[2]);
    //                editFrame.setLayout(new BorderLayout());
    //                editFrame.setSize(300, 400);
    //                editFrame.setVisible(true);
    //                // Point exits at the exit vector of th elane to be processed, so that exit/entrance
    //                // pairs can be added to it
    //                // Need to create a vector of strings to allow for a reasonable list of the entrance
    //                // and exit pairs to be displayed
    //                exitName = new Vector();
    //                for(int[][] i:l.getExits()){
    //                    String name = new String();
    //                    for(int j = 0; j <=1; j++){
    //                        // Construct the name of the waypoints
    //                        name = name + i[j][0] + "." + i[j][1] + "." + i[j][2] + " ";
    //                    }
    ////                    System.out.println("Adding name al;skfdja;sldkfja;lskdfja;lskdjf;alskdjf;alskdfj " + name);
    //                    exitName.add(name);
    //                }
    ////                System.out.println(exitName.size());
    //                Vector<String> rand = new Vector();
    //                rand.add("hello");
    //                selectionList = new JList(exitName);
    ////                selectionList.setListData(exitName);
    //                selectionList.setSize(200, 300);
    //                JScrollPane scroll = new JScrollPane(selectionList);
    //                editFrame.add(scroll, BorderLayout.CENTER);
    //                // Create buttons to edit the exit pair
    //                JButton addExitButton = new JButton("Add");
    //                JButton removeExitButton = new JButton("Remove");
    //                //Create a panel to hold the buttons
    //                JPanel buttonPane = new JPanel();
    //                addExitButton.addActionListener(new AddExitListener());
    //                buttonPane.add(addExitButton);
    //                removeExitButton.addActionListener(new RemoveListener());
    //                buttonPane.add(removeExitButton);
    //                editFrame.add(buttonPane, BorderLayout.SOUTH);
    //                
    ////                editFrame.pack();
    ////                editFrame.setVisible(true);
    //                editFrame.validate();
    //            }
    //            /** Removes an exit pair */
    //            class RemoveListener implements ActionListener{
    //                public void actionPerformed(ActionEvent e){
    //                    int index = selectionList.getSelectedIndex(); // Get he index of the pair to remove
    //                    if(index == -1){
    //                        JOptionPane.showMessageDialog(editFrame, "Please select an exit pair to remove");
    //                        return;
    //                    }
    //                    // Remove the pair from the lane list
    //                    l.getExits().remove(index);
    //                    // Remove the name from the selection list
    //                    exitName.remove(index);
    //                    // Update the UI
    //                    selectionList.setListData(exitName);
    //                    editFrame.validate();
    //                }
    //            }
    //            
    //            /** Adds an exit pair */
    //            class AddExitListener implements ActionListener {
    //                JFrame addFrame;
    //                JList addExitList;
    //                JList addEntranceList;
    //                
    //                public void actionPerformed(ActionEvent e){
    //                    // Create a panel for adding an entrance-exit pair
    //                    addFrame = new JFrame();
    //                    addFrame.setLayout(new BorderLayout());
    //                    JPanel exitPanel = new JPanel();
    //                    exitPanel.setLayout(new BoxLayout(exitPanel, BoxLayout.Y_AXIS));
    //                    JPanel entrancePanel = new JPanel();
    //                    entrancePanel.setLayout(new BoxLayout(entrancePanel, BoxLayout.Y_AXIS));
    //                    JPanel buttonPanel = new JPanel();
    //                    JButton OKButton = new JButton("OK");
    //                    OKButton.addActionListener(new OKListener());
    //                    JButton CancelButton = new JButton("CancelButton");
    //                    CancelButton.addActionListener(new CancelListener());
    //                    buttonPanel.add(OKButton);
    //                    buttonPanel.add(CancelButton);
    //                    // Add a list of all waypoints to the two lists, and assemble the frame
    //                    addExitList = new JList(wayVector);
    //                    JScrollPane exitScroll = new JScrollPane(addExitList);
    //                    exitScroll.setSize(200, 300);
    //                    addEntranceList = new JList(wayVector);
    //                    JScrollPane entranceScroll = new JScrollPane(addEntranceList);
    //                    exitScroll.setSize(200, 300);
    //                    
    //                    
    //                    exitPanel.add(new JLabel("Select the exit waypoint"));
    //                    exitPanel.add(exitScroll);
    //                    addFrame.add(exitPanel, BorderLayout.EAST);
    //                    entrancePanel.add(new JLabel("Select the entrance waypoint"));
    //                    entrancePanel.add(entranceScroll);
    //                    addFrame.add(entrancePanel, BorderLayout.EAST);
    //                    addFrame.add(exitPanel, BorderLayout.WEST);
    //                    
    //                    addFrame.add(buttonPanel, BorderLayout.SOUTH);
    //                    addFrame.pack();
    //                    addFrame.setVisible(true);
    //                }
    //                /** Exit without doing anything */
    //                class CancelListener implements ActionListener{
    //                    public void actionPerformed(ActionEvent e){
    //                        addFrame.setVisible(false);
    //                        return;
    //                    }
    //                }
    //                
    //                /** Add the exit pair to the lane */
    //                class OKListener implements ActionListener{
    //                    public void actionPerformed(ActionEvent e){
    //                        WayPoint exit = (WayPoint) addExitList.getSelectedValue();
    //                        WayPoint entrance = (WayPoint) addEntranceList.getSelectedValue();
    //                        if(exit == null || entrance == null){
    //                            JOptionPane.showMessageDialog(addFrame, "Need to select an entrance and exit");
    //                            return;
    //                        }
    //                        int[][] newExitPair = new int[2][3];
    //                        newExitPair[0][0] = exit.getId()[0];
    //                        newExitPair[0][1] = exit.getId()[1];
    //                        newExitPair[0][2] = exit.getId()[2];
    //                        newExitPair[1][0] = entrance.getId()[0];
    //                        newExitPair[1][1] = entrance.getId()[1];
    //                        newExitPair[1][2] = entrance.getId()[2];
    //                        l.getExits().add(newExitPair); 
    //                        exitName.add(exit.getId()[0] + "." + exit.getId()[1] + "." + exit.getId()[2] + 
    //                                " " + entrance.getId()[0] + "." + entrance.getId()[1] + "." + entrance.getId()[2]);
    //                        addFrame.setVisible(false); // Close the add frame
    //                        selectionList.setListData(exitName);
    //                        selectionList.validate(); // update the selection list
    //                        
    //                        editFrame.validate();
    //                    }
    //                }
    //            }
    //        }

    /**
     * Restest the scale of the map when an action is thrown
     */
    class scaleActionListener implements ActionListener {
      public void actionPerformed(ActionEvent e){
        MapPanel.setScale(Double.parseDouble(perScale.getText()));
        MapPanel.repaint();
      }
    };

    /**
     * Removes the selected waypoint.  Does not perform removal if the waypoint is part of an
     * entrance exit pair.
     */
    class RemoveWaypointListener implements ActionListener {
      public void actionPerformed(ActionEvent e){
        // Locate the selected Waypoint
        WayPoint w = null;;
        for(WayPoint way: wayVector){
          if(way.isSelected()){
            w = way;
          }
        }
        if(w == null){
          JOptionPane.showMessageDialog(MapPanel, "No Selected Waypoint");
          return;
        }
        boolean exit = false; // Holds whether this is a
        // Check to make sure that its not an entry or exit point
        for(Segment s: m.getSegs()){
          for(Lane l: s.getLanes()){
            if(l.isBorderElement(w)){
              JOptionPane.showMessageDialog(MapPanel, "The selected WayPoint is an entrance or exit of lane " + 
                  l.getId()[0] + " " + l.getId()[1]);
              exit = true;
            }
          }
        }
        if(exit){
          return; 
        }
        // Remove the selected waypoint from the lanes
        System.out.println(w.getId()[0]);
        System.out.println(w.getId()[1]);
        System.out.println(m.getSegs().length);
        System.out.println(m.getSegs()[w.getId()[0]].getLanes().length);
        m.getSegs()[w.getId()[0]-1].getLanes()[w.getId()[1]-1].removeWaypoint(w, m);
        wayVector.removeElement(w);
        // update ui
        wayPointList.setListData(m.getSegs()[w.getId()[0]-1].getLanes()[w.getId()[1]-1].getWayPoints());
        wayPointList.validate();
        MapPanel.repaint();
      }

    }

    /**
     * Modifies the selection of waypoints when selected in wayList
     */
    class WayListSelectionListener implements ListSelectionListener{
      JList parent;
      public WayListSelectionListener(JList parentList){
        super();
        parent = parentList;
      }

      public void valueChanged(ListSelectionEvent e){
        for(WayPoint w: wayVector){
          if(w.isSelected())
            w.setSelected(false);
        }
        try{
          for (int i = e.getFirstIndex(); i <= e.getLastIndex(); i++){
            WayPoint w = (WayPoint) ( parent.getModel().getElementAt(i) );
            w.setSelected(parent.isSelectedIndex(i));
          }
        }catch (ArrayIndexOutOfBoundsException f){

        }
        myWindow.repaint();
      }
    }

    /**
     * Modifies the selection of lanes when selected in laneList.  Also changes wayPointList
     * to only display WayPoints in the selected lane
     * 
     */
    class LaneListSelectionListener implements ListSelectionListener{
      Lane lastSelected; //Holds the last selected lane
      public void valueChanged(ListSelectionEvent e){
        // Set the selection field of the lanes to corrospond with the selection
        // in the list
        if(lastSelected != null){
          lastSelected.setSelected(false);
        }
        try{
          for(int i = e.getFirstIndex(); i <= e.getLastIndex(); i++){
            Lane l = (Lane) (laneList.getModel().getElementAt(i));
            l.setSelected(laneList.isSelectedIndex(i));
            if(laneList.isSelectedIndex(i))
              lastSelected = l;
          }
          // Set wayPointList to display the waypoints in the selected list
          try{
            Lane l = (Lane) laneList.getSelectedValue();
            wayPointList.setListData(l.getWayPoints());
          } catch (NullPointerException f){

          }
        } catch (ArrayIndexOutOfBoundsException f){
          // This should only get thrown when a lane gets removed
          System.err.println("Should be the result of removing a lane \n" +
              "ArrayIndexOutOfBoundsException " + f);
        }
        // Repaint the window to show the new selection
        myWindow.repaint();
      }
    }

    /**
     * Modifies selection of segmenst.  Also changes laneList to only show lanes in the
     * selected lane;
     */
    class SegListSelectionListener implements ListSelectionListener {
      public void valueChanged(ListSelectionEvent e){
        for(int i = e.getFirstIndex(); i < e.getLastIndex(); i++){
          Segment s = (Segment) segList.getModel().getElementAt(i);
          s.setSelected(segList.isSelectedIndex(i));
        }
        // Set laneList to display lanes in the selected segment
        try{
          Segment s = (Segment) segList.getSelectedValue();
          laneList.setListData(s.getLanes());
        } catch (NullPointerException f){
          // There isn't a segment selected, likely because the previously selected one was deleted, or selection
          // was cleared
          laneList.setListData(new Vector<Lane>());
        }
        myWindow.repaint();
      }
    }


    /**
     *Resets the xoffset of the map when the action is thrown
     */
    class XOffsetListener implements ActionListener {
      public void actionPerformed(ActionEvent e){
        MapPanel.setXOffset(Double.parseDouble(xOffset.getText()));
        MapPanel.repaint();
      }
    }


    /**
     *Resets the yoffset of the map when the action is thrown
     */
    class YOffsetListener implements ActionListener {
      public void actionPerformed(ActionEvent e){
        MapPanel.setYOffset(Double.parseDouble(yOffset.getText()));
        MapPanel.repaint();
      }
    }

    /**
     * Shifts waypoints up or down in the internal list of waypoints of a list.  Assumes
     * that the list is already sorted.  Must be registered to both wayPointUpButton and
     * wayPointDownButton.
     */
    class WayPointShiftButtonListener implements ActionListener{
      public void actionPerformed(ActionEvent e){
        // If there is no lane selected, print an error message and exit
        if(laneList.getSelectedIndex() == -1){
          System.err.println("No lane to shift waypoints within");
          return;
        }
        // shift the waypoint up in the list
        WayPoint w = (WayPoint) wayPointList.getSelectedValue();
        Lane l = (Lane) laneList.getSelectedValue();
        if(e.getSource() == wayPointUpButton){

          // Check that this is not the first waypoint in the list.
          if(w.getId()[2] > 1){
            l.getWayPoints().elementAt(w.getId()[2] -2).getId()[2] 
              = w.getId()[2]; // -1 gives the w index
            w.getId()[2] = w.getId()[2]-1;
            // Sort the list to get the waypoints into the proper order
            Collections.sort((Vector) l.getWayPoints());

            // Need custom code to update entrance exit pairs because using laneEntranceUpdate
            // won't work when swapping values, as at one point, both waypoints would have the same
            // labeled value in exit
            // Probably should be shifted into the lane class, as it involves fairly involved tinkering with 
            // the lane's guts
            for(Segment s: m.getSegs()){
              for(Lane lane: s.getLanes()){
                for(int[][] pair: lane.getExits()){
                  // check the exits
                  if(pair[0][0] == w.getId()[0] && pair[0][1] == w.getId()[1]){
                    if(pair[0][2] == w.getId()[2]){
                      // Note that the id of w has been updated since the exit list was written,
                      //so need to compensate
                      pair[0][2] = w.getId()[2]+1;
                    }else if(pair[0][2] == w.getId()[2]+1){
                      pair[0][2] = w.getId()[2];
                    }
                  }
                  // check entrances, could put this in a loop
                  if(pair[1][0] == w.getId()[0] && pair[1][1] == w.getId()[1]){
                    if(pair[1][2] == w.getId()[2]){
                      // Note that the id of w has been updated since the exit list was written,
                      //so need to compensate
                      pair[1][2] = w.getId()[2]+1;
                    }else if(pair[1][2] == w.getId()[2]+1){
                      pair[1][2] = w.getId()[2];
                    }
                  }
                }
              }
            }

            // Shift the selection of wayPointList to follow the waypoint
            wayPointList.setSelectedIndex(wayPointList.getSelectedIndex()-1);
            MapPanel.repaint();
          }else{
            System.err.println("Cannot shift waypoint up, as it is at the top"
                + " of the list");
          }
        }else{
          // shift the waypoint down in the waypoint list
          if(w.getId()[2] < l.getWayPoints().size()){
            l.getWayPoints().elementAt(w.getId()[2]).getId()[2] 
              = w.getId()[2]; // w.getID()[2] returns the index of the next waypoint
            w.getId()[2] = w.getId()[2]+1;

            for(Segment s: m.getSegs()){
              for(Lane lane: s.getLanes()){
                for(int[][] pair: lane.getExits()){
                  // check the exits
                  if(pair[0][0] == w.getId()[0] && pair[0][1] == w.getId()[1]){
                    if(pair[0][2] == w.getId()[2]){
                      // Note that the id of w has been updated since the exit list was written,
                      //so need to compensate
                      pair[0][2] = w.getId()[2]-1;
                    }else if(pair[0][2] == w.getId()[2]-1){
                      pair[0][2] = w.getId()[2];
                    }
                  }
                  // check entrances, could put this in a loop
                  if(pair[1][0] == w.getId()[0] && pair[1][1] == w.getId()[1]){
                    if(pair[1][2] == w.getId()[2]){
                      // Note that the id of w has been updated since the exit list was written,
                      //so need to compensate
                      pair[1][2] = w.getId()[2]-1;
                    }else if(pair[1][2] == w.getId()[2]-1){
                      pair[1][2] = w.getId()[2];
                    }
                  }
                }
              }
            }

            // Sort the list to get the waypoints into the proper order
            Collections.sort((Vector) l.getWayPoints());
            // shift the selection of wayPointList to follow the waypoint
            wayPointList.setSelectedIndex(wayPointList.getSelectedIndex()+1);
            MapPanel.repaint();
          }else{
            System.err.println("Cannot shift waypoint up, as it is at the bottom"
                + " of the list");
          }
        }
      }
    }

    /** sets up the menus */
    private void createMenus(JFrame myWindow) {

      JMenuBar mb = new JMenuBar();
      JMenu fileMenu = new JMenu("File");
      fileMenu.add(fileOpenAction);
      fileMenu.add(fileExitAction);
      fileMenu.add(exportRNDFAction);
      mb.add(fileMenu);

      JMenu modeMenu = new JMenu("Mode");
      modeMenu.add(modeUTMAction);
      modeMenu.add(modeLatLonAction);
      mb.add(modeMenu);

      JMenu viewMenu = new JMenu("View");
      viewMenu.add(stopSignAction);
      viewMenu.add(toggleBackground);
      mb.add(viewMenu);

      // Add a menu to do full map transforms
      JMenu transformMenu = new JMenu("Transform");
      JMenuItem latLonTrans = new JMenuItem("LatLon map transform");
      latLonTrans.addActionListener(new MapLatLonTransformListener());
      transformMenu.add(latLonTrans);
      // add the item for a UTM transformation
      JMenuItem UTMTrans = new JMenuItem("UTM map transform");
      UTMTrans.addActionListener(new MapUTMTransformListener());
      transformMenu.add(UTMTrans);
      mb.add(transformMenu);

      // Create a menu to edit segments
      JMenu segmentMenu = new JMenu("Segments");
      segmentMenu.add(addSegmentAction);
      segmentMenu.add(removeSegmentAction);
      mb.add(segmentMenu);

      // Create a menu to edit lanes
      JMenu laneMenu = new JMenu("Lanes");
      laneMenu.add(addLaneAction);
      laneMenu.add(removeLaneAction);
      laneMenu.add(editExitAction);
      mb.add(laneMenu);

      myWindow.setJMenuBar(mb);		
    }


    /** Construct an instance of the Map application by creating and
     *  displaying its main window.
     */
    public MapViewer() {
      m = new Map();
      myWindow = new JFrame("Map Viewer");
      buttonPanel = new JPanel();
      // Holds all components of the display
      JPanel mainPanel = new JPanel();
      // Uses a box layout to display the contained panels side by side
      mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
      // Holds the map display and the display controls
      JPanel displayPanel = new JPanel();
      MapPanel = new MapView(this);

      //Add listeners to the MapPanel to allow for the manipulation of the map

      // Add a listener to allow the addition of new waypoints
      //    MapPanel.addMouseListener(new MapWayPointListener());
      // Add a listener to allow selection/deselection of waypoints
      MapPanel.addMouseListener(new ClickWayPointListener());
      // Add a listener to track if the z key is depressed
      MapKeyListener keyListener = new MapKeyListener();
      MapPanel.addKeyListener(keyListener);
      // Add a listener to allow for the panning of the map via mouse dragging
      MapPanel.addMouseMotionListener(new MapMouseMotionListener(MapPanel, keyListener));
      // Add a listener to allow for zooming the map with the mouse wheel
      MapPanel.addMouseWheelListener(new MapZoomWheelListener(MapPanel));



      displayPanel.setLayout(new BoxLayout(displayPanel, BoxLayout.PAGE_AXIS));
      displayPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
      displayPanel.add(buttonPanel);
      displayPanel.add(MapPanel);
      // Build the button panel, with text fields for the lattitude and longitude
      // position of the mouse, the scale factor to use, and the lattitude and
      // longitude offset (from a centered display) to use
      buttonPanel.add(new JLabel("LatLon coordinate"));
      buttonPanel.add(xcoord);
      buttonPanel.add(ycoord);
      buttonPanel.add(new JLabel("UTM zone"));
      buttonPanel.add(zoneField);
      buttonPanel.add(new JLabel("Easting"));
      buttonPanel.add(eastingField);
      buttonPanel.add(new JLabel("Northing"));
      buttonPanel.add(northingField);
      //Add the map scale factor text field and listener
      buttonPanel.add(new JLabel("Map scale factor"));
      buttonPanel.add(perScale);
      perScale.addActionListener(new scaleActionListener());
      //Add the offset text fields and listeners
      buttonPanel.add(new JLabel("LatLon offsets"));
      buttonPanel.add(xOffset);
      xOffset.addActionListener(new XOffsetListener());
      buttonPanel.add(yOffset);
      yOffset.addActionListener(new YOffsetListener());
      MapPanel.setBorder(BorderFactory.createCompoundBorder(
            BorderFactory.createTitledBorder(""), 
            BorderFactory.createEmptyBorder(300,400,300,400)));		
      displayPanel.add(MapPanel, BorderLayout.SOUTH);

      createControlPanel();

      // Add the component panels to the main panel
      mainPanel.add(displayPanel);
      mainPanel.add(controlPanel);

      createMenus(myWindow);
      // Adds the main panel to the view
      myWindow.setContentPane(mainPanel); 
      myWindow.setSize(1280, 960);
      //myWindow.pack();
      myWindow.setVisible(true);
      myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Creates a panel to hold the various user controls
     */
    private void createControlPanel(){
      // Creates the panel to hold functional controls
      controlPanel = new JPanel();
      controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
    }


    /** 
     *  Also constructs most of the interface, that depends on the data content loaded
     * Actions to perform when user clicks File->Open */
    void doOpen() {
      JFileChooser file = new JFileChooser("testRNDF");
      int result = file.showOpenDialog(null);
      if(result == JFileChooser.APPROVE_OPTION) {
        File f = file.getSelectedFile();
        doOpen( f );
      } else {
        JOptionPane.showMessageDialog(null, "No file selected");
      }
    }

    void doOpen( File f )
    {
      try {
        Reader r = new FileReader(f);
        BufferedReader br = new BufferedReader(r);
        try { 
          m.readMap(br); 
          MapPanel.setMap(m);
          myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          myWindow.setTitle(m.info.get("RNDF_name"));
          // Update the text in the perScale field to display the actual
          // scale used in the perspective
          perScale.setText("" + MapPanel.getScale());
          // Use the actual offset data for the text in the offset fields
          xOffset.setText("" + MapPanel.getXOffset());
          yOffset.setText("" + MapPanel.getYOffset());
          // Get the UTM cooridnates to update the UTM fields
          UTM u = LatLongUTMConversion.LLtoUTM(ellipseIndex, MapPanel.getXOffset(), MapPanel.getYOffset());
          // update the zone, easting, and northing information
          zoneField.setText(u.getZone());
          eastingField.setText("" + u.getEasting());
          northingField.setText("" + u.getNorthing());


          assembleSelectionInterface();

        }
        catch(IOException error) {
          JOptionPane.showMessageDialog(null, error.toString());
        }
        catch(MapFormatException error) {
          JOptionPane.showMessageDialog(null, f.getName() +
              " is malformed!");
        }
      } catch (FileNotFoundException error2) {
        JOptionPane.showMessageDialog(null, "File not found: " + f.getName());
      }
    }    

    /**
     * Assembles the selection and manipulation interface.  Should be called after or within 
     * doOpen, so that all the necessary data strucutres have been initialized
     */
    public void assembleSelectionInterface(){
      // clear wayVector and controlPanel from any previous state
      wayVector.removeAllElements();
      controlPanel.removeAll();

      copyWaypoints(m);
      //            // Adds a button to transform the entire map
      //              JButton transButton = new JButton("Full map transformation");
      //              transButton.addActionListener(new MapLatLonTransformListener());
      //              controlPanel.add(transButton);
      //              // Add a button to transform the entire map using UTM coordinates
      //              JButton utmTransButton = new JButton("Full map UTM transformation");
      //              utmTransButton.addActionListener(new MapUTMTransformListener());
      //              controlPanel.add(utmTransButton);

      // Add the list to allow for selection of segments
      segList = new JList();
      segList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      segList.setSize(50, 70);
      JScrollPane segScroll = new JScrollPane(segList);
      segList.setListData(m.getSegs());
      controlPanel.add(segScroll);
      segList.addListSelectionListener(new SegListSelectionListener());

      // Replaced by menu items
      //              // Add buttons for the addition and removal of segments
      //              JPanel segButtonPanel = new JPanel();
      //              segButtonPanel.setLayout(new GridLayout(1,2));
      //              JButton segAddButton = new JButton("Add Segment");
      //              segAddButton.addActionListener(new SegmentAddListener());
      //              JButton segRemoveButton = new JButton("Remove Segment");
      //              segRemoveButton.addActionListener(new SegmentRemoveListener());
      ////                              segAddButton.set
      //              segButtonPanel.add(segAddButton);
      //              segButtonPanel.add(segRemoveButton);
      //              controlPanel.add(segButtonPanel);

      // Add the list to allow for selection of lanes
      laneList = new JList();
      laneList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      laneList.setSize(50, 100);
      JScrollPane laneScroll = new JScrollPane(laneList);
      laneScroll.setSize(50, 75);
      controlPanel.add(new JLabel("Lane selection menu"));
      controlPanel.add(laneScroll);
      laneList.addListSelectionListener(new LaneListSelectionListener());

      //              // Add buttons to allow for the addition and removal of waypoints
      //              JPanel laneAddPanel = new JPanel();
      //              laneAddPanel.setLayout(new GridLayout(1,2));
      //              JButton laneAddButton = new JButton("Add Lane");
      //              laneAddButton.addActionListener(new LaneAddListener());
      //              JButton laneRemoveButton = new JButton("Remove Lane");
      //              laneRemoveButton.addActionListener(new LaneRemoveListener());
      //              laneAddPanel.add(laneAddButton);
      //              laneAddPanel.add(laneRemoveButton);
      //              controlPanel.add(laneAddPanel);

      //              // Add a button to allow for the editing of lane entrance/exit pairs
      //              JButton editExitButton = new JButton("Edit Lane exits and entrances");
      //              editExitButton.addActionListener(new ExitListener());
      //              controlPanel.add(editExitButton);

      // Add the list to allow for the selection of waypoints
      wayPointList = new JList();
      wayPointList.setListData(wayVector);
      wayPointList.setSize(50, 75);
      // allow for the selection of one waypoint at a time
      wayPointList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      // add a listener to handel selecting waypoints
      wayPointList.addListSelectionListener(new WayListSelectionListener(wayPointList));
      JScrollPane scroll = new JScrollPane(wayPointList);
      scroll.setSize(50, 100);
      controlPanel.add(new JLabel("WayPoint selection menu"));
      controlPanel.add(scroll);
      // Create buttons to move the waypoints about in the lane
      JPanel wayButtonPanel = new JPanel();
      wayButtonPanel.setLayout(new BoxLayout(wayButtonPanel, BoxLayout.X_AXIS));
      wayButtonPanel.add(wayPointUpButton);
      wayButtonPanel.add(wayPointDownButton);
      WayPointShiftButtonListener shift = new WayPointShiftButtonListener();
      wayPointUpButton.addActionListener(shift);
      wayPointDownButton.addActionListener(shift);
      controlPanel.add(wayButtonPanel);

      myWindow.validate();

      //              //Test the writing function
      //                  m.writeMap("Test rndf output.txt");
    }

    /**
     * Creates and displays a new MapViewer.  This is a frame inside of which
     * is a MapView panel to display the waypoints, and associated controls
     * to edit the map and control the view.
     *
     * @param args The command-line arguments; if there is at least one and 
     * the first is "-sysLAF", the application will use the system-specific 
     * look and feel.
     */
    public static void main(String[] args) {


      if (args.length > 0 && args[0].equals("-sysLAF")) {
        try {
          // Try to use a system-specific look and feel...
          javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch ( Exception e ) {
          // It didn't work.  What a shame.  We can still run.
          System.err.println(
              "-sysLAF: Unable to set platform look and feel.  Continuing with \n" +
              "default look and feel.");
        }
      }

      String rndf = "";
      String bgfile = "";

      MapViewer m = new MapViewer();

      // Read through all of the arguments
      for( int i=0; i<args.length; i++ ) {
        // --rndf indicates initial file to load
        if( args[i].equals( "--rndf" ) ) {
          rndf = args[++i];
          if( rndf.length() > 0 ) {
            m.doOpen( new File(rndf) );
          }
        }
        if( args[i].equals( "--background" ) ) {
          bgfile = args[++i];
          if( bgfile.length() > 0 ) {
            
          }
        }
        else if( args[i].equals( "--bgcoords" ) ) {
          String slat1 = args[++i];
          String slon1 = args[++i];
          String slat2 = args[++i];
          String slon2 = args[++i];
          double bglat1 = Double.parseDouble(slat1);
          double bglon1 = Double.parseDouble(slon1);
          double bglat2 = Double.parseDouble(slat2);
          double bglon2 = Double.parseDouble(slon2);
          m.MapPanel.setBackgroundUpperRight( bglat2, bglon2 );
          m.MapPanel.setBackgroundLowerLeft( bglat1, bglon1 );
        }
        else if( args[i].equals( "--help" ) ) {
          System.out.println( " " +
"java Mapviewer [options] \n" +
"  options: \n" +
"  --rndf file  \n" +
"     file: path and name of file to load initially \n" +
"  --bgcoords lat1 lon1 lat2 lon2       \n" +
"    set the initial corner-point coordinates of the background image (background.jpg)      \n" +
"    lat1, lon1 define the lower-left corner      \n" +
"    lat2, lon2 define the upper right      \n" +
"         \n" +
"     "   );
        }

      }


    }

    /**
     * Copies the waypoints held in individual zones and segments in the Map into
     * the wayVector vector, so that individual waypoints can be manipulated by the
     * user
     *
     *@param m The map from which to copy the waypoints
     */
    private void copyWaypoints(Map m){
      //Iterate over the segments, then over the lanes, and copy the waypoints in the 
      // lanes into wayVector
      for(Segment s: m.getSegs()){
        for(Lane l: s.getLanes()){
          for(WayPoint w: l.getWayPoints()){
            wayVector.add(w);
          }
        }
      }
      //Iterate over the zones and add all waypoints in them to wayVector
      for(Zone z: m.getZones()){
        for(Spot s: z.getSpots()){
          WayPoint w = s.getCheckPoint();
          wayVector.add(w);
          w = s.getInPoint();
          wayVector.add(w);
        }
        Perimeter p = z.getPerimeter();
        for(WayPoint w: p.getPoints()){
          wayVector.add(w);
        }
      }

    }

    public JTextField getEastingField() {
      return eastingField;
    }

    public JTextField getNorthingField() {
      return northingField;
    }

    public JTextField getZoneField() {
      return zoneField;
    }

    public int getEllipseIndex() {
      return ellipseIndex;
    }
  }
