/**
 * Used to represent UTM coordinates.
 * 
 * @author Defense Mapping Agency
 */
class UTM
{
    private double northing;
    private double easting;
    String zone;

    public UTM( double northing, double easting, String zone )
    {
	this.northing = northing;
	this.easting  = easting;
	this.zone = zone;
    }
    
    double getNorthing()
    {
	return northing;
    }

    public double getEasting()
    {
	return easting;
    }
    
    public String getZone()
    {
	return zone;
    }
}
