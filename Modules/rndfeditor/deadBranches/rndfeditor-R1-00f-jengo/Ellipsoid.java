

/**
 * Represents an ellipsoid.  Used for LatLong - UTM conversion.
 * 
 * @author Defense Mapping Agency
 */
class Ellipsoid
{
  public Ellipsoid(){};
  Ellipsoid(int Id, String name, int radius, double ecc)
  {
    id = Id; ellipsoidName = name; 
    EquatorialRadius = radius; eccentricitySquared = ecc;
  }

  int id;
  String ellipsoidName;
  double EquatorialRadius; 
  double eccentricitySquared;  
};
