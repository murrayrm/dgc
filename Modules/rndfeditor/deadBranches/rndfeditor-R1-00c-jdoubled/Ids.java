

/** 
 * Joshua Doubleday
 * August 21, 2007
 *
 * Helper class to copy/manage ids passed between various components
 */

class Ids {

  public static int[] copy( int[] id ) {
    int[] nid = new int[id.length];
    for( int i=0; i<id.length; i++ )
      nid[i] = id[i];
    return nid;
  }

}


