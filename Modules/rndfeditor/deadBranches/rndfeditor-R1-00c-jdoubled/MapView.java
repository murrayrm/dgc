
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import java.util.Vector;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.imageio.*;
import java.awt.Image.*;
import java.awt.Image;
import java.io.*;

/**
 * MapView.java
 * A class to represent the display component 
 * for the RNDF map
 * @author Xiang Jerry He 
 */

public class MapView extends JPanel {
	
	/** the button panel of MapViewer */
	private boolean stopSignVisible=false;
	private MapViewer viewer;
	/** the map the viewer is displaying */
	private Map cMap;
        /** Holds unassigned waypoints (I.E. waypoints not associated with a lane or zone
         * Mainly intended as a temporary holding place for waypoints added by the user */
        private Vector<WayPoint> way = new Vector<WayPoint>();
	/** holds the perspective of this map
	 * more specifically the center and range of all way points 
	 */
	protected Perspective p; 

	/** current x position of the mouse point */
	private volatile int x;
	/** current y position of the mouse point */
	private volatile int y;
        
        /**Holds the background image to show */
        private BufferedImage background;
        /**Holds lower left position of the background in lat/lon space */
        Point2D backLowerLeft;
        /**Holds the upper right position of the background in lat/lon space */
        Point2D backUpperRight;
        /** Whether to print after printing each element */
        private boolean verbose = false;

	
	/**
	 * a set of defaults for the rendering of the map
	 * e.g. the color and size of various objects
	 * @author Xiang Jerry He 
	 */
	private static class Default {
            // Default color for rendering waypoints if unselected by the user
		public static final Color unselectedWayPointColor= Color.black;
                // Default color for rendering waypoints selected by the user
                public static final Color selectedColor = Color.green;
		public static final Color laneColor = Color.blue; 
		public static final Color zonePerimColor = Color.red;
		public static final double wayPointSize = 3;
                public static final Color checkPointColor = Color.MAGENTA;
		public static final int waypointDisplayPrecision = 7;
		public static final int stopSignSize = 12;
	}
	
	/**
	 * reverses the current visibility of stop signs
	 */
	
	protected void toggleStopSignVisibility() {
		stopSignVisible = !stopSignVisible;
	}
	
	/**
	 * 
	 * @param viewer the MapViewer that is associated with this viewing component
	 */
	public MapView(MapViewer viewer) {
		super();
		this.viewer = viewer;
		cMap = null;
//                try{
//                    File file = new File("st lukes.jpg");
//                    background = ImageIO.read(file);
//                    backLowerLeft = new Point2D.Double(34.1670639, -118.0938222);
//                    backUpperRight = new Point2D.Double(34.169228, -118.0978833);
//                }catch(IOException e){
//                    System.err.println("Problem with map image file");
//                }
	}
	/**
	 * @param map the map that will be displayed 
	 */
	public MapView( Map map, MapViewer viewer ) {
		super();
		this.viewer = viewer;
		setMap(map);
//	        try{
//                    File file = new File("st lukes.jpg");
//                    background = ImageIO.read(file);
//                    backLowerLeft = new Point2D.Double(34.1670639, -118.0938222);
//                    backUpperRight = new Point2D.Double(34.169228, -118.0978833);
//                }catch(IOException e){
//                    System.err.println("Problem with map image file");
//                }
	}
	
	/**
	 * this sets our current map to reference to Map
	 * 
	 * warning: I used direct referencing rather than Map 
	 * copying to improve performance, but is is imperative that
	 * Map is never modified after calling setMap, or else it'll
	 * have unintended side-effects 
	 * 
	 * @param Map the Map that our instance variable cmap will reference to
	 */
	public void setMap(Map Map) {
	    this.cMap = Map; 
	    p = new Perspective(cMap,this.getWidth(), this.getHeight());
	    /** updates the current mouse location */
	    this.addMouseMotionListener(new MouseMotionListener() {
	    	public void mouseMoved(MouseEvent e) {
	    		x = e.getX();
	    		y = e.getY();
	    		double rfactor = Math.pow(10.0, Default.waypointDisplayPrecision);
	    		Double lattitude = new Double(Math.round(p.pixel2Lat(x)*rfactor)/rfactor);
	    		Double longitude = new Double(Math.round(p.pixel2Lon(y)*rfactor)/rfactor);
                        // Get the UTM coordinates of the lattitude and longitude to 
                        // update the northing and easting fields
                        UTM u = LatLongUTMConversion.LLtoUTM(viewer.getEllipseIndex(), lattitude, longitude);
	    		viewer.xcoord.setText(lattitude.toString());
	    		viewer.ycoord.setText(longitude.toString());
                        // display easting and northing to within a centimeter (BAD MAGIC NUMBERS :(  )
                        viewer.getEastingField().setText("" + Math.round(u.getEasting()*100)/100.0);
                        viewer.getNorthingField().setText("" + Math.round(u.getNorthing()*100)/100.0);
                        viewer.getZoneField().setText(u.getZone());
	    	}
	    	public void mouseDragged(MouseEvent e) {
	    		
	    	}
	    	
	    });
	    /* display the new map */                                                                        
		repaint();         
	}                     
        
        ///////////////////////////////////////////////////////////////////
        // Methods to intereact with the encapsulated perspective object
        
        /** Sets the scale factor of the perspective */
        public void setScale(double scale){
            p.setScale(scale);
        }
        
        /**
         *@return The scale factor of the encapsulated perspective
         */
        public double getScale(){
            return p.getScale();
        }
        
        /**
         * Sets the degree offset in latitude
         *
         *@param xOffset The degree offset in latitude to use
         */
        public void setXOffset(double xOffset){
            p.setXOffset(xOffset);
        }
        
        /**
         *@return the degree offset in latitude
         */
        public double getXOffset(){
            return p.getXOffset();
        }
        
        /**
         * Sets the degree offset in longitude
         *
         *@param yOffset The degree offset in longitude to use
         */
        public void setYOffset(double yOffset){
            p.setYOffset(yOffset);
        }
        
        /**
         *@return the degree offset in longitude
         */
        public double getYOffset(){
            return p.getYOffset();
        }
	
        /////////////////////////////////////////////////////////////////////////
        
	
	/**
	 * 
	 * @return the current displayed RNDF Map
	 */
	public Map getMap() { 
	    return cMap;
	}
        
        public void paintBackground(Graphics2D g2, Perspective ps){
            if(background == null){
                return;
            }
            double xDegPixel = ps.pixel2Lat(1)-ps.pixel2Lat(0); //Get the x scalign
            double yDegPixel = ps.pixel2Lon(0)-ps.pixel2Lon(1);
            double backXscale = (backUpperRight.getX() - backLowerLeft.getX())/
                    ((double )background.getWidth());
            double xScaleFactor = backXscale/xDegPixel;
            double backYscale = (backUpperRight.getY() - backLowerLeft.getY())/
                    ((double) background.getHeight());
            double yScaleFactor = backYscale/yDegPixel;
            
            double centerX = (backUpperRight.getX() - backLowerLeft.getX())/2 + backLowerLeft.getX();
            double centerY = (backUpperRight.getY() - backLowerLeft.getY())/2 +backLowerLeft.getY();
            Image drawImage = background.getScaledInstance((int) (background.getWidth()*xScaleFactor),
                    (int) (background.getHeight()*yScaleFactor), background.SCALE_DEFAULT);
            g2.drawImage(drawImage, (int) (ps.cX(centerX) - background.getWidth()*xScaleFactor/2 ),
                    (int) (ps.cY(centerY) - yScaleFactor*background.getHeight()/2), null);
        }
        
        /**
         * Draws the WayPoint on the screen, using different colors if the WayPoint
         * is or is not selected.
         *
         *@param g2  The graphics context to draw the WayPoint in
         *@param w The WayPoint to draw
         *@param ps The perspective to use while doing the drawing
         */
	public void paintWayPoint(Graphics2D g2, WayPoint w, Perspective ps){
            // Radius to draw the waypoint
            double r = Default.waypointDisplayPrecision;
            if(w.isSelected()){
                // Use the selected color, and increase the size that it is drawn as
                g2.setColor(Default.selectedColor);
                r = r*1.0;
            } else if(w.getCheckpointId() > 0){
                // use the checkppoint color
                g2.setColor(Default.checkPointColor);
            }else {
                g2.setColor(Default.unselectedWayPointColor);
            }
            g2.fill(new Ellipse2D.Double(ps.cX(w.getX())-r/2.0, ps.cY(w.getY())-r/2.0, r, r));
        }
        
        public void paintHollowWaypoint(Graphics2D g2, WayPoint w, Perspective ps){
            if(w.isSelected()){
                // Use the selected color, and increase the size that it is drawn as
                g2.setColor(Default.selectedColor);
            } else {
                g2.setColor(Color.red);
            }
            g2.draw(new Ellipse2D.Double(ps.cX(w.getX()), ps.cY(w.getY()), 3, 3));
        }

        /** Paints unassigned waypoints
         *
         *@param g2 The graphics context in which to paint
         *@param ps The perspective to use in painting
         */
        public void paintUnassignedWayPoints(Graphics2D g2, Perspective ps){
            try{
                for(WayPoint w: way){
                    paintWayPoint(g2, w, ps);
                }
            }catch(NullPointerException e){
                
            }
        }
        
	/**
	 * @param g2 the Graphics2D component
	 * @param l the lane to be painted
	 * @param ps the perspective of the map
	 */
	public void paintLane(Graphics2D g2, Lane l, Perspective ps) {
            // Check if the lane is empty
            if(l.getWayPoints().size() == 0){
                // empty lane
                return;
            }
		Vector<WayPoint> w = l.getWayPoints();
                if(verbose)
                    System.out.println("\tpainting " + w.size() + " waypoints");
		int nwaypoint = w.size();
		WayPoint first = w.get(0);
		g2.setColor(Default.laneColor);
		GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
		thislane.moveTo(ps.cX(first.getX()), ps.cY(first.getY()));
		for(WayPoint p:w) {
			paintWayPoint(g2, p, ps);
                        if(l.isSelected()){
                            g2.setColor(Default.selectedColor);
                        } else {
                            g2.setColor(Default.laneColor);
                        }
		    thislane.lineTo(ps.cX(p.getX()), ps.cY(p.getY()));
		}
		g2.draw(thislane);
    /// Draw the entrance/exit pairs 
    //if( exitsVisible ) {
    if( true ) {
      Vector<int[][]> exits = l.getExits();
      g2.setColor(Color.ORANGE);
      GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, exits.size() );
      for( int[][] exit: exits ) {
        WayPoint w1 = cMap.getWayPoint( exit[Lane.EXIT] );
        WayPoint w2 = cMap.getWayPoint( exit[Lane.ENTRANCE] );
        if( w1 != null && w2 != null ) {
          path.moveTo(p.cX(w1.getX()), p.cY(w1.getY()));
          path.lineTo(p.cX(w2.getX()), p.cY(w2.getY()));
        }
      }
      g2.draw( path );
    }
		if(verbose)
                    System.out.println("\t\tpainting "+l.getStopSign().size()+" stop signs");
		if(stopSignVisible) {
			for(WayPoint st:(l.getStopSign())) {
				g2.setColor(Color.RED);
				int r = Default.stopSignSize;
				g2.fill(new Ellipse2D.Double(ps.cX(st.getX())-r/2.0, ps.cY(st.getY())-r/2.0,r,r));
				//repaint the way point
				paintWayPoint(g2, st, ps);
			}
		}
		g2.setColor(Default.laneColor);
	}
        
	/*
	 * 
	 * @param perimeter the perimeter to be painted
	 * @param ps the perspective of the map
	 * @author David Waylonis
	 */
	public void paintPerimeter(Graphics2D g2, Perimeter per, Perspective ps) {
		Vector<WayPoint> w = per.points;
                if(verbose)
                    System.out.println("\tpainting " + w.size() + " waypoints");
		int nwaypoint = w.size();
		
		WayPoint first = w.get(0);
		GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
		thislane.moveTo(ps.cX(first.getX()), ps.cY(first.getY()));
		double r = Default.wayPointSize;
		for(WayPoint p:w) {
			paintWayPoint(g2, p, ps);
		    thislane.lineTo(ps.cX(p.getX()), ps.cY(p.getY()));
		}
		thislane.lineTo(ps.cX(w.get(0).getX()), ps.cY(w.get(0).getY()));
		g2.setColor(Default.zonePerimColor);
		g2.draw(thislane);
	}
        
        /*
	 * Paints the perimeter using the default color if not selected, or the Default.selected
         * color if selected 
         *
	 * @param perimeter the perimeter to be painted
	 * @param ps the perspective of the map
         * @param selected Whether or not the zone the perimeter is associated with is selected
	 * @author David Waylonis
	 */
	public void paintPerimeter(Graphics2D g2, Perimeter per, Perspective ps, boolean selected) {
		Vector<WayPoint> w = per.points;
                if(verbose)
                    System.out.println("\tpainting " + w.size() + " waypoints");
		int nwaypoint = w.size();
		
		WayPoint first = w.get(0);
		GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
		thislane.moveTo(ps.cX(first.getX()), ps.cY(first.getY()));
		double r = Default.wayPointSize;
		for(WayPoint p:w) {
			paintWayPoint(g2, p, ps);
		    thislane.lineTo(ps.cX(p.getX()), ps.cY(p.getY()));
		}
		thislane.lineTo(ps.cX(w.get(0).getX()), ps.cY(w.get(0).getY()));
                if(selected){
                    g2.setColor(Default.selectedColor);
                } else{
                    g2.setColor(Default.zonePerimColor);
                }
		g2.draw(thislane);
	}
	
	
	/**
         * Doesn't use the default paintWayPoint method
         *
	 * @param g2 the graphics component
	 * @param s the Spot to be painted
	 * @param ps the perspective of the map
         * 
	 */
	public void paintSpot(Graphics2D g2, Spot s, Perspective ps) {
		Vector<WayPoint> w = new Vector<WayPoint>();
		w.add(s.getInPoint());
		w.add(s.getCheckPoint());
                if(verbose)
                    System.out.println("\tpainting " + w.size() + " waypoints");
		int nwaypoint = w.size();
		
		WayPoint first = w.get(0);
		GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
		thislane.moveTo(ps.cX(first.getX()), ps.cY(first.getY()));
		for(WayPoint p:w) {
                    paintHollowWaypoint(g2, p, ps);
			//g2.draw(new Ellipse2D.Double(ps.cX(p.x), ps.cY(p.y), 3, 3));
		    thislane.lineTo(ps.cX(p.getX()), ps.cY(p.getY()));
		}
                g2.setColor(Color.RED);
		g2.draw(thislane);
	}
        
        /**
         * Doesn't use the default paintWayPoint method
         *
	 * @param g2 the graphics component
	 * @param s the Spot to be painted
	 * @param ps the perspective of the map
         *@param selected Whether or not the zone the spot is associated with is selected
         * 
	 */
	public void paintSpot(Graphics2D g2, Spot s, Perspective ps, boolean selected) {
		Vector<WayPoint> w = new Vector<WayPoint>();
		w.add(s.getInPoint());
		w.add(s.getCheckPoint());
                if(verbose)
                    System.out.println("\tpainting " + w.size() + " waypoints");
		int nwaypoint = w.size();
		
		WayPoint first = w.get(0);
		GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
		thislane.moveTo(ps.cX(first.getX()), ps.cY(first.getY()));
		for(WayPoint p:w) {
                    paintHollowWaypoint(g2, p, ps);
			//g2.draw(new Ellipse2D.Double(ps.cX(p.x), ps.cY(p.y), 3, 3));
		    thislane.lineTo(ps.cX(p.getX()), ps.cY(p.getY()));
		}
                if(selected){
                    g2.setColor(Default.selectedColor);
                } else {
                    g2.setColor(Color.RED);
                }
		g2.draw(thislane);
	}
	
	/**
	 * paints the visual representation of the RNDF file
	 * @param g the Graphics component
	 */
	protected void paintComponent( Graphics g ) {
		super.paintComponent(g);
		Graphics g2 = g.create();
        Graphics2D g2d = (Graphics2D)g2;
		//g2d.draw(new Rectangle2D.Double(10, 10, 800 ,600));
		//g2d.translate(25, -13);
		//g2d.translate(100, -50);
		//g2d.scale(4, 4);
        
		if(cMap != null) {
		    //p.setPerspective(this.getWidth(), this.getHeight());
                        paintBackground(g2d, p);
			for(Segment s: cMap.getSegs()) {
                            if(verbose)
				System.out.println("painting segment " + s.segment_name);
				for(Lane l: s.getLanes()) {
                                    try{
                                        if(verbose)
                                            System.out.println("\tpainting Lane with width " + l.getLane_width());
					paintLane(g2d, l, p);
                                    }catch (NullPointerException e){
                                        // Throws after a lane is deleted
                                    }
				}
			}
			for(Zone z: cMap.getZones()) {
                            if(verbose)
				System.out.println("painting zone " + z.zone_name);
                                // pass the selection state of z to insure the use of the
                                // correct color
				paintPerimeter(g2d, z.perimeter, p, z.isSelected());
				for(Spot s: z.getSpots()) {
					paintSpot(g2d, s, p, z.isSelected());
				}
			}
		}
        paintUnassignedWayPoints(g2d, p);
	}

    public Vector<WayPoint> getWay() {
        return way;
    }

    public void setWay(Vector<WayPoint> way) {
        this.way = way;
    }

    /**
     * Returns the perspective used to render the map.
     *
     *@return The perspective used to render the map.
     */
    public Perspective getP() {
        return p;
    }

    public void setP(Perspective p) {
        this.p = p;
    }
}
