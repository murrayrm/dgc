/*-*-c++-*-
=========================================================================
=                                                                       =
=                          rndf2kmldetail.cc                            =
=                                                                       =
=========================================================================

This file converts rndf files to kml format for display.

Author: Rich Petras
Date:   November 20, 2006 

Software Use Notice
------------------
The United States Government has a non-exclusive, non-transferable,
irrevocable royalty-free, worldwide license in the software NPO-30132
under the terms of the prime contract NAS7-1407 with the California
Institute of Technology.  Any agency of the U.S.  Government can use
the software under this license. Since the license is
non-transferable, third party use of this software is limited to uses
for or on behalf of the U.S. Government and permission for any other
use must be negotiated with the Office of Technology Transfer at
Caltech. The software has not necessarily been developed for use
outside of JPL and is being transferred "as is" without warranty of
any kind including fitness for a particular use or purpose.

(C)  2006, Jet Propulsion Laboratory
     California Institute of Technology

Modification History
--------------------
000 20nov2006 rdp - Original creation date.
-------------------------------------------------------------------------*/


#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main (int argc, char** argv)
{
  string input_file[50000];
  string input_line;
  int segment  = 0;
  int lane     = 0; 
  int waypoint = 0;
  int old_segment  = 0;
  int old_lane     = 0; 
  int old_waypoint = 0;

  double latitude  = 0.0;
  double longitude = 0.0;

  char dot;

  string element;
  string data;

  int tab_loc;
  int i = 0;

  struct Waypoint
  {
    string id;
    double lat;
    double lon;
  };

  struct Checkpoint
  {
    string id;
    double lat;
    double lon;
    int    num;
  };

  Waypoint wp_list[50000];
  
  struct Exit
  {
    Waypoint exit;
    Waypoint entry;
  };

  Exit exit_list[5000];
  Waypoint stop_list[5000];
  Checkpoint cp_list[5000];
  int i_wp = 0;
  int i_cp = 0;
  int i_stop = 0;
  int i_exit = 0;

  int n_wp = 0;
  int n_cp = 0;
  int n_stop = 0;
  int n_exit = 0;

  // Set up cout parameters  Need 6 decimal place precision to see proper lat/long detail

  cout.setf(ios::fixed, ios::floatfield);
  cout.setf(ios::showpoint);
  cout.precision(6);

  // Read file in

  do 
    {
      // Read a line from stdin:
      getline (cin,input_file[i]);

      // Get rid of comments
      input_line = input_file[i].substr(0, input_file[i].find("/*"));
      tab_loc = input_line.find('\t');
      element = input_line.substr(0,tab_loc);
      data    = input_line.substr(tab_loc + 1);
      if ((element == "lane")|| 
	  (element == "end_lane")|| 
	  (element == "RNDF_name")|| 
	  (element == "num_segments")||
	  (element == "num_zones")|| 
	  (element == "format_version")|| 
	  (element == "segment")||
	  (element == "end_segment")|| 
	  (element == "num_lanes")||
	  (element == "segment_name")||
	  (element == "num_waypoints")||
	  (element == "lane_width")||
	  (element == "left_boundary")||
	  (element == "right_boundary")||
	  (element == "exit")||
	  (element == "checkpoint")||
	  (element == "stop")||
	  (element == "zone")||
	  (element == "end_zone")||
	  (element == "perimeter")||
	  (element == "end_perimeter")||
	  (element == "spot_width")||
	  (element == "spot")||
	  (element == "end_spot")||
	  (element == "num_spots")||
	  (element == "zone_name")||
	  (element == "end_file")||
	  (element == "num_perimeterpoints")||
	  (element == "creation_date")||
	  (element == ""))
	{
	}
      else
	{
	  // So if the line is nothing else, It must be a coordinate
	  // Parse the line if possible
	  stringstream(input_line) >> segment >> dot >> lane >> dot >> waypoint >> latitude >> longitude;
	  wp_list[i_wp].id = element;
	  wp_list[i_wp].lat = latitude;
	  wp_list[i_wp].lon = longitude;
	  i_wp++;
	}
      i++;
      // Yes I know we don't need this test, but if we don't do a clean exit 
      // above, we can catch it here.
     }while (element != "end_file");
     
  n_wp = i_wp;

  // Start of kml file

  cout << "<kml>" << endl;
  cout << "<Folder>" << endl;


  // Create the lane lines folder

  cout << "<Folder>" << endl;
  cout << "<name> Lane Lines </name>" << endl;
  

  i=0;

  do
    {
      
      // Read a line from data store
      input_line = input_file[i++];

      // Get rid of comments
      input_line = input_line.substr(0, input_line.find("/*"));
      tab_loc = input_line.find('\t');
      element = input_line.substr(0,tab_loc);
      data    = input_line.substr(tab_loc + 1);

      // There is a line for every possible RNDF line except comments 

      if (element == "lane")
	{

	  // Start a new lane 

	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << data << "</name>" << endl;
	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ffff1111</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<LineString>" << endl;
	  cout << "		   <coordinates>" << endl;

	}
      else if (element == "end_lane")
	{

	  // End the lane

	  cout << "		  </coordinates>" << endl;
	  cout << "		</LineString> " << endl;

	  cout << "	</Placemark>" << endl;
	}
      else if (element == "RNDF_name")
	{
	  cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "num_segments")
	{
	}
      else if (element == "num_zones")
	{
	}
      else if (element == "format_version")
	{
	}
      else if (element == "segment")
	{
	  cout << "<Folder>" << endl;
	  cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "end_segment")
	{
	  cout << "</Folder>" << endl;
	}
      else if (element == "num_lanes")
	{
	}
      else if (element == "segment_name")
	{
	  cout << "<description>" << data << "</description>" << endl;
	}
      else if (element == "num_waypoints")
	{
	}
      else if (element == "lane_width")
	{
	}
      else if (element == "left_boundary")
	{
	}
      else if (element == "right_boundary")
	{
	}
      else if (element == "exit")
	{
	  string exit_name;
	  string entry_name;

	  tab_loc    = data.find('\t');
	  exit_name  = data.substr(0,tab_loc);
	  entry_name = data.substr(tab_loc + 1);
	  for (i_wp=0; i_wp < n_wp; i_wp++)
	    {
	      if (exit_name == wp_list[i_wp].id)
		{
		  exit_list[i_exit].exit = wp_list[i_wp]; 
		}
	    }
	  
	  for (i_wp=0; i_wp < n_wp; i_wp++)
	    {
	      if (entry_name == wp_list[i_wp].id)
		{
		  exit_list[i_exit].entry = wp_list[i_wp]; 
		}
	    }

	  i_exit++;
	}
      else if (element == "checkpoint")
	{
	  string cp_name;
	  string cp_num;

	  tab_loc = data.find('\t');
	  cp_name = data.substr(0,tab_loc);
	  cp_num  = data.substr(tab_loc + 1);
	  for (i_wp=0; i_wp < n_wp; i_wp++)
	    {
	      if (cp_name == wp_list[i_wp].id)
		{
		  cp_list[i_cp].id = wp_list[i_wp].id; 
		  cp_list[i_cp].lat = wp_list[i_wp].lat; 
		  cp_list[i_cp].lon = wp_list[i_wp].lon; 
		  stringstream(cp_num) >> cp_list[i_cp].num; 
		}
	    }

	  i_cp++;
	}
      else if (element == "stop")
	{
	  string stop_name;

	  tab_loc = data.find('\t');
	  stop_name = data.substr(0,tab_loc);

	  for (i_wp=0; i_wp < n_wp; i_wp++)
	    {
	      if (stop_name == wp_list[i_wp].id)
		{
 		  stop_list[i_stop] = wp_list[i_wp]; 
		}
	    }

	  i_stop++;
	}
      else if (element == "zone")
	{
	  // Start a zone

	  cout << "<Folder>" << endl;
	  cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "end_zone")
	{
	  cout << "</Folder>" << endl;
	}
      else if (element == "perimeter")
	{
	  // Start a perimeter.  It would be nice to save the initial perimeter point
	  // to close the zone by putting it as the last coordinate.

	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << data << "</name>" << endl;
	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ff00ffff</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<LineString>" << endl;
	  cout << "		   <coordinates>" << endl;
	}
      else if (element == "end_perimeter")
	{
	  cout << "		  </coordinates>" << endl;
	  cout << "		</LineString> " << endl;

	  cout << "	</Placemark>" << endl;
	}
      else if (element == "spot_width")
	{
	}
      else if (element == "spot")
	{
	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << data << "</name>" << endl;
	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ffffffff</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<LineString>" << endl;
	  cout << "		   <coordinates>" << endl;
	}
      else if (element == "end_spot")
	{
	  cout << "		  </coordinates>" << endl;
	  cout << "		</LineString> " << endl;

	  cout << "	</Placemark>" << endl;
	}
      else if (element == "end_file")
	{
	  // Close out the lane line folder

	  cout << "</Folder>" << endl;

	}
      else if (element == "num_perimeterpoints")
	{
	}
      else if (element == "creation_date")
	{
	}
      else if (element == "")
	{
	}
      else if (element == "")
	{
	}
      else
	{
	  // So if the line is nothing else, It must be a coordinate
	  // Parse the line if possible
	  stringstream(input_line) >> segment >> dot >> lane >> dot >> waypoint >> latitude >> longitude;
	  cout  << longitude <<"," << latitude << ",0.0" << endl;
	}

      // Yes I know we don't need this test, but if we don't do a clean exit 
      // above, we can catch it here.

    }while (element != "end_file");

  n_exit = i_exit;
  n_cp   = i_cp;
  n_stop = i_stop;


  // Now create the waypoint folder

  i=0;


  cout << "	<Style id=\"style\">" << endl;
  cout << "		<IconStyle>" << endl;
  // Green donuts
  cout << "			<color>ff00aa00</color>" << endl;
  cout << "			<scale>0.5</scale>" << endl;
  cout << "			<Icon>" << endl;
  cout << "				<href>http://maps.google.com/mapfiles/kml/shapes/donut.png</href>" << endl;
  cout << "			</Icon>" << endl;
  cout << "			<hotSpot x=\"16\" y=\"16\" xunits=\"pixels\" yunits=\"pixels\"/>" << endl;
  cout << "		</IconStyle>" << endl;
  cout << "	</Style>" << endl;

  cout << "<Folder>" << endl;
  cout << "<name> Waypoints </name>" << endl;

  for (i_wp=0; i_wp < n_wp; i_wp++)
	{
	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << wp_list[i_wp].id << "</name>" << endl;
	  cout << "<description>" 
	       << wp_list[i_wp].lat <<", " << wp_list[i_wp].lon 
	       << "</description>" << endl;
	  cout << "				<styleUrl>#style</styleUrl>" << endl;

	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ffffffff</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<Point>" << endl;
	  cout << "		   <coordinates>" << endl;
	  cout << wp_list[i_wp].lon <<"," << wp_list[i_wp].lat << ",0.0" << endl;
	  cout << "		  </coordinates>" << endl;
	  cout << "		</Point> " << endl;

	  cout << "	</Placemark>" << endl;
	}


  // Close waypoint folders

  cout << "</Folder>" << endl;


  // Now create the checkpoint folder

  i=0;


  cout << "	<Style id=\"style\">" << endl;
  cout << "		<IconStyle>" << endl;
  // Blue donuts
  cout << "			<color>ffaa0000</color>" << endl;
  cout << "			<scale>0.5</scale>" << endl;
  cout << "			<Icon>" << endl;
  cout << "				<href>http://maps.google.com/mapfiles/kml/shapes/donut.png</href>" << endl;
  cout << "			</Icon>" << endl;
  cout << "			<hotSpot x=\"16\" y=\"16\" xunits=\"pixels\" yunits=\"pixels\"/>" << endl;
  cout << "		</IconStyle>" << endl;
  cout << "	</Style>" << endl;

  cout << "<Folder>" << endl;
  cout << "<name> Checkpoints </name>" << endl;

  for (i_cp=0; i_cp < n_cp; i_cp++)
	{
	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << cp_list[i_cp].num << "</name>" << endl;
	  cout << "<description>" 
	       << cp_list[i_cp].id << "   "
	       << cp_list[i_cp].lat <<", " << cp_list[i_cp].lon 
	       << "</description>" << endl;
	  cout << "				<styleUrl>#style</styleUrl>" << endl;

	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ff000000</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<Point>" << endl;
	  cout << "		   <coordinates>" << endl;
	  cout  << cp_list[i_cp].lon <<"," << cp_list[i_cp].lat << ",0.0" << endl;
	  cout << "		  </coordinates>" << endl;
	  cout << "		</Point> " << endl;

	  cout << "	</Placemark>" << endl;
	}

   // Close checkpoint folders

  cout << "</Folder>" << endl;


  // Now create the stop folder

  i=0;

  cout << "	<Style id=\"style\">" << endl;
  cout << "		<IconStyle>" << endl;
  // Red donuts
  cout << "			<color>ff0000aa</color>" << endl;
  cout << "			<scale>0.5</scale>" << endl;
  cout << "			<Icon>" << endl;
  cout << "				<href>http://maps.google.com/mapfiles/kml/shapes/donut.png</href>" << endl;
  cout << "			</Icon>" << endl;
  cout << "			<hotSpot x=\"16\" y=\"16\" xunits=\"pixels\" yunits=\"pixels\"/>" << endl;
  cout << "		</IconStyle>" << endl;
  cout << "	</Style>" << endl;

  cout << "<Folder>" << endl;
  cout << "<name> Stop </name>" << endl;

  for (i_stop=0; i_stop < n_stop; i_stop++)
	{
	  cout << "	<Placemark>" << endl;
	  cout << "<description>" 
	       << stop_list[i_stop].id << "   "
	       << stop_list[i_stop].lat <<", " << stop_list[i_stop].lon 
	       << "</description>" << endl;
	  cout << "				<styleUrl>#style</styleUrl>" << endl;

	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ff000000</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<Point>" << endl;
	  cout << "		   <coordinates>" << endl;
	  cout  << stop_list[i_stop].lon <<"," << stop_list[i_stop].lat << ",0.0" << endl;
	  cout << "		  </coordinates>" << endl;
	  cout << "		</Point> " << endl;

	  cout << "	</Placemark>" << endl;
	}

   // Close stop folder

  cout << "</Folder>" << endl;


  // Now create the exit folder

  i=0;

  cout << "	<Style id=\"style\">" << endl;
  cout << "		<IconStyle>" << endl;
  // Orange donuts
  cout << "			<color>ff00aaaa</color>" << endl;
  cout << "			<scale>0.5</scale>" << endl;
  cout << "			<Icon>" << endl;
  cout << "				<href>http://maps.google.com/mapfiles/kml/shapes/donut.png</href>" << endl;
  cout << "			</Icon>" << endl;
  cout << "			<hotSpot x=\"16\" y=\"16\" xunits=\"pixels\" yunits=\"pixels\"/>" << endl;
  cout << "		</IconStyle>" << endl;
  cout << "	</Style>" << endl;

  cout << "<Folder>" << endl;
  cout << "<name> Exit </name>" << endl;

  for (i_exit=0; i_exit < n_exit; i_exit++)
	{
	  cout << "	<Placemark>" << endl;
	  cout << "<name>" 
	       << exit_list[i_exit].exit.id << " to " 
	       << exit_list[i_exit].entry.id<< "   "
	       << "</name>" << endl;
	  cout << "<description>" 
	       << exit_list[i_exit].exit.lat <<", " << exit_list[i_exit].exit.lon 
	       << "</description>" << endl;
	  cout << "				<styleUrl>#style</styleUrl>" << endl;

	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ff00aaff</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
// 	  cout << "		<Point>" << endl;
// 	  cout << "		   <coordinates>" << endl;
// 	  cout << exit_list[i_exit].entry.lon <<"," << exit_list[i_exit].entry.lat << ",0.0" << endl;
// 	  cout << "		  </coordinates>" << endl;
// 	  cout << "		</Point> " << endl;
	  cout << "		<LineString>" << endl;
	  cout << "		   <coordinates>" << endl;
	  cout << exit_list[i_exit].exit.lon << "," << exit_list[i_exit].exit.lat << ",0.0" << endl;
	  cout << exit_list[i_exit].entry.lon <<"," << exit_list[i_exit].entry.lat << ",0.0" << endl;
	  cout << "		  </coordinates>" << endl;
	  cout << "		</LineString> " << endl;

	  cout << "	</Placemark>" << endl;
	}

   // Close exit folders

  cout << "</Folder>" << endl;

  // Now create the exit label folder

  i=0;

  cout << "	<Style id=\"style\">" << endl;
  cout << "		<IconStyle>" << endl;
  // Orange donuts
  cout << "			<color>ff00aaaa</color>" << endl;
  cout << "			<scale>0.5</scale>" << endl;
  cout << "			<Icon>" << endl;
  cout << "				<href>http://maps.google.com/mapfiles/kml/shapes/donut.png</href>" << endl;
  cout << "			</Icon>" << endl;
  cout << "			<hotSpot x=\"16\" y=\"16\" xunits=\"pixels\" yunits=\"pixels\"/>" << endl;
  cout << "		</IconStyle>" << endl;
  cout << "	</Style>" << endl;

  cout << "<Folder>" << endl;
  cout << "<name> Exit Labels </name>" << endl;

  for (i_exit=0; i_exit < n_exit; i_exit++)
	{
	  cout << "	<Placemark>" << endl;
	  cout << "<name>" 
	       << exit_list[i_exit].exit.id << " to " 
	       << exit_list[i_exit].entry.id<< "   "
	       << "</name>" << endl;
	  cout << "<description>" 
	       << exit_list[i_exit].exit.lat <<", " << exit_list[i_exit].exit.lon 
	       << "</description>" << endl;
	  cout << "				<styleUrl>#style</styleUrl>" << endl;

	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ff00aaff</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<Point>" << endl;
	  cout << "		   <coordinates>" << endl;
	  cout << exit_list[i_exit].exit.lon << "," << exit_list[i_exit].exit.lat << ",0.0" << endl;
	  cout << "		  </coordinates>" << endl;
	  cout << "		</Point> " << endl;

	  cout << "	</Placemark>" << endl;
	}

   // Close exit folders

  cout << "</Folder>" << endl;


  // Close out the file and exit

  cout << "</Folder>" << endl;
  cout << "</kml>" << endl;
  
return 0;
}
