RNDF_name	Sample_RNDF_Rev_1.2	
num_segments	2	
num_zones	0
format_version	1.0	
creation_date	5/18/2006	
segment	1	
num_lanes	1	
segment_name	Michigan_Ave	
lane	1.1
num_waypoints	3
lane_width	15	
left_boundary	double_yellow	
right_boundary	road_edge
exit	1.1.3	2.1.1
stop	1.1.3
1.1.1	34.13903300	-118.12352364
1.1.2	34.13891713	-118.12354039
1.1.3	34.13880118	-118.12355713
end_lane		
end_segment		
segment 	2
num_lanes	1
lane	2.1	
num_waypoints	3
lane_width	15
left_boundary	double_yellow
right_boundary	road_edge	
exit 2.1.3 	2.1.3
checkpoint	2.1.3	1
2.1.1	34.13868518	-118.12357387
2.1.2	34.13856929	-118.12359062
2.1.3	34.13845335	-118.12360736
end_lane		
end_segment		
end_file		

