import java.util.LinkedList;

/**
 * A class to calculate the 
 * center and spread of the
 * waypoints
 * 
 * Used to dynamically resize the Viewer
 * so that our RNDF viewer can handle 
 * RNDF maps of all sizes
 * 
 * @author Xiang Jerry He
 */
class Perspective {
	/** a list of all waypoints in our map */
	protected LinkedList<WayPoint> globalWaypointsRaw;
	/** the geometric center of all waypoints */
	protected WayPoint center;
	/** maximum x-coordinate deviation */ 
	double maxXDev;
	/** maximum y-coordinate deviation */ 
	double maxYDev;
	/** average of all x-coordinates */
	double meanX;
	/** average of all y-coordinates */
	double meanY;
	int width, height;
	/** the ratio between the 
	 * actual distance(in degrees) 
	 * and the displayed distance(in pixels)
	 */
	double resizeRatio;
        /** Shifts the image the specified number of degrees in latitude **/
        double xOffset = 0.0;
        /** Shifts the image the specified number of degrees in latitude **/
        double yOffset = 0;
        /** Scales the image **/
        double scale = 1;
	
 /**
  * 
  * @param m       the map whose perspective we want to calculate
  * @param width   the width of the display component(in pixels)
  * @param height  the height of the display component(in pixels)
  */
	public Perspective(Map m, int width, int height) {
		int count=0;
		double sumX=0;
		double sumY = 0;
		globalWaypointsRaw = new LinkedList<WayPoint>();
		this.height = height;
		this.width = width;
		for(Segment s: m.getSegs()) {
			for(Lane l: s.getLanes()) {
				for(WayPoint p: l.getWayPoints()) {
					globalWaypointsRaw.add(p);
					sumX += p.getX();
					sumY += p.getY();
					count++;
				}
			}
		}
		for(Zone z: m.getZones()) {
			for(Spot s: z.getSpots()) {
				globalWaypointsRaw.add(s.getInPoint());
				globalWaypointsRaw.add(s.getCheckPoint());
				sumX += s.getInPoint().getX() + s.getCheckPoint().getX();
				sumY += s.getInPoint().getY() + s.getCheckPoint().getY();
				count += 2;
			}
			for(WayPoint p: z.perimeter.points) {
				globalWaypointsRaw.add(p);
				sumX += p.getX();
				sumY += p.getY();
				count++;
			}
		}
		meanX = sumX/count;
		meanY = sumY/count;
		calculateDev(globalWaypointsRaw, meanX, meanY);
	}
	
        /**
         * Calculates the maximum x and y distance that any waypoint is from the mean
         * x and y point
         */
	private void calculateDev(LinkedList<WayPoint> g, double meanX, double meanY) {
		maxXDev = 0; 
		for(WayPoint p:g) {
			double devX = Math.abs(p.getX() - meanX);
			if(devX > maxXDev) 
				maxXDev = devX;
			double devY = Math.abs(p.getY() - meanY);
			if(devY > maxYDev)
				maxYDev = devY;
		}
		/** the following is exactly the same
		 * code as setPerspective
		 * inlined here for efficiency
		 */
		double xratio = width/maxXDev/2.0/1.2;
		double yratio = height/maxYDev/2.0/1.2;
		resizeRatio = (xratio > yratio? yratio:xratio); 
	}
	
	
	protected void setPerspective(int height, int width) {
		double xratio = width/maxXDev/2.0/1.2;
		double yratio = height/maxYDev/2.0/1.2;
		resizeRatio = (xratio > yratio? yratio:xratio);  
	
	}
	
        /**
         * Converts the x coordinate of a waypoint or similar object ot a pixel.
         *
         *@param x The latitude to converto to a pixel x coordinate
         */
	protected float cX(double x) {
		return (float)(((x + xOffset - meanX)*scale + maxXDev*1.2)*resizeRatio);
	}
	
	protected float cY(double y) {
		return (float)(((y + yOffset - meanY)*scale + maxYDev*1.2)*resizeRatio);
	}
	
	protected double pixel2Lat(int x) {
		return (double)(((x/resizeRatio)/scale - xOffset + meanX) - maxXDev*1.2/scale);
	}
	
	protected double pixel2Lon(int y) {
		return (double) (((y/resizeRatio)/scale - yOffset + meanY) - maxYDev*1.2/scale);
	}
	
	/* TODO have a part that displays UTM
	 *  of current mouse position
	 public Waypoint getUTM(Waypoint w) {
		
	}*/
	
        /**
         * Changes the scale factor.  Numbers larger than 1 zoom in, numbers smaller than
         * 1 zoom out.
         *
         *@param scale The new scale value to use.
         */
        public void setScale(double scale){
            this.scale = scale;
        }
        
        /**
         * Returns the value of the scale factor.  > 1 => zoom in, < 1 => zoom out
         */
        public double getScale(){
            return scale;
        }
        
        /**
         * Changes the value of the latitude offset.
         *
         *@param xOffset The degree to offset by in latitude
         */
        public void setXOffset(double xOffset){
            this.xOffset = xOffset;
        }
        
        /**
         * REturns the value of the latitude offset in degrees
         *
         *@return The value of the latitude offset
         */
        public double getXOffset(){
            return xOffset;
        }
        
        /**
         *Changes the value of the longitude offset
         *
         *@param yOffset The degree value to offset the latitude by
         */
        public void setYOffset(double yOffset){
            this.yOffset = yOffset;
        }
        
        /**
         *Returns the value of the longitude offset in degrees
         *
         *@return The value of the latitude offset
         */
        public double getYOffset(){
            return yOffset;
        }
        
}