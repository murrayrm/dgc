
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import java.util.Vector;
import java.util.regex.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.imageio.*;
import javax.media.jai.*; // COMPILE ERROR HINT: must have the jai_core.jar in classpath.  use: javac -cp .:jai_core.jar:jai_codec.jar *.java 
import java.awt.Image.*;
import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.*;

/**
 * MapView.java
 * A class to represent the display component 
 * for the RNDF map
 * @author Xiang Jerry He 
 */

public class MapView extends JPanel {

  /** the button panel of MapViewer */
  private boolean stopSignVisible=false;
  private MapViewer viewer;
  /** the map the viewer is displaying */
  private Map cMap;
  /** Holds unassigned waypoints (I.E. waypoints not associated with a lane or zone
   * Mainly intended as a temporary holding place for waypoints added by the user */
  private Vector<WayPoint> way = new Vector<WayPoint>();
  /** holds the perspective of this map
   * more specifically the center and range of all way points 
   */
  protected Perspective p; 

  /** current x position of the mouse point */
  private volatile int x;
  /** current y position of the mouse point */
  private volatile int y;

  /**Holds the background image to show */
  private BufferedImage background;
  /**Holds lower left position of the background in lat/lon space */
  Point2D backLowerLeft;
  /**Holds the upper right position of the background in lat/lon space */
  Point2D backUpperRight;
  Point2D backLowerRight;
  Point2D backUpperLeft;
  boolean bWarp = false;
  /** Whether to print after printing each element */
  private boolean verbose = false;


  /**
   * a set of defaults for the rendering of the map
   * e.g. the color and size of various objects
   * @author Xiang Jerry He 
   */
  private static class Default {
    // Default color for rendering waypoints if unselected by the user
    public static final Color unselectedWayPointColor= Color.black;
    // Default color for rendering waypoints selected by the user
    public static final Color selectedColor = Color.green;
    public static final Color laneColor = Color.blue; 
    public static final Color zonePerimColor = Color.red;
    public static final double wayPointSize = 3;
    public static final Color checkPointColor = Color.MAGENTA;
    public static final int waypointDisplayPrecision = 7;
    public static final int stopSignSize = 12;
  }

  /**
   * reverses the current visibility of stop signs
   */

  protected void toggleStopSignVisibility() {
    stopSignVisible = !stopSignVisible;
  }

  /**
   * 
   * @param viewer the MapViewer that is associated with this viewing component
   */
  public MapView(MapViewer viewer) {
    super();
    this.viewer = viewer;
    cMap = null;
    //setBackground( "background.jpg" );
  }

  public void setBackground( String bgfilename ) {
    if( true ) {
      try
      {
        try {
          PlanarImage test = null;
        } catch ( Exception e ) {
          System.err.println( e.getMessage() );
          e.printStackTrace();
          System.err.println( " If you are having trouble with JAI classes missing, make sure you have the " );
          System.err.println( " jar files and are including them in your classpath.  For example you can " );
          System.err.println( " add this to your java commandline: " );
          System.err.println( "    java -cp .:jai_core.jar:jai_codec.jar .... " );
        }

        /// The JAI package that we are currently using is pure java, and 
        /// slightly buggy in that is doesn't know its pure java, so it 
        /// tries to use native... and fails.  Setting this system 
        /// property prevents that attempt.
        System.setProperty( "com.sun.media.jai.disableMediaLib", "true" );

        File file = new File(bgfilename);
        // background = ImageIO.read(file);
        PlanarImage src = (PlanarImage)JAI.create("fileload", bgfilename);
        
        getBgCoords( bgfilename + ".coords" );


        // compute the offsets of the un-fixed corners
        double x_ul = 0.0;
        double y_ul = 0.0;
        double x_lr = 1.0;
        double y_lr = 1.0;

        if( backUpperLeft != null && backLowerRight != null && bWarp ) {
          System.out.println( "Warping image with all four corner points" );
          x_ul = backUpperLeft.getX() - backLowerLeft.getX();
          x_lr = backLowerRight.getX() - backLowerLeft.getX();
          y_ul = backUpperLeft.getY() - backUpperRight.getY();
          y_lr = backLowerRight.getY() - backUpperRight.getY();

          // normalize
          double x0 = backUpperRight.getX() - backLowerLeft.getX();
          double y0 = backLowerLeft.getY() - backUpperRight.getY();
          x_ul /= x0;
          x_lr /= x0;
          y_ul /= y0;
          y_lr /= y0;

          System.out.println( "Scaling by: " );
          
          System.out.println( x_ul );
          System.out.println( x_lr );
          System.out.println( y_ul );
          System.out.println( y_lr );

          if( backUpperRight.getX() < backLowerRight.getX() ) backUpperRight = new Point2D.Double( backLowerRight.getX(), backUpperRight.getY() );
          if( backLowerLeft.getX() > backUpperLeft.getX() ) backLowerLeft = new Point2D.Double( backUpperLeft.getX() , backLowerLeft.getY());
          if( backUpperRight.getY() > backUpperLeft.getY() ) backUpperRight = new Point2D.Double( backUpperRight.getX(), backUpperLeft.getY() );
          if( backLowerLeft.getY() < backLowerRight.getY() ) backLowerLeft = new Point2D.Double(backLowerLeft.getX(), backLowerRight.getY() );

        } else {
          System.out.println( "Only two corner points found, skipping full warp" );
        }

        PerspectiveTransform trans =  PerspectiveTransform.getQuadToQuad( 
          x_ul * src.getWidth(), 
          y_ul * src.getHeight(),
          1.0 * src.getWidth(), 
          0.0 * src.getHeight(),
          x_lr * src.getWidth(), 
          y_lr * src.getHeight(),
          0.0 * src.getWidth(), 
          1.0 * src.getHeight(),
          // end first quad
          0.0 * src.getWidth(), 
          0.0 * src.getHeight(),
          1.0 * src.getWidth(), 
          0.0 * src.getHeight(),
          1.0 * src.getWidth(), 
          1.0 * src.getHeight(),
          0.0 * src.getWidth(), 
          1.0 * src.getHeight()
          );
        WarpPerspective wp = new WarpPerspective( trans );
        
        PlanarImage rsrc = (PlanarImage)JAI.create("warp", src, wp);
        

        background = rsrc.getAsBufferedImage();
      }
      catch(Exception e){
        System.err.println("Problem with map image file");
        System.err.println(" If you are loading a very large image, you might want to try increasing your java heap space" );
        System.err.println(" by adding the following command line options:" );
        System.err.println("    java -Xmx2000m -Xms2000m .... " );
        System.err.println( e.getMessage() );
        e.printStackTrace();

      }
    }
  }

  public void getBgCoords( String bgcoordsfilename ) {
    try {
    Reader r = new FileReader(bgcoordsfilename);
    BufferedReader br = new BufferedReader( r );
    String coords = "";
    while( true) {
      String line = br.readLine();
      if( line == null )
        break;
      else
        coords += line + "\n" ;
    }
    System.out.println( "read coord file: " + coords );
    //BufferedReader br = new BufferedReader(r);
    // We want to load the north-west corner as our upper-right
    // and the SE corner as our lowerleft
    // ==> upperleft and lowerright in background image (north=up) reference
    Pattern pattern;
    Matcher m;
    pattern = Pattern.compile( "Upper Left.*\\(.*\\).*\\(([-\\.0-9]+),([-\\.0-9]+)" );
    m = pattern.matcher( coords );
    while( m.find() ) {
      backUpperRight = new Point2D.Double( Double.parseDouble(m.group(2)), Double.parseDouble(m.group(1)) );
    }
    pattern = Pattern.compile( "Lower Right.*\\(.*\\).*\\(([-\\.0-9]+),([-\\.0-9]+)" );
    m = pattern.matcher( coords );
    while( m.find() ) {
      backLowerLeft = new Point2D.Double( Double.parseDouble(m.group(2)), Double.parseDouble(m.group(1)) );
    }
    if( true ) {
      pattern = Pattern.compile( "Upper Right.*\\(.*\\).*\\(([-\\.0-9]+),([-\\.0-9]+)" );
      m = pattern.matcher( coords );
      while( m.find() ) {
        backLowerRight = new Point2D.Double( Double.parseDouble(m.group(2)), Double.parseDouble(m.group(1)) );
      }
      pattern = Pattern.compile( "Lower Left.*\\(.*\\).*\\(([-\\.0-9]+),([-\\.0-9]+)" );
      m = pattern.matcher( coords );
      while( m.find() ) {
        backUpperLeft = new Point2D.Double( Double.parseDouble(m.group(2)), Double.parseDouble(m.group(1)) );
      }
    }
    } catch (Exception e) {
    }

  }
  /**
   * @param map the map that will be displayed 
   */
  public MapView( Map map, MapViewer viewer ) {
    super();
    this.viewer = viewer;
    setMap(map);
    //setBackground( "background.jpg" );
  }

  /**
   * this sets our current map to reference to Map
   * 
   * warning: I used direct referencing rather than Map 
   * copying to improve performance, but is is imperative that
   * Map is never modified after calling setMap, or else it'll
   * have unintended side-effects 
   * 
   * @param Map the Map that our instance variable cmap will reference to
   */
  public double lat;
  public double lon;
  public void setMap(Map Map) {
    this.cMap = Map; 
    p = new Perspective(cMap,this.getWidth(), this.getHeight());
    /** updates the current mouse location */
    this.addMouseMotionListener(new MouseMotionListener() {
        public void mouseMoved(MouseEvent e) {
        x = e.getX();
        y = e.getY();
        double rfactor = Math.pow(10.0, Default.waypointDisplayPrecision);
        Double lattitude = new Double(Math.round(p.pixel2Lat(x)*rfactor)/rfactor);
        Double longitude = new Double(Math.round(p.pixel2Lon(y)*rfactor)/rfactor);
        lat = lattitude;
        lon = longitude;
        // Get the UTM coordinates of the lattitude and longitude to 
        // update the northing and easting fields
        UTM u = LatLongUTMConversion.LLtoUTM(viewer.getEllipseIndex(), lattitude, longitude);
        viewer.xcoord.setText(lattitude.toString());
        viewer.ycoord.setText(longitude.toString());
        // display easting and northing to within a centimeter (BAD MAGIC NUMBERS :(  )
        viewer.getEastingField().setText("" + Math.round(u.getEasting()*100)/100.0);
        viewer.getNorthingField().setText("" + Math.round(u.getNorthing()*100)/100.0);
        viewer.getZoneField().setText(u.getZone());
        }
        public void mouseDragged(MouseEvent e) {

        }

    });
    /* display the new map */                                                                        
    repaint();         
  }                     

  ///////////////////////////////////////////////////////////////////
  // Methods to intereact with the encapsulated perspective object

  /** Sets the scale factor of the perspective */
  public void setScale(double scale){
    p.setScale(scale);
  }

  /**
   *@return The scale factor of the encapsulated perspective
   */
  public double getScale(){
    return p.getScale();
  }

  /**
   * Sets the degree offset in latitude
   *
   *@param xOffset The degree offset in latitude to use
   */
  public void setXOffset(double xOffset){
    p.setXOffset(xOffset);
  }

  /**
   *@return the degree offset in latitude
   */
  public double getXOffset(){
    return p.getXOffset();
  }

  /**
   * Sets the degree offset in longitude
   *
   *@param yOffset The degree offset in longitude to use
   */
  public void setYOffset(double yOffset){
    p.setYOffset(yOffset);
  }

  /**
   *@return the degree offset in longitude
   */
  public double getYOffset(){
    return p.getYOffset();
  }

  /////////////////////////////////////////////////////////////////////////


  /**
   * 
   * @return the current displayed RNDF Map
   */
  public Map getMap() { 
    return cMap;
  }

  public double oldXScale = 0;
  public double oldYScale = 0;
  public Image drawImage = null;
  public void paintBackground(Graphics2D g2, Perspective ps){
    if(!bShowBackground || background == null){
      return;
    }
    double xDegPixel = ps.pixel2Lat(1)-ps.pixel2Lat(0); //Get the x scalign
    double yDegPixel = ps.pixel2Lon(0)-ps.pixel2Lon(1);
    double backXscale = (backUpperRight.getX() - backLowerLeft.getX())/
      ((double )background.getWidth());
    double xScaleFactor = backXscale/xDegPixel;
    double backYscale = (backUpperRight.getY() - backLowerLeft.getY())/
      ((double) background.getHeight());
    double yScaleFactor = backYscale/yDegPixel;

    double centerX = (backUpperRight.getX() - backLowerLeft.getX())/2 + backLowerLeft.getX();
    double centerY = (backUpperRight.getY() - backLowerLeft.getY())/2 +backLowerLeft.getY();
    if( false && drawImage == null || oldXScale != xScaleFactor || oldYScale != yScaleFactor ) {
      oldXScale = xScaleFactor;
      oldYScale = yScaleFactor;
      drawImage = background.getScaledInstance((int) (background.getWidth()*xScaleFactor),
          (int) (background.getHeight()*yScaleFactor), background.SCALE_DEFAULT);
    }
    g2.drawImage(background, 
        (int) (ps.cX(centerX) - background.getWidth()*xScaleFactor/2 ),
        (int) (ps.cY(centerY) - yScaleFactor*background.getHeight()/2), 
        (int)( background.getWidth()*xScaleFactor ),
        (int)( yScaleFactor*background.getHeight()), 
        null);
  }

  /**
   * Draws the WayPoint on the screen, using different colors if the WayPoint
   * is or is not selected.
   *
   *@param g2  The graphics context to draw the WayPoint in
   *@param w The WayPoint to draw
   *@param ps The perspective to use while doing the drawing
   */
  public void paintWayPoint(Graphics2D g2, WayPoint w, Perspective ps){
    // Radius to draw the waypoint
    double r = Default.waypointDisplayPrecision;
    if(w.isSelected()){
      // Use the selected color, and increase the size that it is drawn as
      g2.setColor(Default.selectedColor);
      r = r*1.0;
    } else if(w.getCheckpointId() > 0){
      // use the checkppoint color
      g2.setColor(Default.checkPointColor);
    }else {
      g2.setColor(Default.unselectedWayPointColor);
    }
    g2.fill(new Ellipse2D.Double(ps.cX(w.getX())-r/2.0, ps.cY(w.getY())-r/2.0, r, r));
  }

  public void paintHollowWaypoint(Graphics2D g2, WayPoint w, Perspective ps){
    if(w.isSelected()){
      // Use the selected color, and increase the size that it is drawn as
      g2.setColor(Default.selectedColor);
    } else {
      g2.setColor(Color.red);
    }
    g2.draw(new Ellipse2D.Double(ps.cX(w.getX()), ps.cY(w.getY()), 3, 3));
  }

  /** Paints unassigned waypoints
   *
   *@param g2 The graphics context in which to paint
   *@param ps The perspective to use in painting
   */
  public void paintUnassignedWayPoints(Graphics2D g2, Perspective ps){
    try{
      for(WayPoint w: way){
        paintWayPoint(g2, w, ps);
      }
    }catch(NullPointerException e){

    }
  }

  /**
   * @param g2 the Graphics2D component
   * @param l the lane to be painted
   * @param ps the perspective of the map
   */
  public void paintLane(Graphics2D g2, Lane l, Perspective ps) {
    // Check if the lane is empty
    if(l.getWayPoints().size() == 0){
      // empty lane
      return;
    }
    Vector<WayPoint> w = l.getWayPoints();
    if(verbose)
      System.out.println("\tpainting " + w.size() + " waypoints");
    int nwaypoint = w.size();
    WayPoint first = w.get(0);
    g2.setColor(Default.laneColor);
    GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
    thislane.moveTo(ps.cX(first.getX()), ps.cY(first.getY()));
    for(WayPoint p:w) {
      paintWayPoint(g2, p, ps);
      if(l.isSelected()){
        g2.setColor(Default.selectedColor);
      } else {
        g2.setColor(Default.laneColor);
      }
      thislane.lineTo(ps.cX(p.getX()), ps.cY(p.getY()));
    }
    g2.draw(thislane);
    /// Draw the entrance/exit pairs 
    //if( exitsVisible ) 
    if( true ) {
      Vector<int[][]> exits = l.getExits();
      g2.setColor(Color.ORANGE);
      GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, exits.size()*3 );
      for( int[][] exit: exits ) {
        WayPoint w1 = cMap.getWayPoint( exit[Lane.EXIT] );
        WayPoint w2 = cMap.getWayPoint( exit[Lane.ENTRANCE] );
        if( w1 != null && w2 != null ) {
          float x = p.cX(w1.getX());
          float y = p.cY(w1.getY());
          float xx = p.cX(w2.getX());
          float yy = p.cY(w2.getY());
          path.moveTo(x, y);
          path.lineTo(xx, yy );
          // draw a very crude arrow
          path.lineTo( xx+(x-xx)/4.0f+5, yy+(y-yy)/4.0f+5  );
          path.lineTo(xx, yy );
          path.lineTo( xx+(x-xx)/4.0f-5, yy+(y-yy)/4.0f  );
        }
      }
      g2.draw( path );
    }
    if(verbose)
      System.out.println("\t\tpainting "+l.getStopSign().size()+" stop signs");
    if(stopSignVisible) {
      for(WayPoint st:(l.getStopSign())) {
        g2.setColor(Color.RED);
        int r = Default.stopSignSize;
        g2.fill(new Ellipse2D.Double(ps.cX(st.getX())-r/2.0, ps.cY(st.getY())-r/2.0,r,r));
        //repaint the way point
        paintWayPoint(g2, st, ps);
      }
    }
    g2.setColor(Default.laneColor);
  }

  /*
   * 
   * @param perimeter the perimeter to be painted
   * @param ps the perspective of the map
   * @author David Waylonis
   */
  public void paintPerimeter(Graphics2D g2, Perimeter per, Perspective ps) {
    Vector<WayPoint> w = per.points;
    if(verbose)
      System.out.println("\tpainting " + w.size() + " waypoints");
    int nwaypoint = w.size();

    WayPoint first = w.get(0);
    GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
    thislane.moveTo(ps.cX(first.getX()), ps.cY(first.getY()));
    double r = Default.wayPointSize;
    for(WayPoint p:w) {
      paintWayPoint(g2, p, ps);
      thislane.lineTo(ps.cX(p.getX()), ps.cY(p.getY()));
    }
    /// Draw the entrance/exit pairs 
    //if( exitsVisible ) {
    if( true ) {
      System.out.println("*******************DRAWING EXIT/ENTRY FOR ZONES");
      Vector<int[][]> exits = per.getExits();
      g2.setColor(Color.GREEN);
      GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, exits.size()*3 );
      for( int[][] exit: exits ) {
        WayPoint w1 = cMap.getWayPoint( exit[Lane.EXIT] );
        WayPoint w2 = cMap.getWayPoint( exit[Lane.ENTRANCE] );
        if( w1 != null && w2 != null ) {
          float x = p.cX(w1.getX());
          float y = p.cY(w1.getY());
          float xx = p.cX(w2.getX());
          float yy = p.cY(w2.getY());
          path.moveTo(x, y);
          path.lineTo(xx, yy );
          // draw a very crude arrow
          path.lineTo( xx+(x-xx)/4.0f+5, yy+(y-yy)/4.0f+5  );
          path.lineTo(xx, yy );
          path.lineTo( xx+(x-xx)/4.0f-5, yy+(y-yy)/4.0f  );
        }
      }
      g2.draw( path );
    }
    thislane.lineTo(ps.cX(w.get(0).getX()), ps.cY(w.get(0).getY()));
    g2.setColor(Default.zonePerimColor);
    g2.draw(thislane);
  }

  /*
   * Paints the perimeter using the default color if not selected, or the Default.selected
   * color if selected 
   *
   * @param perimeter the perimeter to be painted
   * @param ps the perspective of the map
   * @param selected Whether or not the zone the perimeter is associated with is selected
   * @author David Waylonis
   */
  public void paintPerimeter(Graphics2D g2, Perimeter per, Perspective ps, boolean selected) {

    Vector<WayPoint> w = per.points;
    if(verbose)
      System.out.println("\tpainting " + w.size() + " waypoints");
    int nwaypoint = w.size();

    WayPoint first = w.get(0);
    GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
    thislane.moveTo(ps.cX(first.getX()), ps.cY(first.getY()));
    double r = Default.wayPointSize;
    for(WayPoint p:w) {
      paintWayPoint(g2, p, ps);
      thislane.lineTo(ps.cX(p.getX()), ps.cY(p.getY()));
    }
    thislane.lineTo(ps.cX(w.get(0).getX()), ps.cY(w.get(0).getY()));
    if(selected){
      g2.setColor(Default.selectedColor);
    } else{
      g2.setColor(Default.zonePerimColor);
    }
    g2.draw(thislane);
    if( true ) {
      Vector<int[][]> exits = per.getExits();
      g2.setColor(Color.ORANGE);
      GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, exits.size()*3 );
      for( int[][] exit: exits ) {
        WayPoint w1 = cMap.getWayPoint( exit[Lane.EXIT] );
        WayPoint w2 = cMap.getWayPoint( exit[Lane.ENTRANCE] );
        if( w1 != null && w2 != null ) {
          float x = p.cX(w1.getX());
          float y = p.cY(w1.getY());
          float xx = p.cX(w2.getX());
          float yy = p.cY(w2.getY());
          path.moveTo(x, y);
          path.lineTo(xx, yy );
          // draw a very crude arrow
          path.lineTo( xx+(x-xx)/4.0f+5, yy+(y-yy)/4.0f+5  );
          path.lineTo(xx, yy );
          path.lineTo( xx+(x-xx)/4.0f-5, yy+(y-yy)/4.0f  );
        }
      }
      g2.draw( path );
    }
  }


  /**
   * Doesn't use the default paintWayPoint method
   *
   * @param g2 the graphics component
   * @param s the Spot to be painted
   * @param ps the perspective of the map
   * 
   */
  public void paintSpot(Graphics2D g2, Spot s, Perspective ps) {
    Vector<WayPoint> w = new Vector<WayPoint>();
    w.add(s.getInPoint());
    w.add(s.getCheckPoint());
    if(verbose)
      System.out.println("\tpainting " + w.size() + " waypoints");
    int nwaypoint = w.size();

    WayPoint first = w.get(0);
    GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
    thislane.moveTo(ps.cX(first.getX()), ps.cY(first.getY()));
    for(WayPoint p:w) {
      paintHollowWaypoint(g2, p, ps);
      //g2.draw(new Ellipse2D.Double(ps.cX(p.x), ps.cY(p.y), 3, 3));
      thislane.lineTo(ps.cX(p.getX()), ps.cY(p.getY()));
    }
    g2.setColor(Color.RED);
    g2.draw(thislane);
  }

  /**
   * Doesn't use the default paintWayPoint method
   *
   * @param g2 the graphics component
   * @param s the Spot to be painted
   * @param ps the perspective of the map
   *@param selected Whether or not the zone the spot is associated with is selected
   * 
   */
  public void paintSpot(Graphics2D g2, Spot s, Perspective ps, boolean selected) {
    Vector<WayPoint> w = new Vector<WayPoint>();
    w.add(s.getInPoint());
    w.add(s.getCheckPoint());
    if(verbose)
      System.out.println("\tpainting " + w.size() + " waypoints");
    int nwaypoint = w.size();

    WayPoint first = w.get(0);
    GeneralPath thislane = new GeneralPath(GeneralPath.WIND_EVEN_ODD, nwaypoint);
    thislane.moveTo(ps.cX(first.getX()), ps.cY(first.getY()));
    for(WayPoint p:w) {
      paintHollowWaypoint(g2, p, ps);
      //g2.draw(new Ellipse2D.Double(ps.cX(p.x), ps.cY(p.y), 3, 3));
      thislane.lineTo(ps.cX(p.getX()), ps.cY(p.getY()));
    }
    if(selected){
      g2.setColor(Default.selectedColor);
    } else {
      g2.setColor(Color.RED);
    }
    g2.draw(thislane);
  }

  /**
   * paints the visual representation of the RNDF file
   * @param g the Graphics component
   */
  protected void paintComponent( Graphics g ) {
    super.paintComponent(g);
    Graphics g2 = g.create();
    Graphics2D g2d = (Graphics2D)g2;
    //g2d.draw(new Rectangle2D.Double(10, 10, 800 ,600));
    //g2d.translate(25, -13);
    //g2d.translate(100, -50);
    //g2d.scale(4, 4);

    if(cMap != null) {
      //p.setPerspective(this.getWidth(), this.getHeight());
      paintBackground(g2d, p);
      for(Segment s: cMap.getSegs()) {
        if(verbose)
          System.out.println("painting segment " + s.segment_name);
        for(Lane l: s.getLanes()) {
          try{
            if(verbose)
              System.out.println("\tpainting Lane with width " + l.getLane_width());
            paintLane(g2d, l, p);
          }catch (NullPointerException e){
            // Throws after a lane is deleted
          }
        }
      }
      for(Zone z: cMap.getZones()) {
        if(verbose)
          System.out.println("painting zone " + z.zone_name);
        // pass the selection state of z to insure the use of the
        // correct color
        paintPerimeter(g2d, z.perimeter, p, z.isSelected());
        for(Spot s: z.getSpots()) {
          paintSpot(g2d, s, p, z.isSelected());
        }
      }
    }
    paintUnassignedWayPoints(g2d, p);
  }

  public Vector<WayPoint> getWay() {
    return way;
  }

  public void setWay(Vector<WayPoint> way) {
    this.way = way;
  }

  /**
   * Returns the perspective used to render the map.
   *
   *@return The perspective used to render the map.
   */
  public Perspective getP() {
    return p;
  }

  public void setP(Perspective p) {
    this.p = p;
  }

  public void setBackgroundLowerLeft( double lat, double lon ) {
    backLowerLeft = new Point2D.Double(lat, lon );
    System.out.println( "setting lower-left to:    " + lat + "  " + lon );
    repaint();
  }
  public void setBackgroundUpperRight( double lat, double lon ) {
    backUpperRight = new Point2D.Double(lat, lon);
    System.out.println( "setting upper-right to:   " + lat + "  " + lon );
    repaint();
  }

  private boolean bShowBackground = true;
  public boolean toggleBackgroundVisibility() {
    bShowBackground = !bShowBackground;
    return bShowBackground;
  }

  }

