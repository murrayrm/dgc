import java.io.*;
import java.text.DecimalFormat;
/**
 * A class to represent a Way point
 * @author Xiang Jerry He
 *
 */
public class WayPoint implements Comparable{
	/** representing the id as an array
	 * for example, 12.3.16 would be [12 3 16] in this case 
	 */
    public int[] id; /* include this info in toString? */
    /** x coordinate */
    private double x;
    /** y coordinate */
    private double y;
    /** Holds whether this waypoint has been selected by the user for manipulation */
    private boolean selected = false;
    /** Whether this waypoint is a check point */
    private boolean checkpoint = false;
    /** checkpoint id of this waypoint */
    private int checkpointId = 0;
    
    
    private WayPoint() {} // Implicit super constructor: do not use
    
    /**
     * @param ident the id of this waypoint
     * @param x     the x coordinate of this waypoint
     * @param y     the y coordinate of this waypoint
     */
    WayPoint(String ident, double x, double y) {
    	this.x = x;
    	this.y = y;
    	
    	setId(WayPoint.toIntRep(ident));
    }
    
    /**
     * @param id    the id of this waypoint
     * @param x     the x coordinate of this waypoint
     * @param y     the y coordinate of this waypoint
     */
     
	WayPoint(int[] id, double x, double y) {
		this.setId(id);
		this.x = x;
		this.y = y;
	}
	
	/**
	 * 
	 * @param way the id of this waypoint in string form
	 * @return the id of this waypoint in array form
	 */
	public static int[] toIntRep(String way) {
            // For some unknown reason, way.split(".") doesn't work, but if '.' is replaced by '&' first
            // it does.  Also need to kill any white spaces to prevent combining
            // the last element of the id with the first element of the position
            // Not sure why, as the tokenizer ought to take care of this
            way = way.replace('.', '&').trim();
            int index = way.indexOf(" ");
            if(index != -1)
                way = way.substring(0, way.indexOf(" "));
            String[] tokens = way.split("&");
            int[] iRep = new int[tokens.length];
            for (int i = 0; i < tokens.length; i++) {
                    iRep[i] = Integer.parseInt(tokens[i]);
            }

            return iRep;
	}
	
	public String toString() {
            DecimalFormat format = new DecimalFormat();
            format.setMinimumFractionDigits(6);
            format.setMaximumFractionDigits(6);
            try{
                return (getId()[0]+"."+getId()[1]+"."+getId()[2]+"        " + 
                        format.format(x) + "    " + format.format(y));
            } catch(ArrayIndexOutOfBoundsException e){
                try{
                    return (getId()[0]+"."+getId()[1]+ "       " + 
                            format.format(x) + "   " + format.format(y));
                }catch(ArrayIndexOutOfBoundsException f){
                    System.err.println("Waypoint has no associated id");
                    return (x + " " + y);
                }
            }
	}

    /**
     * Prints the WayPoint to a file using the DARPA RNDF format
     *
     * @param file The PrintWriter to use to print the waypoint
     */
     public void printWayPoint(PrintWriter file){
        DecimalFormat format = new DecimalFormat();
        format.setMinimumFractionDigits(6);
        format.setMaximumFractionDigits(6);
        file.print(id[0] + "." + id[1] + "." + id[2] + "\t");
        file.println(format.format(x) + "\t" + format.format(y));
     }
     
     /**
      * Prints the WayPoint as a checkpoint to a file using the DARPA RNDF format.
      * Does nothing if the waypoint is not a checkpoint.  It prints an error and does
      * nothing if the checkpoint id is not greater than zero, as this indicates a design
      * problem.
      */
     public void printCheckPoint(PrintWriter file){
         if(!checkpoint)
             return;
         if(checkpointId<1){
             System.err.println("Error in checkpoint id");
             return;
         }
         System.err.println("Printing Checkpoint");
         file.println("checkpoint\t" + id[0] + "." + id[1] + "." + id[2] + "\t" + checkpointId);
     }
     
     /**
      * Compares to another WayPoint, based on id values.  The segment component of the ID takes 
      * precedence, follwed by the Lane component, followed by the Waypoint component.  (first, second,
      * and third values in the id, respectively).  The difference in the values of the two ids are returned
      * which is negative if 'this' is less than the thing to be compared to.
      *
      *@param other The other Waypoint to be compared to
      *@return The difference in ID values of the two waypoints
      *@throws ClassCastException - if the passed object is not a waypoint
      */
     public int compareTo(Object other){
         if(!(other instanceof WayPoint)){
             throw new ClassCastException("Tried to compare to a non-waypoint");
         }
         WayPoint otherWay = (WayPoint) other;
         // If not in the same segment, return the difference of Segment id
         if(id[0] != otherWay.getId()[0]){
             return id[0] - otherWay.getId()[0];
         }else if(id[1] != otherWay.getId()[1]){ // If not in the same lane, return difference of lane ID
             return id[1] - otherWay.getId()[1];
         } else{
             // Return the difference in WayPoint number within the lane.
             return id[2] - otherWay.getId()[2];
         }
     }
        
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int[] getId() {
        return id;
    }

    public void setId(int[] id) {
        this.id = id;
    }
    
    /** 
     * Sets the value of the specified component of the id.  0 for segment
     * 1 for lane, 2 for waypoint id within the lane
     *
     * @param index The component of the id to modify.
     * @param value The new value for the id component
     */
    public void setId(int index, int value){
        if(index < 0 || index >2){
            System.err.println("Tried to set a non-existent component of an way point id " + this);
            System.exit(-1);
        }
        id[index] = value;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public boolean isCheckpoint() {
        return checkpoint;
    }

    /**
     * Sets checkpoint.  If checkpoint is false, sets the checkpointId to be 0
     */
    public void setCheckpoint(boolean checkpoint) {
        this.checkpoint = checkpoint;
        // If set to not be a checkpoint, change the id to 0, which may be used to 
        // indicate whether this is a checkpoint
        if(!checkpoint){
            checkpointId = 0;
        }
    }

    public int getCheckpointId() {
        return checkpointId;
    }

    /**
     * Sets the checkpointId value.  If the new checkpointId value is <1, sets
     * checkpoint to be false, as it wouldn't be a valid checkpoint id in the first place
     *
     *  CURRENTLY NO CODE TO MAKE SURE MULTIPLE WAYPOINTS AREN'T ASSIGNED SAME CHECKPOINT
     *  ID
     */
    public void setCheckpointId(int checkpointId) {
        this.checkpointId = checkpointId;
        // If checkPointID <1, set not to be a checkpoint, as that isn't a valid checkpoint
        // id
        if(checkpointId < 1){
            checkpoint = false;
        }else{
            checkpoint = true;
        }
    }
}