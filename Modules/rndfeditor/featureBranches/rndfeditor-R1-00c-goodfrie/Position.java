import java.util.StringTokenizer;

/**
 * a Position class
 * @author Xiang Jerry He
 */

public final class Position implements Comparable<Position> {

	private final int row;
	private final int col;
	
	public Position(int r,int c) {
		row = r;
		col = c;
	}
	
	public int getRow() { return row; }
	public int getCol() { return col; }
	
	public boolean equals(Object o) {
		if (o == null || !(o instanceof Position)) {
			return false;
		}
		Position op = (Position) o;
		return ( row == op.row && col == op.col );
	}
	
	/** 
	 * compares two Positions 
	 */
	public int compareTo(Position other) {
		if (this.row > other.row)
			return 1;
		else if(this.row < other.row)
			return -1;
		else if(this.col > other.col)
			return 1;
		else if(this.col < other.col)
			return -1;
		else
			return 0;
	}
	
	/**
	 * @param pos the position in the maze in the form of "x y"
	 * @return a Position instance Position(x, y)
	 */
	public static Position readPosition(String pos) {
		StringTokenizer st = new StringTokenizer(pos);
		int x = Integer.parseInt(st.nextToken());
		int y = Integer.parseInt(st.nextToken());
		assert(!st.hasMoreTokens());
		return new Position(x, y);
	}
	
	/**
	 * @param other the other Position to compare to
	 * @return true if other is adjacent to self
	 * @return false if other is not adjacent to self
	 */
	public boolean adjacentTo(Position other) {
		int dis = Math.abs(this.row-other.row)+Math.abs(this.col-other.col);
		if(dis==1)
			return true;
		return false;
	}
	
	public int hashCode() {
		int result = 17;
		result = 37*result + this.row;
		result = 37*result + this.col;
		return result;
	}
	
	
	public String toString() {
		return "(" + row + "," + col + ")";
	}
	
}
