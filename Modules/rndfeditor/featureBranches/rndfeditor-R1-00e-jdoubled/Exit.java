import java.io.IOException;
import java.io.*;


/**
 * Represents an exit point from a zone.  An exit consists of an exit perimeter point
 * and an entry waypoint, which are connected.
 * @author David Waylonis
 */
public class Exit {

	WayPoint exitPoint;
	WayPoint entryPoint;
	
	/**
	 * Default constructor.  Takes in an exit perimeter point and an entry waypoint.
	 * 
	 * @param exit the exit perimeter point
	 * @param entry the entry waypoint
	 */
	public Exit(WayPoint exit, WayPoint entry) {
		
		exitPoint = exit;
		entryPoint = entry;
	}
        
        public void printExit(PrintWriter file){
           file.print("exit\t\t");
            file.print("" + exitPoint.getId()[0] + "." + exitPoint.getId()[1] + "."
                    + exitPoint.getId()[2] + "\t");
            file.println(entryPoint.getId()[0] + "." + entryPoint.getId()[1] + "." +
                    entryPoint.getId()[2]);
            
        }
}
