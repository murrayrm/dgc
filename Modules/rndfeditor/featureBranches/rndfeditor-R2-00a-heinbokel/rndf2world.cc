/*-*-c++-*-
=========================================================================
=                                                                       =
=                                rndf2world.cc                          =
=                                                                       =
=========================================================================

This file converts rndf files to gazebo world files.

Author: Bjorn Heinbokel
Date:   July 22, 2008 

Modification History
-------------------------------------------------------------------------
0.1    07/22/2008  Original creation date.
-------------------------------------------------------------------------*/


#include <iostream>
#include <string>
#include <sstream>

#include "dgcutils/ggis.h"

using namespace std;

int main (int argc, char** argv)
{
  string input_line;
  int segment  = 0;
  int lane     = 0; 
  int waypoint = 0;
  int old_segment  = 0;
  int old_lane     = 0; 
  int old_waypoint = 0;

  double latitude  = 0.0;
  double longitude = 0.0;
  //origin coordinates, local frame coincides with wp 1.1.1
  double nor0     = 0.0;
  double eas0     = 0.0;

  char dot;

  string element;
  string data;

  int tab_loc;
  
  // LatLon to UTM converter variables
  GisCoordLatLon latlon;
  GisCoordUTM utm;
  utm.zone = 5;
  utm.letter = 'S';
  double northing, easting;
  

  // Set up cout parameters  Need 6 decimal place precision to see proper lat/long detail

  cout.setf(ios::fixed, ios::floatfield);
  cout.setf(ios::showpoint);
  cout.precision(6);

  // Start of kml file
  cout << "<?xml version=\"1.0\"?>" << endl << endl;

  cout << "<!-- Automatically generated model containing the rndf waypoint data -->" << endl;
  cout << "<model:physical name=\"tcAutoWaypoints\"" << endl;
  cout << "  xmlns:xi=\"http://www.w3.org/2001/XInclude\"" << endl;
  cout << "  xmlns:gazebo=\"http://playerstage.sourceforge.net/gazebo/xmlschema/#gz\"" << endl;
  cout << "  xmlns:model=\"http://playerstage.sourceforge.net/gazebo/xmlschema/#model\"" << endl;
  cout << "  xmlns:geom=\"http://playerstage.sourceforge.net/gazebo/xmlschema/#geom\" >" << endl << endl;

  do
    {
      
      // Read a line from stdin:
      getline (cin,input_line);
      // Get rid of comments
      input_line = input_line.substr(0, input_line.find("/*"));
      tab_loc = input_line.find('\t');
      element = input_line.substr(0,tab_loc);
      data    = input_line.substr(tab_loc + 1);

      // There is a line for every possible RNDF line except comments 

      if (element == "lane")
	{
	
/* OLD CODE
	  // Start a new lane 

	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << data << "</name>" << endl;
	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ffff1111</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<LineString>" << endl;
	  cout << "		   <coordinates>" << endl;
*/
	}
      else if (element == "end_lane")
	{
/* OLD CODE
	  // End the lane

	  cout << "		  </coordinates>" << endl;
	  cout << "		</LineString> " << endl;

	  cout << "	</Placemark>" << endl;
*/
	}
      else if (element == "RNDF_name")
	{
	  //cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "num_segments")
	{
	}
      else if (element == "num_zones")
	{
	}
      else if (element == "format_version")
	{
	}
      else if (element == "segment")
	{
	  //cout << "<Folder>" << endl;
	  //cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "end_segment")
	{
	  //cout << "</Folder>" << endl;
	}
      else if (element == "num_lanes")
	{
	}
      else if (element == "segment_name")
	{
	  //cout << "<description>" << data << "</description>" << endl;
	}
      else if (element == "num_waypoints")
	{
	}
      else if (element == "lane_width")
	{
	}
      else if (element == "left_boundary")
	{
	}
      else if (element == "right_boundary")
	{
	}
      else if (element == "exit")
	{
	}
      else if (element == "checkpoint")
	{
	}
      else if (element == "stop")
	{
	}
      else if (element == "zone")
	{
	  // Start a zone

	  //cout << "<Folder>" << endl;
	  //cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "end_zone")
	{
	  //cout << "</Folder>" << endl;
	}
      else if (element == "perimeter")
	{
	  
	  
/*	OLD CODE  
	  // Start a perimeter.  It would be nice to save the initial perimeter point
	  // to close the zone by putting it as the last coordinate.

	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << data << "</name>" << endl;
	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ff00ffff</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<LineString>" << endl;
	  cout << "		   <coordinates>" << endl;
*/
	}
      else if (element == "end_perimeter")
	{
	  /*
	  cout << "		  </coordinates>" << endl;
	  cout << "		</LineString> " << endl;

	  cout << "	</Placemark>" << endl;
	  */
	}
      else if (element == "spot_width")
	{
	}
      else if (element == "spot")
	{
	/*
	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << data << "</name>" << endl;
	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ffffffff</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<LineString>" << endl;
	  cout << "		   <coordinates>" << endl;
	  
	*/
	}
      else if (element == "end_spot")
	{
	/*
	  cout << "		  </coordinates>" << endl;
	  cout << "		</LineString> " << endl;

	  cout << "	</Placemark>" << endl;
*/
	}
      else if (element == "end_file")
	{
	  // Close out the file and exit

	  //cout << "</Folder>" << endl;
	  //cout << "</kml>" << endl;

    cout << "</model:physical>" << endl;    
      
	  return 0;
	}
      else if (element == "num_perimeterpoints")
	{
	}
      else if (element == "creation_date")
	{
	}
      else if (element == "")
	{
	}
      else if (element == "")
	{
	}
      else
	{
	  // So if the line is nothing else, It must be a coordinate
	  // Parse the line if possible
	  stringstream(input_line) >> segment >> dot >> lane >> dot >> waypoint >> latitude >> longitude;
	  
	  // Calculate the northing and easting coordinates using a fn from ggis.h
    latlon.latitude  = latitude;
    latlon.longitude = longitude;
    gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL);
    northing = utm.n;
    easting  = utm.e;
	  
	  if ((segment == 1) && (lane == 1) && (waypoint == 1))
	  {
	    nor0 = northing;
	    eas0 = easting;
	  }
	  
	  northing -= nor0;
	  easting  -= eas0;
	  
	  // Print cross into gazebo worldfile at UTM coordinates
	  cout << "  <model:physical name=\"box_wp_" << segment << "." << lane << "." << waypoint << "\">" << endl;
    cout << "    <xyz>" << northing << " " << -easting << " " << "0</xyz>" << endl;
    //cout << "    <canonicalBody>box1_body</canonicalBody>" << endl;
    cout << "    <static>true</static>" << endl;
    cout << "    <include embedded=\"true\">" << endl;
    cout << "      <xi:include href=\"test_lines.model\" />" << endl;
    cout << "    </include>" << endl;
    cout << "  </model:physical>" << endl;
    cout << endl;
	  

	  
	}

      // Yes I know we don't need this test, but if we don't do a clean exit 
      // above, we can catch it here.

    }while (element != "end_file");

  return -1;
}
