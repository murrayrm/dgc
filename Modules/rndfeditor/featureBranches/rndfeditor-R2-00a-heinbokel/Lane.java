
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;
import java.util.Iterator;
import java.util.regex.Pattern;
import java.util.StringTokenizer;
import java.io.BufferedReader;
import java.io.*;
import java.util.Collections;
//import Ids;

import javax.swing.JOptionPane;

/**
 * A class to represent a Lane
 *
 * NOTE: THE ENTRANCES AND EXITS CURRENTLY DON"T CHANGE IF THE WAYPOINTS ARE SHIFTED, BUT
 * STAY POINTING TO A GIVEN INDEX.  THIS NEEDS TO GET CHANGED AT SOME POINT
 *
 * @author Xiang Jerry He
 */

public class Lane implements Comparable{
    /** 
     * an array of ids of the waypoints in this lane 
     */
    private int[] id;
    /** 
     * number of waypoints on this lane 
     */
    private int num_waypoints;
    /** the width of this lane */
    private int lane_width;

    Vector<Integer> stopSignIndex;
    /** a vector of waypoints that correspond to stop signs */
    private Vector<WayPoint> stopSign;
    /** the left boundary type */
    private String left_boundary;
    /** the right boundary type */
    private String right_boundary;
    /** collection of all waypoints in this lane */
    private Vector<WayPoint> waypoints;
    /** Holds whether this lane is currently selected by the user */
    private boolean selected = false;
    /**
     * Holds the ids of all waypoints that function as exits and exits.  Each pair is stored
     * as a two dimensional array following [entrance/exit][id].  entrance is 1, exit is 0
     */
    private Vector<int[][]> exits;
    /** Entrance index */
    public static final int ENTRANCE = 1;
    /** Exit index */
    public static final int EXIT = 0;

    /** A list of boundaries */
    //TODO change the string based boundary system to enum based
    private static enum Boundary {
	double_yellow, broken_white;
    }

    /**
     * A simple constructor to initialize a lane with the provided
     * id
     */
    public Lane(int[] id){
	stopSignIndex = new Vector<Integer>();
	stopSign = new Vector<WayPoint>();
	waypoints = new Vector<WayPoint>();
	exits = new Vector<int[][]>();
	num_waypoints = 0;
	if(id.length != 2){
	    System.err.println("Illegal lane id " + id);
	    return;
	}
	//this.id = id;
	setId( id );
    }

    public Lane(){

    }

    /**
     * Read in and parse a lane, gathers all lane-specific information  
     * @author Xiang Jerry He
     * @param in the buffered reader for the RNDF
     * @throws IOException if we fail to read in the file
     * @throws MapFormatException if the file is incorrectly formatted
     */
    protected void readLane(BufferedReader in) throws IOException, MapFormatException {
	System.out.println("\tEntering lane");
	waypoints = new Vector<WayPoint>();
	exits = new Vector<int[][]>();
	/** Holds which waypoints are checkpoints. The first element is the index of the waypoint to make a checkpoint.
	 * the second is the corrosponding checkpoint id 
	 */
	Vector<int[]> checkPointIndex = new Vector<int[]>();
	Pattern p = Pattern.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}.*");
	String line = in.readLine();
	System.out.println(line);
	boolean widthRead = false;
	// Temporarily holds the indexes of the waypoints to be checkpoints
	// Used to set the "checkpoint" field in 
	while(!line.startsWith("end_lane")) {
	    StringTokenizer tok = new StringTokenizer(line);
	    if(tok.hasMoreTokens()) {  // if not then we have an empty line so do nothing
		String firstToken = tok.nextToken();
		if(firstToken.equals("num_waypoints")) {
		    num_waypoints = Integer.parseInt(tok.nextToken()); 
		    stopSignIndex = new Vector<Integer>(num_waypoints);
		}
		else if(firstToken.equals("lane_width")) {
		    lane_width = Integer.parseInt(tok.nextToken());
		    widthRead = true;
		}
		else if(firstToken.equals("left_boundary")) {
		    left_boundary = (tok.nextToken());
		}
		else if(firstToken.equals("right_boundary")) {
		    right_boundary = (tok.nextToken());
		}
		else if(firstToken.equals("stop")) {
		    String identifier = tok.nextToken(); 
		    identifier = identifier.replace('.', '&');
		    String[] iden = identifier.split("&");
		    for(String s : iden){
			System.out.println(s);
		    }
		    System.out.println("identifier is " + identifier);
		    id = new int[3];
		    for(int i=0; i < 3; i++) {
			id[i] = Integer.parseInt(iden[i]);
		    }
		    if(id.length > 0) {
			stopSignIndex.add(id[id.length-1]);
		    }
		}
		else if(firstToken.equals("checkpoint")){
		    System.err.println("changing checkpoint value");
		    int[] temp = new int[2];
		    String str = tok.nextToken();
		    str = str.replace('.', '&');
		    String[] wayId = str.split("&");
		    System.out.println("id lendth " + wayId.length);
		    for(String s: wayId){
			System.out.println(s);
		    }
		    // copy in the waypoint index of the checkpoint
		    temp[0] = Integer.parseInt(wayId[2]);
		    // read in the checkpoint id of the checkpoint
		    str = tok.nextToken();
		    temp[1] = Integer.parseInt(str);
		    checkPointIndex.add(temp);
		}
		else if(firstToken.equals("exit")){
		    // read in the entrance exit pair
		    int[][] temp = new int[2][3];

		    // read in the exit
		    String str = tok.nextToken();
		    //Replace '.' with '&' because of funky .split operation
		    str = str.replace('.', '&');
		    String[] wayId = str.split("&");
		    temp[EXIT][0] = Integer.parseInt(wayId[0]);
		    temp[EXIT][1] = Integer.parseInt(wayId[1]);
		    temp[EXIT][2] = Integer.parseInt(wayId[2]);

		    // read in the entrance
		    str = tok.nextToken();
		    //Replace '.' with '&' because of funky .split operation
		    str = str.replace('.', '&');
		    wayId = str.split("&");
		    temp[ENTRANCE][0] = Integer.parseInt(wayId[0]);
		    temp[ENTRANCE][1] = Integer.parseInt(wayId[1]);
		    temp[ENTRANCE][2] = Integer.parseInt(wayId[2]);
		    // add this exit entrance pair to the vector
		    exits.add(temp);
		}
		else if(p.matcher(firstToken).matches()) {
		    // For some unknown reason, firstToken.split(".") doesn't work, but if '.' is replaced by '&' first
		    // it does.  Also need to kill any white spaces to prevent combining
		    // the last element of the id with the first element of the position
		    // Not sure why, as the tokenizer ought to take care of this
		    firstToken = firstToken.replace('.', '&').trim();
		    int index = firstToken.indexOf(" ");
		    if(index != -1)
			firstToken = firstToken.substring(0, firstToken.indexOf(" "));
		    String[] identifier = firstToken.split("&");
		    id = new int[identifier.length];
		    for(int i=0; i < identifier.length; i++) {
			id[i] = Integer.parseInt(identifier[i]);
		    }
		    double x = Double.parseDouble(tok.nextToken());
		    double y = Double.parseDouble(tok.nextToken());
		    waypoints.add(new WayPoint(id, x, y));
		    System.out.println(id[0] + "."+id[1]+"."+id[2] +x + " " + y);
		}
	    }
	    line = in.readLine();
	}
	if( !widthRead ) {
	    lane_width = 12;
	    System.err.println("no lane width found. setting default: " + lane_width );
	}
	stopSign = new Vector<WayPoint>();
	/** Sort the waypoint vector to insure proper ordering.  Then assign checkpoint values
	 * as needed
	 */
	Collections.sort(waypoints);
	// Copy the check point info over from the temporary vectors into the waypoints
	for(int[] i: checkPointIndex){
	    System.err.println("changing checkpoint value");
	    waypoints.elementAt(i[0]-1).setCheckpoint(true);
	    waypoints.elementAt(i[0]-1).setCheckpointId(i[1]);
	}
	for(int i = 0; i < stopSignIndex.size(); i++) {
	    stopSign.add(waypoints.get(stopSignIndex.get(i) - 1));
	}
    }

    /**
     *  Moves a waypoint from one lane to another, handling stopsigns and extrypoints etc.
     */
    public void transferWayPoint( WayPoint w, Lane other, Map m ) {
	if( other == null ) {
	    System.out.println( "transferWayPoint: other lane was null" );
	    return ;
	}

	int id[] = Ids.copy( w.getId() );

	other.waypoints.add(w);
	w.setId( 1, other.getId()[1] );
	w.setId( 2, other.waypoints.size() );


	/// take care of stopsigns
	if( this.stopSign.contains(w) ) {
	    other.stopSign.add( w );
	    this.stopSign.remove(w);
	    this.stopSignIndex.removeElement( id[2]-1 );
	    other.stopSignIndex.add( other.waypoints.size() );
	}

	/// take care of exits
	int[] entryEnum = { EXIT };
	Iterator<int[][]> exitsIter = this.exits.iterator();
	for( ; exitsIter.hasNext() ; ) {
	    int[][] exit = exitsIter.next();
	    for( int e: entryEnum ) {
		if( Arrays.equals( exit[e], id ) ) {
		    System.out.println( "Transfering exit: " + (new Vector(Arrays.asList(exit[e]))).toString() );
		    exit[EXIT][1] = other.getId()[1];
		    other.exits.add( exit );
		    exitsIter.remove();
		    // update all of the other lanes that might have been entering into this lane
		    other.laneUpdateEntrances(id[0], other.id[1], other.id[1], id[2], other.waypoints.size() );
		    for( Segment s: m.getSegs() ) {
			for( Lane ll: s.getLanes() ) {
			    if( ll == this ) continue;
			    if( ll == other ) continue;
			    ll.laneUpdateEntrances(id[0], id[1], other.id[1], id[2], other.waypoints.size() );
			    //ll.laneUpdateEntrances(id[0], other.id[1], other.id[1], id[2], other.waypoints.size() );
			}
		    }
		}
	    }
	}

	/// remove from one level higher where you have access to the iterator
	//this.waypoints.remove(w);
	return;
    }

    /**
     * Writes the lane to a text file using the DARPA RNDF format.  \t is the tab charecter
     *
     * CANNOT write CHECKPOINTS OR EXITS, as the current code doesn't support them
     *
     *@param file The PrintWriter to use to write the file
     */
    public void printLane(PrintWriter file){
	file.println("lane\t" + id[0] + "." + id[1]);
	file.println("num_waypoints\t" + num_waypoints);
	if(lane_width != 0)
	    file.println("lane_width\t" + lane_width);
	if(left_boundary != null)
	    file.println("left_boundary\t" + left_boundary);
	if(right_boundary != null)
	    file.println("right_boundary\t" + right_boundary);
	// Cycle through and print any checkpoints
	for(WayPoint w: waypoints){
	    w.printCheckPoint(file);
	}
	if(stopSign != null && stopSign.size() > 0){
	    for(WayPoint w: stopSign){
		file.println("stop\t" + w.getId()[0] + "." + w.getId()[1] + "."
			     + w.getId()[2]);
	    }
	}
	// Prints out all the exit entrance pairs
	for(int[][] i : exits){
	    file.println("exit\t" + i[EXIT][0] + "." + i[EXIT][1] + "." + i[EXIT][2] + "\t" + i[ENTRANCE][0] + "."
			 + i[ENTRANCE][1] + "." + i[ENTRANCE][2]);
	}
	for(WayPoint w: waypoints){
	    w.printWayPoint(file);
	}
	file.println("end_lane");
    }


    /**
     * Returns true if the waypoint is part of an entrance-exit pair in the lane.
     *
     * @param w The WayPoint to check for membership in an entrance-exit pair
     */
    public boolean isBorderElement(WayPoint w){
	for(int[][] i: exits){
	    if(w.getId().equals(i[ENTRANCE])){
		// returns true if an entrance
		return true;
	    }
	    if(w.getId().equals(i[EXIT])){
		// returns true if an exit
		return true;
	    }
	}
	return false;
    }

    /**
     * Removes the selected waypoint, and updates the ids of the other waypoints
     * Requires the map the waypoint is removed from so that the entrance-exitpairs can be updated
     * @param w The WayPoint to remove
     *
     * @returns True if the waypoint was present, and removed.
     */
    public boolean removeWaypoint(WayPoint w, Map m){
	//Get the index of the waypoint
	System.out.println("Removing waypoint " + w.getId()[0] + "." + w.getId()[1] + "." + w.getId()[2]);
	int i = w.getId()[2]-1;
	/*	for(Segment s: m.getSegs()){
	    for(Lane l: s.getLanes()){
		l.laneUpdateEntrances(id[0], id[1], id[1], i+1, 0);
	    }
	}
	*/
	for(i = i; i< waypoints.size(); i++){
	    // updates the id of the waypoint
	    int[] temp = waypoints.elementAt(i).getId();
	    // NOTE: Works by side effecting.  Probably not the best choice.
	    temp[2] = temp[2]-1;
	    // update exit-entrance pairs globablly
	    for(Segment s: m.getSegs()){
		for(Lane l: s.getLanes()){
		    l.laneUpdateEntrances(id[0], id[1], id[1], i+1, i);
		}
	    }
	}
	stopSign.remove(w);
	for(int j = 0; j< stopSignIndex.size(); j++){
	    if(w.getId()[2] == stopSignIndex.elementAt(j)){
		stopSignIndex.remove(j);
		j--;
	    }
	}
	waypoints.remove(w.getId()[2]);
	num_waypoints--;

	Collections.sort(waypoints); // Just to make sure, shouldn't cause any real speed problems
	return true;
	//        return removed;
    }
    /**
     * Implements compareTo for list sorting.  Bases comparison on the id value, with segment
     * association being more important than lane position within a segment, ie a lane in
     * a lower numbered segment is lesser, then a lane with a lower number within a given segment is
     * lesser.
     *
     * Will return 1 if compareing to a null
     *
     *@param other The lane to compare to
     *@return Less than zero if this<other, 0 if this == other, greater than zero if this>other
     *@throws ClassCastException - if other is not a Lane
     */
    public int compareTo(Object other){
	if(other == null){
	    System.err.println("Tried to compare to null");
	    throw new ClassCastException("Tried to compare to a null");
	    //            return 1;
	}
	if (!(other instanceof Lane)){
	    throw new ClassCastException("Tried to compare to a non-lane");
	}
	Lane otherLane = (Lane) other;
	if(id[0] - otherLane.getId()[0] == 0){
	    // If the lanes are in the same segments, return a value based on their position
	    // within the lane
	    return id[1] - otherLane.getId()[1];
	}else{
	    // If the lanes are in different segments, return a value based on the relative
	    // posotion in the segment
	    return id[0] - otherLane.getId()[0];
	}
    }

    public Vector<WayPoint> getWayPoints() {
	return waypoints;
    }

    /**
     * Enforces sorting of the waypoints
     */
    public void setWayPoints(Vector<WayPoint> waypoints) {
	this.waypoints = waypoints;
	Collections.sort((Vector) waypoints);
    }

    public boolean isSelected() {
	return selected;
    }

    public void setSelected(boolean selected) {
	this.selected = selected;
    }

    public String toString(){
	return "Lane " + id[0] + "." + id[1];
    }

    public int[] getId() {
	return id;
    }

    public void setId(int[] id) {
	this.id = new int[id.length];
	for( int i=0; i<id.length; i++ ) {
	    setId( i, id[i] );
	}
    }

    /**
     * Sets the selected element of the id. 0 for segment, 1 for lane id.  Propogates this change
     * down to the waypoints, and exits (does nothing to entrances)
     *
     * @param component Which part of the id to set 0 segment, 1 lane
     * @param id The new value to use
     */
    public void setId(int component, int id){
	// Update all the waypoints with the new id information
	for(WayPoint w: waypoints){
	    w.setId(component, id);
	}
	// Check the exits
	for(int i = 0; i < exits.size(); i++){
	    // Note that an exit must be a component of this lane, so the update code is simple
	    exits.get(i)[EXIT][component] = id;
	}
	// update the id field itself
	this.id[component] = id;
    }

    /**
     * Updates the entrances to accomodate the specified change in lane id.  Only deals with lane changes
     * within a segment, or the deletion of a segment or lane
     * oldLane == newLane indicates that a waypoint has had its id changed within a waypoint, and that this
     * is what needs updating.
     *
     * Note that values after a termination value, either a-1 or equal numbers for newLane and oldLane, can
     * have arbitary values, probably -1 is best
     * 
     * @param segId The id of the segment who has had its lanes modified
     * @param oldLane The lane that has been modified.  -1 for a deleted segment
     * @param newLane oldLane's new lane id.  -1 for a deleted lane
     * @param oldWaypoint The original waypoint id of the waypoint that has been edited
     * @param newWaypoint The waypoints' new value.  -1 for a deleted waypoint
     */
    public void laneUpdateEntrances(int segId, int oldLane, int newLane, int oldWayPoint, int newWayPoint){
	//	System.out.println("Changing waypoint " + segId + "." + oldLane + "." + oldWayPoint + " to " + segId + "." + newLane + "." + newWayPoint + " in laneUpdateEntrances");
	// This assumes that lanes can never change
	for(int i = 0; i < exits.size(); i++){
	    //	    System.out.println("Checking exit pair " + i);
	    int[][] pair = exits.elementAt(i);
	    // only care about those with entrances in the effected segment
	    if(pair[ENTRANCE][0] == segId){
		// if the segment was deleted, remove any pair that has an entrance in it
		if(oldLane <= -1){
		    System.out.println("Removing exit pair");
		    System.out.println("Exit pair removal successful " + exits.remove(pair));
		    i--; // Make sure that nothing gets skipped
		}else if(pair[ENTRANCE][1] == oldLane){
		    // if the old lane was deleted, remove all entrances from that lane
		    if(newLane <= -1){
			exits.remove(pair);
			i--;
		    } else if(oldLane != newLane && (oldWayPoint <= 0 || pair[ENTRANCE][2] == oldWayPoint) ){
			System.err.println("Copying Lane info " + id[0] + "." + id[1] + " change " + oldLane + " " + newLane);
			// update the entrance to the new lane id
			pair[ENTRANCE][1] = newLane;
		    }else{
			// It is an individual waypoint that needs editing
			if(pair[ENTRANCE][2] == oldWayPoint){
			    // Exit pair contains the waypoint of interest
			    if(newWayPoint <= -1){
				// deletion
				exits.remove(pair);
				i--;
			    }else{
				pair[ENTRANCE][2] = newWayPoint;
			    }
			}
		    }
		}
	    }
	    if(pair[EXIT][0] == segId && pair[EXIT][1] == oldLane ){
		// if the segment was deleted, remove any pair that has an entrance in it
		if(oldLane == 0){
		    System.out.println("Removing exit pair");
		    System.out.println("Exit pair removal successful " + exits.remove(pair));
		    i--; // Make sure that nothing gets skipped
		}else if(oldLane != newLane){
		    System.err.println("Copying Lane info " + id[0] + "." + id[1] + " change " + oldLane + " " + newLane);
		    // update the entrance to the new lane id
		    pair[EXIT][1] = newLane;
		}else{
		    // It is an individual waypoint that needs editing
		    if(pair[EXIT][2] == oldWayPoint){
			// Exit pair contains the waypoint of interest
			if(newWayPoint < 0){
			    // deletion
			    exits.remove(pair);
			    i--;
			}else{
			    pair[EXIT][2] = newWayPoint;
			}
		    }
		}

	    }
	}
    }

    public void changeEntranceSegments( int oldSeg, int oldLane, int newSeg, int newLane ) {
	for( int[][] pair: exits ) {
	    if( pair[ENTRANCE][0] == oldSeg && pair[ENTRANCE][1] == oldLane) {
		pair[ENTRANCE][0] = newSeg;
		pair[ENTRANCE][1] = newLane;
	    }
	    if( pair[EXIT][0] == oldSeg && pair[EXIT][1] == oldLane ) {
		pair[EXIT][0] = newSeg;
		pair[EXIT][1] = newLane;
	    }
	}
    }

    /** 
     * If you've deleted a segment, run this function to shift all the exit/entry points
     * on all segments after that one
     */
    public void shiftBackExitEntrance( int seg ) {
	System.out.println("Shifting back exit/entry for segment " + seg);

	for( int[][] pair: exits ) {
	    System.out.println("Old entrance/exit: " + pair[EXIT][0] + "." + pair[EXIT][1] + " to " 
			       + pair[ENTRANCE][0] + "." + pair[ENTRANCE][1]);
	    if (pair[ENTRANCE][0] > seg) {
		pair[ENTRANCE][0]--;
	    }
	    if( pair[EXIT][0] > seg) {
		pair[EXIT][0]--;
	    }
	    System.out.println("New entrance/exit: " + pair[EXIT][0] + "." + pair[EXIT][1] + " to " 
			       + pair[ENTRANCE][0] + "." + pair[ENTRANCE][1] +"\n");
	}
    }

   /** 
     * If you've added a segment or zone, run this function to shift all the exit/entry points
     * on all segments after that one
     */
    public void shiftForwardExitEntrance( int seg ) {
	System.out.println("Shifting forward exit/entry for segment " + seg);

	for( int[][] pair: exits ) {
	    System.out.println("Old entrance/exit: " + pair[EXIT][0] + "." + pair[EXIT][1] + " to " 
			       + pair[ENTRANCE][0] + "." + pair[ENTRANCE][1]);
	    if (pair[ENTRANCE][0] > seg) {
		pair[ENTRANCE][0]++;
	    }
	    if( pair[EXIT][0] > seg) {
		pair[EXIT][0]++;
	    }
	    System.out.println("New entrance/exit: " + pair[EXIT][0] + "." + pair[EXIT][1] + " to " 
			       + pair[ENTRANCE][0] + "." + pair[ENTRANCE][1] +"\n");
	}
    }

    public void changeSegment( int[] nId ) {
	int[] tmp = getId();
	changeEntranceSegments( tmp[0], tmp[1], nId[0], nId[1] );
	setId( nId );
    }

    public int getNum_waypoints() {
	return num_waypoints;
    }

    public void setNum_waypoints(int num_waypoints) {
	this.num_waypoints = num_waypoints;
    }

    public int getLane_width() {
	return lane_width;
    }

    public void setLane_width(int lane_width) {
	this.lane_width = lane_width;
    }

    public String getLeft_boundary() {
	return left_boundary;
    }

    public void setLeft_boundary(String left_boundary) {
	this.left_boundary = left_boundary;
    }

    public String getRight_boundary() {
	return right_boundary;
    }

    public void setRight_boundary(String right_boundary) {
	this.right_boundary = right_boundary;
    }

    public void setWaypoints(Vector<WayPoint> waypoints) {
	this.waypoints = waypoints;
    }

    public Vector<WayPoint> getStopSign() {
	return stopSign;
    }

    public void setStopSign(Vector<WayPoint> stopSign) {
	this.stopSign = stopSign;
    }

    public Vector<int[][]> getExits() {
	return exits;
    }

    /**
     * The exits are stored as int[][]s, of the form
     * int[exit/entrance][waypoint id], with exit corrosponding to 0, and entrance
     * corrosponding to 1.  The waypoint id is in segment lane waypoint form.
     */
    public void setExits(Vector<int[][]> exits) {
	this.exits = exits;
    }

}
