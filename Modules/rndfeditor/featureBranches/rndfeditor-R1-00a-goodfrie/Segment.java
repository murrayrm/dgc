import java.io.BufferedReader;
import java.io.IOException;
import java.io.*;
import java.util.Arrays;

/**
 * a Segment class
 * @author Xiang Jerry He
 *
 */

public class Segment implements Comparable{
	      /** the id of this segment */
	      protected int id;
	      /** number of lanes */
		  protected int num_lanes;
		  /** name of this segment */
		  protected String segment_name;
		  /** list of all lanes in this segment */
		  private Lane[] lanes; 
                  /** Holds whether this segement is selected */
                  private boolean selected = false;
		  
			/**
			 * Read in and parse a Segment
			 * @author Xiang Jerry He
			 * @param in the buffered reader for the RNDF
			 * @throws IOException if we fail to read in the file
			 * @throws MapFormatException if the file is incorrectly formatted
			 */
		  void readSegment(BufferedReader in) throws IOException, MapFormatException {
			  System.out.println("Entering segment");
			  String line = in.readLine();
                          System.out.println(line);
			  String[] tokens = line.split("\\s");
			  while(!line.startsWith("lane")) {
			       System.out.println(line);
			       //if it is not an empty line, then process it
			       if(line.matches("\\w.*")) {
				       tokens = line.split("\\s");
				       if(line.startsWith("num_lanes")) {
				    	   System.out.println("numlanetoken: " + tokens[1]);
				    	   num_lanes = Integer.parseInt(tokens[1]);
				       } 
				       else if(line.startsWith("segment_name")) {
                                           
				    	   segment_name = tokens[1];
				       } else if(line.startsWith("segment")){
                                           // Doesn't have to worry about getting confused segment_name
                                           // as that has already been checked.
                                           //Read in the segment ID(will not occur on the first
                                           // call from readMap, as the needed line has already been
                                           // read before the readSegment call
                                           System.out.println(tokens[1]);
                                           id = Integer.parseInt(tokens[1]);
                                       }
			       }
			       line = in.readLine();
			  }
			  System.out.println("number of lanes is " + num_lanes);
			  System.out.println("segment name is "+ segment_name);
			  System.out.println(line);
			  lanes = new Lane[num_lanes];
			  for(int i=0; i < num_lanes; i++) {
                                  lanes[i] = new Lane();
				  lanes[i].readLane(in);
			  }
			  assert(in.readLine().equals("end_segment"));
		  }
                 
    /**
     * Writes the segment to file using hte given PrintWriter in darpa specified RNDF
     * format
     *
     *@param file The PrintWriter to use to write the segment
     */              
    public void printSegment(PrintWriter file){
        file.println("segment\t\t" + id);
        file.println("num_lanes\t\t" + num_lanes);
        if(segment_name != null)
            file.println("segment_name\t\t" + segment_name);
        for(Lane l: lanes){
            l.printLane(file);
        }
        file.println("end_segment");
    }
    
    /** Compares to another segment based on the id of the two segments 
     *
     *@param other The other segment to compare to.
     *@throws ClassCastException if other is not a Segment
     */
    public int compareTo(Object other){
        if(!(other instanceof Segment)){
            throw new ClassCastException("Tried to compare to a non-Segment object");
        }
        return id - ((Segment) other).getId();
    }

    public Lane[] getLanes() {
        return lanes;
    }

    /**
     * Enforces sorting
     */
    public void setLanes(Lane[] lanes) {
        this.lanes = lanes;
        Arrays.sort(this.lanes);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String toString(){
        return "Segment " + id;
    }
	  
	}
	