/*-*-c++-*-
=========================================================================
=                                                                       =
=                                rndf2kmlpoint.cc                       =
=                                                                       =
=========================================================================

This file converts rndf files to kml format for display as a list of 
points.

Author: Rich Petras
Date:   June 1, 2007

Software Use Notice
------------------
The United States Government has a non-exclusive, non-transferable,
irrevocable royalty-free, worldwide license in the software NPO-30132
under the terms of the prime contract NAS7-1407 with the California
Institute of Technology.  Any agency of the U.S.  Government can use
the software under this license. Since the license is
non-transferable, third party use of this software is limited to uses
for or on behalf of the U.S. Government and permission for any other
use must be negotiated with the Office of Technology Transfer at
Caltech. The software has not necessarily been developed for use
outside of JPL and is being transferred "as is" without warranty of
any kind including fitness for a particular use or purpose.

(C)  2007, Jet Propulsion Laboratory
     California Institute of Technology

Modification History
--------------------
000 01jun2007 rdp - Original creation date.
-------------------------------------------------------------------------*/


#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main (int argc, char** argv)
{
  string input_line;
  int segment  = 0;
  int lane     = 0; 
  int waypoint = 0;
  int old_segment  = 0;
  int old_lane     = 0; 
  int old_waypoint = 0;

  double latitude  = 0.0;
  double longitude = 0.0;

  char dot;

  string element;
  string data;

  int tab_loc;

  // Set up cout parameters  Need 6 decimal place precision to see proper lat/long detail

  cout.setf(ios::fixed, ios::floatfield);
  cout.setf(ios::showpoint);
  cout.precision(6);

  // Start of kml file

  cout << "<kml>" << endl;

  cout << "<Document>" << endl;

  cout << "	<Style id=\"style\">" << endl;
  cout << "		<IconStyle>" << endl;
  cout << "			<color>ff00aa00</color>" << endl;
  cout << "			<scale>0.5</scale>" << endl;
  cout << "			<Icon>" << endl;
  cout << "				<href>http://maps.google.com/mapfiles/kml/shapes/donut.png</href>" << endl;
  cout << "			</Icon>" << endl;
  cout << "			<hotSpot x=\"16\" y=\"16\" xunits=\"pixels\" yunits=\"pixels\"/>" << endl;
  cout << "		</IconStyle>" << endl;
  cout << "	</Style>" << endl;

  cout << "<Folder>" << endl;


  do
    {
      
      // Read a line from stdin:
      getline (cin,input_line);
      // Get rid of comments
      input_line = input_line.substr(0, input_line.find("/*"));
      tab_loc = input_line.find('\t');
      element = input_line.substr(0,tab_loc);
      data    = input_line.substr(tab_loc + 1);

      // There is a line for every possible RNDF line except comments 

      if (element == "lane")
	{


	}
      else if (element == "end_lane")
	{
	}
      else if (element == "RNDF_name")
	{
	  cout << "<name>" << data << " Points" << "</name>" << endl;
	}
      else if (element == "num_segments")
	{
	}
      else if (element == "num_zones")
	{
	}
      else if (element == "format_version")
	{
	}
      else if (element == "segment")
	{
	  cout << "<Folder>" << endl;
	  cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "end_segment")
	{
	  cout << "</Folder>" << endl;
	}
      else if (element == "num_lanes")
	{
	}
      else if (element == "segment_name")
	{
	  cout << "<description>" << data << "</description>" << endl;
	}
      else if (element == "num_waypoints")
	{
	}
      else if (element == "lane_width")
	{
	}
      else if (element == "left_boundary")
	{
	}
      else if (element == "right_boundary")
	{
	}
      else if (element == "exit")
	{
	}
      else if (element == "checkpoint")
	{
	}
      else if (element == "stop")
	{
	}
      else if (element == "zone")
	{
	  // Start a zone

	  cout << "<Folder>" << endl;
	  cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "end_zone")
	{
	  cout << "</Folder>" << endl;
	}
      else if (element == "perimeter")
	{
	}
      else if (element == "end_perimeter")
	{
	}
      else if (element == "spot_width")
	{
	}
      else if (element == "spot")
	{
	}
      else if (element == "end_spot")
	{
	}
      else if (element == "end_file")
	{
	  // Close out the file and exit

	  cout << "</Folder>" << endl;
	  cout << "</Document>" << endl;
	  cout << "</kml>" << endl;

	  return 0;
	}
      else if (element == "num_perimeterpoints")
	{
	}
      else if (element == "creation_date")
	{
	}
      else if (element == "")
	{
	}
      else if (element == "")
	{
	}
      else
	{
	  // So if the line is nothing else, It must be a coordinate
	  // Parse the line if possible
	  stringstream(input_line) >> segment >> dot >> lane >> dot >> waypoint >> latitude >> longitude;
	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << segment <<"."<< lane <<"."<< waypoint
	       << "  Lat = " << latitude << " Long = " << longitude << "</name>" << endl;
	  cout << "				<styleUrl>#style</styleUrl>" << endl;

	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ffffffff</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<Point>" << endl;
	  cout << "		   <coordinates>" << endl;
	  cout  << longitude <<"," << latitude << ",0.0" << endl;
	  cout << "		  </coordinates>" << endl;
	  cout << "		</Point> " << endl;

	  cout << "	</Placemark>" << endl;
	}

      // Yes I know we don't need this test, but if we don't do a clean exit 
      // above, we can catch it here.

    }while (element != "end_file");

  return -1;
}
