/*-*-c++-*-
=========================================================================
=                                                                       =
=                                rndf2kml.cc                            =
=                                                                       =
=========================================================================

This file converts rndf files to kml format for display.

Author: Rich Petras
Date:   November 20, 2006 

Software Use Notice
------------------
The United States Government has a non-exclusive, non-transferable,
irrevocable royalty-free, worldwide license in the software NPO-30132
under the terms of the prime contract NAS7-1407 with the California
Institute of Technology.  Any agency of the U.S.  Government can use
the software under this license. Since the license is
non-transferable, third party use of this software is limited to uses
for or on behalf of the U.S. Government and permission for any other
use must be negotiated with the Office of Technology Transfer at
Caltech. The software has not necessarily been developed for use
outside of JPL and is being transferred "as is" without warranty of
any kind including fitness for a particular use or purpose.

(C)  2006, Jet Propulsion Laboratory
     California Institute of Technology

Modification History
--------------------
000 20nov2006 rdp - Original creation date.
-------------------------------------------------------------------------*/


#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main (int argc, char** argv)
{
  string input_line;
  int segment  = 0;
  int lane     = 0; 
  int waypoint = 0;
  int old_segment  = 0;
  int old_lane     = 0; 
  int old_waypoint = 0;

  double latitude  = 0.0;
  double longitude = 0.0;

  char dot;

  string element;
  string data;

  int tab_loc;

  // Set up cout parameters  Need 6 decimal place precision to see proper lat/long detail

  cout.setf(ios::fixed, ios::floatfield);
  cout.setf(ios::showpoint);
  cout.precision(6);

  // Start of kml file

  cout << "<kml>" << endl;
  cout << "<Folder>" << endl;


  do
    {
      
      // Read a line from stdin:
      getline (cin,input_line);
      // Get rid of comments
      input_line = input_line.substr(0, input_line.find("/*"));
      tab_loc = input_line.find('\t');
      element = input_line.substr(0,tab_loc);
      data    = input_line.substr(tab_loc + 1);

      // There is a line for every possible RNDF line except comments 

      if (element == "lane")
	{

	  // Start a new lane 

	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << data << "</name>" << endl;
	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ffff1111</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<LineString>" << endl;
	  cout << "		   <coordinates>" << endl;

	}
      else if (element == "end_lane")
	{

	  // End the lane

	  cout << "		  </coordinates>" << endl;
	  cout << "		</LineString> " << endl;

	  cout << "	</Placemark>" << endl;
	}
      else if (element == "RNDF_name")
	{
	  cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "num_segments")
	{
	}
      else if (element == "num_zones")
	{
	}
      else if (element == "format_version")
	{
	}
      else if (element == "segment")
	{
	  cout << "<Folder>" << endl;
	  cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "end_segment")
	{
	  cout << "</Folder>" << endl;
	}
      else if (element == "num_lanes")
	{
	}
      else if (element == "segment_name")
	{
	  cout << "<description>" << data << "</description>" << endl;
	}
      else if (element == "num_waypoints")
	{
	}
      else if (element == "lane_width")
	{
	}
      else if (element == "left_boundary")
	{
	}
      else if (element == "right_boundary")
	{
	}
      else if (element == "exit")
	{
	}
      else if (element == "checkpoint")
	{
	}
      else if (element == "stop")
	{
	}
      else if (element == "zone")
	{
	  // Start a zone

	  cout << "<Folder>" << endl;
	  cout << "<name>" << data << "</name>" << endl;
	}
      else if (element == "end_zone")
	{
	  cout << "</Folder>" << endl;
	}
      else if (element == "perimeter")
	{
	  // Start a perimeter.  It would be nice to save the initial perimeter point
	  // to close the zone by putting it as the last coordinate.

	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << data << "</name>" << endl;
	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ff00ffff</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<LineString>" << endl;
	  cout << "		   <coordinates>" << endl;
	}
      else if (element == "end_perimeter")
	{
	  cout << "		  </coordinates>" << endl;
	  cout << "		</LineString> " << endl;

	  cout << "	</Placemark>" << endl;
	}
      else if (element == "spot_width")
	{
	}
      else if (element == "spot")
	{
	  cout << "	<Placemark>" << endl;
	  cout << "		<name>" << data << "</name>" << endl;
	  cout << "		<DrawOrder>0</DrawOrder>" << endl;
	  cout << "		<Style>" << endl;
	  cout << "			<LineStyle>" << endl;
	  cout << "				<color>ffffffff</color>" << endl;
	  cout << "				<width>4</width>" << endl;
	  cout << "			</LineStyle>" << endl;
	  cout << "		</Style>" << endl;
	  cout << "		<LineString>" << endl;
	  cout << "		   <coordinates>" << endl;
	}
      else if (element == "end_spot")
	{
	  cout << "		  </coordinates>" << endl;
	  cout << "		</LineString> " << endl;

	  cout << "	</Placemark>" << endl;
	}
      else if (element == "end_file")
	{
	  // Close out the file and exit

	  cout << "</Folder>" << endl;
	  cout << "</kml>" << endl;

	  return 0;
	}
      else if (element == "num_perimeterpoints")
	{
	}
      else if (element == "creation_date")
	{
	}
      else if (element == "")
	{
	}
      else if (element == "")
	{
	}
      else
	{
	  // So if the line is nothing else, It must be a coordinate
	  // Parse the line if possible
	  stringstream(input_line) >> segment >> dot >> lane >> dot >> waypoint >> latitude >> longitude;
	  cout  << longitude <<"," << latitude << ",0.0" << endl;
	}

      // Yes I know we don't need this test, but if we don't do a clean exit 
      // above, we can catch it here.

    }while (element != "end_file");

  return -1;
}
