RNDF_name	St_Lukes_RNDF_Full_Map
    
	
num_segments	13	


num_zones	3
	
format_version	1.0
	
creation_date	11/27/2006


segment	1	
num_lanes	2	
segment_name	Fast_Lane	
lane	1.1	
num_waypoints	8	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
stop	1.1.2
exit	1.1.2	7.2.1
exit	1.1.4	8.2.1

exit	1.1.6	11.2.1
exit	1.1.8	10.2.3
exit	1.1.8	10.1.4


1.1.1	34.167452	-118.095284
1.1.2	34.167442	-118.095699
1.1.3	34.167441	-118.095927
1.1.4	34.167437	-118.096022
1.1.5	34.167435	-118.096162
1.1.6	34.167421	-118.096882
1.1.7	34.167420	-118.097017
1.1.8	34.167418	-118.097081
end_lane		
lane	1.2	
num_waypoints	8	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white
checkpoint	1.2.5	12
exit	1.2.2	11.2.1
exit	1.2.4	8.2.1
exit	1.2.6	7.2.1
exit	1.2.8	12.2.1
1.2.1	34.167381	-118.097077
1.2.2	34.167381	-118.097019
1.2.3	34.167383	-118.096885
1.2.4	34.167398	-118.096158
1.2.5	34.167400	-118.096019
1.2.6	34.167403	-118.095926
1.2.7	34.167405	-118.095702
1.2.8	34.167415	-118.095279
end_lane		
end_segment		

segment	2	
num_lanes	2	
segment_name	Waypoint_Road	

lane	2.1	
num_waypoints	10	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
stop	2.1.10	
exit	2.1.10	11.1.3
exit	2.1.10	11.2.3
2.1.1	34.167553	-118.096183
2.1.2	34.167550	-118.096241
2.1.3	34.167549	-118.096324
2.1.4	34.167549	-118.096413
2.1.5	34.167545	-118.096507
2.1.6	34.167544	-118.096599
2.1.7	34.167542	-118.096693
2.1.8	34.167539	-118.096763
2.1.9	34.167537	-118.096821
2.1.10	34.167537	-118.096870
end_lane		
lane	2.2	
num_waypoints	10	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
checkpoint	2.2.2	4
checkpoint	2.2.3	5
checkpoint	2.2.4	6
checkpoint	2.2.5	7
checkpoint	2.2.6	8
checkpoint	2.2.7	9
checkpoint	2.2.8	10
checkpoint	2.2.9	11
stop	2.2.10	
exit	2.2.10	8.1.11
exit	2.2.10	8.2.3
2.2.1	34.167503	-118.096871
2.2.2	34.167505	-118.096824
2.2.3	34.167509	-118.096762
2.2.4	34.167509	-118.096693
2.2.5	34.167509	-118.096597
2.2.6	34.167513	-118.096501
2.2.7	34.167516	-118.096411
2.2.8	34.167518	-118.096324
2.2.9	34.167520	-118.096238
2.2.10	34.167523	-118.096183
end_lane		
end_segment		

segment	3	
num_lanes	2	
segment_name	Murray_Road	
lane	3.1	
num_waypoints	4	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
checkpoint	3.1.2	3
exit	3.1.2	11.1.1
exit	3.1.4	3.2.1
3.1.1	34.167735	-118.096182
3.1.2	34.167723	-118.096882
3.1.3	34.167724	-118.097020
3.1.4	34.167724	-118.097081
end_lane
lane	3.2	
num_waypoints	4	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
stop	3.2.4	
exit	3.2.2	11.1.1
exit	3.2.4	8.1.9
exit	3.2.4	8.2.5
3.2.1	34.167688	-118.097086
3.2.2	34.167688	-118.097023
3.2.3	34.167694	-118.096882
3.2.4	34.167702	-118.096180
end_lane		
end_segment		


segment	4	
num_lanes	2	
segment_name	Blockade_Blvd	
lane	4.1	
num_waypoints	2	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
checkpoint	4.1.2	2
exit	4.1.2	4.2.1
4.1.1	34.167877	-118.096167
4.1.2	34.167876	-118.097079
end_lane		
lane	4.2	
num_waypoints	2	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
stop	4.2.2
exit	4.2.2	8.1.7
exit	4.2.2	8.2.7
4.2.1	34.167847	-118.097079
4.2.2	34.167847	-118.096167
end_lane		
end_segment		
segment	5	
num_lanes	2	
segment_name	Parke_Lane	
lane	5.1	
num_waypoints	2	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
exit	5.1.2	16.0.1
5.1.1	34.167975	-118.096106
5.1.2	34.168158	-118.096662
end_lane		
lane	5.2	
num_waypoints	2	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
checkpoint	5.2.1	19
exit	5.2.2	8.1.4
exit	5.2.2	8.2.9
5.2.1	34.168143	-118.096673
5.2.2	34.167960	-118.096119
end_lane		
end_segment		
segment	6	
num_lanes	2	
segment_name	Short_Street	
lane	6.1	
num_waypoints	2	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
stop	6.1.2	
exit	6.1.2	11.1.4
exit	6.1.2	11.2.3
6.1.1	34.167555	-118.095797
6.1.2	34.167552	-118.096028
end_lane		
lane	6.2	
num_waypoints	2	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
exit	6.2.2	7.1.5
exit	6.2.2	7.2.3
6.2.1	34.167519	-118.096030
6.2.2	34.167522	-118.095850
end_lane		
end_segment		
segment	7	
num_lanes	2	
segment_name	Diagon_Alley	
lane	7.1	
num_waypoints	6	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
stop	7.1.6	
exit	7.1.2	14.0.5
exit	7.1.4	6.1.1
exit	7.1.6	1.1.3
7.1.1	34.167596	-118.095419
7.1.2	34.167597	-118.095499
7.1.3	34.167589	-118.095625
7.1.4	34.167560	-118.095713
7.1.5	34.167498	-118.095818
7.1.6	34.167481	-118.095843
end_lane		
lane	7.2	
num_waypoints	6	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
checkpoint	7.2.4	13
exit	7.2.2	6.1.1
exit	7.2.6	12.1.8
exit	7.2.6	12.2.6
7.2.1	34.167483	-118.095747
7.2.2	34.167497	-118.095725
7.2.3	34.167540	-118.095656
7.2.4	34.167558	-118.095582
7.2.5	34.167562	-118.095503
7.2.6	34.167565	-118.095392
end_lane		
end_segment		
segment	8	
num_lanes	2	
segment_name	St_Luke_Lane	
lane	8.1	
num_waypoints	12	
lane_width	12	
right_boundary	solid_white	
checkpoint	8.1.9	18
stop	8.1.10	
stop	8.1.12	
exit	8.1.2	9.2.1
exit	8.1.4	5.1.1
exit	8.1.6	4.1.1
exit	8.1.8	3.1.1
exit	8.1.10	2.1.1
exit	8.1.10	6.2.1
exit	8.1.12	1.1.5
exit	8.1.12	1.2.5
8.1.1	34.168000	-118.095329
8.1.2	34.167996	-118.095642
8.1.3	34.167996	-118.095742
8.1.4	34.167979	-118.096027
8.1.5	34.167924	-118.096100
8.1.6	34.167896	-118.096115
8.1.7	34.167818	-118.096135
8.1.8	34.167766	-118.096135
8.1.9	34.167672	-118.096134
8.1.10	34.167579	-118.096131
8.1.11	34.167498	-118.096126
8.1.12	34.167479	-118.096126
end_lane		
lane	8.2	
num_waypoints	12	
lane_width	12	
right_boundary	solid_white	
stop	8.2.2
exit	8.2.2	2.1.1
exit	8.2.2	6.2.1
exit	8.2.4	3.1.1
exit	8.2.6	4.1.1
exit	8.2.8	5.1.1
exit	8.2.10	9.2.1
exit	8.2.12	12.1.1
exit	8.2.12	13.2.1
8.2.1	34.167480	-118.096069
8.2.2	34.167497	-118.096069
8.2.3	34.167581	-118.096072
8.2.4	34.167671	-118.096073
8.2.5	34.167765	-118.096077
8.2.6	34.167822	-118.096078
8.2.7	34.167892	-118.096064
8.2.8	34.167920	-118.096041
8.2.9	34.167962	-118.095956
8.2.10	34.167976	-118.095753
8.2.11	34.167977	-118.095634
8.2.12	34.167981	-118.095346
end_lane		
end_segment		
segment	9	
num_lanes	2	
segment_name	Hook_Road	
lane	9.1	
num_waypoints	8	
lane_width	12	
right_boundary	solid_white	
stop	9.1.8	
exit	9.1.4	15.0.4
exit	9.1.8	8.1.3
exit	9.1.8	8.2.11
9.1.1	34.168355	-118.095216
9.1.2	34.168357	-118.095329
9.1.3	34.168352	-118.095601
9.1.4	34.168339	-118.095643
9.1.5	34.168318	-118.095679
9.1.6	34.168283	-118.095701
9.1.7	34.168262	-118.095709
9.1.8	34.168029	-118.095705
end_lane		
lane	9.2	
num_waypoints	5	
lane_width	12	
right_boundary	solid_white	
checkpoint	9.2.2	17
checkpoint	9.2.5	16
exit	9.2.4	13.1.1
exit	9.2.5	9.1.1
9.2.1	34.168029	-118.095678
9.2.2	34.168274	-118.095685
9.2.3	34.168327	-118.095633
9.2.4	34.168341	-118.095328
9.2.5	34.168342	-118.095218
end_lane		
end_segment		
segment	10	
num_lanes	2	
segment_name	Del_Rey_Avenue	
lane	10.1	
num_waypoints	6	
lane_width	15	
checkpoint	10.1.1	1
stop	10.1.6	
exit	10.1.2	3.2.1
exit	10.1.4	1.2.1
exit	10.1.6	10.2.1
10.1.1	34.169025	-118.097165
10.1.2	34.167761	-118.097176
10.1.3	34.167639	-118.097175
10.1.4	34.167465	-118.097180
10.1.5	34.167338	-118.097181
10.1.6	34.166804	-118.097187
end_lane		
lane	10.2	
num_waypoints	6	
lane_width	15	
stop	10.2.6	
exit	10.2.2	1.2.1
exit	10.2.4	3.2.1
exit	10.2.6	10.1.1
10.2.1	34.166803	-118.097146
10.2.2	34.167336	-118.097139
10.2.3	34.167465	-118.097136
10.2.4	34.167648	-118.097134
10.2.5	34.167757	-118.097133
10.2.6	34.169039	-118.097109
end_lane		
end_segment		
segment	11	
num_lanes	2	
segment_name	North_Street	
lane	11.1	
num_waypoints	4	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
stop	11.1.4	
exit	11.1.2	2.2.1
exit	11.1.4	1.1.7
exit	11.1.4	1.2.3
11.1.1	34.167657	-118.096984
11.1.2	34.167569	-118.096983
11.1.3	34.167481	-118.096983
11.1.4	34.167453	-118.096983
end_lane		
lane	11.2	
num_waypoints	4	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
exit	11.2.2	2.2.1
exit	11.2.4	3.1.3
exit	11.2.4	3.2.3
11.2.1	34.167453	-118.096928
11.2.2	34.167480	-118.096925
11.2.3	34.167568	-118.096929
11.2.4	34.167658	-118.096927
end_lane		
end_segment		
segment	12	
num_lanes	2	
segment_name	Winding_Way	
lane	12.1	
num_waypoints	11	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
checkpoint	12.1.3	15
exit	12.1.6	7.1.1
exit	12.1.10	1.1.1
12.1.1	34.167949	-118.095312
12.1.2	34.167883	-118.095312
12.1.3	34.167822	-118.095324
12.1.4	34.167769	-118.095351
12.1.5	34.167692	-118.095374
12.1.6	34.167634	-118.095375
12.1.7	34.167584	-118.095342
12.1.8	34.167553	-118.095303
12.1.9	34.167521	-118.095263
12.1.10	34.167488	-118.095226
12.1.11	34.167354	-118.095204
end_lane		
lane	12.2	
num_waypoints	11	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
checkpoint	12.2.7	14
exit	12.2.4	7.1.1
exit	12.2.11	8.1.1
exit	12.2.11	13.2.1
12.2.1	34.167353	-118.095156
12.2.2	34.167486	-118.095175
12.2.3	34.167520	-118.095204
12.2.4	34.167558	-118.095251
12.2.5	34.167593	-118.095292
12.2.6	34.167636	-118.095325
12.2.7	34.167692	-118.095332
12.2.8	34.167769	-118.095304
12.2.9	34.167816	-118.095287
12.2.10	34.167882	-118.095279
12.2.11	34.167948	-118.095280
end_lane		
end_segment		
segment	13	
num_lanes	2	
segment_name	Telescope_Road	
lane	13.1	
num_waypoints	2	
lane_width	12	
right_boundary	solid_white	
exit	13.1.2	8.1.1
exit	13.1.2	12.1.1
13.1.1	34.168310	-118.095290
13.1.2	34.168031	-118.095300
end_lane		
lane	13.2	
num_waypoints	2	
lane_width	12	
right_boundary	solid_white	
stop	13.2.2
exit	13.2.2	9.1.2
exit	13.2.2	9.2.5
13.2.1	34.168032	-118.095267
13.2.2	34.168312	-118.095259
end_lane		
end_segment		
zone	14	
num_spots	3		
zone_name	Shuttle_Lot		
perimeter	14.0		
num_perimeterpoints	8		
exit	14.0.3	12.2.8	
14.0.1	34.167812	-118.095487
14.0.2	34.167749	-118.095525
14.0.3	34.167726	-118.095575
14.0.4	34.167625	-118.095590
14.0.5	34.167629	-118.095497
14.0.6	34.167629	-118.095416
14.0.7	34.167741	-118.095394
14.0.8	34.167799	-118.095357
end_perimeter		
spot	14.1	
spot_width	12	
checkpoint	14.1.2	20
14.1.1	34.167681	-118.095535
14.1.2	34.167714	-118.095535
end_spot		
spot	14.2	
spot_width	12	
checkpoint	14.2.2	21
14.2.1	34.167682	-118.095495
14.2.2	34.167714	-118.095495
end_spot		
spot	14.3	
spot_width	12	
checkpoint	14.3.2	22
14.3.1	34.167681	-118.095456
14.3.2	34.167713	-118.095456
end_spot		
end_zone
zone	15	
num_spots	3		
zone_name	Executive_Lot		
perimeter	15.0		
num_perimeterpoints	9		
exit	15.0.6	9.1.7
15.0.1	34.168444	-118.095761
15.0.2	34.168425	-118.095840
15.0.3	34.168361	-118.095864
15.0.4	34.168273	-118.095734
15.0.5	34.168315	-118.095715
15.0.6	34.168341	-118.095688
15.0.7	34.168364	-118.095652
15.0.8	34.168373	-118.095616
15.0.9	34.168435	-118.095691
end_perimeter		
spot	15.1	
spot_width	12	
checkpoint	15.1.2	23
15.1.1	34.168331	-118.095755
15.1.2	34.168359	-118.095801
end_spot		
spot	15.2	
spot_width	12	
checkpoint	15.2.2	24
15.2.1	34.168361	-118.095731
15.2.2	34.168387	-118.095775
end_spot		
spot	15.3	
spot_width	12	
checkpoint	15.3.2	25
15.3.1	34.168411	-118.095745
15.3.2	34.168384	-118.095703
end_spot		
end_zone
zone	16	
num_spots	10		
zone_name	Offsite_Parking		
perimeter	16.0		
num_perimeterpoints	8		
exit	16.0.5	5.2.1
16.0.1	34.168428	-118.096797
16.0.2	34.168424	-118.097020
16.0.3	34.168145	-118.097024
16.0.4	34.168142	-118.096705
16.0.5	34.168158	-118.096687
16.0.6	34.168170	-118.096678
16.0.7	34.168180	-118.096671
16.0.8	34.168363	-118.096694
end_perimeter		
spot	16.1	
spot_width	16	
checkpoint	16.1.2	27
16.1.1	34.168357	-118.096952
16.1.2	34.168328	-118.096951
end_spot		
spot	16.2	
spot_width	16	
checkpoint	16.2.2	28
16.2.1	34.168356	-118.096913
16.2.2	34.168326	-118.096911
end_spot		
spot	16.3	
spot_width	16	
checkpoint	16.3.2	29
16.3.1	34.168353	-118.096874
16.3.2	34.168327	-118.096872
end_spot		
spot	16.4	
spot_width	16	
checkpoint	16.4.2	30
16.4.1	34.168327	-118.096840
16.4.2	34.168356	-118.096840
end_spot		
spot	16.5	
spot_width	16	
checkpoint	16.5.2	31
16.5.1	34.168327	-118.096807
16.5.2	34.168353	-118.096807
end_spot		
spot	16.6	
spot_width	16	
checkpoint	16.6.2	32
16.6.1	34.168260	-118.096949
16.6.2	34.168222	-118.096949
end_spot		
spot	16.7	
spot_width	16	
checkpoint	16.7.2	33
16.7.1	34.168259	-118.096911
16.7.2	34.168223	-118.096911
end_spot		
spot	16.8	
spot_width	16	
checkpoint	16.8.2	34
16.8.1	34.168256	-118.096871
16.8.2	34.168224	-118.096871
end_spot		
spot	16.9	
spot_width	16	
checkpoint	16.9.2	35
16.9.1	34.168258	-118.096834
16.9.2	34.168222	-118.096834
end_spot		
spot	16.10	
spot_width	16	
checkpoint	16.10.2	36
16.10.1	34.168255	-118.096803
16.10.2	34.168223	-118.096803
end_spot		
end_zone		
end_file		
