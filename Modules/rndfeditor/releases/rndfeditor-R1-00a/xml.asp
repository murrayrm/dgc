<?xml version="1.0" encoding="iso-8859-1" ?>
<charactersheet>
<characters>
<character name="Macil Eleni" characterID="246666793"></character>
<character name="Tsari Ninque" characterID="266209492"></character>
<character timeInCache="6281" timeLeftInCache="21593719" name="Niun S'Intel" characterID="740693657"><race>Caldari</race>
<bloodLine>Deteis</bloodLine>
<gender>Male</gender>
<corporationName>Keepers of the Holy Bagel</corporationName>
<balance>201022615.3377</balance><attributeEnhancers timeInCache="6266" timeLeftInCache="86393734"></attributeEnhancers><attributes>
<intelligence>7</intelligence>
<charisma>6</charisma>
<perception>8</perception>
<memory>9</memory>
<willpower>9</willpower>
</attributes>
<skills>
<skillGroup groupName="Corporation Management" groupID="266">
<skill typeName="Anchoring" typeID="11584">
  <groupID>266</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>4243</skillpoints>
  <level>2</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Drones" groupID="273">
<skill typeName="Combat Drone Operation" typeID="24241">
  <groupID>273</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2846</skillpoints>
  <level>2</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Drone Durability" typeID="23618">
  <groupID>273</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>1250</skillpoints>
  <level>1</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Drone Interfacing" typeID="3442">
  <groupID>273</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>40044</skillpoints>
  <level>3</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Drone Sharpshooting" typeID="23606">
  <groupID>273</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>250</skillpoints>
  <level>1</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Drones" typeID="3436">
  <groupID>273</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Electronic Warfare Drone Interfacing" typeID="23566">
  <groupID>273</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>1250</skillpoints>
  <level>1</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Heavy Drone Operation" typeID="3441">
  <groupID>273</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>1250</skillpoints>
  <level>1</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Mining Drone Operation" typeID="3438">
  <groupID>273</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>500</skillpoints>
  <level>1</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Repair Drone Operation" typeID="3439">
  <groupID>273</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>2</skillpoints>
  <level>0</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Scout Drone Operation" typeID="3437">
  <groupID>273</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>8000</skillpoints>
  <level>3</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Electronics" groupID="272">
<skill typeName="Cloaking" typeID="11579">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>6</rank>
  <skillpoints>271530</skillpoints>
  <level>4</level>
  <skilllevel1>1500</skilllevel1>
  <skilllevel2>8486</skilllevel2>
  <skilllevel3>48000</skilllevel3>
  <skilllevel4>271530</skilllevel4>
  <skilllevel5>1536000</skilllevel5>
</skill>
<skill typeName="Electronic Warfare" typeID="3427">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>90513</skillpoints>
  <level>4</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Electronics" typeID="3426">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Electronics Upgrades" typeID="3432">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>512000</skillpoints>
  <level>5</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Frequency Modulation" typeID="19760">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>4243</skillpoints>
  <level>2</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Long Distance Jamming" typeID="19759">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>4</rank>
  <skillpoints>32000</skillpoints>
  <level>3</level>
  <skilllevel1>1000</skilllevel1>
  <skilllevel2>5657</skilllevel2>
  <skilllevel3>32000</skilllevel3>
  <skilllevel4>181020</skilllevel4>
  <skilllevel5>1024000</skilllevel5>
</skill>
<skill typeName="Long Range Targeting" typeID="3428">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2829</skillpoints>
  <level>2</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Propulsion Jamming" typeID="3435">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>24000</skillpoints>
  <level>3</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Sensor Linking" typeID="3433">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>135765</skillpoints>
  <level>4</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Signal Dispersion" typeID="19761">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>226275</skillpoints>
  <level>4</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Signal Suppression" typeID="19766">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>8658</skillpoints>
  <level>2</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Signature Analysis" typeID="3431">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>45255</skillpoints>
  <level>4</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Survey" typeID="3551">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>8000</skillpoints>
  <level>3</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Target Painting" typeID="19921">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>750</skillpoints>
  <level>1</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Targeting" typeID="3429">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>8000</skillpoints>
  <level>3</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Weapon Disruption" typeID="3434">
  <groupID>272</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>750</skillpoints>
  <level>1</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Engineering" groupID="271">
<skill typeName="EM Shield Compensation" typeID="12365">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>27</skillpoints>
  <level>0</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Energy Emission Systems" typeID="3423">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>16000</skillpoints>
  <level>3</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Energy Grid Upgrades" typeID="3424">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2829</skillpoints>
  <level>2</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Energy Management" typeID="3418">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>135765</skillpoints>
  <level>4</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Energy Pulse Weapons" typeID="3421">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>500</skillpoints>
  <level>1</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Energy Systems Operation" typeID="3417">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Engineering" typeID="3413">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Explosive Shield Compensation" typeID="12367">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2</skillpoints>
  <level>0</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Kinetic Shield Compensation" typeID="12366">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>94</skillpoints>
  <level>0</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Shield Compensation" typeID="21059">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2829</skillpoints>
  <level>2</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Shield Management" typeID="3419">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>135765</skillpoints>
  <level>4</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Shield Operation" typeID="3416">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Shield Upgrades" typeID="3425">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>90510</skillpoints>
  <level>4</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Tactical Shield Manipulation" typeID="3420">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>4</rank>
  <skillpoints>181020</skillpoints>
  <level>4</level>
  <skilllevel1>1000</skilllevel1>
  <skilllevel2>5657</skilllevel2>
  <skilllevel3>32000</skilllevel3>
  <skilllevel4>181020</skilllevel4>
  <skilllevel5>1024000</skilllevel5>
</skill>
<skill typeName="Thermic Shield Compensation" typeID="11566">
  <groupID>271</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>1</skillpoints>
  <level>0</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Gunnery" groupID="255">
<skill typeName="Advanced Weapon Upgrades" typeID="11207">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>6</rank>
  <skillpoints>48000</skillpoints>
  <level>3</level>
  <skilllevel1>1500</skilllevel1>
  <skilllevel2>8486</skilllevel2>
  <skilllevel3>48000</skilllevel3>
  <skilllevel4>271530</skilllevel4>
  <skilllevel5>1536000</skilllevel5>
</skill>
<skill typeName="Controlled Bursts" typeID="3316">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2829</skillpoints>
  <level>2</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Gunnery" typeID="3300">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Medium Hybrid Turret" typeID="3304">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>769</skillpoints>
  <level>1</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Medium Projectile Turret" typeID="3305">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>135765</skillpoints>
  <level>4</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Motion Prediction" typeID="3312">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>90510</skillpoints>
  <level>4</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Rapid Firing" typeID="3310">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>90510</skillpoints>
  <level>4</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Sharpshooter" typeID="3311">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>16000</skillpoints>
  <level>3</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Small Artillery Specialization" typeID="12201">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>4243</skillpoints>
  <level>2</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Small Autocannon Specialization" typeID="11084">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>4243</skillpoints>
  <level>2</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Small Blaster Specialization" typeID="12210">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>4243</skillpoints>
  <level>2</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Small Hybrid Turret" typeID="3301">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Small Projectile Turret" typeID="3302">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Small Railgun Specialization" typeID="11082">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>10930</skillpoints>
  <level>2</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Surgical Strike" typeID="3315">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>4</rank>
  <skillpoints>181020</skillpoints>
  <level>4</level>
  <skilllevel1>1000</skilllevel1>
  <skilllevel2>5657</skilllevel2>
  <skilllevel3>32000</skilllevel3>
  <skilllevel4>181020</skilllevel4>
  <skilllevel5>1024000</skilllevel5>
</skill>
<skill typeName="Trajectory Analysis" typeID="3317">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>40000</skillpoints>
  <level>3</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Weapon Upgrades" typeID="3318">
  <groupID>255</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>512000</skillpoints>
  <level>5</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Industry" groupID="268">
<skill typeName="Industry" typeID="3380">
  <groupID>268</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>250</skillpoints>
  <level>1</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Mining" typeID="3386">
  <groupID>268</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>1415</skillpoints>
  <level>2</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Refining" typeID="3385">
  <groupID>268</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>250</skillpoints>
  <level>1</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Leadership" groupID="258">
<skill typeName="Armored Warfare" typeID="20494">
  <groupID>258</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>4</skillpoints>
  <level>0</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Information Warfare" typeID="20495">
  <groupID>258</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>1</skillpoints>
  <level>0</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Leadership" typeID="3348">
  <groupID>258</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>250</skillpoints>
  <level>1</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Siege Warfare" typeID="3350">
  <groupID>258</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>1</skillpoints>
  <level>0</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Skirmish Warfare" typeID="3349">
  <groupID>258</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>500</skillpoints>
  <level>1</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Learning" groupID="267">
<skill typeName="Analytical Mind" typeID="3377">
  <groupID>267</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Clarity" typeID="12387">
  <groupID>267</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>135765</skillpoints>
  <level>4</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Eidetic Memory" typeID="12385">
  <groupID>267</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>135765</skillpoints>
  <level>4</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Empathy" typeID="3376">
  <groupID>267</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>250</skillpoints>
  <level>1</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Focus" typeID="12386">
  <groupID>267</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>135765</skillpoints>
  <level>4</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Instant Recall" typeID="3378">
  <groupID>267</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Iron Will" typeID="3375">
  <groupID>267</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Learning" typeID="3374">
  <groupID>267</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>45255</skillpoints>
  <level>4</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Logic" typeID="12376">
  <groupID>267</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>135765</skillpoints>
  <level>4</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Spatial Awareness" typeID="3379">
  <groupID>267</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Mechanic" groupID="269">
<skill typeName="EM Armor Compensation" typeID="22806">
  <groupID>269</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>500</skillpoints>
  <level>1</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Explosive Armor Compensation" typeID="22807">
  <groupID>269</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2829</skillpoints>
  <level>2</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Hull Upgrades" typeID="3394">
  <groupID>269</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>90510</skillpoints>
  <level>4</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Jury Rigging" typeID="26252">
  <groupID>269</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2</skillpoints>
  <level>0</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Kinetic Armor Compensation" typeID="22808">
  <groupID>269</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2829</skillpoints>
  <level>2</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Mechanic" typeID="3392">
  <groupID>269</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Remote Armor Repair Systems" typeID="16069">
  <groupID>269</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>500</skillpoints>
  <level>1</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Repair Systems" typeID="3393">
  <groupID>269</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>45255</skillpoints>
  <level>4</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Salvaging" typeID="25863">
  <groupID>269</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>4243</skillpoints>
  <level>2</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Thermic Armor Compensation" typeID="22809">
  <groupID>269</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2829</skillpoints>
  <level>2</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Missile Launcher Operation" groupID="256">
<skill typeName="Cruise Missile Specialization" typeID="20212">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>8</rank>
  <skillpoints>2000</skillpoints>
  <level>1</level>
  <skilllevel1>2000</skilllevel1>
  <skilllevel2>11314</skilllevel2>
  <skilllevel3>64000</skilllevel3>
  <skilllevel4>362039</skilllevel4>
  <skilllevel5>2048000</skilllevel5>
</skill>
<skill typeName="Cruise Missiles" typeID="3326">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>1280000</skillpoints>
  <level>5</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="FoF Missiles" typeID="3322">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>750</skillpoints>
  <level>1</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Guided Missile Precision" typeID="20312">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>226275</skillpoints>
  <level>4</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Heavy Assault Missiles" typeID="25719">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>750</skillpoints>
  <level>1</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Heavy Missiles" typeID="3324">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>24246</skillpoints>
  <level>3</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Missile Bombardment" typeID="12441">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>2829</skillpoints>
  <level>2</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Missile Launcher Operation" typeID="3319">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>256000</skillpoints>
  <level>5</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Missile Projection" typeID="12442">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>4</rank>
  <skillpoints>181020</skillpoints>
  <level>4</level>
  <skilllevel1>1000</skilllevel1>
  <skilllevel2>5657</skilllevel2>
  <skilllevel3>32000</skilllevel3>
  <skilllevel4>181020</skilllevel4>
  <skilllevel5>1024000</skilllevel5>
</skill>
<skill typeName="Rapid Launch" typeID="21071">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>90510</skillpoints>
  <level>4</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Rockets" typeID="3320">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>1415</skillpoints>
  <level>2</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Standard Missiles" typeID="3321">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>16000</skillpoints>
  <level>3</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Target Navigation Prediction" typeID="20314">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>90510</skillpoints>
  <level>4</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Torpedoes" typeID="3325">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>4</rank>
  <skillpoints>32000</skillpoints>
  <level>3</level>
  <skilllevel1>1000</skilllevel1>
  <skilllevel2>5657</skilllevel2>
  <skilllevel3>32000</skilllevel3>
  <skilllevel4>181020</skilllevel4>
  <skilllevel5>1024000</skilllevel5>
</skill>
<skill typeName="Warhead Upgrades" typeID="20315">
  <groupID>256</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>40000</skillpoints>
  <level>3</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Navigation" groupID="275">
<skill typeName="Acceleration Control" typeID="3452">
  <groupID>275</groupID>
  <flag>7</flag>
  <rank>4</rank>
  <skillpoints>32000</skillpoints>
  <level>3</level>
  <skilllevel1>1000</skilllevel1>
  <skilllevel2>5657</skilllevel2>
  <skilllevel3>32000</skilllevel3>
  <skilllevel4>181020</skilllevel4>
  <skilllevel5>1024000</skilllevel5>
</skill>
<skill typeName="Afterburner" typeID="3450">
  <groupID>275</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>45255</skillpoints>
  <level>4</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Evasive Maneuvering" typeID="3453">
  <groupID>275</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>512000</skillpoints>
  <level>5</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Fuel Conservation" typeID="3451">
  <groupID>275</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>500</skillpoints>
  <level>1</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="High Speed Maneuvering" typeID="3454">
  <groupID>275</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>7072</skillpoints>
  <level>2</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Navigation" typeID="3449">
  <groupID>275</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>45255</skillpoints>
  <level>4</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Warp Drive Operation" typeID="3455">
  <groupID>275</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>45255</skillpoints>
  <level>4</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Science" groupID="270">
<skill typeName="Astrometric Triangulation" typeID="25811">
  <groupID>270</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>2</skillpoints>
  <level>0</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Astrometrics" typeID="3412">
  <groupID>270</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>24000</skillpoints>
  <level>3</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Cybernetics" typeID="3411">
  <groupID>270</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>4243</skillpoints>
  <level>2</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Infomorph Psychology" typeID="24242">
  <groupID>270</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>8000</skillpoints>
  <level>3</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Science" typeID="3402">
  <groupID>270</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>45255</skillpoints>
  <level>4</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>
<skill typeName="Signal Acquisition" typeID="25739">
  <groupID>270</groupID>
  <flag>7</flag>
  <rank>8</rank>
  <skillpoints>8</skillpoints>
  <level>0</level>
  <skilllevel1>2000</skilllevel1>
  <skilllevel2>11314</skilllevel2>
  <skilllevel3>64000</skilllevel3>
  <skilllevel4>362039</skilllevel4>
  <skilllevel5>2048000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Social" groupID="278">
<skill typeName="Connections" typeID="3359">
  <groupID>278</groupID>
  <flag>7</flag>
  <rank>3</rank>
  <skillpoints>4243</skillpoints>
  <level>2</level>
  <skilllevel1>750</skilllevel1>
  <skilllevel2>4243</skilllevel2>
  <skilllevel3>24000</skilllevel3>
  <skilllevel4>135765</skilllevel4>
  <skilllevel5>768000</skilllevel5>
</skill>
<skill typeName="Social" typeID="3355">
  <groupID>278</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>8000</skillpoints>
  <level>3</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Spaceship Command" groupID="257">
<skill typeName="Assault Ships" typeID="12095">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>4</rank>
  <skillpoints>32000</skillpoints>
  <level>3</level>
  <skilllevel1>1000</skilllevel1>
  <skilllevel2>5657</skilllevel2>
  <skilllevel3>32000</skilllevel3>
  <skilllevel4>181020</skilllevel4>
  <skilllevel5>1024000</skilllevel5>
</skill>
<skill typeName="Battlecruisers" typeID="12099">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>6</rank>
  <skillpoints>271530</skillpoints>
  <level>4</level>
  <skilllevel1>1500</skilllevel1>
  <skilllevel2>8486</skilllevel2>
  <skilllevel3>48000</skilllevel3>
  <skilllevel4>271530</skilllevel4>
  <skilllevel5>1536000</skilllevel5>
</skill>
<skill typeName="Caldari Battleship" typeID="3338">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>8</rank>
  <skillpoints>64000</skillpoints>
  <level>3</level>
  <skilllevel1>2000</skilllevel1>
  <skilllevel2>11314</skilllevel2>
  <skilllevel3>64000</skilllevel3>
  <skilllevel4>362039</skilllevel4>
  <skilllevel5>2048000</skilllevel5>
</skill>
<skill typeName="Caldari Cruiser" typeID="3334">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>226275</skillpoints>
  <level>4</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Caldari Frigate" typeID="3330">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>512000</skillpoints>
  <level>5</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Caldari Industrial" typeID="3342">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>4</rank>
  <skillpoints>32000</skillpoints>
  <level>3</level>
  <skilllevel1>1000</skilllevel1>
  <skilllevel2>5657</skilllevel2>
  <skilllevel3>32000</skilllevel3>
  <skilllevel4>181020</skilllevel4>
  <skilllevel5>1024000</skilllevel5>
</skill>
<skill typeName="Covert Ops" typeID="12093">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>4</rank>
  <skillpoints>324533</skillpoints>
  <level>4</level>
  <skilllevel1>1000</skilllevel1>
  <skilllevel2>5657</skilllevel2>
  <skilllevel3>32000</skilllevel3>
  <skilllevel4>181020</skilllevel4>
  <skilllevel5>1024000</skilllevel5>
</skill>
<skill typeName="Destroyers" typeID="12097">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>2</rank>
  <skillpoints>16000</skillpoints>
  <level>3</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Interceptors" typeID="12092">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>4</rank>
  <skillpoints>5657</skillpoints>
  <level>2</level>
  <skilllevel1>1000</skilllevel1>
  <skilllevel2>5657</skilllevel2>
  <skilllevel3>32000</skilllevel3>
  <skilllevel4>181020</skilllevel4>
  <skilllevel5>1024000</skilllevel5>
</skill>
<skill typeName="Minmatar Cruiser" typeID="3333">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>5</rank>
  <skillpoints>40000</skillpoints>
  <level>3</level>
  <skilllevel1>1250</skilllevel1>
  <skilllevel2>7072</skilllevel2>
  <skilllevel3>40000</skilllevel3>
  <skilllevel4>226275</skilllevel4>
  <skilllevel5>1280000</skilllevel5>
</skill>
<skill typeName="Minmatar Frigate" typeID="3329">
  <groupID>257</groupID>
  <flag>61</flag>
  <rank>2</rank>
  <skillpoints>201716</skillpoints>
  <level>4</level>
  <skilllevel1>500</skilllevel1>
  <skilllevel2>2829</skilllevel2>
  <skilllevel3>16000</skilllevel3>
  <skilllevel4>90510</skilllevel4>
  <skilllevel5>512000</skilllevel5>
</skill>
<skill typeName="Spaceship Command" typeID="3327">
  <groupID>257</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>45255</skillpoints>
  <level>4</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>

</skillGroup>
<skillGroup groupName="Trade" groupID="274">
<skill typeName="Trade" typeID="3443">
  <groupID>274</groupID>
  <flag>7</flag>
  <rank>1</rank>
  <skillpoints>250</skillpoints>
  <level>1</level>
  <skilllevel1>250</skilllevel1>
  <skilllevel2>1415</skilllevel2>
  <skilllevel3>8000</skilllevel3>
  <skilllevel4>45255</skilllevel4>
  <skilllevel5>256000</skilllevel5>
</skill>

</skillGroup>
</skills>
</character>
</characters>
</charactersheet>