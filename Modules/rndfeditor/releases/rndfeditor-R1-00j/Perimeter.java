
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Vector;
import java.io.*;

/**
 * Used to represent and parse a zone perimeter.  A perimeter consists of multiple
 * points that outline the zone.
 * @author David Waylonis
 */
public class Perimeter {

    int[] perimeter_id;
    int num_perimeterpoints;
    Vector<WayPoint> points;
    Vector<int[][]> exits;
    int EXIT = 0;
    int ENTRANCE = 1;

    public Vector<int[][]> getExits() {
	return exits;
    }

    // update all the underlying data to reflect a change to an overlying
    // zone (occurs when we insert a new segment )
    void changeZoneId( int newId ) {
	perimeter_id[0] = newId;
	for( WayPoint w: points ) {
	    int[] id = w.getId();
	    id[0] = newId;
	    w.setId( id );
	}
	for( int[][] exit: exits ) {
	    exit[EXIT][0] = newId ;
	}
    }
  
	
    /**
     * Default constructor.  Takes in a perimeter ID by which the perimeter can
     * be identified.
     * 
     * @param id the perimeter ID
     */
    public Perimeter(int[] id) {
		
	perimeter_id = id;
    }

    /** In the event that a segment or zone is deleted, 
     * this function will shift the exit/entrance pairs on the perimeter
     */
    public void shiftBackExitEntrance(int seg) {

	System.out.println("Shifting back exit/entry for " + seg);

	for( int[][] pair: exits ) {
	    System.out.println("Old entrance/exit: " + pair[EXIT][0] + "." + pair[EXIT][1] + " to " 
			       + pair[ENTRANCE][0] + "." + pair[ENTRANCE][1]);
	    if (pair[ENTRANCE][0] > seg) {
		pair[ENTRANCE][0]--;
	    }
	    if( pair[EXIT][0] > seg) {
		pair[EXIT][0]--;
	    }
	    System.out.println("New entrance/exit: " + pair[EXIT][0] + "." + pair[EXIT][1] + " to " 
			       + pair[ENTRANCE][0] + "." + pair[ENTRANCE][1] +"\n");
	}
    }

    public void removeExitsToSeg(int seg) {
	System.out.println("Removing exit/entry for " + seg);

	int exitsSize = exits.size();
	for (int i =0; i< exitsSize; i++) {
	    int[][] pair = exits.elementAt(i);
	    if (pair[ENTRANCE][0] == seg) {
		System.out.println("Removing: " + pair[EXIT][0] + "." + pair[EXIT][1] + " to " 
				   + pair[ENTRANCE][0] + "." + pair[ENTRANCE][1] +"\n");
		exits.remove(pair);
		i++;
	    }
	}
    }	

    /**
     * Read in and parse a perimeter from an RNDF file.  The perimeter ID should have
     * already been read.
     * 
     * @param br the buffered reader for the RNDF file
     * @throws IOException if we cannot read the file
     * @throws MapFormatException if the RNDF is improperly formed
     */
    public void readPerimeter(BufferedReader br) throws IOException, MapFormatException {
		
	System.out.println("Zone perimeter");
	String line = br.readLine();
	System.out.println(line);
	String[] tokens = line.split("\\s");
	/*assert(tokens[0].equals("segment"));
	  System.out.println("token0 is "+ tokens[0]);
	  id = Integer.parseInt(tokens[1]);
	  System.out.println("segment ID is " + id); */
	points = new Vector<WayPoint>();
	exits = new Vector<int[][]>();
	while(!line.startsWith("end_perimeter")) {
			  
	    System.out.println(line);
	    tokens = line.split("\\s");
	    if(line.startsWith("num_perimeterpoints")) {
		System.out.println("numperimetertoken: " + tokens[1]);
		num_perimeterpoints = Integer.parseInt(tokens[1]);
	    }
	    else if(tokens[0].equals("exit")) {
		if (tokens.length < 3) {
		    throw new MapFormatException("Incorrectly formatted \"exit\" line!");
		}
		// read in the entrance exit pair
		int[][] temp = new int[2][3];

		// read in the exit
		String str = tokens[1];
		//Replace '.' with '&' because of funky .split operation
		str = str.replace('.', '&');
		String[] wayId = str.split("&");
		temp[EXIT][0] = Integer.parseInt(wayId[0]);
		temp[EXIT][1] = Integer.parseInt(wayId[1]);
		temp[EXIT][2] = Integer.parseInt(wayId[2]);

		// read in the entrance
		str = tokens[2];
		//Replace '.' with '&' because of funky .split operation
		str = str.replace('.', '&');
		wayId = str.split("&");
		temp[ENTRANCE][0] = Integer.parseInt(wayId[0]);
		temp[ENTRANCE][1] = Integer.parseInt(wayId[1]);
		temp[ENTRANCE][2] = Integer.parseInt(wayId[2]);
		// add this exit entrance pair to the vector
		exits.add(temp);
	    }
	    else if(!tokens[0].startsWith("/")) {
		try {
		    points.add(new WayPoint(tokens[0], Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2])));
		}
		catch(Exception e) {}
	    }
	    line = br.readLine();
	}
	System.out.println("number of perimeter points is " + num_perimeterpoints);
	System.out.println(line);
    }
        
    public Vector<WayPoint> getPoints(){
	return points;
    }
        
    public void printPerimeter(PrintWriter file){
	file.println("perimeter\t" + perimeter_id[0] + "." + perimeter_id[1]);
	file.println("num_perimeterpoints\t" + num_perimeterpoints);
	for(int[][] i: exits){
	    file.println("exit\t" + i[EXIT][0] + "." + i[EXIT][1] + "." + i[EXIT][2] + "\t" 
			 + i[ENTRANCE][0] + "." + i[ENTRANCE][1] + "." + i[ENTRANCE][2]);
	}
	for(WayPoint w: points){
	    w.printWayPoint(file);
	}
	file.println("end_perimeter");

    }
}
