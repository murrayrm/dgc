import java.io.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * Used to represent and parse a parking spot.  A parking spot consists of a
 * center waypoint and an entry waypoint (to enter the spot).
 * @author David Waylonis
 */
public class Spot {

	int[] id;
	int width; // width must be an integer
	private WayPoint inPoint;
	private WayPoint checkPoint; // appears to refer to the second waypoint, rather than
        // the check point mentioned in the RNDF definition document
	
  void changeZoneId( int newId ) {
    id[0] = newId ;
    
    int[] x = inPoint.getId();
    x[0] = newId;
    inPoint.setId(x);

    int[] y = checkPoint.getId();
    y[0] = newId;
    checkPoint.setId(y);
  }

	/**
	 * Default constructor.  Takes in an ID to represent the spot.
	 * 
	 * @param id the spot ID
	 */
	public Spot(int[] id) {
		
		this.id = id;
	}
	
	/**
	 * Constructor with a string to represent the identity of the spot.
	 * 
	 * @param ident the string
	 */
	public Spot(String ident) {
		
		id = WayPoint.toIntRep(ident);
	}
	
	/**
	 * Read in and parse the spot representation.  Set the two waypoint values
	 * used to represent the spot to the corresponding waypoints.
	 * 
	 * @param br the buffered reader for the RNDF file
	 * @throws IOException if we are unable to read the file
	 */
	public void readSpot(BufferedReader br) throws IOException {
		  String line = br.readLine();
		  System.out.println(line);
		  String[] tokens = line.split("\\s");
		  inPoint = null;
		  checkPoint = null;
      WayPoint pseudoCheckpoint = null;
      int cId = 0;
		  while(!line.startsWith("end_spot")) {
			  
		       System.out.println(line);
		       tokens = line.split("\\s");
		       if(line.startsWith("spot_width")) {
		    	   System.out.println("width: " + tokens[1]);
		    	   width = Integer.parseInt(tokens[1]);
		       }
		       else if(tokens[0].equals("checkpoint")) {
            pseudoCheckpoint = new WayPoint( tokens[1], 0.0, 0.0 ); 
            cId = Integer.parseInt( tokens[2] );
		       }
		       else if(!tokens[0].startsWith("/")) {
		    	   try {
		    		   if (inPoint == null) {
		    			   inPoint = new WayPoint(tokens[0], Double.parseDouble(tokens[1]),
		    					   Double.parseDouble(tokens[2]));
		    		   }
		    		   else if (checkPoint == null) {
		    			   checkPoint = new WayPoint(tokens[0], Double.parseDouble(tokens[1]),
		    					   Double.parseDouble(tokens[2]));
		    		   }
		    	   }
		    	   catch(Exception e) {}
		       }
		       line = br.readLine();
		  }

      // update one of the waypoints with the parsed checkpoint information
      int[] checkpointId = pseudoCheckpoint.getId();
      if( Arrays.equals(checkPoint.getId(), checkpointId) ) {
        checkPoint.setCheckpointId( cId );
      } else if( Arrays.equals(inPoint.getId(), checkpointId) ) { 
        inPoint.setCheckpointId( cId );
      }
      

		  System.out.println(line);
	}

        /**
         * Prints the spot in DARPA RNDF format to file
         *
         * DOES NOT HANDEL CHECKPOINTS
         *
         *@param file the PrintWriter to use to print the spot
         */
        public void printSpot(PrintWriter file){
            file.println("spot\t" + id[0] + "." + id[1]);
            if(width != 0)
                file.println("spot_width\t" + width);
           checkPoint.printCheckPoint( file );
           inPoint.printWayPoint(file);
           checkPoint.printWayPoint(file);
           file.println("end_spot");
        }
        
    public WayPoint getInPoint() {
        return inPoint;
    }

    public void setInPoint(WayPoint inPoint) {
        this.inPoint = inPoint;
    }

    public WayPoint getCheckPoint() {
        return checkPoint;
    }

    public void setCheckPoint(WayPoint checkPoint) {
        this.checkPoint = checkPoint;
    }
}
