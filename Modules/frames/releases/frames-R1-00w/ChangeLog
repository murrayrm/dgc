Mon Apr 30 11:59:47 2007	Sam Pfister (sam)

	* version R1-00w
	BUGS:  
	FILES: point2.cc(21687), point2.hh(21687), test_point2.cc(21687)
	Added functions to cut a point2arr at an index.  Added function to
	get the closest or next index at a distance along the point2arr.

Sat Apr 28 10:40:59 2007	 (ahoward)

	* version R1-00v
	BUGS:  
	FILES: ellipse.cc(21310)
	Fixed ambiguous math calls

	FILES: mat44.h(20967)
	New version of mat44

Wed Apr 25 15:20:55 2007	Sam Pfister (sam)

	* version R1-00u
	BUGS:  
	FILES: ellipse.cc(20557), ellipse.hh(20557), point2.cc(20557),
		point2.hh(20557)
	Removed MAX and MIN macros which were unused and not good form. 
	Added Laura's point2 functions used in ladar-car-perceptor as well
	as some added functions to the ellipse class.

	FILES: ellipse.cc(20642), ellipse.hh(20642)
	changed ellipse function el_border back to border.  The compile
	conflict with the name in blobladar was fixed in blobladar.

Sat Apr 14 12:11:35 2007	Andrew Howard (ahoward)

	* version R1-00t
	BUGS:  
	FILES: ellipse.cc(19518)
	Tweaked atan2 call so it builds

Fri Mar 30 20:10:18 2007	Laura Lindzey (lindzey)

	* version R1-00s
	BUGS: 
	New files: ellipse.cc ellipse.hh
	FILES: Makefile.yam(19039), point2.hh(19039)
	adding ellipse class (for use in blobladar, wasn't sure where it
	actually belonged)

Fri Mar 30  2:24:27 2007	Laura Lindzey (lindzey)

	* version R1-00r
	BUGS: 
	FILES: point2.cc(18998)
	whoops. missed a function defined twice after last merge.

Fri Mar 30  2:05:28 2007	Laura Lindzey (lindzey)

	* version R1-00q
	BUGS: 
	FILES: point2.cc(18404), point2.hh(18404)
	added point2 average function

	FILES: point2.cc(18529), point2.hh(18529)
	adding fitline function to point2arr

	FILES: point2.cc(18532), point2.hh(18532)
	added get_inside fxn

	FILES: point2.cc(18559), point2.hh(18559)
	two new functions: merge - merges two point2arrays, checking to see
	that point is external before adding it get_overlap - checks how
	many points from each arr are inside the other

	FILES: point2.cc(18977), point2.hh(18977)
	fixed get_inside this also fixed get_overlap, but I'm not sure how
	merge is working

Mon Mar 19 22:31:40 2007	 (ahoward)

	* version R1-00p
	BUGS: 
	FILES: mat44.h(18162)
	Added float matrix inverse

Sat Mar 17  0:32:10 2007	Sam Pfister (sam)

	* version R1-00o
	BUGS: 
	FILES: point2.cc(18166), point2.hh(18166)
	added split and get_bound_box functions to point2arr class

Wed Mar 14 22:52:01 2007	 (ahoward)

	* version R1-00n
	BUGS: 
	FILES: mat44.h(17694)
	Removed unsupported C const modifiers

Wed Mar 14 15:53:11 2007	Sam Pfister (sam)

	* version R1-00m
	BUGS: 
	FILES: point2.cc(17595), point2.hh(17595)
	added some 2d functionality to point2.cc/hh to support new map
	queries

Wed Mar 14 10:07:07 2007	Sam Pfister (sam)

	* version R1-00l
	BUGS: 
	FILES: point2.cc(17549), point2.hh(17549)
	Added cut_front, cut_back, intersect other geometric functions to
	point2arr.

	FILES: point2.cc(17566), point2.hh(17566)
	added intersection detection and other functionality to 2d geometry
	classes

Tue Mar 13 15:55:06 2007	Sam Pfister (sam)

	* version R1-00k
	BUGS: 
	FILES: point2.cc(17477), point2.hh(17477),
		point2_uncertain.cc(17477), point2_uncertain.hh(17477),
		test_point2.cc(17477)
	added simple line handling and geometric functionality to
	point2.cc.  Cleaned up test_point2.cc.

Mon Mar 12 22:41:55 2007	 (ahoward)

	* version R1-00j
	BUGS: 
	New files: mat44.h
	FILES: Makefile.yam(17076)
	Added minimal 4x4 matrix functions

Thu Mar  8 18:27:44 2007	Noel duToit (ndutoit)

	* version R1-00i
	BUGS: 
	FILES: point2.cc(16809)
	Updated point2.cc to include insert function.  Other small updates
	to work with new version of tplanner.

Wed Mar  7 15:26:49 2007	Sam Pfister (sam)

	* version R1-00h
	BUGS: 
	FILES: point2.cc(16741), point2.hh(16741),
		point2_uncertain.cc(16741), point2_uncertain.hh(16741),
		test_point2.cc(16741)
	added insert function to point2arr and point2arr_uncertain.

Sun Mar  4 17:08:25 2007	Sam Pfister (sam)

	* version R1-00g
	BUGS: 
	FILES: point2.cc(16515), point2.hh(16515), test_point2.cc(16515)
	Added == and != operators to point2 and point2arr.  Both work
	transparently with point2_uncertain and point2arr_uncertain.

Fri Mar  2 18:14:10 2007	Nok Wongpiromsarn (nok)

	* version R1-00f
	BUGS: 
	FILES: point2.cc(16179), point2_uncertain.cc(16179)
	Fixed the include statement in point2.cc and point2_uncertain.cc

Thu Mar  1 19:33:14 2007	Sam Pfister (sam)

	* version R1-00e
	BUGS: 
	New files: point2.cc point2_uncertain.cc test_point2.cc
	FILES: Makefile.yam(16000), point2.hh(16000),
		point2_uncertain.hh(16000)
	added point2arr and point2arr_uncertain classes to point2.hh and
	point2_uncertain.hh.  Basically these are wrappers for a vector of
	points.  They also contain geometric analysis functions such as
	get_side which returns which side of a line a given point lies and
	get_offset which offsets line points to either side for lane line
	initialization or polygon growth and contraction.

Wed Feb 21 19:41:19 2007	Sam Pfister (sam)

	* version R1-00d
	BUGS: 
	New files: matrix2.hh point2.hh point2_uncertain.hh
		pose2_uncertain.hh
	Deleted files: pos2.hh poscov2.hh posecov2.hh
	FILES: Makefile.yam(15306), pose2.hh(15306)
	added some point2 assignment functionality to make it work well
	with point2_uncertain.	Need to flush out pose2 classes and
	consider defining operations on vectors of 2d point and pose
	elements.

Thu Feb  8 19:15:40 2007	Sam Pfister (sam)

	* version R1-00c
	BUGS: 
	New files: pos2.hh poscov2.hh pose2.hh posecov2.hh
	FILES: Makefile.yam(14800)
	pos2.hh defines a simple 2d point struct [x,y], poscov2.hh defines
	a simple 2d point and associated covariance parameters.  pose2.hh
	defines a planar pose [x,y,ang], posecov2 defines a planar pose and
	associated covariance parameters.  Some redundancy in pos2 struct
	with respect to XYcoord struct defined in coords.hh, but pos2 is
	not assumed to be tied to any specific frame.

Wed Jan 31 13:06:22 2007	Andrew Howard (ahoward)

	* version R1-00b
	BUGS: 
	New files: pose3.h quat.h vec3.h
	FILES: Makefile.yam(13732)
	Added pose functions to frames module

Sat Jan 27 19:09:40 2007	murray (murray)

	* version R1-00a
	BUGS: 
	New files: CMatrix.cc CMatrix.hh coords.hh frames.cc frames.hh
		rot_matrix.cc rot_matrix.hh
	FILES: Makefile.yam(13296)
	basic YaM makefile

Sat Jan 27 18:53:59 2007	murray (murray)

	* version R1-00
	Created frames module.



























