/**********************************************************
 **
 **  MATRIX2.HH
 **
 **    Time-stamp: <2007-02-13 13:12:17 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 13 10:08:41 2007
 **
 **
 **********************************************************
 **
 **  Simple 2D square matrix struct
 **
 **********************************************************/


#ifndef MATRIX2_H
#define MATRIX2_H

#include <math.h>
#include "point2.hh"


using namespace std;

struct matrix2
{
	double a, b, c, d;

	matrix2() {}
	matrix2(double ain, double bin, double cin, double din)
		: a(ain), b(bin), c(cin), d(din) {}
	matrix2(double ang)
		: a(cos(ang)), b(-sin(ang)), c(sin(ang)), d(cos(ang)){}

	
	~matrix2() {}
	
	matrix2(const matrix2 &mx){
		a = mx.a;
		b = mx.b;
		c = mx.c;
		d = mx.d;
	}


	matrix2 &operator=(const matrix2 &mx){
		if (this!= &mx){
			a = mx.a;
			b = mx.b;
			c = mx.c;
			d = mx.d;
		}
		return *this;
	}
	}

	matrix2 &operator+(const matrix2 &mx) const {
		matrix2 mxout;
		mxout.a = a + mx.a;
		mxout.b = b + mx.b;
		mxout.c = c + mx.c;
		mxout.d = d + mx.d;
		return mxout;
	
	}

	matrix2 &operator-(const matrix2 &mx) const {
		matrix2 mxout;
		mxout.a = a - mx.a;
		mxout.b = b - mx.b;
		mxout.c = c - mx.c;
		mxout.d = d - mx.d;
		return mxout;
	
	}

	matrix2 &operator*(const matrix2 &mx) const {
		matrix2 mxout;
		mxout.a = a*mx.a + b*mx.c;
		mxout.b = a*mx.b + b*mx.d;
		mxout.c = c*mx.a + d*mx.c;
		mxout.d = c*mx.b + d*mx.d;
		return mxout;
	}
	
	point2 &operator*(const point2 &pt) const {
		point2 ptout;
		ptout.x = a*pt.x + b*pt.y;
		ptout.y = c*pt.x + d*pt.y;
		return ptout;
	}
	
	matrix2 get_rot(double ang) const { 
		matrix2 mxout(ang);
		return mxout;
	}
};
#endif
