/**********************************************************
 **
 **  POSE2.HH
 **
 **    Time-stamp: <2007-05-18 18:54:18 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 13:13:07 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef POSE2_H
#define POSE2_H


#include <math.h>
#include <stream.h>

using namespace std;

struct pose2
{

  public:

  double x, y, ang;

  /* some constructors */
  pose2(): x(0), y(0), ang(0) {}
  pose2(double new_x, double new_y, double new_ang) 
	: x(new_x), y(new_y), ang(new_ang) { unwrap_angle(); }

  /* recomend using this to set the angle rather than accessing the
     element directly in order to make sure that the angle is between 
     -PI and PI */
  void setAngle(double new_ang){ ang = new_ang; unwrap_angle(); }

  /* basic math- this doesn't make too much intuitive sense, so I 
     wouldn't use them too much */
  pose2 operator+ (pose2 param) const
  {
	pose2 tmp;
	tmp.x = x + param.x; tmp.y = y + param.y; tmp.ang = ang + param.ang;
	tmp.unwrap_angle();
	return tmp;
  }

  pose2 operator- (pose2 param) const
  {
	pose2 tmp;
	tmp.x = x - param.x; tmp.y = y - param.y; tmp.ang = ang - param.ang;
	tmp.unwrap_angle();
	return tmp;
  }

  pose2 operator* (double a) const
  {
	pose2 tmp;
	tmp.x = a * x; tmp.y = a * y; tmp.ang = a * ang;
	tmp.unwrap_angle();
	return tmp;
  }

  pose2 operator/ (double a) const
  {
	pose2 tmp;
	tmp.x = x / a; tmp.y = y / a; tmp.ang = ang / a;
	return tmp;
  }

  pose2 &operator-= (const pose2 other)
  {
	*this = *this - other;
	return *this;
  }

  pose2 &operator+= (const pose2 other)
  {
	*this = *this + other;
	return *this;
  }

  /* some (slightly) more advanced and useful transformations */

  /* get the angle between -M_PI and M_PI */
  void unwrap_angle()
  {
	while (ang < -M_PI)
		ang += 2 * M_PI;
	while (ang > M_PI)
		ang -= 2 * M_PI;
  }

  /* just change the angle w/o moving the coordinate */
  void rotateInPlace(double dAng) { ang += dAng; unwrap_angle(); }

  /* rotate the coordinate counter clockwise around the origin, 
     preserving angle */
  void rotateAroundOriginPreserveAngle(double dTheta)
  {
	double tmpx = x, tmpy = y, cosd = cos(dTheta), sind = sin(dTheta);
	x = tmpx * cosd - tmpy * sind;
	y = tmpx * sind + tmpy * cosd;
  }

  /* rotate the coordinate counter clockwise around the origin, rotating 
     angle too */
  void rotateAroundOrigin(double dTheta)
  {
	rotateAroundOriginPreserveAngle(dTheta);
	rotateInPlace(dTheta);
  }

  /* rotate the coordinate counter clockwise around the specified point, 
	preserving angle */
  void rotateAroundPointPreserveAngle(pose2 point, double dTheta)
  {
	*this -= point;
	rotateAroundOriginPreserveAngle(dTheta);
	*this += point;
  }

  /* rotate the coordinate counter clockwise around the specified point, 
	preserving angle */
  void rotateAroundPoint(pose2 point, double dTheta)
  {
	*this -= point;
	rotateAroundOrigin(dTheta);
	*this += point;
  }

  /* translate the coordinate */
  void translate(double dx, double dy)
  {
	x += dx;
	y += dy;
  }

  void translate(pose2 d)
  {
	translate(d.x, d.y);
  }

  double magnitude() { return sqrt((x * x) + (y * y)); }

  double argument() { return atan2(y,x); }

  /* print the values as space separated values, so that they can be
     read in by matlab */
  void print(ostream *output = &cout)
  {
	*output << x << " " << y << " " << ang << endl;
  }

  /* print the values with labels as to what they are */
  void display(ostream *output = &cout)
  {
	*output << "x: " << x << " y: " << y << " ang: " << ang << endl;
  }

};

#endif
