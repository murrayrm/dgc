/**********************************************************
 **
 **  TEST_POINT2.CC
 **
 **    Time-stamp: <2007-03-04 17:00:43 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 27 11:32:07 2007
 **
 **
 **********************************************************
 **
 **  test point2 functions
 **
 **********************************************************/

#include "frames/point2.hh"
#include "frames/point2_uncertain.hh"

#include <iostream>

using namespace std;


int main(int argc, char **argv)
{
	cout << "testing point2 class" << endl;
	
	point2 ptrot(31,41);
	cout << "initial point = " << ptrot <<endl;
	ptrot = ptrot.rot(.25);
	cout << "rotated point = " << ptrot <<endl;
point2arr ptarr1;
	point2arr ptarr2;
	point2arr ptarr3;
	
	ptarr1.push_back(point2(1,1));
	ptarr2.push_back(point2(2,2));

	ptarr1.push_back(point2(2,4));
	ptarr2.push_back(point2(1,4));

	cout << "ptarr1 = " << ptarr1 << endl;

	cout << "ptarr2 = " << endl;

	ptarr3 = ptarr1+ptarr2[0];
	
	cout << "sum = " << ptarr3 << endl;

	ptarr3 = 2.5*ptarr1*3.1;
	cout << "product = " << ptarr3 << endl;	

	point2 pt1(4,12.2);

	cout << "pointprod = " << 1.2*pt1*10 << endl;


	point2 pt2(-3.1,13.2);
	//	vector<point2_uncertain> uptarr1;
	//uptarr1 = convert_point2arr(ptarr3);

	//	cout << "ptunc = " << uptarr1 << endl;

	ptarr1.clear();

	//	ptarr1.push_back(point2(10,-1));
	//	ptarr1.push_back(point2(10,1));
	ptarr1.push_back(point2(1,-1));
	ptarr1.push_back(point2(1,1));
	ptarr1.push_back(point2(3,2));
	ptarr1.push_back(point2(3,4));

	cout <<"ptarr1 = " << ptarr1 << endl;

	ptarr2 = ptarr1;
	cout <<"testing copy to ptarr2 = " << ptarr2 
			 << " size = " << ptarr2.size() << endl;
	
	vector<double> vec= ptarr2.norm();
	cout << "norm = " <<vec <<endl;
	
	vec = ptarr2.dist(ptarr2[2]);
	cout << "dist = " <<vec <<endl;

	double ang = .31;
	ptarr2 = ptarr2.rot(ang);
	cout <<"testing rot of ptarr2 = " << ptarr2 
			 << " through ang = " << ang << endl;
	
	vec= ptarr2.norm();
	cout << "norm = " <<vec <<endl;
	
	vec = ptarr2.dist(ptarr2[2]);
	cout << "dist = " <<vec <<endl;


	point2 pt3(ptarr1[2]);
	cout <<"accessing pt at index 2 " << pt3 <<endl;
	
	cout << endl << endl;
	cout << "===========================================" <<endl;
	cout << "point2_uncertain testing : " << endl << endl;


	point2_uncertain unpt1(pt1);
	cout << "initialize from pt1 "<< unpt1 <<endl;

	point2arr_uncertain unptarr1(ptarr1);
	cout << "initialize from ptarr1 "<< unptarr1 <<endl;

	point2_uncertain unpt2;
	unpt2 = pt2;
	cout << "assignment operator unpt2 " << unpt2 << endl;
	unpt2 = unpt2.rot(.31);
	cout << "rotated unpt2 " << unpt2 << endl;
	pt2 = unpt2;
	cout << "reassigned pt2 " << pt2 << endl;


	point2arr_uncertain unptarr2;
	unptarr2 = ptarr2;
	cout << "assignment operator unptarr2 " << unptarr2 << endl;
	cout << endl << endl;
	cout << "===========================================" <<endl;
	cout << "offset testing : " << endl << endl;
	
	ptarr1.clear();
	ptarr1.push_back(point2(-5,3));
	ptarr1.push_back(point2(-2,-1)); 
	ptarr1.push_back(point2(2,-1));
 	ptarr1.push_back(point2(4,0));
	ptarr1.push_back(point2(5,3)); 
	   
	ptarr2 = ptarr1.get_offset(-2);
	
	cout << "ptarr1 " << ptarr1 << endl;
	cout << "ptarr2 " << ptarr2 << endl;
	ptarr2 = ptarr1.get_offset(2);
	cout << "ptarr2 " << ptarr2 << endl;

cout << "===========================================" <<endl;
	cout << "get_side testing : " << endl << endl;
	
	   
	double index;
	int side;

	for (unsigned i=0;i<180;++i){
		pt1 = point2((double)i/10-8,1.5);

		ptarr1.get_side(pt1,side,index);
		cout << "A side=" << side << " index=" << index <<endl;
		ptarr2.get_side(pt1,side,index);
		cout << "B side=" << side << " index=" << index <<endl<< endl;
	}

cout << "===========================================" <<endl;
	cout << "test == and != : " << endl << endl;

	
	if (ptarr1==ptarr2)
		cout << ptarr1 << "==" << ptarr2 <<endl;

	if (ptarr1!=ptarr2)
		cout << ptarr1 << "!=" << ptarr2 <<endl;

	ptarr2 = ptarr1;
	if (ptarr1==ptarr2)
		cout << ptarr1 << "==" << ptarr2 <<endl;
	
	if (ptarr1!=ptarr2)
		cout << ptarr1 << "!=" << ptarr2 <<endl;

	ptarr2[2].x =1341;
	
if (ptarr1==ptarr2)
		cout << ptarr1 << "==" << ptarr2 <<endl;
	
	if (ptarr1!=ptarr2)
		cout << ptarr1 << "!=" << ptarr2 <<endl;

	return 0;
} 
 
