/*
 * Copyright 2005, by the California Institute of Technology. ALL
 * RIGHTS RESERVED. United States Government Sponsorship
 * acknowledged. Any commercial use must be negotiated with the Office
 * of Technology Transfer at the California Institute of Technology.
 *
 * This software may be subject to U.S. export control laws. By
 * accepting this software, the user agrees to comply with all
 * applicable U.S. export laws and regulations. User has the
 * responsibility to obtain export licenses, or other export authority
 * as may be required before exporting such information to foreign
 * countries or providing access to foreign persons.
 */

/* Desc: Structures and operators for 3D vectors
 * Author: Andrew Howard
 * Date: 5 Oct 2006
 * CVS: $Id: vec3.h,v 1.6 2006/10/23 15:19:01 abhoward Exp $
 */

/** @file 

@brief 2-vector structures and operators.

@sa pose2.h

*/

#ifndef VEC2_H
#define VEC2_H

#include <math.h>


/// @brief 3-vector of doubles.
typedef struct
{
  double x, y;
} vec2_t;


/// @brief 3-vector of floats.
typedef struct
{
  float x, y;
} vec2f_t;


/// @brief Return a zero vector.
static __inline__ vec2_t vec2_zero()
{
  vec2_t a = {0, 0};
  return a;
}


/// @brief Return a zero vector.
static __inline__ vec2f_t vec2f_zero()
{
  vec2f_t a = {0, 0};
  return a;
}


/// @brief Set vector elements.
///
/// @returns Returns the vector @f$ (x, y) @f$.
static __inline__ vec2_t vec2_set(double x, double y)
{
  vec2_t a = {x, y};
  return a;
}


/// @brief Set vector elements.
///
/// @returns Returns the vector @f$ (x, y) @f$.
static __inline__ vec2f_t vec2f_set(float x, float y)
{
  vec2f_t a = {x, y};
  return a;
}


/// @brief Set vector elements from an array of doubles.
///
/// @returns Returns the vector @f$ (v[0], v[1]) @f$.
static __inline__ vec2_t vec2_setv(double *v)
{
  vec2_t a = {v[0], v[1]};
  return a;
}


/// @brief Set vector elements from an array of floats.
///
/// @returns Returns the vector @f$ (v[0], v[1]) @f$.
static __inline__ vec2f_t vec2f_setv(float *v)
{
  vec2f_t a = {v[0], v[1]};
  return a;
}


/// @brief Set vector elements from an array of floats.
///
/// @returns Returns the vector @f$ (v[0], v[1]) @f$.
static __inline__ vec2_t vec2_setvf(float *v)
{
  vec2_t a = {v[0], v[1]};
  return a;
}

/// @brief Set vector elements from an array of doubles.
///
/// @returns Returns the vector @f$ (v[0], v[1]) @f$.
static __inline__ vec2f_t vec2f_setvd(double *v)
{
  vec2f_t a = {(float) v[0], (float) v[1]};
  return a;
}


/// @brief Test for finite vector.
///
/// Tests that all the components are valid and finite.
/// @param[in] a Vector to test.
/// @return Returns non-zero if the vector is finite.
static __inline__ int vec2_finite(vec2_t a)
{
  return finite(a.x) && finite(a.y);
}


/// @brief Test for finite vector.
///
/// Tests that all the components are valid and finite.
/// @param[in] a Vector to test.
/// @return Returns non-zero if the vector is finite.
static __inline__ int vec2f_finite(vec2f_t a)
{
  return finite(a.x) && finite(a.y);
}


/// @brief Add vectors.
///
/// @param[in] a, b Vectors to add.
/// @returns Returns the sum @f$ a + b @f$
static __inline__ vec2_t vec2_add(vec2_t a, vec2_t b)
{
  vec2_t c = {a.x + b.x, a.y + b.y};
  return c;
}


/// @brief Add vectors.
///
/// @param[in] a, b Vectors to add.
/// @returns Returns the sum @f$ a + b @f$
static __inline__ vec2f_t vec2f_add(vec2f_t a, vec2f_t b)
{
  vec2f_t c = {a.x + b.x, a.y + b.y};
  return c;
}


/// @brief Subtract vectors.
///
/// @param[in] a, b Vectors to subtract.
/// @returns Returns the difference @f$ a - b @f$
static __inline__ vec2_t vec2_sub(vec2_t a, vec2_t b)
{
  vec2_t c = {a.x - b.x, a.y - b.y};
  return c;
}


/// @brief Subtract vectors.
///
/// @param[in] a, b Vectors to subtract.
/// @returns Returns the difference @f$ a - b @f$
static __inline__ vec2f_t vec2f_sub(vec2f_t a, vec2f_t b)
{
  vec2f_t c = {a.x - b.x, a.y - b.y};
  return c;
}


/// @brief Take inner (dot) product of vectors.
///
/// @param[in] a, b Vectors to multiply..
/// @returns Returns the scalar dot product @f$ a \cdot b @f$
static __inline__ double vec2_dot(vec2_t a, vec2_t b)
{
  return a.x*b.x + a.y*b.y;
}


/// @brief Take inner (dot) product of vectors.
///
/// @param[in] a, b Vectors to multiply..
/// @returns Returns the scalar dot product @f$ a \cdot b @f$
static __inline__ float vec2f_dot(vec2f_t a, vec2f_t b)
{
  return a.x*b.x + a.y*b.y;
}


/// @brief Take outer (cross) product of vectors.
///
/// @param[in] a, b Vectors to multiply..
/// @returns Returns the scalar cross product @f$ a \times b @f$
static __inline__ double vec2_cross(vec2_t a, vec2_t b)
{
  return a.x*b.y - a.y*b.x;;
}


/// @brief Take outer (cross) product of vectors.
///
/// @param[in] a, b Vectors to multiply..
/// @returns Returns the scalar cross product @f$ a \times b @f$
static __inline__ float vec2f_cross(vec2f_t a, vec2f_t b)
{
  return a.x*b.y - a.y*b.x;;
}


/// @brief Multiply vector with scalar.
///
/// @param[in] s Scalar multiplier
/// @param[in] a Vector.
/// @returns Returns the vector @f$ b = (s a_x, s a_y) @f$
static __inline__ vec2_t vec2_scale(double s, vec2_t a)
{
  vec2_t b = {s*a.x, s*a.y};
  return b;
}


/// @brief Multiply vector with scalar.
///
/// @param[in] s Scalar multiplier
/// @param[in] a Vector.
/// @returns Returns the vector @f$ b = (s a_x, s a_y) @f$
static __inline__ vec2f_t vec2f_scale(float s, vec2f_t a)
{
  vec2f_t b = {s*a.x, s*a.y};
  return b;
}


/// @brief Compute the magnitude (modulus) of a vector
///
/// @param a Input vector @f$ a @f$
/// @returns Returns the scalar @f$ b = |a| = \sqrt{a \cdot a} @f$
static __inline__ double vec2_mag(vec2_t a)
{
  return sqrt(a.x*a.x + a.y*a.y);
}


/// @brief Compute the magnitude (modulus) of a vector
///
/// @param a Input vector @f$ a @f$
/// @returns Returns the scalar @f$ b = |a| = \sqrt{a \cdot a} @f$
static __inline__ float vec2f_mag(vec2f_t a)
{
  return sqrtf(a.x*a.x + a.y*a.y);
}


/// @brief Compute the unit vector
///
/// @param a Input vector @f$ a @f$
/// @returns Returns the unit vectpr @f$ b = a / |a| @f$
static __inline__ vec2_t vec2_unit(vec2_t a)
{
  double s = sqrt(a.x*a.x + a.y*a.y);
  vec2_t c = {a.x/s, a.y/s};
  return c;
}


/// @brief Compute the unit vector
///
/// @param a Input vector @f$ a @f$
/// @returns Returns the unit vectpr @f$ b = a / |a| @f$
static __inline__ vec2f_t vec2f_unit(vec2f_t a)
{
  double s = sqrtf(a.x*a.x + a.y*a.y);
  vec2f_t c = {a.x/s, a.y/s};
  return c;
}

#endif
