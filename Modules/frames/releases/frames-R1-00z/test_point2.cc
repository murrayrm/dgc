/**********************************************************
 **
 **  TEST_POINT2.CC
 **
 **    Time-stamp: <2007-05-14 04:57:53 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 27 11:32:07 2007
 **
 **
 **********************************************************
 **
 **  test point2 functions
 **
 **********************************************************/

#include "point2.hh"
#include "point2_uncertain.hh"
#include <string>
#include <vector>
#include <iostream>

using namespace std;


int main(int argc, char **argv)
{
  int testnum = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;
  cout << endl << "Usage :" << endl;
  cout << " test_point2 [Test number : 0 selects none, -1 selects all] " << endl << endl;
 
  if(argc >1)
    testnum = atoi(argv[1]);
  cout << "Test number selected = " << testnum << endl <<endl;

	
  cout <<"========================================" << endl;
  testname = "Testing construction/ erase";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     

    point2arr ptarr1;
    point2arr ptarr2;
    point2arr ptarr3;
	
    ptarr1.push_back(point2(1,1));
    ptarr2.push_back(point2(2,2));

    ptarr1.push_back(point2(2,4));
    ptarr2.push_back(point2(1,4));

    cout << "ptarr1 = " << ptarr1 << endl;

    cout << "ptarr2 = " << endl;

    cout <<"ptarr1 = " << ptarr1 << endl;
     
     
  }


  cout <<"========================================" << endl;
  testname = "Testing rotation";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     
    point2arr ptarr1;
    ptarr1.push_back(point2(1,-1));
    ptarr1.push_back(point2(1,1));
    ptarr1.push_back(point2(3,2));
    ptarr1.push_back(point2(3,4));
     
    cout << ptarr1 << endl;
      
     

  }
   

  cout <<"========================================" << endl;
  testname = "Testing addition subtraction mult";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     
    point2arr ptarr1;
    ptarr1.push_back(point2(1,-1));
    ptarr1.push_back(point2(1,1));
    ptarr1.push_back(point2(3,2));
    ptarr1.push_back(point2(3,4));
     
    cout << ptarr1 << endl;     
  }
    
  cout <<"========================================" << endl;
  testname = "Testing assignment";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     
    point2arr ptarr1,ptarr2;
    ptarr1.push_back(point2(1,-1));
    ptarr1.push_back(point2(1,1));
    ptarr1.push_back(point2(3,2));
    ptarr1.push_back(point2(3,4));
    ptarr2 =ptarr1;
      
    cout << ptarr1 << endl << ptarr2 << endl;     
  }


  cout <<"========================================" << endl;
  testname = "Testing norm and dist";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     
    point2arr ptarr1,ptarr2;
    ptarr1.push_back(point2(1,-1));
    ptarr1.push_back(point2(1,1));
    ptarr1.push_back(point2(3,2));
    ptarr1.push_back(point2(3,4));

    ptarr2 = ptarr1;

    cout << ptarr1 << endl << ptarr2 << endl;  
    vector<double> vec= ptarr2.norm();
    cout << "norm = " <<vec <<endl;
	
    vec = ptarr2.dist(ptarr2[2]);
    cout << "dist = " <<vec <<endl;

    double ang = .31;
    ptarr2 = ptarr2.rot(ang);
    cout <<"testing rot of ptarr2 = " << ptarr2 
         << " through ang = " << ang << endl;
	
    vec= ptarr2.norm();
    cout << "norm = " <<vec <<endl;
	
    vec = ptarr2.dist(ptarr2[2]);
    cout << "dist = " <<vec <<endl;


  }

  cout <<"========================================" << endl;
  testname = "Testing index, assignment";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     
    point2arr ptarr1;
    ptarr1.push_back(point2(1,-1));
    ptarr1.push_back(point2(1,1));
    ptarr1.push_back(point2(3,2));
    ptarr1.push_back(point2(3,4));
    point2 pt3(ptarr1[2]);
    cout <<"accessing pt at index 2 " << pt3 <<endl;
     
  }
   
  cout <<"========================================" << endl;
  testname = "Testing point2_uncertain interface";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    point2arr ptarr1,ptarr2;
    ptarr1.push_back(point2(1,-1));
    ptarr1.push_back(point2(1,1));
    ptarr1.push_back(point2(3,2));
    ptarr1.push_back(point2(3,4));
    ptarr2 = ptarr1;

    point2 pt1(2,3);
    point2 pt2(5,7.8);
    point2_uncertain unpt1(pt1);
    cout << "initialize from pt1 "<< unpt1 <<endl;
      
    point2arr_uncertain unptarr1(ptarr1);
    cout << "initialize from ptarr1 "<< unptarr1 <<endl;
 
    point2_uncertain unpt2;
    unpt2 = pt2;
    cout << "assignment operator unpt2 " << unpt2 << endl;
    unpt2 = unpt2.rot(.31);
    cout << "rotated unpt2 " << unpt2 << endl;
    pt2 = unpt2;
    cout << "reassigned pt2 " << pt2 << endl;


    point2arr_uncertain unptarr2;
    unptarr2 = ptarr2;
    cout << "ptarr2 = " << ptarr2 << endl;
    cout << "assignment operator unptarr2 " << unptarr2 << endl;

  
             
    //    double val = unptarr2.linelength();
    //cout <<"unptarr2 linelength() = " << val << endl;
    //point2 pt;
    //pt =unptarr1.project(unptarr2[1]+point2(1,1));
    //cout <<"unptarr2 project() = " << pt  << endl;
         
  }

  cout <<"========================================" << endl;
  testname = "Testing get_offset ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     
    point2arr ptarr1,ptarr2;
    ptarr1.push_back(point2(-5,3));
    ptarr1.push_back(point2(-2,-1)); 
    ptarr1.push_back(point2(2,-1));
    ptarr1.push_back(point2(4,0));
    ptarr1.push_back(point2(5,3)); 
	   
    ptarr2 = ptarr1.get_offset(-2);
	
    cout << "ptarr1 " << ptarr1 << endl;
    cout << "ptarr2 " << ptarr2 << endl;
    ptarr2 = ptarr1.get_offset(2);
    cout << "ptarr2 " << ptarr2 << endl;

     
  } 

    
  cout <<"========================================" << endl;
  testname = "Testing get_side";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     
    int index;
    int side;

    point2arr ptarr1,ptarr2;
    ptarr1.push_back(point2(-5,3));
    ptarr1.push_back(point2(-2,-1)); 
    ptarr1.push_back(point2(2,-1));
    ptarr1.push_back(point2(4,0));
    ptarr1.push_back(point2(5,3)); 

    point2 pt1;

    for (unsigned i=0;i<180;++i){
      pt1 = point2((double)i/10-8,1.5);

      ptarr1.get_side(pt1,side,index);
      cout << "A side=" << side << " index=" << index <<endl;
      ptarr2.get_side(pt1,side,index);
      cout << "B side=" << side << " index=" << index <<endl<< endl;
    }     
  }
   
  cout <<"========================================" << endl;
  testname = "Testing == and != operators";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    point2arr ptarr1,ptarr2;
    ptarr1.push_back(point2(-5,3));
    ptarr1.push_back(point2(-2,-1)); 
    ptarr1.push_back(point2(2,-1));
    ptarr1.push_back(point2(4,0));
    ptarr1.push_back(point2(5,3)); 
    ptarr2= ptarr1;
	
    if (ptarr1==ptarr2)
      cout << ptarr1 << "==" << ptarr2 <<endl;

    if (ptarr1!=ptarr2)
      cout << ptarr1 << "!=" << ptarr2 <<endl;

    ptarr2 = ptarr1;
    if (ptarr1==ptarr2)
      cout << ptarr1 << "==" << ptarr2 <<endl;
	
    if (ptarr1!=ptarr2)
      cout << ptarr1 << "!=" << ptarr2 <<endl;

    ptarr2[2].x =1341;
	
    if (ptarr1==ptarr2)
      cout << ptarr1 << "==" << ptarr2 <<endl;
	
    if (ptarr1!=ptarr2)
      cout << ptarr1 << "!=" << ptarr2 <<endl;

          
  }


  cout <<"========================================" << endl;
  testname = "Testing ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    point2arr ptarr1,ptarr2;
    ptarr1.push_back(point2(-5,3));
    ptarr1.push_back(point2(-2,-1)); 
    ptarr1.push_back(point2(2,-1));
    ptarr1.push_back(point2(4,0));
    ptarr1.push_back(point2(5,3)); 
    ptarr2= ptarr1;
    point2 pt1(14,-1312);
    unsigned indexin = 3;
    cout <<ptarr1<< " inserting point " <<pt1 <<" at index " << indexin <<endl;
    unsigned indexout = ptarr1.insert(indexin, pt1);


    cout <<ptarr1<< " out index "<<indexout <<endl;
    indexin = 31;
    cout <<ptarr1<< " inserting point " <<pt1 <<" at index " << indexin <<endl;
    indexout = ptarr1.insert(indexin, pt1);

    cout <<ptarr1<< " out index "<<indexout <<endl;

    
  }


 
 
  

  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
     
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
   
 
  cout <<"========================================" 
       << endl << endl;
  return 0;

} 
 
    
  
