/**********************************************************
 **
 **  POINT2_UNCERTAIN.CC
 **
 **    Time-stamp: <2007-05-18 15:45:39 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 27 11:04:35 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/
#include "point2_uncertain.hh"

using namespace std;

point2_uncertain operator*(const double d, const point2_uncertain& pt)
{
  return pt*d;
}

ostream &operator<<(ostream &os, const point2_uncertain &pt) {
  os << "(" << pt.x << ", " << pt.y <<", "<<pt.z 
     <<  ", {" << pt.max_var 
     <<  ", "<< pt.min_var 
     <<  ", "<< pt.axis <<"})";
  return os;
        }

point2_uncertain::point2_uncertain(const point2 &pt)
{
  x = pt.x;
    y = pt.y;
    z = pt.z;
    max_var = 0;
    min_var = 0;
    axis = 0;
}


point2_uncertain &point2_uncertain::operator=(const point2 &pt) {
    x = pt.x;
    y = pt.y;
    z = pt.z;
    max_var = 0;
    min_var = 0;
    axis = 0;
    return *this;
}

point2_uncertain point2_uncertain::operator+(const point2 &pt) const {
  point2_uncertain tmppt(x+pt.x, y+pt.y, max_var, min_var, axis);
  tmppt.z = pt.z;
  return tmppt;
  }

point2_uncertain point2_uncertain::operator-(const point2 &pt) const {
  point2_uncertain tmppt(x-pt.x, y-pt.y, max_var, min_var, axis);
  tmppt.z = pt.z;
  return tmppt;
}  

void point2_uncertain::set(point2 &pt)
{
  x = pt.x;
  y = pt.y;
  z = pt.z;
  max_var = 0;
  min_var = 0;
  axis = 0;
  
}
 void point2_uncertain::set_point(point2 &pt)
{
  x = pt.x;
  y = pt.y;
  z = pt.z;
}







point2arr_uncertain::point2arr_uncertain(const point2arr &ptarr)
{
  this->resize(ptarr.size());
  for(unsigned int i=0;i<ptarr.size();++i){
    arr[i].x=ptarr[i].x;
    arr[i].y=ptarr[i].y;
    arr[i].z=ptarr[i].z;
    arr[i].max_var=0;
    arr[i].min_var=0;
    arr[i].axis=0;

  }
}
point2arr_uncertain::point2arr_uncertain(const vector<point2> &ptarr){
  this->resize(ptarr.size());
  for(unsigned int i=0;i<ptarr.size();++i){
    arr[i]=ptarr[i];
  }
}

void point2arr_uncertain::set(const point2arr &ptarr)
{
  unsigned int sze = ptarr.size();
  this->resize(sze);
  for(unsigned int i=0;i<sze;++i){
    arr[i].x=ptarr[i].x;
    arr[i].y=ptarr[i].y;
    arr[i].z=ptarr[i].z;
    arr[i].max_var=0;
    arr[i].min_var=0;
    arr[i].axis=0;

  }
}
void point2arr_uncertain::set(const point2arr_uncertain &ptarr)
{
  unsigned int sze = ptarr.size();
  this->resize(sze);
  for(unsigned int i=0;i<sze;++i){
    arr[i].x=ptarr[i].x;
    arr[i].y=ptarr[i].y;
    arr[i].max_var=ptarr[i].max_var;
    arr[i].min_var=ptarr[i].min_var;
    arr[i].axis=ptarr[i].axis;

  }
}


point2arr_uncertain &point2arr_uncertain::operator=(const point2arr &ptarr){
  this->resize(ptarr.size());
  for(unsigned int i=0;i<ptarr.size();++i){
    arr[i]=ptarr[i];
  }
  return *this;
}

point2_uncertain point2arr_uncertain::min() const{
  unsigned int arrsize = this->size();
  double minx = 0;
  double miny = 0;
  double minz = 0;
  double xvar = 0;
  double yvar = 0;
  for (unsigned int i = 0; i < arrsize; ++i){
    if (arr[i].x< minx || i==0){
      minx = arr[i].x;
      xvar = arr[i].max_var;
    }
    if (arr[i].y< miny || i==0){
      miny = arr[i].y;
      yvar = arr[i].max_var;
    }
    if (arr[i].z < minz || i ==0) {
      minz = arr[i].z;
    }
  }

  if (xvar>yvar){ 
    point2_uncertain tmppt(minx,miny,xvar,yvar,0);
    tmppt.z = minz;
    return tmppt;
  }  else  {
    point2_uncertain tmppt(minx,miny,yvar,xvar,M_PI/2);
    tmppt.z = minz;
    return tmppt;
  }
}

point2_uncertain point2arr_uncertain::max() const{
  unsigned int arrsize = this->size();
  double maxx = 0;
  double maxy = 0;
  double maxz = 0;
  double xvar = 0;
  double yvar = 0;
  for (unsigned i = 0; i < arrsize; ++i){
    if (arr[i].x> maxx || i==0){
      maxx = arr[i].x;
      xvar = arr[i].max_var;
    }
    if (arr[i].y> maxy || i==0){
      maxy = arr[i].y;
      yvar = arr[i].max_var;
    }
    if(arr[i].z>maxz || i==0) {
      maxz = arr[i].z;
    }
  }
  if (xvar>yvar) {
    point2_uncertain tmppt(maxx,maxy,xvar,yvar,0);
    tmppt.z = maxz;
    return tmppt; 
  }  else  {
    point2_uncertain tmppt(maxx,maxy,yvar,xvar,M_PI/2);
    tmppt.z = maxz;
    return tmppt;
  }

}

unsigned int point2arr_uncertain::insert(unsigned int n, point2_uncertain &pt)
{
  unsigned int index=n;
  if (n > arr.size())
    index = arr.size();
  arr.insert(arr.begin()+index,pt);
  return index;
  
}


point2arr_uncertain point2arr_uncertain::rot(const double ang) const
{
      point2arr_uncertain outarr;
      unsigned int i;
      for (i=0;i<arr.size();++i){
        outarr.push_back(arr[i].rot(ang));
      }
      return outarr;
}

vector<double> point2arr_uncertain::norm() const
{
      vector<double> outarr;
      unsigned int i;
      for (i=0;i<arr.size();++i){
        outarr.push_back(arr[i].norm());
      }
      return outarr;
}

vector<double> point2arr_uncertain::dist(const point2_uncertain &pt) const
{
      vector<double> outarr;
      unsigned int i;
      for (i=0;i<arr.size();++i){
        outarr.push_back(arr[i].dist(pt));
      }
      return outarr;
}

point2arr_uncertain point2arr_uncertain::operator+(const point2_uncertain &pt) const
{
      point2arr_uncertain outarr;
      unsigned int i;
      for (i=0;i<arr.size();++i){
        outarr.push_back(arr[i]+pt);
      }
      return outarr;
}

point2arr_uncertain point2arr_uncertain::operator-(const point2_uncertain &pt) const
{
      point2arr_uncertain outarr;
      unsigned int i;
      for (i=0;i<arr.size();++i){
        outarr.push_back(arr[i]-pt);
      }
      return outarr;
}


point2arr_uncertain point2arr_uncertain::operator*(const double d) const
{
      point2arr_uncertain outarr;
      unsigned int i;
      for (i=0;i<arr.size();++i){
        outarr.push_back(arr[i]*d);
      }
      return outarr;
}




void point2arr_uncertain::reverse()
{
  vector<point2_uncertain> tmp;
  tmp.clear();
  int i;
  int arrsize = (int)this->size();
  for(i=0;i<arrsize;++i){
    tmp.push_back(arr[arrsize-1-i]);
   }
  arr = tmp;
 } 


void point2arr_uncertain::connect(const point2arr_uncertain &ptarr)
{
  unsigned int sizeB = ptarr.size();
  for (unsigned int i =0;i<sizeB;++i){
    arr.push_back(point2(ptarr[i].x,ptarr[i].y,ptarr[i].z));
   }
}
















point2arr_uncertain operator*(const double d,const point2arr_uncertain &ptarr)
{
  return ptarr*d;
}



ostream &operator<<(ostream &os, const point2arr_uncertain &ptarr) {
  unsigned int i;
  
  for (i=0;i<ptarr.size();++i){
    os  << endl<< "[" << i << "] " << ptarr[i];
  }
  return os;
        }

//vector<point2> operator=(const vector<point2_uncertain>& ptarr){
//  vector<point2> outarr(ptarr.size());
//  unsigned int i;
//  for (i=0;i<ptarr.size();++i){
//    outarr[i].x = ptarr[i].x;
//    outarr[i].y = ptarr[i].y;
//  }
//  return outarr;
//}
