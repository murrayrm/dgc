/*
 * Copyright 2005, by the California Institute of Technology. ALL
 * RIGHTS RESERVED. United States Government Sponsorship
 * acknowledged. Any commercial use must be negotiated with the Office
 * of Technology Transfer at the California Institute of Technology.
 *
 * This software may be subject to U.S. export control laws. By
 * accepting this software, the user agrees to comply with all
 * applicable U.S. export laws and regulations. User has the
 * responsibility to obtain export licenses, or other export authority
 * as may be required before exporting such information to foreign
 * countries or providing access to foreign persons.
 */

/* Desc: Structures and operators for 2D poses and transforms
 * Author: Andrew Howard
 * Date: 3 Oct 2007
 * CVS: $Id: pose2.h,v 1.11 2006/12/06 16:43:11 abhoward Exp $
 */

/** @file 

@brief Structures and operators for 2D transformations.

@sa vec2.h

A pose @f$ p = (p_t, p_r) @f$ to consists of a 2-vector @f$ p_t @f$
describing the position of a body and an angle @f$ p_r @f$
describing its attitude.  The pose also describes a rigid-body
transformation between coordinate systems such that:

@f[ v' = R(p_r) v + p_t @f]

where @f$ v @f$ is a vector measured the frame attached to the body
and @f$ v' @f$ corresponding vector in world coordinates.  For
compactness, we write the above expression as:

@f[ v' = p \otimes v @f]

Poses can be inverted: if @f$ p @f$ is the pose of the body in the
world frame, then @f$ p^{-1} @f$ is the pose of the world in the body
frame.  Equivalently, if @f$ p @f$ transforms points from the body
frame to the world frame, @f$ p^{-1} @f$ will transform points from
the world frame to the body frame:

@f[ v = p^{-1} \otimes v' @f]

Poses can be composed: if @f$ a @f$ is the pose of a camera measured
in the robot frame, and @f$ b @f$ is the pose of the robot in the
world frame, the pose @f$ c @f$ of the camera in the world frame is
given by @f$ c = b \otimes a @f$.  Equivalently, if @f$ a @f$
transforms points from the camera frame to the robot frame, and @f$ b
@f$ transforms points from the robot frame to the world frame, the
combined transform @f$ c = b \otimes a @f$ will transform points from
the camera frame to the world frame:

@f[ v' = c \otimes v = (b \otimes a) \otimes v = b \otimes (a \otimes v) @f]

Poses can also be converted to/from a 3x3 homogenous transform matrix @f$ M
@f$ such that:

@f[
\left[
\begin{array}{l}
v_x' \\ v_y' \\ 1
\end{array}
\right]
=
\left[
\begin{array}{lll}
R_{00} & R_{01} & T_x \\
R_{10} & R_{11} & T_y \\
0 & 0 & 0 & 1 \\
\end{array}
\right]
\left[
\begin{array}{l}
v_x \\ v_y \\ 1
\end{array}
\right]
@f]

*/

#ifndef POSE2_H
#define POSE2_H

#include <math.h>
#include "vec2.h"


/// @brief 2D pose data.
///
/// Represents coordinate transformation such that:
/// @f[ v' = R(r) v + t @f]
/// where @f$ r @f$ is the rotation angle and @f$ t @f$ is
/// the translation (2-vector).
typedef struct
{
  vec2_t pos;
  double rot;
} pose2_t;
typedef struct
{
  vec2f_t pos;
  float rot;
} pose2f_t;


/// @brief Returns the identity pose.
///
/// @returns Returns the identity pose.
static __inline__ pose2_t pose2_ident()
{
  pose2_t c = {{0, 0}, 0};
  return c;
}
static __inline__ pose2f_t pose2f_ident()
{
  pose2f_t c = {{0, 0}, 0};
  return c;
}


/// @brief Set the pose.
///
/// @param[in] pos Translation vector.
/// @param[in] rot Rotation angle.
/// @returns New pose.
static __inline__ pose2_t pose2_set(vec2_t pos, double rot)
{
  pose2_t c = {pos, rot};
  return c;
}
static __inline__ pose2f_t pose2f_set(vec2f_t pos, float rot)
{
  pose2f_t c = {pos, rot};
  return c;
}


/// @brief Test for finite pose.
///
/// Tests that all the components are valid and finite.
/// @param[in] p Pose to test.
/// @return Returns non-zero if the pose is finite.
static __inline__ int pose2_finite(pose2_t p)
{
  return finite(p.rot) && vec2_finite(p.pos);
}
static __inline__ int pose2f_finite(pose2f_t p)
{
  return finite(p.rot) && vec2f_finite(p.pos);
}


/// @brief Rotate vector by a quaternion.
///
/// Given an angle $q$ and vector $v$, compute the rotated
/// vector @f$ v' = R(r) v @f$.
///
/// @param[in] r Rotation angle.
/// @param[in] v 2-vector to rotate.
/// @returns Returns the rotated vector.
static __inline__ vec2_t vec2_rotate(double r, vec2_t v)
{
  double s, c;
  c = cos(r);
  s = sin(r);
  return vec2_set(c*v.x - s*v.y, s*v.x + c*v.y);
}
static __inline__ vec2f_t vec2f_rotate(float r, vec2f_t v)
{
  float s, c;
  c = cosf(r);
  s = sinf(r);
  return vec2f_set(c*v.x - s*v.y, s*v.x + c*v.y);
}


/// @brief Transform a vector.
///
/// Given pose @f$ p @f$ and vector @f$ v @f$, compute
/// the transformed vector @f$ v' @f$ such that:
/// @f[ v' = p \otimes v = R(r) v + p_t @f]
static __inline__ vec2_t vec2_transform(pose2_t p, vec2_t v)
{
  return vec2_add(vec2_rotate(p.rot, v), p.pos);
}
static __inline__ vec2f_t vec2f_transform(pose2f_t p, vec2f_t v)
{
  return vec2f_add(vec2f_rotate(p.rot, v), p.pos);
}


/// @brief Compute the inverse pose.
///
/// Given some pose @f$ p @f$ such that:
/// @f[ v' = p \otimes v @f]
/// this function computes the inverse transform @f$ p^{-1} @f$:
/// @f[ v  = p^{-1} \otimes v' @f]
///
/// @param[in] p Pose to invert.
/// @return Returns the inverse pose.
static __inline__ pose2_t pose2_inv(pose2_t p)
{
  pose2_t q;
  q.rot = -p.rot;
  q.pos = vec2_scale(-1, vec2_rotate(q.rot, p.pos));
  return q;
}
static __inline__ pose2f_t pose2f_inv(pose2f_t p)
{
  pose2f_t q;
  q.rot = -p.rot;
  q.pos = vec2f_scale(-1, vec2f_rotate(q.rot, p.pos));
  return q;
}


/// @brief Combine two transforms.
///
/// Given transforms @f$ b, a @f$ such that
/// @f[ v' = b \otimes a \otimes v @f]
/// compute the combined transform @f$ c = b \otimes a @f$:
/// @f[ v' = c \otimes v @f]
///
/// @param[in] b Transform to apply last.
/// @param[in] a Transform to apply first.
/// @returns Combined transform.
static __inline__ pose2_t pose2_mul(pose2_t b, pose2_t a)
{
  pose2_t c;
  c.rot = b.rot + a.rot;
  c.pos = vec2_add(vec2_rotate(b.rot, a.pos), b.pos);
  return c;
}
static __inline__ pose2f_t pose2f_mul(pose2f_t b, pose2f_t a)
{
  pose2f_t c;
  c.rot = b.rot + a.rot;
  c.pos = vec2f_add(vec2f_rotate(b.rot, a.pos), b.pos);
  return c;
}


/// @brief Create a pose from a 3x3 rotation matrix of doubles.
static __inline__ pose2_t pose2_from_mat33d(double m[3][3])
{
  return pose2_set(vec2_set(m[0][2], m[1][2]), atan2(m[2][0], m[2][1]));
}
static __inline__ pose2f_t pose2f_from_mat33f(float m[3][3])
{
  return pose2f_set(vec2f_set(m[0][2], m[1][2]), atan2(m[2][0], m[2][1]));
}


/// @brief Convert to homogeneous 3x3 matrix of doubles.
///
/// @param[in] p Transform.
/// @param[out] m Homogenous transform matrix.
static __inline__ void pose2_to_mat33d(pose2_t p, double m[3][3])
{

  m[0][0] = +cos(p.rot);
  m[0][1] = -sin(p.rot);

  m[1][0] = +sin(p.rot);
  m[1][1] = +cos(p.rot);

  m[0][2] = p.pos.x;
  m[1][2] = p.pos.y;

  m[2][0] = 0;
  m[2][1] = 0;
  m[2][2] = 1;

  return;
}
static __inline__ void pose2f_to_mat33f(pose2f_t p, float m[3][3])
{

  m[0][0] = +cos(p.rot);
  m[0][1] = -sin(p.rot);

  m[1][0] = +sin(p.rot);
  m[1][1] = +cos(p.rot);

  m[0][2] = p.pos.x;
  m[1][2] = p.pos.y;

  m[2][0] = 0;
  m[2][1] = 0;
  m[2][2] = 1;

  return;
}



#endif
