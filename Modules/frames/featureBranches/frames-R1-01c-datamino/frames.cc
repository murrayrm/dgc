// Frame Transformation Class
// Used to transform coordinates from a body-fixed frame to
// a fixed reference (nav) frame
// 
// Revision History
// H Huang 25 Jan 04
// Created

#include <math.h>
#include "frames.hh"

using namespace std;

// constructors
frames::frames()
{
  // since all XYZcoord and rot_matrix initialize to identity when
  // declared, nothing needs to be done
}

// frames(sensor offsets)
// creates frame transform already initialized with sensor offset
// rotation matrix for S2B and offset are initialized, vehicle origin is not 
frames:: frames(XYZcoord sensor_offset, double sensor_pitch, double sensor_roll, double sensor_yaw)
{
  // initialize rotation matrix with proper attitude angles
  S2B.set_b2n_attitude(sensor_pitch, sensor_roll, sensor_yaw);
  
  // offsets are what must be added to sensor coordinates to get body coords
  offset.X = sensor_offset.X;
  offset.Y = sensor_offset.Y;
  offset.Z = sensor_offset.Z;
}

// initFrames( sensor offsets)
// same thing as constructor above, essentially
void frames::initFrames(XYZcoord sensor_offset, double sensor_pitch, double sensor_roll, double sensor_yaw)
{
  // initialize rotation matrix with proper attitude angles
  S2B.set_b2n_attitude(sensor_pitch, sensor_roll, sensor_yaw);

  // offsets are what must be added to sensor coordinates to get body coords
  offset.X = sensor_offset.X;
  offset.Y = sensor_offset.Y;
  offset.Z = sensor_offset.Z;
  return;
}

// sets the body state of the vehicle to be used in transformations
// from vehicle's body-fixed frame to UTM frame
void frames::updateState(NEDcoord vehPos, float Pitch, float Roll, float Yaw)
{
  // intialize body to nav rotation matrix
  B2N.set_b2n_attitude(Pitch, Roll, Yaw);

  // initialize UTM offsets
  UTMoffset.N = vehPos.N;
  UTMoffset.E = vehPos.E;
  UTMoffset.D = vehPos.D;

  return;
}

// transform a given point in the sensor's coordinate frame to UTM frame
NEDcoord frames::transformS2N(XYZcoord point)
{
  XYZcoord result1;
  NEDcoord result2;
  
  // get point coordinates in body frame first
  result1 = transformS2B(point);

  // now convert from body to nav frame
  result2 = transformB2N(result1);
  
  return result2;
}

// transform a given point in the nav frame (UTM) to body frame
NEDcoord frames::transformB2N(XYZcoord point)
{
  XYZcoord result1;
  NEDcoord result2;

  // perform r otation out of body frame
  result1 = B2N * point;

  // now add UTM offset to get UTM coordinates
  result2.N = UTMoffset.N + result1.X;
  result2.E = UTMoffset.E + result1.Y;
  result2.D = UTMoffset.D + result1.Z;

  return result2;
}

// transforms a given point in the sensor's view frame to the body frame
XYZcoord frames::transformS2B(XYZcoord point)
{
  XYZcoord result;
  //cout << " frames.cc: point: " << point.X << " " << point.Y << " " 
  //    << point.Z << endl;

  // first rotate the point out of the local frame
  result = S2B * point;
  //cout << " frames.cc: result before offset: " << result.X << " " 
  //     << result.Y << " " << result.Z << endl;

  //cout << " frames.cc: S2B matrix: ";
  //S2B.display();

  // now add offsets to get body frame coordinates 
  result.X += offset.X;
  result.Y += offset.Y;
  result.Z += offset.Z;
  
  return result;

}

/*------------------------------------------------------------------*/

// transform a given point in the UTM frame to the sensor's coordinate frame
XYZcoord frames::transformN2S(NEDcoord point)
{
  XYZcoord result1, result2;
  
  // get point coordinates in body frame first
  result1 = transformN2B(point);

  // now convert from body to sensor frame
  result2 = transformB2S(result1);
  
  return result2;
}

// transform a given point in the nav frame to the body frame
XYZcoord frames::transformN2B(NEDcoord point)
{
  XYZcoord result1, result2;

  //create inverse rotation matrix
  rot_matrix N2B(B2N);
  N2B.invert();

  // first subtract UTM offset to get body coordinates
  result1.X = point.N - UTMoffset.N;
  result1.Y = point.E - UTMoffset.E;
  result1.Z = point.D - UTMoffset.D;

  // perform rotation into body frame
  result2 = N2B * result1;

  return result2;
}

// transform a given point in the body frame to the sensor's coordinate frame
XYZcoord frames::transformB2S(XYZcoord point)
{
  XYZcoord result;

  //create inverse rotation matrix
  rot_matrix B2S(S2B);
  B2S.invert();

  // first subtract offsets to get local frame coordinates 
  point.X -= offset.X;
  point.Y -= offset.Y;
  point.Z -= offset.Z;

  // then rotate the point into the local frame
  result = B2S * point;
  
  return result;
}

/*------------------------------------------------------------------*/
