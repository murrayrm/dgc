/**********************************************************
 **
 **  POINT2.CC
 **
 **    Time-stamp: <2007-05-30 09:39:12 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 27 10:52:43 2007
 **
 **
 **********************************************************
 **
 **   Simple 2D point class
 **
 **********************************************************/

#include "point2.hh"


using namespace std;

#define INF 1000



point2 operator*(const double d, const point2& pt)
{
  return pt*d;
}
istream &operator>>(istream &is, point2 &pt) {
  is >> pt.x >> pt.y; 
  return is;
}
ostream &operator<<(ostream &os, const point2 &pt) {
  os << "(" << pt.x << ", " << pt.y << ")";
  return os;
}

ostream &operator<<(ostream &os, const line2 &line) {
  os << "[" << line.ptA << "," << line.ptB << "]" << endl;
  return os;
}

point2::point2(const point2_uncertain &pt)
{
  x = pt.x;
  y = pt.y;
}

point2 &point2::operator=(const point2_uncertain &pt){
  x = pt.x;
  y = pt.y;
    
  return *this;
}

void point2::set(const point2_uncertain &pt)
{
  x = pt.x;
  y = pt.y;
}



point2 point2::operator+(const point2_uncertain &pt) const{
  return point2(x+pt.x,y+pt.y);
}

point2 point2::operator-(const point2_uncertain &pt) const{
  return point2(x-pt.x,y-pt.y);
}


bool point2::operator==(const point2_uncertain &pt) const
{
  return ((x==pt.x)&&(y==pt.y));
}
bool point2::operator!=(const point2_uncertain &pt) const
{
  return ((x!=pt.x)||(y!=pt.y));
}


point2arr::point2arr(const point2arr_uncertain &ptarr)
{
  arr.resize(ptarr.size());
  for (unsigned int i=0; i < ptarr.size();++i){
    arr[i] =ptarr[i];
  }
}


unsigned int point2arr::insert(unsigned int n, point2 &pt)
{
  unsigned int index=n;
  if (n > arr.size())
    index = arr.size();
  arr.insert(arr.begin()+index,pt);
  return index;
} 



bool point2arr::operator== (const point2arr& ptarr) const
{
  if (ptarr.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (ptarr[i]!=this->arr[i])
        return false;
    }
    return true;
  }
  return false;
}

bool point2arr::operator== (const point2arr_uncertain & ptarr) const
{
  if (ptarr.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (this->arr[i]!=ptarr[i])
        return false;
    }
    return true;
  }
  return false;
}

bool point2arr::operator!= (const point2arr& ptarr) const
{
  if (ptarr.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (ptarr[i]!=this->arr[i])
        return true;
    }
    return false;
  }
  return true;
}

bool point2arr::operator!= (const point2arr_uncertain & ptarr) const
{
  if (ptarr.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (this->arr[i]!=ptarr[i])
        return true;
    }
    return false;
  }
  return true;
}


point2arr &point2arr::operator=(const point2arr_uncertain &ptarr){
  this->resize(ptarr.size());
  for(unsigned int i=0;i<ptarr.size();++i){
    arr[i].x=ptarr[i].x;
    arr[i].y=ptarr[i].y;
  }
  return *this;
}

void point2arr::set(const point2arr_uncertain &ptarr){
  arr.resize(ptarr.size());
  for (unsigned int i=0; i < ptarr.size();++i){
    arr[i].x =ptarr[i].x;
    arr[i].y =ptarr[i].y;
  }
}
void point2arr::set(const point2arr &ptarr){
  arr.resize(ptarr.size());
  for (unsigned int i=0; i < ptarr.size();++i){
    arr[i].x =ptarr[i].x;
    arr[i].y =ptarr[i].y;
  }
}



point2arr &point2arr::operator=(const line2 &line)
{
  this->arr.clear();
  this->arr.push_back(line.ptA);
  this->arr.push_back(line.ptB);
  return *this;
}


point2arr point2arr::rot(const double ang) const
{
  point2arr outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i].rot(ang));
  }
  return outarr;
}

vector<double> point2arr::norm() const
{
  vector<double> outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i].norm());
  }
  return outarr;
}

vector<double> point2arr::dist(const point2 &pt) const
{
  vector<double> outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i].dist(pt));
  }
  return outarr;
}

double point2arr::get_min_dist_points(const point2 &pt) const
{
  double mindist = -1;
  double thisdist;
  unsigned int i;
  unsigned int thissize = arr.size();
  if (thissize<1)
    return -1;
  thisdist = arr[0].dist(pt);
  
  for (i=1;i<thissize;++i){
    thisdist = arr[i].dist(pt);
    if (thisdist <mindist){
      mindist = thisdist;
    }
  }
  return mindist;
}

double point2arr::get_min_dist_line(const point2 &pt) const
{
  double mindist = -1;
  unsigned int i;
  unsigned int arrsize = this->size();

  if (arrsize <=0){
    return mindist;
  } else if (arrsize==1){
    mindist = pt.dist(arr[0]);
  }
    
  line2 thisline(arr[0],arr[1]);
  mindist = thisline.dist(pt);
  double thisdist;
  for (i = 2;i<arrsize;++i){
    thisline.set(arr[i-1],arr[i]);
    thisdist = thisline.dist(pt);
    if (thisdist<mindist){
      mindist = thisdist;
    }
  }
  return mindist;
}

double point2arr::get_min_dist_poly(const point2 &pt) const
{
  double mindist = -1;
  unsigned int arrsize = this->size();
  if (arrsize ==0){
    return mindist;
  } else if (arrsize==1){
    mindist = pt.dist(arr[0]);
  }

  if (is_inside_poly(pt)){
    return 0;
  }

  point2arr tmparr(arr);
  tmparr.push_back(arr[0]);
  return get_min_dist_line(pt);
  
}

double point2arr::get_min_dist_points(const point2arr &ptarr) const
{
  double mindist = -1;
  double thisdist;
  unsigned int i,j;
  unsigned int thissize = this->size();
  unsigned int thatsize = ptarr.size();

  if (thissize<1 || thatsize<1)
    return -1;
  mindist = arr[0].dist(ptarr[0]);

  for (i=0;i<thissize;++i){
    for (j=0;j<thatsize;++j){
      thisdist = arr[i].dist(ptarr[j]);
      if (thisdist <mindist){
        mindist = thisdist;
      }
    }
  }
  return mindist;
}

double point2arr::get_min_dist_line(const point2arr &ptarr) const
{
  double mindist = -1;
  unsigned int i,j;
  unsigned int thissize = this->size();
  unsigned int thatsize = ptarr.size();

  if (thissize ==0 || thatsize ==0){
    return mindist;
  } else if (thissize==1){
    return ptarr.get_min_dist_line(arr[0]);
  } else if (thatsize==1) {
    return get_min_dist_line(ptarr[0]);
  }
    
  line2 thisline;
  line2 thatline;

  double thisdist;
  for (i = 1;i<thissize;++i){
    thisline.set(arr[i-1],arr[i]);    
    for (j = 1; j<thatsize; ++j){
      thatline.set(ptarr[j-1],ptarr[j]);
      thisdist = thisline.dist(thatline);
      if (thisdist<mindist){
        mindist = thisdist;
      }
    }
  }
  return mindist;
}

double point2arr::get_min_dist_poly(const point2arr &ptarr) const
{
 double mindist = -1;
  unsigned int i,j;
  unsigned int thissize = this->size();
  unsigned int thatsize = ptarr.size();

  if (thissize ==0 || thatsize ==0){
    return mindist;
  } else if (thissize==1){
    return  ptarr.get_min_dist_poly(arr[0]);
  } else if (thatsize==1) {
    return  get_min_dist_poly(ptarr[0]);
  }
    
  for (i=0;i<thissize;++i){
    if (ptarr.is_inside_poly(arr[i])){
      return 0;
    }
  }
  for (j=0;j<thatsize;++j){
    if (is_inside_poly(ptarr[j])){
      return 0;
    }
  }
  point2arr tmparr(arr), tmparr2(ptarr);
  tmparr.push_back(arr[0]);
  tmparr2.push_back(ptarr[0]);
  
  return tmparr.get_min_dist_line(tmparr2);
  
}

double point2arr::get_min_dist_line_poly(const point2arr &ptarr) const
{
  double mindist = -1;
  unsigned int i;
  unsigned int thissize = this->size();
  unsigned int thatsize = ptarr.size();

  if (thissize ==0 || thatsize ==0){
    return mindist;
  } else if (thissize==1){
    return ptarr.get_min_dist_poly(arr[0]);
  } else if (thatsize==1) {
    return get_min_dist_line(ptarr[0]);
  }

    
  for (i=0;i<thissize;++i){
    if (ptarr.is_inside_poly(arr[i])){
      return 0;
    }
  }

    point2arr tmparr(ptarr);
  tmparr.push_back(ptarr[0]);
  
  return get_min_dist_line(tmparr);
  
}
double point2arr::get_min_dist_poly_line(const point2arr &ptarr) const
{
 double mindist = -1;
  unsigned int j;
  unsigned int thissize = this->size();
  unsigned int thatsize = ptarr.size();

  if (thissize ==0 || thatsize ==0){
    return mindist;
  } else if (thissize==1){
    return ptarr.get_min_dist_line(arr[0]);
  } else if (thatsize==1) {
    return get_min_dist_poly(ptarr[0]);
  }
    
  for (j=0;j<thatsize;++j){
    if (is_inside_poly(ptarr[j])){
      return 0;
    }
  }
  point2arr tmparr(arr);
  tmparr.push_back(arr[0]);
  
  return tmparr.get_min_dist_line(ptarr);
}




int point2arr::get_poly_clip( const point2arr &ptarr, const gpc_op operation,vector<point2arr> & ptarrarr) const
{
  int i,j;
  int thissize = size();
  int thatsize = ptarr.size();
  point2 pt;
  gpc_vertex_list thiscontour = {0};
  gpc_vertex_list thatcontour = {0};
  gpc_polygon clip = {0};
  gpc_polygon dst = {0};
  gpc_polygon base = {0};

  // Create empty contour
  thiscontour.vertex = (gpc_vertex*)calloc(thissize*2+2, sizeof(double));  
  thatcontour.vertex = (gpc_vertex*)calloc(thatsize*2+2, sizeof(double));  
  
  
  for (i = 0; i<thissize;++i){
    pt = arr[i];
  // Update free-space contour
    thiscontour.vertex[i].x = pt.x;
    thiscontour.vertex[i].y = pt.y;
    //thiscontour.num_vertices += 1;
  }
  thiscontour.num_vertices = thissize;

  for (i = 0; i<thatsize;++i){
    pt = ptarr[i];
  // Update free-space contour
    thatcontour.vertex[i].x = pt.x;
    thatcontour.vertex[i].y = pt.y;
    //    thatcontour.num_vertices += 1;
  }
  thatcontour.num_vertices = thatsize;


   gpc_add_contour(&clip, &thatcontour, false);
 free(thatcontour.vertex);

  gpc_add_contour(&base, &thiscontour, false);
 free(thiscontour.vertex);

 gpc_polygon_clip(operation,&base,&clip,&dst);

 // Clean up
 gpc_free_polygon(&base);
 gpc_free_polygon(&clip);

 ptarrarr.clear();
 point2arr tmpptarr;
 int contournum = dst.num_contours;
 int vertexnum =0;
 for (i = 0; i<dst.num_contours;i++){
   tmpptarr.clear();
   vertexnum = dst.contour[i].num_vertices;
   for (j=0 ; j<vertexnum;j++){
     cout << "x = " <<dst.contour[i].vertex[j].x << " y = " << dst.contour[i].vertex[j].y << endl;;
     pt.x = dst.contour[i].vertex[j].x;
     pt.y = dst.contour[i].vertex[j].y;
     tmpptarr.push_back(pt);
   }
   cout << "poly " <<i << " contournum = "<< contournum <<" vertexnum = " <<vertexnum <<tmpptarr << endl;
   ptarrarr.push_back(tmpptarr);
   
 }
 return ptarrarr.size();
}

  int point2arr::get_poly_difference(const point2arr &ptarr,vector<point2arr> & ptarrarr) const 
{return get_poly_clip(ptarr,GPC_DIFF,ptarrarr);}

  int  point2arr::get_poly_intersection(const point2arr &ptarr,vector<point2arr> & ptarrarr) const 
{return get_poly_clip(ptarr,GPC_INT,ptarrarr);}

  int  point2arr::get_poly_xor(const point2arr &ptarr,vector<point2arr> & ptarrarr) const 
{return get_poly_clip(ptarr,GPC_XOR,ptarrarr);}

  int  point2arr::get_poly_union(const point2arr &ptarr,vector<point2arr> & ptarrarr) const 
{return get_poly_clip(ptarr,GPC_UNION,ptarrarr);}
  
double point2arr::get_poly_area() const
{
  int sze = size();
  int i,j;
  if (sze<3)
    return 0.0;

  double area = 0.0;
  for (i=0; i<sze; ++i){
    j= i+1;
    if (j > sze-1){
       j = 0;
    }
    area += arr[i].x*arr[j].y - arr[i].y*arr[j].x;
  }
  area = fabs(0.5*area);
  return area;
}

vector<double> point2arr::get_dist() const
{
  vector<double> outarr;
  unsigned int i;
  unsigned int sze = arr.size();
  for (i=0;i<sze-1;++i){
    outarr.push_back(arr[i].dist(arr[i+1]));
  }
  return outarr;
}

vector<double> point2arr::get_heading() const
{
  vector<double> outarr;
  unsigned int i;
  unsigned int sze = arr.size();
  point2 dpt;
  for (i=0;i<sze-1;++i){
    dpt = arr[i+1]-arr[i];
    outarr.push_back(dpt.heading());
  }
  outarr.push_back(0);
  return outarr;
}

vector<double> point2arr::get_relative_heading() const
{
  unsigned int sze = arr.size();
  vector<double> heading;
  heading = get_heading();  
  vector<double> outarr(sze);

  unsigned int i;
  double dheading;
  for (i=1;i<sze-1;++i){
    dheading = heading[i]-heading[i-1];
    //    while (dheading > M_PI)
    //  dheading -= 2*M_PI;
    // while(dheading< -M_PI)
    //  dth += 2*M_PI;
     dheading = asin(sin(dheading));
    outarr[i] = dheading;
  }
  return outarr;
}


point2arr point2arr::operator+(const point2 &pt) const
{
  point2arr outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i]+pt);
  }
  return outarr;
}

point2arr point2arr::operator-(const point2 &pt) const
{
  point2arr outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i]-pt);
  }
  return outarr;
}


point2arr point2arr::operator*(const double d) const
{
  point2arr outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i]*d);
  }
  return outarr;
}

void point2arr::reverse()
{
  vector<point2> tmp;
  tmp.clear();
  int i;
  int arrsize = (int)this->size();
  for(i=0;i<arrsize;++i){
    tmp.push_back(arr[arrsize-1-i]);
  }
  arr = tmp;
} 

double point2arr::linelength() const
{
  unsigned int i;
  unsigned int arrsize = this->size();
  double len = 0;
  
  for (i = 1;i<arrsize;++i){
    len = len + arr[i-1].dist(arr[i]);
  }
  return len;
}

point2 point2arr::project(const point2& pt) const
{
  unsigned int i;
  unsigned int arrsize = this->size();
  point2 outpt;
  if (arrsize==0){
    outpt= pt;
    return outpt;
  }
  if (arrsize==1){
    outpt= arr[0];
    return outpt;
  }
  line2 thisline;
  double thisdist;
  double mindist;
  for (i = 1;i<size();++i){
    thisline.set(arr[i-1],arr[i]);
    thisdist = thisline.dist(pt);
    if (thisdist<mindist || i==1){
      outpt = thisline.project_bound(pt);
      mindist = thisdist;
    }
  }
  
  return outpt;
}


double point2arr::project_along(const point2& pt) const
{
  unsigned int i;
  unsigned int arrsize = this->size();
  
  if (arrsize==0){
    return 0;
  }
  if (arrsize==1){
    return 0;
  }
  point2 tmppt;
  line2 thisline;
  double thisdist;
  double mindist;
  unsigned int minindex;
  for (i = 1;i<size();++i){
    thisline.set(arr[i-1],arr[i]);
    thisdist = thisline.dist(pt);
    if (thisdist<mindist || i==1){
      tmppt = thisline.project_bound(pt);
      mindist = thisdist;
      minindex = (int)i;
    }
  }
  double totdist = 0;
  for (i = 1;i<minindex;++i){
    totdist = totdist+arr[i-1].dist(arr[i]);
  }
  totdist = totdist+arr[minindex-1].dist(tmppt);
  
  return totdist;
}

int point2arr::closest_index(const point2& pt) const
{
  unsigned int i;
  point2 dpt;
  vector<double> darr;
  darr= dist(pt);
  int minindex = -1;
  double mindist;

  if (darr.size()>0)
    mindist = darr[0];

  for (i = 0; i < darr.size(); ++i){
    if (darr[i]<mindist){
      mindist = darr[i];
      minindex = i;
    }
  }
  return minindex;
}

/** get_index_next returns the lowest index of a point past the given
 * distance along the point2arr.  The distance is measured by adding
 * the points tip to tail.  If the given dist argument is less than
 * zero, then the function returns the highest index of a point which
 * is at least abs(dist) away from the end of the point2arr.
 *
 */

int point2arr::get_index_next(const double dist) const
{

  int i;
  unsigned int arrsize = this->size();
  double len = 0;
  //  int minindex = 0;
  //  double dlen = dist;
  
  if (dist>=0){
    for (i = 1;i<(int)arrsize;++i){
      len = len + arr[i-1].dist(arr[i]);
      // cout << "index = " <<  i << "  len = " << len << "dist = " << dist << endl;
      if (len>dist){
        return i;
      }
      
     
    }
    return arrsize-1; //return maxindex;
  }else{
 return 0; // return min index   
     
  }
  
  return -1; // won't get here
}


/** 
 *  Cuts the front off of the point2arr before the first point that is
 *  at least the given distance from the end of the array, calculated
 *  along the array.
 */

int point2arr::cut_front_at_index(const point2& pt,const double dist)
{
  double project = project_along(pt)+dist;
  int index = get_index_next(project)+1;
  return cut_front_at_index(index);
}


/** 
 *  Cuts the front off of the point2arr at nearest index to the given
 *  point.
 */

//int point2arr::cut_front_at_index(const point2& pt,const int index)
//{
//  return cut_front_at_index(closest_index(pt)+index);
//}

/** 
 *  Cuts the front off of the point2arr which will leave at most
 *  'sizein' points left in the array.  Unchanged if current size is
 *  less than sizein
 */

int point2arr::cut_front_at_index(const int index)
{
  unsigned int arrsize = this->size();
  point2arr ptarrout;
  //double currlen = 0;
  if (arrsize==0)
    return 0;

  //  double thisdist = 0.0;
  
  //  int thisindex = 0;
  //  int currentindex = index;
  point2 dpt,newpt;
  if (index<=0){
    arr = ptarrout.arr;
    return 0; //truncated to size 0
  } 

  ptarrout.push_back(arr[0]); // size = 1


    for (int i=1; i< (int)arrsize;++i){
      if (i>=index){
      //  ptarrout.push_back(arr[i]);
      arr = ptarrout.arr;    
      return arr.size();
    }else{
      ptarrout.push_back(arr[i]);
    }
  }
  arr = ptarrout.arr;
    return arr.size();
}


double point2arr::cut_front(const point2arr& ptarr)
{
  return linelength();
}
double point2arr::cut_front(const point2& pt, const double delta)
{
  return cut_front(project_along(pt)+delta);
}

point2 point2arr::get_point_along(const double length) const{
  unsigned int arrsize = this->size();

  //double currlen = 0;
  if (arrsize==0)
    return point2();
  double currentlength = length;
  double thisdist = 0.0;
  point2 dpt,newpt;
  for (unsigned int i=1; i<size();++i){

    //--------------------------------------------------
    // adding this delta to keep from two points right 
    // on top of each other;
    //--------------------------------------------------
    thisdist = arr[i-1].dist(arr[i]);

    if (thisdist>currentlength){
      dpt = arr[i]-arr[i-1];
      dpt = dpt*currentlength/thisdist;
      newpt = arr[i-1]+dpt;
      return newpt;
    }else{
      currentlength = currentlength-thisdist;
    }

  }

  return arr[arrsize-1];

}
double point2arr::cut_front(const double length)
{
  unsigned int arrsize = this->size();
  point2arr ptarrout;
  //double currlen = 0;
  if (arrsize==0)
    return 0;
  double currentlength = length;
  double thisdist = 0.0;
  point2 dpt,newpt;
  ptarrout.push_back(arr[0]);
  if (length<=0){
    arr = ptarrout.arr;
    return 0;
  }
 

  for (unsigned int i=1; i<size();++i){

    //--------------------------------------------------
    // adding this delta to keep from two points right 
    // on top of each other;
    //--------------------------------------------------
    thisdist = arr[i-1].dist(arr[i]) + length/10000;

    if (thisdist>currentlength){
      dpt = arr[i]-arr[i-1];
      dpt = dpt*currentlength/thisdist;
      newpt = arr[i-1]+dpt;
      ptarrout.push_back(newpt);
      arr = ptarrout.arr;    
      return linelength();
      // }else if (thisdist==currentlength){
      //  ptarrout.push_back(arr[i]);

      //   arr = ptarrout.arr;
      //  return linelength();
    }else{
      currentlength = currentlength-thisdist;
      ptarrout.push_back(arr[i]);
    }

  }
  arr = ptarrout.arr;
  return linelength();
}


/** 
 *  Cuts the back off of the point2arr past the point that is at least
 *  dist away from the given point, calculated along the array.
 * 
 */

int point2arr::cut_back_at_index(const point2& pt,const double dist)
{
  //double project = project_along(pt);
  //int index = get_index_next(project + fabs(dist));

  // cout << "in cut_back_at_index index = " << index << 
  //  " project = " << project << " dist = " << dist << endl;
  // return cut_back_at_index(arr.size()-index);

  double project = project_along(pt)-dist;
  int index= arr.size()-get_index_next(project)+1;
  return cut_back_at_index(index);
}


/** 
 *  Cuts the back off of the point2arr at nearest index to the given
 *  point.
 */

//int point2arr::cut_back_at_index(const point2& pt,const int index)
//{
//  return cut_back_at_index(arr.size()-closest_index(pt)+index);
//}


/** 
 *  Cuts the front off of the point2arr which will leave at most
 *  'sizein' points left in the array.  Unchanged if current size is
 *  less than sizein
 */

int point2arr::cut_back_at_index(const int index)
{
  unsigned int arrsize = this->size();
  point2arr ptarrout;
  //double currlen = 0;
  if (arrsize==0)
    return 0; // size = 0

  //  double thisdist = 0.0;
  
  //  int thisindex = 0;
  //  int currentindex = index;
  point2 dpt,newpt;
  if (index<=0){
    arr = ptarrout.arr;
    return 0; //size = 0
  }

  ptarrout.push_back(arr[arrsize-1]); // size = 1


  int indexcount = 1;
     for (int i=(int)arrsize-2; i>=0;--i){
   if (indexcount>=index){
    //ptarrout.push_back(arr[i]);
      ptarrout.reverse();
      arr = ptarrout.arr;
      return arr.size();
    }else{
      ptarrout.push_back(arr[i]);
    }
    indexcount++;
  }
  ptarrout.reverse();
  arr = ptarrout.arr;
   return arr.size();
} 
double point2arr::cut_back(const point2arr& ptarr)
{
  return linelength();
}
double point2arr::cut_back(const point2& pt, const double delta)
{
  return cut_back(linelength()-project_along(pt)+delta);
}
double point2arr::cut_back(const double length)
{
  unsigned int arrsize = this->size();
  point2arr ptarrout;
  if (arrsize==0)
    return 0;
  
  double currentlength = length;
  double thisdist = 0.0;
  point2 dpt,newpt;
  ptarrout.push_back(arr[arrsize-1]);
  if (length<=0){
    arr = ptarrout.arr;
    return 0;
  }
  for (int i=(int)arrsize-2; i>=0;--i){

    //--------------------------------------------------
    // adding this delta to keep from two points right 
    // on top of each other;
    //--------------------------------------------------  
    thisdist = arr[i+1].dist(arr[i]) + length/10000;
   if (thisdist>currentlength){

     dpt = arr[i]-arr[i+1];
      dpt = dpt*currentlength/thisdist;
      newpt = arr[i+1]+dpt;
      
      ptarrout.push_back(newpt);
      ptarrout.reverse();
      arr = ptarrout.arr;
      return linelength();
      // }else if (thisdist==currentlength){
      //  ptarrout.push_back(arr[i]);
      // ptarrout.reverse();
      // arr = ptarrout.arr;
      //  return linelength();
    }else{
      currentlength = currentlength-thisdist;
      ptarrout.push_back(arr[i]);

    }

  }
  ptarrout.reverse(); 
  arr = ptarrout.arr;
  return linelength();
}

int point2arr::apply_min_ptdist(const double dist){
  point2arr ptarrout;
  double thisdist;
  point2 dpt;

  int startsize = size();
  for (int i=0; i< startsize-1; ++i){
    if (i==0)
      ptarrout.push_back(arr[i]);
    
    dpt = arr[i]-arr[i+1];
    thisdist = dpt.norm();
    if (thisdist > dist){
      ptarrout.push_back(arr[i+1]);
    }
  }
  arr = ptarrout.arr;
  return startsize-arr.size();
  
}

// void point2arr::get_side(const point2& pt,  int& side, double& index)
// {
//   unsigned int i;
//   unsigned int arrsize = this->size();
//   double thiscross, thisdot, thisdist, norm0;
//   point2 pt0;
//   point2 dpt0, dpt1, dpt2;
//   double mindist;
//   double thisindex;
//   int thisside;
//   if (arrsize<2){
//     side = 0;
//     index = 0;
//     return;
//   }

//   dpt2 = pt-arr[0];
//   mindist = dpt2.norm();
//   index = 0;
//   side = 0;

//   for (i=0;i<arrsize-1;++i){   
//     dpt0 = arr[i+1]-arr[i];
//     dpt1 = dpt2;
//     dpt2 = pt-arr[i+1];
//     norm0 = dpt0.norm();
//     thisdot = dpt0.dot(dpt1);
//     thiscross = dpt0.cross(dpt1);

//     if (thiscross>0)
//       thisside = 1;
//     else if (thiscross<0)
//       thisside = -1;
//     else 
//       thisside = 0;

//     if (thisdot>=0 && (thisdot/norm0)<=norm0){
//       thisdist = fabs(thiscross/norm0);
//       thisindex= (double)i+fabs(thisdot/(norm0*norm0));

//     }else if ((thisdot/norm0)>norm0){
//       thisdist = dpt2.norm();
//       thisindex = (double)i+1;
//     }else{
//       thisdist = mindist+1;
//       thisindex = i;
//     }
//     //    cout << "thisindex = " << thisindex 
//     //       << "  thisdist = " << thisdist << endl;
//     if (thisdist<mindist){
//       mindist = thisdist;
//       index = thisindex;
//       side = thisside;
//     }  
//     //  cout << "index = " << index 
//     //         << "  mindist = " << mindist << endl;
//   }
// }

// void point2arr::get_side(const point2& pt,  int& side, double& dist, double&  index)
// {
//   unsigned int i;
//   unsigned int arrsize = this->size();
//   double thiscross, thisdot, thisdist, norm0;
//   point2 pt0;
//   point2 dpt0, dpt1, dpt2;
//   double mindist;
//   double thisindex;
//   int thisside;
//   if (arrsize<2){
//     side = 0;
//     index = 0;
//     return;
//   }

//   dpt2 = pt-arr[0];
//   mindist = dpt2.norm();
//   index = 0;
//   side = 0;

//   for (i=0;i<arrsize-1;++i){   
//     dpt0 = arr[i+1]-arr[i];
//     dpt1 = dpt2;
//     dpt2 = pt-arr[i+1];
//     norm0 = dpt0.norm();
//     thisdot = dpt0.dot(dpt1);
//     thiscross = dpt0.cross(dpt1);
//     thisside = (thiscross ==0) ? 0 : (thiscross<0 ? -1:1);   
//     if (thisdot>=0 && (thisdot/norm0)<=norm0){
//       thisdist = fabs(thiscross/norm0);
//       thisindex= (double)i+fabs(thisdot/(norm0*norm0));

//     }else if ((thisdot/norm0)>norm0){
//       thisdist = dpt2.norm();
//       thisindex = (double)i+1;
//     }else{
//       thisdist = mindist+1;
//       thisindex = i;
//     }
//     //    cout << "thisindex = " << thisindex 
//     //       << "  thisdist = " << thisdist << endl;
//     if (thisdist<mindist){
//       mindist = thisdist;
//       index = thisindex;
//       side = thisside;
//     }  
//     //  cout << "index = " << index 
//     //         << "  mindist = " << mindist << endl;
//   }
//   dist = mindist;
// }


void point2arr::get_side(const point2& pt,  int& side, int& index) const
{
  unsigned int i;
  unsigned int arrsize = this->size();
  double thiscross, thisdot, thisdist, norm0;
  point2 pt0;
  point2 dpt0, dpt1, dpt2;
  double mindist;
  int thisindex;
  int thisside;
  double mindproj, dproj, thisproj;
  if (arrsize<2){
    side = 0;
    index = 0;
    return;
  }
  double epsilon = .00000001;
  dpt2 = pt-arr[0];
  mindist = dpt2.norm();
  index = 0;
  side = 0;
  mindproj = -1;
  dproj = 0;

  for (i=0;i<arrsize-1;++i){   
    dpt0 = arr[i+1]-arr[i];
    dpt1 = dpt2;
    dpt2 = pt-arr[i+1];
    norm0 = dpt0.norm();
    thisdot = dpt0.dot(dpt1);
    thiscross = dpt0.cross(dpt1);
    thisside = (thiscross ==0) ? 0 : (thiscross<0 ? -1:1);   

   
    thisproj = thisdot/norm0;
    // cout << "norm0 = " << norm0
    //     <<" thisPROJ = " << thisproj
    //     <<" thiscross = " << thiscross
    //     <<" thisside = " << thisside 
    //     << endl;
    if (thisproj>=0 && (thisproj)<=norm0){
      //  cout <<"OPTION1"<<endl;
      thisdist = fabs(thiscross/norm0);
      thisindex= i+1;
      dproj = 0; 

    }else if ((thisproj)>norm0){
      // cout <<"OPTION2"<<endl;      
      thisdist = dpt2.norm();
      dproj = thisproj-norm0;
      thisindex = i+2;
    }else if (thisproj<0){
      // cout <<"OPTION2.5"<<endl;      
      thisdist = dpt1.norm();
      dproj = -thisproj;
      thisindex = i;
    }else{
      // cout <<"OPTION3"<<endl;
      thisdist = mindist+1;
      thisindex = i;
    }
    //  cout << "thisindex = " << thisindex 
    //       << "  thisdist = " << thisdist << endl;
    //    cout << "minindex = " << index 
    //       << "  mindist = " << mindist << endl;
    if (thisdist<mindist-epsilon){
      mindist = thisdist;
      index = thisindex;
      side = thisside;
      mindproj = dproj;
      //cout <<"SETMIN mindist="<<mindist 
      //     <<" index=" << index
      //     <<" side=" << side 
      //     << endl;
      
    }else if (thisdist<=(mindist+epsilon) && (dproj<mindproj || mindproj<0)){
      //cout << "EEEEEEEEQUALITY " << endl;
      mindist = thisdist;
      index = thisindex;
      side = thisside;
      mindproj = dproj;
      // cout <<"SETMIN mindist="<<mindist 
      //     <<" index=" << index
      //     <<" side=" << side 
      //     << endl;
      
    }  
    
    
    //--------------------------------------------------
    // return index = -1, 0, 1 for notprojected, 
    //--------------------------------------------------
  
//  cout << "index = " << index 
    //         << "  mindist = " << mindist << endl;
  }
  if (index==0)
      index=-1;
    else if (index>=(int)size())
      index=1;
    else
      index = 0;
}


void point2arr::get_side(const point2arr& ptarr,  int& side) const
{

  int i;
  
  int thissize = ptarr.size();
  point2 thispt;
  //  int thisside =0;
  //  int thisindex =0;

  int tmpside,tmpindex;
  int rightcount = 0;
  int leftcount = 0;

  for (i=0;i<thissize;++i){
    thispt = ptarr[i];
    get_side(thispt,tmpside,tmpindex);
    if (tmpindex==0){
      if (tmpside==1){
        rightcount++;
      }
      else if (tmpside==-1){
        leftcount++;
      }
    }
  }

  if (leftcount>rightcount){
    side = -1;
    if (rightcount>0){
      cerr << " in point2arr::get_side(point2arr) warning - some conflicting informaiton about sidedness" << endl; 
    }
    
  }else if (rightcount>leftcount){
    side = 1;
    if (leftcount>0){
      cerr << " in point2arr::get_side(point2arr) warning - some conflicting informaiton about sidedness" << endl; 
    }
  }else{
    side = 0;
  }
  
}


// void point2arr::get_correspondence_onetoone(const point2arr& ptarr, vector<int>& c1, vector<int>& c2) const{

//   vector<int> index1(size(),-1);
//   vector<int> index2(ptarr.size(),-1);

//   vector<double> dist1(size(),-1);
//   vector<double> dist2(ptarr.size(),-1);

//   double dist;
//   int i,j;

//   int thissize = size();
//   int thatsize = ptarr.size();
//   point2 thispt, thatpt;

//   for (i=0;i<thissize;++i){
//     thispt = arr[i];
//     for (j=0;j<thatsize;++j){
//       thatpt = ptarr[j];
//       dist = thispt.dist(thatpt);
//       if (dist1[i]<0 || dist<dist1[i]){
//         dist1[i] = dist;
//         index1[i] = j;
//       }
//       if (dist2[j]<0 || dist<dist2[j]){
//         dist2[j] = dist;
//         index2[j] = i;
//       }
      
//     }
//   }
//   c1.clear();
//   c2.clear();

//   for (i=0;i<thissize;++i){
//     if (index2[index1[i]] == i){
//       c1.push_back(i);
//       c2.push_back(index1[i]);
//     }
//   }

// }




void point2arr::connect(const point2arr &ptarr)
{
  
  // unsigned int sizeA = size();
  unsigned int sizeB = ptarr.size();

  //  if (sizeA ==0){
  //     arr = ptarr.arr;
  //     return;
  //   }

  //   if (sizeB ==0)
  //     return;

  //   // temp to handl pt balance;
  //   point2 cpt;
  //   cpt = 0.5*(arr[sizeA-1]+ptarr[0]);
  //   arr.push_back(cpt);
  for (unsigned int i =0;i<sizeB;++i){
    arr.push_back(ptarr[i]);
  }
}



void point2arr::connect_intersect(const point2arr &ptarr)
{
  
  unsigned int sizeA = size();
  unsigned int sizeB = ptarr.size();

  //cout << sizeA << " " << sizeB << endl;

  if (sizeB==0)
    return;

  if (sizeA<=1 || sizeB==1){
    connect(ptarr);
    return;
  }
  

  line2 lineAend(arr[sizeA-2],arr[sizeA-1]);
  line2 lineBbeg(ptarr[0],ptarr[1]);
  point2 newpt;
  newpt = lineAend.intersect(lineBbeg);
  unsigned int startindex = 0;

  if (lineBbeg.is_cross(lineAend)){
    //cout << "INTERSECTING1" << endl;
    arr[sizeA-1] = newpt;
  }else {
    arr.push_back(newpt);
    //cout << "ADDING" << endl;
  }
 
  if (lineAend.is_cross(lineBbeg)){
    //cout << "INTERSECTING2" << endl;
    startindex = 1;
  } 


  for (unsigned int i=startindex;i<sizeB;++i){
    arr.push_back(ptarr[i]);
  }
}

void point2arr::set_extend(const double dist)
{
  int sze = size();
  if (sze<2)
    return ;
  
  point2 pta,ptb, dpt,newpt;
  double thisdist;
  if (dist<0){
    pta = arr[0];
    ptb = arr[1];
    dpt = ptb-pta;
    thisdist = dpt.norm();
    newpt = pta+dpt*dist/thisdist;
    insert(0,newpt);
  }else{
    pta = arr[sze-1];
    ptb = arr[sze-2];
    dpt = ptb-pta;
    thisdist = dpt.norm();
    newpt = pta-dpt*dist/thisdist;
    arr.push_back(newpt);
   }

}

bool point2arr::is_intersect(const point2arr &ptarr)const
{
   int sizeA = (int)size();
   int sizeB = (int)ptarr.size();

   // check sizes first
   if (sizeA<=1 || sizeB<=1){
     return false;
   }
   int i,j;
   line2 lineA,lineB;

   for (i=1;i<sizeA;++i){
     lineA.set(arr[i-1],arr[i]);
     for (j=1;j<sizeB;++j){
       lineB.set(ptarr[j-1],ptarr[j]);
       if (lineA.is_intersect(lineB)){
         return true;
       }
     }
   }
   
   return false;

}

//algorithm taken from:
// local.wasp.uwa.edu.au/~pbourke/geometry/insidepoly
bool point2arr::is_inside_poly(const point2& pt) const
{
  double theta = 0;
  point2 delta1; //vector from input pt to ptA of seg
  point2 delta2; //vector from input pt to ptB of seg

  unsigned int i;
  unsigned int sizearr = this->size();
  if (sizearr<=2){
    return false;
  }

  for(i=0; i<sizearr; i++) {
    delta1 = arr[i] - pt;
    delta2 = arr[(i+1)%sizearr] - pt;
    theta += angle2d(delta1, delta2);
   
  }
 theta = fabs(theta); 

 while (theta > 3*M_PI)
   theta -= 2*M_PI;

    if(theta < M_PI) //theta == 0 means pt outside
    return false;

  return true;

}

bool point2arr::is_poly_overlap(const point2arr &ptarr)const
{
   unsigned int i;
  unsigned int thissize = this->size();
  unsigned int othersize = ptarr.size();

  for(i=0;i<othersize;i++) {
    if(this->is_inside_poly(ptarr[i]))
      return true;
  }
  for(i=0;i<thissize;i++) {
    if (ptarr.is_inside_poly(arr[i]))
      return true;
  }
  return is_intersect(ptarr);
} 

bool point2arr::is_line_poly_overlap(const point2arr &ptarr)const
{
  unsigned int i;
  unsigned int thissize = this->size();
  for(i=0;i<thissize;i++) {
    if(ptarr.is_inside_poly(arr[i]))
      return true;
  }
  return is_intersect(ptarr);
} 

bool point2arr::is_poly_line_overlap(const point2arr &ptarr)const
{
  unsigned int i;
  unsigned int othersize = ptarr.size();
  for(i=0;i<othersize;i++) {
    if(this->is_inside_poly(ptarr[i]))
      return true;
  }
  return is_intersect(ptarr);
} 

bool point2arr::is_poly_point_overlap(const point2arr &ptarr)const
{
   unsigned int i;
  unsigned int othersize = ptarr.size();
  int inside;

  for(i=0;i<othersize;i++) {
    inside = this->is_inside_poly(ptarr[i]);
    if(inside == 1) {
            return true;
    }
  }
  return false;
} 
bool point2arr::is_point_poly_overlap(const point2arr &ptarr)const
{
   unsigned int i;
  unsigned int thissize = this->size();
  for(i=0;i<thissize;i++) {
    if (ptarr.is_inside_poly(arr[i]))
             return true;
  }
  return false;
}


int point2arr::set_arc_curve(const point2 startpt, const double startang, const point2 endpt, const double endang, const int numpts)
{
  line2 startline, endline;
  point2 startpt2, endpt2;
  double d = 1000* startpt.dist(endpt);

  startpt2 = startpt + point2(d*cos(startang),d*sin(startang));

  endpt2 = endpt - point2(d*cos(endang),d*sin(endang));

  startline.set(startpt,startpt2);
  endline.set(endpt,endpt2);

  if (!startline.is_intersect(endline)){
    clear();
    return -1;
  }

  point2 midpt;
  midpt = startline.intersect(endline);

  set_arc_curve(startpt,midpt,endpt,numpts);
  return 0;
  

}

void point2arr::set_arc_curve(const point2 startpt, const point2 arcpt, const point2 endpt, const int numpts)
{
  
  clear();
  //  push_back(startpt);

  if (numpts <=0){
    // push_back(endpt);
    return;
  }
   
  point2arr e1(numpts+1);
  point2arr e2(numpts+1);
  point2 tmppt1,tmppt2,dpt1,dpt2;
  point2 newpt;
  line2 l1;
  line2 l2;

  dpt1 = (arcpt-startpt)/(numpts+1);
  dpt2 = (endpt-arcpt)/(numpts+1);

  tmppt1 = startpt;
  tmppt2 = arcpt+dpt2;
  l1.set(tmppt1,tmppt2);
  
  
  for (int i = 0; i<(numpts);++i){
    tmppt1= tmppt1+dpt1;
    tmppt2= tmppt2+dpt2;
    l2.set(tmppt1,tmppt2);
    newpt = l1.intersect(l2);
    push_back(newpt);
    l1=l2;
  }

  //  push_back(endpt);
  
  
}
// point2arr point2arr::get_intersect(const point2arr &ptarr)
// {
//   unsigned int sizeA = size();
//   unsigned int sizeB = ptarr.size();
  
//   point2arr outarr;
    
//   if (sizeA>1 & sizeB>1){
//     point2 ptA0,ptA1,ptB0,ptB1;
//     point2 ptA0_rot,ptA1_rot,ptB0_rot,ptB1_rot;
//     point2 dptA, dptB_rot,newpt, newpt_rot;
//     double angA;

//     ptA0 = arr[0];
//     ptA1 = arr[1];

//     ptB0 = ptarr[0];
//     ptB1 = ptarr[1];
    
//     dptA = ptA1-ptA0;
//     angA = dptA.heading();
    
//     ptA0_rot = ptA0.rot(-angA);
//     ptA1_rot = ptA1.rot(-angA);
//     ptB0_rot = ptB0.rot(-angA);
//     ptB1_rot = ptB1.rot(-angA);

//     dptB_rot = ptB1_rot-ptB0_rot;
//     newpt_rot.y = ptA0_rot.y;
//     if (dptB_rot.y!=0){
//       newpt_rot.x = (newpt_rot.y-ptB0_rot.y)*
//         dptB_rot.x/dptB_rot.y+ptB0_rot.x;
//       newpt = newpt_rot.rot(angA);
//       outarr.push_back(newpt);
//     }

//   }

//   return outarr;
// }


/// Trims out any loops in the line from start to finish. 
/// Overkill for some situations
/// minsize is a hack to maintain large loops for now
int point2arr::set_trim_line_loops(double minsize)
{
  int sze = size();
  if (sze<4){
    return 0;
  }
  int i,j;
  int trimto = -1;
  line2 l1;
  line2 l2;
  point2 newpt;
  point2arr looparr;
  point2 maxpt, minpt;
  int trimnum = 0;
  point2 tmppt;
  double len, wid, orient;
  for (i = 0;i<(sze-1);++i){
    if (i<trimto){
      arr[i]  = newpt;
      trimnum++;
      continue;
    }

    l1.set(arr[i],arr[i+1]);
    
    for (j=i+2;j<(sze-1);++j){
      l2.set(arr[j],arr[j+1]);
      if (l1.is_intersect(l2)){
        //cout << "intersect index = " << i 
        //     << " and " << j << endl;
        newpt = l1.intersect(l2);
        //cout <<"intersect pt = " << newpt << endl;
       
        //--------------------------------------------------
        // analyze loop size
        //--------------------------------------------------
        looparr = get(i+1,j);
        looparr.get_bound_box(tmppt,len,wid,orient);
        // cout << "wid = " << wid << " len = " << len << endl;
        if (wid<minsize){
          trimto = j+1;
          break;
        }

        
      }
    }
  }
  return trimnum;
}

point2arr point2arr::get(const int index1, const int index2) const
{
  point2arr outarr;
  for (int i = index1;i<=index2;++i){
    outarr.push_back(arr[i]);
  }
  return outarr;
}

point2arr point2arr::get_offset(double d)
{
  point2arr outarr;
  unsigned int arrsize = this->size();
  unsigned int i;
  point2 pt0, pt1;
  point2 offpt0A, offpt1A, offpt0B, offpt1B;
  double angB;
  point2 dptA, dptB, newpt;
 
  if (arrsize>=2){
    pt0 = arr[0];
    pt1 = arr[1];
    dptB = pt1-pt0;
    angB = dptB.heading();
      
    offpt0B = pt0.rot(-angB);
    offpt1B = pt1.rot(-angB);
    offpt0B.y = offpt0B.y+d;
    offpt1B.y = offpt1B.y+d;
    offpt0B = offpt0B.rot(angB);
    offpt1B = offpt1B.rot(angB);
    
    outarr.push_back(offpt0B);

    for (i=1;i<arrsize-1;++i){

      offpt0A = offpt0B;
      offpt1A = offpt1B;

      pt0 = pt1;
      pt1 = arr[i+1];

      dptB = pt1-pt0;
      angB = dptB.heading();
      
      offpt0B = pt0.rot(-angB);
      offpt1B = pt1.rot(-angB);
      offpt0B.y = offpt0B.y+d;
      offpt1B.y = offpt1B.y+d;

      offpt0A = offpt0A.rot(-angB);
      offpt1A = offpt1A.rot(-angB);

      dptA = offpt1A-offpt0A;



      newpt.y = offpt0B.y;

    
      if (fabs(dptA.y) <.000001){
        newpt.x = offpt0B.x;
      }else{
        newpt.x = (newpt.y-offpt1A.y)*
          dptA.x/dptA.y+offpt1A.x;
      }
    
      newpt = newpt.rot(angB);

      outarr.push_back(newpt);

      offpt0B = offpt0B.rot(angB);
      offpt1B = offpt1B.rot(angB);
          
      
    
      
      
    }
    outarr.push_back(offpt1B);
  }
  
  // outarr.set_trim_line_loops();
  
  return outarr;
  
}

point2arr point2arr::get_offset_trim(double d)
{
  point2arr outarr;
  outarr = get_offset(d);
  outarr.set_trim_line_loops(2*d);
  
  return outarr;
  
}



point2 point2arr::min() const{
  unsigned int arrsize = this->size();
  double minx;
  double miny;
  for (unsigned int i = 0; i < arrsize; ++i){
    if (arr[i].x< minx || i==0)
      minx = arr[i].x;
    if (arr[i].y< miny || i==0)
      miny = arr[i].y;
  }
  return point2(minx,miny);
}

point2 point2arr::max() const{
  unsigned int arrsize = this->size();
  double maxx;
  double maxy;
  for (unsigned i = 0; i < arrsize; ++i){
    if (arr[i].x> maxx || i==0)
      maxx = arr[i].x;
    if (arr[i].y> maxy || i==0)
      maxy = arr[i].y;
  }
  return point2(maxx,maxy);
}

vector<point2arr> point2arr::split(const double d)
{
  vector<point2arr> ptarrout;

  point2arr ptarr;
    
  if (size()==0)
    return ptarrout;
  double dist;
    ptarr.push_back(arr[0]);
  for(unsigned i = 1; i< size();++i){
    dist = arr[i].dist(arr[i-1]);
    if (dist>d){
      ptarrout.push_back(ptarr);
      ptarr.clear();
    }
    ptarr.push_back(arr[i]);
  }
  if (ptarr.size()>0)
    ptarrout.push_back(ptarr);


  return ptarrout;
}


  point2arr operator*(const double d,const point2arr &ptarr)
    {
      return ptarr*d;
    }



point2 point2arr::average()
{
    double sumx = 0;
    double sumy = 0;
    unsigned int i;
    unsigned int sizearr = arr.size();
    for(i=0; i<sizearr; i++)
    {
        sumx += arr[i].x;
        sumy += arr[i].y;
    }    
    double meanx, meany;
    meanx = sumx/sizearr;
    meany = sumy/sizearr;

    return point2(meanx, meany);
}


void point2arr::split_at(int num, point2arr& arr1, point2arr& arr2) 
{
  arr1.clear();
  arr2.clear();
  for(int i=0; i< num; i++) {
    arr1.push_back(arr[i]);
  } 
  for(unsigned int j = num; j < arr.size(); j++) {
    arr2.push_back(arr[j]);
  }

}

//returns sum(x_i*y_i)
void point2arr::sum_squares(double& ssxx, double& ssyy, double& ssxy)
{
    unsigned int i;
    unsigned int sizearr = this->size();
    double xy = 0;
    double xx = 0;
    double yy = 0;
    for(i = 0; i<sizearr; i++)
    {
        xy += arr[i].x * arr[i].y;
        xx += arr[i].x * arr[i].x;
        yy += arr[i].y * arr[i].y;
    }
    ssxx = xx;
    ssyy = yy;
    ssxy = xy;
    return;
}

double point2arr::fit_line(double& a, double& b, point2& pt1, point2& pt2)
{
    point2 centerpoint;
    double ssxx, ssyy, ssxy;            
    unsigned int sizearr;
    double B, b1, b2, a1, a2;
    double err1, err2, MSE;

    sizearr = this->size();
    centerpoint = this->average();

    this->sum_squares(ssxx, ssyy, ssxy);

    double grr1 = ssyy - sizearr*centerpoint.y*centerpoint.y;
    double grr2 = ssxx - sizearr*centerpoint.x*centerpoint.x;
    double grr3 = sizearr*centerpoint.x*centerpoint.y - ssxy;

    B = .5*(grr1 - grr2)/grr3;

    b1 = -B + sqrt(1+B*B);
    a1 = centerpoint.y - b1*centerpoint.x;

    b2 = -B - sqrt(1+B*B);
    a2 = centerpoint.y - b2*centerpoint.x;

    err1 = this->line_error(a1,b1);
    err2 = this->line_error(a2,b2);

    if(err1 < err2) {
      a = a1;
      b = b1;
      MSE = err1;
    } else {
      a = a2;
      b = b2;
      MSE = err2;
    }

    point2 p1, p2, temppt;
    line2 l1;

    p1 = point2(-1, a-b);
    p2 = point2(1, a+b);
    l1 = line2(p1,p2);

    pt1 = l1.project(arr[0]);
    pt2 = l1.project(arr[sizearr-1]);

    return MSE;
} 

//#warning "this is still untested!"

double point2arr::fit_line_perp(double& a, double& b, point2& pt1, point2& pt2, double slope)
{
    point2 centerpoint;
    unsigned int sizearr;
    double MSE;

    sizearr = this->size();
    centerpoint = this->average();

    b = -1/slope;
    a = centerpoint.y - b*centerpoint.x;

    MSE = this->line_error(a,b);

    point2 p1, p2, temppt;
    line2 l1;

    p1 = point2(-1, a-b);
    p2 = point2(1, a+b);
    l1 = line2(p1,p2);

    pt1 = l1.project(arr[0]);
    pt2 = l1.project(arr[sizearr-1]);

    return MSE;
} 


double point2arr::line_error(double a, double b)
{
  double SE = 0;
  double err;
  unsigned int i;
  unsigned int sizearr = this->size();

  for(i=0; i<sizearr; i++) {
    err = pow((arr[i].y - a - b*arr[i].x),2)/(1+b*b);
    SE += err;
  }

  return SE/sizearr;
}

//algorithm taken from:
// local.wasp.uwa.edu.au/~pbourke/geometry/insidepoly
int point2arr::get_inside(const point2& pt) const
{
  double theta = 0;
  point2 delta1; //vector from input pt to ptA of seg
  point2 delta2; //vector from input pt to ptB of seg

  unsigned int i;
  unsigned int sizearr = this->size();

  for(i=0; i<sizearr-1; i++) {
    delta1 = arr[i] - pt;
    delta2 = arr[i+1] - pt;
    theta += angle2d(delta1, delta2);
    }

    if(fabs(theta) < 1) //theta == 0 means pt outside
    return -1;

  return 1;

}

void point2arr::remove(int index)
{
  arr.erase(arr.begin()+index);
}

void point2arr::add_element(point2 pt, int index)
{
  arr.insert(arr.begin()+index,sizeof(point2),pt);
}

void point2arr::merge(const point2arr ptarr)
{
  unsigned int i;
  unsigned int sizearr = ptarr.size();
  int inside, index;
  for(i=0; i<sizearr; i++) {
    inside = this->get_inside(ptarr[i]);
    index = this->get_closest_side(ptarr[i]);
    if(-1 == inside) {
      this->add_element(ptarr[i],index+1);
    }
  }
  return;
}

int point2arr::get_closest_side(const point2 pt) const
{
  int index = 0;

  line2 thisside;
  double thisdist, mindist;

  unsigned int i;
  unsigned int sizearr = this->size();

  if (sizearr<2){
    index = 0;
    return 0;
  }

  mindist = pt.dist(arr[0]);

  for(i=0; i<sizearr-1; i++) {
    thisside = line2(arr[i],arr[i+1]);
    thisdist = thisside.dist(pt);
    if(thisdist < mindist) {
      mindist = thisdist;
      index = i;
    }
  }

  return index;
}

int point2arr::get_overlap(const point2arr ptarr) const
{
  unsigned int i;
  unsigned int thissize = this->size();
  unsigned int othersize = ptarr.size();
  int numOverlap = 0;
  int inside;

  for(i=0;i<othersize;i++) {
    inside = this->get_inside(ptarr[i]);
    if(inside == 1) {
      numOverlap++;
    }
  }
  for(i=0;i<thissize;i++) {
    inside = ptarr.get_inside(arr[i]);
    if(inside == 1) {
      numOverlap++;
    }
  }

  return numOverlap;

}

void point2arr::get_bound_box(point2 &cpt, double &len, double &wid, double &orientation) const
{
 point2arr  ptarrup, ptarrdn,ptarr;
  
  ptarr = arr;

  
  vector<double> dangarr;
  dangarr.push_back(M_PI/4);
  dangarr.push_back(M_PI/8);
  dangarr.push_back(M_PI/16);
  dangarr.push_back(M_PI/32);
  dangarr.push_back(M_PI/64);
  dangarr.push_back(M_PI/128);

  point2 maxup,maxdn,minup,mindn, maxpt,minpt;
  point2 lwup,lwdn,initlw;
  double thisang =0;
  double dang;
  double minarea;
  double areaup, areadn;
  point2 thismin, thismax;

  maxup = ptarrup.max();
  maxdn = ptarrdn.max();
  minup = ptarrup.min();
  mindn = ptarrdn.min();
  lwup = maxup-minup;
  lwdn = maxdn-mindn;

  maxpt = ptarr.max();
  minpt = ptarr.min();
  initlw = maxpt-minpt;
  minarea = initlw.x*initlw.y;
   thismax = maxpt;
  thismin = minpt;
  for (unsigned i = 0; i< dangarr.size(); ++i){
    dang = dangarr[i];
    ptarrup = ptarr.rot(dang);
    ptarrdn = ptarr.rot(-dang);
    maxup = ptarrup.max();
    maxdn = ptarrdn.max();
    minup = ptarrup.min();
    mindn = ptarrdn.min();
    lwup = maxup-minup;
    lwdn = maxdn-mindn;
    
   
    areaup = lwup.x*lwup.y;
    areadn = lwdn.x*lwdn.y;
 
    if (areaup < minarea && areaup < areadn){
      thisang = thisang - dang;
      ptarr = ptarrup;
      minarea = areaup;
      thismin = minup;
      thismax = maxup;
    }else if(areadn < minarea && areadn < areaup) {
      thisang = thisang + dang;
      ptarr = ptarrdn;
      minarea = areadn;
      thismin = mindn;
      thismax = maxdn;

    }

  }
  orientation = thisang;
  len = thismax.x-thismin.x;
  wid = thismax.y-thismin.y;
  cpt = 0.5*(thismax+thismin);
  cpt = cpt.rot(thisang);
}

point2arr point2arr::get_bound_box() const
{
  point2arr ptarrout, ptarrup, ptarrdn,ptarr;
  
  ptarr = arr;

  
  vector<double> dangarr;
  dangarr.push_back(M_PI/4);
  dangarr.push_back(M_PI/8);
  dangarr.push_back(M_PI/16);
  dangarr.push_back(M_PI/32);
  dangarr.push_back(M_PI/64);
  dangarr.push_back(M_PI/128);

  point2 maxup,maxdn,minup,mindn, maxpt,minpt;
  point2 lwup,lwdn,initlw;
  double thisang =0;
  double dang;
  double minarea;
  double areaup, areadn;
  point2 thismin, thismax;

  maxup = ptarrup.max();
  maxdn = ptarrdn.max();
  minup = ptarrup.min();
  mindn = ptarrdn.min();
  lwup = maxup-minup;
  lwdn = maxdn-mindn;

  maxpt = ptarr.max();
  minpt = ptarr.min();
  initlw = maxpt-minpt;
  minarea = initlw.x*initlw.y;
  thismax = maxpt;
  thismin = minpt;
  
  for (unsigned i = 0; i< dangarr.size(); ++i){
    dang = dangarr[i];
    ptarrup = ptarr.rot(dang);
    ptarrdn = ptarr.rot(-dang);
    maxup = ptarrup.max();
    maxdn = ptarrdn.max();
    minup = ptarrup.min();
    mindn = ptarrdn.min();
    lwup = maxup-minup;
    lwdn = maxdn-mindn;
    
    areaup = lwup.x*lwup.y;
    areadn = lwdn.x*lwdn.y;
 
    if (areaup < minarea && areaup < areadn){
      thisang = thisang - dang;
      ptarr = ptarrup;
      minarea = areaup;
      thismin = minup;
      thismax = maxup;
    }else if(areadn < minarea && areadn < areaup) {
      thisang = thisang + dang;
      ptarr = ptarrdn;
      minarea = areadn;
      thismin = mindn;
      thismax = maxdn;

    }

  }

  point2 tmp;
  tmp.set(thismin.x,thismin.y);
  ptarrout.push_back(tmp);
  tmp.set(thismin.x,thismax.y);
  ptarrout.push_back(tmp);
  tmp.set(thismax.x,thismax.y);
  ptarrout.push_back(tmp);
  tmp.set(thismax.x,thismin.y);
  ptarrout.push_back(tmp);


  point2 lw = thismin-thismax;
  //if (lw.x < lw.y){
  ptarrout = ptarrout.rot(thisang);
  //}else {
  //  ptarrout = ptarrout.rot(thisang-M_PI/2);
  //}


  return ptarrout;
}


ostream &operator<<(ostream &os, const point2arr &ptarr) {
  unsigned int i;
  
  for (i=0;i<ptarr.size();++i){
    os <<endl << "[" << i << "] "  << ptarr[i];
  }
  return os;
}


ostream &operator<<(ostream &os, const vector<double> &vec) {
  unsigned int i;
  
  for (i=0;i<vec.size();++i){
    os <<endl << "[" << i << "] "  << vec[i];
  }
  return os;
}

ostream &operator<<(ostream &os, const vector<int> &vec) {
  unsigned int i;
  
  for (i=0;i<vec.size();++i){
    os <<endl << "[" << i << "] "  << vec[i];
  }
  return os;
}

