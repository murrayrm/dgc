/**********************************************************
 **
 **  POSE2.HH
 **
 **    Time-stamp: <2007-02-12 09:22:56 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 13:13:07 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef POSE2_H
#define POSE2_H


#include <math.h>

using namespace std;

typedef struct
{
  double x, y, ang;
} pose2;

#endif
