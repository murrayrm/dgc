#ifndef ELLIPSE_HH
#define ELLIPSE_HH


#include <math.h>
#include <vector>
#include <iostream>
#include "point2.hh"

#define MAXDIFF .01

using namespace std;

class ellipse
{
public:
  //total number of ladar returns contributing to this ellipse
  int numPoints;

  //average of all points contributing to ellipse
  point2 center;

  //sum_i(x_i*x_i), sum_i(y_i*y_i), sum_i(x_i*y_i)
  double ssxx, ssyy, ssxy;

  //major and minor axes of ellipse
  double a,b;

  //rotation required to diagonalize cov matrix
  double theta;

  //initializes an empty ellipse
  ellipse(): numPoints(0), ssxx(0), ssyy(0), ssxy(0), a(0), b(0), theta(0) {}

  //initializes ellipse w/ points in ptarr
  ellipse(point2arr ptarr);

  ~ellipse() {}



  //add the points in ptarr to the ellipse 
  void add_points(point2arr ptarr);

  //merge the two ellipses
  void merge(ellipse newEllipse);

  //calculates a,b,theta
  void finish();

  //returns point2arr outline of ellipse
  point2arr el_border(double margin);

  //returns 1 if input pt is inside ellipse, -1 otherwise
  int inside(point2 pt, double margin);

  //returns 1 if overlap, -1 otherwise
  //for now, only checks if either center is inside other ellipse
  int overlap(ellipse* newEllipse, double margin);

};

#endif
