/**********************************************************
 **
 **  POSECOV2.HH
 **
 **    Time-stamp: <2007-02-08 13:57:45 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 13:14:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef POSECOV2_HH
#define POSECOV2_HH


#include <math.h>
#include "pose2.hh"

typedef struct
{
	double x, y, ang;
  double major_var, minor_var, axis;
	double ang_var, axis_x, axis_y;
} posecov2;

#endif
