/*
 * Copyright 2005, by the California Institute of Technology. ALL
 * RIGHTS RESERVED. United States Government Sponsorship
 * acknowledged. Any commercial use must be negotiated with the Office
 * of Technology Transfer at the California Institute of Technology.
 *
 * This software may be subject to U.S. export control laws. By
 * accepting this software, the user agrees to comply with all
 * applicable U.S. export laws and regulations. User has the
 * responsibility to obtain export licenses, or other export authority
 * as may be required before exporting such information to foreign
 * countries or providing access to foreign persons.
 */

/* Desc: Structures and operators for 3D vectors
 * Author: Andrew Howard
 * Date: 5 Oct 2006
 * CVS: $Id: vec3.h,v 1.6 2006/10/23 15:19:01 abhoward Exp $
 */

/** @file 

@brief 3-vector structures and operators.

@sa quat.h pose3.h

*/

#ifndef VEC3_H
#define VEC3_H

#include <math.h>


/// @brief 3-vector of doubles.
typedef struct
{
  double x, y, z;
} vec3_t;


/// @brief Return a zero vector.
static __inline__ vec3_t vec3_zero()
{
  vec3_t a = {0, 0, 0};
  return a;
}


/// @brief Set vector elements.
///
/// @returns Returns the vector @f$ (x, y, z) @f$.
static __inline__ vec3_t vec3_set(double x, double y, double z)
{
  vec3_t a = {x, y, z};
  return a;
}


/// @brief Set vector elements from an array of doubles.
///
/// @returns Returns the vector @f$ (v[0], v[1], v[2]) @f$.
static __inline__ vec3_t vec3_setv(double *v)
{
  vec3_t a = {v[0], v[1], v[2]};
  return a;
}


/// @brief Set vector elements from an array of floats.
///
/// @returns Returns the vector @f$ (v[0], v[1], v[2]) @f$.
static __inline__ vec3_t vec3_setvf(float *v)
{
  vec3_t a = {v[0], v[1], v[2]};
  return a;
}


/// @brief Test for finite vector.
///
/// Tests that all the components are valid and finite.
/// @param[in] a Vector to test.
/// @return Returns non-zero if the vector is finite.
static __inline__ int vec3_finite(vec3_t a)
{
  return finite(a.x) && finite(a.y) && finite(a.z);
}


/// @brief Add vectors.
///
/// @param[in] a, b Vectors to add.
/// @returns Returns the sum @f$ a + b @f$
static __inline__ vec3_t vec3_add(vec3_t a, vec3_t b)
{
  vec3_t c = {a.x + b.x, a.y + b.y, a.z + b.z};
  return c;
}


/// @brief Subtract vectors.
///
/// @param[in] a, b Vectors to subtract.
/// @returns Returns the difference @f$ a - b @f$
static __inline__ vec3_t vec3_sub(vec3_t a, vec3_t b)
{
  vec3_t c = {a.x - b.x, a.y - b.y, a.z - b.z};
  return c;
}


/// @brief Take inner (dot) product of vectors.
///
/// @param[in] a, b Vectors to multiply..
/// @returns Returns the scalar dot product @f$ a \cdot b @f$
static __inline__ double vec3_dot(vec3_t a, vec3_t b)
{
  return a.x*b.x + a.y*b.y + a.z*b.z;
}


/// @brief Take outer (cross) product of vectors.
///
/// @param[in] a, b Vectors to multiply..
/// @returns Returns the vector cross product @f$ a \times b @f$
static __inline__ vec3_t vec3_cross(vec3_t a, vec3_t b)
{
  vec3_t c;
  c.x =  a.y*b.z - a.z*b.y;
  c.y = -a.x*b.z + a.z*b.x;
  c.z =  a.x*b.y - a.y*b.x;
  return c;
}


/// @brief Multiple vector with scalar.
///
/// @param[in] s Scalar multiplier
/// @param[in] a Vector.
/// @returns Returns the vector @f$ b = (s a_x, s a_y, s a_z) @f$
static __inline__ vec3_t vec3_scale(double s, vec3_t a)
{
  vec3_t b = {s*a.x, s*a.y, s*a.z};
  return b;
}


/// @brief Compute the magnitude (modulus) of a vector
///
/// @param a Input vector @f$ a @f$
/// @returns Returns the scalar @f$ b = |a| = \sqrt{a \cdot a} @f$
static __inline__ double vec3_mag(vec3_t a)
{
  return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}


/// @brief Compute the unit vector
///
/// @param a Input vector @f$ a @f$
/// @returns Returns the unit vectpr @f$ b = a / |a| @f$
static __inline__ vec3_t vec3_unit(vec3_t a)
{
  double s = sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
  vec3_t c = {a.x/s, a.y/s, a.z/s};
  return c;
}

#endif
