/**********************************************************
 **
 **  POSE2_UNCERTAIN.HH
 **
 **    Time-stamp: <2007-02-12 09:23:21 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 13:14:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef POSE2_UNCERTAIN_HH
#define POSE2_UNCERTAIN_HH


#include <math.h>
#include "pose2.hh"

using namespace std;

typedef struct
{
	double x, y, ang;
  double major_var, minor_var, axis;
	double ang_var, axis_x, axis_y;
} pose2_uncertain;

#endif
