/**********************************************************
 **
 **  POINT2.CC
 **
 **    Time-stamp: <2007-04-24 20:31:47 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 27 10:52:43 2007
 **
 **
 **********************************************************
 **
 **   Simple 2D point class
 **
 **********************************************************/

#include "point2.hh"


using namespace std;

#define INF 1000



point2 operator*(const double d, const point2& pt)
{
  return pt*d;
}
istream &operator>>(istream &is, point2 &pt) {
  is >> pt.x >> pt.y; 
  return is;
}
ostream &operator<<(ostream &os, const point2 &pt) {
  os << "(" << pt.x << ", " << pt.y << ")";
  return os;
}

ostream &operator<<(ostream &os, const line2 &line) {
  os << "[" << line.ptA << "," << line.ptB << "]" << endl;
  return os;
}

point2 &point2::operator=(const point2_uncertain &pt){
  x = pt.x;
  y = pt.y;
    
  return *this;
}


point2 point2::operator+(const point2_uncertain &pt) const{
  return point2(x+pt.x,y+pt.y);
}

point2 point2::operator-(const point2_uncertain &pt) const{
  return point2(x-pt.x,y-pt.y);
}


bool point2::operator==(const point2_uncertain &pt) const
{
  return ((x==pt.x)&&(y==pt.y));
}
bool point2::operator!=(const point2_uncertain &pt) const
{
  return ((x!=pt.x)||(y!=pt.y));
}

unsigned int point2arr::insert(unsigned int n, point2 &pt)
{
  unsigned int index=n;
  if (n > arr.size())
    index = arr.size();
  arr.insert(arr.begin()+index,pt);
  
  return index;
  
} 

point2 &point2arr::operator[](unsigned int n)
{
  if (n>=size()){
    cerr << "Error in point2arr::operator[]  bad index = " << n << " for MapId of size " << size() << endl;
  }
  return arr[n];
}

const point2 &point2arr::operator[](unsigned int n) const
{
  if (n>=size()){
    cerr << "Error in poin2arr::operator[]  bad index = " << n << " for MapId of size " << size() << endl;
  }
  return arr[n];
}



bool point2arr::operator== (const point2arr& ptarr) const
{
  if (ptarr.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (ptarr[i]!=this->arr[i])
        return false;
    }
    return true;
  }
  return false;
}

bool point2arr::operator== (const point2arr_uncertain & ptarr) const
{
  if (ptarr.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (this->arr[i]!=ptarr[i])
        return false;
    }
    return true;
  }
  return false;
}

bool point2arr::operator!= (const point2arr& ptarr) const
{
  if (ptarr.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (ptarr[i]!=this->arr[i])
        return true;
    }
    return false;
  }
  return true;
}

bool point2arr::operator!= (const point2arr_uncertain & ptarr) const
{
  if (ptarr.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (this->arr[i]!=ptarr[i])
        return true;
    }
    return false;
  }
  return true;
}


point2arr &point2arr::operator=(const point2arr_uncertain &ptarr){
  this->resize(ptarr.size());
  for(unsigned int i=0;i<ptarr.size();++i){
    arr[i]=ptarr[i];
  }
  return *this;
}

point2arr &point2arr::operator=(const line2 &line)
{
  this->arr.clear();
  this->arr.push_back(line.ptA);
  this->arr.push_back(line.ptB);
  return *this;
}


point2arr point2arr::rot(const double ang) const
{
  point2arr outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i].rot(ang));
  }
  return outarr;
}

vector<double> point2arr::norm() const
{
  vector<double> outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i].norm());
  }
  return outarr;
}

vector<double> point2arr::dist(const point2 &pt) const
{
  vector<double> outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i].dist(pt));
  }
  return outarr;
}

point2arr point2arr::operator+(const point2 &pt) const
{
  point2arr outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i]+pt);
  }
  return outarr;
}

point2arr point2arr::operator-(const point2 &pt) const
{
  point2arr outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i]-pt);
  }
  return outarr;
}


point2arr point2arr::operator*(const double d) const
{
  point2arr outarr;
  unsigned int i;
  for (i=0;i<arr.size();++i){
    outarr.push_back(arr[i]*d);
  }
  return outarr;
}

void point2arr::reverse()
{
  vector<point2> tmp;
  tmp.clear();
  int i;
  int arrsize = (int)this->size();
  for(i=0;i<arrsize;++i){
    tmp.push_back(arr[arrsize-1-i]);
  }
  arr = tmp;
} 

double point2arr::linelength() const
{
  unsigned int i;
  unsigned int arrsize = this->size();
  double len = 0;
  for (i = 1;i<arrsize;++i){
    len = len + arr[i-1].dist(arr[i]);
  }
  return len;
}

point2 point2arr::project(const point2& pt) const
{
  unsigned int i;
  unsigned int arrsize = this->size();
  point2 outpt;
  if (arrsize==0){
    outpt= pt;
    return outpt;
  }
  if (arrsize==1){
    outpt= arr[0];
    return outpt;
  }
  line2 thisline;
  double thisdist;
  double mindist;
  for (i = 1;i<size();++i){
    thisline.set(arr[i-1],arr[i]);
    thisdist = thisline.dist(pt);
    if (thisdist<mindist || i==1){
      outpt = thisline.project_bound(pt);
      mindist = thisdist;
    }
  }
  
  return outpt;
}

double point2arr::project_along(const point2& pt) const
{
  unsigned int i;
  unsigned int arrsize = this->size();
  
  if (arrsize==0){
    return 0;
  }
  if (arrsize==1){
    return 0;
  }
  point2 tmppt;
  line2 thisline;
  double thisdist;
  double mindist;
  unsigned int minindex;
  for (i = 1;i<size();++i){
    thisline.set(arr[i-1],arr[i]);
    thisdist = thisline.dist(pt);
    if (thisdist<mindist || i==1){
      tmppt = thisline.project_bound(pt);
      mindist = thisdist;
      minindex = (int)i;
    }
  }
  double totdist = 0;
  for (i = 1;i<minindex;++i){
    totdist = totdist+arr[i-1].dist(arr[i]);
  }
  totdist = totdist+arr[minindex-1].dist(tmppt);
  
  return totdist;
}

double point2arr::cut_front(const point2arr& ptarr)
{
  return linelength();
}
double point2arr::cut_front(const point2& pt, const double delta)
{
  return cut_front(project_along(pt)+delta);
}
double point2arr::cut_front(const double length)
{
  unsigned int arrsize = this->size();
  point2arr ptarrout;
  //double currlen = 0;
  if (arrsize==0)
    return 0;
  double currentlength = length;
  double thisdist = 0.0;
  point2 dpt,newpt;
  ptarrout.push_back(arr[0]);
  if (length<=0){
    arr = ptarrout.arr;
    return 0;
  }
  for (unsigned i=1; i<size();++i){
    thisdist = arr[i-1].dist(arr[i]);
    if (thisdist>currentlength){
      dpt = arr[i]-arr[i-1];
      dpt = dpt*currentlength/thisdist;
      newpt = arr[i-1]+dpt;
      ptarrout.push_back(newpt);
      arr = ptarrout.arr;    
      return linelength();
    }else if (thisdist==currentlength){
      ptarrout.push_back(arr[i]);
      arr = ptarrout.arr;
      return linelength();
    }else{
      currentlength = currentlength-thisdist;
      ptarrout.push_back(arr[i]);
    }

  }
  arr = ptarrout.arr;
  return linelength();
}

double point2arr::cut_back(const point2arr& ptarr)
{
  return linelength();
}
double point2arr::cut_back(const point2& pt, const double delta)
{
  return cut_back(linelength()-project_along(pt)+delta);
}
double point2arr::cut_back(const double length)
{
  unsigned int arrsize = this->size();
  point2arr ptarrout;
  if (arrsize==0)
    return 0;
  
  double currentlength = length;
  double thisdist = 0.0;
  point2 dpt,newpt;
  ptarrout.push_back(arr[arrsize-1]);
  if (length<=0){
    arr = ptarrout.arr;
    return 0;
  }
  
  for (int i=(int)arrsize-2; i>=0;--i){
    thisdist = arr[i+1].dist(arr[i]);
    if (thisdist>currentlength){
      dpt = arr[i]-arr[i+1];
      dpt = dpt*currentlength/thisdist;
      newpt = arr[i+1]+dpt;
      ptarrout.push_back(newpt);
      ptarrout.reverse();
      arr = ptarrout.arr;
      return linelength();
    }else if (thisdist==currentlength){
      ptarrout.push_back(arr[i]);
      ptarrout.reverse();
      arr = ptarrout.arr;
      return linelength();
    }else{
      currentlength = currentlength-thisdist;
      ptarrout.push_back(arr[i]);
    }

  }
  ptarrout.reverse(); 
  arr = ptarrout.arr;
  return linelength();
}



void point2arr::get_side(const point2& pt,  int& side, double& index)
{
  unsigned int i;
  unsigned int arrsize = this->size();
  double thiscross, thisdot, thisdist, norm0;
  point2 pt0;
  point2 dpt0, dpt1, dpt2;
  double mindist;
  double thisindex;
  int thisside;
  if (arrsize<2){
    side = 0;
    index = 0;
    return;
  }

  dpt2 = pt-arr[0];
  mindist = dpt2.norm();
  index = 0;
  side = 0;

  for (i=0;i<arrsize-1;++i){   
    dpt0 = arr[i+1]-arr[i];
    dpt1 = dpt2;
    dpt2 = pt-arr[i+1];
    norm0 = dpt0.norm();
    thisdot = dpt0.dot(dpt1);
    thiscross = dpt0.cross(dpt1);

    if (thiscross>0)
      thisside = 1;
    else if (thiscross<0)
      thisside = -1;
    else 
      thisside = 0;

    if (thisdot>=0 && (thisdot/norm0)<=norm0){
      thisdist = fabs(thiscross/norm0);
      thisindex= (double)i+fabs(thisdot/(norm0*norm0));

    }else if ((thisdot/norm0)>norm0){
      thisdist = dpt2.norm();
      thisindex = (double)i+1;
    }else{
      thisdist = mindist+1;
      thisindex = i;
    }
    //    cout << "thisindex = " << thisindex 
    //       << "  thisdist = " << thisdist << endl;
    if (thisdist<mindist){
      mindist = thisdist;
      index = thisindex;
      side = thisside;
    }  
    //  cout << "index = " << index 
    //         << "  mindist = " << mindist << endl;
  }
}

void point2arr::get_side(const point2& pt,  int& side, double& dist, double&  index)
{
  unsigned int i;
  unsigned int arrsize = this->size();
  double thiscross, thisdot, thisdist, norm0;
  point2 pt0;
  point2 dpt0, dpt1, dpt2;
  double mindist;
  double thisindex;
  int thisside;
  if (arrsize<2){
    side = 0;
    index = 0;
    return;
  }

  dpt2 = pt-arr[0];
  mindist = dpt2.norm();
  index = 0;
  side = 0;

  for (i=0;i<arrsize-1;++i){   
    dpt0 = arr[i+1]-arr[i];
    dpt1 = dpt2;
    dpt2 = pt-arr[i+1];
    norm0 = dpt0.norm();
    thisdot = dpt0.dot(dpt1);
    thiscross = dpt0.cross(dpt1);
    thisside = (thiscross ==0) ? 0 : (thiscross<0 ? -1:1);   
    if (thisdot>=0 && (thisdot/norm0)<=norm0){
      thisdist = fabs(thiscross/norm0);
      thisindex= (double)i+fabs(thisdot/(norm0*norm0));

    }else if ((thisdot/norm0)>norm0){
      thisdist = dpt2.norm();
      thisindex = (double)i+1;
    }else{
      thisdist = mindist+1;
      thisindex = i;
    }
    //    cout << "thisindex = " << thisindex 
    //       << "  thisdist = " << thisdist << endl;
    if (thisdist<mindist){
      mindist = thisdist;
      index = thisindex;
      side = thisside;
    }  
    //  cout << "index = " << index 
    //         << "  mindist = " << mindist << endl;
  }
  dist = mindist;
}
void point2arr::get_side(const point2& pt,  int& side, int& index)
{
  unsigned int i;
  unsigned int arrsize = this->size();
  double thiscross, thisdot, thisdist, norm0;
  point2 pt0;
  point2 dpt0, dpt1, dpt2;
  double mindist;
  int thisindex;
  int thisside;
  if (arrsize<2){
    side = 0;
    index = 0;
    return;
  }

  dpt2 = pt-arr[0];
  mindist = dpt2.norm();
  index = 0;
  side = 0;

  for (i=0;i<arrsize-1;++i){   
    dpt0 = arr[i+1]-arr[i];
    dpt1 = dpt2;
    dpt2 = pt-arr[i+1];
    norm0 = dpt0.norm();
    thisdot = dpt0.dot(dpt1);
    thiscross = dpt0.cross(dpt1);
    thisside = (thiscross ==0) ? 0 : (thiscross<0 ? -1:1);   
    if (thisdot>=0 && (thisdot/norm0)<=norm0){
      thisdist = fabs(thiscross/norm0);
      thisindex= i;

    }else if ((thisdot/norm0)>norm0){
      thisdist = dpt2.norm();
      thisindex = i;
    }else{
      thisdist = mindist+1;
      thisindex = i;
    }
    //    cout << "thisindex = " << thisindex 
    //       << "  thisdist = " << thisdist << endl;
    if (thisdist<mindist){
      mindist = thisdist;
      index = thisindex;
      side = thisside;
    }  
    //  cout << "index = " << index 
    //         << "  mindist = " << mindist << endl;
  }
}

void point2arr::connect(const point2arr &ptarr)
{
  
  // unsigned int sizeA = size();
  unsigned int sizeB = ptarr.size();

  //  if (sizeA ==0){
  //     arr = ptarr.arr;
  //     return;
  //   }

  //   if (sizeB ==0)
  //     return;

  //   // temp to handl pt balance;
  //   point2 cpt;
  //   cpt = 0.5*(arr[sizeA-1]+ptarr[0]);
  //   arr.push_back(cpt);
  for (unsigned i =0;i<sizeB;++i){
    arr.push_back(ptarr[i]);
  }
}



void point2arr::connect_intersect(const point2arr &ptarr)
{
  
  unsigned int sizeA = size();
  unsigned int sizeB = ptarr.size();

  cout << sizeA << " " << sizeB << endl;

  if (sizeB==0)
    return;

  if (sizeA<=1 || sizeB==1){
    connect(ptarr);
    return;
  }
  

  line2 lineAend(arr[sizeA-2],arr[sizeA-1]);
  line2 lineBbeg(ptarr[0],ptarr[1]);
  point2 newpt;
  newpt = lineAend.intersect(lineBbeg);
  unsigned startindex = 0;

  if (lineBbeg.is_cross(lineAend)){
    cout << "INTERSECTING1" << endl;
    arr[sizeA-1] = newpt;
  }else {
    arr.push_back(newpt);
    cout << "ADDING" << endl;
  }
 
  if (lineAend.is_cross(lineBbeg)){
    cout << "INTERSECTING2" << endl;
    startindex = 1;
  } 


  for (unsigned i=startindex;i<sizeB;++i){
    arr.push_back(ptarr[i]);
  }
}


point2arr point2arr::get_intersect(const point2arr &ptarr)
{
  unsigned int sizeA = size();
  unsigned int sizeB = ptarr.size();
  //  unsigned int i,j;
  point2arr outarr;
    
  if (sizeA>1 & sizeB>1){
    point2 ptA0,ptA1,ptB0,ptB1;
    point2 ptA0_rot,ptA1_rot,ptB0_rot,ptB1_rot;
    point2 dptA, dptB_rot,newpt, newpt_rot;
    double angA;

    ptA0 = arr[0];
    ptA1 = arr[1];

    ptB0 = ptarr[0];
    ptB1 = ptarr[1];
    
    dptA = ptA1-ptA0;
    angA = dptA.heading();
    
    ptA0_rot = ptA0.rot(-angA);
    ptA1_rot = ptA1.rot(-angA);
    ptB0_rot = ptB0.rot(-angA);
    ptB1_rot = ptB1.rot(-angA);

    dptB_rot = ptB1_rot-ptB0_rot;
    newpt_rot.y = ptA0_rot.y;
    if (dptB_rot.y!=0){
      newpt_rot.x = (newpt_rot.y-ptB0_rot.y)*
        dptB_rot.x/dptB_rot.y+ptB0_rot.x;
      newpt = newpt_rot.rot(angA);
      outarr.push_back(newpt);
    }

  }

  return outarr;
}



point2arr point2arr::get_offset(double d)
{
  point2arr outarr;
  unsigned int arrsize = this->size();
  unsigned int i;
  point2 pt0, pt1;
  point2 offpt0A, offpt1A, offpt0B, offpt1B;
  double angB;
  point2 dptA, dptB, newpt;
 
  if (arrsize>=2){
    pt0 = arr[0];
    pt1 = arr[1];
    dptB = pt1-pt0;
    angB = dptB.heading();
      
    offpt0B = pt0.rot(-angB);
    offpt1B = pt1.rot(-angB);
    offpt0B.y = offpt0B.y+d;
    offpt1B.y = offpt1B.y+d;
    offpt0B = offpt0B.rot(angB);
    offpt1B = offpt1B.rot(angB);
    
    outarr.push_back(offpt0B);

    for (i=1;i<arrsize-1;++i){

      offpt0A = offpt0B;
      offpt1A = offpt1B;

      pt0 = pt1;
      pt1 = arr[i+1];

      dptB = pt1-pt0;
      angB = dptB.heading();
      
      offpt0B = pt0.rot(-angB);
      offpt1B = pt1.rot(-angB);
      offpt0B.y = offpt0B.y+d;
      offpt1B.y = offpt1B.y+d;

      offpt0A = offpt0A.rot(-angB);
      offpt1A = offpt1A.rot(-angB);

      dptA = offpt1A-offpt0A;



      newpt.y = offpt0B.y;

    
      if (fabs(dptA.y) <.000001){
        newpt.x = offpt0B.x;
      }else{
        newpt.x = (newpt.y-offpt1A.y)*
          dptA.x/dptA.y+offpt1A.x;
      }
    
      newpt = newpt.rot(angB);

      outarr.push_back(newpt);

      offpt0B = offpt0B.rot(angB);
      offpt1B = offpt1B.rot(angB);
          
      
    
      
      
    }
    outarr.push_back(offpt1B);
  }
  
  
  return outarr;
  
}

point2 point2arr::min() const{
  unsigned int arrsize = this->size();
  double minx;
  double miny;
  for (unsigned i = 0; i < arrsize; ++i){
    if (arr[i].x< minx || i==0)
      minx = arr[i].x;
    if (arr[i].y< miny || i==0)
      miny = arr[i].y;
  }
  return point2(minx,miny);
}

point2 point2arr::max() const{
  unsigned int arrsize = this->size();
  double maxx;
  double maxy;
  for (unsigned i = 0; i < arrsize; ++i){
    if (arr[i].x> maxx || i==0)
      maxx = arr[i].x;
    if (arr[i].y> maxy || i==0)
      maxy = arr[i].y;
  }
  return point2(maxx,maxy);
}

vector<point2arr> point2arr::split(const double d)
{
  vector<point2arr> ptarrout;

  point2arr ptarr;
    
  if (size()==0)
    return ptarrout;
  double dist;
    ptarr.push_back(arr[0]);
  for(unsigned i = 1; i< size();++i){
    dist = arr[i].dist(arr[i-1]);
    if (dist>d){
      ptarrout.push_back(ptarr);
      ptarr.clear();
    }
    ptarr.push_back(arr[i]);
  }
  if (ptarr.size()>0)
    ptarrout.push_back(ptarr);


  return ptarrout;
}


  point2arr operator*(const double d,const point2arr &ptarr)
    {
      return ptarr*d;
    }



point2 point2arr::average()
{
    double sumx = 0;
    double sumy = 0;
    unsigned int i;
    unsigned int sizearr = arr.size();
    for(i=0; i<sizearr; i++)
    {
        sumx += arr[i].x;
        sumy += arr[i].y;
    }    
    double meanx, meany;
    meanx = sumx/sizearr;
    meany = sumy/sizearr;

    return point2(meanx, meany);
}


void point2arr::split_at(int num, point2arr& arr1, point2arr& arr2) 
{
  arr1.clear();
  arr2.clear();
  for(int i=0; i< num; i++) {
    arr1.push_back(arr[i]);
  } 
  for(unsigned int j = num; j < arr.size(); j++) {
    arr2.push_back(arr[j]);
  }

}

//returns sum(x_i*y_i)
void point2arr::sum_squares(double& ssxx, double& ssyy, double& ssxy)
{
    unsigned int i;
    unsigned int sizearr = this->size();
    double xy = 0;
    double xx = 0;
    double yy = 0;
    for(i = 0; i<sizearr; i++)
    {
        xy += arr[i].x * arr[i].y;
        xx += arr[i].x * arr[i].x;
        yy += arr[i].y * arr[i].y;
    }
    ssxx = xx;
    ssyy = yy;
    ssxy = xy;
    return;
}

double point2arr::fit_line(double& a, double& b, point2& pt1, point2& pt2)
{
    point2 centerpoint;
    double ssxx, ssyy, ssxy;            
    unsigned int sizearr;
    double B, b1, b2, a1, a2;
    double err1, err2, MSE;

    sizearr = this->size();
    centerpoint = this->average();

    this->sum_squares(ssxx, ssyy, ssxy);

    double grr1 = ssyy - sizearr*centerpoint.y*centerpoint.y;
    double grr2 = ssxx - sizearr*centerpoint.x*centerpoint.x;
    double grr3 = sizearr*centerpoint.x*centerpoint.y - ssxy;

    B = .5*(grr1 - grr2)/grr3;

    b1 = -B + sqrt(1+B*B);
    a1 = centerpoint.y - b1*centerpoint.x;

    b2 = -B - sqrt(1+B*B);
    a2 = centerpoint.y - b2*centerpoint.x;

    err1 = this->line_error(a1,b1);
    err2 = this->line_error(a2,b2);

    if(err1 < err2) {
      a = a1;
      b = b1;
      MSE = err1;
    } else {
      a = a2;
      b = b2;
      MSE = err2;
    }

    point2 p1, p2, temppt;
    line2 l1;

    p1 = point2(-1, a-b);
    p2 = point2(1, a+b);
    l1 = line2(p1,p2);

    pt1 = l1.project(arr[0]);
    pt2 = l1.project(arr[sizearr-1]);

    return MSE;
} 

#warning "this is still untested!"

double point2arr::fit_line_perp(double& a, double& b, point2& pt1, point2& pt2, double slope)
{
    point2 centerpoint;
    unsigned int sizearr;
    double err1, err2, MSE;

    sizearr = this->size();
    centerpoint = this->average();

    b = -1/slope;
    a = centerpoint.y - b*centerpoint.x;

    MSE = this->line_error(a,b);

    point2 p1, p2, temppt;
    line2 l1;

    p1 = point2(-1, a-b);
    p2 = point2(1, a+b);
    l1 = line2(p1,p2);

    pt1 = l1.project(arr[0]);
    pt2 = l1.project(arr[sizearr-1]);

    return MSE;
} 


double point2arr::line_error(double a, double b)
{
  double SE = 0;
  double err;
  unsigned int i;
  unsigned int sizearr = this->size();

  for(i=0; i<sizearr; i++) {
    err = pow((arr[i].y - a - b*arr[i].x),2)/(1+b*b);
    SE += err;
  }

  return SE/sizearr;
}

//algorithm taken from:
// local.wasp.uwa.edu.au/~pbourke/geometry/insidepoly
int point2arr::get_inside(const point2& pt)
{
  double theta = 0;
  point2 delta1; //vector from input pt to ptA of seg
  point2 delta2; //vector from input pt to ptB of seg

  unsigned int i;
  unsigned int sizearr = this->size();

  for(i=0; i<sizearr-1; i++) {
    delta1 = arr[i] - pt;
    delta2 = arr[i+1] - pt;
    theta += angle2d(delta1, delta2);
  }

  if(fabs(theta) < 1) //theta == 0 means pt outside
    return -1;

  return 1;

}

void point2arr::remove(int index)
{
  arr.erase(arr.begin()+index);
}

void point2arr::add_element(point2 pt, int index)
{
  arr.insert(arr.begin()+index,sizeof(point2),pt);
}

void point2arr::merge(point2arr ptarr)
{
  unsigned int i;
  unsigned int sizearr = ptarr.size();
  int inside, index;
  for(i=0; i<sizearr; i++) {
    inside = this->get_inside(ptarr[i]);
    index = this->get_closest_side(ptarr[i]);
    if(-1 == inside) {
      this->add_element(ptarr[i],index+1);
    }
  }
  return;
}

int point2arr::get_closest_side(point2 pt)
{
  int index = 0;

  line2 thisside;
  double thisdist, mindist;

  unsigned int i;
  unsigned int sizearr = this->size();

  if (sizearr<2){
    index = 0;
    return 0;
  }

  mindist = pt.dist(arr[0]);

  for(i=0; i<sizearr-1; i++) {
    thisside = line2(arr[i],arr[i+1]);
    thisdist = thisside.dist(pt);
    if(thisdist < mindist) {
      mindist = thisdist;
      index = i;
    }
  }

  return index;
}

int point2arr::get_overlap(point2arr ptarr)
{
  unsigned int i;
  unsigned int thissize = this->size();
  unsigned int othersize = ptarr.size();
  int numOverlap = 0;
  int inside;

  for(i=0;i<othersize;i++) {
    inside = this->get_inside(ptarr[i]);
    if(inside == 1) {
      numOverlap++;
    }
  }
  for(i=0;i<thissize;i++) {
    inside = ptarr.get_inside(arr[i]);
    if(inside == 1) {
      numOverlap++;
    }
  }

  return numOverlap;

}

point2arr point2arr::get_bound_box() const
{
  point2arr ptarrout, ptarrup, ptarrdn,ptarr;
  
  ptarr = arr;

  
  vector<double> dangarr;
  dangarr.push_back(M_PI/4);
  dangarr.push_back(M_PI/8);
  dangarr.push_back(M_PI/16);
  dangarr.push_back(M_PI/32);
  dangarr.push_back(M_PI/64);
  dangarr.push_back(M_PI/128);

  point2 maxup,maxdn,minup,mindn, maxpt,minpt;
  point2 lwup,lwdn,initlw;
  double thisang =0;
  double dang;
  double minarea;
  double areaup, areadn;
  point2 thismin, thismax;

  maxup = ptarrup.max();
  maxdn = ptarrdn.max();
  minup = ptarrup.min();
  mindn = ptarrdn.min();
  lwup = maxup-minup;
  lwdn = maxdn-mindn;

  maxpt = ptarr.max();
  minpt = ptarr.min();
  initlw = maxpt-minpt;
  minarea = initlw.x*initlw.y;

  for (unsigned i = 0; i< dangarr.size(); ++i){
    dang = dangarr[i];
    ptarrup = ptarr.rot(dang);
    ptarrdn = ptarr.rot(-dang);
    maxup = ptarrup.max();
    maxdn = ptarrdn.max();
    minup = ptarrup.min();
    mindn = ptarrdn.min();
    lwup = maxup-minup;
    lwdn = maxdn-mindn;
    
    areaup = lwup.x*lwup.y;
    areadn = lwdn.x*lwdn.y;
 
    if (areaup < minarea && areaup < areadn){
      thisang = thisang - dang;
      ptarr = ptarrup;
      minarea = areaup;
      thismin = minup;
      thismax = maxup;
    }else if(areadn < minarea && areadn < areaup) {
      thisang = thisang + dang;
      ptarr = ptarrdn;
      minarea = areadn;
      thismin = mindn;
      thismax = maxdn;

    }

  }

  point2 tmp;
  tmp.set(thismin.x,thismin.y);
  ptarrout.push_back(tmp);
  tmp.set(thismin.x,thismax.y);
  ptarrout.push_back(tmp);
  tmp.set(thismax.x,thismax.y);
  ptarrout.push_back(tmp);
  tmp.set(thismax.x,thismin.y);
  ptarrout.push_back(tmp);


  point2 lw = thismin-thismax;
  //if (lw.x < lw.y){
  ptarrout = ptarrout.rot(thisang);
  //}else {
  //  ptarrout = ptarrout.rot(thisang-M_PI/2);
  //}


  return ptarrout;
}


ostream &operator<<(ostream &os, const point2arr &ptarr) {
  unsigned int i;
  
  for (i=0;i<ptarr.size();++i){
    os <<endl << "[" << i << "] "  << ptarr[i];
  }
  return os;
}


ostream &operator<<(ostream &os, const vector<double> &vec) {
  unsigned int i;
  
  for (i=0;i<vec.size();++i){
    os <<endl << "[" << i << "] "  << vec[i];
  }
  return os;
}

ostream &operator<<(ostream &os, const vector<int> &vec) {
  unsigned int i;
  
  for (i=0;i<vec.size();++i){
    os <<endl << "[" << i << "] "  << vec[i];
  }
  return os;
}

