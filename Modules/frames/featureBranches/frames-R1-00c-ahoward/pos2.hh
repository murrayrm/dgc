/**********************************************************
 **
 **  POS2.H
 **
 **    Time-stamp: <2007-02-08 13:33:15 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 09:26:14 2007
 **
 **
 **********************************************************
 **
 **  Simple 2D position struct
 **
 **********************************************************/


#ifndef POS2_HH
#define POS2_HH

#include <math.h>

typedef struct
{
  double x, y;
} pos2;



#endif
