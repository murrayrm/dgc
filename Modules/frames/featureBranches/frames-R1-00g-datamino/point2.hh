/**********************************************************
 **
 **  POINT2.H
 **
 **    Time-stamp: <2007-03-04 16:57:58 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 09:26:14 2007
 **
 **
 **********************************************************
 **
 **  Simple 2D point class
 **
 **********************************************************/


#ifndef POINT2_HH
#define POINT2_HH

#include <math.h>
#include <vector>
#include <iostream>
#include "point2_uncertain.hh"
using namespace std;

class point2_uncertain;

class point2
{
public:
  double x, y;

	point2(): x(0), y(0) {}
	point2(double xin, double yin) : x(xin), y(yin) {}

	~point2() {}


	point2(const point2 &pt){
		x = pt.x;
		y = pt.y;
	}

	
	
	point2 &operator=(const point2 &pt){
		if (this!= &pt){
			x = pt.x;
			y = pt.y;
		}
		return *this;
	}

	point2 &operator=(const point2_uncertain &pt);

	bool operator==(const point2 &pt) const
	{
	  return ((x==pt.x)&&(y==pt.y));
	}
	bool operator==(const point2_uncertain &pt) const;


	bool operator!=(const point2 &pt) const
	{
	  return ((x!=pt.x)||(y!=pt.y));
	}
	
	bool operator!=(const point2_uncertain &pt) const;


	point2 operator+(const point2 &pt) const {
		return point2(x+pt.x, y+pt.y);
	
	}
	point2 operator+(const point2_uncertain &pt) const;


	point2 operator-(const point2 &pt) const {
		return point2(x-pt.x,y-pt.y);
	
	}
	point2 operator-(const point2_uncertain &pt) const;

	point2 operator*(const double d) const {
		return point2(d*x,d*y);
	
	}

	point2 operator/(const double d) const {
		return point2(x/d,y/d);
	
	}


		
	point2 rot(double ang) const { 
		double sa = sin(ang);
		double ca = cos(ang);
		return point2(x*ca-y*sa, x*sa+y*ca);
	}
	
	void set(double xin, double yin){
		x = xin;
		y = yin;
	}

	void set(double val){
		x = val;
		y = val;
	}
	void clear() {
		x = 0;
		y = 0;
	}
	double norm() const {
		return hypot(x,y);
	}

	double heading() const {
		return atan2(y,x);
	}

	double cross(const point2& pt) const{
		return this->x*pt.y-this->y*pt.x;
	}

	double dot(const point2& pt) const{
		return this->x*pt.x+this->y*pt.y;
	}

	double dist(const point2& pt) const{
		return hypot(x-pt.x, y-pt.y);
	}




}; 

point2 operator*(const double d, const point2& pt);
istream &operator>>(istream &is, point2 &pt);
ostream &operator<<(ostream &os, const point2 &pt);


class point2arr_uncertain;

class point2arr
{
	
public:
	vector<point2> arr;

	point2arr() {arr.clear();}
	point2arr(const point2arr &ptarr){
		arr = ptarr.arr;
	}
	point2arr(const vector<point2> &ptarr){
		arr =ptarr;
	}
	point2arr(const int size){
		arr.resize((unsigned int)size);
	}

	point2arr(const point2 &pta, const point2 &ptb){
		arr.push_back(pta);
		arr.push_back(ptb);
	}


	~point2arr() {}
	
	void resize(unsigned int size) {arr.resize(size);}
	void clear() {arr.clear();}
	void push_back(const point2 &pt) {arr.push_back(pt);}

	point2 &operator[](unsigned int n) {return arr[n];}
	const point2 &operator[](unsigned int n) const {return arr[n];}
	unsigned int size() const {return arr.size();}

	point2& back() {
		return this->arr.back();
	}


	//	point2 &operator[](int n) {return arr[(unsigned int)n];}

	point2arr &operator=(const point2arr &ptarr){
		if (this!= &ptarr){
			this->arr = ptarr.arr;
		}
		return *this;
	}



	point2arr &operator=(const vector<point2> &ptarr){
		this->arr = ptarr;
		return *this;
	}

	bool operator== (const point2arr& ptarr) const;
	bool operator== (const point2arr_uncertain& ptarr) const;

	bool operator!= (const point2arr& ptarr) const ;
	bool operator!= (const point2arr_uncertain& ptarr) const;


	point2arr &operator=(const point2arr_uncertain &ptarr);

	point2arr rot(const double ang) const;
	vector<double> norm() const;
	vector<double> dist(const point2 &pt) const;

	point2arr operator+( const point2 &pt) const; 
	point2arr operator-( const point2 &pt) const;
	point2arr operator*( const double d) const;



	void get_side(const point2& pt, int& side, double& index);
	point2arr get_intersect(const point2arr &ptarr);
	point2arr get_offset(double d);
};

point2arr operator*(const double d,const point2arr &ptarr);
ostream &operator<<(ostream &os, const point2arr &ptarr);
ostream &operator<<(ostream &os, const vector<double> &vec);
	ostream &operator<<(ostream &os, const vector<int> &vec);
#endif
