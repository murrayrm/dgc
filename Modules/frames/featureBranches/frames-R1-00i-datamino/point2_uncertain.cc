/**********************************************************
 **
 **  POINT2_UNCERTAIN.CC
 **
 **    Time-stamp: <2007-03-07 15:24:39 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 27 11:04:35 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/
#include "point2_uncertain.hh"

using namespace std;

point2_uncertain operator*(const double d, const point2_uncertain& pt)
{
	return pt*d;
}

ostream &operator<<(ostream &os, const point2_uncertain &pt) {
	os << "(" << pt.x << ", " << pt.y
		 <<  ", {" << pt.max_var 
		 <<  ", "<< pt.min_var 
		 <<  ", "<< pt.axis <<"})";
	return os;
        }

point2_uncertain::point2_uncertain(const point2 &pt)
{
	x = pt.x;
		y = pt.y;
		max_var = 0;
		min_var = 0;
		axis = 0;
}


point2_uncertain &point2_uncertain::operator=(const point2 &pt) {
		x = pt.x;
		y = pt.y;
		max_var = 0;
		min_var = 0;
		axis = 0;
		return *this;
}

point2_uncertain point2_uncertain::operator+(const point2 &pt) const {
		return point2_uncertain(x+pt.x, y+pt.y, max_var, min_var, axis);
	}

point2_uncertain point2_uncertain::operator-(const point2 &pt) const {
	return point2_uncertain(x-pt.x, y-pt.y, max_var, min_var, axis);
}	





point2arr_uncertain::point2arr_uncertain(const point2arr &ptarr)
{
	this->resize(ptarr.size());
	for(unsigned int i=0;i<ptarr.size();++i){
		arr[i]=ptarr[i];
	}
}
point2arr_uncertain::point2arr_uncertain(const vector<point2> &ptarr){
	this->resize(ptarr.size());
	for(unsigned int i=0;i<ptarr.size();++i){
		arr[i]=ptarr[i];
	}
}

point2arr_uncertain &point2arr_uncertain::operator=(const point2arr &ptarr){
	this->resize(ptarr.size());
	for(unsigned int i=0;i<ptarr.size();++i){
		arr[i]=ptarr[i];
	}
	return *this;
}

unsigned int point2arr_uncertain::insert(unsigned int n, point2_uncertain &pt)
{
	unsigned int index=n;
	if (n > arr.size())
		index = arr.size();
	arr.insert(arr.begin()+index,pt);
	return index;
	
}

point2arr_uncertain point2arr_uncertain::rot(const double ang) const
{
			point2arr_uncertain outarr;
			unsigned int i;
			for (i=0;i<arr.size();++i){
				outarr.push_back(arr[i].rot(ang));
			}
			return outarr;
}

vector<double> point2arr_uncertain::norm() const
{
			vector<double> outarr;
			unsigned int i;
			for (i=0;i<arr.size();++i){
				outarr.push_back(arr[i].norm());
			}
			return outarr;
}

vector<double> point2arr_uncertain::dist(const point2_uncertain &pt) const
{
			vector<double> outarr;
			unsigned int i;
			for (i=0;i<arr.size();++i){
				outarr.push_back(arr[i].dist(pt));
			}
			return outarr;
}

point2arr_uncertain point2arr_uncertain::operator+(const point2_uncertain &pt) const
{
			point2arr_uncertain outarr;
			unsigned int i;
			for (i=0;i<arr.size();++i){
				outarr.push_back(arr[i]+pt);
			}
			return outarr;
}

point2arr_uncertain point2arr_uncertain::operator-(const point2_uncertain &pt) const
{
			point2arr_uncertain outarr;
			unsigned int i;
			for (i=0;i<arr.size();++i){
				outarr.push_back(arr[i]-pt);
			}
			return outarr;
}


point2arr_uncertain point2arr_uncertain::operator*(const double d) const
{
			point2arr_uncertain outarr;
			unsigned int i;
			for (i=0;i<arr.size();++i){
				outarr.push_back(arr[i]*d);
			}
			return outarr;
}

















point2arr_uncertain operator*(const double d,const point2arr_uncertain &ptarr)
{
	return ptarr*d;
}



ostream &operator<<(ostream &os, const point2arr_uncertain &ptarr) {
	unsigned int i;
	
	for (i=0;i<ptarr.size();++i){
		os  << endl<< "[" << i << "] " << ptarr[i];
	}
	return os;
        }

//vector<point2> operator=(const vector<point2_uncertain>& ptarr){
//	vector<point2> outarr(ptarr.size());
//	unsigned int i;
//	for (i=0;i<ptarr.size();++i){
//		outarr[i].x = ptarr[i].x;
//		outarr[i].y = ptarr[i].y;
//	}
//	return outarr;
//}
