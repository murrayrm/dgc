/*********************************** 
Haomiao Huang
3 x 3 matrix class
***********************************/

#ifndef CMatrix_HH
#define CMatrix_HH

const int rows = 3;
const int cols = 3;


class CMatrix
{
private:
  double data[rows][cols];

public:
  // Constructors
  /*! create empty matrix. */
  CMatrix();  
  /*! copy constructor. */
  CMatrix(const CMatrix &matrix2); 

  // Mutators
  /*! sets the (row, col)th element of the matrix to value. */
  void setelem(int row, int col, double value);  

  // Accessors
  /*! returns the number of rows. */
  int getrows() const;  
  /*! returns the number of columns. */
  int getcols() const;  
  /*! returns the (row, col)th element of the matrix. */
  double getelem(int row, int col) const; 
  /*! returns the transpose of the matrix. */
  CMatrix transpose() const; 

  // Operators
  /*! boolean comparison with matrix2. */
  bool operator==(const CMatrix &matrix2) const;  
  
  /*! add another r x c matrix. */
  CMatrix operator+(const CMatrix &matrix2) const;  
  
  /*! subtract another r x c matrix. */
  CMatrix operator-(const CMatrix &matrix) const;  

  /*! check for not equal. */
  bool operator!=(const CMatrix &matrix2); 

  /*! add another r x c matrix to self. */
  CMatrix& operator+=(const CMatrix &matrix2);
  
  /*! subtract another r x c matrix from self. */
  CMatrix& operator-=(const CMatrix &matrix);

  /*! multiply by another matrix. */
  CMatrix operator*(const CMatrix &matrix2) const;  
  
  /*! multiply self by another matrix. */
  CMatrix& operator*=(const CMatrix &matrix);  
  
  /*! set one matrix equal to another. */
  CMatrix& operator=(const CMatrix &matrix2);

  // Destructors
  /*! Default destructor. */
  ~CMatrix();  
};

#endif
