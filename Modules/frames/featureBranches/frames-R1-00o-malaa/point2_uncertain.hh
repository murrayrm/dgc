/**********************************************************
 **
 **  POINT2_UNCERTAIN.HH
 **
 **    Time-stamp: <2007-03-10 12:11:05 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 09:37:44 2007
 **
 **
 **********************************************************
 **
 **  Simple 2D position covariance structure
 **
 **********************************************************/


#ifndef POINT2_UNCERTAIN_HH
#define POINT2_UNCERTAIN_HH

#include <math.h>
#include <vector>
#include <iostream>
#include "point2.hh"

using namespace std;

//#define MIN(X,Y) ((X) < (Y) ? : (X) : (Y))
//#define MAX(X,Y) ((X) > (Y) ? : (X) : (Y))

class point2;

class point2_uncertain
{
public:
  double x, y;
  double max_var, min_var, axis;

  point2_uncertain(): x(0), y(0), max_var(0), min_var(0), axis(0) {}
  point2_uncertain(double xin, double yin) 
    : x(xin), y(yin), max_var(0), min_var(0), axis(0) {}
  point2_uncertain(double xin, double yin, double var) 
    : x(xin), y(yin), max_var(var), min_var(var), axis(0) {}
  point2_uncertain(double xin, double yin, double max, double min, double axis) 
    : x(xin), y(yin), max_var(max), min_var(min), axis(axis) {}

  ~point2_uncertain() {}


  point2_uncertain(const point2_uncertain &pt){
    x = pt.x;
    y = pt.y;
    max_var = pt.max_var;
    min_var = pt.min_var;
    axis = pt.axis;
  }

  point2_uncertain(const point2 &pt);

  point2_uncertain &operator=(const point2_uncertain &pt){
    if (this!= &pt){
      x = pt.x;
      y = pt.y;
      max_var = pt.max_var;
      min_var = pt.min_var;
      axis = pt.axis;
    }
    return *this;
  }
  point2_uncertain &operator=(const point2 &pt);

  point2_uncertain operator+(const point2_uncertain &pt) const {
    
    //--------------------------------------------------
    // need to finish this
    //--------------------------------------------------
    return point2_uncertain(x+pt.x,y+pt.y);
  
  }
  point2_uncertain operator-(const point2_uncertain &pt) const {
    
    //--------------------------------------------------
    // need to finish this
    //--------------------------------------------------
    return point2_uncertain(x-pt.x,y-pt.y);
  
  }

  point2_uncertain operator+(const point2 &pt) const; 

  point2_uncertain operator-(const point2 &pt) const;


  point2_uncertain operator*(const double d) const {
    return point2_uncertain(d*x,d*y);
  
  }

  point2_uncertain operator/(const double d) const {
    return point2_uncertain(x/d,y/d);
  
  }

  

  point2_uncertain rot(double ang) const { 
    double thisx = x*cos(ang)-y*sin(ang);
    double thisy = x*sin(ang)+y*cos(ang);
    return point2_uncertain(thisx, thisy, max_var, min_var, axis+ang);
  }

  //point2 point(){
  //  return point2(x,y);
  //}

  //  void set_point2arr(const vector<point2>& ptarr);
  //void get_point2arr(vector<point2>& ptarr) const;


  void clear() {
    x = 0;
    y = 0;
    max_var = 0;
    min_var = 0;
    axis = 0;
  }
  
  double norm() const {
    return hypot(x,y);
  }

  double heading() const {
    return atan2(y,x);

  }

  double cross(const point2_uncertain& pt) const{
    return this->x*pt.y-this->y*pt.x;
  }

  double dot(const point2_uncertain& pt) const{
    return this->x*pt.x+this->y*pt.y;
  }

  double dist(const point2_uncertain& pt) const{
    return hypot(x-pt.x, y-pt.y);
  }


  void print() const {
    cout << "x= " << x
         << " y= " << y  
         << " max_var= " << max_var  
         << " min_var= " << min_var  
         << " axis= " << axis << endl; 
  }
};


point2_uncertain operator*(const double d, const point2_uncertain& pt);
ostream &operator<<(ostream &os, const point2_uncertain &pt);

class point2arr;

class point2arr_uncertain
{
  
public:
  vector<point2_uncertain> arr;

  point2arr_uncertain() {arr.clear();}
  point2arr_uncertain(const point2arr_uncertain &ptarr){
    arr = ptarr.arr;
  }

  point2arr_uncertain(const point2arr &ptarr);
  
  point2arr_uncertain(const vector<point2_uncertain> &ptarr){
    arr =ptarr;
  }

  point2arr_uncertain(const vector<point2> &ptarr);

  point2arr_uncertain(const int size){
    arr.resize((unsigned int)size);
  }


  ~point2arr_uncertain() {}
  
  void resize(unsigned int size) {arr.resize(size);}
  void clear() {arr.clear();}
  void push_back(const point2_uncertain &pt) {arr.push_back(pt);}
    unsigned int insert(unsigned int n, point2_uncertain &pt);

  point2_uncertain &operator[](unsigned int n);
  const point2_uncertain &operator[](unsigned int n) const;

  unsigned int size() const {return arr.size();}


  point2arr_uncertain &operator=(const point2arr_uncertain &ptarr){
    if (this!= &ptarr){
      this->arr = ptarr.arr;
    }
    return *this;
  }

  point2arr_uncertain &operator=(const vector<point2_uncertain> &ptarr){
    this->arr = ptarr;
    return *this;
  }
  point2arr_uncertain &operator=(const point2arr &ptarr);




  point2arr_uncertain rot(const double ang) const;
  vector<double> norm() const;
  vector<double> dist(const point2_uncertain &pt) const;

  point2arr_uncertain operator+( const point2_uncertain &pt) const; 
  point2arr_uncertain operator-( const point2_uncertain &pt) const;
  point2arr_uncertain operator*( const double d) const;



  //  void get_side(const point2_uncertain& pt, int& side, double& index);

};

point2arr_uncertain operator*(const double d,const point2arr_uncertain &ptarr);
ostream &operator<<(ostream &os, const point2arr_uncertain &ptarr);

#endif
