/**********************************************************
 **
 **  POSCOV2.HH
 **
 **    Time-stamp: <2007-02-08 18:59:46 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 09:37:44 2007
 **
 **
 **********************************************************
 **
 **  Simple 2D position covariance structure
 **
 **********************************************************/


#ifndef POSCOV2_HH
#define POSCOV2_HH

#include <math.h>
#include "pos2.hh"

struct poscov2
{
	double x, y;
  double major_var, minor_var, axis;

	double norm() const
	{
		return( hypot(x,y));
	}
};


#endif
