/**********************************************************
 **
 **  POINT2.H
 **
 **    Time-stamp: <2007-06-10 10:01:54 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 09:26:14 2007
 **
 **
 **********************************************************
 **
 **  Simple 2D point class
 **
 **********************************************************/


#ifndef POINT2_HH
#define POINT2_HH


#include <math.h>
#include <vector>
#include <iostream>
#include "point2_uncertain.hh"
#include "gpc.h"

using namespace std;


class point2_uncertain;
class point2arr_uncertain;
class line2;

class point2
{
public:
//only the clear, set and simple operations touch z; 
// all else are strictly 2D
  double x, y, z;  

  point2(): x(0), y(0), z(0) {}
  point2(double xin, double yin, double zin=0.0) : x(xin), y(yin), z(zin) {}

  ~point2() {}


  point2(const point2 &pt){
    x = pt.x;
    y = pt.y;
    z = pt.z;
  }
  
  point2(const point2_uncertain &pt);
  
  
  point2 &operator=(const point2 &pt){
    if (this!= &pt){
      x = pt.x;
      y = pt.y;
      z = pt.z;
    }
    return *this;
  }

  point2 &operator=(const point2_uncertain &pt);

  void set(const point2_uncertain &pt);

  bool operator==(const point2 &pt) const
  {
    return ((x==pt.x)&&(y==pt.y)&&(z==pt.z));
  }
  bool operator==(const point2_uncertain &pt) const;


  bool operator!=(const point2 &pt) const
  {
    return ((x!=pt.x)||(y!=pt.y)||(z!=pt.z));
  }
  
  bool operator!=(const point2_uncertain &pt) const;


  point2 operator+(const point2 &pt) const {
    return point2(x+pt.x, y+pt.y, z+pt.z);
  
  }
  point2 operator+(const point2_uncertain &pt) const;


  point2 operator-(const point2 &pt) const {
    return point2(x-pt.x,y-pt.y, z-pt.z);
  
  }
  point2 operator-(const point2_uncertain &pt) const;

  point2 operator*(const double d) const {
    return point2(d*x,d*y,d*z);
  
  }

  point2 operator/(const double d) const {
    return point2(x/d,y/d,z/d);
  
  }


  /**NOTE: this does NOT touch z**/
  point2 rot(double ang) const { 
    double sa = sin(ang);
    double ca = cos(ang);
    return point2(x*ca-y*sa, x*sa+y*ca);
  }
  
  void set(double xin, double yin, double zin=0.0){
    x = xin;
    y = yin;
    z = zin;
  }

  void set(double val){
    x = val;
    y = val;
  }
  void clear() {
    x = 0;
    y = 0;
    z = 0;
  }

  /**NOTE: this does NOT touch z **/
  double norm() const {
    return hypot(x,y);
  }

  /**NOTE: this does NOT touch z **/
  double heading() const {
    return atan2(y,x);
  }

  /**NOTE: this does NOT touch z **/
  double cross(const point2& pt) const{
    return this->x*pt.y-this->y*pt.x;
  }

  /**NOTE: this does NOT touch z **/
  double dot(const point2& pt) const{
    return this->x*pt.x+this->y*pt.y;
  }

  /**NOTE: this does NOT touch z **/
  double dist(const point2& pt) const{
    return hypot(x-pt.x, y-pt.y);
  }

  /**NOTE: this does NOT touch z **/
  void normalize() {
    double hyp = this->norm();
    x = x/hyp;
    y = y/hyp;
  }

  void neg() {
    x = -x;
    y = -y;
  }


}; 

point2 operator*(const double d, const point2& pt);
istream &operator>>(istream &is, point2 &pt);
ostream &operator<<(ostream &os, const point2 &pt);


class point2arr_uncertain;

class point2arr
{
  
public:
  vector<point2> arr;

  point2arr() {arr.clear();}
  point2arr(const point2arr &ptarr){
    arr = ptarr.arr;
  }
  point2arr(const point2arr_uncertain &ptarr);
  point2arr(const vector<point2> &ptarr){
    arr =ptarr;
  }
  point2arr(const int size){
    arr.resize((unsigned int)size);
  }

  point2arr(const point2 &pta, const point2 &ptb){
    arr.push_back(pta);
    arr.push_back(ptb);
  }

  point2arr(const point2 &pta){
    arr.push_back(pta);
  }

  ~point2arr() {}
  
  point2 min() const;
  point2 max() const;
  void resize(unsigned int size) {arr.resize(size);}
  void clear() {arr.clear();}
  void push_back(const point2 &pt) {arr.push_back(pt);}
  void pop_back() {arr.pop_back();}
  unsigned int insert(unsigned int n, point2 &pt); 

  point2 &operator[](unsigned int n) {return arr[n];}
  const point2 &operator[](unsigned int n) const {return arr[n];}
  unsigned int size() const {return arr.size();}
  point2& back() {
    return this->arr.back();
  }


  point2 &operator[](int n) {return arr[(unsigned int)n];}

  point2arr &operator=(const point2arr &ptarr){
    if (this!= &ptarr){
      this->arr = ptarr.arr;
    }
    return *this;
  }

  point2arr &operator=(const line2 &line);

  point2arr &operator=(const vector<point2> &ptarr){
    this->arr = ptarr;
    return *this;
  }

  bool operator== (const point2arr& ptarr) const;
  bool operator== (const point2arr_uncertain& ptarr) const;

  bool operator!= (const point2arr& ptarr) const ;
  bool operator!= (const point2arr_uncertain& ptarr) const;



  point2arr &operator=(const point2arr_uncertain &ptarr);

  void set(const point2arr_uncertain &ptarr);
  void set(const point2arr &ptarr);

/// @brief Returns a point2arr rotated by the given angle
  point2arr rot(const double ang) const;
/// @brief Reverses the order of the point2arr
  void reverse();
/// @brief Returns a vector of norms for each individual point in the array
  vector<double> norm() const;
/// @brief Returns a vector of distances to the given point from each point in the array
  vector<double> dist(const point2 &pt) const; 

/// @brief Returns the minimum distance between a point2arr and a point given the assumption that the point2arr represents a cloud of unconnected points
 double get_min_dist_points(const point2 &pt) const;
/// @brief Returns the minimum distance between a point2arr and a point given the assumption that the point2arr represents unconnected an ordered polyline
  double get_min_dist_line(const point2 &pt) const;
/// @brief Returns the minimum distance between a point2arr and a point given the assumption that the point2arr represents unconnected a closed polygon
  double get_min_dist_poly(const point2 &pt) const;

  /// @brief Returns the minimum distance between two point2arr objects given the assumption that they both represent unconnected points
   double get_min_dist_points(const point2arr &ptarr) const;/// @brief Returns the minimum distance between two point2arr objects given the assumption that they both  represent orderd polylines
  double get_min_dist_line(const point2arr &ptarr) const;
/// @brief Returns the minimum distance between two point2arr objects given the assumption that they both represent closed polygons
  double get_min_dist_poly(const point2arr &ptarr) const;
 /// @brief Returns the minimum distance between two point2arr objects given the assumption that the base point2arr represents an ordered polyline and the function argument represents a closed polygon
  double get_min_dist_line_poly(const point2arr &ptarr) const;
/// @brief Returns the minimum distance between two point2arr objects given the assumption that the base point2arr represents a polygon and the function argument represents an ordered polyline
  double get_min_dist_poly_line(const point2arr &ptarr) const;


  /// @brief Returns the max of the minimum distance between two point2arr objects given the assumption that they both represent unconnected points
   double get_max_dist_points(const point2arr &ptarr) const;/// @brief Returns the max of the minumum distance between two point2arr objects given the assumption that they both  represent orderd polylines
  double get_max_dist_line(const point2arr &ptarr) const;
/// @brief Returns the max of the minimum distance between two point2arr objects given the assumption that they both represent closed polygons
  double get_max_dist_poly(const point2arr &ptarr) const;


  /// @brief Returns a vector of polygons by reference which are the results of clipping between two polygons
  int get_poly_clip(const point2arr &ptarr,const gpc_op operation, vector<point2arr> & ptarrarr) const;

  /// @brief Returns a vector of polygon by reference representing the difference between the two polygons 
  int get_poly_difference(const point2arr &ptarr,vector<point2arr> & ptarrarr) const;
  /// @brief Returns a vector of polygon by reference representing the intersection of the two polygons 
  int get_poly_intersection(const point2arr &ptarr,vector<point2arr> & ptarrarr) const;
  /// @brief Returns a vector of polygon by reference representing the exclusive-or operation for the two polygons 
  int get_poly_xor(const point2arr &ptarr,vector<point2arr> & ptarrarr) const;
  /// @brief Returns a vector of polygon by reference representing the union of the two polygons 
  int get_poly_union(const point2arr &ptarr,vector<point2arr> & ptarrarr) const ;
  
  /// @brief Returns the area inside the polygon
  double get_poly_area() const;

  //--------------------------------------------------
  // point2arr intersect and overlap checking functions
  //--------------------------------------------------
  /// @brief Returns true if the point2arr objects intersect given the assumption that both are connected polylines
  bool is_intersect(const point2arr &ptarr)const;
/// @brief Returns true if the point is inside the point2arr given the assumption that it is a closed polygon
  bool is_inside_poly(const point2& pt) const;
  /// @brief Returns true if there is overlap, assuming the point2arr objects represent closed polygons
  bool is_poly_overlap(const point2arr &ptarr)const;
  bool is_line_poly_overlap(const point2arr &ptarr)const;
  bool is_poly_line_overlap(const point2arr &ptarr)const;
  bool is_point_poly_overlap(const point2arr &ptarr)const;
  bool is_poly_point_overlap(const point2arr &ptarr)const;




    vector<double> get_dist() const;
  vector<double> get_heading() const;
  vector<double> get_relative_heading() const;


  point2arr operator+( const point2 &pt) const; 
  point2arr operator-( const point2 &pt) const;
  point2arr operator*( const double d) const;


 
  double linelength() const; 
  point2 project(const point2& pt) const;
  point2 get_point_along(const double dist) const;
  double project_along(const point2& pt) const;
  int closest_index(const point2& pt) const;
  int get_index_next(const double dist) const;
  // int get_index_last(const double dist) const;
  //int get_index_closest(const double dist) const;

  int cut_front_at_index(const point2& pt, const double dist=0);
  //int cut_front_at_index(const point2& pt, const int index);
  int cut_front_at_index(const int index);

  double cut_front(const point2arr& ptarr);
  double cut_front(const point2& pt, const double delta = 0);
  double cut_front(const double length);
  int cut_back_at_index(const point2& pt, const double dist=0);
  //  int cut_back_at_index(const point2& pt, const int index);
  int cut_back_at_index(const int index);

  double cut_back(const point2arr& ptarr);
  double cut_back(const point2& pt, const double delta = 0);
  double cut_back(const double length);
  
  point2arr get(const int index1, const int index2) const;
  
  void connect(const point2arr& ptarr);
  void connect_intersect(const point2arr &ptarr);

  //void get_side(const point2& pt, int& side, double& index);
  //void get_side(const point2& pt, int& side, double& dist, double& index);
  void get_side(const point2& pt, int& side, int& index) const;
  void get_side(const point2arr& ptarr, int& side) const ;

  //  void get_correspondence_onetoone(const point2arr& ptarr, vector<int>& c1, vector<int>& c2) const;
  void set_extend(const double dist);
 
  void set_arc_curve(const point2 pta, const point2 ptb, const point2 ptc, const int numpts = 10);
  int set_arc_curve(const point2 startpt, const double startang, const point2 endpt, const double endang, const int numpts);

  int set_trim_line_loops(double minsize = 0);
  point2arr get_offset(double d);
  point2arr get_offset_trim(double d);

  vector<point2arr> split(const double d);
  void get_bound_box(point2 &cpt,double &len, double &wid, double &orientation) const;
  point2arr get_bound_box() const;

  double angle2d(const point2& pt1, const point2& pt2) const {
    double th1, th2, dth;
    th1 = atan2(pt1.y, pt1.x);
    th2 = atan2(pt2.y, pt2.x);
    dth = th2 - th1;
    while (dth > M_PI)
      dth -= 2*M_PI;
    while(dth < -M_PI)
      dth += 2*M_PI;
    return dth;
  };

  int apply_min_ptdist(const double dist);

  //laura's new fxns for use in blobladar

  /**
   * Returns mean(x), mean(y) and mean(z) for the array
   **/
  point2 average(); 

  /**
   * splits point2arr into two new arrays, first 
   * one of input length, second one of whatever is left
   **/
  void split_at(int num, point2arr& arr1, point2arr& arr2);

  /**
   * Fits a line to the array, using the formula
   *  found on mathworld.wolfram.com for minimizing
   *  perpendicular distance.
   *
   * Returns:  MSE 
   * Returns by  reference:
   *    a, b (y = a + b * x)
   *    pt1, pt2 -> endpts of arr projected onto line
   *        (note - if line is out of order, these will 
   *          be incorrect)
   */
  double fit_line(double& a, double& b, point2& pt1, point2& pt2);

  /**
   * same as fit_line, but ensures that returned line is
   * perpendicular to input slope
   **/
  double fit_line_perp(double& a, double& b, point2& pt1, point2& pt2, double slope);

  // helper function for fit_line
  void sum_squares(double& ssxx, double& ssxy, double& ssyy);
  /**
   * returns MSE error of points2arr wrt line described
   *  by y = a + b * x
   **/
  double line_error(double a, double b);

  /**
   * checks whether the input point is "inside"
   * the point2arr. This assumes that the point2arr
   * is orderred (but CW/CCW are both ok)
   *
   * Concave polygons are fine
   *
   * Returns: -1 (not inside) 1 (inside)
   **/
  int get_inside(const point2& pt) const;
 
  int get_closest_side(const point2 pt) const;

  //removes the point at given index
  void remove(int index);

  //adds a point, at given index
  void add_element(point2 pt, int index);

  /**
   * Adds points from input arr to this. 
   * Checks to see if input pt is outside the
   * current array before adding it
   **/
  void merge(point2arr ptarr);

  /**
   * Gets overlap between this and ptarr.
   * (pts of arr1 inside arr2, plus pts in arr2
   *   inside arr1)
   **/
  int get_overlap(const point2arr ptarr)const;

};

point2arr operator*(const double d,const point2arr &ptarr);
ostream &operator<<(ostream &os, const point2arr &ptarr);
ostream &operator<<(ostream &os, const vector<double> &vec);
ostream &operator<<(ostream &os, const vector<int> &vec);

//--------------------------------------------------
// simple 2D line class
//--------------------------------------------------
class line2
{
public:
  point2 ptA;
  point2 ptB;
  
  line2(): ptA(0,0), ptB(0,0) {}
  line2(const point2 &pta, const point2 &ptb){
    ptA = pta;
    ptB = ptb;
  }

  ~line2() {}


  line2(const line2 &line){
    ptA = line.ptA;
    ptB = line.ptB;
  }

  
  
  line2 &operator=(const line2 &line){
    if (this!= &line){
      ptA = line.ptA;
      ptB = line.ptB;
    }
    return *this;
  }

  bool operator==(const line2 &line) const
  {
    return ((ptA==line.ptA)&&(ptB==line.ptB));
  }

  bool operator!=(const line2 &line) const
  {
    return ((ptA!=line.ptA)||(ptB!=line.ptB));
  }


  line2 operator+(const point2 &pt) const {
    return line2(ptA+pt, ptB+pt);
  
  }

  line2 operator-(const point2 &pt) const {
    return line2(ptA-pt,ptB-pt);
  
  }

  line2 operator+(const line2 &line) const {
    return line2(ptA+line.ptA, ptB+line.ptB);
  
  }

  line2 operator-(const line2 &line) const {
    return line2(ptA-line.ptA,ptB-line.ptB);
  
  }
  line2 operator*(const double d) const {
    return line2(d*ptA,d*ptB);
  
  }

  line2 operator/(const double d) const {
    return line2(ptA/d,ptB/d);
  
  }


    
  line2 rot(double ang) const { 
    return line2(ptA.rot(ang), ptB.rot(ang));
  }
  

    
  void set(const point2 &pta, const point2 &ptb){
    ptA = pta;
    ptB = ptb;
  }


  void clear() {
    ptA.clear();
    ptB.clear();
  }

  double norm() const {
    point2 pt = (ptB-ptA);
    return pt.norm();
  }

  double heading() const {
    point2 pt = (ptB-ptA);
    if (pt == point2(0,0))
      return 0;

    return pt.heading();
  }

  point2 project(const point2& pt){
    double ang = heading();
    point2 rpta = ptA.rot(-ang);
    point2 rpt = pt.rot(-ang);
    rpt.y = rpta.y;
    return (rpt.rot(ang));
  }
  
  point2 project_bound(const point2& pt){
    double ang = heading();
    point2 rpta = ptA.rot(-ang);
    point2 rptb = ptB.rot(-ang);
    point2 rpt = pt.rot(-ang);
    if(rpt.x>=rpta.x&&rpta.x>=rptb.x)
      return ptA;
    else if(rpt.x<=rpta.x&&rpta.x<=rptb.x)
      return ptA;
    else if(rpt.x>=rptb.x&&rptb.x>=rpta.x)
      return ptB;      
    else if(rpt.x<=rptb.x&&rptb.x<=rpta.x)
      return ptB;
    else
      rpt.y = rpta.y;
    return (rpt.rot(ang));
  }
  
  int is_normal(const point2& pt) const{
    double ang = heading();
    point2 rpta = ptA.rot(-ang);
    point2 rptb = ptB.rot(-ang);
    point2 rpt = pt.rot(-ang);
    if(rpt.x>=rpta.x&&rpt.x<=rptb.x)
      return 1;
    else if(rpt.x>=rptb.x&&rpt.x<=rpta.x)
      return -1;
    
    return 0;
  }
  double dist(const point2& pt) const{
    double ang = heading();

    point2 tpta = ptA - ptA;
    point2 tptb = ptB - ptA;
    point2 tpt  = pt - ptA;

    point2 rpta = tpta.rot(-ang);
    point2 rptb = tptb.rot(-ang);
    point2 rpt = tpt.rot(-ang);

    if (rpt.x>=rpta.x&&rpt.x<=rptb.x)
      return fabs(rpt.y-rpta.y);
    else if (rpt.x>=rptb.x)
      return rptb.dist(rpt);
    else
      return rpta.dist(rpt);
  }

  double max_dist(const point2& pt) const{
    double d1,d2;
    d1 = ptA.dist(pt);
    d2 = ptB.dist(pt);
    if (d1>d2)
      return d1;
    else
      return d2;
    
  }


  double dist(const line2& line) const{
    /* If the segments intersect then the distance is zero */
    if (is_intersect(line)){
      return 0;
    }

    /* Otherwise the minimal distance is achieved by an endpoint */
    double mindist = dist(line.ptA);
    double tmpdist = dist(line.ptB);
    if (tmpdist < mindist) mindist = tmpdist;

    tmpdist = line.dist(ptA);
    if (tmpdist < mindist) mindist = tmpdist;

    tmpdist = line.dist(ptB);
    if (tmpdist < mindist) mindist = tmpdist;

    return mindist;
  }

  double max_dist(const line2& line) const{
    /* If the segments intersect then the distance is zero */
    double d1,d2;

    d1 = max_dist(line.ptA);
    d2 = max_dist(line.ptB);

    if (d1>d2)
      return d1;
    else
      return d2;
  }



  bool is_cross(const line2& line) const{
    double ang = heading();
    
    point2 rpta = ptA.rot(-ang);
    point2 rinpta = line.ptA.rot(-ang);
    point2 rinptb = line.ptB.rot(-ang);
    
    if ((rinpta.y>=rpta.y && rinptb.y<=rpta.y) ||
        (rinpta.y<=rpta.y && rinptb.y>=rpta.y))
      return true;
    return false;
  }

  bool is_intersect(const line2& line) const{

    bool retval;
    line2 tmpline;
    tmpline.ptA = ptA;
    tmpline.ptB = ptB;
   
    retval = is_cross(line) && line.is_cross(tmpline);
    return retval;
  }

  point2 intersect(const line2& line){
    double ang = heading();
    double dang = ang-line.heading();
    double tang = tan(dang);
    point2 outpt;
    
    point2 rpta = ptA.rot(-ang);
    //    point2 rptb = ptB.rot(-ang);
    point2 rinpta = line.ptA.rot(-ang);
    //    point2 rinptb = pt.rot(-ang);
    outpt.y = rpta.y;
    
    if (tang==0){
      cout << "line2::intersect parallel lines found " << endl;
      outpt.x = 1000000;
    }else{
      outpt.x = rinpta.x+(rinpta.y-rpta.y)/tang;
    }
    
    return (outpt.rot(ang));
    
  }

};

ostream &operator<<(ostream &os, const line2 &line);

#endif
