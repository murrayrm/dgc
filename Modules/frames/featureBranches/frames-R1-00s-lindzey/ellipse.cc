#include "ellipse.hh"

using namespace std;

ellipse::ellipse(point2arr ptarr) {
  numPoints = ptarr.size();
  center = ptarr.average();
  point2arr newptarr = point2arr(ptarr - center);
  point2 center2 = newptarr.average();
  //  cout<<"center of newptarr: "<<center2.x<<' '<<center2.y<<endl;
  //sum_squares just sums it, doesn't divide
  newptarr.sum_squares(ssxx, ssyy, ssxy);

  this->finish();
}

void ellipse::add_points(point2arr ptarr) {
  //updating center
  unsigned int numNewPoints = ptarr.size();
  int totalPoints = numPoints + numNewPoints;
  double oldX, oldY, newX, newY, sumX, sumY;
  point2 newCenter = ptarr.average();

  oldX = center.x;
  oldY = center.y;
  newX = newCenter.x;
  newY = newCenter.y;
  
  sumX = oldX * numPoints + newX * numNewPoints;
  sumY = oldY * numPoints + newY * numNewPoints;

  center = point2(sumX/totalPoints, sumY/totalPoints);

  //updating ssxx, ssxy, ssyy
  double newXX, newXY, newYY;
  point2arr newptarr = point2arr(ptarr - center);
  newptarr.sum_squares(newXX, newYY, newXY);
  ssxx += newXX;
  ssxy += newXY;
  ssyy += newYY;

  numPoints = totalPoints;

  this->finish();

  return;
}

#warning "this is untested!"
void ellipse::merge(ellipse newEllipse) {
  //updating center
  unsigned int numNewPoints = newEllipse.numPoints;
  int totalPoints = numPoints + numNewPoints;
  double oldX, oldY, newX, newY, sumX, sumY;

  oldX = center.x;
  oldY = center.y;
  newX = newEllipse.center.x;
  newY = newEllipse.center.y;
  
  sumX = oldX * numPoints + newX * numNewPoints;
  sumY = oldY * numPoints + newY * numNewPoints;

  center = point2(sumX/totalPoints, sumY/totalPoints);

  double dx1, dx2, dy1, dy2;
  dx1 = center.x - oldX;
  dx2 = center.x - newX;
  dy1 = center.y - oldY;
  dy2 = center.y - newY;

  //updating ssxx, ssxy, ssyy
  ssxx += newEllipse.ssxx + numPoints*dx1*dx1 + numNewPoints*dx2*dx2;
  ssxy += newEllipse.ssxy + numPoints*dx1*dy1 + numNewPoints*dy2*dx2;
  ssyy += newEllipse.ssyy  + numPoints*dy1*dy1 + numNewPoints*dy2*dy2;

  numPoints = totalPoints;

  this->finish();

  return;
}

//calculates a,b,theta
void ellipse::finish() {

  //covariances
  double Vxx, Vxy, Vyy;
  Vxx = ssxx / numPoints;
  Vxy = ssxy / numPoints;
  Vyy = ssyy / numPoints;

  //  cout<<"covariances. (xx, xy, yy) = "<<Vxx<<' '<<Vxy<<' '<<Vyy<<endl;

  //eigenvalues
  double foo = (Vxx+Vyy)*(Vxx+Vyy) - 4*(Vxx*Vyy - Vxy *Vxy);
  double lambda1, lambda2;
  lambda1 = (Vxx + Vyy - sqrt(foo))/2;
  lambda2 = (Vxx + Vyy + sqrt(foo))/2;
  //  fprintf(stderr, "eigenvalues %f, %f \n", lambda1, lambda2);

  if(lambda1 == lambda2) {
    fprintf(stderr, "degenerate eigenvalues! %f, %f \n", lambda1, lambda2);
  }

  //eigenvectors
  point2 v1, v2;
  v1 = point2(-Vxy, Vxx - lambda1);
  v2 = point2(-Vxy, Vxx - lambda2);

  v1.normalize();
  v2.normalize();
  
  //  cout<<"v1 = "<<v1.x<<' '<<v1.y<<", and v2 = "<<v2.x<<' '<<v2.y<<endl;

#warning "not robust. find matrix library to take care of eigenvector calcs"
  if(MAXDIFF < (v1.x - v2.y)) {
    //    fprintf(stderr, "a,c not same for rot matrix: %f, %f \n", v1.x, v2.y);
    v1.neg();
  }

  //rotation angle
  double th1, th2;
#warning "check this reasoning"
  //going to use asin, because acos will only result in pos rotations
  th1 = acos(v1.x);
  th2 = asin(v1.y);
  if(th1 != th2) {
    //   fprintf(stderr, "not rotation matrix! th1, th2: %f, %f \n", th1, th2);
  }

  theta = -th2;
  a = sqrt(lambda1);
  b = sqrt(lambda2);

  return;
}

point2arr ellipse::el_border(double margin = 0.0) {
  point2arr outarr;
  int numpts = 64;
  double t;
  point2 newpt, pt;
  //  cout<<"in border, a = "<<a<<" b = "<<b<<" theta = "<<theta<<endl;
  for(int i = 0; i<numpts; i++) {
    t = i * 4 * atan2(1,0)/numpts; 
    pt = point2((margin+a)*cos(t),(margin + b)*sin(t));
    //    cout<<"t = "<<t<<" pi/2 = "<<atan2(1,0)<<" pt.x = "<<pt.x<<" pt.y = "<<pt.y<<endl;
    newpt = pt.rot(theta);
    outarr.push_back(newpt+center);
  }

  return outarr;
}

int ellipse::inside(point2 pt, double margin = 0.0) {
  //  cout<<"called inside. ellipse's center = "<<center.x<<' '<<center.y<<" and pt = "<<pt.x<<' '<<pt.y<<endl;
  point2 shiftpt, rotpt;
  shiftpt = pt - center;
  rotpt = shiftpt.rot(-theta);
  double dist;
  dist = pow((rotpt.x/(a+margin)),2) + pow(rotpt.y / (b+margin),2);

  if(dist > 1)
    return -1;
  else
    return 1;

}

int ellipse::overlap(ellipse* newEllipse, double margin = 0.0) {
  //  cout<<"called inside. ellipse's center = "<<center.x<<' '<<center.y<<" and pt = "<<pt.x<<' '<<pt.y<<endl;
  point2 shiftpt1, rotpt1;
  point2 shiftpt2, rotpt2;
  double dist1, dist2;
 
  shiftpt1 = newEllipse->center - center;

  rotpt1 = shiftpt1.rot(-theta);

  shiftpt2 = center - newEllipse->center;
  rotpt2 = shiftpt2.rot(-newEllipse->theta);

  dist1 = pow((rotpt1.x/(a+margin)),2) + pow(rotpt1.y / (b+margin),2);
  dist2 = pow((rotpt2.x/(newEllipse->a+margin)),2) + pow(rotpt2.y / (newEllipse->b+margin),2);

  if((dist1 < 1) || (dist2 < 1)) {
    return 1;
  } else {
    return -1;
  }
}
