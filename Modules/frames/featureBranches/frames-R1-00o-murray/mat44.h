/*
 * Copyright 2005, by the California Institute of Technology. ALL
 * RIGHTS RESERVED. United States Government Sponsorship
 * acknowledged. Any commercial use must be negotiated with the Office
 * of Technology Transfer at the California Institute of Technology.
 *
 * This software may be subject to U.S. export control laws. By
 * accepting this software, the user agrees to comply with all
 * applicable U.S. export laws and regulations. User has the
 * responsibility to obtain export licenses, or other export authority
 * as may be required before exporting such information to foreign
 * countries or providing access to foreign persons.
 */

/* Desc: Structures and operators for 4x4 matrices
 * Author: Andrew Howard
 * Date: 5 Oct 2006
 * CVS: $Id: vec3.h,v 1.6 2006/10/23 15:19:01 abhoward Exp $
 */

/** @file 

@brief 4x4 matrix structures and operators.

*/

#ifndef MAT44_H
#define MAT44_H

#include <math.h>


/// @brief Create a zero matrix.
///
/// @param[out] dst Destination matrix of zeros.
static __inline__ void mat44_zero(double dst[4][4])
{
  int i, j;
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      dst[j][i] = 0;
  return;
}


/// @brief Initialize double matrix from a matrix of doubles.
///
/// @param[in] src Source matrix of doubles.
/// @param[out] dst Destination matrix.
static __inline__ void mat44d_setd(double dst[4][4], double src[4][4])
{
  int i, j;
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      dst[j][i] = src[j][i];
  return;
}


/// @brief Initialize double matrix from a matrix of floats.
///
/// @param[in] src Source matrix of floats.
/// @param[out] dst Destination matrix.
static __inline__ void mat44d_setf(double dst[4][4], float src[4][4])
{
  int i, j;
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      dst[j][i] = src[j][i];  
  return;
}


/// @brief Initialize float matrix from a matrix of floats.
///
/// @param[in] src Source matrix of floats.
/// @param[out] dst Destination matrix.
static __inline__ void mat44f_setf(float dst[4][4], float src[4][4])
{
  int i, j;
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      dst[j][i] = src[j][i];
  return;
}


/// @brief Initialize float matrix from a matrix of doubles.
///
/// @param[in] src Source matrix of doubles.
/// @param[out] dst Destination matrix.
static __inline__ void mat44f_setd(float dst[4][4], double src[4][4])
{
  int i, j;
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      dst[j][i] = src[j][i];
  return;
}


/// @brief Multiply matrices.
///
/// @param[in] A, B Matrices to multiply.
/// @param[out] C Product matrix @f$ C = AB @f$.
static __inline__ void mat44d_mul(double c[4][4], double a[4][4], double b[4][4])
{
  int i, j;
  double tmp[4][4];
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      tmp[j][i] = a[j][0]*b[0][i] + a[j][1]*b[1][i] + a[j][2]*b[2][i] + a[j][3]*b[3][i];
  mat44d_setd(c, tmp);
  return;
}


/// @brief Multiply matrices.
///
/// @param[in] A, B Matrices to multiply.
/// @param[out] C Product matrix @f$ C = AB @f$.
static __inline__ void mat44f_mul(float c[4][4], float a[4][4], float b[4][4])
{
  int i, j;
  float tmp[4][4];
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      tmp[j][i] = a[j][0]*b[0][i] + a[j][1]*b[1][i] + a[j][2]*b[2][i] + a[j][3]*b[3][i];
  mat44f_setf(c, tmp);
  return;
}


/// @brief Compute matrix determinant.
///
/// @param[in] A Matrix.
/// @returns Returns the matrix determinant.
static __inline__ double mat44d_det(double a[4][4])
{
  double det;
  det = ( + a[0][0] * ( + a[1][1] * (a[2][2] * a[3][3] - a[2][3] * a[3][2]) - a[1][2] * (a[2][1] * a[3][3] - a[2][3] * a[3][1]) + a[1][3] * (a[2][1] * a[3][2] - a[2][2] * a[3][1])) - a[0][1] * ( + a[1][0] * (a[2][2] * a[3][3] - a[2][3] * a[3][2]) - a[1][2] * (a[2][0] * a[3][3] - a[2][3] * a[3][0]) + a[1][3] * (a[2][0] * a[3][2] - a[2][2] * a[3][0])) + a[0][2] * ( + a[1][0] * (a[2][1] * a[3][3] - a[2][3] * a[3][1]) - a[1][1] * (a[2][0] * a[3][3] - a[2][3] * a[3][0]) + a[1][3] * (a[2][0] * a[3][1] - a[2][1] * a[3][0])) - a[0][3] * ( + a[1][0] * (a[2][1] * a[3][2] - a[2][2] * a[3][1]) - a[1][1] * (a[2][0] * a[3][2] - a[2][2] * a[3][0]) + a[1][2] * (a[2][0] * a[3][1] - a[2][1] * a[3][0])));
  return det;
}


/// @brief Compute inverse matrix.
///
/// @param[in] A Source matrix.
/// @param[out] B Destination matrix (inverse of A).
static __inline__ void mat44d_inv(double b[4][4], double a[4][4])
{
  double det;
  double tmp[4][4];
  det = mat44d_det(a);
  tmp[0][0] = ( + ( + a[1][1] * (a[2][2] * a[3][3] - a[2][3] * a[3][2]) - a[1][2] * (a[2][1] * a[3][3] - a[2][3] * a[3][1]) + a[1][3] * (a[2][1] * a[3][2] - a[2][2] * a[3][1]))) / det;
  tmp[0][1] = ( - ( + a[0][1] * (a[2][2] * a[3][3] - a[2][3] * a[3][2]) - a[0][2] * (a[2][1] * a[3][3] - a[2][3] * a[3][1]) + a[0][3] * (a[2][1] * a[3][2] - a[2][2] * a[3][1]))) / det;
  tmp[0][2] = ( + ( + a[0][1] * (a[1][2] * a[3][3] - a[1][3] * a[3][2]) - a[0][2] * (a[1][1] * a[3][3] - a[1][3] * a[3][1]) + a[0][3] * (a[1][1] * a[3][2] - a[1][2] * a[3][1]))) / det;
  tmp[0][3] = ( - ( + a[0][1] * (a[1][2] * a[2][3] - a[1][3] * a[2][2]) - a[0][2] * (a[1][1] * a[2][3] - a[1][3] * a[2][1]) + a[0][3] * (a[1][1] * a[2][2] - a[1][2] * a[2][1]))) / det;
  tmp[1][0] = ( - ( + a[1][0] * (a[2][2] * a[3][3] - a[2][3] * a[3][2]) - a[1][2] * (a[2][0] * a[3][3] - a[2][3] * a[3][0]) + a[1][3] * (a[2][0] * a[3][2] - a[2][2] * a[3][0]))) / det;
  tmp[1][1] = ( + ( + a[0][0] * (a[2][2] * a[3][3] - a[2][3] * a[3][2]) - a[0][2] * (a[2][0] * a[3][3] - a[2][3] * a[3][0]) + a[0][3] * (a[2][0] * a[3][2] - a[2][2] * a[3][0]))) / det;
  tmp[1][2] = ( - ( + a[0][0] * (a[1][2] * a[3][3] - a[1][3] * a[3][2]) - a[0][2] * (a[1][0] * a[3][3] - a[1][3] * a[3][0]) + a[0][3] * (a[1][0] * a[3][2] - a[1][2] * a[3][0]))) / det;
  tmp[1][3] = ( + ( + a[0][0] * (a[1][2] * a[2][3] - a[1][3] * a[2][2]) - a[0][2] * (a[1][0] * a[2][3] - a[1][3] * a[2][0]) + a[0][3] * (a[1][0] * a[2][2] - a[1][2] * a[2][0]))) / det;
  tmp[2][0] = ( + ( + a[1][0] * (a[2][1] * a[3][3] - a[2][3] * a[3][1]) - a[1][1] * (a[2][0] * a[3][3] - a[2][3] * a[3][0]) + a[1][3] * (a[2][0] * a[3][1] - a[2][1] * a[3][0]))) / det;
  tmp[2][1] = ( - ( + a[0][0] * (a[2][1] * a[3][3] - a[2][3] * a[3][1]) - a[0][1] * (a[2][0] * a[3][3] - a[2][3] * a[3][0]) + a[0][3] * (a[2][0] * a[3][1] - a[2][1] * a[3][0]))) / det;
  tmp[2][2] = ( + ( + a[0][0] * (a[1][1] * a[3][3] - a[1][3] * a[3][1]) - a[0][1] * (a[1][0] * a[3][3] - a[1][3] * a[3][0]) + a[0][3] * (a[1][0] * a[3][1] - a[1][1] * a[3][0]))) / det;
  tmp[2][3] = ( - ( + a[0][0] * (a[1][1] * a[2][3] - a[1][3] * a[2][1]) - a[0][1] * (a[1][0] * a[2][3] - a[1][3] * a[2][0]) + a[0][3] * (a[1][0] * a[2][1] - a[1][1] * a[2][0]))) / det;
  tmp[3][0] = ( - ( + a[1][0] * (a[2][1] * a[3][2] - a[2][2] * a[3][1]) - a[1][1] * (a[2][0] * a[3][2] - a[2][2] * a[3][0]) + a[1][2] * (a[2][0] * a[3][1] - a[2][1] * a[3][0]))) / det;
  tmp[3][1] = ( + ( + a[0][0] * (a[2][1] * a[3][2] - a[2][2] * a[3][1]) - a[0][1] * (a[2][0] * a[3][2] - a[2][2] * a[3][0]) + a[0][2] * (a[2][0] * a[3][1] - a[2][1] * a[3][0]))) / det;
  tmp[3][2] = ( - ( + a[0][0] * (a[1][1] * a[3][2] - a[1][2] * a[3][1]) - a[0][1] * (a[1][0] * a[3][2] - a[1][2] * a[3][0]) + a[0][2] * (a[1][0] * a[3][1] - a[1][1] * a[3][0]))) / det;
  tmp[3][3] = ( + ( + a[0][0] * (a[1][1] * a[2][2] - a[1][2] * a[2][1]) - a[0][1] * (a[1][0] * a[2][2] - a[1][2] * a[2][0]) + a[0][2] * (a[1][0] * a[2][1] - a[1][1] * a[2][0]))) / det;
  mat44d_setd(b, tmp);
  return;
}


#endif
