/**********************************************************
 **
 **  POINT2.H
 **
 **    Time-stamp: <2007-02-20 20:42:01 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 09:26:14 2007
 **
 **
 **********************************************************
 **
 **  Simple 2D point struct
 **
 **********************************************************/


#ifndef POINT2_HH
#define POINT2_HH

#include <math.h>
#include "point2_uncertain.hh"
using namespace std;

class point2
{
public:
  double x, y;

	point2() {}
	point2(double xin, double yin) : x(xin), y(yin) {}

	~point2() {}


	point2(const point2 &pt){
		x = pt.x;
		y = pt.y;
	}

	//point2(const point2_uncertain &pt){
	//	x = pt.x;
	//	y = pt.y;
	//}

	point2 &operator=(const point2 &pt){
		if (this!= &pt){
			x = pt.x;
			y = pt.y;
		}
		return *this;
	}
	//	point2 &operator=(const point2_uncertain &pt) {
	//	x = pt.x;
	//	y = pt.y;
	//	return *this;
	//}

	point2 operator+(const point2 &pt) const {
		return point2(x+pt.x, y+pt.y);
	
	}
	//point2 &operator+(const point2_uncertain &pt) const {
	//	point2 ptout;
	//	ptout.x = x + pt.x;
	//	ptout.y = y + pt.y;
	//	return ptout;
	//}


	point2 operator-(const point2 &pt) const {
		return point2(x-pt.x,y-pt.y);
	
	}

	point2 operator*(const double d) const {
		return point2(d*x,d*y);
	
	}

	point2 operator/(const double d) const {
		return point2(x/d,y/d);
	
	}

	//point2 &operator-(const point2_uncertain &pt) const {
	//	point2 ptout;
	//	ptout.x = x - pt.x;
	//	ptout.y = y - pt.y;
	//	return ptout;
	//}
		
	point2 rot(double ang) const { 
		return point2(x*cos(ang)-y*sin(ang), x*sin(ang)+y*cos(ang));
	}
	
	void set(double xin, double yin){
		x = xin;
		y = yin;
	}

	void set(double val){
		x = val;
		y = val;
	}
	void clear() {
		x = 0;
		y = 0;
	}
	double norm() const {
		return hypot(x,y);
	}

	double heading() const {
		return atan2(y,x);
	}
}; 



#endif
