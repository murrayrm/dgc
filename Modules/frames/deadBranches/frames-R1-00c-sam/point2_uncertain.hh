/**********************************************************
 **
 **  POINT2_UNCERTAIN.HH
 **
 **    Time-stamp: <2007-02-21 09:51:12 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 09:37:44 2007
 **
 **
 **********************************************************
 **
 **  Simple 2D position covariance structure
 **
 **********************************************************/


#ifndef POINT2_UNCERTAIN_HH
#define POINT2_UNCERTAIN_HH

#include <math.h>
#include "point2.hh"

using namespace std;

class point2_uncertain
{
public:
	double x, y;
  double max_var, min_var, axis;

	point2_uncertain() {}
	point2_uncertain(double xin, double yin) 
		: x(xin), y(yin) {}
	point2_uncertain(double xin, double yin, double var) 
		: x(xin), y(yin), max_var(var), min_var(var), axis(0) {}
	point2_uncertain(double xin, double yin, double max, double min, double axis) 
		: x(xin), y(yin), max_var(max), min_var(min), axis(axis) {}

	~point2_uncertain() {}


	point2_uncertain(const point2_uncertain &pt){
		x = pt.x;
		y = pt.y;
		max_var = pt.max_var;
		min_var = pt.min_var;
		axis = pt.axis;
	}

	point2_uncertain(const point2 &pt){
		x = pt.x;
		y = pt.y;
		max_var = 0;
		min_var = 0;
		axis = 0;
	}

	point2_uncertain &operator=(const point2_uncertain &pt){
		if (this!= &pt){
			x = pt.x;
			y = pt.y;
			max_var = pt.max_var;
			min_var = pt.min_var;
			axis = pt.axis;
		}
		return *this;
	}
	point2_uncertain &operator=(const point2 &pt) {
		x = pt.x;
		y = pt.y;
		max_var = 0;
		min_var = 0;
		axis = 0;
		return *this;
	}

	point2_uncertain operator+(const point2_uncertain &pt) const {
		
		//--------------------------------------------------
		// need to finish this
		//--------------------------------------------------
		return point2_uncertain(x+pt.x,y+pt.y);
	
	}
	point2_uncertain operator+(const point2 &pt) const {
		return point2_uncertain(x+pt.x, y+pt.y, max_var, min_var, axis);
	}

	

	point2_uncertain rot(double ang) const { 
		double thisx = x*cos(ang)-y*sin(ang);
		double thisy = x*sin(ang)+y*cos(ang);
		return point2_uncertain(thisx, thisy, max_var, min_var, axis+ang);
	}

	point2 point(){
		return point2(x,y);
	}

	void clear() {
		x = 0;
		y = 0;
		max_var = 0;
		min_var = 0;
		axis = 0;
	}
	
	double norm() const {
		return hypot(x,y);
	}

	double heading() const {
		return atan2(y,x);

	}
	
	void print() const {
		cout << "x= " << x
				 << " y= " << y  
				 << " max_var= " << max_var  
				 << " min_var= " << min_var  
				 << " axis= " << axis << endl; 
	}
};


#endif
