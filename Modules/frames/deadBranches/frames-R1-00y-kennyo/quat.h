/*
 * Copyright 2005, by the California Institute of Technology. ALL
 * RIGHTS RESERVED. United States Government Sponsorship
 * acknowledged. Any commercial use must be negotiated with the Office
 * of Technology Transfer at the California Institute of Technology.
 *
 * This software may be subject to U.S. export control laws. By
 * accepting this software, the user agrees to comply with all
 * applicable U.S. export laws and regulations. User has the
 * responsibility to obtain export licenses, or other export authority
 * as may be required before exporting such information to foreign
 * countries or providing access to foreign persons.
 */

/* Desc: Simple quaternion structures and operators
 * Author: Andrew Howard
 * Date: 5 Oct 2006
 * CVS: $Id: quat.h,v 1.7 2006/12/06 16:43:11 abhoward Exp $
 */


/** @file

@brief Quaternion structures and operators.

@sa vec3.h pose3.h

A quaterion is a generalized complex number in which @f$ u @f$ is the
real component and @f$ (x, y, z) @f$ are imaginary components.

@f[ q = u + xi + yj + zk @f]

The imaginary numbers @f$(i, j, k)@f$ have the property that:

@f[ i^2 = j^2 = k^2 = ijk = -1 @f]

Note that quaternions are non-commutative, i.e.:

@f[ pq \neq qp @f]

Quaternions can be used to represent rotations, in which case
a rotation @f$ \theta @f$ around an axis @f$ w = (x, y, z) @f$
is represented by the quaternion:

@f[ \cos(\theta/2) + \sin(\theta/2) xi + \sin(\theta/2) yj + \sin(\theta/2) kz @f]

To rotate a vector @f$ v = (a, b, c) @f$ using this quaternion, multiply as follows:

@f[ v' = q v q^\dagger = (u + xi + yj + zk) (ai + bj + ck) (u - xi - yj - zk) @f]

*/

#ifndef QUAT_H
#define QUAT_H

#include <math.h>


/// @brief Quaternion with floating point elements.
///
/// Represents a quaterion @f$ u + ix + jy + kz @f$, where @f$ u @f$ is
/// the real component and @f$ (x, y, z) @f$ are the imaginary components. 
typedef struct
{
  double u, x, y, z;  
} quat_t;



/// @brief Identity quaternion.
///
/// @returns Returns the identity quaternion: @f$ 1 + 0i + 0j + 0k @f$
static __inline__ quat_t quat_ident()
{
  quat_t a = {1, 0, 0, 0};
  return a;
}


/// @brief Set the quaternion elements.
///
/// @param[in] u Real component.
/// @param[in] x,y,z Complex components.
/// @returns Returns quaterion @f$ u + xi + yj + zk @f$
static __inline__ quat_t quat_set(double u, double x, double y, double z)
{
  quat_t a = {u, x, y, z};
  return a;
}


/// @brief Set the quaternion elements from an array of doubles.
///
/// @returns Returns quaternion @f$ q[0] + q[1] i + q[1] j + q[1] k @f$.
static __inline__ quat_t quat_setv(double *q)
{
  quat_t a = {q[0], q[1], q[2], q[3]};
  return a;
}


/// @brief Set the quaternion elements from an array of floats.
///
/// @returns Returns quaternion @f$ q[0] + q[1] i + q[1] j + q[1] k @f$.
static __inline__ quat_t quat_setvf(float *q)
{
  quat_t a = {q[0], q[1], q[2], q[3]};
  return a;
}


/// @brief Test for finite quaternion.
///
/// Tests that all the components are valid and finite.
/// @param[in] q Quaternion to test.
/// @return Returns non-zero if the quaternion is finite.
static __inline__ int quat_finite(quat_t q)
{
  return finite(q.u) && finite(q.x) && finite(q.y) && finite(q.z);
}


/// @brief Add quaternions.
///
/// @param[in] a, b Quaternions to add.
/// @returns Returns the sum @f$ a + b @f$
static __inline__ quat_t quat_add(quat_t a, quat_t b)
{
  quat_t c = {a.u + b.u, a.x + b.x, a.y + b.y, a.z + b.z};
  return c;
}


/// @brief Subtract quaternions.
///
/// @param[in] a, b Quaternions to subtract.
/// @returns Returns the difference @f$ a - b @f$
static __inline__ quat_t quat_sub(quat_t a, quat_t b)
{
  quat_t c = {a.u - b.u, a.x - b.x, a.y - b.y, a.z - b.z};
  return c;
}


/// @brief Multiply quaternions.
///
/// @param[in] a, b Quaternions to multiply.
/// @returns Returns the product @f$ ab @f$
static __inline__ quat_t quat_mul(quat_t a, quat_t b)
{
  quat_t c = {a.u*b.u - a.x*b.x - a.y*b.y - a.z*b.z,
               a.u*b.x + a.x*b.u + a.y*b.z - a.z*b.y,
               a.u*b.y - a.x*b.z + a.y*b.u + a.z*b.x,
               a.u*b.z + a.x*b.y - a.y*b.x + a.z*b.u};
  return c;
}


/// @brief Compute the complex conjugate of a quaternion.
///
/// @param[in] a Input quaternion.
/// @returns Returns the conjugate @f$ a^\dagger = (u, -x, -y, -z) @f$
static __inline__ quat_t quat_conj(quat_t a)
{
  quat_t c = {a.u, -a.x, -a.y, -a.z};
  return c;
}


/// @brief Compute the magnitude (modulus) of a quaternion.
///
/// @param a Input quaternion.
/// @returns Returns the scalar @f$ |a| = \sqrt{a \cdot a} @f$
static __inline__ double quat_mag(quat_t a)
{
  return sqrt(a.u*a.u + a.x*a.x + a.y*a.y + a.z*a.z);
}


/// @brief Compute the unit quaternion.
///
/// @param[in] a Input quaternion.
/// @returns Returns the unit quaternion @f$ a / |a| @f$
static __inline__ quat_t quat_unit(quat_t a)
{
  double s = sqrt(a.u*a.u + a.x*a.x + a.y*a.y + a.z*a.z);
  quat_t c = {a.u/s, a.x/s, a.y/s, a.z/s};
  return c;
}

/// @brief Create a quaternion from Euler angles (yaw, pitch, roll).
///
/// @deprecated
///
/// Create quaternion from Euler angles, with roll applied first,
/// pitch applied second and yaw applied last.  This is the most common
/// Euler convention for robotics applications.
///
/// @param[in] yaw, pitch, roll Euler angles.
/// @returns Returns a new quaternion.
static __inline__ quat_t quat_from_ypr(double yaw, double pitch, double roll)
{
  quat_t a;
  double phi, the, psi;
  phi = roll / 2;
  the = pitch / 2;
  psi = yaw / 2;
  a.u = cos(phi) * cos(the) * cos(psi) + sin(phi) * sin(the) * sin(psi);
  a.x = sin(phi) * cos(the) * cos(psi) - cos(phi) * sin(the) * sin(psi);
  a.y = cos(phi) * sin(the) * cos(psi) + sin(phi) * cos(the) * sin(psi);
  a.z = cos(phi) * cos(the) * sin(psi) - sin(phi) * sin(the) * cos(psi);
  return a;
}


/// @brief Create a quaternion from Euler angles (roll, pitch, yaw).
///
/// Create quaternion from Euler angles, with roll applied first,
/// pitch applied second and yaw applied last.  This is the most common
/// Euler convention for robotics applications.
///
/// @param[in] roll, pitch, yaw Euler angles.
/// @returns Returns a new quaternion.
static __inline__ quat_t quat_from_rpy(double roll, double pitch, double yaw)
{
  quat_t a;
  double phi, the, psi;
  phi = roll / 2;
  the = pitch / 2;
  psi = yaw / 2;
  a.u = cos(phi) * cos(the) * cos(psi) + sin(phi) * sin(the) * sin(psi);
  a.x = sin(phi) * cos(the) * cos(psi) - cos(phi) * sin(the) * sin(psi);
  a.y = cos(phi) * sin(the) * cos(psi) + sin(phi) * cos(the) * sin(psi);
  a.z = cos(phi) * cos(the) * sin(psi) - sin(phi) * sin(the) * cos(psi);
  return a;
}


/// @brief Compute the Euler angles for a quaternion (yaw, pitch, roll).
///
/// @deprecated
///
/// Create Euler angles from a quaternion, with roll applied first,
/// pitch applied second and yaw applied last.  This is the most common
/// Euler convention for robotics applications.
///
/// @param[in] q Quaternion.
/// @param[out] yaw, pitch, roll Euler angles.
static __inline__ void quat_to_ypr(quat_t q, double *yaw, double *pitch, double *roll)
{
  *roll = atan2(2 * (q.y*q.z + q.u*q.x), (q.u*q.u - q.x*q.x - q.y*q.y + q.z*q.z));
  *pitch = asin(-2 * (q.x*q.z - q.u * q.y));
  *yaw = atan2(2 * (q.x*q.y + q.u*q.z), (q.u*q.u + q.x*q.x - q.y*q.y - q.z*q.z));  
  return;
}


/// @brief Compute the Euler angles for a quaternion (roll, pitch, yaw).
///
/// Create Euler angles from a quaternion, with roll applied first,
/// pitch applied second and yaw applied last.  This is the most common
/// Euler convention for robotics applications.
///
/// @param[in] q Quaternion.
/// @param[out] roll, pitch, yaw Euler angles.
static __inline__ void quat_to_rpy(quat_t q, double *roll, double *pitch, double *yaw)
{
  *roll = atan2(2 * (q.y*q.z + q.u*q.x), (q.u*q.u - q.x*q.x - q.y*q.y + q.z*q.z));
  *pitch = asin(-2 * (q.x*q.z - q.u * q.y));
  *yaw = atan2(2 * (q.x*q.y + q.u*q.z), (q.u*q.u + q.x*q.x - q.y*q.y - q.z*q.z));  
  return;
}


/// @brief Create a quaternion from a 3x3 rotation matrix of doubles.
///
/// @param[in] m Rotation matrix.
/// @returns Returns a new quaternion.
static __inline__ quat_t quat_from_mat33d(double m[3][3])
{
  quat_t b;
  double t, s;
  t = 1 + m[0][0] + m[1][1] + m[2][2];
  if (t > 1e-8)
  {
    s = 0.5 / sqrt(t);
    b.u = 0.25 / s;
    b.x = (m[2][1] - m[1][2]) * s;
    b.y = (m[0][2] - m[2][0]) * s;
    b.z = (m[1][0] - m[0][1]) * s;
  }
  else if (m[0][0] > m[1][1] && m[0][0] > m[2][2])
  {
    s = sqrt(1 + m[0][0] - m[1][1] - m[2][2]) * 2;
    b.x = 0.25 * s;
    b.y = (m[0][1] + m[1][0]) / s;
    b.z = (m[0][2] + m[2][0]) / s;
    b.u = (m[2][1] - m[1][2]) / s;    
  }
  else if (m[1][1] > m[2][2])
  {
    s = sqrt(1 + m[1][1] - m[0][0] - m[2][2]) * 2;
    b.x = (m[0][1] + m[1][0]) / s;
    b.y = 0.25 * s;
    b.z = (m[1][2] + m[2][1]) / s;
    b.u = (m[0][2] - m[2][0]) / s;    
  }
  else
  {
    s = sqrt(1 + m[2][2] - m[0][0] - m[1][1]) * 2;
    b.x = (m[0][2] + m[2][0]) / s;
    b.y = (m[1][2] + m[2][1]) / s;
    b.z = 0.25 * s;
    b.u = (m[1][0] - m[0][1]) / s;    
  }
  return quat_unit(b);
}


/// @brief Create a quaternion from a 3x3 rotation matrix of floats.
static __inline__ quat_t quat_from_mat33f(float m[3][3])
{
  quat_t b;
  float t, s;
  t = 1 + m[0][0] + m[1][1] + m[2][2];
  if (t > 1e-8)
  {
    s = 0.5 / sqrtf(t);
    b.u = 0.25 / s;
    b.x = (m[2][1] - m[1][2]) * s;
    b.y = (m[0][2] - m[2][0]) * s;
    b.z = (m[1][0] - m[0][1]) * s;
  }
  else if (m[0][0] > m[1][1] && m[0][0] > m[2][2])
  {
    s = sqrtf(1 + m[0][0] - m[1][1] - m[2][2]) * 2;
    b.x = 0.25 * s;
    b.y = (m[0][1] + m[1][0]) / s;
    b.z = (m[0][2] + m[2][0]) / s;
    b.u = (m[2][1] - m[1][2]) / s;    
  }
  else if (m[1][1] > m[2][2])
  {
    s = sqrtf(1 + m[1][1] - m[0][0] - m[2][2]) * 2;
    b.x = (m[0][1] + m[1][0]) / s;
    b.y = 0.25 * s;
    b.z = (m[1][2] + m[2][1]) / s;
    b.u = (m[0][2] - m[2][0]) / s;    
  }
  else
  {
    s = sqrtf(1 + m[2][2] - m[0][0] - m[1][1]) * 2;
    b.x = (m[0][2] + m[2][0]) / s;
    b.y = (m[1][2] + m[2][1]) / s;
    b.z = 0.25 * s;
    b.u = (m[1][0] - m[0][1]) / s;    
  }
  return quat_unit(b);
}


/// @brief Create a 3x3 rotation matrix of doubles from a unit
/// quaternion.
///
/// @param[in] q Unit quaternion
/// @param[out] m Rotation matrix
static __inline__ void quat_to_mat33d(quat_t q, double m[3][3])
{
  double tmp1, tmp2;
  double squ, sqx, sqy, sqz;
  squ = q.u*q.u;
  sqx = q.x*q.x;
  sqy = q.y*q.y;
  sqz = q.z*q.z;
  m[0][0] =  sqx - sqy - sqz + squ;
  m[1][1] = -sqx + sqy - sqz + squ;
  m[2][2] = -sqx - sqy + sqz + squ;
  tmp1 = q.x*q.y;
  tmp2 = q.z*q.u;
  m[1][0] = 2.0 * (tmp1 + tmp2);
  m[0][1] = 2.0 * (tmp1 - tmp2);
  tmp1 = q.x*q.z;
  tmp2 = q.y*q.u;
  m[2][0] = 2.0 * (tmp1 - tmp2);
  m[0][2] = 2.0 * (tmp1 + tmp2);
  tmp1 = q.y*q.z;
  tmp2 = q.x*q.u;
  m[2][1] = 2.0 * (tmp1 + tmp2);
  m[1][2] = 2.0 * (tmp1 - tmp2);
  return;
}


/// @brief Create a 3x3 rotation matrix of floats from a unit
/// quaternion.
static __inline__ void quat_to_mat33f(quat_t q, float m[3][3])
{
  double tmp1, tmp2;
  double squ, sqx, sqy, sqz;
  squ = q.u*q.u;
  sqx = q.x*q.x;
  sqy = q.y*q.y;
  sqz = q.z*q.z;
  m[0][0] =  sqx - sqy - sqz + squ;
  m[1][1] = -sqx + sqy - sqz + squ;
  m[2][2] = -sqx - sqy + sqz + squ;
  tmp1 = q.x*q.y;
  tmp2 = q.z*q.u;
  m[1][0] = 2.0 * (tmp1 + tmp2);
  m[0][1] = 2.0 * (tmp1 - tmp2);
  tmp1 = q.x*q.z;
  tmp2 = q.y*q.u;
  m[2][0] = 2.0 * (tmp1 - tmp2);
  m[0][2] = 2.0 * (tmp1 + tmp2);
  tmp1 = q.y*q.z;
  tmp2 = q.x*q.u;
  m[2][1] = 2.0 * (tmp1 + tmp2);
  m[1][2] = 2.0 * (tmp1 - tmp2);
  return;
}


#endif
