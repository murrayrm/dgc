/**********************************************************
 **
 **  POINT2.CC
 **
 **    Time-stamp: <2007-02-28 17:02:02 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 27 10:52:43 2007
 **
 **
 **********************************************************
 **
 **   Simple 2D point class
 **
 **********************************************************/

#include <point2.hh>


using namespace std;





point2 operator*(const double d, const point2& pt)
{
	return pt*d;
}
istream &operator>>(istream &is, point2 &pt) {
	is >> pt.x >> pt.y; 
	return is;
}
ostream &operator<<(ostream &os, const point2 &pt) {
	os << "(" << pt.x << ", " << pt.y << ")";
	return os;
}

point2 &point2::operator=(const point2_uncertain &pt){
	x = pt.x;
	y = pt.y;
		
	return *this;
}
point2 point2::operator+(const point2_uncertain &pt) const{
	return point2(x+pt.x,y+pt.y);
}

point2 point2::operator-(const point2_uncertain &pt) const{
	return point2(x-pt.x,y-pt.y);
}



point2arr &point2arr::operator=(const point2arr_uncertain &ptarr){
	this->resize(ptarr.size());
	for(unsigned int i=0;i<ptarr.size();++i){
		arr[i]=ptarr[i];
	}
	return *this;
}



point2arr point2arr::rot(const double ang) const
{
	point2arr outarr;
	unsigned int i;
	for (i=0;i<arr.size();++i){
		outarr.push_back(arr[i].rot(ang));
	}
	return outarr;
}

vector<double> point2arr::norm() const
{
	vector<double> outarr;
	unsigned int i;
	for (i=0;i<arr.size();++i){
		outarr.push_back(arr[i].norm());
	}
	return outarr;
}

vector<double> point2arr::dist(const point2 &pt) const
{
	vector<double> outarr;
	unsigned int i;
	for (i=0;i<arr.size();++i){
		outarr.push_back(arr[i].dist(pt));
	}
	return outarr;
}

point2arr point2arr::operator+(const point2 &pt) const
{
	point2arr outarr;
	unsigned int i;
	for (i=0;i<arr.size();++i){
		outarr.push_back(arr[i]+pt);
	}
	return outarr;
}

point2arr point2arr::operator-(const point2 &pt) const
{
	point2arr outarr;
	unsigned int i;
	for (i=0;i<arr.size();++i){
		outarr.push_back(arr[i]-pt);
	}
	return outarr;
}


point2arr point2arr::operator*(const double d) const
{
	point2arr outarr;
	unsigned int i;
	for (i=0;i<arr.size();++i){
		outarr.push_back(arr[i]*d);
	}
	return outarr;
}

void point2arr::get_side(const point2& pt,  int& side, double& index)
{
	unsigned int i;
	unsigned int arrsize = this->size();
	double thiscross, thisdot, thisdist, norm0;
	point2 pt0;
	point2 dpt0, dpt1, dpt2;
	double mindist;
	double thisindex;
	int thisside;
	if (arrsize<2){
		side = 0;
		index = 0;
		return;
	}

	dpt2 = pt-arr[0];
	mindist = dpt2.norm();
	index = 0;
	side = 0;

	for (i=0;i<arrsize-1;++i){	 
		dpt0 = arr[i+1]-arr[i];
		dpt1 = dpt2;
		dpt2 = pt-arr[i+1];
		norm0 = dpt0.norm();
		thisdot = dpt0.dot(dpt1);
		thiscross = dpt0.cross(dpt1);
		thisside = (thiscross ==0) ? 0 : (thiscross<0 ? -1:1);	 
		if (thisdot>=0 && (thisdot/norm0)<=norm0){
			thisdist = fabs(thiscross/norm0);
			thisindex= (double)i+fabs(thisdot/(norm0*norm0));

		}else if ((thisdot/norm0)>norm0){
			thisdist = dpt2.norm();
			thisindex = (double)i+1;
		}else{
			thisdist = mindist+1;
			thisindex = i;
		}
		//		cout << "thisindex = " << thisindex 
		//			 << "  thisdist = " << thisdist << endl;
		if (thisdist<mindist){
			mindist = thisdist;
			index = thisindex;
			side = thisside;
		}	
		//	cout << "index = " << index 
		//				 << "  mindist = " << mindist << endl;
	}
}

point2arr point2arr::get_intersect(const point2arr &ptarr)
{
	unsigned int sizeA = size();
	unsigned int sizeB = ptarr.size();
	//	unsigned int i,j;
	point2arr outarr;
		
	if (sizeA>1 & sizeB>1){
		point2 ptA0,ptA1,ptB0,ptB1;
		point2 ptA0_rot,ptA1_rot,ptB0_rot,ptB1_rot;
		point2 dptA, dptB_rot,newpt, newpt_rot;
		double angA;

		ptA0 = arr[0];
		ptA1 = arr[1];

		ptB0 = ptarr[0];
		ptB1 = ptarr[1];
		
		dptA = ptA1-ptA0;
		angA = dptA.heading();
		
		ptA0_rot = ptA0.rot(-angA);
		ptA1_rot = ptA1.rot(-angA);
		ptB0_rot = ptB0.rot(-angA);
		ptB1_rot = ptB1.rot(-angA);

		dptB_rot = ptB1_rot-ptB0_rot;
		newpt_rot.y = ptA0_rot.y;
		if (dptB_rot.y!=0){
			newpt_rot.x = (newpt_rot.y-ptB0_rot.y)*
				dptB_rot.x/dptB_rot.y+ptB0_rot.x;
			newpt = newpt_rot.rot(angA);
			outarr.push_back(newpt);
		}

	}

	return outarr;
}



point2arr point2arr::get_offset(double d)
{
	point2arr outarr;
	unsigned int arrsize = this->size();
	unsigned int i;
	point2 pt0, pt1;
	point2 offpt0A, offpt1A, offpt0B, offpt1B;
	double angA, angB;
	point2 dptA, dptB, newpt;
 
	if (arrsize>=2){
		pt0 = arr[0];
		pt1 = arr[1];
		dptB = pt1-pt0;
		angB = dptB.heading();
			
		offpt0B = pt0.rot(-angB);
		offpt1B = pt1.rot(-angB);
		offpt0B.y = offpt0B.y+d;
		offpt1B.y = offpt1B.y+d;
		offpt0B = offpt0B.rot(angB);
		offpt1B = offpt1B.rot(angB);
		
		outarr.push_back(offpt0B);

		for (i=1;i<arrsize-1;++i){

			offpt0A = offpt0B;
			offpt1A = offpt1B;

			pt0 = pt1;
			pt1 = arr[i+1];

			dptB = pt1-pt0;
			angB = dptB.heading();
			
			offpt0B = pt0.rot(-angB);
			offpt1B = pt1.rot(-angB);
			offpt0B.y = offpt0B.y+d;
			offpt1B.y = offpt1B.y+d;

			offpt0A = offpt0A.rot(-angB);
			offpt1A = offpt1A.rot(-angB);

			dptA = offpt1A-offpt0A;



			newpt.y = offpt0B.y;

		
			if (fabs(dptA.y) <.000001){
				newpt.x = offpt0B.x;
			}else{
				newpt.x = (newpt.y-offpt1A.y)*
					dptA.x/dptA.y+offpt1A.x;
			}
		
			newpt = newpt.rot(angB);

			outarr.push_back(newpt);

			offpt0B = offpt0B.rot(angB);
			offpt1B = offpt1B.rot(angB);
					
			
		
			
			
		}
		outarr.push_back(offpt1B);
	}
	
	
	return outarr;
	
}

	point2arr operator*(const double d,const point2arr &ptarr)
		{
			return ptarr*d;
		}

	ostream &operator<<(ostream &os, const point2arr &ptarr) {
		unsigned int i;
	
		for (i=0;i<ptarr.size();++i){
			os <<endl << "[" << i << "] "  << ptarr[i];
		}
		return os;
	}


	ostream &operator<<(ostream &os, const vector<double> &vec) {
		unsigned int i;
	
		for (i=0;i<vec.size();++i){
			os <<endl << "[" << i << "] "  << vec[i];
		}
		return os;
	}

	ostream &operator<<(ostream &os, const vector<int> &vec) {
		unsigned int i;
	
		for (i=0;i<vec.size();++i){
			os <<endl << "[" << i << "] "  << vec[i];
		}
		return os;
	}

	
