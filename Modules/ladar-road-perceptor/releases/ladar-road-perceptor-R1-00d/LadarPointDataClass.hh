/* This class computes the Hough transform on x-y data from the ladars. */

#include <cv.h>
#include <highgui.h>
#include <math.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;



//#include "gnuplot_i.h"

// opencv constants
#define FLOAT_MAT_TYPE CV_32FC1
#define FLOAT_MAT_ELEM_TYPE float
#define INT_MAT_TYPE CV_8UC1
#define INT_MAT_ELEM_TYPE unsigned char
#define FLOAT_POINT2D CvPoint2D32f
#define FLOAT float
#define INT int
#define SHORT_INT unsigned char

// Other constants
#define WINDOWSIZE 41
#define MIDDLEWIN 20

// LadarPointDataClass constant
#define DEFAULT_FLOAT_MAT_ELEM 1000
#define SQUARE(x) ((x) * (x))
#define ABS(x) (((x) < 0) ? -(x) : (x))  

class LadarPointDataClass {
public:
  // Constructor
  LadarPointDataClass(); 
    
  // Destructor 
  ~LadarPointDataClass();
    
  // Set new xData and yData
  void setNewLadarDataPoints(FLOAT xData[], FLOAT yData[], INT nbrData);

  // Binarize the data. For plotting with opencv and for Hough transform.
  void makeBitMap(FLOAT xShiftData, FLOAT yShiftData, FLOAT scaleData, INT width, INT height);

  // Overloaded method
  void makeBitMap();
  
  // Get bit map for plotting using opencv
  
  CvMat* getBitMap();

  // Compute the Hough Transform
  void computeHoughTransform(FLOAT rMin, FLOAT rMax, FLOAT rStep, FLOAT thetaMin, FLOAT thetaMax, FLOAT thetaStep);
  
  // Get the Hough Transform
  CvMat* getHoughTransform(INT scale, INT& rBins, INT& thetaBins);

  // Compute angle data spectrum
  void computeAngleDataSpectrum(FLOAT threshold);

  // Compute window for smoothing
  void computeSmoothingWindow(FLOAT sigma);

  // Smooth angle data spectrum
  void smoothAngleDataSpectrum();

  // Get angleSpectrum
  FLOAT * getAngleSpectrum(FLOAT* angles, INT& nbrAngles);

  // Get direction information from angle spectrum
  FLOAT getDirectionData(FLOAT& deviation, FLOAT& mass, FLOAT& fractionOfData, INT peak_separation);

  // Get direction information from angle spectrum. Newer method for deviation computation.
  // Uses the smoothed spectrum.
  FLOAT getDirectionData2(FLOAT& mass, FLOAT& fractionOfData, FLOAT& leftDev, FLOAT& rightDev);

  // Remove the current peak in the angleSpectrum (should be done after getDirectionData has found a peak.)
  void removeCurrentAngleSpectrumPeak();
  
  // Remove the current peak in the smoothSpectrum (should be done after getDirectionData has found a peak.)
  void removeCurrentSmoothSpectrumPeak();
private:
  // mod for negative number modified.
  INT mod(INT a, INT b);
  FLOAT dist_on_circle(FLOAT a, FLOAT b, FLOAT base);  

  FLOAT* xData; // in data from ladar
  FLOAT* yData; // in data from ladar
  INT* row; // coordinates of ladar data in binary image
  INT* col;  // coordinates of ladar data in binary image
  INT nbrData; // number of ladar points
  FLOAT xShiftData, yShiftData; // shift in ref
  FLOAT scaleData; // scale data for binarization
  INT height, width; // size of opencv image
  CvMat* houghSpace;
  INT rBins, thetaBins;
  FLOAT* rs;
  FLOAT* thetas;
  FLOAT thetaStep;
  uchar init; 
  FLOAT* angleSpectrum;
  FLOAT* smoothSpectrum; 
  INT start_peak;
  INT end_peak;
  INT peak_index;
  FLOAT mass_angleSpectrum;
  FLOAT  mass_smoothSpectrum;
  FLOAT window[WINDOWSIZE];
};
