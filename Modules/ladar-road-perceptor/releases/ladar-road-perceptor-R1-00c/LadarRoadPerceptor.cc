/**********************************************************
 **
 **  LADARROADPERCEPTOR.CC
 **
 **    Time-stamp: <2007-06-06 22:41:48 sam> 
 **
 **    Author: Henrik Sandberg
 **    Created: Mon Jun  4 10:36:43 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "LadarRoadPerceptor.hh"

// Default constructor
LadarRoadPerceptor::LadarRoadPerceptor()
{
  memset(this, 0, sizeof(*this));
  
  // Initialize Kalman filters
  d_Kalman = 0;
  var_Kalman = 0;
  left_Kalman = 0;
  right_Kalman = 0;
  var_width_Kalman = 0;
  return;
}


// Default destructor
LadarRoadPerceptor::~LadarRoadPerceptor()
{
}


// Parse the command line
int LadarRoadPerceptor::parseCmdLine(int argc, char **argv)
{ 
  
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // Plot deviations or not
  if (this->options.display_deviation_flag)
    this->plotDev = 1;
  else
    this->plotDev = 0;
  
  // Detect second direction or not
  if (this->options.detect_second_direction_flag)
    this->detectSecondPeak = 1;
  else
    this->detectSecondPeak = 0;

  this->FODlevelSecondPeak = this->options.FOD_second_direction_arg;
  this->HoughTransformThresh = this->options.Hough_threshold_arg;

  // Use Kalman filter estimares as output
  if (this->options.use_Kalman_dir_flag)
    this->Kalman_dir = 1;
  else
    this->Kalman_dir = 0;

  if (this->options.use_Kalman_wid_flag)
    this->Kalman_wid = 1;
  else
    this->Kalman_wid = 0;

  // Set noise variances in Kalman filters
  this->varDirProcLevel = this->options.dir_process_noise_var_arg;
  this->varDirMeasLevel = this->options.dir_meas_noise_var_arg;
  this->varWidProcLevel = this->options.wid_process_noise_var_arg;
  this->varWidMeasLevel = this->options.wid_meas_noise_var_arg;
  
  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
    this->mode = modeReplay;
  else
    this->mode = modeLive;
  
  return 0;
}



// Initialize sensnet
int LadarRoadPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;

  if (this->mode == modeLive)
  {
    // Create sensnet interface
    this->sensnet = sensnet_alloc();
    assert(this->sensnet);
    if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
      return -1;
  }
  else
  {
    // Create replay interface
    this->replay = sensnet_replay_alloc();
    assert(this->replay);    
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs) != 0)
      return ERROR("unable to open log");
  }

  // Default ladar set
  numSensorIds = 0;
  //sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RIEGL;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
    ladar = this->ladars + this->numLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (this->sensnet)
    {
      if (sensnet_join(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
        return ERROR("unable to join %d", ladar->sensorId);
    }
  }

  initSendMapElement(skynetKey);
  lastSentSize.resize(this->numLadars);
  for (i=0;i<this->numLadars;++i){
    lastSentSize[i] = 0;
  }
  return 0;
}


// Clean up sensnet
int LadarRoadPerceptor::finiSensnet()
{
  if (this->sensnet)
  {
    int i;
    Ladar *ladar;
    for (i = 0; i < this->numLadars; i++)
    {
      ladar = this->ladars + i;
      sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
    }
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
  else if (this->replay)
  {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Update the map with new range data
int LadarRoadPerceptor::update()
{
  //  fprintf(stderr, "entering update() \n");

  this->numData = 0;
  LadarPointDataClass ladarPointData;

  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;

  // Why is this necessary? AH
  usleep(100000);
  
  if (this->sensnet)
  {
    // In live mode wait for new data, but timeout if
    // we dont get anything new for a while.
    if (sensnet_wait(this->sensnet, 200) != 0)
      return 0;
  }
  else if (this->replay)
  {
    // In replay mode, advance the log to the next record.
    if (sensnet_replay_next(this->replay, 0) != 0)
      return 0;
  }
  
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;

    if (this->sensnet)
    {
      // Check the latest blob id
      if (sensnet_peek(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
        continue;

      // Is this a new blob?
      if (blobId == ladar->blobId)
        continue;

      // If this is a new blob, read it    
      if (sensnet_read(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
        break;
    }
    else if (this->replay)
    {
      // Advance log one step
      if (sensnet_replay_next(this->replay, 0) != 0)
        return -1;
 
      // Read new blob
      if (sensnet_replay_read(this->replay, ladar->sensorId,
                              SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
        break;
    }

  
    //transformed to sensor frame
    float sfx, sfy, sfz; //sensor frame vars
    //transformed to vehicle frame
    float vfx, vfy, vfz; //vehicle frame vars
    //transformed to local frame
    float lfx, lfy, lfz; //local frame vars
    
    int edgepts = 0;
    for(int j=edgepts; j < NUMSCANPOINTS-edgepts; j++) 
    {
      rawData[j][ANGLE] = blob.points[j][ANGLE];
      rawData[j][RANGE] = blob.points[j][RANGE];
      LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE],&sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
      xyzData[j][0] = lfx;
      xyzData[j][1] = lfy;
      xyzData[j][2] = lfz;
   

      //if this isn't a no-return point, send to display 
      
      if(rawData[j][1] < MAXDISTROAD)
	{
	  this->xvData[this->numData] = vfx;
	  this->yvData[this->numData] = vfy;
	  this->numData++;
	}
    }

    if (this->console)
    {
      char token[64];
      snprintf(token, sizeof(token), "%%ladar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(ladar->sensorId),
                  blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));
    }
  } //end cycling through ladar

  fprintf(stderr, "numData: %d \n", this->numData);
  ladarPointData.setNewLadarDataPoints(this->yvData,this->xvData,this->numData);
  
  // Do the Hough transform
  FLOAT rMin = -sqrt(101*101*2)-1;
  FLOAT rMax = -rMin;
  FLOAT rStep = 0.5;
  FLOAT thetaMin = -CV_PI/2;
  FLOAT thetaMax = CV_PI/2;
  FLOAT thetaStep = CV_PI/180;
  ladarPointData.computeHoughTransform(rMin,rMax,rStep,thetaMin,thetaMax,thetaStep);
  //FLOAT threshold = 20.0; // Seems to work OK.
  //threshold = 25.0;
  // threshold = 40.0; // Seems a bit too large
  FLOAT threshold = this->HoughTransformThresh; 

  ladarPointData.computeAngleDataSpectrum(threshold);
  
  // Extract the largest peak.
  FLOAT deviation, mass, fractionOfData;
  INT peak_separation = 3; // Seems to work OK. 5 also works.
  FLOAT direction = ladarPointData.getDirectionData(deviation, mass,fractionOfData,peak_separation);
  
  // Possibly extract a second peak.
  //FLOAT thresh_FOD = 0.8;
  FLOAT thresh_FOD = this->FODlevelSecondPeak;
  INT second_peak = 0;
  FLOAT direction2, deviation2, mass2,fractionOfData2;
  if ((fractionOfData < thresh_FOD) && detectSecondPeak)
    {
      ladarPointData.removeCurrentAngleSpectrumPeak();
      direction2 = ladarPointData.getDirectionData(deviation2, mass2,fractionOfData2,peak_separation);
      second_peak = 1;
    }
  
  FLOAT meas = direction;
  if (second_peak)
    if (ABS(direction2-d_Kalman) < ABS(meas-d_Kalman))
      meas = direction2;

  // A Kalman filter for the direction estimate
  //  FLOAT varProc = 0.001*SQUARE(blob.state.vehXVel);
  FLOAT varProc = varDirProcLevel*SQUARE(blob.state.vehXVel);
  //  FLOAT varMeas = 0.2; // Average uncertainty in angle measurement.
  FLOAT varMeas = varDirMeasLevel; // Average uncertainty in angle measurement.
  var_Kalman = var_Kalman + varProc;
  FLOAT K_Kalman = var_Kalman/(var_Kalman + varMeas);
  d_Kalman = d_Kalman + K_Kalman*(meas - d_Kalman); 
  var_Kalman = (1-K_Kalman)*var_Kalman;

  if (Kalman_dir)
    direction = d_Kalman;

  // fprintf(stderr, "Direction: %3.2f, Deviation: %3.2f, Mass: %5.2f, FOD: %2.2f \n", direction*180/CV_PI,deviation*180/CV_PI,mass,fractionOfData);  
  fprintf(stderr, "Direction: %3.2f, var_Kalman: %3.2f, Mass: %5.2f, FOD: %2.2f \n", direction*180/CV_PI,var_Kalman,mass,fractionOfData);  

  // Plot main direction in mapviewer.
  MapElement el;
  point2arr ptarr;
  point2 statept(blob.state.localX, blob.state.localY);
  point2 dpt(20,0);

  ptarr.push_back(statept);
  if (mass > 0)
    ptarr.push_back(statept + dpt.rot(direction+blob.state.localYaw));
  
  el.setTypePoly();
  el.setId((int)MODladarRoadPerceptor,0);
  el.setColor(MAP_COLOR_RED);
  el.setGeometry(ptarr);
  
  sendMapElement(&el,-2);
  
  // Plot deviation in mapviewer.
  MapElement el_dev;
  point2arr ptarr_dev;
  point2 dpt_dev(15,0);
 
  ptarr_dev.push_back(statept);
  if ((mass > 0) && (this->plotDev))
    {
      ptarr_dev.push_back(statept + dpt_dev.rot(direction-deviation+blob.state.localYaw));
      ptarr_dev.push_back(statept + dpt_dev.rot(direction+deviation+blob.state.localYaw));
    }
  el_dev.setTypePoly();
  el_dev.setId((int)MODladarRoadPerceptor,1);
  el_dev.setColor(MAP_COLOR_GREEN);
  el_dev.setGeometry(ptarr_dev);
  
  sendMapElement(&el_dev,-2);
  
  // Plot second direction, if present
  MapElement el2;
  point2arr ptarr2;
  FLOAT second_peak_big_enough = 0.6;

  ptarr2.push_back(statept);
  if ((second_peak) && (mass2 > 0) && (fractionOfData2 > second_peak_big_enough))
    {
      point2 dpt2(15,0);
      ptarr2.push_back(statept + dpt2.rot(direction2+blob.state.localYaw));
    }
  el2.setTypePoly();
  el2.setId((int)MODladarRoadPerceptor,2);
  el2.setColor(MAP_COLOR_RED);
  el2.setGeometry(ptarr2);
      
  sendMapElement(&el2,-2);
  
  // Plot deviation  of second peak in mapviewer.
  MapElement el2_dev;
  point2arr ptarr2_dev;
  
  ptarr2_dev.push_back(statept);
  if ((mass2 > 0)  && (mass2 > 0) && (fractionOfData2 > second_peak_big_enough)  && (this->plotDev))
    {
      point2 dpt2_dev(10,0);
      ptarr2_dev.push_back(statept + dpt2_dev.rot(direction2-deviation2+blob.state.localYaw));
      ptarr2_dev.push_back(statept + dpt2_dev.rot(direction2+deviation2+blob.state.localYaw));
    }
  el2_dev.setTypePoly();
  el2_dev.setId((int)MODladarRoadPerceptor,3);
  el2_dev.setColor(MAP_COLOR_GREEN);
  el2_dev.setGeometry(ptarr2_dev);
  
  sendMapElement(&el2_dev,-2);  

  // Plot safety box in estimated direction.
  FLOAT length_box = DIST_REAR_AXLE_TO_FRONT; 
  length_box = length_box + 2*blob.state.vehXVel;

  FLOAT width_left, width_right;
  vector<FLOAT> yRightCoord, yLeftCoord;
  
  // Rotate the data with direction radians.
  int k;
  for (k = 0; k < numData ; k++)
    {
      rotxvData[k] = xvData[k]*cos(direction) + yvData[k]*sin(direction);
      rotyvData[k] = -xvData[k]*sin(direction) + yvData[k]*cos(direction);
    }
  
  // Pick out data in the desired corridor
  for (k = 0; k < numData ; k++)
    {
      if ((rotyvData[k]>0) && (rotxvData[k]>=0) && (rotxvData[k]<=length_box))
	{
	  yRightCoord.push_back(rotyvData[k]); 
	}
      if ((rotyvData[k]<0) && (rotxvData[k]>=0) && (rotxvData[k]<=length_box))
	{
	  yLeftCoord.push_back(-rotyvData[k]);
	}
    }
  
  sort(yLeftCoord.begin(),yLeftCoord.end());
  sort(yRightCoord.begin(),yRightCoord.end());
  if (yRightCoord.size()>1)
    width_right = yRightCoord[1];
  else 
    width_right = MAXDISTROAD;
  if (yLeftCoord.size()>1)
    width_left = yLeftCoord[1];
  else
    width_left = MAXDISTROAD;

  // Kalman filters for the width of the corridor.
  // FLOAT varProcWidth = 0.01*SQUARE(blob.state.vehXVel);
  FLOAT varProcWidth = varWidProcLevel*SQUARE(blob.state.vehXVel);
  // FLOAT varMeasWidth = 10; // Average uncertainty in width measurements.
  FLOAT varMeasWidth = varWidMeasLevel; // Average uncertainty in width measurements.
  var_width_Kalman = var_width_Kalman + varProcWidth;
  FLOAT K_width_Kalman = var_width_Kalman/(var_width_Kalman + varMeasWidth);
  
  left_Kalman = left_Kalman + K_width_Kalman*(width_left - left_Kalman); 
  right_Kalman = right_Kalman + K_width_Kalman*(width_right - right_Kalman); 

  var_width_Kalman = (1-K_width_Kalman)*var_width_Kalman;

  if (Kalman_wid)
    {
      width_right = right_Kalman; 
      width_left = left_Kalman;
    }

  fprintf(stderr, "Width left: %3.2f, Width right: %3.2f \n",width_left,width_right);
  
  point2 width_right_local(width_right,0), width_left_local(width_left,0), length_box_local(length_box,0);
 
  MapElement box_left, box_right;
  point2arr box_geo_left, box_geo_right;
  
  box_geo_left.push_back(statept + width_left_local.rot(blob.state.localYaw - CV_PI/2 +direction));
  box_geo_left.push_back(statept + width_left_local.rot(blob.state.localYaw -  CV_PI/2 + direction) + length_box_local.rot(blob.state.localYaw+direction));
  box_geo_right.push_back(statept + width_right_local.rot(blob.state.localYaw + CV_PI/2 + direction));
  box_geo_right.push_back(statept + width_right_local.rot(blob.state.localYaw +  CV_PI/2 + direction) + length_box_local.rot(blob.state.localYaw+direction));
  
  box_left.setTypePoly();
  box_left.setId((int)MODladarRoadPerceptor,4);
  box_left.setColor(MAP_COLOR_GREEN);
  box_left.setGeometry(box_geo_left); 
  sendMapElement(&box_left,-2); 

  box_right.setTypePoly();
  box_right.setId((int)MODladarRoadPerceptor,5);
  box_right.setColor(MAP_COLOR_GREEN);
  box_right.setGeometry(box_geo_right); 
  sendMapElement(&box_right,-2); 

  return 0;
} //end update()


// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"LadarRoadPerceptor $Revision$                                          \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"Ladar[4]: %ladar4%                                                         \n"
"Ladar[5]: %ladar5%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int LadarRoadPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int LadarRoadPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int LadarRoadPerceptor::onUserQuit(cotk_t *console, LadarRoadPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int LadarRoadPerceptor::onUserPause(cotk_t *console, LadarRoadPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  LadarRoadPerceptor *percept;
  
  percept = new LadarRoadPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  percept->send_debug = percept->options.send_debug_flag;
  if (percept->send_debug){
    cout << "Sending data to debugging channel -2" << endl;
  }else{
     cout << "Debugging map element messages off" << endl;
  }




  fprintf(stderr, "entering main thread of LadarRoadPerceptor \n");

  percept->loopCount =0;
  
  while (!percept->quit)
  {
    int tempCount = percept->loopCount;
    percept->loopCount = tempCount+1;

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Get new data and update our perceptor
    if (percept->update() != 0)
      break;
  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}

