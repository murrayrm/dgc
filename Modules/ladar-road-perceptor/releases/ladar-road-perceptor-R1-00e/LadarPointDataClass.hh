/* This class computes the Hough transform on x-y data from the ladars. 
 * Owner: Henrik Sandberg 
 */


#ifndef LADARPOINTDATACLASS_HH
#define LADARPOINTDATACLASS_HH

#include <cv.h>
#include <highgui.h>
#include <math.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;



//#include "gnuplot_i.h"

/*! opencv constants */
#define FLOAT_MAT_TYPE CV_32FC1
#define FLOAT_MAT_ELEM_TYPE float
#define INT_MAT_TYPE CV_8UC1
#define INT_MAT_ELEM_TYPE unsigned char
#define FLOAT_POINT2D CvPoint2D32f
#define FLOAT float
#define INT int
#define SHORT_INT unsigned char

/*! Constants for smoothing of direction estimate data. Number of points in the Gaussian window. */
#define WINDOWSIZE 41
/*! The middle point of the window */
#define MIDDLEWIN 20
/*! Pixel value in a default opencv matrix */
#define DEFAULT_FLOAT_MAT_ELEM 1000
/*! Maximum angle deviation (in degs) allowed in the peak estimate in the anglespectrum. See getDirectionData2 */
#define MAXANGLEDEV 30 

/*! Basic math operations */
#define SQUARE(x) ((x) * (x))
#define ABS(x) (((x) < 0) ? -(x) : (x))  

/*! LadarPointDataClass class. This class implements the Hough transform, and estimates the instantaneous road direction. It also contains some methods for
 * plotting for debugging.
 */
class LadarPointDataClass {
public:
  /*! Constructor */
  LadarPointDataClass(); 
    
  /*! Destructor  */
  ~LadarPointDataClass();
    
  /*! Set new xData and yData 
   * \param xData The x coordinates of ladar data in vehicle frame.
   * \param yData The y coordinates of ladar data in vehicle frame.
   * \param nbrData The size of the arrays (the number of ladar data points.)
   */
  void setNewLadarDataPoints(FLOAT xData[], FLOAT yData[], INT nbrData);

  /*! Binarize the data. For plotting with opencv and for computation of Hough transform. */
  void makeBitMap(FLOAT xShiftData, FLOAT yShiftData, FLOAT scaleData, INT width, INT height);

  /*! Overloaded method. Can be used if the parameters have already been set. */
  void makeBitMap();
  
  /*! Get the binarized image (a bit map) for plotting using opencv */
  CvMat* getBitMap();

  /*! Compute the Hough Transform of the ladar data points. 
   * \param rMin The smallest distance to a line 
   * \param rMax The largest distance to a line in the Hough transform.
   * \param rStep The distance step bewteen each line in the Hough transform
   * \param thetaMin The smallest angle of a line 
   * \param thetaMax The largest angle of a line 
   * \param thetaStep The angle step bewteen each line in the Hough transform
   */ 
  void computeHoughTransform(FLOAT rMin, FLOAT rMax, FLOAT rStep, FLOAT thetaMin, FLOAT thetaMax, FLOAT thetaStep);
  
  /*! Get the Hough Transform 
   * \param scale Multiply the value in each bin with scale. To increase contrast in picture.
   * \param rBins Number of bins in distance direction of the Hough transform
   * \param thetaBins Number of bins in the angle direction of the Hough transform
   */
  CvMat* getHoughTransform(INT scale, INT& rBins, INT& thetaBins);

  /*! Compute angle data spectrum. Sums the Hough transform in the r-direction, disregarding peaks smaller than threshold.
   * \param threshold Sets the threshold for discarding peaks in the Hough transform.
   */
  void computeAngleDataSpectrum(FLOAT threshold);

  /*! Compute Gaussian smoothing window.
   * \param sigma The standard deviation (in degs) of the Gaussian window
   */
  void computeSmoothingWindow(FLOAT sigma);

  /*! Apply the smoothing window to the previously computed angle data spectrum. */
  void smoothAngleDataSpectrum();

  /*! Get angleSpectrum 
   * \param angles Array containing the angle spectrum
   * \param nbrAngles Number of elements in array. Angles lie between thetaMin and thetaMax
   */
  FLOAT * getAngleSpectrum(FLOAT* angles, INT& nbrAngles);

  /*! Returns instantaneous direction information (in rads) from angle spectrum. Picks out the maximum and computes a standard deviation-type estimate of the width of the peak.
   * This turned out to be very conservative. Use getDirectionData2 method below instead.
   * \param deviation Width of peak (in rads)
   * \param mass The number of data points from the Hough transform that is captured in this peak
   * \param fractionOfData The fraction of the mass above and the total mass in the entire Hough transform. In [0,1]
   * \param peak_separation The number of zeros in the angles spectrum in between peaks that distinguishes two different peaks.
   */
  FLOAT getDirectionData(FLOAT& deviation, FLOAT& mass, FLOAT& fractionOfData, INT peak_separation);

  /*! Returns instantaneous direction information (in rads) from angle spectrum. Newer method for deviation computation.
   * Uses the smoothed spectrum, and peak width is defined as the distance to the closest point where the spectrum starts to increase again.
   * \param mass The number of data points from the Hough transform that is captured in this peak
   * \param fractionOfData The fraction of the mass above and the total mass in the entire Hough transform. In [0,1]
   * \param leftDev Peak width to the left (in rads)
   * \param rightDev Peak width to the right (in rads)
   */
  FLOAT getDirectionData2(FLOAT& mass, FLOAT& fractionOfData, FLOAT& leftDev, FLOAT& rightDev);

  /*! Remove the current peak in the angleSpectrum (should be done after getDirectionData has found a peak, and the 
   * next largest peak should be found. ) */
  void removeCurrentAngleSpectrumPeak();
  
  /*! Remove the current peak in the smoothSpectrum (should be done after getDirectionData2 has found a peak, and the 
   * next largest peak should be found.) */
  void removeCurrentSmoothSpectrumPeak();

  /*! Whether to write text file output of anglesspectrum, smoothed anglespectrum, and used window .*/
  int debug_output_file;

private:
  /*! mod for negative number modified. */
  INT mod(INT a, INT b);
  /*! distance on a circle of circumference base. */
  FLOAT dist_on_circle(FLOAT a, FLOAT b, FLOAT base);  

  FLOAT* xData; // in data from ladar
  FLOAT* yData; // in data from ladar
  INT* row; // coordinates of ladar data in binary image
  INT* col;  // coordinates of ladar data in binary image
  INT nbrData; // number of ladar points
  FLOAT xShiftData, yShiftData; // shift in ref
  FLOAT scaleData; // scale data for binarization
  INT height, width; // size of opencv image
  CvMat* houghSpace; // Contains the Hough transform
  INT rBins, thetaBins; // Number of bins in Hough transform
  FLOAT* rs; // array of r's
  FLOAT* thetas; // array of theta's 
  FLOAT thetaStep; // step size in theta array
  uchar init;  // to check if pointers are initialized.
  FLOAT* angleSpectrum; // pointer to anglespectrum
  FLOAT* smoothSpectrum;  // pointer to smoothed anglespectrum
  INT start_peak; // index of start of current max peak in either anglespectrum or its smoothed version
  INT end_peak; // index of end of current max peak in either anglespectrum or its smoothed version
  INT peak_index; // index of current max peak in either anglespectrum or its smoothed version
  FLOAT mass_angleSpectrum; // Total mass in current anglespectrum
  FLOAT  mass_smoothSpectrum; // Total mass in smoothed anglespectrum
  FLOAT window[WINDOWSIZE]; // The smoothing window is stored here.
};

#endif
