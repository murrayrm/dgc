/**********************************************************
 **
 **  LADARROADPERCEPTOR.CC
 **
 **    Time-stamp: <2007-06-06 22:41:48 sam> 
 **
 **    Author: Henrik Sandberg
 **    Created: Mon Jun  4 10:36:43 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "LadarRoadPerceptor.hh"

// Default constructor
LadarRoadPerceptor::LadarRoadPerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
LadarRoadPerceptor::~LadarRoadPerceptor()
{
}


// Parse the command line
int LadarRoadPerceptor::parseCmdLine(int argc, char **argv)
{ 
  
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
    this->mode = modeReplay;
  else
    this->mode = modeLive;
  
  return 0;
}



// Initialize sensnet
int LadarRoadPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;

  if (this->mode == modeLive)
  {
    // Create sensnet interface
    this->sensnet = sensnet_alloc();
    assert(this->sensnet);
    if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
      return -1;
  }
  else
  {
    // Create replay interface
    this->replay = sensnet_replay_alloc();
    assert(this->replay);    
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs) != 0)
      return ERROR("unable to open log");
  }

  // Default ladar set
  numSensorIds = 0;
  //sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RIEGL;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
    ladar = this->ladars + this->numLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (this->sensnet)
    {
      if (sensnet_join(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
        return ERROR("unable to join %d", ladar->sensorId);
    }
  }

  initSendMapElement(skynetKey);
  lastSentSize.resize(this->numLadars);
  for (i=0;i<this->numLadars;++i){
    lastSentSize[i] = 0;
  }
  return 0;
}


// Clean up sensnet
int LadarRoadPerceptor::finiSensnet()
{
  if (this->sensnet)
  {
    int i;
    Ladar *ladar;
    for (i = 0; i < this->numLadars; i++)
    {
      ladar = this->ladars + i;
      sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
    }
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
  else if (this->replay)
  {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Update the map with new range data
int LadarRoadPerceptor::update()
{
  //  fprintf(stderr, "entering update() \n");

  this->numData = 0;
  LadarPointDataClass ladarPointData;

  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;

  // Why is this necessary? AH
  usleep(100000);
  
  if (this->sensnet)
  {
    // In live mode wait for new data, but timeout if
    // we dont get anything new for a while.
    if (sensnet_wait(this->sensnet, 200) != 0)
      return 0;
  }
  else if (this->replay)
  {
    // In replay mode, advance the log to the next record.
    if (sensnet_replay_next(this->replay, 0) != 0)
      return 0;
  }
  
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;

    if (this->sensnet)
    {
      // Check the latest blob id
      if (sensnet_peek(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
        continue;

      // Is this a new blob?
      if (blobId == ladar->blobId)
        continue;

      // If this is a new blob, read it    
      if (sensnet_read(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
        break;
    }
    else if (this->replay)
    {
      // Advance log one step
      if (sensnet_replay_next(this->replay, 0) != 0)
        return -1;
 
      // Read new blob
      if (sensnet_replay_read(this->replay, ladar->sensorId,
                              SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
        break;
    }

    //  fprintf(stderr, "got data %s %d %d %lld\n",
    //  sensnet_id_to_name(ladar->sensorId), ladar->blobId,
    //  blob.scanId, blob.timestamp);
    

    //transformed to sensor frame
    float sfx, sfy, sfz; //sensor frame vars
    //LadarRangeBlobScanToSensor(&blob, blob.points[45][0], blob.points[45][1],&sfx, &sfy, &sfz);
    //   fprintf(stderr, "middle range transformed to sensor frame (x,y,z): %f, %f, %f \n", sfx, sfy, sfz);

    //transformed to vehicle frame
    float vfx, vfy, vfz; //vehicle frame vars
    //LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    //    fprintf(stderr, "middle range transformed to vehicle frame (x,y,z): %f, %f, %f \n", vfx, vfy, vfz);

    //transformed to local frame
    float lfx, lfy, lfz; //local frame vars
    //LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
    //    fprintf(stderr, "middle range transformed to local frame (x,y,z): %f, %f, %f \n", lfx, lfy, lfz);

    //coyping the data to a arrays accessible by any 
    //function in the program. Seems inefficient....
    //double xpts[NUMSCANPOINTS];
    //double ypts[NUMSCANPOINTS];
    //double zpts[NUMSCANPOINTS];

    //int numReturns = 0;
    //    point2arr ptarr;
    //point2arr ptarrextra;
    //ptarr.clear();
    //ptarrextra.clear();
    //point2 pt;

    //int edgepts = 5;
    int edgepts = 0;
    for(int j=edgepts; j < NUMSCANPOINTS-edgepts; j++) 
    {
      rawData[j][ANGLE] = blob.points[j][ANGLE];
      rawData[j][RANGE] = blob.points[j][RANGE];
      LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE],&sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
      xyzData[j][0] = lfx;
      xyzData[j][1] = lfy;
      xyzData[j][2] = lfz;
   

      //if this isn't a no-return point, send to display 
      
      if(rawData[j][1] < 80)
      {
	
        //SAM
        //pt.set(lfx,lfy);
	this->xvData[this->numData] = vfx;
	this->yvData[this->numData] = vfy;
	this->numData++;
	
        //ptarr.push_back(pt);

        //SAM

	
	  
	  //xpts[numReturns]=lfx;
	  //ypts[numReturns]=lfy;
	  //zpts[numReturns]=lfz;
	  //numReturns++;
      }

    }

    //for plotting the segmentation
    //LadarRangeBlobScanToSensor(&blob, 0.0, 0.0, &sfx, &sfy, &sfz);
    //LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    //LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
   

    // Below commented out by Henrik
    //SAM
   //  MapElement el;
//     point2arr tmpptarr;
//     vector<point2arr> ptarrarr;
//     ptarrarr=  ptarr.split(4);



//     int sendsize = ptarrarr.size();
//     for (int k=0;k<sendsize;++k){
//       tmpptarr = ptarrarr[k];
//       el.clear();
//       el.setId(100,i,k);
//       el.setTypeObstacle();
//       el.setState(blob.state);
//       if (tmpptarr.size()<2)
//         continue;
//       el.height=tmpptarr.size();      
//       tmpptarr=tmpptarr.get_bound_box();      
      
//       el.setGeometry(tmpptarr);
//       el.plotColor = MAP_COLOR_YELLOW;
//       el.setFrameTypeLocal();
      
//       sendMapElement(&el,0);      
//       if (send_debug){
//         sendMapElement(&el,-2);      
//       }
//     }
//     if (sendsize<lastSentSize[i]){
//       for (int k=sendsize;k<lastSentSize[i];++k){
//         el.clear();
//         el.setId(100,i,k);
//         el.setTypeClear();
//         sendMapElement(&el,0);      
//         if(send_debug){
//           sendMapElement(&el,-2);
//         }      
        
//       }
//     }
//     lastSentSize[i] = ptarrarr.size();

// Above commented out by Henrik

//     for (unsigned k = ptarrarr.size();k<elcount[i];k++){
//       el.clear();
//       el.type=ELEMENT_CLEAR;
//       el.set_id(-i-10,(int)k);
//       sendMapElement(&el,0);      
//       sendMapElement(&el,-2);      
      
//     }
    
   
//     elcount[i]=ptarrarr.size();
    //TODO: check if this runs fast enough to not be limiting factor
    //    fprintf(stderr, "calling processScan() \n");

    if (this->console)
    {
      char token[64];
      snprintf(token, sizeof(token), "%%ladar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(ladar->sensorId),
                  blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));
    }
  } //end cycling through ladar

  fprintf(stderr, "numData: %d \n", this->numData);
  ladarPointData.setNewLadarDataPoints(this->yvData,this->xvData,this->numData);
  
  // Do the Hough transform
  FLOAT rMin = -sqrt(101*101*2)-1;
  FLOAT rMax = -rMin;
  FLOAT rStep = 0.5;
  FLOAT thetaMin = -CV_PI/2;
  FLOAT thetaMax = CV_PI/2;
  FLOAT thetaStep = CV_PI/180;
  ladarPointData.computeHoughTransform(rMin,rMax,rStep,thetaMin,thetaMax,thetaStep);
  FLOAT threshold = 20.0; // Seems to work OK.
  threshold = 25.0;
  // threshold = 40.0; // Seems a bit too large
  ladarPointData.computeAngleDataSpectrum(threshold);
  
  // Extract the largest peak.
  FLOAT deviation, mass, fractionOfData;
  INT peak_separation = 3; // Seems to work OK. 5 also works.
  FLOAT direction = ladarPointData.getDirectionData(deviation, mass,fractionOfData,peak_separation);
  fprintf(stderr, "Direction: %3.2f, Deviation: %3.2f, Mass: %5.2f, FOD: %2.2f \n", direction*180/CV_PI,deviation*180/CV_PI,mass,fractionOfData);
  
  // Possibly extract a second peak.
  FLOAT thresh_FOD = 0.8;
  INT second_peak = 0;
  FLOAT direction2, deviation2, mass2,fractionOfData2;
  if (fractionOfData < thresh_FOD)
    {
      ladarPointData.removeCurrentAngleSpectrumPeak();
      direction2 = ladarPointData.getDirectionData(deviation2, mass2,fractionOfData2,peak_separation);
      second_peak = 1;
    }
  
  // Plot main direction in mapviewer.
  MapElement el;
  point2arr ptarr;
  point2 statept(blob.state.localX, blob.state.localY);
  point2 dpt(20,0);
  
  ptarr.push_back(statept);
  if (mass > 0)
    ptarr.push_back(statept + dpt.rot(direction+blob.state.localYaw));
  
  el.setTypePoly();
  el.setId((int)MODladarRoadPerceptor,0);
  el.setColor(MAP_COLOR_RED);
  el.setGeometry(ptarr);
  
  sendMapElement(&el,-2);
  
  // Plot deviation in mapviewer.
  MapElement el_dev;
  point2arr ptarr_dev;
  point2 dpt_dev(15,0);
 
  ptarr_dev.push_back(statept);
  if ((mass > 0) && (this->plotDev))
    {
      ptarr_dev.push_back(statept + dpt_dev.rot(direction-deviation+blob.state.localYaw));
      ptarr_dev.push_back(statept + dpt_dev.rot(direction+deviation+blob.state.localYaw));
    }
  el_dev.setTypePoly();
  el_dev.setId((int)MODladarRoadPerceptor,1);
  el_dev.setColor(MAP_COLOR_GREEN);
  el_dev.setGeometry(ptarr_dev);
  
  sendMapElement(&el_dev,-2);
  
  // Plot second direction, if present
  MapElement el2;
  point2arr ptarr2;
  FLOAT second_peak_big_enough = 0.6;

  ptarr2.push_back(statept);
  if ((second_peak) && (mass2 > 0) && (fractionOfData2 > second_peak_big_enough))
    {
      point2 dpt2(15,0);
      ptarr2.push_back(statept + dpt2.rot(direction2+blob.state.localYaw));
    }
  el2.setTypePoly();
  el2.setId((int)MODladarRoadPerceptor,2);
  el2.setColor(MAP_COLOR_RED);
  el2.setGeometry(ptarr2);
      
  sendMapElement(&el2,-2);
  
  // Plot deviation  of second peak in mapviewer.
  MapElement el2_dev;
  point2arr ptarr2_dev;
  
  ptarr2_dev.push_back(statept);
  if ((mass2 > 0)  && (mass2 > 0) && (fractionOfData2 > second_peak_big_enough)  && (this->plotDev))
    {
      point2 dpt2_dev(10,0);
      ptarr2_dev.push_back(statept + dpt2_dev.rot(direction2-deviation2+blob.state.localYaw));
      ptarr2_dev.push_back(statept + dpt2_dev.rot(direction2+deviation2+blob.state.localYaw));
    }
  el2_dev.setTypePoly();
  el2_dev.setId((int)MODladarRoadPerceptor,3);
  el2_dev.setColor(MAP_COLOR_GREEN);
  el2_dev.setGeometry(ptarr2_dev);
  
  sendMapElement(&el2_dev,-2);  
  
  return 0;
} //end update()


// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"LadarRoadPerceptor $Revision$                                          \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"Ladar[4]: %ladar4%                                                         \n"
"Ladar[5]: %ladar5%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int LadarRoadPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int LadarRoadPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int LadarRoadPerceptor::onUserQuit(cotk_t *console, LadarRoadPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int LadarRoadPerceptor::onUserPause(cotk_t *console, LadarRoadPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  LadarRoadPerceptor *percept;
  
  percept = new LadarRoadPerceptor();
  assert(percept);

  // plot deviations or not
  percept->plotDev = 0;
  
  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  percept->send_debug = percept->options.send_debug_flag;
  if (percept->send_debug){
    cout << "Sending data to debugging channel -2" << endl;
  }else{
     cout << "Debugging map element messages off" << endl;
  }




  fprintf(stderr, "entering main thread of LadarRoadPerceptor \n");

  percept->loopCount =0;
  
  while (!percept->quit)
  {
    int tempCount = percept->loopCount;
    percept->loopCount = tempCount+1;

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Get new data and update our perceptor
    if (percept->update() != 0)
      break;
  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}

