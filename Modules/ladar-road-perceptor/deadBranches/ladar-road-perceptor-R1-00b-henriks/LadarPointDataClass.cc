/* This class computes the Hough transform on x-y data from the ladars. */

/*TO DO: Use vectors instead of pointers. */
/*TO DO: don't discretize x-y before Hough transform. */
/*TO DO: interchange x and y in method calls. Partly fixed June 4. Test carefully!*/

#include "LadarPointDataClass.hh"


  // Constructor
LadarPointDataClass::LadarPointDataClass() 
  {
    init = 0;
    nbrData = 0;
    xShiftData = 0;
    yShiftData = 0;
    scaleData = 1;
    rBins = 1;
    thetaBins = 1;
    thetaStep = 0;
    start_peak = 0;
    end_peak = 0;
    peak_index = 0;
    // Initialize pointers
    xData = new FLOAT[nbrData];
    yData = new FLOAT[nbrData];
    row = new INT[nbrData];
    col = new INT[nbrData];
    houghSpace = cvCreateMat(rBins,thetaBins, FLOAT_MAT_TYPE);
    rs = new FLOAT[rBins];
    thetas = new FLOAT[thetaBins];
    angleSpectrum = new FLOAT[thetaBins];
    init = 1;
  }
  
  // Destructor 
LadarPointDataClass::~LadarPointDataClass()
  {
    cvReleaseMat(&houghSpace);
    delete [] xData;
    delete [] yData;
    delete [] col;
    delete [] row;
    delete [] rs;
    delete [] thetas;
    delete [] angleSpectrum;
  } 
  
  // Set new xData and yData
void LadarPointDataClass::setNewLadarDataPoints(FLOAT xData[], FLOAT yData[], INT nbrData)
  {
    // fprintf(stderr, "Entering setNewLadarDataPoints... \n");
    this->nbrData = nbrData;
    if (init)
      {
	//fprintf(stderr, "init in set true... \n");
	delete [] this->xData;
	delete [] this->yData;
      }
    this->xData = new FLOAT[nbrData];
    this->yData = new FLOAT[nbrData];
    for (int k = 0; k < nbrData ; k++) 
      {
	this->xData[k] = xData[k];
	this->yData[k] = yData[k];
      }
  }
  

  // Binarize the data. For plotting with opencv and for Hough transform.
void LadarPointDataClass::makeBitMap(FLOAT xShiftData, FLOAT yShiftData, FLOAT scaleData, INT width, INT height)
  {
    this->xShiftData = xShiftData;
    this->yShiftData = yShiftData;
    this->scaleData = scaleData;
    this->width = width;
    this->height = height;
    if (init)
      {
	delete [] col;
	delete [] row;
      }
    col = new INT[nbrData];
    row = new INT[nbrData];
        
    for (int i = 0 ; i < nbrData ; i++ ) 
      {
	col[i] = (INT)(xData[i]*scaleData + xShiftData);
	row[i] = (INT)(-1*yData[i]*scaleData + yShiftData);
	cout << "xCoord: " << col[i] << " yCoord: " << row[i] << endl;
      }
    
  }
  
  // Overloaded method
void LadarPointDataClass::makeBitMap()
{
    for (int i = 0 ; i < nbrData ; i++ ) 
      {
	col[i] = (INT)(xData[i]*scaleData + xShiftData);
	row[i] = (INT)(yData[i]*scaleData + yShiftData);
      }
  }
  
  // Get bit map for plotting using opencv
  
CvMat* LadarPointDataClass::getBitMap()
  {
    CvMat* img = cvCreateMat(width,height,FLOAT_MAT_TYPE); 
    assert(img!=0);
    // cvSet(img, cvRealScalar(0.0));
    for (int i = 0; i < nbrData ; i++ )
      CV_MAT_ELEM(*img, FLOAT_MAT_ELEM_TYPE, row[i], col[i]) = DEFAULT_FLOAT_MAT_ELEM;
    return(img);
  }

  // Compute the Hough Transform
void LadarPointDataClass::computeHoughTransform(FLOAT rMin, FLOAT rMax, FLOAT rStep, FLOAT thetaMin, FLOAT thetaMax, FLOAT thetaStep)
  {
    rBins = INT((rMax-rMin)/rStep);
    thetaBins = INT((thetaMax-thetaMin)/thetaStep);
    this->thetaStep = thetaStep;
    //fprintf(stderr, "Entering computeHoughTransform... nbrData = %d \n",nbrData);
    
    if (init)
      {
	cvReleaseMat(&houghSpace);
	//fprintf(stderr, "init in compute is true \n");
      }
    houghSpace = cvCreateMat(rBins, thetaBins, FLOAT_MAT_TYPE);
    assert(houghSpace!=0);
    
    //init to zero
    cvSet(houghSpace, cvRealScalar(0.0));
    
    //init values of r and theta
    if (init) // Causes segmentation error for some reason...
      {
	delete [] this->rs;
	delete [] this->thetas;
      }
    rs = new FLOAT[rBins];
    thetas = new FLOAT[thetaBins];
    INT ri, thetai;
    FLOAT r, theta;
    for (r = rMin, ri = 0 ; ri<rBins; ri++,r+=rStep) 
      rs[ri] = r;
    for (theta = thetaMin, thetai = 0 ; thetai<thetaBins; thetai++, theta+=thetaStep) 
       thetas[thetai] = theta;
    
    FLOAT rval;
    INT rdisc;
    for (int i = 0; i<nbrData; i++)
      for (int k = 0; k<thetaBins; k++)
	{
	  //compute the r value for that point and that theta
	  theta = thetas[k];
	  // rval = col[i]*cos(theta) + row[i]*sin(theta); //x y
	  rval = xData[i]*cos(theta) - yData[i]*sin(theta); //x y
	  //rval = 1.0;
	  rdisc = INT((rval - rMin)/rStep);
	  
	  //accumulate in the Hough space if a valid value
	  if ((rdisc>=0) && (rdisc<rBins))
	    {
	      //fprintf(stderr, "r>=0 and r<rBins \n");
	      CV_MAT_ELEM(*houghSpace, FLOAT_MAT_ELEM_TYPE, rdisc, k) = CV_MAT_ELEM(*houghSpace, FLOAT_MAT_ELEM_TYPE, rdisc, k) + 1.0;
	    }
	  //else  // Does not work in sensnet-replay mode for some reason. Maybe the fprintf is too heavy??
	  //{
	  // cerr << "r value in Hough transform out of bound: r=" << r <<endl;
	  //     fprintf(stderr, "r value in Hough transform out of bound: rdisc= %d \n", rdisc);
	  //  }
	}
    // fprintf(stderr, "rval= %f, rdisc= %d \n" ,rval,rdisc);
  }

// Get the Hough Transform
CvMat* LadarPointDataClass::getHoughTransform(INT scale, INT& rBins, INT& thetaBins)
  {
    rBins = this->rBins;
    thetaBins = this->thetaBins;
    CvMat* out = cvCloneMat(houghSpace);
    if (scale > 1)
      for(int i = 0 ; i < rBins ; i++)
	for(int k = 1 ; k < thetaBins ; k ++)
	  CV_MAT_ELEM(*out, FLOAT_MAT_ELEM_TYPE, i, k) *= scale;
    return(out);
  }

  // Compute angle data spectrum
void LadarPointDataClass::computeAngleDataSpectrum(FLOAT threshold)
  {
    int k,i;
    if (init)
      delete [] angleSpectrum;
    angleSpectrum = new FLOAT[thetaBins];
    
    // Write the angleSpectrum to a file for debugging and external plotting.
    //ofstream outSpectrum;
    //outSpectrum.open("anglespectrum.txt");

    for (k = 0 ; k < thetaBins ; k++)
      {
	angleSpectrum[k] = 0;
	for (i = 0 ; i < rBins ; i++)
	  {
	    if(CV_MAT_ELEM(*houghSpace, FLOAT_MAT_ELEM_TYPE, i, k) >= threshold)
	      angleSpectrum[k] =  angleSpectrum[k] + CV_MAT_ELEM(*houghSpace, FLOAT_MAT_ELEM_TYPE, i, k);
	  }
	//outSpectrum << k << "\t" << angleSpectrum[k] << endl;
      }
    //outSpectrum.close();
  }
  
  // Get angleSpectrum
FLOAT * LadarPointDataClass::getAngleSpectrum(FLOAT* angles, INT& nbrAngles)
  {
    nbrAngles = thetaBins;
    FLOAT * out = new FLOAT[thetaBins];
    angles = new FLOAT[thetaBins];
    // Clone outputs
    for (int k = 0; k<thetaBins; k++)
      {
	angles[k] = thetas[k]; 
	out[k] = angleSpectrum[k];
      }
    return(out);
  }
  
  // Get direction information from angle spectrum
FLOAT LadarPointDataClass::getDirectionData(FLOAT& deviation, FLOAT& mass, FLOAT& fractionOfData, INT peak_separation)
  {
    FLOAT max = 0;
    peak_index = 0;
    int i;
    for (i = 0 ; i < thetaBins ; i++)
	if (angleSpectrum[i] > max)
	  {
	    peak_index = i;
	    max = angleSpectrum[i];
	    //cout << "Changed peak index to " << peak_index << endl;
 	  }
    //cout << "peak index = " << peak_index << endl;
    start_peak = peak_index;
    end_peak = peak_index;
    INT cont = 1, nbr_zeros = 0, tmp;
    // cout << "theta1 = " << thetas[0] << "theta max = " << thetas[peak_index] << endl;


    // Find the start and end indices of the maximum in the angleSpectrum 
    while (cont)
      {
	tmp = mod(end_peak+1,thetaBins);
	if (angleSpectrum[tmp] == 0)
	  nbr_zeros++;
	else
	  nbr_zeros = 0;
	if ((nbr_zeros >= peak_separation) || (tmp == peak_index))
	  cont = 0;
	else
	  end_peak = tmp;
      }
    
    cont = 1;
    nbr_zeros = 0;
    while (cont)
      {
	tmp = mod(start_peak-1,thetaBins);
	if (angleSpectrum[tmp] == 0)
	  nbr_zeros++;
	else
	  nbr_zeros = 0;
	if ((nbr_zeros >= peak_separation) || (tmp == end_peak))
	  cont = 0;
	else
	  start_peak = tmp;
      }
    //cout << "start peak = " << start_peak << " end peak = " << end_peak << endl;
    
    // Compute the "mass" of the maximum 
    if (end_peak < start_peak)
      end_peak += thetaBins;
    INT nbr_in_peak = end_peak - start_peak + 1;
    
    mass = 0;
    for (i = 0 ; i < nbr_in_peak ; i++)
      mass += angleSpectrum[mod(start_peak+i,thetaBins)];
    
    // Compute how much of the data that is covered by the maximum
    FLOAT tot_mass = 0;
    for (i = 0; i < thetaBins ; i++)
      tot_mass += angleSpectrum[i];
    if (tot_mass > 0)
      fractionOfData = mass/tot_mass;
    else 
      fractionOfData = 0;
    
    // Estimate the width around the maximum
    deviation = 0;
    if (mass > 0)
      {
	for (i = start_peak; i <= end_peak ; i++)
	  {
	    deviation += (angleSpectrum[mod(i,thetaBins)]/mass)*SQUARE(dist_on_circle(thetas[mod(i,thetaBins)],thetas[peak_index],CV_PI));
	    //	    cout << "theta = " << thetas[mod(i,thetaBins)] << " peak angle = "<< thetas[peak_index] << " distance = " << dist_on_circle(thetas[mod(i,thetaBins)],thetas[peak_index],CV_PI) << endl;
	  }
	deviation = sqrt(deviation);
	if (deviation<thetaStep)
	  deviation = thetaStep;
      }
    //cout << "Deviation= " << deviation << endl;
    return(thetas[peak_index]);
  }

void LadarPointDataClass::removeCurrentAngleSpectrumPeak()
{
  for (int k = start_peak ; k <= end_peak ; k++ )
    {
      angleSpectrum[mod(k,thetaBins)] = 0;
    }
}

INT LadarPointDataClass::mod(INT a, INT b)
{
  INT c = a%b;
  if (c<0)
    c = c + b;
  return(c);
}

FLOAT LadarPointDataClass::dist_on_circle(FLOAT a, FLOAT b, FLOAT base)
  {
    FLOAT dist = b - a;
    while (dist > (base/2))
      dist -= base;
    while (dist < -(base/2))
      dist += base;
    return (dist);
  }
