#include "LadarPointDataClass.hh"

int main(int argc, char** argv)
{
  LadarPointDataClass ladarPointData;
  
  // Set data
  FLOAT xData[3] = {0, 0, 0};
  FLOAT yData[3] = {-10, 2.0, 4.0};
  ladarPointData.setNewLadarDataPoints(xData,yData,3);

  FLOAT xData2[5] = {0,1 , 2, 3, 4};
  FLOAT yData2[5] = {0, -10, -20, -30, -60};
  ladarPointData.setNewLadarDataPoints(yData2,xData2,5);

  
  ladarPointData.makeBitMap(201,201,2,401,401);
  CvMat* image = ladarPointData.getBitMap();

  // Do the Hough transform
  FLOAT rMin = -sqrt(101*101*2)-1;
  FLOAT rMax = -rMin;
  FLOAT rStep = 1.0;
  FLOAT thetaMin = -CV_PI/2;
  FLOAT thetaMax = CV_PI/2;
  FLOAT thetaStep = CV_PI/180;
  ladarPointData.computeHoughTransform(rMin,rMax,rStep,thetaMin,thetaMax,thetaStep);

  INT rBins, thetaBins;
  CvMat* hough = ladarPointData.getHoughTransform(1000,rBins,thetaBins);
  
  cout << "Hough address: " << hough << " nbr r-bins: " << rBins << " nbr theta-bins: " << thetaBins << endl;
  
  FLOAT threshold = 4.0;
  ladarPointData.computeAngleDataSpectrum(threshold);
  FLOAT deviation, mass, fractionOfData;
  INT peak_separation = 10;
  FLOAT direction = ladarPointData.getDirectionData(deviation, mass,fractionOfData,peak_separation);
  cout << "Direction estimate = " << direction*180/CV_PI << " degrees" << endl;
  cout << "Deviation = " << deviation*180/CV_PI << " degrees" << endl;
  cout << "Mass = " << mass << endl;
  cout << "Fraction of data = " << fractionOfData << endl;
  
  // Show the image

  cvNamedWindow( "Image", 1 );
  cvShowImage( "Image", image);
  cvNamedWindow( "Hough", 2 );
  cvShowImage( "Hough", hough);
  cvWaitKey(0);

   // Test some vector properties
  vector<int> test(3);
  test[0] = 1;
  test[1] = 2;
  test[2] = 3;
  vector<int>::iterator iter;
  iter = test.begin();
  cout << test.size()  << "\t" << test.back() << endl;
  cout << *iter << endl;
  test.clear();
  cout << test.size() << "\t" << test[0] << endl;


  cvReleaseMat(&image);
  cvReleaseMat(&hough);
  cout << "Completed!" << endl;  
  
  //usleep(10000);
}
