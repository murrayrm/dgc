/**********************************************************
 **
 **  LADARROADPERCEPTOR.HH
 **
 **    Time-stamp: <2007-06-04 10:35:15 henriks> 
 **
 **    Author: Henrik Sandberg
 **    Created: Mon Jun  4 10:35:15 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef LADARROADPERCEPTOR_HH
#define LADARROADPERCEPTOR_HH

using namespace std;

#include <alice/AliceConstants.h>
#include <assert.h>
#include <ctype.h>
#include <frames/pose3.h>
#include <frames/coords.hh>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <vector>

//openGL support
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include "dgcutils/DGCutils.hh" 
#include "map/MapElementTalker.hh"
#include "map/MapElement.hh"
#include "interfaces/MapElementMsg.h"

// Console support
#include <ncurses.h>
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

// Ladar data operations
#include "LadarPointDataClass.hh"

#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define MAXTRAVEL 2 //max dist obj can travel btwn frames

#define MAXNUMCARS 50 //max num cars we can track
#define MAXNUMOBJS 100

#define MINNUMPOINTS 3 //min # pts to consider object a 'car'
#define NUMSCANPOINTS 181
#define SCALINGFACTOR 4 //used in determining 
                 //discontinuities for segmentation

//#warning "this should be replaced by some probabilistic calculation involving covariances of KF"
#define CARMARGIN 1.0 //if it's this close to outline of 
   //a car, classify it as part
#define RADIALOFFSET 1.0 //how far back to put additional points

#define MINSPEED 2.0   //min velocity to bother sending out
#define MAXSPEED 20.0 //max velocity 
#define MINDIMENSION 2 //we're assuming this is the minimum car dimension 
                       //used when we can only see 1 side...
#define OBJECTTIMEOUT 100 //how many loops before we delete an object
#define CARTIMEOUT 300 //how many loops before we delete a car

//for accessing the rawData array
#define ANGLE 0
#define RANGE 1

#define MAXDISTROAD 80 //max distance to roads used by the road perceptor.
#define MAXROADLANEWIDTH 20 // max road lane width

//! CLASS DESCRIPTION
/*! LadarRoadPerceptor class. This class takes in ladar data points and estimates the instantaneous road direction by computing the Hough transform on
*   the data points (using the LadarPointDataClass). Limited road tracking is available using a Kalman filter. 
*/
class LadarRoadPerceptor :public CMapElementTalker
{
  
public:
  /*!  LadarRoadPerceptor constructor. */  
  LadarRoadPerceptor();

  /*! Destructor */
  ~LadarRoadPerceptor();
  
  /*! Parse the command line */
  int parseCmdLine(int argc, char **argv);
  
  /*! Initialize sensnet */
  int initSensnet();

  /*! Clean up sensnet */
  int finiSensnet();

  /*! Update the map. This is where the data is processed. */
  int update();

  /*! Initialize console display */
  int initConsole();

  /*! Finalize console display */
  int finiConsole();
  
  /*! Console button callback */
  static int onUserQuit(cotk_t *console, LadarRoadPerceptor *self, const char *token);

  /*! Console button callback */
  static int onUserPause(cotk_t *console, LadarRoadPerceptor *self, const char *token);

  /*! Program options */
  gengetopt_args_info options;

  /*! Spread settings */
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;
 
  /*! Sensnet module */
  sensnet_t *sensnet;

  /*! Sensnet replay module */
  sensnet_replay_t *replay;
 
  /*! Operation mode */
  enum {modeLive, modeReplay} mode;

  /*! Console interface */
  cotk_t *console;

  int useDisplay;
  
  /*! Should we quit? */
  bool quit;
  
  /*! Should we pause? */
  bool pause;

  /*! How many times we've looped in the main program */
  int loopCount;

  /*! Individual ladar data */
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  /*! List of currently subscribed ladars */
  int numLadars;
  Ladar ladars[16];

  vector<int> lastSentSize;

  bool send_debug;
  /************ defines from trackMO ***************/

  /*! angle, range data */
  double rawData[NUMSCANPOINTS][2];
  /*! differences in RANGES between consecutive scans */
  double diffs[NUMSCANPOINTS-1]; 
  /*! x,y,z data (in local frame) */
  double xyzData[NUMSCANPOINTS+1][3];
  /*! x,y,z data in local frame, for points 1m radially
   * behind the actual return */
  double depthData[NUMSCANPOINTS+1][3];


  int subgroup; //FIXME: check w/ sam what this value should be

  // For ladar data points 
  // LadarPointDataClass ladarPointData;
  /*! All the data from many ladars are stored here in vehicle frame */
  float xvData[5*NUMSCANPOINTS];
  float yvData[5*NUMSCANPOINTS];
  float rotxvData[5*NUMSCANPOINTS];
  float rotyvData[5*NUMSCANPOINTS]; 

  /*! The total number of ladar data points used in the Hough transform. */
  int numData; 
  /*! Whether to plot deviations in mapviewer. */
  int plotDev;
  /*! Number of the closest ladar returns that are discarded to estimate the road width. */
  unsigned int ladarReturnOffset; 
  /*! Threshold for peaks in the Hough transform. If peaks less than this, they are disregarded in the direction estimation. */
  float HoughTransformThresh; 
  /*! Length of box used to estimate the road width. In multiples of Alice's length.*/
  float lengthRoadBox;

  /*! Kalman filter estimate of direction. In radians, [-PI/2,PI/2] */
  float d_Kalman;
  /*! Variance of Kalman filter estimate of direction. */
  float var_Kalman;
  /*! Kalman filter estimate of distance to left road edge */
  float left_Kalman;
  /*! Kalman filter estimate of distance to right road edge */
  float right_Kalman;
  /*! Variance of Kalman filter estimate of road width. */
  float var_width_Kalman;
  /*! Variance of process noise for direction estimate */
  float varDirProcLevel;
  /*! Variance of measurement noise for direction estimate */
  float varDirMeasLevel;
  /*! Variance of process noise for road width estimate */
  float varWidProcLevel;
  /*! Variance of measurement noise for road width estimate */
  float varWidMeasLevel;
  /*! Whether to use the Kalman filter estimate for plotting */
  int Kalman_dir, Kalman_wid;
  /*! In tracking mode this number tells the number of non-maximum peaks used to maintain a road track. */
  int nbr_not_peak;
};
#endif


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


