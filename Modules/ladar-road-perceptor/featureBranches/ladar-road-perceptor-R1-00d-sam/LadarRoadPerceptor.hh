/**********************************************************
 **
 **  LADARROADPERCEPTOR.HH
 **
 **    Time-stamp: <2007-06-04 10:35:15 henriks> 
 **
 **    Author: Henrik Sandberg
 **    Created: Mon Jun  4 10:35:15 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef LADARROADPERCEPTOR_H
#define LADARROADPERCEPTOR_H

using namespace std;

#include <alice/AliceConstants.h>
#include <assert.h>
#include <ctype.h>
#include <frames/pose3.h>
#include <frames/coords.hh>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <vector>

//openGL support
#include <GL/glut.h>

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include "dgcutils/DGCutils.hh" 
#include "map/MapElementTalker.hh"
#include "map/MapElement.hh"
#include "interfaces/MapElementMsg.h"

// Console support
#include <ncurses.h>
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

// Ladar data operations
#include "LadarPointDataClass.hh"

#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define MAXTRAVEL 2 //max dist obj can travel btwn frames

#define MAXNUMCARS 50 //max num cars we can track
#define MAXNUMOBJS 100

#define MINNUMPOINTS 3 //min # pts to consider object a 'car'
#define NUMSCANPOINTS 181
#define SCALINGFACTOR 4 //used in determining 
                 //discontinuities for segmentation

//#warning "this should be replaced by some probabilistic calculation involving covariances of KF"
#define CARMARGIN 1.0 //if it's this close to outline of 
   //a car, classify it as part
#define RADIALOFFSET 1.0 //how far back to put additional points

#define MINSPEED 2.0   //min velocity to bother sending out
#define MAXSPEED 20.0 //max velocity 
#define MINDIMENSION 2 //we're assuming this is the minimum car dimension 
                       //used when we can only see 1 side...
#define OBJECTTIMEOUT 100 //how many loops before we delete an object
#define CARTIMEOUT 300 //how many loops before we delete a car

//for accessing the rawData array
#define ANGLE 0
#define RANGE 1

#define MAXDISTROAD 80 //max distance to roads used by the road perceptor.
#define MAXROADLANEWIDTH 20 // max road lane width

//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class LadarRoadPerceptor :public CMapElementTalker
{
  
public:
   // Constructor
  LadarRoadPerceptor();

  // Destructor
  ~LadarRoadPerceptor();
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  // Update the map
  int update();

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, LadarRoadPerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, LadarRoadPerceptor *self, const char *token);

 // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;
 
  // Sensnet module
  sensnet_t *sensnet;

  // Sensnet replay module
  sensnet_replay_t *replay;
 
  // Operation mode
  enum {modeLive, modeReplay} mode;

  // Console interface
  cotk_t *console;

  int useDisplay;
  
  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  //how many times we've looped in the main program
  int loopCount;

  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;
  Ladar ladars[16];

  vector<int> lastSentSize;

  bool send_debug;
  /************ defines from trackMO ***************/

  //angle, range data
  double rawData[NUMSCANPOINTS][2];
  //differences in RANGES between consecutive scans
  double diffs[NUMSCANPOINTS-1];
  //x,y,z data (in local frame)
  double xyzData[NUMSCANPOINTS+1][3];
  //x,y,z data in local frame, for points 1m radially
  //behind the actual return
  double depthData[NUMSCANPOINTS+1][3];


  int subgroup; //FIXME: check w/ sam what this value should be

  // For ladar data points 
  // LadarPointDataClass ladarPointData;
  // All the ladar data in vehicle frame
  float xvData[5*NUMSCANPOINTS];
  float yvData[5*NUMSCANPOINTS];
  float rotxvData[5*NUMSCANPOINTS];
  float rotyvData[5*NUMSCANPOINTS]; 

  // Kalman filter states
  float d_Kalman, var_Kalman;
  float left_Kalman, right_Kalman, var_width_Kalman;
  float varDirProcLevel, varDirMeasLevel, varWidProcLevel, varWidMeasLevel;
  int Kalman_dir, Kalman_wid; /* whether to use the Kalman estimates */
  int nbr_not_peak;

  int numData; // Number of ladar scan points actually used in the Hough transform.

  int plotDev; // whether to plot deviations in mapviewer. 
  int detectSecondPeak; // Look for a second direction or not.
  unsigned int ladarReturnOffset; // Number of closest ladar returbs discarded.
  float FODlevelSecondPeak;
  float HoughTransformThresh; 

};
#endif


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


