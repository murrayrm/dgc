
/* 
 * Desc: Road/line finding with Riegl ladar
 * Date: 16 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef LADAR_ROAD_FINDER_H
#define LADAR_ROAD_FINDER_H

/** @file

@brief Road and line finding with the Riegl ladar.

*/

// Dependencies
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/StereoImageBlob.h>
#include <dgcutils/dgc_image.h>

#include "LocalMap.hh"


/// @brief Process ladar data to detect roads and lines.
///
/// Currently, this does some minimal processing on the raw range data
/// but may be changed in future to become a shell for aggregating the
/// output from a seperate perceptor process.
class LadarRoadFinder
{
  public:

  // Constructor
  LadarRoadFinder(LocalMap *localMap);

  // Destructor
  virtual ~LadarRoadFinder();

  private:

  // Hide the copy constructor
  LadarRoadFinder(const LadarRoadFinder &that);

  public:

  // Project raw ladar data into map
  int projectLadarRaw(LocalMap *localMap, LadarRangeBlob *blob);

  // Scan the local map and update the detected lines
  int updateLines(LocalMap *localMap);
  
  private:

  // Compute an expanded notion of invalid readings
  int calcInvalid(float maxValid, int window, int numPoints, float *points);

  // Compute the difference of gaussians on a signal
  int calcDoG(float inner, float outer, int win, int numPoints, float *points);

  // Look for statistically significant single-pixel variations in a window 
  int calcVar(float thresh, int win, int numPoints, float *points);

  // Compute the number of edges in a window 
  int countLocalEdges(LocalMap *localMap, int mi, int mj, float thresh);

  // Calculate the edge response in a local region of the map
  float calcLocalEdge(LocalMap *localMap, int mi, int mj);

  public:

  // Update the intensity map image
  dgc_image_t *updateRawImage(LocalMap *localMap);

  // Update the line image
  dgc_image_t *updateLineImage(LocalMap *localMap);

  // Update the edge image
  dgc_image_t *updateEdgeImage(LocalMap *localMap);

  // Update the ladar roughness map image
  dgc_image_t *updateRoughImage(LocalMap *localMap);

  public:

  // Significance threshold for line detector (larger values imply fewer
  // false positives).
  float lineMinSigma;

  // Local filter size for line detector (must odd and >= 3).
  int lineWindow;
  
  // Local filter threshold for line detector (set to 2 to remove
  // single cell false positives).
  int lineMinCount;
  
  private:

  // Workspace images
  dgc_image_t *workImage;
  
  // Diagnostic images
  dgc_image_t *rawImage, *edgeImage, *lineImage, *roughImage;
};

#endif

