
/* 
 * Desc: Prior map data from the RNDF
 * Date: 19 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PRIOR_MAP_H
#define PRIOR_MAP_H

/** @file

@brief Prior map data from the RNDF.

*/

// Dependencies
#include <frames/pose3.h>
#include <interfaces/VehicleState.h>

// Forward declarations
namespace std
{
  class RNDF;
  class GPSPoint;
}


/// @brief Waypoint on the RNDF graph.
///
/// This can be relatively inefficient in terms of storage, since
/// there are relatively few waypoints in each RNDF.
struct PriorMapWaypoint
{
  // RNDF id
  int segmentId, laneId, waypointId;
  
  // Position in map frame
  float px, py;

  // Next waypoint(s)
  int numNext;
  PriorMapWaypoint *next[16];

  // Previous waypoint(s)
  int numPrev;
  PriorMapWaypoint *prev[16];  
};




/// @brief Map element data
struct PriorMapPoint
{
  // Pose
  float px, py, ph;
};


/// @brief Quad-tree node
struct PriorMapQuad
{
  // Quad center
  float px, py;

  // Quad size
  float size;

  // Leaves from this quad
  PriorMapQuad *leaves[4];

  // Points in this quad
  int numPoints, maxPoints;
  PriorMapPoint *points;
};


/// @brief Process ladar data to detect roads and lines.
class PriorMap
{
  public:

  /// @brief Constructor
  PriorMap();

  /// @brief Destructor
  virtual ~PriorMap();
  
  private:

  // Hide the copy constructor
  PriorMap(const PriorMap &that);
  
  public:

  /// @brief Set the parameters for RNDF interpolation
  int setInterpolation(float spacing, int rails);

  /// @brief Load RNDF file
  int load(char *filename);

  /// @brief Get the RNDF point corresponding to the given ID
  PriorMapWaypoint *getRndfPoint(int segmentId, int laneId, int waypointId);

  private:

  /// Generate the RNDF
  int genRndf(std::RNDF *rndf);

  /// Generate the waypoints for a segment/lane
  int genRndfLane(std::RNDF *rndf, int segmentId, int laneId);

  /// Generate the waypoints for a zone
  int genRndfZone(std::RNDF *rndf, int zoneId);

  /// Generate the RNDF intersections
  int genRndfIntersections(std::RNDF *rndf);

  /// Generate the dense prior map
  int genDenseGraph();

  /// Get the possible rotations for the given waypoint
  int getRotations(const PriorMapWaypoint *point, int maxRotations, float *rotations);

  /// Generate interpolated points
  int genPoints(std::RNDF *rndf, int segmentId, int laneId);

  // Generate a maneuver linking two nodes
  int genManeuver(const PriorMapPoint *nodeA, const PriorMapPoint *nodeB);

  private:

  // Add a point to the map, starting with the given quad
  int addPointMap(PriorMapQuad *quad, const PriorMapPoint *point);

  // Compute waypoint position in map coordinates
  vec3_t posFromWaypoint(std::GPSPoint *w);

  public:
  
  // Map pose in global frame
  pose3_t pose;

  private:

  // Interpolation parameters
  float spacing;
  int rails;
  
  // RNDF graph, stored as a list of waypoints.  Each waypoint points
  // to other waypoints that it connects with.
  int numWaypoints, maxWaypoints;
  PriorMapWaypoint *waypoints;
  
  public:

  /// Root of the quad-tree containing map points
  PriorMapQuad *mapRoot;
  
  /// Stats on the quad-tree
  int mapPoints, mapQuads, mapBytes;

#if USE_GL
  public:

  // Predraw the RNDF
  int predrawRndf(void);

  // Predraw the entire map
  int predrawMap(void);

  // Recursively predraw the given quad
  int predrawMapQuad(PriorMapQuad *quad);

  private:
  
  // GL display list
  int mapList;
#endif
};

#endif

