
/* 
 * Desc: Fused map (combines local with prior)
 * Date: 22 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef FUSED_MAP_H
#define FUSED_MAP_H

/** @file

@brief Fused map.

*/

// Dependencies
#include <frames/pose3.h>
#include <interfaces/VehicleState.h>
#include <dgcutils/dgc_image.h>

class LocalMap;
class PriorMap;


/// @brief Map element data
struct FusedMapPoint
{
  // Index of corresponding point in prior map
  int priorIndex;
  
  // 3D pose in local frame
  pose3_t pose;
  
  // 2D  pose in local frame (for fast c-space tests)
  float px, py, ph;

  // C-space cost 
  float cost;
};


/// @brief Process ladar data to detect roads and lines.
class FusedMap
{
  public:

  /// @brief Constructor
  FusedMap(LocalMap *localMap);

  /// @brief Destructor
  virtual ~FusedMap();
  
  private:

  // Hide the copy constructor
  FusedMap(const FusedMap &that);
  
  public:
    
  /// @brief Update based on the current vehicle state.
  int update(const VehicleState *state, const LocalMap *localMap, const PriorMap *priorMap);

  private:

  // Update the ROI: contains nearby points from the prior map.
  int updateROI(const VehicleState *state, const PriorMap *priorMap);

  // Recursive functions for grabbing points from the prior map quad-tree.
  int addPointROI(const PriorMapQuad *quad, float px, float py, float size);

  private:

  // Create the non-rotated cost image
  int createCostImages(const LocalMap *localMap);
  
  // Create a set of integral rotated cost images
  int createCostSumImages(const LocalMap *localMap);

  // Number of rotated images
  int numImages;

  // Local-to-image transform for each orientation bin
  float transforms[32][6];

  // Scale factor for each image pixel (pixel/m).
  float scale;

  // Flatness images
  dgc_image_t *flatImages[32];
  dgc_image_t *flatSumImages[32];

  public:
  
  // Compute the response at the given pose
  float testPose(float px, float py, float ph,
                 float ax, float ay, float bx, float by);

  public:
  
  /// Points in the current ROI
  int numPoints, maxPoints;
  FusedMapPoint *points;
  
#if USE_GL
  public:

  // Predraw the entire map
  int predrawMap(void);

  private:
  
  // GL display list
  int mapList;
#endif
};

#endif

