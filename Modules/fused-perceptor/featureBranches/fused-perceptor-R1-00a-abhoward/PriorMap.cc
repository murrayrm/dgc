
/* 
 * Desc: Prior map data from the RNDF
 * Date: 19 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include <frames/mat44.h>
#include <rndf/RNDF.hh>
#include <alice/AliceConstants.h>
#include <trajutils/maneuver.h>

#if USE_GL
#include <GL/gl.h>
#endif

#include "PriorMap.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
PriorMap::PriorMap()
{
  this->pose = pose3_ident();

  this->numWaypoints = this->maxWaypoints = 0;
  this->waypoints = NULL;

  this->spacing = 1.0;
  this->rails = 3;
  
  this->mapRoot = NULL;
  this->mapQuads = 0;
  this->mapBytes = 0;
  this->mapList = 0;
  
  return;
}


// Destructor
PriorMap::~PriorMap()
{  
  // TODO: deallocate quad-tree
  
  return;
}


// Set the parameters for RNDF interpolation
int PriorMap::setInterpolation(float spacing, int rails)
{
  this->spacing = spacing;
  this->rails = rails;

  return 0;
}

// Load RNDF file
int PriorMap::load(char *filename)
{
  int sn, ln;
  std::RNDF rndf;
  std::Segment *segment;
  std::Lane *lane;
  std::Zone *zone;
  std::Waypoint *wp;
  vec3_t p;
  
  // Load RNDF from file
  if (filename)
  {
    MSG("loading RNDF %s", filename);
    if (!rndf.loadFile(filename))
      return ERROR("unable to load %s", filename);
  }

  // Take a first pass to count waypoints 
  this->maxWaypoints = 0;
  for (sn = 0; sn < (int) rndf.getAllSegments().size(); sn++)
  {
    segment = rndf.getAllSegments()[sn];    
    for (ln = 0; ln < (int) segment->getAllLanes().size(); ln++)
    {
      lane = segment->getAllLanes()[ln];
      this->maxWaypoints += lane->getAllWaypoints().size();
    }
  }
  for (sn = 0; sn < (int) rndf.getAllZones().size(); sn++)
  {
    zone = rndf.getAllZones()[sn];
    this->maxWaypoints += zone->getAllPerimeterPoints().size();
    this->maxWaypoints += zone->getAllSpots().size() * 2;
  }

  // Pre-allocate space for waypoints 
  this->waypoints = (PriorMapWaypoint*) calloc(this->maxWaypoints, sizeof(this->waypoints[0]));
  
  // Get any waypoint to initialize the pose
  wp = rndf.getWaypoint(1, 1, 1);
  assert(wp);
    
  // Set the map origin
  this->pose.pos.x = wp->getNorthing();
  this->pose.pos.y = wp->getEasting();
  this->pose.pos.z = 0;
  this->pose.rot = quat_ident(); 

  // Create the root node
  this->mapRoot = (PriorMapQuad*) calloc(1, sizeof(*this->mapRoot));
  this->mapRoot->size = 4096; // MAGIC
  p = this->posFromWaypoint(wp);
  this->mapRoot->px = p.x;
  this->mapRoot->py = p.y;

  // Generate the RNDF graph
  this->genRndf(&rndf);

  MSG("create: waypoints %d size %.3f Mb",
      this->numWaypoints, this->maxWaypoints * sizeof(this->waypoints[0]) * 1e-6);
  
  // Generate the dense graph
  this->genDenseGraph();

  MSG("created: quads = %d points = %.3fM size = %.3f Mb",
      this->mapQuads, this->mapPoints * 1e-6, this->mapBytes * 1e-6);
  
  return 0;
}



// Generate the RNDF
int PriorMap::genRndf(std::RNDF *rndf)
{
  std::Segment *segment;
  std::Lane *lane;
  std::Zone *zone;
  int sn, ln;

  // Parse out lanes
  for (sn = 0; sn < (int) rndf->getAllSegments().size(); sn++)
  {
    segment = rndf->getAllSegments()[sn];    
    for (ln = 0; ln < (int) segment->getAllLanes().size(); ln++)
    {
      lane = segment->getAllLanes()[ln];      
      this->genRndfLane(rndf, lane->getSegmentID(), lane->getLaneID());
    }
  }

  // Parse out zones
  for (sn = 0; sn < (int) rndf->getAllZones().size(); sn++)
  {
    zone = rndf->getAllZones()[sn];
    this->genRndfZone(rndf, zone->getZoneID());
  }

  // Create the intersections
  this->genRndfIntersections(rndf);

  return 0;
}


// Generate the waypoints for a segment/lane
int PriorMap::genRndfLane(std::RNDF *rndf, int segmentId, int laneId)
{
  int i;
  std::Lane *lane;
  std::vector<std::Waypoint*> waypoints;
  std::Waypoint *wp;
  vec3_t pp;
  PriorMapWaypoint *pointA, *pointB;

  lane = rndf->getLane(segmentId, laneId);
  waypoints = lane->getAllWaypoints();

  // Step through all the waypoints in the lane.  
  for (i = 0; i < (int) waypoints.size(); i++)
  {
    wp = waypoints[i];
    pp = posFromWaypoint(wp);

    // Add new waypoint
    assert(this->numWaypoints < this->maxWaypoints);
    pointB = this->waypoints + this->numWaypoints++;
    memset(pointB, 0, sizeof(*pointB));
    pointB->segmentId = wp->getSegmentID();
    pointB->laneId = wp->getLaneID();
    pointB->waypointId = wp->getWaypointID();
    pointB->px = pp.x;
    pointB->py = pp.y;

    if (i == 0)
      continue;
    if (this->numWaypoints < 2)
      continue;

    // Set up links with previous waypoint (the last one we added must
    // be a waypoint in the same lane).
    pointA = this->waypoints + this->numWaypoints - 2;
    assert((size_t) pointA->numNext < sizeof(pointA->next)/sizeof(pointA->next[0]));
    pointA->next[pointA->numNext++] = pointB;
    assert((size_t) pointB->numPrev < sizeof(pointB->prev)/sizeof(pointB->prev[0]));
    pointB->prev[pointB->numPrev++] = pointA;
  }

  return 0;
}


// Generate the waypoints for a zone
int PriorMap::genRndfZone(std::RNDF *rndf, int zoneId)
{
  int i;
  std::Zone *zone;
  std::vector<std::PerimeterPoint*> points;
  std::PerimeterPoint *point;
  std::vector<std::ParkingSpot*> spots;
  std::ParkingSpot *spot;
  std::Waypoint *wa, *wb;
  PriorMapWaypoint *pointA, *pointB;    
  vec3_t pa, pb;

  zone = rndf->getZone(zoneId);

  // Step through all the perimeter points in the zone
  points = zone->getAllPerimeterPoints();
  for (i = 0; i < (int) points.size(); i++)
  {
    point = points[i];
    pa = posFromWaypoint(point);
    
    // Add new perimeter point
    assert(this->numWaypoints < this->maxWaypoints);
    pointA = this->waypoints + this->numWaypoints++;
    memset(pointA, 0, sizeof(*pointA));
    pointA->segmentId = point->getSegmentID();
    pointA->laneId = point->getLaneID();
    pointA->waypointId = point->getWaypointID();
    pointA->px = pa.x;
    pointA->py = pa.y;
  }

  /* REMOVE
  // Join the perimeter in a closed polygon.
  for (i = 0; i < (int) points.size(); i++)
  {
    pointA = this->waypoints + this->numWaypoints - points.size() + i;
    pointB = this->waypoints + this->numWaypoints - points.size() + (i + 1) % points.size();      
    assert((size_t) pointA->numNext < sizeof(pointA->next)/sizeof(pointA->next[0]));
    pointA->next[pointA->numNext++] = pointB;
    assert((size_t) pointB->numPrev < sizeof(pointB->prev)/sizeof(pointB->prev[0]));
    pointB->prev[pointB->numPrev++] = pointA;
  }
  */

  // Step through all the spots in the zone.
  spots = zone->getAllSpots();
  for (i = 0; i < (int) spots.size(); i++)
  {
    spot = spots[i];
    
    wa = spot->getWaypoint(1);
    wb = spot->getWaypoint(2);

    pa = posFromWaypoint(wa);
    pb = posFromWaypoint(wb);

    // Add new waypoint
    assert(this->numWaypoints < this->maxWaypoints);
    pointA = this->waypoints + this->numWaypoints++;
    memset(pointA, 0, sizeof(*pointA));
    pointA->segmentId = wa->getSegmentID();
    pointA->laneId = wa->getLaneID(); 
    pointA->waypointId = wa->getWaypointID();
    pointA->px = pa.x;
    pointA->py = pa.y;

    // Add new waypoint
    assert(this->numWaypoints < this->maxWaypoints);
    pointB = this->waypoints + this->numWaypoints++;
    memset(pointB, 0, sizeof(*pointB));
    pointB->segmentId = wb->getSegmentID();
    pointB->laneId = wb->getLaneID();
    pointB->waypointId = wb->getWaypointID();
    pointB->px = pb.x;
    pointB->py = pb.y;

    assert((size_t) pointA->numNext < sizeof(pointA->next)/sizeof(pointA->next[0]));
    pointA->next[pointA->numNext++] = pointB;
    assert((size_t) pointB->numPrev < sizeof(pointB->prev)/sizeof(pointB->prev[0]));
    pointB->prev[pointB->numPrev++] = pointA;
  }

  return 0;
}


// Generate the RNDF intersections
int PriorMap::genRndfIntersections(std::RNDF *rndf)
{
  int i, j;
  std::vector<std::GPSPoint*> waypoints;
  std::GPSPoint *wayA, *wayB;
  PriorMapWaypoint *pointA, *pointB;

  // Step through all the waypoints in our list.
  for (i = 0; i < this->numWaypoints; i++)
  {
    pointA = this->waypoints + i;

    // Get the list of RNDF waypoints that we can go to
    if (pointA->laneId > 0)
      wayA = rndf->getWaypoint(pointA->segmentId, pointA->laneId, pointA->waypointId);
    else
      wayA = rndf->getPerimeterPoint(pointA->segmentId, pointA->waypointId);
    assert(wayA);
    waypoints = wayA->getEntryPoints();

    for (j = 0; j < (int) waypoints.size(); j++)
    {
      wayB = waypoints[j];
      pointB = this->getRndfPoint(wayB->getSegmentID(), wayB->getLaneID(), wayB->getWaypointID());
      assert(pointB);

      // Set up links 
      assert((size_t) pointA->numNext < sizeof(pointA->next)/sizeof(pointA->next[0]));
      pointA->next[pointA->numNext++] = pointB;
      assert((size_t) pointB->numPrev < sizeof(pointB->prev)/sizeof(pointB->prev[0]));
      pointB->prev[pointB->numPrev++] = pointA;
    }
  }

  return 0;
}


// Get the RNDF point corresponding to the given ID
PriorMapWaypoint *PriorMap::getRndfPoint(int segmentId, int laneId, int waypointId)
{
  int i;
  PriorMapWaypoint *point;

  for (i = 0; i < this->numWaypoints; i++)
  {
    point = this->waypoints + i;

    if (point->segmentId == segmentId &&
        point->laneId == laneId &&
        point->waypointId == waypointId)
      return point;
  }
  
  return NULL;
}


// Generate the dense prior map
int PriorMap::genDenseGraph()
{
  int i, j, k;
  int na, nb;
  PriorMapWaypoint *pointA, *pointB;
  PriorMapPoint nodeA, nodeB;
  int numRotationsA, numRotationsB;
  float rotationsA[16], rotationsB[16];
  int rails;

  // Set the number of rails
  rails = (this->rails - 1)/2;
  
  for (i = 0; i < this->numWaypoints; i++)
  {
    pointA = this->waypoints + i;

    // Get the possible rotations for this node
    numRotationsA = this->getRotations(pointA, 16, rotationsA);
    
    for (j = 0; j < pointA->numNext; j++)
    {
      pointB = pointA->next[j];

      // Dont densify zone perimeter points
      //if (pointA->laneId == 0 && pointB->laneId == 0)
      // continue;
      
      // Get the possible rotations for this node
      numRotationsB = this->getRotations(pointB, 16, rotationsB);

      // Create multiple rails
      for (k = -rails; k <= +rails; k++)
      {
        for (na = 0; na < numRotationsA; na++)
        {
          nodeA.px = pointA->px + k * 0.5 * cos(rotationsA[na] + M_PI/2);
          nodeA.py = pointA->py + k * 0.5 * sin(rotationsA[na] + M_PI/2);
          nodeA.ph = rotationsA[na];

          // Generate initial nodes 
          this->addPointMap(this->mapRoot, &nodeA);
            
          for (nb = 0; nb < numRotationsB; nb++)
          {
            nodeB.px = pointB->px + k * 0.5 * cos(rotationsB[nb] + M_PI/2);
            nodeB.py = pointB->py + k * 0.5 * sin(rotationsB[nb] + M_PI/2);
            nodeB.ph = rotationsB[nb];
            this->genManeuver(&nodeA, &nodeB);
          }
        }
      }
    }

    MSG("quads = %d points = %.3fM size = %.3f Mb",
        this->mapQuads, this->mapPoints * 1e-6, this->mapBytes * 1e-6);
  }
    
  return 0;
}


// Get the possible rotations for the given waypoint
int PriorMap::getRotations(const PriorMapWaypoint *point, int maxRotations, float *rotations)
{
  int i;
  int numRotations;
  PriorMapWaypoint *pointB;
  
  numRotations = 0;

  // Generate a rotations corresponding to each orientation of the
  // incoming (previous) nodes.
  for (i = 0; i < point->numPrev; i++)
  {
    pointB = point->prev[i];
    if (point->laneId > 0 && pointB->laneId > 0)
      if (pointB->segmentId != point->segmentId || pointB->laneId != point->laneId)
        continue;
    assert(numRotations < maxRotations);
    rotations[numRotations++] = atan2((point->py - pointB->py), (point->px - pointB->px));
  }

  // Generate a rotations corresponding to each orientation of the
  // outgoing (next) nodes.
  for (i = 0; i < point->numNext; i++)
  {
    pointB = point->next[i];
    if (point->laneId > 0 && pointB->laneId > 0)
      if (pointB->segmentId != point->segmentId || pointB->laneId != point->laneId)
        continue;
    assert(numRotations < maxRotations);
    rotations[numRotations++] = atan2((pointB->py - point->py), (pointB->px - point->px));
  }

  return numRotations;
}


// Generate a maneuver linking two nodes
int PriorMap::genManeuver(const PriorMapPoint *nodeA, const PriorMapPoint *nodeB)
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D poseB;
  VehicleConfiguration configA, config;
  double stepSize, dm, s;
  int i, numSteps;
  PriorMapPoint dst;

  double wheelBase, maxSteer, maxTurn;
  
  assert(nodeA);
  assert(nodeB);

#define MAXSTEER 30
#define MAXTURN 90

  wheelBase = VEHICLE_WHEELBASE;
  maxSteer = MAXSTEER * M_PI/180;
  maxTurn = MAXTURN * M_PI/180;

  // Initial vehicle pose
  configA.x = nodeA->px;
  configA.y = nodeA->py;
  configA.theta = nodeA->ph;
  configA.phi = 0;

  // Final vehicle pose
  poseB.x = nodeB->px;
  poseB.y = nodeB->py;
  poseB.theta = nodeB->ph;

  //MSG("man %f %f : %f %f", poseA.x, poseA.y, poseB.x, poseB.y);

  // Vehicle properties
  vp = maneuver_create_vehicle(wheelBase, maxSteer);
  
  // Create maneuver object
  mp = maneuver_config2pose(vp, &configA, &poseB);

  // Some maneuvers may not be possible, so trap this.
  if (!mp)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  stepSize = this->spacing;
  
  // Distance between nodes
  dm = sqrtf(pow(nodeB->px - nodeA->px, 2) + pow(nodeB->py - nodeA->py, 2));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / stepSize);

  // Dont do very, very short maneuvers.
  if (numSteps < 1)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  //MSG("%f %f %f : %f %f %f",
  //    poseA.x, poseA.y, poseA.theta, poseB.x, poseB.y, poseB.theta);

  /* TODO
  // Check for unfeasable maneuvers. Note the iteration includes the final
  // step in the trajectory (we check 0 to 1 inclusive), to trap the case
  // where the final step has a huge change in theta.
  feasible = true;
  theta = configA.theta;

  for (i = 1; i < numSteps + 1; i++) 
  {
    s = (double) i / numSteps;

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    //fprintf(stdout, "config %d %f %f %f %f %f\n",
    //        i, s, config.x, config.y, config.theta, config.phi);
    
    // Discard turns outside the steering limit
    if (fabs(config.phi) > vp->steerlimit)
      feasible = false;

    // Discard big changes in yaw that occur too fast to be
    // caught by the steering limit test at a course step size.
    if (acos(cos(config.theta - theta)) > MAXTURN * M_PI/180) // MAGIC
      feasible = false;

    theta = config.theta;
  }

  if (!feasible)
  {
    maneuver_free(mp);
    free(vp);
    return -1;      
  }
  */

  // Create the intermediate nodes for this maneuver.  This does not
  // create the first or last nodes (nodeA or nodeB).
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    

    // Fill out basic node data
    dst.px = config.x;
    dst.py = config.y;
    dst.ph = config.theta;

    // Add to the map
    this->addPointMap(this->mapRoot, &dst);
  }

  return 0;
}




// Add a point to the map, starting with the given quad
int PriorMap::addPointMap(PriorMapQuad *quad, const PriorMapPoint *point)
{
  int li;
  PriorMapQuad *leaf;

  //MSG("add %f %f %f", quad->px, quad->py, quad->size/2);

  // Make sure point falls within the quad
  if (fabs(point->px - quad->px) > quad->size/2)
    return ERROR("point %f,%f is outside map", point->px, point->py);
  if (fabs(point->py - quad->py) > quad->size/2)
    return ERROR("point %f,%f is outside map", point->py, point->py);
  
  // If we havent arrived at a leaf yet...
  if (quad->size > 2.0) // MAGIC
  {
    // See which leaf this point belongs to
    li = 2 * (point->py < quad->py) + (point->px < quad->px);
    leaf = quad->leaves[li];
    
    // Create the leaf if it is not there
    if (leaf == NULL)
    {
      leaf = quad->leaves[li] = (PriorMapQuad*) calloc(1, sizeof(*leaf));
      leaf->size = quad->size / 2;
      if (point->px < quad->px)
        leaf->px = quad->px - leaf->size/2;
      else
        leaf->px = quad->px + leaf->size/2;
      if (point->py < quad->py)
        leaf->py = quad->py - leaf->size/2;
      else
        leaf->py = quad->py + leaf->size/2;

      this->mapQuads += 1;
      this->mapBytes += sizeof(*leaf);      
    }
    
    // Recursuvely add point to leaf
    return this->addPointMap(leaf, point);
  }

  // Make room for more points if necessary
  if (quad->numPoints >= quad->maxPoints)
  {
    this->mapBytes -= quad->maxPoints * sizeof(quad->points[0]);
    quad->maxPoints += 8;
    quad->points = (PriorMapPoint*) realloc(quad->points,
                                            quad->maxPoints * sizeof(quad->points[0]));
    this->mapBytes += quad->maxPoints * sizeof(quad->points[0]);
  }
  
  // If we have arrived at a leaf, add in the point
  quad->points[quad->numPoints++] = *point;
  this->mapPoints += 1;
    
  return 0;
}



// Compute waypoint position in map coordinates
vec3_t PriorMap::posFromWaypoint(std::GPSPoint *w)
{
  vec3_t p, q;
  p.x = w->getNorthing();
  p.y = w->getEasting();
  p.z = 0;
  q = vec3_transform(pose3_inv(this->pose), p);
  return q;
}




#if USE_GL


// Predraw the RNDF
int PriorMap::predrawRndf(void)
{
  int i, j;
  PriorMapWaypoint *pointA, *pointB;
  
  // Create display list
  if (this->mapList == 0)
    this->mapList = glGenLists(1);
  glNewList(this->mapList, GL_COMPILE);

  for (i = 0; i < this->numWaypoints; i++)
  {
    pointA = this->waypoints + i;

    if (pointA->laneId > 0)
      glColor3f(1, 1, 0);
    else
      glColor3f(1, 0, 0);
        
    // Draw waypoints
    glBegin(GL_QUADS);
    glVertex2f(pointA->px - 0.5, pointA->py - 0.5);
    glVertex2f(pointA->px + 0.5, pointA->py - 0.5);
    glVertex2f(pointA->px + 0.5, pointA->py + 0.5);
    glVertex2f(pointA->px - 0.5, pointA->py + 0.5);
    glEnd();

    // Draw arcs to next waypoints
    glColor3f(0, 0, 1);
    glBegin(GL_LINES);
    for (j = 0; j < pointA->numNext; j++)
    {
      pointB = pointA->next[j];
      glVertex2f(pointA->px, pointA->py);
      glVertex2f(pointB->px, pointB->py);
    }
    glEnd();

    // Draw arcs to previous waypoints.  These should be identical, so
    // this is a test of graph integrity.
    if (false)
    {
      glColor3f(1, 0, 0);
      glBegin(GL_LINES);
      for (j = 0; j < pointA->numPrev; j++)
      {
        pointB = pointA->prev[j];    
        glVertex3f(pointA->px, pointA->py, 1);
        glVertex3f(pointB->px, pointB->py, 1);
      }
      glEnd();
    }
  }


  glEndList();
    
  return this->mapList;
}

// Predraw the entire prior 
int PriorMap::predrawMap(void)
{
  // Create display list
  if (this->mapList == 0)
    this->mapList = glGenLists(1);
  glNewList(this->mapList, GL_COMPILE);

  // Recursively draw the map
  this->predrawMapQuad(this->mapRoot);
  
  glEndList();
    
  return this->mapList;
}


// Recursively predraw the given quad
int PriorMap::predrawMapQuad(PriorMapQuad *quad)
{
  int i;
  PriorMapQuad *leaf;
  PriorMapPoint *point;

  if (false)
  {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glColor3f(0, 1, 0);
    glBegin(GL_QUADS);
    glVertex2f(quad->px - quad->size/2, quad->py - quad->size/2);
    glVertex2f(quad->px + quad->size/2, quad->py - quad->size/2);
    glVertex2f(quad->px + quad->size/2, quad->py + quad->size/2);
    glVertex2f(quad->px - quad->size/2, quad->py + quad->size/2);
    glEnd();
  }

  // Recursively draw the map
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->predrawMapQuad(leaf);
  }

  // Draw our own points
  glBegin(GL_LINES);
  for (i = 0; i < quad->numPoints; i++)
  {
    point = quad->points + i;
    glColor3f(1, 0, 0);
    glVertex2f(point->px, point->py);
    glColor3f(0, 1, 0);
    glVertex2f(point->px + 0.50 * cos(point->ph), point->py + 0.50 * sin(point->ph));
  }
  glEnd();
      
  return 0;
}


#endif
