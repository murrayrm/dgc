
/* 
 * Desc: Road/line finding with Riegl ladar
 * Date: 16 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <opencv/cv.h>

#include <frames/mat44.h>
#include "LadarRoadFinder.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
LadarRoadFinder::LadarRoadFinder(LocalMap *localMap)
{
  // Initialize diagnostic images
  this->rawImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);
  this->lineImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);
  this->roughImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);  
  
  return;
}


// Destructor
LadarRoadFinder::~LadarRoadFinder()
{  
  dgc_image_free(this->roughImage);
  dgc_image_free(this->lineImage);
  dgc_image_free(this->rawImage);

  return;
}


// Project ladar data into map
int LadarRoadFinder::projectLadarRaw(LocalMap *localMap, LadarRangeBlob *blob)
{
  int i, di, dj;
  float pa, pr;
  float sx, sy, sz;
  float vx, vy, vz;
  float mx, my, mz;
  int mi, mj;
  uint8_t raw;
  float m[4][4];
  LocalMapCell *cell, *ncell;
  float rawEdges[LADAR_BLOB_MAX_POINTS];
  float rangeValid[LADAR_BLOB_MAX_POINTS];
  float rangeEdges[LADAR_BLOB_MAX_POINTS];
  float lines[LADAR_BLOB_MAX_POINTS];
  
  // Run an edge detector on the intensity data
  for (i = 0; i < blob->numPoints; i++)
    rawEdges[i] = (float) blob->intensities[i];    
  //this->calcDoG(0.3, 3, 9, blob->numPoints, rawEdges);   // MAGIC
  this->calcVar(4, 11, blob->numPoints, rawEdges);

  // Run an edge detector on the range data
  for (i = 0; i < blob->numPoints; i++)
  {
    rangeValid[i] = blob->points[i][1];
    rangeEdges[i] = blob->points[i][1];
  }
  this->calcInvalid(150, 3 * 5, blob->numPoints, rangeValid); // MAGIC
  this->calcDoG(0.3, 5, 15, blob->numPoints, rangeEdges);   // MAGIC

  // Decide if this is a lane marking
  for (i = 0; i < blob->numPoints; i++)
  {
    if (fabs(rangeEdges[i]) < 0.10) // MAGIC
      lines[i] = rawEdges[i];
    else
      lines[i] = 0;
  }

  //for (i = 0; i < blob->numPoints; i++)
  //{
  //  printf("%d %f %d %f %f\n", i,
  //         blob->points[i][1], blob->intensities[i],
  //         rangeEdges[i], rawEdges[i]);
  //} 
  //printf("\n\n");
  //fflush(stdout);
  
  // Compute transform from sensor to vehicle to local to map frame
  mat44f_mul(m, blob->veh2loc, blob->sens2veh);
  mat44f_mul(m, localMap->transML, m);

  for (i = 0; i < blob->numPoints; i++)
  {
    pa = blob->points[i][0];
    pr = blob->points[i][1];
    raw = blob->intensities[i];
    
    if (pr < 0 || pr > 80.0) // MAGIC
      continue;

    // Compute points in sensor frame
    LadarRangeBlobScanToSensor(blob, pa, pr, &sx, &sy, &sz);

    // Convert to vehicle frame
    LadarRangeBlobSensorToVehicle(blob, sx, sy, sz, &vx, &vy, &vz);
    
    // Discard points that are higher in the vehicle frame; these
    // probably dont come from the ground.
    if (vz < -0.5) // MAGIC
      continue;
    
    // Convert to map frame
    mx = m[0][0]*sx + m[0][1]*sy + m[0][2]*sz + m[0][3];
    my = m[1][0]*sx + m[1][1]*sy + m[1][2]*sz + m[1][3];
    mz = m[2][0]*sx + m[2][1]*sy + m[2][2]*sz + m[2][3];
        
    // Get the cell at this location
    localMap->cvtPosToIndex(mx, my, &mi, &mj);
    cell = localMap->getCellByIndex(mi, mj);
    if (!cell)
      continue;

    // If this is the first time we have seen the cell, reset the
    // counters
    if (cell->ladarSeen == 0)
    {
      cell->ladarSeen = 1;
      cell->ladarRawCount = 0;
      cell->ladarRaw1 = 0;
      cell->ladarRaw2 = 0;
      cell->ladarLineCount = 0;
      cell->ladarLine = 0;
      cell->ladarRoughCount = 0;
      cell->ladarRough = 0;
    }
    
    // Update the cell intensity values.
    cell->ladarRawCount += 1;
    cell->ladarRaw1 += raw;
    cell->ladarRaw2 += raw * raw;
    cell->ladarLineCount += 1;
    cell->ladarLine += lines[i];
    
    // Update the cell roughness in regions where the ladar is valid.
    if (rangeValid[i])
    {
      cell->ladarRoughCount += 1;
      cell->ladarRough += rangeEdges[i];
    }

    // TESTING
    //MSG("%f %f %d %d", mx, my, mi, mj);
    
    // Fill in nearby cells if they haven't already been seen.  These
    // values serve as a default that gets over-written by the first
    // ladar point that lands in the cell (the cell is "seen").  Kind
    // of an on-the-fly mean filter for unknown cells.  
    if (false)
    {
      for (dj = -1; dj <= +1; dj++)
      {
        for (di = -1; di <= +1; di++)
        {
          ncell = localMap->getCellByIndex(mi + di, mj + dj);
          if (!ncell)
            continue;
          if (ncell->ladarSeen)
            continue;
          ncell->ladarRawCount += 1;
          ncell->ladarRaw1 += raw;
          ncell->ladarRaw2 += raw * raw;
          ncell->ladarLineCount += 1;
          ncell->ladarLine += lines[i];
          if (rangeValid[i])
          {
            ncell->ladarRoughCount += 1;
            ncell->ladarRough += rangeEdges[i];
          }
        }
      }
    }
  }
  //fprintf(stdout, "\n\n");
  //fflush(stdout);
  
  return 0;
}


// Compute an expanded notion of invalid readings
int LadarRoadFinder::calcInvalid(float maxValid, int window, int numPoints, float *points)
{
  int i, di;
  float filt[numPoints];

  for (i = 0; i < window; i++)
    filt[i] = 0;
  for (; i < numPoints - window; i++)
  {
    filt[i] = 1;
    for (di = -window; di <= +window; di++)
    {
      if (points[i + di] > maxValid)
      {
        filt[i] = 0;
        break;
      }
    }
  }
  for (; i < numPoints; i++)
    filt[i] = 0;
  
  // Overwrite in input signal
  for (i = 0; i < numPoints; i++)
    points[i] = filt[i];
    
  return 0;
}


// Compute the difference of gaussians on a signal
int LadarRoadFinder::calcDoG(float inner, float outer, int win, int numPoints, float *points)
{
  int i, di;
  int numInner, numOuter;
  float norm;
  float kernInner[64], kernOuter[64];
  float filtInner[numPoints], filtOuter[numPoints];

  numInner = (win - 1) / 2;
  numOuter = (win - 1) / 2;
  assert(numOuter >= numInner);

  // Construct convolution kernels
  norm = 0;
  for (di = -numInner; di <= +numInner; di++)
    norm += (float) exp(-di*di/(inner*inner)/2);
  assert((size_t) (2 * numInner + 1) < sizeof(kernInner)/sizeof(kernInner[0]));
  for (di = -numInner; di <= +numInner; di++)
    kernInner[di + numInner] = (float) exp(-di*di/(inner*inner)/2) / norm;

  norm = 0;
  for (di = -numOuter; di <= +numOuter; di++)
    norm += (float) exp(-di*di/(outer*outer)/2);
  assert((size_t) (2 * numOuter + 1) < sizeof(kernOuter)/sizeof(kernOuter[0]));
  for (di = -numOuter; di <= +numOuter; di++)
    kernOuter[di + numOuter] = (float) exp(-di*di/(outer*outer)/2) / norm;

  // Compute inner gaussian filter.  Note that we use the radius of
  // the outer filter when discarding edges; this allows us to safely
  // take the difference of inner and outer without creating
  // artifacts.
  for (i = 0; i < numOuter; i++)
    filtInner[i] = 0;
  for (; i < numPoints - numOuter; i++)
  {
    filtInner[i] = 0;
    for (di = -numInner; di <= +numInner; di++)
      filtInner[i] += points[di + i] * kernInner[di + numInner];
  }
  for (; i < numPoints; i++)
    filtInner[i] = 0;

  // Compute outer gaussian filter
  for (i = 0; i < numOuter; i++)
    filtOuter[i] = 0;
  for (; i < numPoints - numOuter; i++)
  {
    filtOuter[i] = 0;
    for (di = -numOuter; di <= +numOuter; di++)
      filtOuter[i] += points[di + i] * kernOuter[di + numOuter];
  }
  for (; i < numPoints; i++)
    filtOuter[i] = 0;

  // Compute difference of gaussians and overwrite in input signal
  for (i = 0; i < numPoints; i++)
    points[i] = filtInner[i] - filtOuter[i];
  
  return 0;
}


// Look for statistically significant variations in a window 
int LadarRoadFinder::calcVar(float thresh, int win, int numPoints, float *points)
{
  int i, di;
  float m0, m1, m2;
  float filt[numPoints];

  win = (win - 1) / 2;

  for (i = 0; i < win; i++)
    filt[i] = 0;
  for (i = win; i < numPoints - win; i++)
  {
    // Compute mean and variance in this window (excluding the center pixel)
    m0 = m1 = m2 = 0;
    for (di = -win; di <= +win; di++)
    {
      if (di == 0)
        continue;
      m0 += 1;
      m1 += points[i + di];
      m2 += points[i + di] * points[i + di];
    }

    m1 = m1/m0;
    m2 = sqrtf(m2/m0 - m1*m1);

    // Suppress anything below the mean + some multiple of the std dev
    if (points[i] < m1 + thresh*m2)
      filt[i] = 0;
    else
      filt[i] = 1; // TESTING points[i];
  }
  for (; i < numPoints; i++)
    filt[i] = 0;
  
  memcpy(points, filt, numPoints * sizeof(points[0]));
  
  return 0;
}
  

/* TODO Use perhaps?
// Remove line noise, such as single pixels in the edgeImage.  This
// should probably do a connected components check, but right now it
// simply counts not zero pixels in a neighborhood.  The results are
// placed in the local map.
int LadarRoadFinder::filterLineNoise(LocalMap *localMap, dgc_image_t *edgeImage, int minCount)
{
  int i, j, di, dj;
  int count;
  LocalMapCell *cell;
  int w;
  uint8_t *cpix, *dpix;

  // Window for the variance filter
  w = (3 - 1) / 2;

  for (j = w; j < edgeImage->rows - w; j++)
  {
    cpix = dgc_image_pixel(this->edgeImage, w, j);
    
    for (i = w; i < edgeImage->cols - w; i++, cpix++)
    {
      // Compute the number of non-zero neighbors
      count = 0;
      for (dj = -w; dj <= +w; dj++)
      {
        for (di = -w; di <= +w; di++)
        {
          dpix = cpix + di + dj * edgeImage->cols;
          if (dpix[0] > 0)
            count++;
        }
      }

      cell = localMap->getCellByIndex(i, j);
      if (cpix[0] > 0 && count >= minCount)
      {
        cell->ladarLineCount = cell->ladarRawCount;
        cell->ladarLine = cell->ladarRaw1;
      }
      else
      {
        cell->ladarLineCount = 0;
        cell->ladarLine = 0;
      }    
    }
  }
  
  return 0;
}
*/


// Update the intensity image
dgc_image_t *LadarRoadFinder::updateRawImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;
  int k;

  pix = (uint8_t*) dgc_image_pixel(this->rawImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);
      if (cell->ladarRawCount > 0)
      {
        k = 2 * cell->ladarRaw1 / cell->ladarRawCount; // MAGIC
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->rawImage;
}


// Update the line image
dgc_image_t *LadarRoadFinder::updateLineImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;
  float k;
        
  pix = (uint8_t*) dgc_image_pixel(this->lineImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);
      if (cell->ladarLineCount > 0)
      {
        k = 0xFF * cell->ladarLine / cell->ladarLineCount; // MAGIC
        if (k < 0) k = 0;
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->lineImage;
}


// Update the rough image
dgc_image_t *LadarRoadFinder::updateRoughImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;
  float k;
        
  pix = (uint8_t*) dgc_image_pixel(this->roughImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);
      if (cell->ladarRoughCount > 0)
      {
        k = 0x80 - cell->ladarRough / cell->ladarRoughCount * 500; // MAGIC
        if (k < 0) k = 0;
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->roughImage;
}
