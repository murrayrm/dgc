
/* 
 * Desc: Fused perceptor: combines data from multple sources into a grid representation.
 * Date: 27 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef FUSED_PERCEPTOR_HH
#define FUSED_PERCEPTOR_HH

#if USE_GL
#include <GL/gl.h>
#endif

#include <frames/pose3.h>
#include <dgcutils/dgc_image.h>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/RoadLineBlob.h>

#include "PriorMap.hh"
#include "LocalMap.hh"
#include "LadarRoadFinder.hh"
#include "StereoRoadFinder.hh"


/// @brief Fused data from multiple sources into cost map, and provide
/// fast queries for the cost of different vehicle configurations.
class FusedPerceptor
{
  public:

  // Default constructor
  FusedPerceptor();

  // Destructor
  ~FusedPerceptor();

  private:

  // Hide the copy constructor
  FusedPerceptor(const FusedPerceptor &that);

  public:
  
  /// @brief Initialize perceptor (live mode)
  int initLive(const char *spreadDaemon, int skynetKey, int moduleId);
  
  /// @brief Initialize perceptor (replay mode)
  int initReplay(int numFilenames, char **filenames, double seekTime);

  /// @brief Finialize perceptor
  int fini(void);
    
  /// @brief Load RNDF file
  int loadRNDF(char *filename);

  /// @brief Update the perceptor with current data (non-blocking).
  int update();

  /// @brief Update the perceptor (live mode)
  int updateLive();

  /// @brief Update the perceptor (replay mode)
  int updateReplay();

  /// @brief Update based on state data.
  ///
  /// @param[in] state Vehicle state message.
  int updateState(const VehicleState *state);

  /// @brief Project raw data from the ladar feeder.
  ///
  int updateLadarRange(LadarRangeBlob *blob);

  /// @brief Project raw data from stereo feeder.
  ///
  int updateStereoImage(StereoImageBlob *blob);

  /// @brief Project line data from road perceptors.
  ///
  int updateRoadLine(RoadLineBlob *blob);

  public:
  
  /// @brief Set the dimensions of the vehicle for c-space cost calculations.
  ///
  /// This function can be called at any time (e.g., before or after prepareCost);
  ///
  /// @param[in] (ax, ay) The lower-left corner of the vehicle rectangle (vehicle frame).
  /// @param[in] (bx, by) The upper-left corner of the vehicle rectangle (vehicle frame).  
  int setDims(float ax, float ay, float bx, float by);

  /// @brief Prepare the internal tables for pose evaluations.
  ///
  /// @param[in] state Current vehicle state, used to compute the transform
  /// from the site frame to the local frame.
  int prepare(const VehicleState *state);
  
  /// @brief Compute the distance to the lane center-line, based on the detected lines.
  ///
  /// @param[in] (px, py, ph) 2D pose (position and heading) in the site frame.
  /// @returns Returns the distance to the center-line.
  float calcLineDist(float px, float py, float ph);

  private:

  /// Create the original line image
  int createLineImage();

  /// Create the distance transform to detected lines.
  int createLineDistImage();

  /// Create a set of line integral images
  int createLineSumImages();
      
  public:

  // What mode are we in?
  enum {modeLive, modeReplay} mode;

  // Spread settings
  const char *spreadDaemon;
  int skynetKey;
  
  // Sensnet object for live mode
  sensnet_t *sensnet;

  // Sensnet replay objct for replay mode
  sensnet_replay_t *replay;

  // Current state data (dianostic purposes only).
  VehicleState state;
  
  // Current blob data
  int ladarBlobId;
  LadarRangeBlob ladarBlob;
     
  // Current stereo blob data
  int stereoBlobId;
  StereoImageBlob stereoBlob;

  // Current road line blob data
  bool lineEnabled;
  int lineBlobId;
  RoadLineBlob lineBlob;

  // Prior map data
  PriorMap *priorMap;
  
  // Local map data
  LocalMap *localMap;

  // Ladar road finder
  LadarRoadFinder *ladarRoadFinder;

  // Stereo road finder
  StereoRoadFinder *stereoRoadFinder;

  // Transform local to planner frame
  float transSL[4][4], transLS[4][4];

  // Transform from local to map image frame (i.e., pixel coordinates)
  float transIL[4][4];

  public:

  // Frame counter
  int frameCount;

  // Canonical fused line image
  dgc_image_t *lineImage;
  dgc_image_t *lineDistImage;

  private:
  
  // Dimensions of the test rectangle in vehicle frame (ax, ay) to (bx, by)
  float rectAx, rectAy, rectBx, rectBy;
  
  // Number of rotated line images
  int numLineImages;
    
  // Planner to image transform for each orientation bin
  float transforms[32][6];

  // Rotated line images (for c-space calculations)
  dgc_image_t *lineRotImages[32];
  dgc_image_t *lineSumImages[32];
  
#if USE_GL

  public:
  
  /// @brief Draw the costs for configurations in the prior map (out to some distance).
  int predrawROI(float size);

  // This function is applied recursively to the quad-tree.
  int predrawROIQuad(PriorMapQuad *quad, float px, float py, float size);

  private:
  
  // GL display list
  int roiList;

#endif

};


#endif

