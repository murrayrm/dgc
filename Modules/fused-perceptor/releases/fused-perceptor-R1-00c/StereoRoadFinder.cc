
/* 
 * Desc: Road/line finding
 * Date: 16 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>

#include "StereoRoadFinder.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
StereoRoadFinder::StereoRoadFinder(LocalMap *localMap)
{  
  // Initialize diagnostic images
  this->rawImage = dgc_image_alloc(localMap->size, localMap->size, 3, 8, 0, NULL);
  this->lineImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);
  
  return;
}


// Destructor
StereoRoadFinder::~StereoRoadFinder()
{
  dgc_image_free(this->lineImage);
  this->lineImage = NULL;
  dgc_image_free(this->rawImage);
  this->rawImage = NULL;

  return;
}


// Project stereo raw data into map
int StereoRoadFinder::projectStereoRaw(LocalMap *localMap, StereoImageBlob *blob)
{
  int i, j;
  float d;
  uint8_t *cpix;
  uint16_t *dpix;
  float m[4][4];
  float sx, sy, sz;
  float vx, vy, vz;
  float mx, my, mz;
  LocalMapCell *cell;

  assert(blob->channels == 3);
    
  // Compute transform from sensor to vehicle to local to map frame
  mat44f_mul(m, blob->veh2loc, blob->leftCamera.sens2veh);
  mat44f_mul(m, localMap->transML, m);
  
  for (j = 0; j < blob->rows; j++)
  {
    cpix = StereoImageBlobGetLeft(blob, 0, j);
    dpix = StereoImageBlobGetDisp(blob, 0, j);
    
    for (i = 0; i < blob->cols; i++, cpix += 3, dpix += 1)
    {
      if (dpix[0] == 0 || dpix[0] > 0x7000)
        continue;

      // Compute true disparity
      d = (float) dpix[0] / blob->dispScale;

      // Convert to sensor frame
      StereoImageBlobImageToSensor(blob, i, j, d, &sx, &sy, &sz);

      // Convert to vehicle frame
      StereoImageBlobSensorToVehicle(blob, sx, sy, sz, &vx, &vy, &vz);

      // Limit points to those nearby and below the vehicle altitude
      if (sz > 15.0) // MAGIC
        continue;
      if (vz < -1.0) // MAGIC
        continue;

      // Convert to map frame
      mx = m[0][0]*sx + m[0][1]*sy + m[0][2]*sz + m[0][3];
      my = m[1][0]*sx + m[1][1]*sy + m[1][2]*sz + m[1][3];
      mz = m[2][0]*sx + m[2][1]*sy + m[2][2]*sz + m[2][3];

      // Get the cell at this location
      cell = localMap->getCell(mx, my);
      if (!cell)
        continue;

      // Update the cell
      cell->stereoRawCount += 1;
      cell->stereoRaw[0] += (unsigned int) cpix[0];
      cell->stereoRaw[1] += (unsigned int) cpix[1];
      cell->stereoRaw[2] += (unsigned int) cpix[2];
    }
  }
  
  return 0;
}


// Project line data from road perceptors.
int StereoRoadFinder::projectRoadLine(LocalMap *localMap, const RoadLineBlob *blob)
{
  int i, j;
  float dx, dy, dd;
  int stepCount;
  float stepSize;
  float m[4][4];
  float px, py, mx, my;
  LocalMapCell *cell;
  const RoadLine *line;

  // Compute transform from local to map frame
  mat44f_setf(m, localMap->transML);
    
  for (i = 0; i < blob->numLines; i++)
  {
    line = blob->lines + i;

    // Line lengths
    dx = line->bx - line->ax;
    dy = line->by - line->ay;
    dd = sqrtf(dx*dx + dy*dy);

    // Step size; we oversample by a factor
    stepSize = localMap->scale / 1.4;
    stepCount = (int) (dd / stepSize);
    stepSize = 1.0 / stepCount;

    // Walk the line
    for (j = 0; j < stepCount; j++)
    {
      px = line->ax + j*dx*stepSize;
      py = line->ay + j*dy*stepSize;

      // Convert to map frame
      mx = m[0][0]*px + m[0][1]*py + m[0][3];
      my = m[1][0]*px + m[1][1]*py + m[1][3];

      // Get the cell at this location
      cell = localMap->getCell(mx, my);
      if (!cell)
        continue;

      // Update the cell
      cell->stereoLineCount += 1;
    }
  }
  return 0;
}


// Update the raw map image
dgc_image_t *StereoRoadFinder::updateRawImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;

  pix = (uint8_t*) dgc_image_pixel(this->rawImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);

      if (cell->stereoRawCount > 0)
      {
        pix[0] = (uint8_t) (cell->stereoRaw[0] / cell->stereoRawCount);
        pix[1] = (uint8_t) (cell->stereoRaw[1] / cell->stereoRawCount);
        pix[2] = (uint8_t) (cell->stereoRaw[2] / cell->stereoRawCount);
      }
      else
      {
        pix[0] = 0x80;
        pix[1] = 0x80;
        pix[2] = 0x80;        
      }
      
      pix += 3;
    }
  }
  
  return this->rawImage;
}


// Update the line map image
dgc_image_t *StereoRoadFinder::updateLineImage(LocalMap *localMap)
{
  int i, j, k;
  LocalMapCell *cell;
  uint8_t *pix;

  pix = (uint8_t*) dgc_image_pixel(this->lineImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);

      if (cell->stereoLineCount > 0) // MAGIC
      {
        k = cell->stereoLineCount * 8;
        if (k > 0xFF)
          k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->lineImage;
}
