
/* 
 * Desc: Road/line finding
 * Date: 16 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef STEREO_ROAD_FINDER_H
#define STEREO_ROAD_FINDER_H

/** @file

@brief Road and road line finding.

*/

// Dependencies
#include <interfaces/StereoImageBlob.h>
#include <interfaces/RoadLineBlob.h>
#include <dgcutils/dgc_image.h>
#include "LocalMap.hh"


/// @brief Process sensor (stereo) data to local roads
class StereoRoadFinder
{
  public:

  // Constructor
  StereoRoadFinder(LocalMap *localMap);

  // Destructor
  virtual ~StereoRoadFinder();

  private:

  // Hide the copy constructor
  StereoRoadFinder(const StereoRoadFinder &that);

  public:
  
  // Project stereo raw data into the map
  int projectStereoRaw(LocalMap *localMap, StereoImageBlob *blob);

  // Project stereo line data into the map
  int projectRoadLine(LocalMap *localMap, const RoadLineBlob *blob);

  public:

  // Update the diagnostic raw image
  dgc_image_t *updateRawImage(LocalMap *localMap);

  // Update the diagnostic line image
  dgc_image_t *updateLineImage(LocalMap *localMap);

  public:
  
  // Diagnostic images
  dgc_image_t *rawImage, *lineImage;
};

#endif

