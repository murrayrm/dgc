
/* 
 * Desc: Local cartesian map
 * Date: 16 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <frames/pose3.h>
#include <frames/mat44.h>

#include "LocalMap.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Allocate new object
LocalMap::LocalMap(int size, float scale)
{
  // Make sure the size is even.
  this->size = size + (size % 2);
  this->scale = scale;  
  this->cells = (LocalMapCell*) calloc(this->size * this->size, sizeof(this->cells[0]));
 
  return;
}


// Destructor
LocalMap::~LocalMap()
{
  free(this->cells);
  return;
}


// Clear the map
int LocalMap::clear(const VehicleState *state)
{
  pose3_t pose;
    
  // Construct map pose from the local pose
  pose.pos = vec3_set(state->localX, state->localY, state->localZ);
  pose.rot = quat_ident();
  this->pose = pose;

  // Construct local<>map transforms
  pose3_to_mat44f(pose, this->transLM);
  mat44f_inv(this->transML, this->transLM);

  // Clear the obstacle map
  memset(this->cells, 0, this->size * this->size * sizeof(this->cells[0]));
  
  return 0;
}


// Move the map center but keep the data
int LocalMap::move(const VehicleState *state)
{
  pose3_t pose, dpose;
  int mi, mj, si, sj, di, dj;
  LocalMapCell *src, *dst;

  // Construct local pose
  pose.pos = vec3_set(state->localX, state->localY, state->localZ);
  pose.rot = quat_ident();

  // Compute the number of cells to move
  dpose = pose3_mul(pose3_inv(this->pose), pose);
  mi = (int) (dpose.pos.x / this->scale);
  mj = (int) (dpose.pos.y / this->scale);

  // Dont do anything unless we have moved a significant distance
  if (abs(mi) < 2 && abs(mj) < 2) // MAGIC 
    return 0;
  
  // Compute the new map pose
  dpose.pos.x = mi * this->scale;
  dpose.pos.y = mj * this->scale;
  dpose.rot = quat_ident();
  this->pose = pose3_mul(this->pose, dpose);

  // Construct local<->map transforms
  pose3_to_mat44f(this->pose, this->transLM);
  mat44f_inv(this->transML, this->transLM);

  if (mi >= 0 && mj >= 0)
  {
    for (dj = 0; dj < this->size; dj++)
    {
      sj = dj + mj;            
      for (di = 0; di < this->size; di++)
      {
        si = di + mi;
        src = this->getCellByIndex(si, sj);
        dst = this->getCellByIndex(di, dj);
        if (src && dst)
          memcpy(dst, src, sizeof(*dst));
        else
          memset(dst, 0, sizeof(*dst));
      }
    }    
  }
  else if (mi >= 0 && mj < 0)
  {
    for (dj = this->size - 1; dj >= 0; dj--)
    {
      sj = dj + mj;            
      for (di = 0; di < this->size; di++)
      {
        si = di + mi;
        src = this->getCellByIndex(si, sj);
        dst = this->getCellByIndex(di, dj);
        if (src && dst)
          memcpy(dst, src, sizeof(*dst));
        else
          memset(dst, 0, sizeof(*dst));
      }
    }    
  }
  else if (mi < 0 && mj >= 0)
  {
    for (dj = 0; dj < this->size; dj++)
    {
      sj = dj + mj;            
      for (di = this->size - 1; di >= 0; di--)
      {
        si = di + mi;
        src = this->getCellByIndex(si, sj);
        dst = this->getCellByIndex(di, dj);
        if (src && dst)
          memcpy(dst, src, sizeof(*dst));
        else
          memset(dst, 0, sizeof(*dst));
      }
    }    
  }
  else if (mi < 0 && mj < 0)
  {
    for (dj = this->size - 1; dj >= 0; dj--)
    {
      sj = dj + mj;            
      for (di = this->size - 1; di >= 0; di--)
      {
        si = di + mi;
        src = this->getCellByIndex(si, sj);
        dst = this->getCellByIndex(di, dj);
        if (src && dst)
          memcpy(dst, src, sizeof(*dst));
        else
          memset(dst, 0, sizeof(*dst));
      }
    }    
  }
  else
  {
    assert(false);
  }
  
  return 0;
}

