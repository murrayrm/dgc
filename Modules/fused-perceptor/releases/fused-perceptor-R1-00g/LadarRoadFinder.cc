
/* 
 * Desc: Road/line finding with Riegl ladar
 * Date: 16 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <frames/mat44.h>
#include "LadarRoadFinder.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
LadarRoadFinder::LadarRoadFinder(LocalMap *localMap)
{
  // Initialize workspace images
  this->workImage = dgc_image_alloc(localMap->size, localMap->size, 1, 16, 0, NULL);
    
  // Initialize diagnostic images
  this->rawImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);
  this->edgeImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);
  this->lineImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);
  this->roughImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);  

  // Magic numbers
  this->lineMinSigma = 1.5;
  this->lineWindow = 5;
  this->lineMinCount = 5;

  return;
}


// Destructor
LadarRoadFinder::~LadarRoadFinder()
{  
  dgc_image_free(this->roughImage);
  dgc_image_free(this->lineImage);
  dgc_image_free(this->edgeImage);
  dgc_image_free(this->rawImage);
  dgc_image_free(this->workImage);
  
  return;
}


// Project ladar data into map
int LadarRoadFinder::projectLadarRaw(LocalMap *localMap, LadarRangeBlob *blob)
{
  int i;
  float pa, pr;
  float sx, sy, sz;
  float vx, vy, vz;
  float mx, my, mz;
  int mi, mj;
  uint8_t raw;
  float m[4][4];
  LocalMapCell *cell;
  float rangeValid[LADAR_BLOB_MAX_POINTS];
  float rangeEdges[LADAR_BLOB_MAX_POINTS];
  float rawEdges[LADAR_BLOB_MAX_POINTS];
  
  // Run an edge detector on the range data
  for (i = 0; i < blob->numPoints; i++)
  {
    rangeValid[i] = blob->points[i][1];
    rangeEdges[i] = blob->points[i][1];
  }
  this->calcInvalid(150, 3 * 5, blob->numPoints, rangeValid); // MAGIC
  this->calcDoG(0.3, 5, 15, blob->numPoints, rangeEdges);   // MAGIC

  // Run an edge detector on the raw intensity data
  for (i = 0; i < blob->numPoints; i++)
  {
    pr = blob->points[i][1];
    raw = blob->intensities[i];
    if (pr < 0 || pr > 80.0) // MAGIC
      rawEdges[i] = -1;
    else
      rawEdges[i] = raw;
  }  
  this->calcVar(0, 9, blob->numPoints, rawEdges); // MAGIC

  /*
  // TESTING
  for (i = 0; i < blob->numPoints; i++)
  {
  printf("%d %f %d %d\n", i,
  blob->points[i][1], blob->intensities[i], sum[i]);
  } 
  printf("\n\n");
  fflush(stdout);
  */

  // Compute transform from sensor to vehicle to local to map frame
  mat44f_mul(m, blob->veh2loc, blob->sens2veh);
  mat44f_mul(m, localMap->transML, m);
    
  for (i = 0; i < blob->numPoints; i++)
  {
    pa = blob->points[i][0];
    pr = blob->points[i][1];
    raw = blob->intensities[i];
    
    if (pr < 0 || pr > 80.0) // MAGIC
      continue;

    // Compute points in sensor frame
    LadarRangeBlobScanToSensor(blob, pa, pr, &sx, &sy, &sz);

    // Convert to vehicle frame
    LadarRangeBlobSensorToVehicle(blob, sx, sy, sz, &vx, &vy, &vz);
    
    // Discard points that are higher in the vehicle frame; these
    // probably dont come from the ground.
    if (vz < -0.5) // MAGIC
      continue;
    
    // Convert to map frame
    mx = m[0][0]*sx + m[0][1]*sy + m[0][2]*sz + m[0][3];
    my = m[1][0]*sx + m[1][1]*sy + m[1][2]*sz + m[1][3];
    mz = m[2][0]*sx + m[2][1]*sy + m[2][2]*sz + m[2][3];
        
    // Get the cell at this location
    localMap->cvtPosToIndex(mx, my, &mi, &mj);
    cell = localMap->getCellByIndex(mi, mj);
    if (!cell)
      continue;
    
    // Update the cell intensity values.
    cell->ladarRawCount += 1;
    cell->ladarRaw1 += raw;
    cell->ladarRaw2 += raw * raw;

    // Update line edge detector
    cell->ladarEdgeCount += 1;
    cell->ladarEdge += rawEdges[i];
    
    // Update the cell roughness in regions where the ladar is valid.
    if (rangeValid[i])
    {
      cell->ladarRoughCount += 1;
      cell->ladarRough += rangeEdges[i];
    }
  }
  
  return 0;
}


// Compute an expanded notion of invalid readings
int LadarRoadFinder::calcInvalid(float maxValid, int window, int numPoints, float *points)
{
  int i, di;
  float filt[numPoints];

  for (i = 0; i < window; i++)
    filt[i] = 0;
  for (; i < numPoints - window; i++)
  {
    filt[i] = 1;
    for (di = -window; di <= +window; di++)
    {
      if (points[i + di] > maxValid)
      {
        filt[i] = 0;
        break;
      }
    }
  }
  for (; i < numPoints; i++)
    filt[i] = 0;
  
  // Overwrite in input signal
  for (i = 0; i < numPoints; i++)
    points[i] = filt[i];
    
  return 0;
}


// Compute the difference of gaussians on a signal
int LadarRoadFinder::calcDoG(float inner, float outer, int win, int numPoints, float *points)
{
  int i, di;
  int numInner, numOuter;
  float norm;
  float kernInner[64], kernOuter[64];
  float filtInner[numPoints], filtOuter[numPoints];

  numInner = (win - 1) / 2;
  numOuter = (win - 1) / 2;
  assert(numOuter >= numInner);

  // Construct convolution kernels
  norm = 0;
  for (di = -numInner; di <= +numInner; di++)
    norm += (float) exp(-di*di/(inner*inner)/2);
  assert((size_t) (2 * numInner + 1) < sizeof(kernInner)/sizeof(kernInner[0]));
  for (di = -numInner; di <= +numInner; di++)
    kernInner[di + numInner] = (float) exp(-di*di/(inner*inner)/2) / norm;

  norm = 0;
  for (di = -numOuter; di <= +numOuter; di++)
    norm += (float) exp(-di*di/(outer*outer)/2);
  assert((size_t) (2 * numOuter + 1) < sizeof(kernOuter)/sizeof(kernOuter[0]));
  for (di = -numOuter; di <= +numOuter; di++)
    kernOuter[di + numOuter] = (float) exp(-di*di/(outer*outer)/2) / norm;

  // Compute inner gaussian filter.  Note that we use the radius of
  // the outer filter when discarding edges; this allows us to safely
  // take the difference of inner and outer without creating
  // artifacts.
  for (i = 0; i < numOuter; i++)
    filtInner[i] = 0;
  for (; i < numPoints - numOuter; i++)
  {
    filtInner[i] = 0;
    for (di = -numInner; di <= +numInner; di++)
      filtInner[i] += points[di + i] * kernInner[di + numInner];
  }
  for (; i < numPoints; i++)
    filtInner[i] = 0;

  // Compute outer gaussian filter
  for (i = 0; i < numOuter; i++)
    filtOuter[i] = 0;
  for (; i < numPoints - numOuter; i++)
  {
    filtOuter[i] = 0;
    for (di = -numOuter; di <= +numOuter; di++)
      filtOuter[i] += points[di + i] * kernOuter[di + numOuter];
  }
  for (; i < numPoints; i++)
    filtOuter[i] = 0;

  // Compute difference of gaussians and overwrite in input signal
  for (i = 0; i < numPoints; i++)
    points[i] = filtInner[i] - filtOuter[i];
  
  return 0;
}


// Look for statistically significant variations in a window 
int LadarRoadFinder::calcVar(float thresh, int win, int numPoints, float *points)
{
  int i, di;
  float m0, m1, m2;
  float filt[numPoints];

  win = (win - 1) / 2;

  for (i = 0; i < win; i++)
    filt[i] = 0;
  for (i = win; i < numPoints - win; i++)
  {
    // Compute mean and variance in this window (excluding the center pixel)
    m0 = m1 = m2 = 0;
    for (di = -win; di <= +win; di++)
    {
      if (di == 0)
        continue;
      if (points[i + di] < 0)
        continue;
      m0 += 1;
      m1 += points[i + di];
      m2 += points[i + di] * points[i + di];
    }

    m1 = m1/m0;
    m2 = m2/m0 - m1*m1;

    if (points[i] >= 0)
      filt[i] = (points[i] - m1)/sqrtf(m2);
    else
      filt[i] = 0;
  }
  for (; i < numPoints; i++)
    filt[i] = 0;
  
  memcpy(points, filt, numPoints * sizeof(points[0]));
  
  return 0;
}


// Scan the local map and update the detected lines
int LadarRoadFinder::updateLines(LocalMap *localMap)
{
  int mi, mj, di, dj, count;  
  LocalMapCell *cell, *ncell;
  float minSigma;
  int minCount, w;

  minSigma = this->lineMinSigma;
  minCount = this->lineMinCount;
    
  // Size of the local line filter
  w = (this->lineWindow - 1)/2;

  // First pass; count good edges in the hood.  We use the line field
  // in the cell as a bitmask, i.e., binary 01 = survived the first pass.
  cell = localMap->getCellByIndex(0, 0);
  for (mj = 0; mj < localMap->size; mj++)
  {
    for (mi = 0; mi < localMap->size; mi++, cell++)
    {
      cell->ladarLine = 0;      
      if (cell->ladarEdgeCount == 0)
        continue;
      if (cell->ladarEdge/cell->ladarEdgeCount < minSigma)
        continue;

      // Count good cells in the neighborhood
      count = 0;
      for (dj = -w; dj <= +w; dj++)
      {
        for (di = -w; di <= +w; di++)
        {
          ncell = localMap->getCellByIndex(mi + di, mj + dj);
          if (!ncell)
            continue;
          if (ncell->ladarEdgeCount == 0)
            continue;
          if (ncell->ladarEdge/ncell->ladarEdgeCount < minSigma)
            continue;
          count += 1;
        }
      }
      
      if (count >= minCount)
        cell->ladarLine = 1;
    }
  }

  // Second pass on the line data to remove remaining singleton cells.
  // The line field in the cell is set to 3 (binary 11) to indicate
  // that both tests where passed.
  cell = localMap->getCellByIndex(0, 0);
  for (mj = 0; mj < localMap->size; mj++)
  {
    for (mi = 0; mi < localMap->size; mi++, cell++)
    {
      if (cell->ladarLine == 0)
        continue;
      
      // Count good cells in the neighborhood
      count = 0;
      for (dj = -1; dj <= +1; dj++)
      {
        for (di = -1; di <= +1; di++)
        {
          ncell = localMap->getCellByIndex(mi + di, mj + dj);
          if (!ncell)
            continue;
          if (ncell->ladarLine == 0)
            continue;
          count += 1;
        }
      }
      
      if (count > 1)
        cell->ladarLine = 3;
    }
  }
 
  return 0;
}


// Compute the number of edges in a window 
int LadarRoadFinder::countLocalEdges(LocalMap *localMap, int mi, int mj, float thresh)
{
  int di, dj;
  int w, m0;
  LocalMapCell *cell;

  cell = localMap->getCellByIndex(mi, mj);
  if (!cell)
    return 0;

  // Does the center cell have a decent edge?
  // If not, this cannot be a line cell.
  if (cell->ladarEdge/cell->ladarEdgeCount < thresh)
    return 0;
  
  // Size of the local line filter
  w = (this->lineWindow - 1)/2;

  // Compute the number of non-zero neighbors
  m0 = 0;
  for (dj = -w; dj <= +w; dj++)
  {
    for (di = -w; di <= +w; di++)
    {
      cell = localMap->getCellByIndex(mi + di, mj + dj);
      if (!cell)
        continue;
      if (cell->ladarEdgeCount == 0)
        continue;
      if (cell->ladarEdge/cell->ladarEdgeCount >= thresh)
        m0 += 1;
    }
  }

  return m0;
}


// Calculate the edge response in a local region of the map
float LadarRoadFinder::calcLocalEdge(LocalMap *localMap, int mi, int mj)
{
  int w;
  int k, di, dj;
  float edge, maxEdge;
  float m0, m1, m2;
  float n0, n1, n2;
  LocalMapCell *cell;
  int *mask;
  
  int kernels[][25] =
    {
      {0, 0, 0, 0, 0,
       0, 0, 0, 0, 0,
       1, 1, 1, 1, 1,
       0, 0, 0, 0, 0,
       0, 0, 0, 0, 0},
      {0, 0, 1, 0, 0,
       0, 0, 1, 0, 0,
       0, 0, 1, 0, 0,
       0, 0, 1, 0, 0,
       0, 0, 1, 0, 0},
      {1, 0, 0, 0, 0,
       0, 1, 0, 0, 0,
       0, 0, 1, 0, 0,
       0, 0, 0, 1, 0,
       0, 0, 0, 0, 1},
      {0, 0, 0, 0, 1,
       0, 0, 0, 1, 0,
       0, 0, 1, 0, 0,
       0, 1, 0, 0, 0,
       1, 0, 0, 0, 0},
    };
  
  w = (5 - 1)/2;

  maxEdge = 0;
  for (k = 0; k < 4; k++)
  { 
    // Calculate stats over a local window
    mask = kernels[k];
    m0 = m1 = m2 = 0;
    n0 = n1 = n2 = 0;
    for (dj = -w; dj <= +w; dj++)
    {
      for (di = -w; di <= +w; di++, mask++)
      {
        cell = localMap->getCellByIndex(mi + di, mj + dj);
        if (!cell)
          continue;      
        if (mask[0] == 0)
        {
          m0 += cell->ladarRawCount;
          m1 += cell->ladarRaw1;
          m2 += cell->ladarRaw2;
        }
        else 
        {
          n0 += cell->ladarRawCount;
          n1 += cell->ladarRaw1;
          n2 += cell->ladarRaw2;
        }
      }
    }

    if (m0 < 9) // MAGIC
      continue;
    if (n0 < 5) // MAGIC
      continue;

    // Compute the stats in the outer region
    m1 = m1/m0;
    m2 = m2/m0 - m1*m1;

    // Compute the stats in the inner region
    n1 = n1/n0;
    n2 = n2/n0 - n1*n1;
    
    // Scaled relative brightness of the inner region
    edge = (n1 - m1) / sqrtf(n2 + m2);
    
    if (edge > maxEdge)
      maxEdge = edge;
  }

  // TESTING
  if (maxEdge < 1.0)
    maxEdge = 0;
  else
    maxEdge = 3.0;
  
  return maxEdge;
}


// Update the intensity image
dgc_image_t *LadarRoadFinder::updateRawImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;
  int k;

  pix = (uint8_t*) dgc_image_pixel(this->rawImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);
      if (cell->ladarRawCount > 0)
      {
        k = 2 * cell->ladarRaw1 / cell->ladarRawCount; // MAGIC
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->rawImage;
}


// Update the edge image
dgc_image_t *LadarRoadFinder::updateEdgeImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;
  float k;
        
  pix = (uint8_t*) dgc_image_pixel(this->edgeImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);
      if (cell->ladarEdgeCount > 0)
      {
        k = 0xFF * cell->ladarEdge / cell->ladarEdgeCount / 6.0; // MAGIC
        if (k < 0) k = 0;
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->edgeImage;
}


// Update the line image
dgc_image_t *LadarRoadFinder::updateLineImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;
  float k;
        
  pix = (uint8_t*) dgc_image_pixel(this->lineImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);
      if (cell->ladarLine > 0)
      {
        k = 0xFF * cell->ladarLine / 3;
        if (k < 0) k = 0;
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->lineImage;
}


// Update the rough image
dgc_image_t *LadarRoadFinder::updateRoughImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;
  float k;
        
  pix = (uint8_t*) dgc_image_pixel(this->roughImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);
      if (cell->ladarRoughCount > 0)
      {
        k = 0x80 - cell->ladarRough / cell->ladarRoughCount * 500; // MAGIC
        if (k < 0) k = 0;
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->roughImage;
}
