
/* 
 * Desc: Road/line finding with Riegl ladar
 * Date: 16 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef LADAR_ROAD_FINDER_H
#define LADAR_ROAD_FINDER_H

/** @file

@brief Road and line finding with the Riegl ladar.

*/

// Dependencies
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/StereoImageBlob.h>
#include <dgcutils/dgc_image.h>

#include "LocalMap.hh"


/// @brief Process ladar data to detect roads and lines.
class LadarRoadFinder
{
  public:

  // Constructor
  LadarRoadFinder(LocalMap *localMap);

  // Destructor
  virtual ~LadarRoadFinder();

  private:

  // Hide the copy constructor
  LadarRoadFinder(const LadarRoadFinder &that);

  public:

  // Project ladar data into map
  int projectLadar(LocalMap *localMap, LadarRangeBlob *blob);

  // Look for lines in the local map
  int findLines(LocalMap *localMap);

  private:

  // Compute an expanded notion of invalid readings
  int calcInvalid(float maxValid, int window, int numPoints, float *points);

  // Compute the difference of gaussians on a signal
  int calcDoG(float inner, float outer, int numPoints, float *points);

  // Run a filter that looks for outliers based on local variance.
  int findLineEdges(LocalMap *localMap, dgc_image_t *edgeImage, int minValue);

  // Remove line noise, such as single pixels.
  int filterLineNoise(LocalMap *localMap, dgc_image_t *edgeImage, int minCount);
  
  public:

  // Update the ladar intensity map image
  dgc_image_t *updateMonoImage(LocalMap *localMap);

  // Update the edge image
  dgc_image_t *updateEdgeImage(LocalMap *localMap);

  // Update the ladar line map image
  dgc_image_t *updateLineImage(LocalMap *localMap);

  // Update the ladar flatness map image
  dgc_image_t *updateFlatImage(LocalMap *localMap);

  public:

  // Worspace/diagnostic images
  dgc_image_t *monoImage, *edgeImage, *lineImage, *flatImage;
};

#endif

