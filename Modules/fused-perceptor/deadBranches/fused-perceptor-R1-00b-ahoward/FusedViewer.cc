
/* 
 * Desc: Fused perceptor viewer utility
 * Date: 21 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>

#include <GL/glut.h>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <dgcutils/Fl_Glv_Window.H>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/RoadLineBlob.h>

#include "cmdline.h"

#include "FusedPerceptor.hh"


class App
{
  public:

  // Default constructor
  App();

  // Destructor
  ~App();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  // Initialize GUI
  int initGUI(int cols, int rows);

  // Finalize GUI
  int finiGUI();
 
  public:

  // Action callback
  static void onAction(Fl_Widget *w, int option);

  // Speed callback
  static void onSpeed(Fl_Widget *w, int option);
  
  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  // Handle draw callbacks
  static void onDraw(Fl_Glv_Window *win, App *self);

  // Handle idle callbacks
  static void onIdle(App *self);

  public:

  // Switch to the vehicle frame
  void pushFrameVehicle(const VehicleState *state);

  // Switch to the local frame
  void pushFrameLocal(const VehicleState *state, pose3_t mapPose);

  // Switch to the global frame
  void pushFrameGlobal(const VehicleState *state, pose3_t mapPose);

  // Revert to previous frame
  void popFrame();

  // Draw a set of axes
  void drawAxes(float size);

  // Draw the robot
  void drawAlice();
  
  public:

  // Initialize the perceptor
  int init();

  // Finalize the perceptor
  int fini();

  // Update the perceptor
  int update();

  // Pre-draw the local map using an image
  void predrawLocalMap(LocalMap *map, const VehicleState *state, dgc_image_t *image);

  public:

  // Command-line options
  struct gengetopt_args_info options;

  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;
  
  // 3D window
  Fl_Glv_Window *worldwin;

  // Should we pause?
  bool pause;

  // Should we advance a single step?
  bool step;
  
  // Should we quit?
  bool quit;

  public:

  // Fused perceptor
  FusedPerceptor *fused;
  
  // Grid display list (local frame)
  GLuint gridTex, gridList;

  // Local frame display list
  GLuint localList;

  // Global frame display list
  GLuint globalList;

  // Are the display lists dirty?
  bool dirty;

  // When we last set the dirty flag?
  uint64_t dirtyTimestamp;
  
  // Which global map layer are we viewing?
  int globalLayer;

  // Which local map layer are we viewing?
  int localLayer;

  // Which grid layer are we viewing?
  int gridLayer;
};



// Commands
enum
{
  APP_ACTION_PAUSE = 0x1000,
  APP_ACTION_STEP,
  
  APP_VIEW_GLOBAL_FIRST,
  APP_VIEW_GLOBAL_NONE,
  APP_VIEW_GLOBAL_RNDF,
  APP_VIEW_GLOBAL_DENSE,
  APP_VIEW_GLOBAL_ROI,
  APP_VIEW_GLOBAL_LAST,

  APP_VIEW_LOCAL_FIRST,
  APP_VIEW_LOCAL_NONE,
  APP_VIEW_LOCAL_LAST,

  APP_VIEW_GRID_FIRST,
  APP_VIEW_GRID_NONE,
  APP_VIEW_GRID_LADAR_RAW,
  APP_VIEW_GRID_LADAR_LINE,
  APP_VIEW_GRID_LADAR_ROUGH,
  APP_VIEW_GRID_STEREO_RAW,
  APP_VIEW_GRID_STEREO_LINE,
  APP_VIEW_GRID_FUSED_LINE,
  APP_VIEW_GRID_FUSED_LINE_DIST,
  APP_VIEW_GRID_LAST,
};


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
App::App()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
App::~App()
{
  return;
}


// Parse command-line options
int App::parseCmdLine(int argc, char **argv)
{
  // Run parser
  if (cmdline_parser(argc, argv, &this->options) != 0)
  {
    cmdline_parser_print_help();
    return -1;
  }

  return 0;
}


// Initialize stuff
int App::initGUI(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) App::onExit},
      {0},
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) App::onAction, (void*) APP_ACTION_PAUSE, FL_MENU_TOGGLE},
      {"Single step", '.', (Fl_Callback*) App::onAction, (void*) APP_ACTION_STEP},
      {0},
      {"&View", 0, 0, 0, FL_SUBMENU},

      {"None", 0, (Fl_Callback*) App::onAction,
       (void*) APP_VIEW_GLOBAL_NONE, FL_MENU_RADIO},
      {"Prior RNDF", 'r', (Fl_Callback*) App::onAction,
       (void*) APP_VIEW_GLOBAL_RNDF, FL_MENU_RADIO},
      {"Prior dense", 'd', (Fl_Callback*) App::onAction,
       (void*) APP_VIEW_GLOBAL_DENSE, FL_MENU_RADIO},
      {"Fused ROI", 'f', (Fl_Callback*) App::onAction,
       (void*) APP_VIEW_GLOBAL_ROI, FL_MENU_RADIO | FL_MENU_VALUE | FL_MENU_DIVIDER},
      
      /* TODO
      {"No fused map", 0, (Fl_Callback*) App::onAction,
       (void*) APP_VIEW_FUSED_NONE, FL_MENU_RADIO},
      {"Default fused map", 'f', (Fl_Callback*) App::onAction,
       (void*) APP_VIEW_FUSED_DEFAULT, FL_MENU_RADIO | FL_MENU_VALUE | FL_MENU_DIVIDER},
      */

      {"None", '0', (Fl_Callback*) App::onAction, (void*) APP_VIEW_GRID_NONE,
       FL_MENU_RADIO},
      {"Ladar raw", '1', (Fl_Callback*) App::onAction, (void*) APP_VIEW_GRID_LADAR_RAW,
       FL_MENU_RADIO | FL_MENU_VALUE},
      {"Ladar line", '2', (Fl_Callback*) App::onAction, (void*) APP_VIEW_GRID_LADAR_LINE,
       FL_MENU_RADIO},
      {"Ladar flat", '3', (Fl_Callback*) App::onAction, (void*) APP_VIEW_GRID_LADAR_ROUGH,
       FL_MENU_RADIO},
      {"Stereo RAW", '4', (Fl_Callback*) App::onAction, (void*) APP_VIEW_GRID_STEREO_RAW,
       FL_MENU_RADIO},
      {"Stereo line", '5', (Fl_Callback*) App::onAction, (void*) APP_VIEW_GRID_STEREO_LINE,
       FL_MENU_RADIO | FL_MENU_DIVIDER},
      {"Fused line", '6', (Fl_Callback*) App::onAction, (void*) APP_VIEW_GRID_FUSED_LINE,
       FL_MENU_RADIO},       
      {"Fused line dist", '7', (Fl_Callback*) App::onAction, (void*) APP_VIEW_GRID_FUSED_LINE_DIST,
       FL_MENU_RADIO | FL_MENU_DIVIDER},
      {0},
      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Fused Perceptor Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new Fl_Glv_Window(0, 30, cols, rows - 30, this, (Fl_Callback*) onDraw);

  this->mainwin->end();

  this->worldwin->set_hfov(40.0);
  this->worldwin->set_clip(4, 1000);
  this->worldwin->set_lookat(-10, -10, -10, 0, 0, 0, 0, 0, -1);

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  // Set consistent menu state
  this->globalLayer = APP_VIEW_GLOBAL_ROI;
  this->localLayer = APP_VIEW_LOCAL_NONE;
  this->gridLayer = APP_VIEW_GRID_LADAR_RAW;
  
  return 0;
}


// Finalize stuff
int App::finiGUI()
{  
  return 0;
}


// Handle menu callbacks
void App::onAction(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data();
  if (option == APP_ACTION_PAUSE)
    self->pause = !self->pause;
  if (option == APP_ACTION_STEP)
    self->step = true;
  
  // Change the selected grid map layer
  if (option > APP_VIEW_GRID_FIRST && option < APP_VIEW_GRID_LAST)
  {
    self->gridLayer = option;
    self->dirty = true;
    self->worldwin->redraw();
  }

  // Change the selected local map layer
  if (option > APP_VIEW_LOCAL_FIRST && option < APP_VIEW_LOCAL_LAST)
  {
    self->localLayer = option;
    self->dirty = true;
    self->worldwin->redraw();
  }

  // Change the selected global map layer
  if (option > APP_VIEW_GLOBAL_FIRST && option < APP_VIEW_GLOBAL_LAST)
  {
    self->globalLayer = option;
    self->dirty = true;
    self->worldwin->redraw();
  }

  return;
}



// Handle menu callbacks
void App::onExit(Fl_Widget *w, int option)
{
  App *self;

  self = (App*) w->user_data();
  self->quit = true;

  return;
}


// Handle draw callbacks
void App::onDraw(Fl_Glv_Window *win, App *self)
{
  uint64_t time;
  
  // Predraw if necessary
  if (self->dirty)
  {
    dgc_image_t *image;

    time = DGCgettime();
    
    // Predraw the vector layer
    if (self->globalLayer == APP_VIEW_GLOBAL_RNDF)
      self->globalList = self->fused->priorMap->predrawRndf();
    if (self->globalLayer == APP_VIEW_GLOBAL_DENSE)
      self->globalList = self->fused->priorMap->predrawMap();
    if (self->globalLayer == APP_VIEW_GLOBAL_ROI)
      self->globalList = self->fused->predrawROI(35);

    image = NULL;
          
    // Predraw ladar finder outputs
    if (self->gridLayer == APP_VIEW_GRID_LADAR_RAW)
      image = self->fused->ladarRoadFinder->updateRawImage(self->fused->localMap);
    else if (self->gridLayer == APP_VIEW_GRID_LADAR_LINE)
      image = self->fused->ladarRoadFinder->updateLineImage(self->fused->localMap);
    else if (self->gridLayer == APP_VIEW_GRID_LADAR_ROUGH)
      image = self->fused->ladarRoadFinder->updateRoughImage(self->fused->localMap);

    // Predraw stereo finder outputs
    if (self->gridLayer == APP_VIEW_GRID_STEREO_RAW)
      image = self->fused->stereoRoadFinder->updateRawImage(self->fused->localMap);
    else if (self->gridLayer == APP_VIEW_GRID_STEREO_LINE)
      image = self->fused->stereoRoadFinder->updateLineImage(self->fused->localMap);

    // Predraw fused images
    if (self->gridLayer == APP_VIEW_GRID_FUSED_LINE)
      image = self->fused->lineImage;
    else if (self->gridLayer == APP_VIEW_GRID_FUSED_LINE_DIST)
      image = self->fused->lineDistImage;

    // Pre-draw the fused map outputs
    //if (self->gridLayer == APP_VIEW_FUSED_LINE)
    //  image = self->fused->getLineImage();

    if (image)
    {
      self->predrawLocalMap(self->fused->localMap, &self->fused->state, image);
    }

    // Save the current map image to a file
    if (false && image)
    {
      char filename[1024];
      snprintf(filename, sizeof(filename), "%04d-map.pnm", 0);
      MSG("writing %s", filename);
      dgc_image_write_pnm(image, filename, NULL);
    }
    
    MSG("predraw %dms", (int) ((DGCgettime() - time) / 1000));
    self->dirty = false;
  }
  
  glPushMatrix();
  glTranslatef(-self->fused->state.localX, -self->fused->state.localY, -self->fused->state.localZ);
  
  // Switch to vehicle frame to draw axes
  self->pushFrameVehicle(&self->fused->state);

  // Draw Alice
  self->drawAxes(1.0);
  self->drawAlice();

  // Draw GPS ellipse.
  // TODO: this is incorrect, since it is rotated wrt the global frame.
  if (true)
  {
    double dx, dy;
    glColor3f(0, 0, 1);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin(GL_QUADS);
    dx = self->fused->state.utmNorthConfidence;
    dy = self->fused->state.utmEastConfidence;
    glVertex2f(-dx, -dy);
    glVertex2f(+dx, -dy);
    glVertex2f(+dx, +dy);
    glVertex2f(-dx, +dy);
    glEnd();
  }
  
  self->popFrame();

  if (self->gridLayer != APP_VIEW_GRID_NONE)
  {
    // Draw map image in map frame.  The local map pose is defined in
    // the local frame.
    glCallList(self->gridList);
  }

  // Draw prior map in prior map frame.  The prior map pose is defined
  // in the global frame, and we we have to manually tweak the z value
  // to draw it under the vehicle.
  if (self->globalLayer != APP_VIEW_GLOBAL_NONE)
  {
    self->pushFrameGlobal(&self->fused->state, self->fused->priorMap->pose);
    glTranslatef(0, 0, self->fused->state.localZ);
    glCallList(self->globalList);
    self->popFrame();
  }

  // Draw stuff in local frame. We still have to manually tweak the z
  // value to draw it under the vehicle.
  if (self->localLayer != APP_VIEW_LOCAL_NONE)
  {
    glPushMatrix();
    glTranslatef(0, 0, self->fused->state.localZ);
    glCallList(self->localList);
    glPopMatrix();
  }
  
  glPopMatrix();
  
  return;
}


// Switch to the vehicle frame
void App::pushFrameVehicle(const VehicleState *state)
{
  pose3_t pose;
  pose.pos = vec3_set(state->localX, state->localY, state->localZ);
  pose.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);

  float m[4][4];
  pose3_to_mat44f(pose, m);  

  // Transpose to column-major order for GL
  int i, j;
  float t[16];
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);
  
  return;
}


// Switch to the map local frame
void App::pushFrameLocal(const VehicleState *state, pose3_t mapPose)
{
  double M[4][4];
  
  // Transform from map frame to local frame
  pose3_to_mat44d(mapPose, M);

  // Change to column-major order
  mat44d_trans(M, M);

  // Use this transform
  glPushMatrix();
  glMultMatrixd((GLdouble*) M);

  return;
}


// Switch to the map global frame
void App::pushFrameGlobal(const VehicleState *state, pose3_t mapPose)
{
  pose3_t vehPoseGlobal, vehPoseLocal;
  double M[4][4], Mv[4][4], Ml[4][4], Mg[4][4];

  // Vehicle pose in local and global frames
  vehPoseLocal.pos = vec3_set(state->localX, state->localY, state->localZ);
  vehPoseLocal.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);
  vehPoseGlobal.pos = vec3_set(state->utmNorthing, state->utmEasting, state->utmAltitude);
  vehPoseGlobal.rot = quat_from_rpy(state->utmRoll, state->utmPitch, state->utmYaw);
  
  // Transform from map frame to global frame
  pose3_to_mat44d(mapPose, Mg);  
  
  // Transform from global frame to vehicle frame
  pose3_to_mat44d(pose3_inv(vehPoseGlobal), Mv);
  
  // Transform from vehicle frame to local frame
  pose3_to_mat44d(vehPoseLocal, Ml);

  // Combined transform
  mat44d_mul(M, Mv, Mg);
  mat44d_mul(M, Ml, M);

  //MSG("%f %f %f", M[0][3], M[1][3], M[2][3]);
          
  // Change to column-major order
  mat44d_trans(M, M);

  // Use this transform
  glPushMatrix();
  glMultMatrixd((GLdouble*) M);
  
  return;
}


// Revert to previous frame
void App::popFrame()
{
  glPopMatrix();  
  return;
}


// Draw a set of axes
void App::drawAxes(float size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Draw the robot
void App::drawAlice()
{
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
    
  return;
}


// Pre-draw the local map as an image
void App::predrawLocalMap(LocalMap *localMap, const VehicleState *state, dgc_image_t *image)
{
  float mx, my;
    
  // Create texture
  if (this->gridTex == 0)
    glGenTextures(1, &this->gridTex);

  // Copy image into texture
  glBindTexture(GL_TEXTURE_2D, this->gridTex);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  // Mono 8-bit images
  if (image->channels == 1 && image->bits == 8)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4, image->cols, image->rows,
                      GL_LUMINANCE, GL_UNSIGNED_BYTE, image->data);
  // RGB 8-bit images
  else if (image->channels == 3 && image->bits == 8)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4, image->cols, image->rows,
                      GL_RGB, GL_UNSIGNED_BYTE, image->data);

  // Mono 32-bit floating point images
  else if (image->channels == 1 && image->bits == 32)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4, image->cols, image->rows,
                      GL_LUMINANCE, GL_FLOAT, image->data);

  // Create display list
  if (this->gridList == 0)
    this->gridList = glGenLists(1);
  glNewList(this->gridList, GL_COMPILE);

  // Draw everything in the local/map frame, with a little offset to
  // place it underneath the wheels
  this->pushFrameLocal(state, this->fused->localMap->pose);
  glTranslatef(0, 0, +VEHICLE_TIRE_RADIUS);
    
  glBindTexture(GL_TEXTURE_2D, this->gridTex);
  glEnable(GL_TEXTURE_2D);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glColor3f(1, 1, 1);

  glBegin(GL_QUADS);
  
  localMap->cvtIndexToPos(0, 0, &mx, &my); 
  glTexCoord2f(0, 0);
  glVertex2f(mx, my);

  localMap->cvtIndexToPos(localMap->size, 0, &mx, &my); 
  glTexCoord2f(1, 0);
  glVertex2f(mx, my);

  localMap->cvtIndexToPos(localMap->size, localMap->size, &mx, &my);
  glTexCoord2f(1, 1);
  glVertex2f(mx, my);

  localMap->cvtIndexToPos(0, localMap->size, &mx, &my); 
  glTexCoord2f(0, 1);
  glVertex2f(mx, my);

  glEnd();

  this->popFrame();
  
  glDisable(GL_TEXTURE_2D);
  
  glEndList();
  
  return;
}


// Handle idle callbacks
void App::onIdle(App *self)
{  
  if (!self->pause || self->step)
  {  
    // Update perceptor
    if (self->update() != 0)
      self->quit = true;

    // Redraw the display
    self->worldwin->redraw();
    
    // Single step mode: we've moved one step, so clear flag
    if (self->step)
      self->step = false;
  }
  else
  {
    // Sleepy bye-bye
    usleep(0);
  }
  
  return;
}


// Initialize the perceptor
int App::init()
{
  // Create fused perceptor
  this->fused = new FusedPerceptor();
  assert(this->fused);

  // If there are no input files, assume we are running in live mode.
  if (this->options.inputs_num == 0)
  {
    const char *spreadDaemon;
    int skynetKey;
    
    // Fill out the spread name
    if (this->options.spread_daemon_given)
      spreadDaemon = this->options.spread_daemon_arg;
    else if (getenv("SPREAD_DAEMON"))
      spreadDaemon = getenv("SPREAD_DAEMON");
    else
      return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
    // Fill out the skynet key
    if (this->options.skynet_key_given)
      skynetKey = this->options.skynet_key_arg;
    else if (getenv("SKYNET_KEY"))
      skynetKey = atoi(getenv("SKYNET_KEY"));
    else
      skynetKey = 0;

    if (this->fused->initLive(spreadDaemon, skynetKey, MODfusedviewer) != 0)
      return -1;
  }
  else
  {
    if (this->fused->initReplay(this->options.inputs_num,
                                this->options.inputs, this->options.seek_arg) != 0)
      return -1;
  }

  // Load the RNDF
  if (this->options.rndf_given)
  {
    if (this->fused->loadRNDF(this->options.rndf_arg) != 0)
      return -1;
  }

  return 0;
}


// Finalize the perceptor
int App::fini()
{
  this->fused->fini();  

  delete this->fused;
  this->fused = NULL;
  
  return 0;  
}


// Update the perceptor
int App::update()
{
  uint64_t time;
  VehicleState *state;
  
  // Update the perceptor
  if (this->fused->mode == FusedPerceptor::modeLive)
  {
    if (sensnet_wait(this->fused->sensnet, 50) == 0)
      this->fused->updateLive();
  }
  else
  {
    this->fused->updateReplay();
  }

  // Use the dianostic state
  state = &this->fused->state;
  
  // Update the display periodically (100ms)
  if (state->timestamp - this->dirtyTimestamp > 200 * 1000)
  {
    MSG("timestamp %.3f", state->timestamp*1e-6);
    
    // Prepare the cost maps
    time = DGCgettime();
    this->fused->prepare(state);
    MSG("prepare %dms", (int) ((DGCgettime() - time) / 1000));
    
    this->dirtyTimestamp = state->timestamp;
    this->dirty = true;
  }
     
  return 0;
}


int main(int argc, char *argv[])
{
  App *app;

  // Initialize GLUT calls
  glutInit(&argc, argv);

  app = new App();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  if (app->initGUI(1024, 768) != 0)
    return -1;

  if (app->init() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) App::onIdle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  app->finiGUI();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
