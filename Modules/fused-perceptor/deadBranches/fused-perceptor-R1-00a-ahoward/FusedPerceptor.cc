
/* 
 * Desc: Fused perceptor: combines data from multple sources into a grid representation.
 * Date: 27 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>

#include <opencv/cv.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include "FusedPerceptor.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
FusedPerceptor::FusedPerceptor()
{
  int i;
  
  this->frameCount = 0;
  
  // Create the prior map
  this->priorMap = new PriorMap(); // MAGIC

  // Create local map
  this->localMap = new LocalMap(256, 0.15);

  // Create ladar road finder
  this->ladarRoadFinder = new LadarRoadFinder(this->localMap);

  // Create stereo road finder
  this->stereoRoadFinder = new StereoRoadFinder(this->localMap);

  // Workspace for configuration space
  this->numLineImages = 16;
  for (i = 0; i < this->numLineImages; i++)
  {
    this->lineImages[i] = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);
    this->lineSumImages[i] = dgc_image_alloc(localMap->size+1, localMap->size+1, 1, 32, 0, NULL);
  }
  
  // Default vehicle dimensions TODO
  this->rectAx = -1;
  this->rectAy = -1;
  this->rectBx = +4;
  this->rectBy = +1;

  // Initialize some storage areas
  memset(&this->state, 0, sizeof(this->state));
  this->lineBlobId = -1;
  
#if USE_GL
  this->roiList = 0;
#endif

  return;
}


// Destructor
FusedPerceptor::~FusedPerceptor()
{  
  delete this->stereoRoadFinder;
  this->stereoRoadFinder = NULL;

  delete this->ladarRoadFinder;
  this->ladarRoadFinder = NULL;

  delete this->localMap;
  this->localMap = NULL;

  delete this->priorMap;
  this->priorMap = NULL;
  
  return;
}


// Initialize perceptor (live mode)
int FusedPerceptor::initLive(const char *spreadDaemon, int skynetKey, int moduleId)
{
  this->mode = modeLive;
  this->spreadDaemon = spreadDaemon;
  this->skynetKey = skynetKey;

  // Open for live display
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);  
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, moduleId) != 0)
    return ERROR("unable to connect to sensnet");

  // MAGIC
  // Join line group for live line data
  this->lineEnabled = true;
  if (sensnet_join(this->sensnet, SENSNET_STEREO_LINE_PERCEPTOR,
                   SENSNET_ROAD_LINE_BLOB, sizeof(RoadLineBlob)) != 0)
    return -1;

  return 0;
}


// Initialize perceptor (replay mode)
int FusedPerceptor::initReplay(int numFilenames, char **filenames, double seekTime)
{
  this->mode = modeReplay;
  
  // Open a set of logs
  this->replay = sensnet_replay_alloc();
  if (sensnet_replay_open(this->replay, numFilenames, filenames) != 0)
    return ERROR("unable to open one or more log files");

  // Seek to starting point
  if (seekTime > 0)
  {
    if (sensnet_replay_seek(this->replay, (uint64_t) (seekTime * 1e6)) != 0)
      return ERROR("unable to seek to %.3f", seekTime);
  }

  return 0;
}


// Finialize perceptor
int FusedPerceptor::fini(void)
{
  if (this->mode == modeLive)
  {
    sensnet_disconnect(this->sensnet);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
  else
  {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Load RNDF file
int FusedPerceptor::loadRNDF(char *filename)
{
  return this->priorMap->load(filename);
}


// Update the perceptor with current data
int FusedPerceptor::update()
{
  if (this->mode == modeLive)
    return this->updateLive();
  else
    return this->updateReplay();
}


// Update the perceptor (live mode)
int FusedPerceptor::updateLive()
{
  int blobId;
  
  assert(this->sensnet);
  
  // Wait some time for new data
  // REMOVE sensnet_wait(this->sensnet, timeout);
    
  // TODO check for ladar data
  
  // TODO check for stereo data

  // Process road line data
  if (this->lineEnabled)
  {
    if (sensnet_read(this->sensnet, SENSNET_STEREO_LINE_PERCEPTOR, SENSNET_ROAD_LINE_BLOB,
                     &blobId, sizeof(this->lineBlob), &this->lineBlob) != 0)
      return ERROR("unable to read line blob");
    if (blobId > this->lineBlobId)
      this->updateRoadLine(&this->lineBlob);
    this->lineBlobId = blobId;
  }

  return 0;
}


// Update the perceptor (replay mode)
int FusedPerceptor::updateReplay()
{
  int sensorId, blobType, blobId;

  assert(this->replay);
  
  // Read data from the logs
  sensnet_replay_next(this->replay, 0);
  sensnet_replay_query(this->replay, NULL, &sensorId, &blobType);

  // Process ladar data
  if (blobType == SENSNET_LADAR_BLOB)
  {
    if (sensnet_replay_read(this->replay, sensorId, blobType,
                            &blobId, sizeof(this->ladarBlob), &this->ladarBlob) != 0)
      return -1;
    this->updateLadarRange(&this->ladarBlob);
  }

  // Process stereo data
  if (blobType == SENSNET_STEREO_IMAGE_BLOB)
  {
    if (sensnet_replay_read(this->replay, sensorId, blobType,
                            &blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;
    this->updateStereoImage(&this->stereoBlob);
  }

  return 0;
}


// Update based on state data
int FusedPerceptor::updateState(const VehicleState *state)
{
  // Keep a copy
  this->state = *state;
    
  // Initialize the map
  if (this->frameCount++ == 0)
    this->localMap->clear(state);
  
  // Shift the local map if necessary
  this->localMap->move(state);

  return 0;
}


// Update with ladar range data
int FusedPerceptor::updateLadarRange(LadarRangeBlob *blob)
{
  // Keep a copy of the latest state
  if (blob->state.timestamp > this->state.timestamp)
    this->updateState(&blob->state);

  this->ladarRoadFinder->projectLadarRaw(this->localMap, blob);
  
  return 0;
}


// Update with stereo image data
int FusedPerceptor::updateStereoImage(StereoImageBlob *blob)
{
  // Keep a copy of the latest state
  if (blob->state.timestamp > this->state.timestamp)
    this->updateState(&blob->state);

  this->stereoRoadFinder->projectStereoRaw(this->localMap, blob);
  
  return 0;
}


// Project line data from road perceptors.
int FusedPerceptor::updateRoadLine(RoadLineBlob *blob)
{
  // Keep a copy of the latest state
  if (blob->state.timestamp > this->state.timestamp)
    this->updateState(&blob->state);

  // TODO
  this->stereoRoadFinder->projectRoadLine(this->localMap, blob);
  
  return 0;
}


// Prepare the internal tables for cost evaluation.
int FusedPerceptor::prepareCost(pose3_t planPose)
{
  uint64_t time;
  VehicleState *state;
  pose3_t transLV, transGV, transLP;

  state = &this->state;

  // Construct transform to local frame from vehicle frame.
  transLV.pos = vec3_set(state->localX, state->localY, state->localZ);
  transLV.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);

  // Construct transform to global frame from vehicle frame.
  transGV.pos = vec3_set(state->utmNorthing, state->utmEasting, state->utmAltitude);
  transGV.rot = quat_from_rpy(state->utmRoll, state->utmPitch, state->utmYaw);

  // Construct transform to local frame from planner frame.
  transLP = pose3_mul(transLV, pose3_mul(pose3_inv(transGV), planPose));

  // Construct transform matrices
  pose3_to_mat44f(transLP, this->transLP);
  mat44f_inv(this->transPL, this->transLP);

  // Construct the line image
  time = DGCgettime();
  this->createLineImage();
  MSG("line image %d", (int) ((DGCgettime() - time) / 1000));
  
  // Construct the rotated integral images from the original line image
  time = DGCgettime();
  this->createLineSumImages();
  MSG("line sum images %d", (int) ((DGCgettime() - time) / 1000));
  
  return 0;
}


// Create the original line image
int FusedPerceptor::createLineImage()
{
  int i, j, k;
  uint8_t *pix;
  dgc_image_t *image;
  const LocalMapCell *cell;

  // TODO: this should be limited to points in a circle,
  // so that we dont get weird planner artifacts arising
  // from c-space tests at different orientation bins.

  // Use the 0 rotation image as the destination
  image = this->lineImages[0];
  
  pix = (uint8_t*) dgc_image_pixel(image, 0, 0);
  cell = this->localMap->getCellByIndex(0, 0);      
  for (j = 0; j < image->rows; j++)
  {
    for (i = 0; i < image->rows; i++, cell += 1, pix += 1)
    {
      pix[0] = 0;

      // TESTING
      // This should be bounded between 0x00 and 0xFF
      //k = 0xFF * cell->stereoLine / cell->stereoLineCount;
      //k = cell->stereoRaw[0]/cell->stereoRawCount;

      // Use the lines to construct the cost image
      if (cell->stereoLineCount > 8) // MAGIC
        k = 0xFF;
      else
        k = 0x00;

      //if (cell->ladarRoughCount == 0)
      //  continue;
      //if (-cell->ladarRough/cell->ladarRoughCount > 0.20) // MAGIC
      //  k = 0x01;
      //else
      //  k = 0x00;
      //k = (int) (-cell->ladarRough/cell->ladarRoughCount*500); // MAGIC

      //printf("%d %d %d\n", i, j, k);
      
      if (k < 0x00) k = 0x00;
      if (k > 0xFF) k = 0xFF;
      pix[0] = (uint8_t) k;
    }
  }

  return 0;
}


// Create a set of integral rotated line images
int FusedPerceptor::createLineSumImages()
{
  int i;
  float rotAngle;
  float m[2][3];
  float transIP[4][4], transIL[4][4], transIR[4][4], transRM[4][4];
  
  rotAngle = M_PI/2 / (this->numLineImages - 1);

  // Source image is the un-rotated line image
  CvMat srcImage = cvMat(this->lineImages[0]->cols,
                         this->lineImages[0]->rows, CV_8U, this->lineImages[0]->data);
  
  // Create images at different rotation angles
  for (i = 0; i < this->numLineImages; i++)
  {
    // Destination rotated image 
    CvMat dstImage = cvMat(this->lineImages[i]->cols,
                           this->lineImages[i]->rows, CV_8U, this->lineImages[i]->data);

    // Destination integral image
    CvMat sumImage = cvMat(this->lineSumImages[i]->cols,
                           this->lineSumImages[i]->rows, CV_32S, this->lineSumImages[i]->data);

    // Construct transform matrix for image rotation
    m[0][0] = +cos(i*rotAngle);  m[0][1] = -sin(i*rotAngle); m[0][2] = 0.5*srcImage.width;
    m[1][0] = +sin(i*rotAngle);  m[1][1] = +cos(i*rotAngle); m[1][2] = 0.5*srcImage.height;

    // Construct transform from map frame to rotated map frame
    mat44f_zero(transRM);
    transRM[0][0] = m[0][0];
    transRM[0][1] = -m[0][1];
    transRM[0][3] = 0; //m[0][2];
    transRM[1][0] = -m[1][0];
    transRM[1][1] = m[1][1];
    transRM[1][3] = 0; //m[1][2];
    transRM[2][2] = 1;
    transRM[3][3] = 1;

    // Construct transform from rotated map frame to image coordinates
    mat44f_zero(transIR);
    transIR[0][0] = 1.0 / this->localMap->scale;
    transIR[1][1] = 1.0 / this->localMap->scale;
    transIR[0][3] = m[0][2];
    transIR[1][3] = m[1][2];
    transIR[2][2] = 1;
    transIR[3][3] = 1;

    // Construct combined transform from local frame to image coordinates
    mat44f_mul(transIL, transRM, localMap->transML);
    mat44f_mul(transIL, transIR, transIL);

    // Construct combined transform from prior frame to image coordinates
    mat44f_mul(transIP, transIL, this->transLP);
    
    this->transforms[i][0] = transIP[0][0];
    this->transforms[i][1] = transIP[0][1];
    this->transforms[i][2] = transIP[0][3];
    this->transforms[i][3] = transIP[1][0];
    this->transforms[i][4] = transIP[1][1];
    this->transforms[i][5] = transIP[1][3];

    // Create rotated image.  For the zero'th bin, we dont need to do
    // anything.
    if (i > 0)
    {
      CvMat cvm = cvMat(2, 3, CV_32F, m);
      cvGetQuadrangleSubPix(&srcImage, &dstImage, &cvm);
    }
        
    // Create integral image
    cvIntegral(&dstImage, &sumImage, NULL, NULL);

    if (false)
    {
      // Save line images
      char filename[1024];
      snprintf(filename, sizeof(filename), "line-%02d.pgm", i);
      MSG("writing %s", filename);
      dgc_image_write_pnm(this->lineImages[i], filename, NULL);
    }
  }
  
  return 0;
}


// Compute the line cost at the given pose.
int FusedPerceptor::evalLine(float px, float py, float ph)
{
  float qx, qy;
  float ax, ay, bx, by;
  float scale;
  int max, may, mbx, mby;
  float *m;
  int saa, sab, sba, sbb, sum;
  float rotAngle;
  int bin;
  dgc_image_t *sumImage;

  // Rotation bin width
  rotAngle = M_PI/2 / (this->numLineImages - 1);

  // Scale factor
  scale = 1.0/this->localMap->scale;

  // Boundary points
  ax = this->rectAx;
  ay = this->rectAy;
  bx = this->rectBx;
  by = this->rectBy;

  // Pick the bin the best aligns with the given orientation, and
  // compute the corners of the test rectangle in that orientation
  // image.  Since we only compute rotated images in the range 0 to
  // PI/2 inclusive, we may have to switch the corners around to get
  // the same part of the image.
  if (0 <= ph && ph < +M_PI/2)
  {
    bin = (int) (ph / rotAngle + 0.5);
    assert(bin >= 0 && bin < this->numLineImages);
    
    m = this->transforms[bin];    
    qx = m[0]*px + m[1]*py + m[2];
    qy = m[3]*px + m[4]*py + m[5];

    max = (int) (ax*scale + qx);
    may = (int) (ay*scale + qy);    
    mbx = (int) ((bx - ax)*scale + max);
    mby = (int) ((by - ay)*scale + may);
  }
  else if (0 <= ph + M_PI/2 && ph + M_PI/2 < M_PI/2)
  {
    ph += M_PI/2;

    bin = (int) (ph / rotAngle + 0.5);
    assert(bin >= 0 && bin < this->numLineImages);
    
    m = this->transforms[bin];    
    qx = m[0]*px + m[1]*py + m[2];
    qy = m[3]*px + m[4]*py + m[5];

    max = (int) (+ay*scale + qx);
    may = (int) (-bx*scale + qy);
    mbx = (int) ((by - ay)*scale + max);
    mby = (int) ((bx - ax)*scale + may);
  }
  else if (0 <= ph - M_PI/2 && ph - M_PI/2 < M_PI/2)
  {
    ph -= M_PI/2;

    bin = (int) (ph / rotAngle + 0.5);
    assert(bin >= 0 && bin < this->numLineImages);
    
    m = this->transforms[bin];    
    qx = m[0]*px + m[1]*py + m[2];
    qy = m[3]*px + m[4]*py + m[5];

    max = (int) (-by*scale + qx);
    may = (int) (+ax*scale + qy);
    mbx = (int) ((by - ay)*scale + max);
    mby = (int) ((bx - ax)*scale + may);
  }
  else if (0 <= ph + M_PI && ph + M_PI < M_PI/2)
  {
    ph += M_PI;

    bin = (int) (ph / rotAngle + 0.5);
    assert(bin >= 0 && bin < this->numLineImages);
    
    m = this->transforms[bin];    
    qx = m[0]*px + m[1]*py + m[2];
    qy = m[3]*px + m[4]*py + m[5];

    max = (int) (-bx*scale + qx);
    may = (int) (-by*scale + qy);
    mbx = (int) ((bx - ax)*scale + max);
    mby = (int) ((by - ay)*scale + may);
  }
  else
    assert(false);

  //MSG("%d : %f %f -> %f %f : %d %d %d %d",
  //    bin, px, py, qx, qy, max, may, mbx, mby);        

  sumImage = this->lineSumImages[bin];
    
  // Do a bounds check
  if (max < 0 || mbx >= sumImage->cols)
    return 0; // TODO
  if (may < 0 || mby >= sumImage->rows)
    return 0; // TODO

  // Compute the sum over the rectangle
  saa = *(int32_t*) dgc_image_pixel(sumImage, max, may);
  sab = *(int32_t*) dgc_image_pixel(sumImage, max, mby);
  sba = *(int32_t*) dgc_image_pixel(sumImage, mbx, may);
  sbb = *(int32_t*) dgc_image_pixel(sumImage, mbx, mby);
  sum = sbb - sab - sba + saa;

  // Normalize
  //sum /= ((mbx - max + 1) * (mby - may + 1));

  //MSG("%f %f %f %d %d %d\n", px, py, ph, saa, sab, sum);
  
  return sum;
}


// Update the stereo line map
dgc_image_t *FusedPerceptor::getLineImage()
{
  return this->lineImages[0];
}


#if USE_GL

// Draw the costs for configurations in the prior map (out to some distance).
int FusedPerceptor::predrawROI(float size)
{
  float m[4][4];
  float qx, qy, px, py;

  // Take the current local map pose and transform it into the prior
  // map frame
  qx = this->localMap->pose.pos.x;
  qy = this->localMap->pose.pos.y;
  mat44f_setf(m, this->transPL);  
  px = m[0][0]*qx + m[0][1]*qy + m[0][3];
  py = m[1][0]*qx + m[1][1]*qy + m[1][3];

  // Create display list
  if (this->roiList == 0)
    this->roiList = glGenLists(1);
  glNewList(this->roiList, GL_COMPILE);

  if (this->priorMap->mapRoot)
    this->predrawROIQuad(this->priorMap->mapRoot, px, py, size);
  
  glEndList();

  return this->roiList;
}


// Draw the costs for configurations in the prior map (out to some distance).
// This function is applied recursively to the quad-tree.
int FusedPerceptor::predrawROIQuad(PriorMapQuad *quad, float px, float py, float size)
{
  int i;
  PriorMapQuad *leaf;
  PriorMapPoint *point;

  // Check for overlap between this quad and the ROI
  if (px - size/2 > quad->px + quad->size/2)
    return 0;
  if (px + size/2 < quad->px - quad->size/2)
    return 0;
  if (py - size/2 > quad->py + quad->size/2)
    return 0;
  if (py + size/2 < quad->py - quad->size/2)
    return 0;

  // Let the leaves add points
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->predrawROIQuad(leaf, px, py, size);
  }
  
  // Draw points in this quad
  for (i = 0; i < quad->numPoints; i++)
  {
    point = quad->points + i;

    glColor3f(1, 1, 0);

    float k;        
    k = this->evalLine(point->px, point->py, point->ph);    
    if (k < 0) k = 0;
    if (k > 1) k = 1;
    glColor3f(k, 1 - k, 0);

    glBegin(GL_LINES);
    glVertex2f(point->px, point->py);
    glVertex2f(point->px + 0.20 * cos(point->ph), point->py + 0.20 * sin(point->ph));
    glEnd();
  
    if (k > 0)
    {
      // Draw the vehicle outline
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glPushMatrix();
      glTranslatef(point->px, point->py, 0);
      glRotatef(point->ph * 180/M_PI, 0, 0, 1);
      glBegin(GL_QUADS);
      glVertex2f(this->rectAx, this->rectAy);
      glVertex2f(this->rectBx, this->rectAy);
      glVertex2f(this->rectBx, this->rectBy);
      glVertex2f(this->rectAx, this->rectBy);
      glEnd();
      glPopMatrix();
    }
  }


  return 0;
}


#endif
