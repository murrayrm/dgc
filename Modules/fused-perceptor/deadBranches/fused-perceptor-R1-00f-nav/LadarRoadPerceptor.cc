
/* 
 * Desc: Road/line finding with Riegl ladar
 * Date: 16 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <opencv/cv.h>

#include <frames/mat44.h>
#include "LadarRoadFinder.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
LadarRoadFinder::LadarRoadFinder(LocalMap *localMap)
{
  // Initialize diagnostic images
  this->monoImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);
  this->edgeImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);
  this->lineImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);
  this->flatImage = dgc_image_alloc(localMap->size, localMap->size, 1, 8, 0, NULL);  
  
  return;
}


// Destructor
LadarRoadFinder::~LadarRoadFinder()
{  
  dgc_image_free(this->flatImage);
  dgc_image_free(this->lineImage);
  dgc_image_free(this->edgeImage);
  dgc_image_free(this->monoImage);

  return;
}


// Project ladar data into map
int LadarRoadFinder::projectLadar(LocalMap *localMap, LadarRangeBlob *blob)
{
  int i, di, dj;
  float pa, pr;
  float sx, sy, sz;
  float vx, vy, vz;
  float mx, my, mz;
  int mi, mj;
  uint8_t mono;
  float m[4][4];
  LocalMapCell *cell, *ncell;
  float monoEdges[LADAR_BLOB_MAX_POINTS];
  float rangeValid[LADAR_BLOB_MAX_POINTS];
  float rangeEdges[LADAR_BLOB_MAX_POINTS];
  float lines[LADAR_BLOB_MAX_POINTS];
  
  // Run an edge detector on the intensity data
  for (i = 0; i < blob->numPoints; i++)
    monoEdges[i] = (float) blob->intensities[i];    
  this->calcDoG(0.3, 3, blob->numPoints, monoEdges);   // MAGIC

  // Run an edge detector on the range data
  for (i = 0; i < blob->numPoints; i++)
  {
    rangeValid[i] = blob->points[i][1];
    rangeEdges[i] = blob->points[i][1];
  }
  this->calcInvalid(150, 3 * 5, blob->numPoints, rangeValid); // MAGIC
  this->calcDoG(0.3, 5, blob->numPoints, rangeEdges);   // MAGIC

  // Decide if this is a lane marking
  for (i = 0; i < blob->numPoints; i++)
  {
    if (fabs(rangeEdges[i]) < 0.10) // MAGIC
      lines[i] = 0; // TESTING monoEdges[i];
    else
      lines[i] = 0;
  }

  //for (i = 0; i < blob->numPoints; i++)
  //{
  //  printf("%d %f %d %f %f\n", i,
  //         blob->points[i][1], blob->intensities[i],
  //         rangeEdges[i], monoEdges[i]);
  //} 
  //printf("\n\n");
  //fflush(stdout);
  
  // Compute transform from sensor to vehicle to local to map frame
  mat44f_mul(m, blob->veh2loc, blob->sens2veh);
  mat44f_mul(m, localMap->transML, m);

  for (i = 0; i < blob->numPoints; i++)
  {
    pa = blob->points[i][0];
    pr = blob->points[i][1];
    mono = blob->intensities[i];
    
    if (pr < 0 || pr > 80.0) // MAGIC
      continue;

    // Compute points in sensor frame
    LadarRangeBlobScanToSensor(blob, pa, pr, &sx, &sy, &sz);

    // Convert to vehicle frame
    LadarRangeBlobSensorToVehicle(blob, sx, sy, sz, &vx, &vy, &vz);
    
    // Discard points that are higher in the vehicle frame; these
    // probably dont come from the ground.
    if (vz < -0.5) // MAGIC
      continue;
    
    // Convert to map frame
    mx = m[0][0]*sx + m[0][1]*sy + m[0][2]*sz + m[0][3];
    my = m[1][0]*sx + m[1][1]*sy + m[1][2]*sz + m[1][3];
    mz = m[2][0]*sx + m[2][1]*sy + m[2][2]*sz + m[2][3];
        
    // Get the cell at this location
    localMap->cvtPosToIndex(mx, my, &mi, &mj);
    cell = localMap->getCellByIndex(mi, mj);
    if (!cell)
      continue;

    // If this is the first time we have seen the cell, reset the
    // counters
    if (cell->ladarSeen == 0)
    {
      cell->ladarSeen = 1;
      cell->ladarMonoCount = 0;
      cell->ladarMono1 = 0;
      cell->ladarMono2 = 0;
      cell->ladarLineCount = 0;
      cell->ladarLine = 0;
      cell->ladarFlatCount = 0;
      cell->ladarFlat = 0;
    }
    
    // Update the cell intensity values.
    cell->ladarMonoCount += 1;
    cell->ladarMono1 += mono;
    cell->ladarMono2 += mono * mono;

    // Update the cell flatness in regions where the ladar is valid.
    if (rangeValid[i])
    {
      cell->ladarFlatCount += 1;
      cell->ladarFlat += rangeEdges[i];
    }

    // Fill in nearby cells if they haven't already been seen.  These
    // values serve as a default that gets over-written by the first
    // ladar point that lands in the cell (the cell is "seen").  Kind
    // of an on-the-fly mean filter for unknown cells.  With the
    // current line detection algorithm, this is entirely cosmetic.
    if (false)
    {
      for (dj = -1; dj <= +1; dj++)
      {
        for (di = -1; di <= +1; di++)
        {
          ncell = localMap->getCellByIndex(mi + di, mj + dj);
          if (!ncell)
            continue;
          if (ncell->ladarSeen)
            continue;
          ncell->ladarMonoCount += 1;
          ncell->ladarMono1 += mono;
          ncell->ladarMono2 += mono * mono;
          if (rangeValid[i])
          {
            ncell->ladarFlatCount += 1;
            ncell->ladarFlat += rangeEdges[i];
          }
        }
      }
    }
  }
  //fprintf(stdout, "\n\n");
  //fflush(stdout);
  
  return 0;
}


// Compute an expanded notion of invalid readings
int LadarRoadFinder::calcInvalid(float maxValid, int window, int numPoints, float *points)
{
  int i, di;
  float filt[numPoints];

  for (i = 0; i < window; i++)
    filt[i] = 0;
  for (; i < numPoints - window; i++)
  {
    filt[i] = 1;
    for (di = -window; di <= +window; di++)
    {
      if (points[i + di] > maxValid)
      {
        filt[i] = 0;
        break;
      }
    }
  }
  for (; i < numPoints; i++)
    filt[i] = 0;
  
  // Overwrite in input signal
  for (i = 0; i < numPoints; i++)
    points[i] = filt[i];
    
  return 0;
}


// Compute the difference of gaussians on a signal
int LadarRoadFinder::calcDoG(float inner, float outer, int numPoints, float *points)
{
  int i, di;
  int numInner, numOuter;
  float norm;
  float kernInner[64], kernOuter[64];
  float filtInner[numPoints], filtOuter[numPoints];

  numInner = (int) ceil(3 * inner);
  numOuter = (int) ceil(3 * outer);
  assert(numOuter >= numInner);

  // Construct convolution kernels
  norm = 0;
  for (di = -numInner; di <= +numInner; di++)
    norm += (float) exp(-di*di/(inner*inner)/2);
  assert((size_t) (2 * numInner + 1) < sizeof(kernInner)/sizeof(kernInner[0]));
  for (di = -numInner; di <= +numInner; di++)
    kernInner[di + numInner] = (float) exp(-di*di/(inner*inner)/2) / norm;

  norm = 0;
  for (di = -numOuter; di <= +numOuter; di++)
    norm += (float) exp(-di*di/(outer*outer)/2);
  assert((size_t) (2 * numOuter + 1) < sizeof(kernOuter)/sizeof(kernOuter[0]));
  for (di = -numOuter; di <= +numOuter; di++)
    kernOuter[di + numOuter] = (float) exp(-di*di/(outer*outer)/2) / norm;

  // Compute inner gaussian filter.  Note that we use the radius of
  // the outer filter when discarding edges; this allows us to safely
  // take the difference of inner and outer without creating
  // artifacts.
  for (i = 0; i < numOuter; i++)
    filtInner[i] = 0;
  for (; i < numPoints - numOuter; i++)
  {
    filtInner[i] = 0;
    for (di = -numInner; di <= +numInner; di++)
      filtInner[i] += points[di + i] * kernInner[di + numInner];
  }
  for (; i < numPoints; i++)
    filtInner[i] = 0;

  // Compute outer gaussian filter
  for (i = 0; i < numOuter; i++)
    filtOuter[i] = 0;
  for (; i < numPoints - numOuter; i++)
  {
    filtOuter[i] = 0;
    for (di = -numOuter; di <= +numOuter; di++)
      filtOuter[i] += points[di + i] * kernOuter[di + numOuter];
  }
  for (; i < numPoints; i++)
    filtOuter[i] = 0;

  // Compute difference of gaussians and overwrite in input signal
  for (i = 0; i < numPoints; i++)
    points[i] = filtInner[i] - filtOuter[i];
  
  return 0;
}




// Look for lines in the local map
int LadarRoadFinder::findLines(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  int bin, count, norm, hist[256];
  uint8_t *cpix;

  // Compute the map histogram; we are only going to accept points
  // in the top percentiles.
  norm = 0;
  memset(hist, 0, sizeof(hist));
  cell = localMap->getCellByIndex(0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, cell++)
    {
      if (cell->ladarMonoCount == 0)
        continue;
      bin = (int) cell->ladarMono1/cell->ladarMonoCount;
      assert(bin >= 0 && bin < 256);
      norm += 1;
      hist[bin] += 1;
    }
  }

  // Find n'th percentile level; the bin number will be used as a
  // threshold on cell intensity.
  count = 0;
  for (bin = 0; bin < 256; bin++)
  {
    count += hist[bin];
    if (count > norm * 0.60) // MAGIC
      break;
  }

  // Find bright pixels and copy them to the edge image.
  // We also threshold the intensity values to discard dark cells.
  // The edges are placed in edgeImage.
  this->findLineEdges(localMap, this->edgeImage, bin);

  // Remove line noise, such as single pixels.
  // The filtered results are place in the map.
  this->filterLineNoise(localMap, this->edgeImage, 2);

  // TODO: put results back in the map
  
    
  if (false)
  {
    CvMemStorage *storage;
    CvSeq *lines;
    CvPoint *line;

    // Allocate hough storage
    storage = cvCreateMemStorage(0);

    // Create edge image for the filter output
    assert(this->edgeImage);
    cpix = dgc_image_pixel(this->edgeImage, 0, 0);
    CvMat edgeImage = cvMat(this->edgeImage->rows, this->edgeImage->cols, CV_8UC1, cpix);

    // Probabilistic Hough Transform to get lines
    lines = cvHoughLines2(&edgeImage, storage, CV_HOUGH_PROBABILISTIC, 2, 2*CV_PI/180, 20, 8, 2);

    MSG("lines %d", lines->total);

    // Create hough image
    assert(this->lineImage);
    cpix = dgc_image_pixel(this->lineImage, 0, 0);
    CvMat lineImage = cvMat(this->lineImage->rows, this->lineImage->cols, CV_8UC1, cpix);

    // Clear the output image
    memset(this->lineImage->data, 0, this->lineImage->data_size);

    // Do some checks on the lines and draw the sensible looking ones
    // into the output image.  Note that we dont use disparity, which is
    // often undefined at strong edges; instead, we look at the rays
    // subtended by line end-points.
    for (i = 0; i < lines->total; i++)
    {
      line = (CvPoint*) cvGetSeqElem(lines, i);

      MSG("line %d %d,%d %d,%d", i, line[0].x, line[0].y, line[1].x, line[1].y);
      
      // Draw line in hough image
      cvLine(&lineImage, line[0], line[1], cvScalarAll(0xFF));
    }
  }

  return 0;
}


// Run a filter that looks for outliers based on local variance.
// That is, we compute the mean and variance over some window, then
// keep only those cells that are significantly brighter than their
// neigbors.  The cells must also be bright when compared against
// the entire map histogram.
int LadarRoadFinder::findLineEdges(LocalMap *localMap, dgc_image_t *image, int minValue)
{
  int i, j, di, dj;
  float m0, m1, m2;
  float thresh;
  LocalMapCell *cell, *ncell;
  int w;
  uint8_t *cpix;

  // Window for the variance filter
  w = (9 - 1) / 2;

  for (j = w; j < localMap->size - w; j++)
  {
    for (i = w; i < localMap->size - w; i++)
    {
      cell = localMap->getCellByIndex(i, j);
      cpix = dgc_image_pixel(this->edgeImage, i, j);
        
      cell->ladarLine = 0;
      cpix[0] = 0;

      // Can't use cells with no data
      if (cell->ladarMonoCount == 0)
        continue;

      // Threshold on the intensity
      if (cell->ladarMono1 < minValue * cell->ladarMonoCount)
        continue;
      
      // Compute local mean and variance
      m0 = m1 = m2 = 0;
      for (dj = -w; dj <= +w; dj++)
      {
        for (di = -w; di <= +w; di++)
        {
          ncell = localMap->getCellByIndex(i + di, j + dj);
          if (ncell->ladarMonoCount == 0)
            continue;
          m0 += ncell->ladarMonoCount;
          m1 += ncell->ladarMono1;
          m2 += ncell->ladarMono2;
        }
      }

      // Dont compute stats if there is insufficient data to make a
      // reliable assessment.
      if (m0 < 3) // MAGIC
        continue;

      // Compute the threshold (only points greater than the mean + n
      // std dev will be included).
      m1 /= m0;
      m2 = sqrtf(m2/m0 - m1*m1);
      thresh = m1 + 1.5 * m2; // MAGIC

      // Threshold on the variance
      if ((float) cell->ladarMono1/cell->ladarMonoCount < thresh)
        continue;

      // REMOVE
      //cell->ladarLineCount = cell->ladarMonoCount;
      //cell->ladarLine = cell->ladarMono1;
      cpix[0] = 2 * (uint8_t) (cell->ladarMono1/cell->ladarMonoCount); // MAGIC
    }
  }

  return 0;
}


// Remove line noise, such as single pixels in the edgeImage.  This
// should probably do a connected components check, but right now it
// simply counts not zero pixels in a neighborhood.  The results are
// placed in the local map.
int LadarRoadFinder::filterLineNoise(LocalMap *localMap, dgc_image_t *edgeImage, int minCount)
{
  int i, j, di, dj;
  int count;
  LocalMapCell *cell;
  int w;
  uint8_t *cpix, *dpix;

  // Window for the variance filter
  w = (3 - 1) / 2;

  for (j = w; j < edgeImage->rows - w; j++)
  {
    cpix = dgc_image_pixel(this->edgeImage, w, j);
    
    for (i = w; i < edgeImage->cols - w; i++, cpix++)
    {
      // Compute the number of non-zero neighbors
      count = 0;
      for (dj = -w; dj <= +w; dj++)
      {
        for (di = -w; di <= +w; di++)
        {
          dpix = cpix + di + dj * edgeImage->cols;
          if (dpix[0] > 0)
            count++;
        }
      }

      cell = localMap->getCellByIndex(i, j);
      if (cpix[0] > 0 && count >= minCount)
      {
        cell->ladarLineCount = cell->ladarMonoCount;
        cell->ladarLine = cell->ladarMono1;
      }
      else
      {
        cell->ladarLineCount = 0;
        cell->ladarLine = 0;
      }    
    }
  }
  
  return 0;
}





// Update the intensity image
dgc_image_t *LadarRoadFinder::updateMonoImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;
  int k;

  pix = (uint8_t*) dgc_image_pixel(this->monoImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);
      if (cell->ladarMonoCount > 0)
      {
        k = 2 * cell->ladarMono1 / cell->ladarMonoCount; // MAGIC
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->monoImage;
}


// Update the edge image
dgc_image_t *LadarRoadFinder::updateEdgeImage(LocalMap *localMap)
{
  return this->edgeImage;
}


// Update the line image
dgc_image_t *LadarRoadFinder::updateLineImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;
  float k;
        
  pix = (uint8_t*) dgc_image_pixel(this->lineImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);
      if (cell->ladarLineCount > 0)
      {
        k = 2 * cell->ladarLine / cell->ladarLineCount; // MAGIC
        if (k < 0) k = 0;
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->lineImage;
}


// Update the flat image
dgc_image_t *LadarRoadFinder::updateFlatImage(LocalMap *localMap)
{
  int i, j;
  LocalMapCell *cell;
  uint8_t *pix;
  float k;
        
  pix = (uint8_t*) dgc_image_pixel(this->flatImage, 0, 0);
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, pix += 1)
    {
      cell = localMap->getCellByIndex(i, j);
      assert(cell);
      if (cell->ladarFlatCount > 0)
      {
        k = 0x80 - cell->ladarFlat / cell->ladarFlatCount * 500; // MAGIC
        if (k < 0) k = 0;
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
      }
      else
      {
        pix[0] = 0x00;
      }
    }
  }
  
  return this->flatImage;
}
