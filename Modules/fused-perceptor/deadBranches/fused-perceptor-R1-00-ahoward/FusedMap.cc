
/* 
 * Desc: Fused map (combines local with prior)
 * Date: 22 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>

#if USE_GL
#include <GL/gl.h>
#endif

#include <dgcutils/DGCutils.hh>
#include <frames/mat44.h>
#include "LocalMap.hh"
#include "PriorMap.hh"
#include "FusedMap.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
FusedMap::FusedMap(LocalMap *localMap)
{
  int i;

  // Allocate space for ROI points
  this->maxPoints = this->numPoints = 0;
  this->points = NULL;

  // Allocate space for rotated cost images
  this->numImages = 32;
  assert((size_t) this->numImages <= sizeof(this->flatImages)/sizeof(this->flatImages[0]));  
  for (i = 0; i < this->numImages; i++)
  {
    this->flatImages[i] = dgc_image_alloc(localMap->size,
                                          localMap->size, 1, 8 * sizeof(float), 0, NULL);
    this->flatSumImages[i] = dgc_image_alloc(localMap->size + 1,
                                             localMap->size + 1, 1, 8 * sizeof(double), 0, NULL);
  }

  this->mapList = 0;
  
  return;
}


// Destructor
FusedMap::~FusedMap()
{
  int i;
  
  for (i = 0; i < this->numImages; i++)
  {
    dgc_image_free(this->flatSumImages[i]);
    dgc_image_free(this->flatImages[i]);
  }
  
  return;
}


// Update based on the current vehicle state.
int FusedMap::update(const VehicleState *state, const LocalMap *localMap, const PriorMap *priorMap)
{
  uint64_t time;

  // Update the ROI
  time = DGCgettime();
  this->updateROI(state, priorMap);
  MSG("update ROI %d", (int) ((DGCgettime() - time) / 1000)); 
  
  // Construct the cost image
  time = DGCgettime();
  this->createCostImages(localMap);
  MSG("cost images %d", (int) ((DGCgettime() - time) / 1000));
  
  // Construct the rotated integral images from the local map
  time = DGCgettime();
  this->createCostSumImages(localMap);
  MSG("sum images %d", (int) ((DGCgettime() - time) / 1000));
  
  if (false)
  {
    // TESTING MAGIC
    this->testPose(state->localX, state->localY, state->localYaw, -1, -1, +4, +1);
  }

  /* TESTING
     int i;
     uint64_t time = DGCgettime();
     for (i = 0; i < 100000; i++)
     this->testPose(lpose.pos.x, lpose.pos.y, state->localYaw, -1, -1, +3, +1);
     MSG("%d tests in %dms; %.3f test/msec",
     i, (int) ((DGCgettime() - time) / 1000), (double) i / (DGCgettime() - time) * 1000);
  */

  time = DGCgettime();  
  if (true)
  {
    int i;
    FusedMapPoint *point;    
    
    // Evaluate configurations in the ROI
    for (i = 0; i < this->numPoints; i++)
    {
      point = this->points + i;
      // TESTING MAGIC
      point->cost = this->testPose(point->px, point->py, point->ph, -1, -1, +4, +1);
    }
  }
  MSG("c-space %d", (int) ((DGCgettime() - time) / 1000));
  
  return 0;
}


// Update the ROI based on the current vehicle state
int FusedMap::updateROI(const VehicleState *state, const PriorMap *priorMap)
{
  int i;
  vec3_t pos;
  pose3_t transLV, transGV, transLP;
  float m[4][4];
  float mh;
  FusedMapPoint *point;

  // Get the center of the ROI in the prior map frame
  pos = vec3_set(state->utmNorthing, state->utmEasting, 0);
  pos = vec3_transform(pose3_inv(priorMap->pose), pos);

  // Suck points from the prior map quad-tree
  this->numPoints = 0;
  if (priorMap->mapRoot)
    this->addPointROI(priorMap->mapRoot, pos.x, pos.y, 40); // TESTING
  MSG("ROI %d points", this->numPoints);
  
  // Construct transform to local frame from vehicle frame.
  transLV.pos = vec3_set(state->localX, state->localY, state->localZ);
  transLV.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);

  // Construct transform to global frame from vehicle frame.
  transGV.pos = vec3_set(state->utmNorthing, state->utmEasting, state->utmAltitude);
  transGV.rot = quat_from_rpy(state->utmRoll, state->utmPitch, state->utmYaw);

  // Construct transform to local frame from prior map frame.
  transLP = pose3_mul(transLV, pose3_mul(pose3_inv(transGV), priorMap->pose));
  pose3_to_mat44f(transLP, m);
  mh = atan2(m[1][0], m[0][0]);

  // Fix up the point coordinates to be in the local frame.
  for (i = 0; i < this->numPoints; i++)
  {
    float px, py, ph;
    float qx, qy, qh;
    
    point = this->points + i;

    px = point->px;
    py = point->py;
    ph = point->ph;

    qx = m[0][0]*px + m[0][1]*py + m[0][3];
    qy = m[1][0]*px + m[1][1]*py + m[1][3];
    qh = mh + point->ph;

    // Normalize the angle 
    while (qh > +M_PI)
      qh -= 2 * M_PI;
    while (qh < -M_PI)
      qh += 2 * M_PI;
    
    point->px = qx;
    point->py = qy;
    point->ph = qh;    
  }

  return 0;
}


// Extract points from the map and add them to the ROI, starting from the given quad
int FusedMap::addPointROI(const PriorMapQuad *quad, float px, float py, float size)
{
  int i;
  PriorMapQuad *leaf;
  PriorMapPoint *src;
  FusedMapPoint *dst;
  
  // Check for overlap between this quad and the ROI
  if (px - size/2 > quad->px + quad->size/2)
    return 0;
  if (px + size/2 < quad->px - quad->size/2)
    return 0;
  if (py - size/2 > quad->py + quad->size/2)
    return 0;
  if (py + size/2 < quad->py - quad->size/2)
    return 0;

  // Let the leaves add points
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->addPointROI(leaf, px, py, size);
  }

  // Add our own points
  for (i = 0; i < quad->numPoints; i++)
  {
    if (this->numPoints >= this->maxPoints)
    {
      this->maxPoints += 2048;
      this->points = (FusedMapPoint*) realloc(this->points,
                                              this->maxPoints * sizeof(this->points[0]));
      assert(this->points);
    }

    src = quad->points + i;
    dst = this->points + this->numPoints++;

    // Make a copy of the point pose; we will modify these later to be
    // in the correct frame.
    memset(dst, 0, sizeof(*dst));
    dst->px = src->px;
    dst->py = src->py;
    dst->ph = src->ph;
  }
  
  return 0;
}

  
// Create the non-rotated cost image
int FusedMap::createCostImages(const LocalMap *localMap)
{
  int i, j;
  float *pix;
  const LocalMapCell *cell;

  // TODO: this should be limited to points in a circle,
  // so that we dont get weird planner artifacts arising
  // from c-space tests at different orientation bins.
  
  CvMat image = cvMat(localMap->size, localMap->size,
                      CV_32F, this->flatImages[0]->data);
  
  pix = (float*) cvPtr2D(&image, 0, 0);
  cell = localMap->getCellByIndex(0, 0);      
  for (j = 0; j < localMap->size; j++)
  {
    for (i = 0; i < localMap->size; i++, cell += 1, pix += 1)
    {
      pix[0] = 0;
      if (cell->ladarFlatCount > 0)
      {
        /*
        int k;        
        k = 2 * cell->ladarMono / cell->ladarCount; // MAGIC
        if (k > 0xFF) k = 0xFF;        
        pix[0] = (uint8_t) k;
        */
        float k;
        //k = 0x80 - cell->ladarFlat / cell->ladarCount * 500; // MAGIC
        //if (k < 0) k = 0;
        //if (k > 0xFF) k = 0xFF;        

        k = cell->ladarFlat / cell->ladarFlatCount;
        if (-k > 0.25)
          k = 1e6;
        else
          k = 0;
        
        pix[0] = (float) k;
      }
    }
  }

  if (false)
  {
    char filename[1024];
    snprintf(filename, sizeof(filename), "flat-00.pgm");
    MSG("writing %s", filename);
    cvSaveImage(filename, &image);
  }

  return 0;
}


// Create a set of integral rotated cost images
int FusedMap::createCostSumImages(const LocalMap *localMap)
{
  int i;
  float rotAngle;
  float m[2][3];
  float transIL[4][4], transIR[4][4], transRM[4][4];
  
  rotAngle = M_PI/2 / (this->numImages - 1);

  CvMat srcImage = cvMat(this->flatImages[0]->cols, this->flatImages[0]->rows,
                         CV_32F, this->flatImages[0]->data);
  
  // Create images at different rotation angles
  for (i = 0; i < this->numImages; i++)
  {    
    // Construct transform matrix for image rotation
    m[0][0] = +cos(i*rotAngle);  m[0][1] = -sin(i*rotAngle); m[0][2] = 0.5*srcImage.width;
    m[1][0] = +sin(i*rotAngle);  m[1][1] = +cos(i*rotAngle); m[1][2] = 0.5*srcImage.height;

    // Construct transform from map frame to rotated map frame
    mat44f_zero(transRM);
    transRM[0][0] = m[0][0];
    transRM[0][1] = -m[0][1];
    transRM[0][3] = 0; //m[0][2];
    transRM[1][0] = -m[1][0];
    transRM[1][1] = m[1][1];
    transRM[1][3] = 0; //m[1][2];
    transRM[2][2] = 1;
    transRM[3][3] = 1;

    // Construct transform from rotated map frame to image coordinates
    mat44f_zero(transIR);
    transIR[0][0] = 1.0 / localMap->scale;
    transIR[1][1] = 1.0 / localMap->scale;
    transIR[0][3] = m[0][2];
    transIR[1][3] = m[1][2];
    transIR[2][2] = 1;
    transIR[3][3] = 1;

    // Construc combined transform from local frame to image coordinates
    mat44f_mul(transIL, transRM, localMap->transML);
    mat44f_mul(transIL, transIR, transIL);
    
    this->transforms[i][0] = transIL[0][0];
    this->transforms[i][1] = transIL[0][1];
    this->transforms[i][2] = transIL[0][3];
    this->transforms[i][3] = transIL[1][0];
    this->transforms[i][4] = transIL[1][1];
    this->transforms[i][5] = transIL[1][3];
    this->scale = 1.0 / localMap->scale;

    // Create rotated image.  For the zero'th bin, we dont need to do
    // anything.
    CvMat dstImage = cvMat(this->flatImages[i]->cols, this->flatImages[i]->rows,
                           CV_32F, this->flatImages[i]->data);
    if (i > 0)
    {
      CvMat cvm = cvMat(2, 3, CV_32F, m);
      cvGetQuadrangleSubPix(&srcImage, &dstImage, &cvm);
    }
        
    // Create integral image
    CvMat sumImage = cvMat(this->flatSumImages[i]->cols, this->flatSumImages[i]->rows,
                           CV_64F, this->flatSumImages[i]->data);
    cvIntegral(&dstImage, &sumImage, NULL, NULL);
    
    if (false)
    {
      char filename[1024];
      snprintf(filename, sizeof(filename), "flat-%02d.pgm", i);
      MSG("writing %s", filename);
      cvSaveImage(filename, &dstImage);
    }
  }
  
  return 0;
}


// Compute the lane response at the given pose
float FusedMap::testPose(float px, float py, float ph,
                         float ax, float ay, float bx, float by)
{
  float qx, qy;
  int max, may, mbx, mby;
  float rotAngle;
  dgc_image_t *sumImage;
  float *m;
  double saa, sab, sba, sbb, sum;
  int bin;
  
  rotAngle = M_PI/2 / (this->numImages - 1);
    
  // Pick the bin the best aligns with the given orientation, and
  // compute the corners of the test rectangle in that orientation
  // image.  Since we only compute rotated images in the range 0 to
  // PI/2 inclusive, we may have to switch the corners around to get
  // the same part of the image.
  if (0 <= ph && ph < +M_PI/2)
  {
    bin = (int) (ph / rotAngle + 0.5);
    assert(bin >= 0 && bin < this->numImages);
    
    m = this->transforms[bin];    
    qx = m[0]*px + m[1]*py + m[2];
    qy = m[3]*px + m[4]*py + m[5];

    max = (int) (ax*this->scale + qx);
    may = (int) (ay*this->scale + qy);    
    mbx = (int) ((bx - ax)*this->scale + max);
    mby = (int) ((by - ay)*this->scale + may);
  }
  else if (0 <= ph + M_PI/2 && ph + M_PI/2 < M_PI/2)
  {
    ph += M_PI/2;

    bin = (int) (ph / rotAngle + 0.5);
    assert(bin >= 0 && bin < this->numImages);
    
    m = this->transforms[bin];    
    qx = m[0]*px + m[1]*py + m[2];
    qy = m[3]*px + m[4]*py + m[5];

    max = (int) (+ay*this->scale + qx);
    may = (int) (-bx*this->scale + qy);
    mbx = (int) ((by - ay)*this->scale + max);
    mby = (int) ((bx - ax)*this->scale + may);
  }
  else if (0 <= ph - M_PI/2 && ph - M_PI/2 < M_PI/2)
  {
    ph -= M_PI/2;

    bin = (int) (ph / rotAngle + 0.5);
    assert(bin >= 0 && bin < this->numImages);
    
    m = this->transforms[bin];    
    qx = m[0]*px + m[1]*py + m[2];
    qy = m[3]*px + m[4]*py + m[5];

    max = (int) (-by*this->scale + qx);
    may = (int) (+ax*this->scale + qy);
    mbx = (int) ((by - ay)*this->scale + max);
    mby = (int) ((bx - ax)*this->scale + may);
  }
  else if (0 <= ph + M_PI && ph + M_PI < M_PI/2)
  {
    ph += M_PI;

    bin = (int) (ph / rotAngle + 0.5);
    assert(bin >= 0 && bin < this->numImages);
    
    m = this->transforms[bin];    
    qx = m[0]*px + m[1]*py + m[2];
    qy = m[3]*px + m[4]*py + m[5];

    max = (int) (-bx*this->scale + qx);
    may = (int) (-by*this->scale + qy);
    mbx = (int) ((bx - ax)*this->scale + max);
    mby = (int) ((by - ay)*this->scale + may);
  }
  else
    assert(false);

  //MSG("%d : %f %f : %f %f %f %f -> %f %f : %d %d %d %d",
  //    bin, px, py, ax, ay, bx, by, qx, qy, max, may, mbx, mby);        

  sumImage = this->flatSumImages[bin];
    
  // Do a bounds check
  if (max < 0 || mbx >= sumImage->cols)
    return 0; // TODO
  if (may < 0 || mby >= sumImage->rows)
    return 0; // TODO

  if (false)
  {
    // Write test image
    dgc_image_t *flatImage;
    char filename[1024];
    static int frame;

    assert(max <= mbx);
    assert(may <= mby);
    
    flatImage = this->flatImages[bin];
    CvMat image = cvMat(flatImage->cols, flatImage->rows, CV_8UC1, flatImage->data);
    cvRectangle(&image, cvPoint(max, may), cvPoint(mbx, mby), cvScalar(0xFF, 0, 0));

    snprintf(filename, sizeof(filename), "test-%04d.pgm", frame++);
    MSG("writing %s", filename);
    cvSaveImage(filename, &image);      
  }

  // Compute the sum over the rectangle
  saa = *(double*) dgc_image_pixel(sumImage, max, may);
  sab = *(double*) dgc_image_pixel(sumImage, max, mby);
  sba = *(double*) dgc_image_pixel(sumImage, mbx, may);
  sbb = *(double*) dgc_image_pixel(sumImage, mbx, mby);
  sum = sbb - sab - sba + saa;

  // TESTING
  sum /= ((mbx - max + 1) * (mby - may + 1));
  
  return (float) sum;
}


#if USE_GL

// Predraw the fused map
int FusedMap::predrawMap(void)
{
  int i;
  FusedMapPoint *point;

  // Create display list
  if (this->mapList == 0)
    this->mapList = glGenLists(1);
  glNewList(this->mapList, GL_COMPILE);
  
  glBegin(GL_LINES);
  for (i = 0; i < this->numPoints; i++)
  {
    point = this->points + i;

    float k;
    k = (float) point->cost;
    if (k < 0) k = 0;
    if (k > 1) k = 1;
    glColor3f(k, 1 - k, 0);
    
    glVertex2f(point->px, point->py);
    glVertex2f(point->px + 0.20 * cos(point->ph), point->py + 0.20 * sin(point->ph));
  }
  glEnd();
    
  glEndList();

  return this->mapList;
}

#endif
