
/* 
 * Desc: Local cartesian map
 * Date: 16 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef LOCAL_MAP_H
#define LOCAL_MAP_H

#include <interfaces/VehicleState.h>
#include <frames/pose3.h>


/** @file

@brief Local cartesian map (data structure).

*/

  
/// @brief Map cell data
struct LocalMapCell
{
  // Has this cell been seen at least once by the ladar?  If not, the
  // ladar stats may contain values inferred from neighboring cells.
  int ladarSeen;
    
  // Ladar intensity (monochrome) moments up to second order.
  int ladarRawCount, ladarRaw1, ladarRaw2;

  // Ladar line filter response
  float ladarLineCount, ladarLine;

  // Ladar roughness filter response
  float ladarRoughCount, ladarRough; 
  
  // RGB color stats
  int stereoRawCount, stereoRaw[3];

  // Stereo line stats
  int stereoLineCount, stereoLine;
};
  

/// @brief Local map data structure
class LocalMap
{
  public:

  // Constructor
  LocalMap(int size, float scale);

  // Destructor
  virtual ~LocalMap();

  private:

  // Hide the copy constructor
  LocalMap(const LocalMap &that);

  public:
  
  /// @brief Clear the map and center it on the current local pose.
  int clear(const VehicleState *state);

  /// @brief Move the map center but keep the data
  int move(const VehicleState *state);
  
  public:

  /// @brief Transfrom from map position to map index
  void cvtPosToIndex(float px, float py, int *mi, int *mj)
    {
      *mi = (int) (px/this->scale + 10000000) - 10000000 + this->size/2;
      *mj = (int) (py/this->scale + 10000000) - 10000000 + this->size/2;
      return;
    }


  /// @brief Transfrom from map index to map position
  void cvtIndexToPos(int mi, int mj, float *px, float *py)
    {
      *px = (mi - this->size/2 + 0.5) * this->scale;
      *py = (mj - this->size/2 + 0.5) * this->scale;
      return;
    }

  // Get the cell at the given map index
  LocalMapCell *getCellByIndex(int mi, int mj) const
    {
      if (mi < 0 || mi >= this->size)
        return NULL;
      if (mj < 0 || mj >= this->size)
        return NULL;
      return this->cells + mi + mj * this->size;
    }

  // Get the cell at the given position (map frame)
  LocalMapCell *getCell(float px, float py)
    {
      int mi, mj;
      this->cvtPosToIndex(px, py, &mi, &mj);
      return this->getCellByIndex(mi, mj);
    }
  
  public:

  // Map size in each dimension
  int size;

  // Map scale (m/cell)
  float scale;

  // Map pose in local frame
  pose3_t pose;
  
  // Transform from local to map frame
  float transML[4][4];

  // Transform from map to local frame
  float transLM[4][4];

  // Map data
  LocalMapCell *cells;  
};


#endif
