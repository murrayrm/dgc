/* cmdline.h */

/* File autogenerated by gengetopt version 2.18  */

#ifndef CMDLINE_H
#define CMDLINE_H

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef CMDLINE_PARSER_PACKAGE
#define CMDLINE_PARSER_PACKAGE "sensviewer"
#endif

#ifndef CMDLINE_PARSER_VERSION
#define CMDLINE_PARSER_VERSION "0.0"
#endif

struct gengetopt_args_info
{
  const char *help_help; /* Print help and exit help description.  */
  const char *version_help; /* Print version and exit help description.  */
  char * spread_daemon_arg;	/* Spread daemon.  */
  char * spread_daemon_orig;	/* Spread daemon original value given at command line.  */
  const char *spread_daemon_help; /* Spread daemon help description.  */
  int skynet_key_arg;	/* Skynet key (default='0').  */
  char * skynet_key_orig;	/* Skynet key original value given at command line.  */
  const char *skynet_key_help; /* Skynet key help description.  */
  char * rndf_arg;	/* Load and display an RNDF file.  */
  char * rndf_orig;	/* Load and display an RNDF file original value given at command line.  */
  const char *rndf_help; /* Load and display an RNDF file help description.  */
  char * image_path_arg;	/* Path to aerial images (default='/dgc/aerial-images').  */
  char * image_path_orig;	/* Path to aerial images original value given at command line.  */
  const char *image_path_help; /* Path to aerial images help description.  */
  int ladar_scans_arg;	/* Number of scans to show (default='1').  */
  char * ladar_scans_orig;	/* Number of scans to show original value given at command line.  */
  const char *ladar_scans_help; /* Number of scans to show help description.  */
  double seek_arg;	/* Seek to the given timestmp (in seconds).  */
  char * seek_orig;	/* Seek to the given timestmp (in seconds) original value given at command line.  */
  const char *seek_help; /* Seek to the given timestmp (in seconds) help description.  */
  double skip_arg;	/* Skip to the given offset (in seconds) from the start of the log.  */
  char * skip_orig;	/* Skip to the given offset (in seconds) from the start of the log original value given at command line.  */
  const char *skip_help; /* Skip to the given offset (in seconds) from the start of the log help description.  */
  float utm_offset_northing_arg;	/* Apply an offset to the UTM/site coordinates (default='0').  */
  char * utm_offset_northing_orig;	/* Apply an offset to the UTM/site coordinates original value given at command line.  */
  const char *utm_offset_northing_help; /* Apply an offset to the UTM/site coordinates help description.  */
  float utm_offset_easting_arg;	/* Apply an offset to the UTM/site coordinates (default='0').  */
  char * utm_offset_easting_orig;	/* Apply an offset to the UTM/site coordinates original value given at command line.  */
  const char *utm_offset_easting_help; /* Apply an offset to the UTM/site coordinates help description.  */
  
  int help_given ;	/* Whether help was given.  */
  int version_given ;	/* Whether version was given.  */
  int spread_daemon_given ;	/* Whether spread-daemon was given.  */
  int skynet_key_given ;	/* Whether skynet-key was given.  */
  int rndf_given ;	/* Whether rndf was given.  */
  int image_path_given ;	/* Whether image-path was given.  */
  int ladar_scans_given ;	/* Whether ladar-scans was given.  */
  int seek_given ;	/* Whether seek was given.  */
  int skip_given ;	/* Whether skip was given.  */
  int utm_offset_northing_given ;	/* Whether utm-offset-northing was given.  */
  int utm_offset_easting_given ;	/* Whether utm-offset-easting was given.  */

  char **inputs ; /* unamed options */
  unsigned inputs_num ; /* unamed options number */
} ;

extern const char *gengetopt_args_info_purpose;
extern const char *gengetopt_args_info_usage;
extern const char *gengetopt_args_info_help[];

int cmdline_parser (int argc, char * const *argv,
  struct gengetopt_args_info *args_info);
int cmdline_parser2 (int argc, char * const *argv,
  struct gengetopt_args_info *args_info,
  int override, int initialize, int check_required);
int cmdline_parser_file_save(const char *filename,
  struct gengetopt_args_info *args_info);

void cmdline_parser_print_help(void);
void cmdline_parser_print_version(void);

void cmdline_parser_init (struct gengetopt_args_info *args_info);
void cmdline_parser_free (struct gengetopt_args_info *args_info);

int cmdline_parser_required (struct gengetopt_args_info *args_info,
  const char *prog_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* CMDLINE_H */
