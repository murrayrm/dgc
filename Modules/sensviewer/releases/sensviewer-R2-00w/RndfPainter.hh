 
/* 
 * Desc: Render RNDF data
 * Date: 16 Apr 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef RNDF_PAINTER_H
#define RNDF_PAINTER_H

#include <interfaces/VehicleState.h>
#include <rndf/RoadGraph.hh>

#ifdef USE_PLAN_GRAPH
#include <temp-planner-interfaces/PlanGraph.hh>
#endif


// Class for rendering prior map data and overhead images
class RndfPainter
{
  public:

  // Constructor
  RndfPainter();

  public:

  // Load RNDF file
  int load(char *filename);
  
  // Draw RNDF in local frame
  int draw(bool drawRNDF, bool drawRoad, bool drawPlan,
           VehicleState *state, float px, float py, float size);
  
  public:

  // Road graph object (interpolated RNDF graph)
  RoadGraph *road;

  // RNDF display list
  int rndfList;

#if USE_PLAN_GRAPH

  // Plan graph object (dense, feasable RNDF graph)
  PlanGraph *plan;

  // Plan graph display list
  int planList;
  
#endif  
};


#endif
