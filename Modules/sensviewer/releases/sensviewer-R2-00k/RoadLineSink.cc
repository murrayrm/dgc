 
/* 
 * Desc: Sink for road lines
 * Date: 16 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/gl.h>

#include <alice/AliceConstants.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>

#include "RoadLineSink.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
RoadLineSink::RoadLineSink(int menuId)
{
  memset(this, 0, sizeof(*this));

  this->menuId = menuId;
  this->sensorId = SENSNET_SKYNET_SENSOR;
  this->blobType = SNroadLine;
  this->blobId = 0;
  
  return;
}


// Enable/disable sink
int RoadLineSink::enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable)
{
  if (sensnet)
  {
    // If in live mode...
    if (enable && !this->enabled)
    {
      sensnet_join(sensnet, this->sensorId, this->blobType, sizeof(this->blob));
      this->enabled = true;
    }
    else if (!enable && this->enabled)
    {
      sensnet_leave(sensnet, this->sensorId, this->blobType);
      this->enabled = false;
    }
  }  
  else if (replay)
  {
    // If in replay mode
    this->enabled = enable;
  }
  return 0;
}


// Update with current sensnet data
int RoadLineSink::update(sensnet_t *sensnet, sensnet_replay_t *replay)
{
  int blobId;
      
  if (sensnet)
  {
    // Peek at the latest data, but dont read it out
    if (sensnet_peek(sensnet, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;

    // Is the data new?
    if (blobId < 0 || blobId == this->blobId)
      return 0;

    // Read the data
    if (sensnet_read(sensnet, this->sensorId, this->blobType,
                     &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;
  }
  else if (replay)
  {
    // Peek at the latest data, but dont read it out
    if (sensnet_replay_peek(replay, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;

    // Is the data new?
    if (blobId < 0 || blobId == this->blobId)
      return 0;

    // Read the data    
    if (sensnet_replay_read(replay, this->sensorId, this->blobType,
                            &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;    
  }
  
  return 0;
}


// Generate lines (local frame)
void RoadLineSink::predrawLines()
{
  int i;
  
  if (this->blobId < 0)
    return;
  
  if (this->lineList == 0)
    this->lineList = glGenLists(1);
  glNewList(this->lineList, GL_COMPILE);

  // TODO better line rendering
  
  // Draw lines 
  glColor3f(1, 0, 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glLineWidth(10);
  glBegin(GL_LINES);  
  for (i = 0; i < blob.num_lines; i++)
  {
    glVertex3f(blob.lines[i].a[0], blob.lines[i].a[1], blob.lines[i].a[2]);
    glVertex3f(blob.lines[i].b[0], blob.lines[i].b[1], blob.lines[i].b[2]);    
  }
  glEnd();
  glLineWidth(1);
  
  glEndList();
  
  return;
}

