
/* 
 * Desc: Window for displaying 3D stuff
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <stdio.h>
#include <GL/glut.h>
#include <jplv/jplv_image.h>

#include <alice/AliceConstants.h>
#include <skynet/sn_types.h>
#include <interfaces/SensnetTypes.h>

#include "WorldWin.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Menu commands
enum
{
  // TODO SOURCE_ROAD_LINE = 0x8000
  // SOURCE_OBS_MAP,
  
  VIEW_VEHICLE = 0x1000,
  VIEW_LOCAL,
  VIEW_FOOTPRINT,
  
  ACTION_CAPTURE
};


// Set up the menu
static Fl_Menu_Item menuitems[] =
{
  {"&Source", 0, 0, 0, FL_SUBMENU},    
  {"LF Short Stereo", 0, (Fl_Callback*) WorldWin::onSensor,
   (void*) SENSNET_LF_SHORT_STEREO, FL_MENU_TOGGLE},
  {"RF Short Stereo", 0, (Fl_Callback*) WorldWin::onSensor,
   (void*) SENSNET_RF_SHORT_STEREO, FL_MENU_TOGGLE | FL_MENU_DIVIDER},
  
  {"MF Bumper Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
   (void*) SENSNET_MF_BUMPER_LADAR, FL_MENU_TOGGLE},
  {"LF Bumper Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
   (void*) SENSNET_LF_BUMPER_LADAR, FL_MENU_TOGGLE},
  {"RF Bumper Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
   (void*) SENSNET_RF_BUMPER_LADAR, FL_MENU_TOGGLE},
  
  {"LF Roof Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
   (void*) SENSNET_LF_ROOF_LADAR, FL_MENU_TOGGLE},
  {"RF Roof Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
   (void*) SENSNET_RF_ROOF_LADAR, FL_MENU_TOGGLE},
  {"MF Roof Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
   (void*) SENSNET_MF_ROOF_LADAR, FL_MENU_TOGGLE | FL_MENU_DIVIDER},  

  // TODO: need both sensor id and blob type in menu
  {"Elevation Map", 0, (Fl_Callback*) WorldWin::onSensor,
   (void*) SENSNET_SKYNET_SENSOR, FL_MENU_TOGGLE | FL_MENU_VALUE},
  {"RoadLine", 0, (Fl_Callback*) WorldWin::onSensor,
   (void*) SENSNET_SKYNET_SENSOR, FL_MENU_TOGGLE},

  {0},
  
  {"&View", 0, 0, 0, FL_SUBMENU},    
  {"Vehicle frame", 0, (Fl_Callback*) WorldWin::onView,
   (void*) VIEW_VEHICLE, FL_MENU_RADIO | FL_MENU_VALUE},
  {"Local frame", 0, (Fl_Callback*) WorldWin::onView,
   (void*) VIEW_LOCAL, FL_MENU_RADIO | FL_MENU_DIVIDER},
  {"Footprints", 0, (Fl_Callback*) WorldWin::onView,
   (void*) VIEW_FOOTPRINT, FL_MENU_TOGGLE},
  {0},

  {"&Action", 0, 0, 0, FL_SUBMENU},    
  {"Capture", 0, NULL,
   (void*) ACTION_CAPTURE, FL_MENU_TOGGLE},
  {0},
  
  {0},
};


// Constructor
WorldWin::WorldWin(int x, int y, int w, int h, int menuh)
    : Fl_Window(x, y, w, h + menuh, NULL)
{
  begin();
  
  this->menubar = new Fl_Menu_Bar(0, 0, w, menuh);
  this->menubar->user_data(this);
  this->menubar->menu(::menuitems);

  this->glwin = new Fl_Glv_Window(0, menuh, w, h, this, (Fl_Callback*) onDraw);
  this->glwin->user_data(this);
  this->resizable(this->glwin);

  end();

  // Use DGC convention
  this->glwin->set_up("-z");
  
  // Things are both near and far away, so set a generous clipping range
  this->glwin->set_clip(1, 500);

  // Set initial camera position
  this->glwin->eye.x = -20;
  this->glwin->eye.y = -20;
  this->glwin->eye.z = -20;

  if (true)
  {
    // Create stereo sinks
    StereoSink *sink;
    this->numStereoSinks = 0;
    sink = this->stereoSinks + this->numStereoSinks++;
    sink->sensorId = SENSNET_LF_SHORT_STEREO;
    sink->blobType = SENSNET_STEREO_BLOB;
    sink = this->stereoSinks + this->numStereoSinks++;
    sink->sensorId = SENSNET_RF_SHORT_STEREO;
    sink->blobType = SENSNET_STEREO_BLOB;
  }

  if (true)
  {
    // Create ladar sinks
    LadarSink *sink;
    this->numLadarSinks = 0;
    sink = this->ladarSinks + this->numLadarSinks++;
    sink->sensorId = SENSNET_MF_BUMPER_LADAR;
    sink->blobType = SENSNET_LADAR_BLOB;
    sink = this->ladarSinks + this->numLadarSinks++;
    sink->sensorId = SENSNET_LF_BUMPER_LADAR;
    sink->blobType = SENSNET_LADAR_BLOB;
    sink = this->ladarSinks + this->numLadarSinks++;
    sink->sensorId = SENSNET_RF_BUMPER_LADAR;
    sink->blobType = SENSNET_LADAR_BLOB;
    sink = this->ladarSinks + this->numLadarSinks++;
    sink->sensorId = SENSNET_LF_ROOF_LADAR;
    sink->blobType = SENSNET_LADAR_BLOB;
    sink = this->ladarSinks + this->numLadarSinks++;
    sink->sensorId = SENSNET_RF_ROOF_LADAR;
    sink->blobType = SENSNET_LADAR_BLOB;
    sink = this->ladarSinks + this->numLadarSinks++;
    sink->sensorId = SENSNET_MF_ROOF_LADAR;
    sink->blobType = SENSNET_LADAR_BLOB;
  }

  if (true)
  {
    // Create road line sink
    // TODO
  }
  
  
  // Hook up menu options
  int i;
  const Fl_Menu_Item *item;
  for (i = 0; i < this->menubar->menu()->size(); i++)
  {
    item = this->menubar->menu() + i;
    if (item->user_data() == (void*) VIEW_VEHICLE)
      this->view_vehicle = item;
    if (item->user_data() == (void*) VIEW_LOCAL)
      this->view_local = item;
    if (item->user_data() == (void*) VIEW_FOOTPRINT)
      this->view_footprint = item;
    if (item->user_data() == (void*) ACTION_CAPTURE)
      this->action_capture = item;
  }
  
  this->aliceList = this->gridList = 0;

  return;
}


// Get the sensor menu state (enabled/disabled)
bool WorldWin::getSensorState(int sensorId)
{
  int i;
  const Fl_Menu_Item *item;    

  for (i = 0; i < (int) (sizeof(::menuitems) / sizeof(::menuitems[0])); i++)
  {
    item = ::menuitems + i;
    if (item->user_data() == (void*) sensorId)
      return (bool) item->value();
  }
  
  return false;
}


// Set the sensor menu state (enabled/disabled)
void WorldWin::setSensorState(int sensorId, bool enable)
{
  int i;
  Fl_Menu_Item *item;    

  for (i = 0; i < (int) (sizeof(::menuitems) / sizeof(::menuitems[0])); i++)
  {
    item = ::menuitems + i;
    if (item->user_data() == (void*) sensorId)
    {
      if (enable)
        item->set();
      else
        item->clear();
    }
  }
  
  return;
}


// Initialize for sensnet
int WorldWin::initSensnet()
{
  int i;
  
  for (i = 0; i < this->numLadarSinks; i++)
  {
    LadarSink *sink;
    sink = this->ladarSinks + i;
    this->setSensorState(sink->sensorId, true);
  }

  return 0;
}


// Update blob data using sensnet
int WorldWin::updateSensnet(sensnet_t *sensnet)
{
  int i;
  int blobId, blobLen;
  bool dirty;

  dirty = false;
  
  // Loop over stereo sinks for new stereo data
  for (i = 0; i < this->numStereoSinks; i++)
  {
    StereoSink *sink = this->stereoSinks + i;

    // Join or leave as necessary
    if (this->getSensorState(sink->sensorId) == true && !sink->enable)
    {
      sensnet_join(sensnet, sink->sensorId, sink->blobType, sizeof(sink->blob), 10);
      sink->enable = true;
    }
    else if (this->getSensorState(sink->sensorId) == false && sink->enable)
    {
      sensnet_leave(sensnet, sink->sensorId, sink->blobType);
      sink->enable = false;
    }

    if (!sink->enable)
      continue;

    // TODO move update to sink
    
    // Look at the latest data
    if (sensnet_peek(sensnet, sink->sensorId, sink->blobType, &blobId, &blobLen) != 0)
      return -1;

    // Is the data valid?
    if (blobId < 0)
      continue;

    // Is the data new?
    if (blobId == sink->blobId)
      continue;

    // Read the data
    if (sensnet_read(sensnet, sink->sensorId, sink->blobType,
                     blobId, sizeof(sink->blob), &sink->blob) != 0)
      return -1;

    sink->blobId = blobId;
    sink->dirty = true;
    dirty = true;

    // See if this is a new state message
    if (sink->blob.state.timestamp > this->state.timestamp)
      this->state = sink->blob.state;
  }

  // Loop over ladar sinks
  for (i = 0; i < this->numLadarSinks; i++)
  {
    LadarSink *sink = this->ladarSinks + i;

    // Join or leave as necessary
    if (this->getSensorState(sink->sensorId) == true && !sink->enable)
    {
      sensnet_join(sensnet, sink->sensorId, sink->blobType, sizeof(sink->blob), 1000);
      sink->enable = true;
    }
    else if (this->getSensorState(sink->sensorId) == false && sink->enable)
    {
      sensnet_leave(sensnet, sink->sensorId, sink->blobType);
      sink->enable = false;
    }
    
    if (!sink->enable)
      continue;

    // Read data from sensnet
    sink->updateSensnet(sensnet);
    if (sink->dirty)
      dirty = true;
    
    // See if this is a new state message
    if (sink->blob.state.timestamp > this->state.timestamp)
      this->state = sink->blob.state;
  }

  // Consider road line sink
  if (true) 
  {
    RoadLineSink *sink = &this->roadLineSink;

    // Join or leave as necessary
    if (this->getSensorState(sink->sensorId) == true && !sink->enable)
    {
      sensnet_join(sensnet, sink->sensorId, sink->blobType, sizeof(sink->blob), 100);
      sink->enable = true;
    }
    else if (this->getSensorState(sink->sensorId) == false && sink->enable)
    {
      sensnet_leave(sensnet, sink->sensorId, sink->blobType);
      sink->enable = false;
    }
    
    if (sink->enable)
    {
      // Read data from sensnet
      sink->updateSensnet(sensnet);
      if (sink->dirty)
        dirty = true;

      // See if this is a new state message
      if (sink->blob.state.timestamp > this->state.timestamp)
        this->state = sink->blob.state;
    }
  }

  if (true)
  {
    // Create obsMap sink
    ObsMapSink *sink = &this->obsMapSink;
    
    // Join or leave as necessary
    if (this->getSensorState(sink->sensorId) == true && !sink->enable)
    {      
      sensnet_join(sensnet, sink->sensorId, sink->blobType, sizeof(sink->blob), 100);
      sink->enable = true;
    }
    else if (this->getSensorState(sink->sensorId) == false && sink->enable)
    {
      sensnet_leave(sensnet, sink->sensorId, sink->blobType);
      sink->enable = false;
    }
    
    if (sink->enable)
    {
      // Read data from sensnet
      sink->updateSensnet(sensnet);
      if (sink->dirty)
        dirty = true;

      // See if this is a new state message
      if (sink->blob.state.timestamp > this->state.timestamp)
        this->state = sink->blob.state;
    }

  }

  
  if (dirty)
    this->glwin->redraw();
  
  return 0;
}


// Handle menu callbacks
void WorldWin::onSensor(Fl_Widget *w, int option)
{
  //WorldWin *self = (WorldWin*) w->user_data();
  
  return;
}


// Handle menu callbacks
void WorldWin::onView(Fl_Widget *w, int option)
{
  int i;
  WorldWin *self = (WorldWin*) w->user_data();
  pose3_t pose;
  vec3_t eye, at;

  // Default camera viewpoint
  eye = vec3_set(-5, 0, -5);
  at = vec3_set(+10, 0, 0);
  
  if (option == VIEW_VEHICLE)
  {
    self->glwin->set_lookat(eye.x, eye.y, eye.z, at.x, at.y, at.z, 0, 0, -1);
  }
  if (option == VIEW_LOCAL)
  {
    pose.pos = vec3_set(self->state.localX, self->state.localY, self->state.localZ);
    pose.rot = quat_from_rpy(self->state.localRoll, self->state.localPitch, self->state.localYaw);
    eye = vec3_transform(pose, eye);
    at = vec3_transform(pose, at);
    self->glwin->set_lookat(eye.x, eye.y, eye.z, at.x, at.y, at.z, 0, 0, -1);    
  }
  
  // Redraw everything
  for (i = 0; i < self->numStereoSinks; i++)
  {
    StereoSink *sink = self->stereoSinks + i;
    sink->dirty = true;
  }  
  for (i = 0; i < self->numLadarSinks; i++)
  {
    LadarSink *sink = self->ladarSinks + i;
    sink->dirty = true;
  }  
  self->glwin->redraw();
  
  return;
}


// Draw window
void WorldWin::onDraw(Fl_Glv_Window *win, WorldWin *self)
{
  int i, j;
  pose3_t pose_local, pose_stable;

  // Predraw static stuff
  if (self->aliceList == 0)
    self->predrawAlice();
  if (self->gridList == 0)
    self->predrawGrid();
  
  // Predraw stereo stuff
  for (i = 0; i < self->numStereoSinks; i++)
  {
    StereoSink *sink = self->stereoSinks + i;
    if (sink->dirty && sink->enable)
    {
      sink->predrawPointCloud();
      sink->predrawFootprint();
      sink->dirty = false;
    }
  }

  // Predraw ladar stuff
  for (i = 0; i < self->numLadarSinks; i++)
  {
    LadarSink *sink = self->ladarSinks + i;
    if (sink->dirty && sink->enable)
    {
      sink->predrawPointCloud();
      sink->predrawFootprint();
      sink->dirty = false;
    }
  }

  // Predraw perceptor stuff
  self->roadLineSink.predrawLines();

  //Predraw elevation map
  self->obsMapSink.predrawCells();

  // Vehicle pose in local frame
  pose_local.pos = vec3_set(self->state.localX, self->state.localY, self->state.localZ);
  pose_local.rot = quat_from_rpy(self->state.localRoll, self->state.localPitch, self->state.localYaw);

  // Vehicle pose in stabilized local frame
  pose_stable.pos = vec3_set(self->state.localX, self->state.localY, self->state.localZ);
  pose_stable.rot = quat_from_rpy(0, 0, self->state.localYaw);

  // Select display frame
  if (self->view_vehicle->value())
    self->pushFrame(pose3_inv(pose_stable));

  // REMOVE
  //glClearColor(0.5, 0.5, 1.0, 1.0);
  //glClear(GL_COLOR_BUFFER_BIT);

  // REMOVE
  // Draw sky
  if (false)
  {
    GLUquadricObj *quad;

    glPushMatrix();
    glTranslatef(self->glwin->eye.x, self->glwin->eye.y, self->glwin->eye.z);

    quad = gluNewQuadric();
    glColor3f(0.5, 0.5, 1.0);
    gluSphere(quad, 99.0, 128, 64); // MAGIC
    gluDeleteQuadric(quad);
    
    glPopMatrix();
  }

  // Draw Alice (vehicle frame)
  if (true)
  {
    self->pushFrameLocal(self->state);
    glCallList(self->aliceList);
    self->popFrame();
  }

  if (false)
  {
    int i;
    float d;
    char text[64];
    GLUquadric *quad;

    self->pushFrameLocal(self->state);

    // Show range rings (in vehicle frame)
    for (i = 1; i <= 8; i++)
    {
      d = 10 * i;
      glLineWidth(1);
      glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glColor3f(1, 1, 1);  
      quad = gluNewQuadric();
      gluDisk(quad, d, d, 128, 2);
      gluDeleteQuadric(quad);
      gl_font(FL_HELVETICA, 12);
      snprintf(text, sizeof(text), "%.0fm", d);
      gl_draw(text, d + 0.5f, 0.5f /*d + 0.5f*/);
    }
    self->popFrame();
  }

  // Draw opaque stereo stuff (in sensor frame)
  for (i = 0; i < self->numStereoSinks; i++)
  {
    StereoSink *sink = self->stereoSinks + i;
    if (!sink->enable)
      continue;

    self->pushFrameLocal(sink->blob.state);
    self->pushFrame(sink->blob.sens2veh);

    self->drawAxes(0.50);
    glCallList(sink->cloudList);

    self->popFrame();
    self->popFrame();
  }

  // Draw opaque ladar stuff (in sensor frame)
  for (i = 0; i < self->numLadarSinks; i++)
  {
    LadarSink *sink = self->ladarSinks + i;
    if (!sink->enable)
      continue;

    self->pushFrameLocal(sink->blob.state);
    self->pushFrame(sink->blob.sens2veh);
    self->drawAxes(0.50);
    self->popFrame();
    self->popFrame();

    //self->pushFrame(sink->blob.state.pose_local);
    // TESTING self->pushFrame(sink->blob.sens2veh);
    for (j = 0; j < sink->maxClouds; j++)
      glCallList(sink->cloudList + j);
    // TESTING self->popFrame();
    //self->popFrame();
  }

  if (self->roadLineSink.enable)
  {
    // Draw lines (local frame)
    glCallList(self->roadLineSink.lineList);
  }

  if (self->obsMapSink.enable)
  {
    // Draw obstacle map (local frame)
    glCallList(self->obsMapSink.cellList);
  }

  // Draw translucent grid
  if (true)
  {
    glPushMatrix();
    glTranslatef(20 * (int) (self->state.localX/20),
                 20 * (int) (self->state.localY/20),
                 (self->state.localZ + VEHICLE_TIRE_RADIUS));
    glCallList(self->gridList);
    glPopMatrix();
  }

  // Draw translucent stereo stuff (in camera frame)
  for (i = 0; i < self->numStereoSinks; i++)
  {
    StereoSink *sink = self->stereoSinks + i;
    if (!sink->enable)
      continue;
    
    self->pushFrameLocal(sink->blob.state);
    self->pushFrame(sink->blob.sens2veh);

    if (self->view_footprint->value())
      glCallList(sink->footList);  

    self->popFrame();
    self->popFrame();
  }

  // Draw translucent ladar stuff (in sensor frame)
  for (i = 0; i < self->numLadarSinks; i++)
  {
    LadarSink *sink = self->ladarSinks + i;
    if (!sink->enable)
      continue;
    
    self->pushFrameLocal(sink->blob.state);
    self->pushFrame(sink->blob.sens2veh);

    if (self->view_footprint->value())
      glCallList(sink->footList);  

    self->popFrame();
    self->popFrame();
  }

  if (self->view_vehicle->value())
    self->popFrame();

  // Screen cap to file
  if (self->action_capture->value())
  {
    jplv_image_t *image;
    char filename[1024];
    int vp[4];

    // TESTING
    if (self->capCount % 10 == 0)
    {
      // Get a subwindow 4:3 aspect ratio
      glGetIntegerv(GL_VIEWPORT, vp);

      // HACK why?
      vp[1] += 50;
      vp[3] -= 50; 

      /*
      while (vp[2] * 3 > vp[3] * 4)
      {
        vp[0] += 1;
        vp[2] -= 2;
      }
      */

      MSG("%d %d %d %d %f", vp[0], vp[1], vp[2], vp[3], (float) vp[2] / (float) vp[3]);

      image = jplv_image_alloc(vp[2], vp[3], 3, 8, 0, NULL);
      glReadPixels(vp[0], vp[1], vp[2], vp[3], GL_RGB, GL_UNSIGNED_BYTE, image->data);
      snprintf(filename, sizeof(filename), "world-%04d.pnm", self->capCount / 10);
      MSG("writing %s", filename);
      jplv_image_write_pnm(image, filename, NULL);
      jplv_image_free(image);
    }

    self->capCount++;
  }
  
  return;
}


// Switch to given frame (homogeneous transform)
void WorldWin::pushFrame(float m[4][4])
{
  int i, j;
  float t[16];

  // Transpose to column-major order for GL
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);

  return;
}


// Switch to the given frame.
void WorldWin::pushFrame(pose3_t pose)
{
  float m[4][4];
  pose3_to_mat44f(pose, m);  
  return pushFrame(m);
}


// Switch to the given local frame.
void WorldWin::pushFrameLocal(VehicleState state)
{
  pose3_t pose;
  pose.pos = vec3_set(state.localX, state.localY, state.localZ);
  pose.rot = quat_from_rpy(state.localRoll, state.localPitch, state.localYaw);
  return pushFrame(pose);
}


// Revert to previous frame
void WorldWin::popFrame()
{
  glPopMatrix();  
  return;
}


// Draw a set of axes
void WorldWin::drawAxes(double size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Predraw the robot
void WorldWin::predrawAlice()
{
  // Generate display list 
  if (this->aliceList == 0)
    this->aliceList = glGenLists(1);
  glNewList(this->aliceList, GL_COMPILE);

  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  glEndList();
    
  return;
}


// Predraw a ground grid
void WorldWin::predrawGrid()
{
  int i, j;

  // Generate display list 
  if (this->gridList == 0)
    this->gridList = glGenLists(1);
  glNewList(this->gridList, GL_COMPILE);

  glPushMatrix();

  glScalef(10, 10, 10);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  glBegin(GL_QUADS);
  for (j = -10; j < +10; j++)
  {
    for (i = -10; i < +10; i++)
    {
      //glColor4f((i + j + 100) % 2, (i + j + 100) % 2, (i + j + 100) % 2, 0.2);
      glColor3f(0.5, 0.5, 0.5);
      glVertex3f(i + 0, j + 0, 0);
      glVertex3f(i + 1, j + 0, 0);
      glVertex3f(i + 1, j + 1, 0);
      glVertex3f(i + 0, j + 1, 0);
    }
  }
  glEnd();

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPolygonMode(GL_BACK, GL_FILL);
  glBegin(GL_QUADS);
  for (j = -10; j < +10; j++)
  {
    for (i = -10; i < +10; i++)
    {
      glColor4f((i + j + 100) % 2, (i + j + 100) % 2, (i + j + 100) % 2, 0.2);
      //glColor3f(0.5, 0.5, 0.5);
      glVertex3f(i + 0, j + 0, 0);
      glVertex3f(i + 1, j + 0, 0);
      glVertex3f(i + 1, j + 1, 0);
      glVertex3f(i + 0, j + 1, 0);
    }
  }
  glEnd();
  glDisable(GL_BLEND);
  
  glPopMatrix();

  glEndList();

  return;
}

