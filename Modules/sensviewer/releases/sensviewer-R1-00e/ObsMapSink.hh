
/* 
 * Desc: Sink for Obstacle Maps
 * Date: 7 Feb 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/
#ifndef OBSMAP_SINK_H
#define OBSMAP_SINK_H

#include "sensnet/sensnet.h"
#include "interfaces/ObsMapMsg.h"
#include "cmap/CMapPlus.hh"


#define NUM_ROWS  500
#define NUM_COLS  500
#define RES_ROWS  0.40
#define RES_COLS  0.40

// Display data for ladar blobs
class ObsMapSink 
{ 
  public: 

  // Constructor
  ObsMapSink();

  public:

  // Update with current sensnet data
  int updateSensnet(sensnet_t *sensnet);
  
  public:

  // draw elevation blocks for all cells
  void predrawCells();
    
  public:

  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId; 

  // Blob data buffer 
  ObsMapMsg blob;

  // Map
  CMap blobMap;
  int blobMapID_meanElevation;
  int blobMapID_stdDev;
  int blobMapID_cellType;

  // Rainbow color scale
  uint8_t rainbow[0x10000][3];
  
  // Are we enabled? 
  bool enable; 

  // Set flag if the data needs predrawing 
  bool dirty; 
  
  // GL Drawing lists
  int cellList;
};

#endif

