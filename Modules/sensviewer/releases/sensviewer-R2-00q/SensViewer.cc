
/* 
 * Desc: Sensor viewer utility
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <GL/glut.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <jplv/Fl_Glv_Window.H>
#include <jplv/jplv_image.h>

#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>

#include "cmdline.h"
#include "WorldWin.hh"


// Application 
class App
{
  public:

  // Default constructor
  App();

  // Destructor
  ~App();
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  // Initialize for reading from SensNet
  int initSensnet();
  
  // Finalize stiff
  int sensnetFini();

  // Initialize GUI
  int init(int cols, int rows);

  // Finalize styff
  int fini();

  public:

  // Action callback
  static void onAction(Fl_Widget *w, int option);

  // Speed callback
  static void onSpeed(Fl_Widget *w, int option);
  
  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  public:
  
  // Command-line options
  struct gengetopt_args_info options;
  
  // Path to GIS map data
  char *gisPath;

  // Top-level window
  Fl_Window *mainwin;
  
  // Top-level menu
  Fl_Menu_Bar *menubar;

  // 3D window
  WorldWin *worldwin;

  // What mode are we in?
  enum {modeLive, modeReplay} mode;

  // What is our replay speed
  int replaySpeed;

  // Last time the idle function was called
  uint64_t idleTime;

  // Current seek time in the log
  uint64_t seekTime;

  // Should we pause?
  bool pause;

  // Should we advance a single step?
  bool step;
  
  // Should we quit?
  bool quit;

  // Spread settings
  char *spreadDaemon; int skynetKey;

  // SensNet handle
  sensnet_t *sensnet;

  // Replay-log replay handle
  sensnet_replay_t *replay;

} app_t;


// Commands
enum
{
  APP_ACTION_PAUSE = 0x1000,
  APP_ACTION_STEP,
};


// Error handling
#define MSG(fmt, ...) \
  fprintf(stdout, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
App::App()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
App::~App()
{
  return;
}


// Parse command-line options
int App::parseCmdLine(int argc, char **argv)
{
  // Run parser
  if (cmdline_parser(argc, argv, &this->options) != 0)
  {
    cmdline_parser_print_help();
    return -1;
  }

  // See which mode we are running in (live or replay)
  if (this->options.inputs_num == 0)
    this->mode = modeLive;
  else
    this->mode = modeReplay;
 
  // Find the config path
  this->gisPath = dgcFindConfigFile("gistools", ".");
  MSG("gis path %s", this->gisPath);
    
  /* TESTING REMOVE
  // Fill out the gis map path
  if (getenv("DGC_CONFIG_PATH"))
  snprintf(this->gisPath, sizeof(this->gisPath),
  "%s/gistools", getenv("DGC_CONFIG_PATH"));
  else
  MSG("please set DGC_CONFIG_PATH to enable prior map painting");
  */

  if (this->mode == modeLive)
  {
    // Fill out the spread name
    if (this->options.spread_daemon_given)
      this->spreadDaemon = this->options.spread_daemon_arg;
    else if (getenv("SPREAD_DAEMON"))
      this->spreadDaemon = getenv("SPREAD_DAEMON");
    else
      return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
    // Fill out the skynet key
    if (this->options.skynet_key_given)
      this->skynetKey = this->options.skynet_key_arg;
    else if (getenv("SKYNET_KEY"))
      this->skynetKey = atoi(getenv("SKYNET_KEY"));
    else
      this->skynetKey = 0;
  }

  return 0;
}


// Initialize for reading from SensNet
int App::initSensnet()
{
  if (this->mode == modeLive)
  {
    // TESTING HACK
    // Need to add sensviewer module id.
    int SNsensviewer = 0xFF;

    // Open for live display
    this->sensnet = sensnet_alloc();
    assert(this->sensnet);  
    if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, SNsensviewer) != 0)
      return ERROR("unable to connect to sensnet");
  }
  else
  {
    // Open for replay
    this->replay = sensnet_replay_alloc();
    assert(this->replay);
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs) != 0)
      return ERROR("unable to open one or more log files");

    // Get the first entry in the log
    if (sensnet_replay_next(this->replay, 0) != 0)
      return ERROR("step failed");
    if (sensnet_replay_query(this->replay, &this->seekTime, NULL, NULL) != 0)
      return ERROR("query failed");

    // Seek to a relative or absolute time
    if (this->options.skip_given)
      this->seekTime += (uint64_t) (this->options.skip_arg * 1e6);
    else if (this->options.seek_given)
      this->seekTime = (uint64_t) (this->options.seek_arg * 1e6);

    // Seek to start frame
    if (sensnet_replay_seek(this->replay, this->seekTime) != 0)
      return ERROR("seek to %.3f failed", (double) this->seekTime * 1e-6);      
    if (sensnet_replay_query(this->replay, &this->seekTime, NULL, NULL) != 0)
      return ERROR("query failed");
  }

  // Initialize the world window
  this->worldwin->init(&this->options, this->sensnet, this->replay,
                       this->gisPath, this->options.rndf_arg);
   
  return 0; 
}


// Finalize stuff
int App::sensnetFini()
{
  if (this->mode == modeLive)
  {
    sensnet_disconnect(this->sensnet);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
  else
  {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Initialize stuff
int App::init(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) App::onExit},
      {0},
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) App::onAction, (void*) APP_ACTION_PAUSE, FL_MENU_TOGGLE},
      {"Single step", '.', (Fl_Callback*) App::onAction, (void*) APP_ACTION_STEP},
      {"Speed x0", '0', (Fl_Callback*) App::onSpeed, (void*) 0, FL_MENU_RADIO | FL_MENU_VALUE},
      {"Speed x1", '1', (Fl_Callback*) App::onSpeed, (void*) 1, FL_MENU_RADIO},
      {"Speed x2", '2', (Fl_Callback*) App::onSpeed, (void*) 2, FL_MENU_RADIO},  
      {"Speed x5", '5', (Fl_Callback*) App::onSpeed, (void*) 5, FL_MENU_RADIO},
      {0},
      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Sensor Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new WorldWin(0, 30, cols, rows - 30, 30);

  this->mainwin->end();

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  // Set default replay speed such that we step through each record
  this->replaySpeed = 0;
   
  return 0;
}


// Finalize styff
int App::fini()
{
  return 0;
}


// Handle menu callbacks
void App::onAction(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data();
  if (option == APP_ACTION_PAUSE)
    self->pause = !self->pause;
  if (option == APP_ACTION_STEP)
    self->step = true;
  
  return;
}



// Handle menu callbacks
void App::onSpeed(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data(); 
  self->replaySpeed = option;
  
  return;
}


// Handle menu callbacks
void App::onExit(Fl_Widget *w, int option)
{
  App *self;

  self = (App*) w->user_data();
  self->quit = true;

  return;
}


// Handle idle callbacks
void main_idle(App *self)
{
  bool dirty;
  uint64_t stepTime;
  uint64_t replayTime;
        
  dirty = false;

  // Compute how long since the last idle message
  if (self->idleTime == 0)
    stepTime = 0;
  else
    stepTime = DGCgettime() - self->idleTime;
  self->idleTime = DGCgettime();
  
  if (!self->pause || self->step)
  {  
    if (self->mode == App::modeLive)
    {
      // Wait some time for new data
      sensnet_wait(self->sensnet, 100);
      dirty = true;
    }
    else
    {      
      if (self->replaySpeed == 0 || self->step)
      {
        // Get the next record in the log
        sensnet_replay_next(self->replay, 0);
        sensnet_replay_query(self->replay, &self->seekTime, NULL, NULL);
        dirty = true;
      }
      else
      {
        // Step through the data at some fixed rate by comparing
        // the current time in the log with our internal seek time.        
        self->seekTime += stepTime * self->replaySpeed;
        sensnet_replay_query(self->replay, &replayTime, NULL, NULL);
        if (self->seekTime > replayTime)
        {
          sensnet_replay_next(self->replay, self->seekTime - replayTime);
          dirty = true;
        }
        else
        {
          usleep(0);
        }
      }
    }

    // Update the world window
    if (dirty)
      self->worldwin->update();

    // Single step mode: we've moved one step, so clear flag
    if (self->step)
      self->step = false;
  }
  else
  {
    // Sleepy bye-bye
    usleep(0);
  }
          
  return;
}


// Main loop
int main(int argc, char *argv[])
{
  App *app;
  
  // Initialize GLUT calls
  glutInit(&argc, argv);
  
  app = new App();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  if (app->init(1024, 768) != 0)
    return -1;
  
  // Initialize with appropriate mode
  if (app->initSensnet() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) main_idle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
