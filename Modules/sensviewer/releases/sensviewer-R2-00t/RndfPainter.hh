 
/* 
 * Desc: Render RNDF data
 * Date: 16 Apr 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef RNDF_PAINTER_H
#define RNDF_PAINTER_H

#include <rndf/RoadGraph.hh>

#ifdef USE_PLAN_GRAPH
#include <temp-planner-interfaces/PlanGraph.hh>
#include <temp-planner-interfaces/PlanGraphBuilder.hh>
#endif


// Class for rendering prior map data and overhead images
class RndfPainter
{
  public:

  // Constructor
  RndfPainter();

  public:

  // Load RNDF file
  int load(char *filename);
  
  // Draw RNDF in site frame
  int draw(bool drawRNDF, bool drawRoad, bool drawPlan, float px, float py, float size);
  
  private:

  // Road graph object (interpolated RNDF graph)
  RoadGraph *road;

  // RNDF display list
  int rndfList;

#if USE_PLAN_GRAPH

  // Plan graph object (dense, feasable RNDF graph)
  PlanGraph *plan;

  // Plan graph builder object (keeps track of ROI)
  PlanGraphBuilder *builder;

  // Plan graph display list
  int planList;
  
#endif  
};


#endif
