 
/* 
 * Desc: Render RNDF data
 * Date: 16 Apr 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <GL/glu.h>
#include <frames/vec3.h>
#include <frames/pose3.h>
#include <dgcutils/DGCutils.hh>
#include <rndf/RNDF.hh>

#include "glUtils.hh"
#include "RndfPainter.hh"

// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
RndfPainter::RndfPainter()
{
  this->road = NULL;
  this->rndfList = 0;
  
#if USE_PLAN_GRAPH
  this->plan = NULL;
  this->builder = NULL;
#endif
  
  return;
}


// Load RNDF file
int RndfPainter::load(char *filename)
{
  // Create RNDF road object
  this->road = new RoadGraph();
  assert(this->road);

  // Load from file
  if (filename)
  {
    MSG("loading %s", filename);
    if (this->road->load(filename) != 0)
      return ERROR("unable to load %s", filename);
  }

#if USE_PLAN_GRAPH
  // TODO: rationalize plan graph versus road graph usage.
  if (filename)
  {
    char pgFilename[1024];
    
    // Create plan graph object
    this->plan = new PlanGraph();
    assert(this->plan);

    // Create plan builder
    this->builder = new PlanGraphBuilder(this->plan);
    
    // Load from saved PG file
    snprintf(pgFilename, sizeof(pgFilename), "%s.pg", filename);
    MSG("loading pre-build plan graph from %s", pgFilename);
    if (this->builder->load(pgFilename) != 0)
      MSG("unable to load %s", pgFilename);
  }
#endif
  
  return 0;
}

  
// Draw RNDF in local frame
int RndfPainter::draw(bool drawRNDF, bool drawRoad, bool drawPlan, float px, float py, float size)
{
  if (drawRNDF)
  {
    // Draw the RNDF
    if (this->rndfList == 0)
      this->rndfList = this->road->rndf.predraw();
    glCallList(this->rndfList);
  }

  if (drawRoad)
  {
    // Draw a frame around the region we are displaying
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glColor3f(1, 1, 1);
    glBegin(GL_QUADS);
    glVertex2f(px - size/2, py - size/2);
    glVertex2f(px + size/2, py - size/2);
    glVertex2f(px + size/2, py + size/2);
    glVertex2f(px - size/2, py + size/2);
    glEnd();

    // Draw the roads
    glCallList(this->road->predraw(px, py, size));
  }

#if USE_PLAN_GRAPH
  if (drawPlan && this->plan && this->builder)
  {
    // Draw a frame around the region we are displaying
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glColor3f(1, 1, 1);
    glBegin(GL_QUADS);
    glVertex2f(px - size/2, py - size/2);
    glVertex2f(px + size/2, py - size/2);
    glVertex2f(px + size/2, py + size/2);
    glVertex2f(px - size/2, py + size/2);
    glEnd();

    // Draw a frame around the ROI we are displaying
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glColor3f(1, 1, 1);
    glBegin(GL_QUADS);
    glVertex2f(px - size/4, py - size/4);
    glVertex2f(px + size/4, py - size/4);
    glVertex2f(px + size/4, py + size/4);
    glVertex2f(px - size/4, py + size/4);
    glEnd();

    // Set the ROI
    uint64_t time;
    time = DGCgettime();  
    this->builder->updateROI(px, py, size/2);
    time = DGCgettime() - time;
    if (time > 1000)
      MSG("update ROI: %.3f ms %.0f %.0f %.0f",
          (double) time * 1e-3, px, py, size);

    int planList;
    time = DGCgettime();  
    planList = this->plan->predraw(px, py, size);
    time = DGCgettime() - time;
    if (time > 1000)
      MSG("predraw ROI: %.3f ms", (double) time * 1e-3);
    
    // Get the plan graph display list
    glCallList(planList);
  }
#endif
 
  return 0;
}

