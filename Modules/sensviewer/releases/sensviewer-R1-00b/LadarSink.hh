 
/* 
 * Desc: Sink for ladar blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include <sensnet/sensnet.h>
#include <interfaces/LadarRangeBlob.h>


// Display data for ladar blobs
class LadarSink
{
  public:

  // Constructor
  LadarSink();

  public:

  // Update with current sensnet data
  int updateSensnet(sensnet_t *sensnet);
  
  public:

  // Generate range point cloud (local frame)
  void predrawPointCloud();

  // Predraw camera footprint (sensor frame)
  void predrawFootprint();
    
  public:

  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId;

  // Blob data buffer
  LadarRangeBlob blob;
  
  // Are we enabled?
  bool enable;

  // Set flag if the data needs predrawing
  bool dirty;

  // Rainbow color scale
  uint8_t rainbow[0x10000][3];
  
  // How many clouds to we render?
  int numClouds, maxClouds;
  
  // GL Drawing lists
  int cloudList, footList;
};
