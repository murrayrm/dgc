
/* 
 * Desc: Sink for stereo blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glu.h>
#include <jplv/jplv_image.h>

#include <alice/AliceConstants.h>
#include <interfaces/SensnetTypes.h>


#include "glUtils.hh"
#include "StereoSink.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
StereoSink::StereoSink(int menuId, int sensorId)
{
  memset(this, 0, sizeof(*this));

  this->menuId = menuId;
  this->sensorId = sensorId;
  this->blobType = SENSNET_STEREO_IMAGE_BLOB;
  this->blobId = -1;

  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
  {
    d = 4 * ((double) i / 0x10000);

    if (d >= 0 && d < 1.0)
    {
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = (int) (d * 0xFF);
      this->rainbow[i][2] = 0xFF;
    }
    else if (d < 2.0)
    {
      d -= 1.0;
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = (int) ((1 - d) * 0xFF);
    }
    else if (d < 3.0)
    {
      d -= 2.0;
      this->rainbow[i][0] = (int) (d * 0xFF);
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = 0x00;
    }
    else if (d < 4.0)
    {
      d -= 3.0;
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = (int) ((1 - d) * 0xFF);
      this->rainbow[i][2] = 0x00;
    }
    else
    {
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = 0x00;
      this->rainbow[i][2] = 0x00;
    }
  }

  return;
}


// Enable/disable sink
int StereoSink::enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable)
{
  if (sensnet)
  {
    // If in live mode...
    if (enable && !this->enabled)
    {
      sensnet_join(sensnet, this->sensorId, this->blobType, sizeof(this->blob));
      this->enabled = true;
    }
    else if (!enable && this->enabled)
    {
      sensnet_leave(sensnet, this->sensorId, this->blobType);
      this->enabled = false;
    }
  }  
  else if (replay)
  {
    // If in replay mode
    this->enabled = enable;
  }
  return 0;
}


// Update with current sensnet data
int StereoSink::update(sensnet_t *sensnet, sensnet_replay_t *replay)
{
  int blobId;
      
  if (sensnet)
  {
    // Peek at the latest data, but dont read it out
    if (sensnet_peek(sensnet, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;

    // Is the data new?
    if (blobId < 0 || blobId == this->blobId)
      return 0;

    // Read the data
    if (sensnet_read(sensnet, this->sensorId, this->blobType,
                     &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;
  }
  else if (replay)
  {
    // Peek at the latest data, but dont read it out
    if (sensnet_replay_peek(replay, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;

    // Is the data new?
    if (blobId < 0 || blobId == this->blobId)
      return 0;

    // Read the data    
    if (sensnet_replay_read(replay, this->sensorId, this->blobType,
                            &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;    
  }
  
  return 0;
}


// Write out the current image
int StereoSink::writeImage()
{
  jplv_image_t *image;
  char filename[1024];

  if (this->blobId < 0)
    return 0;

  if (this->blob.leftSize > 0)
  {
    snprintf(filename, sizeof(filename), "%06d-left.pnm", this->blob.frameId);
    MSG("writing %s", filename);
    image = jplv_image_alloc(this->blob.cols, this->blob.rows, this->blob.channels,
                             8, 0, this->blob.imageData + this->blob.leftOffset);
    jplv_image_write_pnm(image, filename, NULL);
    jplv_image_free(image);
  }

  if (this->blob.rightSize > 0)
  {
    snprintf(filename, sizeof(filename), "%06d-right.pnm", this->blob.frameId);
    MSG("writing %s", filename);
    image = jplv_image_alloc(this->blob.cols, this->blob.rows, this->blob.channels,
                             8, 0, this->blob.imageData + this->blob.rightOffset);
    jplv_image_write_pnm(image, filename, NULL);
    jplv_image_free(image);
  }

  return 0;
}


// Generate images
void StereoSink::predrawImages()
{
  int cols, rows, channels;

  if (this->blobId < 0)
    return;

  // Create display list
  if (this->imageList == 0)
    this->imageList = glGenLists(1);

  // Create left/right image textures
  if (this->imageTex[0] == 0)
    glGenTextures(2, this->imageTex);

  cols = this->blob.cols;
  rows = this->blob.rows;
  channels = this->blob.channels;

  if (this->blob.leftSize > 0)
  {
    // Copy image into texture
    glBindTexture(GL_TEXTURE_2D, this->imageTex[0]);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    if (channels == 1)
      gluBuild2DMipmaps(GL_TEXTURE_2D, 4, cols, rows, GL_LUMINANCE, GL_UNSIGNED_BYTE,
                        this->blob.imageData + this->blob.leftOffset);
    else if (channels == 3)
      gluBuild2DMipmaps(GL_TEXTURE_2D, 4, cols, rows, GL_RGB, GL_UNSIGNED_BYTE,
                        this->blob.imageData + this->blob.leftOffset);
  }

  if (this->blob.rightSize > 0)
  {
    // Copy image into texture
    glBindTexture(GL_TEXTURE_2D, this->imageTex[1]);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    if (channels == 1)
      gluBuild2DMipmaps(GL_TEXTURE_2D, 4, cols, rows, GL_LUMINANCE, GL_UNSIGNED_BYTE,
                        this->blob.imageData + this->blob.rightOffset);
    else if (channels == 3)
      gluBuild2DMipmaps(GL_TEXTURE_2D, 4, cols, rows, GL_RGB, GL_UNSIGNED_BYTE,
                        this->blob.imageData + this->blob.rightOffset);
  }
  
  glNewList(this->imageList, GL_COMPILE);
    
  glEnable(GL_TEXTURE_2D);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glColor3f(1, 1, 1);

  if (this->blob.sensorId == SENSNET_LF_SHORT_STEREO)
  {
    glBindTexture(GL_TEXTURE_2D, this->imageTex[0]);
    glTranslatef(-cols-cols/2, 0, 0);
    glScalef(0.5, 0.5, 1);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(0, 0, 0);
    glTexCoord2f(1, 0);
    glVertex3f(cols, 0, 0);
    glTexCoord2f(1, 1);
    glVertex3f(cols, rows, 0);
    glTexCoord2f(0, 1);
    glVertex3f(0, rows, 0);
    glEnd();
  }
  else if (this->blob.sensorId == SENSNET_RF_SHORT_STEREO)
  {
    glBindTexture(GL_TEXTURE_2D, this->imageTex[0]);
    glTranslatef(cols, 0, 0);
    glScalef(0.5, 0.5, 1);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(0, 0, 0);
    glTexCoord2f(1, 0);
    glVertex3f(cols, 0, 0);
    glTexCoord2f(1, 1);
    glVertex3f(cols, rows, 0);
    glTexCoord2f(0, 1);
    glVertex3f(0, rows, 0);
    glEnd();
  }
  else if (this->blob.sensorId == SENSNET_MF_LONG_STEREO)
  {
    // Draw left image
    glPushMatrix();
    glTranslatef(-cols/2, 0, 0);
    glScalef(0.5, 0.5, 1);
    glBindTexture(GL_TEXTURE_2D, this->imageTex[0]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex3f(0, 0, 0);
    glTexCoord2f(1, 0); glVertex3f(cols, 0, 0);
    glTexCoord2f(1, 1); glVertex3f(cols, rows, 0);
    glTexCoord2f(0, 1); glVertex3f(0, rows, 0);
    glEnd();
    glPopMatrix();

    // Draw right image
    glPushMatrix();
    glTranslatef(0, 0, 0);
    glScalef(0.5, 0.5, 1);
    glBindTexture(GL_TEXTURE_2D, this->imageTex[1]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex3f(0, 0, 0);
    glTexCoord2f(1, 0); glVertex3f(cols, 0, 0);
    glTexCoord2f(1, 1); glVertex3f(cols, rows, 0);
    glTexCoord2f(0, 1); glVertex3f(0, rows, 0);
    glEnd();
    glPopMatrix();    
  }

  glDisable(GL_TEXTURE_2D);

  glEndList();
    
  return;
}


// Generate range point cloud in camera frame
void StereoSink::predrawPointCloud()
{
  int i, j;
  uint8_t *cpix;
  uint16_t *dpix;
  float px, py, pz;
  StereoImageBlob *blob;

  if (this->blobId < 0)
    return;

  // Is there any disparity in this blob?
  if (this->blob.dispSize == 0)
    return;
  
  if (this->cloudList == 0)
    this->cloudList = glGenLists(1);

  glNewList(this->cloudList, GL_COMPILE);

  glPushAttrib(GL_POINT_BIT);
  glPointSize(2.0);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_POINTS);

  blob = &this->blob;

  // Get the color at the pixel
  cpix = StereoImageBlobGetLeft(&this->blob, 0, 0);
  dpix = StereoImageBlobGetDisp(&this->blob, 0, 0);

  for (j = 0; j < blob->rows; j++)
  {              
    for (i = 0; i < blob->cols; i++)
    {
      if (dpix[0] > 0 && dpix[0] < 0x7000)
      {
        float d;

        // Compute true disparity
        d = (float) dpix[0] / this->blob.dispScale;

        // Convert to sensor frame
        StereoImageBlobImageToSensor(&this->blob, i, j, d, &px, &py, &pz);

        if (this->color == ELEVATION_COLOR)
        {
          int k;
          float vx, vy, vz;
                  
          // Convert to vehicle frame
          StereoImageBlobSensorToVehicle(&this->blob, px, py, pz, &vx, &vy, &vz);

          // Select color based on elevation in vehicle frame
          k = (int) (0x10000 * (-vz) / 2.0); // MAGIC
          if (k < 0x0000) k = 0x0000;
          if (k > 0xFFFF) k = 0xFFFF;
          glColor3ub(this->rainbow[k][0], this->rainbow[k][1], this->rainbow[k][2]);
        }
        else
        {
          // Use true pixel color
          if (blob->channels == 1)
            glColor3ub(cpix[0], cpix[0], cpix[0]);
          else
            glColor3ub(cpix[0], cpix[1], cpix[2]);
        }        
        
        glVertex3f(px, py, pz);
      }

      cpix += this->blob.channels;
      dpix += 1;
    }
  }

  glEnd();
  glPopAttrib();

  glEndList();
    
  return;
}


// Predraw camera footprint on ground plane (vehicle frame)
void StereoSink::predrawFootprint()
{
  int camera, i;
  float pi, pj;
  float px, py, pz;
  float vertices[4][2];
  int cols, rows;
  float cx, cy, sx, sy;
  float m[4][4];

  if (this->blobId < 0)
    return;

  if (this->footList == 0)
    this->footList = glGenLists(1);
  glNewList(this->footList, GL_COMPILE);

  cols = this->blob.cols;
  rows = this->blob.rows;

  for (camera = 0; camera < 2; camera++)
  {
    if (camera == 0)
    {
      cx = this->blob.leftCamera.cx;
      cy = this->blob.leftCamera.cy;
      sx = this->blob.leftCamera.sx;
      sy = this->blob.leftCamera.sy;      
      memcpy(m, this->blob.leftCamera.sens2veh, sizeof(m));
    }
    else
    {
      cx = this->blob.rightCamera.cx;
      cy = this->blob.rightCamera.cy;
      sx = this->blob.rightCamera.sx;
      sy = this->blob.rightCamera.sy;
      memcpy(m, this->blob.rightCamera.sens2veh, sizeof(m));
    }

    // Switch to sensor frame
    glPushFrame(m);
          
    // Offset the camera to vehicle transform to find the nominal ground plane
    m[2][3] -= VEHICLE_TIRE_RADIUS - 0.01; // HACK

    vertices[0][0] = 0;
    vertices[0][1] = rows;
    vertices[1][0] = cols;
    vertices[1][1] = rows;
    vertices[2][0] = cols;
    vertices[2][1] = 0;
    vertices[3][0] = 0;
    vertices[3][1] = 0;

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glColor4f(0, 0, 1, 0.50);
    
    glBegin(GL_POLYGON);
    
    for (i = 0; i < 4; i++)
    {
      pi = vertices[i][0];
      pj = vertices[i][1];

      // Some corners of this image may not intersect with the plane, so
      // try a few different values for the row to find a pixel that
      // does.  The equation below computes the ray/z-plane intersection
      // in camera coordinates, assuming the matrix m specifies the
      // camera pose relative to the plane.
      px = py = pz = 0;
      for (; pj <= rows; pj += 4)
      {
        px = (pi - cx) / sx;
        py = (pj - cy) / sy;
        pz = m[2][0] * px + m[2][1] * py + m[2][2];
        pz = -m[2][3] / pz;
        if (pz > 0)
          break;
      }
    
      px *= pz;
      py *= pz;

      glTexCoord2f(pi/cols, pj/rows);
      glVertex3f(px, py, pz);
    }
    glEnd();

    glDisable(GL_BLEND);
    
    // Revert to vehicle frame
    glPopFrame();  
  }
  
  glEndList();
    
  return;
}

