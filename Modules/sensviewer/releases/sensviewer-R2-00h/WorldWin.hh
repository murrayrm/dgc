
/* 
 * Desc: Window for displaying 3D stuff
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Multiline_Output.H>
#include <jplv/Fl_Glv_Window.H>
#include <frames/pose3.h>

#include "sensnet/sensnet.h"
#include "sensnet/sensnet_replay.h"

#include "StereoSink.hh"
#include "LadarSink.hh"
#include "RoadLineSink.hh"
#include "ObsMapSink.hh"

#include "MapPainter.hh"
#include "RndfPainter.hh"



// Widget for displaying images
class WorldWin : public Fl_Group
{
  public:

  // Constructor
  WorldWin(int x, int y, int w, int h, int menuh);

  public:

  // Initialize sensnet stuff
  int init(sensnet_t *sensnet, sensnet_replay_t *replay,
           const char *gisPath, char *rndfFile);

  // Enable/disable a sink
  int toggle(int menuId);

  // Update blob data
  int update();

  public:
  
  // Handle menu options
  static void onSensor(Fl_Widget *w, int option);

  // Handle menu callbacks
  static void onAction(Fl_Widget *w, int option);

  // Handle menu options
  static void onView(Fl_Widget *w, int option);

  // Prepare for drawing
  static void onPrepare(Fl_Glv_Window *win, WorldWin *self);

  // Draw window
  static void onDraw(Fl_Glv_Window *win, WorldWin *self);

  private:

  // Switch to the given frame; i.e., sets up the GL coordinate
  // transforms such that (0, 0, 0) will map to the given pose.
  void pushFrame(pose3_t pose);

  // Switch to given frame (homogeneous transform)
  void pushFrame(float m[4][4]);

  // Switch to given local frame
  void pushFrameLocal(VehicleState state);
  
  // Revert to previous frame
  void popFrame();

  // Switch to viewport projection.
  void pushViewport();

  // Revert to non-viewport projection
  void popViewport();

  private:

  // Predraw state data as text
  void drawStateText();

  // Predraw the robot
  void predrawAlice();

  // Predraw the path
  void predrawPath();

  // Predraw a ground grid
  void predrawGrid();

  public:

  // Local menu
  Fl_Menu_Bar *menubar;
  
  // GL window
  Fl_Glv_Window *glwin;

  // View options
  bool viewVehicle;
  bool viewStatus, viewImages, viewFootprint, viewCloud;
  bool viewGrid, viewMap, viewRndf;

  // Capture screen?
  int capEnable;
  
  // Screen capture counter
  int capCount;

  // Sensnet handle
  sensnet_t *sensnet;

  // Multi-log replay handle
  sensnet_replay_t *replay;
  
  // List of stereo sinks
  int numStereoSinks;
  StereoSink *stereoSinks[8];

  // List of ladar sinks
  int numLadarSinks;
  LadarSink *ladarSinks[8];

  // Road line sink
  RoadLineSink *roadLineSink;

  // Obstacle map sink
  ObsMapSink *obsMapSink;
  
  // Most recent state data
  VehicleState state;

  // Painter for prior map data
  MapPainter mapPainter;

  // RNDF painter
  RndfPainter rndfPainter;

  // Does anything need predrawing?
  bool dirty;

  // Robot pose history
  int numPoses, maxPoses;
  pose3_t *poses;
  
  // Some display lists
  int aliceList, pathList, gridList;
};
