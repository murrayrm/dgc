
/* 
 * Desc: Sink for Obstacle Maps
 * Date: 7 Feb 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/
#ifndef OBSMAP_SINK_H
#define OBSMAP_SINK_H

#include "sensnet/sensnet.h"
#include "sensnet/sensnet_replay.h"
#include "interfaces/ObsMapMsg.h"
#include "cmap/CMap.hh"


const int NUM_ROWS=500;
const int NUM_COLS=500;
const double RES_ROWS=0.40;
const double RES_COLS=0.40;

// Display data for ladar blobs
class ObsMapSink 
{ 
  public: 

  // Constructor
  ObsMapSink(int menuId);

  public:
  
  // Enable/disable sink
  int enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable);

  // Update with current sensnet data
  int update(sensnet_t *sensnet, sensnet_replay_t *replay);
  
  public:

  // draw elevation blocks for all cells
  void predrawCells();
    
  public:
    
  // Our ID in the sensor menu
  int menuId;

  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId; 

  // Blob data buffer 
  ObsMapMsg blob;

  // Map
  CMap blobMap;
  int blobMapID_meanElevation;
  int blobMapID_stdDev;
  int blobMapID_cellType;

  // Rainbow color scale
  uint8_t rainbow[0x10000][3];
  
  // Are we enabled? 
  bool enabled; 

  // Set flag if the data needs predrawing 
  bool dirty; 
  
  // GL Drawing lists
  int cellList;
};

#endif

