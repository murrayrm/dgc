 
/* 
 * Desc: Sink for Obstacle Maps
 * Date: 7 Feb 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/gl.h>

#include <alice/AliceConstants.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>

#include "ObsMapSink.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
ObsMapSink::ObsMapSink(int menuId)
{
  memset(this, 0, sizeof(*this));

  this->menuId = menuId;
  this->sensorId = SENSNET_SKYNET_SENSOR;
  this->blobType = SNfusiondeltamap;
  this->blobId = -1; //cannot be 0 because obsperceptor is 0

  // initialize Map
  this->blobMap.initMap(0.0,0.0,NUM_ROWS,NUM_COLS,RES_ROWS,RES_COLS,0);
  this->blobMapID_meanElevation = this->blobMap.addLayer<double>(0.0, -1.0, false);
  this->blobMapID_stdDev = this->blobMap.addLayer<double>(0.0, 0.0, false);
  this->blobMapID_cellType = this->blobMap.addLayer<CELL_TYPE>(EMPTY,OUTSIDE_MAP,false);


  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
  {
    d = 4 * ((double) i / 0x10000);

    if (d >= 0 && d < 1.0)
    {
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = (int) (d * 0xFF);
      this->rainbow[i][2] = 0xFF;
    }
    else if (d < 2.0)
    {
      d -= 1.0;
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = (int) ((1 - d) * 0xFF);
    }
    else if (d < 3.0)
    {
      d -= 2.0;
      this->rainbow[i][0] = (int) (d * 0xFF);
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = 0x00;
    }
    else if (d < 4.0)
    {
      d -= 3.0;
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = (int) ((1 - d) * 0xFF);
      this->rainbow[i][2] = 0x00;
    }
    else
    {
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = 0x00;
      this->rainbow[i][2] = 0x00;
    }
  }
  
  
  return;
}


// Enable/disable sink
int ObsMapSink::enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable)
{
  if (sensnet)
  {
    // If in live mode...
    if (enable && !this->enabled)
    {
      sensnet_join(sensnet, this->sensorId, this->blobType, sizeof(this->blob));
      this->enabled = true;
    }
    else if (!enable && this->enabled)
    {
      sensnet_leave(sensnet, this->sensorId, this->blobType);
      this->enabled = false;
    }
  }  
  else if (replay)
  {
    // If in replay mode
    this->enabled = enable;
  }
  return 0;
}


// Update with current sensnet data
int ObsMapSink::update(sensnet_t *sensnet, sensnet_replay_t *replay)
{
  if (sensnet)
  {
    int blobId;
      
    // Peek at the latest data, but dont read it out
    if (sensnet_peek(sensnet, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;

    // Is the data valid?
    if (blobId < 0)
      return 0;
    
    // Is the data new?
    if (blobId == this->blobId)
      return 0;

    // Read the data
    if (sensnet_read(sensnet, this->sensorId, this->blobType,
                     &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;
  }
  else if (replay)
  {
    int sensorId, blobType, blobId, blobSize;
    
    // Peek at the latest data, but dont read it out
    if (sensnet_replay_peek(replay, NULL, &sensorId, &blobType, &blobId, &blobSize) < 0)
      return -1;

    // Is the data for this sink?
    if (sensorId != this->sensorId)
      return 0;
    if (blobType != this->blobType)
      return 0;
    
    // Is the data new?
    if (blobId == this->blobId)
      return 0;

    // Sanity check the blob size
    assert((size_t) blobSize <= sizeof(this->blob));
    
    // Read the data
    if (sensnet_replay_read(replay, NULL, &sensorId, &blobType,
                           &this->blobId, sizeof(this->blob), &this->blob) < 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;    
  }


  ElevationData tempData;      
  //update blobMap
  this->blobMap.updateVehicleLoc(this->blob.state.localX,this->blob.state.localY);
  for(int i=0; i<this->blob.numDeltas; i++)
  {
    tempData = this->blob.mapDeltaVec[i];

    // TESTING
    this->blobMap.setDataUTM<double>(this->blobMapID_meanElevation, tempData.UTMPoint.N, tempData.UTMPoint.E, tempData.meanElevation);
    //this->blobMap.setDataUTM<double>(this->blobMapID_stdDev, tempData.UTMPoint.N, tempData.UTMPoint.E, tempData.stdDev);
    //this->blobMap.setDataUTM<CELL_TYPE>(this->blobMapID_cellType, tempData.UTMPoint.N, tempData.UTMPoint.E, tempData.cellType);
  }
  
  this->dirty = true;
  
  return 0;
}


// Generate lines (local frame)
void ObsMapSink::predrawCells()
{
  double meanElevation, groundElevation;
  double northing, easting;
  float x_bottomleft, y_bottomleft;
  float x_bottomright, y_bottomright;
  float x_topleft, y_topleft;
  float x_topright, y_topright;
  double row_resolution = RES_ROWS;
  double col_resolution = RES_COLS;
  int k;


  // Dont draw if we are not dirty
  if (!this->dirty)
    return;
  this->dirty = false;

  if (this->blobId < 0)
    return;

  // still need to figure this out
  if (this->cellList == 0)
    this->cellList = glGenLists(1);
  glNewList(this->cellList, GL_COMPILE);
  
  // Draw cubes over each cell
  //  glColor3f(1, 0, 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  
  for(int i=0; i<NUM_ROWS; i++)
  {
    for(int j=0; j<NUM_COLS; j++)
    {
      meanElevation = this->blobMap.getDataWin<double>(this->blobMapID_meanElevation, i,j);
      this->blobMap.Win2UTM(i,j,&northing, &easting);
            
      // See if there is any data in this cell before we draw it
      if (meanElevation == 0)
        continue;     
      
      // Figure out where the nominal ground should be
      groundElevation = this->blob.state.localZ + VEHICLE_TIRE_RADIUS;

      // Select color based on elevation relative to vehicle
      k = (int) (-0x10000 * (float)(meanElevation-groundElevation) / 2.0); // MAGIC
      if (k < 0x0000) k = 0x0000;
      if (k > 0xFFFF) k = 0xFFFF;
      glColor3ub(this->rainbow[k][0], this->rainbow[k][1], this->rainbow[k][2]);
	  
      /* =================================================================================
       * This function draws a block with the following properties
       * HEIGHT OF BLOCK : meanElevation
       * POSITION OF BLOCK : bottom left base of block is positioned at (northing, easting)
       * WIDTH OF BLOCK : row_resolution
       * LENGTH OF BLOCK : col_resolution
       *
       * THE FOLLOWING DIAGRAM DESCRIBES THE BASE OF THE BLOCK
       *
       *      (x_topleft, y_topleft)               (x_topright, y_topright)
       *               ||================================||
       *               ||                                ||
       *               ||                                ||
       *               ||                                ||
       *               ||                                ||
       *               ||                                ||
       *               ||                                ||
       *               ||                                ||
       *               ||================================||
       *      (x_botleft, y_botleft)               (x_botright, y_botright)
       *
       * =================================================================================	   
       */
	  
      x_bottomleft = northing;
      y_bottomleft = easting;
      x_bottomright = northing+col_resolution;
      y_bottomright = easting;
      x_topleft = northing;
      y_topleft = easting+row_resolution;
      x_topright = northing+col_resolution;
      y_topright = easting+row_resolution;

      // FRONT FACE
      glBegin(GL_QUADS);
      glVertex3f(x_bottomleft,y_bottomleft,groundElevation);
      glVertex3f(x_bottomleft,y_bottomleft,meanElevation);
      glVertex3f(x_bottomright,y_bottomright,meanElevation);
      glVertex3f(x_bottomright,y_bottomright,groundElevation);	
      glEnd();
	  
      //LEFT FACE
      glBegin(GL_QUADS);
      glVertex3f(x_bottomleft,y_bottomleft,groundElevation);
      glVertex3f(x_bottomleft,y_bottomleft,meanElevation);
      glVertex3f(x_topleft,y_topleft,meanElevation);
      glVertex3f(x_topleft,y_topleft,groundElevation);
      glEnd();
	  
      //BACK FACE
      glBegin(GL_QUADS);
      glVertex3f(x_topleft,y_topleft,groundElevation);
      glVertex3f(x_topleft,y_topleft,meanElevation);
      glVertex3f(x_topright,y_topright,meanElevation);
      glVertex3f(x_topright,y_topright,groundElevation);
      glEnd();
	  
      //RIGHT FACE
      glBegin(GL_QUADS);
      glVertex3f(x_topright,y_topright,groundElevation);
      glVertex3f(x_topright,y_topright,meanElevation);
      glVertex3f(x_bottomright,y_bottomright,meanElevation);
      glVertex3f(x_bottomright,y_bottomright,groundElevation);
      glEnd();

  
      //TOP FACE
      glBegin(GL_QUADS);	
      glVertex3f(x_bottomleft,y_bottomleft,meanElevation);
      glVertex3f(x_topleft,y_topleft,meanElevation);
      glVertex3f(x_topright,y_topright,meanElevation);
      glVertex3f(x_bottomright,y_bottomright,meanElevation);	
      glEnd();
	  
	  
    }
  }
  
  glEndList();
  
  return;
}

