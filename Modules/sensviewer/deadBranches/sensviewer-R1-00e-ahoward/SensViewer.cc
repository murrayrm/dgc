
/* 
 * Desc: Sensor viewer utility
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <GL/glut.h>

#include <FL/Fl.H>
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <jplv/Fl_Glv_Window.H>
#include <jplv/jplv_image.h>

#include <dgcutils/DGCutils.hh>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>

#include "cmdline.h"
#include "WorldWin.hh"


// Application 
class App
{
  public:

  // Default constructor
  App();

  // Destructor
  ~App();
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  // Initialize for reading from SensNet
  int initSensnet();
  
  // Finalize stiff
  int sensnetFini();

  // Initialize GUI
  int init(int cols, int rows);

  // Finalize styff
  int fini();

  public:

  // Action callback
  static void onAction(Fl_Widget *w, int option);

  // Speed callback
  static void onSpeed(Fl_Widget *w, int option);
  
  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  public:
  
  // Command-line options
  struct gengetopt_args_info options;
  
  // Default configuration path
  char configPath[256];

  // Top-level window
  Fl_Window *mainwin;
  
  // Top-level menu
  Fl_Menu_Bar *menubar;

  // 3D window
  WorldWin *worldwin;

  // What mode are we in?
  enum {modeSpread, modeReplay} mode;

  // What is our replay speed
  int replaySpeed;

  // Last time the idle function was called
  uint64_t idleTimestamp;

  // Should we pause
  bool pause;
  
  // Should we quit?
  bool quit;

  // Spread settings
  char *spreadDaemon; int skynetKey;

  // SensNet handle
  sensnet_t *sensnet;

} app_t;


// Commands
enum
{
  APP_ACTION_PAUSE = 0x1000,
  APP_ACTION_SPEED_0,
  APP_ACTION_SPEED_5,
};


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
App::App()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
App::~App()
{
  return;
}


// Parse command-line options
int App::parseCmdLine(int argc, char **argv)
{
  // Run parser
  if (cmdline_parser(argc, argv, &this->options) != 0)
  {
    cmdline_parser_print_help();
    return -1;
  }

  // See which mode we are running in (sensnet or log)
  if (this->options.inputs_num == 0)
    this->mode = modeSpread;
  else
    this->mode = modeReplay;

  // Fill out the default config path
  if (getenv("DGC_CONFIG_PATH"))
    snprintf(this->configPath, sizeof(this->configPath),
             "%s/sensviewer", getenv("DGC_CONFIG_PATH"));
  else
    MSG("please set DGC_CONFIG_PATH to enable prior map painting");    

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;

  return 0;
}


// Initialize for reading from SensNet
int App::initSensnet()
{
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);  

  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, -1) != 0)
    return ERROR("unable to connect to sensnet");

  // If replaying log files, now is the time to open them
  if (this->mode == modeReplay)
  {
    if (sensnet_open_replay(this->sensnet, this->options.inputs_num, this->options.inputs) != 0)
      return ERROR("unable to open log");
  }

  // Initialize the world window
  this->worldwin->init(this->sensnet, this->configPath);
   
  return 0; 
}


// Finalize stuff
int App::sensnetFini()
{
  // Clean up SensNet
  if (this->mode == modeReplay)
    sensnet_close_replay(this->sensnet);
  if (sensnet_disconnect(this->sensnet) != 0)
    return ERROR("unable to disconnect from sensnet");
  sensnet_free(this->sensnet);
  
  return 0;
}


// Initialize stuff
int App::init(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) App::onExit},
      {0},
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) App::onAction, (void*) APP_ACTION_PAUSE, FL_MENU_TOGGLE},
      {"Speed x0", '0', (Fl_Callback*) App::onSpeed, (void*) 0, FL_MENU_RADIO},
      {"Speed x1", '1', (Fl_Callback*) App::onSpeed, (void*) 1, FL_MENU_RADIO | FL_MENU_VALUE},
      {"Speed x2", '2', (Fl_Callback*) App::onSpeed, (void*) 2, FL_MENU_RADIO},  
      {"Speed x5", '5', (Fl_Callback*) App::onSpeed, (void*) 5, FL_MENU_RADIO},
      {0},
      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Sensor Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new WorldWin(0, 30, cols, rows - 30, 30);

  this->mainwin->end();

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  return 0;
}


// Finalize styff
int App::fini()
{
  return 0;
}


// Handle menu callbacks
void App::onAction(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data();
  if (option == APP_ACTION_PAUSE)
    self->pause = !self->pause;
  
  return;
}



// Handle menu callbacks
void App::onSpeed(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data(); 
  self->replaySpeed = option;
  
  return;
}


// Handle menu callbacks
void App::onExit(Fl_Widget *w, int option)
{
  App *self;

  self = (App*) w->user_data();
  self->quit = true;

  return;
}


// Handle idle callbacks
void main_idle(App *self)
{
  int dt;

  // Record when we where last in the idle function
  if (self->idleTimestamp > 0)
    dt = (int) ((DGCgettime() - self->idleTimestamp) / 1000);
  else
    dt = 0;
  self->idleTimestamp = DGCgettime();
  
  if (!self->pause)
  {  
    if (self->mode == App::modeReplay)
    {
      // Step through the data at some fixed rate
      sensnet_step(self->sensnet, dt * self->replaySpeed);
    }
    else
    {
      // Wait some time for new data
      sensnet_wait(self->sensnet, 100);
    }

    // Update the world window
    self->worldwin->update();
  }
  else
  {
    // Sleepy bye-bye
    usleep(0);
  }
  
  return;
}


// Main loop
int main(int argc, char *argv[])
{
  App *app;
  
  // Initialize GLUT calls
  glutInit(&argc, argv);
  
  app = new App();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  if (app->init(1024, 768) != 0)
    return -1;
  
  // Initialize with appropriate mode
  if (app->initSensnet() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) main_idle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
