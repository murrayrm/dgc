 
/* 
 * Desc: Sink for road lines
 * Date: 16 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include "sensnet/sensnet.h"
#include "interfaces/RoadLineMsg.h"


// Display data for ladar blobs
class RoadLineSink
{
  public:

  // Constructor
  RoadLineSink();

  public:

  // Update with current sensnet data
  int updateSensnet(sensnet_t *sensnet);
  
  public:

  // Generate lines (local frame)
  void predrawLines();
    
  public:

  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId;

  // Blob data buffer
  RoadLineMsg blob;
  
  // Are we enabled?
  bool enable;

  // Set flag if the data needs predrawing
  bool dirty;
  
  // GL Drawing lists
  int lineList;
};
