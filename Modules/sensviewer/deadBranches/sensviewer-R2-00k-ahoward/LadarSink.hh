 
/* 
 * Desc: Sink for ladar blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include "sensnet/sensnet.h"
#include "sensnet/sensnet_replay.h"
#include "interfaces/LadarRangeBlob.h"


// Display data for ladar blobs
class LadarSink
{
  public:

  // Constructor
  LadarSink(int menuId, int sensorId);

  public:

  // Enable/disable sink
  int enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable);

  // Update with current sensnet data
  int update(sensnet_t *sensnet, sensnet_replay_t *replay);
  
  public:

  // Generate status text (sensor frame)
  void predrawStatus();

  // Generate range point cloud (local frame)
  void predrawPointCloud(int maxScans);

  // Predraw camera footprint (sensor frame)
  void predrawFootprint();
    
  public:

  // Our ID in the sensor menu
  int menuId;
  
  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId;

  // Blob data buffer
  LadarRangeBlob blob;
  
  // Are we enabled?
  bool enabled;

  // Set flag if the data needs predrawing
  bool dirty;

  // Rainbow color scale
  uint8_t rainbow[0x10000][3];
  
  // How many clouds to we render?
  int numClouds, maxClouds;
  
  // GL Drawing lists
  int statusList, cloudList, footList;
};
