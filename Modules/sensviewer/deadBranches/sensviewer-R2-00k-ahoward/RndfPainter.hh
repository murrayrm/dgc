 
/* 
 * Desc: Render RNDF data
 * Date: 16 Apr 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef RNDF_PAINTER_H
#define RNDF_PAINTER_H

#include <rndf/RNDF.hh>
#include <interfaces/VehicleState.h>


// Class for rendering prior map data and overhead images
class RndfPainter
{
  public:

  // Constructor
  RndfPainter();

  public:

  // Load RNDF file
  int load(char *filename);
  
  // Draw RNDF in local frame
  int draw(VehicleState state);

  private:

  // Pre-draw to a display list
  int predraw(VehicleState state);
  
  private:

  // RNDF object
  std::RNDF *rndf;
  
  // RNDF display list
  GLuint list;  
};


#endif
