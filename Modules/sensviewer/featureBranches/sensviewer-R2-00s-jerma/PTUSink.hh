 
/* 
 * Desc: Sink for PTU state blob data
 * Date: 1 June 2007
 * Author: Jeremy Ma (adapted from Andrew Howard's RadarSink.hh)
 * CVS: $Id$
*/

#include "sensnet/sensnet.h"
#include "sensnet/sensnet_replay.h"
#include "interfaces/PTUStateBlob.h"


// Display data for radar blobs
class PTUSink
{
  public:

  // Constructor
  PTUSink(int menuId, int sensorId);

  public:

  // Enable/disable sink
  int enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable);

  // Update with current sensnet data
  int update(sensnet_t *sensnet, sensnet_replay_t *replay);
  
  public:

  // Generate status text (sensor frame)
  void predrawStatus();
    
  // Generate line of site (local frame)
  void predrawLineOfSite();

  public:

  // Our ID in the sensor menu
  int menuId;
  
  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId;

  // Blob data buffer
  PTUStateBlob blob;
  
  // Are we enabled?
  bool enabled;

  // Set flag if the data needs predrawing
  bool dirty;
  
  // GL Drawing lists
  int statusList, lineofsiteList;
};
