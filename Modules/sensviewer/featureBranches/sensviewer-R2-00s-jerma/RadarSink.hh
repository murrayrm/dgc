 
/* 
 * Desc: Sink for radar blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include "sensnet/sensnet.h"
#include "sensnet/sensnet_replay.h"
#include "interfaces/RadarRangeBlob.h"


// Display data for radar blobs
class RadarSink
{
  public:

  // Constructor
  RadarSink(int menuId, int sensorId);

  public:

  // Enable/disable sink
  int enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable);

  // Update with current sensnet data
  int update(sensnet_t *sensnet, sensnet_replay_t *replay);
  
  public:

  // Generate status text (sensor frame)
  void predrawStatus();

  // Generate range point cloud (local frame)
  void predrawPointCloud();
    
  public:

  // Our ID in the sensor menu
  int menuId;
  
  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId;

  // Blob data buffer
  RadarRangeBlob blob;
  
  // Are we enabled?
  bool enabled;

  // Set flag if the data needs predrawing
  bool dirty;

  // Rainbow color scale
  uint8_t rainbow[0x10000][3];
  
  // GL Drawing lists
  int statusList, cloudList;
};
