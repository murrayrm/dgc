
/* 
 * Desc: Sink for ladar blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glu.h>

#include <frames/pose3.h>
#include <alice/AliceConstants.h>
#include <interfaces/SensnetTypes.h>

#include "LadarSink.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
LadarSink::LadarSink()
{
  memset(this, 0, sizeof(*this));

  this->sensorId = SENSNET_NULL_SENSOR;
  this->blobId = -1;

  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
  {
    d = 4 * ((double) i / 0x10000);

    if (d >= 0 && d < 1.0)
    {
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = (int) (d * 0xFF);
      this->rainbow[i][2] = 0xFF;
    }
    else if (d < 2.0)
    {
      d -= 1.0;
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = (int) ((1 - d) * 0xFF);
    }
    else if (d < 3.0)
    {
      d -= 2.0;
      this->rainbow[i][0] = (int) (d * 0xFF);
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = 0x00;
    }
    else if (d < 4.0)
    {
      d -= 3.0;
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = (int) ((1 - d) * 0xFF);
      this->rainbow[i][2] = 0x00;
    }
    else
    {
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = 0x00;
      this->rainbow[i][2] = 0x00;
    }
  }
  
  return;
}


// Update with current sensnet data
int LadarSink::updateSensnet(sensnet_t *sensnet)
{
  int blobId, blobLen;
  
  // Look at the latest data
  if (sensnet_peek(sensnet, this->sensorId, this->blobType, &blobId, &blobLen) != 0)
    return -1;

  // Is the data valid?
  if (blobId < 0)
    return 0;

  // Is the data new?
  if (blobId == this->blobId)
    return 0;
    
  if (sensnet_read(sensnet, this->sensorId, this->blobType,
                   blobId, sizeof(this->blob), &this->blob) != 0)
    return -1;
    
  this->blobId = blobId;
  this->dirty = true;
  
  return 0;
}


// Generate range point cloud (local frame)
void LadarSink::predrawPointCloud()
{
  int list;
  int i, k;
  float pa, pr, px, py, pz;
  LadarRangeBlob *blob;

  // TESTING
  this->maxClouds = 750;
  
  if (this->cloudList == 0)
  {
    this->cloudList = glGenLists(this->maxClouds);
    if (this->cloudList == 0)
    {
      ERROR("unable to create display lists");
      return;
    }
  }

  blob = &this->blob;

  // Pick a list to use based on the scan id.
  list = this->cloudList + (this->numClouds++) % this->maxClouds;
  
  glNewList(list, GL_COMPILE);

  glPushAttrib(GL_POINT_BIT);
  glPointSize(2.0);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glColor3f(0, 0, 1);

  glBegin(GL_POINTS);
  
  for (i = 0; i < blob->num_points; i++)
  {
    pa = blob->points[i][0];
    pr = blob->points[i][1];
    
    if (pr > 0 && pr < 60.0) // MAGIC 
    {
      // Compute points in sensor frame
      sensnet_ladar_br_to_xyz(blob, pa, pr, &px, &py, &pz);

      // Transform to vehicle frame
      sensnet_ladar_sensor_to_vehicle(blob, px, py, pz, &px, &py, &pz);
            
      // Select color based on elevation in vehicle frame
      k = (int) (-0x10000 * pz / 2.0); // MAGIC
      if (k < 0x0000) k = 0x0000;
      if (k > 0xFFFF) k = 0xFFFF;
      glColor3ub(this->rainbow[k][0], this->rainbow[k][1], this->rainbow[k][2]);

      // Transform to local frame
      sensnet_ladar_vehicle_to_local(blob, px, py, pz, &px, &py, &pz);
            
      glVertex3f(px, py, pz);
    }
  }

  glEnd();

  glPopAttrib();

  glEndList();
    
  return;
}


// Predraw sensor footprint
void LadarSink::predrawFootprint()
{
  int i;
  float a, b, d;
  float pa, pr;
  float px, py, pz;

  if (this->blobId < 0)
    return;

  if (this->footList == 0)
    this->footList = glGenLists(1);
  glNewList(this->footList, GL_COMPILE);

  a = this->blob.sens2veh[2][0];
  b = this->blob.sens2veh[2][1];
  d = this->blob.sens2veh[2][3] - VEHICLE_TIRE_RADIUS;

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glColor4f(0, 0, 1, 0.50);
  glBegin(GL_POLYGON);
  
  glVertex3f(0, 0, 0);  
  for (i = -90; i < +90; i++)
  {
    // Range and bearing of point on the ground (sensor frame)
    pa = i * M_PI/180;
    pr = -d / (a * cos(pa) + b * sin(pa));
    if (pr < 0 || pr > 80 || !finite(pr)) // MAGIC
      pr = 80;
    sensnet_ladar_br_to_xyz(&this->blob, pa, pr, &px, &py, &pz);
    glVertex3f(px, py, pz);
  }
  
  glEnd();
  glDisable(GL_BLEND);
  
  glEndList();
  
  return;
}
