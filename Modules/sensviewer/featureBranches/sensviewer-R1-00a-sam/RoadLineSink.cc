 
/* 
 * Desc: Sink for road lines
 * Date: 16 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/gl.h>

#include <alice/AliceConstants.h>
#include <skynet/sn_types.h>
#include <interfaces/SensnetTypes.h>

#include "RoadLineSink.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
RoadLineSink::RoadLineSink()
{
  memset(this, 0, sizeof(*this));

  this->sensorId = SENSNET_SKYNET_SENSOR;
  this->blobType = SNroadLine;
  this->blobId = -1;
  
  return;
}


// Update with current sensnet data
int RoadLineSink::sensnetUpdate(sensnet_t *sensnet)
{
  int blobId, blobLen;
  
  // Look at the latest data
  if (sensnet_peek(sensnet, this->sensorId, this->blobType, &blobId, &blobLen) != 0)
    return -1;

  // Is the data valid?
  if (blobId < 0)
    return 0;

  // Is the data new?
  if (blobId == this->blobId)
    return 0;

  // Read the data
  if (sensnet_read(sensnet, this->sensorId, this->blobType,
                   blobId, sizeof(this->blob), &this->blob) != 0)
    return -1;
    
  this->blobId = blobId;
  this->dirty = true;
  
  return 0;
}


// Generate lines (local frame)
void RoadLineSink::predrawLines()
{
  int i;
  
  if (this->blobId < 0)
    return;

  if (this->lineList == 0)
    this->lineList = glGenLists(1);
  glNewList(this->lineList, GL_COMPILE);

  // TODO better line rendering
  
  // Draw lines 
  glColor3f(1, 0, 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glLineWidth(10);
  glBegin(GL_LINES);  
  for (i = 0; i < blob.num_lines; i++)
  {
    glVertex3f(blob.lines[i].a[0], blob.lines[i].a[1], blob.lines[i].a[2]);
    glVertex3f(blob.lines[i].b[0], blob.lines[i].b[1], blob.lines[i].b[2]);    
  }
  glEnd();
  glLineWidth(1);
  
  glEndList();
  
  return;
}

