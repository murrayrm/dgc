/* 
 * Desc: GL drawing utilities
 * Date: 18 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <string.h>
#include <GL/glut.h>

#include "glUtils.hh"



// Switch to given frame (homogeneous transform)
void glPushFrame(float m[4][4])
{
  int i, j;
  float t[16];

  // Transpose to column-major order for GL
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);

  return;
}


// Revert to previous frame
void glPopFrame()
{
  glPopMatrix();  
  return;
}


// Draw a set of axes
void glDrawAxes(float size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Draw a text box
void glDrawText(float size, const char *text)
{
  int i, count;
  float sx, sy;

  // Compute the scale factors in x/y directions, based on the 'M'
  // character.
  sx = glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, 'M');
  sy = sx * 1.3;
    
  glPushMatrix();
  glScalef(size / sx, size / sy, 1);
  glTranslatef(0, -sy, 0);

  glPushMatrix();
    
  for (i = 0, count = 0; i < (int) strlen(text); i++, count++)
  {
    if (text[i] == '\n')
    {
      glPopMatrix();
      glTranslatef(0, -sy, 0);
      glPushMatrix();
      count = 0;      
    }
    else
    {
      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[i]);
    }
  }

  glPopMatrix();
  glPopMatrix();

  return;
}


// Compute Catmull-Rom spline interpolation
// From: http://www.lighthouse3d.com/opengl/maths/index.php?catmullrom
float catmullRomSpline(float x, float v0, float v1, float v2, float v3)
{
  //const double M11 = 0.0;
  const double M12 = 1.0;
  //const double M13 = 0.0;
  //const double M14 = 0.0;
  const double M21 =-0.5;
  //const double M22 = 0.0;
  const double M23 = 0.5;
  //const double M24 = 0.0;
  const double M31 = 1.0;
  const double M32 =-2.5;
  const double M33 = 2.0;
  const double M34 =-0.5;
  const double M41 =-0.5;
  const double M42 = 1.5;
  const double M43 =-1.5;
  const double M44 = 0.5;
  
  float c1,c2,c3,c4;

	c1 =  	      M12*v1;
	c2 = M21*v0          + M23*v2;
	c3 = M31*v0 + M32*v1 + M33*v2 + M34*v3;
	c4 = M41*v0 + M42*v1 + M43*v2 + M44*v3;

	return (((c4*x + c3)*x +c2)*x + c1);
}


// Draw a Catmull-Rom spline curve.
void glDrawCatmullRomSpline(float x0, float y0, float z0,
                            float x1, float y1, float z1,
                            float x2, float y2, float z2,
                            float x3, float y3, float z3, int steps)
{
  int i;
  float qx, qy, qz;
        
  glBegin(GL_LINE_STRIP);
  for (i = 0; i <= steps; i++)
  {
    qx = catmullRomSpline(i / (float) steps, x0, x1, x2, x3);
    qy = catmullRomSpline(i / (float) steps, y0, y1, y2, y3);
    qz = catmullRomSpline(i / (float) steps, z0, z1, z2, z3); 
    glVertex3f(qx, qy, qz);
  }
  glEnd();
  
  return;
}
