
/* 
 * Desc: Sink for stereo blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glu.h>

#include <alice/AliceConstants.h>
#include <interfaces/SensnetTypes.h>
#include "StereoSink.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
StereoSink::StereoSink()
{
  memset(this, 0, sizeof(*this));

  this->sensorId = SENSNET_NULL_SENSOR;
  this->blobId = -1;
  
  return;
}


// Update with current sensnet data
int StereoSink::sensnetUpdate(sensnet_t *sensnet)
{
  int blobId, blobLen;

  // Look at the latest data
  if (sensnet_peek(sensnet, this->sensorId, this->blobType, &blobId, &blobLen) != 0)
    return -1;
    
  // Is the data valid?
  if (blobId < 0)
    return 0;
    
  // Is the data new?
  if (blobId == this->blobId)
    return 0;
    
  if (sensnet_read(sensnet, this->sensorId, this->blobType,
                   blobId, sizeof(this->blob), &this->blob) != 0)
    return -1;

  this->blobId = blobId;
  this->dirty = true;
  
  return 0;
}


// Generate range point cloud in camera frame
void StereoSink::predrawPointCloud()
{
  int i, j;
  uint8_t *cpix;
  uint16_t *dpix;
  float px, py, pz;
  StereoImageBlob *blob;

  if (this->blobId < 0)
    return;
  
  if (this->cloudList == 0)
    this->cloudList = glGenLists(1);

  glNewList(this->cloudList, GL_COMPILE);

  glPushAttrib(GL_POINT_BIT);
  glPointSize(2.0);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_POINTS);

  blob = &this->blob;
  
  cpix = blob->colorData;
  dpix = blob->dispData;
  for (j = 0; j < blob->rows; j++)
  {              
    for (i = 0; i < blob->cols; i++)
    {      
      if (dpix[0] < 0x1000)
      {
        StereoImageBlobImageToSensor(blob, i, j, dpix[0], &px, &py, &pz);
        if (blob->colorChannels == 1)
          glColor3ub(cpix[0], cpix[0], cpix[0]);
        else
          glColor3ub(cpix[0], cpix[1], cpix[2]);            
        glVertex3f(px, py, pz);
      }
      cpix += blob->colorChannels;
      dpix += 1;
    }
  }

  glEnd();
  glPopAttrib();

  glEndList();
    
  return;
}


// Predraw camera footprint on ground plane
void StereoSink::predrawFootprint()
{
  int i;
  float pi, pj;
  float px, py, pz;
  float vertices[4][2];
  int cols, rows;
  float cx, cy, sx, sy;
  float m[4][4];

  if (this->blobId < 0)
    return;

  if (this->footList == 0)
    this->footList = glGenLists(1);
  glNewList(this->footList, GL_COMPILE);

  cols = this->blob.cols;
  rows = this->blob.rows;
  cx = this->blob.cx;
  cy = this->blob.cy;
  sx = this->blob.sx;
  sy = this->blob.sy;
  memcpy(m, this->blob.sens2veh, sizeof(m));

  // Offset the camera to vehicle transform to find the nominal ground plane
  m[2][3] -= VEHICLE_TIRE_RADIUS - 0.01; // HACK

  vertices[0][0] = 0;
  vertices[0][1] = rows;
  vertices[1][0] = cols;
  vertices[1][1] = rows;
  vertices[2][0] = cols;
  vertices[2][1] = 0;
  vertices[3][0] = 0;
  vertices[3][1] = 0;

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  glColor4f(0, 0, 1, 0.50);
  glBegin(GL_POLYGON);
    
  for (i = 0; i < 4; i++)
  {
    pi = vertices[i][0];
    pj = vertices[i][1];

    // Some corners of this image may not intersect with the plane, so
    // try a few different values for the row to find a pixel that
    // does.  The equation below computes the ray/z-plane intersection
    // in camera coordinates, assuming the matrix m specifies the
    // camera pose relative to the plane.
    px = py = pz = 0;
    for (; pj <= rows; pj += 4)
    {
      px = (pi - cx) / sx;
      py = (pj - cy) / sy;
      pz = m[2][0] * px + m[2][1] * py + m[2][2];
      pz = -m[2][3] / pz;
      if (pz > 0)
        break;
    }
    
    px *= pz;
    py *= pz;

    glVertex3f(px, py, pz);
  }
  
  glEnd();
  glDisable(GL_BLEND);
  
  glEndList();
    
  return;
}
