 
/* 
 * Desc: Render RNDF data
 * Date: 16 Apr 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <GL/glu.h>
#include <frames/vec3.h>
#include <frames/pose3.h>

#include <rndf/RNDF.hh>

#include "glUtils.hh"
#include "RndfPainter.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
RndfPainter::RndfPainter()
{
  this->list = 0;
  
  return;
}


// Load RNDF file
int RndfPainter::load(char *filename)
{
  // Create RNDF object
  this->rndf = new std::RNDF();
  assert(this->rndf);

  // Load from file
  if (filename)
  {
    if (!this->rndf->loadFile(filename))
      return ERROR("unable to load %s", filename);
  }
  
  return 0;
}

  
// Draw RNDF in local frame
int RndfPainter::draw(VehicleState state)
{
  // TODO; we need to predraw periodically to account for drift.
  // If this is our first time through the function, run the predraw.
  if (this->list == 0)
    this->predraw(state);
  
  glCallList(this->list);
  
  return 0;
}


// TODO REMOVE
// Generate the interpolated value for a cardinal spline.
float cardinalSpline(float c, float t, float p0, float p1, float p2, float p3)
{
  float m1, m2;
  float ha, hb, hc, hd;

  m1 = (1 - c) * (p2 - p0) / 2;
  m2 = (1 - c) * (p3 - p1) / 2;

  ha =  2*t*t*t - 3*t*t + 1;
  hb =    t*t*t - 2*t*t + t;
  hc = -2*t*t*t + 3*t*t;
  hd =    t*t*t -   t*t;
  
  return ha * p1 + hb * m1 + hc * p2 + hd * m2;  
}


// Pre-draw to a display list
int RndfPainter::predraw(VehicleState state)
{
  pose3_t lpose, gpose, pose;

  if (this->list == 0)
    this->list = glGenLists(1);

  glNewList(this->list, GL_COMPILE);
  
  // Vehicle global pose
  gpose.pos = vec3_set(state.utmNorthing, state.utmEasting, state.utmAltitude);
  gpose.rot = quat_from_rpy(state.utmRoll, state.utmPitch, state.utmYaw);

  // Vehicle local pose
  lpose.pos = vec3_set(state.localX, state.localY, state.localZ);
  lpose.rot = quat_from_rpy(state.localRoll, state.localPitch, state.localYaw);

  MSG("local %f %f %f", state.localX, state.localY, state.localZ);
  MSG("global %f %f %f", state.utmNorthing, state.utmEasting, state.utmAltitude);
  
  // Transform from global to local frame
  pose = pose3_mul(lpose, pose3_inv(gpose));

  int sn, ln, wn;
  std::Segment *segment;
  std::Lane *lane;
  std::Waypoint *waypoint;
  vec3_t pos;

  // Iterate through segments
  for (sn = 0; sn < (int) this->rndf->getAllSegments().size(); sn++)
  {
    segment = this->rndf->getAllSegments()[sn];

    // Iterate through lanes
    for (ln = 0; ln < (int) segment->getAllLanes().size(); ln++)
    {
      lane = segment->getAllLanes()[ln];

      /* TODO
      // Draw a line along the center of the lane
      glColor3f(0, 0, 1);
      glBegin(GL_LINE_STRIP);
      for (wn = 0; wn < (int) lane->getAllWaypoints().size(); wn++)
      {
      waypoint = lane->getAllWaypoints()[wn];
      pos.x = waypoint->getNorthing();
      pos.y = waypoint->getEasting();
      pos.z = state.utmAltitude;
      pos = vec3_transform(pose, pos);
      glVertex3d(pos.x, pos.y, pos.z);
      }
      glEnd();
      */
      
      // Draw the waypoints
      glPointSize(4.0);
      glBegin(GL_POINTS);
      for (wn = 0; wn < (int) lane->getAllWaypoints().size(); wn++)
      {
        waypoint = lane->getAllWaypoints()[wn];
        pos.x = waypoint->getNorthing();
        pos.y = waypoint->getEasting();
        pos.z = state.utmAltitude;
        pos = vec3_transform(pose, pos);
        if (waypoint->isStopSign())
          glColor3f(1, 0, 0);
        else
          glColor3f(0, 1, 0);
        glVertex3d(pos.x, pos.y, pos.z);        
      }
      glEnd();

      /* REMOVE
         glColor3f(0, 0, 1);
         glBegin(GL_LINE_STRIP);
         for (wn = 0; wn < (int) lane->getAllWaypoints().size(); wn++)
         {
         std::Waypoint *wp0, *wp1;
         vec3_t p0, p1;
        
         wp0 = lane->getAllWaypoints()[wn + 0];
         wp1 = lane->getAllWaypoints()[wn + 1];

         p0 = vec3_set(wp0->getNorthing(), wp0->getEasting(), state.utmAltitude);
         p0 = vec3_transform(pose, p0);
         p1 = vec3_set(wp1->getNorthing(), wp1->getEasting(), state.utmAltitude);
         p1 = vec3_transform(pose, p1);

         waypoint = lane->getAllWaypoints()[wn];
         pos.x = waypoint->getNorthing();
         pos.y = waypoint->getEasting();
         pos.z = state.utmAltitude;
         pos = vec3_transform(pose, pos);
         glVertex3d(pos.x, pos.y, pos.z);
         }
         glEnd();
      */

      // Draw lanes
      for (wn = 0; wn < (int) lane->getAllWaypoints().size() - 3; wn++)
      {
        std::Waypoint *wp0, *wp1, *wp2, *wp3;
        vec3_t p0, p1, p2, p3;
        
        wp0 = lane->getAllWaypoints()[wn + 0];
        wp1 = lane->getAllWaypoints()[wn + 1];
        wp2 = lane->getAllWaypoints()[wn + 2];
        wp3 = lane->getAllWaypoints()[wn + 3];

        p0 = vec3_set(wp0->getNorthing(), wp0->getEasting(), state.utmAltitude);
        p0 = vec3_transform(pose, p0);
        p1 = vec3_set(wp1->getNorthing(), wp1->getEasting(), state.utmAltitude);
        p1 = vec3_transform(pose, p1);
        p2 = vec3_set(wp2->getNorthing(), wp2->getEasting(), state.utmAltitude);
        p2 = vec3_transform(pose, p2);
        p3 = vec3_set(wp3->getNorthing(), wp3->getEasting(), state.utmAltitude);
        p3 = vec3_transform(pose, p3);

        glColor3f(1, 1, 1);
        if (wn == 0)
        {
          glBegin(GL_LINE_STRIP);
          glVertex3d(p0.x, p0.y, p0.z);
          glVertex3d(p1.x, p1.y, p1.z);
          glEnd();
        }
        if (wn == (int) lane->getAllWaypoints().size() - 4)
        {
          glBegin(GL_LINE_STRIP);
          glVertex3d(p2.x, p2.y, p2.z);
          glVertex3d(p3.x, p3.y, p3.z);
          glEnd();
        }       
        glDrawCatmullRomSpline(p0.x, p0.y, p0.z,
                               p1.x, p1.y, p1.z, 
                               p2.x, p2.y, p2.z,
                               p3.x, p3.y, p3.z, 10);
      }


      // TESTING
      // Draw intersections
      int wm;
      std::Waypoint *wp0, *wp1;
      std::GPSPoint *wp2, *wp3;
      vec3_t p0, p1, p2, p3;
            
      for (wn = 0; wn < (int) lane->getAllWaypoints().size() - 1; wn++)
      {
        wp0 = lane->getAllWaypoints()[wn + 0];
        wp1 = lane->getAllWaypoints()[wn + 1];

        p0 = vec3_set(wp0->getNorthing(), wp0->getEasting(), state.utmAltitude);
        p0 = vec3_transform(pose, p0);
        p1 = vec3_set(wp1->getNorthing(), wp1->getEasting(), state.utmAltitude);
        p1 = vec3_transform(pose, p1);

        for (wm = 0; wm < (int) wp1->getEntryPoints().size(); wm++)
        {
          wp2 = wp1->getEntryPoints()[wm];

          // TODO: is this correct?
          wp3 = this->rndf->getWaypoint(wp2->getSegmentID(),
                                        wp2->getLaneID(),
                                        wp2->getWaypointID() + 1);

          printf("%d.%d.%d %d.%d.%d : %d.%d.%d %d.%d.%d\n",
                 wp0->getSegmentID(), wp0->getLaneID(), wp0->getWaypointID(),
                 wp1->getSegmentID(), wp1->getLaneID(), wp1->getWaypointID(),
                 wp2->getSegmentID(), wp2->getLaneID(), wp2->getWaypointID(),
                 wp3->getSegmentID(), wp3->getLaneID(), wp3->getWaypointID());

          p2 = vec3_set(wp2->getNorthing(), wp2->getEasting(), state.utmAltitude);
          p2 = vec3_transform(pose, p2);
          p3 = vec3_set(wp3->getNorthing(), wp3->getEasting(), state.utmAltitude);
          p3 = vec3_transform(pose, p3);

          glColor3f(0, 1, 0);
          glDrawCatmullRomSpline(p0.x, p0.y, p0.z,
                                 p1.x, p1.y, p1.z, 
                                 p2.x, p2.y, p2.z,
                                 p3.x, p3.y, p3.z, 10);

          /*
            glBegin(GL_LINE_STRIP);
            glVertex3f(p0.x, p0.y, p0.z);
            glVertex3f(p1.x, p1.y, p1.z);
            glEnd();
          */
        }
      }      
    }    
  }


  glEndList();
  
  return 0;
}
