/* 
 * Desc: GL drawing utilities
 * Date: 18 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef UTILS_HH
#define UTILS_HH


/// @brief Draw a set of axes.
///
/// Draws a triplet of lines along the XYZ axes, with the origin at
/// (0, 0, 0).
///
/// @param[in] size Length of axes lines, in meters.
void glDrawAxes(float size);


/// @brief Draw a text box
///
/// Draws multiline text in the XY plane, with the first character at
/// (0, 0, 0).
///
/// @param[in] size Text size, in meters.
/// @param[in] text Null-terminated string (newlines ok).
void glDrawText(float size, const char *text);


#endif
