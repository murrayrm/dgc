
/* 
 * Desc: Window for displaying 3D stuff
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Multiline_Output.H>
#include <jplv/Fl_Glv_Window.H>
#include <frames/pose3.h>

#include "sensnet/sensnet.h"

#include "StereoSink.hh"
#include "LadarSink.hh"
#include "RoadLineSink.hh"
#include "ObsMapSink.hh"

#include "MapPainter.hh"


// Widget for displaying images
class WorldWin : public Fl_Group
{
  public:

  // Constructor
  WorldWin(int x, int y, int w, int h, int menuh);

  public:

  // Initialize sensnet stuff
  int init(sensnet_t *sensnet, const char *gisPath);

  // Enable/disable a sink
  int toggle(int menuId);

  // Update blob data
  int update();

  public:
  
  // Handle menu options
  static void onSensor(Fl_Widget *w, int option);
  
  // Handle menu options
  static void onView(Fl_Widget *w, int option);

  // Prepare for drawing
  static void onPrepare(Fl_Glv_Window *win, WorldWin *self);

  // Draw window
  static void onDraw(Fl_Glv_Window *win, WorldWin *self);

  private:

  // Switch to the given frame; i.e., sets up the GL coordinate
  // transforms such that (0, 0, 0) will map to the given pose.
  void pushFrame(pose3_t pose);

  // Switch to given frame (homogeneous transform)
  void pushFrame(float m[4][4]);

  // Switch to given local frame
  void pushFrameLocal(VehicleState state);
    
  // Revert to previous frame
  void popFrame();

  // Predraw state data as text
  void drawStateText();

  // Predraw the robot
  void predrawAlice();

  // Predraw a ground grid
  void predrawGrid();

  public:

  // Local menu
  Fl_Menu_Bar *menubar;
  
  // GL window
  Fl_Glv_Window *glwin;

  // View options
  bool viewVehicle, viewLocal;
  bool viewStatus, viewFootprint, viewCloud;
  bool viewGrid, viewMap;

  // Sensnet handle
  sensnet_t *sensnet;
  
  // List of stereo sinks
  int numStereoSinks;
  StereoSink *stereoSinks[8];

  // List of ladar sinks
  int numLadarSinks;
  LadarSink *ladarSinks[8];

  // Road line sink
  RoadLineSink *roadLineSink;

  // Obstacle map sink
  ObsMapSink *obsMapSink;
  
  // Most recent state data
  VehicleState state;

  // Painter for prior map data
  MapPainter mapPainter;

  // Some display lists
  int aliceList, gridList;

  // Screen capture counter
  int capCount;
};
