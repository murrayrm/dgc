
/* 
 * Desc: Window for displaying 3D stuff
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <GL/glut.h>

//#include <jplv/jplv_image.h>

#include <alice/AliceConstants.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/RoadLineMsg.h>

#include "glUtils.hh"
#include "WorldWin.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Menu commands
enum
{  
  VIEW_VEHICLE = 0x2000,
  VIEW_LOCAL,
  VIEW_STATUS,
  VIEW_IMAGES,
  VIEW_CLOUD,
  VIEW_LADAR_ID_COLOR,
  VIEW_LADAR_ELEV_COLOR,
  VIEW_FOOTPRINT,
  VIEW_GRID,
  VIEW_MAP,
  VIEW_RNDF,
  VIEW_LINEOFSITE,

  VIEW_STEREO_TRUE,
  VIEW_STEREO_ELEVATION,
  
  ACTION_CAPTURE,

  SINK_LF_SHORT_STEREO,
  SINK_RF_SHORT_STEREO,
  SINK_MF_LONG_STEREO,
  SINK_MF_MEDIUM_STEREO,
  
  SINK_LF_BUMPER_LADAR,
  SINK_RF_BUMPER_LADAR,
  SINK_MF_BUMPER_LADAR,
  SINK_LF_ROOF_LADAR,
  SINK_RF_ROOF_LADAR,
  SINK_RIEGL,
  SINK_RADAR,
  SINK_REAR_BUMPER_LADAR,
  SINK_PTU_STATE,
  SINK_PTU_LADAR,
  
  SINK_ROADLINE_PERCEPTOR,
  // REMOVE SINK_OBSMAP_PERCEPTOR,
};


// Constructor
WorldWin::WorldWin(int x, int y, int w, int h, int menuh)
    : Fl_Group(x, y, w, h)
{
  begin();
  this->menubar = new Fl_Menu_Bar(x, y, w, menuh);
  this->menubar->user_data(this);
  this->glwin = new Fl_Glv_Window(x, y + menuh, w, h - menuh, this,
                                  (Fl_Callback*) onPrepare, (Fl_Callback*) onDraw);
  this->glwin->user_data(this);  
  this->resizable(this->glwin);  
  end();  
  
  // Use DGC convention
  this->glwin->set_up("-z");
  
  // Things are both near and far away, so set a generous clipping range
  this->glwin->set_clip(1, 500);

  // Set initial camera position
  this->glwin->eye_x = -20;
  this->glwin->eye_y = -20;
  this->glwin->eye_z = -20;

  this->numStereoSinks = 0;      
  if (true)
  {
    // Create bumper stereo sinks
    this->menubar->add("&Feeders/LF Short Stereo", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_LF_SHORT_STEREO, FL_MENU_TOGGLE | FL_MENU_VALUE);
    this->stereoSinks[this->numStereoSinks++] =
      new StereoSink(SINK_LF_SHORT_STEREO, SENSNET_LF_SHORT_STEREO);

    this->menubar->add("&Feeders/RF Short Stereo", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_RF_SHORT_STEREO, FL_MENU_TOGGLE | FL_MENU_VALUE);
    this->stereoSinks[this->numStereoSinks++] =
      new StereoSink(SINK_RF_SHORT_STEREO, SENSNET_RF_SHORT_STEREO);

    // Create roof stereo sinks
    this->menubar->add("&Feeders/MF Long Stereo", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_MF_LONG_STEREO, FL_MENU_TOGGLE | FL_MENU_VALUE);
    this->stereoSinks[this->numStereoSinks++] =
      new StereoSink(SINK_MF_LONG_STEREO, SENSNET_MF_LONG_STEREO);

    this->menubar->add("&Feeders/MF Medium Stereo", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_MF_MEDIUM_STEREO, FL_MENU_TOGGLE | FL_MENU_VALUE
                       | FL_MENU_DIVIDER);
    this->stereoSinks[this->numStereoSinks++] =
      new StereoSink(SINK_MF_MEDIUM_STEREO, SENSNET_MF_MEDIUM_STEREO);

  }

  this->numLadarSinks = 0;  
  if (true)
  {
    // Create ladar sinks and hook them up with menu items
    this->menubar->add("&Feeders/LF Bumper Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_LF_BUMPER_LADAR, FL_MENU_TOGGLE | FL_MENU_VALUE);
    this->ladarSinks[this->numLadarSinks++] =
      new LadarSink(SINK_LF_BUMPER_LADAR, SENSNET_LF_BUMPER_LADAR);
    
    this->menubar->add("&Feeders/RF Bumper Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_RF_BUMPER_LADAR, FL_MENU_TOGGLE | FL_MENU_VALUE);
    this->ladarSinks[this->numLadarSinks++] =
      new LadarSink(SINK_RF_BUMPER_LADAR, SENSNET_RF_BUMPER_LADAR);

    this->menubar->add("&Feeders/MF Bumper Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_MF_BUMPER_LADAR, FL_MENU_TOGGLE | FL_MENU_VALUE);
    this->ladarSinks[this->numLadarSinks++] =
      new LadarSink(SINK_MF_BUMPER_LADAR, SENSNET_MF_BUMPER_LADAR);

    this->menubar->add("&Feeders/LF Roof Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_LF_ROOF_LADAR, FL_MENU_TOGGLE | FL_MENU_VALUE);
    this->ladarSinks[this->numLadarSinks++] =
      new LadarSink(SINK_LF_ROOF_LADAR, SENSNET_LF_ROOF_LADAR);

    this->menubar->add("&Feeders/Riegl", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_RIEGL, FL_MENU_TOGGLE | FL_MENU_VALUE);
    this->ladarSinks[this->numLadarSinks++] =
      new LadarSink(SINK_RIEGL, SENSNET_RIEGL);

    this->menubar->add("&Feeders/RF Roof Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_RF_ROOF_LADAR, FL_MENU_TOGGLE | FL_MENU_VALUE
                       | FL_MENU_DIVIDER);
    this->ladarSinks[this->numLadarSinks++] =
      new LadarSink(SINK_RF_ROOF_LADAR, SENSNET_RF_ROOF_LADAR);    

    this->menubar->add("&Feeders/Rear Bumper Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_REAR_BUMPER_LADAR, FL_MENU_TOGGLE | FL_MENU_VALUE
                       | FL_MENU_DIVIDER);
    this->ladarSinks[this->numLadarSinks++] =
      new LadarSink(SINK_REAR_BUMPER_LADAR, SENSNET_REAR_BUMPER_LADAR);    

    this->menubar->add("&Feeders/Ptu Ladar", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_PTU_LADAR, FL_MENU_TOGGLE | FL_MENU_VALUE
                       | FL_MENU_DIVIDER);
    this->ladarSinks[this->numLadarSinks++] =
      new LadarSink(SINK_PTU_LADAR, SENSNET_PTU_LADAR);    

  }

  this->numRadarSinks = 0;
  if (true)
  {
    this->menubar->add("&Feeders/Radar", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_RADAR, FL_MENU_TOGGLE | FL_MENU_VALUE | FL_MENU_DIVIDER);
    this->radarSinks[this->numRadarSinks++] =
      new RadarSink(SINK_RADAR, SENSNET_RADAR);

  }

  this->numPTUSinks = 0;
  if (true)
  {
    this->menubar->add("&Feeders/PTU", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_PTU_STATE, FL_MENU_TOGGLE | FL_MENU_VALUE | FL_MENU_DIVIDER);
    this->ptuSinks[this->numPTUSinks++] = 
      new PTUSink(SINK_PTU_STATE, SENSNET_MF_PTU);

  }

  if (true)
  {
    // Create road line sink
    this->menubar->add("&Perceptors/RoadLine", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_ROADLINE_PERCEPTOR, FL_MENU_TOGGLE | FL_MENU_VALUE);
    this->roadLineSink = new RoadLineSink(SINK_ROADLINE_PERCEPTOR);
  }

  /* REMOVE
  if (true)
  {
    // Create obstacle sink
    this->menubar->add("&Perceptors/Obstacle", 0, (Fl_Callback*) WorldWin::onSensor,
                       (void*) SINK_OBSMAP_PERCEPTOR, FL_MENU_TOGGLE);
    this->obsMapSink = new ObsMapSink(SINK_OBSMAP_PERCEPTOR);
  }
  */

  // Create the view menu
  this->menubar->add("&View/Vehicle frame", 0, (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_VEHICLE, FL_MENU_RADIO | FL_MENU_VALUE);
  this->menubar->add("&View/Local frame", 0, (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_LOCAL, FL_MENU_RADIO | FL_MENU_DIVIDER);
  this->viewVehicle = true;

  this->menubar->add("&View/Status", FL_CTRL + 's', (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_STATUS, FL_MENU_TOGGLE | FL_MENU_VALUE);
  this->viewStatus = true;

  this->menubar->add("&View/Images", FL_CTRL + 'i', (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_IMAGES, FL_MENU_TOGGLE | FL_MENU_VALUE);
  this->viewImages = true;

  this->menubar->add("&View/Cloud", FL_CTRL + 'c', (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_CLOUD, FL_MENU_TOGGLE | FL_MENU_VALUE);
  this->viewCloud = true;

  this->menubar->add("&View/Ladar ID color", 0, (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_LADAR_ID_COLOR, FL_MENU_RADIO);
  this->ladarUseIdColor = false;

  this->menubar->add("&View/Ladar elevation color", 0, (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_LADAR_ELEV_COLOR, FL_MENU_RADIO | FL_MENU_VALUE);
  this->ladarUseElevColor = true;

  this->menubar->add("&View/Footprints", FL_CTRL + 'f', (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_FOOTPRINT, FL_MENU_TOGGLE);
  this->viewFootprint = false;

  this->menubar->add("&View/Grid", FL_CTRL + 'g', (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_GRID, FL_MENU_TOGGLE | FL_MENU_VALUE);
  this->viewGrid = true;

  this->menubar->add("&View/Map", FL_CTRL + 'm', (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_MAP, FL_MENU_TOGGLE | FL_MENU_VALUE);
  this->viewMap = true;

  this->menubar->add("&View/RNDF", FL_CTRL + 'r', (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_RNDF, FL_MENU_TOGGLE | FL_MENU_VALUE | FL_MENU_DIVIDER);
  this->viewRndf = true;

  this->menubar->add("&View/Line of Site", FL_CTRL + 'l', (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_LINEOFSITE, FL_MENU_TOGGLE | FL_MENU_VALUE | FL_MENU_DIVIDER);
  this->viewLineOfSite = true;

  // Create stereo view options
  this->menubar->add("&View/Stereo/True color", 0, (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_STEREO_TRUE, FL_MENU_RADIO | FL_MENU_VALUE);
  this->menubar->add("&View/Stereo/Elevation color", 0, (Fl_Callback*) WorldWin::onView,
                     (void*) VIEW_STEREO_ELEVATION, FL_MENU_RADIO);
  
  // Add action menu items
  this->menubar->add("&View/Capture", FL_CTRL + 'c', (Fl_Callback*) WorldWin::onAction,
                     (void*) ACTION_CAPTURE, FL_MENU_TOGGLE);
  this->capEnable = false;
  
  this->aliceList = this->gridList = 0;

  return;
}


// Initialize stuff
int WorldWin::init(const struct gengetopt_args_info *options,
                   sensnet_t *sensnet, sensnet_replay_t *replay,
                   const char *gisPath, char *rndfFile)
{
  int i;
  const Fl_Menu_Item *item;

  this->options = options;

  // Initialize state
  memset(&this->state, 0, sizeof(this->state));
  
  // Store the sensnet pointer for later use
  this->sensnet = sensnet;
  this->replay = replay;

  // Subscribe to state
  if (sensnet)
  {
    if (sensnet_join(sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
      ERROR("unable to subscribe to state");
  }

  // Enable any sinks that are already checked in the menu
  for (i = 0; i < this->menubar->menu()->size(); i++)
  {
    item = this->menubar->menu() + i;
    if (item->value())
      this->toggle((int) item->user_data());
  }

  // Load map data  
  this->mapPainter.load(gisPath);

  // Load RNDF data
  this->rndfPainter.load(rndfFile);
 
  return 0;
}


// Enable/disable a sink
int WorldWin::toggle(int menuId)
{
  int i;

  // Check stereo sinks
  for (i = 0; i < this->numStereoSinks; i++)
  {
    StereoSink *sink = this->stereoSinks[i];
    if (sink->menuId == menuId)
      sink->enable(this->sensnet, this->replay, !sink->enabled);
  }

  // Check ladar sinks
  for (i = 0; i < this->numLadarSinks; i++)
  {
    LadarSink *sink = this->ladarSinks[i];
    if (sink->menuId == menuId)
      sink->enable(this->sensnet, this->replay, !sink->enabled);
  }

  // Check radar sinks
  for (i = 0; i < this->numRadarSinks; i++)
  {
    RadarSink *sink = this->radarSinks[i];
    if (sink->menuId == menuId)
      sink->enable(this->sensnet, this->replay, !sink->enabled);
  }

  for(i = 0; i< this->numPTUSinks; i++)
    {
      // Check PTU sinks
      PTUSink *sink = this->ptuSinks[i];
      if (sink->menuId == menuId)
	sink->enable(this->sensnet, this->replay, !sink->enabled);
    }

  // Check roadline sink 
  if (true)
  {
    RoadLineSink *sink = this->roadLineSink;
    if (sink->menuId == menuId)
      sink->enable(this->sensnet, this->replay, !sink->enabled);
  }

  /* REMOVE
  // Check obsmap sink 
  if (true)
  {
    ObsMapSink *sink = this->obsMapSink;
    if (sink->menuId == menuId)
      sink->enable(this->sensnet, this->replay, !sink->enabled);
  }
  */

  return 0;
}


// Update blob data using sensnet
int WorldWin::update()
{
  int i;
  bool dirty;
  
  dirty = false;

  // Read state directly from sensnet
  if (this->sensnet)
  {
    int blobId;
    VehicleState state;
    if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR,
                     SNstate, &blobId, sizeof(state), &state) == 0)
    {
      if (blobId >= 0 && state.timestamp > this->state.timestamp)
      {
        this->state = state;
        dirty = true;
      }
    }
  }
  
  // Update currently subscribed stereo sinks with new data.  If the
  // sink has new state data, grab a copy of that as well.
  for (i = 0; i < this->numStereoSinks; i++)
  {
    StereoSink *sink = this->stereoSinks[i];      
    if (sink->enabled)
      sink->update(sensnet, replay);
    if (sink->dirty)
      dirty = true;
    if (sink->blob.state.timestamp > this->state.timestamp)
      this->state = sink->blob.state;
  }

  // Update currently subscribed ladar sinks with new data.  If the
  // sink has new state data, grab a copy of that as well.
  for (i = 0; i < this->numLadarSinks; i++)
  {
    LadarSink *sink = this->ladarSinks[i];      
    if (sink->enabled)
      sink->update(sensnet, replay);
    if (sink->dirty)
      dirty = true;
    if (sink->blob.state.timestamp > this->state.timestamp)
      this->state = sink->blob.state;
  }
  
  // Update currently subscribed radar sinks with new data.  If the
  // sink has new state data, grab a copy of that as well.
  for (i = 0; i < this->numRadarSinks; i++)
  {
    RadarSink *sink = this->radarSinks[i];      
    if (sink->enabled)
      sink->update(sensnet, replay);
    if (sink->dirty)
      dirty = true;
    if (sink->blob.state.timestamp > this->state.timestamp)
      this->state = sink->blob.state;
  }

  // Update currently subscribed PTU sink with new data.  If the
  // sink has new state data, grab a copy of that as well.
  for(i = 0; i < this->numPTUSinks; i++)
  {
    PTUSink *sink = this->ptuSinks[i];      
    if (sink->enabled)
      sink->update(sensnet, replay);
    if (sink->dirty)
      dirty = true;
    if (sink->blob.state.timestamp > this->state.timestamp)
      this->state = sink->blob.state;
  }

  // Consider roadline sink
  if (true)
  {
    RoadLineSink *sink = this->roadLineSink;
    if (sink->enabled)
      sink->update(sensnet, replay);
    if (sink->dirty)
      dirty = true;
    if (sink->blob.state.timestamp > this->state.timestamp)
      this->state = sink->blob.state;
  }

  /* REMOVE
  // Consider obsmap sink
  if (true)
  {
  ObsMapSink *sink = this->obsMapSink;
  if (sink->enabled)
  sink->update(sensnet, replay);
  if (sink->dirty)
  dirty = true;
  if (sink->blob.state.timestamp > this->state.timestamp)
  this->state = sink->blob.state;
  }
  */
  
  // If the state has changed, update the pose history
  if (dirty)
  {
    pose3_t pose;

    // Compute local pose
    pose.pos = vec3_set(this->state.localX, this->state.localY, this->state.localZ);
    pose.rot = quat_from_rpy(this->state.localRoll, this->state.localPitch, this->state.localYaw);

    // If we have moved since the last pose was added to the list...
    if (this->numPoses == 0 ||
        vec3_mag(vec3_sub(pose.pos, this->poses[this->numPoses - 1].pos)) > 0.20)
    {
      // Expand the list to make room, if necessary
      if (this->numPoses >= this->maxPoses)
      {
        this->maxPoses += 1000;
        this->poses = (pose3_t*) realloc(this->poses, this->maxPoses * sizeof(this->poses[0]));
        assert(this->poses);
      }

      // Add pose to the list
      this->poses[this->numPoses++] = pose;
    }
  }
  
  if (dirty)
  {
    this->dirty = true;
    this->glwin->redraw();
  }
  
  return 0;
}


// Handle menu callbacks
void WorldWin::onSensor(Fl_Widget *w, int option)
{
  WorldWin *self = (WorldWin*) w->user_data();

  self->toggle(option);
  
  return;
}


// Handle menu callbacks
void WorldWin::onAction(Fl_Widget *w, int option)
{
  //int i;
  WorldWin *self = (WorldWin*) w->user_data();

  if (option == ACTION_CAPTURE)
  {
    /* REMOVE?
    // Let stereo sinks export data
    for (i = 0; i < self->numStereoSinks; i++)
    {
      StereoSink *sink = self->stereoSinks[i];
      sink->writeImage();
    }
    */

    self->capEnable = !self->capEnable;
  }
  
  return;
}


// Handle menu callbacks
void WorldWin::onView(Fl_Widget *w, int option)
{
  int i;
  WorldWin *self = (WorldWin*) w->user_data();
  pose3_t pose;
  vec3_t eye, at;

  // Default camera viewpoint
  eye = vec3_set(-5, 0, -5);
  at = vec3_set(+10, 0, 0);
  
  if (option == VIEW_VEHICLE)
  {
    self->glwin->set_lookat(eye.x, eye.y, eye.z, at.x, at.y, at.z, 0, 0, -1);
    self->viewVehicle = true;
  }
  if (option == VIEW_LOCAL)
  {
    pose.pos = vec3_set(self->state.localX, self->state.localY, self->state.localZ);
    pose.rot = quat_from_rpy(self->state.localRoll, self->state.localPitch, self->state.localYaw);
    eye = vec3_transform(pose, eye);
    at = vec3_transform(pose, at);
    self->glwin->set_lookat(eye.x, eye.y, eye.z, at.x, at.y, at.z, 0, 0, -1);    
    self->viewVehicle = false;
  }

  // Update options
  if (option == VIEW_STATUS)
    self->viewStatus = !self->viewStatus;
  if (option == VIEW_IMAGES)
    self->viewImages = !self->viewImages;
  if (option == VIEW_CLOUD)
    self->viewCloud = !self->viewCloud;
  if (option == VIEW_LADAR_ID_COLOR)
  {
    self->ladarUseIdColor = true;
    self->ladarUseElevColor = false;
  }
  if (option == VIEW_LADAR_ELEV_COLOR)
  {
    self->ladarUseIdColor = false;
    self->ladarUseElevColor = true;
  }
  if (option == VIEW_FOOTPRINT)
    self->viewFootprint = !self->viewFootprint;
  if (option == VIEW_GRID)
    self->viewGrid = !self->viewGrid;
  if (option == VIEW_MAP)
    self->viewMap = !self->viewMap;
  if (option == VIEW_RNDF)
    self->viewRndf = !self->viewRndf;
  if (option == VIEW_LINEOFSITE)
    self->viewLineOfSite = !self->viewLineOfSite;

  // Update stereo sink view options
  for (i = 0; i < self->numStereoSinks; i++)
  {
    StereoSink *sink = self->stereoSinks[i];
    if (option == VIEW_STEREO_TRUE)
      sink->color = StereoSink::TRUE_COLOR;
    else if (option == VIEW_STEREO_ELEVATION)
      sink->color = StereoSink::ELEVATION_COLOR;
  }
  
  // Redraw everything
  for (i = 0; i < self->numStereoSinks; i++)
  {
    StereoSink *sink = self->stereoSinks[i];
    sink->dirty = true;
  }  
  for (i = 0; i < self->numLadarSinks; i++)
  {
    LadarSink *sink = self->ladarSinks[i];
    sink->dirty = true;
  }
  for (i = 0; i < self->numRadarSinks; i++)
  {
    RadarSink *sink = self->radarSinks[i];
    sink->dirty = true;
  }
  for (i = 0; i < self->numPTUSinks; i++)
  {
    PTUSink *sink = self->ptuSinks[i];
    sink->dirty = true;
  }
  self->roadLineSink->dirty = true;  self->roadLineSink->dirty = true;
  // REMOVE self->obsMapSink->dirty = true;
  
  self->glwin->redraw();
  
  return;
}


// Prepare for drawing
void WorldWin::onPrepare(Fl_Glv_Window *win, WorldWin *self)
{
  double dx, dy, dz, dr;

  // Compute distance from eye to at point
  dx = win->eye_x - win->at_x;
  dy = win->eye_y - win->at_y;
  dz = win->eye_z - win->at_z;  
  dr = sqrt(dx * dx + dy * dy + dz * dz);

  // Use a heuristic to set the clip planes, based on the distance
  // from the "at" point.
  if (dr < 10)
    win->set_clip(0.1, 50);
  else if (dr < 100)
    win->set_clip(1, 500);
  else
    win->set_clip(10, dr * 10);

  return;
}


// Draw window
void WorldWin::onDraw(Fl_Glv_Window *win, WorldWin *self)
{
  int i, j;
  pose3_t pose_local, pose_stable;
  
  // Predraw static stuff
  if (self->aliceList == 0)
    self->predrawAlice();
  if (self->gridList == 0)
    self->predrawGrid();

  // Predraw stuff that is dirty
  if (self->dirty)
    self->predrawPath();
  self->dirty = false;
  
  // Predraw stereo stuff
  for (i = 0; i < self->numStereoSinks; i++)
  {
    StereoSink *sink = self->stereoSinks[i];
    if (sink->dirty && sink->enabled)
    {
      sink->predrawImages();
      if (self->viewCloud)      
        sink->predrawPointCloud();
      if (self->viewFootprint)
        sink->predrawFootprint();
      sink->dirty = false;
    }
  }

  // Predraw ladar stuff
  for (i = 0; i < self->numLadarSinks; i++)
  {
    LadarSink *sink = self->ladarSinks[i];
    if (sink->dirty && sink->enabled)
    {
      if (self->viewStatus)
        sink->predrawStatus();
      sink->predrawPointCloud(self->options->ladar_scans_arg,
                              self->ladarUseIdColor, self->ladarUseElevColor);
      if (self->viewFootprint)
        sink->predrawFootprint();
      sink->dirty = false;
    }
  }

  // Predraw radar stuff
  for (i = 0; i < self->numRadarSinks; i++)
  {
    RadarSink *sink = self->radarSinks[i];
    if (sink->dirty && sink->enabled)
    {
      if (self->viewStatus)
        sink->predrawStatus();
      sink->predrawPointCloud();
      sink->dirty = false;
    }
  }

 // Predraw PTU stuff
  for (i = 0; i < self->numPTUSinks; i++)
  {
    PTUSink *sink = self->ptuSinks[i];
    if (sink->dirty && sink->enabled)
    {
      if (self->viewStatus)
        sink->predrawStatus();
      if (self->viewLineOfSite)
	sink->predrawLineOfSite();
      sink->dirty = false;
    }
  }

  // Predraw perceptor stuff
  if (self->roadLineSink->dirty)
  {
    self->roadLineSink->predrawLines();
    self->roadLineSink->dirty = false;
  }

  /* REMOVE
  if (self->obsMapSink->dirty)
  {
    self->obsMapSink->predrawCells();
    self->obsMapSink->dirty = false;
  }
  */

  // Vehicle pose in local frame
  pose_local.pos = vec3_set(self->state.localX,
                            self->state.localY,
                            self->state.localZ);
  pose_local.rot = quat_from_rpy(self->state.localRoll,
                                 self->state.localPitch,
                                 self->state.localYaw);

  // Vehicle pose in stabilized local frame
  pose_stable.pos = vec3_set(self->state.localX,
                             self->state.localY,
                             self->state.localZ);
  pose_stable.rot = quat_from_rpy(0, 0, self->state.localYaw);

  // Select display frame
  if (self->viewVehicle)
    self->pushFrame(pose3_inv(pose_stable));

  // Draw Alice (vehicle frame)
  if (true)
  {
    self->pushFrameLocal(self->state);
    glCallList(self->aliceList);
    self->popFrame();
  }

  // Draw Alice path (local frame)
  if (true)
  {
    glCallList(self->pathList);
  }

  if (false)
  {
    int i;
    float d;
    char text[64];
    GLUquadric *quad;

    self->pushFrameLocal(self->state);

    // Show range rings (in vehicle frame)
    for (i = 1; i <= 8; i++)
    {
      d = 10 * i;
      glLineWidth(1);
      glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glColor3f(1, 1, 1);  
      quad = gluNewQuadric();
      gluDisk(quad, d, d, 128, 2);
      gluDeleteQuadric(quad);
      gl_font(FL_HELVETICA, 12);
      snprintf(text, sizeof(text), "%.0fm", d);
      gl_draw(text, d + 0.5f, 0.5f /*d + 0.5f*/);
    }
    self->popFrame();
  }

  // Draw opaque stereo stuff (in sensor frame)
  for (i = 0; i < self->numStereoSinks; i++)
  {
    StereoSink *sink = self->stereoSinks[i];
    if (!sink->enabled)
      continue;

    self->pushFrameLocal(sink->blob.state);

    // Draw left camera
    self->pushFrame(sink->blob.leftCamera.sens2veh);
    glDrawAxes(0.50);
    if (self->viewCloud)
      glCallList(sink->cloudList);
    self->popFrame();

    // Draw right camera
    self->pushFrame(sink->blob.rightCamera.sens2veh);
    glDrawAxes(0.50);
    self->popFrame();
    
    self->popFrame();
  }
  
  // Draw opaque ladar stuff (in sensor frame)
  for (i = 0; i < self->numLadarSinks; i++)
  {
    LadarSink *sink = self->ladarSinks[i];
    if (!sink->enabled)
      continue;

    self->pushFrameLocal(sink->blob.state);
    self->pushFrame(sink->blob.sens2veh);
    glDrawAxes(0.50);
    if (self->viewStatus)
      glCallList(sink->statusList);
    self->popFrame();
    self->popFrame();

    for (j = 0; j < sink->maxClouds; j++)
      glCallList(sink->cloudList + j);
  }

  // Draw opaque radar stuff (in sensor frame)
  for (i = 0; i < self->numRadarSinks; i++)
  {
    RadarSink *sink = self->radarSinks[i];
    if (!sink->enabled)
      continue;

    self->pushFrameLocal(sink->blob.state);
    self->pushFrame(sink->blob.sens2veh);
    glDrawAxes(0.50);
    if (self->viewStatus)
      glCallList(sink->statusList);
    self->popFrame();
    self->popFrame();

    glCallList(sink->cloudList);
  }

  // Draw PTU state (in sensor frame)
  for (i = 0; i < self->numPTUSinks; i++)
    {
      PTUSink *sink = self->ptuSinks[i];
      if(!sink->enabled)
	continue;
	
      self->pushFrameLocal(sink->blob.state);
      self->pushFrame(sink->blob.ptu2veh);
      glDrawAxes(0.50);
      self->pushFrame(sink->blob.tool2ptu);
      glDrawAxes(0.50);
      if(self->viewStatus)     
	glCallList(sink->statusList);
      if(self->viewLineOfSite)
	glCallList(sink->lineofsiteList);
      self->popFrame();
      self->popFrame();
      self->popFrame();
    }

  if (self->roadLineSink->enabled)
  {
    // Draw lines (local frame)
    glCallList(self->roadLineSink->lineList);
  }

  /* REMOVE
  if (self->obsMapSink->enabled)
  {
    // Draw obstacle map (local frame)
    glCallList(self->obsMapSink->cellList);
  }
  */

  if (self->viewMap)
  {
    // Draw prior map (local frame)
    self->mapPainter.draw(self->state);
  }

  if (self->viewRndf)
  {
    // Draw RNDF (local frame)
    self->rndfPainter.draw(self->state);
  }

  // Draw translucent grid
  if (self->viewGrid)
  {
    glPushMatrix();
    glTranslatef(20 * (int) (self->state.localX/20),
                 20 * (int) (self->state.localY/20),
                 (self->state.localZ + VEHICLE_TIRE_RADIUS));
    glCallList(self->gridList);
    glPopMatrix();
  }

  // Draw translucent stereo stuff
  for (i = 0; i < self->numStereoSinks; i++)
  {
    StereoSink *sink = self->stereoSinks[i];
    if (!sink->enabled)
      continue;    

    self->pushViewport();
    if (self->viewImages)
      glCallList(sink->imageList);
    self->popViewport();

    self->pushFrameLocal(sink->blob.state);
    if (self->viewFootprint)
      glCallList(sink->footList);  
    self->popFrame();
  }

  // Draw translucent ladar stuff (in sensor frame)
  for (i = 0; i < self->numLadarSinks; i++)
  {
    LadarSink *sink = self->ladarSinks[i];
    if (!sink->enabled)
      continue;
    
    self->pushFrameLocal(sink->blob.state);
    self->pushFrame(sink->blob.sens2veh);

    if (self->viewFootprint)
      glCallList(sink->footList);  
 
    self->popFrame();
    self->popFrame();
  }

  if (self->viewVehicle)
    self->popFrame(); 
  
  // Switch to viewport frame and draw state data
  if (true)
  {
    self->pushViewport();
    self->drawStateText();
    self->popViewport();
  }

  // Screen cap to file
  if (self->capEnable)
  {
#ifdef JPLV_IMAGE_H
    jplv_image_t *image;
    char filename[1024];
    int vp[4];
    
    glGetIntegerv(GL_VIEWPORT, vp);
    image = jplv_image_alloc(vp[2], vp[3], 3, 8, 0, NULL);
    glReadPixels(vp[0], vp[1], vp[2], vp[3], GL_RGB, GL_UNSIGNED_BYTE, image->data);
    snprintf(filename, sizeof(filename), "world-%04d.pnm", self->capCount);
    MSG("writing %s", filename);
    jplv_image_write_pnm(image, filename, NULL);
    jplv_image_free(image);
#endif
    
    self->capCount++;
  }
  
  return;
}


// Switch to given frame (homogeneous transform)
void WorldWin::pushFrame(float m[4][4])
{
  int i, j;
  float t[16];

  // Transpose to column-major order for GL
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);

  return;
}


// Switch to the given frame.
void WorldWin::pushFrame(pose3_t pose)
{
  float m[4][4];
  pose3_to_mat44f(pose, m);  
  return pushFrame(m);
}


// Switch to the given local frame.
void WorldWin::pushFrameLocal(VehicleState state)
{
  pose3_t pose;
  pose.pos = vec3_set(state.localX, state.localY, state.localZ);
  pose.rot = quat_from_rpy(state.localRoll, state.localPitch, state.localYaw);
  return pushFrame(pose);
}


// Revert to previous frame
void WorldWin::popFrame()
{
  glPopMatrix();  
  return;
}


// Switch to viewport projection.
void WorldWin::pushViewport()
{
  int vp[4];
      
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Get the viewport dimensions
  glGetIntegerv(GL_VIEWPORT, vp);
  
  // Shift and rescale such that (-cols/2, 0) is the top-left corner
  // and (+cols/2, rows) is the bottom-right corner.
  glTranslatef(0, 1.0, 0);
  glScalef(2.0/vp[2], -2.0/vp[3], 1.0);

  /*
  // Test code
  glColor3f(1, 1, 1);
  glBegin(GL_QUADS);
  glVertex3f(0, 0, 0);
  glVertex3f(100, 0, 0);
  glVertex3f(100, 50, 0);
  glVertex3f(0, 50, 0);
  glEnd();
  */
  
  return;
}


// Revert to non-viewport projection
void WorldWin::popViewport()
{
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  return;
}


// Draw state data as text
void WorldWin::drawStateText()
{
  char text[256];
  char temp[] =
    "Time %.3f\n"
    "Pos %+07.1f %+07.1f %+07.1f\n"
    "Rot  %+06.1f  %+06.1f  %+06.1f\n";

  int vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);

  // Put text in the bottom left corner
  glTranslatef(-vp[2]/2, vp[3] - 64, 0);

  // Hmmm, somewhat hacky way to set the text size.
  // Account for viewport dimensions to preserve 1:1 aspect ratio.
  glScalef(12, -12, 1);
    
  // Construct string
  snprintf(text, sizeof(text), temp,
           fmod((double) this->state.timestamp * 1e-6, 1e4),
           this->state.localX, this->state.localY, this->state.localZ,
           this->state.localRoll * 180/M_PI,
           this->state.localPitch * 180/M_PI,
           this->state.localYaw * 180/M_PI);

  glColor3f(0.7, 0, 0);
  glDrawText(1, text);  

  return;
}


// Predraw the robot
void WorldWin::predrawAlice()
{
  // Generate display list 
  if (this->aliceList == 0)
    this->aliceList = glGenLists(1);
  glNewList(this->aliceList, GL_COMPILE);

  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  glEndList();
    
  return;
}


// Predraw Alice's path
void WorldWin::predrawPath()
{
  int i;
  pose3_t pose;
  float oz;
  
  // Generate display list 
  if (this->pathList == 0)
    this->pathList = glGenLists(1);
  glNewList(this->pathList, GL_COMPILE);

  // MAGIC
  oz = this->state.localZ + VEHICLE_TIRE_RADIUS;

  glColor3f(1, 0, 1);
  glBegin(GL_LINE_STRIP);
  for (i = 0; i < this->numPoses; i++)
  {
    pose = this->poses[i];
    glVertex3f(pose.pos.x, pose.pos.y, oz);
  }
  glEnd();
  
  glEndList();
    
  return;
}


// Predraw a ground grid
void WorldWin::predrawGrid()
{
  int i, j;

  // Generate display list 
  if (this->gridList == 0)
    this->gridList = glGenLists(1);
  glNewList(this->gridList, GL_COMPILE);

  glPushMatrix();

  glScalef(10, 10, 10);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  glBegin(GL_QUADS);
  for (j = -10; j < +10; j++)
  {
    for (i = -10; i < +10; i++)
    {
      //glColor4f((i + j + 100) % 2, (i + j + 100) % 2, (i + j + 100) % 2, 0.2);
      glColor3f(0.5, 0.5, 0.5);
      glVertex3f(i + 0, j + 0, 0);
      glVertex3f(i + 1, j + 0, 0);
      glVertex3f(i + 1, j + 1, 0);
      glVertex3f(i + 0, j + 1, 0);
    }
  }
  glEnd();

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  //glPolygonMode(GL_BACK, GL_FILL);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);
  glBegin(GL_QUADS);
  for (j = -10; j < +10; j++)
  {
    for (i = -10; i < +10; i++)
    {
      glColor4f((i + j + 100) % 2, (i + j + 100) % 2, (i + j + 100) % 2, 0.2);
      //glColor3f(0.5, 0.5, 0.5);
      glVertex3f(i + 0, j + 0, 0);
      glVertex3f(i + 1, j + 0, 0);
      glVertex3f(i + 1, j + 1, 0);
      glVertex3f(i + 0, j + 1, 0);
    }
  }
  glEnd();
  glDisable(GL_BLEND);
  
  glPopMatrix();

  glEndList();

  return;
}


