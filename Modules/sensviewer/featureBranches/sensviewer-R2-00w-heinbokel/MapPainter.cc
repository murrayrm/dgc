 
/* 
 * Desc: Render map data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <float.h>
#include <glob.h>
#include <GL/glu.h>
#include <zlib.h>

#include <alice/AliceConstants.h>
#include <dgcutils/ggis.h>
#include <frames/pose3.h>

#include "MapPainter.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
MapPainter::MapPainter()
{
  this->numTiles = 0;
  this->tile = NULL;
  this->tileTex = 0;
  
  return;
}


// Load map files from a directory
int MapPainter::load(const char *path)
{
  int i;
  glob_t gb;
  char pattern[1024];
  int lat, lon, scale;
  Tile *tile;

  // Construct pattern for map files
  snprintf(pattern, sizeof(pattern), "%s/tile_*_*_*.ppm", path);

  // Glob the files
  if (glob(pattern, 0, NULL, &gb) != 0)
    return ERROR("unable to glob %s", pattern);

  // Construct pattern for scanf
  snprintf(pattern, sizeof(pattern), "%s/tile_%%d_%%d_%%d.ppm", path);

  // Copy available files into tile storage
  for (i = 0; i < (int) gb.gl_pathc; i++)
  {
    assert((size_t) this->numTiles < sizeof(this->tiles)/sizeof(this->tiles[0]));
    tile = this->tiles + this->numTiles++;

    if (sscanf(gb.gl_pathv[i], pattern, &lat, &lon, &scale) < 3)
      MSG("syntax error in tile %s", gb.gl_pathv[i]);

    // Lat/lon are in micro-degrees; scale in is mm/pixel.
    tile->lat = (double) lat * 1e-6;
    tile->lon = (double) lon * 1e-6;
    tile->scale = (double) scale * 1e-3;
    tile->filename = strdup(gb.gl_pathv[i]);
    
    //MSG("found map tile %.6f %.6f %.3f", tile->lat, tile->lon, tile->scale);
  }
  MSG("loaded %d map tiles", (int) gb.gl_pathc);
  
  globfree(&gb);
  
  return 0;
}


// Draw the map at a particular location
void MapPainter::draw(const VehicleState *state, float localX, float localY)
{
  Tile *tile;
  GisCoordUTM utm;
  GisCoordLatLon geo;
  double oz;
  vec3_t p;
  pose3_t localPose, utmPose, transGL, transLG;
  
  // Compute transform from UTM to local frame
  localPose.pos = vec3_set(state->localX, state->localY, state->localZ);
  localPose.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw); 
  utmPose.pos = vec3_set(state->utmNorthing, state->utmEasting, state->utmAltitude);
  utmPose.rot = quat_from_rpy(state->utmRoll, state->utmPitch, state->utmYaw);

  // TESTING
  // Images are in NAD_1983; offset to WGS_84 in Victorville area
  utmPose.pos.x -= +0.7;
  utmPose.pos.y -= -1.3;
  
  transLG = pose3_mul(localPose, pose3_inv(utmPose));
  transGL = pose3_mul(utmPose, pose3_inv(localPose));

  // Check for new tile every few meters only
  if (fabs(localX - this->localX) > 2.0 || fabs(localY - this->localY) > 2.0 || this->tile == NULL)
  {
    this->localX = localX;
    this->localY = localY;

    // Compute the desired location in the UTM frame
    p = vec3_transform(transGL, vec3_set(localX, localY, 0));
    
    // Compute lat/lon from global UTM
    utm.n = p.x;
    utm.e = p.y;
    utm.letter = state->utmLetter;
    utm.zone = state->utmZone;  
    gis_coord_utm_to_latlon(&utm, &geo, GIS_GEODETIC_MODEL_DEFAULT);
  
    // Find the nearest tile
    tile = this->lookupTile(geo.latitude, geo.longitude);
    if (tile == NULL)
    {
      MSG("no tile at geo %.6f %.6f", geo.latitude, geo.longitude);
      MSG("no tile at utm %d %.3f %.3f", utm.zone, utm.n, utm.e);
      return;
    }

    // If the nearest tile has changed, load the new one
    if (tile != this->tile && this->loadTile(tile) != 0)
    {
      MSG("no tile at geo %.6f %.6f", geo.latitude, geo.longitude);
      MSG("no tile at utm %d %.3f %.3f", utm.zone, utm.n, utm.e);
      return;
    }
    
    this->tile = tile;
  }
  if (!this->tile)
    return;  

  // MAGIC
  oz = state->localZ + VEHICLE_TIRE_RADIUS + 0.1;

  glDisable(GL_BLEND);
  glEnable(GL_TEXTURE_2D);
  glColor3f(1, 1, 1);
  glPolygonMode(GL_FRONT, GL_LINE);
  glPolygonMode(GL_BACK, GL_FILL);
  glBindTexture(GL_TEXTURE_2D, this->tileTex);
  glBegin(GL_QUADS);

  // North-west corner
  geo.latitude = this->tile->lat;
  geo.longitude = this->tile->lon;
  gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);
  utm.n += this->tile->scale * this->tile->rows/2;
  utm.e -= this->tile->scale * this->tile->cols/2;
  p = vec3_transform(transLG, vec3_set(utm.n, utm.e, 0));  
  glTexCoord2d(0, 0);
  glVertex3d(p.x, p.y, oz);

  // North-east corner
  geo.latitude = this->tile->lat; 
  geo.longitude = this->tile->lon;
  gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);
  utm.n += this->tile->scale * this->tile->rows/2;
  utm.e += this->tile->scale * this->tile->cols/2;
  p = vec3_transform(transLG, vec3_set(utm.n, utm.e, 0));  
  glTexCoord2d(1, 0);
  glVertex3d(p.x, p.y, oz);

  // South-east corner
  geo.latitude = this->tile->lat; 
  geo.longitude = this->tile->lon;
  gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);
  utm.n -= this->tile->scale * this->tile->rows/2;
  utm.e += this->tile->scale * this->tile->cols/2;
  p = vec3_transform(transLG, vec3_set(utm.n, utm.e, 0));  
  glTexCoord2d(1, 1);
  glVertex3d(p.x, p.y, oz);

  // South-west corner
  geo.latitude = this->tile->lat; 
  geo.longitude = this->tile->lon;
  gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);
  utm.n -= this->tile->scale * this->tile->rows/2;
  utm.e -= this->tile->scale * this->tile->cols/2;
  p = vec3_transform(transLG, vec3_set(utm.n, utm.e, 0));  
  glTexCoord2d(0, 1);
  glVertex3d(p.x, p.y, oz);

  glEnd();
  glDisable(GL_TEXTURE_2D);
  
  return;
}


// Find the map closest to the target lat/lon
MapPainter::Tile *MapPainter::lookupTile(double lat, double lon)
{
  int i;
  double dx, dy, dr;
  double mr;
  Tile *tile, *minTile;

  minTile = NULL;
  mr = DBL_MAX;
  
  for (i = 0; i < this->numTiles; i++)
  {
    tile = this->tiles + i;

    dx = lat - tile->lat;
    dy = lon - tile->lon;
    dr = dx * dx + dy * dy;
    if (dr < mr)
    {
      mr = dr;
      minTile = tile;
    }
  }
  
  return minTile;
}


// Load the data for a tile
int MapPainter::loadTile(Tile *tile)
{
  int channels, header, size;
  char *data;
  
  MSG("loading map tile %.6f %.6f", tile->lat, tile->lon);

  // Create GL texture
  if (this->tileTex == 0)
    glGenTextures(1, &this->tileTex);

  // Read image header
  if (this->loadPnmHeader(tile->filename, &tile->cols, &tile->rows, &channels, &header) != 0)
    return -1;

  // Allocate space to load image
  size = tile->cols * tile->rows * channels;
  data = new char[size];

  // Load data
  this->loadPnmData(tile->filename, tile->cols, tile->rows, channels, header, size, data);  

  // Copy image into texture
  glBindTexture(GL_TEXTURE_2D, this->tileTex);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  if (channels == 1)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4,
                      tile->cols, tile->rows, GL_LUMINANCE, GL_UNSIGNED_BYTE, data);
  else if (channels == 3)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4,
                      tile->cols, tile->rows, GL_RGB, GL_UNSIGNED_BYTE, data);

  delete [] data;
  
  return 0;
}


// Utility function to read PNM image headers
int MapPainter::loadPnmHeader(const char *filename,
                              int *cols, int *rows, int *channels, int *header)
{
  gzFile file;
  char line[4096];
  int i, num_tokens, c;
  int max;
  
  file = gzopen(filename, "r");
  if (!file)
    return ERROR("error opening %s : %s", filename, strerror(errno));

  // Get the first line; must contain the magic number
  if (gzgets(file, line, sizeof(line)) == NULL)
  {
    gzclose(file);
    return ERROR("%s: unable to read image header", filename);
  }

  // RGB or mono
  if (strncmp(line, "P5", 2) == 0)
    *channels = 1;
  else if (strncmp(line, "P6", 2) == 0)
    *channels = 3;
  else
  {
    gzclose(file);
    return ERROR("%s: unknown image type [%.2s]", filename, line);
  }
  
  // Get some number of comment lines and read out the caption
  while (true)
  {
    c = gzgetc(file);
    if (c != '#')
    {
      gzseek(file, -1, SEEK_CUR);
      //ungetc(c, file);
      break;
    }    
    if (gzgets(file, line, sizeof(line)) == NULL)
    {
      gzclose(file);
      return ERROR("unexpected end of file");
    }
  }
  
  // Read two numbers (image dimensions)
  num_tokens = 0;
  for (i = 0; (size_t) i < sizeof(line) - 1 && num_tokens < 2; i++)
  {
    line[i] = gzgetc(file);
    line[i + 1] = 0;
    if (i > 0 && !isdigit(line[i]))
    { 
      if (isdigit(line[i - 1]))
        num_tokens++;
    }
  }
  assert(num_tokens == 2);

  // Read dimensions
  assert(sscanf(line, "%d %d", cols, rows) == 2);

  // Read more comments
  // Get some number of comment lines and read out the caption
  while (true)
  {
    c = gzgetc(file);
    if (c != '#')
    {
      gzseek(file, -1, SEEK_CUR);
      //ungetc(c, file);
      break;
    }
    
    if (gzgets(file, line, sizeof(line)) == NULL)
    {
      gzclose(file);
      return ERROR("unexpected end of file");
    }
  }
  // Read two numbers (image dimensions)
  num_tokens = 0;
  for (i = 0; (size_t) i < sizeof(line) && num_tokens < 1; i++)
  {
    line[i] = gzgetc(file);
    if (i > 0 && !isdigit(line[i]))
    { 
      if (isdigit(line[i - 1]))
        num_tokens++;
    }
  }
  assert(num_tokens == 1);  

  // Read max pixel value
  assert(sscanf(line, "%d", &max) == 1);
  if (max > 256)
    return ERROR("image has more than 8 bpp");

  // Compute size of header
  if (header)
    *header = (size_t) gztell(file);
  
  gzclose(file);

  return 0;
}


// Utility function to read PNM image data
int MapPainter::loadPnmData(const char *filename,
                            int cols, int rows, int channels, int header, int size, char *data)
{
  gzFile file;

  // Open the file
  file = gzopen(filename, "r");
  if (!file)
    return ERROR("error opening %s : %s", filename, strerror(errno));

  // Skip the header
  gzseek(file, header, SEEK_SET);

  // Optimize for certain com
  if (channels == 1)
  {
    gzread(file, data, size);
  }
  else if (channels == 3)
  {
    gzread(file, data, size);
  }
  
  gzclose(file);

  return 0;
}
