 
/* 
 * Desc: Sink for stereo blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include "sensnet/sensnet.h"
#include "interfaces/StereoImageBlob.h"


// Display data for stereo blobs
class StereoSink
{
  public:

  // Constructor
  StereoSink();
  
  // Generate range point cloud 
  void predrawPointCloud();

  // Predraw sensor footprint 
  void predrawFootprint();

  public:

  // Update with current sensnet data
  int updateSensnet(sensnet_t *sensnet);

  public:

  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId;

  // Blob data buffer
  StereoImageBlob blob;
  
  // Are we enabled?
  bool enable;

  // Set flag if the data needs predrawing
  bool dirty;

  // GL Drawing lists
  int cloudList, footList;  
};
