
/* 
 * Desc: Sink for PTU
 * Date: 1 June 2007
 * Author: Jeremy Ma (adapted from Andrew Howard's other sinks)
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glu.h>

#include <frames/pose3.h>
#include <alice/AliceConstants.h>
#include <interfaces/SensnetTypes.h>

#include "glUtils.hh"
#include "PTUSink.hh"

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
PTUSink::PTUSink(int menuId, int sensorId)
{
  memset(this, 0, sizeof(*this));

  this->menuId = menuId;
  this->sensorId = sensorId;
  this->blobType = SENSNET_PTU_STATE_BLOB;
  this->blobId = -1;
  
  return;
}


// Enable/disable sink
int PTUSink::enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable)
{
  if (sensnet)
  {
    // If in live mode...
    if (enable && !this->enabled)
    {
      sensnet_join(sensnet, this->sensorId, this->blobType, sizeof(this->blob));
      this->enabled = true;
    }
    else if (!enable && this->enabled)
    {
      sensnet_leave(sensnet, this->sensorId, this->blobType);
      this->enabled = false;
    }
  }  
  else if (replay)
  {
    // If in replay mode
    this->enabled = enable;
  }
  return 0;
}

// Update with current sensnet data
int PTUSink::update(sensnet_t *sensnet, sensnet_replay_t *replay)
{
  int blobId;
      
  if (sensnet)
  {
    // Peek at the latest data, but dont read it out
    if (sensnet_peek(sensnet, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;

    // Is the data new?
    if (blobId < 0 || blobId == this->blobId)
      return 0;

    // Read the data
    if (sensnet_read(sensnet, this->sensorId, this->blobType,
                     &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;
  }
  else if (replay)
  {
    // Peek at the latest data, but dont read it out
    if (sensnet_replay_peek(replay, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;
    
    // Is the data new?
    if (blobId < 0 || blobId == this->blobId)
      return 0;

    // Read the data    
    if (sensnet_replay_read(replay, this->sensorId, this->blobType,
                            &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;    
  }
   
  return 0;
}


// Generate status text
void PTUSink::predrawStatus()
{
  char text[256];
  char temp[] = "%s\n%.3f %d\n";
  
  if (this->statusList == 0)
    this->statusList = glGenLists(1);
  glNewList(this->statusList, GL_COMPILE);  

  snprintf(text, sizeof(text), temp,
           sensnet_id_to_name((sensnet_id_t) this->sensorId),
           fmod((double) this->blob.timestamp * 1e-6, 1e4), this->blobId);

  glPushMatrix();
  glColor3f(0, 1, 0);
  glRotatef(180, 0, -90, -1);
  glTranslatef(0, 0.5, 0);
  glDrawText(0.1, text);  
  glPopMatrix();

  glEndList();
  
  return;
}


// Generate range point cloud (local frame)
void PTUSink::predrawLineOfSite()
{

  PTUStateBlob *blob;

  if(this->blobId < 0)
    return;

  blob = &this->blob;
  
  if (this->lineofsiteList == 0)
    this->lineofsiteList = glGenLists(1);
  
  glNewList(this->lineofsiteList, GL_COMPILE);

  // Desire to draw a line along the forward pointing unit vector (x-axis) 
  // to it's intersection in the ground plane

  // Draw lines 
  glColor3f(1, 0, 0.5);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glLineWidth(1);
  
  glBegin(GL_LINES);  
  glVertex3f(0,0,0);
  glVertex3f(50,0,0);
  glEnd();

  glEndList();
    
  return;
}
