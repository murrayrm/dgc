 
/* 
 * Desc: Sink for road lines
 * Date: 16 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#include "sensnet/sensnet.h"
#include "sensnet/sensnet_replay.h"
#include "interfaces/RoadLineMsg.h"


// Display data for ladar blobs
class RoadLineSink
{
  public:

  // Constructor
  RoadLineSink(int menuId);

  public:
  
  // Enable/disable sink
  int enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable);

  // Update with current sensnet data
  int update(sensnet_t *sensnet, sensnet_replay_t *replay);
  
  public:

  // Generate lines (local frame)
  void predrawLines();
    
  public:
  
  // Our ID in the sensor menu
  int menuId;

  // Sensor Id
  int sensorId;

  // Blob type
  int blobType;
  
  // Current blob id
  int blobId;

  // Blob data buffer
  RoadLineMsg blob;
  
  // Are we enabled?
  bool enabled;

  // Set flag if the data needs predrawing
  bool dirty;
  
  // GL Drawing lists
  int lineList;
};
