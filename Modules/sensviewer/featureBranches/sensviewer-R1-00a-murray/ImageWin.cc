
/* 
 * Desc: Window for displaying images
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include "ImageWin.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Menu commands
enum
{
  VIEW_RGB = 0x1000,
  VIEW_DISP
};


// Set up the menu
static Fl_Menu_Item menuitems[] =
{
  {"&SensorId", 0, 0, 0, FL_SUBMENU},
  {"None", 0, (Fl_Callback*) ImageWin::onSensor,
   (void*) SENSNET_NULL_SENSOR, FL_MENU_RADIO | FL_MENU_VALUE},
  {"LF Short Stereo", 0, (Fl_Callback*) ImageWin::onSensor,
   (void*) SENSNET_LF_SHORT_STEREO, FL_MENU_RADIO},
  {"RF Short Stereo", 0, (Fl_Callback*) ImageWin::onSensor,
   (void*) SENSNET_RF_SHORT_STEREO, FL_MENU_RADIO},
  {0},
  {"&View", 0, 0, 0, FL_SUBMENU},    
  {"RGB", 0, (Fl_Callback*) ImageWin::onView,
   (void*) VIEW_RGB, FL_MENU_RADIO | FL_MENU_VALUE},
  {"Disparity", 0, (Fl_Callback*) ImageWin::onView,
   (void*) VIEW_DISP, FL_MENU_RADIO | FL_MENU_DIVIDER},
  {0},
  {0},
};


// Constructor
ImageWin::ImageWin(sensnet_t *sensnet, int x, int y, int w, int h, int menuh)
    : Fl_Window(x, y, w, h + menuh, NULL)
{
  
  begin();
  
  this->menubar = new Fl_Menu_Bar(0, 0, w, menuh);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  this->glwin = new Fl_Glv_Window(0, menuh, w, h, this, (Fl_Callback*) onDraw);
  this->glwin->user_data(this);
  
  end();

  this->sensnet = sensnet;
  this->sensorId = SENSNET_NULL_SENSOR;
  this->blobId = -1;
  
  // Hook up menu options
  int i;
  const Fl_Menu_Item *item;
  for (i = 0; i < this->menubar->menu()->size(); i++)
  {
    item = this->menubar->menu() + i;
    if (item->user_data() == (void*) VIEW_RGB)
      this->view_rgb = item;
    if (item->user_data() == (void*) VIEW_DISP)
      this->view_disp = item;
  }

  return;
}


// Handle menu callbacks
void ImageWin::onSensor(Fl_Widget *w, int option)
{
  ImageWin *self = (ImageWin*) w->user_data();

  if (!self->sensnet)
    return;
  
  assert(self->sensnet);
  
  // Leave the old group
  if (self->sensorId != SENSNET_NULL_SENSOR)
    sensnet_leave(self->sensnet, self->sensorId, SENSNET_STEREO_BLOB);
    
  self->sensorId = option;

  // Join the new group
  sensnet_join(self->sensnet, self->sensorId, SENSNET_STEREO_BLOB, sizeof(self->blob), 5);
  self->blobId = -1;
  
  self->glwin->redraw();
    
  return;
}


// Handle menu callbacks
void ImageWin::onView(Fl_Widget *w, int option)
{
  ImageWin *self = (ImageWin*) w->user_data();

  self->glwin->redraw();

  return;
}


// Draw window
void ImageWin::onDraw(Fl_Glv_Window *win, ImageWin *self)
{
  int mousex, mousey;
  int win_cols, win_rows;
  int img_cols, img_rows;

  if (self->blobId < 0 || self->sensorId == SENSNET_NULL_SENSOR)
    return;
  
  img_cols = self->blob.cols;
  img_rows = self->blob.rows;

  win_cols = win->w(); 
  win_rows = win->h(); 
  
  glViewport(0, 0, win_cols, win_rows);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(-1, +1 - (float) 1 / img_rows, 0);  
  glScalef((float) 2 / img_cols, (float) -2 / img_rows, 1);

  // Draw border around image
  // Keep for diagnostics
  if (false)
  {
    glLineWidth(3);
    glBegin(GL_LINE_LOOP);
    glColor3f(1, 0, 0);  
    glVertex2f(0, 0);
    glColor3f(0, 1, 0);  
    glVertex2f(img_cols - 1, 0);
    glColor3f(0, 0, 1);  
    glVertex2f(img_cols - 1, img_rows - 1);
    glColor3f(0, 1, 1);  
    glVertex2f(0, img_rows - 1);
    glEnd();
  }

  glRasterPos3f(0, 0, 0.1);
  glPixelZoom((float) win_cols / img_cols, (float) -win_rows / img_rows);

  if (self->view_rgb->value())
  {
    if (self->blob.colorChannels == 1)
      glDrawPixels(img_cols, img_rows, GL_LUMINANCE, GL_UNSIGNED_BYTE, self->blob.colorData);
    else if (self->blob.colorChannels == 3)
      glDrawPixels(img_cols, img_rows, GL_RGB, GL_UNSIGNED_BYTE, self->blob.colorData);
  }
  else if (self->view_disp->value())
  {
    glDrawPixels(img_cols, img_rows, GL_LUMINANCE, GL_UNSIGNED_SHORT, self->blob.dispData);
  }
    
  if (win->mouse_button >= 0)
  {
    // Compute mouse coords in image
    mousex = (int) win->mouse.x % win_cols * img_cols / win_cols;
    mousey = (int) win->mouse.y % win_rows * img_rows / win_rows;
    if (mousex < 0)
      mousex += img_cols;
    if (mousey < 0)
      mousey += img_rows;

    // Draw mouse cross-hairs
    glColor3f(1, 1, 1);
    glLineWidth(1);
    glBegin(GL_LINES);
    glVertex3f(mousex, 0, -0.1);
    glVertex3f(mousex, img_rows, -0.1);
    glVertex3f(0, mousey, -0.1);
    glVertex3f(img_cols, mousey, -0.1);
    glEnd();
  }

  return;
}


// Check for updates
int ImageWin::update()
{
  int blobId, blobLen;
  
  if (!this->sensnet)
    return 0;

  if (this->sensorId == SENSNET_NULL_SENSOR)
    return 0;

  // Look at the latest data
  if (sensnet_peek(this->sensnet, this->sensorId, SENSNET_STEREO_BLOB, &blobId, &blobLen) != 0)
    return -1;

  // Is the data valid?
  if (blobId < 0)
    return 0;

  // Is the data new?
  if (blobId != this->blobId)
  {
    if (sensnet_read(this->sensnet, this->sensorId, SENSNET_STEREO_BLOB,
                     blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;
    this->blobId = blobId;
    this->glwin->redraw();
  }
  
  // TODO
  
  return 0;
}
