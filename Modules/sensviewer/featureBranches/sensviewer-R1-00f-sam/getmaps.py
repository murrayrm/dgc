
# Desc: Download map tiles from Google Maps
# Author: Andrew Howard
# Date: 14 Feb 2007


import os
import urllib



def fetch_tile(lat, lon, zoom, cols, rows):
    """Fetch a tile using the old google maps interface."""

    req = 'latitude_e6=%d&longitude_e6=%d&zm=%d&w=%d&h=%d&cc=us&min_priority=2' % \
          (lat * 1e6, lon * 1e6, zoom, cols, rows)

    print 'loading %s' % req
    ifile = urllib.urlopen("http://maps.google.com/mapdata?%s" % req)
    img = ifile.read()
    ifile.close()

    ofile = open('tile.gif', 'w')
    ofile.write(img)
    ofile.close()

    # Convert to compressed PPM
    name = 'googlemap_%+010d_%+010d_%06d.ppm.gz' % (lat * 1e6, lon * 1e6, zoom)
    print 'saving %s' % name    
    os.system('convert tile.gif %s' % name)
    os.system('rm tile.gif')
    
    return



    

if __name__ == '__main__':

    #ifile = urllib.urlopen("http://maps.google.com/mapdata?latitude_e6=39400000&longitude_e6=4294590424&zm=4800&w=1200&h=600&cc=us&min_priority=2")

    #img = ifile.read()
    #ifile.close()

    #ofile = open('test.gif', 'w')
    #ofile.write(img)
    #ofile.close()

    # CalTech
    lat = 34.139059
    lon = -118.123620

    # HACK
    step = 1024/2 * 10 * 1e-6

    for j in range(-10, +11):
        for i in range(-10, +11):
            fetch_tile(lat + j * step, lon + i * step, 1000, 1024, 1024)



    

    
