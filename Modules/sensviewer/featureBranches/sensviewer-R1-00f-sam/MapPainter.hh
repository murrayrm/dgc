 
/* 
 * Desc: Render map data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef MAP_PAINTER_H
#define MAP_PAINTER_H

#include <interfaces/VehicleState.h>


// Class for rendering prior map data and overhead images
class MapPainter
{
  public:

  // Constructor
  MapPainter();

  public:

  // Load map files from a directory
  int load(const char *path);
  
  // Draw the map at a particular location
  void draw(VehicleState state);

  private:
  
  // Data for each map tile
  struct Tile
  {
    // Lat/lon of the map center (degrees)
    double lat, lon;

    // Map scale (m/pixel)
    double scale;

    // Map dimensions (pixels)
    int cols, rows;

    // Filename of tile data
    char *filename;    
  };

  private:
  
  // Find the tile closest to the target lat/lon
  Tile *lookupTile(double lat, double lon);

  // Load the data for a tile
  int loadTile(Tile *tile);

  // Utility function to read PNM image headers
  int loadPnmHeader(const char *filename,
                    int *cols, int *rows, int *channels, int *header);

  // Utility function to read PNM image data
  int loadPnmData(const char *filename,
                  int cols, int rows, int channels, int header, int size, char *data);

  private:

  // Array of available tiles
  int numTiles;
  Tile tiles[32 * 32];

  // Currently loaded tile
  Tile *tile;

  // Current tile texture
  GLuint tileTex;  
};

#endif
