
/* 
 * Desc: Window for displaying 3D stuff
 * Date: 10 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <jplv/Fl_Glv_Window.H>
#include <dgcutils/pose3.h>

#include "sensnet/sensnet.h"

#include "StereoSink.hh"
#include "LadarSink.hh"
#include "RoadLineSink.hh"


// Window for displaying images
class WorldWin : public Fl_Window
{
  public:

  // Constructor
  WorldWin(int x, int y, int w, int h, int menuh);

  public:

  // Initialize for sensnet
  int sensnetInit();

  // Update blob data
  int sensnetUpdate(sensnet_t *sensnet);

  public:
  
  // Open log files
  int logOpen(int sensorId, const char *filename);

  // Update blob data using logs
  int logUpdate();

  public:

  // Get the sensor menu state (enabled/disabled)
  bool getSensorState(int sensorId);
  
  // Set the sensor menu state (enabled/disabled)
  void setSensorState(int sensorId, bool enable);

  public:
  
  // Handle menu options
  static void onSensor(Fl_Widget *w, int option);
  
  // Handle menu options
  static void onView(Fl_Widget *w, int option);

  // Draw window
  static void onDraw(Fl_Glv_Window *win, WorldWin *self);

  private:

  // Switch to the given frame; i.e., sets up the GL coordinate
  // transforms such that (0, 0, 0) will map to the given pose.
  void pushFrame(pose3_t pose);

  // Switch to given frame (homogeneous transform)
  void pushFrame(float m[4][4]);

  // Switch to given local frame
  void pushFrameLocal(VehicleState state);
    
  // Revert to previous frame
  void popFrame();

  // Draw a set of axes
  void drawAxes(double size);

  // Predraw the robot
  void predrawAlice();

  // Predraw a ground grid
  void predrawGrid();

  public:

  // Local menu
  Fl_Menu_Bar *menubar;

  // GL window
  Fl_Glv_Window *glwin;

  // Menu options
  const Fl_Menu_Item *view_vehicle, *view_local, *view_footprint, *action_capture;

  // List of stereo sinks
  int numStereoSinks;
  StereoSink stereoSinks[8];

  // List of ladar sinks
  int numLadarSinks;
  LadarSink ladarSinks[8];

  // Road line sink
  RoadLineSink roadLineSink;
  
  // Most recent state data
  VehicleState state;

  // Some display lists
  int aliceList, gridList, lineList;

  // Screen capture counter
  int capCount;
};
