
/* 
 * Desc: Sink for ladar blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glu.h>

#include <frames/pose3.h>
#include <alice/AliceConstants.h>
#include <interfaces/SensnetTypes.h>

#include "glUtils.hh"
#include "LadarSink.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
LadarSink::LadarSink(int menuId, int sensorId)
{
  memset(this, 0, sizeof(*this));

  this->menuId = menuId;
  this->sensorId = sensorId;
  this->blobType = SENSNET_LADAR_BLOB;
  this->blobId = -1;

  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
  {
    d = 4 * ((double) i / 0x10000);

    if (d >= 0 && d < 1.0)
    {
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = (int) (d * 0xFF);
      this->rainbow[i][2] = 0xFF;
    }
    else if (d < 2.0)
    {
      d -= 1.0;
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = (int) ((1 - d) * 0xFF);
    }
    else if (d < 3.0)
    {
      d -= 2.0;
      this->rainbow[i][0] = (int) (d * 0xFF);
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = 0x00;
    }
    else if (d < 4.0)
    {
      d -= 3.0;
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = (int) ((1 - d) * 0xFF);
      this->rainbow[i][2] = 0x00;
    }
    else
    {
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = 0x00;
      this->rainbow[i][2] = 0x00;
    }
  }
  
  return;
}


// Enable/disable sink
int LadarSink::enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable)
{
  if (sensnet)
  {
    // If in live mode...
    if (enable && !this->enabled)
    {
      sensnet_join(sensnet, this->sensorId, this->blobType, sizeof(this->blob));
      this->enabled = true;
    }
    else if (!enable && this->enabled)
    {
      sensnet_leave(sensnet, this->sensorId, this->blobType);
      this->enabled = false;
    }
  }  
  else if (replay)
  {
    // If in replay mode
    this->enabled = enable;
  }
  return 0;
}


// Update with current sensnet data
int LadarSink::update(sensnet_t *sensnet, sensnet_replay_t *replay)
{
  int blobId;
      
  if (sensnet)
  {
    // Peek at the latest data, but dont read it out
    if (sensnet_peek(sensnet, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;

    // Is the data new?
    if (blobId < 0 || blobId == this->blobId)
      return 0;

    // Read the data
    if (sensnet_read(sensnet, this->sensorId, this->blobType,
                     &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;
  }
  else if (replay)
  {
    // Peek at the latest data, but dont read it out
    if (sensnet_replay_peek(replay, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;
    
    // Is the data new?
    if (blobId < 0 || blobId == this->blobId)
      return 0;

    // Read the data    
    if (sensnet_replay_read(replay, this->sensorId, this->blobType,
                            &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;    
  }
   
  return 0;
}


// Generate status text
void LadarSink::predrawStatus()
{
  char text[256];
  char temp[] = "%s\n%.3f %d\n";
  
  if (this->statusList == 0)
    this->statusList = glGenLists(1);
  glNewList(this->statusList, GL_COMPILE);  

  snprintf(text, sizeof(text), temp,
           sensnet_id_to_name((sensnet_id_t) this->sensorId),
           fmod((double) this->blob.timestamp * 1e-6, 1e4), this->blobId);

  glPushMatrix();
  glColor3f(0, 1, 0);
  glRotatef(90, 0, 0, -1);
  glTranslatef(0, 0.5, 0);
  glDrawText(0.1, text);  
  glPopMatrix();

  glEndList();
  
  return;
}


// Generate range point cloud (local frame)
void LadarSink::predrawPointCloud(int maxScans, bool useLadarColor, bool useElevColor)
{
  int list;
  int i, k;
  float pa, pr, px, py, pz;
  float vx, vy, vz;
  float lx, ly, lz;
  LadarRangeBlob *blob;

  this->maxClouds = maxScans;
  
  if (this->cloudList == 0)
  {
    this->cloudList = glGenLists(this->maxClouds);
    if (this->cloudList == 0)
    {
      ERROR("unable to create display lists");
      return;
    }
  }

  blob = &this->blob;

  // Pick a list to use based on the scan id.
  list = this->cloudList + (this->numClouds++) % this->maxClouds;
  
  glNewList(list, GL_COMPILE);

  glPushAttrib(GL_POINT_BIT);
  glPointSize(2.0);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glColor3f(0, 0, 1);

  // Draw point cloud
  glBegin(GL_POINTS);  
  for (i = 0; i < blob->numPoints; i++)
  {
    pa = blob->points[i][0];
    pr = blob->points[i][1];
    
    if (pr > 0 && pr < 80.0) // MAGIC 
    {
      // Compute points in sensor frame
      LadarRangeBlobScanToSensor(blob, pa, pr, &px, &py, &pz);

      // Transform to vehicle frame
      LadarRangeBlobSensorToVehicle(blob, px, py, pz, &vx, &vy, &vz);

      // Transform to local frame
      LadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &lx, &ly, &lz);

      // Select color based on the sensor ID
      if (useLadarColor)
      {
        k = 1 + this->sensorId % 7;
        glColor3ub((k & 0x01) * 128, (k & 0x02) * 64, (k & 0x04) * 32);
      }
            
      // Select color based on elevation in vehicle frame
      if (useElevColor)
      {
        k = (int) (-0x10000 * vz / 2.0); // MAGIC
        if (k < 0x0000) k = 0x0000;
        if (k > 0xFFFF) k = 0xFFFF;
        glColor3ub(this->rainbow[k][0], this->rainbow[k][1], this->rainbow[k][2]);
      }
                  
      glVertex3f(lx, ly, lz);
    }
  }
  glEnd();

  // Draw retro-reflectors (we assume)
  glColor3f(1, 0, 0);
  for (i = 0; i < blob->numPoints; i++)
  {
    if (blob->intensities[i] == 0)
      continue;

    if (blob->sensorId == SENSNET_RIEGL && blob->intensities[i] < 240)
      continue;
    
    pa = blob->points[i][0];
    pr = blob->points[i][1];
    
    if (pr > 0 && pr < 80.0) // MAGIC 
    {
      glBegin(GL_LINES);

      // Compute origin in local frame
      LadarRangeBlobSensorToVehicle(blob, 0, 0, 0, &vx, &vy, &vz);
      LadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &lx, &ly, &lz);
      glVertex3f(lx, ly, lz);
      
      // Compute point in local frame
      LadarRangeBlobScanToSensor(blob, pa, pr, &px, &py, &pz);
      LadarRangeBlobSensorToVehicle(blob, px, py, pz, &vx, &vy, &vz);
      LadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &lx, &ly, &lz);
      glVertex3f(lx, ly, lz);

      glEnd();

      glPointSize(5);
      glBegin(GL_POINTS);
      glVertex3f(lx, ly, lz);
      glEnd();
      glPointSize(1);
    }
  }
  
  glPopAttrib();

  glEndList();
    
  return;
}


// Predraw sensor footprint
void LadarSink::predrawFootprint()
{
  int i;
  float a, b, d;
  float pa, pr;
  float px, py, pz;

  if (this->blobId < 0)
    return;

  if (this->footList == 0)
    this->footList = glGenLists(1);
  glNewList(this->footList, GL_COMPILE);

  a = this->blob.sens2veh[2][0];
  b = this->blob.sens2veh[2][1];
  d = this->blob.sens2veh[2][3] - VEHICLE_TIRE_RADIUS;

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glColor4f(0, 0, 1, 0.50);
  glBegin(GL_POLYGON);
  
  glVertex3f(0, 0, 0);  
  for (i = -90; i < +90; i++)
  {
    // Range and bearing of point on the ground (sensor frame)
    pa = i * M_PI/180;
    pr = -d / (a * cos(pa) + b * sin(pa));
    if (pr < 0 || pr > 80 || !finite(pr)) // MAGIC
      pr = 80;
    LadarRangeBlobScanToSensor(&this->blob, pa, pr, &px, &py, &pz);
    glVertex3f(px, py, pz);
  }
  
  glEnd();
  glDisable(GL_BLEND);
  
  glEndList();
  
  return;
}
