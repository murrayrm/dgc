
/* 
 * Desc: Sink for radar blob data
 * Date: 14 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glu.h>

#include <frames/pose3.h>
#include <alice/AliceConstants.h>
#include <interfaces/SensnetTypes.h>

#include "glUtils.hh"
#include "RadarSink.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
RadarSink::RadarSink(int menuId, int sensorId)
{
  memset(this, 0, sizeof(*this));

  this->menuId = menuId;
  this->sensorId = sensorId;
  this->blobType = SENSNET_RADAR_BLOB;
  this->blobId = -1;

  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
  {
    d = 4 * ((double) i / 0x10000);

    if (d >= 0 && d < 1.0)
    {
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = (int) (d * 0xFF);
      this->rainbow[i][2] = 0xFF;
    }
    else if (d < 2.0)
    {
      d -= 1.0;
      this->rainbow[i][0] = 0x00;
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = (int) ((1 - d) * 0xFF);
    }
    else if (d < 3.0)
    {
      d -= 2.0;
      this->rainbow[i][0] = (int) (d * 0xFF);
      this->rainbow[i][1] = 0xFF;
      this->rainbow[i][2] = 0x00;
    }
    else if (d < 4.0)
    {
      d -= 3.0;
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = (int) ((1 - d) * 0xFF);
      this->rainbow[i][2] = 0x00;
    }
    else
    {
      this->rainbow[i][0] = 0xFF;
      this->rainbow[i][1] = 0x00;
      this->rainbow[i][2] = 0x00;
    }
  }
  
  return;
}


// Enable/disable sink
int RadarSink::enable(sensnet_t *sensnet, sensnet_replay_t *replay, bool enable)
{
  if (sensnet)
  {
    // If in live mode...
    if (enable && !this->enabled)
    {
      sensnet_join(sensnet, this->sensorId, this->blobType, sizeof(this->blob));
      this->enabled = true;
    }
    else if (!enable && this->enabled)
    {
      sensnet_leave(sensnet, this->sensorId, this->blobType);
      this->enabled = false;
    }
  }  
  else if (replay)
  {
    // If in replay mode
    this->enabled = enable;
  }
  return 0;
}


// Update with current sensnet data
int RadarSink::update(sensnet_t *sensnet, sensnet_replay_t *replay)
{
  int blobId;
      
  if (sensnet)
  {
    // Peek at the latest data, but dont read it out
    if (sensnet_peek(sensnet, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;

    // Is the data new?
    if (blobId < 0 || blobId == this->blobId)
      return 0;

    // Read the data
    if (sensnet_read(sensnet, this->sensorId, this->blobType,
                     &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;
  }
  else if (replay)
  {
    // Peek at the latest data, but dont read it out
    if (sensnet_replay_peek(replay, this->sensorId, this->blobType, &blobId, NULL) < 0)
      return -1;
    
    // Is the data new?
    if (blobId < 0 || blobId == this->blobId)
      return 0;

    // Read the data    
    if (sensnet_replay_read(replay, this->sensorId, this->blobType,
                            &this->blobId, sizeof(this->blob), &this->blob) != 0)
      return -1;

    // The data has changed, so we must redraw
    this->dirty = true;    
  }
   
  return 0;
}


// Generate status text
void RadarSink::predrawStatus()
{
  char text[256];
  char temp[] = "%s\n%.3f %d\n";
  
  if (this->statusList == 0)
    this->statusList = glGenLists(1);
  glNewList(this->statusList, GL_COMPILE);  

  snprintf(text, sizeof(text), temp,
           sensnet_id_to_name((sensnet_id_t) this->sensorId),
           fmod((double) this->blob.timestamp * 1e-6, 1e4), this->blobId);

  glPushMatrix();
  glColor3f(0, 1, 0);
  glRotatef(90, 0, 0, -1);
  glTranslatef(0, 0.5, 0);
  glDrawText(0.1, text);  
  glPopMatrix();

  glEndList();
  
  return;
}


// Generate range point cloud (local frame)
void RadarSink::predrawPointCloud()
{
  int i;
  float pa, pr, px, py, pz;
  float vx, vy, vz;
  float ax, ay, az, bx, by, bz;
  RadarRangeBlob *blob;
  RadarRangeBlobObject *obj;

  blob = &this->blob;
  
  if (this->cloudList == 0)
    this->cloudList = glGenLists(1);
  
  glNewList(this->cloudList, GL_COMPILE);

  glPushAttrib(GL_POINT_BIT);
  glPointSize(4.0);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);

  // Draw raw targets
  glColor3f(0, 1, 0);
  for (i = 0; i < RADAR_BLOB_MAX_TARGETS; i++)
  {
    obj = &blob->targets[i];

    if (obj->status == 0)
      continue;

    if (fabs(obj->velocity) < 0.5) // MAGIC
      continue;
    
    pa = obj->yaw;
    pr = obj->range;
    
    // Compute points in sensor frame
    RadarRangeBlobScanToSensor(blob, pa, pr, &px, &py, &pz);

    // Transform to vehicle frame
    RadarRangeBlobSensorToVehicle(blob, px, py, pz, &vx, &vy, &vz);

    // Target location in local frame
    RadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &ax, &ay, &az);
  
    // Velocity vector in the local frame
    vx += obj->velocity;
    RadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &bx, &by, &bz);

    glBegin(GL_POINTS);
    glVertex3f(ax, ay, az);
    glEnd();

    glBegin(GL_LINES);
    glVertex3f(ax, ay, az);
    glVertex3f(bx, by, bz);
    glEnd();
  }
  
  // Draw raw targets
  glColor3f(1, 0, 0);
  glBegin(GL_POINTS);
  for (i = 0; i < RADAR_BLOB_MAX_TRACKS; i++)
  {
    obj = &blob->tracks[i];

    if (obj->status == 0)
      continue;

    if (obj->credibility < 5) // MAGIC
      continue;

    if (fabs(obj->velocity) < 0.5) // MAGIC
      continue;
        
    pa = obj->yaw;
    pr = obj->range;
    
    // Compute points in sensor frame
    RadarRangeBlobScanToSensor(blob, pa, pr, &px, &py, &pz);

    // Transform to vehicle frame
    RadarRangeBlobSensorToVehicle(blob, px, py, pz, &vx, &vy, &vz);

    // Target location in local frame
    RadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &ax, &ay, &az);
  
    // Velocity vector in the local frame
    vx += obj->velocity;
    RadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &bx, &by, &bz);

    glBegin(GL_POINTS);
    glVertex3f(ax, ay, az);
    glEnd();

    glBegin(GL_LINES);
    glVertex3f(ax, ay, az);
    glVertex3f(bx, by, bz);
    glEnd();
  }
  glEnd();
  
  glPopAttrib();
  glEndList();
    
  return;
}

