The follow module performs trajectory tracking for Alice, using a
student-supplied controller and observer.  The documentation
describing how to define your controllers and observers is given on
the GC wiki page:

  http://gc.caltech.edu/wiki/follow

Testing follow
--------------
To make sure the code in this directory is working properly, you
should do the following:

0. Compile the code using 'ymk all', if this wasn't already done by YaM

1. Cd to the root directory of the sandbox and copy the files
   'etc/follow/*.m' into the current directory.  There should be three
   files: UT_lqr.m, UT_sinewave.m, UT_logfile.m.

2. In MATLAB, run the 'UT_lqr' and 'UT_sinewave' scripts to generate a
   basic control law and trajectory.

3. In the main sandbox, run the following commands (in separate
   windows/tabs):

     bin/Drun asim
     bin/Drun planviewer
     bin/Drun follow -c UT_lqr.mat -t UT_sinewave.traj -l default.log \
       -L --projerr

4. Unpause the simulator ('u' in asim) and then resume the controller
   'r' in follow.  When the vehicle comes to a stop at the end of the
   trajectory, hit 'q' to exit follow.

5. Run the 'UT_logplot' script ion MATLAB to view the logged data.
