/*
 * maindisp.dd - main display for follower
 *
 * RMM, 10 Dec 05
 * 
 */

/* Declare callbacks */

/* User specified update routines */

/* Commented out table entries */


%%
Skynet Key: %key              Follow (RMM, 10 Dec 05)      Time (sec): %time      
Home = (%xorigin, %yorigin)                                 Rate (Hz):  %rate 
                                                     Actual Rate (Hz):  %arate

    | Gain | Desired | Control | Total |Ovr? | Override | Actual
----+------+---------+---------+-------+-----+----------+--------
Phi | %pGn | %pFF    | %pCntrl | %pCmd | %pOv| %pRef    | %pVal	  PE = %pe
V   | %vGn | %vFF    | %vCntrl | %vCmd | %vOv| %vRef    | %vVal	  TP = %tp 

 State  | Raw Input    | Cov     | %stateType   | Goal         | Error     Cor	Trj
--------+--------------+---------+--------------+--------------+------- 
X_pos   | %xraw        | %xcov   | %xpos        | %xg          | %xerr     %corx	%txe
Y_pos   | %yraw        | %ycov   | %ypos        | %yg          | %yerr     %cory %tye
Theta   |              | %tcov   | %tpos        | %tg          | %terr     %cort
X_dot   |              | %xvcov  | %xvel        | %xvg         | %xvrr 
Y_dot   |              | %yvcov  | %yvel        | %yvg         | %yvrr 
Th_dot  | %tvraw       | %tvcov  | %tvel        | %tvg         | %tvrr 
X_ddot  |              | %xacccov| %xacc        | %xag         | %xarr    
Y_ddot  |              | %yacccov| %yacc        | %yag         | %yarr     
Th_ddot |              | %tacccov| %tacc        | %tag         | %tarr     

Configuration Files:                         | Ctrlr Status:    Obsvr Status:	
  Controller:     %filenameCtrl              | Enabled:  %sE    Enabled:  %oE	
  Observer:       %filenameObsv              | Paused:   %sP    Disabled: %oD	
  Traj File:      %filenameTraj	             | Disabled: %sD                  
  Log File:       %filenameLog  

------------------------+---------------------+-----------------------
[H] %HOME               | [R] %RESUME         | [O] %OBSERVER     %os
[L] %LOGGING     %ls    | [P] %PAUSE          |
[N] %NEWLOG             | [D] %DISABLE        |
[A] %AUTONAMING     %as | [Q] %QUIT           | %RETURN

Emergency Stop: Hit spacebar or 'D' or scream to the safety driver

Status: %statusMsg
%%

double: %corx correct[0] "%5.2f";
double: %cory correct[1] "%5.2f";
double: %cort correct[2] "%5.2f";

short: %pe PROJ_ERR "%1d";
short: %tp trajpoint "%d";
double: %txe trajerr[0] "%g";
double: %tye trajerr[1] "%g";

short:  %key sn_key "%3d" -ro;
double: %rate controlRate "%5.2f";
double: %arate actualRate "%5.2f" -ro;
double: %time currentTime "%7.3f" -ro;

double: %xorigin xorigin "%7.0f";
double: %yorigin yorigin "%7.0f";

double: %pGn outGain[PHI] "%5.2f";
double: %pCntrl outCtrl[PHI] "%5.2f" -ro;
double: %pFF outFF[PHI] "%5.2f" -ro;
double: %pCmd outCmd[PHI] "%5.2f" -ro;
short:  %pOv outOverride[PHI] "%1d";
double: %pRef outRef[PHI] "%5.2f";
double: %pVal outState[PHI] "%5.2f" -ro;

double: %vGn outGain[V] "%5.2f";
double: %vCntrl outCtrl[V] "%5.2f" -ro;
double: %vFF outFF[V] "%5.2f" -ro;
double: %vCmd outCmd[V] "%5.2f" -ro;
short:  %vOv outOverride[V] "%1d";
double: %vRef outRef[V] "%5.2f";
double: %vVal outState[V] "%5.2f" -ro;

double: %xraw meas[XMEASPOS] "%5.2f" -ro;
double: %yraw meas[YMEASPOS] "%5.2f" -ro;
double: %tvraw meas[TVMEASPOS] "%5.2f" -ro;

double: %xcov covar[XPOS] "%5.2f" -ro;
double: %ycov covar[YPOS] "%5.2f" -ro;
double: %tcov covar[TPOS] "%5.2f" -ro;
double: %xvcov covar[XVEL] "%5.2f" -ro;
double: %yvcov covar[YVEL] "%5.2f" -ro;
double: %tvcov covar[TVEL] "%5.2f" -ro;
# double: %xacccov covar[XACC] "%5.2f" -ro;
# double: %yacccov covar[YACC] "%5.2f" -ro;
# double: %tacccov covar[TACC] "%5.2f" -ro;

double: %xpos inp[XPOS] "%5.2f" -ro;
double: %ypos inp[YPOS] "%5.2f" -ro;
double: %tpos inp[TPOS] "%5.2f" -ro;
double: %xvel inp[XVEL] "%5.2f" -ro;
double: %yvel inp[YVEL] "%5.2f" -ro;
double: %tvel inp[TVEL] "%5.2f" -ro;
# double: %xacc inp[XACC] "%5.2f" -ro;
# double: %yacc inp[YACC] "%5.2f" -ro;
# double: %tacc inp[TACC] "%5.2f" -ro;

double: %xg inp[XPOS+NUMINP] "%5.2f" -ro;
double: %yg inp[YPOS+NUMINP] "%5.2f" -ro;
double: %tg inp[TPOS+NUMINP] "%5.2f" -ro;
double: %xvg inp[XVEL+NUMINP] "%5.2f" -ro;
double: %yvg inp[YVEL+NUMINP] "%5.2f" -ro;
double: %tvg inp[TVEL+NUMINP] "%5.2f" -ro;
# double: %xag inp[XACC+NUMINP] "%5.2f" -ro;
# double: %yag inp[YACC+NUMINP] "%5.2f" -ro;
# double: %tag inp[TACC+NUMINP] "%5.2f" -ro;

double: %xerr err[XPOS] "%5.2f" -ro;
double: %yerr err[YPOS] "%5.2f" -ro;
double: %terr err[TPOS] "%5.2f" -ro;
double: %xvrr err[XVEL] "%5.2f" -ro;
double: %yvrr err[YVEL] "%5.2f" -ro;
double: %tvrr err[TVEL] "%5.2f" -ro;
# double: %xarr err[XACC] "%5.2f" -ro;
# double: %yarr err[YACC] "%5.2f" -ro;
# double: %tarr err[TACC] "%5.2f" -ro;

string: %filenameCtrl ctrlFilename "%s" -callback=resetFiles;
string: %filenameGain gainFilename "%s" -ro -callback=resetFiles;
string: %filenameObsv obsvFilename "%s" -callback=resetFiles;
string: %filenameTraj trajFilename "%s" -callback=resetFiles;
string: %filenameLog logFilename "%s" -callback=dd_redraw;

string: %sE statusEnabled "%s" -ro;
string: %sP statusPaused "%s" -ro;
string: %sD statusDisabled "%s" -ro;

string: %oE obsvEnabled "%s" -ro;
string: %oD obsvDisabled "%s" -ro;

button: %HOME "Home Here" home;
button: %LOGGING "Turn Logging" toggleLogging;
button: %NEWLOG "New Log File" newLogFile;
button: %AUTONAMING "Turn Autonaming" toggleAutoNaming;
button: %RESUME "Resume Control" resumeControl;
button: %PAUSE "Pause Control" pauseControl;
button: %DISABLE "Disable Control" disableControl;
button: %QUIT "Quit" dd_exit_loop;
button: %RETURN "RETURN" dd_prvtbl_cb;
button: %OBSERVER "Turn Observer" toggleObserver;
string: %ls loggingStatus "%s" -ro;
string: %as autoNamingStatus "%s" -ro;
string: %os obsvStatus "%s" -ro;
string: %stateType stateType "%s" -ro;

string: %statusMsg statusMessage "%s" -ro;

tblname: maindisp;
bufname: maindisp_buffer;
