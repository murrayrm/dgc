% UT_lqr.m - generate LQR controller for unit testing
% RMM, 1 Mar 08
%
% This MATLAB script generates a simple LQR controller that can be
% used for unit tests of the follow module.

%
% Control law generation
%
% This section of the script generates a basic LQR controller for the
% system and then converts it into the format needed by follow.
%
% The dynamics that we use for controlling the car are given by the
% lateral dynamics plus an integral error term.
% 
%   eydot = sin(theta) v		y = error in y
%   thdot = v/l tan(phi)		th = error in th
%   zdot = y				z = integral of y error
%
% Note that when using the --projerr flag, the lateral error is given
% by the distance to the nearest point on the trajectory and the angular
% error at that point.
%

% System parameters
vd = 2;					% nominal forward speed
l = 3.55;				% wheel base for Alice
m = 4134;				% vehicle mass in kg

% Process dynamics for the kinematic car (theta = 1)
Ap = [0 vd 0; 0 0 0; 0 0 0];
Bp = [0 0; vd/l 0; 0 1/m];

% Now construct a simple LQR controller
K = lqr(Ap, Bp, eye(size(Ap)), eye(size(Bp'*Bp)))

% Now construct a controller that can be loaded into follow
dT = 0.02;				% sampling time
Ec = [					% selection matrix to get state
  0 1 0 0 0 0 0 0, 0 -1 0 0 0 0 0 0;	% y error
  0 0 1 0 0 0 0 0, 0 0 -1 0 0 0 0 0;	% theta error
  0 0 0 1 0 0 0 0, 0 0 0 -1 0 0 0 0;	% velocity error (in x direction)
];
Ac = [0];				% controller state (unused)
Bc = [0 0 0] * Ec * dT;			% controller input (unused)
Cc = [0; 0];				% controller gain (unused)
Dc = -K(:,1:3) * Ec;			% state feedback

% Save the controller to a file
A = Ac; B = Bc; C = Cc; D = Dc;		% Use standard matrix names
save 'UT_lqr.mat' A B C D;		% Save to file
