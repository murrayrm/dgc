% UT_sinewave.m - generate sinusoidal trajectory file
% RMM, 1 Mar 08
%
% This MATLAB script generates a simple sinusoidal trajectory that can be
% used for unit tests of the follow module.

% Define the parameters for the sinusoid
x0 = [0; 0];				% starting point
time = (0:1:60)';			% time vector for points
vd = 2;					% velocity in the x direction
ymag = 2;				% size of the sinuoid in y direction
omega = 0.5;				% period of the sinusoid

%
% Compute the state and feedforward terms (using flatness)
%

% XY trajectory (flat outputs)
xd = x0(1) + vd * time;
yd = x0(2) + ymag * sin(omega * time);

% Compute the velocities and accelerations of the flat variables
vxd = vd * ones(size(time));
vyd = ymag * omega * cos(omega*time);
axd = 0 * ones(size(time));
ayd = -ymag * omega^2 * sin(omega*time);

% Now compute the remaining state and control variables
thd = atan2(vyd, vxd);			% desired angle
vthd = (vxd .* ayd - vyd .* axd) ./ (vxd.^2 + vyd.^2);
phid = 0 * ones(size(time));		% use controller to generate ffwd
accd = 0 * ones(size(time));		% use controller to generate ffwd

% Velocity along the path, as a check
vd = vxd .* cos(thd) + vyd .* sin(thd);	% desired velocity


% Plot the trajectory so that we know what we are getting
figure(1); clf;
subplot(211); plot(xd, yd); 
  axis([0 100 -15 15]); daspect([1 1 1]);
  xlabel('xd'); ylabel('yd');
subplot(223); plot(time, vd, time, yd, time, thd); 
  xlabel('time'); legend('vd', 'yd', 'thd');
subplot(224); plot(time, phid, time, axd); 
  xlabel('time'); legend('phid', 'axd');

% Save the trajectory to a file
data = [xd yd thd vxd vyd vthd phid accd];	% store data in array
[nrows, ncols] = size(data);			% compute size of traj
header = zeros(1, ncols+1);			% construct header row
header(1) = nrows;				% fill in traj size
header(2) = ncols;
table = [header; [time data]];			% generate trajectory table
save 'UT_sinewave.traj' table -ascii		% save traj file
