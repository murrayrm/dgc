/*
 * \file FollowClient.cc
 * \brief skynet module for follow
 *
 * \author Richard Murray
 * \date 10 Dec 05
 *
 * This file contains the code for implementing the control law in
 * follow.  The controller is specified as a linear, state space
 * system that takes the current state and desired state as the input,
 * and gives the vehicle acceleration and steering angle as the
 * outputs.
 *
 */

#include <math.h>
#include "dgcutils/cfgfile.h"
#include "FollowClient.hh"

extern int PROJ_ERR;		// use projected error?
extern int trajpoint;		// current trajectory point
int project_error(TRAJ_DATA *, double *, double *);

FollowClient::FollowClient(int sn_key, int use_gcdrive, int use_pidvel) 
	: CSkynetContainer(MODfollow, sn_key)
{
  cerr << "FollowClient initalized on key " << sn_key << "\n";

  int Y_FRONT = 1;
  int A_HEADING_FRONT = 0;
  int A_HEADING_REAR = 0;

  /* See if we should use a PID controller for velocity */
  if (use_pidvel) {
    // Start direct copying from TrajFollower constructor
    EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
    EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : 
			      (A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));
    // End direct copying from TrajFollower constructor
    m_speedController = new CPID_Controller(yerrorside, aerrorside);

  } else {
    /* Don't use the PID controller for velocity */
    fprintf(stderr, "NOT using PID controller for velocity\n");
    m_speedController = NULL;
  }

  m_lateralController = NULL;
  m_traj_falcon = NULL;
  m_observer = new CObserver();
  if (setupFiles() != 0) {
    fprintf(stderr, "FollowClient: fatal error loading files\n");
    exit(1);
  }

  controlRate = 50.0;

  logFilename[0] = 0;	// reset log filename
  loggingEnabled = false;
  useAutoLogNaming = true;

  controlEnabled = disabled;
  obsvEnabled = disabled;

  /* See if we are trying to use the gcdrive interface */
  using_gcDrive = 0;
  if (use_gcdrive) {
    my_portHandler = new GcPortHandler();
    m_adriveCommandSF = 
      AdriveCommand::generateSouthface(sn_key, my_portHandler, NULL);
    m_adriveCommandSF->setStaleThreshold(10);
    using_gcDrive = 1;
  }
}

/*
 * Member function for executing control loop
 *
 */
void FollowClient::ControlLoop()
{
  double accelCmd, phi, vRef = 10, accel_Norm, steer_Norm;

  drivecmd_t my_command;
  m_adriveMsgSocket = m_skynet.get_send_sock(SNdrivecmd);
  string logs_location = "";
  double* controllerOutput;
  double* estimatorOutput;
  unsigned long long timeNow, timeDiff;
  double numSecTotal;

  unsigned long long numMicroSecTotal;
  unsigned long long microSecStartProcessing;
  unsigned long long microSecStopProcessing;
  double trajVector[11];

  while (1) {
    DGCgettime(microSecStartProcessing);		

    // Update the current state (via broadcast message)
    UpdateState();
    UpdateActuatorState();

    // Figure out the current time
    // If paused, the current time stays unchanged
    if(controlEnabled == enabled) {
      DGCgettime(timeNow);
      timeDiff = timeNow-timeStart;
      currentTime = DGCtimetosec(timeDiff);
    } else if(controlEnabled == disabled) {
      currentTime = 0.0;
    }

    /*
     * Update the state estimate
     *
     * This code updates the current state estimate using either astate
     * or the internal estimator, depending on flaga settings.
     *
     */

    // Store the raw measurements
    meas[XMEASPOS] = m_state.utmNorthing;
    meas[YMEASPOS] = m_state.utmEasting;
    meas[TVMEASPOS] = m_state.utmYawRate;
    meas[3] = outState[V];	// Use inputs from last iteration
    meas[4] = outState[PHI];

    gamma = 0;			// not used

    if (obsvEnabled == disabled) {
      // Put the data in a simple array for use by the controller
      inp[XPOS] = m_state.utmNorthing;
      inp[YPOS] = m_state.utmEasting;
      inp[TPOS] = m_state.utmYaw;
			
      inp[XVEL] = m_state.utmNorthVel;
      inp[YVEL] = m_state.utmEastVel;
      inp[TVEL] = m_state.utmYawRate;
			
    } else {			
      estimatorOutput = m_observer->obs_compute(meas, gamma);

      inp[XPOS] = estimatorOutput[0];
      inp[YPOS] = estimatorOutput[1];
      inp[TPOS] = estimatorOutput[2];
			
      inp[XVEL] = estimatorOutput[3];
      inp[YVEL] = estimatorOutput[4];
      inp[TVEL] = estimatorOutput[5];
			
      covar[XPOS] = m_observer->covar[0];
      covar[YPOS] = m_observer->covar[7];
      covar[TPOS] = m_observer->covar[14];

      covar[XVEL] = m_observer->covar[21];
      covar[YVEL] = m_observer->covar[28];
      covar[TVEL] = m_observer->covar[35];

      correct[XPOS] = mat_element_get(m_observer->cor, 0, 0);
      correct[YPOS] = mat_element_get(m_observer->cor, 1, 0);
      correct[TPOS] = mat_element_get(m_observer->cor, 2, 0);

      correct[XVEL] = mat_element_get(m_observer->cor, 3, 0);
      correct[YVEL] = mat_element_get(m_observer->cor, 4, 0);
      correct[TVEL] = mat_element_get(m_observer->cor, 5, 0);
    }

    /* Store the actuator state for use in anti-windup controllers */
    inp[PHIPOS] = m_actuatorState.m_steerpos * VEHICLE_MAX_AVG_STEER;
    inp[ACCPOS] =
      m_actuatorState.m_gaspos * VEHICLE_MAX_ACCEL -
      m_actuatorState.m_brakepos * VEHICLE_MAX_DECEL;

    /*
     * Get the current trajectory point
     *
     * Read the trajectory file and figure out where we are supposed
     * to be at.  Also use this to copmute the feedforward forces.
     *
     */

    // Compute error based on error projection flag
    int status;
    if (PROJ_ERR) {
      status = project_error(m_traj_falcon, inp, trajVector);
    } else {
      status = traj_read(m_traj_falcon, trajVector, currentTime);

      /* Shift the origin (as done in project_error() */
      trajVector[XPOS] += xorigin;
      trajVector[YPOS] += yorigin;

      /* Store the current time as the index */
      nearestIndex = (int) currentTime;
    }

    // Check status and make sure everything is OK
    if (status == 2) {
      if (controlEnabled == enabled)
	fprintf(stderr, "Control disabled by trajectory update");
      disableControl(0);

    } else {
      /* Set the feedforward commands based on trajectory file */
      outFF[V] = trajVector[ACCPOS];
      outFF[PHI] = trajVector[PHIPOS];

      /* Copy the desired trajectory into the control input vector */
      for(int i=0; i<NUMINP; i++) {
	inp[NUMINP+i] = trajVector[i];
      }
    }

    /*
     * Now run the controller
     *
     * The control action is split up into lateral control and speed
     * control.  The lateral controller is implemented in falcon, the
     * spead controller is pulled from trajFollower.
     *
     */

    // Perform steering control
    if (controlEnabled == enabled) {
      controllerOutput = ss_compute(m_lateralController, inp);
      outCtrl[PHI] = controllerOutput[PHI];
      outCtrl[V] = controllerOutput[V];

      /* See if we should override the velocity control */
      if (m_speedController != NULL) {
	/* Compute the velocity controller */
	vRef = sqrt(trajVector[XVEL]*trajVector[XVEL] +
		    trajVector[YVEL]*trajVector[YVEL]);
	m_speedController->getVelocityControl_NoErrorChecking(&m_state,
            &m_actuatorState, &accelCmd, &phi, vRef);

	/* Scale the acceleration command (unscaled later) */
	outCtrl[V] = accelCmd *
	  (accelCmd > 0 ? VEHICLE_MAX_ACCEL : VEHICLE_MAX_DECEL);
      }

    } else {
      /* Controller is not enabled */
      outCtrl[V] = -0.9 * VEHICLE_MAX_DECEL;	// bring vehicle to stop
      outCtrl[PHI] = outCtrl[PHI];		// leave steering unchanged
    }

    /* Apply post processing to controller outputs */
    outCmd[PHI] = outGain[PHI]*(outCtrl[PHI] + outFF[PHI]);
    outCmd[V] = outGain[V]*(outCtrl[V]) + outFF[V];

    /* Send the acceleration command to the hardware */
    accel_Norm = outCmd[V] > 0 ?
      (outCmd[V] / VEHICLE_MAX_ACCEL) : (outCmd[V] / VEHICLE_MAX_DECEL);
    accel_Norm = fmax(-1.0, fmin(accel_Norm, 1.0));
    my_command.my_actuator = accel;
    my_command.number_arg = accel_Norm;

    /* See if we should send out commands to actuator */
    if (NO_COMMANDS == 0) {
      if (!using_gcDrive) {
	/* Use old skynet interface (for simulation) */
	m_skynet.send_msg(m_adriveMsgSocket, &my_command, 
			  sizeof(my_command), 0);
      } else {
	/* Use the gcinterface */
	AdriveDirective my_directive;
	my_directive.id = messageID++;
	my_directive.command = SetPosition;
	my_directive.actuator = Acceleration;
	my_directive.arg = my_command.number_arg;

	m_adriveCommandSF->sendDirective(&my_directive);
	my_portHandler->pumpPorts();
	m_adriveCommandSF->haveNewStatus();
      }
    }

    /* Send the steering command to the actuator */
    steer_Norm = outCmd[PHI]/VEHICLE_MAX_AVG_STEER;
    steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);
    my_command.my_actuator = steer;
    my_command.number_arg = steer_Norm;

    if (NO_COMMANDS == 0) {
      if (!using_gcDrive) {
	m_skynet.send_msg(m_adriveMsgSocket, &my_command, 
			  sizeof(my_command), 0);
      } else {
	/* Use the gcinterface */
	AdriveDirective my_directive;
	my_directive.id = messageID++;
	my_directive.command = SetPosition;
	my_directive.actuator = Steering;
	my_directive.arg = my_command.number_arg;

	m_adriveCommandSF->sendDirective(&my_directive);
	my_portHandler->pumpPorts();
	m_adriveCommandSF->haveNewStatus();
      }
    }

    /* Save the current velocity and steering angle for the obverver */
    outState[PHI] = m_actuatorState.m_steerpos*VEHICLE_MAX_AVG_STEER;
    outState[V] = m_state.vehSpeed;
	
    // Calculate errors for sparrow display
    for(int i=0; i<NUMINP; i++) {
      err[i] = inp[i] - inp[i+NUMINP];
    }

    if(loggingEnabled) writeLog();

    /* 
     * Go to sleep until the end of the requested period
     *
     */
    numSecTotal = 1.0/controlRate;
    numMicroSecTotal = (unsigned long long)(numSecTotal*1.0e6);
    DGCgettime(microSecStopProcessing);

    if (numMicroSecTotal - 1000 > 
	(microSecStopProcessing - microSecStartProcessing)) {
      unsigned long long sleepTime;
      DGCusleep(numMicroSecTotal - 
		(microSecStopProcessing - microSecStartProcessing) - 1000);
      DGCgettime(sleepTime);
      actualRate = 1.0e6 / ((double)(sleepTime - microSecStartProcessing));

    } else {
      actualRate = 1.0e6 /
	((double)(microSecStopProcessing - microSecStartProcessing));
    }
  }
}


void FollowClient::writeLog() {
  if (logFile.is_open()) {
    unsigned long long actualTime;
    DGCgettime(actualTime);

    /* Time from start of run + status variables */
    logFile << currentTime << " ";
    logFile << controlEnabled << " " << obsvEnabled << " ";

    /* Vehicle state and inputs + desired trajectory */
    for (int i = 0; i < 2*NUMINP; ++i)
      logFile << setprecision(10) << inp[i] << " ";

    /* Controller output */
    for (int i = 0; i < NUMOUT; ++i)
      logFile << setprecision(10) << outCmd[i] << " ";

    /* Terminate the line */
    logFile << endl;
  }
}

bool FollowClient::toggleLogging() {
  if(loggingEnabled) {
    logFile.close();
    loggingEnabled = false;
  } else {
    loggingEnabled = true;
    if(useAutoLogNaming) {
      time_t currentTime;
      time(&currentTime);
      tm* tmstruct = localtime(&currentTime);
      if (tmstruct != NULL) {
	sprintf(logFilename, "%04d%02d%02d_%02d%02d%02d.log",
		tmstruct->tm_year + 1900, tmstruct->tm_mon + 1,
		tmstruct->tm_mday, tmstruct->tm_hour, 
		tmstruct->tm_min, tmstruct->tm_sec); 
      } else {
	// Couldn't get the current time; print a warning
	perror("follow");
	sprintf(logFilename, "follow.log");
      }
    }

    if(logFile.is_open()) logFile.close();
    logFile.open(logFilename, ofstream::out | ofstream::app);
  }

  return loggingEnabled;
}


bool FollowClient::setControlStatus(FollowClient::ctrlStatus status) {
  bool returnVal = false;

  // Anytime we toggle control status, we should figure out what the
  // closest point on the traj is
  switch(controlEnabled) {
  case enabled:
    switch(status) {
    case enabled:
      break;
    case paused:
      controlEnabled = status;
      DGCgettime(timePause);
      returnVal = true;
      break;
    default:
    case disabled:
      controlEnabled = status;
      returnVal = true;
      break;
    }
    break;
  case paused:
    switch(status) {
    case enabled:
      unsigned long long timeResume;
      DGCgettime(timeResume);
      timeStart = timeStart + timeResume - timePause;
      controlEnabled = status;
      returnVal = true;
      break;
    case paused:
      break;
    default:
    case disabled:
      controlEnabled = status;
      returnVal = true;
      break;
    }
    break;
  default:
  case disabled:
    switch(status) {
    case enabled:
      if((m_traj_falcon != NULL &&
	  m_lateralController != NULL) ||
	 false) {
	DGCgettime(timeStart);
	controlEnabled = status;
	traj_reset(m_traj_falcon);
	returnVal = true;
      } else {
	sprintf(statusMessage,
		"Could not resume control since file is missing!");
      }
      break;
    case paused:
      break;
    default:
    case disabled:
      break;
    }
    break;
  }

  return returnVal;
}


int FollowClient::setupFiles() {
  int errflag = 0;

  if(m_lateralController != NULL) ss_free(m_lateralController);
  if(m_traj_falcon != NULL) traj_free(m_traj_falcon);

  // Load the controller
  char *ctrlfile = dgcFindConfigFile(ctrlFilename, "follow");
  if ((m_lateralController = ss_load(ctrlfile)) == NULL) {
    sprintf(statusMessage, "Error loading controller file '%s'", ctrlFilename);
    ++errflag;
  } else {
    sprintf(statusMessage, "Controller loaded!");
  }

  // Load the trajectory
  char *trajfile = dgcFindConfigFile(trajFilename, "follow");
  if ((m_traj_falcon = traj_load(trajfile)) == NULL) {
    sprintf(statusMessage, "%s, Error loading trajectory file '%s'", 
	    statusMessage, trajFilename);
    ++errflag;
  } else {
    sprintf(statusMessage, "%s, Trajectory loaded!", statusMessage);

    /* Get the initial value of the trajectory */
    int status; double time;
    if ((status = traj_row(m_traj_falcon, 0, &time, initialVector)) != 0) {
      fprintf(stderr, "%s: can't read initial traj point (status %d)\n", 
	      trajFilename, status);
      ++errflag;
    }
    nearestIndex = 0;			// reset initial nearest point on traj

    /* Make sure the size of the trajectory is OK */
    if (traj_numcols(m_traj_falcon) != NUMINP) {
      fprintf(stderr, "%s: wrong number of cols (should be %d + 1)\n",
	      trajFilename, NUMINP);
      ++status;
    }
  }
 
  // Load the observer
  char *obsvfile = dgcFindConfigFile(obsvFilename, "follow");
  if(!(m_observer->loadFile(obsvfile))) {
    sprintf(statusMessage, "%s, Error loading observer file '%s'", 
	    statusMessage, obsvFilename);
    ++errflag;
  } else {
    sprintf(statusMessage, "%s, Observer loaded!", statusMessage);
  }

  /* Initialize internal state (?) */
  for(int i=0; i<NUMOUT; i++) {
    outGain[i] = 1.0;
    outCtrl[i] = 0.0;
    outFF[i] = 0.0;
    outCmd[i] = 0.0;
    outOverride[i] = 0;
    outRef[i] = 0.0;
    outState[i] = 0.0;
  }
  
  return errflag;
}

bool FollowClient::toggleObsvStatus() {
  if(obsvEnabled == disabled) {
    double initial_conds[6];
    for(int i=0; i<6; i++) {
      initial_conds[i] = inp[i];
    }

    m_observer->set_initial_conditions(initial_conds);
    obsvEnabled = enabled;
  } else {
    obsvEnabled = disabled;
  }

  return true;
}

int FollowClient::closestTrajPoint() {
  double  nearestDistSq;
  double  currentDistSq;
  double rowVal[NUMINP];
  double time;
	
  // Compute the nearest point on the trajectory to where we currently are
  nearestDistSq = 1.0e20;
  for(int i=nearestIndex; i<traj_rows(m_traj_falcon); i++) {
    traj_row(m_traj_falcon, i, &time, rowVal);
    currentDistSq =
      pow(inp[XPOS] - (rowVal[XPOS] + xorigin), 2.0) +
      pow(inp[YPOS] - (rowVal[YPOS] + yorigin), 2.0);

    if (currentDistSq < nearestDistSq) {
      nearestDistSq = currentDistSq;
      nearestIndex = i;
    }
  }
  return nearestIndex;
}

/*
 * project_error - compute distance to trajectory
 *
 * This function computes the error between the current state and the nearest 
 * point on a desired trajectory.  It stores the resulting data such that
 * when subtracted from the current state, the resulting error is based
 * on the nearest point to the trajectory.
 *
 * Return status (set to match traj_read):
 *
 *   -1 on an error
 *    0 if at or before the start of the matrix
 *    1 if between time values
 *    2 if at or after the end of the matrix
 *
 */

int FollowClient::project_error(
	TRAJ_DATA *m_traj_falcon,	// Falcon trajectory object
	double *state,			// current state
	double *trajVector)		// nearest point
{
  int i, status;
  extern double trajerr[NUMINP];
  double trajTime;

  /*
   * Find the nearest point on the desired trajectory
   *
   * The first thing that we must do is fine the point on the give
   * trajectory that is nearest to our current position.  We do this
   * by looking for the nearest trajectory point perpindicular to the
   * current vehicle orientation.
   *
   */

  // Get the trajectory entry for the segment we are on
  int startPoint = closestTrajPoint();
  trajpoint = startPoint;

  // If it's the last point, we are done
  if (startPoint == traj_rows(m_traj_falcon)-1) return 2;

#ifdef UNUSED
  double trajStartVector[NUMINP], trajEndVector[NUMINP];

  // Get the start point and end point
  traj_row(m_traj_falcon, startPoint, &trajTime, trajStartVector);
  traj_row(m_traj_falcon, startPoint+1, &trajTime, trajEndVector);
# warning xoffset, yoffset not applied

  // Now figure out the closest location along the segment.  We do
  // this by taking the inner product between the trajectory segment
  // and the vector from start point to current point, then normalize
  // by the length of the trajectory segment
  //
  // Note: this doesn't take current angle into account, so not quite
  // hat we originally wanted (but perhaps good enough for now)
  //
  trajFraction =
    ((trajEndVector[NUMINP+XPOS] - trajStartVector[NUMINP+XPOS]) *
     (state[XPOS] - trajStartVector[NUMINP+XPOS]) +
     (trajEndVector[NUMINP+YPOS] - trajStartVector[NUMINP+YPOS]) *
     (state[YPOS] - trajStartVector[NUMINP+YPOS])) /
    sqrt(pow(trajEndVector[NUMINP+XPOS] - trajStartVector[NUMINP+XPOS], 2.0) -
	 pow(trajEndVector[NUMINP+YPOS] - trajStartVector[NUMINP+YPOS], 2.0));

  // Figure out the time corresponding to that point
  if (trajFraction < 0) {
    cerr << "WARNING: trajFraction < 0; setting to zero\n";
    trajFraction = 0;
  } else if (trajFraction > 1) {
    cerr << "WARNING: trajFraction > 1; setting to one\n";
    trajFraction = 1;
  }
  trajTime = trajStartTime + trajFraction * (trajEndTime - trajStartTime);

  // Get the trajectory for the closest point 
  traj_read(m_traj_falcon, trajVector, trajTime);

#else
  /* Just use the point that is closest as the trajectory */
  traj_row(m_traj_falcon, startPoint, &trajTime, trajVector);

#endif 

  /* Shift the origin if necessary */
  trajVector[XPOS] += xorigin;
  trajVector[YPOS] += yorigin;

  /* 
   * Construct the desired error
   *
   * We now must construct a desired trajectory such that when we
   * subtract the state, we get the desired error vector.  We use the
   * following rules for each state variable:
   *
   * x_err = 0
   * y_err = lateral distance to nearest point
   * t_err = angle between trajectory segment and vehicle angle
   * xvel_err = 0
   * yvel_err = current lateral velocity
   * tvel_err = relative rate error
   *
   */

  // Compute lateral distance in body coordinates
  trajerr[XPOS] = 0;
  trajerr[YPOS] =
    (state[YPOS] - trajVector[YPOS])*cos(trajVector[TPOS]) -
    (state[XPOS] - trajVector[XPOS])*sin(trajVector[TPOS]);

  // Compute error in heading versus angle along the path
  trajerr[TPOS] = state[TPOS] - trajVector[TPOS];

  // Figure out the velocity along the path
  double vdes = sqrt(pow(trajVector[XVEL], 2.0) + pow(trajVector[YVEL], 2.0));
  double vcur = sqrt(pow(state[XVEL], 2.0) + pow(state[YVEL], 2.0));

  // Compute the velocity error in body coordinates
  trajerr[XVEL] = vcur - vdes;
  trajerr[YVEL] = -vdes * sin(trajerr[TPOS]);

  // Compute heading rate error along the path
  trajerr[TVEL] = state[TVEL] - trajVector[TVEL];

  /* 
   * Construct the desired trajectory
   *
   * Finally, we construct the desired trajectory such that when subtracted
   * from the current state, you get the desired error.
   *
   */
  for (i = 0; i < NUMINP-2; ++i) trajVector[i] = state[i] - trajerr[i];

  return status;
}
