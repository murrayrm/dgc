% UT_logfile.m - read log file and plot results
% RMM, 1 Mar 08
%
% This MATLAB script reads the log file created by follow and displays
% the results in a way that can be used for unit testing of the follow
% module.

% Load the logfile from follow
load 'default.log'

% Extract the data that we care about
ctime = default(:, 1);			% current time
ctrlenabled = default(:, 2);		% control enabled
obsvenabled = default(:, 3);		% observer enabled
xpos = default(:, 4);			% vehicle state
ypos = default(:, 5);
tpos = default(:, 6);
xvel = default(:, 7);
yvel = default(:, 8);
tvel = default(:, 9);
phiact = default(:, 10);
accact = default(:, 11);
xdes = default(:, 12);			% desired trajectory
ydes = default(:, 13);
tdes = default(:, 14);
xvdes = default(:, 15);
yvdes = default(:, 16);
tvdes = default(:, 17);
phides = default(:, 18);
accdes = default(:, 19);
phicmd = default(:, 20);		% controller outputs
acccmd = default(:, 21);

% Compute the actual and desired velocity
vel = sqrt(xvel.^2 + yvel.^2);
vdes = sqrt(xvdes.^2 + yvdes.^2);

% Plot the data
figure(1); clf;
subplot(211); 
  plot(xdes, ydes, xpos, ypos); 
  xlabel('x'); ylabel('y'); legend('desired', 'actual');
subplot(223); plot(ctime, ypos - ydes, ctime, vel - vdes); 
  xlabel('time'); legend('e_y', 'e_v');
subplot(224); plot(ctime, phiact, ctime, phicmd); 
  xlabel('time'); legend('phi', 'phicmd');

