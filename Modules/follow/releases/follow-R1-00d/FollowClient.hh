/*
 * follow.h - header file for follow program
 *
 * RMM, 10 Dec 05
 *
 */

#include "skynet/SkynetContainer.hh"	    // skynet object
#include "skynettalker/StateClient.hh"	    // state client object
#include "trajutils/TrajTalker.hh"	    // trajectory receiver
#include "PID_Controller.hh"  // for speed PID control
#include "dgcutils/DGCutils.hh"
#include "interfaces/sn_types.h"
#include "interfaces/ActuatorCommand.h"

// CSA interface
#include "gcinterfaces/AdriveCommand.hh"
#include "gcmodule/GcInterface.hh"
#include "gcmodule/GcModule.hh"

// #include "traj.h"

#include "CObserver.hh"

#include "falcon/state.h"
#include "falcon/traj.h"

/* Origin for locating ourselves on the planet */
extern double xorigin, yorigin;

/* Declare the global input, output and reference variables */
#define NUMINP 8
#define NUMOUT 2
#define NUMMEAS 3

extern double inp[2*NUMINP+1], out[NUMOUT], err[NUMINP], meas[NUMMEAS+NUMOUT];
extern double covar[NUMINP], correct[NUMINP];
extern double outGain[NUMOUT], outCtrl[NUMOUT], outFF[NUMOUT], outCmd[NUMOUT], outRef[NUMOUT], outState[NUMOUT];
extern int outOverride[NUMOUT];
#define ref (inp+NUMINP)

extern int NO_COMMANDS;

extern int sn_key;
extern double controlRate;
extern double currentTime;
extern double actualRate;

extern char ctrlFilename[256];
extern char gainFilename[256];
extern char obsvFilename[256];
extern char trajFilename[256];

extern char statusMessage[80];

extern int disableControl(long arg);

/* Define offsets for states and reference values */
#define XPOS 0
#define YPOS 1
#define TPOS 2
#define XVEL 3
#define YVEL 4
#define TVEL 5
#define PHIPOS 6
#define ACCPOS 7

#define XMEASPOS 0
#define YMEASPOS 1
#define TVMEASPOS 2

#define PHI 0
#define V   1

// Main skynet module
class FollowClient: public CStateClient, public CTrajTalker {
public:
  FollowClient(int sn_key, int use_gcdrive, int use_pidvel = 0);

  int setupFiles();
  void ControlLoop();
  void writeLog();
  bool toggleLogging();

  bool useAutoLogNaming;
  char logFilename[256];
  bool loggingEnabled;

  double initialVector[NUMINP];		// initial traj point
  int nearestIndex;			// previous nearest point on traj

  enum ctrlStatus {
    enabled = 2,
    paused = 1,
    disabled = 0
  };
  ctrlStatus controlEnabled;
  ctrlStatus obsvEnabled;

  bool setControlStatus(FollowClient::ctrlStatus status);
  bool toggleObsvStatus();

private:
  int closestTrajPoint();
  int project_error(TRAJ_DATA *, double *, double *);

  CPID_Controller* m_speedController;
  STATE_SPACE* m_lateralController;
  int m_adriveMsgSocket;
  ofstream logFile;
  unsigned long long timePause;
  unsigned long long timeStart;
  TRAJ_DATA* m_traj_falcon;

  CObserver* m_observer;
  double gamma;

  bool using_gcDrive;  //true makes us talk to gcDrive.
  GcPortHandler* my_portHandler;
  GcModuleLogger* m_trajLogger;
  AdriveCommand::Southface *m_adriveCommandSF;
  AdriveDirective m_adriveDirective;
  unsigned messageID;
};
