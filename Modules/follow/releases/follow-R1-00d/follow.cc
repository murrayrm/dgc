/*
 * \file follow.cc 
 * \brief simple trajectory tracking algorithm for CDS 110b
 *
 * \author Richard M. Murray and Jeremy Gillula
 * \date 2006
 *
 * This file contains a simple trajectory tracking framework for use
 * on Alice.  The controller is defined in a file that is loaded up at
 * run-time, so that students can design controllers and see how they
 * work. 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <getopt.h>
using namespace std;

#include "skynet/skynet.hh";

#include "FollowClient.hh"		// definition of common variables
#include "cmdline.h"
gengetopt_args_info cmdline;		// Make cmd line arguments global

// Global variables
double inp[2*NUMINP+1];		// controller inputs (cur + ref)
double err[NUMINP];		// error between current and reference
double out[NUMOUT];		// controller outputs (acc, steer)
double meas[NUMMEAS+NUMOUT]; //measurements (x,y, theta_dot)
double covar[NUMINP];
double correct[NUMINP];
double outGain[NUMOUT], outCtrl[NUMOUT], outFF[NUMOUT], outCmd[NUMOUT], outRef[NUMOUT], outState[NUMOUT];
int outOverride[NUMOUT];
double xorigin = 0, yorigin = 0;	// Starting point for trajectory
double overrideVel = 0.0;

int sn_key = 0;
double controlRate;
double actualRate;
double currentTime;

#define MAXPATHLEN 255
char ctrlFilename[256] = "default.mat";
char gainFilename[256] = " ";
char obsvFilename[256] = "default_obs.mat";
char trajFilename[256] = "default.traj";
char logFilename[256] = "default.log";
char loggingStatus[4] = "On";
char autoNamingStatus[4] = "Off";
char obsvStatus[4] = "Off";
char stateType[16] = "Value";

char statusEnabled[4] = "[ ]";
char statusDisabled[4] = "[X]";
char statusPaused[4] = "[ ]";
char obsvEnabled[4] = "[ ]";
char obsvDisabled[4] = "[X]";

char statusMessage[80] = "OK";

int USE_SPARROW = 1;
int NO_COMMANDS = 0;
int PROJ_ERR = 0;		// don't project error unless specified
int USE_GCDRIVE = 0;
int trajpoint = -1;		// current trajectory point
double trajerr[11];		// trajectory error (for sparrow)

FollowClient *client;		// skynet client for send/recv messages

//Sparrow functions
int home(long arg);
int toggleLogging(long arg);
int toggleAutoNaming(long arg);
int newLogFile(long arg);
int resumeControl(long arg);
int pauseControl(long arg);
int disableControl(long arg);
int resetFiles(long arg);
int toggleObserver(long arg);

// Sparrow displays - these should be included *after* global variables
#include "sparrow/display.h"
#include "sparrow/errlog.h"
#include "maindisp.h"
#include "overview.h"			// include last for tbl references

int main(int argc, char **argv) {
  /* Process command line options */
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
# ifdef UNUSED
  if (cmdline.config_given &&
      cmdline_parser_configfile(cmdline.config_arg, &cmdline, 0, 0, 1) != 0) {
    exit(1);
  }
# endif

  /* Copy arguments into the old structures */
  if (cmdline.trajectory_given)
    strncpy(trajFilename, cmdline.trajectory_arg, MAXPATHLEN);
  if (cmdline.controller_given)
    strncpy(ctrlFilename, cmdline.controller_arg, MAXPATHLEN);
  if (cmdline.observer_given)
    strncpy(obsvFilename, cmdline.observer_arg, MAXPATHLEN);
  if (cmdline.logfile_given)
    strncpy(logFilename, cmdline.logfile_arg, MAXPATHLEN);

  PROJ_ERR = cmdline.projerr_flag;
  USE_GCDRIVE = cmdline.gcdrive_flag;
  USE_SPARROW = !cmdline.nosparrow_flag;
  NO_COMMANDS = cmdline.nocommands_flag;

  // Get the skynet key that we will be using
  sn_key = skynet_findkey(argc, argv);

  /* 
   * Initialize the follow client 
   *
   * Now we set up the client and adjust any of the member variables
   * to match command line arguments.
   */

  client = new FollowClient(sn_key, USE_GCDRIVE, cmdline.use_pid_velocity_flag);

  /* See if log file was specified */
  if (cmdline.logfile_given) {
    /* Turn off auto log naming if a specific log file name was giving */
    client->useAutoLogNaming = false;
  }

  /*
   * Start up any threads that are required
   *
   * In this section, we start up the various threads that feed data 
   * to the follower.  These threads are each defined in separate
   * files that define the data structures that are used.
   *
   */

  // Threads for reading state and reference
  //   DGCstartMemberFunctionThread(client, &FollowClient::ReadState);
  //   DGCstartMemberFunctionThread(client, &FollowClient::ReadTraj);
  DGCstartMemberFunctionThread(client, &FollowClient::ControlLoop);

  if (USE_SPARROW == 1) {
    // Startup the sparrow display
    if (dd_open() < 0) {
      cerr << "follow: can't initalize display\n";
      exit(1);
    }
    dd_usetbl(overview);		// start in the main display table

    /* Turn on error logging in sparrow */
    dd_errlog_init(50); dd_errlog_bindkey();

    dd_bindkey('Q', dd_exit_loop);
    dd_bindkey('q', dd_exit_loop);
    dd_bindkey('H', home);
    dd_bindkey('h', home);
    dd_bindkey('L', toggleLogging);
    dd_bindkey('l', toggleLogging);
    dd_bindkey('A', toggleAutoNaming);
    dd_bindkey('a', toggleAutoNaming);
    dd_bindkey('N', newLogFile);
    dd_bindkey('n', newLogFile);
    dd_bindkey('R', resumeControl);
    dd_bindkey('r', resumeControl);
    dd_bindkey('E', resumeControl);
    dd_bindkey('e', resumeControl);
    dd_bindkey('P', pauseControl);
    dd_bindkey('p', pauseControl);
    dd_bindkey('D', disableControl);
    dd_bindkey('d', disableControl);
    dd_bindkey('O', toggleObserver);
    dd_bindkey('o', toggleObserver);
    dd_bindkey(' ', disableControl);

    /* Set up display variables that aren't available at compile time */
    dd_rebind("ctrlStatus", &client->controlEnabled);
    dd_rebind("obsvStatus", &client->obsvEnabled);
    dd_rebind("trajIndex", &client->nearestIndex);

    dd_loop();			// start the display manager
    dd_close();			// close up the screen		
  } else {
    while(1) {
      //spin our wheels until I restructure this
      sleep(10000);
    }
  }
  // Close off any threads that we have started

  return 0;
}

/* Reset the offset for the origin of the coordinate system */
int home(long arg) {
  xorigin = inp[XPOS] - client->initialVector[XPOS];
  yorigin = inp[YPOS] - client->initialVector[YPOS];
  client->nearestIndex = 0;	// reset initial nearest point on traj
	
  return 0;
}

/* Turn logging on and off */
int toggleLogging(long arg) {
  sprintf(client->logFilename, "%s", logFilename);

  if(client->toggleLogging()) {
    sprintf(loggingStatus, "Off");
  } else {
    sprintf(loggingStatus, "On");
  }
  sprintf(logFilename, "%s", client->logFilename);
  // dd_redraw(0);

  return 0;
}


int toggleAutoNaming(long arg) {
  if(client->useAutoLogNaming == true) {
    client->useAutoLogNaming = false;
    sprintf(autoNamingStatus, "On");
  } else {
    client->useAutoLogNaming = true;
    sprintf(autoNamingStatus, "Off");		
  }
  // dd_redraw(0);

  return 0;
}


int newLogFile(long arg) {
  if(client->loggingEnabled) {
    toggleLogging(0);
  }
  toggleLogging(0);

  return 0;
}


int resumeControl(long arg) {
  /* See if we should turn on logging */
  if (!client->loggingEnabled && cmdline.autologging_flag) toggleLogging(0);

  int status = client->setControlStatus(FollowClient::enabled);
# ifdef UNUSED
  if (status) {
    sprintf(statusEnabled, "[X]");
    sprintf(statusDisabled, "[ ]");
    sprintf(statusPaused, "[ ]");
    dd_redraw(0);
  }
# endif

  return 0;
}


int pauseControl(long arg) {
  int status = client->setControlStatus(FollowClient::paused);
# ifdef UNUSED
  if (status) {
    sprintf(statusEnabled, "[ ]");
    sprintf(statusDisabled, "[ ]");
    sprintf(statusPaused, "[X]");
    dd_redraw(0);
  }
# endif
  return 0;
}


int disableControl(long arg) {
  int status = client->setControlStatus(FollowClient::disabled);
# ifdef UNUSED
  if (status) {
    sprintf(statusEnabled, "[ ]");
    sprintf(statusDisabled, "[X]");
    sprintf(statusPaused, "[ ]");
    dd_redraw(0);
  }
# endif

  /* See if we should turn off logging */
  if (client->loggingEnabled && cmdline.autologging_flag) toggleLogging(0);

  return 0;
}


int toggleObserver(long arg) {
  client->toggleObsvStatus();

  if(client->obsvEnabled == FollowClient::enabled) {
    sprintf(obsvEnabled, "[X]");
    sprintf(obsvDisabled, "[ ]");
    sprintf(obsvStatus, "Off");
    sprintf(stateType, "Estimate!");
  } else {
    sprintf(obsvEnabled, "[ ]");
    sprintf(obsvDisabled, "[X]");
    sprintf(obsvStatus, "On");
    sprintf(stateType, "Value");
  }
  dd_redraw(0);

  return 0;
}


int resetFiles(long arg) {
  disableControl(0);
  client->setupFiles();

  dd_redraw(0);

  return 0;
}
