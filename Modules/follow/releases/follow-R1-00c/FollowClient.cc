/*
 * FollowClient.cc - skynet module for follow
 *
 * RMM, 10 Dec 05
 *
 */

#include <math.h>
#include "dgcutils/cfgfile.h"
#include "FollowClient.hh"

extern int PROJ_ERR;		// use projected error?
extern int trajpoint;		// current trajectory point
int project_error(TRAJ_DATA *, double *, double *);

FollowClient::FollowClient(int sn_key, int use_gcdrive) 
	: CSkynetContainer(MODfollow, sn_key)
{
  cerr << "FollowClient initalized on key " << sn_key << "\n";

  int Y_FRONT = 1;
  int A_HEADING_FRONT = 0;
  int A_HEADING_REAR = 0;

  //Start direct copying from TrajFollower constructor
  EYerrorside yerrorside = (Y_FRONT==1 ? YERR_FRONT : YERR_BACK);
  EAerrorside aerrorside = (A_HEADING_FRONT==1 ? AERR_FRONT : 
			    (A_HEADING_REAR==1 ? AERR_BACK : AERR_YAW));
  //End direct copying from TrajFollower constructor
  m_speedController = new CPID_Controller(yerrorside, aerrorside);

  m_lateralController = NULL;
  m_traj_falcon = NULL;
  m_observer = new CObserver();
  setupFiles();

  controlRate = 300.0;

  logFilename[0] = 0;	// reset log filename
  loggingEnabled = false;
  useAutoLogNaming = true;

  controlEnabled = disabled;
  obsvEnabled = disabled;

  /* See if we are trying to use the gcdrive interface */
  using_gcDrive = 0;
  if (use_gcdrive) {
    my_portHandler = new GcPortHandler();
    m_adriveCommandSF = 
      AdriveCommand::generateSouthface(sn_key, my_portHandler, NULL);
    m_adriveCommandSF->setStaleThreshold(10);
    using_gcDrive = 1;
  }
}

/*
 * Member function for executing control loop
 *
 */
void FollowClient::ControlLoop()
{
  double accelCmd, phi, vRef = 10, accel_Norm, steer_Norm;

  drivecmd_t my_command;
  m_adriveMsgSocket = m_skynet.get_send_sock(SNdrivecmd);
  string logs_location = "";
  double* controllerOutput;
  double* estimatorOutput;
  unsigned long long timeNow, timeDiff;
  double numSecTotal;

  unsigned long long numMicroSecTotal;
  unsigned long long microSecStartProcessing;
  unsigned long long microSecStopProcessing;
  double trajVector[11];

  while (1) {
    DGCgettime(microSecStartProcessing);		

    // Update the current state (via broadcast message)
    UpdateState();
    UpdateActuatorState();

    // Figure out the current time
    // If paused, the current time stays unchanged
    if(controlEnabled == enabled) {
      DGCgettime(timeNow);
      timeDiff = timeNow-timeStart;
      currentTime = DGCtimetosec(timeDiff);
    } else if(controlEnabled == disabled) {
      currentTime = 0.0;
    }

    /*
     * Update the state estimate
     *
     * This code updates the current state estimate using either astate
     * or the internal estimator, depending on flaga settings.
     *
     */

    // Store the raw measurements
    meas[XMEASPOS] = m_state.utmNorthing; // - xorigin;
    meas[YMEASPOS] = m_state.utmEasting; //- yorigin;
    meas[TVMEASPOS] = m_state.utmYawRate;
    meas[3] = outState[V];	// Use inputs from last iteration
    meas[4] = outState[PHI];

    gamma = 0;			// not used

    if(obsvEnabled == disabled) {
      // Put the data in a simple array for use by the controller
      inp[XPOS] = m_state.utmNorthing - xorigin;
      inp[YPOS] = m_state.utmEasting - yorigin;
      inp[TPOS] = m_state.utmYaw;
			
      inp[XVEL] = m_state.utmNorthVel;
      inp[YVEL] = m_state.utmEastVel;
      inp[TVEL] = m_state.utmYawRate;
			
      inp[XACC] = 0;		// unused; was m_state.utmAcc_N; 
      inp[YACC] = 0;		// unused; was m_state.utmAcc_E;
      inp[TACC] = 0;		// unused; was m_state.utmYawAcc;

    } else {			
      estimatorOutput = m_observer->obs_compute(meas, gamma);

      inp[XPOS] = estimatorOutput[0] - xorigin;
      inp[YPOS] = estimatorOutput[1] - yorigin;
      inp[TPOS] = estimatorOutput[2];
			
      inp[XVEL] = estimatorOutput[3];
      inp[YVEL] = estimatorOutput[4];
      inp[TVEL] = estimatorOutput[5];
			
      inp[XACC] = 0.0;
      inp[YACC] = 0.0;
      inp[TACC] = 0.0;

      covar[XPOS] = m_observer->covar[0];
      covar[YPOS] = m_observer->covar[7];
      covar[TPOS] = m_observer->covar[14];

      covar[XVEL] = m_observer->covar[21];
      covar[YVEL] = m_observer->covar[28];
      covar[TVEL] = m_observer->covar[35];

      correct[XPOS] = mat_element_get(m_observer->cor, 0, 0);
      correct[YPOS] = mat_element_get(m_observer->cor, 1, 0);
      correct[TPOS] = mat_element_get(m_observer->cor, 2, 0);

      correct[XVEL] = mat_element_get(m_observer->cor, 3, 0);
      correct[YVEL] = mat_element_get(m_observer->cor, 4, 0);
      correct[TVEL] = mat_element_get(m_observer->cor, 5, 0);
    }

    /*
     * Get the current trajectory point
     *
     * Read the trajectory file and figure out where we are supposed
     * to be at.  Also use this to copmute the feedforward forces.
     *
     */

    // Compute error based on error projection flag
    int status = PROJ_ERR ? 
      project_error(m_traj_falcon, inp, trajVector) :
      traj_read(m_traj_falcon, trajVector, currentTime);

    // Check status and make sure everything is OK
    if (status == 2) {
      disableControl(0);
    } else {
      outFF[V] = trajVector[0];
      outFF[PHI] = trajVector[1];

      for(int i=0; i<NUMINP; i++) {
	inp[NUMINP+i] = trajVector[2+i];
      }
      // Reorigin the desired stuff
      inp[NUMINP+XPOS] -= xorigin;
      inp[NUMINP+YPOS] -= yorigin;
    }

    /*
     * Now run the controller
     *
     * The control action is split up into lateral control and speed
     * control.  The lateral controller is implemented in falcon, the
     * spead controller is pulled from trajFollower.
     *
     */
    //Perform velocity control
    if(outOverride[V])
      vRef = outRef[V];
    else
      vRef = outFF[V];
    m_speedController->getVelocityControl_NoErrorChecking(&m_state,
        &m_actuatorState, &accelCmd, &phi, vRef);

    outCtrl[V] = accelCmd;
    outCmd[V] = outGain[V]*(outCtrl[V]);// + outFF[V]);
    outState[V] = m_state.vehSpeed;

    accel_Norm = fmax(-1.0, fmin(outCmd[V], 1.0));
    my_command.my_actuator = accel;
    if (controlEnabled == enabled) 
      my_command.number_arg = accel_Norm;
    else
      my_command.number_arg = -1.0;


    if (NO_COMMANDS == 0) {
      if (!using_gcDrive) {
	m_skynet.send_msg(m_adriveMsgSocket, &my_command, 
			  sizeof(my_command), 0);
      } else {
	/* Use the gcinterface */
	AdriveDirective my_directive;
	my_directive.id = messageID++;
	my_directive.command = SetPosition;
	my_directive.actuator = Acceleration;
	my_directive.arg = my_command.number_arg;

	m_adriveCommandSF->sendDirective(&my_directive);
	my_portHandler->pumpPorts();
	m_adriveCommandSF->haveNewStatus();
      }
    }

    outState[PHI] = m_actuatorState.m_steerpos*VEHICLE_MAX_AVG_STEER;
	
    //Perform steering control
    if(controlEnabled == enabled) {
      controllerOutput = ss_compute(m_lateralController, inp);
      outCtrl[PHI] = controllerOutput[0];
      //			outFF[PHI] = 0;
      outCmd[PHI] = outGain[PHI]*(outCtrl[PHI] + outFF[PHI]);
			
      steer_Norm = outCmd[PHI]/VEHICLE_MAX_AVG_STEER;
      steer_Norm = fmax(fmin(steer_Norm, 1.0), -1.0);
    }

    my_command.my_actuator = steer;
    if(controlEnabled == enabled) 
      my_command.number_arg = steer_Norm;
    else
      my_command.number_arg = 0.0;
    if (NO_COMMANDS == 0) {
      if (!using_gcDrive) {
	m_skynet.send_msg(m_adriveMsgSocket, &my_command, 
			  sizeof(my_command), 0);
      } else {
	/* Use the gcinterface */
	AdriveDirective my_directive;
	my_directive.id = messageID++;
	my_directive.command = SetPosition;
	my_directive.actuator = Steering;
	my_directive.arg = my_command.number_arg;

	m_adriveCommandSF->sendDirective(&my_directive);
	my_portHandler->pumpPorts();
	m_adriveCommandSF->haveNewStatus();
      }
    }

    //Calculate Errors
    for(int i=0; i<NUMINP; i++) {
      err[i] = inp[i] - inp[i+NUMINP];
    }

    if(loggingEnabled) writeLog();


    numSecTotal = 1.0/controlRate;
    numMicroSecTotal = (unsigned long long)(numSecTotal*1.0e6);


    /* 
     * Go to sleep until the end of the requested period
     *
     */

    DGCgettime(microSecStopProcessing);



    if(numMicroSecTotal - 1000 > (microSecStopProcessing - microSecStartProcessing)) {
      unsigned long long sleepTime;
      DGCusleep(numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing) - 1000);
      DGCgettime(sleepTime);
      actualRate = 1.0e6/((double)(sleepTime - microSecStartProcessing));
      // 			if(((sleepTime-microSecStopProcessing) > (numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)))) {
      // 				timeDiffFoo = ((sleepTime-microSecStopProcessing) - (numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)));
      // 			} else {
      // 				timeDiffFoo = ((numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)) - (sleepTime-microSecStopProcessing));
      // 			}
      // 			sprintf(statusMessage, "%llu, %llu, should sleep for: %llu, slept for: %llu, diff is %llu", 
      // 							numMicroSecTotal, microSecStopProcessing-microSecStartProcessing, 
      // 							numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing),
      // 							sleepTime-microSecStopProcessing,
      // 							timeDiffFoo);
      // 							(sleepTime-microSecStopProcessing) - (numMicroSecTotal - (microSecStopProcessing - microSecStartProcessing)));
    } else {
      actualRate = 1.0e6/((double)(microSecStopProcessing - microSecStartProcessing));
    }
			 
  }
}


void FollowClient::writeLog() {
  if(logFile.is_open()) {
    unsigned long long actualTime;
    DGCgettime(actualTime);
    logFile << actualTime 
	    << " " << currentTime 
	    << " " << controlEnabled 
	    << " " << outState[V] 
	    << " " << outState[PHI]
	    << " " << outCmd[V]
	    << " " << outCmd[PHI];
    logFile << " " << setprecision(10) << inp[XPOS]+xorigin
	    << " " << setprecision(10) << inp[YPOS]+yorigin;

    for(int i=2; i<NUMINP; i++) {
      logFile << " " << setprecision(10) << inp[i];
    }

    logFile << " " << setprecision(10) << inp[NUMINP+XPOS]+xorigin
	    << " " << setprecision(10) << inp[NUMINP+YPOS]+yorigin;

    for(int i=2+NUMINP; i<NUMINP*2; i++) {
      logFile << " " << setprecision(10) << inp[i];
    }

    logFile << " " << obsvEnabled
	    << " " << setprecision(10) << meas[XMEASPOS]
	    << " " << setprecision(10) << meas[YMEASPOS]
	    << " " << setprecision(10) << meas[TVMEASPOS]
	    << " " << setprecision(10) << gamma;

    logFile << " " << m_state.utmNorthing
	    << " " << m_state.utmEasting
	    << " " << m_state.utmYaw			
	    << " " << m_state.utmNorthVel
	    << " " << m_state.utmEastVel
	    << " " << m_state.utmYawRate
      //				<< " " << m_state.Acc_N
      //				<< " " << m_state.Acc_E
      //				<< " " << m_state.YawAcc
      ;
    for(int i=0; i<NUMINP; i++) {
      logFile << " " << covar[i];
    }

    logFile<<" "<<outFF[PHI];

    logFile << endl;
  } else {
    cout << "file ainb't open" << endl;
  }
}


bool FollowClient::toggleLogging() {
  if(loggingEnabled) {
    logFile.close();
    loggingEnabled = false;
  } else {
    loggingEnabled = true;
    if(useAutoLogNaming) {
      time_t currentTime;
      time(&currentTime);
      tm* tmstruct = localtime(&currentTime);
      if (tmstruct != NULL) {
	sprintf(logFilename, "%04d%02d%02d_%02d%02d%02d.log",
		tmstruct->tm_year + 1900, tmstruct->tm_mon + 1,
		tmstruct->tm_mday, tmstruct->tm_hour, 
		tmstruct->tm_min, tmstruct->tm_sec); 
      } else {
	// Couldn't get the current time; print a warning
	perror("follow");
	sprintf(logFilename, "follow.log");
      }
    }

    if(logFile.is_open()) logFile.close();
    logFile.open(logFilename, ofstream::out | ofstream::app);
  }

  return loggingEnabled;
}


bool FollowClient::setControlStatus(FollowClient::ctrlStatus status) {
	bool returnVal = false;

// 	//Anytime we toggle control status, we should figure out what the closest point on the traj is
	

	switch(controlEnabled) {
	case enabled:
		switch(status) {
		case enabled:
			break;
		case paused:
			controlEnabled = status;
			DGCgettime(timePause);
			returnVal = true;
			break;
		default:
		case disabled:
			controlEnabled = status;
			returnVal = true;
			break;
		}
		break;
	case paused:
		switch(status) {
		case enabled:
			unsigned long long timeResume;
			DGCgettime(timeResume);
			timeStart = timeStart + timeResume - timePause;
			controlEnabled = status;
			returnVal = true;
			break;
		case paused:
			break;
		default:
		case disabled:
			controlEnabled = status;
			returnVal = true;
			break;
		}
		break;
	default:
	case disabled:
		switch(status) {
		case enabled:
			if((m_traj_falcon != NULL &&
				 m_lateralController != NULL) ||
				 false) {
				DGCgettime(timeStart);
				controlEnabled = status;
				traj_reset(m_traj_falcon);
				returnVal = true;
			} else {
				sprintf(statusMessage, "Could not resume control since file is missing!");
			}
			break;
		case paused:
			break;
		default:
		case disabled:
			break;
		}
		break;
	}

	return returnVal;
}


void FollowClient::setupFiles() {
  if(m_lateralController != NULL) ss_free(m_lateralController);
  if(m_traj_falcon != NULL) traj_free(m_traj_falcon);

  // Load the controller
  char *ctrlfile = dgcFindConfigFile(ctrlFilename, "follow");
  if ((m_lateralController = ss_load(ctrlfile)) == NULL) {
    sprintf(statusMessage, "Error loading controller file '%s'", ctrlFilename);
  } else {
    sprintf(statusMessage, "Controller loaded!");
  }

  // Load the trajectory
  char *trajfile = dgcFindConfigFile(trajFilename, "follow");
  if ((m_traj_falcon = traj_load(trajfile)) == NULL) {
    sprintf(statusMessage, "%s, Error loading trajectory file '%s'", 
	    statusMessage, trajFilename);
  } else {
    sprintf(statusMessage, "%s, Trajectory loaded!", statusMessage);
  }
 
  // Load the observer
  char *obsvfile = dgcFindConfigFile(obsvFilename, "follow");
  if(!(m_observer->loadFile(obsvfile))) {
    sprintf(statusMessage, "%s, Error loading observer file '%s'", 
	    statusMessage, obsvFilename);
  } else {
    sprintf(statusMessage, "%s, Observer loaded!", statusMessage);
  }

  /* Initialize internal state (?) */
  for(int i=0; i<NUMOUT; i++) {
    outGain[i] = 1.0;
    outCtrl[i] = 0.0;
    outFF[i] = 0.0;
    outCmd[i] = 0.0;
    outOverride[i] = 0;
    outRef[i] = 0.0;
    outState[i] = 0.0;
  }
}


bool FollowClient::toggleObsvStatus() {
	if(obsvEnabled == disabled) {
		double initial_conds[6];
		for(int i=0; i<6; i++) {
			initial_conds[i] = inp[i];
		}

		initial_conds[XPOS] += xorigin;
		initial_conds[YPOS] += yorigin;

		m_observer->set_initial_conditions(initial_conds);
		obsvEnabled = enabled;
	} else {
		obsvEnabled = disabled;
	}

	return true;
}


int FollowClient::closestTrajPoint() {
  static int     nearestIndex = 0; // save value to avoid going backwards
	double  nearestDistSq;
	double  currentDistSq;
	double rowVal[11];
	double time;
	
	// Compute the nearest point on the trajectory to where we currently are
	nearestDistSq = 1.0e20;
	for(int i=nearestIndex; i<traj_rows(m_traj_falcon); i++) {
			traj_row(m_traj_falcon, i, &time, rowVal);
			currentDistSq =
				pow(inp[XPOS]+xorigin - rowVal[2+XPOS],2.0) +
				pow(inp[YPOS]+yorigin   - rowVal[2+YPOS] ,2.0);
			
			if(currentDistSq < nearestDistSq) {
					nearestDistSq = currentDistSq;
					nearestIndex = i;
			}
	}

	//If we're near the end, round up so we'll stop
// 	if(nearestIndex == traj_rows(m_traj_falcon)-1)
// 		nearestIndex++;

// 	cout << "CLOSEST IS " << nearestIndex << "/" << traj_rows(m_traj_falcon) << " at " << sqrt(nearestDistSq) << endl;
	
	return nearestIndex;
}

/*
 * project_error - compute distance to trajectory
 *
 * This function computes the error between the current state and the nearest 
 * point on a desired trajectory.  It stores the resulting data such that
 * when subtracted from the current state, the resulting error is based
 * on the nearest point to the trajectory.
 *
 * Return status (set to match traj_read):
 *
 *   -1 on an error
 *    0 if at or before the start of the matrix
 *    1 if between time values
 *    2 if at or after the end of the matrix
 *
 */

int FollowClient::project_error(
	TRAJ_DATA *m_traj_falcon,	// Falcon trajectory object
	double *state,			// current state
	double *trajVector)		// nearest point
{
  int i, status;
  extern double trajerr[NUMINP];
  double trajTime;
  double trajStartVector[11], trajEndVector[11];

  /*
   * Find the nearest point on the desired trajectory
   *
   * The first thing that we must do is fine the point on the give
   * trajectory that is nearest to our current position.  We do this
   * by looking for the nearest trajectory point perpindicular to the
   * current vehicle orientation.
   *
   */

  // Get the trajectory entry for the segment we are on
  int startPoint = closestTrajPoint();
  trajpoint = startPoint;

  // If it's the last point, we are done
  if (startPoint == traj_rows(m_traj_falcon)-1) return 2;

  // Get the start point and end point
  traj_row(m_traj_falcon, startPoint, &trajTime, trajStartVector);
  traj_row(m_traj_falcon, startPoint+1, &trajTime, trajEndVector);

  // Now figure out the closest location along the segment.  We do
  // this by taking the inner product between the trajectory segment
  // and the vector from start point to current point, then normalize
  // by the length of the trajectory segment
  //
  // Note: this doesn't take current angle into account, so not quite
  // hat we originally wanted (but perhaps good enough for now)
  //
#ifdef UNUSED
  trajFraction =
    ((trajEndVector[NUMINP+XPOS] - trajStartVector[NUMINP+XPOS]) *
     (state[XPOS] - trajStartVector[NUMINP+XPOS]) +
     (trajEndVector[NUMINP+YPOS] - trajStartVector[NUMINP+YPOS]) *
     (state[YPOS] - trajStartVector[NUMINP+YPOS])) /
    sqrt(pow(trajEndVector[NUMINP+XPOS] - trajStartVector[NUMINP+XPOS], 2.0) -
	 pow(trajEndVector[NUMINP+YPOS] - trajStartVector[NUMINP+YPOS], 2.0));

  // Figure out the time corresponding to that point
  if (trajFraction < 0) {
    cerr << "WARNING: trajFraction < 0; setting to zero\n";
    trajFraction = 0;
  } else if (trajFraction > 1) {
    cerr << "WARNING: trajFraction > 1; setting to one\n";
    trajFraction = 1;
  }
  trajTime = trajStartTime + trajFraction * (trajEndTime - trajStartTime);
#endif

  // Get the trajectory for the closest point 
  traj_read(m_traj_falcon, trajVector, trajTime);

  /* 
   * Construct the desired error
   *
   * We now must construct a desired trajectory such that when we
   * subtract the state, we get the desired error vector.  We use the
   * following rules for each state variable:
   *
   * x_err = 0
   * y_err = lateral distance to nearest point
   * t_err = angle between trajectory segment and vehicle angle
   * xvel_err = 0
   * yvel_err = current lateral velocity
   * tvel_err = relative rate error
   * accels = vehicle accels (assumes constant velocity segments)
   *
   */
  // Compute lateral distance in body coordinates
  trajerr[XPOS] = 0;
  //This is the old version, used in CDS110b, 2006 (computes error 
  // perpendicular to Alice)
  //    trajerr[YPOS] = 
  //    ((state[XPOS]+xorigin - trajVector[2+XPOS]) * sin(state[TPOS]) +
  //    (state[YPOS]+yorigin - trajVector[2+YPOS]) * cos(state[TPOS]));
  
  //This is the new version, which computes y-error the same way 
  //trajFollower does (perpendicular to the traj)
  trajerr[YPOS] =
   - (state[XPOS]+xorigin - trajVector[2+XPOS])*sin(trajVector[2+TPOS]) + 
   (state[YPOS]+yorigin - trajVector[2+YPOS])*cos(trajVector[2+TPOS]);

  // Compute error in heading versus angle along the path
  trajerr[TPOS] = state[TPOS] - trajVector[2+TPOS];

  // Figure out the velocity along the path
  double vdes = sqrt(pow(trajVector[2+XVEL], 2.0) +
		     pow(trajVector[2+YVEL], 2.0));

  // Compute the lateral velocity error in body coordinates
  trajerr[XVEL] = 0;
  trajerr[YVEL] = -vdes * sin(trajerr[TPOS]);

  // Compute heading rate error along the path
  trajerr[TVEL] = state[TVEL] - trajVector[2+TVEL];
			
  trajerr[XACC] = state[XACC];;
  trajerr[YACC] = state[YACC];;
  trajerr[TACC] = state[TACC];;

  /* 
   * Construct the desired trajectory
   *
   * Finally, we construct the desired trajectory such that when subtracted
   * from the current state, you get the desired error.
   *
   */
  for (i = 0; i < NUMINP; ++i) trajVector[2+i] = state[i] - trajerr[i];
  trajVector[2] += xorigin;
  trajVector[3] += yorigin;

  return status;
}
