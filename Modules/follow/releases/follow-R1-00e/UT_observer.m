% UT_observer.m - generate observer for unit testing
% RMM, 4 Mar 08
%
% This MATLAB script generates a file with the covariance matrices required 
% for the observer in follow.

% System parameters
vd = 2;					% nominal forward speed
l = 3.55;				% wheel base for Alice
m = 4134;				% vehicle mass in kg

% Vehicle dynamics (estimator version)
% State = x y theta v omega
A = [
  0 0 0 1 0;			% xdot = v cos(th)
  0 0 vd 0 0;			% ydot = v sin(th)
  0 0 0 0 1;			% thdot = dth
  0 0 0 0 0;			% vdot = uacc/m
  0 0 0 0 0;			% dthdot = uacc/l tan(phi) + phidot (noise)
];
B = [0 0; 0 0; 0 0; 0 1/m; 1/l 0];	% input = phi, acc
C = [1 0 0 0 0; 0 1 0 0 0; 0 0 0 0 1];	% measurement = x, y, dth
D = [0 0; 0 0; 0 0];

% Construct the F matrix for disturbances in steering, acceleration
% These correspond to rotated coordinates
F = [0 0; 0 0; 0 0; 0 1/m; vd/l 0];	% noise in phidot, accel

% Covariance matrices for disturbances and noise
Rv = 0.1 * diag([1 m^2]);		% accel noise in m/s^2 => rescale
Rw = diag([0.2^2 0.2^2 0.01^2]);	% Tuned for asim levels of noise

% Initial conditions for filter
P0 = diag([1 1 0.1 0.1 0.1 0.01]);
X0 = [0 0 0 0 0 0]';

% Use the equilibrium solution of the Riccati equation for initial covariance
[L P0 eigs] = lqe(A, F, C, Rv, Rw, zeros(size(F' * C')))

% Save the observer to matrices to a file
save 'UT_observer.mat' A B C D F Rv Rw P0 X0

