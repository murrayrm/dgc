              Release Notes for "follow" module

Release R1-00e (Sat Mar  8 12:06:17 2008):
	This is a major update to the 'follow' program.  The input formats
	are the same, so previous controllers, trajectories and observers
	should still work.  The output format is different when the observer
	is turned on. 

	Summary of changes:
	  * Switched to falcon library for Kalman filter implementation
	  * Updated log file output to include information observer
	  * Minor changes to unit test scripts: estimator perf + NE coords

	The observer in this impelementation of follow has been
	re-implemented to use the new falcon routine.  Users must specify
	the continuous time linear model (A, B, C, D and F), the covariance
	matrices (Rv and Rw for disturbances and noise) and the initial
	conditions (X0, P0).  The inputs to the observer (measurements) are
	northing (N), easting (E) and yaw rate (Ydot).  The outputs from the
	observer should be northing, easting, yaw (Y), velocity and yaw
	rate.  These are used by follow to fill in the current state (N, E,
	Y, Ndot, Edot, Ydot).  The file 'UT_observer.m' constructs a sample
	observer in the proper format.

	If the --projerr flag is given, follow will update the upper corner
	of the A matrix to correspond to the current (estimated) orientation
	and velocity.  This assumes that the first three states of the
	estimator are N, E and Y and that the system dynamics satisfy

	  dN/dt = V cos(Y)      dE/dt = V sin(Y)

        The other elements of the Kalman filter are *not* updated and at the
	present time the linearization is still used for the prediction
	step.

	If the observer is turned on, then the log file will contain
	additional columns that can be used for debugging.  The following
	entries are included: measurements (N, E, Ydot), Applanix estimate
	(N, E, Y, V, Ydot), estimator state covariance entries (diagonal
	entries of the P matrix).  The file 'UT_logfile.m' shows how to read
	the data.

Release R1-00d (Tue Mar  4 14:52:36 2008):
	This is a major update to the 'follow' program.  NOTE: Previous
	controllers and trajectories are not compatible with this version.

	Summary of changes:
	  * Controller can now command steering and acceleration
	  * Controller input changed; now includes actuator values (for AW)
	  * Modified main display to be smaller and simpler
	  * Updated code for resetting origin of trajectory file ('h' key)
   	  * Trajectory format changed (removed accelerations)
	  * New cmdline options for specifying log file and autologging on run
	  * New MATLAB scripts for generating controller, traj, plots
	  * Reset the default servo rate to 50 Hz

	Controller format: the new version of follow has a modified set of
	inputs and outputs.  The input vector now consists of the vehicle
	state (position, orientation and velocities), the current actuator
	values (for anti-windup compensation) and then the desired vehicle
	state plus feedforward values.  The output vector should now provide
	the desired steering angle and the desired vehicle acceleration (in
	radians and m/s^2, respectively).  This format allows lateral
	position and forward velocity to both be controlled by a
	user-supplied controller.  The MATLAB file UT_lqr.m generates a
	sample controller in the proper format.

	Trajectory file format: the format for trajectories matches the
	controller input vector: time, x, y, th, xdot, ydot, thdot, phi,
	acc.  The MATLAB file UT_sinewave.m generates a sample trajectory in
	the proper format.

	Logging: the file format has changed to consist of the time,
	controller input then controller output.  The MATLAB file
	UT_logfile.m reads a log file in the proper format and plots the
	results.  The '-l' command line option can be used to specify the
	name of a log file on the command line and the '-L' flag can be
	given to automatically turn on logging when the controller is
	resumed (using the 'r' key).

	Display + other changes: the display has been modified to provide a
	simplified view of the programs operation.  Command line processing
	is now via gengetopt, so --help will generate a list of all
	options.  The home function ('h') key should now work better and can
	be used to reset the initial position in a trajectory to the current
	location of the vehicle.

Release R1-00c (Fri Feb  8 19:48:08 2008):
	Added --gcdrive option to allow follow to send gcdrive-compatible
	commands.  Also added stluke_sine_wave.traj to list of files that
	are installed in etc/follow.

Release R1-00b (Mon Jan 28 22:27:50 2008):
	Configuration and default controller files are now loaded using
	dgcFindConfigFile, so that you don't have to run follow out of the
	source directed.

Release R1-00a (Tue Jan  8 20:28:36 2008):
	Initial code, copied over from trunk.  Converted to be compatible
	with new versions of astate and YaM directory structure.

Release R1-00 (Tue Jan  8 19:40:16 2008):
	Created.





