% UT_logfile.m - read log file and plot results
% RMM, 1 Mar 08
%
% This MATLAB script reads the log file created by follow and displays
% the results in a way that can be used for unit testing of the follow
% module.

% Load the logfile from follow
load 'default.log'

logdata = default;			% save the data in a standard name
[nrows, ncols] = size(logdata);		% compute data size

% Extract the data that we care about
ctime = logdata(:, 1);			% current time
ctrlenabled = logdata(:, 2);		% control enabled
obsvenabled = logdata(:, 3);		% observer enabled

xpos = logdata(:, 4);			% vehicle state
ypos = logdata(:, 5);
tpos = logdata(:, 6);
xvel = logdata(:, 7);
yvel = logdata(:, 8);
tvel = logdata(:, 9);
phiact = logdata(:, 10);
accact = logdata(:, 11);

xprj = logdata(:, 12);			% projected trajectory
yprj = logdata(:, 13);
tprj = logdata(:, 14);
xvprj = logdata(:, 15);
yvprj = logdata(:, 16);
tvprj = logdata(:, 17);
phiprj = logdata(:, 18);
accprj = logdata(:, 19);

phicmd = logdata(:, 20);		% controller outputs
acccmd = logdata(:, 21);

% Compute the vehicle velocity
vcur = sqrt(xvel.^2 + yvel.^2);

%
% Unwrap projected error calculations 
%
% We now reconstruct the desired trajectory from the projected error.
% This is mainly a matter or rotating back things into the spatial
% frame (from the body frame).
%
xdes = xpos - (xpos - xprj) .* cos(tprj) + (ypos - yprj) .* sin(tprj);
ydes = ypos - (xpos - xprj) .* sin(tprj) - (ypos - yprj) .* cos(tprj);
tdes = tpos - (tpos - tprj);

% To reconstruct desired velocity, figure out the desire velocity first
terr = tpos - tprj;
vdes = -((xvel - xvprj) - vcur) .* cos(terr) - (yvel - yvprj) .* sin(terr);
xvdes = vdes .* cos(tprj);
yvdes = vdes .* sin(tprj);
tvdes = tvel - (tvel - tvprj);

% Finally, compute the lateral error and velocity error
elat = ypos - yprj;
evel = vcur - vdes;

% Plot the controller data
figure(1); clf;
subplot(211); 
  % Plot the actual versus desired trajectory (note XY instead of NE)
  plot(ypos, xpos, ydes, xdes); 
  xlabel('Easting'); ylabel('Northing'); legend('desired', 'actual');
subplot(223);
  plot(ctime, elat, ctime, evel);
  xlabel('time'); legend('e_{lat}', 'e_{vel}');
subplot(224); plot(ctime, phiact, ctime, phicmd); 
  xlabel('time'); legend('phi', 'phicmd');

% Check to see if the log file has estimator data as well
if (~obsvenabled(1)) return; end;

% Observer was enabled, so file should contain additional data
obscol = 21;				% start of observer data (minus 1)

xmeas = logdata(:, obscol+1);		% measurements
ymeas = logdata(:, obscol+2);
tvmeas = logdata(:, obscol+3);

xact = logdata(:, obscol+4);		% actual vehicle state
yact = logdata(:, obscol+5);
tact = logdata(:, obscol+6);
vact = logdata(:, obscol+7);
tvact = logdata(:, obscol+8);

xcov = logdata(:, obscol+9);		% covariance entries
ycov = logdata(:, obscol+10);
tcov = logdata(:, obscol+11);
vcov = logdata(:, obscol+12);
tvcov = logdata(:, obscol+13);

figure(2); clf;

% Plot the error in the estimate from the actual
subplot(211)
plot(ctime, xpos-xact, ctime, ypos-yact, ctime, tpos-tact, ctime, vcur-vact);
title('Estimation error');
xlabel('Time [sec]'); ylabel('Estimates');
legend('Northing', 'Easting', 'Yaw', 'Velocity');

% Plot the corresponding covariance entries
subplot(212); 
title('Error covariance');
plot(ctime, xcov, ctime, ycov, ctime, tcov, ctime, vcov);
xlabel('Time [sec]'); ylabel('Covariance');
legend('Northing', 'Easting', 'Yaw', 'Velocity');
