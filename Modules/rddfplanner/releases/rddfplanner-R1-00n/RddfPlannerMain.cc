/*!
 * \file RddfPlannerMain.cc
 *
 * \author Richard M. Murray
 * \date 27 January 2007
 *
 * This is the main program for rddfplanner.  It parses the command
 * line arguments, then starts up threads to read in RDDF files and
 * send out paths.
 */

#include <stdlib.h>
#include <iostream>

#include "RddfPlanner.hh"
#include "skynet/skynet.hh"

#include "cmdline.h"
gengetopt_args_info cmdline;

#ifdef SPARROWHAWK
/* SparrowHawk files, including displays */
#include "sparrowhawk/SparrowHawk.hh"
#include "maindisp.h"
#endif

int main(int argc, char** argv)
{
  /* Process command line options */
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  if (cmdline.config_given &&
      cmdline_parser_configfile(cmdline.config_arg, &cmdline, 0, 0, 1) != 0) {
    exit(1);
  }

  /* Retrieve the skynet key */
  int snkey = skynet_findkey(argc, argv);
  cerr << "Constructing skynet with KEY = " << snkey << endl;

  /*
   * Update command line options
   *
   * The original options for rddfplanner were set to use the now
   * deprecated interpolation-based trajectory generation.  Here we
   * reset the various options so that you get the expected behavior
   * when old options are used.
   *
   */

  /* Check to see which trajectory generation algorithm to use */
  if (cmdline.use_interp_flag && cmdline.use_flat_flag) 
    cerr << "Warning: --use-interp overrides --use-flat" << endl;
  cmdline.use_flat_flag = !cmdline.use_interp_flag;

  /* Don't use endpoints with RDDF unless explicitly requested */
  if (cmdline.rddf_given && !cmdline.use_endpoints_flag)
    cmdline.use_endpoints_flag = 0;

  if (cmdline.use_final_flag)
    cerr << "Warning: --use_final is non-standard" << endl;

  /*
   * Run the planner
   *
   * At this point we should be ready to run this planner.  All we
   * need to do is instantiate the object and call the active loop.
   */

  /* Create the planner object */
  RddfPlanner RddfPlannerObj(snkey);

#ifdef SPARROWHAWK
  /* Start up the sparrow display */
  CSparrowHawk &sh = SparrowHawk();
  sh.add_page(rddfPathGen_maindisp, "Main");
  sh.rebind("snkey", &intSkynetKey);
  sh.run();
#endif

  RddfPlannerObj.ActiveLoop(cmdline.verbose_given ? cmdline.verbose_arg : 0);
  return 0;
}
