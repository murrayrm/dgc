/*!
 * \file flattraj.cc
 * \brief Trajectory generation using flatness
 *
 * \author Richard Murray
 * \date 26 Apr 07
 *
 */

#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <assert.h>
using namespace std;

#include "alice/AliceConstants.h"
#include "trajutils/maneuver.h"
#include "trajutils/man2traj.hh"

#include "flattraj.hh"
const int nTrajPoints = 5;	// number of traj points between waypoints

int create_flat_traj(CTraj *ptraj, RDDFVector &waypoints,
		     int first_wp, int last_wp,
		     struct TerminalCondition initial,
		     struct TerminalCondition final, int verbose)
{
  Vehicle *vp = maneuver_create_vehicle(VEHICLE_WHEELBASE, 
					VEHICLE_MAX_AVG_STEER);
  assert(vp != NULL);

  /* Allocate space for keeping trakc of segments and velocities */
  Maneuver **mlist = (Maneuver **) 
    calloc(last_wp-first_wp+1, sizeof(Maneuver *));
  assert(mlist != NULL);
  double *vlist = (double *) calloc(last_wp-first_wp+2, sizeof(double));
  assert(vlist != NULL);

  /* Initialize the trajectory */
  ptraj->startDataInput();

  if (verbose >= 2) {
    cout << "flattraj: computing trajectory" << endl;
    cout << "  Initial: N = " << setprecision(8) << initial.x
	 << ", E = " << setprecision(7) << initial.y 
	 << ", theta = " << setprecision(3) << initial.theta 
	 << ", vel = " << setprecision(3) << initial.v << endl;
    cout << "  Final:   N = " << setprecision(8) << final.x
	 << ", E = " << setprecision(7) << final.y 
	 << ", theta = " << setprecision(3) << final.theta 
	 << ", vel = " << setprecision(3) << final.v << endl;
  }

  /* 
   * Velocity for the first segment 
   *
   * Figure out whether we are starting from a stop and/or coming to a
   * stop.  More comments on my brocken macbook disk.
   */

  const double LOW_SPEED = 1;		// limit for low speed logic
  const double SPEED_INCREMENT = 2;	// amount to bump speed

  if (initial.v < LOW_SPEED && initial.v < waypoints[first_wp].maxSpeed) {
    /* We are trying to get going */
    vlist[0] = min(initial.v + SPEED_INCREMENT, waypoints[first_wp].maxSpeed);

  } else {
    vlist[0] = min(initial.v, waypoints[first_wp].maxSpeed);
  }
  

  /*
   * Solve for intermediate points
   *
   * The first portion of the solution is used for all of the points
   * up to the final one.  In this stage, we solve from one waypoint
   * to the next, with an initial constraint on the angle but no
   * terminal constraint.  The final condition from one iteration
   * serves as the initial condition for the next iteration.
   */
  int nseg = 0;
  Pose2D start, stop;

  // starting point for each interation
  start.x = initial.x; start.y = initial.y; start.theta = initial.theta;

  for (int wp = first_wp+1; wp < last_wp; ++wp) {
    /* Figure out the end point */
    stop.x = waypoints[wp].Northing;
    stop.y = waypoints[wp].Easting;
    // stop.v = waypoints[wp].maxSpeed;
    double next_theta = 
      atan2(waypoints[wp+1].Easting  - waypoints[wp].Easting,
	    waypoints[wp+1].Northing  - waypoints[wp].Northing);
    if (fabs(next_theta - start.theta) > M_PI) {
      /* We have wrapped around M_PI; update next_theta to proper sign */
      if (next_theta < 0) next_theta += 2*M_PI;
      if (start.theta < 0) next_theta -= 2*M_PI;
    }
    stop.theta = (next_theta + start.theta)/2.0;

    /* Figure out the distance to the waypoint */
    double T = sqrt(
      pow(start.x - waypoints[wp].Northing, 2) + 
      pow(start.y - waypoints[wp].Easting, 2)) / 
      max(0.1, waypoints[wp-1].maxSpeed);
    if (verbose >= 5) {
      cout << "Time T = " << T << endl;
    }

    /* Solve for the coefficients */
    maneuver_verbose = verbose;
    Maneuver *mp = maneuver_twopoint(vp, &start, &stop);

    /* Save the maneuver and the (end) speed */
    mlist[nseg++] = mp;
    vlist[nseg] = waypoints[wp].maxSpeed;

    /* Start planning from there the next iteration */
    start = stop;

    if (verbose >= 5) 
      fprintf(stdout, "Reached N = %g, E = %g\n",  start.x, start.y);
  }

  /* 
   * Solve for the final segment
   *
   * For the final portion of the solution, we use the terminal angle
   * as well as the initial angle.  This makes sure that we hit final
   * point at the desired orietnation.
   *
   */

  if (verbose >= 5) {
    cerr << "Final segment: N=" << start.x << ", E=" << start.y
	 << " ---> N=" << final.x << ", E=" << final.y << endl;
  }

  /* Solve for the coefficients */
  stop.x = final.x; stop.y = final.y; stop.theta = final.theta;
  Maneuver *mp = maneuver_twopoint(vp, &start, &stop);

  /* Save the maneuver and the (end) speed */
  mlist[nseg++] = mp;
  vlist[nseg] = min(final.v, waypoints[last_wp].maxSpeed);

  /* Generate the trajectory */
  int npoints = maneuver_profile_generate(vp, nseg, mlist, vlist, 
					  nTrajPoints, ptraj);

  if (verbose >= 2) {
    cout << "  Computed traj with " << npoints << " points" << endl;
  }

  /* Get rid of memory we no longer need */
  for (int i = 0; i < nseg; ++i) maneuver_free(mlist[i]);
  free(mlist);
  free(vlist);

  return npoints;
}

