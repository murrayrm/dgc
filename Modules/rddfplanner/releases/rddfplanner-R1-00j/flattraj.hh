/*!
 * \file flattraj.h
 * \brief Header file for trajectory generation using flatness
 *
 * \author Richard Murray
 * \date 26 Apr 07
 *
 */

#ifndef __FLATTRAJ_HH__
#define __FLATTRAJ_HH__

#include "dgcutils/RDDF.hh"
#include "trajutils/traj.hh"

/*! Terminal conditions */
struct TerminalCondition { double x, y, theta, v; };

int create_flat_traj(CTraj *, RDDFVector &, int, int, 
		     struct TerminalCondition,
		     struct TerminalCondition);

#endif
