/*
 * \file RddfPlanner.cc 
 * \brief simple planner using RDDF centerline
 *
 * \author Richard M. Murray
 * \brief 26 Nov 06
 *
 * This planner reads in an RDDF across skynet and sends out a
 * trajectory corresponding to the centerline of the RDDF corridor.
 *
 * This module was modified from rddfPathGen and it still has some
 * relics of that code.  
 *
 */

#include <unistd.h>
#include <pthread.h>
#include <sstream>
#include <string>
using namespace std;

#include "RddfPlanner.hh"
#include "alice/AliceConstants.h"
#include "trajutils/PathLib.hh"
#include "trajutils/CPath.hh"
#include "dgcutils/RDDF.hh"
#include "trajutils/TrajTalker.hh"
#include "interfaces/sn_types.h"

#include "cmdline.h"
extern gengetopt_args_info cmdline;

#ifdef C2
/* 
 * Include the header file for the refinement stage of the planner,
 * required in order to use Dima's make traj C2 function (used to
 * convert the trajs output by RDDFPathGen to C2 just before they are
 * output from RDDFPathGen
 */
#include "RefinementStage.h"
#endif

RDDF& rddf = *(new RDDF);

/*
 * Processing options - these used to be set on the command line, but
 * for the most part we just hardware them now.
 */

const int NOSKYNET = 0;			// turn off messaging
const int WRITE_COMPLETE_DENSE = 0;	// don't know
const int WRITE_COMPLETE_SPARSE = 1;	// don't know
const int WRITE_EVERY_TRAJ = 0;		// don't know
const int WAIT_STATE = true;		// wait for state data
const int NOMM = 0;			// turn off sending traj's to gui
const int C2 = 0;			// use feasiblize fnction
const int SEND_TRAJ = 1;		// send the trajectory over skynet

RddfPlanner::RddfPlanner(int skynetKey) :
  CSkynetContainer(SNRddfPathGen, skynetKey), CStateClient(true),
  CRDDFTalker(true)
{
}

RddfPlanner::~RddfPlanner()
{
  // Do we need to destruct anything??
  cout << "RddfPlanner destructor has been called." << endl;
}

/*!
 * this is the main function which is run.  execution should be trapped
 * somewhere in this function during operation.
 */
void RddfPlanner::ActiveLoop()
{
  printf("[%s:%d] Entering RddfPlanner::ActiveLoop()\n", __FILE__, __LINE__);

  // Initialization -  set up message counter
  int message_counter = 0;

  // Set up network connection if we are running using skynet
  int skynetguiSocket = m_skynet.get_send_sock(SNtraj);
  int trajSocket = m_skynet.get_send_sock(SNRDDFtraj);

  // generate blank trajectory to be filled in by planner
  CTraj* ptraj;
  ptraj = new CTraj(3);  // order 3 (by default)

  // Figure out how long to sleep for at the end of the loop
  int usleep_time = 1000000/FREQUENCY; // set frequency in RddfPlanner.hh

#ifdef C2
  // Refinement stage for planner; required when using Dima's C2 function
  CRefinementStage refstage;
#endif

  // Main execution loop.  
  while (true) {
    // Read RDDF from file or from talker, depending on cmdline args
    if (cmdline.rddf_given)
      {
	// First time through, read the rddf
	if (message_counter == 0)
	  {
	    rddf.loadFile(cmdline.rddf_arg);
	  }

	// Otherwise sleep
	if (message_counter) usleep(usleep_time);
      }
    else
      {
	/* Get the RDDF via RDDFTalker */
	cout << "rddfPlanner: waiting for RDDF" << endl;
	WaitForRDDF(); UpdateRDDF();
	rddf = m_newRddf;
	cout << "rddfPlanner: received RDDF with " << rddf.getNumTargetPoints()
	     << " points" << endl;
      }

    // update the message counter
    message_counter++;

    rddf.print();
      
    // extract the info from waypoints and store in corridor
    int N = rddf.getNumTargetPoints();   // number of points
    RDDFVector& waypoints = *(new RDDFVector);
    waypoints = rddf.getTargetPoints();

    // store data in corridor
    corridorstruct corridor_whole;
    corridor_whole.numPoints = N;
    for(int i = 0; i < N; i++) {
      // shouldn't get any segfaults here, but who knows...
      corridor_whole.e.push_back(waypoints[i].Easting);
      corridor_whole.n.push_back(waypoints[i].Northing);

      //shrink the corridor width to account for vehicle width, tracking error
      corridor_whole.width.push_back(waypoints[i].radius-RDDF_VEHWIDTH_EFF);
      corridor_whole.speedLimit.push_back(waypoints[i].maxSpeed);
      
      if (waypoints[i].radius-RDDF_VEHWIDTH_EFF < .01)
	cerr << "CORRIDOR WIDTH IS SMALLER THAN RDDF_VEHWIDTH_EFF!" << endl;
    }
    cout << "done (read " << N << " points)." << endl;
	  
    // generate and store whole path (sparse path, so max of 4 path points
    // per corridor point)
    cout << "Generating complete (sparse) path... "; cout.flush();
    pathstruct& path_whole_sparse = *(new pathstruct);
    path_whole_sparse = Path_From_Corridor(corridor_whole);

    // we haven't went anywhere yet, so we're still at the start of the path
    path_whole_sparse.currentPoint = 0; 

    /* create an 'rddf' which contains the path */
    /* This is legacy herman code; may not be quite right */
    RDDF& path_whole_sparse_rddf = *(new RDDF((char *) NULL));
    RDDFData temp_point;
    cout << (int)path_whole_sparse.numPoints << endl;

    for (int i = 0; i < (int)path_whole_sparse.numPoints; i++)
      {
	// Construct the RDDF segment
	temp_point.number = i;
	temp_point.Northing = path_whole_sparse.n[i];
	temp_point.Easting = path_whole_sparse.e[i];
	temp_point.radius = path_whole_sparse.corWidth[i];
	temp_point.maxSpeed = waypoints[i].maxSpeed;

	// Add the segment to the path
	path_whole_sparse_rddf.addDataPoint(temp_point);
      }

    cout << "done (sparse path has " << path_whole_sparse.numPoints
	 << " points, sparse path \'rddf\' has " 
	 << path_whole_sparse_rddf.getNumTargetPoints() << " points)."
	 << endl;

    cout << "-----\n";
    path_whole_sparse_rddf.print();
    cout << "-----\n";
      
    // grab where we are and store it as location, so we only have to
    // access astate once per time through the loop
    vector<double> location(2);
    GetLocation(location);

    pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location,
						   CHOPBEHIND, CHOPAHEAD, -1);
    StorePath(*ptraj, path_chopped_dense);

    if(SEND_TRAJ && !NOSKYNET) {
#ifdef C2
      if (C2 == 1) {
	// Use Dima's function to make the output traj C2
	// This also sets the speed 
	refstage.MakeTrajC2(ptraj);
      } else 
#endif
      if (C2 == 2)
	ptraj->feasiblize(RDDF_MAXLATERAL, RDDF_MAXACCEL,
			  RDDF_MAXDECEL, CURVATURE_AVERAGING_DISTANCE);

#ifdef DEBUG
      cout << "----" << endl;
      ptraj->print();
      cout << "----" << endl;
#endif

      SendTraj(trajSocket, ptraj);

      // Also send to skynetgui
      if (!NOMM) SendTraj(skynetguiSocket, ptraj);
    }
      
      // Sleep until it is time to replan
      usleep(usleep_time);

      // Delete data structures that we are done with
      delete &path_whole_sparse;
      delete &path_whole_sparse_rddf;
      delete &waypoints;
    }
  delete ptraj;
}

void RddfPlanner::GetLocation(vector<double>& location)
{
  UpdateState(); 
  location[0] = m_state.utmNorthing;
  location[1] = m_state.utmEasting;
}

void RddfPlanner::ReceiveDataThread()
{
  cout << "RddfPlanner is unable to receive data at this time." << endl;
}
