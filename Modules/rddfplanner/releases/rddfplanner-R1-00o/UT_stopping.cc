/*!
 * \file UT_stoping.cc
 * \brief Unit test for making sure stopping works as expectec by tplanner
 *
 * \author Richard Murray
 * \date 26 April 2007
 *
 * This unit test sends out information in the same format as tplanner
 * uses to send information to dplanner (using OCPtSpecs).  It starts
 * by sending out an RDDF with the first five points in the specified
 * RDDF and a initial final point that should plan a motion.  It then
 * sends a "STOPPING" type of profile, followed by a stopped profile.
 * This should be run using
 *
 *   rddfplanner --use-endpoints
 *
 */

#include <iostream.h>
#include "interfaces/TpDpInterface.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/RDDFTalker.hh"
#include "dgcutils/RDDF.hh"
#include "UT_cmdline.h"
#include <math.h>

class CRDDFTalkerTestSend : public CRDDFTalker
{
public:
  CRDDFTalkerTestSend(int sn_key, RDDF &rddf)
    : CSkynetContainer(MODrddftalkertestsend, sn_key)
  {
    RDDFVector waypoints = rddf.getTargetPoints();

    cout << "about to get_send_sock...";
    int rddfSocket = m_skynet.get_send_sock(SNrddf);
    cout << " get_send_sock returned" << endl;
		
    cout << "about to send rddf with "
	 << rddf.getNumTargetPoints() << " points" << endl;
    SendRDDF(rddfSocket, &rddf);
    cout << " sent an rddf!" << endl;
    flush(cout);
  }
};

int main(int argc, char** argv)
{

  // Parse command line options
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  /* Make sure that we got an input */
  if (cmdline.inputs_num != 1) {
    fprintf(stderr, "Usage: %s [-S key] rddf-file\n", argv[0]);
    exit(1);
  }

  // Figure out the skynet key
  int skynet_key = skynet_findkey(argc, argv);

  // Load the RDDF file that we will use
  RDDF rddf(cmdline.inputs[0]);	// read from an RDDF file
  rddf.print();		// print out the RDDF file
  RDDFVector waypoints = rddf.getTargetPoints();

  /*
   * Grab the first five segments of the RDDF
   *
   * We send out the first five waypoints in a file and send an
   * initial and final condition corresponding to traversing the first
   * two waypoints (with some small offsets).
   */
  const int rddf_len = 5;		// number of points to scroll ahead
  const double start_offset = 0.5;	// offset at start of RDDF
  const double stop_offset = -0.5;

  int last_wp = min(rddf.getNumTargetPoints()-1, rddf_len-1);

  /*
   * Send out the RDDF file to use
   *
   */
  RDDF segment;
  for (int i = 0; i <= last_wp; ++i) {
    if (i != 0) waypoints[i].maxSpeed = 0;
    segment.addDataPoint(waypoints[i]);
  }
    
  CRDDFTalkerTestSend test(skynet_key, segment);

  /*
   * Send the initial condition as we "drive" down the corridor 
   *
   */
  // Set up the parameters based on the waypoint file
  OCPparams myparams;

  // Initial point = first waypoint, with same upper and lower bounds
  myparams.initialConditionLB[NORTHING_IDX_C] = 
    waypoints[0].Northing + start_offset;
  myparams.initialConditionLB[EASTING_IDX_C] = 
    waypoints[0].Easting + start_offset;
  myparams.initialConditionLB[VELOCITY_IDX_C] = waypoints[0].maxSpeed;

  // Now figure out the angle that we should use
  myparams.initialConditionLB[HEADING_IDX_C] = 
    atan2(waypoints[1].Easting  - waypoints[0].Easting,
	  waypoints[1].Northing  - waypoints[0].Northing);
    
  myparams.initialConditionLB[ACCELERATION_IDX_C] = 0;
  myparams.initialConditionLB[STEERING_IDX_C] = 0;
  for (int i = 0; i < COND_NUMBER; ++i) 
    myparams.initialConditionUB[i] = myparams.initialConditionLB[i];

  // Final point = second waypoint, with same upper and lower bounds
  myparams.finalConditionLB[NORTHING_IDX_C] = 
    waypoints[1].Northing + stop_offset;
  myparams.finalConditionLB[EASTING_IDX_C] = 
    waypoints[1].Easting + stop_offset;
  myparams.finalConditionLB[VELOCITY_IDX_C] = 0;

  // Now figure out the angle that we should use
  myparams.finalConditionLB[HEADING_IDX_C] = 
    atan2(waypoints[1].Easting  - waypoints[0].Easting,
	  waypoints[1].Northing  - waypoints[0].Northing);

  myparams.finalConditionLB[ACCELERATION_IDX_C] = 0;
  myparams.finalConditionLB[STEERING_IDX_C] = 0;
  for (int i = 0; i < COND_NUMBER; ++i) 
    myparams.finalConditionUB[i] = myparams.finalConditionLB[i];

  myparams.parameters[VMIN_IDX_P] = 0.1;
  myparams.parameters[VMAX_IDX_P] = 26.8;
  myparams.parameters[AMIN_IDX_P] = -3.0;
  myparams.parameters[AMAX_IDX_P] = 0.981;
  myparams.parameters[PHIMIN_IDX_P] = -0.45 ;
  myparams.parameters[PHIMAX_IDX_P] = +0.45;
  myparams.parameters[PHIDMIN_IDX_P] = -1.31 ;
  myparams.parameters[PHIDMAX_IDX_P] = +1.31 ;

  // Let the user know what we are sending
  cout << "Sending initial = (N = " 
       << myparams.initialConditionLB[NORTHING_IDX_C] << ", E = "
       << myparams.initialConditionLB[EASTING_IDX_C] << ", V = "
       << myparams.initialConditionUB[VELOCITY_IDX_C] << "), "
       << "final = (N = "
       << myparams.finalConditionLB[NORTHING_IDX_C] << ", E = "
       << myparams.finalConditionLB[EASTING_IDX_C] << ", V = "
       << myparams.finalConditionUB[VELOCITY_IDX_C] << ")" << endl; 

  SkynetTalker<OCPparams> paramsSender(skynet_key, SNocpParams,
				       MODdynamicplanner);
  bool result = paramsSender.send(&myparams);
  cout << " sent: " << result << endl;

  cout << "Press return to send out stopped trajectory"; getc(stdin);
  for (int i = 0; i < COND_NUMBER; ++i) {
    myparams.initialConditionLB[i] = myparams.finalConditionLB[i];
    myparams.initialConditionUB[i] = myparams.finalConditionUB[i];
  }
    
  cout << "Sending initial = (N = " 
       << myparams.initialConditionLB[NORTHING_IDX_C] << ", E = "
       << myparams.initialConditionLB[EASTING_IDX_C] << ", V = "
       << myparams.initialConditionUB[VELOCITY_IDX_C] << "), "
       << "final = (N = "
       << myparams.finalConditionLB[NORTHING_IDX_C] << ", E = "
       << myparams.finalConditionLB[EASTING_IDX_C] << ", V = "
       << myparams.finalConditionUB[VELOCITY_IDX_C] << ")" << endl; 

  result = paramsSender.send(&myparams);
  cout << " sent: " << result << endl;
}
