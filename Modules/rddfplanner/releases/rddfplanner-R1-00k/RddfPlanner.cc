/*
 * \file RddfPlanner.cc 
 * \brief simple planner using RDDF centerline
 *
 * \author Richard M. Murray
 * \brief 26 Nov 06
 *
 * This planner reads in an RDDF across skynet and sends out a
 * trajectory corresponding to the centerline of the RDDF corridor.
 *
 * This module was modified from rddfPathGen and it still has some
 * relics of that code.  
 *
 */

#include <unistd.h>
#include <pthread.h>
#include <sstream>
#include <string>
using namespace std;

#include "alice/AliceConstants.h"
#include "trajutils/PathLib.hh"
#include "trajutils/CPath.hh"
#include "dgcutils/RDDF.hh"
#include "trajutils/TrajTalker.hh"
#include "interfaces/sn_types.h"
#include "trajfollower/trajF_status_struct.hh"

#include "RddfPlanner.hh"
#include "flattraj.hh"

// Pseudocon interface for sending gear changes to trajfollower
#include "pseudocon/interface_superCon_trajF.hh"
using namespace sc_interface;

#include "cmdline.h"
extern gengetopt_args_info cmdline;

/* Functions defined later in this file */
int findClosestPoint(RDDFVector &, RDDFData &);
int create_traj(CTraj *, RDDFVector &, int, int, vector<double>& location, int);

#ifdef C2
/* 
 * Include the header file for the refinement stage of the planner,
 * required in order to use Dima's make traj C2 function (used to
 * convert the trajs output by RDDFPathGen to C2 just before they are
 * output from RDDFPathGen
 */
#include "RefinementStage.h"
#endif

RDDF& rddf = *(new RDDF);

/*
 * Processing options - these used to be set on the command line, but
 * for the most part we just hardware them now.
 */

const int NOSKYNET = 0;			// turn off messaging
const int WRITE_COMPLETE_DENSE = 0;	// don't know
const int WRITE_COMPLETE_SPARSE = 1;	// don't know
const int WRITE_EVERY_TRAJ = 0;		// don't know
const int WAIT_STATE = true;		// wait for state data
const int NOMM = 0;			// turn off sending traj's to gui
const int C2 = 2;			// use feasiblize fnction
const int SEND_TRAJ = 1;		// send the trajectory over skynet

RddfPlanner::RddfPlanner(int skynetKey) :
  CSkynetContainer(SNRddfPathGen, skynetKey), CStateClient(true)
# ifdef USE_RDDFTALKER
  , CRDDFTalker(true)
# endif
{
}

RddfPlanner::~RddfPlanner()
{
  // Do we need to destruct anything??
  if (cmdline.verbose_arg >= 9) 
    cerr << "RddfPlanner destructor has been called." << endl;
}

/*!
 * this is the main function which is run.  execution should be trapped
 * somewhere in this function during operation.
 */
void RddfPlanner::ActiveLoop(int verbose)
{
  if (verbose >= 2)
    printf("[%s:%d] Entering RddfPlanner::ActiveLoop()\n", __FILE__, __LINE__);

  // Initialization -  set up message counter
  int message_counter = 0;

  // Set up network connection if we are running using skynet
  int skynetguiSocket = m_skynet.get_send_sock(SNtraj);
  int trajSocket = m_skynet.get_send_sock(SNRDDFtraj);

  // generate blank trajectory to be filled in by planner
  CTraj* ptraj;
  ptraj = new CTraj(3);  // order 3 (by default)

  // Figure out how long to sleep for at the end of the loop
  int usleep_time = 1000000/FREQUENCY; // set frequency in RddfPlanner.hh

#ifdef C2
  // Refinement stage for planner; required when using Dima's C2 function
  CRefinementStage refstage;
#endif

  // Main execution loop.  
  while (true) {
    // Read RDDF from file or from talker, depending on cmdline args
    if (cmdline.rddf_given) {
      // First time through, read the rddf
      if (message_counter == 0) {
	rddf.loadFile(cmdline.rddf_arg);
      }

      // Otherwise sleep
      if (message_counter) usleep(usleep_time);
    } else {
      /* Get the RDDF via OCPtSpecs */
      m_pOcpTSpecs->getRDDFcorridor(&rddf);

#ifdef USE_RDDFTALKER
      /* Get the RDDF via RDDFTalker */
      if (verbose >= 4) cerr << "rddfPlanner: waiting for RDDF" << endl;
      WaitForRDDF(); UpdateRDDF();
      rddf = m_newRddf;
#endif

      /* Check to make sure we actually got something */
      if (rddf.getNumTargetPoints() == 0) {
	if (verbose) cerr << "empty RDDF" << endl;
	sleep(1);
	continue;
      }

      if (verbose >= 4) {
	cout << "rddfPlanner: received RDDF with " << rddf.getNumTargetPoints()
	     << " points" << endl;
      }
    }

    // update the message counter
    message_counter++;
    if (verbose >= 4) rddf.print();

    /*
     * Check to see if the trajectory is in forward or reverse.  If we
     * switch directions, then send a command to trajfollower via the
     * superCon interface to switch directions
     */
    enum direction { Unknown=0, Forward=1, Reverse=-1 } ;
    static enum direction current_dir = Unknown;
    enum direction command_dir = current_dir;

    /* Look for parameters from OCPtSpecs to decide on direction */
    int mode; m_pOcpTSpecs->getMode(&mode);
    command_dir = (mode == md_REV) ? Reverse : Forward;

    /* Decide if we need to change gears */
    //if (command_dir != current_dir) {
    if (1) {
      if (verbose >= 4) {
	cout << "Change dir: current = " << current_dir
	   << ", command = " << command_dir << endl;
      }
      /* Send a superCon command to trajfollower asking to change gears */
      int tfsocket = m_skynet.get_send_sock(SNsuperconTrajfCmd);
      struct superConTrajFcmd scCmd;
      scCmd.commandType = (command_dir == Forward) ? tf_forwards : tf_reverse;

      // Send the command to trajfollower (via superCon message)
      m_skynet.send_msg(tfsocket, &scCmd, sizeof(scCmd),0);
	
      // Wait for a bit, just to make sure messages stay in order
      // sleep(1);

      // Mark our current gear as the commanded gear
      current_dir = command_dir;
    }
      
    // extract the info from waypoints and store in corridor
    int N = rddf.getNumTargetPoints();   // number of points
    RDDFVector& waypoints = *(new RDDFVector);
    waypoints = rddf.getTargetPoints();

    int first_waypoint = 0;
    int last_waypoint = N-1;
    struct TerminalCondition initialCondition, finalCondition;

    /* See if we should replace the end points with those given in OCPspecs */
    if (cmdline.use_endpoints_flag || cmdline.use_final_flag) {
      BoundsType type;			// type of condition
      double upper[COND_NUMBER];	// storage for initial/final condition
      RDDFData initial, final;

      /* Get the initial condition */
      type = ubt_UpperBound;	// need for final speed
      m_pOcpTSpecs->getInitialCondition(&type, upper);

      if (verbose >= 5) {
	cerr << "OcpTSpecs initial: N = " << upper[NORTHING_IDX_C]
	     << ", E = " << upper[EASTING_IDX_C] 
	     << ", H = " << upper[HEADING_IDX_C] << endl;
      }

      initialCondition.x = initial.Northing = upper[NORTHING_IDX_C];
      initialCondition.y = initial.Easting = upper[EASTING_IDX_C];
      initialCondition.theta = upper[HEADING_IDX_C];
      if ((initialCondition.v = upper[VELOCITY_IDX_C]) == 0) 
	initialCondition.v = 0.01;

      if (verbose >= 5) {
	cout << "Setting initial to N = " << initial.Northing 
	     << ", E = " << initial.Easting << endl;
      }

      /* Get the final condition */
      type = ubt_UpperBound;
      m_pOcpTSpecs->getFinalCondition(&type, upper);

      double lower[COND_NUMBER];	// storage for initial/final condition
      type = ubt_LowerBound;
      m_pOcpTSpecs->getFinalCondition(&type, lower);

      if (verbose >= 5) {
	cerr << "OcpTSpecs final: N = " << upper[NORTHING_IDX_C]
	     << ", E = " << upper[EASTING_IDX_C] 
	     << ", H = " << lower[HEADING_IDX_C]
	     << " to " << upper[HEADING_IDX_C] << endl;
      }

      /* Set the final condition assuming upper == lower */
      finalCondition.x = final.Northing = upper[NORTHING_IDX_C];
      finalCondition.y = final.Easting = upper[EASTING_IDX_C];

      /* Set the final velocity to be the upper bound */
      finalCondition.v = final.maxSpeed = upper[VELOCITY_IDX_C];
      if (finalCondition.v == 0) finalCondition.v = 0.01;

      /* Figure out what angle we should use */
      if (lower[HEADING_IDX_C] == upper[HEADING_IDX_C]) {
	/* Use the heading we were sent */
	finalCondition.theta = upper[HEADING_IDX_C];
	if (verbose >= 5) {
	  cerr << "RddfPlanner: recv'd specific heading for final; theta = " 
	       << finalCondition.theta << endl;
	}

      } else {
	/* We weren't sent a heading, so use the heading of the corridor */
	int wp = findClosestPoint(waypoints, final);
	if (wp < 0) {
	  /* Something is wrong; let's try to do something sensible */
	  cerr << "RddfPlanner: couldn't find waypoint near final" << endl;

	  finalCondition.theta = 
	    (upper[HEADING_IDX_C] + lower[HEADING_IDX_C])/2;

	} else if (wp == 0) {
	  /* Strange condition; final point is near first waypoint */
	  finalCondition.theta = 
	    atan2(waypoints[wp+1].Easting  - waypoints[wp].Easting,
		  waypoints[wp+1].Northing  - waypoints[wp].Northing);

	} else {
	  /* Set the heading based on the corridor */
	  finalCondition.theta = 
	    atan2(waypoints[wp].Easting  - waypoints[wp-1].Easting,
		  waypoints[wp].Northing  - waypoints[wp-1].Northing);
	}

	/* Make sure theta is in the proper range */
	while (finalCondition.theta > M_PI) finalCondition.theta -= 2*M_PI;
	while (finalCondition.theta < -M_PI) finalCondition.theta += 2*M_PI;
      }

      if (verbose >= 5) {
	cout << "RddfPlanner: Setting final (" << N << ") to N = " 
	     << final.Northing << ", E = " << final.Easting 
	     << " at " << final.maxSpeed << " m/s" << endl;
      }

      /* 
       * If we only have two points in the RDDF, just plan between
       * beginning and end.  This is the mode that is used for
       * U-turns.  Otherwise, find the first and last waypoint
       * assuming we are moving in the direction of the RDDF.
       *
       * In the case that N == 2, we already have everything
       * initialized proplery, so we can just skip looking for
       * waypoints.
       */
      if (N != 2) {
	/* Find the initial and final segments of the relevant corridor */
	if ((first_waypoint = findClosestPoint(waypoints, initial)) < 0) 
	  first_waypoint=0;

	if ((last_waypoint = findClosestPoint(waypoints, final)) < 0) 
	  last_waypoint=N;

	/* Make sure we get something sensible */
	if (first_waypoint > last_waypoint) {
	  /* We have past the point we should have cross => don't replan */
	  continue;
	}

	/* Make sure that we don't have first_waypoint == last_waypoint */
	if (first_waypoint == last_waypoint) {
	  /* Figure out which direction I can expand */
	  if (first_waypoint > 0) first_waypoint--;	// plan from prev
	  else if (last_waypoint < N) last_waypoint++;	// plan to next
	  else cerr << "RDDF only has one point" << endl;
	}
      }

      /* Replace the relevant waypoints with the initial and final points */
      if (N == 2 || cmdline.use_endpoints_flag) {
	waypoints[first_waypoint].Northing = initial.Northing;
	waypoints[first_waypoint].Easting = initial.Easting;
      }

      /* Allow final point to be set independent of initial */
      if (N == 2 || cmdline.use_endpoints_flag || cmdline.use_final_flag) {
	waypoints[last_waypoint].Northing = final.Northing;
	waypoints[last_waypoint].Easting = final.Easting;
	if (!cmdline.ignore_end_velocity_flag) {
	  waypoints[last_waypoint].maxSpeed = final.maxSpeed;
	}
      }
    }

    /*
     * At this point in the code, first_waypoint should be the
     * waypoint that is nearest our starting point and last_waypoint
     * is nearest our endpoint.  If the --use-endpoints flag was not
     * specified or we only had two waypoints, then first_waypoint = 0
     * and last_waypoint = N-1.
     */

    if (verbose >= 5) {
      cout << "Planning path from waypoint " << first_waypoint
	   << " to waypoint " << last_waypoint << endl;
    }

    vector<double> location(2);
    if (cmdline.use_endpoints_flag) {
      /* Start planning from the initial point in the path */
      location[0] = waypoints[first_waypoint].Northing;
      location[1] = waypoints[first_waypoint].Easting;

    } else {
      // grab where we are and store it as location, so we only have to
      // access astate once per time through the loop
      GetLocation(location);
    }

    /* Generate the trajectory */
    if (cmdline.use_flat_flag) {
      if (create_flat_traj(ptraj, waypoints, first_waypoint, last_waypoint, 
			   initialCondition, finalCondition, verbose) != 0) {
	cerr << "error creating flat trajectory; trying backup" << endl;
	create_traj(ptraj, waypoints, first_waypoint, last_waypoint, location,
		    verbose);
      }

    } else {
      /* Use the old trajectory generation route */
      create_traj(ptraj, waypoints, first_waypoint, last_waypoint, location,
		  verbose);
    }

    if(SEND_TRAJ && !NOSKYNET) {
      if (verbose >= 3) {
	cout << "----" << endl;
	ptraj->print();
	cout << "----" << endl;
      }

      SendTraj(trajSocket, ptraj);

      // Also send to skynetgui
      if (!NOMM) SendTraj(skynetguiSocket, ptraj);
    }
      
    // Sleep until it is time to replan
    usleep(usleep_time);
    
    delete &waypoints;
  }
  delete ptraj;
}

void RddfPlanner::GetLocation(vector<double>& location)
{
  UpdateState(); 
  location[0] = m_state.utmNorthing;
  location[1] = m_state.utmEasting;
}

void RddfPlanner::ReceiveDataThread()
{
  cerr << "RddfPlanner is unable to receive data at this time." << endl;
}

/*
 * Utility functions
 * 
 *   * findClosestPoint - find the closest point in an RDDF
 *
 */

int findClosestPoint(RDDFVector &path, RDDFData &point)
{
  int closestIndex = -1;
  double distance, closestDistance;

  /* Go through the points and look for the closest one */
  for (unsigned int i = 0; i < path.size(); ++i) {
    /* Compute the distance between points (squared) */
    distance = 
      pow(point.Northing - path[i].Northing, 2.0) + 
      pow(point.Easting - path[i].Easting, 2.0);

    /* See if this is closer than our closest point so far */
    if (closestIndex == -1 || distance < closestDistance) {
      closestIndex = i;
      closestDistance = distance;
    }
  }

  /* Return the index to the closest point */
  return closestIndex;
}

/* Create a trajectory (old style) */
int create_traj(CTraj *ptraj, RDDFVector &waypoints, 
		int first_waypoint, int last_waypoint, 
		vector<double>& location, int verbose) 
{
    // store data in corridor 
    corridorstruct corridor_whole;
    corridor_whole.numPoints = last_waypoint - first_waypoint + 1;
    for(int i = first_waypoint; i <= last_waypoint; ++i) {
      // shouldn't get any segfaults here, but who knows...
      corridor_whole.e.push_back(waypoints[i].Easting);
      corridor_whole.n.push_back(waypoints[i].Northing);

      //shrink the corridor width to account for vehicle width, tracking error
      corridor_whole.width.push_back(waypoints[i].radius-RDDF_VEHWIDTH_EFF);
      corridor_whole.speedLimit.push_back(waypoints[i].maxSpeed);
      
      if (waypoints[i].radius-RDDF_VEHWIDTH_EFF < .01)
	cerr << "CORRIDOR WIDTH IS SMALLER THAN RDDF_VEHWIDTH_EFF!" << endl;
    }
    if (verbose >= 5) {
      cout << "done (generated " << last_waypoint - first_waypoint + 1 
	   << " points)." << endl;
    }
	  
    // generate and store whole path (sparse path, so max of 4 path points
    // per corridor point)
    if (verbose >= 5) {
      cout << "Generating complete (sparse) path... "; cout.flush();
    }
    pathstruct& path_whole_sparse = *(new pathstruct);
    path_whole_sparse = Path_From_Corridor(corridor_whole);

    // we haven't went anywhere yet, so we're still at the start of the path
    path_whole_sparse.currentPoint = 0; 

#ifdef C2
    /* create an 'rddf' which contains the path */
    /* This is legacy herman code; may not be quite right */
    RDDF& path_whole_sparse_rddf = *(new RDDF((char *) NULL));
    RDDFData temp_point;
    if (verbose >= 5) {
      cout << (int)path_whole_sparse.numPoints << endl;
    }

    for (int i = 0; i < (int)path_whole_sparse.numPoints; i++)
      {
	// Construct the RDDF segment
	temp_point.number = i;
	temp_point.Northing = path_whole_sparse.n[i];
	temp_point.Easting = path_whole_sparse.e[i];
	temp_point.radius = path_whole_sparse.corWidth[i];
	temp_point.maxSpeed = waypoints[i].maxSpeed;

	// Add the segment to the path
	path_whole_sparse_rddf.addDataPoint(temp_point);
      }

    if (verbose >= 5) {
      cout << "done (sparse path has " << path_whole_sparse.numPoints
	   << " points, sparse path \'rddf\' has " 
	   << path_whole_sparse_rddf.getNumTargetPoints() << " points)."
	   << endl;

      cout << "-----\n";
      path_whole_sparse_rddf.print();
      cout << "-----\n";
    }
#endif

    pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location,
						   CHOPBEHIND, CHOPAHEAD, -1);
    StorePath(*ptraj, path_chopped_dense);

#ifdef C2
    if (C2 == 1) {
      // Use Dima's function to make the output traj C2
      // This also sets the speed 
      refstage.MakeTrajC2(ptraj);
    } else 
#endif
      if (C2 == 2)
	ptraj->feasiblize(RDDF_MAXLATERAL, RDDF_MAXACCEL, RDDF_MAXDECEL, 
			  cmdline.curvature_averaging_distance_arg);

    // Delete data structures that we are done with
    delete &path_whole_sparse;
#ifdef C2
    delete &path_whole_sparse_rddf;
#endif

    return 0;
}
