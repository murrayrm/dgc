/*!
 * \file RddfPlanner.hh
 * \brief RddfPlanner class definitions
 *
 * \author Jason Yosinski
 * \date 2004-05
 *
 */

#ifndef RDDFPLANNER_HH
#define RDDFPLANNER_HH

#include "skynettalker/StateClient.hh"
#include "skynettalker/RDDFTalker.hh"
#include "skynettalker/SkynetTalker.hh"
#include "trajutils/TrajTalker.hh"
#include "interfaces/TpDpInterface.hh"

/*! frequency at which chopped dense traj's are sent over skynet */
#define FREQUENCY 10

/********************************************
 * Options available to the user
 */

/** distance over which curvature will be averaged when 
 * running with the feasiblize option */
#define CURVATURE_AVERAGING_DISTANCE 5.0

/** If a turn has a radius greater than this, it will be
 * be reduced to this value.  This is so on extremely long
 * corridor segments we don't end up really far away from
 * the centerline of the corridor in exchange for a
 * marginal increase in maximum dynamically feasible
 * speed */
#define MAXRADIUS 200.0

/** The file containing the rddf.  Most likely this will
 * be a symlink to dgc/rddf/rddf.dat */
#define RDDF_FILE "rddf.dat"

/** The file that the complete dense traj is output to. */
#define TRAJOUTPUT "rddf.traj"

/** The file that the complete sparse traj is output to. */
#define SPARSE_PATH_FILE "sparse_path.traj"

/** RddfPlanner will continuously output the most recent traj
 * to a file starting with this base name */
#define CHOPPED_OUTPUT_BASE "chopped_traj" 


/** A slightly advanced option.  To deal with
 * overlapping paths, RddfPlanner keeps track of where
 * we are on the complete sparse path.  In theory,
 * there is a pathological example where our algorithm
 * breaks down (starting pathgen in the middle of a
 * long corridor with loops) and becomes stuck, 
 * thinking we are closest to a point quite far from
 * the actual vehicle location.  However, if at any
 * point we're farther than MAXDISTANCEFROMPATH from
 * the point on the path we think is closest, RPG will
 * do an exhaustive search of all points to guarantee
 * our choice of the closest point is the correct one.
 **/
#define MAXDISTANCEFROMPATH 30

/** Several constants that RddfPlanner needs are
 * defined in AliceConstants.h.  These include:
 * -#RDDF_MAXACCEL - maximum forward acceleration
 * -#RDDF_MAXDECEL - maximum braking acceleration
 * -#RDDF_MAXLATERAL - maximum sideways acceleration
 * -#RDDF_VEHWIDTH_EFF - width subtracted from the
 *    corridor before any paths are computed.
 *    The minimum corridor width must be greater than
 *    this value.
 */

/*!
 * \class RddfPlanner
 */
class RddfPlanner : public CTrajTalker, public CRDDFTalker
{
  // optimal control problem parameters
  OCPparams receivedParams;
  pthread_mutex_t paramsMutex;
  SkynetTalker<OCPparams> paramsTalker;

protected:
  OCPparams params;

public:
  /** Constructor
   *  @param skynetKey the skynet key the constructor uses to send traj's */
  RddfPlanner(int skynetKey);

  /** Standard destructor */
  ~RddfPlanner();

  /** The main loop where execution is trapped */
  void ActiveLoop(int);
};

#endif  // RDDFPLANNER_HH
