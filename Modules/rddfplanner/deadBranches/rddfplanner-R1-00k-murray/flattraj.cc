/*!
 * \file flattraj.cc
 * \brief Trajectory generation using flatness
 *
 * \author Richard Murray
 * \date 26 Apr 07
 *
 */

#include <math.h>
#include "flattraj.hh"

/* dgels declaration */
extern "C" {
extern void dgels_(char *, int *, int *, int *, double *, int *, double *,
	      int *, double *, int *, int *);
}

/* Some constants that control our operations */
const int nBasisFunctions = 5;	// number of basis functions
const int nBasisDerivs = 1;	// number of derivatives supported
const int nFlatOutputs = 2;	// number of flat outputs that we use
const int nFlatDerivs = 1;	// number of derivatives of flat outputs used
const int nTrajPoints = 10;	// number of traj points between waypoints

/* Basis functions - these are used for planning trajectories */
double basis(int index, int deriv, double time)
{
  switch (index) {
  case 0:			// phi(t) = (1-t)^3
    switch (deriv) {
    case 0:	return pow(1.0 - time, 3.0);
    case 1:	return -3.0 * pow(1.0 - time, 2.0);
    case 2:	return 6.0 * (1.0 - time);
    }

  case 1:			// phi(t) = 3 t (1-t)^2
    switch (deriv) {
    case 0:	return 3.0 * time * pow(1 - time, 2.0);
    case 1:	return 3.0 * pow(1-time, 2.0) - 6.0 * time * (1-time);
    case 2:	return -6.0 * (1-time) - 6.0 * (1-time) + 6.0*time;
    }

  case 2:			// phi(t) = 3 t^2 (1-t)
    switch (deriv) {
    case 0:	return 3.0 * pow(time, 2.0) * (1 - time);
    case 1:	return 6.0 * time * (1 - time) - 3.0 * pow(time, 2.0);
    case 2:	return 6.0 * (1-time) - 6.0 * time - 6.0 * time;
    }

  case 3:			// phi(t) = t^3
    switch (deriv) {
    case 0:	return pow(time, 3.0);
    case 1:	return 3.0 * pow(time, 2.0);
    case 2:	return 6.0 * time;
    }

  case 4:			// phi(t) = 5 t
    switch (deriv) {
    case 0:	return 5*time;
    case 1:	return 5.0;
    case 2:	return 0;
    }
  }
  return 0;
}

int create_flat_traj(CTraj *ptraj, RDDFVector &waypoints,
		     int first_wp, int last_wp,
		     struct TerminalCondition initial,
		     struct TerminalCondition final, int verbose)
{
  /* Initialize the trajectory */
  ptraj->startDataInput();
  int npoints = 0;

  if (verbose >= 2) {
    cout << "Initial: N = " << initial.x << ", E = " << initial.y 
	 << ", theta = " << initial.theta << ", vel = " << initial.v << endl;
    cout << "Final: N = " << final.x << ", E = " << final.y 
	 << ", theta = " << final.theta << ", vel = " << final.v << endl;
  }

  /*
   * Solve for intermediate points
   *
   * The first portion of the solution is used for all of the points
   * up to the final one.  In this stage, we solve from one waypoint
   * to the next, with an initial constraint on the angle but no
   * terminal constraint.  The final condition from one iteration
   * serves as the initial condition for the next iteration.
   */
  for (int wp = first_wp+1; wp < last_wp; ++wp) {
    /* Figure out the distance to the waypoint */
    double T = sqrt(
      pow(initial.x - waypoints[wp].Northing, 2) + 
      pow(initial.y - waypoints[wp].Easting, 2)) / 
      max(0.1, waypoints[wp-1].maxSpeed);
    if (verbose >= 5) {
      cout << "Time T = " << T << endl;
    }

    /* Generate the linear problem that we need to solve */
    double zbar[2*nFlatOutputs*(1+nFlatDerivs) - 2];
    double alpha[nFlatOutputs*nBasisFunctions];
    double M[2*nFlatOutputs*(1+nFlatDerivs) - 2][nFlatOutputs*nBasisFunctions];
    for (int index = 0; index < nBasisFunctions; ++index) {
      int row = 0;

      /* Initial angle and velocity */
      M[row][index] = basis(index, 1, 0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = initial.v * cos(initial.theta);

      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 1, 0);
      zbar[row++] = initial.v * sin(initial.theta);

      /* Final position in x (Northing) */
      M[row][index] = basis(index, 0, 1.0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = waypoints[wp].Northing - initial.x;

      /* Final position in y (Easting) */
      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 0, 1.0); 
      zbar[row++] = waypoints[wp].Easting - initial.y;

      /* Set the final angle to the average angle */
      double next_theta = 
	atan2(waypoints[wp+1].Easting  - waypoints[wp].Easting,
	      waypoints[wp+1].Northing  - waypoints[wp].Northing);
      double mid_theta;
      if (fabs(next_theta - initial.theta) > M_PI) {
	/* We have wrapped around M_PI; update next_theta to proper sign */
	if (next_theta < 0) next_theta += 2*M_PI;
	if (initial.theta < 0) next_theta -= 2*M_PI;
      }
      mid_theta = (next_theta + initial.theta)/2.0;

      /* Make sure angle is in the right range */
      while (mid_theta > M_PI) mid_theta -= 2*M_PI;
      while (mid_theta < -M_PI) mid_theta += 2*M_PI;

      /* Final point and velocity */
      M[row][index] = basis(index, 1, 1.0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = waypoints[wp].maxSpeed * cos(mid_theta);

      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 1, 1.0);
      zbar[row++] = waypoints[wp].maxSpeed * sin(mid_theta);
    }

    /* Declare the variables that we need for lapack */
    char trans = 'N';		// solve for underdetermined system
    int nrows = 2*nFlatOutputs*(1+nFlatDerivs) - 2, lda = nrows;
    int ncols = nFlatOutputs*nBasisFunctions, ldb = max(nrows,ncols);
    int nrhs = 1;
    int lwork = 2*nrows*ncols;
    double A[ncols*nrows];
    double work[2*nrows*ncols];
    int info;

    /* Fill in the matrices with the information */
    for (int i = 0; i < nrows; ++i) {
      for (int j = 0; j < ncols; ++j) {
	A[i + j*nrows] = M[i][j];
      }
      alpha[i] = zbar[i];
    }

    dgels_(&trans, &nrows, &ncols, &nrhs, A, &lda, 
	   alpha, &ldb, work, &lwork, &info);
    if (verbose >= 5) {
      cerr << "dgels: info = " << info << endl;
    }

    /* Check the answer to see if it is OK */
    double zchk[2*nFlatOutputs*(1+nFlatDerivs) - 2];
    for (int i = 0; i < nrows; ++i) {
      zchk[i] = 0;
      for (int j = 0; j < ncols; ++j) {
	zchk[i] += M[i][j] * alpha[j];
      }
    }

    if (verbose >= 5) {
      // Print the problems we are trying to solve
      unsigned int row;
      for (row = 0; row < 2*nFlatOutputs*(1+nFlatDerivs) - 2; ++row) {
	cout << zbar[row] << " = M[" << row << ",:] = [";
	for (unsigned int col = 0; col < nFlatOutputs*nBasisFunctions; ++col) {
	  cout << M[row][col] << " ";
	}
	cout << "] --- chk: " << zchk[row] << endl;
      }

      for (row = 0; row < 2*nBasisFunctions; ++row) {
	cout << "alpha[" << row << "] = " << alpha[row] << endl;
      }
    }

    /* Create the CTraj given the flat trajectory (1 meter spacing) */
    double N, Nd, Ndd, E, Ed, Edd;
    for (double t = 0; t < T + T/(2*nTrajPoints); t += T/nTrajPoints) {
      N = initial.x, Nd = 0, Ndd = 0;
      E = initial.y, Ed = 0, Edd = 0;
      for (int index = 0; index < nBasisFunctions; ++index) {
	N += alpha[index] * basis(index, 0, t/T);
	E += alpha[index+nBasisFunctions] * basis(index, 0, t/T);

	Nd += alpha[index] * basis(index, 1, t/T);
	Ed += alpha[index+nBasisFunctions] * basis(index, 1, t/T);

	Ndd += alpha[index] * basis(index, 2, t/T);
	Edd += alpha[index+nBasisFunctions] * basis(index, 2, t/T);
      }

      /* Add the point to the trajectory, minus the last point */
      if (t < T - T/(2*nTrajPoints)) {
	ptraj->addPoint(N, Nd, Ndd, E, Ed, Edd);
	++npoints;
      }
    }

    /* Reset the initial condition to the final state we reached */
    initial.x = N;
    initial.y = E;
    initial.theta = atan2(Ed, Nd);
    initial.v = waypoints[wp+1].maxSpeed;
    if (verbose >= 5) {
      cout << "Reached N = " << initial.x << ", E = " << initial.y << endl;
    }
  }

  /* 
   * Solve for the final segment
   *
   * For the final portion of the solution, we use the terminal angle
   * as well as the initial angle.  This makes sure that we hit final
   * point at the desired orietnation.
   *
   */

  if (verbose >= 5) {
    cerr << "Final segment: N=" << initial.x << ", E=" << initial.y
	 << " ---> N=" << final.x << ", E=" << final.y << endl;
  }

  /* Figure out the rough distance to the end point */
  double T = sqrt(
    pow(initial.x - final.x, 2) + pow(initial.y - final.y, 2)) /
    max(initial.v, waypoints[last_wp-1].maxSpeed);
  if (verbose >= 5) {
    cout << "Time T = " << T << endl;
  }

  /* Generate the linear problem that we need to solve */
  double zbar[2*nFlatOutputs*(1+nFlatDerivs) - 2];
  double alpha[nFlatOutputs*nBasisFunctions];
  double M[2*nFlatOutputs*(1+nFlatDerivs) - 2][nFlatOutputs*nBasisFunctions];
  for (int index = 0; index < nBasisFunctions; ++index) {
    int row = 0;

#   ifdef UNUSED		// initial position always satisfied
    /* Initial position in x (Northing) */
    M[row][index] = basis(index, 0, 0);	
    M[row][index+nBasisFunctions] = 0;
    zbar[row++] = initial.x - initial.x;

    /* Initial position in y (Easting) */
    M[row][index] = 0;
    M[row][index+nBasisFunctions] = basis(index, 0, 0);
    zbar[row++] = initial.y - initial.y;
#   endif

    /* Initial angle and velocity */
    M[row][index] = basis(index, 1, 0);
    M[row][index+nBasisFunctions] = 0;
    zbar[row++] = initial.v * cos(initial.theta);

    M[row][index] = 0;
    M[row][index+nBasisFunctions] = basis(index, 1, 0);
    zbar[row++] = initial.v * sin(initial.theta);

    /* Final position in x (Northing) */
    M[row][index] = basis(index, 0, 1.0);
    M[row][index+nBasisFunctions] = 0;
    zbar[row++] = final.x - initial.x;

    /* Final position in y (Easting) */
    M[row][index] = 0;
    M[row][index+nBasisFunctions] = basis(index, 0, 1.0); 
    zbar[row++] = final.y - initial.y;

    /* Final angle and velocity */
    M[row][index] = basis(index, 1, T);
    M[row][index+nBasisFunctions] = 0;
    zbar[row++] = final.v * cos(final.theta);

    M[row][index] = 0;
    M[row][index+nBasisFunctions] = basis(index, 1, 1.0);
    zbar[row++] = final.v * sin(final.theta);
  }

  /* Declare the variables that we need for lapack */
  char trans = 'N';		// solve for underdetermined system
  int nrows = 2*nFlatOutputs*(1+nFlatDerivs) - 2, lda = nrows;
  int ncols = nFlatOutputs*nBasisFunctions, ldb = max(nrows,ncols);
  int nrhs = 1;
  int lwork = 2*nrows*ncols;
  double A[ncols*nrows];
  double work[2*nrows*ncols];
  int info;

  /* Fill in the matrices with the information */
  for (int i = 0; i < nrows; ++i) {
    for (int j = 0; j < ncols; ++j) {
      A[i + j*nrows] = M[i][j];
    }
    alpha[i] = zbar[i];
  }

  dgels_(&trans, &nrows, &ncols, &nrhs, A, &lda, 
	 alpha, &ldb, work, &lwork, &info);
  if (verbose >= 5) {
    cerr << "dgels: info = " << info << endl;
  }

  /* Check the answer to see if it is OK */
  double zchk[2*nFlatOutputs*(1+nFlatDerivs) - 2];
  for (int i = 0; i < nrows; ++i) {
    zchk[i] = 0;
    for (int j = 0; j < ncols; ++j) {
      zchk[i] += M[i][j] * alpha[j];
    }
  }

  // Print the problems we are trying to solve
  if (verbose >= 6) {
    for (unsigned int row = 0; row < 2*nFlatOutputs*(1+nFlatDerivs) - 2; ++row) {
      cout << zbar[row] << "= M[" << row << ",:] = [";
      for (unsigned int col = 0; col < nFlatOutputs*nBasisFunctions; ++col) {
	cout << M[row][col] << " ";
      }
      cout << "] --- chk: " << zchk[row] << endl;
    }

    for (int row = 0; row < 2*nBasisFunctions; ++row) {
      cout << "alpha[" << row << "] = " << alpha[row] << endl;
    }
  }

  /* Create the CTraj given the flat trajectory (1 meter spacing) */
  /* Make sure to get the very last point */
  for (double t = 0; t < T + T/(2*nTrajPoints); t += T/nTrajPoints) {
    double N = initial.x, Nd = 0, Ndd = 0;
    double E = initial.y, Ed = 0, Edd = 0;
    
    for (int index = 0; index < nBasisFunctions; ++index) {
      N += alpha[index] * basis(index, 0, t/T);
      E += alpha[index+nBasisFunctions] * basis(index, 0, t/T);

      Nd += alpha[index] * basis(index, 1, t/T);
      Ed += alpha[index+nBasisFunctions] * basis(index, 1, t/T);

      Ndd += alpha[index] * basis(index, 2, t/T);
      Edd += alpha[index+nBasisFunctions] * basis(index, 2, t/T);
    }

    /* Add the point to the trajectory */
    ptraj->addPoint(N, Nd, Ndd, E, Ed, Edd);
    ++npoints;
  }
# ifdef DEBUG
  cout << "Added " << npoints << " points" << endl;
  cout << "ptraj has " << ptraj->getNumPoints() << " points" << endl;
# endif

  return 0;
}

