/*
 * \file RddfPlanner.cc 
 * \brief simple planner using RDDF centerline
 *
 * \author Richard M. Murray
 * \brief 26 Nov 06
 *
 * This planner reads in an RDDF across skynet and sends out a
 * trajectory corresponding to the centerline of the RDDF corridor.
 *
 * This module was modified from rddfPathGen and it still has some
 * relics of that code.  
 *
 */

#include <unistd.h>
#include <pthread.h>
#include <sstream>
#include <string>
using namespace std;

#include "alice/AliceConstants.h"
#include "trajutils/PathLib.hh"
#include "trajutils/CPath.hh"
#include "dgcutils/RDDF.hh"
#include "trajutils/TrajTalker.hh"
#include "interfaces/sn_types.h"
#include "trajfollower/trajF_status_struct.hh"

#include "RddfPlanner.hh"
#include "flattraj.hh"

// Pseudocon interface for sending gear changes to trajfollower
#include "pseudocon/interface_superCon_trajF.hh"
using namespace sc_interface;

#include "cmdline.h"
extern gengetopt_args_info cmdline;

/* Functions defined later in this file */
int findClosestPoint(RDDFVector &, RDDFData &);
int create_traj(CTraj *, RDDFVector &, int, int, vector<double>&, int);

#ifdef C2
/* 
 * Include the header file for the refinement stage of the planner,
 * required in order to use Dima's make traj C2 function (used to
 * convert the trajs output by RDDFPathGen to C2 just before they are
 * output from RDDFPathGen
 */
#include "RefinementStage.h"
#endif

RDDF& rddf = *(new RDDF);
double localNorthing = 0, localEasting = 0;

/*
 * Processing options - these used to be set on the command line, but
 * for the most part we just hardware them now.
 */

const int NOSKYNET = 0;			// turn off messaging
const int WRITE_COMPLETE_DENSE = 0;	// don't know
const int WRITE_COMPLETE_SPARSE = 1;	// don't know
const int WRITE_EVERY_TRAJ = 0;		// don't know
const int WAIT_STATE = true;		// wait for state data
const int NOMM = 0;			// turn off sending traj's to gui
const int C2 = 2;			// use feasiblize fnction
const int SEND_TRAJ = 1;		// send the trajectory over skynet

RddfPlanner::RddfPlanner(int skynetKey) :
  CSkynetContainer(SNRddfPathGen, skynetKey), CRDDFTalker(true),
  paramsTalker(skynetKey, SNocpParams, MODdynamicplanner)
{
}

RddfPlanner::~RddfPlanner()
{
  // Do we need to destruct anything??
  if (cmdline.verbose_arg >= 9) 
    cerr << "RddfPlanner destructor has been called." << endl;
}

/*!
 * this is the main function which is run.  execution should be trapped
 * somewhere in this function during operation.
 */
void RddfPlanner::ActiveLoop(int verbose)
{
  if (verbose >= 2)
    printf("[%s:%d] Entering RddfPlanner::ActiveLoop()\n", __FILE__, __LINE__);

  // Set up network connection if we are running using skynet
  int skynetguiSocket = m_skynet.get_send_sock(SNtraj);
  int trajSocket = m_skynet.get_send_sock(SNRDDFtraj);

  // generate blank trajectory to be filled in by planner
  CTraj* ptraj;
  ptraj = new CTraj(3);  // order 3 (by default)

#ifdef C2
  // Refinement stage for planner; required when using Dima's C2 function
  CRefinementStage refstage;
#endif

  /* Get an RDDF that we can use to start planng */
  if (cmdline.rddf_given) {
    rddf.loadFile(cmdline.rddf_arg);

  } else {
      /* Get the RDDF via RDDFTalker */
    cerr << "Waiting for RDDF" << endl;
    WaitForRDDF(); UpdateRDDF();
    rddf = m_newRddf;
  }

  if (verbose >= 2) {
    cout << "rddfPlanner: received RDDF with " << rddf.getNumTargetPoints()
	 << " points" << endl;
  }
  if (verbose >= 4) rddf.print();

  /* 
   * See if we should convert to local coordinates 
   */
  if (cmdline.use_local_flag) {
    localNorthing = rddf.getWaypointNorthing(0);
    localEasting = rddf.getWaypointEasting(0);
  }

  // Main execution loop
  while (true) {
    /* 
     * Get the parameters from tplanner
     *
     * If tplanner is sending commands, this is the point where we
     * will bring them into our local variable space.
     */
    if (cmdline.use_endpoints_flag || cmdline.use_final_flag) {
      if (verbose) cerr << "waiting for params" << endl;

      int status = paramsTalker.receive(&receivedParams);
      if (!status) {
	cerr << "error receiving parameters" << endl;
	continue;

      } else {
	params = receivedParams;

	/* Get an updated RDDF is there is one */
	UpdateRDDF();
	rddf = m_newRddf;

	if (verbose >= 5) rddf.print();
      }
    }

    /*
     * Check to see if the trajectory is in forward or reverse.  If we
     * switch directions, then send a command to trajfollower via the
     * superCon interface to switch directions
     */
    enum direction { Unknown=0, Forward=1, Reverse=-1 } ;
    static enum direction current_dir = Unknown;
    enum direction command_dir = current_dir;

    /* Look for parameters from OCPtSpecs to decide on direction */
    command_dir = (params.mode == md_REV) ? Reverse : Forward;

    /* Decide if we need to change gears */
    //if (command_dir != current_dir) {
    if (1) {
      if (command_dir != current_dir && verbose >= 2) {
	cout << "Change dir: current = " << current_dir
	     << ", command = " << command_dir << endl;
      }
      /* Send a superCon command to trajfollower asking to change gears */
      int tfsocket = m_skynet.get_send_sock(SNsuperconTrajfCmd);
      struct superConTrajFcmd scCmd;
      scCmd.commandType = (command_dir == Forward) ? tf_forwards : tf_reverse;

      // Send the command to trajfollower (via superCon message)
      m_skynet.send_msg(tfsocket, &scCmd, sizeof(scCmd),0);
	
      // Wait for a bit, just to make sure messages stay in order
      // sleep(1);

      // Mark our current gear as the commanded gear
      current_dir = command_dir;
    }

    /*
     * Trajectory setup
     *
     * At this point we set up the parameters that will be used to
     * plan the path.  The corridor is given to us in the rddf object
     * and, if available, the initial and final conditions are in the
     * params object.
     *
     */
      
    // extract the info from waypoints and store in corridor
    int N = rddf.getNumTargetPoints();   // number of points
    RDDFVector& waypoints = *(new RDDFVector);
    waypoints = rddf.getTargetPoints();

    if (N < 2) {
      cerr << "Only received " << N << " waypoints; ignorning" << endl;
      continue;
    }

    /* 
     * Convert to local coordinates.  The variables localNorthing and
     * localEasting are set only if --use-local is specified,
     * otherwise they are zero.
     */
    for (int i = 0; i < N; ++i) {
      waypoints[i].Northing -= localNorthing;
      waypoints[i].Easting -= localEasting;
    }

    int first_waypoint = 0;
    int last_waypoint = N-1;
    struct TerminalCondition initialCondition, finalCondition;

    /* 
     * Start by setting the initial and final conditions to waypoints 
     * This is the default action if we aren't receiving endpoints
     */
    initialCondition.x = waypoints[0].Northing;
    initialCondition.y = waypoints[0].Easting;
    initialCondition.v = waypoints[0].maxSpeed;
    initialCondition.theta = 
      atan2(waypoints[1].Easting  - waypoints[0].Easting,
	    waypoints[1].Northing  - waypoints[0].Northing);

    finalCondition.x = waypoints[N-1].Northing;
    finalCondition.y = waypoints[N-1].Easting;
    finalCondition.v = waypoints[N-1].maxSpeed;
    finalCondition.theta = 
      atan2(waypoints[N-1].Easting  - waypoints[N-2].Easting,
	    waypoints[N-1].Northing  - waypoints[N-2].Northing);

    /* See if we should replace the end points with those given in OCPspecs */
    if (cmdline.use_endpoints_flag || cmdline.use_final_flag) {
      double upper[COND_NUMBER];	// storage for initial/final condition
      double lower[COND_NUMBER];
      RDDFData initialPoint, finalPoint;

      /* Get the initial condition */
      for (int i = 0; i < COND_NUMBER; ++ i)
	upper[i] = params.initialConditionUB[i];

      if (verbose >= 5) {
	cerr << "OcpTSpecs initial: N = " << upper[NORTHING_IDX_C]
	     << ", E = " << upper[EASTING_IDX_C] 
	     << ", H = " << upper[HEADING_IDX_C] << endl;
      }

      if (cmdline.use_endpoints_flag || N == 2) {
	initialCondition.x = initialPoint.Northing = upper[NORTHING_IDX_C];
	initialCondition.y = initialPoint.Easting = upper[EASTING_IDX_C];
	initialCondition.theta = upper[HEADING_IDX_C];
	initialCondition.v = upper[VELOCITY_IDX_C];
	if (initialCondition.v == 0) initialCondition.v = 0.01;

	if (verbose >= 5) {
	  cout << "Setting initial to N = " << initialPoint.Northing 
	       << ", E = " << initialPoint.Easting << endl;
	}
      }

      /* Get the final condition */
      for (int i = 0; i < COND_NUMBER; ++ i) {
	lower[i] = params.finalConditionLB[i];
	upper[i] = params.finalConditionUB[i];
      }

      if (verbose >= 5) {
	cerr << "OcpTSpecs final: N = " << upper[NORTHING_IDX_C]
	     << ", E = " << upper[EASTING_IDX_C] 
	     << ", H = " << lower[HEADING_IDX_C]
	     << " to " << upper[HEADING_IDX_C] << endl;
      }

      /* Set the final condition assuming upper == lower */
      finalCondition.x = finalPoint.Northing = upper[NORTHING_IDX_C];
      finalCondition.y = finalPoint.Easting = upper[EASTING_IDX_C];

      /* Set the final velocity to be the upper bound */
      finalCondition.v = finalPoint.maxSpeed = upper[VELOCITY_IDX_C];
      if (finalCondition.v == 0) finalCondition.v = 0.01;

      /* Figure out what angle we should use */
      if (lower[HEADING_IDX_C] == upper[HEADING_IDX_C]) {
	/* Use the heading we were sent */
	finalCondition.theta = upper[HEADING_IDX_C];
	if (verbose >= 5) {
	  cerr << "RddfPlanner: recv'd specific heading for final; theta = " 
	       << finalCondition.theta << endl;
	}

      } else {
	/* We weren't sent a heading, so use the heading of the corridor */
	int wp = findClosestPoint(waypoints, finalPoint);
	if (wp < 0) {
	  /* Something is wrong; let's try to do something sensible */
	  cerr << "RddfPlanner: couldn't find waypoint near final" << endl;

	  finalCondition.theta = 
	    (upper[HEADING_IDX_C] + lower[HEADING_IDX_C])/2;

	} else if (wp == 0) {
	  /* Strange condition; final point is near first waypoint */
	  finalCondition.theta = 
	    atan2(waypoints[wp+1].Easting  - waypoints[wp].Easting,
		  waypoints[wp+1].Northing  - waypoints[wp].Northing);

	} else {
	  /* Set the heading based on the corridor */
	  finalCondition.theta = 
	    atan2(waypoints[wp].Easting  - waypoints[wp-1].Easting,
		  waypoints[wp].Northing  - waypoints[wp-1].Northing);
	}

	/* Make sure theta is in the proper range */
	while (finalCondition.theta > M_PI) finalCondition.theta -= 2*M_PI;
	while (finalCondition.theta < -M_PI) finalCondition.theta += 2*M_PI;
      }

      if (verbose >= 5) {
	cout << "RddfPlanner: Setting final (" << N << ") to N = " 
	     << finalPoint.Northing << ", E = " << finalPoint.Easting 
	     << " at " << finalPoint.maxSpeed << " m/s" << endl;
      }

      /* 
       * If we only have two points in the RDDF, just plan between
       * beginning and end.  This is the mode that is used for
       * U-turns.  Otherwise, find the first and last waypoint
       * assuming we are moving in the direction of the RDDF.
       *
       * In the case that N == 2, we already have everything
       * initialized proplery, so we can just skip looking for
       * waypoints.
       */

      if (N != 2) {
	/* Find the initial and final segments of the relevant corridor */
	if ((first_waypoint = findClosestPoint(waypoints, initialPoint)) < 0) 
	  first_waypoint=0;

	if ((last_waypoint = findClosestPoint(waypoints, finalPoint)) < 0) 
	  last_waypoint=N;

	/* Make sure we get something sensible */
	if (first_waypoint > last_waypoint) {
	  /* We have past the point we should have cross => don't replan */
	  cerr << "first_waypoint > last_waypoint" << endl;
	  continue;
	}

	/* Make sure that we don't have first_waypoint == last_waypoint */
	if (first_waypoint == last_waypoint) {
	  /* Figure out which direction I can expand */
	  if (first_waypoint > 0) first_waypoint--;	// plan from prev
	  else if (last_waypoint < N) last_waypoint++;	// plan to next
	  else cerr << "RDDF only has one point" << endl;
	}

	/* Reset the velocity of the input point based on corridor */
	initialCondition.v = waypoints[first_waypoint].maxSpeed;
	if (initialCondition.v < 0.01) initialCondition.v = 0.01;
      }

      /* Replace the relevant waypoints with the initial and final points */
#     warning Code may be out of date with respect to flat code
      if (N == 2 || cmdline.use_endpoints_flag) {
	waypoints[first_waypoint].Northing = initialPoint.Northing;
	waypoints[first_waypoint].Easting = initialPoint.Easting;
      }

      /* Allow final point to be set independent of initial */
      if (N == 2 || cmdline.use_endpoints_flag || cmdline.use_final_flag) {
	waypoints[last_waypoint].Northing = finalPoint.Northing;
	waypoints[last_waypoint].Easting = finalPoint.Easting;
	if (!cmdline.ignore_end_velocity_flag) {
	  waypoints[last_waypoint].maxSpeed = finalPoint.maxSpeed;
	}
      }
    }

    /*
     * At this point in the code, first_waypoint should be the
     * waypoint that is nearest our starting point and last_waypoint
     * is nearest our endpoint.  If the --use-endpoints flag was not
     * specified or we only had two waypoints, then first_waypoint = 0
     * and last_waypoint = N-1.
     */

    if (verbose >= 5) {
      cout << "Planning path from waypoint " << first_waypoint
	   << " to waypoint " << last_waypoint << endl;
    }


    /* Generate the trajectory */
    if (cmdline.use_flat_flag) {
      if (create_flat_traj(ptraj, waypoints, first_waypoint, last_waypoint, 
			   initialCondition, finalCondition, verbose) != 0) {
	cerr << "error creating flat trajectory" << endl;
      }
    } else {
      /* Start planning from the initial point in the path */
      vector<double> location(2);
      location[0] = initialCondition.x;
      location[1] = initialCondition.y;

      /* Use the old trajectory generation route */
      create_traj(ptraj, waypoints, first_waypoint, last_waypoint, location,
		  verbose);
    }

    if(SEND_TRAJ && !NOSKYNET) {
      if (verbose >= 3) {
	cout << "----" << endl;
	ptraj->print();
	cout << "----" << endl;
      }

      SendTraj(trajSocket, ptraj);

      // Also send to skynetgui
      if (!NOMM) SendTraj(skynetguiSocket, ptraj);
    }
      
    if (!cmdline.use_endpoints_flag && !cmdline.use_final_flag) {
      /* Not using endpoints, so wait until we get a new RDDF */
      cerr << "Waiting for RDDF" << endl;
      WaitForRDDF(); UpdateRDDF();
      rddf = m_newRddf;
    }
    
    delete &waypoints;
  }
  delete ptraj;
}

/*
 * Utility functions
 * 
 *   * findClosestPoint - find the closest point in an RDDF
 *
 */

int findClosestPoint(RDDFVector &path, RDDFData &point)
{
  int closestIndex = -1;
  double distance, closestDistance;

  /* Go through the points and look for the closest one */
  for (unsigned int i = 0; i < path.size(); ++i) {
    /* Compute the distance between points (squared) */
    distance = 
      pow(point.Northing - path[i].Northing, 2.0) + 
      pow(point.Easting - path[i].Easting, 2.0);

    /* See if this is closer than our closest point so far */
    if (closestIndex == -1 || distance < closestDistance) {
      closestIndex = i;
      closestDistance = distance;
    }
  }

  /* Return the index to the closest point */
  return closestIndex;
}

/* Create a trajectory (old style) */
int create_traj(CTraj *ptraj, RDDFVector &waypoints, 
		int first_waypoint, int last_waypoint, 
		vector<double>& location, int verbose) 
{
    // store data in corridor 
    corridorstruct corridor_whole;
    corridor_whole.numPoints = last_waypoint - first_waypoint + 1;
    for(int i = first_waypoint; i <= last_waypoint; ++i) {
      // shouldn't get any segfaults here, but who knows...
      corridor_whole.e.push_back(waypoints[i].Easting);
      corridor_whole.n.push_back(waypoints[i].Northing);

      //shrink the corridor width to account for vehicle width, tracking error
      corridor_whole.width.push_back(waypoints[i].radius-RDDF_VEHWIDTH_EFF);
      corridor_whole.speedLimit.push_back(waypoints[i].maxSpeed);
      
      if (waypoints[i].radius-RDDF_VEHWIDTH_EFF < .01)
	cerr << "CORRIDOR WIDTH IS SMALLER THAN RDDF_VEHWIDTH_EFF!" << endl;
    }
    if (verbose >= 5) {
      cout << "done (generated " << last_waypoint - first_waypoint + 1 
	   << " points)." << endl;
    }
	  
    // generate and store whole path (sparse path, so max of 4 path points
    // per corridor point)
    if (verbose >= 5) {
      cout << "Generating complete (sparse) path... "; cout.flush();
    }
    pathstruct& path_whole_sparse = *(new pathstruct);
    path_whole_sparse = Path_From_Corridor(corridor_whole);

    // we haven't went anywhere yet, so we're still at the start of the path
    path_whole_sparse.currentPoint = 0; 

#ifdef C2
    /* create an 'rddf' which contains the path */
    /* This is legacy herman code; may not be quite right */
    RDDF& path_whole_sparse_rddf = *(new RDDF((char *) NULL));
    RDDFData temp_point;
    if (verbose >= 5) {
      cout << (int)path_whole_sparse.numPoints << endl;
    }

    for (int i = 0; i < (int)path_whole_sparse.numPoints; i++)
      {
	// Construct the RDDF segment
	temp_point.number = i;
	temp_point.Northing = path_whole_sparse.n[i];
	temp_point.Easting = path_whole_sparse.e[i];
	temp_point.radius = path_whole_sparse.corWidth[i];
	temp_point.maxSpeed = waypoints[i].maxSpeed;

	// Add the segment to the path
	path_whole_sparse_rddf.addDataPoint(temp_point);
      }

    if (verbose >= 5) {
      cout << "done (sparse path has " << path_whole_sparse.numPoints
	   << " points, sparse path \'rddf\' has " 
	   << path_whole_sparse_rddf.getNumTargetPoints() << " points)."
	   << endl;

      cout << "-----\n";
      path_whole_sparse_rddf.print();
      cout << "-----\n";
    }
#endif

    pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location,
						   CHOPBEHIND, CHOPAHEAD, -1);
    StorePath(*ptraj, path_chopped_dense);

#ifdef C2
    if (C2 == 1) {
      // Use Dima's function to make the output traj C2
      // This also sets the speed 
      refstage.MakeTrajC2(ptraj);
    } else 
#endif
      if (C2 == 2)
	ptraj->feasiblize(RDDF_MAXLATERAL, RDDF_MAXACCEL, RDDF_MAXDECEL, 
			  cmdline.curvature_averaging_distance_arg);

    // Delete data structures that we are done with
    delete &path_whole_sparse;
#ifdef C2
    delete &path_whole_sparse_rddf;
#endif

    return 0;
}
