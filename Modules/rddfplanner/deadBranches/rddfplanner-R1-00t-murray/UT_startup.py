#! /usr/bin/python -i
#
# \file UT_startup.py 
# \brief Short script to start up programs needed for unit tests
#
# \author Richard Murray
# \date 19 May 2007
#
# This scripts starts up the simulator, trajfollower and gcdrive to
# allow testing of rddfplanner.
#

import os;
import sys;

# Load the library that starts up modules
sys.path.append('../../etc/system-tests')
import sysTestLaunch

# Specify the situation we want to run
scenario = 'UT_startup'
rddf = 'stluke_sinewave.rddf'
rndf = ''
mdf = ''
obsFile = 'noObs.log'
sceneFunc = ''

# Make sure we are using bash
os.putenv('SHELL', '/bin/bash')

# Define the applications that we want to run
apps = [ \
 ('asim',  '--no-pause --rddf=%(rddf)s --gcdrive' % locals(), 1, 'localhost'), \
 ('rddfplanner',  '--verbose=2', 1, 'localhost'), \
 ('gcfollower',  '--use-new', 1, 'localhost'), \
 ('gcdrive',  '--simulate', 1, 'localhost'), \
 ('planviewer',  '', 5, 'localhost'), \
]

# Launch everything
sysTestLaunch.runApps(apps, scenario, obsFile, rndf, mdf, sceneFunc, False)
