/*!
 * \file gpscap.cc
 * \brief Program to capture points as you drive and create an RDDF
 *
 * \author Unknown (currently owned by Richard Murray)
 * \date 2004?
 *
 * This is a simple program to capture GPS points as you drive in
 * Alice and generate a simple RDDF.
 */

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fstream>
#include <pthread.h>

#include "gpscap.hh"

char *testlog  = new char[100];
char *pathlog  = new char[100];

using namespace std;


gpsCap::gpsCap(int skynet_key) 	: CSkynetContainer(SNastate, skynet_key) 
{
  cout<<"before DGCgettime"<<endl;	
	DGCgettime(starttime);
	cout<<"after DGCgettime"<<endl;
		
		char gpsFileName[256];
		time_t t = time(NULL);
		tm *local;
		local = localtime(&t);

  sprintf(gpsFileName, "gps_%04d%02d%02d_%02d%02d%02d.rddf", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

  m_outputGPS.open(gpsFileName);
		
 	m_outputGPS << setprecision(20);

}

void gpsCap::Active() {

  char cmd[99];
  GisCoordLatLon latlon;
  GisCoordUTM utm;
  int index = 1;
  utm.zone = 11;
  utm.letter = 'S';

  cout << "Now outputting direct to RDDF format!" << endl;
  cout << endl << endl << "This function now assumes a default speed of 10mph and width of 15 feet." << endl;
  cout << "You may change it manually (in emacs, meta-shift-5 is find and replace)" << endl << endl << endl;

  cout << "Enter c to capture waypoint, q for quit." << endl;
  scanf("%99s",cmd);

  while(cmd[0] != 'q' ) 
    {
      UpdateState();

#ifdef RAW_GPS
      /* Use the raw GPS from the state data, rather than the state estimate */
#     warning Using raw GPS
      utm.n = m_state.GPS_Northing;
      utm.e = m_state.GPS_Easting;
#else
      /* Use state estimate */
      utm.n = m_state.utmNorthing;
      utm.e = m_state.utmEasting;
#endif
		
      if(!gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL)) {
	cerr << "Coordinate transform failed at (E, N) = (" << utm.e << ", " << utm.n << ")." 
	     << endl; 
	exit(1);
      }

      m_outputGPS << index << RDDF_DELIMITER << latlon.latitude <<
		  RDDF_DELIMITER << latlon.longitude << RDDF_DELIMITER 
		  <<"15"<< RDDF_DELIMITER <<"10"<<endl;
      index++;

      printf("Enter c to capture waypoint, q for quit \n");
      scanf("%s",cmd);

    }
}


int main(int argc, char **argv) {

  int sn_key = 0;

  char* pSkynetkey = getenv("SKYNET_KEY");
  cout<<"got skynet key"<<endl;
  if ( pSkynetkey == NULL)
    {
      cerr << "SKYNET_KEY environment variable isn't set." << endl;
    } else
      {
	sn_key = atoi(pSkynetkey);
      }
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  gpsCap ast(sn_key);
  ast.Active();

  return 0;
}


