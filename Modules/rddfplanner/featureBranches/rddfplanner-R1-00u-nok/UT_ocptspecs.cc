/*!
 * \file UT_ocptspecs.cc
 * \brief Unit test for making sure we can read OCPtSpecs input
 *
 * \author Richard Murray
 * \date 26 April 2007
 *
 * This unit test sends out information in the same format as tplanner
 * uses to send information to dplanner (using OCPtSpecs).  This
 * allows testing of the rddfplanner OCPtSpecs interfaces.  To run
 * this unit test, you need to run rddfplanner as follows:
 *
 *   rddfplanner --use-endpoints --ignore-end-velocity
 *
 */

#include <iostream.h>
#include "interfaces/TpDpInterface.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/RDDFTalker.hh"
#include "dgcutils/RDDF.hh"
#include "UT_cmdline.h"
#include <math.h>

class CRDDFTalkerTestSend : public CRDDFTalker
{
public:
  CRDDFTalkerTestSend(int sn_key, RDDF &rddf)
    : CSkynetContainer(MODrddftalkertestsend, sn_key)
  {
    RDDFVector waypoints = rddf.getTargetPoints();

    cout << "about to get_send_sock...";
    int rddfSocket = m_skynet.get_send_sock(SNrddf);
    cout << " get_send_sock returned" << endl;
		
    cout << "about to send rddf with "
	 << rddf.getNumTargetPoints() << " points" << endl;
    SendRDDF(rddfSocket, &rddf);
    cout << " sent an rddf!" << endl;
    flush(cout);
  }
};

int main(int argc, char** argv)
{

  // Parse command line options
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  /* Make sure that we got an input */
  if (cmdline.inputs_num != 1) {
    fprintf(stderr, "Usage: %s [-S key] rddf-file\n", argv[0]);
    exit(1);
  }

  // Figure out the skynet key
  int skynet_key = skynet_findkey(argc, argv);

  // Load the RDDF file that we will use
  RDDF rddf(cmdline.inputs[0]);	// read from an RDDF file
  rddf.print();		// print out the RDDF file
  RDDFVector waypoints = rddf.getTargetPoints();

  if (cmdline.use_local_flag) {
    /* Subtract off the initial waypoint from all points */
    for (int i = waypoints.size()-1; i >= 0; --i) {
      waypoints[i].Northing -= waypoints[0].Northing;
      waypoints[i].Easting -= waypoints[0].Easting;
    }
  }

  /* Construct a new corridor so we can subtract off points */
  RDDF corridor;
  for (int i = 1; i < waypoints.size(); ++i) 
    corridor.addDataPoint(waypoints[i]);

  /* Now send out the RDDF as a corridor */
  CRDDFTalkerTestSend test(skynet_key, corridor);

  // Set up the parameters based on the waypoint file
  OCPparams myparams;

  // Initial point = first waypoint, with same upper and lower bounds
  myparams.initialConditionLB[NORTHING_IDX_C] = waypoints[0].Northing;
  myparams.initialConditionLB[EASTING_IDX_C] = waypoints[0].Easting;
  myparams.initialConditionLB[VELOCITY_IDX_C] = waypoints[0].maxSpeed;

  // Now figure out the angle that we should use
  myparams.initialConditionLB[HEADING_IDX_C] = 
    atan2(waypoints[1].Easting  - waypoints[0].Easting,
	  waypoints[1].Northing  - waypoints[0].Northing);

  myparams.initialConditionLB[ACCELERATION_IDX_C] = 0;
  myparams.initialConditionLB[STEERING_IDX_C] = 0;
  for (int i = 0; i < COND_NUMBER; ++i) 
    myparams.initialConditionUB[i] = myparams.initialConditionLB[i];

  // Final point = last waypoint, with same upper and lower bounds
  const int N = rddf.getNumTargetPoints()-1;
  myparams.finalConditionLB[NORTHING_IDX_C] = waypoints[N].Northing;
  myparams.finalConditionLB[EASTING_IDX_C] = waypoints[N].Easting;
  myparams.finalConditionLB[VELOCITY_IDX_C] = waypoints[N].maxSpeed;

  // Now figure out the angle that we should use
  myparams.finalConditionLB[HEADING_IDX_C] = 
    atan2(waypoints[N].Easting  - waypoints[N-1].Easting,
	  waypoints[N].Northing  - waypoints[N-1].Northing);

  myparams.finalConditionLB[ACCELERATION_IDX_C] = 0;
  myparams.finalConditionLB[STEERING_IDX_C] = 0;
  for (int i = 0; i < COND_NUMBER; ++i) 
    myparams.finalConditionUB[i] = myparams.finalConditionLB[i];

  myparams.parameters[VMIN_IDX_P] = 0.1;
  myparams.parameters[VMAX_IDX_P] = 26.8;
  myparams.parameters[AMIN_IDX_P] = -3.0;
  myparams.parameters[AMAX_IDX_P] = 0.981;
  myparams.parameters[PHIMIN_IDX_P] = -0.45 ;
  myparams.parameters[PHIMAX_IDX_P] = +0.45;
  myparams.parameters[PHIDMIN_IDX_P] = -1.31 ;
  myparams.parameters[PHIDMAX_IDX_P] = +1.31 ;

  // Let the user know what we are sending
  cout << "Sending initial = (N = " 
	<< myparams.initialConditionLB[NORTHING_IDX_C] << ", E = "
	<< myparams.initialConditionLB[EASTING_IDX_C] << "), "
	<< "final = (N = "
	<< myparams.finalConditionLB[NORTHING_IDX_C] << ", E = "
	<< myparams.finalConditionLB[EASTING_IDX_C] << ")" << endl; 

  SkynetTalker<OCPparams> paramsSender(skynet_key, SNocpParams,
				       MODdynamicplanner);
  bool result = paramsSender.send(&myparams);
  cout << " sent: " << result << endl;

  /* Send a stop trajectory when ready */
  cout << "Press enter to send stop trajectory" << endl;
  getc(stdin);

  // Initial point = last waypoint
  myparams.initialConditionLB[NORTHING_IDX_C] = waypoints[N].Northing;
  myparams.initialConditionLB[EASTING_IDX_C] = waypoints[N].Easting;
  myparams.initialConditionLB[VELOCITY_IDX_C] = waypoints[N].maxSpeed;

  // Now figure out the angle that we should use
  myparams.initialConditionLB[HEADING_IDX_C] = 
    atan2(waypoints[N].Easting  - waypoints[N-1].Easting,
	  waypoints[N].Northing  - waypoints[N-1].Northing);

  for (int i = 0; i < COND_NUMBER; ++i) 
    myparams.initialConditionUB[i] = myparams.initialConditionLB[i];

  // Final point = last waypoint, with same upper and lower bounds
  myparams.finalConditionLB[NORTHING_IDX_C] = waypoints[N].Northing;
  myparams.finalConditionLB[EASTING_IDX_C] = waypoints[N].Easting;
  myparams.finalConditionLB[VELOCITY_IDX_C] = 0;

  // Now figure out the angle that we should use
  myparams.finalConditionLB[HEADING_IDX_C] = 
    atan2(waypoints[N].Easting  - waypoints[N-1].Easting,
	  waypoints[N].Northing  - waypoints[N-1].Northing);

  for (int i = 0; i < COND_NUMBER; ++i) 
    myparams.finalConditionUB[i] = myparams.finalConditionLB[i];

  result = paramsSender.send(&myparams);
  cout << " sent: " << result << endl;
}
