/*!
 * \file interptraj.cc
 * \brief Old routine for generating trajectory by interpolation
 *
 * \author Richard M. Murray
 * \date 2006
 *
 * The 'create_interp_traj' function is used to generate an
 * interpolated trajectory along an RDDF.  This is one of the routines
 * from the old rddfpathgen and is now obsolete.
 */

#include "trajutils/PathLib.hh"
#include "trajutils/CPath.hh"
#include "interptraj.hh"

/* Create a trajectory (old style) */
int create_interp_traj(CTraj *ptraj, RDDFVector &waypoints, 
		int first_waypoint, int last_waypoint, 
		vector<double>& location, int verbose) 
{
    // store data in corridor 
    corridorstruct corridor_whole;
    corridor_whole.numPoints = last_waypoint - first_waypoint + 1;
    for(int i = first_waypoint; i <= last_waypoint; ++i) {
      // shouldn't get any segfaults here, but who knows...
      corridor_whole.e.push_back(waypoints[i].Easting);
      corridor_whole.n.push_back(waypoints[i].Northing);

      //shrink the corridor width to account for vehicle width, tracking error
      corridor_whole.width.push_back(waypoints[i].radius-RDDF_VEHWIDTH_EFF);
      corridor_whole.speedLimit.push_back(waypoints[i].maxSpeed);
      
      if (waypoints[i].radius-RDDF_VEHWIDTH_EFF < .01)
	cerr << "CORRIDOR WIDTH IS SMALLER THAN RDDF_VEHWIDTH_EFF!" << endl;
    }
    if (verbose >= 5) {
      cout << "done (generated " << last_waypoint - first_waypoint + 1 
	   << " points)." << endl;
    }
	  
    // generate and store whole path (sparse path, so max of 4 path points
    // per corridor point)
    if (verbose >= 5) {
      cout << "Generating complete (sparse) path... "; cout.flush();
    }
    pathstruct& path_whole_sparse = *(new pathstruct);
    path_whole_sparse = Path_From_Corridor(corridor_whole);

    // we haven't went anywhere yet, so we're still at the start of the path
    path_whole_sparse.currentPoint = 0; 
    pathstruct path_chopped_dense = DensifyAndChop(path_whole_sparse, location,
						   CHOPBEHIND, CHOPAHEAD, -1);
    StorePath(*ptraj, path_chopped_dense);

    ptraj->feasiblize(RDDF_MAXLATERAL, RDDF_MAXACCEL, RDDF_MAXDECEL, 5.0);

    // Delete data structures that we are done with
    delete &path_whole_sparse;
    return 0;
}
