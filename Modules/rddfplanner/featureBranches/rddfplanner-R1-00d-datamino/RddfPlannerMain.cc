/*!
 * \file RddfPlannerMain.cc
 *
 * \author Richard M. Murray
 * \date 27 January 2007
 *
 * This is the main program for rddfplanner.  It parses the command
 * line arguments, then starts up threads to read in RDDF files and
 * send out paths.
 */

#include <stdlib.h>
#include <iostream>

#include "RddfPlanner.hh"
#include "skynet/skynet.hh"

#include "cmdline.h"
gengetopt_args_info cmdline;

#ifdef SPARROWHAWK
/* SparrowHawk files, including displays */
#include "sparrowhawk/SparrowHawk.hh"
#include "maindisp.h"
#endif

int main(int argc, char** argv)
{
  /* Process command line options */
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  if (cmdline.config_given &&
      cmdline_parser_configfile(cmdline.config_arg, &cmdline, 0, 0, 1) != 0) {
    exit(1);
  }

  /* Retrieve the skynet key */
  int snkey = skynet_findkey(argc, argv);
  cerr << "Constructing skynet with KEY = " << snkey << endl;

  /* Create the planner object */
  RddfPlanner RddfPlannerObj(snkey);

  /* Create an OCPtSpecs object to take commands from tplanner */
  RddfPlannerObj.m_pOcpTSpecs = new OCPtSpecs(snkey);

#ifdef SPARROWHAWK
  /* Start up the sparrow display */
  CSparrowHawk &sh = SparrowHawk();
  sh.add_page(rddfPathGen_maindisp, "Main");
  sh.rebind("snkey", &intSkynetKey);
  sh.run();
#endif

  RddfPlannerObj.ActiveLoop();
  return 0;
}
