              Release Notes for "rddfplanner" module

Release R1-00i (Sun Mar 18 20:27:02 2007):
	This is the version of rddfplanner that was used for FT2.  Added
	--use-final flag, which uses the final condition but not the initial
	condition.  This is needed for getting the right behavior when
	moving onto an RDDF.  In a "zone" (RDDF with N=2) we still use
	initial and final if either --use-endpoints or --use-final is
	specified.

	This version can be considered stable, but should be replaced with
	dplanner or s1planner as quickly as possible.  Functionality is too
	limited for the future testing we are going to need to do.

Release R1-00h (Sat Mar 17 13:36:54 2007):
	Fixed the way endpoints are handled for corridors with only two
	waypoints (beginning and end).  These are currently used for U-turn
	segments, so we simply replace the RDDF that is sent with the start
	and end point.  Works correctly with tplanner and trajfollower.

Release R1-00g (Fri Mar 16 10:18:27 2007):
	This version of rddfplanner has several new features to allow it to
	be used as a temporary substitute for dplanner:

	* The --use-endpoints flag reads the endpoint information sent by
          tplanner to modify the trajectories to start and end at a
          specified location.  This is needed for stoplines, intersections,
          etc.   We use the terminal velocity for the endpoint, but ignore
          initial velocity.  You can turn off the use of terminal velocity
          using the --ignore-end-velocity flag.

	* The feasiblize function was not being called properly and was not
          computing accelerations properly.  This is now fixed, although it
          doesn't really change that much.  Note that this version of
          rddfplanner does not take into account curvature limits of the
          vehicle (the dgc/trunk version did, but needed lots of code that
          we don't want to import into YaM)

	* There is now a --verbose option that can be used to turn on
          different levels of status messages.  The number of messages
          printed depend on the level of verbosity.  Roughly, verbose = 1
          will generate RddfPlanner messages that tell you stuff is
          happening.  Setting verbose = 2 will generate more detailed
          messages and will also turn on messages in OCPtSpecs (at level
          verbose-1).

        Things that are still missing: we don't set the angle of the vehicle
        at the end of the path.  This may be needed for U-turns.

Release R1-00f (Wed Mar 14 17:02:10 2007):
	Fixed a small bug that put is in the opposite mode as asked (forward
	vs reverse) 

Release R1-00e (Wed Mar 14 16:53:11 2007):
	Updated makefile to fix some linking errors.

Release R1-00d (Sun Mar 11 21:45:22 2007):
	This version of rddfplanner has hooks for planning in reverse.  This
	consists of two main changes: (1) added an interface to tplanner
	through the ocpspecs library that can receive mode changes from
	tplanner and (2) added an interface to trajfollower that can command
	a change in direction via the superCon (pseudocon) interface.

	This code has not been tested yet, mainly because we don't yet have
	a way to command reverse (that I know of) and the latest release of
	trajfollower doesn't appear to have reverse capability yet.  The
	code has been tested when no reverse command is sent and works as
	before. 

	The code here can serve as a starting point for the equivalent
	capability in dplanner.  See lines 132-162 of RddfPlanner.cc.

Release R1-00c (Sat Mar  3 19:11:41 2007):
	Found and fixed a bug which was causing a seg fault.  This version
	should now work equivalently to the dgc/trunk version with option
	'feasibilize' (not that no path type is required for the yam
	version).

Release R1-00b (Thu Feb 1 2:09:03 2007): 
	Fixed the linking problems and changed the variable names so it
	compiles against R1-00m of interfaces.

Release R1-00a (Wed Jan 31 13:39:11 2007):
	This is the first release of the rddfplanner module, copied from
	trunk.  In order to get this to work without going insane, I used
	the "feasiblize" option rather than the 'c2' option (which brings in
	a bunch of code from Dima's planner and is a mess).  I aslo hard
	coded in some of the other options that nobody has usd in years.

	This version will not compile against R1-00a of trajutils due to
	problems with linking to the standard vector library.  See release
	notes for trajutils for a bit more detail.

Release R1-00 (Sat Jan 27 18:49:02 2007):
	Created.









