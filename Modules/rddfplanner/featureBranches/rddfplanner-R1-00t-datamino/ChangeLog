Tue May 29  3:04:02 2007	murray (murray)

	* version R1-00t
	BUGS:  
	New files: UT_startup.py
	FILES: RddfPlanner.cc(25350), UT_ocptspecs.cc(25350)
	fixed stop trajectory (cos -> sin)

Sat May 19 12:57:52 2007	murray (murray)

	* version R1-00s
	BUGS:  
	FILES: Makefile.yam(23985), UT_fwdbwd.cc(23985)
	updated UT_fwdbwd to be used from outside rddfplanner

	FILES: RddfPlannerMain.cc(23954)
	updated enpoint processing

Sat May 19  9:33:04 2007	murray (murray)

	* version R1-00r
	BUGS:  
	FILES: RddfPlanner.cc(23916)
	added findroute processing

	FILES: RddfPlannerMain.cc(23915)
	fixed processing for final flag

2007-05-18  Richard Murray  <murray@dhcp-209.cds.caltech.edu>

	* RddfPlannerMain.cc (main): updated flag processing - don't use
	endpoints if --use-final is given

Sat May 12 10:26:42 2007	Richard Murray (murray)

	* version R1-00o
	BUGS:  
	FILES: Makefile.yam(22818)
	removed ocpspecs dependence (not needed)

	FILES: RddfPlanner.cc(21925), RddfPlannerMain.cc(21925),
		UT_fwdbwd.cc(21925), flattraj.cc(21925)
	fix to cmdline option processing: --use-endpoints by default

	FILES: RddfPlanner.cc(22816)
	removed mode logic (not used) + gui traj (not used)

	FILES: flattraj.cc(22817)
	removed mode logic (not used)

2007-05-12  Richard Murray  <murray@kona.local>

	* RddfPlanner.cc (RddfPlanner::ActiveLoop): removed unused gui
	socket (planviewer listen to same skynet group as trajfollower)

	* flattraj.cc (create_flat_traj): removed extraneous dir argument in
	call to maneuver_profile_generate (left over from previous testing)

	* Makefile.yam: removed ocpspecs dependence

Wed May  2 10:45:31 2007	murray (murray)

	* version R1-00n
	BUGS:  
	New files: UT_rectangle.rddf UT_stopping.cc UT_straight.rddf
		UT_turnright.rddf interptraj.cc interptraj.hh
	FILES: Makefile.yam(21851)
	new unit tests

	FILES: Makefile.yam(21860), RddfPlanner.cc(21860),
		rddfplanner.ggo(21860)
	split off old traj gen to separate file

	FILES: RddfPlanner.cc(21622)
	typo in comment

	FILES: RddfPlanner.cc(21686)
	updated speed profile for stopped condition

	FILES: RddfPlanner.cc(21850), flattraj.cc(21849)
	converted to trajutils + fixed some indexing problems

	FILES: RddfPlanner.cc(21863), RddfPlannerMain.cc(21863),
		rddfplanner.ggo(21863)
	updated command line processing

	FILES: RddfPlanner.cc(21865), flattraj.cc(21865)
	moved velocity profile code to trajutils

	FILES: RddfPlanner.cc(21872)
	fixed bug in gear sending

	FILES: UT_fwdbwd.cc(21623)
	send multiple reverse commands for tplanner

	FILES: UT_fwdbwd.cc(21624, 21626)
	more resends of rev

	FILES: UT_fwdbwd.cc(21854), flattraj.cc(21854)
	temporarily reset accels to zero

	FILES: flattraj.cc(21853)
	updated initial velocity planning

	FILES: flattraj.cc(21855)
	new logic for starting from a stop

	FILES: flattraj.cc(21873)
	added proper Alice parameters

Sun Apr 29 13:43:22 2007	murray (murray)

	* version R1-00m
	BUGS:  
	New files: UT_fwdbwd.cc UT_longtwopoint.rddf UT_tpstop.rddf
	FILES: Makefile.yam(21519), RddfPlanner.cc(21519),
		RddfPlanner.hh(21519), RddfPlannerMain.cc(21519)
	OCPtSpecs free version

	FILES: Makefile.yam(21520)
	unit test for driving foward, then backward

	FILES: RddfPlanner.cc(21523)
	improved debugging + wait for params

	FILES: RddfPlanner.cc(21544)
	tweaked non-endpoint operations

	FILES: RddfPlanner.cc(21547)
	don't use initial condition unless use-endpoints is given

	FILES: RddfPlanner.cc(21548)
	fixe mispelling

	FILES: RddfPlanner.cc(21549)
	fixed initial condition initialization for N=2

	FILES: RddfPlanner.cc(21550, 21551)
	change how initial speed is handled

	FILES: RddfPlanner.cc(21553), UT_cmdline.ggo(21553),
		UT_ocptspecs.cc(21553), rddfplanner.ggo(21553)
	added ability to use local coordinates

	FILES: RddfPlanner.cc(21556), RddfPlanner.hh(21556)
	removed dependence on state

	FILES: UT_ocptspecs.cc(21524)
	send RDDF before params

	FILES: UT_ocptspecs.cc(21554)
	fixed initial condition

	FILES: UT_ocptspecs.cc(21555)
	dumb bug

	FILES: flattraj.cc(21521)
	added speed scaling

	FILES: flattraj.cc(21525)
	udpated acceleration computations

	FILES: flattraj.cc(21543)
	collected solving functions + add'l basis functions

	FILES: flattraj.cc(21545)
	added initial condition to make sure we plan from start of traj

	FILES: flattraj.hh(21558)
	file name fixes for doxygen

Sat Apr 28 19:05:51 2007	murray (murray)

	* version R1-00l
	BUGS:  
	FILES: Makefile.yam(21473)
	added links to /dgc/otg

Sat Apr 28 15:57:49 2007	murray (murray)

	* version R1-00k
	BUGS:  
	New files: UT_nearpi.rddf UT_tpscroll.cc
	FILES: Makefile.yam(21333), RddfPlanner.hh(21333),
		RddfPlannerMain.cc(21333)
	added new unit test for testing scrolling

	FILES: RddfPlanner.cc(21295), flattraj.cc(21295)
	updated theta for final condition

	FILES: RddfPlanner.cc(21307)
	updated theta - use corridor

	FILES: RddfPlanner.cc(21320), flattraj.cc(21320),
		flattraj.hh(21320)
	improvied error messages; new theta calcs

	FILES: RddfPlanner.cc(21344)
	tweaked error messages

	FILES: RddfPlanner.cc(21347)
	added more debugging

	FILES: RddfPlanner.cc(21360), RddfPlanner.hh(21360),
		RddfPlannerMain.cc(21360), flattraj.cc(21360)
	updated basis function calculations

	FILES: RddfPlanner.hh(21370), RddfPlannerMain.cc(21370)
	switched back to default ocpspecs

	FILES: flattraj.cc(21349)
	converted bo bezier basis functions

	FILES: flattraj.cc(21369)
	fixed bug in use of basis functions

	FILES: flattraj.cc(21371)
	tweaked gains

2007-04-28  Richard Murray  <murray@kona.local>

	* flattraj.cc: increased number of trajectory points to 10

	* RddfPlanner.cc (RddfPlanner::ActiveLoop): changed to use average
	heading for final condition

Sat Apr 28  7:30:44 2007	murray (murray)

	* version R1-00j
	BUGS:  
	New files: UT_cmdline.ggo UT_ocptspecs.cc UT_sinewave.rddf
		UT_twopoint.rddf flattraj.cc flattraj.hh
	FILES: Makefile.yam(21244), RddfPlanner.cc(21244),
		rddfplanner.ggo(21244)
	first pass at flatness base planner

	FILES: RddfPlanner.cc(21274)
	fixed intermediate point calculation + velocities

2007-04-26  Richard Murray  <murray@kona.local>

	* RddfPlanner.hh (RddfPlanner): converted to use TPlannerSpecs
	instead of OCPtSpecs

Sun Mar 18 20:26:57 2007	murray (murray)

	* version R1-00i
	BUGS: 
	FILES: RddfPlanner.cc(18566), rddfplanner.ggo(18566)
	added --use-final flag

2007-03-18  murray  <murray@gcfield.dgc.caltech.edu>

	* RddfPlanner.cc (ActiveLoop): Added --use-final flag, which uses
	the final condition but not the initial condition.  This is needed
	for getting the right behavior when moving onto an RDDF.  In a
	"zone" (RDDF with N=2) we still use initial and final if either
	--use-endpoints or --use-final is specified.

	Note: the command line switches could probably use better names at
	this point.

Sat Mar 17 13:36:40 2007	murray (murray)

	* version R1-00h
	BUGS: 
	FILES: RddfPlanner.cc(18107)
	put in logic for handling 2 pt RDDF that should work for reverse

	FILES: RddfPlanner.cc(18127)
	fixed bug in way the waypoints were set for N=2

Fri Mar 16 10:18:17 2007	murray (murray)

	* version R1-00g
	BUGS: 
	FILES: Makefile.yam(17720)
	updated dependencies for cmdline

	FILES: RddfPlanner.cc(17722), rddfplanner.ggo(17722)
	added --use-endpoints and --curvature-averaging-distance command
	line arguments

	FILES: RddfPlanner.cc(17767)
	updated C2 flag to call Traj::feasiblize

	FILES: RddfPlanner.cc(17846)
	new computation for endpoints, using nearest start and end of RDDF

	FILES: RddfPlanner.cc(17847)
	send direction continuously

	FILES: RddfPlanner.cc(17856)
	added info proint

	FILES: RddfPlanner.cc(17861), RddfPlanner.hh(17861)
	using OCPtSpecs for RDDF

	FILES: RddfPlanner.cc(17863)
	check for empty rddf from OCPtspecs

	FILES: RddfPlanner.cc(17874)
	added sleep if no RDDF received yet

	FILES: RddfPlanner.cc(17894), RddfPlanner.hh(17894)
	added fuctionality for initial and end velocity, with flags to tune
	behavior

	FILES: RddfPlanner.cc(17964), RddfPlanner.hh(17964)
	added verbose flag as argument to active loop

	FILES: RddfPlannerMain.cc(17965), rddfplanner.ggo(17965)
	changed versbose flag to allow level

	FILES: rddfplanner.ggo(17845)
	added verbose option

	FILES: rddfplanner.ggo(17893)
	added ignore-end-velocity flag

2007-03-15  murray  <murray@gclab.dgc.caltech.edu>

	* RddfPlanner.cc (ActiveLoop): changed to use OCPtSpecs to read RDDF.
	(ActiveLoop): updated processing of initial and final conditions,
	including using speed for final condition

	* RddfPlanner.cc (findClosestPoint): created new function to
	compute closest point to a set of RDDF waypoints
	(ActiveLoop): added new method for computing initial and final
	points.  This version starts the corridor based on the nearest
	start and end point in the RDDF.  Not yet tested...

	* rddfplanner.ggo: changed snkey to skynet-key (and -S).  Added
	--verbose flag (-v).

	* RddfPlanner.cc: set C2 flag to use the feasiblize function (was
	not getting called before)

2007-03-14  murray  <murray@gclab.dgc.caltech.edu>

	* rddfplanner.ggo: added --use-endpoints flag

	* RddfPlanner.cc (ActiveLoop): Removed references to
	path_whole_sparse_rddf, which were only needed for Dima's code
	(#ifdef C2)

Wed Mar 14 17:02:06 2007	Chris Schantz (cschantz)

	* version R1-00f
	BUGS: 
	FILES: RddfPlanner.cc(17636)
	Fixed a bug that put us in the opposide mode as asked (Forward vs
	Reverse)

Wed Mar 14 16:53:08 2007	Noel duToit (ndutoit)

	* version R1-00e
	BUGS: 
	FILES: Makefile.yam(17624)
	fixed a linking error in make file

Sun Mar 11 21:45:15 2007	murray (murray)

	* version R1-00d
	BUGS: 
	FILES: Makefile.yam(17139)
	removed interfaces library; not used

	FILES: Makefile.yam(17177), RddfPlanner.cc(17177),
		RddfPlanner.hh(17177), RddfPlannerMain.cc(17177)
	added interface to tplanner via ocpspecs

	FILES: RddfPlanner.cc(17140)
	logic for changing gears (belongs in trajfollower)

	FILES: RddfPlanner.cc(17143)
	updated gear change to use superCon interface

	FILES: RddfPlanner.cc(17151)
	updated to reflect new pseudocon interface

	FILES: RddfPlanner.cc(17152)
	updated comments

	FILES: RddfPlanner.cc(17153)
	took out RDDF vel check for direction and put in hook for tplanner
	mode

Sat Mar  3 19:11:36 2007	murray (murray)

	* version R1-00c
	BUGS: 
	FILES: Makefile.yam(16472), RddfPlanner.hh(16470)
	fixed up include paths to match modified interfaces

	FILES: RddfPlanner.cc(16471)
	fixed bug in which rddfs were showing zero length

Thu Feb  1  2:08:59 2007	Nok Wongpiromsarn (nok)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(13875), RddfPlanner.cc(13875)
	Fixed the linking problems and changed the variable names for the
	new VehicleState

Wed Jan 31 13:39:07 2007	murray (murray)

	* version R1-00a
	BUGS: 3100
	New files: RddfPlanner.cc RddfPlanner.hh RddfPlannerMain.cc
		maindisp.dd rddfplanner.ggo
	FILES: Makefile.yam(13327)
	initial pass at rddfplanner; compiles but not yet fully functional

	FILES: Makefile.yam(13603)
	first pass at rddfplanner; long way from working

	FILES: README(13741)
	upated for YaM

2007-01-31  Richard Murray  <murray@dhcp-195.cds.caltech.edu>

	* RddfPlannerMain.cc: new file to hold main program.  Uses gengetopt
	for flags.

	* RddfPlanner.cc: got rid of links to Dima's refinement stage; use
	feasiblize (sic) instead.
	
Sat Jan 27 18:49:02 2007	Nok Wongpiromsarn (nok)

	* version R1-00
	Created rddfplanner module.


















