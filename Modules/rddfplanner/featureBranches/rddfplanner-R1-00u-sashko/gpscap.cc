/*
 * \file gpscap.cc
 * \brief Program to capture points as you drive and create an RDDF
 *
 * \author Unknown (currently owned by Richard Murray)
 * \date 2004?
 *
 * This is a simple program to capture GPS points as you drive in
 * Alice and generate a simple RDDF.
 */

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fstream>
#include <pthread.h>
#include <sstream>

#include "gpscap.hh"

char *testlog  = new char[100];
char *pathlog  = new char[100];

using namespace std; 



gpsCap::gpsCap(int skynet_key) 	: CSkynetContainer(SNastate, skynet_key) 
{
  cout<<"before DGCgettime"<<endl;	
	DGCgettime(starttime);
	cout<<"after DGCgettime"<<endl;
		
		char gpsFileName[256];
		time_t t = time(NULL);
		tm *local;
		local = localtime(&t);

  sprintf(gpsFileName, "gps_%04d%02d%02d_%02d%02d%02d.rddf", 
	  local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	  local->tm_hour, local->tm_min, local->tm_sec);

  m_outputGPS.open(gpsFileName);
		
 	m_outputGPS << setprecision(20);

}

const int smax=128;
const int lmax=24;
const int wmax=128;

void gpsCap::Active() {

  GisCoordLatLon latlon;
  GisCoordUTM utm;
  utm.zone = 11;
  utm.letter = 'S';
  int s, l, w; //waypoint ID
  int inputstats=0; // whatever scanf returns
  char cmd[101]; // the command

  cout << "\nbefore declaration";
  float coords[smax][lmax][wmax][2];
  bool used[smax][lmax][wmax]; //if !0 is changed
  cout << "\nafter declarations";

  for(s=0; s<smax; s++)
    {
      for(l=0; l<lmax; l++)
	{
	  for(w=0; w<wmax; w++)
	    {
	      used[s][l][w]=0;
	    }
	}
    }

  int numsegs=0;
  int numlans[smax];
  int numwpts[smax][lmax];

  cout << "\nCreate or replace?: ";
  char replace;

  cin >> replace;

  cout << endl << endl << "This function assumes a default lane width of 15 feet." << endl;


  do{
    cout << "\ninput!!\n";
  
    inputstats=scanf("%d.%d.%d", &s, &l, &w);
                      
    if(inputstats==0)
      {
	inputstats=0;
	inputstats=scanf("%c %d.%d.%d", &cmd[0], &s, &l, &w);
      }
    
    if(!inputstats)
      inputstats=scanf("%c", &cmd[0]);

    if(!inputstats)
      {
	cout << "\nincorrect input format!\n";
      }

    if(inputstats && cmd[0]!='q')
      {

	UpdateState();

#ifdef RAW_GPS
	/* Use the raw GPS from the state data, rather than the state estimate */
#warning Using raw GPS
	utm.n = m_state.GPS_Northing;
	utm.e = m_state.GPS_Easting;
#else
	/* Use state estimate */
	utm.n = m_state.utmNorthing;
	utm.e = m_state.utmEasting;
#endif
		
	if(!gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL)) 
	  {
	    cerr << "Coordinate transform failed at (E, N) = (" << utm.e << ", " << utm.n << ")."
		 << endl; 
	    exit(1);
	  }

	/*
	  all of the input stuff------------------
	  ----------------------------------------
	  ----------------------------------------
	*/

	cout << "\nWaypoint selected: " << s << "." << l << "." << w;

	if(l>lmax)
	  {
	    cout << "incorrect lane number (too large)";
	    continue;
	  }
	if(s>smax)
	  {
	    cout << "incorrect segment number (too large)";
	    continue;
	  }
	if(w>wmax)
	  {
	    cout << "incorrect waypoint number (too large)";
	    continue;
	  }

	if(s>numsegs)
	  {
	    numsegs=s;
	  }
	if(l>numlans[s])
	  {
	    numlans[s]=l;
	  }
	if(w>numwpts[s][l])
	  {
	    numwpts[s][l]=w;
	  }

	printf("\nnumsegs = %d\nnumlans = %d\nnumwypts = %d", numsegs, numlans[s], numwpts[s][l]);

	coords[s][l][w][0]=latlon.latitude;
	coords[s][l][w][1]=latlon.longitude;
	used[s][l][w]=1;

	/*
	  end of the input stuff------------------
	  ----------------------------------------
	  ----------------------------------------
	*/



      } // end of if statement
  } while (cmd[0] != 'q'); // end of loop

  if(replace=='y')
    {
  string temp;
  string in;
  ifstream infile("in.txt");
	for(int i=1; i<=numsegs; i++)
	  {
	    for(int j=1; j<=numlans[i]; j++)
	      {
		for(int k=1; k<=numwpts[i][j]; k++)
		  {
		    if(used[i][j][k])
		      {
			printf("\n%d.%d.%d (%f,%f) %d", i,j,k,coords[i][j][k][0],coords[i][j][k][1],used[i][j][k]);
			cout << " REPLACE";
			stringstream ID;
			ID << i << '.' << j << '.' << k;
			temp=ID.str();
			cout << " temp = " << temp;
			while(infile>>in)
			  {
			    infile.getline(cmd,100,'\n');
			    if(temp == in)
			      {
				cout << "\nreplace " << temp << cmd;
				m_outputGPS << temp << "\t" << coords[i][j][k][0] << "\t" << coords[i][j][k][1] << "\n";
				break;
			      }
			    else
			      {
				cout << ".";
				m_outputGPS << in << cmd << "\n";
			      }
			  }
		      }
		  }
	      }
	  }
	cout << "\nend\n";

	cout << "writing\n";
	while(infile >> in)
	  {
	    infile.getline(cmd, 100, '\n');
	    m_outputGPS << in << cmd << '\n';
	  }
    }
  else
    {
      m_outputGPS << "RNDF_name\trndf_created_with_gpscap\n";
      m_outputGPS << "num_segments\t" << numsegs << "\n";
      m_outputGPS << "num_zones\t0\n";
      m_outputGPS << "format_version\t1.0\n";
      m_outputGPS << "creation_date\t01-Jan-07\n";
      
      for(int i=1; i<=numsegs; i++)
	{
	  m_outputGPS << "segment\t" << i << "\n";
	  m_outputGPS << "num_lanes\t" << numlans[i] << "\n";
	  m_outputGPS << "segment_name\tsegment_" << i << "\n";
	  for(int j=1; j<=numlans[i]; j++)
	    {
	      m_outputGPS << "lane\t" << i << '.' << j << "\n";
	      m_outputGPS << "num_waypoints\t" << numwpts[i][j] << "\n";
	      m_outputGPS << "lane_width\t" << 15 << "\n";
	      //m_outputGPS << "left_boundary\t" << "broken_white\n";
	      for(int k=1; k<=numwpts[i][j]; k++)
		{
		  m_outputGPS << i << "." << j << "." << k << "\t" << coords[i][j][k][0] << "\t" << coords[i][j][k][1] << "\n";
		}
	      m_outputGPS << "end_lane\n";
	    }
	  m_outputGPS << "end_segment\n";
	}
      m_outputGPS << "end_file\n";
    }

}


int main(int argc, char **argv) {

  int sn_key = 0;

  char* pSkynetkey = getenv("SKYNET_KEY");
  cout<<"got skynet key"<<endl;
  if ( pSkynetkey == NULL)
    {
      cerr << "SKYNET_KEY environment variable isn't set." << endl;
    } else
      {
	sn_key = atoi(pSkynetkey);
      }
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  gpsCap ast(sn_key);
  ast.Active();

  return 0;
}


