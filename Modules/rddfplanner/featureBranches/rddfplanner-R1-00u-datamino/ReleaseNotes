              Release Notes for "rddfplanner" module

Release R1-00u (Mon Jun 18 19:22:31 2007):
	Added the 'gpscap' program, which captures GPS points from astate
	and saves them to an RDDF file.  This program is useful for creating
	RDDF (and RNDF) files by driving Alice around.  See [[gpscap]] for
	more info.

Release R1-00t (Tue May 29  3:04:07 2007):
	Fixed a small bug in the trajectory that was commanded for a "stop"
	condition (changed cos -> sin, as pointed out by Kenny O).  Also
	updated UT_ocpspecs to send stop condition at end (after user
	prompt) and added a script to start up programs needed for the unit
	test (using system-tests scripts).

Release R1-00s (Sat May 19 12:57:57 2007):
	Converted UT_fwdbwd to an installed executable, so that it can be
	used for unit testing in trajfollower.  No changes in
	functionality. 

Release R1-00r (Sat May 19  9:33:09 2007):
	Added dgcFindRouteFile processing for --rddf flag.  Also fixed a bug
	with --use-final processing to allow it to work without the --rddf
	file.  (R1-00q was supposed to have this, but I forgot to commit the
	changes -rmm.)

Release R1-00q (Fri May 18 22:04:53 2007):
	Botched save (my fault, not YaM)

Release R1-00p (Fri May 18  8:57:09 2007):
	Fixed bugs in UT_fwdbwd that was causing reverse RDDF to look funny.

Release R1-00o (Sat May 13 10:27:31 2007):
	Minor update to eliminate some unused features (see ChangeLog).  No
	change in functionality.

Release R1-00n (Wed May  2 10:45:47 2007):
	This is a fairly major update to rddfplanner, with much better
	support for generating feasible trajectories with proper feedforward
	commands.  Currently works well for forward driving; backward
	driving needs work (in trajutils).

	Summary of changes:

	  * Velocity profiles are now computed properly, with accelerations
            set so that the feedfoward commands to the right thing with
            trajfollower.  This allows much tighter following of
            trajectories than previous releases.

          * The stop condition from tplanner (initial and final position
            equal, final velocity = 0) is now handled properly.  This will
            generate a trajectory that keeps the vehicle stopped.

	  * Added some logic for detecting tplanner start conditions: if we
            start at a slow velocity in a segment with a higher speed limit,
            we do a gradual profile to get to the desired speed.

	  * Updated options so the most common case is now default.  The old
  	    options still work, you just don't need them (eg: rddfplanner is
  	    equivalent to rddfplanner --use-endpoints --use-flat).

	  * Added new unit test files for rectangles, stopping, driving
            straight, turning.

Release R1-00m (Sun Apr 29 13:43:36 2007):
	The version of 'rddfplanner' has a number of enhancements to make it
	easier to use for testing:

	  * No longer requires the ocpspecs module (just uses talkers)
	  * Added --use-local option to UT_ocpspecs for shifting RDDFs
	  * Added UT_fwdbwd unit test for testing forward -> reverse
	  * Speed is now linearly scaled according to RDDF speed profile

	Other changes in this version:

	  * Removed dependence on StateClient (state no used)
	  * Recompute/resend traj only when params or RDDF is sent

	Details: The main change in this version is the way in which we look
	for RDDFs and initial/final conditions.  When the program starts up,
	it expects to read an RDDF either through skynet or from the command
	line.  In the former case, operation will pause until the RDDF is
	received.  Once an RDDF is received, rdddfplanner will process
	either initial/final conditions sent using OCPparams (if
	--use-endpoints or --use-final is given) or when a new RDDF is
	received (via skynet).  If the --use-local flag is given, the
	initial waypoint in the initial RDDF is taking to be the origin of
	the local coordinate system and this value is subtracted off from
	all subsequent RDDFs that are received.

	In addition to rddfplanner changes, the 'UT_ocptspecs' unit test
	program now accepts the --use-local flag and similar strips off the
	first point of the RDDF file before sending out RDDFs.  This can be
	used when you want to send out an RDDF but in local coordinates.

	Internal enhancements: we now use 8 basis functions for computing
	trajectories (Bezier + quadratic and cubic).  This seems to give
	good trajectories in testing, but note that the trajectories do not
	necessarily satisfy turning radius or turning rate limits (s1planner
	and/or dplanner are required for that).

Release R1-00l (Sat Apr 28 19:05:59 2007):
	Added links to /dgc/otg (need to fix ocpspecs at some point...)

Release R1-00k (Sat Apr 28 15:58:09 2007):
	Update the flatness computations to use Bezier curves.  This should
	generate better trajectories in most cases.  Also did some tweaking
	on the way the initial angle is handled.

Release R1-00j (Sat Apr 28  7:31:09 2007):
	This version of rddfplanner adds a "--use-flat" option to use
	differential flatness for computing trajectories.  The resulting
	trajectory will go from the initial condition to the final
	condition, passing through each waypoint at the velocity specified
	as the maxSpeed for that waypoint.  The angle at the waypoint is
	currently set to be the average angle between the previous and last
	waypoints.

	Details: this version of the planner works by solving a linear least
	squares problems to figure out the coefficients for the flat outputs
	that satisfy the initial, final and waypoint constraints.  It works
	by iterating through whatever RDDF it is sent an planning from one
	waypoint to the next.  It has been tested in simulation (using
	trajfollower) and the trajectories seems acceptable.  You can test
	the code by running the following unit test:

	  rddfplanner --use-endpoints --use-flat
	  UT_ocptspecs UT_sinewave.rddf

Release R1-00i (Sun Mar 18 20:27:02 2007):
	This is the version of rddfplanner that was used for FT2.  Added
	--use-final flag, which uses the final condition but not the initial
	condition.  This is needed for getting the right behavior when
	moving onto an RDDF.  In a "zone" (RDDF with N=2) we still use
	initial and final if either --use-endpoints or --use-final is
	specified.

	This version can be considered stable, but should be replaced with
	dplanner or s1planner as quickly as possible.  Functionality is too
	limited for the future testing we are going to need to do.

Release R1-00h (Sat Mar 17 13:36:54 2007):
	Fixed the way endpoints are handled for corridors with only two
	waypoints (beginning and end).  These are currently used for U-turn
	segments, so we simply replace the RDDF that is sent with the start
	and end point.  Works correctly with tplanner and trajfollower.

Release R1-00g (Fri Mar 16 10:18:27 2007):
	This version of rddfplanner has several new features to allow it to
	be used as a temporary substitute for dplanner:

	* The --use-endpoints flag reads the endpoint information sent by
          tplanner to modify the trajectories to start and end at a
          specified location.  This is needed for stoplines, intersections,
          etc.   We use the terminal velocity for the endpoint, but ignore
          initial velocity.  You can turn off the use of terminal velocity
          using the --ignore-end-velocity flag.

	* The feasiblize function was not being called properly and was not
          computing accelerations properly.  This is now fixed, although it
          doesn't really change that much.  Note that this version of
          rddfplanner does not take into account curvature limits of the
          vehicle (the dgc/trunk version did, but needed lots of code that
          we don't want to import into YaM)

	* There is now a --verbose option that can be used to turn on
          different levels of status messages.  The number of messages
          printed depend on the level of verbosity.  Roughly, verbose = 1
          will generate RddfPlanner messages that tell you stuff is
          happening.  Setting verbose = 2 will generate more detailed
          messages and will also turn on messages in OCPtSpecs (at level
          verbose-1).

        Things that are still missing: we don't set the angle of the vehicle
        at the end of the path.  This may be needed for U-turns.

Release R1-00f (Wed Mar 14 17:02:10 2007):
	Fixed a small bug that put is in the opposite mode as asked (forward
	vs reverse) 

Release R1-00e (Wed Mar 14 16:53:11 2007):
	Updated makefile to fix some linking errors.

Release R1-00d (Sun Mar 11 21:45:22 2007):
	This version of rddfplanner has hooks for planning in reverse.  This
	consists of two main changes: (1) added an interface to tplanner
	through the ocpspecs library that can receive mode changes from
	tplanner and (2) added an interface to trajfollower that can command
	a change in direction via the superCon (pseudocon) interface.

	This code has not been tested yet, mainly because we don't yet have
	a way to command reverse (that I know of) and the latest release of
	trajfollower doesn't appear to have reverse capability yet.  The
	code has been tested when no reverse command is sent and works as
	before. 

	The code here can serve as a starting point for the equivalent
	capability in dplanner.  See lines 132-162 of RddfPlanner.cc.

Release R1-00c (Sat Mar  3 19:11:41 2007):
	Found and fixed a bug which was causing a seg fault.  This version
	should now work equivalently to the dgc/trunk version with option
	'feasibilize' (not that no path type is required for the yam
	version).

Release R1-00b (Thu Feb 1 2:09:03 2007): 
	Fixed the linking problems and changed the variable names so it
	compiles against R1-00m of interfaces.

Release R1-00a (Wed Jan 31 13:39:11 2007):
	This is the first release of the rddfplanner module, copied from
	trunk.  In order to get this to work without going insane, I used
	the "feasiblize" option rather than the 'c2' option (which brings in
	a bunch of code from Dima's planner and is a mess).  I aslo hard
	coded in some of the other options that nobody has usd in years.

	This version will not compile against R1-00a of trajutils due to
	problems with linking to the standard vector library.  See release
	notes for trajutils for a bit more detail.

Release R1-00 (Sat Jan 27 18:49:02 2007):
	Created.





















