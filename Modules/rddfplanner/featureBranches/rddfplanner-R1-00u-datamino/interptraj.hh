/*!
 * \file interptraj.hh
 * \brief Header file for trajectory interpolation routine
 *
 */

#include "trajutils/traj.hh"
#include "dgcutils/RDDF.hh"

extern int create_interp_traj(CTraj *, RDDFVector &, int, int, 
			      vector<double>&, int);
