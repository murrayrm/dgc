/*!
 * \file flattraj.cc
 * \brief Trajectory generation using flatness
 *
 * \author Richard Murray
 * \date 26 Apr 07
 *
 */

#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

#include "flattraj.hh"

/* dgels declaration */
extern "C" {
extern void dgels_(char *, int *, int *, int *, double *, int *, double *,
	      int *, double *, int *, int *);
}

/* Some constants that control our operations */
const int nBasisFunctions = 8;	// number of basis functions
const int nBasisDerivs = 1;	// number of derivatives supported
const int nFlatOutputs = 2;	// number of flat outputs that we use
const int nFlatDerivs = 1;	// number of derivatives of flat outputs used
const int nTrajPoints = 5;	// number of traj points between waypoints

/* Basis functions - these are used for planning trajectories */
double basis(int index, int deriv, double time)
{
  switch (index) {
  case 0:			// phi(t) = t
    switch (deriv) {
    case 0:	return time;
    case 1:	return 1;
    case 2:	return 0;
    }

  case 1:			// phi(t) = t^2
    switch (deriv) {
    case 0:	return pow(time, 2);
    case 1:	return 2*time;
    case 2:	return 1;
    }

  case 2:			// phi(t) = t^3
    switch (deriv) {
    case 0:	return pow(time, 3);
    case 1:	return 3 * pow(time, 2);
    case 2:	return 6 * time;
    }

  case 3:			// phi(t) = (1-t)^3
    switch (deriv) {
    case 0:	return pow(1-time, 3);
    case 1:	return -3 * pow(1-time, 2);
    case 2:	return 6 * (1-time);
    }

  case 4:			// phi(t) = (1-t)^2
    switch (deriv) {
    case 0:	return pow(1-time, 2);
    case 1:	return -2 * (1-time);
    case 2:	return 2;
    }

  case 5:			// phi(t) = (1-t)
    switch (deriv) {
    case 0:	return (1-time);
    case 1:	return -1;
    case 2:	return 0;
    }

  case 6:			// phi(t) = 3 t (1-t)^2
    switch (deriv) {
    case 0:	return 3 * time * pow(1-time, 2);
    case 1:	return 3 * pow(1-time, 2) - 6 * time * (1-time);
    case 2:	return -6 * (1-time) - 6 * (1-time) + 6 * time;
    }

  case 7:			// phi(t) = 3 t^2 (1-t)
    switch (deriv) {
    case 0:	return 3 * pow(time, 2) * (1-time);
    case 1:	return 6 * time * (1-time) - 3 * pow(time, 2);
    case 2:	return 6 * (1-time) - 6 * time - 6 * time;
    }

  }
  return 0;
}

void flat_linsolve(TerminalCondition &start, TerminalCondition &stop, 
		   double *alpha, int verbose)
{
    /* Generate the linear problem that we need to solve */
    double zbar[2*nFlatOutputs*(1+nFlatDerivs)];
    double M[2*nFlatOutputs*(1+nFlatDerivs)][nFlatOutputs*nBasisFunctions];
    for (int index = 0; index < nBasisFunctions; ++index) {
      int row = 0;

      /* Initial position in x (Northing) */
      M[row][index] = basis(index, 0, 0.0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = start.x - start.x;

      /* Initial position in y (Easting) */
      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 0, 0.0); 
      zbar[row++] = start.y - start.y;

      /* Initial angle and velocity */
      M[row][index] = basis(index, 1, 0.0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = start.v * cos(start.theta);

      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 1, 0.0);
      zbar[row++] = start.v * sin(start.theta);

      /* Final position in x (Northing) */
      M[row][index] = basis(index, 0, 1.0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = stop.x - start.x;

      /* Final position in y (Easting) */
      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 0, 1.0); 
      zbar[row++] = stop.y - start.y;

      /* Final point and velocity */
      M[row][index] = basis(index, 1, 1.0);
      M[row][index+nBasisFunctions] = 0;
      zbar[row++] = stop.v * cos(stop.theta);

      M[row][index] = 0;
      M[row][index+nBasisFunctions] = basis(index, 1, 1.0);
      zbar[row++] = stop.v * sin(stop.theta);
    }

    /* Declare the variables that we need for lapack */
    char trans = 'N';		// solve for underdetermined system
    int nrows = 2*nFlatOutputs*(1+nFlatDerivs), lda = nrows;
    int ncols = nFlatOutputs*nBasisFunctions, ldb = max(nrows,ncols);
    int nrhs = 1;
    int lwork = 2*nrows*ncols;
    double A[ncols*nrows];
    double work[2*nrows*ncols];
    int info;

    /* Fill in the matrices with the information */
    for (int i = 0; i < nrows; ++i) {
      for (int j = 0; j < ncols; ++j) {
	A[i + j*nrows] = M[i][j];
      }
      alpha[i] = zbar[i];
    }

    dgels_(&trans, &nrows, &ncols, &nrhs, A, &lda, 
	   alpha, &ldb, work, &lwork, &info);
    if (verbose >= 5) {
      cerr << "dgels: info = " << info << endl;
    }

    /* Check the answer to see if it is OK */
    double zchk[2*nFlatOutputs*(1+nFlatDerivs)];
    for (int i = 0; i < nrows; ++i) {
      zchk[i] = 0;
      for (int j = 0; j < ncols; ++j) {
	zchk[i] += M[i][j] * alpha[j];
      }
    }

    if (verbose >= 5) {
      // Print the problems we are trying to solve
      unsigned int row;
      for (row = 0; row < 2*nFlatOutputs*(1+nFlatDerivs); ++row) {
	cout << zbar[row] << " = M[" << row << ",:] = [";
	for (unsigned int col = 0; col < nFlatOutputs*nBasisFunctions; ++col) {
	  cout << M[row][col] << " ";
	}
	cout << "] --- chk: " << zchk[row] << endl;
      }

      for (row = 0; row < 2*nBasisFunctions; ++row) {
	cout << "alpha[" << row << "] = " << alpha[row] << endl;
      }
    }
}

int flat_maketraj(TerminalCondition &start, double *alpha, double T, 
		  CTraj *ptraj, int last_point, int verbose) {    
  int npoints = 0;

  /* Create the CTraj given the flat trajectory (1 meter spacing) */
  double N, Nd, Ndd, E, Ed, Edd;
  for (double t = 0; t < T + T/(2*nTrajPoints); t += T/nTrajPoints) {
    N = start.x, Nd = 0, Ndd = 0;
    E = start.y, Ed = 0, Edd = 0;
    for (int index = 0; index < nBasisFunctions; ++index) {
      N += alpha[index] * basis(index, 0, t/T);
      E += alpha[index+nBasisFunctions] * basis(index, 0, t/T);
      
      Nd += alpha[index] * basis(index, 1, t/T);
      Ed += alpha[index+nBasisFunctions] * basis(index, 1, t/T);
      
      Ndd += alpha[index] * basis(index, 2, t/T);
      Edd += alpha[index+nBasisFunctions] * basis(index, 2, t/T);
    }

    if (verbose >= 9) {
      cerr << "Creating traj point for t = " << t 
	   << " at N = " << N << ", E = " << E << endl;
    }

#ifdef HACK
    /* Set the acceleration to be uniform across this segement */
    Ndd = Nd / sqrt(Nd*Nd + Ed*Ed) * (final.v - start.v);
    Edd = Ed / sqrt(Nd*Nd + Ed*Ed) * (final.v - start.v);
#endif

    /* Rescale velocity and acceleration to account for time scaling */
    Nd /= T;    Ed /= T;
    Ndd /= T*T; Edd /= T*T;

    /* Add the point to the trajectory, minus the last point */
    if (last_point || t < T - T/(2*nTrajPoints)) {
      ptraj->addPoint(N, Nd, Ndd, E, Ed, Edd);
      ++npoints;
    }
  }

  /* Reset the start condition to the final state we reached */
  start.x = N;
  start.y = E;
  start.theta = atan2(Ed, Nd);
  start.v = Nd * cos(start.theta) + Ed * sin(start.theta);

  return npoints;
}

int create_flat_traj(CTraj *ptraj, RDDFVector &waypoints,
		     int first_wp, int last_wp,
		     struct TerminalCondition initial,
		     struct TerminalCondition final, int verbose)
{
  /* Initialize the trajectory */
  ptraj->startDataInput();
  int npoints = 0;

  if (verbose >= 2) {
    cout << "flattraj: computing trajectory" << endl;
    cout << "  Initial: N = " << setprecision(8) << initial.x
	 << ", E = " << setprecision(7) << initial.y 
	 << ", theta = " << setprecision(3) << initial.theta 
	 << ", vel = " << setprecision(3) << initial.v << endl;
    cout << "  Final:   N = " << setprecision(8) << final.x
	 << ", E = " << setprecision(7) << final.y 
	 << ", theta = " << setprecision(3) << final.theta 
	 << ", vel = " << setprecision(3) << final.v << endl;
  }

  /*
   * Solve for intermediate points
   *
   * The first portion of the solution is used for all of the points
   * up to the final one.  In this stage, we solve from one waypoint
   * to the next, with an initial constraint on the angle but no
   * terminal constraint.  The final condition from one iteration
   * serves as the initial condition for the next iteration.
   */
  TerminalCondition start = initial; // starting point for each interation
  TerminalCondition stop;

  for (int wp = first_wp+1; wp < last_wp; ++wp) {
    /* Figure out the end point */
    stop.x = waypoints[wp].Northing;
    stop.y = waypoints[wp].Easting;
    stop.v = waypoints[wp].maxSpeed;
    double next_theta = 
      atan2(waypoints[wp+1].Easting  - waypoints[wp].Easting,
	    waypoints[wp+1].Northing  - waypoints[wp].Northing);
    if (fabs(next_theta - start.theta) > M_PI) {
      /* We have wrapped around M_PI; update next_theta to proper sign */
      if (next_theta < 0) next_theta += 2*M_PI;
      if (start.theta < 0) next_theta -= 2*M_PI;
    }
    stop.theta = (next_theta + start.theta)/2.0;

    /* Check for a zero velocity segment */
    if (start.v == 0 && stop.v == 0) {
      /* Just stay at this point */
      ptraj->addPoint(start.x, 0, 0, start.y, 0, 0);
      start = stop;
      continue;
    }

    /* Figure out the distance to the waypoint */
    double T = sqrt(
      pow(start.x - waypoints[wp].Northing, 2) + 
      pow(start.y - waypoints[wp].Easting, 2)) / 
      max(0.1, waypoints[wp-1].maxSpeed);
    if (verbose >= 5) {
      cout << "Time T = " << T << endl;
    }

    /* Solve for the coefficients */
    double alpha[nFlatOutputs*nBasisFunctions];
    flat_linsolve(start, stop, alpha, verbose);

    /* Generate the trajectory */
    npoints += flat_maketraj(start, alpha, T, ptraj, 0, verbose);

    if (verbose >= 5) {
      cout << "Reached N = " << start.x << ", E = " << start.y << endl;
    }
  }

  /* 
   * Solve for the final segment
   *
   * For the final portion of the solution, we use the terminal angle
   * as well as the initial angle.  This makes sure that we hit final
   * point at the desired orietnation.
   *
   */

  if (verbose >= 5) {
    cerr << "Final segment: N=" << start.x << ", E=" << start.y
	 << " ---> N=" << final.x << ", E=" << final.y << endl;
  }

  /* Make sure there is something to be done */
  if (start.v == 0 && final.v == 0) {
    /* Stay stopped */
    ptraj->addPoint(start.x, 0, 0, start.y, 0, 0);

  } else {
    double alpha[nFlatOutputs*nBasisFunctions];
    double T = sqrt(pow(start.x - final.x, 2) + pow(start.y - final.y, 2)) /
      max(start.v, waypoints[last_wp-1].maxSpeed);

    /* Solve for the coefficients */
    flat_linsolve(start, final, alpha, verbose);

    /* Generate the trajectory */
    npoints += flat_maketraj(start, alpha, T, ptraj, 1, verbose);
  }

  /*
   * Rescale the velocity
   *
   * At this point we have a trajectory that satisfies the kinematics,
   * but the velocity may be strange due to the spline functions we
   * have used.  So we go back through the trajectory and scale the
   * velocity to be something more reasonable
   *
   */

  for (int wp = first_wp; wp < last_wp; ++wp) {
    /* Set up the initial and final velocities for the segment */
    double initial_vel = 
      (wp == first_wp) ? initial.v : waypoints[wp].maxSpeed;
    double final_vel =
      (wp == last_wp-1) ? final.v : waypoints[wp+1].maxSpeed;

    /* Go through the waypoints for segment and determine desired vel */
    for (int i = 0; i < nTrajPoints; ++i) {
      int index = wp*nTrajPoints + i;

      /* Desired velocity: linearly interpolate between end points */
      double desvel = (final_vel - initial_vel) * ((double) i / nTrajPoints) +
	initial_vel;

      /* Set the speed of this waypoint */
      if (verbose >= 9) {
	cout << "setting speed for traj point " << index 
	     << " on segment " << wp << " to " << desvel << endl;
      }
      ptraj->setSpeed(index, desvel);
    }
  }

  if (verbose >= 2) {
    cout << "  Computed traj with " << ptraj->getNumPoints() 
	 << " points" << endl;
  }

  return 0;
  }

