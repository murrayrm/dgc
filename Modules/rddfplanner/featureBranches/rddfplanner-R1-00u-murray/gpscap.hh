#ifndef ASTATE_TEST_HH
#define ASTATE_TEST_HH

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>

#include "interfaces/VehicleState.h"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"
#include "dgcutils/ggis.h"
#include "dgcutils/RDDF.hh"

class gpsCap : public CStateClient {

  ofstream m_outputGPS;

  unsigned long long starttime;

  double timecalc;

public:
  gpsCap(int);
  void Active();


};

#endif
