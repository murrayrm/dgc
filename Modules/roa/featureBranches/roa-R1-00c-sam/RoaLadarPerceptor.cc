
/**********************************************************
 **
 **  ROALADARPERCEPTOR.CC
 **
 **    Time-stamp: <2007-04-04 18:02:18 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Apr  4 09:15:25 2007
 **    Modified: Sun Apr  15 08:16:00 2007 by Ghyrn Loveness
 **
 **********************************************************
 **
 **  Using Sam's template to create a simple ladar perceptor for ROA
 **
 **********************************************************/

#include "RoaLadarPerceptor.hh"

// Default constructor
RoaLadarPerceptor::RoaLadarPerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
RoaLadarPerceptor::~RoaLadarPerceptor()
{
}


// Parse the command line
int RoaLadarPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
      
  return 0;
}



// Initialize sensnet
int RoaLadarPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;
    
  // Create sensnet itnerface
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // If replaying log files, now is the time to open them
  //  if (this->mode == modeReplay)
  //  if (sensnet_open_replay(this->sensnet, this->options.inputs_num, this->options.inputs) != 0)
  //    return ERROR("unable to open log");

  // Default ladar set
  numSensorIds = 0;
  
  //Commented out the other LADARs so that we just listen to the middle bumper
  //sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RIEGL;
  //sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
    ladar = this->ladars + this->numLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (sensnet_join(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
      return ERROR("unable to join %d", ladar->sensorId);
  }

  initSendMapElement(skynetKey);

  allClear = 0;
  
  return 0;
}


// Clean up sensnet
int RoaLadarPerceptor::finiSensnet()
{
  int i;
  Ladar *ladar;

  // if(this->mode == modeReplay)
  //  sensnet_close_replay(this->sensnet);

  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;
    sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
  }
  sensnet_free(this->sensnet);
  
  return 0;
}


// Update the map with new range data
int RoaLadarPerceptor::update()
{
  //  fprintf(stderr, "entering update() \n");

  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;

  usleep(100000);
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;

    // Check the latest blob id
    if (sensnet_peek(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
      break;

    // Is this a new blob?
    if (blobId == ladar->blobId)
      continue;
    ladar->blobId = blobId;

    // If this is a new blob, read it
    if (sensnet_read(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, blobLen, &blob) != 0)
      break;
    //transformed to sensor frame
    float sfx, sfy, sfz; //sensor frame vars
     LadarRangeBlobScanToSensor(&blob, blob.points[45][0], blob.points[45][1],&sfx, &sfy, &sfz);
    //    fprintf(stderr, "middle range transformed to sensor frame (x,y,z): %f, %f, %f \n", sfx, sfy, sfz);

    //transformed to vehicle frame
    float vfx, vfy, vfz; //vehicle frame vars
    LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    //    fprintf(stderr, "middle range transformed to vehicle frame (x,y,z): %f, %f, %f \n", vfx, vfy, vfz);

    //transformed to local frame
    float lfx, lfy, lfz; //local frame vars
   LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
    //    fprintf(stderr, "middle range transformed to local frame (x,y,z): %f, %f, %f \n", lfx, lfy, lfz);

    //coyping the data to a arrays accessible by any 
    //function in the program. Seems inefficient....
    double xpts[NUMSCANPOINTS];
    double ypts[NUMSCANPOINTS];
    double zpts[NUMSCANPOINTS];

    int returnCountRight = 0;
    int returnCountLeft = 0;

    int numReturns = 0;
    point2arr ptarr;
    //    point2arr ptarrextra;
    ptarr.clear();
    // ptarrextra.clear();
    point2 pt;


    for(int j=0; j < NUMSCANPOINTS; j++) 
    {
      rawData[j][ANGLE] = blob.points[j][ANGLE];
      rawData[j][RANGE] = blob.points[j][RANGE];
      LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE],&sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
      xyzData[j][0] = lfx;
      xyzData[j][1] = lfy;
      xyzData[j][2] = lfz;

      
      //if this isn't a no-return point, send to display
      if(rawData[j][1] < 80 && rawData[j][1] > 2) {
          
          //SAM
          pt.set(lfx,lfy);
          
          
          ptarr.push_back(pt);
          
          //SAM
          
          xpts[numReturns]=lfx;
          ypts[numReturns]=lfy;
          zpts[numReturns]=lfz;
          numReturns++;
      }
      
      
      
      //ROA Processing
      
      //Count returns from the ladars within specified buffers
      // WARNING: Magic numbers here to set buffers
      double lateralBuffer = 3.0; //meters
      double forwardBuffer = 10.0; //meters
      double minRangeBuffer = 0.5; //meters
      double xd, yd; //[meters]
      
      //// Note: Left / Right is legacy of previous implementation, is obviously
      //// redundant. May want separate later, though, so it stays.

      //Read right half of ladar range vector
      if (j<(NUMSCANPOINTS/2)) {
          yd = fabs(rawData[j][RANGE]*sin((j-90)*3.14159/180));
          xd = fabs(rawData[j][RANGE]*cos((j-90)*3.14159/180));
        
          // Check within buffers
          if(xd < forwardBuffer  && yd < lateralBuffer && rawData[i][RANGE] > minRangeBuffer)
          {	
              returnCountRight++;
              //  cout<<"xd, yd: "<<xd<<' '<<yd<<endl;
          }
      }//end right half for loop
      
      //Read left half of ladar range vector
      else {
          yd = fabs(rawData[j][RANGE]*sin((j-90)*3.14159/180));
          xd = fabs(rawData[j][RANGE]*cos((j-90)*3.14159/180));
          
          // Check within buffers
          if(xd < forwardBuffer  && yd < lateralBuffer && rawData[i][RANGE] > minRangeBuffer)
          {	
              returnCountLeft++;
          }
          
      }// end left half for loop
      
    }//end ladar vector loop
    
    

    //for plotting the segmentation
    LadarRangeBlobScanToSensor(&blob, 0.0, 0.0, &sfx, &sfy, &sfz);
    LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
   
    //cout<< "leftReturnCount: "<<returnCountLeft<<endl;
    //cout<< "rightReturnCount: "<<returnCountRight<<endl;
    
    // If there is something seen by the bumper ladar within the buffers
    // that we set, then 
    if(returnCountRight + returnCountLeft > 4) {
      //cout<<"Should be slowing down"<<endl;
      allClear = 0;
      roaComm(true);

	/*
        //SAM
        MapElement el;
        point2arr tmpptarr;
        vector<point2arr> ptarrarr;
        ptarrarr=  ptarr.split(4);



        for (unsigned k = 0 ; k< ptarrarr.size(); ++k){
            tmpptarr = ptarrarr[k];
            el.clear();
            el.type = ELEMENT_OBSTACLE;
            //center_type = CENTER_BOUND_BOX;
            el.geometry_type = GEOMETRY_POLY;
            el.plot_color = MAP_COLOR_RED;
            el.height = 10;
            el.set_id(-i-10,(int)k);
            tmpptarr=tmpptarr.get_bound_box();
            el.set_geometry(tmpptarr);
            sendMapElement(&el,0);      
            sendMapElement(&el,-2);   
	  
            
            
        } //end ptarrarr loop
	*/

    } // end if statement
    else {
      allClear++;
    }

    if (allClear > 50) {
      roaComm(false);
    }
    
    //cout<<"allClear: "<<allClear<<endl;
    
//     for (unsigned k = ptarrarr.size();k<elcount[i];k++){
//       el.clear();
//       el.type=ELEMENT_CLEAR;
//       el.set_id(-i-10,(int)k);
//       sendMapElement(&el,0);      
//       sendMapElement(&el,-2);      
//     }
    
   
//     elcount[i]=ptarrarr.size();
    //TODO: check if this runs fast enough to not be limiting factor
    //    fprintf(stderr, "calling processScan() \n");

    
    
    
    if (this->console)
    {
      char token[64];
      snprintf(token, sizeof(token), "%%ladar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(ladar->sensorId),
                  blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));
    }

  cout<<"allClear: "<<allClear<<endl;
  cout<<"returnCountLeft: "<<returnCountLeft<<endl;
  cout<<"returnCountRight: "<<returnCountRight<<endl;
  
  } //end cycling through ladars

  return 0;
} //end update()


// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"RoaLadarPerceptor $Revision$                                                    \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"Ladar[4]: %ladar4%                                                         \n"
"Ladar[5]: %ladar5%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int RoaLadarPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int RoaLadarPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int RoaLadarPerceptor::onUserQuit(cotk_t *console, RoaLadarPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int RoaLadarPerceptor::onUserPause(cotk_t *console, RoaLadarPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

//method to send ROA command to TrajFollower
void RoaLadarPerceptor::roaComm(bool roa_flag)
{
  SkynetTalker<bool> roaTalker(skynetKey, SNroaFlag, MODroaLadarPerceptor);
  roaTalker.send(&roa_flag);
}


// Main program thread
int main(int argc, char **argv)
{
  RoaLadarPerceptor *percept;
  
  percept = new RoaLadarPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;


  fprintf(stderr, "entering main thread of RoaLadarPerceptor \n");

  percept->loopCount =0;
  
  while (!percept->quit)
  {
    int tempCount = percept->loopCount;
    percept->loopCount = tempCount+1;

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Wait for new data
    if (sensnet_wait(percept->sensnet, 100) != 0)
    {
      continue; ///////changed to continue to test fixing premature exits
      //break;
    } else {
      //     fprintf(stderr, "should have new data! \n");
    }

    // Update the map
    if (percept->update() != 0)
    {	
      break;
    } else {
      //      fprintf(stderr, "should be updating the map! \n");
    }

  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}

