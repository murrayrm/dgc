/**********************************************************
 **
 **  ROALADARPERCEPTOR.CC
 **
 **    Time-stamp: <2007-05-22 15:28:40 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Apr  4 09:15:25 2007
 **    Modified: Sun Apr  15 08:16:00 2007 by Ghyrn Loveness
 **    Added Map Features: Thur May 17 10:46:30 by Ghyrn Loveness
 **
 **********************************************************
 **
 **  Using Sam's template to create a simple ladar perceptor for ROA
 **
 **  Using Pete Trautman's MapPrediction file for map access code for
 **  ROA overlap detection and processing
 **
 **********************************************************/

#include "RoaLadarPerceptor.hh"

double inline Speed2(VehicleState &s) {
  return hypot(s.utmNorthVel, s.utmEastVel);
}

// Default constructor
RoaLadarPerceptor::RoaLadarPerceptor() :
  spreadDaemon( 0 ),
  skynetKey( 0 ),
  moduleId( (modulename) 0 ),
  sensnet( 0 ),
  console( 0 ),
  useDisplay( 0 ),
  quit( false ),
  pause( false ),
  loopCount( 0 ),
  allClear( 0 ),
  //  notClear( 0 ),
  //  roaCommands( 0 ),
  numLadars( 0 )
  //  subgroup( 0 )
{
  memset( ladars, 0, sizeof(ladars) );
  memset( rawData, 0, sizeof(rawData) );
  memset( xyzData, 0, sizeof(xyzData) );
  memset( depthData, 0, sizeof(depthData) );  
  //  memset( &tempObstacle, 0, sizeof(obstacle) );
}

// Not good for lists, other non-POD
//// Default constructor
//RoaLadarPerceptor::RoaLadarPerceptor()
  //{
  //  memset(this, 0, sizeof(*this));
  //  return;
  //}

/*****************************************************************************/

// Default destructor
RoaLadarPerceptor::~RoaLadarPerceptor()
{

  // Close logfile
  fclose(logfile);

  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}

/*****************************************************************************/

// Parse the command line
int RoaLadarPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
      
  return 0;
}

/*****************************************************************************/

// Initialize sensnet
int RoaLadarPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;
    
  // Create sensnet itnerface
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // Default ladar set
  numSensorIds = 0;
  
  //Commented out the other LADARs so that we just listen to the middle bumper
  //sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RIEGL;
  //sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
    ladar = this->ladars + this->numLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (sensnet_join(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
      return ERROR("unable to join %d", ladar->sensorId);
  }
  
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex); 
  
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey,0);
  initSendMapElement(skynetKey);
  outchannel = -2;
  baseId = -10;

  lastSentSize.resize(this->numLadars);
  for (i=0;i<this->numLadars;++i){
    lastSentSize[i] = 0;
  }


  m_localMap = new Map();
  
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // counters
  allClear = 0;
  numRoaCommands = 0;

  char* ladarFilename;

  time_t* currentTime;
  time(currentTime);
  tm* tmstruct = localtime(currentTime);
  sprintf(ladarFilename, "%04d%02d%02d_%02d%02d%02d.RoaLog",
	  tmstruct->tm_year + 1900, tmstruct->tm_mon + 1,
	  tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min,
	  tmstruct->tm_sec);

  //Open Log file
  logfile = fopen(ladarFilename,"w");
  
  return 0;
}

/*****************************************************************************/

// Clean up sensnet
int RoaLadarPerceptor::finiSensnet()
{
  int i;
  Ladar *ladar;

  // if(this->mode == modeReplay)
  //  sensnet_close_replay(this->sensnet);

  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;
    sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
  }
  sensnet_free(this->sensnet);
  
  return 0;
}

/*****************************************************************************/

// Thread to update map
void RoaLadarPerceptor::getLocalMapThread()
{
    
    MapElement recvEl;
    int bytesRecv;
    cout <<"Listening for map elements" << endl;
    while (true){
        bytesRecv = recvMapElementBlock(&recvEl);
        
        if (bytesRecv>0){
          DGClockMutex(&m_LocalMapMutex);
          m_localMap->addEl(recvEl);
          DGCunlockMutex(&m_LocalMapMutex);
        }else {
            cout << "Error in RoaLadarPerceptor::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
            usleep(100);
        }
    }
}

/*****************************************************************************/

// 1.) Read sensnet blobs with new range data
// 2.) Compare with map
// 3.) Send stop command and send map object

int RoaLadarPerceptor::update()
{
  MapElement el;
  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;

  unsigned long long startTime;
  DGCgettime(startTime);

  fprintf(logfile, "%llu ", startTime);
  
  usleep(1000);
 
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;

    // Check the latest blob id
    if (sensnet_peek(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
      break;

    // Is this a new blob?
    if (blobId == ladar->blobId)
      continue;
    ladar->blobId = blobId;

    // If this is a new blob, read it
    if (sensnet_read(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, blobLen, &blob) != 0)
      break;
    //transformed to sensor frame
    float sfx, sfy, sfz; //sensor frame vars
    LadarRangeBlobScanToSensor(&blob, blob.points[45][0], blob.points[45][1],&sfx, &sfy, &sfz);
    //    fprintf(stderr, "middle range transformed to sensor frame (x,y,z): %f, %f, %f \n", sfx, sfy, sfz);
    
    //transformed to vehicle frame
    float vfx, vfy, vfz; //vehicle frame vars
    LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    //    fprintf(stderr, "middle range transformed to vehicle frame (x,y,z): %f, %f, %f \n", vfx, vfy, vfz);
    
    //transformed to local frame
    float lfx, lfy, lfz; //local frame vars
    LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
    //    fprintf(stderr, "middle range transformed to local frame (x,y,z): %f, %f, %f \n", lfx, lfy, lfz);
    
    int returnCountRight = 0;
    int returnCountLeft = 0;
    double currSpeed = 0;
    //int numReturns = 0;

    point2 pt;    
    point2arr ptarr;
    ptarr.clear();

    point2 mv_pt;
    point2arr mv_ptarr;
    mv_ptarr.clear();

    currSpeed = Speed2(blob.state);
    //currSpeed = 2.3;//meters per  second
    
    point2 zero_pt;
    zero_pt.set(0,0);

    // WARNING: Magic numbers here to set buffers
    
    double minY = 1.3; //meters
    double minX = 1.6; //meters
    double min_speed = 0.1; //meters per second

    double min_Roa_Speed = 2; // meters/second
    double max_Separation_Time = 1.2; // seconds

    // (klimka) For consistency, these buffers should really be expressed in terms of the time buffers above. 
    double lateralBuffer = 3.0; //meters
    double forwardBuffer = 2*currSpeed * max_Separation_Time; //meters -- note the factor of 2; we'll be checking for obstacles twice as far away as we really need to look
    double minRangeBuffer = 0.5; //meters

    double max_OK_Closing_V = 0; // meters/second
    //    double min_Deceleration = ?; // meters/second^2 --may be useful later; not used anywhere now...

    for(int j=0; j < NUMSCANPOINTS; j++) 
    {
      rawData[j][ANGLE] = blob.points[j][ANGLE];
      rawData[j][RANGE] = blob.points[j][RANGE];
      LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE],&sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
      
      
      //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
      //ROA Processing
      
      //Count returns from the ladars within specified buffers
      
      // Note: Left / Right is legacy of previous implementation, is obviously
      // redundant. May want separate later, though, so it stays.

      double yd = fabs(rawData[j][RANGE]*sin((j-90)*3.14159/180));
      double xd = fabs(rawData[j][RANGE]*cos((j-90)*3.14159/180));

      //Read right half of ladar range vector
      if (j<(NUMSCANPOINTS/2)) 
	{
	  /*
	  if (currSpeed > min_speed) 
	    {
	      
	      if(xd < minX && yd < minY) 
		{
		  if(allClear != 0) //(klimka) do we need this if loop at all? couldn't we just say allClear = 0; roaComm(true); numRoaCommands++ ? Why is only the first ROA command counted?
		    {
		      allClear = 0;
		      //roaComm(true);
		      numRoaCommands++;
		    }
		  else 
		    {
		      //roaComm(true);
		    }
		}
	    }
	  */
	  // Check within buffers
	  if(xd < forwardBuffer  && yd < lateralBuffer && rawData[j][RANGE] > minRangeBuffer)
	    {	
	      // create pt for ladar return and add to array of return pts
	      pt.set(lfx,lfy);
	      ptarr.push_back(pt);
	      
	      mv_pt.set(xd, yd);
	      mv_ptarr.push_back(mv_pt);
	      
	      returnCountRight++;
	    }
	}//end right half for loop
      
      //Read left half of ladar range vector
      else 
	{
	  /*
	if (currSpeed > min_speed) 
	  {
	    if(xd < minX && yd < minY) 
	      {
		if(allClear != 0) 
		  {
		    allClear = 0;
		    //roaComm(true);
		    numRoaCommands++;
		  }
		else 
		  {
		    //roaComm(true);
		  }
	      }
	  }          
	  */
	// Check within buffers
	if(xd < forwardBuffer  && yd < lateralBuffer && rawData[j][RANGE] > minRangeBuffer)
          {	
	    // create pt for ladar return and add to array of return pts
	    pt.set(lfx,lfy);
	    ptarr.push_back(pt);
	    
	    mv_pt.set(xd, yd);
	    mv_ptarr.push_back(mv_pt);
	    
	    returnCountLeft++;
          }
	
	}// end left half for loop
      
    }//end ladar vector loop

    //for plotting the segmentation
    //LadarRangeBlobScanToSensor(&blob, 0.0, 0.0, &sfx, &sfy, &sfz);
    //LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    //LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
   
    // Lump pts in ptarr into clouds less than 1m apart and store in ptarrarr
    vector<point2arr> ptarrarr;
    vector<point2arr> mv_ptarrarr;
    ptarrarr = ptarr.split(1);
    mv_ptarrarr = mv_ptarr.split(1);
    
    // consider the pt clouds as obstacles and find their average
    // then check that average against the map for nearby obstacles
    int numObs = 0;
    vector<MapElement> obsarr;
    list<point2> tmpptarr;
    point2 tmppt;

    int sendsize = ptarrarr.size();


    unsigned long long processTime;
    DGCgettime(processTime);
    int processDelay = (processTime - startTime);


    for (int k = 0; k < sendsize; ++k) {
      
  
      tmppt = ptarrarr[k].average();
      tmpptarr.push_back( mv_ptarrarr[k].average() );


      DGClockMutex(&m_LocalMapMutex);
      numObs += m_localMap->getObsNearby(obsarr, tmppt, 0.5);
      DGCunlockMutex(&m_LocalMapMutex);
      
      el.clear();
      el.setId(baseId,i,k);
      el.setTypeObstacle();
      el.geometryType = GEOMETRY_POINTS;
      el.setGeometry(ptarrarr[k]);
      el.setState(blob.state);
      if (obsarr.size()>0)
        el.setColor(MAP_COLOR_GREEN);
      else
        el.setColor(MAP_COLOR_RED);
      
      sendMapElement(&el,outchannel);
      
      //  for (unsigned obsindex = 0; obsindex<obsarr.size();++obsindex){
      //   el.setId(baseId,i,k,obsindex);
      //   sendMapElement(&obsarr[obsindex],outchannel);
      // }
    }
    if (sendsize<lastSentSize[i]){
      for (int k=sendsize;k<lastSentSize[i];++k){
        el.clear();
        el.setId(baseId,i,k);
        el.setTypeClear();
        sendMapElement(&el,outchannel);      
      }
    }

    // (edit)

    if(tmpptarr.empty())
      {
	fprintf(stderr, "Empty tmpptarr. \n");
	fprintf(stderr, "Empty tmpptarr. \n");
	fprintf(stderr, "Empty tmpptarr. \n");
	fprintf(stderr, "Empty tmpptarr. \n");
	fprintf(logfile, "%d", 0);
	fprintf(logfile, "%d", 0);
	fprintf(logfile, "%d \n", 0);
      }


    int newObstacle = 0; 
    int dangerousObstacle = 0;

    double count;

    double dt_separation;
    double closing_v;
    vector< vector<double> > tmpvecvec;

    double curr_timestamp = (double) blob.timestamp;
    double d_timestamp = (curr_timestamp - previous_timestamp)/1000000.0 ;

    while( !tmpptarr.empty() )
      {
	point2 o = tmpptarr.front(); 
	tmpptarr.pop_front(); 

	fprintf(stderr, "(o.x, o.y) = %f , %f \n", o.x, o.y);

	bool updated = false;

	double distance = o.dist(zero_pt);
	double time_separation = o.dist(zero_pt)/currSpeed; 
	bool popped_up = false;

	fprintf(stderr, "Distance to object is: %f \n", distance);
	fprintf(logfile, "%f", distance);

	for(vector< vector<double> >::iterator k = old_ptvec.begin() ; k != old_ptvec.end() ; k++)
	  {

	    point2 old_pt;
	    old_pt.set((*k)[0], (*k)[1]);
	    double old_time_separation = (*k)[2];
	    count = (*k)[3];
	    double old_popped_up = (*k)[4];

	    if( o.dist(old_pt) <= 25*d_timestamp )//the 25 represents some high velocity that we don't anticipate any obstacle to be moving faster than (meters per second)
	      {
		count += 1;
		//		if(count == 4 )
		//		  newObstacle++;
		dt_separation = (old_time_separation - time_separation);
		closing_v = ( dt_separation/fabs(dt_separation) )*( o.dist(old_pt) )/d_timestamp; // dt_separation here is used only to establish a sign convention (since dist() always gives a positive value, and we want to distinguish between approaching and receding obstacles)
		old_ptvec.erase( k );
		updated = true;
		popped_up = (bool) old_popped_up;
		break;
	      }
	  }
	if ( !updated )
	  {
	    count = 0;
	    dt_separation = 0;
	    closing_v = currSpeed;
	    newObstacle++;
	    if ( time_separation < max_Separation_Time ) 
	      popped_up  = true;
	  }
	
	fprintf(stderr, "time separation: %f \n", time_separation);
	fprintf(stderr, "closing velocity: %f \n", closing_v);
	fprintf(logfile, "%f", time_separation);
	fprintf(logfile, "%f \n", dt_separation);

	if( popped_up && closing_v > max_OK_Closing_V && currSpeed > min_Roa_Speed ) // dangerous obstacle is one that popps up and is fast-closing. If an obstacle was previously sighted outside of the max_Time_Separation buffer, we assume that planners have had time to take care of it.
	  dangerousObstacle++;
	
	vector<double> tmpptvec;
	tmpptvec.push_back( o.x );
	tmpptvec.push_back( o.y );
	tmpptvec.push_back( time_separation );
	tmpptvec.push_back( count );
	tmpptvec.push_back( (double) popped_up );

	tmpvecvec.push_back( tmpptvec );
      }

    old_ptvec = tmpvecvec;

    // debug print
    fprintf(stderr, "The delay between cycles is: %f \n", d_timestamp);
  
    previous_timestamp = curr_timestamp; 

    assert( tmpptarr.empty() );
    // (end of edit)

    lastSentSize[i] = ptarrarr.size();
    

    if (sendsize > 0 && numObs > 1 && dangerousObstacle  > 0 ) {
      if (allClear != 0) {
            allClear = 0;
            numRoaCommands++;
            //roaComm(true);
	    fprintf(logfile, "%d ", 1);
        }
      else {
	allClear = 0;
	//roaComm(true);
	fprintf(logfile, "%d", 1);
      }
    }
    else {
      fprintf(logfile, "%d ", 0);
      allClear++;
    }
    
    if (allClear > 30) {
      //roaComm(false);
    }

    unsigned long long mapCheckTime;
    DGCgettime(mapCheckTime);

    int mapDelay = mapCheckTime - processTime;
    int totalDelay = mapCheckTime - startTime;

    fprintf(logfile, "%d ", sendsize);    
    fprintf(logfile, "%d ", processDelay);
    fprintf(logfile, "%d ", mapDelay);
    fprintf(logfile, "%d ", totalDelay);
    fprintf(logfile, "%d ", allClear);
    fprintf(logfile, "%d ", returnCountLeft);
    fprintf(logfile, "%d ", returnCountRight);
    fprintf(logfile, "%d ", numRoaCommands);
    fprintf(logfile, "%d ", numObs);
    fprintf(logfile, "%d ", dangerousObstacle);
    fprintf(logfile, "%f \n", currSpeed);


    if (this->console)
    {
      char token[64];
      snprintf(token, sizeof(token), "%%ladar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(ladar->sensorId),
                  blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));
    }

    fprintf(stderr, "allClear: %d \n", allClear);
    fprintf(stderr, "returnCountLeft: %d \n", returnCountLeft);
    fprintf(stderr, "returnCountRight: %d \n", returnCountRight);
    fprintf(stderr, "Number of ROA Commands: %d \n", numRoaCommands);
    fprintf(stderr, "Number of Obstacles Nearby: %d \n", numObs);
    fprintf(stderr, "Number of fast closing obstacles sighted: %d \n", dangerousObstacle); 
    fprintf(stderr, "Current speed: %f \n", currSpeed);

  } //end cycling through ladars

  return 0;
} //end update()

/*****************************************************************************/

// Main map overlap checking loop
void RoaLadarPerceptor::MapCheckLoop(void) {
    
}

/*****************************************************************************/

// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"RoaLadarPerceptor $Revision$                                                    \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"Ladar[4]: %ladar4%                                                         \n"
"Ladar[5]: %ladar5%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";

/*****************************************************************************/

// Initialize console display
int RoaLadarPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}

/*****************************************************************************/

// Finalize sparrow display
int RoaLadarPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}

/*****************************************************************************/

// Handle button callbacks
int RoaLadarPerceptor::onUserQuit(cotk_t *console, RoaLadarPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}

/*****************************************************************************/

// Handle button callbacks
int RoaLadarPerceptor::onUserPause(cotk_t *console, RoaLadarPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

/*****************************************************************************/

//method to send ROA command to TrajFollower
void RoaLadarPerceptor::roaComm(bool roa_flag)
{
  SkynetTalker<bool> roaTalker(skynetKey, SNroaFlag, MODroaLadarPerceptor);
  roaTalker.send(&roa_flag);
}

/*****************************************************************************/

// Main program thread
int main(int argc, char **argv)
{
  RoaLadarPerceptor *percept;
  
  percept = new RoaLadarPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  DGCstartMemberFunctionThread(percept, &RoaLadarPerceptor::getLocalMapThread);
  fprintf(stderr, "entering main thread of RoaLadarPerceptor \n");

  percept->loopCount =0;
  
  while (!percept->quit)
  {
    int tempCount = percept->loopCount;
    percept->loopCount = tempCount+1;

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Wait for new data
    if (sensnet_wait(percept->sensnet, 100) != 0)
    {
      continue; ///////changed to continue to test fixing premature exits
      //break;
    } else {
      //     fprintf(stderr, "should have new data! \n");
    }

    // Update the map
    if (percept->update() != 0)
    {	
      break;
    } else {
      //      fprintf(stderr, "should be updating the map! \n");
    }

  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();
  
  MSG("exited cleanly");
  
  return 0;
}

