/**********************************************************
 **
 **  ROALADARPERCEPTOR.CC
 **
 **    Time-stamp: <2007-05-22 15:28:40 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Apr  4 09:15:25 2007
 **    Modified: Sun Apr  15 08:16:00 2007 by Ghyrn Loveness
 **    Added Map Features: Thur May 17 10:46:30 by Ghyrn Loveness
 **    Added Logging: Thur May 24 11:14:20 by Ghyrn Loveness

 **********************************************************
 **
 **  Using Sam's template to create a simple ladar perceptor for ROA
 **
 **  Citing Pete Trautman's MapPrediction file for map access code for
 **  ROA overlap detection and processing
 **
 **********************************************************/

#include "RoaLadarPerceptor.hh"

double inline Speed2(VehicleState &s) {
  return hypot(s.utmNorthVel, s.utmEastVel);
}


// Default constructor
RoaLadarPerceptor::RoaLadarPerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}

/*****************************************************************************/

// Default destructor
RoaLadarPerceptor::~RoaLadarPerceptor()
{

  // Close logfile
  fclose(logfile);

  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}

/*****************************************************************************/

// Parse the command line
int RoaLadarPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
      
  return 0;
}

/*****************************************************************************/

// Initialize sensnet
int RoaLadarPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;
    
  // Create sensnet itnerface
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // Default ladar set
  numSensorIds = 0;
  
  //Commented out the other LADARs so that we just listen to the middle bumper
  //sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RIEGL;
  //sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
    ladar = this->ladars + this->numLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (sensnet_join(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
      return ERROR("unable to join %d", ladar->sensorId);
  }
  
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex); 
  
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey,0);
  initSendMapElement(skynetKey);
  outchannel = -2;
  baseId = -10;

  lastSentSize.resize(this->numLadars);
  for (i=0;i<this->numLadars;++i){
    lastSentSize[i] = 0;
  }


  m_localMap = new Map();
  
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // counters
  allClear = 0;
  numRoaCommands = 0;

  char* ladarFilename;

  time_t* currentTime;
  time(currentTime);
  tm* tmstruct = localtime(currentTime);
  sprintf(ladarFilename, "%04d%02d%02d_%02d%02d%02d.RoaLog",
	  tmstruct->tm_year + 1900, tmstruct->tm_mon + 1,
	  tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min,
	  tmstruct->tm_sec);

  //Open Log file
  logfile = fopen(ladarFilename,"w");
  
  return 0;
}

/*****************************************************************************/

// Clean up sensnet
int RoaLadarPerceptor::finiSensnet()
{
  int i;
  Ladar *ladar;

  // if(this->mode == modeReplay)
  //  sensnet_close_replay(this->sensnet);

  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;
    sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
  }
  sensnet_free(this->sensnet);
  
  return 0;
}

/*****************************************************************************/

// Thread to update map
void RoaLadarPerceptor::getLocalMapThread()
{
    
    MapElement recvEl;
    int bytesRecv;
    cout <<"Listening for map elements" << endl;
    while (true){
        bytesRecv = recvMapElementBlock(&recvEl);
        
        if (bytesRecv>0){
          DGClockMutex(&m_LocalMapMutex);
          m_localMap->addEl(recvEl);
          DGCunlockMutex(&m_LocalMapMutex);
        }else {
            cout << "Error in RoaLadarPerceptor::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
            usleep(100);
        }
    }
}

/*****************************************************************************/

// 1.) Read sensnet blobs with new range data
// 2.) Compare with map
// 3.) Send stop command and send map object

int RoaLadarPerceptor::update()
{
  MapElement el;
  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;

  unsigned long long startTime;
  DGCgettime(startTime);

  fprintf(logfile, "%llu ", startTime);
  
  usleep(100000);
 
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;

    // Check the latest blob id
    if (sensnet_peek(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
      break;

    // Is this a new blob?
    if (blobId == ladar->blobId)
      continue;
    ladar->blobId = blobId;
    
    // If this is a new blob, read it
    if (sensnet_read(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, blobLen, &blob) != 0)
      break;
    //transformed to sensor frame
    float sfx, sfy, sfz; //sensor frame vars
    LadarRangeBlobScanToSensor(&blob, blob.points[45][0], blob.points[45][1],&sfx, &sfy, &sfz);
    //    fprintf(stderr, "middle range transformed to sensor frame (x,y,z): %f, %f, %f \n", sfx, sfy, sfz);
    
    //transformed to vehicle frame
    float vfx, vfy, vfz; //vehicle frame vars
    LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    //    fprintf(stderr, "middle range transformed to vehicle frame (x,y,z): %f, %f, %f \n", vfx, vfy, vfz);
    
    //transformed to local frame
    float lfx, lfy, lfz; //local frame vars
    LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
    //    fprintf(stderr, "middle range transformed to local frame (x,y,z): %f, %f, %f \n", lfx, lfy, lfz);
    
    int returnCountRight = 0;
    int returnCountLeft = 0;
    double currSpeed = 0;
    //int numReturns = 0;
    
    point2arr ptarr;
    ptarr.clear();
    point2 pt;

    for(int j=0; j < NUMSCANPOINTS; j++) 
    {
      // Get the best estimate of Alice's current ground velocity
      currSpeed = Speed2(blob.state);
      
      rawData[j][ANGLE] = blob.points[j][ANGLE];
      rawData[j][RANGE] = blob.points[j][RANGE];
      LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE],&sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
 

      //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
      //ROA Processing
      
      //Count returns from the ladars within specified buffers
      // WARNING: Magic numbers here to set buffers
      
      double lateralBuffer = 3.0; //meters
      double forwardBuffer = 10.0; //meters
      double minRangeBuffer = 0.5; //meters
      double xd, yd; //meters
      
      double minY = 1.3; //meters
      double minX = 1.6; //meters
      double min_speed = 0.1; //meters per second
      double min_Roa_Speed = 2.0; //meters per second
      double max_Separation_Time = 1.2; //seconds
      double separation_time; //seconds
            
      //Read right half of ladar range vector
      if (j<(NUMSCANPOINTS/2)) {
	yd = fabs(rawData[j][RANGE]*sin((j-90)*3.14159/180));
	xd = fabs(rawData[j][RANGE]*cos((j-90)*3.14159/180));
	
	if (currSpeed > min_speed){
	  if(xd < minX && yd < minY){
	    roaComm(true);
	    numRoaCommands++;
	  }
	}

	if (currSpeed > min_Roa_Speed) {
	  separation_time = (rawData[j][RANGE])/currSpeed;
	}
	
	// Check within buffers
	if(xd < forwardBuffer  && yd < lateralBuffer && rawData[j][RANGE] > minRangeBuffer && separation_time < max_Separation_Time)
          {	
	    // create pt for ladar return and add to array of return pts
	    pt.set(lfx,lfy);
	    ptarr.push_back(pt);
	    returnCountRight++;
          }
      }//end right half for loop
      
      //Read left half of ladar range vector
      else {
	yd = fabs(rawData[j][RANGE]*sin((j-90)*3.14159/180));
	xd = fabs(rawData[j][RANGE]*cos((j-90)*3.14159/180));

	if (currSpeed > min_speed){
	  if(xd < minX && yd < minY){
	    roaComm(true);
	    numRoaCommands++;
	  }
	}
	
	if (currSpeed > min_Roa_Speed) {
	  separation_time = (rawData[j][RANGE])/currSpeed;
	}

	// Check within buffers
	if(xd < forwardBuffer  && yd < lateralBuffer && rawData[j][RANGE] > minRangeBuffer && separation_time < max_Separation_Time)
          {	
	    // create pt for ladar return and add to array of return pts
	    pt.set(lfx,lfy);
	    ptarr.push_back(pt);
	    returnCountLeft++;
          }
	
      }// end left half for loop
      
    }//end ladar vector loop

    fprintf(stderr, "Current Alice Ground Velocity: %f \n", currSpeed);
    
    //for plotting the segmentation
    //     LadarRangeBlobScanToSensor(&blob, 0.0, 0.0, &sfx, &sfy, &sfz);
    //     LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    //     LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
   
    // Lump pts in ptarr into clouds less than 1m apart and store in ptarrarr
    vector<point2arr> ptarrarr;
    ptarrarr = ptarr.split(1);
    
    // consider the pt clouds as obstacles and find their average
    // then check that average against the map for nearby obstacles
    int numObs = 0;
    vector<MapElement> obsarr;
    point2arr tmpptarr;
    point2 tmppt;
   
    int sendsize = ptarrarr.size();
    fprintf(logfile, "%d ", sendsize);

    unsigned long long processTime;
    DGCgettime(processTime);
    int processDelay = (processTime - startTime);
    fprintf(logfile, "%d ", processDelay);

    for (int k = 0; k < sendsize; ++k) {
      
  
      tmppt = ptarrarr[k].average();
      //        tmpptarr.push_back(ptarrarr[k].average());
      
      DGClockMutex(&m_LocalMapMutex);
      numObs += m_localMap->getObsNearby(obsarr, tmppt, 0.5);
      DGCunlockMutex(&m_LocalMapMutex);
      
      el.clear();
      el.setId(baseId,i,k);
      el.setTypeObstacle();
      el.geometryType = GEOMETRY_POINTS;
      el.setGeometry(ptarrarr[k]);
      el.setState(blob.state);
      if (obsarr.size()>0)
        el.setColor(MAP_COLOR_GREEN);
      else
        el.setColor(MAP_COLOR_RED);
      
      sendMapElement(&el,outchannel);
      
      //  for (unsigned obsindex = 0; obsindex<obsarr.size();++obsindex){
      //   el.setId(baseId,i,k,obsindex);
      //   sendMapElement(&obsarr[obsindex],outchannel);
      // }
    }
    if (sendsize<lastSentSize[i]){
      for (int k=sendsize;k<lastSentSize[i];++k){
        el.clear();
        el.setId(baseId,i,k);
        el.setTypeClear();
        sendMapElement(&el,outchannel);      
      }
    }

    lastSentSize[i] = ptarrarr.size();
    
    if (sendsize > 0 && numObs > 1) {
      if (allClear != 0) {
            allClear = 0;
	    numRoaCommands++;
            roaComm(true);
	    fprintf(logfile, "%d ", 1);
        }
      else {
	allClear = 0;
	roaComm(true);
	fprintf(logfile, "%d", 1);
      }
    }
    else {
      fprintf(logfile, "%d ", 0);
      allClear++;
    }
    
    if (allClear > 30) {
        roaComm(false);
    }

    unsigned long long mapCheckTime;
    DGCgettime(mapCheckTime);

    int mapDelay = mapCheckTime - processTime;
    int totalDelay = mapCheckTime - startTime;
    
    fprintf(logfile, "%d ", mapDelay);
    fprintf(logfile, "%d ", totalDelay);
    fprintf(logfile, "%d ", allClear);
    fprintf(logfile, "%d ", returnCountLeft);
    fprintf(logfile, "%d ", returnCountRight);
    fprintf(logfile, "%d ", numRoaCommands);
    fprintf(logfile, "%d \n", numObs);

    if (this->console)
    {
      char token[64];
      snprintf(token, sizeof(token), "%%ladar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(ladar->sensorId),
                  blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));
    }

    fprintf(stderr, "allClear: %d \n", allClear);
    fprintf(stderr, "returnCountLeft: %d \n", returnCountLeft);
    fprintf(stderr, "returnCountRight: %d \n", returnCountRight);
    fprintf(stderr, "Number of ROA Commands: %d \n", numRoaCommands);
    fprintf(stderr, "Number of Obstacles Nearby: %d \n", numObs);
 
  } //end cycling through ladars

  return 0;
} //end update()

/*****************************************************************************/

// Main map overlap checking loop
void RoaLadarPerceptor::MapCheckLoop(void) {
    
}

/*****************************************************************************/

// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"RoaLadarPerceptor $Revision$                                                    \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"Ladar[4]: %ladar4%                                                         \n"
"Ladar[5]: %ladar5%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";

/*****************************************************************************/

// Initialize console display
int RoaLadarPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}

/*****************************************************************************/

// Finalize sparrow display
int RoaLadarPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}

/*****************************************************************************/

// Handle button callbacks
int RoaLadarPerceptor::onUserQuit(cotk_t *console, RoaLadarPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}

/*****************************************************************************/

// Handle button callbacks
int RoaLadarPerceptor::onUserPause(cotk_t *console, RoaLadarPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

/*****************************************************************************/

//method to send ROA command to TrajFollower
void RoaLadarPerceptor::roaComm(bool roa_flag)
{
  SkynetTalker<bool> roaTalker(skynetKey, SNroaFlag, MODroaLadarPerceptor);
  roaTalker.send(&roa_flag);
}

/*****************************************************************************/

// Main program thread
int main(int argc, char **argv)
{
  RoaLadarPerceptor *percept;
  
  percept = new RoaLadarPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  DGCstartMemberFunctionThread(percept, &RoaLadarPerceptor::getLocalMapThread);
  fprintf(stderr, "entering main thread of RoaLadarPerceptor \n");

  percept->loopCount =0;
  
  while (!percept->quit)
  {
    int tempCount = percept->loopCount;
    percept->loopCount = tempCount+1;

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Wait for new data
    if (sensnet_wait(percept->sensnet, 100) != 0)
    {
      continue; ///////changed to continue to test fixing premature exits
      //break;
    } else {
      //     fprintf(stderr, "should have new data! \n");
    }

    // Update the map
    if (percept->update() != 0)
    {	
      break;
    } else {
      //      fprintf(stderr, "should be updating the map! \n");
    }

  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();
  
  MSG("exited cleanly");
  
  return 0;
}

