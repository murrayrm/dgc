#ifndef __CELEVATIONFUSER_HH__
#define __CELEVATIONFUSER_HH__

#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>

#include <frames/coords.hh>

using namespace std;

enum CELL_CONDITION {
    DIRTY = 0,
    CLEAN = 1
};

class CElevationFuser {

public:
  CElevationFuser();
  ~CElevationFuser();
  
  enum CELL_TYPE {
    OUTSIDE_MAP = 0,
    EMPTY = 1,
    DATA  = 2
  };
  
  struct ElevationFuserData {
    double meanElevation;
    double meanSquaredElevation;
    int  numPoints;
    double variance;
    double stdDev;
    CELL_TYPE cellType;
    CELL_CONDITION cellCondition;
    NEDcoord UTMPoint;
  };
  
  enum STATUS {
    ERROR,
    GOOD
  };

  //KF fusing
  STATUS fuse_KFElevation(NEDcoord otherPoint, double measHeightVar);
  
  //Simplest fusion possible - unweighted averaging
  STATUS fuse_MeanElevation(NEDcoord otherPoint);

  STATUS setOutsideMap();
  STATUS resetNoData();
  STATUS setCellCondition(CELL_CONDITION condition);
  STATUS setMeanElevation(double elev);


  ElevationFuserData getData() const;
  double getMeanElevation() const;
  double getMeanSquaredElevation() const;
  int getNumPoints() const;
  double getVariance() const;
  double getStdDev() const;
  CELL_TYPE getCellType() const;
  CELL_CONDITION getCellCondition() const;

  bool operator== (const CElevationFuser other) const;
  bool operator!= (const CElevationFuser other) const;

  CElevationFuser& operator=(const CElevationFuser& other); 

  friend istream& operator>> (istream& is, CElevationFuser& destination);

private:
  ElevationFuserData _data;
};

ostream& operator<< (ostream& os, const CElevationFuser& source);

#endif //__CELEVATIONFUSER_HH__
