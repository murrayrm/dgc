
/* 
 * Desc: Obstacle perceptor
 * Date: 31 Jan 2007
 * Author: Andrew Howard
 * SVN: $Id$
*/

#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

// Sensnet/Skynet support
#include <skynet/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>

// Console support
#include <ncurses.h>
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"


// Line perceptor module
class ObsPerceptor
{
  public:

  // Constructor
  ObsPerceptor();

  // Destructor
  ~ObsPerceptor();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  public:
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  public:

  // Update the map
  int update();

  public:

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, ObsPerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, ObsPerceptor *self, const char *token);

  public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;

  // Sensnet module
  sensnet_t *sensnet;

  // Console interface
  cotk_t *console;
  
  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;
  Ladar ladars[16];
};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
ObsPerceptor::ObsPerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
ObsPerceptor::~ObsPerceptor()
{
  return;
}


// Parse the command line
int ObsPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
      
  return 0;
}



// Initialize sensnet
int ObsPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;
    
  // Create sensnet itnerface
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // Default ladar set
  numSensorIds = 0;
  sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
    ladar = this->ladars + this->numLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (sensnet_join(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob), 5) != 0)
      return ERROR("unable to join %s", ladar->sensorId);
  }
  
  return 0;
}


// Clean up sensnet
int ObsPerceptor::finiSensnet()
{
  int i;
  Ladar *ladar;

  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;
    sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
  }
  sensnet_free(this->sensnet);
  
  return 0;
}


// Update the map with new range data
int ObsPerceptor::update()
{
  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;

  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;

    // Check the latest blob id
    if (sensnet_peek(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
      break;

    // Is this a new blob?
    if (blobId == ladar->blobId)
      continue;
    ladar->blobId = blobId;

    // If this is a new blob, read it
    if (sensnet_read(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, blobId, blobLen, &blob) != 0)
      break;

    // TODO: update the map
    if (this->console)
    {
      char token[64];
      snprintf(token, sizeof(token), "%%ladar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(ladar->sensorId),
                  blob.scanid, fmod((double) blob.timestamp * 1e-6, 10000));
    }
  }

  return 0;
}


// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"ObsPerceptor $Revision$                                                    \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int ObsPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int ObsPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int ObsPerceptor::onUserQuit(cotk_t *console, ObsPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int ObsPerceptor::onUserPause(cotk_t *console, ObsPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  ObsPerceptor *percept;
  
  percept = new ObsPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;
  
  while (!percept->quit)
  {
    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Update the map
    if (percept->update() != 0)
      break;
  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}



