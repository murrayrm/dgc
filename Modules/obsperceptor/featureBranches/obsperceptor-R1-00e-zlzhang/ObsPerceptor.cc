
/* 
 * Desc: Obstacle perceptor
 * Date: 31 Jan 2007
 * Author: Andrew Howard
 * 
 * Modified by: Jeremy Ma
 * Date: 3 Feb 2007
 * SVN: $Id$
*/

#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <iostream>
#include <iomanip>
#include <vector>

#include <dgcutils/DGCutils.hh>
#include <alice/AliceConstants.h>
#include <frames/pose3.h>

// Sensnet/Skynet support
#include <sensnet/sensnet.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/ObsMapMsg.h>

// Console support
#include <ncurses.h>
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

// CMapPlus class
#include <cmap/CMapPlus.hh>
#include "CElevationFuser.hh"


// Resolution of the map;
// NOTE: if you change these values, you will most likely
//       want to change the resolution of the map maintained
//       in sensviewer
#define NUM_ROWS 500
#define NUM_COLS 500
#define RES_ROWS 0.40
#define RES_COLS 0.40

// Line perceptor module
class ObsPerceptor
{
public:

  // Constructor
  ObsPerceptor();

  // Destructor
  ~ObsPerceptor();

public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

public:
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  int writeSensnet();

public:

  // Update the map
  int update();

public:

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, ObsPerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, ObsPerceptor *self, const char *token);

public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;

  // Fusion Map parameters
  CMapPlus fusionMap;
  int fusedMapID;

  // Sensnet module
  sensnet_t *sensnet;

  // Operation mode
  enum {modeLive, modeReplay} mode;

  // Console interface
  cotk_t *console;
  
  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  // Current state data
  VehicleState state;
  
  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;

  Ladar ladars[16];

  //this will be a list of delta cell locations
  vector<NEDcoord> deltaVec;

  // Time at which we last sent a map update
  uint64_t lastWriteTime;

  // Total number of points processed since last map update
  int numPoints;

  // Outgoing map update
  ObsMapMsg obsMsg;
};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
ObsPerceptor::ObsPerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
ObsPerceptor::~ObsPerceptor()
{
  return;
}


// Parse the command line
int ObsPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
  
  // See which mode we are running in (sensnet or log)
  if (this->options.inputs_num == 0)
    this->mode = modeLive;
  else
    this->mode = modeReplay;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
      
  return 0;
}



// Initialize sensnet
int ObsPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;
  CElevationFuser NoDataValue,OutsideMapValue;

  // Initialize the values for NoData and OutsideMap cell values
  NoDataValue.resetNoData();
  OutsideMapValue.setOutsideMap();

  // Create sensnet interface
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // If replaying log files, now is the time to open them
  if (this->mode == modeReplay)
    if (sensnet_open_replay(this->sensnet, this->options.inputs_num, this->options.inputs) != 0)
      return ERROR("unable to open log");
  
  // Default ladar set
  numSensorIds = 0;
  sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;

  // fusionMap will be initially centered at origin
  // the map will be centered once we enter the update loop
  this->fusionMap.initMap( 0.0, 0.0, NUM_ROWS, NUM_COLS, RES_ROWS, RES_COLS, 0);

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
    ladar = this->ladars + this->numLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (sensnet_join(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob), 5) != 0)
      return ERROR("unable to join %d", ladar->sensorId);
  }

  // Add single layer to fusionMap that will be a layer of CElevationFuser cells
  // --the false is a boolean indicating whether or not we're using deltas and 
  // --because of poor documentation on deltas, I'm not using them 
  this->fusedMapID = this->fusionMap.addLayer<CElevationFuser>(NoDataValue,OutsideMapValue, false);

  // Initialize number of deltas to zero
  this->obsMsg.numDeltas = 0;

  return 0;
}




// Clean up sensnet
int ObsPerceptor::finiSensnet()
{
  int i;
  Ladar *ladar;

  if (this->mode == modeReplay)
    sensnet_close_replay(this->sensnet);
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;
    sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
  }
  sensnet_free(this->sensnet);
  
  return 0;
}


// Update the map with new range data
int ObsPerceptor::update()
{
  int i,j;
  bool NOT_YET_UPDATED_VEHICLE = true;
  Ladar *ladar;
  LadarRangeBlob blob;
  int blobId, blobLen;
  NEDcoord newUTMPoint;
  CElevationFuser tempCell;

  //=====================================================
  // Generate fused map across all ladars
  // -- for each ladar, check if we have new measurements
  // -- for each new point, transform to global frame
  // -- fuse each new point into appropriate cell
  //=====================================================
  
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;
    
    // Check the latest blob id
    if (sensnet_peek(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
      break;
    
    // Is this a new blob?
    if (blobId == ladar->blobId)
      continue;
    
    ladar->blobId = blobId;
    
    // If this is a new blob, read it
    if (sensnet_read(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, blobId, blobLen, &blob) != 0)
      break;

    // Store the latest vehicle state
    if (blob.state.timestamp > this->state.timestamp)
      this->state = blob.state;

    // Keep track of the total number of points processed since the
    // last map update.
    this->numPoints += blob.num_points;
    
    if(NOT_YET_UPDATED_VEHICLE)
    {
      this->fusionMap.updateVehicleLoc(blob.state.localX, blob.state.localY);
      NOT_YET_UPDATED_VEHICLE = false;
    }
    
    for (j = 0; j< blob.num_points; j++)
    {
      float sx, sy, sz;
      float vx, vy, vz;
      float lx, ly, lz;

      // HACK MAGIC
      // Cutoff points at max range
      if (blob.points[j][1] > 79.0)
        continue;
      
      //take range points and transform into local frame
      sensnet_ladar_br_to_xyz(&blob,blob.points[j][0],blob.points[j][1], &sx, &sy, &sz);
      sensnet_ladar_sensor_to_vehicle(&blob, sx, sy, sz, &vx, &vy, &vz);
      sensnet_ladar_vehicle_to_local(&blob, vx, vy, vz, &lx, &ly, &lz);

      newUTMPoint.N = lx;
      newUTMPoint.E = ly;
      newUTMPoint.D = lz;
      
      tempCell = this->fusionMap.getDataUTM<CElevationFuser>(this->fusedMapID, newUTMPoint.N, newUTMPoint.E);
      if(tempCell.getCellType()!= CElevationFuser::OUTSIDE_MAP)
      {
        tempCell.fuse_MeanElevation(newUTMPoint);
        if(tempCell.getCellCondition()==CLEAN)
        {
          this->deltaVec.push_back(newUTMPoint);
          tempCell.setCellCondition(DIRTY);
        }
        this->fusionMap.setDataUTM<CElevationFuser>(this->fusedMapID, newUTMPoint.N, newUTMPoint.E, tempCell);
      }
    }

    if (this->console)
    {
      char token[64];
      snprintf(token, sizeof(token), "%%ladar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(ladar->sensorId),
                  blob.scanid, fmod((double) blob.timestamp * 1e-6, 10000));
    }
  }
  
  return 0;
}


// Publish data to Sensnet
int ObsPerceptor::writeSensnet()
{
  int messLen; 
  CElevationFuser tempCell;
  CElevationFuser::CELL_TYPE cellTypeEF;
  ElevationData tempData;

  // How long since we last sent a message?
  // TODO use command line option for update rate
  if (this->state.timestamp - this->lastWriteTime < 100000)
    return 0;
  this->lastWriteTime = this->state.timestamp;

  if (this->console)
  {
    // Display some map stats
    cotk_printf(this->console, "%stats%", A_NORMAL, "points %d deltas %d",
                this->numPoints, this->deltaVec.size());
  }
  
  // Clear the message to be safe (but slow)
  memset(&this->obsMsg, 0, sizeof(this->obsMsg)); 
  
  // Copy latest vehicle state
  this->obsMsg.state = this->state;
  
  // See how many changes there are
  this->obsMsg.numDeltas = (int)(this->deltaVec.size());
  if (this->obsMsg.numDeltas == 0)
    return 0;

  // Check for message overflow
  if ((size_t) obsMsg.numDeltas >= sizeof(obsMsg.mapDeltaVec)/sizeof(obsMsg.mapDeltaVec[0]))
  {
    MSG("discarding deltas; FIX ME");
    obsMsg.numDeltas = sizeof(obsMsg.mapDeltaVec)/sizeof(obsMsg.mapDeltaVec[0]);
  }
          
  for(int i=0; i< this->obsMsg.numDeltas; i++)
  {
    tempCell = this->fusionMap.getDataUTM<CElevationFuser>(this->fusedMapID, this->deltaVec[i].N, this->deltaVec[i].E);

    //=======================================================================================
    //NOTE: deltaVec[i] is not equal to tempData.UTMPoint; tempData.UTMPoint is the UTMPoint
    //      of the last point to be fused into that cell; deltaVec[i] is the first data point 
    //      to be fused into that cell; ultimately, this is a minor detail since the two 
    //      points will be off by only about 40cm; both points still access the same cell
    //=======================================================================================
    tempData.meanElevation = tempCell.getMeanElevation();
    tempData.stdDev = tempCell.getStdDev();
    tempData.UTMPoint =this->deltaVec[i];
    cellTypeEF = tempCell.getCellType();
    if(cellTypeEF==CElevationFuser::OUTSIDE_MAP)
    {
      tempData.cellType = OUTSIDE_MAP;
    }
    else if(cellTypeEF==CElevationFuser::EMPTY)
    {
      tempData.cellType = EMPTY;
    }
    else if(cellTypeEF==CElevationFuser::DATA)
    {
      tempData.cellType = DATA;
    }
      
    this->obsMsg.mapDeltaVec[i] = tempData;
    tempCell.setCellCondition(CLEAN);
    this->fusionMap.setDataUTM<CElevationFuser>(this->fusedMapID,this->deltaVec[i].N, this->deltaVec[i].E, tempCell);
  }

  this->obsMsg.msg_type = SNfusiondeltamap; 
  messLen = sizeof(this->obsMsg) - sizeof(this->obsMsg.mapDeltaVec) + this->obsMsg.numDeltas*sizeof(this->obsMsg.mapDeltaVec[0]); 

  // Send obstacle map 
  sensnet_write(this->sensnet, SENSNET_SKYNET_SENSOR, 
                this->obsMsg.msg_type,  
                0, messLen, &this->obsMsg); 

  // Reset the delta vector, since we sent them all
  this->numPoints = 0;
  this->deltaVec.clear();
    
  return 0;
} 

// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"ObsPerceptor $Revision$                                                    \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"Ladar[4]: %ladar4%                                                         \n"
"Ladar[5]: %ladar5%                                                         \n"
"                                                                           \n"
"Map : %stats%                                                              \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int ObsPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console, NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int ObsPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int ObsPerceptor::onUserQuit(cotk_t *console, ObsPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int ObsPerceptor::onUserPause(cotk_t *console, ObsPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  ObsPerceptor *percept;
  
  percept = new ObsPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;
  
  while (!percept->quit)
  {
    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }
    
    if (percept->mode == ObsPerceptor::modeLive)
    {
      // Wait for new data
      if (sensnet_wait(percept->sensnet, 100) != 0)
        break;
    }
    else
    {
      // Step through the log
      //usleep(15000); // HACK TODO
      if (sensnet_step(percept->sensnet, 0) != 0)
        break;
    }
    
    // Update the map
    if (percept->update() != 0)
      break;

    // Publish data
    if (percept->writeSensnet() != 0)
      break;
  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}



