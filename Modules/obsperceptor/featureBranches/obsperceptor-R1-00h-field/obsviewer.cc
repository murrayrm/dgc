#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include <interfaces/sn_types.h>
#include <cmap/CMapPlus.hh>
#include <dgcutils/DGCutils.hh>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ObsMapMsg.h>
#include <alice/AliceConstants.h>

using namespace std;


const int NUM_ROWS=500;
const int NUM_COLS=500;
const double RES_ROWS=0.40;
const double RES_COLS=0.40;


//OpenGL variables
float lastx, lasty;
bool lbuttondown = false;
bool rbuttondown = false;
bool CIRCLE=true;
bool SQUARE=false;

const int WINDOW_WIDTH=1000;
const int WINDOW_HEIGHT=800;

const double BORDER_WIDTH=60;
const double BORDER_HEIGHT=60;

const double XMIN=-(double)NUM_COLS/2-BORDER_WIDTH;
const double XMAX= (double)NUM_COLS/2+BORDER_WIDTH;
const double YMIN=-(double)NUM_ROWS/2-BORDER_HEIGHT;
const double YMAX= (double)NUM_ROWS/2+BORDER_HEIGHT;

const double ELEVATION_FILTER_THRESH = 1; //in meters  

#define MAX_DELTA_SIZE 1000000


//Spread global variables
int flag = 0;
int mess_recvd = 0;

// Rainbow color scale
uint8_t rainbow[0x10000][3];

// Map variables
CMapPlus* obsmap = new CMapPlus();
ObsMapMsg* blob = new ObsMapMsg();
int obsmapID_meanElevation, obsmapID_stdDev, obsmapID_cellType;
unsigned long long timestamp;


//SENSNET VARIABLES
sensnet_t *sensnet;
char* spreadDaemon;
int skynetKey;

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

int init(void)
{
  // Fill out the spread name
  if (getenv("SPREAD_DAEMON"))
    {
      spreadDaemon = getenv("SPREAD_DAEMON");
    }
  else
    {
      printf("unknown Spread daemon: please set SPREAD_DAEMON");
      return(-1);
    }

  // Fill out the skynet key
  if (getenv("SKYNET_KEY"))
    {
      skynetKey = atoi(getenv("SKYNET_KEY"));      
    }
  else
    {
      printf("unknown SKYNET_KEY: please set SKYNET_KEY");
      return(-1);
    }

  // Create sensnet interface
  sensnet = sensnet_alloc();
  assert(sensnet);
  if (sensnet_connect(sensnet, spreadDaemon, skynetKey,300) != 0)
    return(-1);

  // Join group
  if(sensnet_join(sensnet,SENSNET_SKYNET_SENSOR, SNfusiondeltamap, sizeof(*blob), 1000)!=0)
    {
     printf("unable to join %d", SNfusiondeltamap);
     return(-1);
    }
  
  // initialize Map
  obsmap->initMap(0.0,0.0,NUM_ROWS,NUM_COLS,RES_ROWS,RES_COLS,0);
  obsmapID_meanElevation = obsmap->addLayer<double>(0.0, -1.0, false);
  obsmapID_stdDev = obsmap->addLayer<double>(0.0, 0.0, false);
  obsmapID_cellType = obsmap->addLayer<CELL_TYPE>(EMPTY,OUTSIDE_MAP,false);

  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
    {
      d = 4 * ((double) i / 0x10000);
      
      if (d >= 0 && d < 1.0)
	{
	  rainbow[i][0] = 0x00;
	  rainbow[i][1] = (int) (d * 0xFF);
	  rainbow[i][2] = 0xFF;
	}
      else if (d < 2.0)
	{
	  d -= 1.0;
	  rainbow[i][0] = 0x00;
	  rainbow[i][1] = 0xFF;
	  rainbow[i][2] = (int) ((1 - d) * 0xFF);
	}
      else if (d < 3.0)
	{
	  d -= 2.0;
	  rainbow[i][0] = (int) (d * 0xFF);
	  rainbow[i][1] = 0xFF;
	  rainbow[i][2] = 0x00;
	}
      else if (d < 4.0)
	{
	  d -= 3.0;
	  rainbow[i][0] = 0xFF;
	  rainbow[i][1] = (int) ((1 - d) * 0xFF);
	  rainbow[i][2] = 0x00;
	}
      else
	{
	  rainbow[i][0] = 0xFF;
	  rainbow[i][1] = 0x00;
	  rainbow[i][2] = 0x00;
	}
    }
  return(0);
  
}

void drawString (char *s)
{
  unsigned int i;
  for (i = 0; i < strlen (s); i++)
    glutBitmapCharacter (GLUT_BITMAP_HELVETICA_12, s[i]);
};


void display(void)
{
  double relativeElevation;
  double meanElevation;
  double groundElevation;
  int k;
  float x_bottomleft, x_bottomright, x_topleft, x_topright;
  float y_bottomleft, y_bottomright, y_topleft, y_topright;
  static char time_label[100];
  static char title_label[100];
  static char state_label[100];

  glClearColor (0.0,0.0,0.0,1.0); //clear the screen to black
  glClear (GL_COLOR_BUFFER_BIT); //clear the color buffer 

  //====================
  //draw bounding box
  //====================
  //vertical lines
  glLineWidth(0.5);
  glColor3f (0.7F, 0.7F, 0.7F);

  glBegin (GL_LINES);
  glVertex2f (-NUM_COLS/2, YMIN+BORDER_HEIGHT);
  glVertex2f (-NUM_COLS/2, YMAX-BORDER_HEIGHT);
  glEnd ();

  glBegin (GL_LINES);
  glVertex2f (NUM_COLS/2, YMIN+BORDER_HEIGHT);
  glVertex2f (NUM_COLS/2, YMAX-BORDER_HEIGHT);
  glEnd ();
  
  
  //horizontal lines
  glLineWidth(0.5);
  glColor3f (0.7F, 0.7F, 0.7F);
  glBegin (GL_LINES);
  glVertex2f (XMIN+BORDER_WIDTH, -NUM_ROWS/2);
  glVertex2f (XMAX-BORDER_WIDTH, -NUM_ROWS/2);
  glEnd ();

  glBegin (GL_LINES);
  glVertex2f (XMIN+BORDER_WIDTH, NUM_ROWS/2);
  glVertex2f (XMAX-BORDER_WIDTH, NUM_ROWS/2);
  glEnd ();

  if(SQUARE)
    {
      //===================
      //draw Alice as a square
      //===================
      glColor3f(1.0f,0.0f,0.0f);
      glBegin(GL_LINE_STRIP);
      glVertex2f(-5,-5);
      glVertex2f(-5,5);
      glVertex2f(5,5);
      glVertex2f(5,-5);
      glVertex2f(-5,-5);
      glEnd();
    }

  if(CIRCLE)
    {
      //===================
      //draw Alice as a circle
      //===================
      double angle;
      glColor3f(0.0f,1.0f,1.0f);
      glBegin(GL_LINE_LOOP);
      for(int j=0; j<360; j++)
	{
	  angle = 2*M_PI/360*j;
	  glVertex2f(5*cos(angle), 5*sin(angle));
	}
      glEnd();
    }

  if(mess_recvd==1)
    {
            
      //============START MUTEX LOCK=============
      pthread_mutex_lock(&mutex1);
      
      for(int j=0; j<NUM_ROWS; j++)
	{
	  for(int i=0; i<NUM_COLS; i++)
	    {
	      
	      groundElevation = blob->state.localZ + VEHICLE_TIRE_RADIUS;
	      meanElevation = obsmap->getDataWin<double>(obsmapID_meanElevation, i,j);
	      
	      // See if there is any data in this cell before we draw it
	      if (meanElevation == 0)
		continue;     
	      
	      relativeElevation = groundElevation-meanElevation;
	      if(relativeElevation>ELEVATION_FILTER_THRESH)
		{
	      
		  // Select color based on elevation relative to vehicle
		  k = (int) (0x10000 * (float)(relativeElevation)/ 2.0); // MAGIC
		  if (k < 0x0000) k = 0x0000;
		  if (k > 0xFFFF) k = 0xFFFF;
		  glColor3ub(rainbow[k][0], rainbow[k][1], rainbow[k][2]);
		  
		  
		  //start filling in boxes from bottom left of grid
		  
		  x_bottomleft = i-NUM_COLS/2;
		  y_bottomleft = j-NUM_ROWS/2;
		  x_bottomright = i+1-NUM_COLS/2;
		  y_bottomright = j-NUM_ROWS/2;
		  x_topleft = i-NUM_COLS/2;
		  y_topleft = j+1-NUM_ROWS/2;
		  x_topright = i+1-NUM_COLS/2;
		  y_topright = j+1-NUM_ROWS/2;
		  
		  glBegin(GL_QUADS);
		  glVertex2f(x_bottomleft,y_bottomleft);
		  glVertex2f(x_bottomleft,y_topleft);
		  glVertex2f(x_bottomright,y_topright);
		  glVertex2f(x_bottomright,y_bottomright);	
		  glEnd();

		}
	      
	    }
	}

      DGCgettime(timestamp);
	  
      sprintf(time_label, "timestamp : %20.1f", (double)timestamp);
      glColor3f(0.0F,1.0F,0.0F);
      glRasterPos2f ( XMAX-165, YMAX-BORDER_HEIGHT/2);
      drawString (time_label); 
      
      sprintf(state_label, "state(N,E) :   (%3.4f, %3.4f)", blob->state.localX, blob->state.localY);
      glColor3f(0.0F,1.0F,0.0F);
      glRasterPos2f ( XMAX-165, YMAX-BORDER_HEIGHT/2-15);
      drawString (state_label);       
  
      sprintf(title_label, "ObsPERCEPTOR VIEWER"); 
      glColor3f(0.0F,1.0F,0.0F);
      glRasterPos2f ( -50, YMAX-BORDER_HEIGHT/2);
      drawString (title_label); 

      flag = 0;
      mess_recvd = 0;

      //============STOP MUTEX LOCK==============
      pthread_mutex_unlock(&mutex1);
    }
  glutSwapBuffers();

}


void resize(int w, int h)
{
  //if (height == 0) height = 1;
  flag = 1;
  glLoadIdentity();
  glViewport (0, 0, w, h); //set the viewport to the current window specifications
  glMatrixMode(GL_PROJECTION);
  gluOrtho2D(XMIN,XMAX,YMIN,YMAX);
}


void *Read_message( void* ptr)
{

  ElevationData tempData;  

  while(true)
    {   
      //=========START MUTEX LOCK=========
      pthread_mutex_lock(&mutex1);
      
      int blobId, blobLen;
      
      // Look at the latest data
      if (sensnet_peek(sensnet, SENSNET_SKYNET_SENSOR, SNfusiondeltamap, &blobId, &blobLen) != 0)
	{	 
	  pthread_mutex_unlock(&mutex1);
	  continue;
	}
      
      // Is the data valid?
      if (blobId < 0)
	{	  
	  pthread_mutex_unlock(&mutex1);
	  continue;
	}
      
      
      //read the data into blob
      //do we need to clear old blob? let's go ahead and do that for now
      memset(blob, 0, sizeof(*blob)); 
      if (sensnet_read(sensnet, SENSNET_SKYNET_SENSOR, SNfusiondeltamap, -1, sizeof(*blob), blob) != 0)
	{	
	  pthread_mutex_unlock(&mutex1);
	  continue;
	}           
      
      //update blobMap
      obsmap->updateVehicleLoc(blob->state.localX,blob->state.localY);
      for(int i=0; i< blob->numDeltas; i++)
	{
	  tempData = blob->mapDeltaVec[i];
	  
	  // TESTING
	  obsmap->setDataUTM<double>(obsmapID_meanElevation, tempData.UTMPoint.N, tempData.UTMPoint.E, tempData.meanElevation);
	  obsmap->setDataUTM<double>(obsmapID_stdDev, tempData.UTMPoint.N, tempData.UTMPoint.E, tempData.stdDev);
	  obsmap->setDataUTM<CELL_TYPE>(obsmapID_cellType, tempData.UTMPoint.N, tempData.UTMPoint.E, tempData.cellType);
	}       
      
      flag = 1;
      mess_recvd = 1;      
      
      //=========STOP MUTEX LOCK=========
      pthread_mutex_unlock(&mutex1);
            
      usleep(500);
      
    }
}


void idle (void) 
{
  /* Check flag variable */ 
  //============START MUTEX LOCK============= 
  pthread_mutex_lock(&mutex1);

  if(flag==1 || mess_recvd==1)
    {
	glutPostRedisplay();
    }

  //============STOP MUTEX LOCK============= 
  pthread_mutex_unlock(&mutex1);

}




void mouse(int button, int state, int x, int y)
{

  if(button == GLUT_LEFT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  lbuttondown = true;
	}
      else
	{
	  lbuttondown = false;
	}
    }

  if(button == GLUT_RIGHT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  rbuttondown = true;
	}
      else
	{
	  rbuttondown = false;
	}
    }
  

}



void motion(int x, int y)
{
  if (lbuttondown)
    {
      float diffx=(x-lastx)/10; //check the difference between the current x and the last x position  
      float diffy=(y-lasty)/10;

      lastx = x;
      lasty = y;

      glTranslatef(diffx, -diffy, 0);

      glutPostRedisplay();     
    }

  if (rbuttondown)
    {     

      GLint _viewport[4];
      GLdouble _modelMatrix[16];
      GLdouble _projMatrix[16];
      GLdouble objx, objy, objz;

      glGetIntegerv(GL_VIEWPORT,_viewport);
      glGetDoublev(GL_PROJECTION_MATRIX, _projMatrix);
      glGetDoublev(GL_MODELVIEW_MATRIX, _modelMatrix);

      gluUnProject(WINDOW_WIDTH/2,WINDOW_HEIGHT/2,0,_modelMatrix,_projMatrix,_viewport,&objx, &objy, &objz);

      float dy=(y-lasty); //check the difference between the current y and the last y position  

      double s = exp((double)dy*0.01);

      glTranslatef(objx,objy,0);
      glScalef(s,s,s);
      glTranslatef(-objx,-objy,0);

      lasty = y;
      lastx = x;

      glutPostRedisplay();  
    }
  
}

void motionPassive(int x, int y)
{
  lastx = x;
  lasty = y;
}


int main(int argc, char **argv)
{

  if(init()!=0)
    return(-1);
  
  int iret1;
  pthread_t thread1;
  
  iret1 = pthread_create( &thread1, NULL, Read_message, NULL);
  
  
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowPosition(40, 40);
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutCreateWindow("OBSVIEWER");
  
  glClearColor(0.0, 0.0, 0.0, 0.0); //black bkgrnd
  glColor3f(1.0, 1.0, 1.0);
  
  /* set callback functions */
  glutDisplayFunc(display); 
  
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutPassiveMotionFunc(motionPassive);

  glutReshapeFunc(resize);
  glutIdleFunc(idle);
  
  glutMainLoop();
  
  return 0;
}
