#include "CElevationFuser.hh"

CElevationFuser::CElevationFuser() {
  _data.meanElevation = 0;
  _data.meanSquaredElevation = 0;
  _data.numPoints = 0;
  _data.variance = 0.0;
  _data.stdDev = 0.0;
  _data.cellType = EMPTY;
}


CElevationFuser::~CElevationFuser() {
}

CElevationFuser::STATUS CElevationFuser::fuse_KFElevation(NEDcoord otherPoint, double measHeightVar) {
  //STATUS fuseStatus;
  switch(_data.cellType) 
    {
    case EMPTY:
      _data.meanElevation = otherPoint.D;
      _data.variance = measHeightVar;
      _data.numPoints = 1;
      _data.cellType = DATA;
      return GOOD;
      break;
    case DATA:
      //don't care about meanSquaredElevation or stdDev;
      //only care about meanElevation (which by abuse of notation is the KFelevation)
      _data.meanElevation = (measHeightVar*_data.meanElevation + _data.variance*otherPoint.D)/(_data.variance + measHeightVar);
      _data.variance = _data.variance*measHeightVar/(_data.variance + measHeightVar);
      _data.numPoints++;
      return GOOD;
      break;
    case OUTSIDE_MAP:
      return ERROR;
    default:
      return ERROR;
      break;
    }
}

// Simplest fusion possible - unweighted averaging
CElevationFuser::STATUS CElevationFuser::fuse_MeanElevation(NEDcoord otherPoint) {
  switch(_data.cellType) 
    {
    case EMPTY:
      _data.meanElevation = otherPoint.D;
      _data.meanSquaredElevation = pow(otherPoint.D,2);
      _data.numPoints = 1;
      _data.cellType = DATA;
      return GOOD;
      break;
    case DATA:
      _data.meanElevation = (_data.numPoints*_data.meanElevation + otherPoint.D)/(_data.numPoints+1);
      _data.meanSquaredElevation = (_data.numPoints*_data.meanSquaredElevation + pow(otherPoint.D, 2))/(_data.numPoints + 1);
      _data.numPoints++;
      _data.variance = _data.meanSquaredElevation - pow(_data.meanElevation,2);
      _data.stdDev = sqrt( _data.variance );
      return GOOD;
    case OUTSIDE_MAP:
      return ERROR;
    default:
      return ERROR;
      break;
    }
}


CElevationFuser::STATUS CElevationFuser::setOutsideMap() {
  _data.cellType = OUTSIDE_MAP;
  return GOOD;
}

CElevationFuser::STATUS CElevationFuser::resetNoData() {
  _data.cellType = EMPTY;
  _data.meanElevation = 0;
  _data.meanSquaredElevation = 0;
  _data.numPoints = 0;
  _data.variance = 0.0;
  _data.stdDev = 0.0;
  return GOOD;
}

CElevationFuser::STATUS CElevationFuser::setMeanElevation(double elev) {
  _data.meanElevation = elev;
  return GOOD;
}

CElevationFuser::ElevationFuserData CElevationFuser::getData() const{
  return _data;
}

double CElevationFuser::getMeanElevation() const {
  return _data.meanElevation;
}

double CElevationFuser::getMeanSquaredElevation() const {
  return _data.meanSquaredElevation;
}

int CElevationFuser::getNumPoints() const {
  return _data.numPoints;
}

double CElevationFuser::getVariance() const {
  return _data.variance;
}

double CElevationFuser::getStdDev() const {
  return _data.stdDev;
}

CElevationFuser::CELL_TYPE CElevationFuser::getCellType() const {
  return _data.cellType;
}

bool CElevationFuser::operator== (const CElevationFuser other) const {
  if(_data.cellType!=other.getCellType()) 
    {
      return false;
    } 
  else if(_data.cellType!=DATA) 
    {
      return true;
    }

  return ((_data.meanElevation == other.getMeanElevation()) &&
	  (_data.meanSquaredElevation == other.getMeanSquaredElevation()) &&
	  (_data.numPoints == other.getNumPoints()));
}


bool CElevationFuser::operator!= (const CElevationFuser other) const {
  return !(*this == other);
}


CElevationFuser& CElevationFuser::operator= (const CElevationFuser& other) {
  if(this != &other) 
    {
      _data = other.getData();
    }
  
  return *this;
}


istream& operator>> (istream& is, CElevationFuser& destination) {
  double meanElevation, meanSquaredElevation;
  double variance, stdDev;
  int numPoints, cellType;
  
  is >> meanElevation >> meanSquaredElevation 
     >> numPoints >> variance >> stdDev
     >> cellType;
  
  destination._data.meanElevation = meanElevation;
  destination._data.meanSquaredElevation = meanSquaredElevation;
  destination._data.numPoints = numPoints;
  destination._data.variance = variance;
  destination._data.stdDev = stdDev;
  destination._data.cellType = (CElevationFuser::CELL_TYPE) cellType;
  return is;
}


ostream& operator<< (ostream& os, const CElevationFuser& source) {
  os << setprecision(10) 
     << "_data.meanElevation=" << source.getMeanElevation()
     << ", _data.meanSquaredElevation=" << source.getMeanSquaredElevation()
     << ", _data.numPoints=" << source.getNumPoints()
     << ", _data.variance=" << source.getVariance()
     << ", _data.stdDev=" << source.getStdDev()
     << ", _data.cellType=" << (int)source.getCellType();
   
    return os;
}


