
#ifndef __INTERPOLATION_MAP_HH__
#define __INTERPOLATION_MAP_HH__

#include <list>
#include <iterator>
#include <utility>
#include <gtkmm.h>


template< typename Indep_T, typename Dep_T >
class InterpolationMap
{
protected:
  class uPair : public std::pair<Indep_T, Dep_T>
  { 
  public:
    uPair(const Indep_T &i, const Dep_T & d)
    {
      this->first  = i;
      this->second = d;
    }
    uPair operator = (const uPair & rhs)
    {
      return uPair(rhs.first, rhs.second);
    }
    bool operator < (const uPair & rhs)
    {
      return this->first < rhs.first;
    }
  };

  typedef typename std::list<uPair>::iterator ListPairIterator;
  std::list<uPair> m_data_set;

  Glib::RecMutex   m_mutex;

public:

  InterpolationMap() {}
  InterpolationMap(InterpolationMap & im)
  {
    Glib::RecMutex::Lock lock(im.m_mutex);
    m_data_set = im.m_data_set; 

  }
  ~InterpolationMap() {}

  void add_entry(const Indep_T& it, const Dep_T& dt) 
  {
    Glib::RecMutex::Lock lock(m_mutex);

    m_data_set.push_back(uPair( it, dt ));
    m_data_set.sort();
  }

  Dep_T get_value(const Indep_T & luv)
  {
    Glib::RecMutex::Lock lock(m_mutex);

    // No data in table -- return default type
    if ( m_data_set.size() == 0 ) return Dep_T();

    ListPairIterator last_less_than = m_data_set.begin();
    ListPairIterator x;
    for( x=m_data_set.begin(); x != m_data_set.end(); x++ ) {
      if( x->first < luv )
	{
	  last_less_than = x;
	}
      else 
	{
	  // found one larger
	  double tot_dist = (double) x->first - (double) last_less_than->first;
	  if( tot_dist <= 0 ) return Interpolate( last_less_than->second, x->second, 0.5 );
	  else 
	    {
	      double first_contribution = 1.0 - ((double) luv - (double) last_less_than->first) / tot_dist;
	      return Interpolate( last_less_than->second, x->second, first_contribution );
	    }
	}

    }

    // luv is larger than any in table, just return highest value
    return last_less_than->second;
  }

};




#endif
