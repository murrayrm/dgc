#ifndef __RECTANGLE_HH__
#define __RECTANGLE_HH__

#include <iostream>
#include <iomanip>

template <class T>
struct POINT2D
{
  T x, y;

  POINT2D()
    : x(0), y(0)
  {
  }
  POINT2D(T x, T y)
    : x(x), y(y)
  {}
  
  template <class T2>
  POINT2D( const POINT2D<T2> & rhs )
    : x( (T) rhs.x ), y( (T) rhs.y )
  {
  }

  POINT2D<T> operator + (const POINT2D<T> & rhs) const
  {
    return POINT2D( x + rhs.x, y + rhs.y ); 
  }

  template <class T2>
  POINT2D<T> operator * (const T2 & rhs ) const 
  {
    return POINT2D( x * rhs, y * rhs ); 
  }


};

template<class T>
struct RECTANGLE
{
  T left, right, bottom, top;

  RECTANGLE()
    : left(0), right(0), bottom(0), top(0)
  {
  }
  RECTANGLE(T left, T right, T bottom, T top)
    : left(left), right(right), bottom(bottom), top(top)
  {
  }

  RECTANGLE(T all_dims)
    : left(all_dims), right(all_dims), bottom(all_dims), top(all_dims)
  {

  }

  // change type to another type of rectangle
  template <class T2>
  RECTANGLE(const RECTANGLE<T2> & rhs)
  : left( (T) rhs.left), right( (T) rhs.right), 
    bottom( (T) rhs.bottom ), top( (T) rhs.top)
  {
  }

  T width() const
  {
    return right-left;
  }
  T height() const 
  {
    return top-bottom;
  }
  T center_x() const
  {
    return (T) ((double) (left+right) / 2.0 );
  } 
  T center_y() const 
  {
    return (T) ( (double) (top+bottom) / 2.0 );
  }

  POINT2D<T> center() const
  {
    return POINT2D<T> (center_x(), center_y());
  }
  T area() const 
  {
    return width() * height();
  }
  double aspect_ratio() const
  {
    return ((double)(right-left)) / ((double)(top-bottom));
  }

  RECTANGLE center_scale( double scale ) const
  {
    return RECTANGLE ( (T) (center_x() - scale * (double) width()  / 2.0),
		       (T) (center_x() + scale * (double) width()  / 2.0),
		       (T) (center_y() - scale * (double) height() / 2.0),
		       (T) (center_y() + scale * (double) height() / 2.0) );
  }

  RECTANGLE fit_aspect_ratio_inside(double ratio) const
  {
 
    if( ratio < aspect_ratio() )
      {
	// height is limiting factor
	double lr_offset = ratio * height() / 2.0;
	return RECTANGLE( (T) ((double)center_x() - lr_offset),
			  (T) ((double)center_x() + lr_offset),
			  (T) this->bottom,
			  (T) this->top );
      }
    else 
      {
	// width is limiting factor
	double tb_offset = width() / ratio / 2.0;
	return RECTANGLE( (T) this->left,
			  (T) this->right,
			  (T) ((double)center_y() - tb_offset),
			  (T) ((double)center_y() + tb_offset) );
      }
  }
  RECTANGLE intersection( const RECTANGLE & rhs ) const
  {
    return RECTANGLE<T> ( max(rhs.left,   this->left),
			  min(rhs.right,  this->right),
			  max(rhs.bottom, this->bottom),
			  min(rhs.top,    this->top) );
  }

  static RECTANGLE centered_at(const POINT2D<T> & center, T width, T height) 
  {
    return RECTANGLE<T>( (T) ((double)center.x - (double)width/2.0),  (T) ((double)center.x + (double)width/2.0),
			 (T) ((double)center.y - (double)height/2.0), (T) ((double)center.y + (double)height/2.0) );    
  }

};

template <class T>
std::ostream & operator << (std::ostream & c, const RECTANGLE<T> &rhs)
{
  c << setiosflags(ios::showpoint)
    << setiosflags(ios::fixed)
    << setprecision(8) 
    << "Left = " << setw(10) << rhs.left << ", Right = " << setw(10) << rhs.right
    << ", Bottom = " << setw(10) << rhs.bottom << ", Top = " << setw(10) << rhs.top
    << ", Width = " << setw(10) << rhs.width() << ", Height = " << setw(10) << rhs.height();

  return c;
}

template <class T1, class T2>
class RECTANGLE_MAP
{
private:
  RECTANGLE<double> from;
  RECTANGLE<double> to;
public:
  RECTANGLE_MAP( const RECTANGLE<T1> & from, const RECTANGLE<T2> & to )
    : from(from), to(to)
  {}

  POINT2D<T2> map_point( const POINT2D<T1> & input )
  {
    return POINT2D<T2> ( (T2) (to.left   + (input.x - from.left)   / (from.right - from.left)   * to.width()),
			 (T2) (to.bottom + (input.y - from.bottom) / (from.top   - from.bottom) * to.height()) );			 
  }
  POINT2D<T1> reverse_map_point( const POINT2D<T2> & input )
  {
    return POINT2D<T1> ( (T1) (from.left   + (input.x - to.left)   / (to.right - to.left)   * from.width()),
			 (T1) (from.bottom + (input.y - to.bottom) / (to.top   - to.bottom) * from.height()) );			 
  }


  RECTANGLE<T2> map_rectangle( const RECTANGLE<T1> input )
  {
    POINT2D<double> bottom_left = map_point( POINT2D<double> ( (double) input.left,  (double) input.bottom) );
    POINT2D<double> top_right   = map_point( POINT2D<double> ( (double) input.right, (double) input.top) );

    return RECTANGLE<T2>( (T2) bottom_left.x, (T2) top_right.x, (T2) bottom_left.y, (T2) top_right.y ); 
  }

};

#endif
