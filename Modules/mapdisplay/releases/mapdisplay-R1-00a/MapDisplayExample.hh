#ifndef __MAP_DISPLAY_EXAMPLE_HH__
#define __MAP_DISPLAY_EXAMPLE_HH__

// Pretend we are the FusionMapper

#include <time.h>
#include <unistd.h>
#include <string>
#include <strstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <algorithm>  

class MapDisplayThread; // Forward declaration

#include "Constants.h" //vehicle constants

// Include the Map class here.
#include "CMap/CMapPlus.hh"
//Include the RDDF class
#include "rddf.hh"

using namespace std;

// All data that needs to be shared by common threads
struct MapDisplayExampleDatum 
{
  VehicleState SS;
  
  CMapPlus my_cmap;

  // !!!!!!!!!!!!!!!!
  // A pointer to a GTK MapDisplayThread class
  MapDisplayThread * mdt;

};

class MapDisplayExample
{
public:
  // and change the constructors
  MapDisplayExample();
  ~MapDisplayExample();

  // The states in the state machine.
  // Uncomment these if you wish to define these functions.
  void Init();

  void Active();

private:

  /*! The datum named d. */
  MapDisplayExampleDatum d;

};









#endif
