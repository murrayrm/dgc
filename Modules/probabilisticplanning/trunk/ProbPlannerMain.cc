#include <getopt.h>
#include "cmdline.h"
#include <iostream>
#include "ProbPlanner.hh"
#include "dgcutils/DGCutils.hh"
#include "skynet/skynet.hh"
#include "iostream"
      
using namespace std;             
     
//run 
//1. ./asim --rndf=
//2. ./ProbPlanner --rndf=...
//3. ./mapviewer --recv-subgroup=20 (or whatever maprediction is sending over

int main(int argc, char **argv)              
{
  gengetopt_args_info cmdline;
  

  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
   
  // Figure out what skynet key to use
  int sn_key = skynet_findkey(argc, argv);
  
  int debugLevel, verboseLevel;
  debugLevel = cmdline.debug_arg;
  verboseLevel = cmdline.verbose_arg; 
 
  // Initialize Traffic Planner Class
  CProbPlanner* pProbPlanner = new CProbPlanner(sn_key, !cmdline.nowait_given,
                                                         debugLevel, verboseLevel, 
                                                         cmdline.log_flag);

  // Initialize the map with rndf if given
  if (cmdline.rndf_given){
    string RNDFfilename = cmdline.rndf_arg;
    cout << "RNDF Filename in = "  << RNDFfilename << endl;
    if (!pProbPlanner->loadRNDF(RNDFfilename)){
      return 0; 
    }  
		 
  }     

   DGCstartMemberFunctionThread(pProbPlanner, &CProbPlanner::getLocalMapThread);
      
  pProbPlanner->ProbPlannerLoop();
  return 0;
}

