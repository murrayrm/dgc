/* ProbPlanner.cc
 * Author: Pete Trautman
 * Last revision: May 1 2007
 * */

#include "ProbPlanner.hh"

using namespace std;

CProbPlanner::CProbPlanner(int skynetKey, bool bWaitForStateFill,
                                 int debugLevel, int verboseLevel, 
                                 bool log)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = skynetKey;
  if (debugLevel>0){
    m_debug = true;
    cout << "debug is on" << endl;
  }
  else m_debug = false;
  if (verboseLevel>0) {
    m_verbose = true;
    cout << "verbose is on" << endl;
  }
  else m_verbose = false;
  
  m_log = log;
  if (m_log) 
    cout << "logging is on" << endl;


  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions
 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey);
  initSendMapElement(skynetKey);
  if(!m_recvLocalMap){
    //bool loadMapFromRNDF =  loadRNDF(string filename) {return localmap.loadRNDF(filename);
  }
  m_localMap = new Map();
}

CProbPlanner::~CProbPlanner() 
{
  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}


void CProbPlanner::getLocalMapThread()
{

  MapElement recvEl;
  int bytesRecv;
  while (true){
  bytesRecv = recvMapElementBlock(&recvEl,1);
 
  if (bytesRecv>0){
      DGClockMutex(&m_LocalMapMutex);
    m_localMap->addEl(recvEl);
    DGCunlockMutex(&m_LocalMapMutex);
  }else {
    cout << "Error in CMapPrediction::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
    usleep(100);
  }
}
 
}

void CProbPlanner::ProbPlannerLoop(void)
{
  //=====================================================
  UpdateState(); // this gives m_state
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;         
  //======================================================
   
   MapElement recvEl;
   MapElement tempEl;
   int sendChannel=0;

  while(1){
  DGClockMutex(&m_LocalMapMutex);

  for (unsigned i =0; i< m_localMap->data.size(); ++i)
    {    
     sendMapElement(&m_localMap->data[i], sendChannel);
    }

  for (unsigned i =0; i< m_localMap->prior.data.size(); ++i)
    {
      m_localMap->prior.getEl(tempEl,i);
      sendMapElement(&tempEl, sendChannel);
    }

  DGCunlockMutex(&m_LocalMapMutex);

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 //here's where the consequential stuff begins
   
   MapElement dob1; //dynamic obstacle 1
   MapElement alice; //alice
   MapElement lineBoundary; //line boundaries functioning as measurements
   int horizon=500; //horizon of prediction
   double range=10;

   LaneLabel dob1label(1,1); 
   LaneLabel alabel(1,1); 
   point2 dob1size(2,5);//good size for block cars
   point2 asize(2,5);//good size for block cars

   //initialization of the obstacles; this should be read in from mapper in the future
   point2 dob1cpt(0,-30);//initial position straight down
   //point2 dob1cpt(30,-72);//initial position on turn
   //point2 dob1cpt(0,8);//initial position bottom
   point2 vel(0.1,-2);//initial velocity straight down
   //point2 vel(5,0);//initial velocity turn
   
   ////////alice
   point2 acpt(0,-10);//initial position straight down
   point2 avel(0.1,-10);
   
   int N = 50; //number of particles
   double sigma=0.5; //initial covariance in the samples
   double resampleFactor = 0.9; //Neff <= resampleFactor*N
   int modelType=1;
   int partitions = 2;
   point2 noise(0,0);

   vector<double> ang(N);
   point2arr particlesPlus;
   point2arr particlesNow;
   point2arr velNow;

   vector<double> aang(N);
   point2arr aparticlesPlus;
   point2arr aparticlesNow;
   point2arr avelNow;

   ParticleFilter dob1PF(resampleFactor,N,dob1cpt,vel,modelType,sigma);   
   ParticleFilter aliceSim(resampleFactor,N,acpt,avel,modelType,sigma);

   velNow = dob1PF.getVelNow();
   particlesNow=dob1PF.getParticlesNow();

   avelNow = aliceSim.getVelNow();
   aparticlesNow=aliceSim.getParticlesNow();

   point2 sampleFactor(0.5,0.2); //noise in sampling distribution  
   point2 asampleFactor(0.3,0.2); //noise in sampling distribution 
   double deltaT=0.1;	
   long long int start,end;
   
   point2arr dob1History; 
   point2arr aHistory;
   vector<double> angHistory; 
   vector<double> aangHistory;
  //MAIN PREDICTION LOOP           
  for (int i =0; i< horizon; ++i)
   { 
      ang = dob1PF.getang();
      aang = aliceSim.getang();
      //cout<<"ang"<<ang<<endl;

      double angAve = 0, aangAve = 0; 
      point2 particlesAve(0,0);
      point2 aparticlesAve(0,0);
      
      for(int k=0; k<N;k++)
       {   
        particlesAve = particlesAve + particlesNow[k];
        angAve = angAve + ang[k];  
        aparticlesAve = aparticlesAve + aparticlesNow[k];
        aangAve = aangAve + aang[k];    
       } 
          
      particlesAve = particlesAve/N;
      angAve = angAve/N;
      aparticlesAve = aparticlesAve/N;
      aangAve = aangAve/N;
      
      dob1History.push_back(particlesAve);
      aHistory.push_back(aparticlesAve);
      angHistory.push_back(angAve);
      aangHistory.push_back(aangAve);
/*      
      //plotting mean of the particles part
      dob1.set_block_obs(iddob1,particlesAve,angAve,dob1size.x,dob1size.y); 
      dob1.plot_color=MAP_COLOR_RED;
      sendMapElement(&dob1,sendChannel); 
                 
      alice.set_block_obs(ida,aparticlesAve,aangAve,asize.x,asize.y); 
      alice.plot_color=MAP_COLOR_GREEN;
      sendMapElement(&alice,sendChannel); 
      //end plotting mean part
*/ 
     /////////////here is plot ALL the particles
  
     for(int k=0; k<N;k++)
      {   
        dob1.id=k;
        dob1.setTypeVehicle();

        dob1.setColor(MAP_COLOR_RED,100);
        dob1.setGeometry(particlesNow[k], dob1size.x, dob1size.y, ang[k]);
        sendMapElement(&dob1,sendChannel);
       
      }

     for(int k=0; k<N;k++)
      {   
        alice.id=k+2*N;
        alice.setTypeVehicle();

        alice.setColor(MAP_COLOR_GREEN,100);
        alice.setGeometry(aparticlesNow[k], asize.x, asize.y, aang[k]);
        sendMapElement(&alice,sendChannel);
        
      }  
//////here is the end of plot ALL the particles  

     //begin particle filter
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
   
   start=DGCgettime();
   
   particlesPlus=dob1PF.sampleConstVel(sampleFactor,deltaT);//predicts Now to Plus
   //particlesPlus=dob1PF.sampleCoordTurn(sampleFactor,deltaT);
   
   vector<point2arr> lbound(N),rbound(N); //measurement vector
   for(int k=0; k<N;k++)
    {//measurement collection for each particle
     m_localMap->getBounds(lbound[k],rbound[k],dob1label,particlesPlus[k],range);
    }
    
    vector<point2arr> sensed_data;
    
    dob1PF.weight(lbound,rbound,sensed_data);
    
    dob1PF.resample(partitions);
    
    particlesNow=dob1PF.getParticlesNow();
    velNow=dob1PF.getVelNow();
    //cout<<"velNow"<<velNow<<endl;
    
    //end PF
    
    end = DGCgettime();
    deltaT = (end-start)/100000.0;

//$$$$$$$$$$$$$$$$$$$begin alice PF

   start=DGCgettime();
   
   aparticlesPlus=aliceSim.sampleConstVel(asampleFactor,deltaT);//predicts Now to Plus
   //particlesPlus=dob1PF.sampleCoordTurn(asampleFactor,deltaT);
   
   vector<point2arr> albound(N),arbound(N); //measurement vector

   for(int k=0; k<N;k++)
    {//measurement collection for each particle
     m_localMap->getBounds(albound[k],arbound[k],alabel,aparticlesPlus[k],range);
    }
    
     vector<point2arr> asensed_data;
    
    aliceSim.plan(albound,arbound,asensed_data,particlesNow);
    
    aliceSim.resample(partitions);
    
    aparticlesNow=aliceSim.getParticlesNow();
    avelNow=aliceSim.getVelNow();
    //cout<<"velNow"<<avelNow<<endl;
    
    //end PF
    
    end = DGCgettime();
    deltaT = (end-start)/100000.0;    
 
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    //alice's state
    UpdateState(); //updates m_state
    alice.set_alice(m_state);
    sendMapElement(&alice,sendChannel);
    //end alice's state    

    usleep(100000);
    }
  }
}

