/* ProbPlanner.cc
 * Author: Pete Trautman
 * Last revision: May 1 2007
 * */

#include "ProbPlanner.hh"

using namespace std;

CProbPlanner::CProbPlanner(int skynetKey, bool bWaitForStateFill,
                                 int debugLevel, int verboseLevel, 
                                 bool log)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = skynetKey;
  if (debugLevel>0){
    m_debug = true;
    cout << "debug is on" << endl;
  }
  else m_debug = false;
  if (verboseLevel>0) {
    m_verbose = true;
    cout << "verbose is on" << endl;
  }
  else m_verbose = false;
  
  m_log = log;
  if (m_log) 
    cout << "logging is on" << endl;


  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions
 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey);
  initSendMapElement(skynetKey);
  if(!m_recvLocalMap){
    //bool loadMapFromRNDF =  loadRNDF(string filename) {return localmap.loadRNDF(filename);
  }
  m_localMap = new Map();
}

CProbPlanner::~CProbPlanner() 
{
  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}


void CProbPlanner::getLocalMapThread()
{

  MapElement recvEl;
  int bytesRecv;
  while (true){
  bytesRecv = recvMapElementBlock(&recvEl,1);
 
  if (bytesRecv>0){
      DGClockMutex(&m_LocalMapMutex);
    m_localMap->addEl(recvEl);
    DGCunlockMutex(&m_LocalMapMutex);
  }else {
    cout << "Error in CMapPrediction::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
    usleep(100);
  }
}
 
}

void CProbPlanner::ProbPlannerLoop(void)
{
  //=====================================================
  UpdateState(); // this gives m_state
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;         
  //======================================================
   
   MapElement recvEl;
   MapElement tempEl;
   int sendChannel=0;

  while(1){
  DGClockMutex(&m_LocalMapMutex);

  for (unsigned i =0; i< m_localMap->data.size(); ++i)
    {    
     sendMapElement(&m_localMap->data[i], sendChannel);
    }

  for (unsigned i =0; i< m_localMap->prior.data.size(); ++i)
    {
      m_localMap->prior.getEl(i,tempEl);
      sendMapElement(&tempEl, sendChannel);
    }

  DGCunlockMutex(&m_LocalMapMutex);

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 //here's where the consequential stuff begins
   
   MapElement dob1; //dynamic obstacle 1
   MapElement alice; //alice
   MapElement lineBoundary; //line boundaries functioning as measurements
   int horizon=500; //horizon of prediction
   double range=10;

   LaneLabel dob1label(1,1); 
   LaneLabel alabel(1,1); 
   point2 dob1size(2,5);//good size for block cars
   point2 asize(2,5);//good size for block cars

   //id identifiers for my obstacles
   vector<int> iddob1; 
   vector<int> idalice;
   vector<int> ida;
   iddob1.push_back(1); 
   idalice.push_back(0);
   ida.push_back(2);
 
   //initialization of the obstacles; this should be read in from mapper in the future
   point2 dob1cpt(0,-30);//initial position straight down
   //point2 dob1cpt(30,-72);//initial position on turn
   //point2 dob1cpt(0,8);//initial position bottom
   point2 vel(0.1,-2);//initial velocity straight down
   //point2 vel(5,0);//initial velocity turn
   
   ////////alice
   point2 acpt(0,-10);//initial position straight down
   point2 avel(0.1,-10);
   
   int N = 50; //number of particles
   double sigma=0.5; //initial covariance in the samples
   double resampleFactor = 0.9; //Neff <= resampleFactor*N
   int modelType=1;
   int partitions = 2;
   point2 noise(0,0);

   vector<double> ang(N);
   point2arr particlesPlus;
   point2arr particlesNow;
   point2arr velNow;

   vector<double> aang(N);
   point2arr aparticlesPlus;
   point2arr aparticlesNow;
   point2arr avelNow;

   ParticleFilter dob1PF(resampleFactor,N,dob1cpt,vel,modelType,sigma);   
   ParticleFilter aliceSim(resampleFactor,N,acpt,avel,modelType,sigma);

   velNow = dob1PF.getVelNow();
   particlesNow=dob1PF.getParticlesNow();

   avelNow = aliceSim.getVelNow();
   aparticlesNow=aliceSim.getParticlesNow();

   point2 sampleFactor(0.5,0.2); //noise in sampling distribution  
   point2 asampleFactor(0.3,0.2); //noise in sampling distribution 
   double deltaT=0.1;	
   long long int start,end;
   
   point2arr dob1History; 
   point2arr aHistory;
   vector<double> angHistory; 
   vector<double> aangHistory;
  //MAIN PREDICTION LOOP           
  for (int i =0; i< horizon; ++i)
   { 
      ang = dob1PF.getang();
      aang = aliceSim.getang();
      //cout<<"ang"<<ang<<endl;

      double angAve = 0, aangAve = 0; 
      point2 particlesAve(0,0);
      point2 aparticlesAve(0,0);
      
      for(int k=0; k<N;k++)
       {   
        particlesAve = particlesAve + particlesNow[k];
        angAve = angAve + ang[k];  
        aparticlesAve = aparticlesAve + aparticlesNow[k];
        aangAve = aangAve + aang[k];    
       } 
          
      particlesAve = particlesAve/N;
      angAve = angAve/N;
      aparticlesAve = aparticlesAve/N;
      aangAve = aangAve/N;
      
      dob1History.push_back(particlesAve);
      aHistory.push_back(aparticlesAve);
      angHistory.push_back(angAve);
      aangHistory.push_back(aangAve);
/*      
      //plotting mean of the particles part
      dob1.set_block_obs(iddob1,particlesAve,angAve,dob1size.x,dob1size.y); 
      dob1.plot_color=MAP_COLOR_RED;
      sendMapElement(&dob1,sendChannel); 
                 
      alice.set_block_obs(ida,aparticlesAve,aangAve,asize.x,asize.y); 
      alice.plot_color=MAP_COLOR_GREEN;
      sendMapElement(&alice,sendChannel); 
      //end plotting mean part
*/ 
     /////////////here is plot ALL the particles
  
     for(int k=0; k<N;k++)
      {   
       dob1.set_block_obs(iddob1,particlesNow[k],ang[k],dob1size.x,dob1size.y);
       dob1.plot_color=MAP_COLOR_RED;
       sendMapElement(&dob1,sendChannel); 
       iddob1.clear();
       iddob1.push_back(k);
      }
/*
     for(int k=0; k<N;k++)
      {   
       alice.set_block_obs(ida,aparticlesNow[k],aang[k],asize.x,asize.y);
       alice.plot_color=MAP_COLOR_GREEN;
       sendMapElement(&alice,sendChannel); 
       ida.clear();
       ida.push_back(k);
      }  
//////here is the end of plot ALL the particles
*/
     for(int k=0; k<i;k++)
      {   
       alice.set_block_obs(ida,aHistory[k],aangHistory[k],asize.x,asize.y);
       alice.plot_color=MAP_COLOR_MAGENTA;
       sendMapElement(&alice,sendChannel); 
       ida.clear();
       ida.push_back(k);
      }  
  

     //begin particle filter
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
   
   start=DGCgettime();
   
   particlesPlus=dob1PF.sampleConstVel(sampleFactor,deltaT);//predicts Now to Plus
   //particlesPlus=dob1PF.sampleCoordTurn(sampleFactor,deltaT);
   
   vector<point2arr> lbound(N),rbound(N); //measurement vector
   for(int k=0; k<N;k++)
    {//measurement collection for each particle
     m_localMap->getBounds(lbound[k],rbound[k],dob1label,particlesPlus[k],range);
    }
    
    vector<point2arr> sensed_data;
    
    dob1PF.weight(lbound,rbound,sensed_data);
    
    dob1PF.resample(partitions);
    
    particlesNow=dob1PF.getParticlesNow();
    velNow=dob1PF.getVelNow();
    //cout<<"velNow"<<velNow<<endl;
    
    //end PF
    
    end = DGCgettime();
    deltaT = (end-start)/100000.0;

//$$$$$$$$$$$$$$$$$$$begin alice PF

   start=DGCgettime();
   
   aparticlesPlus=aliceSim.sampleConstVel(asampleFactor,deltaT);//predicts Now to Plus
   //particlesPlus=dob1PF.sampleCoordTurn(asampleFactor,deltaT);
   
   vector<point2arr> albound(N),arbound(N); //measurement vector
   for(int k=0; k<N;k++)
    {//measurement collection for each particle
     m_localMap->getBounds(albound[k],arbound[k],alabel,aparticlesPlus[k],range);
    }
    
      vector<point2arr> asensed_data;
    
    aliceSim.plan(albound,arbound,asensed_data,particlesNow);
    
    aliceSim.resample(partitions);
    
    aparticlesNow=aliceSim.getParticlesNow();
    avelNow=aliceSim.getVelNow();
    //cout<<"velNow"<<avelNow<<endl;
    
    //end PF
    
    end = DGCgettime();
    deltaT = (end-start)/100000.0;    
 
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    //alice's state
    UpdateState(); //updates m_state
    alice.set_alice(m_state);
    sendMapElement(&alice,sendChannel);
    //end alice's state    

    lineBoundary.clear();
    lineBoundary.set_id(1,2,-4);
    lineBoundary.set_laneline();
    lineBoundary.set_geometry(lbound[0]);
    lineBoundary.plot_color=MAP_COLOR_GREEN;
    sendMapElement(&lineBoundary,sendChannel);

    lineBoundary.clear();
    lineBoundary.set_id(1,2,-5);
    lineBoundary.set_laneline();
    lineBoundary.set_geometry(rbound[0]);
    lineBoundary.plot_color=MAP_COLOR_MAGENTA;
    sendMapElement(&lineBoundary,sendChannel);
    
    usleep(100000);
    }
  }
}








//fun stuff

//calculating the "measurement"
    //point2 cptave(0,0);
    //for(int k=0; k<N;k++)
    //  {
    //   cptave.x = cptave.x + particlesPlus[k].x;
    //   cptave.y = cptave.y + particlesPlus[k].y;
    //  }
    //    cptave.x=cptave.x/N;
    //	cptave.y=cptave.y/N;
   
//point2 centerpt;
    //centerpt = dob1.center;

//here's a block obstacle; first need 
//1. a MapElement dob1
//2. dob1.set_block_obs(id, centerpoint, yaw, w,h)
//alice.center.x = m_state.localX;
    //alice.center.y = m_state.localY;	

//here is an experiment section which inserts some vehicles; use this part 
//to set obstacles in rndf (and which particles run over;
//1.  need to find a way to label/weight rndf portions
//2.  
 //point2 center;
 //center =tempEl.center;
 //point2arr geom; 
 //geom =tempEl.geometry;

 //id.clear();  //clears the id
 //id.push_back(24); //redefines the id

 //--------------------------------------------------
 // sending state to viewer
 //--------------------------------------------------
 
 //how do we get alice to move through the scene?
 //use planner stack; ./mapviewer listens to everything.
 //UpdateState();
 //alice.set_alice(m_state);
 //sendMapElement(&alice,sendChannel);
 //usleep(100000);
	//MapElement pointout
        //pointout.clear();
	//pointout.set_id(100);
	//pointout.set_points();
	//rbound = rbound+point2(10,0);
	
	//point2 tmppt;
	//for (int k =0; k <10; ++k)
	//  { 
 	//   tmppt = rbound[0];
	//   tmppt = tmppt + point2(k,k);
	//   rbound.push_back(tmppt);//.push_back makes a new entry in the vect of value tmppt.
        //  }

	//pointout.set_geometry(rbound);
        //pointout.plot_color=MAP_COLOR_YELLOW;
	//sendMapElement(&pointout,sendChannel);


