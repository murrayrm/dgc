              Release Notes for "probabilisticplanning" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "probabilisticplanning" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "probabilisticplanning" module can be found in
the ChangeLog file.

Release R1-00a-sam (Mon May 14 15:58:18 2007):
	Updated to work with new map release.  Minor changes in MapElement
	interface functions.

Release R1-00a (Fri May  4 17:41:45 2007):
	This is the first release of actual code for probabilisticplanner.  It is a demonstration version, which if you run asim --rndf=routes-stluke/stluke_ft2.rndf, probplanner --rndf=routes-stluke/stluke_ft2.rndf, mapviewer, will show the computation of an actual probabilistic trajectory in the presence of a dynamic obstacle being predicted without measurement.  If the planner goes off the road, run it again, since it will give a good trajectory sooner or later (its probablistic, after all).  Also, this behavior is both fixable and to be expected (explained below).

	This module estimates p(trajectory of alice|prediction,current data).  It extracts the mean or MAP of this pdf as the actual trajectory to be followed.  The probabilistic nature is necessary because both prediction and the current data is highly uncertain (indeed, prediction is just pure prior knowledge without measurement update).

	Here are some reasons for the bad behavior:

	1.  Bad cost map--values of drivable areas were hard to spoof.  Map query functions could be very useful in this regard.  Additionally, the ability to label areas of the map as being of a certain value (i.e., off road is very high cost) would be very useful.
	2.  The true solution to p(trajectory|prediction,current data) is most surely multimodal, with some (lower probability) trajectories passing the dynamic obstacle on the right.  It is possible to make this type of behavior a logical impossibility, but that defeats the whole purpose of being probabilistic in the first place--unanticipated behavior (in this sense) is good!  Anyway, the correct way to do this involves "hacking" the particle filter so that it more reliably captures low probability modes.


Release R1-00 (Tue May  1 17:41:13 2007):
	Created.

