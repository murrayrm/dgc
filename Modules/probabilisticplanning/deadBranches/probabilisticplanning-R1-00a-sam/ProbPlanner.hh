/*! ProbPlanner.hh
 * Pete Trautman
 * May 1 2007
 */

#ifndef PROBPLANNER_HH
#define PROBPLANNER_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <time.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
//#include "filters/ParticleFilter.hh"
#include "ParticleFilter.hh"


// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

/*! ProbPlanner class
 * This is the main class for Probabilistic Planning which produces trajectories in the presence of arbitrary posteriors over the map data.  This function inherits from StateClient and LocalMapTalker
 * \brief Main class for map tracker function  
 */ 
class CProbPlanner : public CStateClient, public CMapElementTalker
{ 
public:

  /*! Contstructor */
  CProbPlanner(int skynetKey, bool bWaitForStateFill,
                 int debugLevel, int verboseLevel, 
                 bool log);
  /*! Standard destructor */
  ~CProbPlanner();


  /////////////////////////////////////////////////////////////////////
  // Threads
  /////////////////////////////////////////////////////////////////////
  /*! this is the function that continually reads the latest object map*/
  void getLocalMapThread();

  /*! this is the function that continually reads dplanner status*/

  /////////////////////////////////////////////////////////////////////
  // Main planning loop
  /////////////////////////////////////////////////////////////////////
  /*! the main ProbPlanner loop*/
  void ProbPlannerLoop(void);

  /////////////////////////////////////////////////////////////////////
  // initialize map from rndf file
  /////////////////////////////////////////////////////////////////////
  bool loadRNDF(string filename) 
  {
    bool loadedMap = m_localMap->loadRNDF(filename);
    //    localMap->loadRNDF(filename);
    return loadedMap;
  }


private:

  /*!\param m_snKey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;
 
  /*!\param m_debugLevel specifies level of debugging
   */
  bool m_debug;

  /*!\param m_verbose specifies level of debugging
   */
  bool m_verbose;

  /*!\param m_log indicates whether logging was enabled
   */
  bool m_log;

  // Local Map
  Map * m_localMap;
  bool m_recvLocalMap;

  pthread_mutex_t m_LocalMapMutex;
};

#endif  // PROBPLANNER_HH

