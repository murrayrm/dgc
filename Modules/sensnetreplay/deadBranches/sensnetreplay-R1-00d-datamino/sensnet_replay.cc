#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <memory>
#include <cstdlib>
#include <cassert>
#include <cerrno>
// unix headers
#include <signal.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <ncurses.h>
// DGC headers
#include <cotk/cotk.h>
#include <sensnet/sensnet.h>
// local headers
#include "SensnetMultiLog.hh"
#include "replay_cmdline.h"

// Useful message macro
#define MSG(arg) (cerr << __FILE__ << ':' << __LINE__ << ": " << arg << endl)
// Useful error macro
#define ERROR(arg) MSG("*** ERROR: " << arg)

// and some only for debugging
#ifndef DEBUG
#  define DEBUG 1 // set this to 0 (-DDEBUG=0 with gcc) to disable debugging messages
#endif
#if DEBUG
#  define DBG(arg) MSG("DBG: " << arg)
#else
#  define DBG(arg)
#endif

using namespace std;

uint64_t gettime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

ostream& operator << (ostream& os, const SensnetBlob& blob)
{
    os << "(ts: " << blob.timestamp/1e6 << "s, id:" << blob.id << ", sensor_id:"
       << blob.sensor_id << ", type:" << blob.type << ", len:" << blob.len << ")";
    return os;
}

static const int64_t SMALL_TIME = 1000;  // 1ms, usually linux kernel is scheduled at no more than 1000Hz
                                         // setting it equal to the kernel period is a good choice
static const int64_t LONG_TIME = 200000; // 0.5 sec, used to print a warning if we can't keep up with the
                                         // correct timings

// wait for a keypress or for the timeout to pass, whichever comes first.
// timeout is in microseconds.
// Returns 0 if timeout expires, or != 0 if a key is pressed.
int uwait(uint64_t timeout)
{
    uint64_t endTime = gettime() + timeout;
    if (timeout > 100000L) // one tenth of a second
    {
        long tenths = timeout / 100000L;
        long msecs = tenths * 100;
        // wait for a keypress
        timeout(msecs);
        int c = getch();
        timeout(0);
        if (c != ERR) {
            ungetch(c);
            return 1;
        }
    }
    usleep(endTime - gettime());
    return 0;
}

class SensnetReplay;
typedef int (SensnetReplay::*cotk_member_callback_t)(cotk_t *cotk, const char *token);

struct CotkMemberCallbackData {
    cotk_member_callback_t cb;
    SensnetReplay* self;
    CotkMemberCallbackData(SensnetReplay* _self, cotk_member_callback_t _cb)
        : cb(_cb), self(_self)
    { }
};

int cotkMemberCallback(cotk_t *cotk, void *d, const char *token)
{
    CotkMemberCallbackData* data = reinterpret_cast<CotkMemberCallbackData*>(d);
    return (data->self->*(data->cb))(cotk, token);
}

static const char* consoleTemplate =
"Number of blobs sent: %num%\n"
"Current timestamp: %tstamp%\n"
"Current real time: %now%\n"
"\n"
"Next blob:\n"
"    ts: %nextts%\n"
"    id: %blobid%\n"
"    sensor id: %sensorid%\n"
"    type: %blobtype%\n"
"    length: %bloblen% bytes\n"
"\n"
"Output log: %outlog%\n"
//"      Size: %outsize%\n" // TBD
"\n"
"%QUIT%|%PAUSE%|%STEP%|%RW%|%FF%|%FLOOD%|%LOG%\n"
"%timerr%\n"
"%stderr%\n"
"%stderr%\n"
"%stderr%\n"
"%stderr%\n";

class SensnetReplay {

    static volatile sig_atomic_t quit;

    static void sigint_handler(int /*sig*/);


    gengetopt_args_info options;
    bool flood;
    sensnet_t *sensnet;

    cotk_t *console;
    char* consoleTempl;

    uint64_t startTStamp ;
    uint64_t startTime;
    int num; 

    bool pause;

    SensnetMultiLog reader;

    bool outputLog;

public:
    SensnetReplay() : flood(false), sensnet(NULL), console(NULL),
                      consoleTempl(NULL), startTStamp(0), startTime(0),
                      num(-1), pause(false), outputLog(false)
    { }

    ~SensnetReplay()
    {
        cmdline_parser_free(&options);
        if (sensnet != NULL) {
            sensnet_disconnect(sensnet);
            sensnet_free(sensnet);
        }
        if (console != NULL) {
            cotk_close(console);
            cotk_free(console);
        }
    }
    
    int onQuit(cotk_t* /*cotk*/, const char* /*token*/)
    {
        quit ++;
        return 0;
    }

    // set the startTime so the current time equals the timestamp
    // of the last blob
    void resetTimer()
    {
        startTime = gettime() - (reader.getTimestamp() - startTStamp);
    }

    int togglePause(cotk_t* /*cotk*/, const char* /*token*/)
    {
        if (pause) {
            pause = false;
            resetTimer();
        } else {
            pause = true;
        }
        displayStatus();
        cotk_update(console);
        return 0;
    }

    int toggleFlood(cotk_t* /*cotk*/, const char* /*token*/)
    {
        if (flood) {
            flood = false;
            resetTimer();
        } else {
            flood = true;
        }
        return 0;
    }

    int toggleLog(cotk_t* /*cotk*/, const char* /*token*/)
    {
        if (cotk_toggle_get(console, "%LOG%"))
        {  
            bool ok = true;

            // check if log_dir exists, and if not, create it
            struct stat st;
            int ret = stat(options.log_path_arg, &st);
            if (ret == 0) {
                // log_path exists, check if it's a directory
                if (!S_ISDIR(st.st_mode))
                {
                    ERROR('\'' << options.log_path_arg << "' is not a directory, cannot create logs!!!");
                    ok = false;
                }
            } else if (errno == ENOENT) {
                // log_path doesn't exist, create it
                if(mkdir(options.log_path_arg, 0755) != 0)
                {
                    ERROR("cannot create dir. '" << options.log_path_arg
                          << "': " << strerror(errno) << "!!!");
                    ok = false;
                }
            } else {
                ERROR("cannot stat '" << options.log_path_arg
                      << "': " << strerror(errno) << "!!!");
                ok = false;
            }

            if (ok)
            {
                // open a new log file each time (is this the best way? you can
                // merge them afterwards if you want)
                ostringstream oss;
                char timestr[64];
                time_t t = time(NULL);
                strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
                oss << options.log_path_arg << "/" << timestr << "-" << options.log_name_arg;
                string name = oss.str();
                string suffix = "";

                // if it exists already, append .1, .2, .3 ... 
                for (int i = 1; stat((name + suffix).c_str(), &st) == 0; i++) {
                    ostringstream tmp;
                    tmp << '.' << i;
                    suffix = tmp.str();
                }
                name += suffix;

                cotk_printf(console, "%outlog%", A_NORMAL, "%-70s", name.c_str());
                sensnet_open_record(sensnet, name.c_str());
                outputLog = true;
                sensnet_enable_record(sensnet, true);
            } else {
                cotk_toggle_set(console, "%LOG%", false);
                return -1;
            }
        } else {
            // disable logging
            sensnet_enable_record(sensnet, false);
            outputLog = false;
            sensnet_close_record(sensnet);
        }
        return 0;
    }

    /** Advance in one blob steps, and pause.
     */
    int onStep(cotk_t* /*cotk*/, const char* /*token*/)
    {
        if (!pause) {
            pause = true;
            cotk_toggle_set(console, "%PAUSE%", pause);
        }
        SensnetBlob blob;
        displayStatus();
        reader.read(&blob);
        displayBlob(blob);
        cotk_update(console);
        writeBlob(&blob);
        return 0;
    }

    int doSeek(uint64_t timestamp)
    {
        try {
            reader.seek(timestamp);

            SensnetBlob blob;
            displayStatus();
            reader.read(&blob);
            displayBlob(blob);
            cotk_update(console);
            writeBlob(&blob);
            resetTimer();
        } catch (...) {
            ERROR("Cannot seek to time " << timestamp - startTStamp);
            return -1;
        }
        return 0;
    }

    /** Seek forward in 1 sec steps.
     */
    int onFastForward(cotk_t* /*cotk*/, const char* /*token*/)
    {
        return doSeek(reader.getTimestamp() + 1000000);
    }

    /** Seek backward in 1 sec steps.
     */
    int onRewind(cotk_t* /*cotk*/, const char* /*token*/)
    {
        return doSeek(reader.getTimestamp() - 1000000);
    }

    // Wait the amount of time between the last blob and this one
    void waitBlob(const SensnetBlob& blob)
    {
        uint64_t relStamp = blob.timestamp - startTStamp;
        int64_t wait = startTime + relStamp - gettime();
        if (wait < -LONG_TIME) {
            MSG("Message " << num << " ts=" << blob.timestamp/1e6
                << " delayed by " << -wait << "us. Computer too slow?");
            // reset startTime and startTstamp, to avoid accumulating the error
            startTime += -wait;
            startTStamp += -wait;
        } else {
            while (!quit && wait > 2*SMALL_TIME) {
                DBG("sleeping for " << (wait - SMALL_TIME)/1e3 << "msec");
                cotk_update(console);
                uwait(wait - SMALL_TIME);
                wait = startTime + relStamp - gettime();
            }
            cotk_printf(console, "%timerr%", A_NORMAL,
                        "Timing error (timestamp - real time): %gmsec", wait/1e3);
        }
    }

    bool writeBlob(SensnetBlob* blob)
    {
        if (sensnet_write(sensnet, blob->sensor_id,
                          blob->type, blob->id, blob->len, blob->data) != 0)
        {
            ERROR("Error sending blob with timestamp " << blob->timestamp);
            cotk_update(console); // to show the error
            return false;
        }
        num++;
        if (blob->data) {
            delete[] blob->data;
        }
        return true;
    }

    void displayStatus()
    {
        cotk_printf(console, "%num%", A_NORMAL, "%-10d", num);
        cotk_printf(console, "%tstamp%", A_NORMAL, "%-12g sec",
                    (reader.getTimestamp() - startTStamp)/1e6);
        if (pause) 
        {
            cotk_printf(console, "%now%", A_NORMAL, "%-12s", "PAUSED");
        } else {
            cotk_printf(console, "%now%", A_NORMAL, "%-12g sec",
                        (gettime() - startTime)/1e6);
        }
    }

    void displayBlob(const SensnetBlob& blob) {
        cotk_printf(console, "%nextts%", A_NORMAL, "%-12g sec",
                    (blob.timestamp - startTStamp)/1e6);
        cotk_printf(console, "%blobid%", A_NORMAL, "%d", blob.id);
        cotk_printf(console, "%sensorid%", A_NORMAL, "%d", blob.sensor_id);
        cotk_printf(console, "%blobtype%", A_NORMAL, "%d", blob.type);
        cotk_printf(console, "%bloblen%", A_NORMAL, "%ld", blob.len);
    }

    int main(int argc, char **argv)
    {
        /* Parse command line options */
        if (cmdline_parser (argc, argv, &options) != 0)
        {
            return 1;
        }
  
        /* Read parsed command line options */
        char* spreadDaemon = options.spread_daemon_arg;
        flood = options.flood_flag;

        if (!options.spread_daemon_given)
        {
            char *daemon = getenv("SPREAD_DAEMON");
            if (!daemon)
            {
                MSG("No --spread-daemon option given, and SPREAD_DAEMON variable unset.");
                MSG("Using SPREAD_DAEMON=" << spreadDaemon);
            } else {
                spreadDaemon = daemon;
            }
        }
        if (!options.skynet_key_given)
        {
            char* skynetKey = getenv("SKYNET_KEY");
            if (!skynetKey)
            {
                MSG("No --skynet-key option given, and SKYNET_KEY variable unset.");
                options.skynet_key_arg = 0;
            } else {
                char* endp;
                options.skynet_key_arg = strtol(skynetKey, &endp, 0); // returns 0 when given invalid input
                if (*endp != '\0')
                {
                    MSG("The content of SKYNET_KEY=\"" << skynetKey << "\" is not a valid integer!");
                }
            }
        }
        MSG("using skynet key: " << options.skynet_key_arg);
   
        // console template, partly built at runtime
        ostringstream consoleTemplOss;
        consoleTemplOss << "Sensnet Log Player\n\n";
  
        /* open the specified logs */
        for (unsigned i = 0; i < options.inputs_num; i++)
        {
            reader.addLogDir(string(options.inputs[i])); // throws if doesn't exist
            consoleTemplOss << "Log " << i << ": " << options.inputs[i] << "\n";
        }

        sensnet = sensnet_alloc();
        if (sensnet_connect(sensnet, spreadDaemon, options.skynet_key_arg, 0) < 0)
        {
            ERROR("Cannot connect to sensnet.");
            return 3;
        }

        /* catch CTRL-C and exit cleanly, if possible */
        signal(SIGINT, sigint_handler);

        startTStamp = reader.getTimestamp();
        startTime = gettime();

        consoleTemplOss << "\n" << consoleTemplate; // add the static template

        console = cotk_alloc();
        assert(console);
        int templLen = consoleTemplOss.str().length();
        // for some reason, it's better to copy the template in a safe place
        consoleTempl = new char[templLen+1];
        assert(consoleTempl);
        memcpy(consoleTempl, consoleTemplOss.str().c_str(), templLen+1);
        cotk_bind_template(console, consoleTempl);

        // bind buttons
        cotk_bind_button(console, "%QUIT%", " QUIT ", "Qq", cotkMemberCallback,
                         new CotkMemberCallbackData(this, &SensnetReplay::onQuit));
        cotk_bind_toggle(console, "%PAUSE%", " PAUSE ", "Pp", cotkMemberCallback,
                         new CotkMemberCallbackData(this, &SensnetReplay::togglePause));
        cotk_bind_button(console, "%STEP%", " STEP ", "Ss", cotkMemberCallback,
                         new CotkMemberCallbackData(this, &SensnetReplay::onStep));
        cotk_bind_button(console, "%RW%", " << ", "<jJ", cotkMemberCallback,
                         new CotkMemberCallbackData(this, &SensnetReplay::onRewind));
        cotk_bind_button(console, "%FF%", " >> ", ">kK", cotkMemberCallback,
                         new CotkMemberCallbackData(this, &SensnetReplay::onFastForward));
        cotk_bind_toggle(console, "%FLOOD%", " FLOOD ", "Ff", cotkMemberCallback,
                         new CotkMemberCallbackData(this, &SensnetReplay::toggleFlood));
        cotk_toggle_set(console, "%FLOOD%", flood);
        cotk_bind_toggle(console, "%LOG%", " LOG ", "Ll", cotkMemberCallback,
                         new CotkMemberCallbackData(this, &SensnetReplay::toggleLog));
        cotk_toggle_set(console, "%LOG%", options.write_log_flag);

        cotk_open(console, NULL); // no message logging at the moment

        toggleLog(console, "%LOG%"); // if write_log_flag == true, this will open the log file

        while (reader.hasNext() && !quit)
        {
            SensnetBlob blob; // initialized to zero

            if (pause) {
                // wait for a keypress
                nodelay(stdscr, FALSE);
                int c = getch();
                ungetch(c);
                nodelay(stdscr, TRUE);
                cotk_update(console);
                continue;
            }

            displayStatus();

            reader.read(&blob);
            
            displayBlob(blob);

            if (!flood) {
                waitBlob(blob);
            }
            if (!writeBlob(&blob)) {
                break;
            }
            cotk_update(console);
        } // end of main loop
        return 0;
    }
};

volatile sig_atomic_t SensnetReplay::quit = 0;

void SensnetReplay::sigint_handler(int /*sig*/)
{
    // if the user presses C-c 3 or more times, and the program still
    // doesn't terminate, abort
    if (quit > 2) {
        abort();
    }
    quit++;
}

int main(int argc, char** argv)
{
    SensnetReplay app;
    return app.main(argc, argv);
}
