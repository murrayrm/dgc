#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <memory>
#include <cstdlib>
#include <cassert>
// unix headers
#include <signal.h>
#include <sys/time.h>
#include <ncurses.h>
// DGC headers
#include <cotk/cotk.h>
#include <sensnet/sensnet.h>
// local headers
#include "SensnetMultiLog.hh"
#include "replay_cmdline.h"

// Useful message macro
#define MSG(arg) (cerr << __FILE__ << ':' << __LINE__ << ": " << arg << endl)
// Useful error macro
#define ERROR(arg) MSG("*** ERROR: " << arg)

// and some only for debugging
#ifndef DEBUG
#  define DEBUG 1 // set this to 0 (-DDEBUG=0 with gcc) to disable debugging messages
#endif
#if DEBUG
#  define DBG(arg) MSG("DBG: " << arg)
#else
#  define DBG(arg)
#endif

using namespace std;

uint64_t gettime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

ostream& operator << (ostream& os, const SensnetBlob& blob)
{
    os << "(ts: " << blob.timestamp/1e6 << "s, id:" << blob.id << ", sensor_id:"
       << blob.sensor_id << ", type:" << blob.type << ", len:" << blob.len << ")";
    return os;
}

sig_atomic_t quit = 0;

void sigint_handler(int /*sig*/)
{
    // if the user presses C-c 3 or more times, and the program still
    // doesn't terminate, abort
    if (quit > 2) {
        abort();
    }
    quit++;
}

const int64_t SMALL_TIME = 1000; // 1ms, usually linux kernel is scheduled at no more than 1000Hz
                                 // setting it equal to the kernel period is a good choice
const int64_t LONG_TIME = 200000; // 0.5 sec, used to print a warning if we can't keep up with the
                                  // correct timings

int main(int argc, char **argv)
{
  gengetopt_args_info options;

  /* Parse command line options */
  if (cmdline_parser (argc, argv, &options) != 0)
  {
    return 1;
  }
  
  // console template, built dynamically
  ostringstream consoleTemplate;
  consoleTemplate << "Sensnet Log Player\n\n";
  
  /* Read parsed command line options */
  char* spreadDaemon = options.spread_daemon_arg;
  bool flood = options.flood_flag;

  if (!options.spread_daemon_given)
  {
    char *daemon = getenv("SPREAD_DAEMON");
    if (!daemon)
    {
      MSG("No --spread-daemon option given, and SPREAD_DAEMON variable unset.");
      MSG("Using SPREAD_DAEMON=" << spreadDaemon);
    } else {
      spreadDaemon = daemon;
    }
  }
  
  /* open the specified logs */
  SensnetMultiLog reader;
  for (unsigned i = 0; i < options.inputs_num; i++)
  {
    reader.addLogDir(string(options.inputs[i])); // throws if doesn't exist
    consoleTemplate << "Log " << i << ": " << options.inputs[i] << "\n";
  }

  sensnet_t *sensnet = sensnet_alloc();
  if (sensnet_connect(sensnet, spreadDaemon, options.skynet_key_arg, 0) < 0)
  {
    ERROR("Cannot connect to sensnet.");
    return 3;
  }

  /* no more commandline processing */
  cmdline_parser_free(&options);

  if (flood) {
    consoleTemplate << "\nFlood mode ON (output blobs as fast as possible)\n";
  }

  /* catch CTRL-C and exit cleanly, if possible */
  signal(SIGINT, sigint_handler);


  uint64_t startTStamp = reader.getTimestamp();
  //uint64_t lastTime = gettime();
  uint64_t startTime = gettime();
  //uint64_t lastStamp = reader.getTimestamp();
  int num = 0; 

  consoleTemplate
    << "\nBlob number: %num%\n"
    << "Current timestamp: %tstamp%\n"
    << "Current real time: %now%\n"
    << "\n"
    << "Last blob:\n"
    << "    ts: %lastts%\n"
    << "    id: %blobid%\n"
    << "    sensor id: %sensorid%\n"
    << "    type: %blobtype%\n"
    << "    length: %bloblen% bytes\n"
    << "\n"
    << "(hit CTRL-C to terminate)\n"
    << "%timerr%\n"
    << "%stderr%\n"
    << "%stderr%\n"
    << "%stderr%\n"
    << "%stderr%\n";
  cotk_t *console = cotk_alloc();
  assert(console);

  int templLen = consoleTemplate.str().length();
  // for some reason, it's better to copy the template in a safe place
  char* consoleTempl = new char[templLen+1];
  assert(consoleTempl);
  memcpy(consoleTempl, consoleTemplate.str().c_str(), templLen+1);
  cotk_bind_template(console, consoleTempl);
  cotk_open(console, NULL); // no message logging at the moment

  while (reader.hasNext() && !quit)
  {
    SensnetBlob blob;
    reader.read(&blob);

    cotk_printf(console, "%num%", A_NORMAL, "%d", num);
    num++;
    cotk_printf(console, "%tstamp%", A_NORMAL, "%-12g sec",
		(reader.getTimestamp() - startTStamp)/1e6);
    cotk_printf(console, "%now%", A_NORMAL, "%-12g sec",
		(gettime() - startTime)/1e6);
    cotk_printf(console, "%lastts%", A_NORMAL, "%-12g sec",
		(blob.timestamp - startTStamp)/1e6);
    cotk_printf(console, "%blobid%", A_NORMAL, "%d", blob.id);
    cotk_printf(console, "%sensorid%", A_NORMAL, "%d", blob.sensor_id);
    cotk_printf(console, "%blobtype%", A_NORMAL, "%d", blob.type);
    cotk_printf(console, "%bloblen%", A_NORMAL, "%ld", blob.len);

    uint64_t relStamp = blob.timestamp - startTStamp;
    if (!flood) {
      // Wait the amount of time between the last blob and this one
      int64_t wait = startTime + relStamp - gettime();
      if (wait < -LONG_TIME) {
        MSG("Message " << num << " ts=" << blob.timestamp/1e6
            << " delayed by " << -wait << "us. Computer too slow?");
	// reset startTime and startTstamp, to avoid accumulating the error
	startTime += -wait;
	startTStamp += -wait;
      } else {
        while (!quit && wait > 2*SMALL_TIME) {
          DBG("sleeping for " << (wait - SMALL_TIME)/1e3 << "msec");
          usleep(wait - SMALL_TIME);
          wait = startTime + relStamp - gettime();
        }
        cotk_printf(console, "%timerr%", A_NORMAL,
		    "Timing error (timestamp - real time): %gmsec", wait/1e3);
      }
    }

    cotk_update(console);
    if (sensnet_write(sensnet, blob.sensor_id,
                      blob.type, blob.id, blob.len, blob.data) != 0)
    {
      ERROR("Error sending blob with timestamp " << blob.timestamp);
      cotk_update(console); // to show the error
      break;
    }
    if (blob.data) {
        delete[] blob.data;
    }
  } // end of main loop

  cotk_close(console);
  cotk_free(console);

  sensnet_disconnect(sensnet);
  sensnet_free(sensnet);
  return 0;
}
