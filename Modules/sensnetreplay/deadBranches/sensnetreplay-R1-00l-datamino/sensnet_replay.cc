#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <memory>
#include <cstdlib>
#include <cassert>
#include <cerrno>
// unix headers
#include <signal.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <ncurses.h>
// DGC headers
#include <cotk/cotk.h>
#include <sensnet/sensnet.h>
#include <interfaces/sn_types.h>
// local headers
#include "SensnetMultiLog.hh"
#include "replay_cmdline.h"

// Useful message macro
#define MSG(arg) (cerr << __FILE__ << ':' << __LINE__ << ": " << arg << endl)
// Useful error macro
#define ERROR(arg) MSG("*** ERROR: " << arg)

// and some only for debugging
#ifndef DEBUG
#  define DEBUG 1 // set this to 0 (-DDEBUG=0 with gcc) to disable debugging messages
#endif
#if DEBUG
#  define DBG(arg) MSG("DBG: " << arg)
#else
#  define DBG(arg)
#endif

using namespace std;

uint64_t gettime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

ostream& operator << (ostream& os, const SensnetBlob& blob)
{
    os << setprecision(16) << "(ts: " << blob.timestamp/1e6 << "s, id:"
       << blob.id << ", sensor_id:" << blob.sensor_id << ", type:" << blob.type
       << ", len:" << blob.len << ")";
    return os;
}

static const int64_t SMALL_TIME = 1000;  // 1ms, usually linux kernel is scheduled at no more than 1000Hz
                                         // setting it equal to the kernel period is a good choice
static const int64_t LONG_TIME = 200000; // 0.5 sec, used to print a warning if we can't keep up with the
                                         // correct timings

// wait for a keypress or for the timeout to pass, whichever comes first.
// timeout is in microseconds.
// Returns 0 if timeout expires, or != 0 if a key is pressed.
int uwait(uint64_t timeout)
{
    uint64_t endTime = gettime() + timeout;
    if (timeout > 100000L) // one tenth of a second
    {
        long tenths = timeout / 100000L;
        long msecs = tenths * 100;
        // wait for a keypress
        timeout(msecs);
        int c = getch();
        timeout(0);
        if (c != ERR) {
            ungetch(c);
            return 1;
        }
    }
    usleep(endTime - gettime());
    return 0;
}

class SensnetReplay;
typedef int (SensnetReplay::*cotk_member_callback_t)(cotk_t *cotk, const char *token);

struct CotkMemberCallbackData {
    cotk_member_callback_t cb;
    SensnetReplay* self;
    CotkMemberCallbackData(SensnetReplay* _self, cotk_member_callback_t _cb)
        : cb(_cb), self(_self)
    { }
};

int cotkMemberCallback(cotk_t *cotk, void *d, const char *token)
{
    CotkMemberCallbackData* data = reinterpret_cast<CotkMemberCallbackData*>(d);
    return (data->self->*(data->cb))(cotk, token);
}

static const char* consoleTemplate =
"Number of blobs sent: %num%\n"
"Current tstamp: %tstampRel%              %tstamp%\n"
"Current time:   %now%\n"
"Method: %method%"
"\n"
"Next blob:\n"
"    tstamp: %nexttsRel%                  %nextts%\n"
"    id: %blobid%\n"
"    sensor id: %sensorid%\n"
"    type: %blobtype%\n"
"    length: %bloblen% bytes\n"
"\n"
"Output log: %outlog%\n"
//"      Size: %outsize%\n" // TBD
"\n"
"%QUIT%|%PAUSE%|%STEP%|%RW%|%FF%|%FLOOD%|%LOG%|%METHOD%\n"
"%timerr%\n"
"%stderr%\n"
"%stderr%\n"
"%stderr%\n"
"%stderr%\n";

class SensnetReplay {

    static volatile sig_atomic_t quit;

    static void sigint_handler(int /*sig*/);


    gengetopt_args_info options;
    bool flood;
    char* spreadDaemon;
    sensnet_t *sensnet;
    modulename moduleId;

    cotk_t *console;
    char* consoleTempl;

    uint64_t startTStamp ;
    uint64_t startTime;
    int num; 

    bool pause;

    double replayRate;

    SensnetMultiLog reader;
    SensnetLog writer; // used when the LOG button is on

    bool outputLog;

    int method; // SENSNET_METHOD_*

public:
    SensnetReplay() : flood(false), sensnet(NULL), console(NULL),
                      consoleTempl(NULL), startTStamp(0), startTime(0),
                      num(-1), pause(false), replayRate(1.0), outputLog(false)
    { }

    ~SensnetReplay()
    {
        cmdline_parser_free(&options);
        if (sensnet != NULL) {
            sensnet_disconnect(sensnet);
            sensnet_free(sensnet);
        }
        if (console != NULL) {
            cotk_close(console);
            cotk_free(console);
        }
    }
    
    int onQuit(cotk_t* /*cotk*/, const char* /*token*/)
    {
        quit ++;
        return 0;
    }

    // set the startTime so the current time equals the timestamp
    // of the last blob
    void resetTimer()
    {
        startTime = gettime() - (reader.getTimestamp() - startTStamp);
    }

    int togglePause(cotk_t* /*cotk*/, const char* /*token*/)
    {
        if (pause) {
            pause = false;
            resetTimer();
        } else {
            pause = true;
        }
        displayStatus();
        updateDisplay();
        return 0;
    }

    int toggleFlood(cotk_t* /*cotk*/, const char* /*token*/)
    {
        if (flood) {
            flood = false;
            resetTimer();
        } else {
            flood = true;
        }
        return 0;
    }

    int startOutputLog()
    {
	bool ok = true;
	
	// check if log_dir exists, and if not, create it
	struct stat st;
	int ret = stat(options.log_path_arg, &st);
	if (ret == 0) {
	    // log_path exists, check if it's a directory
	    if (!S_ISDIR(st.st_mode))
            {
		ERROR('\'' << options.log_path_arg << "' is not a directory, cannot create logs!!!");
		ok = false;
	    }
	} else if (errno == ENOENT) {
	    // log_path doesn't exist, create it
	    if(mkdir(options.log_path_arg, 0755) != 0)
	    {
		ERROR("cannot create dir. '" << options.log_path_arg
		      << "': " << strerror(errno) << "!!!");
		ok = false;
	    }
	} else {
	    ERROR("cannot stat '" << options.log_path_arg
		  << "': " << strerror(errno) << "!!!");
	    ok = false;
	}

	if (ok)
        {
	    // open a new log file each time (is this the best way? you can
	    // merge them afterwards if you want)
	    ostringstream oss;
	    char timestr[64];
	    time_t t = time(NULL);
	    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
	    oss << options.log_path_arg << "/" << timestr << "-" << options.log_name_arg;
	    string name = oss.str();
	    string suffix = "";
	    
	    // if it exists already, append .1, .2, .3 ... 
	    for (int i = 1; stat((name + suffix).c_str(), &st) == 0; i++) {
		ostringstream tmp;
		tmp << '.' << i;
		suffix = tmp.str();
	    }
	    name += suffix;
	    
	    if (!options.disable_console_flag) {
		cotk_printf(console, "%outlog%", A_NORMAL, "%-70s", name.c_str());
	    } else {
		MSG("Start recording on '" << name << '\'');
	    }
            sensnet_log_header_t header;
            header.timestamp = gettime();
            writer.openWrite(name, header);
	    outputLog = true;
	} else {
	    cotk_toggle_set(console, "%LOG%", false);
	    return -1;
	}
        return 0;
    }

    int stopOutputLog()
    {
	// disable logging
        writer.close();
	outputLog = false;
	MSG("Stopped recording");
	return 0;
    }

    int toggleLog(cotk_t* /*cotk*/, const char* /*token*/)
    {
        if (cotk_toggle_get(console, "%LOG%"))
        {  
	    return startOutputLog();
        } else {
	    return stopOutputLog();
        }
    }

    int toggleMethod(cotk_t* /*cotk*/, const char* /*token*/)
    {
        if (method == SENSNET_METHOD_CHUNK) {
            method = SENSNET_METHOD_SHMEM;
        } else {
            method = SENSNET_METHOD_CHUNK;
        }
        return 0;
    }

    /** Advance in one blob steps, and pause.
     */
    int onStep(cotk_t* /*cotk*/, const char* /*token*/)
    {
        if (!pause) {
            pause = true;
            cotk_toggle_set(console, "%PAUSE%", pause);
        }
        SensnetBlob blob;
        displayStatus();
        reader.read(&blob);
        displayBlob(blob);
        updateDisplay();
        writeBlob(&blob);
        return 0;
    }

    int doSeek(uint64_t timestamp)
    {
        try {
            reader.seek(timestamp);

            SensnetBlob blob;
            displayStatus();
            reader.read(&blob);
            displayBlob(blob);
            updateDisplay();
            writeBlob(&blob);
            resetTimer();
        } catch (...) {
            ERROR("Cannot seek to time " << timestamp - startTStamp);
            return -1;
        }
        return 0;
    }

    /** Seek forward in 1 sec steps.
     */
    int onFastForward(cotk_t* /*cotk*/, const char* /*token*/)
    {
        return doSeek(reader.getTimestamp() + 1000000);
    }

    /** Seek backward in 1 sec steps.
     */
    int onRewind(cotk_t* /*cotk*/, const char* /*token*/)
    {
        return doSeek(reader.getTimestamp() - 1000000);
    }

    // Wait the amount of time between the last blob and this one
    void waitBlob(const SensnetBlob& blob)
    {
        uint64_t relStamp = uint64_t((blob.timestamp - startTStamp)/replayRate);
        int64_t wait = (startTime + relStamp - gettime());
        if (wait < -LONG_TIME) {
            MSG("Message " << num << " ts=" << blob.timestamp/1e6
                << " delayed by " << -wait << "us. Computer too slow?");
            // reset startTime and startTstamp, to avoid accumulating the error
            startTime += -wait;
            startTStamp += -wait;
        } else {
            while (!quit && wait > 2*SMALL_TIME) {
                DBG("sleeping for " << (wait - SMALL_TIME)/1e3 << "msec");
		if (!options.disable_console_flag) {
		    cotk_update(console);
		    uwait(wait - SMALL_TIME);
		} else {
		    usleep(wait - SMALL_TIME);
		}
                wait = startTime + relStamp - gettime();
            }
	    if (!options.disable_console_flag) {
		cotk_printf(console, "%timerr%", A_NORMAL,
			    "Time difference (timestamp - real time): %gmsec", wait/1e3);
	    } else {
		DBG("Time difference (timestamp - real time): " << wait/1e3 << " msec");
	    }
        }
    }

    bool writeBlob(SensnetBlob* blob)
    {
        if (sensnet_write(sensnet, method, blob->sensor_id, blob->type,
                          blob->id, blob->len, blob->data) != 0)
        {
            ERROR("Error sending blob with timestamp " << blob->timestamp);
            updateDisplay(); // to show the error
            return false;
        }
        // API changed, now to write to a log fine you have to explicitly call sensnet_log_write
        if (outputLog) {
            writer.write(*blob);
        }
        num++;
        if (blob->data) {
            delete[] blob->data;
        }
        return true;
    }

    void displayStatus()
    {
	if (!options.disable_console_flag) {
            cotk_printf(console, "%spread%", A_NORMAL, "%s:%s",
                        spreadDaemon, modulename_asString(this->moduleId));
            cotk_printf(console, "%skynetkey%", A_NORMAL, "%d", options.skynet_key_arg);

	    cotk_printf(console, "%num%", A_NORMAL, "%-10d", num);
	    cotk_printf(console, "%tstampRel%", A_NORMAL, "%-8g sec (relative)",
			(reader.getTimestamp() - startTStamp)/1e6);
	    cotk_printf(console, "%tstamp%", A_NORMAL, "%-12.0f usec (since the epoch)",
			static_cast<double>(reader.getTimestamp()));
	    if (pause) 
	    {
		cotk_printf(console, "%now%", A_NORMAL, "%-12s", "PAUSED");
	    } else {
		cotk_printf(console, "%now%", A_NORMAL, "%-12g sec",
			    (gettime() - startTime)/1e6);
	    }
            char* methodStr = "unknown";
            if (method == SENSNET_METHOD_CHUNK) {
                methodStr = "network (SENSNET_METHOD_CHUNK)";
            } else if (method == SENSNET_METHOD_SHMEM) {
                methodStr = "shared mem (SENSNET_METHOD_SHMEM)";
            } else if (method == SENSNET_METHOD_SKYNET) {
                methodStr = "skynet (SENSNET_METHOD_SKYNET)";
            }
            cotk_printf(console, "%method%", A_NORMAL, "%-35s", methodStr);
	} else {
	    MSG("current ts: " << reader.getTimestamp()/1e6 << " sec, "
		<< num << " total blobs");
	}
    }

    void displayBlob(const SensnetBlob& blob) {
	if (!options.disable_console_flag) {
	    cotk_printf(console, "%nexttsRel%", A_NORMAL, "%-8g sec (relative)",
			(blob.timestamp - startTStamp)/1e6);
	    cotk_printf(console, "%nextts%", A_NORMAL, "%-12.0f usec (since the epoch)",
			static_cast<double>(blob.timestamp));
	    cotk_printf(console, "%blobid%", A_NORMAL, "%d", blob.id);
	    cotk_printf(console, "%sensorid%", A_NORMAL, "%d", blob.sensor_id);
	    cotk_printf(console, "%blobtype%", A_NORMAL, "%d", blob.type);
	    cotk_printf(console, "%bloblen%", A_NORMAL, "%ld", blob.len);
	} else {
	    MSG("Next blob: " << blob);
	}
    }

    void updateDisplay()
    {
	if (!options.disable_console_flag)
	    cotk_update(console);
    }

    int main(int argc, char **argv)
    {
        /* Parse command line options */
        if (cmdline_parser (argc, argv, &options) != 0)
        {
            return 1;
        }
  
        /* Read parsed command line options */
        spreadDaemon = options.spread_daemon_arg;
        flood = options.flood_flag;
	replayRate = options.replayrate_arg;

	fprintf(stderr, "input replay rate is %f \n", replayRate);

        if (!options.spread_daemon_given)
        {
            char *daemon = getenv("SPREAD_DAEMON");
            if (!daemon)
            {
                MSG("No --spread-daemon option given, and SPREAD_DAEMON variable unset.");
                MSG("Using SPREAD_DAEMON=" << spreadDaemon);
            } else {
                spreadDaemon = daemon;
            }
        }
        // TODO: find a clean way to use skynet_findkey (clean, i.e. don't introduce skynet
        // as a dependency, because we don't use it. ==> add a sensnet_findkey?)
        if (!options.skynet_key_given)
        {
            char* skynetKey = getenv("SKYNET_KEY");
            if (!skynetKey)
            {
                MSG("No --skynet-key option given, and SKYNET_KEY variable unset.");
                options.skynet_key_arg = 0;
            } else {
                char* endp;
                options.skynet_key_arg = strtol(skynetKey, &endp, 0); // returns 0 when given invalid input
                if (*endp != '\0')
                {
                    MSG("The content of SKYNET_KEY=\"" << skynetKey << "\" is not a valid integer!");
                }
            }
        }
        MSG("using skynet key: " << options.skynet_key_arg);

        method = SENSNET_METHOD_CHUNK;
        if (options.method_given) {
            if (string("net") == options.method_arg) {
                method = SENSNET_METHOD_CHUNK;
                MSG("Using method 'net' (SENSNET_METHOD_CHUNK)");
            } else if (string("shm") == options.method_arg) {
                method = SENSNET_METHOD_SHMEM;
                MSG("Using method 'shm' (SENSNET_METHOD_SHMEM)");
            } else {
                MSG("Method '" << options.method_arg << "' unknwn or unsupported, using net (SENSNET_METHOD_CHUNK)");
            }
        } else {
            MSG("Using method 'net' (SENSNET_METHOD_CHUNK)");
        }
   
	pause = options.pause_flag;
        if (options.disable_console_flag && pause) {
            MSG("Cannot start in PAUSE mode without console display (how do you unpause?)");
            return 1;
        }

        // console template, partly built at runtime
        ostringstream consoleTemplOss;
        consoleTemplOss << "Sensnet Log Player: %spread%\n"
                        << "        Skynet Key: %skynetkey%\n";
  
        /* open the specified logs */
        for (unsigned i = 0; i < options.inputs_num; i++)
        {
            reader.addLogDir(string(options.inputs[i])); // throws if doesn't exist
            consoleTemplOss << "Log " << i << ": " << options.inputs[i] << "\n";
        }

        sensnet = sensnet_alloc();

        moduleId = modulenamefromString(options.module_id_arg);
        if (moduleId <= 0) {
            ERROR("invalid module id: " + string(options.module_id_arg));
            sensnet_free(sensnet);
            sensnet = NULL; // avoid the destructor to try to disconnect and core dump
            return 2;
        }

        if (sensnet_connect(sensnet, spreadDaemon, options.skynet_key_arg, moduleId) < 0)
        {
            ERROR("Cannot connect to sensnet.");
            sensnet_free(sensnet);
            sensnet = NULL; // avoid the destructor to try to disconnect and core dump
            return 3;
        }

        /* catch CTRL-C and exit cleanly, if possible */
        signal(SIGINT, sigint_handler);

        startTStamp = reader.getTimestamp();
        startTime = gettime();

        consoleTemplOss << "\n" << consoleTemplate; // add the static template

	if (!options.disable_console_flag)
	{
	    console = cotk_alloc();
	    assert(console);
	    int templLen = consoleTemplOss.str().length();
	    // for some reason, it's better to copy the template in a safe place
	    consoleTempl = new char[templLen+1];
	    assert(consoleTempl);
	    memcpy(consoleTempl, consoleTemplOss.str().c_str(), templLen+1);
	    cotk_bind_template(console, consoleTempl);
	    
	    // bind buttons
	    cotk_bind_button(console, "%QUIT%", " QUIT ", "Qq", cotkMemberCallback,
			     new CotkMemberCallbackData(this, &SensnetReplay::onQuit));
	    cotk_bind_toggle(console, "%PAUSE%", " PAUSE ", "Pp", cotkMemberCallback,
			     new CotkMemberCallbackData(this, &SensnetReplay::togglePause));
	    cotk_toggle_set(console, "%PAUSE%", pause);
	    cotk_bind_button(console, "%STEP%", " STEP ", "Ss", cotkMemberCallback,
			     new CotkMemberCallbackData(this, &SensnetReplay::onStep));
	    cotk_bind_button(console, "%RW%", " << ", "<jJ", cotkMemberCallback,
			     new CotkMemberCallbackData(this, &SensnetReplay::onRewind));
	    cotk_bind_button(console, "%FF%", " >> ", ">kK", cotkMemberCallback,
			     new CotkMemberCallbackData(this, &SensnetReplay::onFastForward));
	    cotk_bind_toggle(console, "%FLOOD%", " FLOOD ", "Ff", cotkMemberCallback,
			     new CotkMemberCallbackData(this, &SensnetReplay::toggleFlood));
	    cotk_toggle_set(console, "%FLOOD%", flood);
	    cotk_bind_toggle(console, "%LOG%", " LOG ", "Ll", cotkMemberCallback,
			     new CotkMemberCallbackData(this, &SensnetReplay::toggleLog));
	    cotk_toggle_set(console, "%LOG%", options.write_log_flag);
	    cotk_bind_button(console, "%METHOD%", " METHOD ", "Mm", cotkMemberCallback,
			     new CotkMemberCallbackData(this, &SensnetReplay::toggleMethod));
	    
	    cotk_open(console, NULL); // no message logging at the moment
	    
	    toggleLog(console, "%LOG%"); // if write_log_flag == true, this will open the log file
	}

        displayStatus();
            
        while (reader.hasNext() && !quit)
        {
            SensnetBlob blob; // initialized to zero

            if (pause) {
		// can't pause without console
		assert(!options.disable_console_flag);
                // wait for a keypress	      
		nodelay(stdscr, FALSE);
		int c = getch();
		ungetch(c);
		nodelay(stdscr, TRUE);
		updateDisplay();
                continue;
            }

            displayStatus();

            reader.read(&blob);
            
            displayBlob(blob);

            if (!flood) {
                waitBlob(blob);
            }
            if (!writeBlob(&blob)) {
                break;
            }
	    updateDisplay();
        } // end of main loop
        return 0;
    }
};

volatile sig_atomic_t SensnetReplay::quit = 0;

void SensnetReplay::sigint_handler(int /*sig*/)
{
    // if the user presses C-c 3 or more times, and the program still
    // doesn't terminate, abort
    if (quit > 2) {
        abort();
    }
    quit++;
}

int main(int argc, char** argv)
{
    SensnetReplay app;
    try {
	return app.main(argc, argv);
    } catch (SensnetMultiLog::Error& e) {
	ERROR("caught SensnetMultiLog::Error: " << e.what());
	return -1;
    } catch (SensnetLog::Error& e) {
	ERROR("caught SensnetLog::Error: " << e.what());
	return -1;
    } catch (std::exception& e) {
	ERROR("caught std::exception (" << typeid(e).name() << "): " << e.what());
	return -1;
    }
}
