Thu Jul 19 16:14:39 2007	datamino (datamino)

	* version R1-00l-datamino
	BUGS:  
	FILES: replay_cmdline.c(29644), replay_cmdline.ggo(29644),
		replay_cmdline.h(29644), sensnet_replay.cc(29644)
	- Added the --module-id option to set the module id, default
	MODsensnetReplay (before it was set to zero!). - Added spread
	daemon, module id and skynet key to the cotk display - Typo: 4083
	instead of 4803 in default spread daemon - Minor: Changed "Timing
	error" into "Time difference" as people tought it was an error
	while it isn't.

Mon May 14 17:16:08 2007	Laura Lindzey (lindzey)

	* version R1-00l
	BUGS:  
	FILES: replay_cmdline.c(23088), replay_cmdline.ggo(23088),
		replay_cmdline.h(23088), sensnet_replay.cc(23088)
	adding command-line option to replay sensor data faster/slower than
	real time. 

Wed Apr 18  1:22:21 2007	datamino (datamino)

	* version R1-00k
	BUGS:  
	FILES: README(19874)
	Updated README

	FILES: replay_cmdline.c(19873), replay_cmdline.ggo(19873),
		replay_cmdline.h(19873), sensnet_replay.cc(19873)
	Added an option to select the sensnet method (SENSNET_METHOD_CHUNK,
	SENSNET_METHOD_SHMEM). The option is --method=net|shm (or -m
	net|shm). No option for using both, as was done before, because
	it's very likely that a blob be received twice.

	FILES: replay_cmdline.ggo(19805)
	typo

Mon Apr 16 16:26:59 2007	datamino (datamino)

	* version R1-00j
	BUGS:  
	FILES: SensnetLog.hh(19787), sensnet_replay.cc(19787)
	Updated sensnet-replay to work with the recent API changes in
	sensnet.

Thu Mar 22 13:49:45 2007	datamino (datamino)

	* version R1-00i
	BUGS: 
	FILES: SensnetLog.hh(18867), sensnet_replay.cc(18867)
	Fixed compilation after changes to sensnet logging API. Decided not
	to use DMA to write logs, because this is not needed, I think,
	while editing logs offline. May become a commandline option.

Wed Feb 28 15:31:25 2007	datamino (datamino)

	* version R1-00h
	BUGS: 
	FILES: replay_cmdline.c(15978), replay_cmdline.ggo(15978),
		replay_cmdline.h(15978), sensnet_replay.cc(15978)
	Added an option to start in pause mode

Tue Feb 27 17:03:14 2007	datamino (datamino)

	* version R1-00g
	BUGS: 3167
	New files: logcut_cmdline.c logcut_cmdline.ggo logcut_cmdline.h
		sensnet_logcut.cc
	FILES: Makefile.yam(15893), replay_cmdline.c(15893),
		replay_cmdline.ggo(15893), sensnet_replay.cc(15893)
	Some cosmetic changes on sensnet-replay. Added a new tool,
	sensnet-logcut, to extract a piece of a log, delimited by two
	timestamps, and save it into a new file. At the moment I've add it
	here, but it could be moved to sensnet or to it own module, too.
	KNOWN BUGS: at the time, no validation is done on command line
	arguments, and every error (file not found, etc), cause the program
	to abort().

	FILES: Makefile.yam(15915), replay_cmdline.c(15915),
		replay_cmdline.ggo(15915)
	Implemented error handling in sensnet-logcut. Added an option to
	use timestamps relative to the beginning of the log.

	FILES: SensnetLog.hh(15892)
	Fixed some typos in the writing methods.

	FILES: replay_cmdline.c(15917), replay_cmdline.ggo(15917),
		replay_cmdline.h(15917), sensnet_replay.cc(15917)
	Added an option to sensnet-replay to disable console display.

	FILES: sensnet_replay.cc(15920)
	Print timestamps with more digits in no-console mode.

Mon Feb 26 13:21:23 2007	datamino (datamino)

	* version R1-00f
	BUGS: 
	FILES: sensnet_replay.cc(15819)
	Added to the gui the visualization of absolute timestamps (since
	the epoch), as they are stored in the log files. This may be useful
	as a reference.

	FILES: sensnet_replay.cc(15846)
	Added exception handling in main().

Mon Feb 26  1:53:50 2007	datamino (datamino)

	* version R1-00e
	BUGS: 3167
	FILES: replay_cmdline.c(15810), replay_cmdline.ggo(15810),
		replay_cmdline.h(15810), sensnet_replay.cc(15810)
	Implementent output to a log file. Each time the LOG toggle is
	activated, a new log file is created using the sensnet logging
	functions, thus providing a minimal but usable cut and paste
	functionality.

Sun Feb 25 21:27:25 2007	datamino (datamino)

	* version R1-00d
	BUGS: 
	FILES: replay_cmdline.c(15787), replay_cmdline.ggo(15787),
		replay_cmdline.h(15787), sensnet_replay.cc(15787)
	Properly use env variable SKYNET_KEY if no --skynet-key option is
	given.

Sat Feb 24 21:38:34 2007	datamino (datamino)

	* version R1-00c
	BUGS: 3113
	FILES: Makefile.yam(15617), SensnetMultiLog.cc(15617),
		SensnetMultiLog.hh(15617), sensnet_replay.cc(15617)
	Improved pause behavior, implemented STEP button to advance one
	blob at a time. Rewind and fast forward are in place, but
	sensnet_log_seek() is not implemented in the current revision of
	sensnet!!! So, to compile this, an implementation of
	sensnet_log_seek is needed (coming soon). Makefile.yam: added -W
	-Wall -ggdb3 to generate a lot of warnings and detailed debug
	informations.

	FILES: SensnetMultiLog.cc(15624), SensnetMultiLog.hh(15624),
		sensnet_replay.cc(15624)
	Correctly implemented and tested seeking (requires an updated
	sensnet with the implementation of sensnet_log_seek()). Improved
	promptness in responding to keypresses.

Fri Feb 23 17:43:42 2007	datamino (datamino)

	* version R1-00b
	BUGS: 
	FILES: SensnetMultiLog.hh(15585), sensnet_replay.cc(15585)
	Some refactoring, splitted monilithic main() into methods within a
	class. Implemented QUIT and PAUSE buttons.

Fri Feb 23  1:51:17 2007	datamino (datamino)

	* version R1-00a
	BUGS: 3113
	New files: SensnetLog.hh SensnetMultiLog.cc SensnetMultiLog.hh
		replay_cmdline.c replay_cmdline.ggo replay_cmdline.h
		sensnet_replay.cc
	FILES: Makefile.yam(15551)
	Moved sensnet-replay from sensnet to its own module.

	FILES: README(15552)
	Updated description

Fri Feb 23  1:23:58 2007	datamino (datamino)

	* version R1-00
	Created sensnetreplay module.












