#ifndef __SENSNETLOG_HH__
#define __SENSNETLOG_HH__

#include <string>
#include <exception>
// DGC headers
#include <sensnet/sensnet_log.h>

#include <sstream>
static inline std::string __errmsg(const char* msg, const char* file, int line)
{
  std::ostringstream os;
  os << file << ":" << line << ": " << msg;
  return os.str();
}
#define ERRMSG(msg)  __errmsg(msg, __FILE__, __LINE__)


struct SensnetBlob
{
    uint64_t timestamp;
    int sensor_id; 
    int type;
    int id;
    int len;
    int8_t *data;

    SensnetBlob() : timestamp(0), sensor_id(-1), type(-1), id(-1), len(0), data(0) { }
    SensnetBlob(uint64_t tstamp, int sensid, int _type, int _id,
                int _len, int8_t *_data)
        : timestamp(tstamp), sensor_id(sensid), type(_type), id(_id),
          len(_len), data(_data)
    { }
};

/** 
 * Simple inline C++ wrapper for sensnet_log.c
 */

class SensnetLog
{
private:

  bool open;
  sensnet_log_t* self;
  sensnet_log_header_t head;
  
public:

  /** 
   * Exception thrown if any of the sensnet_log_* functions fails.
   * The message should be something that describes (to a human) what
   * went wrong. At the moment, only the name of the function that failed
   * is inceluded.
   * IMPORTANT NOTE: this error is thrown like in throw Error("error message");
   * NOT as a pointer (e.g. throw new Error(...)), and should be catched as a
   * const reference, NOT as a plain object, e.g.
   *   catch(const SensnetLog::Error& e) { ... }
   * on the stack to avoid problems with copy constructors and objects not
   * being destroyed (I had horrible experiences, maybe in the modern gcc this
   * bug is fixed, but this is the safest and more efficient way).
   */
  class Error : public std::exception {
    std::string msg;
  public:
    Error(std::string m) : msg(m) { }
    ~Error() throw() { }
    virtual std::string getMsg() const throw() { return msg; }
    virtual const char* what() const throw() { return msg.c_str(); }
  };



  /**
   * Default constructor, initializes the object without opening any log.
   */
  SensnetLog() throw() : open(false), self(sensnet_log_alloc()) { }

  /**
   * Initilize the object and opens a log for reading.
   * \param logdir The directory of the log
   */
  SensnetLog(const std::string& logdir) throw(Error)
    : open(false), self(sensnet_log_alloc())
  {
    openRead(logdir);
  }

  /**
   * Same as before, but takes a const char* instead of a string.
   */
  SensnetLog(const char* logdir) throw(Error)
    : open(false), self(sensnet_log_alloc())
  {
    openRead(logdir);
  }

  /**
   * Initialize the object and opens a log for reading.
   * \param logdir The directory of the log
   */
  SensnetLog(const std::string& logdir, const sensnet_log_header_t& h) throw(Error)
    : open(false), self(sensnet_log_alloc())
  {
    openWrite(logdir, h);
  }

  /**
   * Same as before, but takes a const char* instead of a string.
   */
  SensnetLog(const char* logdir, const sensnet_log_header_t& h) throw(Error)
    : open(false), self(sensnet_log_alloc())
  {
    openWrite(logdir, h);
  }

  /**
   * Closes the open log, if any, and frees memory.
   */
  ~SensnetLog() {
    if (self) { // should be always true
      if (open) sensnet_log_close(self); // don't use close() as it may throw
      sensnet_log_free(self);
      self = 0; // not needed, but I'm paranoid ;-)
    }
  } 

  void openRead(const char* logdir) throw(Error) {
    if (sensnet_log_open_read(self, logdir, &head) != 0)
      throw Error(ERRMSG("sensnet_log_open_read failed"));
    open = true;
  }

  // don't use DMA direct write by default, we don't have this need while editing logs offline
  void openWrite(const char* logdir, const sensnet_log_header_t& h,
       bool usedma = false) throw(Error)
  {
    if (sensnet_log_open_write(self, logdir, &head, usedma) != 0)
      throw Error(ERRMSG("sensnet_log_open_write failed"));
    head = h; // do this after the call, as it changes the header (sets version)
    open = true;
  }

  void openRead(const std::string& logdir) throw(Error) {
    openRead(logdir.c_str());
  }

  // don't use DMA direct write by default, we don't have this need while editing logs offline
  void openWrite(const std::string& logdir, const sensnet_log_header_t& h, bool usedma = false)
    throw(Error)
  {
    openWrite(logdir.c_str(), h, usedma);
  }

  /**
   * \return the header of the open log, if any. If no log is open, it
   * throws a SensnetLog::Error exception.
   */
  sensnet_log_header_t& getHeader() throw(Error) {
    if (!open)
      throw Error(ERRMSG("BUG: can't get log header if no log is opened first!!!"));
    return head;
  }

  /**
   * \return the const header of the open log, if any. If no log is open, it
   * throws an exception.
   */
  const sensnet_log_header_t& getHeader() const throw(Error) {
    if (!open)
      throw Error(ERRMSG("BUG: can't get log header if no log is opened first!!!"));
    return head;
  }

  /**
   * \return The version of the open log. Throws a SensnetLog::Error if no log
   * is open.
   */
  int getVersion() const throw(Error) {
    if (!open)
      throw Error(ERRMSG("BUG: can't get version if no log is opened first!!!"));
    return head.version;
  }

  /**
   * Close the currently open log, if any. If an error occurs while closing
   * a SensnetLog::Error exception is thrown. If no log is open, it does nothing.
   */
  void close() throw(Error) {
    if (open) {
      if (sensnet_log_close(self) != 0) {
	 // this never happens at the moment
	throw Error(ERRMSG("sensnet_log_close() failed"));
      }
      open = false;
    }
  }

  bool hasNext() const throw() {
    return sensnet_log_has_next(self);
  }

  /*! Reads  blob from the log, and stores its attributes in the location pointed to
   * by the given parameters. 'blob_data' must be big enough to hold
   */
  void read(uint64_t *timestamp, int *sensor_id, int *blob_type, int *blob_id,
            int *blob_len, int8_t **blob_data) throw(Error)
  {
    if (!hasNext()) {
      throw Error(ERRMSG("Can't read blob, end-of-file reached"));
    }

    if (*blob_data == NULL) {
      peek(NULL, NULL, NULL, NULL, blob_len);
      *blob_data = new int8_t[*blob_len];
    }

    if (sensnet_log_read(self, timestamp, sensor_id, blob_type,
                         blob_id, *blob_len, *blob_data) != 0)
    {
      throw Error(ERRMSG("sensnet_log_read() failed"));
    }
  }

  void read(SensnetBlob* blob) throw(Error)
  {
    read(&blob->timestamp, &blob->sensor_id, &blob->type, &blob->id, &blob->len, &blob->data);
  }

  void write(const SensnetBlob& blob) throw(Error) 
  {
    if (sensnet_log_write(self, blob.timestamp, blob.sensor_id, blob.type,
                          blob.id, blob.len, blob.data) != 0)
    {
      throw Error(ERRMSG("sensnet_log_write() failed"));
    }
  }

  void write(uint64_t timestamp, int sensor_id, int blob_type, int blob_id,
             int blob_len, const void *blob_data) throw(Error) {
    if (sensnet_log_write(self, timestamp, sensor_id, blob_type,
                          blob_id, blob_len, blob_data) != 0)
    {
      throw Error(ERRMSG("sensnet_log_write() failed"));
    }
  }

  void peek(uint64_t *timestamp, int *sensor_id, int *blob_type, int *blob_id, int *blob_len)
  {
    if (sensnet_log_peek(self, timestamp, sensor_id, blob_type, blob_id, blob_len)) {
      throw Error(ERRMSG("sensnet_log_peek() failed"));
    }
  }

  void peek(SensnetBlob* blob)
  {
    peek(&blob->timestamp, &blob->sensor_id, &blob->type, &blob->id, &blob->len);
  }

  void seek(uint64_t timestamp) throw(Error)
  {
    if (sensnet_log_seek(self, timestamp) != 0) {
      throw Error(ERRMSG("sensnet_log_seek() failed"));
    }
  }

};

#undef ERRMSG // don't clutter the enviroinment ;-)

#endif
