#ifndef __SENSNETMULTILOG_H__
#define __SENSNETMULTILOG_H__

#include <string>
#include <vector>
#include <queue> // for std::priority_queue

#include "SensnetLog.hh"

class SensnetMultiLog
{
private:

    std::vector<SensnetLog*> m_logs; // one for each log
    std::vector<SensnetBlob> m_blobs; // one for each log
    typedef std::vector<SensnetLog*>::iterator log_iterator;
    typedef std::vector<SensnetLog*>::const_iterator log_const_iterator;

    // curret global timestamp
    uint64_t m_timestamp;

    // priority queue with timestamps of currently available but
    // not yes processed scans (m_logs[n].scan). Used to find who's next
    // when reading scans sequentially.
    struct pq_data {
        uint64_t timestamp;
        int logIdx; // log index within m_logs
        pq_data(uint64_t ts, int idx) : timestamp(ts), logIdx(idx) { }
        bool operator<(const pq_data& d) const {
            return timestamp > d.timestamp; // YES, invert order of the queue!
        }
    };
    std::priority_queue<pq_data> m_pqueue;

public:

    class Error : public std::exception { // generic error
        std::string source;
    public:
        Error(std::string s) : source(s) { }
        virtual ~Error() throw() { }
        virtual const char* what() const throw() { return source.c_str(); }
        virtual std::string getSource() const throw() { return source; }
    };

    class FileNotFound : public Error {
    public:
        FileNotFound(std::string fn) : Error(fn) { }
        virtual ~FileNotFound() throw() { }
        virtual const char* what() const throw() {
            return (getSource() + ": File not found!").c_str();
        }
        virtual std::string getFilename() const throw() { return getSource(); }
    };

    class DirNotFound : public FileNotFound {
    public:
        DirNotFound(std::string fn) : FileNotFound(fn) { }
        virtual ~DirNotFound() throw() { }
        virtual const char* what() const throw() {
            return (getFilename() + ": Directory not found, or not a directory!").c_str();
        }
    };

    /*
     * Initialize the object with no log.
     */
    SensnetMultiLog() throw()
        : m_logs(0), m_blobs(1), m_timestamp(0)
    { }


    /*
     * Takes care of freeing all the memory used, and closing any open file.
     */
    virtual ~SensnetMultiLog();

    /*
     * Adds a new ladar source, using the log data found in logdir.
     * \param logdir The directory where to find the log files.
     * \return the Log ID of this log.
     */
    int addLogDir(std::string logdir) throw(SensnetLog::Error);

    /*
     * Remove all logs dirs from the list, close the files, free the memory.
     * After this, the object is in the same state as it was when just created.
     */
    void clear() throw();

    /*
     * Reads the next blob and returns it into 'blob'. If blob == NULL, read and
     * discard the blob.
     */
    void read(SensnetBlob* blob) throw(SensnetMultiLog::Error, SensnetLog::Error);

    /*
     * Skip (discard) the next blob.
     */
    void skip() {
        read(0);
    }

    /*
     * \return true if there is at least another scan to process, or false
     * if we're at the end of all the logs.
     */
    bool hasNext() throw();

    /*
     * Seek to the desired timestamp. The next blob read will be the one with
     * timestamp equal or immediately following the specified one.
     * \param timestamp The desired timestamp
     */
    void seek(uint64_t timestamp) throw(SensnetLog::Error);

    /*
     * Returns the current timestamp, i.e. the one of the last scan read by next().
     */
    uint64_t getTimestamp() const throw() { return m_timestamp; }

};

#endif /* __SENSNETMULTILOG_H__ */
