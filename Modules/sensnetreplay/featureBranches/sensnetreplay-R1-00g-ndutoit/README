Description of "sensnetreplay" module:

A tool to load multiple sensnet logs (with data from the same run)
and replay them.

Using it is very straightforward, for example, if we have three log directories
called LOGDIR1, LOGDIR2 and LOGDIR3, the command

   $ ./sensnet-replay LOGDIR1 LOGDIR2 LOGDIR3

will replay them.
By default, it sleeps the correct amount of time between each blob, to
reproduce the same behavior seen during the race (or a test), but if you use
the --flood (-f) option, like in

   $ ./sensnet-replay -f LOGDIR1 LOGDIR2 LOGDIR3

the blobs will be written as fast as possible.
The other options are the standard ones (--skynet-key, --spread-daemon, --help, --version)
and typing "./sensnet-replay --help" will type the complete list and descriptions.

The user interface has some buttons you can use to control the program behavior:

QUIT: (hotkey: Q) quits!
PAUSE: (hotkey: P) pause the replay, press again to restart
STEP: (hotkey: S) used to output each blob steb by step. Press STEP to output the next blob
and pause. Press PAUSE to resume normal playback.
<< (rewind): (hotkey: J, <) goes backward 1 second steps). Warning: don't play nice with sensviewer,
(and likely other modules too) because it expects timestamp values to increase only.
>> (rewind): (hotkey: K, >)  goes forward in 1 second steps.
FLOOD: (hotkey: F) Turn flood mode on or off, like the --flood (-f) option.
LOG: (hotkey: L) Write the output to a new log file. This doesn't turn off the normal sensnet output.
You can change the path for the logs with the --log-path option (use --help for more details).

To be done:
* Synchronization of many instances of sensnet-replay running on different
computers (to avoid sending loads of data through the network).	

Another tool, sensnet-logcut, is available to cut a portion of a single log file,
specifying the start and end timestamps. This tool has only a commandline interface,
and it should be easy to write a bash script to work with multiple files.

    $ ./sensnet-logcut --start 1171239936257302 --end  1171239946257302 input_log_dir output_log_dir

If you omit one of the two timestamps (start or end), the start or the end of the file is used.
Timestamps are in microseconds and relative to the epoch (sensnet-replay shows these values, too).
If you use the --relative (-r) option, instead, timestamps are relative to the start of the log.
This extracts the first ten seconds:

    $ ./sensnet-logcut --relative --start 0 --end 10000000 input_log_dir output_log_dir


Other modules that "sensnetreplay" depends on:

sensnet
cotk
