#include <iostream>
#include <string>
#include <cstdlib>
#include <cassert>

#include <sys/time.h>

#include "SensnetLog.hh"
#include "logcut_cmdline.h"

// Useful message macro
#define MSG(arg) (cerr << __FILE__ << ':' << __LINE__ << ": " << arg << endl)
// Useful error macro
#define ERROR(arg) MSG("*** ERROR: " << arg)

// and some only for debugging
#ifndef DEBUG
#  define DEBUG 1 // set this to 0 (-DDEBUG=0 with gcc) to disable debugging messages
#endif
#if DEBUG
#  define DBG(arg) MSG("DBG: " << arg)
#else
#  define DBG(arg)
#endif

using namespace std;

uint64_t gettime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

ostream& operator << (ostream& os, const SensnetBlob& blob)
{
    os << "(ts: " << blob.timestamp/1e6 << "s, id:" << blob.id << ", sensor_id:"
       << blob.sensor_id << ", type:" << blob.type << ", len:" << blob.len << ")";
    return os;
}



int main(int argc, char** argv)
{
    gengetopt_args_info options;
    
    /* Parse command line options */
    if (cmdline_parser (argc, argv, &options) != 0)
    {
        return 1;
    }

    if (options.inputs_num < 2) {
      ERROR("You need to specify the input and output log files!");
      cmdline_parser_print_help();
      return 1;
    }

    int64_t start = int64_t(options.start_arg);
    int64_t end = int64_t(options.end_arg);

    DBG("input: " << options.inputs[0]);
    DBG("output: " << options.inputs[1]);
    DBG("Start: " << start << " usec " << (options.relative_flag ? "relative" : "absolute"));
    DBG("End: " << end << " usec " << (options.relative_flag ? "relative" : "absolute"));

    try {
	// open log for reading
	DBG("Opening " << options.inputs[0] << " for reading ...");
	SensnetLog logIn(options.inputs[0]);
    
	if (options.relative_flag)
	{
	    SensnetBlob tmp;
	    logIn.peek(&tmp);
	    start += tmp.timestamp;
	    end += tmp.timestamp;
	}

	// open log for writing
	SensnetLog logOut;
	sensnet_log_header_t head;
	head.timestamp = gettime();
	DBG("Opening " << options.inputs[1] << " for writing ...");
	logOut.openWrite(options.inputs[1], head);

	if (options.start_arg > 0) {
	    logIn.seek(start);
	}

	while (logIn.hasNext())
	{
	    SensnetBlob blob;
 	    logIn.read(&blob);
	    //DBG("Read blob: " << blob);
	    cout << '.' << flush;
	    if (end != -1 && blob.timestamp >= uint64_t(end)) 
	    {
		break;
	    }
	    logOut.write(blob);
	}
	cout << endl;
    } catch (SensnetLog::Error& e) {
	ERROR("caught SensnetLog::Error: " << e.what());
	return -1;
    } catch (std::exception& e) {
	ERROR("caught std::exception (" << typeid(e).name() << "): " << e.what());
	return -1;
    }
    return 0;
}
