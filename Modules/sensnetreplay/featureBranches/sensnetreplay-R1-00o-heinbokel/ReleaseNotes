              Release Notes for "sensnetreplay" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "sensnetreplay" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "sensnetreplay" module can be found in
the ChangeLog file.

Release R1-00o (Mon Sep  3 23:44:51 2007):
	Implemented sending the state found in sensor blobs over skynet
	(for currently known blobs). This way is possible to run programs
	that listen for state (planner, mplanner, etc.) out of sensnet
	logs. Disabled by default, cmdline option: --send-state.

Release R1-00n (Thu Jul 26 15:14:26 2007):
 	Fixing a bug that causes the program to wait forever under certain
	conditions (expecially with many log files and very close
	timestamps).
        Updating is strongly encouraged ... it's happening pretty often to me!

Release R1-00m (Thu Jul 19 16:14:46 2007):
        NOTE: this needs the latest release of interfaces (>=4-02f)
	- Added the --module-id option to set the module id, default
	  MODsensnetReplay (before it was set to zero!).
        - Added spread daemon, module id and skynet key to the cotk display
        - Typo: 4083 instead of 4803 in default spread daemon
        - Minor: Changed "Timing error" into "Time difference" as people tought
          it was an error while it isn't.

Release R1-00l (Mon May 14 17:16:18 2007):
	Added an option to choose replay speed (--replayrate=x will replay at x times actual speed)

Release R1-00k (Wed Apr 18  1:22:29 2007):
	Added an option to select the sensnet method (SENSNET_METHOD_CHUNK,
	SENSNET_METHOD_SHMEM). The option is --method=net|shm (or
	-m net|shm). No option for using both, as was done before, because
	it's very likely that a blob be received twice.
	Also added a new button in the cotk interface, to change method on the fly.

Release R1-00j (Mon Apr 16 16:27:03 2007):
	Updated sensnet-replay to work with the recent API changes in
	sensnet.

Release R1-00i (Thu Mar 22 13:49:55 2007):
	Fixed compilation after changes to sensnet logging API. Decided not
	to use DMA to write logs, because this is not needed, I think,
	while editing logs offline. May become a commandline option.

Release R1-00h (Wed Feb 28 15:31:31 2007):
	Added an option to start in pause mode.

Release R1-00g (Tue Feb 27 17:03:27 2007):
	Added a new tool, sensnet-logcut, to extract a piece of a sensnet log
	between two specified timestamps (see README for a description).
	Added an option to sensnet-replay to disable console GUI.

Release R1-00f (Mon Feb 26 13:21:26 2007):
	Show both absolute (since the epoch) and relative (since the beginning
	of the log) timestamps.
	Handle exceptions in main(), so it won't abort() when some file is missing
	or other trivial errors.

Release R1-00e (Mon Feb 26  1:53:57 2007):
	Implemented writing the output to a sensnet log file. This, together
	with the ability to seek, pause and single-step, enable some basic log
	editing, such as cutting a smaller piece of a log file, or merging
	logs together.

Release R1-00d (Sun Feb 25 21:27:32 2007):
	Use SKYNET_KEY env variable if no --skynet-key option is given.

Release R1-00c (Sat Feb 24 21:38:41 2007):
	Implemented seeking and single-step mode. The latest version of
	sensnet is required (sensnet-R1-00m)

Release R1-00b (Fri Feb 23 17:43:46 2007):
	Implemented QUIT and PAUSE buttons. Shouldn't crash anymore when
	one of the log files ends (use latest sensnet release).

Release R1-00a (Fri Feb 23  1:51:21 2007):
	Moved sensnet-replay from sensnet to this module.

Release R1-00 (Fri Feb 23  1:23:58 2007):
	Created.















