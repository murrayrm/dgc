/*!
 * \file Planner.hh
 * \brief Header for planner class
 *
 * \author Noel duToit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef PLANNER_HH_
#define PLANNER_HH_

#include <fstream>
#include <queue>
#include <interfaces/VehicleState.h>
#include <gcmodule/GcModule.hh>
#include <gcmodule/GcInterface.hh>
#include <gcinterfaces/SegGoals.hh>
#include <gcinterfaces/SegGoalsStatus.hh>
#include <gcinterfaces/AdriveCommand.hh>
#include <gcinterfaces/FollowerCommand.hh>
#include <trajutils/TrajTalker.hh>
#include <frames/pose3.h>

/* To send the direction of the trajectory */
#include <skynettalker/StateClient.hh>
#include <skynettalker/SkynetTalker.hh>

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <graph-updater/GraphUpdater.hh>
#include <path-planner/PathPlanner.hh>
#include <vel-planner/VelPlanner.hh>
#include <logic-planner/LogicPlanner.hh>
#include <logic-planner/IntersectionHandling.hh>
#include <prediction/Prediction.hh>

#include "TrafficStateEst.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <temp-planner-interfaces/Log.hh>

#include "PlannerUtils.hh"
#include "ZoneCorridor.hh"

/* To generate UTURN */
#include <trajutils/maneuver.h>
#include <trajutils/man2traj.hh>
#include <alice/AliceConstants.h>
#include <pseudocon/interface_superCon_trajF.hh>
#include <skynettalker/SkynetTalker.hh>
#include <trajfollower/trajF_status_struct.hh>

using namespace sc_interface;
using namespace std;

/*! Input interface from which SegGoals are received from the Route Planner */
typedef GcInterface<SegGoals, SegGoalsStatus, SNsegGoals, SNtplannerStatus, MODmissionplanner> MissionPlannerInterface;

class PlannerControlStatus : public ControlStatus
{
public:
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };

  /* The id of the merged directive that this control status corresponds to. */
  unsigned int ID; 
  Status status;
  ReasonForFailure reason;
  bool wasPaused;
};

class PlannerMergedDirective : public MergedDirective
{
public:
 
  PlannerMergedDirective(){};
  ~PlannerMergedDirective(){};

  list<int> segGoalsIDs; 
  SegGoals::SegmentType segType; 
  SegGoals::IntersectionType interType; 

  int exitSegmentID; 
  int exitLaneID; 
  int exitWaypointID;
 
};


struct PlannerMergedDirResp
{
  enum Status{ QUEUED, SENT, ACCEPTED, COMPLETED, FAILED };
  enum ReasonForFailure{ R1, R2, R3 };

  PlannerMergedDirResp()
    :status(QUEUED)
  {
  }

  Status status;
  ReasonForFailure reason;
};


class Planner : public GcModule, public CStateClient {

public: 
  
  /*! Constructor */
  Planner(bool lineFusionEnabled);

  /*! Destructor */
  virtual ~Planner();

private :
  
  /*! Arbitration for the traffic planner control module. It computes the next
    merged directive based on the directives from mission control
    and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);
  
  /*! Control for the traffic planner control module. It computes and sends
    directives to all its controlled modules based on the 
    merged directive and outputs the control status
    based on all the status from its controlled modules. */
  void control(ControlStatus*, MergedDirective*);
  
  /*! Returns whether or not the goal is completed when there is an exit waypoint involved */
  bool isGoalComplete(PointLabel exitWayptLabel);

  /*! Returns the time of day */
  uint64_t getTime();

  /*! Output directive details to standard out */
  void printDirective(SegGoals* newDirective);

  /*! Handles hacked UTurn maneuver */
  Err_t planUTurn(CTraj *traj, VehicleState &vehState, Map *map, PointLabel curr_endpoint);

  /*! Handles hacked Backup maneuver */
  Err_t planBackup(CTraj *traj, VehicleState &vehState, Map *map);

  /*! Handles turning signals */
  int determineSignaling(StateProblem_t problem);

  /*! Sends the turning signal command */
  void sendTurnSignalCommand(int signal);

  /*! Get all segments alice might go in */
  void getSegments(vector<int> &segments);

  /*!\param merged directive sent from arbiter to control */
  PlannerMergedDirective m_mergedDirective;

  /*!\param directives currently stored until planning horizon reqs are met */
  deque<SegGoals> m_accSegGoalsQ;

  /*!\Current SegGoals  */
  SegGoals m_currSegGoals;

  /*!\param control status sent from control to arbiter */
  PlannerControlStatus m_controlStatus;

  /*!\param GcInterface variable */
  MissionPlannerInterface::Northface* m_missTraffInterfaceNF;

  /*!\param GcInterface variable */
  AdriveCommand::Southface* m_traffAdriveInterfaceSF;

  /*!\param GcInterface variable */
  FollowerCommand::Southface* m_traffFollowInterfaceSF;

  /*! Response from Follower */
  FollowerResponse m_followResponse;

  bool m_isInit;

  /*!\Singleton intance of the Traffic State Estimator */
  TrafficStateEst* m_traffStateEst;

  bool m_completed;

  /*! UTURN variables */
  bool uturn_initialized;
  int uturn_stage;

  /*!\Adrive Directive  */
  AdriveDirective m_adriveDir; 

  Path_t m_path;
  Graph_t* m_graph;
  CTraj m_traj;
  CTrajTalker* m_trajTalker;
  
  CMapElementTalker predictionMap;

  /*!\param m_planFromCurrPos tells path planner whether to enforce 
   * continuity between planning cycles, or to plan from current position.
   */
  bool m_planFromCurrPos;
};

#endif /*PLANNER_HH_*/
