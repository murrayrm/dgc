/*!
 * \file TrafficStateEst.cc
 * \brief Some legacy code from the tplanner. Maintains the local map and the vehicle state. Needs to be incorporated into the planner.
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include "TrafficStateEst.hh"
#include <frames/mat44.h>
#include <interfaces/sn_types.h>
#include <alice/AliceConstants.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Console.hh>


#define QUEUE_MAPELEMENTS 0

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(bool waitForStateFill) 
  : CSkynetContainer(MODtrafficplanner, CmdArgs::sn_key)
  , CStateClient(waitForStateFill)
{
  // Create the mapper
	if (CmdArgs::mapper_use_internal)
  {
		m_mapper = new Mapper();
		assert(m_mapper);
  }

  // Create public copy of the map
  m_map = new Map(!CmdArgs::mapper_disable_line_fusion);
  m_map->init();
	
  // TODO MOVE
	loadRNDF(CmdArgs::RNDF_file);

	// Set the initial site transform
	UpdateState();  
  m_map->setLocalToSiteOffset(m_state);

 
  if (!(CmdArgs::disable_fused_perceptor) ) {
    this->fused = NULL;
  }

  return;
}

TrafficStateEst::~TrafficStateEst() 
{
	if (CmdArgs::mapper_use_internal)
		delete m_mapper;
	delete m_map;

  return;
}


TrafficStateEst* TrafficStateEst::Instance(bool waitForStateFill)
{
  if (pinstance == 0)
    pinstance = new TrafficStateEst(waitForStateFill);
  return pinstance;
}


void TrafficStateEst::Destroy()
{
  delete pinstance;
  pinstance = 0;
}


// Update both vehicle and actuator states.
void TrafficStateEst::updateState() 
{
  UpdateState();
  UpdateActuatorState();

  m_currVehState = m_state;
  m_currActState = m_actuatorState;
  
  m_map->setLocalToSiteOffset(m_state);

  return;
}


VehicleState TrafficStateEst::getVehState() 
{
  return m_currVehState;
}


ActuatorState TrafficStateEst::getActState() 
{
  return m_currActState;
}


bool TrafficStateEst::loadRNDF(string filename)
{
  return m_map->loadRNDF(filename);
}


// Initialize the mapper and start the thread
int TrafficStateEst::startMapper()
{
  if (CmdArgs::use_RNDF)
  {
    Log::getStream(1)<<"I am using the RNDF"<<endl;

    if (!CmdArgs::RNDF_file.empty())
    {
      Log::getStream(1)<<"RNDF is not empty "<<endl;

			//Check if the internal mapper is being used
			//if (!CmdArgs::mapper_use_internal){
      m_mapElemTalker.initSendMapElement(CmdArgs::sn_key);
      m_mapElemTalker.initRecvMapElement(CmdArgs::sn_key, 1);
			//}    
    }
    else
    {
      Log::getStream(1)<<"TFEST: RNDFfilename is empty"<<endl;
    }
  }
  else
  {
    Log::getStream(1)<<"TFEST: not using RNDF"<<endl;
  }

  DGCcreateMutex(&m_mapperMutex);

  // Start up the mapper thread
  if (pthread_create(&m_mapperThread, NULL, (void*(*)(void*)) mapperThreadFn, this) != 0)
    return -1; 
  
  return 0;
}


// Stop the mapper thread and clean up
int TrafficStateEst::stopMapper()
{
  // Stop the mapper thread cleanly
  pthread_cancel(m_mapperThread);
  pthread_join(m_mapperThread, NULL);
  DGCdeleteMutex(&m_mapperMutex);
  
  return 0;
}


// Mapper thread callback wrapper.
int TrafficStateEst::mapperThreadFn(TrafficStateEst *self)
{
  self->runMapper();
  return 0;
}


// Main thread function for the mapper
void TrafficStateEst::runMapper()
{
  MapElement recvEl;
  //int bytesRecv;  
	int retval;
	//bool useRndfFrame = CmdArgs::use_rndf_frame;

  bool senddebug = true;
  MapElement debugEl;
  // send messages every nth time
	int everyn = 100;
  uint64_t starttime;
  uint64_t fullstarttime;

// timing variables for full update loop
	int dtime;
	int peakdtime=0;;
	double avgdtime = 0;
	int totdtime;
	int count = 0;

  // timing variables for main mapping loop
	int maindtime;
	int peakmaindtime=0;;
	double avgmaindtime = 0;
	int totmaindtime;
	int maincount = 0;

  // timing variables for map copy 
	int copydtime;
	int totcopydtime = 0;
	double avgcopydtime;
	int peakcopydtime=0;
	int copycount=0;

	int zerocount = 0;
	int errorcount = 0;

  // REMOVE (segfaults) Log::getStream(1) << "In mapper thread ..." <<endl;
    
	if (CmdArgs::mapper_use_internal)
  {
		m_mapper->initComm(CmdArgs::sn_key);
		m_mapper->enableGroundstrikeFiltering(CmdArgs::mapper_enable_groundstrike_filtering);	
		m_mapper->disableLineFusion(CmdArgs::mapper_disable_line_fusion);
		m_mapper->enableLineFusionCompat(CmdArgs::mapper_line_fusion_compat);
		m_mapper->disableObsFusion(CmdArgs::mapper_disable_obs_fusion);
		m_mapper->decayAgeThresh = CmdArgs::mapper_decay_thresh;
    m_mapper->init(CmdArgs::RNDF_file);
    m_mapper->setLocalFrameOffset(m_state);

    m_mapper->plannerOutputDisabled = true;
    m_mapper->debugSubGroup = CmdArgs::mapper_debug_subgroup;

		while (true)
    {
      // See if we should quit
      pthread_testcancel();

      //MSG("map cycle time %.3f", (double) (DGCgettime() - fullstarttime) / 1e3);
      
      fullstarttime = DGCgettime();
      
      // Run the main loop of the mapper
      starttime = DGCgettime();
			retval = m_mapper->mainLoop();
			maindtime = DGCgettime()-starttime;			

      // Send debug information to remote viewer
			if (senddebug)
      {			
				totmaindtime+=maindtime;
				maincount++;
				if (maindtime>peakmaindtime) peakmaindtime=maindtime;

				if (maincount%everyn==0)
        {
					avgmaindtime = (double)totmaindtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,8);
					debugEl.setLabel(0,"Avg main time ", avgmaindtime);
					debugEl.setLabel(1,"Peak main time ", peakmaindtime);
					debugEl.setLabel(2,"Main count ", maincount);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totmaindtime = 0;
					peakmaindtime = 0;
				}
			}

      // If we got an error from the mapper...
			if (retval < 0)
      {
				if (senddebug)
        {			
					errorcount++;
					if (errorcount%everyn==0)
          {
						debugEl.clear();
						debugEl.setTypeDebug();
						debugEl.setId(-1,6);
						debugEl.setLabel(0,"error count ", errorcount);
						m_mapElemTalker.sendMapElement(&debugEl,-3);
					}
				}
				// REMOVE (segfaults) Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received error from mapper:updateMap = " << endl;
        MSG("mapping errored");
        assert(false);
      }

      // If we got some updates from the mapper...
      else if (retval > 0)
      {
        // Update the public copy of the map if we got a request to do so.
        DGClockMutex(&m_mapperMutex);
				if (m_mapCopyState !=0)
        {
					starttime = DGCgettime();

          m_mapper->map.copyMapData(m_map);

					m_mapCopyState = 0;

					if (senddebug)
          {			
						copydtime = DGCgettime()-starttime;		
						totcopydtime+=copydtime;
						copycount++;
						if (dtime>peakcopydtime) peakcopydtime=copydtime;
					
						if (copycount%everyn==0){
							avgcopydtime = (double)totcopydtime/(double)(everyn);
						
							debugEl.clear();
							debugEl.setTypeDebug();
							debugEl.setId(-1,2);
							debugEl.setLabel(0,"Avg copy time ", avgcopydtime);
							debugEl.setLabel(1,"Peak copy time ", peakcopydtime);
							debugEl.setLabel(2,"Copy count ", copycount);
							m_mapElemTalker.sendMapElement(&debugEl,-3);

							debugEl.clear();
							debugEl.setTypeDebug();
							debugEl.setId(-1,16);
							debugEl.setLabel(0,"map size ", (int)m_mapper->map.data.size());
							debugEl.setLabel(1,"used size ", (int)m_mapper->map.usedIndices.size());
							debugEl.setLabel(2,"unused size ", (int)m_mapper->map.openIndices.size());
							m_mapElemTalker.sendMapElement(&debugEl,-3);
					
							totcopydtime = 0;
							peakcopydtime = 0;
						}
					}
				}
				DGCunlockMutex(&m_mapperMutex);
			}

      // Else if we go nothing from the mapper...
      else
      {
				if (senddebug)
        {			
					zerocount++;
					if (zerocount%everyn==0)
          {
						debugEl.clear();
						debugEl.setTypeDebug();
						debugEl.setId(-1,5);
						debugEl.setLabel(0,"zero count ", zerocount);
						m_mapElemTalker.sendMapElement(&debugEl,-3);
					}		
				}
			}
      
      dtime = DGCgettime()-fullstarttime;			
			
			if (senddebug)
      {			
				totdtime+=dtime;
				count++;
				if (dtime>peakdtime) peakdtime=dtime;

				if (count%everyn==0){
					avgdtime = (double)totdtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,12);
					debugEl.setLabel(0,"Avg update time ", avgdtime);
					debugEl.setLabel(1,"Peak update time ", peakdtime);
					debugEl.setLabel(2,"Update count ", count);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totdtime = 0;
					peakdtime = 0;
				}
			}
		}
  }

// REMOVE  
#if 0
  else
  {
    while (true)
    {

      fullstarttime = DGCgettime();

      bytesRecv = m_mapElemTalker.recvMapElementBlock(&recvEl,1);

      if (bytesRecv>0) {
        if (recvEl.id.dat[0] == 10) {
          if (!(recvEl.type == ELEMENT_CLEAR))
            Console::increaseObstSize();
          else
            Console::increaseClearSize();
        }

        DGClockMutex(&m_mapperMutex);
#if QUEUE_MAPELEMENTS == 0
        m_localUpdateMap->addEl(recvEl);
#else
        if (frozen) {
          mapElements.push_back(recvEl);
        } else {
          for (unsigned int i = 0; i < mapElements.size(); i++) {
            m_localMap->addEl(mapElements[i]);
          }
          mapElements.clear();
          m_localUpdateMap->addEl(recvEl);
        }
#endif
        DGCunlockMutex(&m_mapperMutex);

      } else {
        Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
                          << bytesRecv << endl;
      }

      dtime = DGCgettime()-fullstarttime;			
			
			if (senddebug){			
				totdtime+=dtime;
				count++;
				if (dtime>peakdtime) peakdtime=dtime;

				if (count%everyn==0){
					avgdtime = (double)totdtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,12);
					debugEl.setLabel(0,"Avg update time ", avgdtime);
					debugEl.setLabel(1,"Peak update time ", peakdtime);
					debugEl.setLabel(2,"Update count ", count);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totdtime = 0;
					peakdtime = 0;
				}
			}

    }
  }
#endif

  return;
}


void TrafficStateEst::updateMap()
{
  if (CmdArgs::mapper_use_internal)
    {
      // time to wait for a new map in usec
      int waittime = 1000000;
      uint64_t starttime;

      UpdateState();

      // set variable to trigger map copy in mapper thread
      DGClockMutex(&m_mapperMutex);
      m_mapper->setLocalFrameOffset(m_state);
      m_mapCopyState = 1;
      DGCunlockMutex(&m_mapperMutex);
		
      starttime = DGCgettime();

      // TODO: replace this with a conditional mutex (much faster).
      // wait for map copy in mapper thread
      while((int)(DGCgettime()-starttime) < waittime)
	{
	  DGClockMutex(&m_mapperMutex);
	  if (m_mapCopyState==0)
	    {
	      DGCunlockMutex(&m_mapperMutex);
	      break;
	    }
	  DGCunlockMutex(&m_mapperMutex);
	  usleep(0);
	}
    }

#if 0 // REMOVE
  else
    {
#if QUEUE_MAPELEMENTS == 0
      DGClockMutex(&m_localMapUpMutex);
      m_localUpdateMap->copyMapData(m_localMap);
      DGCunlockMutex(&m_localMapUpMutex);
#else
    
      DGClockMutex(&m_localMapUpMutex);
      /* optmize by only copying the pointer and dumping the MapElement vector */
      m_localMap = m_localUpdateMap;
      for (unsigned int i = 0; i < mapElements.size(); i++) {
	m_localMap->addEl(mapElements[i]);
      }
      mapElements.clear();
      DGCunlockMutex(&m_localMapUpMutex);
#endif

    }
#endif

  // Update the local/site frame transform
  UpdateState();
  m_map->setLocalToSiteOffset(m_state);

  return;
}


void TrafficStateEst::freezeMap()
{
  // This is a no-op now.
  // frozen = true;
}

void TrafficStateEst::meltMap()
{
  // This is a no-op now.
  //frozen = false;
}


Map* TrafficStateEst::getMap()
{
  return m_map;
}



// Constants for obstacle checking
#if 1
  #define ALICE_BOX_FRONT 9.7
  #define ALICE_BOX_REAR  6.5 
  #define ALICE_BOX_SIDE  1.5
  #define VEHICLE_RADIUS  9.8152
#else
  #define ALICE_BOX_FRONT 9.7112 // VEHICLE_LENGTH + DIST_REAR_AXLE_TO_FRONT
  #define ALICE_BOX_REAR  6.5956 // VEHICLE_LENGTH + DIST_REAR_TO_REAR_AXLE 
  #define ALICE_BOX_SIDE  2.0668 // VEHICLE_WIDTH/2 + 1
  #define VEHICLE_RADIUS  9.9287 // sqrt(VEHICLE_BOX_FRONT^2+ALICE_BOX_SIDE^2)
#endif

// Update graph with sensed obstacles from the map
int TrafficStateEst::updateGraphStateProblem(PlanGraph *graph, StateProblem_t &problem)
{
  Map *map;
  PlanGraphNode *node;
  MapElement mapEl;
  point2 obs_pt, point;
  point2arr new_geom, bounds;
  point2arr alice_box;
  float mx, my, nx, ny, smallRadius, bigRadius;
  float alice_rear, alice_front, alice_side_left, alice_side_right;
  float m[3][3];
  bool collide;  
  PlanGraphNodeList nodes;

  // Use the map copy
  map = getMap();

  // Keep a running counter so we can check for the freshness of
  // status flags.  TODO; should probably move this somewhere else,
  // since it controls the "freshness" logic of the node status values,
  // and other functions will also update node status.
  // REMOVE graph->statusCount++;
  // REMOVE MSG("status count %d", graph->statusCount);
  graph->setStatusTime(DGCgettime());

  int numElems, numNodes;
  uint64_t time;

  numElems = numNodes = 0;
  time = DGCgettime();
  
  // Check each node against each object in the map for a possible
  // collision.
  Log::getStream(1) << "  Number of obstacle in the map: " << map->usedIndices.size() << endl;
  Console::updateMapSize(map->usedIndices.size());

  nodes.reserve(200);

  for (int j = 0; j < (int)map->usedIndices.size(); j++)
  {
    // mapEl = &map->data[j];
    map->getFusedEl(mapEl,j);

    if (!mapEl.isObstacle())
      continue;
    if (mapEl.geometry.size() == 0)
      continue;

    // TESTING
    numElems++;
    
    //Log::getStream(1) << " Element: " << mapEl.geometry.size() << " " << mapEl.id << endl;  

    // Get the center of the bounding circle
    mx = mapEl.center.x;
    my = mapEl.center.y;

    // Compute the size of the small bounding circle that captures the obstacle.
    smallRadius = MAX(mapEl.width, mapEl.height)/2;
    
    // Compute the big bounding circle that captures the obstacle and Alice.
    bigRadius = smallRadius + VEHICLE_RADIUS;

    // Get the nodes that might collide with the obstacle
    graph->getRegion(&nodes, mx, my, bigRadius*2, bigRadius*2);
    
    // Check each node against the obstacle
    for (unsigned int i = 0; i < nodes.size(); i++)
    {
      collide = false;
      node = nodes[i];
      
      // TESTING
      numNodes++;

      /* THIS MAY BE CAUSING PROBLEMS
      // Ignore node that already collided (give priority to vehicle collision).
      if (graph->isStatusFresh(node))
      {
      if (node->status.collideCar)
      continue;
      if (mapEl.isObstacle() && !mapEl.isVehicle() && node->status.collideObs)
      continue;
      }
      */
      
      // Check against big bounding box
      nx = node->pose.pos.x;
      ny = node->pose.pos.y;
      if (nx < mx - bigRadius || nx > mx + bigRadius ||
          ny < my - bigRadius || ny > my + bigRadius)
        continue;

      // Transform small bounding circle into node frame
      pose2f_to_mat33f(pose2f_inv(node->pose), m);
      nx = m[0][0]*mx + m[0][1]*my + m[0][2];
      ny = m[1][0]*mx + m[1][1]*my + m[1][2];

      // check against unrestricted bounding box around bounding circle
      if (ALICE_BOX_FRONT < nx - smallRadius || -ALICE_BOX_REAR > nx + smallRadius ||
          ALICE_BOX_SIDE < ny - smallRadius || -ALICE_BOX_SIDE > ny + smallRadius)
        continue;

      // Be conservative only in lanes only if the obstacle field
      // in the state is set to OBSTACLE_SAFETY
      if (problem.obstacle != OBSTACLE_SAFETY || !(node->flags.isLane) || !(node->flags.isLaneChange))
      {
        switch (problem.obstacle) {
          case OBSTACLE_SAFETY:
            // If the node is not in a lane or the obstacle is not in the same
            // lane than the node then use the 1m separation distance
            alice_front = DIST_REAR_AXLE_TO_FRONT + VEHICLE_LENGTH;
            alice_rear = DIST_REAR_TO_REAR_AXLE + 1.0;
            alice_side_left = VEHICLE_WIDTH/2.0 + 1.0;
            alice_side_right = alice_side_left;
            break;
          case OBSTACLE_AGGRESSIVE:
            // If obstacle safety is aggressive, then use 1m in front and .5m on the sides
            alice_front = DIST_REAR_AXLE_TO_FRONT + 2.0;
            alice_rear = DIST_REAR_TO_REAR_AXLE + 1.0;
            alice_side_left = VEHICLE_WIDTH/2.0 + 1.0;
            alice_side_right = alice_side_left;
            break;
          case OBSTACLE_BARE:
            // If obstacle safety is bare, then do not grow alice
            alice_front = DIST_REAR_AXLE_TO_FRONT + 1.0;
            alice_rear = DIST_REAR_TO_REAR_AXLE + 0.5;
            alice_side_left = VEHICLE_WIDTH/2.0 + 0.5;
            alice_side_right = alice_side_left;
            break;
        }
      }
      else
      {
        // get distance to next turn node
        double dist = 1.0 + DIST_REAR_AXLE_TO_FRONT;
        PlanGraphNode *prev_node = node;
        do
        {
          PlanGraphNode *next_node = NULL;

          // Find the next node in the lane.
          for (int k = 0; k < prev_node->numNext; k++)
          {
            next_node = prev_node->next[k];
            if (next_node->flags.isLane)
              break;
          }
          
          if (next_node == NULL || (next_node->flags.isTurn))
            break;
          dist += sqrt(pow(prev_node->pose.pos.x - next_node->pose.pos.x,2) +
                       pow(prev_node->pose.pos.y - next_node->pose.pos.y,2));
          prev_node = next_node;
        }
        while (dist < ALICE_BOX_FRONT);

        // Set the effective size of Alice based on these distances
        alice_front = MIN(dist, ALICE_BOX_FRONT);
        alice_rear = ALICE_BOX_REAR;
        alice_side_right = VEHICLE_WIDTH/2.0 + 0.2;
        alice_side_left = ALICE_BOX_SIDE;

        /* TESTING Not sure what the intent of this is.
           if (node->railId > 0)
           {
           alice_side_right = VEHICLE_WIDTH/2.0 + 0.2;
           alice_side_left = ALICE_BOX_SIDE;
           }
           else if (node->railId < 0)
           {
           alice_side_left = VEHICLE_WIDTH/2.0 + 0.2;
           alice_side_right = ALICE_BOX_SIDE;
           }
           else
           {
           float laneWidth;
           if (node->nextWaypoint)
           laneWidth = node->nextWaypoint->laneWidth;
           else if (node->prevWaypoint)
           laneWidth = node->prevWaypoint->laneWidth;
           else
           laneWidth = 4.0; // MAGIC
           alice_side_left = alice_side_right = MIN(laneWidth/2.0 + 0.2, ALICE_BOX_SIDE);
           }
        */
      }

      // Transform obstacle into node coordinates.  Check if the
      // individual points fall into the Alice box.  At the same time
      // we build a polygon of the object that we may need to test
      // for intersection.
      if (!collide)
      {
        new_geom.clear();
        for (unsigned int k=0; k<mapEl.geometry.size(); k++)
        {
          obs_pt.set(mapEl.geometry[k]);
          point.x = m[0][0] * obs_pt.x + m[0][1] * obs_pt.y + m[0][2];
          point.y = m[1][0] * obs_pt.x + m[1][1] * obs_pt.y + m[1][2];
          new_geom.push_back(point);

          // Check point with alice bounding box
          if (point.x > -alice_rear && point.x < alice_front &&
              point.y > -alice_side_right && point.y < alice_side_left)
          {
            collide = true;
            break;
          }
        }
        new_geom.push_back(new_geom[0]);
      }

      // Check for intersection between the obstacle polygon and the
      // Alice box. This is the slowest test, and therefore applied
      // last.
      if (!collide)
      {
        // populate alice box
        alice_box.clear();
        alice_box.push_back(point2(alice_front, alice_side_left));
        alice_box.push_back(point2(alice_front, -alice_side_right));
        alice_box.push_back(point2(-alice_rear, -alice_side_right));
        alice_box.push_back(point2(-alice_rear, alice_side_left));
        alice_box.push_back(point2(alice_front, alice_side_left));        
        collide = new_geom.is_intersect(alice_box);
      }

      // No collisions, dont update anything
      if (!collide)
        continue;

      /* REMOVE
      // We have a collision; if the node is not fresh, reset both
      // collision flags now.
      if (!graph->isStatusFresh(node))
      {
        memset(&node->status, 0, sizeof(node->status));
        graph->setStatusFresh(node);
      }
      */

      // Now update the map
      if (mapEl.isObstacle() && !mapEl.isVehicle())
      {
        node->status.obsCollision = true;
        node->status.obsTime = graph->getStatusTime();
      }
      else if (mapEl.isVehicle())
      {
        node->status.carCollision = true;
        node->status.carTime = graph->getStatusTime();
      }
    }
  }

  // TESTING
  time = DGCgettime() - time;
  MSG("%d elements, %d nodes, %.3fms", numElems, numNodes, (double) time/1e3);

  return GU_OK;
}


#if 0

// Function for evaluating node status (lazy evaluation in the planner)
int TrafficStateEst::updateNodeStatus(TrafficStateEst *self, PlanGraph *graph, PlanGraphNode *node)
{
  Map *map;
  MapElement mapEl;
  point2 obs_pt, point;
  point2arr new_geom, bounds;
  point2arr alice_box;
  float mx, my, nx, ny, smallRadius, bigRadius;
  float alice_rear, alice_front, alice_side_left, alice_side_right;
  float m[3][3];
  bool collide;  
  
  // HACK
  // Dont bother with nodes outside the ROI
  if (node->pose.pos.x - self->m_currVehState.siteNorthing < -50 ||
      node->pose.pos.x - self->m_currVehState.siteNorthing > +50)
    return 0;
  if (node->pose.pos.y - self->m_currVehState.siteEasting < -50 ||
      node->pose.pos.y - self->m_currVehState.siteEasting > +50)
    return 0;

  //MSG("test node %d.%d.%d.%d", node->segmentId, node->laneId, node->waypointId, node->interId);
  
  // Use the map copy
  map = self->getMap();

  // TODO full logic
  // If the node is not in a lane or the obstacle is not in the same
  // lane than the node then use the 1m separation distance
  alice_front = DIST_REAR_AXLE_TO_FRONT + 1.0;
  alice_rear = DIST_REAR_TO_REAR_AXLE + 1.0;
  alice_side_left = VEHICLE_WIDTH/2.0 + 1.0;
  alice_side_right = alice_side_left;

  // Check each object in the map for a possible collision.
  for (int j = 0; j < (int)map->usedIndices.size(); j++)
  {
    map->getFusedEl(mapEl,j);

    if (!mapEl.isObstacle())
      continue;
    if (mapEl.geometry.size() == 0)
      continue;
        
    // Get the center of the bounding circle
    mx = mapEl.center.x;
    my = mapEl.center.y;

    // Compute the size of the small bounding circle that captures the obstacle.
    smallRadius = MAX(mapEl.width, mapEl.height)/2;
    
    // Compute the big bounding circle that captures the obstacle and Alice.
    bigRadius = smallRadius + VEHICLE_RADIUS;
      
    // Check node against big bounding box
    nx = node->pose.pos.x;
    ny = node->pose.pos.y;
    if (nx < mx - bigRadius || nx > mx + bigRadius ||
        ny < my - bigRadius || ny > my + bigRadius)
      continue;

    // Transform small bounding circle into node frame
    pose2f_to_mat33f(pose2f_inv(node->pose), m);
    nx = m[0][0]*mx + m[0][1]*my + m[0][2];
    ny = m[1][0]*mx + m[1][1]*my + m[1][2];
    
    // check against unrestricted bounding box around bounding circle
    if (ALICE_BOX_FRONT < nx - smallRadius || -ALICE_BOX_REAR > nx + smallRadius ||
        ALICE_BOX_SIDE < ny - smallRadius || -ALICE_BOX_SIDE > ny + smallRadius)
      continue;

    collide = false;

    // Transform obstacle into node coordinates.  Check if the
    // individual points fall into the Alice box.  At the same time
    // we build a polygon of the object that we may need to test
    // for intersection.
    if (!collide)
    {
      new_geom.clear();
      for (unsigned int k=0; k<mapEl.geometry.size(); k++)
      {
        obs_pt.set(mapEl.geometry[k]);
        point.x = m[0][0] * obs_pt.x + m[0][1] * obs_pt.y + m[0][2];
        point.y = m[1][0] * obs_pt.x + m[1][1] * obs_pt.y + m[1][2];
        new_geom.push_back(point);

        // Check point with alice bounding box
        if (point.x > -alice_rear && point.x < alice_front &&
            point.y > -alice_side_right && point.y < alice_side_left)
        {
          collide = true;
          break;
        }
      }
      new_geom.push_back(new_geom[0]);
    }

    // Check for intersection between the obstacle polygon and the
    // Alice box. This is the slowest test, and therefore applied
    // last.
    if (!collide)
    {
      // populate alice box
      alice_box.clear();
      alice_box.push_back(point2(alice_front, alice_side_left));
      alice_box.push_back(point2(alice_front, -alice_side_right));
      alice_box.push_back(point2(-alice_rear, -alice_side_right));
      alice_box.push_back(point2(-alice_rear, alice_side_left));
      alice_box.push_back(point2(alice_front, alice_side_left));        
      collide = new_geom.is_intersect(alice_box);
    }

    // Now update the map
    if (mapEl.isObstacle() && !mapEl.isVehicle())
    {
      node->status.collideObs = collide;
    }
    else if (mapEl.isVehicle())
    {
      node->status.collideCar = collide;
    }
  }
  
  return 0;
}

#endif

// int TrafficStateEst::updatePathStoplines(PlanGraphPath *path)
// {
//   PlanGraphNode *node;
//   RNDFGraphWaypoint *wp;
//   PointLabel stopline;
//   point2 stopPt;
//   double dist, dx, dy;

//   // Use the map copy
//   Map *map = getMap();

//   for (int i=0; i<path->pathLen; i++) {
//     node = path->nodes[i];

//     if (path->dists[i] > 30.0) return 0;

//     if (node->flags.isStop)
//     {
//       wp = node->nextWaypoint;

//       if (wp == NULL) return -1;
//       if (!wp->flags.isStop) return 0;

//       stopline.segment = wp->segmentId;
//       stopline.lane    = wp->laneId;
//       stopline.point   = wp->waypointId;
//       // Get the actual stopline position from the map
//       if (map->getStopLineSensed(stopPt, stopline) < 0) return -1;

//       dx = stopPt.x-node->pose.pos.x;
//       dy = stopPt.y-node->pose.pos.y;
//       dist = cos(node->pose.rot)*dx + sin(node->pose.rot)*dy;

//       // Update the distance to the stopline
//       node->status.stopDist = dist;
//     }
//   }

//   return 0;
// }

// int TrafficStateEst::updateGraphStoplines(PlanGraph *graph)
// {
//   PlanGraphNode *node;
//   PointLabel stopline;
//   point2 stopPt;
//   double dist, dx, dy;

//   // Use the map copy
//   Map *map = getMap();

//   int railCount = 5;
//   float railSpacing = 0.75;

//   // Compute a size that is sure to capture all the rails.
//   float size = 16 + 4 * railCount * railSpacing;

//   // Get the next waypoint on our path
//   if (graph->vehicleNode == NULL) return -1;
//   RNDFGraphWaypoint *wp = graph->vehicleNode->nextWaypoint;
//   if (wp == NULL) return -1;
//   if (!wp->flags.isStop) return 0;
//   stopline.segment = wp->segmentId;
//   stopline.lane    = wp->laneId;
//   stopline.point   = wp->waypointId;
//   // Get the actual stopline position from the map
//   if (map->getStopLineSensed(stopPt, stopline) < 0) return -1;

//   // Get all nodes close to that waypoint
//   PlanGraphNodeList nodes;
//   graph->getRegion(&nodes, wp->px, wp->py, size, size);

//   for (int i=0; i<(int)nodes.size(); i++)
//   {
//     node = nodes[i];

//     if (!node->flags.isStop) continue;

//     dx = stopPt.x-node->pose.pos.x;
//     dy = stopPt.y-node->pose.pos.y;
//     dist = cos(node->pose.rot)*dx + sin(node->pose.rot)*dy;

//     // Update the distance to the stopline
//     node->status.stopDist = dist;
//   }

//   return 0;
// }

// TODO Use correct error/message handling
// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Initialize the fused perceptor
int TrafficStateEst::startFused(const char *spreadDaemon, int skynetKey,
                                bool useRiegl, bool useStereo)
{  
  // Create perceptor
  this->fused = new FusedPerceptor();
  assert(this->fused);

  // Initialize the perceptor
  // TODO if (this->fused->initLive(spreadDaemon, skynetKey, MODfusedplanner) != 0)
  if (this->fused->initLive(spreadDaemon, skynetKey, MODfusedviewer + 16) != 0)
  {
    delete this->fused;
    this->fused = NULL;
    return ERROR("unable to initialize fused perceptor");
  }

  // Subscribe to the selected sensors/perceptors
  if (useRiegl)
  {
    if (this->fused->joinRiegl() != 0)
      return ERROR("unable to subscribe to riegl data");
  }

  if (useStereo)
  {
    if (this->fused->joinStereoLine() != 0)
      return ERROR("unable to subscribe to stereo line data");
    this->fused->stereoMinCount = 2; // MAGIC
  }

  // Create the mutex
  pthread_mutex_init(&this->fusedMutex, NULL);
  
  // Create the thread
  MSG("starting fused perceptor thread");
  if (pthread_create(&this->fusedThread, NULL, (void*(*)(void*)) mainFused, this) != 0)
    return ERROR("unable to start fused perceptor thread");
  
  return 0;
}


// Finalize the fused perceptor
int TrafficStateEst::stopFused()
{
  if (!this->fused)
    return 0;
  
  // Stop the thread
  MSG("cancelling fused perceptor thread");
  pthread_cancel(this->fusedThread);
  pthread_join(this->fusedThread, NULL);
  MSG("cancelled fused perceptor thread");
   
  // Clean up
  pthread_mutex_destroy(&this->fusedMutex);
  this->fused->fini();
  delete this->fused;
  this->fused = NULL;
  
  return 0;
}


// Lock the fused perceptor and return a pointer
FusedPerceptor *TrafficStateEst::lockFused()
{
  pthread_mutex_lock(&this->fusedMutex);
  return this->fused;
}


// Unlock the fused perceptor
FusedPerceptor *TrafficStateEst::unlockFused()
{
  pthread_mutex_unlock(&this->fusedMutex);
  return NULL;
}


// Main loop for fused perceptor
int TrafficStateEst::mainFused(TrafficStateEst *self)
{
  FusedPerceptor *fused = self->fused;

  if (!fused)
    return 0;

  MSG("running fused perceptor thread");
  while (true)
  {
    pthread_testcancel();

    // Wait for new data.  The thread cancel call will force this to
    // exit if we are currently blocked.
    if (sensnet_wait(self->fused->sensnet, -1) == 0)
    {
      pthread_mutex_lock(&self->fusedMutex);
      fused->updateLive();
      pthread_mutex_unlock(&self->fusedMutex);
      //MSG("fused time %.3f", (double) fused->state.timestamp * 1e-6);
    }
  }
  
  return 0;
}


// Update the graph with data from the fused perceptor
int TrafficStateEst::updateFusedGraph(PlanGraph *graph, const VehicleState *state)
{
  int i;
  PlanGraphNode *node;
  PlanGraphNodeList nodes;
  float m[4][4];
  float lx, ly, px, py, size;
  uint64_t timeA, timeB;

  // Lock access to the perceptor.  This is probably not necessary,
  // but is done for the sake of good form.
  pthread_mutex_lock(&this->fusedMutex);

  timeA = DGCgettime();
  
  // Prepare the cost map.  
  this->fused->prepare(state);

  // Size of the local map (no point looking outside this region)
  size = this->fused->localMap->size * this->fused->localMap->scale;

  // Get the transform from local to site frame
  mat44f_setf(m, this->fused->transSL);

  // Compute the region of interest in the site frame
  lx = this->fused->localMap->pose.pos.x;
  ly = this->fused->localMap->pose.pos.y;
  px = m[0][0]*lx + m[0][1]*ly + m[0][3];
  py = m[1][0]*lx + m[1][1]*ly + m[1][3];

  timeA = DGCgettime() - timeA;

  timeB = DGCgettime();
    
  //MSG("getting quadtree %.3f %.3f %.3f %.3f",
  //    px - size/2, px + size/2, py - size/2, py + size/2);
  
  // Get all the nodes in the local region
  graph->getRegion(&nodes, px, py, size, size);
  
  // Update each node
  for (i = 0; i < (int) nodes.size(); i++)
  {
    float laneWidth, dist;
    
    node = nodes[i];

    // Get the lane width from the RNDF.  If we cant figure out which
    // lane we are in, or there is no width defined, use a nominal
    // width.
    laneWidth = VEHICLE_WIDTH + 2.0;
    if (node->nextWaypoint)
      laneWidth = node->nextWaypoint->laneWidth;
    if (laneWidth < 1)
      laneWidth = VEHICLE_WIDTH + 2.0;

    // Get the distance to the nearest line
    dist = this->fused->calcLineDist(node->pose.pos.x, node->pose.pos.y, node->pose.rot);

    // Compute the distance to the lane center
    dist = fabsf(laneWidth/2 - dist);
    if (dist < 0)
      dist = 0;
    if (dist > laneWidth) // TESTING; try pulling us back from longer distances
      dist = laneWidth;
    node->status.centerLineDist = dist;
    
    // Use the distance to the nearest rough patch.
    dist = this->fused->calcRoughDist(0, node->pose.pos.x, node->pose.pos.y);
    dist = laneWidth/2 - dist;
    if (dist < 0)
      dist = 0;
    node->status.centerRoughDist = dist;

    // Freshen the update time
    node->status.centerTime = graph->getStatusTime();
  }

  timeB = DGCgettime() - timeB;
  
  MSG("fused %d nodes in %.3fms (prep) %.3fms (test)",
      (int) nodes.size(), (double) timeA/1e3, (double) timeA/1e3);
    
  // Unlock access to the perceptor
  pthread_mutex_unlock(&this->fusedMutex);
  
  return 0;
}


// Update the graph with data from the fused perceptor; alternate method.
// Should not be used in off-road mode.
int TrafficStateEst::updateFusedGraphAlt(PlanGraph *graph, const VehicleState *state)
{
  int i, j;
  PlanGraphNode *node;
  PlanGraphNodeList nodes;
  float m[4][4];
  float lx, ly, px, py;
  uint16_t include, exclude;
  int railCount;  
  float railSpacing, innerSize, outerSize;
  uint64_t timeA, timeB, timeC;
  float lineDist[32] = {0};
  float lineCount[32] = {0};
  float roughDist[32] = {0};
  float roughCount[32] = {0};

  // MAGIC numbers.  
  railSpacing = 0.75;
  innerSize = 32.0;
  outerSize = 64.0;
  
  // HACK; use some arbitrary rail count; must be larger than
  // the actual rail count or badness will ensue.
  railCount = 9;

  // Do a sanity check on the rail count
  assert(railCount <= sizeof(lineCount)/sizeof(lineCount[0]));

  // Lock access to the perceptor.  This is probably not necessary,
  // but is done for the sake of good form.
  pthread_mutex_lock(&this->fusedMutex);

  timeA = DGCgettime();
  
  // Prepare the cost map.  
  this->fused->prepare(state);

  // Get the transform from local to site frame
  mat44f_setf(m, this->fused->transSL);

  // Compute the region of interest in the site frame
  lx = this->fused->localMap->pose.pos.x;
  ly = this->fused->localMap->pose.pos.y;
  px = m[0][0]*lx + m[0][1]*ly + m[0][3];
  py = m[1][0]*lx + m[1][1]*ly + m[1][3];

  timeA = DGCgettime() - timeA;

  timeB = DGCgettime();
    
  //MSG("getting quadtree %.3f %.3f %.3f %.3f",
  //    px - size/2, px + size/2, py - size/2, py + size/2);

  // Get all the lane nodes in the local region
  include = PLAN_GRAPH_NODE_LANE;
  exclude = PLAN_GRAPH_NODE_ONCOMING;
  graph->getRegion(&nodes, px, py, innerSize, innerSize, include, exclude);
  
  // Consider lane nodes and update the stats.  This could get
  // confused on multi-lane roads.
  for (i = 0; i < (int) nodes.size(); i++)
  {
    float laneWidth, dist;
    
    node = nodes[i];

    // Rail index in stats
    j = node->railId + (railCount - 1) / 2;
    
    // Get the lane width from the RNDF.  If we cant figure out which
    // lane we are in, or there is no width defined, use a nominal
    // width.
    laneWidth = VEHICLE_WIDTH + 2.0;
    if (node->nextWaypoint)
      laneWidth = node->nextWaypoint->laneWidth;
    if (laneWidth < 1)
      laneWidth = VEHICLE_WIDTH + 2.0;

    // Get the distance to the lane center, based on lane lines
    dist = this->fused->calcLineDist(node->pose.pos.x, node->pose.pos.y, node->pose.rot);
    dist = fabsf(laneWidth/2 - dist);

    lineDist[j] += dist;
    lineCount[j] += 1;
    
    // Compute the distance to the lane center, based on roughness
    dist = this->fused->calcRoughDist(0, node->pose.pos.x, node->pose.pos.y);
    dist = fabsf(laneWidth/2 - dist);
    
    roughDist[j] += dist;
    roughCount[j] += 1;
  }

  timeB = DGCgettime() - timeB;

  timeC = DGCgettime();

  // Pick the optimal rail, based on the aggregated stats.
  int minRail;
  float dist, minDist;
  minDist = FLT_MAX;
  minRail = 0;
  for (j = 0; j < railCount; j++)
  {
    if (lineCount[j] < 1 || roughCount[j] < 1)
      continue;
    dist = lineDist[j]/lineCount[j] + roughDist[j]/roughCount[j];
    if (dist < minDist)
    {
      minDist = dist;
      minRail = j - (railCount - 1) / 2;
    }
  }

  // Get all the lane nodes in the the ROI
  include = PLAN_GRAPH_NODE_LANE;
  exclude = PLAN_GRAPH_NODE_ONCOMING;
  graph->getRegion(&nodes, px, py, outerSize, outerSize, include, exclude);

  // Set the lane distances using a winner-take-all approach.
  for (i = 0; i < (int) nodes.size(); i++)
  {
    node->status.centerLineDist = abs(node->railId - minRail) * railSpacing;
    node->status.centerRoughDist = abs(node->railId - minRail) * railSpacing;
    node->status.centerTime = graph->getStatusTime();
  }

  timeC = DGCgettime() - timeC;
    
  MSG("fused %d nodes in %.3fms (prep) %.3fms (test) %.3fms (update)",
      (int) nodes.size(), (double) timeA/1e3, (double) timeB/1e3, (double) timeB/1e3);
    
  // Unlock access to the perceptor
  pthread_mutex_unlock(&this->fusedMutex);
  
  return 0;
}



// Update the graph state
int TrafficStateEst::updateStatus(PlanGraph *graph, StateProblem_t &problem)
{
  // get the latest map
  Map* map;
  MapElement mapEl;
  map = getMap();

  //VehicleState state;
  float ox, oy, sx, sy;
  vec2f_t pa, pb;

  // Hard limits on Alice's size
  m_aliceInnerFront = DIST_REAR_AXLE_TO_FRONT;
  m_aliceInnerRear  = DIST_REAR_TO_REAR_AXLE;
  m_aliceInnerSide = VEHICLE_WIDTH/2;
  
  // Set Alice's size based on the state problem
  switch (problem.obstacle) {
    case OBSTACLE_AGGRESSIVE:
      // If obstacle safety is aggressive, then use 1m in front and .5m on the sides
      m_aliceOuterFront = m_aliceInnerFront + OBS_AGGR_FRONT;
      m_aliceOuterRear = m_aliceInnerRear + OBS_AGGR_REAR;
      m_aliceOuterRight = m_aliceInnerSide +  OBS_AGGR_RIGHT;
      m_aliceOuterLeft = m_aliceInnerSide + OBS_AGGR_LEFT;
      break;
    case OBSTACLE_BARE:
      // If obstacle safety is bare, then do not grow alice
      m_aliceOuterFront = m_aliceInnerFront + OBS_BARE_FRONT;
      m_aliceOuterRear = m_aliceInnerRear + OBS_BARE_REAR;
      m_aliceOuterRight = m_aliceInnerSide + OBS_BARE_RIGHT;
      m_aliceOuterLeft = m_aliceInnerSide + OBS_BARE_LEFT;
      break;
    default:
      m_aliceOuterFront = m_aliceInnerFront + OBS_SAFE_FRONT;
      m_aliceOuterRear = m_aliceInnerRear + OBS_SAFE_REAR;
      m_aliceOuterRight = m_aliceInnerSide + OBS_SAFE_RIGHT;
      m_aliceOuterLeft = m_aliceInnerSide + OBS_SAFE_LEFT;
      break;
  }
  m_aliceOuterDiam = sqrtf(powf(m_aliceOuterFront + m_aliceOuterRear, 2) +
                            powf(m_aliceOuterRight + m_aliceOuterLeft, 2));
  
  // set graph status time
  graph->setStatusTime(DGCgettime());
  
  // get the vehicle state
  //  state = this->getVehState();
  
  // Project each obstacle into the ROI.
  int i, j;
  int numElems = 0;
  for (i = 0; i < (int)map->usedIndices.size(); i++)
  {
    map->getFusedEl(mapEl,i);

    if (!mapEl.isObstacle())
      continue;
    if (mapEl.geometry.size() == 0)
      continue;

    numElems++;

    // Get the bounding box for the obstacle (site frame)
    this->getSiteBox(vec2f_set(mapEl.geometryMin.x, mapEl.geometryMin.y), 
                     vec2f_set(mapEl.geometryMax.x, mapEl.geometryMax.y),
                     &ox, &oy, &sx, &sy);

    // Expand the bounding box by the size of the vehicle box.
    sx += 2 * m_aliceOuterDiam;
    sy += 2 * m_aliceOuterDiam;

    // Check the bounding box against the graph; if there are no
    // nodes in this bounding box, ignore the obstacle.
    if (!this->checkBox(graph->root, ox, oy, sx, sy))
      continue;

    //MSG("testing obstacle %d/%d", i, this->numObs);
    for (j=0; j<(int)mapEl.geometry.size(); j++) {
      pa = vec2f_set(mapEl.geometry[j].x, mapEl.geometry[j].y);
      if (j+1<(int)mapEl.geometry.size())
        pb = vec2f_set(mapEl.geometry[j+1].x, mapEl.geometry[j+1].y);
      else
        pb = vec2f_set(mapEl.geometry[0].x, mapEl.geometry[0].y);
      this->updateLineStatus(pa, pb, graph, !mapEl.isVehicle(), mapEl.isVehicle(), mapEl.isPredicted());
    }
  }

  // get all the obstacles
  Log::getStream(1) << "  Number of elements in the map: " << numElems << endl;
  //  Console::addMessage(" Number of elements in map %d ", numElems);
  Console::updateMapSize(numElems);

  
  return 0;
}


// Get the bounding box for the obstacle (site frame)
void TrafficStateEst::getSiteBox(vec2f_t min, vec2f_t max,
                                 float *ox, float *oy, float *sx, float *sy)
{
  *ox = (min.x + max.x) / 2;
  *oy = (min.y + max.y) / 2;
  *sx = (max.x - min.x);
  *sy = (max.y - min.y);

  return;
}


// Check for any nodes in the obstacle bounding box (site frame)
bool TrafficStateEst::checkBox(PlanGraphQuad *quad, float ox, float oy, float sx, float sy)
{
  int i;
  PlanGraphQuad *leaf;
  PlanGraphNode *node;
  
  // Check for intersection between the quad and the box.
  if (!quad->hasIntersection(ox, oy, sx, sy))
    return false;

  // Check our children
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (!leaf)
      continue;    
    if (this->checkBox(leaf, ox, oy, sx, sy))
      return true;
  }

  // Check our static nodes to see if any fall inside the bounding box
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];
    if (node->pose.pos.x < ox - sx/2)
      continue;
    if (node->pose.pos.x > ox + sx/2)
      continue;
    if (node->pose.pos.y < oy - sy/2)
      continue;
    if (node->pose.pos.y > oy + sy/2)
      continue;
    return true;
  }

  return false;
}


// Update the status values along a line
void TrafficStateEst::updateLineStatus(vec2f_t pa, vec2f_t pb, PlanGraph *graph, bool obs, bool car, bool pred)
{
  int i, numSteps;
  float length, spacing;
  float px, py, dx, dy;

  // MAGIC
  // Spacing of the test points
  spacing = 0.5;
  
  length = vec2f_mag(vec2f_sub(pb, pa));
  numSteps = (int) (ceil(length / spacing));
  assert(numSteps > 0);

  // Compute line parameters
  px = pa.x;
  py = pa.y;
  dx = (pb.x - pa.x) / numSteps;
  dy = (pb.y - pa.y) / numSteps;

  // Walk the line
  for (i = 0; i < numSteps; i++)
  {
    //printf("line %d %f %f\n", i, px, py);    
    this->updateQuadStatus(graph->root, graph, px, py, obs, car, pred);
    px += dx;
    py += dy;
  }
  
  return;
}


// Update the status values for intersecting nodes
void TrafficStateEst::updateQuadStatus(PlanGraphQuad *quad, PlanGraph *graph, 
                                         float ox, float oy, bool obs, bool car, bool pred)
{
  int i;
  PlanGraphQuad *leaf;
  PlanGraphNode *node;
  float px, py;
  float m[3][3];
        
  // See of the quad can contain any node that overlaps this point.
  if (pred)
  {
    if (!quad->hasIntersection(ox, oy, 2.0, 2.0)) // MAGIC
      return;
  }
  else
  {
    if (!quad->hasIntersection(ox, oy, 2*m_aliceOuterDiam, 2*m_aliceOuterDiam))
      return;
  }

  // Check our children
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (!leaf)
      continue;    
    this->updateQuadStatus(leaf, graph, ox, oy, obs, car, pred);
  }

  // Check our nodes.
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];
    // TESTING
    //    if (graph->isStatusFresh(node->status.carTime)) continue;
    //    if (obs && graph->isStatusFresh(node->status.obsTime)) continue;

    // Compute transform from site frame to node frame.  If the value
    // has never been set, do it now.
    if (node->status.transTime == 0)
    {
      pose2f_to_mat33f(pose2f_inv(node->pose), m);
      memcpy(node->status.trans[0], m[0], sizeof(m[0]));
      memcpy(node->status.trans[1], m[1], sizeof(m[1]));
      node->status.transTime = graph->getStatusTime();
    }

    // Transform point into the node frame.
    px = node->status.trans[0][0]*ox + node->status.trans[0][1]*oy + node->status.trans[0][2];
    py = node->status.trans[1][0]*ox + node->status.trans[1][1]*oy + node->status.trans[1][2];

    if (pred && (px > 1.0 || px < -1.0 || py > 1.0 || py < -1.0))
      continue;

    // Update obstacle distances
    if (obs) 
    {
      if (!graph->isStatusFresh(node->status.obsDistTime))
      {
	node->status.obsSideDist = fabsf(py) - m_aliceInnerSide;
	node->status.obsFrontDist = m_aliceOuterFront - m_aliceInnerFront;
	node->status.obsRearDist = m_aliceOuterRear - m_aliceInnerRear;
	node->status.obsDistTime = graph->getStatusTime();
      }
      if (py > -m_aliceInnerSide && py < +m_aliceInnerSide)
      {
	if (px > 0)
	  node->status.obsFrontDist = MIN(node->status.obsFrontDist, +px - m_aliceInnerFront);
	else
	  node->status.obsRearDist = MIN(node->status.obsRearDist, -px - m_aliceInnerRear);
      }
      node->status.obsSideDist = MIN(node->status.obsSideDist, fabsf(py) - m_aliceInnerSide);
    }

    // Ignore points outside the Alice box
    if (px > +m_aliceOuterFront)
      continue;
    if (px < -m_aliceOuterRear)
      continue;
    if (py > +m_aliceOuterLeft)
      continue;
    if (py < -m_aliceOuterRight)
      continue;

    // Update the node
    if (obs)
    {
      node->status.obsCollision = true;
      node->status.obsTime = graph->getStatusTime();
    }
    if (car)
    {
      node->status.carCollision = true;
      node->status.carTime = graph->getStatusTime();
    }
  }    
  
  return;
}
