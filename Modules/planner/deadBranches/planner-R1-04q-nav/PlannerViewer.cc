
/* 
 * Desc: Planner viewer utility
 * Date: 29 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>

#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>

#include "PlannerArgs.hh"

#include "PlannerViewer.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "%s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
PlannerViewer::PlannerViewer()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
PlannerViewer::~PlannerViewer()
{
  return;
}


// Initialize stuff
int PlannerViewer::initGUI(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) PlannerViewer::onExit},
      {0},

      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) PlannerViewer::onAction,
       (void*) CMD_ACTION_PAUSE, FL_MENU_TOGGLE},
      {0},
      
      {"&View", 0, 0, 0, FL_SUBMENU},

      {"Alice", 'a', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_VIEW_ALICE + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},      
      {"RNDF", 'r', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_VIEW_RNDF + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Graph", 'g', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_VIEW_GRAPH + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Vector map", 'm', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_VIEW_MAP + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Fused map", 'f', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_VIEW_FUSED + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Lanes", 'n', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_VIEW_LANE + CMD_VIEW_FIRST), FL_MENU_TOGGLE},
      {"Trajectory", 't', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_VIEW_TRAJ + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_DIVIDER},
      
      // Graph display properties
      {"Plan graph", 0, 0, 0, 0},
      {"Lane changes", FL_CTRL + 'l', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_GRAPH_LANE_CHANGES + CMD_GRAPH_FIRST), FL_MENU_TOGGLE},
      {"Node status", FL_CTRL + 's', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_GRAPH_NODE_STATUS + CMD_GRAPH_FIRST),
       FL_MENU_TOGGLE | FL_MENU_VALUE | FL_MENU_DIVIDER},

      // Fused map display properties
      {"Fused map", 0, 0, 0, 0},
      {"None", FL_CTRL + 'n', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_FUSED_NONE + CMD_FUSED_FIRST), FL_MENU_RADIO | FL_MENU_VALUE},
      {"Lines", FL_CTRL + 'i', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_FUSED_LINES + CMD_FUSED_FIRST), FL_MENU_RADIO},
      {"Line dist", FL_CTRL + 'd', (Fl_Callback*) PlannerViewer::onAction,
       (void*) (CMD_FUSED_LDIST + CMD_FUSED_FIRST), FL_MENU_RADIO},
      
      {0},

      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows + 30, "DGC Planner Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new Fl_Glv_Window(0, 30, cols, rows, this, (Fl_Callback*) onDraw);

  this->mainwin->end();

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  // Set the initial POV
  this->worldwin->set_hfov(40.0);
  this->worldwin->set_clip(4, 1000);
  this->worldwin->set_lookat(-20, -20, -20, 0, 0, 0, 0, 0, -1);

  // Set consistent menu state
  this->viewLayers = (1 << CMD_VIEW_ALICE) | (1 << CMD_VIEW_RNDF) |
    (1 << CMD_VIEW_GRAPH) | (1 << CMD_VIEW_MAP) | (1 << CMD_VIEW_FUSED);
  this->graphProps = (1  << CMD_GRAPH_NODE_STATUS);
  this->mapProps = 0;
  this->fusedProps = (1 << CMD_FUSED_LINES);

  this->rndfList = 0;
  this->dirty = true;
  
  return 0;
}


// Finalize stuff
int PlannerViewer::finiGUI()
{  
  return 0;
}


// Handle menu callbacks
void PlannerViewer::onExit(Fl_Widget *w, int option)
{
  PlannerViewer *self;

  self = (PlannerViewer*) w->user_data();
  self->quit = true;

  return;
}


// Handle menu callbacks
void PlannerViewer::onAction(Fl_Widget *w, int option)
{
  PlannerViewer *self;  

  self = (PlannerViewer*) w->user_data();
  if (option == CMD_ACTION_PAUSE)
    self->pause = !self->pause;
  
  // Check for changes in the selected display viewLayers
  if (option >= CMD_VIEW_FIRST && option < CMD_VIEW_LAST)
  {
    self->viewLayers = (self->viewLayers ^ (1 << (option - CMD_VIEW_FIRST)));
    self->dirty = true;
    self->worldwin->redraw();
  }

  // Check for changes in the selected graph properties
  if (option >= CMD_GRAPH_FIRST && option < CMD_GRAPH_LAST)
  {
    self->graphProps = (self->graphProps ^ (1 << (option - CMD_GRAPH_FIRST)));
    self->dirty = true;
    self->worldwin->redraw();
  }

  // Check for changes in the selected fused map properties
  if (option >= CMD_FUSED_FIRST && option < CMD_FUSED_LAST)
  {
    self->fusedProps = (1 << (option - CMD_FUSED_FIRST));
    self->dirty = true;
    self->worldwin->redraw();
  }

  return;
}


// Handle draw callbacks
void PlannerViewer::onDraw(Fl_Glv_Window *win, PlannerViewer *self)
{
  VehicleState state;
  PlanGraph *graph;
  Map *map;

  // Get the state; the one state in the traffic state estimator seems
  // to be canonical.
  state = self->planner->m_traffStateEst->getVehState();
  
  // Get the graph so we know the global to graph offset
  graph = &self->planner->m_graph;
  assert(graph);

  // Get the vector map
  map = self->planner->m_traffStateEst->getMap();
  
#if USE_FUSED
  // Lock the fused perceptor while we play with it
  FusedPerceptor *fused;
  fused = self->planner->m_traffStateEst->lockFused();
  assert(fused);
#endif

  // Predraw the static RNDF if necessary
  if (self->rndfList == 0)
    self->rndfList = self->planner->m_graph.rndf.predraw();
  
  // Predraw if necessary
  if (self->dirty)
  {
    // Draw the graph
    if (self->viewLayers & (1 << CMD_VIEW_GRAPH))
      self->predrawGraph(graph, &state, self->graphProps);
      
    // Draw the vector map
    if (self->viewLayers & (1 << CMD_VIEW_MAP))
      self->predrawMap(map, self->mapProps);
    
#if USE_FUSED
    // Draw the fused map
    if (self->viewLayers & (1 << CMD_VIEW_FUSED))
      self->predrawFused(fused, self->fusedProps);

    // Draw the lane distances
    if (self->viewLayers & (1 << CMD_VIEW_LANE))
    {
      float size;
      size = fused->localMap->size * fused->localMap->scale;
      self->predrawLane(graph, state.siteNorthing, state.siteEasting, size);
    }
#endif

    self->dirty = false;
  }    
    
  // Switch to graph frame
  glPushMatrix();

  // Do a translation to center on the vehicle
  glTranslatef(-state.siteNorthing, -state.siteEasting, -state.siteAltitude);

  // Switch to site frame
  if (true)
  {
    glPushMatrix();
    
    // Translate z so that the maps are directly beneath the vehicle
    glTranslatef(0, 0, state.siteAltitude + VEHICLE_TIRE_RADIUS);

    // Draw the RNDF 
    if (self->viewLayers & (1 << CMD_VIEW_RNDF))
      glCallList(self->rndfList);  

    // Draw the graph
    if (self->viewLayers & (1 << CMD_VIEW_GRAPH))
    {
      glCallList(self->graphList);
      if (self->graphProps & (1  << CMD_GRAPH_NODE_STATUS))
        glCallList(self->statusList);
    }

    // Draw the vector map
    if (self->viewLayers & (1 << CMD_VIEW_MAP))    
      glCallList(self->mapList);

    // Draw the lane distances
    if (self->viewLayers & (1 << CMD_VIEW_LANE))    
      glCallList(self->laneList);

    // Draw the planned path
    if (true)
      self->drawPath(&self->planner->m_path);

    glPopMatrix();
  }

  // Switch to vehicle frame
  if (true)
  {
    self->pushFrameVehicle(&state);
    
    // Draw Alice
    if (self->viewLayers & (1 << CMD_VIEW_ALICE))
    {
      self->drawAxes(1.0);
      self->drawAlice();
    }
  
    self->popFrame();
  }

  // Switch to local frame
  if (true)
  {
    self->pushFrameLocal(&state);

    // Translate z so that the maps are directly beneath the vehicle
    glTranslatef(0, 0, state.localZ + VEHICLE_TIRE_RADIUS);

    // Draw the enabled layers (local frame)
    if (self->viewLayers & (1 << CMD_VIEW_FUSED))
      glCallList(self->fusedList);  

    // Draw the trajectory we actually sent (local frame)
    if (self->viewLayers & (1 << CMD_VIEW_TRAJ))
      self->drawTraj(&self->planner->m_sentTraj);

    self->popFrame();
  }

  glPopMatrix();

#if USE_FUSED
  // Done now; unlock the fused perceptor
  self->planner->m_traffStateEst->unlockFused();
#endif
  
  return;
}


// Switch to the vehicle frame 
void PlannerViewer::pushFrameVehicle(const VehicleState *state)
{
  PlanGraph *graph;
  pose3_t pose;
  float m[4][4];
  
  graph = &this->planner->m_graph;
  
  // Transform from vehicle to site frame
  pose.pos = vec3_set(state->siteNorthing, state->siteEasting, state->siteAltitude);
  pose.rot = quat_from_rpy(state->siteRoll, state->sitePitch, state->siteYaw);
  pose3_to_mat44f(pose, m);

  // Transpose to column-major order for GL
  mat44f_trans(m, m);
  
  glPushMatrix();
  glMultMatrixf((GLfloat*) m);
  
  return;
}


// Switch to the map local frame
void PlannerViewer::pushFrameLocal(const VehicleState *state)
{
  pose3_t transSV, transLV;
  float m[4][4];

  // Transform from vehicle to site frame
  transSV.pos = vec3_set(state->siteNorthing, state->siteEasting, state->siteAltitude);
  transSV.rot = quat_from_rpy(state->siteRoll, state->sitePitch, state->siteYaw);

  // Transform from vehicle to local frame
  transLV.pos = vec3_set(state->localX, state->localY, state->localZ);
  transLV.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);

  // Transform from site to local frame
  pose3_to_mat44f(pose3_mul(transSV, pose3_inv(transLV)), m);

  // Transpose to column-major order for GL
  mat44f_trans(m, m);
  
  glPushMatrix();
  glMultMatrixf((GLfloat*) m);
  
  return;
}


// Revert to previous frame
void PlannerViewer::popFrame()
{
  glPopMatrix();  
  return;
}


// Handle idle callbacks
void PlannerViewer::onIdle(PlannerViewer *self)
{
  if (!self->pause)
  {  
    // Update perceptor
    if (self->update() != 0)
      self->quit = true;

    // Redraw the display
    self->worldwin->redraw();
  }
  else
  {
    // Sleepy bye-bye
    usleep(0);
  }
  
  return;
}


// Parse command-line options
int PlannerViewer::parseCmdLine(int argc, char **argv)
{
  // Run parser
  if (planner_cmdline(argc, argv, &this->options) != 0)
  {
    planner_cmdline_print_help();
    return -1;
  }

  // Suck out options into CmdArgs DEPRECATE
  plannerArgs(argc, argv, &this->options);
  
  return 0;
}


// Initialize the planner
int PlannerViewer::init()
{
  // Create planner
  this->planner = new Planner(&this->options);

  // Start the portHandler thread
  this->planner->GcSimplePortHandlerThread::ReStart();

  return 0;
}


// Finalize the planner
int PlannerViewer::fini()
{
  // Stop the portHandler thread
  this->planner->GcSimplePortHandlerThread::Stop();
  
  // Clean up
  delete this->planner;
  this->planner = NULL;
  
  return 0;  
}


// Update the planner
int PlannerViewer::update()
{
  // Run through the update cycle; assume we re-display at the same
  // rate (more-or-less) as the control cycle.
  while (DGCgettime() - this->dirtyTimestamp < (uint64_t) (1e6/this->options.rate_arg))
  {
    this->planner->arbitrate(&this->planner->m_controlStatus, &this->planner->m_mergedDirective);
    this->planner->control(&this->planner->m_controlStatus, &this->planner->m_mergedDirective);
  }

  this->dirty = true;
  this->dirtyTimestamp = DGCgettime();
  
  return 0;
}


int main(int argc, char *argv[])
{
  PlannerViewer *app;

  // Initialize GLUT calls
  glutInit(&argc, argv);

  app = new PlannerViewer();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  if (app->initGUI(1024, 768) != 0)
    return -1;

  if (app->init() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) PlannerViewer::onIdle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  app->finiGUI();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
