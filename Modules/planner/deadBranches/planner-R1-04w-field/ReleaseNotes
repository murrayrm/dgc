              Release Notes for "planner" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "planner" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "planner" module can be found in
the ChangeLog file.

Release R1-04w-field (Mon Nov 12  8:39:54 2007):
        * Changed the exit condition for the zone region. 
        * Changes in the field.
        * Decrease growth of obstacles for NQE A
        * added some logging info to planner. reduced obstacle 
persistence in graph from 3 seconds to 1 second.
        * Switched to optimized collision detection
        * new zone perimeter and parking spot thresholds
        * Changed rate limiting approach
        * changed final condition coming through intersectinos to allow 
completion of mission
        * NQE run 3
        * Fixed a bug in the subPath population when we do not have any 
path. Was causing segfaults and bizare behavior.
        * Pass transpos to logic planner
        * added fcn call to display planner state on mapviewer.
        * Fix of the bug in the extract subpath fcn for rail planner.
        * Add turn signals
        * Reverted sensed lane cost to lane rails only
        * removed silly error message for rail planner in zones.
        * Reduced the bounding box around alice for s1planner.
        * Added costs for driving into oncoming cars
        * Added curvature cost

Release R1-04w (Thu Oct 25  0:00:49 2007):
        * adding a bunch of functionality for dplanner.  All the 
committed code should only affect dplanner.
        * Overriding ROA when in START_CHUTE mode
        * Added untested functionality to use rail planner for backup. Does not break anything else though.
        * Changed the extractSubPath fcn call in zones for 
circle-planner.
        * adding replan functionality for dplanner -- it will replan if 
an obstacle crosses the path.
        * adding functionality so dplanner replans immediately after 
finishing a backup segment.
        * Added evasion maneuver
        * added debug info to track down pathLen problem
        * Modified evasive mode to call setGoalRadius
        * Populate the startControls field with current steering angle 
(in radians).
        * Populating evasion field for LogicParams
        * When signaling while being near intersection, do not send this 
information to prediction
        * augmenting logic for dplanner 
        * Implemented more intelligent obstacle ignoring logic. Tested 
the lateral distance setting functions. (temporarily) increased the 
goals distance to get rid of the pathLen problem. Need to investigate some more though.
        * Fixed bug where Planner first planner to the exit of the zone 
rather than the parking spot
        * Field fixes
        * Added failure handling for s1planner + not using s1planner 
when in pause
        * Reduced s1planner failure count threshold
        * Dont play DARPA ball while NOT in DRIVE/STOP_OBS
        * set the ignore mode in utils so that the dist to obs function 
is syncronized with the current behavior.
        * reread config file
        * Passes along the merging waypoints from logic-planner to 
prediction while merging at right-of-way intersection
        * Added aerial image overlay
        * Modified slightly confusing menu options
        * Populating the side dist to obstacle even if nodes do not 
collide
        * Populating correctly obsDist fields now
        * Added winner-take-all rail selection for the fused-perceptor
        * Added cost terms for glancing collisions (sideDist)

Release R1-04v (Mon Oct 22  8:33:45 2007):
        * populate the new collision flags based on the lateral distance 
from the path to the nearest obstacle.
        * Added additional distance calculations
        * implemented setting the max lat error that we want to allow 
based on the path and proximity of obstacles close to us.
        * Outputs timestamp to log files
        * Added the final condition check for parking spot for circle 
planner.
        * Plans further ahead (3 seggoals if possible). Implemented list 
of waypoints that we would like to hit.
        * now passes the set of entry/exit waypoint that we DO NOT want 
to exclude. This is set based on the seg goals from mplanner and is used 
to constrain the rail planner to stay on the correct segments.
        * added some execution time logging to planner.
        * planner calls updatePathLaneId to make sure that all nodes in 
the path have proper laneId information
        * Added a vector of waypoints that we would like to hit. 
Actually defines the complement of the entry/exit points we want to 
exclude.

Release R1-04u (Thu Oct 18 11:18:37 2007):
	* Added USE_FUSED flag
	* Switched over drawing functions
	* implemented final pos spec and correct flag spec for driving in off road mode.
	* PlannerViewer now compiles + changed isGoalComplete to avoid the
	* no completion* problem when planner is delayed
	* Field changes: 
	* trying to debug why circle planner is not playing nicely with circle-planner 
	* debugging why rail planner sometimes fail to find the optimal path, or any path. 
	* Implemented another extractSubpath function for circle-planner * played with the costs a little.
	* adapted to new updatePathStoplines interfaces
	* ROA overriding in place
	* Implemented PASS_REV
	* Fixed bug in the if PASS_REV statement
	* Implementing Darpa Ball functionality
	* Fixed bug where planner reported goal completion to mplanner when it finished backing up
	* Added path fixing
	* Set readConfig flag for logic planner
	* Added support for speed/curvature costs
	* Changed ordering of path fixing/unfixing; need to check interaction with other planners
	* fixed segfault for when we switch between planners.
	* Added fused perceptor options
	* Added support for initial direction in rail planner
	* Updated the function call to circle-planner to be consistent with changes in the latest circle planner.
	* Tweaked visualization and plan costs
	* Visualization tweaks
	* Tweaks
	* Added support for using both lines and roughness
	* Fixed new map/graph update code and added hacky support for roughness
	* Distance to obstacle for nodes (close enough to an obstacle) is now populated
	* Display tweaks
	* Fixed bug in TrafficStateEst.cc where the local to site frame	transformation wasn't being updated over time.	This allows us to run astate without the --fix-xy option. 
	* increased cost outside of the RNDF to make sure that s1planner doesn't go off of the RNDF. This will really hurt its ability to "just get us through" but will reduce our likelihood of 
ending up completely off the road and lost
	* Added terms for speed/curvature costs

Release R1-04t (Tue Oct 16 21:37:46 2007):
	* implemented loading planner params from a file. This allows us to
	switch between graph updating modes. Implemented a sparse cmd arg
	to specify a segment as sparse. Implemented a (hopefully) faster
	graph-update function to associate obstacles with nodes.
	* updatePathStoplines move to temp-planner-interfaces and modified it
	* fixed backup completion test. Adjusted the bounding box around
	alice in the rail planner.
	* changed fcn call to align with PlanGraphUpdater fcn.
	* Changed to new getDistToStopLine structure
	* Putting changes from the field

Release R1-04s (Mon Oct 15 23:52:48 2007):
	* implemented disable-fused-perceptor and --visualization-level 
args
	* updated stoplines on the path
	* clean up isGoalComplete fcns
	* updated uturn complete fcn to complete when Alice is in 
correct lane with correct heading
	* updated uturn final pose spec
	* implemented visualization level
	* implemented parking spot completion (and fixed bugs)
	* implemented ability to re-read path_plan params file
	* fixed zone completion code
	* planner is now able to start in a zone
	* tweaked corridor costs for uturn
	* adjusted the costs for lane changing with soft AND hard 
constraints. Default is hard constraints. Can switch between hard and 
soft constraints by changing path_plan.config and then hitting 'u' in 
the console.

Release R1-04r (Mon Oct 15  8:40:07 2007):
	Major release with changes for new Rail Planner
	* added back prediction, circle-planner
	* Re-enabled fused-perceptor
	* Added support for lazy-update (graph freshness)
	* Updated backup/uturn final conditions and completion check
	* Re-enabled veh sub-graph generation
	* fixed path reuse/volatile graph interaction
	* traj now sent in local frame - planner keeps traj in site- and local frames.
	* moved path replan code to planner (from path-planner)
	* fixed thread handling for clean exit
	* implemented an extract subpath fcn for s1planner
	* update stoplines from sensed info (needs to be verified, updated)
	* update obstacle painting and ignores obstacles far away
	* now checks path to ensure it makes any sense (pose-wise).
	* taking into account the obstacle safety field
	* added unfeasibleCost and unfeasibleExponent
	* Tweaked lane change cost, but likely have to switch back to hard constraints.

Release R1-04q (Thu Oct 11  2:08:56 2007):
	* Do not paint lane line when we're in uturn state.
	* Handle backup directive from mplanner.

Release R1-04p (Thu Oct 11  0:38:50 2007):
  Major revision to support new graph structure, new rail planner, 2D poses and
  site frame.  Will have bugs.  You have been warned.

Release R1-04o (Mon Oct  8 22:15:03 2007):
        GraphUpdater::updateGraphMap is now called from Planner.cc I.e. stop lines should now be updated if run with command line option --update-stoplines.

Release R1-04n (Mon Oct  8 20:52:28 2007):
	* Changed the check for switching out of backup to
	now account for both the path and the final condition specified.

Release R1-04m (Mon Oct  8 19:43:32 2007):
	* Increased out of road cost to 200 for S1Planner to prevent it from going all over the place

Release R1-04l (Mon Oct  8 17:55:29 2007):
	* Implemented bread crumbs to keep track of where Alice drove (for about 20m).
	* Set up the back up problem better, taking us back along our previous path,
	or straight back if we do not have enough history.

Release R1-04k (Mon Oct  8 15:33:23 2007):
	* Painted road: right now the cost is 100 outside the road
	and 1 inside the road (I tried playing around with the number. 
	When I used 20 as Kenny suggested
	we drove out of the road instead of making a uturn most of the time.
	so I increased it to 100.)
	* Gaussian drop off for obstacle


Release R1-04j (Sat Oct  6 16:45:35 2007):
	* Updates Console::Prediction from Planner now
	* took out the planFromCurrPos code for now since it is breaking the planner while genVehSubgraph 
fcn does not work.
	* Copied over code checking if we have completed special mans from
        Planner.cc in the team01 sandbox of Alice (used at the last el Toro
        test). Verified repeatedly in simulation that we switch back to
        rail planner from S1 planner after backups and u-turns.
	* Changes the path-planner config to use the dijkstra instead of the
        astar.
	* Added the changes that Jeremy had made to the dplanner code in
        planner.
	* removed some debugging information being displayed and printed to
        the log.
	* Painting road boundaries in planning with s1planner in road
        regions.  --vc

Release R1-04i (Sat Oct  6 14:17:41 2007):
	Updated TrafficStateEst::UpdateMap function to automatically update
	the local to site transforms for the map

Release R1-04h (Fri Oct  5 15:02:24 2007):
	fixed buges in christians release

Release R1-04g (Fri Oct  5  9:16:41 2007):
	changed output from prediction to console

Release R1-04f (Thu Oct  4  0:23:22 2007):
	implemented some uturn logic, but it still doesn't work- we successfully complete a uturn with s1planner and switch back to the rail planner, 
	but then we seg fault

	tuned some parameters- less time for s1planner to plan (it doesn't need as long when we update the clothoid tree), and
	decreased the distance we have to be from the end of a segment in a particular direction before switching to the part in the
	opposite direction to try to clear up gear shift issue

Release R1-04e (Wed Oct  3 20:30:03 2007):
  Tweaks for display and lane handling.

Release R1-04d (Wed Oct  3 19:06:51 2007):
	Added --no-failure-handling flag

Release R1-04c (Wed Oct  3 17:58:32 2007):
	Changed the order of starting up the threads to allow for longer graph load times.

Release R1-04b (Tue Oct  2 16:36:53 2007):
	Added possibility to change the path planner between Dijkstra and A* in the config file.

Release R1-04a (Tue Oct  2 15:21:24 2007):
	modified the way how ActutatorState gets updated now. This will provide the necessary information to underlying functions again that got broken. Intersections, etc work again

Release R1-04 (Tue Oct  2 11:58:25 2007):
	Moved print functions in planner utils to Utils.
	Updated function calls for above change.
	Commented out the UT in the PROJ_BINS to speed up compiling. 
	To use UT, just uncomment (easier than maintaining deprecated UT's)

Release R1-03z (Tue Oct  2  8:20:00 2007):
	Adapted to new output from intersection handler

Release R1-03y (Tue Oct  2  0:50:57 2007):
  This is a partial switch-over to using the site frame rather than the
  local frame.  The two are currently the same, so the switch should not
  yet have any major impact.  Also changed the Planner class such that
  it is no longer a CStateClient; this removes an unnecessary thread 
  (that's two down, nine to go).

Release R1-03x (Mon Oct  1 15:07:27 2007):
	Fixed a bug in the planner constructor that called the sparrow->run() fcn twice,
	causing a segfault.
	
	Changed the default compiling behavior: 
	ymk all compiles the libplanner and planner binary without otg support 
	(to allow off-campus users to compile without depending on proprietory code)
	ymk all "FLAVORS-libplanner=plain otg" "FLAVORS-planner=plain otg" compiles
	the includes the otg support in the library and binary (libplanner-otg and 
	planner-otg) as well as the plain library and binary (libplanner and planner).
	In this second version, the plain can be left out to compile only the otg version,
	but then the unit tests and planner-viewer do not compile.

Release R1-03w (Sat Sep 29 13:09:28 2007):
  * Integrated FusedPerceptor.
  * Added OpenGL viewer : planner-viewer.
  * Added OTG and non-OTG flavors of the library and planner executable.

Release R1-03v (Sun Sep 30 13:53:49 2007):
	Added --use-hacked-uturn and --use-hacked-backup cmd args. These are off by default
	and are used to command which algorithms to use for these maneuvers. By default it
	uses s1planner at the moment, but that needs more work, so use these flags!
	
	Rail planner now replans when we switch out of backup.

Release R1-03u (Sun Sep 30  8:36:26 2007):
	Fixed corridor creation for road regions using a zone planner (corridor and obstacles not populated properly).
	Added circle-planner to the zone-planner list for planning in road region.
	Initial implementation to use a zone planner for uturns and backup (not fully functional yet).

Release R1-03t (Sat Sep 29 18:06:41 2007):
	Taking command line argument no-fault-handling to disable fault handling in planner.  This 
also can be toggled on and off by pressing "f" on the planner console.  
Release R1-03s (Sat Sep 29 16:01:18 2007):
	Fixed the PAUSE logic. Planner should pause right away when
	mplanner tells it to do so instead of waiting for completion of
	previous directives. disable-console is now working.

Release R1-03r (Sat Sep 29 10:38:33 2007):
	Give mplanner reason for failing.

Release R1-03q (Sat Sep 29 10:00:36 2007):
	Fix for reading path planner costs from file: linking the file to etc directory
	somehow got lost (perhaps in merging with the latest release?). Fixed now.

Release R1-03p (Fri Sep 28 16:08:07 2007):
	Path-planner parameters can be re-read from a file upon request (from console).

Release R1-03o (Fri Sep 28 16:14:09 2007):
	Changed interfaces so that monitorAliceSpeed gets the correct estop unit from the derived ActuatorState

Release R1-03n (Fri Sep 28 14:31:58 2007):
	Added back the dplanner functionality for zones. 
Release R1-03m (Fri Sep 28  7:00:04 2007):

	Updated TrafficStateEst::getLocalMapUpdate() thread function to run
	efficiently with mapper instance inside planner. 

  Added debugging output for internal mapper which can be visualized
	with mapviewer on subgroup -3.  

  For mapper external, implemented faster map data copy in the case when
	the new QUEUE_MAPELEMENTS option is not used which only copies
	portions of the map which have been updated and so it scales much
	better with large RNDFs.  QUEUE_MAPELEMENTS is still on by default
	in the case that mapper is run external to planner, though the speed
	benefits are not as significant now.  There is the possibility that
	the queue might introduce more latency when handling high volumes of
	sensor data so this should be looked at in the field.  

  Also the faster map copy function Map::copyMapData(Map *targetmap)
	might be useful to speed up the map copy operations in prediction.


Release R1-03l (Thu Sep 27 21:17:11 2007):
	Fixed bug in the updateGraphMap function.

Release R1-03k (Thu Sep 27 11:01:25 2007):
	Improved performance of map snapshot (copying the map for prediction still takes 50 ms in victorville).
	Overall performance gain in victorville is around 300 ms per cycles (for all the changes released so far).

Release R1-03j (Thu Sep 27  0:30:46 2007):
	Generating generic corridors and painting them appropriately in the case of using zone planners in road 
regions and intersections.  
	
Release R1-03i (Wed Sep 26 21:57:42 2007):
	Changed interface to logic-planner so that planner starts to replan if fault handling in logic-planner commands to do so

Release R1-03h (Wed Sep 26 20:49:29 2007):
	Implemented the new cmdline args: --update-lanes and --update-stoplines.
	Updated the function call to update the graph from the map.

Release R1-03g (Wed Sep 26 11:17:22 2007):
	Correctly populating the segments to load (when --step-by-step-loading is on).

Release R1-03f (Wed Sep 26  9:23:57 2007):
	Fixed circle planner calling option

Release R1-03e (Tue Sep 25 13:31:46 2007):
	Fixed bug preventing Alice from entering zones

Release R1-03d (Tue Sep 25 11:20:26 2007):
	Fixed bug preventing Alice from completing the mission after an intersection
	Pause now uses the previous path to brake on (rather than a path of length 1)

Release R1-03c (Tue Sep 25 11:10:07 2007):
	Interfaced circle-planner for zone planning. To use, give flag --use-circle-planner.
	Changed the replanning conditions and handling for s1planner.
	Implemented sending cost map to map viewer upon request. Use --show-costmap.
	Implemented setting different safety boxes around alice. This will be used during fault handling.

Release R1-03b (Mon Sep 24 18:36:15 2007):
	Changed interface to Prediction. Sends information when Alice wants to change lanes.

Release R1-03a (Sun Sep 23 15:18:29 2007):
	Added cmd line args for specifying the zone planner. 
--use-dplanner to use dplanner, --use-s1planner to use s1planner, and 
--use-circle-planner to use circle-planner. If nothing is specified, 
then the planner defaults to s1planner.

Release R1-03 (Fri Sep 21 21:29:12 2007):
	Creating an arbitraty perimeter around Alice and the exit point for zone-like planning. 

Release R1-02z (Fri Sep 21 20:14:11 2007):
	Cleaned up the zone-planning code by restructuring the planner. Implemented some functions to further assist with the planning problem specification for zone 
planners. This release effectively lost the functionality to interface with dplanner, but this will be put back (and cleaned up) soon. Initial integration of the 
circle planner.

Release R1-02y (Fri Sep 21 18:09:19 2007):
	Took prediction out of the planner loop. Prediction runs in its own thread now. Turning on/off prediction 
also turns the thread loop on and off. This could help in case the CPU load gets too high.

Release R1-02x (Fri Sep 21 16:04:22 2007):
	Major restructuring of planner to fascilitate fault handling.
	SPecifically, uses state problem from logic planner to choose planner and to set up planning problem. This was implemented for the regular driving and initial zone.
	Also, restructured the zone planning problem specification to make extension for fault handling much simpler.
	I started cleaning up the zone code, but that needs some more work.
	
	This is a pretty major restructuring, but does not add any functionality.

Release R1-02w (Fri Sep 21 12:21:53 2007):
	Added setting the direction properly in the case the path is not a subpath generated by 
s1planner. 
Release R1-02v (Fri Sep 21  5:09:47 2007):
Added basic support methods for the RNDF frame implementation. 
	Needs fused-perceptor RNDF frame update before full testing can be
	done.  The option to use the RNDF frame is off by default and can
	be enabled with the flag --use-rndf-frame.  Also improved structure
	of function calls for running mapper internal to planner.  This
	works well in simulation, but needs testing in the field to see if
	it fixed any issues

Release R1-02u (Thu Sep 20 13:43:21 2007):
	Finding and setting path direction in Cspecs for zone planning. 

Release R1-02t (Thu Sep 20 11:55:20 2007):
	Populating the CSpecsObstacle vector for dplanner for every replan.  
Release R1-02s (Thu Sep 20 10:53:06 2007):
	Using s1planner as seed for dplanner.  Extracting subpaths and resetting final conditions for compound 
trajectories for dplanner. Recoded replanning logic and error handling logic for zone planning.  

Release R1-02r (Tue Sep 18 19:07:57 2007):
	Reduced the size of obstacle sent by prediction.

Release R1-02q (Mon Sep 17  8:51:43 2007):
	Updated mapper calls in planner to allow for non-static local to
	global frame transform

Release R1-02p (Sat Sep 15 16:20:14 2007):
	Added timing info for createOtgTrajectory call (dplanner) and 
output to to log file.  
	Added replanning if Alice is not moving for more than 20 seconds 
in dplanner case as well. 

Release R1-02o (Sat Sep 15 13:04:15 2007):
	set the obstacles to be grown by 1 alice width in both directions (rather than a different amount in different directions

	added a call to cSpecs.setSafetyMargin to have it keep us ZONE_SAFETY_MARGIN away from any obstacles and the zone parimiter. currently
	ZONE_SAFETY_MARGIN is set to 0.5m; just search for ZONE_SAFETY_MARGIN in ZoneCorridor.cc if you want to change this number.
	note that the goal pose must have room for alice + the safety margin on all sides of her for clothoid planner to succeed. its ok if
	she would stick of the corridor, but if there is an obstacle that would less than the safety margin away from alice at the goal pose, then 
	clothoid planner woud fail 

Release R1-02n (Sat Sep 15  3:51:34 2007):
	*using proper map element accessor functions rather than
	ELEMENT_OBSTACLE and ELEMENT_VEHICLE 

	*commenting out return statement that caused corridor painting to
	return w/o doing anything if incoming obstacle was of type vehicle
	(This was actually Kenny's catch, but he's headed for bed)
	This *should* fix the problems seen in the field today where the 
	mapviewer and the costmap didn't agree on obstacles. 
	Kenny and I pulled Alice out of the shop, ran a zone simulation with 
	real sensor data, and the only times we saw that error were for 
	objects classified as moving vehicles. 

Release R1-02m (Fri Sep 14  6:39:06 2007):
	Minor adjustmens in the prediction part to work with the new map/mapper structure

Release R1-02l (Fri Sep 14  2:24:47 2007):
	completely changed the way replanning in zones is done- now with clothoid planner, we replan every cycle,
	but we replan by updating the clothoid tree rather than deleting it and planning from scratch.

Release R1-02k (Thu Sep 13 18:01:26 2007):
	Planner uses isObstacle and isVehicle functions now. Obstacles 
sent by prediction have their own type. The bug with clearing those 
obstacles is also solved.

Release R1-02j (Mon Sep 10 22:22:27 2007):	
	Populating cspecs with the point2arr representation of a map element for obstacles 
for planning in zones,
 
Release R1-02i (Mon Sep 10 19:09:30 2007):
	Populating cspecs with information relating to whether we are entering a parking spot, exiting a 
parking spot, or traversing a zone without parking (s1planner requirement).  

Release R1-02h (Mon Sep 10 15:47:02 2007):
	Added a state for zone planners and functionality to determine if we are entering a parking 
spot or not.  
Release R1-02g (Mon Sep 10 12:36:33 2007):
	Changed to getObsInZone call from map, however this does not work reliably.
	Re-incorporated attention module exit point information that had been overwritten in a 
previous release and fixed a bug for INTERSECTION_STRAIGHT. 
	
Release R1-02f (Fri Sep  7 19:17:59 2007):
	Added painting cost for obstacles in zone regions.  
Release R1-02e (Thu Sep  6 16:55:37 2007):
	Re-incorporated dplanner changes that were overwritten 
erroneously in the previous release.  Reset the final velocity condition 
for zones  to 0.2, the minimum requirement for dplanner.  
Release R1-02d (Wed Sep  5 13:25:04 2007):
	Added sending exit waypoint information to attention module for intersections.  

Release R1-02c (Wed Sep  5 11:56:15 2007):
	Fix possible segfault (next_goal not initialized in getFinalPose)

Release R1-02b (Tue Sep  4 20:27:31 2007):
	Added cmdline flag -use-dplanner and corresponding supoprt to use dplanner in the case of zones.  

Release R1-02a (Tue Sep  4  4:01:04 2007):
	Added new cmdline flag (--mapper-line-fusion-compat) to enable
        old line fusion behavior of fusing elements while adding them to
        the map when the internal mapper (with the external mapper, the
        old behavior is the only possible one). Default when using the
        internal mapper is to run the line fusion within the mapper main
        loop at 10Hz.

Release R1-02 (Tue Sep  4  1:20:15 2007):
	Tweaked some bitmap parameters.  
	Added smaller threshold for goal completions in zone, which will enable us to get into a parking spot properly before exiting the zone. 

Release R1-01z (Mon Sep  3 18:57:41 2007):
	Implemented cost painting for parking spots. 

Release R1-01y (Sun Sep  2 11:55:14 2007):
	Added timing information
	Replanning in zones when stopped for more than 20 seconds
	Fixed UTurn final pose
	Fixed the infinite loop issue

Release R1-01x (Sun Sep  2 17:10:35 2007):
	Fixed bug that was causing execution of planner to increase in zones.  Added cost map 
painting for zone perimeters.  

Release R1-01w (Sun Sep  2 11:25:44 2007):
	Output more information about prediction and re-compiled against latest temp-planner-interfaces and
	logic-planner

Release R1-01v (Sat Sep  1 17:26:09 2007):
	fixed bug where --mapper-enable-groundstrike-filtering caused
	mapper initialization to hang.	Should be able to run mapper
	internal to planner with all current options now. 

Release R1-01u (Thu Aug 30 17:56:44 2007):
	WE NOW HAVE ZONE OPERATION! (at least the initial working implementation)
	Fixed the extractSubPath function to correctly handle zones stages.
	Changed the isCurrectPathDone (isComplete) function to account for Alice' heading.

Release R1-01t (Thu Aug 30 17:26:53 2007):
	Minor adjustments for using prediction.

Release R1-01s (Thu Aug 30 14:22:59 2007):
	Planner is now planning ahead on the SegGoals sent by mplanner

Release R1-01r (Wed Aug 29 11:48:48 2007):
	Continued debugging of the zone region. Added some debugging information to assist with this process.
	Implemented feature where the zone path gets replanned if our new final position is different.

Release R1-01q (Wed Aug 29 19:05:13 2007):
	Changed isCurrentPathComplete to call a reverseProjection method with a path extension argument.  
Release R1-01p (Wed Aug 29 16:16:01 2007):
	changed data structures used by ZoneCorridor.cc to match modified cspecs structures

Release R1-01o (Wed Aug 29 11:21:44 2007):
	Implemented isCurrentPathComplete to replace isGoalComplete.  Checks goal completion to 0.5 m wrt to an exit point projection (if necessary) on 
the path.  For now Alice will stop at the end of goal completion.  Continuity will be handled in a later release.   
	
Release R1-01n (Wed Aug 29  8:41:25 2007):
	Initial implementation of more advanced UTurn handling, and Zone handling. Neither of these work
	fully, but at the same time it does not break existing functionality, so I wanted to get the 
	release out.
	TODO: track down bugs in extraction of subPath. Path from s1planner seems garbled. Add feature to 
	only replan in zone/uturn if environment changes (right now only plan once).

Release R1-01m (Tue Aug 28 12:35:09 2007):
	Adapted to new distToStopLine function 

Release R1-01l (Mon Aug 27 16:51:13 2007):
	adding command line option to planner that enables groundstrike
	filtering on internal map

Release R1-01k (Mon Aug 27  9:08:37 2007):
	Changed call to getDistFromStopLine to one in Utils that considers the graph for the calculation (more robust).  
Release R1-01j (Thu Aug 23 14:06:35 2007):
	Made hacked UTURN and BACKUP more robust

Release R1-01i (Thu Aug 23 16:43:26 2007):
	Fixed bug that would set the state to PASS_RIGHT in the middle of the intersection.  Sending NOMINAL planner state 
to the attention module.       

Release R1-01h (Thu Aug 23 14:00:18 2007):
	Changed the makefile to include the libraries now needed by the 
mapper. NOTE: you have to get the emap module for the planner to 
compile.

Release R1-01g (Tue Aug 21 19:17:16 2007):
	Adapted interface to Prediction and re-compiled against latest logic-planner

Release R1-01f (Tue Aug 21 18:23:08 2007):
	Not re-generating the vehicle sub-graph at intersections anymore

Release R1-01e (Tue Aug 21 16:48:32 2007):
	Removed a few messages in zones to be able to see what clothoid planner is doing.

Release R1-01d (Tue Aug 21  6:23:12 2007):
	Enabled the option to have mapper run inside a thread in planner. 
	This is off by default but can be turned on by setting the
	--mapper-use-internal flag.  Also the mapper options can be passed
	through from the command line and have a mapper prefix. 

Release R1-01c (Mon Aug 20 21:47:58 2007):
	Updated interface to IntersectionHandling so that it receives the graph now.

	DON'T USE --LOAD-GRAPH-FILES in this release unless you like debugging...

Release R1-01b (Mon Aug 20 19:45:36 2007):
	Fixed hacked UTurn maneuver

Release R1-01a (Mon Aug 20 18:30:42 2007):
	Added southface to communicate planner state to the Attention module.  Changed goal completion check to 
account for zone regions. 

Release R1-01 (Fri Aug 17 18:56:17 2007):
	Added call to clothoid planner trajectory generation and velocity planner for planning in zone 
regions.  Took out ocpspecs dependency from Makefile.yam. 

Release R1-00z (Thu Aug 16 20:10:51 2007):
	Removed usage of getWaypoint from Map (because not working with zones)
	Zones are still not working in planner yet

Release R1-00y (Wed Aug 15 18:01:40 2007):
	Correctly added the --enable-line-fusion flag (using CmdArgs now)
	Made selection of turning signal much faster (not using getLane from Map anymore)

Release R1-00x (Tue Aug 14 23:09:28 2007):
	Added a cmdline option to enable line fusion in Map
	(--enable-line-fusion, default off). Remember that
	--update-from-map is needed too for the planner to use the fused
	data.

Release R1-00w (Tue Aug 14 18:17:56 2007):
	Switched over to the cspecs interface to the zone planner. Changes were mostly in the ZoneCorridor.* files.

Release R1-00v (Tue Aug 14 13:16:24 2007):
	Forgot to add these files for the previous release...

Release R1-00u (Tue Aug 14 12:10:39 2007):
	Added ZoneCorridor.* and UT for zone region generation.
	Added multi-path-planner calls for different path planning problems.
	The Zone handling is not yet stable and should not be used until further notice. This does not break nominal planning.

Release R1-00t (Tue Aug 14  9:55:25 2007):
	Added new console that allows manual override of Intersection Handling, turn on/off prediction, override the current FSM State.

	The keys that are assigned to the new console are marked with a () in the console. Everything is lower case. One special case in the 
	intersection handling. Pressing "i" does not give the GO-command directly. It will just switch into the next state within the Intersection 
	Handling (from WAIT_LEGAL --> WAIT_POSSIBLE --> WAIT_CLEAR --> GO).

Release R1-00s (Mon Aug 13 19:59:40 2007):
	Added communication with Follower.  Changed as per new gcinterface 
specification for different types of pauses. 

Release R1-00r (Wed Aug  8 15:27:48 2007):
	Handling hacked backup/uturn maneuver

Release R1-00q (Wed Aug  8 17:30:17 2007):
	When Prediction used Particle Filters, it slowed down the planning loop drastically. Now planner decides which method prediction will use.

Release R1-00p (Wed Aug  8 13:22:32 2007):
	Now populates segment goals to LogicPlanner

Release R1-00o (Sat Aug  4  3:46:56 2007):
	Now populates velParams that is sent to VelPlanner

Release R1-00n (Fri Aug  3 20:45:29 2007):
	added --update-from-map arg

Release R1-00m (Fri Aug  3  8:46:06 2007):
	Integrated the interface to Prediction

Release R1-00l (Fri Aug  3  0:51:24 2007):
	Added argument --step-by-step-loading: causes the planner to load the RNDF segments (as graph nodes) only when needed
	Added argument --load-graph-files: causes the planner to load the graph nodes from pre-computed files rather than re-generating them
	Stopping the car while loading from the RNDF (with --step-by-step-loading on)
	Fixed mplanner recovery handling (if planner receives directive 1, it assumes that mplanner had to restart)

Release R1-00k (Tue Jul 31 19:43:08 2007):
	Calling updateGraphMap

Release R1-00j (Tue Jul 31 13:47:45 2007):
	The module prediction is now part of the planner and is linked as a library. It can be turned off by using the command 
	line argument --noprediction. The planning behaviour is not affected by prediction yet.

Release R1-00i (Fri Jul 27  9:28:53 2007):
	Enforcing continuity between plans
	UTurn implemented as a hack
	Added option for planner to close the loop by computing path from current position and only using feed-forward in gcfollower
	Controlling turn signal now

Release R1-00h (Tue Jul 24 15:36:13 2007):
	The console display is now thread (ctrl+S will not affect the planner). PlannerMain.cc is responsible for updating the 
console.
	Planner now takes a --log-level argument to set the gcmodule log level
	Modified CmdArgs to include the log filename

Release R1-00g (Tue Jul 24  8:38:21 2007):
	Added some doxygen documentation.

Release R1-00f (Mon Jul 23 19:42:42 2007):
	Added comments
	Modified console display (does not flicker anymore)

Release R1-00e (Thu Jul 19 18:45:39 2007):
	Modified the console display

Release R1-00d (Wed Jul 18 16:37:06 2007):
	Updated logic-planner. Now Intersection output is shown in the Console.

Release R1-00c (Wed Jul 18 12:34:57 2007):
	Made AliceStateHelper available to other planner libraries
	Correctly setting the errors received from the planner libraries

Release R1-00b (Mon Jul 16 13:54:42 2007):
	Now using Graph as Graph_t

Release R1-00a (Fri Jul 13 16:05:59 2007):
	Initial release of the planner module. At the moment this module 
interfaces with the mplanner at the top, and sends trajectories. The 
planner accepts the map, and an rndf.
	Four libraries where created that interfaces with the planner. 
These libraries where created in separate modules to allow for 
concurrent development. These are: path-planner, vel-planner, 
logic-planner, and graph-updater.
	There is also one temporary module (library) that contains 
selected files from GraphPlanner and a file that define the interfaces 
between the planner and it's libraries. This module will later be 
removed.
	Currently, the planner generates a maintains a graph and map. 
Inside the planner, the logic-planner is called. Next, the graph-updater 
is called to make the relavent changes (due to logic planner). The path 
planner then searches the graph to construct a path. This path is then 
passed to the velocity planner. The velocity planner returns a 
trajectory that can be evaluated by the planner and sent to follower.
	The current functionality includes: generate a graph from the 
rndf (using graph-planner fcn), update the graph with obstacles (basic), 
plan a path (using graph-planner fcns), plan velocity (using 
graph-planner function). Thus, there is no new functionality other than 
the planner itself and all the interfaces. The logic part has not been 
inserted. This only serves as a place to start.


Release R1-00 (Fri Jul  6 16:49:31 2007):
	Created.




















































<<<<<<< .working


=======

>>>>>>> .merge-right.r41199





<<<<<<< .working






=======

>>>>>>> .merge-right.r42000

































