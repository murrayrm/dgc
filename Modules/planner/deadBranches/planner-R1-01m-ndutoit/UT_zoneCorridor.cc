/**********************************************************
 **
 **  UT_ZONECORRIDOR.CC
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 23:28:44 2007
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <s1planner/tempClothoidInterface.hh>
#include <path-planner/PathPlanner.hh>
#include <graph-updater/GraphUpdater.hh>

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);
  map->prior.delta = point2(3778410.5+2876, 403942.3-4914);
  CmdArgs::sn_key = skynet_findkey(argc, args);
  //CmdArgs::sn_key = 130;
  //int sn_key = (int)CmdArgs::sn_key;

  cout<<"Loaded RNDF"<<endl; 

  // set up some problem to solve
  CSpecs_t cSpec;
  Path_params_t params;

  // Specify some zone to plan in
  params.flag = ZONE;
  params.planFromCurrPos = false;
  //params.zoneId = 14;
  params.zoneId = 6;
  params.spotId = 1;
  params.exitId = 2;
  params.velMin = 0;
  params.velMax = 5;

  // Specify some vehicle state
  VehicleState vehState;
  memset(&vehState, 0, sizeof(vehState));
  vehState.localX = -10;
  vehState.localY = 10;
  vehState.localZ = 0;
  vehState.localYaw = -M_PI/2;

  point2 first, second;
  SpotLabel spotLabel(params.zoneId, params.spotId);
  map->getSpotWaypoints(first, second, spotLabel);
  cout<<"Got map waypoints"<<endl; 

  double finHeading = atan2(second.y-first.y, second.x-first.x);
  cout << "heading = " << finHeading << endl;
  pose3_t finPose;
  finPose.pos.x = second.x;
  finPose.pos.y = second.y;
  finPose.pos.z = 0;
  finPose.rot = quat_from_rpy(0, 0, finHeading);

  // get the zone region
  point2arr zonePerimeter;
  map->getZonePerimeter(zonePerimeter, params.zoneId);
  zonePerimeter.push_back(zonePerimeter[0]);
  cout<<"Got zone perimeter"<<endl; 

  // Generate the appropriate corridor
  Path_t* path = new Path_t();

  ZoneCorridor::generateCorridor(cSpec, zonePerimeter, params, finPose, vehState);

  cout<<"Generated corridor"<<endl; 
  
  double runtime = 0.2;
  ClothoidPlannerInterface::GenerateTrajOneShot(CmdArgs::sn_key, true, cSpec, path, runtime);

  cout<<"Generated path of length = "<< path->pathLen << endl;

  // DISPLAY PATH
  if (0) {
    int sendSubgroup = 10;
    CMapElementTalker meTalker;
    meTalker.initSendMapElement(CmdArgs::sn_key);
    point2 point;
    vector<point2> points;
    point2 start, end;
    MapId mapId = 12500;
    GraphNode* node;
    MapElement me;
    
    for (int i=0; i<path->pathLen; i++) {
      node = path->path[i];
      if (!node) break;
      point.set(node->pose.pos.x,node->pose.pos.y);
      points.push_back(point);
    }
    me.setId(mapId);
    me.setTypeLine();
    me.setColor(MAP_COLOR_DARK_GREEN, 100);
    me.setGeometry(points);
    meTalker.sendMapElement(&me,sendSubgroup);
    cout << ((point2arr)points) << endl;
  }

  delete path;
}
