
/* 
 * Desc: Copy options to the arguments class; this should be removed
 * Date: 30 September 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <sys/time.h>
#include <sys/stat.h>
#include <iostream>
#include <sstream>
#include "skynet/skynet.hh"
#include "dgcutils/cfgfile.h"
#include "temp-planner-interfaces/CmdArgs.hh"
#include "PlannerArgs.hh"


// Fill out the options.  This function should be DEPRECATED
int plannerArgs(int argc, char **argv, struct planner_options *options)
{
  string logFileName;
    
  /* Get the skynet key */
  CmdArgs::sn_key = skynet_findkey(argc, argv);
 
  /* Is console enabled? */
  CmdArgs::console = !options->disable_console_given;

  /* What is the verbose level? */
  if (options->verbose_given) {
    CmdArgs::verbose_level = options->verbose_arg;
    if (CmdArgs::verbose_level > 0) CmdArgs::debug = true;
  }

  if (options->log_level_given) {
    CmdArgs::log_level = options->log_level_arg;
  }
    
  /* Get the RNDF filename */
  if (options->rndf_given){
    CmdArgs::use_RNDF = true;
    CmdArgs::RNDF_file = options->rndf_arg;
    if (!CmdArgs::console)
      cout << "RNDF Filename in = "  << CmdArgs::RNDF_file << endl;
    /* The RNDF gets loaded into the map inside the Planner constructor */
  }     

  /* Plan from current pos? (closed loop)*/
  CmdArgs::closed_loop = options->closed_loop_flag;

  /* Use Prediction ? */
  CmdArgs::noprediction = options->noprediction_flag;

  /* Step-by-step loading of the graph */
  CmdArgs::stepbystep_load = options->step_by_step_loading_flag;

  /* Loading graph from files */
  CmdArgs::load_graph_files = options->load_graph_files_flag;

  /* Updating graph from map */
  CmdArgs::update_from_map = options->update_from_map_flag;

  /* do we want to run with the fused perceptor?*/
  CmdArgs::disable_fused_perceptor = options->disable_fused_perceptor_given;

  /* Updating the cost for centerline dists based on sensed lane information */
  CmdArgs::update_lanes = options->update_lanes_given;

  /* Updating the stoplines based on sensed info */
  CmdArgs::update_stoplines = options->update_stoplines_given;

  /* Is logging enabled? */
  CmdArgs::logging = options->log_flag;

  /* What is the log path? */
  CmdArgs::log_path = options->log_path_arg;

  /* internal mapper options->*/
  
  /* Enable the use of the local mapper */
  CmdArgs::mapper_use_internal = options->mapper_use_internal_flag;

  /* The debugging map element subchannel for the internal mapper */
  CmdArgs::mapper_debug_subgroup = options->mapper_debug_subgroup_arg;

  /* If internal mapper is used, disables obstacle fusion */
  CmdArgs::mapper_disable_obs_fusion= options->mapper_disable_obs_fusion_flag;

  /* Disable line fusion in the mapper*/
  CmdArgs::mapper_disable_line_fusion = options->mapper_disable_line_fusion_flag;

  /* Use old line fusion behavior*/
  CmdArgs::mapper_line_fusion_compat = options->mapper_line_fusion_compat_flag;

  /* Set time thresh to remove stale obstacles in the map*/
  CmdArgs::mapper_decay_thresh = options->mapper_decay_thresh_arg;

  CmdArgs::mapper_enable_groundstrike_filtering = options->mapper_enable_groundstrike_filtering_flag;

  /* Specify which zone planner should currently be used*/
  CmdArgs::use_dplanner = options->use_dplanner_given;
  CmdArgs::use_s1planner = options->use_s1planner_given;
  CmdArgs::use_circle_planner = options->use_circle_planner_given;

  /* Specify what to use for the uturn/backup maneuvers*/
  CmdArgs::use_hacked_uturn = options->use_hacked_uturn_given;
  CmdArgs::use_hacked_backup = options->use_hacked_backup_given;
  
  if (CmdArgs::use_hacked_backup)
    cout << "using hacked backup" << endl;
  if (CmdArgs::use_hacked_uturn)
    cout << "using hacked uturn" << endl;
  sleep(1);

  /* Display the costmap in mapviewer */
  CmdArgs::show_costmap = options->show_costmap_given;

  CmdArgs::no_failure_handling = options->no_failure_handling_given;

  /* Deal with config files here*/
  if (options->path_plan_config_given) {
    // something is specified, so use that
    CmdArgs::path_plan_config = options->path_plan_config_arg;
  } else {
    // for the default, look in the appropriate places ...
    char* filename = dgcFindConfigFile(options->path_plan_config_arg, "planner");
    CmdArgs::path_plan_config = filename;
  }
  if (options->planner_config_given) {
    // something is specified, so use that
    CmdArgs::planner_config = options->planner_config_arg;
  } else {
    // for the default, look in the appropriate places ...
    char* filename = dgcFindConfigFile(options->planner_config_arg, "planner");
    CmdArgs::planner_config = filename;
  }

  /*If no zone planner is specified, then use s1planner (for now)*/ 
  if ( !(CmdArgs::use_dplanner) && !(CmdArgs::use_s1planner) && !(CmdArgs::use_circle_planner) ) {
    CmdArgs::use_s1planner = true;
  }

  CmdArgs::use_rndf_frame = options->use_rndf_frame_given;
 
  /* Generate a unique filename to log to */
  if(CmdArgs::logging) {
    string tmpRNDFname;
  
    ostringstream oss; 
    struct stat st;
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t)); 
    tmpRNDFname.assign(CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("/")+1, CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("."));
    oss << options->log_path_arg << "planner-" << tmpRNDFname << "." << timestr << ".log";
    logFileName = oss.str();
    string suffix = "";

    /* if it exists already, append .1, .2, .3 ... */
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
    CmdArgs::log_filename = logFileName;
  }

  /* To be removed (are we using this?) */
  CmdArgs::lane_cost = true;

  // set the level of visualization info sent to mapviewer
  CmdArgs::visualization_level = options->visualization_level_arg;

  // Enable new fancy modular logic planner?
  CmdArgs::mod_log_planner = options->mod_log_planner_flag;

  // Enable new linear temproal logic planner?
  CmdArgs::lt_planner = options->lt_planner_flag;
  
  // set a segment to be sparse
  CmdArgs::sparse = options->sparse_arg;

  return 0;
}
