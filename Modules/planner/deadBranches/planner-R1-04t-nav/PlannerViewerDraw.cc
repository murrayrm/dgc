
/* 
 * Desc: Planner viewer drawing functions
 * Date: 29 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>

#include "PlannerViewer.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "%s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

/* REMOVE

// Draw a set of axes
void PlannerViewer::drawAxes(float size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Draw a text box
void PlannerViewer::drawText(float size, const char *text)
{
  int i, count;
  float sx, sy;

  // Compute the scale factors in x/y directions, based on the 'M'
  // character.
  sx = glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, 'M');
  sy = sx * 1.3;
    
  glPushMatrix();
  glScalef(size / sx, size / sy, 1);
  glTranslatef(0, -sy, 0);

  glPushMatrix();
    
  for (i = 0, count = 0; i < (int) strlen(text); i++, count++)
  {
    if (text[i] == '\n')
    {
      glPopMatrix();
      glTranslatef(0, -sy, 0);
      glPushMatrix();
      count = 0;      
    }
    else
    {
      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[i]);
    }
  }

  glPopMatrix();
  glPopMatrix();

  return;
}


// Draw Alice (vehicle frame)
void PlannerViewer::drawAlice(float steerAngle)
{
  glLineWidth(1.0);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(steerAngle * 180/M_PI, 0, 0, 1);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(steerAngle * 180/M_PI, 0, 0, 1);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
    
  return;
}


// Predraw a grid
void PlannerViewer::predrawGrid(float px, float py, float spacing)
{
  int i, j, n;
  float qx, qy;
  char text[256];

  n = (int) (100 / spacing);

  // Pick an offset that will line us up with the grid spacing
  qx = -fmodf(px + 1e6, spacing);
  qy = -fmodf(py + 1e6, spacing);

  // Dont re-draw the grid unless we move
  if (this->gridList != 0 && qx == this->gridPx && qy == this->gridPy)
    return;
  this->gridPx = qx;
  this->gridPy = qy;
      
  // Create display list
  if (this->gridList == 0)
    this->gridList = glGenLists(1);
  glNewList(this->gridList, GL_COMPILE);

  glLineWidth(1.0);
  glColor3f(0.5, 0.5, 0.5);

  glPushMatrix();
  glTranslatef(qx, qy, 0);
  
  // Draw horizontal lines
  for (j = -n; j <= +n; j++)
  {
    glBegin(GL_LINE_STRIP);
    for (i = -n; i < +n; i++)
      glVertex2f(i * spacing, j * spacing);
    glEnd();
  }  
  
  // Draw vertical lines
  for (j = -n; j <= +n; j++)
  {
    glBegin(GL_LINE_STRIP);
    for (i = -n; i < +n; i++)
      glVertex2f(j * spacing, i * spacing);    
    glEnd();
  }  

  // Draw vertex coordinates
  for (j = -n; j < +n; j++)
  {
    for (i = -n; i < +n; i++)
    {
      snprintf(text, sizeof(text), "%+.0f %+.0f", px + qx + i * spacing, py + qy + j * spacing);
      glPushMatrix();
      glTranslatef(-j * spacing, i * spacing, 0);      
      glRotatef(180, 0, 1, 0);
      this->drawText(0.5, text);
      glPopMatrix();
    }
  }
  
  glPopMatrix();

  glEndList();
  
	return;
}
*/


// Pre-draw the graph to a display list
void PlannerViewer::predrawGraph(PlanGraph *graph, VehicleState *state, int props)
{
  float px, py, size;
  uint16_t include, exclude;

  px = state->siteNorthing;
  py = state->siteEasting;
  size = 128; // MAGIC

  // Select the type of nodes to display
  include = PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_TURN;
  exclude = PLAN_GRAPH_NODE_ONCOMING;

  if (graphProps & (1 << CMD_GRAPH_VEHICLE))
    include |= PLAN_GRAPH_NODE_VEHICLE;
  
  if (graphProps & (1 << CMD_GRAPH_LANE_CHANGES))
  {
    include |= PLAN_GRAPH_NODE_LANE_CHANGE;
    exclude &= ~PLAN_GRAPH_NODE_ONCOMING;
  }

  // Draw the graph structure
  this->graphList = this->planner->m_graph.predrawStruct(px, py, size, include, exclude);

  // Draw the graph status
  this->statusList = this->planner->m_graph.predrawStatus(px, py, size, include, exclude);

  return;
}


// Pre-draw the vector map
void PlannerViewer::predrawMap(Map *map, int props)
{
  int i, j;
  MapElement mapel;
  point2_uncertain *point;

  // Create display list
  if (this->mapList == 0)
    this->mapList = glGenLists(1);
  glNewList(this->mapList, GL_COMPILE);
  
  for (i = 0; i < (int) map->usedIndices.size(); i++)
  {
    map->getFusedEl(mapel, i);

    if (!mapel.isObstacle())
      continue;
    if (mapel.geometry.size() == 0)
      continue;
    
    if (true)
    {
      // Draw the bounding box
      glPushMatrix();
      glTranslated(mapel.center.x, mapel.center.y, 0);
      glRotated(mapel.orientation * 180/M_PI, 0, 0, 1);
      glColor3f(0.5, 0.5, 0.5);
      glBegin(GL_LINE_LOOP);
      glVertex2d(-mapel.length/2, -mapel.width/2);
      glVertex2d(+mapel.length/2, -mapel.width/2);
      glVertex2d(+mapel.length/2, +mapel.width/2);
      glVertex2d(-mapel.length/2, +mapel.width/2);
      glEnd();
      glPopMatrix();
    }

    // Draw all points on the object
    if (true)
    {
      glPointSize(2);
      glColor3f(1, 0, 0);
      glBegin(GL_POINTS);
      for (j = 0; j < (int) mapel.geometry.size(); j++)
      {
        point = &mapel.geometry[j];
        glVertex2d(point->x, point->y);
      }
      glEnd();
    }

    // Draw polygon
    if (mapel.geometryType == GEOMETRY_POLY)
    {
      glColor3f(1, 0, 0);
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glBegin(GL_POLYGON);
      for (j = 0; j < (int) mapel.geometry.size(); j++)
      {
        point = &mapel.geometry[j];
        glVertex3d(point->x, point->y, 0);
      }
      glEnd();
    }
  }
  
  glEndList();
  
  return;
}


// Pre-draw the fused map to a display list
void PlannerViewer::predrawFused(FusedPerceptor *fused, int props)
{
  bool invert;
  float mx, my;
  dgc_image_t *image;

  invert = false;

  // Select the map image
  if (props & (1 << CMD_FUSED_LINE))
  {
    image = fused->lineImage;
    invert = true;
  }
  else if (props & (1 << CMD_FUSED_LINEDIST))
  {
    image = fused->lineDistImage;
    invert = true;
  }
  else if (props & (1 << CMD_FUSED_ROUGH))
  {
    image = fused->roughLevels[0].binImage;
    invert = true;
  }
  else if (props & (1 << CMD_FUSED_ROUGHDIST))
  {
    image = fused->roughLevels[0].distImage;
    invert = true;
  }
  else
    return;

  // Create texture
  if (this->fusedTex == 0)
    glGenTextures(1, &this->fusedTex);

  // Show as positive or negative image
  if (invert)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
  else
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  
  // Copy image into texture
  glBindTexture(GL_TEXTURE_2D, this->fusedTex);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  
  // Mono 8-bit images
  if (image->channels == 1 && image->bits == 8)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4, image->cols, image->rows,
                      GL_LUMINANCE, GL_UNSIGNED_BYTE, image->data);
  // RGB 8-bit images
  else if (image->channels == 3 && image->bits == 8)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4, image->cols, image->rows,
                      GL_RGB, GL_UNSIGNED_BYTE, image->data);

  // Mono 32-bit floating point images
  else if (image->channels == 1 && image->bits == 32)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4, image->cols, image->rows,
                      GL_LUMINANCE, GL_FLOAT, image->data);
  
  // Create display list
  if (this->fusedList == 0)
    this->fusedList = glGenLists(1);
  glNewList(this->fusedList, GL_COMPILE);

  // Transform into map frame (assume we are currently in the local frame).
  glPushMatrix();
  glTranslatef(fused->localMap->pose.pos.x, fused->localMap->pose.pos.y, 0.10);
  
  glBindTexture(GL_TEXTURE_2D, this->fusedTex);

  // Draw the image as a textured quad
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glColor3f(1, 1, 1);
  glEnable(GL_TEXTURE_2D);
  glBegin(GL_QUADS);  
  fused->localMap->cvtIndexToPos(0, 0, &mx, &my); 
  glTexCoord2f(0, 0);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(fused->localMap->size, 0, &mx, &my); 
  glTexCoord2f(1, 0);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(fused->localMap->size, fused->localMap->size, &mx, &my);
  glTexCoord2f(1, 1);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(0, fused->localMap->size, &mx, &my); 
  glTexCoord2f(0, 1);
  glVertex2f(mx, my);
  glEnd();
  glDisable(GL_TEXTURE_2D);
  
  // Draw the bounding rectangle
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glColor3f(0, 0, 1);
  glBegin(GL_QUADS);  
  fused->localMap->cvtIndexToPos(0, 0, &mx, &my); 
  glTexCoord2f(0, 0);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(fused->localMap->size, 0, &mx, &my); 
  glTexCoord2f(1, 0);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(fused->localMap->size, fused->localMap->size, &mx, &my);
  glTexCoord2f(1, 1);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(0, fused->localMap->size, &mx, &my); 
  glTexCoord2f(0, 1);
  glVertex2f(mx, my);
  glEnd();
  
  glPopMatrix();
  glEndList();
    
  return;
}


// Draw the center-lane distances
void PlannerViewer::predrawLane(PlanGraph *graph, float px, float py, float size)
{
  int i;
  PlanGraphNode *node;
  PlanGraphNodeList nodes;
  uint16_t include, exclude;
  float s;
  
  // Create display list
  if (this->laneList == 0)
    this->laneList = glGenLists(1);
  glNewList(this->laneList, GL_COMPILE);

  // Get some nodes
  include = PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_RAIL_CHANGE | PLAN_GRAPH_NODE_VEHICLE;
  exclude = PLAN_GRAPH_NODE_ONCOMING | PLAN_GRAPH_NODE_LANE_CHANGE;
  graph->getRegion(&nodes, px, py, size, size, include, exclude);

  glPointSize(3.0);
  glBegin(GL_POINTS);
  for (i = 0; i < (int) nodes.size(); i++)
  {
    node = nodes[i];

    if (!graph->isStatusFresh(node->status.centerTime))
        continue;

    s = node->status.lineDist / 3.0;
    if (s > 1.0)
      s = 1.0;    
    glColor3f(s, 1 - s, 0);
    
    glVertex2f(node->pose.pos.x, node->pose.pos.y);
  }
  glEnd();
  
  glEndList();
  
  return;
}


// Draw the path (site frame)
void PlannerViewer::drawPath(PlanGraphPath *path)
{
  int i;
  PlanGraphNode *nodeA, *nodeB;

  glLineWidth(2);
  
  glBegin(GL_LINES);
  for (i = 0; i < path->pathLen - 1; i++)
  {
    nodeA = path->nodes[i];
    nodeB = path->nodes[i + 1];

    if (path->collideObs)
      glColor3f(1, 1, 0);
    else if (path->collideCar)
      glColor3f(1, 0.5, 0);
    else if (path->directions[i] == PLAN_GRAPH_PATH_REV)
      glColor3f(1, 0, 0);
    else
      glColor3f(0, 1, 1);
    
    glVertex3f(nodeA->pose.pos.x, nodeA->pose.pos.y, -0.5);
    glVertex3f(nodeB->pose.pos.x, nodeB->pose.pos.y, -0.5);
  }
  glEnd();

  return;
}


// Draw the trajectory (local frame)
void PlannerViewer::drawTraj(CTraj *traj)
{
  int i;
  vec3_t p, q;

  // Draw velocity vectors.  This assumes the trajectory is in the
  // local frame, and we are drawing into the local frame.
  glPointSize(2.0);
  glColor3f(0, 1, 0);  
  for (i = 0; i < traj->getNumPoints(); i++)
  {
    p.x = traj->getNdiffarray(0)[i];
    p.y = traj->getEdiffarray(0)[i];    

    q.x = p.x + traj->getNdiffarray(1)[i] * 0.5;
    q.y = p.y + traj->getEdiffarray(1)[i] * 0.5;    

    //printf("%d %f %f\n", i, p.x, p.y);

    glBegin(GL_POINTS);
    glVertex2d(p.x, p.y);
    glEnd();    

    glBegin(GL_LINES);
    glVertex2d(p.x, p.y);
    glVertex2d(q.x, q.y);
    glEnd();
  }
  glPointSize(1.0);
  
  return;
}
