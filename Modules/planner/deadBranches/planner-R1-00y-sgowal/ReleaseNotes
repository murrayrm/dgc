              Release Notes for "planner" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "planner" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "planner" module can be found in
the ChangeLog file.

Release R1-00y-sgowal (Thu Aug 16 20:10:51 2007):
	Removed usage of getWaypoint from Map (because not working with zones)
	Zones are still not working in planner yet

Release R1-00y (Wed Aug 15 18:01:40 2007):
	Correctly added the --enable-line-fusion flag (using CmdArgs now)
	Made selection of turning signal much faster (not using getLane from Map anymore)

Release R1-00x (Tue Aug 14 23:09:28 2007):
	Added a cmdline option to enable line fusion in Map
	(--enable-line-fusion, default off). Remember that
	--update-from-map is needed too for the planner to use the fused
	data.

Release R1-00w (Tue Aug 14 18:17:56 2007):
	Switched over to the cspecs interface to the zone planner. Changes were mostly in the ZoneCorridor.* files.

Release R1-00v (Tue Aug 14 13:16:24 2007):
	Forgot to add these files for the previous release...

Release R1-00u (Tue Aug 14 12:10:39 2007):
	Added ZoneCorridor.* and UT for zone region generation.
	Added multi-path-planner calls for different path planning problems.
	The Zone handling is not yet stable and should not be used until further notice. This does not break nominal planning.

Release R1-00t (Tue Aug 14  9:55:25 2007):
	Added new console that allows manual override of Intersection Handling, turn on/off prediction, override the current FSM State.

	The keys that are assigned to the new console are marked with a () in the console. Everything is lower case. One special case in the 
	intersection handling. Pressing "i" does not give the GO-command directly. It will just switch into the next state within the Intersection 
	Handling (from WAIT_LEGAL --> WAIT_POSSIBLE --> WAIT_CLEAR --> GO).

Release R1-00s (Mon Aug 13 19:59:40 2007):
	Added communication with Follower.  Changed as per new gcinterface 
specification for different types of pauses. 

Release R1-00r (Wed Aug  8 15:27:48 2007):
	Handling hacked backup/uturn maneuver

Release R1-00q (Wed Aug  8 17:30:17 2007):
	When Prediction used Particle Filters, it slowed down the planning loop drastically. Now planner decides which method prediction will use.

Release R1-00p (Wed Aug  8 13:22:32 2007):
	Now populates segment goals to LogicPlanner

Release R1-00o (Sat Aug  4  3:46:56 2007):
	Now populates velParams that is sent to VelPlanner

Release R1-00n (Fri Aug  3 20:45:29 2007):
	added --update-from-map arg

Release R1-00m (Fri Aug  3  8:46:06 2007):
	Integrated the interface to Prediction

Release R1-00l (Fri Aug  3  0:51:24 2007):
	Added argument --step-by-step-loading: causes the planner to load the RNDF segments (as graph nodes) only when needed
	Added argument --load-graph-files: causes the planner to load the graph nodes from pre-computed files rather than re-generating them
	Stopping the car while loading from the RNDF (with --step-by-step-loading on)
	Fixed mplanner recovery handling (if planner receives directive 1, it assumes that mplanner had to restart)

Release R1-00k (Tue Jul 31 19:43:08 2007):
	Calling updateGraphMap

Release R1-00j (Tue Jul 31 13:47:45 2007):
	The module prediction is now part of the planner and is linked as a library. It can be turned off by using the command 
	line argument --noprediction. The planning behaviour is not affected by prediction yet.

Release R1-00i (Fri Jul 27  9:28:53 2007):
	Enforcing continuity between plans
	UTurn implemented as a hack
	Added option for planner to close the loop by computing path from current position and only using feed-forward in gcfollower
	Controlling turn signal now

Release R1-00h (Tue Jul 24 15:36:13 2007):
	The console display is now thread (ctrl+S will not affect the planner). PlannerMain.cc is responsible for updating the 
console.
	Planner now takes a --log-level argument to set the gcmodule log level
	Modified CmdArgs to include the log filename

Release R1-00g (Tue Jul 24  8:38:21 2007):
	Added some doxygen documentation.

Release R1-00f (Mon Jul 23 19:42:42 2007):
	Added comments
	Modified console display (does not flicker anymore)

Release R1-00e (Thu Jul 19 18:45:39 2007):
	Modified the console display

Release R1-00d (Wed Jul 18 16:37:06 2007):
	Updated logic-planner. Now Intersection output is shown in the Console.

Release R1-00c (Wed Jul 18 12:34:57 2007):
	Made AliceStateHelper available to other planner libraries
	Correctly setting the errors received from the planner libraries

Release R1-00b (Mon Jul 16 13:54:42 2007):
	Now using Graph as Graph_t

Release R1-00a (Fri Jul 13 16:05:59 2007):
	Initial release of the planner module. At the moment this module 
interfaces with the mplanner at the top, and sends trajectories. The 
planner accepts the map, and an rndf.
	Four libraries where created that interfaces with the planner. 
These libraries where created in separate modules to allow for 
concurrent development. These are: path-planner, vel-planner, 
logic-planner, and graph-updater.
	There is also one temporary module (library) that contains 
selected files from GraphPlanner and a file that define the interfaces 
between the planner and it's libraries. This module will later be 
removed.
	Currently, the planner generates a maintains a graph and map. 
Inside the planner, the logic-planner is called. Next, the graph-updater 
is called to make the relavent changes (due to logic planner). The path 
planner then searches the graph to construct a path. This path is then 
passed to the velocity planner. The velocity planner returns a 
trajectory that can be evaluated by the planner and sent to follower.
	The current functionality includes: generate a graph from the 
rndf (using graph-planner fcn), update the graph with obstacles (basic), 
plan a path (using graph-planner fcns), plan velocity (using 
graph-planner function). Thus, there is no new functionality other than 
the planner itself and all the interfaces. The logic part has not been 
inserted. This only serves as a place to start.


Release R1-00 (Fri Jul  6 16:49:31 2007):
	Created.




























