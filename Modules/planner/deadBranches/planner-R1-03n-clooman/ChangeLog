Fri Sep 28 16:20:22 2007	Christian Looman (clooman)

	* version R1-03n-clooman
	BUGS:  
	FILES: Planner.cc(41186), PlannerUtils.cc(41186),
		PlannerUtils.hh(41186), TrafficStateEst.cc(41186)
	Commit after merge

	FILES: Planner.cc(41178), PlannerUtils.cc(41178),
		PlannerUtils.hh(41178), TrafficStateEst.cc(41178)
	Changed structures so that monitorAliceSpeed gets the estop
	information

Fri Sep 28 14:31:55 2007	vcarson (vcarson)

	* version R1-03n
	BUGS:  
	FILES: Planner.cc(41076)
	Made dplanner changes.

Fri Sep 28  7:00:00 2007	Sam Pfister (sam)

	* version R1-03m
	BUGS:  
	FILES: TrafficStateEst.cc(40963), TrafficStateEst.hh(40963)
	Updated TrafficStateEst::getLocalMapUpdate() thread function to run
	efficiently with mapper instance inside planner.  Added debugging
	output for internal mapper which can be visualized with mapviewer
	on subgroup -3.  For mapper external, implemented faster map data
	copy in the case when the new QUEUE_MAPELEMENTS option is not used
	which only copies portions of the map which have been updated and
	so it scales much better with large RNDFs.  QUEUE_MAPELEMENTS is
	still on by default in the case that mapper is run external to
	planner, though the speed benefits are not as significant now. 
	There is the possibility that the queue might introduce more
	latency when handling high volumes of sensor data so this should be
	looked at in the field.  Also the faster map copy function
	Map::copyMapData(Map *targetmap) might be useful to speed up the
	map copy operations in prediction. 

Thu Sep 27 21:17:08 2007	Noel duToit (ndutoit)

	* version R1-03l
	BUGS:  
	FILES: Planner.cc(40919)
	field changes: fixed bug in the updateGraphMap function.

Thu Sep 27 11:47:41 2007	Sven Gowal (sgowal)

	* version R1-03k
	BUGS:  
	FILES: Planner.cc(40846), TrafficStateEst.cc(40846),
		TrafficStateEst.hh(40846)
	Merged

	FILES: Planner.cc(40831), TrafficStateEst.cc(40831),
		TrafficStateEst.hh(40831)
	Improved performance of map snapshot (copying the map for
	prediction still takes 50 ms in victorville)

Thu Sep 27  0:30:41 2007	vcarson (vcarson)

	* version R1-03j
	BUGS:  
	FILES: Planner.cc(40695), ZoneCorridor.cc(40695),
		ZoneCorridor.hh(40695)
	Added functionality to create generic corridors and paint them
	appropriately for planning with zone planners in road and
	intersection regions.  

Wed Sep 26 22:25:25 2007	Christian Looman (clooman)

	* version R1-03i
	BUGS:  
	FILES: Planner.cc(40675)
	Commit after merge

	FILES: Planner.cc(40642)
	Changed interface to logic-planner so that it returns the
	replanning command from the fault handler

Wed Sep 26 20:49:23 2007	Noel duToit (ndutoit)

	* version R1-03h
	BUGS:  
	FILES: Planner.cc(40607), PlannerMain.cc(40607), planner.ggo(40607)
	Added cmd line args for --update-lanes and --update-stoplines. 

	FILES: Planner.cc(40611)
	small fix

Wed Sep 26 11:24:07 2007	Sven Gowal (sgowal)

	* version R1-03g
	BUGS:  
	FILES: Planner.cc(40500)
	Merged

	FILES: Planner.cc(40492)
	Correctly populating the segments to load (when
	--step-by-step-loading is on).

Wed Sep 26  9:23:54 2007	Joel Burdick (jwb)

	* version R1-03f
	BUGS:  
	FILES: PlannerMain.cc(40421)
	fixed bug for switching into circle planner

Tue Sep 25 13:31:42 2007	Sven Gowal (sgowal)

	* version R1-03e
	BUGS:  
	FILES: Planner.cc(40337)
	Fixed bug preventing Alice from entering zones

Tue Sep 25 11:32:46 2007	Sven Gowal (sgowal)

	* version R1-03d
	BUGS:  
	FILES: Planner.cc(40296)
	Merged

	FILES: Planner.cc(40275)
	Fixed bug preventing Alice from completing the mission after an
	intersection

Tue Sep 25 11:09:57 2007	Noel duToit (ndutoit)

	* version R1-03c
	BUGS:  
	FILES: Makefile.yam(40231), Planner.cc(40231), Planner.hh(40231),
		UT_zoneCorridor.cc(40231)
	Changed replanning in s1planner to always replan, but to take only
	0.2s

	FILES: Planner.cc(40246), PlannerMain.cc(40246),
		ZoneCorridor.cc(40246), planner.ggo(40246)
	Send the cost map to the mapviewer upon request.

	FILES: ZoneCorridor.cc(40232)
	Implemented setting different safety boxes around Alice based on
	our situation.

Mon Sep 24 18:36:11 2007	Christian Looman (clooman)

	* version R1-03b
	BUGS:  
	FILES: Planner.cc(40184), Planner.hh(40184)
	Adapted the interface to send information to planner when Alice
	wants to change lanes

Sun Sep 23 15:18:25 2007	Noel duToit (ndutoit)

	* version R1-03a
	BUGS:  
	FILES: PlannerMain.cc(40059), planner.ggo(40059)
	Added cmd line args for specifying the zone planner. --use-dplanner
	for dynamic planner, --use-s1planner for using s1planner, and
	--use-circle-planner for using circle-planner. If nothing is
	specified, then use s1planner.

Fri Sep 21 21:29:08 2007	vcarson (vcarson)

	* version R1-03
	BUGS:  
	FILES: ZoneCorridor.cc(39979), ZoneCorridor.hh(39979)
	Added creating a perimeter around Alice and the exit point of the
	current seg goal for zone-like planning.  

Fri Sep 21 20:36:00 2007	Noel duToit (ndutoit)

	* version R1-02z
	BUGS:  
	FILES: Makefile.yam(39972), Planner.cc(39972), Planner.hh(39972),
		PlannerUtils.cc(39972), PlannerUtils.hh(39972),
		UT_zoneCorridor.cc(39972), ZoneCorridor.cc(39972),
		ZoneCorridor.hh(39972)
	merged with latest release

	FILES: Makefile.yam(39965), UT_zoneCorridor.cc(39965)
	minor fixes

	FILES: Planner.cc(39964), Planner.hh(39964),
		PlannerUtils.cc(39964), PlannerUtils.hh(39964),
		UT_zoneCorridor.cc(39964), ZoneCorridor.cc(39964),
		ZoneCorridor.hh(39964)
	Implemented the clean version of zone planning, including all the
	code restructuring.

Fri Sep 21 18:26:52 2007	Christian Looman (clooman)

	* version R1-02y
	BUGS:  
	FILES: Planner.cc(39957), Planner.hh(39957)
	Commit after merge

	FILES: Planner.cc(39939), Planner.hh(39939)
	Changed structure to run prediction in a thread

Fri Sep 21 16:50:55 2007	Noel duToit (ndutoit)

	* version R1-02x
	BUGS:  
	FILES: Planner.cc(39887), Planner.hh(39887),
		PlannerUtils.cc(39887), PlannerUtils.hh(39887),
		ZoneCorridor.cc(39887), ZoneCorridor.hh(39887)
	merged with the latest release

	FILES: Planner.cc(39904)
	removed some debugging info

	FILES: Planner.cc(39887), Planner.hh(39887),
		PlannerUtils.cc(39887), PlannerUtils.hh(39887),
		ZoneCorridor.cc(39887), ZoneCorridor.hh(39887)
	merged with the latest release

	FILES: Planner.cc(39603), Planner.hh(39603)
	Initial restructuring of planner to fascilitate fault handling.

	FILES: Planner.cc(39860), Planner.hh(39860),
		PlannerUtils.cc(39860), PlannerUtils.hh(39860),
		ZoneCorridor.cc(39860), ZoneCorridor.hh(39860)
	Restructured planner to enable easier extension for fault handling.
	Specifically, created a separate function for planning the traj. In
	this function we can call the appropriate planner and set up the
	planning problem in a convenient fashion. Also started implementing
	the planning problem specification (new) which is in a format that
	should be easily extended to set up the different planning
	problems. This release does not extend functionality.

Fri Sep 21 12:21:48 2007	vcarson (vcarson)

	* version R1-02w
	BUGS:  
	FILES: Planner.cc(39822), ZoneCorridor.cc(39822),
		ZoneCorridor.hh(39822)
	Setting direction properly in case we do not have a subpath.

Fri Sep 21  5:09:36 2007	Sam Pfister (sam)

	* version R1-02v
	BUGS:  
	FILES: PlannerMain.cc(39702), TrafficStateEst.cc(39702),
		TrafficStateEst.hh(39702), planner.ggo(39702)
	Added basic support methods for the RNDF frame implementation. 
	Needs fused-perceptor RNDF frame update before full testing can be
	done.  The option to use the RNDF frame is off by default and can
	be enabled with the flag --use-rndf-frame.  Also improved structure
	of function calls for running mapper internal to planner.  This
	works well in simulation, but needs testing in the field to see if
	it fixed any issues

Thu Sep 20 13:43:16 2007	vcarson (vcarson)

	* version R1-02u
	BUGS:  
	FILES: Planner.cc(39606), ZoneCorridor.cc(39606),
		ZoneCorridor.hh(39606)
	Setting path direction in Cspecs.

Thu Sep 20 11:55:17 2007	vcarson (vcarson)

	* version R1-02t
	BUGS:  
	FILES: ZoneCorridor.cc(39587)
	Populating the obstacles for dplanner. 

Thu Sep 20 10:52:59 2007	vcarson (vcarson)

	* version R1-02s
	BUGS:  
	FILES: Planner.cc(39577), Planner.hh(39577),
		ZoneCorridor.cc(39577), ZoneCorridor.hh(39577)
	Providing s1planner seed for dplanner.	Recoded replanning and
	error handling in the zone with s1planner and dplanner. 

Tue Sep 18 19:07:53 2007	Christian Looman (clooman)

	* version R1-02r
	BUGS:  
	FILES: Planner.cc(39381)
	Reduced the size of obstacle sent by prediction

Mon Sep 17  8:51:39 2007	Sam Pfister (sam)

	* version R1-02q
	BUGS:  
	FILES: TrafficStateEst.cc(39105)
	Updated mapper calls in planner to allow for non-static local to
	global frame transform

Sat Sep 15 16:19:42 2007	vcarson (vcarson)

	* version R1-02p
	BUGS:  
	FILES: Planner.cc(38962)
	Put timing info in log file for dplanner createOtgTrajectory call. 
	Added replanning for dplanner if we are stopped for more than 20
	seconds. 

Sat Sep 15 13:04:10 2007	Kenny Oslund (kennyo)

	* version R1-02o
	BUGS:  
	FILES: Planner.cc(38948), ZoneCorridor.cc(38948)
	added setting a safety margin around alice  set obstacles to be
	grown equally in all directions

Sat Sep 15  3:51:31 2007	Laura Lindzey (lindzey)

	* version R1-02n
	BUGS:  
	FILES: ZoneCorridor.cc(38900)
	using proper map element accessor functions rather than
	ELEMENT_OBSTACLE and ELEMENT_VEHICLE

	FILES: ZoneCorridor.cc(38905)
	commenting out return statement that caused corridor painting to
	return w/o doing anything if incoming obstacle was of type vehicle

Fri Sep 14  6:45:05 2007	Christian Looman (clooman)

	* version R1-02m
	BUGS:  
	FILES: Planner.cc(38806)
	Commit after merge

	FILES: Planner.cc(38799)
	Adjusted prediction to new map/mapper structure

Fri Sep 14  2:24:43 2007	Kenny Oslund (kennyo)

	* version R1-02l
	BUGS:  
	FILES: Planner.cc(38709), Planner.hh(38709)
	Changed zone code wrt replanning.  Also, clearing the clothoid tree
	on a replan.  

	FILES: Planner.cc(38732)
	minor change to the function that resets the clothoid tree

Thu Sep 13 18:01:21 2007	Christian Looman (clooman)

	* version R1-02k
	BUGS:  
	FILES: Makefile.yam(38670), Planner.cc(38670),
		TrafficStateEst.cc(38670)
	TrafficStateEst sends number of obstacles received to Console.
	Planner uses isObstacle/isVehicle functions now. Also, obstacles
	sent by prediction have different type now.

Mon Sep 10 22:22:22 2007	vcarson (vcarson)

	* version R1-02j
	BUGS:  
	FILES: Planner.cc(38231), ZoneCorridor.cc(38231),
		ZoneCorridor.hh(38231)
	Added the functionality to convert the MapElement to its point2arr
	description and populated cspecs with this information for planning
	in zones. (s1planner requirement).  

Mon Sep 10 19:09:25 2007	vcarson (vcarson)

	* version R1-02i
	BUGS:  
	FILES: Planner.cc(38210), Planner.hh(38210), ZoneCorridor.hh(38210)
	Populating cspecs with data related to whether we are entering,
	exiting parking spot  or traversing a zone.  

Mon Sep 10 15:46:59 2007	vcarson (vcarson)

	* version R1-02h
	BUGS:  
	FILES: Planner.cc(38131)
	Added whether we are entering a parking spot or not, and populated
	cspecs for zone planners. 

Mon Sep 10 12:36:28 2007	vcarson (vcarson)

	* version R1-02g
	BUGS:  
	FILES: Planner.cc(38057), ZoneCorridor.cc(38057),
		ZoneCorridor.hh(38057)
	Changed to new map function to get obstacles in a zone. Added back
	sending exit point to attention module, it had been overwritten in
	someone's previous release.  

	FILES: Planner.cc(38059)
	Logging more data wrt obstacles in zone.

Fri Sep  7 19:17:53 2007	vcarson (vcarson)

	* version R1-02f
	BUGS:  
	FILES: Planner.cc(37888), UT_zoneCorridor.cc(37888),
		ZoneCorridor.cc(37888), ZoneCorridor.hh(37888)
	Painting obstacles for zone regions.

	FILES: ZoneCorridor.cc(37906)
	getObsinBounds is not working by dividing up the zone perimeter so
	reverted to getObsNearby

Thu Sep  6 16:55:32 2007	vcarson (vcarson)

	* version R1-02e
	BUGS:  
	FILES: Planner.cc(37738), PlannerMain.cc(37738),
		ZoneCorridor.cc(37738)
	Re-incorporated the dplanner changes that had been overwritten
	erroneously in a previous release...

Wed Sep  5 13:35:12 2007	vcarson (vcarson)

	* version R1-02d
	BUGS:  
	FILES: Planner.cc(37648), Planner.hh(37648)
	Added exit point information to send to Attentino module. 

	FILES: Planner.cc(37627), Planner.hh(37627)
	Added exit waypoint information for intersections to the attention
	module message. 

Wed Sep  5 11:56:10 2007	Sven Gowal (sgowal)

	* version R1-02c
	BUGS:  
	FILES: Planner.cc(37591), PlannerMain.cc(37591)
	Fixed possible segfault

Tue Sep  4 23:12:19 2007	vcarson (vcarson)

	* version R1-02b
	BUGS:  
	FILES: Makefile.yam(37556), Planner.cc(37556),
		PlannerMain.cc(37556), planner.ggo(37556)
	Added support for using dplanner in zones. 

	FILES: Makefile.yam(37542), Planner.cc(37542),
		PlannerMain.cc(37542), planner.ggo(37542)
	Added use-dplanner option.  By specifying use-dplanner option,
	planner uses dplanner for zone regions else uses s1planner. 
	Linking to dplanner libraries. 

Tue Sep  4  4:00:57 2007	datamino (datamino)

	* version R1-02a
	BUGS:  
	FILES: PlannerMain.cc(37476), TrafficStateEst.cc(37476),
		planner.ggo(37476)
	yam screwed the previous release, trying again

	FILES: PlannerMain.cc(37448), TrafficStateEst.cc(37448),
		planner.ggo(37448)
	Added new cmdline flag to enable old line fusion behavior of fusing
	elements while adding them to the map when the internal mapper
	(with the external mapper, the old behavior is the only possible
	one). Default when using the internal mapper is to run the line
	fusion within the mapper main loop at 10Hz.

	FILES: PlannerMain.cc(37339), TrafficStateEst.cc(37339),
		planner.ggo(37339)
	Added option to run line fusion the old way
	(--mapper-line-fusion-compat), in Map::addEl() instead of
	Mapper::mainLoop().

Tue Sep  4  1:20:12 2007	vcarson (vcarson)

	* version R1-02
	BUGS:  
	FILES: Planner.cc(37404), ZoneCorridor.cc(37404)
	Tweaked the bitmap parameters. Added smaller threshold for goal
	completion in a zone. 

	FILES: Planner.cc(37433)
	Changed zone threshold for goal completion to 0.5 m.

Mon Sep  3 18:57:35 2007	vcarson (vcarson)

	* version R1-01z
	BUGS:  
	FILES: Planner.cc(37295), UT_zoneCorridor.cc(37295),
		ZoneCorridor.cc(37295), ZoneCorridor.hh(37295)
	changes 

	FILES: Planner.cc(37326), ZoneCorridor.cc(37326)
	Implemented parking spot painting.  

Sun Sep  2 17:51:11 2007	Sven Gowal (sgowal)

	* version R1-01y
	BUGS:  
	FILES: Planner.cc(37215)
	merged

	FILES: Planner.cc(37096)
	Added timing information

Sun Sep  2 17:27:01 2007	vcarson (vcarson)

	* version R1-01x
	BUGS:  
	FILES: Planner.cc(37186), Planner.hh(37186),
		ZoneCorridor.cc(37186), ZoneCorridor.hh(37186)
	Fixed delayed in planner execution in zone regions. Added cost map
	painting for zone perimeters. 

	FILES: Planner.cc(37176), Planner.hh(37176),
		ZoneCorridor.cc(37176), ZoneCorridor.hh(37176)
	Fixed planner execution increase in zones. Painted cost map for
	perimeter

Sun Sep  2 11:25:41 2007	Christian Looman (clooman)

	* version R1-01w
	BUGS:  
	FILES: Planner.cc(37079)
	more log information at prediction

Sat Sep  1 17:26:05 2007	Sam Pfister (sam)

	* version R1-01v
	BUGS:  
	FILES: PlannerMain.cc(37018), TrafficStateEst.cc(37018)
	fixed bug where --mapper-enable-groundstrike-filtering caused
	mapper initialization to hang.	Should be able to run mapper
	internal to planner with all current options now. 

Thu Aug 30 18:49:00 2007	Noel duToit (ndutoit)

	* version R1-01u
	BUGS:  
	FILES: Planner.cc(36721), PlannerUtils.cc(36721),
		ZoneCorridor.cc(36721)
	merges with the latest release of planner.

	FILES: Planner.cc(36500), PlannerUtils.cc(36500)
	Fixed a few bugs in the extractSubPath function.

	FILES: Planner.cc(36631), ZoneCorridor.cc(36631)
	Finally got Alice to drive into and out of a zone

	FILES: Planner.cc(36667)
	Took out some print outs and added some debugging info.

Thu Aug 30 17:26:49 2007	Christian Looman (clooman)

	* version R1-01t
	BUGS:  
	FILES: Planner.cc(36632)
	Minor adjustments for calling prediction

Thu Aug 30 14:22:53 2007	Sven Gowal (sgowal)

	* version R1-01s
	BUGS:  
	FILES: Planner.cc(36539), Planner.hh(36539)
	Planning ahead

Wed Aug 29 20:42:43 2007	Noel duToit (ndutoit)

	* version R1-01r
	BUGS:  
	FILES: Makefile.yam(36434), Planner.cc(36434), Planner.hh(36434),
		PlannerUtils.cc(36434), UT_zoneCorridor.cc(36434)
	merge with latest release of planner.

	FILES: Makefile.yam(36215), Planner.cc(36215),
		UT_zoneCorridor.cc(36215)
	Continued debugging of the zones operations

Wed Aug 29 19:05:10 2007	vcarson (vcarson)

	* version R1-01q
	BUGS:  
	FILES: Planner.cc(36370)
	Fixed isCurrentPathComplete to call reverseProjection with path
	extension.

Wed Aug 29 16:15:55 2007	Kenny Oslund (kennyo)

	* version R1-01p
	BUGS:  
	FILES: ZoneCorridor.cc(36311)
	Modified ZoneCorridor.cc to work with updated access in cspecs

	FILES: ZoneCorridor.cc(36318)
	changed ZoneCorridor.cc to use a different version of the over
	loaded corridor setting function

Wed Aug 29 11:21:40 2007	vcarson (vcarson)

	* version R1-01o
	BUGS:  
	FILES: Planner.cc(36198), Planner.hh(36198)
	Implemented isCurrentPathComplete to replace isGoalComplete.  We
	now check goal completions wrt exit points that are projected onto
	the path.

Wed Aug 29  8:41:16 2007	Noel duToit (ndutoit)

	* version R1-01n
	BUGS:  
	FILES: Makefile.yam(36149), Planner.cc(36149), Planner.hh(36149)
	partially integrated functionality for zones and Uturns. Also for
	the sectioning of the path into subPaths according to the direction

	FILES: Planner.cc(36125), Planner.hh(36125)
	modified planner so the now we only plan through the zone once.
	Will have to update this to replan if the cost of the path changes.

	FILES: Planner.cc(36128)
	Fiddled with zones some more, but it is not yet functional.

	FILES: PlannerUtils.cc(36121), PlannerUtils.hh(36121),
		UT_zoneCorridor.cc(36121), ZoneCorridor.cc(36121)
	commiting so that Francisco can grab my changes.

	FILES: PlannerUtils.hh(36102)
	merged in Francisco's changes to the latest branch

Tue Aug 28 13:16:06 2007	Christian Looman (clooman)

	* version R1-01m
	BUGS:  
	FILES: Planner.cc(35914)
	Commit after merge

	FILES: Planner.cc(35901)
	Adapted to new getDistToStopLine function

Mon Aug 27 16:51:07 2007	Laura Lindzey (lindzey)

	* version R1-01l
	BUGS:  
	FILES: PlannerMain.cc(35734), TrafficStateEst.cc(35734),
		planner.ggo(35734)
	adding command line option to planner that enables groundstrike
	filtering on internal map

	FILES: PlannerMain.cc(35735)
	finishing cmdline changes

Mon Aug 27  9:08:33 2007	vcarson (vcarson)

	* version R1-01k
	BUGS:  
	FILES: Planner.cc(35425), Planner.hh(35425)
	Changed call to get distance to stop line.  Setting
	INTERSECTION_STRAIGHT for attention module.  

Thu Aug 23 17:10:18 2007	Sven Gowal (sgowal)

	* version R1-01j
	BUGS:  
	FILES: Planner.cc(35315)
	merged

	FILES: Planner.cc(35223)
	Made hacked UTURN and BACKUP more robust

Thu Aug 23 16:43:21 2007	vcarson (vcarson)

	* version R1-01i
	BUGS:  
	FILES: Planner.cc(35271), Planner.hh(35271), PlannerUtils.cc(35271)
	Finished attention/planner state sending. 

Thu Aug 23 14:00:14 2007	Noel duToit (ndutoit)

	* version R1-01h
	BUGS:  
	FILES: Makefile.yam(35213)
	Added new libraries that the mapper depends on. For this planner to
	work, you also have to check out the emap module

Tue Aug 21 19:24:28 2007	Christian Looman (clooman)

	* version R1-01g
	BUGS:  
	FILES: Planner.cc(34786)
	commit after merge

	FILES: Planner.cc(34765)
	adapted interface to new prediction module

Tue Aug 21 18:36:00 2007	Sven Gowal (sgowal)

	* version R1-01f
	BUGS:  
	FILES: Planner.cc(34758)
	Merged

	FILES: Planner.cc(34751)
	Fixed segfault + not re-generating the vehicle subgraph at
	intersection anymore

Tue Aug 21 16:48:18 2007	Noel duToit (ndutoit)

	* version R1-01e
	BUGS:  
	FILES: Planner.cc(34691), ZoneCorridor.cc(34691),
		ZoneCorridor.hh(34691)
	removed some comments that were printed to the console when in
	zones.

Tue Aug 21  6:23:03 2007	Sam Pfister (sam)

	* version R1-01d
	BUGS:  
	FILES: Makefile.yam(34548), Planner.cc(34548),
		PlannerMain.cc(34548), TrafficStateEst.cc(34548),
		TrafficStateEst.hh(34548), planner.ggo(34548)
	Enabled the option to have mapper run inside a thread in planner. 
	This is off by default but can be turned on by setting the
	--mapper-use-internal flag.  Also the mapper options can be passed
	through from the command line and have a mapper prefix. 

Mon Aug 20 23:56:39 2007	Christian Looman (clooman)

	* version R1-01c
	BUGS:  
	FILES: Planner.cc(34521), Planner.hh(34521)
	Changes after merge. Fixed segfault in isGoalComplete when graph is
	not initialized yet.

	FILES: Planner.cc(34451), Planner.hh(34451)
	Adjusted interfaces to LogicPlanner/IntersectionHandling so that
	IntersectionHandling receives the graph from planner

Mon Aug 20 19:53:22 2007	Sven Gowal (sgowal)

	* version R1-01b
	BUGS:  
	FILES: Planner.cc(34413)
	merged

	FILES: Planner.cc(34406)
	Fixed hacked UTurn

Mon Aug 20 18:30:38 2007	vcarson (vcarson)

	* version R1-01a
	BUGS:  
	FILES: Planner.cc(34339), Planner.hh(34339)
	Added southface for sending planner states to the attention module.
	 Changed isGoalComplete method to account for zone regions.  

Fri Aug 17 18:56:10 2007	vcarson (vcarson)

	* version R1-01
	BUGS:  
	FILES: Makefile.yam(34104), Planner.cc(34104), Planner.hh(34104),
		UT_zoneCorridor.cc(34104), ZoneCorridor.cc(34104)
	Changed files to support zone regions. 

	FILES: Makefile.yam(34123)
	Took out ocpspecs dependencies in planner. 

Thu Aug 16 20:10:46 2007	Sven Gowal (sgowal)

	* version R1-00z
	BUGS:  
	FILES: Planner.cc(33992), ZoneCorridor.cc(33992)
	Removed use of getWaypoint from Map (because not working for zones)

Wed Aug 15 18:01:31 2007	Sven Gowal (sgowal)

	* version R1-00y
	BUGS:  
	FILES: Planner.cc(33778), Planner.hh(33778), PlannerMain.cc(33778),
		TrafficStateEst.cc(33778), TrafficStateEst.hh(33778)
	Correctly added enable-line-fusion + made selection of the turning
	signal much faster

Tue Aug 14 23:09:19 2007	datamino (datamino)

	* version R1-00x
	BUGS:  
	FILES: Planner.cc(33621), Planner.hh(33621), PlannerMain.cc(33621),
		TrafficStateEst.cc(33621), TrafficStateEst.hh(33621),
		planner.ggo(33621)
	Added a cmdline option to enable line fusion in Map
	(--enable-line-fusion, default off). Remember that
	--update-from-map is needed too for the planner to use the fused
	data.

Tue Aug 14 18:17:49 2007	Noel duToit (ndutoit)

	* version R1-00w
	BUGS:  
	FILES: Makefile.yam(33509), Planner.cc(33509),
		ZoneCorridor.cc(33509), ZoneCorridor.hh(33509)
	Switched to the zone planner interface defined in cspecs.

Tue Aug 14 13:16:22 2007	Noel duToit (ndutoit)

	* version R1-00v
	BUGS:  
	New files: UT_zoneCorridor.cc ZoneCorridor.cc ZoneCorridor.hh
Tue Aug 14 12:10:28 2007	Noel duToit (ndutoit)

	* version R1-00u
	BUGS:  
	FILES: Makefile.yam(33358), Planner.cc(33358), Planner.hh(33358)
	Added ZoneCorridor.* for the corridor generation in zone regions.
	Implemented multi path-planner problem in planner. This is not yet
	stable, but does not break normal driving.

Tue Aug 14  9:55:19 2007	Christian Looman (clooman)

	* version R1-00t
	BUGS:  
	FILES: Makefile.yam(33306), Planner.cc(33306),
		PlannerMain.cc(33306)
	commit after merges

Mon Aug 13 19:59:36 2007	vcarson (vcarson)

	* version R1-00s
	BUGS:  
	FILES: Planner.cc(33202), Planner.hh(33202)
	Communication with follower responses.	Implemented new seggaols
	interface change. 

Thu Aug  9 13:36:45 2007	Sven Gowal (sgowal)

	* version R1-00r
	BUGS:  
	FILES: Planner.cc(32823), Planner.hh(32823)
	Merged

Wed Aug  8 17:30:14 2007	Christian Looman (clooman)

	* version R1-00q
	BUGS:  
	FILES: Planner.cc(32649)
	minor bug that slowed down planning loop when prediction uses
	particle filters

Wed Aug  8 13:22:28 2007	Christian Looman (clooman)

	* version R1-00p
	BUGS:  
	FILES: Planner.cc(32602)
	Fills logicParams with segment goals 

Sat Aug  4  3:46:53 2007	Magnus Linderoth (mlinderoth)

	* version R1-00o
	BUGS:  
	FILES: Planner.cc(31905)
	Now sets speed in velParams

	FILES: Planner.cc(31938)
	Added writing to velParams to a second place

Fri Aug  3 20:45:23 2007	Sven Gowal (sgowal)

	* version R1-00n
	BUGS:  
	FILES: Planner.cc(31748), PlannerMain.cc(31748), planner.ggo(31748)
	added update_from_map flag

	FILES: Planner.cc(31883)
	added --update-from-map arg

Fri Aug  3  9:05:17 2007	Christian Looman (clooman)

	* version R1-00m
	BUGS:  
	FILES: Makefile.yam(31730), Planner.cc(31730), Planner.hh(31730)
	Merged the two branches

	FILES: Makefile.yam(31723), Planner.cc(31723), Planner.hh(31723)
	Integrated the interface to Prediction

Fri Aug  3  0:51:17 2007	Sven Gowal (sgowal)

	* version R1-00l
	BUGS:  
	FILES: Planner.cc(31445), Planner.hh(31445), PlannerMain.cc(31445),
		planner.ggo(31445)
	Added argument --step-by-step-loading

	FILES: Planner.cc(31447)
	Stopping the car while loading from the RNDF

	FILES: Planner.cc(31660), PlannerMain.cc(31660), planner.ggo(31660)
	added arg + fixed mplanner error handling

Tue Jul 31 20:19:26 2007	Sven Gowal (sgowal)

	* version R1-00k
	BUGS:  
	FILES: Planner.cc(31286), TrafficStateEst.cc(31286),
		TrafficStateEst.hh(31286)
	Robust UTURN

	FILES: Planner.cc(30954)
	Calling updateGraphMap

	FILES: TrafficStateEst.cc(30851), TrafficStateEst.hh(30851)
	Changed rights

Tue Jul 31 13:47:30 2007	Christian Looman (clooman)

	* version R1-00j
	BUGS:  
	FILES: Makefile.yam(31092), Planner.cc(31092), Planner.hh(31092),
		PlannerMain.cc(31092), planner.ggo(31092)
	Integrated prediction into the planning loop. The prediction module
	is linked as a library. Prediction can be turned of by using the
	command line argument --noprediction

Fri Jul 27  9:28:39 2007	Sven Gowal (sgowal)

	* version R1-00i
	BUGS:  
	Deleted files: AliceStateHelper.cc AliceStateHelper.hh CmdArgs.cc
		CmdArgs.hh Console.cc Console.hh Log.cc Log.hh
	FILES: Makefile.yam(30390), PlannerMain.cc(30390),
		PlannerUtils.cc(30390), TrafficStateEst.cc(30390),
		UT_planner.cc(30390)
	trickling through changes in temp-planner-interfaces.

	FILES: Planner.cc(30292), Planner.hh(30292)
	started enforcing continuity between plans. Not done yet.

	FILES: Planner.cc(30308), UT_planner.cc(30308)
	Fixed small bug

	FILES: Planner.cc(30385), Planner.hh(30385)
	Fixed compilation

	FILES: Planner.cc(30478)
	Implemented some continuity enforcement in planner. Also, there is
	some fault handling here too. Might have to be moved to
	logic-planner?

	FILES: Planner.cc(30502), Planner.hh(30502)
	merged

	FILES: Planner.cc(30507, 30520, 30522)
	tuning UTURN

	FILES: Planner.cc(30534), PlannerMain.cc(30534), planner.ggo(30534)
	added option for planner to close the loop by computing path from
	current position and only using feed-forward in gcfollower.

	FILES: Planner.cc(30536), Planner.hh(30536)
	Successful UTURN with follower (follower doesn't continue after the
	UTURN though)

	FILES: Planner.cc(30556), Planner.hh(30556),
		PlannerUtils.cc(30556), PlannerUtils.hh(30556)
	Activating turn signals now

	FILES: Planner.hh(30375), PlannerUtils.hh(30375)
	Moved the common files in planner over from planner itself to the
	interfaces library.

Tue Jul 24 15:36:00 2007	Sven Gowal (sgowal)

	* version R1-00h
	BUGS:  
	FILES: CmdArgs.cc(30153), CmdArgs.hh(30153), Planner.cc(30153),
		Planner.hh(30153), PlannerMain.cc(30153),
		planner.ggo(30153)
	Added log-level for gcmodule logger

	FILES: Console.cc(30181), Console.hh(30181), Planner.cc(30181),
		PlannerMain.cc(30181)
	Made console threaded. PlannerMain is now responsible for
	refreshing the console

Tue Jul 24  8:38:02 2007	Noel duToit (ndutoit)

	* version R1-00g
	BUGS:  
	FILES: AliceStateHelper.cc(30116), AliceStateHelper.hh(30116),
		CmdArgs.cc(30116), CmdArgs.hh(30116), Console.cc(30116),
		Console.hh(30116), Log.cc(30116), Log.hh(30116),
		Planner.cc(30116), Planner.hh(30116),
		PlannerMain.cc(30116), PlannerUtils.cc(30116),
		PlannerUtils.hh(30116), TrafficStateEst.cc(30116),
		TrafficStateEst.hh(30116)
	Added some doxygen comments.

Mon Jul 23 19:42:27 2007	Sven Gowal (sgowal)

	* version R1-00f
	BUGS:  
	FILES: AliceStateHelper.cc(30029), Console.cc(30029),
		Planner.cc(30029), PlannerMain.cc(30029),
		PlannerUtils.cc(30029)
	Added comments

	FILES: Console.cc(30024), Planner.cc(30024)
	Removed flicker in the Console display

	FILES: Console.cc(30041)
	Updated console

	FILES: Planner.cc(30046), Planner.hh(30046), PlannerMain.cc(30046),
		planner.ggo(30046)
	Updated the commandline options for the planner.

Thu Jul 19 18:45:31 2007	Sven Gowal (sgowal)

	* version R1-00e
	BUGS:  
	FILES: Console.cc(29549), Console.hh(29549), Planner.cc(29549)
	Setting the flag to pass to path-planner

	FILES: Console.cc(29603), Console.hh(29603)
	changed console for new intersection return values

	FILES: Console.cc(29611)
	Layout changes

	FILES: Console.cc(29686), Console.hh(29686), Planner.cc(29686),
		Planner.hh(29686)
	Updated Console to display last and current waypoints

	FILES: Planner.cc(29626)
	Added console output

Wed Jul 18 16:37:02 2007	Christian Looman (clooman)

	* version R1-00d
	BUGS:  
	FILES: Console.cc(29476), Console.hh(29476)
	bugfixes

	FILES: Console.cc(29479), Console.hh(29479)
	Updated Console for showing intersection status

Wed Jul 18 12:34:42 2007	Sven Gowal (sgowal)

	* version R1-00c
	BUGS:  
	FILES: CmdArgs.cc(29398), CmdArgs.hh(29398), Console.cc(29398),
		Console.hh(29398), Planner.cc(29398),
		PlannerMain.cc(29398), PlannerUtils.cc(29398),
		PlannerUtils.hh(29398)
	Modified the gcmodule log path

	FILES: Makefile.yam(29400)
	Changed Makefile to allow libraries to use AliceStateHelper

	FILES: Planner.cc(29416)
	Modified error checking

Mon Jul 16 13:54:37 2007	Francisco zabala (zabala)

	* version R1-00b
	BUGS:  
	New files: Planner.cc Planner.hh PlannerMain.cc PlannerUtils.cc
		PlannerUtils.hh planner.ggo
	Deleted files: TrafficManager.cc TrafficManager.hh
		TrafficPlannerMainCSS.cc TrafficUtils.cc TrafficUtils.hh
		tplanner.ggo
	FILES: Makefile.yam(29118)
	Updated file names and executable names.

	FILES: Makefile.yam(29144)
	Updated Makefile

	FILES: UT_planner.cc(29143)
	Changed Graph_t to be Graph (instead of GraphPlanner)

Fri Jul 13 16:05:56 2007	Noel duToit (ndutoit)

	* version R1-00a
	BUGS:  
	New files: AliceStateHelper.cc AliceStateHelper.hh CmdArgs.cc
		CmdArgs.hh Console.cc Console.hh Log.cc Log.hh
		TrafficManager.cc TrafficManager.hh
		TrafficPlannerMainCSS.cc TrafficStateEst.cc
		TrafficStateEst.hh TrafficUtils.cc TrafficUtils.hh
		UT_planner.cc tplanner.ggo
	FILES: Makefile.yam(28605)
	Copied files over directly from both tplanner and graphplanner.
	First step is going to be to interface graph planner with tplanner,
	and then to clean up the code by placing pieces into libraries and
	using function calls.

	FILES: Makefile.yam(28692)
	Removed all references to the tplanner/mapping library.

	FILES: Makefile.yam(28715)
	Implemented UT_planner to test the graph, path and trajectory
	generation. Updated the GraphUpdater, PathPlanner and VelPlanner
	accordingly. Removed redundant graph-planner files in graph
	directory.

	FILES: Makefile.yam(28723)
	Updated the makefile to copy the planner interfaces file into the
	include directory.

	FILES: Makefile.yam(28733)
	removed planner interfaces dependence on temporary files by using
	fwd declarations

	FILES: Makefile.yam(28758)
	Extracted libraries for the planner module. Only logic-planner and
	planner code exist here now. Removed all the unnecessary files.
	Added UT_planner where we have successfully initialized a graph,
	planned a path, and planned the velocity. This executable outputs a
	file that can be read into matlab for verification.

	FILES: Makefile.yam(28806)
	Cleaned up some of the tplanner files (that still needs to be moved
	to the logic-planner library). Integrated the new planner library
	functions with TrafficManager, which will become the template for
	the final planner. The new planner now gives a planning problem to
	path-planner, which constructs the path. The velocity planner then
	creates the trajectory. This trajectory is sent directly to
	follower.

	FILES: Makefile.yam(28905)
	Removed tplanner FSM as well as corridor generation code. Cleaned
	up the files.

	FILES: Makefile.yam(29004)
	Added calls to the logic planner as well as the graph-updater

	FILES: Makefile.yam(29027)
	Added the logic-planner library to the UT_planner.

Fri Jul  6 16:49:31 2007	Noel duToit (ndutoit)

	* version R1-00
	Created planner module.








































































<<<<<<< .working



=======

>>>>>>> .merge-right.r37177








































































