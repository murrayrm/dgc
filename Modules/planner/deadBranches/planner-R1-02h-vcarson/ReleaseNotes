              Release Notes for "planner" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "planner" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "planner" module can be found in
the ChangeLog file.

Release R1-02h-vcarson (Mon Sep 10 19:09:30 2007):
	Populating cspecs with information relating to whether we are entering a parking spot, exiting a 
parking spot, or traversing a zone without parking (s1planner requirement).  

Release R1-02h (Mon Sep 10 15:47:02 2007):
	Added a state for zone planners and functionality to determine if we are entering a parking 
spot or not.  
Release R1-02g (Mon Sep 10 12:36:33 2007):
	Changed to getObsInZone call from map, however this does not work reliably.
	Re-incorporated attention module exit point information that had been overwritten in a 
previous release and fixed a bug for INTERSECTION_STRAIGHT. 
	
Release R1-02f (Fri Sep  7 19:17:59 2007):
	Added painting cost for obstacles in zone regions.  
Release R1-02e (Thu Sep  6 16:55:37 2007):
	Re-incorporated dplanner changes that were overwritten 
erroneously in the previous release.  Reset the final velocity condition 
for zones  to 0.2, the minimum requirement for dplanner.  
Release R1-02d (Wed Sep  5 13:25:04 2007):
	Added sending exit waypoint information to attention module for intersections.  

Release R1-02c (Wed Sep  5 11:56:15 2007):
	Fix possible segfault (next_goal not initialized in getFinalPose)

Release R1-02b (Tue Sep  4 20:27:31 2007):
	Added cmdline flag -use-dplanner and corresponding supoprt to use dplanner in the case of zones.  

Release R1-02a (Tue Sep  4  4:01:04 2007):
	Added new cmdline flag (--mapper-line-fusion-compat) to enable
        old line fusion behavior of fusing elements while adding them to
        the map when the internal mapper (with the external mapper, the
        old behavior is the only possible one). Default when using the
        internal mapper is to run the line fusion within the mapper main
        loop at 10Hz.

Release R1-02 (Tue Sep  4  1:20:15 2007):
	Tweaked some bitmap parameters.  
	Added smaller threshold for goal completions in zone, which will enable us to get into a parking spot properly before exiting the zone. 

Release R1-01z (Mon Sep  3 18:57:41 2007):
	Implemented cost painting for parking spots. 

Release R1-01y (Sun Sep  2 11:55:14 2007):
	Added timing information
	Replanning in zones when stopped for more than 20 seconds
	Fixed UTurn final pose
	Fixed the infinite loop issue

Release R1-01x (Sun Sep  2 17:10:35 2007):
	Fixed bug that was causing execution of planner to increase in zones.  Added cost map 
painting for zone perimeters.  

Release R1-01w (Sun Sep  2 11:25:44 2007):
	Output more information about prediction and re-compiled against latest temp-planner-interfaces and
	logic-planner

Release R1-01v (Sat Sep  1 17:26:09 2007):
	fixed bug where --mapper-enable-groundstrike-filtering caused
	mapper initialization to hang.	Should be able to run mapper
	internal to planner with all current options now. 

Release R1-01u (Thu Aug 30 17:56:44 2007):
	WE NOW HAVE ZONE OPERATION! (at least the initial working implementation)
	Fixed the extractSubPath function to correctly handle zones stages.
	Changed the isCurrectPathDone (isComplete) function to account for Alice' heading.

Release R1-01t (Thu Aug 30 17:26:53 2007):
	Minor adjustments for using prediction.

Release R1-01s (Thu Aug 30 14:22:59 2007):
	Planner is now planning ahead on the SegGoals sent by mplanner

Release R1-01r (Wed Aug 29 11:48:48 2007):
	Continued debugging of the zone region. Added some debugging information to assist with this process.
	Implemented feature where the zone path gets replanned if our new final position is different.

Release R1-01q (Wed Aug 29 19:05:13 2007):
	Changed isCurrentPathComplete to call a reverseProjection method with a path extension argument.  
Release R1-01p (Wed Aug 29 16:16:01 2007):
	changed data structures used by ZoneCorridor.cc to match modified cspecs structures

Release R1-01o (Wed Aug 29 11:21:44 2007):
	Implemented isCurrentPathComplete to replace isGoalComplete.  Checks goal completion to 0.5 m wrt to an exit point projection (if necessary) on 
the path.  For now Alice will stop at the end of goal completion.  Continuity will be handled in a later release.   
	
Release R1-01n (Wed Aug 29  8:41:25 2007):
	Initial implementation of more advanced UTurn handling, and Zone handling. Neither of these work
	fully, but at the same time it does not break existing functionality, so I wanted to get the 
	release out.
	TODO: track down bugs in extraction of subPath. Path from s1planner seems garbled. Add feature to 
	only replan in zone/uturn if environment changes (right now only plan once).

Release R1-01m (Tue Aug 28 12:35:09 2007):
	Adapted to new distToStopLine function 

Release R1-01l (Mon Aug 27 16:51:13 2007):
	adding command line option to planner that enables groundstrike
	filtering on internal map

Release R1-01k (Mon Aug 27  9:08:37 2007):
	Changed call to getDistFromStopLine to one in Utils that considers the graph for the calculation (more robust).  
Release R1-01j (Thu Aug 23 14:06:35 2007):
	Made hacked UTURN and BACKUP more robust

Release R1-01i (Thu Aug 23 16:43:26 2007):
	Fixed bug that would set the state to PASS_RIGHT in the middle of the intersection.  Sending NOMINAL planner state 
to the attention module.       

Release R1-01h (Thu Aug 23 14:00:18 2007):
	Changed the makefile to include the libraries now needed by the 
mapper. NOTE: you have to get the emap module for the planner to 
compile.

Release R1-01g (Tue Aug 21 19:17:16 2007):
	Adapted interface to Prediction and re-compiled against latest logic-planner

Release R1-01f (Tue Aug 21 18:23:08 2007):
	Not re-generating the vehicle sub-graph at intersections anymore

Release R1-01e (Tue Aug 21 16:48:32 2007):
	Removed a few messages in zones to be able to see what clothoid planner is doing.

Release R1-01d (Tue Aug 21  6:23:12 2007):
	Enabled the option to have mapper run inside a thread in planner. 
	This is off by default but can be turned on by setting the
	--mapper-use-internal flag.  Also the mapper options can be passed
	through from the command line and have a mapper prefix. 

Release R1-01c (Mon Aug 20 21:47:58 2007):
	Updated interface to IntersectionHandling so that it receives the graph now.

	DON'T USE --LOAD-GRAPH-FILES in this release unless you like debugging...

Release R1-01b (Mon Aug 20 19:45:36 2007):
	Fixed hacked UTurn maneuver

Release R1-01a (Mon Aug 20 18:30:42 2007):
	Added southface to communicate planner state to the Attention module.  Changed goal completion check to 
account for zone regions. 

Release R1-01 (Fri Aug 17 18:56:17 2007):
	Added call to clothoid planner trajectory generation and velocity planner for planning in zone 
regions.  Took out ocpspecs dependency from Makefile.yam. 

Release R1-00z (Thu Aug 16 20:10:51 2007):
	Removed usage of getWaypoint from Map (because not working with zones)
	Zones are still not working in planner yet

Release R1-00y (Wed Aug 15 18:01:40 2007):
	Correctly added the --enable-line-fusion flag (using CmdArgs now)
	Made selection of turning signal much faster (not using getLane from Map anymore)

Release R1-00x (Tue Aug 14 23:09:28 2007):
	Added a cmdline option to enable line fusion in Map
	(--enable-line-fusion, default off). Remember that
	--update-from-map is needed too for the planner to use the fused
	data.

Release R1-00w (Tue Aug 14 18:17:56 2007):
	Switched over to the cspecs interface to the zone planner. Changes were mostly in the ZoneCorridor.* files.

Release R1-00v (Tue Aug 14 13:16:24 2007):
	Forgot to add these files for the previous release...

Release R1-00u (Tue Aug 14 12:10:39 2007):
	Added ZoneCorridor.* and UT for zone region generation.
	Added multi-path-planner calls for different path planning problems.
	The Zone handling is not yet stable and should not be used until further notice. This does not break nominal planning.

Release R1-00t (Tue Aug 14  9:55:25 2007):
	Added new console that allows manual override of Intersection Handling, turn on/off prediction, override the current FSM State.

	The keys that are assigned to the new console are marked with a () in the console. Everything is lower case. One special case in the 
	intersection handling. Pressing "i" does not give the GO-command directly. It will just switch into the next state within the Intersection 
	Handling (from WAIT_LEGAL --> WAIT_POSSIBLE --> WAIT_CLEAR --> GO).

Release R1-00s (Mon Aug 13 19:59:40 2007):
	Added communication with Follower.  Changed as per new gcinterface 
specification for different types of pauses. 

Release R1-00r (Wed Aug  8 15:27:48 2007):
	Handling hacked backup/uturn maneuver

Release R1-00q (Wed Aug  8 17:30:17 2007):
	When Prediction used Particle Filters, it slowed down the planning loop drastically. Now planner decides which method prediction will use.

Release R1-00p (Wed Aug  8 13:22:32 2007):
	Now populates segment goals to LogicPlanner

Release R1-00o (Sat Aug  4  3:46:56 2007):
	Now populates velParams that is sent to VelPlanner

Release R1-00n (Fri Aug  3 20:45:29 2007):
	added --update-from-map arg

Release R1-00m (Fri Aug  3  8:46:06 2007):
	Integrated the interface to Prediction

Release R1-00l (Fri Aug  3  0:51:24 2007):
	Added argument --step-by-step-loading: causes the planner to load the RNDF segments (as graph nodes) only when needed
	Added argument --load-graph-files: causes the planner to load the graph nodes from pre-computed files rather than re-generating them
	Stopping the car while loading from the RNDF (with --step-by-step-loading on)
	Fixed mplanner recovery handling (if planner receives directive 1, it assumes that mplanner had to restart)

Release R1-00k (Tue Jul 31 19:43:08 2007):
	Calling updateGraphMap

Release R1-00j (Tue Jul 31 13:47:45 2007):
	The module prediction is now part of the planner and is linked as a library. It can be turned off by using the command 
	line argument --noprediction. The planning behaviour is not affected by prediction yet.

Release R1-00i (Fri Jul 27  9:28:53 2007):
	Enforcing continuity between plans
	UTurn implemented as a hack
	Added option for planner to close the loop by computing path from current position and only using feed-forward in gcfollower
	Controlling turn signal now

Release R1-00h (Tue Jul 24 15:36:13 2007):
	The console display is now thread (ctrl+S will not affect the planner). PlannerMain.cc is responsible for updating the 
console.
	Planner now takes a --log-level argument to set the gcmodule log level
	Modified CmdArgs to include the log filename

Release R1-00g (Tue Jul 24  8:38:21 2007):
	Added some doxygen documentation.

Release R1-00f (Mon Jul 23 19:42:42 2007):
	Added comments
	Modified console display (does not flicker anymore)

Release R1-00e (Thu Jul 19 18:45:39 2007):
	Modified the console display

Release R1-00d (Wed Jul 18 16:37:06 2007):
	Updated logic-planner. Now Intersection output is shown in the Console.

Release R1-00c (Wed Jul 18 12:34:57 2007):
	Made AliceStateHelper available to other planner libraries
	Correctly setting the errors received from the planner libraries

Release R1-00b (Mon Jul 16 13:54:42 2007):
	Now using Graph as Graph_t

Release R1-00a (Fri Jul 13 16:05:59 2007):
	Initial release of the planner module. At the moment this module 
interfaces with the mplanner at the top, and sends trajectories. The 
planner accepts the map, and an rndf.
	Four libraries where created that interfaces with the planner. 
These libraries where created in separate modules to allow for 
concurrent development. These are: path-planner, vel-planner, 
logic-planner, and graph-updater.
	There is also one temporary module (library) that contains 
selected files from GraphPlanner and a file that define the interfaces 
between the planner and it's libraries. This module will later be 
removed.
	Currently, the planner generates a maintains a graph and map. 
Inside the planner, the logic-planner is called. Next, the graph-updater 
is called to make the relavent changes (due to logic planner). The path 
planner then searches the graph to construct a path. This path is then 
passed to the velocity planner. The velocity planner returns a 
trajectory that can be evaluated by the planner and sent to follower.
	The current functionality includes: generate a graph from the 
rndf (using graph-planner fcn), update the graph with obstacles (basic), 
plan a path (using graph-planner fcns), plan velocity (using 
graph-planner function). Thus, there is no new functionality other than 
the planner itself and all the interfaces. The logic part has not been 
inserted. This only serves as a place to start.


Release R1-00 (Fri Jul  6 16:49:31 2007):
	Created.











