#ifndef PLANNERUTILS_HH_
#define PLANNERUTILS_HH_

#include "frames/point2.hh"
#include "map/Map.hh"
#include "AliceStateHelper.hh"


class PlannerUtils {

public: 
  static int addAngles(double &angle_out, double angle_in1, double angle_in2);
  static double getAngleInRange(double angle);
  static bool isStopped(VehicleState &vehState);
private :

};

#endif /*PLANNERUTILS_HH_*/




