/**********************************************************
 **
 **  ZONECORRIDOR.CC
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 17:23:25 2007
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <skynettalker/SkynetTalker.hh>
#include "temp-planner-interfaces/Log.hh"
#include "alice/AliceConstants.h"
#include <math.h>

#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0:  0)


Err_t ZoneCorridor::generateCorridor(CSpecs_t& cSpecs, point2arr zonePerimeter, vector<point2arr> parkingSpots, vector<MapElement> obstacles, Path_params_t params, pose3_t finPose, VehicleState vehState)
{

  unsigned long long time1, time2;

  DGCgettime(time1);

  // set up cspecs problem
  // starting state
  double startingState[4];
  startingState[0] = AliceStateHelper::getPositionRearAxle(vehState).x;
  startingState[1] = AliceStateHelper::getPositionRearAxle(vehState).y;
  startingState[2] = AliceStateHelper::getVelocityMag(vehState);
  startingState[3] = AliceStateHelper::getHeading(vehState);

  Log::getStream(9) << "ZoneCorridor:CSpecs starting specs x=" << startingState[0]<< ", y=" << startingState[1]<<endl;

  cSpecs.setStartingState(startingState);
  //Log::getStream(9) << "ZoneCorridor:CSpecs-setStartingState execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  DGCgettime(time1);
  // starting controls
  double startingControls[2];
  startingControls[0] = AliceStateHelper::getAccelerationMag(vehState);
  startingControls[1] = 0;
  cSpecs.setStartingControls(startingControls);
  DGCgettime(time2);
  //Log::getStream(9) << "ZoneCorridor:CSpecs-setStartingControls execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  DGCgettime(time1);
  // final state
  double finalState[4];
  double roll, pitch, yaw;
  quat_to_rpy(finPose.rot, &roll, &pitch, &yaw);
  finalState[0] = finPose.pos.x - DIST_REAR_AXLE_TO_FRONT*cos(yaw);
  finalState[1] = finPose.pos.y - DIST_REAR_AXLE_TO_FRONT*sin(yaw);
  finalState[2] = 0.2; //dplanner requirement change to 0.1
  finalState[3] = yaw;
  cSpecs.setFinalState(finalState);

  Log::getStream(9) << "ZoneCorridor:CSpecs final specs x=" << finalState[0]<< ", y=" << finalState[1]<<endl;

  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:CSpecs-setFinalState execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  //set maximums 
  cSpecs.setMaxVelocity(6);
  cSpecs.setMaxAcc(VEHICLE_MAX_ACCEL);
  cSpecs.setMaxBraking(VEHICLE_MAX_DECEL);
  cSpecs.setMaxSteeringAngle(VEHICLE_MAX_AVG_STEER);
  cSpecs.setMaxSteeringRate(M_PI/2);

  DGCgettime(time1);
  // final controls
  double finalControls[2];
  finalControls[0] = 0;
  finalControls[1] = 0;
  cSpecs.setFinalControls(finalControls);
  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:CSpecs-setFinalControls execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // other parameters
  // TODO: add things like setting the max vel etc.
  //  cSpecs.icfc_prob.velMin = params.velMin;
  //  cSpecs.icfc_prob.velMax = params.velMax;
 
  DGCgettime(time1);
  // set the corridor
  cSpecs.setBoundingPolygon(zonePerimeter);
  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:CSpecs-setBoundingPolygon execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  DGCgettime(time1);
  
  // define the cost map
  BitmapParams bmParams;
  PolygonParams polygonParams;
  bmParams.resX = 0.1;
  bmParams.resY = 0.1;
  bmParams.width = 800;
  bmParams.height = 800;
  bmParams.baseVal = 1 ;
  bmParams.outOfBounds = 100;
  polygonParams.centerlaneVal = 1;
  polygonParams.obsCost = 500;

  point2 initPos(startingState[0], startingState[1]);
  getBitmapParams(bmParams, polygonParams, initPos, zonePerimeter, parkingSpots, obstacles);
  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:getBitmapParams execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  DGCgettime(time1);
  cSpecs.setCostMap(bmParams);
  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:getBitmapParams execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  return 0;
}

void ZoneCorridor::getBitmapParams(BitmapParams& bmparams, PolygonParams &polygonParams,
                     point2 alicePos, point2arr zonePerimeter, vector<point2arr> parkingSpots, vector<MapElement> obstacles)
{
  bmparams.centerX = alicePos.x;
  bmparams.centerY = alicePos.y;
  
  bmparams.polygons.clear();
  float perimCost = 50; /* The max value of the perimeter cost */
  float perimXCoeff = -0.5;
  float perimWidth =  VEHICLE_WIDTH*2;  /* The width of the cost painting around the perimeter line */

  float pSpaceCost = 0; /* The max value of the parking space cost */
  float pSpaceXCoeff = 0; /* A coefficient for the parking space cost function  */
  float pSpaceWidth =  4;  /* The width of the cost painting around the parking space line FIX */

  float obsCost = polygonParams.obsCost; /* The max value of the obstacle cost */
  float obsXCoeff = -0.5;

  float minValPerim = 1;
  float minValPark = 40;
  float minValObs = 1;

  CostSide side = LEFT_AND_RIGHT; /* Paint the cost to the left and right of the line */

  assignPerimeterCost(bmparams.polygons,zonePerimeter, perimXCoeff, perimCost, minValPerim, perimWidth, side);
  assignParkingSpaceCost(bmparams.polygons,parkingSpots, pSpaceXCoeff, pSpaceCost, minValPark, pSpaceWidth, side);

  Log::getStream(9) << "ZoneCorridor: Number of obstacles=" << obstacles.size()<<endl;

  for (unsigned int i= 0; i < obstacles.size(); i++) {    
    assignObstacleCost(bmparams.polygons,obstacles[i], obsXCoeff, obsCost, minValObs, pSpaceWidth,FILL_EXP2,COMB_MAX); //FIX 
  }
}


void ZoneCorridor::display(int sn_key, int sendSubgroup, point2arr zonePerimeter, CSpecs_t cSpecs)
{
  CMapElementTalker meTalker;
  meTalker.initSendMapElement(sn_key);
  int counter=12000;
  point2 point;
  MapId mapId;
  MapElement me;
  
  mapId = counter;

  // display the corridor
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_BLUE, 100);
  me.setGeometry(zonePerimeter);
  meTalker.sendMapElement(&me,sendSubgroup);
  
  // print the initial and final cond's
  vector<double> startingState = cSpecs.getStartingState();
  vector<point2> points;
  point.set(startingState[0], startingState[1]);
  points.push_back(point);
  point.set(startingState[0]+cos(startingState[3]), startingState[1]+sin(startingState[3]));
  points.push_back(point);
  mapId = 12001;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_YELLOW, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

  // print the initial and final cond's
  vector<double> finalState = cSpecs.getFinalState();
  point.set(finalState[0], finalState[1]);
  points.push_back(point);
  point.set(finalState[0]+cos(finalState[3]), finalState[1]+sin(finalState[3]));
  points.push_back(point);
  mapId = 12002;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_RED, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

}

void ZoneCorridor::print(point2arr zonePerimeter, CSpecs_t cSpecs)
{
  vector<double> startingState = cSpecs.getStartingState();
  vector<double> finalState = cSpecs.getFinalState();
  vector<double> startingControls = cSpecs.getStartingControls();
  vector<double> finalControls = cSpecs.getFinalControls();
  Console::addMessage("Init state = (%6.2f,%6.2f,%6.2f,%6.2f)", startingState[0], startingState[1], startingState[2], startingState[3]);
  Console::addMessage("Init control = (%6.2f,%6.2f)", startingControls[0], startingControls[1]);
  Console::addMessage("Fin state = (%6.2f,%6.2f,%6.2f,%6.2f)", finalState[0], finalState[1], finalState[2], finalState[3]);
  Console::addMessage("Fin control = (%6.2f,%6.2f)", finalControls[0], finalControls[1]);
}


void ZoneCorridor::assignPerimeterCost(vector<Polygon>& polygons, point2arr zonePerimeter, float xCoeff,
				       float cost, float minVal, float width, CostSide side) 
{  

  /* Add the first point again to the end of zonePerimeter array */
  zonePerimeter.push_back(zonePerimeter[0]);
  assignCostToLine(polygons, zonePerimeter, xCoeff, cost, minVal, width, side, FILL_EXP2,COMB_MAX);
}

void ZoneCorridor::assignParkingSpaceCost(vector<Polygon>& polygons,vector<point2arr> parkingSpots, float xCoeff, float cost, float minVal, float width, CostSide side) 
{
  for ( unsigned int i =0; i < parkingSpots.size(); i++) {
    assignCostToLine(polygons, parkingSpots[i], xCoeff, cost, minVal, width, side, FILL_LINEAR,COMB_MAX);
  }
}

void ZoneCorridor::assignObstacleCost(vector<Polygon>& polygons, MapElement& el,
				      float xCoeff, float obsCost, float bval, float width, 
				      bitmap::FillFunc fillFunc, bitmap::CombineFunc combFunc) 
{

  Log::getStream(9) << "ZoneCorridor: About to assign obstacle cost " << endl;

  double sigma = 1*VEHICLE_WIDTH/2;
  if (el.type != ELEMENT_OBSTACLE && el.type != ELEMENT_VEHICLE) {
    // this shouldn't happen, because it's checked in convertMapElementToPolygon()
    cerr << "CORRIDOR: FAILED ERROR: " << __FILE__ << ":" << __LINE__ << " cannot handle nonobstacle "
	 << el.type << " of map element" << endl;
    return; 
  }

  if (el.type == ELEMENT_VEHICLE) {
    Log::getStream(9) << "ZoneCorridor: el.type = ELEMENT_VEHICLE" << endl;
    return;
  }

  if (el.geometryType != GEOMETRY_POLY) {
    cerr << "CORRIDOR- FAILED ERROR: " << __FILE__ << ":" << __LINE__ << "cannot handle geometry type "
	 << el.geometryType << " of map element" << endl;
    return;
  }

  point2arr_uncertain vertices1(el.geometry);
  point2arr_uncertain vertices2(el.geometry);

  if (vertices1.size() == 0) {
    cerr << "ZoneCorridor: FAILED ERROR: " << __FILE__ << ":" << __LINE__ << " obstacle has no vertices." << endl;
    return;
  }

  Log::getStream(9) << "ZoneCorridor: el.type = ELEMENT_OBSTACLE. Will be painted in the map" << endl;
  Log::getStream(9) << "Obstacle at " << el.geometry << endl;
  Log::getStream(9) << "  height = " << endl;

  point2_uncertain obsCenter(0,0);
  for (unsigned i=0; i < vertices1.size(); i++)
    obsCenter = obsCenter + vertices1[i];
  
  obsCenter = obsCenter/vertices1.size();

  // Grow the obstacle
  for (unsigned i=0; i < vertices1.size(); i++) {
    double distFromCenter1 = obsCenter.dist(vertices1[i]) + 3*VEHICLE_WIDTH/4;
    double distFromCenter2 = obsCenter.dist(vertices1[i]) + 7*VEHICLE_WIDTH/4;
    double directionFromCenter = (vertices1[i] - obsCenter).heading();
    vertices1[i].set_point(obsCenter.x + distFromCenter1*cos(directionFromCenter),
			   obsCenter.y + distFromCenter1*sin(directionFromCenter));
    vertices2[i].set_point(obsCenter.x + distFromCenter2*cos(directionFromCenter),
			   obsCenter.y + distFromCenter2*sin(directionFromCenter));
  }

  Log::getStream(9) << "ZoneCorridor: Grew obstacle " << endl;

  vector<float> cost(vertices1.size(), obsCost);
  Polygon tmpPoly;
  tmpPoly.setVertices(vertices1, cost);
  tmpPoly.setCombFunc(combFunc);
  polygons.push_back(tmpPoly);

  // Gaussian function for continuity
  double a = -1/(2*pow(sigma,2));
  tmpPoly.setA(a);
  tmpPoly.setB(obsCost);
  tmpPoly.setC(0);
  tmpPoly.setFillFunc(fillFunc);

  for (unsigned i=0; i < vertices1.size(); i++) {
    point2arr_uncertain tmpVertices;
    cost.clear();

    cost.push_back(0);
    cost.push_back(0);
    double baseX = sqrt((1/a)*log(bval/obsCost));
    cost.push_back(baseX);
    cost.push_back(baseX);
    tmpVertices.push_back(vertices1[i]);
    if (i < vertices1.size() - 1) {
      tmpVertices.push_back(vertices1[i+1]);
      tmpVertices.push_back(vertices2[i+1]);
    }
    else {
      tmpVertices.push_back(vertices1[0]);
      tmpVertices.push_back(vertices2[0]);
    }
    tmpVertices.push_back(vertices2[i]);

    tmpPoly.setVertices(tmpVertices, cost);
    polygons.push_back(tmpPoly);
  }

}

void ZoneCorridor::assignCostToLine(vector<Polygon>& polygons, point2arr line,
				    float xCoeff, float cost, float minVal, float width, 
				    CostSide side,bitmap::FillFunc fillFunc, bitmap::CombineFunc combFunc)
{
  Polygon p;
  const cost_t sideVal = 2; // go down to 2*sigma (cost*ext(-0.5*2*2)=cost*0.1353)
 
  p.setFillFunc(fillFunc);
  p.setA(xCoeff);
  p.setB(cost);
  p.setC(minVal);
  p.setCombFunc(combFunc);

  // for ease of notation

  for (unsigned int i = 0; i < line.size()-1; i++)
  {
      // calculate bisectors for vertices v1 and v2. For a generic vertex k, the
      // bisector is the orthogonal to v(k+1) - v(k-1) through the vertex v(k)
      // except for the first and the last, which are treated specially
      point2 bisec[2];
      point2 distVec;

      if (i > 0) {
          distVec = line[i + 1] - line[i - 1];
      } else {
          distVec = line[i + 1] - line[i];
      }
      bisec[0] = point2(-distVec.y, distVec.x); // orthog to distVec
      bisec[0].normalize(); // FIXME: what if the points are so close that the distance is ~zero?

      if (i + 2 < line.size()) {
          distVec = line[i + 2] - line[i];
      } else {
          distVec = line[i + 1] - line[i];
      }
      bisec[1] = point2(-distVec.y, distVec.x); // orthog to distVec
      bisec[1].normalize(); // FIXME: what if the points are so close that the distance is ~zero?

      point2arr vert(4);
      vector<cost_t> val(4);
      // FIXME: there is the possibility that the top egde intersects the
      // bottom edge, for very sharp turns, creating something that is not a polygon
      // (a star? a butterfly? it's just that vertices are not in cw nor in ccw order)
      // The resulting cost map will be weird, but still acceptable, I think.

      // left side
      vert[0] = line[i + 1] + bisec[1]*width;
      val[0] = -sideVal;
      vert[1] = line[i]     + bisec[0]*width;
      val[1] = -sideVal;

      // right side
      vert[2] = line[i]     - bisec[0]*width;
      val[2] = sideVal;
      vert[3] = line[i + 1] - bisec[1]*width;
      val[3] = sideVal;

      p.setVertices(vert, val);
      polygons.push_back(p);
  }
}
