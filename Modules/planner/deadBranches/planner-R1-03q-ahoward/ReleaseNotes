              Release Notes for "planner" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "planner" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "planner" module can be found in
the ChangeLog file.

Release R1-03q-ahoward (Sat Sep 29 13:09:28 2007):
  Bogus release for merge.

Release R1-03q (Sat Sep 29 10:00:36 2007):
	Fix for reading path planner costs from file: linking the file to etc directory
	somehow got lost (perhaps in merging with the latest release?). Fixed now.

Release R1-03p (Fri Sep 28 16:08:07 2007):
	Path-planner parameters can be re-read from a file upon request (from console).

Release R1-03o (Fri Sep 28 16:14:09 2007):
	Changed interfaces so that monitorAliceSpeed gets the correct estop unit from the derived ActuatorState

Release R1-03n (Fri Sep 28 14:31:58 2007):
	Added back the dplanner functionality for zones. 
Release R1-03m (Fri Sep 28  7:00:04 2007):

	Updated TrafficStateEst::getLocalMapUpdate() thread function to run
	efficiently with mapper instance inside planner. 

  Added debugging output for internal mapper which can be visualized
	with mapviewer on subgroup -3.  

  For mapper external, implemented faster map data copy in the case when
	the new QUEUE_MAPELEMENTS option is not used which only copies
	portions of the map which have been updated and so it scales much
	better with large RNDFs.  QUEUE_MAPELEMENTS is still on by default
	in the case that mapper is run external to planner, though the speed
	benefits are not as significant now.  There is the possibility that
	the queue might introduce more latency when handling high volumes of
	sensor data so this should be looked at in the field.  

  Also the faster map copy function Map::copyMapData(Map *targetmap)
	might be useful to speed up the map copy operations in prediction.


Release R1-03l (Thu Sep 27 21:17:11 2007):
	Fixed bug in the updateGraphMap function.

Release R1-03k (Thu Sep 27 11:01:25 2007):
	Improved performance of map snapshot (copying the map for prediction still takes 50 ms in victorville).
	Overall performance gain in victorville is around 300 ms per cycles (for all the changes released so far).

Release R1-03j (Thu Sep 27  0:30:46 2007):
	Generating generic corridors and painting them appropriately in the case of using zone planners in road 
regions and intersections.  
	
Release R1-03i (Wed Sep 26 21:57:42 2007):
	Changed interface to logic-planner so that planner starts to replan if fault handling in logic-planner commands to do so

Release R1-03h (Wed Sep 26 20:49:29 2007):
	Implemented the new cmdline args: --update-lanes and --update-stoplines.
	Updated the function call to update the graph from the map.

Release R1-03g (Wed Sep 26 11:17:22 2007):
	Correctly populating the segments to load (when --step-by-step-loading is on).

Release R1-03f (Wed Sep 26  9:23:57 2007):
	Fixed circle planner calling option

Release R1-03e (Tue Sep 25 13:31:46 2007):
	Fixed bug preventing Alice from entering zones

Release R1-03d (Tue Sep 25 11:20:26 2007):
	Fixed bug preventing Alice from completing the mission after an intersection
	Pause now uses the previous path to brake on (rather than a path of length 1)

Release R1-03c (Tue Sep 25 11:10:07 2007):
	Interfaced circle-planner for zone planning. To use, give flag --use-circle-planner.
	Changed the replanning conditions and handling for s1planner.
	Implemented sending cost map to map viewer upon request. Use --show-costmap.
	Implemented setting different safety boxes around alice. This will be used during fault handling.

Release R1-03b (Mon Sep 24 18:36:15 2007):
	Changed interface to Prediction. Sends information when Alice wants to change lanes.

Release R1-03a (Sun Sep 23 15:18:29 2007):
	Added cmd line args for specifying the zone planner. 
--use-dplanner to use dplanner, --use-s1planner to use s1planner, and 
--use-circle-planner to use circle-planner. If nothing is specified, 
then the planner defaults to s1planner.

Release R1-03 (Fri Sep 21 21:29:12 2007):
	Creating an arbitraty perimeter around Alice and the exit point for zone-like planning. 

Release R1-02z (Fri Sep 21 20:14:11 2007):
	Cleaned up the zone-planning code by restructuring the planner. Implemented some functions to further assist with the planning problem specification for zone 
planners. This release effectively lost the functionality to interface with dplanner, but this will be put back (and cleaned up) soon. Initial integration of the 
circle planner.

Release R1-02y (Fri Sep 21 18:09:19 2007):
	Took prediction out of the planner loop. Prediction runs in its own thread now. Turning on/off prediction 
also turns the thread loop on and off. This could help in case the CPU load gets too high.

Release R1-02x (Fri Sep 21 16:04:22 2007):
	Major restructuring of planner to fascilitate fault handling.
	SPecifically, uses state problem from logic planner to choose planner and to set up planning problem. This was implemented for the regular driving and initial zone.
	Also, restructured the zone planning problem specification to make extension for fault handling much simpler.
	I started cleaning up the zone code, but that needs some more work.
	
	This is a pretty major restructuring, but does not add any functionality.

Release R1-02w (Fri Sep 21 12:21:53 2007):
	Added setting the direction properly in the case the path is not a subpath generated by 
s1planner. 
Release R1-02v (Fri Sep 21  5:09:47 2007):
Added basic support methods for the RNDF frame implementation. 
	Needs fused-perceptor RNDF frame update before full testing can be
	done.  The option to use the RNDF frame is off by default and can
	be enabled with the flag --use-rndf-frame.  Also improved structure
	of function calls for running mapper internal to planner.  This
	works well in simulation, but needs testing in the field to see if
	it fixed any issues

Release R1-02u (Thu Sep 20 13:43:21 2007):
	Finding and setting path direction in Cspecs for zone planning. 

Release R1-02t (Thu Sep 20 11:55:20 2007):
	Populating the CSpecsObstacle vector for dplanner for every replan.  
Release R1-02s (Thu Sep 20 10:53:06 2007):
	Using s1planner as seed for dplanner.  Extracting subpaths and resetting final conditions for compound 
trajectories for dplanner. Recoded replanning logic and error handling logic for zone planning.  

Release R1-02r (Tue Sep 18 19:07:57 2007):
	Reduced the size of obstacle sent by prediction.

Release R1-02q (Mon Sep 17  8:51:43 2007):
	Updated mapper calls in planner to allow for non-static local to
	global frame transform

Release R1-02p (Sat Sep 15 16:20:14 2007):
	Added timing info for createOtgTrajectory call (dplanner) and 
output to to log file.  
	Added replanning if Alice is not moving for more than 20 seconds 
in dplanner case as well. 

Release R1-02o (Sat Sep 15 13:04:15 2007):
	set the obstacles to be grown by 1 alice width in both directions (rather than a different amount in different directions

	added a call to cSpecs.setSafetyMargin to have it keep us ZONE_SAFETY_MARGIN away from any obstacles and the zone parimiter. currently
	ZONE_SAFETY_MARGIN is set to 0.5m; just search for ZONE_SAFETY_MARGIN in ZoneCorridor.cc if you want to change this number.
	note that the goal pose must have room for alice + the safety margin on all sides of her for clothoid planner to succeed. its ok if
	she would stick of the corridor, but if there is an obstacle that would less than the safety margin away from alice at the goal pose, then 
	clothoid planner woud fail 

Release R1-02n (Sat Sep 15  3:51:34 2007):
	*using proper map element accessor functions rather than
	ELEMENT_OBSTACLE and ELEMENT_VEHICLE 

	*commenting out return statement that caused corridor painting to
	return w/o doing anything if incoming obstacle was of type vehicle
	(This was actually Kenny's catch, but he's headed for bed)
	This *should* fix the problems seen in the field today where the 
	mapviewer and the costmap didn't agree on obstacles. 
	Kenny and I pulled Alice out of the shop, ran a zone simulation with 
	real sensor data, and the only times we saw that error were for 
	objects classified as moving vehicles. 

Release R1-02m (Fri Sep 14  6:39:06 2007):
	Minor adjustmens in the prediction part to work with the new map/mapper structure

Release R1-02l (Fri Sep 14  2:24:47 2007):
	completely changed the way replanning in zones is done- now with clothoid planner, we replan every cycle,
	but we replan by updating the clothoid tree rather than deleting it and planning from scratch.

Release R1-02k (Thu Sep 13 18:01:26 2007):
	Planner uses isObstacle and isVehicle functions now. Obstacles 
sent by prediction have their own type. The bug with clearing those 
obstacles is also solved.

Release R1-02j (Mon Sep 10 22:22:27 2007):	
	Populating cspecs with the point2arr representation of a map element for obstacles 
for planning in zones,
 
Release R1-02i (Mon Sep 10 19:09:30 2007):
	Populating cspecs with information relating to whether we are entering a parking spot, exiting a 
parking spot, or traversing a zone without parking (s1planner requirement).  

Release R1-02h (Mon Sep 10 15:47:02 2007):
	Added a state for zone planners and functionality to determine if we are entering a parking 
spot or not.  
Release R1-02g (Mon Sep 10 12:36:33 2007):
	Changed to getObsInZone call from map, however this does not work reliably.
	Re-incorporated attention module exit point information that had been overwritten in a 
previous release and fixed a bug for INTERSECTION_STRAIGHT. 
	
Release R1-02f (Fri Sep  7 19:17:59 2007):
	Added painting cost for obstacles in zone regions.  
Release R1-02e (Thu Sep  6 16:55:37 2007):
	Re-incorporated dplanner changes that were overwritten 
erroneously in the previous release.  Reset the final velocity condition 
for zones  to 0.2, the minimum requirement for dplanner.  
Release R1-02d (Wed Sep  5 13:25:04 2007):
	Added sending exit waypoint information to attention module for intersections.  

Release R1-02c (Wed Sep  5 11:56:15 2007):
	Fix possible segfault (next_goal not initialized in getFinalPose)

Release R1-02b (Tue Sep  4 20:27:31 2007):
	Added cmdline flag -use-dplanner and corresponding supoprt to use dplanner in the case of zones.  

Release R1-02a (Tue Sep  4  4:01:04 2007):
	Added new cmdline flag (--mapper-line-fusion-compat) to enable
        old line fusion behavior of fusing elements while adding them to
        the map when the internal mapper (with the external mapper, the
        old behavior is the only possible one). Default when using the
        internal mapper is to run the line fusion within the mapper main
        loop at 10Hz.

Release R1-02 (Tue Sep  4  1:20:15 2007):
	Tweaked some bitmap parameters.  
	Added smaller threshold for goal completions in zone, which will enable us to get into a parking spot properly before exiting the zone. 

Release R1-01z (Mon Sep  3 18:57:41 2007):
	Implemented cost painting for parking spots. 

Release R1-01y (Sun Sep  2 11:55:14 2007):
	Added timing information
	Replanning in zones when stopped for more than 20 seconds
	Fixed UTurn final pose
	Fixed the infinite loop issue

Release R1-01x (Sun Sep  2 17:10:35 2007):
	Fixed bug that was causing execution of planner to increase in zones.  Added cost map 
painting for zone perimeters.  

Release R1-01w (Sun Sep  2 11:25:44 2007):
	Output more information about prediction and re-compiled against latest temp-planner-interfaces and
	logic-planner

Release R1-01v (Sat Sep  1 17:26:09 2007):
	fixed bug where --mapper-enable-groundstrike-filtering caused
	mapper initialization to hang.	Should be able to run mapper
	internal to planner with all current options now. 

Release R1-01u (Thu Aug 30 17:56:44 2007):
	WE NOW HAVE ZONE OPERATION! (at least the initial working implementation)
	Fixed the extractSubPath function to correctly handle zones stages.
	Changed the isCurrectPathDone (isComplete) function to account for Alice' heading.

Release R1-01t (Thu Aug 30 17:26:53 2007):
	Minor adjustments for using prediction.

Release R1-01s (Thu Aug 30 14:22:59 2007):
	Planner is now planning ahead on the SegGoals sent by mplanner

Release R1-01r (Wed Aug 29 11:48:48 2007):
	Continued debugging of the zone region. Added some debugging information to assist with this process.
	Implemented feature where the zone path gets replanned if our new final position is different.

Release R1-01q (Wed Aug 29 19:05:13 2007):
	Changed isCurrentPathComplete to call a reverseProjection method with a path extension argument.  
Release R1-01p (Wed Aug 29 16:16:01 2007):
	changed data structures used by ZoneCorridor.cc to match modified cspecs structures

Release R1-01o (Wed Aug 29 11:21:44 2007):
	Implemented isCurrentPathComplete to replace isGoalComplete.  Checks goal completion to 0.5 m wrt to an exit point projection (if necessary) on 
the path.  For now Alice will stop at the end of goal completion.  Continuity will be handled in a later release.   
	
Release R1-01n (Wed Aug 29  8:41:25 2007):
	Initial implementation of more advanced UTurn handling, and Zone handling. Neither of these work
	fully, but at the same time it does not break existing functionality, so I wanted to get the 
	release out.
	TODO: track down bugs in extraction of subPath. Path from s1planner seems garbled. Add feature to 
	only replan in zone/uturn if environment changes (right now only plan once).

Release R1-01m (Tue Aug 28 12:35:09 2007):
	Adapted to new distToStopLine function 

Release R1-01l (Mon Aug 27 16:51:13 2007):
	adding command line option to planner that enables groundstrike
	filtering on internal map

Release R1-01k (Mon Aug 27  9:08:37 2007):
	Changed call to getDistFromStopLine to one in Utils that considers the graph for the calculation (more robust).  
Release R1-01j (Thu Aug 23 14:06:35 2007):
	Made hacked UTURN and BACKUP more robust

Release R1-01i (Thu Aug 23 16:43:26 2007):
	Fixed bug that would set the state to PASS_RIGHT in the middle of the intersection.  Sending NOMINAL planner state 
to the attention module.       

Release R1-01h (Thu Aug 23 14:00:18 2007):
	Changed the makefile to include the libraries now needed by the 
mapper. NOTE: you have to get the emap module for the planner to 
compile.

Release R1-01g (Tue Aug 21 19:17:16 2007):
	Adapted interface to Prediction and re-compiled against latest logic-planner

Release R1-01f (Tue Aug 21 18:23:08 2007):
	Not re-generating the vehicle sub-graph at intersections anymore

Release R1-01e (Tue Aug 21 16:48:32 2007):
	Removed a few messages in zones to be able to see what clothoid planner is doing.

Release R1-01d (Tue Aug 21  6:23:12 2007):
	Enabled the option to have mapper run inside a thread in planner. 
	This is off by default but can be turned on by setting the
	--mapper-use-internal flag.  Also the mapper options can be passed
	through from the command line and have a mapper prefix. 

Release R1-01c (Mon Aug 20 21:47:58 2007):
	Updated interface to IntersectionHandling so that it receives the graph now.

	DON'T USE --LOAD-GRAPH-FILES in this release unless you like debugging...

Release R1-01b (Mon Aug 20 19:45:36 2007):
	Fixed hacked UTurn maneuver

Release R1-01a (Mon Aug 20 18:30:42 2007):
	Added southface to communicate planner state to the Attention module.  Changed goal completion check to 
account for zone regions. 

Release R1-01 (Fri Aug 17 18:56:17 2007):
	Added call to clothoid planner trajectory generation and velocity planner for planning in zone 
regions.  Took out ocpspecs dependency from Makefile.yam. 

Release R1-00z (Thu Aug 16 20:10:51 2007):
	Removed usage of getWaypoint from Map (because not working with zones)
	Zones are still not working in planner yet

Release R1-00y (Wed Aug 15 18:01:40 2007):
	Correctly added the --enable-line-fusion flag (using CmdArgs now)
	Made selection of turning signal much faster (not using getLane from Map anymore)

Release R1-00x (Tue Aug 14 23:09:28 2007):
	Added a cmdline option to enable line fusion in Map
	(--enable-line-fusion, default off). Remember that
	--update-from-map is needed too for the planner to use the fused
	data.

Release R1-00w (Tue Aug 14 18:17:56 2007):
	Switched over to the cspecs interface to the zone planner. Changes were mostly in the ZoneCorridor.* files.

Release R1-00v (Tue Aug 14 13:16:24 2007):
	Forgot to add these files for the previous release...

Release R1-00u (Tue Aug 14 12:10:39 2007):
	Added ZoneCorridor.* and UT for zone region generation.
	Added multi-path-planner calls for different path planning problems.
	The Zone handling is not yet stable and should not be used until further notice. This does not break nominal planning.

Release R1-00t (Tue Aug 14  9:55:25 2007):
	Added new console that allows manual override of Intersection Handling, turn on/off prediction, override the current FSM State.

	The keys that are assigned to the new console are marked with a () in the console. Everything is lower case. One special case in the 
	intersection handling. Pressing "i" does not give the GO-command directly. It will just switch into the next state within the Intersection 
	Handling (from WAIT_LEGAL --> WAIT_POSSIBLE --> WAIT_CLEAR --> GO).

Release R1-00s (Mon Aug 13 19:59:40 2007):
	Added communication with Follower.  Changed as per new gcinterface 
specification for different types of pauses. 

Release R1-00r (Wed Aug  8 15:27:48 2007):
	Handling hacked backup/uturn maneuver

Release R1-00q (Wed Aug  8 17:30:17 2007):
	When Prediction used Particle Filters, it slowed down the planning loop drastically. Now planner decides which method prediction will use.

Release R1-00p (Wed Aug  8 13:22:32 2007):
	Now populates segment goals to LogicPlanner

Release R1-00o (Sat Aug  4  3:46:56 2007):
	Now populates velParams that is sent to VelPlanner

Release R1-00n (Fri Aug  3 20:45:29 2007):
	added --update-from-map arg

Release R1-00m (Fri Aug  3  8:46:06 2007):
	Integrated the interface to Prediction

Release R1-00l (Fri Aug  3  0:51:24 2007):
	Added argument --step-by-step-loading: causes the planner to load the RNDF segments (as graph nodes) only when needed
	Added argument --load-graph-files: causes the planner to load the graph nodes from pre-computed files rather than re-generating them
	Stopping the car while loading from the RNDF (with --step-by-step-loading on)
	Fixed mplanner recovery handling (if planner receives directive 1, it assumes that mplanner had to restart)

Release R1-00k (Tue Jul 31 19:43:08 2007):
	Calling updateGraphMap

Release R1-00j (Tue Jul 31 13:47:45 2007):
	The module prediction is now part of the planner and is linked as a library. It can be turned off by using the command 
	line argument --noprediction. The planning behaviour is not affected by prediction yet.

Release R1-00i (Fri Jul 27  9:28:53 2007):
	Enforcing continuity between plans
	UTurn implemented as a hack
	Added option for planner to close the loop by computing path from current position and only using feed-forward in gcfollower
	Controlling turn signal now

Release R1-00h (Tue Jul 24 15:36:13 2007):
	The console display is now thread (ctrl+S will not affect the planner). PlannerMain.cc is responsible for updating the 
console.
	Planner now takes a --log-level argument to set the gcmodule log level
	Modified CmdArgs to include the log filename

Release R1-00g (Tue Jul 24  8:38:21 2007):
	Added some doxygen documentation.

Release R1-00f (Mon Jul 23 19:42:42 2007):
	Added comments
	Modified console display (does not flicker anymore)

Release R1-00e (Thu Jul 19 18:45:39 2007):
	Modified the console display

Release R1-00d (Wed Jul 18 16:37:06 2007):
	Updated logic-planner. Now Intersection output is shown in the Console.

Release R1-00c (Wed Jul 18 12:34:57 2007):
	Made AliceStateHelper available to other planner libraries
	Correctly setting the errors received from the planner libraries

Release R1-00b (Mon Jul 16 13:54:42 2007):
	Now using Graph as Graph_t

Release R1-00a (Fri Jul 13 16:05:59 2007):
	Initial release of the planner module. At the moment this module 
interfaces with the mplanner at the top, and sends trajectories. The 
planner accepts the map, and an rndf.
	Four libraries where created that interfaces with the planner. 
These libraries where created in separate modules to allow for 
concurrent development. These are: path-planner, vel-planner, 
logic-planner, and graph-updater.
	There is also one temporary module (library) that contains 
selected files from GraphPlanner and a file that define the interfaces 
between the planner and it's libraries. This module will later be 
removed.
	Currently, the planner generates a maintains a graph and map. 
Inside the planner, the logic-planner is called. Next, the graph-updater 
is called to make the relavent changes (due to logic planner). The path 
planner then searches the graph to construct a path. This path is then 
passed to the velocity planner. The velocity planner returns a 
trajectory that can be evaluated by the planner and sent to follower.
	The current functionality includes: generate a graph from the 
rndf (using graph-planner fcn), update the graph with obstacles (basic), 
plan a path (using graph-planner fcns), plan velocity (using 
graph-planner function). Thus, there is no new functionality other than 
the planner itself and all the interfaces. The logic part has not been 
inserted. This only serves as a place to start.


Release R1-00 (Fri Jul  6 16:49:31 2007):
	Created.




















































<<<<<<< .working


=======

>>>>>>> .merge-right.r41199



