/*!
 * \file TrafficStateEst.cc
 * \brief Some legacy code from the tplanner. Maintains the local map and the vehicle state. Needs to be incorporated into the planner.
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include "TrafficStateEst.hh"
#include <frames/mat44.h>
#include <interfaces/sn_types.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Quadtree.hh>


#define QUEUE_MAPELEMENTS 0

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(bool waitForStateFill) 
  : CSkynetContainer(MODtrafficplanner, CmdArgs::sn_key)
  , CStateClient(waitForStateFill)
{
  DGCcreateMutex(&m_localMapUpMutex);
	DGCcreateMutex(&m_localRndfOffsetMutex);

	UpdateState();
	if (CmdArgs::mapper_use_internal){

		m_mapper = new Mapper();
		assert(m_mapper);
  }

  m_localMap = new Map(!CmdArgs::mapper_disable_line_fusion);
	m_localUpdateMap = new Map(!CmdArgs::mapper_disable_line_fusion);
  m_localMap->init();
  m_localUpdateMap->init();

  frozen = false;
  mapElements.reserve(200);
	
	loadRNDF(CmdArgs::RNDF_file);
	
	UpdateState();
	m_localMap->setLocalToSiteOffset(m_state);
	DGClockMutex(&m_localMapUpMutex);
	m_localUpdateMap->setLocalToSiteOffset(m_state);
	DGCunlockMutex(&m_localMapUpMutex);

  this->fused = NULL;
}

TrafficStateEst::~TrafficStateEst() 
{
  delete m_localMap;
	delete m_localUpdateMap;	
	if (CmdArgs::mapper_use_internal){
		delete m_mapper;
	}
		
  DGCdeleteMutex(&m_localMapUpMutex);
}


TrafficStateEst* TrafficStateEst::Instance(bool waitForStateFill)
{
  if (pinstance == 0)
    pinstance = new TrafficStateEst(waitForStateFill);
  return pinstance;
}

int TrafficStateEst::init()
{
  if (CmdArgs::use_RNDF){
    Log::getStream(1)<<"I am using the RNDF"<<endl;
    if (!CmdArgs::RNDF_file.empty()){
      Log::getStream(1)<<"RNDF is not empty "<<endl;
			

			//Check if the internal mapper is being used
			//if (!CmdArgs::mapper_use_internal){
      m_mapElemTalker.initSendMapElement(CmdArgs::sn_key);
      m_mapElemTalker.initRecvMapElement(CmdArgs::sn_key, 1);
			//}
    
    } else {
      Log::getStream(1)<<"TFEST: RNDFfilename is empty"<<endl;
    }
  } else {
    Log::getStream(1)<<"TFEST: not using RNDF"<<endl;
  }
}


void TrafficStateEst::Destroy()
{
  delete pinstance;
  pinstance = 0;
}


// REMOVE this function
// HACK this currently does a bogus translation of the vehicle local
// pose values to make them line up with the graph frame.  Local pose
// SHOULD NOT BE USED in the planner, and all references nead to be
// expunged.
void TrafficStateEst::updateState() 
{

  UpdateState();
  UpdateActuatorState();

  m_currVehState = m_state;
  m_currActState = m_actuatorState;

  // HACK HACK HACK
  //m_state.localX = m_state.utmNorthing - graph->pos.x;
  //m_state.localY = m_state.utmEasting - graph->pos.y;
  
  /*
  if (CmdArgs::use_rndf_frame){
    DGClockMutex(&m_localRndfOffsetMutex);
    m_currVehState.localX += m_localToRndfOffset.x;
    m_currVehState.localY += m_localToRndfOffset.y;
    DGCunlockMutex(&m_localRndfOffsetMutex);
  }
  */
}

VehicleState TrafficStateEst::getVehState() 
{
  return m_currVehState;
}


ActuatorState TrafficStateEst::getActState() 
{
  return m_currActState;
}

void TrafficStateEst::freezeMap()
{
  frozen = true;
}

void TrafficStateEst::meltMap()
{
  frozen = false;
}

void TrafficStateEst::getLocalMapUpdate()
{
  MapElement recvEl;
  int bytesRecv;
  Log::getStream(1) << "In local map update ..." <<endl; 

  
	int retval;
	//bool useRndfFrame = CmdArgs::use_rndf_frame;

  bool senddebug = true;
  MapElement debugEl;
  // send messages every nth time
	int everyn = 100;
  uint64_t starttime;
  uint64_t fullstarttime;

// timing variables for full update loop
	int dtime;
	int peakdtime=0;;
	double avgdtime = 0;
	int totdtime;
	int count = 0;

  // timing variables for main mapping loop
	int maindtime;
	int peakmaindtime=0;;
	double avgmaindtime = 0;
	int totmaindtime;
	int maincount = 0;

  // timing variables for map copy 
	int copydtime;
	int totcopydtime = 0;
	double avgcopydtime;
	int peakcopydtime=0;
	int copycount=0;

	int zerocount = 0;
	int errorcount = 0;

	if (CmdArgs::mapper_use_internal){

		m_mapper->initComm(CmdArgs::sn_key);
    m_mapper->enableGroundstrikeFiltering(CmdArgs::mapper_enable_groundstrike_filtering);	
		m_mapper->disableLineFusion(CmdArgs::mapper_disable_line_fusion);
		m_mapper->enableLineFusionCompat(CmdArgs::mapper_line_fusion_compat);
		m_mapper->disableObsFusion(CmdArgs::mapper_disable_obs_fusion);
		m_mapper->decayAgeThresh = CmdArgs::mapper_decay_thresh;
    m_mapper->init(CmdArgs::RNDF_file);
    m_mapper->setLocalFrameOffset(m_state);

    m_mapper->plannerOutputDisabled = true;
    m_mapper->debugSubGroup = CmdArgs::mapper_debug_subgroup;


		while (true) {

      fullstarttime = DGCgettime();

      //if (useRndfFrame){
      // DGClockMutex(&m_localRndfOffsetMutex);
        // m_localToRndfOffset = listeneroutput;
      //  DGCunlockMutex(&m_localRndfOffsetMutex);    
      //}
      
      starttime = DGCgettime();
			retval = m_mapper->mainLoop();
			maindtime = DGCgettime()-starttime;			
			
			if (senddebug){			
				totmaindtime+=maindtime;
				maincount++;
				if (maindtime>peakmaindtime) peakmaindtime=maindtime;

				if (maincount%everyn==0){
					avgmaindtime = (double)totmaindtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,8);
					debugEl.setLabel(0,"Avg main time ", avgmaindtime);
					debugEl.setLabel(1,"Peak main time ", peakmaindtime);
					debugEl.setLabel(2,"Main count ", maincount);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totmaindtime = 0;
					peakmaindtime = 0;
				}
			}

			if (retval < 0){
				if (senddebug){			
					errorcount++;
					if (errorcount%everyn==0){
						debugEl.clear();
						debugEl.setTypeDebug();
						debugEl.setId(-1,6);
						debugEl.setLabel(0,"error count ", errorcount);
						m_mapElemTalker.sendMapElement(&debugEl,-3);
					}
				}
				Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received error from mapper:updateMap = " << endl;
      }else if (retval >0){ 
				DGClockMutex(&m_localMapUpMutex);
				if (m_mapCopyState !=0){
					starttime = DGCgettime();

          m_mapper->map.copyMapData(m_localMap);

					m_mapCopyState = 0;

					if (senddebug){			
						copydtime = DGCgettime()-starttime;		
						totcopydtime+=copydtime;
						copycount++;
						if (dtime>peakcopydtime) peakcopydtime=copydtime;
					
						if (copycount%everyn==0){
							avgcopydtime = (double)totcopydtime/(double)(everyn);
						
							debugEl.clear();
							debugEl.setTypeDebug();
							debugEl.setId(-1,2);
							debugEl.setLabel(0,"Avg copy time ", avgcopydtime);
							debugEl.setLabel(1,"Peak copy time ", peakcopydtime);
							debugEl.setLabel(2,"Copy count ", copycount);
							m_mapElemTalker.sendMapElement(&debugEl,-3);

							debugEl.clear();
							debugEl.setTypeDebug();
							debugEl.setId(-1,16);
							debugEl.setLabel(0,"map size ", (int)m_mapper->map.data.size());
							debugEl.setLabel(1,"used size ", (int)m_mapper->map.usedIndices.size());
							debugEl.setLabel(2,"unused size ", (int)m_mapper->map.openIndices.size());
							m_mapElemTalker.sendMapElement(&debugEl,-3);
					
							totcopydtime = 0;
							peakcopydtime = 0;
						}
					}
				}
				DGCunlockMutex(&m_localMapUpMutex);
      
			}else{
				if (senddebug){			
					zerocount++;
					if (zerocount%everyn==0){
						debugEl.clear();
						debugEl.setTypeDebug();
						debugEl.setId(-1,5);
						debugEl.setLabel(0,"zero count ", zerocount);
						m_mapElemTalker.sendMapElement(&debugEl,-3);
					}		
				}
			}
      
      dtime = DGCgettime()-fullstarttime;			
			
			if (senddebug){			
				totdtime+=dtime;
				count++;
				if (dtime>peakdtime) peakdtime=dtime;

				if (count%everyn==0){
					avgdtime = (double)totdtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,12);
					debugEl.setLabel(0,"Avg update time ", avgdtime);
					debugEl.setLabel(1,"Peak update time ", peakdtime);
					debugEl.setLabel(2,"Update count ", count);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totdtime = 0;
					peakdtime = 0;
				}
			}

		}

  } else {

    while (true) {

      fullstarttime = DGCgettime();
      /*
        if (useRndfFrame){
        DGClockMutex(&m_localRndfOffsetMutex);
        m_localToRndfOffset = listeneroutput;
        DGCunlockMutex(&m_localRndfOffsetMutex);
        }
      */

      bytesRecv = m_mapElemTalker.recvMapElementBlock(&recvEl,1);

      if (bytesRecv>0) {
        if (recvEl.id.dat[0] == 10) {
          if (!(recvEl.type == ELEMENT_CLEAR))
            Console::increaseObstSize();
          else
            Console::increaseClearSize();
        }

        DGClockMutex(&m_localMapUpMutex);
#if QUEUE_MAPELEMENTS == 0
        m_localUpdateMap->addEl(recvEl);
#else
        if (frozen) {
          mapElements.push_back(recvEl);
        } else {
          for (unsigned int i = 0; i < mapElements.size(); i++) {
            m_localMap->addEl(mapElements[i]);
          }
          mapElements.clear();
          m_localUpdateMap->addEl(recvEl);
        }
#endif
        DGCunlockMutex(&m_localMapUpMutex);

      } else {
        Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
                          << bytesRecv << endl;
      }

      dtime = DGCgettime()-fullstarttime;			
			
			if (senddebug){			
				totdtime+=dtime;
				count++;
				if (dtime>peakdtime) peakdtime=dtime;

				if (count%everyn==0){
					avgdtime = (double)totdtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,12);
					debugEl.setLabel(0,"Avg update time ", avgdtime);
					debugEl.setLabel(1,"Peak update time ", peakdtime);
					debugEl.setLabel(2,"Update count ", count);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totdtime = 0;
					peakdtime = 0;
				}
			}

    }
  }
}

void TrafficStateEst::updateMap() {





	if (CmdArgs::mapper_use_internal){

		bool locked = true;
		// time to wait for a new map in usec
		int waittime = 1000000;
		uint64_t starttime;
		// set variable to trigger map copy in mapper thread
		DGClockMutex(&m_localMapUpMutex);
		m_mapCopyState = 1;
		DGCunlockMutex(&m_localMapUpMutex);
		
		starttime = DGCgettime();
		// wait for map copy in mapper thread
		while((int)(DGCgettime()-starttime) < waittime){
			DGClockMutex(&m_localMapUpMutex);
			if (m_mapCopyState==0){
				locked = false;
				DGCunlockMutex(&m_localMapUpMutex);
				break;
			}
			DGCunlockMutex(&m_localMapUpMutex);
			usleep(0);
		}
	}else{
#if QUEUE_MAPELEMENTS == 0
    DGClockMutex(&m_localMapUpMutex);
    m_localUpdateMap->copyMapData(m_localMap);
    DGCunlockMutex(&m_localMapUpMutex);
#else
    
    DGClockMutex(&m_localMapUpMutex);
    /* optmize by only copying the pointer and dumping the MapElement vector */
    m_localMap = m_localUpdateMap;
    for (unsigned int i = 0; i < mapElements.size(); i++) {
      m_localMap->addEl(mapElements[i]);
    }
    mapElements.clear();
    DGCunlockMutex(&m_localMapUpMutex);
#endif
    

  }
	UpdateState();
	m_localMap->setLocalToSiteOffset(m_state);

  m_localToGlobalOffset = m_localMap->getLocalToGlobalOffset();
  m_localToGlobalYawOffset = m_localMap->getLocalToGlobalYawOffset();

}



Map* TrafficStateEst::getMap() {
  return m_localMap;
}


bool TrafficStateEst::loadRNDF(string filename) {

  m_localUpdateMap->loadRNDF(filename);
  return m_localMap->loadRNDF(filename);
}


// TODO Use correct error/message handling
// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Initialize the fused perceptor
int TrafficStateEst::initFused(const char *spreadDaemon, int skynetKey)
{  
  // Create perceptor
  this->fused = new FusedPerceptor();
  assert(this->fused);

  // HACK FIX
  if (this->fused->initLive(spreadDaemon, skynetKey, MODfusedviewer + 16) != 0)
  {
    delete this->fused;
    this->fused = NULL;
    return ERROR("unable to initialize fused perceptor");
  }

  // Create the mutex
  pthread_mutex_init(&this->fusedMutex, NULL);
  
  // Create the thread
  MSG("starting fused perceptor thread");
  if (pthread_create(&this->fusedThread, NULL, (void*(*)(void*)) mainFused, this) != 0)
    return -1; // TODO 
  
  return 0;
}


// Finalize the fused perceptor
int TrafficStateEst::finiFused()
{
  if (!this->fused)
    return 0;
  
  // Stop the thread
  MSG("cancelling fused perceptor thread");
  pthread_cancel(this->fusedThread);
  pthread_join(this->fusedThread, NULL);
  MSG("cancelled fused perceptor thread");
   
  // Clean up
  pthread_mutex_destroy(&this->fusedMutex);
  this->fused->fini();
  delete this->fused;
  this->fused = NULL;
  
  return 0;
}


// Lock the fused perceptor and return a pointer
FusedPerceptor *TrafficStateEst::lockFused()
{
  pthread_mutex_lock(&this->fusedMutex);
  return this->fused;
}


// Unlock the fused perceptor
FusedPerceptor *TrafficStateEst::unlockFused()
{
  pthread_mutex_unlock(&this->fusedMutex);
  return NULL;
}


// Main loop for fused perceptor
int TrafficStateEst::mainFused(TrafficStateEst *self)
{
  FusedPerceptor *fused = self->fused;

  if (!fused)
    return 0;

  MSG("running fused perceptor thread");
  while (true)
  {
    pthread_testcancel();

    // Wait for new data.  The thread cancel call will force this to
    // exit if we are currently blocked.
    if (sensnet_wait(self->fused->sensnet, -1) == 0)
    {
      pthread_mutex_lock(&self->fusedMutex);
      fused->updateLive();
      pthread_mutex_unlock(&self->fusedMutex);
      //MSG("fused time %.3f", (double) fused->state.timestamp * 1e-6);
    }
  }
  
  return 0;
}


// Update the graph with data from the fused perceptor
int TrafficStateEst::updateFusedGraph(Graph_t *graph, const VehicleState *state)
{
  int i;
  GraphNode *node;
  vector<GraphNode*> nodes;
  float m[4][4];
  float lx, ly, px, py, size;

  // Lock access to the perceptor.  This is probably not necessary,
  // but is done for the sake of good form.
  pthread_mutex_lock(&this->fusedMutex);

  // Prepare the cost map.  
  this->fused->prepare(state);

  // Size of the local map (no point looking outside this region)
  size = this->fused->localMap->size * this->fused->localMap->scale;

  // Get the transform from local to site frame
  mat44f_setf(m, this->fused->transSL);

  // Compute the region of interest in the site frame
  lx = this->fused->localMap->pose.pos.x;
  ly = this->fused->localMap->pose.pos.y;
  px = m[0][0]*lx + m[0][1]*ly + m[0][3];
  py = m[1][0]*lx + m[1][1]*ly + m[1][3];

  MSG("getting quadtree %.3f %.3f %.3f %.3f",
      px - size/2, px + size/2, py - size/2, py + size/2);
  
  // Get all the nodes in the local region
  graph->quadtree->get_all_nodes(nodes, px - size/2, px + size/2, py - size/2, py + size/2);

  MSG("got %d of %d nodes", (int) nodes.size(), graph->numNodes);
  
  // Update each node
  for (i = 0; i < (int) nodes.size(); i++)
  {    
    node = nodes[i];

    float lineDist;
    lineDist = this->fused->calcLineDist(node->pose_x, node->pose_y, node->pose_h);

    //MSG("lineDist %f %f %f", node->pose_x, node->pose_y, lineDist);

    // TESTING HACK
    // Assume a fixed lane width
    float width = 4.0;    
    node->centerDist = fabs(width/2 - lineDist);
    if (node->centerDist > width/2)
      node->centerDist = width/2;
  }

  // Unlock access to the perceptor
  pthread_mutex_unlock(&this->fusedMutex);
  
  return 0;
}

