/**********************************************************
 **
 **  UT_PLANNER.CC
 **
 **    Author: Noel du Toit
 **    Created: Tue Jul 10 14:03:52 2007
 **
 **
 **********************************************************
 **
 **  Unit test for the planner stack
 **
 **********************************************************/

#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "graph-updater/GraphUpdater.hh"
#include "path-planner/PathPlanner.hh"
#include "vel-planner/VelPlanner.hh"
#include "interfaces/VehicleState.h"
#include "interfaces/ActuatorState.h"
#include "frames/pose3.h"
#include "temp-planner-interfaces/CmdArgs.hh"
#include <iostream>
#include <sstream>
#include <fstream>
#include <trajutils/traj.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <skynet/skynet.hh>
#include "temp-planner-interfaces/Graph.hh"
#include "temp-planner-interfaces/AliceStateHelper.hh"

using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);
  map->prior.delta = point2(3778410.5, 403942.3);

  // INITIALIZE THE GRAPH
  Graph_t* graph;
  GraphNode* node;
    
  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey(argc, args);
  GraphUpdater::init(&graph, map);
  vec3_t offset = graph->getNode(0)->pose.pos;

  ofstream file_out;
  file_out.open("plot_graph.m");
  file_out<< "clear all; close all;" << endl;
  // print graph to file that can be plotted in matlab
  if (1) {
    
    ostringstream x, y;
    x << "nodes_x = [ ";
    y << "nodes_y = [ ";
    for (int i=0; i<graph->getStaticNodeCount(); i++) {
      node = graph->getNode(i);
      x << node->pose.pos.x << " ";
      y << node->pose.pos.y << " ";
    }
    x << "];" << endl;
    y << "];" << endl;
    
    file_out << "% underlying graph output" << endl << x.str() << y.str();
    file_out << "plot(nodes_x, nodes_y, 'b.'); hold on;" << endl;
  }

  
  // PLAN PATH
  Path_t path;
  memset(&path, 0 ,sizeof(path));
  VehicleState vehState;
  memset(&vehState, 0, sizeof(vehState));
  ActuatorState actState;
  memset(&actState, 0, sizeof(actState));
  Cost_t cost;
  pose3_t finPose;
  double roll, pitch, yaw;

  // set the vehicle state to the first waypoint
  GraphNode* initNode = graph->getNodeFromRndfId(1,1,1);
  vehState.localX = initNode->pose.pos.x;
  vehState.localY = initNode->pose.pos.y;
  vehState.localZ = initNode->pose.pos.z;
  quat_to_rpy(initNode->pose.rot, &roll, &pitch, &yaw);
  vehState.localYaw = yaw;

  //  vehState.localX = 0;
  //  vehState.localY = .05;
  //  vehState.localZ = 0;
  //  quat_to_rpy(initNode->pose.rot, &roll, &pitch, &yaw);
  //  vehState.localYaw = -0.79;


  // set the actuator state
  //actState.m_steerPos
  
  // set final pose
  GraphNode* finalNode = graph->getNodeFromRndfId(1,2,3);
  finPose = finalNode->pose;
  finPose.pos.x = 107.2;
  finPose.pos.y = -32.4;
  finPose.pos.z = 0;
  finPose.rot = quat_from_rpy(0, 0, -0.79);

  // plan the path
  Path_params_t pathParams;
  GraphUpdater::genVehicleSubGraph(graph, vehState, actState);
  int err = PathPlanner::planPath(&path, cost, graph, vehState, finPose, pathParams);
  
  if (1) {
    ostringstream x, y;
    x << "path_x = [ ";
    y << "path_y = [ ";
    for (int i=0; i<path.pathLen; i++) {
      node = path.path[i];
      x << node->pose.pos.x << " ";
      y << node->pose.pos.y << " ";
    }
    x << "];" << endl;
    y << "];" << endl;
    
    file_out << "% path output" << endl << x.str() << y.str();
    file_out << "plot(path_x, path_y, 'r-'); hold on;" << endl;
  }

  // VELOCITY PLANNER
  err = VelPlanner::init();
  // initialize trajectory
  CTraj traj(3);
  // initialize state problem
  StateProblem_t stateProblem;
  stateProblem.state = DRIVE;
  stateProblem.probability = 1.0;
  stateProblem.flag = NO_PASS;

  // plan the velocity
  Vel_params_t velParams;
  err = VelPlanner::planVel(&traj, &path, stateProblem, vehState, velParams);

  if (1) {
    ostringstream x, y, dx, dy, ddx, ddy;
    x << "traj_x = [ ";
    y << "traj_y = [ ";
    dx << "traj_dx = [ ";
    dy << "traj_dy = [ ";
    ddx << "traj_ddx = [ ";
    ddy << "traj_ddy = [ ";
    for (int i=0; i<traj.getNumPoints(); i++) {
      x << traj.getNorthing(i) << " ";
      y << traj.getEasting(i) << " ";
      dx << traj.getNorthingDiff(i, 1) << " ";
      dy << traj.getEastingDiff(i, 1) << " ";
      ddx << traj.getNorthingDiff(i, 2) << " ";
      ddy << traj.getEastingDiff(i, 2) << " ";
    }
    x << "];" << endl;
    y << "];" << endl;
    dx << "];" << endl;
    dy << "];" << endl;
    ddx << "];" << endl;
    ddy << "];" << endl;
    
    file_out << "% path output" << endl << x.str() << y.str() << dx.str() << dy.str() << ddx.str() << ddy.str();
    file_out << "speed = sqrt(traj_dx.^2 + traj_dy.^2);" << endl;
    file_out << "acc = sqrt(traj_ddx.^2 + traj_ddy.^2);" << endl;
    file_out << "arclen(1) = 0;" << endl;
    file_out << "for ii=2:length(traj_x) arclen(ii)=arclen(ii-1)+sqrt((traj_x(ii)-traj_x(ii-1))^2 + (traj_y(ii)-traj_y(ii-1))^2);  end" << endl;
    file_out << "plot(traj_x, traj_y, 'g-'); hold off;" << endl;
    file_out << "figure;" << endl;
    file_out << "plot(arclen, speed, 'g'); hold on;" << endl << "plot(arclen, acc, 'r');" << endl;
  }

  file_out.close();

  // DISPLAY INFORMATION IN MAPVIEWER
  if (1) {
    // DISPLAY GRAPH
    GraphUpdater::display(10, graph, vehState);

    // DISPLAY PATH
    PathPlanner::display(10, &path);

    // DISPLAY TRAJ
    VelPlanner::display(10, &traj);
  }
}
