/*!
 * \file Console.hh
 * \brief Console header for the planning stack as a whole
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef _CONSOLE_H
#define _CONSOLE_H

#include <ncurses.h>
#include <sstream>
#include <fstream>

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <trajutils/traj.hh>
#include <map/MapPrior.hh>
#include <pthread.h>

class Console {

public:
  static void init();
  static void refresh();
  static void destroy();
  
  static void updateRate(double time);
  static void updateTrajectory(CTraj *traj);
  static void updateState(VehicleState &vehState);
  static void updateInter(bool SafetyFlag, bool CabmodeFlag, int CountPrecedence, string Status);
  static void updateLastWpt(PointLabel last);
  static void updateNextWpt(PointLabel next);
  static void updateTurning(int direction);
  static void updateFSMFlag(FSM_flag_t flag);
  static void updateFSMState(FSM_state_t state);
  static void addMessage(char *format, ...);
  static void refresh_window();
  static void thread_refresh();

private:
  static WINDOW *fsm_win;
  static WINDOW *traj_win;
  static WINDOW *state_win;
  static WINDOW *inter_win;
  static WINDOW *msg_win;
  static char messages[6][77];
  static bool initialized;
  static pthread_cond_t refresh_notifier;
  static pthread_mutex_t refresh_mutex;
  static WINDOW *refresh_win;
};

#endif
