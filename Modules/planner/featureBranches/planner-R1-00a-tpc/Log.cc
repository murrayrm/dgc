#include "Log.hh"
#include "CmdArgs.hh"

#include <math.h>

/** Initialize verbose level to maximum */
int Log::verbose_level = 9; 
NullStream Log::nStream;
bool Log::generic_logging = false;
ofstream Log::genericFileStream;
DoubleStream Log::dualStream(genericFileStream, cout);

int Log::getVerboseLevel()
{
    return verbose_level;
}

void Log::setVerboseLevel(int level)
{
    verbose_level = level;
}

void Log::setGenericLogFile(const char *filename)
{
  genericFileStream.open(filename);
  if (genericFileStream.is_open()) {
    generic_logging = true;
    genericFileStream << "TPlanner log file [START]" << endl << endl;
  }
}

ostream& Log::getStream(int level)
{
    if (verbose_level >= level) {
      if (generic_logging) {
        if (!CmdArgs::console)
          return dualStream;
        else
          return genericFileStream;
      } else {
        return cout;
      }
    } else {
      return nStream;
    }
}
