/**********************************************************
 **
 **  UT_ZONECORRIDOR.CC
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 23:28:44 2007
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <s1planner/tempClothoidInterface.hh>

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);
  map->prior.delta = point2(3778410.5, 403942.3);
  CmdArgs::sn_key = skynet_findkey(argc, args);
  int sn_key = (int)CmdArgs::sn_key;

  // set up some problem to solve
  Corridor_t cSpec;
  Path_params_t params;

  // Specify some zone to plan in
  params.flag = ZONE;
  params.planFromCurrPos = false;
  params.zoneId = 14;
  params.spotId = 1;
  params.exitId = 2;
  params.velMin = 0;
  params.velMax = 5;

  // Specify some vehicle state
  VehicleState vehState;
  memset(&vehState, 0, sizeof(vehState));
  vehState.localX = 2876;
  vehState.localY = -4914;
  vehState.localZ = 0;
  vehState.localYaw = 0;

  point2 first, second;
  SpotLabel spotLabel(params.zoneId, params.spotId);
  map->getSpotWaypoints(first, second, spotLabel);
  double finHeading = atan2(second.y-first.y, second.x-first.x);
  cout << "heading = " << finHeading << endl;
  pose3_t finPose;
  finPose.pos.x = second.x;
  finPose.pos.y = second.y;
  finPose.pos.z = 0;
  finPose.rot = quat_from_rpy(0, 0, finHeading);

  // get the zone region
  point2arr zonePerimeter;
  map->getZonePerimeter(zonePerimeter, params.zoneId);
  //  zonePerimeter.push_back(zonePerimeter.arr[0]);
  /*
  point2 point;
  point.set(initPose.pos.x-15,initPose.pos.y+7.5);
  zonePerimeter.push_back(point);
  point.set(initPose.pos.x+15,initPose.pos.y+7.5);
  zonePerimeter.push_back(point);
  point.set(initPose.pos.x+15,initPose.pos.y-2.5);
  zonePerimeter.push_back(point);
  point.set(initPose.pos.x+0,initPose.pos.y-20);
  zonePerimeter.push_back(point);
  point.set(initPose.pos.x-15,initPose.pos.y-2.5);
  zonePerimeter.push_back(point);
  point.set(initPose.pos.x-15,initPose.pos.y+7.5);
  zonePerimeter.push_back(point);
  */
  //cout << "zone = " << endl << zonePerimeter << endl;

  // Generate the appropriate corridor
  Path_t* path = NULL;

  ZoneCorridor::generateCorridor(cSpec, zonePerimeter, params, finPose, vehState);

  
  //double runtime = 0.2;
  //GenerateTrajOneShot(CmdArgs::sn_key, true, ocpParams, polyCorridor, bmparams, path, runtime);

  delete path;
  //  delete polyCorridor;
}
