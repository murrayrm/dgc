/**********************************************************
 **
 **  ZONECORRIDOR.CC
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 17:23:25 2007
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/CmdArgs.hh>

#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0:  0)


Err_t ZoneCorridor::generateCorridor(Corridor_t& cSpec, point2arr zonePerimeter, Path_params_t params, pose3_t finPose, VehicleState vehState)
{
  cSpec.ocpParams = OCPparams();

  // set up ocp params problem
  point2 initPos, finPos;
  initPos = AliceStateHelper::getPositionRearAxle(vehState);
  double initHeading = AliceStateHelper::getHeading(vehState);
  double initVel = AliceStateHelper::getVelocityMag(vehState);
  double initAcc = AliceStateHelper::getAccelerationMag(vehState);
  double initSteer = 0;

  int mode = 0;

  finPos.x = finPose.pos.x;
  finPos.y = finPose.pos.y;
  double roll, pitch, finHeading;
  quat_to_rpy(finPose.rot, &roll, &pitch, &finHeading);
  double finVel = 0;
  double finAcc = 0;
  double finSteer = 0;

  setOCPparams(cSpec.ocpParams, params.velMax, params.velMin, initPos, initHeading, initVel, initAcc, initSteer, mode, finPos, finHeading, finVel, finAcc, finSteer);

  int nPolytopes = 1;
  generatePolyCorridor(&cSpec.polyCorridor, nPolytopes, zonePerimeter);

  // define the cost map
  PolygonParams polygonParams;
  cSpec.bmParams.resX = 0.1;
  cSpec.bmParams.resY = 0.1;
  cSpec.bmParams.width =800;
  cSpec.bmParams.height = 800;
  cSpec.bmParams.baseVal = 100 ;
  cSpec.bmParams.outOfBounds = 200;
  polygonParams.centerlaneVal = 1;
  polygonParams.obsCost = 1000;
  getBitmapParams(cSpec.bmParams, polygonParams, initPos, zonePerimeter);
  
  // some debug info ...
  if (1) {
    // print corridor
    print(zonePerimeter, cSpec.ocpParams, cSpec.polyCorridor);
    
    // display the corridor
    display(CmdArgs::sn_key, 10, zonePerimeter, cSpec.ocpParams, cSpec.bmParams);
  }

  //ocpSpecs->setOCPspecs(ocpParams, polyCorridor, bmparams);

  //delete polyCorridor;
  //delete ocpSpecs;
  return 0;
}

void ZoneCorridor::setOCPparams(OCPparams& ocpParams, double velMax, double velMin, point2 initPos, double initHeading, double initVel, double initAcc, double initSteer, int mode, point2 finPos, double finHeading, double finVel, double finAcc, double finSteer)
{

  // set min and max speed to curr segment min/max speed (from mdf)
  ocpParams.parameters[VMIN_IDX_P] = velMin;
  ocpParams.parameters[VMAX_IDX_P] = velMax;

  // populate the initial conditions - lower bound
  ocpParams.initialConditionLB[EASTING_IDX_C] = initPos.y;
  ocpParams.initialConditionLB[NORTHING_IDX_C] = initPos.x;
  ocpParams.initialConditionLB[VELOCITY_IDX_C] = initVel;
  ocpParams.initialConditionLB[HEADING_IDX_C] = initHeading;
  ocpParams.initialConditionLB[ACCELERATION_IDX_C] = initAcc;
  ocpParams.initialConditionLB[STEERING_IDX_C] = initSteer;

  // populate the initial conditions - upper bound
  ocpParams.initialConditionUB[EASTING_IDX_C] = initPos.y;
  ocpParams.initialConditionUB[NORTHING_IDX_C] = initPos.x;
  ocpParams.initialConditionUB[VELOCITY_IDX_C] = initVel;
  ocpParams.initialConditionUB[HEADING_IDX_C] = initHeading;
  ocpParams.initialConditionUB[ACCELERATION_IDX_C] = initAcc;
  ocpParams.initialConditionUB[STEERING_IDX_C] = initSteer;

  // initialize to fwd mode by default
  ocpParams.mode = mode;

  // populate the initial conditions - lower bound
  ocpParams.finalConditionLB[EASTING_IDX_C] = finPos.y;
  ocpParams.finalConditionLB[NORTHING_IDX_C] = finPos.x;
  ocpParams.finalConditionLB[VELOCITY_IDX_C] = finVel;
  ocpParams.finalConditionLB[HEADING_IDX_C] = finHeading;
  ocpParams.finalConditionLB[ACCELERATION_IDX_C] = finAcc;
  ocpParams.finalConditionLB[STEERING_IDX_C] = finSteer;

  // populate the initial conditions - upper bound
  ocpParams.finalConditionUB[EASTING_IDX_C] = finPos.y;
  ocpParams.finalConditionUB[NORTHING_IDX_C] = finPos.x;
  ocpParams.finalConditionUB[VELOCITY_IDX_C] = finVel;
  ocpParams.finalConditionUB[HEADING_IDX_C] = finHeading;
  ocpParams.finalConditionUB[ACCELERATION_IDX_C] = finAcc;
  ocpParams.finalConditionUB[STEERING_IDX_C] = finSteer;
}

void ZoneCorridor::generatePolyCorridor(CPolytope** polyCorridor, int &nPolytopes, point2arr zonePerimeter)
{  
  bool isLegal = true;
  bool polyCorridorLegal;
  
  int NofDim = 2;
  CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
  int NofVtx = zonePerimeter.size();
  double*** vertices;
  vertices = new double**[nPolytopes];
  for(int k=0; k<nPolytopes;k++) {
    vertices[k] = new double*[NofDim];
    for(int i=0;i<NofDim;i++) {
      vertices[k][i] = new double[NofVtx];
    }
  }
  
  for (int k=0; k<NofVtx; k++) {
    vertices[0][0][k]=zonePerimeter.arr[k].x;
    vertices[0][1][k]=zonePerimeter.arr[k].y;
  }

  *polyCorridor = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[0] ); //Ask Melvin: is this Correct?
  ALGErrorType algErr = algGeom->VertexAndFacetEnumeration(*polyCorridor);
  
  if( algErr != alg_NoError) {
    cout << "CORRIDOR: error when building polytopes" << endl;
    isLegal = false;
  }
  
  for (int i=0; i<nPolytopes; i++) {
    for(int j=0; j<NofDim; j++)
      delete[] vertices[i][j];
    delete[] vertices[i];
  }
  delete[] vertices;
  delete algGeom;
  
  if(isLegal)
    polyCorridorLegal = true;
  else {   
    cout << "CORRIDOR: FAILED - Corridor generation " << endl;
    polyCorridorLegal = false;
  }
}

void ZoneCorridor::convertZoneToPolygon(vector<Polygon>& polygons, point2arr& zonePerimeter, float val)
{
  val = sqrt(val);

  vector<float> cost;
  point2arr_uncertain vertices(zonePerimeter);
  for (int i=0; i<zonePerimeter.size(); i++) {
    cost.push_back(val);
  }
  
  Polygon polygon;
  polygon.setVertices(vertices, cost);
  polygon.setFillFunc(FILL_SQUARE);
  polygon.setCombFunc(COMB_REPLACE);

  polygons.push_back(polygon);
}

void ZoneCorridor::getBitmapParams(BitmapParams& bmparams, PolygonParams &polygonParams,
                     point2 alicePos, point2arr zonePerimeter)
{
  bmparams.centerX = alicePos.x;
  bmparams.centerY = alicePos.y;

  bmparams.polygons.clear();
  convertZoneToPolygon(bmparams.polygons, zonePerimeter,polygonParams.centerlaneVal);
}

void ZoneCorridor::display(int sn_key, int sendSubgroup, point2arr zonePerimeter, OCPparams ocpParams, BitmapParams bmparams)
{
  CMapElementTalker meTalker;
  meTalker.initSendMapElement(sn_key);
  int counter=12000;
  point2 point;
  MapId mapId;
  MapElement me;
  
  mapId = counter;

  // display the corridor
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_DARK_GREEN, 100);
  me.setGeometry(zonePerimeter);
  meTalker.sendMapElement(&me,sendSubgroup);
  
  // print the initial and final cond's
  vector<point2> points;
  point.set(ocpParams.initialConditionUB[NORTHING_IDX_C], ocpParams.initialConditionUB[EASTING_IDX_C]);
  points.push_back(point);
  point.set(ocpParams.finalConditionUB[NORTHING_IDX_C], ocpParams.finalConditionUB[EASTING_IDX_C]);
  points.push_back(point);
  mapId = 12001;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_YELLOW, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

  // send the costmap too
  // Instantiate a talker
  SkynetTalker< BitmapParams > polyTalker(sn_key, SNbitmapParams, 
                                          MODtrafficplanner);
 polyTalker.send(&bmparams);


}

void ZoneCorridor::print(point2arr zonePerimeter, OCPparams ocpParams, CPolytope* polyCorridor)
{
  MSG("Init Pos = (%6.2f,%6.2f)",ocpParams.initialConditionLB[NORTHING_IDX_C], ocpParams.initialConditionLB[EASTING_IDX_C]);
  MSG("Fin Pos = (%6.2f,%6.2f)",ocpParams.finalConditionLB[NORTHING_IDX_C], ocpParams.finalConditionLB[EASTING_IDX_C]);
  cout << "polycorridor:" << endl << polyCorridor << endl;
}


