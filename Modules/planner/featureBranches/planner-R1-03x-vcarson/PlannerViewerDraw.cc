
/* 
 * Desc: Planner viewer drawing functions
 * Date: 29 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>

#include "PlannerViewer.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "%s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// Draw a set of axes
void PlannerViewer::drawAxes(float size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Draw a text box
void PlannerViewer::drawText(float size, const char *text)
{
  int i, count;
  float sx, sy;

  // Compute the scale factors in x/y directions, based on the 'M'
  // character.
  sx = glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, 'M');
  sy = sx * 1.3;
    
  glPushMatrix();
  glScalef(size / sx, size / sy, 1);
  glTranslatef(0, -sy, 0);

  glPushMatrix();
    
  for (i = 0, count = 0; i < (int) strlen(text); i++, count++)
  {
    if (text[i] == '\n')
    {
      glPopMatrix();
      glTranslatef(0, -sy, 0);
      glPushMatrix();
      count = 0;      
    }
    else
    {
      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[i]);
    }
  }

  glPopMatrix();
  glPopMatrix();

  return;
}


// Draw Alice (vehicle frame)
void PlannerViewer::drawAlice()
{
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
    
  return;
}


// Pre-draw the RNDF to a display list
void PlannerViewer::predrawRndf()
{
  int i;
  Graph_t *graph;
  GraphNode *node;
  
  // Create display list
  if (this->rndfList == 0)
    this->rndfList = glGenLists(1);
  glNewList(this->rndfList, GL_COMPILE);

  graph = this->planner->m_graph;

  // Draw the nodes
  for (i = 0; i < graph->getNodeCount(); i++)
  {
    node = graph->getNode(i);
    if (node->isWaypoint)
      this->drawRndfNode(node);
  }
  
  glEndList();
    
  return;
}


// Draw an RNDF waypoint
void PlannerViewer::drawRndfNode(GraphNode *node)
{
  char text[256];
  GraphArc *arc;
  GLUquadric *quad;

  /// TODO
  //if (node->isCheckpoint)
  //{
  //  glColor3f(1, 1, 0);
  //  snprintf(text, sizeof(text), "%d.%d.%d\n CP %d",
  //           node->segmentId, node->laneId, node->waypointId, node->checkpointId);
  //}
  //else
  {
    glColor3f(0.7, 0, 0.7);
    snprintf(text, sizeof(text), "%d.%d.%d",
             node->segmentId, node->laneId, node->waypointId);
  }

  quad = gluNewQuadric();

  // Draw a ciricle with text around the waypoint
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glPushMatrix();
  glTranslatef(node->pose_x, node->pose_y, 0);
  glRotatef(180, 0, 1, 0);
  gluDisk(quad, 0.50, 0.50, 16, 1);
  glTranslatef(-0.40, 0, 0);
  if (true)
    this->drawText(0.2, text);
  glPopMatrix();

  gluDeleteQuadric(quad);
    
  // Draw the outgoing arcs
  glBegin(GL_LINES);    
  for (arc = node->outFirst; arc != NULL; arc = arc->outNext)
  {
    if (arc->type != GRAPH_ARC_WAYPOINT)
      continue;
    glVertex2d(arc->nodeA->pose_x, arc->nodeA->pose_y);
    glVertex2d(arc->nodeB->pose_x, arc->nodeB->pose_y);
  }
  glEnd();

  return;
}


// Pre-draw the graph to a display list
void PlannerViewer::predrawGraph(FusedPerceptor *fused, int props)
{
  int i;
  Graph_t *graph;
  GraphNode *node;
  vector<GraphNode*> nodes;
  float m[4][4];
  float lx, ly, px, py, size;
  
  // Create display list
  if (this->graphList == 0)
    this->graphList = glGenLists(1);
  glNewList(this->graphList, GL_COMPILE);

  graph = this->planner->m_graph;
  
  // Size of the local map (no point looking outside this region)
  size = fused->localMap->size * fused->localMap->scale;
  
  // Compute the local region in planner (graph) coordinates
  mat44f_setf(m, fused->transPL);
  lx = fused->localMap->pose.pos.x;
  ly = fused->localMap->pose.pos.y;
  px = m[0][0]*lx + m[0][1]*ly + m[0][3];
  py = m[1][0]*lx + m[1][1]*ly + m[1][3];

  // Get the nodes in this regions
  graph->quadtree->get_all_nodes(nodes, px - size/2, px + size/2, py - size/2, py + size/2);

  // Draw the nodes
  for (i = 0; i < (int) nodes.size(); i++)
  {
    node = nodes[i];
    this->drawNode(node, props);
  }
  
  glEndList();
    
  return;
}


// Draw an indivual graph node
void PlannerViewer::drawNode(GraphNode *node, int props)
{
  GraphArc *arc;

  // Check if we should be drawing this type of node
  if ((props & (1 << CMD_GRAPH_RAILS)) == 0)
    if (node->railId != 0)
      return;
  if ((props & (1 << CMD_GRAPH_CHANGES)) == 0)
    if (node->type == GRAPH_NODE_CHANGE)
      return;
  
  //MSG("node %f %f %f", node->pose_x, node->pose_y, node->pose_h);

  // Pick the node color based on it's properties
  if (node->centerDist > 1.0)
    glColor3f(0.5, 0, 0);
  else
    glColor3f(1, 1, 1);
  
  // Draw the node center
  glPointSize(3);
  glBegin(GL_POINTS);  
  glVertex2d(node->pose_x, node->pose_y);
  glEnd();
  
  if ((props & (1 << CMD_GRAPH_ARCS)))
  {
    // Draw the outgoing arcs
    glBegin(GL_LINES);    
    for (arc = node->outFirst; arc != NULL; arc = arc->outNext)
    {
      if (arc->type == GRAPH_ARC_WAYPOINT)
        continue;
      if ((props & (1 << CMD_GRAPH_RAILS)) == 0)
        if (arc->nodeB->railId != 0)
          continue;
      if ((props & (1 << CMD_GRAPH_CHANGES)) == 0)
        if (arc->nodeB->type == GRAPH_NODE_CHANGE)
          continue;

      glColor3f(0.5, 0.5, 0.5);
      glVertex2d(arc->nodeA->pose_x, arc->nodeA->pose_y);
      glColor3f(1, 1, 1);
      glVertex2d(arc->nodeB->pose_x, arc->nodeB->pose_y);

      //MSG("%f %f to %f %f",
      //    arc->nodeA->pose_x, arc->nodeA->pose_y,
      //    arc->nodeB->pose_x, arc->nodeB->pose_y);                
    }
    glEnd();
  }
  
  return;
}


// Pre-draw the fused map to a display list
void PlannerViewer::predrawFused(FusedPerceptor *fused, int props)
{
  float mx, my;
  dgc_image_t *image;

  // Create texture
  if (this->fusedTex == 0)
    glGenTextures(1, &this->fusedTex);

  // Select the map image
  image = fused->stereoRoadFinder->updateLineImage(fused->localMap);

  // Copy image into texture
  glBindTexture(GL_TEXTURE_2D, this->fusedTex);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  gluBuild2DMipmaps(GL_TEXTURE_2D, 4,
                    image->cols, image->rows,
                    (image->channels == 3 ? GL_RGB : GL_LUMINANCE), GL_UNSIGNED_BYTE, image->data);
  
  // Create display list
  if (this->fusedList == 0)
    this->fusedList = glGenLists(1);
  glNewList(this->fusedList, GL_COMPILE);

  // Transform into map frame (assume we are currently in the local frame).
  // TODO Should do full transform rather than simple translation
  glPushMatrix();
  glTranslatef(fused->localMap->pose.pos.x, fused->localMap->pose.pos.y, 0.10);
  
  glBindTexture(GL_TEXTURE_2D, this->fusedTex);

  // Draw the image as a textured quad
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glColor3f(1, 1, 1);
  glEnable(GL_TEXTURE_2D);
  glBegin(GL_QUADS);  
  fused->localMap->cvtIndexToPos(0, 0, &mx, &my); 
  glTexCoord2f(0, 0);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(fused->localMap->size, 0, &mx, &my); 
  glTexCoord2f(1, 0);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(fused->localMap->size, fused->localMap->size, &mx, &my);
  glTexCoord2f(1, 1);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(0, fused->localMap->size, &mx, &my); 
  glTexCoord2f(0, 1);
  glVertex2f(mx, my);
  glEnd();
  glDisable(GL_TEXTURE_2D);
  
  // Draw the bounding rectangle
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glColor3f(0, 0, 1);
  glBegin(GL_QUADS);  
  fused->localMap->cvtIndexToPos(0, 0, &mx, &my); 
  glTexCoord2f(0, 0);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(fused->localMap->size, 0, &mx, &my); 
  glTexCoord2f(1, 0);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(fused->localMap->size, fused->localMap->size, &mx, &my);
  glTexCoord2f(1, 1);
  glVertex2f(mx, my);
  fused->localMap->cvtIndexToPos(0, fused->localMap->size, &mx, &my); 
  glTexCoord2f(0, 1);
  glVertex2f(mx, my);
  glEnd();
  
  glPopMatrix();
  glEndList();
    
  return;
}


// Draw the trajectory (local frame)
void PlannerViewer::drawTraj(CTraj *traj)
{
  int i;
  vec3_t p, q;

  // Draw velocity vectors.  This assumes the trajectory is in the
  // local frame, and we are drawing into the local frame.
  glPointSize(2.0);
  glColor3f(0, 1, 0);  
  for (i = 0; i < traj->getNumPoints(); i++)
  {
    p.x = traj->getNdiffarray(0)[i];
    p.y = traj->getEdiffarray(0)[i];    

    q.x = p.x + traj->getNdiffarray(1)[i] * 0.5;
    q.y = p.y + traj->getEdiffarray(1)[i] * 0.5;    

    //printf("%d %f %f\n", i, p.x, p.y);

    glBegin(GL_POINTS);
    glVertex2d(p.x, p.y);
    glEnd();    

    glBegin(GL_LINES);
    glVertex2d(p.x, p.y);
    glVertex2d(q.x, q.y);
    glEnd();
  }
  glPointSize(1.0);

  
  // Use this code if the trajectory is in the global frame and we
  // want to draw in the graph frame.
  /*
  Graph_t *graph;
  
  graph = this->planner->m_graph;

  MSG("traj %d", traj->getNumPoints());
  
  // Draw velocity vectors
  glPointSize(2.0);
  glColor3f(0, 1, 0);  
  for (i = 0; i < traj->getNumPoints(); i++)
  {
    p.x = traj->getNdiffarray(0)[i];
    p.y = traj->getEdiffarray(0)[i];    

    q.x = p.x + traj->getNdiffarray(1)[i] * 0.5;
    q.y = p.y + traj->getEdiffarray(1)[i] * 0.5;    

    printf("%d %f %f\n", i, p.x, p.y);

    glBegin(GL_POINTS);
    glVertex2d(p.x - graph->pos.x, p.y - graph->pos.y);
    glEnd();    

    glBegin(GL_LINES);
    glVertex2d(p.x - graph->pos.x, p.y - graph->pos.y);
    glVertex2d(q.x - graph->pos.x, q.y - graph->pos.y);
    glEnd();
  }
  glPointSize(1.0);
  */
  
  return;
}
