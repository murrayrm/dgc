
/* 
 * Desc: Copy options to the arguments class; this should be removed
 * Date: 30 September 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLANNER_ARGS_HH
#define PLANNER_ARGS_HH

#include "temp-planner-interfaces/planner_cmdline.h"

// Fill out the options.  This function should be DEPRECATED
int plannerArgs(int argc, char **argv, struct planner_options *options);

#endif
