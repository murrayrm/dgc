/*!
 * \file TrafficStateEst.hh
 * \brief Some legacy code from the tplanner. Maintains the local map and the vehicle state. Needs to be incorporated into the planner.
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef TRAFFICSTATEEST_HH_
#define TRAFFICSTATEEST_HH_

#include <vector>
#include <deque>
#include <map/Map.hh>
#include <mapper/Mapper.hh>
#include <interfaces/VehicleState.h>
#include <skynettalker/StateClient.hh>
#include <map/MapElementTalker.hh>
#include <temp-planner-interfaces/PlanGraph.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>

#if USE_FUSED
#include <fused-perceptor/FusedPerceptor.hh>
#endif

// REMOVE #include <interfaces/LeadingVehicleInfo.hh>


class TrafficStateEst : public CStateClient
{
  protected: 

  /*! The constructors are protected since we want a singleton */
  TrafficStateEst(bool waitForStateFill);
  TrafficStateEst(const TrafficStateEst&);
  ~TrafficStateEst();

  public:

  /*! Get the singleton instance */
  static TrafficStateEst* Instance(bool waitForStateFill);

  int init();

  /*! Free the singleton instance */
  static void Destroy();

  /*! You are not allowed to copy a singleton */
  TrafficStateEst& operator=(const TrafficStateEst&);

  /*! Update the current vehicle/actuator state. */
  void updateState();

  /*! Get the vehicle state at the last estimate. */
  VehicleState getVehState();

  /*! Get the actuator state at the last estimate. */
  ActuatorState getActState();

  /*! Get map at last estimate */
  Map* getMap(); 

  void updateMap();

  /*! Gets a local map update thread */
  void getLocalMapUpdate();
  void freezeMap();
  void meltMap();

  /**
   * @brief This function modifies the graph according to the map and the current state problem
   *
   * Obstacles and cars in the map are not treated in the same way depending on the current
   * state problem. The role of this function is to update the graph accordingly.
   */
  int updateGraphStateProblem(PlanGraph *graph, StateProblem_t &problem);  

  /*! List of nodes we affected on the last state problem update. */
  PlanGraphNodeList markedNodes;  

  private : 

  /*! Get singleton instance */
  static TrafficStateEst* pinstance;

  /*! Get the local to global map update. */
  void getLocalGlobalMapUpdate();

  bool loadRNDF(string filename);

  bool frozen;

  /*! The current vehicle state  */
  VehicleState m_currVehState;

  /*! The current actuator state */
  ActuatorState m_currActState;

  /*! The map element talker to receive map updates*/
  CMapElementTalker m_mapElemTalker;

  /*! The map getting updated */
  Map* m_localUpdateMap;

  /*! The map to read  */
  Map* m_localMap;

  /*! The queue of MapElements received since the last map snapshot */
  vector<MapElement> mapElements;

  /*! The local mapper object  */
  Mapper* m_mapper;

  /*! The current map */
  pthread_mutex_t m_localMapUpMutex;

  /*! The current RNDF frame offset mutex */
  pthread_mutex_t m_localRndfOffsetMutex;

  /*! The local to global delta */
  point2 m_localToGlobalOffset;
	double m_localToGlobalYawOffset;

	point2 m_localToRndfOffset;
  
  /*! used for mapper internal
    0 - no copy, 1 - copy requested */
  int m_mapCopyState;

#if USE_FUSED
  public:

  /// Initialize the fused perceptor; starts an internal thread.
  int initFused(const char *spreadDaemon, int skynetKey);

  /// Finalize the fused perceptor; stops the internal thread.
  int finiFused();

  /// Update the graph with data from the fused perceptor
  int updateFusedGraph(PlanGraph *graph, const VehicleState *state);

  /// Lock the fused perceptor and return a pointer
  FusedPerceptor *lockFused();

  /// Unlock the fused perceptor and return a null pointer
  FusedPerceptor *unlockFused();

  private:
  
  /// Main loop for fused perceptor
  static int mainFused(TrafficStateEst *self);

  private:

  // Fused perceptor
  FusedPerceptor *fused;
  
  // Thread control
  pthread_t fusedThread;
  pthread_mutex_t fusedMutex;
#endif
};

#endif /*TRAFFICSTATEEST_HH_*/
