/**********************************************************
 **
 **  UT_ZONECORRIDOR.CC
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 23:28:44 2007
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <s1planner/tempClothoidInterface.hh>
#include <path-planner/PathPlanner.hh>
#include <graph-updater/GraphUpdater.hh>
#include "PlannerUtils.hh"
#include <circle-planner/CirclePlanner.hh>

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  cerr << " starting Main in UT_zoneCorridor " << endl;

  Map* map = new Map();
  map->loadRNDF(args[1]);
  map->prior.delta = point2(3778410.5+2876, 403942.3-4914);
  CmdArgs::sn_key = skynet_findkey(argc, args);
  //CmdArgs::sn_key = 130;
  //int sn_key = (int)CmdArgs::sn_key;

  cout<<"Loaded RNDF"<<endl; 

  // set up some problem to solve
  CSpecs_t cSpec;
  Path_params_t params;

  // Specify some zone to plan in
  params.flag = ZONE;
  params.planFromCurrPos = false;
  //params.zoneId = 14;
  params.zoneId = 6;
  params.spotId = 1;
  params.exitId = 2;
  params.velMin = 0;
  params.velMax = 5;

  // Specify some vehicle state
  VehicleState vehState;
  memset(&vehState, 0, sizeof(vehState));
  vehState.localX = -5;
  vehState.localY = 10;
  vehState.localZ = 0;
  vehState.localYaw = -M_PI/2;

  point2 first, second;
  SpotLabel spotLabel(params.zoneId, params.spotId);
  map->getSpotWaypoints(first, second, spotLabel);
  cout<<"Got map waypoints"<<endl; 

  double finHeading = atan2(second.y-first.y, second.x-first.x);
  cerr << "heading = " << finHeading << endl;
  pose3_t finPose;
  finPose.pos.x = second.x;
  finPose.pos.y = second.y;
  finPose.pos.z = 0;
  finPose.rot = quat_from_rpy(0, 0, finHeading);

  // get the zone region
  point2arr zonePerimeter;
  map->getZonePerimeter(zonePerimeter, params.zoneId);
  zonePerimeter.push_back(zonePerimeter[0]);
  cerr << "Got zone perimeter" << endl; 

  // Generate the appropriate corridor
  Path_t path;

  // Populate the parking spots
  vector<point2arr> parkingSpots; 
  point2arr spotWaypt; 

  spotWaypt.push_back(first);
  spotWaypt.push_back(second);
  parkingSpots.push_back(spotWaypt);
  


  //Pass obstacles 

  vector<MapElement> obstacles; 
  //  ZoneCorridor::generateCorridor(cSpec, zonePerimeter,parkingSpots, obstacles, params, finPose, vehState);

  cerr << "Generated corridor" << endl; 
  
  Err_t error;
  error =  CirclePlanner::GenerateTraj(&cSpec, &path);

  cout << "Generated path of length = " << path.pathLen << endl;

  Utils::init();
  Utils::printPath(&path);
  Utils::displayPath(-5, &path, 3);

  // a quick hack to continue debugging...
  //  int halfPath = 5;
  //  cout << "halfPath = " << halfPath << endl;
  //  for (int i=halfPath; i<path.pathLen; i++) {
  //    path.path[i]->pathDir = GRAPH_PATH_REV;
  //  }
  sleep(5);

  Path_t subPath;
  PlannerUtils::extractSubPath(subPath, path, vehState);
  Utils::displayPath(-5, &subPath, 3, MAP_COLOR_BLUE);
  Utils::printPath(&subPath);
  
}
