#include "Planner.hh"

Planner::Planner(const char *logFileName, char* configFile) 
  : GcModule("Planner", &m_controlStatus, &m_mergedDirective, 100000, 1000)
  , m_isInit(false)
  , m_completed(false)
{
  /*!\param GcInterface variable */
  m_missTraffInterfaceNF = MissionPlannerInterface::generateNorthface(CmdArgs::sn_key, this);

  /*!\param GcInterface variable */
  m_traffAdriveInterfaceSF = AdriveCommand::generateSouthface(CmdArgs::sn_key, this);

  if (CmdArgs::logging) {
    Log::setGenericLogFile(logFileName);
  }

  Log::setVerboseLevel(CmdArgs::verbose_level);
  if (CmdArgs::console) {
    if (!CmdArgs::logging)
      Log::setVerboseLevel(0);
    Console::init();
    Console::addMessage("Debug = %d, Verbose Level = %d, Log = %d", CmdArgs::debug, CmdArgs::verbose_level, CmdArgs::logging);
  }

  m_traffStateEst = TrafficStateEst::Instance(true);

  if (CmdArgs::debug) {
    ostringstream str;
    str << CmdArgs::log_path << "planner-gcmodule";
    this->setLogLevel(9);
    this->addLogfile(str.str().c_str());
  }

  DGCstartMemberFunctionThread(m_traffStateEst, &TrafficStateEst::getLocalMapUpdate);
  
  // path is already initialized
  // initialize traj
  m_traj = CTraj(3);
  // initialize graph
  GraphUpdater::init(&m_graph, m_traffStateEst->getMap());
  // initialize logic
  LogicPlanner::init();
  // Initialize the path planner
  PathPlanner::init();
  // Initialize the velocity planner
  VelPlanner::init();

  // Initialize traj talker
  m_trajTalker = new CTrajTalker(MODtrafficplanner, CmdArgs::sn_key);

}

Planner::~Planner()
{
  MissionPlannerInterface::releaseNorthface(m_missTraffInterfaceNF);
  TrafficStateEst::Destroy();
  Console::destroy();
  delete m_trajTalker;
  GraphUpdater::destroy(m_graph);
  LogicPlanner::destroy();
  PathPlanner::destroy();
  VelPlanner::destroy();
}

void Planner::arbitrate(ControlStatus* cs, MergedDirective* md) {
  // refresh console (to avoid spurious map error printouts)
  Console::refresh();

  // Display time
  static uint64_t old_time = getTime();
  uint64_t new_time = getTime();
  Log::getStream(4) << "Time consumed = " << (new_time - old_time)/(double)1000000 << " seconds" << endl;
  old_time = getTime();

  PlannerControlStatus *controlStatus = dynamic_cast<PlannerControlStatus *>(cs);
  PlannerMergedDirective *mergedDirective = dynamic_cast<PlannerMergedDirective *>(md);

  SegGoalsStatus segGoalsStatus; 

  if (!m_isInit) {
    segGoalsStatus.goalID = 0;
    segGoalsStatus.status = SegGoalsStatus::COMPLETED;
    
    m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
    sleep(5);
    
    while (!m_missTraffInterfaceNF->haveNewDirective()) {    
      Log::getStream(1) << "TRFMGR: Sending Init Goal " << segGoalsStatus.goalID << endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
      sleep(5);
    }
    m_isInit = true;
  }

  m_traffStateEst->updateMap();
  m_traffStateEst->updateVehState();
  VehicleState vs = m_traffStateEst->getVehState();
  Console::updateState(vs);

  if (PlannerControlStatus::EXECUTING == controlStatus->status) {
    Log::getStream(1) << endl << endl;
    Log::getStream(1)<<"TRFMGR:  Executing Goal ID :"<<controlStatus->ID<<endl; 
  }
  
  /* Check on the status of the last command acted on by control */
  if (controlStatus->status == PlannerControlStatus::COMPLETED ||
      controlStatus->status == PlannerControlStatus::FAILED) {
    
    if (controlStatus->status == PlannerControlStatus::COMPLETED) {  
      segGoalsStatus.status = SegGoalsStatus::COMPLETED;
      segGoalsStatus.goalID = controlStatus->ID;

      m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
      Log::getStream(1)<<"TRFMGR: GOAL ID COMPLETED "<<segGoalsStatus.goalID<<endl;

      /* We dont want to pop if we are completing a previous goal */
      if (m_accSegGoalsQ.size() > 0 && m_accSegGoalsQ.front().goalID == segGoalsStatus.goalID){ 
        m_accSegGoalsQ.pop_front();
      } else { Log::getStream(1)<<"TRFMGR: waiting for more goals: m_accSegGoals.size()==0 "<<endl;}
    } else if (controlStatus->status == PlannerControlStatus::FAILED)  {
      /* Flush out the NF queue and send failure status */
      while (m_missTraffInterfaceNF->haveNewDirective()) {
        SegGoals newDirective;
        m_missTraffInterfaceNF->getNewDirective( &newDirective );
        segGoalsStatus.goalID = newDirective.goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        Log::getStream(1)<<"TRFMGR: GOAL ID, FAILED (but not sending) "<<segGoalsStatus.goalID<<endl;
        /* Dont send response yet, wait until beginning of planning loop */
      }
      
      /* Flush out the accumulating segGoals control queue and send failure status */
      while (m_accSegGoalsQ.size() > 0) {
        segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        Log::getStream(1)<<"TRFMGR: GOAL ID, FAILED (not sending)"<<segGoalsStatus.goalID<<endl;
        /* Dont send response yet, wait until beginning of planning loop */
        m_accSegGoalsQ.pop_front();
      }

      segGoalsStatus.status = SegGoalsStatus::FAILED;
      segGoalsStatus.goalID = controlStatus->ID;
      Log::getStream(1)<<"TRFMGR: GOAL ID, FAILED (sending) "<<segGoalsStatus.goalID<<endl;
      m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
    }
    Log::getStream(1) << endl << endl;
  }

  /* Get all the new directives and put it in the queue */
  while (m_missTraffInterfaceNF->haveNewDirective()) {

    SegGoals newDirective;
    m_missTraffInterfaceNF->getNewDirective(&newDirective);
    
    printDirective(&newDirective);

    if ((m_accSegGoalsQ.size()>0) && (newDirective.goalID <= m_accSegGoalsQ.back().goalID)) {
      continue;
    }

    m_accSegGoalsQ.push_back(newDirective);

    if (SegGoals::PAUSE == newDirective.segment_type) {
      SegGoalsStatus segGoalsStatus;
      
      /* First flush the queue where the seg goals are accumulating */
      while (SegGoals::PAUSE != m_accSegGoalsQ.front().segment_type) {
        segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        controlStatus->status = PlannerControlStatus::FAILED;
        Log::getStream(1)<<"TRFMGR: GOAL ID PAUSE,FAILED "<<segGoalsStatus.goalID<<endl;
        m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
        m_accSegGoalsQ.pop_front();
      }
      break; 
    }
  }

  if (m_accSegGoalsQ.size() > 0 && PlannerControlStatus::EXECUTING != controlStatus->status) {
    SegGoals newGoal =  m_accSegGoalsQ.front();

    if (SegGoals::PAUSE == newGoal.segment_type) {
      mergedDirective->segType = newGoal.segment_type;
      mergedDirective->id = newGoal.goalID;
      m_currSegGoals = newGoal; 
    } else if (SegGoals::UNKNOWN == newGoal.segment_type) {
      segGoalsStatus.goalID = newGoal.goalID;
      mergedDirective->segType = newGoal.segment_type;
      segGoalsStatus.status = SegGoalsStatus::FAILED;
      Log::getStream(1)<<"TRFGMR: GOAL ID,UNKNOWN "<<segGoalsStatus.goalID<<endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
         
    } else if (SegGoals::END_OF_MISSION == newGoal.segment_type) {
      mergedDirective->id = newGoal.goalID; 
      mergedDirective->segType = newGoal.segment_type; 
      m_currSegGoals = newGoal;

    } else {

      mergedDirective->id = newGoal.goalID; 
      mergedDirective->segType = newGoal.segment_type;
      m_currSegGoals = newGoal;
      
    }
  } else {
    Log::getStream(1)<<"TRFMGR: Waiting for Mission Planner goal "<<endl;
  }
}

void Planner::control(ControlStatus* cs, MergedDirective* md)
{
  static Err_t error = GU_OK | LP_OK | PP_OK | VP_OK;

  PlannerControlStatus* controlStatus =
    dynamic_cast<PlannerControlStatus *>(cs);
  
  PlannerMergedDirective* mergedDirective =
    dynamic_cast<PlannerMergedDirective *>(md);

  // initialize vehState
  VehicleState vehState = m_traffStateEst->getVehState();

  if (mergedDirective->segType==SegGoals::PAUSE || mergedDirective->segType==SegGoals::END_OF_MISSION) {
    m_completed = PlannerUtils::isStopped(vehState);
  } else {
    PointLabel currGoalEndpoint = PointLabel(m_currSegGoals.exitSegmentID,
                                             m_currSegGoals.exitLaneID,
                                             m_currSegGoals.exitWaypointID);
    m_completed = isGoalComplete(currGoalEndpoint);
  }  

  if (m_completed) {
    controlStatus->ID = m_currSegGoals.goalID;
    controlStatus->status = PlannerControlStatus::COMPLETED;
    m_currSegGoals.segment_type = SegGoals::UNKNOWN;
    return;
  }

  // initialize cost
  Cost_t cost;

  // initialize vector of state problems
  vector<StateProblem_t> state_problems;

  // temporary solution...
  ActuatorState actState;
  memset(&actState, 0, sizeof(actState));

  // Execute logic planner
  Logic_params_t logicParams;
  logicParams.segment_type = mergedDirective->segType;
  logicParams.seg_goal = m_currSegGoals;
  error = LogicPlanner::planLogic(state_problems, m_graph, error, vehState, m_traffStateEst->getMap(), logicParams);
  Console::updateFSMState(state_problems[0].state);
  Console::updateFSMFlag(state_problems[0].flag);
  error = 0;

  // Update graph to account for the current car position
  error |= GraphUpdater::genVehicleSubGraph(m_graph, vehState, actState);

  // Update graph according to the new state problem
  error |= GraphUpdater::updateGraphStateProblem(m_graph, state_problems[0], m_traffStateEst->getMap());

  // set the final pose based on the goal position
  pose3_t finPose;
  point2 final;

  if (mergedDirective->segType==SegGoals::PAUSE || mergedDirective->segType==SegGoals::END_OF_MISSION) {
    final.set(vehState.localX, vehState.localY);
  } else {
    PointLabel currGoalEndpoint = PointLabel(m_currSegGoals.exitSegmentID,
                                             m_currSegGoals.exitLaneID,
                                             m_currSegGoals.exitWaypointID);
    m_traffStateEst->getMap()->getWaypoint(final, currGoalEndpoint);
  }
  
  double heading;
  m_traffStateEst->getMap()->getHeading(heading, final);

  finPose.pos.x = final.x;
  finPose.pos.y = final.y;
  finPose.pos.z = 0;
  finPose.rot = quat_from_rpy(0,0, heading);
  
  // plan the path
  Path_params_t pathParams;
  error |= PathPlanner::planPath(&m_path, cost, m_graph, finPose, pathParams);
  
  // vel planner flags (to be implemented)     
  Vel_params_t velParams;
  error |= VelPlanner::planVel(&m_traj, &m_path, state_problems[0], vehState, velParams);

  // print graph and path to default mapviewer channel
  GraphUpdater::display(-2, m_graph, vehState);
  PathPlanner::display(-2, &m_path);
  Console::updateTrajectory(&m_traj);
  
  // send traj
  int trajSocket = m_trajTalker->m_skynet.get_send_sock(SNtraj);
  m_trajTalker->SendTraj(trajSocket, &m_traj);
}

bool Planner::isGoalComplete(PointLabel exitWayptLabel)
{
  double completeDist = 3;
  bool completed = false;
  double angle, dotProd, AliceHeading, absDist, headingDiff; 

  Log::getStream(1)<<"TRFMGR::isGoalComplete "<<endl;
  
  point2 exitWaypt, currPos;  
  m_traffStateEst->getMap()->getWaypoint(exitWaypt, exitWayptLabel);
  currPos = AliceStateHelper::getPositionFrontBumper(m_traffStateEst->getVehState());
    
  m_traffStateEst->getMap()->getHeading(angle, exitWayptLabel);
    
  Log::getStream(1)<<"TRFMGR: exit waypoint label "<<exitWayptLabel<<endl;
  Log::getStream(1)<<"TRFMGR: currPos "<<currPos<<endl;
  Log::getStream(1)<<"TRFMGR: exit waypoint "<<exitWaypt<<endl;

  dotProd = (-exitWaypt.x+currPos.x)*cos(angle) + (-exitWaypt.y+currPos.y)*sin(angle);

  Log::getStream(1) << "TRFMGR: dot product  = " << dotProd << endl;
  
  AliceHeading = AliceStateHelper::getHeading(m_traffStateEst->getVehState());
  absDist = exitWaypt.dist(currPos);

  double anglePiMinusPi;
  PlannerUtils::addAngles(anglePiMinusPi,angle,-AliceHeading);
  headingDiff = anglePiMinusPi;

  if((dotProd>-completeDist) && (fabs(headingDiff)<M_PI/6) && (absDist<10)) {
    /* when we get within 1 m of the hyperplane defined by the exit pt, goal complete */
    completed = true;
  } else {
    completed = false;
  }

  return completed;
}

uint64_t Planner::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

void Planner::printDirective(SegGoals* newDirective) 
{
  Log::getStream(1)<<"TRFMGR: New dir recvd, ID "<<newDirective->goalID<<",type "<<newDirective->segment_type;
  Log::getStream(1)<<", intersection type "<<newDirective->intersection_type;
  Log::getStream(1)<<", illegalPassingAllowed "<<newDirective->illegalPassingAllowed; 
  Log::getStream(1)<<", exit waypt ";
  Log::getStream(1)<<newDirective->exitSegmentID<<"."<<newDirective->exitLaneID<<"."<<newDirective->exitWaypointID<<endl;
}
