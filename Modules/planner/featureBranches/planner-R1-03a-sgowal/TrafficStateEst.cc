/*!
 * \file TrafficStateEst.cc
 * \brief Some legacy code from the tplanner. Maintains the local map and the vehicle state. Needs to be incorporated into the planner.
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include "TrafficStateEst.hh"
#include <interfaces/sn_types.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Console.hh>

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(bool waitForStateFill) 
  : CSkynetContainer(MODtrafficplanner, CmdArgs::sn_key)
  , CStateClient(waitForStateFill)
{
  DGCcreateMutex(&m_localMapUpMutex);
	DGCcreateMutex(&m_localRndfOffsetMutex);

  if (CmdArgs::use_RNDF){
    Log::getStream(1)<<"I am using the RNDF"<<endl;
    if (!CmdArgs::RNDF_file.empty()){
      Log::getStream(1)<<"RNDF is not empty "<<endl;
			

			//Check if the internal mapper is being used
			//if (!CmdArgs::mapper_use_internal){
        m_mapElemTalker.initSendMapElement(CmdArgs::sn_key);
        m_mapElemTalker.initRecvMapElement(CmdArgs::sn_key, 1);
			//}
    
    } else {
      Log::getStream(1)<<"TFEST: RNDFfilename is empty"<<endl;
    }
  } else {
    Log::getStream(1)<<"TFEST: not using RNDF"<<endl;
  }

	UpdateState();
	if (CmdArgs::mapper_use_internal){
		m_mapper = new Mapper();
		assert(m_mapper);
		m_mapper->initComm(CmdArgs::sn_key);
    m_mapper->enableGroundstrikeFiltering(CmdArgs::mapper_enable_groundstrike_filtering);	
    m_mapper->init(CmdArgs::RNDF_file);
    m_mapper->setLocalFrameOffset(m_state);
		m_mapper->disableLineFusion(CmdArgs::mapper_disable_line_fusion);
		m_mapper->enableLineFusionCompat(CmdArgs::mapper_line_fusion_compat);
		m_mapper->disableObsFusion(CmdArgs::mapper_disable_obs_fusion);
		m_mapper->decayAgeThresh = CmdArgs::mapper_decay_thresh;
    m_mapper->plannerOutputDisabled = true;
    m_mapper->debugSubGroup = CmdArgs::mapper_debug_subgroup;
	}



  m_localMap = new Map(!CmdArgs::mapper_disable_line_fusion);
	m_localUpdateMap = new Map(!CmdArgs::mapper_disable_line_fusion);
	
	loadRNDF(CmdArgs::RNDF_file);
	
	UpdateState();
	m_localMap->setLocalToGlobalOffset(m_state);
	DGClockMutex(&m_localMapUpMutex);
	m_localUpdateMap->setLocalToGlobalOffset(m_state);
	DGCunlockMutex(&m_localMapUpMutex);
}

TrafficStateEst::~TrafficStateEst() 
{
  delete m_localMap;
	delete m_localUpdateMap;	
	if (CmdArgs::mapper_use_internal){
		delete m_mapper;
	}
		
  DGCdeleteMutex(&m_localMapUpMutex);
}


TrafficStateEst* TrafficStateEst::Instance(bool waitForStateFill)
{
    if (pinstance == 0)
        pinstance = new TrafficStateEst(waitForStateFill);
    return pinstance;
}

void TrafficStateEst::Destroy()
{
    delete pinstance;
    pinstance = 0;
}

void TrafficStateEst::updateVehState() 
{
  UpdateState();
  m_currVehState = m_state; 

  if (CmdArgs::use_rndf_frame){
    DGClockMutex(&m_localRndfOffsetMutex);
    m_currVehState.localX += m_localToRndfOffset.x;
    m_currVehState.localY += m_localToRndfOffset.y;
    DGCunlockMutex(&m_localRndfOffsetMutex);


  }
}

VehicleState TrafficStateEst::getVehState() 
{
  return m_currVehState;
}

void TrafficStateEst::getLocalMapUpdate()
{
  MapElement recvEl;
  int bytesRecv;
  Log::getStream(1) << "In local map update ..." <<endl; 

	int retval;
	bool useRndfFrame = CmdArgs::use_rndf_frame;

	if (CmdArgs::mapper_use_internal){

		while (true) {
      if (useRndfFrame){
        DGClockMutex(&m_localRndfOffsetMutex);
        // m_localToRndfOffset = listeneroutput;
        DGCunlockMutex(&m_localRndfOffsetMutex);
        
      }
			
			retval = m_mapper->mainLoop();
			if (retval < 0){
				Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received error from mapper:updateMap = " << endl;
      }else if (retval >0){ 
			
				DGClockMutex(&m_localMapUpMutex);
				*m_localUpdateMap = m_mapper->map;
				DGCunlockMutex(&m_localMapUpMutex);
			}
      


		}
	}else{
	  while (true) {
      if (useRndfFrame){
        DGClockMutex(&m_localRndfOffsetMutex);
        // m_localToRndfOffset = listeneroutput;
        DGCunlockMutex(&m_localRndfOffsetMutex);
        
      }

	    bytesRecv = m_mapElemTalker.recvMapElementBlock(&recvEl,1);
	    

			
			if (bytesRecv>0)
			  {
			    if(recvEl.id.dat[0] == 10) {
            if(!(recvEl.type == ELEMENT_CLEAR))
              Console::increaseObstSize();
            else
              Console::increaseClearSize();
          }
			    DGClockMutex(&m_localMapUpMutex);
			    m_localUpdateMap->addEl(recvEl);
			    DGCunlockMutex(&m_localMapUpMutex);
			  }else {
			    Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
					      << bytesRecv << endl;
			}
		}
	}
}

void TrafficStateEst::updateMap() {

  DGClockMutex(&m_localMapUpMutex);
  *m_localMap = *m_localUpdateMap;
  DGCunlockMutex(&m_localMapUpMutex);
  m_localToGlobalOffset = m_localMap->getLocalToGlobalOffset();
	m_localToGlobalYawOffset = m_localMap->getLocalToGlobalYawOffset();

}

Map* TrafficStateEst::getMap() {
  return m_localMap;
}


bool TrafficStateEst::loadRNDF(string filename) {

  m_localUpdateMap->loadRNDF(filename);
  return m_localMap->loadRNDF(filename);
}


