/**********************************************************
 **
 **  ZONECORRIDOR.CC
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 17:23:25 2007
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <skynettalker/SkynetTalker.hh>
//#include <temp-planner-interfaces/PlannerInterfaces.h>
#include "temp-planner-interfaces/Log.hh"

#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0:  0)

Err_t ZoneCorridor::generateCorridor(CSpecs_t& cSpecs, point2arr zonePerimeter, Path_params_t params, pose3_t finPose, VehicleState vehState)
{

  unsigned long long time1, time2;

  DGCgettime(time1);

  // set up cspecs problem
  // starting state
  double startingState[4];
  startingState[0] = AliceStateHelper::getPositionRearAxle(vehState).x;
  startingState[1] = AliceStateHelper::getPositionRearAxle(vehState).y;
  startingState[2] = AliceStateHelper::getVelocityMag(vehState);
  startingState[3] = AliceStateHelper::getHeading(vehState);
  cSpecs.setStartingState(startingState);
  Log::getStream(9) << "ZoneCorridor:CSpecs-setStartingState execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  DGCgettime(time1);
  // starting controls
  double startingControls[2];
  startingControls[0] = AliceStateHelper::getAccelerationMag(vehState);
  startingControls[1] = 0;
  cSpecs.setStartingControls(startingControls);
  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:CSpecs-setStartingControls execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  DGCgettime(time1);
  // final state
  double finalState[4];
  double roll, pitch, yaw;
  quat_to_rpy(finPose.rot, &roll, &pitch, &yaw);
  finalState[0] = finPose.pos.x - DIST_REAR_AXLE_TO_FRONT*cos(yaw);
  finalState[1] = finPose.pos.y - DIST_REAR_AXLE_TO_FRONT*sin(yaw);
  finalState[2] = 0;
  finalState[3] = yaw;
  cSpecs.setFinalState(finalState);

  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:CSpecs-setFinalState execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  DGCgettime(time1);
  // final controls
  double finalControls[2];
  finalControls[0] = 0;
  finalControls[1] = 0;
  cSpecs.setFinalControls(finalControls);
  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:CSpecs-setFinalControls execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // other parameters
  // TODO: add things like setting the max vel etc.
  //  cSpecs.icfc_prob.velMin = params.velMin;
  //  cSpecs.icfc_prob.velMax = params.velMax;
 
  DGCgettime(time1);
  // set the corridor
  cSpecs.setBoundingPolygon(zonePerimeter);
  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:CSpecs-setBoundingPolygon execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  DGCgettime(time1);
  // define the cost map
  BitmapParams bmParams;
  PolygonParams polygonParams;
  bmParams.resX = 0.1;
  bmParams.resY = 0.1;
  bmParams.width =800;
  bmParams.height = 800;
  bmParams.baseVal = 100 ;
  bmParams.outOfBounds = 200;
  polygonParams.centerlaneVal = 1;
  polygonParams.obsCost = 1000;
  point2 initPos(startingState[0], startingState[1]);
  getBitmapParams(bmParams, polygonParams, initPos, zonePerimeter);
  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:getBitmapParams execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  DGCgettime(time1);
  cSpecs.setCostMap(bmParams);
  DGCgettime(time2);
  Log::getStream(9) << "ZoneCorridor:getBitmapParams execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // some debug info ...
  if (1) {
    // print corridor
    //print(zonePerimeter, cSpecs);

    DGCgettime(time1);
    // display the corridor
    //display(CmdArgs::sn_key, 10, zonePerimeter, cSpecs);
    DGCgettime(time2);
    Log::getStream(9) << "ZoneCorridor:display execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  }

  return 0;
}

void ZoneCorridor::convertZoneToPolygon(vector<Polygon>& polygons, point2arr& zonePerimeter, float val)
{
  val = sqrt(val);

  vector<float> cost;
  point2arr_uncertain vertices(zonePerimeter);
  for (unsigned int i=0; i<zonePerimeter.size(); i++) {
    cost.push_back(val);
  }
  
  Polygon polygon;
  polygon.setVertices(vertices, cost);
  polygon.setFillFunc(FILL_SQUARE);
  polygon.setCombFunc(COMB_REPLACE);

  polygons.push_back(polygon);
}

void ZoneCorridor::getBitmapParams(BitmapParams& bmparams, PolygonParams &polygonParams,
                     point2 alicePos, point2arr zonePerimeter)
{
  bmparams.centerX = alicePos.x;
  bmparams.centerY = alicePos.y;

  bmparams.polygons.clear();
  float perimCost = 500;
  float pSpaceCost = 100;
  float minVal = 1;

  assignPerimeterCost(bmparams.polygons,zonePerimeter, perimCost, minVal);
  assignParkingSpaceCost(bmparams.polygons,pSpaceCost, minVal);

}


void ZoneCorridor::display(int sn_key, int sendSubgroup, point2arr zonePerimeter, CSpecs_t cSpecs)
{
  CMapElementTalker meTalker;
  meTalker.initSendMapElement(sn_key);
  int counter=12000;
  point2 point;
  MapId mapId;
  MapElement me;
  
  mapId = counter;

  // display the corridor
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_BLUE, 100);
  me.setGeometry(zonePerimeter);
  meTalker.sendMapElement(&me,sendSubgroup);
  
  // print the initial and final cond's
  vector<double> startingState = cSpecs.getStartingState();
  vector<point2> points;
  point.set(startingState[0], startingState[1]);
  points.push_back(point);
  point.set(startingState[0]+cos(startingState[3]), startingState[1]+sin(startingState[3]));
  points.push_back(point);
  mapId = 12001;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_YELLOW, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

  // print the initial and final cond's
  vector<double> finalState = cSpecs.getFinalState();
  point.set(finalState[0], finalState[1]);
  points.push_back(point);
  point.set(finalState[0]+cos(finalState[3]), finalState[1]+sin(finalState[3]));
  points.push_back(point);
  mapId = 12002;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_RED, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

}

void ZoneCorridor::print(point2arr zonePerimeter, CSpecs_t cSpecs)
{
  vector<double> startingState = cSpecs.getStartingState();
  vector<double> finalState = cSpecs.getFinalState();
  vector<double> startingControls = cSpecs.getStartingControls();
  vector<double> finalControls = cSpecs.getFinalControls();
  Console::addMessage("Init state = (%6.2f,%6.2f,%6.2f,%6.2f)", startingState[0], startingState[1], startingState[2], startingState[3]);
  Console::addMessage("Init control = (%6.2f,%6.2f)", startingControls[0], startingControls[1]);
  Console::addMessage("Fin state = (%6.2f,%6.2f,%6.2f,%6.2f)", finalState[0], finalState[1], finalState[2], finalState[3]);
  Console::addMessage("Fin control = (%6.2f,%6.2f)", finalControls[0], finalControls[1]);
}


void ZoneCorridor::assignPerimeterCost(vector<Polygon>& polygons, point2arr zonePerimeter, 
				       float cost, float minVal) 
{  
  /* Add the first point again to the end of zonePerimeter array */
  zonePerimeter.push_back(zonePerimeter[0]);
  assignCostToLine(polygons, zonePerimeter, cost, minVal);
}

void ZoneCorridor::assignParkingSpaceCost(vector<Polygon>& polygons,float cost, float minVal) 
{

}

void ZoneCorridor::assignCostToLine(vector<Polygon>& polygons, point2arr line,float cost, float bval)
{
  const double width = VEHICLE_WIDTH*2;

  Polygon p;
  const cost_t sideVal = 2; // go down to 2*sigma (cost*ext(-0.5*2*2)=cost*0.1353)
  
  // use a gaussian-like function
  p.setFillFunc(FILL_EXP2);
  p.setA(-0.5);
  p.setB(cost);
  p.setC(bval);
  p.setCombFunc(COMB_MAX);

  // for ease of notation

  for (unsigned int i = 0; i < line.size()-1; i++)
  {
      // calculate bisectors for vertices v1 and v2. For a generic vertex k, the
      // bisector is the orthogonal to v(k+1) - v(k-1) through the vertex v(k)
      // except for the first and the last, which are treated specially
      point2 bisec[2];
      point2 distVec;

      if (i > 0) {
          distVec = line[i + 1] - line[i - 1];
      } else {
          distVec = line[i + 1] - line[i];
      }
      bisec[0] = point2(-distVec.y, distVec.x); // orthog to distVec
      bisec[0].normalize(); // FIXME: what if the points are so close that the distance is ~zero?

      if (i + 2 < line.size()) {
          distVec = line[i + 2] - line[i];
      } else {
          distVec = line[i + 1] - line[i];
      }
      bisec[1] = point2(-distVec.y, distVec.x); // orthog to distVec
      bisec[1].normalize(); // FIXME: what if the points are so close that the distance is ~zero?

      point2arr vert(4);
      vector<cost_t> val(4);
      // FIXME: there is the possibility that the top egde intersects the
      // bottom edge, for very sharp turns, creating something that is not a polygon
      // (a star? a butterfly? it's just that vertices are not in cw nor in ccw order)
      // The resulting cost map will be weird, but still acceptable, I think.

      // left side
      vert[0] = line[i + 1] + bisec[1]*width;
      val[0] = -sideVal;
      vert[1] = line[i]     + bisec[0]*width;
      val[1] = -sideVal;

      // right side
      vert[2] = line[i]     - bisec[0]*width;
      val[2] = sideVal;
      vert[3] = line[i + 1] - bisec[1]*width;
      val[3] = sideVal;

      p.setVertices(vert, val);
      polygons.push_back(p);
  }
}
