/**********************************************************
 **
 **  UT_ZONECORRIDOR.CC
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 23:28:44 2007
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <s1planner/tempClothoidInterface.hh>

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);
  map->prior.delta = point2(3778410.5, 403942.3);
  CmdArgs::sn_key = skynet_findkey(argc, args);
  int sn_key = (int)CmdArgs::sn_key;

  //OCPtSpecs* ocpSpecs = new OCPtSpecs(sn_key, 0, false, true, false);
  //  OCPtSpecs(sn_key, verbose, useAstate, useThreads);

  // Specify some zone to plan in
  int zoneLabel = 14;

  // Specify some init pose (in the zone)
  pose3_t initPose, finPose;
  double yaw = 0;
  initPose.pos.x = 2876;
  initPose.pos.y = -4914;
  initPose.pos.z = 0;
  initPose.rot = quat_from_rpy(0, 0, yaw);

  // Specify some vehicle state
  VehicleState vehState;
  memset(&vehState, 0, sizeof(vehState));
  vehState.localX = initPose.pos.x;
  vehState.localY = initPose.pos.y;
  vehState.localZ = initPose.pos.z;
  vehState.localYaw = yaw;

  // get the final pose from some parking spot in the zone
  SpotLabel spotLabel(14,1);
  point2 first, second;
  map->getSpotWaypoints(first, second, spotLabel);
  double finHeading = atan2(second.y-first.y, second.x-first.x);
  cout << "heading = " << finHeading << endl;
  //  finPose.pos.x = second.x;
  //  finPose.pos.y = second.y;
  finPose.pos.x = initPose.pos.x + 15;
  finPose.pos.y = initPose.pos.y + 6;
  finPose.pos.z = 0;
  //  finPose.rot = quat_from_rpy(0, 0, finHeading);
  finPose.rot = quat_from_rpy(0, 0, M_PI/4);
  double finVel = 0;
  double finAcc = 0;

  //specify vel limits for zone
  double velMin = 0;
  double velMax = 5;
  

  // get the zone region
  point2arr zonePerimeter;
  map->getZonePerimeter(zonePerimeter, zoneLabel);
  zonePerimeter.push_back(zonePerimeter.arr[0]);
  /*
  point2 point;
  point.set(initPose.pos.x-15,initPose.pos.y+7.5);
  zonePerimeter.push_back(point);
  point.set(initPose.pos.x+15,initPose.pos.y+7.5);
  zonePerimeter.push_back(point);
  point.set(initPose.pos.x+15,initPose.pos.y-2.5);
  zonePerimeter.push_back(point);
  point.set(initPose.pos.x+0,initPose.pos.y-20);
  zonePerimeter.push_back(point);
  point.set(initPose.pos.x-15,initPose.pos.y-2.5);
  zonePerimeter.push_back(point);
  point.set(initPose.pos.x-15,initPose.pos.y+7.5);
  zonePerimeter.push_back(point);
  */
  cout << "zone = " << endl << zonePerimeter << endl;

  // Generate the appropriate corridor
  Path_t* path = NULL;
  OCPparams ocpParams = OCPparams();
  CPolytope* polyCorridor;
  BitmapParams bmparams;

  ZoneCorridor::generateCorridor(ocpParams, &polyCorridor, bmparams, zonePerimeter, zoneLabel, initPose, finPose, velMin, velMax, vehState, finVel, finAcc);  
  
  double runtime = 0.2;
  GenerateTrajOneShot(CmdArgs::sn_key, true, ocpParams, polyCorridor, bmparams, path, runtime);

  delete path;
  delete polyCorridor;
}
