/*!
 * \file PlannerMain.cc
 * \brief Main planner execution entry point
 *
 * \author Noel duToit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */
   
#include <getopt.h>
#include <iostream>
#include "Planner.hh"
#include "dgcutils/DGCutils.hh"

#include "temp-planner-interfaces/planner_cmdline.h"
#include "temp-planner-interfaces/Console.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "PlannerArgs.hh"

using namespace std;                 
  
/**
 * @brief Entry point of the planner
 *
 * This function populates the CmdArgs static class
 * and starts the planner
 */
int main(int argc, char **argv)              
{  
  struct planner_options options;      

  /* Parse command-line arguments */
  if (planner_cmdline(argc, argv, &options) != 0)
    exit (1);

  // Suck out the options and place them in CmdArgs.  Should be
  // deprecated.
  plannerArgs(argc, argv, &options);

  /* Display that console is not enabled */
  if (!CmdArgs::console){
    cout << "No display" << endl;
    cout << "Constructing skynet with KEY = " << CmdArgs::sn_key << endl;
    cout << "debug = " << CmdArgs::debug << endl;
    cout << "verbose level = " << CmdArgs::verbose_level << endl;
  }

  /* Initializes Planner and start the gcmodule */
  Planner* planner = new Planner(&options); 
  sleep(1);

#if 0
  // I have block this out, since it appears to start an additional
  // unnecessary thread.  We will run the main process thread instead.

  planner->Start();

  /* Refreshing the console */
  unsigned int cnt = 0;
  while (true) {
    sleep(5);
  }
#endif

  /// Startup the portHandler thread
  planner->GcSimplePortHandlerThread::ReStart();

  // Run the arbitration and control cycle
  while (!planner->IsStopped()) // TODO: this needs to pick a quit flag from somewhere
  {
    planner->arbitrate(&planner->m_controlStatus, &planner->m_mergedDirective);
    planner->control(&planner->m_controlStatus, &planner->m_mergedDirective);
  }
  
  return 0;
}

