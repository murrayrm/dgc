/**********************************************************
 **
 **  ZONECORRIDOR.HH
 **
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 15:41:29 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef ZONECORRIDOR_HH
#define ZONECORRIDOR_HH

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <frames/point2.hh>
#include <map/Map.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
#include <frames/pose2.h>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Utils.hh>

enum CostSide { LEFT,RIGHT,LEFT_AND_RIGHT };

struct PolygonParams
{
  float centerlaneVal;
  float obsCost;
  
  PolygonParams()
  {
    centerlaneVal = 0;
    obsCost = 10000;
  }
};


//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class ZoneCorridor
{  
public:
  /*! Sets up the CSpecs problem for the zone planners based on the map, our position and goal, and the state problem*/
  static Err_t setupCSpecs(CSpecs_t& cSpecs, Map* map, VehicleState vehState, ActuatorState actState, SegGoals currSegGoal, StateProblem_t stateProblem, Path_params_t params, pose2f_t finPose);

  /*! Resets final conditions in cspecs with finPose */
  static void resetFinalConditions(CSpecs& cSpecs, pose2f_t finPose, PlanGraphPathDirection direction);

  /*! Sets the direction of the path */
  static void setPathDirection(CSpecs& cSpecs, PlanGraphPathDirection direction);
  
  /*! Determine a generic perimeter */
  static void generateGenericPerimeter(point2arr& perim, Map* map, point2 currPos, SegGoals currSegGoals);

private:
  
  /*! Paint the cost for a zone region */
  static void paintCostZone(CSpecs_t& cSpecs, BitmapParams& bmparams, point2arr& corridor, PolygonParams &polygonParams, 
			    Map* map, point2 alicePos, int zoneId);

  /*! Paint the cost for the road region */
  static void paintCostRoadRegion(CSpecs_t& cSpecs, BitmapParams& bmparams,point2arr& corridor, PolygonParams &polygonParams, 
				  Map* map, point2 initPos, SegGoals currSegGoal);

  /*! Paint the cost for obstacles and the zone perimeter */
  static void paintCost(BitmapParams& bmparams, PolygonParams &polygonParams, point2 alicePos, point2arr zonePerimeter, vector<MapElement> obstacles);

  /*! display some debugging info */
  static  void display(int sn_key, int sendSubgroup, point2arr zonePerimeter, CSpecs_t cSpecs);

  /*! print some debugging info */
  static void print(point2arr zonePerimeter, CSpecs_t cSpecs);


  /*! Assigns a cost to a road region with lanes */
  static void assignCostToRoad();

  /*! Define the bitmap parameters associated to the zone perimeter */
  static void assignPerimeterCost(vector<Polygon>& polygon, point2arr zonePerimeter, 
				  float xCoeff, float cost, float minVal, float width, CostSide side);

  /*! Define the bitmap parameters associated to the lane lines for a road region or intersection */
  static void assignLaneCost(vector<Polygon>& polygons, point2arr& leftbound, point2arr &rightbound,
			     float xCoeff, float cost, float minVal, float width, CostSide side);

  /*! Define the bitmap parameters associated to parking spots. */
  static void assignParkingSpaceCost(vector<Polygon>& polygon, vector<point2arr> parkingSpots, 
				     float xCoeff, float cost, float minVal, float width, CostSide side);

  /*! Assigns a cost to an obstacle. */
  static void assignObstacleCost(vector<Polygon>& polygons, MapElement& el,
			  float xCoeff, float cost, float bval, float width, 
			  bitmap::FillFunc fillFunc, bitmap::CombineFunc combFunc);

  /*! Assigns a cost to a line. */
  static void assignCostToLine(vector<Polygon>& polygons, point2arr line,
			       float xCoeff, float cost, float bval, float width, CostSide side, 
			       bitmap::FillFunc fillFunc, bitmap::CombineFunc combFunc);

  /*! Populates cspecs with the point2arr_uncertain representation of obstacles. */
  static void populatePolygonalObstacles(CSpecs& cspecs, vector<MapElement> obstacles);

  static int determineZoneAction(PointLabel exitLabel, int zoneId, Map* map);

  /*! Assigns a cost to road/lane */
  static void convertLaneToPolygon(vector<Polygon>& polygons, point2arr& leftbound, 
				   point2arr &rightbound, float bval, float cval);

private:
  /// What did we do the previous cycle when in the zone
  static int m_prevZoneAction;

};
#endif
