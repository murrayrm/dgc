/*!
 * \file PlannerUtils.cc
 * \brief Source code for some utility functions for the planner module
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include "PlannerUtils.hh"
#include <alice/AliceConstants.h>
#include <math.h>
#include <temp-planner-interfaces/Log.hh>

/**
 * @brief Adds two angles and adjust the sum according to the PI/-PI convention
 */
int PlannerUtils::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<=-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}

/**
 * @brief Returns an -PI, PI bounded angle\
 */
double PlannerUtils::getAngleInRange(double angle)
{
  double angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}

/**
 * @brief Checks whether the vehicle is stopped
 */
bool PlannerUtils::isStopped(VehicleState &vehState) {
  return (AliceStateHelper::getVelocityMag(vehState) < 0.2);
}

int PlannerUtils::getLaneSide(Map* localMap, VehicleState vehState, LaneLabel currLane, LaneLabel desiredLane) {
  bool isReverse = false;
  point2 tmp_pt;

  if (currLane == LaneLabel(0,0) || desiredLane == LaneLabel(0,0) || currLane == desiredLane) {
    return 0; 
  }

  double currlane_angle;
  double alice_angle = AliceStateHelper::getHeading(vehState);
  point2 alice_rearaxle = AliceStateHelper::getPositionRearAxle(vehState);
  localMap->getHeading(currlane_angle, tmp_pt, currLane, alice_rearaxle);
  double diff_angle = fabs(getAngleInRange(alice_angle-currlane_angle));
  isReverse = (diff_angle > M_PI/2);

  LaneLabel lane;
  localMap->getNeighborLane(lane,currLane,-1);

  if (lane == desiredLane) {
    if (isReverse)
      return 1;
    return -1;
  } 

  localMap->getNeighborLane(lane,currLane,1);

  if (lane == desiredLane) {
    if (isReverse)
      return -1;
    return 1;
  } 

  return 0;
}

double PlannerUtils::getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &current_lane)
{
  // Set position
  point2 currFrontPos;
  currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  // Get all stoplines on that lane
  vector<PointLabel> stoplines;
  map->getLaneStopLines(stoplines, current_lane);

  // Get center line
  point2arr centerline;
  map->getLaneCenterLine(centerline, current_lane);

  // Find the closest stopline on the lane on front of use
  point2 stop_position;
  double dist, min_dist = -1;
  for (unsigned int i=0; i<stoplines.size(); i++) {
    map->getWaypoint(stop_position, stoplines[i]);
    map->getDistAlongLine(dist, centerline, stop_position, currFrontPos);
    if (dist >= 0.0 && (dist < min_dist || min_dist == -1)) {
      min_dist = dist;
    }
  }

  return min_dist;
}
