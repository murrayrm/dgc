/**********************************************************
 **
 **  ZONECORRIDOR.CC
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 17:23:25 2007
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <skynettalker/SkynetTalker.hh>
#include "temp-planner-interfaces/Log.hh"
#include "alice/AliceConstants.h"
#include <math.h>

int ZoneCorridor::m_prevZoneAction = TRAFFIC_STATE_UNSPECIFIED;
bool ZoneCorridor::m_firstTimeBackup = true;
point2 ZoneCorridor::m_bup_finPos;
double ZoneCorridor::m_bup_finHead;

#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0:  0)

Err_t ZoneCorridor::setupCSpecs(CSpecs_t& cSpecs, Map* map, VehicleState vehState, SegGoals currSegGoals, StateProblem_t stateProblem, Path_params_t pathParams, pose3_t finPose)
{
  Err_t error = GU_OK | LP_OK | PP_OK | VP_OK | PLANNER_OK;
  //  unsigned long long time1, time2;   //  DGCgettime(time1);
  point2arr corridor;
  BitmapParams bmParams;
  PolygonParams polygonParams;
  bmParams.resX = 0.1;
  bmParams.resY = 0.1;
  bmParams.width = 800;
  bmParams.height = 800;
  bmParams.baseVal = 1 ;
  bmParams.outOfBounds = 100;
  polygonParams.centerlaneVal = 1;
  polygonParams.obsCost = 500;

  point2 initPos = AliceStateHelper::getPositionRearAxle(vehState);
  PointLabel exitLabel = PointLabel(currSegGoals.exitSegmentID, currSegGoals.exitLaneID, currSegGoals.exitWaypointID); 

  // Set up the corridor and paint the costs
  switch (stateProblem.region) {
  case ROAD_REGION:

    Log::getStream(4)<<"ZONE/ROAD_REGION: Exit label  "<<exitLabel<<endl;

    /* set the zone action*/ 
    if (stateProblem.state == UTURN)
      cSpecs.setTrafficState(TRAFFIC_STATE_UTURN);
    else if (stateProblem.state == BACKUP)
      cSpecs.setTrafficState(TRAFFIC_STATE_BACKUP);
    else
      cSpecs.setTrafficState(TRAFFIC_STATE_ROAD);
    
    /* Paint the cost*/
    paintCostRoadRegion(cSpecs, bmParams, corridor, polygonParams, map, initPos, currSegGoals);
    
    break;
      
  case ZONE_REGION:
      
    pathParams.zoneId = currSegGoals.entrySegmentID;
    
    /* Determine what we plan to do in the zone based on exit point */
    Log::getStream(4)<<"ZONE: Exit label  "<<exitLabel<<endl;
    
    /* set the zone action*/ 
    cSpecs.setTrafficState(determineZoneAction(exitLabel, pathParams.zoneId, map));
    
    // Actually paint the costs
    paintCostZone(cSpecs,bmParams,corridor, polygonParams, map, initPos, pathParams.zoneId);
            
    break;
	
  case INTERSECTION:

    /* Get the corridor*/

    Log::getStream(4)<<"ZONE/ROAD_REGION: Exit label  "<<exitLabel<<endl;
    
    /* set the zone action*/ 
    cSpecs.setTrafficState(TRAFFIC_STATE_ROAD);


    /* Paint the cost*/
    paintCostRoadRegion(cSpecs, bmParams,corridor, polygonParams, map, initPos, currSegGoals);
    
    //    // Assign the cost map in cSpecs
    //    cSpecs.setCostMap(bmParams);
    
    break;
    
  default:
    Console::addMessage("ZONECORRIDOR: gen corridor: SHOULD NEVER GET HERE");
    Log::getStream(1) <<"ZONECORRIDOR: gen corridor: SHOULD NEVER GET HERE" << endl;
	
  }
  // Set up initial, final conditions
  double startingState[4];
  startingState[0] = initPos.x;
  startingState[1] = initPos.y;
  startingState[2] = AliceStateHelper::getVelocityMag(vehState);
  startingState[3] = AliceStateHelper::getHeading(vehState);
  cSpecs.setStartingState(startingState);
  // starting controls
  double startingControls[2];
  startingControls[0] = AliceStateHelper::getAccelerationMag(vehState);
  startingControls[1] = 0;
  cSpecs.setStartingControls(startingControls);
  
  // final state
  
  if (stateProblem.region == ROAD_REGION) { // Fault handling/uturn/backup
    point2 tmpPt;
    double heading;
    if (stateProblem.state != BACKUP) { // fault handling and uturn
      map->getWaypoint(tmpPt, exitLabel);
      map->getHeading(heading, exitLabel);
    } else { // uturn/road
#define BACKUP_DISTANCE -2.0
      if (m_firstTimeBackup) {
        // TODO: we want to fix this!!
        LaneLabel lane = Utils::getCurrentLane(vehState, map);
        map->getLaneCenterPoint(m_bup_finPos, lane, initPos, BACKUP_DISTANCE);
        map->getHeading(m_bup_finHead, m_bup_finPos);
        m_firstTimeBackup = false;
      }
      tmpPt = m_bup_finPos;
      heading = m_bup_finHead;
    }
    finPose.pos.x = tmpPt.x;
    finPose.pos.y = tmpPt.y;
    finPose.pos.z = 0;
    finPose.rot = quat_from_rpy(0,0,heading);
  }

  double finalState[4];
  double roll, pitch, yaw;
  quat_to_rpy(finPose.rot, &roll, &pitch, &yaw);
  if (stateProblem.state == BACKUP) {
    finalState[0] = finPose.pos.x;
    finalState[1] = finPose.pos.y;
    finalState[2] = 0.2; //dplanner requirement change to 0.1
    finalState[3] = yaw;
  } else {
    finalState[0] = finPose.pos.x - DIST_REAR_AXLE_TO_FRONT*cos(yaw);
    finalState[1] = finPose.pos.y - DIST_REAR_AXLE_TO_FRONT*sin(yaw);
    finalState[2] = 0.2; //dplanner requirement change to 0.1
    finalState[3] = yaw;
  }
  cSpecs.setFinalState(finalState);
  // final controls
  double finalControls[2];
  finalControls[0] = 0;
  finalControls[1] = 0;
  cSpecs.setFinalControls(finalControls);
  
  //set maximums 
  cSpecs.setMaxVelocity(6);
  cSpecs.setMaxAcc(VEHICLE_MAX_ACCEL);
  cSpecs.setMaxBraking(VEHICLE_MAX_DECEL);
  cSpecs.setMaxSteeringAngle(VEHICLE_MAX_AVG_STEER);
  cSpecs.setMaxSteeringRate(M_PI/2);
  
    // Planner specific functions
  switch (stateProblem.planner) {
    case S1PLANNER:
    case CIRCLE_PLANNER:
    case DPLANNER:
      break;
    default:
      Console::addMessage("ZONECORRIDOR: planner specific cspecs functions: SHOULD NEVER GET HERE");
      Log::getStream(1) <<"ZONECORRIDOR: planner specific cspecs functions: SHOULD NEVER GET HERE" << endl;
  }

  // Dealing with obstacles in the different ways
  vector<double> obstacle_safety_margin;
  double front, side, rear;
  switch (stateProblem.obstacle) {
  case OBSTACLE_SAFETY:
    front = 2.0;
    side = 1.0;
    rear = 1.0;
    break;
  case OBSTACLE_AGGRESSIVE:
    front = 1.0;
    side = 0.75;
    rear = 0.75;
    break;
  case OBSTACLE_BARE:
    front = 0.5;
    side = 0.5;
    rear = 0.5;
    break;    
  default:
    Console::addMessage("ZONECORRIDOR: dealing with obstacles: SHOULD NEVER GET HERE");
    Log::getStream(1) <<"ZONECORRIDOR: dealing with obstacles: SHOULD NEVER GET HERE" << endl;
    
  }

  obstacle_safety_margin.push_back(front);
  obstacle_safety_margin.push_back(side); // right
  obstacle_safety_margin.push_back(rear);
  obstacle_safety_margin.push_back(side); // left
  Log::getStream(7) << "ZONE: setting a obstacle safety margin of " << obstacle_safety_margin << " around Alice" << endl;
  cSpecs.setObstacleSafetyMargins(obstacle_safety_margin);
  
  // vector<double> perimeter_safety_margin;
  //  perimeter_safety_margin.push_back(1.0);
  double perimeter_safety_margin = 1.0;
  Log::getStream(7) << "ZONE: setting a perimeter safety margin of " << perimeter_safety_margin << " around Alice" << endl;
  cSpecs.setPerimeterSafetyMargins(perimeter_safety_margin);

  // reset the first time in backup flag
  if ( (stateProblem.state != BACKUP) && (!(m_firstTimeBackup)) )
    m_firstTimeBackup = true;

  return error;
}

void ZoneCorridor::paintCostZone(CSpecs_t& cSpecs, BitmapParams& bmparams, point2arr& corridor, PolygonParams &polygonParams, Map* map, point2 alicePos, int zoneId)
{
  bmparams.centerX = alicePos.x;
  bmparams.centerY = alicePos.y;
  
  bmparams.polygons.clear();

  float pSpaceCost = 0; /* The max value of the parking space cost */
  float pSpaceXCoeff = 0; /* A coefficient for the parking space cost function  */
  float pSpaceWidth =  4;  /* The width of the cost painting around the parking space line FIX */

  float minValPark = 40;

  CostSide side = LEFT_AND_RIGHT; /* Paint the cost to the left and right of the line */

  /* Get the parking spots in the zone and paint them */
  vector<SpotLabel> parkingLabels; 
  vector<point2arr> parkingSpots; 
  if (0 != zoneId) {
    /* get the zone perimeter */
    map->getZonePerimeter(corridor, zoneId);
    cSpecs.setBoundingPolygon(corridor);
    
    /* Paint the cost*/
    /* Get the parking spots */
    SpotLabel spotLabel; 
    point2 first, second; 
    point2arr spotWaypt; 
    
    map->getZoneParkingSpots(parkingLabels, zoneId);
    
    for (unsigned int i =0; i<parkingLabels.size(); i++) {	
      map->getSpotWaypoints(first, second,parkingLabels[i]);
      spotWaypt.push_back(first);
      spotWaypt.push_back(second);
      parkingSpots.push_back(spotWaypt);
      spotWaypt.clear();
    }
  }
        
  /* Get the obstacles in the zone */
  vector<MapElement> obstacles; 
  map->getObsInZone(obstacles, zoneId);
  /* Convert the obstacles from map elements to point2arr and populate cSpecs */
  populatePolygonalObstacles(cSpecs, obstacles);

  /* Paint the obstacle and perimeter cost and parking space cost*/
  paintCost(bmparams,polygonParams, alicePos, corridor, obstacles);
  assignParkingSpaceCost(bmparams.polygons,parkingSpots, pSpaceXCoeff, pSpaceCost, minValPark, pSpaceWidth, side);
  // Assign the cost map in cSpecs
  cSpecs.setCostMap(bmparams);

}


void ZoneCorridor::paintCostRoadRegion(CSpecs_t& cSpecs, BitmapParams& bmparams,point2arr& corridor, PolygonParams &polygonParams, Map* map, point2 alicePos, SegGoals currSegGoals)
{
  
  /* Create the zone-like corridor*/
  generateGenericPerimeter(corridor, map, alicePos, currSegGoals);
  cSpecs.setBoundingPolygon(corridor);

  Log::getStream(4)<<"ZONE/ROADREGION: corridor: ";     
  for (unsigned int i = 0; i <corridor.size(); i++) {
    Log::getStream(4)<<"x= "<<corridor[i].x<<"y="<<corridor[i].y<<endl;
  }

  /* Get the obstacles in this zone perimeter */
  vector<MapElement> obstacles; 
  map->getObsInPoly(obstacles, corridor);

  /* Convert the obstacles from map elements to point2arr and populate cSpecs */
  populatePolygonalObstacles(cSpecs, obstacles);

  /* Paints the zone perimeter and obstacle costs */
  paintCost(bmparams,polygonParams,alicePos, corridor, obstacles);

  /* Paints the lane lines that are inside the corridor FIX: for now just get current lane*/
  point2arr rightBound, leftBound; 
  LaneLabel lane; 
  map->getLane(lane,alicePos);

  map->getLaneLeftBound(leftBound, lane);
  map->getLaneRightBound(rightBound, lane);

  float cost = 50; /* The max value of the perimeter cost */
  float xCoeff = -0.5;
  float width =  VEHICLE_WIDTH;  /* The width of the cost painting around the perimeter line */
  float minVal = 1;
  CostSide side = LEFT_AND_RIGHT; /* Paint the cost to the left and right of the line */
  
  assignLaneCost(bmparams.polygons, leftBound, rightBound,xCoeff,cost, minVal, width, side);

  // Assign the cost map in cSpecs
  cSpecs.setCostMap(bmparams);

}

void ZoneCorridor::paintCost(BitmapParams& bmparams, PolygonParams &polygonParams, point2 alicePos, point2arr corridor,vector<MapElement> obstacles)
{
  bmparams.centerX = alicePos.x;
  bmparams.centerY = alicePos.y;
  
  bmparams.polygons.clear();
  float perimCost = 50; /* The max value of the perimeter cost */
  float perimXCoeff = -0.5;
  float perimWidth =  VEHICLE_WIDTH*2;  /* The width of the cost painting around the perimeter line */

  float obsCost = polygonParams.obsCost; /* The max value of the obstacle cost */
  float obsXCoeff = -0.5;
  float obsWidth = 4; /* FIX ME */

  float minValPerim = 1;
  float minValPark = 40;
  float minValObs = 1;

  CostSide side = LEFT_AND_RIGHT; /* Paint the cost to the left and right of the line */

  assignPerimeterCost(bmparams.polygons,corridor, perimXCoeff, perimCost, minValPerim, perimWidth, side);

  Log::getStream(9) << "ZoneCorridor: Number of obstacles=" << obstacles.size()<<endl;

  for (unsigned int i= 0; i < obstacles.size(); i++) {    
    assignObstacleCost(bmparams.polygons,obstacles[i], obsXCoeff, obsCost, minValObs, obsWidth,FILL_EXP2,COMB_MAX); 
  }
}


void ZoneCorridor::display(int sn_key, int sendSubgroup, point2arr corridor, CSpecs_t cSpecs)
{
  CMapElementTalker meTalker;
  meTalker.initSendMapElement(sn_key);
  int counter=12000;
  point2 point;
  MapId mapId;
  MapElement me;
  
  mapId = counter;

  // display the corridor
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_BLUE, 100);
  me.setGeometry(corridor);
  meTalker.sendMapElement(&me,sendSubgroup);
  
  // print the initial and final cond's
  vector<double> startingState = cSpecs.getStartingState();
  vector<point2> points;
  point.set(startingState[0], startingState[1]);
  points.push_back(point);
  point.set(startingState[0]+cos(startingState[3]), startingState[1]+sin(startingState[3]));
  points.push_back(point);
  mapId = 12001;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_YELLOW, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

  // print the initial and final cond's
  vector<double> finalState = cSpecs.getFinalState();
  point.set(finalState[0], finalState[1]);
  points.push_back(point);
  point.set(finalState[0]+cos(finalState[3]), finalState[1]+sin(finalState[3]));
  points.push_back(point);
  mapId = 12002;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_RED, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

}

void ZoneCorridor::print(point2arr corridor, CSpecs_t cSpecs)
{
  vector<double> startingState = cSpecs.getStartingState();
  vector<double> finalState = cSpecs.getFinalState();
  vector<double> startingControls = cSpecs.getStartingControls();
  vector<double> finalControls = cSpecs.getFinalControls();
  Console::addMessage("Init state = (%6.2f,%6.2f,%6.2f,%6.2f)", startingState[0], startingState[1], startingState[2], startingState[3]);
  Console::addMessage("Init control = (%6.2f,%6.2f)", startingControls[0], startingControls[1]);
  Console::addMessage("Fin state = (%6.2f,%6.2f,%6.2f,%6.2f)", finalState[0], finalState[1], finalState[2], finalState[3]);
  Console::addMessage("Fin control = (%6.2f,%6.2f)", finalControls[0], finalControls[1]);
}


void ZoneCorridor::assignPerimeterCost(vector<Polygon>& polygons, point2arr corridor, float xCoeff,
				       float cost, float minVal, float width, CostSide side) 
{  

  /* Add the first point again to the end of corridor array */
  corridor.push_back(corridor[0]);
  assignCostToLine(polygons, corridor, xCoeff, cost, minVal, width, side, FILL_EXP2,COMB_MAX);
}

void ZoneCorridor::assignLaneCost(vector<Polygon>& polygons, point2arr& leftbound, point2arr &rightbound,
				  float xCoeff, float cost, float minVal, float width, CostSide side)
{

  assignCostToLine(polygons, leftbound, xCoeff, cost, minVal, width, side, FILL_EXP2,COMB_MAX);
  assignCostToLine(polygons, rightbound, xCoeff, cost, minVal, width, side, FILL_EXP2,COMB_MAX);
}



void ZoneCorridor::assignParkingSpaceCost(vector<Polygon>& polygons,vector<point2arr> parkingSpots, float xCoeff, float cost, float minVal, float width, CostSide side) 
{
  for ( unsigned int i =0; i < parkingSpots.size(); i++) {
    assignCostToLine(polygons, parkingSpots[i], xCoeff, cost, minVal, width, side, FILL_LINEAR,COMB_MAX);
  }
}

void ZoneCorridor::assignObstacleCost(vector<Polygon>& polygons, MapElement& el,
				      float xCoeff, float obsCost, float bval, float width, 
				      bitmap::FillFunc fillFunc, bitmap::CombineFunc combFunc) 
{

  Log::getStream(9) << "ZoneCorridor: About to assign obstacle cost " << endl;

  double sigma = 1*VEHICLE_WIDTH/2;
  //  if (el.type != ELEMENT_OBSTACLE && el.type != ELEMENT_VEHICLE) {
  if (!el.isObstacle()) {
    // this shouldn't happen, because it's checked in convertMapElementToPolygon()
    cerr << "CORRIDOR: FAILED ERROR: " << __FILE__ << ":" << __LINE__ << " cannot handle nonobstacle "
	 << el.type << " of map element" << endl;
    return; 
  }

  //  if (el.type == ELEMENT_VEHICLE) {
  if (el.isVehicle()) {
    Log::getStream(9) << "ZoneCorridor: el.type = ELEMENT_VEHICLE" << endl;
    //    return;
  }

  if (el.geometryType != GEOMETRY_POLY) {
    cerr << "CORRIDOR- FAILED ERROR: " << __FILE__ << ":" << __LINE__ << "cannot handle geometry type "
	 << el.geometryType << " of map element" << endl;
    return;
  }

  point2arr_uncertain vertices1(el.geometry);
  point2arr_uncertain vertices2(el.geometry);

  if (vertices1.size() == 0) {
    cerr << "ZoneCorridor: FAILED ERROR: " << __FILE__ << ":" << __LINE__ << " obstacle has no vertices." << endl;
    return;
  }

  Log::getStream(9) << "ZoneCorridor: el.type = ELEMENT_OBSTACLE. Will be painted in the map" << endl;
  Log::getStream(9) << "Obstacle at " << el.geometry << endl;
  Log::getStream(9) << "  height = " << endl;

  point2_uncertain obsCenter(0,0);
  for (unsigned i=0; i < vertices1.size(); i++)
    obsCenter = obsCenter + vertices1[i];
  
  obsCenter = obsCenter/vertices1.size();

  // Grow the obstacle
  for (unsigned i=0; i < vertices1.size(); i++) {
    double distFromCenter1 = obsCenter.dist(vertices1[i]) + VEHICLE_WIDTH;
    double distFromCenter2 = obsCenter.dist(vertices1[i]) + VEHICLE_WIDTH;
    double directionFromCenter = (vertices1[i] - obsCenter).heading();
    vertices1[i].set_point(obsCenter.x + distFromCenter1*cos(directionFromCenter),
			   obsCenter.y + distFromCenter1*sin(directionFromCenter));
    vertices2[i].set_point(obsCenter.x + distFromCenter2*cos(directionFromCenter),
			   obsCenter.y + distFromCenter2*sin(directionFromCenter));
  }

  Log::getStream(9) << "ZoneCorridor: Grew obstacle " << endl;

  vector<float> cost(vertices1.size(), obsCost);
  Polygon tmpPoly;
  tmpPoly.setVertices(vertices1, cost);
  tmpPoly.setCombFunc(combFunc);
  polygons.push_back(tmpPoly);

  // Gaussian function for continuity
  double a = -1/(2*pow(sigma,2));
  tmpPoly.setA(a);
  tmpPoly.setB(obsCost);
  tmpPoly.setC(0);
  tmpPoly.setFillFunc(fillFunc);

  for (unsigned i=0; i < vertices1.size(); i++) {
    point2arr_uncertain tmpVertices;
    cost.clear();

    cost.push_back(0);
    cost.push_back(0);
    double baseX = sqrt((1/a)*log(bval/obsCost));
    cost.push_back(baseX);
    cost.push_back(baseX);
    tmpVertices.push_back(vertices1[i]);
    if (i < vertices1.size() - 1) {
      tmpVertices.push_back(vertices1[i+1]);
      tmpVertices.push_back(vertices2[i+1]);
    }
    else {
      tmpVertices.push_back(vertices1[0]);
      tmpVertices.push_back(vertices2[0]);
    }
    tmpVertices.push_back(vertices2[i]);

    tmpPoly.setVertices(tmpVertices, cost);
    polygons.push_back(tmpPoly);
  }

}

void ZoneCorridor::assignCostToLine(vector<Polygon>& polygons, point2arr line,
				    float xCoeff, float cost, float minVal, float width, 
				    CostSide side,bitmap::FillFunc fillFunc, bitmap::CombineFunc combFunc)
{
  Polygon p;
  const cost_t sideVal = 2; // go down to 2*sigma (cost*ext(-0.5*2*2)=cost*0.1353)
 
  p.setFillFunc(fillFunc);
  p.setA(xCoeff);
  p.setB(cost);
  p.setC(minVal);
  p.setCombFunc(combFunc);

  // for ease of notation

  for (unsigned int i = 0; i < line.size()-1; i++)
  {
      // calculate bisectors for vertices v1 and v2. For a generic vertex k, the
      // bisector is the orthogonal to v(k+1) - v(k-1) through the vertex v(k)
      // except for the first and the last, which are treated specially
      point2 bisec[2];
      point2 distVec;

      if (i > 0) {
          distVec = line[i + 1] - line[i - 1];
      } else {
          distVec = line[i + 1] - line[i];
      }
      bisec[0] = point2(-distVec.y, distVec.x); // orthog to distVec
      bisec[0].normalize(); // FIXME: what if the points are so close that the distance is ~zero?

      if (i + 2 < line.size()) {
          distVec = line[i + 2] - line[i];
      } else {
          distVec = line[i + 1] - line[i];
      }
      bisec[1] = point2(-distVec.y, distVec.x); // orthog to distVec
      bisec[1].normalize(); // FIXME: what if the points are so close that the distance is ~zero?

      point2arr vert(4);
      vector<cost_t> val(4);
      // FIXME: there is the possibility that the top egde intersects the
      // bottom edge, for very sharp turns, creating something that is not a polygon
      // (a star? a butterfly? it's just that vertices are not in cw nor in ccw order)
      // The resulting cost map will be weird, but still acceptable, I think.

      // left side
      vert[0] = line[i + 1] + bisec[1]*width;
      val[0] = -sideVal;
      vert[1] = line[i]     + bisec[0]*width;
      val[1] = -sideVal;

      // right side
      vert[2] = line[i]     - bisec[0]*width;
      val[2] = sideVal;
      vert[3] = line[i + 1] - bisec[1]*width;
      val[3] = sideVal;

      p.setVertices(vert, val);
      polygons.push_back(p);
  }
}

void ZoneCorridor::populatePolygonalObstacles(CSpecs& cspecs, vector<MapElement> obstacles) {
  
  vector<point2arr> polyObstacles; 
  vector<CSpecsObstacle> cSpecsObstacles; 
  point2arr polygon; 
  CSpecsObstacle obs; 

  for (unsigned int i =0; i< obstacles.size(); i++) {
    for(unsigned int j=0; j<obstacles[i].geometry.size();j++) {
      polygon.push_back(obstacles[i].geometry[j]);
      obs.x =  obstacles[i].center.x;
      obs.y = obstacles[i].center.y;
      obs.dx = 0; //for now 
      obs.dy = 0; //for now
      obs.length = obstacles[i].length;
      obs.width = obstacles[i].width; 
    }
    cSpecsObstacles.push_back(obs);
    polyObstacles.push_back(polygon);
    polygon.clear();
  }
  cspecs.setPolyObstacles(polyObstacles);
  cspecs.setObstacles(cSpecsObstacles);
}

void ZoneCorridor::resetFinalConditions(CSpecs& cSpecs, pose3_t finPose, PathDirection direction) {

  double finalState[4];
  double roll, pitch, yaw;
  quat_to_rpy(finPose.rot, &roll, &pitch, &yaw);
  if (GRAPH_PATH_FWD == direction)
  {
  	finalState[0] = finPose.pos.x - DIST_REAR_AXLE_TO_FRONT*cos(yaw);
  	finalState[1] = finPose.pos.y - DIST_REAR_AXLE_TO_FRONT*sin(yaw);
  }
  else
  {
  	finalState[0] = finPose.pos.x;
  	finalState[1] = finPose.pos.y;
  }
  finalState[2] = 0.2; //dplanner requirement change to 0.1
  finalState[3] = yaw;

  setPathDirection(cSpecs, direction);
  cSpecs.setFinalState(finalState);

}


void ZoneCorridor::setPathDirection(CSpecs& cSpecs, PathDirection direction) { 
  int dir = 0;
  if (GRAPH_PATH_FWD == direction) 
    dir = 1;
  else if (GRAPH_PATH_REV == direction) 
    dir = -1;
  cSpecs.setPathDirection(dir);
}


void ZoneCorridor::generateGenericPerimeter(point2arr& perim, Map* map, point2 currPos, SegGoals currSegGoals) {

  /* Exit label of the current goal */
  PointLabel exitLabel = PointLabel(currSegGoals.exitSegmentID, currSegGoals.exitLaneID, currSegGoals.exitWaypointID); 

  /* Calculate distance from Alice to the exit point */
  point2 exitPoint; 

  map->getWaypoint(exitPoint, exitLabel);

  Log::getStream(4)<<"ZONE: Exit point "<<exitPoint<<endl;
 
  double distAliceExit = currPos.dist(exitPoint);

 /* Project the currPos away 20 m from Alice in the y direction */
  point2 projCurrPos(currPos.x, currPos.y - 20);   
 
  point2 projExit; 
  
  /* Add vertices 50 m from projCurrPos in x direction */
  point2 v1(projCurrPos.x + 50, projCurrPos.y, 0);
  point2 v2(projCurrPos.x - 50, projCurrPos.y, 0); 

  if (distAliceExit < 80) {

    double distExit = 80 - distAliceExit;

    Log::getStream(4)<<"ZONE Distance Alice and Exit point "<<distAliceExit<<endl; 

    /* Project the exit point distExit away in the y direction from Alice */
    projExit.x = exitPoint.x;
    projExit.y = exitPoint.y + distExit;
    projExit.z = 0;

  } else {

    /* Project the exit point 20 m in the y direction */
    projExit.x = exitPoint.x;
    projExit.y = exitPoint.y +20;  
    projExit.z = 0;
  }
 
  /* Add vertices 50 m from projExit in x direction */  
  point2 v3(projExit.x + 50, projExit.y, 0);
  point2 v4(projExit.x - 50, projExit.y, 0); 

  /* Insert the points counterclockwise*/

  perim.push_back(v2);
  perim.push_back(projCurrPos);
  perim.push_back(v1);
  perim.push_back(v3);
  perim.push_back(projExit);
  perim.push_back(v4);
  
  for (unsigned int i = 0; i<perim.size(); i++) {
    Log::getStream(4)<<"ZONE Perimeter "<<endl; 
    Log::getStream(4)<<"x_"<<i<<"= "<<perim[i].x<<", y_"<<i<<"= "<<perim[i].y<<endl;
  }
}

void ZoneCorridor::printCSpecs(CSpecs* cSpecs) {

  Log::getStream(4) << "ZONE/CSPECS Initial state ( ";
  vector <double> initState = cSpecs->getStartingState();
  for (unsigned int i = 0; i < initState.size(); i++) {
    Log::getStream(4)<<initState[i]<<", ";
  }
  Log::getStream(4) << ") "<<endl;
  
  Log::getStream(4) << "ZONE/CSPECS Final state (";
  vector <double> finalState = cSpecs->getFinalState();
  for (unsigned int i = 0; i < finalState.size(); i++) {
    Log::getStream(4)<<finalState[i]<<", ";
  }
  Log::getStream(4) << ")"<<endl; 
  Log::getStream(4) << "ZONE/CSPECS Path direction = "<< cSpecs->getPathDirection()<<endl;
}



int ZoneCorridor::determineZoneAction(PointLabel exitLabel, int zoneId, Map* map) {

  /* If the current seg goal exit waypoint corresponds to the end of a parking spot, then we are entering a spot */
  if (zoneId == exitLabel.segment && 2 == exitLabel.point) {
    m_prevZoneAction = TRAFFIC_STATE_ENTER_PARK;
  } else {
    vector<PointLabel> exitLabels, exitLinks; 
    map->getZoneExits(exitLabels, exitLinks,zoneId);

    for (unsigned int i=0; i<exitLabels.size(); i++) {

      /* If the exit waypoint correponds to the exit of a zone and we had previously entered a parking spot or in the middle of exiting a spot. */
      if (exitLabel == exitLabels[i] && (TRAFFIC_STATE_ENTER_PARK == m_prevZoneAction || TRAFFIC_STATE_EXIT_PARK == m_prevZoneAction)) {
        m_prevZoneAction = TRAFFIC_STATE_EXIT_PARK;
        return m_prevZoneAction;
        /* If the exit waypoint correponds to the exit of a zone but we previously had not entered a parking spot, we are traversing a zone */
      } else if (exitLabel == exitLabels[i]) {
        m_prevZoneAction = TRAFFIC_STATE_TRAVERSE_ZONE;

      } else {
        m_prevZoneAction = TRAFFIC_STATE_UNSPECIFIED;
      }
    }
  }
  return m_prevZoneAction;
}

