/**********************************************************
 **
 **  ZONECORRIDOR.HH
 **
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 15:41:29 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef ZONECORRIDOR_HH
#define ZONECORRIDOR_HH

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <frames/point2.hh>
#include <map/Map.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
//#include <ocpspecs/OCPtSpecs.hh>
#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
#include <frames/pose3.h>
#include <temp-planner-interfaces/AliceStateHelper.hh>

struct PolygonParams
{
  float centerlaneVal;
  float obsCost;
  
  PolygonParams()
  {
    centerlaneVal = 0;
    obsCost = 10000;
  }
};


//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class ZoneCorridor
{  
public:
  static Err_t generateCorridor(CSpecs& cSpecs, point2arr zonePerimeter, Path_params_t params, pose3_t finPose, VehicleState vehState);


  
private:
  /*! set the ocp params variable */
  //  static void setOCPparams(OCPparams& ocpParams, double velMax, double velMin, point2 initPos, double initHeading, double initVel, double initAcc, double initSteer, int mode, point2 finPos, double finHeading, double finVel, double finAcc, double finSteer);

  /*! generate the polycorridor */
  //  static void generatePolyCorridor(CPolytope** polyCorridor, int& nPolytopes, point2arr zonePerimeter);
  
  /*! paint the lane in the costmap*/
  static void convertZoneToPolygon(vector<Polygon>& polygons, point2arr& zonePerimeter, float val);
  
  /*! define the cost map ito polytopes */
  static void getBitmapParams(BitmapParams& bmparams, PolygonParams &polygonParams, point2 alicePos, point2arr zonePerimeter);

  /*! display some debugging info */
  //  static  void display(int sn_key, int sendSubgroup, point2arr zonePerimeter, OCPparams ocpParams, BitmapParams bmparams);

  /*1 print some debugging info */
  //  static void print(point2arr zonePerimeter, OCPparams ocpParams, CPolytope* polyCorridor);
  
  

};
#endif
