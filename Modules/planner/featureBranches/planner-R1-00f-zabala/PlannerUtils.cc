#include "PlannerUtils.hh"
#include <alice/AliceConstants.h>
#include <math.h>
#include "Log.hh"

#define EPS 0.2

/**
 * @brief Adds two angles and adjust the sum according to the PI/-PI convention
 */
int PlannerUtils::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<=-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}

/**
 * @brief Returns an -PI, PI bounded angle\
 */
double PlannerUtils::getAngleInRange(double angle)
{
  double angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}

/**
 * @brief Checks whether the vehicle is stopped
 */
bool PlannerUtils::isStopped(VehicleState &vehState) {
  return (AliceStateHelper::getVelocityMag(vehState) < 0.2);
}
