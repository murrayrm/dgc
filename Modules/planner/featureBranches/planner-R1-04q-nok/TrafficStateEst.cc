/*!
 * \file TrafficStateEst.cc
 * \brief Some legacy code from the tplanner. Maintains the local map and the vehicle state. Needs to be incorporated into the planner.
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include "TrafficStateEst.hh"
#include <frames/mat44.h>
#include <interfaces/sn_types.h>
#include <alice/AliceConstants.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Console.hh>


#define QUEUE_MAPELEMENTS 0

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(bool waitForStateFill) 
  : CSkynetContainer(MODtrafficplanner, CmdArgs::sn_key)
  , CStateClient(waitForStateFill)
{
  DGCcreateMutex(&m_localMapUpMutex);
	DGCcreateMutex(&m_localRndfOffsetMutex);

	UpdateState();
	if (CmdArgs::mapper_use_internal){

		m_mapper = new Mapper();
		assert(m_mapper);
  }

  m_localMap = new Map(!CmdArgs::mapper_disable_line_fusion);
	m_localUpdateMap = new Map(!CmdArgs::mapper_disable_line_fusion);
  m_localMap->init();
  m_localUpdateMap->init();

  frozen = false;
  mapElements.reserve(200);
	
	loadRNDF(CmdArgs::RNDF_file);
	
	UpdateState();
	m_localMap->setLocalToSiteOffset(m_state);
	DGClockMutex(&m_localMapUpMutex);
	m_localUpdateMap->setLocalToSiteOffset(m_state);
	DGCunlockMutex(&m_localMapUpMutex);

#if USE_FUSED
  this->fused = NULL;
#endif  
}

TrafficStateEst::~TrafficStateEst() 
{
  delete m_localMap;
	delete m_localUpdateMap;	
	if (CmdArgs::mapper_use_internal){
		delete m_mapper;
	}
		
  DGCdeleteMutex(&m_localMapUpMutex);
}


TrafficStateEst* TrafficStateEst::Instance(bool waitForStateFill)
{
  if (pinstance == 0)
    pinstance = new TrafficStateEst(waitForStateFill);
  return pinstance;
}

int TrafficStateEst::init()
{
  if (CmdArgs::use_RNDF){
    Log::getStream(1)<<"I am using the RNDF"<<endl;
    if (!CmdArgs::RNDF_file.empty()){
      Log::getStream(1)<<"RNDF is not empty "<<endl;
			

			//Check if the internal mapper is being used
			//if (!CmdArgs::mapper_use_internal){
      m_mapElemTalker.initSendMapElement(CmdArgs::sn_key);
      m_mapElemTalker.initRecvMapElement(CmdArgs::sn_key, 1);
			//}
    
    } else {
      Log::getStream(1)<<"TFEST: RNDFfilename is empty"<<endl;
    }
  } else {
    Log::getStream(1)<<"TFEST: not using RNDF"<<endl;
  }
  return 0;
}


void TrafficStateEst::Destroy()
{
  delete pinstance;
  pinstance = 0;
}


// Update both vehicle and actuator states.
// TODO REMOVE
void TrafficStateEst::updateState() 
{

  UpdateState();
  UpdateActuatorState();

  DGClockMutex(&m_localRndfOffsetMutex);
  m_currVehState = m_state;
  m_currActState = m_actuatorState;
  DGCunlockMutex(&m_localRndfOffsetMutex);
  
  /* REMOVE  
  if (CmdArgs::use_rndf_frame){
    DGClockMutex(&m_localRndfOffsetMutex);
    m_currVehState.localX += m_localToRndfOffset.x;
    m_currVehState.localY += m_localToRndfOffset.y;
    DGCunlockMutex(&m_localRndfOffsetMutex);
  }
  */
}

VehicleState TrafficStateEst::getVehState() 
{
  return m_currVehState;
}


ActuatorState TrafficStateEst::getActState() 
{
  return m_currActState;
}

void TrafficStateEst::freezeMap()
{
  frozen = true;
}

void TrafficStateEst::meltMap()
{
  frozen = false;
}

void TrafficStateEst::getLocalMapUpdate()
{
  MapElement recvEl;
  int bytesRecv;
  Log::getStream(1) << "In local map update ..." <<endl; 

  
	int retval;
	//bool useRndfFrame = CmdArgs::use_rndf_frame;

  bool senddebug = true;
  MapElement debugEl;
  // send messages every nth time
	int everyn = 100;
  uint64_t starttime;
  uint64_t fullstarttime;

// timing variables for full update loop
	int dtime;
	int peakdtime=0;;
	double avgdtime = 0;
	int totdtime;
	int count = 0;

  // timing variables for main mapping loop
	int maindtime;
	int peakmaindtime=0;;
	double avgmaindtime = 0;
	int totmaindtime;
	int maincount = 0;

  // timing variables for map copy 
	int copydtime;
	int totcopydtime = 0;
	double avgcopydtime;
	int peakcopydtime=0;
	int copycount=0;

	int zerocount = 0;
	int errorcount = 0;

	if (CmdArgs::mapper_use_internal){

		m_mapper->initComm(CmdArgs::sn_key);
    m_mapper->enableGroundstrikeFiltering(CmdArgs::mapper_enable_groundstrike_filtering);	
		m_mapper->disableLineFusion(CmdArgs::mapper_disable_line_fusion);
		m_mapper->enableLineFusionCompat(CmdArgs::mapper_line_fusion_compat);
		m_mapper->disableObsFusion(CmdArgs::mapper_disable_obs_fusion);
		m_mapper->decayAgeThresh = CmdArgs::mapper_decay_thresh;
    m_mapper->init(CmdArgs::RNDF_file);
    m_mapper->setLocalFrameOffset(m_state);

    m_mapper->plannerOutputDisabled = true;
    m_mapper->debugSubGroup = CmdArgs::mapper_debug_subgroup;


		while (true) {

      fullstarttime = DGCgettime();

      //if (useRndfFrame){
      // DGClockMutex(&m_localRndfOffsetMutex);
        // m_localToRndfOffset = listeneroutput;
      //  DGCunlockMutex(&m_localRndfOffsetMutex);    
      //}
      
      starttime = DGCgettime();
			retval = m_mapper->mainLoop();
			maindtime = DGCgettime()-starttime;			
			
			if (senddebug){			
				totmaindtime+=maindtime;
				maincount++;
				if (maindtime>peakmaindtime) peakmaindtime=maindtime;

				if (maincount%everyn==0){
					avgmaindtime = (double)totmaindtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,8);
					debugEl.setLabel(0,"Avg main time ", avgmaindtime);
					debugEl.setLabel(1,"Peak main time ", peakmaindtime);
					debugEl.setLabel(2,"Main count ", maincount);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totmaindtime = 0;
					peakmaindtime = 0;
				}
			}

			if (retval < 0){
				if (senddebug){			
					errorcount++;
					if (errorcount%everyn==0){
						debugEl.clear();
						debugEl.setTypeDebug();
						debugEl.setId(-1,6);
						debugEl.setLabel(0,"error count ", errorcount);
						m_mapElemTalker.sendMapElement(&debugEl,-3);
					}
				}
				Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received error from mapper:updateMap = " << endl;
      }else if (retval >0){ 
				DGClockMutex(&m_localMapUpMutex);
				if (m_mapCopyState !=0){
					starttime = DGCgettime();

          m_mapper->map.copyMapData(m_localMap);

					m_mapCopyState = 0;

					if (senddebug){			
						copydtime = DGCgettime()-starttime;		
						totcopydtime+=copydtime;
						copycount++;
						if (dtime>peakcopydtime) peakcopydtime=copydtime;
					
						if (copycount%everyn==0){
							avgcopydtime = (double)totcopydtime/(double)(everyn);
						
							debugEl.clear();
							debugEl.setTypeDebug();
							debugEl.setId(-1,2);
							debugEl.setLabel(0,"Avg copy time ", avgcopydtime);
							debugEl.setLabel(1,"Peak copy time ", peakcopydtime);
							debugEl.setLabel(2,"Copy count ", copycount);
							m_mapElemTalker.sendMapElement(&debugEl,-3);

							debugEl.clear();
							debugEl.setTypeDebug();
							debugEl.setId(-1,16);
							debugEl.setLabel(0,"map size ", (int)m_mapper->map.data.size());
							debugEl.setLabel(1,"used size ", (int)m_mapper->map.usedIndices.size());
							debugEl.setLabel(2,"unused size ", (int)m_mapper->map.openIndices.size());
							m_mapElemTalker.sendMapElement(&debugEl,-3);
					
							totcopydtime = 0;
							peakcopydtime = 0;
						}
					}
				}
				DGCunlockMutex(&m_localMapUpMutex);
      
			}else{
				if (senddebug){			
					zerocount++;
					if (zerocount%everyn==0){
						debugEl.clear();
						debugEl.setTypeDebug();
						debugEl.setId(-1,5);
						debugEl.setLabel(0,"zero count ", zerocount);
						m_mapElemTalker.sendMapElement(&debugEl,-3);
					}		
				}
			}
      
      dtime = DGCgettime()-fullstarttime;			
			
			if (senddebug){			
				totdtime+=dtime;
				count++;
				if (dtime>peakdtime) peakdtime=dtime;

				if (count%everyn==0){
					avgdtime = (double)totdtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,12);
					debugEl.setLabel(0,"Avg update time ", avgdtime);
					debugEl.setLabel(1,"Peak update time ", peakdtime);
					debugEl.setLabel(2,"Update count ", count);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totdtime = 0;
					peakdtime = 0;
				}
			}

		}

  } else {

    while (true) {

      fullstarttime = DGCgettime();
      /*
        if (useRndfFrame){
        DGClockMutex(&m_localRndfOffsetMutex);
        m_localToRndfOffset = listeneroutput;
        DGCunlockMutex(&m_localRndfOffsetMutex);
        }
      */

      bytesRecv = m_mapElemTalker.recvMapElementBlock(&recvEl,1);

      if (bytesRecv>0) {
        if (recvEl.id.dat[0] == 10) {
          if (!(recvEl.type == ELEMENT_CLEAR))
            Console::increaseObstSize();
          else
            Console::increaseClearSize();
        }

        DGClockMutex(&m_localMapUpMutex);
#if QUEUE_MAPELEMENTS == 0
        m_localUpdateMap->addEl(recvEl);
#else
        if (frozen) {
          mapElements.push_back(recvEl);
        } else {
          for (unsigned int i = 0; i < mapElements.size(); i++) {
            m_localMap->addEl(mapElements[i]);
          }
          mapElements.clear();
          m_localUpdateMap->addEl(recvEl);
        }
#endif
        DGCunlockMutex(&m_localMapUpMutex);

      } else {
        Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
                          << bytesRecv << endl;
      }

      dtime = DGCgettime()-fullstarttime;			
			
			if (senddebug){			
				totdtime+=dtime;
				count++;
				if (dtime>peakdtime) peakdtime=dtime;

				if (count%everyn==0){
					avgdtime = (double)totdtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,12);
					debugEl.setLabel(0,"Avg update time ", avgdtime);
					debugEl.setLabel(1,"Peak update time ", peakdtime);
					debugEl.setLabel(2,"Update count ", count);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totdtime = 0;
					peakdtime = 0;
				}
			}

    }
  }
}

void TrafficStateEst::updateMap() {





	if (CmdArgs::mapper_use_internal){

		bool locked = true;
		// time to wait for a new map in usec
		int waittime = 1000000;
		uint64_t starttime;
		// set variable to trigger map copy in mapper thread
		DGClockMutex(&m_localMapUpMutex);
		m_mapCopyState = 1;
		DGCunlockMutex(&m_localMapUpMutex);
		
		starttime = DGCgettime();
		// wait for map copy in mapper thread
		while((int)(DGCgettime()-starttime) < waittime){
			DGClockMutex(&m_localMapUpMutex);
			if (m_mapCopyState==0){
				locked = false;
				DGCunlockMutex(&m_localMapUpMutex);
				break;
			}
			DGCunlockMutex(&m_localMapUpMutex);
			usleep(0);
		}
	}else{
#if QUEUE_MAPELEMENTS == 0
    DGClockMutex(&m_localMapUpMutex);
    m_localUpdateMap->copyMapData(m_localMap);
    DGCunlockMutex(&m_localMapUpMutex);
#else
    
    DGClockMutex(&m_localMapUpMutex);
    /* optmize by only copying the pointer and dumping the MapElement vector */
    m_localMap = m_localUpdateMap;
    for (unsigned int i = 0; i < mapElements.size(); i++) {
      m_localMap->addEl(mapElements[i]);
    }
    mapElements.clear();
    DGCunlockMutex(&m_localMapUpMutex);
#endif
    

  }
	UpdateState();
	m_localMap->setLocalToSiteOffset(m_state);

  m_localToGlobalOffset = m_localMap->getLocalToGlobalOffset();
  m_localToGlobalYawOffset = m_localMap->getLocalToGlobalYawOffset();

}



Map* TrafficStateEst::getMap() {
  return m_localMap;
}


bool TrafficStateEst::loadRNDF(string filename) {

  m_localUpdateMap->loadRNDF(filename);
  return m_localMap->loadRNDF(filename);
}


// Constants for obstacle checking
#if 1
  #define ALICE_BOX_FRONT 9.7
  #define ALICE_BOX_REAR  6.5 
  #define ALICE_BOX_SIDE  1.5
  #define VEHICLE_RADIUS  9.8152
#else
  #define ALICE_BOX_FRONT 9.7112 // VEHICLE_LENGTH + DIST_REAR_AXLE_TO_FRONT
  #define ALICE_BOX_REAR  6.5956 // VEHICLE_LENGTH + DIST_REAR_TO_REAR_AXLE 
  #define ALICE_BOX_SIDE  2.0668 // VEHICLE_WIDTH/2 + 1
  #define VEHICLE_RADIUS  9.9287 // sqrt(VEHICLE_BOX_FRONT^2+ALICE_BOX_SIDE^2)
#endif


// Update graph with sensed obstacles from the map
int TrafficStateEst::updateGraphStateProblem(PlanGraph *graph, StateProblem_t &problem)
{
  Map *map;
  PlanGraphNode *node;
  MapElement mapEl;
  point2 obs_pt, point;
  point2arr new_geom, bounds;
  LaneLabel node_lane;
  float x_min, x_max, y_min, y_max, x, y, bx_min, bx_max, by_min, by_max;
  float mean_x, mean_y, size_x, size_y;
  float alice_rear, alice_front, alice_side_left, alice_side_right;
  point2arr alice_box;
  float bb_x, bb_y, bb_radius, tmp;
  float m[3][3];
  bool collide, obs_in_lane;
  
  PlanGraphNodeList nodes;

  unsigned long long time1 = 0;
  unsigned long long time2;

  // Use the map copy
  map = m_localMap;
  
  nodes.reserve(200);
  markedNodes.reserve(200);

  // Reset the collision flags for all nodes
  DGCgettime(time1);
  for (unsigned int i = 0; i < markedNodes.size(); i++)
  {
    node = markedNodes[i];
    node->status.collideObs = false;
    node->status.collideCar = false;
  }
  markedNodes.clear();
  DGCgettime(time2);
  Log::getStream(9) << "  Resetting nodes: " << (time2-time1)/1000.0 << " ms" << endl;

  // Check each node against each object in the map for a possible
  // collision.
  Log::getStream(1) << "  Number of obstacle in the map: " << map->usedIndices.size() << endl;
  Console::updateMapSize(map->usedIndices.size());

  for (int j = 0; j < (int)map->usedIndices.size(); j++)
  {
    // mapEl = &map->data[j];
    map->getFusedEl(mapEl,j);

    if (!mapEl.isObstacle())
      continue;

    if (mapEl.geometry.size() == 0)
      continue;
      
    Log::getStream(1) << " Element: " << mapEl.geometry.size() << " " << mapEl.id << endl;  
    
    // Build bounding boxes
    DGCgettime(time1);
    x_min = y_min = INFINITY;
    x_max = y_max = -INFINITY;
    for (unsigned int k=0; k<mapEl.geometry.size(); k++)
    {
      obs_pt.set(mapEl.geometry[k]);
      if (obs_pt.x < x_min) x_min = obs_pt.x;
      if (obs_pt.y < y_min) y_min = obs_pt.y;
      if (obs_pt.x > x_max) x_max = obs_pt.x;
      if (obs_pt.y > y_max) y_max = obs_pt.y;
    }
    // Create the big bounding box
    bx_min = x_min - VEHICLE_RADIUS;
    bx_max = x_max + VEHICLE_RADIUS;
    by_min = y_min - VEHICLE_RADIUS;
    by_max = y_max + VEHICLE_RADIUS;

    // Clip to ensure it doesn't get too large
    mean_x = (bx_max + bx_min)/2;
    size_x = (bx_max - bx_min)/2;
    mean_y = (by_max + by_min)/2;
    size_y = (by_max - by_min)/2;
    size_x = MIN(size_x, 30.0); // MAGIC
    size_y = MIN(size_y, 30.0); // MAGIC    
    bx_min = mean_x - size_x;
    bx_max = mean_x + size_x;
    by_min = mean_y - size_y;
    by_max = mean_y + size_y;
    
    DGCgettime(time2);
    Log::getStream(9) << "  Find min, max time: " << (time2-time1)/1000.0 << " ms" << endl;

#define SEND_MAPVIEWER 0
#if SEND_MAPVIEWER
    /* DEBUG */
    point2arr obs_points;
    obs_points.push_back(point2(bx_min, by_min));
    obs_points.push_back(point2(bx_max, by_min));
    obs_points.push_back(point2(bx_max, by_max));
    obs_points.push_back(point2(bx_min, by_max));
    MapElement me;
    me.setId(50000+2*j);
    me.setTypePoly();
    me.setColor(MAP_COLOR_ORANGE,100);
    me.setGeometry(obs_points);
    meTalker.sendMapElement(&me,-2);
#endif

    // Get the nodes that might collide with the obstacle
    graph->getRegion(&nodes, (bx_min + bx_max)/2, (by_min + by_max)/2, (bx_max - bx_min), (by_max - by_min));

    Log::getStream(1) << "  Bounding box: " << bx_min << bx_max << by_min << by_min << endl;  
    Log::getStream(1) << "  Number of nodes in the map: " << nodes.size() << endl;
        
#if SEND_MAPVIEWER
    obs_points.clear();
    for (unsigned int i=0; i<nodes.size(); i++)
    {
      obs_points.push_back(point2(nodes[i]->pose.pos.x, nodes[i]->pose.pos.y));
    }
    me.setId(50000+2*j+1);
    me.setTypePoints();
    me.setColor(MAP_COLOR_ORANGE,50);
    me.setGeometry(obs_points);
    meTalker.sendMapElement(&me,-2);
#endif
    
    double t1, t2, t3, t4;
    t1 = t2 = t3 = t4 = 0;
    unsigned long long a1, a2;

    // Check each node
    for (unsigned int i = 0; i < nodes.size(); i++)
    {
      collide = false;
      node = nodes[i];

      // Ignore node that already collided (give priority to vehicle collision)
      if (node->status.collideCar)
        continue;
      if (mapEl.isObstacle() && !mapEl.isVehicle() && node->status.collideObs)
        continue;

      // Check against big bounding box
      x = node->pose.pos.x;
      y = node->pose.pos.y;
      if (x < bx_min || x > bx_max || y < by_min || y > by_max)
        continue;

      // Transform small bounding box circle into node coordinates
      DGCgettime(a1);
      pose2f_to_mat33f(pose2f_inv(node->pose), m);
      bb_radius = sqrt(pow((x_max-x_min)/2,2)+pow((y_max-y_min)/2,2));
      bb_x = (x_min+x_max)/2;
      bb_y = (y_min+y_max)/2;
      tmp  = m[0][0] * bb_x + m[0][1] * bb_y + m[0][2];
      bb_y = m[1][0] * bb_x + m[1][1] * bb_y + m[1][2];
      bb_x = tmp;
      DGCgettime(a2);
      t1 += (a2-a1)/1000.0;

      // check against unrestricted bounding box around bounding circle
      if (ALICE_BOX_FRONT < bb_x - bb_radius || -ALICE_BOX_REAR > bb_x + bb_radius ||
          ALICE_BOX_SIDE < bb_y - bb_radius || -ALICE_BOX_SIDE > bb_y + bb_radius)
        continue;

      DGCgettime(a1);
#if 0
      // restrict alice box according to current situation of the node
      obs_in_lane = false;
      node_lane.segment = node->segmentId;
      node_lane.lane = node->laneId;
      // Check if obstacle in the same lane
      if (node->type & GRAPH_NODE_LANE)
      {
        if (map->getLaneBoundsPoly(bounds, node_lane) < 0)
        {
          obs_in_lane = true;
        }
        else
        {
          obs_in_lane = mapEl.isOverlap(bounds);
        }
      }
#endif
      obs_in_lane = true;
      // Be conservative only in lanes
      if (!(node->flags.isLane) || !(node->flags.isLane) || !obs_in_lane)
      {
        // If the node is not in a lane or the obstacle is not in the same
        // lane than the node then use the 1m separation distance
        alice_front = DIST_REAR_AXLE_TO_FRONT + 1.0;
        alice_rear = DIST_REAR_TO_REAR_AXLE + 1.0;
        alice_side_left = VEHICLE_WIDTH/2.0 + 1.0;
        alice_side_right = alice_side_left;
      }
      else
      {
        // get distance to next turn node
        double dist = 1.0 + DIST_REAR_AXLE_TO_FRONT;

        /* TESTING
        PlanGraphNode *prev_node = node;
        do
        {
          PlanGraphNode *next_node = NULL;

          // Find the next node in the lane.
          for (int k = 0; k < prev_node->numNext; k++)
          {
            next_node = prev_node->next[k];
            if (next_node->flags.isLane)
              break;
          }
          
          if (next_node == NULL || (next_node->flags.isTurn))
            break;
          dist += sqrt(pow(prev_node->pose.pos.x - next_node->pose.pos.x,2) +
                       pow(prev_node->pose.pos.y - next_node->pose.pos.y,2));
          prev_node = next_node;
        }
        while (dist < ALICE_BOX_FRONT);
        */
        
        // set front
        alice_front = MIN(dist, ALICE_BOX_FRONT);
        // set rear
        alice_rear = ALICE_BOX_REAR;
        // set side
        if (node->railId > 0)
        {
          alice_side_right = VEHICLE_WIDTH/2.0 + 0.2;
          alice_side_left = ALICE_BOX_SIDE;
        }
        else if (node->railId < 0)
        {
          alice_side_left = VEHICLE_WIDTH/2.0 + 0.2;
          alice_side_right = ALICE_BOX_SIDE;
        }
        else
        {
          float laneWidth;
          if (node->nextWaypoint)
            laneWidth = node->nextWaypoint->laneWidth;
          else if (node->prevWaypoint)
            laneWidth = node->prevWaypoint->laneWidth;
          else
            laneWidth = 4.0; // MAGIC
          alice_side_left = alice_side_right = MIN(laneWidth/2.0 + 0.2, ALICE_BOX_SIDE);
        }
      }
      DGCgettime(a2);
      t2 += (a2-a1)/1000.0;

      // Transform obstacle into node coordinates
      DGCgettime(a1);
      new_geom.clear();
      for (unsigned int k=0; k<mapEl.geometry.size(); k++)
      {
        obs_pt.set(mapEl.geometry[k]);
        point.x = m[0][0] * obs_pt.x + m[0][1] * obs_pt.y + m[0][2];
        point.y = m[1][0] * obs_pt.x + m[1][1] * obs_pt.y + m[1][2];
        new_geom.push_back(point);

        // Check point with alice bounding box
        if (point.x > -alice_rear && point.x < alice_front &&
            point.y > -alice_side_right && point.y < alice_side_left)
        {
          collide = true;
          break;
        }
      }
      DGCgettime(a2);
      t3 += (a2-a1)/1000.0;

      // Check for intersection between bounding box and obstacle (worst case scenario)
      DGCgettime(a1);
      if (!collide)
      {
        new_geom.push_back(new_geom[0]);

        // populate alice box
        alice_box.clear();
        alice_box.push_back(point2(alice_front, alice_side_left));
        alice_box.push_back(point2(alice_front, -alice_side_right));
        alice_box.push_back(point2(-alice_rear, -alice_side_right));
        alice_box.push_back(point2(-alice_rear, alice_side_left));
        alice_box.push_back(point2(alice_front, alice_side_left));
        

        collide = new_geom.is_intersect(alice_box);
      }
      DGCgettime(a2);
      t4 += (a2-a1)/1000.0;

      if (mapEl.isObstacle() && !mapEl.isVehicle())
      {
        node->status.collideObs = collide;
        markedNodes.push_back(node);
      }
      else if (mapEl.isVehicle())
      {
        node->status.collideCar = collide;
        markedNodes.push_back(node);
      }
    }

#if 1
    Log::getStream(9) << "  Transform bounding box execution time: " << t1/nodes.size() << " ms" << endl;
    Log::getStream(9) << "  Changing alice box execution time: " << t2/nodes.size() << " ms" << endl;
    Log::getStream(9) << "  Simple overlap execution time: " << t3/nodes.size() << " ms" << endl;
    Log::getStream(9) << "  Complex overlap execution time: " << t4/nodes.size() << " ms" << endl;
#endif
  }

  return GU_OK;
}



#if USE_FUSED

// TODO Use correct error/message handling
// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Initialize the fused perceptor
int TrafficStateEst::initFused(const char *spreadDaemon, int skynetKey)
{  
  // Create perceptor
  this->fused = new FusedPerceptor();
  assert(this->fused);

  // HACK FIX
  if (this->fused->initLive(spreadDaemon, skynetKey, MODfusedviewer + 16) != 0)
  {
    delete this->fused;
    this->fused = NULL;
    return ERROR("unable to initialize fused perceptor");
  }

  // Create the mutex
  pthread_mutex_init(&this->fusedMutex, NULL);
  
  // Create the thread
  MSG("starting fused perceptor thread");
  if (pthread_create(&this->fusedThread, NULL, (void*(*)(void*)) mainFused, this) != 0)
    return -1; // TODO 
  
  return 0;
}


// Finalize the fused perceptor
int TrafficStateEst::finiFused()
{
  if (!this->fused)
    return 0;
  
  // Stop the thread
  MSG("cancelling fused perceptor thread");
  pthread_cancel(this->fusedThread);
  pthread_join(this->fusedThread, NULL);
  MSG("cancelled fused perceptor thread");
   
  // Clean up
  pthread_mutex_destroy(&this->fusedMutex);
  this->fused->fini();
  delete this->fused;
  this->fused = NULL;
  
  return 0;
}


// Lock the fused perceptor and return a pointer
FusedPerceptor *TrafficStateEst::lockFused()
{
  pthread_mutex_lock(&this->fusedMutex);
  return this->fused;
}


// Unlock the fused perceptor
FusedPerceptor *TrafficStateEst::unlockFused()
{
  pthread_mutex_unlock(&this->fusedMutex);
  return NULL;
}


// Main loop for fused perceptor
int TrafficStateEst::mainFused(TrafficStateEst *self)
{
  FusedPerceptor *fused = self->fused;

  if (!fused)
    return 0;

  MSG("running fused perceptor thread");
  while (true)
  {
    pthread_testcancel();

    // Wait for new data.  The thread cancel call will force this to
    // exit if we are currently blocked.
    if (sensnet_wait(self->fused->sensnet, -1) == 0)
    {
      pthread_mutex_lock(&self->fusedMutex);
      fused->updateLive();
      pthread_mutex_unlock(&self->fusedMutex);
      //MSG("fused time %.3f", (double) fused->state.timestamp * 1e-6);
    }
  }
  
  return 0;
}


// Update the graph with data from the fused perceptor
int TrafficStateEst::updateFusedGraph(PlanGraph *graph, const VehicleState *state)
{
  int i;
  PlanGraphNode *node;
  PlanGraphNodeList nodes;
  float m[4][4];
  float lx, ly, px, py, size;

  // Lock access to the perceptor.  This is probably not necessary,
  // but is done for the sake of good form.
  pthread_mutex_lock(&this->fusedMutex);

  // Prepare the cost map.  
  this->fused->prepare(state);

  // Size of the local map (no point looking outside this region)
  size = this->fused->localMap->size * this->fused->localMap->scale;

  // Get the transform from local to site frame
  mat44f_setf(m, this->fused->transSL);

  // Compute the region of interest in the site frame
  lx = this->fused->localMap->pose.pos.x;
  ly = this->fused->localMap->pose.pos.y;
  px = m[0][0]*lx + m[0][1]*ly + m[0][3];
  py = m[1][0]*lx + m[1][1]*ly + m[1][3];

  MSG("getting quadtree %.3f %.3f %.3f %.3f",
      px - size/2, px + size/2, py - size/2, py + size/2);
  
  // Get all the nodes in the local region
  graph->getRegion(&nodes, px, py, size);
  MSG("got %d of %d nodes", (int) nodes.size(), graph->nodeCount);
  
  // Update each node
  for (i = 0; i < (int) nodes.size(); i++)
  {    
    node = nodes[i];
    node->status.lineDist = this->fused->calcLineDist(node->pose.pos.x,
                                                      node->pose.pos.y,
                                                      node->pose.rot);
    //MSG("lineDist %f %f %f", node->pose_x, node->pose_y, lineDist);
  }

  // Unlock access to the perceptor
  pthread_mutex_unlock(&this->fusedMutex);
  
  return 0;
}

#endif
