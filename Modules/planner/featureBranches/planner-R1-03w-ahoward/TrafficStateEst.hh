/*!
 * \file TrafficStateEst.hh
 * \brief Some legacy code from the tplanner. Maintains the local map and the vehicle state. Needs to be incorporated into the planner.
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef TRAFFICSTATEEST_HH_
#define TRAFFICSTATEEST_HH_

#include <vector>
#include <deque>
#include <map/Map.hh>
#include <mapper/Mapper.hh>
#include <interfaces/VehicleState.h>
#include <skynettalker/StateClient.hh>
#include <map/MapElementTalker.hh>
#include <fused-perceptor/FusedPerceptor.hh>
#include <temp-planner-interfaces/Graph.hh>
// REMOVE #include <interfaces/LeadingVehicleInfo.hh>


class TrafficStateEst : public CStateClient
{
  protected: 

  /*! The constructors are protected since we want a singleton */
  TrafficStateEst(bool waitForStateFill);
  TrafficStateEst(const TrafficStateEst&);
  ~TrafficStateEst();

  public:

  /*! Get the singleton instance */
  static TrafficStateEst* Instance(bool waitForStateFill);

  /*! Free the singleton instance */
  static void Destroy();

  /*! You are not allowed to copy a singleton */
  TrafficStateEst& operator=(const TrafficStateEst&);

  /*! Get the current vehicle state. */
  void updateVehState(const Graph_t *graph);

  /*! Get the vehicle state at the last estimate.  
   */
  VehicleState getVehState();

  /*! Get map at last estimate */
  Map* getMap(); 

  void updateMap();

  /*! Gets a local map update thread */
  void getLocalMapUpdate();
  void freezeMap();
  void meltMap();
      
  private : 

  /*! Get singleton instance */
  static TrafficStateEst* pinstance;

  /*! Get the local to global map update. */
  void getLocalGlobalMapUpdate();

  bool loadRNDF(string filename);

  bool frozen;

  /*! The current vehicle state  */
  VehicleState m_currVehState;

  /*! The updated vehicle state  */
  VehicleState m_updatedVehState;

  /*! The map element talker to receive map updates*/
  CMapElementTalker m_mapElemTalker;

  /*! The map getting updated */
  Map* m_localUpdateMap;

  /*! The map to read  */
  Map* m_localMap;

  /*! The queue of MapElements received since the last map snapshot */
  vector<MapElement> mapElements;

  /*! The local mapper object  */
  Mapper* m_mapper;


  /*! The current map */
  pthread_mutex_t m_localMapUpMutex;
  /*! The current RNDF frame offset mutex */
  pthread_mutex_t m_localRndfOffsetMutex;

  /*! The local to global delta */
  point2 m_localToGlobalOffset;
	double m_localToGlobalYawOffset;

	point2 m_localToRndfOffset;
  
  /*! used for mapper internal
    0 - no copy, 1 - copy requested */
  int m_mapCopyState;

  public:

  /// Initialize the fused perceptor; starts an internal thread.
  int initFused(const char *spreadDaemon, int skynetKey);

  /// Finalize the fused perceptor; stops the internal thread.
  int finiFused();

  /// Update the graph with data from the fused perceptor
  int updateFusedGraph(Graph_t *graph);

  /// Lock the fused perceptor and return a pointer
  FusedPerceptor *lockFused();

  /// Unlock the fused perceptor and return a null pointer
  FusedPerceptor *unlockFused();

  private:
  
  /// Main loop for fused perceptor
  static int updateFused(TrafficStateEst *self);

  // Recursively update graph quads
  int updateFusedQuad(Quadtree *quad, float px, float py, float size);

  private:

  // Fused perceptor
  FusedPerceptor *fused;
  
  // Thread control
  pthread_t fusedThread;
  pthread_mutex_t fusedMutex;
};

#endif /*TRAFFICSTATEEST_HH_*/
