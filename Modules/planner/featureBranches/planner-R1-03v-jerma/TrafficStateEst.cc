/*!
 * \file TrafficStateEst.cc
 * \brief Some legacy code from the tplanner. Maintains the local map and the vehicle state. Needs to be incorporated into the planner.
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include "TrafficStateEst.hh"
#include <interfaces/sn_types.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Console.hh>

#define QUEUE_MAPELEMENTS 0

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(bool waitForStateFill) 
  : CSkynetContainer(MODtrafficplanner, CmdArgs::sn_key)
  , CStateClient(waitForStateFill)
{
  DGCcreateMutex(&m_localMapUpMutex);
	DGCcreateMutex(&m_localRndfOffsetMutex);

  if (CmdArgs::use_RNDF){
    Log::getStream(1)<<"I am using the RNDF"<<endl;
    if (!CmdArgs::RNDF_file.empty()){
      Log::getStream(1)<<"RNDF is not empty "<<endl;
			

			//Check if the internal mapper is being used
			//if (!CmdArgs::mapper_use_internal){
      m_mapElemTalker.initSendMapElement(CmdArgs::sn_key);
      m_mapElemTalker.initRecvMapElement(CmdArgs::sn_key, 1);
			//}
    
    } else {
      Log::getStream(1)<<"TFEST: RNDFfilename is empty"<<endl;
    }
  } else {
    Log::getStream(1)<<"TFEST: not using RNDF"<<endl;
  }

	UpdateState();
	if (CmdArgs::mapper_use_internal){

		m_mapper = new Mapper();
		assert(m_mapper);
  }






  m_localMap = new Map(!CmdArgs::mapper_disable_line_fusion);
	m_localUpdateMap = new Map(!CmdArgs::mapper_disable_line_fusion);
  m_localMap->init();
  m_localUpdateMap->init();

  frozen = false;
  mapElements.reserve(200);
	
	loadRNDF(CmdArgs::RNDF_file);
	
	UpdateState();
	m_localMap->setLocalToGlobalOffset(m_state);
	DGClockMutex(&m_localMapUpMutex);
	m_localUpdateMap->setLocalToGlobalOffset(m_state);
	DGCunlockMutex(&m_localMapUpMutex);
}

TrafficStateEst::~TrafficStateEst() 
{
  delete m_localMap;
	delete m_localUpdateMap;	
	if (CmdArgs::mapper_use_internal){
		delete m_mapper;
	}
		
  DGCdeleteMutex(&m_localMapUpMutex);
}


TrafficStateEst* TrafficStateEst::Instance(bool waitForStateFill)
{
  if (pinstance == 0)
    pinstance = new TrafficStateEst(waitForStateFill);
  return pinstance;
}

void TrafficStateEst::Destroy()
{
  delete pinstance;
  pinstance = 0;
}

void TrafficStateEst::updateVehState() 
{
  UpdateState();
  m_currVehState = m_state; 

  if (CmdArgs::use_rndf_frame){
    DGClockMutex(&m_localRndfOffsetMutex);
    m_currVehState.localX += m_localToRndfOffset.x;
    m_currVehState.localY += m_localToRndfOffset.y;
    DGCunlockMutex(&m_localRndfOffsetMutex);


  }
}

VehicleState TrafficStateEst::getVehState() 
{
  return m_currVehState;
}

void TrafficStateEst::freezeMap()
{
  frozen = true;
}

void TrafficStateEst::meltMap()
{
  frozen = false;
}

void TrafficStateEst::getLocalMapUpdate()
{
  MapElement recvEl;
  int bytesRecv;
  Log::getStream(1) << "In local map update ..." <<endl; 

  
	int retval;
	bool useRndfFrame = CmdArgs::use_rndf_frame;

  bool senddebug = true;
  MapElement debugEl;
  // send messages every nth time
	int everyn = 100;
  uint64_t starttime;
  uint64_t fullstarttime;

// timing variables for full update loop
	int dtime;
	int peakdtime=0;;
	double avgdtime = 0;
	int totdtime;
	int count = 0;

  // timing variables for main mapping loop
	int maindtime;
	int peakmaindtime=0;;
	double avgmaindtime = 0;
	int totmaindtime;
	int maincount = 0;

  // timing variables for map copy 
	int copydtime;
	int totcopydtime = 0;
	double avgcopydtime;
	int peakcopydtime=0;
	int copycount=0;

	int zerocount = 0;
	int errorcount = 0;

	if (CmdArgs::mapper_use_internal){

		m_mapper->initComm(CmdArgs::sn_key);
    m_mapper->enableGroundstrikeFiltering(CmdArgs::mapper_enable_groundstrike_filtering);	
		m_mapper->disableLineFusion(CmdArgs::mapper_disable_line_fusion);
		m_mapper->enableLineFusionCompat(CmdArgs::mapper_line_fusion_compat);
		m_mapper->disableObsFusion(CmdArgs::mapper_disable_obs_fusion);
		m_mapper->decayAgeThresh = CmdArgs::mapper_decay_thresh;
    m_mapper->init(CmdArgs::RNDF_file);
    m_mapper->setLocalFrameOffset(m_state);

    m_mapper->plannerOutputDisabled = true;
    m_mapper->debugSubGroup = CmdArgs::mapper_debug_subgroup;


		while (true) {

      fullstarttime = DGCgettime();

      //if (useRndfFrame){
      // DGClockMutex(&m_localRndfOffsetMutex);
        // m_localToRndfOffset = listeneroutput;
      //  DGCunlockMutex(&m_localRndfOffsetMutex);    
      //}
		


	
      
      starttime = DGCgettime();
			retval = m_mapper->mainLoop();
			maindtime = DGCgettime()-starttime;			
			
			if (senddebug){			
				totmaindtime+=maindtime;
				maincount++;
				if (maindtime>peakmaindtime) peakmaindtime=maindtime;

				if (maincount%everyn==0){
					avgmaindtime = (double)totmaindtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,8);
					debugEl.setLabel(0,"Avg main time ", avgmaindtime);
					debugEl.setLabel(1,"Peak main time ", peakmaindtime);
					debugEl.setLabel(2,"Main count ", maincount);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totmaindtime = 0;
					peakmaindtime = 0;
				}
			}

			if (retval < 0){
				if (senddebug){			
					errorcount++;
					if (errorcount%everyn==0){
						debugEl.clear();
						debugEl.setTypeDebug();
						debugEl.setId(-1,6);
						debugEl.setLabel(0,"error count ", errorcount);
						m_mapElemTalker.sendMapElement(&debugEl,-3);
					}
				}
				Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received error from mapper:updateMap = " << endl;
      }else if (retval >0){ 
				DGClockMutex(&m_localMapUpMutex);
				if (m_mapCopyState !=0){
					starttime = DGCgettime();

          m_mapper->map.copyMapData(m_localMap);

					m_mapCopyState = 0;

					if (senddebug){			
						copydtime = DGCgettime()-starttime;		
						totcopydtime+=copydtime;
						copycount++;
						if (dtime>peakcopydtime) peakcopydtime=copydtime;
					
						if (copycount%everyn==0){
							avgcopydtime = (double)totcopydtime/(double)(everyn);
						
							debugEl.clear();
							debugEl.setTypeDebug();
							debugEl.setId(-1,2);
							debugEl.setLabel(0,"Avg copy time ", avgcopydtime);
							debugEl.setLabel(1,"Peak copy time ", peakcopydtime);
							debugEl.setLabel(2,"Copy count ", copycount);
							m_mapElemTalker.sendMapElement(&debugEl,-3);

							debugEl.clear();
							debugEl.setTypeDebug();
							debugEl.setId(-1,16);
							debugEl.setLabel(0,"map size ", (int)m_mapper->map.data.size());
							debugEl.setLabel(1,"used size ", (int)m_mapper->map.usedIndices.size());
							debugEl.setLabel(2,"unused size ", (int)m_mapper->map.openIndices.size());
							m_mapElemTalker.sendMapElement(&debugEl,-3);
					
							totcopydtime = 0;
							peakcopydtime = 0;
						}
					}
				}
				DGCunlockMutex(&m_localMapUpMutex);
      
			}else{
				if (senddebug){			
					zerocount++;
					if (zerocount%everyn==0){
						debugEl.clear();
						debugEl.setTypeDebug();
						debugEl.setId(-1,5);
						debugEl.setLabel(0,"zero count ", zerocount);
						m_mapElemTalker.sendMapElement(&debugEl,-3);
					}		
				}
			}
      
      dtime = DGCgettime()-fullstarttime;			
			
			if (senddebug){			
				totdtime+=dtime;
				count++;
				if (dtime>peakdtime) peakdtime=dtime;

				if (count%everyn==0){
					avgdtime = (double)totdtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,12);
					debugEl.setLabel(0,"Avg update time ", avgdtime);
					debugEl.setLabel(1,"Peak update time ", peakdtime);
					debugEl.setLabel(2,"Update count ", count);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totdtime = 0;
					peakdtime = 0;
				}
			}

		}

  } else {

    while (true) {

      fullstarttime = DGCgettime();
      /*
        if (useRndfFrame){
        DGClockMutex(&m_localRndfOffsetMutex);
        m_localToRndfOffset = listeneroutput;
        DGCunlockMutex(&m_localRndfOffsetMutex);
        }
      */

      bytesRecv = m_mapElemTalker.recvMapElementBlock(&recvEl,1);

      if (bytesRecv>0) {
        if (recvEl.id.dat[0] == 10) {
          if (!(recvEl.type == ELEMENT_CLEAR))
            Console::increaseObstSize();
          else
            Console::increaseClearSize();
        }

        DGClockMutex(&m_localMapUpMutex);
#if QUEUE_MAPELEMENTS == 0
        m_localUpdateMap->addEl(recvEl);
#else
        if (frozen) {
          mapElements.push_back(recvEl);
        } else {
          for (unsigned int i = 0; i < mapElements.size(); i++) {
            m_localMap->addEl(mapElements[i]);
          }
          mapElements.clear();
          m_localUpdateMap->addEl(recvEl);
        }
#endif
        DGCunlockMutex(&m_localMapUpMutex);

      } else {
        Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
                          << bytesRecv << endl;
      }

      dtime = DGCgettime()-fullstarttime;			
			
			if (senddebug){			
				totdtime+=dtime;
				count++;
				if (dtime>peakdtime) peakdtime=dtime;

				if (count%everyn==0){
					avgdtime = (double)totdtime/(double)(everyn);

					debugEl.clear();
					debugEl.setTypeDebug();
					debugEl.setId(-1,12);
					debugEl.setLabel(0,"Avg update time ", avgdtime);
					debugEl.setLabel(1,"Peak update time ", peakdtime);
					debugEl.setLabel(2,"Update count ", count);
					m_mapElemTalker.sendMapElement(&debugEl,-3);
					
					totdtime = 0;
					peakdtime = 0;
				}
			}

    }
  }
}

void TrafficStateEst::updateMap() {





	if (CmdArgs::mapper_use_internal){

		bool locked = true;
		// time to wait for a new map in usec
		int waittime = 1000000;
		uint64_t starttime;
		// set variable to trigger map copy in mapper thread
		DGClockMutex(&m_localMapUpMutex);
		m_mapCopyState = 1;
		DGCunlockMutex(&m_localMapUpMutex);
		
		starttime = DGCgettime();
		// wait for map copy in mapper thread
		while((int)(DGCgettime()-starttime) < waittime){
			DGClockMutex(&m_localMapUpMutex);
			if (m_mapCopyState==0){
				locked = false;
				DGCunlockMutex(&m_localMapUpMutex);
				break;
			}
			DGCunlockMutex(&m_localMapUpMutex);
			usleep(0);
		}
	}else{
#if QUEUE_MAPELEMENTS == 0
    DGClockMutex(&m_localMapUpMutex);
    m_localUpdateMap->copyMapData(m_localMap);
    DGCunlockMutex(&m_localMapUpMutex);
#else
    
    DGClockMutex(&m_localMapUpMutex);
    /* optmize by only copying the pointer and dumping the MapElement vector */
    m_localMap = m_localUpdateMap;
    for (unsigned int i = 0; i < mapElements.size(); i++) {
      m_localMap->addEl(mapElements[i]);
    }
    mapElements.clear();
    DGCunlockMutex(&m_localMapUpMutex);
#endif
    

  }

  m_localToGlobalOffset = m_localMap->getLocalToGlobalOffset();
  m_localToGlobalYawOffset = m_localMap->getLocalToGlobalYawOffset();

}



Map* TrafficStateEst::getMap() {
  return m_localMap;
}


bool TrafficStateEst::loadRNDF(string filename) {

  m_localUpdateMap->loadRNDF(filename);
  return m_localMap->loadRNDF(filename);
}


