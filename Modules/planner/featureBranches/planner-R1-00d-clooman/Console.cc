#include "Console.hh"
#include <string>
#include <iostream>
#include <unistd.h>
#include <stdarg.h>
#include "AliceStateHelper.hh"
#include "frames/point2.hh"
#include "map/Map.hh"
#include <logic-planner/LogicPlanner.hh>

/* Properties of state machine window */
#define FSM_WIN_X      0
#define FSM_WIN_Y      1
#define FSM_WIN_HEIGHT 6
#define FSM_WIN_WIDTH  40

/* Properties of the corridor window */
#define TRAJ_WIN_X      FSM_WIN_X
#define TRAJ_WIN_Y      (FSM_WIN_Y + FSM_WIN_HEIGHT)
#define TRAJ_WIN_HEIGHT 9
#define TRAJ_WIN_WIDTH  FSM_WIN_WIDTH

/* Properties of the state window */
#define STATE_WIN_X      (FSM_WIN_X + FSM_WIN_WIDTH)
#define STATE_WIN_Y      FSM_WIN_Y
#define STATE_WIN_HEIGHT 10
#define STATE_WIN_WIDTH  40

/* Properties of the inter window */
#define INTER_WIN_X      (FSM_WIN_X + FSM_WIN_WIDTH)
#define INTER_WIN_Y      (STATE_WIN_Y + STATE_WIN_HEIGHT)
#define INTER_WIN_HEIGHT 10
#define INTER_WIN_WIDTH  40

/* Properties of the message window */
#define MSG_WIN_X      FSM_WIN_X
#define MSG_WIN_Y      (INTER_WIN_Y + INTER_WIN_HEIGHT)
#define MSG_WIN_HEIGHT 8
#define MSG_WIN_WIDTH  (STATE_WIN_X - FSM_WIN_X + STATE_WIN_WIDTH)
#define MAX_MSG        (MSG_WIN_HEIGHT - 2)
#define MSG_LENGTH     (MSG_WIN_WIDTH-3)

WINDOW *Console::fsm_win = NULL;
WINDOW *Console::traj_win = NULL;
WINDOW *Console::state_win = NULL;
WINDOW *Console::inter_win = NULL;
WINDOW *Console::msg_win = NULL;
char Console::messages[MAX_MSG][MSG_LENGTH];
bool Console::initialized = false;

void Console::init()
{
  initialized = true;

  initscr();
  noecho();
  cbreak();
  curs_set(0);
  refresh();
    
  attron(A_BOLD);
  mvprintw(0, 0, " TPlanner%71c", ' ');
  attroff(A_BOLD);

  /* Create a window for the state machines */
  fsm_win = newwin(FSM_WIN_HEIGHT, FSM_WIN_WIDTH, FSM_WIN_Y, FSM_WIN_X);
  wborder(fsm_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(fsm_win);
  mvwprintw(fsm_win, 0, 2, " State Machines ");
  mvwprintw(fsm_win, 2, 2, "FSM State: ");
  mvwprintw(fsm_win, 3, 2, "FSM Flag: ");
  wrefresh(fsm_win);

  /* Create a window for the Trajectory */
  traj_win = newwin(TRAJ_WIN_HEIGHT, TRAJ_WIN_WIDTH, TRAJ_WIN_Y, TRAJ_WIN_X);
  wborder(traj_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(traj_win);
  mvwprintw(traj_win, 0, 2, " Trajectory ");
  attron(A_UNDERLINE);
  mvwprintw(traj_win, 2, 11, "Initial");
  mvwprintw(traj_win, 2, 23, "Final");
  attroff(A_UNDERLINE);
  mvwprintw(traj_win, 3, 9, "X");
  mvwprintw(traj_win, 4, 9, "Y");
  mvwprintw(traj_win, 5, 2, "Velocity");
  mvwprintw(traj_win, 6, 3, "Heading");
  wrefresh(traj_win);

  /* Create a window for the state */
  state_win = newwin(STATE_WIN_HEIGHT, STATE_WIN_WIDTH, STATE_WIN_Y, STATE_WIN_X);
  wborder(state_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(state_win);
  mvwprintw(state_win, 0, 2, " Alice State ");
  mvwprintw(state_win, 2, 2, "Position: ");
  mvwprintw(state_win, 3, 2, "Velocity: ");
  mvwprintw(state_win, 5, 2, "Desired Lane: ");
  mvwprintw(state_win, 6, 2, "Queueing: ");
  mvwprintw(state_win, 7, 2, "Turning: ");
  wrefresh(state_win);

  /* Create a window for the intersection */
  inter_win = newwin(INTER_WIN_HEIGHT, INTER_WIN_WIDTH, INTER_WIN_Y, INTER_WIN_X);
  wborder(inter_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(inter_win);
  mvwprintw(inter_win, 0, 2, " Intersection ");
  mvwprintw(inter_win, 2, 2, "Safety flag: ");
  mvwprintw(inter_win, 3, 2, "Cab mode flag: ");
  mvwprintw(inter_win, 4, 2, "Veh with precedence: ");
  mvwprintw(inter_win, 5, 2, "Legal to go: ");
  mvwprintw(inter_win, 6, 2, "Poss to go: ");
  mvwprintw(inter_win, 7, 2, "Clear to go: ");
  wrefresh(inter_win);

  /* Create a window for the messages */
  msg_win = newwin(MSG_WIN_HEIGHT, MSG_WIN_WIDTH, MSG_WIN_Y, MSG_WIN_X);
  wborder(msg_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(msg_win);
  mvwprintw(msg_win, 0, 2, " Messages ");
  wrefresh(msg_win);
  for (int i=0; i<MAX_MSG; i++) {
    messages[i][0] = '\0';
  }
}

void Console::refresh()
{
  if (!initialized) return;

  touchwin(stdscr);
  touchwin(fsm_win);
  touchwin(traj_win);
  touchwin(msg_win);
  touchwin(state_win);
  touchwin(inter_win);
  wrefresh(stdscr);
  wrefresh(fsm_win);
  wrefresh(traj_win);
  wrefresh(state_win);
  wrefresh(inter_win);
  wrefresh(msg_win);
}

void Console::destroy()
{
  if (!initialized) return;

  delwin(fsm_win);
  delwin(traj_win);
  delwin(state_win);
  delwin(msg_win);
  endwin();
}

void Console::updateTrajectory(CTraj *traj)
{
  int size = traj->getNumPoints();

  if (!initialized) return;
  if (size < 1) return;

  point2 initial(traj->getNorthing(0),traj->getEasting(0));
  point2 final(traj->getNorthing(size-1),traj->getEasting(size-1));

  double ivel = traj->getSpeed(0);
  double fvel = traj->getSpeed(size-1);

  double iheading = 0;
  double fheading = 0;

  mvwprintw(traj_win, 3, 11, "%7.2f", initial.x); /* Initial - X */
  mvwprintw(traj_win, 4, 11, "%7.2f", initial.y); /* Initial - Y */
  mvwprintw(traj_win, 5, 11, "%7.2f", ivel);      /* Initial - Velocity */
  mvwprintw(traj_win, 6, 11, "%7.2f", iheading);  /* Initial - Heading */
  mvwprintw(traj_win, 3, 23, "%7.2f", final.x);   /* Final - X */
  mvwprintw(traj_win, 4, 23, "%7.2f", final.y);   /* Final - Y */
  mvwprintw(traj_win, 5, 23, "%7.2f", fvel);      /* Final - Velocity */
  mvwprintw(traj_win, 6, 23, "%7.2f", fheading);  /* Final - Heading */
  wrefresh(traj_win);
}

void Console::updateState(VehicleState &vehState)
{
  if (!initialized) return;

  point2 pos = AliceStateHelper::getPositionRearAxle(vehState);
  double vel = AliceStateHelper::getVelocityMag(vehState);
  LaneLabel lane = AliceStateHelper::getDesiredLaneLabel();

  stringstream str;
  str << lane;

  mvwprintw(state_win, 2, 16, "[ %7.2f, %7.2f ]", pos.x, pos.y); /* Position */
  mvwprintw(state_win, 3, 16, "%.2f", vel);                      /* Velocity */
  mvwprintw(state_win, 5, 16, "%s", str.str().c_str());          /* Desired Lane */
  wrefresh(state_win);
}

void Console::updateInter(string SafetyFlag, string CabmodeFlag, int CountPrecedence, string LegalToGo, string PossibleToGo, string ClearToGo)
{
    if (!initialized) return;

    mvwprintw(inter_win, 2, 24, "%s", SafetyFlag.c_str() ); /* Safety flag set? */
    mvwprintw(inter_win, 3, 24, "%s", CabmodeFlag.c_str() ); /* Cabmode flag set? */
    mvwprintw(inter_win, 4, 24, "%i", CountPrecedence ); /* Vehicles with Precedence */
    mvwprintw(inter_win, 5, 24, "%s", LegalToGo.c_str() ); /* Is it legal to go? */
    mvwprintw(inter_win, 6, 24, "%s", PossibleToGo.c_str() ); /* Is it possible to go? */
    mvwprintw(inter_win, 7, 24, "%s", ClearToGo.c_str()); /* Is it clear to go? */
    wrefresh(inter_win);
}

void Console::updateQueueing(bool queueing)
{
  if (!initialized) return;

  mvwprintw(state_win, 6, 16, "%d", queueing);                  /* Queueing */
  wrefresh(fsm_win);
}

void Console::updateTurning(int direction)
{
  if (!initialized) return;

  if (direction == -1)
    mvwprintw(state_win, 7, 16, "<--");                  /* Turning */
  else if (direction == 1)
    mvwprintw(state_win, 7, 16, "-->");
  else
    mvwprintw(state_win, 7, 16, "   ");
  wrefresh(fsm_win);
}

void Console::updateFSMState(FSM_state_t state)
{
  if (!initialized) return;

  string str = LogicPlanner::stateToString(state);
  mvwprintw(fsm_win, 2, 17, "%-21s", str.c_str());       /* FSM State */
  wrefresh(fsm_win);
}

void Console::updateFSMFlag(FSM_flag_t flag)
{
  if (!initialized) return;

  string str = LogicPlanner::flagToString(flag);
  mvwprintw(fsm_win, 3, 17, "%-21s", str.c_str());      /* FSM flag */
  wrefresh(fsm_win);
}

void Console::addMessage(char *format, ...)
{
  static char msg[MSG_LENGTH];

  if (!initialized) return;

  va_list valist;
  va_start(valist, format);
  vsnprintf(msg, MSG_LENGTH, format, valist);
  va_end(valist);

  for (int i = 0; i < MAX_MSG; i++) {
    strncpy(messages[i], messages[i+1], MSG_LENGTH-1);
    messages[i][MSG_LENGTH-1] = '\0';
  }
  strncpy(messages[MAX_MSG-1], msg, MSG_LENGTH-1);
  messages[MAX_MSG-1][MSG_LENGTH-1] = '\0';
  for (int i=0; i<MAX_MSG; i++) {
    mvwprintw(msg_win, 1+i, 2, "%-76s", messages[i]);
  }
  wrefresh(msg_win);
}
