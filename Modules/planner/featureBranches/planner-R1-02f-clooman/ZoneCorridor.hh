/**********************************************************
 **
 **  ZONECORRIDOR.HH
 **
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 15:41:29 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef ZONECORRIDOR_HH
#define ZONECORRIDOR_HH

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <frames/point2.hh>
#include <map/Map.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
#include <frames/pose3.h>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <temp-planner-interfaces/Console.hh>


enum CostSide { LEFT,RIGHT,LEFT_AND_RIGHT };

struct PolygonParams
{
  float centerlaneVal;
  float obsCost;
  
  PolygonParams()
  {
    centerlaneVal = 0;
    obsCost = 10000;
  }
};


//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class ZoneCorridor
{  
public:
  static Err_t generateCorridor(CSpecs_t& cSpecs, point2arr zonePerimeter, vector<point2arr> parkingSpots, vector<MapElement> obstacles, Path_params_t params, pose3_t finPose, VehicleState vehState);

  static void findZoneObstacles(vector<MapElement>& obstacles, Map* map, point2 currPos, point2arr zonePerim);

  
private:
  
  /*! define the cost map ito polytopes */
  static void getBitmapParams(BitmapParams& bmparams, PolygonParams &polygonParams, point2 alicePos, point2arr zonePerimeter, vector<point2arr> parkingSpots, vector<MapElement> obstacles);

  /*! display some debugging info */
  static  void display(int sn_key, int sendSubgroup, point2arr zonePerimeter, CSpecs_t cSpecs);

  /*! print some debugging info */
  static void print(point2arr zonePerimeter, CSpecs_t cSpecs);

  /*! Define the bitmap parameters associated to the zone perimeter */
  static void assignPerimeterCost(vector<Polygon>& polygon, point2arr zonePerimeter, 
				  float xCoeff, float cost, float minVal, float width, CostSide side);

  /*! Define the bitmap parameters associated to parking spots. */
  static void assignParkingSpaceCost(vector<Polygon>& polygon, vector<point2arr> parkingSpots, 
				     float xCoeff, float cost, float minVal, float width, CostSide side);

  /*! Assigns a cost to an obstacle. */
  static void assignObstacleCost(vector<Polygon>& polygons, MapElement& el,
			  float xCoeff, float cost, float bval, float width, 
			  bitmap::FillFunc fillFunc, bitmap::CombineFunc combFunc);

  /*! Assigns a cost to a line. */
  static void assignCostToLine(vector<Polygon>& polygons, point2arr line,
			       float xCoeff, float cost, float bval, float width, CostSide side, 
			       bitmap::FillFunc fillFunc, bitmap::CombineFunc combFunc);
  

};
#endif
