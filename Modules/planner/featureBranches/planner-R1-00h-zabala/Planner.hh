/*!
 * \file Planner.hh
 * \brief Header for planner class
 *
 * \author Noel duToit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef PLANNER_HH_
#define PLANNER_HH_

#include <fstream>
#include <queue>
#include <interfaces/VehicleState.h>
#include <gcmodule/GcModule.hh>
#include <gcmodule/GcInterface.hh>
#include <gcinterfaces/SegGoals.hh>
#include <gcinterfaces/SegGoalsStatus.hh>
#include <gcinterfaces/AdriveCommand.hh>
#include <trajutils/TrajTalker.hh>
#include <frames/pose3.h>

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <graph-updater/GraphUpdater.hh>
#include <path-planner/PathPlanner.hh>
#include <vel-planner/VelPlanner.hh>
#include <logic-planner/LogicPlanner.hh>

#include "TrafficStateEst.hh"
#include "CmdArgs.hh"
#include "Console.hh"
#include "AliceStateHelper.hh"
#include "PlannerUtils.hh"
#include "Log.hh"

/*! Input interface from which SegGoals are received from the Route Planner */
typedef GcInterface<SegGoals, SegGoalsStatus, SNsegGoals, SNtplannerStatus, MODmissionplanner> MissionPlannerInterface;

class PlannerControlStatus : public ControlStatus
{
public:
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };

  /* The id of the merged directive that this control status corresponds to. */
  unsigned int ID; 
  Status status;
  ReasonForFailure reason;
  bool wasPaused;
};

class PlannerMergedDirective : public MergedDirective
{
public:
 
  PlannerMergedDirective(){};
  ~PlannerMergedDirective(){};

  list<int> segGoalsIDs; 
  SegGoals::SegmentType segType; 
  SegGoals::IntersectionType interType; 

  int exitSegmentID; 
  int exitLaneID; 
  int exitWaypointID;
 
};


struct PlannerMergedDirResp
{
  enum Status{ QUEUED, SENT, ACCEPTED, COMPLETED, FAILED };
  enum ReasonForFailure{ R1, R2, R3 };

  PlannerMergedDirResp()
    :status(QUEUED)
  {
  }

  Status status;
  ReasonForFailure reason;
};


class Planner : public GcModule {

public: 
  
  /*! Constructor */
  Planner();

  /*! Destructor */
  virtual ~Planner();

private :
  
  /*! Arbitration for the traffic planner control module. It computes the next
    merged directive based on the directives from mission control
    and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);
  
  /*! Control for the traffic planner control module. It computes and sends
    directives to all its controlled modules based on the 
    merged directive and outputs the control status
    based on all the status from its controlled modules. */
  void control(ControlStatus*, MergedDirective*);
  
  /*! Returns whether or not the goal is completed when there is an exit waypoint involved */
  bool isGoalComplete(PointLabel exitWayptLabel);

  /*! Returns the time of day */
  uint64_t getTime();

  /*! Output directive details to standard out */
  void printDirective(SegGoals* newDirective);

  /*!\param merged directive sent from arbiter to control */
  PlannerMergedDirective m_mergedDirective;

  /*!\param directives currently stored until planning horizon reqs are met */
  deque<SegGoals> m_accSegGoalsQ;

  /*!\Current SegGoals  */
  SegGoals m_currSegGoals;

  /*!\param control status sent from control to arbiter */
  PlannerControlStatus m_controlStatus;

  /*!\param GcInterface variable */
  MissionPlannerInterface::Northface* m_missTraffInterfaceNF;

  /*!\param GcInterface variable */
  AdriveCommand::Southface* m_traffAdriveInterfaceSF;

  bool m_isInit;

  /*!\Singleton intance of the Traffic State Estimator */
  TrafficStateEst* m_traffStateEst;

  bool m_completed;

  /*!\Adrive Directive  */
  AdriveDirective m_adriveDir; 

  Path_t m_path;
  Graph_t* m_graph;
  CTraj m_traj;
  CTrajTalker* m_trajTalker;

};

#endif /*PLANNER_HH_*/
