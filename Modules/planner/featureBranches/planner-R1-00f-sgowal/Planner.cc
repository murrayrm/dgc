#include "Planner.hh"

/**
 * @brief Constructor of the Planner class
 *
 * This constructor initializes the North and South faces of the gcmodule interface,
 * it activates the logging mechanism and sets the verbose level. It also starts
 * the TrafficStateEst class (which is a snapshot of the environment). Finally it
 * starts the different libraries that compose the planner module.
 */
Planner::Planner(const char *logFileName) 
  : GcModule("Planner", &m_controlStatus, &m_mergedDirective, 100000, 1000)
  , m_isInit(false)
  , m_completed(false)
{
  /* Initialize the northface */
  m_missTraffInterfaceNF = MissionPlannerInterface::generateNorthface(CmdArgs::sn_key, this);

  /* Initialize the southface */
  m_traffAdriveInterfaceSF = AdriveCommand::generateSouthface(CmdArgs::sn_key, this);

  /* Set the path to the log file */
  if (CmdArgs::logging) {
    Log::setGenericLogFile(logFileName);
  }

  /* Set the verbose level */
  Log::setVerboseLevel(CmdArgs::verbose_level);
  if (CmdArgs::console) {
    if (!CmdArgs::logging)
      Log::setVerboseLevel(0);
    Console::init();
    Console::addMessage("Debug = %d, Verbose Level = %d, Log = %d", CmdArgs::debug, CmdArgs::verbose_level, CmdArgs::logging);
  }

  /* Initializes the traffic state estimator */
  m_traffStateEst = TrafficStateEst::Instance(true);

  /* Activates the gcmodule logging mechanism */
  if (CmdArgs::debug) {
    ostringstream str;
    str << CmdArgs::log_path << "planner-gcmodule";
    this->setLogLevel(9);
    this->addLogfile(str.str().c_str());
  }

  /* Starts the map update thread */
  DGCstartMemberFunctionThread(m_traffStateEst, &TrafficStateEst::getLocalMapUpdate);
  
  /* Initialize traj */
  m_traj = CTraj(3);
  /* Initialize the graph-updater (graph-updater is responsible of the initialization of m_graph) */
  GraphUpdater::init(&m_graph, m_traffStateEst->getMap());
  /* Initialize the logic-planner */
  LogicPlanner::init();
  /* Initialize the path-planner */
  PathPlanner::init();
  /* Initialize the vel-planner */
  VelPlanner::init();

  /* Initialize traj talker (this is really our south face now) */
  m_trajTalker = new CTrajTalker(MODtrafficplanner, CmdArgs::sn_key);

}

/**
 * @brief Destructor
 *
 * It frees the memory used by the planner and its libraries
 */
Planner::~Planner()
{
  MissionPlannerInterface::releaseNorthface(m_missTraffInterfaceNF);
  TrafficStateEst::Destroy();
  Console::destroy();
  delete m_trajTalker;
  GraphUpdater::destroy(m_graph);
  LogicPlanner::destroy();
  PathPlanner::destroy();
  VelPlanner::destroy();
}

/**
 * @brief Arbitrate function of the gcmodule
 *
 * This function listens to the north face and gets messages from mplanner.
 * It populates a directive queue from the directive sent by mplanner and
 * respond to mplanner either by successfully reaching a goal and failing to do
 * so.
 */
void Planner::arbitrate(ControlStatus* cs, MergedDirective* md) {
  /* Refresh console every 20 cycles
   * Refreshing every cycle would create flickers on the
   * terminal (not very nice for the eye) */
  static int cnt_refresh = 0;
  if (cnt_refresh > 20) {
    cnt_refresh = 0;
    Console::refresh();
  }
  cnt_refresh++;

  /* Display the time needed to make a full planner cycle */
  static uint64_t old_time = getTime();
  uint64_t new_time = getTime();
  Log::getStream(4) << "Time consumed = " << (new_time - old_time)/(double)1000000 << " seconds" << endl;
  Console::updateRate((new_time - old_time)/(double)1000000);
  old_time = getTime();

  /* Grab the status and merged directive from the arguments */
  PlannerControlStatus *controlStatus = dynamic_cast<PlannerControlStatus *>(cs);
  PlannerMergedDirective *mergedDirective = dynamic_cast<PlannerMergedDirective *>(md);

  SegGoalsStatus segGoalsStatus;

  /* Send a directive completion for goal 0 to mplanner.
   * It tells mplanner that planner started and that it
   * can start sending directives */
  if (!m_isInit) {
    segGoalsStatus.goalID = 0;
    segGoalsStatus.status = SegGoalsStatus::COMPLETED;
    
    m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
    sleep(5);
    
    while (!m_missTraffInterfaceNF->haveNewDirective()) {    
      Log::getStream(1) << "TRFMGR: Sending Init Goal " << segGoalsStatus.goalID << endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
      sleep(5);
    }
    m_isInit = true;
  }

  /* Create the snapshot of the environment */
  m_traffStateEst->updateMap();
  m_traffStateEst->updateVehState();
  VehicleState vs = m_traffStateEst->getVehState();
  Console::updateState(vs);

  /* Printing that we are currently executing a goal */
  if (PlannerControlStatus::EXECUTING == controlStatus->status) {
    Log::getStream(1) << endl << endl;
    Log::getStream(1)<<"TRFMGR:  Executing Goal ID :"<<controlStatus->ID<<endl; 
  }
  
  /********************************************
   * Notification to Northface that we either *
   * failed or completed a directive          *
   ********************************************/

  /* Check on the status of the last command acted on by control */
  if (controlStatus->status == PlannerControlStatus::COMPLETED ||
      controlStatus->status == PlannerControlStatus::FAILED) {
    
    /* If Alice successfully reached the previous directive
     * notify mplanner */
    if (controlStatus->status == PlannerControlStatus::COMPLETED) {  
      segGoalsStatus.status = SegGoalsStatus::COMPLETED;
      segGoalsStatus.goalID = controlStatus->ID;
      m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );

      /* Display result */
      Log::getStream(1)<<"TRFMGR: GOAL ID COMPLETED "<<segGoalsStatus.goalID<<endl;
      Console::updateLastWpt(PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID));

      /* Pop the previous directive off the queue front (if the queue wasn't cleared meanwhile) */
      if (m_accSegGoalsQ.size() > 0 && m_accSegGoalsQ.front().goalID == segGoalsStatus.goalID){ 
        m_accSegGoalsQ.pop_front();
      } else {
        /* We don't have any directives on the queue */
        Log::getStream(1)<<"TRFMGR: waiting for more goals: m_accSegGoals.size()==0 "<<endl;
      }

    /* If Alice failed, flush the entire queue and notify mplanner */
    } else if (controlStatus->status == PlannerControlStatus::FAILED)  {
      /* Flush out the NF queue */
      while (m_missTraffInterfaceNF->haveNewDirective()) {
        SegGoals newDirective;
        m_missTraffInterfaceNF->getNewDirective( &newDirective );
        segGoalsStatus.goalID = newDirective.goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        Log::getStream(1)<<"TRFMGR: GOAL ID, FAILED (but not sending) "<<segGoalsStatus.goalID<<endl;
      }
      
      /* Flush out the accumulating segGoals control queue */
      while (m_accSegGoalsQ.size() > 0) {
        segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        Log::getStream(1)<<"TRFMGR: GOAL ID, FAILED (but not sending)"<<segGoalsStatus.goalID<<endl;
        m_accSegGoalsQ.pop_front();
      }

      /* Notify mplanner that we failed the current goal */
      segGoalsStatus.status = SegGoalsStatus::FAILED;
      segGoalsStatus.goalID = controlStatus->ID;
      Log::getStream(1)<<"TRFMGR: GOAL ID, FAILED (sending) "<<segGoalsStatus.goalID<<endl;
      m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
    }
    Log::getStream(1) << endl << endl;
  }

  /********************************************
   * Reponding to the Northface by populating *
   * our own directive queue                  *
   ********************************************/

  /* Get all the new directives and put them in the queue */
  while (m_missTraffInterfaceNF->haveNewDirective()) {

    SegGoals newDirective;
    m_missTraffInterfaceNF->getNewDirective(&newDirective);
    printDirective(&newDirective);

    /* If it is an old directive that mplanner sent us twice ignore it */
    if ((m_accSegGoalsQ.size()>0) && (newDirective.goalID <= m_accSegGoalsQ.back().goalID)) {
      continue;
    }

    /* Push the new directive on the queue */
    m_accSegGoalsQ.push_back(newDirective);

    /* A PAUSE directive preempt everything, the current queue is flushed */
    if (SegGoals::PAUSE == newDirective.segment_type) {
      SegGoalsStatus segGoalsStatus;
      
      /* First flush the queue where the seg goals are accumulating */
      while (SegGoals::PAUSE != m_accSegGoalsQ.front().segment_type) {
        segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        controlStatus->status = PlannerControlStatus::FAILED;
        Log::getStream(1)<<"TRFMGR: GOAL ID PAUSE,FAILED "<<segGoalsStatus.goalID<<endl;
        m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
        m_accSegGoalsQ.pop_front();
      }
      /* Break to answer as fast as possible to the PAUSE directive */
      break; 
    }
  }

  /* If we are currently not executing any goal, set the current goal to
   * the bottom most directive of the queue */
  if (m_accSegGoalsQ.size() > 0 && PlannerControlStatus::EXECUTING != controlStatus->status) {
    SegGoals newGoal =  m_accSegGoalsQ.front();

    /* PAUSE and END_OF_MISSION directive do not specify a exit waypoint */
    if (SegGoals::PAUSE == newGoal.segment_type || SegGoals::END_OF_MISSION == newGoal.segment_type) {
      mergedDirective->segType = newGoal.segment_type;
      mergedDirective->id = newGoal.goalID;
      m_currSegGoals = newGoal;
    /* if UNKNOWN fail right away */
    } else if (SegGoals::UNKNOWN == newGoal.segment_type) {
      segGoalsStatus.goalID = newGoal.goalID;
      mergedDirective->segType = newGoal.segment_type;
      segGoalsStatus.status = SegGoalsStatus::FAILED;
      Log::getStream(1)<<"TRFGMR: GOAL ID,UNKNOWN "<<segGoalsStatus.goalID<<endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
    /* Populates the exit waypoint */
    } else {
      mergedDirective->id = newGoal.goalID; 
      mergedDirective->segType = newGoal.segment_type;
      m_currSegGoals = newGoal;
      Console::updateNextWpt(PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID));
    }

  /* Nothing on the Northface */
  } else Log::getStream(1)<<"TRFMGR: Waiting for Mission Planner goal "<<endl;
}

/**
 * @brief Control function of the gcmodule
 *
 * This function generates the trajectory according
 * to the current goal
 */
void Planner::control(ControlStatus* cs, MergedDirective* md)
{
  static Err_t error = GU_OK | LP_OK | PP_OK | VP_OK;

  /* Grab the status and merged directive from the arguments */
  PlannerControlStatus* controlStatus = dynamic_cast<PlannerControlStatus *>(cs);
  PlannerMergedDirective* mergedDirective = dynamic_cast<PlannerMergedDirective *>(md);

  /* Initialize vehState (renaming) */
  VehicleState vehState = m_traffStateEst->getVehState();

  /* Completing a PAUSE or an END_OF_MISSION is just a matter of stopping */
  if (mergedDirective->segType==SegGoals::PAUSE || mergedDirective->segType==SegGoals::END_OF_MISSION) {
    m_completed = PlannerUtils::isStopped(vehState);
  /* Check if Alice completed the previous goal */
  } else {
    PointLabel currGoalEndpoint = PointLabel(m_currSegGoals.exitSegmentID,
                                             m_currSegGoals.exitLaneID,
                                             m_currSegGoals.exitWaypointID);
    m_completed = isGoalComplete(currGoalEndpoint);
  }

  /* If we completed, set the status to completed.
   * This will notify mplanner at the beginning of the
   * next arbitrate function call */
  if (m_completed) {
    controlStatus->ID = m_currSegGoals.goalID;
    controlStatus->status = PlannerControlStatus::COMPLETED;
    m_currSegGoals.segment_type = SegGoals::UNKNOWN;
    return;
  }

  /* Initialize cost (this is currently not used) */
  Cost_t cost;

  /* Initialize vector of state problems */
  vector<StateProblem_t> state_problems;

  /* Grab the actuator state (temporary solution) */
  ActuatorState actState;
  memset(&actState, 0, sizeof(actState));

  /* Execute logic planner */
  Logic_params_t logicParams;
  logicParams.segment_type = mergedDirective->segType;
  logicParams.seg_goal = m_currSegGoals;
  error = LogicPlanner::planLogic(state_problems, m_graph, error, vehState, m_traffStateEst->getMap(), logicParams);
  Console::updateFSMState(state_problems[0].state);
  Console::updateFSMFlag(state_problems[0].flag);

  /* Reset the error */
  error = 0;

  /* Update graph to account for the current car position */
  error |= GraphUpdater::genVehicleSubGraph(m_graph, vehState, actState);

  /* Update graph according to the new state problem */
  error |= GraphUpdater::updateGraphStateProblem(m_graph, state_problems[0], m_traffStateEst->getMap());

  /* Set the final pose based on the goal position */
  pose3_t finPose;
  point2 final;
  if (mergedDirective->segType==SegGoals::PAUSE || mergedDirective->segType==SegGoals::END_OF_MISSION) {
    final.set(vehState.localX, vehState.localY);
  } else {
    PointLabel currGoalEndpoint = PointLabel(m_currSegGoals.exitSegmentID,
                                             m_currSegGoals.exitLaneID,
                                             m_currSegGoals.exitWaypointID);
    m_traffStateEst->getMap()->getWaypoint(final, currGoalEndpoint);
  }
  double heading;
  m_traffStateEst->getMap()->getHeading(heading, final);
  finPose.pos.x = final.x;
  finPose.pos.y = final.y;
  finPose.pos.z = 0;
  finPose.rot = quat_from_rpy(0,0, heading);
  
  /* Plan the path */
  Path_params_t pathParams;
  pathParams.flag = state_problems[0].flag;
  error |= PathPlanner::planPath(&m_path, cost, m_graph, finPose, pathParams);
  
  /* Plan the trajectory */
  Vel_params_t velParams;
  error |= VelPlanner::planVel(&m_traj, &m_path, state_problems[0], vehState, velParams);

  /* print graph and path to default mapviewer channel */
  GraphUpdater::display(-2, m_graph, vehState);
  PathPlanner::display(-2, &m_path);
  Console::updateTrajectory(&m_traj);
  
  /* Send the trajectory */
  int trajSocket = m_trajTalker->m_skynet.get_send_sock(SNtraj);
  m_trajTalker->SendTraj(trajSocket, &m_traj);

  /* Set status to executing */
  controlStatus->status = PlannerControlStatus::EXECUTING;
}

/**
 * @brief Checks whether Alice has reached the exit waypoint
 *
 * Checks that the exit waypoint is less that a meter in front of Alice in
 * a 10 meter radius region.
 */
bool Planner::isGoalComplete(PointLabel exitWayptLabel)
{
  double completeDist = 3;
  bool completed = false;
  double angle, dotProd, AliceHeading, absDist, headingDiff; 

  Log::getStream(1)<<"PLANNER: isGoalComplete "<<endl;
  
  point2 exitWaypt, currPos;  
  m_traffStateEst->getMap()->getWaypoint(exitWaypt, exitWayptLabel);
  currPos = AliceStateHelper::getPositionFrontBumper(m_traffStateEst->getVehState());
    
  m_traffStateEst->getMap()->getHeading(angle, exitWayptLabel);
    
  Log::getStream(1) << "PLANNER:  exit waypoint label " << exitWayptLabel << endl;
  Log::getStream(1) << "PLANNER:  currPos " << currPos << endl;
  Log::getStream(1) << "PLANNER:  exit waypoint " << exitWaypt << endl;

  dotProd = (-exitWaypt.x+currPos.x)*cos(angle) + (-exitWaypt.y+currPos.y)*sin(angle);

  Log::getStream(1) << "PLANNER:  dot product  = " << dotProd << endl;
  
  AliceHeading = AliceStateHelper::getHeading(m_traffStateEst->getVehState());
  absDist = exitWaypt.dist(currPos);

  double anglePiMinusPi;
  PlannerUtils::addAngles(anglePiMinusPi,angle,-AliceHeading);
  headingDiff = anglePiMinusPi;

  if((dotProd>-completeDist) /* && (fabs(headingDiff)<M_PI/6) */ && (absDist<10)) {
    /* when we get within 1 m of the hyperplane defined by the exit pt, goal complete */
    completed = true;
  } else {
    completed = false;
  }

  return completed;
}

/**
 * @brief Gets the current time
 */
uint64_t Planner::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

/**
 * @brief Prints the new directive
 */
void Planner::printDirective(SegGoals* newDirective) 
{
  Log::getStream(1) << "PLANNER: New dir recvd, ID "<<newDirective->goalID<<",type "<<newDirective->segment_type;
  Log::getStream(1) << ", intersection type "<<newDirective->intersection_type;
  Log::getStream(1) << ", illegalPassingAllowed "<<newDirective->illegalPassingAllowed; 
  Log::getStream(1) << ", exit waypt ";
  Log::getStream(1) << newDirective->exitSegmentID<<"."<<newDirective->exitLaneID<<"."<<newDirective->exitWaypointID<<endl;
}
