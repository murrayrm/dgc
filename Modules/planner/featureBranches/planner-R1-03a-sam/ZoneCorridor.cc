/**********************************************************
 **
 **  ZONECORRIDOR.CC
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 17:23:25 2007
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <skynettalker/SkynetTalker.hh>
#include "temp-planner-interfaces/Log.hh"
#include "alice/AliceConstants.h"
#include <math.h>

int ZoneCorridor::m_prevZoneAction = TRAFFIC_STATE_UNSPECIFIED;

#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0:  0)

Err_t ZoneCorridor::setupCSpecs(CSpecs_t& cSpecs, Map* map, VehicleState vehState, SegGoals currSegGoals, StateProblem_t stateProblem, Path_params_t pathParams, pose3_t finPose)
{
  Err_t error = GU_OK | LP_OK | PP_OK | VP_OK | PLANNER_OK;
  //  unsigned long long time1, time2;   //  DGCgettime(time1);
  point2arr corridor;
  BitmapParams bmParams;
  PolygonParams polygonParams;
  bmParams.resX = 0.1;
  bmParams.resY = 0.1;
  bmParams.width = 800;
  bmParams.height = 800;
  bmParams.baseVal = 1 ;
  bmParams.outOfBounds = 100;
  polygonParams.centerlaneVal = 1;
  polygonParams.obsCost = 500;

  point2 initPos = AliceStateHelper::getPositionRearAxle(vehState);
  PointLabel exitLabel = PointLabel(currSegGoals.exitSegmentID, currSegGoals.exitLaneID, currSegGoals.exitWaypointID); 

  // Set up the corridor and paint the costs
  switch (stateProblem.region)
    {
    case ROAD_REGION:
      {
        /* Get the corridor*/
        
        /* Paint the cost*/

        break;
      }
    case ZONE_REGION:
      {
        pathParams.zoneId = currSegGoals.entrySegmentID;

        /* Determine what we plan to do in the zone based on exit point */
        Log::getStream(4)<<"ZONE: Exit label  "<<exitLabel<<endl;

        /* set the zone action*/ 
        cSpecs.setTrafficState(determineZoneAction(exitLabel, pathParams.zoneId, map));

        vector<SpotLabel> parkingLabels; 
        vector<point2arr> parkingSpots; 
        if (0 != pathParams.zoneId) {
          /* get the zone perimeter */
          map->getZonePerimeter(corridor, pathParams.zoneId);
          cSpecs.setBoundingPolygon(corridor);
  
          /* Paint the cost*/
          /* Get the parking spots */
          SpotLabel spotLabel; 
          point2 first, second; 
          point2arr spotWaypt; 
          
          map->getZoneParkingSpots(parkingLabels, pathParams.zoneId);
          
          for (unsigned int i =0; i<parkingLabels.size(); i++) {	
            map->getSpotWaypoints(first, second,parkingLabels[i]);
            spotWaypt.push_back(first);
            spotWaypt.push_back(second);
            parkingSpots.push_back(spotWaypt);
            spotWaypt.clear();
          }
        
          /* Get the obstacles in the zone */
          vector<MapElement> obstacles; 
          
          map->getObsInZone(obstacles, pathParams.zoneId);
          
          /* Convert the obstacles from map elements to point2arr and populate cSpecs */
          populatePolygonalObstacles(cSpecs, obstacles);
          
          // Actually paint the costs
          paintCostZone(bmParams, polygonParams, initPos, corridor, parkingSpots, obstacles);
          // Assign the cost map in cSpecs
          cSpecs.setCostMap(bmParams);
        }
        break;
      }
    case INTERSECTION:
      {
        /* Get the corridor*/
        
        /* Paint the cost*/

        break;
      }
    default:
      {
        Console::addMessage("ZONECORRIDOR: gen corridor: SHOULD NEVER GET HERE");
        Log::getStream(1) <<"ZONECORRIDOR: gen corridor: SHOULD NEVER GET HERE" << endl;
      }
    }

  // Set up initial, final conditions
  double startingState[4];
  startingState[0] = AliceStateHelper::getPositionRearAxle(vehState).x;
  startingState[1] = AliceStateHelper::getPositionRearAxle(vehState).y;
  startingState[2] = AliceStateHelper::getVelocityMag(vehState);
  startingState[3] = AliceStateHelper::getHeading(vehState);
  cSpecs.setStartingState(startingState);
  // starting controls
  double startingControls[2];
  startingControls[0] = AliceStateHelper::getAccelerationMag(vehState);
  startingControls[1] = 0;
  cSpecs.setStartingControls(startingControls);
  
  // final state
  double finalState[4];
  double roll, pitch, yaw;
  quat_to_rpy(finPose.rot, &roll, &pitch, &yaw);
  finalState[0] = finPose.pos.x - DIST_REAR_AXLE_TO_FRONT*cos(yaw);
  finalState[1] = finPose.pos.y - DIST_REAR_AXLE_TO_FRONT*sin(yaw);
  finalState[2] = 0.2; //dplanner requirement change to 0.1
  finalState[3] = yaw;
  cSpecs.setFinalState(finalState);
  // final controls
  double finalControls[2];
  finalControls[0] = 0;
  finalControls[1] = 0;
  cSpecs.setFinalControls(finalControls);
  
  //set maximums 
  cSpecs.setMaxVelocity(6);
  cSpecs.setMaxAcc(VEHICLE_MAX_ACCEL);
  cSpecs.setMaxBraking(VEHICLE_MAX_DECEL);
  cSpecs.setMaxSteeringAngle(VEHICLE_MAX_AVG_STEER);
  cSpecs.setMaxSteeringRate(M_PI/2);
  
    // Planner specific functions
  switch (stateProblem.planner)
    {
    case S1PLANNER:
    case CIRCLE_PLANNER:
    case DPLANNER:
      break;
    default:
      {
        Console::addMessage("ZONECORRIDOR: planner specific cspecs functions: SHOULD NEVER GET HERE");
        Log::getStream(1) <<"ZONECORRIDOR: planner specific cspecs functions: SHOULD NEVER GET HERE" << endl;
      }
    }
  
  // Dealing with obstacles in the different ways
  switch (stateProblem.obstacle)
    {
    case OBSTACLE_SAFETY:
      {
        double zone_safety_margin = 0.5;

        Log::getStream(7) << "ZONE: setting a safety margin of " << zone_safety_margin << " around Alice" << endl;
        cSpecs.setSafetyMargins(zone_safety_margin);
        break;
      }
    case OBSTACLE_AGGRESSIVE:
      {
        break;
      }
    case OBSTACLE_BARE:
      {
        break;
      }
    default:
      {
        Console::addMessage("ZONECORRIDOR: dealing with obstacles: SHOULD NEVER GET HERE");
        Log::getStream(1) <<"ZONECORRIDOR: dealing with obstacles: SHOULD NEVER GET HERE" << endl;
      }
    }
  
  return error;
}

void ZoneCorridor::paintCostZone(BitmapParams& bmparams, PolygonParams &polygonParams, point2 alicePos, point2arr zonePerimeter, vector<point2arr> parkingSpots, vector<MapElement> obstacles)
{
  bmparams.centerX = alicePos.x;
  bmparams.centerY = alicePos.y;
  
  bmparams.polygons.clear();
  float perimCost = 50; /* The max value of the perimeter cost */
  float perimXCoeff = -0.5;
  float perimWidth =  VEHICLE_WIDTH*2;  /* The width of the cost painting around the perimeter line */

  float pSpaceCost = 0; /* The max value of the parking space cost */
  float pSpaceXCoeff = 0; /* A coefficient for the parking space cost function  */
  float pSpaceWidth =  4;  /* The width of the cost painting around the parking space line FIX */

  float obsCost = polygonParams.obsCost; /* The max value of the obstacle cost */
  float obsXCoeff = -0.5;

  float minValPerim = 1;
  float minValPark = 40;
  float minValObs = 1;

  CostSide side = LEFT_AND_RIGHT; /* Paint the cost to the left and right of the line */

  assignPerimeterCost(bmparams.polygons,zonePerimeter, perimXCoeff, perimCost, minValPerim, perimWidth, side);
  assignParkingSpaceCost(bmparams.polygons,parkingSpots, pSpaceXCoeff, pSpaceCost, minValPark, pSpaceWidth, side);

  Log::getStream(9) << "ZoneCorridor: Number of obstacles=" << obstacles.size()<<endl;

  for (unsigned int i= 0; i < obstacles.size(); i++) {    
    assignObstacleCost(bmparams.polygons,obstacles[i], obsXCoeff, obsCost, minValObs, pSpaceWidth,FILL_EXP2,COMB_MAX); 
  }
}

void ZoneCorridor::display(int sn_key, int sendSubgroup, point2arr zonePerimeter, CSpecs_t cSpecs)
{
  CMapElementTalker meTalker;
  meTalker.initSendMapElement(sn_key);
  int counter=12000;
  point2 point;
  MapId mapId;
  MapElement me;
  
  mapId = counter;

  // display the corridor
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_BLUE, 100);
  me.setGeometry(zonePerimeter);
  meTalker.sendMapElement(&me,sendSubgroup);
  
  // print the initial and final cond's
  vector<double> startingState = cSpecs.getStartingState();
  vector<point2> points;
  point.set(startingState[0], startingState[1]);
  points.push_back(point);
  point.set(startingState[0]+cos(startingState[3]), startingState[1]+sin(startingState[3]));
  points.push_back(point);
  mapId = 12001;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_YELLOW, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

  // print the initial and final cond's
  vector<double> finalState = cSpecs.getFinalState();
  point.set(finalState[0], finalState[1]);
  points.push_back(point);
  point.set(finalState[0]+cos(finalState[3]), finalState[1]+sin(finalState[3]));
  points.push_back(point);
  mapId = 12002;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_RED, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

}

void ZoneCorridor::print(point2arr zonePerimeter, CSpecs_t cSpecs)
{
  vector<double> startingState = cSpecs.getStartingState();
  vector<double> finalState = cSpecs.getFinalState();
  vector<double> startingControls = cSpecs.getStartingControls();
  vector<double> finalControls = cSpecs.getFinalControls();
  Console::addMessage("Init state = (%6.2f,%6.2f,%6.2f,%6.2f)", startingState[0], startingState[1], startingState[2], startingState[3]);
  Console::addMessage("Init control = (%6.2f,%6.2f)", startingControls[0], startingControls[1]);
  Console::addMessage("Fin state = (%6.2f,%6.2f,%6.2f,%6.2f)", finalState[0], finalState[1], finalState[2], finalState[3]);
  Console::addMessage("Fin control = (%6.2f,%6.2f)", finalControls[0], finalControls[1]);
}


void ZoneCorridor::assignPerimeterCost(vector<Polygon>& polygons, point2arr zonePerimeter, float xCoeff,
				       float cost, float minVal, float width, CostSide side) 
{  

  /* Add the first point again to the end of zonePerimeter array */
  zonePerimeter.push_back(zonePerimeter[0]);
  assignCostToLine(polygons, zonePerimeter, xCoeff, cost, minVal, width, side, FILL_EXP2,COMB_MAX);
}

void ZoneCorridor::assignParkingSpaceCost(vector<Polygon>& polygons,vector<point2arr> parkingSpots, float xCoeff, float cost, float minVal, float width, CostSide side) 
{
  for ( unsigned int i =0; i < parkingSpots.size(); i++) {
    assignCostToLine(polygons, parkingSpots[i], xCoeff, cost, minVal, width, side, FILL_LINEAR,COMB_MAX);
  }
}

void ZoneCorridor::assignObstacleCost(vector<Polygon>& polygons, MapElement& el,
				      float xCoeff, float obsCost, float bval, float width, 
				      bitmap::FillFunc fillFunc, bitmap::CombineFunc combFunc) 
{

  Log::getStream(9) << "ZoneCorridor: About to assign obstacle cost " << endl;

  double sigma = 1*VEHICLE_WIDTH/2;
  //  if (el.type != ELEMENT_OBSTACLE && el.type != ELEMENT_VEHICLE) {
  if (!el.isObstacle()) {
    // this shouldn't happen, because it's checked in convertMapElementToPolygon()
    cerr << "CORRIDOR: FAILED ERROR: " << __FILE__ << ":" << __LINE__ << " cannot handle nonobstacle "
	 << el.type << " of map element" << endl;
    return; 
  }

  //  if (el.type == ELEMENT_VEHICLE) {
  if (el.isVehicle()) {
    Log::getStream(9) << "ZoneCorridor: el.type = ELEMENT_VEHICLE" << endl;
    //    return;
  }

  if (el.geometryType != GEOMETRY_POLY) {
    cerr << "CORRIDOR- FAILED ERROR: " << __FILE__ << ":" << __LINE__ << "cannot handle geometry type "
	 << el.geometryType << " of map element" << endl;
    return;
  }

  point2arr_uncertain vertices1(el.geometry);
  point2arr_uncertain vertices2(el.geometry);

  if (vertices1.size() == 0) {
    cerr << "ZoneCorridor: FAILED ERROR: " << __FILE__ << ":" << __LINE__ << " obstacle has no vertices." << endl;
    return;
  }

  Log::getStream(9) << "ZoneCorridor: el.type = ELEMENT_OBSTACLE. Will be painted in the map" << endl;
  Log::getStream(9) << "Obstacle at " << el.geometry << endl;
  Log::getStream(9) << "  height = " << endl;

  point2_uncertain obsCenter(0,0);
  for (unsigned i=0; i < vertices1.size(); i++)
    obsCenter = obsCenter + vertices1[i];
  
  obsCenter = obsCenter/vertices1.size();

  // Grow the obstacle
  for (unsigned i=0; i < vertices1.size(); i++) {
    double distFromCenter1 = obsCenter.dist(vertices1[i]) + VEHICLE_WIDTH;
    double distFromCenter2 = obsCenter.dist(vertices1[i]) + VEHICLE_WIDTH;
    double directionFromCenter = (vertices1[i] - obsCenter).heading();
    vertices1[i].set_point(obsCenter.x + distFromCenter1*cos(directionFromCenter),
			   obsCenter.y + distFromCenter1*sin(directionFromCenter));
    vertices2[i].set_point(obsCenter.x + distFromCenter2*cos(directionFromCenter),
			   obsCenter.y + distFromCenter2*sin(directionFromCenter));
  }

  Log::getStream(9) << "ZoneCorridor: Grew obstacle " << endl;

  vector<float> cost(vertices1.size(), obsCost);
  Polygon tmpPoly;
  tmpPoly.setVertices(vertices1, cost);
  tmpPoly.setCombFunc(combFunc);
  polygons.push_back(tmpPoly);

  // Gaussian function for continuity
  double a = -1/(2*pow(sigma,2));
  tmpPoly.setA(a);
  tmpPoly.setB(obsCost);
  tmpPoly.setC(0);
  tmpPoly.setFillFunc(fillFunc);

  for (unsigned i=0; i < vertices1.size(); i++) {
    point2arr_uncertain tmpVertices;
    cost.clear();

    cost.push_back(0);
    cost.push_back(0);
    double baseX = sqrt((1/a)*log(bval/obsCost));
    cost.push_back(baseX);
    cost.push_back(baseX);
    tmpVertices.push_back(vertices1[i]);
    if (i < vertices1.size() - 1) {
      tmpVertices.push_back(vertices1[i+1]);
      tmpVertices.push_back(vertices2[i+1]);
    }
    else {
      tmpVertices.push_back(vertices1[0]);
      tmpVertices.push_back(vertices2[0]);
    }
    tmpVertices.push_back(vertices2[i]);

    tmpPoly.setVertices(tmpVertices, cost);
    polygons.push_back(tmpPoly);
  }

}

void ZoneCorridor::assignCostToLine(vector<Polygon>& polygons, point2arr line,
				    float xCoeff, float cost, float minVal, float width, 
				    CostSide side,bitmap::FillFunc fillFunc, bitmap::CombineFunc combFunc)
{
  Polygon p;
  const cost_t sideVal = 2; // go down to 2*sigma (cost*ext(-0.5*2*2)=cost*0.1353)
 
  p.setFillFunc(fillFunc);
  p.setA(xCoeff);
  p.setB(cost);
  p.setC(minVal);
  p.setCombFunc(combFunc);

  // for ease of notation

  for (unsigned int i = 0; i < line.size()-1; i++)
  {
      // calculate bisectors for vertices v1 and v2. For a generic vertex k, the
      // bisector is the orthogonal to v(k+1) - v(k-1) through the vertex v(k)
      // except for the first and the last, which are treated specially
      point2 bisec[2];
      point2 distVec;

      if (i > 0) {
          distVec = line[i + 1] - line[i - 1];
      } else {
          distVec = line[i + 1] - line[i];
      }
      bisec[0] = point2(-distVec.y, distVec.x); // orthog to distVec
      bisec[0].normalize(); // FIXME: what if the points are so close that the distance is ~zero?

      if (i + 2 < line.size()) {
          distVec = line[i + 2] - line[i];
      } else {
          distVec = line[i + 1] - line[i];
      }
      bisec[1] = point2(-distVec.y, distVec.x); // orthog to distVec
      bisec[1].normalize(); // FIXME: what if the points are so close that the distance is ~zero?

      point2arr vert(4);
      vector<cost_t> val(4);
      // FIXME: there is the possibility that the top egde intersects the
      // bottom edge, for very sharp turns, creating something that is not a polygon
      // (a star? a butterfly? it's just that vertices are not in cw nor in ccw order)
      // The resulting cost map will be weird, but still acceptable, I think.

      // left side
      vert[0] = line[i + 1] + bisec[1]*width;
      val[0] = -sideVal;
      vert[1] = line[i]     + bisec[0]*width;
      val[1] = -sideVal;

      // right side
      vert[2] = line[i]     - bisec[0]*width;
      val[2] = sideVal;
      vert[3] = line[i + 1] - bisec[1]*width;
      val[3] = sideVal;

      p.setVertices(vert, val);
      polygons.push_back(p);
  }
}

void ZoneCorridor::populatePolygonalObstacles(CSpecs& cspecs, vector<MapElement> obstacles) {
  
  vector<point2arr> polyObstacles; 
  vector<CSpecsObstacle> cSpecsObstacles; 
  point2arr polygon; 
  CSpecsObstacle obs; 

  for (unsigned int i =0; i< obstacles.size(); i++) {
    for(unsigned int j=0; j<obstacles[i].geometry.size();j++) {
      polygon.push_back(obstacles[i].geometry[j]);
      obs.x =  obstacles[i].center.x;
      obs.y = obstacles[i].center.y;
      obs.dx = 0; //for now 
      obs.dy = 0; //for now
      obs.length = obstacles[i].length;
      obs.width = obstacles[i].width; 
    }
    cSpecsObstacles.push_back(obs);
    polyObstacles.push_back(polygon);
    polygon.clear();
  }
  cspecs.setPolyObstacles(polyObstacles);
  cspecs.setObstacles(cSpecsObstacles);
}

void ZoneCorridor::resetFinalConditions(CSpecs& cSpecs, pose3_t finPose, PathDirection direction) {

  double finalState[4];
  double roll, pitch, yaw;
  quat_to_rpy(finPose.rot, &roll, &pitch, &yaw);
  finalState[0] = finPose.pos.x - DIST_REAR_AXLE_TO_FRONT*cos(yaw);
  finalState[1] = finPose.pos.y - DIST_REAR_AXLE_TO_FRONT*sin(yaw);
  finalState[2] = 0.2; //dplanner requirement change to 0.1
  finalState[3] = yaw;

  setPathDirection(cSpecs, direction);
  cSpecs.setFinalState(finalState);

}


void ZoneCorridor::setPathDirection(CSpecs& cSpecs, PathDirection direction) { 
  int dir = 0;
  if (GRAPH_PATH_FWD == direction) 
    dir = 1;
  else if (GRAPH_PATH_REV == direction) 
    dir = -1;
  cSpecs.setPathDirection(dir);
}


void ZoneCorridor::generateGenericPerimeter(point2arr& perim, Map* map, point2 currPos, SegGoals currSegGoals) {

  /* Exit label of the current goal */
  PointLabel exitLabel = PointLabel(currSegGoals.exitSegmentID, currSegGoals.exitLaneID, currSegGoals.exitWaypointID); 

  /* Calculate distance from Alice to the exit point */
  point2 exitPoint; 

  map->getWaypoint(exitPoint, exitLabel);

  double distAliceExit = currPos.dist(exitPoint);

  point2 projCurrPos(currPos.x, currPos.y - 20);   

 
    
  /* Project the currPos away 20 m from Alice perpendicular currPos heading */
  
  point2 projExit; 
  
  /* Add vertices 100 m from projCurrPos in x direction, perpendicular to projCurrPos heading */
  point2 v1(projCurrPos.x + 100, projCurrPos.y, 0);
  point2 v2(projCurrPos.x - 100, projCurrPos.y, 0); 

  if (distAliceExit < 80) {

    double distExit = 80 - distAliceExit;
    
    /* Project the exit point in the direction of the line going through Alice and the exit point so that it is 80 m from Alice's projected point */
    point2 projExit(80*projCurrPos.x/distExit, 80*projCurrPos.y/distExit);    
  } else {

    /* Project the exit point 20 m in the y direction */
    point2 projExit(exitPoint.x, exitPoint.y +20);  
  }
 
  /* Add vertices 100  m from projExit in x direction */
  
  point2 v3(projExit.x + 100, projExit.y, 0);
  point2 v4(projExit.x - 100, projExit.y, 0); 
  
  /* Insert the points counterclockwise*/

  perim.push_back(v2);
  perim.push_back(projCurrPos);
  perim.push_back(v1);
  perim.push_back(v3);
  perim.push_back(projExit);
  perim.push_back(v4);
  
  for (unsigned int i = 0; i<perim.size(); i++) {
    Log::getStream(4)<<"ZONE Perimeter "<<endl; 
    Log::getStream(4)<<"x_"<<i<<"= "<<perim[i].x<<", y_"<<i<<"= "<<perim[i].y<<endl;
  }
}

void ZoneCorridor::printCSpecs(CSpecs* cSpecs) {

  Log::getStream(4) << "ZONE/CSPECS Initial state ( ";
  vector <double> initState = cSpecs->getStartingState();
  for (unsigned int i = 0; i < initState.size(); i++) {
    Log::getStream(4)<<initState[i]<<", ";
  }
  Log::getStream(4) << ") "<<endl;
  
  Log::getStream(4) << "ZONE/CSPECS Final state (";
  vector <double> finalState = cSpecs->getFinalState();
  for (unsigned int i = 0; i < finalState.size(); i++) {
    Log::getStream(4)<<finalState[i]<<", ";
  }
  Log::getStream(4) << ")"<<endl; 
  Log::getStream(4) << "ZONE/CSPECS Path direction = "<< cSpecs->getPathDirection()<<endl;
}



int ZoneCorridor::determineZoneAction(PointLabel exitLabel, int zoneId, Map* map) {

  /* If the current seg goal exit waypoint corresponds to the end of a parking spot, then we are entering a spot */
  if (zoneId == exitLabel.segment && 2 == exitLabel.point) {
    m_prevZoneAction = TRAFFIC_STATE_ENTER_PARK;
  } else {
    vector<PointLabel> exitLabels, exitLinks; 
    map->getZoneExits(exitLabels, exitLinks,zoneId);

    for (unsigned int i=0; i<exitLabels.size(); i++) {

      /* If the exit waypoint correponds to the exit of a zone and we had previously entered a parking spot or in the middle of exiting a spot. */
      if (exitLabel == exitLabels[i] && (TRAFFIC_STATE_ENTER_PARK == m_prevZoneAction || TRAFFIC_STATE_EXIT_PARK == m_prevZoneAction)) {
        m_prevZoneAction = TRAFFIC_STATE_EXIT_PARK;
        return m_prevZoneAction;
        /* If the exit waypoint correponds to the exit of a zone but we previously had not entered a parking spot, we are traversing a zone */
      } else if (exitLabel == exitLabels[i]) {
        m_prevZoneAction = TRAFFIC_STATE_TRAVERSE_ZONE;

      } else {
        m_prevZoneAction = TRAFFIC_STATE_UNSPECIFIED;
      }
    }
  }
  return m_prevZoneAction;
}

