Wed Sep  5 13:35:12 2007	vcarson (vcarson)

	* version R1-02d
	BUGS:  
	FILES: Planner.cc(37648), Planner.hh(37648)
	Added exit point information to send to Attentino module. 

	FILES: Planner.cc(37627), Planner.hh(37627)
	Added exit waypoint information for intersections to the attention
	module message. 

Wed Sep  5 11:56:10 2007	Sven Gowal (sgowal)

	* version R1-02c
	BUGS:  
	FILES: Planner.cc(37591), PlannerMain.cc(37591)
	Fixed possible segfault

Tue Sep  4 23:12:19 2007	vcarson (vcarson)

	* version R1-02b
	BUGS:  
	FILES: Makefile.yam(37556), Planner.cc(37556),
		PlannerMain.cc(37556), planner.ggo(37556)
	Added support for using dplanner in zones. 

	FILES: Makefile.yam(37542), Planner.cc(37542),
		PlannerMain.cc(37542), planner.ggo(37542)
	Added use-dplanner option.  By specifying use-dplanner option,
	planner uses dplanner for zone regions else uses s1planner. 
	Linking to dplanner libraries. 

Tue Sep  4  4:00:57 2007	datamino (datamino)

	* version R1-02a
	BUGS:  
	FILES: PlannerMain.cc(37476), TrafficStateEst.cc(37476),
		planner.ggo(37476)
	yam screwed the previous release, trying again

	FILES: PlannerMain.cc(37448), TrafficStateEst.cc(37448),
		planner.ggo(37448)
	Added new cmdline flag to enable old line fusion behavior of fusing
	elements while adding them to the map when the internal mapper
	(with the external mapper, the old behavior is the only possible
	one). Default when using the internal mapper is to run the line
	fusion within the mapper main loop at 10Hz.

	FILES: PlannerMain.cc(37339), TrafficStateEst.cc(37339),
		planner.ggo(37339)
	Added option to run line fusion the old way
	(--mapper-line-fusion-compat), in Map::addEl() instead of
	Mapper::mainLoop().

Tue Sep  4  1:20:12 2007	vcarson (vcarson)

	* version R1-02
	BUGS:  
	FILES: Planner.cc(37404), ZoneCorridor.cc(37404)
	Tweaked the bitmap parameters. Added smaller threshold for goal
	completion in a zone. 

	FILES: Planner.cc(37433)
	Changed zone threshold for goal completion to 0.5 m.

Mon Sep  3 18:57:35 2007	vcarson (vcarson)

	* version R1-01z
	BUGS:  
	FILES: Planner.cc(37295), UT_zoneCorridor.cc(37295),
		ZoneCorridor.cc(37295), ZoneCorridor.hh(37295)
	changes 

	FILES: Planner.cc(37326), ZoneCorridor.cc(37326)
	Implemented parking spot painting.  

Sun Sep  2 17:51:11 2007	Sven Gowal (sgowal)

	* version R1-01y
	BUGS:  
	FILES: Planner.cc(37215)
	merged

	FILES: Planner.cc(37096)
	Added timing information

Sun Sep  2 17:27:01 2007	vcarson (vcarson)

	* version R1-01x
	BUGS:  
	FILES: Planner.cc(37186), Planner.hh(37186),
		ZoneCorridor.cc(37186), ZoneCorridor.hh(37186)
	Fixed delayed in planner execution in zone regions. Added cost map
	painting for zone perimeters. 

	FILES: Planner.cc(37176), Planner.hh(37176),
		ZoneCorridor.cc(37176), ZoneCorridor.hh(37176)
	Fixed planner execution increase in zones. Painted cost map for
	perimeter

Sun Sep  2 11:25:41 2007	Christian Looman (clooman)

	* version R1-01w
	BUGS:  
	FILES: Planner.cc(37079)
	more log information at prediction

Sat Sep  1 17:26:05 2007	Sam Pfister (sam)

	* version R1-01v
	BUGS:  
	FILES: PlannerMain.cc(37018), TrafficStateEst.cc(37018)
	fixed bug where --mapper-enable-groundstrike-filtering caused
	mapper initialization to hang.	Should be able to run mapper
	internal to planner with all current options now. 

Thu Aug 30 18:49:00 2007	Noel duToit (ndutoit)

	* version R1-01u
	BUGS:  
	FILES: Planner.cc(36721), PlannerUtils.cc(36721),
		ZoneCorridor.cc(36721)
	merges with the latest release of planner.

	FILES: Planner.cc(36500), PlannerUtils.cc(36500)
	Fixed a few bugs in the extractSubPath function.

	FILES: Planner.cc(36631), ZoneCorridor.cc(36631)
	Finally got Alice to drive into and out of a zone

	FILES: Planner.cc(36667)
	Took out some print outs and added some debugging info.

Thu Aug 30 17:26:49 2007	Christian Looman (clooman)

	* version R1-01t
	BUGS:  
	FILES: Planner.cc(36632)
	Minor adjustments for calling prediction

Thu Aug 30 14:22:53 2007	Sven Gowal (sgowal)

	* version R1-01s
	BUGS:  
	FILES: Planner.cc(36539), Planner.hh(36539)
	Planning ahead

Wed Aug 29 20:42:43 2007	Noel duToit (ndutoit)

	* version R1-01r
	BUGS:  
	FILES: Makefile.yam(36434), Planner.cc(36434), Planner.hh(36434),
		PlannerUtils.cc(36434), UT_zoneCorridor.cc(36434)
	merge with latest release of planner.

	FILES: Makefile.yam(36215), Planner.cc(36215),
		UT_zoneCorridor.cc(36215)
	Continued debugging of the zones operations

Wed Aug 29 19:05:10 2007	vcarson (vcarson)

	* version R1-01q
	BUGS:  
	FILES: Planner.cc(36370)
	Fixed isCurrentPathComplete to call reverseProjection with path
	extension.

Wed Aug 29 16:15:55 2007	Kenny Oslund (kennyo)

	* version R1-01p
	BUGS:  
	FILES: ZoneCorridor.cc(36311)
	Modified ZoneCorridor.cc to work with updated access in cspecs

	FILES: ZoneCorridor.cc(36318)
	changed ZoneCorridor.cc to use a different version of the over
	loaded corridor setting function

Wed Aug 29 11:21:40 2007	vcarson (vcarson)

	* version R1-01o
	BUGS:  
	FILES: Planner.cc(36198), Planner.hh(36198)
	Implemented isCurrentPathComplete to replace isGoalComplete.  We
	now check goal completions wrt exit points that are projected onto
	the path.

Wed Aug 29  8:41:16 2007	Noel duToit (ndutoit)

	* version R1-01n
	BUGS:  
	FILES: Makefile.yam(36149), Planner.cc(36149), Planner.hh(36149)
	partially integrated functionality for zones and Uturns. Also for
	the sectioning of the path into subPaths according to the direction

	FILES: Planner.cc(36125), Planner.hh(36125)
	modified planner so the now we only plan through the zone once.
	Will have to update this to replan if the cost of the path changes.

	FILES: Planner.cc(36128)
	Fiddled with zones some more, but it is not yet functional.

	FILES: PlannerUtils.cc(36121), PlannerUtils.hh(36121),
		UT_zoneCorridor.cc(36121), ZoneCorridor.cc(36121)
	commiting so that Francisco can grab my changes.

	FILES: PlannerUtils.hh(36102)
	merged in Francisco's changes to the latest branch

Tue Aug 28 13:16:06 2007	Christian Looman (clooman)

	* version R1-01m
	BUGS:  
	FILES: Planner.cc(35914)
	Commit after merge

	FILES: Planner.cc(35901)
	Adapted to new getDistToStopLine function

Mon Aug 27 16:51:07 2007	Laura Lindzey (lindzey)

	* version R1-01l
	BUGS:  
	FILES: PlannerMain.cc(35734), TrafficStateEst.cc(35734),
		planner.ggo(35734)
	adding command line option to planner that enables groundstrike
	filtering on internal map

	FILES: PlannerMain.cc(35735)
	finishing cmdline changes

Mon Aug 27  9:08:33 2007	vcarson (vcarson)

	* version R1-01k
	BUGS:  
	FILES: Planner.cc(35425), Planner.hh(35425)
	Changed call to get distance to stop line.  Setting
	INTERSECTION_STRAIGHT for attention module.  

Thu Aug 23 17:10:18 2007	Sven Gowal (sgowal)

	* version R1-01j
	BUGS:  
	FILES: Planner.cc(35315)
	merged

	FILES: Planner.cc(35223)
	Made hacked UTURN and BACKUP more robust

Thu Aug 23 16:43:21 2007	vcarson (vcarson)

	* version R1-01i
	BUGS:  
	FILES: Planner.cc(35271), Planner.hh(35271), PlannerUtils.cc(35271)
	Finished attention/planner state sending. 

Thu Aug 23 14:00:14 2007	Noel duToit (ndutoit)

	* version R1-01h
	BUGS:  
	FILES: Makefile.yam(35213)
	Added new libraries that the mapper depends on. For this planner to
	work, you also have to check out the emap module

Tue Aug 21 19:24:28 2007	Christian Looman (clooman)

	* version R1-01g
	BUGS:  
	FILES: Planner.cc(34786)
	commit after merge

	FILES: Planner.cc(34765)
	adapted interface to new prediction module

Tue Aug 21 18:36:00 2007	Sven Gowal (sgowal)

	* version R1-01f
	BUGS:  
	FILES: Planner.cc(34758)
	Merged

	FILES: Planner.cc(34751)
	Fixed segfault + not re-generating the vehicle subgraph at
	intersection anymore

Tue Aug 21 16:48:18 2007	Noel duToit (ndutoit)

	* version R1-01e
	BUGS:  
	FILES: Planner.cc(34691), ZoneCorridor.cc(34691),
		ZoneCorridor.hh(34691)
	removed some comments that were printed to the console when in
	zones.

Tue Aug 21  6:23:03 2007	Sam Pfister (sam)

	* version R1-01d
	BUGS:  
	FILES: Makefile.yam(34548), Planner.cc(34548),
		PlannerMain.cc(34548), TrafficStateEst.cc(34548),
		TrafficStateEst.hh(34548), planner.ggo(34548)
	Enabled the option to have mapper run inside a thread in planner. 
	This is off by default but can be turned on by setting the
	--mapper-use-internal flag.  Also the mapper options can be passed
	through from the command line and have a mapper prefix. 

Mon Aug 20 23:56:39 2007	Christian Looman (clooman)

	* version R1-01c
	BUGS:  
	FILES: Planner.cc(34521), Planner.hh(34521)
	Changes after merge. Fixed segfault in isGoalComplete when graph is
	not initialized yet.

	FILES: Planner.cc(34451), Planner.hh(34451)
	Adjusted interfaces to LogicPlanner/IntersectionHandling so that
	IntersectionHandling receives the graph from planner

Mon Aug 20 19:53:22 2007	Sven Gowal (sgowal)

	* version R1-01b
	BUGS:  
	FILES: Planner.cc(34413)
	merged

	FILES: Planner.cc(34406)
	Fixed hacked UTurn

Mon Aug 20 18:30:38 2007	vcarson (vcarson)

	* version R1-01a
	BUGS:  
	FILES: Planner.cc(34339), Planner.hh(34339)
	Added southface for sending planner states to the attention module.
	 Changed isGoalComplete method to account for zone regions.  

Fri Aug 17 18:56:10 2007	vcarson (vcarson)

	* version R1-01
	BUGS:  
	FILES: Makefile.yam(34104), Planner.cc(34104), Planner.hh(34104),
		UT_zoneCorridor.cc(34104), ZoneCorridor.cc(34104)
	Changed files to support zone regions. 

	FILES: Makefile.yam(34123)
	Took out ocpspecs dependencies in planner. 

Thu Aug 16 20:10:46 2007	Sven Gowal (sgowal)

	* version R1-00z
	BUGS:  
	FILES: Planner.cc(33992), ZoneCorridor.cc(33992)
	Removed use of getWaypoint from Map (because not working for zones)

Wed Aug 15 18:01:31 2007	Sven Gowal (sgowal)

	* version R1-00y
	BUGS:  
	FILES: Planner.cc(33778), Planner.hh(33778), PlannerMain.cc(33778),
		TrafficStateEst.cc(33778), TrafficStateEst.hh(33778)
	Correctly added enable-line-fusion + made selection of the turning
	signal much faster

Tue Aug 14 23:09:19 2007	datamino (datamino)

	* version R1-00x
	BUGS:  
	FILES: Planner.cc(33621), Planner.hh(33621), PlannerMain.cc(33621),
		TrafficStateEst.cc(33621), TrafficStateEst.hh(33621),
		planner.ggo(33621)
	Added a cmdline option to enable line fusion in Map
	(--enable-line-fusion, default off). Remember that
	--update-from-map is needed too for the planner to use the fused
	data.

Tue Aug 14 18:17:49 2007	Noel duToit (ndutoit)

	* version R1-00w
	BUGS:  
	FILES: Makefile.yam(33509), Planner.cc(33509),
		ZoneCorridor.cc(33509), ZoneCorridor.hh(33509)
	Switched to the zone planner interface defined in cspecs.

Tue Aug 14 13:16:22 2007	Noel duToit (ndutoit)

	* version R1-00v
	BUGS:  
	New files: UT_zoneCorridor.cc ZoneCorridor.cc ZoneCorridor.hh
Tue Aug 14 12:10:28 2007	Noel duToit (ndutoit)

	* version R1-00u
	BUGS:  
	FILES: Makefile.yam(33358), Planner.cc(33358), Planner.hh(33358)
	Added ZoneCorridor.* for the corridor generation in zone regions.
	Implemented multi path-planner problem in planner. This is not yet
	stable, but does not break normal driving.

Tue Aug 14  9:55:19 2007	Christian Looman (clooman)

	* version R1-00t
	BUGS:  
	FILES: Makefile.yam(33306), Planner.cc(33306),
		PlannerMain.cc(33306)
	commit after merges

Mon Aug 13 19:59:36 2007	vcarson (vcarson)

	* version R1-00s
	BUGS:  
	FILES: Planner.cc(33202), Planner.hh(33202)
	Communication with follower responses.	Implemented new seggaols
	interface change. 

Thu Aug  9 13:36:45 2007	Sven Gowal (sgowal)

	* version R1-00r
	BUGS:  
	FILES: Planner.cc(32823), Planner.hh(32823)
	Merged

Wed Aug  8 17:30:14 2007	Christian Looman (clooman)

	* version R1-00q
	BUGS:  
	FILES: Planner.cc(32649)
	minor bug that slowed down planning loop when prediction uses
	particle filters

Wed Aug  8 13:22:28 2007	Christian Looman (clooman)

	* version R1-00p
	BUGS:  
	FILES: Planner.cc(32602)
	Fills logicParams with segment goals 

Sat Aug  4  3:46:53 2007	Magnus Linderoth (mlinderoth)

	* version R1-00o
	BUGS:  
	FILES: Planner.cc(31905)
	Now sets speed in velParams

	FILES: Planner.cc(31938)
	Added writing to velParams to a second place

Fri Aug  3 20:45:23 2007	Sven Gowal (sgowal)

	* version R1-00n
	BUGS:  
	FILES: Planner.cc(31748), PlannerMain.cc(31748), planner.ggo(31748)
	added update_from_map flag

	FILES: Planner.cc(31883)
	added --update-from-map arg

Fri Aug  3  9:05:17 2007	Christian Looman (clooman)

	* version R1-00m
	BUGS:  
	FILES: Makefile.yam(31730), Planner.cc(31730), Planner.hh(31730)
	Merged the two branches

	FILES: Makefile.yam(31723), Planner.cc(31723), Planner.hh(31723)
	Integrated the interface to Prediction

Fri Aug  3  0:51:17 2007	Sven Gowal (sgowal)

	* version R1-00l
	BUGS:  
	FILES: Planner.cc(31445), Planner.hh(31445), PlannerMain.cc(31445),
		planner.ggo(31445)
	Added argument --step-by-step-loading

	FILES: Planner.cc(31447)
	Stopping the car while loading from the RNDF

	FILES: Planner.cc(31660), PlannerMain.cc(31660), planner.ggo(31660)
	added arg + fixed mplanner error handling

Tue Jul 31 20:19:26 2007	Sven Gowal (sgowal)

	* version R1-00k
	BUGS:  
	FILES: Planner.cc(31286), TrafficStateEst.cc(31286),
		TrafficStateEst.hh(31286)
	Robust UTURN

	FILES: Planner.cc(30954)
	Calling updateGraphMap

	FILES: TrafficStateEst.cc(30851), TrafficStateEst.hh(30851)
	Changed rights

Tue Jul 31 13:47:30 2007	Christian Looman (clooman)

	* version R1-00j
	BUGS:  
	FILES: Makefile.yam(31092), Planner.cc(31092), Planner.hh(31092),
		PlannerMain.cc(31092), planner.ggo(31092)
	Integrated prediction into the planning loop. The prediction module
	is linked as a library. Prediction can be turned of by using the
	command line argument --noprediction

Fri Jul 27  9:28:39 2007	Sven Gowal (sgowal)

	* version R1-00i
	BUGS:  
	Deleted files: AliceStateHelper.cc AliceStateHelper.hh CmdArgs.cc
		CmdArgs.hh Console.cc Console.hh Log.cc Log.hh
	FILES: Makefile.yam(30390), PlannerMain.cc(30390),
		PlannerUtils.cc(30390), TrafficStateEst.cc(30390),
		UT_planner.cc(30390)
	trickling through changes in temp-planner-interfaces.

	FILES: Planner.cc(30292), Planner.hh(30292)
	started enforcing continuity between plans. Not done yet.

	FILES: Planner.cc(30308), UT_planner.cc(30308)
	Fixed small bug

	FILES: Planner.cc(30385), Planner.hh(30385)
	Fixed compilation

	FILES: Planner.cc(30478)
	Implemented some continuity enforcement in planner. Also, there is
	some fault handling here too. Might have to be moved to
	logic-planner?

	FILES: Planner.cc(30502), Planner.hh(30502)
	merged

	FILES: Planner.cc(30507, 30520, 30522)
	tuning UTURN

	FILES: Planner.cc(30534), PlannerMain.cc(30534), planner.ggo(30534)
	added option for planner to close the loop by computing path from
	current position and only using feed-forward in gcfollower.

	FILES: Planner.cc(30536), Planner.hh(30536)
	Successful UTURN with follower (follower doesn't continue after the
	UTURN though)

	FILES: Planner.cc(30556), Planner.hh(30556),
		PlannerUtils.cc(30556), PlannerUtils.hh(30556)
	Activating turn signals now

	FILES: Planner.hh(30375), PlannerUtils.hh(30375)
	Moved the common files in planner over from planner itself to the
	interfaces library.

Tue Jul 24 15:36:00 2007	Sven Gowal (sgowal)

	* version R1-00h
	BUGS:  
	FILES: CmdArgs.cc(30153), CmdArgs.hh(30153), Planner.cc(30153),
		Planner.hh(30153), PlannerMain.cc(30153),
		planner.ggo(30153)
	Added log-level for gcmodule logger

	FILES: Console.cc(30181), Console.hh(30181), Planner.cc(30181),
		PlannerMain.cc(30181)
	Made console threaded. PlannerMain is now responsible for
	refreshing the console

Tue Jul 24  8:38:02 2007	Noel duToit (ndutoit)

	* version R1-00g
	BUGS:  
	FILES: AliceStateHelper.cc(30116), AliceStateHelper.hh(30116),
		CmdArgs.cc(30116), CmdArgs.hh(30116), Console.cc(30116),
		Console.hh(30116), Log.cc(30116), Log.hh(30116),
		Planner.cc(30116), Planner.hh(30116),
		PlannerMain.cc(30116), PlannerUtils.cc(30116),
		PlannerUtils.hh(30116), TrafficStateEst.cc(30116),
		TrafficStateEst.hh(30116)
	Added some doxygen comments.

Mon Jul 23 19:42:27 2007	Sven Gowal (sgowal)

	* version R1-00f
	BUGS:  
	FILES: AliceStateHelper.cc(30029), Console.cc(30029),
		Planner.cc(30029), PlannerMain.cc(30029),
		PlannerUtils.cc(30029)
	Added comments

	FILES: Console.cc(30024), Planner.cc(30024)
	Removed flicker in the Console display

	FILES: Console.cc(30041)
	Updated console

	FILES: Planner.cc(30046), Planner.hh(30046), PlannerMain.cc(30046),
		planner.ggo(30046)
	Updated the commandline options for the planner.

Thu Jul 19 18:45:31 2007	Sven Gowal (sgowal)

	* version R1-00e
	BUGS:  
	FILES: Console.cc(29549), Console.hh(29549), Planner.cc(29549)
	Setting the flag to pass to path-planner

	FILES: Console.cc(29603), Console.hh(29603)
	changed console for new intersection return values

	FILES: Console.cc(29611)
	Layout changes

	FILES: Console.cc(29686), Console.hh(29686), Planner.cc(29686),
		Planner.hh(29686)
	Updated Console to display last and current waypoints

	FILES: Planner.cc(29626)
	Added console output

Wed Jul 18 16:37:02 2007	Christian Looman (clooman)

	* version R1-00d
	BUGS:  
	FILES: Console.cc(29476), Console.hh(29476)
	bugfixes

	FILES: Console.cc(29479), Console.hh(29479)
	Updated Console for showing intersection status

Wed Jul 18 12:34:42 2007	Sven Gowal (sgowal)

	* version R1-00c
	BUGS:  
	FILES: CmdArgs.cc(29398), CmdArgs.hh(29398), Console.cc(29398),
		Console.hh(29398), Planner.cc(29398),
		PlannerMain.cc(29398), PlannerUtils.cc(29398),
		PlannerUtils.hh(29398)
	Modified the gcmodule log path

	FILES: Makefile.yam(29400)
	Changed Makefile to allow libraries to use AliceStateHelper

	FILES: Planner.cc(29416)
	Modified error checking

Mon Jul 16 13:54:37 2007	Francisco zabala (zabala)

	* version R1-00b
	BUGS:  
	New files: Planner.cc Planner.hh PlannerMain.cc PlannerUtils.cc
		PlannerUtils.hh planner.ggo
	Deleted files: TrafficManager.cc TrafficManager.hh
		TrafficPlannerMainCSS.cc TrafficUtils.cc TrafficUtils.hh
		tplanner.ggo
	FILES: Makefile.yam(29118)
	Updated file names and executable names.

	FILES: Makefile.yam(29144)
	Updated Makefile

	FILES: UT_planner.cc(29143)
	Changed Graph_t to be Graph (instead of GraphPlanner)

Fri Jul 13 16:05:56 2007	Noel duToit (ndutoit)

	* version R1-00a
	BUGS:  
	New files: AliceStateHelper.cc AliceStateHelper.hh CmdArgs.cc
		CmdArgs.hh Console.cc Console.hh Log.cc Log.hh
		TrafficManager.cc TrafficManager.hh
		TrafficPlannerMainCSS.cc TrafficStateEst.cc
		TrafficStateEst.hh TrafficUtils.cc TrafficUtils.hh
		UT_planner.cc tplanner.ggo
	FILES: Makefile.yam(28605)
	Copied files over directly from both tplanner and graphplanner.
	First step is going to be to interface graph planner with tplanner,
	and then to clean up the code by placing pieces into libraries and
	using function calls.

	FILES: Makefile.yam(28692)
	Removed all references to the tplanner/mapping library.

	FILES: Makefile.yam(28715)
	Implemented UT_planner to test the graph, path and trajectory
	generation. Updated the GraphUpdater, PathPlanner and VelPlanner
	accordingly. Removed redundant graph-planner files in graph
	directory.

	FILES: Makefile.yam(28723)
	Updated the makefile to copy the planner interfaces file into the
	include directory.

	FILES: Makefile.yam(28733)
	removed planner interfaces dependence on temporary files by using
	fwd declarations

	FILES: Makefile.yam(28758)
	Extracted libraries for the planner module. Only logic-planner and
	planner code exist here now. Removed all the unnecessary files.
	Added UT_planner where we have successfully initialized a graph,
	planned a path, and planned the velocity. This executable outputs a
	file that can be read into matlab for verification.

	FILES: Makefile.yam(28806)
	Cleaned up some of the tplanner files (that still needs to be moved
	to the logic-planner library). Integrated the new planner library
	functions with TrafficManager, which will become the template for
	the final planner. The new planner now gives a planning problem to
	path-planner, which constructs the path. The velocity planner then
	creates the trajectory. This trajectory is sent directly to
	follower.

	FILES: Makefile.yam(28905)
	Removed tplanner FSM as well as corridor generation code. Cleaned
	up the files.

	FILES: Makefile.yam(29004)
	Added calls to the logic planner as well as the graph-updater

	FILES: Makefile.yam(29027)
	Added the logic-planner library to the UT_planner.

Fri Jul  6 16:49:31 2007	Noel duToit (ndutoit)

	* version R1-00
	Created planner module.








































































<<<<<<< .working



=======

>>>>>>> .merge-right.r37177
















