/*!
 * \file PlannerMain.cc
 * \brief Main planner execution entry point
 *
 * \author Noel duToit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */
   
#include <getopt.h>
#include "cmdline.h"
#include <iostream>
#include "Planner.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/cfgfile.h"
#include "skynet/skynet.hh"
#include <sys/time.h>
#include <sys/stat.h>
#include "temp-planner-interfaces/CmdArgs.hh"
#include "temp-planner-interfaces/Console.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"
 
using namespace std;                 
  
/**
 * @brief Entry point of the planner
 *
 * This function populates the CmdArgs static class
 * and starts the planner
 */
int main(int argc, char **argv)              
{  
  gengetopt_args_info cmdline;      
  string logFileName; 

  /* Get the skynet key */
  CmdArgs::sn_key = skynet_findkey(argc, argv);
 
  /* Parse command-line arguments */
  if (cmdline_parser(argc, argv, &cmdline) != 0)
    exit (1);

  /* Is console enabled? */
  CmdArgs::console = !cmdline.disable_console_given;

  /* What is the verbose level? */
  if (cmdline.verbose_given) {
    CmdArgs::verbose_level = cmdline.verbose_arg;
    if (CmdArgs::verbose_level > 0) CmdArgs::debug = true;
  }

  if (cmdline.log_level_given) {
    CmdArgs::log_level = cmdline.log_level_arg;
  }
    
  /* Get the RNDF filename */
  if (cmdline.rndf_given){
    CmdArgs::use_RNDF = true;
    CmdArgs::RNDF_file = cmdline.rndf_arg;
    if (!CmdArgs::console)
      cout << "RNDF Filename in = "  << CmdArgs::RNDF_file << endl;
    /* The RNDF gets loaded into the map inside the Planner constructor */
  }     

  /* Plan from current pos? (closed loop)*/
  CmdArgs::closed_loop = cmdline.closed_loop_flag;

  /* Use Prediction ? */
  CmdArgs::noprediction = cmdline.noprediction_flag;

  /* Step-by-step loading of the graph */
  CmdArgs::stepbystep_load = cmdline.step_by_step_loading_flag;

  /* Loading graph from files */
  CmdArgs::load_graph_files = cmdline.load_graph_files_flag;

  /* Updating graph from map */
  CmdArgs::update_from_map = cmdline.update_from_map_flag;

  /* Updating the cost for centerline dists based on sensed lane information */
  CmdArgs::update_lanes = cmdline.update_lanes_given;

  /* Updating the stoplines based on sensed info */
  CmdArgs::update_stoplines = cmdline.update_stoplines_given;

  /* Is logging enabled? */
  CmdArgs::logging = cmdline.log_flag;

  /* What is the log path? */
  CmdArgs::log_path = cmdline.log_path_arg;

  /* internal mapper options */
  
  /* Enable the use of the local mapper */
  CmdArgs::mapper_use_internal = cmdline.mapper_use_internal_flag;

  /* The debugging map element subchannel for the internal mapper */
  CmdArgs::mapper_debug_subgroup = cmdline.mapper_debug_subgroup_arg;

  /* If internal mapper is used, disables obstacle fusion */
  CmdArgs::mapper_disable_obs_fusion= cmdline.mapper_disable_obs_fusion_flag;

  /* Disable line fusion in the mapper*/
  CmdArgs::mapper_disable_line_fusion = cmdline.mapper_disable_line_fusion_flag;

  /* Use old line fusion behavior*/
  CmdArgs::mapper_line_fusion_compat = cmdline.mapper_line_fusion_compat_flag;

  /* Set time thresh to remove stale obstacles in the map*/
  CmdArgs::mapper_decay_thresh = cmdline.mapper_decay_thresh_arg;

  CmdArgs::mapper_enable_groundstrike_filtering = cmdline.mapper_enable_groundstrike_filtering_flag;

  /* Specify which zone planner should currently be used*/
  CmdArgs::use_dplanner = cmdline.use_dplanner_given;
  CmdArgs::use_s1planner = cmdline.use_s1planner_given;
  CmdArgs::use_circle_planner = cmdline.use_circle_planner_given;
  /* Display the costmap in mapviewer */
  CmdArgs::show_costmap = cmdline.show_costmap_given;

  /* Deal with config files here*/
  if (cmdline.path_plan_config_given) {
    // something is specified, so use that
    CmdArgs::path_plan_config = cmdline.path_plan_config_arg;
  } else {
    // for the default, look in the appropriate places ...
    char* filename = dgcFindConfigFile(cmdline.path_plan_config_arg, "planner");
    CmdArgs::path_plan_config = filename;
  }

  /*If no zone planner is specified, then use s1planner (for now)*/ 
  if ( !(CmdArgs::use_dplanner) && !(CmdArgs::use_s1planner) && !(CmdArgs::use_circle_planner) ) {
    CmdArgs::use_s1planner = true;
  }

  CmdArgs::use_rndf_frame = cmdline.use_rndf_frame_given;
 
  /* Generate a unique filename to log to */
  if(CmdArgs::logging) {
    string tmpRNDFname;
  
    ostringstream oss; 
    struct stat st;
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t)); 
    tmpRNDFname.assign(CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("/")+1, CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("."));
    oss << cmdline.log_path_arg << "planner-" << tmpRNDFname << "." << timestr << ".log";
    logFileName = oss.str();
    string suffix = "";

    /* if it exists already, append .1, .2, .3 ... */
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
    CmdArgs::log_filename = logFileName;
  }

   /* Display that console is not enabled */
   if (!CmdArgs::console){
     cout << "No display" << endl;
     cout << "Constructing skynet with KEY = " << CmdArgs::sn_key << endl;
     cout << "debug = " << CmdArgs::debug << endl;
     cout << "verbose level = " << CmdArgs::verbose_level << endl;
   }

  /* To be removed (are we using this?) */
  CmdArgs::lane_cost = true;

  /* Initializes Planner and start the gcmodule */
  Planner* planner = new Planner(); 
  sleep(1);
  planner->Start();

  /* Refreshing the console */
  unsigned int cnt = 0;
  while (true) {
    sleep(5);
  }

  return 0;
}

