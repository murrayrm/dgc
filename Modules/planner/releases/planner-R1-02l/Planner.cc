/*!
 * \file Planner.cc
 * \brief Source for planner class
 *
 * \author Noel duToit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include "Planner.hh"
#include <dgcutils/DGCutils.hh>
#include "s1planner/tempClothoidInterface.hh"
#include "temp-planner-interfaces/Utils.hh"
#include "/dgc/otg/include/otg.hh"

#define ZONE_ENDPT_THRESH    (1.0)

/**
 * @brief Constructor of the Planner class
 *
 * This constructor initializes the North and South faces of the gcmodule interface,
 * it activates the logging mechanism and sets the verbose level. It also starts
 * the TrafficStateEst class (which is a snapshot of the environment). Finally it
 * starts the different libraries that compose the planner module.
 */
Planner::Planner() 
  : CSkynetContainer(MODtrafficplanner, CmdArgs::sn_key)
  , GcModule("Planner", &m_controlStatus, &m_mergedDirective, 10000, 1000)
  , CStateClient(false)
  , m_isInit(false)
  , m_completed(false)
  , m_polyTalker(CmdArgs::sn_key, SNbitmapParams, MODtrafficplanner)
  , m_planFromCurrPos(true)
  , m_attentionPlanState(PlanningState::NOMINAL)
  , m_attentionExitPoint(0,0,0)
  , m_firstTimeInZone(true)
  , m_firstTimeInUTurn(true)
  , m_prevZoneAction(TRAFFIC_STATE_UNSPECIFIED) 
  , m_replanInZone(true)
{
  /* Initialize the northface */
  m_missTraffInterfaceNF = MissionPlannerInterface::generateNorthface(CmdArgs::sn_key, this);

  /* Initialize the southface */
  m_traffAdriveInterfaceSF = AdriveCommand::generateSouthface(CmdArgs::sn_key, this);
  m_traffFollowInterfaceSF = FollowerCommand::generateSouthface(CmdArgs::sn_key, this);
  m_plannerStateInterfaceSF = PlannerStateInterface::generateSouthfaceLite(CmdArgs::sn_key, this);

  /* Set the path to the log file */
  if (CmdArgs::logging) {
    Log::setGenericLogFile(CmdArgs::log_filename.c_str());
  }

  /* Set the verbose level */
  Log::setVerboseLevel(CmdArgs::verbose_level);
  if (CmdArgs::console) {
    if (!CmdArgs::logging)
      Log::setVerboseLevel(0);
    Console::init();
    Console::addMessage("Debug = %d, Verbose Level = %d, Log = %d", CmdArgs::debug, CmdArgs::verbose_level, CmdArgs::logging);
  }

  /* Initializes the traffic state estimator */
  m_traffStateEst = TrafficStateEst::Instance(true);

  /* Activates the gcmodule logging mechanism */
  if (CmdArgs::log_level > 0) {
    ostringstream str;
    str << CmdArgs::log_path << "planner-gcmodule";
    this->setLogLevel(CmdArgs::log_level);
    this->addLogfile(str.str().c_str());
  }

  /* Starts the map update thread */
  DGCstartMemberFunctionThread(m_traffStateEst, &TrafficStateEst::getLocalMapUpdate);
  
  /* Initialize traj */
  m_traj = CTraj(3);

  /* Initialize MapTalker */
  predictionMap.initSendMapElement(CmdArgs::sn_key);

  /* Initialize the graph-updater (graph-updater is responsible of the initialization of m_graph) */
  GraphUpdater::init(&m_graph, m_traffStateEst->getMap());
  /* Initialize the logic-planner */
  LogicPlanner::init();
  /* Initialize the path-planner */
  PathPlanner::init();
  /* Initialize the vel-planner */
  VelPlanner::init();

  /* Initialize the prediction */
  Prediction::init(m_graph, m_traffStateEst->getMap());

  /* Initialize traj talker (this is really our south face now) */
  m_trajTalker = new CTrajTalker(MODtrafficplanner, CmdArgs::sn_key);

  /* Initialize UTURN */
  uturn_initialized = false;
  uturn_stage = 0;

  /* start display */
  Console::display->run();
}

/**
 * @brief Destructor
 *
 * It frees the memory used by the planner and its libraries
 */
Planner::~Planner()
{
  MissionPlannerInterface::releaseNorthface(m_missTraffInterfaceNF);
  AdriveCommand::releaseSouthface(m_traffAdriveInterfaceSF);
  FollowerCommand::releaseSouthface(m_traffFollowInterfaceSF);
  PlannerStateInterface::releaseSouthfaceLite(m_plannerStateInterfaceSF);
  TrafficStateEst::Destroy();
  Console::destroy();
  delete m_trajTalker;
  GraphUpdater::destroy(m_graph);
  LogicPlanner::destroy();
  PathPlanner::destroy();
  VelPlanner::destroy();
  Prediction::destroy();
}

/**
 * @brief Arbitrate function of the gcmodule
 *
 * This function listens to the north face and gets messages from mplanner.
 * It populates a directive queue from the directive sent by mplanner and
 * respond to mplanner either by successfully reaching a goal and failing to do
 * so.
 */
void Planner::arbitrate(ControlStatus* cs, MergedDirective* md) {

  /* Display the time needed to make a full planner cycle */
  static uint64_t old_time = getTime();
  uint64_t new_time = getTime();
  Log::getStream(4) << "Time consumed = " << (new_time - old_time)/(double)1000000 << " seconds" << endl;
  Console::updateRate((new_time - old_time)/(double)1000000);
  old_time = getTime();

  unsigned long long time1, time2;
  DGCgettime(time1);

  /* Grab the status and merged directive from the arguments */
  PlannerControlStatus *controlStatus = dynamic_cast<PlannerControlStatus *>(cs);
  PlannerMergedDirective *mergedDirective = dynamic_cast<PlannerMergedDirective *>(md);

  SegGoalsStatus segGoalsStatus;

  /* Send a directive completion for goal 0 to mplanner.
   * It tells mplanner that planner started and that it
   * can start sending directives */
  if (!m_isInit) {
    segGoalsStatus.goalID = 0;
    segGoalsStatus.status = SegGoalsStatus::COMPLETED;
    m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
    sleep(2);
    
    while (!m_missTraffInterfaceNF->haveNewDirective()) {    
      Log::getStream(2) << "PLANNER: Sending Init Goal " << segGoalsStatus.goalID << endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
      sleep(1);
    }
    m_isInit = true;
  }

  /* Create the snapshot of the environment */
  m_traffStateEst->updateMap();
  m_traffStateEst->updateVehState();
  VehicleState vs = m_traffStateEst->getVehState();
  Console::updateState(vs);

  /* Printing that we are currently executing a goal */
  if (PlannerControlStatus::EXECUTING == controlStatus->status) {
    Log::getStream(1) << "PLANNER:  Executing Goal ID :" << controlStatus->ID << endl; 
  }

  /********************************************
   * Receive status back from the Follower    *
   * module Southface. FIXME                  *
   ********************************************/
  
  pumpPorts();
  
  while (m_traffFollowInterfaceSF->haveNewStatus()) {
    m_followResponse = (FollowerResponse)*(m_traffFollowInterfaceSF->getLatestStatus());
    Log::getStream(1) << "PLANNER: Follower response:  "<< m_followResponse.toString()<< endl;
    
    //Just deal with Lat and Long failures for now 
    if (m_followResponse.status == GcInterfaceDirectiveStatus::FAILED) {
      if (m_followResponse.reason & FollowerState::LateralMaxError) {
	//m_planFromCurrPos = true;
	if (m_followResponse.reason & FollowerState::LongitudinalMaxError) {

	}
      }
    }
  }
  
  /********************************************
   * Notification to Northface that we either *
   * failed or completed a directive          *
   ********************************************/
  
  /* Check on the status of the last command acted on by control */
  if ((controlStatus->status == PlannerControlStatus::COMPLETED ||
       controlStatus->status == PlannerControlStatus::FAILED) && m_currSegGoals.goalID != 0) {
    
    /* If Alice successfully reached the previous directive
     * notify mplanner */
    if (controlStatus->status == PlannerControlStatus::COMPLETED) {  
      segGoalsStatus.status = SegGoalsStatus::COMPLETED;
      segGoalsStatus.goalID = controlStatus->ID;
      m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
      Console::addMessage("Completion reported to MPlanner for GoalID %d", controlStatus->ID);

      /* Display result */
      Log::getStream(1) << "PLANNER: GOAL ID COMPLETED " << segGoalsStatus.goalID << endl;
      Console::updateLastWpt(PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID));

      /* Pop the previous directive off the queue front (if the queue wasn't cleared meanwhile) */
      if (m_accSegGoalsQ.size() > 0 && m_accSegGoalsQ.front().goalID == segGoalsStatus.goalID){ 
        m_accSegGoalsQ.pop_front();
      } else {
        /* We don't have any directives on the queue */
        Log::getStream(2) << "PLANNER: waiting for more goals: m_accSegGoals.size()==0 " << endl;
      }

    /* If Alice failed, flush the entire queue and notify mplanner */
    } else if (controlStatus->status == PlannerControlStatus::FAILED)  {
      /* Flush out the NF queue */
      while (m_missTraffInterfaceNF->haveNewDirective()) {
        SegGoals newDirective;
        m_missTraffInterfaceNF->getNewDirective( &newDirective );
        segGoalsStatus.goalID = newDirective.goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        Log::getStream(1) << "PLANNER: GOAL ID, FAILED (but not sending) " << segGoalsStatus.goalID << endl;
      }
      
      /* Flush out the accumulating segGoals control queue */
      while (m_accSegGoalsQ.size() > 0) {
        segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        Log::getStream(1) << "PLANNER: GOAL ID, FAILED (but not sending) " << segGoalsStatus.goalID << endl;
        m_accSegGoalsQ.pop_front();
      }

      /* Notify mplanner that we failed the current goal */
      segGoalsStatus.status = SegGoalsStatus::FAILED;
      segGoalsStatus.goalID = controlStatus->ID;
      m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
      Console::addMessage("Failure reported to MPlanner for GoalID %d", controlStatus->ID);
      Log::getStream(1) << "PLANNER: GOAL ID, FAILED (sending) " << segGoalsStatus.goalID << endl;
    }
  }
  Log::getStream(1) << endl << endl;


  /********************************************
   * Reponding to the Northface by populating *
   * our own directive queue                  *
   ********************************************/

  /* Get all the new directives and put them in the queue */
  while (m_missTraffInterfaceNF->haveNewDirective()) {

    SegGoals newDirective;
    m_missTraffInterfaceNF->getNewDirective(&newDirective);
    printDirective(&newDirective);

    Console::addMessage("I have a new directive #%d", newDirective.goalID);

    /* MPlanner had a reboot: flush the queue */
    if (newDirective.goalID == 1) {
      Console::addMessage("I reveived directive #1!");
      while (m_accSegGoalsQ.size()>0) {
        m_accSegGoalsQ.pop_front();
      }
    }

    /* If it is an old directive that mplanner sent us twice ignore it */
    if ((m_accSegGoalsQ.size()>0) && (newDirective.goalID <= m_accSegGoalsQ.back().goalID)) {
      continue;
    }

    /* Push the new directive on the queue */
    m_accSegGoalsQ.push_back(newDirective);

    /* A EMERGENCY_STOP or RESET directive preempt everything, the current queue is flushed */
    if ((SegGoals::EMERGENCY_STOP == newDirective.segment_type) || (SegGoals::RESET == newDirective.segment_type)) {
      SegGoalsStatus segGoalsStatus;
      
      /* First flush the queue where the seg goals are accumulating */
      while (SegGoals::EMERGENCY_STOP != m_accSegGoalsQ.front().segment_type && SegGoals::RESET != m_accSegGoalsQ.front().segment_type) {
        segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        controlStatus->status = PlannerControlStatus::FAILED;
        Log::getStream(1)<<"PLANNER: GOAL ID EMERGENCY_STOP, RESET, FAILED "<<segGoalsStatus.goalID<<endl;
        m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
        m_accSegGoalsQ.pop_front();
      }
      /* Break to answer as fast as possible to the EMERGENCY_STOP or RESET directive */
      break; 
    }
  }

  /* If we are currently not executing any goal, set the current goal to
   * the bottom most directive of the queue */
  if (m_accSegGoalsQ.size() > 0 && (PlannerControlStatus::EXECUTING != controlStatus->status || m_currSegGoals.goalID == 0)) {
    SegGoals newGoal =  m_accSegGoalsQ.front();

    /* EMERGENCY_STOP and END_OF_MISSION directive do not specify a exit waypoint */
    if (SegGoals::EMERGENCY_STOP == newGoal.segment_type || SegGoals::RESET == newGoal.segment_type || SegGoals::END_OF_MISSION == newGoal.segment_type) {
      mergedDirective->segType = newGoal.segment_type;
      mergedDirective->id = newGoal.goalID;
      m_currSegGoals = newGoal;
    /* if UNKNOWN fail right away */
    } else if (SegGoals::UNKNOWN == newGoal.segment_type) {
      segGoalsStatus.goalID = newGoal.goalID;
      mergedDirective->segType = newGoal.segment_type;
      segGoalsStatus.status = SegGoalsStatus::FAILED;
      Log::getStream(1)<<"PLANNER: GOAL ID,UNKNOWN "<<segGoalsStatus.goalID<<endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
    /* Populates the exit waypoint */
    } else {
      mergedDirective->id = newGoal.goalID; 
      mergedDirective->segType = newGoal.segment_type;
      m_currSegGoals = newGoal;
      Console::updateNextWpt(PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID));
    }
    Console::addMessage("New goal received %d - illegal passing: %d", newGoal.goalID, newGoal.illegalPassingAllowed);
    Log::getStream(4) << "New goal to complete #" << newGoal.goalID << endl; 

  /* Nothing on the Northface */
  } else if (PlannerControlStatus::EXECUTING != controlStatus->status) {
    Log::getStream(1)<<"PLANNER: Waiting for Mission Planner goal "<<endl;
    /* Pause if we are waiting for goals */
    mergedDirective->segType = SegGoals::EMERGENCY_STOP;
    mergedDirective->id = 0;
    m_currSegGoals.goalID = 0;
  }

  DGCgettime(time2);
  Log::getStream(9) << "Arbitrate execution time: " << (time2-time1)/1000.0 << " ms" << endl;
}

/**
 * @brief Control function of the gcmodule
 *
 * This function generates the trajectory according
 * to the current goal
 */
void Planner::control(ControlStatus* cs, MergedDirective* md)
{
  /* Initialize error */
  static Err_t error = GU_OK | LP_OK | PP_OK | VP_OK;

  /* Initialize cost */
  static Cost_t cost = 0;

  /* Initialize vector of state problems */
  static vector<StateProblem_t> state_problems;

  /* Endpoint given by MPlanner */
  static PointLabel curr_endpoint;

  /* Old turning signal (so that we don't send turn at every cycle but only when it changes) */
  static int old_signal = 0;

  /* Segments */
  static vector<int> segments;

  static bool isGraphCreated = false;

  unsigned long long time1, time2;

  /* Grab the status and merged directive from the arguments */
  PlannerControlStatus* controlStatus = dynamic_cast<PlannerControlStatus *>(cs);
  PlannerMergedDirective* mergedDirective = dynamic_cast<PlannerMergedDirective *>(md);

  Log::getStream(9) << "Control on directive #" << m_currSegGoals.goalID << endl;

  /* Initialize vehState (renaming) */
  VehicleState vehState = m_traffStateEst->getVehState();

  /* Initialize the planner state sent to attention module */
  m_attentionPlanState = PlanningState::NOMINAL;

  // Listen to requests from Console
  DGCgettime(time1);
  if (Console::queryToggleIntersectionSafety())
    IntersectionHandling::toggleIntersectionSafety();
  if (Console::queryToggleIntersectionCabmode())
    IntersectionHandling::toggleIntersectionCabmode();
  // Toggle Prediction to turn ON/OFF ?
  if (Console::queryTogglePredictionFlag())
    CmdArgs::noprediction = !CmdArgs::noprediction;
  if (Console::queryResetStateFlag()) {
    LogicPlanner::resetState();
    m_planFromCurrPos = true;
    uturn_initialized = false;
    uturn_stage = 0;
  }
  IntersectionHandling::updateConsole();
  DGCgettime(time2);
  Log::getStream(9) << "Querying console execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Load graph if necessary */
  if (CmdArgs::stepbystep_load) {
    getSegments(segments);
    if (GraphUpdater::hasNewSegments(segments)) {
      StateProblem_t problem;
      problem.state = PAUSE;
      Vel_params_t velParams;
      velParams.minSpeed = m_currSegGoals.minSpeedLimit;
      velParams.maxSpeed = m_currSegGoals.maxSpeedLimit;
      VelPlanner::planVel(&m_traj, &m_path, problem, vehState, velParams);
      int trajSocket = m_trajTalker->m_skynet.get_send_sock(SNtraj);
      m_trajTalker->SendTraj(trajSocket, &m_traj);
      GraphUpdater::load(m_graph, m_traffStateEst->getMap(), segments);
      m_planFromCurrPos = true;
    }
  }

  /* Completing a EMERGENCY_STOP or an END_OF_MISSION is just a matter of stopping */
  DGCgettime(time1);
  if (mergedDirective->segType==SegGoals::EMERGENCY_STOP || mergedDirective->segType==SegGoals::RESET ||
      mergedDirective->segType==SegGoals::END_OF_MISSION) {
    m_completed = PlannerUtils::isStopped(vehState);
    Log::getStream(4) << "Checking if PAUSE is completed (" << m_completed << ")" << endl;
  } else if (mergedDirective->segType == SegGoals::UTURN) {
    m_completed = (error & VP_UTURN_FINISHED);
    if (m_completed) m_planFromCurrPos = true;
    Log::getStream(4) << "Checking if UTURN is completed (" << m_completed << ")" << endl;
  /* Check if Alice completed the previous goal */
  } else {
    m_completed = isCurrentPathComplete();
    Log::getStream(4)<<"PLANNER: isCurrPathComplete "<<m_completed<<endl;
  }
  DGCgettime(time2);
  Log::getStream(9) << "Completion execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* If we completed, set the status to completed.
   * This will notify mplanner at the beginning of the
   * next arbitrate function call (we cannot goal id 0) */
  if (m_completed && m_currSegGoals.goalID != 0) {
    controlStatus->ID = m_currSegGoals.goalID;
    controlStatus->status = PlannerControlStatus::COMPLETED;
    m_currSegGoals.segment_type = SegGoals::UNKNOWN;
    return;
  }

  /* Grab the actuator state (temporary solution) */
  ActuatorState actState;
  memset(&actState, 0, sizeof(actState));

  /* Try to replan from current position if stuck */
  if ((error & PP_NOPATH_LEN) || (error & PP_NOPATH_COST)) {
    m_planFromCurrPos = true;
  }

  /* Execute logic planner */
  DGCgettime(time1);
  state_problems.clear();
  Logic_params_t logicParams;
  logicParams.segment_type = mergedDirective->segType;
  logicParams.seg_goal = m_currSegGoals;
  logicParams.seg_goal_queue = &m_accSegGoalsQ;
  logicParams.m_path = &m_path;
  error = LogicPlanner::planLogic(state_problems, m_graph, error, vehState, m_traffStateEst->getMap(), logicParams);
  Console::updateFSMState(LogicPlanner::stateToString(state_problems[0].state));
  Log::getStream(1) << "Current State = " << LogicPlanner::stateToString(state_problems[0].state) << endl;
  Console::updateFSMFlag(LogicPlanner::flagToString(state_problems[0].flag));
  Log::getStream(1) << "Current Flag = " << LogicPlanner::flagToString(state_problems[0].flag) << endl;
  DGCgettime(time2);
  Log::getStream(9) << "Logic Planner execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Notify MPlanner to "fail" */
  if (error & LP_FAIL_MPLANNER) {
    controlStatus->ID = m_currSegGoals.goalID;
    controlStatus->status = PlannerControlStatus::FAILED;
    return;
  }

  /* Reset the error */
  error = 0;

  /* Update the graph according to sensed lanes */
  if (CmdArgs::update_from_map) {
    DGCgettime(time1);
    error |= GraphUpdater::updateGraphMap(m_graph, vehState, m_traffStateEst->getMap());
    DGCgettime(time2);
    Log::getStream(9) << "Graph update from map execution time: " << (time2-time1)/1000.0 << " ms" << endl;
  }

  /* Set the final pose based on the goal position */
  double heading;
  DGCgettime(time1);
  pose3_t finPose = getFinalPose(state_problems[0], mergedDirective, m_currFinalPos, heading);
  DGCgettime(time2);
  Log::getStream(9) << "Finding final pose execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Set the path planner parameters */
  Path_params_t pathParams;
  pathParams.flag = state_problems[0].flag;
  pathParams.planFromCurrPos = m_planFromCurrPos;
  pathParams.velMin = m_currSegGoals.minSpeedLimit;
  pathParams.velMax = m_currSegGoals.maxSpeedLimit;

  /* Set the vel planner parameters */
  Vel_params_t velParams;
  velParams.minSpeed = m_currSegGoals.minSpeedLimit;
  velParams.maxSpeed = m_currSegGoals.maxSpeedLimit;

  /* Compute trajectory */
  if (state_problems[0].state == UTURN) {

    if (1) {

      /* temporary generate UTurn manually */
      error |= planUTurn(&m_traj, vehState, m_traffStateEst->getMap(), PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID));

    }
    
    if(0) {

      Console::addMessage("About to execute a UTurn");
      double vel = AliceStateHelper::getVelocityMag(vehState); 

      point2 start_pt, ref_pt, aref_pt;
      Uturn_t uTurn = UTURN_DARPA; // Nomimal for now
      LaneLabel curr_lane;
             
      start_pt.x = vehState.localX;
      start_pt.y = vehState.localY;

      if (uTurn == UTURN_OBS) {             // Obstacle U-turns use obstacle point as reference (1)

          /* 
           * Map obs functions here 
           * NYI
           */

      } else if (uTurn == UTURN_DARPA) {   // Nominal U-turns use a projected point (15m out) as reference (2)

        m_traffStateEst->getMap()->getLane(curr_lane, start_pt);
        m_traffStateEst->getMap()->getLaneCenterPoint(aref_pt, curr_lane, start_pt, 25.0); // centerline ends?
            
        // point2arr centerline;
        // m_traffStateEst->getMap()->getLaneCenterLine(centerline, curr_lane);
        // m_traffStateEst->getMap()->getPointAlongLine(ref_pt, centerline, 0.0);

        ref_pt.x = aref_pt.x - 10*cos(heading);  // set it manually (need to check if above function cares
        ref_pt.y = aref_pt.y - 6.0*sin(heading); // about centerline ending...)
      }    
      
      // Debug info
      bool deb = false;
      if (deb) {
        cout << "My start_pt = (" << start_pt.x << ", " << start_pt.y << ")" << endl;
        cout << "My ref_pt = (" << ref_pt.x << ", " << ref_pt.y << ")" << endl;
        double Dx = ref_pt.x - start_pt.x; double Dy = ref_pt.y - start_pt.y; double Dm = sqrt(Dx*Dx + Dy*Dy);
        cout << "Dm = " << Dm <<  endl;
      }    

      GraphNode *nodeVeh;

      /* Update graph, generate Uturn Graph */
      if(!isGraphCreated) {
        if (vel < 0.2) {
          Console::addMessage("About to generate Uturn Graph");
          GraphUpdater::genUturnGraph(&m_gUturn, m_traffStateEst->getMap(), finPose, ref_pt, &nodeVeh, vehState, actState, uTurn);
          isGraphCreated = true;
        } else {
          Console::addMessage("Uturn: Velocity > 0.2");
          return;
        }
      }

      // Generate the Path 
      Console::addMessage("About to generate Path");

      // Generate the Uturn Maneuvers for this stage
      GraphUtils::genUturnManeuvers(m_gUturn, &nodeVeh, vehState, actState);

      // error |= GraphUpdater::genVehicleSubGraph(m_gUturn, vehState, actState);

      error |= PathPlanner::planPath(&m_pathUturn, cost, m_gUturn, vehState, finPose, pathParams);

      Console::addMessage("About to extract Path");
      PathPlanner::print(&m_pathUturn);
      PathPlanner::display(10, &m_pathUturn);

      PlannerUtils::extractSubPath(m_subPath, m_path, vehState);
      PathPlanner::print(&m_subPath);
      PathPlanner::display(10, &m_subPath);
      

      Console::addMessage("About to extract Path");
      // Display new path
      GraphUpdater::display(-2, m_gUturn, vehState);
      PathPlanner::display(-2, &m_path);
      // PathPlanner::display(-2, &m_subPath);

      if(deb) cout << "FinPose = (" << finPose.pos.x << ", " << finPose.pos.y << ")" << endl;

      // Generate the Trajectory
      Console::addMessage("About to generate Traj");
      Vel_params_t velParams;

      error |= VelPlanner::planVel(&m_traj, &m_pathUturn, state_problems[0], vehState, velParams);
    }

  } else if (state_problems[0].state == BACKUP) {

    error |= planBackup(&m_traj, vehState, m_traffStateEst->getMap());

  } else {

    if (m_planFromCurrPos) {
      DGCgettime(time1);
      /* Update graph to account for the current car position */
      error |= GraphUpdater::genVehicleSubGraph(m_graph, vehState, actState);
      Console::addMessage("Replanning from the current position");
      if (!CmdArgs::closed_loop) {
        // reset so that we plan from previous path
        m_planFromCurrPos = false;
      }
      DGCgettime(time2);
      Log::getStream(9) << "genVehicleSubGraph execution time: " << (time2-time1)/1000.0 << " ms" << endl;
    }

    /* Update graph according to the new state problem */
    DGCgettime(time1);
    error |= GraphUpdater::updateGraphStateProblem(m_graph, state_problems[0], m_traffStateEst->getMap());
    DGCgettime(time2);
    Log::getStream(9) << "Graph update from state problem execution time: " << (time2-time1)/1000.0 << " ms" << endl;

    /* Predict trajectories of obstacles and possible collisions */
    int numberObstacles=0;
    string predStatus;

    DGCgettime(time1);
    static bool PredictionCollisionFlag = false;
    MapElement me;
    if (!CmdArgs::noprediction) {
      Prediction::USE_PARTICLES = false;
      Prediction::PRINT_TRAJ = false;
      PredictionObstacle pObstacle;
      Prediction::predictionReturn predReturn;
      Prediction::updateList(vehState);
      predReturn = Prediction::predictCollision(vehState, pObstacle, numberObstacles);
        
      if (predReturn == Prediction::CLEAR && PredictionCollisionFlag) {
        me.setId(MODtrafficplanner,400);
        me.setTypeClear();
        predictionMap.sendMapElement(&me,1);
        predictionMap.sendMapElement(&me,-2);
        predStatus="CLEAR    ";
        Log::getStream(1)<<"Collision: clear collisionPoint "<<pObstacle.collisionPoint<<endl;
        PredictionCollisionFlag = false;
      } else if  (predReturn == Prediction::COLLISION) {
        me.setId(MODtrafficplanner,400);
        me.setTypePredictedVehicle();
        me.setColor(MAP_COLOR_RED);
        me.setGeometry(pObstacle.collisionPoint, 5.0);
        predictionMap.sendMapElement(&me,1);
        predictionMap.sendMapElement(&me,-2);
        Log::getStream(1)<<"Collision: send collisionPoint "<<pObstacle.collisionPoint<<endl;
        predStatus="COLLISION";
        Console::addMessage("Collision predicted!!");
        PredictionCollisionFlag = true;
      } else predStatus="UNKOWN";
    }
    else {
      if (PredictionCollisionFlag) {
        me.setId(MODtrafficplanner,400);
        me.setTypeClear();
        predictionMap.sendMapElement(&me,1);
        predictionMap.sendMapElement(&me,-2);
        PredictionCollisionFlag = false;
      }
      predStatus="OFF      ";
    }
    Console::updatePred(predStatus,numberObstacles);
    DGCgettime(time2);
    Log::getStream(9) << "Prediction execution time: " << (time2-time1)/1000.0 << " ms" << endl;

    /* Plan the path */
    if (pathParams.flag != ZONE) {

      /* Use the rail-based planner to plan the path and vel-planner to plan 
       * traj */
      DGCgettime(time1);
      error |= PathPlanner::planPath(&m_path, cost, m_graph, vehState, finPose, pathParams);
      DGCgettime(time2);
      Log::getStream(9) << "Path Planner execution time: " << (time2-time1)/1000.0 << " ms" << endl;
      if (error & PP_NONODEFOUND) {
        // for the next cycle, plan from current pos
        m_planFromCurrPos = false;
      }
      
      /* Plan the trajectory */
      DGCgettime(time1);
      error |= VelPlanner::planVel(&m_traj, &m_path, state_problems[0], vehState, velParams);
      DGCgettime(time2);
      Log::getStream(9) << "Velocity Planner execution time: " << (time2-time1)/1000.0 << " ms" << endl;

      //m_firstTimeInZone = true;

    } else {

      /* Use the clothoid-based planner to plan the path, and the vel-planner to
       * plan the velocity */
      Log::getStream(4) << "PLANNER:  we are in ZONE REGION "<<endl;

      /* Set the planner state for the attention module to zone */
      m_attentionPlanState = PlanningState::ZONE;

      /* Get the zone perimeter*/
      point2arr zonePerimeter;
      pathParams.zoneId = m_currSegGoals.entrySegmentID;

      m_traffStateEst->getMap()->getZonePerimeter(zonePerimeter, pathParams.zoneId);

      /* Get the parking spots*/
      SpotLabel spotLabel; 
      point2 first, second; 
      point2arr spotWaypt; 
      vector<SpotLabel> parkingSpots; 
      vector<point2arr> parkingSegments; 

      m_traffStateEst->getMap()->getZoneParkingSpots(parkingSpots, pathParams.zoneId);
      
      for (unsigned int i =0; i<parkingSpots.size(); i++) {	
	m_traffStateEst->getMap()->getSpotWaypoints(first, second,parkingSpots[i]);
	spotWaypt.push_back(first);
	spotWaypt.push_back(second);
	parkingSegments.push_back(spotWaypt);
	spotWaypt.clear();
      }

      CSpecs_t cSpecs;
      cSpecs = CSpecs_t();
      DGCgettime(time1);

      /* Determine what we plan to do in the zone based on exit point */
      PointLabel exitLabel = PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID); 

      Log::getStream(4)<<"PLANNER: Exit zone label  "<<exitLabel<<endl;

      int zoneAction = determineZoneAction(exitLabel, pathParams.zoneId);

      Log::getStream(4)<<"PLANNER: Zone Action "<<zoneAction<<endl;
      cSpecs.setTrafficState(zoneAction);


      /* Get the obstacles in the zone */

      vector<MapElement> obstacles; 

      if (!CmdArgs::use_dplanner) {
	m_traffStateEst->getMap()->getObsInZone(obstacles, pathParams.zoneId);
      }

      Log::getStream(4)<<"PLANNER: obstacles in zone "<<obstacles.size()<<endl;

      /* Generate the corridor for the zone */
      ZoneCorridor::generateCorridor(cSpecs, zonePerimeter, parkingSegments, obstacles, pathParams, finPose, vehState);

      BitmapParams bmParams = cSpecs.getBitmapParams(); 
      m_polyTalker.send(&bmParams);

      DGCgettime(time2);
      Log::getStream(9) << "Zone: Generate corridor execution time: " << (time2-time1)/1000.0 << " ms" << endl;      
      Path_t subPath;

      // if (m_firstTimeInZone) {
        //m_prevFinalPos.set(m_currFinalPos);

      if (CmdArgs::use_dplanner && m_replanInZone) { /* Dplanner */

	int info; 
	m_traj = createOtgTrajectory(cSpecs, &info); 

	if(1<= info && info<=3) { 
	  Console::addMessage("DPLANNER: created GOOD traj!!!");
	  Log::getStream(4) << "DPLANNER, created GOOD traj "<<endl;
	  m_replanInZone = false;
	} else {
	  Console::addMessage("DPLANNER: created BAD traj!!!", info);
	  Log::getStream(4) << "DPLANNER, created BAD traj "<<info<<endl;
	  m_replanInZone = true;
	}
      } else { /* Clothoid planner */

	double runtime = 0.6;

	DGCgettime(time1);

	if (m_replanInZone)
	{
	  // delete the clothoid tree and replan from scratch
	  ClothoidPlannerInterface::clearClothoidTree(&cSpecs);
	  error |= ClothoidPlannerInterface::GenerateTraj(NULL, &m_path, runtime, CmdArgs::sn_key, false);
	} else {
	  // replan by updating the clothoid tree
	  error |= ClothoidPlannerInterface::GenerateTraj(&cSpecs, &m_path, runtime, CmdArgs::sn_key, false);
	}

	DGCgettime(time2);
	Log::getStream(9) << "Zone: s1planner path generation execution time: " << (time2-time1)/1000.0 << " ms" << endl;      
	  
	PathPlanner::display(10, &m_path, MAP_COLOR_BLUE, 12501);
	
	/* Extract forward (or reverse) paths to give to velocity planner */
 
	if (PlannerUtils::extractSubPath(subPath, m_path, vehState) != 0) {
	  Log::getStream(9) << "Zone:Extract subpath returned errors: " << endl; 
	  m_replanInZone = true;
	
	} else {	  

	  PathPlanner::display(10, &subPath);

	  /* Replan if path length is zero */	  
	  if (m_path.pathLen == 0) {
	    /* If the path is of zero length, replan and clear the clothoid tree */
	    Log::getStream(4) << "Planner: Zone - path of zero length - replan" << endl;
	    m_replanInZone = true; 
	  } else {
	    /* Call the velocity planner */	  
	    error |= VelPlanner::planVel(&m_traj, &subPath, state_problems[0], vehState, velParams);
	    m_replanInZone = false; 
	  }
	}
      }

      /* If we are stopped for more than 20 seconds, replan */
      if (Utils::monitorAliceSpeed(vehState) > 40.0 && !CmdArgs::use_dplanner) {
      	Log::getStream(4) << "Planner: Zone - stopped for more than 20 seconds - replan";
	m_replanInZone = true; 
      }

	//}      
	
	/* Conditions for replanning for both dplanner and s1planner */
	/* If our final point has changed significantly - as in completed a goal, replan */
	//if (m_currFinalPos.dist(m_prevFinalPos) > ZONE_ENDPT_THRESH) {
	//	Log::getStream(4) << "Planner: Zone - endpt moved - replan. Previous Pos = " << m_prevFinalPos << " and Curr Pos = " << m_currFinalPos<< endl;
	//	m_firstTimeInZone = true;
      //}
	
    }
  }

  /* print graph and path to default mapviewer channel */
  DGCgettime(time1);
  GraphUpdater::display(-2, m_graph, vehState);
  PathPlanner::display(-2, &m_path);
  Console::updateTrajectory(&m_traj);
  DGCgettime(time2);
  Log::getStream(9) << "Sending to MapViewer execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Send turning signal */
  DGCgettime(time1);
  int signal = determineSignaling(state_problems[0]);
  if (old_signal != signal) {
    Console::updateTurning(signal);
    sendTurnSignalCommand(signal);
    old_signal = signal;
  }
  DGCgettime(time2);
  Log::getStream(9) << "Turning signal execution time: " << (time2-time1)/1000.0 << " ms" << endl;
  
  /* Send the trajectory */
  DGCgettime(time1);
  int trajSocket = m_trajTalker->m_skynet.get_send_sock(SNtraj);
  m_trajTalker->SendTraj(trajSocket, &m_traj);
  DGCgettime(time2);
  Log::getStream(9) << "Sending trajectory execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Now we want to send the planner state to the attention module */
  DGCgettime(time1);
  sendPlanningState(state_problems);
  DGCgettime(time2);
  Log::getStream(9) << "Sending to planning state execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Set status */
  controlStatus->status = PlannerControlStatus::EXECUTING;
  controlStatus->ID = m_currSegGoals.goalID;
}


bool Planner::isGoalComplete(vector<StateProblem_t> state_problems)
{
  double completeDist = 4;
  bool completed = false;
  double angle,dotProd,AliceHeading,absDist,headingDiff; 
  double absMaxDist = 10;

  PointLabel exitWayptLabel = PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID);
  GraphNode *entryNode = NULL; 
  GraphNode *exitNode = NULL;


  /* Get all entry and exit nodes */
  if (m_currSegGoals.entrySegmentID != 0)
    entryNode = m_graph->getNodeFromRndfId(m_currSegGoals.entrySegmentID, m_currSegGoals.entryLaneID, m_currSegGoals.entryWaypointID);

  /* Check if we are in an intersection */ 
  /* If state is in STOP_INT, do not complete segment goal. Call IntersectionHandling before to be sure that it receives the correct SegGoals */
  if (state_problems.size() != 0 && entryNode!=NULL && entryNode->isStop && state_problems[0].state == STOP_INT) {
    completed = false;
    return completed;
  }

  /* Check if we are in a zone region */ 
  exitNode = m_graph->getNodeFromRndfId(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID);

  if (exitNode == NULL)
    return completed;

  if (NULL != exitNode && GRAPH_NODE_ZONE == exitNode->type) {
    completeDist = 0.5;
    absMaxDist = 2;
    Log::getStream(4) << "PLANNER: Node is ZONE type " << endl;
  }
  
  point2 exitWaypt, currPos;

  exitWaypt.set(exitNode->pose.pos.x, exitNode->pose.pos.y);
  currPos = AliceStateHelper::getPositionRearAxle(m_traffStateEst->getVehState());
    
  m_traffStateEst->getMap()->getHeading(angle, exitWayptLabel);
    
  Log::getStream(4) << "PLANNER:  exit waypoint label " << exitWayptLabel << endl;
  Log::getStream(4) << "PLANNER:  currPos " << currPos << endl;
  Log::getStream(4) << "PLANNER:  exit waypoint " << exitWaypt << endl;

  dotProd = (-exitWaypt.x+currPos.x)*cos(angle) + (-exitWaypt.y+currPos.y)*sin(angle);

  Log::getStream(1) << "PLANNER:  dot product  = " << dotProd << endl;
  
  AliceHeading = AliceStateHelper::getHeading(m_traffStateEst->getVehState());
  absDist = exitWaypt.dist(currPos);

  double anglePiMinusPi;
  PlannerUtils::addAngles(anglePiMinusPi,angle,-AliceHeading);
  headingDiff = anglePiMinusPi;


  if((dotProd>-completeDist) /* && (fabs(headingDiff)<M_PI/6) */ && (absDist<absMaxDist)) {
    completed = true;
  } else {
    completed = false;
  }

  return completed;
}


bool Planner::isGoalComplete(SegGoals segGoals)
{
  double completeDist = 0.5;
  bool completed = false;
  double angle,dotProd,AliceHeading,absDist,headingDiff; 
  double absMaxDist = 10;

  PointLabel exitWayptLabel = PointLabel(segGoals.exitSegmentID, segGoals.exitLaneID, segGoals.exitWaypointID);
  GraphNode *exitNode = NULL;

  /* Check if we are in a zone region */ 
  exitNode = m_graph->getNodeFromRndfId(segGoals.exitSegmentID, segGoals.exitLaneID, segGoals.exitWaypointID);

  if (exitNode == NULL)
    return completed;

  if (NULL != exitNode) {
    absMaxDist = 2;
  }
  
  point2 exitWaypt, currPos;

  exitWaypt.set(exitNode->pose.pos.x, exitNode->pose.pos.y);
  currPos = AliceStateHelper::getPositionFrontBumper(m_traffStateEst->getVehState());
    
  m_traffStateEst->getMap()->getHeading(angle, exitWayptLabel);
    
  Log::getStream(4) << "PLANNER:  exit waypoint label " << exitWayptLabel << endl;
  Log::getStream(4) << "PLANNER:  currPos " << currPos << endl;
  Log::getStream(4) << "PLANNER:  exit waypoint " << exitWaypt << endl;

  dotProd = (-exitWaypt.x+currPos.x)*cos(angle) + (-exitWaypt.y+currPos.y)*sin(angle);

  Log::getStream(1) << "PLANNER:  dot product  = " << dotProd << endl;
  
  AliceHeading = AliceStateHelper::getHeading(m_traffStateEst->getVehState());
  absDist = exitWaypt.dist(currPos);

  double anglePiMinusPi;
  PlannerUtils::addAngles(anglePiMinusPi,angle,-AliceHeading);
  headingDiff = anglePiMinusPi;


  if((dotProd>-completeDist) /* && (fabs(headingDiff)<M_PI/6) */ && (absDist<absMaxDist)) {
    completed = true;
  } else {
    completed = false;
  }

  return completed;
}

bool Planner::isCurrentPathComplete() {

  double maxDistToProj =  10; 
  double maxPathExtension = 10;

  point2 currVehPos, projVehPos, exitWaypt, projExit, tmpPt;
  point2arr pointsPath; 

  double distVeh, distExit;
  double maxCompleteDist = 3; //magic number fixable  
  double aliceHeading = AliceStateHelper::getHeading(m_traffStateEst->getVehState());

  /* If there is no path, then we assume we do not complete */
  if (m_path.pathLen <= 1) {
    return false; 
  }

  /* First get the path into a point2 array */
  for (int i = 0; i < m_path.pathLen; i++) {
    pointsPath.push_back(point2(m_path.path[i]->pose.pos.x, m_path.path[i]->pose.pos.y));
  }
  
  bool exitInZone = false; 
  
  /* Get exit waypoint */
  PointLabel exitWayptLabel = PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID);
  GraphNode *exitNode =  m_graph->getNodeFromRndfId(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID);

  if  (GRAPH_NODE_ZONE == exitNode->type) {
    exitInZone = true; 
    maxCompleteDist = 0.5; 
  }

  Log::getStream(4) << "PLANNER: isCurrPathComplete: exitInZone " << exitInZone<<endl; 
  
  exitWaypt.set(exitNode->pose.pos.x ,exitNode->pose.pos.y,0.0); 
  
  Log::getStream(4) << "PLANNER: isCurrPathComplete: m_currSegGoals ID " <<m_currSegGoals.goalID<< ", exitWaypoint " << exitWaypt << endl;
  
  //unsigned int k = 0;
  
  double r, p, y;  
  quat_to_rpy(exitNode->pose.rot, &r, &p, &y);
  if ( 0 != Utils::distToProjectedPoint(projExit, distExit, exitWaypt, y, pointsPath, maxDistToProj, maxPathExtension))
    return false;
  
  Log::getStream(4) << "PLANNER: isCurrPathComplete, projected exit point = " <<projExit<<endl;

  
  /* Get Alice current position and project it onto the path */
  currVehPos.set(AliceStateHelper::getPositionFrontBumper(m_traffStateEst->getVehState()));
  
  if ( 0 != Utils::distToProjectedPoint(tmpPt, distVeh, currVehPos, aliceHeading, pointsPath, maxDistToProj, maxPathExtension))
      return false;
  
  Log::getStream(4) << "PLANNER: isCurrPathComplete: distVeh " << distVeh << ", distExit " << distExit << ", distExit - distVeh " << distExit - distVeh<<endl;
  
  /* Now measure the distance along the path */
  if ((m_currSegGoals.segment_type != SegGoals::INTERSECTION && fabs(distExit - distVeh) <= maxCompleteDist) ||
      (m_currSegGoals.segment_type == SegGoals::INTERSECTION && (distExit - distVeh < -3.0) && fabs(distExit - distVeh) <= 2.0*maxCompleteDist+3.0)) {
    if (exitInZone) 
      m_replanInZone = true;

    return true; 
  }

  return false; 
}

/**
 * @brief Gets the current time
 */
uint64_t Planner::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

/**
 * @brief Prints the new directive
 */
void Planner::printDirective(SegGoals* newDirective) 
{
  Log::getStream(1) << "PLANNER: New dir recvd, ID "<<newDirective->goalID<<",type "<<newDirective->segment_type;
  Log::getStream(1) << ", intersection type "<<newDirective->intersection_type;
  Log::getStream(1) << ", illegalPassingAllowed "<<newDirective->illegalPassingAllowed; 
  Log::getStream(1) << ", exit waypt ";
  Log::getStream(1) << newDirective->exitSegmentID<<"."<<newDirective->exitLaneID<<"."<<newDirective->exitWaypointID<<endl;
}

/**
 * @brief Gets the next final point based on the current state and the directive queue
 */
pose3_t Planner::getFinalPose(StateProblem_t problem, PlannerMergedDirective *mergedDirective, point2 &finalPoint, double &heading)
{
  pose3_t finPose;
  VehicleState vehState = m_traffStateEst->getVehState();

  if (mergedDirective->segType == SegGoals::EMERGENCY_STOP || mergedDirective->segType == SegGoals::RESET ||
      mergedDirective->segType == SegGoals::END_OF_MISSION || mergedDirective->segType == SegGoals::UTURN) {
    finalPoint.set(vehState.localX, vehState.localY);
    heading = vehState.localYaw;
  } else {
    double r, p;
    GraphNode *node;
    SegGoals *goal;
    SegGoals *prev_goal = &m_currSegGoals;
    SegGoals *next_goal = &m_currSegGoals;

    if (prev_goal->segment_type != SegGoals::ROAD_SEGMENT) {
      if (prev_goal->segment_type != SegGoals::INTERSECTION)
        next_goal = prev_goal;
      else {
        if (m_accSegGoalsQ.size() > 1) {
          next_goal = &(m_accSegGoalsQ[1]);
        } else {
          next_goal = prev_goal;
        }
      }
    } else {
      for (unsigned int i=1; i<m_accSegGoalsQ.size(); i++) {
        goal = &(m_accSegGoalsQ[i]);

        /* If the goal is neither an intersection, nor a road, break */
        if (goal->segment_type != SegGoals::INTERSECTION && goal->segment_type != SegGoals::ROAD_SEGMENT) {
          next_goal = prev_goal;
          break;
        }

        /* If the goal is a Checkpoint go there */
        if (goal->isExitCheckpoint) {
          next_goal = goal;
          break;
        }
  
        /* If it is an intersection stop looking */
        if (goal->segment_type == SegGoals::INTERSECTION) {
          next_goal = goal;
          break;
        }

        /* If it would be unsafe to go there break */
        if (goal->segment_type != SegGoals::INTERSECTION &&
            (m_currSegGoals.exitSegmentID != goal->exitSegmentID ||
             m_currSegGoals.exitLaneID != goal->exitLaneID ||
             m_currSegGoals.exitWaypointID > goal->exitWaypointID)) {
          next_goal = prev_goal;
          break;
        }

        prev_goal = goal;
      }
    }

    node = m_graph->getNodeFromRndfId(next_goal->exitSegmentID, next_goal->exitLaneID, next_goal->exitWaypointID);
    if (node == NULL) {
      finalPoint.set(vehState.localX, vehState.localY);
      heading = vehState.localYaw;
    } else {
      finalPoint.set(node->pose.pos.x, node->pose.pos.y);
      quat_to_rpy(node->pose.rot, &r, &p, &heading);
    }
  }

  finPose.pos.x = finalPoint.x; finPose.pos.y = finalPoint.y; finPose.pos.z = 0;
  finPose.rot = quat_from_rpy(0,0, heading);
  
  return finPose;
}

/**
 * @brief Gets all useful segments
 */
void Planner::getSegments(vector<int> &segments)
{
  LaneLabel current_lane;
  VehicleState vehState = m_traffStateEst->getVehState();
  Map *map = m_traffStateEst->getMap();
  segments.clear();

  if (map->getLane(current_lane, AliceStateHelper::getPositionRearAxle(vehState)) >= 0) {
    segments.push_back(current_lane.segment);
  }

  for (unsigned int i= 0; i < m_accSegGoalsQ.size(); i++) {
    SegGoals goal =  m_accSegGoalsQ[i];
    if (goal.entrySegmentID != 0) segments.push_back(goal.entrySegmentID);
    if (goal.exitSegmentID != 0) segments.push_back(goal.exitSegmentID);
  }
}

/**
 * @brief Determines the signaling
 */
int Planner::determineSignaling(StateProblem_t problem)
{
  LaneLabel current_lane;
  LaneLabel next_lane;
  point2 node_pos;

  /* Reinitialize the exit point */
  m_attentionExitPoint.segment = 0;
  m_attentionExitPoint.lane = 0;
  m_attentionExitPoint.waypoint = 0;

  VehicleState vehState = m_traffStateEst->getVehState();
  Map *map = m_traffStateEst->getMap();
  if (!m_graph->vehicleNode) return 0;
  current_lane.segment = m_graph->vehicleNode->segmentId;
  current_lane.lane = m_graph->vehicleNode->laneId;

  /* Am I UTURNing? */
  if (problem.state == UTURN) {
    m_attentionPlanState = PlanningState::UTURN;
    return 0;
  }

  /* Am I overtaking? THIS needs to be fixed*/
  GraphNode *node;
  int signal = 0;
  for (int i = 1; i<m_path.pathLen; i++) {
    if (i > 20) break;
    node = m_path.path[i];
    // node_pos.set(node->pose.pos.x, node->pose.pos.y);
    next_lane.segment = node->segmentId;
    next_lane.lane = node->laneId;
    // map->getLane(next_lane, node_pos);
    signal = PlannerUtils::getLaneSide(map, vehState, current_lane, next_lane);
    if (signal != 0) {
      if (-1 == signal)
	m_attentionPlanState = PlanningState::PASS_LEFT;
      else if (1 == signal)  
	m_attentionPlanState = PlanningState::PASS_RIGHT;
      return signal;
    }
  }

  /* Am I close to an intersection  ? */
  
  SegGoals segGoals;
  double dist_stopline =  Utils::getDistToStopline(vehState, m_graph, m_traffStateEst->getMap(), &m_path, &m_accSegGoalsQ, segGoals);

  Log::getStream(4)<<"PLANNER_INT: dist_StopLine "<<dist_stopline<<endl;
  Log::getStream(4)<<"PLANNER_INT: segGoals "<<segGoals.toString()<<endl;
  Log::getStream(4)<<"PLANNER_INT: m_currSegGoals "<<m_currSegGoals.toString()<<endl;
 
  if (SegGoals::INTERSECTION_STRAIGHT == segGoals.intersection_type) {
    m_attentionPlanState = PlanningState::INTERSECT_STRAIGHT;
    signal = 0;
  }

  if ((dist_stopline < 30.0 && dist_stopline > 0.0) || (segGoals.goalID == m_currSegGoals.goalID && !isGoalComplete(segGoals))) {	
    if (SegGoals::INTERSECTION_LEFT == segGoals.intersection_type) {
      m_attentionPlanState = PlanningState::INTERSECT_LEFT;
      signal = -1;
    } else if (SegGoals::INTERSECTION_RIGHT == segGoals.intersection_type) {
      m_attentionPlanState = PlanningState::INTERSECT_RIGHT;
      signal = 1;
    }
  }

  /* Populate the exit point */
  m_attentionExitPoint.segment = segGoals.exitSegmentID;
  m_attentionExitPoint.lane = segGoals.exitLaneID;
  m_attentionExitPoint.waypoint = segGoals.exitWaypointID;

  return signal;
}

/**
 * @brief Sends the turn signal to Adrive
 */
void Planner::sendTurnSignalCommand(int signal)
{
  static AdriveDirective adriveDir;
  static int dirID = 0;

  adriveDir.id = ++dirID;
  adriveDir.actuator = TurnSignal;
  adriveDir.command = SetPosition;
  adriveDir.arg = signal;
  m_traffAdriveInterfaceSF->sendDirective(&adriveDir);
  Log::getStream(4) << "PLANNER: Sent turn signal " << adriveDir.arg << endl;
}

/*******************
 * BACKUP handling *
 *******************/

Err_t Planner::planBackup(CTraj *traj, VehicleState &vehState, Map *map)
{
  // To send the direction
  static int superConSendSocket = m_skynet.get_send_sock(SNsuperconTrajfCmd);
  static struct superConTrajFcmd scCmd;

  static bool initialized = false;
  static Vehicle *vp = maneuver_create_vehicle(VEHICLE_WHEELBASE, 30.0 * M_PI/180);
  static Maneuver *mp = NULL;
  static double speeds[19] = { 0.25, 0.5, 0.8, 0.9, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0 };
  static point2 start_pt, end_pt, tmp_point;
  static Pose2D start, end;
  static int old_direction = 1;
  static int direction = 1;
  static LaneLabel current_lane;

  if (!initialized) {
    Console::addMessage("BACKUP: Starting");
    old_direction = 1;

    /* Generate start point */
    start_pt = AliceStateHelper::getPositionRearAxle(vehState);
    start.x = start_pt.x;
    start.y = start_pt.y;
    start.theta = AliceStateHelper::getHeading(vehState);

    /* Set all the variable necessary for the rest of the UTurn */
    map->getLane(current_lane, start_pt);

    /* planning in reverse */
    start.theta = PlannerUtils::getAngleInRange(start.theta + M_PI);
    direction = -1;

    /* getting end point 10m behind */
    map->getLaneCenterPoint(end_pt, current_lane, start_pt, -5.0);
    map->getHeading(end.theta, tmp_point, current_lane, end_pt);
    end.theta = PlannerUtils::getAngleInRange(end.theta + M_PI);
    end.x = end_pt.x;
    end.y = end_pt.y;

    /* generate maneuver */
    if (mp) maneuver_free(mp);
    mp = maneuver_twopoint(vp, &start, &end);

    initialized = true;
  }

  /* create CTraj from the maneuver */
  traj->startDataInput();
  maneuver_profile_single(vp, mp, 19, speeds, 20, traj);
  traj->setDirection(direction);

  /* send direction */
  if (old_direction != direction) {
    if (direction == 1) scCmd.commandType = tf_forwards;
    else scCmd.commandType = tf_reverse;
    m_skynet.send_msg(superConSendSocket, &scCmd, sizeof(scCmd), 0);
    old_direction = direction;
  }

  /* Am I at the end of the segment? */
  point2 pos = AliceStateHelper::getPositionRearAxle(vehState);
  double min_dist = INFINITY;
  double dist;
  double x, y;
  int min_index;
  if (PlannerUtils::isStopped(vehState)) {
    for (int i=0; i<traj->getNumPoints(); i++) {
      x = traj->getNorthing(i);
      y = traj->getEasting(i);
      dist = sqrt(pow(x-pos.x,2)+pow(y-pos.y,2));
      if (dist < min_dist) {
        min_dist = dist;
        min_index = i;
      }
    }
    /* Check if we are still close to the trajectory */
    if (min_dist > 10.0) {
      Console::addMessage("BACKUP: robust replan");
      initialized = false;
      scCmd.commandType = tf_forwards;
      m_skynet.send_msg(superConSendSocket, &scCmd, sizeof(scCmd), 0);
    }
    /* Check if at the end of the segment */
    if (min_index > traj->getNumPoints()-3) {
      Console::addMessage("BACKUP: Completed");
      initialized = false;
      scCmd.commandType = tf_forwards;
      m_skynet.send_msg(superConSendSocket, &scCmd, sizeof(scCmd), 0);
      return VP_BACKUP_FINISHED;
    }
  }

  return VP_OK;
}

/******************
 * UTURN handling *
 ******************/

Err_t Planner::planUTurn(CTraj *traj, VehicleState &vehState, Map *map, PointLabel curr_endpoint)
{
  // To send the direction
  static int superConSendSocket = m_skynet.get_send_sock(SNsuperconTrajfCmd);
  static struct superConTrajFcmd scCmd;

  // each stage will initialize some points to go to
  static Vehicle *vp = maneuver_create_vehicle(VEHICLE_WHEELBASE, 30.0 * M_PI/180);
  static Maneuver *mp = NULL;
  static double speeds[19] = { 0.25, 0.5, 0.8, 0.9, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0 };
  static point2 start_pt, end_pt, tmp_point;
  static Pose2D start, end;
  static int old_direction = 1;
  static int direction = 1;
  static LaneLabel current_lane;
  static LaneLabel opposite_lane;

  if (!uturn_initialized) {
    Console::addMessage("UTURN: Starting stage %d (going to %d.%d)", uturn_stage, curr_endpoint.segment, curr_endpoint.lane);

    /* Generate start point */
    start_pt = AliceStateHelper::getPositionRearAxle(vehState);
    start.x = start_pt.x;
    start.y = start_pt.y;
    start.theta = AliceStateHelper::getHeading(vehState);

    /* Generate end point */
    switch (uturn_stage) {
      case 0:
        old_direction = 1;

        /* Set all the variable necessary for the rest of the UTurn */
        map->getLane(current_lane, start_pt);
        opposite_lane = LaneLabel(curr_endpoint.segment, curr_endpoint.lane);

        /* go 90 */
        direction = 1;

        /* getting end point 5m ahead on the other road */
        map->getLaneRightPoint(end_pt, opposite_lane, start_pt, -8.0);
        map->getHeading(end.theta, tmp_point, opposite_lane, end_pt);
        end.theta = PlannerUtils::getAngleInRange(end.theta + M_PI/2);
        break;
      case 1:
        old_direction = 1;

        /* go backward */
        start.theta = PlannerUtils::getAngleInRange(start.theta + M_PI);
        direction = -1;

        /* getting end point 5m back on the other road */
        map->getLaneRightPoint(end_pt, current_lane, start_pt, 4.0);
        map->getHeading(end.theta, tmp_point, current_lane, end_pt);
        end.theta = PlannerUtils::getAngleInRange(end.theta + M_PI/4);
        break;
      case 2:
        old_direction = -1;

        /* go forward */
        direction = 1;

        /* getting on lane */
        map->getLaneCenterPoint(end_pt, opposite_lane, start_pt, 8.0);
        map->getHeading(end.theta, tmp_point, opposite_lane, end_pt);
        break;
    }
    end.x = end_pt.x;
    end.y = end_pt.y;

    /* generate maneuver */
    if (mp) maneuver_free(mp);
    mp = maneuver_twopoint(vp, &start, &end);

    uturn_initialized = true;
  }

  /* create CTraj from the maneuver */
  traj->startDataInput();
  maneuver_profile_single(vp, mp, 19, speeds, 20, traj);
  traj->setDirection(direction);

  /* send direction */
  if (old_direction != direction) {
    if (direction == 1) scCmd.commandType = tf_forwards;
    else scCmd.commandType = tf_reverse;
    m_skynet.send_msg(superConSendSocket, &scCmd, sizeof(scCmd), 0);
    old_direction = direction;
  }

  /* Am I at the end of the segment? */
  point2 pos = AliceStateHelper::getPositionRearAxle(vehState);
  double min_dist = INFINITY;
  double dist;
  double x, y;
  int min_index;
  if (PlannerUtils::isStopped(vehState)) {
    for (int i=0; i<traj->getNumPoints(); i++) {
      x = traj->getNorthing(i);
      y = traj->getEasting(i);
      dist = sqrt(pow(x-pos.x,2)+pow(y-pos.y,2));
      if (dist < min_dist) {
        min_dist = dist;
        min_index = i;
      }
    }
    /* Check if we are still close to the trajectory */
    if (min_dist > 20.0) {
      Console::addMessage("UTURN: robust replan");
      uturn_initialized = false;
      uturn_stage = 0;
      scCmd.commandType = tf_forwards;
      m_skynet.send_msg(superConSendSocket, &scCmd, sizeof(scCmd), 0);
    }
    Console::addMessage("UTURN: min_index %d (%d)", min_index, traj->getNumPoints());
    if (min_index > traj->getNumPoints()-6) {
      Console::addMessage("UTURN: Completed stage %d", uturn_stage);
      uturn_initialized = false;
      uturn_stage++;
      if (uturn_stage == 3) {
        uturn_stage = 0;
        return VP_UTURN_FINISHED;
      }
    }
  }

  return VP_OK;
}

void Planner::sendPlanningState(vector<StateProblem_t> stateProb) {
  PlannerState plannerStateDir; 
  plannerStateDir.mode = m_attentionPlanState; 
  plannerStateDir.exitPoint = m_attentionExitPoint;
  Log::getStream(4) << "Attention plan state " << m_attentionPlanState << " exit point " << m_attentionExitPoint<< endl;  
  m_plannerStateInterfaceSF->sendDirective(&plannerStateDir);
}


int Planner::determineZoneAction(PointLabel exitLabel, int zoneId) {

  /* If the current seg goal exit waypoint corresponds to the end of a parking spot, then we are entering a spot */
  if (zoneId == m_currSegGoals.exitSegmentID && 2 == m_currSegGoals.exitWaypointID) {
    m_prevZoneAction = TRAFFIC_STATE_ENTER_PARK;
  } else {
    vector<PointLabel> exitLabels, exitLinks; 
    m_traffStateEst->getMap()->getZoneExits(exitLabels, exitLinks,zoneId);

    for (unsigned int i=0; i<exitLabels.size(); i++) {

      /* If the exit waypoint correponds to the exit of a zone and we had previously entered a parking spot or in the middle of exiting a spot. */
      if (exitLabel == exitLabels[i] && (TRAFFIC_STATE_ENTER_PARK == m_prevZoneAction || TRAFFIC_STATE_EXIT_PARK == m_prevZoneAction)) {
	m_prevZoneAction = TRAFFIC_STATE_EXIT_PARK;
	return m_prevZoneAction;
	/* If the exit waypoint correponds to the exit of a zone but we previously had not entered a parking spot, we are traversing a zone */
      } else if (exitLabel == exitLabels[i]) {
	m_prevZoneAction = TRAFFIC_STATE_TRAVERSE_ZONE;

      } else {
	m_prevZoneAction = TRAFFIC_STATE_UNSPECIFIED;
      }
    }
  }
  return m_prevZoneAction;
}
