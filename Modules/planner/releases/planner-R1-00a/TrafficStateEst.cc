#include "TrafficStateEst.hh"
#include "interfaces/sn_types.h"
#include "Log.hh"
#include "CmdArgs.hh"

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(bool waitForStateFill) 
  : CSkynetContainer(MODtrafficplanner, CmdArgs::sn_key)
    , CStateClient(waitForStateFill)
{
  DGCcreateMutex(&m_localMapUpMutex);

  if (CmdArgs::use_RNDF){
    Log::getStream(1)<<"I am using the RNDF"<<endl;
    if (!CmdArgs::RNDF_file.empty()){
      Log::getStream(1)<<"RNDF is not empty "<<endl;
      m_mapElemTalker.initRecvMapElement(CmdArgs::sn_key, 1);
      m_mapElemTalker.initSendMapElement(CmdArgs::sn_key);
    } else {
      Log::getStream(1)<<"TFEST: RNDFfilename is empty"<<endl;
    }
  } else {
    Log::getStream(1)<<"TFEST: not using RNDF"<<endl;
  }

  m_localUpdateMap = new Map();
  m_localMap = new Map();

  loadRNDF(CmdArgs::RNDF_file);

  UpdateState();
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;
  DGClockMutex(&m_localMapUpMutex);
  m_localUpdateMap->prior.delta = statedelta;
  DGCunlockMutex(&m_localMapUpMutex);
}

TrafficStateEst::~TrafficStateEst() 
{
  delete m_localMap;
  delete m_localUpdateMap;
  DGCdeleteMutex(&m_localMapUpMutex);
}


TrafficStateEst* TrafficStateEst::Instance(bool waitForStateFill)
{
    if (pinstance == 0)
        pinstance = new TrafficStateEst(waitForStateFill);
    return pinstance;
}

void TrafficStateEst::Destroy()
{
    delete pinstance;
    pinstance = 0;
}

void TrafficStateEst::updateVehState() 
{
  UpdateState();
  m_currVehState = m_state; 
}

VehicleState TrafficStateEst::getVehState() 
{
  return m_currVehState;
}

void TrafficStateEst::getLocalMapUpdate()
{
  MapElement recvEl;
  int bytesRecv;
  Log::getStream(1) << "In local map update ..." <<endl; 
  while (true) {
    bytesRecv = m_mapElemTalker.recvMapElementBlock(&recvEl,1);
    
    if (bytesRecv>0){
      DGClockMutex(&m_localMapUpMutex);
      m_localUpdateMap->addEl(recvEl);
      DGCunlockMutex(&m_localMapUpMutex);
    }else {
      Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
	                      << bytesRecv << endl;
      usleep(100);
    }
  }
}

void TrafficStateEst::updateMap() {

  DGClockMutex(&m_localMapUpMutex);
  *m_localMap = *m_localUpdateMap;
  DGCunlockMutex(&m_localMapUpMutex);
  m_gloToLocalDelta = m_localMap->prior.delta;

}

Map* TrafficStateEst::getMap() {
  return m_localMap;
}

bool TrafficStateEst::loadRNDF(string filename) {
  
  m_localUpdateMap->loadRNDF(filename);
  return m_localMap->loadRNDF(filename);
}


