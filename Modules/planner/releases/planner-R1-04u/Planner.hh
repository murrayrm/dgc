/*!
 * \file Planner.hh
 * \brief Header for planner class
 *
 * \author Noel duToit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef PLANNER_HH_
#define PLANNER_HH_

#include <interfaces/VehicleState.h>
#include <gcmodule/GcModule.hh>
#include <gcmodule/GcInterface.hh>
#include <gcinterfaces/SegGoals.hh>
#include <gcinterfaces/SegGoalsStatus.hh>
#include <gcinterfaces/AdriveCommand.hh>
#include <gcinterfaces/FollowerCommand.hh>
#include <gcinterfaces/AttentionPlannerState.hh>
#include <trajutils/TrajTalker.hh>

/* To send the direction of the trajectory */
#include <skynettalker/SkynetTalker.hh>

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <path-planner/RailPlanner.hh>
#include <vel-planner/VelPlanner.hh>
#include <logic-planner/LogicPlanner.hh>
#include <logic-planner/IntersectionHandling.hh>
#include <pseudocon/interface_superCon_trajF.hh>
#include <skynettalker/SkynetTalker.hh>
#include <trajfollower/trajF_status_struct.hh>
#include <temp-planner-interfaces/planner_cmdline.h>
#include <temp-planner-interfaces/PlanGraphUpdater.hh>

#include <prediction/Prediction.hh>
#include <path-planner/RailPlanner.hh>
#include <circle-planner/CirclePlanner.hh>
#include "TrafficStateEst.hh"

using namespace sc_interface;
using namespace std;

/*! Input interface from which SegGoals are received from the Route Planner */
typedef GcInterface<SegGoals, SegGoalsStatus, SNsegGoals, SNtplannerStatus, MODmissionplanner> MissionPlannerInterface;

class PlannerControlStatus : public ControlStatus
{
public:
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ BLOCKED_LANE, BLOCKED_ROAD, KNOWLEDGE, ACTUATOR_FAILURE, LOST };

  /* The id of the merged directive that this control status corresponds to. */
  unsigned int ID; 
  Status status;
  ReasonForFailure reason;
  int currentSegmentID;
  int currentLaneID;

  bool wasPaused;
};

class PlannerMergedDirective : public MergedDirective
{
public:
 
  PlannerMergedDirective(){};
  ~PlannerMergedDirective(){};

  list<int> segGoalsIDs; 
  SegGoals::SegmentType segType; 
  SegGoals::IntersectionType interType; 

  int exitSegmentID; 
  int exitLaneID; 
  int exitWaypointID;
 
};


struct PlannerMergedDirResp
{
  enum Status{ QUEUED, SENT, ACCEPTED, COMPLETED, FAILED };
  enum ReasonForFailure{ R1, R2, R3 };

  PlannerMergedDirResp()
    :status(QUEUED)
  {
  }

  Status status;
  ReasonForFailure reason;
};


class Planner : public GcModule, public CSkynetContainer /* REMOVE, public CStateClient*/ {

public: 
  
  /*! Constructor */
  Planner(struct planner_options *options);

  /*! Destructor */
  virtual ~Planner();

public:
  
  /*! Arbitration for the traffic planner control module. It computes the next
    merged directive based on the directives from mission control
    and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);
  
  /*! Control for the traffic planner control module. It computes and sends
    directives to all its controlled modules based on the 
    merged directive and outputs the control status
    based on all the status from its controlled modules. */
  void control(ControlStatus*, MergedDirective*);

private:
  
  /*! Returns whether or not the current path is complete based on the current segGoal exit waypoint. */
  bool isGoalComplete();

  /*! Returns the time of day */
  uint64_t getTime();

  /*! Output directive details to standard out */
  void printDirective(SegGoals* newDirective);

  /*! Handles hacked UTurn maneuver */
  Err_t planUTurn(CTraj *traj, VehicleState &vehState, Map *map, PointLabel curr_endpoint);

  /*! Handles hacked Backup maneuver */
  Err_t planBackup(CTraj *traj, VehicleState &vehState, Map *map);

  /*! Finds the next final point we want to reach */
  pose2f_t getFinalPose(StateProblem_t problem, PlannerMergedDirective *directive, point2 &point, double &heading);

  /*! Handles turning signals */
  int determineSignaling(StateProblem_t problem);

  /*! Sends the turning signal command */
  void sendTurnSignalCommand(int signal);

  /*! Get all segments alice might go in */
  void getSegments(vector<int> &segments);

  /*! Send the planning state to the Attention module */
  void sendPlanningState(vector<StateProblem_t> stateProb);

  /*! Plan the trajectory to the given goal. */
  Err_t planTrajectory(VehicleState vehState, ActuatorState actState, vector<StateProblem_t> state_problems, Path_params_t pathParams, Vel_params_t velParams, pose2f_t finPose, Cost_t cost);

  /*! Plan path from pose A to pose B using the rail planner. */
  Err_t planRailTrajectory(VehicleState vehState, ActuatorState actState, StateProblem_t state_problem, Path_params_t pathParams, Vel_params_t velParams, pose2f_t curPose, pose2f_t finPose);

  /*! Determine if we need to replan the path and update m_replanInZone*/
  void replanInZone(PlanGraphPath* path, Err_t error, VehicleState vehState, CSpecs_t* cSpecs, StateProblem_t stateProblem);

  /*! Convert a trajectory from site frame to local frame. */
  void convertTraj(const VehicleState *vehState, CTraj *traj);

  /*! determine the final pose that we want to plan to in a zone/unstructured region. */ 
  pose2f_t getRoadZoneFinPose(StateProblem_t stateProblem, VehicleState vehState);
  /*! determine wether we have completed an intermediate goal such as backing up, or uturn*/
  Err_t checkInternalComplete(StateProblem_t stateProblem, VehicleState vehState, CSpecs_t cSpecs);

  /*! load the planner params*/
  int loadPlannerParams(const char *filename);

public:

  /*! Pointer to the command line options. */
  struct planner_options *m_options;
  
  /*! Time we last generate a plan */
  uint64_t m_planTime;
  
  /*!\param merged directive sent from arbiter to control */
  PlannerMergedDirective m_mergedDirective;

  /*!\param directives currently stored until planning horizon reqs are met */
  deque<SegGoals> m_accSegGoalsQ;

  /*!\Current SegGoals  */
  SegGoals m_currSegGoals;

  /*!\param control status sent from control to arbiter */
  PlannerControlStatus m_controlStatus;

  /*!\param GcInterface variable */
  MissionPlannerInterface::Northface* m_missTraffInterfaceNF;

  /*!\param GcInterface variable */
  AdriveCommand::Southface* m_traffAdriveInterfaceSF;

  /*!\param GcInterface variable */
  FollowerCommand::Southface* m_traffFollowInterfaceSF;

  /*!\param GcInterface variable */
  PlannerStateInterface::SouthfaceLite* m_plannerStateInterfaceSF;

  /*! Response from Follower */
  FollowerResponse m_followResponse;

  bool m_isInit;

  /*!\Singleton intance of the Traffic State Estimator */
  TrafficStateEst* m_traffStateEst;

  bool m_completed;

  /*! UTURN variables */
  bool uturn_initialized;
  int uturn_stage;

  /*!\Adrive Directive  */
  AdriveDirective m_adriveDir; 

  /*! The canonical graph used for rail planning. */
  PlanGraph m_graph;

  /*! The utility to generate instant graphs. */
  PlanGraphUpdater *m_graphUpdater;

  //#if USE_RAILPLANNER
  /*! Rail planner */
  RailPlanner *m_railPlanner;

  /*! Start and end poses for the rail planner (diagnostics) */
  pose2f_t m_railStartPose, m_railGoalPose;
  PlanGraphNode *m_railStartNode, *m_railGoalNode;
 
  //#endif  
  
  PlanGraphPath m_path;
  PlanGraphPath m_pathUturn;
  PlanGraphPath m_subPath;
  PlanGraph* m_gUturn;
  CTraj m_traj;
  /*! Added a second traj that gets converted to local frame and sent to follower*/
  CTraj m_sentTraj;
  CTrajTalker* m_trajTalker;
  
  /*!\Bitmap talker  */
  SkynetTalker< BitmapParams > m_polyTalker;

  //#if USE_PREDICTION
  // Element Prediction
  CPrediction* m_prediction;
  bool m_predictionLaneChange;
  //#endif

  /*!\param m_planFromCurrPos tells path planner whether to enforce 
   * continuity between planning cycles, or to plan from current position.
   */
  bool m_planFromCurrPos;

  PlanningState::Mode m_attentionPlanState; 
  PlanningState::ExitPoint  m_attentionExitPoint; 

  bool m_firstTimeInZone;

  bool m_firstTimeInUTurn;

  point2 m_prevFinalPos, m_currFinalPos;

  /* The zone action that was taken previously */
  int m_prevZoneAction; 

  /* Reset the previously generated paths (for zones) */
  bool m_replanInZone;

  /* Reset the previously generated paths (for zones) */
  bool m_useSubPath;

  PlanGraphPath m_prevSubPath;

  /* Read in the path-planner param file again to update the costs*/
  bool m_readPathPlanParams;

  /* Remember where we were for the last X meters */
  deque<VehicleState> m_vehStateHistory;

  /// Is this the first time we switch into uturn? if so, set the final point
  bool m_firstTimeUTurn;
  /// remember what point we want to plan to with uturn
  point2 m_uTurn_finPos;
  /// remember the heading of our final pos in uturn
  double m_uTurn_finHead;

  /// Is this the first time we switch into backup? if so, set the final point
  bool m_firstTimeBackup;
  /// remember what point we want to plan to with backup
  point2 m_bup_finPos;
  /// remember the heading of our final pos in backup
  double m_bup_finHead;

  /// chooses which updateGraph function to use (for associating obs with nodes)
  /// 0 = old function (updateGraphStateProblem)
  /// 1 = new function (updateStatus)
  int m_updateGraphFunction;

  /// Timeouts in seconds to disable ROA
  unsigned long long m_ROAFailedSince;
  unsigned long long m_ROALastFailure;
  unsigned long long m_ROADisabledSince;
  double m_resetROAStatusTimeout;
  double m_checkROAStatusTimeout;
  double m_disableROATimeout;
  bool m_disableROA;

  /// Remember what the previous planner was that we used
  int m_previousPlanner;
};

#endif /*PLANNER_HH_*/
