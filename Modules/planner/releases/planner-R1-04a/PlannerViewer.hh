
/* 
 * Desc: Planner viewer utility
 * Date: 29 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLANNER_VIEWER_HH
#define PLANNER_VIEWER_HH


#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <dgcutils/Fl_Glv_Window.H>

#include <interfaces/VehicleState.h>
#include <temp-planner-interfaces/planner_cmdline.h>

#include "Planner.hh"


class PlannerViewer
{
  public:

  // Default constructor
  PlannerViewer();

  // Destructor
  ~PlannerViewer();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  // Initialize GUI
  int initGUI(int cols, int rows);

  // Finalize GUI
  int finiGUI();
 
  public:
    
  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  // Action callback
  static void onAction(Fl_Widget *w, int option);

  // Handle draw callbacks
  static void onDraw(Fl_Glv_Window *win, PlannerViewer *self);

  // Handle idle callbacks
  static void onIdle(PlannerViewer *self);

  private:

  // Switch to the vehicle frame
  void pushFrameVehicle(const VehicleState *state);

  // Switch to the local frame
  void pushFrameLocal(const FusedPerceptor *fused);

  // Revert to previous frame
  void popFrame();

  private:
  
  // Draw a set of axes
  void drawAxes(float size);

  // Draw a text box
  void drawText(float size, const char *text);

  // Draw the robot (vehicle frame)
  void drawAlice();

  // Pre-draw the RNDF to a display list
  void predrawRndf();

  // Draw an RNDF waypoint
  void drawRndfNode(GraphNode *node);

  // Pre-draw the graph to a display list
  void predrawGraph(FusedPerceptor *fused, int props);

  // Draw an indivual graph node
  void drawNode(GraphNode *node, int props);

  // Pre-draw the fused map to a display list
  void predrawFused(FusedPerceptor *fused, int props);

  // Draw the trajectory (local frame)
  void drawTraj(CTraj *traj);

  public:

  // Initialize the perceptor
  int init();

  // Finalize the perceptor
  int fini();

  // Update the perceptor
  int update();

  // Pre-draw the local map using an image
  // TODO void predrawLocalMap(LocalMap *map, const VehicleState *state, dgc_image_t *image);

  public:

  // MENU options
  enum
  {
    CMD_ACTION_PAUSE = 0x1000,  

    CMD_VIEW_FIRST  = 0x2000,
    CMD_VIEW_ALICE  = 0x00,
    CMD_VIEW_RNDF   = 0x01,
    CMD_VIEW_GRAPH  = 0x02,
    CMD_VIEW_FUSED  = 0x03,
    CMD_VIEW_TRAJ   = 0x04,
    CMD_VIEW_LAST   = 0x20FF,
    
    CMD_GRAPH_FIRST  = 0x2100,
    CMD_GRAPH_ARCS   = 0x00,
    CMD_GRAPH_RAILS  = 0x01,
    CMD_GRAPH_CHANGES = 0x02,
    CMD_GRAPH_LAST   = 0x21FF,

    CMD_FUSED_FIRST  = 0x2200,
    CMD_FUSED_NONE   = 0x00,
    CMD_FUSED_LINES  = 0x01,
    CMD_FUSED_LDIST  = 0x02,
    CMD_FUSED_LAST   = 0x22FF,
  };
  
  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;
  
  // 3D window
  Fl_Glv_Window *worldwin;

  // Should we pause?
  bool pause;

  // Should we advance a single step?
  bool step;
  
  // Should we quit?
  bool quit;
  
  // Command-line options
  struct planner_options options;

  // Planner object
  Planner *planner;
  
  // Are the display lists dirty?
  bool dirty;

  // When we last set the dirty flag?
  uint64_t dirtyTimestamp;

  // Which layers are we viewing?  This is a bit-mask, with each bit
  // position denoting a seperate layer that is on or off.
  int viewLayers;

  // Which graph properties are enabled?  A bit-mask.
  int graphProps;

  // Which fused map properties are enabled?  A bit-mask.
  int fusedProps;

  // Display lists 
  GLuint rndfList, graphList;
  GLuint fusedTex, fusedList;
  
};

#endif
