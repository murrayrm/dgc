/*!
 * \file TrafficStateEst.cc
 * \brief Some legacy code from the tplanner. Maintains the local map and the vehicle state. Needs to be incorporated into the planner.
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include "TrafficStateEst.hh"
#include <interfaces/sn_types.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/CmdArgs.hh>

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(bool waitForStateFill) 
  : CSkynetContainer(MODtrafficplanner, CmdArgs::sn_key)
  , CStateClient(waitForStateFill)
{
  DGCcreateMutex(&m_localMapUpMutex);

  if (CmdArgs::use_RNDF){
    Log::getStream(1)<<"I am using the RNDF"<<endl;
    if (!CmdArgs::RNDF_file.empty()){
      Log::getStream(1)<<"RNDF is not empty "<<endl;
			

			//Check if the internal mapper is being used
			//if (!CmdArgs::mapper_use_internal){
        m_mapElemTalker.initSendMapElement(CmdArgs::sn_key);
        m_mapElemTalker.initRecvMapElement(CmdArgs::sn_key, 1);
				//}
    
    } else {
      Log::getStream(1)<<"TFEST: RNDFfilename is empty"<<endl;
    }
  } else {
    Log::getStream(1)<<"TFEST: not using RNDF"<<endl;
  }

	if (CmdArgs::mapper_use_internal){
		m_mapper = new Mapper();
		assert(m_mapper);
		m_mapper->initComm(CmdArgs::sn_key);
		m_mapper->init(CmdArgs::RNDF_file);
			m_mapper->setLocalFrameOffset(getVehState());
		m_mapper->disableLineFusion(CmdArgs::mapper_disable_line_fusion);
		m_mapper->disableObsFusion(CmdArgs::mapper_disable_obs_fusion);
		m_mapper->decayAgeThresh = CmdArgs::mapper_decay_thresh;
    m_mapper->plannerOutputDisabled = true;
    m_mapper->debugSubGroup = CmdArgs::mapper_debug_subgroup;
	}



  m_localMap = new Map(!CmdArgs::mapper_disable_line_fusion);
	m_localUpdateMap = new Map(!CmdArgs::mapper_disable_line_fusion);
	
	loadRNDF(CmdArgs::RNDF_file);
	
	UpdateState();
	point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
	m_localMap->setTransform(statedelta);
	DGClockMutex(&m_localMapUpMutex);
	m_localUpdateMap->setTransform(statedelta);
	DGCunlockMutex(&m_localMapUpMutex);


	


	
}

TrafficStateEst::~TrafficStateEst() 
{
  delete m_localMap;
	delete m_localUpdateMap;	
	if (CmdArgs::mapper_use_internal){
		delete m_mapper;
	}
		
  DGCdeleteMutex(&m_localMapUpMutex);
}


TrafficStateEst* TrafficStateEst::Instance(bool waitForStateFill)
{
    if (pinstance == 0)
        pinstance = new TrafficStateEst(waitForStateFill);
    return pinstance;
}

void TrafficStateEst::Destroy()
{
    delete pinstance;
    pinstance = 0;
}

void TrafficStateEst::updateVehState() 
{
  UpdateState();
  m_currVehState = m_state; 
}

VehicleState TrafficStateEst::getVehState() 
{
  return m_currVehState;
}

void TrafficStateEst::getLocalMapUpdate()
{
  MapElement recvEl;
  int bytesRecv;
  Log::getStream(1) << "In local map update ..." <<endl; 

	if (CmdArgs::mapper_use_internal){

		while (true) {
			m_mapper->setLocalFrameOffset(getVehState());
			
			if (m_mapper->mainLoop() !=0){
				Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received error from mapper:updateMap = " << endl;
      }
			
			DGClockMutex(&m_localMapUpMutex);
			*m_localUpdateMap = m_mapper->map;
			DGCunlockMutex(&m_localMapUpMutex);
		}
	}else{
		while (true) {
			bytesRecv = m_mapElemTalker.recvMapElementBlock(&recvEl,1);
			
			if (bytesRecv>0){
				DGClockMutex(&m_localMapUpMutex);
				m_localUpdateMap->addEl(recvEl);
				DGCunlockMutex(&m_localMapUpMutex);
			}else {
				Log::getStream(1) << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
													<< bytesRecv << endl;
			}
		}
	}
}

void TrafficStateEst::updateMap() {

  DGClockMutex(&m_localMapUpMutex);
  *m_localMap = *m_localUpdateMap;
  DGCunlockMutex(&m_localMapUpMutex);
  m_gloToLocalDelta = m_localMap->prior.delta;

}

Map* TrafficStateEst::getMap() {
  return m_localMap;
}


bool TrafficStateEst::loadRNDF(string filename) {

  m_localUpdateMap->loadRNDF(filename);
  return m_localMap->loadRNDF(filename);
}


