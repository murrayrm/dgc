#include <ncurses.h>
#include <sstream>
#include <fstream>

#include <interfaces/VehicleState.h>
#include <trajutils/traj.hh>

class Console {

  public:
    static void init();
    static void refresh();
    static void destroy();
 
    static void updateTrajectory(CTraj *traj);
    static void updateState(VehicleState &vehState);
    static void updateQueueing(bool queueing);
    static void updateTurning(int direction);
    static void updateTrafficState();
    static void updateControlState();
    static void addMessage(char *format, ...);

  private:
    static WINDOW *fsm_win;
    static WINDOW *traj_win;
    static WINDOW *state_win;
    static WINDOW *msg_win;
    static char messages[6][77];
    static bool initialized;
};
