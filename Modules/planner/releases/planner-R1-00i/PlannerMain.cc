/*!
 * \file PlannerMain.cc
 * \brief Main planner execution entry point
 *
 * \author Noel duToit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include <getopt.h>
#include "cmdline.h"
#include <iostream>
#include "Planner.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/cfgfile.h"
#include "skynet/skynet.hh"
#include <sys/time.h>
#include <sys/stat.h>
#include "temp-planner-interfaces/CmdArgs.hh"
#include "temp-planner-interfaces/Console.hh"

using namespace std;             

/**
 * @brief Entry point of the planner
 *
 * This function populates the CmdArgs static class
 * and starts the planner
 */
int main(int argc, char **argv)              
{
  gengetopt_args_info cmdline;    
  string logFileName;

  /* Get the skynet key */
  CmdArgs::sn_key = skynet_findkey(argc, argv);
 
  /* Parse command-line arguments */
  if (cmdline_parser(argc, argv, &cmdline) != 0)
    exit (1);

  /* Is console enabled? */
  CmdArgs::console = !cmdline.disable_console_given;

  /* What is the verbose level? */
  if (cmdline.verbose_given) {
    CmdArgs::verbose_level = cmdline.verbose_arg;
    if (CmdArgs::verbose_level > 0) CmdArgs::debug = true;
  }

  if (cmdline.log_level_given) {
    CmdArgs::log_level = cmdline.log_level_arg;
  }
    
  /* Get the RNDF filename */
  if (cmdline.rndf_given){
    CmdArgs::use_RNDF = true;
    CmdArgs::RNDF_file = cmdline.rndf_arg;
    if (!CmdArgs::console)
      cout << "RNDF Filename in = "  << CmdArgs::RNDF_file << endl;
    /* The RNDF gets loaded into the map inside the Planner constructor */
  }     

  /* Plan from current pos? (closed loop)*/
  CmdArgs::closed_loop = cmdline.closed_loop_given;

  /* Is logging enabled? */
  CmdArgs::logging = cmdline.log_given;

  /* What is the log path? */
  CmdArgs::log_path = cmdline.log_path_arg;

  /* Generate a unique filename to log to */
  if(CmdArgs::logging) {
    string tmpRNDFname;
  
    ostringstream oss;
    struct stat st;
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
    tmpRNDFname.assign(CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("/")+1, CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("."));
    oss << cmdline.log_path_arg << "planner-" << tmpRNDFname << "." << timestr << ".log";
    logFileName = oss.str();
    string suffix = "";

    /* if it exists already, append .1, .2, .3 ... */
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
    CmdArgs::log_filename = logFileName;
  }

  /* Display that console is not enabled */
  if (!CmdArgs::console){
    cout << "No display" << endl;
    cout << "Constructing skynet with KEY = " << CmdArgs::sn_key << endl;
    cout << "debug = " << CmdArgs::debug << endl;
    cout << "verbose level = " << CmdArgs::verbose_level << endl;
  }

  /* To be removed (are we using this?) */
  CmdArgs::lane_cost = true;

  /* Initializes Planner and start the gcmodule */
  Planner* planner = new Planner(); 
  planner->Start();

  /* Refreshing the console */
  unsigned int cnt = 0;
  while (true) {
    /* Refresh console every 100 changes
     * Refreshing every changes would create flickers on the
     * terminal (not very nice for the eye) */
    Console::thread_refresh();
    cnt++;
    if (cnt > 100) {
      cnt = 0;
      Console::refresh();
    }
  }

  return 0;
}

