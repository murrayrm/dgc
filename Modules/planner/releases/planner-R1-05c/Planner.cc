/*!
 * \file Planner.cc
 * \brief Source for planner class
 *
 * \author Noel duToit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include "Planner.hh"
#include <dgcutils/DGCutils.hh>
#include <trajutils/man2traj.hh>
#include <path-planner/PathUtils.hh>
#include "temp-planner-interfaces/Utils.hh"
#include "temp-planner-interfaces/CmdArgs.hh"
#include "temp-planner-interfaces/Console.hh"
#include "PlannerUtils.hh"
#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/ConfigFile.hh>

//#if USE_S1PLANNER
#include "s1planner/tempClothoidInterface.hh"
//#endif

#define S1THRUST 3

#if USE_OTG
#include "/dgc/otg/include/otg.hh"
#endif

#define MAX_DOUBLE 0x7FFFFF

/**
 * @brief Constructor of the Planner class
 *
 * This constructor initializes the North and South faces of the
gcmodule interface,
 * it activates the logging mechanism and sets the verbose level. It also starts
 * the TrafficStateEst class (which is a snapshot of the environment).
Finally it
 * starts the different libraries that compose the planner module.
 */
Planner::Planner(struct planner_options *options)
   : GcModule("Planner", &m_controlStatus, &m_mergedDirective, 10000, 1000)
 , CSkynetContainer(MODtrafficplanner, CmdArgs::sn_key)
 , m_isInit(false)
 , m_completed(false)
 , m_polyTalker(CmdArgs::sn_key, SNbitmapParams, MODtrafficplanner)
 , m_planFromCurrPos(true)
 , m_attentionPlanState(PlanningState::NOMINAL)
 , m_attentionExitPoint(0,0,0)
 , m_firstTimeInZone(true)
 , m_firstTimeInUTurn(true)
 , m_prevZoneAction(TRAFFIC_STATE_UNSPECIFIED)
 , m_replanInZone(true)
 , m_useSubPath(false)
 , m_readPathPlanParams(false)
 , m_firstTimeUTurn(true)
 , m_firstTimeBackup(true)
{

  m_options = options;
  // REMOVE m_planTime = 0;

  /* Initializes the traffic state estimator */
  m_traffStateEst = TrafficStateEst::Instance(true);

  // Compute the offset between the site frame and the global (UTM) frame.
  // We need to know the vehicle state to compute this.
  vec3_t siteOffset;
  VehicleState vehState;
  m_traffStateEst->updateState();
  vehState = m_traffStateEst->getVehState();
  siteOffset = vec3_set(vehState.utmNorthing - vehState.siteNorthing,
                        vehState.utmEasting - vehState.siteEasting,
                        vehState.utmAltitude - vehState.siteAltitude);
  fprintf(stderr, "using site offset %.0f %.0f %.0f\n", siteOffset.x,
          siteOffset.y, siteOffset.z);
  m_vehStateHistory.push_front(vehState);
  
  /* Load the pre-built graph */
  char filename[1024];
  snprintf(filename, sizeof(filename), "%s.pg", CmdArgs::RNDF_file.c_str());
  if (m_graph.load(filename) != 0)
  {
    Log::getStream(1) << "Unable to load " << filename;
    exit(-1);
  }

  /* Initialize paths */
  m_path.pathLen = 0;
  m_pathUturn.pathLen = 0;
  m_subPath.pathLen = 0;
  
  point2 m_evasivePos;

  /* Initialize the graph-updater */
  m_graphUpdater = new PlanGraphUpdater(&m_graph);

  /* Initialize the Utils function in temp-planner-interfaces to start up the
   * me talker so that we can send debug info */
  Utils::init();
  
  // Initilize the RRT planner
  if (CmdArgs::use_rrt_planner) {
	  m_rrtPlanner = new RRTPlanner(&m_graph);
  }
  else{
  // Initialize the rail planner
  m_railPlanner = new RailPlanner(&m_graph);

  // Load the weights from a file
  m_railPlanner->loadWeights(CmdArgs::path_plan_config);
  }
  m_previousPlanner = RAIL_PLANNER;


  /* Set the verbose level */
  Log::setVerboseLevel(CmdArgs::verbose_level);

  /* Set the path to the log file */
  if (CmdArgs::logging) {
    Log::setGenericLogFile(CmdArgs::log_filename.c_str());
  }

  // Use new or old logic planner(s)?
  if(CmdArgs::mod_log_planner) {
	  Log::getStream(1) << "\nInitializing new Modular Logic Planner...\n";
	  // Init new modular logic planner
	  // Automatic initializationof ModLogPlanner, no need for an extra call
	  m_modLogPlanner = new ModLogPlanner();	
  } else if (CmdArgs::lt_planner) {
    Log::getStream(1) << "Initializing a LtPlanner ...\n";
    m_ltPlanner = new LtPlanner();
  } else {
    // Init old logic planner
    // Don't need an instance to call static member functions.
    LogicPlanner::init();
  }

  /* Initialize the vel-planner */
  VelPlanner::init();

  /* Initialize the northface */
  m_missTraffInterfaceNF =
    MissionPlannerInterface::generateNorthface(CmdArgs::sn_key, this);

  /* Initialize the southface */
  m_traffAdriveInterfaceSF =
    AdriveCommand::generateSouthface(CmdArgs::sn_key, this);
  m_traffFollowInterfaceSF =
    FollowerCommand::generateSouthface(CmdArgs::sn_key, this);
  m_plannerStateInterfaceSF =
    PlannerStateInterface::generateSouthfaceLite(CmdArgs::sn_key, this);

  /* Activates the gcmodule logging mechanism */
  if (CmdArgs::log_level > 0) {
    ostringstream str;
    str << CmdArgs::log_path << "planner-gcmodule";
    this->setLogLevel(CmdArgs::log_level);
    this->addLogfile(str.str().c_str());
  }

  /* Initialize traffic state est */
  /* Starts the map update thread */
  if (m_traffStateEst->startMapper())
    Log::getStream(1) << "Unable to initialize mapper";

  //#if USE_PREDICTION
  /* Initialize and start Prediction thread */
  m_predictionLaneChange = false;
  m_prediction = new CPrediction(CmdArgs::sn_key, CmdArgs::visualization_level);
  //#endif

  /* Initialize traj */
  m_traj = CTraj(3);
  m_sentTraj = CTraj(3);

  // Initialize the graph status update module
  m_traffStateEst->initStatus(&m_graph);
  
  if (!(CmdArgs::disable_fused_perceptor) ) {
    // Initialize the fused perceptor after we are done with graph
    // loading.  This will start a private thread.
    const char *spreadDaemon;
    spreadDaemon = getenv("SPREAD_DAEMON");
    if (spreadDaemon == NULL)
      spreadDaemon = "4803";
    if (m_traffStateEst->startFused(spreadDaemon, CmdArgs::sn_key,
                                    options->use_fused_riegl_arg, options->use_fused_stereo_arg))
      Log::getStream(1) << "Unable to initialize fused perceptor";
  }

  /* Initialize traj talker (this is really our south face now) */
  m_trajTalker = new CTrajTalker(MODtrafficplanner, CmdArgs::sn_key);

  /* Initialize UTURN */
  uturn_initialized = false;
  uturn_stage = 0;

  m_resetROAStatusTimeout = 0.5;
  m_checkROAStatusTimeout = 3.0;
  m_disableROATimeout = 3.0;
  m_disableROA = false;
  DGCgettime(m_ROAFailedSince);
  m_ROALastFailure = m_ROAFailedSince;
  m_ROADisabledSince = m_ROAFailedSince;

  /* Initialize evasive status */
  m_EvasionTimeout = 30.0;
  m_evasive = false;
  m_evasion_completed = false;
  m_evasion_stopped = false;
  m_firstEvasive = true;
  DGCgettime(m_EvasionSince);

  /* Initialize merging waypoints */
  mergingWaypoints.clear();

  // load planner parameters from file
  loadPlannerParams(CmdArgs::planner_config);

  /* start display */
  if (CmdArgs::console) {
    if (!CmdArgs::logging)
      Log::setVerboseLevel(0);
    Console::init();
    Console::display->run();
    Console::addMessage("Debug = %d, Verbose Level = %d, Log = %d",
                        CmdArgs::debug, CmdArgs::verbose_level,
                        CmdArgs::logging);
  }
}


/**
 * @brief Destructor
 *
 * It frees the memory used by the planner and its libraries
 */
Planner::~Planner()
{
  MissionPlannerInterface::releaseNorthface(m_missTraffInterfaceNF);
  AdriveCommand::releaseSouthface(m_traffAdriveInterfaceSF);
  FollowerCommand::releaseSouthface(m_traffFollowInterfaceSF);
  PlannerStateInterface::releaseSouthfaceLite(m_plannerStateInterfaceSF);

  if(CmdArgs::mod_log_planner) {
	delete m_modLogPlanner;
  } else if (CmdArgs::lt_planner) {
    delete m_ltPlanner;
  } else {
    LogicPlanner::destroy();
  }
  VelPlanner::destroy();
  Utils::destroy();

  //#if USE_PREDICTION
  if (m_prediction->isRunning())
    m_prediction->stopLoop();
  delete m_prediction;
  //#endif

  if (!(CmdArgs::disable_fused_perceptor) ) {
    m_traffStateEst->stopFused();
  }

  m_traffStateEst->finiStatus();
  
  // Shut down the mapping thread
  m_traffStateEst->stopMapper();
  
  TrafficStateEst::Destroy();
  Console::destroy();
  delete m_trajTalker;

  if (CmdArgs::use_rrt_planner) {
    delete m_rrtPlanner;
	  m_rrtPlanner = NULL;
  }
  else{
  //#if USE_RAILPLANNER
    delete m_railPlanner;
    m_railPlanner = NULL;
  //#endif
  }

  delete m_graphUpdater;
  m_graphUpdater = NULL;

}

/**
 * @brief Arbitrate function of the gcmodule
 *
 * This function listens to the north face and gets messages from mplanner.
 * It populates a directive queue from the directive sent by mplanner and
 * respond to mplanner either by successfully reaching a goal and failing to do
 * so.
 */
void Planner::arbitrate(ControlStatus* cs, MergedDirective* md) {

  /* Display the time needed to make a full planner cycle */
  static uint64_t old_time = getTime();
  uint64_t new_time = getTime();
  Log::getStream(4) << "Time consumed = " << (new_time - old_time)/(double)1000000 << " seconds" << endl;
  Console::updateRate((new_time - old_time)/(double)1000000);
  old_time = getTime();

  time_t currentTime;
  time(&currentTime);
  Log::getStream(1) << "Timestamp: "<<ctime(&currentTime)<<endl;

  unsigned long long time1, time2, t1, t2;
  DGCgettime(time1);

  unsigned long long time;
  DGCgettime(time);
  
  /* Grab the status and merged directive from the arguments */
  PlannerControlStatus *controlStatus = dynamic_cast<PlannerControlStatus *>(cs);
  PlannerMergedDirective *mergedDirective = dynamic_cast<PlannerMergedDirective *>(md);

  SegGoalsStatus segGoalsStatus;

  /* Send a directive completion for goal 0 to mplanner.
   * It tells mplanner that planner started and that it
   * can start sending directives */
  if (!m_isInit) {

    pumpPorts();
      
    segGoalsStatus.goalID = 0;
    segGoalsStatus.status = SegGoalsStatus::COMPLETED;
    m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
    sleep(2);
    
    while (!m_missTraffInterfaceNF->haveNewDirective()) {    
      Log::getStream(2) << "PLANNER: Sending Init Goal " << segGoalsStatus.goalID << endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
      sleep(1);
    }
    m_isInit = true;
  }

  /* Printing that we are currently executing a goal */
  if (PlannerControlStatus::EXECUTING == controlStatus->status) {
    Log::getStream(1) << "PLANNER:  Executing Goal ID :" << controlStatus->ID << endl; 
  }

  /********************************************
   * Receive status back from the Follower    *
   * module Southface. FIXME                  *
   ********************************************/

  pumpPorts();

  while (m_traffFollowInterfaceSF->haveNewStatus()) {

    m_followResponse = (FollowerResponse)*(m_traffFollowInterfaceSF->getLatestStatus());
    Log::getStream(6) << "PLANNER: Follower response:  "<< m_followResponse.toString()<< endl;
    
    //Just deal with Lat and Long failures for now 
    if (m_followResponse.status == GcInterfaceDirectiveStatus::FAILED) {
      if (m_followResponse.reason & FollowerState::LateralMaxError) {
        // REMOVE ?
        // m_planFromCurrPos = true;
      }
      if (m_followResponse.reason & FollowerState::LongitudinalMaxError) {
        // REMOVE ?
      }

      // ROA triggered?
      if (m_followResponse.reason & FollowerState::ReactiveObstacleAvoidance) {

        // If it is the first time that ROA was triggered, set the m_ROAFailedSince
        if ((time-m_ROALastFailure)/1000000.0 > m_resetROAStatusTimeout) {
          m_ROAFailedSince = time;
        }

        // If ROA has been failing for more than the timeout (not spurious)
        if ((time-m_ROAFailedSince)/1000000.0 > m_checkROAStatusTimeout) {
          m_ROADisabledSince = time;
          m_disableROA = true;
        }
        
        m_ROALastFailure = time;
      }
    }
  }
  // If last ROA failure was too long ago reset m_disableROA to false;
  if (m_disableROA && ((time-m_ROADisabledSince)/1000000.0 > m_disableROATimeout)) {
    m_disableROA = false;
  }
  Console::updateROA(m_disableROA);

  /********************************************
   * Notification to Northface that we either *
   * failed or completed a directive          *
   ********************************************/
  
  /* Check on the status of the last command acted on by control */
  if ((controlStatus->status == PlannerControlStatus::COMPLETED ||
       controlStatus->status == PlannerControlStatus::FAILED) && m_currSegGoals.goalID != 0) {

    /* If Alice successfully reached the previous directive
     * notify mplanner */
    if (controlStatus->status == PlannerControlStatus::COMPLETED) {  
      segGoalsStatus.status = SegGoalsStatus::COMPLETED;
      segGoalsStatus.goalID = controlStatus->ID;
      m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
      Console::addMessage("Completion reported to MPlanner for GoalID %d", controlStatus->ID);

      /* Display result */
      Log::getStream(1) << "PLANNER: GOAL ID COMPLETED " << segGoalsStatus.goalID << endl;
      Console::updateLastWpt(PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID));

      /* Pop the previous directive off the queue front (if the queue wasn't cleared meanwhile) */
      if (m_accSegGoalsQ.size() > 0 && m_accSegGoalsQ.front().goalID == segGoalsStatus.goalID){ 
        m_accSegGoalsQ.pop_front();
      } else {
        /* We don't have any directives on the queue */
        Log::getStream(2) << "PLANNER: waiting for more goals: m_accSegGoals.size()==0 " << endl;
      }

      /* If Alice failed, flush the entire queue and notify mplanner */
    } else if (controlStatus->status == PlannerControlStatus::FAILED)  {
      /* Flush out the NF queue */
      while (m_missTraffInterfaceNF->haveNewDirective()) {
        SegGoals newDirective;
        m_missTraffInterfaceNF->getNewDirective( &newDirective );
        segGoalsStatus.goalID = newDirective.goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        Log::getStream(1) << "PLANNER: GOAL ID, FAILED (but not sending) " << segGoalsStatus.goalID << endl;
      }
      
      /* Flush out the accumulating segGoals control queue */
      while (m_accSegGoalsQ.size() > 0) {
        segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        Log::getStream(1) << "PLANNER: GOAL ID, FAILED (but not sending) " << segGoalsStatus.goalID << endl;
        m_accSegGoalsQ.pop_front();
      }

      /* Notify mplanner that we failed the current goal */
      segGoalsStatus.status = SegGoalsStatus::FAILED;
      segGoalsStatus.goalID = controlStatus->ID;
      if (controlStatus->reason == PlannerControlStatus::BLOCKED_LANE)
        segGoalsStatus.reason = SegGoalsStatus::BLOCKED_LANE;
      else if (controlStatus->reason == PlannerControlStatus::BLOCKED_ROAD)
        segGoalsStatus.reason = SegGoalsStatus::BLOCKED_ROAD;
      else if (controlStatus->reason == PlannerControlStatus::KNOWLEDGE)
        segGoalsStatus.reason = SegGoalsStatus::KNOWLEDGE;
      else if (controlStatus->reason == PlannerControlStatus::ACTUATOR_FAILURE)
        segGoalsStatus.reason = SegGoalsStatus::ACTUATOR_FAILURE;
      else 
        segGoalsStatus.reason = SegGoalsStatus::LOST;
      segGoalsStatus.currentSegmentID = controlStatus->currentSegmentID;
      segGoalsStatus.currentLaneID = controlStatus->currentLaneID;

      m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
      Console::addMessage("Failure reported to MPlanner for GoalID %d", controlStatus->ID);
      Log::getStream(1) << "PLANNER: GOAL ID, FAILED (sending) " << segGoalsStatus.goalID << endl;
    }
  }
  Log::getStream(1) << endl << endl;

    
  /********************************************
   * Reponding to the Northface by populating *
   * our own directive queue                  *
   ********************************************/

  /* Get all the new directives and put them in the queue */
  while (m_missTraffInterfaceNF->haveNewDirective()) {

    SegGoals newDirective;
    m_missTraffInterfaceNF->getNewDirective(&newDirective);
    printDirective(&newDirective);

    Console::addMessage("I have a new directive #%d", newDirective.goalID);

    /* MPlanner had a reboot: flush the queue */
    if (newDirective.goalID == 1) {
      Console::addMessage("I reveived directive #1!");
      while (m_accSegGoalsQ.size()>0) {
        m_accSegGoalsQ.pop_front();
      }
    }

    /* If it is an old directive that mplanner sent us twice ignore it */
    if ((m_accSegGoalsQ.size()>0) && (newDirective.goalID <= m_accSegGoalsQ.back().goalID) &&
	SegGoals::EMERGENCY_STOP != newDirective.segment_type &&
	SegGoals::RESET != newDirective.segment_type) {
      continue;
    }

    /* Push the new directive on the queue */
    m_accSegGoalsQ.push_back(newDirective);

    /* A EMERGENCY_STOP or RESET directive preempt everything, the current queue is flushed */
    if ((SegGoals::EMERGENCY_STOP == newDirective.segment_type) || (SegGoals::RESET == newDirective.segment_type)) {
      SegGoalsStatus segGoalsStatus;
      
      /* First flush the queue where the seg goals are accumulating */
      while (SegGoals::EMERGENCY_STOP != m_accSegGoalsQ.front().segment_type && SegGoals::RESET != m_accSegGoalsQ.front().segment_type) {
        segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        segGoalsStatus.reason = SegGoalsStatus::PREEMPTED_BY_PAUSE;

        Log::getStream(1)<<"PLANNER: GOAL ID EMERGENCY_STOP, RESET, FAILED "<<segGoalsStatus.goalID<<endl;
        m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
        m_accSegGoalsQ.pop_front();
      }
      /* Break to answer as fast as possible to the EMERGENCY_STOP or RESET directive */
      break; 
    }
  }

  Log::getStream(1)<<"m_accSegGoalsQ.size() = "<< m_accSegGoalsQ.size() << endl;

  if (m_accSegGoalsQ.size() > 0) {
    SegGoals newGoal = m_accSegGoalsQ.front();

    /* EMERGENCY_STOP and END_OF_MISSION directive do not specify a exit waypoint */
    if (SegGoals::EMERGENCY_STOP == newGoal.segment_type || SegGoals::RESET == newGoal.segment_type || SegGoals::END_OF_MISSION == newGoal.segment_type) {
      mergedDirective->segType = newGoal.segment_type;
      mergedDirective->id = newGoal.goalID;
      m_currSegGoals = newGoal;
      /* if UNKNOWN fail right away */
    } 

    /* If we are currently not executing any goal, set the current goal to
     * the bottom most directive of the queue */
    else if ((PlannerControlStatus::EXECUTING != controlStatus->status || m_currSegGoals.goalID == 0)) {   

      if (SegGoals::UNKNOWN == newGoal.segment_type) {
	segGoalsStatus.goalID = newGoal.goalID;
	mergedDirective->segType = newGoal.segment_type;
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	Log::getStream(1)<<"PLANNER: GOAL ID,UNKNOWN "<<segGoalsStatus.goalID<<endl;
	m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
	/* Populates the exit waypoint */
      } else {
	mergedDirective->id = newGoal.goalID; 
	mergedDirective->segType = newGoal.segment_type;
	m_currSegGoals = newGoal;
	Console::updateNextWpt(PointLabel(m_currSegGoals.exitSegmentID, 
					  m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID));
      }

      Console::addMessage("New goal received %d - illegal passing: %d", newGoal.goalID, newGoal.illegalPassingAllowed);
      Log::getStream(4) << "New goal to complete #" << newGoal.goalID << endl; 
      
      /* Nothing on the Northface */
    } 
  }
  else if (PlannerControlStatus::EXECUTING != controlStatus->status) {
    Log::getStream(1)<<"PLANNER: Waiting for Mission Planner goal "<<endl;
    /* Pause if we are waiting for goals */
    mergedDirective->segType = SegGoals::EMERGENCY_STOP;
    mergedDirective->id = 0;
    m_currSegGoals.goalID = 0;
  }

  /* Create the snapshot of the environment */
  DGCgettime(t1);
  m_traffStateEst->freezeMap();
  m_traffStateEst->updateMap();
  m_traffStateEst->updateState();
  VehicleState vs = m_traffStateEst->getVehState();
  Utils::printCurrPose(AliceStateHelper::getPoseRearAxle(vs));
  Log::getStream(1) << "Current velocity = " << AliceStateHelper::getVelocityMag(vs) << endl;
  // remember where we were for the last X meters
  point2 tmpPt1, tmpPt2;
  tmpPt1 = AliceStateHelper::getPositionRearAxle(vs);
  tmpPt2 = AliceStateHelper::getPositionRearAxle(m_vehStateHistory[0]);
  if (tmpPt1.dist(tmpPt2) > 1.0) {
    m_vehStateHistory.push_front(vs);
    if (m_vehStateHistory.size() > 20) // remember 20 m worth of history
      m_vehStateHistory.pop_back();
    // display the bread crumbs that we are leaving...
    point2arr tmpArr;
    for (int i=0; i<(int)m_vehStateHistory.size(); i++) {
      tmpArr.push_back(AliceStateHelper::getPositionRearAxle(m_vehStateHistory[i]));
    }
    Utils::displayPoints(-2, tmpArr, 2, MAP_COLOR_MAGENTA, 13500);
  }
  Console::updateState(vs);

  //#if USE_PREDICTION
  bool darpaBall;
  // if we signal because of intersection, we don't want to use prediction
  if (m_predictionLaneChange && m_accSegGoalsQ.size()>0 && m_accSegGoalsQ[0].segment_type==SegGoals::INTERSECTION)
      m_predictionLaneChange = false;

  m_prediction->updateVariables(&m_traj, m_traffStateEst->getMap(), m_predictionLaneChange, mergingWaypoints, vs, m_readPathPlanParams, darpaBall);

  if (darpaBall) {
    m_evasive = true;
    // Store time at which we started evasion, so that we can turn it off
    // automatically in case something goes wrong
    m_EvasionSince = time;
  }
  // Reset evasion mode if it takes too long
  if ((m_evasive || m_evasion_stopped) && ((time-m_EvasionSince)/1000000.0 > m_EvasionTimeout)) {
    m_evasive = false;
    m_evasion_completed = false;
    m_evasion_stopped = false;
    m_firstEvasive = true;
  }
  //#endif
  
  DGCgettime(t2);
  Log::getStream(9) << "Snapshot execution time: " << (t2-t1)/1000.0 << " ms" << endl;

  DGCgettime(time2);
  Log::getStream(9) << "Arbitrate execution time: " << (time2-time1)/1000.0 << " ms" << endl;
}

/**
 * @brief Control function of the gcmodule
 *
 * This function generates the trajectory according
 * to the current goal
 */
void Planner::control(ControlStatus* cs, MergedDirective* md)
{
  /* Initialize error */
  static Err_t error = GU_OK | LP_OK | PP_OK | VP_OK | PLANNER_OK;

  /* Initialize cost */
  static Cost_t cost = 0;

  /* Initialize vector of state problems */
  //static vector<StateProblem_t> state_problems;
  // This is what i should have been declared as, according to 2008 logic planner developers
  static vector<StateProblem_t*>state_problems_p;
  
 /* Endpoint given by MPlanner */
  static PointLabel curr_endpoint;

  /* Old turning signal (so that we don't send turn at every cycle but only when it changes) */
  static int old_signal = 0;

  /* Segments */
  static vector<int> segments;

  //  static bool isGraphCreated = false;

  unsigned long long time1, time2;
  
  /* Grab the status and merged directive from the arguments */
  PlannerControlStatus* controlStatus = dynamic_cast<PlannerControlStatus *>(cs);
  PlannerMergedDirective* mergedDirective = dynamic_cast<PlannerMergedDirective *>(md);

  Log::getStream(9) << "Control on directive #" << m_currSegGoals.goalID << endl;

  /* Initialize vehState (renaming) */
  VehicleState vehState = m_traffStateEst->getVehState();

  /* Initialize actuator state */
  ActuatorState actState = m_traffStateEst->getActState();

  /* Set the local/site transform for display functions */
  // REMOVE Utils::setSiteToLocal(&vehState);
  
  /* Initialize the planner state sent to attention module */
  m_attentionPlanState = PlanningState::NOMINAL;

  // Listen to requests from Console
  DGCgettime(time1);
  // Toggle Prediction to turn ON/OFF ?
  if (Console::queryTogglePredictionFlag())
    CmdArgs::noprediction = !CmdArgs::noprediction;
  if (Console::queryResetStateFlag()) {

	  if(CmdArgs::mod_log_planner) {
		  m_modLogPlanner->resetState();
      } else if (CmdArgs::lt_planner) {
    	  m_ltPlanner->resetState();
	  } else {
		  LogicPlanner::resetState();
	  }
      m_planFromCurrPos = true;
	  uturn_initialized = false;
	  uturn_stage = 0;
  }
  m_readPathPlanParams = Console::queryReadPathPlanParamsFlag();

  // New or old planner(s)?
  if(CmdArgs::mod_log_planner) {
	  m_modLogPlanner->updateIntersectionHandlingConsole();
  } else if (CmdArgs::lt_planner) {
    m_ltPlanner->updateIntersectionConsole();
  } else {
	  LogicPlanner::updateIntersectionHandlingConsole();
  }
  
  
  DGCgettime(time2);
  Log::getStream(9) << "Querying console execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Completing a EMERGENCY_STOP or an END_OF_MISSION is just a matter of stopping */
  DGCgettime(time1);
  if (mergedDirective->segType==SegGoals::EMERGENCY_STOP || mergedDirective->segType==SegGoals::RESET ||
      mergedDirective->segType==SegGoals::END_OF_MISSION) {
    m_completed = PlannerUtils::isStopped(vehState);
    Log::getStream(4) << "Checking if PAUSE is completed (" << m_completed << ")" << endl;
  } else if (mergedDirective->segType == SegGoals::UTURN) {
    m_completed = (error & VP_UTURN_FINISHED);
    if (m_completed) m_planFromCurrPos = true;
    Log::getStream(4) << "Checking if UTURN is completed (" << m_completed << ")" << endl;
    /* Check if Alice completed the previous goal */
  } else if ( (mergedDirective->segType == SegGoals::BACK_UP)) {
    m_completed = (error & VP_BACKUP_FINISHED);
    if (m_completed) m_planFromCurrPos = true;
    Log::getStream(4) << "Checking if BACKUP is completed (" << m_completed << ")" << endl;
  } else if (state_problems_p.size()>0 && state_problems_p[0]->region == ZONE_REGION && m_currSegGoals.exitLaneID != 0) {
    m_completed = (error & VP_PARKING_SPOT_FINISHED);
    Log::getStream(4) << "Checking if PARKING is completed (" << m_completed << ")" << endl;
  }
  else {
    m_completed = isGoalComplete();
    Log::getStream(4) << "PLANNER: isGoalComplete " << m_completed << endl;
  }
  // Check evasive maneuver is finished
  if (m_evasive) {
    m_evasion_completed = isEvasionCompleted();
    Log::getStream(4) << "Checking if evasive maneuver is completed (" << m_evasion_completed << ")" << endl;
  }
  DGCgettime(time2);
  Log::getStream(9) << "Completion execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* If we completed, set the status to completed.
   * This will notify mplanner at the beginning of the
   * next arbitrate function call (we cannot goal id 0) */
  if (m_completed && m_currSegGoals.goalID != 0) {
    controlStatus->ID = m_currSegGoals.goalID;
    controlStatus->status = PlannerControlStatus::COMPLETED;
    m_currSegGoals.segment_type = SegGoals::UNKNOWN;
    return;
  }

  if (m_evasion_completed) {
    m_firstEvasive = true;
    m_evasive = false;
    m_evasion_stopped = true;
  }
  if (m_evasion_stopped && AliceStateHelper::getVelocityMag(vehState) > 0.4) {
    m_evasion_stopped = false;
  }

  /* REMOVE
  // If we are running too fast, give up our time slice.
  DGCgettime(time1);
  if (DGCgettime() - m_planTime < (uint64_t) (1e6/m_options->rate_arg))
  {
    usleep(100000); // CHECK THIS; MAY CAUSE PROBLEMS.
    return;
  }
  m_planTime = DGCgettime();
  DGCgettime(time2);
  Log::getStream(9) << "Sleep time: " << (time2-time1)/1000.0 << " ms" << endl;
  */

  // REMOVE MSG("%.3f %.3f %f", DGCgettime() * 1e-6, m_planTime * 1e-6, m_options->rate_arg);

  /* Execute logic planner */
  DGCgettime(time1);
  // Delete all members of problems_p properly
  Log::getStream(9) << "Deleting all '"<< state_problems_p.size() <<"' element(s) from state_problems_p: " << endl;  	  
  while( state_problems_p.size() ) {
	  Log::getStream(9) << "Deleting problem '"<< state_problems_p.back() <<"'." << endl;
	  delete state_problems_p.back();
	  state_problems_p.pop_back();
  }
  state_problems_p.clear();	// This is a redundant clear since last block of code.
  Logic_params_t logicParams;
  logicParams.segment_type = mergedDirective->segType;
  logicParams.seg_goal = m_currSegGoals;
  logicParams.seg_goal_queue = &m_accSegGoalsQ;
  logicParams.m_graph = &m_graph;
  logicParams.m_path = &m_path;
  logicParams.m_estop = actState.m_estoppos;
  logicParams.readConfig = m_readPathPlanParams;
  logicParams.evasion = m_evasive || m_evasion_stopped;
  logicParams.transpos = actState.m_transpos;

  // Get current lane from logic planner. Need to tell mplanner where we are in case we get lost.
  int currentSegmentId = 0;
  int currentLaneId = 0;

  // New or old planner?
  if(CmdArgs::mod_log_planner) {
    error = m_modLogPlanner->planLogic(state_problems_p, &m_graph, error,
      vehState, m_traffStateEst->getMap(), logicParams,
      currentSegmentId, currentLaneId, m_replanInZone);
  } else if (CmdArgs::lt_planner) {
    error = m_ltPlanner->planLogic(state_problems_p, &m_graph, error,
      vehState, m_traffStateEst->getMap(), logicParams,
      currentSegmentId, currentLaneId, m_replanInZone);
  } else {  
    error = LogicPlanner::planLogic(state_problems_p, &m_graph, error,
      vehState, m_traffStateEst->getMap(), logicParams,
      currentSegmentId, currentLaneId, m_replanInZone);
  }
  

  // save mergingWaypoints to send them later to prediction
  if (logicParams.mergingWaypoints.size()>0)
    mergingWaypoints = logicParams.mergingWaypoints;
  else
    mergingWaypoints.clear();

  // If we are not in rail planner, reset evasion
  if (state_problems_p[0]->planner != RAIL_PLANNER && state_problems_p[0]->state!=DRIVE && state_problems_p[0]->state!=STOP_OBS && m_evasive) {
    m_evasive = false;
    m_firstEvasive = true;
  }

  // If we are in evasion, use obstacle bare
  if (m_evasive) {
    state_problems_p[0]->obstacle = OBSTACLE_BARE;
  }

  // deal with the fact that we may have switched planners
  if (state_problems_p[0]->planner != m_previousPlanner) {
    // clear the path if we switched to a different planner
    if (m_previousPlanner == RAIL_PLANNER)
      m_path.unfix(&m_graph);
    else
      m_path.pathLen = 0;
    // remember which planner we are using now
    m_previousPlanner = state_problems_p[0]->planner;
  }

  // this is the interface to LogicPlanner::faultHandler
  m_planFromCurrPos = logicParams.planFromCurrPos;

  if (m_evasion_completed) {
    m_planFromCurrPos = true;
    m_evasion_completed = false;
  }

  // New or old planner(s)?
  if(CmdArgs::mod_log_planner) {
    
     // New (simplified) way to debug StateProblem_t
     Console::updateFSMState(    stateString[    state_problems_p[0]->state]    );
     Console::updateFSMFlag(     flagString[     state_problems_p[0]->flag]     );
     Console::updateFSMRegion(   regionString[   state_problems_p[0]->region]   );
     Console::updateFSMPlanner(  plannerString[  state_problems_p[0]->planner]  );
     Console::updateFSMObstacle( obstacleString[ state_problems_p[0]->obstacle] );
  
     // Print the ProblemState in one line to the log
     Log::getStream(1) << "Final StateProblem = { " << stateString[state_problems_p[0]->state] << ", "
                       << flagString[state_problems_p[0]->flag] <<", "<< regionString[state_problems_p[0]->region] << ", "
                       << plannerString[state_problems_p[0]->planner] << ", " << obstacleString[state_problems_p[0]->obstacle]
                       << " }" << endl;
  	  
     // Send the planner status to the map viewer
     Utils::displayPlannerStatus(regionString[state_problems_p[0]->region], stateString[state_problems_p[0]->state],
                                 plannerString[state_problems_p[0]->planner], flagString[state_problems_p[0]->flag] );
	  

  } else if (CmdArgs::lt_planner) {

    // When this function does something, I'll have to come back and
    // get rid of the above statements.
    m_ltPlanner->updateDisplayInfo();
    
  } else {
    Console::updateFSMState(LogicPlanner::stateToString(state_problems_p[0]->state));
    Log::getStream(1) << "Current State = "
      << LogicPlanner::stateToString(state_problems_p[0]->state) << endl;
    Console::updateFSMFlag(LogicPlanner::flagToString(state_problems_p[0]->flag));
    Log::getStream(1) << "Current Flag = "
      << LogicPlanner::flagToString(state_problems_p[0]->flag) << endl;
    Console::updateFSMRegion(LogicPlanner::regionToString(state_problems_p[0]->region));
    Log::getStream(1) << "Current Region = "
      << LogicPlanner::regionToString(state_problems_p[0]->region) << endl;
    Console::updateFSMPlanner(LogicPlanner::plannerToString(state_problems_p[0]->planner));
    Log::getStream(1) << "Current Planner = "
      << LogicPlanner::plannerToString(state_problems_p[0]->planner) << endl;
    Console::updateFSMObstacle(LogicPlanner::obstacleToString(state_problems_p[0]->obstacle));
    Log::getStream(1) << "Current Obstacle Setting = "
      << LogicPlanner::obstacleToString(state_problems_p[0]->obstacle) << endl;
	
    // Send the planner status to the map viewer
    Utils::displayPlannerStatus(
      LogicPlanner::regionToString(state_problems_p[0]->region),
      LogicPlanner::stateToString(state_problems_p[0]->state),
      LogicPlanner::plannerToString(state_problems_p[0]->planner),
      LogicPlanner::flagToString(state_problems_p[0]->flag));
  }
  
  
  DGCgettime(time2);
  Log::getStream(7) << "-->\tLogic Planner execution time: " << (time2-time1)/1000.0 << " ms" << endl << endl;

  /* Notify MPlanner to "fail" */
  if ((error & LP_FAIL_MPLANNER_LANE_BLOCKED) || 
      (error & LP_FAIL_MPLANNER_ROAD_BLOCKED) ||
      (error & LP_FAIL_MPLANNER_LOST)) {
    controlStatus->ID = m_currSegGoals.goalID;
    controlStatus->status = PlannerControlStatus::FAILED;
    if (error & LP_FAIL_MPLANNER_LANE_BLOCKED)
      controlStatus->reason = PlannerControlStatus::BLOCKED_LANE;
    else if (error & LP_FAIL_MPLANNER_ROAD_BLOCKED)
      controlStatus->reason = PlannerControlStatus::BLOCKED_ROAD;
    else
      controlStatus->reason = PlannerControlStatus::LOST;
    controlStatus->currentSegmentID = currentSegmentId;
    controlStatus->currentLaneID = currentLaneId;
    return;
  }

  /* Reset the error */
  error = 0;

  /* Set the final pose based on the goal position */
  double heading;
  DGCgettime(time1);
  pose2f_t finPose = getFinalPose(state_problems_p[0], mergedDirective, m_currFinalPos, heading);
  DGCgettime(time2);
  Console::updateFinalCond(finPose.pos.x, finPose.pos.y);
  Log::getStream(9) << "Finding final pose execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Set the path planner parameters */
  Path_params_t pathParams;
  pathParams.flag = state_problems_p[0]->flag;
  pathParams.planFromCurrPos = m_planFromCurrPos;
  pathParams.velMin = m_currSegGoals.minSpeedLimit;
  pathParams.velMax = m_currSegGoals.maxSpeedLimit;
  pathParams.readPathPlanParams = m_readPathPlanParams;

  /* Set the vel planner parameters */
  Vel_params_t velParams;
  velParams.minSpeed = m_currSegGoals.minSpeedLimit;
  velParams.maxSpeed = m_currSegGoals.maxSpeedLimit;

  //#if USE_PREDICTION
  /* Predict trajectories of obstacles and possible collisions */
  DGCgettime(time1);
  if (!CmdArgs::noprediction && !m_prediction->isRunning())
    m_prediction->startLoop();
  else if (CmdArgs::noprediction && m_prediction->isRunning())
    m_prediction->stopLoop();

  Console::updatePrediction(m_prediction->isRunning());
  DGCgettime(time2);
  Log::getStream(9) << "Prediction execution time: " << (time2-time1)/1000.0 << " ms" << endl;
  //#endif
  
  if (m_readPathPlanParams) {
    if (loadPlannerParams(CmdArgs::planner_config) == 0)
      Console::addMessage("Read the planner params from file");
    else
      Console::addMessage("FAILED to read the planner params from file");
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  /* Compute trajectory -  populates the m_path and m_traj variables */
  DGCgettime(time1);
  error |= Planner::planTrajectory(vehState, actState, state_problems_p, pathParams, velParams, finPose, cost);
  DGCgettime(time2);
  Log::getStream(9) << "PlanTrajectory execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // set stopline info along the path
  DGCgettime(time1);
  Utils::updatePathStoplines(&m_path, m_traffStateEst->getMap(), &m_graph, vehState);
  DGCgettime(time2);
  Log::getStream(9) << "update stopline execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  DGCgettime(time1);
  Utils::updatePathLaneId(&m_path, &m_graph);
  DGCgettime(time2);
  Log::getStream(9) << "update laneid execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  ////////////////////////////////////////////////////////////////////////////////////////////

  /* print graph and path to default mapviewer channel */
  DGCgettime(time1);
  if (state_problems_p[0]->region != ZONE_REGION)
    Utils::displayGraph(-2, &m_graph, vehState, 2);
  //  Utils::displayGraphHeadings(-2, m_graph, vehState, 25);
  Utils::displayPath(-2, &m_path, 2);
  Utils::printPath(&m_path);
  Utils::printTraj(&m_traj);
  Console::updateTrajectory(&m_traj);
  DGCgettime(time2);
  Log::getStream(9) << "Sending to MapViewer execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Send turning signal */
  DGCgettime(time1);
  int signal = determineSignaling(state_problems_p[0]); 
  if (old_signal != signal) {
    Console::updateTurning(signal);
    sendTurnSignalCommand(signal);
    old_signal = signal;
  }
  DGCgettime(time2);
  Log::getStream(9) << "Turning signal execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Convert to local frame */
  DGCgettime(time1);
  m_sentTraj = m_traj;
  convertTraj(&vehState, &m_sentTraj);
  DGCgettime(time2);
  Log::getStream(9) << "Convert traj execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  //  Utils::printTraj(&m_sentTraj);

  /* Send the trajectory */
  DGCgettime(time1);
  int trajSocket = m_trajTalker->m_skynet.get_send_sock(SNtraj);
  m_sentTraj.setOverrideROA(m_disableROA);
  if (m_currSegGoals.segment_type == SegGoals::START_CHUTE) m_sentTraj.setOverrideROA(true);
  m_trajTalker->SendTraj(trajSocket, &m_sentTraj);
  DGCgettime(time2);
  Log::getStream(9) << "Sending trajectory execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Now we want to send the planner state to the attention module */
  DGCgettime(time1);
  sendPlanningState(state_problems_p);
  DGCgettime(time2);
  Log::getStream(9) << "Sending to planning state execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  /* Set status */
  controlStatus->status = PlannerControlStatus::EXECUTING;
  controlStatus->ID = m_currSegGoals.goalID;

  Utils::printErrorInBinary(error);

}

bool Planner::isGoalComplete()
{
  point2 exitWaypt;
  double exitHeading;
  point2 currPos, pathExit;
  point2 projExit, projAlice;
  double distExit, distAlice;
  point2arr pathPoints;
  double maxDistToProj = 10;
  double maxPathExtension = 10;
  double completeHeading = M_PI/12.0;
  double completeDistance = 3.0;
  VehicleState vehState = m_traffStateEst->getVehState();
  double currHeading = AliceStateHelper::getHeading(vehState);

  /* Make trivial check (avoids completion check failure due to delays in planner):
   * If the current node's previous waypoint is the waypoint we want to go to
   * we must have crossed it, this only works if MPlanner continues to send
   * segment by segment goals */
  if (m_currSegGoals.segment_type == SegGoals::INTERSECTION || m_currSegGoals.segment_type == SegGoals::ROAD_SEGMENT) {
    currPos = AliceStateHelper::getPositionRearBumper(vehState);
    uint16_t include = PLAN_GRAPH_NODE_LANE;
    uint16_t exclude = PLAN_GRAPH_NODE_ONCOMING | PLAN_GRAPH_NODE_VOLATILE;
    PlanGraphNode *node = m_graph.getNearestPos(currPos.x, currPos.y, 1.0, include, exclude);
    if (node) {
      RNDFGraphWaypoint *wp = node->prevWaypoint;
      if (wp) {
        if (m_currSegGoals.exitSegmentID == wp->segmentId &&
            m_currSegGoals.exitLaneID == wp->laneId &&
            m_currSegGoals.exitWaypointID == wp->waypointId) {
          return true;
        }
      }
    }
  }

  /* The path has to have at least 2 nodes */
  if (m_path.pathLen <= 1) return false;

  /* Get the position of the exit waypoint */
  RNDFGraphWaypoint *wp = m_graph.rndf.getWaypoint(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID);
  assert(wp);
  PlanGraphNode *exitNode =  m_graph.getWaypoint(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID);
  if (!exitNode) {
    exitWaypt.set(wp->px, wp->py);
    exitHeading = atan2(m_path.nodes[m_path.pathLen-1]->pose.pos.y - m_path.nodes[m_path.pathLen-2]->pose.pos.y,
                        m_path.nodes[m_path.pathLen-1]->pose.pos.x - m_path.nodes[m_path.pathLen-2]->pose.pos.x);
  } else {
    exitWaypt.set(exitNode->pose.pos.x, exitNode->pose.pos.y);
    exitHeading = exitNode->pose.rot;
  }

  /* If we are completing inside a zone (not a parking spot though), use rear axle */
  if (wp->flags.isZonePerimeter && wp->flags.isEntry) {
    currPos = AliceStateHelper::getPositionRearAxle(vehState);
    completeDistance = 3.0; // MAGIC
  } else {
    currPos = AliceStateHelper::getPositionFrontBumper(vehState);
    completeDistance = 3.0; // MAGIC
  }

  /* Populate the current path */
  for (int i = 0; i < m_path.pathLen; i++) {
    pathPoints.push_back(point2(m_path.nodes[i]->pose.pos.x, m_path.nodes[i]->pose.pos.y));
  }  pathExit = point2(m_path.nodes[m_path.pathLen-1]->pose.pos.x, m_path.nodes[m_path.pathLen-1]->pose.pos.y);

  if (wp->flags.isZonePerimeter && wp->flags.isExit) {
    exitWaypt.set(exitWaypt.x-DIST_REAR_AXLE_TO_FRONT*cos(exitHeading), 
		  exitWaypt.y-DIST_REAR_AXLE_TO_FRONT*sin(exitHeading));
  }

  /* Project exit point on the path */
  if (Utils::distToProjectedPoint(projExit, distExit, exitWaypt, exitHeading, pathPoints, maxDistToProj, maxPathExtension) != 0) {
    Log::getStream(1) << "isComplete: projection of exit pt failed" << endl;
    return false; 
  }

  /* Project curr position on the path */
  if (Utils::distToProjectedPoint(projAlice, distAlice, currPos, currHeading, pathPoints, maxDistToProj, maxPathExtension) != 0) {
    Log::getStream(1) << "isComplete: projection of alice failed" << endl;
    return false;
  }

  if (wp->flags.isZonePerimeter) {
    Log::getStream(1) << "zone completion: dist along path" << fabs(distExit - distAlice) << " complete dist = " << completeDistance << " abs dist = " <<  pathExit.dist(exitWaypt) << " heading " << Utils::getAngleInRange(currHeading - exitHeading) << " complete heading " << completeHeading << endl;

  }

  if (wp->flags.isZonePerimeter && 
      //      fabs(distExit - distAlice) <= completeDistance && 
      //      pathExit.dist(exitWaypt) <= completeDistance &&
      currPos.dist(exitWaypt) <= completeDistance &&
      fabs(Utils::getAngleInRange(currHeading - exitHeading)) < completeHeading) {
    m_replanInZone = true;
    m_useSubPath = false;
    return true;
  } else if (wp->flags.isZoneParking) {
    // use the internal completion check
    return false;
  } else if ( ( m_currSegGoals.segment_type != SegGoals::INTERSECTION && fabs(distExit - distAlice) <= completeDistance ) || 
	      ( m_currSegGoals.segment_type == SegGoals::INTERSECTION && (distExit - distAlice < -3.0) && fabs(distExit - distAlice) <= 2.0*completeDistance+3.0 ) ) {
    return true;
  }
  return false;
}

/**
 * @brief Gets the current time
 */
uint64_t Planner::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

/**
 * @brief Prints the new directive
 */
void Planner::printDirective(SegGoals* newDirective) 
{
  Log::getStream(1) << "PLANNER: New dir recvd, ID "<<newDirective->goalID<<",type "<<newDirective->segment_type;
  Log::getStream(1) << ", intersection type "<<newDirective->intersection_type;
  Log::getStream(1) << ", illegalPassingAllowed "<<newDirective->illegalPassingAllowed; 
  Log::getStream(1) << ", exit waypt ";
  Log::getStream(1) << newDirective->exitSegmentID<<"."<<newDirective->exitLaneID<<"."<<newDirective->exitWaypointID<<endl;
}

/**
 * @brief Gets the next final point based on the current state and the directive queue
 */
pose2f_t Planner::getFinalPose(StateProblem_t* problem, PlannerMergedDirective *mergedDirective, point2 &finalPoint, double &heading)
{
  static pose2f_t prev_pose;
  static bool first_time = true;
  int numGoal = 0; // 0 = current goal, 1 = next goal, 2 = next_next goal

  pose2f_t finPose;
  VehicleState vehState = m_traffStateEst->getVehState();

  if (first_time) {
    point2 alice = AliceStateHelper::getPositionRearAxle(vehState);
    double heading = AliceStateHelper::getHeading(vehState);
    prev_pose.pos.x = alice.x; prev_pose.pos.y = alice.y;
    prev_pose.rot = heading;
    first_time = false;
  }

  if (m_evasive && m_firstEvasive) {
    point2 alice = AliceStateHelper::getPositionRearAxle(vehState);
    double heading = AliceStateHelper::getHeading(vehState);
    m_evasiveStart.pos.x = alice.x;
    m_evasiveStart.pos.y = alice.y;
    m_evasiveStart.rot = heading;
    m_evasiveEnd.pos.x = m_evasiveStart.pos.x + 20.0*cos(m_evasiveStart.rot) - 5.0*sin(m_evasiveStart.rot);
    m_evasiveEnd.pos.y = m_evasiveStart.pos.y + 20.0*sin(m_evasiveStart.rot) + 5.0*cos(m_evasiveStart.rot);
    m_evasiveEnd.rot = m_evasiveStart.rot;
  }
  if (m_evasive) {
    finalPoint.set(m_evasiveEnd.pos.x, m_evasiveEnd.pos.y);
    heading = m_evasiveEnd.rot;
    finPose.pos.x = finalPoint.x; finPose.pos.y = finalPoint.y;
    finPose.rot = heading;
    return finPose;
  }
  m_goalWaypoints.clear();

  if (m_accSegGoalsQ.size() == 0 ||
	  mergedDirective->segType == SegGoals::EMERGENCY_STOP || 
	  mergedDirective->segType == SegGoals::RESET ||
      mergedDirective->segType == SegGoals::END_OF_MISSION || 
	  mergedDirective->segType == SegGoals::UTURN ||
      mergedDirective->segType == SegGoals::UNKNOWN ||
	  mergedDirective->segType == SegGoals::BACK_UP ||
	  mergedDirective->segType == SegGoals::DRIVE_AROUND || // we do not use this one
	  mergedDirective->segType == SegGoals::PREZONE) {  // we do not use this one
	// we do not have well defined points to plan to, so use previous goal
    finalPoint.set(prev_pose.pos.x, prev_pose.pos.y);
    heading = prev_pose.rot;
  } else {

	// We are in Road OR Zone OR Intersection
    PlanGraphNode *node;
    SegGoals *goal;
    SegGoals *prev_goal = &m_currSegGoals;
    SegGoals *next_goal = &m_currSegGoals;
    SegGoals *next_next_goal = NULL;

    if (m_accSegGoalsQ.size() == 1) {
      // we only have  goal in the queue, so just give that
      next_goal = prev_goal;
      numGoal = 0;
    }
    
    if (prev_goal->segment_type == SegGoals::PARKING_ZONE) {
      next_goal = prev_goal;
      numGoal = 0;
    } else if (prev_goal->segment_type == SegGoals::INTERSECTION) {
      if (m_accSegGoalsQ.size() > 1) {
	next_goal = &(m_accSegGoalsQ[1]);
	numGoal = 1;
	// some exeptions where we do not want to plan ahead
	if (next_goal->segment_type != SegGoals::ROAD_SEGMENT && next_goal->segment_type != SegGoals::PARKING_ZONE &&
	    next_goal->segment_type != SegGoals::INTERSECTION && next_goal->segment_type != SegGoals::PREZONE) {
	  next_goal = prev_goal;
	  numGoal = 0;
	}
      }
      
    } else if (prev_goal->segment_type == SegGoals::ROAD_SEGMENT) {
      /* if we are driving off-road, go to the next checpoint*/
      if (problem->flag == OFFROAD) {
	next_goal = prev_goal;
	numGoal = 0;
      } else {
	for (unsigned int i=1; i<2; i++) {
	  goal = &(m_accSegGoalsQ[i]);
	  
		  /* If the goal is neither an intersection, nor a road, break */
	  if (goal->segment_type != SegGoals::INTERSECTION && 
	      goal->segment_type != SegGoals::ROAD_SEGMENT &&
	      goal->segment_type != SegGoals::PARKING_ZONE) {
	    next_goal = prev_goal;
	    numGoal = i-1;
			break;
	  }
	  
	  /* If the goal is a Checkpoint go there */
	  // TESTING
	  //		  if (goal->isExitCheckpoint) {
	  //			next_goal = goal;
	  //			numGoal = i;
	  //			break;
	  //		  }
	  
	  /* If it is an intersection stop looking */
	  if (goal->segment_type == SegGoals::INTERSECTION) {
	    // check to see if we have another goal in the queue beyond the intersection?
	    if (i+1<m_accSegGoalsQ.size()) {
	      // this means that we have another goal in the queue that we can access
	      next_next_goal = &(m_accSegGoalsQ[i+1]);
			  // if this is a road region then use this to plan to
	      if (next_next_goal->segment_type == SegGoals::ROAD_SEGMENT) {
		next_goal = next_next_goal;
		numGoal = i+1;
		break;
	      }
	    } else { // we do not have another goal beyond the intersection, so plan to that
	      next_goal = goal;
	      numGoal = i;
			}
	    break;
	  }
	  
	  /* If it would be unsafe to go there break */
	  if (goal->segment_type != SegGoals::INTERSECTION &&
	      (m_currSegGoals.exitSegmentID != goal->exitSegmentID ||
	       m_currSegGoals.exitLaneID != goal->exitLaneID ||
	       m_currSegGoals.exitWaypointID > goal->exitWaypointID)) {
	    next_goal = prev_goal;
	    numGoal = i-1;
	    break;
	  }
	  
		  prev_goal = goal;
		}
	  }
	}

    // TESTING
	node = m_graph.getWaypoint(next_goal->exitSegmentID, next_goal->exitLaneID, next_goal->exitWaypointID);
	if (node == NULL) {
	  Log::getStream(1) << "ERROR: Specifying a waypoint that is not in the rndf - should never happen" << endl;
	  finalPoint.set(AliceStateHelper::getPositionRearAxle(vehState));
	  heading = AliceStateHelper::getHeading(vehState);
	} else {
	  finalPoint.set(node->pose.pos.x, node->pose.pos.y);
	  heading = node->pose.rot;
	}
  }

  finPose.pos.x = finalPoint.x; finPose.pos.y = finalPoint.y; ;
  finPose.rot = heading;
  if (!(mergedDirective->segType == SegGoals::EMERGENCY_STOP || mergedDirective->segType == SegGoals::RESET ||
        mergedDirective->segType == SegGoals::END_OF_MISSION)) {
    prev_pose = finPose;
  }

  // populate the list that is the complement of the list of entry/exit points that we want to include
  //  Console::addMessage("SegGoal # = %d", numGoal);
  Log::getStream(5) << "SegGoal # = " <<  numGoal << endl;
  RNDFGraphWaypoint* waypoint = NULL;
  if ((int)m_accSegGoalsQ.size()-1 < numGoal) {
    // dont want to go beyond the end
    numGoal = m_accSegGoalsQ.size() - 1;	
  }

  for (int i=0; i<=numGoal; i++) {
    // include the entry waypoint for the first goal if it is not 0.0.0
    if ( (i==0) && 
         ( m_accSegGoalsQ[i].entrySegmentID != 0 || 
           m_accSegGoalsQ[i].entryLaneID != 0 || 
           m_accSegGoalsQ[i].entryWaypointID != 0 ) ) {
      waypoint = m_graph.rndf.getWaypoint(m_accSegGoalsQ[i].entrySegmentID, m_accSegGoalsQ[i].entryLaneID, m_accSegGoalsQ[i].entryWaypointID);
	  
      if ( (waypoint != NULL) && 
           ((waypoint->flags.isExit == 1) || (waypoint->flags.isEntry == 1)) ) {// do not populate for checkpoint if not also a waypoint
        m_goalWaypoints.push_back(waypoint);
        Log::getStream(5) << "Waypoint included in the list = " << waypoint->segmentId << " " << waypoint->laneId << " " << waypoint->waypointId << endl;
      }
    }

    // get the exit labels of the goals (curr entry wp = prev exit wp)
    waypoint = m_graph.rndf.getWaypoint(m_accSegGoalsQ[i].exitSegmentID, m_accSegGoalsQ[i].exitLaneID, m_accSegGoalsQ[i].exitWaypointID);
    if ( (waypoint != NULL) && 
         ((waypoint->flags.isExit == 1) || (waypoint->flags.isEntry == 1)) ) {// do not populate for checkpoint if not also a waypoint
      m_goalWaypoints.push_back(waypoint);
      Log::getStream(5) << "Waypoint included in the list = " << waypoint->segmentId << " " << waypoint->laneId << " " << waypoint->waypointId << endl;
    }
    if (i == numGoal && waypoint!=NULL)
      Log::getStream(5) << "Waypoint Node that we are planning to = " << waypoint->segmentId << " " << waypoint->laneId << " " << waypoint->waypointId << endl;
    if (i == numGoal)
      Log::getStream(5) << "RNDF Node that we are planning to = " << m_accSegGoalsQ[i].entrySegmentID << "." <<  m_accSegGoalsQ[i].entryLaneID << "." <<  m_accSegGoalsQ[i].entryWaypointID << endl;
  }
  
 return finPose;
}

/**
 * @brief Gets all useful segments
 */
void Planner::getSegments(vector<int> &segments)
{
  LaneLabel current_lane;
  VehicleState vehState = m_traffStateEst->getVehState();
  Map *map = m_traffStateEst->getMap();
  segments.clear();

  if (map->getLane(current_lane, AliceStateHelper::getPositionRearAxle(vehState)) >= 0) {
    segments.push_back(current_lane.segment);
  }

  for (unsigned int i= 0; i < m_accSegGoalsQ.size(); i++) {
    SegGoals goal =  m_accSegGoalsQ[i];
    // if we have an intersection, we have to load every segments that goes into the intersection
    if (goal.segment_type == SegGoals::INTERSECTION) {
      Map *map = m_traffStateEst->getMap();
      vector<PointLabel> WayPointEntries;
      vector<PointLabel> WayPointExits;
      PointLabel WayPointExit(goal.exitSegmentID, goal.exitLaneID, goal.exitWaypointID);
      map->getWayPointEntries(WayPointEntries,WayPointExit);
      for (unsigned j=0; j<WayPointEntries.size(); j++) {
        map->getWayPointExits(WayPointExits, WayPointEntries[j]);
        if (WayPointEntries[j].segment != 0) segments.push_back(WayPointEntries[j].segment);
        for (unsigned k=0;k<WayPointExits.size(); k++) {
          if (WayPointExits[k].segment != 0) segments.push_back(WayPointExits[k].segment);
        }
      }
    }

    if (goal.entrySegmentID != 0) segments.push_back(goal.entrySegmentID);
    if (goal.exitSegmentID != 0) segments.push_back(goal.exitSegmentID);
  }
}

/**
 * @brief Determines the signaling
 */
int Planner::determineSignaling(StateProblem_t* problem)
{
  LaneLabel current_lane;
  LaneLabel next_lane;
  point2 node_pos;

  if (m_path.pathLen < 2) return 0;

  /* Reinitialize the exit point */
  m_attentionExitPoint.segment = 0;
  m_attentionExitPoint.lane = 0;
  m_attentionExitPoint.waypoint = 0;

  VehicleState vehState = m_traffStateEst->getVehState();
  Map *map = m_traffStateEst->getMap();

  if (!m_graph.vehicleNode) 
    return 0;
  current_lane.segment = m_graph.vehicleNode->segmentId;
  current_lane.lane = m_graph.vehicleNode->laneId;

  /* Am I UTURNing? */
  if (problem->state == UTURN) {
    m_attentionPlanState = PlanningState::UTURN;
    return 0;
  } else if (problem->state == PAUSE) {
    return 0;
  }

  /* Am I overtaking? THIS needs to be fixed*/
  PlanGraphNode *node;
  int signal = 0;
  for (int i = 1; i<m_path.pathLen; i++) {
    if (i > 20) break;
    node = m_path.nodes[i];
    // node_pos.set(node->pose.pos.x, node->pose.pos.y);
    next_lane.segment = node->segmentId;
    next_lane.lane = node->laneId;
    // map->getLane(next_lane, node_pos);
    signal = PlannerUtils::getLaneSide(map, vehState, current_lane, next_lane);

    //#if USE_PREDICTION
    // set flag for prediction to check other lanes while lange change
    if (signal!=0)
      m_predictionLaneChange = true;
    else
      m_predictionLaneChange = false;
    //#endif

    if (signal != 0) {
      if (-1 == signal)
        m_attentionPlanState = PlanningState::PASS_LEFT;
      else if (1 == signal)  
        m_attentionPlanState = PlanningState::PASS_RIGHT;
      return signal;
    }
  }

  /* Am I close to an intersection  ? */
  
  SegGoals segGoals;
  double dist_stopline =  Utils::getDistToStopline(&m_path, &m_accSegGoalsQ, segGoals);

  Log::getStream(4)<<"PLANNER_INT: dist_StopLine "<<dist_stopline<<endl;
  Log::getStream(4)<<"PLANNER_INT: segGoals "<<segGoals.toString()<<endl;
  Log::getStream(4)<<"PLANNER_INT: m_currSegGoals "<<m_currSegGoals.toString()<<endl;
 
  if (SegGoals::INTERSECTION_STRAIGHT == segGoals.intersection_type) {
    m_attentionPlanState = PlanningState::INTERSECT_STRAIGHT;
    signal = 0;
  }

  if ((dist_stopline < 30.0 && dist_stopline > 0.0) || (segGoals.goalID == m_currSegGoals.goalID && !m_completed)) {	
    if (SegGoals::INTERSECTION_LEFT == segGoals.intersection_type) {
      m_attentionPlanState = PlanningState::INTERSECT_LEFT;
      signal = -1;
    } else if (SegGoals::INTERSECTION_RIGHT == segGoals.intersection_type) {
      m_attentionPlanState = PlanningState::INTERSECT_RIGHT;
      signal = 1;
    }
  }

  /* Populate the exit point */
  m_attentionExitPoint.segment = segGoals.exitSegmentID;
  m_attentionExitPoint.lane = segGoals.exitLaneID;
  m_attentionExitPoint.waypoint = segGoals.exitWaypointID;

  return signal;
}

/**
 * @brief Sends the turn signal to Adrive
 */
void Planner::sendTurnSignalCommand(int signal)
{
  static AdriveDirective adriveDir;
  static int dirID = 0;

  adriveDir.id = ++dirID;
  adriveDir.actuator = TurnSignal;
  adriveDir.command = SetPosition;
  adriveDir.arg = signal;
  m_traffAdriveInterfaceSF->sendDirective(&adriveDir);
  Log::getStream(4) << "PLANNER: Sent turn signal " << adriveDir.arg << endl;
}

/*******************
 * BACKUP handling *
 *******************/

Err_t Planner::planBackup(CTraj *traj, VehicleState &vehState, Map *map)
{
  // To send the direction
  static int superConSendSocket = m_skynet.get_send_sock(SNsuperconTrajfCmd);
  static struct superConTrajFcmd scCmd;

  static bool initialized = false;
  static Vehicle *vp = maneuver_create_vehicle(VEHICLE_WHEELBASE, 30.0 * M_PI/180);
  static Maneuver *mp = NULL;
  static double speeds[19] = { 0.25, 0.5, 0.8, 0.9, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0 };
  static point2 start_pt, end_pt, tmp_point;
  static Pose2D start, end;
  static int old_direction = 1;
  static int direction = 1;
  static LaneLabel current_lane;

  if (!initialized) {
    Console::addMessage("BACKUP: Starting");
    Log::getStream(1) << "BACKUP: Starting" << endl;
    old_direction = 1;

    /* Generate start point */
    start_pt = AliceStateHelper::getPositionRearAxle(vehState);
    start.x = start_pt.x;
    start.y = start_pt.y;
    start.theta = AliceStateHelper::getHeading(vehState);

    /* Set all the variable necessary for the rest of the UTurn */
    map->getLane(current_lane, start_pt);

    /* planning in reverse */
    start.theta = PlannerUtils::getAngleInRange(start.theta + M_PI);
    direction = -1;

    /* getting end point 10m behind */
    map->getLaneCenterPoint(end_pt, current_lane, start_pt, -5.0);
    map->getHeading((double&) end.theta, tmp_point, current_lane, end_pt);
    end.theta = PlannerUtils::getAngleInRange(end.theta + M_PI);
    end.x = end_pt.x;
    end.y = end_pt.y;

    /* generate maneuver */
    if (mp) maneuver_free(mp);
    mp = maneuver_twopoint(vp, &start, &end);

    initialized = true;
  }

  /* create CTraj from the maneuver */
  traj->startDataInput();
  maneuver_profile_single(vp, mp, 19, speeds, 20, traj);
  traj->setDirection(direction);

  /* send direction */
  if (old_direction != direction) {
    if (direction == 1) scCmd.commandType = tf_forwards;
    else scCmd.commandType = tf_reverse;
    m_skynet.send_msg(superConSendSocket, &scCmd, sizeof(scCmd), 0);
    old_direction = direction;
  }

  /* Am I at the end of the segment? */
  point2 pos = AliceStateHelper::getPositionRearAxle(vehState);
  double min_dist = INFINITY;
  double dist;
  double x, y;
  int min_index;
  if (PlannerUtils::isStopped(vehState)) {
    for (int i=0; i<traj->getNumPoints(); i++) {
      x = traj->getNorthing(i);
      y = traj->getEasting(i);
      dist = sqrt(pow(x-pos.x,2)+pow(y-pos.y,2));
      if (dist < min_dist) {
        min_dist = dist;
        min_index = i;
      }
    }
    /* Check if we are still close to the trajectory */
    if (min_dist > 10.0) {
      Console::addMessage("BACKUP: robust replan");
      Log::getStream(1) << "BACKUP: robust replan" << endl;
      initialized = false;
      scCmd.commandType = tf_forwards;
      m_skynet.send_msg(superConSendSocket, &scCmd, sizeof(scCmd), 0);
    }
    /* Check if at the end of the segment */
    if (min_index > traj->getNumPoints()-3) {
      Console::addMessage("BACKUP: Completed");
      Log::getStream(1) << "BACKUP: Completed" << endl;
      initialized = false;
      scCmd.commandType = tf_forwards;
      m_skynet.send_msg(superConSendSocket, &scCmd, sizeof(scCmd), 0);
      return VP_BACKUP_FINISHED;
    }
  }

  return VP_OK;
}

/******************
 * UTURN handling *
 ******************/

Err_t Planner::planUTurn(CTraj *traj, VehicleState &vehState, Map *map, PointLabel curr_endpoint)
{
  // To send the direction
  static int superConSendSocket = m_skynet.get_send_sock(SNsuperconTrajfCmd);
  static struct superConTrajFcmd scCmd;

  // each stage will initialize some points to go to
  static Vehicle *vp = maneuver_create_vehicle(VEHICLE_WHEELBASE, 30.0 * M_PI/180);
  static Maneuver *mp = NULL;
  static double speeds[19] = { 0.25, 0.5, 0.8, 0.9, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0 };
  static point2 start_pt, end_pt, tmp_point;
  static Pose2D start, end;
  static int old_direction = 1;
  static int direction = 1;
  static LaneLabel current_lane;
  static LaneLabel opposite_lane;

  if (!uturn_initialized) {
    Console::addMessage("UTURN: Starting stage %d (going to %d.%d)", uturn_stage, curr_endpoint.segment, curr_endpoint.lane);

    /* Generate start point */
    start_pt = AliceStateHelper::getPositionRearAxle(vehState);
    start.x = start_pt.x;
    start.y = start_pt.y;
    start.theta = AliceStateHelper::getHeading(vehState);

    /* Generate end point */
    switch (uturn_stage) {
    case 0:
      old_direction = 1;

      /* Set all the variable necessary for the rest of the UTurn */
      map->getLane(current_lane, start_pt);
      opposite_lane = LaneLabel(curr_endpoint.segment, curr_endpoint.lane);

      /* go 90 */
      direction = 1;

      /* getting end point 5m ahead on the other road */
      map->getLaneRightPoint(end_pt, opposite_lane, start_pt, -8.0);
      map->getHeading((double&) end.theta, tmp_point, opposite_lane, end_pt);
      end.theta = PlannerUtils::getAngleInRange(end.theta + M_PI/2);
      break;
    case 1:
      old_direction = 1;

      /* go backward */
      start.theta = PlannerUtils::getAngleInRange(start.theta + M_PI);
      direction = -1;

      /* getting end point 5m back on the other road */
      map->getLaneRightPoint(end_pt, current_lane, start_pt, 4.0);
      map->getHeading((double&) end.theta, tmp_point, current_lane, end_pt);
      end.theta = PlannerUtils::getAngleInRange(end.theta + M_PI/4);
      break;
    case 2:
      old_direction = -1;

      /* go forward */
      direction = 1;

      /* getting on lane */
      map->getLaneCenterPoint(end_pt, opposite_lane, start_pt, 8.0);
      map->getHeading((double&) end.theta, tmp_point, opposite_lane, end_pt);
      break;
    }
    end.x = end_pt.x;
    end.y = end_pt.y;

    /* generate maneuver */
    if (mp) maneuver_free(mp);
    mp = maneuver_twopoint(vp, &start, &end);

    uturn_initialized = true;
  }

  /* create CTraj from the maneuver */
  traj->startDataInput();
  maneuver_profile_single(vp, mp, 19, speeds, 20, traj);
  traj->setDirection(direction);

  /* send direction */
  if (old_direction != direction) {
    if (direction == 1) scCmd.commandType = tf_forwards;
    else scCmd.commandType = tf_reverse;
    m_skynet.send_msg(superConSendSocket, &scCmd, sizeof(scCmd), 0);
    old_direction = direction;
  }

  /* Am I at the end of the segment? */
  point2 pos = AliceStateHelper::getPositionRearAxle(vehState);
  double min_dist = INFINITY;
  double dist;
  double x, y;
  int min_index;
  if (PlannerUtils::isStopped(vehState)) {
    for (int i=0; i<traj->getNumPoints(); i++) {
      x = traj->getNorthing(i);
      y = traj->getEasting(i);
      dist = sqrt(pow(x-pos.x,2)+pow(y-pos.y,2));
      if (dist < min_dist) {
        min_dist = dist;
        min_index = i;
      }
    }
    /* Check if we are still close to the trajectory */
    if (min_dist > 20.0) {
      Console::addMessage("UTURN: robust replan");
      uturn_initialized = false;
      uturn_stage = 0;
      scCmd.commandType = tf_forwards;
      m_skynet.send_msg(superConSendSocket, &scCmd, sizeof(scCmd), 0);
    }
    Console::addMessage("UTURN: min_index %d (%d)", min_index, traj->getNumPoints());
    if (min_index > traj->getNumPoints()-6) {
      Console::addMessage("UTURN: Completed stage %d", uturn_stage);
      uturn_initialized = false;
      uturn_stage++;
      if (uturn_stage == 3) {
        uturn_stage = 0;
        return VP_UTURN_FINISHED;
      }
    }
  }

  return VP_OK;
}



void Planner::sendPlanningState(vector<StateProblem_t*> stateProb) {
  PlannerState plannerStateDir; 
  plannerStateDir.mode = m_attentionPlanState; 
  plannerStateDir.exitPoint = m_attentionExitPoint;
  Log::getStream(4) << "Attention plan state " << m_attentionPlanState << " exit point " << m_attentionExitPoint<< endl;  
  m_plannerStateInterfaceSF->sendDirective(&plannerStateDir);
}



Err_t Planner::planTrajectory(VehicleState vehState, ActuatorState actState, vector<StateProblem_t*> state_problems_p, Path_params_t pathParams, Vel_params_t velParams, pose2f_t finPose, Cost_t cost) 
{
  static int s1planner_failure_count = 0;

  Err_t error = GU_OK | LP_OK | PP_OK | VP_OK | PLANNER_OK;
  unsigned long long time1, time2;
  pose2f_t curPose;


#define SPECIAL_COMPLETE_DIST 1.0

  // Get the vehicle pose (site frame)
  curPose = AliceStateHelper::getPoseRearAxle(vehState);
  
  if ( (state_problems_p[0]->state == UTURN) && (CmdArgs::use_hacked_uturn)) {

    /* temporary generate UTurn manually */
    error |= Planner::planUTurn(&m_traj, vehState, m_traffStateEst->getMap(), PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID));

  } else if ( (state_problems_p[0]->state == BACKUP) && (CmdArgs::use_hacked_backup) ){

    /* temporary generate backup manually */
    error |= Planner::planBackup(&m_traj, vehState, m_traffStateEst->getMap());

  } else {
    switch (state_problems_p[0]->region) 
      {
      case ROAD_REGION:
        {
          switch (state_problems_p[0]->planner) 
            {
              //#if USE_RAILPLANNER                
            case RAIL_PLANNER:
              {
                if(CmdArgs::use_rrt_planner){
                  error |= planRRTTrajectory(vehState, actState, state_problems_p[0],
                                              pathParams, velParams, curPose, finPose);
                
                }
                else{
                  // Generate trajectory using the rail planner
                  error |= planRailTrajectory(vehState, actState, state_problems_p[0],
                                              pathParams, velParams, curPose, finPose);
                }
                break;
              }
              //#endif
              

              //#if USE_S1PLANNER
            case S1PLANNER: 
              {
                if ( (m_currSegGoals.exitSegmentID == 0) && 
                     (m_currSegGoals.exitLaneID == 0) && 
                     (m_currSegGoals.exitWaypointID == 0) )
                  break;
                // get the planning problem
                CSpecs_t cSpecs;
                cSpecs = CSpecs_t();
                PlanGraphPath subPath;

                // Specify the final pose that we want to plan to
                finPose = getRoadZoneFinPose(state_problems_p[0], vehState);
                error |= ZoneCorridor::setupCSpecs(cSpecs, m_traffStateEst->getMap(), vehState, actState, m_currSegGoals, *(state_problems_p[0]), pathParams, finPose);

                Utils::printCSpecs(&cSpecs);

                
                // Send the costmap (debug info) if specified (off by default)
                if (CmdArgs::show_costmap) {
                  BitmapParams bmParams = cSpecs.getBitmapParams(); 
                  m_polyTalker.send(&bmParams);
                }

                double runtime = 0.5;
		
                // PLAN THE PATH
                DGCgettime(time1);
                if (m_replanInZone) {
                  // delete the clothoid tree and replan from scratch
                  ClothoidPlannerInterface::clearClothoidTree(&cSpecs);
                  if (state_problems_p[0]->state != PAUSE)
                    error |= ClothoidPlannerInterface::GenerateTraj(NULL, &m_path, runtime, CmdArgs::sn_key, false);
                } else {
                  // replan by updating the clothoid tree
                  if (state_problems_p[0]->state != PAUSE)
                    error |= ClothoidPlannerInterface::GenerateTraj(&cSpecs, &m_path, runtime, CmdArgs::sn_key, false);
                }

                if (state_problems_p[0]->state == PAUSE || ((error & S1PLANNER_PATH_INCOMPLETE) && s1planner_failure_count < S1THRUST)) {
                  m_path.pathLen = 0;
                }
                if (error & S1PLANNER_PATH_INCOMPLETE)
                  s1planner_failure_count++;

                DGCgettime(time2);
                Log::getStream(9) << "ZONE: s1planner path generation execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  

                Utils::displayPath(-5, &m_path, 3, MAP_COLOR_BLUE, 12501);
		
                // EXTRACT THE SUBPATH
                DGCgettime(time1);
                error |= PlannerUtils::extractS1SubPath(subPath, m_path, vehState, actState.m_estoppos);
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: extract subpath execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  
                Utils::displayPath(-5, &subPath, 3, MAP_COLOR_GREEN, 12502);
		
                // PLAN THE VEL/TRAJ
                DGCgettime(time1);
                error |= VelPlanner::planVel(&m_traj, &m_graph, &subPath, *(state_problems_p[0]), vehState, velParams);
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: vel planner execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  
                // DECIDE WHETHER A REPLAN IS NECESSARY
                replanInZone(&m_path, error, vehState, &cSpecs, state_problems_p[0]);

                // CHECK IF WE HAVE COMPLETED SPECIAL MANEAVERS
                error |= checkInternalComplete(state_problems_p[0], vehState, cSpecs);
                
                break;
              }
              //#endif
            case DPLANNER:
              {
                Console::addMessage("PLANNER: PLANTRAJECTORY: THIS PLANNER IS NOT YET IMPLEMENTED FOR ROAD_REGION");
                Log::getStream(1) <<"PLANNER: PLANTRAJECTORY: THIS PLANNER IS NOT YET IMPLEMENTED FOR ROAD_REGION" << endl;
                break;
              }

              //#if USE_CIRCLE_PLANNER              
            case CIRCLE_PLANNER:
              {
                if ( (m_currSegGoals.exitSegmentID == 0) && 
                     (m_currSegGoals.exitLaneID == 0) && 
                     (m_currSegGoals.exitWaypointID == 0) )
                  break;

                // get the planning problem
                CSpecs_t cSpecs;
                cSpecs = CSpecs_t();
                PlanGraphPath subPath;
                
                // Specify the final pose that we want to plan to
                finPose = getRoadZoneFinPose(state_problems_p[0], vehState);
                error |= ZoneCorridor::setupCSpecs(cSpecs, m_traffStateEst->getMap(), vehState, actState, m_currSegGoals, *(state_problems_p[0]), pathParams, finPose);
                
                Utils::printCSpecs(&cSpecs);


                // Send the costmap (debug info) if specified (off by default)
                if (CmdArgs::show_costmap) {
                  BitmapParams bmParams = cSpecs.getBitmapParams(); 
                  m_polyTalker.send(&bmParams);
                }

                error |= CirclePlanner::GenerateTraj(&cSpecs, &m_path, m_replanInZone);

                DGCgettime(time2);
                Log::getStream(9) << "ZONE: circle-planner path generation execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  

                Utils::displayPath(-5, &m_path, 3, MAP_COLOR_BLUE, 12501);
                
                // EXTRACT THE SUBPATH
                DGCgettime(time1);
                error |= PlannerUtils::extractSubPath(subPath, m_path, vehState, actState.m_estoppos);
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: extract subpath execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  

                Utils::displayPath(-5, &subPath, 3, MAP_COLOR_GREEN, 12502);

                // PLAN THE VEL/TRAJ
                DGCgettime(time1);
                error |= VelPlanner::planVel(&m_traj, &m_graph, &subPath, *(state_problems_p[0]), vehState, velParams);
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: vel planner execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  
                // DECIDE WHETHER A REPLAN IS NECESSARY
                replanInZone(&m_path, error, vehState, &cSpecs, state_problems_p[0]);

                // CHECK IF WE HAVE COMPLETED SPECIAL MANEAVERS
                error |= checkInternalComplete(state_problems_p[0], vehState, cSpecs);

                break;
              }
              //#endif
            default:
              {
                Console::addMessage("PLANNER: PLANTRAJECTORY: SHOULD NEVER GET HERE");
                Log::getStream(1) <<"PLANNER: PLANTRAJECTORY: SHOULD NEVER GET HERE" << endl;
              }
            }

          break;
        }

        //#if USE_ZONES
      case ZONE_REGION:
        {
          // get the planning problem
          CSpecs_t cSpecs;
          cSpecs = CSpecs_t();
          PlanGraphPath subPath;
          //          Console::addMessage("In Zone: Final conditions = ( %3.2f, %3.2f) and heading = %3.2f", finPose.pos.x, finPose.pos.y, finPose.rot);
          Log::getStream(8) << "In Zone: Final conditions = (" << finPose.pos.x << "," <<  finPose.pos.y << " and heading = " << finPose.rot << endl;
             
          error |= ZoneCorridor::setupCSpecs(cSpecs, m_traffStateEst->getMap(), vehState, actState, m_currSegGoals, *(state_problems_p[0]), pathParams, finPose);
          Utils::printCSpecs(&cSpecs);

          //          Log::getStream(8) << "In Zone: finished getting the cspecs" << endl;

          // Send the costmap (debug info) if specified (off by default)
          if (CmdArgs::show_costmap) {
            BitmapParams bmParams = cSpecs.getBitmapParams(); 
            m_polyTalker.send(&bmParams);
          }
          
          switch (state_problems_p[0]->planner) 
            {
            case RAIL_PLANNER:
              {
                if(CmdArgs::use_rrt_planner){
                  error |= planRRTTrajectory(vehState, actState, state_problems_p[0],
                                              pathParams, velParams, curPose, finPose);
                
                }
                else{
                  // Generate trajectory using the rail planner
                  error |= planRailTrajectory(vehState, actState, state_problems_p[0],
                                              pathParams, velParams, curPose, finPose);
                }
                break;
              }
              //#if USE_S1PLANNER
            case S1PLANNER:
              {
                double runtime = 0.2;

                // PLAN THE PATH
                DGCgettime(time1);
                if (m_replanInZone) {
                  // delete the clothoid tree and replan from scratch
                  ClothoidPlannerInterface::clearClothoidTree(&cSpecs);
                  if (state_problems_p[0]->state != PAUSE)
                    error |= ClothoidPlannerInterface::GenerateTraj(NULL, &m_path, runtime, CmdArgs::sn_key, false);
                } else {
                  // replan by updating the clothoid tree
                  if (state_problems_p[0]->state != PAUSE)
                    error |= ClothoidPlannerInterface::GenerateTraj(&cSpecs, &m_path, runtime, CmdArgs::sn_key, false);
                }

                if (state_problems_p[0]->state == PAUSE || ((error & S1PLANNER_PATH_INCOMPLETE) && s1planner_failure_count < S1THRUST)) {
                  m_path.pathLen = 0;
                }
                if (error & S1PLANNER_PATH_INCOMPLETE)
                  s1planner_failure_count++;
                
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: s1planner path generation execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  
                
                Utils::displayPath(-5, &m_path, 3, MAP_COLOR_BLUE, 12501);

                // EXTRACT THE SUBPATH
                DGCgettime(time1);
                error |= PlannerUtils::extractS1SubPath(subPath, m_path, vehState, actState.m_estoppos);
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: extract subpath execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  
                //Utils::printPath(&subPath);
                //Utils::displayPath(-5, &subPath, 3, MAP_COLOR_GREEN, 12502);

                // PLAN THE VEL/TRAJ
                DGCgettime(time1);
                error |= VelPlanner::planVel(&m_traj, &m_graph, &subPath, *(state_problems_p[0]), vehState, velParams);
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: vel planner execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  
                // DECIDE WHETHER A REPLAN IS NECESSARY
                replanInZone(&m_path, error, vehState, &cSpecs, state_problems_p[0]);

                // CHECK IF WE HAVE COMPLETED SPECIAL MANEAVERS
                error |= checkInternalComplete(state_problems_p[0], vehState, cSpecs);
                break;
              }
              //#endif
              
#if USE_OTG
            case DPLANNER:
              {
                int info = 0; 
                
                double runtime = 0.6;

                // PLAN THE PATH
                DGCgettime(time1);
	
                if (m_replanInZone) {
                  
					if (state_problems_p[0]->state == PAUSE)
					{
	                    Log::getStream(4) << "DPLANNER, catches a pause!!!!!!! " <<endl;
						CTraj traj1(3);
						//CTraj traj(3);
						traj1.setMinSpeed(0);
						traj1.setDirection(1);
						traj1.startDataInput();
		    			traj1.addPoint(0,0,0, 0,0,0);
						m_traj = traj1;
					}
					else
					{
	                  DGCgettime(time1);
	                  replanWithOtg(cSpecs, &info, &m_path, &m_traj); 	  
	                  DGCgettime(time2);
	                  
	                  Log::getStream(4) << "ZONE: DPLANNER, createOtgTrajectory: " << 
					  			(time2-time1)/1000.0 << " ms" << endl;      
	                  Log::getStream(4) << "ZONE: DPLANNER, m_traj direction " << 
					  			m_traj.getDirection()<<endl;
	                  Log::getStream(4) << "ZONE: DPLANNER, m_traj final point (" << 
					  			m_traj.lastN()<< ", "<<m_traj.lastE()<< ")"<<endl;
			  
	                  if(1<= info && info<=3) { 
	                    Console::addMessage("DPLANNER: created GOOD traj!!!");
	                    Log::getStream(4) << "DPLANNER, created GOOD traj "<<endl;
	                    m_replanInZone = false;
	                    
	                  } else {
	                    Console::addMessage("DPLANNER: created BAD traj!!!", info);
	                    Log::getStream(4) << "DPLANNER, created BAD traj, do not send traj "<<info<<endl;
	                    ++error;
	                    m_replanInZone = true; 
	                  }		 
	                }
				}
                // DECIDE WHETHER A REPLAN IS NECESSARY

                if (info < 4)
                {
					vector<CSpecsObstacle> myObstacles = cSpecs.getLinearObstacles();
					double ob_x, ob_y, a, b, val;
	                Log::getStream(4) << "DPLANNER, testing m_traj "<<info<<endl;
	                Log::getStream(4) << "DPLANNER, "<< myObstacles.size() << " obstacles " <<endl;
					point2 alicePos = AliceStateHelper::getPositionRearAxle(vehState);
					int index = m_traj.getClosestPoint(alicePos.x, alicePos.y);
					for (int j = index; m_replanInZone == false && j < m_traj.getNumPoints(); j+=5 )
					{
					 	for (int n=0;n<myObstacles.size();n++)
						{
							ob_x = myObstacles[n].x; 
							ob_y = myObstacles[n].y; 
							a = myObstacles[n].length+0.5; 
							b = myObstacles[n].width+0.5;
							val = pow((m_traj.getNorthing(j) - ob_x)/a,2) + pow((m_traj.getEasting(j) - ob_y)/b, 2);
							if (val<1)
							{
	                    		Console::addMessage("DPLANNER: Obstacle in the way");
								Log::getStream(4) << "DPLANNER, obstacle is obstructing trajectory "<< n <<endl;
								Log::getStream(4) << "DPLANNER, x = " << m_traj.getNorthing(j) << ", y = " 
									<< m_traj.getEasting(j) << ", ob_x = " << ob_x << ", ob_y = " << ob_y << endl;
								
								m_replanInZone = true;
								CTraj traj1(3);
								//CTraj traj(3);
								traj1.setMinSpeed(0);
								traj1.setDirection(1);
								traj1.startDataInput();
				    			traj1.addPoint(0,0,0, 0,0,0);
								m_traj = traj1;

								break;
							}
						}
					}
					
				    point2 vehPos = AliceStateHelper::getPositionRearAxle(vehState);
					index = m_traj.getNumPoints() - 1; 
				    point2 finalTrajPt = point2(m_traj.getNorthing(index), m_traj.getEasting(index));
					if (finalTrajPt.dist(vehPos) < 1 && AliceStateHelper::getVelocityMag(vehState) < 0.2)
				    {
						Log::getStream(4) << "ZONE - Jeremy says we finished traj segment (?)"<< endl;
				       	m_replanInZone = true; 
				    }
				
                    //replanInZone(&subPath, error, vehState, &cSpecs, state_problems_p[0]);
                }
				
				// CHECK IF WE HAVE COMPLETED SPECIAL MANEUVERS
                error |= checkInternalComplete(state_problems_p[0], vehState, cSpecs);
                break;
              }
#endif

              //#if USE_CIRCLE_PLANNER
            case CIRCLE_PLANNER:
              {
                // PLAN THE PATH
		//		Utils::printErrorInBinary(error);
                DGCgettime(time1);
		Console::addMessage("replan in zone = %d", m_replanInZone);
		Log::getStream(8) << "replan in zone = " <<  m_replanInZone << endl;
                error |= CirclePlanner::GenerateTraj(&cSpecs, &m_path, m_replanInZone);
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: circle-planner path generation execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  

		//		Utils::printErrorInBinary(error);
                Utils::displayPath(-5, &m_path, 3, MAP_COLOR_BLUE, 12501);
		//		Utils::printPath(&m_path);
                // EXTRACT THE SUBPATH
                DGCgettime(time1);

                // use the old extract function
                error |= PlannerUtils::extractS1SubPath(subPath, m_path, vehState, actState.m_estoppos);

                DGCgettime(time2);
                Log::getStream(9) << "ZONE: extract subpath execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  

                Utils::displayPath(-5, &subPath, 3, MAP_COLOR_GREEN, 12502);

                // PLAN THE VEL/TRAJ
                DGCgettime(time1);
                error |= VelPlanner::planVel(&m_traj, &m_graph, &subPath, *(state_problems_p[0]), vehState, velParams);
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: vel planner execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  
                // DECIDE WHETHER A REPLAN IS NECESSARY
                replanInZone(&m_path, error, vehState, &cSpecs, state_problems_p[0]);
                // CHECK IF WE HAVE COMPLETED SPECIAL MANEAVERS
                error |= checkInternalComplete(state_problems_p[0], vehState, cSpecs);


#if (0)
                pose2f_t subPathFinPose = subPath.nodes[subPath.pathLen-1]->pose;
                double distToFinal = vec2f_mag(vec2f_sub(subPathFinPose.pos, finPose.pos));
                double distThresh = 1.0; 
                if ( distToEnd<distThresh && distToFinal>distThresh) {
                  // if we are close to the end of the path, but the path does not reach the goal, replan.
                  Log::getStream(4) <<"Jeremy says we're close to the end of the path but it doesn't reach the goal" << endl;
                  m_replanInZone = true;
                }
#endif

                break;
              }
              //#endif
            default:
              {
                Console::addMessage("PLANNER: PLANTRAJECTORY: SHOULD NEVER GET HERE");
                Log::getStream(1) <<"PLANNER: PLANTRAJECTORY: SHOULD NEVER GET HERE" << endl;
              }
            }
          break;
        }
        //#endif
        
      case INTERSECTION:
        {
          switch (state_problems_p[0]->planner) 
            {
            case RAIL_PLANNER:
              {
//                if(CmdArgs::use_rrt_planner){
//                  error |= planRRTTrajectory(vehState, actState, state_problems_p[0],
//                                              pathParams, velParams, curPose, finPose);                
//                }
//                else{
                Console::addMessage("PLANNER: PLANTRAJECTORY: THIS PLANNER IS NOT YET IMPLEMENTED FOR ROAD_REGION");
                Log::getStream(1) <<"PLANNER: PLANTRAJECTORY: THIS PLANNER IS NOT YET IMPLEMENTED FOR ROAD_REGION" << endl;//              }
                break;
              }
              //#if USE_S1PLANNER
            case S1PLANNER:
              {
                // get the planning problem
                CSpecs_t cSpecs;
                cSpecs = CSpecs_t();
                PlanGraphPath subPath;
	    
                error |= ZoneCorridor::setupCSpecs(cSpecs, m_traffStateEst->getMap(), vehState, actState, m_currSegGoals, *(state_problems_p[0]), pathParams, finPose);
	    
                // Send the costmap (debug info) if specified (off by default)
                if (CmdArgs::show_costmap) {
                  BitmapParams bmParams = cSpecs.getBitmapParams(); 
                  m_polyTalker.send(&bmParams);
                }
	    
                double runtime = 0.2;
	    
                // PLAN THE PATH
                DGCgettime(time1);
                if (m_replanInZone) {
                  // delete the clothoid tree and replan from scratch
                  ClothoidPlannerInterface::clearClothoidTree(&cSpecs);
		  if (state_problems_p[0]->state != PAUSE)
                    error |= ClothoidPlannerInterface::GenerateTraj(NULL, &m_path, runtime, CmdArgs::sn_key, false);
                } else {
                  // replan by updating the clothoid tree
		  if (state_problems_p[0]->state != PAUSE)
                    error |= ClothoidPlannerInterface::GenerateTraj(&cSpecs, &m_path, runtime, CmdArgs::sn_key, false);
                }
		if (state_problems_p[0]->state == PAUSE || ((error & S1PLANNER_PATH_INCOMPLETE) && s1planner_failure_count < S1THRUST)) {
		  m_path.pathLen = 0;
		}
		if (error & S1PLANNER_PATH_INCOMPLETE)
		  s1planner_failure_count++;
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: s1planner path generation execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  
	    
                Utils::displayPath(-5, &m_path, 3, MAP_COLOR_BLUE, 12501);
	    
                // EXTRACT THE SUBPATH
                DGCgettime(time1);
                error |= PlannerUtils::extractS1SubPath(subPath, m_path, vehState, actState.m_estoppos);
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: extract subpath execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  
	    
                Utils::displayPath(-5, &subPath, 3, MAP_COLOR_GREEN, 12502);
	    
                // PLAN THE VEL/TRAJ
                DGCgettime(time1);
                error |= VelPlanner::planVel(&m_traj, &m_graph, &subPath, *(state_problems_p[0]), vehState, velParams);
                DGCgettime(time2);
                Log::getStream(9) << "ZONE: vel planner execution time: " << (time2-time1)/1000.0 << " ms" << endl;      	  
                // DECIDE WHETHER A REPLAN IS NECESSARY
                replanInZone(&m_path, error, vehState, &cSpecs, state_problems_p[0]);
	    
                break;
              }
              //#endif
            case DPLANNER:
            case CIRCLE_PLANNER:
              {
                Console::addMessage("PLANNER: PLANTRAJECTORY: THIS PLANNER IS NOT YET IMPLEMENTED FOR ROAD_REGION");
                Log::getStream(1) <<"PLANNER: PLANTRAJECTORY: THIS PLANNER IS NOT YET IMPLEMENTED FOR ROAD_REGION" << endl;
                break;
              }
              
            default:
              {
                Console::addMessage("PLANNER: PLANTRAJECTORY: SHOULD NEVER GET HERE");
                Log::getStream(1) <<"PLANNER: PLANTRAJECTORY: SHOULD NEVER GET HERE" << endl;
              }
            }


          break;
        }
      default:
        {
          Console::addMessage("PLANNER: PLANTRAJECTORY: SHOULD NEVER GET HERE");
          Log::getStream(1) <<"PLANNER: PLANTRAJECTORY: SHOULD NEVER GET HERE" << endl;
        }
      }

    // Reset this flag when not in a zone region
    if (state_problems_p[0]->planner == RAIL_PLANNER) {
      Log::getStream(4) <<"Jeremy says we're using rail planner so we must replaninzone" << endl;
      
	  m_replanInZone = true;
    }
    // Force plan from current pos when we exit backup
    if (state_problems_p[0]->state == BACKUP) {
      m_planFromCurrPos = true;
    }

    if (!(error & S1PLANNER_PATH_INCOMPLETE))
      s1planner_failure_count = 0;
    // reset the first time in backup flag
    if ( (state_problems_p[0]->state != BACKUP) && (!(m_firstTimeBackup)) )
      m_firstTimeBackup = true;
    // reset the first time in uturn flag
    if ( (state_problems_p[0]->state != UTURN) && (!(m_firstTimeUTurn)) )
      m_firstTimeUTurn = true;

  }
                
  /* Allow map to be updated now */
  m_traffStateEst->meltMap();

  return error;

}




// Plan path from pose A to pose B using the rrt planner.
Err_t Planner::planRRTTrajectory(VehicleState vehState, ActuatorState actState,
                                  StateProblem_t* state_problem, Path_params_t pathParams,
                                  Vel_params_t velParams, pose2f_t curPose, pose2f_t finPose)
{
  int status;
  float maxDist;
  uint16_t include, exclude;
  float steerAngle;
  PlanGraphPathDirection startDir;
  PlanGraphNode *startNode, *goalNode;

  unsigned long long time1, time2;

  bool allowOffRoad = false;
  bool allowPass = false;
  bool allowReverse = false;
  
  status = PP_OK;
  startNode = NULL;
  goalNode = NULL;

  // Pick up the current transmission state: foward or reverse.
  startDir = (actState.m_TransmissionPosition < 0 ? PLAN_GRAPH_PATH_REV : PLAN_GRAPH_PATH_FWD);

  m_railStartPose = curPose;
  m_railGoalPose = finPose;
  m_railStartNode = NULL;
  m_railGoalNode = NULL;
  
  // Enable off-road driving.
  if (state_problem->flag == OFFROAD) {
    Console::addMessage("In off-road mode");
    Log::getStream(1) << "In off-road mode" << endl; 
    allowOffRoad = true;
    allowPass = true;
    allowReverse = true;
  } else if (state_problem->state == BACKUP) {
    allowOffRoad = false;
    allowPass = false;
    allowReverse = true;
    m_goalWaypoints.clear();
  } else {
    allowOffRoad = false;    
    allowPass = (state_problem->flag == PASS || state_problem->flag == PASS_REV);
    allowReverse = (state_problem->flag == PASS_REV);
  }
  
  // Get the goal node; we will go to anthing that is not in an
  // oncoming lane (i.e., the correct goal position but going the
  // wrong way).
  DGCgettime(time1);
  if (m_evasive) {
    include = PLAN_GRAPH_NODE_VOLATILE;
    exclude = PLAN_GRAPH_NODE_ONCOMING | PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_ZONE;
  } else if (state_problem->state == BACKUP) {
    include = PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_ZONE | PLAN_GRAPH_NODE_TURN;
    exclude = PLAN_GRAPH_NODE_ONCOMING;
  } else {
    include = PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_ZONE;
    exclude = PLAN_GRAPH_NODE_ONCOMING | PLAN_GRAPH_NODE_VOLATILE;
  }
  // Actually get the goal node
  goalNode = m_graph.getNearestPos(finPose.pos.x, finPose.pos.y, 1.0, include, exclude);
  DGCgettime(time2); 
  Log::getStream(10) << "Get goal node execution time: " << (time2-time1)/1000.0 << " ms" << endl;
  
  // We need both start and goal nodes to make a path.
  if (goalNode == NULL)
  {
    Console::addMessage("goal not found");
    Log::getStream(1) << "goal not found";
    status |= PP_NOPATH_LEN;
    return status;
  } else {
    Log::getStream(1) << "goal found";
  }
  // Record the goal for visualization.
  m_railGoalNode = goalNode;

  // Nodes must be within this distance to be eligable as starting nodes.
  DGCgettime(time1);
  maxDist = this->getMaxFollowerError();
  //Console::addMessage("maxDist = %3.2f", maxDist);
  Log::getStream(6) << "maxDist = " << maxDist << endl;
  DGCgettime(time2);
  Log::getStream(10) << "Get max follower distance execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // Unless otherwise directed, try to pick a start node on the current path.
  DGCgettime(time1);
  if (!allowOffRoad && !m_planFromCurrPos && m_path.pathLen > 1 && !(m_evasive && m_firstEvasive))
    startNode = PathUtils::getNearestNodeProjection(&m_path, curPose, maxDist);    
//    startNode = m_graph.getNearestPos(curPose.pos.x, curPose.pos.y, 1.0, include, exclude);
  DGCgettime(time2);
  Log::getStream(10) << "Finding start node execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // Reset evasive flag
  if (m_evasive && m_firstEvasive) m_firstEvasive = false;

  // If we could not find a start node, or have been directed to,
  // generate a new vehicle subgraph.
  if (!m_evasive && (m_planFromCurrPos || startNode == NULL))
  {
    /* Generate a sub-graph from the current vehicle position.  */
    DGCgettime(time1);
    steerAngle = actState.m_steerpos*VEHICLE_MAX_AVG_STEER;
    m_graphUpdater->setMode(allowPass, allowReverse, allowOffRoad);
    Log::getStream(1)<<"distToStopline: "<<Utils::getDistToStopline(&m_path)<<endl;
    startNode = m_graphUpdater->update(curPose, steerAngle, goalNode);
    Log::getStream(1)<<"REPLAN FROM CURRENT POSITION"<<endl;
    Log::getStream(1)<<"distToStopline: "<<Utils::getDistToStopline(&m_path)<<endl;
    DGCgettime(time2);
    Log::getStream(9) << "Gen vehicle graph execution time: " << (time2-time1)/1000.0 << " ms" << endl;
  }

  // This should never happen, but return error if it does.
  if (!startNode)
  {
    status |= PP_NONODEFOUND;
    return status;
  }

  // Record the start for visualization.
  m_railStartNode = startNode;
  
  // Record this start node for later use; may not be necessary.
  m_graph.vehicleNode = startNode;
  
  // Unfix the current path (which will set the path length to zero
  // and may delete nodes).  We are about to re-generate it anyway.
  DGCgettime(time1);
  m_path.unfix(&m_graph);
  DGCgettime(time2);
  Log::getStream(10) << "unfix graph execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // actually plan the path
  DGCgettime(time1);
  if (m_rrtPlanner->planPath(startNode, startDir, goalNode, &m_path, m_traffStateEst->getMap()) != 0) {
    // we did not find anything, so if we have already generated the vechicle subgraph
	  // try relaxing the constraints and plan again
    Console::addMessage("ERROR: could not find path the first time, vehiclesubgraph = %d", m_planFromCurrPos);
    Log::getStream(1) << "ERROR: could not find path the first time, vehiclesubgraph = " << m_planFromCurrPos << endl;
    status |= PP_NOPATH_LEN;
    m_planFromCurrPos = true;
  
  } else { // we found a path
	status |= PP_OK;
  }
  DGCgettime(time2);
  Log::getStream(10) << "Plan path execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // DEBUG
  if (state_problem->flag == PASS_REV) {
    Utils::printPath(&m_path,5);
    Log::getStream(5) << "PASS_REV: PathLen after rail planner = " <<m_path.pathLen << endl;; 
  }

  // Fix this path in memory so it doesn't get deleted in subsequent graph operations.
  DGCgettime(time1);
  m_path.fix(&m_graph);
  DGCgettime(time2);
  Log::getStream(10) << "fix graph execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // evaluate this path
  // are we colliding?
  DGCgettime(time1);
  if (m_path.pathLen>0)
    status |= this->evaluatePathForCollisions(state_problem, vehState); 
  DGCgettime(time2);
  Log::getStream(10) << "Evaluate path for collisions execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // Overwrite the direction on the first state to correspond to that on the 
  // second.  This is a hack to make the planner logic work correctly.
  if (m_path.pathLen >= 2)
    m_path.directions[0] = m_path.directions[1];

  DGCgettime(time1);  
  status |= VelPlanner::planVel(&m_traj, &m_graph, &m_path, *state_problem, vehState, velParams);
  DGCgettime(time2);
  Log::getStream(10) << "plan velocity execution time: " << (time2-time1)/1000.0 << " ms" << endl;


  return status;    
}

// Plan path from pose A to pose B using the rail planner.
Err_t Planner::planRailTrajectory(VehicleState vehState, ActuatorState actState,
                                  StateProblem_t* state_problem, Path_params_t pathParams,
                                  Vel_params_t velParams, pose2f_t curPose, pose2f_t finPose)
{
  int status;
  float maxDist;
  uint16_t include, exclude;
  float steerAngle;
  PlanGraphPathDirection startDir;
  PlanGraphNode *startNode, *goalNode;

  unsigned long long time1, time2;

  bool allowOffRoad = false;
  bool allowPass = false;
  bool allowReverse = false;
  
  status = PP_OK;
  startNode = NULL;
  goalNode = NULL;

  // Pick up the current transmission state: foward or reverse.
  startDir = (actState.m_TransmissionPosition < 0 ? PLAN_GRAPH_PATH_REV : PLAN_GRAPH_PATH_FWD);

  m_railStartPose = curPose;
  m_railGoalPose = finPose;
  m_railStartNode = NULL;
  m_railGoalNode = NULL;
  
  // Enable off-road driving.
  if (state_problem->flag == OFFROAD) {
    Console::addMessage("In off-road mode");
    Log::getStream(1) << "In off-road mode" << endl; 
    allowOffRoad = true;
    allowPass = true;
    allowReverse = true;
  } else if (state_problem->state == BACKUP) {
    allowOffRoad = false;
    allowPass = false;
    allowReverse = true;
    m_goalWaypoints.clear();
  } else {
    allowOffRoad = false;    
    allowPass = (state_problem->flag == PASS || state_problem->flag == PASS_REV);
    allowReverse = (state_problem->flag == PASS_REV);
  }
  
  // DEBUG
  if (state_problem->flag == PASS_REV) {
    Utils::printPath(&m_path,5);
    Log::getStream(5) << "PASS_REV: PathLen BEFORE planning = " <<m_path.pathLen << endl;; 
  }


  // if commanded, re-read the path planner weights
  if (m_readPathPlanParams) {
    // Load the weights from a file
    if (m_railPlanner->loadWeights(CmdArgs::path_plan_config) == 0)
      Console::addMessage("Read the path planning weights from file");
    else
      Console::addMessage("FAILED to read the path planning weights from file");
  }

  Console::updateEvasion(m_evasive || m_evasion_stopped);

  // Generate evasion plan
  if (m_evasive && m_firstEvasive) {
    Console::addMessage("Generating evasion");
    Log::getStream(10) << "Preparing evasion maneuver" << endl;
    startNode = m_graphUpdater->genEvasive(m_evasiveStart, actState.m_steerpos*VEHICLE_MAX_AVG_STEER, m_evasiveEnd, 2.0, 2.0);
  }

  // If we are trying to back up, then adjust the final condition
  if (state_problem->state == BACKUP)
    finPose = getRoadZoneFinPose(state_problem, vehState);

  // Get the goal node; we will go to anthing that is not in an
  // oncoming lane (i.e., the correct goal position but going the
  // wrong way).
  DGCgettime(time1);
  if (m_evasive) {
    include = PLAN_GRAPH_NODE_VOLATILE;
    exclude = PLAN_GRAPH_NODE_ONCOMING | PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_ZONE;
  } else if (state_problem->state == BACKUP) {
    include = PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_ZONE | PLAN_GRAPH_NODE_TURN;
    exclude = PLAN_GRAPH_NODE_ONCOMING;
  } else {
    include = PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_ZONE;
    exclude = PLAN_GRAPH_NODE_ONCOMING | PLAN_GRAPH_NODE_VOLATILE;
  }
  goalNode = m_graph.getNearestPos(finPose.pos.x, finPose.pos.y, 1.0, include, exclude);
  DGCgettime(time2); 
  Log::getStream(10) << "Get goal node execution time: " << (time2-time1)/1000.0 << " ms" << endl;
  
  // We need both start and goal nodes to make a path.
  if (goalNode == NULL)
  {
    Console::addMessage("goal not found");
    Log::getStream(1) << "goal not found";
    status |= PP_NOPATH_LEN;
    return status;
  } else {
    Log::getStream(1) << "goal found";
  }
  // Record the goal for visualization.
  m_railGoalNode = goalNode;

  // Nodes must be within this distance to be eligable as starting nodes.
  DGCgettime(time1);
  maxDist = this->getMaxFollowerError();
  //Console::addMessage("maxDist = %3.2f", maxDist);
  Log::getStream(6) << "maxDist = " << maxDist << endl;
  DGCgettime(time2);
  Log::getStream(10) << "Get max follower distance execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // Unless otherwise directed, try to pick a start node on the current path.
  DGCgettime(time1);
  if (!allowOffRoad && !m_planFromCurrPos && m_path.pathLen > 1 && !(m_evasive && m_firstEvasive))
    startNode = PathUtils::getNearestNodeProjection(&m_path, curPose, maxDist);    
  DGCgettime(time2);
  Log::getStream(10) << "Finding start node execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // Reset evasive flag
  if (m_evasive && m_firstEvasive) m_firstEvasive = false;

  // If we could not find a start node, or have been directed to,
  // generate a new vehicle subgraph.
  if (!m_evasive && (m_planFromCurrPos || startNode == NULL))
  {
    /* Generate a sub-graph from the current vehicle position.  */
    DGCgettime(time1);
    steerAngle = actState.m_steerpos*VEHICLE_MAX_AVG_STEER;
    m_graphUpdater->setMode(allowPass, allowReverse, allowOffRoad);
    Log::getStream(1)<<"distToStopline: "<<Utils::getDistToStopline(&m_path)<<endl;
    startNode = m_graphUpdater->update(curPose, steerAngle, goalNode);
    Log::getStream(1)<<"REPLAN FROM CURRENT POSITION"<<endl;
    Log::getStream(1)<<"distToStopline: "<<Utils::getDistToStopline(&m_path)<<endl;
    DGCgettime(time2);
    Log::getStream(9) << "Gen vehicle graph execution time: " << (time2-time1)/1000.0 << " ms" << endl;
  }

  // This should never happen, but return error if it does.
  if (!startNode)
  {
    status |= PP_NONODEFOUND;
    return status;
  }

  // Record the start for visualization.
  m_railStartNode = startNode;
  
  // Record this start node for later use; may not be necessary.
  m_graph.vehicleNode = startNode;
  
  // Update the ROI in the graph.  
  DGCgettime(time1);
  m_graph.updateROI(curPose.pos.x, curPose.pos.y, 128); // MAGIC
  DGCgettime(time2);
  Log::getStream(10) << "Update ROI execution time: " << (time2-time1)/1000.0 << " ms" << endl;
            
  /* Update graph according to the new state problem */
  DGCgettime(time1);
  //    Console::addMessage("Using the NEW graph update function");
  m_traffStateEst->updateStatus(&m_graph, *state_problem);
  DGCgettime(time2);
  Log::getStream(10) << "Update nodes' status execution time: " << (time2-time1)/1000.0 << " ms" << endl;


  if (!(CmdArgs::disable_fused_perceptor) ) {
    DGCgettime(time1);
    // Update the graph based on the fused map data.  The first form
    // applies cost to all nodes individually, the second uses a winner-take-all
    // approach to select the preferred rail.
    if (allowOffRoad)
      m_traffStateEst->updateFusedGraph(&m_graph, &vehState);
    else
      m_traffStateEst->updateFusedGraphAlt(&m_graph, &vehState);
    DGCgettime(time2);
    Log::getStream(10) << "Update fused graph execution time: " << (time2-time1)/1000.0 << " ms" << endl;
  }
                
  // Set the mode; do we allow passing?
  DGCgettime(time1);
  m_railPlanner->setMode(allowPass, allowReverse);
  DGCgettime(time2);
  Log::getStream(10) << "Set mode execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // Set the current speed to control swerving.
  DGCgettime(time1);
  m_railPlanner->setSpeed(vehState.vehSpeed);
  DGCgettime(time2);
  Log::getStream(10) << "Set speed execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // Unfix the current path (which will set the path length to zero
  // and may delete nodes).  We are about to re-generate it anyway.
  DGCgettime(time1);
  m_path.unfix(&m_graph);
  DGCgettime(time2);
  Log::getStream(10) << "unfix graph execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // constrain the planner to consider the requested plan from mplanner
  DGCgettime(time1);
  m_railPlanner->setCorridor(&m_goalWaypoints);
  DGCgettime(time2);
  Log::getStream(10) << "Set corridor execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // enable/disable evasive-style maneuvers
  if (m_evasive)
    m_railPlanner->setGoalRadius(2.0, true);
  else
    m_railPlanner->setGoalRadius(3.0, false);

  // actually plan the path
  DGCgettime(time1);
  if (m_railPlanner->planPath(startNode, startDir, goalNode, &m_path) != 0) {
    // we did not find anything, so if we have already generated the vechicle subgraph
	  // try relaxing the constraints and plan again
    Console::addMessage("ERROR: could not find path the first time, vehiclesubgraph = %d", m_planFromCurrPos);
    Log::getStream(1) << "ERROR: could not find path the first time, vehiclesubgraph = " << m_planFromCurrPos << endl;
    // TESTING
    //if (m_planFromCurrPos) {
    //  m_railPlanner->setCorridor(NULL);
    //  if (m_railPlanner->planPath(startNode, startDir, goalNode, &m_path) != 0) {
    //	status |= PP_NOPATH_LEN;
    //	Console::addMessage("ERROR: could not find path the second time, vehiclesubgraph = %d", m_planFromCurrPos);
    //	Log::getStream(1) << "ERROR: could not find path the second time, vehiclesubgraph = " << m_planFromCurrPos << endl;
    //  }
    //} else // we have not replanned yet, so try that first
    status |= PP_NOPATH_LEN;
    m_planFromCurrPos = true;
  
  } else { // we found a path
	status |= PP_OK;
  }
  DGCgettime(time2);
  Log::getStream(10) << "Plan path execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // DEBUG
  if (state_problem->flag == PASS_REV) {
    Utils::printPath(&m_path,5);
    Log::getStream(5) << "PASS_REV: PathLen after rail planner = " <<m_path.pathLen << endl;; 
  }

  // Fix this path in memory so it doesn't get deleted in subsequent graph operations.
  DGCgettime(time1);
  m_path.fix(&m_graph);
  DGCgettime(time2);
  Log::getStream(10) << "fix graph execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // If we got bad path, we should keep re-generating the subgraph.
  // Otherwise, stick with the path we have.
  if ( (status & PP_NONODEFOUND) || (status & PP_NOPATH_LEN) )
    m_planFromCurrPos = true;
  else
    m_planFromCurrPos = false;

  // evaluate this path
  // are we colliding?
  DGCgettime(time1);
  if (m_path.pathLen>0)
    status |= this->evaluatePathForCollisions(state_problem, vehState); 
  DGCgettime(time2);
  Log::getStream(10) << "Evaluate path for collisions execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // Overwrite the direction on the first state to correspond to that on the 
  // second.  This is a hack to make the planner logic work correctly.
  if (m_path.pathLen >= 2)
    m_path.directions[0] = m_path.directions[1];

  /* Update path nodes according to the sensed stoplines */
  //  Utils::updatePathStoplines(&m_path, m_traffStateEst->getMap(), &m_graph, vehState);

  /* Plan the trajectory */
  PlanGraphPath subPath = {0};
  // REMOVE memset(&subPath, 0, sizeof(subPath));
  if (allowReverse)
    status |= PlannerUtils::extractRailSubPath(subPath, m_path, actState.m_estoppos);
  else 
    subPath = m_path;

  // DEBUG
  if (state_problem->flag == PASS_REV) {
    Utils::printPath(&m_path,5);
    Log::getStream(5) << "PASS_REV: PathLen after subpath extraction = " <<m_path.pathLen << endl;; 
  }


  DGCgettime(time1);  
  status |= VelPlanner::planVel(&m_traj, &m_graph, &subPath,
                                *state_problem, vehState, velParams);
  DGCgettime(time2);
  Log::getStream(10) << "plan velocity execution time: " << (time2-time1)/1000.0 << " ms" << endl;

  // DEBUG
  if (state_problem->flag == PASS_REV) {
    Utils::printPath(&m_path,5);
    Log::getStream(5) << "PASS_REV: PathLen after vel planner = " <<m_path.pathLen << endl;; 
  }

  return status;    
}


void Planner::replanInZone(PlanGraphPath* path, Err_t error, VehicleState vehState, CSpecs_t* cSpecs, StateProblem_t* stateProblem)
{
  //double endPtReplanThresh = 1.0;
  ActuatorState actState;

  actState = m_traffStateEst->getActState();

  if (stateProblem->region == ZONE_REGION) {
    //double endPtReplanThresh = 1.0;
    
    if (error & S1PLANNER_FAILED) {	  
      Log::getStream(4) << "ZONE: s1planner gave path of zero length - replan" << endl;
      m_replanInZone = true;
      return;
    }
    
    if (error & P_EXTRACT_SUBPATH_ERROR) {	  
      // This error means: 1st point on path was NULL or we were too far from the path
      Log::getStream(4) << "ZONE: subpath extraction failed - replan" << endl;
      m_replanInZone = true;
      return;
    }
    
    if (path->pathLen == 0) {
      Log::getStream(4) << "ZONE: path of ZERO length - replan" << endl;
      m_replanInZone = true; 
      return;
    }
    
    /* If we are stopped for more than 40 seconds, replan */
    if (Utils::monitorAliceSpeed(vehState, actState.m_estoppos) > 40.0) {
      Log::getStream(4) << "ZONE - stopped for more than 20 seconds - replan"<< endl;
      m_replanInZone = true; 
      return;
    }
    
/*
#if USE_OTG
    point2 vehPos = AliceStateHelper::getPositionRearAxle(vehState);
    point2 finalTrajPt;
    int length = path->pathLen;
    if (length > 0) {
    	PlanGraphNode* trajNode = path->nodes[length-1];
      finalTrajPt.set(trajNode->pose.pos.x, trajNode->pose.pos.y);
	   	if (stateproblem->planner == DPLANNER && finalTrajPt.dist(vehPos) < 1) {
    		Log::getStream(4) << "ZONE - Jeremy says we finished traj segment (?)"<< endl;
        m_replanInZone = true; 
    		return;
      }
      else Log::getStream(4) << "ZONE - Jeremy says we're away from endpoint: dist = " << 
             finalTrajPt.dist(vehPos) << endl << "x endpt = " << finalTrajPt.x << " y endpt = " <<
             finalTrajPt.y << endl << "x cur = " << vehPos.x << " y cur = " << vehPos.y << endl;
    }
#endif
  */  
    /*
      vector<double> finalState = cSpecs->getFinalState();
      point2 finalPos, finalPlan;
      finalPos.set(finalState[0], finalState[1]);
      int length = path->pathLen;
      if (length > 0) {
      GraphNode* node = path->nodes[length-1];
      finalPlan.set(node->pose.pos.x, node->pose.pos.y);
      if (finalPos.dist(finalPlan) > endPtReplanThresh){
      Log::getStream(4) << "ZONE - plan does not reach goal - replan - goal = (" << finalPos.x << "," << finalPos.y << ") and final plan = (" << finalPlan.x << "," << finalPlan.y << ")" << endl;
      m_replanInZone = true;
      return;
      }
      }
    */
    
  } else if (stateProblem->region == ROAD_REGION) {
    if (m_replanInZone) {
      m_replanInZone = false;
      return;
    } else return;
  }
  
  m_replanInZone = false;
  return;
}


// Convert a trajectory from site frame to local frame.
void Planner::convertTraj(const VehicleState *vehState, CTraj *traj)
{
  int i;
  double sx, sy;
  double px, py, vx, vy, ax, ay;
  pose3_t transSV, transLV, transLS;
  double m[4][4];
    
  // Construct transform from vehicle to site frame
  transSV.pos = vec3_set(vehState->siteNorthing, vehState->siteEasting, vehState->siteAltitude);
  transSV.rot = quat_from_rpy(vehState->siteRoll, vehState->sitePitch, vehState->siteYaw);

  // Construct transform from vehicle to local frame
  transLV.pos = vec3_set(vehState->localX, vehState->localY, vehState->localZ);
  transLV.rot = quat_from_rpy(vehState->localRoll, vehState->localPitch, vehState->localYaw);
  
  // Construct transform from site to vehicle frame
  transLS = pose3_mul(transLV, pose3_inv(transSV));
  pose3_to_mat44d(transLS, m);

  // Assume third order
  assert(traj->getOrder() == 3);
    
  for (i = 0; i < traj->getNumPoints(); i++)
  {
    // Rotate and translate position
    sx = traj->getNorthingDiff(i, 0);
    sy = traj->getEastingDiff(i, 0);
    px = m[0][0]*sx + m[0][1]*sy + m[0][3];
    py = m[1][0]*sx + m[1][1]*sy + m[1][3];

    // Rotate velocity
    sx = traj->getNorthingDiff(i, 1);
    sy = traj->getEastingDiff(i, 1);
    vx = m[0][0]*sx + m[0][1]*sy;
    vy = m[1][0]*sx + m[1][1]*sy;

    // Rotate velocity
    sx = traj->getNorthingDiff(i, 2);
    sy = traj->getEastingDiff(i, 2);
    ax = m[0][0]*sx + m[0][1]*sy;
    ay = m[1][0]*sx + m[1][1]*sy;

    traj->setPoint(i, px, vx, ax, py, vy, ay);
  }

  return;
}

pose2f_t Planner::getRoadZoneFinPose(StateProblem_t* stateProblem, VehicleState vehState)
{
  vector<pose2f_t> finPoses;
  pose2f_t finPose;
  
  PointLabel exitLabel = PointLabel(m_currSegGoals.exitSegmentID, m_currSegGoals.exitLaneID, m_currSegGoals.exitWaypointID);

  if (stateProblem->state == BACKUP) {

    if (m_firstTimeBackup) {
#define BACKUP_DISTANCE 5.0 // set the distance that we want to backup
      double distReversed = 0.0;
      double dotProduct;
      point2 pathPt_old, pathPt_new;
      double yaw_old, yaw_new;
      point2 currPos;
      currPos.set(AliceStateHelper::getPositionRearAxle(vehState));
      double currYaw = AliceStateHelper::getHeading(vehState);
      // set the first points to be the current points
      pathPt_old.set(currPos);
      yaw_old = currYaw;
      for (unsigned int i=0; i<m_vehStateHistory.size(); i++) {
        // get the next pt in our state history
        pathPt_new = AliceStateHelper::getPositionRearAxle(m_vehStateHistory[i]);
        yaw_new = AliceStateHelper::getHeading(m_vehStateHistory[i]);
        
        // check to see if this point is behind the previous point
        dotProduct = (pathPt_old.x-pathPt_new.x)*cos(yaw_old) + (pathPt_old.y-pathPt_new.y)*sin(yaw_old);
        Log::getStream(8) << "Dot Product = " << dotProduct << endl;
        if (dotProduct>0) {
          
          // calculate the dist to the next pt
          distReversed += pathPt_old.dist(pathPt_new);
          // see if this is far enough
          if (distReversed > BACKUP_DISTANCE) {
            m_bup_finPos = pathPt_new;
            m_bup_finHead = yaw_new;
            break;
          } 
          
          // if we have not reached the end yet, move on the the next point in the history
          pathPt_old = pathPt_new;
          yaw_old = yaw_new;
        }
        
        // if not, see if we have reached the end of the vector, in which case we want to back up straight
        if (i == m_vehStateHistory.size()-1) {
          m_bup_finPos.set(currPos.x+(-BACKUP_DISTANCE)*cos(currYaw), currPos.y+(-BACKUP_DISTANCE)*sin(currYaw));
          m_bup_finHead = currYaw;
        }
      }
      m_firstTimeBackup = false;
    }

    finPose.pos.x = m_bup_finPos.x;
    finPose.pos.y = m_bup_finPos.y;
    finPose.rot = m_bup_finHead;
    finPoses.push_back(finPose);

  } else if (stateProblem->state == UTURN) {

    if (m_firstTimeUTurn) {
      LaneLabel lane = LaneLabel(exitLabel.segment, exitLabel.lane);
      point2 currPos = AliceStateHelper::getPositionRearAxle(vehState);
      if (m_traffStateEst->getMap()->getLaneCenterPoint(m_uTurn_finPos, lane, currPos, 8.0) < 0) {
        m_traffStateEst->getMap()->getWaypoint(m_uTurn_finPos, exitLabel);
        m_traffStateEst->getMap()->getHeading(m_uTurn_finHead, exitLabel);
      } else {
        point2 tmpPt;
        m_traffStateEst->getMap()->getHeading(m_uTurn_finHead, tmpPt, lane, m_uTurn_finPos);
      }

      m_firstTimeUTurn = false;
    }

    finPose.pos.x = m_uTurn_finPos.x;
    finPose.pos.y = m_uTurn_finPos.y;
    finPose.rot = m_uTurn_finHead;
    finPoses.push_back(finPose);

  } else {

    point2 planPt;
    double heading;
    m_traffStateEst->getMap()->getWaypoint(planPt, exitLabel);
    m_traffStateEst->getMap()->getHeading(heading, exitLabel);
    finPose.pos.x = planPt.x;
    finPose.pos.y = planPt.y;
    finPose.rot = heading;
    finPoses.push_back(finPose);

  }

  return finPoses[0];
}

Err_t Planner::checkInternalComplete(StateProblem_t* stateProblem, VehicleState vehState, CSpecs_t cSpecs)
{
  double specialCompleteDist = 2.0;
  double specialCompleteTheta = M_PI/12.0;

  // CHECK IF WE HAVE COMPLETED SPECIAL MANEUVERS
  
  point2 currPos, finPos;
  double currYaw;
  vector<double> finState;
  double finYaw;
  double angleDiff;
  uint16_t include, exclude;
  PlanGraphNode *node;

  switch (stateProblem->state) {

  case BACKUP:

    // get our current pos
    currPos = AliceStateHelper::getPositionRearAxle(vehState);
    currYaw = AliceStateHelper::getHeading(vehState);
    // get our final pos
    finState = cSpecs.getFinalState();
    finPos.set(finState[STATE_IDX_X], finState[STATE_IDX_Y]);
    finYaw = finState[STATE_IDX_THETA];
    angleDiff = Utils::getAngleInRange(currYaw - finYaw);

    if ( (finPos.dist(currPos) < specialCompleteDist) &&
         (fabs(angleDiff) < specialCompleteTheta) )
    {
      return VP_BACKUP_FINISHED;
    }
    break;
                  
  case UTURN:

    // get our current pos
    currPos = AliceStateHelper::getPositionRearAxle(vehState);
    currYaw = AliceStateHelper::getHeading(vehState);
    // get our final pos
    finState = cSpecs.getFinalState();
    finPos.set(finState[STATE_IDX_X], finState[STATE_IDX_Y]);
    finYaw = finState[STATE_IDX_THETA];
    angleDiff = Utils::getAngleInRange(currYaw - finYaw);

    // Check distance and angle
    if ( (finPos.dist(currPos) < specialCompleteDist) &&
         (fabs(angleDiff) < specialCompleteTheta) )
    {
      return VP_UTURN_FINISHED;
    }

    // Check with final lane and heading
    include = PLAN_GRAPH_NODE_LANE;
    exclude = PLAN_GRAPH_NODE_ONCOMING | PLAN_GRAPH_NODE_VOLATILE;
    node = m_graph.getNearestPos(currPos.x, currPos.y, 1.0, include, exclude);
    if (node)
    {
      angleDiff = Utils::getAngleInRange(currYaw - node->pose.rot);
      if (node->segmentId == m_currSegGoals.exitSegmentID && node->laneId == m_currSegGoals.exitLaneID &&
          (fabs(angleDiff) < specialCompleteTheta) )
      {
        return VP_UTURN_FINISHED;
      }
    }
    break;

  default:
    if (stateProblem->region == ZONE_REGION && m_currSegGoals.exitLaneID != 0) {
      // get our current pos
      currPos = AliceStateHelper::getPositionRearAxle(vehState);
      currYaw = AliceStateHelper::getHeading(vehState);
      // get our final pos
      finState = cSpecs.getFinalState();
      finPos.set(finState[STATE_IDX_X], finState[STATE_IDX_Y]);
      finYaw = finState[STATE_IDX_THETA];
      angleDiff = Utils::getAngleInRange(currYaw - finYaw);

      Log::getStream(1) << "Special complete: delta dist = " <<finPos.dist(currPos) << " and delta yaw = " <<fabs(angleDiff) << endl;
      // Check distance and angle
      if ( (finPos.dist(currPos) < specialCompleteDist) &&
           (fabs(angleDiff) < 2*specialCompleteTheta) )
      {
        Console::addMessage("Completed parking maneuver.");
        return VP_PARKING_SPOT_FINISHED;
      } 
    }

    // We should not go there!
    break;
  }

  return PP_OK;
}

int Planner::loadPlannerParams(const char *filename) {
  FILE * file;

  if (!filename)
    return -1;
  
  // Check for file existance before opening 
  file = fopen(filename, "r");
  if (file == NULL)
    return -1;
  
  ConfigFile config(filename);
  config.readInto(this->m_updateGraphFunction,"updateGraphFunction");
  config.readInto(this->m_resetROAStatusTimeout,"resetROAStatusTimeout");
  config.readInto(this->m_checkROAStatusTimeout,"checkROAStatusTimeout");
  config.readInto(this->m_disableROATimeout,"disableROATimeout");
  config.readInto(this->m_EvasionTimeout,"EvasionTimeout");
  fclose(file);

  return 0;
}

Err_t Planner::evaluatePathForCollisions(StateProblem_t* stateProblem, VehicleState vehState)
{
  Err_t status = PP_OK;
  
  bool collide = false;
  
  // calculate the distance that we want to stop at for obstacles, with a min of 30m in front of Alice
  double distToStopObs = MAX(pow(AliceStateHelper::getVelocityMag(vehState),2)/(2*0.5)+VEHICLE_LENGTH, 30 + VEHICLE_LENGTH);

  // calculate distance to closest obstacle on the path
  // if there is an obstacle, then use that distance, otherwise just check for some distancne ahead
  // double distToObsOnPath = MAX(PathUtils::distanceToObsOnPath(&m_graph, &m_path),30.0);
  
  // step along the path and decide how close (laterally) we are to obs to decide 
  double minLatDist = MAX_DOUBLE;
  double tmpDist = 0.0;
  double bufferFront, bufferRear, bufferSide;

  /* TESTING
  // Set Alice's size based on the state problem
  switch (stateproblem->obstacle) {
    case OBSTACLE_AGGRESSIVE:
      // If obstacle safety is aggressive, then use 1m in front and .5m on the sides
      bufferFront = OBS_AGGR_FRONT;
      bufferRear = OBS_AGGR_REAR;
      bufferSide = MAX(OBS_AGGR_RIGHT, OBS_AGGR_LEFT);
      break;
    case OBSTACLE_BARE:
      // If obstacle safety is bare, then do not grow alice
      bufferFront = OBS_BARE_FRONT;
      bufferRear = OBS_BARE_REAR;
      bufferSide = MAX(OBS_BARE_RIGHT, OBS_BARE_LEFT);
      break;
    default:
      bufferFront = OBS_SAFE_FRONT;
      bufferRear = OBS_SAFE_REAR;
      bufferSide = MAX(OBS_SAFE_RIGHT, OBS_SAFE_LEFT);
      break;
  }
  */

  //TESTING -  NEED TO TAKE THIS OUT WHEN OBSFRONTDIST AND OBSREARDIST ARE POPULATED
  bufferFront = OBS_DEFAULT;
  bufferRear = OBS_DEFAULT;
  bufferSide = OBS_DEFAULT;

  double rearFrontIgnore = DIST_REAR_AXLE_TO_FRONT - DIST_FRONT_AXLE_TO_FRONT;
  // if we are starting with a collision we want to step through and ignore those nodes until we clear the obs
  int i=0;
  if ( m_graph.isObsCollision(m_path.nodes[0])  && m_graph.isStatusFresh(m_path.nodes[0]->status.obsDistTime) ) {
    //    double maxIgnoreDist;

    if (m_path.directions[0] == PLAN_GRAPH_PATH_REV) {
      // if there is something behind us and not next to us
      if ( (m_path.nodes[0]->status.obsSideDist <= 0.2)  && 
           ( m_path.nodes[0]->status.obsRearDist <= bufferRear || 
             m_path.nodes[0]->status.obsFrontDist < -rearFrontIgnore ) ) {// MAGIC
        Log::getStream(1) << "COLLIDING ON THE VEHICLE NODE, BUT CANNOT REVERSE OUT" << endl;
        Log::getStream(1) << "SideDist = " << m_path.nodes[0]->status.obsSideDist << " Rear dist = " << m_path.nodes[0]->status.obsRearDist << endl;
        Console::addMessage("COLLIDING ON THE VEHICLE NODE, BUT CANNOT REVERSE OUT");
        Console::addMessage("SideDist = %3.2f and RearDist = %3.2f",m_path.nodes[0]->status.obsSideDist, m_path.nodes[0]->status.obsRearDist);
      } else {
        Log::getStream(1) << "COLLIDING ON THE VEHICLE OBS, BUT REVERSING OUT" << endl;
        Console::addMessage("COLLIDING ON THE VEHICLE OBS, BUT REVERSING OUT");

        //        maxIgnoreDist = DIST_REAR_AXLE_TO_FRONT + bufferFront;
        
        for (i = 0; i<m_path.pathLen; i++) { 
          
          //          if (tmpDist >= maxIgnoreDist) { 
            // there must be an obstacle in front and behind alice, so reset the params
            // TODO: figure out what to do here
          //            i =0;
          //            tmpDist = 0.0;
          //            break;
          //          }
          
          // step out when the obstacle status of the node changes (i.e., the status is not fresh anymore
          if ( !m_graph.isObsCollision(m_path.nodes[i]) )
            break;
          
          // update the distance that we have stepped along this path
          if (i != 0)
            tmpDist += vec2f_mag(vec2f_sub(m_path.nodes[i]->pose.pos, m_path.nodes[i-1]->pose.pos));
        }
      }
      
    } else {
      // if there is something behind us and not next to us
      if ( (m_path.nodes[0]->status.obsSideDist < 0.2)  &&
           (m_path.nodes[0]->status.obsFrontDist > -rearFrontIgnore) ) {// MAGIC
        Log::getStream(1) << "COLLIDING ON THE VEHICLE NODE, BUT CANNOT DRIVE OUT" << endl;
        Log::getStream(1) << "SideDist = " << m_path.nodes[0]->status.obsSideDist << " Rear dist = " << m_path.nodes[0]->status.obsFrontDist << endl;
        Console::addMessage("COLLIDING ON THE VEHICLE NODE, BUT CANNOT DRIVE OUT");
        Console::addMessage("SideDist = %3.2f and FrontDist = %3.2f",m_path.nodes[0]->status.obsSideDist, m_path.nodes[0]->status.obsFrontDist);

      } else {
        Log::getStream(1) << "COLLIDING ON THE VEHICLE NODE, BUT DRIVING OUT" << endl;
        Console::addMessage("COLLIDING ON THE VEHICLE NODE, BUT DRIVING OUT");
        
        //        maxIgnoreDist = DIST_REAR_TO_REAR_AXLE + bufferRear;
        
        for (i = 0; i<m_path.pathLen; i++) { 
          
          //          if (tmpDist >= maxIgnoreDist) { 
            // there must be an obstacle in front and behind alice, so reset the params
            // TODO: figure out what to do here
          //            i =0;
          //            tmpDist = 0.0;
          //            break;
          //          }
          
          // step out when the obstacle status of the node changes (i.e., the status is not fresh anymore
          if ( !m_graph.isObsCollision(m_path.nodes[i]) )
            break;
          
          // update the distance that we have stepped along this path
          if (i != 0)
            tmpDist += vec2f_mag(vec2f_sub(m_path.nodes[i]->pose.pos, m_path.nodes[i-1]->pose.pos));
        }
      }
    }
  }

  if (tmpDist > 0.0) {
    Utils::setIgnoreMode(true);
    Console::addMessage("IGNORING OBSTACLE THAT WE ARE ON in %d direction", m_path.directions[0]);
    Log::getStream(1) << "IGNORING OBSTACLE THAT WE ARE ON in " << m_path.directions[0]  << " direction" << endl;
  } else
    Utils::setIgnoreMode(false);
  
  // get the minimum lateral distance along the path
  for (; i<m_path.pathLen; i++) {
    // update the distance that we have stepped along this path
    if (i != 0)
      tmpDist += vec2f_mag(vec2f_sub(m_path.nodes[i]->pose.pos, m_path.nodes[i-1]->pose.pos));
    
    // if this distance is greater that what we are interested in then step out of loop
    if (tmpDist > distToStopObs)
      break;

    // remember if there is a collision on the path
    if ( m_graph.isObsCollision(m_path.nodes[i]))
      collide = true;

    // if this node collides with an obstacle, then set the lateral distance to zero and step out
    if (m_path.nodes[i]->status.obsSideDist < minLatDist && m_graph.isStatusFresh(m_path.nodes[i]->status.obsDistTime)) {
      minLatDist = MAX(m_path.nodes[i]->status.obsSideDist,0.0);
    }
  }

  //Log::getStream(7) << "minLatDist = " << minLatDist << " safe " << OBS_SAFE_LEFT << " aggr " << OBS_AGGR_LEFT << " bare " << OBS_BARE_LEFT << endl;

  /* TESTING
  switch (stateproblem->obstacle) {
  case OBSTACLE_AGGRESSIVE:
  if (collide) {
              status |= PP_COLLISION_AGGRESSIVE;
              status |= PP_COLLISION_SAFETY;
              if (minLatDist<OBS_BARE_LEFT)
                status |= PP_COLLISION_BARE;
            } else {
              if (minLatDist<OBS_SAFE_LEFT)
                status |= PP_COLLISION_SAFETY;
            }
      
            break;
    case OBSTACLE_BARE:
            if (collide) {
              status |= PP_COLLISION_SAFETY;
              status |= PP_COLLISION_AGGRESSIVE;
              status |= PP_COLLISION_BARE;
            } else {
              if (minLatDist<OBS_AGGR_LEFT)
                status |= PP_COLLISION_AGGRESSIVE;
              if (minLatDist<OBS_SAFE_LEFT)
                status |= PP_COLLISION_SAFETY;
            }
            break;
    default:
      if (collide) {
        status |= PP_COLLISION_SAFETY;
        if (minLatDist<OBS_BARE_LEFT)
          status |= PP_COLLISION_BARE;
        if (minLatDist<OBS_AGGR_LEFT)
          status |= PP_COLLISION_AGGRESSIVE;
      }
      break;
  }
  */

  // TESTING
  if ( (collide) || (minLatDist<bufferSide) ) {
    status |= PP_COLLISION_SAFETY;
    status |= PP_COLLISION_BARE;
    status |= PP_COLLISION_AGGRESSIVE;
  }


  //  Console::addMessage("Collision status: SAFE %d AGGR %d BARE %d",status & PP_COLLISION_SAFETY, 
  //                      status & PP_COLLISION_AGGRESSIVE, status & PP_COLLISION_BARE);
  Log::getStream(5) << "collision status: SAFE " << (int)(status & PP_COLLISION_SAFETY) 
                    << " AGGR " << (int)(status & PP_COLLISION_AGGRESSIVE) << " BARE " 
                    << (int)(status & PP_COLLISION_BARE) << endl;;

  return status;

  }


float Planner::getMaxFollowerError()
{
  // step along the path and decide how close (laterally) we are to obs to decide 
  double minLatDist = MAX_DOUBLE;
  double tmpDist = 0.0;
  double distToCheck = 10.0; // MAGIC - how far do we want to check along the path
  

  for (int i=0; i<m_path.pathLen; i++) {
    // update the distance that we have stepped along this path
    if (i != 0)
      tmpDist += vec2f_mag(vec2f_sub(m_path.nodes[i]->pose.pos, m_path.nodes[i-1]->pose.pos));
    
    // if this distance is greater that what we are interested in then step out of loop
    if (tmpDist > distToCheck)
      break;

    // if the status of this node is not fresh then move on to next node
    if ( !(m_graph.isStatusFresh(m_path.nodes[i]->status.obsDistTime)) )
      continue;
      
    // if this node collides with an obstacle, then set the lateral distance to zero and step out
    if (m_path.nodes[i]->status.obsSideDist < minLatDist)
      minLatDist = m_path.nodes[i]->status.obsSideDist;
    //Console::addMessage("obsSideDist = %3.2f", m_path.nodes[i]->status.obsSideDist);
    Log::getStream(7) << "obsSideDist = " << m_path.nodes[i]->status.obsSideDist << endl;;

    // if this node collides with an obstacle, then set the lateral distance to zero and step out
    // TESTING
    //    if (m_path.nodes[i]->status.carSideDist < minLatDist)
    //      minLatDist = m_path.nodes[i]->status.carSideDist;

  }

  //  Console::addMessage("minLatDist = %3.2f", minLatDist);
  Log::getStream(7) << "minLatDist = " << minLatDist << endl;;
  
  if (minLatDist<OBS_SAFE_LEFT)
    return 0.2; // MAGIC -  the minimum error we can expect
  
  return 0.2 + MIN(0.8, minLatDist-OBS_SAFE_LEFT); // the biggest error we want to have is 1m

}

bool Planner::isEvasionCompleted()
{
  // Get our current pos
  VehicleState vehState = m_traffStateEst->getVehState();
  point2 currPos = AliceStateHelper::getPositionRearAxle(vehState);
  point2 endPos = point2(m_evasiveEnd.pos.x, m_evasiveEnd.pos.y);
  double currYaw = AliceStateHelper::getHeading(vehState);
  double angleDiff = Utils::getAngleInRange(currYaw - m_evasiveEnd.rot);
  double currVel = AliceStateHelper::getVelocityMag(vehState);

  if (currPos.dist(endPos) < 2.0 && fabs(angleDiff) < M_PI/6.0 && currVel < 0.2) {
    return true;
  }
  return false;
}

