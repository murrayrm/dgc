/*!
 * \file TrafficStateEst.hh
 * \brief Some legacy code from the tplanner. Maintains the local map and the vehicle state. Needs to be incorporated into the planner.
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef TRAFFICSTATEEST_HH_
#define TRAFFICSTATEEST_HH_

#include <vector>
#include <deque>
#include <map/Map.hh>
#include <interfaces/VehicleState.h>
#include <skynettalker/StateClient.hh>
#include <map/MapElementTalker.hh>
#include <interfaces/LeadingVehicleInfo.hh>

class TrafficStateEst : public CStateClient {

protected: 

  /*! The constructors are protected since we want a singleton */
  TrafficStateEst(bool waitForStateFill);
  TrafficStateEst(const TrafficStateEst&);
  ~TrafficStateEst();

public:

  /*! Get the singleton instance */
  static TrafficStateEst* Instance(bool waitForStateFill);

  /*! Free the singleton instance */
  static void Destroy();

  /*! You are not allowed to copy a singleton */
  TrafficStateEst& operator=(const TrafficStateEst&);

  /*! Get the current vehicle state */
  void updateVehState();

  /*! Get the vehicle state at the last estimate */
  VehicleState getVehState();

  /*! Get map at last estimate */
  Map* getMap(); 

  void updateMap();

  /*! Gets a local map update thread */
  void getLocalMapUpdate();

private : 

  /*! Get singleton instance */
  static TrafficStateEst* pinstance;

  /*! Get the local to global map update. */
  void getLocalGlobalMapUpdate();

  bool loadRNDF(string filename); 

  /*! The current vehicle state  */
  VehicleState m_currVehState;

  /*! The updated vehicle state  */
  VehicleState m_updatedVehState;

  /*! The map element talker to receive map updates*/
  CMapElementTalker m_mapElemTalker;

  /*! The map getting updated */
  Map* m_localUpdateMap;

  /*! The map to read  */
  Map* m_localMap;

  /*! The current map */
  pthread_mutex_t m_localMapUpMutex;

  /*! The global to local delta */
  point2 m_gloToLocalDelta;

};

#endif /*TRAFFICSTATEEST_HH_*/
