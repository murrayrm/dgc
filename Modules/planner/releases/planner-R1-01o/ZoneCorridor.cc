/**********************************************************
 **
 **  ZONECORRIDOR.CC
 **
 **    Author: Noel duToit
 **    Created: Thu Aug  2 17:23:25 2007
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "ZoneCorridor.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <skynettalker/SkynetTalker.hh>
//#include <temp-planner-interfaces/PlannerInterfaces.h>

#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0:  0)


Err_t ZoneCorridor::generateCorridor(CSpecs_t& cSpecs, point2arr zonePerimeter, Path_params_t params, pose3_t finPose, VehicleState vehState)
{
  // set up cspecs problem
  // starting state
  double startingState[4];
  startingState[0] = AliceStateHelper::getPositionRearAxle(vehState).x;
  startingState[1] = AliceStateHelper::getPositionRearAxle(vehState).y;
  startingState[2] = AliceStateHelper::getVelocityMag(vehState);
  startingState[3] = AliceStateHelper::getHeading(vehState);
  cSpecs.setStartingState(startingState);

  // starting controls
  double startingControls[2];
  startingControls[0] = AliceStateHelper::getAccelerationMag(vehState);
  startingControls[1] = 0;
  cSpecs.setStartingControls(startingControls);

  // final state
  double finalState[4];
  finalState[0] = finPose.pos.x;
  finalState[1] = finPose.pos.y;
  double roll, pitch, yaw;
  quat_to_rpy(finPose.rot, &roll, &pitch, &yaw);
  finalState[2] = 0;
  finalState[3] = yaw;
  cSpecs.setFinalState(finalState);

  // final controls
  double finalControls[2];
  finalControls[0] = 0;
  finalControls[1] = 0;
  cSpecs.setFinalControls(finalControls);

  // other parameters
  // TODO: add things like setting the max vel etc.
  //  cSpecs.icfc_prob.velMin = params.velMin;
  //  cSpecs.icfc_prob.velMax = params.velMax;
 
  // set the corridor
  cSpecs.setBoundingPolytope(zonePerimeter.arr);

  // define the cost map
  BitmapParams bmParams;
  PolygonParams polygonParams;
  bmParams.resX = 0.1;
  bmParams.resY = 0.1;
  bmParams.width =800;
  bmParams.height = 800;
  bmParams.baseVal = 100 ;
  bmParams.outOfBounds = 200;
  polygonParams.centerlaneVal = 1;
  polygonParams.obsCost = 1000;
  point2 initPos(startingState[0], startingState[1]);
  getBitmapParams(bmParams, polygonParams, initPos, zonePerimeter);
  cSpecs.setCostMap(bmParams);

  // some debug info ...
  if (1) {
    // print corridor
    //print(zonePerimeter, cSpecs);
    
    // display the corridor
    display(CmdArgs::sn_key, 10, zonePerimeter, cSpecs);
  }

  return 0;
}

void ZoneCorridor::convertZoneToPolygon(vector<Polygon>& polygons, point2arr& zonePerimeter, float val)
{
  val = sqrt(val);

  vector<float> cost;
  point2arr_uncertain vertices(zonePerimeter);
  for (unsigned int i=0; i<zonePerimeter.size(); i++) {
    cost.push_back(val);
  }
  
  Polygon polygon;
  polygon.setVertices(vertices, cost);
  polygon.setFillFunc(FILL_SQUARE);
  polygon.setCombFunc(COMB_REPLACE);

  polygons.push_back(polygon);
}

void ZoneCorridor::getBitmapParams(BitmapParams& bmparams, PolygonParams &polygonParams,
                     point2 alicePos, point2arr zonePerimeter)
{
  bmparams.centerX = alicePos.x;
  bmparams.centerY = alicePos.y;

  bmparams.polygons.clear();
  convertZoneToPolygon(bmparams.polygons, zonePerimeter,polygonParams.centerlaneVal);
}


void ZoneCorridor::display(int sn_key, int sendSubgroup, point2arr zonePerimeter, CSpecs_t cSpecs)
{
  CMapElementTalker meTalker;
  meTalker.initSendMapElement(sn_key);
  int counter=12000;
  point2 point;
  MapId mapId;
  MapElement me;
  
  mapId = counter;

  // display the corridor
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_BLUE, 100);
  me.setGeometry(zonePerimeter);
  meTalker.sendMapElement(&me,sendSubgroup);
  
  // print the initial and final cond's
  double* startingState = cSpecs.getStartingState();
  vector<point2> points;
  point.set(startingState[0], startingState[1]);
  points.push_back(point);
  point.set(startingState[0]+cos(startingState[3]), startingState[1]+sin(startingState[3]));
  points.push_back(point);
  mapId = 12001;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_YELLOW, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

  // print the initial and final cond's
  double* finalState = cSpecs.getFinalState();
  point.set(finalState[0], finalState[1]);
  points.push_back(point);
  point.set(finalState[0]+cos(finalState[3]), finalState[1]+sin(finalState[3]));
  points.push_back(point);
  mapId = 12002;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_RED, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();

}

void ZoneCorridor::print(point2arr zonePerimeter, CSpecs_t cSpecs)
{
  double* startingState = cSpecs.getStartingState();
  double* finalState = cSpecs.getFinalState();
  double* startingControls = cSpecs.getStartingControls();
  double* finalControls = cSpecs.getFinalControls();
  Console::addMessage("Init state = (%6.2f,%6.2f,%6.2f,%6.2f)", startingState[0], startingState[1], startingState[2], startingState[3]);
  Console::addMessage("Init control = (%6.2f,%6.2f)", startingControls[0], startingControls[1]);
  Console::addMessage("Fin state = (%6.2f,%6.2f,%6.2f,%6.2f)", finalState[0], finalState[1], finalState[2], finalState[3]);
  Console::addMessage("Fin control = (%6.2f,%6.2f)", finalControls[0], finalControls[1]);
}


