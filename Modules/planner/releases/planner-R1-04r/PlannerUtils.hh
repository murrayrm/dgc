/*!
 * \file PlannerUtils.hh
 * \brief Header for some utility functions for the planner module
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef PLANNERUTILS_HH_
#define PLANNERUTILS_HH_

// REMOVE #include <frames/point2.hh>
// REMOVE #include <map/Map.hh>
// REMOVE #include <temp-planner-interfaces/AliceStateHelper.hh>
#include <interfaces/VehicleState.h>
#include <temp-planner-interfaces/PlanGraphPath.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
// REMOVE #include <cspecs/CSpecs.hh>

class PlannerUtils {

public: 
  static int addAngles(double &angle_out, double angle_in1, double angle_in2);
  static double getAngleInRange(double angle);
  static bool isStopped(VehicleState &vehState);
  static int getLaneSide(Map* localMap, VehicleState vehState, LaneLabel currLane, LaneLabel desiredLane);
  static double getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &current_lane);
  static Err_t extractSubPath(PlanGraphPath &subPath, PlanGraphPath &path, VehicleState vehState, int estop = 0);
  static Err_t extractS1SubPath(PlanGraphPath &subPath, PlanGraphPath &path, VehicleState vehState, int estop = 0);
 
  //  static void printErrorInBinary(Err_t error);
  //  static void printCSpecs(CSpecs_t* cSpecs);

private :

};

#endif /*PLANNERUTILS_HH_*/




