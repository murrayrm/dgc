/*!
 * \file PlannerMain.cc
 * \brief Main planner execution entry point
 *
 * \author Noel duToit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */
   
#include <getopt.h>
#include <iostream>
#include "Planner.hh"
#include "dgcutils/DGCutils.hh"

#include "temp-planner-interfaces/planner_cmdline.h"
#include "temp-planner-interfaces/Console.hh"
#include "temp-planner-interfaces/CmdArgs.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "PlannerArgs.hh"

using namespace std;                 
  
/**
 * @brief Entry point of the planner
 *
 * This function populates the CmdArgs static class
 * and starts the planner
 */
int main(int argc, char **argv)              
{  
  struct planner_options options;
  uint64_t updateTime;

  /* Parse command-line arguments */
  if (planner_cmdline(argc, argv, &options) != 0)
    exit (1);

  // Suck out the options and place them in CmdArgs.  Should be
  // deprecated.
  plannerArgs(argc, argv, &options);

  /* Display that console is not enabled */
  if (!CmdArgs::console){
    cout << "No display" << endl;
    cout << "Constructing skynet with KEY = " << CmdArgs::sn_key << endl;
    cout << "debug = " << CmdArgs::debug << endl;
    cout << "verbose level = " << CmdArgs::verbose_level << endl;
  }

  /* Initializes Planner and start the gcmodule */
  Planner* planner = new Planner(&options); 
  sleep(1);

  /// Startup the portHandler thread
  planner->GcSimplePortHandlerThread::ReStart();

  // Run the arbitration and control cycle
  while (!planner->IsStopped()) // TODO: this needs to pick a quit flag from somewhere
  {
    planner->arbitrate(&planner->m_controlStatus, &planner->m_mergedDirective);
    planner->control(&planner->m_controlStatus, &planner->m_mergedDirective);

    // If we are running too fast, give up our time slice.
    if (DGCgettime() - updateTime < (uint64_t) (1e6/options.rate_arg))
      usleep(100000);
    updateTime = DGCgettime();
  }
  
  return 0;
}

