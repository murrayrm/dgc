/*!
 * \file PlannerUtils.cc
 * \brief Source code for some utility functions for the planner module
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include "PlannerUtils.hh"
#include <alice/AliceConstants.h>
#include <math.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Utils.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>


#define ZONE_INTERNAL_COMPLETE     (0.4)
#define ZONE_INTERNAL_STOPPED      (1.0)

/**
 * @brief Adds two angles and adjust the sum according to the PI/-PI convention
 */
int PlannerUtils::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<=-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}

/**
 * @brief Returns an -PI, PI bounded angle\
 */
double PlannerUtils::getAngleInRange(double angle)
{
  double angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}

/**
 * @brief Checks whether the vehicle is stopped
 */
bool PlannerUtils::isStopped(VehicleState &vehState) {
  return (AliceStateHelper::getVelocityMag(vehState) < 0.2);
}

int PlannerUtils::getLaneSide(Map* localMap, VehicleState vehState, LaneLabel currLane, LaneLabel desiredLane) {
  bool isReverse = false;
  point2 tmp_pt;

  if (currLane == LaneLabel(0,0) || desiredLane == LaneLabel(0,0) || currLane == desiredLane) {
    return 0; 
  }

  double currlane_angle;
  double alice_angle = AliceStateHelper::getHeading(vehState);
  point2 alice_rearaxle = AliceStateHelper::getPositionRearAxle(vehState);
  localMap->getHeading(currlane_angle, tmp_pt, currLane, alice_rearaxle);
  double diff_angle = fabs(getAngleInRange(alice_angle-currlane_angle));
  isReverse = (diff_angle > M_PI/2);

  LaneLabel lane;
  localMap->getNeighborLane(lane,currLane,-1);

  if (lane == desiredLane) {
    if (isReverse)
      return 1;
    return -1;
  } 

  localMap->getNeighborLane(lane,currLane,1);

  if (lane == desiredLane) {
    if (isReverse)
      return -1;
    return 1;
  } 

  return 0;
}

double PlannerUtils::getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &current_lane)
{
  // Set position
  point2 currFrontPos;
  currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  // Get all stoplines on that lane
  vector<PointLabel> stoplines;
  map->getLaneStopLines(stoplines, current_lane);

  // Get center line
  point2arr centerline;
  map->getLaneCenterLine(centerline, current_lane);

  // Find the closest stopline on the lane on front of use
  point2 stop_position;
  double dist, min_dist = -1;
  for (unsigned int i=0; i<stoplines.size(); i++) {
    map->getWaypoint(stop_position, stoplines[i]);
    map->getDistAlongLine(dist, centerline, stop_position, currFrontPos);
    if (dist >= 0.0 && (dist < min_dist || min_dist == -1)) {
      min_dist = dist;
    }
  }

  return min_dist;
}

Err_t PlannerUtils::extractSubPath(PlanGraphPath &subPath, PlanGraphPath &path, VehicleState vehState, int estop){
  // Temporary location of distance threshold
  PlanGraphPathDirection currPathDir;
  int i, counter;
  double stoppedDuration = Utils::monitorAliceSpeed(vehState, estop);
  
  // Assign current direction based on path
  if (path.nodes[0] !=  NULL) {
    currPathDir = path.directions[0];
  } else {
    subPath.nodes[0] = NULL;
    return P_EXTRACT_SUBPATH_ERROR;
  }

  // Calculate the distance along path until we get a change in direction
  // Calculate the distance along the path from Alice's projection onto the path 
  // to the point that indicates a change in direction

  /* Get Alice current position and project it onto the path */
  point2 currVehPos, tmpPt;
  point2arr pointsPath; 
  double yaw = AliceStateHelper::getHeading(vehState);
  double distFrontToPathEnd = 0.0;
  double distFrontToDirChange = 0.0;
  double distAliceToDirChange = 0.0;
  double distAliceToPathEnd = 0.0;
  double distFrontToAliceOnPath = 0.0;

  /* Populate a point2 arr to use the project_along method */
  for (int j = 0; j < path.pathLen; j++) {
    pointsPath.push_back(point2(path.nodes[j]->pose.pos.x, path.nodes[j]->pose.pos.y));
  }

  currVehPos.set(AliceStateHelper::getPositionRearBumper(vehState));
  distFrontToPathEnd =  pointsPath.linelength();
  // Set the dist to dir change equal to the length for the case where the whole path is the same dir
  distFrontToDirChange = pointsPath.linelength();
  // Update the dist to the dir change if there is one.
  for (i=0; i<path.pathLen; i++) {
    if (path.directions[i] != currPathDir) {
      if (Utils::distToProjectedPoint(tmpPt, distFrontToDirChange, pointsPath.arr[i], yaw, pointsPath, 1, 5) != 0) {
        Log::getStream(3) << "PlannerUtils.extractSubPath: error in getting distF2DC" << endl;
        return P_EXTRACT_SUBPATH_ERROR;
      }
      break;
    }
  }
  if (Utils::distToProjectedPoint(tmpPt, distFrontToAliceOnPath, currVehPos, yaw, pointsPath, 1, 5) != 0) {
    Log::getStream(3) << "PlannerUtils.extractSubPath: error in getting distF2AOP " << currPathDir << endl;
    return P_EXTRACT_SUBPATH_ERROR;
  }
   
  distAliceToDirChange = distFrontToDirChange - distFrontToAliceOnPath;
  distAliceToPathEnd = distFrontToPathEnd - distFrontToAliceOnPath;

  // If the path in this direction is less than the "complete dist threshold" 
  // and some other conditions are met, then completed a stage, so we have to
  // remove the first points from the path that are still in our current direction
  // and switch the direction of the path
  if ( (distAliceToDirChange < ZONE_INTERNAL_COMPLETE) && 
       (distAliceToPathEnd >= ZONE_INTERNAL_COMPLETE) && 
       (stoppedDuration>ZONE_INTERNAL_STOPPED) ) {

    Log::getStream(6) << "PlannerUtils.extractSubPath: End of a stage of direction = " << currPathDir << endl;
    Log::getStream(6) << "PlannerUtils.extractSubPath: Path length  = " << path.pathLen << " and first node with diff direction = " << i << endl;
    Log::getStream(6) << "PlannerUtils.extractSubPath: distF2DC = " << distFrontToDirChange << " distF2EP = " << distFrontToPathEnd <<" distF2AOP = " << distFrontToAliceOnPath <<" distA2DC = " << distAliceToDirChange <<" distA2PE = " << distAliceToPathEnd << " stage complete threshold = " << ZONE_INTERNAL_COMPLETE << endl;

    // step along the path and remove the initial nodes that are in the wrong direction
    PlanGraphPath tmpPath;
    for (i=0; i<path.pathLen;i++) {
      if (path.directions[i] != currPathDir)
        break;
    }
    counter = 0;
    for (; i<path.pathLen; i++) {
      tmpPath.nodes[counter] = path.nodes[i];
      tmpPath.directions[counter] = path.directions[i];
      counter++;
    }
    tmpPath.pathLen = counter;
    for (i=0;i<tmpPath.pathLen; i++) {
      path.nodes[i] = tmpPath.nodes[i];
      path.directions[counter] = tmpPath.directions[i];
    }
    path.pathLen = tmpPath.pathLen;
    
    // switch direction
    if (currPathDir == PLAN_GRAPH_PATH_FWD)
      currPathDir = PLAN_GRAPH_PATH_REV;
    else
      currPathDir = PLAN_GRAPH_PATH_FWD;
  }

  counter = 0;
  // Should now have a path with a longer segment in the correct direction
  // populate the subPath
  for (int i=0; i<path.pathLen; i++) {
    if (path.directions[i] == currPathDir) {
      subPath.nodes[counter] = path.nodes[i];
      subPath.directions[counter] = path.directions[i];
      counter++;
    }
    else
      break;
  }
  subPath.pathLen = counter;
  
  return PLANNER_OK;
}

// s1planner handles switching directions when needed, so this function can (and should) be much simpler
Err_t PlannerUtils::extractS1SubPath(PlanGraphPath &subPath, PlanGraphPath &path, VehicleState vehState, int estop){
  int counter = 0;
  // Should now have a path with a longer segment in the correct direction
  // populate the subPath
  PlanGraphPathDirection currPathDir = path.directions[0];
  
    while (path.directions[counter] == currPathDir && counter < path.pathLen ) {
      subPath.nodes[counter] = path.nodes[counter];
      subPath.directions[counter] = path.directions[counter];
      counter++;
    }
  subPath.pathLen = counter;
  
  return PLANNER_OK;
}


/*
  void PlannerUtils::printErrorInBinary(Err_t error)
  {
  //  int bit;
  Log::getStream(1) << "error = " << error;
  Log::getStream(1) << " and in Binary (reversed!!) = ";
  
  int i = 0;
  while ((int)error >= pow(2,i)) {
  //    bit = (((int)error >> i) & 1);((int)error >> i) & 1;
  Log::getStream(1) << (((int)error >> i) & 1);
  i++;
  }
  Log::getStream(1) << endl;

  return;

  }

  void PlannerUtils::printCSpecs(CSpecs_t* cSpecs)
  {
  Log::getStream(1) << "cSpecs: " << endl;
  Log::getStream(1) << "Initial state = " << cSpecs->getStartingState() << endl;
  Log::getStream(1) << "Initial controls = " << cSpecs->getStartingControls() << endl;
  Log::getStream(1) << "Final state = " << cSpecs->getFinalState() << endl;
  Log::getStream(1) << "Final controls = " << cSpecs->getFinalControls() << endl;
  Log::getStream(1) << "Perimeter = " << cSpecs->getBoundingPolygon() << endl;

  }
*/
