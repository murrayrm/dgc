/*!
 * \file TrafficStateEst.hh
 * \brief Some legacy code from the tplanner. Maintains the local map and the vehicle state. Needs to be incorporated into the planner.
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef TRAFFICSTATEEST_HH_
#define TRAFFICSTATEEST_HH_

#include <vector>
#include <deque>
#include <map/Map.hh>
#include <mapper/Mapper.hh>
#include <interfaces/VehicleState.h>
#include <skynettalker/StateClient.hh>
#include <map/MapElementTalker.hh>
#include <temp-planner-interfaces/PlanGraph.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <fused-perceptor/FusedPerceptor.hh>


class TrafficStateEst : public CStateClient
{
  protected: 

  /*! The constructors are protected since we want a singleton */
  TrafficStateEst(bool waitForStateFill);
  TrafficStateEst(const TrafficStateEst&);
  ~TrafficStateEst();

  public:

  /*! Get the singleton instance */
  static TrafficStateEst* Instance(bool waitForStateFill);

  /*! Free the singleton instance */
  static void Destroy();

  /*! You are not allowed to copy a singleton */
  TrafficStateEst& operator=(const TrafficStateEst&);

  private : 

  /*! Get singleton instance */
  static TrafficStateEst* pinstance;
  
  public:

  /*! Update the current vehicle/actuator state. */
  void updateState();

  /*! Get the vehicle state at the last estimate. */
  VehicleState getVehState();

  /*! Get the actuator state at the last estimate. */
  ActuatorState getActState();

  private:
  
  /*! The current vehicle state  */
  VehicleState m_currVehState;

  /*! The current actuator state */
  ActuatorState m_currActState;

  public:
  
  /*! Load the RNDF into the map. */
  bool loadRNDF(string filename);

  /*! Initialize the mapper and start the thread */
  int startMapper();

  /*! Stop the mapper thread and clean up */
  int stopMapper();
    
  /*! Update the map copy */
  void updateMap();

  /*! Get map at last estimate */
  Map* getMap(); 
  
  /*! Lock the map while we do some processing. This is currently a NOP,
    but left in just in case we need to use it in the future. */
  void freezeMap();

  /*! Unlock the map. */
  void meltMap();

  private:
  
  /*! Mapper thread callback wrapper. */
  static int mapperThreadFn(TrafficStateEst *self);

  /*! Gets a local map (update thread) */
  void runMapper();

  private:

  /*! Mapper thread */
  pthread_t m_mapperThread;

  /*! Mutex for interaction wuth the mapper thread */
  pthread_mutex_t m_mapperMutex;

  /*! The map element talker to receive map updates. */
  CMapElementTalker m_mapElemTalker;

  /*! The mapper object, updated in the mapper thread.  */
  Mapper* m_mapper;

  /*! The map to use in other threads (a copy of the one in mapper). */
  Map* m_map;

  /*! used for mapper internal
    0 - no copy, 1 - copy requested */
  int m_mapCopyState;
  
  public:
  
  /**
   * @brief This function modifies the graph according to the map and the current state problem
   *
   * Obstacles and cars in the map are not treated in the same way depending on the current
   * state problem. The role of this function is to update the graph accordingly.
   */
  int updateGraphStateProblem(PlanGraph *graph, StateProblem_t &problem);  

  /** Function for evaluating node status (lazy evaluation in the planner) */
  static int updateNodeStatus(TrafficStateEst *self, PlanGraph *graph, PlanGraphNode *node);
  
  /**
   * @brief This function modifies the graph according to the map
   *
   * Distance to stoplines are updated according to the sensed stoplines.
   */
  //  int updateGraphStoplines(PlanGraph *graph);

  /**
   * @brief This function modifies the stop nodes on the path according to the map
   */
  //  int updatePathStoplines(PlanGraphPath *path);
  
  public:

  /// Initialize the fused perceptor; starts an internal thread.
  int startFused(const char *spreadDaemon, int skynetKey);

  /// Finalize the fused perceptor; stops the internal thread.
  int stopFused();

  /// Update the graph with data from the fused perceptor
  int updateFusedGraph(PlanGraph *graph, const VehicleState *state);

  /// Lock the fused perceptor and return a pointer
  FusedPerceptor *lockFused();

  /// Unlock the fused perceptor and return a null pointer
  FusedPerceptor *unlockFused();

  private:
  
  /// Thread function for fused perceptor
  static int mainFused(TrafficStateEst *self);

  private:

  // Fused perceptor
  FusedPerceptor *fused;
  
  // Thread control
  pthread_t fusedThread;
  pthread_mutex_t fusedMutex;

public:

  // Update the graph status
  int updateStatus(PlanGraph *graph, StateProblem_t &problem);

  // Size of Alice for c-space tests
  float alice_front, alice_rear, alice_left, alice_right, alice_diam;

  private:

  // Get the bounding box for the obstacle (site frame)
  void getSiteBox(vec2f_t min, vec2f_t max, float *ox, float *oy, float *sx, float *sy);

  // Check for any nodes in the obstacle bounding box (site frame)
  bool checkBox(PlanGraphQuad *quad, float ox, float oy, float sx, float sy);

  // Update the status values along a line
  void updateLineStatus(vec2f_t pa, vec2f_t pb, PlanGraph *graph, bool obs, bool car);

  // Update the status values for intersecting nodes
  void updateQuadStatus(PlanGraphQuad *quad, PlanGraph *graph, float px, float py, bool obs, bool car);

};

#endif /*TRAFFICSTATEEST_HH_*/
