
/* 
 * Desc: Graph planner viewer utility
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <limits.h>
#include <float.h>

#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <FL/Fl.H>
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <dgcutils/Fl_Glv_Window.H>
//#include <jplv/jplv_image.h> // Add this to DGC utils

#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>
#include <trajutils/traj.hh>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <rndf/MDF.hh>

// Live mode
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <map/MapElementTalker.hh>
#include <trajutils/TrajTalker.hh>

#include "TurnSignalSender.hh"

#include <map/Map.hh>
#include "GraphPlanner.hh"
#include "ConfigSpace.hh"
#include "TrajPlanner.hh"

#include "cmdline.h"


class App
{
  public:

  // Default constructor
  App();

  // Destructor
  ~App();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  // Initialize GUI
  int initGUI(int cols, int rows);

  // Finalize GUI
  int finiGUI();
 
  public:

  // Action callback
  static void onAction(Fl_Widget *w, int option);

  // View callback
  static void onView(Fl_Widget *w, int option);
  
  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  // Handle draw callbacks
  static void onDraw(Fl_Glv_Window *win, App *self);

  // Handle idle callbacks
  static void onIdle(App *self);

  public:

  // Switch to the vehicle frame
  void pushFrameVehicle(const VehicleState *state);

  // Switch to the local frame
  void pushFrameLocal(const VehicleState *state);

  // Switch to the a given pose
  void pushFramePose(pose3_t pose);

  // Revert to previous frame
  void popFrame();
  
  // Switch to viewport projection.
  void pushViewport();

  // Revert to non-viewport projection
  void popViewport();

  // Draw a set of axes
  void drawAxes(float size);

  // Draw the robot
  void drawAlice(float steerAngle, int signalState);

  // Draw status info
  void drawStatus();

  // Draw the configuration space info for a node
  void drawNodeCSpace(GraphNode *node);

  // Predraw the map 
  void predrawMap();

  // Predraw the static graph
  void predrawStaticGraph();

  // Predraw the non-static graph data
  void predrawGraph();

  // Predraw the lane boundaries 
  void predrawStaticLanes();

  // Predraw the non-static lane data
  void predrawLanes();

  // Draw the path to the goal
  void predrawPath();
  
  // Draw the trajectory
  void predrawTraj();

  // Draw the trajectory speed profile (viewport frame)
  void predrawSpeed();

  // Draw the path history
  void predrawHist();

  public:

  // Initialize live mode
  int initLive();

  // Finalize live mode
  int finiLive();

  // Initialize the algorithms
  int init();

  // Finalize the algorithms
  int fini();

  // Update the algorithms
  int update();
  
  public:

  // Command-line options
  struct gengetopt_args_info options;

  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;
  
  // 3D window
  Fl_Glv_Window *worldwin;

  // Should we pause?
  bool pause;

  // Should we advance a single step?
  bool step;
  
  // Should we quit?
  bool quit;

  // Should we capture a single frame?
  bool captureFrame;

  // Should we capture a movie?
  bool captureMovie;
  
  // Screen capture counter
  int capCount;

  // Display options
  bool viewChanges, viewTurns, viewVolatile, viewLanes, viewMap, viewCSpace;
  bool viewPath, viewTraj, viewHist;

  public:
  
  // Operating mode: internal simulation or live
  enum {modeSim, modeLive} mode;

  // Mission file
  MDF mdf;

  // Current state data
  VehicleState vehState;

  // Current actuator data
  ActuatorState actState;

  // Global map
  Map map;

  // Global graph
  Graph *graph;
  
  // Global planner
  GraphPlanner *planner;

  // Configuration space predictor
  ConfigSpace *configSpace;
  
  // Current path 
  GraphPath path;

  // Trajectory planner
  TrajPlanner *trajPlanner;
  
  // Current trajectory
  CTraj *traj;

  // Number of updates
  int updateCount;
  
  // Time of last update
  uint64_t updateTime;

  // Elapsed processing time for last update
  uint64_t stateInterval, mapInterval, updateInterval;

  // Current goal index
  int goalIndex;

  // Current goal checkpoint id
  int goalId;

  // Last time we got a valid path
  uint64_t validPathTime;

  // Current turn signal state
  int signalState, lastSignalState;

  public:
  
  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Sensnet handle (for getting state)
  sensnet_t *sensnet;

  // Talkers for live mode
  CMapElementTalker *mapTalker;
  CTrajTalker *trajTalker;
  TurnSignalSender *turnSender;

  public:

  // Display offset (used to improve GL precision)
  vec3_t offset;

  // Robot pose history
  int numPoses, maxPoses;
  pose3_t *poses;
  
  // Display lists
  GLuint staticGraphList, graphList, staticLaneList, laneList, mapList;
  GLuint pathList, trajList, speedList, histList;

  // Is the display dirty?
  bool dirty;

  // Is the static graph display dirty?
  bool dirtyStatic;
};



// Commands
enum
{
  APP_ACTION_PAUSE = 0x1000,
  APP_ACTION_STEP,
  APP_ACTION_CAPTURE_FRAME,
  APP_ACTION_CAPTURE_MOVIE,
  APP_VIEW_CHANGES,
  APP_VIEW_TURNS,
  APP_VIEW_VOLATILE,
  APP_VIEW_LANES,
  APP_VIEW_MAP,
  APP_VIEW_CSPACE,
  APP_VIEW_PATH,
  APP_VIEW_TRAJ,
  APP_VIEW_HIST
};


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Common macros
#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))


// Default constructor
App::App()
{
  this->goalIndex = 0;
  this->goalId = 0;

  this->graph = NULL;
  this->planner = NULL;
  this->trajPlanner = NULL;
  
  memset(&this->path, 0, sizeof(this->path));
  memset(&this->vehState, 0, sizeof(this->vehState));
  memset(&this->actState, 0, sizeof(this->actState));

  this->updateCount = 0;
  this->updateTime = 0;
  this->signalState = 0;
  this->lastSignalState = 0;

  this->numPoses = this->maxPoses = 0;
  this->poses = NULL;
  
  this->staticGraphList = 0;
  this->graphList = 0;
  this->staticLaneList = 0;
  this->laneList = 0;
  this->pathList = 0;
  this->mapList = 0;
  this->speedList = 0;
  this->trajList = 0;
  this->histList = 0;

  cmdline_parser_init(&this->options);
  
  return;
}


// Destructor
App::~App()
{
  if (this->poses)
    free(this->poses);
  this->poses = NULL;
  cmdline_parser_free(&this->options);
  
  return;
}


// Parse command-line options
int App::parseCmdLine(int argc, char **argv)
{
  // Run parser
  if (cmdline_parser(argc, argv, &this->options) != 0)
  {
    cmdline_parser_print_help();
    return -1;
  }

  if (this->options.sim_flag)
    this->mode = modeSim;
  else
    this->mode = modeLive;

  if (this->mode == modeLive)
  {
    // Fill out the spread name
    if (this->options.spread_daemon_given)
      this->spreadDaemon = this->options.spread_daemon_arg;
    else if (getenv("SPREAD_DAEMON"))
      this->spreadDaemon = getenv("SPREAD_DAEMON");
    else
      return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
    // Fill out the skynet key
    if (this->options.skynet_key_given)
      this->skynetKey = this->options.skynet_key_arg;
    else if (getenv("SKYNET_KEY"))
      this->skynetKey = atoi(getenv("SKYNET_KEY"));
    else
      this->skynetKey = 0;
  }

  return 0;
}


// Initialize stuff
int App::initGUI(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) App::onExit},
      {0},
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) App::onAction,
       (void*) APP_ACTION_PAUSE, FL_MENU_TOGGLE},
      {"Single step", '.', (Fl_Callback*) App::onAction,
       (void*) APP_ACTION_STEP},
      {"Capture frame", 'c', (Fl_Callback*) App::onAction,
       (void*) APP_ACTION_CAPTURE_FRAME},
      {"Capture movie", 'm', (Fl_Callback*) App::onAction,
       (void*) APP_ACTION_CAPTURE_MOVIE, FL_MENU_TOGGLE},
      {0},
      {"&View", 0, 0, 0, FL_SUBMENU},    
      {"Changes", FL_CTRL + 'e', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_CHANGES, FL_MENU_TOGGLE},
      {"Volatile", FL_CTRL + 'v', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_VOLATILE, FL_MENU_TOGGLE},
      {"Lanes", FL_CTRL + 'l', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_LANES, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Map", FL_CTRL + 'm', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_MAP, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"CSpace", FL_CTRL + 'c', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_CSPACE, FL_MENU_TOGGLE},
      {"Path", FL_CTRL + 'p', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_PATH, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Trajectory", FL_CTRL + 't', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_TRAJ, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"History", FL_CTRL + 'h', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_HIST, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {0},
      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Graph Planner Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new Fl_Glv_Window(0, 30, cols, rows - 30, this, (Fl_Callback*) onDraw);

  this->mainwin->end();

  this->worldwin->set_hfov(40);
  this->worldwin->set_clip(2, 1000);
  this->worldwin->set_lookat(-1, 0, -75, 0, 0, 0, 0, 0, -1);

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  this->viewTurns = true;
  this->viewChanges = false;
  this->viewLanes = true;
  this->viewVolatile = false;
  this->viewMap = true;
  this->viewCSpace = false;
  this->viewPath = true;
  this->viewTraj = true;
  this->viewHist = true;

  // Dont set this unless we also set the menu state.
  this->pause = false;  
  this->step = true;
  this->dirty = true;
  this->dirtyStatic = true;

  this->captureFrame = false;
  this->captureMovie = false;
  
  return 0;
}


// Finalize stuff
int App::finiGUI()
{
  assert(this->mainwin);
  delete this->mainwin;
  this->mainwin = NULL;

  return 0;
}


// Handle menu callbacks
void App::onAction(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data();
  if (option == APP_ACTION_PAUSE)
    self->pause = !self->pause;
  if (option == APP_ACTION_STEP)
    self->step = true;
  if (option == APP_ACTION_CAPTURE_FRAME)
  {
    self->captureFrame = true;
    self->worldwin->redraw();
  }
  if (option == APP_ACTION_CAPTURE_MOVIE)
    self->captureMovie = true;
  
  return;
}


// Handle menu callbacks
void App::onView(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data();

  if (option == APP_VIEW_TURNS)
  {
    self->viewTurns = !self->viewTurns;
    self->dirtyStatic = true;
  }
  if (option == APP_VIEW_CHANGES)
  {
    self->viewChanges = !self->viewChanges;
    self->dirtyStatic = true;
  }
  if (option == APP_VIEW_LANES) 
    self->viewLanes = !self->viewLanes;
  
  if (option == APP_VIEW_VOLATILE)
    self->viewVolatile = !self->viewVolatile;
  if (option == APP_VIEW_MAP)
    self->viewMap = !self->viewMap;  
  if (option == APP_VIEW_CSPACE)
  {
    self->viewCSpace = !self->viewCSpace;
    self->dirtyStatic = true;
  }
  if (option == APP_VIEW_PATH)
    self->viewPath = !self->viewPath;
  if (option == APP_VIEW_TRAJ)
    self->viewTraj = !self->viewTraj;
  if (option == APP_VIEW_HIST)
    self->viewHist = !self->viewHist;

  self->dirty = true;
  self->worldwin->redraw();
  
  return;
}


// Handle menu callbacks
void App::onExit(Fl_Widget *w, int option)
{
  App *self;

  self = (App*) w->user_data();
  self->quit = true;

  return;
}


// Handle draw callbacks
void App::onDraw(Fl_Glv_Window *win, App *self)
{
  if (self->dirtyStatic)
  {
    self->predrawStaticGraph();
    self->predrawStaticLanes();
    self->dirtyStatic = false;
  }

  if (self->dirty)
  {
    if (self->viewLanes)
      self->predrawLanes();
    if (self->viewVolatile)
      self->predrawGraph();
    if (self->viewMap)
      self->predrawMap();
    if (self->viewPath)
      self->predrawPath();
    if (self->viewTraj)
      self->predrawTraj();
    if (true)
      self->predrawSpeed();
    if (self->viewHist)
      self->predrawHist();
    self->dirty = false;
  }
  
  glPushMatrix();
  glTranslatef(-self->vehState.utmNorthing + self->offset.x,
               -self->vehState.utmEasting + self->offset.y,
               -0 + self->offset.z);
  
  // Switch to vehicle frame and draw Alice
  self->pushFrameVehicle(&self->vehState);
  self->drawAxes(1.0);
  self->drawAlice(self->actState.m_steerpos * VEHICLE_MAX_AVG_STEER, self->signalState);
  self->popFrame();
  
  // Draw the map
  if (self->viewMap)
  {
    self->pushFrameLocal(&self->vehState);
    glCallList(self->mapList);
    self->popFrame();
  }

  // REMOVE
  /*
  // Draw bounding box on map obstacles
  if (true)
  {
    glColor3f(1, 1, 1);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin(GL_QUADS);
    glVertex2f(self->configSpace->mapelAx - self->offset.x,
               self->configSpace->mapelAy - self->offset.y);
    glVertex2f(self->configSpace->mapelBx - self->offset.x,
               self->configSpace->mapelAy - self->offset.y);
    glVertex2f(self->configSpace->mapelBx - self->offset.x,
               self->configSpace->mapelBy - self->offset.y);
    glVertex2f(self->configSpace->mapelAx - self->offset.x,
               self->configSpace->mapelBy - self->offset.y);
    glEnd();
  }
  */

  // Draw the static graph
  glCallList(self->staticGraphList);

  // Draw the lanes
  if (self->viewLanes)
  {
    glCallList(self->staticLaneList);
    glCallList(self->laneList);
  }

  // Draw the volatile graph
  if (self->viewVolatile)
    glCallList(self->graphList);

  // Draw the path
  if (self->viewPath)
    glCallList(self->pathList);

  // Draw the trajectory
  if (self->viewTraj)
    glCallList(self->trajList);

  // Draw the path history
  if (self->viewHist)
    glCallList(self->histList);

  glPopMatrix();

  // Switch to viewport frame
  self->pushViewport();

  // Draw textual status
  self->drawStatus();
  
  // Draw the speed profile
  if (true)
    glCallList(self->speedList);
  
  self->popViewport();

  // Screen cap to file
  if (self->captureFrame || self->captureMovie)
  {
#ifdef JPLV_IMAGE_H
    jplv_image_t *image;
    char filename[1024];
    int vp[4];
    
    glGetIntegerv(GL_VIEWPORT, vp);
    image = jplv_image_alloc(vp[2], vp[3], 3, 8, 0, NULL);
    glReadPixels(vp[0], vp[1], vp[2], vp[3], GL_RGB, GL_UNSIGNED_BYTE, image->data);       
    snprintf(filename, sizeof(filename), "planner-%04d.pnm", self->capCount);
    MSG("writing %s", filename);
    jplv_image_write_pnm(image, filename, NULL);
    jplv_image_free(image);
#endif
    
    self->captureFrame = false;
    self->capCount++;
  }

  return;
}


// Switch to the vehicle frame
void App::pushFrameVehicle(const VehicleState *state)
{
  pose3_t pose;

  // Project the vehicle down to zero altitude
  pose.pos = vec3_set(state->utmNorthing, state->utmEasting, 0); 
  pose.rot = quat_from_rpy(state->utmRoll, state->utmPitch, state->utmYaw);

  return this->pushFramePose(pose);
}


// Switch to the local frame
void App::pushFrameLocal(const VehicleState *state)
{
  pose3_t gpose, lpose, pose;
  
  // Vehicle global pose
  gpose.pos = vec3_set(state->utmNorthing,
                       state->utmEasting,
                       state->utmAltitude);
  gpose.rot = quat_from_rpy(state->utmRoll,
                            state->utmPitch,
                            state->utmYaw);

  // Vehicle local pose
  lpose.pos = vec3_set(state->localX,
                       state->localY,
                       state->localZ);
  lpose.rot = quat_from_rpy(state->localRoll,
                            state->localPitch,
                            state->localYaw);

  // Transform from local to global frame
  pose = pose3_mul(gpose, pose3_inv(lpose));

  return this->pushFramePose(pose);
}


// Switch to the a given pose
void App::pushFramePose(pose3_t pose)
{
  float m[4][4];

  // Add in the display offset
  pose.pos.x -= this->offset.x;
  pose.pos.y -= this->offset.y;
  pose.pos.z -= this->offset.z;
  
  // Convert to matrix
  pose3_to_mat44f(pose, m);  

  // Transpose to column-major order for GL
  int i, j;
  float t[16];
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);
  
  return;
}


// Revert to previous frame
void App::popFrame()
{
  glPopMatrix();  
  return;
}


// Switch to viewport projection.
void App::pushViewport()
{
  GLint vp[4];
      
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Get the viewport dimensions
  glGetIntegerv(GL_VIEWPORT, vp);
  
  // Shift and rescale such that (0, 0) is the top-left corner and
  // (cols, rows) is the bottom-right corner.
  glTranslatef(-1.0, 1.0, 0);
  glScalef(2.0/vp[2], -2.0/vp[3], 1.0);

  /*
  // Test code
  glColor3f(1, 1, 1);
  glBegin(GL_QUADS);
  glVertex3f(0, 0, 0);
  glVertex3f(100, 0, 0);
  glVertex3f(100, 50, 0);
  glVertex3f(0, 50, 0);
  glEnd();
  */
  
  return;
}


// Revert to non-viewport projection
void App::popViewport()
{
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  return;
}


// Draw a set of axes
void App::drawAxes(float size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Draw the robot
void App::drawAlice(float steerAngle, int signalState)
{
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(steerAngle * 180/M_PI, 0, 0, 1);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(steerAngle * 180/M_PI, 0, 0, 1);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  int onoff;
  
  // Draw the left turn signal
  onoff = (signalState == -1) && ((DGCgettime() / 300000) % 2);  
  glPushMatrix();
  glTranslatef(-DIST_REAR_TO_REAR_AXLE, -VEHICLE_WIDTH/2, -VEHICLE_HEIGHT/2);
  glColor3f(0.5 * (onoff + 1), 0.25 * (onoff + 1), 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glutSolidCube(0.30);
  glPopMatrix();
  
  // Draw the right turn signal
  onoff = (signalState == +1) && ((DGCgettime() / 300000) % 2);  
  glPushMatrix();
  glTranslatef(-DIST_REAR_TO_REAR_AXLE, +VEHICLE_WIDTH/2, -VEHICLE_HEIGHT/2);
  glColor3f(0.5 * (onoff + 1), 0.25 * (onoff + 1), 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glutSolidCube(0.30);
  glPopMatrix();

  // Draw a rectangle showing the c-space guards
  if (true)
  {
    glColor3f(1, 0.5, 0);
    glBegin(GL_LINE_LOOP);
    glVertex2f(this->configSpace->outerAx, this->configSpace->outerAy);
    glVertex2f(this->configSpace->outerBx, this->configSpace->outerAy);
    glVertex2f(this->configSpace->outerBx, this->configSpace->outerBy);
    glVertex2f(this->configSpace->outerAx, this->configSpace->outerBy);
    glEnd();

    glColor3f(1, 0, 0);
    glBegin(GL_LINE_LOOP);
    glVertex2f(this->configSpace->innerAx, this->configSpace->innerAy);
    glVertex2f(this->configSpace->innerBx, this->configSpace->innerAy);
    glVertex2f(this->configSpace->innerBx, this->configSpace->innerBy);
    glVertex2f(this->configSpace->innerAx, this->configSpace->innerBy);
    glEnd();
  }

  return;
}


// TODO move to file with other GL helpers
// Draw a text box
void glDrawText(float size, const char *text)
{
  int i, count;
  float sx, sy;

  // Compute the scale factors in x/y directions, based on the 'M'
  // character.
  sx = glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, 'M');
  sy = sx * 1.3;
    
  glPushMatrix();
  glScalef(size / sx, size / sy, 1);
  glTranslatef(0, -sy, 0);

  glPushMatrix();
    
  for (i = 0, count = 0; i < (int) strlen(text); i++, count++)
  {
    if (text[i] == '\n')
    {
      glPopMatrix();
      glTranslatef(0, -sy, 0);
      glPushMatrix();
      count = 0;      
    }
    else
    {
      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[i]);
    }
  }

  glPopMatrix();
  glPopMatrix();

  return;
}


// Draw status info
void App::drawStatus()
{
  char text[1024];
       
  // Construct string.
  // HACK: we assume PDT (GMT-8+1) for the timezone.
  snprintf(text, sizeof(text),
           "Time  : %02lld:%02lld:%02lld:%03lld\n"
           "Goal  : %d CP %d %.1fm\n"
           "Speed : %.1fm/s %.1fmph\n"
           "Stop  : %d %.1fs\n"
           "CPU   : %d %d %d ms\n",
           (this->vehState.timestamp / 1000000 / 60 / 60 + (24 - 8 + 1)) % 24,
           (this->vehState.timestamp / 1000000 / 60) % 60,
           (this->vehState.timestamp / 1000000) % 60,
           (this->vehState.timestamp / 1000) % 1000,
           this->goalIndex, this->goalId, this->path.goalDist,
           this->vehState.vehSpeed, this->vehState.vehSpeed * 2.24,
           this->trajPlanner->stopState,
           (this->trajPlanner->stopState == 1 ?
           (double) (this->vehState.timestamp - this->trajPlanner->stopTime) * 1e-6 : 0),
           (int) (this->updateInterval / 1000),
           (int) (this->stateInterval / 1000),
           (int) (this->mapInterval / 1000)
           );

  // Hmmm, somewhat hacky way to set the text size.
  // Account for viewport dimensions to preserve 1:1 aspect ratio.
  glPushMatrix();
  glScalef(12, -12, 1);
  glColor3f(1, 1, 0);
  glDrawText(1, text);
  glPopMatrix();
  
  return;
}


// Draw the configuration space info for a node
void App::drawNodeCSpace(GraphNode *node)
{
  GLUquadric *quad;
  int collide;

  collide = MAX(node->collideObs, node->collideCar);
  if (collide == 0)
    return;
  
  if (collide == GRAPH_COLLIDE_MAP_OUTER)
    glColor3f(1, 1, 0);
  else if (collide == GRAPH_COLLIDE_MAP_INNER)
    glColor3f(1, 0, 0);
  else if (collide == GRAPH_COLLIDE_LANE_OUTER)
    glColor3f(0.5, 0.5, 0);
  else if (collide == GRAPH_COLLIDE_LANE_INNER)
    glColor3f(0.5, 0, 0);

  glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);

  this->pushFramePose(node->pose);    

  quad = gluNewQuadric();
    
  // Show the node
  gluDisk(quad, 0, 0.20, 16, 1);

  // Show the node orientation
  glBegin(GL_LINES);  
  glVertex3d(0, 0, 0);
  glVertex3d(0.40, 0, 0);
  glEnd();

  if (false)
  {
    // Draw outer vehicle dimensions TODO replace magic numbers
    glPushMatrix();
    glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
    glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
    glColor3f(0, 0, 1);
    glutWireCube(1.0);
    glPopMatrix();
  }

  this->popFrame();

  gluDeleteQuadric(quad);

  return;
}


// Predraw the map in the local frame
void App::predrawMap()
{
  int i, j;
  MapElement *mapel;
  point2_uncertain *point;
  
  if (this->mapList == 0)
    this->mapList = glGenLists(1);
  glNewList(this->mapList, GL_COMPILE);
  
  for (i = 0; i < (int) this->map.data.size(); i++)
  {
    mapel = &this->map.data[i];

    if (mapel->geometry.size() == 0)
      continue;
    
    if (false)
    {
      // Draw the bounding box
      glPushMatrix();
      glTranslated(mapel->center.x, mapel->center.y, 0);
      glRotated(mapel->orientation * 180/M_PI, 0, 0, 1);
      glColor3f(1, 0, 0);
      glBegin(GL_LINE_LOOP);
      glVertex2d(-mapel->length/2, -mapel->width/2);
      glVertex2d(+mapel->length/2, -mapel->width/2);
      glVertex2d(+mapel->length/2, +mapel->width/2);
      glVertex2d(-mapel->length/2, +mapel->width/2);
      glEnd();
      glPopMatrix();
    }

    // Draw points
    if (mapel->geometryType == GEOMETRY_POINTS)
    {
      glPointSize(2);
      glColor3f(1, 0, 0);
      glBegin(GL_POINTS);
      //glBegin(GL_QUADS);
      for (j = 0; j < (int) mapel->geometry.size(); j++)
      {
        point = &mapel->geometry[j];
        glVertex3d(point->x, point->y, 0);
        //glVertex3f(point->x - 0.10, point->y - 0.10, 0);
        //glVertex3f(point->x + 0.10, point->y - 0.10, 0);
        //glVertex3f(point->x + 0.10, point->y + 0.10, 0);
        //glVertex3f(point->x - 0.10, point->y + 0.10, 0);
      }
      glEnd();
    }

    // Draw polygon
    if (mapel->geometryType == GEOMETRY_POLY)
    {
      glPointSize(2);
      glColor3f(1, 0, 0);
      glBegin(GL_POLYGON);
      for (j = 0; j < (int) mapel->geometry.size(); j++)
      {
        point = &mapel->geometry[j];
        glVertex3d(point->x, point->y, 0);
      }
      glEnd();
    }
  }
  
  glEndList();
  
  return;
}


// Predraw the static graph in the local frame
void App::predrawStaticGraph()
{
  int i;
  GraphNode *node;
  GraphArc *arc;
  vec3_t pa, pb;
  GLUquadric *quad;
  char text[64];
    
  if (this->staticGraphList == 0)
    this->staticGraphList = glGenLists(1);
  glNewList(this->staticGraphList, GL_COMPILE);

  quad = gluNewQuadric();
  
  // Draw nodes
  for (i = 0; i < this->graph->getStaticNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    assert(node);
        
    if (!this->viewTurns)
      if (node->type == GRAPH_NODE_TURN)
        continue;
    if (!this->viewChanges)
      if (node->type == GRAPH_NODE_CHANGE)
        continue;

    if (node->type == GRAPH_NODE_LANE)
    {
      if (node->isEntry)
        glColor3f(0, 1, 0);
      else if (node->isExit)
        glColor3f(1, 0, 0);
      else if (node->isCheckpoint)
        glColor3f(1, 0, 1);
      else if (node->isWaypoint)
        glColor3f(1, 1, 1);
      else
        glColor3f(0.5, 0.5, 1);
    }
    else
    {
      glColor3f(0.5, 0.5, 0.5);
    }
    
    pa = node->pose.pos;
    pb = vec3_transform(node->pose, vec3_set(0.4, 0, 0));

    pa = vec3_sub(pa, this->offset);
    pb = vec3_sub(pb, this->offset);

    // Show the node center
    glPointSize(3);
    glBegin(GL_POINTS);  
    glVertex3d(pa.x, pa.y, pa.z);
    glEnd();

    // Show the node orientation
    if (true && node->isWaypoint)
    {
      glBegin(GL_LINES);  
      glVertex3d(pa.x, pa.y, pa.z);
      glVertex3d(pb.x, pb.y, pa.z);    
      glEnd();
    }

    // If this has a stop line, show that
    if (node->isStop)
    {
      pa = vec3_transform(node->pose, vec3_set(0, -node->laneWidth/2, 0));
      pa = vec3_sub(pa, this->offset);
      pb = vec3_transform(node->pose, vec3_set(0, +node->laneWidth/2, 0));
      pb = vec3_sub(pb, this->offset);      
      glBegin(GL_LINES);
      glVertex3d(pa.x, pa.y, pa.z);
      glVertex3d(pb.x, pb.y, pa.z);    
      glEnd();
    }

    // Show node info
    if (true && node->isCheckpoint)
    {
      snprintf(text, sizeof(text), "%d.%d.%d CP %d\nPC %.3f",
               node->segmentId, node->laneId,
               node->waypointId, node->checkpointId,
               (node->planCost < GRAPH_PLAN_COST_MAX ? node->planCost * 1e-2 : -1));

      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glPushMatrix();
      glTranslated(node->pose.pos.x - this->offset.x,
                   node->pose.pos.y - this->offset.y,
                   node->pose.pos.z - this->offset.z);
      glRotatef(180, 0, 1, 0);
      gluDisk(quad, 0.50, 0.50, 16, 1);
      glDrawText(0.1, text);
      glPopMatrix();
    }
    else if (true && node->isWaypoint)
    {
      snprintf(text, sizeof(text), "%d.%d.%d",
               node->segmentId, node->laneId, node->waypointId);

      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glPushMatrix();
      glTranslated(node->pose.pos.x - this->offset.x,
                   node->pose.pos.y - this->offset.y,
                   node->pose.pos.z - this->offset.z);
      glRotatef(180, 0, 1, 0);
      gluDisk(quad, 0.50, 0.50, 16, 1);
      glDrawText(0.1, text);
      glPopMatrix();
    }
    else if (false && node->direction > 0)
    {
      snprintf(text, sizeof(text), "%d.%d:%d",
               node->segmentId, node->laneId, node->direction);
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glPushMatrix();
      glTranslated(node->pose.pos.x - this->offset.x,
                   node->pose.pos.y - this->offset.y,
                   node->pose.pos.z - this->offset.z);
      glRotatef(180, 0, 1, 0);
      gluDisk(quad, 0.10, 0.10, 16, 1);
      glDrawText(0.05, text);
      glPopMatrix();
    }

    if (this->viewCSpace)
      this->drawNodeCSpace(node);
  }

  // Draw arcs
  glBegin(GL_LINES);    
  for (i = 0; i < this->graph->getStaticNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    assert(node);

    for (arc = node->outFirst; arc != NULL; arc = arc->outNext)
    {
      if (!this->viewTurns)
        if (arc->nodeA->type == GRAPH_NODE_TURN || arc->nodeB->type == GRAPH_NODE_TURN)
          continue;
      if (!this->viewChanges)
        if (arc->nodeA->type == GRAPH_NODE_CHANGE || arc->nodeB->type == GRAPH_NODE_CHANGE)
          continue;

      glColor3f(0.5, 0.5, 0.5);
      glVertex3d(arc->nodeA->pose.pos.x - this->offset.x,
                 arc->nodeA->pose.pos.y - this->offset.y,
                 arc->nodeA->pose.pos.z - this->offset.z);
      glColor3f(1, 1, 1);
      glVertex3d(arc->nodeB->pose.pos.x - this->offset.x,
                 arc->nodeB->pose.pos.y - this->offset.y,
                 arc->nodeB->pose.pos.z - this->offset.z);
    }
  }
  glEnd();
  
  gluDeleteQuadric(quad);
    
  glEndList();

  return;
}


// Predraw the non-static graph info
void App::predrawGraph()
{
  int i;
  GraphNode *node;
  GraphArc *arc;
  GLUquadric *quad;
  char text[64];
    
  quad = gluNewQuadric();
  
  if (this->graphList == 0)
    this->graphList = glGenLists(1);
  glNewList(this->graphList, GL_COMPILE);

  // Draw nodes
  for (i = this->graph->getStaticNodeCount(); i < this->graph->getNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    assert(node);

    // Stop stop nodes
    if (false && node->isStop)
    {
      glColor3f(1, 0, 0);
      glPointSize(9);
      glBegin(GL_POINTS);  
      glVertex3d(node->pose.pos.x - this->offset.x,
                 node->pose.pos.y - this->offset.y,
                 node->pose.pos.z - this->offset.z);
      glEnd();      
    }

    if (true)  // TESTING
    {
      snprintf(text, sizeof(text), "%d.%d:%d",
               node->segmentId, node->laneId, node->direction);

      glColor3f(0, 0, 1);
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glPushMatrix();
      glTranslated(node->pose.pos.x - this->offset.x,
                   node->pose.pos.y - this->offset.y,
                   node->pose.pos.z - this->offset.z);
      glRotatef(180, 0, 1, 0);
      gluDisk(quad, 0.10, 0.10, 16, 1);
      glDrawText(0.05, text);
      glPopMatrix();
    }
    
    if (this->viewCSpace)
      this->drawNodeCSpace(node);
  }

  // Draw arcs
  glBegin(GL_LINES);
  for (i = this->graph->getStaticNodeCount(); i < this->graph->getNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    assert(node);

    for (arc = node->outFirst; arc != NULL; arc = arc->outNext)
    {
      glColor3f(arc->nodeA->centerDist, 0, 1 - arc->nodeA->centerDist);
      //if (!arc->nodeA->offRoad)
      //  glColor3f(0, 0, 0.5);
      //else
      //  glColor3f(0.5, 0, 0);      
      glVertex3d(arc->nodeA->pose.pos.x - this->offset.x,
                 arc->nodeA->pose.pos.y - this->offset.y,
                 arc->nodeA->pose.pos.z - this->offset.z);
      glColor3f(arc->nodeB->centerDist, 0, 1 - arc->nodeB->centerDist);
      //if (!arc->nodeB->offRoad)
      //  glColor3f(0, 0, 1);
      //else
      //  glColor3f(1, 0, 0);
      glVertex3d(arc->nodeB->pose.pos.x - this->offset.x,
                 arc->nodeB->pose.pos.y - this->offset.y,
                 arc->nodeB->pose.pos.z - this->offset.z);
    }
  }
  glEnd();

  gluDeleteQuadric(quad);
    
  glEndList();

  return;
}


// Predraw the lane boundaries 
void App::predrawStaticLanes()
{
  int i;
  GraphNode *node;
  double px, py, qx, qy;

  if (this->staticLaneList == 0)
    this->staticLaneList = glGenLists(1);
  glNewList(this->staticLaneList, GL_COMPILE);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  
  glColor4f(1, 1, 0, 0.5);
  
  for (i = 0; i < this->graph->getStaticNodeCount(); i++)
  {
    node = this->graph->getNode(i);

    if (!(node->type == GRAPH_NODE_LANE || node->type == GRAPH_NODE_TURN))
      continue;
    
    glBegin(GL_LINES);
        
    px = -node->laneLength/2;
    py = -node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = +node->laneLength/2;
    py = -node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = +node->laneLength/2;
    py = +node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = -node->laneLength/2;
    py = +node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    glEnd();
  }

  glEndList();
  glDisable(GL_BLEND);
  
  return;
}


// Predraw lanes with obstacles
void App::predrawLanes()
{
  int i;
  GraphNode *node;
  double px, py, qx, qy;
  //double k;

  if (this->laneList == 0)
    this->laneList = glGenLists(1);
  glNewList(this->laneList, GL_COMPILE);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      
  for (i = 0; i < this->graph->getStaticNodeCount(); i++)
  {
    node = this->graph->getNode(i);

    if (!(node->type == GRAPH_NODE_LANE || node->type == GRAPH_NODE_TURN))
      continue;

    /* REMOVE?
    if (!node->isOcc)
      continue;
    
    char text[64];
    snprintf(text, sizeof(text), "%.1f",
             (this->vehState.timestamp - node->occTime) * 1e-6);
    MSG("lanel %s", text);
    glColor3f(1, 0, 0);
    glPushMatrix();
    glTranslated(node->pose.pos.x - this->offset.x,
                 node->pose.pos.y - this->offset.y,
                 node->pose.pos.z - this->offset.z);
    glRotatef(180, 0, 1, 0);
    glDrawText(0.05, text);
    glPopMatrix();

    k = (this->vehState.timestamp - node->occTime) * 1e-7;
    glColor4f(k, 1 - k, 0, 0.5);

    //continue;

    //if (!node->isOcc)
    //  continue;
    */

    if (node->obsInLane)
      glColor4f(1, 0, 0, 0.5);
    else if (node->carInLane)
      glColor4f(1, 1, 0, 0.5);
    //else
    //  glColor4f(1, 1, 1, 0.50);
    else 
     continue;
    
    glBegin(GL_POLYGON);
        
    px = -node->laneLength/2;
    py = -node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = +node->laneLength/2;
    py = -node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = +node->laneLength/2;
    py = +node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = -node->laneLength/2;
    py = +node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    glEnd();
  }

  glDisable(GL_BLEND);
  
  glEndList();
  
  return;
}



// Draw the future path in the global frame
void App::predrawPath()
{
  int i;
  GraphNode *node;
  
  if (this->pathList == 0)
    this->pathList = glGenLists(1);
  glNewList(this->pathList, GL_COMPILE);

  glLineWidth(2);
  glColor3f(1, 0, 1);
  glBegin(GL_LINE_STRIP);
  for (i = 0; i < this->path.pathLen; i++)
  {    
    node = this->path.path[i];
    assert(node);

    /* REMOVE
    // We start out coloring the path in green, but change to
    // red if we see a collision.
    if (node->collideObs || node->collideCar)
      glColor3f(1, 0, 0);
    */
      
    glVertex3d(node->pose.pos.x - this->offset.x,
               node->pose.pos.y - this->offset.y,
               node->pose.pos.z - 0.20 - this->offset.z);
  }
  glEnd();
  glLineWidth(1);
  
  glEndList();

  return;
}


// Draw the trajectory in the global frame
void App::predrawTraj()
{
  int i;
  vec3_t p, q;
  //float speed;
  
  if (this->trajList == 0)
    this->trajList = glGenLists(1);
  glNewList(this->trajList, GL_COMPILE);

  // Draw velocity vectors
  glPointSize(2.0);
  glColor3f(0, 1, 0);  
  for (i = 0; i < this->traj->getNumPoints(); i++)
  {
    p.x = this->traj->getNdiffarray(0)[i];
    p.y = this->traj->getEdiffarray(0)[i];    
    p.z = 0;

    q.x = p.x + this->traj->getNdiffarray(1)[i] * 0.5;
    q.y = p.y + this->traj->getEdiffarray(1)[i] * 0.5;    
    q.z = 0;

    glBegin(GL_LINES);
    glVertex3d(p.x - this->offset.x,
               p.y - this->offset.y,
               p.z - 0.30 - this->offset.z);
    glVertex3d(q.x - this->offset.x,
               q.y - this->offset.y,
               q.z - 0.30 - this->offset.z);
    glEnd();

    glBegin(GL_POINTS);
    glVertex3d(p.x - this->offset.x,
               p.y - this->offset.y,
               p.z - 0.30 - this->offset.z);
    glEnd();    
  }
  glPointSize(1.0);
  
  glEndList();

  return;
}


// Draw the trajectory speed profile (viewport frame)
void App::predrawSpeed()
{
  int i;
  double ax, ay, bx, by;
  double ps, pd;
  double maxDist;
  GLint vp[4];

  // Get the viewport dimensions
  glGetIntegerv(GL_VIEWPORT, vp);

  maxDist = 20.0; // MAGIC
  
  if (this->speedList == 0)
    this->speedList = glGenLists(1);
  glNewList(this->speedList, GL_COMPILE);

  glPushMatrix();
  glTranslatef(10, vp[3] - 100, 0);
  glScalef(15, -10, 1);

  // Draw label
  glPushMatrix();
  glTranslatef(0, -6, 0);
  glDrawText(0.7, "Speed (m/s) versus distance (m)");
  glPopMatrix();
  
  // Draw x-axis
  glBegin(GL_LINES);
  for (i = -6; i <= +6; i++)
  {    
    glColor3f((i == 0 ? 1 : 0.5), 0, 0);
    glVertex3d(0, i, 0.1);
    glVertex3d(maxDist, i, 0.1);
  }
  glEnd();
  
  // Draw y axis
  glBegin(GL_LINES);
  for (i = 0; i <= maxDist; i += 4)
  {    
    glColor3f(0, (i == 0 ? 1 : 0.5), 0);
    glVertex3d(i, -6, 0.1);
    glVertex3d(i, +6, 0.1);
  }
  glEnd();

  if (false)
  {
    // Draw the path speed profile  
    pd = 0;
    glColor3f(1, 1, 1);
    glLineWidth(2);
    glBegin(GL_LINE_STRIP);
    for (i = 0; i < this->path.pathLen; i++)
    {
      pd = this->path.dists[i];
      ps = this->path.speeds[i];
      glVertex3d(pd, ps, 0);
      if (pd > maxDist)
        break;
    }      
    glEnd();
    glLineWidth(1);
  }

  if (true)
  {
    // Draw the trajectory speed profile
    pd = 0;
    glColor3f(1, 1, 1);
    glLineWidth(2);
    glBegin(GL_LINE_STRIP);
    for (i = 0; i < this->traj->getNumPoints(); i++)
    {
      ax = this->traj->getNdiffarray(0)[i + 0];
      ay = this->traj->getEdiffarray(0)[i + 0];    
      ps = this->traj->getSpeed(i + 0);

      glVertex3d(pd, ps, 0);

      if (i + 1 < this->traj->getNumPoints())
      {
        bx = this->traj->getNdiffarray(0)[i + 1];
        by = this->traj->getEdiffarray(0)[i + 1];
        pd += sqrt((bx - ax) * (bx - ax) + (by - ay) * (by - ay));
        if (pd > maxDist)
          break;
      }
    }      
    glEnd();
    glLineWidth(1);
  }

  glPopMatrix();
  glEndList();

  return;
}


// Draw the path history in the global frame
void App::predrawHist()
{
  int i;
  pose3_t pose;
  
  // Generate display list 
  if (this->histList == 0)
    this->histList = glGenLists(1);
  glNewList(this->histList, GL_COMPILE);

  glColor3f(1, 0, 1);
  glBegin(GL_LINE_STRIP);
  for (i = 0; i < this->numPoses; i++)
  {
    pose = this->poses[i];

    // Compute front axle pose
    if (true)
      pose.pos = vec3_transform(pose, vec3_set(VEHICLE_AXLE_DISTANCE, 0, 0));

    glVertex3d(pose.pos.x - this->offset.x,
               pose.pos.y - this->offset.y,
               0 - this->offset.z);
  }
  glEnd();
  
  glEndList();
    
  return;
}


// Handle idle callbacks
void App::onIdle(App *self)
{
  if ((self->mode == modeLive && !self->options.asim_hacks_flag) || !self->pause || self->step)
  {      
    // Update perceptor
    if (self->update() != 0)
    {
      self->quit = true;
      return;
    }

    // Redraw the display
    self->worldwin->redraw();
    
    // Single step mode: we've moved one step, so clear flag
    if (self->step)
      self->step = false;

    // Play nice and max out at 20Hz (dont busy-loop)
    usleep(50000);
  }
  else
  {
    // Sleepy bye-bye
    usleep(20000);
  }
  
  return;
}



// Initialize live mode
int App::initLive()
{
  // Create sensnet interface for state messages
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);

  // Open connection
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, MODgraphPlanner) != 0)
    return ERROR("unable to connect to sensnet");
 
  // Listen to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR,
                   SNstate, sizeof(this->vehState)) != 0)
    return ERROR("unable to join state group");

  // Listen to actuator state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR,
                   SNactuatorstate, sizeof(this->actState)) != 0)
    return ERROR("unable to join state group");

  // Create talker for map update
  this->mapTalker = new CMapElementTalker();
  this->mapTalker->initRecvMapElement(this->skynetKey, MODgraphPlanner);
  
  // Create talker for the output trajectory
  this->trajTalker = new CTrajTalker(MODgraphPlanner, this->skynetKey);

  // Turn signal interface
  this->turnSender = new TurnSignalSender(this->skynetKey);
  
  return 0;
}


// Finalize live mode
int App::finiLive()
{
  // Clean up talkers
  delete this->turnSender;
  this->turnSender = NULL;
  delete this->trajTalker;
  this->trajTalker = NULL;
  delete this->mapTalker;
  this->mapTalker = NULL;

  // Clean up sensnet
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNactuatorstate);
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}


// Initialize the planner
int App::init()
{
  // Create graph
  this->graph = new Graph(this->options.max_nodes_arg, this->options.max_arcs_arg);
  
  // Create planner
  this->planner = new GraphPlanner(this->graph);
  assert(this->planner);

  // Set vehicle properties
  if (true)
  {
    GraphPlannerKinematics kin;
    this->planner->getKinematics(&kin);
    kin.wheelBase = VEHICLE_WHEELBASE;
    kin.maxSteer = this->options.max_steer_arg * M_PI/180;
    kin.maxTurn = this->options.max_turn_arg * M_PI/180;
    this->planner->setKinematics(&kin);
  }

  // Load up the RNDF and initialize the graph
  assert(this->options.rndf_given);
  if (this->planner->loadRndf(this->options.rndf_arg) != 0)
    return ERROR("failed loading RNDF");

  if (this->graph->getNodeCount() == 0)
    return ERROR("RNDF is empty (no nodes)");
  
  // Set the display offset (improves numerical conditioning for GL)
  if (true)
  {
    GraphNode *node;
    node = this->graph->getNode(0);
    assert(node);
    this->offset = node->pose.pos;
  }

  // Create configuration space predictor
  this->configSpace = new ConfigSpace(this->graph);
  assert(this->configSpace);

  // Set vehicle dimensions for configuration space calculations
  if (true)
  {
    ConfigSpaceDimensions dim;
    this->configSpace->getDimensions(&dim);    
    dim.front = this->options.front_dist_arg;
    dim.left = this->options.side_dist_arg;
    dim.right = this->options.side_dist_arg;
    this->configSpace->setDimensions(&dim);
  }

  // Create trajectory planner
  this->trajPlanner = new TrajPlanner(this->graph);
  assert(this->trajPlanner);
  
  // Set vehicle properties
  if (true)
  {
    TrajPlannerKinematics kin;
    this->trajPlanner->getKinematics(&kin);
    kin.wheelBase = VEHICLE_WHEELBASE;
    kin.maxSteer = this->options.max_steer_arg * M_PI/180;
    this->trajPlanner->setKinematics(&kin);

    TrajPlannerSpeeds speeds;
    this->trajPlanner->getSpeeds(&speeds);
    speeds.maxLaneSpeed = this->options.max_speed_arg;
    this->trajPlanner->setSpeeds(&speeds);
  }

  // Load up the MDF if given
  if (this->options.mdf_given)
  {
    MSG("loading %s", this->options.mdf_arg);
    if (this->mdf.loadFile(this->options.mdf_arg) != 0)
      return ERROR("failed loading MDF");
    MSG("MDF has %d checkpoints", this->mdf.getNumCheckpoints());
  }

  // In simulation mode, initialize the state to waypoint in the RNDF
  if (this->mode == modeSim)
  {
    int segment, lane, waypoint;
    GraphNode *node;

    // Default to the first node
    node = this->graph->getNode(0);

    if (this->options.rndf_start_given)
    {
      // Parse out the node from the command line args
      if (sscanf(this->options.rndf_start_arg, "%d.%d.%d", &segment, &lane, &waypoint) < 3)
        MSG("syntax error: %s", this->options.rndf_start_arg);
      node = this->graph->getNodeFromRndfId(segment, lane, waypoint);
      if (!node)
        MSG("no such waypoint: %s", this->options.rndf_start_arg);
    }

    // Set initial position and heading
    if (node)
    {
      double roll, pitch, yaw;      
      memset(&this->vehState, 0, sizeof(this->vehState));
      this->vehState.utmNorthing = node->pose.pos.x;
      this->vehState.utmEasting = node->pose.pos.y;
      this->vehState.utmAltitude = node->pose.pos.z;
      quat_to_rpy(node->pose.rot, &roll, &pitch, &yaw);
      this->vehState.utmYaw = yaw;
    }
  }

  // Create trajectory
  this->traj = new CTraj(3);
  
  return 0;
}


// Finalize the planner
int App::fini()
{
  // Clean up trajectory
  assert(this->traj);
  delete this->traj;
  this->traj = NULL;
    
  // Clean up traj planner
  assert(this->trajPlanner);
  delete this->trajPlanner;
  this->trajPlanner = NULL;

  // Clean up config space
  assert(this->configSpace);
  delete this->configSpace;
  this->configSpace = NULL;
    
  // Clean up planner
  assert(this->planner);
  delete this->planner;
  this->planner = NULL;
 
  // Clean up graph
  assert(this->graph);
  delete this->graph;
  this->graph = NULL;
  
  return 0;  
}


// Update the planner
int App::update()
{
  uint64_t time;

  if (this->mode == modeSim)
  {
    this->vehState.timestamp = DGCgettime();
    
    // In simulation mode, we step the vehicle along the current path.
    if (this->path.valid && this->path.pathLen >= 2)
    {
      GraphNode *node;
      pose3_t pose;
      double roll, pitch, yaw;
      
      node = this->path.path[1];

      if (true) // node->collideObs < 2 && node->collideCar < 2)
      {
        pose = node->pose;
        quat_to_rpy(pose.rot, &roll, &pitch, &yaw);            
        this->vehState.utmNorthing = pose.pos.x;
        this->vehState.utmEasting = pose.pos.y;
        this->vehState.utmAltitude = pose.pos.z;
        this->vehState.utmRoll = roll;
        this->vehState.utmPitch = pitch;
        this->vehState.utmYaw = yaw;

        pose = node->pose;
        quat_to_rpy(pose.rot, &roll, &pitch, &yaw);            
        this->vehState.localX = pose.pos.x;
        this->vehState.localY = pose.pos.y;
        this->vehState.localZ = pose.pos.z;
        this->vehState.localRoll = roll;
        this->vehState.localPitch = pitch;
        this->vehState.localYaw = yaw;

        // Fake actuator state
        this->actState.m_steercmd = node->steerAngle / VEHICLE_MAX_AVG_STEER;
        this->actState.m_steerpos = this->actState.m_steercmd;
      }      
    }

    // Fake actuator state
    this->actState.m_estoppos = 2;
  }
  else
  {
    int blobId;
    MapElement mapel;

    this->stateInterval = DGCgettime();      
    while (true)
    {
      // Get the vehicle state from sensnet
      sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
                   &blobId, sizeof(this->vehState), &this->vehState);
      if (blobId >= 0)
        break;
    }
    while (true)
    {
      // Get the actuator state from sensnet
      sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNactuatorstate,
                   &blobId, sizeof(this->actState), &this->actState);
      if (blobId >= 0)
        break;
    }
    this->stateInterval = DGCgettime() - this->stateInterval;

    // Read any map elements and update the map.
    // This takes longer than it should; possible skynet problems?
    this->mapInterval = DGCgettime();
    while (this->mapTalker->recvMapElementNoBlock(&mapel) > 0)
    {
      MSG("got map element %f %f %d %d",
          mapel.center.x, mapel.center.y, mapel.geometry.size(), mapel.type);
      // Override the element type
      if (this->options.force_obs_flag)
        mapel.type = ELEMENT_OBSTACLE;
      if (this->options.force_cars_flag)
        mapel.type = ELEMENT_VEHICLE;      
      this->map.addEl(mapel);
    }
    this->mapInterval = DGCgettime() - this->mapInterval;
  }

  if (this->options.asim_hacks_flag)
  {
    // Hack for asim (doesn't set speed)
    this->vehState.vehSpeed =
      sqrt(pow(this->vehState.utmNorthVel, 2) + pow(this->vehState.utmEastVel, 2));
  }
  
  // If the state has changed, update the pose history
  if (true)
  {
    pose3_t pose;

    // Compute global pose
    pose.pos = vec3_set(this->vehState.utmNorthing,
                        this->vehState.utmEasting,
                        this->vehState.utmAltitude);
    pose.rot = quat_from_rpy(this->vehState.utmRoll,
                             this->vehState.utmPitch,
                             this->vehState.utmYaw);

    // If we have moved since the last pose was added to the list...
    if (this->numPoses == 0 ||
        vec3_mag(vec3_sub(pose.pos, this->poses[this->numPoses - 1].pos)) > 0.20)
    {
      // Expand the list to make room, if necessary
      if (this->numPoses >= this->maxPoses)
      {
        this->maxPoses += 1000;
        this->poses = (pose3_t*) realloc(this->poses, this->maxPoses * sizeof(this->poses[0]));
        assert(this->poses);
      }
      // Add pose to the list
      this->poses[this->numPoses++] = pose;
      //MSG("added pose %d", this->numPoses);
    }
  }
  
  // Create static obstacles
  if (this->options.fake_obs_given > 0)
  {
    int i;
    int segment, lane, waypoint;
    GraphNode *node;
    
    for (i = 0; i < (int) this->options.fake_obs_given; i++)
    {
      if (sscanf(this->options.fake_obs_arg[i], "%d.%d.%d", &segment, &lane, &waypoint) < 3)
      {
        MSG("syntax error: %s", this->options.fake_obs_arg[i]);
        continue;
      }
      MSG("fake obstacle at %d.%d.%d", segment, lane, waypoint);
      node = this->graph->getNodeFromRndfId(segment, lane, waypoint);
      assert(node);
      if (this->options.force_cars_flag)
        this->configSpace->addFakeCar(node, &this->vehState, &this->map, ELEMENT_VEHICLE);
      else
        this->configSpace->addFakeCar(node, &this->vehState, &this->map, ELEMENT_OBSTACLE);
    }    
  }
  
  // Test code for logging the pose
  if (false)
  {
    // Log the pose
    fprintf(stdout, "pose %f %f %f %f %f %f steer %f %f\n",
            this->vehState.utmNorthing,
            this->vehState.utmEasting,
            this->vehState.utmAltitude,
            this->vehState.utmRoll,
            this->vehState.utmPitch,
            this->vehState.utmYaw,
            this->actState.m_steercmd * VEHICLE_MAX_AVG_STEER,
            this->actState.m_steerpos * VEHICLE_MAX_AVG_STEER);
  }

  // Throttle the update rate
  if (DGCgettime() - this->updateTime < (uint64_t) (1e6 / this->options.rate_arg))
    return 0;
  this->updateTime = DGCgettime();

  this->updateInterval = DGCgettime();
  
  // Generate the sub-graph from the current vehicle state to the static graph
  time = DGCgettime();
  if (this->planner->genVehicleSubGraph(&this->vehState, &this->actState) != 0)
    return ERROR("failed creating vehicle subgraph");
  MSG("vehicle %lldms", (DGCgettime() - time) / 1000);

  // Update c-space
  time = DGCgettime();
  this->configSpace->update(&this->vehState, &this->map,
                            this->options.force_obs_flag, this->options.force_cars_flag);
  MSG("cspace %lldms", (DGCgettime() - time) / 1000);

  // Update goal
  this->goalId = 0;
  if (this->options.mdf_given)
  {
    // Pick from the mdf list
    if (this->options.loop_arg > 0)
      this->goalIndex = this->goalIndex % this->mdf.getNumCheckpoints();
    if (this->goalIndex < (int) this->mdf.getNumCheckpoints())
      this->goalId = this->mdf.getCheckpoint(this->goalIndex);
  }
  else if (this->options.goal_given)
  {
    // Pick from the goal list
    if (this->options.loop_arg > 0)
      this->goalIndex = this->goalIndex % this->options.goal_given;
    if (this->goalIndex < (int) this->options.goal_given)
      this->goalId = this->options.goal_arg[this->goalIndex];
  }
   
  if (this->goalId > 0)
  {
    // Make a plan for getting to the goal
    time = DGCgettime();
    this->planner->makePlan(this->goalId); 
    MSG("plan %lldms", (DGCgettime() - time) / 1000);

    // Get the path to the goal, if there is one
    this->planner->makePath(&this->path);

    // Create a trajectory from the path.  This includes the velocity
    // profile.
    time = DGCgettime();
    this->trajPlanner->genSpeedProfile(&this->path, &this->vehState, &this->actState);
    this->trajPlanner->genTraj(&this->path, &this->vehState, &this->actState, this->traj);
    MSG("traj %lldms", (DGCgettime() - time) / 1000);

    // Pick the turn signal state
    this->trajPlanner->genTurnSignal(&this->path, &this->vehState, &this->signalState);    
  }
  else
  {
    // No goal so send empty trajectory
    memset(&this->path, 0, sizeof(this->path));
    this->traj->startDataInput();    
  }
  
  if (this->mode == modeLive)
  {
    int trajSocket;

    // TODO clean up the truly awful TrajTalker.
    // Get the socket id for sending messages.
    trajSocket = this->trajTalker->m_skynet.get_send_sock(SNtraj);

    // Send trajectory data to follower
    assert(this->trajTalker);
    this->trajTalker->SendTraj(trajSocket, this->traj);

    // Generate the command to set the turn signals.
    if (this->options.enable_signals_flag && !this->options.asim_hacks_flag)
    {
      if (this->lastSignalState != this->signalState)
      {
        this->turnSender->setTurnSignal(this->signalState);
        this->lastSignalState = this->signalState;
      }
    }
  }

  // If we are in run mode, do some logic to see if we should switch
  // to the next goal.
  if (this->actState.m_estoppos == 2)
  {
    // Record when we last got a valid path (no collisions)
    if (this->path.collideObs < GRAPH_COLLIDE_MAP_OUTER)
      this->validPathTime = this->vehState.timestamp;
    
    // If there is no path to this goal, skip to the next
    if (!this->path.valid)
    {
      MSG("invalid path; skipping %d CP %d", this->goalIndex, this->goalId);
      this->goalIndex += 1;      
    }

    // If we can't reach the goal for 10 seconds, skip to the next
    else if (this->vehState.timestamp - this->validPathTime > 10 * 1000000) // MAGIC
    {
      MSG("obstacle in path; skipping %d CP %d", this->goalIndex, this->goalId);
      this->goalIndex += 1;      
    }
            
    // If we are near the current goal (measured along the path), we
    // can go to the next.
    else if (this->path.goalDist < 4.0) // MAGIC
    {
      MSG("arrived at goal (path) %d CP %d", this->goalIndex, this->goalId);
      this->goalIndex += 1;      
    }
  
    // If we are near the current goal (euclidean distance),
    // we can go to the next
    double dx, dy, dd;
    GraphNode *goal;
    goal = this->graph->getNodeFromCheckpointId(this->goalId);
    if (goal)
    {
      dx = goal->pose.pos.x - this->vehState.utmNorthing;
      dy = goal->pose.pos.y - this->vehState.utmEasting;
      dd = sqrt(dx * dx + dy * dy);
      if (dd < 2.0) // MAGIC
      {
        MSG("arrived at goal (euclidean) %d CP %d", this->goalIndex, this->goalId);
        this->goalIndex += 1;            
      }
    }
  }

  // Track total time to process this update
  this->updateInterval = DGCgettime() - this->updateInterval;
  
  // Keep track of how many times through the update cycle
  this->updateCount++;
    
  // Stuff has changed, so we need to redraw
  this->dirty = true;
  
  return 0;
}




int main(int argc, char *argv[])
{
  App *app;

  // Initialize GLUT calls
  glutInit(&argc, argv);

  app = new App();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  //if (app->initGUI(800, 600 + 30) != 0)
  if (app->initGUI(1024, 768 + 30) != 0)
    return -1;

  // Initialize live mode
  if (app->mode == App::modeLive)
    if (app->initLive() != 0)
      return -1;

  // Initialize algorithms
  if (app->init() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) App::onIdle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  app->finiGUI();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
