
/* 
 * Desc: Plan trajectories
 * Date: 25 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef TRAJ_PLANNER_HH
#define TRAJ_PLANNER_HH

#include <stdint.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <rndf/MDF.hh>

#include "Graph.hh"
#include "GraphPath.hh"


// Forward declarations
class CTraj;


/// @brief Vehicle kinematic properties.
struct TrajPlannerKinematics
{
  /// Distance between front and rear wheels (m).
  double wheelBase;

  /// Maximum steering angle (degrees).  This is the largest angle
  /// that the lowel-level will command.  This value is also used to
  /// scale the measured steering position in ActuatorState (which is
  /// in the domain [-1, +1]) to a steering angle.
  double maxSteer;
};


/// @brief General speed limits.
struct TrajPlannerSpeeds
{  
  /// Max speed for driving in lanes (m/s).
  double maxLaneSpeed;

  /// Max speed for taking turns (m/s).
  double maxTurnSpeed;

  /// Max speed for driving in close proximity to obstacles (m/s).
  double maxNearSpeed;

  /// Max speed for driving off-road (m/s).
  double maxOffRoadSpeed;

  /// Speed for crossing stop lines (m/s).
  double maxStopSpeed;

  /// Maximum acceleration (m/s/s).
  double maxAccel;

  /// Maximum deceleration (m/s/s).
  double maxDecel;

  /// Speed increment over the current speed (m/s).  This is a hacky way of generating
  /// a ramp-up from the current speed.
  double deltaSpeed;

  /// Stop line distance (m).  This is the desired distance between the stop
  /// lane and Alice's rear axel.
  double stopDist;

  /// Stop line tolerance (m).  This is the size of the stopping region in which
  /// Alice will plan to stop.
  double stopTol;

  /// Stop line stopping time (sec).
  double stopTime;

  /// Distance at which to start signalling turns.
  double signalDist;
};


/// @brief Trajectory planner
class TrajPlanner
{
  public:

  // Default constructor
  TrajPlanner(Graph* graph);

  // Destructor
  virtual ~TrajPlanner();

  private:
  
  // Global graph
  Graph *graph;

  public:

  /// @brief Get the vehicle kinematics
  ///
  /// @param[out] kin Current vehicle kinematic values.
  int getKinematics(TrajPlannerKinematics *kin);

  /// @brief Set the vehicle kinematics
  ///
  /// @param[int] kin Current vehicle kinematic values.
  int setKinematics(const TrajPlannerKinematics *kin);

  private:

  // Vehicle kinematic properties
  TrajPlannerKinematics kin;

  public:
  
  /// @brief Get the speed limits.
  ///
  /// @param[out] speeds Current speed limits.
  int getSpeeds(TrajPlannerSpeeds *speeds);

  /// @brief Set the speed limits.
  ///
  /// @param[in] speeds New speed limits.
  int setSpeeds(const TrajPlannerSpeeds *speeds);

  public:
  
  // Initialize speed limits from an MDF file.
  // This can be called multiple times to change the speed limits.
  int loadMDF(const char *filename);

  private:

  // MDF containing speed limits
  MDF mdf;

  private:

  // Speed limits
  TrajPlannerSpeeds speeds;
  
  public:

  /// @brief Generate the speed profile.
  /// @param[in,out] path Planned path; this function fills out the speed array.
  /// @param[in] vehState Current vehicle state data.
  /// @param[in] actState Current actuator state data.
  /// @return Returns zero on success and non-zero on error.
  int genSpeedProfile(GraphPath *path,
                      const VehicleState *vehState, const ActuatorState *actState);

  /// @brief Generate a vehicle trajectory.
  int genTraj(const GraphPath *path,
              const VehicleState *vehState, const ActuatorState *actState,
              CTraj *traj);

  private:

  // Nodes with stop signs
  int numStopNodes;
  GraphNode *stopNodes[64];

  // TESTING
  public:
  
  // Are we going stopped or planning to stop at a stop line?
  int stopState;

  // Time when we were last set the state.
  uint64_t stopTime;

  public:

  /// @brief Compute turn signal state
  /// @param[out] signal Signal state (-1, 0, +1).
  int genTurnSignal(const GraphPath *path, const VehicleState *vehState, int *signal);

  // Turn signal state (-1, 0, +1);
  int signalState;

  // Time when we last set the state
  uint64_t signalTimestamp;
  
};



#endif
