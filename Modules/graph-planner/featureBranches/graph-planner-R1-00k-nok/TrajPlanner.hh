
/* 
 * Desc: Plan trajectories
 * Date: 25 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef TRAJ_PLANNER_HH
#define TRAJ_PLANNER_HH

#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <rndf/MDF.hh>

#include "Graph.hh"
#include "GraphPath.hh"


// Forward declarations
class CTraj;


/// @brief Vehicle kinematic properties.
struct TrajPlannerKinematics
{
  /// Distance between front and rear wheels (m).
  double wheelBase;

  /// Maximum steering angle (degrees).  This is the largest angle
  /// that the lowel-level will command.  This value is also used to
  /// scale the measured steering position in ActuatorState (which is
  /// in the domain [-1, +1]) to a steering angle.
  double maxSteer;
};


/// @brief General speed limits.
struct TrajPlannerSpeeds
{  
  /// Max speed for driving in lanes (m/s).
  double maxLaneSpeed;

  /// Max speed for taking turns (m/s).
  double maxTurnSpeed;

  /// Max speed for driving in close proximity to obstacles (m/s).
  double maxNearSpeed;

  /// Max speed for driving off-road (m/s).
  double maxOffRoadSpeed;

  /// Speed for crossing stop lines (m/s).
  double maxStopSpeed;

  /// Maximum acceleration (m/s/s).
  double maxAccel;

  /// Maximum deceleration (m/s/s).
  double maxDecel;
};


/// @brief Trajectory planner
class TrajPlanner
{
  public:

  // Default constructor
  TrajPlanner(Graph* graph);

  // Destructor
  virtual ~TrajPlanner();

  private:
  
  // Global graph
  Graph *graph;

  public:

  /// @brief Get the vehicle kinematics
  ///
  /// @param[out] kin Current vehicle kinematic values.
  int getKinematics(TrajPlannerKinematics *kin);

  /// @brief Set the vehicle kinematics
  ///
  /// @param[int] kin Current vehicle kinematic values.
  int setKinematics(const TrajPlannerKinematics *kin);

  private:

  // Vehicle kinematic properties
  TrajPlannerKinematics kin;

  public:
  
  /// @brief Get the speed limits.
  ///
  /// @param[out] speeds Current speed limits.
  int getSpeeds(TrajPlannerSpeeds *speeds);

  /// @brief Set the speed limits.
  ///
  /// @param[in] speeds New speed limits.
  int setSpeeds(const TrajPlannerSpeeds *speeds);

  public:
  
  // Initialize speed limits from an MDF file.
  // This can be called multiple times to change the speed limits.
  int loadMDF(const char *filename);

  private:

  // MDF containing speed limits
  MDF mdf;

  private:

  // Speed limits
  TrajPlannerSpeeds speeds;
  
  public:

  /// @brief Generate the speed profile.
  /// @param[in,out] path Planned path; this function fills out the speed array.
  /// @return Returns zero on success and non-zero on error.
  int genSpeedProfile(GraphPath *path);

  /// @brief Generate a vehicle trajectory.
  int genTraj(const GraphPath *path,
              const VehicleState *vehicleState,
              const ActuatorState *actuatorState,
              CTraj *traj);

#ifdef USE_OLD_TRAJ
  private:
  
  // Assign a speed based on the local context
  double selectSpeed(const GraphPath *path, const GraphNode *node);
#endif

  public:

  /// @brief Compute turn signal state
  /// @param[out] signal Signal state (-1, 0, +1).
  int genTurnSignal(const GraphPath *path, int *signal);
  
};



#endif
