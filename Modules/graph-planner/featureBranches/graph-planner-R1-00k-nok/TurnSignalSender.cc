
/*!
 * \file TurnSignalSender.cc
 * \brief Interface code for turn signals.
 *
 * \author Richard M. Murray (adapted by Andrew H).
 * \date 3 Jun 2007
 *
 */

#include "TurnSignalSender.hh"


/*! Constructor */
TurnSignalSender::TurnSignalSender(int skynet_key)
{
  m_logger = new GcModuleLogger("TurnSignalSender");
  m_turnHandler = new GcPortHandler();
  m_adriveSF = AdriveCommand::
    generateSouthface(skynet_key, m_turnHandler, m_logger);
  id = 0;
}


/*! Set the turn signals in a given direction */
int TurnSignalSender::setTurnSignal(double dir)
{
  /* Generate the command to set the turn signals */
  AdriveDirective turnCommand;
  turnCommand.id = id++;
  turnCommand.command = SetPosition;
  turnCommand.actuator = TurnSignal;
  turnCommand.arg = dir;

  /* Send the directive to adrive */
  m_adriveSF->sendDirective(&turnCommand);
  m_turnHandler->pumpPorts();

  /* Wait for the return message */
  for (int i = 0; i < 2; ++i) {
    if (m_adriveSF->haveNewStatus()) {
      /* Get the response from adrive */
      AdriveResponse *turnResponse = m_adriveSF->getLatestStatus();
  
      /* Set return status based on whether we completed or not */
      return 
        turnResponse->status == GcInterfaceDirectiveStatus::COMPLETED ? 1 : 0;
    }

    /* Wait for a bit and then grab new inputs */
    DGCusleep(100000);		// MAGIC
    m_turnHandler->pumpPorts();
  }

  /* Didn't receive a response */
  return -1;
}


/*! Get the current turn signal direction */
int TurnSignalSender::getTurnSignal()
{
  /* Not implemented */
  return -1;
}

