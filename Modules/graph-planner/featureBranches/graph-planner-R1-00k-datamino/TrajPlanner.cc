
/* 
 * Desc: Graph-based planner; functions for local trajectory generation
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>

#include <frames/pose3.h>
#include <trajutils/maneuver.h>
#include <trajutils/man2traj.hh>
#include <trajutils/traj.hh>

#define USE_NEW_TRAJ

#include "TrajPlanner.hh"


#define MIN(a,b) ((a) < (b) ? (a) : (b))

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
TrajPlanner::TrajPlanner(Graph* graph)
{
  this->graph = graph;

  this->speeds.maxLaneSpeed = 4.0;
  this->speeds.maxTurnSpeed = 2.0;
  this->speeds.maxNearSpeed = 1.0;
  this->speeds.maxOffRoadSpeed = 1.0;
  this->speeds.maxStopSpeed = 0.5;
  this->speeds.maxAccel = +1.0;
  this->speeds.maxDecel = -1.0;
  
  return;
}


// Destructor
TrajPlanner::~TrajPlanner()
{
  return;
}


// Get the vehicle kinematics
int TrajPlanner::getKinematics(TrajPlannerKinematics *kin)
{
  *kin = this->kin;
  return 0;
}


// Current vehicle kinematic values.
int TrajPlanner::setKinematics(const TrajPlannerKinematics *kin)
{
  this->kin = *kin;
  return 0;
}


// Get the speed limits.
int TrajPlanner::getSpeeds(TrajPlannerSpeeds *speeds)
{
  *speeds = this->speeds;
  return 0;
}


// Set the speed limits.
int TrajPlanner::setSpeeds(const TrajPlannerSpeeds *speeds)
{
  this->speeds = *speeds;
  return 0;
}


// Initialize speed limits from an MDF file.
int TrajPlanner::loadMDF(const char *filename)
{
  MSG("loading %s", filename);
  if (this->mdf.loadFile(filename) != 0)
    return ERROR("failed loading MDF");
  
  return 0;
}


// Generate the speed profile
int TrajPlanner::genSpeedProfile(GraphPath *path)
{
  int i;
  double stopDist;

  // If there is no path, there is nothing to do
  if (path->pathLen <= 0)
    return 0;

  // Zeroth pass: walk along the path and construct the distance
  // profile.
  path->dists[0] = 0;
  for (i = 1; i < path->pathLen; i++) 
  {
    GraphNode *nodeA, *nodeB;
    double dx, dy, dd;
    nodeA = path->path[i - 1];
    nodeB = path->path[i + 0];
    dx = nodeB->pose.pos.x - nodeA->pose.pos.x;
    dy = nodeB->pose.pos.y - nodeA->pose.pos.y;
    dd = sqrt(dx * dx + dy * dy);
    assert(dd > 0);
    path->dists[i] = path->dists[i - 1] + dd;
  }
    
  // First pass: walk along the path and construct the max speed
  // profile.
  for (i = 0; i < path->pathLen; i++) 
  {
    GraphNode *node;
    double speed;
      
    node = path->path[i];
    speed = this->speeds.maxLaneSpeed;

    // Check for corridor/lane speeds
    if (node->segmentId > 0)
    {
      // If driving in a segment, consider the maximum speed for this
      // segment.  The miminum speed is currently ignored.
      double minSpeed, maxSpeed;
      this->mdf.getSpeedLimits(node->segmentId, &minSpeed, &maxSpeed);
      if (maxSpeed > 0)
        speed = MIN(speed, maxSpeed);
    }
    else
    {
      // Are we driving through an intersection?  If so, use the
      // intersection speed.
      speed = MIN(speed, this->speeds.maxTurnSpeed);
    }

    // Check for collisions with obstacles
    if (node->collideObs == 1)
      speed = MIN(speed, this->speeds.maxNearSpeed);
    if (node->collideObs == 2)
      speed = 0;

    // Check for collisions with cars
    if (node->collideCar == 1)
      speed = 0; // TESTING MIN(speed, this->speeds.maxNearSpeed);
    if (node->collideCar == 2)
      speed = 0;

    // Check for the goal
    if (node->planCost < 500) // MAGIC
      speed = 0;

    path->speeds[i] = speed;
  }

  // Second pass: walk backwards and assign slow-down regions near
  // stop signs
  stopDist = DBL_MAX;
  for (i = path->pathLen - 1; i >= 0; i--)
  {
    GraphNode *node;
    node = path->path[i];
    if (node->isExit && node->isStop)
      stopDist = path->dists[i];
    if (stopDist - path->dists[i] >= 2 && stopDist - path->dists[i] < 6) // MAGIC
      path->speeds[i] = MIN(path->speeds[i], this->speeds.maxStopSpeed);
  }

  // Smooth out the accelerations
  for (i = 0; i < path->pathLen - 1; i++)
  {
    double d0, d1, s0, s1, acc, speed;
    
    d0 = path->dists[i + 0];
    d1 = path->dists[i + 1];    
    s0 = path->speeds[i + 0];    
    s1 = path->speeds[i + 1];

    assert(d1 > d0);
    
    // Compute acceleration; if it is high, trim the speed
    acc = (s1 * s1 - s0 * s0) / (d1 - d0) / 2;
    if (acc > this->speeds.maxAccel)
    {
      acc = this->speeds.maxAccel;
      speed = sqrt(2 * acc * (d1 - d0) + s0 * s0);
      //MSG("%f %f %f   %f %f", d1 - d0, s1 - s0, s1, acc, speed);
      assert(speed <= s1);
      path->speeds[i + 1] = MIN(path->speeds[i + 1], speed);
    }
  }

  // Smooth out the deccelerations
  for (i = path->pathLen - 1; i > 0; i--)
  {
    double d0, d1, s0, s1, dec, speed;
    
    d0 = path->dists[i + 0];
    d1 = path->dists[i - 1];    
    s0 = path->speeds[i + 0];    
    s1 = path->speeds[i - 1];

    assert(d1 < d0);
    
    // Compute acceleration; if it is high, trim the speed
    dec = (s1 * s1 - s0 * s0) / (d1 - d0) / 2;
    if (dec < this->speeds.maxDecel)
    {
      dec = this->speeds.maxDecel;
      speed = sqrt(2 * dec * (d1 - d0) + s0 * s0);
      //MSG("%f %f %f   %f %f", d1 - d0, s1 - s0, s1, dec, speed);
      assert(speed <= s1);
      path->speeds[i - 1] = MIN(path->speeds[i - 1], speed);
    }
  }

  return 0;
}


#ifdef USE_NEW_TRAJ
// Generate a vehicle trajectory.
int TrajPlanner::genTraj(const GraphPath *path,
                         const VehicleState *vehicleState,
                         const ActuatorState *actuatorState,                           
                         CTraj *traj)
{
  int i, numSteps, numSpeeds;
  double roll, pitch, yaw;
  Vehicle *vp;
  Maneuver *maneuver;
  GraphNode *nodeA, *nodeB;
  double speeds[2], dists[2];
  VehicleConfiguration configA;  
  Pose2D poseA, poseB;
  double stepSize;

  // Spacing between trajectory points
  stepSize = 0.05; // MAGIC

  // Make sure we have a path
  if (path->pathLen < 2)
  {
    MSG("no valid destination, stopping vehicle");
    traj->startDataInput();
    traj->addPoint(vehicleState->utmNorthing, 0, 0,
                   vehicleState->utmEasting, 0, 0);
    return 0;
  }

  assert(path->pathLen >= 1);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->kin.wheelBase, this->kin.maxSteer);
      
  traj->startDataInput();

  // Start with the vehicle node
  nodeA = path->path[0];
  speeds[0] = path->speeds[0];
  dists[0] = path->dists[0];

  // Look for the first non-volatile node; this is the destination for the
  // first maneuver
  nodeB = NULL;
  for (i = 1; i < path->pathLen; i++)
  {
    nodeB = path->path[i];
    speeds[1] = path->speeds[i];
    dists[1] = path->dists[i];
    if (nodeB->type != GRAPH_NODE_VOLATILE)
      break;
  }
  assert(nodeB != NULL);

  // Compute the number of speeds to consider along this maneuver.
  numSpeeds = i + 1;
    
  // Compute the number of interpolation steps, based on the path
  // distance between the two nodes.
  numSteps = (int) ((dists[1] - dists[0]) / stepSize) + 1;

  // Use the initial configuration given by the state.
  // This causes oscillations.
  //configA.x = vehicleState->utmNorthing;
  //configA.y = vehicleState->utmEasting;
  //configA.theta = vehicleState->utmYaw;
  //configA.phi = actuatorState->m_steerpos * this->kin.maxSteer;
  //speedA = vehicleState->vehSpeed;

  // Use the initial configuration given by the node.
  // This produces stable behavior at low speeds
  quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = yaw;
  configA.phi = nodeA->steerAngle;

  quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
  poseB.x = nodeB->pose.pos.x;
  poseB.y = nodeB->pose.pos.y;
  poseB.theta = yaw;
    
  // Generate a trajectory from the maneuver.
  maneuver = maneuver_config2pose(vp, &configA, &poseB);
  maneuver_profile_single(vp, maneuver, numSpeeds, path->speeds, numSteps, traj);
  maneuver_free(maneuver);
  
  // Create the rest of the maneuvers
  for (; i < path->pathLen - 1; i++) 
  {
    nodeA = path->path[i + 0];
    nodeB = path->path[i + 1];

    speeds[0] = path->speeds[i + 0];
    speeds[1] = path->speeds[i + 1];

    dists[0] = path->dists[i + 0];
    dists[1] = path->dists[i + 1];
          
    // Compute the number of interpolation steps, based on the path
    // distance between the two nodes.
    numSteps = (int) ((dists[1] - dists[0]) / stepSize) + 1;

    // For non-vehicle nodes, use the node pose and assume zero-steering angle
    quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
    poseA.x = nodeA->pose.pos.x;
    poseA.y = nodeA->pose.pos.y;
    poseA.theta = yaw;

    // Final pose on node B
    quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
    poseB.x = nodeB->pose.pos.x;
    poseB.y = nodeB->pose.pos.y;
    poseB.theta = yaw;

    // Generate a trajectory from the maneuver list.
    maneuver = maneuver_pose2pose(vp, &poseA, &poseB);
    maneuver_profile_generate(vp, 1, &maneuver, speeds, numSteps, traj);
    maneuver_free(maneuver);
 
    // Dont plan too far into the future
    if (dists[1] > 20.0) // MAGIC
      break;
  }

  return 0;
}
#endif


#ifdef USE_OLD_TRAJ
// Generate a vehicle trajectory.
int TrajPlanner::genTraj(const GraphPath *path,
                         const VehicleState *vehicleState,
                         const ActuatorState *actuatorState,                           
                         CTraj *traj)
{
  int i;
  double roll, pitch, yaw;
  Vehicle *vp;
  int numManeuvers;
  Maneuver *maneuvers[64];
  double speeds[64 + 1];
  GraphNode *nodeA, *nodeB;
  VehicleConfiguration configA;  
  Pose2D poseB;
  double speedA, speedB;
  bool collide;
  
  assert(path->pathLen >= 1);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->kin.wheelBase, this->kin.maxSteer);
      
  // Use the initial configuration given by the state.
  // This causes oscillations.
  //configA.x = vehicleState->utmNorthing;
  //configA.y = vehicleState->utmEasting;
  //configA.theta = vehicleState->utmYaw;
  //configA.phi = actuatorState->m_steerpos * this->kin.maxSteer;
  //speedA = vehicleState->vehSpeed;

  // Use the initial configuration given by the node.
  // This produces more stable behavior.
  nodeA = path->path[0];
  quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = yaw;
  configA.phi = nodeA->steerAngle;
  speedA = this->selectSpeed(path, nodeA);

  nodeB = NULL;

  collide = false;
  numManeuvers = 0;
    
  for (i = 1; i < path->pathLen && i < 40; i++) // MAGIC
  {
    nodeB = path->path[i];

    // Everything after a collision will have zero speed,
    // so record the fact here.
    if (nodeB->collideObs > 1 || nodeB->collideCar > 1)
      collide = true;

    // Look for a node at which to terminate the maneuver:
    //  - when the type changes
    //  - when the collision state changes
    if (nodeB->type != nodeA->type ||
        nodeB->collideObs != nodeA->collideObs ||
        nodeB->collideCar != nodeA->collideCar)
    {
      // We can get weird trajectories if the start and end nodes are very close,
      // so this forces them apart.  This is a workaround and should be replaced.
      if (vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos)) > 1) // MAGIC
      {
        quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
        poseB.x = nodeB->pose.pos.x;
        poseB.y = nodeB->pose.pos.y;
        poseB.theta = yaw;
        speedB = this->selectSpeed(path, nodeB);

        // Create maneuver
        assert((size_t) numManeuvers < sizeof(maneuvers)/sizeof(maneuvers[0]));
        maneuvers[numManeuvers] = maneuver_config2pose(vp, &configA, &poseB);
        speeds[numManeuvers + 0] = speedA;
        speeds[numManeuvers + 1] = speedB;
        numManeuvers++;

        configA.x = poseB.x;
        configA.y = poseB.y;
        configA.theta = poseB.theta;
        configA.phi = 0;
        speedA = speedB;
        nodeA = nodeB;
      }
    }
  }
  
  // Add final maneuver
  if (nodeB && nodeB != nodeA)
  {
    // We can get weird trajectories if the start and end nodes are very close,
    // so this forces them apart.  This is a workaround and should be replaced.
    if (vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos)) > 1) // MAGIC
    {
      quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
      poseB.x = nodeB->pose.pos.x;
      poseB.y = nodeB->pose.pos.y;
      poseB.theta = yaw;
      speedB = this->selectSpeed(path, nodeB);

      // Create maneuver
      assert((size_t) numManeuvers < sizeof(maneuvers)/sizeof(maneuvers[0]));
      maneuvers[numManeuvers] = maneuver_config2pose(vp, &configA, &poseB);
      speeds[numManeuvers + 0] = speedA;
      speeds[numManeuvers + 1] = speedB;
      numManeuvers++;
    }
  }

  if (numManeuvers == 0)
  {
    MSG("no valid destination, stopping vehicle");
    traj->startDataInput();
    traj->addPoint(nodeA->pose.pos.x, 0, 0,
                   nodeA->pose.pos.y, 0, 0);
    return 0;
  }
  
  // Generate a trajectory from the maneuver list.
  traj->startDataInput();
  maneuver_profile_generate(vp, numManeuvers, maneuvers, speeds, 50, traj); // MAGIC
  
  //traj->print(cout);
  //traj->printSpeedProfile(cout);
  //fprintf(stdout, "\n");  
    
  // Clean up
  while (numManeuvers > 0)
    maneuver_free(maneuvers[--numManeuvers]);
  free(vp);
  
  return 0;
}


// Assign a speed based on the local context
double TrajPlanner::selectSpeed(const GraphPath *path, const GraphNode *node)
{
  double speed;

  speed = this->speeds.maxLaneSpeed;
  
  if (node->segmentId == 0)
  {
    // Are we driving through an intersection?  If so, use the
    // intersection speed.
    speed = MIN(speed, this->speeds.maxTurnSpeed);
  }
  else
  {    
    // If driving in a segment, consider the maximum speed for this
    // segment.  The miminum speed is currently ignored.
    double minSpeed, maxSpeed;
    this->mdf.getSpeedLimits(node->segmentId, &minSpeed, &maxSpeed);
    if (maxSpeed > 0)
      speed = MIN(speed, maxSpeed);
  }

  // TODO: fix off-road test (lane-changes are incorrectly listed as off-road)
  // Are we driving off-road?
  //if (node->offRoad)
  //  speed = MIN(speed, this->speeds.maxOffRoadSpeed);
  
  // Are we in collision or near-collision
  if (node->collideObs > 1 || node->collideCar > 1)
    speed = 0;
  else if (node->collideObs > 0 || node->collideCar > 0)
    speed = MIN(speed, this->speeds.maxNearSpeed);
    
  // Are we at the goal?
  if (node->planCost < 500) // MAGIC
    speed = 0;

  // Are we approaching a stop line?
  if (true)
  {
    int i, j;
    GraphNode *nodeA, *nodeB;

    // Find the node in the path
    nodeA = NULL;
    for (i = 0; i < path->pathLen; i++)
    {
      nodeA = path->path[i];
      if (nodeA == node)
        break;      
    }
    assert(nodeA);

    // Skip ahead to find the stop line; we drive slowing if
    // we are in the stopping "zone" for a California rolling stop.
    for (j = i + 7; j < i + 7 + 2 && j < path->pathLen; j++) // MAGIC HACK
    {
      nodeB = path->path[j];
      if (nodeA->segmentId > 0 && nodeB->isExit && nodeB->isStop)
        speed = MIN(speed, this->speeds.maxStopSpeed);
    }
  }

  return speed;
}

#endif


// Compute turn signal state
int TrajPlanner::genTurnSignal(const GraphPath *path, int *signal)
{
  int i;
  double px, py, ax, ay, theta;
  GraphNode *nodeA, *nodeB;
  bool change;
  
  if (path->pathLen <= 0)
  {
    *signal = 0;
    return 0;
  }
  
  // Get the current lane
  nodeA = path->path[0];
  
  // Walk forward to see if we are leaving our current lane.
  for (i = 0; i < path->pathLen; i++)
  {
    nodeB = path->path[i];

    // If we have going to be in the same lane for a while, there is
    // no need to set the turn signal.
    if (nodeA->segmentId > 0 &&
        nodeB->segmentId == nodeA->segmentId &&
        nodeB->laneId == nodeA->laneId &&
        path->dists[i] > 20) // MAGIC
      break;

    change = false;
    
    // If we are changing lanes or segments...
    change |= (nodeB->segmentId > 0 &&
               (nodeB->segmentId != nodeA->segmentId || nodeB->laneId != nodeA->laneId));

    // If we are moving to a distant part of the same segment
    // TODO
    
    // If we are changing lanes or segments...
    if (change)
    {
      px = nodeB->pose.pos.x;
      py = nodeB->pose.pos.y;     
      ax = nodeA->transNG[0][0] * px + nodeA->transNG[0][1] * py + nodeA->transNG[0][3];
      ay = nodeA->transNG[1][0] * px + nodeA->transNG[1][1] * py + nodeA->transNG[1][3];
      theta = atan2(ay, ax);

      MSG("nodes %d %d : %d %d : %f",
          nodeA->segmentId, nodeA->laneId,
          nodeB->segmentId, nodeB->laneId, theta);
      
      if (theta < -M_PI/4)
        *signal = -1;
      else if (theta > +M_PI/4)
        *signal = +1;
      else
        *signal = 0;
      return 0;
    }
  }

  // Reset the signal
  *signal = 0;
  
  return 0;
}
