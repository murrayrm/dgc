
/* 
 * Desc: Graph-based planner; functions for local trajectory generation
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <frames/pose3.h>
#include <trajutils/maneuver.h>
#include <trajutils/man2traj.hh>
#include <trajutils/traj.hh>

#include "GraphPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// Generate a vehicle trajectory.
int GraphPlanner::makeTraj(const VehicleState *vehicleState,
                           const ActuatorState *actuatorState,                           
                           GraphPath *path, double maxSpeed, CTraj *traj)
{
  int i;
  double roll, pitch, yaw;
  Vehicle *vp;
  int numManeuvers;
  Maneuver *maneuvers[64];
  double speeds[64 + 1];
  GraphNode *nodeA, *nodeB;
  VehicleConfiguration configA;  
  Pose2D poseB;
  double speedA, speedB;
  bool collide;

  assert(path->pathLen >= 1);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->kin.wheelBase, this->kin.maxSteer);
      
  // Use the initial configuration given by the state.
  // This causes oscillations.
  //configA.x = vehicleState->utmNorthing;
  //configA.y = vehicleState->utmEasting;
  //configA.theta = vehicleState->utmYaw;
  //configA.phi = actuatorState->m_steerpos * this->kin.maxSteer;
  //speedA = vehicleState->vehSpeed;

  // Use the initial configuration given by the node.
  // This produces more stable behavior.
  nodeA = path->path[0];
  quat_to_rpy(nodeA->poseGlobal.rot, &roll, &pitch, &yaw);
  configA.x = nodeA->poseGlobal.pos.x;
  configA.y = nodeA->poseGlobal.pos.y;
  configA.theta = yaw;
  configA.phi = nodeA->steerAngle;
  speedA = maxSpeed;

  // Check for collisions and zero speed accordingly
  if (nodeA->collideObs || nodeA->collideCar)
    speedA = 0;

  nodeB = NULL;

  collide = false;
  numManeuvers = 0;
    
  for (i = 1; i < path->pathLen && i < 20; i++) // MAGIC
  {
    nodeB = path->path[i];

    // Everything after a collision will have zero speed,
    // so record the fact here.
    if (nodeB->collideObs || nodeB->collideCar)
      collide = true;
    
    if (nodeB->type != nodeA->type ||
        nodeB->collideObs != nodeA->collideObs ||
        nodeB->collideCar != nodeA->collideCar)
    {
      quat_to_rpy(nodeB->poseGlobal.rot, &roll, &pitch, &yaw);
      poseB.x = nodeB->poseGlobal.pos.x;
      poseB.y = nodeB->poseGlobal.pos.y;
      poseB.theta = yaw;
      speedB = maxSpeed;

      // Adjust the speed to prevent collisions
      if (collide)
        speedB = 0;

      // Create maneuver
      assert((size_t) numManeuvers < sizeof(maneuvers)/sizeof(maneuvers[0]));
      maneuvers[numManeuvers] = maneuver_config2pose(vp, &configA, &poseB);
      speeds[numManeuvers + 0] = speedA;
      speeds[numManeuvers + 1] = speedB;
      numManeuvers++;

      configA.x = poseB.x;
      configA.y = poseB.y;
      configA.theta = poseB.theta;
      configA.phi = 0;
      speedA = speedB;
      nodeA = nodeB;
    }
  }
  
  // Add final maneuver
  if (nodeB && nodeB != nodeA)
  {
    quat_to_rpy(nodeB->poseGlobal.rot, &roll, &pitch, &yaw);
    poseB.x = nodeB->poseGlobal.pos.x;
    poseB.y = nodeB->poseGlobal.pos.y;
    poseB.theta = yaw;
    speedB = maxSpeed;

    // Create maneuver
    assert((size_t) numManeuvers < sizeof(maneuvers)/sizeof(maneuvers[0]));
    maneuvers[numManeuvers] = maneuver_config2pose(vp, &configA, &poseB);
    speeds[numManeuvers + 0] = speedA;
    speeds[numManeuvers + 1] = speedB;
    numManeuvers++;
  }

  if (numManeuvers == 0)
  {
#warning "TODO: need to stop the vehicle if there is no path."       
    // TODO : stop Alice!
    MSG("no valid destination, stopping vehicle");
    traj->startDataInput();
    return 0;
  }
  
  // Generate a trajectory from the maneuver list.
  traj->startDataInput();
  maneuver_profile_generate(vp, numManeuvers, maneuvers, speeds, 10, traj); // MAGIC
  
  //traj->print(cout);
  //traj->printSpeedProfile(cout);
  //fprintf(stdout, "\n");  
    
  // Clean up
  while (numManeuvers > 0)
    maneuver_free(maneuvers[--numManeuvers]);
  free(vp);
  
  return 0;
}

