
/* 
 * Desc: Update configuration space
 * Date: 25 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef CONFIG_SPACE_HH
#define CONFIG_SPACE_HH

#include <interfaces/VehicleState.h>

#include "Graph.hh"


/// @brief Vehicle configuration space dimensions.
///
/// All measurements are relative to the vehicle origin, which is
/// usually the middle of the rear axel.
struct ConfigSpaceDimensions
{
  /// Distance to the front of the vehicle (m).
  float front;

  /// Distance to the rear of the vehicle (m).
  float rear;

  /// Distance to the left of the vehicle (m).
  float left;

  /// Distance to the right of the vehicle (m).
  float right;
};


/// @brief Traffic configuration clearance distances.
struct ConfigSpaceClearances
{
  /// Obstacle clearances; front and back
  double obsFront, obsRear;

  /// Car clearances; front and back
  double carFront, carRear;
};


/// @brief Update configuration space.
class ConfigSpace
{
  public:

  // Default constructor
  ConfigSpace(Graph* graph);

  // Destructor
  virtual ~ConfigSpace();

  private:
  
  // Global graph
  Graph *graph;

  public:

  /// @brief Get the vehicle dimensions
  ///
  /// @param[out] dim Current vehicle dimensions values.
  int getDimensions(ConfigSpaceDimensions *dim);

  /// @brief Set the vehicle dimensions
  ///
  /// @param[in] dim Current vehicle kinematic values.
  int setDimensions(const ConfigSpaceDimensions *dim);

  public:

  // Vehicle dimensions
  ConfigSpaceDimensions dim;

  // Inner guard (vehicle frame)
  float innerAx, innerBx, innerAy, innerBy, innerRadius;

  // Outer guard (vehicle frame)
  float outerAx, outerBx, outerAy, outerBy, outerRadius;

  private:
  
  // Traffic clearances
  ConfigSpaceClearances clear;

  public:

  /// @brief Add a fake car to the map (for testing).
  int addFakeCar(const GraphNode *node, const VehicleState *state,
                 Map *map, MapElementType type);

  public:

  /// @brief Update configuration space from the given map.
  int update(const VehicleState *state, Map *map, bool forceObs, bool forceCar);

  private:

  /// @brief Determine which lane elements contain map objects.
  int updateLaneElements(const VehicleState *state, Map *map);
  
  /// @brief Check lane element to see if it contains a map object.
  bool testLaneElement(GraphNode *lanel, MapElement *mapel, double transGL[4][4]);

  public:

  // REMOVE double mapelAx, mapelBx, mapelAy, mapelBy;

  private:
  
  /// @brief Expand occupied lane elements.
  int expandLaneElements(const VehicleState *state, bool forceObs, bool forceCar);

  /// @brief Expand obstacles along the graph.
  int expandObs(GraphNode *lanel, int direction, double dist);
  
  /// @brief Expand cars along the graph.
  int expandCar(GraphNode *lanel, uint64_t carTime, int direction, double dist);

  private:

  /// @brief Update the collision flags against the lanes.
  int updateConfigLanes();
  
  /// @brief  Check a configuration for collision with occupied lane elements.
  /// @returns Returns 0 if no collision, 1 on collision with outer
  /// guard, 2 on collision with inner guard.
  int testConfigLane(GraphNode *node, GraphNode *lanel);

  private:
  
  /// @brief Update the collision flags against the map.
  int updateConfigMap(const VehicleState *state, Map *map, bool forceObs, bool forceCar);

  /// @brief Check a configuration for collision with a map element.
  /// @returns Returns 0 if no collision, 1 on collision with outer
  /// guard, 2 on collision with inner guard.
  int testConfigMap(GraphNode *node, MapElement *mapel, double transGL[4][4]);
};



#endif
