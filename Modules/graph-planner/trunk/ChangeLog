Wed Jun 13 18:19:50 2007	Andrew Howard (ahoward)

	* version R1-00l
	BUGS:  
	New files: start-default.sh start-static.sh start-traffic.sh
	FILES: ConfigSpace.cc(26512), ConfigSpace.hh(26512),
		GraphPlannerGen.cc(26512), GraphPlannerViewer.cc(26512),
		TrajPlanner.cc(26512), TrajPlanner.hh(26512)
	Added state machine for stop lines

	FILES: ConfigSpace.cc(26737)
	Tweaked car propagation for speed

	FILES: ConfigSpace.cc(26836), ConfigSpace.hh(26836),
		Graph.hh(26836), GraphPlannerViewer.cc(26836),
		TrajPlanner.cc(26836), cmdline.ggo(26836)
	Extended collision types for better obs versus lane, inner versus
	outer conditions

	FILES: ConfigSpace.cc(26905), GraphPlannerViewer.cc(26905, 27147),
		TrajPlanner.cc(26905)
	Field tweaks

	FILES: ConfigSpace.cc(26973), ConfigSpace.hh(26973),
		GraphPlannerViewer.cc(26973), TrajPlanner.cc(26973)
	Modified car propagation to go through intersections only

	FILES: ConfigSpace.cc(27314), ConfigSpace.hh(27314)
	Added clearance paramters

	FILES: ConfigSpace.cc(27497), ConfigSpace.hh(27497),
		Graph.hh(27497), GraphPlanner.cc(27497),
		GraphPlanner.hh(27497), GraphPlannerGen.cc(27497),
		TrajPlanner.cc(27497)
	Added a penalty for deviating from the lane center-line; should
	track corners much better now

	FILES: ConfigSpace.cc(27586), ConfigSpace.hh(27586),
		Graph.hh(27586), GraphPlanner.cc(27586),
		GraphPlannerGen.cc(27586), GraphPlannerViewer.cc(27586),
		TrajPlanner.cc(27586), cmdline.ggo(27586)
	Re-wrote configuration space calculations with timestamps; now
	supports automatic car/obstacle classification

	FILES: ConfigSpace.cc(27588), GraphPlannerViewer.cc(27588)
	Fixed a minor bug in bounding box comparisons

	FILES: ConfigSpace.cc(27605), GraphPlannerViewer.cc(27605),
		TrajPlanner.cc(27605), TrajPlanner.hh(27605)
	Added simple intersection precedence handling; will not work for a
	more complicated RNDF

	FILES: ConfigSpace.cc(27628), Graph.cc(27628),
		GraphPlannerGen.cc(27628), GraphPlannerViewer.cc(27628),
		Makefile.yam(27628), TrajPlanner.hh(27628)
	Tidy up

	FILES: ConfigSpace.cc(27769), ConfigSpace.hh(27769)
	Fixed bounding-box check (for real this time)

	FILES: ConfigSpace.cc(27774), ConfigSpace.hh(27774)
	Added clearance distances for obstacles (was a magic number)

	FILES: ConfigSpace.cc(27783)
	Added lane obstacke tolerance hack

	FILES: ConfigSpace.cc(27802), GraphPlannerViewer.cc(27802)
	Merged changes

	FILES: ConfigSpace.cc(27856), GraphPlanner.cc(27856),
		GraphPlannerGen.cc(27856), GraphPlannerViewer.cc(27856),
		TrajPlanner.cc(27856), cmdline.ggo(27856)
	Field hacks

	FILES: GraphPath.hh(27770), GraphPlanner.cc(27770),
		GraphPlannerGen.cc(27770), GraphPlannerViewer.cc(27770),
		TrajPlanner.cc(27770)
	Fixed goal in obstacle case, but only if the force-obs flag is set
	(the more general case requires some thought).

	FILES: GraphPlanner.cc(27857), GraphPlannerViewer.cc(27857),
		TrajPlanner.cc(27857)
	Fixed cost calculation for collisions

	FILES: GraphPlanner.cc(27858), GraphPlanner.hh(27858),
		GraphPlannerViewer.cc(27858), TrajPlanner.cc(27858)
	Improved robustness of goal switching for blocked goals

	FILES: GraphPlannerGen.cc(27028)
	Added discraceful hack for site visit RNDF

	FILES: GraphPlannerGen.cc(27316)
	Added interpolated RNDF id assignment for lane changes

	FILES: GraphPlannerGen.cc(27860), GraphPlannerViewer.cc(27859)
	Added some minor timing diagnostics

	FILES: GraphPlannerViewer.cc(26831), TrajPlanner.cc(26831)
	Improved signal handling for intersections that do not have a
	segment transition

	FILES: GraphPlannerViewer.cc(26974)
	Modified MDF goal selection to including skipping unreachable goals

	FILES: GraphPlannerViewer.cc(27315)
	Removed pause option in live mode

	FILES: GraphPlannerViewer.cc(27496)
	Added some additional diagnostic info

	FILES: TrajPlanner.cc(26736)
	Added initial ramp-up in acceleration

	FILES: TrajPlanner.cc(26753), TrajPlanner.hh(26753)
	Improved turn signal behavior slightly

	FILES: TrajPlanner.cc(26812), TrajPlanner.hh(26812)
	Improved stop-line state machine

	FILES: TrajPlanner.cc(26932)
	Added additional check for stop lines; not tested yet

	FILES: TrajPlanner.cc(27317), TrajPlanner.hh(27317)
	Tweaked speed profile for stop lines and goal; re-wrote turn signal
	logic

	FILES: TrajPlanner.cc(27627)
	Fixed valgrind bug

	FILES: TrajPlanner.cc(27775)
	Tweaked give way code for vehicle in intersection, but the code
	running today *should* have worked

	FILES: TrajPlanner.cc(27881)
	Tweaks

Mon Jun  4 23:41:18 2007	 (ahoward)

	* version R1-00k
	BUGS:  
	New files: TurnSignalSender.cc TurnSignalSender.hh
		santaanita_sitevisit_leftloop.mdf
		santaanita_sitevisit_rightloop.mdf
	FILES: ConfigSpace.cc(26279), GraphPath.hh(26279),
		GraphPlannerViewer.cc(26279), TrajPlanner.cc(26279),
		TrajPlanner.hh(26279)
	Added new speed profile and trajectory generation

	FILES: ConfigSpace.cc(26355), ConfigSpace.hh(26355),
		GraphPlannerViewer.cc(26355), Makefile.yam(26355),
		TrajPlanner.cc(26355), TrajPlanner.hh(26355),
		cmdline.ggo(26355)
	Modified trajectory generation to use new trajutils function; looks
	more stable in simulation

	FILES: GraphPlannerGen.cc(26228)
	Changed lane generator to use maneuvers rather than splines; should
	make it possible to do better lane generation later

	FILES: GraphPlannerViewer.cc(26229), Makefile.yam(26229)
	Added more status info

	FILES: GraphPlannerViewer.cc(26289), Makefile.yam(26289),
		TrajPlanner.cc(26289), TrajPlanner.hh(26289)
	Added support for turn signals

	FILES: GraphPlannerViewer.cc(26290), cmdline.ggo(26290)
	Added flag to enable turn signal directives (all GcModule-derived
	class segfault on my machine)

	FILES: TrajPlanner.cc(26468)
	Fixed a valgrind bug

Sat Jun  2 14:29:02 2007	Andrew Howard (ahoward)

	* version R1-00j
	BUGS:  
	New files: ConfigSpace.cc ConfigSpace.hh TrajPlanner.cc
		TrajPlanner.hh
	FILES: Graph.cc(24893), Graph.hh(24893), GraphPlanner.cc(24893),
		GraphPlanner.hh(24893)
	Partial implementation of planning with driving in reverse

	FILES: Graph.cc(24963), GraphPlanner.cc(24963),
		GraphPlanner.hh(24963), GraphPlannerCSpace.cc(24963),
		GraphPlannerGen.cc(24963), GraphPlannerViewer.cc(24963)
	Seperated moved graph object outside the planner

	FILES: Graph.cc(24997), Graph.hh(24997), GraphPlanner.cc(24997),
		GraphPlanner.hh(24997), GraphPlannerGen.cc(24997),
		GraphPlannerViewer.cc(24997), Makefile.yam(24997)
	Broke out configuration space into seperate class; switched GUI to
	global frame

	FILES: Graph.cc(25021), Graph.hh(25021), GraphPlanner.cc(25021),
		GraphPlanner.hh(25021), GraphPlannerGen.cc(25021),
		GraphPlannerViewer.cc(25021), Makefile.yam(25021)
	Implemented very basic intersection handling (yield to all other
	traffic)

	FILES: Graph.cc(25151), Graph.hh(25151), GraphPlannerGen.cc(25151),
		GraphPlannerViewer.cc(25151)
	Added inner/outer c-space guards to avoid aiming to just miss

	FILES: Graph.cc(25153), Graph.hh(25153), GraphPlanner.cc(25153),
		GraphPlanner.hh(25153), GraphPlannerGen.cc(25153),
		GraphPlannerViewer.cc(25153)
	Added crude off-road test and lane assignment; needs optimization

	FILES: Graph.cc(25157), Graph.hh(25157), GraphPlannerGen.cc(25157),
		GraphPlannerViewer.cc(25157)
	Added grid-based spatial indexing for optimizing

	FILES: Graph.hh(25144), GraphPlanner.cc(25144),
		GraphPlanner.hh(25144), GraphPlannerGen.cc(25144)
	Added K-turn maneuvers, but currently disabled

	FILES: Graph.hh(25149), GraphPlannerGen.cc(25149),
		GraphPlannerViewer.cc(25149)
	Tidied up and optimized configuration space checks for better
	obstacle clearance

	FILES: GraphPlanner.cc(24913), GraphPlannerGen.cc(24913),
		GraphPlannerViewer.cc(24913), Makefile.yam(24913),
		cmdline.ggo(24913)
	Added support for MDFs

	FILES: GraphPlanner.cc(24968), GraphPlanner.hh(24968),
		GraphPlannerViewer.cc(24968), Makefile.yam(24968)
	Broke out trajectory planning into separate class

	FILES: GraphPlanner.cc(25171)
	Tweaks

	FILES: GraphPlannerGen.cc(25929), GraphPlannerViewer.cc(25929)
	Minor display tweaks

	FILES: GraphPlannerViewer.cc(25025), cmdline.ggo(25025)
	Tidied up the GUI for better c-space visualization

	FILES: GraphPlannerViewer.cc(25228)
	Re-enabled max-speed option

	FILES: GraphPlannerViewer.cc(25263)
	Added (hacky) code for rolling stop at stop signs

	FILES: GraphPlannerViewer.cc(25337)
	Default speed tweaks

	FILES: GraphPlannerViewer.cc(26054)
	Minor fixes for gcc 4

Fri May 18 19:16:01 2007	Andrew Howard (ahoward)

	* version R1-00i
	BUGS:  
	FILES: GraphPlannerCSpace.cc(23817), GraphPlannerViewer.cc(23817)
	Minor tweaks for fix map obstacle types

	FILES: santaanita_sitevisit_split.rndf(23842)
	Split RNDF into more parts

Thu May 17 22:31:22 2007	 (ahoward)

	* version R1-00h
	BUGS:  
	New files: santaanita_sitevisit_split.rndf
	FILES: Graph.cc(23625), Graph.hh(23625), GraphPlanner.cc(23625),
		GraphPlanner.hh(23625), GraphPlannerGen.cc(23625),
		GraphPlannerTraj.cc(23625), GraphPlannerViewer.cc(23625)
	Synced up with trunk

	New files: santaanita_sitevisit_split.rndf
	Deleted files: Readme
	FILES: Graph.cc(23100), Graph.hh(23100), GraphPlanner.cc(23100),
		GraphPlanner.hh(23100), GraphPlannerGen.cc(23100),
		GraphPlannerTraj.cc(23100), GraphPlannerViewer.cc(23100)
	Added lane boundaries, overhauled trajectory generation and vehicle
	sub-graph selection

	FILES: Graph.cc(23252), GraphPlannerGen.cc(23252),
		GraphPlannerViewer.cc(23252)
	Optimized arc generation (was slowing down volatile graph
	generation)

	FILES: Graph.hh(23251), GraphPlanner.cc(23251),
		GraphPlannerGen.cc(23251), GraphPlannerTraj.cc(23251),
		GraphPlannerViewer.cc(23251)
	Tweaked trajectory generation for more stable performance; tested
	with asim

	FILES: GraphPlannerCSpace.cc(23411), GraphPlannerViewer.cc(23411)
	Modified for map api changes

	FILES: GraphPlannerGen.cc(23254), GraphPlannerTraj.cc(23254)
	Fixed velocity handling for obstacles at intersections

Mon May 14 16:30:34 2007	Sam Pfister (sam)

	* version R1-00g
	BUGS:  
	FILES: GraphPlannerCSpace.cc(23091), GraphPlannerViewer.cc(23091)
	Updated to work with the new Map interface.

Sun May 13 14:58:28 2007	murray (murray)

	* version R1-00f
	BUGS:  
	FILES: GraphPlannerTraj.cc(22919)
	Andrew's fix for end of maneuver = nan

	FILES: GraphPlannerTraj.cc(22930)
	tweaked debugging output

Sat May 12 10:58:24 2007	murray (murray)

	* version R1-00e
	BUGS:  
	FILES: GraphPlannerViewer.cc(22827), Makefile.yam(22827),
		README(22827)
	added ifdefs to allow compilation on MACOSX

	FILES: stluke_graph_planner1.rndf(22798)
	reset executable mode

Fri May 11 12:49:13 2007	murray (murray)

	* version R1-00d
	BUGS:  
	Deleted files: Readme
Wed May  9 23:58:16 2007	Andrew Howard (ahoward)

	* version R1-00c
	BUGS:  
	FILES: GraphPlanner.cc(22651), GraphPlannerGen.cc(22651)
	Fixed gcc 4 compatibility bugs

Wed May  9 23:26:40 2007	 (ahoward)

	* version R1-00b
	BUGS:  
	FILES: Graph.cc(22370), Graph.hh(22370, 22371), GraphPath.hh(22370,
		22371), GraphPlanner.cc(22370, 22371),
		GraphPlanner.hh(22370, 22371),
		GraphPlannerCSpace.cc(22371), GraphPlannerGen.cc(22370),
		GraphPlannerViewer.cc(22370, 22371), cmdline.ggo(22370)
	Optimized memory storage with linked lists; now has a *much*
	smaller memory footprint

	FILES: Graph.hh(22372), GraphPlanner.cc(22372),
		GraphPlannerTraj.cc(22372), GraphPlannerViewer.cc(22372),
		cmdline.ggo(22372)
	Added basic velocity profile for stopping before collision

	FILES: Graph.hh(22641), GraphPlanner.hh(22641),
		GraphPlannerGen.cc(22641), GraphPlannerTraj.cc(22641),
		GraphPlannerViewer.cc(22641), Makefile.yam(22641)
	Fixed up simulator mode

	FILES: GraphPath.hh(22368), GraphPlanner.cc(22368),
		GraphPlannerGen.cc(22368), GraphPlannerViewer.cc(22368),
		cmdline.ggo(22368)
	Added support for multiple goals

	FILES: GraphPlanner.cc(22478), GraphPlanner.hh(22478),
		GraphPlannerGen.cc(22478), GraphPlannerTraj.cc(22478),
		GraphPlannerViewer.cc(22478)
	Added support for current steer angle, but also some hacks for
	debugging trajectories

	FILES: GraphPlanner.cc(22553), GraphPlanner.hh(22553),
		GraphPlannerCSpace.cc(22553), GraphPlannerGen.cc(22553),
		GraphPlannerTraj.cc(22553), GraphPlannerViewer.cc(22553),
		cmdline.ggo(22553)
	Added interface for plan constraints

	FILES: GraphPlanner.cc(22580), GraphPlannerViewer.cc(22599)
	Tweaks

	FILES: GraphPlanner.cc(22597), GraphPlanner.hh(22597),
		GraphPlannerTraj.cc(22597), GraphPlannerViewer.cc(22597),
		cmdline.ggo(22597)
	Much better trajectory generation

	FILES: GraphPlannerViewer.cc(22643), Makefile.yam(22643)
	Tweaks for release

	FILES: Makefile.yam(22642)
	Create graph planner library

Sun May  6 14:13:23 2007	 (ahoward)

	* version R1-00a
	BUGS:  
	New files: Graph.cc Graph.hh GraphPath.hh GraphPlanner.cc
		GraphPlanner.hh GraphPlannerCSpace.cc GraphPlannerGen.cc
		GraphPlannerTraj.cc GraphPlannerViewer.cc cmdline.ggo
		stluke_bigloop.rndf stluke_graph_planner1.rndf
	FILES: Makefile.yam(21774)
	Basic proof-of-concept of graph generation

	FILES: Makefile.yam(21780)
	Added sketch of static obstacle handling

	FILES: Makefile.yam(21830)
	Integrated with trajutils maneuver generator

	FILES: Makefile.yam(21951)
	Added decent map support; currently limited to point obstacles only

	FILES: Makefile.yam(21973)
	Tweaks

	FILES: Makefile.yam(22060)
	Testing trajectory generation

	FILES: Makefile.yam(22336)
	Removing some hacks for first release

Mon Apr 30 21:59:38 2007	 (ahoward)

	* version R1-00
	Created graph-planner module.














