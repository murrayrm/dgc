
/*!
 * \file TurnSignalSender.hh
 * \brief Interface code for turn signals.
 *
 * \author Richard M. Murray (adapted by Andrew H).
 * \date 3 Jun 2007
 *
 */

#ifndef TURN_SIGNAL_SENDER_HH
#define TURN_SIGNAL_SENDER_HH

#define ACKNOWLEDGE_STL_MAP_ALLOCATOR_ISSUE_

#include "gcmodule/GcInterface.hh"
#include "gcinterfaces/AdriveCommand.hh"

class TurnSignalSender 
{
  GcModuleLogger* m_logger;
  GcPortHandler *m_turnHandler;
  AdriveCommand::Southface *m_adriveSF;
  int id;

public:
  TurnSignalSender(int);
  ~TurnSignalSender() {};
  int setTurnSignal(double);
  int getTurnSignal();
};


#endif
