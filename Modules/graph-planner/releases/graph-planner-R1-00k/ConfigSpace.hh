
/* 
 * Desc: Update configuration space
 * Date: 25 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef CONFIG_SPACE_HH
#define CONFIG_SPACE_HH

#include <interfaces/VehicleState.h>

#include "Graph.hh"


/// @brief Vehicle configuration space dimensions.
///
/// All measurements are relative to the vehicle origin, which is
/// usually the middle of the rear axel.
struct ConfigSpaceDimensions
{
  /// Distance to the front of the vehicle (m).
  float front;

  /// Distance to the rear of the vehicle (m).
  float rear;

  /// Distance to the left of the vehicle (m).
  float left;

  /// Distance to the right of the vehicle (m).
  float right;
};


/// @brief Update configuration space.
class ConfigSpace
{
  public:

  // Default constructor
  ConfigSpace(Graph* graph);

  // Destructor
  virtual ~ConfigSpace();

  private:
  
  // Global graph
  Graph *graph;

  public:

  /// @brief Get the vehicle dimensions
  ///
  /// @param[out] dim Current vehicle dimensions values.
  int getDimensions(ConfigSpaceDimensions *dim);

  /// @brief Set the vehicle dimensions
  ///
  /// @param[in] dim Current vehicle kinematic values.
  int setDimensions(const ConfigSpaceDimensions *dim);

  public:

  // Vehicle dimensions
  ConfigSpaceDimensions dim;

  // Inner guard (vehicle frame)
  float innerAx, innerBx, innerAy, innerBy, innerRadius;

  // Outer guard (vehicle frame)
  float outerAx, outerBx, outerAy, outerBy, outerRadius;

  public:

  /// @brief Add a fake car to the map (for testing).
  int addFakeCar(const GraphNode *node, const VehicleState *state,
                 Map *map, MapElementType type);

  public:

  /// @brief Update segment and lane ids to volatile nodes.
  ///
  /// The segment/lane id is used to penalize excursions outside of
  /// lane boundaries.
  int updateLanes();

  /// @brief Update configuration space from the given map.
  int updateMap(const VehicleState *state, Map *map);

  private:

  /// @brief Check a configuration for collision with a map element.
  /// @returns Returns 0 if no collision, 1 on collision with outer
  /// guard, 2 on collision with inner guard.
  int testConfigMap(GraphNode *node, MapElement *mapel, double transGL[4][4]);

  // Check lane element to see if it contains a map element
  bool testLane(GraphNode *node, MapElement *mapel, double transGL[4][4]);
  
  // Check a configuration for collision with occupied lane elements
  bool testConfigLane(GraphNode *node, GraphNode *lanel);

  // Propagate a car obstacle along the graph
  int propagateCar(GraphNode *node, float dist);
};



#endif
