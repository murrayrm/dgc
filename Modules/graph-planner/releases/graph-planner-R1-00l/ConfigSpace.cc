
/* 
 * Desc: Update configuration space
 * Date: 25 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <float.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <map/Map.hh>

#include "ConfigSpace.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Additional limits
#define UINT64_MAX	18446744073709551615ULL

// Common macros
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))


// Default constructor
ConfigSpace::ConfigSpace(Graph* graph)
{
  this->graph = graph;

  this->dim.front = 4;
  this->dim.rear = 2;
  this->dim.left = 1;
  this->dim.right = 1;
  this->setDimensions(&this->dim);

  this->clear.carFront = 4;
  this->clear.carRear = 8;
  this->clear.obsFront = 6;
  this->clear.obsRear = 8;
  
  return;
}


// Destructor
ConfigSpace::~ConfigSpace()
{
  return;
}


// Get the vehicle dimensions
int ConfigSpace::getDimensions(ConfigSpaceDimensions *dim)
{
  *dim = this->dim;
  return 0;
}


// Set the vehicle dimensions
int ConfigSpace::setDimensions(const ConfigSpaceDimensions *dim)
{
  this->dim = *dim;

  // Outer rectangle dimensions
  this->outerAx = -DIST_REAR_TO_REAR_AXLE - dim->rear;
  this->outerBx = +VEHICLE_LENGTH - DIST_REAR_TO_REAR_AXLE + dim->front;
  this->outerAy = -VEHICLE_WIDTH/2 - dim->left;
  this->outerBy = +VEHICLE_WIDTH/2 + dim->right;
  this->outerRadius = 0;
  this->outerRadius += (this->outerBx - this->outerAx) * (this->outerBx - this->outerAx);
  this->outerRadius += (this->outerBy - this->outerAy) * (this->outerBy - this->outerAy);
  this->outerRadius = 0.5 * sqrtf(this->outerRadius);

  // Inner rectangle dimensions
  this->innerAx = -DIST_REAR_TO_REAR_AXLE - dim->rear * 0.5;
  this->innerBx = +VEHICLE_LENGTH - DIST_REAR_TO_REAR_AXLE + dim->front * 0.5;
  this->innerAy = -VEHICLE_WIDTH/2 - dim->left * 0.5;
  this->innerBy = +VEHICLE_WIDTH/2 + dim->right * 0.5;
  this->innerRadius = 0;
  this->innerRadius += (this->innerBx - this->innerAx) * (this->innerBx - this->innerAx);
  this->innerRadius += (this->innerBy - this->innerAy) * (this->innerBy - this->innerAy);
  this->innerRadius = 0.5 * sqrtf(this->innerRadius);
  
  return 0;
}


// Add a fake car to the map
int ConfigSpace::addFakeCar(const GraphNode *node, const VehicleState *state, 
                            Map *map, MapElementType type)
{
  int i;
  MapElement mapel;
  pose3_t pose, lpose, gpose;
  vec3_t pos;
  double cx, cy;
  double roll, pitch, yaw;
  vector<point2> points;

  // Compute transform global to local frame
  gpose.pos = vec3_set(state->utmNorthing,
                       state->utmEasting,
                       state->utmAltitude);
  gpose.rot = quat_from_rpy(state->utmRoll,
                            state->utmPitch,
                            state->utmYaw);
  lpose.pos = vec3_set(state->localX,
                       state->localY,
                       state->localZ);
  lpose.rot = quat_from_rpy(state->localRoll,
                            state->localPitch,
                            state->localYaw);  
  pose = pose3_mul(lpose, pose3_inv(gpose));

  // Get the object pose
  pose = pose3_mul(pose, node->pose);
  quat_to_rpy(pose.rot, &roll, &pitch, &yaw);

  // Create some points in a rough car-shape  
  cx = 4.0;
  cy = 2.0;
  for (i = 0; i < 100; i++)
  {
    pos.x = (double) rand() / RAND_MAX * cx - cx / 2;
    pos.y = (double) rand() / RAND_MAX * cy - cy / 2;
    pos.z = 0;
    pos = vec3_transform(pose, pos);
    points.push_back(point2(pos.x, pos.y));
  }

  mapel.setId(3333, node->index);

  if (type == ELEMENT_OBSTACLE)
    mapel.setTypeObstacle();
  else if (type == ELEMENT_VEHICLE)
    mapel.setTypeVehicle();
  else
    return ERROR("invalid map type");
  mapel.geometryType = GEOMETRY_POINTS;  
  mapel.setGeometry(points);
  mapel.setColor(MAP_COLOR_RED,0xFF);
  
  map->addEl(mapel);
  
  return 0;
}


// Construct configuration space from the given map
int ConfigSpace::update(const VehicleState *state, Map *map, bool forceObs, bool forceCar)
{
  int i;
  GraphNode *node;
  
  // See which lane elements are occupied
  this->updateLaneElements(state, map);

  // Expand the occupied lane elements
  this->expandLaneElements(state, forceObs, forceCar);
    
  // Reset the collision flags 
  for (i = 0; i < this->graph->getNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    node->collideObs = 0;
    node->collideCar = 0;
  }

  // Update the collision flags
  this->updateConfigLanes();
  this->updateConfigMap(state, map, forceObs, forceCar);

  return 0;
}


// Determine which lane elements contain objects
int ConfigSpace::updateLaneElements(const VehicleState *state, Map *map)
{
  int i, j;
  pose3_t gpose, lpose, pose;
  GraphNode *lanel;
  MapElement *mapel;
  double transGL[4][4];
  double px, py, qx, qy;
  double mapelAx, mapelBx, mapelAy, mapelBy;

  // Compute transform from local to global frame
  gpose.pos = vec3_set(state->utmNorthing,
                       state->utmEasting,
                       state->utmAltitude);
  gpose.rot = quat_from_rpy(state->utmRoll,
                            state->utmPitch,
                            state->utmYaw);
  lpose.pos = vec3_set(state->localX,
                       state->localY,
                       state->localZ);
  lpose.rot = quat_from_rpy(state->localRoll,
                            state->localPitch,
                            state->localYaw);  
  pose = pose3_mul(gpose, pose3_inv(lpose));
  pose3_to_mat44d(pose, transGL);

  // Reset the flags on all lane elements
  for (i = 0; i < this->graph->getStaticNodeCount(); i++)
  {
    lanel = this->graph->getNode(i);
    lanel->wasOcc = lanel->isOcc;
    lanel->isOcc = false;
  }
  
  // Place map object in lane elements.
  for (j = 0; j < (int) map->data.size(); j++)
  {
    mapel = &map->data[j];

    if (!(mapel->type == ELEMENT_VEHICLE || mapel->type == ELEMENT_OBSTACLE))
      continue;
    if (mapel->geometry.size() == 0)
      continue;

    // Compute the bounding box for this map element in the global frame.
    mapelAx = +DBL_MAX; mapelBx = -DBL_MAX;
    mapelAy = +DBL_MAX; mapelBy = -DBL_MAX;
    for (i = 0; i < 4; i++)
    {
      px = mapel->center.x;
      px += +cos(mapel->orientation) * mapel->length/2 * (1 - 2 * (i % 2));
      px += -sin(mapel->orientation) * mapel->width/2 * (1 - 2 * (i / 2));
      py = mapel->center.y;
      py += +sin(mapel->orientation) * mapel->length/2 * (1 - 2 * (i % 2));
      py += +cos(mapel->orientation) * mapel->width/2 * (1 - 2 * (i / 2));
      qx = transGL[0][0] * px + transGL[0][1] * py + transGL[0][3];
      qy = transGL[1][0] * px + transGL[1][1] * py + transGL[1][3];      
      if (qx < mapelAx) mapelAx = qx;
      if (qx > mapelBx) mapelBx = qx;
      if (qy < mapelAy) mapelAy = qy;
      if (qy > mapelBy) mapelBy = qy;
    }
    
    // Check static lane and turn nodes (lane elements)
    for (i = 0; i < this->graph->getStaticNodeCount(); i++)
    {
      lanel = this->graph->getNode(i);

      if (lanel->direction < +1)
        continue;
      if (lanel->type != GRAPH_NODE_LANE && lanel->type != GRAPH_NODE_TURN)
        continue;
      
      // Test bounding box
      assert(lanel->laneRadius > 0);
      if (mapelBx < lanel->pose.pos.x - lanel->laneRadius)
        continue;
      if (mapelAx > lanel->pose.pos.x + lanel->laneRadius)
        continue;
      if (mapelBy < lanel->pose.pos.y - lanel->laneRadius)
        continue;
      if (mapelAy > lanel->pose.pos.y + lanel->laneRadius)
        continue;

      // See if this lane element contains a map object
      if (this->testLaneElement(lanel, mapel, transGL))
      {
        lanel->isOcc = true;
        if (!lanel->wasOcc)
          lanel->occTime = state->timestamp;
      }
    }
  }   

  return 0;
}


// Check lane element to see if it contains a map element
bool ConfigSpace::testLaneElement(GraphNode *lanel, MapElement *mapel, double transGL[4][4])
{
  int i;
  double ax, ay, bx, by;
  point2_uncertain *point;
  double tol;

  // Distance objects must intrude into lanes before we consider them
  // real lane obstacles.  This is probably a hack.
  tol = 0.50; // MAGIC
  
  if (mapel->geometryType == GEOMETRY_POINTS)
  {
    for (i = 0; i < (int) mapel->geometry.size(); i++)
    {
      point = &mapel->geometry[i];

      // Transform point from local to global frame
      ax = transGL[0][0] * point->x + transGL[0][1] * point->y + transGL[0][3];
      ay = transGL[1][0] * point->x + transGL[1][1] * point->y + transGL[1][3];
      
      // Transform point from global to lanel frame
      bx = lanel->transNG[0][0] * ax + lanel->transNG[0][1] * ay + lanel->transNG[0][3];
      by = lanel->transNG[1][0] * ax + lanel->transNG[1][1] * ay + lanel->transNG[1][3];

      // Check point to see if it is in the lane
      if (bx < -lanel->laneLength/2 || bx > +lanel->laneLength/2)
        continue;
      if (by < -lanel->laneWidth/2 + tol || by > +lanel->laneWidth/2 - tol)
        continue;
      
      return true;
    }
  }
  
  return false;
}


// Expand occupied lane elements
int ConfigSpace::expandLaneElements(const VehicleState *state, bool forceObs, bool forceCar)
{
  int i;
  GraphNode *lanel;

  // Reset all flags
  for (i = 0; i < this->graph->getStaticNodeCount(); i++)
  {
    lanel = this->graph->getNode(i);
    lanel->obsInLane = false;
    lanel->carInLane = false;
    lanel->carTime = UINT64_MAX;
  }

  // Expand things that look like obstacles.
  if (!forceCar)
  {
    for (i = 0; i < this->graph->getStaticNodeCount(); i++)
    {
      lanel = this->graph->getNode(i);
      if (!lanel->isOcc)
        continue;
      if (forceObs || state->timestamp - lanel->occTime > 10 * 1000000) // MAGIC
      {
        this->expandObs(lanel, -1, this->clear.obsRear); 
        this->expandObs(lanel, +1, this->clear.obsFront); 
      }
    }
  }

  // Expand things that we assume are cars; since we dont expand cars
  // through obstacles, we do this bit second.
  if (!forceObs)
  {
    for (i = 0; i < this->graph->getStaticNodeCount(); i++)
    {
      lanel = this->graph->getNode(i);
      if (!lanel->isOcc)
        continue;
      if (forceCar || state->timestamp - lanel->occTime <= 10 * 1000000) // MAGIC
      {
        this->expandCar(lanel, lanel->occTime, -1, this->clear.carRear);
        this->expandCar(lanel, lanel->occTime, +1, this->clear.carFront);
      }
    }
  }

  return 0;
}


// Expand obstacles along the graph
int ConfigSpace::expandObs(GraphNode *lanel, int direction, double dist)
{
  GraphArc *arc;

  if (lanel->type != GRAPH_NODE_LANE && lanel->type != GRAPH_NODE_TURN)
    return 0;
  if (dist < 0)
    return 0;
  
  lanel->obsInLane = true;

  if (direction > 0)
  {
    // Propagate prediction along all outgoing arcs.
    for (arc = lanel->outFirst; arc != NULL; arc = arc->outNext)
    {
      assert(arc->nodeB != lanel);
      this->expandObs(arc->nodeB, direction, dist - arc->planDist / 1000.0);
    }
  }
  else
  {
    // Propagate prediction along all ingoing arcs.
    for (arc = lanel->inFirst; arc != NULL; arc = arc->inNext)
    {
      assert(arc->nodeA != lanel);
      this->expandObs(arc->nodeA, direction, dist - arc->planDist / 1000.0);
    }
  }
  
  return 0;
}

  
// Expand cars along the graph
int ConfigSpace::expandCar(GraphNode *lanel, uint64_t carTime, int direction, double dist)
{
  GraphArc *arc;

  // Dont expand through obstacles
  if (lanel->obsInLane)
    return 0;

  // Expand a finite distance along lanes.  Expand all the way across
  // intersections in the forward direction, but not at all in the
  // backward direction.
  if (lanel->type == GRAPH_NODE_LANE)
  {
    if (dist < 0)
      return 0;
  }
  else if (lanel->type == GRAPH_NODE_TURN)
  {
    if (direction < 0)
      return 0;
  }
  else
    return 0;

  lanel->carInLane = true;
  lanel->carTime = MIN(lanel->carTime, carTime);  

  // Dont expand past stop signs
  if (lanel->isStop)
    return 0;

  if (direction > 0)
  {
    // Propagate prediction along all outgoing arcs.
    for (arc = lanel->outFirst; arc != NULL; arc = arc->outNext)
    {
      assert(arc->nodeB != lanel);
      this->expandCar(arc->nodeB, carTime, direction, dist - arc->planDist / 1000.0);
    }
  }
  else
  {
    // Propagate prediction along all ingoing arcs.
    for (arc = lanel->inFirst; arc != NULL; arc = arc->inNext)
    {
      assert(arc->nodeA != lanel);
      this->expandCar(arc->nodeA, carTime, direction, dist - arc->planDist / 1000.0);
    }
  }
  
  return 0;
}


// Update the configuration space against lane elements
int ConfigSpace::updateConfigLanes()
{
  int i, j;
  double radius, dx, dy;
  int collide;
  GraphNode *node, *lanel;  
  
  // Take a second pass to figure out which configurations collide
  // with occupied lane elements.
  for (j = 0; j < this->graph->getStaticNodeCount(); j++)
  {
    lanel = this->graph->getNode(j);
    if (!(lanel->obsInLane || lanel->carInLane))
      continue;

    // Radius for bounding box check
    assert(lanel->laneRadius > 0);
    radius = this->outerRadius + lanel->laneRadius;

    for (i = 0; i < this->graph->getNodeCount(); i++)
    {
      node = this->graph->getNode(i);

      // Do a quick bounding box check
      dx = node->pose.pos.x - lanel->pose.pos.x;
      if (dx < -radius || dx > +radius)
        continue;
      dy = node->pose.pos.y - lanel->pose.pos.y;
      if (dy < -radius || dy > +radius)
        continue;
      
      // Do a full check for collision
      collide = this->testConfigLane(node, lanel);
      if (lanel->obsInLane)
        node->collideObs = MAX(node->collideObs, collide);
      if (lanel->carInLane)
        node->collideCar = MAX(node->collideCar, collide);
    }
  }

  return 0;
}


// Check a configuration for collision with occupied lane elements
int ConfigSpace::testConfigLane(GraphNode *node, GraphNode *lanel)
{
  int j;
  double ax, ay, bx, by, cx, cy;
  int collide;

  collide = 0;
  
  // TODO: denser sampling
  // Sample points across the lane.
  for (j = -1; j <= +1; j++)
  {
    ax = 0;
    ay = j * lanel->laneWidth/2;

    // Transform from node to global frame
    bx = lanel->transGN[0][0] * ax + lanel->transGN[0][1] * ay + lanel->transGN[0][3];
    by = lanel->transGN[1][0] * ax + lanel->transGN[1][1] * ay + lanel->transGN[1][3];

    // Transform from global to node frame
    cx = node->transNG[0][0] * bx + node->transNG[0][1] * by + node->transNG[0][3];
    cy = node->transNG[1][0] * bx + node->transNG[1][1] * by + node->transNG[1][3];

    // Check point against outer box
    if (cx < this->outerAx || cx > this->outerBx)
      continue;
    if (cy < this->outerAy || cy > this->outerBy)
      continue;

    collide = GRAPH_COLLIDE_LANE_OUTER;

    // Check point against inner box
    if (cx < this->innerAx || cx > this->innerBx)
      continue;
    if (cy < this->innerAy || cy > this->innerBy)
      continue;

    collide = GRAPH_COLLIDE_LANE_INNER;
    break;
  }    

  return collide;
}


// Update the configuration space against the map
int ConfigSpace::updateConfigMap(const VehicleState *state, Map *map, bool forceObs, bool forceCar)
{
  int i, j;
  pose3_t gpose, lpose, pose;
  GraphNode *node;
  MapElement *mapel;
  double transGL[4][4];
  double px, py, qx, qy;
  double mapelAx, mapelBx, mapelAy, mapelBy;
  int collide;

  // Compute transform from local to global frame
  gpose.pos = vec3_set(state->utmNorthing,
                       state->utmEasting,
                       state->utmAltitude);
  gpose.rot = quat_from_rpy(state->utmRoll,
                            state->utmPitch,
                            state->utmYaw);
  lpose.pos = vec3_set(state->localX,
                       state->localY,
                       state->localZ);
  lpose.rot = quat_from_rpy(state->localRoll,
                            state->localPitch,
                            state->localYaw);  
  pose = pose3_mul(gpose, pose3_inv(lpose));
  pose3_to_mat44d(pose, transGL);
  
  // Test each map element
  for (j = 0; j < (int) map->data.size(); j++)
  {
    mapel = &map->data[j];

    if (!(mapel->type == ELEMENT_VEHICLE || mapel->type == ELEMENT_OBSTACLE))
      continue;
    if (mapel->geometry.size() == 0)
      continue;

    // Compute the bounding box for this map element in the global frame.
    mapelAx = +DBL_MAX; mapelBx = -DBL_MAX;
    mapelAy = +DBL_MAX; mapelBy = -DBL_MAX;
    for (i = 0; i < 4; i++)
    {
      px = mapel->center.x;
      px += +cos(mapel->orientation) * mapel->length/2 * (1 - 2 * (i % 2));
      px += -sin(mapel->orientation) * mapel->width/2 * (1 - 2 * (i / 2));
      py = mapel->center.y;
      py += +sin(mapel->orientation) * mapel->length/2 * (1 - 2 * (i % 2));
      py += +cos(mapel->orientation) * mapel->width/2 * (1 - 2 * (i / 2));
      qx = transGL[0][0] * px + transGL[0][1] * py + transGL[0][3];
      qy = transGL[1][0] * px + transGL[1][1] * py + transGL[1][3];      
      if (qx < mapelAx) mapelAx = qx;
      if (qx > mapelBx) mapelBx = qx;
      if (qy < mapelAy) mapelAy = qy;
      if (qy > mapelBy) mapelBy = qy;
    }
    
    // Check all nodes
    for (i = 0; i < this->graph->getNodeCount(); i++)
    {
      node = this->graph->getNode(i);

      // Test bounding box
      assert(this->outerRadius > 0);
      if (mapelBx < node->pose.pos.x - this->outerRadius)
        continue;
      if (mapelAx > node->pose.pos.x + this->outerRadius)
        continue;
      if (mapelBy < node->pose.pos.y - this->outerRadius)
        continue;
      if (mapelAy > node->pose.pos.y + this->outerRadius)
        continue;

      // See if this lane element contains a map object
      collide = this->testConfigMap(node, mapel, transGL);

      // Since we cant tell cars from obstacles in the map, treat
      // everything as a car until told not to.  TODO: this does not
      // produce exactly the right behavior.
      if (forceObs)
        node->collideObs = MAX(node->collideObs, collide);
      else
        node->collideCar = MAX(node->collideCar, collide);
    }
  }   

  return 0;
}


// Check a configuration for collision with a map element
int ConfigSpace::testConfigMap(GraphNode *node, MapElement *mapel, double transGL[4][4])
{
  int i;
  double ax, ay, bx, by;
  point2_uncertain *point;
  int collide;

  collide = 0;
  
  if (mapel->geometryType == GEOMETRY_POINTS)
  {
    for (i = 0; i < (int) mapel->geometry.size(); i++)
    {
      point = &mapel->geometry[i];

      // Transform point from local to global frame
      ax = transGL[0][0] * point->x + transGL[0][1] * point->y + transGL[0][3];
      ay = transGL[1][0] * point->x + transGL[1][1] * point->y + transGL[1][3];
      
      // Transform point from global to node frame
      bx = node->transNG[0][0] * ax + node->transNG[0][1] * ay + node->transNG[0][3];
      by = node->transNG[1][0] * ax + node->transNG[1][1] * ay + node->transNG[1][3];

      // Check point against outer box
      if (bx < this->outerAx || bx > this->outerBx)
        continue;
      if (by < this->outerAy || by > this->outerBy)
        continue;

      collide = GRAPH_COLLIDE_MAP_OUTER;

      // Check point against inner box
      if (bx < this->innerAx || bx > this->innerBx)
        continue;
      if (by < this->innerAy || by > this->innerBy)
        continue;

      collide = GRAPH_COLLIDE_MAP_INNER;
      break;
    }
  }
  
  return collide;
}
