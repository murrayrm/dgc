/* 
 * Desc: Graph-based planner; functions for local trajectory generation
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <frames/pose3.h>
#include <trajutils/maneuver.h>
#include <trajutils/man2traj.hh>
#include <trajutils/traj.hh>

#include "GraphPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// Generate a vehicle trajectory.
int GraphPlanner::makeTraj(const VehicleState *vehicleState,
                           const ActuatorState *actuatorState,                           
                           GraphPath *path, double maxSpeed, CTraj *traj)
{
  int i;
  Vehicle *vp;
  int numSegments;
  Maneuver *mp[2];
  double steerAngle;
  double roll, pitch, yaw;
  GraphNode *node, *nodeA, *nodeB, *nodeC;
  VehicleConfiguration configA;  
  Pose2D poseB, poseC;
  double vel[3];

  if (path->pathLen < 2)
    return ERROR("invalid path");

  // Initialize manuever start and end nodes
  nodeA = path->path[0];
  nodeB = NULL;
  nodeC = NULL;
    
  // Initialize the default start and end velocities
  vel[0] = maxSpeed;
  vel[1] = maxSpeed;
  vel[2] = maxSpeed;
  
  // Walk along the path to determine both our target node and speed
  // profile.  This is current a hack that divides the path into a
  // short segment that connects the vehicle to the static graph
  // (nodeA to nodeB) and a longer segment that follows the static
  // graph (nodeB to nodeC).  The second part is currently a HACK using
  // a fixed-length look-ahead.
  for (i = 0; i < path->pathLen && i < 20; i++) // MAGIC
  {
    node = path->path[i];

    // If this node is in collision, set our terminal velocities to
    // zero and stop walking the path.
    if (node->collideObs || node->collideCar)
    {
      vel[1] = 0.0;
      vel[2] = 0.0;
      break;
    }

    // Look for the first static node (which is the end of the first
    // segment), then for a node some distance along the path.
    if (node->type == GRAPH_NODE_VEHICLE || node->type == GRAPH_NODE_VOLATILE)
      continue;

    if (nodeB == NULL)
      // The first time we get here will be the first static node; store it
      nodeB = node;
    else
      nodeC = node;
  }

  if (!nodeB || nodeB == nodeA)
  {
    // TODO : stop Alice!
    MSG("no valid destination, stopping vehicle");
    traj->startDataInput();
#warning "TODO: Stop the vehicle if there is no path."       
    return 0;
  }

  numSegments = 0;
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->kin.wheelBase, this->kin.maxSteer);

  // Compute the current steering angle for maneuver generation
  steerAngle = actuatorState->m_steerpos * this->kin.maxSteer;

  // Use the initial configuration given by the state
  configA.x = vehicleState->utmNorthing;
  configA.y = vehicleState->utmEasting;
  configA.theta = vehicleState->utmYaw;
  configA.phi = steerAngle;

  // Intermediate vehicle pose
  poseB.x = nodeB->poseGlobal.pos.x;
  poseB.y = nodeB->poseGlobal.pos.y;
  quat_to_rpy(nodeB->poseGlobal.rot, &roll, &pitch, &yaw);
  poseB.theta = yaw;

  // Stop at the goal
  if (nodeB->planCost == 0)
    vel[numSegments + 1] = 0.0;

  // Create maneuver
  mp[numSegments] = maneuver_config2pose(vp, &configA, &poseB);
  assert(mp[numSegments]);

  // fprintf(stdout, "manAB (%d -> %d): %f %f %f %f ; %f %f %f \n",
  //      nodeA->index, nodeB->index,
  //      configA.x, configA.y, configA.theta, configA.phi,
  //      poseB.x, poseB.y, poseB.theta);

  numSegments++;

  if (nodeC)
  {    
    // Final vehicle pose
    poseC.x = nodeC->poseGlobal.pos.x;
    poseC.y = nodeC->poseGlobal.pos.y;
    quat_to_rpy(nodeC->poseGlobal.rot, &roll, &pitch, &yaw);
    poseC.theta = yaw;

    // Stop at the goal
    if (nodeC->planCost == 0)
      vel[numSegments + 1] = 0.0;

    // Create maneuver 
    mp[numSegments] = maneuver_pose2pose(vp, &poseB, &poseC);
    assert(mp[numSegments]);

    // fprintf(stdout, "manBC (%d -> %d): %f %f %f ; %f %f %f \n",
    //      nodeB->index, nodeC->index,
    //      poseB.x, poseB.y, poseB.theta,
    //      poseC.x, poseC.y, poseC.theta);

    numSegments++;
  }

  MSG("segments %d", numSegments);
  
  // Sanity check
  assert(numSegments <= 2);
  
  // Generate a trajectory from the maneuver, with the given
  // initial and final velocity.
  traj->startDataInput();
  maneuver_profile_generate(vp, numSegments, mp, vel, 10, traj); // MAGIC
  
  //traj->print(cout);
  //traj->printSpeedProfile(cout);
  //fprintf(stdout, "\n");  
    
  // Clean up
  while (numSegments > 0)
    maneuver_free(mp[--numSegments]);
  free(vp);
  
  return 0;
}

