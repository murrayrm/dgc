
/* 
 * Desc: Trajectory graph data structure
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "Graph.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// Default constructor
Graph::Graph()
{
  memset(this, 0, sizeof(*this));
  
  return;
}


// Destructor
Graph::~Graph()
{
  return;
}


// Create a new node
GraphNode *Graph::createNode()
{
  GraphNode *node;
  
  assert((size_t) this->numNodes < sizeof(this->nodes)/sizeof(this->nodes[0]));

  // TODO We need a more efficient data structure for nodes, arcs and
  // the maps.  Using realloc is no good, since that screws up the
  // pointers in the arc list and the maps.

  // Create the node
  node = this->nodes + this->numNodes;
  memset(node, 0, sizeof(*node));  
  node->index = this->numNodes;
  this->numNodes++;

  // Clear the entries in the map also.
  this->numInArcs[node->index] = 0;
  this->numOutArcs[node->index] = 0;
  
  return node;
}


// Get the number of nodes
int Graph::getNodeCount()
{
  return this->numNodes;
}


// Get a node from an index
GraphNode *Graph::getNode(int index)
{
  if (index < 0 || index >= this->numNodes)
    return NULL;
  return this->nodes + index;
}


// Create an arc between two nodes
GraphArc *Graph::createArc(int indexa, int indexb)
{
  int i;
  GraphNode *nodeA, *nodeB;
  GraphArc *arc;

  // Make sure both nodes exist
  nodeA = this->getNode(indexa);
  if (!nodeA)
    return NULL;
  nodeB = this->getNode(indexb);
  if (!nodeB)
    return NULL;

  // Check for an existing arc
  for (i = 0; i < this->numArcs; i++)
  {
    arc = this->arcs + i;
    if (arc->nodeA->index == indexa && arc->nodeB->index == indexb)
      return arc;
  }

  // Create a new arc
  assert((size_t) this->numArcs < sizeof(this->arcs)/sizeof(this->arcs[0]));
  arc = this->arcs + this->numArcs;
  memset(arc, 0, sizeof(*arc));  
  arc->index = this->numArcs++;
  arc->nodeA = nodeA;
  arc->nodeB = nodeB;

  // Add to outgoing arc map
  assert(this->numOutArcs[indexa] < GRAPH_MAX_ARCS);
  this->outArcs[indexa][this->numOutArcs[indexa]++] = arc;

  // Add to incoming arc map
  assert(this->numInArcs[indexb] < GRAPH_MAX_ARCS);
  this->inArcs[indexb][this->numInArcs[indexb]++] = arc;

  //MSG("arc %d %d : %d %d", indexa, indexb,
  //    this->numOutArcs[indexa],
  //    this->numInArcs[indexb]);
  
  return arc;
}



// Get the number of arcs
int Graph::getArcCount()
{
  return this->numArcs;
}


// Get an arc from a node and arc index
GraphArc *Graph::getArc(int index)
{
  // Make sure the arc exists
  if (index < 0 || index >= this->numArcs)
    return NULL;
  return this->arcs + index;
}


// Get the number of incoming arcs for a node.
int Graph::getInCount(int nodeIndex)
{
  // Check the the node exists
  if (!this->getNode(nodeIndex))
    return 0;
  return this->numInArcs[nodeIndex];
}


// Get a particular incoming arc for a node.
GraphArc *Graph::getInArc(int nodeIndex, int arcIndex)
{
  // Check the the node exists
  if (!this->getNode(nodeIndex))
    return NULL;
  if (arcIndex < 0 || arcIndex >= this->numInArcs[nodeIndex])
    return NULL;
  return this->inArcs[nodeIndex][arcIndex];
}


// Get the number of outgoing arcs for a node.
int Graph::getOutCount(int nodeIndex)
{
  // Check the the node exists
  if (!this->getNode(nodeIndex))
    return 0;
  return this->numOutArcs[nodeIndex];
}


// Get a particular outgoing arc for a node.
GraphArc *Graph::getOutArc(int nodeIndex, int arcIndex)
{
  // Check the the node exists
  if (!this->getNode(nodeIndex))
    return NULL;
  if (arcIndex < 0 || arcIndex >= this->numOutArcs[nodeIndex])
    return NULL;
  return this->outArcs[nodeIndex][arcIndex];
}


// Get a node from an RNDF id
GraphNode *Graph::getNodeFromRndfId(int segmentId, int laneId, int waypointId)
{
  int i;
  GraphNode *node;

  for (i = 0; i < this->numNodes; i++)
  {
    node = this->nodes + i;
    if (node->isWaypoint &&
        node->segmentId == segmentId &&
        node->laneId == laneId &&
        node->waypointId == waypointId)
      return node;
  }
  
  return NULL;
}


// Get a node from a checkpoint id  
GraphNode *Graph::getNodeFromCheckpointId(int checkpointId)
{
  int i;
  GraphNode *node;
  for (i = 0; i < this->numNodes; i++)
  {
    node = this->nodes + i;
    if (node->isCheckpoint && node->checkpointId == checkpointId)
      return node;
  }  
  return NULL;
}



// Get the next node for lane following
GraphNode *Graph::getNextLaneNode(int index)
{
  int i;
  GraphNode *node;

  node = this->getNode(index);
  if (!node)
    return NULL;
  
  for (i = 0; i < this->numOutArcs[index]; i++)
  {
    if (this->outArcs[index][i]->nodeB->type == GRAPH_NODE_LANE)
      return this->outArcs[index][i]->nodeB;
  }
  
  return NULL;
}


// Get the previous node for lane following
GraphNode *Graph::getPrevLaneNode(int index)
{
  int i;
  GraphNode *node;

  node = this->getNode(index);
  if (!node)
    return NULL;

  for (i = 0; i < this->numInArcs[index]; i++)
  {
    if (this->inArcs[index][i]->nodeA->type == GRAPH_NODE_LANE)
      return this->inArcs[index][i]->nodeA;
  }
  
  return NULL;
}


// Freeze the current graph.
void Graph::freezeStatic()
{
  this->numStaticNodes = this->numNodes;
  this->numStaticArcs = this->numArcs;
  
  return;
}


// Clear volatile components.
void Graph::clearVolatile()
{
  int i, j;
  int na, nb;
  GraphArc *arc;
  
  // Destroy any volitile arcs.  We have do this explitly, since there
  // may be arcs between static and volatile nodes.
  for (i = this->numStaticArcs; i < this->numArcs; i++)
  {
    arc = &this->arcs[i];
    na = arc->nodeA->index;
    nb = arc->nodeB->index;

    // If the source node in the static set...
    if (na < this->numStaticNodes)
    {
      // Remove the offending entry from the map
      for (j = 0; j < this->numOutArcs[na]; j++)
      {
        if (this->outArcs[na][j] == arc)
        {
          this->numOutArcs[na] -= 1;
          memmove(&this->outArcs[na][j], &this->outArcs[na][j + 1],
                  (this->numOutArcs[na] - j) * sizeof(this->outArcs[na]));
                  
        }
      }
    }
    // If the destination node is in the static set
    else if (nb < this->numStaticNodes)
    {
      // Remove the offending entry from the map
      for (j = 0; j < this->numInArcs[nb]; j++)
      {
        if (this->inArcs[nb][j] == arc)
        {
          this->numInArcs[nb] -= 1;
          memmove(&this->inArcs[nb][j], &this->inArcs[nb][j + 1],
                  (this->numInArcs[nb] - j) * sizeof(this->inArcs[nb]));
                  
        }
      }
    }    
  }
  
  this->numNodes = this->numStaticNodes;
  this->numArcs = this->numStaticArcs;
  
  return;
}


// Get the number of static nodes.
int Graph::getStaticNodeCount()
{
  return this->numStaticNodes;
}
