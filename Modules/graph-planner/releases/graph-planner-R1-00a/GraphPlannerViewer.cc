
/* 
 * Desc: Graph planner viewer utility
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <limits.h>
#include <float.h>

#include <GL/glut.h>
#include <FL/Fl.H>
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <dgcutils/Fl_Glv_Window.H>
//#include <jplv/jplv_image.h>

#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>
#include <trajutils/traj.hh>
#include <interfaces/VehicleState.h>

// Live mode
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <map/MapElementTalker.hh>
#include <trajutils/TrajTalker.hh>

#include <map/Map.hh>
#include "GraphPlanner.hh"

#include "cmdline.h"


class App
{
  public:

  // Default constructor
  App();

  // Destructor
  ~App();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  // Initialize GUI
  int initGUI(int cols, int rows);

  // Finalize GUI
  int finiGUI();
 
  public:

  // Action callback
  static void onAction(Fl_Widget *w, int option);

  // View callback
  static void onView(Fl_Widget *w, int option);
  
  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  // Handle draw callbacks
  static void onDraw(Fl_Glv_Window *win, App *self);

  // Handle idle callbacks
  static void onIdle(App *self);

  public:

  // Switch to the vehicle frame
  void pushFrameVehicle(VehicleState state);

  // Switch to the a given pose
  void pushFramePose(pose3_t pose);

  // Revert to previous frame
  void popFrame();

  // Draw a set of axes
  void drawAxes(float size);

  // Draw the robot
  void drawAlice();

  // Predraw the map in the local frame
  void predrawMap();

  // Predraw the static graph in the local frame
  void predrawStaticGraph();

  // Predraw the volatile graph in the local frame
  void predrawVolatileGraph();

  // Predraw the CSpace in the local frame
  void predrawCSpace();

  // Draw the path to the goal
  void predrawPath();

  // Draw the trajectory
  void predrawTraj();

  public:

  // Initialize live mode
  int initLive();

  // Finalize live mode
  int finiLive();

  // Initialize the algorithms
  int init();

  // Finalize the algorithms
  int fini();

  // Update the algorithms
  int update();
  
  public:

  // Command-line options
  struct gengetopt_args_info options;

  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;
  
  // 3D window
  Fl_Glv_Window *worldwin;

  // Should we pause?
  bool pause;

  // Should we advance a single step?
  bool step;
  
  // Should we quit?
  bool quit;

  // Should we screen capture?
  bool capture;
  
  // Screen capture counter
  int capCount;

  // Display options
  bool viewChanges, viewTurns, viewVolatile, viewMap, viewCSpace, viewPath, viewTraj;

  public:
  
  // Operating mode: internal simulation or live
  enum {modeSim, modeLive} mode;

  // Global map
  Map map;
  
  // Global planner
  GraphPlanner planner;
  
  // Current state data
  VehicleState state;

  // Current path data
  GraphPath path;

  // Current trajectory
  CTraj *traj;

  // Time of last update
  uint64_t updateTime;

  public:
  
  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Sensnet handle (for getting state)
  sensnet_t *sensnet;

  // Talkers for live mode
  CMapElementTalker *mapTalker;
  CTrajTalker *trajTalker;

  public:

  // Display lists
  GLuint mapList, staticList, volatileList, cspaceList, pathList, trajList;

  // Is the display dirty?
  bool dirty;

  // Is the static graph display dirty?
  bool dirtyStatic;
};



// Commands
enum
{
  APP_ACTION_PAUSE = 0x1000,
  APP_ACTION_STEP,
  APP_ACTION_CAPTURE,
  APP_VIEW_CHANGES,
  APP_VIEW_TURNS,
  APP_VIEW_VOLATILE,
  APP_VIEW_MAP,
  APP_VIEW_CSPACE,
  APP_VIEW_PATH,
  APP_VIEW_TRAJ
};


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
App::App()
{
  memset(&this->path, 0, sizeof(this->path));
  this->staticList = 0;
  this->volatileList = 0;
  this->cspaceList = 0;
  this->pathList = 0;
  
  return;
}


// Destructor
App::~App()
{
  return;
}


// Parse command-line options
int App::parseCmdLine(int argc, char **argv)
{
  // Run parser
  if (cmdline_parser(argc, argv, &this->options) != 0)
  {
    cmdline_parser_print_help();
    return -1;
  }

  if (this->options.sim_flag)
    this->mode = modeSim;
  else
    this->mode = modeLive;

  if (this->mode == modeLive)
  {
    // Fill out the spread name
    if (this->options.spread_daemon_given)
      this->spreadDaemon = this->options.spread_daemon_arg;
    else if (getenv("SPREAD_DAEMON"))
      this->spreadDaemon = getenv("SPREAD_DAEMON");
    else
      return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
    // Fill out the skynet key
    if (this->options.skynet_key_given)
      this->skynetKey = this->options.skynet_key_arg;
    else if (getenv("SKYNET_KEY"))
      this->skynetKey = atoi(getenv("SKYNET_KEY"));
    else
      this->skynetKey = 0;
  }

  return 0;
}


// Initialize stuff
int App::initGUI(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) App::onExit},
      {0},
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) App::onAction, (void*) APP_ACTION_PAUSE, FL_MENU_TOGGLE},
      {"Single step", '.', (Fl_Callback*) App::onAction, (void*) APP_ACTION_STEP},
      {"Screen capture", 'c', (Fl_Callback*) App::onAction, (void*) APP_ACTION_CAPTURE},
      {0},
      {"&View", 0, 0, 0, FL_SUBMENU},    
      {"Changes", FL_CTRL + 'e', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_CHANGES, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Volatile", FL_CTRL + 'v', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_VOLATILE, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Map", FL_CTRL + 'm', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_MAP, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"CSpace", FL_CTRL + 'c', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_CSPACE, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Path", FL_CTRL + 'p', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_PATH, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Traj", FL_CTRL + 't', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_TRAJ, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {0},
      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Ladar Blob Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new Fl_Glv_Window(0, 30, cols, rows - 30, this, (Fl_Callback*) onDraw);

  this->mainwin->end();

  this->worldwin->set_hfov(40);
  this->worldwin->set_clip(2, 1000);
  this->worldwin->set_lookat(-1, 0, -100, 0, 0, 0, 0, 0, -1);

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  this->viewTurns = true;
  this->viewChanges = true;
  this->viewVolatile = true;
  this->viewMap = true;
  this->viewCSpace = true;
  this->viewPath = true;
  this->viewTraj = true;

  // Dont set this unless we also set the menu state.
  // this->pause = true;  
  this->step = true;
  this->dirty = true;
  this->dirtyStatic = true;
  
  return 0;
}


// Finalize stuff
int App::finiGUI()
{  
  return 0;
}


// Handle menu callbacks
void App::onAction(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data();
  if (option == APP_ACTION_PAUSE)
    self->pause = !self->pause;
  if (option == APP_ACTION_STEP)
    self->step = true;
  if (option == APP_ACTION_CAPTURE)
  {
    self->capture = true;
    self->worldwin->redraw();
  }
  
  return;
}


// Handle menu callbacks
void App::onView(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data();

  if (option == APP_VIEW_TURNS)
  {
    self->viewTurns = !self->viewTurns;
    self->dirtyStatic = true;
  }
  if (option == APP_VIEW_CHANGES)
  {
    self->viewChanges = !self->viewChanges;
    self->dirtyStatic = true;
  }
  
  if (option == APP_VIEW_VOLATILE)
    self->viewVolatile = !self->viewVolatile;
  if (option == APP_VIEW_MAP)
    self->viewMap = !self->viewMap;  
  if (option == APP_VIEW_CSPACE)
    self->viewCSpace = !self->viewCSpace;  
  if (option == APP_VIEW_PATH)
    self->viewPath = !self->viewPath;

  self->dirty = true;
  self->worldwin->redraw();
  
  return;
}


// Handle menu callbacks
void App::onExit(Fl_Widget *w, int option)
{
  App *self;

  self = (App*) w->user_data();
  self->quit = true;

  return;
}


// Handle draw callbacks
void App::onDraw(Fl_Glv_Window *win, App *self)
{
  if (self->dirtyStatic)
  {
    self->predrawStaticGraph();
    self->dirtyStatic = false;
  }

  if (self->dirty)
  {
    if (self->viewVolatile)
      self->predrawVolatileGraph();
    if (self->viewMap)
      self->predrawMap();
    if (self->viewCSpace)
      self->predrawCSpace();
    if (self->viewPath)
      self->predrawPath();
    if (self->viewTraj)
      self->predrawTraj();
    self->dirty = false;
  }
  
  glPushMatrix();
  glTranslatef(-self->state.localX, -self->state.localY, -self->state.localZ);
  
  // Switch to vehicle frame to draw axes
  self->pushFrameVehicle(self->state);
  self->drawAxes(1.0);
  self->drawAlice();
  self->popFrame();

  // Draw the map
  if (self->viewMap)
    glCallList(self->mapList);

  // Draw the graph
  glCallList(self->staticList);

  // Draw the volatile graph
  if (self->viewVolatile)
    glCallList(self->volatileList);

  // Draw the c-space
  if (self->viewCSpace)
    glCallList(self->cspaceList);

  // Draw the path
  if (self->viewPath)
    glCallList(self->pathList);

  // Draw the trajectory
  if (self->viewTraj)
    glCallList(self->trajList);

  glPopMatrix();

  // Screen cap to file
  if (self->capture)
  {
#ifdef JPLV_IMAGE_H
    jplv_image_t *image;
    char filename[1024];
    int vp[4];
    
    glGetIntegerv(GL_VIEWPORT, vp);
    image = jplv_image_alloc(vp[2], vp[3], 3, 8, 0, NULL);
    glReadPixels(vp[0], vp[1], vp[2], vp[3], GL_RGB, GL_UNSIGNED_BYTE, image->data);       
    snprintf(filename, sizeof(filename), "planner-%04d.pnm", self->capCount);
    MSG("writing %s", filename);
    jplv_image_write_pnm(image, filename, NULL);
    jplv_image_free(image);
#endif
    
    self->capture = false;
    self->capCount++;
  }

  return;
}


// Switch to the vehicle frame
void App::pushFrameVehicle(VehicleState state)
{
  pose3_t pose;
  pose.pos = vec3_set(state.localX, state.localY, state.localZ);
  pose.rot = quat_from_rpy(state.localRoll, state.localPitch, state.localYaw);

  float m[4][4];
  pose3_to_mat44f(pose, m);  

  // Transpose to column-major order for GL
  int i, j;
  float t[16];
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);
  
  return;
}


// Switch to the a given pose
void App::pushFramePose(pose3_t pose)
{
  float m[4][4];
  pose3_to_mat44f(pose, m);  

  // Transpose to column-major order for GL
  int i, j;
  float t[16];
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);
  
  return;
}


// Revert to previous frame
void App::popFrame()
{
  glPopMatrix();  
  return;
}


// Draw a set of axes
void App::drawAxes(float size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Draw the robot
void App::drawAlice()
{
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
    
  return;
}


// TODO move to file with other GL helpers
// Draw a text box
void glDrawText(float size, const char *text)
{
  int i, count;
  float sx, sy;

  // Compute the scale factors in x/y directions, based on the 'M'
  // character.
  sx = glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, 'M');
  sy = sx * 1.3;
    
  glPushMatrix();
  glScalef(size / sx, size / sy, 1);
  glTranslatef(0, -sy, 0);

  glPushMatrix();
    
  for (i = 0, count = 0; i < (int) strlen(text); i++, count++)
  {
    if (text[i] == '\n')
    {
      glPopMatrix();
      glTranslatef(0, -sy, 0);
      glPushMatrix();
      count = 0;      
    }
    else
    {
      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[i]);
    }
  }

  glPopMatrix();
  glPopMatrix();

  return;
}


// Predraw the map in the local frame
void App::predrawMap()
{
  int i, j;
  MapElement *mapel;
  point2_uncertain *point;
  
  if (this->mapList == 0)
    this->mapList = glGenLists(1);
  glNewList(this->mapList, GL_COMPILE);
  
  for (i = 0; i < (int) this->map.data.size(); i++)
  {
    mapel = &this->map.data[i];

    // TESTING
    if (mapel->geometry.size() > 0)
      MSG("mapel %d type %d %f %f", i, mapel->type,
          mapel->geometry[0].x, mapel->geometry[0].y);

    // Draw the bounding box
    glPushMatrix();
    glTranslated(mapel->center.x, mapel->center.y, this->state.localZ);
    glRotated(mapel->orientation * 180/M_PI, 0, 0, 1);
    glColor3f(1, 0, 0);
    glBegin(GL_LINE_LOOP);
    glVertex2d(-mapel->length/2, -mapel->width/2);
    glVertex2d(+mapel->length/2, -mapel->width/2);
    glVertex2d(+mapel->length/2, +mapel->width/2);
    glVertex2d(-mapel->length/2, +mapel->width/2);
    glEnd();
    glPopMatrix();

    // Draw points
    if (mapel->geometry_type == GEOMETRY_POINTS)
    {
      glPointSize(2);
      glColor3f(1, 0, 0);
      glBegin(GL_POINTS);
      for (j = 0; j < (int) mapel->geometry.size(); j++)
      {
        point = &mapel->geometry[j];
        glVertex3d(point->x, point->y, this->state.localZ);
      }
      glEnd();
    }

    // Draw polygon
    if (mapel->geometry_type == GEOMETRY_POLY)
    {
      glPointSize(2);
      glColor3f(1, 0, 0);
      glBegin(GL_POLYGON);
      for (j = 0; j < (int) mapel->geometry.size(); j++)
      {
        point = &mapel->geometry[j];
        glVertex3d(point->x, point->y, this->state.localZ);
      }
      glEnd();
    }
  }
  
  glEndList();
  
  return;
}


// Predraw the static graph in the local frame
void App::predrawStaticGraph()
{
  int i, j;
  GraphNode *node;
  GraphArc *arc;
  vec3_t pa, pb;
  GLUquadric *quad;
  
  if (this->staticList == 0)
    this->staticList = glGenLists(1);
  glNewList(this->staticList, GL_COMPILE);

  quad = gluNewQuadric();
  
  // Draw nodes
  glPointSize(3);
  for (i = 0; i < this->planner.graph.getStaticNodeCount(); i++)
  {
    node = this->planner.graph.getNode(i);
    assert(node);
        
    if (!this->viewTurns)
      if (node->type == GRAPH_NODE_TURN)
        continue;
    if (!this->viewChanges)
      if (node->type == GRAPH_NODE_CHANGE)
        continue;

    if (node->type == GRAPH_NODE_LANE)
    {
      if (node->isEntry)
        glColor3f(0, 1, 0);
      else if (node->isExit)
        glColor3f(1, 0, 0);
      else if (node->isCheckpoint)
        glColor3f(1, 0, 1);
      else if (node->isWaypoint)
        glColor3f(1, 1, 1);
      else
        glColor3f(0.5, 0.5, 1);
    }
    else
    {
      glColor3f(0.5, 0.5, 0.5);
    }
    
    pa = node->poseLocal.pos;
    pb = vec3_transform(node->poseLocal, vec3_set(0.4, 0, 0));

    // Show the node center
    glBegin(GL_POINTS);  
    glVertex3d(pa.x, pa.y, pa.z);
    glEnd();

    // Show the node orientation
    if (true && node->isWaypoint)
    {
      glBegin(GL_LINES);  
      glVertex3d(pa.x, pa.y, pa.z);
      glVertex3d(pb.x, pb.y, pa.z);    
      glEnd();
    }

    // Show node info
    if (true && node->isWaypoint)
    {
      char text[64];
      snprintf(text, sizeof(text), "%d.%d.%d CP %d\n%d/%d\nPC %.3f",
               node->segmentId, node->laneId,
               node->waypointId, node->checkpointId,
               this->planner.graph.getInCount(node->index),
               this->planner.graph.getOutCount(node->index),
               (node->planCost < INT_MAX ? node->planCost * 1e-3 : -1));

      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glPushMatrix();
      glTranslated(node->poseLocal.pos.x, node->poseLocal.pos.y, node->poseLocal.pos.z);
      glRotatef(180, 0, 1, 0);
      gluDisk(quad, 0.50, 0.50, 16, 1);
      glDrawText(0.1, text);
      glPopMatrix();
    }
  }

  // Draw arcs
  glBegin(GL_LINES);

  node = this->planner.graph.getNode(i);
  assert(node);

  for (i = 0; i < this->planner.graph.getStaticNodeCount(); i++)
  {
    node = this->planner.graph.getNode(i);
    assert(node);

    for (j = 0; j < this->planner.graph.getOutCount(node->index); j++)
    {
      arc = this->planner.graph.getOutArc(node->index, j);

      if (!this->viewTurns)
        if (arc->nodeA->type == GRAPH_NODE_TURN || arc->nodeB->type == GRAPH_NODE_TURN)
          continue;
      if (!this->viewChanges)
        if (arc->nodeA->type == GRAPH_NODE_CHANGE || arc->nodeB->type == GRAPH_NODE_CHANGE)
          continue;

      glColor3f(0.5, 0.5, 0.5);
      glVertex3d(arc->nodeA->poseLocal.pos.x,
                 arc->nodeA->poseLocal.pos.y,
                 arc->nodeA->poseLocal.pos.z);
      glColor3f(1, 1, 1);
      glVertex3d(arc->nodeB->poseLocal.pos.x,
                 arc->nodeB->poseLocal.pos.y,
                 arc->nodeB->poseLocal.pos.z);
    }
  }
  glEnd();

  gluDeleteQuadric(quad);
    
  glEndList();

  return;
}


// Predraw the volatile graph in the local frame
void App::predrawVolatileGraph()
{
  int i, j;
  GraphNode *node;
  GraphArc *arc;
  
  if (this->volatileList == 0)
    this->volatileList = glGenLists(1);
  glNewList(this->volatileList, GL_COMPILE);

  if (false)
  {
    // Draw nodes
    glPointSize(3);  
    glColor3f(0.5, 0.5, 0.5);
    glBegin(GL_POINTS);  
    for (i = this->planner.graph.getStaticNodeCount(); i < this->planner.graph.getNodeCount(); i++)
    {
      node = this->planner.graph.getNode(i);
      assert(node);

      // Show the node center
      glVertex3d(node->poseLocal.pos.x,
                 node->poseLocal.pos.y,
                 node->poseLocal.pos.z - 0.20);
    }
    glEnd();
  }

  // Draw arcs
  glBegin(GL_LINES);
  for (i = this->planner.graph.getStaticNodeCount(); i < this->planner.graph.getNodeCount(); i++)
  {
    node = this->planner.graph.getNode(i);
    assert(node);

    for (j = 0; j < this->planner.graph.getOutCount(node->index); j++)
    {
      arc = this->planner.graph.getOutArc(node->index, j);
      
      glColor3f(0, 0, 0.5);
      glVertex3d(arc->nodeA->poseLocal.pos.x,
                 arc->nodeA->poseLocal.pos.y,
                 arc->nodeA->poseLocal.pos.z - 0.20);
      glColor3f(0, 0, 1.0);
      glVertex3d(arc->nodeB->poseLocal.pos.x,
                 arc->nodeB->poseLocal.pos.y,
                 arc->nodeB->poseLocal.pos.z - 0.20);
    }
  }
  glEnd();
    
  glEndList();

  return;
}


// Predraw the CSpace in the local frame
void App::predrawCSpace()
{
  int i;
  GraphNode *node;
  GLUquadric *quad;
  
  if (this->cspaceList == 0)
    this->cspaceList = glGenLists(1);
  glNewList(this->cspaceList, GL_COMPILE);

  quad = gluNewQuadric();
  
  for (i = 0; i < this->planner.graph.getNodeCount(); i++)
  {
    node = this->planner.graph.getNode(i);
    assert(node);
    if (!node->collide)
      continue;

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glColor3f(1, 0, 0);

    this->pushFramePose(node->poseLocal);    
    gluDisk(quad, 0, 0.50, 16, 1);

    // Draw outer vehicle dimensions
    glPushMatrix();
    glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
    glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
    glColor3f(0, 0, 1);
    glutWireCube(1.0);
    glPopMatrix();

    this->popFrame();
  }

  gluDeleteQuadric(quad);

  glEndList();
  
  return;
}


// Draw the path in the local frame
void App::predrawPath()
{
  int i;
  GraphNode *node;
  
  if (this->pathList == 0)
    this->pathList = glGenLists(1);
  glNewList(this->pathList, GL_COMPILE);

  glLineWidth(2);
  glColor3f(0, 1, 0);
  glBegin(GL_LINE_STRIP);
  for (i = 0; i < this->path.pathLen; i++)
  {    
    node = this->path.path[i];
    assert(node);

    // We start out coloring the path in green, but change to
    // red if we see a collision.
    if (node->collide)
      glColor3f(1, 0, 0);
      
    glVertex3d(node->poseLocal.pos.x,
               node->poseLocal.pos.y,
               node->poseLocal.pos.z - 0.30);
  }
  glEnd();
  glLineWidth(1);
  
  glEndList();

  return;
}


// Draw the trajectory in the local frame
void App::predrawTraj()
{
  int i;
  vec3_t p;
  pose3_t pose, lpose, gpose;
  
  if (this->trajList == 0)
    this->trajList = glGenLists(1);
  glNewList(this->trajList, GL_COMPILE);

  // We assume the trajectory is in the global frame (for now).
  // Construct transform from global to local pose.
  gpose.pos = vec3_set(this->state.utmNorthing,
                       this->state.utmEasting,
                       this->state.utmAltitude);
  gpose.rot = quat_from_rpy(this->state.utmRoll,
                            this->state.utmPitch,
                            this->state.utmYaw);
  lpose.pos = vec3_set(this->state.localX,
                       this->state.localY,
                       this->state.localZ);
  lpose.rot = quat_from_rpy(this->state.localRoll,
                            this->state.localPitch,
                            this->state.localYaw);
  pose = pose3_mul(lpose, pose3_inv(gpose));
    
  glLineWidth(2);
  glColor3f(1, 0, 1);
  glBegin(GL_LINE_STRIP);

  for (i = 0; i < this->traj->getNumPoints(); i++)
  {    
    p.x = this->traj->getNorthing(i);
    p.y = this->traj->getEasting(i);
    p.z = this->state.utmAltitude;

    p = vec3_transform(pose, p);
    
    glVertex3d(p.x, p.y, p.z);
  }
  
  glEnd();
  glLineWidth(1);
  
  glEndList();

  return;
}


// Handle idle callbacks
void App::onIdle(App *self)
{
  if (!self->pause || self->step)
  {
    // Throttle the update rate
    if (DGCgettime() - self->updateTime < (uint64_t) (1e6 / self->options.rate_arg))
    {
      usleep(0);
      return;
    }
    self->updateTime = DGCgettime();
      
    // Update perceptor
    self->update();

    // Redraw the display
    self->worldwin->redraw();
    
    // Single step mode: we've moved one step, so clear flag
    if (self->step)
      self->step = false;
  }
  else
  {
    // Sleepy bye-bye
    usleep(20000);
  }
  
  return;
}



// Initialize live mode
int App::initLive()
{
  // Create sensnet interface for state messages
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);

  // Open connection
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, MODgraphPlanner) != 0)
    return ERROR("unable to connect to sensnet");

  // Listen to state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(this->state)) != 0)
    return ERROR("unable to join state group");

  // Create talker for map update
  this->mapTalker = new CMapElementTalker();
  this->mapTalker->initRecvMapElement(this->skynetKey, MODgraphPlanner);
  
  // Create talker for the output trajectory
  this->trajTalker = new CTrajTalker(MODgraphPlanner, this->skynetKey);
  
  return 0;
}


// Finalize live mode
int App::finiLive()
{
  // Clean up talkers
  delete this->trajTalker;
  this->trajTalker = NULL;
  delete this->mapTalker;
  this->mapTalker = NULL;

  // Clean up sensnet
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}


// Initialize the planner
int App::init()
{
  // Set vehicle properties
  this->planner.initVehicle(VEHICLE_WHEELBASE,
                            this->options.max_steer_arg * M_PI/180,
                            this->options.max_speed_arg);

  // Set vehicle size
  this->planner.setVehicleSize(DIST_REAR_TO_REAR_AXLE,
                               VEHICLE_LENGTH - DIST_REAR_TO_REAR_AXLE +
                               this->options.front_dist_arg,
                               VEHICLE_WIDTH/2 + this->options.side_dist_arg,
                               VEHICLE_WIDTH/2 + this->options.side_dist_arg);

  // Load up the RNDF and initialize the graph
  if (this->options.rndf_given)
    this->planner.loadRndf(this->options.rndf_arg);

  // In simulation mode, initialize the state to waypoint in the RNDF
  if (this->mode == modeSim)
  {
    int segment, lane, waypoint;
    GraphNode *node;

    // Default to the first node
    node = this->planner.graph.getNode(0);

    if (this->options.rndf_start_given)
    {
      // Parse out the node from the command line args
      if (sscanf(this->options.rndf_start_arg, "%d.%d.%d", &segment, &lane, &waypoint) < 3)
        MSG("syntax error: %s", this->options.rndf_start_arg);
      node = this->planner.graph.getNodeFromRndfId(segment, lane, waypoint);
      if (!node)
        MSG("no such waypoint: %s", this->options.rndf_start_arg);
    }

    // Set initial position and heading
    if (node)
    {
      double roll, pitch, yaw;      
      memset(&this->state, 0, sizeof(this->state));
      this->state.utmNorthing = node->poseGlobal.pos.x;
      this->state.utmEasting = node->poseGlobal.pos.y;
      this->state.utmAltitude = node->poseGlobal.pos.z;
      quat_to_rpy(node->poseGlobal.rot, &roll, &pitch, &yaw);
      this->state.utmYaw = yaw;
    }

    // Update state so the local poses are set
    this->planner.updateState(&this->state);
  }

  // Create trajectory
  this->traj = new CTraj(3);
    
  return 0;
}


// Finalize the planner
int App::fini()
{

  // Clean up trajectory
  delete this->traj;
  this->traj = NULL;
  
  return 0;  
}


// Update the planner
int App::update()
{
  uint64_t time;

  if (this->mode == modeSim)
  {
    // In simulation mode, we step the vehicle along the current path.
    if (this->path.pathLen >= 2)
    {
      GraphNode *node;
      pose3_t pose;
      double roll, pitch, yaw;
      
      node = this->path.path[1];

      pose = node->poseGlobal;
      quat_to_rpy(pose.rot, &roll, &pitch, &yaw);            
      this->state.utmNorthing = pose.pos.x;
      this->state.utmEasting = pose.pos.y;
      this->state.utmAltitude = pose.pos.z;
      this->state.utmRoll = roll;
      this->state.utmPitch = pitch;
      this->state.utmYaw = yaw;

      pose = node->poseLocal;
      quat_to_rpy(pose.rot, &roll, &pitch, &yaw);            
      this->state.localX = pose.pos.x;
      this->state.localY = pose.pos.y;
      this->state.localZ = pose.pos.z;
      this->state.localRoll = roll;
      this->state.localPitch = pitch;
      this->state.localYaw = yaw;
    }
  }
  else
  {
    MapElement mapel;
    
    // In live mode, get the state from sensnet
    sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
                 NULL, sizeof(this->state), &this->state);

    // Read any map elements and update the map.
    // This takes far longer than it should; possible skynet problems?
    time = DGCgettime();
    while (this->mapTalker->recvMapElementNoBlock(&mapel) > 0)
    {
      MSG("got map element %f %f", mapel.center.x, mapel.center.y);
      this->map.addEl(mapel);
    }
    MSG("map %lldms", (DGCgettime() - time) / 1000);
  }

  // Create static obstacles
  if (this->options.fake_obs_given > 0)
  {
    int i;
    int segment, lane, waypoint;
    GraphNode *node;
    
    for (i = 0; i < (int) this->options.fake_obs_given; i++)
    {
      if (sscanf(this->options.fake_obs_arg[i], "%d.%d.%d", &segment, &lane, &waypoint) < 3)
      {
        MSG("syntax error: %s", this->options.fake_obs_arg[i]);
        continue;
      }
      MSG("fake obstacle at %d.%d.%d", segment, lane, waypoint);
      node = this->planner.graph.getNodeFromRndfId(segment, lane, waypoint);
      assert(node);
      this->planner.addFakeCar(&this->map, node);
    }    
  }

  // Test code for logging the pose
  if (false)
  {
    // Log the pose
    fprintf(stdout, "pose %f %f %f %f %f %f\n",
            this->state.localX, this->state.localY, this->state.localZ,
            this->state.localRoll, this->state.localPitch, this->state.localYaw);             
  }

  // Update local positions using the current state info
  time = DGCgettime();
  this->planner.genVehicleSubGraph(&this->state);
  MSG("vehicle %lldms", (DGCgettime() - time) / 1000);

  // Update local positions using the current state info
  time = DGCgettime();
  this->planner.updateState(&this->state);
  MSG("state %lldms", (DGCgettime() - time) / 1000);

  // Update c-space
  time = DGCgettime();
  this->planner.makeCSpace(&this->map);
  MSG("cspace %lldms", (DGCgettime() - time) / 1000);
  
  // Create plan to goal
  if (this->options.goal_given)
  {
    time = DGCgettime();
    this->planner.makePlan(this->options.goal_arg); 
    MSG("plan %lldms", (DGCgettime() - time) / 1000);

    // Get the path to the goal, if there is one
    this->planner.makePath(&this->path);

    // Create a trajectory from the path.  This includes the velocity
    // profile.
    this->planner.makeTraj(&this->state, &this->path, this->traj);
  }
  
  if (this->mode == modeLive)
  {
    int trajSocket;

    // TODO clean up the truly awful TrajTalker.
    // Get the socket id for sending messages.
    trajSocket = this->trajTalker->m_skynet.get_send_sock(SNtraj);

    // Send trajectory data to both followe and GUI
    assert(this->trajTalker);
    this->trajTalker->SendTraj(trajSocket, this->traj);
  }
  
  // Stuff has changed, so we need to redraw
  this->dirty = true;
  
  return 0;
}




int main(int argc, char *argv[])
{
  App *app;

  // Initialize GLUT calls
  glutInit(&argc, argv);

  app = new App();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  if (app->initGUI(1024, 768) != 0)
    return -1;

  // Initialize live mode
  if (app->mode == App::modeLive)
    if (app->initLive() != 0)
      return -1;

  // Initialize algorithms
  if (app->init() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) App::onIdle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  app->finiGUI();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
