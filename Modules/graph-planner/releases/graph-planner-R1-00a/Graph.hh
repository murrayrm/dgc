
/* 
 * Desc: Trajectory graph data structure
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef GRAPH_HH
#define GRAPH_HH

#include <frames/pose3.h>


#define GRAPH_MAX_NODES 20000
#define GRAPH_MAX_ARCS 256


// Foward declarations
class GraphNode;
class GraphArc;


// Types for nodes
enum GraphNodeType
{
  GRAPH_NODE_VEHICLE,
  GRAPH_NODE_LANE,
  GRAPH_NODE_TURN,
  GRAPH_NODE_CHANGE,
  GRAPH_NODE_VOLATILE,
};


/// @brief Class for storing a single node in the graph.
struct GraphNode
{
  // Node type
  int type;
  
  // Unique node index
  int index;

  // Direction (with traffic +1 or against traffic -1)
  int direction;

  // Segment id
  int segmentId;
  
  // Lane id
  int laneId;

  // Waypoint id (waypoints only)
  int waypointId;

  // Checkpoint id (checkpoints only)
  int checkpointId;

  // Is this a waypoint?
  bool isWaypoint;
  
  // Is this a check point?
  bool isCheckpoint;
  
  // Is this an entry point?
  bool isEntry;

  // Is this an exit point?
  bool isExit;
  
  // Node pose in global frame
  pose3_t poseGlobal;

  // Node pose in local frame
  pose3_t poseLocal;

  // Does this node collide with something?
  bool collide;
  
  // Plan cost
  int planCost;
};


/// @brief Class for storing a single arc in the graph.
struct GraphArc
{
  // Unique arc index
  int index;
    
  // Nodes joined by this arc
  GraphNode *nodeA, *nodeB;  
};


/// @brief Class for storing trajectory graphs.
class Graph
{
  public:

  // Default constructor
  Graph();

  // Destructor
  virtual ~Graph();

  public:

  // Create a new node
  GraphNode *createNode();

  // Get the number of nodes
  int getNodeCount();

  // Get a node from an node index
  GraphNode *getNode(int index);

  // Create an arc between two nodes
  GraphArc *createArc(int indexa, int indexb);

  // Get the number of arcs
  int getArcCount();

  // Get an arc from an arc index
  GraphArc *getArc(int index);

  // Get the number of incoming arcs for a node.
  int getInCount(int nodeIndex);

  // Get a particular incoming arc for a node.
  GraphArc *getInArc(int nodeIndex, int arcIndex);

  // Get the number of outgong arcs for a node.
  int getOutCount(int nodeIndex);

  // Get a particular outgoing arc for a node.
  GraphArc *getOutArc(int nodeIndex, int arcIndex);

  public:

  /// @brief Freeze the current graph.
  ///
  /// Call this function when all the static components have been added to
  /// the graph.  Any future additions will be volatile, meaning they can
  /// be erased with the ::clearVolatile function.
  void freezeStatic();

  /// @brief Clear volatile components.
  ///
  /// Remove any elements added since ::freezeStatic was called.
  void clearVolatile();

  /// @brief Get the number of static nodes.
  int getStaticNodeCount();
  
  public:
  
  // Get the next node for lane following
  GraphNode *getNextLaneNode(int index);

  // Get the previous node for lane following
  GraphNode *getPrevLaneNode(int index);

  // Get a node from an RNDF id
  GraphNode *getNodeFromRndfId(int segmentId, int laneId, int waypointId);

  // Get a node from a checkpoint id  
  GraphNode *getNodeFromCheckpointId(int checkpointId);

  private:

  // Flat list of nodes
  int numNodes;
  GraphNode nodes[GRAPH_MAX_NODES];

  // Flat list of arcs
  int numArcs;
  GraphArc arcs[GRAPH_MAX_NODES * GRAPH_MAX_ARCS];

  // Map describing all the outgoing arcs for each node.
  int numOutArcs[GRAPH_MAX_NODES];  
  GraphArc *outArcs[GRAPH_MAX_NODES][GRAPH_MAX_ARCS];

  // Map describing all the incoming arcs for each node.
  int numInArcs[GRAPH_MAX_NODES];  
  GraphArc *inArcs[GRAPH_MAX_NODES][GRAPH_MAX_ARCS];

  // Start of the volatile portion of the graph.
  int numStaticNodes, numStaticArcs;
};





#endif
