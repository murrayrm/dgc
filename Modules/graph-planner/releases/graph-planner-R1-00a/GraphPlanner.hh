
/* 
 * Desc: Graph-based planner
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef GRAPH_PLANNER_HH
#define GRAPH_PLANNER_HH


// Forward declarations
namespace std
{
  class Lane;
  class RNDF;
}
class Map;
class MapElement;
class CTraj;


#include <interfaces/VehicleState.h>

#include "Graph.hh"
#include "GraphPath.hh"



/// @brief Graph-based planner
class GraphPlanner
{
  public:

  // Default constructor
  GraphPlanner();

  // Destructor
  virtual ~GraphPlanner();

  public:

  /// @brief Set the vehicle properties.
  ///
  /// This must be done before the RNDF or map is loaded.
  ///
  /// @param[in] wheelBase Distance between front and rear wheels (m).
  /// @param[in] maxSteer Maximum steering angle (degrees).
  /// @param[in] maxSpeed Maximum speed (m/sec).
  int initVehicle(double wheelBase, double maxSteer, double maxSpeed);

  // Initialize the graph from an RNDF file.
  int loadRndf(const char *filename);

  // Initialize the graph from a map.
  // TODO int loadMap(Map *map);

  private:

  // Vehicle control properties
  double wheelBase, maxSteer, maxSpeed;
  
  private:

  // TODO: graph generation could be moved to a seperate class.
  
  // Generate lane nodes
  int genLanes(std::RNDF *rndf);

  // Generate a lane, driving either with traffic (direction = +1) or
  // against traffic (direction = -1).
  int genLane(std::Lane *lane, int direction);

  // Generate lane tangents
  int genLaneTangents(std::Lane *lane);
  
  // Generate turn manuevers
  int genTurns(std::RNDF *rndf);

  // Generate lane-change manuevers
  int genChanges(std::RNDF *rndf);
  
  // Generate a maneuver linking two nodes
  int genManeuver(GraphNode *nodeA, GraphNode *nodeB, int nodeType);

  public:

  /// @brief Update the vehicle node and associated maneuvers.
  ///
  /// Unlike the rest of the graph, this must be done dynamically.
  int genVehicleSubGraph(const VehicleState *state);

  /// @brief Set the current vehicle state data
  int updateState(const VehicleState *state);

  /// @brief Set the vehicle size for c-space calculations.
  ///
  /// This function can be called before ::makeCSpace to dynamically
  /// alter the effective vehicle size (e.g., to switch between
  /// agressive and conservative behaviors).
  ///
  /// All measurements are relative to the vehicle origin, which is
  /// usually the middle of the rear axel.
  ///
  /// @param[in] rear  Distance to the rear of the vehicle (m).
  /// @param[in] front Distance to the front of the vehicle (m).
  /// @param[in] left  Distance to the left side of the vehicle (m).
  /// @param[in] right Distance to the right side of the vehicle (m).
  int setVehicleSize(double rear, double front, double left, double right);

  /// @brief Construct configuration space from the given map
  int makeCSpace(Map *map);

  /// @brief Add a fake car to the map (for testing).
  int addFakeCar(Map *map, const GraphNode *node);

  private:
  
  // Check for collision between a node and a map element
  bool checkCollision(GraphNode *node, MapElement *mapel,
                      float mapelAx, float mapelBx, float mapelAy, float mapelBy);

  // Vehicle c-space bounding box
  float vehicleAx, vehicleBx, vehicleAy, vehicleBy;

  // Radius of vehicle c-space circle
  float vehicleRadius;
  
  public:
  
  /// @brief Construct a plan for reaching the given checkpoint
  int makePlan(int checkId);

  /// @brief Construct path.
  ///
  /// @param[out] path The planned path, including some meta-data.
  int makePath(GraphPath *path);

  private:

  // Push node onto the priority queue
  int pushNode(GraphNode *node, int planCost);
    
  // Pop node from the priority queue
  GraphNode *popNode();

  // Priority queue for Dijkstra
  int queueLen;
  GraphNode *queue[GRAPH_MAX_NODES];

  public:
  
  /// @brief Generate a vehicle trajectory.
  int makeTraj(const VehicleState *state, GraphPath *path, CTraj *traj);
  
  // TODO: make private
  public:

  // Vehicle node in graph
  GraphNode *vehicleNode;

  public:
  
  // Global plan graph
  Graph graph;
};



#endif
