
/* 
 * Desc: Graph-based planner; functions for graph generation
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>

#include <frames/pose3.h>
#include <rndf/RNDF.hh>
#include <trajutils/maneuver.h>

#include "GraphPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// TODO move to an interolation library 
// Compute Catmull-Rom spline interpolation.
// Based on code from: http://www.lighthouse3d.com/opengl/maths/index.php?catmullrom.
double catmullRomSpline(double x, double v0, double v1, double v2, double v3)
{
  //const double M11 = 0.0;
  const double M12 = 1.0;
  //const double M13 = 0.0;
  //const double M14 = 0.0;
  const double M21 =-0.5;
  //const double M22 = 0.0;
  const double M23 = 0.5;
  //const double M24 = 0.0;
  const double M31 = 1.0;
  const double M32 =-2.5;
  const double M33 = 2.0;
  const double M34 =-0.5;
  const double M41 =-0.5;
  const double M42 = 1.5;
  const double M43 =-1.5;
  const double M44 = 0.5;
  
  double c1,c2,c3,c4;

	c1 =  	      M12*v1;
	c2 = M21*v0          + M23*v2;
	c3 = M31*v0 + M32*v1 + M33*v2 + M34*v3;
	c4 = M41*v0 + M42*v1 + M43*v2 + M44*v3;

	return (((c4*x + c3)*x +c2)*x + c1);
}


// TODO move to an interpolation library
// Generate the interpolated value for a cardinal spline.
float cardinalSpline(float c, float t, float p0, float p1, float p2, float p3)
{
  float m1, m2;
  float ha, hb, hc, hd;

  m1 = (1 - c) * (p2 - p0) / 2;
  m2 = (1 - c) * (p3 - p1) / 2;

  ha =  2*t*t*t - 3*t*t + 1;
  hb =    t*t*t - 2*t*t + t;
  hc = -2*t*t*t + 3*t*t;
  hd =    t*t*t -   t*t;
  
  return ha * p1 + hb * m1 + hc * p2 + hd * m2;  
}


// Generate lane nodes
int GraphPlanner::genLanes(std::RNDF *rndf)
{
  int sn, ln;
  std::Segment *segment;
  std::Lane *lane;

  // Iterate through segments and lanes, adding intermediate
  // nodes between waypoints in the same lane.
  for (sn = 0; sn < (int) rndf->getAllSegments().size(); sn++)
  {
    segment = rndf->getAllSegments()[sn];    
    for (ln = 0; ln < (int) segment->getAllLanes().size(); ln++)
    {
      lane = segment->getAllLanes()[ln];

      // Generate nodes for driving with traffic
      this->genLane(lane, +1);
      this->genLaneTangents(lane);

      // Generate nodes for driving against traffic
      this->genLane(lane, -1);
      this->genLaneTangents(lane);
    }
  }
  return 0;
}


// Generate a lane
int GraphPlanner::genLane(std::Lane *lane, int direction)
{
  int wn;
  int size;
  std::vector<std::Waypoint*> waypoints;
  std::Waypoint *wa, *wb, *wc, *wd;
  GraphNode *node;
  GraphArc *arc;
  double stepSize;
  int i, numSteps;
  double dx, dy, dm, s;
  vec3_t pos;
      
  // MAGIC
  // Spacing for interpolated lane nodes
  stepSize = 1.0;

  // List of all waypoints in the lane
  waypoints = lane->getAllWaypoints();
  size = (int) waypoints.size();
  
  for (wn = 0; wn < size; wn++)
  {
    // Pick out four waypoints for interpolation.  The lane nodes will
    // be generated between b and c.
    wa = wb = wc = wd = NULL;
    if (direction > 0)
    {
      if (wn - 1 >= 0)
        wa = waypoints[wn - 1];
      if (true)
        wb = waypoints[wn + 0];
      if (wn + 1 < (int) waypoints.size())
        wc = waypoints[wn + 1];
      if (wn + 2 < (int) waypoints.size())
        wd = waypoints[wn + 2];
    }
    else if (direction < 0)
    {
      if (wn - 1 >= 0)
        wa = waypoints[size - 1 - (wn - 1)];
      if (true)
        wb = waypoints[size - 1 - (wn + 0)];
      if (wn + 1 < (int) waypoints.size())
        wc = waypoints[size - 1 - (wn + 1)];
      if (wn + 2 < (int) waypoints.size())
        wd = waypoints[size - 1 - (wn + 2)];
    }
    else
    {
      assert(false);
    }

    // Create a node for waypoint b
    node = this->graph.createNode();
    assert(node);
    node->type = GRAPH_NODE_LANE;
    node->direction = direction;
    node->isWaypoint = (direction > 0);
    node->isEntry = wb->isEntry();
    node->isExit = wb->isExit();
    node->isCheckpoint = wb->isCheckpoint();        
    node->segmentId = wb->getSegmentID();
    node->laneId = wb->getLaneID();
    node->waypointId = wb->getWaypointID();
    node->checkpointId = wb->getCheckpointID();
    node->poseGlobal.pos = vec3_set(wb->getNorthing(), wb->getEasting(), 0);
      
    // Create an arc from the previous node to the new node
    if (wa)
      arc = this->graph.createArc(node->index - 1, node->index);

    // Stop here if b is the last waypoint in the lane.
    if (!wc)
      continue;
        
    // Distance between nodes
    dx = wc->getNorthing() - wb->getNorthing();
    dy = wc->getEasting() - wb->getEasting();
    dm = sqrt(dx*dx + dy*dy);

    // Number of intermediate nodes
    numSteps = (int) ceil(dm / stepSize);
    
    for (i = 1; i < numSteps; i++)
    {
      s = (double) i / numSteps;

      if (wa && wd)
      {
        // Spline interpolation
        pos.x = catmullRomSpline(s,
                                 wa->getNorthing(), wb->getNorthing(),
                                 wc->getNorthing(), wd->getNorthing());
        pos.y = catmullRomSpline(s,
                                 wa->getEasting(), wb->getEasting(),
                                 wc->getEasting(), wd->getEasting());
        pos.z = 0; 
      }
      else
      {
        // Linear interpolation
        pos.x = s * dx + wb->getNorthing();
        pos.y = s * dy + wb->getEasting();
        pos.z = 0; 
      }
      
      // Create a interpolated node between waypoints b and c
      node = this->graph.createNode();
      node->type = GRAPH_NODE_LANE;
      node->direction = direction;
      node->segmentId = wb->getSegmentID();
      node->laneId = wb->getLaneID();
      node->poseGlobal.pos = pos;
      
      // Create an arc frome the previous node to the new node
      arc = this->graph.createArc(node->index - 1, node->index);
    }
  }
  
  return 0;
}


// Generate lane tangents
int GraphPlanner::genLaneTangents(std::Lane *lane)
{
  int i;
  GraphNode *na, *nb, *nc;
  double th;

  for (i = 0; i < this->graph.getNodeCount(); i++)
  {
    nb = this->graph.getNode(i);
    if (nb->laneId != lane->getLaneID())
      continue;

    na = this->graph.getPrevLaneNode(nb->index);
    nc = this->graph.getNextLaneNode(nb->index);

    // Crude tangent calculation across nearby points.
    if (na && nc)
    {
      th = atan2(nc->poseGlobal.pos.y - na->poseGlobal.pos.y,
                 nc->poseGlobal.pos.x - na->poseGlobal.pos.x);
    }
    else if (na)
    {
      th = atan2(nb->poseGlobal.pos.y - na->poseGlobal.pos.y,
                 nb->poseGlobal.pos.x - na->poseGlobal.pos.x);
    }
    else if (nc)
    {
      th = atan2(nc->poseGlobal.pos.y - nb->poseGlobal.pos.y,
                 nc->poseGlobal.pos.x - nb->poseGlobal.pos.x);
    }
    else
      assert(false);

    nb->poseGlobal.rot = quat_from_rpy(0, 0, th);
  }

  return 0;
}


// Generate turn maneuvers
int GraphPlanner::genTurns(std::RNDF *rndf)
{
  int sn, ln, wn, vn;
  std::Segment *segment;
  std::Lane *lane;
  std::Waypoint *wpA;
  std::GPSPoint *wpB;
  GraphNode *nodeA, *nodeB;
  
  // Iterate through waypoints in the RNDF
  for (sn = 0; sn < (int) rndf->getAllSegments().size(); sn++)
  {
    segment = rndf->getAllSegments()[sn];    
    for (ln = 0; ln < (int) segment->getAllLanes().size(); ln++)
    {
      lane = segment->getAllLanes()[ln];
      for (wn = 0; wn < (int) lane->getAllWaypoints().size(); wn++)
      {
        wpA = lane->getAllWaypoints()[wn];

        for (vn = 0; vn < (int) wpA->getEntryPoints().size(); vn++)
        {
          wpB = wpA->getEntryPoints()[vn];

          //MSG("turn %d.%d.%d %d.%d.%d",
          //    wpA->getSegmentID(), wpA->getLaneID(), wpA->getWaypointID(),
          //    wpB->getSegmentID(), wpB->getLaneID(), wpB->getWaypointID());
          
          nodeA = this->graph.getNodeFromRndfId(wpA->getSegmentID(),
                                                wpA->getLaneID(),
                                                wpA->getWaypointID());
          assert(nodeA);

          nodeB = this->graph.getNodeFromRndfId(wpB->getSegmentID(),
                                                wpB->getLaneID(),
                                                wpB->getWaypointID());

          // Skip turns from zones
          if (!nodeB)
            continue;

          // Generate the turn maneuver
          if (this->genManeuver(nodeA, nodeB, GRAPH_NODE_TURN) != 0)
            MSG("warning: unable to turn from %d.%d.%d to %d.%d.%d",
                wpA->getSegmentID(), wpA->getLaneID(), wpA->getWaypointID(),
                wpB->getSegmentID(), wpB->getLaneID(), wpB->getWaypointID());
        }
      }
    }
  }
  
  return 0;
}


// Generate lane-change maneuvers
int GraphPlanner::genChanges(std::RNDF *rndf)
{
  int numNodes;
  int na, nb;
  GraphNode *nodeA, *nodeB;
  vec3_t pa, pb;
  double dm;
  
  numNodes = this->graph.getNodeCount();

  // Consider all pairs of lane nodes
  for (nb = 0; nb < numNodes; nb++)
  {    
    for (na = 0; na < numNodes; na++)
    {
      nodeA = this->graph.getNode(na);
      nodeB = this->graph.getNode(nb);

      // Consider lane nodes only
      if (!(nodeA->type == GRAPH_NODE_LANE && nodeB->type == GRAPH_NODE_LANE))
        continue;

      // Look for nodes that are the same segment, but different lanes.
      if (nodeA->segmentId != nodeB->segmentId)
        continue;
      if (nodeA->laneId == nodeB->laneId)
        continue;

      // Space out the lane-changes so we dont overload the graph
      if (!(nodeA->index % 10 == 0 && nodeB->index % 10 == 0)) // MAGIC
        continue;

      // Nodes must have similar alignment
      pa = vec3_rotate(nodeA->poseGlobal.rot, vec3_set(1, 0, 0));
      pb = vec3_rotate(nodeB->poseGlobal.rot, vec3_set(1, 0, 0));
      if (acos(vec3_dot(pa, pb)) > 45 * M_PI/180) // MAGIC
        continue;
      
      // Nodes must be close, but not too close
      dm = vec3_mag(vec3_sub(nodeB->poseGlobal.pos, nodeA->poseGlobal.pos));
      if (dm < 10 || dm > 30) // MAGIC
        continue;

      // The destination must be ahead of the source
      pb = vec3_transform(pose3_inv(nodeA->poseGlobal), nodeB->poseGlobal.pos);
      if (pb.x < 0)
        continue;

      //MSG("change %d %d.%d %d %d.%d %d",
      //    na, nodeA->segmentId, nodeA->laneId,
      //    nb, nodeB->segmentId, nodeB->laneId,
      //    this->graph.getNodeCount());
      
      // Generate the lane-change maneuver.  It is generally ok
      // if we cannot make a particular maneuver here.
      this->genManeuver(nodeA, nodeB, GRAPH_NODE_CHANGE);
    }
  }
    
  return 0;
}


// Generate or update the vehicle node and associated maneuvers.
// Unlike the rest of the graph (which is initialized once), this must
// be done continuously.
int GraphPlanner::genVehicleSubGraph(const VehicleState *state)
{
  int i;
  GraphNode *nodeA, *nodeB;
  //vec3_t pb;
  double dm;

  // Clear the volatile portion of the graph
  this->graph.clearVolatile();
    
  // Create vehicle node 
  nodeA = this->graph.createNode();
  assert(nodeA);
  nodeA->type = GRAPH_NODE_VEHICLE;    

  // Remember the node for later use.  
  this->vehicleNode = nodeA;

  // Set the node pose
  nodeA->poseGlobal.pos = vec3_set(state->utmNorthing, state->utmEasting, state->utmAltitude);
  nodeA->poseGlobal.rot = quat_from_rpy(state->utmRoll, state->utmPitch, state->utmYaw);
  nodeA->poseLocal.pos = vec3_set(state->localX, state->localY, state->localZ);
  nodeA->poseLocal.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);
  
  // Find destination nodes on the static graph.
  //
  // TODO: This should really be checking things like whether or not
  // lanes are adjacent, and also doing a quick rejection of unfeasible
  // destinations (e.g., on distance and minimum turning radius).    
  for (i = 0; i < this->graph.getNodeCount(); i++)
  {
    nodeB = this->graph.getNode(i);
    if (nodeB == nodeA)
      continue;

    // Must be lane nodes 
    if (!(nodeB->type == GRAPH_NODE_LANE || nodeB->type == GRAPH_NODE_TURN))
      continue;

    //if (nodeB->type == GRAPH_NODE_LANE && nodeB->index % 2 != 0)
    //  continue;

    // Nodes must be reasonably close, but not too close.
    dm = vec3_mag(vec3_sub(nodeB->poseGlobal.pos, nodeA->poseGlobal.pos));
    if (dm < 2 || dm > 15) // MAGIC
      continue;

    // The destination must be ahead of the source
    //pb = vec3_transform(pose3_inv(nodeA->poseGlobal), nodeB->poseGlobal.pos);
    //if (fabs(atan2(pb.y, pb.x)) > 135 * M_PI/180) // MAGIC
    //  continue;

    // Generate the maneuver
    if (this->genManeuver(nodeA, nodeB, GRAPH_NODE_VOLATILE) != 0)
      continue;
  }
  
  return 0;
}



// Generate a maneuver linking two nodes
int GraphPlanner::genManeuver(GraphNode *nodeA, GraphNode *nodeB, int nodeType)
{
  Vehicle *vp;
  Maneuver *mp;
  Pose2D poseA, poseB, pose;
  VehicleConfiguration config;
  double roll, pitch, yaw;
  double stepSize, dm, s;
  int i, numSteps;
  bool feasible;
  GraphNode *src, *dst;
  GraphArc *arc;

  assert(nodeA);
  assert(nodeB);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Initial vehicle pose
  poseA.x = nodeA->poseGlobal.pos.x;
  poseA.y = nodeA->poseGlobal.pos.y;
  quat_to_rpy(nodeA->poseGlobal.rot, &roll, &pitch, &yaw);
  poseA.theta = yaw;

  // Final vehicle pose
  poseB.x = nodeB->poseGlobal.pos.x;
  poseB.y = nodeB->poseGlobal.pos.y;
  quat_to_rpy(nodeB->poseGlobal.rot, &roll, &pitch, &yaw);
  poseB.theta = yaw;

  //MSG("man %f %f : %f %f", poseA.x, poseA.y, poseB.x, poseB.y);

  // Create maneuver object
  mp = maneuver_twopoint(vp, &poseA, &poseB);

  // Some maneuvers may not be possible, so trap this.
  if (!mp)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  // MAGIC
  stepSize = 1.0;
  
  // Distance between nodes
  dm = vec3_mag(vec3_sub(nodeB->poseGlobal.pos, nodeA->poseGlobal.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / stepSize);

  // Dont do very, very short maneuvers.
  if (numSteps < 1)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  // Check for unfeasable maneuvers by looking at the steering limit.
  feasible = true;
  for (i = 0; i < numSteps; i++)
  {
    s = (double) i / numSteps;

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    //fprintf(stdout, "config %d %f %f %f %f %f\n",
    //        i, s, config.x, config.y, config.theta, config.phi);

#warning "Workaround for bad maneuvers; remove"
    if (acos(cos(config.theta - poseA.theta)) > 120*M_PI/180)
      feasible = false;
    
    // Discard turns outside the steering limit
    if (fabs(config.phi) > vp->steerlimit)
      feasible = false;
  }

  if (!feasible)
  {
    maneuver_free(mp);
    free(vp);
    return -1;      
  }

  // Create the intermediate nodes for this maneuver
  src = nodeA;  
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the pose at this step
    pose = maneuver_evaluate_pose(vp, mp, s);
    //MSG("pose %d %f %f %f %f", i, s, pose.x, pose.y, acos(cos(pose.theta)));
    
    // Create a maneuver node
    dst = this->graph.createNode();
    assert(dst);
    dst->type = nodeType;
    dst->direction = +1;
    dst->poseGlobal.pos = vec3_set(pose.x, pose.y, 0);
    dst->poseGlobal.rot = quat_from_rpy(0, 0, pose.theta);
    
    // Create an arc from the previous node to the new node
    arc = this->graph.createArc(src->index, dst->index);    
    src = dst;
  }

  // Create an arc to the final node
  dst = nodeB;
  arc = this->graph.createArc(src->index, dst->index);

  maneuver_free(mp);
  free(vp);
  
  return 0;
}




