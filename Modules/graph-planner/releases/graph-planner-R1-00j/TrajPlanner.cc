
/* 
 * Desc: Graph-based planner; functions for local trajectory generation
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <frames/pose3.h>
#include <trajutils/maneuver.h>
#include <trajutils/man2traj.hh>
#include <trajutils/traj.hh>

#include "TrajPlanner.hh"


#define MIN(a,b) ((a) < (b) ? (a) : (b))

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
TrajPlanner::TrajPlanner(Graph* graph)
{
  this->graph = graph;

  this->speeds.maxLaneSpeed = 4.0;
  this->speeds.maxTurnSpeed = 2.0;
  this->speeds.maxNearSpeed = 1.0;
  this->speeds.maxOffRoadSpeed = 1.0;
  this->speeds.maxStopSpeed = 0.3;
  
  return;
}


// Destructor
TrajPlanner::~TrajPlanner()
{
  return;
}


// Get the vehicle kinematics
int TrajPlanner::getKinematics(TrajPlannerKinematics *kin)
{
  *kin = this->kin;
  return 0;
}


// Current vehicle kinematic values.
int TrajPlanner::setKinematics(const TrajPlannerKinematics *kin)
{
  this->kin = *kin;
  return 0;
}


// Get the speed limits.
int TrajPlanner::getSpeeds(TrajPlannerSpeeds *speeds)
{
  *speeds = this->speeds;
  return 0;
}


// Set the speed limits.
int TrajPlanner::setSpeeds(const TrajPlannerSpeeds *speeds)
{
  this->speeds = *speeds;
  return 0;
}


// Initialize speed limits from an MDF file.
int TrajPlanner::loadMDF(const char *filename)
{
  MSG("loading %s", filename);
  if (this->mdf.loadFile(filename) != 0)
    return ERROR("failed loading MDF");
  
  return 0;
}


// Generate a vehicle trajectory.
int TrajPlanner::makeTraj(const GraphPath *path,
                          const VehicleState *vehicleState,
                          const ActuatorState *actuatorState,                           
                          CTraj *traj)
{
  int i;
  double roll, pitch, yaw;
  Vehicle *vp;
  int numManeuvers;
  Maneuver *maneuvers[64];
  double speeds[64 + 1];
  GraphNode *nodeA, *nodeB;
  VehicleConfiguration configA;  
  Pose2D poseB;
  double speedA, speedB;
  bool collide;
  
  assert(path->pathLen >= 1);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->kin.wheelBase, this->kin.maxSteer);
      
  // Use the initial configuration given by the state.
  // This causes oscillations.
  //configA.x = vehicleState->utmNorthing;
  //configA.y = vehicleState->utmEasting;
  //configA.theta = vehicleState->utmYaw;
  //configA.phi = actuatorState->m_steerpos * this->kin.maxSteer;
  //speedA = vehicleState->vehSpeed;

  // Use the initial configuration given by the node.
  // This produces more stable behavior.
  nodeA = path->path[0];
  quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = yaw;
  configA.phi = nodeA->steerAngle;
  speedA = this->selectSpeed(path, nodeA);

  nodeB = NULL;

  collide = false;
  numManeuvers = 0;
    
  for (i = 1; i < path->pathLen && i < 40; i++) // MAGIC
  {
    nodeB = path->path[i];

    // Everything after a collision will have zero speed,
    // so record the fact here.
    if (nodeB->collideObs > 1 || nodeB->collideCar > 1)
      collide = true;

    // Look for a node at which to terminate the maneuver:
    //  - when the type changes
    //  - when the collision state changes
    if (nodeB->type != nodeA->type ||
        nodeB->collideObs != nodeA->collideObs ||
        nodeB->collideCar != nodeA->collideCar)
    {
      // We can get weird trajectories if the start and end nodes are very close,
      // so this forces them apart.  This is a workaround and should be replaced.
      if (vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos)) > 1) // MAGIC
      {
        quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
        poseB.x = nodeB->pose.pos.x;
        poseB.y = nodeB->pose.pos.y;
        poseB.theta = yaw;
        speedB = this->selectSpeed(path, nodeB);

        // Create maneuver
        assert((size_t) numManeuvers < sizeof(maneuvers)/sizeof(maneuvers[0]));
        maneuvers[numManeuvers] = maneuver_config2pose(vp, &configA, &poseB);
        speeds[numManeuvers + 0] = speedA;
        speeds[numManeuvers + 1] = speedB;
        numManeuvers++;

        configA.x = poseB.x;
        configA.y = poseB.y;
        configA.theta = poseB.theta;
        configA.phi = 0;
        speedA = speedB;
        nodeA = nodeB;
      }
    }
  }
  
  // Add final maneuver
  if (nodeB && nodeB != nodeA)
  {
    // We can get weird trajectories if the start and end nodes are very close,
    // so this forces them apart.  This is a workaround and should be replaced.
    if (vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos)) > 1) // MAGIC
    {
      quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
      poseB.x = nodeB->pose.pos.x;
      poseB.y = nodeB->pose.pos.y;
      poseB.theta = yaw;
      speedB = this->selectSpeed(path, nodeB);

      // Create maneuver
      assert((size_t) numManeuvers < sizeof(maneuvers)/sizeof(maneuvers[0]));
      maneuvers[numManeuvers] = maneuver_config2pose(vp, &configA, &poseB);
      speeds[numManeuvers + 0] = speedA;
      speeds[numManeuvers + 1] = speedB;
      numManeuvers++;
    }
  }

  if (numManeuvers == 0)
  {
    MSG("no valid destination, stopping vehicle");
    traj->startDataInput();
    traj->addPoint(nodeA->pose.pos.x, 0, 0,
                   nodeA->pose.pos.y, 0, 0);
    return 0;
  }
  
  // Generate a trajectory from the maneuver list.
  traj->startDataInput();
  maneuver_profile_generate(vp, numManeuvers, maneuvers, speeds, 50, traj); // MAGIC
  
  //traj->print(cout);
  //traj->printSpeedProfile(cout);
  //fprintf(stdout, "\n");  
    
  // Clean up
  while (numManeuvers > 0)
    maneuver_free(maneuvers[--numManeuvers]);
  free(vp);
  
  return 0;
}


// Assign a speed based on the local context
double TrajPlanner::selectSpeed(const GraphPath *path, const GraphNode *node)
{
  double speed;

  speed = this->speeds.maxLaneSpeed;
  
  if (node->segmentId == 0)
  {
    // Are we driving through an intersection?  If so, use the
    // intersection speed.
    speed = MIN(speed, this->speeds.maxTurnSpeed);
  }
  else
  {    
    // If driving in a segment, consider the maximum speed for this
    // segment.  The miminum speed is currently ignored.
    double minSpeed, maxSpeed;
    this->mdf.getSpeedLimits(node->segmentId, &minSpeed, &maxSpeed);
    if (maxSpeed > 0)
      speed = MIN(speed, maxSpeed);
  }

  // TODO: fix off-road test (lane-changes are incorrectly listed as off-road)
  // Are we driving off-road?
  //if (node->offRoad)
  //  speed = MIN(speed, this->speeds.maxOffRoadSpeed);
  
  // Are we in collision or near-collision
  if (node->collideObs > 1 || node->collideCar > 1)
    speed = 0;
  else if (node->collideObs > 0 || node->collideCar > 0)
    speed = MIN(speed, this->speeds.maxNearSpeed);
    
  // Are we at the goal?
  if (node->planCost < 500) // MAGIC
    speed = 0;

  // Are we approaching a stop line?
  if (true)
  {
    int i, j;
    GraphNode *nodeA, *nodeB;

    // Find the node in the path
    nodeA = NULL;
    for (i = 0; i < path->pathLen; i++)
    {
      nodeA = path->path[i];
      if (nodeA == node)
        break;      
    }
    assert(nodeA);

    // Skip ahead to find the stop line; we drive slowing if
    // we are in the stopping "zone" for a California rolling stop.
    for (j = i + 7; j < i + 7 + 2 && j < path->pathLen; j++) // MAGIC HACK
    {
      nodeB = path->path[j];
      if (nodeA->segmentId > 0 && nodeB->isExit && nodeB->isStop)
        speed = MIN(speed, this->speeds.maxStopSpeed);
    }
  }

  return speed;
}

