
/* 
 * Desc: Graph planner viewer utility
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <limits.h>
#include <float.h>

#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <FL/Fl.H>
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <dgcutils/Fl_Glv_Window.H>
//#include <jplv/jplv_image.h> // Add this to DGC utils

#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>
#include <trajutils/traj.hh>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <rndf/MDF.hh>

// Live mode
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <map/MapElementTalker.hh>
#include <trajutils/TrajTalker.hh>

#include <map/Map.hh>
#include "GraphPlanner.hh"
#include "ConfigSpace.hh"
#include "TrajPlanner.hh"

#include "cmdline.h"


class App
{
  public:

  // Default constructor
  App();

  // Destructor
  ~App();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  // Initialize GUI
  int initGUI(int cols, int rows);

  // Finalize GUI
  int finiGUI();
 
  public:

  // Action callback
  static void onAction(Fl_Widget *w, int option);

  // View callback
  static void onView(Fl_Widget *w, int option);
  
  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  // Handle draw callbacks
  static void onDraw(Fl_Glv_Window *win, App *self);

  // Handle idle callbacks
  static void onIdle(App *self);

  public:

  // Switch to the vehicle frame
  void pushFrameVehicle(const VehicleState *state);

  // Switch to the local frame
  void pushFrameLocal(const VehicleState *state);

  // Switch to the a given pose
  void pushFramePose(pose3_t pose);

  // Revert to previous frame
  void popFrame();
  
  // Switch to viewport projection.
  void pushViewport();

  // Revert to non-viewport projection
  void popViewport();

  // Draw a set of axes
  void drawAxes(float size);

  // Draw the robot
  void drawAlice(float steerAngle);

  // Draw status info
  void drawStatus();

  // Draw the configuration space info for a node
  void drawNodeCSpace(GraphNode *node);

  // Predraw the map 
  void predrawMap();

  // Predraw the static graph
  void predrawStaticGraph();

  // Predraw the non-static graph data
  void predrawGraph();

  // Predraw the lane boundaries 
  void predrawStaticLanes();

  // Predraw the non-static lane data
  void predrawLanes();

  // Draw the path to the goal
  void predrawPath();

  // Draw the trajectory
  void predrawTraj();

  public:

  // Initialize live mode
  int initLive();

  // Finalize live mode
  int finiLive();

  // Initialize the algorithms
  int init();

  // Finalize the algorithms
  int fini();

  // Update the algorithms
  int update();
  
  public:

  // Command-line options
  struct gengetopt_args_info options;

  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;
  
  // 3D window
  Fl_Glv_Window *worldwin;

  // Should we pause?
  bool pause;

  // Should we advance a single step?
  bool step;
  
  // Should we quit?
  bool quit;

  // Should we capture a single frame?
  bool captureFrame;

  // Should we capture a movie?
  bool captureMovie;
  
  // Screen capture counter
  int capCount;

  // Display options
  bool viewChanges, viewTurns, viewVolatile, viewLanes, viewMap, viewCSpace, viewPath, viewTraj;

  public:
  
  // Operating mode: internal simulation or live
  enum {modeSim, modeLive} mode;

  // Mission file
  MDF mdf;

  // Current state data
  VehicleState vehState;

  // Current actuator data
  ActuatorState actState;

  // Global map
  Map map;

  // Global graph
  Graph *graph;
  
  // Global planner
  GraphPlanner *planner;

  // Configuration space predictor
  ConfigSpace *configSpace;
  
  // Current path 
  GraphPath path;

  // Trajectory planner
  TrajPlanner *trajPlanner;
  
  // Current trajectory
  CTraj *traj;

  // Time of last update
  uint64_t updateTime;

  // Current goal index
  int goalIndex;

  public:
  
  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Sensnet handle (for getting state)
  sensnet_t *sensnet;

  // Talkers for live mode
  CMapElementTalker *mapTalker;
  CTrajTalker *trajTalker;

  public:

  // Display offset (used to improve GL precision)
  vec3_t offset;
  
  // Display lists
  GLuint staticGraphList, graphList, staticLaneList, laneList, mapList, pathList, trajList;

  // Is the display dirty?
  bool dirty;

  // Is the static graph display dirty?
  bool dirtyStatic;
};



// Commands
enum
{
  APP_ACTION_PAUSE = 0x1000,
  APP_ACTION_STEP,
  APP_ACTION_CAPTURE_FRAME,
  APP_ACTION_CAPTURE_MOVIE,
  APP_VIEW_CHANGES,
  APP_VIEW_TURNS,
  APP_VIEW_VOLATILE,
  APP_VIEW_LANES,
  APP_VIEW_MAP,
  APP_VIEW_CSPACE,
  APP_VIEW_PATH,
  APP_VIEW_TRAJ
};


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
App::App()
{
  this->goalIndex = 0;

  this->graph = NULL;
  this->planner = NULL;
  this->trajPlanner = NULL;
  
  memset(&this->path, 0, sizeof(this->path));

  memset(&this->vehState, 0, sizeof(this->vehState));
  memset(&this->actState, 0, sizeof(this->actState));
  
  this->staticGraphList = 0;
  this->graphList = 0;
  this->staticLaneList = 0;
  this->laneList = 0;
  this->pathList = 0;
  this->mapList = 0;
  this->trajList = 0;

  this->updateTime = 0;

  cmdline_parser_init(&this->options);
  
  return;
}


// Destructor
App::~App()
{
  cmdline_parser_free(&this->options);
  
  return;
}


// Parse command-line options
int App::parseCmdLine(int argc, char **argv)
{
  // Run parser
  if (cmdline_parser(argc, argv, &this->options) != 0)
  {
    cmdline_parser_print_help();
    return -1;
  }

  if (this->options.sim_flag)
    this->mode = modeSim;
  else
    this->mode = modeLive;

  if (this->mode == modeLive)
  {
    // Fill out the spread name
    if (this->options.spread_daemon_given)
      this->spreadDaemon = this->options.spread_daemon_arg;
    else if (getenv("SPREAD_DAEMON"))
      this->spreadDaemon = getenv("SPREAD_DAEMON");
    else
      return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
    // Fill out the skynet key
    if (this->options.skynet_key_given)
      this->skynetKey = this->options.skynet_key_arg;
    else if (getenv("SKYNET_KEY"))
      this->skynetKey = atoi(getenv("SKYNET_KEY"));
    else
      this->skynetKey = 0;
  }

  return 0;
}


// Initialize stuff
int App::initGUI(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) App::onExit},
      {0},
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) App::onAction,
       (void*) APP_ACTION_PAUSE, FL_MENU_TOGGLE},
      {"Single step", '.', (Fl_Callback*) App::onAction,
       (void*) APP_ACTION_STEP},
      {"Capture frame", 'c', (Fl_Callback*) App::onAction,
       (void*) APP_ACTION_CAPTURE_FRAME},
      {"Capture movie", 'm', (Fl_Callback*) App::onAction,
       (void*) APP_ACTION_CAPTURE_MOVIE, FL_MENU_TOGGLE},
      {0},
      {"&View", 0, 0, 0, FL_SUBMENU},    
      {"Changes", FL_CTRL + 'e', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_CHANGES, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Volatile", FL_CTRL + 'v', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_VOLATILE, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Lanes", FL_CTRL + 'l', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_LANES, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Map", FL_CTRL + 'm', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_MAP, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"CSpace", FL_CTRL + 'c', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_CSPACE, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Path", FL_CTRL + 'p', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_PATH, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Traj", FL_CTRL + 't', (Fl_Callback*) App::onView,
       (void*) APP_VIEW_TRAJ, FL_MENU_TOGGLE | FL_MENU_VALUE},
      {0},
      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Graph Planner Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new Fl_Glv_Window(0, 30, cols, rows - 30, this, (Fl_Callback*) onDraw);

  this->mainwin->end();

  this->worldwin->set_hfov(40);
  this->worldwin->set_clip(2, 1000);
  this->worldwin->set_lookat(-1, 0, -100, 0, 0, 0, 0, 0, -1);

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  this->viewTurns = true;
  this->viewChanges = true;
  this->viewLanes = true;
  this->viewVolatile = true;
  this->viewMap = true;
  this->viewCSpace = true;
  this->viewPath = true;
  this->viewTraj = true;

  // Dont set this unless we also set the menu state.
  this->pause = false;  
  this->step = true;
  this->dirty = true;
  this->dirtyStatic = true;

  this->captureFrame = false;
  this->captureMovie = false;
  
  return 0;
}


// Finalize stuff
int App::finiGUI()
{
  assert(this->mainwin);
  delete this->mainwin;
  this->mainwin = NULL;

  return 0;
}


// Handle menu callbacks
void App::onAction(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data();
  if (option == APP_ACTION_PAUSE)
    self->pause = !self->pause;
  if (option == APP_ACTION_STEP)
    self->step = true;
  if (option == APP_ACTION_CAPTURE_FRAME)
  {
    self->captureFrame = true;
    self->worldwin->redraw();
  }
  if (option == APP_ACTION_CAPTURE_MOVIE)
    self->captureMovie = true;
  
  return;
}


// Handle menu callbacks
void App::onView(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data();

  if (option == APP_VIEW_TURNS)
  {
    self->viewTurns = !self->viewTurns;
    self->dirtyStatic = true;
  }
  if (option == APP_VIEW_CHANGES)
  {
    self->viewChanges = !self->viewChanges;
    self->dirtyStatic = true;
  }
  if (option == APP_VIEW_LANES) 
    self->viewLanes = !self->viewLanes;
  
  if (option == APP_VIEW_VOLATILE)
    self->viewVolatile = !self->viewVolatile;
  if (option == APP_VIEW_MAP)
    self->viewMap = !self->viewMap;  
  if (option == APP_VIEW_CSPACE)
  {
    self->viewCSpace = !self->viewCSpace;
    self->dirtyStatic = true;
  }
  if (option == APP_VIEW_PATH)
    self->viewPath = !self->viewPath;
  if (option == APP_VIEW_TRAJ)
    self->viewTraj = !self->viewTraj;

  self->dirty = true;
  self->worldwin->redraw();
  
  return;
}


// Handle menu callbacks
void App::onExit(Fl_Widget *w, int option)
{
  App *self;

  self = (App*) w->user_data();
  self->quit = true;

  return;
}


// Handle draw callbacks
void App::onDraw(Fl_Glv_Window *win, App *self)
{
  if (self->dirtyStatic)
  {
    self->predrawStaticGraph();
    self->predrawStaticLanes();
    self->dirtyStatic = false;
  }

  if (self->dirty)
  {
    if (self->viewLanes)
      self->predrawLanes();
    if (self->viewVolatile)
      self->predrawGraph();
    if (self->viewMap)
      self->predrawMap();
    if (self->viewPath)
      self->predrawPath();
    if (self->viewTraj)
      self->predrawTraj();
    self->dirty = false;
  }
  
  glPushMatrix();
  glTranslatef(-self->vehState.utmNorthing + self->offset.x,
               -self->vehState.utmEasting + self->offset.y,
               -0 + self->offset.z);
  
  // Switch to vehicle frame to draw axes
  self->pushFrameVehicle(&self->vehState);
  self->drawAxes(1.0);
  self->drawAlice(self->actState.m_steerpos * VEHICLE_MAX_AVG_STEER);
  self->popFrame();

  // Draw the map
  if (self->viewMap)
  {
    self->pushFrameLocal(&self->vehState);
    glCallList(self->mapList);
    self->popFrame();
  }

  // Draw the static graph
  glCallList(self->staticGraphList);

  // Draw the lanes
  if (self->viewLanes)
  {
    glCallList(self->staticLaneList);
    glCallList(self->laneList);
  }

  // Draw the volatile graph
  if (self->viewVolatile)
    glCallList(self->graphList);

  // Draw the path
  if (self->viewPath)
    glCallList(self->pathList);

  // Draw the trajectory
  if (self->viewTraj)
    glCallList(self->trajList);

  glPopMatrix();

  // Draw textual status
  self->pushViewport();
  self->drawStatus();
  self->popViewport();

  // Screen cap to file
  if (self->captureFrame || self->captureMovie)
  {
#ifdef JPLV_IMAGE_H
    jplv_image_t *image;
    char filename[1024];
    int vp[4];
    
    glGetIntegerv(GL_VIEWPORT, vp);
    image = jplv_image_alloc(vp[2], vp[3], 3, 8, 0, NULL);
    glReadPixels(vp[0], vp[1], vp[2], vp[3], GL_RGB, GL_UNSIGNED_BYTE, image->data);       
    snprintf(filename, sizeof(filename), "planner-%04d.pnm", self->capCount);
    MSG("writing %s", filename);
    jplv_image_write_pnm(image, filename, NULL);
    jplv_image_free(image);
#endif
    
    self->captureFrame = false;
    self->capCount++;
  }

  return;
}


// Switch to the vehicle frame
void App::pushFrameVehicle(const VehicleState *state)
{
  pose3_t pose;

  // Project the vehicle down to zero altitude
  pose.pos = vec3_set(state->utmNorthing, state->utmEasting, 0); 
  pose.rot = quat_from_rpy(state->utmRoll, state->utmPitch, state->utmYaw);

  return this->pushFramePose(pose);
}


// Switch to the local frame
void App::pushFrameLocal(const VehicleState *state)
{
  pose3_t gpose, lpose, pose;
  
  // Vehicle global pose
  gpose.pos = vec3_set(state->utmNorthing,
                       state->utmEasting,
                       state->utmAltitude);
  gpose.rot = quat_from_rpy(state->utmRoll,
                            state->utmPitch,
                            state->utmYaw);

  // Vehicle local pose
  lpose.pos = vec3_set(state->localX,
                       state->localY,
                       state->localZ);
  lpose.rot = quat_from_rpy(state->localRoll,
                            state->localPitch,
                            state->localYaw);

  // Transform from local to global frame
  pose = pose3_mul(gpose, pose3_inv(lpose));

  return this->pushFramePose(pose);
}


// Switch to the a given pose
void App::pushFramePose(pose3_t pose)
{
  float m[4][4];

  // Add in the display offset
  pose.pos.x -= this->offset.x;
  pose.pos.y -= this->offset.y;
  pose.pos.z -= this->offset.z;
  
  // Convert to matrix
  pose3_to_mat44f(pose, m);  

  // Transpose to column-major order for GL
  int i, j;
  float t[16];
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);
  
  return;
}


// Revert to previous frame
void App::popFrame()
{
  glPopMatrix();  
  return;
}


// Switch to viewport projection.
void App::pushViewport()
{
  GLint vp[4];
      
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Get the viewport dimensions
  glGetIntegerv(GL_VIEWPORT, vp);
  
  // Shift and rescale such that (0, 0) is the top-left corner and
  // (cols, rows) is the bottom-right corner.
  glTranslatef(-1.0, 1.0, 0);
  glScalef(2.0/vp[2], -2.0/vp[3], 1.0);

  /*
  // Test code
  glColor3f(1, 1, 1);
  glBegin(GL_QUADS);
  glVertex3f(0, 0, 0);
  glVertex3f(100, 0, 0);
  glVertex3f(100, 50, 0);
  glVertex3f(0, 50, 0);
  glEnd();
  */
  
  return;
}


// Revert to non-viewport projection
void App::popViewport()
{
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  return;
}


// Draw a set of axes
void App::drawAxes(float size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Draw the robot
void App::drawAlice(float steerAngle)
{
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(steerAngle * 180/M_PI, 0, 0, 1);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(steerAngle * 180/M_PI, 0, 0, 1);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Draw a rectangle showing the c-space guards
  if (true)
  {
    glColor3f(1, 0.5, 0);
    glBegin(GL_LINE_LOOP);
    glVertex2f(this->configSpace->outerAx, this->configSpace->outerAy);
    glVertex2f(this->configSpace->outerBx, this->configSpace->outerAy);
    glVertex2f(this->configSpace->outerBx, this->configSpace->outerBy);
    glVertex2f(this->configSpace->outerAx, this->configSpace->outerBy);
    glEnd();

    glColor3f(1, 0, 0);
    glBegin(GL_LINE_LOOP);
    glVertex2f(this->configSpace->innerAx, this->configSpace->innerAy);
    glVertex2f(this->configSpace->innerBx, this->configSpace->innerAy);
    glVertex2f(this->configSpace->innerBx, this->configSpace->innerBy);
    glVertex2f(this->configSpace->innerAx, this->configSpace->innerBy);
    glEnd();
  }

  return;
}


// TODO move to file with other GL helpers
// Draw a text box
void glDrawText(float size, const char *text)
{
  int i, count;
  float sx, sy;

  // Compute the scale factors in x/y directions, based on the 'M'
  // character.
  sx = glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, 'M');
  sy = sx * 1.3;
    
  glPushMatrix();
  glScalef(size / sx, size / sy, 1);
  glTranslatef(0, -sy, 0);

  glPushMatrix();
    
  for (i = 0, count = 0; i < (int) strlen(text); i++, count++)
  {
    if (text[i] == '\n')
    {
      glPopMatrix();
      glTranslatef(0, -sy, 0);
      glPushMatrix();
      count = 0;      
    }
    else
    {
      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[i]);
    }
  }

  glPopMatrix();
  glPopMatrix();

  return;
}


// Draw status info
void App::drawStatus()
{
  char text[1024];
  double speedA, speedB;
  
  // Speed according to the trajectory
  if (this->traj->getNumPoints() >= 2)
  {
    speedA = this->traj->getSpeed(0);
    speedB = this->traj->getSpeed(this->traj->getNumPoints() - 1);
  }
  else
  {
    speedA = -1;
    speedB = -1;
  }

     
  // Construct string
  snprintf(text, sizeof(text),
           "Speed : %.1f %.1f %.1f\n",
           this->vehState.vehSpeed, speedA, speedB
           );

  // Hmmm, somewhat hacky way to set the text size.
  // Account for viewport dimensions to preserve 1:1 aspect ratio.
  glPushMatrix();
  glScalef(12, -12, 1);
  glColor3f(1, 1, 0);
  glDrawText(1, text);
  glPopMatrix();
  
  return;
}


// Draw the configuration space info for a node
void App::drawNodeCSpace(GraphNode *node)
{
  GLUquadric *quad;
      
  if (node->collideObs)
    glColor3f(1, 0, 0);    
  else if (node->collideCar)
    glColor3f(1, 0, 0);
  else
    return;

  glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);

  this->pushFramePose(node->pose);    

  quad = gluNewQuadric();
    
  // Show the node
  gluDisk(quad, 0, 0.20, 16, 1);

  // Show the node orientation
  glBegin(GL_LINES);  
  glVertex3d(0, 0, 0);
  glVertex3d(0.40, 0, 0);
  glEnd();

  if (false)
  {
    // Draw outer vehicle dimensions TODO replace magic numbers
    glPushMatrix();
    glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
    glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
    glColor3f(0, 0, 1);
    glutWireCube(1.0);
    glPopMatrix();
  }

  this->popFrame();

  gluDeleteQuadric(quad);

  return;
}


// Predraw the map in the local frame
void App::predrawMap()
{
  int i, j;
  MapElement *mapel;
  point2_uncertain *point;
  
  if (this->mapList == 0)
    this->mapList = glGenLists(1);
  glNewList(this->mapList, GL_COMPILE);
  
  for (i = 0; i < (int) this->map.data.size(); i++)
  {
    mapel = &this->map.data[i];

    if (mapel->geometry.size() == 0)
      continue;
    
    // Draw the bounding box
    glPushMatrix();
    glTranslated(mapel->center.x, mapel->center.y, 0);
    glRotated(mapel->orientation * 180/M_PI, 0, 0, 1);
    glColor3f(1, 0, 0);
    glBegin(GL_LINE_LOOP);
    glVertex2d(-mapel->length/2, -mapel->width/2);
    glVertex2d(+mapel->length/2, -mapel->width/2);
    glVertex2d(+mapel->length/2, +mapel->width/2);
    glVertex2d(-mapel->length/2, +mapel->width/2);
    glEnd();
    glPopMatrix();

    // Draw points
    if (mapel->geometryType == GEOMETRY_POINTS)
    {
      glPointSize(2);
      glColor3f(1, 0, 0);
      glBegin(GL_POINTS);
      for (j = 0; j < (int) mapel->geometry.size(); j++)
      {
        point = &mapel->geometry[j];
        glVertex3d(point->x, point->y, 0);
      }
      glEnd();
    }

    // Draw polygon
    if (mapel->geometryType == GEOMETRY_POLY)
    {
      glPointSize(2);
      glColor3f(1, 0, 0);
      glBegin(GL_POLYGON);
      for (j = 0; j < (int) mapel->geometry.size(); j++)
      {
        point = &mapel->geometry[j];
        glVertex3d(point->x, point->y, 0);
      }
      glEnd();
    }
  }
  
  glEndList();
  
  return;
}


// Predraw the static graph in the local frame
void App::predrawStaticGraph()
{
  int i;
  GraphNode *node;
  GraphArc *arc;
  vec3_t pa, pb;
  GLUquadric *quad;
  
  if (this->staticGraphList == 0)
    this->staticGraphList = glGenLists(1);
  glNewList(this->staticGraphList, GL_COMPILE);

  quad = gluNewQuadric();
  
  // Draw nodes
  for (i = 0; i < this->graph->getStaticNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    assert(node);
        
    if (!this->viewTurns)
      if (node->type == GRAPH_NODE_TURN)
        continue;
    if (!this->viewChanges)
      if (node->type == GRAPH_NODE_CHANGE)
        continue;

    if (node->type == GRAPH_NODE_LANE)
    {
      if (node->isEntry)
        glColor3f(0, 1, 0);
      else if (node->isExit)
        glColor3f(1, 0, 0);
      else if (node->isCheckpoint)
        glColor3f(1, 0, 1);
      else if (node->isWaypoint)
        glColor3f(1, 1, 1);
      else
        glColor3f(0.5, 0.5, 1);
    }
    else
    {
      glColor3f(0.5, 0.5, 0.5);
    }
    
    pa = node->pose.pos;
    pb = vec3_transform(node->pose, vec3_set(0.4, 0, 0));

    pa = vec3_sub(pa, this->offset);
    pb = vec3_sub(pb, this->offset);

    // Show the node center
    glPointSize(3);
    glBegin(GL_POINTS);  
    glVertex3d(pa.x, pa.y, pa.z);
    glEnd();

    // Show the node orientation
    if (true && node->isWaypoint)
    {
      glBegin(GL_LINES);  
      glVertex3d(pa.x, pa.y, pa.z);
      glVertex3d(pb.x, pb.y, pa.z);    
      glEnd();
    }

    // If this has a stop line, show that
    if (node->isStop)
    {
      pa = vec3_transform(node->pose, vec3_set(0, -node->laneWidth/2, 0));
      pa = vec3_sub(pa, this->offset);
      pb = vec3_transform(node->pose, vec3_set(0, +node->laneWidth/2, 0));
      pb = vec3_sub(pb, this->offset);      
      glBegin(GL_LINES);
      glVertex3d(pa.x, pa.y, pa.z);
      glVertex3d(pb.x, pb.y, pa.z);    
      glEnd();
    }

    // Show node info
    if (true && node->isWaypoint)
    {
      char text[64];
      snprintf(text, sizeof(text), "%d.%d.%d CP %d\nPC %.3f",
               node->segmentId, node->laneId,
               node->waypointId, node->checkpointId,
               (node->planCost < GRAPH_PLAN_COST_MAX ? node->planCost * 1e-2 : -1));

      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glPushMatrix();
      glTranslated(node->pose.pos.x - this->offset.x,
                   node->pose.pos.y - this->offset.y,
                   node->pose.pos.z - this->offset.z);
      glRotatef(180, 0, 1, 0);
      gluDisk(quad, 0.50, 0.50, 16, 1);
      glDrawText(0.1, text);
      glPopMatrix();
    }

    if (this->viewCSpace)
      this->drawNodeCSpace(node);
  }

  // Draw arcs
  glBegin(GL_LINES);    
  for (i = 0; i < this->graph->getStaticNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    assert(node);

    for (arc = node->outFirst; arc != NULL; arc = arc->outNext)
    {
      if (!this->viewTurns)
        if (arc->nodeA->type == GRAPH_NODE_TURN || arc->nodeB->type == GRAPH_NODE_TURN)
          continue;
      if (!this->viewChanges)
        if (arc->nodeA->type == GRAPH_NODE_CHANGE || arc->nodeB->type == GRAPH_NODE_CHANGE)
          continue;

      glColor3f(0.5, 0.5, 0.5);
      glVertex3d(arc->nodeA->pose.pos.x - this->offset.x,
                 arc->nodeA->pose.pos.y - this->offset.y,
                 arc->nodeA->pose.pos.z - this->offset.z);
      glColor3f(1, 1, 1);
      glVertex3d(arc->nodeB->pose.pos.x - this->offset.x,
                 arc->nodeB->pose.pos.y - this->offset.y,
                 arc->nodeB->pose.pos.z - this->offset.z);
    }
  }
  glEnd();
  
  gluDeleteQuadric(quad);
    
  glEndList();

  return;
}


// Predraw the non-static graph info
void App::predrawGraph()
{
  int i;
  GraphNode *node;
  GraphArc *arc;
  
  if (this->graphList == 0)
    this->graphList = glGenLists(1);
  glNewList(this->graphList, GL_COMPILE);

  // Draw nodes
  for (i = this->graph->getStaticNodeCount(); i < this->graph->getNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    assert(node);

    // Stop stop nodes
    if (false && node->isStop)
    {
      glColor3f(1, 0, 0);
      glPointSize(9);
      glBegin(GL_POINTS);  
      glVertex3d(node->pose.pos.x - this->offset.x,
                 node->pose.pos.y - this->offset.y,
                 node->pose.pos.z - this->offset.z);
      glEnd();      
    }
    
    if (this->viewCSpace)
      this->drawNodeCSpace(node);
  }

  // Draw arcs
  glBegin(GL_LINES);
  for (i = this->graph->getStaticNodeCount(); i < this->graph->getNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    assert(node);

    for (arc = node->outFirst; arc != NULL; arc = arc->outNext)
    {
      if (!arc->nodeA->offRoad)
        glColor3f(0, 0, 0.5);
      else
        glColor3f(0.5, 0, 0);
      glVertex3d(arc->nodeA->pose.pos.x - this->offset.x,
                 arc->nodeA->pose.pos.y - this->offset.y,
                 arc->nodeA->pose.pos.z - this->offset.z);
      if (!arc->nodeB->offRoad)
        glColor3f(0, 0, 1);
      else
        glColor3f(1, 0, 0);
      glVertex3d(arc->nodeB->pose.pos.x - this->offset.x,
                 arc->nodeB->pose.pos.y - this->offset.y,
                 arc->nodeB->pose.pos.z - this->offset.z);
    }
  }
  glEnd();
    
  glEndList();

  return;
}


// Predraw the lane boundaries 
void App::predrawStaticLanes()
{
  int i;
  GraphNode *node;
  double px, py, qx, qy;

  if (this->staticLaneList == 0)
    this->staticLaneList = glGenLists(1);
  glNewList(this->staticLaneList, GL_COMPILE);

  glColor4f(1, 1, 0, 0.5);
  
  for (i = 0; i < this->graph->getStaticNodeCount(); i++)
  {
    node = this->graph->getNode(i);

    if (!(node->type == GRAPH_NODE_LANE || node->type == GRAPH_NODE_TURN))
      continue;
    
    glBegin(GL_LINES);
        
    px = -node->laneLength/2;
    py = -node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = +node->laneLength/2;
    py = -node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = +node->laneLength/2;
    py = +node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = -node->laneLength/2;
    py = +node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    glEnd();
  }

  glEndList();
  
  return;
}


// Predraw lanes with obstacles
void App::predrawLanes()
{
  int i;
  GraphNode *node;
  double px, py, qx, qy;

  if (this->laneList == 0)
    this->laneList = glGenLists(1);
  glNewList(this->laneList, GL_COMPILE);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      
  for (i = 0; i < this->graph->getStaticNodeCount(); i++)
  {
    node = this->graph->getNode(i);

    if (!(node->type == GRAPH_NODE_LANE || node->type == GRAPH_NODE_TURN))
      continue;

    if (node->obsInLane)
      glColor4f(1, 1, 0, 0.5);
    else if (node->carInLane)
      glColor4f(1, 1, 0, 0.5);
    else
      continue;
    
    glBegin(GL_POLYGON);
        
    px = -node->laneLength/2;
    py = -node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = +node->laneLength/2;
    py = -node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = +node->laneLength/2;
    py = +node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    px = -node->laneLength/2;
    py = +node->laneWidth/2;
    qx = node->transGN[0][0] * px + node->transGN[0][1] * py + node->transGN[0][3];
    qy = node->transGN[1][0] * px + node->transGN[1][1] * py + node->transGN[1][3];        
    glVertex2f(qx - this->offset.x, qy - this->offset.y);

    glEnd();
  }

  glDisable(GL_BLEND);
  
  glEndList();
  
  return;
}



// Draw the path in the global frame
void App::predrawPath()
{
  int i;
  GraphNode *node;
  
  if (this->pathList == 0)
    this->pathList = glGenLists(1);
  glNewList(this->pathList, GL_COMPILE);

  glLineWidth(2);
  glColor3f(1, 0, 1);
  glBegin(GL_LINE_STRIP);
  for (i = 0; i < this->path.pathLen; i++)
  {    
    node = this->path.path[i];
    assert(node);

    /* REMOVE
    // We start out coloring the path in green, but change to
    // red if we see a collision.
    if (node->collideObs || node->collideCar)
      glColor3f(1, 0, 0);
    */
      
    glVertex3d(node->pose.pos.x - this->offset.x,
               node->pose.pos.y - this->offset.y,
               node->pose.pos.z - 0.20 - this->offset.z);
  }
  glEnd();
  glLineWidth(1);
  
  glEndList();

  return;
}


// Draw the trajectory in the global frame
void App::predrawTraj()
{
  int i;
  vec3_t p, q;
  //float speed;
  
  if (this->trajList == 0)
    this->trajList = glGenLists(1);
  glNewList(this->trajList, GL_COMPILE);

  // Draw velocity vectors
  glPointSize(2.0);
  glColor3f(0, 1, 0);  
  for (i = 0; i < this->traj->getNumPoints(); i++)
  {
    p.x = this->traj->getNdiffarray(0)[i];
    p.y = this->traj->getEdiffarray(0)[i];    
    p.z = 0;

    q.x = p.x + this->traj->getNdiffarray(1)[i] * 0.5;
    q.y = p.y + this->traj->getEdiffarray(1)[i] * 0.5;    
    q.z = 0;

    glBegin(GL_LINES);
    glVertex3d(p.x - this->offset.x,
               p.y - this->offset.y,
               p.z - 0.30 - this->offset.z);
    glVertex3d(q.x - this->offset.x,
               q.y - this->offset.y,
               q.z - 0.30 - this->offset.z);
    glEnd();

    glBegin(GL_POINTS);
    glVertex3d(p.x - this->offset.x,
               p.y - this->offset.y,
               p.z - 0.30 - this->offset.z);
    glEnd();    
  }
  glPointSize(1.0);
  
  glEndList();

  return;
}


// Handle idle callbacks
void App::onIdle(App *self)
{
  if (!self->pause || self->step)
  {      
    // Update perceptor
    if (self->update() != 0)
    {
      self->quit = true;
      return;
    }

    // Redraw the display
    self->worldwin->redraw();
    
    // Single step mode: we've moved one step, so clear flag
    if (self->step)
      self->step = false;

    // Play nice and max out at 20Hz (dont busy-loop)
    usleep(50000);
  }
  else
  {
    // Sleepy bye-bye
    usleep(20000);
  }
  
  return;
}



// Initialize live mode
int App::initLive()
{
  // Create sensnet interface for state messages
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);

  // Open connection
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, MODgraphPlanner) != 0)
    return ERROR("unable to connect to sensnet");
 
  // Listen to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR,
                   SNstate, sizeof(this->vehState)) != 0)
    return ERROR("unable to join state group");

  // Listen to actuator state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR,
                   SNactuatorstate, sizeof(this->actState)) != 0)
    return ERROR("unable to join state group");

  // Create talker for map update
  this->mapTalker = new CMapElementTalker();
  this->mapTalker->initRecvMapElement(this->skynetKey, MODgraphPlanner);
  
  // Create talker for the output trajectory
  this->trajTalker = new CTrajTalker(MODgraphPlanner, this->skynetKey);
  
  return 0;
}


// Finalize live mode
int App::finiLive()
{
  // Clean up talkers
  delete this->trajTalker;
  this->trajTalker = NULL;
  delete this->mapTalker;
  this->mapTalker = NULL;

  // Clean up sensnet
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNactuatorstate);
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}


// Initialize the planner
int App::init()
{
  // Create graph
  this->graph = new Graph(this->options.max_nodes_arg, this->options.max_arcs_arg);
  
  // Create planner
  this->planner = new GraphPlanner(this->graph);
  assert(this->planner);

  // Set vehicle properties
  if (true)
  {
    GraphPlannerKinematics kin;
    this->planner->getKinematics(&kin);
    kin.wheelBase = VEHICLE_WHEELBASE;
    kin.maxSteer = this->options.max_steer_arg * M_PI/180;
    kin.maxTurn = this->options.max_turn_arg * M_PI/180;
    this->planner->setKinematics(&kin);
  }

  // Load up the RNDF and initialize the graph
  assert(this->options.rndf_given);
  if (this->planner->loadRndf(this->options.rndf_arg) != 0)
    return ERROR("failed loading RNDF");

  if (this->graph->getNodeCount() == 0)
    return ERROR("RNDF is empty (no nodes)");
  
  // Set the display offset (improves numerical conditioning for GL)
  if (true)
  {
    GraphNode *node;
    node = this->graph->getNode(0);
    assert(node);
    this->offset = node->pose.pos;
  }

  // Create configuration space predictor
  this->configSpace = new ConfigSpace(this->graph);
  assert(this->configSpace);

  // Set vehicle dimensions for configuration space calculations
  if (true)
  {
    ConfigSpaceDimensions dim;
    this->configSpace->getDimensions(&dim);    
    dim.front = this->options.front_dist_arg;
    dim.left = this->options.side_dist_arg;
    dim.right = this->options.side_dist_arg;
    this->configSpace->setDimensions(&dim);
  }

  // Create trajectory planner
  this->trajPlanner = new TrajPlanner(this->graph);
  assert(this->trajPlanner);
  
  // Set vehicle properties
  if (true)
  {
    TrajPlannerKinematics kin;
    this->trajPlanner->getKinematics(&kin);
    kin.wheelBase = VEHICLE_WHEELBASE;
    kin.maxSteer = this->options.max_steer_arg * M_PI/180;
    this->trajPlanner->setKinematics(&kin);

    TrajPlannerSpeeds speeds;
    this->trajPlanner->getSpeeds(&speeds);
    speeds.maxLaneSpeed = this->options.max_speed_arg;
    this->trajPlanner->setSpeeds(&speeds);
  }

  // Load up the MDF if given
  if (this->options.mdf_given)
  {
    MSG("loading %s", this->options.mdf_arg);
    if (this->mdf.loadFile(this->options.mdf_arg) != 0)
      return ERROR("failed loading MDF");
    MSG("MDF has %d checkpoints", this->mdf.getNumCheckpoints());
  }

  // In simulation mode, initialize the state to waypoint in the RNDF
  if (this->mode == modeSim)
  {
    int segment, lane, waypoint;
    GraphNode *node;

    // Default to the first node
    node = this->graph->getNode(0);

    if (this->options.rndf_start_given)
    {
      // Parse out the node from the command line args
      if (sscanf(this->options.rndf_start_arg, "%d.%d.%d", &segment, &lane, &waypoint) < 3)
        MSG("syntax error: %s", this->options.rndf_start_arg);
      node = this->graph->getNodeFromRndfId(segment, lane, waypoint);
      if (!node)
        MSG("no such waypoint: %s", this->options.rndf_start_arg);
    }

    // Set initial position and heading
    if (node)
    {
      double roll, pitch, yaw;      
      memset(&this->vehState, 0, sizeof(this->vehState));
      this->vehState.utmNorthing = node->pose.pos.x;
      this->vehState.utmEasting = node->pose.pos.y;
      this->vehState.utmAltitude = node->pose.pos.z;
      quat_to_rpy(node->pose.rot, &roll, &pitch, &yaw);
      this->vehState.utmYaw = yaw;
    }
  }

  // Create trajectory
  this->traj = new CTraj(3);
  
  return 0;
}


// Finalize the planner
int App::fini()
{
  // Clean up trajectory
  assert(this->traj);
  delete this->traj;
  this->traj = NULL;
    
  // Clean up traj planner
  assert(this->trajPlanner);
  delete this->trajPlanner;
  this->trajPlanner = NULL;

  // Clean up config space
  assert(this->configSpace);
  delete this->configSpace;
  this->configSpace = NULL;
    
  // Clean up planner
  assert(this->planner);
  delete this->planner;
  this->planner = NULL;
 
  // Clean up graph
  assert(this->graph);
  delete this->graph;
  this->graph = NULL;
  
  return 0;  
}


// Update the planner
int App::update()
{
  uint64_t time;

  if (this->mode == modeSim)
  {
    // In simulation mode, we step the vehicle along the current path.
    if (this->path.valid && this->path.pathLen >= 2)
    {
      GraphNode *node;
      pose3_t pose;
      double roll, pitch, yaw;
      
      node = this->path.path[1];

      if (node->collideObs < 2 && node->collideCar < 2)
      {
        pose = node->pose;
        quat_to_rpy(pose.rot, &roll, &pitch, &yaw);            
        this->vehState.utmNorthing = pose.pos.x;
        this->vehState.utmEasting = pose.pos.y;
        this->vehState.utmAltitude = pose.pos.z;
        this->vehState.utmRoll = roll;
        this->vehState.utmPitch = pitch;
        this->vehState.utmYaw = yaw;

        pose = node->pose;
        quat_to_rpy(pose.rot, &roll, &pitch, &yaw);            
        this->vehState.localX = pose.pos.x;
        this->vehState.localY = pose.pos.y;
        this->vehState.localZ = pose.pos.z;
        this->vehState.localRoll = roll;
        this->vehState.localPitch = pitch;
        this->vehState.localYaw = yaw;

        this->actState.m_steercmd = node->steerAngle / VEHICLE_MAX_AVG_STEER;
        this->actState.m_steerpos = this->actState.m_steercmd;
      }
    }    
  }
  else
  {
    MapElement mapel;
    
    // Get the vehicle state from sensnet
    sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
                 NULL, sizeof(this->vehState), &this->vehState);

    // Get the actuator state from sensnet
    sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNactuatorstate,
                 NULL, sizeof(this->actState), &this->actState);

    // Read any map elements and update the map.
    // This takes far longer than it should; possible skynet problems?
    time = DGCgettime();
    while (this->mapTalker->recvMapElementNoBlock(&mapel) > 0)
    {
      MSG("got map element %f %f %d",
          mapel.center.x, mapel.center.y, mapel.geometry.size());
      this->map.addEl(mapel);
    }
    MSG("map %lldms", (DGCgettime() - time) / 1000);
  }

  // Create static obstacles
  if (this->options.fake_obs_given > 0)
  {
    int i;
    int segment, lane, waypoint;
    GraphNode *node;
    
    for (i = 0; i < (int) this->options.fake_obs_given; i++)
    {
      if (sscanf(this->options.fake_obs_arg[i], "%d.%d.%d", &segment, &lane, &waypoint) < 3)
      {
        MSG("syntax error: %s", this->options.fake_obs_arg[i]);
        continue;
      }
      MSG("fake obstacle at %d.%d.%d", segment, lane, waypoint);
      node = this->graph->getNodeFromRndfId(segment, lane, waypoint);
      assert(node);
      this->configSpace->addFakeCar(&this->vehState, &this->map, node);
    }    
  }
  
  // Test code for logging the pose
  if (false)
  {
    // Log the pose
    fprintf(stdout, "pose %f %f %f %f %f %f steer %f %f\n",
            this->vehState.utmNorthing,
            this->vehState.utmEasting,
            this->vehState.utmAltitude,
            this->vehState.utmRoll,
            this->vehState.utmPitch,
            this->vehState.utmYaw,
            this->actState.m_steercmd * VEHICLE_MAX_AVG_STEER,
            this->actState.m_steerpos * VEHICLE_MAX_AVG_STEER);
  }

  // Throttle the update rate
  if (DGCgettime() - this->updateTime < (uint64_t) (1e6 / this->options.rate_arg))
    return 0;
  this->updateTime = DGCgettime();
  
  // Generate the sub-graph from the current vehicle state to the static graph
  time = DGCgettime();
  if (this->planner->genVehicleSubGraph(&this->vehState, &this->actState) != 0)
    return ERROR("failed creating vehicle subgraph");
  MSG("vehicle %lldms", (DGCgettime() - time) / 1000);

  // Update lanes
  time = DGCgettime();
  this->configSpace->updateLanes();
  MSG("lanes %lldms", (DGCgettime() - time) / 1000);

  // Update c-space
  time = DGCgettime();
  this->configSpace->updateMap(&this->vehState, &this->map);
  MSG("cspace %lldms", (DGCgettime() - time) / 1000);

  int goal = 0;
      
  if (this->options.mdf_given)
  {
    // Pick from the mdf list
    if (this->options.loop_arg > 0)
      this->goalIndex = this->goalIndex % this->mdf.getNumCheckpoints();
    if (this->goalIndex < (int) this->mdf.getNumCheckpoints())
      goal = this->mdf.getCheckpoint(this->goalIndex);
  }
  else if (this->options.goal_given)
  {
    // Pick from the goal list
    if (this->options.loop_arg > 0)
      this->goalIndex = this->goalIndex % this->options.goal_given;
    if (this->goalIndex < (int) this->options.goal_given)
      goal = this->options.goal_arg[this->goalIndex];
  }
   
  if (goal > 0)
  {
    // Make a plan for getting to the goal
    time = DGCgettime();
    this->planner->makePlan(goal); 
    MSG("plan %lldms", (DGCgettime() - time) / 1000);

    // Get the path to the goal, if there is one
    this->planner->makePath(&this->path);

    MSG("goal dist %.3f", this->path.goalDist);

    // If we are next to the goal, pick a new goal index for the next
    // time around.  This may involve looping around to the first goal
    // again.
    if (this->path.valid && this->path.goalDist < 4.0) // MAGIC
    {
      MSG("goal achieved");
      this->goalIndex += 1;      
    }

    // Create a trajectory from the path.  This includes the velocity
    // profile.
    time = DGCgettime();
    this->trajPlanner->makeTraj(&this->path, &this->vehState, &this->actState, this->traj);
    MSG("traj %lldms", (DGCgettime() - time) / 1000);
  }
  else
  {
    MSG("no goal");
    memset(&this->path, 0, sizeof(this->path));
    this->traj->startDataInput();
  }
  
  if (this->mode == modeLive)
  {
    int trajSocket;

    // TODO clean up the truly awful TrajTalker.
    // Get the socket id for sending messages.
    trajSocket = this->trajTalker->m_skynet.get_send_sock(SNtraj);

    // Send trajectory data to both followe and GUI
    assert(this->trajTalker);
    this->trajTalker->SendTraj(trajSocket, this->traj);
  }
  
  // Stuff has changed, so we need to redraw
  this->dirty = true;
  
  return 0;
}




int main(int argc, char *argv[])
{
  App *app;

  // Initialize GLUT calls
  glutInit(&argc, argv);

  app = new App();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  if (app->initGUI(1024, 768) != 0)
    return -1;

  // Initialize live mode
  if (app->mode == App::modeLive)
    if (app->initLive() != 0)
      return -1;

  // Initialize algorithms
  if (app->init() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) App::onIdle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  app->finiGUI();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
