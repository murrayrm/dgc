
/* 
 * Desc: Graph-based planner
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <float.h>
#include <string.h>

#include <frames/pose3.h>
#include <rndf/RNDF.hh>

#include "GraphPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
GraphPlanner::GraphPlanner(int maxNodes, int maxArcs)
    : graph(maxNodes, maxArcs)
{
  memset(&this->kin, 0, sizeof(this->kin));

  // Set some reasonable default plan constraints
  memset(&this->cons, 0, sizeof(this->cons));
  this->cons.laneCost = 100;
  this->cons.changeCost = 100;
  this->cons.headOnCost = 10000;
  this->cons.obsCost = 100000;
  this->cons.carCost = 0;
  
  // Make a guess at a suitable queue size for planning.
  this->queueLen = 0;
  this->queueMax = maxNodes / 16;
  this->queue = new (GraphNode*)[this->queueMax];
  
  return;
}


// Destructor
GraphPlanner::~GraphPlanner()
{
  delete [] this->queue;
  return;
}


// Get the vehicle kinematics
int GraphPlanner::getKinematics(GraphPlannerKinematics *kin)
{
  *kin = this->kin;
  return 0;
}

// Current vehicle kinematic values.
int GraphPlanner::setKinematics(const GraphPlannerKinematics *kin)
{
  this->kin = *kin;
  return 0;
}


// Load an RNDF and initialize the graph.
int GraphPlanner::loadRndf(const char *filename)
{
  std::RNDF *rndf;
    
  // Create RNDF object
  rndf = new std::RNDF();
  assert(rndf);

  // Load RNDF from file
  if (!rndf->loadFile(filename))
    return ERROR("unable to load %s", filename);
  
  // Create lane nodes
  if (genLanes(rndf) != 0)
    return -1;      

  // Create turn maneuvers
  if (genTurns(rndf) != 0)
    return -1;

  // Create lane-change nodes
  if (genChanges(rndf) != 0)
    return -1;

  // Fix the static portion of the map so we can add volatile bits
  // for the vehicle.
  this->graph.freezeStatic();

  // Clean up
  delete rndf;

  MSG("created %d nodes %d arcs",
      this->graph.getNodeCount(), this->graph.getArcCount());
  
  return 0;
}


// Update the local position of graph nodes using the state info.
int GraphPlanner::updateState(const VehicleState *vehicleState)
{
  int i;
  GraphNode *node;
  pose3_t gpose, lpose, pose;
  
  // Vehicle global pose
  gpose.pos = vec3_set(vehicleState->utmNorthing,
                       vehicleState->utmEasting,
                       vehicleState->utmAltitude);
  gpose.rot = quat_from_rpy(vehicleState->utmRoll,
                            vehicleState->utmPitch,
                            vehicleState->utmYaw);

  // Vehicle local pose
  lpose.pos = vec3_set(vehicleState->localX,
                       vehicleState->localY,
                       vehicleState->localZ);
  lpose.rot = quat_from_rpy(vehicleState->localRoll,
                            vehicleState->localPitch,
                            vehicleState->localYaw);

  // Transform from global to local frame
  pose = pose3_mul(lpose, pose3_inv(gpose));

  // Compute the local position for each node in the graph
  for (i = 0; true; i++)
  {
    node = this->graph.getNode(i);
    if (!node)
      break;

    // Use the current UTM altitude for the node Z; otherwise
    // various distance checks fail.
    node->poseGlobal.pos.z = vehicleState->utmAltitude;    
    node->poseLocal = pose3_mul(pose, node->poseGlobal);
  }
  
  return 0;
}


// Get the plan constraints
int GraphPlanner::getConstraints(GraphPlannerConstraints *cons)
{
  *cons = this->cons;
  return 0;
}

// Set the plan constraints
int GraphPlanner::setConstraints(const GraphPlannerConstraints *cons)
{
  this->cons = *cons;
  return 0;
}


// Construct a plan for reaching the given checkpoint
int GraphPlanner::makePlan(int checkpointId)
{
  int i;
  int planCost;
  GraphNode *goal, *src, *dst;
  GraphArc *arc;

  // Find the node for the goal checkpoint
  goal = this->graph.getNodeFromCheckpointId(checkpointId);
  if (!goal)
    return ERROR("unknown checkpoint %d", checkpointId);
  assert(goal->checkpointId == checkpointId);
  
  //MSG("goal: checkpoint %d RNDF %d.%d.%d",
  //    goal->checkpointId, goal->segmentId, goal->laneId, goal->waypointId);

  // Reset the queue
  this->queueLen = 0;
  
  // Initialize costs and priority queue
  for (i = 0; i < this->graph.getNodeCount(); i++)
  {
    dst = this->graph.getNode(i);
    dst->planCost = GRAPH_PLAN_COST_MAX;
  }

  // Initialize the goal cost
  this->pushNode(goal, 0);
  
  while (true)
  {
    // Get the waiting node from the queue
    dst = this->popNode();
    if (!dst)
      break;

    // Hmmm, looks like there is no plan to this node, so
    // we must be done.
    if (dst->planCost >= GRAPH_PLAN_COST_MAX)
      break;

    // Got the vehicle node, so we are done.
    // Comment this out to test worst-case planning performance.
    //if (dst->type == GRAPH_NODE_VEHICLE)
    //  break;

    planCost = dst->planCost;

    if (dst->type == GRAPH_NODE_CHANGE)
    {
      // Discourage too many lane changes
      if (this->cons.changeCost < 0)
        continue;
      planCost += this->cons.changeCost;
    }
    else
    {
      planCost += this->cons.laneCost;
    }

    // Discourage driving on the wrong side of the road.
    if (dst->direction < 0)
    {
      if (this->cons.headOnCost < 0)
        continue;
      planCost += this->cons.headOnCost;
    }

    // Discourage collisions
    if (dst->collideObs)
    {
      if (this->cons.obsCost < 0)
        continue;
      planCost += this->cons.obsCost;
    }
    
    // Consider all incoming arcs for this node
    for (arc = dst->inFirst; arc != NULL; arc = arc->inNext)
    {
      assert(arc->nodeB == dst);
      src = arc->nodeA;
      //MSG("src %d %d %f %d", i, src->id, src->planCost, src->direction); 
      if (planCost < src->planCost)
        this->pushNode(src, planCost);
    }    
  }
  
  return 0;
}


// Construct path
int GraphPlanner::makePath(GraphPath *path)
{
  int minCost;
  GraphArc *arc;
  GraphNode *srcNode, *dstNode, *minNode;

  if (!this->vehicleNode)
    return ERROR("no vehicle node");
  
  // Get the starting node
  assert(this->vehicleNode);
  srcNode = this->vehicleNode;

  path->valid = true;
  path->collideObs = false;
  path->collideCar = false;
  path->goalDist = 0;
  path->pathLen = 0;
  
  while (true)
  {
    //MSG("path %d %d %f", path->pathLen, srcNode->index, srcNode->planCost);
            
    assert((size_t) path->pathLen < sizeof(path->path)/sizeof(path->path[0]));
    path->path[path->pathLen++] = srcNode;
    
    // Does this path contains a collision?
    if (srcNode->collideObs)
      path->collideObs = true;
    if (srcNode->collideCar)
      path->collideCar = true;

    // No plan for this node
    if (srcNode->planCost == GRAPH_PLAN_COST_MAX)
      break;

    minCost = srcNode->planCost;
    minNode = NULL;
    
    // Consider all outgoing arcs for this node
    //for (i = 0; i < this->graph.getOutCount(srcNode->index); i++)
    for (arc = srcNode->outFirst; arc != NULL; arc = arc->outNext)
    {
      //arc = this->graph.getOutArc(srcNode->index, i);
      assert(arc->nodeA == srcNode);
      dstNode = arc->nodeB;

      if (dstNode->planCost < minCost)
      {
        minCost = dstNode->planCost;
        minNode = dstNode;
      }
    }
    if (!minNode)
      break;

    // Keep track of the path length
    path->goalDist += (float) vec3_mag(vec3_sub(minNode->poseLocal.pos, srcNode->poseLocal.pos));
    
    srcNode = minNode;
  }

  // See if we have a path
  if (path->pathLen < 2)
  {
    path->valid = false;
    MSG("no path to goal (plan len %d)", path->pathLen);
    return -1;
  }
  
  // See if we reached to goal
  if (srcNode->planCost > 0)
  {
    path->valid = false;
    MSG("no path to goal (cost %d)", srcNode->planCost);
    return -1;
  }

  MSG("path to goal (cost %d)", path->path[0]->planCost);  
  
  return 0;
}


// Push node onto the priority queue
int GraphPlanner::pushNode(GraphNode *node, int planCost)
{
  int i;
  GraphNode *a, *b;
  
  assert(this->queueLen < this->queueMax);
  
  if (node->planCost == GRAPH_PLAN_COST_MAX)
  {
    // If the node is not already in the queue, insert it at the beginning.
    memmove(this->queue + 1, this->queue, this->queueLen * sizeof(this->queue[0]));
    this->queue[0] = node;
    this->queueLen++;
  }

  // Set the new plan cost
  node->planCost = planCost;

  // Bubble sort the queue.  This can work in one pass, since at most
  // one node has changed, and the value of that node must be less
  // than it's previous value.  Hence we only need to move that one
  // node towards the end of the queue.
  for (i = 0; i < this->queueLen - 1; i++)
  {
    a = this->queue[i + 0];
    b = this->queue[i + 1];
    if (a->planCost < b->planCost)
    {
      this->queue[i + 0] = b;
      this->queue[i + 1] = a;
    }
  }

  //MSG("queuelen %d", this->queueLen);

  return 0;
}


// Pop node from the priority queue
GraphNode *GraphPlanner::popNode()
{
  // Check for empty queue
  if (this->queueLen == 0)
    return NULL;
  return this->queue[--this->queueLen];  
}
