
/* 
 * Desc: Graph-based planner; functions for c-space computation
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <float.h>

#include <frames/pose3.h>
#include <map/Map.hh>

#include "GraphPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Set the vehicle size for c-space calculations.
int GraphPlanner::setCSpaceSize(double rear, double front, double left, double right)
{
  float dx, dy;
  
  // Vehicle dimensions
  this->vehicleAx = -rear;
  this->vehicleBx = +front;
  this->vehicleAy = -left;
  this->vehicleBy = +right;

  // Compute the radius of a circle that encloses the vehicle.
  if (front > rear)
    dx = front;
  else
    dx = rear;
  if (left > right)
    dy = left;
  else
    dy = right;
  this->vehicleRadius = sqrtf(dx * dx + dy * dy);

  return 0;
}


// Construct configuration space from the given map
int GraphPlanner::makeCSpace(Map *map)
{
  int i, j;
  GraphNode *node;
  MapElement *mapel;
  float px, py;
  float mapelAx, mapelBx, mapelAy, mapelBy;

  // TODO: the map does not have static/non-static obstacle
  // distinction, so we treat everything as static right now.
  
  // TODO: optimize this with a spatially-indexed data-struct on
  // either the graph or the map.
  
  // Reset the collision flags for all nodes
  for (i = 0; i < this->graph.getNodeCount(); i++)
  {
    node = this->graph.getNode(i);
    node->collideObs = false;
    node->collideCar = false;
  }
  
  // Check each node against each object in the map for a possible
  // collision.
  for (j = 0; j < (int) map->data.size(); j++)
  {
    mapel = &map->data[j];
    if (mapel->type != ELEMENT_OBSTACLE)
      continue;

    // Compute the bounding box for this map element in the local frame.
    mapelAx = +FLT_MAX; mapelBx = -FLT_MAX;
    mapelAy = +FLT_MAX; mapelBy = -FLT_MAX;
    for (i = 0; i < 4; i++)
    {
      px = mapel->center.x;
      px += +cos(mapel->orientation) * mapel->length/2 * (1 - 2 * (i % 2));
      px += -sin(mapel->orientation) * mapel->width/2 * (1 - 2 * (i / 2));
      py = mapel->center.y;
      py += +sin(mapel->orientation) * mapel->length/2 * (1 - 2 * (i % 2));
      py += +cos(mapel->orientation) * mapel->width/2 * (1 - 2 * (i / 2));
      if (px < mapelAx) mapelAx = px;
      if (px > mapelBx) mapelBx = px;
      if (py < mapelAy) mapelAy = py;
      if (py > mapelBy) mapelBy = py;
    }
    
    // Check each node.  If a node is aleady in collision, dont waste
    // time checking it again.
    for (i = 0; i < this->graph.getNodeCount(); i++)
    {
      node = this->graph.getNode(i);
      if (node->collideObs)
        continue;
      node->collideObs = this->checkCollision(node, mapel, mapelAx, mapelBx, mapelAy, mapelBy);
    }
  }
  
  return 0;
}


// Check for collision between a node and a map element
bool GraphPlanner::checkCollision(GraphNode *node, MapElement *mapel,
                                  float mapelAx, float mapelBx, float mapelAy, float mapelBy)
{
  int i;
  float m[4][4];
  float px, py;
  point2_uncertain *point;
    
  // Do a quick check for overlapping bounding boxes
  mapelAx -= node->poseLocal.pos.x;
  mapelBx -= node->poseLocal.pos.x;
  mapelAy -= node->poseLocal.pos.y;
  mapelBy -= node->poseLocal.pos.y;
  if (mapelBx < -this->vehicleRadius || mapelAx > +this->vehicleRadius)
    return false;
  if (mapelBy < -this->vehicleRadius || mapelAy > +this->vehicleRadius)
    return false;
  
  // Transfrom from local to vehicle frame (when the vehicle is at
  // this node).
  // mat44_setf(m, node->loc2veh);
  // TODO: store this for each node for faster evaluation
  pose3_to_mat44f(pose3_inv(node->poseLocal), m);

  if (mapel->geometryType == GEOMETRY_POINTS)
  {
    for (i = 0; i < (int) mapel->geometry.size(); i++)
    {
      point = &mapel->geometry[i];

      // Transform point into vehicle frame
      px = m[0][0] * point->x + m[0][1] * point->y + m[0][3];
      py = m[1][0] * point->x + m[1][1] * point->y + m[1][3];

      // Check point against vehicle
      if (px < this->vehicleAx || px > this->vehicleBx)
        continue;
      if (py < this->vehicleAy || py > this->vehicleBy)
        continue;
      
      return true;
    }
  }
  
  return false;
}


// Add a fake car to the map
int GraphPlanner::addFakeCar(Map *map, const GraphNode *node)
{
  int i;
  MapElement mapel;

  pose3_t pose;
  vec3_t pos;
  double cx, cy;
  double roll, pitch, yaw;
  vector<int> ids;
  vector<point2> points;

  pose = node->poseLocal;
  quat_to_rpy(pose.rot, &roll, &pitch, &yaw);
        
  // Create some points in a rough car-shape  
  cx = 4.0;
  cy = 2.0;
  for (i = 0; i < 20; i++)
  {
    pos.x = (double) rand() / RAND_MAX * cx - cx / 2;
    pos.y = (double) rand() / RAND_MAX * cy - cy / 2;
    pos.z = 0;
    pos = vec3_transform(pose, pos);
    points.push_back(point2(pos.x, pos.y));
  }

  ids.push_back(node->index);
  mapel.set_points(ids, points);
  mapel.type = ELEMENT_OBSTACLE;
  mapel.center.x = pose.pos.x;
  mapel.center.y = pose.pos.y;
  mapel.length = cx;
  mapel.width = cy;
  mapel.orientation = yaw;

  mapel.plotColor = MAP_COLOR_RED;
  mapel.plotValue = 0xFF;

  map->addEl(mapel);

  return 0;
}
