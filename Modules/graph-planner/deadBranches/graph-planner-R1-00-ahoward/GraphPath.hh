
/* 
 * Desc: Path data structure
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef GRAPH_PATH_HH
#define GRAPH_PATH_HH

#include "Graph.hh"


/// @brief Class describing a planned path.
struct GraphPath
{
  // Does this path have a collision?
  bool collide;
  
  // List of graph nodes in the path
  int pathLen;
  GraphNode *path[GRAPH_MAX_NODES];
};



#endif
