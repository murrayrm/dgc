
/* 
 * Desc: Graph-based planner; functions for local trajectory generation
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <frames/pose3.h>
#include <trajutils/maneuver.h>
#include <trajutils/man2traj.hh>
#include <trajutils/traj.hh>

#include "GraphPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// Generate a vehicle trajectory.
int GraphPlanner::makeTraj(const VehicleState *state, GraphPath *path, CTraj *traj)
{
  int i;
  Vehicle *vp;
  Maneuver *mp;
  double vel[2];
  Pose2D poseA, poseB;
  double roll, pitch, yaw;
  GraphNode *nodeA, *nodeB;

  if (path->pathLen < 2)
    return ERROR("invalid path");

  // Initialize manuever start and end nodes
  nodeA = path->path[0];
  nodeB = NULL;
  
  // Walk along the path until we find the first non-volatile node;
  // i.e. the first node on the static graph.  This is the destination
  // for the maneuver.
  for (i = 1; i < path->pathLen; i++)
  {
    nodeB = path->path[i];
    if (nodeB->type != GRAPH_NODE_VOLATILE)
      break;
  }
  assert(nodeB);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Initial vehicle pose
  poseA.x = nodeA->poseGlobal.pos.x;
  poseA.y = nodeA->poseGlobal.pos.y;
  quat_to_rpy(nodeA->poseGlobal.rot, &roll, &pitch, &yaw);
  poseA.theta = yaw;

  // Final vehicle pose
  poseB.x = nodeB->poseGlobal.pos.x;
  poseB.y = nodeB->poseGlobal.pos.y;
  quat_to_rpy(nodeB->poseGlobal.rot, &roll, &pitch, &yaw);
  poseB.theta = yaw;

  // Create maneuver object
  mp = maneuver_twopoint(vp, &poseA, &poseB);
  assert(mp);

  // TESTING
  vel[0] = this->maxSpeed;
  vel[1] = this->maxSpeed;
  
  // Generate a trajectory from the maneuver, with the given
  // initial and final velocity.
  traj->startDataInput();
  maneuver_profile_generate(vp, 1, &mp, vel, 20, traj); // MAGIC

  // TESTING
  //traj->print();
  //traj->printSpeedProfile();
  
  // Clean up
  maneuver_free(mp);
  free(vp);
  
  return 0;
}

