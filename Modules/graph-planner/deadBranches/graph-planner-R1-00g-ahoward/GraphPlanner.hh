
/* 
 * Desc: Graph-based planner
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef GRAPH_PLANNER_HH
#define GRAPH_PLANNER_HH


// Forward declarations
namespace std
{
  class Lane;
  class RNDF;
}
class Map;
class MapElement;
class CTraj;


#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>

#include "Graph.hh"
#include "GraphPath.hh"


/// @brief Vehicle kinematic properties.
struct GraphPlannerKinematics
{
  /// Distance between front and rear wheels (m).
  double wheelBase;

  /// Maximum steering angle (degrees).  This is the largest angle
  /// that the lowel-level will command.  This value is also used to
  /// scale the measured steering position in ActuatorState (which is
  /// in the domain [-1, +1]) to a steering angle.
  double maxSteer;

  /// Maximum allowed steering angle for generating turning maneuvers.
  /// This can be higher than maxSteer, in which case the vehicle will
  /// attempt turns that are beyond its actual steering capability (and
  /// swing wide as a result).
  double maxTurn;
};


/// @brief Plan constraints.
///
/// These are the weights used to construct plans.  A negative value
/// inidicates a hard constraint.
struct GraphPlannerConstraints
{  
  /// Cost for driving in a lane.  This value usually sets the scale
  /// for the remaining costs; e.g., 100 might indicate that costs
  /// measure distances in cm.
  int laneCost;

  /// Cost for changing lanes.
  int changeCost;

  /// Cost for driving in an on-coming lane (and risking a head-on
  /// crash). This value should be much larger than laneCost.
  int headOnCost;

  /// Cost for driving through a static obstacle.  Usually set to -1
  /// to denote a hard constraint.
  int obsCost;

  /// Cost for driving through another vehicle. This can be set
  /// to zero to produce queuing behavior at intersections (the
  /// trajectory generation step will set the velocity profile
  /// such that we dont crash into other vehicles.
  int carCost;
};


/// @brief Graph-based planner
class GraphPlanner
{
  public:

  // Default constructor
  GraphPlanner(int maxNodes, int maxArcs);

  // Destructor
  virtual ~GraphPlanner();

  public:

  /// @brief Get the vehicle kinematics
  ///
  /// @param[out] kin Current vehicle kinematic values.
  int getKinematics(GraphPlannerKinematics *kin);

  /// @brief Set the vehicle kinematics
  ///
  /// @param[int] kin Current vehicle kinematic values.
  int setKinematics(const GraphPlannerKinematics *kin);

  private:

  // Vehicle kinermatic properties
  GraphPlannerKinematics kin;

  public:
  
  // Initialize the graph from an RNDF file.
  int loadRndf(const char *filename);

  // Initialize the graph from a map.
  // TODO int loadMap(Map *map);
  
  private:

  // TODO: graph generation could be moved to a seperate class.
  
  // Generate lane nodes
  int genLanes(std::RNDF *rndf);

  // Generate a lane, driving either with traffic (direction = +1) or
  // against traffic (direction = -1).
  int genLane(std::Lane *lane, int direction);

  // Generate lane tangents
  int genLaneTangents(std::Lane *lane);
  
  // Generate turn manuevers
  int genTurns(std::RNDF *rndf);

  // Generate lane-change manuevers
  int genChanges(std::RNDF *rndf);
  
  // Generate a maneuver linking two nodes
  int genManeuver(int nodeType, GraphNode *nodeA, GraphNode *nodeB, double maxSteer, int penalty);

  public:

  /// @brief Update the vehicle node and associated maneuvers.
  ///
  /// Unlike the rest of the graph, this must be done dynamically.
  int genVehicleSubGraph(const VehicleState *vehicleState, const ActuatorState *actuatorState);

  /// @brief Set the current vehicle state data
  int updateState(const VehicleState *vehicleState);

  /// @brief Set the vehicle size for c-space calculations.
  ///
  /// This function can be called before ::makeCSpace to dynamically
  /// alter the effective vehicle size (e.g., to switch between
  /// agressive and conservative behaviors).
  ///
  /// All measurements are relative to the vehicle origin, which is
  /// usually the middle of the rear axel.
  ///
  /// @param[in] rear  Distance to the rear of the vehicle (m).
  /// @param[in] front Distance to the front of the vehicle (m).
  /// @param[in] left  Distance to the left side of the vehicle (m).
  /// @param[in] right Distance to the right side of the vehicle (m).
  int setCSpaceSize(double rear, double front, double left, double right);

  /// @brief Construct configuration space from the given map
  int makeCSpace(Map *map);

  /// @brief Add a fake car to the map (for testing).
  int addFakeCar(Map *map, const GraphNode *node);

  private:
  
  // Check for collision between a node and a map element
  bool checkCollision(GraphNode *node, MapElement *mapel,
                      float mapelAx, float mapelBx, float mapelAy, float mapelBy);

  // Vehicle c-space bounding box
  float vehicleAx, vehicleBx, vehicleAy, vehicleBy;

  // Radius of vehicle c-space circle
  float vehicleRadius;
  
  public:

  /// @brief Get the plan constraints
  ///
  /// @param[out] cons Current plan constraints. 
  int getConstraints(GraphPlannerConstraints *cons);

  /// @brief Set the plan constraints
  ///
  /// @param[in] cons Current plan constraints. 
  int setConstraints(const GraphPlannerConstraints *cons);

  /// @brief Construct a plan for reaching the given checkpoint
  int makePlan(int checkId);

  /// @brief Construct path.
  ///
  /// @param[out] path The planned path, including some meta-data.
  int makePath(GraphPath *path);

  private:

  // Push node onto the priority queue
  int pushNode(GraphNode *node, int planCost);
    
  // Pop node from the priority queue
  GraphNode *popNode();

  // Plan constraints
  GraphPlannerConstraints cons;
  
  // Priority queue for Dijkstra
  int queueLen, queueMax;
  GraphNode **queue;

  public:
  
  /// @brief Generate a vehicle trajectory.
  int makeTraj(const VehicleState *vehicleState,
               const ActuatorState *actuatorState,
               GraphPath *path, double maxSpeed, CTraj *traj);
  
  // TODO: make private
  public:

  // Current segment/lane (both are set to zero if the
  // vehicle is in an intersection).
  int segmentId, laneId;
  
  // Vehicle node in graph
  GraphNode *vehicleNode;

  public:
  
  // Global plan graph
  Graph graph;
};



#endif
