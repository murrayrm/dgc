
/* 
 * Desc: Update configuration space
 * Date: 25 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <float.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <map/Map.hh>

#include "ConfigSpace.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
ConfigSpace::ConfigSpace(Graph* graph)
{
  this->graph = graph;

  this->dim.front = 4;
  this->dim.rear = 2;
  this->dim.left = 1;
  this->dim.right = 1;
  this->setDimensions(&this->dim);
  
  return;
}


// Destructor
ConfigSpace::~ConfigSpace()
{
  return;
}


// Get the vehicle dimensions
int ConfigSpace::getDimensions(ConfigSpaceDimensions *dim)
{
  *dim = this->dim;
  return 0;
}


// Set the vehicle dimensions
int ConfigSpace::setDimensions(const ConfigSpaceDimensions *dim)
{
  float s;
  
  this->dim = *dim;

  // Outer rectangle dimensions
  this->outerAx = -DIST_REAR_TO_REAR_AXLE - dim->rear;
  this->outerBx = +VEHICLE_LENGTH - DIST_REAR_TO_REAR_AXLE + dim->front;
  this->outerAy = -VEHICLE_WIDTH/2 - dim->left;
  this->outerBy = +VEHICLE_WIDTH/2 + dim->right;
  this->outerRadius = 0;
  this->outerRadius += (this->outerBx - this->outerAx) * (this->outerBx - this->outerAx);
  this->outerRadius += (this->outerBy - this->outerAy) * (this->outerBy - this->outerAy);
  this->outerRadius = 0.5 * sqrtf(this->outerRadius);

  s = 0.5;
  
  // Inner rectangle dimensions
  this->innerAx = -DIST_REAR_TO_REAR_AXLE - dim->rear * s;
  this->innerBx = +VEHICLE_LENGTH - DIST_REAR_TO_REAR_AXLE + dim->front * s;
  this->innerAy = -VEHICLE_WIDTH/2 - dim->left * s;
  this->innerBy = +VEHICLE_WIDTH/2 + dim->right * s;
  this->innerRadius = 0;
  this->innerRadius += (this->innerBx - this->innerAx) * (this->innerBx - this->innerAx);
  this->innerRadius += (this->innerBy - this->innerAy) * (this->innerBy - this->innerAy);
  this->innerRadius = 0.5 * sqrtf(this->innerRadius);
  
  return 0;
}


// Add a fake car to the map
int ConfigSpace::addFakeCar(const VehicleState *state, Map *map, const GraphNode *node)
{
  int i;
  MapElement mapel;
  pose3_t pose, lpose, gpose;
  vec3_t pos;
  double cx, cy;
  double roll, pitch, yaw;
  vector<point2> points;

  // Compute transform global to local frame
  gpose.pos = vec3_set(state->utmNorthing,
                       state->utmEasting,
                       state->utmAltitude);
  gpose.rot = quat_from_rpy(state->utmRoll,
                            state->utmPitch,
                            state->utmYaw);
  lpose.pos = vec3_set(state->localX,
                       state->localY,
                       state->localZ);
  lpose.rot = quat_from_rpy(state->localRoll,
                            state->localPitch,
                            state->localYaw);  
  pose = pose3_mul(lpose, pose3_inv(gpose));

  // Get the object pose
  pose = pose3_mul(pose, node->pose);
  quat_to_rpy(pose.rot, &roll, &pitch, &yaw);

  // Create some points in a rough car-shape  
  cx = 4.0;
  cy = 2.0;
  for (i = 0; i < 100; i++)
  {
    pos.x = (double) rand() / RAND_MAX * cx - cx / 2;
    pos.y = (double) rand() / RAND_MAX * cy - cy / 2;
    pos.z = 0;
    pos = vec3_transform(pose, pos);
    points.push_back(point2(pos.x, pos.y));
  }

  mapel.setId(3333, node->index);
  mapel.setTypeObstacle();
  // TESTING mapel.setTypeVehicle();
  mapel.geometryType = GEOMETRY_POINTS;  
  mapel.setGeometry(points);
  mapel.setColor(MAP_COLOR_RED,0xFF);
  
  map->addEl(mapel);
  
  return 0;
}


// Assign segment and lane ids
int ConfigSpace::updateLanes()
{
  int i;
  GraphNode *node, *lanel;
  double ax, ay, bx, by;
  
  // Pass through volatile nodes to assign segment and lane ids
  for (i = this->graph->getStaticNodeCount(); i < this->graph->getNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    assert(node);

    node->segmentId = 0;
    node->laneId = 0;
    node->offRoad = true;

    // Get the nearest lane or turn node; these are the lane elements
    // that are "on the road".
    lanel = this->graph->getNearestNode(node->pose.pos,
                                        GRAPH_NODE_LANE | GRAPH_NODE_TURN, 10); // MAGIC
    if (!lanel)
      continue;
    
    // Compute node position relative to lane element
    ax = node->pose.pos.x;
    ay = node->pose.pos.y;
    bx = lanel->transNG[0][0] * ax + lanel->transNG[0][1] * ay + lanel->transNG[0][3];
    by = lanel->transNG[1][0] * ax + lanel->transNG[1][1] * ay + lanel->transNG[1][3];

    // See if the node is in the lane in a strict euclidean sense
    if (bx < -lanel->laneLength/2 || bx > +lanel->laneLength/2)
      continue;
    if (by < -lanel->laneWidth/2 || by > +lanel->laneWidth/2)
      continue;

    // Assign an RNDF id
    node->segmentId = lanel->segmentId;
    node->laneId = lanel->laneId;

    // Assign RNDF data
    node->isEntry = lanel->isEntry;
    node->isExit = lanel->isExit;
    node->isStop = lanel->isStop;

    // See if the node is in the lane in a configuration space sense.
    // TODO: this is not really correct; needs to check configuration to
    // see if any part of the robot in off the road.
    // TODO: check inner/outer lane boundaries.
    if (by < -lanel->laneWidth/2 + 1.0 || by > +lanel->laneWidth/2 - 1.0) // MAGIC
      continue;
    node->offRoad = false;
  }

  return 0;
}


// Construct configuration space from the given map
int ConfigSpace::updateMap(const VehicleState *state, Map *map)
{
  int i, j;
  pose3_t gpose, lpose, pose;
  GraphNode *node;
  MapElement *mapel;
  double transGL[4][4];
  float px, py, qx, qy;
  float mapelAx, mapelBx, mapelAy, mapelBy;
  int collide;
  
  // TODO: optimize this with a spatially-indexed data-struct on the
  // map and the graph.
  
  // Reset the collision flags for all nodes
  for (i = 0; i < this->graph->getNodeCount(); i++)
  {
    node = this->graph->getNode(i);
    node->obsInLane = false;
    node->carInLane = false;
    node->collideObs = false;
    node->collideCar = false;
  }

  // Compute transform from local to global frame
  gpose.pos = vec3_set(state->utmNorthing,
                       state->utmEasting,
                       state->utmAltitude);
  gpose.rot = quat_from_rpy(state->utmRoll,
                            state->utmPitch,
                            state->utmYaw);
  lpose.pos = vec3_set(state->localX,
                       state->localY,
                       state->localZ);
  lpose.rot = quat_from_rpy(state->localRoll,
                            state->localPitch,
                            state->localYaw);  
  pose = pose3_mul(gpose, pose3_inv(lpose));
  pose3_to_mat44d(pose, transGL);

  // Look at the map elements and do two things:
  //   1. Find out which configurations collide with map obstacles.
  //   2. Find out which lane elements have obstacles in them.
  for (j = 0; j < (int) map->data.size(); j++)
  {
    mapel = &map->data[j];

    if (mapel->geometry.size() == 0)
      continue;
    if (!(mapel->type == ELEMENT_VEHICLE || mapel->type == ELEMENT_OBSTACLE))
      continue;

    // Compute the bounding box for this map element in the global frame.
    mapelAx = +FLT_MAX; mapelBx = -FLT_MAX;
    mapelAy = +FLT_MAX; mapelBy = -FLT_MAX;
    for (i = 0; i < 4; i++)
    {
      px = mapel->center.x;
      px += +cos(mapel->orientation) * mapel->length/2 * (1 - 2 * (i % 2));
      px += -sin(mapel->orientation) * mapel->width/2 * (1 - 2 * (i / 2));
      py = mapel->center.y;
      py += +sin(mapel->orientation) * mapel->length/2 * (1 - 2 * (i % 2));
      py += +cos(mapel->orientation) * mapel->width/2 * (1 - 2 * (i / 2));
      qx = transGL[0][0] * px + transGL[0][1] * py + transGL[0][3];
      qy = transGL[1][0] * px + transGL[1][1] * py + transGL[1][3];      
      if (qx < mapelAx) mapelAx = qx;
      if (qx > mapelBx) mapelBx = qx;
      if (qy < mapelAy) mapelAy = qy;
      if (qy > mapelBy) mapelBy = qy;
    }
    
    // Check static lane and turn nodes
    for (i = 0; i < this->graph->getNodeCount(); i++)
    {
      node = this->graph->getNode(i);
      
      // Test bounding box
      if (mapelBx < node->pose.pos.x - this->outerRadius)
        continue;
      if (mapelAx > node->pose.pos.x + this->outerRadius)
        continue;
      if (mapelBy < node->pose.pos.y - this->outerRadius)
        continue;
      if (mapelAy > node->pose.pos.y + this->outerRadius)
        continue;
      
      if (node->direction < +1)
        continue;

      if (node->type == GRAPH_NODE_VOLATILE)
      {
        // See if this configuration collides with the map obstacle
        collide = this->testConfigMap(node, mapel, transGL);
        if (mapel->type == ELEMENT_OBSTACLE)
          node->collideObs = collide;
        if (mapel->type == ELEMENT_VEHICLE)
          node->collideCar = collide;
      }
      else if (node->type == GRAPH_NODE_LANE || node->type == GRAPH_NODE_TURN)
      {
        // See if this lane element contains a map obstacle
        if (this->testLane(node, mapel, transGL))
        {
          if (mapel->type == ELEMENT_OBSTACLE)
          {
            node->obsInLane = true;
            node->collideObs = true;
          }
          if (mapel->type == ELEMENT_VEHICLE)
          {
            node->carInLane = true;
            node->collideCar = true;
            this->propagateCar(node, 20); // MAGIC
          }
        }
      }
    }
  }   

  float radius, dx, dy;
  GraphNode *lanel;
  
  // Take a second pass to figure out which configurations collide
  // with occupied lane elements.
  for (j = 0; j < this->graph->getNodeCount(); j++)
  {
    lanel = this->graph->getNode(j);
    if (!(lanel->obsInLane || lanel->carInLane))
      continue;

    // Radius for bounding box check
    radius = this->outerRadius;
    radius += sqrt(lanel->laneLength*lanel->laneLength + lanel->laneWidth*lanel->laneWidth)/2;

    for (i = 0; i < this->graph->getNodeCount(); i++)
    {
      node = this->graph->getNode(i);

      // Do a quick bounding box check
      dx = node->pose.pos.x - lanel->pose.pos.x;
      if (dx < -radius || dx > +radius)
        continue;
      dy = node->pose.pos.y - lanel->pose.pos.y;
      if (dy < -radius || dy > +radius)
        continue;
      
      // Do a full check for collision
      if (this->testConfigLane(node, lanel))
      {
        node->collideObs = lanel->obsInLane;
        node->collideCar = lanel->carInLane;
      }
    }
  }
  
  return 0;
}


// Check a configuration for collision with a map element
int ConfigSpace::testConfigMap(GraphNode *node, MapElement *mapel, double transGL[4][4])
{
  int i;
  double ax, ay, bx, by;
  point2_uncertain *point;
  int collide;

  collide = 0;
  
  if (mapel->geometryType == GEOMETRY_POINTS)
  {
    for (i = 0; i < (int) mapel->geometry.size(); i++)
    {
      point = &mapel->geometry[i];

      // Transform point from local to global frame
      ax = transGL[0][0] * point->x + transGL[0][1] * point->y + transGL[0][3];
      ay = transGL[1][0] * point->x + transGL[1][1] * point->y + transGL[1][3];
      
      // Transform point from global to node frame
      bx = node->transNG[0][0] * ax + node->transNG[0][1] * ay + node->transNG[0][3];
      by = node->transNG[1][0] * ax + node->transNG[1][1] * ay + node->transNG[1][3];

      // Check point against outer box
      if (bx < this->outerAx || bx > this->outerBx)
        continue;
      if (by < this->outerAy || by > this->outerBy)
        continue;

      collide = 1;

      // Check point against inner box
      if (bx < this->innerAx || bx > this->innerBx)
        continue;
      if (by < this->innerAy || by > this->innerBy)
        continue;

      collide = 2;

      break;
    }
  }
  
  return collide;
}


// Check lane element to see if it contains a map element
bool ConfigSpace::testLane(GraphNode *node, MapElement *mapel, double transGL[4][4])
{
  int i;
  double ax, ay, bx, by;
  point2_uncertain *point;
  
  if (mapel->geometryType == GEOMETRY_POINTS)
  {
    for (i = 0; i < (int) mapel->geometry.size(); i++)
    {
      point = &mapel->geometry[i];

      // Transform point from local to global frame
      ax = transGL[0][0] * point->x + transGL[0][1] * point->y + transGL[0][3];
      ay = transGL[1][0] * point->x + transGL[1][1] * point->y + transGL[1][3];
      
      // Transform point from global to node frame
      bx = node->transNG[0][0] * ax + node->transNG[0][1] * ay + node->transNG[0][3];
      by = node->transNG[1][0] * ax + node->transNG[1][1] * ay + node->transNG[1][3];

      // Check point to see if it is in the lane
      if (bx < -node->laneLength/2 || bx > +node->laneLength/2) // MAGIC
        continue;
      if (by < -node->laneWidth/2 || by > +node->laneWidth/2)
        continue;
      
      return true;
    }
  }
  
  return false;
}


// Check a configuration for collision with occupied lane elements
bool ConfigSpace::testConfigLane(GraphNode *node, GraphNode *lanel)
{
  int j;
  float ax, ay, bx, by, cx, cy;
  
  // TODO: denser sampling
  // Sample points across the lane.
  for (j = -1; j <= +1; j++)
  {
    ax = 0;
    ay = j * lanel->laneWidth/2;

    // Transform from node to global frame
    bx = lanel->transGN[0][0] * ax + lanel->transGN[0][1] * ay + lanel->transGN[0][3];
    by = lanel->transGN[1][0] * ax + lanel->transGN[1][1] * ay + lanel->transGN[1][3];

    // Transform from global to node frame
    cx = node->transNG[0][0] * bx + node->transNG[0][1] * by + node->transNG[0][3];
    cy = node->transNG[1][0] * bx + node->transNG[1][1] * by + node->transNG[1][3];

    // Check point against vehicle dimensions
    if (cx < this->outerAx || cx > this->outerBx)
      continue;
    if (cy < this->outerAy || cy > this->outerBy)
      continue;

    return true;
  }    

  return false;
}


// Propagate a car obstacle along the graph
int ConfigSpace::propagateCar(GraphNode *node, float dist)
{
  GraphArc *arc;

  if (dist < 0)
    return 0;

  // Restrict propagation to lanes and turns
  if (node->direction < +1)
    return 0;
  if (!(node->type == GRAPH_NODE_LANE || node->type == GRAPH_NODE_TURN))
    return 0;

  // We expect this node to contain a car at some time in the future
  node->carInLane = true;
        
  // Propagate prediction along all outgoing arcs.
  for (arc = node->outFirst; arc != NULL; arc = arc->outNext)
  {
    assert(arc->nodeB != node);
    this->propagateCar(arc->nodeB, dist - arc->planDist / 1000.0);
  }
  
  return 0;
}
