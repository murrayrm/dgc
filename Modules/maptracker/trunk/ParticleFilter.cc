/*!ParticleFilter.cc
 * Author: Pete Trautman
 * Last revision: April 18 2007
 * */

#include "ParticleFilter.hh"

using namespace std;

ParticleFilter::ParticleFilter(double resampleFactor,int N,point2& cpt,point2& vel,int modelType, double sigma)
{
   //initialization of randomization variables for gsl.
   gsl_rng_env_setup();
   m_T = gsl_rng_default;
   m_r = gsl_rng_alloc (m_T);
   m_sigma=sigma;
 
   //initialization of scalars
   m_resampleFactor = resampleFactor;
   m_N = N;
   m_Cpt = cpt;
   m_modelType=modelType;
   
   //initialization of vectors
   for (int i=0; i<m_N;i++)
   {
    m_Noise.x = gsl_ran_gaussian(m_r,m_sigma);
    m_Noise.y = gsl_ran_gaussian(m_r,m_sigma);
    
    m_particlesNow.push_back(m_Cpt+m_Noise);
    m_particlesPlus.push_back(m_Cpt+m_Noise);
    
    m_Weights.push_back(1.0/m_N);
   }
   
   double u;
   point2 tmpvel;
   for(int i=0;i<m_N;i++)
   {
     u = gsl_rng_uniform(m_r);
     m_Noise.x = gsl_ran_gaussian(m_r,m_sigma);
     m_Noise.y = gsl_ran_gaussian(m_r,m_sigma);
     tmpvel.x = vel.x+m_Noise.x;//vel.x*(u-0.5);
     tmpvel.y = vel.y+m_Noise.y;//vel.y*(u+0.5);
     m_velPlus.push_back(tmpvel);
     m_velNow.push_back(tmpvel);
     
     m_omegaNow.push_back(0.000001*(u-0.5));
     m_omegaPlus.push_back(0.000001*(u-0.5));

     m_ang.push_back(-atan(m_velNow[i].x/m_velNow[i].y));
   }
}

ParticleFilter::~ParticleFilter() 
{
}

point2arr ParticleFilter::getParticlesNow()
{
  return m_particlesNow;
}

vector<double> ParticleFilter::getang()
{
  return m_ang;
}

point2arr ParticleFilter::getVelNow()
{
  return m_velNow;
}

void ParticleFilter::kalmanFilter()
{
  CvKalman* kalman;
  int dynam_params=1;
  int measure_params=1;
  int control_params=0;
  //CvMat* state = cvCreateMat( 2, 1, CV_32FC1 );
  //CvMat* process_noise = cvCreateMat( 2, 1, CV_32FC1 );
  kalman=cvCreateKalman( dynam_params, measure_params, control_params);
}

point2arr ParticleFilter::sampleConstVel(point2 sampleFactor,double deltaT)
{  
   //constant velocity model with noise
   point2 m_sampleFactor(sampleFactor.x,sampleFactor.y);
   double u;
   for(int i=0;i<m_N;i++)
    { 
    m_Noise.x = gsl_ran_gaussian(m_r,m_sampleFactor.x);
    m_Noise.y = gsl_ran_gaussian(m_r,m_sampleFactor.y);    
    
    m_particlesPlus[i]=m_particlesNow[i]+m_velNow[i]*deltaT+0.5*m_Noise*pow(deltaT,2);
    
    m_ang[i] = -atan(m_velNow[i].x/m_velNow[i].y);
    
    //u = gsl_rng_uniform(m_r);
    //m_Noise.x = m_Noise.x/1;
    //u = gsl_rng_uniform(m_r);
    //m_Noise.y = u/1;
    m_velPlus[i]=m_velNow[i]+m_Noise*deltaT;
    }
  return m_particlesPlus;
}

point2arr ParticleFilter::sampleCoordTurn(point2 sampleFactor,double deltaT)
{   //coordinated turn model
    point2 m_sampleFactor(sampleFactor.x,sampleFactor.y);
    for(int i=0;i<m_N;i++)
    {
     m_Noise.x = gsl_ran_gaussian(m_r,m_sampleFactor.x*m_sigma);
     m_Noise.y = gsl_ran_gaussian(m_r,m_sampleFactor.y*m_sigma); 

     m_particlesPlus[i].x=m_particlesNow[i].x+
		(m_velNow[i].x*sin(deltaT*m_omegaNow[i]))/m_omegaNow[i]+
		(m_velNow[i].y*(1-cos(deltaT*m_omegaNow[i])))/m_omegaNow[i]+
		 m_Noise.x*0.5*pow(deltaT,2);
     m_velPlus[i].x=m_velNow[i].x*cos(deltaT*m_omegaNow[i])-
                    m_velNow[i].y*sin(deltaT*m_omegaNow[i])+m_Noise.x*deltaT;
     
     m_particlesPlus[i].y=m_particlesNow[i].y+
		(m_velNow[i].y*sin(deltaT*m_omegaNow[i]))/m_omegaNow[i]+
		(m_velNow[i].x*(1-cos(deltaT*m_omegaNow[i])))/m_omegaNow[i]+
		 m_Noise.y*0.5*pow(deltaT,2);
     m_velPlus[i].y=m_velNow[i].y*cos(deltaT*m_omegaNow[i])+
                    m_velNow[i].x*sin(deltaT*m_omegaNow[i])+m_Noise.y*deltaT;   
     
     m_omegaPlus[i] = m_omegaNow[i];//+m_Noise.x/100;

     m_ang[i] = -atan(m_velNow[i].x/m_velNow[i].y);
    }
  return m_particlesPlus;
}

void ParticleFilter::plan(vector<point2arr>& lbound,vector<point2arr>& rbound,
		          point2arr& sensedData,point2arr particlesNow)
{  //weights against dynamic obstacles as well 
   
   double m=0;
   double l=0;
   double r=0;
   double wSigma=10;
   double pSigma=10;
   double logWeight;
   double x;
   point2 planW;
   point2 mbound;
    
  for(int i=0;i<m_N;i++)//calculating weights per particle
     { 
        for(int k=0;k<m_N;k++)//calculating dynamic weighting for all DO particles
          {
           planW.x=m_particlesPlus[i].x-particlesNow[k].x;
           planW.y=m_particlesPlus[i].y-particlesNow[k].y;
           m_Weights[i] = m_Weights[i]*
               (1-exp(-pow(planW.x,2)/pSigma-pow(planW.y,2)/(10*pSigma)));
          }
       
       l=m_particlesPlus[i].x-lbound[i][0].x;
       r=m_particlesPlus[i].x-rbound[i][0].x;
      
          if(l>0 && r<0)//in lane
          {
           m = (lbound[i][0].x+rbound[i][0].x)/2.0;
           x=m-m_particlesPlus[i].x;
           logWeight=pow(x,2);
           wSigma=10;	
           m_Weights[i]=m_Weights[i]*exp(-logWeight/wSigma);
          }
         
          if(l<0 && r<0)//probably in other lane; need logic for dirt on other side
          {
           m = (lbound[i][0].x+rbound[i][0].x)/2.0;
           x=m-m_particlesPlus[i].x;
           logWeight=pow(x,2);
           wSigma=10;	
           double penalty = 0.1;	
           if(l<-3)
            {
             penalty = penalty*0.000001;	
            }
           m_Weights[i]=penalty*m_Weights[i]*exp(-logWeight/wSigma);
          }

         if(l>0 && r>0)//in the dirt
          {
           double penalty = 0.0000001;	
           m_Weights[i]=penalty*m_Weights[i];
          }     
    }
     
      double sum = 0;
  for(int i=0;i<m_N;i++)//calculating normalizer
     {
       sum=sum+m_Weights[i];
     }
   
   for(int i=0;i<m_N;i++)//normalization
     {
       m_Weights[i]=m_Weights[i]/sum;
     }
}

void ParticleFilter::weight(vector<point2arr>& lbound,vector<point2arr>& rbound,
		     point2arr& sensedPos,point2arr& sensedVel,int time,double sensedSigma)
{     
      double x;
      double m=0;
      double l=0;
      double r=0;
      double dataPosx=0,dataPosy = 0,dataPos=0;
      double dataVelx=0,dataVely = 0,dataVel=0;
      double logWeight;
      double wSigma=5; 
         
  for(int i=0;i<m_N;i++)
   {/* 
       l=m_particlesPlus[i].x-lbound[i][0].x;
       r=m_particlesPlus[i].x-rbound[i][0].x;
      
          if(l>0 && r<0)//in lane
          {
           m = (lbound[i][0].x+rbound[i][0].x)/2.0;
           x=m-m_particlesPlus[i].x;
           logWeight=pow(x,2);
           wSigma=10;	
           m_Weights[i]=m_Weights[i]*exp(-logWeight/wSigma);
          }
         
          if(l<0 && r<0)//probably in other lane; need logic for dirt on other side
          {
           m = (lbound[i][0].x+rbound[i][0].x)/2.0;
           x=m-m_particlesPlus[i].x;
           logWeight=pow(x,2);
           wSigma=10;	
           double penalty = 0.1;
	   if(l<-3)
            {
             penalty = penalty*0.000001;	
            }
           m_Weights[i]=penalty*m_Weights[i]*exp(-logWeight/wSigma);
          }

         if(l>0 && r>0)//in the dirt
          {
           double penalty = 0.0000001;	
           m_Weights[i]=penalty*m_Weights[i];
          }
      */    
      ///$$$$$$$$$$$$$$sensed data weighting portion
      ///* 

    if(sensedPos[time].x == 0 && sensedPos[time].y ==0)
     {
     }     
    else
     {     
      dataPosx=m_particlesPlus[i].x-sensedPos[time].x;
      dataPosy=m_particlesPlus[i].y-sensedPos[time].y;
      dataPos = pow(dataPosx,2)+pow(dataPosy,2);
      m_Weights[i]=m_Weights[i]*exp(-dataPos/(1*pow(sensedSigma,2)));
      ///*
      dataVelx=m_velPlus[i].x-sensedVel[time].x;
      dataVely=m_velPlus[i].y-sensedVel[time].y;
      dataVel = pow(dataVelx,2)+pow(dataVely,2);
      m_Weights[i]=m_Weights[i]*exp(-dataVel/(1*pow(sensedSigma,2)));
      //*/
      //$$$$$$$$$$$$$$$end sensed data
     }
   }
      
   //$$$$$$$$$$$$$$$$$$normalization
      double sum = 0;
   for(int i=0;i<m_N;i++)//calculating normalizer
     {
       sum=sum+m_Weights[i];
     }
  for(int i=0;i<m_N;i++)//normalization
     {
       m_Weights[i]=m_Weights[i]/sum;
     }
  //$$$$$$$$$$$$$$$$$$$$$$end normalization
}

void ParticleFilter::resample(int partitions, point2 resampleNoise)
{   //resampling using Neff.  returns Weights either same or =1/N
    //returns Now either = resampledPlus or just Plus
  
  double particleVar=0;
   for(int k=0; k<m_N;k++)
    {
     particleVar=particleVar+pow(m_Weights[k],2);
    }
   double Neff=1.0/particleVar;
   
  //  cout<<"Neff"<<Neff<<endl;
 if(Neff<=m_resampleFactor*m_N)
  { //&&&&&&&&&&if
	//cout<<"resample"<<endl;
     double P[m_N];
   for(int i=0;i<m_N;i++)
    {
     P[i]=m_Weights[i];
    } 
     //gsl initialization
     gsl_ran_discrete_t* g;
     size_t sampleIndex;
     g = gsl_ran_discrete_preproc(sizeof(m_N), P);
    
     //variance in gaussian sampling
     double sigmax = resampleNoise.x;
     double sigmay = resampleNoise.y;
   for(int i=0;i<m_N;i++)  //resampling loop
     {
      m_Noise.x = gsl_ran_gaussian(m_r,sigmax);
      m_Noise.y = gsl_ran_gaussian(m_r,sigmay); 
      
      sampleIndex = gsl_ran_discrete (m_r,g); //returns sampleIndex i with Prob=weight(i)
      m_particlesNow[i] = m_particlesPlus[sampleIndex]+m_Noise; //new particles are born from old
      
      m_velNow[i]=m_velPlus[sampleIndex];//+m_Noise;
      m_omegaNow[i]=m_omegaPlus[sampleIndex];//+m_Noise.x;
     }
    
   for(int i=0;i<m_N;i++)//resetting the weights
     {
      m_Weights[i]=1.0/m_N;
     }
  }       //&&&&&&&&&&&&&&&&&&&&&&&&&&&if

 else //if resampling unneccesary, then everything is just as .predict said
  {
   //cout<<"don't resample"<<endl;
   m_particlesNow=m_particlesPlus;
   m_velNow=m_velPlus;
   m_omegaNow=m_omegaPlus;
  }
}




















