/*!MapPrediction.cc
 * Author: Pete Trautman
 * Last revision: April 18 2007
 * */

#include "MapTracker.hh"

using namespace std;

CMapTracker::CMapTracker(int skynetKey, bool bWaitForStateFill,
                                 int debugLevel, int verboseLevel, 
                                 bool log)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = skynetKey;
  if (debugLevel>0){
    m_debug = true;
    cout << "debug is on" << endl;
  }
  else m_debug = false;
  if (verboseLevel>0) {
    m_verbose = true;
    cout << "verbose is on" << endl;
  }
  else m_verbose = false;
  
  m_log = log;
  if (m_log) 
    cout << "logging is on" << endl;


  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions
 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey);
  initSendMapElement(skynetKey);
  if(!m_recvLocalMap){
    //bool loadMapFromRNDF =  loadRNDF(string filename) {return localmap.loadRNDF(filename);
  }
  m_localMap = new Map();
}

CMapTracker::~CMapTracker() 
{
  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}


void CMapTracker::getLocalMapThread()
{

  MapElement recvEl;
  int bytesRecv;
  while (true){
  bytesRecv = recvMapElementBlock(&recvEl,1);
 
  if (bytesRecv>0){
      DGClockMutex(&m_LocalMapMutex);
    m_localMap->addEl(recvEl);
    DGCunlockMutex(&m_LocalMapMutex);
  }else {
    cout << "Error in CMapPrediction::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
    usleep(100);
  }
}
 
}

void CMapTracker::MapTrackerLoop(void)
{
  //=====================================================
  UpdateState(); // this gives m_state
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;         
  //======================================================
   
   MapElement recvEl;
   MapElement tempEl;
   int sendChannel=0;

  while(1){
  DGClockMutex(&m_LocalMapMutex);

  for (unsigned i =0; i< m_localMap->data.size(); ++i)
    {    
     sendMapElement(&m_localMap->data[i], sendChannel);
    }

  for (unsigned i =0; i< m_localMap->prior.data.size(); ++i)
    {
      m_localMap->prior.getEl(i,tempEl);
      sendMapElement(&tempEl, sendChannel);
    }

  DGCunlockMutex(&m_LocalMapMutex);

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 //here's where the consequential stuff begins
   
   MapElement dob1; //dynamic obstacle 1
   MapElement alice; //alice
   MapElement dob1TrueData,dob1SenseData;
   MapElement lineBoundary; //line boundaries functioning as measurements
   MapElement covBox;

   int horizon=500; //horizon of prediction
   double range=10;

   LaneLabel dob1label(1,2); 
   point2 dob1size(2,5);//good size for block cars

   //id identifiers for my obstacles
   vector<int> iddob1; 
   vector<int> idalice;
   vector<int> iddob1TrueData,iddob1SenseData;
   vector<int> idcovBox;
   iddob1.push_back(1); 
   idalice.push_back(0);
   iddob1TrueData.push_back(3);
   iddob1SenseData.push_back(4);
   idcovBox.push_back(5);  

   //initialization of the obstacles; this should be read in from mapper in the future
   point2 dob1cpt(-1,-60);//initial position straight down
   point2 trueObsCpt(-5,-70);
   //point2 dob1cpt(30,-72);//initial position on turn
   //point2 dob1cpt(0,8);//initial position bottom
   point2 vel(0.7,3);//initial velocity straight down
   //point2 vel(5,0);//initial velocity turn
   point2 tvel(0.7,3);      

   int N = 50; //number of particles
   double sigma=2; //initial covariance in the samples
   point2 resampleNoise(0.4,0.4);
   double resampleFactor = 0.7; //Neff <= resampleFactor*N
   point2 sampleFactor(1.0,1.0); //how reliable the sampling distribution is
   int modelType=1;
   int partitions = 2;
   point2 noise(0,0);
   vector<double> ang(N);
   double angData;
   //double anginit = 0.05;//-2*acos(-1)/4; ang 0 = straight down; ccw from that is neg 2pi
   point2arr particlesPlus;
   point2arr particlesNow;
   point2arr velNow;
   ParticleFilter dob1PF(resampleFactor,N,dob1cpt,vel,modelType,sigma);   
   
   velNow = dob1PF.getVelNow();
   particlesNow=dob1PF.getParticlesNow();
     
   double deltaT=0.1;	
   long long int start,end;

   double sensedSigma=1.0; //how reliable the data is
   double badReturnRate = 0.0; //1=always drop, 0 = never drop
   point2 trueDynObsPos=trueObsCpt;
   point2 trueDynObsVel=tvel;//dynamic obstacle data
   point2 senseDynObsPos=trueObsCpt;
   point2 senseDynObsVel=tvel;//dynamic obstacle data
   
   Spoof spoofer(modelType,sensedSigma,trueDynObsPos,trueDynObsVel,badReturnRate);
     
   vector<point2arr> lbound(N),rbound(N);
   point2arr sensedPos; //static data vector
   point2arr sensedVel;   
   
   point2arr truePos; //static data vector
   point2arr trueVel;   

   double datax[N],datay[N],covAng[N];
   double sizeSDBoxx,sizeSDBoxy,angCovBox;
   point2 cptCovBox;

  //MAIN PREDICTION LOOP           
  for (int i =0; i< horizon; i++)
   { 
     ang = dob1PF.getang();
     angData= spoofer.getdynObsAng();
    
    //$$$$Plotting of data  
    /*
       point2 particlesAve(0,0);
       double angAve = 0;  
    for(int k=0; k<N;k++)
       {   
        particlesAve = particlesAve + particlesNow[k];
        angAve = angAve + ang[k];  
       }    
    
    particlesAve = particlesAve/N;
    angAve = angAve/N;
       
      dob1.set_block_obs(iddob1,particlesAve,angAve,dob1size.x,dob1size.y); 
      dob1.plot_color=MAP_COLOR_RED;
      sendMapElement(&dob1,sendChannel); 
   */   
     ///*     
     for(int k=0; k<50;k++)
         {   
          dob1.set_block_obs(iddob1,particlesNow[k],ang[k],dob1size.x,dob1size.y);
          dob1.plot_color=MAP_COLOR_RED;
          sendMapElement(&dob1,sendChannel); //here's how we send the obstacle to skynet
          iddob1.clear();
          iddob1.push_back(k);
         } 
    // */
      //computing covariance box parameters
      for(int k=0; k<N;k++)
       {
        datax[k] = particlesNow[k].x;
	datay[k] = particlesNow[k].y;
        covAng[k] = ang[k];
       }
      
      cptCovBox.x = gsl_stats_mean(datax,1,N);
      cptCovBox.y = gsl_stats_mean(datay,1,N); 
     
      sizeSDBoxx = gsl_stats_sd_m(datax,1,N,cptCovBox.x);
      sizeSDBoxy = gsl_stats_sd_m(datay,1,N,cptCovBox.y);
      //sizeSDBoxx = pow((cptCovBox.x-trueDynObsPos.x),2);
      //sizeSDBoxy = pow((cptCovBox.y-trueDynObsPos.y),2);           
      //sizeSDBoxx = pow(sizeSDBoxx,0.5);
      //sizeSDBoxy = pow(sizeSDBoxy,0.5);

      angCovBox = gsl_stats_mean(covAng,1,N);

      //cout<<sizeSDBoxx<<endl;

      covBox.set_block_obs(idcovBox,cptCovBox,angCovBox,1*sizeSDBoxx,1*sizeSDBoxy);
      covBox.plot_color=MAP_COLOR_YELLOW;
      sendMapElement(&covBox,sendChannel);

      dob1TrueData.set_block_obs(iddob1TrueData,trueDynObsPos,angData,dob1size.x,dob1size.y);
      dob1TrueData.plot_color=MAP_COLOR_GREEN;
      sendMapElement(&dob1TrueData,sendChannel);

      dob1SenseData.set_block_obs(iddob1SenseData,senseDynObsPos,angData,dob1size.x,dob1size.y);
      dob1SenseData.plot_color=MAP_COLOR_MAGENTA;
      sendMapElement(&dob1SenseData,sendChannel);
      //$$$$$$$$$$$$end plotting of data

      //begin particle filter and measurements
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
   
   start=DGCgettime();
   
   //spoof & measure
   spoofer.dynObs(deltaT);
   
   particlesPlus=dob1PF.sampleConstVel(sampleFactor,deltaT);//predicts Now to Plus
    //particlesPlus=dob1PF.sampleCoordTurn(sampleFactor,deltaT);

   for(int k=0;k<N;k++)
    {
     m_localMap->getBounds(lbound[k],rbound[k],dob1label,particlesPlus[k],range);
    }

    senseDynObsPos = spoofer.getSenseDynObsPos();    
    senseDynObsVel = spoofer.getSenseDynObsVel();    
        
    trueDynObsPos = spoofer.getTrueDynObsPos();    
    trueDynObsVel = spoofer.getTrueDynObsVel();
   
    sensedPos.push_back(senseDynObsPos); 
    sensedVel.push_back(senseDynObsVel);    

    truePos.push_back(trueDynObsPos); 
    trueVel.push_back(trueDynObsVel); 
    //end spoofing & measurements     

    dob1PF.weight(lbound,rbound,sensedPos,sensedVel,i,sensedSigma);
    
    dob1PF.resample(partitions,resampleNoise);
    
    particlesNow=dob1PF.getParticlesNow();
    velNow=dob1PF.getVelNow();
    //end PF
    
    end = DGCgettime();
    //deltaT = (end-start)/100000.0;
 
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    //alice's state
    UpdateState(); //updates m_state
    alice.set_alice(m_state);
    sendMapElement(&alice,sendChannel);
    //end alice's state    

    //here we retrieve the lineboundary at time t = i
    //m_localMap->getBounds(lbound, rbound, dob1label,dob1cpt, range);
		
    lineBoundary.clear();
    lineBoundary.set_id(1,2,-4);
    lineBoundary.set_laneline();
    lineBoundary.set_geometry(lbound[0]);
    lineBoundary.plot_color=MAP_COLOR_GREEN;
    sendMapElement(&lineBoundary,sendChannel);

    lineBoundary.clear();
    lineBoundary.set_id(1,2,-5);
    lineBoundary.set_laneline();
    lineBoundary.set_geometry(rbound[0]);
    lineBoundary.plot_color=MAP_COLOR_MAGENTA;
    sendMapElement(&lineBoundary,sendChannel);
    //finish retrieving lineboundary
    usleep(100000);
    }
  }
}
