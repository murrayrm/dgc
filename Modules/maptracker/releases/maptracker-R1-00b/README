Description of "maptracker" module:
The point of this module is to do tracking at the map level; as a first cut, we suggest doing tracking of dynamic obstacles through the sensed map, such that static information be included in the estimate.  

For instance, a dynamic obstacle observation (i.e., the output of Laura's/Daniele's/the radar's algorithm) is passed up to the map; the "prediction" (in the filtering sense here, so the prediction is over a time step of the same length as it takes to make an observation) would then take into account RNDF/sensed data/static obstacle information.  Through standard Bayesian updating, this information is then incorporated into the final estimate of the dynamic obstacle.  

Additionally, one might imagine using RNDF/sensed data information at this level to eliminate many of the false positives which perceptors inevitably produce.

The end goal of this implementation is full "data" fusion: one can imagine simultaneously estimating the state values of any object in the map against both RNDF, sensed, and dynamic obstacle data.

Notice also that "OBsensor" fusion can be accomplished in a straightforward way within this framework, so long as particle filters are used: the measurement MODE is merely another factor of the likelihood/weighting function.  RNDF, sensed obstacle data (of any mode), and dynamic obstacle data are all treated in the same fashion: as a factor in the likelihood function (weight function) of the particle filter.

This initial implementation of maptracker is just a copy of mapprediction, except that we pass perceptor data to the weighting function.
	

Other modules that "maptracker" depends on:
Libraries:
gsl

Modules:
map
sn_msg.hh
StateClient.hh
Skynet_Talker.hh
DGCutils.hh
Particlefilter.hh
AliceConstants.hh
cotk.h
ncurses.h

