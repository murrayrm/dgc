/*!MapPrediction.cc
 * Author: Pete Trautman
 * Last revision: April 18 2007
 * */

#include "MapTracker.hh"

using namespace std;

CMapTracker::CMapTracker(int skynetKey, bool bWaitForStateFill,
                                 int debugLevel, int verboseLevel, 
                                 bool log)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = skynetKey;
  if (debugLevel>0){
    m_debug = true;
    cout << "debug is on" << endl;
  }
  else m_debug = false;
  if (verboseLevel>0) {
    m_verbose = true;
    cout << "verbose is on" << endl;
  }
  else m_verbose = false;
  
  m_log = log;
  if (m_log) 
    cout << "logging is on" << endl;


  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions
 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey);
  initSendMapElement(skynetKey);
  if(!m_recvLocalMap){
    //bool loadMapFromRNDF =  loadRNDF(string filename) {return localmap.loadRNDF(filename);
  }
  m_localMap = new Map();
}

CMapTracker::~CMapTracker() 
{
  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}


void CMapTracker::getLocalMapThread()
{

  MapElement recvEl;
  int bytesRecv;
  while (true){
  bytesRecv = recvMapElementBlock(&recvEl,1);
 
  if (bytesRecv>0){
      DGClockMutex(&m_LocalMapMutex);
    m_localMap->addEl(recvEl);
    DGCunlockMutex(&m_LocalMapMutex);
  }else {
    cout << "Error in CMapPrediction::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
    usleep(100);
  }
}
 
}

void CMapTracker::MapTrackerLoop(void)
{
  //=====================================================
  UpdateState(); // this gives m_state
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;         
  //======================================================
   
   MapElement recvEl;
   MapElement tempEl;
   int sendChannel=0;

  while(1){
  DGClockMutex(&m_LocalMapMutex);

  for (unsigned i =0; i< m_localMap->data.size(); ++i)
    {    
     sendMapElement(&m_localMap->data[i], sendChannel);
    }

  for (unsigned i =0; i< m_localMap->prior.data.size(); ++i)
    {
      m_localMap->prior.getEl(i,tempEl);
      sendMapElement(&tempEl, sendChannel);
    }

  DGCunlockMutex(&m_LocalMapMutex);

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 //here's where the consequential stuff begins
   
   MapElement dob1; //dynamic obstacle 1
   MapElement alice; //alice
   MapElement lineBoundary; //line boundaries functioning as measurements
   int horizon=130; //horizon of prediction
   double range=10;

   LaneLabel dob1label(1,2); 
   point2 dob1size(2,5);//good size for block cars

   //id identifiers for my obstacles
   vector<int> iddob1; 
   vector<int> idalice;
   iddob1.push_back(1); 
   idalice.push_back(0);
 
   //initialization of the obstacles; this should be read in from mapper in the future
   point2 dob1cpt(-5,-72);//initial position straight down
   //point2 dob1cpt(30,-72);//initial position on turn
   //point2 dob1cpt(0,8);//initial position bottom
   point2 vel(0.5,5);//initial velocity straight down
   //point2 vel(5,0);//initial velocity turn
      
   int N = 50; //number of particles
   double sigma=0.5; //initial covariance in the samples
   double resampleFactor = 0.9; //Neff <= resampleFactor*N
   point2 sampleFactor(0.1,0.4); //noise in sampling distribution
   int modelType=1;
   int partitions = 2;
   point2 noise(0,0);
   vector<double> ang(N);
   //double anginit = 0.05;//-2*acos(-1)/4; ang 0 = straight down; ccw from that is neg 2pi
   point2arr particlesPlus;
   point2arr particlesNow;
   point2arr velNow;
   ParticleFilter dob1PF(resampleFactor,N,dob1cpt,vel,modelType,sigma);   
   
   velNow = dob1PF.getVelNow();
   particlesNow=dob1PF.getParticlesNow();
     
   double deltaT=0.05;	
   long long int start,end;
  //MAIN PREDICTION LOOP           
  for (int i =0; i< horizon; ++i)
   { 
     ang = dob1PF.getang();
     //cout<<"ang"<<ang<<endl;
     for(int k=0; k<N;k++)
         {   
          dob1.set_block_obs(iddob1,particlesNow[k],ang[k],dob1size.x,dob1size.y);
          dob1.plot_color=MAP_COLOR_RED;
          sendMapElement(&dob1,sendChannel); //here's how we send the obstacle to skynet
          iddob1.clear();
          iddob1.push_back(k);
         }
       //begin particle filter
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
   
   start=DGCgettime();
   
   particlesPlus=dob1PF.sampleConstVel(sampleFactor,deltaT);//predicts Now to Plus
   //particlesPlus=dob1PF.sampleCoordTurn(sampleFactor,deltaT);
   
   //collecting the measurements for each particle; in general, one measurement
   //will exist for particle set at time t 
     vector<point2arr> lbound(N),rbound(N); //measurement vector
   for(int k=0; k<N;k++)
    {
     m_localMap->getBounds(lbound[k],rbound[k],dob1label,particlesPlus[k],range);
    }
    
      vector<point2arr> sensed_data;
    //for (unsigned i =0; i< m_localMap->data.size(); ++i)
    //{    
    // sensed_data = m_localMap->data[i];
   // }
    //end calculating "Measurement"
 
    //here is the corresponding weight function:
    //dob1PF.weight(lbound,rbound,sensed_data);

    dob1PF.weight(lbound,rbound,sensed_data);
    
    dob1PF.resample(partitions);
    
    particlesNow=dob1PF.getParticlesNow();
    velNow=dob1PF.getVelNow();
	//cout<<"velNow"<<velNow<<endl;
    
    //end PF
    
    end = DGCgettime();
   deltaT = (end-start)/100000.0;
    
 
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    //alice's state
    UpdateState(); //updates m_state
    alice.set_alice(m_state);
    sendMapElement(&alice,sendChannel);
    //end alice's state    

    //here we retrieve the lineboundary at time t = i
    //m_localMap->getBounds(lbound, rbound, dob1label,dob1cpt, range);
		
    lineBoundary.clear();
    lineBoundary.set_id(1,2,-4);
    lineBoundary.set_laneline();
    lineBoundary.set_geometry(lbound[0]);
    lineBoundary.plot_color=MAP_COLOR_GREEN;
    sendMapElement(&lineBoundary,sendChannel);

    lineBoundary.clear();
    lineBoundary.set_id(1,2,-5);
    lineBoundary.set_laneline();
    lineBoundary.set_geometry(rbound[0]);
    lineBoundary.plot_color=MAP_COLOR_MAGENTA;
    sendMapElement(&lineBoundary,sendChannel);
    //finish retrieving lineboundary
    usleep(100000);
    }
  }
}








//fun stuff

//calculating the "measurement"
    //point2 cptave(0,0);
    //for(int k=0; k<N;k++)
    //  {
    //   cptave.x = cptave.x + particlesPlus[k].x;
    //   cptave.y = cptave.y + particlesPlus[k].y;
    //  }
    //    cptave.x=cptave.x/N;
    //	cptave.y=cptave.y/N;
   
//point2 centerpt;
    //centerpt = dob1.center;

//here's a block obstacle; first need 
//1. a MapElement dob1
//2. dob1.set_block_obs(id, centerpoint, yaw, w,h)
//alice.center.x = m_state.localX;
    //alice.center.y = m_state.localY;	

//here is an experiment section which inserts some vehicles; use this part 
//to set obstacles in rndf (and which particles run over;
//1.  need to find a way to label/weight rndf portions
//2.  
 //point2 center;
 //center =tempEl.center;
 //point2arr geom; 
 //geom =tempEl.geometry;

 //id.clear();  //clears the id
 //id.push_back(24); //redefines the id

 //--------------------------------------------------
 // sending state to viewer
 //--------------------------------------------------
 
 //how do we get alice to move through the scene?
 //use planner stack; ./mapviewer listens to everything.
 //UpdateState();
 //alice.set_alice(m_state);
 //sendMapElement(&alice,sendChannel);
 //usleep(100000);
	//MapElement pointout
        //pointout.clear();
	//pointout.set_id(100);
	//pointout.set_points();
	//rbound = rbound+point2(10,0);
	
	//point2 tmppt;
	//for (int k =0; k <10; ++k)
	//  { 
 	//   tmppt = rbound[0];
	//   tmppt = tmppt + point2(k,k);
	//   rbound.push_back(tmppt);//.push_back makes a new entry in the vect of value tmppt.
        //  }

	//pointout.set_geometry(rbound);
        //pointout.plot_color=MAP_COLOR_YELLOW;
	//sendMapElement(&pointout,sendChannel);


