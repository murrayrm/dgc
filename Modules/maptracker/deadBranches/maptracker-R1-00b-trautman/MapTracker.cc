/*!MapPrediction.cc
 * Author: Pete Trautman
 * Last revision: April 18 2007
 * */

#include "MapTracker.hh"

using namespace std;

CMapTracker::CMapTracker(int skynetKey, bool bWaitForStateFill,
                                 int debugLevel, int verboseLevel, 
                                 bool log)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = skynetKey;
  if (debugLevel>0){
    m_debug = true;
    cout << "debug is on" << endl;
  }
  else m_debug = false;
  if (verboseLevel>0) {
    m_verbose = true;
    cout << "verbose is on" << endl;
  }
  else m_verbose = false;
  
  m_log = log;
  if (m_log) 
    cout << "logging is on" << endl;


  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions
 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey);
  initSendMapElement(skynetKey);
  if(!m_recvLocalMap){
    //bool loadMapFromRNDF =  loadRNDF(string filename) {return localmap.loadRNDF(filename);
  }
  m_localMap = new Map();
}

CMapTracker::~CMapTracker() 
{
  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}


void CMapTracker::getLocalMapThread()
{

  MapElement recvEl;
  int bytesRecv;
  while (true){
  bytesRecv = recvMapElementBlock(&recvEl,1);
 
  if (bytesRecv>0){
      DGClockMutex(&m_LocalMapMutex);
    m_localMap->addEl(recvEl);
    DGCunlockMutex(&m_LocalMapMutex);
  }else {
    cout << "Error in CMapPrediction::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
    usleep(100);
  }
}
 
}

void CMapTracker::MapTrackerLoop(void)
{
  //=====================================================
  UpdateState(); // this gives m_state
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;         
  //======================================================
   
   MapElement recvEl;
   MapElement tempEl;
   int sendChannel=0;

  while(1){
  DGClockMutex(&m_LocalMapMutex);

  for (unsigned i =0; i< m_localMap->data.size(); ++i)
    {    
     sendMapElement(&m_localMap->data[i], sendChannel);
    }
  
  for (unsigned i =0; i< m_localMap->prior.data.size(); ++i)
    {
      m_localMap->prior.getEl(tempEl,i);
      sendMapElement(&tempEl, sendChannel);
    }
   
  DGCunlockMutex(&m_LocalMapMutex);

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 //here's where the consequential stuff begins
   
   MapElement dob1; //dynamic obstacle 1
   MapElement alice; //alice
   MapElement dob1TrueData,dob1SenseData;
   MapElement lineBoundary; //line boundaries functioning as measurements
   MapElement covBox,covEll;

   int horizon=500; //horizon of prediction
   double range=10;

   LaneLabel dob1label(1,2); 
   point2 dob1size(2,5);//good size for block cars
   
   //initialization of the obstacles; this should be read in from mapper in the future
   point2 dob1cpt(-1,-60);//initial position straight down
   point2 trueObsCpt(-5,-70);
   //point2 dob1cpt(30,-72);//initial position on turn
   //point2 dob1cpt(0,8);//initial position bottom
   point2 vel(0.7,3);//initial velocity straight down
   //point2 vel(5,0);//initial velocity turn
   point2 tvel(0.7,3);      

   int N = 50; //number of particles
   double sigma=2; //initial covariance in the samples
   point2 resampleNoise(0.4,0.4);
   double resampleFactor = 0.7; //Neff <= resampleFactor*N
   point2 sampleFactor(1.0,1.0); //how reliable the sampling distribution is
   int modelType=1;
   int partitions = 2;
   point2 noise(0,0);
   vector<double> ang(N);
   double angData;
   //double anginit = 0.05;//-2*acos(-1)/4; ang 0 = straight down; ccw from that is neg 2pi
   point2arr particlesPlus;
   point2arr particlesNow;
   point2arr velNow;
   ParticleFilter dob1PF(resampleFactor,N,dob1cpt,vel,modelType,sigma);   
   
   velNow = dob1PF.getVelNow();
   particlesNow=dob1PF.getParticlesNow();
     
   double deltaT=0.1;	
   long long int start,end;

   double sensedSigma=1.0; //how reliable the data is
   double badReturnRate = 0.0; //1=always drop, 0 = never drop
   point2 trueDynObsPos=trueObsCpt;
   point2 trueDynObsVel=tvel;//dynamic obstacle data
   point2 senseDynObsPos=trueObsCpt;
   point2 senseDynObsVel=tvel;//dynamic obstacle data
   
   Spoof spoofer(modelType,sensedSigma,trueDynObsPos,trueDynObsVel,badReturnRate);
     
   vector<point2arr> lbound(N),rbound(N);
   point2arr sensedPos; //static data vector
   point2arr sensedVel;   
   
   point2arr truePos; //static data vector
   point2arr trueVel;   

   double datax[N],datay[N],covAng[N];
   double varX,varY,angCovEll;
   point2 cptCovEll;
   point2_uncertain covEllData;

  //MAIN PREDICTION LOOP           
  for (int i =0; i< horizon; i++)
   { 
     ang = dob1PF.getang();
     angData= spoofer.getdynObsAng();
    
    //$$$$Plotting of data  
    /*
       point2 particlesAve(0,0);
       double angAve = 0;  
    for(int k=0; k<N;k++)
       {   
        particlesAve = particlesAve + particlesNow[k];
        angAve = angAve + ang[k];  
       }    
    
    particlesAve = particlesAve/N;
    angAve = angAve/N;
       
      dob1.set_block_obs(iddob1,particlesAve,angAve,dob1size.x,dob1size.y); 
      dob1.plot_color=MAP_COLOR_RED;
      sendMapElement(&dob1,sendChannel); 
   */   
     ///*
	 
     for(int k=0; k<50;k++)
         { 
          dob1.id=k;
          dob1.setTypeVehicle();

          dob1.setColor(MAP_COLOR_RED,100);
          dob1.setGeometry(particlesNow[k], dob1size.x, dob1size.y, ang[k]);
          sendMapElement(&dob1,sendChannel);
         } 
    // */
      //computing covariance parameters
      for(int k=0; k<N;k++)
       {
        datax[k] = particlesNow[k].x;
	datay[k] = particlesNow[k].y;
        covAng[k] = ang[k];
       }
      
      cptCovEll.x = gsl_stats_mean(datax,1,N);
      cptCovEll.y = gsl_stats_mean(datay,1,N); 
      
      varX = gsl_stats_variance_m(datax,1,N,cptCovEll.x);
      varY = gsl_stats_variance_m(datay,1,N,cptCovEll.y);
      
      angCovEll = gsl_stats_mean(covAng,1,N);
      /*
      covEllData.set(cptCovEll.x, cptCovEll.y, varX,varY, angCovEll);
      covEll.id=-5;
      covEll.setColor(MAP_COLOR_YELLOW,100);
      covEll.setGeometry(covEllData);      
      sendMapElement(&covEll,sendChannel);    
      */
    
      covEll.id=-5;
      covEll.setColor(MAP_COLOR_YELLOW,100);
      
      if(varX >= varY)
        { covEll.setGeometry(cptCovEll,varX); }
      else      
        { covEll.setGeometry(cptCovEll,varY); }
      sendMapElement(&covEll,sendChannel);

      dob1TrueData.id=N+1;
      dob1TrueData.setTypeVehicle();

      dob1TrueData.setColor(MAP_COLOR_GREEN,100);
      dob1TrueData.setGeometry(trueDynObsPos, dob1size.x, dob1size.y, angData);
      sendMapElement(&dob1TrueData,sendChannel);

      dob1SenseData.id=N+2;
      dob1SenseData.setTypeVehicle();

      dob1SenseData.setColor(MAP_COLOR_MAGENTA,100);
      dob1SenseData.setGeometry(trueDynObsPos, dob1size.x, dob1size.y, angData);
      sendMapElement(&dob1SenseData,sendChannel);
      
      //begin particle filter and measurements
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
   
   start=DGCgettime();
   
   //spoof & measure
   spoofer.dynObs(deltaT);
   
   particlesPlus=dob1PF.sampleConstVel(sampleFactor,deltaT);//predicts Now to Plus
    //particlesPlus=dob1PF.sampleCoordTurn(sampleFactor,deltaT);

   for(int k=0;k<N;k++)
    {
     m_localMap->getBounds(lbound[k],rbound[k],dob1label,particlesPlus[k],range);
    }

    senseDynObsPos = spoofer.getSenseDynObsPos();    
    senseDynObsVel = spoofer.getSenseDynObsVel();    
        
    trueDynObsPos = spoofer.getTrueDynObsPos();    
    trueDynObsVel = spoofer.getTrueDynObsVel();
   
    sensedPos.push_back(senseDynObsPos); 
    sensedVel.push_back(senseDynObsVel);    

    truePos.push_back(trueDynObsPos); 
    trueVel.push_back(trueDynObsVel); 
    //end spoofing & measurements     

    dob1PF.weight(lbound,rbound,sensedPos,sensedVel,i,sensedSigma);
    
    dob1PF.resample(partitions,resampleNoise);
    
    particlesNow=dob1PF.getParticlesNow();
    velNow=dob1PF.getVelNow();
    //end PF
    
    end = DGCgettime();
    //deltaT = (end-start)/100000.0;
 
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    //alice's state
    UpdateState(); //updates m_state
    alice.set_alice(m_state);
    sendMapElement(&alice,sendChannel);
    //end alice's state    
    
    usleep(100000); 
   
    }
  }
}
