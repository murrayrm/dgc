              Release Notes for "follower" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "follower" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "follower" module can be found in
the ChangeLog file.

Release R1-00x (Sun Aug 12 21:24:43 2007):
Mostly minor fixes, not affecting overall performance.

Release R1-00w (Sun Aug 12 13:09:39 2007):
Resolved fault handling bugs found in simulation.

Release R1-00v (Fri Aug 10 17:47:11 2007):
First implementation with complete CSS structure, stable enough for the field. Anti integrator windup is implemented and tested. Handling of actuator failures is partly tested.

Release R1-00u (Fri Aug 10 12:22:49 2007):
CSS fault handling is all in place now. 

This release is stable, but should not be concidered safe, since integral antiwindup and other special case handling has not been tested since the program structure changed.

A new release, with verified fault handling functionality will be availible today, or latest this weekend.


Release R1-00t (Wed Aug  8 18:30:04 2007):
Eliminated the segfault occuring in the previous release. Went through the code and cleaned up bugs.
Improved comments.

Release R1-00s (Tue Aug  7  8:29:36 2007):
The first stable release after merging with the CSS code Vanessa wrote. There is still a little of
CSS stuff to be done, but most is in place.

Apart from CSS, a major code restructuring and clean has been done. 

Currently follower segfaults and dumps core when you run it from process control and try to quit it by pressing q or 
hitting the QUIT button in its sparrowHawk display. (However, there seems to be no segfault when closing follower
down from process control, or when closing process control while follower is running.)

Do not turn on lateral integration using this release.

Release R1-00r (Mon Aug  6 23:02:19 2007):
When limiting max lateral acceleration in the previous release, I forgot the 
'a' in front of 'tan'. The correct way to do this is:

|steerAngle| < atan(ALICE_WHEELBASE * MAX_LATERAL_ACCELERATION / currentSpeed^2)

Also, there is a test that currentSpeed > verySmallSpeed, to avoid div by 0 errors.


Release R1-00q (Mon Aug  6 22:00:19 2007):
Limited max lateral acceleration, by limiting steer angle so that
|steerAngle| < tan(ALICE_WHEELBASE * MAX_LATERAL_ACCELERATION / currentSpeed^2)

Modified followerDisplay. For some reason process control only displays the last 24 rows of my table. Robbie is working on this. I believe it affects all modules using sparrow(Hawk). 

Changed longitudinal feed forward parameter. Longitudinal feed forward is now fully used.


Release R1-00p (Mon Aug  6 19:41:12 2007):
No longer assumes Alice is in gear 1 when follower starts. Instead the gear at startup is read from actuatorstate. There could still be a problem if Alice is shifting gears while the constructor of follower is invoked. 

Release R1-00o (Mon Aug  6 12:15:21 2007):
Now the follower does not steer if Estop paused && vehicle stopped.

Release R1-00n (Mon Aug  6  0:39:51 2007):
The integrals for longitudinal and lateral control are reset when EstopPos is not EstopRun. They are also reset, when trajectory receiving from planner times out. 

The way it is done, is by reading directly from ActuatorState. I will move over to use the SF towards gcdrive for obtaining Estop info. (This, however, requires information about Estop state as reason to the reject-responses sent by gcdrive.)

In order to yam save, I had to update my link modules. After this, yam auto-merged my code (since I was not working on the latest release of the follower module). Unfortunately, it was no longer possible to simulate using process-control after the link update. This means I have not been able to verify that my code merged correctly. 

Release R1-00m (Sat Aug  4  1:42:05 2007):
	Changed default value of parameter lo_alpha from 1.0 to 0.1. This means that feed forward acceleration is multiplied by 0.1 before 
application.

Release R1-00l (Thu Aug  2 23:59:38 2007):
        Shifting gears is now done the propper CSS way. Follower pauses if it does not receive any trajectories, and unpauses if it starts receiving trajectories anew. Code for a North face is in place, but so far commented away. It compiled on another branch, but still have not been able to reproduce that behavior here.  

Release R1-00k (Tue Jul 31 15:12:42 2007):
        Removed compiler warnings. Added some minor things needed for NF / SF.

Release R1-00j (Mon Jul 30 19:54:32 2007):
        Got rid of bug, causing Alice to freeze in simulation.

Release R1-00i (Fri Jul 27 14:46:24 2007):
        Added option to follow front wheel trajectory (as opposed to rear wheel). Reversing (and U turn) works in simulation. Added option to run on feed forward only (as opposed to feed forward + feedback). Restructured the way in which commands are sent to gcdrive.

Release R1-00h (Thu Jul 26 12:18:44 2007):
        Added an option to visualize the operation of the lateral controller in mapviewer. Plus some minor improvements.

Release R1-00g (Tue Jul 24 23:00:49 2007):
        Changed from isA to hasA CTrajTalker. Let traj communication tread exit cleanly. Hopefully, the sparrow thread also exits cleanly (at least no core dumps so far). Cleaned up some code and added some comments. Started preparing for proper CSS NF and SF usage.

Release R1-00f (Tue Jul 24  2:19:58 2007):
	Added automatic shifting, integral anti-windup and minor fixes.

Release R1-00e (Thu Jul 19 23:53:43 2007):
	Release for friday test 2007-07-20.

Release R1-00d (Wed Jul 18  5:53:18 2007):
	Added logging capabilities, Sparrow interface and some small improvements.

Release R1-00c (Mon Jul 16  12:40:10 2007):
        Release with controllers working in simulation, and on Alice. Not yet tested together with the entire planning stack. Some features not yet implemented.

Release R1-00a (Wed Jun 27 14:07:30 2007):
        Added programs:
        * adriveloggen (generates skynet log file containing AdriveDirectives)
        * skynetlogparser (parses skynet log file and writes to file which Matlb can read)

        FIXME: use gengetopt for command line parsing, clean up code.
        

Release R1-00 (Thu Jun 21 17:54:31 2007):
	Created.

























