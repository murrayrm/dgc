#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <signal.h>
#include <sys/stat.h>

#include <dgcutils/DGCutils.hh>
#include <skynet/sn_msg.hh>
#include "CmdArgs.hh"
#include "cmdline.h"
#include "Follower.hh"


using namespace std;

volatile sig_atomic_t quit = 0;
void sigintHandler(int /*sig*/)
{
  // if the user presses CTRL-C three or more times, and the program still
  // doesn't terminate, abort
  if (quit > 2) {
    abort();
  }
  quit++;
}

int main(int argc, char **argv) 
{
  // catch CTRL-C and exit cleanly, if possible
  signal(SIGINT, sigintHandler);
  // and SIGTERM, i.e. when someone types "kill <pid>" or the like
  signal(SIGTERM, sigintHandler);

  gengetopt_args_info cmdline;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
  
  /* Figure out what skynet key to use */
  CmdArgs::sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << CmdArgs::sn_key << endl;

  if(cmdline.log_level_arg > 0)
  {
    /* Check if the logs directory exists */
    struct stat st;
    int ret = stat(cmdline.log_path_arg, &st);
    if (errno == ENOENT) {
      if(mkdir(cmdline.log_path_arg, 0755) != 0)
	cerr << __FILE__ << ":" << __LINE__ << "cannot create log dir.";
    }
  }

  CmdArgs::log_level = cmdline.log_level_arg;
  CmdArgs::log_path = cmdline.log_path_arg;

  /*! control status sent from control to arbiter */
  FollowerControlStatus controlStatus;

  /*! merged directive sent from arbiter to control */
  FollowerMergedDirective mergedDirective;

  /*! Follower object */
  Follower follower(CmdArgs::sn_key, &controlStatus, &mergedDirective);

  unsigned long long timestamp1, timestamp2;
  const long SLEEP_TIME = (long)1000000/cmdline.rate_arg;  

  DGCstartMemberFunctionThread(&follower, &Follower::CommThread);

  while (!quit)
  {
    // timers to make sure our control frequency is what we set (not
    // affected by how long it takes to compute each cycle)

    // time checkpoint at the start of the cycle
    DGCgettime(timestamp1);

    //perform operations necessary to figure out the merged directive for this
    //control cycle
    follower.arbitrate(&controlStatus, &mergedDirective);

    //perform operations to make the vehicle comply with the merged directive
    follower.control(&controlStatus, &mergedDirective);

    // time checkpoint at the end of the cycle
    DGCgettime(timestamp2);

    /** now sleep for SLEEP_TIME usec less the amount of time the
     * computation took */
    int delaytime = SLEEP_TIME - (timestamp2 - timestamp1);

    if(delaytime > 0)
      usleep(delaytime);
  }
  
  return 0;
} // end main()
