#ifndef CMDARGS_HH_345678945670987654
#define CMDARGS_HH_345678945670987654

#include <string>

using namespace std;

class CmdArgs {
public:
  static int sn_key;
  static int log_level;
  static char* log_path;
};

#endif
