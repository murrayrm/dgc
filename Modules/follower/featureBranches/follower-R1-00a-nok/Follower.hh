#ifndef FOLLOWER_MODULE_345623456789098765
#define FOLLOWER_MODULE_345623456789098765

#include <interfaces/sn_types.h>
#include <trajutils/TrajTalker.hh>
#include <gcmodule/GcModule.hh>
#include <gcmodule/GcInterface.hh>
#include <gcinterfaces/AdriveCommand.hh>
#include <gcinterfaces/FollowerCommand.hh>
#include <skynettalker/StateClient.hh>

class FollowerControlStatus : public ControlStatus
{
public:
  unsigned id;
  FollowerResponse::FollowerFailure reason;

  FollowerControlStatus() {
    id = 0;
    status = RUNNING;
  }
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " status: " << status;
    return s.str();
  }
};

/*
 * \class FollowerMergedDirective
 * 
 * This is the directive sent between Follower Arbitration and
 * Follower Control.
 */
class FollowerMergedDirective : public MergedDirective
{
public:
  unsigned id;
  CTraj* traj;
  FollowerMergedDirective() {
    id = 0;
    traj = NULL;
  }
  std::string toString() const {
    stringstream s("");
    s << "id: " << id;
    return s.str();
  }
};

/*
 * \class Follower
 *
 * This class is the actual GcModule for Follower
 */
class Follower : public CStateClient, public CTrajTalker, public GcModule
{
private:

  /*!\param GcInterface variable for talking to gcdrive */
  AdriveCommand::Southface *m_adriveCommandSF;

  /*!\param directive to be sent to gcdrive */
  AdriveDirective m_directive;

  /*!\param ID for the next directive to send to gcdrive */
  unsigned m_adriveCommandID;

  /*!\param response to be sent to pplanner */
  FollowerResponse m_response;

  /*!\param Latest received traj */
  CTraj* m_recvdTraj;

  /*!\param Mutex controlling access to m_recvdTraj */
  pthread_mutex_t m_recvdTrajMutex;

  /*! Send directive to gcdrive */
  void sendDirective();

  /*! Send response back to planner */
  void sendResponse();

public:
  /*! Constructor */
  Follower(int, FollowerControlStatus*, FollowerMergedDirective*);

  /*! Destructor */
  ~Follower();

  /*! Arbitration for the Follower module. It computes the next
    merged directive based on the directives from the latest
    received traj and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);

  /*! Control for the Follower module. It computes and sends
      directives to gcdrive based on the 
      merged directive and outputs the control status
      based on the latest status from gcdrive. */
  void control(ControlStatus*, MergedDirective*);

  /*! Communication thread: receive new traj from skynet */
  void CommThread ();
};

#endif //FOLLOWER_MODULE_345623456789098765
