#include "Follower.hh"
#include "CmdArgs.hh"

using namespace std;

Follower::Follower(int skynetKey, FollowerControlStatus* controlStatus, FollowerMergedDirective* mergedDirective) :
  CSkynetContainer(SNtrajfollower, skynetKey),
  CStateClient(true),
  GcModule("Follower", controlStatus, mergedDirective),
  m_adriveCommandID(1),
  m_recvdTraj(0)
{
  m_adriveCommandSF = AdriveCommand::generateSouthface(skynetKey, this);
  if (CmdArgs::log_level > 0) {
    stringstream ssfilename("");
    ssfilename << CmdArgs::log_path  << "/follower";
    this->addLogfile(ssfilename.str());
    this->setLogLevel(CmdArgs::log_level);
  }

  this->setLogLevel(CmdArgs::log_level);
  m_adriveCommandSF->setStaleThreshold(100);
  DGCcreateMutex(&m_recvdTrajMutex);
}

Follower::~Follower() 
{
  AdriveCommand::releaseSouthface( m_adriveCommandSF );
  DGCdeleteMutex(&m_recvdTrajMutex);
  if (m_recvdTraj != NULL)
    delete m_recvdTraj;
}


/*
 * Follower::arbitrate
 *
 * Arbitrator for Follower.  The artibiter receives traj
 * commands from the planner and sends them to control.
 *
 */
void Follower::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  FollowerControlStatus* controlStatus = dynamic_cast<FollowerControlStatus*>(cs);
  FollowerMergedDirective* mergedDirective = dynamic_cast<FollowerMergedDirective*>(md);

  /* 
   * Check on the status of the last command acted on by control 
   *
   * Since we operate a FIFO queue, we can just need to pass the
   * information from execution back to the actuation interface.  The
   * directive IDs will make sure that everyone stays in sync.
   */
  switch (controlStatus->status) {
  case ControlStatus::STOPPED:
    /* We have finished processing the directive; return a response */
    m_response.id = controlStatus->id;
    m_response.status = GcInterfaceDirectiveStatus::COMPLETED;
    sendResponse();
    break;

  case ControlStatus::FAILED:
    /* A failure occured in processing the directive; return a response */
    m_response.id = controlStatus->id;
    m_response.status = GcInterfaceDirectiveStatus::FAILED;
    m_response.reason = controlStatus->reason;
    sendResponse();
    gclog(1) << m_response.toString() << endl;
    break;

  case ControlStatus::RUNNING:
    /* No action required */
    break;
  }

  /*
   * Compute the next mergedDirective
   *
   * This code determines whether or not there is a new directive to
   * be sent to the execution unit.  We use the directive ID to keep
   * to keep track of whether we have pass something new down.  Note
   * that the ID for the mergedDirective is just a counter => we have
   * to keep track of the parent ID separately.
   * 
   */
  DGClockMutex(&m_recvdTrajMutex); // This will be unlocked in the control loop
  if (m_recvdTraj != NULL && m_recvdTraj != mergedDirective->traj) {
    mergedDirective->traj = m_recvdTraj;
    mergedDirective->id++;
  }
}

/*
 * FollowerModule::control
 *
 * Control for Follower.  The artibiter receives merged
 * directives from the Follower arbitrator.  This is
 * just the latest traj.
 *
 */
void Follower::control(ControlStatus *cs, MergedDirective *md)
{
  FollowerControlStatus* controlStatus = dynamic_cast<FollowerControlStatus *>(cs);
  FollowerMergedDirective* mergedDirective = dynamic_cast<FollowerMergedDirective *>(md);
  CTraj* traj = mergedDirective->traj;
  
  if (traj == NULL || traj->getNumPoints() == 0) {
    m_directive.actuator = Acceleration;
    m_directive.command = Pause;
    sendDirective();
  }
  else {
    UpdateState();
    cout << "Traj ID = " << mergedDirective->id << " size = " << traj->getNumPoints() << endl;
  }
  DGCunlockMutex(&m_recvdTrajMutex); // Locked in the arbitrate loop
}

void Follower::sendResponse()
{

}

void Follower::sendDirective()
{
  m_directive.id = m_adriveCommandID;
  m_adriveCommandID++;
  m_adriveCommandSF->sendDirective(&m_directive);
}

void Follower::CommThread ()
{
  int trajSocket;
  trajSocket = m_skynet.listen(SNtraj, SNplanner);
  CTraj* newTraj = new CTraj(3);
  CTraj* tmpTraj;

  while (true) {
    bool trajReceived = RecvTraj (trajSocket, newTraj);
    while (m_skynet.is_msg(trajSocket)) {
      trajReceived = RecvTraj(trajSocket, newTraj);
    }
    if (trajReceived) {
      cout << "Traj received" << endl;
      DGClockMutex(&m_recvdTrajMutex);
      if (m_recvdTraj == NULL)
	m_recvdTraj = new CTraj(3);
      tmpTraj = newTraj;
      newTraj = m_recvdTraj;
      m_recvdTraj = tmpTraj;
      DGCunlockMutex(&m_recvdTrajMutex);
    }
    else {
      cerr << "ERROR receiving traj" << endl;
    }
  }
  delete newTraj;
}
  
