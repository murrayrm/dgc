t = 0.1;

% Ramps up to speed stays there and ramps down again
posAcc = 1;     % m/s^2
negAcc = -1;     % m/s^2
speed1 = 3;   % m/s
speed2 = 0.5;   % m/s
constTime1 = 6; % s
constTime2 = 6; % s
constTime3 = 6; % s

% acc
a = [posAcc*ones(round(speed1/posAcc/t), 1);
    zeros(constTime1/t,1);
    negAcc*ones(round((speed2-speed1)/negAcc/t), 1);
    zeros(constTime2/t,1);
    posAcc*ones(round(-(speed2-speed1)/posAcc/t),1);
    zeros(constTime3/t,1);
    negAcc*ones(round(-speed1/negAcc/t),1);
    ];
v = t*cumsum(a);
p = t*cumsum(v);

T = [p v a zeros(length(a),3)];

while T(end,1)==T(end-1,1) && T(end,4)==T(end-1,4)
    T = T(1:end-1,:);
end

dist = p(end)

save speedRamps3_05_3.traj T -ASCII