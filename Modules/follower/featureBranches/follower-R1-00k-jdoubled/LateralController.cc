/*!
 * \file LateralController.cc
 * \brief Takes care of the lateral lateral part in trajectory following.
 *
 * \author Kristian Soltesz, Magnus Linderoth
 * \date 10 July 2007
 *
 * This file is part of the follower module.
 */

#include "alice/AliceConstants.h"
#include "LateralController.hh"


/* Constructor for LateralController */
LateralController::LateralController(LateralControllerParams* params, FollowerLogger* internalLogger):
  m_params(*params), 
  m_internalLogger(internalLogger),
  m_I(0)
{
  /* Send state pointers to SparrowHawk display */
  sparrowHawk->rebind("la_error", &m_lateralError);
  sparrowHawk->set_readonly("la_error");
  sparrowHawk->rebind("la_I", &m_I);
  sparrowHawk->set_readonly("la_I");
  sparrowHawk->rebind("la_phi", &m_old_phi);  
  sparrowHawk->set_readonly("la_phi");   
  sparrowHawk->rebind("la_phiFF", &m_old_phiFF);
  sparrowHawk->set_readonly("la_phiFF");
  sparrowHawk->rebind("la_l1_gs", &m_l1_gs);
  sparrowHawk->set_readonly("la_l1_gs");
  sparrowHawk->rebind("la_l2_gs", &m_l2_gs);
  sparrowHawk->set_readonly("la_l2_gs");

  /* Send param pointers to SparrowHawk display */
  sparrowHawk->rebind("la_l1",&m_params.l1);
  sparrowHawk->rebind("la_l1Scale",&m_params.l1Scale);
  sparrowHawk->rebind("la_l2",&m_params.l2);
  sparrowHawk->rebind("la_l2Scale",&m_params.l2Scale);
  sparrowHawk->rebind("la_trackFront",&m_params.trackFront);
  sparrowHawk->rebind("la_alpha",&m_params.alpha);
  sparrowHawk->rebind("la_delay",&m_params.delay);
  sparrowHawk->rebind("la_replanError",&m_params.replanError);

  /* Receive whether lateral controller principle should be visulized in mapviewer from SparrowHawk display*/
  sparrowHawk->set_notify("sparrowToggleVisLat", this, &LateralController::sparrowToggleVisLat);
  sparrowHawk->set_keymap((int)'l', this, &LateralController::sparrowToggleVisLat);

}

/* Method for caluclating control signal */
bool LateralController::control(FollowerResponse* response, AdriveDirective* steeringDirective, 
                                FollowerMergedDirective* followerMergedDirective,
                                SingleFrameVehicleState* vehicleState, int dir, int EstopState) {

  CTraj* traj = followerMergedDirective->traj; 

  /* Calculate feed forward distance */
  double ffDist = vehicleState->speed * m_params.delay;

  /* Current vehicle state */
  double xCR = vehicleState->x;
  double yCR = vehicleState->y;
  double thetaC = vehicleState->yaw;
  if (CmdArgs::verbose >= 2) printConsole("Alice vehicle state: x=" << xCR << " y=" << yCR << " yaw=" << thetaC << endl);

  /* Determine the reference and feed forward points on the traj by linear interpolation*/
  int closestIndex;
  double closestFractionToNext;
  TrajPoint refPoint(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  if (m_params.trackFront) {
    refPoint = traj->interpolGetClosest(xCR + dir*VEHICLE_WHEELBASE*cos(thetaC),
                                        yCR + dir*VEHICLE_WHEELBASE*sin(thetaC),
                                        &closestIndex, &closestFractionToNext); 
  } else {
    refPoint = traj->interpolGetClosest(xCR,
                                        yCR,
                                        &closestIndex, &closestFractionToNext); 
  }
  TrajPoint ffPoint = traj->interpolGetPointAhead(closestIndex, closestFractionToNext, ffDist, NULL, NULL);

  /* Calcualte feed forward control signal. Avoid division by zero */
  double phiFF;
  if (ffPoint.nd == 0 && ffPoint.ed == 0) {
    /* If feed forward velocity is zero ==> turning radius cannot be calculated, 
     * then use the latest valid feed forward steering */
    phiFF = m_old_phiFF;
  } else {
    phiFF = atan(VEHICLE_WHEELBASE * (ffPoint.nd*ffPoint.edd - ffPoint.ed*ffPoint.ndd) /
                 pow(ffPoint.nd*ffPoint.nd + ffPoint.ed*ffPoint.ed, 1.5));   
  }
  m_old_phiFF = phiFF;

  double phi, l1_gs, l2_gs, xS, yS, xCF, yCF;
  static double thetaR = 0;
  if (CmdArgs::ffOnly) {
    phi = dir * phiFF;
  } else {
    /* Detect end of trajectroy */
    bool endOfTraj = closestIndex == traj->getNumPoints() - 1;
    
    /* Error to show in  SparrowHawk display */
    m_lateralError = (-(xCR-refPoint.n)*refPoint.ed + (yCR-refPoint.e)*refPoint.nd) / sqrt(refPoint.nd*refPoint.nd + refPoint.ed*refPoint.ed);
    
    if (CmdArgs::verbose >= 2) printConsole("ClosestIndex: " << closestIndex 
                                            << " ClosestFractionToNext: " << closestFractionToNext << endl);
    
    /* Gain scheduling */
    l1_gs = m_params.l1Scale*vehicleState->speed;
    if (l1_gs < m_params.l1) {
      l1_gs = m_params.l1;
    }
    m_l1_gs = l1_gs;
    
    l2_gs = m_params.l2Scale * vehicleState->speed*vehicleState->speed / VEHICLE_WHEELBASE / VEHICLE_MAX_LATERAL_ACCEL;
    if (l2_gs < m_params.l2) {
      l2_gs = m_params.l2;
    }
    if (CmdArgs::verbose >= 2) printConsole("l1=" << l1_gs << ", l2=" << l2_gs << endl);
    m_l2_gs = l2_gs;
    
    /* Yaw of reference vehicle */ 
    if (refPoint.ed != 0 || refPoint.nd != 0) {
      thetaR = atan2(refPoint.ed, refPoint.nd);
    }
    
    /* Point to steer towards */
    if (m_params.trackFront) {
      xS = refPoint.n + l2_gs*cos(thetaR);
      yS = refPoint.e + l2_gs*sin(thetaR);
    } else {
      xS = refPoint.n + l1_gs*cos(thetaR) + l2_gs*cos(thetaR + m_params.alpha*phiFF);
      yS = refPoint.e + l1_gs*sin(thetaR) + l2_gs*sin(thetaR + m_params.alpha*phiFF);
    }
    if (CmdArgs::verbose >= 2) printConsole("Steer point: " << xS << ", " << yS << endl);
    
    /* Current position of front wheel axis center.
     * In the case of reverse driving it is the front 
     * wheel position of the mirrored car 
     */
    if (m_params.trackFront) {
      xCF = xCR + dir * VEHICLE_WHEELBASE * cos(thetaC);
      yCF = yCR + dir * VEHICLE_WHEELBASE * sin(thetaC);
    } else {
      xCF = xCR + dir * l1_gs * cos(thetaC);
      yCF = yCR + dir * l1_gs * sin(thetaC);
    }
    if (CmdArgs::verbose >= 2) printConsole("Front point: " << xCF << ", " << yCF << endl);
    
    /* Update integral part */
    bool updateIntegral = m_params.lI != 0 && !endOfTraj && EstopState == EstopRun;
    if (updateIntegral) {
      m_I -= m_lateralError * dir * vehicleState->speed / m_params.lI / CmdArgs::rate;
    }
    /* Limit integral part */
    if (m_I > m_params.maxI*VEHICLE_MAX_AVG_STEER) {
      m_I = m_params.maxI*VEHICLE_MAX_AVG_STEER;
    } else if (m_I < -m_params.maxI*VEHICLE_MAX_AVG_STEER) {
      m_I = -m_params.maxI*VEHICLE_MAX_AVG_STEER;
    }
    
    /* Steer angle */
    switch (dir) {
    case 1:
      /* Forward */
      phi = atan2(yS-yCF, xS-xCF) - thetaC + m_I;
      break;
    case -1:
      /* Reverse */
      phi = -(atan2(yS-yCF, xS-xCF) - thetaC + M_PI) + m_I;
      break;
    default:
      if(CmdArgs::verbose) printConsole("LateralController::control error: Direction is neither forwards nor backwards" << endl);
      break;
    }
    
    /* Make sure angle is in interval [-pi, pi[ */
    phi = fmod(phi + 31*M_PI, 2*M_PI) - M_PI;
    if (CmdArgs::verbose >= 2) printConsole("phi: " << phi << "  dir: " << dir << endl);
    
    /* Saturation of steer angle and anti-windup */
    if (phi > VEHICLE_MAX_AVG_STEER) {
      if (updateIntegral) {
        m_I -= phi - VEHICLE_MAX_AVG_STEER;
      }
      phi = VEHICLE_MAX_AVG_STEER;
    } else if (phi < -VEHICLE_MAX_AVG_STEER) {
      if (updateIntegral) {
        m_I -= phi + VEHICLE_MAX_AVG_STEER;
      }
      phi = -VEHICLE_MAX_AVG_STEER;
    }
    
    /* Keep old steering if end of traj is reached */
    if (endOfTraj) {
      phi = m_old_phi;
    }
  }
  
  m_old_phi = phi;
  
  /* Visualize lateral control */
 
  {
    static bool elementDrawn = true;
    MapElement me;    
    int mapIdBase = 3478;    

    if (CmdArgs::vis_lat_control && !CmdArgs::ffOnly) {
      elementDrawn = true;
      point2 point;
      vector<point2> refPoints;
      vector<point2> carPoints;
      
      point.set(refPoint.n, refPoint.e);
      refPoints.push_back(point);
      if (!m_params.trackFront) {
        point.set(refPoint.n + l1_gs*cos(thetaR), refPoint.e + l1_gs*sin(thetaR));
        refPoints.push_back(point);
      }
      point.set(xS, yS);
      refPoints.push_back(point);
      
      if (m_params.trackFront) {
        point.set(xCR + dir*VEHICLE_WHEELBASE*cos(thetaC),
                  yCR + dir*VEHICLE_WHEELBASE*sin(thetaC));
        carPoints.push_back(point);
      } else {
        point.set(xCR, yCR);
        carPoints.push_back(point);
        point.set(xCF, yCF);
        carPoints.push_back(point);
      }
      point.set(xS, yS);
      carPoints.push_back(point);
      
      /* Referance car */
      me.setColor(MAP_COLOR_ORANGE);
      me.setGeometry(refPoints);
      me.setId(mapIdBase + 0);
      me.setTypePoints();
      meTalker.sendMapElement(&me, SEND_SUBGROUP);
      me.setId(mapIdBase + 1);
      me.setTypeLine();
      meTalker.sendMapElement(&me, SEND_SUBGROUP);
      
      /* Real car */
      me.setColor(MAP_COLOR_PINK);
      me.setGeometry(carPoints);
      me.setId(mapIdBase + 2);
      me.setTypePoints();
      meTalker.sendMapElement(&me, SEND_SUBGROUP);
      me.setId(mapIdBase + 3);
      me.setTypeLine();
      meTalker.sendMapElement(&me, SEND_SUBGROUP);
    } else if (elementDrawn) {
      me.setTypeClear();
      elementDrawn = false;
      for (int i = 0; i < 4; i++) {
        me.setId(mapIdBase + i);
        me.setColor(MAP_COLOR_ORANGE);
        meTalker.sendMapElement(&me, SEND_SUBGROUP);
      }
    }
  }

  if (CmdArgs::log_internal) {
    /* Varibales */
    m_internalLogger->la_l1_gs = l1_gs;
    m_internalLogger->la_l2_gs = l2_gs;
    m_internalLogger->la_phiFF = phiFF;
    m_internalLogger->la_phi = phi;
    m_internalLogger->la_I = m_I;
    /* Parameters */
    m_internalLogger->la_l1 = m_params.l1;
    m_internalLogger->la_l1Scale = m_params.l1Scale;
    m_internalLogger->la_l2 = m_params.l2;
    m_internalLogger->la_l2Scale = m_params.l2Scale;
    m_internalLogger->la_alpha = m_params.alpha;
    m_internalLogger->la_delay = m_params.delay;
    /* Common for lateral and longitudinal controller */
    m_internalLogger->closestIndex = closestIndex;
    m_internalLogger->closestFractionToNext = closestFractionToNext;
    m_internalLogger->closestN = refPoint.n;
    m_internalLogger->closestNd = refPoint.nd;
    m_internalLogger->closestNdd = refPoint.ndd;
    m_internalLogger->closestE = refPoint.e;
    m_internalLogger->closestEd = refPoint.ed;
    m_internalLogger->closestEdd = refPoint.edd;
  }


  /* Output steer command to returned AdriveDirective */
  steeringDirective->arg = phi / VEHICLE_MAX_AVG_STEER;
  return true;
}

/* Called from sparrowHawk display to toggle visualization of the lateral controller principle in mapviewer */
void LateralController::sparrowToggleVisLat() {
  CmdArgs::vis_lat_control = !CmdArgs::vis_lat_control;
}

