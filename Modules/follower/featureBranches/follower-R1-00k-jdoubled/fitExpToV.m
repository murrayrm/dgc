minT = 10.8;
maxT = 35;

minI = min(find(VehicleStates(:,1) > minT*1e6));
maxI = max(find(VehicleStates(:,1) < maxT*1e6));

v = speed(minI:maxI);
t = VehicleStates(minI:maxI,1)*1e-6;
y = 2.4*(1-exp(-0.25*(t-t(1))));

figure(935)

plot(t,v,t,y)



