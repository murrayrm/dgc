t = 0.1;


% acc
a = [0.5*ones(18/t,1);
    zeros(9/t,1);
    0.2*ones(12/t,1);
    zeros(30/t,1);
    -0.3*ones(24/t,1);
    -0.7*ones(6/t,1);
    ];
v = t*cumsum(a);
p = t*cumsum(v);

T = [p v a zeros(length(a),3)];


save speedRamps.traj T -ASCII