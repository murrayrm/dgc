#
# This is the source file for managing follower command-line options.  
# It uses gengetopt (http://www.gnu.org/software/gengetopt/gengetopt.html)
#
# Kristian Soltesz, 2007-07-10
#
# This file is part of the follower module
#
# Specification:
#   (1) no command-line options required if default settings are ok
#   (2) Command-line options will override default settings

package "follower"
purpose "The follower module is used to follow a trajectory in Alice. It receives 
trajectory and state information over skynet, and outputs actuation commands over 
skynet. (For testing purposes trajectories can also be loaded from file)."
version "1.0"

#follower options
option "rate"                   r "Rate at which to run follower (Hz)"          int     default="10"      no
option "frame"                  f "Local or UTM frame" string default="local"   no
option "traj-file"              t "Read CTraj from file" string                 no
option "ff-only"                - "Use feed forward control only, no feedback"  flag                      off
option "trajReceiveTimeout"     - "Time before failing to planner"              long    default="1000000" no      

# Configuration files
option "paramfile" - "file containing controller paramters (and perhaps other command line arguments" string default="paramfile" no

# Options for logging
option "log-level" - "set the gcmodule log level" int default="0" no argoptional
option "log-path" - "set the log path"
  string default="./logs" no
option "log-internal" - "log the internal state of follower" flag off

# Standard options
option "skynet-key" S "skynet key" int default="0" no
option "enable-astate" - "enable astate (from Applanix)" flag off
option "disable-console" D "turn off sparrow display" flag  off
option "verbose" v "turn on verbose messages" int default="1" no argoptional

#temporary test option
option "gear" g "gear" int default="1" no

#visualize lateral controller
option "vis-lat-control" - "visualize lateral control" flag off

# Hidden options - used to set controller parameters
option "lo-lV"                  - "longitudinal controller parameter"   double    no      hidden
option "lo-lI"                  - "longitudinal controller parameter"   double    no      hidden
option "lo-lISlow"              - "longitudinal controller parameter"   double    no      hidden
option "lo-maxI"                - "longitudinal controller parameter"   double    no      hidden
option "lo-pitch-gain"          - "longitudinal controller parameter"   double    no      hidden
option "lo-pitch-time"          - "longitudinal controller parameter"   double    no      hidden
option "lo-alpha"               - "longitudinal controller parameter"   double    no      hidden
option "lo-delay"               - "longitudinal controller parameter"   double    no      hidden

option "lo-replanErrorAbs"      - "longitudinal controller fail parameter"   double    no      hidden
option "lo-replanErrorRel"      - "longitudinal controller fail parameter"   double    no      hidden
option "lo-stopSpeed"           - "speed below which we brake and stop"      double    no      hidden 

option "gasDeadU"               - "longitudinal nonlinearity parameter" double   no      hidden
option "gasDeadA"               - "longitudinal nonlinearity parameter" double   no      hidden
option "brakeDeadU"             - "longitudinal nonlinearity parameter" double   no      hidden
option "brakeDeadA"             - "longitudinal nonlinearity parameter" double   no      hidden
option "gasGain"                - "longitudinal nonlinearity parameter" double   no      hidden
option "brakeGain"              - "longitudinal nonlinearity parameter" double   no      hidden
option "gasVelocityGain"        - "longitudinal nonlinearity parameter" double   no      hidden
option "brakeVelocityGain"      - "longitudinal nonlinearity parameter" double   no      hidden
option "noGasSpeed"             - "longitudinal nonlinearity parameter" double   no      hidden
option "idleAcceleration"       - "longitudinal nonlinearity parameter" double   no      hidden

option "la-l1"                  - "lateral controller parameter"        double   no      hidden
option "la-l1-scale"            - "lateral controller parameter"        double   no      hidden
option "la-l2"                  - "lateral controller parameter"        double   no      hidden
option "la-l2-scale"            - "lateral controller parameter"        double   no      hidden
option "la-l2-gain-pow"         - "lateral controller parameter"        double   no      hidden
option "la-lI"                  - "lateral controller parameter"        double   no      hidden
option "la-I-dist"              - "lateral controller parameter"        double   no      hidden
option "la-maxI"                - "lateral controller parameter"        double   no      hidden
option "la-trackFront"          - "lateral controller parameter"        int      no      hidden
option "la-alpha"               - "lateral controller parameter"        double   no      hidden
option "la-delay"               - "lateral controller parameter"        double   no      hidden

option "la-replanError"         - "lateral controller fail parameter"   double   no      hidden




