#ifndef SHIFT_CONTROLLER_HH_JGIUGJBBVGDNMBVIVVCHG
#define SHIFT_CONTROLLER_HH_JGIUGJBBVGDNMBVIVVCHG

#include "FollowerUtils.hh"
#include "interfaces/ActuatorState.h"
#include "gcinterfaces/AdriveCommand.hh"

/*!
 * \class ShiftController
 *
 * This is the class in which the gear is updated to match the trajectory
 */
class ShiftController
{
protected:

  /* SparrowHawk needs these to be members */
  int m_trajDir;
  int m_currentGear;
  int m_lastRealGear;
  int m_commandGear;
  bool m_currentlyShifting;

public:
  /*! Constructor for ShiftController */
  ShiftController();
  
  
  /*! Method in which gear check and update takes place 
   *
   * \param adriveDirective pointer to which the resulting AdriveDirective is written
   * \param directivesToSend pointer to which the number of resulting AdriveDirectives is written
   * \param followerMergedDirective pointer to CTraj with timestamp
   * \param vehicleState pointer to vehicle state object
   * \param actuatorState pointer to actuator state object
   *
   */
  bool control(AdriveDirective* transmissionDirective, FollowerMergedDirective* mergedDirective,
              SingleFrameVehicleState* vehicleState, ActuatorState* actuatorState, int* drivingDir);

  int getCurrentGear();
  int getCurrentTrajDir();
  bool getCurrentlyShifting ();



};


#endif // SHIFT_CONTROLLER_HH_JGIUGJBBVGDNMBVIVVCHG
