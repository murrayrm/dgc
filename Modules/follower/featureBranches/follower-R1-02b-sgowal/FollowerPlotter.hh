/*!
 * \file FollowerPlotter.hh
 * \brief Class used to show a time plot of follower varibles in a mapviewer.
 *
 * \author Magnus Linderoth
 * \date 13 June 2007
 *
 */

#ifndef FOLLOWER_PLOTTER_HH_VBIPFDLUHFLEHIIUKJBL
#define FOLLOWER_PLOTTER_HH_VBIPFDLUHFLEHIIUKJBL

#include "FollowerLogger.hh"
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>


/*!
 * \class FollowerPlotter
 *
 * Class used to make a time plot of follower 
 * variables in MapViewer.
 */
class FollowerPlotter {
private:
  CMapElementTalker m_meTalker;

  static const int N_VARS = 6;   // Number of variables to plot
  static const int N_SAMPS = 300; // Number of time samples to remember
  int m_index;      // Index of newest time sample
  double m_data[N_VARS+1][N_SAMPS]; // Array with data to plot

public:
  /*! Constructor */
  FollowerPlotter();

  /*! Destructor */
  ~FollowerPlotter();

  /*! 
   * Uses the FollowerLogger object that is being populated anyway.
   * Reads current value of chosen variables and updates plot.
   */
  void updatePlot(FollowerLogger* logger);

};



#endif  //FOLLOWER_PLOTTER_HH_THDYKTFJGHJYTFYUJGYUJ
