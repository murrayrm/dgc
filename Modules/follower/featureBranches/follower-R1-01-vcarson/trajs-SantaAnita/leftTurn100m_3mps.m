% Drives forwards, turns left 90 degrees and continues forward
t = 0.1;     % s
posAcc = 1;  % m/s^2
speed = 3;   % m/s
radius = 100; % m
negAcc = -1; % m/s^2
n = 30; % # points in the curve


a = posAcc*ones(round(speed/posAcc/t), 1);
v = t*cumsum(a);
p = t*cumsum(v);
T1 = [zeros(length(a),3) p v a];
dist1 = p(end);   % m


x = pi/2*[1:n-1]'/n;

T2 = zeros(n-1, 6);
T2(:,1) = radius*(1 - cos(x));
T2(:,4) = dist1 + radius*sin(x);
T2(:,2) = speed*sin(x);
T2(:,5) = speed*cos(x);
T2(:,3) = speed^2/radius*cos(x);
T2(:,6) = -speed^2/radius*sin(x);


a = negAcc*ones(round(-speed/negAcc/t), 1);
v = t*cumsum(a) + speed;
p = t*cumsum(v) + radius;
T3 = [p v a (dist1+radius)*ones(length(a),1) zeros(length(a),2)];


T = [T1; T2; T3];

while T(end,1)==T(end-1,1) && T(end,4)==T(end-1,4)
    T = T(1:end-1,:);
end


save leftTurn100m_3mps.traj T -ASCII