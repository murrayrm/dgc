/*
 * followerDisplay.dd - SparrowHawk display for follower
 *
 * Kristian Soltesz, Magnus Linderoth
 * 17 July 2007
 *
 */

extern long long sparrowhawk;
extern void sparrowQuit(long);
extern void sparrowToggleVisLat(long);

#define D(x)    (sparrowhawk)



%%
SNKEY: %sn_key
--------------------+---------------------+----------------+
Lateral             |Longitudinal (speed) |Shifting        |
--------------------+---------------------+----------------+
error:   %la_err    |error:  %lo_err      |trajDir:  %sh_d |<-State
I-part:  %la_I      |I-part: %lo_I        |Cur gear: %sh_g |
phi:     %la_phi    |vRef:   %lo_vR       |Shifting: %sh_s |
phi FF:  %la_pFF    |vel:    %lo_vel      |                |
l1 GS:   %la_l1G    |a  :    %lo_a        |                |
l2 GS:   %la_l2G    |aFF:    %lo_aFF      |                |
                    |aPitch: %lo_aP       |                |
u:       %la_u      |u:      %lo_u        |                |
--------------------+---------------------+----------------+
%send_steer         |%send_acc            |%send_trans     |<-Directives: %id
--------------------+---------------------+----------------+
l1:      %la_l1     |lV:     %lo_lV       |n/a             |<-Params
l1Scale: %la_l1S    |lI:     %lo_lI       |                |
l2:      %la_l2     |                     |                |
l2Scale: %la_l2S    |pitch:               |                |
l2Pow:   %la_l2P    |gain:   %lo_PG       |                |
lI:      %la_lI     |time:   %lo_PT       |                |
IDist:   %la_IDs    |                     |                |
trFront: %la_tf     |                     |                |
alpha:   %la_alp    |alpha:  %lo_alp      |                |
delay:   %la_del    |delay:  %lo_del      |                |
--------------------+---------------------+----------------+
Abs: %la_rp         |Abs: %lo_rpa         |n/a             |<-Max errors
                    |Rel: %lo_rpr         |                |  before replan
--------------------+---------------------+----------------+
%la_v : 'l'         |n/a                  |n/a             |<-Visualization
--------------------+---------------------+----------------+
%QUIT                                      State: 0x%state
%%

#Misc
int:    %sn_key         D(sn_key)               "%8d";
int:	%QUIT	        D(sparrowQuit)	        "QUIT";
int:    %la_v           D(sparrowToggleVisLat)  "TOGGLE";
int:    %state          D(state)                "%x"; 

#Directive sending to gcdrive
int:    %send_steer     D(send_steering)            "%d";
int:    %send_acc       D(send_acceleration)        "%d";
int:    %send_trans     D(send_transmission)        "%d";
int:    %id             D(directive_id)             "%d";

#Shifting controller state
int:    %sh_d           D(sh_trajDir)           "%2d";
int:    %sh_g           D(sh_gear)              "%2d"; 
int:    %sh_s           D(sh_shifting)          "%d";

#Lateral controller state
double: %la_err         D(la_error)     "%7.3f";
double: %la_I           D(la_I)         "%7.3f";
double: %la_phi         D(la_phi)       "%7.3f";
double: %la_pFF         D(la_phiFF)     "%7.3f";
double: %la_l1G         D(la_l1_gs)     "%7.3f";
double: %la_l2G         D(la_l2_gs)     "%7.3f";
double: %la_u           D(la_u)         "%7.3f";

#Lateral controller params
double: %la_l1          D(la_l1)                "%7.3f";
double: %la_l1S         D(la_l1Scale)           "%7.3f";
double: %la_l2          D(la_l2)                "%7.3f";
double: %la_l2S         D(la_l2Scale)           "%7.3f";
double: %la_l2P         D(la_l2Pow)             "%7.3f";
double: %la_lI          D(la_lI)                "%7.3f";
double: %la_IDs         D(la_IDist)             "%7.3f";
int:    %la_tf          D(la_trackFront)        "%3d";
double: %la_alp         D(la_alpha)             "%7.3f";
double: %la_del         D(la_delay)             "%7.3f";
double: %la_rp          D(la_replanError)       "%7.3f";

#Longitudinal controller state
double: %lo_err         D(lo_error)     "%7.3f";
double: %lo_I           D(lo_I)         "%7.3f";
double: %lo_vR          D(lo_vR)        "%7.3f";
double: %lo_vel         D(lo_vel)       "%7.3f";
double: %lo_a           D(lo_a)         "%7.3f";
double: %lo_aFF         D(lo_aFF)       "%7.3f";
double: %lo_aP          D(lo_aPitch)    "%7.3f";
double: %lo_u           D(lo_u)         "%7.3f";

#Longitudinal controller params
double: %lo_lV          D(lo_lV)                 "%7.3f";
double: %lo_lI          D(lo_lI)                 "%7.3f";
double: %lo_PG          D(lo_pitchGain)          "%7.3f";
double: %lo_PT          D(lo_pitchTime)          "%7.3f";
double: %lo_alp         D(lo_alpha)              "%7.3f";
double: %lo_del         D(lo_delay)              "%7.3f";
double: %lo_rpa         D(lo_replanErrorAbs)     "%7.3f";
double: %lo_rpr         D(lo_replanErrorRel)     "%7.3f";

tblname: followertable;
bufname: followerbuf;

