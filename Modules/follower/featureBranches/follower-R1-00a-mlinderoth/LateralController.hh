/*!
 * \file LateralController.hh
 * \brief Takes care of the lateral lateral part in trajectory following.
 *
 * \author Kristian Soltesz, Magnus Linderoth
 * \date 10 July 2007
 *
 * This file is part of the follower module.
 */

#ifndef LATERAL_CONTROLLER_HH_IRGBFBFWESHIFDLUHAIUHF
#define LATERAL_CONTROLLER_HH_IRGBFBFWESHIFDLUHAIUHF

#include "trajutils/traj.hh"
#include "FollowerUtils.hh"
#include "gcinterfaces/AdriveCommand.hh"

/* 
 * \struct LateralControllerParams
 *
 * This struct holds the parameters of the lateral controller.
 */
struct LateralControllerParams {
  double l1;
  double l1Scale;
  double l2;
  double l2Scale;
  double alpha;
  double delay; // sec
  
  /*Constructor for LateralControllerParmas */
  LateralControllerParams(double param1, double param2, double param3, double param4, double param5, double param6) {
    l1 = param1;
    l1Scale = param2;
    l2 = param3;
    l2Scale = param4;
    alpha = param5;
    delay = param6;
  }
};

/*
 * \class LateralController
 *
 * This is the class in which the lateral controller is implemented.
 */
class LateralController
{
protected:
  /**  The parameters used by the lateral controller */
  LateralControllerParams m_params;

  /* Variable used to remember and keep old steering 
   * commands when reaching end of traj */
  double m_old_phi;
  /* Variable used to remember and keep old steering 
   * commands when velocity on traj is zero */
  double m_old_phiFF;

public:
  /* Constructor for LateralController
   *
   * @ param params lateral controller parameters
   */
  LateralController(LateralControllerParams* params);

  /* Method in which the control signal is calculated 
   *
   * @param adriveDirective pointer to which the resulting AdriveDirective is written
   * @param followerMergedDirective pointer to CTraj with timestamp
   * @param state pointer to vehicle state object
   *
   */
  void control(AdriveDirective* adriveDirective, FollowerMergedDirective* followerMergedDirective,
               SingleFrameVehicleState* state);
};

#endif // LATERAL_CONTROLLER_HH_IRGBFBFWESHIFDLUHAIUHF
