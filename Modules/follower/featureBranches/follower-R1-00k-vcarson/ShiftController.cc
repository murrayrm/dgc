#include "ShiftController.hh"

ShiftController::ShiftController(): 
  m_trajDir(0),
  m_currentGear(0),
  m_lastRealGear(1),
  m_commandGear(0),
  m_currentlyShifting(false)

{
  /* Send state pointers to SparrowHawk display */
  sparrowHawk->rebind("sh_trajDir", &m_trajDir);
  sparrowHawk->set_readonly("sh_trajDir");
  sparrowHawk->rebind("sh_currentGear", &m_currentGear);
  sparrowHawk->set_readonly("sh_currentGear");
  sparrowHawk->rebind("sh_commandGear", &m_commandGear);
  sparrowHawk->set_readonly("sh_commandGear");
  sparrowHawk->rebind("sh_currentlyShifting", &m_currentlyShifting);
  sparrowHawk->set_readonly("sh_currentlyShifting");
}

bool ShiftController::control(FollowerResponse* response, AdriveDirective* transmissionDirective, FollowerMergedDirective* mergedDirective,
                             SingleFrameVehicleState* vehicleState, ActuatorState* actuatorState, int* drivingDirection)
{

  
  /* Update trajectory direction */
  m_trajDir = mergedDirective->traj->getDirection();
  
  /* Update current gear */
  m_currentGear = actuatorState->m_transpos;

  /* Update the lastReal gear. By real is meant either forward or reverse */
  if (m_currentGear == -1 || m_currentGear == 1) {
    m_lastRealGear = m_currentGear;
  }

  /* This is the direction the longitudinal and lateral controllers use. SHOULD NOT == 0 */
  *drivingDirection = m_lastRealGear;
  
  /* Update commanded gear */
  m_commandGear = actuatorState->m_transcmd;

  
  if (m_currentlyShifting) { /* Alice is currently shifting*/
    if (m_currentGear == m_trajDir) { /* Alice just shifted into correct gear, stop shifting */
      m_currentlyShifting = false; 
      return false; 
    } else if (m_commandGear != m_trajDir) { /* Should never occur, but does in simulation */
      transmissionDirective->arg = m_trajDir;
      return true;
    } else { /* Already shifting, don't reissue command */
      return false; 
    }
  } else {
    if (m_currentGear != m_trajDir) { /* Alice is in the wrong gear, start shifting*/
      transmissionDirective->arg = m_trajDir;
      m_currentlyShifting = true;
      return true;
    } else { /* Alice is in the right gear, don't shift */
      return false;
    }
  }
  
  
}

int ShiftController::getCurrentGear() {
  return m_currentGear;
}

int ShiftController::getCurrentTrajDir() {
  return m_trajDir;
}

bool ShiftController::getCurrentlyShifting() {
  return m_currentlyShifting;
}
