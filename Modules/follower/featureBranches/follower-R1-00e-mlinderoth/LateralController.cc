/*!
 * \file LateralController.cc
 * \brief Takes care of the lateral lateral part in trajectory following.
 *
 * \author Kristian Soltesz, Magnus Linderoth
 * \date 10 July 2007
 *
 * This file is part of the follower module.
 */

#include "alice/AliceConstants.h"
#include "LateralController.hh"


/* Constructor for LataralController */
LateralController::LateralController(LateralControllerParams* params, CSparrowHawk* sparrowHawk, FollowerLogger* internalLogger):
  m_params(*params), 
  m_sparrowHawk(sparrowHawk), 
  m_internalLogger(internalLogger),
  m_I(0)
{
  /* Send state pointers to SparrowHawk display */
  m_sparrowHawk->rebind("la_error", &m_lateralError);
  m_sparrowHawk->set_readonly("la_error");
  m_sparrowHawk->rebind("la_I", &m_I);
  m_sparrowHawk->set_readonly("la_I");
  m_sparrowHawk->rebind("la_phi", &m_old_phi);  
  m_sparrowHawk->set_readonly("la_phi");   
  m_sparrowHawk->rebind("la_phiFF", &m_old_phiFF);
  m_sparrowHawk->set_readonly("la_phiFF");
  m_sparrowHawk->rebind("la_l1_gs", &m_l1_gs);
  m_sparrowHawk->set_readonly("la_l1_gs");
  m_sparrowHawk->rebind("la_l2_gs", &m_l2_gs);
  m_sparrowHawk->set_readonly("la_l2_gs");

  /* Send param pointers to SparrowHawk display */
  m_sparrowHawk->rebind("la_l1",&m_params.l1);
  m_sparrowHawk->rebind("la_l1Scale",&m_params.l1Scale);
  m_sparrowHawk->rebind("la_l2",&m_params.l2);
  m_sparrowHawk->rebind("la_l2Scale",&m_params.l2Scale);
  m_sparrowHawk->rebind("la_alpha",&m_params.alpha);
  m_sparrowHawk->rebind("la_delay",&m_params.delay);
}

/* Method for caluclating control signal */
void LateralController::control(AdriveDirective* adriveDirective, 
                                int* directivesToSend, 
                                FollowerMergedDirective* followerMergedDirective,
                                SingleFrameVehicleState* vehicleState,
                                ActuatorState* actuatorState) {

  CTraj* traj = followerMergedDirective->traj; 

  /* Extract traj */
  int dir = traj->getDirection();
  dir = 1; // FIXME: bypass

  /* Calculate feed forward point */
  double ffDist = vehicleState->speed * m_params.delay;

  /* Current vehicle state */
  double xCR = vehicleState->x;
  double yCR = vehicleState->y;
  double thetaC = vehicleState->yaw;
  if (CmdArgs::verbose >= 2) cerr << "Alice vehicle state: x=" << xCR << " y=" << yCR << " yaw=" << thetaC << endl;

  /* Determine the reference and feed forward points on the traj by linear interpolation*/
  int closestIndex;
  double closestFractionToNext;
  TrajPoint refPoint = traj->interpolGetClosest(xCR, yCR, &closestIndex, &closestFractionToNext); 
  TrajPoint ffPoint = traj->interpolGetPointAhead(closestIndex, closestFractionToNext, ffDist, NULL, NULL);

  /* Detect end of trajectroy */
  bool endOfTraj = closestIndex == traj->getNumPoints() - 1;

  /* Error to show in  SparrowHawk display */
  m_lateralError = (-(xCR-refPoint.n)*refPoint.ed + (yCR-refPoint.e)*refPoint.nd) / sqrt(refPoint.nd*refPoint.nd + refPoint.ed*refPoint.ed);
 
  if (CmdArgs::verbose >= 2) cerr << "ClosestIndex: " << closestIndex 
                                 << " ClosestFractionToNext: " << closestFractionToNext << endl;
  
  /* Update integral part */
  if (m_params.lI != 0 && !endOfTraj /*&& actuatorState->m_estoppos==EstopRun*/) {
    m_I += m_lateralError * dir * vehicleState->speed / m_params.lI / CmdArgs::rate;
  }

  /* Gain scheduling */
  double l1_gs = m_params.l1Scale*vehicleState->speed;
  if (l1_gs < m_params.l1) {
    l1_gs = m_params.l1;
  }
  m_l1_gs = l1_gs;

  double l2_gs = m_params.l2Scale * vehicleState->speed*vehicleState->speed / VEHICLE_WHEELBASE / VEHICLE_MAX_LATERAL_ACCEL;
  if (l2_gs < m_params.l2) {
    l2_gs = m_params.l2;
  }
  if (CmdArgs::verbose >= 2) cerr << "l1=" << l1_gs << ", l2=" << l2_gs << endl;
  m_l2_gs = l2_gs;
  
  /* Calcualte feed forward control signal. Avoid division by zero */
  double phiFF;
  if (ffPoint.nd == 0 && ffPoint.ed == 0) {
    /* If feed forward velocity is zero ==> turning radius cannot be calculated, 
     * then use the latest valid feed forward steering */
    phiFF = m_old_phiFF;
  } else {
 phiFF = atan(VEHICLE_WHEELBASE * (ffPoint.nd*ffPoint.edd - ffPoint.ed*ffPoint.ndd) /
              pow(ffPoint.nd*ffPoint.nd + ffPoint.ed*ffPoint.ed, 1.5));   
  }
  m_old_phiFF = phiFF;

  /* Yaw of reference vehicle */ 
  double thetaR = atan2(refPoint.ed, refPoint.nd);

  /* Point to steer towards */
  double xS = refPoint.n + l1_gs*cos(thetaR) + l2_gs*cos(thetaR + m_params.alpha*phiFF);
  double yS = refPoint.e + l1_gs*sin(thetaR) + l2_gs*sin(thetaR + m_params.alpha*phiFF);
  if (CmdArgs::verbose >= 2) cerr << "Steer point: " << xS << ", " << yS << endl;
  
  /* Current position of front wheel axis center.
   * In the case of reverse driving it is the front 
   * wheel position of the mirrored car 
   */
  double xCF = xCR + dir * l1_gs * cos(thetaC);
  double yCF = yCR + dir * l1_gs * sin(thetaC);
  if (CmdArgs::verbose >= 2) cerr << "Front point: " << xCF << ", " << yCF << endl;
  
  /* Steer angle */
  double phi;
  switch (dir) {
  case 1:
    /* Forward */
    phi = atan2(yS-yCF, xS-xCF) - thetaC - m_I;
    break;
  case -1:
    /* Reverse */
    phi = -(atan2(yS-yCF, xS-xCF) - thetaC + M_PI) - m_I;
    break;
  default:
    if(CmdArgs::verbose) cerr << "LateralController::control error: Direction is neither forwards nor backwards" << endl;
    break;
  }

  /* Make sure angle is in interval [-pi, pi[ */
  phi = fmod(phi + 31*M_PI, 2*M_PI) - M_PI;
  if (CmdArgs::verbose >= 2) cerr << "phi: " << phi << "  dir: " << dir << endl;
  
  /* Saturate steer angle */
  if (phi > VEHICLE_MAX_AVG_STEER) {
    phi = VEHICLE_MAX_AVG_STEER;
  } else if (phi < -VEHICLE_MAX_AVG_STEER) {
    phi = -VEHICLE_MAX_AVG_STEER;
  }

  /* Keep old steering if end of traj is reached */
  if (endOfTraj) {
    phi = m_old_phi;
  }

  m_old_phi = phi;

  /* Output steer command to returned AdriveDirective */
  adriveDirective[0].actuator = Steering;
  adriveDirective[0].command = SetPosition;
  adriveDirective[0].arg = phi / VEHICLE_MAX_AVG_STEER;
  *directivesToSend = 1;

  if (CmdArgs::log_internal) {
    /* Varibales */
    m_internalLogger->la_l1_gs = l1_gs;
    m_internalLogger->la_l2_gs = l2_gs;
    m_internalLogger->la_phiFF = phiFF;
    m_internalLogger->la_phi = phi;
    m_internalLogger->la_I = m_I;
    /* Parameters */
    m_internalLogger->la_l1 = m_params.l1;
    m_internalLogger->la_l1Scale = m_params.l1Scale;
    m_internalLogger->la_l2 = m_params.l2;
    m_internalLogger->la_l2Scale = m_params.l2Scale;
    m_internalLogger->la_alpha = m_params.alpha;
    m_internalLogger->la_delay = m_params.delay;
    /* Common for lateral and longitudinal controller */
    m_internalLogger->closestIndex = closestIndex;
    m_internalLogger->closestFractionToNext = closestFractionToNext;
    m_internalLogger->closestN = refPoint.n;
    m_internalLogger->closestNd = refPoint.nd;
    m_internalLogger->closestNdd = refPoint.ndd;
    m_internalLogger->closestE = refPoint.e;
    m_internalLogger->closestEd = refPoint.ed;
    m_internalLogger->closestEdd = refPoint.edd;
  }
}


