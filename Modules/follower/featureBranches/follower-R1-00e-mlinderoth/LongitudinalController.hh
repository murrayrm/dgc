/*!
 * \file LongitudinalController.hh
 * \brief Takes care of the longitudinal part in trajectory following.
 *
 * \author Kristian Soltesz, Magnus Linderoth
 * \date 10 July 2007
 *
 * This file is part of the follower module.
 */

#ifndef LONGITUDINAL_CONTROLLER_HH_HBIFUWEHIFGSEJOIFJS
#define LONGITUDINAL_CONTROLLER_HH_HBIFUWEHIFGSEJOIFJS

#include "FollowerUtils.hh"
#include "gcinterfaces/AdriveCommand.hh"
#include "interfaces/ActuatorState.h"
#include "FollowerLogger.hh"

#define LONGITUDINAL_SPEED_CONTROLLER 0
#define LONGITUDINAL_POSITION_CONTROLLER 1
#define SHIFT_BRAKING -0.7

/*!
 * \struct LongitudinalControllerParams
 *
 * This stuct holds the parameters of the longitudinal controller
 */
struct LongitudinalControllerParams {
  double lV;
  double lI;
  double alpha;
  double delay; // [s]

  double gasDeadU;  // [cmd]
  double gasDeadA;  // [m/s^2]
  double brakeDeadU;  // [cmd]
  double brakeDeadA;  // [m/s^2]

  double gasGain;    // a/u [m/s^2 / cmd]
  double brakeGain;  // a/u [m/s^2 / cmd]

  double gasVelocityGain;  // a/v [m/s^2 / m/s  =  1/s]
  double brakeVelocityGain;  // a/v [m/s^2 / m/s  =  1/s]

  double noGasSpeed;  // [m/s]
  double idleAcceleration;  // [m/s^2]

  /*! Constructors for LongitudinalControllerParams */
  LongitudinalControllerParams()
  {
    LongitudinalControllerParams(0,0,0,0,0,0,0,0,0,0,0,0,0,0);
  }

  /*! Overloaded constructor for LongitudinalControllerParams */
  LongitudinalControllerParams(double v, double i, double a, double d,
                               double gdu, double gda, double bdu, double bda, 
                               double gg, double bg, double gvg, double bvg, 
                               double ngs, double ia) {
    lV = v;
    lI = i;
    alpha = a;
    delay = d;

    gasDeadU = gdu;
    gasDeadA = gda;
    brakeDeadU = bdu;
    brakeDeadA = bda;
    gasGain = gg;
    brakeGain = bg;
    gasVelocityGain = gvg;
    brakeVelocityGain = bvg;
    noGasSpeed = ngs;
    idleAcceleration = ia;
  }
};

/*!
 * \class LongitudinalController
 *
 * This is the class in which the longitudinal controller is implemented
 */
class LongitudinalController
{
protected:
  /*! The parameters used by the longitudinal controller*/
  LongitudinalControllerParams m_params;

  /*! SparrowHawk display is the runtime operator interface*/
  CSparrowHawk* m_sparrowHawk;

 /*! SparrowHawk needs these to be members */
  double m_vErr;
  double m_vR;
  double m_vel;
  double m_a;
  double m_aFF;
  double m_u;
  int m_dir;

  /*! FollowerLogger is used to evaluate performance of follower */
  FollowerLogger* m_internalLogger;
  
  /*! Integral part */
  double m_I;

  /*! Inverse of Alice's drive train non-linearities */
  double linearize(double a, double v, LongitudinalControllerParams* params);


public:
  /*! Constrtuctor for LongitudinalController 
   *
   * \param params longitudinal controller parameters
   * \param sparrowHawk pointer to sparrowHawk (wil be removed)
   * \param m_internalLogger pointer to internalLogger
   */
  LongitudinalController(LongitudinalControllerParams* params, CSparrowHawk* sparrowHawk, 
                         FollowerLogger* m_internalLogger);

 /*! Method in which the control signal is calculated 
  *
  * \param adriveDirective pointer to which the resulting AdriveDirective is written
  * \param followerMergedDirective pointer to CTraj with timestamp
  * \param state pointer to vehicle state object
  *
  */
  void control(AdriveDirective* adriveDirective, 
               int* directivesToSend, FollowerMergedDirective* followerMergedDirective, 
               SingleFrameVehicleState* vehicleState, ActuatorState* actuatorState);
};

#endif // LONGITUDINAL_CONTROLLER_HH_HBIFUWEHIFGSEJOIFJS
