              Release Notes for "follower" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "follower" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "follower" module can be found in
the ChangeLog file.

Release R1-02o (Mon Nov 12  8:38:48 2007):
        * Removed dependence on trajectory age in calculation of delay 
distance.
        * changed engine start time to turn signal directino
        * Changed trajectory timeout from 2 s to 3 s (last line of 
paramfile, paramfileSim), since railplanner sometimes takes more than 2 
s in intersections. Confirmed with Noel.
	* Changed parameters to be less conservative
        * Changed ROA parameters to let the vehicle come closer to obstacles.
        * Changing feed forward gain on longitudinal controller

Release R1-02n (Wed Oct 24 14:28:33 2007):
	Added explicit trajectory timeout. Currently set to two seconds
	both in paramfile and paramfileSim. Before Alice did not always
	stop even when the planner died - this fixes it. It requires ROA to
	be enabled in the paramfile(Sim), which it is by default. It is tested in simulation.

Release R1-02m (Tue Oct 23 12:34:02 2007):
	Forgot to reset parameter, in the previous release. This resulted
	ROA to activate all the time in simulation. This is now fixed.

Release R1-02l (Tue Oct 23 12:15:35 2007):
 	Now ROA draws a magenta cross in mapviewer two meters in front of /
	behind (depending on gear) Alice when ROA is braking. The cross is
	drawn in subgroup -2.

Release R1-02k (Mon Oct 22 11:33:53 2007):
	Modified integral anti-windup of longitudinal controller. This 
should prevent integral part to grow when braking hard. Thought I fixed 
this before, let's hope it works better this time.

Release R1-02j (Sat Oct 20 21:12:43 2007):
	Added log analyzer.m for evaluating log files. Added roa collision
	distance and trigger to the logger.

Release R1-02i (Sat Oct 20 18:21:41 2007):
	Fixed small error in follower's internal logger. (This should not
	affect anybody, except me, getting useful log data.)


Release R1-02h (Sat Oct 20 11:51:29 2007):
        ROA will now stop somewhat further from obstacles (given they are
	seen in time).

Release R1-02g (Sat Oct 20  8:39:46 2007):
	Parameter file after ROA tuning at el Toro tuning 071019. Nominal safety distances have been somewhat increased.

Release R1-02f (Fri Oct 19  1:53:27 2007):
	Added bare mode which allows ROA to go closer to obstacle at low
	speeds. There will probably  be a little "bunny jumping" when
	switching back and forth between bare mode.  

        By default ROA is
	DISABLED in the field parameterfile ('roa-enable-master = 0' in
	paramfile). This will be changed as soon as I have tested the new
	code in the field (currently tested in simulator, i.e. not with ladars).  
        Added and reorganized debugging information in
	sparrowHawk display.

Release R1-02e (Thu Oct 18 22:34:51 2007):
	Changed paramfile according to tests at stluke. Changed
	paramfileSim so that we don't get stuck all the time because of
	ROA.


Release R1-02d (Thu Oct 18 15:03:34 2007):
	Removed the ability to affect longitudinal feed forward from ROA
	(since nowadays ROA affects actuation directly, rather than
	reference velocity).

Release R1-02c (Thu Oct 18 14:42:30 2007):
        Now re-enables feedforward when ROA is overridden by planner (so
	that we actually) move.

Release R1-02b (Thu Oct 18 10:31:29 2007):
        Now possible to override ROA from planner.

Release R1-02a (Tue Oct 16 22:36:17 2007):
        Changed so that ROA commands the brake rather than the reference velocity. The longitudinal integral is turned off when ROA is braking. Added debug information to sparrow (mainly for tomorrow's test).
 

Release R1-02 (Tue Oct 16 14:38:03 2007):
        New parameterfile (paramfileSim) fixes issue with ROA triggering when backing up in the simulator.

Release R1-01z (Mon Oct 15 22:12:19 2007):
        ROA now fails the current trajectory to planner when triggered. (I dont think planner handles the failure yet). The field changes from the last el Toro test have been implemented (in a less hacky fashion).


Release R1-01y (Sun Oct 14  0:58:54 2007):
	recompile with latest gcmodule

Release R1-01x (Sat Oct 13 22:29:28 2007):
        ROA revamped.

Release R1-01w (Wed Oct  3 16:51:28 2007):
	This is most probably the release that will be run on el Toro 2007-10-03. A lot of "fix me" comments have been resolved. delay handling has moved into ROA (but is still pretty basic).

Release R1-01v (Tue Oct  2 23:41:13 2007):
        This is release of a version of follower containing ROA which was successfully tested at St Luke 2007-10-02. The parameter --trajReceiveTimeout is deprecated from now onwards, since delays are handled in ROA. Right now, it brakes if planner has not produced a trajectory for > 2 s. This is currently hardcoded, but will be parameterized anew.

        (The last release apparently did not make it to the trunk. This is causing some trouble.)

Release R1-01u (Sun Sep 30  8:18:13 2007):
        It is now possible to enable/disable the different components of the ROA during runtime from the paramfile. The current ROA state is shown in sparrow. The ladar code has been rewritten, and works in simulation. I need to verify in Alice that coordinate transformations and distance calculations are correct. 
 

Release R1-01t (Sun Sep 30  0:39:20 2007):
        The skynet interface now works. There is still a lot to fix:
        * Verify that the functions for calculating estimated time to collision work.
        * Tune heuristic functions determining how reference speed should be limited.
        * Not care about obstacles in front of us when we are reversing away from them.
        * Use the rear ladar when backing up. 
        
Release R1-01s (Sat Sep 29 21:48:21 2007):
        Do not use this version! It has a rewritten ladar 'perceptor' which hopefully works. Will try it tonight on Alice and make a new *propper* release of follower later tonight.

Release R1-01r (Sat Sep 29 15:34:53 2007):
        Repaired the code so that follower brakes when planner delays are large. The trajectory timeout notion is obsolete, and replaced by the delay monitor inthe ROA. Added debug information (sparrow, print console and logging to file) to ROA for field testing. Added further ROA parameters to the paramfile. 

Release R1-01q (Thu Sep 27 17:32:19 2007):
        Added riscZoneHalfWidth parameter for ROA. Found and fixed two bugs in the ladar related part of the ROA code.

Release R1-01p (Thu Sep 27 14:57:43 2007):
        The way follower handles transmission has been completely rewritten. Works in simulation - hopefully also in the field.


Release R1-01o (Thu Sep 27 11:36:46 2007):
        Fixed transmission bug in follower (sorry for this one). Streamlined ROA code. 

Release R1-01n (Thu Sep 27  1:15:08 2007):
        The Reactive Obstacle Avoidance (ROA) module now limits reference velocities as a linear function of system (planner and gcdrive) delays. Three parameters have been added to the follower parameter file. The path of this file is displayed in follower's SparrowHawk display. This file can be edited and saved while follower is running. After saving, focus on follower's SparrowHawk display and hit 'u'. This loads the new parameters (verify that the update counter is incremented). 'roa-enable' [1,0] turns ROA on/off. 'roa-minDelay' is the allowed average system delay before ROA limits reference velocity (corresponding to 'ROA factor' 0 in follower's SparrowHawk display). For average delays >= 'roa-maxDelay' ('ROA factor 1') reference velocity is set to 0. When ROA limits reference velocity the string 'ACTIVE' shows up next to 'ROA factor' in the SparrowHawk display.

        Warning: The default ROA parameter values are OK for simulation but might cause interesting (gas/brake) behaviour in Alice. Try increasing them or turning ROA off if this happens.


Release R1-01m (Wed Sep 26 16:31:01 2007):
        The sensnet connection works and all the code runs in the loop without major issues.

Release R1-01l (Mon Sep 24 23:58:11 2007):
First cut of Reactive Obstacle Avoidance (ROA) class has been added. It obtains delay information, logs it to file and displays it in sparrowHawk display. 

* The delay information might need filtering. 
* All delay information is not yet displayed in sparrow.
* SparrowHawk output is badly formatted. 
* Need to verify that the logging works.

ROA module also obtains ladar blobs but does not yet process them.


Release R1-01k (Wed Sep 19 20:18:36 2007):
	Enabled feed forward from vehicle pitch in longitudinal 
	controller.

Release R1-01j (Wed Sep 19  7:27:24 2007):
	Fixed bug in how lateral integral is updated.

	Modified how follower handles coming into a region with zero 
reference velocity. Should make stops smoother, but you never know until 
you sit in the back of Alice and feel it.

Release R1-01i (Wed Sep 12  0:28:00 2007):
Tuned lateral integral.

Release R1-01h (Mon Sep 10 10:19:18 2007)
Now the output file from the internal logger shows which estop state follower was in when a given log-line was written.

Release R1-01g (Mon Sep  3 21:31:49 2007):
Changed transmission controller so that follower does no longer get stuck in 'transmission pending' state. This happened several times in the field, upon resuming after Estop pause. 

Release R1-01f (Mon Sep  3 16:31:58 2007):
Now trajReceiveTimout can be changed during runtime. (I thought this was the case also before, but it was not). Other than that, only minor changes. 

Release R1-01e (Thu Aug 23 14:44:23 2007):
Removed max limit on longitudinal integral. The max limit would had stopped us completely at a curb, steep hill, etc. 

Integral incrementation is, however, much slower, when the integral is above the old max limit. (Decrementation is equally fast, on either side of the old max limit).

Note: This is better than before, but relys too much on what the velocity planner outputs. Robustness with respect to this  is currently being developed.

Release R1-01d (Thu Aug 23 13:18:59 2007):
Added stopSpeed parameter. If speed reference is below stopSpeed, we bring Alice to a stop. (This functionality was previously hardcoded.)

Now disabling longitudinal integrator updates while reference speed is below stopSpeed.

Fixed bug causing follower to sometimes not recover from planner going down. Since this behavior has only been observed in Alice - never in simulation - I cannot be 100% confident I really fixed the bug.   


Release R1-01c (Wed Aug 22 11:57:28 2007):
Now requests replans from current position when Estopped. Updated the sparrow to show replan requests.


Release R1-01b (Tue Aug 21 16:37:16 2007):
If gcdrive is killed and restarted, follower will not 'hang', as happened sometimes before (due to the way responses were handled). This has been verified in simulation, but not yet on Alice.

Fault handling has been reviewed and properly tested in simulation. Some minor improvements have been made, to make it more robust.

The sparrowHawk display has been revamped to become (more) user friendly. You can now see to which actuators we are sending directives, and what responses we get. Trajectory related faults are now displayed as strings in the upper right corner of the display.

The code of the main program has been divided into functions, to make it more readable.

Release R1-01a (Fri Aug 17  0:33:17 2007):
Removed controller parameters from the SparrowHawk display, and added functionality which instead allows the controller parameters to be updated from file during runtime.

The path of the  file from which the parameters are read when updating is shown in the sparrowHawk dispay. 

It is important that *all* controller parameters are defined in this file. 

Updating can be done either by pressing 'U' or by hitting the update button in the sparrowHawk display.

Release R1-01 (Tue Aug 14 22:37:41 2007):
	Changed paramters. Changed gain schedule for lateral integral.

Release R1-00z (Tue Aug 14  0:44:13 2007):
	Added possibility to show a time plot of follower variables in a 
	mapviewer with receive subgroup -16

Release R1-00y (Mon Aug 13  6:15:17 2007):
	Added integral limitation on longitudinal controller.

	Extended options for gain scheduling of lateral controller.

	Added feed forward from pitch to longitudinal controller.
	This adds a term to the control signal to compensate for how
	the acceleration is affected by driving on a slope.

	Added variables and increased resolution on sparrow display.

Release R1-00x (Sun Aug 12 21:24:43 2007):
Mostly minor fixes, not affecting overall performance.

Release R1-00w (Sun Aug 12 13:09:39 2007):
Resolved fault handling bugs found in simulation.

Release R1-00v (Fri Aug 10 17:47:11 2007):
First implementation with complete CSS structure, stable enough for the field. Anti integrator windup is implemented and tested. Handling of actuator failures is partly tested.

Release R1-00u (Fri Aug 10 12:22:49 2007):
CSS fault handling is all in place now. 

This release is stable, but should not be concidered safe, since integral antiwindup and other special case handling has not been tested since the program structure changed.

A new release, with verified fault handling functionality will be availible today, or latest this weekend.


Release R1-00t (Wed Aug  8 18:30:04 2007):
Eliminated the segfault occuring in the previous release. Went through the code and cleaned up bugs.
Improved comments.

Release R1-00s (Tue Aug  7  8:29:36 2007):
The first stable release after merging with the CSS code Vanessa wrote. There is still a little of
CSS stuff to be done, but most is in place.

Apart from CSS, a major code restructuring and clean has been done. 

Currently follower segfaults and dumps core when you run it from process control and try to quit it by pressing q or 
hitting the QUIT button in its sparrowHawk display. (However, there seems to be no segfault when closing follower
down from process control, or when closing process control while follower is running.)

Do not turn on lateral integration using this release.

Release R1-00r (Mon Aug  6 23:02:19 2007):
When limiting max lateral acceleration in the previous release, I forgot the 
'a' in front of 'tan'. The correct way to do this is:

|steerAngle| < atan(ALICE_WHEELBASE * MAX_LATERAL_ACCELERATION / currentSpeed^2)

Also, there is a test that currentSpeed > verySmallSpeed, to avoid div by 0 errors.


Release R1-00q (Mon Aug  6 22:00:19 2007):
Limited max lateral acceleration, by limiting steer angle so that
|steerAngle| < tan(ALICE_WHEELBASE * MAX_LATERAL_ACCELERATION / currentSpeed^2)

Modified followerDisplay. For some reason process control only displays the last 24 rows of my table. Robbie is working on this. I believe it affects all modules using sparrow(Hawk). 

Changed longitudinal feed forward parameter. Longitudinal feed forward is now fully used.


Release R1-00p (Mon Aug  6 19:41:12 2007):
No longer assumes Alice is in gear 1 when follower starts. Instead the gear at startup is read from actuatorstate. There could still be a problem if Alice is shifting gears while the constructor of follower is invoked. 

Release R1-00o (Mon Aug  6 12:15:21 2007):
Now the follower does not steer if Estop paused && vehicle stopped.

Release R1-00n (Mon Aug  6  0:39:51 2007):
The integrals for longitudinal and lateral control are reset when EstopPos is not EstopRun. They are also reset, when trajectory receiving from planner times out. 

The way it is done, is by reading directly from ActuatorState. I will move over to use the SF towards gcdrive for obtaining Estop info. (This, however, requires information about Estop state as reason to the reject-responses sent by gcdrive.)

In order to yam save, I had to update my link modules. After this, yam auto-merged my code (since I was not working on the latest release of the follower module). Unfortunately, it was no longer possible to simulate using process-control after the link update. This means I have not been able to verify that my code merged correctly. 

Release R1-00m (Sat Aug  4  1:42:05 2007):
	Changed default value of parameter lo_alpha from 1.0 to 0.1. This means that feed forward acceleration is multiplied by 0.1 before 
application.

Release R1-00l (Thu Aug  2 23:59:38 2007):
        Shifting gears is now done the propper CSS way. Follower pauses if it does not receive any trajectories, and unpauses if it starts receiving trajectories anew. Code for a North face is in place, but so far commented away. It compiled on another branch, but still have not been able to reproduce that behavior here.  

Release R1-00k (Tue Jul 31 15:12:42 2007):
        Removed compiler warnings. Added some minor things needed for NF / SF.

Release R1-00j (Mon Jul 30 19:54:32 2007):
        Got rid of bug, causing Alice to freeze in simulation.

Release R1-00i (Fri Jul 27 14:46:24 2007):
        Added option to follow front wheel trajectory (as opposed to rear wheel). Reversing (and U turn) works in simulation. Added option to run on feed forward only (as opposed to feed forward + feedback). Restructured the way in which commands are sent to gcdrive.

Release R1-00h (Thu Jul 26 12:18:44 2007):
        Added an option to visualize the operation of the lateral controller in mapviewer. Plus some minor improvements.

Release R1-00g (Tue Jul 24 23:00:49 2007):
        Changed from isA to hasA CTrajTalker. Let traj communication tread exit cleanly. Hopefully, the sparrow thread also exits cleanly (at least no core dumps so far). Cleaned up some code and added some comments. Started preparing for proper CSS NF and SF usage.

Release R1-00f (Tue Jul 24  2:19:58 2007):
	Added automatic shifting, integral anti-windup and minor fixes.

Release R1-00e (Thu Jul 19 23:53:43 2007):
	Release for friday test 2007-07-20.

Release R1-00d (Wed Jul 18  5:53:18 2007):
	Added logging capabilities, Sparrow interface and some small improvements.

Release R1-00c (Mon Jul 16  12:40:10 2007):
        Release with controllers working in simulation, and on Alice. Not yet tested together with the entire planning stack. Some features not yet implemented.

Release R1-00a (Wed Jun 27 14:07:30 2007):
        Added programs:
        * adriveloggen (generates skynet log file containing AdriveDirectives)
        * skynetlogparser (parses skynet log file and writes to file which Matlb can read)

        FIXME: use gengetopt for command line parsing, clean up code.
        

Release R1-00 (Thu Jun 21 17:54:31 2007):
	Created.









































































