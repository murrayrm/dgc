///
/// \file FollowerUtils.cc
/// \brief Classes used by follower
///
/// \author Kristian Soltesz, Magnus Linderoth
/// \date 18 July 2007
///
/// This file holds resources shared within the follower module.

#include "FollowerUtils.hh"
#include <string>

// Declare sparrowHawk
CSparrowHawk* sparrowHawk = &SparrowHawk();

// Declar exit flag
bool ExitHandler::quit = false;

// Declare printStream
stringstream printStream;

void printMessage() {
  if (CmdArgs::disable_console) {
    cout << printStream.str();  // Print to terminal if there is no SparrowHawk display
  } else {
    SparrowHawk().log(printStream.str()); // Print to SparrowHawk log if it is displayed
  }
  printStream.str("");
}

// instantiation shared ommand line arguments
int CmdArgs::sn_key = 0;
int CmdArgs::rate = 10;
int CmdArgs::log_level = 0;
char* CmdArgs::log_path = NULL;
char* CmdArgs::frame = NULL;
bool CmdArgs::ffOnly = false;
char* CmdArgs::traj_file = NULL;
int CmdArgs::verbose = 0;
bool CmdArgs::disable_console = false;
bool CmdArgs::log_internal = false;
int CmdArgs::gear = 1; // This row will be removed in final version
bool CmdArgs::vis_lat_control = false;
unsigned long long CmdArgs::trajReceiveTimeout = 0;
char* CmdArgs::paramfile = NULL;

CMapElementTalker meTalker;
