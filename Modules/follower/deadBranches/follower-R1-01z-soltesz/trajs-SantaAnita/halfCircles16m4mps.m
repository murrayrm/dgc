N = 4;
n = 100;
R = 16;
v = 4;


T = zeros(N*n,6);

% pos
T(:,4) = -R * sin(pi*(1:N*n)/n);
for i = 1:N
    T(((i-1)*n+1 : i*n), 1) = R*(-1 + 2*i - cos(pi*(1:n)/n));
end

% speed
T(:,5) = -v * cos(pi*(1:N*n)/n);
for i = 1:N
    T(((i-1)*n+1 : i*n), 2) = v*sin(pi*(1:n)/n);
end

% acc
T(:,6) = v^2 / R * sin(pi*(1:N*n)/n);
for i = 1:N
    T(((i-1)*n+1 : i*n), 3) = v^2/R*cos(pi*(1:n)/n);
end



save halfCircles16m4mps.traj T -ASCII