///
/// \file TransmissionController.cc
/// \brief Takes care of the gear shifting part in trajectory following.
///
/// \author Kristian Soltesz
/// \date 2 August 2007
///
/// This file is part of the follower module.

#include "TransmissionController.hh"

TransmissionController::TransmissionController():
  m_gear(1),
  m_trajectoryDirection(1),
  m_transmissionPending(false)
{
  sparrowHawk->rebind("tr_gear", &m_gear);
  sparrowHawk->set_readonly("tr_gear");
  sparrowHawk->rebind("tr_direction", &m_trajectoryDirection);
  sparrowHawk->rebind("tr_pending", &m_transmissionPending);
}

bool TransmissionController::control(FollowerControlStatus* controlStatus, ROA* roa, AdriveDirective* transmissionDirective, CTraj* traj, int currentGear)
{
  m_transmissionPending = controlStatus->state & FollowerState::TransmissionPending; 
  int trajectoryDirection = traj->getDirection();

  // Safety check
  if (currentGear == 1 || currentGear == -1) {
    m_gear = currentGear;
    roa->updateGear(m_gear);
  } else {
    m_gear = 1; 
  }

  // Safety check
  if (trajectoryDirection == 1 || trajectoryDirection == -1) {
    m_trajectoryDirection = trajectoryDirection;
  }

  if (m_gear != trajectoryDirection) {
    controlStatus->state |= FollowerState::TransmissionPending;
    transmissionDirective->arg = trajectoryDirection;
    return true;
  } else {
    controlStatus->state &= ~(FollowerState::TransmissionPending);
    return false;
  }
}
 
/* 
void TransmissionController::initialize(int gear)
{
  // This is the real initialization
  m_gear = gear;
}
*/
