///
/// \file FollowerMain.cc
/// \brief Main file of trajectory following program using closed loop controllers.
///
/// \author Nok Wongpiromsarn, Kristian Soltesz, Magnus Linderoth
/// \date 10 July 2007
///
/// Follower is the program ensuring that Alice follows trajectories 
/// generated by the planner. It is a replacement for the old 
/// trajfollower program, rewritten to be compatible with the 
/// canonical software architecture.
///
/// This file is part of the follower module.

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <skynet/sn_msg.hh>
#include "Follower.hh"


using namespace std;

int main(int argc, char **argv) 
{
  // Set up SparrowHawk to display the followertable 
  // This needs to be done before the first invokation
  // of printConsole().
  // 
  sparrowHawk->add_page(followertable, "follower");
  
  // In the below blocks of code, command line arguments 
  // are read. There are two sources for command line arguments:
  // the actual command line and a file, refered to as 'paranfuke'
  // since it also defines the parameters to be used by the controllers

  //Read command line arguments from actual command line
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
  
  //Read command line arguments from paramfile
  cmdline.paramfile_arg = dgcFindConfigFile(cmdline.paramfile_arg, "follower");

  // FIXME: print error message and exit if paramfile not found


  // FIXME: needed here?
  //if (cmdline.paramfile_given && cmdline_parser_configfile(cmdline.paramfile_arg, &cmdline, 0, 0, 1) != 0) {
  //  exit(1);
  //}

  // The command line arguments, which are not controller parameters 
  // are written to a struct, accessible to all who include FollowerUtils.hh.
  // (In addition to this, each controller has a parameter struct, where its 
  // parameters are stored.)
  CmdArgs::rate = cmdline.rate_arg;
  CmdArgs::traj_file = cmdline.traj_file_arg;
  CmdArgs::log_level = cmdline.log_level_arg;
  CmdArgs::log_path = cmdline.log_path_arg;
  CmdArgs::frame = cmdline.frame_arg;
  CmdArgs::ffOnly = cmdline.ff_only_given;
  CmdArgs::verbose = cmdline.verbose_arg;
  CmdArgs::disable_console = cmdline.disable_console_given;
  CmdArgs::log_internal = cmdline.log_internal_given;
  CmdArgs::gear = cmdline.gear_arg; //FIXME: This row will be removed in final version
  CmdArgs::vis_lat_control = cmdline.vis_lat_control_given;
  CmdArgs::paramfile = cmdline.paramfile_arg;

  // Figure out what skynet key to use
  CmdArgs::sn_key = skynet_findkey(argc, argv);
  if (CmdArgs::verbose >= 1) printConsole("Constructing skynet with KEY = " << CmdArgs::sn_key << endl); 
  
  // The skynet key is sent to SparrowHawk, 
  // which displays it during execution 
  sparrowHawk->rebind("sn_key", &CmdArgs::sn_key);
  sparrowHawk->set_readonly("sn_key");

  // Initialize CMapElementTalker. 
  // The talker is used to send visual elements to the map.
  // These elements are displayed during runtime and used
  // for testing and debugging purposes.
  meTalker.initSendMapElement(CmdArgs::sn_key);

  // Determine what shall be logged
  if(cmdline.log_level_arg > 0)
  {
    // Check if the logs directory exists.
    // If it does not exist, make sure it is created. 
    struct stat st;
    stat(cmdline.log_path_arg, &st);
    if (errno == ENOENT) {
      if(mkdir(cmdline.log_path_arg, 0755) != 0)
	if (CmdArgs::verbose) printConsole(__FILE__ << ":" << __LINE__ 
                                           << "FollowerMain error: cannot create log dir" << endl);
    }
  }

  /// Control status sent from control to arbiter
  //FIXME: better to declare pointer for concistency? 
  FollowerControlStatus controlStatus;
  controlStatus.state = FollowerState::Running;


  /// Merged directive sent from arbiter to control
  static FollowerMergedDirective mergedDirective;

  /// Follower object
  Follower follower(&controlStatus, &mergedDirective);

  /// Time stamps used to analyze internal delays
  unsigned long long timestamp1, timestamp2, timestamp3, timestamp4;
  const long SLEEP_TIME = SECOND/cmdline.rate_arg;  

  // For test purposes, follower can read a trajectory
  // from a file (as opposed to receiving trajectories from 
  // the planner). If the command line option for reading
  // a trajectory from file is not set, a thread for reading
  // trajectories from the planner is started. Else, the program tries to
  // read a trajectory from the location indicated by the corresponding 
  // command line argument.
  if (CmdArgs::traj_file == NULL) {
    DGCstartMemberFunctionThread(&follower, &Follower::CommThread);
  } else {
    mergedDirective.id = 0;
    try {
      mergedDirective.traj = new CTraj(3,CmdArgs::traj_file);
      if (CmdArgs::verbose >= 1) printConsole("Traj file: " << CmdArgs::traj_file << "   Trajpointer: " 
                                              << mergedDirective.traj << "Traj length: " 
                                              << mergedDirective.traj->getLength() << endl);

      follower.broadcastTraj(mergedDirective.traj);
    } catch ( ...) {
      if (CmdArgs::verbose) printConsole("FollowerMain error: Could not load the specified CTraj file" << endl);
      exit(1);
    }
    mergedDirective.traj->setDirection(CmdArgs::gear);
  }


  unsigned long followerPeriod = 0;
  sparrowHawk->rebind("follower_period", (int*)(&followerPeriod));
  sparrowHawk->set_readonly("follower_period");

  // Main loop from which the arbitrate and 
  // control functions are invoked.
  while(!ExitHandler::quit) {
    // A timer is used to make sure our control frequency 
    // is what we set (not affected by how long it takes 
    // to compute each cycle). In addition to this, 
    // time stamps are set, telling how long time the
    // arbitration and control functions take, respectively.
    
    DGCgettime(timestamp1);
    
    // The arbitrate function is invoked. It performs
    // operations necessary to figure out the merged 
    // directive for this control cycle.
    follower.arbitrate(&controlStatus, &mergedDirective);

    // The control function is invoked. It performs 
    // operations to make the vehicle comply with the 
    // merged directive
    DGCgettime(timestamp3);
    follower.control(&controlStatus, &mergedDirective);
    DGCgettime(timestamp4);

    // Time checkpoint at the end of the cycle
    DGCgettime(timestamp2);

    // Now sleep for SLEEP_TIME usec less the amount of time the
    // computation took 
    int delaytime = SLEEP_TIME - (timestamp2 - timestamp1);

    if(delaytime > 0) {
      DGCusleep(delaytime);
    }

    // Here it is possible to print the computation time
    // of one follower cycle. The time used by the control
    // function is printed separately.
    if (CmdArgs::verbose >= 2) printConsole("Computational delay: " << (timestamp2 - timestamp1) 
                                            << " us, of which " << (timestamp4 - timestamp3) 
                                            << "us in follower.control()" << endl);
 
    followerPeriod = DGCgettime()-timestamp1;
  }

  if (CmdArgs::verbose) printConsole("Exiting follower..." << endl);

  sparrowHawk->stop();
  
  // Give trajectory receiving thread time to exit.
  DGCusleep(SECOND/10); //FIXME: magic number

  return 0;
}



