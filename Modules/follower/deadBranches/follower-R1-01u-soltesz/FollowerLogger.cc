/*!
 * \file FollowerLogger.cc
 * \brief Class used to log the internal state of follower.
 *
 * \author Magnus Linderoth
 * \date 16 July 2007
 *
 * Variables that are to be logged are written to an instance of this class.
 * writeLog() is called periodically to write the data to the log file.
 */

#include <time.h>
#include "FollowerLogger.hh"
#include "FollowerUtils.hh"
#include <string.h>
#include <iostream>

/* Constructor */
FollowerLogger::FollowerLogger()
{
  // Test whether to log or not
  if (CmdArgs::log_internal) {
    // Create log file name
    char filePath[8192];  // That sould do for most file names
    char dateString[4096];
    char sourcePath[4096];
    char* sourceName;

    // Create date string
    time_t timer = time(NULL);
    strftime(dateString, sizeof(dateString), "%Y-%m-%d-%a-%H-%M", localtime(&timer));

    // If Follower is run with a trajectory from a file,
    // include the name of the traj-file in the name of
    // the log file to simplify analysis
    if (CmdArgs::traj_file == NULL) {
      // Trajectories come from Skynet, no extra
      // description in the log file name as of now.
      sourcePath[0] = '\0';
      sourceName = sourcePath;
    } else {
      // Get source file name without path
      strcpy(sourcePath, CmdArgs::traj_file);
      sourceName = strrchr(sourcePath, '/');
      if (sourceName == NULL) {
        sourceName = sourcePath;
      } else {
        sourceName++;
      }
    }

    // Generate file name
    sprintf(filePath, "%s/followerInternalLog-%s.%s.log", CmdArgs::log_path, sourceName, dateString);

    // Open file
    m_file = fopen(filePath, "w");
    if (m_file == NULL) {
      if (CmdArgs::verbose) printConsole("Could not open log file " << filePath << endl);
      exit(1);
    }
  }
}

/* Destructor */
FollowerLogger::~FollowerLogger()
{
  if (CmdArgs::log_internal) {
    fclose(m_file);
  }
}

/* Write current values of logged variables to log file */
void FollowerLogger::writeLog()
{
  fprintf(m_file, "%llu\t"
          "%f\t%f\t%f\t%f\t%f\t"
          "%f\t%f\t%f\t%f\t%f\t%f\t"
          "%d\t%d\t%f\t"
          "%f\t%f\t%f\t%f\t%f\t%f\t"
          "%f\t%f\t%f\t%f\t%f\t%f\t"
          "%f\t%f\t%f\t%f\t%f\t%f\t"
          "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t"
          "%f",
          // time
          timeStamp,  // 1

          // Lateral variables
          la_l1_gs,  // 2
          la_l2_gs,
          la_phiFF,
          la_phi,
          la_I,

          // Longitudinal variables
          lo_vRef,  // 7
          lo_aFF,
          lo_speed,
          lo_I,
          lo_a,
          lo_u,

          // Trajectory info
          trajID,  // 13
          closestIndex,
          closestFractionToNext,

          // Vehicle state
          curN,  // 16
          curNd,
          curE,
          curEd,
          curYaw,
          curYawRate,

          // Data for closest point on  trajectory
          closestN,  // 22
          closestNd,
          closestNdd,
          closestE,
          closestEd,
          closestEdd,

          // Lateral parameters
          la_l1,  // 28
          la_l1Scale,
          la_l2,
          la_l2Scale,
          la_alpha,
          la_delay,

          // Longitudinal parameters
          lo_lV,  // 34
          lo_lI,
          lo_alpha,
          lo_delay,
          lo_gasDeadU,
          lo_gasDeadA,
          lo_brakeDeadU,
          lo_brakeDeadA,
          lo_gasGain,
          lo_brakeGain,
          lo_gasVelocityGain,
          lo_brakeVelocityGain,
          lo_noGasSpeed,
          lo_idleAcceleration,

          // Estop state
          Estop);  // 48


  // ROA logging:

  // Enable
  fprintf(m_file, "\t%d\t%d\t%d\t%d", 
          roa_enableMaster,
          roa_enableDelay,
          roa_enableLateralError,
          roa_enableObstacle);

  // Ages
  for (int i = 0; i < 6 ; i++) {
    fprintf(m_file, "\t%f", roa_ages[i]);
  }

  // newline
  fprintf(m_file, "\n");


}
