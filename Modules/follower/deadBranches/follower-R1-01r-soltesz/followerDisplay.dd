/*
 * followerDisplay.dd - SparrowHawk display for follower
 *
 * Kristian Soltesz, Magnus Linderoth
 * 17 July 2007
 *
 */

extern long long sparrowhawk; 
extern void sparrowQuit(long);
extern void updateControllerParams(long);
extern void sparrowToggleVisLat(long);

#define D(x)    (sparrowhawk)



%%
SNKEY: %sn_key    
--------------------+---------------------+----------------+---------------------
Lateral (steering)  |Longitudinal (speed) |Transmission    |Status
--------------------+---------------------+----------------+---------------------
error:   %la_err    |error:  %lo_err      |trajDir:  %tr_d | Traj ID:      %traj_id
I-part:  %la_I      |I-part: %lo_I        |Cur gear: %tr_g | Traj age:     %traj_age
phi:     %la_phi    |vRef:   %lo_vR       |Pending:  %tr_p | astate age:   %astate_age
phi FF:  %la_pFF    |vel:    %lo_vel      |                | %trajectoryTimeout            
l1 GS:   %la_l1G    |a  :    %lo_a        |                | %trajectoryError                         
l2 GS:   %la_l2G    |aFF:    %lo_aFF      |                | %trajectoryEnd
                    |aPitch: %lo_aP       |                | %EstopPause                           
u:       %la_u      |u:      %lo_u        |                | %EstopDisable                                  
%la_replan          |%lo_replan           |                |<-Replan
--------------------+---------------------+----------------+
%dir_steer          |%dir_acc             |%dir_trans      |<-Directives
%resp_steer         |%resp_acc            |%resp_trans     |<-Responses
%send_steer         |%send_acc            |%send_trans     |<-Sending
%steer_loop_age     |%acc_loop_age        |                |<-Loop age
%steer_state_age    |%acc_state_age       |                |<-State age
--------------------+---------------------+----------------+ 
                    |Gas:     |Brake:     |                |
%rejSteer           |%rejGas  |%rejBrake  |%rejTrans       |<-Rejected      
%failSteer          |%failGas |%failBrake |%failTrans      |<-Failed
--------------------+---------------------+----------------+
%la_v : 'l'         |n/a                  |n/a             |<-Visualization
--------------------+---------------------+----------------+---------------------
ROA %roa_enable                  (The stuff below is for debugging ROA)
-------------------------------------+-------------------------------------------
lateral error:  %roa_lateral_error   |delay  factor: %roa_df
speed:          %roa_speed           |error  factor: %roa_ef 
steer angle:    %roa_steer_angle     |obst.  factor: %roa_of          
time to coll:   %roa_ttc   <-WRONG   |master factor: %roa_mf              
delay:          %roa_delay           |         

%u %upd    s of parameters from file: 
%paramfile


%QUIT        %update_params 
%%


#Misc
int:    %sn_key         D(sn_key)                               "%8d";
string: %paramfile      D(paramfile)                            "%s"; 
int:    %u              D(param_id)                             "%2d";
int:	%QUIT	        D(sparrowQuit)	                        "(q)uit";
int:    %upd              D(updateControllerParams)             "(u)pdate";        
int:    %la_v           D(sparrowToggleVisLat)                  "TOGGLE";
int:    %traj_id        D(trajectory_counter)                   "%d";

#Trajectory Faults
string: %trajectoryTimeout      D(trajectoryTimeout)            "%s";
string: %trajectoryError        D(trajectoryError)              "%s";
string: %trajectoryEnd          D(trajectoryEnd)                "%s";

#Estop Status
string: %EstopPause             D(estopPause)                   "%s";
string: %EstopDisable           D(estopDisable)                 "%s";

#Currently sending directives?
int: %send_steer                D(send_steering)                "%d";
int: %send_acc                  D(send_acceleration)            "%d";
int: %send_trans                D(send_transmission)            "%d";

#Directives sent to gcdrive
int: %dir_steer                 D(steering_directive)           "%d";
int: %dir_acc                   D(acceleration_directive)       "%d";
int: %dir_trans                 D(transmission_directive)       "%d";

#Responses received from gcdrive 
int: %resp_steer                D(steering_response)            "%d";
int: %resp_acc                  D(acceleration_response)        "%d";
int: %resp_trans                D(transmission_response)        "%d";

#Rejected Responses
string: %rejSteer               D(steering_rejected)            "%s";
string: %rejGas                 D(gas_rejected)                 "%s";
string: %rejBrake               D(brake_rejected)               "%s";
string: %rejTrans               D(transmission_rejected)        "%s";

#Failure Responses
string: %failSteer              D(steering_failure)             "%s";
string: %failGas                D(gas_failure)                  "%s";
string: %failBrake              D(brake_failure)                "%s";
string: %failTrans              D(transmission_failure)         "%s";

#Transmission controller state
int:    %tr_d                   D(tr_direction)                 "%2d";
int:    %tr_g                   D(tr_gear)                      "%2d"; 
int:    %tr_p                   D(tr_pending)                   "%d";

#Loop ages
int:    %steer_loop_age         D(steering_loop_age)            "%10d";
int:    %acc_loop_age           D(acceleration_loop_age)        "%10d";

#State ages
int:    %steer_state_age        D(steering_state_age)           "%10d";
int:    %acc_state_age          D(acceleration_state_age)       "%10d";

#Other ages
int:    %traj_age               D(trajectory_age)               "%d";
int:    %astate_age             D(vehicle_state_age)            "%d";        

#Lateral controller state
double: %la_err                 D(la_error)                     "%7.3f";
double: %la_I                   D(la_I)                         "%7.3f";
double: %la_phi                 D(la_phi)                       "%7.3f";
double: %la_pFF                 D(la_phiFF)                     "%7.3f";
double: %la_l1G                 D(la_l1_gs)                     "%7.3f";
double: %la_l2G                 D(la_l2_gs)                     "%7.3f";
double: %la_u                   D(la_u)                         "%7.3f";
string: %la_replan              D(la_replan)                    "%s";

#Longitudinal controller state
double: %lo_err                 D(lo_error)                     "%7.3f";
double: %lo_I                   D(lo_I)                         "%7.3f";
double: %lo_vR                  D(lo_vR)                        "%7.3f";
double: %lo_vel                 D(lo_vel)                       "%7.3f";
double: %lo_a                   D(lo_a)                         "%7.3f";
double: %lo_aFF                 D(lo_aFF)                       "%7.3f";
double: %lo_aP                  D(lo_aPitch)                    "%7.3f";
double: %lo_u                   D(lo_u)                         "%7.3f";
string: %lo_replan              D(lo_replan)                    "%s";

#Reactive obstacle avoidance
string: %roa_a                  D(roa_active)                   "%s";
string: %roa_enable             D(roa_enable)                   "%s";
double: %roa_lateral_error      D(roa_lateral_error)            "%7.3f";
double: %roa_speed              D(roa_speed)                    "%7.3f";
double: %roa_steer_angle        D(roa_steer_angle)              "%7.3f";
int:    %roa_delay              D(roa_delay)                    "%10d";
int:    %roa_ttc                D(roa_time_to_collision)        "%10d";

double: %roa_df                 D(roa_factor_delay)             "%7.3f";
double: %roa_ef                 D(roa_factor_lateral_error)     "%7.3f";
double: %roa_of                 D(roa_factor_obstacle)          "%7.3f";
double: %roa_mf                 D(roa_factor_master)            "%7.3f";

int:    %roa_del                D(roa_delay)                    "%10d";


tblname: followertable;
bufname: followerbuf;

