delay = 0;

L = 3.55;
l1 = 3;
l2 = .5;

s = tf('s');

G = 1/(L*l2*s^2 + l1*s);
G.InputDelay = delay;
figure(234)
nyquist(G)
axis equal
figure(235)
margin(G)



l1 = 0:0.5:15;
l2 = 0:2:50;
[L1,L2] = meshgrid(l1,l2);


w = sqrt((-L1.^2 + sqrt(L1.^4 + 4*L^2*L2.^2)) ./ (2*L^2*L2.^2));
figure(236);clf;hold on
contour(L1,L2,w)
mesh(L1,L2,w)
grid on
title('Cuttoff freq. for different parameters l_1, l_2')
xlabel('l_1 / m')
ylabel('l_2 / m')
zlabel('\omega_c / rad s^{-1}')

phaseRad = pi/2 - atan(L*L2.*w ./ L1);
figure(237);clf;hold on
contour(L1,L2,phaseRad * 180 / pi)
mesh(L1,L2,phaseRad * 180 / pi)
grid on
title('Phase margin for different parameters l_1, l_2')
xlabel('l_1 / m')
ylabel('l_2 / m')
zlabel('\phi_m / ^{\circ}')

maxDelay = phaseRad ./ w;
figure(238);clf;hold on
contour(L1,L2,maxDelay)
mesh(L1,L2,maxDelay)
grid on
title('Maximum allowed delay (in meters) for stability')
xlabel('l_1 / m')
ylabel('l_2 / m')
zlabel('Delay / m')

maxDelay30 = (phaseRad - 30*pi/180) ./ w;
figure(239);clf;hold on
contour(L1,L2,maxDelay30)
mesh(L1,L2,maxDelay30)
grid on
surface(L1,L2,zeros(size(L1)))
title('Maximum allowed delay (in meters) for \phi_m > 30^{\circ}')
xlabel('l_1 / m')
ylabel('l_2 / m')
zlabel('delay / m')

