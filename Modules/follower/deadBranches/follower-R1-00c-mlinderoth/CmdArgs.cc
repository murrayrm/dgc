/*!
 * \file CmdArgs.cc
 * \brief Enables follower commandline arguments to be read by all parts of follower.
 *
 * \author Nok Wongpiromsarn, Kristian Soltesz
 * \date 10 July 2007
 *
 */

#include "CmdArgs.hh"

/* shared ommand line arguments */
int CmdArgs::sn_key = 0;
int CmdArgs::rate = 10;
int CmdArgs::log_level = 0;
char* CmdArgs::log_path = NULL;
char* CmdArgs::frame = NULL;
char* CmdArgs::traj_file = NULL;
int CmdArgs::verbose = 0;
bool CmdArgs::disable_console = false;
bool CmdArgs::log_internal = false;
int CmdArgs::gear = 1; //This row will be removed in final version

