#ifndef FOLLOWER_LOGGER_HH_THDYKTFJGHJYTFYUJGYUJ
#define FOLLOWER_LOGGER_HH_THDYKTFJGHJYTFYUJGYUJ

#include <stdio.h>

class FollowerLogger {
private:
  FILE* m_file;

public:
  // time
  unsigned long long timeStamp;

  // Lateral
  double la_l1_gs;
  double la_l2_gs;
  double la_phiFF;
  double la_phi;
  double la_I;

  // Longitudinal
  double lo_vRef;
  double lo_aFF;
  double lo_speed;
  double lo_I;
  double lo_a;
  double lo_u;

  // Trajectory info
  int trajID;
  int closestIndex;
  double closestFractionToNext;

  // Vehicle state
  double curN;
  double curNd;
  double curE;
  double curEd;
  double curYaw;
  double curYawRate;

  // Data for closest point on  trajectory
  double closestN;
  double closestNd;
  double closestNdd;
  double closestE;
  double closestEd;
  double closestEdd;

  // Lateral controller parameters
  double la_l1;  // 28
  double la_l1Scale;
  double la_l2;
  double la_l2Scale;
  double la_alpha;
  double la_delay;

  // Longitudinal parameters
  double lo_lV;
  double lo_lI;
  double lo_alpha;
  double lo_delay;
  double lo_gasDeadU;
  double lo_gasDeadA;
  double lo_brakeDeadU;
  double lo_brakeDeadA;
  double lo_gasGain;
  double lo_brakeGain;
  double lo_gasVelocityGain;
  double lo_brakeVelocityGain;
  double lo_noGasSpeed;
  double lo_idleAcceleration;


  FollowerLogger();
  ~FollowerLogger();

  void writeLog();
};

#endif  // FOLLOWER_LOGGER_HH_THDYKTFJGHJYTFYUJGYUJ
