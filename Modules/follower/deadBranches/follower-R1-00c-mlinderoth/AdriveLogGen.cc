/*!
 * \file AdriveLogGen.cc
 * \brief Extracts meta-AdriveDirectives and corresponding timestamps from inputfiles.
 * \brief The extracted information is used to generate AdriveDirectives.
 * \brief The AdriveDirectives are stored in skynet logfile format.
 *  
 * \author Magnus Linderoth & Kristian Soltesz
 * \date 26 June 2007
*/

#include <stdio.h>
#include <iostream>
#include "gcinterfaces/AdriveCommand.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"

#define DBG(x)
using namespace std;

class AdriveLogGen {
  FILE *m_inputLog, *m_outputLog;
  char m_buffer[1000000]; //fixme
  int m_bufSize; //size in bytes of m_buffer 
  
public:
  AdriveLogGen(const char* inputLog, const char* outputLog) {
    //Open log files
    m_inputLog = fopen(inputLog, "r");
    if(m_inputLog == NULL){
      cerr << "Cannot open input log file: " << inputLog << endl;
      exit(1);
    }
    m_outputLog = fopen(outputLog, "w");
    if(m_outputLog == NULL){
      cerr << "Cannot open output log file: " << outputLog << endl;
      exit(1);
    }
    m_bufSize = 1000000;
  }

  ~AdriveLogGen() {
    //Close log files
    fclose(m_inputLog);
    fclose(m_outputLog);
  }
  
  
  bool logAdriveDirective(const AdriveDirective* ad, unsigned long long timeStamp) {
    char* pm_buffer = m_buffer; //buffer used to send and receive messages
    // Serialize the message to be written 
    //This code is from the skynettalker module (skynettalker.hh)
    try
      {
        using namespace skynettalker;
        SkynetBufferSink sink(&pm_buffer, &m_bufSize);
        boostio::stream<SkynetBufferSink> os(sink);
        boost::archive::binary_oarchive ar(os); //same as OAr in SkynetTalker.hh
        
        ar << *ad;
        os.flush(); // without this not all data is written in the buffer!
        int msgSize = os->getPos();
        const int msgType = SNadrive_command;
        
        //write standard skynet logfile format
        fwrite((char *) &timeStamp, sizeof(timeStamp), 1 ,m_outputLog);
        fwrite((char *) &msgType, sizeof(msgType), 1, m_outputLog);
        fwrite((char *) &msgSize, sizeof(msgSize), 1, m_outputLog);
        fwrite((char *) m_buffer, msgSize, 1, m_outputLog);
        return true;
      } catch( ... ) {
        // FIXME: catch the correct type of exception and print a more detailed error message
        // Serialization is the only thing that can throw, anyway
        cerr << "Exception caught serializing message." << endl;
        return false;
      } 
  }


  bool buildAdriveDirective(AdriveDirective* ad, unsigned long long* timeStamp, unsigned long* msgNum) {
    //read from m_inputLog, check for EOF
    msgNum++;
    if (fscanf(m_inputLog, "%llu\t%d\t%d\t%d\t%d\t%lf\n",
               timeStamp,
               &(ad->id),
               &(ad->source),
               (int *)&(ad->actuator),
               (int *)&(ad->command),
               &(ad->arg)) != 6) {
      if (!feof(m_inputLog)) {
        cerr << "Could not read message: " << &msgNum << "from file: " << m_inputLog << endl;
        exit(1);
      }
      return false;
    }
    return true;
  }


  bool processMsgs() {
    AdriveDirective ad;
    unsigned long long timeStamp;
    unsigned long msgNum;
    
    while (buildAdriveDirective(&ad, &timeStamp, &msgNum)) {
      
      if (!logAdriveDirective(&ad, timeStamp)) {
        exit(1);
      }
    }
    return true;
  }
};


int main(int argc, char** argv)
{
  if(argc != 3){
    cout << "Usage: adriveloggen inputLog outputLog" << endl;
  }
  char *inputLog = argv[1];
  char *outputLog = argv[2];
  AdriveLogGen adriveLogGen(inputLog, outputLog);
  adriveLogGen.processMsgs();
  return 0;
}

