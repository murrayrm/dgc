#include "ShiftController.hh"

ShiftController::ShiftController(): 
  m_trajDir(0),
  m_currentGear(0),
  m_commandGear(0)
{
  /* Send state pointers to SparrowHawk display */
  sparrowHawk->rebind("sh_trajDir", &m_trajDir);
  sparrowHawk->set_readonly("sh_trajDir");
  sparrowHawk->rebind("sh_currentGear", &m_currentGear);
  sparrowHawk->set_readonly("sh_currentGear");
  sparrowHawk->rebind("sh_commandGear", &m_commandGear);
  sparrowHawk->set_readonly("sh_commandGear");
}

bool ShiftController::control(AdriveDirective* adriveDirective,
                             int* directivesToSend, int trajDir,
                             SingleFrameVehicleState* vehicleState, ActuatorState* actuatorState)
{

  /* Update trajectory direction */
  m_trajDir = trajDir;
  
  /* Update current gear */
  m_currentGear = actuatorState->m_transpos;

  /* Update commanded gear */
  m_commandGear = actuatorState->m_transcmd;

  
  if (m_currentGear != m_commandGear && m_commandGear != 0) { /* Alice is shifting*/
    printConsole("Alice is shifting" << endl);
    if (m_currentGear == m_trajDir) { /* Shifting is completed */
      return false;
    } else { /* Shifting in progress */
      return true;
    }
  } else { /* Alice is not shifting */
    if (m_currentGear != m_trajDir) { /* Alice needs to shift gears */

      /* Send shift directive*/
      adriveDirective[0].actuator = Transmission;
      adriveDirective[0].command = SetPosition;
      adriveDirective[0].arg = m_trajDir;
      *directivesToSend = 1;
      return true;

    } else { /* Alice is in the right gear */
      return false;
    }
  }
}
