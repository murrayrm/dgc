/*!
 * \file LateralController.cc
 * \brief Takes care of the lateral lateral part in trajectory following.
 *
 * \author Kristian Soltesz, Magnus Linderoth
 * \date 10 July 2007
 *
 * This file is part of the follower module.
 */

#include "alice/AliceConstants.h"
#include "LateralController.hh"
#include "CmdArgs.hh"

/* Constructor for LataralController */
LateralController::LateralController(LateralControllerParams* params):
  m_params(*params) {}

/* Method for caluclating control signal */
void LateralController::control(AdriveDirective* adriveDirective, FollowerMergedDirective* followerMergedDirective,
                                SingleFrameVehicleState* state) {
  //FIXME: is this neccessary?
  double pxR[3];
  double pyR[3];
  double pxFF[3];
  double pyFF[3];

  /* Extract a pointer to the traj */
  CTraj* traj = followerMergedDirective->traj; 

  /* Extract direction of the traj */
  int dir = traj->getDirection();
  // FIXME remove dir override
  dir = 1;

  /* Calculate feed forward point */
  double ffDist = state->speed * m_params.delay;

  /* Current state of Alice */
  double xCR = state->x;
  double yCR = state->y;
  double thetaC = state->yaw;
  if (CmdArgs::verbose > 3) cout << "Alice state: x=" << xCR << " y=" << yCR << " yaw=" << thetaC << endl;

  /* Determine the reference and feed forward points on the traj by linear interpolation*/
  int closestIndex;
  double closestFractionToNext;
  TrajPoint refPoint = traj->interpolGetClosest(xCR, yCR, &closestIndex, &closestFractionToNext); 
  TrajPoint ffPoint = traj->interpolGetPointAhead(closestIndex, closestFractionToNext, ffDist, NULL, NULL);

  //FIXME: is this neccessary?  Fill in the reference and feed forward points
  pxR[0] = refPoint.n;
  pxR[1] = refPoint.nd;
  pxR[2] = refPoint.ndd;
  pyR[0] = refPoint.e;
  pyR[1] = refPoint.ed;
  pyR[2] = refPoint.edd;
  pxFF[0] = ffPoint.n;
  pxFF[1] = ffPoint.nd;
  pxFF[2] = ffPoint.ndd;
  pyFF[0] = ffPoint.e;
  pyFF[1] = ffPoint.ed;
  pyFF[2] = ffPoint.edd;
  

  if (CmdArgs::verbose >3 ) cout << "ClosestIndex: " << closestIndex << " ClosestFractionToNext: " << closestFractionToNext << endl;
  
  /* Gain scheduling */
  double l1_gs = m_params.l1Scale*state->speed;
  if (l1_gs < m_params.l1) {
    l1_gs = m_params.l1;
  }
  double l2_gs = m_params.l2Scale * state->speed*state->speed / VEHICLE_WHEELBASE / VEHICLE_MAX_LATERAL_ACCEL;
  if (l2_gs < m_params.l2) {
    l2_gs = m_params.l2;
  }
  if (CmdArgs::verbose > 3) cout << "l1=" << l1_gs << ", l2=" << l2_gs << endl;
  
  /* Calcualte feed forward control signal */
  double phiFF = atan(VEHICLE_WHEELBASE * (pxFF[1]*pyFF[2] - pyFF[1]*pxFF[2])
                      / pow(pxFF[1]*pxFF[1] + pyFF[1]*pyFF[1], 1.5));

  /* Yaw of reference vehicle */
  double thetaR = atan2(pyR[1], pxR[1]);

  /* Point to steer towards */
  double xS = pxR[0] + l1_gs*cos(thetaR) + l2_gs*cos(thetaR + m_params.alpha*phiFF);
  double yS = pyR[0] + l1_gs*sin(thetaR) + l2_gs*sin(thetaR + m_params.alpha*phiFF);
  if (CmdArgs::verbose > 3) cout << "Steer point: " << xS << ", " << yS << endl;
  
  /* Current position of front wheel axis center.
   * In the case of reverse driving it is the front 
   * wheel position of the mirrored car 
   */
  double xCF = xCR + dir * l1_gs * cos(thetaC);
  double yCF = yCR + dir * l1_gs * sin(thetaC);
  if (CmdArgs::verbose > 3) cout << "Front point: " << xCF << ", " << yCF << endl;
  
  /* Steer angle */
  double phi;
  switch (dir) {
  case 1:
    /* Forward */
    phi = atan2(yS-yCF, xS-xCF) - thetaC;
    break;
  case -1:
    /* Reverse */
    phi = -(atan2(yS-yCF, xS-xCF) - thetaC + M_PI);
    break;
  default:
    if(CmdArgs::verbose) cerr << "LateralController::control error: Direction is neither forwards nor backwards" << endl;
    break;
  }
  /* Make sure angle is in interval [-pi, pi[ */
  phi = fmod(phi + 31*M_PI, 2*M_PI) - M_PI;
  if (CmdArgs::verbose > 3) cout << "phi: " << phi << "  dir: " << dir << endl;
  
  /* Saturate steer angle */
  if (phi > VEHICLE_MAX_AVG_STEER) {
    phi = VEHICLE_MAX_AVG_STEER;
  } else if (phi < -VEHICLE_MAX_AVG_STEER) {
    phi = -VEHICLE_MAX_AVG_STEER;
  }

  /* Keep old steering if end of traj is reached */
  if (closestIndex == traj->getNumPoints() - 1) {
    phi = m_old_phi;
  }

  m_old_phi = phi;

  /*Output steer command to returned AdriveDirective */
  adriveDirective->actuator = Steering;
  adriveDirective->command = SetPosition;
  adriveDirective->arg = phi / VEHICLE_MAX_AVG_STEER;
}


