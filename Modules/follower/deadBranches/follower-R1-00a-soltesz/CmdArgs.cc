/*!
 * \file CmdArgs.cc
 * \brief Enables follower commandline arguments to be read by all parts of follower.
 *
 * \author Nok Wongpiromsarn, Kristian Soltesz
 * \date 10 July 2007
 *
 */

#include "CmdArgs.hh"

/* Non hidden command line arguments */
int CmdArgs::sn_key = 0;
int CmdArgs::rate = 10;
int CmdArgs::log_level = 0;
char* CmdArgs::log_path = NULL;
char* CmdArgs::frame = NULL;
char* CmdArgs::traj_file = NULL;
int CmdArgs::verbose = 0;

