t = 0.1;

% Ramps up to speed stays there and ramps down again
posAcc = 1;     % m/s^2
maxSpeed = 4;   % m/s
constTime = 15; % s
negAcc = -1;     % m/s^2

% acc
a = [posAcc*ones(maxSpeed/posAcc/t,1);
    zeros(constTime/t,1);
    negAcc*ones(-maxSpeed/negAcc/t,1);
    ];
v = t*cumsum(a);
p = t*cumsum(v);

T = [zeros(length(a),3) p v a];

while T(end,1)==T(end-1,1) && T(end,4)==T(end-1,4)
    T = T(1:end-1,:);
end

dist = p(end)

save speed4Ramp1.traj T -ASCII