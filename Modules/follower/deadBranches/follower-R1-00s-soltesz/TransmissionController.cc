///
/// \file TransmissionController.cc
/// \brief Takes care of the gear shifting part in trajectory following.
///
/// \author Kristian Soltesz
/// \date 2 August 2007
///
/// This file is part of the follower module.

#include "TransmissionController.hh"

TransmissionController::TransmissionController():
  m_gear(1),
  m_shifting(false)
{
  sparrowHawk->rebind("sh_gear", &m_gear);
  sparrowHawk->set_readonly("sh_gear");
  sparrowHawk->rebind("sh_shifting", &m_shifting);
  sparrowHawk->set_readonly("sh_shifting");

}

bool TransmissionController::control(FollowerControlStatus* controlStatus, AdriveDirective* transmissionDirective, int trajectoryDirection)
{

  // A shift direction should only be 
  // sent if Alice is in the wrong gear
  // and shifting is not pending
  if ((m_gear != trajectoryDirection) && !m_shifting && !(FollowerOpStates::state & FollowerOpStates::TrajectoryError)) {
    transmissionDirective->arg = trajectoryDirection;
    FollowerOpStates::state |= FollowerOpStates::TransmissionPending;
    m_shifting = true;

    // Returning true, indicating 
    // that a shift directive should be sent
    return true;
  } 

  // Returning false, indicating
  // that no shift direcitve should be sent
  return false;
}

void TransmissionController::completedShifting(int newGear)
{
  // The new gear is written
  // and we are no longer shifting
  m_gear = newGear;
  FollowerOpStates::state &= ~FollowerOpStates::TransmissionPending;
  m_shifting = false;
}

void TransmissionController::failedShifting()
{
  // We are no longer shifting, since shifting failed
  m_shifting = false;
}

void TransmissionController::initialize(int gear)
{
  // This is the real initialization
  m_gear = gear;
  
  // For bobustness
  m_shifting = false;
  FollowerOpStates::state &= ~FollowerOpStates::TransmissionPending;
}
