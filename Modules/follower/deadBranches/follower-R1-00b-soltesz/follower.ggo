#
# This is the source file for managing follower command-line options.  
# It uses gengetopt (http://www.gnu.org/software/gengetopt/gengetopt.html)
#
# Kristian Soltesz, 2007-07-10
#
# This file is part of the follower module
#
# Specification:
#   (1) no command-line options required if default settings are ok
#   (2) Command-line options will override default settings

package "follower"
purpose "The follower module is used to follow a trajectory in Alice. It receives 
trajectory and state information over skynet, and outputs actuation commands over 
skynet. (For testing purposes trajectories can also be loaded from file)."
version "1.0"

#follower options
option "rate" r "Rate at which to run follower (Hz)" int default="10" no
option "frame" f "Local or UTM frame" string default="local" no
option "traj-file" t "Read CTraj from file" string no

# Configuration files
option "argfile" - "file containing command line arguments" string no

# Options for logging
option "log-level" - "set the gcmodule log level" int default="0" no argoptional
option "log-path" - "set the log path"
  string default="./logs" no

# Standard options
option "skynet-key" S "skynet key" int default="0" no
option "enable-astate" - "enable astate (from Applanix)" flag off
option "disable-console" D "turn off sparrow display" flag  off
option "verbose" v "turn on verbose messages" int default="1" no argoptional

# Hidden options - used to set controller parameters
option "lo-lV"                  - "longitudinal controller parameter"   double default="0.0"    no
option "lo-lI"                  - "longitudinal controller parameter"   double default="0.0"    no
option "lo-alpha"               - "longitudinal controller parameter"   double default="1.0"    no
option "lo-delay"               - "longitudinal controller parameter"   double default="0.4"    no

option "gasDeadU"               - "longitudinal nonlinearity parameter" double default="0.125"  no
option "gasDeadA"               - "longitudinal nonlinearity parameter" double default="0.0"    no
option "brakeDeadU"             - "longitudinal nonlinearity parameter" double default="-0.39"  no
option "brakeDeadA"             - "longitudinal nonlinearity parameter" double default="-0.17"  no
option "gasGain"                - "longitudinal nonlinearity parameter" double default="13.0"   no
option "brakeGain"              - "longitudinal nonlinearity parameter" double default="4.0"    no
option "gasVelocityGain"        - "longitudinal nonlinearity parameter" double default="0.15"   no
option "brakeVelocityGain"      - "longitudinal nonlinearity parameter" double default="0.3"    no
option "noGasSpeed"             - "longitudinal nonlinearity parameter" double default="2.0"    no
option "idleAcceleration"       - "longitudinal nonlinearity parameter" double default="0.6"    no

option "la-l1"                  - "lateral controller parameter"        double default="4.0"    no
option "la-l1-scale"             - "lateral controller parameter"        double default="0.0"    no
option "la-l2"                  - "lateral controller parameter"        double default="6.0"    no
option "la-l2-scale"             - "lateral controller parameter"        double default="0.0"    no
option "la-alpha"               - "lateral controller parameter"        double default="1.0"    no 
option "la-delay"               - "lateral controller parameter"        double default="0.6"    no




