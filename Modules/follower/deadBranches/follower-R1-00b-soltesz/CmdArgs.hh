/*!
 * \file CmdArgs.hh
 * \brief Enables follower commandline arguments to be read by all parts of follower.
 *
 * \author Nok Wongpiromsarn, Kristian Soltesz
 * \date 10 July 2007
 *
 */

#ifndef CMDARGS_HH_345678945670987654
#define CMDARGS_HH_345678945670987654

#include <string>

using namespace std;

class CmdArgs {
public:
  static int sn_key;
  static int rate;
  static int log_level;
  static char* log_path;
  static char* frame;
  static char* traj_file;
  static int verbose;
  static bool disable_console;
};

#endif
