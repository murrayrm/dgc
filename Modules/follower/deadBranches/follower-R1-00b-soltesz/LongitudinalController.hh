/*!
 * \file LongitudinalController.hh
 * \brief Takes care of the longitudinal part in trajectory following.
 *
 * \author Kristian Soltesz, Magnus Linderoth
 * \date 10 July 2007
 *
 * This file is part of the follower module.
 */

#ifndef LONGITUDINAL_CONTROLLER_HH_HBIFUWEHIFGSEJOIFJS
#define LONGITUDINAL_CONTROLLER_HH_HBIFUWEHIFGSEJOIFJS

#include "trajutils/traj.hh"
#include "FollowerUtils.hh"
#include "gcinterfaces/AdriveCommand.hh"

#define LONGITUDINAL_SPEED_CONTROLLER 0
#define LONGITUDINAL_POSITION_CONTROLLER 1

//enum {LONGITUDINAL_SPEED_CONTROLLER = 0,
//       LONGITUDINAL_POSITION_CONTROLLER};

/*
 * \struct LongitudinalControllerParams
 *
 * This stuct holds the parameters of the longitudinal controller
 */
struct LongitudinalControllerParams {
  double lV;
  double lI;
  double alpha;
  double delay; // sec


  double gasDeadU;  // [cmd]
  double gasDeadA;  // [m/s^2]
  double brakeDeadU;  // [cmd]
  double brakeDeadA;  // [m/s^2]

  double gasGain;    // a/u [m/s^2 / cmd]
  double brakeGain;  // a/u [m/s^2 / cmd]

  double gasVelocityGain;  // a/v [m/s^2 / m/s  =  1/s]
  double brakeVelocityGain;  // a/v [m/s^2 / m/s  =  1/s]

  double noGasSpeed;  // [m/s]
  double idleAcceleration;  // [m/s^2]


  /* Constructor for LongitudinalController */
  LongitudinalControllerParams(double v, double i, double a, double d,
                               double gdu, double gda, double bdu, double bda, 
                               double gg, double bg, double gvg, double bvg, 
                               double ngs, double ia) {
    lV = v;
    lI = i;
    alpha = a;
    delay = d;


    gasDeadU = gdu;
    gasDeadA = gda;
    brakeDeadU = bdu;
    brakeDeadA = bda;
    gasGain = gg;
    brakeGain = bg;
    gasVelocityGain = gvg;
    brakeVelocityGain = bvg;
    noGasSpeed = ngs;
    idleAcceleration = ia;
    
  }
};

/*
 * \class LongitudinalController
 *
 * This is the class in which the longitudinal controller is implemented
 */
class LongitudinalController
{
protected:
  /** The parameters used by the longitudinal controller*/
  LongitudinalControllerParams m_params;
  double m_I; //FIXME

  double linearize(double a, double v, LongitudinalControllerParams* params);

public:
  /* Constrtuctor for LongitudinalController 
   *
   * @param params longitudinal controller parameters
   */
  LongitudinalController(LongitudinalControllerParams* params);

 /* Method in which the control signal is calculated 
  *
  * @param adriveDirective pointer to which the resulting AdriveDirective is written
  * @param followerMergedDirective pointer to CTraj with timestamp
  * @param state pointer to vehicle state object
  *
  */
  void control(AdriveDirective* adriveDirective, FollowerMergedDirective* followerMergedDirective, 
               SingleFrameVehicleState* state);
};

#endif // LONGITUDINAL_CONTROLLER_HH_HBIFUWEHIFGSEJOIFJS
