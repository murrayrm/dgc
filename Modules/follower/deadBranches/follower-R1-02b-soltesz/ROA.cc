///
/// \file ROA.cc
/// \brief Reactive Obstacle Avoidance.
/// 
/// \Kristian Soltesz, Magnus Linderoth
/// \date 21 September 2007
/// 
/// ROA gets delay and lateral error information from follower,
/// together with front and rear center ladar scans. It uses
/// this information to shape the velocity reference in order 
/// to avoid collisions. 
/// 
/// This file is part of the follower module.

#include "ROA.hh"
#define MAX_DISPLAY_DOUBLE 1000.0
#define sign(x) ((x)>0 ? 1:((x)<0?(-1):0))  // does not return 0 for x==0

// Constructor for ROA
ROA::ROA(FollowerControlStatus* controlStatus, FollowerLogger* internalLogger) :
  m_override(false),
  m_delayDistance(0.0),
  m_obstacleMaxDecelerationTerm(0.0),
  m_trigger(MAX_DISPLAY_DOUBLE),
  m_obstacleCollisionDistance(0.0),
  m_internalLogger(internalLogger),
  m_lateralError(0),
  m_gear(1),
  m_speed(0.0),
  m_steerAngle(0.0) {
  memset(m_timeStamps, 0, sizeof(m_timeStamps));
  memset(m_ages, 0, sizeof(m_ages));
  initSensnet();
  sparrowInit();
}


int ROA::initSensnet() {
  // Set spread daemon
  char* spreadDaemon;
  
  // FIXME: use dgc utils find function
  if (getenv("SPREAD_DAEMON")) {
    spreadDaemon = getenv("SPREAD_DAEMON");
  } else {
    cerr << "SPREAD_DEAMON environment variable not set." << endl;
    return -1;
  }

  // Set skynet key
  int skynetKey;

  if (getenv("SKYNET_KEY"))
    skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    skynetKey = 0;

  // Create sensnet interface
  m_sensnet = sensnet_alloc();
  assert(m_sensnet);
  if (sensnet_connect(m_sensnet, spreadDaemon, skynetKey, MODROA) != 0) {
    printConsole("Unable to connect to sensnet" << endl);
    return -1;
  }

  if (sensnet_join(m_sensnet,SENSNET_MF_BUMPER_LADAR, SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0) {
    printConsole("Unable to join SENSNET_MF_BUMPER_LADAR" << endl);
    exit(1);
    return -1;
  }

  if (sensnet_join(m_sensnet,SENSNET_REAR_BUMPER_LADAR, SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0) {
    printConsole("Unable to join SENSNET_REAR_BUMPER_LADAR" << endl);
    exit(1);
    return -1;
  }

  return 0;
}


void ROA::setParams(ROAParams params) {
  m_params = params;
}

void ROA::sparrowInit() {

  // Gcdrive loop ages
  sparrowHawk->rebind("steering_loop_age", &m_ages[STEERING_LOOP]);
  sparrowHawk->set_readonly("steering_loop_age");
  sparrowHawk->rebind("acceleration_loop_age", &m_ages[ACCELERATION_LOOP]);
  sparrowHawk->set_readonly("acceleration_loop_age");

  // Gcdrive state ages
  sparrowHawk->rebind("steering_state_age", &m_ages[STEERING_STATE]);
  sparrowHawk->set_readonly("steering_state_age");
  sparrowHawk->rebind("acceleration_state_age",&m_ages[ACCELERATION_STATE]);
  sparrowHawk->set_readonly("acceleration_state_age");

  // Other ages
  sparrowHawk->rebind("trajectory_age", &m_ages[TRAJECTORY]);
  sparrowHawk->set_readonly("trajectory_age");
  sparrowHawk->rebind("vehicle_state_age", &m_ages[VEHICLE_STATE]);
  sparrowHawk->set_readonly("vehicle_state_age");

  // State
  sparrowHawk->rebind("roa_gear", &m_gear);
  sparrowHawk->set_readonly("roa_gear");
  sparrowHawk->rebind("roa_lateral_error", &m_lateralError);
  sparrowHawk->set_readonly("roa_lateral_error");
  sparrowHawk->rebind("roa_speed", &m_speed);
  sparrowHawk->set_readonly("roa_speed");
  sparrowHawk->rebind("roa_steer_angle", &m_steerAngle);
  sparrowHawk->set_readonly("roa_steer_angle");

  // Obstacle and delay
  sparrowHawk->rebind("roa_collisionDistance", &m_obstacleCollisionDistance);
  sparrowHawk->set_readonly("roa_collisionDistance");
  sparrowHawk->rebind("roa_obstacleTrigger", &m_trigger);
  sparrowHawk->set_readonly("roa_obstacleTrigger");
  sparrowHawk->rebind("roa_delayDistance",&m_delayDistance);
  sparrowHawk->set_readonly("roa_delayDistance");
  sparrowHawk->rebind("roa_decelerationTerm", &m_obstacleMaxDecelerationTerm);
  sparrowHawk->set_readonly("roa_decelerationTerm");
}

void ROA::updateTimeStamp(int index, unsigned long long timeStamp) {
  m_timeStamps[index] = timeStamp;
}

void ROA::updateAge(int index) {
  // [micro seconds]
  double tmpAge = (DGCgettime()-m_timeStamps[index])/1000000.0;
  m_ages[index] = min(tmpAge, MAX_DISPLAY_DOUBLE);

  // Write data to logger object
  if (CmdArgs::log_internal || CmdArgs::plot) {
    m_internalLogger->roa_ages[index] = tmpAge; //m_ages[index];
  }
}

void ROA::updateAllAges() {
  for (int index = 0; index < NUMBER_OF_TIMES; index++) {
    updateAge(index);
  }
}

void ROA::updateLateralError(double lateralError) {
  m_lateralError = lateralError;
}

void ROA::updateSpeed(double speed) {
  m_speed = speed;
}

void ROA::updateSteerAngle(double steerAngle) {
  m_steerAngle = steerAngle;
}

void ROA::updateGear(int gear) {
  m_gear = gear;
}

int ROA::updateLadar() {
  
  int ladarId;

  if (m_gear == -1) {
    ladarId = SENSNET_REAR_BUMPER_LADAR;    
  } else if (m_gear == 1) {
    ladarId = SENSNET_MF_BUMPER_LADAR;
  } else {
    return -1;
  }
  
  int blobId, blobLen;
  if (sensnet_peek(m_sensnet, ladarId, SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
    {    
      printConsole("Error peeking sensnet" << endl);   
      return -1;
    }

  if (sensnet_read(m_sensnet, ladarId, SENSNET_LADAR_BLOB, &blobId, sizeof(m_blob), &m_blob) != 0) {
    printConsole("Error reading from sensnet" << endl);
    return -1;
  } else {
    return 0;
  }
}

bool ROA::inRiscZone(float vfx, float vfy, float* distance) {


  // FIXME: This function needs to be tuned
  bool returnVal = false;
  float straightDistance = std::numeric_limits<float>::max();
  float arcDistance = std::numeric_limits<float>::max();
 
  printConsole("vfx: " << vfx << " vfy: " << vfy << " m_gear: " << m_gear << endl);

  if (m_gear == 1) { 
    
    // Straight ahead check
    
    if (fabs(vfy) < m_params.forwardRiscZoneHalfWidth) {
      straightDistance = fabs(vfx);
      returnVal = true;
    } 
    
    // FIXME: hack
    if (m_steerAngle < 0.001 && m_steerAngle > -0.001) {
      m_steerAngle = 0.001;
    } 
    
    // Negative 'radius' when turning CCW
    float turningRadius = VEHICLE_WHEELBASE/tan(m_steerAngle);
    
    // Circle check
    if (fabs(fabs(turningRadius)-sqrt(pow(vfx,2.0)+pow(vfy-turningRadius,2.0))) < m_params.forwardRiscZoneHalfWidth) {
      // *distance = min(*distance, (float)(atan2(vfx,(turningRadius-vfy)*turningRadius/fabs(turningRadius))*fabs(turningRadius)));
            arcDistance = atan2(vfx,(turningRadius-vfy)*turningRadius/fabs(turningRadius))*fabs(turningRadius);
	    returnVal = true;
    }

  } else if (m_gear == -1) {
    if (fabs(vfy) < m_params.backupRiscZoneHalfWidth && m_params.enableBackup) {
      straightDistance = fabs(vfx);
      returnVal = true;
    }
  }

  printConsole("straight: " << straightDistance << endl);
  printConsole("arc: " << arcDistance << endl);

  *distance = min(straightDistance, arcDistance);
  return returnVal;
}
  
int ROA::updateCollisionDistance() {
  LadarRangeBlob* blob = &m_blob;
  
  // Populate the blob
  if (updateLadar() != 0) {
      return -1;
  }
 
  
  // Loop through the blob
  float tmpDistance, minDistance = std::numeric_limits<float>::max();
  
  for (int i = 0; i < blob->numPoints; i++) {
    // Vehicle frame
    float vfx, vfy;
    
    // Protection from using wrong coordinates
    {
      float vfz, sfx, sfy, sfz;
      float ba = blob->points[i][ANGLE];
      float br = blob->points[i][RANGE];
      LadarRangeBlobScanToSensor(blob, ba, br, &sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    }
    
    if (inRiscZone(vfx, vfy, &tmpDistance) && tmpDistance < minDistance) {
      minDistance = tmpDistance;
    }
  }

  double tmpCollisionDistance;
  if (m_gear == -1) {
    tmpCollisionDistance = max(minDistance - MAX_DIST_REAR_TO_REAR_AXLE, 0.0);
    m_obstacleCollisionDistance = min(tmpCollisionDistance, MAX_DISPLAY_DOUBLE);
    return 0;
  } else if (m_gear == 1){
    tmpCollisionDistance = max(minDistance - MAX_DIST_REAR_AXLE_TO_FRONT,0.0);
    m_obstacleCollisionDistance = min(tmpCollisionDistance, MAX_DISPLAY_DOUBLE);
    return 0;
  } 
  
  return -1;
}


void ROA::control(FollowerControlStatus* controlStatus, double* u) {
  
  // actually displays status from previous invocation
  sparrowUpdateStatus(controlStatus);

  // Find out how old things are
  updateAllAges();

    if (m_params.enableDelay) {
      m_delayDistance = (m_ages[TRAJECTORY]+max(m_ages[STEERING_LOOP],m_ages[ACCELERATION_LOOP]))*m_speed;
    } else {
     m_delayDistance = 0.0;
    }

    
    if (m_params.enableObstacle && updateCollisionDistance() != 0 || !m_params.enableObstacle) {
      // Not able to read ladar
      m_obstacleCollisionDistance = m_params.ladarErrorSafetyDistance; 
    } 

    m_obstacleMaxDecelerationTerm = pow(m_speed,2.0) / (2.0*m_params.maxDeceleration);

    double tmpTrigger = m_obstacleCollisionDistance - 
      (m_delayDistance + m_obstacleMaxDecelerationTerm + m_params.obstacleDistanceMargin);

    m_trigger = min(tmpTrigger, MAX_DISPLAY_DOUBLE);


    // FIXME: debug information
    if (m_override && m_trigger < 0.0) {
      printConsole("overriding ROA" << endl);
    }

    if (m_trigger < 0.0 && m_params.enableMaster && !m_override) { 
      printConsole("ROA BRAKING" << endl);
      *u = -1.0; 
      controlStatus->state |= FollowerState::ReactiveObstacleAvoidance;
    } else {
      controlStatus->state &= ~FollowerState::ReactiveObstacleAvoidance;
    }
    
  // Log ROA status
  if (CmdArgs::log_internal || CmdArgs::plot) {
    m_internalLogger->roaBraking = (int)(currentlyBraking()); //m_ages[index];
  }
}


void ROA::updateLongitudinalFeedForward(double* feedForward) {
  if (m_trigger < 0.0 && !m_override) {
    *feedForward = 0.0;
  }
}

bool ROA::currentlyBraking() {
  return (m_params.enableMaster && m_trigger < 0.0);
}

void ROA::updateOverride(bool state) {
  m_override = state;
}


void ROA::sparrowUpdateStatus(FollowerControlStatus* controlStatus) {
  if (controlStatus->state & FollowerState::ReactiveObstacleAvoidance) {
          sparrowHawk->set_string("la_replan", "Requested replan");
  } else {
      sparrowHawk->set_string("la_replan", "                ");
  }


  if (m_params.enableMaster) {
    sparrowHawk->set_string("roa_enable_master", "MASTER");
  } else {
    sparrowHawk->set_string("roa_enable_master", "      ");
  }

  if (m_params.enableDelay) {
    sparrowHawk->set_string("roa_enable_delay", "DELAY");
  } else {
    sparrowHawk->set_string("roa_enable_delay", "     ");
  }

  if (m_params.enableObstacle) {
    sparrowHawk->set_string("roa_enable_obstacle", "OBSTACLE");
  } else {
    sparrowHawk->set_string("roa_enable_obstacle", "        ");
  }

  if (m_params.enableBackup) {
    sparrowHawk->set_string("roa_enable_backup", "BACKUP");
  } else {
    sparrowHawk->set_string("roa_enable_backup", "      ");
  }
}

