///
/// \file ROA.hh
/// \brief Reactive Obstacle Avoidance.
/// 
/// \Kristian Soltesz, Magnus Linderoth
/// \date 21 September 2007
/// 
/// ROA gets delay and lateral error information from follower,
/// together with front and rear center ladar scans. It uses
/// this information to shape the velocity reference in order 
/// to avoid collisions. 
/// 
/// This file is part of the follower module.

#ifndef ROA_UJGGIOSDBUYCBIUSDGNBDSHGIUCBUIGDJK
#define ROA_UJGGIOSDBUYCBIUSDGNBDSHGIUCBUIGDJK

#include "FollowerUtils.hh"
#include "FollowerLogger.hh"
#include "alice/AliceConstants.h"
#include <assert.h>

// Sensnet/Skynet support
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include <skynettalker/SkynetTalker.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>

#define ANGLE 0
#define RANGE 1

/// 
/// \struct ROAParams
///
/// This struct holds the parameters of the ROA.
struct ROAParams {
  bool enableMaster;  // FIXME: structure (array)
  bool enableDelay;
  bool enableObstacle;
  bool enableBackup;
  double maxDeceleration;
  double forwardRiscZoneHalfWidth;
  double backupRiscZoneHalfWidth;
  double obstacleDistanceMargin;
  double ladarErrorSafetyDistance;

  ROAParams()
  {
    ROAParams(false, false, false, false, 0.0, 0.0, 0.0, 0.0, 0.0);
  }

  /// Overloaded constructor for LateralControllerParams
  ROAParams(bool param1, bool param2, bool param3, bool param4, double param5, double param6, double param7, double param8, double param9) {  
    
    enableMaster = param1;
    enableDelay = param2;
    enableObstacle = param3;
    enableBackup = param4;
    maxDeceleration = param5;
    forwardRiscZoneHalfWidth = param6;
    backupRiscZoneHalfWidth = param7;
    obstacleDistanceMargin = param8;
    ladarErrorSafetyDistance = param9;
  }
};

class ROA : public CMapElementTalker
{
public:
  enum TIMES {
    STEERING_LOOP = 0, 
    ACCELERATION_LOOP, 
    STEERING_STATE,
    ACCELERATION_STATE,
    TRAJECTORY,    
    VEHICLE_STATE,
    FOLLOWER_CYCLE,
    NUMBER_OF_TIMES
  };

  enum ROA_FACTORS {
    DELAY = 0,
    LATERAL_ERROR,
    OBSTACLE,
    MASTER
  };
  
  // Initialize sensnet
  int initSensnet();
  
protected:

  // ROA override signal from planner
  bool m_override;

  double m_delayDistance;
  double m_obstacleMaxDecelerationTerm;
  double m_trigger;
  
  /// Distance to collision
  double m_obstacleCollisionDistance;

  LadarRangeBlob m_blob;

  // Sensnet module
  sensnet_t* m_sensnet; 


  /// The parameters used by the ROA
  ROAParams m_params;

  /// FollowerLogger is used to evaluate performance of follower
  FollowerLogger* m_internalLogger;

  // Vector of time stamps used for calculating delays
  unsigned long long m_timeStamps[NUMBER_OF_TIMES];

  // Ages (of time stamps) used for calculating delays
  double m_ages[NUMBER_OF_TIMES];

  /// Error used by ROA function
  double m_lateralError;

  /// Current gear
  int m_gear;

  /// Current speed
  double m_speed;
  
  /// Current steer angle
  double m_steerAngle;


  /// Binds variables to sparrowHawk display
  void sparrowInit();


  /// Reads information from the ladar feeder
  int updateLadar();

  void updateClosestObstacle();

  // Checks whether a given point in vehicle frame is in the risc zone
  bool inRiscZone(float vfx, float vfy, float* distance);

  /// Updates the estimeated timeToCollision
  int updateCollisionDistance();

  // Updates age used to calculate delay
  void updateAge(int index);

  // Updates ages used to calculate delay
  void updateAllAges();

  
  // Refreshes status in sparrowHawk display
  void sparrowUpdateStatus(FollowerControlStatus* controlStatus);
  
public:
  ROA(FollowerControlStatus* controlStatus, FollowerLogger* internalLogger);  

  /// Sets whether ROA should be overridden
  void updateOverride(bool state);

  void updateTimeStamp(int index, unsigned long long timeStamp);
  

  /// Invoked from the lateral controller. 
  /// Updates the lateral error used in the ROA module
  void updateLateralError(double lateralError);

  void updateSpeed(double speed);

  void updateSteerAngle(double steerAngle);

  /// Lets ROA know which direction Alice is goingg
  void updateGear (int gear);

  /// The actual ROA function
  void control(FollowerControlStatus* controlStatus, double* u);

  /// Modifies the feed forward term
  void updateLongitudinalFeedForward(double* feedForward);

  /// Sets the parameters of the ROA
  void setParams(ROAParams params);

  /// returns whether ROA is currently braking
  bool currentlyBraking();
};

#endif //ROA_UJGGIOSDBUYCBIUSDGNBDSHGIUCBUIGDJK
