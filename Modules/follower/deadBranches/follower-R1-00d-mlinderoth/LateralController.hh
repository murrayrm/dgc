/*!
 * \file LateralController.hh
 * \brief Takes care of the lateral lateral part in trajectory following.
 *
 * \author Kristian Soltesz, Magnus Linderoth
 * \date 10 July 2007
 *
 * This file is part of the follower module.
 */

#ifndef LATERAL_CONTROLLER_HH_IRGBFBFWESHIFDLUHAIUHF
#define LATERAL_CONTROLLER_HH_IRGBFBFWESHIFDLUHAIUHF

#include "FollowerUtils.hh"
#include "gcinterfaces/AdriveCommand.hh"
#include "interfaces/ActuatorState.h"
#include "FollowerLogger.hh"

/*! 
 * \struct LateralControllerParams
 *
 * This struct holds the parameters of the lateral controller.
 */
struct LateralControllerParams {
  double l1;  // [m]   
  double l1Scale;
  double l2; // [m]
  double l2Scale;
  double lI;
  double alpha;
  double delay; // [s]
  
  /*! Constructors for LateralControllerParmas */
  LateralControllerParams()
  {
    LateralControllerParams(0,0,0,0,0,0,0);
  }

  /*! Overloaded constructor for LateralControllerParams */
  LateralControllerParams(double param1, double param2, double param3, 
                          double param4, double param5, double param6, double param7) {
    l1 = param1;
    l1Scale = param2;
    l2 = param3;
    l2Scale = param4;
    lI = param5;
    alpha = param6;
    delay = param7;
  }
};


/*!
 * \class LateralController
 *
 * This is the class in which the lateral controller is implemented.
 */
class LateralController
{
protected:
  /*! The parameters used by the lateral controller */
  LateralControllerParams m_params;

  /*! SparrowHawk display is the runtime operator interface */
  CSparrowHawk* m_sparrowHawk;

  /*! SparrowHawk needs these to be members */
  double m_lateralError;
  double m_l1_gs; 
  double m_l2_gs;


  /*! FollowerLogger is used to evaluate performance of follower*/
  FollowerLogger* m_internalLogger;


  /*! Variable used to remember and keep old steering 
   * commands when reaching end of traj. 
   * Also used by SparrowHawk */
  double m_old_phi;

  /*! Variable used to remember and keep old steering 
   * commands when velocity on traj is zero. 
   * Also used by SparrowHawk */
  double m_old_phiFF;

  /*! Integral part */
  double m_I;


public:
  /*! Constructor for LateralController
   *
   * \param params lateral controller parameters
   * \param sparrowHawk pointer to sparrowHawk (will be removed)
   * \param m_internalLogger pointer to internalLogger
   */
  LateralController(LateralControllerParams* params, CSparrowHawk* sparrowHawk, 
                    FollowerLogger* internalLogger);

  /*! Method in which the control signal is calculated 
   *
   * \param adriveDirective pointer to which the resulting AdriveDirective is written
   * \param followerMergedDirective pointer to CTraj with timestamp
   * \param state pointer to vehicle state object
   *
   */
  void control(AdriveDirective* adriveDirective, 
               int* directivesToSend, FollowerMergedDirective* followerMergedDirective,
               SingleFrameVehicleState* vehicleState, ActuatorState* actuatorState);
};

#endif // LATERAL_CONTROLLER_HH_IRGBFBFWESHIFDLUHAIUHF
