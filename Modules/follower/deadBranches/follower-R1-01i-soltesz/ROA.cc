///
/// \file ROA.cc
/// \brief Reactive Obstacle Avoidance.
/// 
/// \Kristian Soltesz, Magnus Linderoth
/// \date 21 September 2007
/// 
/// ROA gets delay and lateral error information from follower,
/// together with front and rear center ladar scans. It uses
/// this information to shape the velocity reference in order 
/// to avoid collisions. 
/// 
/// This file is part of the follower module.

#include "ROA.hh"

// Constructor for ROA
ROA::ROA(FollowerControlStatus* controlStatus, FollowerLogger* internalLogger) :
  m_internalLogger(internalLogger),
  
m_lateralError(0),
  m_delay(0),
  m_time2Collision(0){
  m_timeStamps[0] = 1;
  m_ages[0]=2;
  sparrowInit();
}

void ROA::sparrowInit() {

  // Loop ages
  sparrowHawk->rebind("steering_loop_age", (int*)(&m_ages[STEERING_LOOP]));
  sparrowHawk->set_readonly("steering_loop_age");
  sparrowHawk->rebind("acceleration_loop_age", (int*)(&m_ages[ACCELERATION_LOOP]));
  sparrowHawk->set_readonly("acceleration_loop_age");
  //sparrowHawk->rebind("transmission_loop_age", (int*)(&m_ages[TRANSMISSION_LOOP]));
  //sparrowHawk->set_readonly("transmission_loop_age", (int*)(&m_ages[TRANSMISSION_LOOP]));

  // State ages
  sparrowHawk->rebind("steering_state_age", (int*)(&m_ages[STEERING_STATE]));
  sparrowHawk->set_readonly("steering_state_age");
  sparrowHawk->rebind("acceleration_state_age", (int*)(&m_ages[ACCELERATION_STATE]));
  sparrowHawk->set_readonly("acceleration_state_age");
  //sparrowHawk->rebind("transmission_state_age", (int*)(&m_ages[TRANSMISSION_STATE]));
  //sparrowHawk->set_readonly("transmission_state_age", (int*)(&m_ages[TRANSMISSION_STATE]));

  printConsole("ROA BOUND TO SPARROW" << endl);
}

// Initialize sensnet
int ROA::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;
    
  // Create sensnet itnerface
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, CmdArgs::sn_key, this->moduleId) != 0)
    return -1;

  // Default ladar set
  numSensorIds = 0;
  
  //Commented out the other LADARs so that we just listen to the middle bumper
  //sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RIEGL;
  //sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
    ladar = this->ladars + this->numLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (sensnet_join(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
      return -1;
  }
  
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // counters
  allClear = 0;
  numRoaCommands = 0;
  return 0;
}

/*****************************************************************************/

// Clean up sensnet
int ROA::finiSensnet()
{
  int i;
  Ladar *ladar;

  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;
    sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
  }
  sensnet_free(this->sensnet);
  
  return 0;
}

/*****************************************************************************/

void ROA::setTimeStamp(int index, unsigned long long timeStamp) {
  m_timeStamps[index] = timeStamp;
}

void ROA::updateAge(int index) {
  // [micro seconds]
  m_ages[index] = (DGCgettime()-m_timeStamps[index])/1000;
  m_internalLogger->roa_ages[index] = m_ages[index];
}

void ROA::updateAllAges() {
  for (int index = 0; index < NUMBER_OF_TIMES; index++) {
    updateAge(index);
    //printConsole(index << ": " << m_ages[index] << endl);
  }
  //printConsole(endl);
}


void ROA::updateLateralError(double lateralError) {
  // update the lateral error
  m_lateralError = lateralError;
}

void ROA::updateDelay() {
  // update the estimated delay
}

void ROA::updateTimeToCollide() {
  // update the estimated time to collide
}

void ROA::limitReferenceVelocity(double* referenceVelocity) {
  // this is the actual ROA function
}



