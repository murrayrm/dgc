///
/// \file ROA.hh
/// \brief Reactive Obstacle Avoidance.
/// 
/// \Kristian Soltesz, Magnus Linderoth
/// \date 21 September 2007
/// 
/// ROA gets delay and lateral error information from follower,
/// together with front and rear center ladar scans. It uses
/// this information to shape the velocity reference in order 
/// to avoid collisions. 
/// 
/// This file is part of the follower module.

#ifndef ROA_UJGGIOSDBUYCBIUSDGNBDSHGIUCBUIGDJK
#define ROA_UJGGIOSDBUYCBIUSDGNBDSHGIUCBUIGDJK

//using namespace std;

#include "FollowerUtils.hh"
#include "FollowerLogger.hh"
#include <assert.h>

// Sensnet/Skynet support
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include <skynettalker/SkynetTalker.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>

class ROA : public CMapElementTalker
{
public:
  enum TIMES {
    STEERING_LOOP = 0, 
    ACCELERATION_LOOP, 
    TRANSMISSION_LOOP, // Not very interesting
    STEERING_STATE,
    ACCELERATION_STATE,
    TRANSMISSION_STATE,
    TRAJECTORY,    
    VEHICLE_STATE, 
    NUMBER_OF_TIMES
  };

protected:
  /// FollowerLogger is used to evaluate performance of follower
  FollowerLogger* m_internalLogger;

  // Vector of time stamps used for calculating delays
  unsigned long long m_timeStamps[NUMBER_OF_TIMES];

  // Ages (of time stamps) used for calculating delays
  unsigned long  m_ages[NUMBER_OF_TIMES];

  /// Error used by ROA function
  double m_lateralError;
  
  /// Delay used by ROA function
  unsigned long m_delay;

  /// Estimated time to collission
  unsigned long m_time2Collision;


  // Spread settings
  char *spreadDaemon;
  modulename moduleId;
 
  // Sensnet module
  sensnet_t *sensnet;
 
  // ROA vector of obsacles;
  vector< vector<double> > old_ptvec;

  // ROA counter for allClear
  int allClear;
  
  // Number of ROA activations
  int numRoaCommands;

  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;
  Ladar ladars[16];

  // Previous scan time stamp
  unsigned long long previous_timestamp;

  
  /// Binds variables to sparrowHawk display
  void sparrowInit();

  /// Initialize sensnet
  int initSensnet();

  /// Clean up sensnet
  int finiSensnet();

  /// updates the estimated delay
  void updateDelay();

  /// Updates the estimeated time2Collision
  void updateTimeToCollide();

  
public:
  ROA(FollowerControlStatus* controlStatus, FollowerLogger* internalLogger);  

  void setTimeStamp(int index, unsigned long long timeStamp);
  void updateAge(int index);
  void updateAllAges();
  

  /// Invoked from the lateral controller. 
  /// Updates the lateral error used in the ROA module
  void updateLateralError(double lateralError);

  /// The actual ROA function
  void limitReferenceVelocity(double* referenceVelocity);

};

#endif //ROA_UJGGIOSDBUYCBIUSDGNBDSHGIUCBUIGDJK

