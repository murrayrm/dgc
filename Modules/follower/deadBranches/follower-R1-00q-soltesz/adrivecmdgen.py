#adrivecmdgen.py
#author: Kristian Soltesz
#date: 2007-06-28

import sys, os, getopt 

#display help msg
print '''
                *** adrivecmdgen.py ***
---------------------------------------------------------------
Options:

         -h      --help                  prints this message
         -m      --mfile         file    input m-file (omit .m)
         -t      --tmplog        file    save tmplog to file
         -l      --log           file    save log to file

---------------------------------------------------------------
Note:   The m-file should contain the following declarations:

          steer=[time1;steercmd(time1)];
          acceleration=[time2;accelerationcmd(time2)];

        where time1, time2, steercmd, accelerationcmd are 
        row vectors. Time is given in micro seconds.  
---------------------------------------------------------------
'''

#parse cmd line opts using getopt
try:
  opts, args = getopt.getopt(sys.argv[1:], "hm:t:l:s", ["help", "mfile=", "tmplog=", "log=", "simulate"])
except getopt.GetoptError:
  print 'Error: Wrong cmd line opt syntax.'
  print 'Exiting...\n'      
  sys.exit(2)

#defaults
mfile = None
tmplog = None
keeptmp = None
log = None
exit = False

#run through parsed cmd line opts
for o, a in opts:
    if o in ("-h", "--help"):
        exit = True
    if o in ("-m", "--mfile"):
        mfile = a
    if o in ("-t", "--tmplog"):
        tmplog = a
        keeptmp = True
    if o in ("-l", "--log"):
        log = a

#catch errors
if (mfile == None):
  print 'Error:   -m      --mfile         file    not specified.' 
  exit = True
if (tmplog == None):
  tmplog = 'tmplog'
  keeptmp = False
if (log == None):
  print 'Error:   -l      --log           file    not specified.'
  exit = True

if (exit):
  print 'Exiting...\n'
  sys.exit(2)

#run matlab script which outputs 'meta' skynet logfile
os.popen('xterm -e matlab -nojvm -nosplash -r \
 "run '+mfile+', \
  adrivecmdgen(steer,acceleration,\''+tmplog+'\'), \
  exit"')

#run C++ program which converts 'meta' skynet log into log
os.popen('../../bin/Drun adriveloggen '+tmplog+' '+log)  

#should the 'meta' log be kept (for debugging)?
if (not keeptmp):
  os.remove(tmplog)
