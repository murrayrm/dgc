% Drives forwards, turns left 90 degrees and continues forward
speed = 2;   % m/s
dist1 = 20;   % m
radius = 10; % m
dist2 = 15;   % m

n = 30; % # points in the curve


T1 = zeros(dist1 + 1, 6);
T1(:,5) = speed * ones(dist1 + 1, 1);
T1(:,4) = [0:dist1]';

x = pi/2*[1:n-1]'/n;

T2 = zeros(n-1, 6);
T2(:,1) = radius*(1 - cos(x));
T2(:,4) = dist1 + radius*sin(x);
T2(:,2) = speed*sin(x);
T2(:,5) = speed*cos(x);
T2(:,3) = speed^2/radius*cos(x);
T2(:,6) = -speed^2/radius*sin(x);



T3 = zeros(dist2 + 1, 6);
T3(:,4) = (dist1 + radius)*ones(dist2 + 1, 1);
T3(:,2) = speed * ones(dist2 + 1, 1);
T3(:,1) = [radius: radius + dist2]';

T = [T1; T2; T3];


save leftTurn10m_2mps.traj T -ASCII