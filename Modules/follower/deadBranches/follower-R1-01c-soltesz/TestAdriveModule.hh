#include "gcinterfaces/AdriveCommand.hh"

class TestAdriveModule : public GcModule
{

public:

  TestAdriveModule(int skynet_key);

  /* Arbitrate will create and send responses to Follower */
  void arbitrate(ControlStatus *, MergedDirective *);

  void control(ControlStatus *, MergedDirective *);


  /* Utility function for generating any number of adrive fault combinations */
  void generateAdriveFaultCombinations();

  void startModule();

  ControlStatus m_testStatus;		// unused
  MergedDirective m_testDirective;	// unused

private:

  AdriveCommand::Northface *m_followerInterfaceNF;

  GcInterfaceDirectiveStatus::Status getStatusFromInteger(int someInt);
  AdriveFailure getReasonFromInteger(int someInt);
};
