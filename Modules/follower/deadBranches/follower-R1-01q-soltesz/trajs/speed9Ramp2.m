t = 0.1;

% Ramps up to speed stays there and ramps down again
posAcc = 2;     % m/s^2
maxSpeed = 9;   % m/s
constTime = 4; % s
negAcc = -2;     % m/s^2

% acc
a = [posAcc*ones(maxSpeed/posAcc/t,1);
    zeros(constTime/t,1);
    negAcc*ones(-maxSpeed/negAcc/t,1);
    ];
v = t*cumsum(a);
p = t*cumsum(v);

T = [zeros(length(a),3) p v a];

while T(end,1)==T(end-1,1) && T(end,4)==T(end-1,4)
    T = T(1:end-1,:);
end

dist = p(end)

save speed9Ramp2.traj T -ASCII