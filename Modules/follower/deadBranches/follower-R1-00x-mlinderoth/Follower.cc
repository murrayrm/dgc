///
/// \file Follower.cc
/// \brief Trajectory following program using closed loop controllers.
///
/// \author Nok Wongpiromsarn, Kristian Soltesz, Magnus Linderoth
/// \date 10 July 2007
///
/// Follower is the program ensuring that Alice follows trajectories 
/// generated by the planner. It is a replacement for the old 
/// trajfollower program, rewritten to be compatible with the 
/// canonical software architecture.
///
/// Note: once follower is up and running, the old trajfollower will be
/// removed and this program will replace it.
/// This file is part of the follower module.


#include "Follower.hh"

// Size of the south face Q
// used in communication with
// gcdrive
#define SF_Q_SIZE 20

using namespace std;

Follower::Follower(FollowerControlStatus* controlStatus, 
                   ControllerParams* controllerParams, FollowerMergedDirective* mergedDirective) :
  CSkynetContainer(SNtrajfollower, CmdArgs::sn_key),
  CStateClient(true),
  GcModule("Follower", controlStatus, mergedDirective),
  m_transmissionDirective(),
  m_accelerationDirective(),
  m_steeringDirective(),
  m_sendTransmission(false),
  m_sendSteering(false),
  m_sendAcceleration(false),
  m_adriveDirectiveID(0),
  m_timeOfLastTraj(0),
  m_trajectoryDirection(1),
  m_recvdTraj(0),
  m_internalLogger(),
  m_transmissionController(),
  m_longitudinalController(controllerParams->longitudinalControllerParams, &m_internalLogger),
  m_lateralController(controllerParams->lateralControllerParams, &m_internalLogger)
{
  m_transmissionDirective.actuator = Transmission;
  m_accelerationDirective.actuator = Acceleration;
  m_steeringDirective.actuator = Steering;
  m_transmissionDirective.command = SetPosition;
  m_steeringDirective.command = SetPosition;
  m_accelerationDirective.command = SetPosition;
  
  m_followerResponseNF = FollowerCommand::generateNorthface(CmdArgs::sn_key, this);
  m_adriveCommandSF = AdriveCommand::generateSouthface(CmdArgs::sn_key, this);

  // Initialize the transmission controller
  // to the current gear.
  // If Alice is shifting, 
  // wait for shifting to complete
  // before calling initialization
  // FIXME: what if transmission starts while 
  //        transmission controller initializes?
  UpdateActuatorState();
  m_transmissionController.initialize(m_actuatorState.m_transpos);

  if (CmdArgs::log_level > 0) {
    stringstream ssfilename("");
    ssfilename << CmdArgs::log_path  << "/follower";
    this->addLogfile(ssfilename.str());
    this->setLogLevel(CmdArgs::log_level);
  }
  this->setLogLevel(CmdArgs::log_level);

  // Not limiting the Q size, slows down the program 
  // critically.
  m_adriveCommandSF->setStaleThreshold(SF_Q_SIZE);
  
  // Trajectory receiving mutex. Ensures that
  // trajectories are not changed in
  // the middle of control loop.
  DGCcreateMutex(&m_recvdTrajMutex);

  // Add information to the 
  // sparrow display.
  sparrowHawk->set_notify("sparrowQuit", this, &Follower::sparrowQuit);
  sparrowHawk->set_keymap((int)'q', this, &Follower::sparrowQuit);
  sparrowHawk->rebind("sh_trajDir", &m_trajectoryDirection);
  sparrowHawk->set_readonly("sh_trajDir");
  sparrowHawk->rebind("state", &(controlStatus->state));
  sparrowHawk->rebind("send_steering", &m_sendSteering);
  sparrowHawk->set_readonly("send_steering");
  sparrowHawk->rebind("send_acceleration", &m_sendAcceleration);
  sparrowHawk->set_readonly("send_acceleration");
  sparrowHawk->rebind("send_transmission", &m_sendTransmission);
  sparrowHawk->set_readonly("send_transmission");
  sparrowHawk->rebind("directive_id", &m_adriveDirectiveID);
  sparrowHawk->set_readonly("directive_id");

  // Start the SparrowHawk display if not 
  // disabled at the command line 
  if (!CmdArgs::disable_console) {
    sparrowHawk->run();
  }
}

// The destructor frees the 
// NF, SF, and deletes the
// trajectory receiving mutex.
Follower::~Follower() 
{
  AdriveCommand::releaseSouthface( m_adriveCommandSF );
  FollowerCommand::releaseNorthface( m_followerResponseNF );
  DGCdeleteMutex(&m_recvdTrajMutex);
}

// Arbitrate function of Follower. 
//
// First the arbitrater handles control status from the controllers,
// generated during the previous control cycle.
// These responses are sent through the NF to planner
//
// Next, responses from the gcdrive SF are read and handled.
//
// Finally, a merged directive is populated, if a new trajectory
// has been received.
void Follower::arbitrate(ControlStatus* cs, MergedDirective* md)
{

  FollowerControlStatus* controlStatus = (FollowerControlStatus*)cs;
  FollowerMergedDirective* mergedDirective = (FollowerMergedDirective*)md;

  // Responses from gcdrive are read off the SF Q
  // until the Q is emptied. 
  // Responses are handled. 
  pumpPorts();
  
  
  // Reset the actuator failures
  // Rejects are reset when completion from 
  // the corresponding actuators arrive
  controlStatus->state &= ~(FollowerState::SteeringRejected |
                             FollowerState::ThrottleRejected |
                             FollowerState::BrakeRejected |
                             // FIXME: TransmissionRejected is an exception for now
                             FollowerState::SteeringFailure  | 
			     FollowerState::ThrottleFailure  | 
			     FollowerState::BrakeFailure  | 
			     FollowerState::TransmissionFailure);
  
  while (m_adriveCommandSF->haveNewStatus()){
    
    m_adriveResponse = (AdriveResponse)*(m_adriveCommandSF->getLatestStatus());
      
    AdriveDirective directive;
    bool foundDirective = matchResponse2Directive(&directive); 
   
    if (!foundDirective) {
      // Directive corresponding to response was not in list
    }
    else if (directive.actuator == Acceleration && directive.arg > 0.0) {
      Follower::handleThrottleResponse(controlStatus);
    } 
    else if (directive.actuator == Acceleration && directive.arg < 0.0) {
      Follower::handleBrakeResponse(controlStatus);
    }
    else if (directive.actuator == Transmission) {
      Follower::handleTransmissionResponse(controlStatus);
    } 
    else if (directive.actuator == Steering) {
      Follower::handleSteeringResponse(controlStatus);
    } 
   
  }    

  // Check if the control status
  // should be written to a response
  // and propagated to planner.
  m_response.id = controlStatus->id;
  m_response.reason = controlStatus->state;

  if (controlStatus->state & 
      (FollowerState::LateralMaxError |
       FollowerState::LongitudinalMaxError |
       FollowerState::SteeringFailure |
       FollowerState::ThrottleFailure |
       FollowerState::BrakeFailure |
       FollowerState::TransmissionFailure)) {
    
    // A failure occured in processing the directive; return a response
    m_response.status = GcInterfaceDirectiveStatus::FAILED;
    sendResponse();
    
    // Write the response to the gclog.
    gclog(1) << m_response.toString() << endl;

    //Want to send completion of trajectory (relevant in UTurn, Stop lines, ...) 
  } else if (FollowerState::TrajectoryEnd & controlStatus->state) {    
    m_response.status = GcInterfaceDirectiveStatus::COMPLETED;  
    sendResponse();
  }

  // Compute the next mergedDirective
  //
  // This code determines whether or not there is a new directive to
  // be sent to the execution unit.  We use the directive ID to keep
  // track of whether we have pass something new down.  Note
  // that the ID for the mergedDirective is just a counter. We
  // keep track of the parent ID separately.
  
  // Mutex is unlocked in the end of 
  // the control loop
  DGClockMutex(&m_recvdTrajMutex);
  
  // This is where the trajectory is locally updated
  if (m_recvdTraj != NULL && m_recvdTraj != mergedDirective->traj) {
    mergedDirective->traj = m_recvdTraj;
    mergedDirective->id++;
    controlStatus->state &= ~(FollowerState::TrajectoryTimeout |
                              FollowerState::LateralMaxError |
                              FollowerState::LongitudinalMaxError);
  }
}

  // Control function of follower
  //
  // Control for Follower.  The artibiter receives merged
  // directives from the Follower arbitrator.  This is
  // just the latest traj.
  //
void Follower::control(ControlStatus *cs, MergedDirective *md)
{
  FollowerControlStatus* controlStatus = (FollowerControlStatus*)cs;
  FollowerMergedDirective* mergedDirective = (FollowerMergedDirective*)md; 
  SingleFrameVehicleState vehicleState;

  // Update VehicleState.
  // The vehicle state is
  // needed by the controllers
  // to compute error signals
  UpdateState();
  
  // Extract vehicle state in selected coordinate frame
  // FIXME: Magnus, is this needed anymore?
  //        Should eventually be removed
  extractFrameState(&m_state, &vehicleState);

  // Reads the current Estop State 
  // and populates the FollowerOpState
  // with it.
  // 
  // Since Estop state  is a sensed variable
  // it is OK to obtain itlike this
  // rather than extracting it from 
  // responses sent by gcdrive
  UpdateActuatorState();
  switch (m_actuatorState.m_estoppos) {  // {EstopDisable, EstopPause, EstopRun}
    
  case EstopDisable:
    controlStatus->state &= ~FollowerState::EstopPause;
    controlStatus->state |= FollowerState::EstopDisable;
    break;

  case EstopPause:
    controlStatus->state &= ~FollowerState::EstopDisable;
    controlStatus->state |= FollowerState::EstopPause;
    break;

  case EstopRun:
    controlStatus->state &= ~FollowerState::EstopDisable;
    controlStatus->state &= ~FollowerState::EstopPause;

  }

  // Extract trajectory poiter from merged directive
  CTraj* traj = mergedDirective->traj;  

  // If we have not received a new trajectory for a while, we fail
  // and put ourselves in pause. Pause is released when a new 
  // trajectory is received.
  if (getTimeSinceLastTraj() > CmdArgs::trajReceiveTimeout && CmdArgs::traj_file == NULL) {
    controlStatus->state |= FollowerState::TrajectoryTimeout;
  }

  // If the trajectory pointer is NULL
  // or if the trajectory has no points
  // the follower should pause
  if (traj == NULL || traj->getNumPoints() == 0) {
    controlStatus->state |= FollowerState::TrajectoryError; 
    if (CmdArgs::verbose) printConsole("Follower::control(ControlStatus*, MergedDirective): " 
                                      << " NULL or empty trajectory." << endl);    
  
  } else {

    controlStatus->state &= ~ FollowerState::TrajectoryError;

    // The trajectory direction is updated
    m_trajectoryDirection = traj->getDirection();    
  
    // Invoke LongitudinalController
    m_sendAcceleration = m_longitudinalController.control(controlStatus, &m_accelerationDirective, traj, 
                                                          &vehicleState, m_trajectoryDirection);
    
    // Invoke LateralController
    m_sendSteering = m_lateralController.control(controlStatus, &m_steeringDirective, traj, 
                                                     &vehicleState, m_trajectoryDirection);
    // Invoke TransmissionController
    m_sendTransmission = m_transmissionController.control(controlStatus, &m_transmissionDirective, m_trajectoryDirection);
  }
  
  // The below if blocks are where directives are sent 
  // to gcdrive. They also take care of updating
  // the ID associated with the sent directives.
  if (m_sendTransmission) {
    m_transmissionDirective.id = ++m_adriveDirectiveID;    
    m_adriveCommandSF->sendDirective(&m_transmissionDirective);
  }

  if (m_sendSteering) {
    m_steeringDirective.id = ++m_adriveDirectiveID;
    m_adriveCommandSF->sendDirective(&m_steeringDirective);
    m_adriveDirectiveList.push_back(m_steeringDirective);
  }

  if (m_sendAcceleration) {
    m_accelerationDirective.id = ++m_adriveDirectiveID;
    m_adriveCommandSF->sendDirective(&m_accelerationDirective);
    m_adriveDirectiveList.push_back(m_accelerationDirective);
  }

  DGCunlockMutex(&m_recvdTrajMutex);
  
  // Do internal logging
  if (CmdArgs::log_internal) {
    DGCgettime(m_internalLogger.timeStamp);
    m_internalLogger.trajID = mergedDirective->id;
    m_internalLogger.curN = vehicleState.x;
    m_internalLogger.curNd = vehicleState.xVel;
    m_internalLogger.curE = vehicleState.y;
    m_internalLogger.curEd = vehicleState.yVel;
    m_internalLogger.curYaw = vehicleState.yaw;
    m_internalLogger.curYawRate = vehicleState.yawRate;
  }

  // Do internal logging
  if (CmdArgs::log_internal) {

    // Write to the log file
    m_internalLogger.writeLog();
  }
}

void Follower::sendResponse()
{
  m_followerResponseNF->sendResponse(&m_response, m_response.id);
}

//FIXME: Magnus, is it safe to remove this now?
void Follower::extractFrameState(VehicleState* vehicleState, SingleFrameVehicleState* frameState)
{
  if (strcmp(CmdArgs::frame, "local") == 0) {
    frameState->x = vehicleState->localX;
    frameState->xVel = vehicleState->localXVel;
    frameState->y = vehicleState->localY;
    frameState->yVel = vehicleState->localYVel;
    frameState->yaw = vehicleState->localYaw;
    frameState->yawRate = vehicleState->localYawRate;
    frameState->pitch = vehicleState->localPitch;
  } else if (strcmp(CmdArgs::frame, "localSim") == 0) {
    frameState->x = vehicleState->localX;
    frameState->xVel = vehicleState->utmNorthVel;
    frameState->y = vehicleState->localY;
    frameState->yVel = vehicleState->utmEastVel;
    frameState->yaw = vehicleState->localYaw;
    frameState->yawRate = vehicleState->utmYawRate;
    frameState->pitch = vehicleState->localPitch;
  } else if (strcmp(CmdArgs::frame, "UTM") == 0) {
    frameState->x = vehicleState->utmNorthing;
    frameState->xVel = vehicleState->utmNorthVel;
    frameState->y = vehicleState->utmEasting;
    frameState->yVel = vehicleState->utmEastVel;
    frameState->yaw = vehicleState->utmYaw;
    frameState->yawRate = vehicleState->utmYawRate;
    frameState->pitch = vehicleState->utmPitch;
  } else {
    if (CmdArgs::verbose) printConsole("Follower::extractFrameState(VehicleState*, SingleFrameVehicleState*):" <<
                                       " Unrecognized coordinate frame" << CmdArgs::frame << endl);
  }
  frameState->speed = sqrt(frameState->xVel*frameState->xVel + frameState->yVel*frameState->yVel);
}

// Trajectory-receiving thread
void Follower::CommThread ()
{
  int trajSocket;
  trajSocket = m_skynet.listen(SNtraj, SNplanner);
  CTraj* newTraj = new CTraj(3);
  CTraj* tmpTraj;

  while (!quit) {
    
    bool trajReceived = RecvTraj (trajSocket, newTraj);
    while (m_skynet.is_msg(trajSocket)) {
      trajReceived = RecvTraj(trajSocket, newTraj);
    }

    // If a new trajectory is received
    // trajectory error is cleared
    // A time stamp is also set
    // so we can keep track of 
    // how long it was since a trajectory
    // was last received.
    if (trajReceived) {
      DGCgettime(m_timeOfLastTraj);
      if (CmdArgs::verbose >=3) printConsole("Follower::CommThread():" <<
                                             " Trajectory received at: " << m_timeOfLastTraj << endl);
      DGClockMutex(&m_recvdTrajMutex);
      if (m_recvdTraj == NULL)
	m_recvdTraj = new CTraj(3);
      tmpTraj = newTraj;
      newTraj = m_recvdTraj;
      m_recvdTraj = tmpTraj;
      DGCunlockMutex(&m_recvdTrajMutex);
    }
    else {
      if (CmdArgs::verbose) printConsole("Follower::CommThread(): " <<
                                         " error receiving traj" << endl);
    }
  }
  delete newTraj;
}

// Broadcasts the trajectory in order to make it visible for others
//FIXME: is this the right way to do it?
void Follower::broadcastTraj(CTraj* traj)
{
  int trajSocket = m_skynet.get_send_sock(SNtraj);
  SendTraj (trajSocket, traj);
}

void Follower::sparrowQuit() {
  quit = true;
}

unsigned long Follower::getTimeSinceLastTraj() {
  return DGCgettime()-m_timeOfLastTraj;
}

bool Follower::matchResponse2Directive(AdriveDirective* directive) {

  // First we check if the response
  // corresponds to the previous 
  // transmission directive.
  if (m_adriveResponse.id == m_transmissionDirective.id) {
    *directive = m_transmissionDirective;
    return true;
  }
  
  // If it is not a transmission response,
  // we iterate through the list of 
  // issued acceleration and steering
  // directives, searching for the 
  // one corresponding to the response.
  list<AdriveDirective>::iterator pos;
  for (pos = m_adriveDirectiveList.begin(); pos != m_adriveDirectiveList.end(); pos++) {
    if ((*pos).id == m_adriveResponse.id) {
      *directive = *pos;
      m_adriveDirectiveList.erase(pos);
      return true;
    }
  }
  return false;
}


void Follower::handleTransmissionResponse(FollowerControlStatus* controlStatus) {
 switch (m_adriveResponse.status) {
        
 case AdriveResponse::REJECTED:
   controlStatus->state |= FollowerState::TransmissionRejected;
   m_transmissionController.failedShifting();
   break;
   
 case AdriveResponse::FAILED:
   // FIXME: Here nothing else shall be done, right?
   controlStatus->state |= FollowerState::TransmissionFailure;
   m_transmissionController.failedShifting();
   break;
   
 case AdriveResponse::COMPLETED:
   controlStatus->state &= ~(FollowerState::TransmissionPending |
                             FollowerState::TransmissionRejected);
   m_transmissionController.completedShifting((int)m_transmissionDirective.arg);
   break;
   
 case AdriveResponse::ACCEPTED:
   break;
   
 default:
   break; 
 }
}


void Follower::handleBrakeResponse(FollowerControlStatus* controlStatus) {
  switch (m_adriveResponse.status) {
        
  case AdriveResponse::REJECTED:
    controlStatus->state |= FollowerState::BrakeRejected;
    break;
        
  case AdriveResponse::FAILED:
    controlStatus->state |= FollowerState::BrakeFailure;
    break;
    
  case AdriveResponse::COMPLETED:
    // FIXME: this cannot be used right away to reset fail / reject
    break;
    
  case AdriveResponse::ACCEPTED:
    break;
  default:
    break;
  }
}


void Follower::handleThrottleResponse(FollowerControlStatus* controlStatus) {
  switch (m_adriveResponse.status) {
        
  case AdriveResponse::REJECTED:
    controlStatus->state |= FollowerState::ThrottleRejected;
    break;
        
  case AdriveResponse::FAILED:
    controlStatus->state |= FollowerState::ThrottleFailure;
    break;
        
  case AdriveResponse::COMPLETED:
    // FIXME: this cannot be used right away to reset fail / reject
    break;
        
  case AdriveResponse::ACCEPTED:
    break;
  default:
    break;
  }
}

void Follower::handleSteeringResponse(FollowerControlStatus* controlStatus) {
  switch (m_adriveResponse.status) {
        
  case AdriveResponse::REJECTED:
    controlStatus->state |= FollowerState::SteeringRejected;
    break;
        
  case AdriveResponse::FAILED:
    controlStatus->state |= FollowerState::SteeringFailure;
    break;
        
  case AdriveResponse::COMPLETED:
    // FIXME: this cannot be used right away to reset fail / reject
    break;
      
  case AdriveResponse::ACCEPTED:
    break;
  default:
    break; 
  }
}
