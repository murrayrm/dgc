///
/// \file LongitudinalController.cc
/// \brief Takes care of the longitudinal part in trajectory following.
///
/// \author Kristian Soltesz, Magnus Linderoth
/// \date 10 July 2007
///
/// This file is part of the follower module.

#include "alice/AliceConstants.h"
#include "LongitudinalController.hh"
#include <algorithm>

// Constructor for LongitudinalController
LongitudinalController::LongitudinalController(LongitudinalControllerParams* params, FollowerLogger* internalLogger):
  m_params(*params),
  m_internalLogger(internalLogger),
  m_I(0)
{
  // Send state pointers to SparrowHawk display
  sparrowHawk->rebind("lo_error", &m_vErr);
  sparrowHawk->set_readonly("lo_error");
  sparrowHawk->rebind("lo_I", &m_I);
  sparrowHawk->rebind("lo_vR", &m_vR);
  sparrowHawk->set_readonly("lo_vR");
  sparrowHawk->rebind("lo_vel", &m_vel);
  sparrowHawk->set_readonly("lo_vel");
  sparrowHawk->rebind("lo_a", &m_a);
  sparrowHawk->set_readonly("lo_a");
  sparrowHawk->rebind("lo_aFF", &m_aFF);
  sparrowHawk->set_readonly("lo_aFF");
  sparrowHawk->rebind("lo_aPitch", &m_aPitch);
  sparrowHawk->set_readonly("lo_aPitch");
  sparrowHawk->rebind("lo_u", &m_u);
  sparrowHawk->set_readonly("lo_u");

  // Send param pointers to SparrowHawk display
  sparrowHawk->rebind("lo_lV", &m_params.lV);
  sparrowHawk->rebind("lo_lI", &m_params.lI);
  sparrowHawk->rebind("lo_pitchGain", &m_params.pitchGain);
  sparrowHawk->rebind("lo_pitchTime", &m_params.pitchTime);
  sparrowHawk->rebind("lo_alpha", &m_params.alpha);
  sparrowHawk->rebind("lo_delay", &m_params.delay);
  sparrowHawk->rebind("lo_replanErrorAbs", &m_params.replanErrorAbs);
  sparrowHawk->rebind("lo_replanErrorRel", &m_params.replanErrorRel);
}

// Method for calculating control signal
bool LongitudinalController::control(FollowerControlStatus* controlStatus, AdriveDirective* accelerationDirective,
                                     CTraj* traj,
                                     SingleFrameVehicleState* vehicleState, int trajectoryDirection) {

  // Calculate feed forward point
  m_vel = vehicleState->speed;
  double ffDist = m_vel * m_params.delay;

  // Current vehicle state of the Alice
  double xCR = vehicleState->x;
  double yCR = vehicleState->y;
  if (CmdArgs::verbose >= 2) printConsole("Vehicle state: x=" << xCR << " y=" << yCR << endl);

  // Determine the reference and feed forward points on the traj by linear interpolation
  int closestIndex;
  double closestFractionToNext;
  TrajPoint refPoint = traj->interpolGetClosest(xCR, yCR, &closestIndex, &closestFractionToNext); 
  TrajPoint ffPoint = traj->interpolGetPointAhead(closestIndex, closestFractionToNext, ffDist, NULL, NULL);

  // Feed forward acceleration. Avoid division by zero.
  if (ffPoint.nd == 0 && ffPoint.ed == 0) {

    // When velocity == 0, then lateral acceleration == 0             \
    // Velocity on traj should never be opposite of driving direction / ==> 
    // all acceleration is in forward direction
    m_aFF = sqrt(ffPoint.ndd*ffPoint.ndd + ffPoint.edd*ffPoint.edd);
  } else {
    m_aFF = (ffPoint.nd*ffPoint.ndd + ffPoint.ed*ffPoint.edd) /
           sqrt(ffPoint.nd*ffPoint.nd + ffPoint.ed*ffPoint.ed);   
  }

  /// Pitch feed forward
  // Update pitch estimate
  m_pitch = m_pitch + (vehicleState->pitch - m_pitch) / m_params.pitchTime / CmdArgs::rate;
  m_aPitch = trajectoryDirection * sin(m_pitch) * VEHICLE_MASS * 9.81;
  

  double u;
  if (CmdArgs::ffOnly) {
      u = linearize(m_params.alpha*m_aFF + m_params.pitchGain*m_aPitch, m_vel, &m_params);
  } else {
    // Reference speed
    m_vR = sqrt(refPoint.nd*refPoint.nd + refPoint.ed*refPoint.ed);
    
    // FIXME: Detect end of trajectroy
    //        in a much more robust way.
    //        Maybe move this to a separate function.
    if (closestIndex == traj->getNumPoints() - 1) {
      controlStatus->state |=  FollowerState::TrajectoryEnd;
    } else {
      controlStatus->state &= ~FollowerState::TrajectoryEnd;
    }

    // Speed error
    m_vErr = vehicleState->speed - m_vR;
    if (CmdArgs::verbose >= 2) printConsole("vR=" << m_vR << " vErr=" << m_vErr << " speed=" << vehicleState->speed << endl);

    // Check if we need a replan
    // This should not affect
    // actions taken by the follower
    // FIXME: Think really hard. 
    //        Should this be used at all?
    if (fabs(m_vErr) >= m_params.replanErrorAbs && fabs(m_vErr) >= m_params.replanErrorRel * m_vel) {
      controlStatus->state |= FollowerState::LongitudinalMaxError;
      // FIXME: Should we reset integral here?
    } else {
      controlStatus->state &=~FollowerState::LongitudinalMaxError;
    }

    bool updateIntegral = (controlStatus->state == FollowerState::Running);
    
    if (updateIntegral) {
      m_I -= m_vErr / m_params.lI / CmdArgs::rate;
    }
    else if (controlStatus->state & (FollowerState::EstopPause | 
                                     FollowerState::EstopDisable)) {
      m_I = 0.0;
    }

    // Limit integral
    if (m_I > m_params.maxI) {
      m_I = m_params.maxI;
    } else if (m_I < -m_params.maxI) {
      m_I = -m_params.maxI;
    }

    // Calculate control signal
    m_a = -m_params.lV*m_vErr + m_params.alpha*m_aFF + m_params.pitchGain*m_aPitch + m_I;
    u = linearize(m_a, vehicleState->speed, &m_params);
    while (u > 1.0 && updateIntegral) {
      m_I -= 0.1;
      m_a = -m_params.lV*m_vErr + m_params.alpha*m_aFF + m_params.pitchGain*m_aPitch + m_I;
      u = linearize(m_a, vehicleState->speed, &m_params);
    }
    while (u < -1.0 && updateIntegral) {
      m_I += 0.1;
      m_a = -m_params.lV*m_vErr + m_params.alpha*m_aFF + m_params.pitchGain*m_aPitch + m_I;
      u = linearize(m_a, vehicleState->speed, &m_params);
    }

    if (CmdArgs::verbose >= 2) printConsole("m_I=" << m_I << " a=" << m_a << ", u=" << u << endl);
  }

  // u must be in [-1,1]. This truncations is added only for robustness. 
  // *Should* never be needed.
  u = max(u,-1.0);
  u = min(u,1.0);

  // FIXME: Maybe make one if out of the below if:s?


  // These are states leading
  // to pause braking.
  // There's some redundancy here,
  // since gcdrive brakes automatically
  // at some failures
  if (controlStatus->state & (FollowerState::TrajectoryTimeout |
                              FollowerState::TrajectoryError | 
                              FollowerState::EstopPause |
                              FollowerState::EstopDisable |
                              FollowerState::TransmissionPending |
                              FollowerState::SteeringRejected | // FIXME: is this what we want?
                              FollowerState::BrakeRejected | // FIXME: is this what we wnat?
                              FollowerState::TrajectoryEnd |
                              FollowerState:: SteeringFailure |
                              FollowerState::BrakeFailure)) {
    u = -0.7; // FIXME: magic number (PAUSE_BRAKING)
  }


  // This is to avoid stopping beyond
  // stop lines.
  if (m_vR < 0.2) { // FIXME: magic number
    u = -1.0; //FIXME: magic number (STOP_BRAKING)
  }

  m_u = u;
  accelerationDirective->arg = u;

  // Write data to logger object
  if (CmdArgs::log_internal) {

    // Variables
    m_internalLogger->lo_vRef = m_vR;
    m_internalLogger->lo_aFF = m_aFF;
    m_internalLogger->lo_speed = vehicleState->speed;
    m_internalLogger->lo_I = m_I;
    m_internalLogger->lo_a = m_a;
    m_internalLogger->lo_u = u;

    // Parameters
    m_internalLogger->lo_lV = m_params.lV;
    m_internalLogger->lo_lI = m_params.lI;
    m_internalLogger->lo_alpha = m_params.alpha;
    m_internalLogger->lo_delay = m_params.delay;
    m_internalLogger->lo_gasDeadU = m_params.gasDeadU;
    m_internalLogger->lo_gasDeadA = m_params.gasDeadA;
    m_internalLogger->lo_brakeDeadU = m_params.brakeDeadU;
    m_internalLogger->lo_brakeDeadA = m_params.brakeDeadA;
    m_internalLogger->lo_gasGain = m_params.gasGain;
    m_internalLogger->lo_brakeGain = m_params.brakeGain;
    m_internalLogger->lo_gasVelocityGain = m_params.gasVelocityGain;
    m_internalLogger->lo_brakeVelocityGain = m_params.brakeVelocityGain;
    m_internalLogger->lo_noGasSpeed = m_params.noGasSpeed;
    m_internalLogger->lo_idleAcceleration = m_params.idleAcceleration;
  }

  return true;
}


double LongitudinalController::linearize(double a, double v, LongitudinalControllerParams* params)
{
  double u;

  if (a <= params->brakeDeadA || a <= params->brakeDeadA - (v-params->noGasSpeed)*params->brakeVelocityGain){
    
    // Braking 
    u = (a - params->brakeDeadA)/params->brakeGain + params->brakeDeadU;
    
    // Actual braking varies at low speeds
    if (v < params->noGasSpeed) {
      u += params->brakeVelocityGain*(v-params->noGasSpeed)/params->brakeGain;
    }
  } else if (a >= params->gasDeadA && a >= params->gasDeadA + params->idleAcceleration - v*params->gasVelocityGain) {
    
    // Gassing
    u = (a - params->idleAcceleration + params->gasVelocityGain*v) / params->gasGain + params->gasDeadU;

  } else {
    
    // Dead band 
    if (v < params->noGasSpeed || v < params->idleAcceleration/params->gasVelocityGain) {
    
      // Running slowly
      // Interpolate between brakeDeadU and gasDeadU
      double brakeA = params->brakeDeadA;
      if (v < params->noGasSpeed) {
        brakeA -= params->brakeVelocityGain*(v-params->noGasSpeed);
      }
      double gasA = params->idleAcceleration - params->gasVelocityGain*v;
      u = params->brakeDeadU + (params->gasDeadU - params->brakeDeadU)*(a - brakeA)/(gasA - brakeA);
    } else {
      
      // Running faster
      // Interpolate between brakeDeadA and gasDeadA
      double brakeU = params->brakeDeadU;
      double gasU = (params->gasDeadA - params->idleAcceleration + params->gasVelocityGain*v) / params->gasGain
        + params->gasDeadU;
      u = brakeU + (gasU - brakeU)*(a - params->brakeDeadA)/(params->gasDeadA - params->brakeDeadA);
    }
  }
  return u;
}
