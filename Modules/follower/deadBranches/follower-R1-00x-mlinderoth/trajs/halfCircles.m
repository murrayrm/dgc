N = 6;
n = 100;
R = 25;
v = 4;


T = zeros(N*n,6);

% pos
T(:,1) = -R * sin(pi*(1:N*n)/n);
for i = 1:N
    T(((i-1)*n+1 : i*n), 4) = R*(-1 + 2*i - cos(pi*(1:n)/n));
end

% speed
T(:,2) = -v * cos(pi*(1:N*n)/n);
for i = 1:N
    T(((i-1)*n+1 : i*n), 5) = v*sin(pi*(1:n)/n);
end

% acc
T(:,3) = v^2 / R * sin(pi*(1:N*n)/n);
for i = 1:N
    T(((i-1)*n+1 : i*n), 6) = v^2/R*cos(pi*(1:n)/n);
end







save halfCircles.traj T -ASCII