///
/// \file LongitudinalController.hh
/// \brief Takes care of the longitudinal part in trajectory following.
///
/// \author Kristian Soltesz, Magnus Linderoth
/// \date 10 July 2007
///
/// This file is part of the follower module.

#ifndef LONGITUDINAL_CONTROLLER_HH_HBIFUWEHIFGSEJOIFJS
#define LONGITUDINAL_CONTROLLER_HH_HBIFUWEHIFGSEJOIFJS

#include "FollowerUtils.hh"
#include "gcinterfaces/AdriveCommand.hh"
#include "interfaces/ActuatorState.h"
#include "FollowerLogger.hh"

#define LONGITUDINAL_SPEED_CONTROLLER 0
#define LONGITUDINAL_POSITION_CONTROLLER 1
#define SHIFT_BRAKING -0.7

///
/// \struct LongitudinalControllerParams
///
/// This stuct holds the parameters of the longitudinal controller
struct LongitudinalControllerParams {
  double lV;
  double lI;
  double maxI;  // [m/s^2]
  double pitchGain;
  double pitchTime;
  double alpha;
  double delay; // [s]
  double replanErrorAbs; // [m/s] 
  double replanErrorRel; // [see code]

  double gasDeadU;  // [cmd]
  double gasDeadA;  // [m/s^2]
  double brakeDeadU;  // [cmd]
  double brakeDeadA;  // [m/s^2]

  double gasGain;    // a/u [m/s^2 / cmd]
  double brakeGain;  // a/u [m/s^2 / cmd]

  double gasVelocityGain;  // a/v [m/s^2 / m/s  =  1/s]
  double brakeVelocityGain;  // a/v [m/s^2 / m/s  =  1/s]

  double noGasSpeed;  // [m/s]
  double idleAcceleration;  // [m/s^2]

  /// Constructors for LongitudinalControllerParams
  LongitudinalControllerParams()
  {
    LongitudinalControllerParams(0.0, 0.0, 0.0 ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  }

  /// Overloaded constructor for LongitudinalControllerParams
  LongitudinalControllerParams(double param1, double param2, double param3, double param4, double param5, double param6,
                               double param7, double param8, double param9, double param10, 
                               double param11, double param12, double param13, double param14, 
                               double param15, double param16, double param17, double param18, double param19) {
    lV = param1;
    lI = param2;
    maxI = param3;
    pitchGain = param4;
    pitchTime = param5;
    alpha = param6;
    delay = param7;
    replanErrorAbs = param8;
    replanErrorRel = param9;

    gasDeadU = param10;
    gasDeadA = param11;
    brakeDeadU = param12;
    brakeDeadA = param13;
    gasGain = param14;
    brakeGain = param15;
    gasVelocityGain = param16;
    brakeVelocityGain = param17;
    noGasSpeed = param18;
    idleAcceleration = param19;
  }
};

///
/// \class LongitudinalController
///
/// This is the class in which the longitudinal controller is implemented
class LongitudinalController
{
protected:
  /// The parameters used by the longitudinal controller
  LongitudinalControllerParams m_params;

  /// SparrowHawk needs these to be members
  double m_vErr;
  double m_vR;
  double m_vel;
  double m_a;
  double m_aFF;
  double m_u;

  /// FollowerLogger is used to evaluate performance of follower
  FollowerLogger* m_internalLogger;
  
  /// Integral part
  double m_I;

  /// Low-pass filtered pitch
  double m_pitch;

  /// Contribution to acceleration command from pitch feed forward
  double m_aPitch;

  /// Inverse of Alice's drive train non-linearities
  double linearize(double a, double v, LongitudinalControllerParams* params);


public:
  /// Constrtuctor for LongitudinalController 
  ///
  /// \param params longitudinal controller parameters
  /// \param m_internalLogger pointer to internalLogger
  LongitudinalController(LongitudinalControllerParams* params, 
                         FollowerLogger* m_internalLogger);

  /// Method in which the control signal is calculated 
  ///
  /// \param adriveDirective pointer to which the resulting AdriveDirective is written
  /// \param directivesToSend pointer to which the number of resulting AdriveDirectives is written
  /// \param followerMergedDirective pointer to CTraj with timestamp
  /// \param vehicleState pointer to vehicle state object
  /// \param trajectoryDirection trajectory direction 
  bool control(FollowerControlStatus* controlStatus, AdriveDirective* accelerationDirective, 
               CTraj* traj, 
               SingleFrameVehicleState* vehicleState, int trajectoryDirection);
};

#endif // LONGITUDINAL_CONTROLLER_HH_HBIFUWEHIFGSEJOIFJS
