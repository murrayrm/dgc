///
/// \file TransmissionController.cc
/// \brief Takes care of the gear shifting part in trajectory following.
///
/// \author Kristian Soltesz
/// \date 2 August 2007
///
/// This file is part of the follower module.

#include "TransmissionController.hh"

TransmissionController::TransmissionController():
  m_gear(1),
  m_trajectoryDirection(1),
  m_transmissionPending(false)
{
  // FIXME: transmission pending sparrowHawk+->rebind();
  sparrowHawk->rebind("tr_gear", &m_gear);
  sparrowHawk->set_readonly("tr_gear");
  sparrowHawk->rebind("tr_direction", &m_trajectoryDirection);
  sparrowHawk->rebind("tr_pending", &m_transmissionPending);
}

bool TransmissionController::control(FollowerControlStatus* controlStatus, AdriveDirective* transmissionDirective, CTraj* traj)
{
  
  int trajectoryDirection = traj->getDirection();

  // The sparrow display is updated 
  // once per cycle, which should 
  // be enough.
  m_transmissionPending = controlStatus->state & FollowerState::TransmissionPending; 
  m_trajectoryDirection = trajectoryDirection;

  // A shift direction should only be 
  // sent if Alice is in the wrong gear.
  // If we have a trajectory error we should
  // not shift, since the trajectory telling 
  // us the reference gear is corrupt.
  if ((m_gear != trajectoryDirection) && !(controlStatus->state & FollowerState::TrajectoryError)) {
    transmissionDirective->arg = trajectoryDirection;
    controlStatus->state |= FollowerState::TransmissionPending;

    // Returning true, indicating 
    // that a shift directive should be sent
    return true;
  } 

  // Returning false, indicating
  // that no shift direcitve should be sent
  return false;
}

void TransmissionController::completedShifting(int newGear)
{
  // The new gear is written
  // and we are no longer shifting
  m_gear = newGear;
}

void TransmissionController::initialize(int gear)
{
  // This is the real initialization
  m_gear = gear;
}
