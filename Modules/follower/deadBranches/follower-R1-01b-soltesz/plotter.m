%function plotter(inputLog)

run(inputLog);
%run ~/gcDriveCharacterization/adriver_20070704_35_brakeResTest.m
%run ~/gcDriveCharacterization/skynet2007_06_29_11_gasStep07.m
%run ./reverseBug.m

I = find(AdriveDirectives(:,4)==1);
accDir = AdriveDirectives(I,:);
gasDir = accDir;
gasDir(:,6) = max(gasDir(:,6), 0);
brakeDir = accDir;
brakeDir(:,6) = -min(brakeDir(:,6), 0);
I = find(AdriveDirectives(:,4)==0);
steerDir = AdriveDirectives(I,:);

speed = sqrt(VehicleStates(:,4).^2 + VehicleStates(:,6).^2)';


figure(1)
plot(VehicleStates(:,1)*1e-6, VehicleStates(:,1)+startTime-VehicleStates(:,2),'-*')
title('Time jitter on vehicle state / \mus')

figure(2)
plot(VehicleStates(:,15), VehicleStates(:,13),'b-*',VehicleStates(1,15), VehicleStates(1,13),'g-*')
axis equal
title('Vehicle position')

figure(3)
plot([ActuatorStates(:,1), ActuatorStates(:,1)]*1e-6, [ActuatorStates(:,4), ActuatorStates(:,3)],'-*', steerDir(:,1)*1e-6,steerDir(:,6),'-*')
title('steering')

figure(4)
plot([ActuatorStates(:,1), ActuatorStates(:,1)]*1e-6, [ActuatorStates(:,8), ActuatorStates(:,7)],'-*', gasDir(:,1)*1e-6,gasDir(:,6),'-*')
title('gassing')

figure(5)
plot([ActuatorStates(:,1), ActuatorStates(:,1), ActuatorStates(:,1)]*1e-6, [ActuatorStates(:,12), ActuatorStates(:,11), ActuatorStates(:,13)],'-*', brakeDir(:,1)*1e-6,brakeDir(:,6),'-*')
title('braking')

figure(6)
plot(VehicleStates(:,1)*1e-6, speed,'-*', accDir(:,1)*1e-6,accDir(:,6),'-*', ActuatorStates(:,1)*1e-6, ActuatorStates(:,16), '-*',VehicleStates(:,1)*1e-6, 100*VehicleStates(:,21),'-*',VehicleStates(:,1)*1e-6, 100*VehicleStates(:,19),'-*')
title('speed')



