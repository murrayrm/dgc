///
/// \file TransmissionController.hh
/// \brief Takes care of the gear shifting part in trajectory following.
///
/// \author Kristian Soltesz
/// \date 2 August 2007
///
/// This file is part of the follower module.

#ifndef TRANSMISSIONCONTROLLER_HH_JGDHDUSGUVJBSXVYUDGJXVDGYT
#define TRANSMISSIONCONTROLLER_HH_JGDHDUSGUVJBSXVYUDGJXVDGYT

#include "FollowerUtils.hh"
#include "gcinterfaces/AdriveCommand.hh"

///
/// \class TransmissionController
///
/// This is the calss in which the transmission 
/// controller is implemented.
class TransmissionController
{
protected:

  /// The current gear of Alice
  int m_gear;
  
  /// True iff Alice is currently shifting
  bool m_shifting;

public:

  /// Default constructor of TransmissionController
  TransmissionController();

  /// Method in which the transmission control signal is calculated
  ///
  /// \param controlStatus control status used by arbitrate
  /// \param transmissionDirective pointer to which teh resulting AdriveDirective is written
  /// \param trajectoryDirection trajectory direction
  bool control(FollowerControlStatus* controlStatus, AdriveDirective* transmissionDirective, int trajectoryDirection);

  /// Method used to inform the transmission
  /// controller, that shifting was completed
  ///
  /// \param newGear the new current gear of Alice
  void completedShifting(int newGear);

  /// Method used to inform the transmission
  /// controller that shifting failed
  void failedShifting();

  /// Accesor method, used
  /// by other to obtain the 
  /// tranmission state
  bool isShifting();

  void initialize(int gear);

};

#endif // TRANSMISSIONCONTROLLER_HH_JGDHDUSGUVJBSXVYUDGJXVDGYT
