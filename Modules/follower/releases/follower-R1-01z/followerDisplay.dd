/*
 * followerDisplay.dd - SparrowHawk display for follower
 *
 * Kristian Soltesz, Magnus Linderoth
 * 17 July 2007
 *
 */

extern long long sparrowhawk; 
extern void sparrowQuit(long);
extern void updateControllerParams(long);
extern void sparrowToggleVisLat(long);

#define D(x)    (sparrowhawk)



%%
SNKEY: %sn_key                          Hit 'l' to toggle visualization in mapviewer
--------------------+---------------------+----------------+-------------------------
Lateral (steering)  |Longitudinal (speed) |Transmission    |Status
--------------------+---------------------+----------------+-------------------------
error:   %la_err    |error:  %lo_err      |trajDir:  %tr_d | Traj ID: %traj_id
I-part:  %la_I      |I-part: %lo_I        |Cur gear: %tr_g | Traj age:   %traj_age     
phi:     %la_phi    |vRef:   %lo_vR       |Pending:  %tr_p | astate age: %astate_age
phi FF:  %la_pFF    |vel:    %lo_vel      |                | 
l1 GS:   %la_l1G    |a  :    %lo_a        |                | %trajectoryError                         
l2 GS:   %la_l2G    |aFF:    %lo_aFF      |                | %trajectoryEnd
                    |aPitch: %lo_aP       |                | %EstopPause                           
u:       %la_u      |u:      %lo_u        |                | %EstopDisable                                  
%la_replan          |%lo_replan           |                |<-Replan
--------------------+---------------------+----------------+
%dir_steer          |%dir_acc             |%dir_trans      |<-Directives
%resp_steer         |%resp_acc            |%resp_trans     |<-Responses
%send_steer         |%send_acc            |%send_trans     |<-Sending
%steer_loop_age     |%acc_loop_age        |                |<-Loop age
%steer_state_age    |%acc_state_age       |                |<-State age
--------------------+---------------------+----------------+ 
                    |Gas:     |Brake:     |                |
%rejSteer           |%rejGas  |%rejBrake  |%rejTrans       |<-Rejected      
%failSteer          |%failGas |%failBrake |%failTrans      |<-Failed
--------------------+---------------------+----------------+
Trigger: %roa_ot    Obs dist: %roa_cd    delay: %roa_dd    |<- ROA [m]                                      
-----------------------------------------------------------+
%roa_men       %roa_den       %roa_oen       %roa_ben      |<- ROA enabled            
-----------------------------------------------------------+
%u %upd    s of parameters from file: 
%paramfile


%QUIT        %update_params 
%%


#ROA ages
double: %steer_loop_age         D(steering_loop_age)            "%7.3f";
double: %acc_loop_age           D(acceleration_loop_age)        "%7.3f";
double: %steer_state_age        D(steering_state_age)           "%7.3f";
double: %acc_state_age          D(acceleration_state_age)       "%7.3f";

double: %traj_age               D(trajectory_age)               "%7.3f";
double: %astate_age             D(vehicle_state_age)            "%7.3f";        

#ROA enable
string: %roa_men                D(roa_enable_master)            "%s";     
string: %roa_den                D(roa_enable_delay)             "%s";
string: %roa_oen                D(roa_enable_obstacle)          "%s";
string: %roa_ben                D(roa_enable_backup)            "%s";

#ROA general
double: %roa_ot                 D(roa_obstacleTrigger)          "%7.3f";        
double: %roa_cd                 D(roa_collisionDistance)        "%7.3f";
double: %roa_dd                 D(roa_delayDistance)            "%7.3f";

#Misc
int:    %sn_key                 D(sn_key)                               "%8d";
string: %paramfile              D(paramfile)                            "%s"; 
int:    %u                      D(param_id)                             "%2d";
int:	%QUIT	                D(sparrowQuit)	                        "(q)uit";
int:    %upd                    D(updateControllerParams)             "(u)pdate";        
int:    %traj_id                D(trajectory_counter)                   "%d";

#Trajectory Faults
string: %trajectoryError        D(trajectoryError)              "%s";
string: %trajectoryEnd          D(trajectoryEnd)                "%s";

#Estop Status
string: %EstopPause             D(estopPause)                   "%s";
string: %EstopDisable           D(estopDisable)                 "%s";

#Currently sending directives?
int: %send_steer                D(send_steering)                "%d";
int: %send_acc                  D(send_acceleration)            "%d";
int: %send_trans                D(send_transmission)            "%d";

#Directives sent to gcdrive
int: %dir_steer                 D(steering_directive)           "%d";
int: %dir_acc                   D(acceleration_directive)       "%d";
int: %dir_trans                 D(transmission_directive)       "%d";

#Responses received from gcdrive 
int: %resp_steer                D(steering_response)            "%d";
int: %resp_acc                  D(acceleration_response)        "%d";
int: %resp_trans                D(transmission_response)        "%d";

#Rejected Responses
string: %rejSteer               D(steering_rejected)            "%s";
string: %rejGas                 D(gas_rejected)                 "%s";
string: %rejBrake               D(brake_rejected)               "%s";
string: %rejTrans               D(transmission_rejected)        "%s";

#Failure Responses
string: %failSteer              D(steering_failure)             "%s";
string: %failGas                D(gas_failure)                  "%s";
string: %failBrake              D(brake_failure)                "%s";
string: %failTrans              D(transmission_failure)         "%s";

#Transmission controller state
int:    %tr_d                   D(tr_direction)                 "%2d";
int:    %tr_g                   D(tr_gear)                      "%2d"; 
int:    %tr_p                   D(tr_pending)                   "%d";



#Lateral controller state
double: %la_err                 D(la_error)                     "%7.3f";
double: %la_I                   D(la_I)                         "%7.3f";
double: %la_phi                 D(la_phi)                       "%7.3f";
double: %la_pFF                 D(la_phiFF)                     "%7.3f";
double: %la_l1G                 D(la_l1_gs)                     "%7.3f";
double: %la_l2G                 D(la_l2_gs)                     "%7.3f";
double: %la_u                   D(la_u)                         "%7.3f";
string: %la_replan              D(la_replan)                    "%s";

#Longitudinal controller state
double: %lo_err                 D(lo_error)                     "%7.3f";
double: %lo_I                   D(lo_I)                         "%7.3f";
double: %lo_vR                  D(lo_vR)                        "%7.3f";
double: %lo_vel                 D(lo_vel)                       "%7.3f";
double: %lo_a                   D(lo_a)                         "%7.3f";
double: %lo_aFF                 D(lo_aFF)                       "%7.3f";
double: %lo_aP                  D(lo_aPitch)                    "%7.3f";
double: %lo_u                   D(lo_u)                         "%7.3f";
string: %lo_replan              D(lo_replan)                    "%s";



tblname: followertable;
bufname: followerbuf;

