% Gasing @ v = 0 m/s
ug = [0.125 0.15 0.2 0.3 0.4 0.5 0.6 0.7];
ag = [0.6 0.9 1.625 2.24 2.56 2.86 2.9 2.5];

xg = 0.125:0.025:0.7;
yg = 0.6 + 2.3*(1-exp(-7*(xg-0.125)));
yg2 = 0.6 + 13*(xg-.125);

figure(205)
plot(ug,ag,'*-',xg,yg,'-',xg,yg2,'-')
axis([0 0.8 0 3])
grid on
xlabel('u')
ylabel('a')



% Braking @ v > 2 m/s
%ub = [0:-.1:-1];
ub = -[0 1 3 4 6 7 9 10 12 13 15]/15 - 1/30;
abe = [-0.17 -0.17 -0.17 -0.17 -.28 -.61 -1.17 -1.37 -1.74 -2.23 -2.8];
abw = [-.2 -.2 -.2 -.2 -.37 -.68 -1.38 -1.53 -1.99 -2.19 -2.92];

xb = [-1 0];
yb = -0.17 + 4*(xb+0.39);

figure(206)
plot(ub,abe,'*-',ub,abw,'*-',xb,yb)%,'-',xg,yg2,'-')
axis([-1 0 -3 0])
grid on
xlabel('u')
ylabel('a')


u = [-1:0.01:1];
v = [0:0.2:15];
[U,V] = meshgrid(u,v);
a = NaN*ones(size(U));

% Gas u >= 1.15
%aTmp = 0.6 + 2.3*(1-exp(-7*(U-0.125))) - 0.15*V;
aTmp = 0.6 + 13*(U-.125) - 0.15*V;
I = find((U >= 0.15) & (aTmp >= 0));
a(I) = aTmp(I);

% Gas u >= 1.15
aTmp = 0.6 + 13*(U-.125) - (0.3 + (0.15-0.3)*(U-0.125)/(0.15-.125)).*V;
I = find((U >= 0.125) & (U < 0.15) & (aTmp >= 0));
a(I) = aTmp(I);

% Gas 0 < u < 0.125
aTmp = 0.6 - 0.3*V;
I = find((U > 0) & (U < 0.125) & (aTmp >= 0));
a(I) = aTmp(I);

% Brake v >= 2 m/s, u >= -0.39
aTmp = -.17*ones(size(U));
I = find((U >= -0.39) & (U < 0) & (V >= 2));
a(I) = aTmp(I);

% Brake v >= 2 m/s, u < -0.39
aTmp = -0.17 + 4*(U+0.39);
I = find((U < -0.39) & (V >= 2));
a(I) = aTmp(I);

% Brake v < 2 m/s, u >= -0.39
aTmp = -.17*ones(size(U)) - 0.3*(V-2);
I = find((U >= -0.39) & (U < 0) & (V < 2));
a(I) = aTmp(I);

% Brake v < 2 m/s, u < -0.39
aTmp = -0.17 + 4*(U+0.39) - 0.3*(V-2);
I = find((U < -0.39) & (V < 2));
a(I) = aTmp(I);

figure(207)
mesh(U,V,a)