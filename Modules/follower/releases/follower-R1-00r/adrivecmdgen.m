%input format:    steer=[time; function(time)]; acceleration=[time;function(time)];

function adrivecmdgen(steer,acceleration,outputfile)
    steer=[steer;zeros(1,size(steer,2))];
    acceleration=[acceleration;ones(1,size(acceleration,2))];
    merge=[steer acceleration];
    [Y,I]=sort(merge(1,:));
    merge=merge((1:3),I);
    tmplog=[merge(1,:)' zeros(size(merge,2),2) merge(3,:)' zeros(size(merge,2),1) merge(2,:)'];

    fid=fopen(outputfile,'wt');
    fprintf(fid,'%11.0f %3.0f %3.0f %3.0f %3.0f %15f\n',tmplog');
    fclose(fid);


    
    