/*!
 * \file FollowerUtils.hh
 * \brief Classes used by follower
 *
 * \author Nok Wongpiromsarn, Kristian Soltesz, Magnus Linderoth
 * \date 10 July 2007
 *
 * This file holds resources shared within the follower module.
 */

#ifndef FOLLOWER_UTILS_HH_HTXDOUYIUDTESDTDCHG
#define FOLLOWER_UTILS_HH_HTXDOUYIUDTESDTDCHG

#define printConsole(x) {printStream << x; printMessage();}

#include <gcinterfaces/FollowerCommand.hh>
#include "trajutils/traj.hh"
#include "sparrowhawk/SparrowHawk.hh"
#include <string>
#include <sstream>


/*!
 * \class FollowerControlStatus
 * 
 * This is the response from the controllers
 * 
 */
class FollowerControlStatus : public ControlStatus
{
public:
  unsigned id;
  FollowerResponse::FollowerFailure reason;

  FollowerControlStatus() {
    id = 0;
    status = RUNNING;
  }
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " status: " << status;
    return s.str();
  }
};


/*!
 * \class FollowerMergedDirective
 * 
 * This is the directive sent between Follower Arbitration and
 * Follower Control.
 */
class FollowerMergedDirective : public MergedDirective
{
public:
  unsigned id;
  CTraj* traj;
  FollowerMergedDirective() {
    id = 0;
    traj = NULL;
  }
  std::string toString() const {
    stringstream s("");
    s << "id: " << id;
    return s.str();
  }
};


/*! 
 * \struct SingeFrameVehicleState
 * 
 * Contains state information about the vehicle in one frame
 * (ususally local frame or UTM frame)
 */
struct SingleFrameVehicleState {
  double x;       // m
  double xVel;    // m/s
  double y;       // m
  double yVel;    // m/s
  double yaw;     // rad
  double yawRate; // rad/s
  double speed;   // m/s  Speed in forward direction
};

/* Pointer to SparrowHawk accessible to all who include this file*/
extern CSparrowHawk* sparrowHawk;

/*! Stream used for all message printing in follower */
extern stringstream printStream;

/*! Function used to print messages either in the terminal or SparrowHawk window */
void printMessage();

using namespace std;
/*!
 * \class CmdArgs
 *
 * This class holds shared command line arguments
 *
 */
class CmdArgs {
public:
  static int sn_key;
  static int rate;
  static int log_level;
  static char* log_path;
  static char* frame;
  static char* traj_file;
  static int verbose;
  static bool disable_console;
  static bool log_internal;
  static int gear; //This row will be removed in final version
};

#endif // FOLLOWER_UTILS_HH_HTXDOUYIUDTESDTDCHG
