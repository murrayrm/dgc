/*!
 * \file FollowerLogger.hh
 * \brief Class used to log the internal state of follower.
 *
 * \author Magnus Linderoth
 * \date 16 July 2007
 *
 * Variables that are to be logged are written to an instance of this class.
 * writeLog() is called periodically to write the data to the log file.
 */

#ifndef FOLLOWER_LOGGER_HH_THDYKTFJGHJYTFYUJGYUJ
#define FOLLOWER_LOGGER_HH_THDYKTFJGHJYTFYUJGYUJ

#include <stdio.h>

/*! 
 * \class FollowerLogger
 *
 * Class used to log internal state of follower
 */
class FollowerLogger {
private:
  /*! Pointer to log file */
  FILE* m_file;

public:
  /*! Time stamp */
  unsigned long long timeStamp;  // 1

  /*! Lateral controller variables */
  double la_l1_gs;  // 2
  double la_l2_gs;
  double la_phiFF;
  double la_phi; 
  double la_I;

  /*! Longitudinal controller variables */
  double lo_vRef;  // 7
  double lo_aFF;
  double lo_speed;
  double lo_I;
  double lo_a;
  double lo_u;

  /*! Info about current trajectory and point closest to Alice */
  int trajID;  // 13
  int closestIndex;
  double closestFractionToNext;

  /*! Current state of Alice */
  double curN;  // 16
  double curNd;
  double curE;
  double curEd;
  double curYaw;
  double curYawRate;

  /*! Data for closest point on  trajectory */
  double closestN;  // 22
  double closestNd;
  double closestNdd;
  double closestE;
  double closestEd;
  double closestEdd;

  /*! Lateral controller parameters */
  double la_l1;  // 28
  double la_l1Scale;
  double la_l2;
  double la_l2Scale;
  double la_alpha;
  double la_delay;

  /*! Longitudinal controller parameters */
  double lo_lV;  // 34
  double lo_lI;
  double lo_alpha;
  double lo_delay;
  double lo_gasDeadU;
  double lo_gasDeadA;
  double lo_brakeDeadU;
  double lo_brakeDeadA;
  double lo_gasGain;
  double lo_brakeGain;
  double lo_gasVelocityGain;
  double lo_brakeVelocityGain;
  double lo_noGasSpeed;
  double lo_idleAcceleration;

  /*! Estop state */
  double Estop;

  /*! ROA ages */
  double roa_ages[6];

  /*! Various stuff added 071015 */
  int gear;
  int trajectoryDirection;
  double crossTrackError;
  int roaBraking;  

  /*! Constructor */
  FollowerLogger();

  /*! Destructor */
  ~FollowerLogger();

  /*! 
   * Uses the current values of the member variables of the
   * FollowerLogger object to write one line of data to the log file
   */
  void writeLog();
};

#endif  // FOLLOWER_LOGGER_HH_THDYKTFJGHJYTFYUJGYUJ
