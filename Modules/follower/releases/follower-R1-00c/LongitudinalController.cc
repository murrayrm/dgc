/*!
 * \file LongitudinalController.cc
 * \brief Takes care of the longitudinal part in trajectory following.
 *
 * \author Kristian Soltesz, Magnus Linderoth
 * \date 10 July 2007
 *
 * This file is part of the follower module.
 */


#include "LongitudinalController.hh"
#include "CmdArgs.hh"

/* Constructor for LongitudinalController*/
LongitudinalController::LongitudinalController(LongitudinalControllerParams* params):
  m_params(*params)
{
  m_I = 0;
}

/* Method for calculating control signal */
void LongitudinalController::control(AdriveDirective* adriveDirective, 
                                     FollowerMergedDirective* followerMergedDirective,
                                     SingleFrameVehicleState* state) {
  //FIXME: is this neccessary?
  double pxR[3];
  double pyR[3];
  double pxFF[3];
  double pyFF[3];

  /* Extract direction of traj */
  CTraj* traj = followerMergedDirective->traj;
  int dir = traj->getDirection();

  /* Calculate feed forward point */
  double ffDist = state->speed * m_params.delay;

  /*Current state of the Alice */
  double xCR = state->x;
  double yCR = state->y;
  if (CmdArgs::verbose > 3) cout << "Car state: x=" << xCR << " y=" << yCR /*<< " yaw=" << thetaC*/ << endl;

  /* Determine the reference and feed forward points on the traj by linear interpolation */
  int closestIndex;
  double closestFractionToNext;
  TrajPoint refPoint = traj->interpolGetClosest(xCR, yCR, &closestIndex, &closestFractionToNext); 
  TrajPoint ffPoint = traj->interpolGetPointAhead(closestIndex, closestFractionToNext, ffDist, NULL, NULL);

  //FIXME: is this neccessary? Fill in the reference and feed forward points
  pxR[0] = refPoint.n;
  pxR[1] = refPoint.nd;
  pxR[2] = refPoint.ndd;
  pyR[0] = refPoint.e;
  pyR[1] = refPoint.ed;
  pyR[2] = refPoint.edd;
  pxFF[0] = ffPoint.n;
  pxFF[1] = ffPoint.nd;
  pxFF[2] = ffPoint.ndd;
  pyFF[0] = ffPoint.e;
  pyFF[1] = ffPoint.ed;
  pyFF[2] = ffPoint.edd;
 
  /* Reference speed */
  double vR = sqrt(pxR[1]*pxR[1] + pyR[1]*pyR[1]);

  /* Feed forward acceleration. Avoid division by zero. */
  double aFF;
  if (pxFF[1] == 0 && pyFF[1] == 0) {
    /* When velocity == 0, then lateral acceleration == 0             \
     * Velocity on traj should never be opposite of driving direction / ==> 
     * all acceleration is in forward direction */
    aFF = sqrt(pxFF[2]*pxFF[2] + pyFF[2]*pyFF[2]);
  } else {
    aFF = (pxFF[1]*pxFF[2] + pyFF[1]*pyFF[2]) / sqrt(pxFF[1]*pxFF[1] + pyFF[1]*pyFF[1]);
  }


  /* Speed error */
  double vErr = state->speed - vR;
  if (CmdArgs::verbose > 3) {
    cerr << "vR=" << vR << " vErr=" << vErr << " speed=" << state->speed << endl;
  }

  /* Update integral part */
  if (m_params.lI != 0) {
    m_I += vErr / m_params.lI / CmdArgs::rate;
  }

  /* Calculate control signal */
  double a = -m_params.lV*vErr + m_params.alpha*aFF - m_I;
  double u = linearize(a, state->speed, &m_params);
  if (CmdArgs::verbose > 3) {
    cerr << "m_I=" << m_I << " a=" << a << ", u=" << u << endl;
  }

  /* Stop if end of traj is reached */
  if (closestIndex == traj->getNumPoints() - 1) {
    u = -0.7;
  }

  /* Output steer command to returned AdriveDirective */
  adriveDirective->actuator = Acceleration;
  adriveDirective->command = SetPosition;
  adriveDirective->arg = u;
}


double LongitudinalController::linearize(double a, double v, LongitudinalControllerParams* params)
{
  double u;

  if (a <= params->brakeDeadA || a <= params->brakeDeadA - (v-params->noGasSpeed)*params->brakeVelocityGain){
    // Braking
    
    u = (a - params->brakeDeadA)/params->brakeGain + params->brakeDeadU;
    // Actual braking varies at low speeds
    if (v < params->noGasSpeed) {
      u += params->brakeVelocityGain*(v-params->noGasSpeed)/params->brakeGain;
    }
  } else if (a >= params->gasDeadA && a >= params->gasDeadA + params->idleAcceleration - v*params->gasVelocityGain) {
    //Gassing
    u = (a - params->idleAcceleration + params->gasVelocityGain*v) / params->gasGain + params->gasDeadU;

  } else {
    // Dead band
    
    if (v < params->noGasSpeed || v < params->idleAcceleration/params->gasVelocityGain) {
      // Running slowly
      // Interpolate between brakeDeadU and gasDeadU
      double brakeA = params->brakeDeadA;
      if (v < params->noGasSpeed) {
        brakeA -= params->brakeVelocityGain*(v-params->noGasSpeed);
      }
      double gasA = params->idleAcceleration - params->gasVelocityGain*v;
      u = params->brakeDeadU + (params->gasDeadU - params->brakeDeadU)*(a - brakeA)/(gasA - brakeA);
    } else {
      // Running faster
      // Interpolate between brakeDeadA and gasDeadA
      double brakeU = params->brakeDeadU;
      double gasU = (params->gasDeadA - params->idleAcceleration + params->gasVelocityGain*v) / params->gasGain
        + params->gasDeadU;
      u = brakeU + (gasU - brakeU)*(a - params->brakeDeadA)/(params->gasDeadA - params->brakeDeadA);
    }
  }
  return u;
}
