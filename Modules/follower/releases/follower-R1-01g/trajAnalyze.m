% Makes some plots that help analyze a trajectory.


% The input is a .traj-file created by CTraj::print().
% Unfortunately at the moment the two first lines of the file describing
% its content have to be removed
load UT_sinewave.traj

wheelbase = 3.55;
S = UT_sinewave;


steer = atan(wheelbase*((S(:,2).*S(:,6)-S(:,5).*S(:,3))./((S(:,2).^2+S(:,5).^2).^1.5)));

subplot(412);
plot(S(:,4), S(:,1),'*', S(:,4), 6*steer, '*-', [S(1,4) S(end,4)], [0 0]);
subplot(413);
plot(sqrt(S(:,5).^2+S(:,2).^2),'*');
subplot(414);
plot(sqrt(S(:,6).^2+S(:,3).^2),'-*');
subplot(411);
plot(-steer,'-*')