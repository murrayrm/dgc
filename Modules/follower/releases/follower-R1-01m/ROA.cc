///
/// \file ROA.cc
/// \brief Reactive Obstacle Avoidance.
/// 
/// \Kristian Soltesz, Magnus Linderoth
/// \date 21 September 2007
/// 
/// ROA gets delay and lateral error information from follower,
/// together with front and rear center ladar scans. It uses
/// this information to shape the velocity reference in order 
/// to avoid collisions. 
/// 
/// This file is part of the follower module.

#include "ROA.hh"

// Constructor for ROA
ROA::ROA(FollowerControlStatus* controlStatus, FollowerLogger* internalLogger) :
  m_internalLogger(internalLogger),
  m_lateralError(0),
  m_speed(0.0),
  m_steerAngle(0.0),
  m_delay(0),
  m_timeToCollision(0),
  m_ROAFactor(1.0),
  m_numberOfLadars(0) {
  memset(m_timeStamps, 0, sizeof(m_timeStamps));
  memset(m_ages, 0, sizeof(m_ages));
  
  initSensnet();
  sparrowInit();
}

void ROA::sparrowInit() {

  // Loop ages
  sparrowHawk->rebind("steering_loop_age", (int*)(&m_ages[STEERING_LOOP]));
  sparrowHawk->set_readonly("steering_loop_age");
  sparrowHawk->rebind("acceleration_loop_age", (int*)(&m_ages[ACCELERATION_LOOP]));
  sparrowHawk->set_readonly("acceleration_loop_age");
  //sparrowHawk->rebind("transmission_loop_age", (int*)(&m_ages[TRANSMISSION_LOOP]));
  //sparrowHawk->set_readonly("transmission_loop_age", (int*)(&m_ages[TRANSMISSION_LOOP]));

  // State ages
  sparrowHawk->rebind("steering_state_age", (int*)(&m_ages[STEERING_STATE]));
  sparrowHawk->set_readonly("steering_state_age");
  sparrowHawk->rebind("acceleration_state_age", (int*)(&m_ages[ACCELERATION_STATE]));
  sparrowHawk->set_readonly("acceleration_state_age");
  //sparrowHawk->rebind("transmission_state_age", (int*)(&m_ages[TRANSMISSION_STATE]));
  //sparrowHawk->set_readonly("transmission_state_age", (int*)(&m_ages[TRANSMISSION_STATE]));

  sparrowHawk->rebind("trajectory_age", (int*)(&m_ages[TRAJECTORY]));
  sparrowHawk->set_readonly("trajectory_age");

  sparrowHawk->rebind("vehicle_state_age", (int*)(&m_ages[VEHICLE_STATE]));
  sparrowHawk->set_readonly("vehicle_state_age");

  sparrowHawk->rebind("roa_delay", (int*)(&m_delay));
  sparrowHawk->set_readonly("roa_delay");
}

// Initialize sensnet
int ROA::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;
    
  // Create sensnet itnerface
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  
  modulename moduleId = MODROA;

  char* spreadDaemon;
  if (getenv("SPREAD_DAEMON")) {
    spreadDaemon = getenv("SPREAD_DAEMON");
  } else {
    cerr << "SPREAD_DEAMON environment variable not set." << endl;
    exit(1);
  }

 if (sensnet_connect(this->sensnet, spreadDaemon, CmdArgs::sn_key, moduleId) != 0)
    return -1;

  // Default ladar set
  numSensorIds = 0;
  //Commented out the other LADARs so that we just listen to the middle bumper
  //sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RIEGL;
  //sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  //sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->m_numberOfLadars < sizeof(this->m_ladars) / sizeof(this->m_ladars[0]));
    ladar = this->m_ladars + this->m_numberOfLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (sensnet_join(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
      return -1;
  }
  return 0;
}

// Clean up sensnet
int ROA::finiSensnet()
{
  int i;
  Ladar *ladar;

  for (i = 0; i < this->m_numberOfLadars; i++)
  {
    ladar = this->m_ladars + i;
    sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
  }
  sensnet_free(this->sensnet);
  
  return 0;
}


void ROA::updateTimeStamp(int index, unsigned long long timeStamp) {
  m_timeStamps[index] = timeStamp;
}

void ROA::updateAge(int index) {
  // [micro seconds]
  m_ages[index] = (DGCgettime()-m_timeStamps[index])/1000;
  m_internalLogger->roa_ages[index] = m_ages[index];
}

void ROA::updateAllAges() {
  for (int index = 0; index < NUMBER_OF_TIMES; index++) {
    updateAge(index);
  }
}


void ROA::updateLateralError(double lateralError) {
  // FIXME: Not sure if we want this error metric
  m_lateralError = lateralError;
}

void ROA::updateSpeed(double speed) {
  m_speed = speed;
}

void ROA::updateSteerAngle(double steerAngle) {
  m_steerAngle = steerAngle;
}

void ROA::updateDelay() {
  
  // For now just a weighted average
  // normalized weights in [0,1]
  // FIXME: should this be static?
  double weights[NUMBER_OF_TIMES];

  // FIXME: tweak weights
  memset(weights, 0, sizeof(weights));
  weights[STEERING_LOOP] = 1.0;
  weights[ACCELERATION_LOOP] = 1.0;
  weights[TRAJECTORY] = 1.0;

  unsigned long tmpDelay = 0;
  double numberOfWeights;
  for (int i = 0; i < NUMBER_OF_TIMES-1; i++) {
    if (weights[i] != 0.0) {
      numberOfWeights++;
      tmpDelay += (unsigned long)(weights[i]*m_ages[i]);
    } 
  }
  // now m_delay contains weighted sum of relevant delays
}

void ROA::updateLadars(LadarRangeBlob* blob, int* blobLen) {
  Ladar * ladar;
  int blobId;
  
  for (int i = 0; i < m_numberOfLadars; i++) {
    ladar = m_ladars + i;
    
    // Check the latest blob id
    if (sensnet_peek(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, blobLen) != 0)
      break;
    
    // Is this a new blob?
    if (blobId == ladar->blobId)
      continue;
    ladar->blobId = blobId;
    
    // If this is a new blob, read it
    if (sensnet_read(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, *blobLen, blob) != 0)
      break;
  }
}

bool ROA::inRiscZone(float vfx, float vfy, float* arcDistance) {
  // Negative 'radius' when turning CCW
  float turningRadius = VEHICLE_WHEELBASE/tan(m_steerAngle); 
  float RiscZoneWidth = 4.0; // FIXME: This is the width of the risc zone corridor
  
  if (sqrt(pow(vfx,2.0)+pow(vfy-turningRadius,2.0)) > (pow(turningRadius,2.0)-RiscZoneWidth)) {
    // FIXME: is this correct?
    *arcDistance = atan(vfx/(turningRadius-vfy))*turningRadius;
    return true;
  }
  return false;
}

void ROA::updateTimeToCollision() {
  LadarRangeBlob blob;
  int blobLen;
  // Populates the blob
  updateLadars(&blob, &blobLen);

  // Loops through the blob
  float tmpArcDistance, minArcDistance = std::numeric_limits<float>::max();

  for (int i = 0; i < NUMSCANPOINTS; i++) {
    // Vehicle frame
    float vfx, vfy;
   
    // Protection from using wrong coordinates
    {
      float vfz, sfx, sfy, sfz;
   
      LadarRangeBlobScanToSensor(&blob, blob.points[i][ANGLE], blob.points[i][RANGE], &sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    }

    if (inRiscZone(vfx, vfy, &tmpArcDistance) && tmpArcDistance < minArcDistance) {
      minArcDistance = tmpArcDistance;
    }
  }
  
  // Estimate the time to collision based on arc length
  m_timeToCollision = (unsigned long)(minArcDistance/m_speed);
}

void ROA::updateROAFactor() {
  // FIXME:
}

void ROA::limitReferenceSpeed(double* referenceVelocity) {
  // Find out how old things are
  updateAllAges();

  // Use the updated age information to estimate a 'master delay'
  updateDelay();

  // Estimate time to collision using LADAR information
  updateTimeToCollision();

  // Updates the factor by which nominal reference speed is multiplied
  updateROAFactor();

  // Limit reference velocity
  *referenceVelocity *= m_ROAFactor;
}



