/*!
 * \file FollowerUtils.hh
 * \brief Classes used by follower
 *
 * \author Nok Wongpiromsarn, Kristian Soltesz, Magnus Linderoth
 * \date 10 July 2007
 *
 * This file is part of the follower module.
 */


#ifndef FOLLOWER_UTILS_HH_HTXDOUYIUDTESDTDCHG
#define FOLLOWER_UTILS_HH_HTXDOUYIUDTESDTDCHG

#include <gcinterfaces/FollowerCommand.hh>


class FollowerControlStatus : public ControlStatus
{
public:
  unsigned id;
  FollowerResponse::FollowerFailure reason;

  FollowerControlStatus() {
    id = 0;
    status = RUNNING;
  }
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " status: " << status;
    return s.str();
  }
};

/*
 * \class FollowerMergedDirective
 * 
 * This is the directive sent between Follower Arbitration and
 * Follower Control.
 */
class FollowerMergedDirective : public MergedDirective
{
public:
  unsigned id;
  CTraj* traj;
  FollowerMergedDirective() {
    id = 0;
    traj = NULL;
  }
  std::string toString() const {
    stringstream s("");
    s << "id: " << id;
    return s.str();
  }
};

/* 
 * \struct SingeFrameVehicleState
 * 
 * Contains state information about the vehicle in one frame
 * (ususally local frame or UTM frame)
 */
struct SingleFrameVehicleState {
  double x;       // m
  double xVel;    // m/s
  double y;       // m
  double yVel;    // m/s
  double yaw;     // rad
  double yawRate; // rad/s
  double speed;   // m/s  Speed in forward direction
};

#endif // FOLLOWER_UTILS_HH_HTXDOUYIUDTESDTDCHG
