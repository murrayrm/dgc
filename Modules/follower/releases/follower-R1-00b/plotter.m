%function plotter(inputLog)

logfile

figure(1)
plot(VehicleStates(:,1)*1e-6, VehicleStates(:,1)+startTime-VehicleStates(:,2))
title('Time jitter on vehicle state / \mus')

figure(2)
plot(VehicleStates(:,5), VehicleStates(:,3))
axis equal
title('Vehicle position')

figure(3)
plot([ActuatorStates(:,1), ActuatorStates(:,1)]*1e-6, [ActuatorStates(:,2), ActuatorStates(:,3)])
title('steering')

figure(4)
plot([ActuatorStates(:,1), ActuatorStates(:,1)]*1e-6, [ActuatorStates(:,4), ActuatorStates(:,5)])
title('gassing')

figure(5)
plot([ActuatorStates(:,1), ActuatorStates(:,1), ActuatorStates(:,1)]*1e-6, [ActuatorStates(:,6), ActuatorStates(:,7), ActuatorStates(:,8)])
title('braking')



