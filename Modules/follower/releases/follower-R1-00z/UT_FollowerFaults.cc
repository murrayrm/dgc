/*!
 * \file UT_FollowerFaults.cc 
 * \brief Follower Fault Handling unit test
 *
 * \author Vanessa D. Carson
 * \date 3 August 2007
 *
 * \ingroup unittest
 *
 * This program serves as a basic unit test for the follower fault handling capabilities.  It
 * works by receiving fault responses from a mock adrive through the GcModule interface
 * and verifying that the appropriate handling of the fault was done.  This
 * test is designed to be run with FollowerMain.
 */



#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <sys/stat.h>

#include <dgcutils/DGCutils.hh>
#include <skynet/sn_msg.hh>
#include "cmdline.h"
#include "Follower.hh"
#include "LateralController.hh"
#include "LongitudinalController.hh"
#include "dgcutils/cfgfile.h"

#include <iostream>
#include <vector>
#include <ctime> 
#include "skynet/skynet.hh"
#include "TestAdriveModule.hh"

using namespace std;

int main (int argc, char **argv)
{
  cout << "Unit test: " << argv[0] << endl;
  int skynet_key = skynet_findkey(argc, argv);

  /* Create a GcModule for the test program */
  TestAdriveModule *testModule = new TestAdriveModule(skynet_key);
  testModule->startModule();
  return 0;
}
