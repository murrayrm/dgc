///
/// \file TransmissionController.hh
/// \brief Takes care of the gear shifting part in trajectory following.
///
/// \author Kristian Soltesz
/// \date 2 August 2007
///
/// This file is part of the follower module.

#ifndef TRANSMISSIONCONTROLLER_HH_JGDHDUSGUVJBSXVYUDGJXVDGYT
#define TRANSMISSIONCONTROLLER_HH_JGDHDUSGUVJBSXVYUDGJXVDGYT

#include "FollowerUtils.hh"
#include "gcinterfaces/AdriveCommand.hh"

///
/// \class TransmissionController
///
/// This is the calss in which the transmission 
/// controller is implemented.
class TransmissionController
{
protected:

  /// The current gear of Alice
  int m_gear;

  /// Direction of last trajectory
  int m_trajectoryDirection;

  /// Are we currently changing gears?
  bool m_transmissionPending;
  
public:

  /// Default constructor of TransmissionController
  TransmissionController();

  /// Method in which the transmission control signal is calculated
  ///
  /// \param controlStatus control status used by arbitrate
  /// \param transmissionDirective pointer to which teh resulting AdriveDirective is written
  /// \param trajectoryDirection trajectory direction
  bool control(FollowerControlStatus* controlStatus, AdriveDirective* transmissionDirective, CTraj* traj, int currentGear);
  void initialize(int gear);
};

#endif // TRANSMISSIONCONTROLLER_HH_JGDHDUSGUVJBSXVYUDGJXVDGYT
