function phi = computeSteer(y, gamma, maxSteer, L, r, l1,l2)
phiFF = -atan(L./r);
delta = atan2(l2.*sin(phiFF) - l1.*sin(gamma) - y, l1 + l2.*cos(phiFF) - l1.*cos(gamma));
phi = min(maxSteer, max(-maxSteer, mod(delta-gamma+pi, 2*pi)-pi));