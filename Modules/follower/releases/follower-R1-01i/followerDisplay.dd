/*
 * followerDisplay.dd - SparrowHawk display for follower
 *
 * Kristian Soltesz, Magnus Linderoth
 * 17 July 2007
 *
 */

extern long long sparrowhawk; 
extern void sparrowQuit(long);
extern void updateControllerParams(long);
extern void sparrowToggleVisLat(long);

#define D(x)    (sparrowhawk)



%%
SNKEY: %sn_key    
--------------------+---------------------+----------------+---------------------
Lateral (steering)  |Longitudinal (speed) |Transmission    |Status
--------------------+---------------------+----------------+---------------------
error:   %la_err    |error:  %lo_err      |trajDir:  %tr_d | Trajectory: %traj_id
I-part:  %la_I      |I-part: %lo_I        |Cur gear: %tr_g | %trajectoryTimeout            
phi:     %la_phi    |vRef:   %lo_vR       |Pending:  %tr_p | %trajectoryError 
phi FF:  %la_pFF    |vel:    %lo_vel      |                | %trajectoryEnd                        
l1 GS:   %la_l1G    |a  :    %lo_a        |                | 
l2 GS:   %la_l2G    |aFF:    %lo_aFF      |                | %EstopPause                           
                    |aPitch: %lo_aP       |                | %EstopDisable                                  
u:       %la_u      |u:      %lo_u        |                |
%la_replan          |%lo_replan           |                |<-Replan
--------------------+---------------------+----------------+
%dir_steer          |%dir_acc             |%dir_trans      |<-Directives
%resp_steer         |%resp_acc            |%resp_trans     |<-Responses  
%send_steer         |%send_acc            |%send_trans     |<-Sending                    
--------------------+---------------------+----------------+ 
                    |Gas:     |Brake:     |                |
%rejSteer           |%rejGas  |%rejBrake  |%rejTrans       |<-Rejected      
%failSteer          |%failGas |%failBrake |%failTrans      |<-Failed
--------------------+---------------------+----------------+
%la_v : 'l'         |n/a                  |n/a             |<-Visualization
--------------------+---------------------+----------------+---------------------
%u %upd    s of controller parameters from file: 
%paramfile


%QUIT        %update_params 
%%

#Misc
int:    %sn_key         D(sn_key)                               "%8d";
string: %paramfile      D(paramfile)                            "%s"; 
int:    %u              D(param_id)                             "%2d";
int:	%QUIT	        D(sparrowQuit)	                        "(q)uit";
int:    %upd              D(updateControllerParams)             "(u)pdate";        
int:    %la_v           D(sparrowToggleVisLat)                  "TOGGLE";
int:    %traj_id        D(trajectory_counter)                   "%d";

#Trajectory Faults
string: %trajectoryTimeout      D(trajectoryTimeout)            "%s";
string: %trajectoryError        D(trajectoryError)              "%s";
string: %trajectoryEnd          D(trajectoryEnd)                "%s";

#Estop Status
string: %EstopPause             D(estopPause)                   "%s";
string: %EstopDisable           D(estopDisable)                 "%s";

#Currently sending directives?
int: %send_steer                D(send_steering)                "%d";
int: %send_acc                  D(send_acceleration)            "%d";
int: %send_trans                D(send_transmission)            "%d";

#Directives sent to gcdrive
int: %dir_steer                 D(steering_directive)           "%d";
int: %dir_acc                   D(acceleration_directive)       "%d";
int: %dir_trans                 D(transmission_directive)       "%d";

#Responses received from gcdrive 
int: %resp_steer                D(steering_response)            "%d";
int: %resp_acc                  D(acceleration_response)        "%d";
int: %resp_trans                D(transmission_response)        "%d";

#Rejected Responses
string: %rejSteer               D(steering_rejected)            "%s";
string: %rejGas                 D(gas_rejected)                 "%s";
string: %rejBrake               D(brake_rejected)               "%s";
string: %rejTrans               D(transmission_rejected)        "%s";

#Failure Responses
string: %failSteer              D(steering_failure)             "%s";
string: %failGas                D(gas_failure)                  "%s";
string: %failBrake              D(brake_failure)                "%s";
string: %failTrans              D(transmission_failure)         "%s";

#Transmission controller state
int:    %tr_d                   D(tr_direction)                 "%2d";
int:    %tr_g                   D(tr_gear)                      "%2d"; 
int:    %tr_p                   D(tr_pending)                   "%d";

#Lateral controller state
double: %la_err                 D(la_error)                     "%7.3f";
double: %la_I                   D(la_I)                         "%7.3f";
double: %la_phi                 D(la_phi)                       "%7.3f";
double: %la_pFF                 D(la_phiFF)                     "%7.3f";
double: %la_l1G                 D(la_l1_gs)                     "%7.3f";
double: %la_l2G                 D(la_l2_gs)                     "%7.3f";
double: %la_u                   D(la_u)                         "%7.3f";
string: %la_replan              D(la_replan)                    "%s";

#Longitudinal controller state
double: %lo_err                 D(lo_error)                     "%7.3f";
double: %lo_I                   D(lo_I)                         "%7.3f";
double: %lo_vR                  D(lo_vR)                        "%7.3f";
double: %lo_vel                 D(lo_vel)                       "%7.3f";
double: %lo_a                   D(lo_a)                         "%7.3f";
double: %lo_aFF                 D(lo_aFF)                       "%7.3f";
double: %lo_aP                  D(lo_aPitch)                    "%7.3f";
double: %lo_u                   D(lo_u)                         "%7.3f";
string: %lo_replan              D(lo_replan)                    "%s";

tblname: followertable;
bufname: followerbuf;

