///
/// \file ROA.cc
/// \brief Reactive Obstacle Avoidance.
/// 
/// \Kristian Soltesz, Magnus Linderoth
/// \date 21 September 2007
/// 
/// ROA gets delay and lateral error information from follower,
/// together with front and rear center ladar scans. It uses
/// this information to shape the velocity reference in order 
/// to avoid collisions. 
/// 
/// This file is part of the follower module.

#include "ROA.hh"
#define MAX_DECELERATION 2.7

// Constructor for ROA
ROA::ROA(FollowerControlStatus* controlStatus, FollowerLogger* internalLogger) :
  m_obstacleDelayTerm(0.0),
  m_obstacleMaxDecelerationTerm(0.0),
  m_obstacleTrigger(0.0),
  m_obstacleCollisionDistance(0.0),
  m_internalLogger(internalLogger),
  m_lateralError(0),
  m_gear(0),
  m_speed(0.0),
  m_steerAngle(0.0) {
  memset(m_timeStamps, 0, sizeof(m_timeStamps));
  memset(m_ages, 0, sizeof(m_ages));
  initSensnet();
  sparrowInit();
}


int ROA::initSensnet() {
  // Set spread daemon
  char* spreadDaemon;
  if (getenv("SPREAD_DAEMON")) {
    spreadDaemon = getenv("SPREAD_DAEMON");
  } else {
    cerr << "SPREAD_DEAMON environment variable not set." << endl;
    return -1;
  }

  // Set skynet key
  int skynetKey;

  if (getenv("SKYNET_KEY"))
    skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    skynetKey = 1378;

  /*
  if (CmdArgs::sn_key) {
    skynetKey = CmdArgs::sn_key;
  } else {
    skynetKey = 0;
  }
  */

  // Create sensnet interface
  m_sensnet = sensnet_alloc();
  assert(m_sensnet);
  if (sensnet_connect(m_sensnet, spreadDaemon, skynetKey, MODROA) != 0) {
    printConsole("Unable to connect to sensnet" << endl);
    return -1;
  }

  if (sensnet_join(m_sensnet,SENSNET_MF_BUMPER_LADAR, SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0) {
    printConsole("Unable to join sensnet" << endl);
    exit(1);
    return -1;
  }
  return 0;
}


// Clean up sensnet
int ROA::finiSensnet()
{
  // There are more important things to implement right now
  return 0;
}


void ROA::setParams(ROAParams params) {
  m_params = params;
}

void ROA::sparrowInit() {

  // Gcdrive loop ages
  sparrowHawk->rebind("steering_loop_age", &m_ages[STEERING_LOOP]);
  sparrowHawk->set_readonly("steering_loop_age");
  sparrowHawk->rebind("acceleration_loop_age", &m_ages[ACCELERATION_LOOP]);
  sparrowHawk->set_readonly("acceleration_loop_age");

  // Gcdrive state ages
  sparrowHawk->rebind("steering_state_age", &m_ages[STEERING_STATE]);
  sparrowHawk->set_readonly("steering_state_age");
  sparrowHawk->rebind("acceleration_state_age",&m_ages[ACCELERATION_STATE]);
  sparrowHawk->set_readonly("acceleration_state_age");

  // Other ages
  sparrowHawk->rebind("trajectory_age", &m_ages[TRAJECTORY]);
  sparrowHawk->set_readonly("trajectory_age");
  sparrowHawk->rebind("vehicle_state_age", &m_ages[VEHICLE_STATE]);
  sparrowHawk->set_readonly("vehicle_state_age");

  // State information
  sparrowHawk->rebind("roa_gear", &m_gear);
  sparrowHawk->set_readonly("roa_gear");
  sparrowHawk->rebind("roa_lateral_error", &m_lateralError);
  sparrowHawk->set_readonly("roa_lateral_error");
  sparrowHawk->rebind("roa_speed", &m_speed);
  sparrowHawk->set_readonly("roa_speed");
  sparrowHawk->rebind("roa_steer_angle", &m_steerAngle);
  sparrowHawk->set_readonly("roa_steer_angle");

  // Obstacle
  sparrowHawk->rebind("roa_delayTerm", &m_obstacleDelayTerm);
  sparrowHawk->set_readonly("roa_delayTerm");
  sparrowHawk->rebind("roa_maxDecelerationTerm", &m_obstacleMaxDecelerationTerm);
  sparrowHawk->set_readonly("roa_maxDecelerationTerm");
  sparrowHawk->rebind("roa_marginTerm", &m_params.obstacleDistanceMargin);
  sparrowHawk->set_readonly("roa_marginTerm");
  sparrowHawk->rebind("roa_obstacleTrigger", &m_obstacleTrigger);
  sparrowHawk->set_readonly("roa_obstacleTrigger");
}

void ROA::updateTimeStamp(int index, unsigned long long timeStamp) {
  m_timeStamps[index] = timeStamp;
}

void ROA::updateAge(int index) {
  // [micro seconds]
  m_ages[index] = (DGCgettime()-m_timeStamps[index])/1000000.0;
  m_internalLogger->roa_ages[index] = m_ages[index];
}

void ROA::updateAllAges() {
  for (int index = 0; index < NUMBER_OF_TIMES; index++) {
    updateAge(index);
  }
}

void ROA::updateLateralError(double lateralError) {
  m_lateralError = lateralError;
}

void ROA::updateSpeed(double speed) {
  m_speed = speed;
}

void ROA::updateSteerAngle(double steerAngle) {
  m_steerAngle = steerAngle;
}

void ROA::updateGear(int gear) {
  m_gear = gear;
}

int ROA::updateLadar() {
  int blobId, blobLen;
  if (sensnet_peek(m_sensnet, SENSNET_MF_BUMPER_LADAR, SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
    {    
      return -1;
    }

  if (sensnet_read(m_sensnet, SENSNET_MF_BUMPER_LADAR, SENSNET_LADAR_BLOB, &blobId, sizeof(m_blob), &m_blob) != 0) {
    printConsole("Error reading from sensnet" << endl);
    return -1;
  } else {
    return 0;
  }
}

bool ROA::inRiscZone(float vfx, float vfy, float* arcDistance) {
  // Negative 'radius' when turning CCW
  float turningRadius = VEHICLE_WHEELBASE/tan(m_steerAngle);

  if (fabs(fabs(turningRadius)-sqrt(pow(vfx,2.0)+pow(vfy-turningRadius,2.0))) < m_params.riscZoneHalfWidth) {
    *arcDistance = atan2(vfx,(turningRadius-vfy)*turningRadius/fabs(turningRadius))*fabs(turningRadius); // see paper note for verification
    return true;
  }
  return false;
}

void ROA::updateCollisionDistance() {
  LadarRangeBlob* blob = &m_blob;

  // Populate the blob
  if (updateLadar() != 0) {
    return;
  }

  // Loops through the blob
  float tmpArcDistance, minArcDistance = std::numeric_limits<float>::max();

  for (int i = 0; i < blob->numPoints; i++) {
    // Vehicle frame
    float vfx, vfy;
   
    // Protection from using wrong coordinates
    {
      float vfz, sfx, sfy, sfz;
      float ba = blob->points[i][0];
      float br = blob->points[i][1];
      LadarRangeBlobScanToSensor(blob, ba, br, &sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    }

    if (inRiscZone(vfx, vfy, &tmpArcDistance) && tmpArcDistance < minArcDistance) {
      minArcDistance = tmpArcDistance;
    }
  }

  m_obstacleCollisionDistance = max(minArcDistance - DIST_REAR_AXLE_TO_FRONT,0.0);
}

void ROA::limitReferenceSpeed(double* referenceSpeed) {
  
  if (m_params.enableMaster) {
    sparrowHawk->set_string("roa_enable_master", "MASTER");
  } else {
    sparrowHawk->set_string("roa_enable_master", "      ");
  }

  if (m_params.enableDelay) {
    sparrowHawk->set_string("roa_enable_delay", "DELAY");
  } else {
    sparrowHawk->set_string("roa_enable_delay", "     ");
  }

  if (m_params.enableObstacle) {
    sparrowHawk->set_string("roa_enable_obstacle", "OBSTACLE");
  } else {
    sparrowHawk->set_string("roa_enable_obstacle", "        ");
  }

  if (m_params.enableLateralError) {
    sparrowHawk->set_string("roa_enable_lateral_error", "LAT ERR");
  } else {
    sparrowHawk->set_string("roa_enable_lateral_error", "       ");
  }

  // Find out how old things are
  updateAllAges();

  if (m_params.enableMaster && m_params.enableObstacle && m_gear == 1) {
    updateCollisionDistance();

    if (m_params.enableDelay) {
      m_obstacleDelayTerm = (m_ages[TRAJECTORY]+max(m_ages[STEERING_LOOP],m_ages[ACCELERATION_LOOP]))*m_speed;
    } else {
      m_obstacleDelayTerm = 0.0;
    }

    m_obstacleMaxDecelerationTerm = pow(m_speed,2.0) / (2.0*m_params.maxDeceleration);
    
    m_obstacleTrigger = m_obstacleCollisionDistance - 
      (m_obstacleDelayTerm + m_obstacleMaxDecelerationTerm + m_params.obstacleDistanceMargin);

    if (m_obstacleTrigger < 0.0) {
      printConsole("ROA BRAKING" << endl);
      *referenceSpeed = 0.0;
    }
  } 


  printConsole("Max delay: " << m_params.maxDelay << endl);
  // FIXME: should also slow down for gcdrive latencies
  //        should not be discrete
  if (m_params.enableDelay && m_ages[TRAJECTORY] > m_params.maxDelay) {  // FIXME: hardcoded magic number
    *referenceSpeed = 0.0;
  }

  // Log whether ROA is enabled or disabled
  m_internalLogger->roa_enableMaster  = m_params.enableMaster;
  m_internalLogger->roa_enableDelay = m_params.enableDelay;
  m_internalLogger->roa_enableLateralError = m_params.enableLateralError;
  m_internalLogger->roa_enableObstacle = m_params.enableObstacle;
}

void ROA::updateLongitudinalFeedForward(double* feedForward) {
  if (m_obstacleTrigger < 0.0) {
    *feedForward = 0.0;
  }
}
