/*!
 * \file FollowerPlotter.cc
 * \brief Class used to show a time plot of follower varibles in a mapviewer.
 *
 * \author Magnus Linderoth
 * \date 13 June 2007
 *
 */

#include "FollowerPlotter.hh"
#include "FollowerUtils.hh"

/* Constructor */
FollowerPlotter::FollowerPlotter()
{
  m_meTalker.initSendMapElement(CmdArgs::sn_key);

  memset(m_data, 0, sizeof(m_data));

}

/* Destructor */
FollowerPlotter::~FollowerPlotter()
{

}


void FollowerPlotter::updatePlot(FollowerLogger* logger)
{
  int sendSubGroup = -16;

  m_index = (m_index + 1) % N_SAMPS;

  m_data[0][m_index] = logger->timeStamp*1e-6;
  m_data[1][m_index] = logger->lo_vRef;
  m_data[2][m_index] = logger->lo_aFF;
  m_data[3][m_index] = logger->lo_speed;
  m_data[4][m_index] = logger->lo_I;
  m_data[5][m_index] = logger->lo_a;
  m_data[6][m_index] = logger->lo_u;

  MapElement me;
  point2 point;
  vector<point2> points;

  // Reference velocity
  for (int i = 1; i <= N_SAMPS; i++) {
    point.set(m_data[0][(i + m_index) % N_SAMPS]-m_data[0][m_index],
              m_data[1][(i + m_index) % N_SAMPS]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_CYAN);
  me.setGeometry(points);
  me.setId(1);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(2);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  // Speed
  points.clear();
  for (int i = 1; i <= N_SAMPS; i++) {
    point.set(m_data[0][(i + m_index) % N_SAMPS]-m_data[0][m_index],
              m_data[3][(i + m_index) % N_SAMPS]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(points);
  me.setId(5);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(6);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);
 
  // Integral
  points.clear();
  for (int i = 1; i <= N_SAMPS; i++) {
    point.set(m_data[0][(i + m_index) % N_SAMPS]-m_data[0][m_index],
              m_data[4][(i + m_index) % N_SAMPS]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_MAGENTA);
  me.setGeometry(points);
  me.setId(7);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(8);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);
 
  // Command
  points.clear();
  for (int i = 1; i <= N_SAMPS; i++) {
    point.set(m_data[0][(i + m_index) % N_SAMPS]-m_data[0][m_index],
              m_data[6][(i + m_index) % N_SAMPS]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_DARK_GREEN);
  me.setGeometry(points);
  me.setId(11);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(12);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  
}
