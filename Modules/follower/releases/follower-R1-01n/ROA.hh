///
/// \file ROA.hh
/// \brief Reactive Obstacle Avoidance.
/// 
/// \Kristian Soltesz, Magnus Linderoth
/// \date 21 September 2007
/// 
/// ROA gets delay and lateral error information from follower,
/// together with front and rear center ladar scans. It uses
/// this information to shape the velocity reference in order 
/// to avoid collisions. 
/// 
/// This file is part of the follower module.

#ifndef ROA_UJGGIOSDBUYCBIUSDGNBDSHGIUCBUIGDJK
#define ROA_UJGGIOSDBUYCBIUSDGNBDSHGIUCBUIGDJK

#include "FollowerUtils.hh"
#include "FollowerLogger.hh"
#include "alice/AliceConstants.h"
#include <assert.h>

// Sensnet/Skynet support
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include <skynettalker/SkynetTalker.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>

#define NUMSCANPOINTS 181
#define ANGLE 0
#define RANGE 1


/// 
/// \struct ROAParams
///
/// This struct holds the parameters of the ROA.
struct ROAParams {
  int enable;
  int minDelay;
  int maxDelay;

  /// Constructors for ROAParmas
  ROAParams()
  {
    ROAParams(0, 0, 0);
  }

  /// Overloaded constructor for LateralControllerParams
  ROAParams(int param1, int param2, int param3) {
    enable = param1;
    minDelay = param2;
    maxDelay = param3;
  }
};

class ROA : public CMapElementTalker
{
public:
  enum TIMES {
    STEERING_LOOP = 0, 
    ACCELERATION_LOOP, 
    TRANSMISSION_LOOP, // Not very interesting
    STEERING_STATE,
    ACCELERATION_STATE,
    TRANSMISSION_STATE,
    TRAJECTORY,    
    VEHICLE_STATE, 
    NUMBER_OF_TIMES
  };

protected:
  /// The parameters used by the ROA
  ROAParams m_params;

  /// FollowerLogger is used to evaluate performance of follower
  FollowerLogger* m_internalLogger;

  // Vector of time stamps used for calculating delays
  unsigned long long m_timeStamps[NUMBER_OF_TIMES];

  // Ages (of time stamps) used for calculating delays
  unsigned long m_ages[NUMBER_OF_TIMES];

  /// Error used by ROA function
  double m_lateralError;

  /// Current speed
  double m_speed;
  
  /// Current steer angle
  double m_steerAngle;
  
  /// Delay used by ROA function
  unsigned long m_delay;

  /// Estimated time to collision
  unsigned long m_timeToCollision;

  /// Nominal velocity reference is multiplied by m_ROAFactor
  double m_ROAFactor;
 
  // Sensnet module
  sensnet_t *sensnet;
 
  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int m_numberOfLadars;
  Ladar m_ladars[16];

  /// Binds variables to sparrowHawk display
  void sparrowInit();

  /// Initialize sensnet
  int initSensnet();

  /// Clean up sensnet
  int finiSensnet();

  /// updates the estimated delay
  void updateDelay();

  /// Reads information from the ladar feeder
  void updateLadars(LadarRangeBlob* blob, int* blobLen);

  void updateClosestObstacle();

  // Checks whether a given point in vehicle frame is in the risc zone
  bool inRiscZone(float vfx, float vfy, float* arcDistance);

  /// Updates the estimeated timeToCollision
  void updateTimeToCollision();

  /// Updates the factor by which nominal reference speed is multiplied
  void updateROAFactor();

  // Updates age used to calculate delay
  void updateAge(int index);

  // Updates ages used to calculate delay
  void updateAllAges();
  
public:
  ROA(FollowerControlStatus* controlStatus, FollowerLogger* internalLogger);  

  void updateTimeStamp(int index, unsigned long long timeStamp);
  

  /// Invoked from the lateral controller. 
  /// Updates the lateral error used in the ROA module
  void updateLateralError(double lateralError);

  void updateSpeed(double speed);

  void updateSteerAngle(double steerAngle);

  /// The actual ROA function
  void limitReferenceSpeed(double* referenceVelocity);

  /// Sets the parameters of the ROA
  void setParams(ROAParams params);
};

#endif //ROA_UJGGIOSDBUYCBIUSDGNBDSHGIUCBUIGDJK

