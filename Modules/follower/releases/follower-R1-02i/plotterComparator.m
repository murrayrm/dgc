FILE_GROUP = 5;

inFileArray = {

% West
           {'~/gcDriveCharacterization/adriver_20070704_12_brake0';
            '~/gcDriveCharacterization/adriver_20070704_14_brake01';
            '~/gcDriveCharacterization/adriver_20070704_16_brake02';
            '~/gcDriveCharacterization/adriver_20070704_18_brake03';
            '~/gcDriveCharacterization/adriver_20070704_20_brake04';
            '~/gcDriveCharacterization/adriver_20070704_22_brake05';
            '~/gcDriveCharacterization/adriver_20070704_24_brake06';
            '~/gcDriveCharacterization/adriver_20070704_26_brake07';
            '~/gcDriveCharacterization/adriver_20070704_28_brake08';
            '~/gcDriveCharacterization/adriver_20070704_30_brake09';
            '~/gcDriveCharacterization/adriver_20070704_32_brake1'};


% East
           {'~/gcDriveCharacterization/adriver_20070704_13_brake0';
            '~/gcDriveCharacterization/adriver_20070704_15_brake01';
            '~/gcDriveCharacterization/adriver_20070704_17_brake02';
            '~/gcDriveCharacterization/adriver_20070704_19_brake03';
            '~/gcDriveCharacterization/adriver_20070704_21_brake04';
            '~/gcDriveCharacterization/adriver_20070704_23_brake05';
            '~/gcDriveCharacterization/adriver_20070704_25_brake06';
            '~/gcDriveCharacterization/adriver_20070704_27_brake07';
            '~/gcDriveCharacterization/adriver_20070704_29_brake08';
            '~/gcDriveCharacterization/adriver_20070704_31_brake09';
            '~/gcDriveCharacterization/adriver_20070704_33_brake1'};


% West
           {'~/gcDriveCharacterization/adriver_20070704_10_gasStep00';
            '~/gcDriveCharacterization/adriver_20070704_08_gasStep005';
            '~/gcDriveCharacterization/adriver_20070704_06_gasStep01';
            '~/gcDriveCharacterization/adriver_20070704_04_gasStep0125';
            '~/gcDriveCharacterization/adriver_20070704_02_gasStep015'};

% East
           {'~/gcDriveCharacterization/adriver_20070704_11_gasStep00';
            '~/gcDriveCharacterization/adriver_20070704_09_gasStep005';
            '~/gcDriveCharacterization/adriver_20070704_07_gasStep01';
            '~/gcDriveCharacterization/adriver_20070704_05_gasStep0125';
            '~/gcDriveCharacterization/adriver_20070704_03_gasStep015';
            '~/gcDriveCharacterization/adriver_20070704_01_gasStep02'};

% West
           {'~/gcDriveCharacterization/adriver_20070704_10_gasStep00';
            '~/gcDriveCharacterization/adriver_20070704_08_gasStep005';
            '~/gcDriveCharacterization/adriver_20070704_06_gasStep01';
            '~/gcDriveCharacterization/adriver_20070704_04_gasStep0125';
            '~/gcDriveCharacterization/adriver_20070704_02_gasStep015';
            '~/gcDriveCharacterization/skynet2007_06_29_09_gasStep04';
            '~/gcDriveCharacterization/skynet2007_06_29_11_gasStep07.m'};

% East
           {'~/gcDriveCharacterization/adriver_20070704_11_gasStep00';
            '~/gcDriveCharacterization/adriver_20070704_09_gasStep005';
            '~/gcDriveCharacterization/adriver_20070704_07_gasStep01';
            '~/gcDriveCharacterization/adriver_20070704_05_gasStep0125';
            '~/gcDriveCharacterization/adriver_20070704_03_gasStep015';
            '~/gcDriveCharacterization/adriver_20070704_01_gasStep02';
            '~/gcDriveCharacterization/skynet2007_06_29_07_gasStep05';
            '~/gcDriveCharacterization/skynet2007_06_29_10_gasStep06'};
};   % inFiles
           
inFiles = inFileArray{FILE_GROUP};

speeds = {};
times = {};
for i = 1:length(inFiles)
    inputLog = inFiles{i};
    plotter;
    speeds{i} = speed;
    times{i} = VehicleStates(:,1) - accDir(1,1);
end


colors = 'bgrcmyk';

% Speed vs time
figure(20)
clf
hold on
for i = 1:length(inFiles)
    plot(times{i}*1e-6, speeds{i}, [colors(mod(i-1,7)+1) '*-'])
end
legend(inFiles)
xlabel('time (s)')
ylabel('speed (m/s)')
grid on 
        
        
% Acceleration vs speed
AVERAGE_SAMPS = 3;
START_TIME = 0*1e6;
accVels = {};
accs = {};
for i = 1:length(inFiles)
    I = find(times{i} > START_TIME);
    I = I(1:end-AVERAGE_SAMPS);
    accVels{i} = speeds{i}(I);
    accs{i} = [];
    for j = 1:length(I)
        accs{i}(j) = (speeds{i}(I(j)+AVERAGE_SAMPS)-speeds{i}(I(j)-AVERAGE_SAMPS)) / (times{i}(I(j)+AVERAGE_SAMPS)-times{i}(I(j)-AVERAGE_SAMPS)) * 1e6;
    end
end

figure(21)
clf
hold on
for i = 1:length(inFiles)
    plot(accVels{i}, accs{i}, [colors(mod(i-1,7)+1) '*-'])
end
legend(inFiles)
xlabel('speed (m/s)')
ylabel('acceleration (m/s^2)')
grid on


