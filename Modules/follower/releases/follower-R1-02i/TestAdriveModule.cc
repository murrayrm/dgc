#include "TestAdriveModule.hh"


TestAdriveModule::TestAdriveModule(int skynet_key) 
  : GcModule("testModule", &m_testStatus, &m_testDirective, 10000, 10000)
{
  cout<<"SKYNET KEY"<< skynet_key<<endl;
  m_followerInterfaceNF = AdriveCommand::generateNorthface(skynet_key, this);
  cout<<"generated northface"<<endl;
}

/* Arbitrate will create and send responses to Follower */
void TestAdriveModule::arbitrate(ControlStatus *, MergedDirective *) 
{
  cout<<"in arbitrate "<<endl;
  generateAdriveFaultCombinations();
  cout<<"called generateAdriveFaultCombinations "<<endl;
  usleep(330000);
};

void TestAdriveModule::control(ControlStatus *, MergedDirective *) 
{
  cout<<"in control "<<endl;
  usleep(330000);
};



/* Utility function for generating any number of adrive fault combinations */
void TestAdriveModule::generateAdriveFaultCombinations() {

  vector<AdriveResponse> faultCombs; 

  /* Calculate a random number of fault combinations, for now say 1000 */
  unsigned int numCombs = 8; 
  AdriveResponse response, vResponse;

  srand((unsigned)time(0));
  int randInt1, randInt2, randInt3, randInt4, id = 0; 

  for (unsigned int i=0; i < numCombs; i++) {
    response.id = id++;
    /* Pick the status randomly */
    randInt1 = rand()%4; 
    cout<<endl;

    cout<<"STATUS "<<randInt1<<endl;
    response.status = getStatusFromInteger(randInt1);

    if (GcInterfaceDirectiveStatus::REJECTED == response.status || GcInterfaceDirectiveStatus::FAILED == response.status) { 
      /* Pick the reason randomly */
      randInt2 = rand()%4;
      cout<<endl;
      cout<<"REASON "<<randInt2<<endl;
      response.reason = getReasonFromInteger(randInt2);    
    }
    faultCombs.push_back(response);
  }

  /* Print the combinations for the user */
  for (unsigned int i=0; i < faultCombs.size(); i++) {
    vResponse = (AdriveResponse)faultCombs.back(); 
    cout<<"["<<i<<"]= "<< vResponse.toString()<<endl; 

    m_followerInterfaceNF->sendResponse(&(AdriveResponse)faultCombs.back(), faultCombs.back().id);
    faultCombs.pop_back();

    /* Sleep for random increments of time at "random intervals" */
    randInt3 = rand()%9;
    randInt4 = rand()%13;
    if (1 == randInt3 || 4 == randInt3) {
      usleep(randInt4 * 10000);
    }
  }
}

void TestAdriveModule::startModule() {
  arbitrate(&m_testStatus, &m_testDirective);
  control(&m_testStatus,&m_testDirective);
}

GcInterfaceDirectiveStatus::Status TestAdriveModule::getStatusFromInteger(int someInt) { 

  GcInterfaceDirectiveStatus::Status status; 

  switch(someInt) {
  case 0:
    status = GcInterfaceDirectiveStatus::COMPLETED;
    cout<<"COMPLETED";
    break; 
  case 1: 
    status = GcInterfaceDirectiveStatus::FAILED;
    cout<<"FAILED";
    break; 
  case 2: 
    status = GcInterfaceDirectiveStatus::ACCEPTED;
    cout<<"ACCEPTED";
    break; 
  case 3:
    status = GcInterfaceDirectiveStatus::REJECTED;
    cout<<"REJECTED";
    break; 
  default: 
    status = GcInterfaceDirectiveStatus::ACCEPTED;
    cout<<"DEFAULT";
    break;
  }
  return status;    
}

AdriveFailure TestAdriveModule::getReasonFromInteger(int someInt) 
{ 
  AdriveFailure reason; 

  switch(someInt) {
  case 0:
    reason = NotInitialized;
    cout<<"  NotInitialized"<<endl;
    break; 
  case 1: 
    reason = OutOfRange;
    cout<<"  OutOfRange"<<endl;
    break; 
  case 2: 
    reason = VehiclePaused;
    cout<<"  VehiclePaused"<<endl;
    break; 
  case 3:
    reason = VehicleNotStopped;
    cout<<"  VehicleNotStopped"<<endl;
    break; 
  default: 
    reason = NotInitialized;
    cout<<"  Default"<<endl;
    break;
  }
  return reason; 
}

