%log = load('/home/users/soltesz/followerInternalLog/2007-10-14-Sun-eltoro/followerInternalLog-.2007-10-14-Sun-16-16.log');
log = load('/home/users/soltesz/followerInternalLog/2007-10-19-Fri-eltoro/followerInternalLog-.2007-10-19-Fri-11-20.log');

%%% beginning of loading %%%

% time
timeStamp = log(:,1);  % 1

% Lateral variables 
          la_l1_gs      = log(:,2); % 2 
          la_l2_gs      = log(:,3);
          la_phiFF      = log(:,4);
          la_phi        = log(:,5);
          la_I          = log(:,6);

% Longitudinal variables
          lo_vRef       = log(:,7); % 7
          lo_aFF        = log(:,8);
          lo_speed      = log(:,9);
          lo_I          = log(:,10);
          lo_a          = log(:,11);
          lo_u          = log(:,12);

% Trajectory info          
          trajID                = log(:,13);  % 13
          closestIndex          = log(:,14);
          closestFractionToNext = log(:,15);
          
% Vehicle state
          curN          = log(:,16);  % 16  
          curNd         = log(:,17);
          curE          = log(:,18);
          curEd         = log(:,19);
          curYaw        = log(:,20);
          curYawRate    = log(:,21);
         
% Data for closest point on trajectory          
          closestN      = log(:,22);  % 22
          closestNd     = log(:,23);
          closestNdd    = log(:,24);
          closestE      = log(:,25);
          closestEd     = log(:,26);
          closestEdd    = log(:,27);
          
% Lateral parameters          
          la_l1         = log(:,28);  % 28  
          la_l1Scale    = log(:,29);
          la_l2         = log(:,30);
          la_l2Scale    = log(:,31);
          la_alpha      = log(:,32);
          la_delay      = log(:,33);
          
% Longitudinal parameters          
          lo_lV                 = log(:,34);  % 34 
          lo_lI                 = log(:,35);
          lo_alpha              = log(:,36);
          lo_delay              = log(:,37);
          lo_gasDeadU           = log(:,38);
          lo_gasDeadA           = log(:,39);
          lo_brakeDeadU         = log(:,40);
          lo_brakeDeadA         = log(:,41);
          lo_gasGain            = log(:,42);
          lo_brakeGain          = log(:,43);
          lo_gasVelocityGain    = log(:,44);
          lo_brakeVelocityGain  = log(:,45);
          lo_noGasSpeed         = log(:,46);
          lo_idleAcceleration   = log(:,47);
          
% Estop state          
          Estop         = log(:,48);  % 48
          
% Ages          

          roa_ages_STEERING_LOOP        = log(:,49);  % 49
          roa_ages_ACCELERATION_LOOP    = log(:,50);
          roa_ages_STEERING_STATE       = log(:,51);
          roa_ages_ACCELERATION_STATE   = log(:,52);
          roa_ages_TRAJECTORY           = log(:,53);
          roa_ages_VEHICLE_STATE        = log(:,54);

%% START: logs before 071021

          gear = log(:,55);  
          trajectoryDirection = log(:,56);
          crossTrackError = log(:,57);
          roaBraking = log(:,58);


%% END: logs before 071021

%% START: logs after 071021          
%          roa_ages_FOLLOWER_CYCLE       = log(:,55);  
%          
% Misc. added 071015          
%          gear = log(:,56);  % 56
%          trajectoryDirection = log(:,57);
%          crossTrackError = log(:,58);
%          roa_currentlyBraking = log(:,59);
%
% Misc. added 071020
%          roa_trigger = log(:,60);
%          roa_obstacleCollisionDistance = log(:,61);
%% END: logs after 071021


time = timeStamp-timeStamp(1,1);
%errorSign = sign(closestEd.*(curN-closestN)-closestNd.*(curE-closestE));
%crossError = errorSign.*sqrt((curN-closestN).^2+(curE-closestE).^2);

%beware of spurious outliers (probably where closestNd == closestEd == 0.0)
yawError = mod(curYaw-atan2(closestEd,closestNd)+pi/2,pi)-pi/2;

