#adriver.py
#author: Kristian Soltesz
#date: 2007-06-28

import sys
import os
import getopt
import signal
import time 

#display help msg
print '''
                *** adriver.py ***
----------------------------------------------------------------------
Options:

         -h      --help                  prints this message
         -c      --cmdfile      file     skynet log file to run
         -f      --field                 sets skynet key to 0

----------------------------------------------------------------------
'''

#parse cmd line opts using getopt
try:
  opts, args = getopt.getopt(sys.argv[1:], "hc:f", ["help", "cmdfile=", "field"])
except getopt.GetoptError:
  print 'Error: Wrong cmd line opt syntax.'
  print 'Exiting...\n'      
  sys.exit(2)

#defaults
cmdfile = None

#run through parsed cmd line opts
exit = False
skynetKey = '' #not yet used. to be implemented
for o, a in opts:
    if o in ("-h", "--help"):
      exit = True
    if o in ("-c", "--cmdfile"):
      cmdfile = a
    if o in ("-f", "--field"):
      skynetKey = a


#catch errors
if (cmdfile == None):
  print 'Error:   -c      --cmdfile         file    not specified.'
  exit = True

if (exit):
  print 'Exiting..\n'
  sys.exit(2)

#logging preferences
logPath = '/dgc/logs'  
timeStamp = time.strftime( '%Y-%m-%d-%a-%H-%M-%S')
outputLogName = '%s/adrivelog-%s-%s.log' % (logPath, cmdfile, timeStamp)
print "Log file: %s" %outputLogName 

#this is the stuff to do
actuators = '--disable-steer'
apps = [ \
('astate', '', 1, 'leela'), \
 ('skynet-logger', 'outputLogName', 1, 'gcfield'), \
 ('gcdrive', '--disable-command-timeout', 1, 'leela'), \
 ('skynet-player', cmdfile+' -j', 1, 'leela'), \
]

# ('gcdrive',  '--disable-command-timeout '+actuators, 1, 'leela'), \

#start a process and return its PID
def spawn ( cmd, nonsplitargs='' ):
  a = cmd.split()[0]
  wcmd = os.popen( 'which %s' % a ).read().strip()
  pid = os.spawnv( os.P_NOWAIT, wcmd, [wcmd] + cmd.split()[1:] + [nonsplitargs]) 
  return pid


#looks for already running processes
def lookForProcess( procName , psserver = None ):
  if psserver == '' or psserver == 'localhost':
    psserver = '`hostname`'
  if psserver != None:
    # running ps with the 'c' option will truncate the process name to 16 char
    psout = os.popen( "ssh -Y %s '{ ps axc --no-heading -o cmd,pid | grep %s; }'" % (psserver, procName[:15]) ).readlines()
  else:
    psout = os.popen( "runall 'ps axc --no-heading -o cmd,pid | grep %s'" % procName[:15] ).readlines()
  ret = []
  tmpout = []
  for i in psout:
    if i.strip() != '':
      tmpout.append(i.strip())
  psout = tmpout
  for i in psout:
    s = i.split()
    # running ps with the 'c' option will truncate the process name to 16 char
    if len(s) == 2 and s[0] == procName[:15]:
      ret.append( int(s[1]) )
  return (ret, psout)



#run the apps
def runApps(apps):
  #check for unavailable modules...      
  cwd = os.getcwd()
  #print cwd
  appdict = {}
  for i in apps:
    (appname, params, sleeptime, host) = i
    appdict[appname] = [appname, '', '', 0]   #(appname, fullpath, pid)

  bFoundAll = True
  for root, dirs, files in os.walk('/home/users/team/alice-hardware-team01/bin/i486-gentoo-linux/'):
    for name in files:
      if name in appdict.keys():
        #print 'found our target'
        #print os.path.join( root, name )
        lpath =  os.path.join( os.getcwd(), os.path.join(root,name) )
        if( os.path.islink(lpath) ):
          path =  os.readlink(lpath)  
          path = os.path.join(os.path.dirname(lpath), path)
          if( os.path.isfile(path) ):
            appdict[name][1] = lpath
          else:
            print 'found a broken link: %s -> %s ' % (lpath, path)
            bFoundAll = False
        else:
          appdict[name][1] = lpath
        #print appdict[name]

  print 
  print 

  for i in appdict.keys():
    if appdict[i][1] == '':
      print 'could not find path for %s' % i
      bFoundAll = False 
  if not bFoundAll:
    print 'unavailable modules, exiting'
    sys.exit()

  pids = []


  #check for already running instances of the processes
  print "Looking for instances of modules already running"
  for i in apps:
    (appname, params, sleeptime, host) = i
    print "Checking", appname, " ... ",
    a,b = lookForProcess(appname)
    if len(a) > 0:
      print "Other instances of %s found: " % appname, str(a)
      print "\n".join(b)
      print "Please take the appropriate action (none, sudo runall pkill, etc.), and \n" \
            "press return to continue (will not check again):"
      raw_input()
    else:
      print "ok"

  print
  for i in apps:
      (appname, params, sleeptime, host) = i
      path = appdict[appname][1]
      shcmd = '{ echo; source /etc/profile.dgc; cd "%(cwd)s"; "%(path)s" %(params)s; echo press enter to close; read; }' % locals()
      if host != 'localhost' and host != '' :
        cmd = '''xterm -rightbar -sb -T %(appname)s -e ssh -t -Y %(host)s''' % locals()
      else:
        cmd = '''xterm -rightbar -sb -T %(appname)s -e''' % locals()
      whichcmd= os.popen( 'which %s' % cmd.split()[0] ).read().strip()
      pid = spawn(  cmd, shcmd )
      print appname + ": " + str( pid )
      # print pid
      pids.append( pid )
      os.system( 'sleep %(sleeptime)d' % locals() )
  return pids	

#run the apps

pids = runApps(apps)

#kill off all started processes
print
print "enter 'c' (no quotes) to close all the windows for you"
theInput = raw_input()
if( theInput == 'c' ):
  print "killing processes: " + str(pids)
  for i in pids:
    os.kill( i, signal.SIGTERM )
else:
  print "xterm pids left open: ", str(pids)


#now that the processes are running, we would like to give skynet player attention in order to start playback.
# Preferably, this should be done without user interaction: i.e. the skynet-player should receive
# >1
# >s1

#then we wait until skynet-player exits
#if we get a 'c' from the keyboard, all started processes should be killed, and the script should exit.


#comment: adriveInterface.sendDirective(directive*)
