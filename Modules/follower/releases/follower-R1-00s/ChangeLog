Tue Aug  7 23:13:26 2007	Kristian Soltesz (soltesz)

	* version R1-00s
	BUGS:  
	New files: TestAdriveModule.cc TestAdriveModule.hh
		UT_FollowerFaults.cc
	FILES: Follower.cc(32535), Follower.hh(32535),
		FollowerMain.cc(32535), FollowerUtils.cc(32535),
		FollowerUtils.hh(32535), LateralController.cc(32535),
		LongitudinalController.cc(32535)
	Added communication for fault handling between Follower/Adrive and
	Follower/Planner. 

	FILES: Follower.cc(32538), Follower.hh(32538)
	Added comments to Follower.cc Especially, a bunch of FIXME:s were
	added.

	FILES: Follower.cc(32560), Follower.hh(32560),
		FollowerUtils.cc(32560), FollowerUtils.hh(32560),
		LateralController.cc(32560), LateralController.hh(32560),
		LongitudinalController.cc(32560),
		LongitudinalController.hh(32560),
		TransmissionController.cc(32560),
		TransmissionController.hh(32560)
	This is the firt stable version after CSS merge. There are still a
	lot of FIXME:s in the code, and lateral integral won't work as it
	is now. 

	FILES: Follower.cc(32561), followerDisplay.dd(32561)
	updated CSS FIXME:s in Follower.cc

	FILES: Follower.cc(32562), Follower.hh(32562),
		FollowerMain.cc(32562), FollowerUtils.hh(32562)
	removed Follower::quit and added Exiting to the enum in
	FollowerOpStates. Just as with quit (using process control), it
	segfault-core-dumps when you hit q or press the quit button. This
	will be resolved, but is not of highest priority. 

	New files: TestAdriveModule.cc TestAdriveModule.hh
		UT_FollowerFaults.cc
	FILES: Follower.cc(31781), Follower.hh(31781),
		FollowerMain.cc(31781), Makefile.yam(31781)
	Changed Follower to support faults from adrive.   Added a unit test
	that generates a random set of adrive responses including faults
	and completions and sends them to Follower.  Changed Makefile.yam
	to compile FollowerMain.cc as a binary and follower supporting
	files as a library.    

	FILES: Follower.cc(31830), Makefile.yam(31830)
	Separated out TestAdriveModule from UTFollowerFaults.  Follower has
	new prints for messages from Adrive. 

	FILES: Follower.cc(32508), Follower.hh(32508),
		FollowerUtils.cc(32508), FollowerUtils.hh(32508),
		LateralController.cc(32508),
		LongitudinalController.cc(32508)
	Implemented fault handling communication between Adrive and
	Follower via CSS. 

Mon Aug  6 23:02:13 2007	Kristian Soltesz (soltesz)

	* version R1-00r
	BUGS:  
	FILES: Follower.cc(32427)
	pausing on empty or NULL trajectory

	FILES: LateralController.cc(32446)
	added a in front of tan

Mon Aug  6 22:00:12 2007	Kristian Soltesz (soltesz)

	* version R1-00q
	BUGS:  
	FILES: LateralController.cc(32412), LateralController.hh(32412),
		followerDisplay.dd(32412)
	Limiting max lateral acceleration. Modified sparrow, since - for
	some reason, process control does only display the last 24 rows of
	my table.

	FILES: paramfile(32415)
	Changed longitudinal FF parameter.

Mon Aug  6 19:41:06 2007	Kristian Soltesz (soltesz)

	* version R1-00p
	BUGS:  
	FILES: Follower.cc(32358), TransmissionController.cc(32358),
		TransmissionController.hh(32358)
	initialize transmissioncontroller to current gear at construction.

Mon Aug  6 12:15:16 2007	Kristian Soltesz (soltesz)

	* version R1-00o
	BUGS:  
	FILES: Follower.cc(32231)
	If we are stopped & Estop paused, we do not steer.

Mon Aug  6  1:05:58 2007	Kristian Soltesz (soltesz)

	* version R1-00n
	BUGS:  
	FILES: Follower.cc(32185), Follower.hh(32185),
		LateralController.cc(32185), LateralController.hh(32185),
		LongitudinalController.cc(32185),
		LongitudinalController.hh(32185)
	merging

	FILES: Follower.cc(32178), Follower.hh(32178),
		LateralController.cc(32178), LateralController.hh(32178),
		LongitudinalController.cc(32178),
		LongitudinalController.hh(32178)
	integral antiwindup for estop. rather hacky solution

Sat Aug  4  1:41:59 2007	Magnus Linderoth (mlinderoth)

	* version R1-00m
	BUGS:  
	FILES: paramfile(31928)
	Change default value of paramter lo_alpha from 1.0 to 0.1

Thu Aug  2 23:59:23 2007	Kristian Soltesz (soltesz)

	* version R1-00l
	BUGS:  
	New files: TransmissionController.cc TransmissionController.hh
	Deleted files: ShiftController.cc ShiftController.hh
	FILES: Follower.cc(31579), Follower.hh(31579), Makefile.yam(31579),
		followerDisplay.dd(31579)
	Proper SF shifting without ShiftController.

	FILES: Follower.cc(31582), Follower.hh(31582)
	minor mods. verified in sim.

	FILES: Follower.cc(31590), LateralController.cc(31590),
		LateralController.hh(31590),
		LongitudinalController.cc(31590),
		LongitudinalController.hh(31590)
	adopted the notion of paused, instead of reading from estop. not
	fully implemented.

	FILES: Follower.cc(31592), Follower.hh(31592), Makefile.yam(31592)
	new transmission controller verified working. needs comments.

	FILES: Follower.cc(31593), Follower.hh(31593),
		LateralController.cc(31593), LateralController.hh(31593),
		LongitudinalController.cc(31593),
		LongitudinalController.hh(31593)
	transmission works the way it should. Fault handling needs to be
	fully implemented.

	FILES: Follower.cc(31594), Follower.hh(31594)
	minor changes. completed commenting.

	FILES: Follower.cc(31611), LateralController.cc(31611),
		LateralController.hh(31611),
		LongitudinalController.cc(31611),
		LongitudinalController.hh(31611)
	changed the controllers to write the right type of responses.
	Actually, they are not yet writing anything useful back.

	FILES: Follower.cc(31612), followerDisplay.dd(31612)
	pause on trajectory timeout and pause release verified working in
	simulation.

	FILES: Follower.cc(31613), Follower.hh(31613)
	North face stuff is in place, however, commented out since it is
	not working

	FILES: FollowerMain.cc(31225), FollowerUtils.hh(31225)
	cleaned up comments.

Tue Jul 31 15:12:20 2007	Kristian Soltesz (soltesz)

	* version R1-00k
	BUGS:  
	FILES: Follower.cc(30960), Follower.hh(30960),
		FollowerMain.cc(30960), LateralController.cc(30960),
		LateralController.hh(30960),
		LongitudinalController.cc(30960),
		LongitudinalController.hh(30960),
		ShiftController.cc(30960), ShiftController.hh(30960),
		follower.ggo(30960), followerDisplay.dd(30960),
		paramfile(30960)
	It is now possible to write status responses directly from the
	controllers. Added parametersdescribing the max errors allowed
	before a replan from Alice current state is requested.

	FILES: Follower.cc(31120), Follower.hh(31120),
		FollowerMain.cc(31120), FollowerUtils.cc(31120),
		FollowerUtils.hh(31120), follower.ggo(31120)
	Removed warnings. Prepared for NF, SF.

Mon Jul 30 19:54:26 2007	Kristian Soltesz (soltesz)

	* version R1-00j
	BUGS:  
	FILES: Follower.cc(30925), LongitudinalController.cc(30925)
	Truncated longitudinal control signal to [-1,1]. Rebound 'q' to
	call Follower::sparrowQuit().

	FILES: Follower.cc(30934), LongitudinalController.cc(30934)
	Fixed getting stopped bug.

Fri Jul 27 14:45:25 2007	Kristian Soltesz (soltesz)

	* version R1-00i
	BUGS:  
	FILES: Follower.cc(30741), Follower.hh(30741),
		LateralController.cc(30741), LateralController.hh(30741),
		LongitudinalController.cc(30741),
		LongitudinalController.hh(30741),
		ShiftController.cc(30741), ShiftController.hh(30741),
		followerDisplay.dd(30741)
	Fixed zero vel steer bug. Fixed shifting in simulator. Changed the
	way commands are passed to gcdrive.

	FILES: FollowerMain.cc(30710), LateralController.cc(30710),
		LateralController.hh(30710), follower.ggo(30710),
		followerDisplay.dd(30710), paramfile(30710)
	Added option to track trajectory with front wheels instead of rear
	wheels

	FILES: FollowerMain.cc(30723), FollowerUtils.cc(30723),
		FollowerUtils.hh(30723), LateralController.cc(30723),
		LongitudinalController.cc(30723), follower.ggo(30723)
	Added option to use feedforward control only, no feedback

	FILES: LongitudinalController.cc(30508)
	Stops at low reference speeds

	FILES: ShiftController.cc(30511)
	Changed bug regarding directivesToSend.

Thu Jul 26 12:18:24 2007	Kristian Soltesz (soltesz)

	* version R1-00h
	BUGS:  
	FILES: Follower.cc(30480), Follower.hh(30480),
		FollowerMain.cc(30480), FollowerUtils.cc(30480),
		FollowerUtils.hh(30480), LateralController.cc(30480),
		LateralController.hh(30480), Makefile.yam(30480),
		follower.ggo(30480), followerDisplay.dd(30480)
	Not core dumping. Now with visual aid for lateral controller.

Tue Jul 24 23:00:39 2007	Kristian Soltesz (soltesz)

	* version R1-00g
	BUGS:  
	FILES: Follower.cc(30161), Follower.hh(30161),
		FollowerMain.cc(30161), FollowerUtils.cc(30161),
		follower.ggo(30161)
	changed away from dynamic casts. Changed from isA to hasA
	CTrajTalker.

Tue Jul 24  2:19:32 2007	Magnus Linderoth (mlinderoth)

	* version R1-00f
	BUGS:  
	New files: ShiftController.cc ShiftController.hh
	FILES: Follower.cc(29976), Follower.hh(29976),
		FollowerMain.cc(29976), FollowerUtils.cc(29976),
		LateralController.cc(29976), LateralController.hh(29976),
		LongitudinalController.cc(29976),
		LongitudinalController.hh(29976)
	SparrowHawk pointer now lives in FollowerUtils.hh.

	FILES: Follower.cc(30009), FollowerLogger.cc(30009),
		FollowerMain.cc(30009), FollowerUtils.cc(30009),
		FollowerUtils.hh(30009), LateralController.cc(30009),
		LongitudinalController.cc(30009)
	Added printing support for sparrow/console.

	FILES: Follower.cc(30023), FollowerLogger.cc(30023),
		FollowerMain.cc(30023), FollowerUtils.hh(30023),
		LateralController.cc(30023),
		LongitudinalController.cc(30023)
	Defined macro so prints to console/sparrow are made on one row,
	using printConsole().

	FILES: Follower.cc(30033), Follower.hh(30033),
		LateralController.cc(30033), LateralController.hh(30033),
		LongitudinalController.cc(30033),
		LongitudinalController.hh(30033), Makefile.yam(30033),
		followerDisplay.dd(30033)
	Added shifting controller and surrounding logics. The control
	function of the shifting controller is not yet written. This commit
	has not been tested in simulation.

	FILES: Follower.cc(30090), Follower.hh(30090),
		FollowerMain.cc(30090), followerDisplay.dd(30090)
	shifting works in simulation, using static trajectories.

	FILES: Follower.cc(30091)
	passing EstopState to controllers

	FILES: Follower.cc(30093)
	changed indentation.

	FILES: FollowerMain.cc(30092), LateralController.cc(30092),
		LateralController.hh(30092),
		LongitudinalController.cc(30092),
		LongitudinalController.hh(30092), follower.ggo(30092),
		paramfile(30092)
	Added integral anti-windup on lateral controller and stop of
	integration when Estop is not run

	FILES: FollowerUtils.hh(29977)
	Forgot this in previous ci.

	FILES: LateralController.cc(30038)
	removed deprecated comments.

	FILES: LongitudinalController.cc(30036)
	Moved misplaced parentheses in feed forward acceleration
	computation.

	FILES: LongitudinalController.cc(30095)
	Added integral anti-windup

	FILES: followerDisplay.dd(30037)
	Added shifting information to ShiftController and sparrow.

Thu Jul 19 23:53:19 2007	Kristian Soltesz (soltesz)

	* version R1-00e
	BUGS:  
	New files: FollowerUtils.cc paramfile
	Deleted files: CmdArgs.cc CmdArgs.hh argfile
	FILES: AdriveLogGen.cc(29565), Follower.cc(29565),
		Follower.hh(29565), FollowerMain.cc(29565),
		FollowerUtils.hh(29565), LateralController.cc(29570),
		LateralController.hh(29570),
		LongitudinalController.cc(29570),
		LongitudinalController.hh(29570), SkynetLogParser.cc(29565)
	Cleaned up code and added propper comments.

	FILES: Follower.cc(29528), Follower.hh(29528),
		FollowerMain.cc(29528), Makefile.yam(29528),
		follower.ggo(29528), followerDisplay.dd(29528)
	Quits cleanly. Added paramfile auto search

	FILES: Follower.cc(29560), Follower.hh(29560),
		FollowerLogger.cc(29560), FollowerMain.cc(29560),
		FollowerUtils.hh(29560), LateralController.cc(29560),
		LateralController.hh(29560),
		LongitudinalController.cc(29560),
		LongitudinalController.hh(29560), Makefile.yam(29560)
	Resturcured the code. CmdArg files gone.

	FILES: Follower.cc(29805), FollowerMain.cc(29805)
	friday test setup

	FILES: FollowerLogger.cc(29573), FollowerLogger.hh(29573)
	Improved comments

	FILES: FollowerLogger.hh(29575), SkynetLogParser.cc(29575)
	Corrected comments

	FILES: FollowerMain.cc(29534)
	Removed bogus comment.

	FILES: FollowerMain.cc(29563), FollowerUtils.hh(29563),
		LateralController.cc(29563)
	Minor changes.

	FILES: FollowerUtils.hh(29562), LateralController.cc(29562)
	minor include bug fix

Wed Jul 18  5:52:58 2007	Magnus Linderoth (mlinderoth)

	* version R1-00d
	BUGS:  
	New files: FollowerLogger.cc FollowerLogger.hh
		trajs-SantaAnita/halfCircles16m4mps.m
		trajs-SantaAnita/halfCircles16m4mps.traj
		trajs-SantaAnita/halfCircles50m4mps.m
		trajs-SantaAnita/halfCircles50m4mps.traj
		trajs-SantaAnita/halfCircles50m7mps.m
		trajs-SantaAnita/halfCircles50m7mps.traj
		trajs-SantaAnita/halfCircles8m2mps.m
		trajs-SantaAnita/halfCircles8m2mps.traj
		trajs-SantaAnita/leftTurn100m_10mps.m
		trajs-SantaAnita/leftTurn100m_10mps.traj
		trajs-SantaAnita/leftTurn100m_3mps.m
		trajs-SantaAnita/leftTurn100m_3mps.traj
		trajs-SantaAnita/leftTurn10m_1mps.m
		trajs-SantaAnita/leftTurn10m_1mps.traj
		trajs-SantaAnita/leftTurn10m_2mps.m
		trajs-SantaAnita/leftTurn10m_2mps.traj
		trajs-SantaAnita/leftTurn20m_1mps.m
		trajs-SantaAnita/leftTurn20m_1mps.traj
		trajs-SantaAnita/leftTurn20m_2mps.m
		trajs-SantaAnita/leftTurn20m_2mps.traj
		trajs-SantaAnita/leftTurn20m_3mps.m
		trajs-SantaAnita/leftTurn20m_3mps.traj
		trajs-SantaAnita/leftTurn20m_4mps.m
		trajs-SantaAnita/leftTurn20m_4mps.traj
		trajs-SantaAnita/speed02Ramp1.m
		trajs-SantaAnita/speed02Ramp1.traj
		trajs-SantaAnita/speed05Ramp1.m
		trajs-SantaAnita/speed05Ramp1.traj
		trajs-SantaAnita/speed11Ramp1.m
		trajs-SantaAnita/speed11Ramp1.traj
		trajs-SantaAnita/speed13Ramp1.m
		trajs-SantaAnita/speed13Ramp1.traj
		trajs-SantaAnita/speed15Ramp1.m
		trajs-SantaAnita/speed15Ramp1.traj
		trajs-SantaAnita/speed15Ramp2.m
		trajs-SantaAnita/speed15Ramp2.traj
		trajs-SantaAnita/speed15Ramp3.m
		trajs-SantaAnita/speed15Ramp3.traj
		trajs-SantaAnita/speed1Ramp1.m
		trajs-SantaAnita/speed1Ramp1.traj
		trajs-SantaAnita/speed2Ramp1.m
		trajs-SantaAnita/speed2Ramp1.traj
		trajs-SantaAnita/speed3Ramp1.m
		trajs-SantaAnita/speed3Ramp1.traj
		trajs-SantaAnita/speed4Ramp1.m
		trajs-SantaAnita/speed4Ramp1.traj
		trajs-SantaAnita/speed5Ramp1.m
		trajs-SantaAnita/speed5Ramp1.traj
		trajs-SantaAnita/speed6Ramp05.m
		trajs-SantaAnita/speed6Ramp05.traj
		trajs-SantaAnita/speed6Ramp1.m
		trajs-SantaAnita/speed6Ramp1.traj
		trajs-SantaAnita/speed6Ramp2.m
		trajs-SantaAnita/speed6Ramp2.traj
		trajs-SantaAnita/speed6Ramp3.m
		trajs-SantaAnita/speed6Ramp3.traj
		trajs-SantaAnita/speed7Ramp1.m
		trajs-SantaAnita/speed7Ramp1.traj
		trajs-SantaAnita/speed7Ramp1short.m
		trajs-SantaAnita/speed7Ramp1short.traj
		trajs-SantaAnita/speed9Ramp1.m
		trajs-SantaAnita/speed9Ramp1.traj
		trajs-SantaAnita/speed9Ramp1short.m
		trajs-SantaAnita/speed9Ramp1short.traj
		trajs-SantaAnita/speedRamps3_05_3.m
		trajs-SantaAnita/speedRamps3_05_3.traj
		trajs-SantaAnita/speedRamps3_0_3.m
		trajs-SantaAnita/speedRamps3_0_3.traj
	FILES: CmdArgs.cc(29260), CmdArgs.hh(29260), Follower.cc(29260),
		Follower.hh(29260), FollowerMain.cc(29260),
		LateralController.cc(29260), LateralController.hh(29260),
		LongitudinalController.hh(29260), Makefile.yam(29260),
		follower.ggo(29260)
	Added frame work for logging

	FILES: CmdArgs.cc(29372), CmdArgs.hh(29372), Follower.cc(29372),
		FollowerMain.cc(29372), LateralController.cc(29372),
		LateralController.hh(29372),
		LongitudinalController.cc(29372),
		LongitudinalController.hh(29372), followerDisplay.dd(29372)
	Further polishing of sparrow. Still some stuff to fix. General Code
	clean up.

	FILES: CmdArgs.cc(29380), CmdArgs.hh(29380),
		FollowerMain.cc(29380), LongitudinalController.cc(29380),
		follower.ggo(29380), followerDisplay.dd(29380)
	St Anita test configuration.

	FILES: Follower.cc(29203), Follower.hh(29203),
		LateralController.cc(29203), LateralController.hh(29203),
		LongitudinalController.cc(29203),
		LongitudinalController.hh(29203)
	Added functionality for changing gears.

	FILES: Follower.cc(29255), FollowerMain.cc(29255),
		LateralController.cc(29255), LateralController.hh(29255),
		LongitudinalController.cc(29255),
		LongitudinalController.hh(29255), follower.ggo(29255)
	Lateral controller gets actuatorstate. Controllers get pointer to
	sparrow. Removed pxR, pyR, pxFF, pyFF vectors.

	FILES: Follower.cc(29270), Follower.hh(29270),
		FollowerMain.cc(29270), LateralController.cc(29270),
		followerDisplay.dd(29270)
	Merged with Magnus. Lateral controller parameters can be edit from
	sparrow. Magnus has added logging functionality.

	FILES: Follower.cc(29274), LateralController.cc(29274),
		LateralController.hh(29274)
	Added logging capabilities to LateralController

	FILES: Follower.cc(29304), LateralController.cc(29304),
		LongitudinalController.cc(29304),
		LongitudinalController.hh(29304)
	Added logging capabilities to Longitudinal controller and removed
	bug making the logged currN always be zero

	FILES: FollowerMain.cc(29384), LateralController.cc(29384),
		LateralController.hh(29384),
		LongitudinalController.cc(29384), argfile(29384),
		follower.ggo(29384)
	Added integral action to lateral controller

	FILES: LateralController.cc(29345), LateralController.hh(29345),
		LongitudinalController.cc(29345),
		LongitudinalController.hh(29345), follower.ggo(29345),
		followerDisplay.dd(29345)
	SparrowHawk display works. Needs code cleanup.

	FILES: followerDisplay.dd(29381)
	minor layout mod.

Mon Jul 16 12:38:00 2007	Kristian Soltesz (soltesz)

	* version R1-00c
	BUGS:  
	New files: followerDisplay.dd
	FILES: CmdArgs.cc(29110), CmdArgs.hh(29110), Follower.cc(29110),
		Follower.hh(29110), FollowerMain.cc(29110),
		LateralController.cc(29110), LateralController.hh(29110),
		LongitudinalController.cc(29110), Makefile.yam(29110)
	Merging with mlinderoth to make release.

	FILES: Follower.cc(28896), Makefile.yam(28896),
		SkynetLogParser.cc(28896), plotter.m(28896)
	Restored functioning version from directory of mlinderoth

	FILES: Makefile.yam(28869)
	Cleaning up...

	FILES: Makefile.yam(28974)
	minor edit

	FILES: Follower.cc(28896), Makefile.yam(28896),
		SkynetLogParser.cc(28896), plotter.m(28896)
	Restored functioning version from directory of mlinderoth

	FILES: Makefile.yam(28869)
	Cleaning up...

	FILES: Makefile.yam(28974)
	minor edit

Wed Jun 27 14:07:27 2007	Kristian Soltesz (soltesz)

	* version R1-00a
	BUGS:  
	New files: AdriveLogGen.cc SkynetLogParser.cc plotter.m
	FILES: Makefile.yam(28253)
	First draft of AdriveCommandExtractor

	FILES: Makefile.yam(28294)
	Extended AdriveCommandExtractor to SkynetLogParser

	FILES: Makefile.yam(28307)
	skynetlogparser works. AdriveLogGen compiles and runs but needs to
	be verified.

	FILES: Makefile.yam(28327)
	Added doxygen generation.

Thu Jun 21 17:54:31 2007	Kristian Soltesz (soltesz)

	* version R1-00
	Created follower module.























