delay = 0;

L = 3.55;
l1 = 3.55;
l2 = 4;

s = tf('s');

G = 1/(L*l2*s^2 + (l1+l2)*s);
G.InputDelay = delay;
figure(234)
nyquist(G)
axis equal
figure(235)
margin(G)



l1 = 0.05:0.5:8;
l2 = .5:.5:30;
[L1,L2] = meshgrid(l1,l2);


%w = sqrt((-L1.^2 + sqrt(L1.^4 + 4*L^2*L2.^2)) ./ (2*L^2*L2.^2));
w = sqrt((-(L1+L2).^2 + sqrt((L1+L2).^4 + 4*L^2*L2.^2)) ./ (2*L^2*L2.^2));
%w = sqrt((L1.^2 + sqrt(L1.^4 + 4*L^2*L2.^2)) ./ (2*L^2*L2.^2));
figure(236);clf;hold on
colormap([0 0 0])
contour(L1,L2,w)
mesh(L1,L2,w)
grid on
title('Cuttoff freq. for different parameters l_1, l_2')
xlabel('l_1 / m')
ylabel('l_2 / m')
zlabel('\omega_c / rad m^{-1}')

%phaseRad = pi/2 - atan(L*L2.*w ./ L1);
phaseRad = pi/2 - atan(L*L2.*w ./ (L1+L2));
%phaseRad = atan(w.*L1);
figure(237);clf;hold on
contour(L1,L2,phaseRad * 180 / pi)
mesh(L1,L2,phaseRad * 180 / pi)
grid on
title('Phase margin for different parameters l_1, l_2')
xlabel('l_1 / m')
ylabel('l_2 / m')
zlabel('\phi_m / ^{\circ}')

maxDelay = phaseRad ./ w;
figure(238);clf;hold on
contour(L1,L2,maxDelay)
mesh(L1,L2,maxDelay)
grid on
title('Maximum allowed delay (in meters) for stability')
xlabel('l_1 / m')
ylabel('l_2 / m')
zlabel('Delay / m')

maxDelay30 = (phaseRad - 30*pi/180) ./ w;
figure(239);clf;hold on
contour(L1,L2,maxDelay30)
mesh(L1,L2,maxDelay30)
grid on
surface(L1,L2,zeros(size(L1)))
title('Maximum allowed delay (in meters) for \phi_m > 30^{\circ}')
xlabel('l_1 / m')
ylabel('l_2 / m')
zlabel('delay / m')

maxSpeed = maxDelay / 0.4;
figure(240);clf;hold on
contour(L1,L2,maxSpeed)
mesh(L1,L2,maxSpeed)
grid on
title('Maximum allowed speed for stability')
xlabel('l_1 / m')
ylabel('l_2 / m')
zlabel('Speed / ms^{-1}')

maxSpeed30 = maxDelay30 / 0.4;
figure(241);clf;hold on
colormap([0 0 0])
contour(L1,L2,maxSpeed)
mesh(L1,L2,maxSpeed)
grid on
title('Maximum allowed speed for phase margin 30^{\circ}')
xlabel('l_1 / m')
ylabel('l_2 / m')
zlabel('Speed / ms^{-1}')

figure(242);clf;hold on
I = find(l1 == 3.55);
plot(l2, maxSpeed30(:,I), l2, 0.5*l2)
grid on

% Root locus
loc_l1 = 3.55;
loc_l2 = 1./[1/50:.01:1/.01];
rloc1 = (-(loc_l1+loc_l2) + sqrt((loc_l1+loc_l2).^2 - 4*L*loc_l2))./(2*L*loc_l2);
rloc2 = (-(loc_l1+loc_l2) - sqrt((loc_l1+loc_l2).^2 - 4*L*loc_l2))./(2*L*loc_l2);
figure(243);clf;hold on
plot(real(rloc1), imag(rloc1), 'b*-',...
     real(rloc2), imag(rloc2), 'g*-')

% Root locus
loc_l1 = [0:.01:15];
loc_l2 = 5;
rloc1 = (-(loc_l1+loc_l2) + sqrt((loc_l1+loc_l2).^2 - 4*L*loc_l2))./(2*L*loc_l2);
rloc2 = (-(loc_l1+loc_l2) - sqrt((loc_l1+loc_l2).^2 - 4*L*loc_l2))./(2*L*loc_l2);
figure(244);clf;hold on
plot(real(rloc1), imag(rloc1), 'b*-',...
     real(rloc2), imag(rloc2), 'g*-')


