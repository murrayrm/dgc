///
/// \file LateralController.hh
/// \brief Takes care of the lateral lateral part in trajectory following.
///
/// \author Kristian Soltesz, Magnus Linderoth
/// \date 10 July 2007
///
/// This file is part of the follower module.

#ifndef LATERAL_CONTROLLER_HH_IRGBFBFWESHIFDLUHAIUHF
#define LATERAL_CONTROLLER_HH_IRGBFBFWESHIFDLUHAIUHF

#define MAX_LATERAL_ACCELERATION 1.0 //magic number


#include "FollowerUtils.hh"
#include "gcinterfaces/AdriveCommand.hh"
#include "FollowerLogger.hh"
#include "interfaces/ActuatorState.h"

/// 
/// \struct LateralControllerParams
///
/// This struct holds the parameters of the lateral controller.
struct LateralControllerParams {
  double l1;  // Reference vehicle length [m] 
  double l1Scale; // Gain scheduling parameter for l1Scale []
  double l2; // Reference vehicle tow handle length [m]
  double l2Scale; // Gain scheduling parameter for l2Scale []
  double l2GainPow; // l2_gs is proportional to speed^l2GainPow
  double lI; // Integral part parameter
  double IDist; // Distance in front of rear wheels to calculate error for integral update [m]
  double maxI; // Maximum value of I [command unit]
  int trackFront; // Track front wheels instead of rear wheels
  double alpha; //
  double delay; // Models actuator delays. Used for Feed Forward [s]
  double replanError; // If exceeded, replanning from current position is requested from planner
  
  /// Constructors for LateralControllerParmas
  LateralControllerParams()
  {
    LateralControllerParams(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 0.0);
  }

  /// Overloaded constructor for LateralControllerParams
  LateralControllerParams(double param1, double param2, double param3, double param4,
                          double param5, double param6, double param7, double param8,
                          int param9, double param10, double param11, double param12) {
    l1 = param1;
    l1Scale = param2;
    l2 = param3;
    l2Scale = param4;
    l2GainPow = param5;
    lI = param6;
    IDist = param7;
    maxI = param8;
    trackFront = param9;
    alpha = param10;
    delay = param11;
    replanError = param12;
  }
};


///
/// \class LateralController
///
/// This is the class in which the lateral controller is implemented.
class LateralController
{
protected:
  /// The parameters used by the lateral controller
  LateralControllerParams m_params;

  /// SparrowHawk needs these to be members
  double m_lateralError;
  double m_l1_gs; 
  double m_l2_gs;
  double m_u;


  /// FollowerLogger is used to evaluate performance of follower
  FollowerLogger* m_internalLogger;


  /// Variable used to remember and keep old steering 
  /// commands when reaching end of traj. 
  /// Also used by SparrowHawk 
  double m_old_phi;

  /// Variable used to remember and keep old steering 
  /// commands when velocity on traj is zero. 
  /// Also used by SparrowHawk
  double m_old_phiFF;

  /// Integral part
  double m_I;

  /// Toggle function for visualization of lateral controller principle in mapviewer
  void sparrowToggleVisLat();

public:
  /// Constructor for LateralController
  ///
  /// \param params lateral controller parameters
  /// \param m_internalLogger pointer to internalLogger
  LateralController(FollowerLogger* internalLogger);

  /// Method in which the control signal is calculated 
  ///
  /// \param adriveDirective pointer to which the resulting AdriveDirective is written
  /// \param directivesToSend pointer to which the number of resulting AdriveDirectives is written
  /// \param followerMergedDirective pointer to CTraj with timestamp
  /// \param vehicleState pointer to vehicle state object
  /// \param trajectoryDirection trajectory direction
  ///
  bool control(FollowerControlStatus* controlStatus, AdriveDirective* steeringDirective, 
               CTraj* traj,
               SingleFrameVehicleState* vehicleState);

  /// Sets the parameters of the lateral controller
  void setParams(LateralControllerParams params);
};

#endif // LATERAL_CONTROLLER_HH_IRGBFBFWESHIFDLUHAIUHF
