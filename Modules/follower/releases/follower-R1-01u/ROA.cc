///
/// \file ROA.cc
/// \brief Reactive Obstacle Avoidance.
/// 
/// \Kristian Soltesz, Magnus Linderoth
/// \date 21 September 2007
/// 
/// ROA gets delay and lateral error information from follower,
/// together with front and rear center ladar scans. It uses
/// this information to shape the velocity reference in order 
/// to avoid collisions. 
/// 
/// This file is part of the follower module.

#include "ROA.hh"

// Constructor for ROA
ROA::ROA(FollowerControlStatus* controlStatus, FollowerLogger* internalLogger) :
  m_internalLogger(internalLogger),
  m_lateralError(0),
  m_gear(0),
  m_speed(0.0),
  m_steerAngle(0.0),
  m_delay(0),
  m_timeToCollision(0) {
  memset(m_enable, 0, sizeof(m_enable));
  memset(m_ROAFactors, 0, sizeof(m_ROAFactors));
  memset(m_timeStamps, 0, sizeof(m_timeStamps));
  memset(m_ages, 0, sizeof(m_ages));
  //if (!CmdArgs::sim) initSensnet();
  if (initSensnet() != 0) {
    exit(1);
  }
  sparrowInit();
}


int ROA::initSensnet() {
  // Set spread daemon
  char* spreadDaemon;
  if (getenv("SPREAD_DAEMON")) {
    spreadDaemon = getenv("SPREAD_DAEMON");
  } else {
    cerr << "SPREAD_DEAMON environment variable not set." << endl;
    return -1;
  }

  // Set skynet key
  int skynetKey;

  if (getenv("SKYNET_KEY"))
    skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    skynetKey = 1378;

  /*
  if (CmdArgs::sn_key) {
    skynetKey = CmdArgs::sn_key;
  } else {
    skynetKey = 0;
  }
  */

  // Create sensnet interface
  m_sensnet = sensnet_alloc();
  assert(m_sensnet);
  if (sensnet_connect(m_sensnet, spreadDaemon, skynetKey, MODROA) != 0) {
    printConsole("Unable to connect to sensnet" << endl);
    return -1;
  }

  if (sensnet_join(m_sensnet,SENSNET_MF_BUMPER_LADAR, SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0) {
    printConsole("Unable to join sensnet" << endl);
    exit(1);
    return -1;
  }
  return 0;
}


// Clean up sensnet
int ROA::finiSensnet()
{
  // There are more important things to implement right now
  return 0;
}


void ROA::setParams(ROAParams params) {
  m_params = params;
}

void ROA::sparrowInit() {

  // Gcdrive loop ages
  sparrowHawk->rebind("steering_loop_age", (int*)(&m_ages[STEERING_LOOP]));
  sparrowHawk->set_readonly("steering_loop_age");
  sparrowHawk->rebind("acceleration_loop_age", (int*)(&m_ages[ACCELERATION_LOOP]));
  sparrowHawk->set_readonly("acceleration_loop_age");

  // Gcdrive state ages
  sparrowHawk->rebind("steering_state_age", (int*)(&m_ages[STEERING_STATE]));
  sparrowHawk->set_readonly("steering_state_age");
  sparrowHawk->rebind("acceleration_state_age", (int*)(&m_ages[ACCELERATION_STATE]));
  sparrowHawk->set_readonly("acceleration_state_age");

  // Other ages
  sparrowHawk->rebind("trajectory_age", (int*)(&m_ages[TRAJECTORY]));
  sparrowHawk->set_readonly("trajectory_age");
  sparrowHawk->rebind("vehicle_state_age", (int*)(&m_ages[VEHICLE_STATE]));
  sparrowHawk->set_readonly("vehicle_state_age");

  // State information
  sparrowHawk->rebind("roa_gear", &m_gear);
  sparrowHawk->set_readonly("roa_gear");
  sparrowHawk->rebind("roa_lateral_error", &m_lateralError);
  sparrowHawk->set_readonly("roa_lateral_error");
  sparrowHawk->rebind("roa_speed", &m_speed);
  sparrowHawk->set_readonly("roa_speed");
  sparrowHawk->rebind("roa_steer_angle", &m_steerAngle);
  sparrowHawk->set_readonly("roa_steer_angle");
  

  sparrowHawk->rebind("roa_delay", (int*)(&m_delay));
  sparrowHawk->set_readonly("roa_delay");
  sparrowHawk->rebind("roa_time_to_collision", (int*)(&m_timeToCollision));
  sparrowHawk->set_readonly("roa_time_to_collision");
  sparrowHawk->rebind("roa_factor", &(m_ROAFactors[DELAY]));
  sparrowHawk->set_readonly("roa_factor");

  // ROA factors
  sparrowHawk->rebind("roa_factor_delay", &(m_ROAFactors[DELAY]));
  sparrowHawk->set_readonly("roa_factor_delay");
  sparrowHawk->rebind("roa_factor_lateral_error", &(m_ROAFactors[LATERAL_ERROR]));
  sparrowHawk->set_readonly("roa_factor_lateral_error");
  sparrowHawk->rebind("roa_factor_obstacle", &(m_ROAFactors[OBSTACLE]));
  sparrowHawk->set_readonly("roa_factor_obstacle");
  sparrowHawk->rebind("roa_factor_master", &(m_ROAFactors[MASTER]));
  sparrowHawk->set_readonly("roa_factor_master");
}



void ROA::updateTimeStamp(int index, unsigned long long timeStamp) {
  m_timeStamps[index] = timeStamp;
}

void ROA::updateAge(int index) {
  // [micro seconds]
  m_ages[index] = (DGCgettime()-m_timeStamps[index])/1000;
  m_internalLogger->roa_ages[index] = m_ages[index];
}

void ROA::updateAllAges() {
  for (int index = 0; index < NUMBER_OF_TIMES; index++) {
    updateAge(index);
  }
}


void ROA::updateLateralError(double lateralError) {
  // FIXME: Not sure if we want this error metric
  m_lateralError = lateralError;
}

void ROA::updateSpeed(double speed) {
  m_speed = speed;
}

void ROA::updateSteerAngle(double steerAngle) {
  m_steerAngle = steerAngle;
}

void ROA::updateGear(int gear) {
  m_gear = gear;
}

void ROA::updateDelay() {
  double weights[NUMBER_OF_TIMES];
  memset(weights, 0, sizeof(weights));
  weights[STEERING_LOOP] = 1.0;
  weights[ACCELERATION_LOOP] = 1.0;
  weights[TRAJECTORY] = 1.0;

  double tmpDelay = 0.0;
  double sumOfWeights = 0.0;
  for (int i = 0; i < NUMBER_OF_TIMES-1; i++) {
    if (weights[i] != 0.0) {
      sumOfWeights += weights[i];
      tmpDelay += (weights[i]*m_ages[i]);
    } 
  }

  m_delay = (unsigned long)(tmpDelay / sumOfWeights); 
  m_internalLogger->roa_delay = m_delay;
}

int ROA::updateLadar() {
  int blobId, blobLen;
  if (sensnet_peek(m_sensnet, SENSNET_MF_BUMPER_LADAR, SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
    {    
      return -1;
    }

  if (sensnet_read(m_sensnet, SENSNET_MF_BUMPER_LADAR, SENSNET_LADAR_BLOB, &blobId, sizeof(m_blob), &m_blob) != 0) {
    printConsole("Error reading from sensnet" << endl);
    return -1;
  } else {
    return 0;
  }
}

bool ROA::inRiscZone(float vfx, float vfy, float* arcDistance) {
  // Negative 'radius' when turning CCW
  //float turningRadius = VEHICLE_WHEELBASE/tan(m_steerAngle); 
  volatile float turningRadius = m_params.fakeTurningRadius;
  

  //printConsole("Turning radius: " << turningRadius << endl); // correct in simulation

  // FIXME: debug
  //*arcDistance = sqrt(pow(vfx,2.0)+pow(vfy,2.0)); // This is not arclength
  //return true;
  
  // FIXME: debug
  //*arcDistance = atan(vfx/(turningRadius-vfy))*turningRadius; // see paper note for verification
  //return true;


  if (fabs(fabs(turningRadius)-sqrt(pow(vfx,2.0)+pow(vfy-turningRadius,2.0))) < m_params.riscZoneHalfWidth) {
    *arcDistance = atan2(vfx,(turningRadius-vfy)*turningRadius/fabs(turningRadius))*fabs(turningRadius); // see paper note for verification
    
    //printConsole("Hit: (" << vfx << "," << vfy << ")  arc dist: " << *arcDistance 
    //             << " turn radius: " << turningRadius << endl);
    return true;
  }
  return false;
}

void ROA::updateTimeToCollision() {
  LadarRangeBlob* blob = &m_blob;

  // Populates the blob
  if (updateLadar() != 0) {
    // No successful read - should we do something on the safe side instead?
    exit(1);
    return;
  }

  // Loops through the blob
  float tmpArcDistance, minArcDistance = std::numeric_limits<float>::max();

  //printConsole("blob points: " << blob->numPoints << endl);

  for (int i = 0; i < blob->numPoints; i++) {
    // Vehicle frame
    float vfx, vfy;
   
    // Protection from using wrong coordinates
    {
      float vfz, sfx, sfy, sfz;
      float ba = blob->points[i][0];
      float br = blob->points[i][1];
      LadarRangeBlobScanToSensor(blob, ba, br, &sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    }

    //printConsole("Ladar hit: (" << vfx << "," << vfy << ")" << endl);

    if (inRiscZone(vfx, vfy, &tmpArcDistance) && tmpArcDistance < minArcDistance) {
//printConsole("Risc zone hit: (" << vfx << "," << vfy << ")" << endl);
      minArcDistance = tmpArcDistance;
      //printConsole("Temporary closest point: (" << vfx << "," << vfy << "), Distance: " << minArcDistance << endl);
    }
  }
  
  printConsole("Scan min dist: " << minArcDistance<< "Tur rad: " << m_params.fakeTurningRadius << endl);
  
  // Estimate the time to collision based on arc length
  m_timeToCollision = (unsigned long)(1000.0*minArcDistance/m_speed);
}

void ROA::updateROAFactor() {

  double tmpFactor;

  if (m_params.enableMaster && m_params.enableDelay) {
    sparrowHawk->set_string("roa_enable_delay", "ENABLED");
    tmpFactor = ((double)(m_delay)-(double)(m_params.minDelay)) /
      ((double)(m_params.maxDelay)-(double)(m_params.minDelay));
    tmpFactor = min(1.0,tmpFactor);
    tmpFactor = max(0.0,tmpFactor);
    m_ROAFactors[DELAY] = tmpFactor;
    if (m_ROAFactors[DELAY] > 0.0) {
      sparrowHawk->set_string("roa_active_delay", "ACTIVE");
    } else {
      sparrowHawk->set_string("roa_active_delay", "      ");
    }
  } else {
    sparrowHawk->set_string("roa_enable_delay", "       ");
    m_ROAFactors[DELAY] = 0.0;
  }
  
  if (m_params.enableMaster && m_params.enableLateralError) {
    sparrowHawk->set_string("roa_enable_lateral_error", "ENABLED");
    tmpFactor = m_lateralError*(double)(m_params.lateralErrorWeight);
    tmpFactor = min(1.0,tmpFactor);
    tmpFactor = max(0.0,tmpFactor);
    m_ROAFactors[LATERAL_ERROR] = tmpFactor;
    if (m_ROAFactors[LATERAL_ERROR] > 0.0) {
      sparrowHawk->set_string("roa_active_lateral_error", "ACTIVE");
    } else {
      sparrowHawk->set_string("roa_active_lateral_error", "      ");
    }
  } else {
    sparrowHawk->set_string("roa_enable_lateral_error", "       ");
    m_ROAFactors[LATERAL_ERROR] = 0.0;
  }
  
  if (m_params.enableMaster && m_params.enableObstacle && !CmdArgs::sim && m_gear == 1) {
    sparrowHawk->set_string("roa_enable_obstacle", "ENABLED");
    tmpFactor = 1.0 / (m_timeToCollision*m_params.collisionTimeWeight);
    tmpFactor = min(1.0,tmpFactor);
    tmpFactor = max(0.0,tmpFactor);
    m_ROAFactors[OBSTACLE] = tmpFactor;
    if (m_ROAFactors[OBSTACLE] > 0.0) {
      sparrowHawk->set_string("roa_active_obstacle", "ACTIVE");
    } else {
      sparrowHawk->set_string("roa_active_obstacle", "      ");
    }
  } else {
    sparrowHawk->set_string("roa_enable_obstacle", "       ");
    m_ROAFactors[OBSTACLE] = 0.0;
  }


  //m_ROAFactors[MASTER] = m_ROAFactors[DELAY];
  

  if (m_ROAFactors[MASTER] > 0.0) {
    sparrowHawk->set_string("roa_active_master", "ACTIVE");
  } else {
    sparrowHawk->set_string("roa_active_master", "      ");
  }
}

void ROA::limitReferenceSpeed(double* referenceVelocity) {
  

  // Find out how old things are
  updateAllAges();

  // Use the updated age information to estimate a 'master delay'
  updateDelay();

  // Estimate time to collision using LADAR information
  if (!CmdArgs::sim) updateTimeToCollision();

  // Updates the factor by which nominal reference speed is multiplied
  updateROAFactor();

  // Log whether ROA is enabled or disabled
  m_internalLogger->roa_enableMaster  = m_params.enableMaster;
  m_internalLogger->roa_enableDelay = m_params.enableDelay;
  m_internalLogger->roa_enableLateralError = m_params.enableLateralError;
  m_internalLogger->roa_enableObstacle = m_params.enableObstacle;

  // Only limit reference speed when ROA is enabled
  if (!m_params.enableMaster) {
    return;
  }

 // Limit reference velocity
 *referenceVelocity = *referenceVelocity*(1.0-m_ROAFactors[MASTER]);
}



