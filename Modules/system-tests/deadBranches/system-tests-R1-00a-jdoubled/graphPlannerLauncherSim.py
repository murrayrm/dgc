import os;
import sys;

import sysTestLaunch

apps = [ \
('asim',  '--rndf=../../src/graph-planner/stluke_bigloop.rndf', 2, 'berlin'), \
 ('graph-planner-viewer', '--rndf=../../src/graph-planner/stluke_bigloop.rndf --goal=2 --loop=5', 2 , 'localhost'), \
 ('trajfollower',  '--disable-trans', 2 , 'berlin'), \
 ('planviewer',  '', 2 , 'localhost'), \
]


sysTestLaunch.runApps( apps )

