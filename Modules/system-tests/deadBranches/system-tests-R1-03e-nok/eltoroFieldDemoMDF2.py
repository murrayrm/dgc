#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoroFieldDemoMDF2'
rndf = '../../etc/routes-eltoro/eltoro_field_demo2_adj.rndf'
mdf = '../../etc/routes-eltoro/Sept_2007_2.mdf'
obsFile = 'noObs'
sceneFunc = 'eltoro_buildingsandbushes'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
