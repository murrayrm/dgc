#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoroNorthOfPerimWithCar'
rndf = '../../etc/routes-eltoro/eltoro_field_demo_zone.rndf'
mdf = '../../etc/routes-eltoro/eltoro_field_demo_zone_1.mdf'
obsFile = 'eltoroNorthOfPerimWithCar.obs'
sceneFunc = 'eltoro_north_of_perim_with_car'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

