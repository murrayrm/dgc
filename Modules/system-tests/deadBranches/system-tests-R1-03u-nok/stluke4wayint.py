#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stluke4wayint'
rndf = '../../etc/routes-stluke/stluke_4wayint.rndf'
mdf = '../../etc/routes-stluke/stluke_4wayint.mdf'
obsFile = 'stluke4wayint.obs'
sceneFunc = 'stluke4wayint_1'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )


