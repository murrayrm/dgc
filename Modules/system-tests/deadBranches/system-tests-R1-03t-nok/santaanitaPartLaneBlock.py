#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.2.1'
scenario = 'santaanitaPartLaneBlock'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit_split.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_testobs.mdf'
obsFile = 'santaanitaPartLaneBlock.obs'
sceneFunc = 'santaanita_sitevisit_partlaneblock'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )


