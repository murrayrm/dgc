#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeSmallZoneExtendedZone10'
rndf = '../../etc/routes-stluke/stluke_small_zone_extended.rndf'
mdf = '../../etc/routes-stluke/stluke_small_zone_extended_zone10only.mdf'
obsFile = 'stlukeSmallZoneExtendedZone10.obs'
sceneFunc = 'stlukesmallzoneextended_zone10'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )


