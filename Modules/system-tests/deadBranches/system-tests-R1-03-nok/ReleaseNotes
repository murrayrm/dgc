              Release Notes for "system-tests" module

Release R1-03-nok (Wed Oct  3 23:38:11 2007):
	Added scripts for running at El Toro (eltoroFieldDemoMDF*.py) which uses Jessica's buildings and bushes scenario if run in sim.

Release R1-03 (Sun Sep 30  9:09:48 2007):
	Added processing for alicesim so that it is handled properly.
	This goes along with the changes in startup.  No changes in
	functionality.

Release R1-02z (Sun Sep 30  8:46:32 2007):
	Added el toro scenario that has lots of bushes and buildings up in the northern area that has a one-way section: python eltoroBuildingsAndBushes.py

Release R1-02y (Fri Sep 21 17:19:35 2007):
	added victorville scenario. This basic scenario does not have any other cars or obstacles, but loads up an MDF that includes all the RNDF segments (60 of them...). This is currently not working with planner.

Release R1-02x (Fri Sep 21 11:11:34 2007):
	Added new scenario: stlukeSmallStandardLotsofObstacles.py 
	This scenario has about 100 trafsim obstacles.

Release R1-02w (Thu Sep 20 14:46:35 2007):
	Added the list of modules and command line options to the analysis
	file.

Release R1-02v (Wed Sep 19 22:47:56 2007):
	Added the scripts for intersection and merging tests at El Toro.

Release R1-02u (Wed Sep 19 14:17:53 2007):
	Added scenarios for one way roads: road blocks and traffic jams. These are intended to test going into zone planner mode since we can't do uturns in these situations

Release R1-02t (Mon Sep 17 16:21:58 2007):
	Added scenarios for the new st luke rndfs

Release R1-02s (Mon Sep 17 15:48:41 2007):
	Count the number of manual interventions

Release R1-02r (Fri Sep 14 23:59:02 2007):
	The font in the previous release is really small. I was playing
	around with that with Robbie earlier and forgot to change it back.

Release R1-02q (Fri Sep 14 23:50:24 2007):
	Added a script for field demo

Release R1-02p (Tue Sep 11 23:10:24 2007):
	Do not add the time when we're in pause when computing the average
	speed and anything that involves time.

Release R1-02o (Mon Sep 10 14:43:45 2007):
	Made xterm window smaller for process-control to reflect the 
changes in startup module.

Release R1-02n (Fri Sep  7 23:25:18 2007):
	Added scripts for testing zone.

Release R1-02m (Fri Sep  7 19:01:44 2007):
	system-tests now opens process-control to the exact right 
height, so that in sim mode, we dont have a window that is way too 
large. 

	Also commiting some changes for Nok because she needed to sleep. 
(The changes have something to do with analyzer, if it's really that 
important I'm sure Nok will add more to this.

Release R1-02l (Thu Sep  6 18:31:26 2007):
	Call fflush in the analyzer so at least something will be written
	to the analysis file.

Release R1-02k (Tue Sep  4  1:11:24 2007):
	Made process-control window 90x60 instead of 90x50, because we 
somehow ran out of space even with 90x50. Also added no-start option for 
system-tests so it works with startup.

Release R1-02j (Sat Sep  1  3:50:50 2007):
	Added startup script for mock field demo 2 at St Luke.

Release R1-02i (Fri Aug 31 17:29:48 2007):
	Added 3 10 mile scenarios for el toro field demo 2

Release R1-02h (Wed Aug 29 16:22:41 2007):
	Added test scripts for testing follower with curvy path. followerCurvyPath.py only runs follower, gcdrive and asim. (follower reads in traj file.) stlukeCurvyPath.py runs the whole stack.

Release R1-02g (Mon Aug 27 23:16:08 2007):
	Added a script for the standard run without stop at 1.1.4 and 1.2.5.

Release R1-02f (Mon Aug 27 18:22:06 2007):
	Added scenario for new santa anita rndf (with zone). See http://gc.caltech.edu/wiki07/index.php/System-tests#santaanitaMockFieldDemo2Long.py

Release R1-02e (Sat Aug 25 11:04:23 2007):
	* added the 'continue' flag so we continue the mission without
	starting from the first waypoint. 
	* Fixed segfault in analyzer (which I think happened in the nightly logging section of the code).

Release R1-02d (Fri Aug 24 18:32:59 2007):
	Added 2 new scenarios which test advanced traffic situations: a car going the wrong way in our lane, and cars driving across our lane. the latter would probably be good for testing prediction

Release R1-02c (Fri Aug 24 16:53:22 2007):
	Added more scenarios for el toro, including some with zones and areas north of perimeter road. See http://gc.caltech.edu/wiki07/index.php/System-tests#El_Toro_Scripts for more details

Release R1-02b (Thu Aug 23 17:48:29 2007):
	Updated to work with new startup module.
	
	Now uses otherProcsField.CFG when in field configuration

	Also, pressing any key will now kill all processes, instead of 
just 'c'. This will hopefully cut down random hidden screens being left 
to run for all eternity

Release R1-02a (Wed Aug 22 17:05:49 2007):
	Added a python script for the new zone rndf at stluke.

Release R1-02 (Mon Aug 20 17:23:15 2007):
	* Remove the TIMEOUT in sysTestLaunch. It definitely should NOT be there. We'll add the timeout in the bash script instead.
	* Added estop to state log. Analyzer also looks at estop status.
	* By default, when running in sim, process control won't restart a
	process that dies. To have it do that, run the script with
	'restart' flag.


Release R1-01z (Mon Aug 20 13:04:55 2007):
	Added nightly test functionality.

	Analyzer now scores and outputs it to /logs/nightly.log
	sysTestLaunch now automatically closes all windows and ends the simulation after TIMEOUT seconds (default = 600)
	nightly.sh is a bash script that runs every stluke*.py and santaanita*.py file in simulation

	The goal of this is to be able to look at just the /logs/nightly.log file and be able to tell at a glance which
	goals completed correctly and which caused serious problems and failed the mission.
	It also allows users to compare performance to see if new modules are becoming more optimized.	

Release R1-01y (Thu Aug 16 13:44:00 2007):
	Added a new python script for zone regions.

Release R1-01x (Wed Aug 15 20:29:49 2007):
	Forgot to mention this in the startup release I just made, but 
Trafsim and sensor-sim are both being run in process-control, although 
all the .ts and .py files can stay in system-tests for the time being. 

	Besides that, just changed system-tests to reflect the 
new command line configuration for the startup module just released. 
Again, to not have process-control running in auto-restart mode, the 
following code works great:

	python stlukeSmallStandard.py no-restart

Release R1-01w (Tue Aug 14 20:50:48 2007):
	Added a script for running at El Toro.

Release R1-01v (Tue Aug 14 17:27:30 2007):
	Added simple zone scenario

Release R1-01u (Thu Aug  9  0:24:11 2007):
	* statelogger now has a sparrow display that shows mileage, average
	speed and max speed
	* Changed the log path to /logs when the field flag is given

Release R1-01t (Mon Aug  6 11:54:12 2007):
	Added sensorsim to system-tests. This module runs in between trafsim and mapper. It intercepts trafsim mapelements, modifies them (adds noise, removes from the map if they're out of sensor range, etc) and sends them on to the mapper.

	trafsim obstacles that are out of sensor range will still show up in mapviewer as dotted lines, however these obstacles will not be passed on to planner. If the obstacle is visualized, it will show up with a pink, blue, or green outline.

	IMPORTANT NOTE: if you've been adding obstacles in mapviewer, I highly recommend you start adding them into trafsim instead. All you have to do is hold 'o' and click where you want to place them. This way you can avoid the strange "flickering" behavior that people have been seeing, where obstacles drop in and out.

Release R1-01s (Thu Aug  2 10:18:15 2007):
	Added RNDF for St. Luke for testing intersections without stop lines

Release R1-01r (Thu Aug  2  1:49:12 2007):
	Update the analyzer so it also tells the max and average speed

Release R1-01q (Wed Aug  1 17:21:48 2007):
	Added some new scenarios for santa anita mock field demo1 rndf.

Release R1-01p (Wed Aug  1 14:42:57 2007):
	Made startup window that system-tests opens up wider, changed 
bin prefix passed to startup to not include i486-gentoo-linux so Drun 
works on startup

Release R1-01o (Wed Aug  1 10:57:52 2007):
	Added scripts for baseline and high speed test runs at Santa Anita

Release R1-01n (Tue Jul 31 20:02:43 2007):
	Added a system test for steele lot. This is for system-tests
	of Alice before/without leaving caltech.

Release R1-01m (Tue Jul 31 15:59:41 2007):
	Added test for stluke area

Release R1-01l (Fri Jul 27 17:12:39 2007):
	Took off some comments in sysTestLaunch that were temporarily 
not allowing Trafsim to run. I was on Paris, which apparently "doesn't 
like" Trasim, and I forgot to remove the comments... 

Release R1-01k (Fri Jul 27  5:43:05 2007):
	Changed sysTestLaunch to accomodate the changes in startup, such 
as adding a restart option

Release R1-01j (Thu Jul 26  9:52:50 2007):

The configuration file IntersectionHandling.conf was added. As the file name already implies, this file containts configuration values and flags for intersection handling.
Two flags are important;

If INTERSECTION_SAFETY_FLAG is set to TRUE, the Sitevisit rules apply which are more conservative than the rules for the challenge. e.g. intersection is only clear if there 
is no obstacle in the whole intersection. Turn off this flag only if nominal driving is very reliable and precise.

If INTERSECTION_CABMODE_FLAG is set to TRUE, DARPA's cabmode rule will be activated. If it is Alice's turn to go but the intersection is blocked for more than 10 seconds, Alice needs to follow any 
feasible trajectory through the intersection or needs to replan the mission. Leave this flag at FALSE until planner is finished completly.

Release R1-01i (Tue Jul 24  9:32:42 2007):
	modified the command given when running process-control in field 
configuration by fixing a bug that randomly had a space between -- and  
startall, causing none of the processes to start at the beginning

Release R1-01h (Thu Jul 19 18:29:27 2007):
	sysTestLaunch.py now does not run the sensingStack on its own, 
this is already done in the field.CFG file in startup. Also made xterm 
window bigger so that process-control will not cut off the bottom 
options.

Release R1-01g (Wed Jul 18 13:21:10 2007):
	sysTestLaunch.py now lets process-control call mappers and loggers instead of doing it itself. It now only calls Trafsim, everything else is done in process-control	

Release R1-01f (Thu Jul 12 19:56:45 2007):
	Changed sysTestLaunch.py to run process-control, which has a config file to start all of the planners instead of running 
all the planners in sysTestLaunch.py

Release R1-01e (Mon Jul  2 14:21:18 2007):
	Added a script to run the standard mdf

Release R1-01d (Wed Jun 27 20:45:40 2007):
	Changed command line argument for tplanner.

Release R1-01c (Sun Jun 24 21:21:47 2007):
	Moved logs to /logs

Release R1-01b (Tue Jun 19 21:40:12 2007):
	added scripts for for running stluke_small_smallloop.mdf and stluke_small_bigloop.mdf

Release R1-01a (Sun Jun 17 10:23:42 2007):
	Added a script for going around two loops at St Luke.

Release R1-01 (Sat Jun  9 23:38:45 2007):
	* Added the --pause-braking=0.9 flag to gcdrive and added the
	speed-limit, accel-limit and brake-limit flags to s1planner for
	estop test
	* Added the flag 'costmap' to the script. When it's given,
	the costmap will be shown on mapviewer
	* Added the flag 'sim' to estop test scripts.

Release R1-00z (Sat Jun  9  0:10:15 2007):
	* Added --steer-speed-threshold flag to gcdrive. 
	* Added	estopTestLauncher.py

Release R1-00y (Fri Jun  8 11:04:33 2007):
  Fixed an issue with template-ladarperceptor being restarted continually.
  ps was returning a string truncated to 16 characters which did not match 
  the restart mechanism currently cannot distinguish modules with names 
  similar in hte first 16 char

  gcfollower has --use-lead appended to it arguments (don't know who made 
  this change -- suspect stafano)

Release R1-00x (Wed Jun  6 23:45:23 2007):
	restart code looks only on machine designated for the process to minimize 
  polling interval and response time

Release R1-00w (Wed Jun  6 22:59:37 2007):
	Do not look for processes on all machines when we're running in
	simulation

Release R1-00v (Wed Jun  6 13:20:02 2007):
  * new code to look for processes on all machines..  Should prompt for
  user to take independent action if a duplicate process is running
  * monitor for processes and to restart them if not present

Release R00u (Tue Jun  5 15:04:54 2007):
	Fixed segfault

Release R1-00t (Wed May 30  8:40:38 2007):
	added a selectMDF() function for basic UI of selecting MDF files
  from an anticipated folder

Release R1-00s (Thu May 31 23:23:00 2007):
	All logs go to /tmp/logs

Release R1-00r (Thu May 31  6:17:33 2007):
	* Added an updated santaanitaIntersection scenario 
(santaanitaIntersectionScripted) that adds the cars once Alice has 
passed a trafsim "checkpoint"
	* Added 2 extended scenarios for Santa Anita.  These scenarios 
include multiple types of obstacles and cars will appear when Alice 
reaches some trafsim "checkpoints".  Some quick tests show Alice having 
some problems with the scenarios.
	* Merge appeared successful

Release R1-00q (Thu May 31  1:05:18 2007):
	Run the different processes on the machines on Alice in simulation
	when 'alicesim' argument is given. Added some sleep to the sensing
	script.

Release R1-00p (Wed May 30 12:30:47 2007):
	Log all the tplanner and s1planner output (whatever they print out
	on the screen). The log files have timestamp and scenario name
	appended to the module name (same format as log file automatically
	generated by gcmodule). This is just a temporary solution as those
	modules cannot yet log what they're doing. Since I just did '|
	tee', what printed out on the screen may be delayed.

Release R1-00o (Mon May 28 22:03:44 2007):
	Used tplannerCSS by default. Draw polytopes instead of rddf on
	mapviewer. Added --use-new flag to gcfollower.

Release R1-00n (Sat May 26  3:48:55 2007):
	From Scott Goodfriend: I broke my svn for system-tests
	* Changed the 4 Santa Anita special case scenarios so that they 
have .obs files with them
	* Added 4 St. Luke dynamic obstacle scenarios
	* Nok changed the start-up script so that trafsim would start 
first and then sleep 8 seconds for the planning stack to start.  To 
allow trafsim and Alice to start less far apart in time.

Release R1-00m (Fri May 25 19:13:07 2007):
	* Fixed the name of log file.
	* Append YAM.config to analysis file.

Release R1-00l (Thu May 24 13:41:33 2007):
	* Removed the press enter before starting trafsim stack so the timing
	works right for dynamic obstacles. 
	* Added all the sensing modules to sensingLaunch
	* Added one more scenrio (turning left) as per Noel's request.
	* Added parser for dynamic obstacles.
	* Do not start skynettalker if we run in sim.
	* Changed the name of skynetlogger logfile so it uses the same format
	as gcmodule

Release R1-00j (Wed May 23 20:47:50 2007):	
	* Only need to specify starting point, scenario, rndf, mdf, obsFile
	and sceneFunc in the script. sysTestLaunch.runPlanners() contains the default set
	of planners to run based on the command line argument (*sim* or
	*field*). Because of this, we don't need two versions of the script
	(one for field and one for sim) anymore. 
	* To run the sim version, do, for example,
	  python -i santaanitaIntersectionRight.py sim
	  To run the field version which will also starts all the sensing modules specified in sensingLaunch.py, use field instead of sim.
	* Add sensingLaunch.py that specifies sensing modules that will be run in Alice. I don't have the complete set of sensing modules yet. So right now, it only starts template-ladarperceptor. Will ask Sam to add other modules in.


Release R1-00i (Wed May 23 12:49:33 2007):
	* You can now press 'c' to quit all the program.
	* Added template-ladarperceptor to all the scripts.
	* Automatically start skynetlogger.


Release R1-00h (Wed May 23  1:46:16 2007):
	* Dramatically reduced the number of stop line messages. Richard
	should be happier now.
	* Made s1planner the default planner instead of rddfplanner.
	* Fixed send/recv subgroups in mapviewer, testMapper, mapper and
	trafsim.

Release R1-00g (Tue May 22  4:13:12 2007):
	Added three trafsim created scenarios that are kinda weird, but give 
interesting results.

Release R1-00f (Tue May 22  0:27:44 2007):
	* Fixed stupid segfault problem in the analyzer. Added more scripts
	for testing with s1planner in sim
	* log the apps and params in a given run into the ..analyze log

Release R1-00e (Sun May 20 22:14:56 2007):
	Added files that are used in the field test on May 20, 2007. To run tplannerNoCSS-R2-02u, create a link in bin to an executable tplannerNoCSS from tplanner-R2-02u.

Release R1-00d (Sun May 20 12:44:53 2007):
	* Changed command line argument for rddfplanner from --use-endpoints
	to --use-final. Run testMapper instead of trafsim when we're not
	running in sim so we also see the rndf plotted on mapviewer.
	* Run testMapper instead of trafsim for field config
	* Unpaused asim at startup


Release R1-00c (Fri May 18 10:29:50 2007):
	Added a new script for a new scenario 
(santaanitaSparseRoadBlock.py)

Release R1-00b (Wed May 16 13:09:15 2007):
	* Modularized the test script.  
	* Added scripts for basic navigation
	* Added the state logger and the analyzer

Release R1-00a (Mon May 14 13:30:47 2007):
	Extended to allow running apps on remote machines via ssh.
  

Release R1-00 (Fri May 11  9:36:20 2007):
	Created.

























































































