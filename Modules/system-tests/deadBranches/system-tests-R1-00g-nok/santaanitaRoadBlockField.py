#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'santaanitaRoadBlockField'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_simpleloop.mdf'
obsFile = 'santaanitaSiteVisitRoadBlockObs.log'
sceneFunc = 'santaanita_sitevisit_roadblock'

apps = [ \
('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'localhost'), \
 ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local --send-costmap' % locals(), 2 , 'localhost'), \
 ('s1planner',  '', 2 , 'localhost'), \
 ('gcfollower',  '--use-local', 2 , 'leela'), \
 ('gcdrive',  '', 2 , 'leela'), \
# ('planviewer',  '--use-local', 2 , 'fry'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )

