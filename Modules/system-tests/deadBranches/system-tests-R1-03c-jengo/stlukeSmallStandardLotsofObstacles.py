#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '3.1.1'
scenario = 'stlukeSmallStandardLotsofObstacles'
rndf = '../../etc/routes-stluke/stluke_small.rndf'
mdf = '../../etc/routes-stluke/stluke_small_standard.mdf'
obsFile = 'stlukeSmallStandardLotsofObstacles.obs'
sceneFunc = 'stlukesmall_standard_lotsofobstacles'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
