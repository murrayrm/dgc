#! /usr/bin/python -i

import os;
import sys;
import time
import string

import sysTestLaunch

scenario = 'santaanitaEstopTest'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit_estoptest.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_estop20.mdf'
obsFile = 'noObs'
sceneFunc = ''

lognameTimestamp = time.strftime( '%Y-%m-%d-%a-%H-%M' )
tplannerlogname = '/tmp/logs/tplanner-%s.%s.log' % ( scenario, lognameTimestamp ) 
s1plannerlogname = '/tmp/logs/s1planner-%s.%s.log' % ( scenario, lognameTimestamp ) 

apps = [ \
('mapper', '--rndf=%(rndf)s --noconsole' % locals(), 1, 'bender'), \
 ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l --log-level=8 --log-path=/tmp/logs/' % locals(), 1, 'bender'), \
 ('tplannerCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local --send-costmap | tee %(tplannerlogname)s' % locals(), 1 , 'bender'), \
 ('s1planner', ' | tee  %(s1plannerlogname)s' % locals(), 1 , 'bender'), \
 ('gcfollower',  '--use-local --use-new --use-lead', 1 , 'leela'), \
 ('gcdrive',  '--auto-shift --steer-speed-threshold=2', 1 , 'leela'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )