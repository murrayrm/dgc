#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '21.1.8'
scenario = 'NqeTestAreaCUturn2'
rndf = '../../etc/routes-darpa/nqe.rndf'
mdf = '../../etc/routes-darpa/nqe_areac_uturn2.mdf'
obsFile = 'NqeTestAreaC.obs'
sceneFunc = 'nqe_area_c_mod'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

