#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '6.2.1'
scenario = 'santaanitaMockFieldDemoCarWrongWay'
rndf = '../../etc/routes-santaanita/santaanita_mock_field_demo1.rndf'
mdf = '../../etc/routes-santaanita/santaanita_mock_field_demo_bigloop.mdf'
obsFile = 'NoObs'
sceneFunc = 'santaanita_mock_field_demo_carwrongway'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

