#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '40.1.1.'
scenario = 'testAreaB_NEW'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-darpa/nqe_areaB_rmm1.mdf'
obsFile = 'testAreaB_NEW.obs'
sceneFunc = 'testAreaB_NEW'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

