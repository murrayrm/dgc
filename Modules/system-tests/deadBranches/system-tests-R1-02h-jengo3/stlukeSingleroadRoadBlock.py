#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeSingleroadRoadBlock'
rndf = '../../etc/routes-stluke/stluke_singleroad.rndf'
mdf = '../../etc/routes-stluke/stluke_singleroad_testobs.mdf'
obsFile = 'stlukeSingleroadRoadBlock.obs'
sceneFunc = 'stluke_singleroad_roadblock'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

