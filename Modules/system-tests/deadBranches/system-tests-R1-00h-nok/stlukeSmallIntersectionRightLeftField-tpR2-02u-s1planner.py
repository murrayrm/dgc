#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'stlukeSmallIntersectionRightLeftFields1planner'
rndf = '../../etc/routes-stluke/stluke_small.rndf'
mdf = '../../etc/routes-stluke/stluke_small_rightleft.mdf'
obsFile = 'noObs.log'
sceneFunc = ''

apps = [ \
('template-ladarperceptor', '--send-debug', 2, 'amy'), \
 ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'bender'), \
 ('tplannerNoCSS-R2-02u-field', '--rndf=%(rndf)s --disable-console --debug=1 --use-local --send-costmap' % locals(), 2 , 'bender'), \
 ('s1planner',  '--use-final --use-flat --verbose=2', 2 , 'bender'), \
 ('gcfollower',  '--use-local', 2 , 'leela'), \
 ('gcdrive',  '', 2 , 'leela'), \
# ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )

