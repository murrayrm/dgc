#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'santaanitaLaneBlockField'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_testobs.mdf'
obsFile = 'santaanitaSiteVisitBlockObs.log'
sceneFunc = 'santaanita_sitevisit_block'

apps = [ \
('template-ladarperceptor', '--send-debug', 2, 'amy'), \
 ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'localhost'), \
 ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local --send-costmap' % locals(), 2 , 'localhost'), \
 ('s1planner',  '', 2 , 'localhost'), \
 ('gcfollower',  '--use-local', 2 , 'leela'), \
 ('gcdrive',  '', 2 , 'leela'), \
# ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )

