#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeZone2'
rndf = '../../etc/routes-stluke/stluke_zone.rndf'
mdf = '../../etc/routes-stluke/stluke_zone_1.mdf'
obsFile = 'NoObs'
sceneFunc = 'stlukezone_2'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
