#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.2.7'
scenario = 'stlukeSmallStandard'
rndf = '../../etc/routes-stluke/stluke_small_zone.rndf'
mdf = '../../etc/routes-stluke/stluke_small_zone.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

