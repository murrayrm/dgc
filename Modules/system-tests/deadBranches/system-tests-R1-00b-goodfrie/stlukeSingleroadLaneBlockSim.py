#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'stlukeSingleroadLaneBlockSim'
rndf = '../../etc/routes-stluke/stluke_singleroad.rndf'
mdf = '../../etc/routes-stluke/stluke_singleroad_testobs.mdf'
obsFile = 'stlukeSingleroadLaneBlockObs.log'
sceneFunc = 'stluke_singleroad_laneblock'

apps = [ \
('asim',  '--rndf=%(rndf)s --rndf-start=1.1.1 --gcdrive' % locals(), 2, 'berlin'), \
 ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'moscow'), \
 ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local' % locals(), 2 , 'moscow'), \
 ('rddfplanner',  '--use-endpoints --use-flat --verbose=2', 2 , 'moscow'), \
 ('gcfollower',  '--use-local', 2 , 'berlin'), \
 ('gcdrive',  '--simulate', 2 , 'berlin'), \
# ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, True )

