#! /usr/bin/python -i

import os;
import sys;
import time
import string

import sysTestLaunch

start = '1.1.1'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit_estop.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_estop20.mdf'

sysTestLaunch.runEstopTest( start, rndf, mdf )