#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'stlukeSmallIntersectionStraightField-tpR2-02u-s1planner'
rndf = '../../etc/routes-stluke/stluke_small.rndf'
mdf = '../../etc/routes-stluke/stluke_small_straight.mdf'
obsFile = 'noObs.log'
sceneFunc = ''

apps = [ \
('asim', '--rndf=%(rndf)s --rndf-start=1.1.1 --gcdrive' % locals(), 2, 'localhost'), \
 ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'localhost'), \
 ('tplannerNoCSS-R2-02u-field', '--rndf=%(rndf)s --disable-console --debug=1 --use-local --send-costmap' % locals(), 2 , 'localhost'), \
 ('s1planner',  '--use-final --use-flat --verbose=2', 2 , 'localhost'), \
 ('gcfollower',  '--use-local', 2 , 'localhost'), \
 ('gcdrive',  '--simulate', 2 , 'localhost'), \
 ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, True )

