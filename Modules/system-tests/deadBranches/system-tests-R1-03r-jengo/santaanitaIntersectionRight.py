#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.2.25'
scenario = 'santaanitaIntersectionRight'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_right.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

