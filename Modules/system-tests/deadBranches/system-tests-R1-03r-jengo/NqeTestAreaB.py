#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '32.1.1'
scenario = 'NqeTestAreaB'
rndf = '../../etc/routes-darpa/nqe.rndf'
mdf = '../../etc/routes-darpa/nqe_test1.mdf'
obsFile = 'NqeTestAreaB.obs'
sceneFunc = 'nqe_area_b'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
