#!/bin/bash
TIMEOUT=5
DIV='===================================================================='
echo "$DIV"
echo -n "Starting nightly test run for: "
date +%D
echo "(press 'q' in $TIMEOUT second(s) to cancel)"
read -s -t $TIMEOUT -n 1 blah
if [ "$blah" = 'q' ]; then
    echo "The nightly test has been cancelled and will run again in 24 hours."
else
    mkdir -p logs
    echo "*** NIGHTLY TEST ***" > logs/nightly.log # clears file before writing
    echo User: "$USER" >> logs/nightly.log
    echo Date: $(date) >> logs/nightly.log
    echo Sandbox: $PWD >> logs/nightly.log
    echo -e "$DIV\nScenario:\t\t\t\t\t  Score:\n$DIV" >> logs/nightly.log

    for X in stlukeSmallStandard.py eltoroBuildingsAndBushes.py eltoroFieldDemo2Short*.py; do # runs files in current folder
#    for X in stlukeSmall*.py ; do # runs files in current folder
#    for X in stluke*.py santaanita*.py; do # runs files in current folder
	blah='a'
	echo "$DIV"
	echo "* Testing:  $X (press 'q' now to cancel) *"
	read -s -t $TIMEOUT -n 1 blah
	if [ "$blah" = 'q' ]; then
	    echo "$X will not be tested."
	    echo -n -e "$X\b\b\b" >> logs/nightly.log

	    let "NUM_TABS=(${#X}-3)/8" # indents score column evenly
	    for ((i=6;i>$NUM_TABS;i--)); do
		echo -n -e "\t" >> logs/nightly.log
	    done

	    echo "  SKIPPED" >> logs/nightly.log
	else
	    python "$X" test-timeout sim
	fi
    done
fi
echo "$DIV"
echo -n "The nightly test has completed at "
date +%T%p
echo "and will run again in 24 hours."
echo "$DIV"

