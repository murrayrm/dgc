#!/bin/bash
sandbox=${PWD}/../../
hour=03 # the hour in which to run, in 24-hour notation with a leading zero if necessary
emails="jdoubled@aig.jpl.nasa.gov nok@caltech.edu livermouse@gmail.com"

DGC_CONFIG_PATH=${sandbox}/etc
while true ; do

  if [[ `date +'%H'` == $hour ]] ; then
    echo "Preparing for testing...."
    # notify all users on this machine that things will be slowing down quite a bit
    wall "Automatic system tests are beginning in 3 minutes.  The cpu on this machine will be hogged. \
     You may want to consider saving your data and logging off"
    sleep 180

    # get the latest of all the link modules
    cd $sandbox
    yam config -noedit -update-links -o newconfig
    mv YAM.config yam.config-`date +"%F-%R"`
    mv newconfig YAM.config
    yam relink -no-prompt
    cd src/system-tests

    # run the nightly test
    bash nightly.sh 

    # prepare the log to be sent on sendmail
    echo . >> logs/nightly.log

    # report
    # we have to ssh to gc because its the only machine setup to send email
    ssh gc /usr/sbin/sendmail $emails < logs/nightly.log

    # wait for at least an hour so we do not try to rerun at the 3 oclock hour
    sleep 3600

  fi

  #check every ten minutes
  sleep 600
done


