#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.2.5'
scenario = 'santaanitaSparseRoadblock'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_testobs.mdf'
obsFile = 'santaanitaSparseRoadblock.obs'
sceneFunc = 'santaanita_sitevisit_sparseroadblock3'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

