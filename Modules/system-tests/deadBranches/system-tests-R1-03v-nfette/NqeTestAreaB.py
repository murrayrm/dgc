#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

#start = '40.1.1'
start = '38.1.1'
scenario = 'NqeTestAreaB'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-darpa/nqe_b1_mdf.txt'
obsFile = 'NqeTestAreaB.obs'
sceneFunc = 'nqe_area_b_mod'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )
