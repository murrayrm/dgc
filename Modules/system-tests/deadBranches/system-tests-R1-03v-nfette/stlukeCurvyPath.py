#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'followerCurvyPath'
rndf = '../../etc/routes-stluke/stluke_sinewave.rndf'
mdf = '../../etc/routes-stluke/stluke_sinewave.mdf'
obsFile = 'noObs'
sceneFunc = ''
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

