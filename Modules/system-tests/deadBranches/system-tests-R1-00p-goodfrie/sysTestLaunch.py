import os
import signal
import sys
import time
import re
import string

import sensingLaunch

# here is a sample apps array
# testapps = [ \
# ('asim',  '--rndf=%(rndf)s' % locals(), 2, 'berlin'), \
#  ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p --nomap' % locals(), 2 , 'localhost'), \
#  ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local --send-costmap' % locals(), 2 , 'localhost'), \
#  ('s1planner',  '', 2 , 'localhost'), \
#  ('trajfollower',  '--use-local', 2 , 'localhost'), \
#  ('planviewer',  '--use-local', 2 , 'localhost'), \
# ]
#

def spawn ( cmd, nonsplitargs='' ):
  a = cmd.split()[0]
  wcmd = os.popen( 'which %s' % a ).read().strip()
  pid = os.spawnv( os.P_NOWAIT, wcmd, [wcmd] + cmd.split()[1:] + [nonsplitargs])
  return pid

def runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc ):
  if( not 'field' in sys.argv and not 'sim' in sys.argv ):
    print "Assume running in simulation"

  apps = []
  bRunTrafsim = True

  lognameTimestamp = time.strftime( '%Y-%m-%d-%a-%H-%M' )
  tplannerlogname = './logs/tplanner-%s.%s.log' % ( scenario, lognameTimestamp ) 
  s1plannerlogname = './logs/s1planner-%s.%s.log' % ( scenario, lognameTimestamp ) 
  #tplannerlogname = './logs/tplanner-%s.%s.log' % ( re.sub( '.*/', '', re.sub( r'\.mdf$', '', mdf) ), time.strftime( '%Y-%m-%d-%a-%H-%M-%S' ) )

  if( 'field' in sys.argv ):
    sensingApps = sensingLaunch.runSensingStack( )
    apps = sensingApps + [ \
    ('mapper', '--rndf=%(rndf)s --noconsole' % locals(), 1, 'bender'), \
     ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l --log-level=8' % locals(), 1, 'bender'), \
     ('tplannerCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local --send-costmap | tee %(tplannerlogname)s' % locals(), 1 , 'bender'), \
     ('s1planner', ' | tee  %(s1plannerlogname)s' % locals(), 1 , 'fry'), \
     ('gcfollower',  '--use-local --use-new', 1 , 'leela'), \
     ('gcdrive',  '--auto-shift', 1 , 'leela'), \
#    ('planviewer',  '--use-local', 1 , 'localhost'), \
    ]
    bRunTrafsim = False
  else:
    apps = apps + [ \
    ('asim', '--no-pause --rndf=%(rndf)s --rndf-start=%(start)s --gcdrive' % locals(), 1, 'localhost'), \
     ('mapper', '--rndf=%(rndf)s --noconsole' % locals(), 1, 'localhost'), \
     ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l --log-level=8' % locals(), 1, 'localhost'), \
     ('tplannerCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local --send-costmap | tee %(tplannerlogname)s' % locals(), 1 , 'localhost'), \
     ('s1planner', ' | tee  %(s1plannerlogname)s' % locals(), 1 , 'localhost'), \
     ('gcfollower',  '--use-local --use-new', 1 , 'localhost'), \
     ('gcdrive',  '--simulate', 1, 'localhost'), \
#    ('planviewer',  '--use-local', 1, 'localhost'), \
    ]
  runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, bRunTrafsim )

def runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, bRunTrafsim ):
  packagePath = '../../'

  # please define the rndf and mdf functions from the calling script, as in the 
  # examples below
  # rndf = '../../etc/routes-stluke/tplanner_unittest.rndf'
  # mdf = '../../etc/routes-stluke/tplanner_unittest_rightleft.mdf'
  # sceneFunc = 'stluke_singleroad_partlaneblock'
  #if( not ('rndf' in globals() and 'mdf' in globals() and 'obsFile' in globals()) ):
    #print "you must specify the obsFile, rndf and mdf variables as global variables"
    #print globals()
    #sys.exit()
  if( not sceneFunc ):
    print "no sceneFunc defined, no obstacles will be placed on map"

  try:
    os.mkdir ( './logs' )
  except:
    print './logs folder already created. leaving' 

  #lognameTimestamp = time.strftime( '%Y-%m-%d-%a-%H-%M-%S' )
  #logname = './logs/systemtest-%s-%s.log' % ( time.strftime( '%Y-%m-%d-%H-%M-%S' ), re.sub( '.*/', '', re.sub( r'\.mdf$', '', mdf) ) )
  lognameTimestamp = time.strftime( '%Y-%m-%d-%a-%H-%M' )
  logname = './logs/systemtest-%s-%s.log' % ( lognameTimestamp, scenario ) 
  print "log name: %s" % (logname)

  cwd = os.getcwd()
  print cwd
  appdict = {}
  for i in apps:
    (appname, params, sleeptime, host) = i
    appdict[appname] = [appname, '', '', 0]   #(appname, fullpath, pid)

  bFoundAll = True
  for root, dirs, files in os.walk('../../bin'):
    for name in files:
      if name in appdict.keys():
        #print 'found our target'
        #print os.path.join( root, name )
        lpath =  os.path.join( os.getcwd(), os.path.join(root,name) )
        if( os.path.islink(lpath) ):
          path =  os.readlink(lpath)  
          path = os.path.join(os.path.dirname(lpath), path)
          if( os.path.isfile(path) ):
            appdict[name][1] = lpath
          else:
            print 'found a broken link: %s -> %s ' % (lpath, path)
            bFoundAll = False
        else:
          appdict[name][1] = lpath
        print appdict[name]

  print 
  print 

  for i in appdict.keys():
    if appdict[i][1] == '':
      print 'could not find path for %s' % i
      bFoundAll = False 
  if not bFoundAll:
    print 'unavailable modules, exiting'
    sys.exit()

  pids = []

#  print "press enter to start the trafsim stack, or C-d or C-c to interrupt"
#  blah = raw_input()
  print "Starting the trafsim stack"
  simpids = runTrafsimStack(obsFile, rndf, mdf, sceneFunc, logname, bRunTrafsim, packagePath )
  pids = pids + simpids 

  logAnalysisFile = re.sub( r'\.log', '.analysis', logname )
  logAnalysis = open( '''%s''' % logAnalysisFile, "w" )
  print "PID:"
  for i in apps:
    (appname, params, sleeptime, host) = i
    path = appdict[appname][1]
    shcmd = '{ echo; source /etc/profile.dgc; cd "%(cwd)s"; "%(path)s" %(params)s; echo press enter to close; read; }' % locals()
    if host != 'localhost' and host != '' :
      cmd = '''xterm -rightbar -sb -T %(appname)s -e ssh -t -Y %(host)s''' % locals()
    else:
      cmd = '''xterm -rightbar -sb -T %(appname)s -e''' % locals()
    whichcmd= os.popen( 'which %s' % cmd.split()[0] ).read().strip()
    pid = spawn(  cmd, shcmd )
    print appname + ": " + str( pid )
    # print pid
    pids.append( pid )
    os.system( 'sleep %(sleeptime)d' % locals() )
    # prepend the analysis log file with the applications run...
    logAnalysis.write( str(i) ) 
    logAnalysis.write( '\n' )
    # logAnalysis.write( ' -> ' + cmd + '\n' )
  logAnalysis.close()

  print
  print "starting the statelogger"
  #os.system( 'xterm -e "{ %(packagePath)sbin/Drun statelogger --file=%(logname)s; echo press return to close; read; }" & ' % locals() )
  cmd = 'xterm -e'
  nsargs = "{ %(packagePath)sbin/Drun statelogger --file=%(logname)s; echo press return to close; read; }" % locals()
  pid = spawn( cmd, nsargs )
  pids.append( pid )

  if ( not bRunTrafsim ):
    skynetloggerLogname = './logs/skynetlogger-%s-%s.log' % ( scenario, lognameTimestamp ) 
    #skynetloggerLogname = re.sub( r'systemtest-', '', logname )
    #skynetloggerLogname = re.sub( r'\.log', '-skynetlogger.log', skynetloggerLogname )
    print "starting the skynetlogger"
    #os.system( 'xterm -e "{ %(packagePath)sbin/Drun skynet-logger %(skynetloggerLogname)s; echo press return to close; read; }" & ' % locals() )
    cmd = 'xterm -e'
    nsargs = "{ %(packagePath)sbin/Drun skynet-logger %(skynetloggerLogname)s; echo press return to close; read; }" % locals()
    pid = spawn( cmd, nsargs )
    pids.append( pid )

  print
  print
  print "once testing is complete, close all windows, and press enter to analyze"
  print "enter 'c' (no quotes) to close all the windows for you"
  blah = raw_input()
  if( blah == 'c' ):
    print "killing processes: " + str(pids)
    for i in pids:
      os.kill( i, signal.SIGTERM )

  os.system( '%(packagePath)sbin/Drun analyzer --state-logfile=%(logname)s --obs-logfile=%(obsFile)s --rndf=%(rndf)s --mdf=%(mdf)s' % locals() )

  try:
    yamConfig = open('%(packagePath)sYAM.config' % locals(), "r" )
    logAnalysis = open( '''%s''' % logAnalysisFile, "a" )
    logAnalysis.write( '\n\n' )
    logAnalysis.write( '-------------------------------\n\n' )
    configLine = yamConfig.readlines()
    for line in configLine:
      singleLine = line.rstrip()
      if ( string.find(singleLine, '#' ,0 ,1) == -1 ):
        logAnalysis.write( singleLine ) 
        logAnalysis.write( '\n' )
    logAnalysis.close()
    yamConfig.close()
  except IOError, (errno, strerror):
    print "I/O error(%s): %s" % (errno, strerror)
  except:
    print "Unexpected error:", sys.exc_info()[0]
    raise


def runTrafsimStack( obsFile, rndf, mdf, sceneFunc, logname, bRunTrafsim, packagePath ):
  
            #'%(packagePath)s./bin/Drun python ../../lib/PYTHON/trafsim/GenericApp.py  ' % locals(), \
  stack = [ '%(packagePath)s./bin/Drun mapviewer --recv-subgroup=-2 ' % locals(), \
            '%(packagePath)s./bin/Drun planviewer2 ' % locals(), \
            # '%(packagePath)s./bin/Drun mapper --rndf=%(rndf)s --noconsole ' % locals(), \
            '%(packagePath)s./bin/Drun testMapper %(rndf)s 2 ' % locals() ]
  if bRunTrafsim:
    tsfile = re.sub( r'\.rndf', '.ts', rndf )
    if 'sceneFunc' in locals () and sceneFunc:
      stack = stack + [ '%(packagePath)s./bin/Drun python -i %(packagePath)slib/PYTHON/trafsim/GenericApp.py --loadFile %(tsfile)s --loadFunction %(sceneFunc)s --sleep 8 '  % locals() ]
    else:
      stack = stack + [ '%(packagePath)s./bin/Drun python -i %(packagePath)slib/PYTHON/trafsim/GenericApp.py --loadFile %(tsfile)s '  % locals() ]
#  else:
#    stack = stack + [ '%(packagePath)s./bin/Drun testMapper %(rndf)s 2 ' %locals() ]

  pids = [] 
  for i in stack:
    cmd = '''xterm -e''' 
    args = "{ %s ; echo press return; read ; }" % (i) 
    pids.append( spawn(cmd, args) )
  return pids

