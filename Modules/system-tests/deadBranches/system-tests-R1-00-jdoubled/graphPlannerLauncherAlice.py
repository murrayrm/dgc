import os;
import sys;

import sysTestLaunch

apps = [ \
#('asim',  '--rndf=../../src/graph-planner/stluke_bigloop.rndf', 2, 'leela'), \
('astate', '', 2, 'leela'), \
 ('graph-planner-viewer', '--rndf=../../src/graph-planner/stluke_bigloop.rndf --goal=2 --loop=5', 2 , 'localhost'), \
 ('trajfollower',  '--disable-trans', 2 , 'leela'), \
 ('planviewer',  '', 2 , 'localhost'), \
 ('adrive', '-t -g -b', 'leela'), \
]

sysTestLaunch.runApps( apps )

