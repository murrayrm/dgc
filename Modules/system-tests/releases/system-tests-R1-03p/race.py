#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'nqe'
rndf = '../../etc/routes-victorville/victorville_mod.rndf'
#mdf = '../../etc/routes-victorville/victorville_startfinish.mdf'
obsFile = 'noObs'
sceneFunc = ''

mdf = sysTestLaunch.selectMDF()
sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

