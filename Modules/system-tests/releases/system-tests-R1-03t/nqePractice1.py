#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'nqePractice1'
rndf = '../../etc/routes-victorville/nqe_practice1.rndf'
mdf = sysTestLaunch.selectMDF()
#mdf = '../../etc/routes-victorville/nqe_practice1.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

