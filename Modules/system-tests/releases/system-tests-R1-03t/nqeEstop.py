#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.1.1'
scenario = 'nqeEstop'
rndf = '../../etc/routes-victorville/estop.rndf'
mdf = '../../etc/routes-victorville/estop.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

