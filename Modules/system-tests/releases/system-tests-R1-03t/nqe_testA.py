#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'nqe_testA'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-victorville/nqe_practice1.mdf'
obsFile = 'nqe_testA_merging.obs'
sceneFunc = 'nqe_testA_tmp'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
