#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'stlukeSmallIntersectionRightLeftSim'
rndf = '../../etc/routes-stluke/stluke_small.rndf'
mdf = '../../etc/routes-stluke/stluke_small_rightleft.mdf'
obsFile = 'noObs.log'
sceneFunc = ''

apps = [ \
('asim', '--no-pause --rndf=%(rndf)s --rndf-start=1.1.1 --gcdrive' % locals(), 2, 'localhost'), \
 ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'localhost'), \
 ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local' % locals(), 2 , 'localhost'), \
 ('rddfplanner',  '--use-final --use-flat --verbose=2', 2 , 'localhost'), \
 ('gcfollower',  '--use-local', 2 , 'localhost'), \
 ('gcdrive',  '--simulate', 2 , 'localhost'), \
# ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, True )

