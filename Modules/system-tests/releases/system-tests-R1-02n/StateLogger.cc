#include <getopt.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <signal.h>
#include <math.h>
#include <sparrowhawk/SparrowHawk.hh>
#include "dgcutils/DGCutils.hh"
#include "skynet/skynet.hh"
#include "alice/AliceConstants.h"
#include "interfaces/sn_types.h"
#include "skynettalker/StateClient.hh"
#include "statelogger_cmdline.h"
#include "stateloggerDisplay.h"

void sigintHandler(int /*sig*/);

class StateLogger : public CStateClient
{ 
  FILE* m_logFile;
  unsigned long long startTime;
  CSparrowHawk *m_shp;
  int m_skynetKey;
  double m_dist;
  double m_avgSpeed;
  double m_maxSpeed;
  int m_time;
  double m_lastNorthing;
  double m_lastEasting;
  int m_numSpeed;
  double m_sumSpeed;

public:
  StateLogger(char* fileName, int skynetKey) : 
    CSkynetContainer(SNstateSim, skynetKey), CStateClient(true)
  {
    CSparrowHawk &sh = SparrowHawk();  
    m_skynetKey = skynetKey;
    m_dist = 0;
    m_avgSpeed = 0;
    m_maxSpeed = 0;
    m_time = 0;
    m_lastNorthing = 0;
    m_lastEasting = 0;
    m_numSpeed = 0;
    m_sumSpeed = 0;

    m_shp = &sh;
    m_shp->add_page(stateloggertable, "StateLogger");
    m_shp->rebind("snkey", &m_skynetKey);
    m_shp->rebind("time", &m_time);
    m_shp->rebind("dist", &(m_dist));
    m_shp->rebind("avgSpeed", &(m_avgSpeed));
    m_shp->rebind("maxSpeed", &(m_maxSpeed));
    m_shp->set_readonly("snkey");
    m_shp->set_readonly("time");
    m_shp->set_readonly("dist");
    m_shp->set_readonly("avgSpeed");
    m_shp->set_readonly("maxSpeed");
    m_shp->run();
    signal(SIGINT, sigintHandler);

    m_logFile = fopen(fileName, "wt");
    if (m_logFile == NULL) {
      cerr << "Cannot open log file: " << fileName << endl;
      exit(1);
    }
    UpdateState();
    UpdateActuatorState();
    startTime = m_state.timestamp;
  }

  ~StateLogger()
  {
    fclose(m_logFile);
    m_shp->stop();
  }

  void listeningLoop()
  {
    UpdateState();
    UpdateActuatorState();
    unsigned timestamp = m_state.timestamp - startTime;
    m_time = (int)timestamp/1000000;
    double northingFront = m_state.utmNorthing + 
      DIST_REAR_AXLE_TO_FRONT*cos(m_state.utmYaw);
    double eastingFront = m_state.utmEasting + 
      DIST_REAR_AXLE_TO_FRONT*sin(m_state.utmYaw);

    if ((m_lastNorthing != 0 || m_lastEasting != 0) && m_actuatorState.m_estoppos == 2) {
      m_dist += sqrt(pow(northingFront - m_lastNorthing, 2) + 
		     pow(eastingFront - m_lastEasting, 2)) * 0.000621371;
    }
    m_lastNorthing = northingFront;
    m_lastEasting = eastingFront;

    double yaw = m_state.utmYaw;
    double vel = sqrt(pow(m_state.utmNorthVel, 2) + 
		      pow(m_state.utmEastVel, 2));
    if (m_actuatorState.m_estoppos == 2) {
      if (vel*2.236936292 > m_maxSpeed)
	m_maxSpeed = vel*2.236936292;

      m_numSpeed++;
      m_sumSpeed = m_sumSpeed + vel*2.236936292;
      m_avgSpeed = m_sumSpeed/m_numSpeed;
    }

    fprintf(m_logFile, "%d\t%0.2lf\t%0.2lf\t%0.6lf\t%0.4lf\t%d\n",
	    timestamp, northingFront, eastingFront, yaw, vel, m_actuatorState.m_estoppos);
    fflush(m_logFile);
  }
};

using std::cout;

volatile sig_atomic_t quit = 0;

void sigintHandler(int /*sig*/)
{
  // if the user presses CTRL-C three or more times, and the program still
  // doesn't terminate, abort
  if (quit > 2) {
    abort();
  }
  quit++;
}

int main(int argc, char** argv)
{
  // catch CTRL-C and exit cleanly, if possible
  signal(SIGINT, sigintHandler);
  // and SIGTERM, i.e. when someone types "kill <pid>" or the like
  signal(SIGTERM, sigintHandler);

  // Parse command line options
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  // Figure out the skynet key
  int skynetKey = skynet_findkey(argc, argv);
  cout << "StateLogger: log file = " << cmdline.file_arg <<endl;
  StateLogger stateLogger = StateLogger(cmdline.file_arg, skynetKey);
  while (!quit) {
    stateLogger.listeningLoop();
    usleep(100000);
  }
}
