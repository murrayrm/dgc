#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.4'
scenario = 'onewayIntersectionTrafficJam'
rndf = '../../etc/routes-advanced/oneway_intersection.rndf'
mdf = '../../etc/routes-advanced/oneway_intersection_trafficjam.mdf'
obsFile = 'onewayIntersectionTrafficJam.obs'
sceneFunc = 'onewayinter_trafficjam'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

