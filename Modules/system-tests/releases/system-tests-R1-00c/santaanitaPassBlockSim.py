#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'santaanitaPassBlockSim'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_testobs.mdf'
obsFile = 'santaanitaSiteVisitPassBlockObs.log'
sceneFunc = 'santaanita_sitevisit_passblock'

apps = [ \
('asim',  '--rndf=%(rndf)s --rndf-start=2.2.5 --gcdrive' % locals(), 2, 'berlin'), \
 ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'moscow'), \
 ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local' % locals(), 2 , 'moscow'), \
 ('rddfplanner',  '--use-endpoints --use-flat --verbose=2', 2 , 'moscow'), \
 ('gcfollower',  '--use-local', 2 , 'berlin'), \
 ('gcdrive',  '--simulate', 2 , 'berlin'), \
# ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, True )

