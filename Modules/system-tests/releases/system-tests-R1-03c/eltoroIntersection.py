#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoroIntersection'
rndf = '../../etc/routes-eltoro/eltoro_field_demo2_adj.rndf'
mdf = '../../etc/routes-eltoro/eltoro_field_demo2_intersection.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
