              Release Notes for "system-tests" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "system-tests" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "system-tests" module can be found in
the ChangeLog file.

Release R1-01v (Tue Aug 14 17:27:30 2007):
	Added simple zone scenario

Release R1-01u (Thu Aug  9  0:24:11 2007):
	* statelogger now has a sparrow display that shows mileage, average
	speed and max speed
	* Changed the log path to /logs when the field flag is given

Release R1-01t (Mon Aug  6 11:54:12 2007):
	Added sensorsim to system-tests. This module runs in between trafsim and mapper. It intercepts trafsim mapelements, modifies them (adds noise, removes from the map if they're out of sensor range, etc) and sends them on to the mapper.

	trafsim obstacles that are out of sensor range will still show up in mapviewer as dotted lines, however these obstacles will not be passed on to planner. If the obstacle is visualized, it will show up with a pink, blue, or green outline.

	IMPORTANT NOTE: if you've been adding obstacles in mapviewer, I highly recommend you start adding them into trafsim instead. All you have to do is hold 'o' and click where you want to place them. This way you can avoid the strange "flickering" behavior that people have been seeing, where obstacles drop in and out.

Release R1-01s (Thu Aug  2 10:18:15 2007):
	Added RNDF for St. Luke for testing intersections without stop lines

Release R1-01r (Thu Aug  2  1:49:12 2007):
	Update the analyzer so it also tells the max and average speed

Release R1-01q (Wed Aug  1 17:21:48 2007):
	Added some new scenarios for santa anita mock field demo1 rndf.

Release R1-01p (Wed Aug  1 14:42:57 2007):
	Made startup window that system-tests opens up wider, changed 
bin prefix passed to startup to not include i486-gentoo-linux so Drun 
works on startup

Release R1-01o (Wed Aug  1 10:57:52 2007):
	Added scripts for baseline and high speed test runs at Santa Anita

Release R1-01n (Tue Jul 31 20:02:43 2007):
	Added a system test for steele lot. This is for system-tests
	of Alice before/without leaving caltech.

Release R1-01m (Tue Jul 31 15:59:41 2007):
	Added test for stluke area

Release R1-01l (Fri Jul 27 17:12:39 2007):
	Took off some comments in sysTestLaunch that were temporarily 
not allowing Trafsim to run. I was on Paris, which apparently "doesn't 
like" Trasim, and I forgot to remove the comments... 

Release R1-01k (Fri Jul 27  5:43:05 2007):
	Changed sysTestLaunch to accomodate the changes in startup, such 
as adding a restart option

Release R1-01j (Thu Jul 26  9:52:50 2007):

The configuration file IntersectionHandling.conf was added. As the file name already implies, this file containts configuration values and flags for intersection handling.
Two flags are important;

If INTERSECTION_SAFETY_FLAG is set to TRUE, the Sitevisit rules apply which are more conservative than the rules for the challenge. e.g. intersection is only clear if there 
is no obstacle in the whole intersection. Turn off this flag only if nominal driving is very reliable and precise.

If INTERSECTION_CABMODE_FLAG is set to TRUE, DARPA's cabmode rule will be activated. If it is Alice's turn to go but the intersection is blocked for more than 10 seconds, Alice needs to follow any 
feasible trajectory through the intersection or needs to replan the mission. Leave this flag at FALSE until planner is finished completly.

Release R1-01i (Tue Jul 24  9:32:42 2007):
	modified the command given when running process-control in field 
configuration by fixing a bug that randomly had a space between -- and  
startall, causing none of the processes to start at the beginning

Release R1-01h (Thu Jul 19 18:29:27 2007):
	sysTestLaunch.py now does not run the sensingStack on its own, 
this is already done in the field.CFG file in startup. Also made xterm 
window bigger so that process-control will not cut off the bottom 
options.

Release R1-01g (Wed Jul 18 13:21:10 2007):
	sysTestLaunch.py now lets process-control call mappers and loggers instead of doing it itself. It now only calls Trafsim, everything else is done in process-control	

Release R1-01f (Thu Jul 12 19:56:45 2007):
	Changed sysTestLaunch.py to run process-control, which has a config file to start all of the planners instead of running 
all the planners in sysTestLaunch.py

Release R1-01e (Mon Jul  2 14:21:18 2007):
	Added a script to run the standard mdf

Release R1-01d (Wed Jun 27 20:45:40 2007):
	Changed command line argument for tplanner.

Release R1-01c (Sun Jun 24 21:21:47 2007):
	Moved logs to /logs

Release R1-01b (Tue Jun 19 21:40:12 2007):
	added scripts for for running stluke_small_smallloop.mdf and stluke_small_bigloop.mdf

Release R1-01a (Sun Jun 17 10:23:42 2007):
	Added a script for going around two loops at St Luke.

Release R1-01 (Sat Jun  9 23:38:45 2007):
	* Added the --pause-braking=0.9 flag to gcdrive and added the
	speed-limit, accel-limit and brake-limit flags to s1planner for
	estop test
	* Added the flag 'costmap' to the script. When it's given,
	the costmap will be shown on mapviewer
	* Added the flag 'sim' to estop test scripts.

Release R1-00z (Sat Jun  9  0:10:15 2007):
	* Added --steer-speed-threshold flag to gcdrive. 
	* Added	estopTestLauncher.py

Release R1-00y (Fri Jun  8 11:04:33 2007):
  Fixed an issue with template-ladarperceptor being restarted continually.
  ps was returning a string truncated to 16 characters which did not match 
  the restart mechanism currently cannot distinguish modules with names 
  similar in hte first 16 char

  gcfollower has --use-lead appended to it arguments (don't know who made 
  this change -- suspect stafano)

Release R1-00x (Wed Jun  6 23:45:23 2007):
	restart code looks only on machine designated for the process to minimize 
  polling interval and response time

Release R1-00w (Wed Jun  6 22:59:37 2007):
	Do not look for processes on all machines when we're running in
	simulation

Release R1-00v (Wed Jun  6 13:20:02 2007):
  * new code to look for processes on all machines..  Should prompt for
  user to take independent action if a duplicate process is running
  * monitor for processes and to restart them if not present

Release R00u (Tue Jun  5 15:04:54 2007):
	Fixed segfault

Release R1-00t (Wed May 30  8:40:38 2007):
	added a selectMDF() function for basic UI of selecting MDF files
  from an anticipated folder

Release R1-00s (Thu May 31 23:23:00 2007):
	All logs go to /tmp/logs

Release R1-00r (Thu May 31  6:17:33 2007):
	* Added an updated santaanitaIntersection scenario 
(santaanitaIntersectionScripted) that adds the cars once Alice has 
passed a trafsim "checkpoint"
	* Added 2 extended scenarios for Santa Anita.  These scenarios 
include multiple types of obstacles and cars will appear when Alice 
reaches some trafsim "checkpoints".  Some quick tests show Alice having 
some problems with the scenarios.
	* Merge appeared successful

Release R1-00q (Thu May 31  1:05:18 2007):
	Run the different processes on the machines on Alice in simulation
	when 'alicesim' argument is given. Added some sleep to the sensing
	script.

Release R1-00p (Wed May 30 12:30:47 2007):
	Log all the tplanner and s1planner output (whatever they print out
	on the screen). The log files have timestamp and scenario name
	appended to the module name (same format as log file automatically
	generated by gcmodule). This is just a temporary solution as those
	modules cannot yet log what they're doing. Since I just did '|
	tee', what printed out on the screen may be delayed.

Release R1-00o (Mon May 28 22:03:44 2007):
	Used tplannerCSS by default. Draw polytopes instead of rddf on
	mapviewer. Added --use-new flag to gcfollower.

Release R1-00n (Sat May 26  3:48:55 2007):
	From Scott Goodfriend: I broke my svn for system-tests
	* Changed the 4 Santa Anita special case scenarios so that they 
have .obs files with them
	* Added 4 St. Luke dynamic obstacle scenarios
	* Nok changed the start-up script so that trafsim would start 
first and then sleep 8 seconds for the planning stack to start.  To 
allow trafsim and Alice to start less far apart in time.

Release R1-00m (Fri May 25 19:13:07 2007):
	* Fixed the name of log file.
	* Append YAM.config to analysis file.

Release R1-00l (Thu May 24 13:41:33 2007):
	* Removed the press enter before starting trafsim stack so the timing
	works right for dynamic obstacles. 
	* Added all the sensing modules to sensingLaunch
	* Added one more scenrio (turning left) as per Noel's request.
	* Added parser for dynamic obstacles.
	* Do not start skynettalker if we run in sim.
	* Changed the name of skynetlogger logfile so it uses the same format
	as gcmodule

Release R1-00j (Wed May 23 20:47:50 2007):	
	* Only need to specify starting point, scenario, rndf, mdf, obsFile
	and sceneFunc in the script. sysTestLaunch.runPlanners() contains the default set
	of planners to run based on the command line argument (*sim* or
	*field*). Because of this, we don't need two versions of the script
	(one for field and one for sim) anymore. 
	* To run the sim version, do, for example,
	  python -i santaanitaIntersectionRight.py sim
	  To run the field version which will also starts all the sensing modules specified in sensingLaunch.py, use field instead of sim.
	* Add sensingLaunch.py that specifies sensing modules that will be run in Alice. I don't have the complete set of sensing modules yet. So right now, it only starts template-ladarperceptor. Will ask Sam to add other modules in.


Release R1-00i (Wed May 23 12:49:33 2007):
	* You can now press 'c' to quit all the program.
	* Added template-ladarperceptor to all the scripts.
	* Automatically start skynetlogger.


Release R1-00h (Wed May 23  1:46:16 2007):
	* Dramatically reduced the number of stop line messages. Richard
	should be happier now.
	* Made s1planner the default planner instead of rddfplanner.
	* Fixed send/recv subgroups in mapviewer, testMapper, mapper and
	trafsim.

Release R1-00g (Tue May 22  4:13:12 2007):
	Added three trafsim created scenarios that are kinda weird, but give 
interesting results.

Release R1-00f (Tue May 22  0:27:44 2007):
	* Fixed stupid segfault problem in the analyzer. Added more scripts
	for testing with s1planner in sim
	* log the apps and params in a given run into the ..analyze log

Release R1-00e (Sun May 20 22:14:56 2007):
	Added files that are used in the field test on May 20, 2007. To run tplannerNoCSS-R2-02u, create a link in bin to an executable tplannerNoCSS from tplanner-R2-02u.

Release R1-00d (Sun May 20 12:44:53 2007):
	* Changed command line argument for rddfplanner from --use-endpoints
	to --use-final. Run testMapper instead of trafsim when we're not
	running in sim so we also see the rndf plotted on mapviewer.
	* Run testMapper instead of trafsim for field config
	* Unpaused asim at startup


Release R1-00c (Fri May 18 10:29:50 2007):
	Added a new script for a new scenario 
(santaanitaSparseRoadBlock.py)

Release R1-00b (Wed May 16 13:09:15 2007):
	* Modularized the test script.  
	* Added scripts for basic navigation
	* Added the state logger and the analyzer

Release R1-00a (Mon May 14 13:30:47 2007):
	Extended to allow running apps on remote machines via ssh.
  

Release R1-00 (Fri May 11  9:36:20 2007):
	Created.



















































