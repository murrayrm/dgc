#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'santaanitaFakeIntersectionOrdering'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_simpleloop.mdf'
obsFile = 'santaanitaFakeIntersectionOrdering.obs'
sceneFunc = 'santaanita_sitevisit_fakeIntersectionOrdering'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )
