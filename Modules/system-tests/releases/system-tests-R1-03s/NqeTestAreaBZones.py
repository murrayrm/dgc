#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '11.2.14'
scenario = 'NqeTestAreaBZones'
rndf = '../../etc/routes-darpa/nqe.rndf'
mdf = '../../etc/routes-darpa/nqe_test_areab_zones.mdf'
obsFile = 'NqeTestAreaBZones.obs'
sceneFunc = 'nqe_test_areabzones'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
