#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'santaanitaExtended'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_simpleloop.mdf'
obsFile = 'santaanitaExtended.obs'
sceneFunc = 'santaanita_sitevisit_extended'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
