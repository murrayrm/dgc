#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeSmallZoneExtendedZone9'
rndf = '../../etc/routes-stluke/stluke_small_zone_extended.rndf'
mdf = '../../etc/routes-stluke/stluke_small_zone_extended_zone9only.mdf'
obsFile = 'stlukeSmallZoneExtendedZone9.obs'
sceneFunc = 'stlukesmallzoneextended_zone9'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
