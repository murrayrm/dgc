#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'nqeTestAMoreCars'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-victorville/nqe_practice1.mdf'
obsFile = ''
sceneFunc = 'nqe_testA_morecars'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

