#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '20.2.6'
scenario = 'NqeTestAreaC'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-darpa/nqe_test3.mdf'
obsFile = 'NqeTestAreaC.obs'
sceneFunc = 'nqe_area_c_mod'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

