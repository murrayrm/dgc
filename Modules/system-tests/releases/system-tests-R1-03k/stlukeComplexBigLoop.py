#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeComplexBigLoop'
rndf = '../../etc/routes-stluke/stluke_complex.rndf'
mdf = '../../etc/routes-stluke/stluke_complex_bigloop.mdf'
obsFile = 'stlukeComplexBigLoop.obs'
sceneFunc = 'stlukecomplex_bigloop'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
