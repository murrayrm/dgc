#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoroBuildingsAndBushes'
rndf = '../../etc/routes-eltoro/eltoro_field_demo2.rndf'
mdf = '../../etc/routes-eltoro/eltoro_northofperim.mdf'
obsFile = 'eltoroBuildingsAndBushes.obs'
sceneFunc = 'eltoro_buildingsandbushes'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
